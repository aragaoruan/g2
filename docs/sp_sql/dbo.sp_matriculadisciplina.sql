CREATE PROCEDURE [dbo].[sp_matriculadisciplina]
    (
      @id_contrato INT ,
      @bl_retornamatricula BIT = 1
    )
AS
    SET NOCOUNT ON;

    DECLARE @id_matricula INT ,
        @id_projetopedagogico INT ,
        @id_modulo INT ,
        @id_disciplina INT ,
        @bl_obrigatoria BIT



    DECLARE MATRICULA CURSOR
    FOR
        SELECT  id_matricula
        FROM    tb_contratomatricula
        WHERE   id_contrato = @id_contrato
    OPEN MATRICULA
    FETCH NEXT FROM MATRICULA INTO @id_matricula
    WHILE @@FETCH_STATUS = 0
        BEGIN
            DELETE  FROM tb_matriculadisciplina
            WHERE   id_matricula = @id_matricula and id_evolucao = 11
            DECLARE PROJETOPEDAGOGICO CURSOR
            FOR
                SELECT  id_projetopedagogico
                FROM    tb_matricula AS m
                WHERE   m.id_matricula = @id_matricula
            OPEN PROJETOPEDAGOGICO
            FETCH NEXT FROM PROJETOPEDAGOGICO INTO @id_projetopedagogico
            WHILE @@FETCH_STATUS = 0
                BEGIN
                    DECLARE MODULO CURSOR
                    FOR
                        SELECT  id_modulo
                        FROM    tb_modulo
                        WHERE   id_projetopedagogico = @id_projetopedagogico
                                AND bl_ativo = 1
                    OPEN MODULO
                    FETCH NEXT FROM MODULO INTO @id_modulo
                    WHILE @@FETCH_STATUS = 0
                        BEGIN
                            DECLARE DISCIPLINA CURSOR
                            FOR
                                SELECT  id_disciplina ,
                                        bl_obrigatoria
                                FROM    tb_modulodisciplina
                                WHERE   id_modulo = @id_modulo
                                        AND bl_ativo = 1
                                        AND id_disciplina NOT IN (
                                            SELECT id_disciplina FROM tb_matriculadisciplina as md
                                            WHERE md.id_matricula = @id_matricula
                                )
                            OPEN DISCIPLINA
                            FETCH NEXT FROM DISCIPLINA INTO @id_disciplina,
                                @bl_obrigatoria
                            WHILE @@FETCH_STATUS = 0
                                BEGIN
                                    INSERT  INTO tb_matriculadisciplina
                                            ( id_matricula ,
                                              id_evolucao ,
                                              id_disciplina ,
                                              id_situacao ,
                                              nu_aprovafinal ,
                                              bl_obrigatorio ,
                                              id_matriculaoriginal
                                            )
                                    VALUES  ( @id_matricula ,
                                              11 ,
                                              @id_disciplina ,
                                              53 ,
                                              NULL ,
                                              @bl_obrigatoria ,
                                              @id_matricula
                                            )
                                    FETCH NEXT FROM DISCIPLINA INTO @id_disciplina,
                                        @bl_obrigatoria
                                END;
                            CLOSE DISCIPLINA;
                            DEALLOCATE DISCIPLINA;
                            FETCH NEXT FROM MODULO INTO @id_modulo
                        END;
                    CLOSE MODULO;
                    DEALLOCATE MODULO;
                    FETCH NEXT FROM PROJETOPEDAGOGICO INTO @id_projetopedagogico
                END;
            CLOSE PROJETOPEDAGOGICO;
            DEALLOCATE PROJETOPEDAGOGICO;
            FETCH NEXT FROM MATRICULA INTO @id_matricula
        END;
    CLOSE MATRICULA;
    DEALLOCATE MATRICULA;

    IF ( @bl_retornamatricula = 1 )
        BEGIN
    SELECT  id_matriculadisciplina ,
                    id_evolucao ,
                    id_situacao ,
                    id_matricula ,
                    id_disciplina ,
                    nu_aprovafinal ,
                    bl_obrigatorio ,
                    dt_conclusao
            FROM    tb_matriculadisciplina
            WHERE   id_matricula = @id_matricula
        END

CREATE PROCEDURE [dbo].[sp_importarusuario]
    @id_usuario INT ,
    @id_entidadeorigem INT ,
    @id_entidadedestino INT ,
    @id_usuariocadastro INT
AS 
    BEGIN
        SET NOCOUNT ON;

--BLOCO DE DECLARA‚ÌO DE VARIçVEIS A SEREM UTILIZADAS NO PROCESSO
--===============================================================
        DECLARE @bl_ativo BIT ,
            @bl_padrao BIT ,
            @dt_dataexpedicao DATETIME ,
            @dt_nascimento DATETIME ,
            @dt_datareservista DATETIME ,
            @dt_fim DATETIME ,
            @dt_inicio DATETIME ,
            @id_areaatuacao INT ,
            @id_endereco INT ,
            @id_redesocial INT ,
            @id_email INT ,
            @id_pessoa INT ,
            @id_telefone INT ,
            @id_situacao INT ,
            @id_tipodeconta INT ,
            @id_estadocivil INT ,
            @id_municipio INT ,
            @id_etnia INT ,
            @id_tiposanguineo INT ,
            @id_pais INT ,
            @id_nivelensino INT ,
            @nu_numero VARCHAR(50) ,
            @nu_serie VARCHAR(20) ,
            @nu_rg VARCHAR(20) ,
            @nu_zona INT ,
            @nu_glicemia VARCHAR(MAX) ,
            @st_curso NVARCHAR(200) ,
            @st_cargo NVARCHAR(150) ,
            @st_organizacao NVARCHAR(150) ,
            @st_nomeinstituicao NVARCHAR(200) ,
            @st_orgaoexpedidor VARCHAR(80) ,
            @st_alergias VARCHAR(MAX) ,
            @st_deficiencias VARCHAR(MAX) ,
            @st_necessidadesespeciais VARCHAR(MAX) ,
            @st_glicemia VARCHAR(MAX) ,
            @st_digitoagencia VARCHAR(2) ,
            @st_agencia VARCHAR(20) ,
            @st_digitoconta VARCHAR(2) ,
            @st_conta VARCHAR(10) ,
            @st_banco VARCHAR(3) ,
            @sg_uf CHAR(2) ,
            @st_area VARCHAR(20) ,
            @st_livro VARCHAR(50) ,
            @st_folhas VARCHAR(10) ,
            @st_descricaocartorio VARCHAR(8000) ,
            @st_nomeexibicao VARCHAR(255) ,
            @st_nomemae VARCHAR(100) ,
            @st_nomepai VARCHAR(255) ,
            @st_passaporte VARCHAR(255) ,
            @st_login VARCHAR(100) ,
            @st_senha VARCHAR(30) ,
            @st_sexo CHAR(1);

--===============================================================

        IF @id_entidadedestino = @id_entidadeorigem 
            BEGIN
                PRINT 'A entidade de origem e destino não pode ser igual';
                RETURN (2);
            END

-- verifico se existe o usu‡rio informado
        IF NOT EXISTS ( SELECT  u.id_usuario
                        FROM    dbo.tb_usuario u
                        WHERE   u.id_usuario = @id_usuario ) 
            BEGIN
                PRINT 'Usu‡rio inexistente';
                RETURN (2);
            END

-- verifico se existe a entidade de origem
        IF NOT EXISTS ( SELECT  e.id_entidade
                        FROM    dbo.tb_entidade e
                        WHERE   e.id_entidade = @id_entidadeorigem ) 
            BEGIN
                PRINT 'Entidade de origem inexistente';
                RETURN (2);
            END

-- verifico se existe a entidade de destino
        IF NOT EXISTS ( SELECT  e.id_entidade
                        FROM    dbo.tb_entidade e
                        WHERE   e.id_entidade = @id_entidadedestino ) 
            BEGIN
                PRINT 'Entidade de destino inexistente';
                RETURN (2);
            END

--verifico se existe o usu‡rio na entidade de origem
        IF NOT EXISTS ( SELECT  pes.id_usuario id_pessoa
                        FROM    dbo.tb_usuario usu
                                INNER JOIN dbo.tb_pessoa pes ON pes.id_usuario = usu.id_usuario
                                                              AND pes.id_entidade = @id_entidadeorigem
                        WHERE   usu.id_usuario = @id_usuario ) 
            BEGIN
                PRINT ' Usu‡rio inexistente na entidade de origem ';
                RETURN (2);
            END

--verifico se existe o usu‡rio na entidade de destino
        IF EXISTS ( SELECT  pes.id_usuario id_pessoa
                    FROM    dbo.tb_usuario usu
                            INNER JOIN dbo.tb_pessoa pes ON pes.id_usuario = usu.id_usuario
                                                            AND pes.id_entidade = @id_entidadedestino
                    WHERE   usu.id_usuario = @id_usuario ) 
            BEGIN
                SELECT  u.id_usuario ,
                        u.st_cpf ,
                        u.st_login ,
                        u.st_nomecompleto ,
                        u.bl_ativo ,
                        p.id_entidade ,
                        p.dt_cadastro ,
                        p.id_situacao
                FROM    dbo.tb_usuario u
                        INNER JOIN dbo.tb_pessoa p ON p.id_usuario = u.id_usuario
                WHERE   u.id_usuario = @id_usuario
                        AND p.id_entidade = @id_entidadedestino

                RETURN (0);
            END


-- INICIANDO A TRANSA‚ÌO DE IMPORTA‚ÌO DE DADOS
        BEGIN TRANSACTION ImportarPessoa WITH MARK;

/*
* DELETANDO OS DADOS ANTIGOS DO USUARIO
*/

-- DELETANDO OS DADOS DE ENDERECO
--DELETE FROM dbo.tb_pessoaendereco WHERE id_usuario = @id_usuario AND id_entidade = @id_entidadedestino;
-- DELETANDO OS DADOS DA ENTIDADE DE DESTINo
--DELETE FROM dbo.tb_documentocertidaodenascimento WHERE id_usuario = @id_usuario AND id_entidade = @id_entidadedestino;
-- DELETANDO OS DADOS DA ENTIDADE DE destino
--DELETE FROM tb_documentotitulodeeleitor WHERE id_entidade = @id_entidadedestino AND id_usuario = @id_usuario;
--deletando os dados da entidade de destino
--DELETE FROM tb_documentodereservista WHERE id_entidade = @id_entidadedestino AND id_usuario = @id_usuario;
-- deletando os dados da entidade de destino
--DELETE FROM tb_contatostelefonepessoa WHERE id_entidade = @id_entidadedestino AND id_usuario = @id_usuario ;
-- deletando os dados banc‡rios da entidade de destindo
--DELETE FROM dbo.tb_contapessoa WHERE id_entidade = @id_entidadedestino AND id_usuario = @id_usuario;
-- deletando os contatos de email da pessoa
--DELETE FROM dbo.tb_contatosemailpessoaperfil WHERE id_entidade = @id_entidadedestino AND id_usuario = @id_usuario;
-- deletando os dados biomŽdicos
--DELETE FROM dbo.tb_dadosbiomedicos WHERE id_entidade = @id_entidadedestino AND id_usuario = @id_usuario ;
-- deletando o documetno de identidade
--DELETE FROM dbo.tb_documentoidentidade WHERE id_entidade = @id_entidadedestino AND id_usuario = @id_usuario ;
-- deletando os contatos de redes sociais
--DELETE FROM dbo.tb_contatosredesocialpessoa WHERE id_entidade = @id_entidadedestino AND id_usuario = @id_usuario;
-- deletando os dados de informacao academica
--DELETE FROM dbo.tb_informacaoacademicapessoa WHERE id_entidade = @id_entidadedestino AND id_usuario = @id_usuario;

--DELETE FROM dbo.tb_informacaoprofissionalpessoa WHERE id_entidade = @id_entidadedestino AND id_usuario = @id_usuario;

-- Apagando os dados antigos de pessoa da entidade
--DELETE FROM dbo.tb_pessoa WHERE id_entidade = @id_entidadedestino AND id_usuario = @id_usuario

/*
* Duplicando dados de pessoa
*/
        DECLARE cursor_pessoa CURSOR
        FOR
            SELECT  pes.bl_ativo ,
                    pes.dt_nascimento ,
                    pes.id_estadocivil ,
                    pes.id_municipio ,
                    pes.id_pais ,
                    pes.id_situacao ,
                    pes.sg_uf ,
                    pes.st_nomeexibicao ,
                    pes.st_nomemae ,
                    pes.st_nomepai ,
                    pes.st_passaporte ,
                    pes.st_sexo
            FROM    dbo.tb_pessoa pes
            WHERE   pes.id_usuario = @id_usuario
                    AND pes.id_entidade = @id_entidadeorigem;

        OPEN cursor_pessoa;

        FETCH NEXT FROM cursor_pessoa INTO @bl_ativo, @dt_nascimento,
            @id_estadocivil, @id_municipio, @id_pais, @id_situacao, @sg_uf,
            @st_nomeexibicao, @st_nomemae, @st_nomepai, @st_passaporte,
            @st_sexo;

        INSERT  INTO dbo.tb_pessoa
                ( id_usuario ,
                  bl_ativo ,
                  dt_nascimento ,
                  id_estadocivil ,
                  id_municipio ,
                  id_pais ,
                  id_situacao ,
                  sg_uf ,
                  st_nomeexibicao ,
                  st_nomemae ,
                  st_nomepai ,
                  st_passaporte ,
                  st_sexo ,
                  id_entidade ,
                  dt_cadastro ,
                  id_usuariocadastro 
                )
        VALUES  ( @id_usuario ,
                  1 ,
                  @dt_nascimento ,
                  @id_estadocivil ,
                  @id_municipio ,
                  @id_pais ,
                  1 ,
                  @sg_uf ,
                  @st_nomeexibicao ,
                  @st_nomemae ,
                  @st_nomepai ,
                  @st_passaporte ,
                  @st_sexo ,
                  @id_entidadedestino ,
                  GETDATE() ,
                  @id_usuariocadastro 
                )

        IF @@ERROR <> 0 
            BEGIN
                CLOSE cursor_pessoa;
                DEALLOCATE cursor_pessoa;
                ROLLBACK TRANSACTION ImportarPessoa;
                RETURN @@Error
            END

        CLOSE cursor_pessoa;
        DEALLOCATE cursor_pessoa;


/*

*/



        DECLARE cursor_endereco CURSOR
        FOR
            SELECT  id_endereco ,
                    bl_padrao
            FROM    dbo.tb_pessoaendereco
            WHERE   id_usuario = @id_usuario
                    AND id_entidade = @id_entidadeorigem;

        OPEN cursor_endereco;
        FETCH NEXT FROM cursor_endereco INTO @id_endereco, @bl_padrao

        WHILE @@FETCH_STATUS = 0 
            BEGIN
                INSERT  INTO dbo.tb_pessoaendereco
                        ( bl_padrao ,
                          id_endereco ,
                          id_usuario ,
                          id_entidade 
                        )
                VALUES  ( @bl_padrao ,
                          @id_endereco ,
                          @id_usuario ,
                          @id_entidadedestino 
                        )

                IF @@ERROR <> 0 
                    BEGIN
                        CLOSE cursor_endereco;
                        DEALLOCATE cursor_endereco;
                        ROLLBACK TRANSACTION ImportarPessoa;
                        RETURN @@Error
                    END

                FETCH NEXT FROM cursor_endereco INTO @id_endereco, @bl_padrao
            END
        CLOSE cursor_endereco;
        DEALLOCATE cursor_endereco;


--listo o documento de certid‹o de nascimento do usu‡rio (tb_documentocertidaodenascimento)
/*
* Duplicando o documento de certid‹o de nascimento
*/

        DECLARE cursor_certidao_nascimento CURSOR
        FOR
            SELECT  st_numero ,
                    st_livro ,
                    st_folhas ,
                    st_descricaocartorio ,
                    dt_dataexpedicao
            FROM    dbo.tb_documentocertidaodenascimento
            WHERE   id_usuario = @id_usuario
                    AND id_entidade = @id_entidadeorigem

        OPEN cursor_certidao_nascimento;
        FETCH NEXT FROM cursor_certidao_nascimento INTO @nu_numero, @st_folhas,
            @st_livro, @st_descricaocartorio, @dt_dataexpedicao

        IF @@FETCH_STATUS = 0 
            BEGIN

                INSERT  INTO dbo.tb_documentocertidaodenascimento
                        ( id_entidade ,
                          id_usuario ,
                          dt_dataexpedicao ,
                          st_folhas ,
                          st_livro ,
                          st_descricaocartorio ,
                          st_numero 
                        )
                VALUES  ( @id_entidadedestino ,
                          @id_usuario ,
                          @dt_dataexpedicao ,
                          @st_folhas ,
                          @st_livro ,
                          @st_descricaocartorio ,
                          @nu_numero 
                        )


                IF @@ERROR <> 0 
                    BEGIN
                        CLOSE cursor_certidao_nascimento;
                        DEALLOCATE cursor_certidao_nascimento;
                        ROLLBACK TRANSACTION ImportarPessoa;
                        RETURN @@Error
                    END

            END
        CLOSE cursor_certidao_nascimento;
        DEALLOCATE cursor_certidao_nascimento;

/*
* Duplicando os dados de documento de titulo de eleitor
*/


-- LISTANDO E DUPLICANDO OS DADOS DA ENTIDADE DE ORIGEM
        DECLARE cursor_documento_titulo_eleitor CURSOR
        FOR
            SELECT  st_numero ,
                    st_zona ,
                    st_area ,
                    dt_dataexpedicao
            FROM    dbo.tb_documentotitulodeeleitor
            WHERE   id_entidade = @id_entidadeorigem
                    AND id_usuario = @id_usuario
        OPEN cursor_documento_titulo_eleitor;
        FETCH NEXT FROM cursor_documento_titulo_eleitor INTO @nu_numero,
            @nu_zona, @st_area, @dt_dataexpedicao

        IF ( @@FETCH_STATUS = 0 ) 
            BEGIN
                INSERT  INTO dbo.tb_documentotitulodeeleitor
                        ( dt_dataexpedicao ,
                          id_entidade ,
                          id_usuario ,
                          st_numero ,
                          st_zona ,
                          st_area 
                        )
                VALUES  ( @dt_dataexpedicao ,
                          @id_entidadedestino ,
                          @id_usuario ,
                          @nu_numero ,
                          @nu_zona ,
                          @st_area 
                        )

                IF @@ERROR <> 0 
                    BEGIN
                        CLOSE cursor_documento_titulo_eleitor;
                        DEALLOCATE cursor_documento_titulo_eleitor;
                        ROLLBACK TRANSACTION ImportarPessoa;
                        RETURN @@Error
                    END
            END
        CLOSE cursor_documento_titulo_eleitor;
        DEALLOCATE cursor_documento_titulo_eleitor;

/*
* Duplicando o documento de reservista (tb_documentodereservista)
*/


-- listando e duplicando os dados na entidade de destino
        DECLARE cursor_documento_reservista CURSOR
        FOR
            SELECT  st_numero ,
                    st_serie ,
                    dt_datareservista ,
                    sg_uf ,
                    id_municipio
            FROM    dbo.tb_documentodereservista
            WHERE   id_entidade = @id_entidadeorigem
                    AND id_usuario = @id_usuario;

        OPEN cursor_documento_reservista;
        FETCH NEXT FROM cursor_documento_reservista INTO @nu_numero, @nu_serie,
            @dt_datareservista, @sg_uf, @id_municipio

        IF @@FETCH_STATUS = 0 
            BEGIN
                INSERT  INTO dbo.tb_documentodereservista
                        ( dt_datareservista ,
                          id_entidade ,
                          id_municipio ,
                          id_usuario ,
                          st_numero ,
                          st_serie ,
                          sg_uf 
                        )
                VALUES  ( @dt_datareservista ,
                          @id_entidadedestino ,
                          @id_municipio ,
                          @id_usuario ,
                          @nu_numero ,
                          @nu_serie ,
                          @sg_uf 
                        )

                IF @@ERROR <> 0 
                    BEGIN
                        CLOSE cursor_documento_reservista;
                        DEALLOCATE cursor_documento_reservista;
                        ROLLBACK TRANSACTION ImportarPessoa;
                        RETURN @@Error
                    END
            END
        CLOSE cursor_documento_reservista;
        DEALLOCATE cursor_documento_reservista;

--duplicando os contatos de telefone

/*
* Duplicando os contatos de telefone
*/


--listando e duplicando os dados da entidade de origem
        DECLARE cursor_contato_telefone CURSOR
        FOR
            SELECT  id_telefone ,
                    bl_padrao
            FROM    tb_contatostelefonepessoa
            WHERE   id_usuario = @id_usuario
                    AND id_entidade = @id_entidadeorigem

        OPEN cursor_contato_telefone;
        FETCH NEXT FROM cursor_contato_telefone INTO @id_telefone, @bl_padrao

        WHILE @@FETCH_STATUS = 0 
            BEGIN

                INSERT  INTO tb_contatostelefonepessoa
                        ( bl_padrao ,
                          id_entidade ,
                          id_telefone ,
                          id_usuario 
                        )
                VALUES  ( @bl_padrao ,
                          @id_entidadedestino ,
                          @id_telefone ,
                          @id_usuario 
                        )

                IF @@ERROR <> 0 
                    BEGIN
                        CLOSE cursor_contato_telefone;
                        DEALLOCATE cursor_contato_telefone;
                        ROLLBACK TRANSACTION ImportarPessoa;
                        RETURN @@Error
                    END

                FETCH NEXT FROM cursor_contato_telefone INTO @id_telefone,
                    @bl_padrao
            END
        CLOSE cursor_contato_telefone;
        DEALLOCATE cursor_contato_telefone;


/*
* Duplicando os dados de conta
*/


        DECLARE cursor_conta_pessoa CURSOR
        FOR
            SELECT  st_banco ,
                    id_tipodeconta ,
                    st_digitoagencia ,
                    st_agencia ,
                    st_digitoconta ,
                    st_conta ,
                    bl_padrao
            FROM    dbo.tb_contapessoa
            WHERE   id_entidade = @id_entidadeorigem
                    AND id_usuario = @id_usuario

        OPEN cursor_conta_pessoa;

        FETCH NEXT FROM cursor_conta_pessoa INTO @st_banco, @id_tipodeconta,
            @st_digitoagencia, @st_agencia, @st_digitoconta, @st_conta,
            @bl_padrao

        WHILE @@FETCH_STATUS = 0 
            BEGIN
                INSERT  INTO dbo.tb_contapessoa
                        ( bl_padrao ,
                          st_banco ,
                          id_entidade ,
                          id_tipodeconta ,
                          id_usuario ,
                          st_agencia ,
                          st_conta ,
                          st_digitoagencia ,
                          st_digitoconta 
                        )
                VALUES  ( @bl_padrao ,
                          @st_banco ,
                          @id_entidadedestino ,
                          @id_tipodeconta ,
                          @id_usuario ,
                          @st_agencia ,
                          @st_conta ,
                          @st_digitoagencia ,
                          @st_digitoconta 
                        )

                IF @@ERROR <> 0 
                    BEGIN
                        CLOSE cursor_conta_pessoa;
                        DEALLOCATE cursor_conta_pessoa;
                        ROLLBACK TRANSACTION ImportarPessoa;
                        RETURN @@Error
                    END

                FETCH NEXT FROM cursor_conta_pessoa INTO @st_banco,
                    @id_tipodeconta, @st_digitoagencia, @st_agencia,
                    @st_digitoconta, @st_conta, @bl_padrao
            END
        CLOSE cursor_conta_pessoa;
        DEALLOCATE cursor_conta_pessoa;




/*
* Duplicando as contas de e-mail
*/


        DECLARE cursor_conta_email CURSOR
        FOR
            SELECT  bl_padrao ,
                    id_email ,
                    bl_ativo
            FROM    dbo.tb_contatosemailpessoaperfil
            WHERE   id_entidade = @id_entidadeorigem
                    AND id_usuario = @id_usuario

        OPEN cursor_conta_email;
        FETCH NEXT FROM cursor_conta_email INTO @bl_padrao, @id_email,
            @bl_ativo

        WHILE @@FETCH_STATUS = 0 
            BEGIN

--verificar se existe tal conta de e-mail como nula e criar outra nula?????

                INSERT  INTO dbo.tb_contatosemailpessoaperfil
                        ( bl_ativo ,
                          bl_padrao ,
                          id_email ,
                          id_entidade ,
                          id_usuario 
                        )
                VALUES  ( @bl_ativo ,
                          @bl_padrao ,
                          @id_email ,
                          @id_entidadedestino ,
                          @id_usuario 
                        )

                IF @@ERROR <> 0 
                    BEGIN
                        CLOSE cursor_conta_email;
                        DEALLOCATE cursor_conta_email;
                        ROLLBACK TRANSACTION ImportarPessoa;
                        RETURN @@Error
                    END

                FETCH NEXT FROM cursor_conta_email INTO @bl_padrao, @id_email,
                    @bl_ativo
            END
        CLOSE cursor_conta_email;
        DEALLOCATE cursor_conta_email;


/*
*Duplicando os dados de acesso
*/


        DECLARE cursor_dados_acesso CURSOR
        FOR
            SELECT  DA.st_login ,
                    DA.st_senha ,
                    DA.bl_ativo
            FROM    tb_dadosacesso AS DA
            WHERE   DA.id_usuario = @id_usuario
                    AND DA.id_entidade = @id_entidadeorigem
                    AND DA.bl_ativo = 1
                    AND DA.bl_ativo = @bl_ativo

        OPEN cursor_dados_acesso;

        FETCH NEXT FROM cursor_dados_acesso INTO @st_login, @st_senha,
            @bl_ativo

        WHILE @@FETCH_STATUS = 0 
            BEGIN 
                INSERT  INTO dbo.tb_dadosacesso
                        ( id_usuario ,
                          st_login ,
                          st_senha ,
                          dt_cadastro ,
                          bl_ativo ,
                          id_usuariocadastro ,
                          id_entidade
                        )
                VALUES  ( @id_usuario , -- id_usuario - int
                          @st_login , -- st_login - varchar(50)
                          @st_senha , -- st_senha - varchar(50)
                          GETDATE() , -- dt_cadastro - datetime2
                          @bl_ativo , -- bl_ativo - bit
                          @id_usuario , -- id_usuariocadastro - int
                          @id_entidadedestino-- id_entidade - int
                        )
				
                IF @@ERROR <> 0 
                    BEGIN 
                        CLOSE cursor_dados_acesso;
                        DEALLOCATE cursor_dados_acesso;
                        ROLLBACK TRANSACTION ImportarPessoa;
                        RETURN @@ERROR
                    END
		
                FETCH NEXT FROM cursor_dados_acesso INTO @st_login, @st_senha,
                    @bl_ativo
            END 
        CLOSE cursor_dados_acesso;
        DEALLOCATE cursor_dados_acesso;      

/*
* Duplicando os dados biomŽdicos
*/



        DECLARE cursor_dados_biomedicos CURSOR
        FOR
            SELECT  id_etnia ,
                    id_tiposanguineo ,
                    nu_glicemia ,
                    st_glicemia ,
                    st_alergias ,
                    st_deficiencias ,
                    st_necessidadesespeciais
            FROM    dbo.tb_dadosbiomedicos
            WHERE   id_entidade = @id_entidadeorigem
                    AND id_usuario = @id_usuario;

        OPEN cursor_dados_biomedicos;
        FETCH NEXT FROM cursor_dados_biomedicos INTO @id_etnia,
            @id_tiposanguineo, @nu_glicemia, @st_glicemia, @st_alergias,
            @st_deficiencias, @st_necessidadesespeciais

        WHILE @@FETCH_STATUS = 0 
            BEGIN
                INSERT  INTO dbo.tb_dadosbiomedicos
                        ( id_entidade ,
                          id_etnia ,
                          id_tiposanguineo ,
                          id_usuario ,
                          nu_glicemia ,
                          st_alergias ,
                          st_deficiencias ,
                          st_glicemia ,
                          st_necessidadesespeciais 
                        )
                VALUES  ( @id_entidadedestino ,
                          @id_etnia ,
                          @id_tiposanguineo ,
                          @id_usuario ,
                          @nu_glicemia ,
                          @st_alergias ,
                          @st_deficiencias ,
                          @st_glicemia ,
                          @st_necessidadesespeciais 
                        )

                IF @@ERROR <> 0 
                    BEGIN
                        CLOSE cursor_dados_biomedicos;
                        DEALLOCATE cursor_dados_biomedicos;
                        ROLLBACK TRANSACTION ImportarPessoa;
                        RETURN @@Error
                    END

                FETCH NEXT FROM cursor_dados_biomedicos INTO @id_etnia,
                    @id_tiposanguineo, @nu_glicemia, @st_glicemia,
                    @st_alergias, @st_deficiencias, @st_necessidadesespeciais
            END

        CLOSE cursor_dados_biomedicos;
        DEALLOCATE cursor_dados_biomedicos;

/*
* Duplicando os dados de documento de identidade
*/


        DECLARE cursor_documento_identidade CURSOR
        FOR
            SELECT  st_rg ,
                    st_orgaoexpeditor ,
                    dt_dataexpedicao
            FROM    dbo.tb_documentoidentidade
            WHERE   id_entidade = @id_entidadeorigem
                    AND id_usuario = @id_usuario;

        OPEN cursor_documento_identidade;
        FETCH NEXT FROM cursor_documento_identidade INTO @nu_rg,
            @st_orgaoexpedidor, @dt_dataexpedicao

        IF ( @@FETCH_STATUS = 0 ) 
            BEGIN
                INSERT  INTO dbo.tb_documentoidentidade
                        ( dt_dataexpedicao ,
                          id_entidade ,
                          id_usuario ,
                          st_rg ,
                          st_orgaoexpeditor 
                        )
                VALUES  ( @dt_dataexpedicao ,
                          @id_entidadedestino ,
                          @id_usuario ,
                          @nu_rg ,
                          @st_orgaoexpedidor 
                        )

                IF @@ERROR <> 0 
                    BEGIN
                        CLOSE cursor_documento_identidade;
                        DEALLOCATE cursor_documento_identidade;
                        ROLLBACK TRANSACTION ImportarPessoa;
                        RETURN @@Error
                    END
            END
        CLOSE cursor_documento_identidade;
        DEALLOCATE cursor_documento_identidade;

/*
* Duplicando os dados de redes sociais
*/



        DECLARE cursor_rede_social CURSOR
        FOR
            SELECT  id_redesocial
            FROM    dbo.tb_contatosredesocialpessoa
            WHERE   id_entidade = @id_entidadeorigem
                    AND id_usuario = @id_usuario;

        OPEN cursor_rede_social;
        FETCH NEXT FROM cursor_rede_social INTO @id_redesocial;

        WHILE ( @@FETCH_STATUS = 0 ) 
            BEGIN
                INSERT  INTO dbo.tb_contatosredesocialpessoa
                        ( id_entidade ,
                          id_redesocial ,
                          id_usuario 
                        )
                VALUES  ( @id_entidadedestino ,
                          @id_redesocial ,
                          @id_usuario 
                        )

                IF ( @@ERROR <> 0 ) 
                    BEGIN
                        CLOSE cursor_rede_social;
                        DEALLOCATE cursor_rede_social;
                        ROLLBACK TRANSACTION ImportarPessoa;
                        RETURN @@Error;
                    END
                FETCH NEXT FROM cursor_rede_social INTO @id_redesocial;
            END
        CLOSE cursor_rede_social;
        DEALLOCATE cursor_rede_social;

/*

*/



        DECLARE cursor_informacao_academica CURSOR
        FOR
            SELECT  id_nivelensino ,
                    st_curso ,
                    st_nomeinstituicao
            FROM    dbo.tb_informacaoacademicapessoa
            WHERE   id_entidade = @id_entidadeorigem
                    AND id_usuario = @id_usuario;

        OPEN cursor_informacao_academica;
        FETCH NEXT FROM cursor_informacao_academica INTO @id_nivelensino,
            @st_curso, @st_nomeinstituicao

        WHILE ( @@FETCH_STATUS = 0 ) 
            BEGIN
                INSERT  INTO dbo.tb_informacaoacademicapessoa
                        ( id_entidade ,
                          id_nivelensino ,
                          id_usuario ,
                          st_curso ,
                          st_nomeinstituicao 
                        )
                VALUES  ( @id_entidadedestino ,
                          @id_nivelensino ,
                          @id_usuario ,
                          @st_curso ,
                          @st_nomeinstituicao 
                        )

                IF ( @@ERROR <> 0 ) 
                    BEGIN
                        CLOSE cursor_informacao_academica;
                        DEALLOCATE cursor_informacao_academica;
                        ROLLBACK TRANSACTION ImportarPessoa;
                        RETURN @@Error;
                    END

                FETCH NEXT FROM cursor_informacao_academica INTO @id_nivelensino,
                    @st_curso, @st_nomeinstituicao
            END
        CLOSE cursor_informacao_academica;
        DEALLOCATE cursor_informacao_academica;

/*

*/



        DECLARE cursor_informacao_profissional CURSOR
        FOR
            SELECT  id_areaatuacao ,
                    st_cargo ,
                    st_organizacao ,
                    dt_inicio ,
                    dt_fim
            FROM    dbo.tb_informacaoprofissionalpessoa
            WHERE   id_entidade = @id_entidadeorigem
                    AND id_usuario = @id_usuario

        OPEN cursor_informacao_profissional;
        FETCH NEXT FROM cursor_informacao_profissional INTO @id_areaatuacao,
            @st_cargo, @st_organizacao, @dt_inicio, @dt_fim


        WHILE ( @@FETCH_STATUS = 0 ) 
            BEGIN

                INSERT  INTO dbo.tb_informacaoprofissionalpessoa
                        ( dt_fim ,
                          dt_inicio ,
                          id_areaatuacao ,
                          id_entidade ,
                          id_usuario ,
                          st_cargo ,
                          st_organizacao 
                        )
                VALUES  ( @dt_fim ,
                          @dt_inicio ,
                          @id_areaatuacao ,
                          @id_entidadedestino ,
                          @id_usuario ,
                          @st_cargo ,
                          @st_organizacao 
                        )

                IF ( @@ERROR <> 0 ) 
                    BEGIN
                        CLOSE cursor_informacao_profissional;
                        DEALLOCATE cursor_informacao_profissional;
                        ROLLBACK TRANSACTION ImportarPessoa;
                        RETURN @@Error;
                    END

                FETCH NEXT FROM cursor_informacao_profissional INTO @id_areaatuacao,
                    @st_cargo, @st_organizacao, @dt_inicio, @dt_fim
            END

        CLOSE cursor_informacao_profissional;
        DEALLOCATE cursor_informacao_profissional;

        COMMIT TRANSACTION ImportarPessoa

        SELECT  u.id_usuario ,
                u.st_cpf ,
                u.st_login ,
                u.st_nomecompleto ,
                u.bl_ativo ,
                p.id_entidade ,
                p.dt_cadastro ,
                p.id_situacao
        FROM    dbo.tb_usuario u
                INNER JOIN dbo.tb_pessoa p ON p.id_usuario = u.id_usuario
        WHERE   u.id_usuario = @id_usuario
                AND p.id_entidade = @id_entidadedestino

        RETURN (0)
    END
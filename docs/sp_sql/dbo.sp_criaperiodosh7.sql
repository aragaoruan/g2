CREATE PROCEDURE [dbo].[sp_criaperiodosh7] @id_horarioaula BIGINT
AS
SET NOCOUNT ON

DECLARE 
	@id_sistema BIGINT, -- id do sistema integrado (Henry7)
	@id_periodo BIGINT -- variavel para guardar o ID do periodo criado (HE15_AT_COD)

SET @id_sistema = 12 -- Id do Henry7 na tabela tb_sistema

INSERT INTO henry7xteste..HE15(HE15_HR_HORINI, HE15_HR_HOREND, HE15_ST_DIAS)
SELECT
	'1900-01-01 ' + SUBSTRING(cast(hr_inicio as varchar), 0, 9) HE15_HR_HORINI,
	'1900-01-01 ' + SUBSTRING(cast(hr_fim as varchar), 0, 9) HE15_HR_HOREND,
	HE15_ST_DIAS=STUFF(
	(
		SELECT '' + (CASE WHEN id_diasemana = 7 THEN 0 ELSE	id_diasemana END) 
		FROM 
			tb_horarioaula h 
		LEFT JOIN
			tb_horariodiasemana d 
		ON
			h.id_horarioaula = d.id_horarioaula
		WHERE
			d.id_horarioaula = A.id_horarioaula
		FOR 
			XML Path('')
	)
	, 1 , 0, '')
FROM
	tb_horarioaula A
WHERE id_horarioaula = @id_horarioaula
GROUP BY A.id_horarioaula, hr_inicio, hr_fim

 -- Guarda o ID do Periodo criado para fazer a inserçao na tabela tb_horarioaulaintegracao
SET @id_periodo = @@IDENTITY

INSERT INTO 
	tb_horarioaulaintegracao(st_codintegrado, id_horarioaula, id_sistema, dt_cadastro)
values
	(@id_periodo, @id_horarioaula, @id_sistema, GETDATE())
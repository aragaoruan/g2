CREATE PROCEDURE [dbo].[sp_tramite] 
        @id_categoriatramite        INT, 
		@id_campo                   VARCHAR(8000), -- id da chave da tabela de relação com trâmite. Exemplo: tb_tramitematricula - id_campo = id_matricula
        @id_tipotramite             INT = NULL
AS
BEGIN
        SET NOCOUNT ON;
        
        DECLARE
        @st_tabela        varchar(50),
        @st_campo        varchar(50),
    @id_entregamaterialatual                varchar(50),
        @sql                varchar(max)
        
        --        Busca a tabela e campo da categoria do trâmite
        SELECT @st_tabela =  st_tabela, @st_campo =  st_campo
        FROM dbo.tb_categoriatramite
        WHERE id_categoriatramite        = @id_categoriatramite

    -- Se o id_categoriatramite for de entrega de material a SP buscará todos os tramites de entrega de materia daquela matrícula pra o item relacionado
    IF(@id_categoriatramite = 2)
    BEGIN
        DECLARE cursor_entregamaterial CURSOR FOR 
            SELECT en.id_entregamaterial FROM tb_entregamaterial en
                JOIN tb_entregamaterial enId ON en.id_matricula = enId.id_matricula AND en.id_itemdematerial = enId.id_itemdematerial
                    WHERE enId.id_entregamaterial = @id_campo;
            OPEN cursor_entregamaterial;
            
            FETCH NEXT FROM cursor_entregamaterial INTO @id_entregamaterialatual;
                WHILE @@fetch_status = 0
                BEGIN
                    SELECT @id_campo = @id_campo + ', ' + @id_entregamaterialatual;
                    FETCH NEXT FROM cursor_entregamaterial INTO @id_entregamaterialatual
                END        
            CLOSE cursor_entregamaterial;
    DEALLOCATE cursor_entregamaterial;
    END
        -- Busca que traz os tramites de acordo com a categoria e o id informado
        
        SET @sql        = 'SELECT tm.id_tramite, tm.id_upload, up.st_upload, tm.st_tramite, tm.id_tipotramite, tt.st_tipotramite, tt.id_categoriatramite, ct.st_categoriatramite, tm.id_usuario, us.st_nomecompleto,tm.id_entidade, tm.dt_cadastro, tm.st_url, tm.bl_visivel, tramiterelacao.'+ @st_campo + ' AS id_campo ';
        SET @sql        = @sql + ' FROM dbo.'+ @st_tabela + ' AS tramiterelacao' ;
        SET @sql        = @sql + ' JOIN dbo.tb_tramite AS tm ON tramiterelacao.id_tramite        = tm.id_tramite';
        SET @sql        = @sql + ' LEFT JOIN dbo.tb_usuario AS US ON us.id_usuario                = tm.id_usuario';
        SET @sql        = @sql + ' LEFT JOIN dbo.tb_upload AS up ON up.id_upload = tm.id_upload';
        SET @sql        = @sql + ' LEFT JOIN dbo.tb_tipotramite as tt ON tt.id_tipotramite                = tm.id_tipotramite';
        SET @sql        = @sql + ' LEFT JOIN dbo.tb_categoriatramite as ct ON ct.id_categoriatramite = tt.id_categoriatramite and ct.id_categoriatramite = '+ cast( @id_categoriatramite as varchar(3) );
        
        SET @sql        = @sql + ' where tramiterelacao.'+ @st_campo +' in ('+ cast( @id_campo as varchar(8000))+')';
        
		IF(@id_tipotramite IS NOT NULL)
		BEGIN
			SET @sql	= @sql + ' AND tm.id_tipotramite = ' + cast( @id_tipotramite as varchar(8000)) ;	
		END
		
        SET @sql		= @sql + ' order by dt_cadastro desc ';
        
        --PRINT @sql
        EXECUTE( @sql )
                
        RETURN (0)
END
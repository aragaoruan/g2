CREATE PROCEDURE [dbo].[sp_matriculadisciplinadireta] ( @id_matricula INT )
AS
    SET NOCOUNT ON ;

    DECLARE @id_projetopedagogico INT ,
        @id_modulo INT ,
        @id_disciplina INT ,
        @bl_obrigatoria BIT



    DELETE  FROM tb_matriculadisciplina
    WHERE   id_matricula = @id_matricula

    DECLARE
    PROJETOPEDAGOGICO
    CURSOR FOR
    SELECT id_projetopedagogico FROM tb_matricula AS m WHERE m.id_matricula = @id_matricula
    OPEN PROJETOPEDAGOGICO
    FETCH NEXT FROM PROJETOPEDAGOGICO INTO @id_projetopedagogico
    WHILE @@FETCH_STATUS = 0
        BEGIN
            DECLARE
            MODULO
            CURSOR FOR
            SELECT id_modulo FROM tb_modulo WHERE id_projetopedagogico = @id_projetopedagogico AND bl_ativo = 1
            OPEN MODULO
            FETCH NEXT FROM MODULO INTO @id_modulo
            WHILE @@FETCH_STATUS = 0
                BEGIN
                    DECLARE
                    DISCIPLINA
                    CURSOR FOR
                    SELECT id_disciplina, bl_obrigatoria FROM tb_modulodisciplina WHERE id_modulo = @id_modulo AND bl_ativo = 1
                    OPEN DISCIPLINA
                    FETCH NEXT FROM DISCIPLINA INTO @id_disciplina, @bl_obrigatoria
                    WHILE @@FETCH_STATUS = 0
                        BEGIN
                            INSERT  INTO tb_matriculadisciplina
                                    ( id_matricula ,
                                      id_evolucao ,
                                      id_disciplina ,
                                      id_situacao ,
                                      bl_obrigatorio,
									  id_matriculaoriginal
                                    )
                            VALUES  ( @id_matricula ,
                                      11 ,
                                      @id_disciplina ,
                                      53 ,
                                      @bl_obrigatoria,
									  @id_matricula
                                    )
                            FETCH NEXT FROM DISCIPLINA INTO @id_disciplina, @bl_obrigatoria
                        END ;
                    CLOSE DISCIPLINA ;
                    DEALLOCATE DISCIPLINA ;
                    FETCH NEXT FROM MODULO INTO @id_modulo
                END ;
            CLOSE MODULO ;
            DEALLOCATE MODULO ;
            FETCH NEXT FROM PROJETOPEDAGOGICO INTO @id_projetopedagogico
        END ;
    CLOSE PROJETOPEDAGOGICO ;
    DEALLOCATE PROJETOPEDAGOGICO ;
--FINAL_PROCEDURE:
    SELECT  *
    FROM    tb_matriculadisciplina
    WHERE   id_matricula = @id_matricula

    RETURN (0)
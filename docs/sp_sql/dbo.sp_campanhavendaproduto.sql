CREATE PROCEDURE [dbo].[sp_campanhavendaproduto](
  @id_tipocampanha INT,
  @id_formapagamento INT,
  @id_entidade INT,
  @id_produto INT = NULL,
  @id_venda INT = NULL,
  @id_campanhaselecionada INT = NULL
)
AS
  IF (@id_tipocampanha = 1)
    BEGIN
      SELECT
        DISTINCT
        *
      FROM [dbo].[fn_campanhacomercialvenda](@id_tipocampanha, @id_formapagamento, @id_entidade, @id_produto, @id_venda)
      GOTO FINAL_PROCEDURE
    END
  IF (@id_tipocampanha = 2)
    BEGIN
      SELECT
        DISTINCT
        *
      FROM [dbo].[fn_campanhacomercialproduto](@id_tipocampanha, @id_formapagamento, @id_entidade, @id_produto,
                                               @id_venda, @id_campanhaselecionada)
      GOTO FINAL_PROCEDURE
    END
  ELSE
    BEGIN
      SELECT
        DISTINCT
        *
      FROM [dbo].[fn_campanhacomercialpontualidade](@id_tipocampanha, @id_formapagamento, @id_entidade, @id_produto,
                                                    @id_venda)
      GOTO FINAL_PROCEDURE
    END
  FINAL_PROCEDURE:
GO

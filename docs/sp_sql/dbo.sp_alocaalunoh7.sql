CREATE PROCEDURE [dbo].[sp_alocaalunoh7] @id_entidade int, @id_usuario int

AS

SET NOCOUNT ON

--VARIAVEIS DO GESTOR
DECLARE @st_codusuario VARCHAR(50)				--codigo do aluno na tabela de integração
DECLARE @st_loginintegrado VARCHAR(50)			--codigo do horario h7 na tabela de integracao

--VARIAVEIS DO HENRY7
DECLARE @HE15_AT_COD int 						--variavel que guarda o ID do horário no H7
DECLARE @HE02_AT_COD int						--variavel que guarda o ID do usuario no H7
DECLARE @HE11_AT_COD int
DECLARE @HE02_NR_COD int

--VARIAVEIS DA SP
DECLARE @CurrentStep  VARCHAR(50)
DECLARE @Return_Message VARCHAR(255)

--DEFINE VALORES PADRÃO
SET @st_codusuario = NULL
SET @st_loginintegrado = NULL
SET @HE15_AT_COD = 0
SET @HE02_AT_COD = 0
SET @HE02_NR_COD = 0
SET @HE11_AT_COD = 0
SET @CurrentStep = 'Inicio da SP'


BEGIN TRANSACTION tx
BEGIN TRY

SELECT @st_codusuario = st_codusuario, @st_loginintegrado = st_loginintegrado from tb_usuariointegracao where id_usuario = @id_usuario and id_entidade = @id_entidade and id_sistema = 12
IF(@st_codusuario > 0) 
BEGIN
	SET @HE11_AT_COD = @st_loginintegrado
	SET @HE02_AT_COD = @st_codusuario
END
ELSE
BEGIN

SET @CurrentStep = 'Primeiro Insert'
insert into henry7xteste.dbo.HE11(HE11_ST_DESCHOR) values ((cast(@id_usuario as varchar) + '-'+cast(@id_entidade as varchar))) -- cria um horário específico para esse usuário no henry7x

SELECT @HE11_AT_COD = SCOPE_IDENTITY()

if(@HE11_AT_COD > 0)
BEGIN

	SET @CurrentStep = 'Segundo Insert'

	-- insere um novo usuário no henry7 
	insert into henry7xteste.dbo.HE02(HE02_ST_MATRICULA, HE02_ST_NOME, HE02_NR_EMPRESA, HE02_NR_HORARIO, HE02_FL_ACESSO) select @id_usuario, st_nomecompleto, @id_entidade, @HE11_AT_COD, 2 from tb_usuario where id_usuario=@id_usuario
	SELECT @HE02_AT_COD = SCOPE_IDENTITY()

	SET @CurrentStep = 'Terceiro Insert'
	insert tb_usuariointegracao	(id_usuario,id_usuariocadastro,id_sistema,id_entidade,st_codusuario,st_loginintegrado) values (@id_usuario,@id_usuario,12,@id_entidade, @HE02_AT_COD, @HE11_AT_COD) -- insere usuário na tabela de integração

END
END

SET @CurrentStep = 'Apagar os Períodos'
delete from henry7xteste.dbo.HE09 where HE09_NR_HORARIO = @HE11_AT_COD


SET @CurrentStep = 'Inserir os Periodos'
insert into henry7xteste.dbo.HE09 (HE09_NR_HORARIO,HE09_NR_PERIODO) 
select DISTINCT @HE11_AT_COD,hi.st_codintegrado from tb_matricula as mt
JOIN tb_matriculadisciplina as md on md.id_matricula = mt.id_matricula
JOIN tb_alocacao as al on al.id_matriculadisciplina = md.id_matriculadisciplina and al.bl_ativo = 1
JOIN vw_salahorariolocal as sal on sal.id_saladeaula = al.id_saladeaula
JOIN tb_saladeaula as sda on sda.id_saladeaula = sal.id_saladeaula
JOIN tb_horarioaulaintegracao as hi on hi.id_horarioaula = sal.id_horarioaula where mt.id_entidadeatendimento = @id_entidade and mt.id_usuario = @id_usuario
AND sda.dt_encerramento >= cast(GETDATE() as date)-- somente os cursos cuja data de encerramento ainda não passou.


IF @@TRANCOUNT > 0
  BEGIN --SUCCESS, nothing failed, now I can commit!!
    SELECT @Return_Message = 'SUCCESSO - Aluno Alocado'
    COMMIT TRANSACTION tx;    -- now everything is committed
  END
END TRY
BEGIN CATCH
  IF @@TRANCOUNT > 0 --something failed
  BEGIN
    IF @CurrentStep = 'Primeiro Insert'
      SELECT @Return_Message = 'FALHOU - insert do horário falhou'
    ELSE IF @CurrentStep = 'Segundo Insert'
      SELECT @Return_Message = 'FALHOU - insert do aluno falhou'
	ELSE IF @CurrentStep = 'Terceiro Insert'
      SELECT @Return_Message = 'FALHOU - insert do aluno integracao falhou'

    RAISERROR (@Return_Message, 10, 1)

    ROLLBACK TRAN tx; -- everything is rolled back
  END
END CATCH

Select @Return_Message
ALTER PROCEDURE [dbo].[sp_matricula_turma]
  @id_entidade INT = NULL,
  @id_turma INT = NULL,
  @dt_datainformada DATE,
  @id_evolucao INT = NULL,
  @id_entidadepai INT
AS
DECLARE
@id_entidade_ INT,
@id_turma_ INT,
@dt_datainformada_ DATE,
@id_evolucao_ INT,
@id_entidadepai_ INT

SET @id_entidade_ = @id_entidade
SET @id_turma_ = @id_turma
SET @dt_datainformada_ = @dt_datainformada
SET @id_evolucao_ = @id_evolucao
SET @id_entidadepai_ = @id_entidadepai

BEGIN
  SELECT
    DISTINCT
    p.id_produto,
    p.st_produto,
    e.id_entidade,
    e.id_entidadecadastro,
    e.st_nomeentidade,
    pp.id_projetopedagogico,
    pp.st_projetopedagogico,
    ppp.nu_cargahoraria,
    t.id_turma,
    t.st_turma,
    ac.id_areaconhecimento,
    ac.st_areaconhecimento,
    CONVERT(VARCHAR, t.dt_inicio, 103) AS dt_inicio,
    ev.id_evolucao,
    st_evolucao,
    (qtd6.qtd) AS nu_dia6,
    (qtd5.qtd) AS nu_dia5,
    (qtd4.qtd) AS nu_dia4,
    (qtd3.qtd) AS nu_dia3,
    (qtd2.qtd) AS nu_dia2,
    (qtd.qtd) AS nu_dia1,
    (fp.free_pass) AS qtd_FP,
    (fp2.free_pass) AS qtd_FP2,
    (fp3.free_pass) AS qtd_FP3,
    (fp4.free_pass) AS qtd_FP4,
    (fp5.free_pass) AS qtd_FP5,
    (fp6.free_pass) AS qtd_FP6,
    (gd.garantia_duo) AS qtd_GD,
    (gd2.garantia_duo) AS qtd_GD2,
    (gd3.garantia_duo) AS qtd_GD3,
    (gd4.garantia_duo) AS qtd_GD4,
    (gd5.garantia_duo) AS qtd_GD5,
    (gd6.garantia_duo) AS qtd_GD6,
    CONVERT(VARCHAR, @dt_datainformada_, 103) AS dt_atual
  FROM
    tb_turma AS t
    JOIN tb_turmaentidade AS tme ON tme.id_turma = t.id_turma
    JOIN tb_entidade AS e ON e.id_entidade = tme.id_entidade
    JOIN dbo.vw_entidaderecursivaid AS eri ON eri.id_entidade = e.id_entidade OR eri.nu_entidadepai = e.id_entidade
    LEFT JOIN tb_vendaproduto AS vp ON vp.id_turma = t.id_turma
    LEFT JOIN tb_produto AS p ON p.id_produto = vp.id_produto AND p.bl_ativo = 1
    LEFT JOIN dbo.tb_produtoprojetopedagogico AS ppp ON ppp.id_produto = p.id_produto
    LEFT JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = ppp.id_projetopedagogico
    LEFT JOIN dbo.tb_evolucao AS ev ON ev.id_evolucao = t.id_evolucao
    LEFT JOIN dbo.tb_produtoarea AS pa ON pa.id_produto = p.id_produto
    LEFT JOIN dbo.tb_areaconhecimento AS ac ON ac.id_areaconhecimento = pa.id_areaconhecimento
    CROSS APPLY (
                  SELECT
                    count(*) AS qtd
                  FROM tb_venda AS vnd
                    INNER JOIN tb_contrato AS ctr1 ON vnd.id_venda = ctr1.id_venda
                    INNER JOIN tb_contratoregra AS ctrr1 ON ctr1.id_contratoregra = ctrr1.id_contratoregra
                    INNER JOIN tb_vendaproduto AS vprd1 ON vnd.id_venda = vprd1.id_venda
                    INNER JOIN tb_matricula AS mtr1 ON mtr1.id_vendaproduto = vprd1.id_vendaproduto
                    LEFT JOIN tb_cancelamento AS ca ON ca.id_cancelamento = mtr1.id_cancelamento
                    INNER JOIN tb_turma AS tu ON tu.id_turma = mtr1.id_turma
                  WHERE mtr1.id_evolucao IN (45, 46, 6)
                        AND vprd1.id_produto = p.id_produto
                        AND cast(tu.dt_inicio AS DATE) IS NOT NULL
                        AND cast(vnd.dt_confirmacao AS DATE) <= DATEADD(D, 0, CAST(@dt_datainformada_ AS VARCHAR))
                        AND (mtr1.id_cancelamento IS NULL)
                ) AS qtd
    CROSS APPLY (
                  SELECT
                    count(*) AS qtd
                  FROM tb_venda AS vnd
                    INNER JOIN tb_contrato AS ctr1 ON vnd.id_venda = ctr1.id_venda
                    INNER JOIN tb_contratoregra AS ctrr1 ON ctr1.id_contratoregra = ctrr1.id_contratoregra
                    INNER JOIN tb_vendaproduto AS vprd1 ON vnd.id_venda = vprd1.id_venda
                    INNER JOIN tb_matricula AS mtr1 ON mtr1.id_vendaproduto = vprd1.id_vendaproduto
                    LEFT JOIN tb_cancelamento AS ca ON ca.id_cancelamento = mtr1.id_cancelamento
                    INNER JOIN tb_turma AS tu ON tu.id_turma = mtr1.id_turma
                  WHERE mtr1.id_evolucao IN (45, 46, 6)
                        AND vprd1.id_produto = p.id_produto
                        AND cast(tu.dt_inicio AS DATE) IS NOT NULL
                        AND cast(vnd.dt_confirmacao AS DATE) <= DATEADD(D, -1, CAST(@dt_datainformada_ AS VARCHAR))
                        AND (mtr1.id_cancelamento IS NULL)
                ) AS qtd2
    CROSS APPLY (
                  SELECT
                    count(*) AS qtd
                  FROM tb_venda AS vnd
                    INNER JOIN tb_contrato AS ctr1 ON vnd.id_venda = ctr1.id_venda
                    INNER JOIN tb_contratoregra AS ctrr1 ON ctr1.id_contratoregra = ctrr1.id_contratoregra
                    INNER JOIN tb_vendaproduto AS vprd1 ON vnd.id_venda = vprd1.id_venda
                    INNER JOIN tb_matricula AS mtr1 ON mtr1.id_vendaproduto = vprd1.id_vendaproduto
                    LEFT JOIN tb_cancelamento AS ca ON ca.id_cancelamento = mtr1.id_cancelamento
                    INNER JOIN tb_turma AS tu ON tu.id_turma = mtr1.id_turma
                  WHERE mtr1.id_evolucao IN (45, 46, 6)
                        AND vprd1.id_produto = p.id_produto
                        AND cast(tu.dt_inicio AS DATE) IS NOT NULL
                        AND cast(vnd.dt_confirmacao AS DATE) <= DATEADD(D, -2, CAST(@dt_datainformada_ AS VARCHAR))
                        AND (mtr1.id_cancelamento IS NULL)
                ) AS qtd3
    CROSS APPLY (
                  SELECT
                    count(*) AS qtd
                  FROM tb_venda AS vnd
                    INNER JOIN tb_contrato AS ctr1 ON vnd.id_venda = ctr1.id_venda
                    INNER JOIN tb_contratoregra AS ctrr1 ON ctr1.id_contratoregra = ctrr1.id_contratoregra
                    INNER JOIN tb_vendaproduto AS vprd1 ON vnd.id_venda = vprd1.id_venda
                    INNER JOIN tb_matricula AS mtr1 ON mtr1.id_vendaproduto = vprd1.id_vendaproduto
                    LEFT JOIN tb_cancelamento AS ca ON ca.id_cancelamento = mtr1.id_cancelamento
                    INNER JOIN tb_turma AS tu ON tu.id_turma = mtr1.id_turma
                  WHERE mtr1.id_evolucao IN (45, 46, 6)
                        AND vprd1.id_produto = p.id_produto
                        AND cast(tu.dt_inicio AS DATE) IS NOT NULL
                        AND cast(vnd.dt_confirmacao AS DATE) <= DATEADD(D, -3, CAST(@dt_datainformada_ AS VARCHAR))
                        AND (mtr1.id_cancelamento IS NULL)
                ) AS qtd4
    CROSS APPLY (
                  SELECT
                    count(*) AS qtd
                  FROM tb_venda AS vnd
                    INNER JOIN tb_contrato AS ctr1 ON vnd.id_venda = ctr1.id_venda
                    INNER JOIN tb_contratoregra AS ctrr1 ON ctr1.id_contratoregra = ctrr1.id_contratoregra
                    INNER JOIN tb_vendaproduto AS vprd1 ON vnd.id_venda = vprd1.id_venda
                    INNER JOIN tb_matricula AS mtr1 ON mtr1.id_vendaproduto = vprd1.id_vendaproduto
                    LEFT JOIN tb_cancelamento AS ca ON ca.id_cancelamento = mtr1.id_cancelamento
                    INNER JOIN tb_turma AS tu ON tu.id_turma = mtr1.id_turma
                  WHERE mtr1.id_evolucao IN (45, 46, 6)
                        AND vprd1.id_produto = p.id_produto
                        AND cast(tu.dt_inicio AS DATE) IS NOT NULL
                        AND cast(vnd.dt_confirmacao AS DATE) <= DATEADD(D, -4, CAST(@dt_datainformada_ AS VARCHAR))
                        AND (mtr1.id_cancelamento IS NULL)
                ) AS qtd5
    CROSS APPLY (
                  SELECT
                    count(*) AS qtd
                  FROM tb_venda AS vnd
                    INNER JOIN tb_contrato AS ctr1 ON vnd.id_venda = ctr1.id_venda
                    INNER JOIN tb_contratoregra AS ctrr1 ON ctr1.id_contratoregra = ctrr1.id_contratoregra
                    INNER JOIN tb_vendaproduto AS vprd1 ON vnd.id_venda = vprd1.id_venda
                    INNER JOIN tb_matricula AS mtr1 ON mtr1.id_vendaproduto = vprd1.id_vendaproduto
                    LEFT JOIN tb_cancelamento AS ca ON ca.id_cancelamento = mtr1.id_cancelamento
                    INNER JOIN tb_turma AS tu ON tu.id_turma = mtr1.id_turma
                  WHERE mtr1.id_evolucao IN (45, 46, 6)
                        AND vprd1.id_produto = p.id_produto
                        AND cast(tu.dt_inicio AS DATE) IS NOT NULL
                        AND cast(vnd.dt_confirmacao AS DATE) <= DATEADD(D, -5, CAST(@dt_datainformada_ AS VARCHAR))
                        AND (mtr1.id_cancelamento IS NULL)
                ) AS qtd6
    CROSS APPLY (
                  SELECT
                    count(*) AS free_pass
                  FROM tb_venda AS fpvd1
                    INNER JOIN tb_contrato AS fpctr1 ON fpvd1.id_venda = fpctr1.id_venda
                    INNER JOIN tb_contratoregra AS fpctrr1 ON fpctr1.id_contratoregra = fpctrr1.id_contratoregra
                    INNER JOIN tb_vendaproduto AS fpvprd1 ON fpvd1.id_venda = fpvprd1.id_venda
                    INNER JOIN tb_matricula AS fpmtr1 ON fpmtr1.id_vendaproduto = fpvprd1.id_vendaproduto
                    LEFT JOIN tb_cancelamento AS ca ON ca.id_cancelamento = fpmtr1.id_cancelamento
                    INNER JOIN tb_turma AS tu ON tu.id_turma = fpmtr1.id_turma
                  WHERE fpctrr1.st_contratoregra LIKE '%free%' AND fpmtr1.id_evolucao IN (45, 46, 6)
                        AND fpvprd1.id_produto = vp.id_produto
                        AND cast(fpvd1.dt_confirmacao AS DATE) = CAST(@dt_datainformada_ AS VARCHAR)
                        AND (fpmtr1.id_cancelamento IS NULL OR ca.dt_solicitacao > CAST(@dt_datainformada_ AS VARCHAR))
                ) AS fp
    CROSS APPLY (
                  SELECT
                    count(*) AS free_pass
                  FROM tb_venda AS fpvd1
                    INNER JOIN tb_contrato AS fpctr1 ON fpvd1.id_venda = fpctr1.id_venda
                    INNER JOIN tb_contratoregra AS fpctrr1 ON fpctr1.id_contratoregra = fpctrr1.id_contratoregra
                    INNER JOIN tb_vendaproduto AS fpvprd1 ON fpvd1.id_venda = fpvprd1.id_venda
                    INNER JOIN tb_matricula AS fpmtr1 ON fpmtr1.id_vendaproduto = fpvprd1.id_vendaproduto
                    LEFT JOIN tb_cancelamento AS ca ON ca.id_cancelamento = fpmtr1.id_cancelamento
                    INNER JOIN tb_turma AS tu ON tu.id_turma = fpmtr1.id_turma
                  WHERE fpctrr1.st_contratoregra LIKE '%free%' AND fpmtr1.id_evolucao IN (45, 46, 6)
                        AND fpvprd1.id_produto = vp.id_produto
                        AND cast(fpvd1.dt_confirmacao AS DATE) = DATEADD(D, -1, CAST(@dt_datainformada_ AS VARCHAR))
                        AND (fpmtr1.id_cancelamento IS NULL OR ca.dt_solicitacao > CAST(@dt_datainformada_ AS VARCHAR))
                ) AS fp2
    CROSS APPLY (
                  SELECT
                    count(*) AS free_pass
                  FROM tb_venda AS fpvd1
                    INNER JOIN tb_contrato AS fpctr1 ON fpvd1.id_venda = fpctr1.id_venda
                    INNER JOIN tb_contratoregra AS fpctrr1 ON fpctr1.id_contratoregra = fpctrr1.id_contratoregra
                    INNER JOIN tb_vendaproduto AS fpvprd1 ON fpvd1.id_venda = fpvprd1.id_venda
                    INNER JOIN tb_matricula AS fpmtr1 ON fpmtr1.id_vendaproduto = fpvprd1.id_vendaproduto
                    LEFT JOIN tb_cancelamento AS ca ON ca.id_cancelamento = fpmtr1.id_cancelamento
                    INNER JOIN tb_turma AS tu ON tu.id_turma = fpmtr1.id_turma
                  WHERE fpctrr1.st_contratoregra LIKE '%free%' AND fpmtr1.id_evolucao IN (45, 46, 6)
                        AND fpvprd1.id_produto = vp.id_produto
                        AND cast(fpvd1.dt_confirmacao AS DATE) = DATEADD(D, -2, CAST(@dt_datainformada_ AS VARCHAR))
                        AND (fpmtr1.id_cancelamento IS NULL OR ca.dt_solicitacao > CAST(@dt_datainformada_ AS VARCHAR))
                ) AS fp3
    CROSS APPLY (
                  SELECT
                    count(*) AS free_pass
                  FROM tb_venda AS fpvd1
                    INNER JOIN tb_contrato AS fpctr1 ON fpvd1.id_venda = fpctr1.id_venda
                    INNER JOIN tb_contratoregra AS fpctrr1 ON fpctr1.id_contratoregra = fpctrr1.id_contratoregra
                    INNER JOIN tb_vendaproduto AS fpvprd1 ON fpvd1.id_venda = fpvprd1.id_venda
                    INNER JOIN tb_matricula AS fpmtr1 ON fpmtr1.id_vendaproduto = fpvprd1.id_vendaproduto
                    LEFT JOIN tb_cancelamento AS ca ON ca.id_cancelamento = fpmtr1.id_cancelamento
                    INNER JOIN tb_turma AS tu ON tu.id_turma = fpmtr1.id_turma
                  WHERE fpctrr1.st_contratoregra LIKE '%free%' AND fpmtr1.id_evolucao IN (45, 46, 6)
                        AND fpvprd1.id_produto = vp.id_produto
                        AND cast(fpvd1.dt_confirmacao AS DATE) = DATEADD(D, -3, CAST(@dt_datainformada_ AS VARCHAR))
                        AND (fpmtr1.id_cancelamento IS NULL OR ca.dt_solicitacao > CAST(@dt_datainformada_ AS VARCHAR))
                ) AS fp4
    CROSS APPLY (
                  SELECT
                    count(*) AS free_pass
                  FROM tb_venda AS fpvd1
                    INNER JOIN tb_contrato AS fpctr1 ON fpvd1.id_venda = fpctr1.id_venda
                    INNER JOIN tb_contratoregra AS fpctrr1 ON fpctr1.id_contratoregra = fpctrr1.id_contratoregra
                    INNER JOIN tb_vendaproduto AS fpvprd1 ON fpvd1.id_venda = fpvprd1.id_venda
                    INNER JOIN tb_matricula AS fpmtr1 ON fpmtr1.id_vendaproduto = fpvprd1.id_vendaproduto
                    LEFT JOIN tb_cancelamento AS ca ON ca.id_cancelamento = fpmtr1.id_cancelamento
                    INNER JOIN tb_turma AS tu ON tu.id_turma = fpmtr1.id_turma
                  WHERE fpctrr1.st_contratoregra LIKE '%free%' AND fpmtr1.id_evolucao IN (45, 46, 6)
                        AND fpvprd1.id_produto = vp.id_produto
                        AND cast(fpvd1.dt_confirmacao AS DATE) = DATEADD(D, -4, CAST(@dt_datainformada_ AS VARCHAR))
                        AND (fpmtr1.id_cancelamento IS NULL OR ca.dt_solicitacao > CAST(@dt_datainformada_ AS VARCHAR))
                ) AS fp5
    CROSS APPLY (
                  SELECT
                    count(*) AS free_pass
                  FROM tb_venda AS fpvd1
                    INNER JOIN tb_contrato AS fpctr1 ON fpvd1.id_venda = fpctr1.id_venda
                    INNER JOIN tb_contratoregra AS fpctrr1 ON fpctr1.id_contratoregra = fpctrr1.id_contratoregra
                    INNER JOIN tb_vendaproduto AS fpvprd1 ON fpvd1.id_venda = fpvprd1.id_venda
                    INNER JOIN tb_matricula AS fpmtr1 ON fpmtr1.id_vendaproduto = fpvprd1.id_vendaproduto
                    LEFT JOIN tb_cancelamento AS ca ON ca.id_cancelamento = fpmtr1.id_cancelamento
                    INNER JOIN tb_turma AS tu ON tu.id_turma = fpmtr1.id_turma
                  WHERE fpctrr1.st_contratoregra LIKE '%free%' AND fpmtr1.id_evolucao IN (45, 46, 6)
                        AND fpvprd1.id_produto = vp.id_produto
                        AND cast(fpvd1.dt_confirmacao AS DATE) = DATEADD(D, -5, CAST(@dt_datainformada_ AS VARCHAR))
                        AND (fpmtr1.id_cancelamento IS NULL OR ca.dt_solicitacao > CAST(@dt_datainformada_ AS VARCHAR))
                ) AS fp6
    CROSS APPLY (
                  SELECT
                    count(*) AS garantia_duo
                  FROM tb_venda AS fpvd1
                    INNER JOIN tb_contrato AS fpctr1 ON fpvd1.id_venda = fpctr1.id_venda
                    INNER JOIN tb_contratoregra AS fpctrr1 ON fpctr1.id_contratoregra = fpctrr1.id_contratoregra
                    INNER JOIN tb_vendaproduto AS fpvprd1 ON fpvd1.id_venda = fpvprd1.id_venda
                    INNER JOIN tb_matricula AS fpmtr1 ON fpmtr1.id_vendaproduto = fpvprd1.id_vendaproduto
                    LEFT JOIN tb_cancelamento AS ca ON ca.id_cancelamento = fpmtr1.id_cancelamento
                    INNER JOIN tb_turma AS tu ON tu.id_turma = fpmtr1.id_turma
                  WHERE fpctrr1.st_contratoregra LIKE '%duo%' AND fpmtr1.id_evolucao IN (45, 46, 6)
                        AND fpvprd1.id_produto = vp.id_produto
                        AND cast(fpvd1.dt_confirmacao AS DATE) = DATEADD(D, 0, CAST(@dt_datainformada_ AS VARCHAR))
                        AND (fpmtr1.id_cancelamento IS NULL OR ca.dt_solicitacao > CAST(@dt_datainformada_ AS VARCHAR))
                ) AS gd
    CROSS APPLY (
                  SELECT
                    count(*) AS garantia_duo
                  FROM tb_venda AS fpvd1
                    INNER JOIN tb_contrato AS fpctr1 ON fpvd1.id_venda = fpctr1.id_venda
                    INNER JOIN tb_contratoregra AS fpctrr1 ON fpctr1.id_contratoregra = fpctrr1.id_contratoregra
                    INNER JOIN tb_vendaproduto AS fpvprd1 ON fpvd1.id_venda = fpvprd1.id_venda
                    INNER JOIN tb_matricula AS fpmtr1 ON fpmtr1.id_vendaproduto = fpvprd1.id_vendaproduto
                    LEFT JOIN tb_cancelamento AS ca ON ca.id_cancelamento = fpmtr1.id_cancelamento
                    INNER JOIN tb_turma AS tu ON tu.id_turma = fpmtr1.id_turma
                  WHERE fpctrr1.st_contratoregra LIKE '%duo%' AND fpmtr1.id_evolucao IN (45, 46, 6)
                        AND fpvprd1.id_produto = vp.id_produto
                        AND cast(fpvd1.dt_confirmacao AS DATE) = DATEADD(D, -1, CAST(@dt_datainformada_ AS VARCHAR))
                        AND (fpmtr1.id_cancelamento IS NULL OR ca.dt_solicitacao > CAST(@dt_datainformada_ AS VARCHAR))
                ) AS gd2
    CROSS APPLY (
                  SELECT
                    count(*) AS garantia_duo
                  FROM tb_venda AS fpvd1
                    INNER JOIN tb_contrato AS fpctr1 ON fpvd1.id_venda = fpctr1.id_venda
                    INNER JOIN tb_contratoregra AS fpctrr1 ON fpctr1.id_contratoregra = fpctrr1.id_contratoregra
                    INNER JOIN tb_vendaproduto AS fpvprd1 ON fpvd1.id_venda = fpvprd1.id_venda
                    INNER JOIN tb_matricula AS fpmtr1 ON fpmtr1.id_vendaproduto = fpvprd1.id_vendaproduto
                    LEFT JOIN tb_cancelamento AS ca ON ca.id_cancelamento = fpmtr1.id_cancelamento
                    INNER JOIN tb_turma AS tu ON tu.id_turma = fpmtr1.id_turma
                  WHERE fpctrr1.st_contratoregra LIKE '%duo%' AND fpmtr1.id_evolucao IN (45, 46, 6)
                        AND fpvprd1.id_produto = vp.id_produto
                        AND cast(fpvd1.dt_confirmacao AS DATE) = DATEADD(D, -2, CAST(@dt_datainformada_ AS VARCHAR))
                        AND (fpmtr1.id_cancelamento IS NULL OR ca.dt_solicitacao > CAST(@dt_datainformada_ AS VARCHAR))
                ) AS gd3
    CROSS APPLY (
                  SELECT
                    count(*) AS garantia_duo
                  FROM tb_venda AS fpvd1
                    INNER JOIN tb_contrato AS fpctr1 ON fpvd1.id_venda = fpctr1.id_venda
                    INNER JOIN tb_contratoregra AS fpctrr1 ON fpctr1.id_contratoregra = fpctrr1.id_contratoregra
                    INNER JOIN tb_vendaproduto AS fpvprd1 ON fpvd1.id_venda = fpvprd1.id_venda
                    INNER JOIN tb_matricula AS fpmtr1 ON fpmtr1.id_vendaproduto = fpvprd1.id_vendaproduto
                    LEFT JOIN tb_cancelamento AS ca ON ca.id_cancelamento = fpmtr1.id_cancelamento
                    INNER JOIN tb_turma AS tu ON tu.id_turma = fpmtr1.id_turma
                  WHERE fpctrr1.st_contratoregra LIKE '%duo%' AND fpmtr1.id_evolucao IN (45, 46, 6)
                        AND fpvprd1.id_produto = vp.id_produto
                        AND cast(fpvd1.dt_confirmacao AS DATE) = DATEADD(D, -3, CAST(@dt_datainformada_ AS VARCHAR))
                        AND (fpmtr1.id_cancelamento IS NULL OR ca.dt_solicitacao > CAST(@dt_datainformada_ AS VARCHAR))
                ) AS gd4
    CROSS APPLY (
                  SELECT
                    count(*) AS garantia_duo
                  FROM tb_venda AS fpvd1
                    INNER JOIN tb_contrato AS fpctr1 ON fpvd1.id_venda = fpctr1.id_venda
                    INNER JOIN tb_contratoregra AS fpctrr1 ON fpctr1.id_contratoregra = fpctrr1.id_contratoregra
                    INNER JOIN tb_vendaproduto AS fpvprd1 ON fpvd1.id_venda = fpvprd1.id_venda
                    INNER JOIN tb_matricula AS fpmtr1 ON fpmtr1.id_vendaproduto = fpvprd1.id_vendaproduto
                    LEFT JOIN tb_cancelamento AS ca ON ca.id_cancelamento = fpmtr1.id_cancelamento
                    INNER JOIN tb_turma AS tu ON tu.id_turma = fpmtr1.id_turma
                  WHERE fpctrr1.st_contratoregra LIKE '%duo%' AND fpmtr1.id_evolucao IN (45, 46, 6)
                        AND fpvprd1.id_produto = vp.id_produto
                        AND cast(fpvd1.dt_confirmacao AS DATE) = DATEADD(D, -4, CAST(@dt_datainformada_ AS VARCHAR))
                        AND (fpmtr1.id_cancelamento IS NULL OR ca.dt_solicitacao > CAST(@dt_datainformada_ AS VARCHAR))
                ) AS gd5
    CROSS APPLY (
                  SELECT
                    count(*) AS garantia_duo
                  FROM tb_venda AS fpvd1
                    INNER JOIN tb_contrato AS fpctr1 ON fpvd1.id_venda = fpctr1.id_venda
                    INNER JOIN tb_contratoregra AS fpctrr1 ON fpctr1.id_contratoregra = fpctrr1.id_contratoregra
                    INNER JOIN tb_vendaproduto AS fpvprd1 ON fpvd1.id_venda = fpvprd1.id_venda
                    INNER JOIN tb_matricula AS fpmtr1 ON fpmtr1.id_vendaproduto = fpvprd1.id_vendaproduto
                    LEFT JOIN tb_cancelamento AS ca ON ca.id_cancelamento = fpmtr1.id_cancelamento
                    INNER JOIN tb_turma AS tu ON tu.id_turma = fpmtr1.id_turma
                  WHERE fpctrr1.st_contratoregra LIKE '%duo%' AND fpmtr1.id_evolucao IN (45, 46, 6)
                        AND fpvprd1.id_produto = vp.id_produto
                        AND cast(fpvd1.dt_confirmacao AS DATE) = DATEADD(D, -5, CAST(@dt_datainformada_ AS VARCHAR))
                        AND (fpmtr1.id_cancelamento IS NULL OR ca.dt_solicitacao > CAST(@dt_datainformada_ AS VARCHAR))
                ) AS gd6
  WHERE eri.nu_entidadepai = CAST(@id_entidadepai_ AS VARCHAR)
        AND t.dt_inicio >= CAST(@dt_datainformada_ AS VARCHAR)
        AND e.id_entidade = CASE
                            WHEN (@id_entidade_ IS NOT NULL)
                              THEN CAST(@id_entidade_ AS VARCHAR)
                            ELSE e.id_entidade
                            END
        AND t.id_turma = CASE
                         WHEN (@id_turma_ IS NOT NULL)
                           THEN CAST(@id_turma_ AS VARCHAR)
                         ELSE t.id_turma
                         END
        AND t.id_evolucao = CASE
                            WHEN (@id_evolucao_ IS NOT NULL)
                              THEN CAST(@id_evolucao_ AS VARCHAR)
                            ELSE t.id_evolucao
                            END
  ORDER BY
    ac.st_areaconhecimento ASC,
    e.st_nomeentidade ASC

END





GO



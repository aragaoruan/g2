DROP PROCEDURE dbo.sp_email_alunos_nao_enviados_pontosoft;
GO;

ALTER PROCEDURE dbo.sp_email_alunos_nao_enviados_pontosoft
AS

  EXEC msdb.dbo.sp_send_dbmail
      @profile_name = ''SQL Server Mail'',
      @recipients =''ludmilla.flores@unyleya.com.br'',
      @query = ''execute as login = ''''elton.gomes'''' 
	  SET NOCOUNT ON
		select st_nomeentidade as entidade,
		st_nomecompleto as nome,
		id_usuario as "id do usuario",
		id_matricula as "id da matricula",
		dt_inicioturma as "data da matricula",
		st_turma as turma
		from vw_matriculaintegracao_pontosoft'',
  @body_format = ''HTML'',
  @query_attachment_filename = ''Relatorio_alunos_nao_enviados_pontosoft.csv'', 
  @subject = ''Alunos não enviados enviados para o banco do pontosoft'',
  @attach_query_result_as_file = 1,
  @query_result_header = 1,
  @query_result_width = 32767,
  @query_result_separator = ''	'',
  @exclude_query_output = 0,
  @append_query_error = 1,
  @query_result_no_padding =1;
GO;

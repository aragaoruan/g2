/****** Object:  Trigger [dbo].[Validacao_login_senha_INSERIR]    Script Date: 02/06/2013 10:51:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- select top 90 * from tb_pessoaendereco
-- sp_help tb_pessoaendereco
CREATE TRIGGER [dbo].[controle_endereco_padrao]
ON [dbo].[tb_pessoaendereco]
AFTER INSERT, UPDATE
AS
	BEGIN
	--IF TRIGGER_NESTLEVEL() > 1
	--	RETURN
	--Declar���o das variaveis
	DECLARE 
		  @id_usuario		INT
		, @id_entidade		INT
		, @id_endereco		INT
		, @bl_padrao	    BIT
		
		
	-- Recuperando os dados Inseridos ou Atualizados
		SELECT	  @id_usuario	= id_usuario
				, @id_entidade	= id_entidade
				, @id_endereco	= id_endereco
				, @bl_padrao	= bl_padrao 
		FROM INSERTED 
		
	-- Verificando se para este usuario nessa entidade, j� temos endere�o padr�o
	IF(@bl_padrao = 1)
	BEGIN
	
	UPDATE tb_pessoaendereco SET bl_padrao = 0 WHERE id_usuario = @id_usuario and id_entidade = @id_entidade and bl_padrao = 1 and id_endereco != @id_endereco
	
	END
END

GO

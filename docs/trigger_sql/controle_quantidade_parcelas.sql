/****** Object:  Trigger [dbo].[Validacao_login_senha_INSERIR]    Script Date: 02/06/2013 10:51:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/**
-- ELCIO MAURO GUIMAR�ES
DUAS TRIGGERS CRIADAS PARA CONTROLAR A QUANTIDADE DE PARCELAS DE UMA VENDA.
ALIMENTAM O CAMPO NU_PARCELAS DA TABELA TB_VENDA

**/

ALTER TRIGGER [dbo].[controle_quantidade_parcelas]
ON [dbo].[tb_lancamentovenda]
AFTER INSERT, UPDATE 
AS
BEGIN

SET NOCOUNT ON

	UPDATE tb_venda 
		SET nu_parcelas = (select count(*) 
								from tb_lancamento l join tb_lancamentovenda lv on (l.id_lancamento = lv.id_lancamento)
								where l.bl_ativo = 1 and lv.id_venda in (select DISTINCT id_venda from INSERTED)) 
		WHERE id_venda in (select DISTINCT id_venda from INSERTED)

END

GO


ALTER TRIGGER [dbo].[controle_quantidade_parcelas_apagadas]
ON [dbo].[tb_lancamentovenda]
AFTER DELETE 
AS
BEGIN

SET NOCOUNT ON

	UPDATE tb_venda 
		SET nu_parcelas = (select count(*) 
								from tb_lancamento l join tb_lancamentovenda lv on (l.id_lancamento = lv.id_lancamento)
								where l.bl_ativo = 1 and lv.id_venda in (select DISTINCT id_venda from DELETED)) 
		WHERE id_venda in (select DISTINCT id_venda from DELETED)

END


GO
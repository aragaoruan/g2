USE [G2H_UNY]
GO

/****** Object:  Trigger [dbo].[trg_valida_insert_update_titular_projeto]    Script Date: 02/05/2016 17:02:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER TRIGGER [dbo].[trg_valida_insert_update_titular_projeto] ON [dbo].[tb_usuarioperfilentidadereferencia]
INSTEAD OF INSERT, UPDATE
AS BEGIN
SET NOCOUNT ON

    declare
        @id_perfil INT, @id_projetopedagogico INT, @bl_titular BIT, @id_usuario INT, @id_perfilreferencia INT,
        @id_entidade INT, @id_areaconhecimento INT, @id_saladeaula INT, @id_disciplina INT, @bl_ativo BIT,
        @id_livro INT, @bl_autor BIT, @nu_porcentagem DECIMAL, @dt_inicio DATE, @dt_fim DATE, @id_titulacao INT,
        @bl_desativarmoodle BIT

    SELECT
            @id_perfil = INSERTED.id_perfil,
            @id_projetopedagogico = INSERTED.id_projetopedagogico,
            @bl_titular = INSERTED.bl_titular,
            @id_usuario = INSERTED.id_usuario,
            @id_perfilreferencia = INSERTED.id_perfilreferencia,
            @id_entidade = INSERTED.id_entidade,
            @id_areaconhecimento = INSERTED.id_areaconhecimento,
            @id_saladeaula = INSERTED.id_saladeaula,
            @id_disciplina = INSERTED.id_disciplina,
            @bl_ativo = INSERTED.bl_ativo,
            @id_livro = INSERTED.id_livro,
            @bl_autor = INSERTED.bl_autor,
            @nu_porcentagem = INSERTED.nu_porcentagem,
            @dt_inicio = INSERTED.dt_inicio,
            @dt_fim = INSERTED.dt_fim,
            @id_titulacao = INSERTED.id_titulacao,
            @bl_desativarmoodle = INSERTED.bl_desativarmoodle
    from INSERTED

    IF @bl_titular = 1
        BEGIN
            UPDATE tb_usuarioperfilentidadereferencia SET
                bl_titular = 0
                WHERE id_perfil = @id_perfil
                and id_projetopedagogico = @id_projetopedagogico
                and bl_titular = 1
				and id_disciplina is null
                and id_usuario <> @id_usuario;
        END

    IF @id_perfilreferencia <> ''
        BEGIN
        UPDATE tb_usuarioperfilentidadereferencia SET
                id_usuario = @id_usuario, id_entidade = @id_entidade, id_perfil = @id_perfil, id_areaconhecimento = @id_areaconhecimento,
                id_projetopedagogico = @id_projetopedagogico, id_saladeaula = @id_saladeaula, id_disciplina = @id_disciplina,
                bl_ativo = @bl_ativo, bl_titular = @bl_titular, id_livro = @id_livro, bl_autor = @bl_autor, nu_porcentagem = @nu_porcentagem,
                dt_inicio = @dt_inicio, dt_fim = @dt_fim, id_titulacao = @id_titulacao, bl_desativarmoodle = @bl_desativarmoodle
            WHERE id_perfilreferencia = @id_perfilreferencia
        END
    ELSE
        BEGIN
          IF @id_areaconhecimento IS NOT NULL
            BEGIN
            INSERT INTO tb_usuarioperfilentidadereferencia (id_usuario, id_entidade, id_perfil, id_areaconhecimento, id_projetopedagogico,
                    id_saladeaula, id_disciplina, bl_ativo, bl_titular, id_livro, bl_autor, nu_porcentagem, dt_inicio,
                    dt_fim, id_titulacao, bl_desativarmoodle) VALUES (@id_usuario, @id_entidade, @id_perfil, @id_areaconhecimento, @id_projetopedagogico,
                    @id_saladeaula, @id_disciplina, @bl_ativo, @bl_titular, @id_livro, @bl_autor, @nu_porcentagem, @dt_inicio,
                    @dt_fim, @id_titulacao, @bl_desativarmoodle)
            END
          ELSE
            BEGIN
              INSERT INTO tb_usuarioperfilentidadereferencia (id_usuario, id_entidade, id_perfil, id_projetopedagogico,
                    id_saladeaula, id_disciplina, bl_ativo, bl_titular, id_livro, bl_autor, nu_porcentagem, dt_inicio,
                    dt_fim, id_titulacao, bl_desativarmoodle) VALUES (@id_usuario, @id_entidade, @id_perfil, @id_projetopedagogico,
                    @id_saladeaula, @id_disciplina, @bl_ativo, @bl_titular, @id_livro, @bl_autor, @nu_porcentagem, @dt_inicio,
                    @dt_fim, @id_titulacao, @bl_desativarmoodle)
            END
        END
END
GO








ALTER FUNCTION [dbo].[fn_extratocomissaodireitos]
(	
@id_usuario INT,
@id_entidade INT,
@dt_inicio DATE,
@dt_termino DATETIME2
)
RETURNS TABLE 
AS


RETURN 
(
SELECT  DISTINCT crs.st_disciplina ,
        CAST(crs.nu_valor AS FLOAT) AS nu_valor,
        'Sub Total' AS st_tipo,
        ps.id_usuario, 
		crs.id_disciplina,
		crs.nu_valorvenda
		FROM tb_pessoa AS ps
		CROSS APPLY (
			SELECT 
				sum(cr.nu_comissaoreceber) AS nu_valor, 
				cr.id_usuario, 
				cr.st_disciplina, 
				cr.id_disciplina,
				cr.nu_valorliquido as nu_valorvenda
			FROM dbo.vw_comissaodireitos AS cr 
			WHERE ps.id_usuario = cr.id_usuario 
			AND @id_entidade = cr.id_entidade 
			AND dt_confirmacao BETWEEN @dt_inicio  AND @dt_termino AND cr.id_funcao = 4
			GROUP BY cr.id_usuario, cr.st_disciplina, cr.id_disciplina, cr.nu_valorliquido
		) AS crs 

UNION
SELECT DISTINCT 
        dc.st_disciplina ,
        CAST(lc.nu_valor AS FLOAT) AS nu_valor,
        'Adiantamento' AS st_tipo ,
        cl.id_usuario,
		cl.id_disciplina,
		null as nu_valorvenda
FROM    dbo.tb_comissaolancamento AS cl
        JOIN dbo.tb_lancamento AS lc ON cl.id_lancamento = lc.id_lancamento 
			AND lc.dt_vencimento BETWEEN @dt_inicio 
			AND DATEADD(hh, 23, DATEADD(mi, 59, DATEADD(ss, 59, @dt_termino))) 
        JOIN dbo.tb_disciplina AS dc ON cl.id_disciplina = dc.id_disciplina
        LEFT JOIN dbo.tb_vendaproduto AS vp ON vp.id_vendaproduto = cl.id_vendaproduto
		LEFT JOIN dbo.tb_venda AS vd ON vd.id_venda = vp.id_venda AND vd.id_entidade = @id_entidade
        LEFT JOIN dbo.tb_produtoprojetopedagogico AS pp ON pp.id_produto = vp.id_produto
        LEFT JOIN dbo.tb_projetopedagogico AS ppd ON ppd.id_projetopedagogico = pp.id_projetopedagogico
WHERE   cl.bl_adiantamento = 1 AND cl.id_usuario = @id_usuario
UNION
SELECT DISTINCT 
        st_disciplina ,
        CAST(nu_saldo AS FLOAT) AS nu_valor ,
        'Saldo Anterior' AS st_tipo ,
        id_usuario, 
		id_disciplina,
		null as nu_valorvenda
FROM    dbo.fn_saldocomissaodireitos(
	@id_usuario,
	@id_entidade, 
	@dt_inicio, 
	null)
)











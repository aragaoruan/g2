CREATE FUNCTION [dbo].[fn_saldocomissaodireitos_coordenador] (
	@id_usuario INT,
	@id_entidade INT,
	@dt_inicio DATE,
	@dt_fim DATE
) RETURNS TABLE AS RETURN (
	SELECT
		lcc.nu_porcentagem,
		us.id_usuario,
		ps.id_entidade,
		(ISNULL(lcc.nu_soma, 0)) AS nu_saldo,
		(ISNULL(lcc.nu_soma_quitado, 0)) AS nu_soma_quitado,
		lcc.nu_soma AS c,
		NULL AS a,
		uper.id_projetopedagogico,
		pp.st_projetopedagogico
	FROM
		dbo.tb_usuario AS us
	JOIN tb_pessoa AS ps ON ps.id_usuario = us.id_usuario	AND ps.id_usuario = @id_usuario	AND ps.id_entidade = @id_entidade
	JOIN dbo.tb_usuarioperfilentidadereferencia AS uper ON uper.id_entidade = ps.id_entidade AND uper.id_usuario = ps.id_usuario AND uper.id_projetopedagogico IS NOT NULL
	JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = uper.id_projetopedagogico
OUTER APPLY (
		SELECT
			SUM (cr.nu_comissaoreceber) AS nu_soma,
			SUM (cr.nu_quitado) AS nu_soma_quitado,
			cr.id_usuario,
			cr.id_projetopedagogico,
			cr.st_projetopedagogico,
			cr.nu_porcentagem
		FROM
			vw_comissaorecebercoordenador AS cr
		WHERE
			cr.id_usuario = us.id_usuario
		AND @id_entidade = cr.id_entidade
		AND pp.id_projetopedagogico = cr.id_projetopedagogico
		AND cr.dt_quitado BETWEEN @dt_inicio AND @dt_fim
		GROUP BY
			cr.id_usuario,
			cr.st_projetopedagogico,
			cr.id_projetopedagogico,
			cr.nu_porcentagem
	) AS lcc
)
go


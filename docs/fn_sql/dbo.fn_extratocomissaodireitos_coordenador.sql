CREATE FUNCTION [dbo].[fn_extratocomissaodireitos_coordenador] (
  @id_usuario INT,
  @id_entidade INT,
  @dt_inicio DATE,
  @dt_termino DATETIME2
) RETURNS TABLE AS RETURN (
SELECT DISTINCT
  id_projetopedagogico,
  CAST (nu_saldo AS NUMERIC(15,2)) AS nu_valor,
  CAST (nu_soma_quitado AS NUMERIC(15,2)) AS nu_valorvenda,
  'Sub Total' AS st_tipo,
  id_usuario,
  st_projetopedagogico,
  CAST (nu_porcentagem AS INTEGER) AS nu_porcentagem
FROM
    dbo.fn_saldocomissaodireitos_coordenador (
        @id_usuario,
        @id_entidade,
        @dt_inicio,
        @dt_termino
    )
)
go


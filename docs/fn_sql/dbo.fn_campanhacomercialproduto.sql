CREATE FUNCTION [dbo].[fn_campanhacomercialproduto]
  (
    @id_tipocampanha INT,
    @id_formapagamento INT,
    @id_entidade INT,
    @id_produto INT = NULL,
    @id_venda INT = NULL,
    @id_campanhaselecionada INT = NULL
  )
  RETURNS TABLE
  AS
  RETURN
  (
  SELECT
    cc.id_campanhacomercial,
    cc.id_entidade,
    cc.id_situacao,
    sit.st_situacao,
    cc.nu_disponibilidade,
    cc.st_campanhacomercial,
    cc.st_descricao,
    cc.dt_inicio,
    cc.dt_fim,
    cc.bl_todasformas,
    cc.bl_todosprodutos,
    cc.id_tipocampanha,
    tc.st_tipocampanha,
    0 AS nu_descontominimo,
    cc.nu_valordesconto AS nu_descontaximo,
    0 AS nu_valormin,
    cc.nu_valordesconto AS nu_valormax,
    cc.id_tipodesconto
  FROM tb_campanhacomercial AS cc
--INNER JOIN tb_campanhadesconto AS cd ON cc.id_campanhacomercial = cd.id_campanhacomercial
    INNER JOIN tb_tipocampanha AS tc ON cc.id_tipocampanha = tc.id_tipocampanha
    INNER JOIN tb_situacao AS sit ON cc.id_situacao = sit.id_situacao
    LEFT JOIN tb_campanhaproduto AS cp ON cc.id_campanhacomercial = cp.id_campanhacomercial
    LEFT JOIN tb_campanhaformapagamento AS cfp ON cc.id_campanhacomercial = cfp.id_campanhacomercial
  WHERE
    CFP.id_formapagamento = @id_formapagamento
    AND cc.id_tipocampanha = 2
    AND cc.id_finalidadecampanha in (1,4)
    AND cc.id_entidade = @id_entidade
    AND id_produto = @id_produto
    AND cc.dt_inicio <= GETDATE()
    AND (cc.dt_fim >= GETDATE() OR cc.dt_fim IS NULL)
  UNION

  SELECT
    cc.id_campanhacomercial,
    cc.id_entidade,
    cc.id_situacao,
    sit.st_situacao,
    cc.nu_disponibilidade,
    cc.st_campanhacomercial,
    cc.st_descricao,
    cc.dt_inicio,
    cc.dt_fim,
    cc.bl_todasformas,
    cc.bl_todosprodutos,
    cc.id_tipocampanha,
    tc.st_tipocampanha,
    0 AS nu_descontominimo,
    cc.nu_valordesconto AS nu_descontaximo,
    0 AS nu_valormin,
    cc.nu_valordesconto AS nu_valormax,
    cc.id_tipodesconto
  FROM tb_campanhacomercial AS cc
--INNER JOIN tb_campanhadesconto AS cd ON cc.id_campanhacomercial = cd.id_campanhacomercial
    INNER JOIN tb_tipocampanha AS tc ON cc.id_tipocampanha = tc.id_tipocampanha
    INNER JOIN tb_situacao AS sit ON cc.id_situacao = sit.id_situacao
    LEFT JOIN tb_campanhaproduto AS cp ON cc.id_campanhacomercial = cp.id_campanhacomercial
    LEFT JOIN tb_campanhaformapagamento AS cfp ON cc.id_campanhacomercial = cfp.id_campanhacomercial
  WHERE
    (
      cc.id_finalidadecampanha = 1
      AND cc.bl_ativo = 1
      AND CFP.id_formapagamento = @id_formapagamento
      AND bl_todosprodutos = 1
      AND cc.id_tipocampanha = 2
      AND cc.id_finalidadecampanha in (1,4)
      AND cc.id_entidade = @id_entidade
      AND cc.dt_inicio <= GETDATE()
      AND (cc.dt_fim >= GETDATE() OR cc.dt_fim IS NULL)
    )
    OR (
      @id_campanhaselecionada IS NOT NULL
      AND cc.id_campanhacomercial = @id_campanhaselecionada
    )
  UNION

  SELECT
    cc.id_campanhacomercial,
    cc.id_entidade,
    cc.id_situacao,
    sit.st_situacao,
    cc.nu_disponibilidade,
    cc.st_campanhacomercial,
    cc.st_descricao,
    cc.dt_inicio,
    cc.dt_fim,
    cc.bl_todasformas,
    cc.bl_todosprodutos,
    cc.id_tipocampanha,
    tc.st_tipocampanha,
    0 AS nu_descontominimo,
    cc.nu_valordesconto AS nu_descontaximo,
    0 AS nu_valormin,
    cc.nu_valordesconto AS nu_valormax,
    cc.id_tipodesconto
  FROM tb_campanhacomercial AS cc
--iNNER JOIN tb_campanhadesconto AS cd ON cc.id_campanhacomercial = cd.id_campanhacomercial
    INNER JOIN tb_tipocampanha AS tc ON cc.id_tipocampanha = tc.id_tipocampanha
    INNER JOIN tb_situacao AS sit ON cc.id_situacao = sit.id_situacao
    LEFT JOIN tb_campanhaproduto AS cp ON cc.id_campanhacomercial = cp.id_campanhacomercial
    LEFT JOIN tb_campanhaformapagamento AS cfp ON cc.id_campanhacomercial = cfp.id_campanhacomercial
  WHERE
    (
      cc.bl_ativo = 1
      AND bl_todasformas = 1
      AND bl_todosprodutos = 1
      AND cc.id_tipocampanha = 2
      AND cc.id_finalidadecampanha in (1,4)
      AND cc.id_entidade = @id_entidade
      AND cc.dt_inicio <= GETDATE()
      AND (cc.dt_fim >= GETDATE() OR cc.dt_fim IS NULL)
    )
    OR (
      @id_campanhaselecionada IS NOT NULL
      AND cc.id_campanhacomercial = @id_campanhaselecionada
    )
  UNION

  SELECT
    cc.id_campanhacomercial,
    cc.id_entidade,
    cc.id_situacao,
    sit.st_situacao,
    cc.nu_disponibilidade,
    cc.st_campanhacomercial,
    cc.st_descricao,
    cc.dt_inicio,
    cc.dt_fim,
    cc.bl_todasformas,
    cc.bl_todosprodutos,
    cc.id_tipocampanha,
    tc.st_tipocampanha,
    0 AS nu_descontominimo,
    cc.nu_valordesconto AS nu_descontaximo,
    0 AS nu_valormin,
    cc.nu_valordesconto AS nu_valormax,
    cc.id_tipodesconto
  FROM tb_campanhacomercial AS cc
-- INNER JOIN tb_campanhadesconto AS cd ON cc.id_campanhacomercial = cd.id_campanhacomercial
    INNER JOIN tb_tipocampanha AS tc ON cc.id_tipocampanha = tc.id_tipocampanha
    INNER JOIN tb_situacao AS sit ON cc.id_situacao = sit.id_situacao
    LEFT JOIN tb_campanhaproduto AS cp ON cc.id_campanhacomercial = cp.id_campanhacomercial
    LEFT JOIN tb_campanhaformapagamento AS cfp ON cc.id_campanhacomercial = cfp.id_campanhacomercial
  WHERE
    (
      cc.bl_ativo = 1
      AND bl_todasformas = 1
      AND id_produto = @id_produto
      AND cc.id_tipocampanha = 2
      AND cc.id_finalidadecampanha in (1,4)
      AND cc.id_entidade = @id_entidade
      AND cc.dt_inicio <= GETDATE()
      AND (cc.dt_fim >= GETDATE() OR cc.dt_fim IS NULL)
    )
    OR (
      @id_campanhaselecionada IS NOT NULL
      AND cc.id_campanhacomercial = @id_campanhaselecionada
    )
  UNION

  SELECT
    cc.id_campanhacomercial,
    cc.id_entidade,
    cc.id_situacao,
    sit.st_situacao,
    cc.nu_disponibilidade,
    cc.st_campanhacomercial,
    cc.st_descricao,
    cc.dt_inicio,
    cc.dt_fim,
    cc.bl_todasformas,
    cc.bl_todosprodutos,
    cc.id_tipocampanha,
    tc.st_tipocampanha,
    0 AS nu_descontominimo,
    cc.nu_valordesconto AS nu_descontominimo,
    0 AS nu_valormin,
    cc.nu_valordesconto AS nu_valormax,
    cc.id_tipodesconto
  FROM tb_campanhacomercial AS cc
--INNER JOIN tb_campanhadesconto AS cd ON cc.id_campanhacomercial = cd.id_campanhacomercial
    INNER JOIN tb_tipocampanha AS tc ON cc.id_tipocampanha = tc.id_tipocampanha
    INNER JOIN tb_situacao AS sit ON cc.id_situacao = sit.id_situacao
    INNER JOIN tb_campanhaproduto AS cp ON cc.id_campanhacomercial = cp.id_campanhacomercial
    INNER JOIN tb_vendaproduto AS vp ON vp.id_produto = cp.id_produto
    INNER JOIN tb_venda AS vd ON vd.id_venda = vp.id_venda
    LEFT JOIN tb_campanhaformapagamento AS cfp ON cc.id_campanhacomercial = cfp.id_campanhacomercial
  WHERE
    (
      cc.bl_ativo = 1
      AND vp.id_produto = @id_produto
      AND cc.id_tipocampanha = 2
      AND cc.id_finalidadecampanha in (1,4)
      AND cc.id_entidade = @id_entidade
      AND vd.id_venda = @id_venda
    )
    OR (
      @id_campanhaselecionada IS NOT NULL
      AND cc.id_campanhacomercial = @id_campanhaselecionada
    )
  )

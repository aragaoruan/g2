





ALTER FUNCTION [dbo].[fn_saldocomissaodireitos]
    (
      @id_usuario INT ,
      @id_entidade INT ,
      @dt_inicio DATE ,
      @dt_fim DATE

    )
RETURNS TABLE
AS
RETURN
    ( SELECT    us.id_usuario ,
                ps.id_entidade ,
                (ISNULL(lcc.nu_soma,0) - ISNULL(lca.nu_soma,0)) AS nu_saldo
				,lcc.nu_soma AS c 
				,lca.nu_soma AS a,
				uper.id_disciplina,
				dc.st_disciplina

      FROM      dbo.tb_usuario AS us

                JOIN tb_pessoa AS ps ON ps.id_usuario = us.id_usuario
                                        AND ps.id_usuario = @id_usuario
                                        AND ps.id_entidade = @id_entidade
				JOIN dbo.tb_usuarioperfilentidadereferencia AS uper ON uper.id_entidade = ps.id_entidade AND uper.id_usuario = ps.id_usuario AND uper.id_disciplina IS NOT NULL
                JOIN dbo.tb_disciplina AS dc ON dc.id_disciplina = uper.id_disciplina
                OUTER APPLY (SELECT sum(cr.nu_comissaoreceber) AS nu_soma,cr.id_usuario, cr.st_disciplina, cr.id_disciplina 
                              FROM      vw_comissaoreceber AS cr
                              WHERE    cr.id_usuario = us.id_usuario AND @id_entidade = cr.id_entidade AND cr.id_disciplina = uper.id_disciplina
                                        AND cr.dt_confirmacao < @dt_inicio  GROUP BY cr.id_usuario, cr.st_disciplina, cr.id_disciplina 
                            ) AS lcc
                OUTER APPLY ( SELECT   cl.id_usuario, 
                                         SUM(ISNULL(lc.nu_valor,0)) AS nu_soma,cl.id_disciplina
                              FROM      dbo.tb_comissaolancamento AS cl
                                        JOIN dbo.tb_lancamento AS lc ON cl.id_lancamento = lc.id_lancamento
                                        JOIN dbo.tb_disciplina AS dc ON cl.id_disciplina = dc.id_disciplina
                              WHERE     lc.id_entidade = ps.id_entidade
                                        AND cl.bl_adiantamento = 1
                                        AND cl.id_usuario = us.id_usuario AND cl.id_disciplina = uper.id_disciplina
                                        AND lc.dt_vencimento < @dt_inicio GROUP BY cl.id_usuario, cl.id_disciplina
                            ) AS lca
               

    )







/****** Object:  View [dbo].[vw_pesquisarpessoa]    Script Date: 10/08/2012 17:42:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE VIEW [dbo].[vw_pesquisarlivro]
As

SELECT l.id_livro
      ,l.id_situacao
      ,l.id_entidadecadastro
      ,l.id_usuariocadastro
      ,l.id_tipolivro
      ,l.id_livrocolecao
      ,l.st_livro
      ,l.st_isbn
      ,l.st_codigocontrole
      ,l.bl_ativo
      
      ,lc.st_livrocolecao
      
      ,tl.st_tipolivro
      
      ,s.st_situacao
      
  FROM tb_livro as l 
	join tb_livrocolecao as lc on l.id_livrocolecao = lc.id_livrocolecao
	join tb_tipolivro as tl on l.id_tipolivro = tl.id_tipolivro
	join tb_situacao as s on s.st_tabela = 'tb_livro' and s.id_situacao = l.id_situacao
  where l.bl_ativo = 1 and lc.bl_ativo = 1
  
GO

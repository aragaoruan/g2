CREATE VIEW dbo.vw_alocacao
AS
SELECT
  al.id_alocacao,
  al.id_saladeaula,
  mt.id_usuario,
  mt.bl_institucional,
  ei.nu_perfilalunoobs,
  mt.id_entidadeatendimento,
  al.bl_ativo
FROM dbo.tb_alocacao al
JOIN dbo.tb_matriculadisciplina md
  ON md.id_matriculadisciplina = al.id_matriculadisciplina
JOIN dbo.tb_matricula mt
  ON mt.id_matricula = md.id_matricula
JOIN dbo.tb_saladeaula AS sa
  ON sa.id_saladeaula = al.id_saladeaula
LEFT JOIN dbo.tb_saladeaulaintegracao AS sai
  ON sa.id_saladeaula = sai.id_saladeaula
LEFT JOIN dbo.tb_entidadeintegracao AS ei
  ON ei.id_entidade = sa.id_entidade
  AND sai.id_sistema = ei.id_sistema
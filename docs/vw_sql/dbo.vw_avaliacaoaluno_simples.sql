CREATE view [dbo].[vw_avaliacaoaluno_simples] as
  SELECT DISTINCT
    mt.id_entidadematricula ,
    id_avaliacaoaluno ,
    mt.id_matricula ,
    us.st_nomecompleto ,
    aa.st_nota ,
    acr.id_avaliacao ,
    av.st_avaliacao ,
    md.id_situacao ,
    st.st_situacao ,
    ac.id_tipoprova ,
    md.id_evolucao ,
    ev.st_evolucao ,
    alc.id_saladeaula,
    aa.bl_ativo,
    md.id_disciplina,
    dc.st_tituloexibicao as st_tituloexibicaodisciplina,
    CAST(av.nu_valor AS VARCHAR(100)) AS nu_notamax ,
    ac.id_tipocalculoavaliacao ,
    av.id_tipoavaliacao ,
    ta.st_tipoavaliacao ,
    ( CASE WHEN acr.id_avaliacaorecupera IS NULL THEN 0
      ELSE acr.id_avaliacaorecupera
      END ) AS id_avaliacaorecupera ,
    pp.nu_notamaxima ,
    pp.nu_percentualaprovacao ,
    ac.id_avaliacaoconjunto ,
    pp.st_projetopedagogico ,
    mt.id_projetopedagogico ,
    aa.dt_defesa ,
    aa.st_tituloavaliacao ,
    mt.id_entidadeatendimento ,
    md.id_matriculadisciplina ,
    aa.id_tiponota ,
    sdis.id_evolucao AS id_evolucaodisciplina ,
    sdis.st_evolucao AS st_evolucaodisciplina ,
    dc.nu_cargahoraria,
    aa.id_notaconceitual
  FROM  tb_matriculadisciplina AS md
    JOIN tb_matricula AS mt ON md.id_matricula = mt.id_matricula
    JOIN dbo.tb_usuario AS us ON us.id_usuario = mt.id_usuario
    JOIN tb_disciplina as dc on dc.id_disciplina = md.id_disciplina
    JOIN tb_alocacao AS alc ON alc.id_matriculadisciplina = md.id_matriculadisciplina
                               AND alc.bl_ativo = 1
    JOIN tb_projetopedagogico AS PP ON pp.id_projetopedagogico = mt.id_projetopedagogico
    JOIN tb_avaliacaoconjuntoreferencia AS acrf ON acrf.id_saladeaula = alc.id_saladeaula
                                                   AND acrf.dt_fim IS NULL
--JOIN tb_disciplinasaladeaula AS dsa ON dsa.id_saladeaula = alc.id_saladeaula
    JOIN tb_evolucao AS sdis ON sdis.id_evolucao = md.id_evolucao
    JOIN tb_avaliacaoconjunto AS ac ON ac.id_avaliacaoconjunto = acrf.id_avaliacaoconjunto
                                       AND id_tipoprova = 2
    JOIN tb_avaliacaoconjuntorelacao AS acr ON acr.id_avaliacaoconjunto = ac.id_avaliacaoconjunto
    JOIN tb_avaliacao AS av ON av.id_avaliacao = acr.id_avaliacao
    JOIN tb_tipoavaliacao AS ta ON ta.id_tipoavaliacao = av.id_tipoavaliacao
    LEFT JOIN tb_avaliacaoaluno AS aa ON aa.id_matricula = mt.id_matricula
                                         AND aa.id_avaliacaoconjuntoreferencia = acrf.id_avaliacaoconjuntoreferencia
                                         AND aa.id_avaliacao = av.id_avaliacao
                                         AND aa.bl_ativo = 1
                                         AND aa.id_situacao = 86
    JOIN tb_situacao AS st ON st.id_situacao = md.id_situacao
    JOIN tb_evolucao AS ev ON ev.id_evolucao = md.id_evolucao
  WHERE   mt.bl_ativo = 1 and dc.id_tipodisciplina <> 3
GO


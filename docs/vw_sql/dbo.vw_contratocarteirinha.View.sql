
/****** Object:  View [dbo].[vw_contratocarteirinha]    Script Date: 26/12/2014 17:20:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_contratocarteirinha] as
SELECT
	c.id_contrato,
	cr.id_contratoregra,
	cr.st_contratoregra,
	cr.id_modelocarteirinha,
	mc.st_modelocarteirinha
FROM
	tb_contrato c
JOIN tb_contratoregra cr ON c.id_contratoregra = cr.id_contratoregra
AND cr.bl_ativo = 1
JOIN tb_modelocarteirinha mc ON mc.id_modelocarteirinha = cr.id_modelocarteirinha
AND mc.bl_ativo = 1
AND mc.id_situacao = 140
WHERE
	c.id_situacao = 44
AND c.bl_ativo = 1

GO



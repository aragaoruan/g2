CREATE VIEW [dbo].[vw_matriculacargahoraria] AS
  WITH _config AS
       (SELECT 0    AS FALSE,
			         1    AS TRUE,
			         12   AS EVOLUCAO_APROVADO,
			         6    AS EVOLUCAO_CURSANDO,
			         15   AS EVOLUCAO_CONCLUINTE,
			         21   AS EVOLUCAO_TRANCADO,
			         27   AS EVOLUCAO_CANCELADO,
			         40   AS EVOLUCAO_BLOQUEADO,
			         41   AS EVOLUCAO_ANULADO,
			         81   AS EVOLUCAO_FORMADO,
			         82   AS EVOLUCAO_DIPLOMADO,
			         65   AS SITUACAO_ISENTO
       )

 SELECT m.id_matricula,
        m.id_usuario,
        us.st_nomecompleto,
        us.st_cpf,
        pp.id_projetopedagogico,
        pp.st_projetopedagogico,
        m.id_entidadematricula AS id_entidade,
        etm.st_nomeentidade AS st_entidade,
        st.id_situacao,
        st.st_situacao,
        ev.id_evolucao,
        ev.st_evolucao,
        m.dt_cadastro AS dt_matricula,
        (SELECT SUM(dis.nu_cargahoraria)
           FROM tb_matriculadisciplina md
           JOIN tb_disciplina dis
             ON dis.id_disciplina = md.id_disciplina
          WHERE md.id_matricula = m.id_matricula) AS nu_cargahoraria,
        CASE
             WHEN etmz.id_esquemaconfiguracao = _.TRUE THEN
                 (SELECT COALESCE(SUM(dc.nu_cargahoraria), 0)
                    FROM tb_matriculadisciplina md
                    JOIN tb_disciplina dc
                      ON dc.id_disciplina = md.id_disciplina
                    LEFT JOIN tb_alocacao al
                      ON al.id_matriculadisciplina = md.id_matriculadisciplina
                     AND al.bl_ativo = _.TRUE
                    LEFT JOIN dbo.vw_matriculanota mn
                      ON mn.id_matriculadisciplina = md.id_matriculadisciplina
                     AND mn.id_alocacao = al.id_alocacao
                   WHERE md.id_matricula = m.id_matricula
                     AND (   (al.id_alocacao IS NOT NULL AND md.id_evolucao = _.EVOLUCAO_APROVADO AND mn.bl_aprovado = _.TRUE) /* aprovado */
                          OR (md.id_aproveitamento IS NOT NULL AND md.id_situacao = _.SITUACAO_ISENTO) /* isento */
                         )
                 )

                 +

                 (SELECT COALESCE(SUM(atividade.nu_horasconvalidada), 0)
                    FROM tb_atividadecomplementar atividade
                   WHERE atividade.id_matricula = m.id_matricula) /* atividades complementares */
             ELSE 0
         END AS nu_cargahorariaintegralizada,
        m.dt_ultimaoferta,
        m.dt_ultimoacesso,
        etmz.id_esquemaconfiguracao
   FROM tb_matricula m
  CROSS APPLY _config _
   JOIN tb_projetopedagogico pp
     ON m.id_projetopedagogico = pp.id_projetopedagogico
   JOIN tb_areaprojetopedagogico ap
     ON ap.id_projetopedagogico = pp.id_projetopedagogico
   JOIN tb_entidade etmz
     ON etmz.id_entidade = m.id_entidadematriz
   JOIN tb_entidade etm
     ON etm.id_entidade = m.id_entidadematricula
   JOIN tb_entidade eta
     ON eta.id_entidade = m.id_entidadeatendimento
   JOIN tb_situacao st
     ON st.id_situacao = m.id_situacao
   JOIN tb_evolucao ev
     ON ev.id_evolucao = m.id_evolucao
    AND ev.id_evolucao IN (_.EVOLUCAO_CURSANDO,
                           _.EVOLUCAO_CONCLUINTE,
                           _.EVOLUCAO_TRANCADO,
                           _.EVOLUCAO_CANCELADO,
                           _.EVOLUCAO_BLOQUEADO,
                           _.EVOLUCAO_ANULADO,
                           _.EVOLUCAO_FORMADO,
                           _.EVOLUCAO_DIPLOMADO)
   JOIN tb_usuario us
     ON us.id_usuario = m.id_usuario

  WHERE m.bl_ativo = _.TRUE

GO

CREATE view vw_saladeaulaquantitativos as
SELECT DISTINCT sa.id_saladeaula,
        sa.dt_cadastro,
        sa.st_saladeaula,
        sa.bl_ativa,
        sa.id_usuariocadastro,
        sa.id_modalidadesaladeaula,
        sa.dt_inicioinscricao,
        sa.dt_fiminscricao,
        sa.dt_abertura,
        sa.st_localizacao,
        sa.id_tiposaladeaula,
        sa.dt_encerramento,
        sa.id_entidade,
        sa.id_periodoletivo,
        sa.bl_usardoperiodoletivo,
        sa.id_situacao,
        sa.nu_maxalunos,
        sa.nu_diasencerramento,
        sa.bl_semencerramento,
        sa.nu_diasaluno,
        sa.bl_semdiasaluno,
        msa.st_modalidadesaladeaula,
        dsa.id_disciplina,
	   (SELECT COUNT(id_alocacao) FROM tb_alocacao al WHERE al.id_saladeaula = sa.id_saladeaula AND al.bl_ativo = 1) AS nu_alunos
FROM tb_saladeaula AS sa
INNER JOIN tb_modalidadesaladeaula AS msa ON msa.id_modalidadesaladeaula = sa.id_modalidadesaladeaula
INNER JOIN tb_tiposaladeaula AS tsa ON tsa.id_tiposaladeaula = sa.id_tiposaladeaula
INNER JOIN tb_entidade AS e ON e.id_entidade = sa.id_entidade
INNER JOIN tb_situacao AS s ON s.id_situacao = sa.id_situacao
INNER JOIN tb_categoriasala AS csa ON csa.id_categoriasala = sa.id_categoriasala
LEFT JOIN tb_disciplinasaladeaula dsa ON dsa.id_saladeaula = sa.id_saladeaula WHERE sa.id_categoriasala = 1




GO
/****** Object:  View [dbo].[vw_entidaderecursivaid]    Script Date: 10/08/2012 17:41:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_entidaderecursivaid] AS
WITH rec (
	id_entidade,
	st_nomeentidade,
	id_entidadepai,
	nu_entidadepai
)AS (
	SELECT DISTINCT
		e.id_entidade,
		e.st_nomeentidade,
		e.id_entidadepai,
	CAST(e.id_entidadepai AS VARCHAR) AS nu_entidadepai
	FROM dbo.vw_entidaderelacao e
		where e.id_situacao = 2
	UNION ALL
	SELECT
		e.id_entidade,
		e.st_nomeentidade,
		e.id_entidadepai,
		nu_entidadepai
	FROM dbo.vw_entidaderelacao AS e
		JOIN rec AS r ON (e.id_entidadepai = r.id_entidade) AND e.id_entidade != e.id_entidadepai
		where e.id_situacao = 2
	)SELECT DISTINCT
		id_entidade,
		st_nomeentidade,id_entidadepai,
		nu_entidadepai
	FROM rec
GO

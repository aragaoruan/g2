CREATE VIEW [dbo].[vw_modulodisciplina] AS
SELECT
	p.id_projetopedagogico,
	p.st_projetopedagogico,
	md.id_modulodisciplina,
	md.id_modulo,
	m.st_modulo,
	md.id_disciplina,
	d.st_disciplina,
	ar.id_areaconhecimento,
	CASE
WHEN ar.st_areaconhecimento IS NULL THEN
	'Não informado'
ELSE
	ar.st_areaconhecimento
END AS st_areaconhecimento,
 md.id_serie,
 s.st_serie,
 md.id_nivelensino,
 ns.st_nivelensino,
 md.bl_obrigatoria,
 md.nu_ordem,
 md.bl_ativo,
 md.nu_ponderacaocalculada,
 md.nu_ponderacaoaplicada,
 md.id_usuarioponderacao,
 md.dt_cadastroponderacao,
 md.nu_importancia,
 d.nu_cargahoraria AS nu_cargahorariad,
 d.nu_repeticao,
 d.sg_uf AS st_estudio,
 CASE
WHEN md.nu_cargahoraria IS NULL THEN
	d.nu_cargahoraria
ELSE
	md.nu_cargahoraria
END AS nu_cargahoraria,
 md.nu_obrigatorioalocacao,
 md.nu_disponivelapartirdo,
 md.nu_percentualsemestre,
 d.id_tipodisciplina
FROM
	tb_modulodisciplina AS md
INNER JOIN tb_modulo AS m ON m.id_modulo = md.id_modulo AND m.bl_ativo = 1
INNER JOIN tb_projetopedagogico AS p ON m.id_projetopedagogico = p.id_projetopedagogico
INNER JOIN tb_disciplina AS d ON md.id_disciplina = d.id_disciplina
LEFT JOIN tb_serie AS s ON s.id_serie = md.id_serie
LEFT JOIN tb_nivelensino AS ns ON ns.id_nivelensino = md.id_nivelensino
LEFT JOIN tb_areaconhecimento AS ar ON ar.id_areaconhecimento = md.id_areaconhecimento
WHERE
	md.bl_ativo = 1
GO


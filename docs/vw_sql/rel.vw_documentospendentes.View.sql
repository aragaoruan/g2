
GO
/****** Object:  View [rel].[vw_documentospendentes]    Script Date: 10/08/2012 17:42:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [rel].[vw_documentospendentes] as
SELECT  mt.id_matricula ,
        ved.st_nomecompleto ,
        pp.st_projetopedagogico ,
        mt.id_projetopedagogico,
        ved.st_documentos,
        mt.id_entidadeatendimento AS id_entidade
FROM  dbo.tb_matricula AS mt
        JOIN dbo.tb_projetopedagogico AS PP ON pp.id_projetopedagogico = mt.id_projetopedagogico
        JOIN dbo.vw_entregadocumento ved on ved.id_usuarioaluno = mt.id_usuario AND ved.id_entidade = mt.id_entidadeatendimento AND ved.id_documentosutilizacao = 1
WHERE   mt.id_evolucao IN (6) AND (ved.id_situacao != 59 OR ved.id_situacao IS NULL) AND mt.bl_ativo = 1
GO

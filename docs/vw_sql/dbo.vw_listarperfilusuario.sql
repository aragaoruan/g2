
GO

/****** Object:  View [dbo].[vw_listarperfilusuario]    Script Date: 20/05/2014 16:07:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




 CREATE VIEW [dbo].[vw_listarperfilusuario] as
SELECT DISTINCT
		upe.id_usuario,
		upe.dt_termino ,
        upe.dt_inicio ,
        upe.dt_cadastro ,
        upe.id_situacao ,
        p.id_perfil ,
        p.st_nomeperfil ,
        er.st_nomeentidade ,
        er.nu_entidadepai,
		s.st_situacao AS st_situacaoperfil,
		er.id_entidade
		
	FROM dbo.tb_usuarioperfilentidade AS upe
JOIN dbo.tb_perfil AS p ON upe.id_perfil = p.id_perfil
JOIN dbo.tb_situacao AS s ON p.id_situacao  = s.id_situacao
JOIN dbo.vw_entidaderecursivaid AS er ON er.id_entidade = upe.id_entidade
WHERE upe.bl_ativo = 1 
UNION
SELECT DISTINCT
		upe.id_usuario,
		upe.dt_termino ,
        upe.dt_inicio ,
        upe.dt_cadastro ,
        upe.id_situacao ,
        p.id_perfil ,
        p.st_nomeperfil ,
        er.st_nomeentidade ,
        p.id_entidade AS nu_entidadepai,
		s.st_situacao AS st_situacaoperfil,
		er.id_entidade
		
	FROM dbo.tb_usuarioperfilentidade AS upe
JOIN dbo.tb_perfil AS p ON upe.id_perfil = p.id_perfil
JOIN dbo.tb_situacao AS s ON p.id_situacao  = s.id_situacao
JOIN dbo.tb_entidade AS er ON er.id_entidade = p.id_entidade
WHERE upe.bl_ativo = 1 



GO



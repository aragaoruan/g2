ALTER VIEW [dbo].[vw_gradetipotcc] AS
	SELECT  DISTINCT sa.dt_abertura,
		pp.id_projetopedagogico,
		pp.st_projetopedagogico,
		sa.st_saladeaula,
		aa.id_avaliacao ,
			st_nota = CASE WHEN dc.id_tipodisciplina = 2 AND aa.dt_defesa IS NULL AND aa.bl_ativo = 1
			THEN NULL
								ELSE CAST(aa.st_nota AS NUMERIC(8,2))
								END ,
		mdc.id_modulo,
		mdl.st_modulo,
			mdl.st_tituloexibicao AS st_tituloexibicaomodulo,
		md.id_disciplina,
			dc.st_tituloexibicao AS st_disciplina,
			dc.st_tituloexibicao AS st_tituloexibicaodisciplina,
		mt.id_matricula ,
			id_tipoavaliacao = CASE WHEN tt.id_tipoavaliacao IS NULL THEN 5
												 ELSE tt.id_tipoavaliacao
												 END ,
			aa.st_tipoavaliacao AS st_labeltipo,
			nu_notafinal = CASE WHEN dc.id_tipodisciplina = 2 AND aa.dt_defesa IS NULL THEN NULL
										 ELSE CAST((md.nu_aprovafinal* pp.nu_notamaxima/100)AS NUMERIC(5,2))
										 END ,
		ev.st_evolucao,
		st.st_situacao,
		dc.nu_cargahoraria,
		sa.id_saladeaula,
		sa.dt_encerramento,
		ndm.nu_notatotal,
			st_status = CASE WHEN  ea.id_alocacao IS NOT NULL AND  md.id_evolucao = 12 THEN ev.st_evolucao
									WHEN  ea.id_alocacao IS NOT NULL AND  md.id_evolucao = 19 THEN ev.st_evolucao
									WHEN  ea.id_alocacao IS NOT NULL AND  md.id_situacao = 65 THEN 'Crédito Concedido'
									WHEN  ea.id_alocacao IS NOT NULL AND  (ndm.nu_notatotal + nf.nu_notafaltante) >= (pp.nu_notamaxima * pp.nu_percentualaprovacao/100) THEN 'Satisfatório'
									WHEN  ea.id_alocacao IS NOT NULL AND  (ndm.nu_notatotal + nf.nu_notafaltante) < (pp.nu_notamaxima * pp.nu_percentualaprovacao/100) THEN 'Insatisfatório'
									ELSE '-'
									END
		,bl_status  = CASE WHEN md.id_evolucao = 12 THEN 1
									WHEN md.id_evolucao = 19 THEN 0
									WHEN md.id_situacao = 65 THEN 1
									WHEN (ndm.nu_notatotal + nf.nu_notafaltante) >= (pp.nu_notamaxima * pp.nu_percentualaprovacao/100) THEN 1
									WHEN (ndm.nu_notatotal + nf.nu_notafaltante) < (pp.nu_notamaxima * pp.nu_percentualaprovacao/100) THEN 0
									ELSE 0
									END
									, st_statusdisciplina = CASE
							WHEN al.id_alocacao IS NULL
							    AND md.id_situacao != 65
                                  THEN 'Não Iniciada'
                             WHEN ea.id_alocacao IS NOT NULL
                                  AND md.id_evolucao = 12
								  AND ndm.bl_aprovado = 1
								   THEN 'Aprovada'
							 WHEN ea.id_alocacao IS NOT NULL
								  AND ndm.bl_aprovado = 0
								  AND md.id_evolucao = 19
                             THEN 'Reprovado'
							 WHEN md.id_aproveitamento IS NOT NULL
                                  AND md.id_situacao = 65
                             THEN 'Isento'
                             WHEN al.id_alocacao IS NOT NULL
								  AND md.id_aproveitamento IS NULL
                                  AND( md.nu_aprovafinal IS NULL OR ea.id_alocacao IS NULL)
								  AND ( (md.id_situacao = 13 AND (ndm.st_notaead IS NULL OR ndm.st_notaatividade IS NULL OR ndm.st_notafinal IS NULL AND ndm.st_notarecuperacao IS NULL))
										OR  (md.id_situacao != 13 AND ea.id_alocacao IS NULL) )
                             THEN 'Cursando'


                             ELSE '-'
                        END
	, md.id_situacao
    , nu_ordem = CASE WHEN tt.id_tipoavaliacao = 1
                                  THEN 1
					  WHEN tt.id_tipoavaliacao =  4 THEN 2
					  WHEN tt.id_tipoavaliacao =  5 THEN 3
					  WHEN tt.id_tipoavaliacao = 2 THEN 4
					  ELSE 10
					  END
	FROM    dbo.tb_matricula AS mt
		JOIN dbo.tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
		JOIN tb_evolucao AS ev ON ev.id_evolucao = md.id_evolucao
		JOIN dbo.tb_situacao AS st ON st.id_situacao = md.id_situacao
		JOIN dbo.tb_disciplina AS dc ON dc.id_disciplina = md.id_disciplina
		LEFT JOIN dbo.tb_alocacao AS al ON al.id_matriculadisciplina = md.id_matriculadisciplina AND al.bl_ativo = 1
		LEFT JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula
		LEFT JOIN tb_encerramentoalocacao AS ea ON ea.id_alocacao = al.id_alocacao
		JOIN dbo.tb_tipoavaliacao AS tt ON tt.id_tipoavaliacao IN  (1,4,5,6)
		LEFT JOIN dbo.vw_avaliacaoaluno_simples AS aa ON aa.id_tipoavaliacao = tt.id_tipoavaliacao and aa.id_matriculadisciplina = md.id_matriculadisciplina
		--AND mt.id_matricula = aa.id_matricula AND aa.id_disciplina = md.id_disciplina AND aa.id_tipoprova = 2 AND aa.id_alocacao = al.id_alocacao
		JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
		JOIN dbo.tb_modulo AS mdl ON mdl.id_projetopedagogico = pp.id_projetopedagogico AND mdl.bl_ativo = 1
		JOIN dbo.tb_modulodisciplina AS mdc ON mdc.id_modulo = mdl.id_modulo AND mdc.id_disciplina = dc.id_disciplina   AND mdc.bl_ativo = 1
		LEFT JOIN vw_matriculanota AS ndm ON ndm.id_matriculadisciplina = md.id_matriculadisciplina and ndm.id_alocacao = al.id_alocacao

		OUTER APPLY ( SELECT    id_matricula ,
                                        id_disciplina ,
                                        SUM(CAST(nu_notamax AS FLOAT)) AS nu_notafaltante
                              FROM      vw_avaliacaoaluno_simples
                              WHERE     bl_ativo IS NULL
                                        AND id_matricula = mt.id_matricula
                                        AND id_disciplina = md.id_disciplina
                                        AND id_tipoprova = 2
										 AND id_avaliacaorecupera = 0
                              GROUP BY  id_disciplina ,
                                        id_matricula
                            ) AS nf


GO

CREATE VIEW [rel].[vw_alunosforarenovacao] AS
  WITH _config AS (
      SELECT
        0 AS FALSE,
        1 AS TRUE,
        6 AS EVOLUCAO_CURSANDO,
        7 AS EVOLUCAO_AGUARDANDO_NEGOCIACAO,
        9 AS EVOLUCAO_AGUARDANDO_RECEBIMENTO,
        10 AS EVOLUCAO_CONFIRMADA,
        11 AS EVOLUCAO_NAO_ALOCADO,
        17 AS EVOLUCAO_NEGOCIADA,
        22 AS ITEM_CONFIGURACAO_LINHA_NEGOCIO,
        53 AS SITUACAO_DISCIPLINA_PENDENTE,
        CONVERT(DATE, GETDATE()) AS TODAY
  )

  SELECT
    DISTINCT
    mat.id_usuario,
    usuario.st_nomecompleto,
    usuario.st_cpf,

    usu.st_email,
    usu.nu_ddd,
    usu.nu_telefone,
    usu.nu_dddalternativo,
    usu.nu_telefonealternativo,

    mat.id_matricula,
    mat.dt_inicio AS dt_matricula,
    mat.id_projetopedagogico,
    pro_ped.st_projetopedagogico,
    enre.id_entidade AS id_entidadeacesso,
    mat.id_entidadematricula AS id_entidade,
    ent.st_nomeentidade,
      st_outrosmotivos = (
      CASE WHEN venda.dt_ultimaoferta IS NULL
        THEN
          'Não foi encontrada a data da última oferta'
      WHEN _.TODAY < renovacao.dt_renovacao
        THEN
          CONCAT(
              'A renovação só será iniciada após ',
              CONVERT(
                    VARCHAR(10),
                    renovacao.dt_renovacao,
                    103
              )
          )
      WHEN venda.st_errorenovacao IS NOT NULL
        THEN
          venda.st_errorenovacao
      ELSE
        '-'
      END
    ),
    ISNULL(mat.bl_documentacao, 0) AS bl_motivodocumentacao,
      st_motivodocumentacao = (
      CASE mat.bl_documentacao
      WHEN 1
        THEN
          'OK'
      ELSE
        'NÃO OK'
      END
    ),
    ISNULL(venda.bl_semsalarenovacao, 0) AS bl_motivoacademico,
      st_motivoacademico = (
      CASE venda.bl_semsalarenovacao
      WHEN 1
        THEN
          'NÃO OK'
      ELSE
        'OK'
      END
    ),
      bl_motivofinanceiro = (
      CASE WHEN COUNT(lancamentos.id_lancamento) > 0
        THEN
          1
      ELSE
        0
      END
    ),
      st_motivofinanceiro = (
      CASE WHEN COUNT(lancamentos.id_lancamento) > 0
        THEN
          'NÃO OK'
      ELSE
        'OK'
      END
    ),
	ven.dt_ultimaoferta,
	ven.id_venda,
	COUNT(DISTINCT ocor.id_ocorrencia) as nu_totalocorrencia
  FROM
    tb_matricula AS mat CROSS APPLY _config AS _
    INNER JOIN tb_projetopedagogico AS pro_ped ON pro_ped.id_projetopedagogico = mat.id_projetopedagogico AND
                                                  pro_ped.bl_renovar = _.TRUE
    INNER JOIN tb_entidade AS ent ON ent.id_entidade = mat.id_entidadematricula AND mat.bl_ativo = _.TRUE AND
                                     mat.id_evolucao = _.EVOLUCAO_CURSANDO AND mat.bl_institucional = _.FALSE

    LEFT JOIN dbo.tb_entidaderelacao AS er_pai ON er_pai.id_entidade = mat.id_entidadematricula
    LEFT JOIN dbo.tb_entidaderelacao AS enre ON enre.id_entidade IN (er_pai.id_entidade, er_pai.id_entidadepai)

    INNER JOIN tb_esquemaconfiguracaoitem AS esq_conf_ite ON esq_conf_ite.id_esquemaconfiguracao =
                                                             ent.id_esquemaconfiguracao AND esq_conf_ite.st_valor = 2
    INNER JOIN tb_itemconfiguracao AS ite_con ON ite_con.id_itemconfiguracao = esq_conf_ite.id_itemconfiguracao AND
                                                 ite_con.id_itemconfiguracao = _.ITEM_CONFIGURACAO_LINHA_NEGOCIO
    INNER JOIN tb_contratomatricula AS con_mat ON con_mat.id_matricula = mat.id_matricula AND con_mat.bl_ativo = _.TRUE
    INNER JOIN tb_contrato AS con ON con.id_contrato = con_mat.id_contrato AND con.bl_ativo = _.TRUE
    INNER JOIN tb_venda AS ven ON ven.id_venda = con.id_venda AND
                                  (ven.dt_tentativarenovacao IS NULL OR ven.dt_tentativarenovacao < _.TODAY)
    OUTER APPLY (
                  SELECT
                    TOP 1
                    _ven.id_venda,
                    _ven.st_errorenovacao,
                    _ven.bl_semsalarenovacao,
                    _ven.dt_ultimaoferta
                  FROM
                    tb_venda AS _ven
                    INNER JOIN tb_vendaproduto _ven_pro ON _ven.id_venda = _ven_pro.id_venda AND
                                                           _ven_pro.id_matricula = mat.id_matricula
                    INNER JOIN tb_contrato cont ON cont.id_venda = _ven.id_venda AND cont.bl_ativo = _.TRUE
                  WHERE
                    _ven.id_evolucao = _.EVOLUCAO_CONFIRMADA
                    AND _ven.bl_ativo = _.TRUE
                  ORDER BY
                    _ven.id_venda DESC
                ) AS venda
    CROSS APPLY (
                  SELECT
                    DATEADD(
                        D,
                        -10,
                        venda.dt_ultimaoferta
                    ) AS dt_renovacao
                ) AS renovacao
    INNER JOIN tb_vendaproduto AS ven_pro ON ven_pro.id_vendaproduto = mat.id_vendaproduto AND
                                             venda.id_venda IS NOT NULL
    INNER JOIN tb_usuario AS usuario ON usuario.id_usuario = mat.id_usuario
	  INNER JOIN dbo.vw_pessoa AS usu ON usu.id_usuario = mat.id_usuario AND usu.id_entidade = mat.id_entidadematricula
	   LEFT JOIN tb_ocorrencia ocor ON ocor.id_venda = ven.id_venda AND ocor.st_titulo = 'Renovação - Negociação Inadimplência'
    OUTER APPLY (
-- Não tenha parcela vencida não paga
                  SELECT
                    TOP 1
                    lan.id_lancamento
                  FROM
                    tb_lancamento AS lan
                    INNER JOIN tb_lancamentovenda AS lan_ven ON lan_ven.id_lancamento = lan.id_lancamento AND
                                                                lan.bl_ativo = _.TRUE AND lan.bl_quitado = _.FALSE AND
                                                                lan_ven.id_venda = venda.id_venda
                  -- Não pode ter parcela original não vencida
                  UNION
                  SELECT
                    TOP 1
                    lan.id_lancamento
                  FROM
                    tb_lancamento AS lan
                    INNER JOIN tb_lancamentovenda AS lan_ven ON lan_ven.id_lancamento = lan.id_lancamento AND
                                                                lan_ven.id_venda = venda.id_venda AND
                                                                lan.dt_vencimento > _.TODAY AND lan.bl_original = _.TRUE
                  -- Verificar parcelas que não tiveram acordo
                  UNION
                  SELECT
                    TOP 1
                    lan.id_lancamento
                  FROM
                    tb_lancamento AS lan
                    INNER JOIN tb_lancamentovenda AS lan_ven ON lan_ven.id_lancamento = lan.id_lancamento AND
                                                                lan_ven.id_venda = venda.id_venda AND
                                                                lan.dt_vencimento > _.TODAY AND lan.id_acordo IS NULL
                  -- Não pode possuir alguma venda aguardando negociação ou recebimento
                  UNION
                  SELECT
                    TOP 1
                    ven.id_venda
                  FROM
                    tb_venda AS ven
                    INNER JOIN tb_vendaproduto AS ven_pro ON ven_pro.id_venda = ven.id_venda AND
                                                             ven_pro.id_matricula = mat.id_matricula
                  WHERE
                    ven.id_evolucao IN (
                      _.EVOLUCAO_AGUARDANDO_NEGOCIACAO,
                      _.EVOLUCAO_AGUARDANDO_RECEBIMENTO
                    )
                    AND ven.bl_ativo = _.TRUE
                    AND ven.dt_cadastro < (
                      SELECT
                        TOP 1
                        CASE
                        WHEN vd.dt_tentativarenovacao IS NOT NULL
                          THEN
                            vd.dt_tentativarenovacao
                        ELSE
                          vd.dt_cadastro
                        END
                      FROM
                        tb_venda AS vd
                        JOIN tb_vendaproduto AS vdp ON vdp.id_venda = vd.id_venda
                      WHERE
                        vd.id_evolucao = _.EVOLUCAO_CONFIRMADA
                        AND vdp.id_matricula = mat.id_matricula
                        AND vd.dt_tentativarenovacao IS NOT NULL
                      ORDER BY
                        vd.dt_cadastro DESC
                    )
                  -- Não existir inadimplência nas vendas anteriores da matrícula a ser renovada (Parcelas originais e acordadas)
                  UNION
                  SELECT
                    lc.id_lancamento
                  FROM
                    tb_venda AS vd
                    JOIN tb_vendaproduto AS vdp ON vdp.id_venda = vd.id_venda
                    JOIN tb_lancamentovenda AS lc_ven ON lc_ven.id_venda = vd.id_venda
                    JOIN tb_lancamento AS lc ON lc.id_lancamento = lc_ven.id_lancamento
                  WHERE
                    vd.bl_ativo = _.TRUE
                    AND lc.bl_ativo = _.TRUE
                    AND vd.id_evolucao IN (
                      _.EVOLUCAO_CONFIRMADA,
                      _.EVOLUCAO_NEGOCIADA
                    )
                    AND lc.bl_quitado = _.FALSE
                    AND lc.dt_vencimento < _.TODAY
                    AND vdp.id_matricula = mat.id_matricula
                    AND vd.id_usuario = mat.id_usuario
                ) AS lancamentos
  WHERE
    NOT EXISTS(
        SELECT
          TOP 1
          lc.id_lancamento
        FROM
          tb_lancamento AS lc
          INNER JOIN tb_lancamentovenda AS lc_ven ON lc_ven.id_lancamento = lc.id_lancamento
                                                     AND lc_ven.id_venda = venda.id_venda
        WHERE
          lc.bl_ativo = _.TRUE
          AND lc.bl_original = _.TRUE
          AND lc.dt_vencimento > _.TODAY
        ORDER BY
          lc_ven.nu_ordem DESC
    )

    AND ( (mat.bl_documentacao = _.TRUE
	       AND ( venda.bl_semsalarenovacao IS NULL
				OR venda.bl_semsalarenovacao = _.FALSE)
		   AND lancamentos.id_lancamento IS NOT NULL)
		  OR (venda.dt_ultimaoferta IS NOT NULL)
    )

    -- GII-9824 - É necessário possuir disciplinas à cursar
    AND EXISTS(
        SELECT
          id_matriculadisciplina
        FROM tb_matriculadisciplina
        WHERE
          id_evolucao = _.EVOLUCAO_NAO_ALOCADO
          AND id_situacao = _.SITUACAO_DISCIPLINA_PENDENTE
          AND id_matricula = mat.id_matricula
    )

  GROUP BY
    mat.id_usuario,
    usuario.st_nomecompleto,
    usuario.st_cpf,
    usu.st_email,
    usu.nu_ddd,
    usu.nu_telefone,
    usu.nu_dddalternativo,
    usu.nu_telefonealternativo,
    mat.id_matricula,
    mat.dt_inicio,
    mat.id_projetopedagogico,
    pro_ped.st_projetopedagogico,
    enre.id_entidade,
    mat.id_entidadematricula,
    ent.st_nomeentidade,
    mat.bl_documentacao,
    venda.bl_semsalarenovacao,
    _.TODAY,
    renovacao.dt_renovacao,
    venda.dt_ultimaoferta,
    venda.st_errorenovacao,
	ven.dt_ultimaoferta,
	ven.id_venda

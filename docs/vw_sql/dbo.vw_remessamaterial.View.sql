
GO
/****** Object:  View [dbo].[vw_remessamaterial]    Script Date: 10/08/2012 17:42:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_remessamaterial] as
SELECT rm.id_projetopedagogico, rm.id_situacao, rm.nu_remessa,
 rm.id_remessa, rm.dt_inicio, rm.dt_fim, im.id_itemdematerial, im.st_itemdematerial, dc.id_disciplina, dc.st_disciplina
 FROM dbo.tb_remessa rm
JOIN dbo.tb_materialremessa mr ON mr.id_remessa = rm.id_remessa
JOIN dbo.tb_itemdematerial im ON im.id_itemdematerial = mr.id_itemdematerial
JOIN dbo.tb_modulo md ON md.id_projetopedagogico = rm.id_projetopedagogico
JOIN dbo.tb_modulodisciplina mdc ON mdc.id_modulo = md.id_modulo
JOIN dbo.tb_itemdematerialdisciplina imd ON imd.id_disciplina = mdc.id_disciplina AND im.id_itemdematerial = imd.id_itemdematerial
JOIN dbo.tb_disciplina dc ON dc.id_disciplina = imd.id_disciplina
GO

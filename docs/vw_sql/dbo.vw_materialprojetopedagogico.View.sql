
GO
/****** Object:  View [dbo].[vw_materialprojetopedagogico]    Script Date: 10/08/2012 17:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_materialprojetopedagogico] as
SELECT mp.id_materialprojeto, mp.id_itemdematerial,mp.id_usuariocadastro,mp.dt_cadastro,mp.id_projetopedagogico,pp.st_projetopedagogico,pp.st_tituloexibicao 
FROM tb_materialprojeto AS mp
INNER JOIN tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mp.id_projetopedagogico
GO

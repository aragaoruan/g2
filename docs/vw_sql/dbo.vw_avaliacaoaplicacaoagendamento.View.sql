SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vw_avaliacaoaplicacaoagendamento] as 
select
mt.id_matricula, ac.id_tipoprova, av.id_avaliacao,av.st_avaliacao, ha.hr_inicio, ha.hr_fim, aca.bl_unica, aca.dt_aplicacao
,ha.id_horarioaula, acrf.id_avaliacaoconjuntoreferencia, aca.id_avaliacaoaplicacao
, aa.id_avaliacaoagendamento,aa.dt_agendamento , et.st_nomeentidade, aa.id_situacao, si.st_situacao
,md.id_disciplina, dc.st_disciplina, mdl.id_modulo, mdl.st_modulo
from tb_matricula mt
JOIN tb_projetopedagogico as pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
LEFT JOIN tb_matriculadisciplina as md ON md.id_matricula = mt.id_matricula and md.id_evolucao in (13,19)
JOIN tb_modulo as mdl ON mdl.id_projetopedagogico = pp.id_projetopedagogico and mdl.bl_ativo = 1
JOIN tb_modulodisciplina as mdlc ON mdlc.id_modulo = mdl.id_modulo and mdlc.id_disciplina = md.id_disciplina and mdlc.bl_ativo = 1
LEFT JOIN tb_alocacao as al ON al.id_matriculadisciplina = md.id_matriculadisciplina and al.bl_ativo = 1
JOIN tb_avaliacaoconjunto as ac ON mt.id_entidadeatendimento = ac.id_entidade
JOIN tb_avaliacaoconjuntoreferencia as acrf on acrf.dt_inicio <= GETDATE() and (acrf.dt_fim >= GETDATE()or acrf.dt_fim is null) and ((acrf.id_saladeaula = al.id_saladeaula and ac.id_tipoprova = 2) or (acrf.id_projetopedagogico = pp.id_projetopedagogico and ac.id_tipoprova = 1))and ac.id_avaliacaoconjunto = acrf.id_avaliacaoconjunto
JOIN tb_avaliacaoconjuntorelacao as acr ON acr.id_avaliacaoconjunto = acrf.id_avaliacaoconjunto
JOIN tb_avaliacaoaplicacaorelacao as acar ON acr.id_avaliacao = acar.id_avaliacao
JOIN tb_avaliacaoaplicacao as aca ON aca.id_avaliacaoaplicacao = acar.id_avaliacaoaplicacao AND aca.bl_ativo = 1
JOIN tb_avaliacao as av on av.id_avaliacao = acar.id_avaliacao
JOIN tb_avaliacaodisciplina as ad ON ad.id_disciplina = md.id_disciplina and av.id_avaliacao = ad.id_avaliacao
LEFT JOIN tb_disciplinasaladeaula as ds ON ds.id_saladeaula = al.id_saladeaula and md.id_disciplina = ds.id_disciplina
LEFT JOIN tb_disciplina as dc ON md.id_disciplina = dc.id_disciplina
JOIN tb_horarioaula as ha ON ha.id_horarioaula = aca.id_horarioaula
LEFT JOIN tb_avaliacaoagendamento as aa ON aa.id_avaliacaoaplicacao = aca.id_avaliacaoaplicacao and aa.id_avaliacaoconjuntoreferencia = acrf.id_avaliacaoconjuntoreferencia and aa.id_matricula = mt.id_matricula and acr.id_avaliacao = aa.id_avaliacao
JOIN tb_entidadeendereco as ee ON ee.id_entidadeendereco = aca.id_entidadeendereco
JOIN tb_entidade as et ON et.id_entidade = ee.id_entidade
LEFT JOIN tb_situacao as si ON si.id_situacao = aa.id_situacao
GO
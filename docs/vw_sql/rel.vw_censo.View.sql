/* View utilizada nos relatórios CENSO e ENADE. */

CREATE VIEW [rel].[vw_censo] AS

-- CASEs feitos usando a tabela do CENSO
SELECT
  mt.id_matricula,
  en.id_entidade,
  mt.id_projetopedagogico,
  mt.id_evolucao,
  ce.st_cpf,
  ce.st_nome,
  ce.st_nomecompletomae,
  (
    SUBSTRING(ce.dt_nascimento, 0, 3) + '/'
    + SUBSTRING(ce.dt_nascimento, 3, 2) + '/'
    + SUBSTRING(ce.dt_nascimento, 5, 4)
  ) AS st_nascimento,
  (CASE ce.nu_sexo
   WHEN 0
     THEN 'M'
   WHEN 1
     THEN 'F'
   ELSE 'Não informado'
   END) AS st_sexo,
  (CASE ce.nu_corraca
   WHEN 0
     THEN 'Não declarado'
   WHEN 1
     THEN 'Branca'
   WHEN 2
     THEN 'Preta'
   WHEN 3
     THEN 'Parda'
   WHEN 4
     THEN 'Amarela'
   WHEN 5
     THEN 'Indígena'
   ELSE 'Não informado'
   END) AS st_etnia,
  (CASE ce.nu_nacionalidade
   WHEN 1
     THEN 'Brasileira'
   WHEN 2
     THEN 'Brasileira - Nascido no exterior ou naturalizado'
   ELSE 'Estrangeira'
   END) AS st_nacionalidade,
  (CASE ce.nu_alunodeftranstsuperdotacao
   WHEN 1
     THEN 'Sim'
   ELSE 'Não'
   END) AS st_possuideficiencia,
	SUBSTRING(deficiencias.st_tiposdeficiencia, 0,
		LEN(deficiencias.st_tiposdeficiencia))
		AS st_tiposdeficiencia,
  pp.st_projetopedagogico,
  en.st_nomeentidade,
  (CASE ce.nu_situacaovinculoalunocurso
   WHEN 2
     THEN 'Cursando'
   WHEN 3
     THEN 'Trancado'
   WHEN 4
     THEN 'Desvinculado'
   WHEN 5
     THEN 'Transferido'
   WHEN 6
     THEN 'Formado'
   WHEN 7
     THEN 'Falecido'
   ELSE 'Não informado'
   END) AS st_vinculo,
  ce.nu_cargahorariototalporaluno AS st_cargahorariacurso,
  ce.nu_cargahorariointegralizadapeloaluno AS st_cargahorariaaluno,
  (CASE ce.nu_tipoescolaconclusaoensinomedio
   WHEN 0
     THEN 'Privado'
   WHEN 1
     THEN 'Público'
   WHEN 2
     THEN 'Não dispõe da informação'
   ELSE 'Não informado'
   END) AS st_tipoescola,
  (CASE
  WHEN nu_formaingselecaoenem = 1
     THEN 'ENEM'
  WHEN nu_formaingselecaovestibular = 1
     THEN 'Vestibular'
  WHEN nu_formaingselecaoavaliacaoseriada = 1
     THEN 'Avaliação Seriada'
   WHEN nu_formaingselecaoselecaosimplificada = 1
     THEN 'Seleção Simplificada'
   WHEN nu_formaingselecaoegressobili = 1
     THEN 'Egresso BI/LI'
   WHEN nu_formaingselecaoegressopecg = 1
     THEN 'PEC-G'
   WHEN nu_formaingselecaotrasnfexofficio = 1
     THEN 'Transferência Ex Officio'
   WHEN nu_formaingselecaodecisaojudicial = 1
     THEN 'Decisão judicial'
   WHEN nu_formaingselecaovagasremanescentes = 1
     THEN 'Seleção para Vagas Remanescentes'
   WHEN nu_formaingselecaovagasprogespeciais = 1
     THEN 'Seleção para Vagas de Programas Especiais'
   ELSE 'Não informado'
   END) AS st_formadeingresso,
  (CASE ce.nu_turnoaluno
    WHEN 1
      THEN 'Matutino'
    WHEN 2
      THEN 'Vespertino'
    WHEN 3
      THEN 'Noturno'
    WHEN 4
      THEN 'Integral'
    END) AS st_turno,
   endereco.st_cep AS st_ceppolo,
   ce.st_anoconclusaoensinomedio,
   ce.nu_anocenso AS st_anocenso,
   CONCAT(
     SUBSTRING (ce.nu_semestreingcurso, 0,3), '/' , SUBSTRING (ce.nu_semestreingcurso, 3,4)
   ) AS st_semestreingresso,
   ce.dt_inicioturma,
   mt.id_turma,
   CONCAT (CAST
    (CAST(ce.nu_cargahorariointegralizadapeloaluno AS decimal) / CAST (nu_cargahorariototalporaluno AS decimal)
      * 100 AS NUMERIC (32,2)
    ),
   '%') AS st_percentualcargahorariaintegralizada,
   ce.st_semestreconclusao
FROM
  dbo.vw_censo AS ce
  JOIN tb_matricula AS mt ON mt.id_matricula = ce.id_matricula
  JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
  JOIN tb_entidade AS en ON en.id_entidade = mt.id_entidadematricula
  LEFT JOIN tb_entidadeendereco AS ee ON ee.id_entidade = en.id_entidade
  LEFT JOIN tb_endereco AS endereco ON ee.id_endereco = endereco.id_endereco
  OUTER APPLY (SELECT CONCAT(
      (CASE ce.nu_tipodeficienciacegueira
       WHEN 1
         THEN 'Cegueira, '
       END),
      (CASE ce.nu_tipodeficienciabaixavisao
       WHEN 1
         THEN 'Baixa visão, '
       END),
      (CASE ce.nu_tipodeficienciasurdez
       WHEN 1
         THEN 'Surdez, '
       END),
      (CASE ce.nu_tipodeficienciaauditiva
       WHEN 1
         THEN 'Auditiva, '
       END),
      (CASE ce.nu_tipodeficienciafisica
       WHEN 1
         THEN 'Física, '
       END),
      (CASE ce.nu_tipodeficienciasurdocegueira
       WHEN 1
         THEN 'Surdocegueira, '
       END),
      (CASE ce.nu_tipodeficienciamultipla
       WHEN 1
         THEN 'Múltipla, '
       END),
      (CASE ce.nu_tipodeficienciaintelectcual
       WHEN 1
         THEN 'Intelectual, '
       END),
      (CASE ce.nu_tipodeficienciaautismo
       WHEN 1
         THEN 'Autismo, '
       END),
      (CASE ce.nu_tipodeficienciaasperger
       WHEN 1
         THEN 'Síndrome de Asperger, '
       END),
      (CASE ce.nu_tipodeficienciarett
       WHEN 1
         THEN 'Síndrome de RETT, '
       END),
      (CASE ce.nu_tipodeficienciatranstdesinfancia
       WHEN 1
         THEN 'Transtorno desintegrativo da infância, '
       END),
      (CASE ce.nu_tipodeficienciasuperdotacao
       WHEN 1
         THEN 'Altas habilidades / superdotação, '
       END)
  ) AS st_tiposdeficiencia) AS deficiencias

GO
/****** Object:  View [dbo].[vw_entidadecontatotelefone]    Script Date: 10/08/2012 17:41:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_entidadecontatotelefone] AS
select ent.id_entidade, ce.id_contatoentidade, ct.id_telefone, ct.id_tipotelefone, tt.st_tipotelefone, ct.nu_ddi, ct.nu_ddd, ct.nu_telefone, ct.st_descricao
from  tb_entidade ent, tb_contatostelefoneentidadecontato ctec, tb_contatoentidade ce, tb_contatostelefone ct, tb_tipotelefone tt
where ent.id_entidade = ce.id_entidade and
	  ce.id_contatoentidade = ctec.id_contatoentidade and
	  ct.id_telefone  = ctec.id_telefone and
	  tt.id_tipotelefone = ct.id_tipotelefone and
	  ctec.bl_padrao = 1
GO

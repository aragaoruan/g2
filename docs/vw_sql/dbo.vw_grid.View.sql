
GO
/****** Object:  View [dbo].[vw_grid]    Script Date: 10/08/2012 17:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_grid] AS
SELECT
mt.id_matricula, mt.id_projetopedagogico,
mdl.id_modulo, mdl.st_tituloexibicao AS st_modulo, mdd.id_serie ,md.id_disciplina, md.nu_aprovafinal, dc.nu_cargahoraria, mt.dt_concluinte,
ed.sg_uf, ed.st_cidade,dc.st_tituloexibicao AS st_disciplina, se.st_serie,ac.st_tituloexibicao AS st_areaconhecimento, ap.st_tituloexibicao AS st_areaagregadora, ap.id_areaconhecimento AS id_areaagregadora
,ent.st_nomeentidade, ent.st_razaosocial, md.id_evolucao
FROM tb_matricula mt
JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
JOIN tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
JOIN tb_modulo AS mdl ON mdl.id_projetopedagogico = pp.id_projetopedagogico AND mdl.bl_ativo = 1
JOIN tb_modulodisciplina AS mdd ON mdd.id_modulo = mdl.id_modulo AND mdd.id_disciplina = md.id_disciplina AND mdd.bl_ativo = 1
JOIN tb_disciplinaserienivelensino AS dsn ON mdd.id_disciplina = dsn.id_disciplina AND mdd.id_nivelensino = dsn.id_nivelensino AND mdd.id_serie = dsn.id_serie
JOIN tb_disciplina AS dc ON dc.id_disciplina = md.id_disciplina
LEFT JOIN tb_entidadeendereco AS ee ON ee.id_entidade = mt.id_entidadematriz AND ee.bl_padrao = 1
LEFT JOIN tb_endereco AS ed ON ed.id_endereco = ee.id_endereco
JOIN tb_entidade AS ent ON ent.id_entidade = mt.id_entidadematriz
JOIN tb_serie AS se ON se.id_serie = dsn.id_serie
JOIN dbo.tb_areaconhecimento AS ac ON ac.id_areaconhecimento = mdd.id_areaconhecimento
JOIN dbo.tb_evolucao AS ev ON ev.id_evolucao = md.id_evolucao
LEFT JOIN dbo.tb_areaconhecimento AS ap ON ap.id_areaconhecimento = ac.id_areaconhecimentopai AND ap.id_tipoareaconhecimento = 1
GO

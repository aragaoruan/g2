
GO
/****** Object:  View [dbo].[vw_pacotematricula]    Script Date: 10/08/2012 17:42:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_pacotematricula] AS 
SELECT em.id_projetopedagogico, em.id_matricula, rm.nu_remessa, rm.id_entidade, us.st_nomecompleto, em.id_itemdematerial FROM dbo.vw_entregamaterial AS em 
JOIN dbo.tb_matricula mt ON mt.id_matricula = em.id_matricula
JOIN dbo.tb_materialremessa AS mr ON mr.id_itemdematerial = em.id_itemdematerial
JOIN dbo.tb_remessa AS rm ON rm.id_remessa = mr.id_remessa AND rm.id_entidade = mt.id_entidadeatendimento AND rm.id_situacao = 81
JOIN dbo.tb_usuario AS us ON us.id_usuario = mt.id_usuario
WHERE em.id_situacaoentrega NOT in (61, 93,94) AND (em.bl_ativo = 1 OR em.bl_ativo IS NULL)
GO

CREATE VIEW vw_pesquisarcampanha AS

SELECT
  c.id_campanhacomercial,
  c.st_campanhacomercial,
  tc.st_tipocampanha,
  tc.id_tipocampanha,
  f.st_finalidadecampanha,
  s.st_situacao,
  c.id_situacao,
  c.id_entidade,
  c.bl_ativo
FROM tb_campanhacomercial c
  join tb_tipocampanha as tc on tc.id_tipocampanha = c.id_tipocampanha
  join tb_situacao as s on s.id_situacao = c.id_situacao
  left join tb_finalidadecampanha as f on (f.id_finalidadecampanha = c.id_finalidadecampanha)

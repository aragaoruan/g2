
GO
/****** Object:  View [dbo].[vw_contratoafiliado]    Script Date: 04/11/2013 11:17:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_contratoafiliado]
AS 
SELECT ca.id_contratoafiliado,  
  ca.st_contratoafiliado,  
  ca.id_entidadeafiliada,  
  ca.id_entidadecadastro,  
  ca.id_usuariocadastro,  
  ca.id_usuarioresponsavel,  
  ca.dt_cadastro,  
  ca.st_url,  
  ca.st_descricao,
        ca.id_situacao,
        si.st_situacao,
        ca.bl_ativo,
        ca.id_classeafiliado,
        cl.st_classeafiliado,  
  en.st_nomeentidade AS st_nomeentidadeafiliada,  
  us.st_nomecompleto AS st_nomecompletoresponsavel  
FROM tb_contratoafiliado AS ca 
LEFT JOIN tb_entidade AS en ON en.id_entidade = ca.id_entidadeafiliada
LEFT JOIN tb_usuario AS us ON us.id_usuario = ca.id_usuarioresponsavel
JOIN tb_situacao as si ON si.id_situacao = ca.id_situacao
JOIN tb_classeafiliado AS cl ON cl.id_classeafiliado = ca.id_classeafiliado
GO
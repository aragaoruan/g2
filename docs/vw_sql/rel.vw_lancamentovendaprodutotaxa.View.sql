CREATE VIEW [rel].[vw_lancamentovendaprodutotaxa] AS
  SELECT
    DISTINCT
    vd.id_entidade,
    e.st_nomeentidade,
    usat.id_usuario AS id_atendente,
    usat.st_nomecompleto AS st_atendente,
    usal.id_usuario AS id_aluno,
    usal.st_nomecompleto AS st_aluno,
    usal.st_cpf,
    ende.sg_uf,
    vd.id_venda,
    vd.dt_cadastro,
    vd.dt_confirmacao,
    vd.nu_valorliquido,
    pp.id_produto,
    pp.st_produto,
    pv.nu_valor,
    lc.dt_vencimento,
    mp.st_meiopagamento,
    lc.id_lancamento,
    lc.nu_valor AS nu_valorlancamento,
    lc.dt_quitado AS dt_baixa,
    CAST(lc.nu_quitado AS DECIMAL(30, 2)) AS nu_valorlancamentopago
  FROM
    tb_venda AS vd
    JOIN tb_usuario AS usat ON usat.id_usuario = vd.id_usuariocadastro
    JOIN tb_usuario AS usal ON usal.id_usuario = vd.id_usuario
    JOIN tb_vendaproduto AS vpp ON vpp.id_venda = vd.id_venda
    JOIN tb_produto AS pp ON pp.id_produto = vpp.id_produto
                             AND pp.id_tipoproduto = 2
-- Apenas produtos do tipo TAXA
    JOIN tb_produtovalor pv ON pv.id_produto = pp.id_produto
    JOIN tb_pessoaendereco pe ON pe.id_usuario = usal.id_usuario
    JOIN tb_endereco ende ON ende.id_endereco = pe.id_endereco
    JOIN tb_lancamentovenda AS lv ON lv.id_venda = vd.id_venda
    JOIN tb_lancamento AS lc ON lc.id_lancamento = lv.id_lancamento
    LEFT JOIN tb_meiopagamento AS mp ON mp.id_meiopagamento = lc.id_meiopagamento
    JOIN tb_entidade AS e ON vd.id_entidade = e.id_entidade
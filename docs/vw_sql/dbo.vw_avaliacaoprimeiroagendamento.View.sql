CREATE VIEW [dbo].[vw_avaliacaoprimeiroagendamento] AS
  SELECT
    DISTINCT
    mt.id_matricula,
    al.id_alocacao,
    md.id_matriculadisciplina,
    mt.id_usuario,
    us.st_nomecompleto,
    mt.id_entidadeatendimento,
    en.st_nomeentidade,
    av.id_avaliacao,
    av.id_tipoavaliacao,
    av.st_avaliacao,
    md.id_disciplina,
    dc.st_disciplina,
    acr.id_avaliacaoconjuntoreferencia
  FROM
    tb_matriculadisciplina AS md
-- Buscar somente de alunos que possuem alocação ativa
    JOIN tb_alocacao AS al ON al.id_matriculadisciplina = md.id_matriculadisciplina AND al.bl_ativo = 1
-- Somente matrículas Ativas e Cursando
    JOIN tb_matricula AS mt
      ON mt.id_matricula = md.id_matricula
         AND mt.bl_ativo = 1
         AND mt.id_evolucao = 6
-- Buscar dados do usuário
    JOIN tb_usuario AS us ON us.id_usuario = mt.id_usuario
-- Buscar dados da entidade
    JOIN tb_entidade AS en ON en.id_entidade = mt.id_entidadeatendimento AND en.id_esquemaconfiguracao = 1
-- Buscar dados da disciplina
    JOIN tb_disciplina AS dc ON dc.id_disciplina = md.id_disciplina
-- Somente Salas de Aula ativas
    JOIN tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula AND sa.bl_ativa = 1 AND sa.id_situacao = 8
-- Somente MÓDULOS Ativos
    JOIN tb_modulo AS mod
      ON mod.id_projetopedagogico = mt.id_projetopedagogico
         AND mod.bl_ativo = 1
-- Somente ModuloDisciplina Ativos
    JOIN tb_modulodisciplina AS mod_dc
      ON mod_dc.id_modulo = mod.id_modulo
         AND md.id_disciplina = mod_dc.id_disciplina
         AND mod_dc.bl_ativo = 1
-- Somente Entidades com Configuração de Primeiro Agendamento Automático
    JOIN vw_entidadeesquemaconfiguracao AS eec
      ON eec.id_entidade = mt.id_entidadematricula
         AND eec.id_itemconfiguracao = 23
         AND eec.st_valor = 1
-- Buscar o conjunto referência
    JOIN tb_avaliacaoconjuntoreferencia AS acr ON acr.id_saladeaula = al.id_saladeaula
-- Buscar avaliação do aluno
    JOIN vw_avaliacaoaluno_simples AS av
      ON av.id_matricula = md.id_matricula
         AND av.id_disciplina = md.id_disciplina
         --AND av.id_avaliacaorecupera = 0
         --AND av.bl_recuperacao = 0
         AND av.id_tipoavaliacao IN (2, 4) -- Prova Final (Recuperação), Prova Presencial
         AND av.id_evolucaodisciplina IN (13, 19)
         AND av.nu_cargahoraria != 0
-- Buscar um único agendamento do aluno em tal avaliação da disciplina que esteja ativo
    OUTER APPLY
    (
      SELECT
        TOP 1
        aa.id_avaliacaoagendamento
      FROM
        tb_avaliacaoagendamento AS aa
        JOIN tb_avalagendamentoref AS agr ON agr.id_avaliacaoagendamento = aa.id_avaliacaoagendamento
        JOIN tb_avaliacaoconjuntoreferencia AS acrf ON acrf.id_avaliacaoconjuntoreferencia = agr.id_avaliacaoconjuntoreferencia
        JOIN tb_alocacao AS al ON al.id_matriculadisciplina = md.id_matriculadisciplina AND al.id_saladeaula = acrf.id_saladeaula AND al.bl_ativo = 1
        JOIN tb_avaliacaoconjuntorelacao AS acr ON acr.id_avaliacao = aa.id_avaliacao
        JOIN tb_avaliacaoconjunto AS ac ON ac.id_avaliacaoconjunto = acr.id_avaliacaoconjunto AND ac.id_tipoprova = 2 -- Prova Por Sala de Aula
      WHERE
        aa.id_matricula = mt.id_matricula
        AND aa.id_avaliacao = av.id_avaliacao
        AND aa.id_situacao != 70 -- Agendamento Cancelado
    ) AS ag
  WHERE
    -- Aluno nao pode ter nenhum agendamento desta disciplina
    ag.id_avaliacaoagendamento IS NULL

    -- Só pode ser agendado caso não possua nota
    AND av.st_nota IS NULL

    -- GII-10183
    -- Somente salas que já foram iniciadas e que foram encerradas num período de 180 dias
    AND CONVERT(DATE, GETDATE()) BETWEEN sa.dt_abertura AND DATEADD(DAY, 180, sa.dt_encerramento)

    -- A situacao do agendamento tem que ser APTO
    AND (
      -- APTO Prova Presencial
      (
        md.id_situacaoagendamento = 120
        AND av.id_tipoavaliacao IN (1, 4)
      )
      OR
      -- APTO Prova Final (Recuperação)
      (
        md.id_situacaoagendamento = 177
        AND av.id_tipoavaliacao = 2
      )
    )

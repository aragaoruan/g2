
GO
/****** Object:  View [dbo].[vw_fundamentomatricula]    Script Date: 10/08/2012 17:41:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_fundamentomatricula] AS 
SELECT DISTINCT mt.id_matricula, fl.*  FROM dbo.tb_matricula mt
JOIN dbo.tb_projetopedagogico pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
JOIN dbo.tb_projetopedagogicoserienivelensino AS ppsn ON ppsn.id_projetopedagogico = pp.id_projetopedagogico
JOIN dbo.tb_fundamentolegal AS fl ON fl.id_nivelensino = ppsn.id_nivelensino
GO

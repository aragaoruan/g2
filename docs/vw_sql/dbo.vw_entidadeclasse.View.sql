
GO
/****** Object:  View [dbo].[vw_entidadeclasse]    Script Date: 10/08/2012 17:41:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_entidadeclasse] as
select e.id_entidade, e.id_situacao, e.nu_cnpj, e.nu_inscricaoestadual, e.st_nomeentidade, e.st_razaosocial, e.st_urlimglogo, e.bl_ativo, 
	   e.bl_acessasistema, er.id_entidadeclasse, id_entidadepai = CASE er.id_entidadepai WHEN e.id_entidade THEN NULL ELSE er.id_entidadepai END, ec.st_entidadeclasse
from tb_entidade e--, tb_entidadeclasse ec, tb_entidaderelacao er 
	LEFT JOIN tb_entidaderelacao as er ON er.id_entidade = e.id_entidade
	LEFT JOIN tb_entidadeclasse as ec ON ec.id_entidadeclasse = er.id_entidadeclasse
	--LEFT JOIN tb_entidaderelacao as er2 ON e.id_entidadecadastro = er2.id_entidadepai
where e.bl_ativo = 1
GO

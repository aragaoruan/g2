
GO
/****** Object:  View [dbo].[vw_referenciasaladeaulaprofessor]    Script Date: 10/08/2012 17:42:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_referenciasaladeaulaprofessor] AS
select uper.id_saladeaula, uper.id_usuario, uper.id_entidade, uper.id_perfil, sap.*
from tb_usuarioperfilentidadereferencia uper
JOIN tb_saladeaulaprofessor sap ON sap.id_perfilreferencia = uper.id_perfilreferencia
GO

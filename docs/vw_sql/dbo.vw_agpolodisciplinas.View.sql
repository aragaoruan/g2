SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_agpolodisciplinas]
AS
SELECT       vw.st_nomecompleto,
vw.id_usuario ,
vw.st_email,
vw.nu_telefone,
vw.nu_ddd,
vw.nu_ddi,
ppg.id_projetopedagogico,
ppg.st_projetopedagogico,
di.id_disciplina,
di.st_disciplina,
ag.id_avaliacaoagendamento,
ag.id_tipodeavaliacao,
(case when ag.id_tipodeavaliacao=0 then 'Prova Final' else 'Prova de Recuperação '+ CAST(ag.id_tipodeavaliacao as char) end) as st_tipodeavaliacao,
avp.id_avaliacaoaplicacao

FROM            dbo.tb_avaliacaoagendamento as ag 
INNER JOIN tb_avaliacaoaplicacao as avp on avp.id_avaliacaoaplicacao = ag.id_avaliacaoaplicacao 
INNER JOIN tb_avalagendamentoref as agr on agr.id_avaliacaoagendamento=ag.id_avaliacaoagendamento
INNER JOIN tb_avaliacaoconjuntoreferencia as avref on avref.id_avaliacaoconjuntoreferencia = agr.id_avaliacaoconjuntoreferencia
INNER JOIN vw_pessoa as vw on vw.id_usuario = ag.id_usuario
INNER JOIN tb_disciplinasaladeaula as tds on tds.id_saladeaula=avref.id_saladeaula
INNER JOIN tb_disciplina as di on di.id_disciplina=tds.id_disciplina
INNER JOIN tb_matricula as mt on mt.id_matricula=ag.id_matricula
INNER JOIN tb_projetopedagogico as ppg on ppg.id_projetopedagogico = mt.id_projetopedagogico
where ag.id_situacao=68

GO

GO
/****** Object:  View [dbo].[vw_notasdisciplinamatricula]    Script Date: 10/08/2012 17:42:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE VIEW [dbo].[vw_notasdisciplinamatricula] AS
SELECT id_matricula,id_disciplina,id_tipoprova,SUM(CAST(st_nota AS FLOAT)) AS nu_notatotal FROM vw_avaliacaoaluno WHERE bl_ativo = 1 GROUP BY id_disciplina, id_matricula, id_tipoprova
GO

--
--GO

/****** Object:  View [rel].[vw_consultatransacao]    Script Date: 02/25/2015 08:54:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




 CREATE VIEW [rel].[vw_consultatransacao] as
SELECT		--u.id_usuario,	
			v.id_venda,
			u.st_nomecompleto,			-- ALUNO
			tf.st_titularcartao,		-- N�O TEMOS UM TITULAR DO CART�O
			CASE WHEN l.st_ultimosdigitos IS NOT NULL THEN l.st_ultimosdigitos ELSE tf.st_ultimosdigitos END AS st_ultimosdigitos,		-- D�VIDA -- ULTIMOS DIGITOS CART�O
			cb.st_cartaobandeira,		-- BANDEIRA
			v.nu_parcelas,				-- N�MERO DE PARCELAS
			tf.dt_cadastro,				-- DATA DA TRANSA��O
			tf.un_transacaofinanceira,	-- TID
			null AS st_autorizacao,		-- D�VIDA -- ESSA CAMPO EXISTE NO G2????	
			ra.st_descricao,			-- MENSAGEM DE ERRO
			tf.nu_status,				-- D�VIDA -- ESSE STATUS � S� 1 E 2???
			tf.nu_valorcielo
FROM 
			tb_lancamentovenda AS lv
			JOIN tb_lancamento AS l ON l.id_lancamento = lv.id_lancamento
			JOIN tb_venda AS v ON v.id_venda = lv.id_venda
			JOIN tb_usuario AS u ON u.id_usuario = v.id_usuario
			JOIN tb_cartaoconfig AS cc ON cc.id_cartaoconfig = l.id_cartaoconfig
			JOIN tb_cartaobandeira AS cb ON cb.id_cartaobandeira = cc.id_cartaobandeira
			LEFT JOIN tb_transacaofinanceira AS tf ON tf.id_venda = v.id_venda
			LEFT JOIN tb_respostaautorizacao AS ra ON ra.id_respostaautorizacao = tf.id_respostaautorizacao



GO



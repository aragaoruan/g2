CREATE VIEW [rel].[vw_ecommerce] AS
SELECT
	us.st_nomecompleto,
	us.st_email,
	us.st_cpf,
	CAST(nu_ddd AS VARCHAR) + CAST(nu_telefone AS VARCHAR) AS st_telefone,
	ede.st_endereco +' Nro. '+ ede.nu_numero +' '+ ede.st_complemento AS st_endereco,
	ede.st_cep,
	ede.st_bairro,
	ede.st_complemento,
	ede.nu_numero,
	ede.st_nomemunicipio AS st_cidade,
	ede.sg_uf,
	pd.st_produto,
	pd.id_produto,
	vp.nu_desconto AS nu_descontoproduto,
	vp.nu_valorbruto AS nu_valortabela,
	vp.nu_valorliquido AS nu_valornegociado,
	vd.nu_valorliquido AS nu_valorliquidovenda,
	CONVERT(VARCHAR(30), vd.dt_confirmacao, 103) AS dt_confirmacaoshow,
	vd.dt_confirmacao,
	--mp.st_meiopagamento, 
	mp.id_meiopagamento,
	pd.id_tipoproduto,
	vd.id_venda,
	vd.id_evolucao,
	ev.st_evolucao,
	ve.id_entidade,
	CONVERT(VARCHAR(30), vd.dt_cadastro, 103) AS dt_cadastroshow,
	vd.dt_cadastro,
	pdc.st_produto AS st_combo,
	pdc.id_produto AS id_produtocombo,
	ve.st_nomeentidade,
	te.id_entidade AS id_entidadepai,
	vd.st_observacao,
	us.id_usuario AS cod_aluno,
	vd.id_cupom,
	us_atendente.st_nomecompleto
	AS st_nomeatendente,
	CONVERT(VARCHAR(30), us.dt_nascimento, 103) AS dt_nascimento,
	----- 
	--(
	--  SELECT COALESCE(cti.st_categoria + ', ', '')
	--  FROM
	--    dbo.tb_categoriaproduto AS cpi
	--    JOIN dbo.tb_categoria                                     AS cti ON cti.id_categoria = cpi.id_categoria
	--  WHERE
	--    cpi.id_produto = pd.id_produto
	--  FOR XML PATH ('')
	--) 
	NULL AS st_categorias,
	cp.id_categoria,
	cu.nu_desconto
	AS nu_valorcupom,
	cu.st_codigocupom,
	td.st_tipodesconto,
	cc.st_campanhacomercial,
	lvc.nu_parcelas,
	--(
	--  SELECT COALESCE(
	--    ac2.st_areaconhecimento + ', ',
	--    ''
	--)
	--  FROM
	--    dbo.tb_areaconhecimento ac2
	--    JOIN dbo.tb_areaprojetopedagogico app2
	--    JOIN dbo.tb_produtoprojetopedagogico AS ppp ON ppp.id_projetopedagogico = app2.id_projetopedagogico
	--      ON ac2.id_areaconhecimento = app2.id_areaconhecimento
	--  WHERE
	--    ppp.id_produto = vp.id_produto
	--  FOR XML PATH ('')
	--) 
	NULL
	AS st_areaconhecimento,
	CASE cu.nu_desconto
		WHEN 100.00 THEN 'Gratuito'
		ELSE mp.st_meiopagamento
	END AS 'st_meiopagamento',
	st_status =
				CASE
					WHEN vd.id_evolucao = 10 THEN 'Confirmada'
					WHEN vd.id_evolucao = 9 THEN 'Aguardando Recebimento'
					WHEN vd.id_evolucao = 7 AND
						ts.id_transacaofinanceira IS NOT NULL THEN 'Cartão Recusado'
					WHEN vd.id_evolucao = 7 AND
						ts.id_transacaofinanceira IS NULL THEN 'Abandonada'
					ELSE ev.st_evolucao
				END


FROM tb_entidade AS te
JOIN dbo.vw_entidaderecursivaid AS ve
	ON ve.nu_entidadepai = te.id_entidade
JOIN dbo.tb_venda AS vd
	ON vd.id_entidade = ve.id_entidade
LEFT JOIN dbo.tb_usuario AS us_atendente
	ON vd.id_atendente = us_atendente.id_usuario
JOIN dbo.tb_evolucao AS ev
	ON ev.id_evolucao = vd.id_evolucao
JOIN dbo.vw_pessoa AS us
	ON us.id_usuario = vd.id_usuario
	AND us.id_entidade = vd.id_entidade
LEFT JOIN dbo.tb_municipio AS mn
	ON mn.id_municipio = us.id_municipio
JOIN dbo.tb_vendaproduto AS vp
	ON vd.id_venda = vp.id_venda
JOIN dbo.tb_produto AS pd
	ON pd.id_produto = vp.id_produto
LEFT JOIN tb_cupom AS cu
	ON cu.id_cupom = vd.id_cupom
LEFT JOIN dbo.tb_campanhacomercial AS cc
	ON cu.id_campanhacomercial = cc.id_campanhacomercial
LEFT JOIN dbo.tb_tipodesconto AS td
	ON td.id_tipodesconto = cu.id_tipodesconto
LEFT JOIN dbo.tb_categoriaproduto AS cp
	ON cp.id_produto = pd.id_produto
LEFT JOIN dbo.tb_lancamentovenda AS lv
	ON lv.id_venda = vd.id_venda
	AND bl_entrada = 1
LEFT JOIN dbo.tb_lancamento AS lc
	ON lc.id_lancamento
	= lv.id_lancamento
LEFT JOIN dbo.tb_meiopagamento AS mp
	ON mp.id_meiopagamento = lc.id_meiopagamento
LEFT JOIN tb_produto AS pdc
	ON pdc.id_produto = vp.id_produtocombo
OUTER APPLY (SELECT
	COUNT(lvo.id_lancamento) AS nu_parcelas
FROM dbo.tb_lancamentovenda AS lvo
WHERE lvo.id_venda = vd.id_venda) AS lvc
OUTER APPLY (SELECT TOP 1
	tf.id_transacaofinanceira
FROM dbo.tb_transacaofinanceira AS tf
WHERE tf.id_venda = vd.id_venda
AND tf.nu_status = 2) AS ts
OUTER APPLY (SELECT
	ed.*, mn.st_nomemunicipio
FROM tb_endereco AS ed
JOIN tb_municipio AS mn
	ON mn.id_municipio = ed.id_municipio
WHERE ed.id_endereco = vd.id_enderecoentrega) AS ede
WHERE vd.bl_ativo = 1 UNION SELECT
	us.st_nomecompleto,
	us.st_email,
	us.st_cpf,
	CAST(nu_ddd AS VARCHAR) + CAST(nu_telefone AS VARCHAR) AS st_telefone,
    	ede.st_endereco +' Nro. '+ ede.nu_numero +' '+ ede.st_complemento AS st_endereco,
	ede.st_cep,
	ede.st_bairro,
	ede.st_complemento,
	ede.nu_numero,
	ede.st_nomemunicipio AS st_cidade,
	ede.sg_uf,
	pd.st_produto,
	pd.id_produto,
	vp.nu_desconto AS nu_descontoproduto,
	vp.nu_valorbruto AS nu_valortabela,
	vp.nu_valorliquido AS nu_valornegociado,
	vd.nu_valorliquido AS nu_valorliquidovenda,
	CONVERT(VARCHAR(30), vd.dt_confirmacao, 103) AS dt_confirmacaoshow,
	vd.dt_confirmacao,
	--mp.st_meiopagamento, 
	mp.id_meiopagamento,
	pd.id_tipoproduto,
	vd.id_venda,
	vd.id_evolucao,
	ev.st_evolucao,
	te.id_entidade,
	CONVERT(VARCHAR(30), vd.dt_cadastro, 103) AS dt_cadastroshow,
	vd.dt_cadastro,
	pdc.st_produto AS st_combo,
	pdc.id_produto AS id_produtocombo,
	te.st_nomeentidade,
	te.id_entidade AS id_entidadepai,
	vd.st_observacao,
	us.id_usuario AS cod_aluno,
	vd.id_cupom,
	us_atendente.st_nomecompleto
	AS st_nomeatendente,
	CONVERT(VARCHAR(30), us.dt_nascimento, 103) AS dt_nascimento,
	----- 
	(SELECT
		COALESCE(cti.st_categoria + ', ', '')
	FROM dbo.tb_categoriaproduto AS cpi
	JOIN dbo.tb_categoria AS cti
		ON cti.id_categoria = cpi.id_categoria
	WHERE cpi.id_produto = pd.id_produto
	FOR XML PATH (''))
	AS st_categorias,
	cp.id_categoria,
	cu.nu_desconto
	AS nu_valorcupom,
	cu.st_codigocupom,
	td.st_tipodesconto,
	cc.st_campanhacomercial,
	lvc.nu_parcelas,
	--(
	--  SELECT COALESCE(
	--    ac2.st_areaconhecimento + ', ',
	--    ''
	--)
	--  FROM
	--    dbo.tb_areaconhecimento ac2
	--    JOIN dbo.tb_areaprojetopedagogico app2
	--    JOIN dbo.tb_produtoprojetopedagogico AS ppp ON ppp.id_projetopedagogico = app2.id_projetopedagogico
	--      ON ac2.id_areaconhecimento = app2.id_areaconhecimento
	--  WHERE
	--    ppp.id_produto = vp.id_produto
	--  FOR XML PATH ('')
	--) 
	NULL
	AS st_areaconhecimento,
	CASE cu.nu_desconto
		WHEN 100.00 THEN 'Gratuito'
		ELSE mp.st_meiopagamento
	END AS 'st_meiopagamento',
	st_status =
				CASE
					WHEN vd.id_evolucao = 10 THEN 'Confirmada'
					WHEN vd.id_evolucao = 9 THEN 'Aguardando Recebimento'
					WHEN vd.id_evolucao = 7 AND
						ts.id_transacaofinanceira IS NOT NULL THEN 'Cartão Recusado'
					WHEN vd.id_evolucao = 7 AND
						ts.id_transacaofinanceira IS NULL THEN 'Abandonada'
					ELSE ev.st_evolucao
				END


FROM tb_entidade AS te
-- JOIN dbo.vw_entidaderecursivaid AS ve ON ve.nu_entidadepai = te.id_entidade OR ve.id_entidade = te.id_entidade
JOIN dbo.tb_venda AS vd
	ON vd.id_entidade = te.id_entidade
LEFT JOIN dbo.tb_usuario AS us_atendente
	ON vd.id_atendente = us_atendente.id_usuario
JOIN dbo.tb_evolucao AS ev
	ON ev.id_evolucao = vd.id_evolucao
JOIN dbo.vw_pessoa AS us
	ON us.id_usuario = vd.id_usuario
	AND us.id_entidade = vd.id_entidade
LEFT JOIN dbo.tb_municipio AS mn
	ON mn.id_municipio = us.id_municipio
JOIN dbo.tb_vendaproduto AS vp
	ON vd.id_venda = vp.id_venda
JOIN dbo.tb_produto AS pd
	ON pd.id_produto = vp.id_produto
LEFT JOIN tb_cupom AS cu
	ON cu.id_cupom = vd.id_cupom
LEFT JOIN dbo.tb_campanhacomercial AS cc
	ON cu.id_campanhacomercial = cc.id_campanhacomercial
LEFT JOIN dbo.tb_tipodesconto AS td
	ON td.id_tipodesconto = cu.id_tipodesconto
LEFT JOIN dbo.tb_categoriaproduto AS cp
	ON cp.id_produto = pd.id_produto
LEFT JOIN dbo.tb_lancamentovenda AS lv
	ON lv.id_venda = vd.id_venda
	AND bl_entrada = 1
LEFT JOIN dbo.tb_lancamento AS lc
	ON lc.id_lancamento
	= lv.id_lancamento
LEFT JOIN dbo.tb_meiopagamento AS mp
	ON mp.id_meiopagamento = lc.id_meiopagamento
LEFT JOIN tb_produto AS pdc
	ON pdc.id_produto = vp.id_produtocombo
OUTER APPLY (SELECT
	COUNT(lvo.id_lancamento) AS nu_parcelas
FROM dbo.tb_lancamentovenda AS lvo
WHERE lvo.id_venda = vd.id_venda) AS lvc
OUTER APPLY (SELECT TOP 1
	tf.id_transacaofinanceira
FROM dbo.tb_transacaofinanceira AS tf
WHERE tf.id_venda = vd.id_venda
AND tf.nu_status = 2) AS ts
OUTER APPLY (SELECT
	ed.*, mn.st_nomemunicipio
FROM tb_endereco AS ed
JOIN tb_municipio AS mn
	ON mn.id_municipio = ed.id_municipio
WHERE ed.id_endereco = vd.id_enderecoentrega) AS ede
WHERE vd.bl_ativo = 1





GO



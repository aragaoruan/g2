CREATE VIEW [dbo].[vw_entregadocumento] AS

SELECT ct.id_usuario AS id_usuarioaluno,dc.id_documentos, dc.st_documentos,dc.id_tipodocumento, du.id_documentosutilizacao ,us.id_usuario, us.st_nomecompleto ,
NULL AS id_contratoresponsavel,ed.id_entregadocumentos, ed.id_situacao,dp.id_projetopedagogico, ct.id_entidade,
st_situacao = CASE ed.id_situacao
WHEN ed.id_situacao THEN st.st_situacao
ELSE 'Pendente'
END,
dc.nu_ordenacao
FROM tb_contrato AS ct
JOIN tb_vendaproduto AS vp ON vp.id_venda = ct.id_venda
JOIN tb_entidade AS ent ON ent.id_entidade = ct.id_entidade
JOIN tb_produtoprojetopedagogico AS ppp ON ppp.id_produto = vp.id_produto
JOIN tb_documentosprojetopedagogico AS dp ON ppp.id_projetopedagogico = dp.id_projetopedagogico
JOIN tb_documentos AS dc ON dc.id_tipodocumento = 1 AND dp.id_documentos = dc.id_documentos
AND (dc.id_entidade = ct.id_entidade OR (dc.id_entidade = (CASE WHEN ent.id_entidadecadastro IS NOT NULL THEN ent.id_entidadecadastro ELSE ct.id_entidade END ))) AND dc.bl_ativo = 1
JOIN tb_documentosutilizacaorelacao AS du ON du.id_documentos = dc.id_documentos
JOIN tb_usuario AS us ON us.id_usuario = ct.id_usuario AND du.id_documentosutilizacao = 1
LEFT JOIN tb_entregadocumentos AS ed ON ed.id_documentos = dc.id_documentos AND ed.id_usuario = us.id_usuario
LEFT JOIN tb_situacao AS st ON ed.id_situacao = st.id_situacao
WHERE ct.bl_ativo = 1

UNION

SELECT ct.id_usuario AS id_usuarioaluno,dc.id_documentos, dc.st_documentos,dc.id_tipodocumento, du.id_documentosutilizacao ,us.id_usuario, us.st_nomecompleto ,
NULL AS id_contratoresponsavel,ed.id_entregadocumentos, ed.id_situacao,NULL AS id_projetopedagogico, ct.id_entidade,
st_situacao = CASE ed.id_situacao
WHEN ed.id_situacao THEN st.st_situacao
ELSE 'Pendente'
END,
dc.nu_ordenacao
FROM tb_contrato AS ct
JOIN tb_entidade AS ent ON ent.id_entidade = ct.id_entidade
JOIN tb_documentos AS dc ON (dc.id_entidade = ct.id_entidade OR (dc.id_entidade = (CASE WHEN ent.id_entidadecadastro IS NOT NULL THEN ent.id_entidadecadastro ELSE ct.id_entidade END )))
AND dc.id_tipodocumento = 2 AND dc.bl_ativo = 1
JOIN tb_documentosutilizacaorelacao AS du ON du.id_documentos = dc.id_documentos
JOIN tb_usuario AS us ON us.id_usuario = ct.id_usuario AND du.id_documentosutilizacao = 1
LEFT JOIN tb_entregadocumentos AS ed ON ed.id_documentos = dc.id_documentos AND ed.id_usuario = us.id_usuario
LEFT JOIN tb_situacao AS st ON ed.id_situacao = st.id_situacao

UNION

SELECT ct.id_usuario AS id_usuarioaluno, dc.id_documentos, dc.st_documentos,dc.id_tipodocumento, du.id_documentosutilizacao ,us.id_usuario, us.st_nomecompleto , cr.id_contratoresponsavel, 
ed.id_entregadocumentos, ed.id_situacao, NULL AS id_projetopedagogico, dc.id_entidade,
st_situacao = CASE ed.id_situacao
WHEN ed.id_situacao THEN st.st_situacao
ELSE 'Pendente'
END,
dc.nu_ordenacao
FROM tb_documentos AS dc
JOIN tb_entidade ent ON (dc.id_entidade = ent.id_entidade OR (dc.id_entidade = (CASE WHEN ent.id_entidadecadastro IS NOT NULL THEN ent.id_entidadecadastro ELSE ent.id_entidade END )))
JOIN tb_contrato AS ct ON dc.id_entidade = ct.id_entidade AND dc.bl_ativo = 1
JOIN tb_documentosutilizacaorelacao AS du ON du.id_documentos = dc.id_documentos
JOIN tb_contratoresponsavel AS cr ON ct.id_contrato = cr.id_contrato AND cr.bl_ativo = 1 AND cr.id_tipocontratoresponsavel = 1
JOIN tb_usuario AS us ON us.id_usuario = cr.id_usuario AND du.id_documentosutilizacao = 2 AND ct.id_usuario != cr.id_usuario
LEFT JOIN tb_entregadocumentos AS ed ON ed.id_documentos = dc.id_documentos AND ed.id_contratoresponsavel = cr.id_contratoresponsavel
LEFT JOIN tb_situacao AS st ON ed.id_situacao = st.id_situacao
LEFT JOIN tb_documentosprojetopedagogico AS dp ON dp.id_projetopedagogico IS NULL
WHERE dc.id_tipodocumento = 2
GO
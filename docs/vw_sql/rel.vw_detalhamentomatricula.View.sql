CREATE VIEW [rel].[vw_detalhamentomatricula] AS
  SELECT
    mt.id_matricula,
    mt.id_entidadematricula AS id_entidade,
    mt.dt_cadastro,
    pp.id_projetopedagogico,
    pp.st_projetopedagogico,
    us.st_nomecompleto,
    us.st_cpf,
    ev.st_evolucao,
    dc_projeto.nu_total AS st_totaldiscprojeto,
    dc_cursando.nu_total AS st_totaldisccursando,
    dc_aprovadas.nu_total AS st_totaldiscaprovadas,
    dc_reprovadas.nu_total AS st_totaldiscreprovadas,
    dc_acursar.nu_total AS st_totaldiscacursar,
    dc_isentas.nu_total AS st_totaldiscisentas,
    dc_trancadas.nu_total AS st_totaldisctrancadas,
    FORMAT(mt.dt_ultimoacesso, 'dd/MM/yyyy HH:mm') AS st_ultimoacesso,
    ult_venda.id_venda,
    (CASE
     WHEN ult_venda.bl_renovacao = 1
       THEN ult_venda.dt_cadastro
     ELSE NULL
     END) AS dt_renovacao
  FROM
    tb_matricula AS mt
    JOIN tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
    JOIN tb_usuario AS us ON us.id_usuario = mt.id_usuario
    JOIN tb_evolucao AS ev ON ev.id_evolucao = mt.id_evolucao
-- Buscar última venda
    OUTER APPLY (
                  SELECT
                    TOP 1
                    ve.id_venda,
                    ve.dt_cadastro,
                    ve.bl_renovacao
                  FROM
                    tb_vendaproduto AS vp
                    JOIN tb_venda AS ve ON ve.id_venda = vp.id_venda
                  WHERE
                    vp.id_matricula = mt.id_matricula
                  ORDER BY
                    ve.id_venda DESC
                ) AS ult_venda
    OUTER APPLY (
                  SELECT
                    COUNT(DISTINCT mdc.id_disciplina) AS nu_total
                  FROM
                    tb_modulo AS md
                    JOIN tb_modulodisciplina AS mdc
                      ON mdc.id_modulo = md.id_modulo AND mdc.bl_ativo = 1
                    JOIN tb_disciplina AS dc
                      ON dc.id_disciplina = mdc.id_disciplina AND dc.bl_ativa = 1
                  WHERE
                    md.bl_ativo = 1
                    AND md.id_projetopedagogico = mt.id_projetopedagogico
                ) AS dc_projeto
    OUTER APPLY (
                  SELECT
                    COUNT(id_matriculadisciplina) AS nu_total
                  FROM
                    tb_matriculadisciplina
                  WHERE
                    id_matricula = mt.id_matricula
                    AND id_situacao = 54
                    AND id_evolucao = 13
                ) AS dc_cursando
    OUTER APPLY (
                  SELECT
                    COUNT(id_matriculadisciplina) AS nu_total
                  FROM
                    tb_matriculadisciplina
                  WHERE
                    id_matricula = mt.id_matricula
                    AND id_situacao = 54
                    AND id_evolucao = 12
                ) AS dc_aprovadas
    OUTER APPLY (
                  SELECT
                    COUNT(id_matriculadisciplina) AS nu_total
                  FROM
                    tb_matriculadisciplina
                  WHERE
                    id_matricula = mt.id_matricula
                    AND id_situacao = 54
                    AND id_evolucao = 19
                ) AS dc_reprovadas
    OUTER APPLY (
                  SELECT
                    COUNT(id_matriculadisciplina) AS nu_total
                  FROM
                    tb_matriculadisciplina
                  WHERE
                    id_matricula = mt.id_matricula
                    AND id_situacao = 53
                    AND id_evolucao = 11
                ) AS dc_acursar
    OUTER APPLY (
                  SELECT
                    COUNT(id_matriculadisciplina) AS nu_total
                  FROM
                    tb_matriculadisciplina
                  WHERE
                    id_matricula = mt.id_matricula
                    AND id_situacao = 11
                    AND id_evolucao = 19
                ) AS dc_isentas
    OUTER APPLY (
                  SELECT
                    COUNT(id_matriculadisciplina) AS nu_total
                  FROM
                    tb_matriculadisciplina
                  WHERE
                    id_matricula = mt.id_matricula
                    AND id_situacao = 53
                    AND id_evolucao = 70
                ) AS dc_trancadas
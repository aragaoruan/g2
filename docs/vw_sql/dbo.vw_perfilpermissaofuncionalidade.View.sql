
GO
/****** Object:  View [dbo].[vw_perfilpermissaofuncionalidade]    Script Date: 10/08/2012 17:42:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_perfilpermissaofuncionalidade] AS
select p.id_perfil,p.st_nomeperfil, p.bl_ativo as bl_ativoperfil, 
           p.id_situacao, s.st_situacao, s.st_descricaosituacao, pf.id_permissao, per.st_nomepermissao, 
           per.st_descricao, per.bl_ativo as bl_ativopermissao,
           f.id_funcionalidade, f.id_funcionalidadepai, f.st_classeflex, f.st_funcionalidade, f.bl_ativo as bl_ativofuncionalidade,
           CASE WHEN ppf.id_permissao IS NOT NULL THEN 1 ELSE 0 END as bl_selecionado
from tb_perfil as p
JOIN tb_perfilfuncionalidade as pff ON pff.id_perfil = p.id_perfil
JOIN tb_permissaofuncionalidade as pf ON pf.id_funcionalidade = pff.id_funcionalidade
JOIN tb_funcionalidade as f ON f.id_funcionalidade = pff.id_funcionalidade
JOIN tb_permissao as per ON per.id_permissao = pf.id_permissao
JOIN tb_situacao as s ON s.id_situacao = f.id_situacao
JOIN tb_perfilpermissaofuncionalidade as ppf ON ppf.id_perfil = p.id_perfil and ppf.id_permissao = pf.id_permissao and pf.id_funcionalidade = ppf.id_funcionalidade
GO

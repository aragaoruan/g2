
GO

/****** Object:  View [dbo].[vw_vitrine]    Script Date: 02/15/2013 11:38:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 CREATE VIEW [dbo].[vw_vitrine] AS
SELECT ca.id_contratoafiliado,
		ca.st_contratoafiliado,
		ca.id_classeafiliado,
		ca.id_entidadeafiliada,
		ca.id_entidadecadastro,
		ca.id_usuarioresponsavel,
		ca.dt_cadastro,
		ca.st_url,
		ca.st_descricao,
		cla.id_tipopessoa,
		cla.id_situacao,
		v.id_vitrine,
		v.st_vitrine,
		v.bl_ativo,
		v.id_modelovitrine,
		e.st_nomeentidade,
		u.st_nomecompleto
FROM tb_contratoafiliado AS ca
		INNER JOIN tb_classeafiliado AS cla ON ca.id_classeafiliado  = cla.id_classeafiliado
		LEFT  JOIN tb_entidade       AS e   ON e.id_entidade         = ca.id_entidadeafiliada
		LEFT  JOIN tb_usuario        AS u   ON u.id_usuario          = ca.id_usuarioresponsavel
		left  JOIN tb_vitrine        AS v   ON v.id_contratoafiliado = ca.id_contratoafiliado
GO
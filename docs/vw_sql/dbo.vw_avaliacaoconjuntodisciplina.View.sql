SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


Create VIEW [dbo].[vw_avaliacaoconjuntodisciplina]
AS
select distinct avc.id_avaliacaoconjunto,
avc.st_avaliacaoconjunto,
avc.id_tipoprova,
avc.id_entidade,
d.id_disciplina,
d.st_disciplina,
d.id_tipodisciplina,
td.st_tipodisciplina
 from tb_disciplina as d
join tb_disciplinasaladeaula as sd on d.id_disciplina=sd.id_disciplina
join tb_saladeaula as sal on sd.id_saladeaula=sal.id_saladeaula
join tb_avaliacaoconjuntodisciplina as avd on d.id_tipodisciplina=avd.id_tipodisciplina
join tb_avaliacaoconjunto as avc on avc.id_avaliacaoconjunto=avd.id_avaliacaoconjunto
join tb_tipodisciplina as td on td.id_tipodisciplina=avd.id_tipodisciplina
--where d.id_disciplina=564


GO



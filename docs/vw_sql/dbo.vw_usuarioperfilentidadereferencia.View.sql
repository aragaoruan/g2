 CREATE VIEW [dbo].[vw_usuarioperfilentidadereferencia]
AS
SELECT 
		upr.id_perfilreferencia
		,upr.id_usuario
		,upr.id_entidade
		,upr.id_perfil
		,upr.id_areaconhecimento
		,upr.id_projetopedagogico
        ,pp.st_projetopedagogico
		,upr.id_saladeaula
		,upr.id_disciplina
		,upr.id_livro
		,upr.bl_ativo
		,upr.bl_titular
		,upr.bl_autor
		,u.st_nomecompleto,
		sa.st_saladeaula,
		pr.id_perfilreferenciaintegracao
		,upr.nu_porcentagem
		, p.id_perfilpedagogico
	FROM dbo.tb_usuarioperfilentidadereferencia AS upr
	INNER JOIN dbo.tb_usuario AS u ON u.id_usuario = upr.id_usuario
	INNER JOIN dbo.tb_perfil as p ON p.id_perfil = upr.id_perfil
    LEFT JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = upr.id_projetopedagogico
    LEFT JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = upr.id_saladeaula
    LEFT JOIN dbo.tb_perfilreferenciaintegracao AS pr ON pr.id_perfilreferencia = upr.id_perfilreferencia AND upr.id_saladeaula is NOT null
	WHERE upr.bl_ativo = 1



GO



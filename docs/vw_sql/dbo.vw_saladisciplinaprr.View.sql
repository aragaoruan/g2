
/****** Object:  View [dbo].[vw_saladisciplinaprr]    Script Date: 14/01/2014 17:14:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 CREATE VIEW [dbo].[vw_saladisciplinaprr] as
select aps.id_saladeaula, sa.st_saladeaula, sa.nu_maxalunos,aps.id_projetopedagogico, pp.id_trilha,mt.id_matricula, ds.id_disciplina,dc.st_disciplina , md.id_matriculadisciplina, mt.id_usuario,al.id_alocacao, se.id_entidade
		, ( SELECT COUNT(alc.id_alocacao)FROM tb_alocacao alc, dbo.tb_matriculadisciplina mdc where alc.id_saladeaula = sa.id_saladeaula and mdc.id_matriculadisciplina = alc.id_matriculadisciplina AND mdc.id_evolucao = 13)AS nu_alocados
		, sa.dt_abertura
		, sa.dt_encerramento
		, sa.id_categoriasala
		, pp.st_projetopedagogico, wp.id_produto, wp.nu_valor, pv.id_vendaproduto, pv.dt_entrega, pv.id_venda, oc.id_ocorrencia
from tb_matricula as mt
JOIN tb_projetopedagogico as pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_areaprojetosala as aps ON aps.id_projetopedagogico =  mt.id_projetopedagogico
JOIN tb_saladeaula as sa ON sa.id_saladeaula = aps.id_saladeaula  AND sa.id_situacao = 8 AND sa.bl_ativa = 1 AND sa.id_categoriasala = 2
JOIN tb_disciplinasaladeaula as ds ON ds.id_saladeaula = sa.id_saladeaula
JOIN dbo.tb_disciplina AS dc ON dc.id_disciplina = ds.id_disciplina
JOIN tb_matriculadisciplina as md ON md.id_matricula = mt.id_matricula AND ds.id_disciplina = md.id_disciplina
JOIN tb_saladeaulaentidade as se ON se.id_saladeaula = sa.id_saladeaula and se.id_entidade = mt.id_entidadeatendimento
LEFT JOIN dbo.tb_produtoprr AS ppr ON ppr.id_saladeaula = sa.id_saladeaula
LEFT JOIN dbo.vw_produto AS wp ON wp.id_produto = ppr.id_produto
LEFT JOIN tb_alocacao al ON al.id_matriculadisciplina = md.id_matriculadisciplina AND al.id_saladeaula = sa.id_saladeaula AND al.bl_ativo = '1' AND al.id_alocacao 
NOT IN (SELECT ea.id_alocacao FROM dbo.tb_encerramentoalocacao AS ea WHERE ea.id_alocacao = al.id_alocacao)
LEFT JOIN dbo.tb_ocorrencia AS oc ON oc.id_matricula = mt.id_matricula
LEFT JOIN tb_venda AS vd ON  oc.id_ocorrencia = vd.id_ocorrencia
LEFT JOIN tb_vendaproduto AS pv ON  vd.id_venda = pv.id_venda and  pv.id_produto = wp.id_produto
WHERE (sa.dt_encerramento >= CAST(GETDATE() AS DATE)) OR (dt_encerramento IS NULL)OR (al.id_saladeaula = sa.id_saladeaula)



GO



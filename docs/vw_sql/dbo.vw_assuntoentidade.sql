CREATE VIEW [dbo].[vw_assuntoentidade]
AS

SELECT
	tb_entidade.st_nomeentidade,
	ap.st_assuntoco AS st_assuntopai,
	aco.id_assuntocopai AS id_assuntopai,
	aco.st_assuntoco AS st_subassunto,
	aco.id_assuntoco AS id_subassunto,
	aco.id_assuntoco as id_assunto,
	ae.id_entidade AS id_entidade,
	aco.id_entidadecadastro AS id_entidadecadastro
FROM dbo.tb_assuntoco AS aco
JOIN dbo.tb_assuntoco AS ap
	ON ap.id_assuntoco = aco.id_assuntocopai
LEFT JOIN dbo.tb_assuntoentidadeco AS ae
	ON ae.id_assuntoco = aco.id_assuntoco
LEFT JOIN tb_entidade
	ON tb_entidade.id_entidade = ae.id_entidade

	UNION
	
	SELECT
	tb_entidade.st_nomeentidade,
	aco.st_assuntoco AS st_assuntopai,
	aco.id_assuntoco AS id_assuntopai,
	null AS st_subassunto,
	null AS id_subassunto,
	aco.id_assuntoco as id_assunto,
	ae.id_entidade AS id_entidade,
	aco.id_entidadecadastro AS id_entidadecadastro
FROM dbo.tb_assuntoco AS aco
LEFT JOIN dbo.tb_assuntoentidadeco AS ae
	ON ae.id_assuntoco = aco.id_assuntoco
LEFT JOIN tb_entidade
	ON tb_entidade.id_entidade = ae.id_entidade
WHERE aco.id_assuntocopai IS null

GO
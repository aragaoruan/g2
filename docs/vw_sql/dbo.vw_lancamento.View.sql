 CREATE VIEW vw_lancamento AS
SELECT
    lv.bl_entrada,
    lv.id_venda,
    lv.id_lancamento,
    l.nu_valor,
    l.nu_vencimento,
    l.id_tipolancamento,
    tl.st_tipolancamento,
    l.dt_cadastro ,
    l.id_meiopagamento,
    mp.st_meiopagamento,
    l.id_usuariolancamento,
    l.id_entidade,
    elancamento.st_nomeentidade,
    l.bl_quitado ,
    l.dt_vencimento,
    l.dt_quitado,
    l.dt_emissao,
    l.dt_prevquitado,
    l.st_emissor,
    l.st_coddocumento,
    l.id_cartaoconfig ,
    CASE
        WHEN l.bl_quitado = 1
        THEN 'Pago'
        WHEN l.dt_vencimento < GETDATE()
        THEN 'Pendente'
        WHEN l.dt_vencimento > GETDATE()
        AND l.bl_quitado != 1
        THEN 'Atrasado'
        ELSE 'null'
    END AS st_quitado ,
    CASE
        WHEN lv.bl_entrada = 1
        THEN 'Entrada'
        ELSE CONVERT(VARCHAR(10),lv.nu_ordem-1)
    END AS st_nuentrada
FROM
    tb_lancamentovenda lv
INNER JOIN tb_lancamento AS l ON l.id_lancamento = lv.id_lancamento AND l.bl_ativo = 1
INNER JOIN tb_meiopagamento AS mp ON mp.id_meiopagamento = l.id_meiopagamento
INNER JOIN tb_tipolancamento AS tl ON tl.id_tipolancamento = l.id_tipolancamento
INNER JOIN tb_entidade AS elancamento ON elancamento.id_entidade = l.id_entidade


GO
/****** Object:  View [dbo].[vw_pagamentocartao]    Script Date: 10/08/2012 17:42:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_pagamentocartao] AS 
SELECT SUM(nu_valor) AS nu_valorcartao, COUNT(lc.id_lancamento) AS nu_vezes, id_venda FROM dbo.tb_lancamentovenda AS lv
JOIN dbo.tb_lancamento AS lc ON lc.id_lancamento = lv.id_lancamento AND id_meiopagamento = 1 AND lc.dt_prevquitado IS NULL GROUP BY id_venda
GO

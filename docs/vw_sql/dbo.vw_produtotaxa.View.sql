
GO
/****** Object:  View [dbo].[vw_produtotaxa]    Script Date: 10/08/2012 17:42:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_produtotaxa] AS 
SELECT pd.id_produto, pd.st_produto, pd.bl_ativo
, pd.id_tipoproduto, tp.st_tipoproduto, tp.st_tabelarelacao
, pt.id_taxa, tx.st_taxa
, vp.id_venda, vp.id_matricula, vd.id_entidade
FROM 
tb_produto AS pd
JOIN dbo.tb_produtotaxa AS pt ON pt.id_produto = pd.id_produto
JOIN dbo.tb_taxa AS tx ON tx.id_taxa = pt.id_taxa
JOIN dbo.tb_tipoproduto AS tp ON tp.id_tipoproduto = pd.id_tipoproduto 
LEFT JOIN dbo.tb_vendaproduto  AS vp ON vp.id_produto = pd.id_produto
LEFT JOIN tb_venda AS vd ON vd.id_venda = vp.id_venda
WHERE pd.bl_ativo = 1
GO

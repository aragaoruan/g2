
CREATE VIEW [dbo].[vw_emissoesautorizacaoentrada] AS 
SELECT DISTINCT
	us.id_usuario,
	us.st_nomecompleto,
	us.st_cpf,
	COUNT (ae.id_autorizacaoentrada) AS nu_totalemissao,
	us.id_entidade
FROM vw_pessoa AS us
left join	tb_autorizacaoentrada AS ae on us.id_usuario = ae.id_usuario and ae.bl_ativo = 1
GROUP BY
	us.id_usuario,
	us.st_nomecompleto,
	us.st_cpf,
	us.id_entidade
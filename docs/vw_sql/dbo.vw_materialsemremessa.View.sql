
GO
/****** Object:  View [dbo].[vw_materialsemremessa]    Script Date: 10/08/2012 17:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_materialsemremessa] as
--view com os materias que ainda não tem remessa ativa de um projeto pedagogico--
SELECT mp.id_projetopedagogico, mp.id_itemdematerial,im.st_itemdematerial, dc.id_disciplina,dc.st_disciplina
FROM dbo.tb_materialprojeto mp
JOIN dbo.tb_itemdematerial im ON mp.id_itemdematerial = im.id_itemdematerial
JOIN dbo.tb_modulo md ON md.id_projetopedagogico = mp.id_projetopedagogico
JOIN dbo.tb_modulodisciplina mdc ON mdc.id_modulo = md.id_modulo
JOIN dbo.tb_itemdematerialdisciplina imd ON imd.id_disciplina = mdc.id_disciplina AND im.id_itemdematerial = imd.id_itemdematerial
JOIN dbo.tb_disciplina dc ON dc.id_disciplina = imd.id_disciplina
EXCEPT
SELECT mp.id_projetopedagogico, im.id_itemdematerial,im.st_itemdematerial,dc.id_disciplina,dc.st_disciplina
FROM dbo.tb_materialprojeto mp
JOIN dbo.tb_itemdematerial im ON mp.id_itemdematerial = im.id_itemdematerial
JOIN dbo.tb_modulo md ON md.id_projetopedagogico = mp.id_projetopedagogico
JOIN dbo.tb_modulodisciplina mdc ON mdc.id_modulo = md.id_modulo
JOIN dbo.tb_itemdematerialdisciplina imd ON imd.id_disciplina = mdc.id_disciplina AND im.id_itemdematerial = imd.id_itemdematerial
JOIN dbo.tb_disciplina dc ON dc.id_disciplina = imd.id_disciplina
JOIN dbo.tb_remessa rm ON rm.id_projetopedagogico = mp.id_projetopedagogico AND rm.id_situacao = 81
JOIN dbo.tb_materialremessa mr ON mr.id_remessa = rm.id_remessa AND im.id_itemdematerial = mr.id_itemdematerial
GO

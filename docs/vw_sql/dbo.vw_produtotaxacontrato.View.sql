
GO
/****** Object:  View [dbo].[vw_produtotaxacontrato]    Script Date: 10/08/2012 17:42:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_produtotaxacontrato] AS
SELECT DISTINCT m.id_matricula, t.id_taxa, t.st_taxa, pv.nu_valor, pro.id_produto, pro.id_entidade, pro.id_situacao, pro.id_tipoproduto, pro.nu_gratuito, pro.st_produto, c.id_contrato, m.id_projetopedagogico FROM tb_contrato c
INNER JOIN dbo.tb_contratomatricula cm ON c.id_contrato = cm.id_contrato
INNER JOIN dbo.tb_matricula m ON cm.id_matricula = m.id_matricula
INNER JOIN dbo.tb_produtotaxaprojeto ptp ON m.id_projetopedagogico = ptp.id_projetopedagogico
INNER JOIN dbo.tb_produtotaxa pt ON pt.id_produto = ptp.id_produto AND pt.id_entidade = ptp.id_entidade
INNER JOIN dbo.tb_produto pro ON pro.id_produto = pt.id_produto
INNER JOIN dbo.tb_produtovalor pv ON pv.id_produto = pro.id_produto
INNER JOIN dbo.tb_taxa t ON t.id_taxa = pt.id_taxa
GO

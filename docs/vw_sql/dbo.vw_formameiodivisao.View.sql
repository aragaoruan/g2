
/****** Object:  View [dbo].[vw_formameiodivisao]    Script Date: 10/08/2012 17:41:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_formameiodivisao] as
SELECT fp.id_formapagamento, fp.st_formapagamento, 2 AS id_tipodivisaofinanceira,'Parcela' AS st_tipodivisaofinanceira, mp.id_meiopagamento, mp.st_meiopagamento,
fpp.nu_parcelaquantidademin, fpp.nu_parcelaquantidademax, fpp.nu_juros, fp.nu_maxparcelas, fpar.id_formapagamentoaplicacao, fp.id_entidade
FROM tb_meiopagamento AS mp
INNER JOIN tb_formapagamentoparcela AS fpp ON fpp.id_meiopagamento = mp.id_meiopagamento
INNER JOIN tb_formapagamento AS fp ON fp.id_formapagamento = fpp.id_formapagamento
INNER JOIN dbo.tb_formapagamentoaplicacaorelacao AS fpar ON fpar.id_formapagamento = fp.id_formapagamento
UNION ALL
SELECT fmp.id_formapagamento, fp.st_formapagamento, tdf.id_tipodivisaofinanceira, tdf.st_tipodivisaofinanceira, mp.id_meiopagamento, mp.st_meiopagamento,
NULL AS nu_parcelaquantidademin, NULL AS nu_parcelaquantidademax, NULL AS nu_juros, fp.nu_maxparcelas, fpar.id_formapagamentoaplicacao, fp.id_entidade
FROM tb_formameiopagamento fmp
INNER JOIN tb_formapagamento as fp ON fp.id_formapagamento = fmp.id_formapagamento
INNER JOIN tb_tipodivisaofinanceira as tdf ON tdf.id_tipodivisaofinanceira = fmp.id_tipodivisaofinanceira AND tdf.id_tipodivisaofinanceira = 1
INNER JOIN tb_meiopagamento as mp ON mp.id_meiopagamento = fmp.id_meiopagamento
INNER JOIN dbo.tb_formapagamentoaplicacaorelacao AS fpar ON fpar.id_formapagamento = fp.id_formapagamento
GO



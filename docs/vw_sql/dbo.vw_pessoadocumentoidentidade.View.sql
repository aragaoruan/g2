
GO
/****** Object:  View [dbo].[vw_pessoadocumentoidentidade]    Script Date: 10/08/2012 17:42:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_pessoadocumentoidentidade]

as

select p.*, sit.st_situacao, di.st_rg, di.st_orgaoexpeditor, di.dt_dataexpedicao
from tb_pessoa as p 
LEFT JOIN tb_documentoidentidade as di ON di.id_usuario = p.id_usuario
LEFT JOIN tb_documentoidentidade ON di.id_entidade = p.id_entidade
INNER JOIN tb_situacao as sit ON p.id_situacao = sit.id_situacao
GO

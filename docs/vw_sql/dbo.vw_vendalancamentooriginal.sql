CREATE VIEW [dbo].[vw_vendalancamentooriginal] AS
    SELECT DISTINCT vd.id_venda, vd.id_usuario AS id_usuariovenda
            -- Este campo na tela de venda exibe o Responsável Financeiro, mas antes não mostrava do campo certo
           , CASE
                WHEN usf.st_nomecompleto IS NULL THEN
                    us.st_nomecompleto
                ELSE
                    usf.st_nomecompleto
                END AS st_nomecompleto
           , us.st_nomecompleto AS st_nomeusuario
           , vd.id_evolucao
           , vd.id_formapagamento
           , vd.nu_parcelas
           , vd.nu_valorliquido
           , lv.bl_entrada
           , lv.nu_ordem
           , lc.id_lancamento
           , lc.nu_valor
           , CAST('0.00' AS NUMERIC(7,2)) AS nu_valoratualizado
           , lc.nu_vencimento
           , lc.id_tipolancamento
           , lc.dt_cadastro
           , lc.id_meiopagamento
           , lc.id_usuariolancamento
           , lc.id_entidadelancamento
           , vd.id_entidade
           , lc.id_usuariocadastro
           , lc.st_banco
           , lc.id_cartaoconfig
           , lc.id_boletoconfig
           , lc.bl_quitado
           , lc.dt_vencimento
           , lc.dt_quitado
           , lc.dt_emissao
           , lc.dt_prevquitado
           , lc.st_emissor
           , lc.st_coddocumento
           , lc.nu_juros
           , lc.nu_desconto
           , lc.nu_quitado
           , lc.nu_multa
           , lc.nu_verificacao
           , mp.st_meiopagamento
           , fp.nu_juros AS nu_jurosatraso
           , fp.nu_multa AS nu_multaatraso
           , vmv.nu_diavencimento
           , tl.st_tipolancamento
           , lc.bl_ativo
           , li.st_codlancamento
           , lc.bl_original
           , vd.recorrente_orderid
           , lc.st_retornoverificacao
           , tc.id_campanhacomercial
           , lc.nu_cartao
           , NULL AS st_urlpagamento -- campo fake, gerado no php
		   , lc.id_sistemacobranca
      FROM tb_venda AS vd
INNER JOIN tb_usuario AS us ON us.id_usuario = vd.id_usuario
INNER JOIN tb_lancamentovenda AS lv ON lv.id_venda = vd.id_venda
INNER JOIN tb_lancamento AS lc ON lc.id_lancamento = lv.id_lancamento AND lc.bl_original = 1
INNER JOIN tb_formapagamento AS fp ON fp.id_formapagamento = vd.id_formapagamento
 LEFT JOIN tb_usuario AS usf ON usf.id_usuario = lc.id_usuariolancamento
 LEFT JOIN tb_meiopagamento AS mp ON lc.id_meiopagamento = mp.id_meiopagamento
 LEFT JOIN tb_vendameiovencimento AS vmv ON vd.id_venda = vmv.id_venda AND vmv.id_meiopagamento = mp.id_meiopagamento
INNER JOIN tb_tipolancamento AS tl ON tl.id_tipolancamento = lc.id_tipolancamento
 LEFT JOIN tb_lancamentointegracao AS li ON li.id_lancamento = lc.id_lancamento
 LEFT JOIN tb_campanhacomercial AS tc  ON vd.id_campanhacomercial = tc.id_campanhacomercial AND tc.id_categoriacampanha = 3
GO
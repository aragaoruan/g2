USE [G2_UNY]
GO

/****** Object:  View [rel].[vw_comercialgeral]    Script Date: 9/6/2016 9:45:41 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*Cláusula "cm.bl_ativo = 1" comentada, com o objetivo de pagar a matricula original*/

ALTER VIEW [rel].[vw_comercialgeral] as
  SELECT DISTINCT
    CAST (mt.dt_cadastro AS DATE) AS dt_matricula,
--app.id_areaconhecimento ,
    ps.sg_uf,
    ve.id_entidade,
    ve.st_nomeentidade,
    ps.st_nomecompleto,
    ps.st_cpf,
    st_cidade,
    st_projetopedagogico,
    mt.id_projetopedagogico,
    (
      SELECT
        COALESCE (
            ac2.st_areaconhecimento + ', ',
            ''
        )
      FROM
        dbo.tb_areaconhecimento ac2
        JOIN dbo.tb_areaprojetopedagogico app2 ON ac2.id_areaconhecimento = app2.id_areaconhecimento
      WHERE
        app2.id_projetopedagogico = mt.id_projetopedagogico FOR XML PATH ('')
    ) AS st_areaconhecimento,
    (
      SELECT
        COALESCE (
            tc.st_categoria + ', ',
            ''
        )
      from tb_categoriaproduto tbcp
        LEFT JOIN tb_categoria tc ON tc.id_categoria = tbcp.id_categoria
      WHERE
        tbcp.id_produto = pdt.id_produto FOR XML PATH ('')
    ) AS st_categoria,
--st_areaconhecimento,
    tm.st_turma,
    tm.id_turma,
    tm.dt_inicio,
    tm.dt_fim,
    usr.st_nomecompleto AS st_representante,
    usa.st_nomecompleto AS st_atendente,
    mt.id_matricula,
    ev.id_evolucao,
    ev.st_evolucao,
    st.st_situacao,
    vdl.nu_parcelas AS st_parcelas,
    vdl.nu_parcelas,
    vdlb.nu_valorboleto,
    vdlcc.nu_valorcartaoc,
    vdlcd.nu_valorcartaod,
    vdlcr.nu_valorcartaor,
    vdlc.nu_valorcheque,
    vdlo.nu_valoroutros,
    pd.nu_valorliquido,
    vdlct.nu_valorcarta,
    vdlbl.nu_valorbolsa,
    vd.id_venda,
    vdlv.dt_vencimento AS dt_vencimentoultima,
    pdc.st_produto AS st_combo,
    pdc.id_produto AS id_produtocombo,
    ve.st_nomeentidade AS st_nomeentidaderec,
    et.id_entidade AS id_entidadepai,
    pdt.id_produto,
    '1' as id_formapagamentoaplicacao,
    'todas' as st_formapagamentoaplicacao,
      st_afiliado = (CASE WHEN afi.st_afiliado IS NOT NULL THEN afi.st_afiliado ELSE '-' END),
      st_nucleotelemarketing = (CASE WHEN nt.st_nucleotelemarketing IS NOT NULL THEN nt.st_nucleotelemarketing ELSE '-' END),
      st_vendedor = (CASE WHEN vdr.st_vendedor IS NOT NULL THEN vdr.st_vendedor ELSE '-' END),
      st_siteorigem = (CASE WHEN vd.st_siteorigem IS NOT NULL THEN vd.st_siteorigem ELSE '-' END),
      st_campanhacomercial = (CASE WHEN cam_com.st_campanhacomercial IS NOT NULL THEN cam_com.st_campanhacomercial ELSE '-' END),
    vd.st_nomconvenio,
    vd.st_codpromocao,
	(SELECT MAX(l.id_logacesso)
                FROM dbo.tb_logacesso l
                WHERE (    l.id_funcionalidade = 448
                        OR (l.id_funcionalidade = 799
                AND l.id_matricula = mt.id_matricula))
                AND l.id_entidade  = mt.id_entidadematricula
                AND l.id_usuario   = mt.id_usuario) AS id_logacesso,
     CAST (mt.dt_ultimoacesso AS DATE) AS dt_ultimoacesso
  FROM
    tb_entidade AS et
    JOIN dbo.vw_entidaderecursivaid AS ve ON ve.nu_entidadepai = et.id_entidade
                                             OR ve.id_entidade = et.id_entidade
    JOIN dbo.tb_matricula AS mt ON ve.id_entidade = mt.id_entidadematricula and mt.id_evolucao not in (41)
                                   AND mt.bl_ativo = 1 AND mt.id_matriculaorigem IS NULL
    JOIN dbo.tb_evolucao AS ev ON ev.id_evolucao = mt.id_evolucao
    JOIN dbo.tb_situacao AS st ON st.id_situacao = mt.id_situacao
    JOIN dbo.tb_areaprojetopedagogico AS app ON app.id_projetopedagogico = mt.id_projetopedagogico
    JOIN dbo.tb_areaconhecimento AS aa ON aa.id_areaconhecimento = app.id_areaconhecimento -- AND aa.id_tipoareaconhecimento = 1
    JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
    LEFT JOIN tb_turma AS tm ON tm.id_turma = mt.id_turma
    JOIN dbo.vw_pessoa AS ps ON ps.id_usuario = mt.id_usuario
                                AND ps.id_entidade = ve.id_entidade
    JOIN dbo.tb_contratomatricula AS cm ON cm.id_matricula = mt.id_matricula --AND cm.bl_ativo = 1
    JOIN dbo.tb_contrato AS ct ON ct.id_contrato = cm.id_contrato
    JOIN dbo.tb_venda AS vd ON vd.id_venda = ct.id_venda and vd.bl_transferidoentidade = 0
    LEFT JOIN dbo.tb_afiliado AS afi ON afi.id_afiliado = vd.id_afiliado
    LEFT JOIN dbo.tb_nucleotelemarketing AS nt ON nt.id_nucleotelemarketing = vd.id_nucleotelemarketing
    LEFT JOIN dbo.tb_vendedor AS vdr ON vdr.id_vendedor = vd.id_vendedor
    LEFT JOIN dbo.tb_usuario AS usa ON usa.id_usuario = vd.id_atendente
    LEFT JOIN dbo.tb_vendaenvolvido AS ver ON ver.id_venda = vd.id_venda
                                              AND ver.id_funcao = 5
    LEFT JOIN dbo.tb_usuario AS usr ON usr.id_usuario = ver.id_usuario CROSS APPLY (
                                                                                     SELECT
                                                                                       COUNT (id_lancamento) AS nu_parcelas
                                                                                     FROM
                                                                                       dbo.vw_vendalancamentooriginal
                                                                                     WHERE
                                                                                       id_venda = vd.id_venda
                                                                                   ) AS vdl OUTER APPLY (
                                                                                                          SELECT
                                                                                                            SUM (nu_valor) AS nu_valorboleto
                                                                                                          FROM
                                                                                                            dbo.vw_vendalancamentooriginal
                                                                                                          WHERE
                                                                                                            id_venda = vd.id_venda
                                                                                                            AND id_meiopagamento = 2
                                                                                                        ) AS vdlb OUTER APPLY (
                                                                                                                                SELECT
                                                                                                                                  SUM (nu_valor) AS nu_valorcartaoc
                                                                                                                                FROM
                                                                                                                                  dbo.vw_vendalancamentooriginal
                                                                                                                                WHERE
                                                                                                                                  id_venda = vd.id_venda
                                                                                                                                  AND id_meiopagamento = 1
                                                                                                                              ) AS vdlcc OUTER APPLY (
                                                                                                                                                       SELECT
                                                                                                                                                         SUM (nu_valor) AS nu_valorcartaod
                                                                                                                                                       FROM
                                                                                                                                                         dbo.vw_vendalancamentooriginal
                                                                                                                                                       WHERE
                                                                                                                                                         id_venda = vd.id_venda
                                                                                                                                                         AND id_meiopagamento = 7
                                                                                                                                                     ) AS vdlcd OUTER APPLY (
                                                                                                                                                                              SELECT
                                                                                                                                                                                SUM (nu_valor) AS nu_valorcartaor
                                                                                                                                                                              FROM
                                                                                                                                                                                dbo.vw_vendalancamentooriginal
                                                                                                                                                                              WHERE
                                                                                                                                                                                id_venda = vd.id_venda
                                                                                                                                                                                AND id_meiopagamento = 11
                                                                                                                                                                            ) AS vdlcr OUTER APPLY (
                                                                                                                                                                                                     SELECT
                                                                                                                                                                                                       SUM (nu_valor) AS nu_valorcheque
                                                                                                                                                                                                     FROM
                                                                                                                                                                                                       dbo.vw_vendalancamentooriginal
                                                                                                                                                                                                     WHERE
                                                                                                                                                                                                       id_venda = vd.id_venda
                                                                                                                                                                                                       AND id_meiopagamento = 4
                                                                                                                                                                                                   ) AS vdlc OUTER APPLY (
                                                                                                                                                                                                                           SELECT
                                                                                                                                                                                                                             SUM (nu_valor) AS nu_valoroutros
                                                                                                                                                                                                                           FROM
                                                                                                                                                                                                                             dbo.vw_vendalancamentooriginal
                                                                                                                                                                                                                           WHERE
                                                                                                                                                                                                                             id_venda = vd.id_venda
                                                                                                                                                                                                                             AND id_meiopagamento NOT IN (2, 1, 7, 11, 4, 13, 10)
                                                                                                                                                                                                                         ) AS vdlo OUTER APPLY (
                                                                                                                                                                                                                                                 SELECT
                                                                                                                                                                                                                                                   SUM (nu_valor) AS nu_valorcarta
                                                                                                                                                                                                                                                 FROM
                                                                                                                                                                                                                                                   dbo.vw_vendalancamentooriginal
                                                                                                                                                                                                                                                 WHERE
                                                                                                                                                                                                                                                   id_venda = vd.id_venda
                                                                                                                                                                                                                                                   AND id_meiopagamento = 13
                                                                                                                                                                                                                                               ) AS vdlct OUTER APPLY (
                                                                                                                                                                                                                                                                        SELECT
                                                                                                                                                                                                                                                                          SUM (nu_valor) AS nu_valorbolsa
                                                                                                                                                                                                                                                                        FROM
                                                                                                                                                                                                                                                                          dbo.vw_vendalancamentooriginal
                                                                                                                                                                                                                                                                        WHERE
                                                                                                                                                                                                                                                                          id_venda = vd.id_venda
                                                                                                                                                                                                                                                                          AND id_meiopagamento = 10
                                                                                                                                                                                                                                                                      ) AS vdlbl
    JOIN dbo.tb_vendaproduto AS pd ON pd.id_venda = vd.id_venda
                                      AND pd.id_vendaproduto = mt.id_vendaproduto
    JOIN dbo.tb_produto AS pdt ON pdt.id_produto = pd.id_produto
                                  AND pdt.id_modelovenda in (1,3)
    LEFT JOIN dbo.tb_campanhacomercial as cam_com ON pd.id_campanhacomercial = cam_com.id_campanhacomercial
    LEFT JOIN tb_produto AS pdc ON pdc.id_produto = pd.id_produtocombo
    CROSS APPLY (
                  SELECT
                    TOP 1 dt_vencimento
                  FROM
                    dbo.vw_vendalancamentooriginal
                  WHERE
                    id_venda = vd.id_venda
                  ORDER BY
                    dt_vencimento DESC
                ) AS vdlv
GO

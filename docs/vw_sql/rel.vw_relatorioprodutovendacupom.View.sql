
GO

/****** Object:  View [rel].[vw_relatorioprodutovendacupom]    Script Date: 01/08/2014 15:07:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 CREATE VIEW [rel].[vw_relatorioprodutovendacupom] as
SELECT
	ROW_NUMBER() OVER (ORDER BY c.id_cupom) AS st_ordem,
	p.id_produto,
	p.st_produto,
	ct.id_categoria,
	ct.st_categoria,
	pdv.nu_valor,
	vp.nu_desconto,
	vp.nu_valorliquido,
	vp.nu_valorbruto,
	v.id_entidade,
	e.st_nomeentidade,
	v.id_venda,
	CASE v.id_cupom
	WHEN CONVERT(int, v.id_cupom) THEN v.dt_cadastro ELSE NULL
	END AS dt_utilizacao,
	cc.id_campanhacomercial,
	cc.st_campanhacomercial,
	c.id_cupom,
	c.st_codigocupom,
	CASE v.id_cupom
	WHEN CONVERT(int, v.id_cupom) THEN 'Sim' ELSE 'Não'
	END AS st_utilizado,
	CASE c.bl_unico
	WHEN 1 THEN 'Sim' ELSE 'Não'
	END AS st_unico,
	c.dt_cadastro AS dt_criacaocupom,
	c.dt_fim AS dt_validadecupom,
	CASE
	WHEN c.dt_fim > GETDATE() THEN 'Sim' ELSE 'Não'
	END AS st_valido,
	(SELECT
	urc.st_nomecompleto
	 FROM tb_usuario urc
	 WHERE urc.id_usuario = c.id_usuariocadastro)
																					AS st_nomeresponsavelcupom,
	uc.id_usuario,
	uc.st_nomecompleto,
	uc.st_cpf,
	pc.sg_uf,
	c.id_tipodesconto,
	CASE c.id_tipodesconto
	WHEN 1 THEN c.nu_desconto ELSE NULL
	END AS nu_valordesconto,
	CASE c.id_tipodesconto
	WHEN 2 THEN c.nu_desconto ELSE NULL
	END AS nu_valordescontoporcentagem,
	CASE c.id_tipodesconto
	WHEN 1 THEN CAST(ISNULL(c.nu_desconto, 0) AS smallmoney) -- ISNULL(pdv.nu_valor, 0) - ISNULL(c.nu_desconto, 0)
	WHEN 2 THEN CAST(ISNULL(pdv.nu_valor, 0) * (ISNULL(c.nu_desconto, 0) / 100) AS smallmoney)
	END AS nu_valordescontocupom,
	h.id_holding,
	h.st_holding
FROM tb_produto p
	join tb_categoriaproduto ctp ON ctp.id_produto = p.id_produto
	join tb_categoria ct ON ct.id_categoria = ctp.id_categoria
	JOIN tb_produtovalor pdv ON pdv.id_produto = p.id_produto
	JOIN tb_vendaproduto vp ON p.id_produto = vp.id_produto
	JOIN tb_venda v ON v.id_venda = vp.id_venda
	JOIN tb_entidade e ON e.id_entidade = v.id_entidade
	JOIN tb_usuario uc ON uc.id_usuario = v.id_usuario
	JOIN tb_pessoa pc ON uc.id_usuario = pc.id_usuario AND pc.id_entidade = v.id_entidade
	LEFT JOIN tb_cupom c ON c.id_cupom = v.id_cupom
	LEFT JOIN tb_campanhacomercial cc ON (cc.id_campanhacomercial = v.id_campanhacomercial OR c.id_campanhacomercial = cc.id_campanhacomercial)
	JOIN tb_holdingfiliada as hf on hf.id_entidade = e.id_entidade
	JOIN tb_holding as h on h.id_holding = hf.id_holding
--ORDER BY c.id_cupom ASC



GO



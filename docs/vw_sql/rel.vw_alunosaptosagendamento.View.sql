CREATE VIEW [rel].[vw_alunosaptosagendamento] AS
  SELECT
    DISTINCT
    mt.id_turma,
    mt.id_evolucao,
    mt.st_evolucao,
    mt.id_projetopedagogico,
    mt.dt_inicio,
    mt.st_nomecompleto,
    mt.st_cpf,
    mt.id_matricula,
    mt.id_contrato,
    mt.st_email,
    mt.st_telefone,
    mt.sg_uf,
    mt.st_situacao,
    mt.st_areaconhecimento,
    mt.st_projetopedagogico,
    mt.st_turma,
    mt.dt_inicioturma,
    mt.id_situacaoagendamento,
    mt.id_entidadeatendimento,
    mt.id_situacao,
    ava_rec.id_tipodeavaliacao,
    (CASE
     WHEN ava_rec.id_tipodeavaliacao > 0
       THEN 'RECUPERAÇÃO'
     ELSE 'PROVA FINAL'
     END) AS 'st_avaliacao',
    (CASE
     WHEN (ISNULL(ec.st_valor, 0) = 1)
       THEN CAST(md.dt_aptoagendamento AS DATE)
     ELSE CAST(mt.dt_aptoagendamento AS DATE)
     END) AS dt_aptoagendamento
  FROM rel.vw_matricula AS mt
    JOIN tb_projetopedagogico pp
      ON pp.id_projetopedagogico = mt.id_projetopedagogico
         AND pp.bl_disciplinacomplementar = 0
    OUTER APPLY (
                  SELECT
                    TOP 1
                    *
                  FROM tb_avaliacaoagendamento
                  WHERE id_matricula = mt.id_matricula
                  ORDER BY
                    nu_presenca DESC,
                    id_tipodeavaliacao DESC,
                    id_avaliacaoagendamento DESC
                ) AS age
    OUTER APPLY (
                  SELECT
                    (CASE
                     WHEN age.nu_presenca IS NULL
                       THEN age.id_tipodeavaliacao
                     ELSE age.id_tipodeavaliacao + 1
                     END) AS id_tipodeavaliacao
                ) AS ava_rec
-- Esquema configuração (se for prova por disciplina)
    LEFT JOIN vw_entidadeesquemaconfiguracao AS ec
      ON ec.id_entidade = mt.id_entidadeatendimento AND
         ec.id_itemconfiguracao = 24 AND ec.st_valor = 1
-- Se for prova por disciplina, então verificar se em alguma disciplina, o aluno está apto
    OUTER APPLY (
                  SELECT
                    TOP 1
                    dt_aptoagendamento,
                    id_situacaoagendamento
                  FROM
                    tb_matriculadisciplina AS md2
                  WHERE
                    md2.id_matricula = mt.id_matricula
                    AND md2.id_situacaoagendamento IN (120, 177)
                ) AS md
  WHERE
    (ISNULL(ec.st_valor, 0) = 0 AND mt.id_situacaoagendamento = 120)
    OR
    (ISNULL(ec.st_valor, 0) = 1 AND md.id_situacaoagendamento IN (120, 177))

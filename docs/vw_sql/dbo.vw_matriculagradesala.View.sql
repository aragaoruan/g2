CREATE VIEW [dbo].[vw_matriculagradesala] AS
SELECT DISTINCT mt.id_usuario, us.st_nomecompleto
, mt.id_matricula
, pp.id_projetopedagogico, pp.st_projetopedagogico, uspp.st_nomecompleto AS st_coordenadorprojeto
, md.id_modulo, md.st_modulo, md.st_tituloexibicao AS st_moduloexibicao
, mdd.id_disciplina, dc.st_disciplina, dc.st_tituloexibicao AS st_disciplinaexibicao, dc.nu_cargahoraria
, ds.id_saladeaula, sa.st_saladeaula, usp.st_nomecompleto AS st_professortitular,
sa.dt_abertura ,
sa.dt_encerramento,
 mdc.id_matriculadisciplina, mdc.id_situacao, st.st_situacao, mdc.id_evolucao, ev.st_evolucao,(mdc.nu_aprovafinal*pp.nu_notamaxima/100) AS nu_notafinal
--, sum(cast ( aa.st_nota as numeric(5,2) )) as nu_notatemp
, em.st_situacaoentrega
FROM
tb_matricula AS mt
JOIN tb_matriculadisciplina AS mdc ON mt.id_matricula = mdc.id_matricula
JOIN tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
JOIN tb_modulo AS md ON md.id_projetopedagogico = mt.id_projetopedagogico AND md.bl_ativo = 1
JOIN tb_modulodisciplina AS mdd ON mdd.id_modulo = md.id_modulo AND mdc.id_disciplina = mdd.id_disciplina AND mdd.bl_ativo = 1
JOIN tb_disciplina AS dc ON dc.id_disciplina = mdd.id_disciplina AND dc.id_disciplina = mdc.id_disciplina
JOIN dbo.tb_usuario us ON us.id_usuario = mt.id_usuario
LEFT JOIN tb_alocacao AS al ON al.id_matriculadisciplina = mdc.id_matriculadisciplina AND al.bl_ativo = 1
LEFT JOIN tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula
LEFT JOIN tb_disciplinasaladeaula AS ds ON ds.id_disciplina = mdd.id_disciplina AND ds.id_saladeaula = al.id_saladeaula
LEFT JOIN tb_situacao AS st ON st.id_situacao = mdc.id_situacao
LEFT JOIN tb_evolucao AS ev ON ev.id_evolucao = mdc.id_evolucao
LEFT JOIN tb_avaliacaoconjuntoreferencia AS acr ON acr.dt_inicio <= GETDATE() AND acr.dt_fim >= GETDATE() AND (acr.id_projetopedagogico = mt.id_projetopedagogico OR acr.id_saladeaula = al.id_saladeaula)
LEFT JOIN tb_avaliacaoaluno AS aa ON aa.id_avaliacaoconjuntoreferencia = acr.id_avaliacaoconjuntoreferencia AND mt.id_matricula = aa.id_matricula AND aa.bl_ativo = 1
LEFT JOIN tb_avaliacaodisciplina AS ad ON ad.id_avaliacao = aa.id_avaliacao AND ad.id_disciplina = mdc.id_disciplina
LEFT JOIN dbo.vw_entregamaterial AS em ON em.id_matricula = mt.id_matricula AND em.id_disciplina = dc.id_disciplina --AND md.id_modulo = em.id_modulo AND em.bl_ativo = 1
LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS uper ON uper.id_saladeaula = sa.id_saladeaula AND uper.bl_titular = 1 AND uper.bl_ativo = 1
LEFT JOIN dbo.tb_usuario AS usp ON usp.id_usuario = uper.id_usuario
RIGHT JOIN dbo.tb_usuarioperfilentidadereferencia AS uperp ON uperp.id_projetopedagogico = pp.id_projetopedagogico AND uperp.bl_titular = 1 AND uperp.bl_ativo = 1
RIGHT JOIN tb_perfil AS perfil ON perfil.id_perfil = uperp.id_perfil  AND  perfil.id_perfilpedagogico = 2 
RIGHT JOIN dbo.tb_usuario AS uspp ON uspp.id_usuario = uperp.id_usuario



GO



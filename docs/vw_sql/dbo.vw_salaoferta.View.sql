CREATE VIEW [dbo].[vw_salaoferta] AS
SELECT DISTINCT
	se.id_entidade,
	pp.id_projetopedagogico,
	mo.id_modulo,
	mo.st_modulo,
	ISNULL(md.nu_obrigatorioalocacao, 0) AS nu_obrigatorioalocacao,
	ISNULL(md.nu_disponivelapartirdo, 0) AS nu_disponivelapartirdo,
	md.nu_percentualsemestre,
	aps.id_saladeaula,
	sa.st_saladeaula,
	sa.nu_maxalunos,
	sa.bl_ofertaexcepcional,
	pp.id_trilha,
	ds.id_disciplina,
	dc.id_tipodisciplina,
  dc.nu_creditos,
	pl.id_periodoletivo,
	pl.st_periodoletivo,
	pl.dt_inicioinscricao,
	pl.dt_fiminscricao,
	pl.dt_abertura,
	pl.dt_encerramento,
	pl.id_tipooferta,
	md.nu_ordem
FROM tb_projetopedagogico AS pp
JOIN tb_projetoentidade as pe on pe.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_areaprojetosala AS aps
	ON aps.id_projetopedagogico = pp.id_projetopedagogico
JOIN vw_periodoletivoentidade AS pl ON pe.id_entidade = pl.id_entidade
JOIN tb_saladeaula AS sa
	ON sa.id_saladeaula = aps.id_saladeaula
	AND sa.id_periodoletivo = pl.id_periodoletivo
	AND sa.bl_ativa = 1
	AND sa.id_situacao = 8
	AND sa.id_categoriasala = 1
	--AND se.id_saladeaula = sa.id_saladeaula
JOIN tb_saladeaulaentidade AS se
	ON (aps.id_saladeaula = se.id_saladeaula OR sa.bl_todasentidades = 1) and pe.id_entidade = se.id_entidade
JOIN tb_disciplinasaladeaula AS ds
	ON ds.id_saladeaula = sa.id_saladeaula
JOIN tb_modulo AS mo
	ON mo.id_projetopedagogico = pp.id_projetopedagogico and mo.bl_ativo = 1
JOIN tb_modulodisciplina AS md
	ON ds.id_disciplina = md.id_disciplina and md.bl_ativo = 1 and md.id_modulo = mo.id_modulo
JOIN tb_disciplina AS dc
	ON dc.id_disciplina = ds.id_disciplina
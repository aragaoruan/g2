

/****** Object:  View [dbo].[vw_produtocombo]    Script Date: 24/04/2014 10:14:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 CREATE VIEW [dbo].[vw_produtocombo] as
select DISTINCT
  pc.id_produtocombo
, combo.id_produto as id_combo
, combo.st_produto as st_combo
, pc.id_produtoitem
, pc.nu_descontoporcentagem
, cast((pdv.nu_valor - ((pdv.nu_valor/100)*pc.nu_descontoporcentagem)) as numeric(18,2)) as nu_valorprodutoitem
, cast(((pdv.nu_valor/100)*pc.nu_descontoporcentagem) as numeric(18,2)) as nu_valordescontoitem
, pdv.nu_valor, pdv.nu_valormensal, tpv.id_tipoprodutovalor, tpv.st_tipoprodutovalor

, pd.id_produto, pd.st_produto,pd.id_entidade
, pd.nu_gratuito, tpd.id_tipoproduto, tpd.st_tipoproduto,pdv.id_produtovalor
, pd.id_situacao, st.st_situacao, pvc.nu_valor as nu_valorcombo
from tb_produto as pd
left JOIN tb_produtovalor as pdv ON pdv.id_produto = pd.id_produto and pdv.dt_inicio <= CAST(GETDATE() AS DATE) and (dt_termino >= CAST(GETDATE() AS DATE) or dt_termino is null) and pdv.id_tipoprodutovalor = 4
left JOIN tb_tipoproduto as tpd ON tpd.id_tipoproduto = pd.id_tipoproduto
left JOIN tb_tipoprodutovalor as tpv ON pdv.id_tipoprodutovalor = tpv.id_tipoprodutovalor
left JOIN tb_situacao as st ON st.id_situacao = pd.id_situacao
JOIN tb_produtocombo as pc ON pc.id_produtoitem = pd.id_produto
JOIN tb_produto as combo ON combo.id_produto = pc.id_produto
left JOIN tb_produtovalor as pvc ON pvc.id_produto = combo.id_produto and pvc.dt_inicio <= CAST(GETDATE() AS DATE) and (pvc.dt_termino >= CAST(GETDATE() AS DATE) or pvc.dt_termino is null) and pvc.id_tipoprodutovalor = 4
where combo.bl_ativo = 1 and combo.id_tipoproduto = 7


GO



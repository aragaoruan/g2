GO

/****** Object:  View [dbo].[vw_encerramentovalor]    Script Date: 08/01/2014 13:10:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_encerramentovalor] AS
  SELECT
    DISTINCT
    ea.id_encerramentosala,
    upr.id_usuario,
    es.id_usuarioprofessor,
    m.id_projetopedagogico,
    app.id_areaconhecimento,
    m.id_matricula,
    m.id_entidadematricula AS id_entidade,
    d.id_disciplina,
    d.id_tipodisciplina,
    a.id_saladeaula,
    d.nu_cargahoraria,
    a.id_alocacao,
      nu_valor =
      CASE
      WHEN rp1.id_regrapagamento IS NOT NULL
        THEN rp1.nu_valor
      WHEN rp2.id_regrapagamento IS NOT NULL
        THEN rp2.nu_valor
      WHEN rp3.id_regrapagamento IS NOT NULL
        THEN rp3.nu_valor
      WHEN rp4.id_regrapagamento IS NOT NULL
        THEN rp4.nu_valor
      WHEN rp5.id_regrapagamento IS NOT NULL
        THEN rp5.nu_valor
      ELSE NULL
      END,
      id_regrapagamento =
      CASE
      WHEN rp1.id_regrapagamento IS NOT NULL
        THEN rp1.id_regrapagamento
      WHEN rp2.id_regrapagamento IS NOT NULL
        THEN rp2.id_regrapagamento
      WHEN rp3.id_regrapagamento IS NOT NULL
        THEN rp3.id_regrapagamento
      WHEN rp4.id_regrapagamento IS NOT NULL
        THEN rp4.id_regrapagamento
      WHEN rp5.id_regrapagamento IS NOT NULL
        THEN rp5.id_regrapagamento
      ELSE NULL
      END
  FROM tb_encerramentoalocacao ea
    INNER JOIN dbo.tb_encerramentosala es
      ON ea.id_encerramentosala = es.id_encerramentosala
    INNER JOIN dbo.tb_alocacao a
      ON a.id_alocacao = ea.id_alocacao
    INNER JOIN dbo.tb_matriculadisciplina md
      ON md.id_matriculadisciplina = a.id_matriculadisciplina
    INNER JOIN tb_disciplina d
      ON d.id_disciplina = md.id_disciplina
    INNER JOIN dbo.tb_matricula m
      ON m.id_matricula = md.id_matricula
    INNER JOIN dbo.tb_entidade e
      ON e.id_entidade = m.id_entidadematricula
    INNER JOIN dbo.vw_pessoa p
      ON p.id_usuario = m.id_usuario
    INNER JOIN dbo.tb_areaprojetopedagogico app
      ON app.id_projetopedagogico = m.id_projetopedagogico
    JOIN dbo.tb_usuarioperfilentidadereferencia AS upr
      ON upr.id_saladeaula = a.id_saladeaula
         AND upr.bl_titular = 1 AND upr.bl_ativo = 1

-- JOIN para regra tipo 5 PROFESSOR POR PROJETO
    OUTER APPLY (
                  SELECT
                    rp.*,
                    ch.nu_cargahoraria
                  FROM tb_regrapagamento AS rp,
                    dbo.tb_cargahoraria ch
                  WHERE rp.id_cargahoraria = ch.id_cargahoraria
                        AND ch.nu_cargahoraria = d.nu_cargahoraria
                        AND rp.id_tiporegrapagamento = 5
                        -- "OR e.id_esquemaconfiguracao = 1" foi adicionado para contemplar também as Entidades da GRADUAÇÃO
                        AND (rp.id_entidade = m.id_entidadematricula OR e.id_esquemaconfiguracao = 1)
                        AND d.id_tipodisciplina = rp.id_tipodisciplina
                        AND rp.id_professor = upr.id_usuario
                        AND rp.id_projetopedagogico = m.id_projetopedagogico
                        AND rp.bl_ativo = 1
                ) AS rp1
-- JOIN para regra tipo 4 PROFESSOR
    OUTER APPLY (
                  SELECT
                    rp.*,
                    ch.nu_cargahoraria
                  FROM tb_regrapagamento AS rp
                    JOIN dbo.tb_cargahoraria ch ON rp.id_cargahoraria = ch.id_cargahoraria
                                                   AND ch.nu_cargahoraria = d.nu_cargahoraria
                  WHERE rp.id_tiporegrapagamento = 4
                        -- "OR e.id_esquemaconfiguracao = 1" foi adicionado para contemplar também as Entidades da GRADUAÇÃO
                        AND (rp.id_entidade = m.id_entidadematricula OR e.id_esquemaconfiguracao = 1)
                        AND d.id_tipodisciplina = rp.id_tipodisciplina
                        AND rp.id_professor = upr.id_usuario
                        AND rp.bl_ativo = 1
                ) AS rp2
-- JOIN para regra tipo 3 PROJETO
    OUTER APPLY (
                  SELECT
                    rp.*,
                    ch.nu_cargahoraria
                  FROM tb_regrapagamento AS rp
                    JOIN dbo.tb_cargahoraria ch
                      ON rp.id_cargahoraria = ch.id_cargahoraria AND ch.nu_cargahoraria = d.nu_cargahoraria
                  WHERE rp.id_tiporegrapagamento = 3
                        -- "OR e.id_esquemaconfiguracao = 1" foi adicionado para contemplar também as Entidades da GRADUAÇÃO
                        AND (rp.id_entidade = m.id_entidadematricula OR e.id_esquemaconfiguracao = 1)
                        AND d.id_tipodisciplina = rp.id_tipodisciplina
                        AND rp.id_projetopedagogico = m.id_projetopedagogico
                        AND rp.bl_ativo = 1
                ) AS rp3
-- JOIN para regra tipo 2 AREA
    OUTER APPLY (
                  SELECT
                    rp.*,
                    ch.nu_cargahoraria
                  FROM tb_regrapagamento AS rp
                    JOIN dbo.tb_cargahoraria ch
                      ON rp.id_cargahoraria = ch.id_cargahoraria
                         AND cast(ch.nu_cargahoraria AS INT) = cast(d.nu_cargahoraria AS INT)
                  WHERE rp.id_tiporegrapagamento = 2
                        -- "OR e.id_esquemaconfiguracao = 1" foi adicionado para contemplar também as Entidades da GRADUAÇÃO
                        AND (rp.id_entidade = m.id_entidadematricula OR e.id_esquemaconfiguracao = 1)
                        AND d.id_tipodisciplina = rp.id_tipodisciplina
                        AND rp.id_areaconhecimento = app.id_areaconhecimento
                        AND rp.bl_ativo = 1
                ) AS rp4
-- JOIN para regra tipo 1 ENTIDADE
    OUTER APPLY (
                  SELECT
                    rp.*,
                    ch.nu_cargahoraria
                  FROM tb_regrapagamento AS rp
                    JOIN dbo.tb_cargahoraria ch
                      ON rp.id_cargahoraria = ch.id_cargahoraria
                         AND ch.nu_cargahoraria = d.nu_cargahoraria
                  WHERE rp.id_tiporegrapagamento = 1
                        -- "OR e.id_esquemaconfiguracao = 1" foi adicionado para contemplar também as Entidades da GRADUAÇÃO
                        AND (rp.id_entidade = m.id_entidadematricula OR e.id_esquemaconfiguracao = 1)
                        AND d.id_tipodisciplina = rp.id_tipodisciplina
                        AND rp.bl_ativo = 1
                ) AS rp5
  WHERE d.bl_ativa = 1
        AND m.bl_ativo = 1


GO



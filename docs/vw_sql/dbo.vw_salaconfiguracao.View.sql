/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2014 (12.0.2000)
    Source Database Engine Edition : Microsoft SQL Server Standard Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2014
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/


ALTER VIEW [dbo].[vw_salaconfiguracao]
AS
SELECT DISTINCT
  sa.id_saladeaula,
  sa.dt_cadastro,
  st_saladeaula,
  dc.st_disciplina,
  dc.id_tipodisciplina,
  uscd.st_nomecompleto AS st_tutor,
  sa.bl_ativa,
  sa.id_usuariocadastro,
  id_modalidadesaladeaula,
  dt_inicioinscricao,
  dt_fiminscricao,
  dt_abertura,
  DATEADD(DAY, cast(isnull(ce.st_valor, 0) AS INT), sa.dt_encerramento) AS dt_encerramentoprofessor,
  st_localizacao,
  id_tiposaladeaula,
  dt_encerramento,
  sa.id_entidade,
  id_periodoletivo,
  bl_usardoperiodoletivo,
  sa.id_situacao,
  nu_maxalunos,
  nu_diasencerramento,
  bl_semencerramento,
  nu_diasaluno,
  si.id_sistema,
  si.st_codsistemacurso,
  bl_semdiasaluno,
  alc.nu_alocados,
  dc.id_disciplina,
  lg.nu_acesso,
  (alc.nu_alocados - lg.nu_acesso) AS nu_semacesso,
  (SELECT
     isnull(vwce.st_valor, 0)
   FROM vw_entidadeesquemaconfiguracao vwce
   WHERE vwce.id_entidade = sa.id_entidade AND vwce.id_itemconfiguracao = 4) AS nu_maxextensao,
  sa.nu_diasextensao,
  st_pontosnegativos,
  st_pontospositivos,
  st_pontosmelhorar,
  st_pontosresgate,
  st_conclusoesfinais,
  st_justificativaacima,
  st_justificativaabaixo,
  0 AS nu_semnota,
  0 AS nu_maiornota,
  0 AS nu_menornota
FROM dbo.tb_saladeaula AS sa
JOIN dbo.tb_disciplinasaladeaula AS ds  ON ds.id_saladeaula = sa.id_saladeaula
JOIN dbo.tb_disciplina AS dc  ON dc.id_disciplina = ds.id_disciplina
  LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS upr ON upr.id_saladeaula = ds.id_saladeaula AND upr.bl_ativo = 1
                                                             AND upr.bl_titular = 1
LEFT JOIN dbo.tb_usuario AS uscd  ON uscd.id_usuario = upr.id_usuario
LEFT JOIN dbo.vw_entidadeesquemaconfiguracao AS ce  ON ce.id_entidade = sa.id_entidade and ce.id_itemconfiguracao = 5
LEFT JOIN dbo.tb_saladeaulaintegracao AS si  ON si.id_saladeaula = sa.id_saladeaula
CROSS APPLY (SELECT
               COUNT(id_alocacao) AS nu_alocados
             FROM dbo.tb_alocacao AS ae
               JOIN dbo.tb_matriculadisciplina AS md ON md.id_matriculadisciplina = ae.id_matriculadisciplina
               JOIN dbo.tb_matricula AS mt ON mt.id_matricula = md.id_matricula AND mt.bl_institucional = 0 AND
                                              mt.bl_ativo = 1
             WHERE ae.id_saladeaula = sa.id_saladeaula AND ae.bl_ativo = 1) AS alc
CROSS APPLY (SELECT
               COUNT(DISTINCT mt.id_matricula) AS nu_acesso
             FROM dbo.tb_alocacao AS ae
               JOIN dbo.tb_matriculadisciplina AS md ON md.id_matriculadisciplina = ae.id_matriculadisciplina
               JOIN dbo.tb_matricula AS mt ON mt.id_matricula = md.id_matricula AND mt.bl_institucional = 0 AND
                                              mt.bl_ativo = 1
             WHERE ae.id_saladeaula = sa.id_saladeaula
                   AND ((sa.id_tiposaladeaula = 2) OR sa.id_tiposaladeaula = 1) -- alteração para AC-26484
                   AND ae.bl_ativo = 1 AND exists(SELECT
                                                    la.id_usuario
                                                  FROM dbo.tb_logacesso AS la
                                                  WHERE la.id_saladeaula = ae.id_saladeaula AND
                                                        mt.id_usuario = la.id_usuario AND
                                                        la.id_entidade = mt.id_entidadeatendimento AND
                                                        sa.dt_encerramento >= CAST(la.dt_cadastro AS DATE)
                   )) AS lg
GO







GO
/****** Object:  View [dbo].[vw_disciplinasextras]    Script Date: 10/08/2012 17:41:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_disciplinasextras] as
select
mt.id_matricula, mt.id_projetopedagogico, pp.st_tituloexibicao, apm.id_serie, se.st_serie, se.id_serieanterior,0 AS id_areaconhecimento, apm.st_disciplinaoriginal as st_areaconhecimento, 2 AS id_tipoareaconhecimento
,0 AS id_disciplina, apm.st_disciplinaoriginal AS st_disciplina, apm.st_notaoriginal AS nu_aprovafinal, pp.nu_notamaxima, GETDATE() as dt_conclusaodisciplina ,apm.st_cargahoraria AS nu_cargahoraria ,12 AS id_evolucao, 65 AS id_situacao
, apm.st_cargahoraria as st_cargahorariaaproveitamento, apm.st_notaoriginal as st_notaoriginalaproveitamento, apd.st_instituicao as st_instituicaoaproveitamento, apd.sg_uf as sg_ufaproveitamento, mn.st_nomemunicipio as st_nomemunicipioaproveitamento
 , CAST(cast(apm.nu_anoconclusao AS VARCHAR(4)) + '-01-01' AS date) as dt_conclusaoaproveitamento
, ent.st_nomeentidade, ed.sg_uf, mun.st_nomemunicipio, apm.id_serie AS nu_ordem, ap.st_tituloexibicao AS st_areaagregadora, ap.id_areaconhecimento AS id_areaagregadora
from tb_matricula as mt
JOIN tb_projetopedagogico as pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_aproveitamentomatriculadisciplina as apm ON apm.id_matricula = mt.id_matricula and apm.id_disciplina IS null
JOIN tb_aproveitamentodisciplina as apd ON apd.id_aproveitamentodisciplina = apm.id_aproveitamentodisciplina
JOIN tb_serie as se ON se.id_serie = apm.id_serie
JOIN dbo.tb_projetopedagogicoserienivelensino AS psn ON psn.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_nivelensino as ne ON ne.id_nivelensino = psn.id_nivelensino
LEFT JOIN tb_municipio as mn ON mn.id_municipio = apd.id_municipio
JOIN tb_entidade as ent ON ent.id_entidade = mt.id_entidadematriz
JOIN tb_entidadeendereco as eend ON eend.id_entidade = ent.id_entidade and eend.bl_padrao = 1
JOIN tb_endereco as ed ON ed.id_endereco = eend.id_endereco
JOIN tb_municipio as mun ON mun.id_municipio = ed.id_municipio
LEFT JOIN dbo.tb_areaconhecimento AS ap ON ap.bl_ativo = 1 AND bl_extras = 1 AND ap.id_entidade = mt.id_entidadematriz
GO

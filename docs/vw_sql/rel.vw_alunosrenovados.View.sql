CREATE VIEW [rel].[vw_alunosrenovados] AS
  SELECT
    DISTINCT
    usu.st_nomecompleto,
    usu.st_cpf,
    proped.st_projetopedagogico,
    proped.id_projetopedagogico,
    tur.st_turma,
    tur.id_turma,
    venpro.id_matricula,
    ven.dt_cadastro,
    TRY_CAST(ven.dt_limiterenovacao AS DATETIME2) AS dt_limiterenovacao,
    ven.dt_confirmacao,
    vwpe.st_email,
    enre.id_entidade AS id_entidadeacesso,
    en.id_entidade,
    en.st_nomeentidade,
    ev.st_evolucao,
    ven.id_venda,
    ven.nu_valorbruto,
    ven.id_campanhapontualidade,
    camp_pont.st_campanhacomercial AS st_campanhapontualidade,
    venpro.id_campanhacomercial,
    camp_comerc.st_campanhacomercial,
-- GII-8018 INICIO
    total_disciplinas.nu_disciplinas AS st_totaldisciplinaselecionada
  -- GII-8018 FIM
  FROM
    tb_venda AS ven
    JOIN tb_usuario AS usu ON usu.id_usuario = ven.id_usuario
    JOIN vw_pessoaemail AS vwpe ON vwpe.id_usuario = usu.id_usuario
    JOIN tb_vendaproduto AS venpro ON venpro.id_venda = ven.id_venda
    LEFT JOIN tb_matricula AS mat ON mat.id_matricula = venpro.id_matricula
    LEFT JOIN tb_evolucao AS ev ON ev.id_evolucao = mat.id_evolucao
    LEFT JOIN tb_entidade AS en ON en.id_entidade = mat.id_entidadematricula
    LEFT JOIN dbo.tb_entidaderelacao AS er_pai ON er_pai.id_entidade = mat.id_entidadematricula
    LEFT JOIN dbo.tb_entidaderelacao AS enre ON enre.id_entidade IN (er_pai.id_entidade, er_pai.id_entidadepai)
    JOIN tb_turma AS tur ON tur.id_turma = venpro.id_turma
    JOIN tb_turmaprojeto AS turpro ON turpro.id_turma = tur.id_turma AND
                                      turpro.id_projetopedagogico = mat.id_projetopedagogico
    JOIN tb_projetopedagogico AS proped ON proped.id_projetopedagogico = turpro.id_projetopedagogico
-- Campanhas
-- id_campanhacomercial sempre é da vendaproduto
    LEFT JOIN tb_campanhacomercial AS camp_comerc ON camp_comerc.id_campanhacomercial = venpro.id_campanhacomercial
    LEFT JOIN tb_campanhacomercial AS camp_pont ON camp_pont.id_campanhacomercial = ven.id_campanhapontualidade
-- GII-8018 INICIO
    OUTER APPLY (
                  SELECT
                    COUNT(
                        predisc.id_prematriculadisciplina
                    ) AS nu_disciplinas
                  FROM
                    tb_prematriculadisciplina AS predisc
                  WHERE
                    predisc.id_venda = ven.id_venda
                ) AS total_disciplinas
-- GII-8018 FIM
  WHERE
    ven.bl_renovacao = 1

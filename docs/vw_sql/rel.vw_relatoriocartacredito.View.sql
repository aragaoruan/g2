CREATE VIEW rel.vw_relatoriocartacredito AS 
SELECT
  mat.id_matricula AS 'id_matricula',
  u.st_nomecompleto AS 'st_nomecompleto',
  sit.id_situacao AS 'id_situacao',
  sit.st_situacao AS 'st_situacao',
  canc.dt_solicitacao AS 'dt_solicitacao',
  canc.nu_valortotal AS 'nu_valortotal',
  DATEADD(YEAR, 1, carta.dt_cadastro) as 'dt_validade',
  carta.nu_valororiginal,
  carta.nu_valorutilizado,
  carta.nu_valordisponivel,
  canc.nu_valordevolucao AS 'nu_valordevolucao',
  canc.id_cancelamento AS 'id_cancelamento',
  carta.id_entidade AS 'id_entidade',
  e.st_nomeentidade as 'st_entidade',
  t.st_turma as 'st_turma',
  u.st_cpf AS 'st_cpf',
  carta.id_cartacredito
FROM
  tb_matricula AS mat
  JOIN tb_usuario AS u ON u.id_usuario = mat.id_usuario
  JOIN tb_cancelamento AS canc ON canc.id_cancelamento = mat.id_cancelamento
  JOIN dbo.vw_cartacredito AS carta ON carta.id_cancelamento = canc.id_cancelamento
  JOIN tb_situacao AS sit ON sit.id_situacao = carta.id_situacao
  JOIN tb_entidade as e on e.id_entidade = carta.id_entidade
  JOIN tb_turma as t on t.id_turma = mat.id_turma
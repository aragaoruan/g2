
GO
/****** Object:  View [dbo].[vw_matriculadisciplinasaladeaula]    Script Date: 10/08/2012 17:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_matriculadisciplinasaladeaula] AS
SELECT mat.id_matricula, mat.bl_ativo, mat.id_entidadematricula, mat.id_evolucao, ev.st_evolucao,  mat.id_situacao, si.st_situacao ,
    md.id_matriculadisciplina, md.id_disciplina, dsa.id_saladeaula
FROM tb_matricula mat
JOIN tb_matriculadisciplina md ON md.id_matricula = mat.id_matricula
JOIN tb_situacao si ON si.id_situacao = mat.id_situacao
JOIN tb_evolucao ev ON ev.id_evolucao = mat.id_evolucao
JOIN tb_areaprojetosala as aps ON aps.id_projetopedagogico = mat.id_projetopedagogico
JOIN tb_disciplinasaladeaula dsa ON dsa.id_disciplina = md.id_disciplina
JOIN tb_saladeaula sa ON sa.id_saladeaula = dsa.id_saladeaula AND sa.bl_ativa = 1 AND (sa.dt_encerramento >= getdate()or sa.dt_encerramento is null) and aps.id_saladeaula = sa.id_saladeaula
WHERE sa.bl_ativa = 1
GO

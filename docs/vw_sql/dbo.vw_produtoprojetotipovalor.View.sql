SET DEADLOCK_PRIORITY HIGH
go
 CREATE VIEW [dbo].[vw_produtoprojetotipovalor] AS
SELECT DISTINCT
  pro.id_produto,
  pro.st_produto,
  pro.id_situacao,
  s.st_situacao,
  s.st_descricaosituacao,
  pro.bl_ativo,
  pro.bl_todasformas,
  pro.bl_todascampanhas,
  pro.id_entidade,

  pp.id_entidadecadastro AS id_entidadematriz,
  pp.st_projetopedagogico,
  pp.id_contratoregra,
  pp.bl_turma,


  e.st_nomeentidade,

  ppp.id_projetopedagogico,

  pp.st_tituloexibicao,

  pv.id_produtovalor,
  pv.dt_inicio,
  pv.dt_termino,
  pv.id_tipoprodutovalor,
  tpv.st_tipoprodutovalor,

  pv.nu_valor,
  pv.nu_valormensal,
  pv.nu_basepropor,

  ne.id_nivelensino,
  ne.st_nivelensino,

  areapp.id_areaconhecimento,
  areac.st_areaconhecimento
FROM tb_projetopedagogico pp
JOIN tb_produtoprojetopedagogico ppp
  ON ppp.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_produto pro
  ON ppp.id_produto = pro.id_produto
  AND pro.id_tipoproduto = 1
JOIN tb_entidade e
  ON pro.id_entidade = e.id_entidade
JOIN tb_produtovalor pv
  ON pv.id_produto = pro.id_produto
  AND ((CAST(GETDATE() AS DATE) BETWEEN pv.dt_inicio AND pv.dt_termino)
  OR(CAST(GETDATE() AS DATE) >= pv.dt_inicio AND  pv.dt_termino IS NULL))
JOIN tb_tipoprodutovalor tpv
  ON tpv.id_tipoprodutovalor = pv.id_tipoprodutovalor
JOIN tb_situacao s
  ON pro.id_situacao = s.id_situacao
LEFT JOIN tb_projetopedagogicoserienivelensino ppsne
  ON ppsne.id_projetopedagogico = pp.id_projetopedagogico
LEFT JOIN tb_nivelensino ne
  ON ne.id_nivelensino = ppsne.id_nivelensino
LEFT JOIN tb_areaprojetopedagogico areapp
  ON areapp.id_projetopedagogico = pp.id_projetopedagogico
LEFT JOIN tb_areaconhecimento areac
  ON areac.id_areaconhecimento = areapp.id_areaconhecimento

GO



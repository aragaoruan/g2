
GO
/****** Object:  View [dbo].[vw_trilhadisciplina]    Script Date: 10/08/2012 17:42:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_trilhadisciplina] AS
SELECT pp.id_projetopedagogico,pp.id_trilha, d.id_disciplina, d.st_disciplina, td.id_disciplinaprerequisito, dpre.st_disciplina as st_disciplinaprerequisito
FROM tb_modulo mo
INNER JOIN tb_projetopedagogico pp ON pp.id_projetopedagogico = mo.id_projetopedagogico
INNER JOIN tb_modulodisciplina md ON md.id_modulo = mo.id_modulo
INNER JOIN tb_disciplina d ON d.id_disciplina = md.id_disciplina
LEFT JOIN tb_trilhadisciplina td ON d.id_disciplina = td.id_disciplina AND td.id_trilha = pp.id_trilha
LEFT JOIN tb_disciplina dpre ON dpre.id_disciplina = td.id_disciplinaprerequisito
WHERE d.bl_ativa = 1 AND mo.bl_ativo = 1
GO


ALTER VIEW [rel].[vw_quantidade_transacoes_cartao_credito2] AS
SELECT DISTINCT 
	e.st_nomeentidade, e.id_entidade, 
	tf.id_venda AS nu_totaltransacoes, 
	visa.id_venda AS nu_visa , 
	masterc.id_venda AS nu_master , 
	amex.id_venda AS nu_amex, 
	elo.id_venda AS nu_elo,
	total.valor AS nu_totalsomatorio,
	dt_transacao.dt_cadastro

FROM tb_transacaofinanceira tf 
	JOIN tb_venda v ON tf.id_venda = v.id_venda 
	JOIN tb_entidade e ON e.id_entidade = v.id_entidade
	CROSS APPLY (SELECT SUM(l.nu_valor) AS valor FROM tb_lancamentovenda lv 
						JOIN tb_lancamento l ON lv.id_lancamento = l.id_lancamento
					WHERE l.id_meiopagamento = 1 
						AND lv.id_venda = v.id_venda 
					GROUP BY lv.id_venda
				) AS total
	CROSS APPLY ( SELECT CAST(tff.dt_cadastro AS DATE) AS dt_cadastro
		FROM tb_transacaofinanceira tff
		WHERE tff.id_venda = v.id_venda) AS dt_transacao
	OUTER APPLY ( SELECT id_venda FROM tb_lancamentovenda lv 
						JOIN tb_lancamento l ON lv.id_lancamento = l.id_lancamento
						JOIN tb_cartaoconfig cc ON l.id_cartaoconfig = cc.id_cartaoconfig
						JOIN tb_cartaobandeira cb ON cc.id_cartaobandeira = cb.id_cartaobandeira 
						JOIN tb_contaentidade ce ON cc.id_contaentidade = ce.id_contaentidade
					WHERE l.id_meiopagamento = 1 
						AND lv.id_venda = v.id_venda 
						AND cb.id_cartaobandeira = 1
					GROUP BY lv.id_venda
				) AS visa
	OUTER APPLY ( SELECT id_venda FROM tb_lancamentovenda lv 
						JOIN tb_lancamento l ON lv.id_lancamento = l.id_lancamento
						JOIN tb_cartaoconfig cc ON l.id_cartaoconfig = cc.id_cartaoconfig
						JOIN tb_cartaobandeira cb ON cc.id_cartaobandeira = cb.id_cartaobandeira 
						JOIN tb_contaentidade ce ON cc.id_contaentidade = ce.id_contaentidade
					WHERE l.id_meiopagamento = 1 
						AND lv.id_venda = v.id_venda 
						AND cb.id_cartaobandeira = 2
					GROUP BY lv.id_venda
				) AS masterc
	OUTER APPLY ( SELECT id_venda FROM tb_lancamentovenda lv 
						JOIN tb_lancamento l ON lv.id_lancamento = l.id_lancamento
						JOIN tb_cartaoconfig cc ON l.id_cartaoconfig = cc.id_cartaoconfig
						JOIN tb_cartaobandeira cb ON cc.id_cartaobandeira = cb.id_cartaobandeira 
						JOIN tb_contaentidade ce ON cc.id_contaentidade = ce.id_contaentidade
					WHERE l.id_meiopagamento = 1 
						AND lv.id_venda = v.id_venda 
						AND cb.id_cartaobandeira = 3
					GROUP BY lv.id_venda
				) AS amex
	OUTER APPLY ( SELECT id_venda FROM tb_lancamentovenda lv 
						JOIN tb_lancamento l ON lv.id_lancamento = l.id_lancamento
						JOIN tb_cartaoconfig cc ON l.id_cartaoconfig = cc.id_cartaoconfig
						JOIN tb_cartaobandeira cb ON cc.id_cartaobandeira = cb.id_cartaobandeira 
						JOIN tb_contaentidade ce ON cc.id_contaentidade = ce.id_contaentidade
					WHERE l.id_meiopagamento = 1 
						AND lv.id_venda = v.id_venda 
						AND cb.id_cartaobandeira = 4
					GROUP BY lv.id_venda
				) AS elo
WHERE tf.nu_status = '0'
--AND v.id_entidade = 14
--GROUP BY e.st_nomeentidade, e.id_entidade
--, dt_transacao.dt_cadastro
--ORDER BY e.st_nomeentidade

	
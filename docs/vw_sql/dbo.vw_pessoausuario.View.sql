
GO
/****** Object:  View [dbo].[vw_pessoausuario]    Script Date: 10/08/2012 17:42:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_pessoausuario] AS
select u.id_usuario, u.id_registropessoa, u.st_cpf, u.st_login, u.st_senha, 
	   u.st_nomecompleto, u.bl_ativo, p.id_entidade, rp.st_registropessoa,
	   p.id_usuariocadastro, p.st_nomeexibicao, ec.id_estadocivil, ec.st_estadocivil, 
	   p.st_nomemae, p.st_nomepai, p.st_passaporte, p.st_sexo, 
	   p.dt_cadastro, p.dt_nascimento, mu.id_municipio, mu.st_nomemunicipio, 
	   uf.sg_uf, uf.st_uf, ps.id_pais, ps.st_nomepais  
from tb_usuario as u, tb_pessoa as p, tb_registropessoa as rp, tb_pais as ps, tb_municipio as mu, tb_uf as uf, tb_estadocivil as ec
where u.id_usuario = p.id_usuario and
	  u.id_registropessoa = rp.id_registropessoa and
	  p.id_municipio = mu.id_municipio and
	  p.sg_uf = uf.sg_uf and
	  p.id_usuariocadastro = u.id_usuario and
	  p.id_estadocivil = ec.id_estadocivil and
	  p.id_pais = ps.id_pais
GO

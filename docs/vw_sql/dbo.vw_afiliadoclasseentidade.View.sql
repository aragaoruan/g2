CREATE VIEW dbo.vw_afiliadoclasseentidade AS (

  SELECT
    a.id_afiliado,
    a.st_afiliado,
    ca.id_classeafiliacao,
    ca.st_classeafiliacao,
    ace.id_entidade
  FROM tb_afiliado AS a
  JOIN tb_afiliadoclasseentidade AS ace ON ace.id_afiliado = a.id_afiliado
  JOIN tb_classeafiliacao AS ca ON ca.id_classeafiliacao = ace.id_classeafiliacao

)
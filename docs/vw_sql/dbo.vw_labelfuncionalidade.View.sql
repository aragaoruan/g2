
GO
/****** Object:  View [dbo].[vw_labelfuncionalidade]    Script Date: 10/08/2012 17:42:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_labelfuncionalidade] as
select f.id_funcionalidade, CASE
								WHEN ef.st_label IS NOT NULL THEN ef.st_label
								ELSE f.st_funcionalidade
								END
								AS st_funcionalidade,
		f.id_funcionalidadepai, f.bl_ativo as bl_funcionalidadeativo, f.id_situacao as id_situacaofuncionalidade, f.st_classeflex, f.st_urlicone, f.id_tipofuncionalidade,
		CASE
			WHEN ef.nu_ordem IS NOT NULL THEN ef.nu_ordem
			ELSE f.nu_ordem
			END
			AS nu_ordem,
			f.st_classeflexbotao, f.bl_pesquisa, ef.bl_ativo as bl_entidadefuncionalidadeativo, ef.id_situacao as id_situacaoentidadefuncionalidade, ef.bl_obrigatorio,
			ef.bl_visivel, ef.id_entidade
FROM tb_funcionalidade as f
LEFT JOIN tb_entidadefuncionalidade as ef ON f.id_funcionalidade = ef.id_funcionalidade
GO

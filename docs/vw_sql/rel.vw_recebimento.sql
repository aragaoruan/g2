CREATE VIEW [rel].[vw_recebimento]  AS
  SELECT
    DISTINCT
    et.st_razaosocial,
    et.st_nomeentidade,
    mt.id_matricula,
    CONVERT(VARCHAR(MAX), mt.dt_cadastro, 103) AS st_datamatricula,
    mt.dt_cadastro,
    vd.id_venda,
    pp.st_projetopedagogico,
    pp.id_projetopedagogico,
    tm.st_turma,
    tm.id_turma,
    ps.st_nomecompleto,
    ps.id_usuario,
    ps.nu_ddd + ps.nu_telefone AS st_telefone,
    ps.st_cpf,
    ps.st_email,
    ps.st_endereco,
    ps.st_cep,
    ps.st_bairro,
    ps.st_complemento,
    ps.nu_numero,
    ps.sg_uf,
    ps.st_cidade,
    CASE WHEN lc.nu_quitado IS NULL
      THEN '0'
    ELSE LEFT(lc.nu_quitado, (LEN(lc.nu_quitado) - 3))
    END AS 'nu_valorbaixadog2',
    CASE WHEN vd.nu_valorliquido = 0
      THEN CONVERT(MONEY, 0)
    ELSE CONVERT(MONEY, CAST((lc.nu_quitado * vp.nu_valorliquido
                              / vd.nu_valorliquido) AS NUMERIC(9,
                             2)))
    END AS nu_proporcionalbaixado,
    CASE WHEN lv.nu_ordem IS NULL
              AND lv.bl_entrada = 1
      THEN 0
    ELSE lv.nu_ordem
    END AS 'nu_ordem',
    CONVERT(VARCHAR(12), lc.dt_quitado, 103) AS st_databaixa,
    CONVERT(VARCHAR(12), lc.dt_vencimento, 103) AS st_datavencimento,
    vd.id_entidade,
    lc.id_lancamento,
    CAST(lc.dt_quitado AS DATE) AS dt_pagamento,
    tb_situacao.st_situacao,
      st_afiliado = (CASE WHEN afi.st_afiliado IS NOT NULL
      THEN afi.st_afiliado
                     ELSE '-' END),
      st_nucleotelemarketing = (CASE WHEN nt.st_nucleotelemarketing IS NOT NULL
      THEN nt.st_nucleotelemarketing
                                ELSE '-' END),
      st_vendedor = (CASE WHEN vdr.st_vendedor IS NOT NULL
      THEN vdr.st_vendedor
                     ELSE '-' END),
      st_siteorigem = (CASE WHEN vd.st_siteorigem IS NOT NULL
      THEN vd.st_siteorigem
                       ELSE '-' END)
  FROM tb_venda AS vd
    LEFT JOIN dbo.tb_afiliado AS afi ON afi.id_afiliado = vd.id_afiliado
    LEFT JOIN dbo.tb_nucleotelemarketing AS nt ON nt.id_nucleotelemarketing = vd.id_nucleotelemarketing
    LEFT JOIN dbo.tb_vendedor AS vdr ON vdr.id_vendedor = vd.id_vendedor
    JOIN dbo.tb_entidade AS et ON et.id_entidade = vd.id_entidade
    JOIN dbo.tb_entidadeintegracao AS ei ON ei.id_entidade = vd.id_entidade AND ei.id_sistema = 3
    JOIN dbo.tb_contrato AS ct ON ct.id_venda = vd.id_venda
    LEFT JOIN dbo.tb_contratomatricula AS cm ON cm.id_contrato = ct.id_contrato AND cm.bl_ativo = 1
    JOIN dbo.tb_vendaproduto AS vp ON vp.id_venda = vd.id_venda
    JOIN dbo.tb_matricula AS mt ON mt.id_matricula = vp.id_matricula
    JOIN dbo.tb_situacao AS st ON st.id_situacao = mt.id_situacao
    JOIN dbo.vw_pessoa AS ps ON ps.id_usuario = mt.id_usuario AND ps.id_entidade = mt.id_entidadeatendimento
    LEFT JOIN dbo.tb_turma AS tm ON tm.id_turma = mt.id_turma
    JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
    JOIN dbo.tb_lancamentovenda AS lv ON lv.id_venda = vd.id_venda
    JOIN dbo.tb_lancamento AS lc ON lc.id_lancamento = lv.id_lancamento
    LEFT JOIN dbo.tb_lancamentointegracao AS li ON lv.id_lancamento = li.id_lancamento AND
                                                   li.id_lancamento = lc.id_lancamento
    JOIN dbo.tb_situacao ON dbo.tb_situacao.id_situacao = vd.id_situacao AND dbo.tb_situacao.st_tabela = 'tb_venda'
  WHERE mt.id_evolucao <> 20
        AND lc.bl_quitado = 1
        AND lc.bl_ativo = 1


CREATE VIEW [dbo].[vw_listagem_projetos_sala] AS
SELECT DISTINCT
            pp.st_projetopedagogico ,
            us.id_usuario ,
            pe.id_entidade ,
            perf.id_perfil ,
            perf.id_perfilpedagogico ,
            pp.id_projetopedagogico ,
            NULL AS id_status, NULL AS st_status
						  ,usc.st_nomecompleto AS st_coordenador
    FROM    tb_perfil AS perf
            JOIN tb_usuarioperfilentidadereferencia AS uper ON perf.id_perfil = uper.id_perfil
                                                              AND uper.bl_ativo = 1
                                                              AND uper.id_projetopedagogico IS NOT NULL
            JOIN tb_usuario AS us ON uper.id_usuario = us.id_usuario
            JOIN tb_usuarioperfilentidade AS upe ON upe.id_usuario = us.id_usuario
                                                    AND upe.id_entidade = uper.id_entidade
                                                    AND upe.id_perfil = perf.id_perfil
                                                    AND upe.bl_ativo = 1
            JOIN dbo.tb_projetopedagogico AS pp ON pp.bl_ativo = 1
                                                   AND pp.id_projetopedagogico = uper.id_projetopedagogico and pp.id_situacao = 7
			JOIN dbo.tb_projetoentidade AS pe ON pe.id_projetopedagogico = pp.id_projetopedagogico
   --         JOIN dbo.tb_areaprojetosala AS aps ON aps.id_projetopedagogico = pp.id_projetopedagogico
   --         JOIN dbo.tb_saladeaulaentidade AS sei ON aps.id_saladeaula = sei.id_saladeaula
			--JOIN tb_saladeaula AS s ON s.id_saladeaula = sei.id_saladeaula
   --                                    AND aps.id_saladeaula = s.id_saladeaula
   --                                    AND s.bl_ativa = 1

		LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS vwref ON vwref.id_projetopedagogico  = pp.id_projetopedagogico AND vwref.bl_titular = 1 AND vwref.bl_ativo = 1 AND vwref.id_disciplina IS null
		LEFT  JOIN tb_perfil AS pf2 ON vwref.id_perfil = pf2.id_perfil AND pf2.id_perfilpedagogico = 2
		LEFT JOIN dbo.tb_usuario AS usc ON usc.id_usuario = vwref.id_usuario


    WHERE   perf.id_perfilpedagogico = 2
    UNION
    SELECT DISTINCT
            pp.st_projetopedagogico ,
            us.id_usuario ,
            pe.id_entidade ,
            perf.id_perfil ,
            perf.id_perfilpedagogico ,
            pp.id_projetopedagogico ,
            NULL AS id_status, NULL AS st_status
						  ,usc.st_nomecompleto AS st_coordenador
    FROM    tb_perfil AS perf
            JOIN tb_usuarioperfilentidadereferencia AS uper ON perf.id_perfil = uper.id_perfil
                                                              AND uper.bl_ativo = 1
            JOIN tb_usuario AS us ON uper.id_usuario = us.id_usuario
            JOIN tb_usuarioperfilentidade AS upe ON upe.id_usuario = us.id_usuario
                                                    AND upe.id_entidade = uper.id_entidade
                                                    AND upe.id_perfil = uper.id_perfil
                                                    AND upe.bl_ativo = 1
            JOIN tb_perfilpedagogico AS perfped ON perfped.id_perfilpedagogico = perf.id_perfilpedagogico
            JOIN dbo.tb_projetoentidade AS pe ON uper.id_entidade = pe.id_entidade
            JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = pe.id_projetopedagogico
                                                   AND pp.bl_ativo = 1 and pp.id_situacao = 7
            --JOIN dbo.tb_areaprojetosala AS aps ON aps.id_projetopedagogico = pp.id_projetopedagogico
            --JOIN dbo.tb_saladeaulaentidade AS sei ON sei.id_entidade = uper.id_entidade AND aps.id_saladeaula = sei.id_saladeaula
            --JOIN tb_saladeaula AS s ON s.id_saladeaula = sei.id_saladeaula
            --                           AND aps.id_saladeaula = s.id_saladeaula

			LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS vwref ON vwref.id_projetopedagogico  = pp.id_projetopedagogico AND vwref.bl_titular = 1 AND vwref.bl_ativo = 1 AND vwref.id_disciplina IS null
			LEFT  JOIN tb_perfil AS pf2 ON vwref.id_perfil = pf2.id_perfil AND pf2.id_perfilpedagogico = 2
			LEFT JOIN dbo.tb_usuario AS usc ON usc.id_usuario = vwref.id_usuario
			WHERE perf.id_perfilpedagogico IN (7,3)
CREATE VIEW [dbo].[vw_comercial_g1_g2] AS
SELECT
	id_entidade AS 'Código Entidade',
	st_nomeentidade AS Entidade,
	st_nomecompleto AS Aluno,
	st_cpf AS CPF,
	sg_uf AS UF,
	st_cidade AS Cidade,
	st_projetopedagogico AS Curso,
	NULL AS Segmento,
	NULL AS 'Código Área',
	st_areaconhecimento AS 'Área',
	st_projetopedagogico AS 'Título Curso',
	CONVERT (CHAR, dt_matricula, 103) AS 'Data_Matricula',
	st_representante AS Vendedor,
	st_atendente AS Atendente,
	NULL AS 'Núcleo de Atendimento',
	nu_valorboleto AS 'Valor Boleto',
	nu_valorcarta AS 'Valor Carta Crédito',
	nu_valorbolsa AS 'Valor Bolsa',
	nu_valorliquido AS 'Valor Negociado',
	nu_valorcartaoc AS 'Valor no Cartão de Crédito',
	st_evolucao AS 'Evolução da Matrícula',
	NULL AS 'Código Contrato G1',
	id_venda AS 'Código Contrato G2',
	NULL AS 'Transferida outra Entidade ?',
	NULL AS Afiliada,
	id_entidadepai,
	NULL AS gptqry_codentidade,
	dt_matricula AS 'dt_matricula',
	id_matricula AS 'Mátricula'
FROM
	rel.vw_comercialgeral
WHERE
	--id_entidade in (12,19) and
	id_entidadepai IN (12,19,176,177,178,179,423,569,510,39,352)
UNION
	SELECT
		[Cód. Entidade] AS 'Código Entidade',
		Entidade COLLATE SQL_Latin1_General_CP1_CI_AI AS Entidade,
		Aluno COLLATE SQL_Latin1_General_CP1_CI_AI AS Aluno,
		CPF COLLATE SQL_Latin1_General_CP1_CI_AI AS CPF,
		UF COLLATE SQL_Latin1_General_CP1_CI_AI AS UF,
		Cidade COLLATE SQL_Latin1_General_CP1_CI_AI AS Cidade,
		[Título Curso] COLLATE SQL_Latin1_General_CP1_CI_AI AS Curso,
		Segmento COLLATE SQL_Latin1_General_CP1_CI_AI AS Segmento,
		[Cód. Área] AS 'Código Área',
		Área COLLATE SQL_Latin1_General_CP1_CI_AI AS Área,
		[Título Curso] COLLATE SQL_Latin1_General_CP1_CI_AI AS 'Título Curso',
		[Data Matrícula] AS 'Data Matrícula',
		Vendedor COLLATE SQL_Latin1_General_CP1_CI_AI AS Vendedor,
		Atendente COLLATE SQL_Latin1_General_CP1_CI_AI AS Atendente,
		Núcleo COLLATE SQL_Latin1_General_CP1_CI_AI AS 'Núcleo de Atendimento',
		[Valor Boleto] AS 'Valor Boleto',
		[Valor Carta] AS 'Valor Carta Crédito',
		[Valor Bolsa] AS 'Valor Bolsa',
		[Valor Negociado] AS 'Valor Negociado',
		[Valor no Cartão de Crédito] AS 'Valor no Cartão de Crédito',
		[Última Matrícula Evolução] COLLATE SQL_Latin1_General_CP1_CI_AI AS 'Evolução da Matrícula',
		[Cód. Contrato] AS 'Código Contrato G1',
		[Identificação Venda G2] AS 'Código Contrato G2',
		[Transferida outra Entidade] COLLATE SQL_Latin1_General_CP1_CI_AI AS 'Transferida outra Entidade ?',
		AFILIADA COLLATE SQL_Latin1_General_CP1_CI_AI AS Afiliada,
		NULL AS id_entidadepai,
		gptqry_codentidade,
		gptqry_dt_matricula AS 'dt_matricula',
		[Cód. Matrícula] AS 'Mátricula'
	FROM
		[WIN-MB0LL52UPVD].G1_DEV.rel.vw_com5_comercial_geral
	WHERE
		[Cód. Entidade] IN (112,686,967,966,974,608,868,1289,1412,1546,1491,1583)
GO


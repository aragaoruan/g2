CREATE VIEW [rel].[vw_alunoinformacoescomplementares] AS
  SELECT
    mt.id_matricula,
    mt.id_entidadematricula AS id_entidade,
    us.st_nomecompleto,
    us.st_cpf,
    ev.id_evolucao,
    ev.st_evolucao,
    pp.id_projetopedagogico,
    pp.st_projetopedagogico,
    tm.id_turma,
    tm.st_turma,
-- Primeira Venda
    primeira_venda.id_venda,
    tpsel.st_tiposelecao,
    primeira_venda.dt_ingresso,
-- Dados Complementares
      st_ensinomedioconcluido = (CASE
                                 WHEN pdc.id_tipoescola IS NULL
                                   THEN 'Não'
                                 ELSE 'Sim'
                                 END),
    pdc.dt_anoconclusao AS st_anoconclusao,
    pdc.st_curso,
    pdc.st_nomeinstituicao,
    pdc.sg_ufinstituicao,
    mn.st_nomemunicipio,
    et.st_etnia,
    df.st_pessoacomdeficiencia,
    pdc.st_descricaodeficiencia
  FROM
    tb_matricula AS mt
    JOIN tb_usuario AS us ON us.id_usuario = mt.id_usuario
    JOIN tb_evolucao AS ev ON ev.id_evolucao = mt.id_evolucao
    JOIN tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
    JOIN tb_turma AS tm ON tm.id_turma = mt.id_turma
-- Primeira Venda
    JOIN tb_vendaproduto AS primeira_venda ON primeira_venda.id_vendaproduto = mt.id_vendaproduto
    JOIN tb_tiposelecao AS tpsel ON tpsel.id_tiposelecao = primeira_venda.id_tiposelecao
-- Dados Complementares
    JOIN tb_pessoadadoscomplementares AS pdc ON pdc.id_usuario = mt.id_usuario
    LEFT JOIN tb_municipio AS mn ON mn.id_municipio = pdc.id_municipioinstituicao
    LEFT JOIN tb_etnia AS et ON et.id_etnia = pdc.id_etnia
    LEFT JOIN tb_pessoacomdeficiencia AS df ON df.id_pessoacomdeficiencia = pdc.id_pessoacomdeficiencia

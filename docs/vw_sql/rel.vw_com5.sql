	
CREATE VIEW rel.vw_com5 AS	
	SELECT DISTINCT
		vw_m.id_matricula,
		vw_m.id_entidadematricula,
		vw_m.st_entidadematricula,
		vw_m.st_nomecompleto,
		vw_m.st_cpf,
		vw_p.sg_uf,
		vw_p.st_cidade,
		pr.st_produto,
		pr.id_produto,
		ac.st_areaconhecimento AS st_segmengo,
		vw_m.id_projetopedagogico AS id_area,
		vw_m.st_projetopedagogico AS st_area,
		vw_m.dt_inicio,
		vw_m.dt_termino,
		vw_m.dt_cadastro,
		'' AS st_master,
		'' AS st_vendedor,
		'' AS st_atendente,
		'' AS st_nucleo,
		'' AS id_promocional,
		'' AS st_convenio,
		v.id_venda,
		lancamentos.nu_lancamentos,
		lancamentos_boleto.nu_lancamentos AS nu_lancamentosboleto,
		valor_boleto.nu_valorboleto,
		valor_carta.nu_valorcarta,
		valor_bolsa.nu_valorbolsa,
		v.nu_valorliquido AS nu_valornegociado,
		valor_cartaocredito.nu_valorcartaocredito,
		lancamentos_cartaocredito.nu_lancamentoscartaocredito,
		ultimo_lancamento.dt_vencimento AS dt_ultimaparcela,
		'' AS nu_matriculastransferidas,
		'' AS st_cadeiamatricula,
		'' AS id_matriculaorigem,
		vw_m.st_situacao,
		vw_m.st_evolucao,
		'' AS id_matriculacomplementar,
		'' AS id_matricularesgate,
		'' AS id_ultimamatricula,
		'' AS dt_referenciacancelamento,
		'' AS id_ultimaevolucao,	
		'' AS bl_transferencia,
		'' AS id_evolucaocancelamento,
		'' AS nu_proporcionalcursado,	
		'' AS id_prematricula
		 
	FROM  dbo.vw_matricula vw_m
	INNER JOIN vw_pessoa vw_p ON vw_p.id_usuario = vw_m.id_usuario
	INNER JOIN tb_vendaproduto vp ON vw_m.id_matricula = vp.id_matricula
	INNER JOIN tb_venda v ON v.id_venda = vw_m.id_venda
	INNER JOIN tb_produto pr ON pr.id_produto = vp.id_produto
	INNER JOIN tb_areaconhecimento ac ON ac.id_areaconhecimento = vw_m.id_areaconhecimento
	
	CROSS APPLY(
		SELECT 
				COUNT(l.id_lancamento) AS nu_lancamentos 
			FROM tb_lancamentovenda lv 
				JOIN tb_lancamento l ON lv.id_lancamento = l.id_lancamento 
			WHERE id_venda = v.id_venda
			AND bl_ativo = 1
	) AS lancamentos
	
	CROSS APPLY(
		SELECT 
				COUNT(l.id_lancamento) AS nu_lancamentos
			FROM tb_lancamentovenda lv 
				JOIN tb_lancamento l ON lv.id_lancamento = l.id_lancamento 
			WHERE id_venda = v.id_venda
			AND bl_ativo = 1
			AND id_meiopagamento = 2
	) AS lancamentos_boleto
	
	CROSS APPLY(
		SELECT 
				SUM(l.nu_valor) AS nu_valorboleto
			FROM tb_lancamentovenda lv 
				JOIN tb_lancamento l ON lv.id_lancamento = l.id_lancamento 
			WHERE id_venda = v.id_venda
			AND bl_ativo = 1
			AND id_meiopagamento = 2
	) AS valor_boleto
	
	CROSS APPLY(
		SELECT 
				SUM(l.nu_valor) AS nu_valorcarta
			FROM tb_lancamentovenda lv 
				JOIN tb_lancamento l ON lv.id_lancamento = l.id_lancamento 
			WHERE id_venda = v.id_venda
			AND bl_ativo = 1
			AND id_meiopagamento = 13
	) AS valor_carta
	
	CROSS APPLY(
		SELECT 
				SUM(l.nu_valor) AS nu_valorbolsa
			FROM tb_lancamentovenda lv 
				JOIN tb_lancamento l ON lv.id_lancamento = l.id_lancamento 
			WHERE id_venda = v.id_venda
			AND bl_ativo = 1
			AND id_meiopagamento = 10
	) AS valor_bolsa
	
	CROSS APPLY(
		SELECT 
				SUM(l.nu_valor) AS nu_valornegociado
			FROM tb_lancamentovenda lv 
				JOIN tb_lancamento l ON lv.id_lancamento = l.id_lancamento 
			WHERE id_venda = v.id_venda
			AND bl_ativo = 1
			AND id_meiopagamento = 2
	) AS valor_negociado
	
	CROSS APPLY(
		SELECT 
				SUM(l.nu_valor) AS nu_valorcartaocredito
			FROM tb_lancamentovenda lv 
				JOIN tb_lancamento l ON lv.id_lancamento = l.id_lancamento 
			WHERE id_venda = v.id_venda
			AND bl_ativo = 1
			AND id_meiopagamento = 1
	) AS valor_cartaocredito
	
	CROSS APPLY(
		SELECT 
				COUNT(l.id_lancamento) AS nu_lancamentoscartaocredito
			FROM tb_lancamentovenda lv 
				JOIN tb_lancamento l ON lv.id_lancamento = l.id_lancamento 
			WHERE id_venda = v.id_venda
			AND bl_ativo = 1
			AND id_meiopagamento = 1
	) AS lancamentos_cartaocredito
	
	CROSS APPLY(
		SELECT 
				MAX(dt_vencimento) AS dt_vencimento
			FROM tb_lancamentovenda lv 
				JOIN tb_lancamento l ON lv.id_lancamento = l.id_lancamento 
			WHERE id_venda = v.id_venda
			AND bl_ativo = 1
	) AS ultimo_lancamento
	
	--ORDER BY vw_m.id_matricula
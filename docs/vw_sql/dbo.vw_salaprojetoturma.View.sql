USE [G2_UNY]
GO

/****** Object:  View [dbo].[vw_salasprojetoturma]    Script Date: 29/04/2013 17:46:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 CREATE VIEW [dbo].[vw_salasprojetoturma] as
SELECT tp.st_projetopedagogico,tp.id_projetopedagogico,tt.id_turma, td.id_disciplina, td.st_disciplina,ts.st_saladeaula, ts.dt_inicioinscricao, ts.dt_fiminscricao, ts.dt_abertura, ts.dt_encerramento, tu1.st_nomecompleto AS st_professor, ts1.bl_conteudo, ts2.st_sistema, ts.id_entidade
FROM tb_projetopedagogico tp
JOIN tb_modulo tm ON tp.id_projetopedagogico = tm.id_projetopedagogico AND tm.bl_ativo = 1
JOIN tb_modulodisciplina tm1 ON tm.id_modulo = tm1.id_modulo AND tm1.bl_ativo = 1
JOIN tb_turmaprojeto tt ON tp.id_projetopedagogico = tt.id_projetopedagogico AND tt.bl_ativo = 1
JOIN tb_turma tt1 ON tt.id_turma = tt1.id_turma
JOIN tb_disciplina td ON tm1.id_disciplina = td.id_disciplina
LEFT JOIN tb_disciplinasaladeaula td1 ON td.id_disciplina = td1.id_disciplina 
LEFT JOIN tb_areaprojetosala ta ON tp.id_projetopedagogico = ta.id_projetopedagogico AND ta.id_saladeaula = td1.id_saladeaula
LEFT JOIN tb_saladeaula ts ON td1.id_saladeaula = ts.id_saladeaula AND ta.id_saladeaula = ts.id_saladeaula AND CAST(tt1.dt_inicio AS date) BETWEEN CAST(ts.dt_inicioinscricao AS date) AND CAST(ts.dt_fiminscricao AS date)
LEFT JOIN tb_usuarioperfilentidadereferencia tu ON ts.id_saladeaula = tu.id_saladeaula AND tu.bl_ativo =1 AND tu.bl_titular = 1
LEFT JOIN tb_usuario tu1 ON tu.id_usuario = tu1.id_usuario
LEFT JOIN tb_saladeaulaintegracao ts1 ON ts.id_saladeaula = ts1.id_saladeaula
LEFT JOIN tb_sistema ts2 ON ts1.id_sistema = ts2.id_sistema 
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vw_campanhaformapagamentoproduto] as

select cc.*, fp.id_formapagamento, fp.st_formapagamento, pd.id_produto, pd.st_produto  from tb_campanhacomercial cc
inner join tb_campanhadesconto as cd ON cd.id_campanhacomercial = cc.id_campanhacomercial
left join tb_campanhaformapagamento as cf ON cc.id_campanhacomercial = cf.id_campanhacomercial
left join tb_formapagamento as fp ON fp.id_formapagamento = cf.id_formapagamento
left join tb_campanhaproduto as cp ON cc.id_campanhacomercial = cp.id_campanhacomercial
left join tb_produto as pd ON cp.id_produto = pd.id_produto
GO

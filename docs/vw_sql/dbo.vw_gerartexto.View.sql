
GO
/****** Object:  View [dbo].[vw_gerartexto]    Script Date: 10/08/2012 17:41:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_gerartexto] as
select
ts.id_textosistema, ts.st_texto, tc.id_textocategoria, tc.st_textocategoria, tc.st_metodo, tv.st_textovariaveis,tv.st_camposubstituir,tv.st_mascara
from tb_textosistema ts
JOIN tb_textocategoria as tc ON tc.id_textocategoria = ts.id_textocategoria
LEFT JOIN tb_textovariaveis as tv ON tv.id_textocategoria = tc.id_textocategoria
GO

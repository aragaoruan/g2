CREATE VIEW [rel].[vw_cronogramacoordenador] AS


SELECT DISTINCT
    sa.id_saladeaula,
    sa.st_saladeaula,
    sa.dt_inicioinscricao,
    sa.dt_fiminscricao,
    sa.dt_abertura,
    sa.dt_encerramento,
    CAST(dt_atualiza AS DATE) AS dt_atualiza,
    dc.id_disciplina,
    st_disciplina,
    dc.st_tituloexibicao AS st_tituloexibicaodisciplina,
    st_projetopedagogico,
    aps.id_projetopedagogico,
    uss.st_nomecompleto AS st_nomeprofessor,
    usp.id_usuario AS id_coordenador,
    usp.st_nomecompleto AS st_nomecoordenador,
    si.st_codsistemacurso AS st_codava,
    sa.id_entidade,
		sa.id_categoriasala,
    pl.st_periodoletivo,
    pp.bl_disciplinacomplementar,
		CAST(sa.dt_cadastro AS DATE) AS dt_cadastro,
    di.st_codsistema,
    di.st_salareferencia,
    (CASE WHEN si.bl_conteudo = 1 THEN 'SIM' ELSE 'NÃO' END) AS st_processada,
		CAST(COUNT(ta.id_matriculadisciplina) AS VARCHAR) AS st_alunos

FROM
  dbo.tb_saladeaula AS sa
  JOIN dbo.tb_areaprojetosala AS aps ON aps.id_saladeaula = sa.id_saladeaula
  JOIN dbo.tb_disciplinasaladeaula AS da ON da.id_saladeaula = sa.id_saladeaula
  JOIN tb_disciplina AS dc ON dc.id_disciplina = da.id_disciplina
  JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = aps.id_projetopedagogico
  LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS ups ON ups.id_saladeaula = aps.id_saladeaula AND ups.bl_ativo = 1
  LEFT JOIN dbo.tb_usuario AS uss ON uss.id_usuario = ups.id_usuario
  LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS upp ON upp.id_projetopedagogico = pp.id_projetopedagogico AND
                                                             upp.bl_ativo = 1 AND upp.id_disciplina IS NULL
  LEFT JOIN dbo.tb_perfil AS tbp ON tbp.id_perfil = upp.id_perfil AND tbp.id_perfilpedagogico = 2
  LEFT JOIN dbo.tb_usuario AS usp ON usp.id_usuario = upp.id_usuario
  LEFT JOIN dbo.tb_saladeaulaintegracao AS si ON si.id_saladeaula = sa.id_saladeaula
  LEFT JOIN dbo.tb_disciplinaintegracao AS di ON di.id_disciplinaintegracao = si.id_disciplinaintegracao
  LEFT JOIN dbo.tb_alocacao AS ta ON ta.id_saladeaula = sa.id_saladeaula AND ta.bl_ativo = 1
  LEFT JOIN tb_periodoletivo AS pl ON pl.id_periodoletivo = sa.id_periodoletivo

WHERE
  sa.bl_ativa = 1
  AND sa.id_situacao != 74
  AND sa.id_categoriasala = 1 -- Somente salas "Normal"

GROUP BY
    sa.id_saladeaula,
    st_saladeaula,
    sa.dt_inicioinscricao,
    sa.dt_fiminscricao,
    sa.dt_abertura,
    sa.dt_encerramento,
    dt_atualiza,
    dc.id_disciplina,
    st_disciplina,
    dc.st_tituloexibicao,
    st_projetopedagogico,
    aps.id_projetopedagogico,
    uss.st_nomecompleto,
    usp.id_usuario,
    usp.st_nomecompleto,
    si.st_codsistemacurso,
    sa.id_entidade,
		sa.dt_cadastro,
    di.st_codsistema,
    di.st_salareferencia,
    si.bl_conteudo,
		sa.id_categoriasala,
    pl.st_periodoletivo,
    pp.bl_disciplinacomplementar
GO

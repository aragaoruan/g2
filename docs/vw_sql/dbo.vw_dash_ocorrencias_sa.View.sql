CREATE VIEW [dbo].[vw_dash_ocorrencias_sa] 
WITH SCHEMABINDING
AS 

 SELECT COUNT(DISTINCT id_ocorrencia) as nu_pendente 
, 0 as nu_andamento
, ocp.id_entidade

FROM  dbo.tb_ocorrencia AS ocp 
JOIN dbo.tb_assuntoco as ac ON ac.id_assuntoco = ocp.id_assuntoco AND ac.id_tipoocorrencia = 1 AND ac.bl_ativo = 1
JOIN dbo.tb_assuntoentidadeco AS aeco ON aeco.id_assuntoco = ocp.id_assuntoco AND ocp.id_entidade = aeco.id_entidade
JOIN dbo.tb_nucleopessoaco AS nuco ON ocp.id_assuntoco = nuco.id_assuntoco 
JOIN dbo.tb_nucleoco AS nu ON nu.id_nucleoco = nuco.id_nucleoco AND nu.id_tipoocorrencia = ac.id_tipoocorrencia AND nu.bl_ativo = 1
JOIN dbo.tb_nucleoassuntoco AS nas ON  nas.bl_ativo = 1 AND nu.id_nucleoco = nas.id_nucleoco
WHERE  ocp.id_situacao = 97
--AND ocp.id_entidade = 14

GROUP BY ocp.id_entidade

UNION  

SELECT 0 as nu_pendente
,COUNT(DISTINCT id_ocorrencia) as nu_andamento 
, ocp.id_entidade

FROM  dbo.tb_ocorrencia AS ocp 
JOIN dbo.tb_assuntoco as ac ON ac.id_assuntoco = ocp.id_assuntoco AND ac.id_tipoocorrencia = 1 AND ac.bl_ativo = 1
JOIN dbo.tb_assuntoentidadeco AS aeco ON aeco.id_assuntoco = ocp.id_assuntoco AND ocp.id_entidade = aeco.id_entidade
JOIN dbo.tb_nucleopessoaco AS nuco ON ocp.id_assuntoco = nuco.id_assuntoco 
JOIN dbo.tb_nucleoco AS nu ON nu.id_nucleoco = nuco.id_nucleoco AND nu.id_tipoocorrencia = ac.id_tipoocorrencia AND nu.bl_ativo = 1
JOIN dbo.tb_nucleoassuntoco AS nas ON  nas.bl_ativo = 1 AND nu.id_nucleoco = nas.id_nucleoco
WHERE  ocp.id_situacao = 98 
--AND ocp.id_entidade = 14

GROUP BY ocp.id_entidade




GO




GO
/****** Object:  View [dbo].[vw_disciplinarequisito]    Script Date: 10/08/2012 17:41:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_disciplinarequisito] as
select tl.id_trilha, td.id_disciplina, dc.st_disciplina, td.id_disciplinaprerequisito, dcp.st_disciplina as st_disciplinaprerequisito
from tb_trilha as tl
JOIN tb_trilhadisciplina as td ON td.id_trilha = tl.id_trilha
JOIN tb_disciplina as dc ON dc.id_disciplina = td.id_disciplina 
JOIN tb_disciplina as dcp ON dcp.id_disciplina = td.id_disciplinaprerequisito
GO

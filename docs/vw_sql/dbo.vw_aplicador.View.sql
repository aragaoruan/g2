CREATE VIEW [dbo].[vw_aplicador] as
SELECT  DISTINCT  ap.id_aplicadorprova
		, ap.id_entidadeaplicador
		, ap.id_usuarioaplicador
		, ap.id_entidadecadastro
		, ap.id_usuariocadastro
		, ap.st_aplicadorprova
		, ap.dt_cadastro
		, ap.bl_ativo 
		, ape.id_entidade
		, vw.sg_uf

			FROM  tb_aplicadorprovaentidade AS ape 
					INNER JOIN tb_aplicadorprova AS ap ON ap.id_aplicadorprova = ape.id_aplicadorprova 
					INNER JOIN vw_aplicadorendereco AS vw ON vw.id_aplicadorprova = ap.id_aplicadorprova AND vw.id_entidaderelacionada = ape.id_entidade

GO



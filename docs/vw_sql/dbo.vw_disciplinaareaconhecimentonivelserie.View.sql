
GO
/****** Object:  View [dbo].[vw_disciplinaareaconhecimentonivelserie]    Script Date: 10/08/2012 17:41:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_disciplinaareaconhecimentonivelserie] AS
select d.id_disciplina, d.st_disciplina, d.nu_cargahoraria, d.st_descricao, d.id_situacao, sit.st_situacao, ad.id_areaconhecimento, d.id_entidade, ac.st_areaconhecimento, ac.bl_ativo,
	   ne.st_nivelensino, dsn.id_nivelensino, dsn.id_serie, s.st_serie, td.id_tipodisciplina, td.st_tipodisciplina
from tb_disciplina d
LEFT JOIN tb_areadisciplina as ad ON ad.id_disciplina = d.id_disciplina
LEFT JOIN tb_areaconhecimento as ac ON ac.id_areaconhecimento = ad.id_areaconhecimento
LEFT JOIN tb_disciplinaserienivelensino as dsn ON dsn.id_disciplina = d.id_disciplina
LEFT JOIN tb_nivelensino as ne ON ne.id_nivelensino = dsn.id_nivelensino
LEFT JOIN tb_serie as s ON s.id_serie = dsn.id_serie
INNER JOIN tb_tipodisciplina as td ON td.id_tipodisciplina = d.id_tipodisciplina
LEFT JOIN tb_situacao as sit ON sit.id_situacao = d.id_situacao
WHERE d.bl_ativa = 1
GO

CREATE VIEW [rel].[vw_programa_curso]
AS
SELECT DISTINCT
        dbo.tb_entidade.st_nomeentidade, --AS 'ENTIDADE' ,
        dbo.tb_areaconhecimento.id_areaconhecimento,--AS 'CODIGO AREA' ,
        dbo.tb_areaconhecimento.st_areaconhecimento, --AS 'AREA' ,
        situacaoareaconhecimento.st_situacao AS st_situacaoareaconhecimento, --AS 'SITUACAO AREA' ,
        dbo.tb_projetopedagogico.id_projetopedagogico, --AS 'CODIGO PROJETO PEDAGOGICO' ,
        dbo.tb_projetopedagogico.st_projetopedagogico, --AS 'PROJETO PEDAGOGICA' ,
        dbo.tb_projetopedagogico.nu_cargahoraria AS nu_cargahorariaprojetopedagogico ,-- 'CARGA HORARIA PROJETO PEDAGOGICO' ,
        situacaoprojetopedagogico.st_situacao AS st_situacaoprojetopedagogico, --AS 'SITUACAO PROJETO PEDAGOGICO' ,
        dbo.tb_disciplina.id_disciplina, --AS 'CODIGO DISCIPLINA' ,
        dbo.tb_disciplina.st_tituloexibicao, --'DISCIPLINA' ,
		CASE WHEN dbo.tb_modulodisciplina.bl_obrigatoria = 0 THEN 'Não'
		ELSE  'Sim'
		END AS st_obrigatoria ,
        dbo.tb_disciplina.nu_cargahoraria AS nu_cargahorariadisciplina, --AS 'CARGA HORARIA DISCIPLINA' ,
        dbo.tb_disciplina.st_disciplina, --AS 'DISCIPLINA INTERNAMENTE' ,
        situacaodisciplina.st_situacao AS st_situacaosituacaodisciplina,  --AS 'SITUACAO DISCIPLINA' ,
        situacaoproduto.st_descricaosituacao, --AS 'SITUACAO NO SITE' ,
        dbo.tb_usuario.st_nomecompleto, --AS 'COORDENADOR DO PROJETO PEDAGOGICO'
        dbo.tb_produto.id_entidade
FROM    dbo.tb_projetopedagogico
		JOIN tb_modulo ON dbo.tb_modulo.id_projetopedagogico = dbo.tb_projetopedagogico.id_projetopedagogico AND dbo.tb_modulo.bl_ativo = 1
		JOIN dbo.tb_modulodisciplina ON dbo.tb_modulo.id_modulo =  dbo.tb_modulodisciplina.id_modulo AND dbo.tb_modulodisciplina.bl_ativo = 1
        JOIN dbo.tb_disciplina ON dbo.tb_disciplina.id_disciplina = dbo.tb_modulodisciplina.id_disciplina
        JOIN dbo.tb_areaprojetopedagogico ON dbo.tb_areaprojetopedagogico.id_projetopedagogico = dbo.tb_projetopedagogico.id_projetopedagogico
        JOIN dbo.tb_areaconhecimento ON dbo.tb_areaconhecimento.id_areaconhecimento = dbo.tb_areaprojetopedagogico.id_areaconhecimento
        JOIN dbo.tb_situacao AS situacaoareaconhecimento ON situacaoareaconhecimento.id_situacao = dbo.tb_areaconhecimento.id_situacao
                                                            AND situacaoareaconhecimento.st_tabela = 'tb_areaconhecimento'
        JOIN dbo.tb_situacao AS situacaoprojetopedagogico ON situacaoprojetopedagogico.id_situacao = dbo.tb_projetopedagogico.id_situacao
                                                             AND situacaoprojetopedagogico.st_tabela = 'tb_projetopedagogico'
        JOIN dbo.tb_situacao AS situacaodisciplina ON situacaodisciplina.id_situacao = dbo.tb_disciplina.id_situacao
                                                      AND situacaodisciplina.st_tabela = 'tb_disciplina'
        JOIN dbo.tb_produtoprojetopedagogico ON dbo.tb_produtoprojetopedagogico.id_projetopedagogico = dbo.tb_projetopedagogico.id_projetopedagogico
        JOIN dbo.tb_produto ON dbo.tb_produto.id_produto = dbo.tb_produtoprojetopedagogico.id_produto
        JOIN dbo.tb_situacao AS situacaoproduto ON situacaoproduto.id_situacao = dbo.tb_produto.id_situacao
                                                   AND situacaoproduto.st_tabela = 'tb_produto'
        LEFT JOIN dbo.tb_usuarioperfilentidadereferencia ON dbo.tb_usuarioperfilentidadereferencia.id_projetopedagogico = dbo.tb_projetopedagogico.id_projetopedagogico AND bl_titular = 1
        JOIN dbo.tb_perfil ON dbo.tb_perfil.id_perfil = dbo.tb_usuarioperfilentidadereferencia.id_perfil
                                   AND dbo.tb_perfil.st_nomeperfil LIKE '%Coordenador de Projeto Pedagógico%'
        LEFT JOIN dbo.tb_usuario ON dbo.tb_usuario.id_usuario = dbo.tb_usuarioperfilentidadereferencia.id_usuario
        JOIN dbo.tb_entidade
        ON dbo.tb_entidade.id_entidade = dbo.tb_produto.id_entidade
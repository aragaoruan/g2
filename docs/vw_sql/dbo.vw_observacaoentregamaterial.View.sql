
GO
/****** Object:  View [dbo].[vw_observacaoentregamaterial]    Script Date: 10/08/2012 17:42:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_observacaoentregamaterial] as
SELECT
ob.id_usuario, ob.id_observacao, ob.dt_cadastro, ob.st_observacao,
em.id_itemdematerial, em.id_matricula, em.id_entregamaterial
FROM
tb_observacao ob
JOIN tb_observacaoentregamaterial as oe ON oe.id_observacao = ob.id_observacao
JOIN tb_entregamaterial as em on em.id_entregamaterial = oe.id_entregamaterial
GO


/****** Object:  View [dbo].[vw_produtovendalivro]    Script Date: 10/08/2012 17:42:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE VIEW [dbo].[vw_produtovendalivro] AS
SELECT
vpro.id_vendaproduto,

pro.id_produto,
pro.st_produto,
pro.id_entidade,
tpro.id_tipoproduto,
tpro.st_tipoproduto,

v.id_venda,
v.bl_ativo,
vpro.nu_valorbruto,
vpro.nu_valorliquido,
CASE
WHEN vpro.nu_valorbruto = 0 THEN CAST(vpro.nu_desconto*100 as NUMERIC(30,6))
WHEN vpro.nu_valorbruto != 0 THEN CAST((vpro.nu_desconto*100)/vpro.nu_valorbruto as NUMERIC(30,6))
END AS nu_descontoporcentagem,

vpro.nu_desconto as nu_descontovalor,
pv.id_tipoprodutovalor,
tpv.st_tipoprodutovalor,

cc.id_campanhacomercial,
cc.st_campanhacomercial,

 l.id_livro
,l.id_situacao
,l.id_entidadecadastro
,l.id_usuariocadastro
,l.id_tipolivro
,l.id_livrocolecao
,l.st_livro
,l.st_isbn
,l.st_codigocontrole

,lc.st_livrocolecao

,tl.st_tipolivro

FROM tb_vendaproduto vpro
JOIN tb_venda v ON vpro.id_venda = v.id_venda
JOIN tb_produto pro ON vpro.id_produto = pro.id_produto
JOIN tb_tipoproduto tpro ON pro.id_tipoproduto = tpro.id_tipoproduto and tpro.id_tipoproduto = 6
LEFT JOIN tb_campanhacomercial cc ON vpro.id_campanhacomercial = cc.id_campanhacomercial
LEFT JOIN tb_produtovalor pv ON pv.id_produto = vpro.id_produto
LEFT JOIN tb_tipoprodutovalor as tpv ON tpv.id_tipoprodutovalor = pv.id_tipoprodutovalor
JOIN tb_produtolivro as pl on pro.id_produto = pl.id_produto and pl.id_entidade = pro.id_entidade
JOIN tb_livro as l on pl.id_livro = l.id_livro

join tb_livrocolecao as lc on l.id_livrocolecao = lc.id_livrocolecao
join tb_tipolivro as tl on l.id_tipolivro = tl.id_tipolivro

  
GO
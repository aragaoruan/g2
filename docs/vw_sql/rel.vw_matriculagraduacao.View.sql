CREATE VIEW [rel].[vw_matriculagraduacao] AS
  SELECT
    DISTINCT
    mt.id_matricula,
    enre.id_entidade AS id_entidadeacesso,
    mt.id_entidadematricula,
    en.st_nomeentidade AS st_nomeentidadematricula,
    mt.id_projetopedagogico,
    pp.st_projetopedagogico,
    mt.id_turma,
    tm.st_turma,
    mt.id_situacao,
    si.st_situacao,
    mt.id_evolucao,
    ev.st_evolucao,
    us.st_nomecompleto,
    us.st_cpf,
    cm.id_contrato,
    pe.st_email,
    CAST(pe.nu_ddd AS VARCHAR) + '-' + CAST(pe.nu_telefone AS VARCHAR) AS st_telefone,
    pe.sg_uf,
    ac.st_areaconhecimento,
    mt.dt_inicio,
    tm.dt_inicio AS dt_inicioturma,
    CAST(mt.dt_concluinte AS DATE) AS dt_concluinte,
    (CASE
     WHEN mt.bl_documentacao = 1
       THEN 'OK'
     WHEN mt.bl_documentacao = 0
       THEN 'Pendente'
     ELSE 'Não Analisado'
     END) AS st_statusdocumentacao
  FROM
    dbo.tb_matricula AS mt
    JOIN dbo.tb_entidade AS en ON en.id_entidade = mt.id_entidadematricula
    JOIN dbo.tb_entidaderelacao AS er_pai ON er_pai.id_entidade = mt.id_entidadematricula
    JOIN dbo.tb_entidaderelacao AS enre ON enre.id_entidade IN (er_pai.id_entidade, er_pai.id_entidadepai)
    JOIN dbo.tb_usuario AS us ON us.id_usuario = mt.id_usuario
    JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
    JOIN dbo.vw_pessoa AS pe ON pe.id_entidade = mt.id_entidadematricula AND pe.id_usuario = mt.id_usuario
    JOIN dbo.tb_situacao AS si ON si.id_situacao = mt.id_situacao
    JOIN dbo.tb_evolucao AS ev ON ev.id_evolucao = mt.id_evolucao
    LEFT JOIN dbo.tb_turma AS tm ON tm.id_turma = mt.id_turma
    LEFT JOIN dbo.tb_contratomatricula AS cm ON cm.id_matricula = mt.id_matricula AND cm.bl_ativo = 1
    JOIN dbo.tb_areaprojetopedagogico AS ap ON ap.id_projetopedagogico = mt.id_projetopedagogico
    OUTER APPLY
    (SELECT
       TOP 1
       id_areaconhecimento,
       st_areaconhecimento
     FROM dbo.tb_areaconhecimento
     WHERE
       id_areaconhecimento = ap.id_areaconhecimento
       AND id_tipoareaconhecimento = 1
    ) AS ac
  WHERE
    en.id_esquemaconfiguracao = 1

CREATE VIEW [rel].[vw_salasnaoencerradas] AS

SELECT DISTINCT
  sa.id_saladeaula,
  CAST(sa.dt_cadastro AS DATE) AS dt_cadastro,
  CAST(dt_atualiza AS DATE)    AS dt_atualiza,
  sa.st_saladeaula,
  sa.dt_inicioinscricao,
  sa.dt_fiminscricao,
  sa.dt_abertura,
  sa.dt_encerramento,
  extensao.dt_encerramentoextensao,
  dc.id_disciplina,
  dc.st_disciplina,
  pp.st_projetopedagogico,
  coord.id_usuario             AS id_coordenador,
  coord.st_nomecompleto        AS st_nomecoordenador,
  prof.id_usuario              AS id_professor,
  prof.st_nomecompleto         AS st_nomeprofessor,
  si.st_codsistemacurso        AS st_codava,
  dc.st_tituloexibicao         AS st_tituloexibicaodisciplina,
  alocados.st_alunosalocados,
  aps.id_projetopedagogico,
  sa.id_entidade

FROM
  dbo.tb_saladeaula AS sa
  JOIN dbo.tb_areaprojetosala AS aps ON aps.id_saladeaula = sa.id_saladeaula
  JOIN dbo.tb_disciplinasaladeaula AS da ON da.id_saladeaula = sa.id_saladeaula
  JOIN dbo.tb_disciplina AS dc ON dc.id_disciplina = da.id_disciplina AND dc.id_tipodisciplina = 1
  JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = aps.id_projetopedagogico
  LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS ups ON ups.id_saladeaula = aps.id_saladeaula AND ups.bl_titular = 1 AND ups.bl_ativo = 1
  LEFT JOIN dbo.tb_usuario AS prof ON prof.id_usuario = ups.id_usuario
  LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS upp ON upp.id_projetopedagogico = pp.id_projetopedagogico AND upp.bl_titular = 1 AND upp.bl_ativo = 1 AND upp.id_disciplina IS NULL
  LEFT JOIN dbo.tb_perfil AS tbp ON tbp.id_perfil = upp.id_perfil AND tbp.id_perfilpedagogico = 2
  LEFT JOIN dbo.tb_usuario AS coord ON coord.id_usuario = upp.id_usuario
  LEFT JOIN dbo.tb_saladeaulaintegracao AS si ON si.id_saladeaula = sa.id_saladeaula
  LEFT JOIN dbo.tb_disciplinaintegracao AS di ON di.id_disciplinaintegracao = si.id_disciplinaintegracao
  LEFT JOIN dbo.tb_alocacao AS ta ON ta.id_saladeaula = sa.id_saladeaula AND ta.bl_ativo = 1
  LEFT JOIN dbo.tb_periodoletivo AS pl ON pl.id_periodoletivo = sa.id_periodoletivo

  -- Encerramento da Sala
  LEFT JOIN dbo.tb_encerramentosala AS enc ON enc.id_saladeaula = sa.id_saladeaula AND enc.dt_encerramentoprofessor IS NOT NULL

  -- Alunos Alocados
  OUTER APPLY
  (
    SELECT COUNT(alc.id_alocacao) AS st_alunosalocados
    FROM
      dbo.tb_alocacao AS alc
      JOIN dbo.tb_matriculadisciplina AS mdc ON mdc.id_matriculadisciplina = alc.id_matriculadisciplina AND mdc.id_evolucao = 13
    WHERE
      alc.bl_ativo = 1
      AND alc.id_saladeaula = sa.id_saladeaula
  ) AS alocados

  -- Encerramento com extensão
  OUTER APPLY
  (
    SELECT DATEADD(DAY, ISNULL(sa.nu_diasextensao, 0), sa.dt_encerramento) AS dt_encerramentoextensao
  ) AS extensao

WHERE
  sa.bl_ativa = 1
  AND sa.id_situacao != 74
  AND extensao.dt_encerramentoextensao <= CAST(GETDATE() AS DATE)
  AND alocados.st_alunosalocados > 0
  AND sa.id_tiposaladeaula = 2
  --Somente salas que NÃO possuem encerramento
  AND enc.id_encerramentosala IS NULL

GROUP BY
  sa.id_saladeaula,
  st_saladeaula,
  sa.dt_inicioinscricao,
  sa.dt_fiminscricao,
  sa.dt_abertura,
  sa.dt_encerramento,
  dt_atualiza,
  st_disciplina,
  dc.st_tituloexibicao,
  st_projetopedagogico,
  aps.id_projetopedagogico,
  prof.id_usuario,
  prof.st_nomecompleto,
  coord.id_usuario,
  coord.st_nomecompleto,
  si.st_codsistemacurso,
  sa.id_entidade,
  sa.dt_cadastro,
  si.bl_conteudo,
  alocados.st_alunosalocados,
  extensao.dt_encerramentoextensao,
  dc.id_disciplina
CREATE VIEW [rel].[vw_alunosnota] AS
  SELECT
    mt.id_entidadeatendimento,
    mt.id_matricula,
    us.st_nomecompleto,
    us.st_cpf,
    pp.st_projetopedagogico,
    dc.id_disciplina,
    dc.st_disciplina,
    sa.dt_abertura,
    sa.dt_encerramento,
    CAST(CAST((md.nu_aprovafinal * pp.nu_notamaxima / 100) AS NUMERIC(3, 0)) AS VARCHAR) AS st_nota,
    ev.st_evolucao,
    mt.dt_concluinte,
    mt.id_projetopedagogico,
    mt.id_turma,
    mt.id_evolucao,
    mt.dt_cadastro,
    en.st_nomeentidade,
    CAST(mn.st_notaead AS NUMERIC(3, 0)) AS st_notaead,
    CAST(mn.st_notaatividade AS NUMERIC(3, 0)) AS st_notaatividade,
    CAST(mn.st_notafinal AS NUMERIC(3, 0)) AS st_notafinal,
    CAST(mn.st_notarecuperacao AS NUMERIC(3, 0)) AS st_notarecuperacao,
    sa.st_saladeaula
  FROM dbo.tb_matricula AS mt
    JOIN dbo.tb_usuario AS us ON us.id_usuario = mt.id_usuario
    JOIN dbo.tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
    JOIN dbo.tb_alocacao AS al ON al.id_matriculadisciplina = md.id_matriculadisciplina AND al.bl_ativo = 1
    JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula
    JOIN dbo.tb_disciplina AS dc ON md.id_disciplina = dc.id_disciplina
    JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
    JOIN dbo.tb_evolucao AS ev ON ev.id_evolucao = mt.id_evolucao
    JOIN dbo.tb_entidade AS en ON en.id_entidade = mt.id_entidadeatendimento
    LEFT JOIN dbo.vw_matriculanota AS mn ON md.id_matriculadisciplina = mn.id_matriculadisciplina AND
                                            mn.id_alocacao = al.id_alocacao
  WHERE mt.id_evolucao != 20 AND mt.bl_ativo = 1
CREATE VIEW [dbo].[vw_avaliacaoconjunto] as
  select

    ac.id_avaliacaoconjunto,
    ac.st_avaliacaoconjunto,

    ac.id_situacao,
    st.st_situacao,

    ac.id_tipoprova,
    tp.st_tipoprova,

    ac.id_tipocalculoavaliacao,
    tca.st_tipocalculoavaliacao,

    ac.id_entidade

  from tb_avaliacaoconjunto ac
    JOIN tb_tipoprova as tp ON tp.id_tipoprova = ac.id_tipoprova
    JOIN tb_tipocalculoavaliacao as tca ON tca.id_tipocalculoavaliacao = ac.id_tipocalculoavaliacao OR ac.id_tipocalculoavaliacao IS NULL
    JOIN tb_situacao as st ON st.id_situacao = ac.id_situacao
GO
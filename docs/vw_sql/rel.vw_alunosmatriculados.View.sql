
GO
/****** Object:  View [rel].[vw_alunosmatriculados]    Script Date: 10/08/2012 17:42:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [rel].[vw_alunosmatriculados] as

select us.st_nomecompleto,pp.st_tituloexibicao, pp.id_projetopedagogico,mt.dt_inicio, mt.id_entidadematricula,mt.dt_concluinte
from tb_matricula as mt
JOIN tb_usuario as us ON us.id_usuario = mt.id_usuario
JOIN tb_pessoa as ps ON ps.id_usuario = mt.id_usuario and ps.id_entidade = mt.id_entidadeatendimento
JOIN tb_projetopedagogico as pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
WHERE mt.bl_ativo = 1 AND mt.bl_institucional = 0
GO

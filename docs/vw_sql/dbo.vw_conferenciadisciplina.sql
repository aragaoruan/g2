
GO

/****** Object:  View [dbo].[vw_conferenciadisciplina]    Script Date: 09/05/2013 11:29:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vw_conferenciadisciplina] AS
SELECT DISTINCT
    tt.id_turma,
    tp.id_projetopedagogico,
    tp1.id_entidade,
    td.id_disciplina,
    tp.st_projetopedagogico,
    td.st_disciplina,
    ts.st_saladeaula,
    ts.dt_inicioinscricao,
    ts.dt_fiminscricao,
    ts.dt_abertura,
    ts.dt_encerramento,
    ts.st_professor,
    ts.bl_conteudo,
    ts.st_sistema,
    di.st_codsistema,
    di.st_salareferencia,
    (CASE WHEN si.bl_conteudo = 1 THEN 'SIM' ELSE 'NÃO' END) AS st_processada,
	ei.st_titulo AS st_nomemoodle
FROM dbo.tb_projetopedagogico AS tp
INNER JOIN tb_projetoentidade tp1 ON tp.id_projetopedagogico = tp1.id_projetopedagogico
INNER JOIN dbo.tb_modulo AS tm
    ON tp.id_projetopedagogico = tm.id_projetopedagogico AND tm.bl_ativo = 1
INNER JOIN dbo.tb_modulodisciplina AS tm1
    ON tm.id_modulo = tm1.id_modulo AND tm1.bl_ativo = 1
INNER JOIN dbo.tb_turmaprojeto AS tt
    ON tp.id_projetopedagogico = tt.id_projetopedagogico AND tt.bl_ativo = 1
INNER JOIN dbo.tb_turma AS tt1
    ON tt.id_turma = tt1.id_turma
INNER JOIN dbo.tb_disciplina AS td
    ON tm1.id_disciplina = td.id_disciplina
LEFT JOIN vw_saladisciplinaentidade as ts
    ON CAST(tt1.dt_inicio AS date)
       BETWEEN CAST(ts.dt_inicioinscricao AS date) AND CAST(ts.dt_fiminscricao AS date)
    AND td.id_disciplina = ts.id_disciplina
    AND tp1.id_projetopedagogico = ts.id_projetopedagogico
    AND tp1.id_entidade = ts.id_entidade
LEFT JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = ts.id_saladeaula
LEFT JOIN dbo.tb_entidadeintegracao AS ei ON ei.id_entidadeintegracao = sa.id_entidadeintegracao
LEFT JOIN dbo.tb_saladeaulaintegracao AS si ON si.id_saladeaula = ts.id_saladeaula
LEFT JOIN dbo.tb_disciplinaintegracao AS di ON di.id_disciplinaintegracao = si.id_disciplinaintegracao

GO
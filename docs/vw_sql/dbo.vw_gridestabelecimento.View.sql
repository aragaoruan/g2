
GO
/****** Object:  View [dbo].[vw_gridestabelecimento]    Script Date: 10/08/2012 17:42:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_gridestabelecimento] as
SELECT DISTINCT mt.id_matricula, md1.id_serie, se.st_serie , aa.dt_avaliacao, st_instituicao = 
CASE
WHEN aa.dt_avaliacao IS NULL THEN ad.st_instituicao
ELSE et.st_nomeentidade
END,
sg_uf = CASE
WHEN aa.dt_avaliacao IS NULL THEN ad.sg_uf
ELSE ed.sg_uf
END,
st_nomemunicipio = CASE
WHEN aa.dt_avaliacao IS NULL THEN mn.st_nomemunicipio
ELSE mne.st_nomemunicipio
END,apm.nu_anoconclusao,  
md1.nu_ordem, ad.dt_cadastro, md.id_evolucao
FROM dbo.tb_matricula AS mt
JOIN dbo.tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula 
JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
JOIN dbo.tb_modulo AS ml ON ml.id_projetopedagogico = pp.id_projetopedagogico
JOIN dbo.tb_modulodisciplina AS md1 ON md1.id_modulo = ml.id_modulo AND md1.id_disciplina = md.id_disciplina
--CROSS APPLY (select TOP 1 id_areaconhecimento, id_serie, nu_ordem, id_disciplina FROM dbo.tb_modulodisciplina where id_modulo = ml.id_modulo AND id_areaconhecimento = md1.id_areaconhecimento AND md1.id_serie = id_serie GROUP BY id_serie, nu_ordem, id_areaconhecimento, id_disciplina ORDER BY nu_ordem desc) AS mdc
LEFT JOIN dbo.tb_aproveitamentomatriculadisciplina AS apm ON apm.id_matricula = mt.id_matricula AND apm.id_disciplina = md.id_disciplina
LEFT JOIN dbo.tb_aproveitamentodisciplina AS ad ON ad.id_aproveitamentodisciplina = apm.id_aproveitamentodisciplina
LEFT JOIN dbo.vw_avaliacaoaluno AS aa ON aa.id_disciplina = md.id_disciplina AND aa.id_matricula = mt.id_matricula
LEFT JOIN dbo.tb_municipio AS mn ON mn.id_municipio = ad.id_municipio
JOIN dbo.tb_serie AS se ON se.id_serie = md1.id_serie
JOIN dbo.tb_entidade AS et ON et.id_entidade = mt.id_entidadematriz
JOIN dbo.tb_entidadeendereco AS ee ON ee.id_entidade = et.id_entidade AND ee.bl_padrao = 1
JOIN dbo.tb_endereco AS ed ON ed.id_endereco = ee.id_endereco
JOIN dbo.tb_municipio AS mne ON mne.id_municipio = ed.id_municipio
GO

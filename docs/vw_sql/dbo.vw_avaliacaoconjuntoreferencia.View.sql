SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_avaliacaoconjuntoreferencia] as
select
ac.st_avaliacaoconjunto, tp.st_tipoprova, acr.*
from tb_avaliacaoconjuntoreferencia acr
JOIN tb_avaliacaoconjunto as ac ON ac.id_avaliacaoconjunto = acr.id_avaliacaoconjunto
JOIN tb_tipoprova as tp ON tp.id_tipoprova = ac.id_tipoprova
GO

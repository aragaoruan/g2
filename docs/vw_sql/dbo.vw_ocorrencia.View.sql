CREATE VIEW [dbo].[vw_ocorrencia]
AS
SELECT DISTINCT
  oc.id_ocorrencia,
  uso.st_cpf AS st_cpf,
  uso.st_nomecompleto AS st_nomeinteressado,
  tc1.st_email,
  uso.st_senha, uso.st_login,
  (CAST(tc3.nu_ddd AS VARCHAR) + '-' + CAST(tc3.nu_telefone AS VARCHAR)) AS st_telefone,
  oc.id_matricula,
  em.st_urlportal,
  oc.id_evolucao,
  oc.id_situacao,
  oc.id_usuariointeressado,
  oc.id_categoriaocorrencia,
  oc.id_assuntoco,
  oc.id_saladeaula,
  oc.st_titulo,
  oc.dt_cadastro,
  (CAST(CONVERT(VARCHAR, oc.dt_cadastro, 103) + ' - ' + FORMAT(oc.dt_cadastro , 'HH:mm') AS VARCHAR(MAX))) AS st_cadastro,
  oc.st_ocorrencia,
  oc.id_entidade,
  dbo.tb_situacao.st_situacao,
  dbo.tb_evolucao.st_evolucao,
  dbo.tb_assuntoco.st_assuntoco,
  dbo.tb_assuntoco.id_assuntocopai,
  dbo.tb_assuntoco.id_tipoocorrencia,
  dbo.tb_categoriaocorrencia.st_categoriaocorrencia,
  (CAST(CONVERT(VARCHAR, oc.dt_ultimotramite, 103) + ' - ' + FORMAT(oc.dt_ultimotramite , 'HH:mm') AS VARCHAR(MAX))) AS st_ultimotramite,
  oc.st_tramite,
  oc.dt_ultimotramite AS dt_ultimotramite,
  (case WHEN oc.dt_ultimotramite is null then oc.dt_cadastro else oc.dt_ultimotramite end) AS dt_ultimaacao,
  oresp.id_usuario AS id_usuarioresponsavel,
    st_nomeresponsavel =
    CASE
    WHEN usr.id_usuario IS NULL THEN 'Năo distribuído'
    ELSE usr.st_nomecompleto
    END,
  GETDATE() - 1 AS dt_atendimento,
  asp.st_assuntoco AS st_assuntocopai
  ,pp.id_projetopedagogico
  ,pp.st_projetopedagogico
  ,sa.st_saladeaula
  , mt.id_evolucao AS id_evolucaomatricula
  , oc.id_ocorrenciaoriginal,
  oc.st_codissue,
  oc.bl_confirmarciencia
FROM dbo.tb_ocorrencia AS oc
  JOIN tb_usuario AS uso
    ON uso.id_usuario = oc.id_usuariointeressado
  INNER JOIN dbo.tb_situacao
    ON dbo.tb_situacao.id_situacao = oc.id_situacao
  INNER JOIN dbo.tb_evolucao
    ON dbo.tb_evolucao.id_evolucao = oc.id_evolucao
  INNER JOIN dbo.tb_categoriaocorrencia
    ON dbo.tb_categoriaocorrencia.id_categoriaocorrencia = oc.id_categoriaocorrencia
  INNER JOIN dbo.tb_assuntoco
    ON oc.id_assuntoco = dbo.tb_assuntoco.id_assuntoco
  LEFT JOIN dbo.tb_assuntoco AS asp
    ON asp.id_assuntoco = dbo.tb_assuntoco.id_assuntocopai
  LEFT JOIN dbo.tb_ocorrenciaresponsavel AS oresp
    ON oresp.id_ocorrencia = oc.id_ocorrencia
       AND oresp.bl_ativo = 1
  LEFT JOIN dbo.tb_usuario AS usr
    ON usr.id_usuario = oresp.id_usuario
  LEFT JOIN tb_contatosemailpessoaperfil tc
    ON uso.id_usuario = tc.id_usuario
       AND tc.bl_padrao = 1
       AND tc.bl_ativo = 1
       AND oc.id_entidade = tc.id_entidade
  LEFT JOIN tb_contatosemail tc1
    ON tc.id_email = tc1.id_email
  LEFT JOIN tb_contatostelefonepessoa tc2
    ON uso.id_usuario = tc2.id_usuario
       AND tc2.bl_padrao = 1
       AND oc.id_entidade = tc2.id_entidade
  LEFT JOIN tb_contatostelefone tc3
    ON tc2.id_telefone = tc3.id_telefone
--alteraçőes para impressăo
  LEFT JOIN dbo.tb_matricula AS mt
    ON mt.id_matricula = oc.id_matricula

  LEFT JOIN dbo.tb_entidade as em
    ON em.id_entidade = mt.id_entidadematricula

  LEFT JOIN dbo.tb_projetopedagogico AS pp
    ON pp.id_projetopedagogico = mt.id_projetopedagogico
  LEFT JOIN dbo.tb_saladeaula AS sa
    ON sa.id_saladeaula = oc.id_saladeaula
GO


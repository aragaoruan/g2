
GO
/****** Object:  View [dbo].[vw_professorsaladeaula]    Script Date: 10/08/2012 17:42:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_professorsaladeaula] as

select p.id_entidade, e.st_nomeentidade, p.id_usuario, uper.id_perfilreferencia, perf.id_perfilpedagogico, u.st_nomecompleto, uper.bl_ativo, perf.id_perfil, perf.st_nomeperfil, uper.bl_titular, sda.id_saladeaula, sda.st_saladeaula,
ui.st_loginintegrado, ui.st_senhaintegrada,  ei.st_codsistema AS st_codsistemaent, si.st_codsistemacurso, ui.st_codusuario
from tb_pessoa as p
JOIN tb_usuarioperfilentidadereferencia as uper on p.id_usuario = uper.id_usuario 
--AND p.id_entidade = uper.id_entidade
JOIN dbo.tb_perfilreferenciaintegracao AS pri ON pri.id_perfilreferencia = uper.id_perfilreferencia
--JOIN tb_saladeaulaprofessor as sdap on uper.id_perfilreferencia = sdap.id_perfilreferencia
LEFT JOIN dbo.tb_saladeaulaintegracao AS si ON pri.id_saladeaulaintegracao = si.id_saladeaulaintegracao
JOIN tb_saladeaula as sda on si.id_saladeaula = sda.id_saladeaula
JOIN tb_entidade as e on e.id_entidade = sda.id_entidade
JOIN tb_perfil as perf on perf.id_perfil = uper.id_perfil and perf.id_entidade = uper.id_entidade
JOIN tb_usuario as u on u.id_usuario = p.id_usuario
JOIN dbo.tb_usuariointegracao AS ui ON ui.id_usuario = u.id_usuario AND ui.id_entidade = sda.id_entidade AND ui.id_sistema = si.id_sistema
JOIN dbo.tb_entidadeintegracao AS ei ON ei.id_entidade = sda.id_entidade AND ei.id_sistema = si.id_sistema
GO

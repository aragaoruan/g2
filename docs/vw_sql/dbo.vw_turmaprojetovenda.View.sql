CREATE view [dbo].[vw_turmaprojetovenda] AS
  select distinct v.id_entidade,
  pp.id_projetopedagogico,
  pp.st_projetopedagogico,
  p.id_produto,
  p.st_produto,
  t.st_turma,
  t.id_turma,
  u.id_usuario,
  u.st_nomecompleto,
  u.st_cpf,
  mat.id_matricula,
  v.id_venda,
  v.dt_cadastro,
  mat.id_situacao,
  sit.st_situacao,
  v.nu_valorliquido
FROM tb_venda as v
  JOIN tb_vendaproduto as vp on v.id_venda = vp.id_venda
  JOIN tb_produto as p on p.id_produto = vp.id_produto
  JOIN tb_tipoproduto as tp on tp.id_tipoproduto = p.id_tipoproduto and p.id_tipoproduto = 1
  JOIN tb_produtoprojetopedagogico as ppp on p.id_produto = ppp.id_produto
JOIN tb_projetopedagogico as pp on pp.id_projetopedagogico = ppp.id_projetopedagogico
LEFT JOIN tb_turmaprojeto as tpp on tpp.id_projetopedagogico = pp.id_projetopedagogico
LEFT JOIN  tb_turma as t on t.id_turma = tpp.id_turma and t.bl_ativo = 1
  JOIN tb_usuario as u on u.id_usuario = v.id_usuario
  JOIN tb_matricula as mat on mat.id_matricula = vp.id_matricula and mat.bl_ativo = 1 and mat.id_situacao = 50
  JOIN tb_situacao as sit on sit.id_situacao = mat.id_situacao

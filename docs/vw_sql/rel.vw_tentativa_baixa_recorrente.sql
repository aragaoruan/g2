
/*
	FIN-300
	Como Financeiro G2 quero um relat�rio que recupere da Braspag as tentativas de baixar parcelas.
	Essas tentativas de baixa podem ser resultantes de uma nova venda ou de vendas j� efetivadas cujas parcelas s�o recorrentes.

	O relat�rio dever� oferecer par�metros de pesquisa que possibilite filtrar os resultado por:
	Per�odo - inicio e fim do per�odo de tentativas.
	Trazer carregado por padr�o a data corrente nos dois campos.
	As datas de inicio e fim ser�o utilizadas INCLUSIVE na pesquisa
	Aluno / email - Nome do aluno ou ent�o o email do mesmo
	Venda - Identifica��o da venda

	O relat�rio dever� trazer as seguintes colunas:


	Dever� existir uma op��o de gerar o XLS.
	OBS: Estou entendendo que uma tentativa de baixa pode ser relacionada a uma nova venda ou ent�o a uma parcela que � recorrente.

*/

 CREATE VIEW [rel].[vw_tentativa_baixa_recorrente] AS

	SELECT  DISTINCT
		v.id_venda , 
		v.id_entidade,
		v.dt_cadastro , 
		vwp.st_nomecompleto ,
		vwp.st_email,
		--vp.id_matricula  ,
		CAST(v.id_venda AS VARCHAR)+'G2U' AS st_contratofluxus,
		l.id_lancamento ,
		l.dt_vencimento ,
		l.nu_valor ,
		tf.un_transacaofinanceira ,
		CAST(l.nu_tentativabraspag AS VARCHAR) AS st_tentativabraspag,
		tf.dt_cadastro AS dt_tentativa,
		un_orderid,
		l.nu_quitado,
		CASE WHEN bl_quitado = 0 THEN 'Pendente' WHEN bl_quitado = 1 THEN 'Baixado' ELSE 'Indefinido' END AS st_situacao,
		l.st_statuslan,
		l.bl_quitado,
		CAST(lv.nu_ordem AS VARCHAR) AS st_ordem
		
		
	FROM  tb_venda v
		INNER JOIN tb_lancamentovenda lv ON lv.id_venda = v.id_venda
		INNER JOIN tb_lancamento l ON l.id_lancamento = lv.id_lancamento
		INNER JOIN vw_pessoa vwp ON v.id_usuario = vwp.id_usuario
		--LEFT JOIN tb_vendaproduto vp ON vp.id_venda = v.id_venda
		LEFT JOIN tb_transacaolancamento tl ON tl.id_lancamento = l.id_lancamento
		LEFT JOIN tb_transacaofinanceira tf ON tf.id_transacaofinanceira = tl.id_transacaofinanceira
	
	WHERE l.id_meiopagamento = 11
	
GO
	
	
	
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vw_campanhaformapagamento] as
select
cc.id_campanhacomercial, cc.st_campanhacomercial,cf.id_formapagamento, fp.st_formapagamento, cc.id_entidade
from tb_campanhacomercial as cc
JOIN tb_campanhaformapagamento as cf ON cf.id_campanhacomercial = cc.id_campanhacomercial
JOIN tb_formapagamento as fp ON fp.id_formapagamento = cf.id_formapagamento
GO

USE [G2_UNY]
GO

/****** Object:  View [dbo].[vw_salahorarioprofessor]    Script Date: 02/05/2013 16:40:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 CREATE VIEW [dbo].[vw_salahorarioprofessor] as
SELECT ts.id_localaula, ts.id_horarioaula, ts.id_saladeaula, tu.id_usuario FROM tb_usuarioperfilentidadereferencia tu
JOIN tb_salahorariolocal ts ON tu.id_saladeaula = ts.id_saladeaula
WHERE tu.bl_titular = 1 AND tu.bl_ativo = 1
GO


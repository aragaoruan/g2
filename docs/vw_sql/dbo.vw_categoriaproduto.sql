USE [G2H_EAD1]
GO

/****** Object:  View [dbo].[vw_categoriaproduto]    Script Date: 28-04-2015 11:02:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER VIEW [dbo].[vw_categoriaproduto] AS

SELECT
  c.id_categoria,
  c.st_nomeexibicao,
  c.st_categoria,
  c.id_usuariocadastro,
  ce.id_entidade AS id_entidadecadastro,
  c.id_uploadimagem,
  c.id_categoriapai,
  c.id_situacao,
  c.dt_cadastro,
  c.bl_ativo,
  cp.id_produto,
  p.st_produto
FROM
  tb_categoria AS c
	LEFT JOIN tb_categoriaproduto AS cp ON (c.id_categoria = cp.id_categoria)
	JOIN dbo.tb_categoriaentidade AS ce ON ce.id_categoria = c.id_categoria
	JOIN dbo.tb_produto AS p ON cp.id_produto = p.id_produto


GO



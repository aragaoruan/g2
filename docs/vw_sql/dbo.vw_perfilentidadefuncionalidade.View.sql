
GO

/****** Object:  View [dbo].[vw_perfilentidadefuncionalidade]    Script Date: 20/02/2015 15:06:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 CREATE VIEW [dbo].[vw_perfilentidadefuncionalidade]
AS

SELECT
    pe.id_entidade,
    p.id_perfil,
    p.id_perfilpai,
    pf.id_funcionalidade,
    f.st_urlicone,
    p.st_nomeperfil,
    f.st_funcionalidade,
    f.bl_pesquisa,
    ef.st_label,
    f.id_funcionalidadepai,
    f.bl_ativo,
    f.st_classeflex,
    tf.st_tipofuncionalidade,
	f.id_situacao,
    f.id_sistema,
    CASE WHEN ef.nu_ordem IS NOT NULL THEN ef.nu_ordem ELSE f.nu_ordem END AS nu_ordem,
    f.st_classeflexbotao,
    ef.bl_obrigatorio,
    ef.bl_visivel,
    f.bl_lista,
    f.st_ajuda,
    f.id_tipofuncionalidade,
    f.st_target,
	f.st_urlcaminho,
	f.st_urlcaminhoedicao,
	f.bl_visivel AS bl_funcionalidadevisivel,
	f.bl_relatorio,
	f.bl_delete

FROM tb_perfil AS p
JOIN dbo.tb_perfilentidade AS pe ON pe.id_perfil = p.id_perfil
JOIN tb_perfilfuncionalidade AS pf ON p.id_perfil = pf.id_perfil AND  pf.bl_ativo = 1
JOIN tb_entidadefuncionalidade AS ef ON  pf.id_funcionalidade = ef.id_funcionalidade AND ef.id_entidade = pe.id_entidade AND ef.bl_visivel = 1 AND ef.bl_ativo = 1
JOIN tb_funcionalidade AS f ON f.id_funcionalidade = ef.id_funcionalidade
AND f.id_funcionalidade = pf.id_funcionalidade -- AND f.id_situacao = 123
JOIN  tb_tipofuncionalidade tf ON f.id_tipofuncionalidade = tf.id_tipofuncionalidade
WHERE
f.bl_ativo = 1;



GO



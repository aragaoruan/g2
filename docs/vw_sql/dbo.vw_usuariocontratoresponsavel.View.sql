
GO
/****** Object:  View [dbo].[vw_usuariocontratoresponsavel]    Script Date: 10/08/2012 17:42:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_usuariocontratoresponsavel] as 
SELECT 
cr.id_contratoresponsavel,cr.id_usuario, u.st_nomecompleto, cr.id_contrato, id_tipocontratoresponsavel, cr.nu_porcentagem, cr.bl_ativo, 
vd.id_venda
FROM tb_contratoresponsavel as cr
JOIN tb_usuario as u on u.id_usuario = cr.id_usuario
JOIN tb_contrato as ct ON ct.id_contrato = cr.id_contrato
LEFT JOIN tb_venda as vd ON vd.id_venda = ct.id_venda
GO

﻿GO
/****** Object:  View [dbo].[vw_textosistema]    Script Date: 10/08/2012 17:42:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_textosistema] AS
  SELECT
    ts.*,
    te.st_textoexibicao,
    tc.st_textocategoria,
    s.st_situacao,
    s.st_descricaosituacao,
    ot.st_orientacaotexto
  FROM tb_textosistema ts
    JOIN tb_textoexibicao AS te
      ON ts.id_textoexibicao = te.id_textoexibicao
    JOIN tb_textocategoria AS tc
      ON tc.id_textocategoria = ts.id_textocategoria
    JOIN tb_situacao AS s
      ON ts.id_situacao = s.id_situacao
    INNER JOIN tb_orientacaotexto ot
      ON ts.id_orientacaotexto = ot.id_orientacaotexto
  WHERE bl_edicao = 1
GO


GO
/****** Object:  View [rel].[vw_alunoscursandostatus]    Script Date: 10/08/2012 17:42:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [rel].[vw_alunoscursandostatus] as
SELECT  mt.id_matricula ,
        us.st_nomecompleto ,
      '('+  CAST (ct.nu_ddd AS VARCHAR) + ') ' + cast(ct.nu_telefone AS varchar) as nu_telefone ,
        mt.dt_inicio ,
        ev.st_evolucao ,
        aa.dt_avaliacao ,
        vl1.dt_quitado ,
        vl2.dt_vencimento ,
        md.st_faltantes,
        mt.id_projetopedagogico,
        mt.id_entidadeatendimento
FROM    tb_matricula AS mt
        JOIN tb_usuario AS us ON us.id_usuario = mt.id_usuario
        LEFT JOIN dbo.tb_contatostelefonepessoa AS ctp ON ctp.id_usuario = us.id_usuario
                                                          AND ctp.id_entidade = mt.id_entidadeatendimento
                                                          AND ctp.bl_padrao = 1
        LEFT JOIN dbo.tb_contatostelefone AS ct ON ct.id_telefone = ctp.id_telefone
        JOIN dbo.tb_evolucao AS ev ON ev.id_evolucao = mt.id_evolucao
        OUTER APPLY ( SELECT TOP 1
                                dt_avaliacao
                      FROM      dbo.tb_avaliacaoaluno
                      WHERE     id_matricula = mt.id_matricula
                      ORDER BY  dt_avaliacao DESC
                    ) AS aa
        JOIN dbo.tb_contratomatricula AS cmt ON cmt.id_matricula = mt.id_matricula AND cmt.bl_ativo = 1
        JOIN tb_contrato AS ctt ON ctt.id_contrato = cmt.id_contrato
        OUTER APPLY ( SELECT TOP 1
                                dt_quitado
                      FROM      dbo.vw_vendalancamento
                      WHERE     id_venda = ctt.id_venda
                      ORDER BY  dt_quitado DESC
                    ) AS vl1
        OUTER APPLY ( SELECT TOP 1
                                dt_vencimento
                      FROM      dbo.vw_vendalancamento
                      WHERE     id_venda = ctt.id_venda
                                AND bl_quitado = 0
                      ORDER BY  dt_vencimento ASC
                    ) AS vl2
        CROSS APPLY ( SELECT TOP 1
                                COUNT(id_matriculadisciplina) AS st_faltantes
                      FROM      dbo.tb_matriculadisciplina
                      WHERE     id_matricula = mt.id_matricula
                                AND id_evolucao != 12
                    ) AS md
WHERE mt.id_evolucao = 6 AND mt.bl_ativo = 1 AND mt.bl_institucional = 0
GO

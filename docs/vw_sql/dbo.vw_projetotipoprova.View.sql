
GO
/****** Object:  View [dbo].[vw_projetotipoprova]    Script Date: 10/08/2012 17:42:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_projetotipoprova] AS
select ptp.id_tipoprova as id_tipoprovaresult, tp.id_tipoprova, st_tipoprova, st_descricao, 
	   id_projetopedagogico, nu_taxa from tb_tipoprova as tp
LEFT JOIN tb_projetotipoprova as ptp ON ptp.id_tipoprova = tp.id_tipoprova
UNION
SELECT null as id_tipoprovaresult, *,null as id_projetopedagogico, null as nu_taxa  FROM tb_tipoprova
GO

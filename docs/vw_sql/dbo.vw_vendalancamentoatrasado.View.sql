
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 CREATE VIEW [dbo].[vw_vendalancamentoatrasado] AS

SELECT DISTINCT
	vd.id_venda,
	lc.id_lancamento,
	vd.id_usuario,
	us.st_nomecompleto,
	us.st_cpf,
	(SELECT TOP 1 
			ce.st_email 
		FROM 
			tb_contatosemailpessoaperfil AS cepp 
			LEFT JOIN tb_contatosemail ce ON ce.id_email = cepp.id_email 
		WHERE 
			cepp.id_usuario = vd.id_usuario AND cepp.id_entidade = vd.id_entidade AND cepp.bl_ativo = 1 AND cepp.bl_padrao = 1) 
		AS st_email,
	lc.id_usuariolancamento,
	ul.st_nomecompleto AS st_nomeresponsavel,
	lc.id_entidadelancamento,
	el.st_nomeentidade AS st_entidaderesponsavel,
	vd.id_evolucao, 
	e.st_evolucao,
	vd.nu_parcelas, 
	lc.nu_valor,
	lc.nu_quitado,
	lc.dt_vencimento,
	s.id_situacao AS id_situacaomatricula,
	s.st_situacao AS st_situacaomatricula,
	(CASE 
		WHEN (SELECT bl_temocorrencia FROM vw_vendaocorrencia WHERE id_venda = vd.id_venda) = 1 
		THEN 115
		ELSE 116 END) AS id_situacaolancamento,
	(SELECT st_situacao FROM tb_situacao WHERE id_situacao = (CASE 
		WHEN (SELECT bl_temocorrencia FROM vw_vendaocorrencia WHERE id_venda = vd.id_venda) = 1 
		THEN 115
		ELSE 116 END)) AS st_situacaolancamento,
	vd.id_entidade,
	DATEDIFF(DAY,lc.dt_quitado, GETDATE()) as nu_diasatraso,
	pr.id_produto,
	pr.st_produto
        
FROM 
	tb_venda AS vd 
	JOIN tb_vendaproduto		AS vp ON vd.id_venda		= vp.id_venda
	JOIN tb_produto				AS pr ON vp.id_produto		= pr.id_produto AND pr.bl_ativo = 1
	JOIN tb_usuario				AS us ON us.id_usuario		= vd.id_usuario
	JOIN tb_pessoa				AS p  ON p.id_usuario		= vd.id_usuario
	JOIN tb_lancamentovenda		AS lv ON lv.id_venda		= vd.id_venda
	JOIN tb_lancamento			AS lc ON lc.id_lancamento	= lv.id_lancamento
	JOIN tb_entidadefinanceiro	AS ef ON lc.id_entidade		= ef.id_entidade
	JOIN tb_evolucao			AS e  ON vd.id_evolucao		= e.id_evolucao
	JOIN tb_situacao			AS s  ON vd.id_situacao		= s.id_situacao
	LEFT JOIN tb_entidade		AS el ON el.id_entidade		= lc.id_entidadelancamento
	LEFT JOIN tb_usuario		AS ul ON ul.id_usuario		= lc.id_usuariolancamento

--LEFT JOIN tb_contatosemailpessoaperfil AS cepp ON cepp.id_usuario = us.id_usuario AND cepp.id_entidade = p.id_entidade AND cepp.bl_ativo = 1 AND cepp. bl_padrao = 1
--LEFT JOIN tb_contatosemail AS ce ON ce.id_email = cepp.id_email 
	
WHERE 
	lc.bl_quitado = 1 AND 
	lc.bl_ativo = 1 AND 
	lc.dt_quitado IS NOT NULL AND 
	GETDATE() >= DATEADD(day, ef.nu_avisoatraso, lC.dt_vencimento)
	--AND US.id_usuario = 1

GO
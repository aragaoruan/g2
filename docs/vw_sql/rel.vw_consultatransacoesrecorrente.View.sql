USE [G2_UNY]
GO

/****** Object:  View [dbo].[vw_aplicadorprova]    Script Date: 10/12/2013 11:44:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 CREATE VIEW [rel].[vw_consultatransacoesrecorrente] AS
SELECT DISTINCT
	vw.id_venda,
	CAST(vw.id_venda AS VARCHAR)+ 'G2U' AS st_pedido,
	vw.st_nomecompleto,
	vw.id_meiopagamento,
	vw.st_meiopagamento,
	co.st_cartaooperadora,
	cb.st_cartaobandeira,
	vw.nu_valor,
	vw.nu_quitado,
	vw.dt_vencimento,
	vw.dt_quitado,
	vw.st_situacao,
	l.nu_tentativabraspag,
	tf.st_mensagem,
	tf.id_situacaointegracao,
	vw.nu_parcela,
	si3.st_situacao AS st_situacaointegracao,
	vw.bl_quitado
FROM
	dbo.vw_resumofinanceiro vw
	JOIN dbo.tb_transacaofinanceira tf ON vw.id_venda = tf.id_venda
	JOIN dbo.tb_transacaolancamento tl ON vw.id_lancamento = tl.id_lancamento
	JOIN dbo.tb_lancamento l ON vw.id_lancamento = l.id_lancamento
	LEFT JOIN dbo.tb_cartaobandeira cb ON tf.id_cartaobandeira = cb.id_cartaobandeira
	LEFT JOIN dbo.tb_cartaooperadora co ON tf.id_cartaooperadora = co.id_cartaooperadora
	LEFT JOIN dbo.tb_situacaointegracao si ON tf.id_situacaointegracao = si.id_situacaointegracao
	
OUTER APPLY ( SELECT TOP 1 si2.st_situacao  
       FROM tb_transacaolancamento AS tl 
       INNER JOIN tb_transacaofinanceira AS tf ON tl.id_transacaofinanceira = tf.id_transacaofinanceira
       INNER JOIN tb_situacaointegracao AS si2 ON si2.id_situacaointegracao = tf.id_situacaointegracao
       WHERE tl.id_lancamento = L.id_lancamento
       ORDER BY tf.dt_cadastro DESC
     ) AS si3
     	
WHERE vw.id_meiopagamento = 11

GO
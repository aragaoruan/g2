-- VIEW VW_ETIQUETADECLARACAO --
CREATE VIEW vw_etiquetadeclaracao AS
  SELECT
    DISTINCT
    us.id_usuario AS id_usuario,
    us.st_nomecompleto AS st_nomecompleto,
    us.st_cpf AS st_cpf,
    mt.id_matricula AS id_matricula,
    ec.st_endereco AS st_endereco,
    ec.nu_numero AS st_numero,
    ec.st_bairro AS st_bairro,
    ec.st_cidade AS st_cidade,
    ec.sg_uf AS st_uf,
    ec.st_cep AS st_cep,
    ec.st_complemento AS st_complemento,
    'Declaração' AS st_servico,
    ps.id_entidade AS id_entidade,
    ed.dt_solicitacao AS dt_solicitacao,
    ed.dt_entrega AS dt_entrega,
    ev.st_evolucao AS st_evolucao,
    ce.st_email AS st_email,
    pp.st_projetopedagogico AS st_projetopedagogico
  FROM tb_entregadeclaracao ed
    JOIN tb_matricula mt ON mt.id_matricula = ed.id_matricula
    JOIN tb_projetopedagogico pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
    JOIN tb_usuario us ON us.id_usuario = mt.id_usuario
    JOIN tb_pessoa ps ON ps.id_usuario = us.id_usuario
    JOIN tb_pessoaendereco pe ON pe.id_usuario = us.id_usuario AND pe.id_entidade = ps.id_entidade
    JOIN tb_endereco ec ON ec.id_endereco = pe.id_endereco
    JOIN tb_evolucao ev ON ev.id_evolucao = mt.id_evolucao
    JOIN tb_contatosemailpessoaperfil cp ON cp.id_usuario = us.id_usuario AND cp.id_entidade = ps.id_entidade
    JOIN tb_contatosemail ce ON ce.id_email = cp.id_email
  WHERE pe.bl_padrao = 1 AND ec.id_tipoendereco = 5 AND cp.bl_padrao = 1 AND cp.bl_ativo = 1
GO
-- END --
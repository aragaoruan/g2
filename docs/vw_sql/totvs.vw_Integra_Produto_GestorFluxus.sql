ALTER VIEW [totvs].[vw_Integra_Produto_Venda_GestorFluxus] AS

	SELECT 
			ei.st_codsistema AS CODCOLIGADA_FCFO,
			vi.st_codvenda AS IDMOV_TITMOV,
			NULL AS NSEQITMOV_TITMOV,
			NULL AS NUMEROSEQUENCIAL_TITMOV,
			p.st_codigoprodutoexterno AS IDPRD_TITMOV,
			1 AS QUANTIDADE_TITMOV,
			vp.nu_valorbruto AS PRECOUNITARIO_TITMOV,
			vp.nu_valorbruto AS PRECOTABELA_TITMOV,
			CAST(vd.dt_confirmacao AS DATETIME) AS DATAEMISSAO_TITMOV,
			'UN' AS CODUND_TITMOV,
			1 AS QUANTIDADEARECEBER_TITMOV,
			vp.nu_valorbruto AS VALORUNITARIO_TITMOV,
			vp.nu_valorbruto AS VALORFINANCEIRO_TITMOV,
			--ei.st_codsistema AS CODCOLFCO_TITMOV,
			0 AS ALIQORDENACAO_TITMOV,
			1 AS QUANTIDADEORIGINAL_TITMOV,
			CASE WHEN p.id_tipoproduto = 6 AND ped.sg_uf IS NOT NULL AND ped.sg_uf = 'DF'	THEN 181
				 WHEN p.id_tipoproduto = 6 AND ped.sg_uf IS NOT NULL AND ped.sg_uf <> 'DF'	THEN 182
				 WHEN p.id_tipoproduto <> 6 AND ped.sg_uf IS NOT NULL AND ped.sg_uf = 'DF'	THEN 183
				 WHEN p.id_tipoproduto <> 6 AND ped.sg_uf IS NOT NULL AND ped.sg_uf <> 'DF' THEN 184
				 END AS IDNAT_TITMOV,
			0 AS FLAG_TITMOV,
			0 AS FATORCONVUND_TITMOV,
			vp.nu_valorliquido AS VALORTOTALITEM_TITMOV,
			1 AS CODFILIAL_TITMOV,
			0 AS QUANTIDADESEPARADA_TITMOV,
			0 AS PERCENTCOMISSAO_TITMOV,
			0 AS COMISSAOREPRES_TITMOV,
			0 AS VALORESCRITURACAO_TITMOV,
			0 AS VALORFINPEDIDO_TITMOV,
			0 AS VALORPFRM1_TITMOV,
			0 AS VALORPFRM2_TITMOV,
			0 AS PRECOEDITADO_TITMOV,
			1 AS QTDEVOLUMEUNITARIO_TITMOV,
			'040' AS CST_TITMOV,
			0 AS VALORDESCCONDICONALITM_TITMOV,
			0 AS VALORDESPCONDICIONALITM_TITMOV,
			CAST(vd.dt_confirmacao AS DATETIME) AS DATAORCAMENTO_TITMOV,
			9999 AS CODTBORCAMENTO_TITMOV,
			0 AS VALORUNTORCAMENTO_TITMOV,
			0 AS VALSERVICONFE_TITMOV,
			'01.001' AS CODLOC_TITMOV,
			0 AS VALORBEM_TITMOV,
			vp.nu_valorliquido AS VALORLIQUIDO_TITMOV,
			SUBSTRING ( CAST(emun.id_municipio AS VARCHAR) , 3 , LEN(CAST(emun.id_municipio AS VARCHAR)) ) AS CODMUNSERVICO_TITMOV,
			CODETDMUNSERV_TITMOV = CASE WHEN ede.sg_uf IS NOT NULL  THEN ede.sg_uf ELSE 'DF' END,
			(vp.nu_valorbruto - vp.nu_desconto) AS RATEIOCCUSTODEPTO_TITMOV,
			vp.nu_valorbruto AS VALORBRUTOITEM_TITMOV,
			vp.nu_valorbruto AS VALORBRUTOITEMORIG_TITMOV,
			ei.st_codsistema AS CODCOLTBORCAMENTO_TITMOV,
			1 AS QUANTIDADETOTAL_TITMOV,
			0 AS PRODUTOSUBSTITUTO_TITMOV,
			0 AS PRECOUNITARIOSELEC_TITMOV,
			'Venda enviada pelo gestor as '+CONVERT(VARCHAR(8), GETDATE(),  114 ) + ' de ' + CONVERT(VARCHAR, GETDATE(),  103  ) AS HISTORICOCURTO,
			vp.nu_valorliquido AS VALOR,
			100 AS PERCENTUAL,
			ei.st_codchave AS CODCCUSTO,
			vd.id_venda,
			p.id_produto,
			vd.id_entidade

		FROM tb_venda vd	
			CROSS APPLY ( SELECT TOP 1 * 
							FROM tb_entidadeintegracao ei
							WHERE ei.id_entidade  = vd.id_entidade 
								AND ei.id_sistema = 29 
						) AS ei
			JOIN tb_vendaproduto AS vp ON vd.id_venda = vp.id_venda
			JOIN tb_produto AS p ON p.id_produto = vp.id_produto
			JOIN tb_vendaintegracao vi ON vi.id_venda = vd.id_venda AND vi.id_sistema = 29 AND vi.st_codvenda IS NOT NULL
			OUTER APPLY(SELECT TOP 1 * 
							FROM tb_entidadeendereco AS eeo 
							WHERE eeo.id_entidade = vd.id_entidade
								AND eeo.bl_padrao = 1
						) as peeo
	
			JOIN tb_endereco AS ede ON ede.id_endereco = peeo.id_endereco
			JOIN tb_municipio AS emun ON ede.id_municipio = emun.id_municipio

			OUTER APPLY(
				SELECT mun.sg_uf 
					FROM tb_usuario usr 
					JOIN tb_pessoa AS ps ON ps.id_usuario = usr.id_usuario AND ps.id_entidade = vd.id_entidade
					JOIN tb_pessoaendereco  AS pseo ON pseo.id_usuario = ps.id_usuario AND ps.id_entidade = pseo.id_entidade AND pseo.bl_padrao = 1
					JOIN tb_endereco AS ed ON ed.id_endereco = pseo.id_endereco
					JOIN tb_municipio AS mun ON ed.id_municipio = mun.id_municipio
				WHERE usr.id_usuario = vd.id_usuario
				) ped

		WHERE vd.id_venda NOT IN (
			SELECT 
				id_venda
			FROM tb_vendaprodutointegracao
			WHERE id_sistema = 29
				AND id_venda = vd.id_venda
				AND id_produto = p.id_produto
				AND id_entidade = vd.id_entidade
		)

		--WHERE vd.id_venda = 112656
GO

GO

/****** Object:  View [dbo].[vw_pesquisavitrine]    Script Date: 02/15/2013 14:16:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 CREATE VIEW [dbo].[vw_pesquisavitrine] AS
SELECT DISTINCT
	ca.id_contratoafiliado,
	ca.st_contratoafiliado,
	ca.id_entidadeafiliada,
	ca.id_entidadecadastro,
	ca.id_usuarioresponsavel,
	ca.dt_cadastro,
	ca.st_url,
	ca.st_descricao,
	cla.id_tipopessoa,
	cla.id_situacao,
	cla.st_classeafiliado,
	e.st_nomeentidade,
	u.st_nomecompleto,
	s.st_situacao,
	t.st_tipopessoa
FROM 
	tb_contratoafiliado AS ca
	INNER JOIN tb_classeafiliado AS cla ON ca.id_classeafiliado  = cla.id_classeafiliado
	LEFT  JOIN tb_entidade		 AS e   ON e.id_entidade		 = ca.id_entidadeafiliada
	LEFT  JOIN tb_usuario		 AS u   ON u.id_usuario			 = ca.id_usuarioresponsavel
	LEFT  JOIN tb_vitrine		 AS v   ON v.id_contratoafiliado = ca.id_contratoafiliado
	LEFT  JOIN tb_tipopessoa     AS t   ON cla.id_tipopessoa     = t.id_tipopessoa
	LEFT  JOIN tb_situacao       AS s   ON cla.id_situacao       = s.id_situacao	AND s.st_tabela = 'tb_classeafiliado'
GO

GO
/****** Object:  View [dbo].[vw_turnohorarioaula]    Script Date: 10/08/2012 17:42:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_turnohorarioaula] as (
			select ha.*, t.st_turno, hds.id_diasemana
			from tb_turno t, tb_horarioaula ha, tb_horariodiasemana hds
			where t.id_turno = ha.id_turno
			AND hds.id_horarioaula = ha.id_horarioaula
			
			)
GO

CREATE VIEW [dbo].[vw_disciplinaintegracaomoodle] AS
  SELECT
    de.id_disciplinaintegracao,
    de.id_disciplina,
    de.id_sistema,
    de.id_usuariocadastro,
    de.st_codsistema,
    de.dt_cadastro,
    de.id_entidade,
    de.bl_ativo,
    e.st_nomeentidade,
    de.st_salareferencia,
    de.id_entidadeintegracao AS id_moodleintegracao,
    ei.st_titulo AS st_moodleintegracao
  FROM
    tb_disciplinaintegracao de
    LEFT JOIN tb_entidadeintegracao AS ei ON ei.id_entidadeintegracao = de.id_entidadeintegracao
    LEFT JOIN tb_entidade e ON e.id_entidade = de.id_entidade
  WHERE
    de.id_sistema = 6
GO




/****** Object:  View [dbo].[vw_contaentidadefinanceiro]    Script Date: 08/09/2015 16:04:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


Create VIEW [dbo].[vw_contaentidadefinanceiro] AS
SELECT DISTINCT
	 c.id_contaentidade ,
        c.id_entidade ,
        c.id_tipodeconta ,
        c.st_banco ,
        c.st_digitoagencia ,
        c.st_agencia ,
        c.st_conta ,
        c.bl_padrao ,
        c.st_digitoconta, ef.id_entidadefinanceiro,
	ef.nu_diaspagamentoentrada,
	ef.bl_automatricula,
	--ef.nu_multasemcarta,
	eec.st_valor as nu_multasemcarta,
	eec2.st_valor as nu_multacomcarta,
	--ef.nu_multacomcarta,
	tc.st_tipodeconta,
	b.st_nomebanco
FROM
	tb_contaentidade c
left join vw_entidadeesquemaconfiguracao eec on eec.id_entidade = c.id_entidade and eec.id_itemconfiguracao = 15
left join vw_entidadeesquemaconfiguracao eec2 on eec2.id_entidade = c.id_entidade and eec2.id_itemconfiguracao = 16
LEFT JOIN tb_entidadefinanceiro ef ON c.id_entidade = ef.id_entidade
LEFT JOIN tb_tipodeconta tc ON c.id_tipodeconta = tc.id_tipodeconta
LEFT JOIN tb_banco b ON c.st_banco = b.id_banco


GO



USE [G2_UNY]
GO

/****** Object:  View [dbo].[vw_salahorariolocal]    Script Date: 06/05/2013 09:57:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 CREATE VIEW [dbo].[vw_salahorariolocal] as
SELECT
	l.id_localaula,
	h.id_horarioaula,
	l.st_localaula,
	h.st_horarioaula
FROM tb_localaula l
JOIN tb_salahorariolocal shl
	ON shl.id_localaula = l.id_localaula
JOIN tb_horarioaula h
	ON h.id_horarioaula = shl.id_horarioaula
GO


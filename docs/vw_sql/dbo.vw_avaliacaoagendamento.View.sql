CREATE VIEW [dbo].[vw_avaliacaoagendamento] AS
  SELECT
    DISTINCT
    aa.id_avaliacaoagendamento,
    aa.id_matricula,
    us.id_usuario,
    us.st_nomecompleto,
    us.st_cpf,
    av.id_avaliacao,
    av.st_avaliacao,
    ac.id_tipoprova,
    tp.st_tipoprova,
    aa.dt_agendamento,
    aa.bl_automatico,
    aa.id_situacao,
    st.st_situacao,
    aa.id_avaliacaoaplicacao,
    aa.id_entidade,
    et.st_nomeentidade,
    ap.id_horarioaula,
    ha.st_horarioaula,
    mt.id_projetopedagogico,
    pp.st_projetopedagogico,
    pp.bl_possuiprova,
    (CASE
     WHEN pp.bl_possuiprova = 1
       THEN 'SIM'
     ELSE 'NÃO'
     END) AS st_possuiprova,
    aai.st_codsistema,
    aa.bl_ativo,
    ap.st_endereco,
    ap.dt_aplicacao,
    ap.id_aplicadorprova,
    ap.st_aplicadorprova,
    ap.st_cidade,
    aa.nu_presenca,
    aa.id_usuariocadastro,
    aa.id_usuariolancamento,
    usu_lancamento.st_nomecompleto AS st_usuariolancamento,
    av.id_tipoavaliacao,
-- Esse campo define o index da prova de recuperação (0 = não recuperação / 1 = rec1 / 2 = rec2 / etc
    (CASE
     WHEN aa.id_tipodeavaliacao IS NULL
       THEN 0
     ELSE aa.id_tipodeavaliacao
     END) AS id_tipodeavaliacao,
    (CASE
     WHEN (ap.dt_aplicacao IS NOT NULL
           AND ap.dt_aplicacao > Getdate()
           AND aa.bl_ativo = 1
           AND aa.nu_presenca IS NULL)
       THEN 0
     WHEN (ap.dt_aplicacao IS NOT NULL
           AND ap.dt_aplicacao > Getdate()
           AND aa.bl_ativo = 0
           AND aa.id_situacao = 68
           AND aa.nu_presenca IS NOT NULL)
       THEN 1
     WHEN (ap.dt_aplicacao IS NOT NULL
           AND ap.dt_aplicacao < Getdate()
           AND aa.bl_ativo = 1
           AND aa.id_situacao = 68
           AND aa.nu_presenca IS NULL)
       THEN 2
     ELSE 3
     END) AS id_situacaopresenca,
    (CASE
     WHEN (ap.dt_aplicacao IS NOT NULL
           AND ap.dt_aplicacao > Getdate()
           AND aa.bl_ativo = 1
           AND aa.nu_presenca IS NULL)
       THEN 'Aguardando aplicação de prova'
     WHEN (ap.dt_aplicacao IS NOT NULL
           AND ap.dt_aplicacao > Getdate()
           AND aa.bl_ativo = 0
           AND aa.id_situacao = 68
           AND aa.nu_presenca IS NOT NULL)
       THEN 'Presença lançada'
     WHEN (ap.dt_aplicacao IS NOT NULL
           AND ap.dt_aplicacao < Getdate()
           AND aa.bl_ativo = 1
           AND aa.id_situacao = 68
           AND aa.nu_presenca IS NULL)
       THEN 'Aplicação realizada e presença não lançada'
     ELSE 'Situação Indefinida'
     END) AS st_situacaopresenca,
    tb_chamada.id_aval AS id_chamada,
    primeira.id_primeirachamada,
    primeirapresenca.nu_primeirapresenca,
    primeira.id_segundachamada AS id_ultimachamada,
    ultima.nu_ultimapresenca,
    mt.id_evolucao,
    aa.bl_provaglobal,
    ap.dt_antecedenciaminima,
    ap.dt_alteracaolimite,
    (CASE
     WHEN aa.id_avaliacaoaplicacao IS NULL
          OR ((Cast(Getdate() AS DATE)) BETWEEN
              ap.dt_antecedenciaminima AND
              ap.dt_alteracaolimite
              AND aa.id_avaliacaoaplicacao =
                  ap.id_avaliacaoaplicacao
              AND aa.bl_ativo = 1)
       THEN 0
     ELSE 1
     END) AS id_mensagens,
    (CASE
     WHEN (Cast(Getdate() AS DATE)) < ap.dt_antecedenciaminima
       THEN 'A data para o agendamento ainda não chegou'
     WHEN (Cast(Getdate() AS DATE)) > ap.dt_alteracaolimite
       THEN 'Agendamento passou do prazo limite para alteração'
     WHEN aa.id_avaliacaoaplicacao IS NULL
       THEN ''
     ELSE 'Agendamento dentro do prazo limite para alteração'
     END) AS st_mensagens,
    provasrealizadas.id_provasrealizadas,
    Isnull(temnotafinal.bl_temnotafinal, 0) AS bl_temnotafinal,
    pe.st_email,
    pe.nu_ddi,
    pe.nu_ddd,
    pe.nu_telefone,
    pe.nu_ddialternativo,
    pe.nu_dddalternativo,
    pe.nu_telefonealternativo,
    ha.hr_inicio,
    ha.hr_fim,
    ap.sg_uf,
    ap.st_complemento,
    ap.nu_numero,
    ap.st_bairro,
    ap.st_telefone AS 'st_telefoneaplicador',
    NULL AS id_disciplina,
    NULL AS st_disciplina,
    aa.dt_envioaviso,
    NULL AS id_avaliacaoconjuntoreferencia,
    cp.cod_prova,
    (CASE
     WHEN cp.cod_prova IS NOT NULL AND cp.cod_prova != ''
       THEN 1
     ELSE 0
     END) AS bl_temprovaintegrada,
    aa.bl_sincronizado
  FROM
    tb_avaliacaoagendamento aa
    INNER JOIN tb_matricula AS mt ON mt.id_matricula = aa.id_matricula
    INNER JOIN tb_entidade AS ett ON ett.id_entidade = mt.id_entidadeatendimento
    INNER JOIN tb_usuario AS us ON us.id_usuario = mt.id_usuario
    INNER JOIN tb_avaliacao AS av ON av.id_avaliacao = aa.id_avaliacao
    INNER JOIN tb_avaliacaoconjuntorelacao AS acr ON acr.id_avaliacao = av.id_avaliacao
    INNER JOIN tb_avaliacaoconjunto AS ac ON ac.id_avaliacaoconjunto = acr.id_avaliacaoconjunto
    INNER JOIN tb_tipoprova AS tp ON tp.id_tipoprova = ac.id_tipoprova
    INNER JOIN tb_situacao AS st ON st.id_situacao = aa.id_situacao
    INNER JOIN tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
    INNER JOIN vw_pessoa AS pe ON pe.id_usuario = us.id_usuario AND pe.id_entidade = mt.id_entidadematricula
    INNER JOIN tb_esquemaconfiguracao AS ec ON ett.id_esquemaconfiguracao = ec.id_esquemaconfiguracao
    INNER JOIN tb_esquemaconfiguracaoitem AS eci ON eci.id_esquemaconfiguracao = ec.id_esquemaconfiguracao AND
                                                    eci.id_itemconfiguracao = 24 AND eci.st_valor = 0
    LEFT JOIN vw_avaliacaoaplicacao AS ap ON ap.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao AND ap.bl_ativo = 1 AND
                                             ap.id_entidade = aa.id_entidade
    LEFT JOIN tb_entidade AS et ON et.id_entidade = ap.id_entidade
    LEFT JOIN tb_horarioaula AS ha ON ha.id_horarioaula = ap.id_horarioaula
    LEFT JOIN tb_avalagendaintegracao AS aai ON aai.id_avaliacaoagendamento = aa.id_avaliacaoagendamento
    OUTER APPLY (SELECT
                   st_nomecompleto
                 FROM tb_usuario
                 WHERE id_usuario = aa.id_usuariolancamento) AS
                usu_lancamento
    OUTER APPLY (SELECT
                   Count(cha.id_avaliacaoagendamento) AS id_aval
                 FROM tb_avaliacaoagendamento AS cha
                   LEFT JOIN tb_avaliacaoaplicacao AS ch
                     ON ch.id_avaliacaoaplicacao =
                        cha.id_avaliacaoaplicacao
                 WHERE cha.id_matricula = mt.id_matricula
                       AND cha.id_tipodeavaliacao = aa.id_tipodeavaliacao
                       AND cha.id_situacao = 68) AS tb_chamada
    OUTER APPLY (SELECT
                   Min(id_avaliacaoagendamento) AS id_primeirachamada,
                   Max(id_avaliacaoagendamento) AS id_segundachamada
                 FROM tb_avaliacaoagendamento
                 WHERE id_matricula = aa.id_matricula
                       AND id_situacao = 68
                       AND id_tipodeavaliacao = aa.id_tipodeavaliacao) AS
                primeira
    OUTER APPLY (SELECT
                   nu_presenca AS nu_primeirapresenca
                 FROM tb_avaliacaoagendamento
                 WHERE id_avaliacaoagendamento =
                       (SELECT
                          Min(id_avaliacaoagendamento)
                        FROM tb_avaliacaoagendamento
                        WHERE id_matricula = aa.id_matricula
                              AND id_situacao = 68
                              AND id_tipodeavaliacao =
                                  aa.id_tipodeavaliacao
                       )) AS primeirapresenca
    OUTER APPLY (SELECT
                   nu_presenca AS nu_ultimapresenca
                 FROM tb_avaliacaoagendamento
                 WHERE id_avaliacaoagendamento =
                       (SELECT
                          Max(id_avaliacaoagendamento)
                        FROM tb_avaliacaoagendamento
                        WHERE id_matricula = aa.id_matricula
                              AND id_situacao = 68
                              AND id_tipodeavaliacao =
                                  aa.id_tipodeavaliacao
                       )) AS ultima
    OUTER APPLY (SELECT
                   Count(id_avaliacaoagendamento) AS
                   id_provasrealizadas
                 FROM tb_avaliacaoagendamento
                 WHERE id_matricula = aa.id_matricula
                       AND nu_presenca = 1
                       AND bl_ativo = 0
                       AND id_situacao = 68) AS provasrealizadas
    OUTER APPLY (SELECT
                   (CASE
                    WHEN st_nota IS NULL
                      THEN 0
                    ELSE 1
                    END) AS bl_temnotafinal
                 FROM tb_avaliacaoaluno
                 WHERE id_matricula = mt.id_matricula
                       AND id_avaliacao = aa.id_avaliacao
                       AND id_situacao = 86) AS temnotafinal
    OUTER APPLY (
                  SELECT
                    (CASE
                     WHEN aa.bl_provaglobal = 1
                       THEN pp.st_codprovaintegracao
                     ELSE NULL
                     END) AS cod_prova
                ) AS cp
  WHERE
    aa.id_situacao <> 70
    AND ac.id_tipoprova = 2
  UNION
  SELECT
    DISTINCT
    aa.id_avaliacaoagendamento,
    aa.id_matricula,
    us.id_usuario,
    us.st_nomecompleto,
    us.st_cpf,
    av.id_avaliacao,
    av.st_avaliacao,
    ac.id_tipoprova,
    tp.st_tipoprova,
    aa.dt_agendamento,
    aa.bl_automatico,
    aa.id_situacao,
    st.st_situacao,
    aa.id_avaliacaoaplicacao,
    aa.id_entidade,
    et.st_nomeentidade,
    ap.id_horarioaula,
    ha.st_horarioaula,
    mt.id_projetopedagogico,
    pp.st_projetopedagogico,
    pp.bl_possuiprova,
    (CASE
     WHEN pp.bl_possuiprova = 1
       THEN 'SIM'
     ELSE 'NÃO'
     END) AS st_possuiprova,
    aai.st_codsistema,
    aa.bl_ativo,
    ap.st_endereco,
    ap.dt_aplicacao,
    ap.id_aplicadorprova,
    ap.st_aplicadorprova,
    ap.st_cidade,
    aa.nu_presenca,
    aa.id_usuariocadastro,
    aa.id_usuariolancamento,
    usu_lancamento.st_nomecompleto AS st_usuariolancamento,
    av.id_tipoavaliacao,
    (CASE
     WHEN aa.id_tipodeavaliacao IS NULL
       THEN 0
     ELSE aa.id_tipodeavaliacao
     END) AS id_tipodeavaliacao,
    (CASE
     WHEN (ap.dt_aplicacao IS NOT NULL
           AND ap.dt_aplicacao > Getdate()
           AND aa.bl_ativo = 1
           AND aa.nu_presenca IS NULL)
       THEN 0
     WHEN (ap.dt_aplicacao IS NOT NULL
           AND ap.dt_aplicacao > Getdate()
           AND aa.bl_ativo = 0
           AND aa.id_situacao = 68
           AND aa.nu_presenca IS NOT NULL)
       THEN 1
     WHEN (ap.dt_aplicacao IS NOT NULL
           AND ap.dt_aplicacao < Getdate()
           AND aa.bl_ativo = 1
           AND aa.id_situacao = 68
           AND aa.nu_presenca IS NULL)
       THEN 2
     ELSE 3
     END) AS id_situacaopresenca,
    (CASE
     WHEN (ap.dt_aplicacao IS NOT NULL
           AND ap.dt_aplicacao > Getdate()
           AND aa.bl_ativo = 1
           AND aa.nu_presenca IS NULL)
       THEN 'Aguardando aplicação de prova'
     WHEN (ap.dt_aplicacao IS NOT NULL
           AND ap.dt_aplicacao > Getdate()
           AND aa.bl_ativo = 0
           AND aa.id_situacao = 68
           AND aa.nu_presenca IS NOT NULL)
       THEN 'Presença lançada'
     WHEN (ap.dt_aplicacao IS NOT NULL
           AND ap.dt_aplicacao < Getdate()
           AND aa.bl_ativo = 1
           AND aa.id_situacao = 68
           AND aa.nu_presenca IS NULL)
       THEN 'Aplicação realizada e presença não lançada'
     ELSE 'Situação Indefinida'
     END) AS st_situacaopresenca,
    tb_chamada.id_aval AS id_chamada,
    primeira.id_primeirachamada,
    primeirapresenca.nu_primeirapresenca,
    primeira.id_segundachamada AS id_ultimachamada,
    ultima.nu_ultimapresenca,
    mt.id_evolucao,
    aa.bl_provaglobal,
    ap.dt_antecedenciaminima,
    ap.dt_alteracaolimite,
    (CASE
     WHEN aa.id_avaliacaoaplicacao IS NULL
          OR ((Cast(Getdate() AS DATE)) BETWEEN
              ap.dt_antecedenciaminima AND
              ap.dt_alteracaolimite
              AND aa.id_avaliacaoaplicacao =
                  ap.id_avaliacaoaplicacao
              AND aa.bl_ativo = 1)
       THEN 0
     ELSE 1
     END) AS id_mensagens,
    (CASE
     WHEN (Cast(Getdate() AS DATE)) < ap.dt_antecedenciaminima
       THEN 'A data para o agendamento ainda não chegou'
     WHEN (Cast(Getdate() AS DATE)) > ap.dt_alteracaolimite
       THEN 'Agendamento passou do prazo limite para alteração'
     WHEN aa.id_avaliacaoaplicacao IS NULL
       THEN ''
     ELSE 'Agendamento dentro do prazo limite para alteração'
     END) AS st_mensagens,
    provasrealizadas.id_provasrealizadas,
    Isnull(temnotafinal.bl_temnotafinal, 0) AS bl_temnotafinal,
    pe.st_email,
    pe.nu_ddi,
    pe.nu_ddd,
    pe.nu_telefone,
    pe.nu_ddialternativo,
    pe.nu_dddalternativo,
    pe.nu_telefonealternativo,
    ha.hr_inicio,
    ha.hr_fim,
    ap.sg_uf,
    ap.st_complemento,
    ap.nu_numero,
    ap.st_bairro,
    ap.st_telefone AS 'st_telefoneaplicador',
    md.id_disciplina,
    dis.st_disciplina,
    aa.dt_envioaviso,
    acrf.id_avaliacaoconjuntoreferencia,
    cp.cod_prova,
    (CASE
     WHEN cp.cod_prova IS NOT NULL AND cp.cod_prova != ''
       THEN 1
     ELSE 0
     END) AS bl_temprovaintegrada,
    aa.bl_sincronizado
  FROM
    tb_avaliacaoagendamento aa
    INNER JOIN tb_avalagendamentoref AS agr ON aa.id_avaliacaoagendamento = agr.id_avaliacaoagendamento
    INNER JOIN tb_avaliacaoconjuntoreferencia AS acrf ON acrf.id_avaliacaoconjuntoreferencia =
                                                         agr.id_avaliacaoconjuntoreferencia
    INNER JOIN tb_matricula AS mt ON mt.id_matricula = aa.id_matricula
    INNER JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
    INNER JOIN tb_disciplina AS dis ON md.id_disciplina = dis.id_disciplina
    INNER JOIN tb_alocacao AS al ON al.id_matriculadisciplina = md.id_matriculadisciplina AND al.bl_ativo = 1 AND
                                    al.id_saladeaula = acrf.id_saladeaula
    INNER JOIN tb_entidade AS ett ON ett.id_entidade = mt.id_entidadeatendimento
    INNER JOIN tb_usuario AS us ON us.id_usuario = mt.id_usuario
    INNER JOIN tb_avaliacao AS av ON av.id_avaliacao = aa.id_avaliacao
    INNER JOIN tb_avaliacaoconjuntorelacao AS acr ON acr.id_avaliacao = av.id_avaliacao
    INNER JOIN tb_avaliacaoconjunto AS ac ON ac.id_avaliacaoconjunto = acr.id_avaliacaoconjunto
    INNER JOIN tb_tipoprova AS tp ON tp.id_tipoprova = ac.id_tipoprova
    INNER JOIN tb_situacao AS st ON st.id_situacao = aa.id_situacao
    INNER JOIN tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
    INNER JOIN vw_pessoa AS pe ON pe.id_usuario = us.id_usuario AND pe.id_entidade = mt.id_entidadematricula
    INNER JOIN tb_esquemaconfiguracao AS ec ON ett.id_esquemaconfiguracao = ec.id_esquemaconfiguracao
    INNER JOIN tb_esquemaconfiguracaoitem AS eci ON eci.id_esquemaconfiguracao = ec.id_esquemaconfiguracao AND
                                                    eci.id_itemconfiguracao = 24 AND eci.st_valor = 1
    LEFT JOIN vw_avaliacaoaplicacao AS ap ON ap.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao AND ap.bl_ativo = 1 AND
                                             ap.id_entidade = aa.id_entidade
    LEFT JOIN tb_entidade AS et ON et.id_entidade = ap.id_entidade
    LEFT JOIN tb_horarioaula AS ha ON ha.id_horarioaula = ap.id_horarioaula
    LEFT JOIN tb_avalagendaintegracao AS aai ON aai.id_avaliacaoagendamento = aa.id_avaliacaoagendamento
    OUTER APPLY (SELECT
                   st_nomecompleto
                 FROM tb_usuario
                 WHERE id_usuario = aa.id_usuariolancamento) AS
                usu_lancamento
    OUTER APPLY (SELECT
                   Count(cha.id_avaliacaoagendamento) AS id_aval
                 FROM tb_avaliacaoagendamento AS cha
                   INNER JOIN tb_avalagendamentoref AS cagr ON
                                                              cagr.id_avaliacaoagendamento = cha.id_avaliacaoagendamento
                                                              AND cagr.id_avaliacaoconjuntoreferencia =
                                                                  acrf.id_avaliacaoconjuntoreferencia
                   LEFT JOIN tb_avaliacaoaplicacao AS ch ON ch.id_avaliacaoaplicacao = cha.id_avaliacaoaplicacao
                 WHERE cha.id_matricula = mt.id_matricula
                       AND cha.id_tipodeavaliacao = aa.id_tipodeavaliacao
                       AND cha.id_situacao = 68) AS tb_chamada
    OUTER APPLY (SELECT
                   Min(id_avaliacaoagendamento) AS id_primeirachamada,
                   Max(id_avaliacaoagendamento) AS id_segundachamada
                 FROM tb_avaliacaoagendamento
                 WHERE id_matricula = aa.id_matricula
                       AND id_situacao = 68
                       AND id_tipodeavaliacao = aa.id_tipodeavaliacao) AS
                primeira
    OUTER APPLY (SELECT
                   nu_presenca AS nu_primeirapresenca
                 FROM tb_avaliacaoagendamento
                 WHERE id_avaliacaoagendamento =
                       (SELECT
                          Min(id_avaliacaoagendamento)
                        FROM tb_avaliacaoagendamento
                        WHERE id_matricula = aa.id_matricula
                              AND id_situacao = 68
                              AND id_tipodeavaliacao = aa.id_tipodeavaliacao
                       )) AS primeirapresenca
    OUTER APPLY (SELECT
                   nu_presenca AS nu_ultimapresenca
                 FROM tb_avaliacaoagendamento
                 WHERE id_avaliacaoagendamento =
                       (SELECT
                          Max(id_avaliacaoagendamento)
                        FROM tb_avaliacaoagendamento
                        WHERE id_matricula = aa.id_matricula
                              AND id_situacao = 68
                              AND id_tipodeavaliacao =
                                  aa.id_tipodeavaliacao
                       )) AS ultima
    OUTER APPLY (SELECT
                   Count(id_avaliacaoagendamento) AS
                   id_provasrealizadas
                 FROM tb_avaliacaoagendamento
                 WHERE id_matricula = aa.id_matricula
                       AND nu_presenca = 1
                       AND bl_ativo = 0
                       AND id_situacao = 68) AS provasrealizadas
    OUTER APPLY (SELECT
                   (CASE
                    WHEN st_nota IS NULL
                      THEN 0
                    ELSE 1
                    END) AS bl_temnotafinal
                 FROM tb_avaliacaoaluno
                 WHERE id_matricula = mt.id_matricula
                       AND id_avaliacao = aa.id_avaliacao
                       AND id_situacao = 86) AS temnotafinal
    OUTER APPLY (
                  SELECT
                    (CASE
                     WHEN aa.bl_provaglobal = 0 AND av.id_tipoavaliacao = 2
                       THEN dis.st_codprovarecuperacaointegracao
                     WHEN aa.bl_provaglobal = 0 AND av.id_tipoavaliacao <> 2
                       THEN dis.st_codprovaintegracao
                     ELSE NULL
                     END) AS cod_prova
                ) AS cp
  WHERE
    aa.id_situacao <> 70
    AND ac.id_tipoprova = 2
GO


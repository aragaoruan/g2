
GO
/****** Object:  View [dbo].[vw_dadosrelatorio]    Script Date: 10/08/2012 17:41:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_dadosrelatorio] AS
select
r.id_relatorio, 
r.id_tipoorigemrelatorio, 
r.id_funcionalidade,
cr.nu_ordem, 
r.id_usuario, 
r.id_entidade, 
r.st_relatorio, 
r.st_origem, 
r.bl_geral, 
r.dt_cadastro, 
r.st_descricao, 
cr.id_camporelatorio, 
cr.st_camporelatorio,
cr.st_titulocampo,
cr.bl_filtro,
cr.bl_exibido,
cr.bl_obrigatorio,
tco.id_operacaorelatorio,
tco.id_tipocampo,
tco.id_interface,
tpcr.id_tipopropriedadecamporel,
tpcr.st_tipopropriedadecamporel,
pcr.st_valor
from tb_relatorio r
JOIN tb_camporelatorio as cr ON cr.id_relatorio = r.id_relatorio
LEFT JOIN tb_propriedadecamporelatorio as pcr ON pcr.id_camporelatorio = cr.id_camporelatorio
LEFT JOIN tb_tipopropriedadecamporel as tpcr ON tpcr.id_tipopropriedadecamporel = pcr.id_tipopropriedadecamporel
LEFT JOIN tb_tipocampooperacao as tco 
ON CAST(tco.id_interface AS VARCHAR) IN (select st_valor  from tb_propriedadecamporelatorio where id_tipopropriedadecamporel = 2 and cr.id_camporelatorio = id_camporelatorio)and
 CAST(tco.id_tipocampo AS VARCHAR) in (select st_valor from tb_propriedadecamporelatorio where id_tipopropriedadecamporel = 3 and cr.id_camporelatorio = id_camporelatorio) and
 CAST(tco.id_operacaorelatorio AS VARCHAR) in (select st_valor from tb_propriedadecamporelatorio where id_tipopropriedadecamporel = 1 and cr.id_camporelatorio = id_camporelatorio)
GO

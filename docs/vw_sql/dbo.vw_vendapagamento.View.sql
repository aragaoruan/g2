
GO
/****** Object:  View [dbo].[vw_vendapagamento]    Script Date: 10/08/2012 17:42:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_vendapagamento] as

select vd.id_venda, vd.id_evolucao, vd.id_formapagamento, vd.nu_parcelas, vd.nu_valorliquido

, lv.bl_entrada, lv.nu_ordem

, lc.*

, mp.st_meiopagamento

from tb_venda as vd

JOIN tb_lancamentovenda as lv ON lv.id_venda = vd.id_venda

JOIN tb_lancamento as lc ON lc.id_lancamento = lv.id_lancamento

JOIN tb_meiopagamento as mp ON lc.id_meiopagamento = mp.id_meiopagamento
GO

ALTER VIEW [dbo].[vw_matricula_turmainiciada]
AS
SELECT
  DISTINCT
  t.id_turma AS id_turma,
  t.st_turma AS st_turma,
  c.st_categoria AS st_categoria,
  e.st_nomeentidade AS st_entidade,
  e.id_entidade AS id_entidade,
  e.id_entidadecadastro AS id_entidadecadastro,
  ppp.nu_cargahoraria AS st_cargahoraria,
  t.dt_inicio AS dt_inicio,
  ev.id_evolucao AS id_evolucao,
  ev.st_evolucao AS st_evolucao,
  tn.st_turno AS st_turno,
  c.id_categoria,
  qtd1.qtd AS nu_matriculas,
  fp.free_pass AS nu_freepass,
  gd.garantia_duo AS nu_garantiaduo
FROM
  dbo.tb_turma AS t
  JOIN dbo.tb_turmaprojeto AS tp ON tp.id_turma = t.id_turma
                                    AND t.bl_ativo = 1
  JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = tp.id_projetopedagogico
  JOIN dbo.tb_produtoprojetopedagogico AS ppp ON ppp.id_projetopedagogico = pp.id_projetopedagogico AND
                                                 ppp.id_turma = t.id_turma
  JOIN dbo.tb_categoriaproduto AS cp ON cp.id_produto = ppp.id_produto
  JOIN dbo.tb_categoria AS c ON c.id_categoria = cp.id_categoria
  JOIN dbo.tb_entidade AS e ON e.id_entidade = t.id_entidadecadastro
  JOIN dbo.tb_evolucao AS ev ON ev.id_evolucao = t.id_evolucao
  JOIN dbo.tb_turmaturno AS tt ON tt.id_turma = t.id_turma
  JOIN dbo.tb_turno AS tn ON tn.id_turno = tt.id_turno
  CROSS APPLY (
                SELECT
                  COUNT(id_matricula) AS qtd
                FROM tb_vendaproduto AS vpd
                  JOIN tb_venda AS vd ON vd.id_venda = vpd.id_venda
                WHERE vpd.id_turma = t.id_turma
                      AND vpd.dt_cadastro != cast(getdate() - 6 AS DATE)
              ) AS qtd1
  CROSS APPLY (
                SELECT
                  count(*) AS garantia_duo
                FROM tb_venda AS fpvd1
                  INNER JOIN tb_contrato AS fpctr1 ON fpvd1.id_venda = fpctr1.id_venda
                  INNER JOIN tb_contratoregra AS fpctrr1 ON fpctr1.id_contratoregra = fpctrr1.id_contratoregra
                  INNER JOIN tb_vendaproduto AS fpvprd1 ON fpvd1.id_venda = fpvprd1.id_venda
                  INNER JOIN tb_matricula AS fpmtr1 ON fpmtr1.id_vendaproduto = fpvprd1.id_vendaproduto
                WHERE fpctrr1.st_contratoregra LIKE '%duo%'
                      AND fpvprd1.dt_cadastro <= cast(getdate() AS DATE)
                      AND fpvprd1.id_turma = t.id_turma
              ) AS gd
  CROSS APPLY (
                SELECT
                  count(*) AS free_pass
                FROM tb_venda AS fpvd1
                  INNER JOIN tb_contrato AS fpctr1 ON fpvd1.id_venda = fpctr1.id_venda
                  INNER JOIN tb_contratoregra AS fpctrr1 ON fpctr1.id_contratoregra = fpctrr1.id_contratoregra
                  INNER JOIN tb_vendaproduto AS fpvprd1 ON fpvd1.id_venda = fpvprd1.id_venda
                  INNER JOIN tb_matricula AS fpmtr1 ON fpmtr1.id_vendaproduto = fpvprd1.id_vendaproduto
                WHERE fpctrr1.st_contratoregra LIKE '%free%'
                      AND fpvprd1.dt_cadastro <= cast(getdate() AS DATE)
                      AND fpvprd1.id_turma = t.id_turma
              ) AS fp
WHERE t.dt_inicio != cast(getdate() AS DATE)


GO



USE [G2H_EAD1]
GO

/****** Object:  View [dbo].[vw_professorsaladeaulalote]    Script Date: 12/05/2015 16:30:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].vw_professorsaladeaulalote as


SELECT DISTINCT sda.id_entidade
, sda.id_saladeaula
, sda.st_saladeaula
, e.st_nomeentidade
, dsa.id_disciplina
, ds.st_disciplina
, sda.dt_abertura
, sda.dt_encerramento
, u.id_usuario
, uper.id_perfilreferencia
, pf.id_perfilpedagogico
, u.st_nomecompleto
, uper.bl_ativo
, pf.id_perfil
, pf.st_nomeperfil
, uper.bl_titular
, CASE WHEN uper.bl_titular = 1 THEN 'Sim'
	   WHEN uper.bl_titular = 0 THEN 'N�o'
	   ELSE ' - '
	   END as st_titular		
, CONVERT (VARCHAR, sda.dt_abertura, 103) as st_dtabertura
, CONVERT (VARCHAR, sda.dt_encerramento, 103) as st_dtencerramento

FROM tb_saladeaula AS sda
JOIN tb_disciplinasaladeaula as dsa on dsa.id_saladeaula = sda.id_saladeaula
JOIN tb_disciplina as ds on ds.id_disciplina = dsa.id_disciplina
JOIN tb_entidade as e on e.id_entidade = sda.id_entidade
LEFT JOIN tb_usuarioperfilentidadereferencia as uper on sda.id_saladeaula = uper.id_saladeaula 
LEFT JOIN tb_usuario as u ON uper.id_usuario = u.id_usuario
LEFT JOIN tb_perfil AS pf ON pf.id_perfil = uper.id_perfil AND pf.id_perfilpedagogico = 1

LEFT JOIN dbo.tb_perfilreferenciaintegracao AS pri ON pri.id_perfilreferencia = uper.id_perfilreferencia
LEFT JOIN dbo.tb_saladeaulaintegracao AS si ON pri.id_saladeaulaintegracao = si.id_saladeaulaintegracao
LEFT JOIN dbo.tb_usuariointegracao AS ui ON ui.id_usuario = u.id_usuario AND ui.id_entidade = sda.id_entidade AND ui.id_sistema = si.id_sistema
LEFT JOIN dbo.tb_entidadeintegracao AS ei ON ei.id_entidade = sda.id_entidade AND ei.id_sistema = si.id_sistema
GO



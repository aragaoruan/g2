
/****** Object:  View [dbo].[vw_ocorrenciainteracao]    Script Date: 02/05/2013 15:34:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 CREATE VIEW [dbo].[vw_ocorrenciainteracao] as
SELECT
	tmo.id_tramite,
	tmo.id_ocorrencia,
	tt.bl_visivel,
	tt.id_usuario,
	tt.dt_cadastro,
	tt.st_tramite,
	tt.id_entidade,
	tu.st_upload,
	tu.id_upload
	, US.st_nomecompleto,
	(CAST (CONVERT(VARCHAR,tt.dt_cadastro,103)+ ' �s ' + CONVERT(VARCHAR,tt.dt_cadastro,108) AS VARCHAR(max))) AS st_cadastro
FROM tb_tramiteocorrencia AS tmo
JOIN tb_tramite tt
	ON tmo.id_tramite = tt.id_tramite
LEFT JOIN tb_upload tu
	ON tt.id_upload = tu.id_upload
JOIN dbo.tb_usuario AS US 
	ON us.id_usuario = tt.id_usuario	
	
GO


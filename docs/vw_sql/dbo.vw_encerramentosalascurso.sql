
CREATE VIEW [dbo].[vw_encerramentosalascurso]
AS
    SELECT DISTINCT
            es.id_saladeaula ,
	        es.id_encerramentosala,
	    		id_usuarioprofessor,
            dt_encerramentoprofessor,
	--id_usuariocoordenador,
            dt_encerramentocoordenador ,
	--id_usuariopedagogico,
            dt_encerramentopedagogico ,
	--id_usuariofinanceiro,
            dt_encerramentofinanceiro ,
            sa.id_entidade ,
            sa.id_disciplina ,
            sa.id_projetopedagogico ,
            sa.st_projetopedagogico ,
            di.st_disciplina ,
            di.nu_cargahoraria,
            sa.st_saladeaula,
            sa.id_tipodisciplina ,
            ac.id_areaconhecimento,
            ac.st_areaconhecimento,
            al.nu_alunos AS QTD_ALUNOS_ENCERRAR,
	--tcc.st_nomecompleto AS st_aluno,
	--tcc.st_tituloavaliacao,
	--tcc.id_matricula,
            us.st_nomecompleto AS st_professor,
			vw_valor.nu_valor,
			es.nu_valorpago
    FROM    dbo.tb_encerramentosala AS es
            JOIN vw_saladeaula AS sa ON sa.id_saladeaula = es.id_saladeaula
            OUTER APPLY ( SELECT TOP 1
                                    aa.st_nomecompleto ,
                                    aa.st_tituloavaliacao ,
                                    md.id_matricula
                          FROM      dbo.tb_encerramentoalocacao AS ee
                                    JOIN dbo.tb_alocacao AS ali ON ali.id_alocacao = ee.id_alocacao
                                    JOIN dbo.tb_matriculadisciplina AS md ON md.id_matriculadisciplina = ali.id_matriculadisciplina
                                    LEFT JOIN dbo.vw_avaliacaoaluno AS aa ON aa.id_saladeaula = sa.id_saladeaula
                                                              AND md.id_matricula = aa.id_matricula
                                                              AND aa.id_tipoavaliacao = 6
                          WHERE     es.id_encerramentosala = ee.id_encerramentosala
                        ) AS tcc
            LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS upr ON upr.id_saladeaula = sa.id_saladeaula
                                                              AND upr.bl_titular = 1
            LEFT JOIN dbo.tb_usuario AS us ON us.id_usuario = upr.id_usuario
			JOIN dbo.tb_areaprojetopedagogico AS tap ON tap.id_projetopedagogico = sa.id_projetopedagogico
		    JOIN dbo.tb_areaconhecimento AS ac ON ac.id_areaconhecimento = tap.id_areaconhecimento
            CROSS APPLY ( SELECT    COUNT(DISTINCT ea.id_alocacao) AS nu_alunos
                          FROM      dbo.tb_encerramentoalocacao AS ea
						  JOIN		dbo.tb_alocacao AS ta ON ea.id_alocacao = ta.id_alocacao
						  JOIN		dbo.tb_matriculadisciplina AS tm ON ta.id_matriculadisciplina = tm.id_matriculadisciplina
						  JOIN		dbo.tb_matricula AS tm2 ON tm.id_matricula = tm2.id_matricula AND tm2.id_projetopedagogico = sa.id_projetopedagogico
                          WHERE     ea.id_encerramentosala = es.id_encerramentosala
                        ) AS al
            INNER JOIN dbo.tb_disciplina AS di ON di.id_disciplina = sa.id_disciplina

			-- totalização dos valores
			OUTER APPLY (SELECT SUM(nu_valor) AS nu_valor
				FROM dbo.vw_encerramentovalor v 
				WHERE v.id_encerramentosala = es.id_encerramentosala
				GROUP BY id_encerramentosala) AS vw_valor



    WHERE al.nu_alunos >0



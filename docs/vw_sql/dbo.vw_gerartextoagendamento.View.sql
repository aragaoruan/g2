
/****** Object:  View [dbo].[vw_gerartextoagendamento]    Script Date: 07/10/2013 13:59:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_gerartextoagendamento]
AS
    SELECT  CONVERT(VARCHAR, ta.dt_agendamento, 103) AS dt_agendamento ,
            ta3.st_avaliacao ,
            st_nomecompleto = CASE WHEN tm.id_usuario IS NULL
                                   THEN us.st_nomecompleto
                                   ELSE tu.st_nomecompleto
                              END ,
            ta.id_avaliacaoagendamento ,
            ta.id_entidade
    FROM    dbo.tb_avaliacaoagendamento AS ta
            LEFT JOIN tb_usuario AS us ON us.id_usuario = ta.id_usuario
            LEFT JOIN dbo.tb_matricula AS tm ON tm.id_matricula = ta.id_matricula
            LEFT JOIN dbo.tb_usuario AS tu ON tm.id_usuario = tu.id_usuario
            LEFT JOIN dbo.tb_dadosacesso AS da ON da.id_usuario = ta.id_usuario
                                                  AND da.bl_ativo = 1
                                                  AND da.id_entidade = ta.id_entidade
            JOIN dbo.tb_avaliacaoaplicacao AS ta2 ON ta2.id_avaliacaoaplicacao = ta.id_avaliacaoaplicacao
            JOIN dbo.tb_avaliacao AS ta3 ON ta.id_avaliacao = ta3.id_avaliacao

GO



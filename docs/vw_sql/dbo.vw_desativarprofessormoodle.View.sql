USE [G2H_EAD1]
GO

/****** Object:  View [dbo].[vw_desativarprofessormoodle]    Script Date: 08/05/2015 11:52:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vw_desativarprofessormoodle] 
AS
SELECT 
uper.id_perfilreferencia,
uper.id_usuario,
sali.st_codsistemacurso,
usi.id_entidade,
uper.bl_desativarmoodle


FROM tb_usuarioperfilentidadereferencia AS uper
join tb_usuario as u on u.id_usuario = uper.id_usuario
join tb_perfil as pr on pr.id_perfil = uper.id_perfil
join tb_perfilpedagogico as pp on pp.id_perfilpedagogico = pr.id_perfilpedagogico  AND pp.id_perfilpedagogico = 1
join tb_perfilreferenciaintegracao as pri on pri.id_perfilreferencia=uper.id_perfilreferencia and pri.id_sistema=6
join tb_saladeaulaintegracao as sali on sali.id_saladeaulaintegracao = pri.id_saladeaulaintegracao
join tb_usuariointegracao as usi on usi.id_usuario = uper.id_usuario and usi.id_sistema=pri.id_sistema
WHERE uper.bl_desativarmoodle = 1

GO



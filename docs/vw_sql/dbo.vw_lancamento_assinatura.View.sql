 ALTER VIEW [dbo].[vw_lancamento_assinatura] AS

SELECT DISTINCT
        v.id_venda ,

		pd.id_produto,
		
        CAST(( CAST(( DATEPART(YEAR, GETDATE()) ) AS VARCHAR) + '-'
               + CAST(( DATEPART(MONTH, GETDATE()) ) AS VARCHAR) + '-'
               + right('0' + CAST(( nu_diamensalidade ) AS VARCHAR), 2) ) AS DATE) AS dt_vencimento ,
			  
		pv.nu_valormensal,

        nu_valordesconto = CAST((
		
			CASE WHEN v.id_cupom IS NOT NULL AND cp.id_tipodesconto = 2 THEN ( isnull(pv.nu_valormensal, 0) * (isnull(cp.nu_desconto, 0) / 100) ) 
				 WHEN v.id_cupom IS NOT NULL AND cp.id_tipodesconto = 1 THEN isnull(cp.nu_desconto, 0)
				 ELSE 0
				 END
			 
			 
		) AS NUMERIC(30,2)) ,

        nu_valorlancamento = CAST((
		
			CASE WHEN v.id_cupom IS NULL THEN isnull(pv.nu_valormensal, 0)
				 WHEN v.id_cupom IS NOT NULL AND cp.id_tipodesconto = 2 THEN ( isnull(pv.nu_valormensal, 0) - ( isnull(pv.nu_valormensal, 0) * (isnull(cp.nu_desconto, 0) / 100) ) )
				 WHEN v.id_cupom IS NOT NULL AND cp.id_tipodesconto = 1 THEN ( isnull(pv.nu_valormensal, 0) - isnull(cp.nu_desconto, 0) ) 
				 ELSE isnull(pv.nu_valormensal, 0)
				 END
			 
			 
		) AS NUMERIC(30,2)) ,
		
        v.id_entidade ,
        v.id_usuario ,
        v.dt_confirmacao,
		vp.id_matricula,
		isnull((select count(*) + 1 from tb_lancamentovenda where id_venda = v.id_venda and nu_ordem is not null),1) as nu_ordem,
		(select dt_vencimento from tb_lancamento where id_lancamento = la.id_lancamento) as dt_vencimentoanterior,
		la.id_lancamento as id_lancamentoanterior, la.nu_valor as nu_valoranterior, la.id_cartaoconfig, la.id_entidadelancamento, la.id_meiopagamento, la.id_tipolancamento, la.id_usuariocadastro, la.id_usuariolancamento
		, la.nu_cobranca, la.nu_assinatura
				
FROM    tb_venda AS v
        JOIN dbo.tb_vendaproduto AS vp ON v.id_venda = vp.id_venda
        JOIN dbo.tb_produto AS pd ON pd.id_produto = vp.id_produto AND pd.id_modelovenda = 2
        JOIN tb_produtovalor AS pv ON pv.id_produto = pd.id_produto
                                      AND GETDATE() >= pv.dt_inicio
                                      AND ( GETDATE() <= pv.dt_termino
                                            OR pv.dt_termino IS NULL
                                          )
                                      AND pv.id_tipoprodutovalor IN ( 4, 5 )
        JOIN tb_entidade AS ent ON ent.id_entidade = v.id_entidade


		JOIN (  select max(l.id_lancamento) as id_lancamento,  lv.id_venda, l.nu_valor, l.id_cartaoconfig, l.id_entidade, l.id_entidadelancamento, l.id_meiopagamento, l.id_tipolancamento, l.id_usuariocadastro, l.id_usuariolancamento, l.nu_cobranca, l.nu_assinatura
				from tb_lancamento as l join 
				tb_lancamentovenda as lv on (l.id_lancamento = lv.id_lancamento) 
				group by lv.id_venda, l.nu_valor, l.id_cartaoconfig, l.id_entidade, l.id_entidadelancamento, l.id_meiopagamento, l.id_tipolancamento, l.id_usuariocadastro, l.id_usuariolancamento, l.nu_cobranca, l.nu_assinatura
			) as la on (la.id_venda = v.id_venda)

        LEFT JOIN dbo.tb_cupom AS cp ON v.id_cupom = cp.id_cupom
WHERE   v.id_evolucao = 10
		
		AND v.id_situacao = 39 

		AND  NOT EXISTS ( SELECT lv.id_venda
								 FROM   tb_lancamento AS lc
										JOIN tb_lancamentovenda AS lv ON lc.id_lancamento = lv.id_lancamento
																		 AND lv.id_venda = v.id_venda
								 WHERE  MONTH(GETDATE()) = MONTH(lc.dt_vencimento) )
		AND GETDATE() >= CAST(( CAST(( DATEPART(YEAR, GETDATE()) ) AS VARCHAR)
                                + '-'
                                + CAST(( DATEPART(MONTH, GETDATE()) ) AS VARCHAR)
                                + '-' + right('0' + CAST(( nu_diamensalidade ) AS VARCHAR), 2) ) AS DATE)
								
		AND v.id_venda in (select id_venda from tb_lancamentovenda where id_venda = v.id_venda)

		AND la.id_meiopagamento = 11
GO


-- select * from tb_lancamento where id_lancamento = 1131996

GO
/****** Object:  View [rel].[vw_agendamentos]    Script Date: 10/08/2012 17:42:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [rel].[vw_agendamentos] AS
SELECT
ROW_NUMBER()OVER (ORDER BY ag.dt_cadastro ASC) AS nu_numero, us.st_nomecompleto, av.st_avaliacao,
'-                                                                                                        -' AS st_assinatura,
ag.dt_agendamento, mt.id_projetopedagogico, mt.id_entidadeatendimento
FROM dbo.tb_avaliacaoagendamento AS ag
JOIN dbo.tb_matricula AS mt ON ag.id_matricula = mt.id_matricula
JOIN dbo.tb_avaliacao AS av ON av.id_avaliacao = ag.id_avaliacao
JOIN dbo.tb_usuario AS us ON us.id_usuario = mt.id_usuario
GO

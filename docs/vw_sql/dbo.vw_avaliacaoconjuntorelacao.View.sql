SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_avaliacaoconjuntorelacao] AS
SELECT acr.id_avaliacaoconjuntorelacao, 
acr.id_avaliacao, 
av.st_avaliacao, 
acr.id_avaliacaorecupera, 
(CASE WHEN acr.id_avaliacaorecupera=1 THEN 'Prova de Recuperação 1'
WHEN acr.id_avaliacaorecupera=2 THEN 'Prova de Recuperação 2'
ELSE 'Prova de Recuperação '+ cast(acr.id_avaliacaorecupera as varchar(4)) END) AS st_avaliacaorecupera,
acr.id_avaliacaoconjunto, 
av.nu_valor, av.id_tipoavaliacao, 
av.bl_recuperacao,
ta.st_tipoavaliacao,
avconj.id_entidade
FROM tb_avaliacaoconjuntorelacao AS acr
INNER JOIN tb_avaliacao AS av ON av.id_avaliacao = acr.id_avaliacao
LEFT JOIN tb_avaliacao AS avr ON avr.id_avaliacao = acr.id_avaliacaorecupera
INNER JOIN tb_avaliacaoconjunto as avconj on avconj.id_avaliacaoconjunto = acr.id_avaliacaoconjunto and avconj.id_situacao=66
INNER JOIN tb_tipoavaliacao AS ta ON ta.id_tipoavaliacao = av.id_tipoavaliacao

GO
CREATE VIEW vw_entidadesmesmaholding AS
  SELECT
    DISTINCT
    hf.id_entidade,
    e.id_entidade AS id_entidadeparceira,
    e.st_nomeentidade AS st_nomeentidadeparceira
  FROM tb_holdingfiliada hf
    JOIN tb_holdingfiliada hf2 ON hf2.id_holding = hf.id_holding
    JOIN tb_entidade e ON e.id_entidade = hf2.id_entidade
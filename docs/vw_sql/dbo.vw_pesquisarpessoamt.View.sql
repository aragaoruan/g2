
GO
/****** Object:  View [dbo].[vw_pesquisarpessoamt]    Script Date: 10/08/2012 17:42:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_pesquisarpessoamt]

as

select u.id_usuario, p.id_entidade, u.st_nomecompleto, u.st_cpf, 
m.id_municipio, p.sg_uf, m.st_nomemunicipio, ce.st_email, p.bl_ativo,
sit.id_situacao,sit.st_situacao, p.st_sexo, p.dt_nascimento, p.st_nomepai, p.st_nomemae, mt.id_matricula
from tb_usuario u
INNER JOIN tb_pessoa as p ON p.id_usuario = u.id_usuario
INNER JOIN tb_situacao as sit ON p.id_situacao = sit.id_situacao
LEFT JOIN tb_municipio as m ON m.id_municipio = p.id_municipio
LEFT JOIN tb_contatosemailpessoaperfil as cepp ON cepp.id_entidade = p.id_entidade and cepp.id_usuario = p.id_usuario and cepp.bl_padrao = 1
LEFT JOIN tb_contatosemail as ce ON ce.id_email = cepp.id_email
--LEFT JOIN tb_usuarioperfilentidade as upe ON upe.id_entidade = p.id_entidade and upe.id_usuario = p.id_usuario
--LEFT JOIN tb_perfil as per ON per.id_entidade = upe.id_entidade and per.id_entidade = cepp.id_entidade and per.id_perfil = cepp.id_perfil -- olha o perfil
LEFT JOIN dbo.tb_matricula AS mt ON mt.id_usuario = u.id_usuario AND mt.id_entidadeatendimento = p.id_entidade
where u.bl_ativo = 1
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vw_alocacaointegracao] as
select 
al.id_alocacao, al.id_saladeaula, al.id_situacao, si.st_codsistemasala, si.st_codsistemacurso, si.st_codsistemareferencia,
mt.id_usuario, ui.st_codusuario, ai.st_codalocacao, si.id_sistema, ai.id_usuariocadastro
from
tb_alocacao as al
JOIN tb_saladeaulaintegracao as si ON si.id_saladeaula = al.id_saladeaula
JOIN tb_matriculadisciplina as md ON md.id_matriculadisciplina = al.id_matriculadisciplina
JOIN tb_matricula as mt on mt.id_matricula = md.id_matricula
LEFT JOIN tb_usuariointegracao as ui ON ui.id_usuario = mt.id_usuario
LEFT JOIN tb_alocacaointegracao as ai ON ai.id_alocacao = al.id_alocacao
where al.bl_ativo = 1
GO

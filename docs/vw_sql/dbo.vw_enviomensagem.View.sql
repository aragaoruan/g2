ALTER VIEW [dbo].[vw_enviomensagem] AS
     WITH _config AS (
        SELECT 0 AS FALSE,
               1 AS TRUE,
               3 AS TIPO_ENVIO_EMAIL
               4 AS TIPO_ENVIO_HTML
     )
     SELECT env_men.id_enviomensagem,
            env_men.id_mensagem,
            env_men.id_tipoenvio,
            env_men.id_emailconfig,
            env_men.id_evolucao,
            env_men.dt_cadastro,
            env_men.dt_enviar,
            dt_tentativa,
            env_des.id_enviodestinatario,
            env_des.id_usuario,
            men.st_caminhoanexo,
     CASE
       WHEN env_men.id_tipoenvio = _.TIPO_ENVIO_HTML THEN env_men.dt_cadastro
       ELSE env_men.dt_envio END AS dt_envio,
     CASE
       WHEN env_des.id_usuario IS NULL THEN env_des.st_nome
       ELSE usu.st_nomecompleto
     END AS st_nome,
     CASE
       WHEN env_des.id_usuario IS NULL THEN env_des.st_endereco
       ELSE con_ema.st_email
     END AS st_endereco,
            env_des.id_tipodestinatario,
            men.st_mensagem,
            men.st_texto,
            env_des.nu_telefone,
            pes.id_entidade,
            env.st_evolucao,
            tip_env.st_tipoenvio,
            env_env_des.st_evolucao AS st_evolucaodestinatario,
            env_env_des.id_evolucao AS id_evolucaodestinatario
      FROM tb_enviomensagem             AS env_men
CROSS APPLY _config                     AS _
INNER JOIN tb_mensagem                  AS men              ON env_men.id_mensagem  = men.id_mensagem
INNER JOIN tb_enviodestinatario         AS env_des          ON env_men.id_enviomensagem = env_des.id_enviomensagem
INNER JOIN tb_usuario                   AS usu              ON env_des.id_usuario   = usu.id_usuario
INNER JOIN tb_evolucao                  AS env              ON env_men.id_evolucao  = env.id_evolucao
INNER JOIN tb_tipoenvio                 AS tip_env          ON env_men.id_tipoenvio = tip_env.id_tipoenvio
INNER JOIN tb_pessoa                    AS pes              ON usu.id_usuario       = pes.id_usuario AND pes.id_entidade = men.id_entidade
 LEFT JOIN tb_evolucao                  AS env_env_des      ON env_des.id_evolucao  = env_env_des.id_evolucao
 LEFT JOIN tb_contatosemailpessoaperfil AS con_ema_pes_per  ON env_des.id_usuario   = con_ema_pes_per.id_usuario
                                                           AND con_ema_pes_per.id_entidade = men.id_entidade
                                                           AND con_ema_pes_per.bl_ativo = _.TRUE
                                                           AND con_ema_pes_per.bl_padrao = _.TRUE
LEFT JOIN tb_contatosemail AS con_ema   ON con_ema_pes_per.id_email = con_ema.id_email
LEFT JOIN tb_emailconfig   AS ema_con   ON env_men.id_emailconfig = ema_con.id_emailconfig
    WHERE ((env_men.id_emailconfig IS NOT NULL AND ema_con.bl_erro = _.FALSE) OR env_men.id_tipoenvio <> _.TIPO_ENVIO_EMAIL)

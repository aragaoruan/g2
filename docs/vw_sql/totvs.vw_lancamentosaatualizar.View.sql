SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 CREATE VIEW [totvs].[vw_lancamentosaatualizar] AS
SELECT
	lc.id_entidade,
	lv.id_venda,
	lc.id_lancamento,
	li.st_codlancamento,
	lc.dt_vencimento,
	lc.nu_valor
FROM
	dbo.tb_lancamentointegracao li
	INNER JOIN dbo.tb_lancamento lc ON li.id_lancamento = lc.id_lancamento
	INNER JOIN dbo.tb_lancamentovenda lv ON lc.id_lancamento = lv.id_lancamento
	INNER JOIN dbo.tb_entidadeintegracao ei ON lc.id_entidade = ei.id_entidade AND ei.id_sistema = 3
WHERE 
	lc.dt_atualizado >= li.dt_sincronizado

GO
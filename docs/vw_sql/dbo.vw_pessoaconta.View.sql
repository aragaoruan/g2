
GO
/****** Object:  View [dbo].[vw_pessoaconta]    Script Date: 10/08/2012 17:42:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_pessoaconta] AS
SELECT b.id_banco, b.st_nomebanco, cp.bl_padrao, cp.id_entidade, cp.id_usuario, cp.st_agencia, cp.st_conta, cp.st_banco
, tc.id_tipodeconta, tc.st_tipodeconta, cp.id_contapessoa, b.st_nomebanco+'/'+tc.st_tipodeconta as st_bancoconta
FROM tb_banco as b
INNER JOIN tb_contapessoa AS cp ON cp.st_banco = b.st_banco
INNER JOIN tb_tipodeconta AS tc ON tc.id_tipodeconta = cp.id_tipodeconta
INNER JOIN tb_pessoa AS p ON p.id_entidade = cp.id_entidade AND p.id_usuario = cp.id_usuario
GO

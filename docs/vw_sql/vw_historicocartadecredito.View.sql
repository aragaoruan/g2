/* Criando view do histórico da carta de crédito
 */

CREATE VIEW vw_historicocartacredito AS select
vwcc.id_cartacredito,
vp.id_venda,
p.id_produto,
p.st_produto,
e.id_evolucao,
e.st_evolucao,
CONVERT(CHAR, vp.dt_cadastro, 103) AS dt_utilizacao,
vwcc.nu_valororiginal,
vcc.nu_valorutilizado,
vwcc.nu_valordisponivel
from tb_produto as p
join tb_vendaproduto as vp on p.id_produto = vp.id_produto
join tb_vendacartacredito as vcc on vcc.id_venda = vp.id_venda
join vw_cartacredito as vwcc on vcc.id_cartacredito = vwcc.id_cartacredito
join tb_venda as v on vcc.id_venda = v.id_venda
join tb_evolucao as e on v.id_evolucao = e.id_evolucao
and v.id_evolucao = 10;
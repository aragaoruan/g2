
GO
/****** Object:  View [dbo].[vw_usuariomatriculaprojeto]    Script Date: 10/08/2012 17:42:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE VIEW [dbo].[vw_usuariomatriculaprojeto] as
select distinct 
		  us.id_usuario, us.st_login, us.st_senha, us.st_nomecompleto
		, pes.st_nomeexibicao
		, mt.id_matricula
		, mt.bl_ativo
		, mt.id_entidadeatendimento
		, en.id_entidade, en.st_nomeentidade, en.st_razaosocial
		, 'Aluno' as st_nomeperfil
		, 5 as id_perfil
		, mt.id_projetopedagogico
		, pp.st_tituloexibicao, pp.id_trilha 
		, ev.st_evolucao
		
		
from
tb_usuario as us
JOIN tb_matricula as mt ON us.id_usuario = mt.id_usuario
JOIN tb_pessoa as pes ON pes.id_usuario = us.id_usuario AND (mt.id_entidadeatendimento = pes.id_entidade OR mt.id_entidadematriz = pes.id_entidade )
JOIN tb_projetopedagogico as pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_projetoentidade as pe ON pe.id_projetopedagogico = mt.id_projetopedagogico and pe.id_entidade = mt.id_entidadeatendimento
JOIN tb_entidade as en ON en.id_entidade = pe.id_entidade
JOIN tb_evolucao as ev ON ev.id_evolucao = mt.id_evolucao
--JOIN tb_contratomatricula as cm ON cm.id_matricula = mt. id_matricula
--JOIN tb_contrato as ct ON ct.id_contrato = cm.id_contrato
where us.bl_ativo = 1
--and mt.bl_ativo = 1
--and mt.id_situacao = 50
--and mt.id_evolucao = 6
and pp.bl_ativo = 1
and pp.id_situacao in (7)
and pe.bl_ativo = 1
and pe.id_situacao = 51
--and ct.bl_ativo = 1
--and ct.id_situacao = 44
--and ct.id_evolucao = 4


GO

CREATE VIEW [dbo].[vw_produtoprojeto] AS
SELECT
	ppp.id_projetopedagogico,
	ppp.id_produto,
	ppp.nu_cargahoraria,
	tm.dt_inicio,
	tn.st_turno,
	ppp.st_disciplina,
	NULL AS st_diassemana
FROM
	dbo.tb_produtoprojetopedagogico AS ppp
LEFT JOIN dbo.tb_turmaprojeto AS tp ON tp.id_projetopedagogico = ppp.id_projetopedagogico
AND ppp.id_turma = tp.id_turma
LEFT JOIN dbo.tb_turma AS tm ON tm.id_turma = tp.id_turma
LEFT JOIN tb_turmaturno AS tt ON tt.id_turma = tm.id_turma
LEFT JOIN dbo.tb_turno AS tn ON tn.id_turno = tt.id_turno
WHERE ppp.id_turma IS NULL
UNION ALL
SELECT
	ppp.id_projetopedagogico,
	ppp.id_produto,
	ppp.nu_cargahoraria,
	tm.dt_inicio,
	tn.st_turno,
	ppp.st_disciplina,
	null AS st_diassemana
FROM
	dbo.tb_produtoprojetopedagogico AS ppp
JOIN dbo.tb_turmaprojeto AS tp ON tp.id_projetopedagogico = ppp.id_projetopedagogico
AND ppp.id_turma = tp.id_turma
JOIN dbo.tb_turma AS tm ON tm.id_turma = ppp.id_turma
LEFT JOIN tb_turmaturno AS tt ON tt.id_turma = tm.id_turma
LEFT JOIN dbo.tb_turno AS tn ON tn.id_turno = tt.id_turno
WHERE ppp.id_turma IS NOT NULL
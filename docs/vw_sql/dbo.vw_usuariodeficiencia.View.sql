CREATE VIEW [dbo].[vw_usuariodeficiencia] AS
  SELECT
    udf.id_usuariodeficiencia,
    us.id_usuario,
    df.id_deficiencia,
    us.st_nomecompleto,
    df.st_deficiencia,
    df.st_descricao,
    (CASE
     WHEN udf.id_usuariodeficiencia IS NULL
       THEN 0
     ELSE 1
     END) AS bl_possuideficiencia
  FROM
    tb_usuario AS us
    JOIN tb_deficiencia AS df ON df.bl_ativo = 1
    LEFT JOIN tb_usuariodeficiencia AS udf ON udf.id_usuario = us.id_usuario AND udf.id_deficiencia = df.id_deficiencia
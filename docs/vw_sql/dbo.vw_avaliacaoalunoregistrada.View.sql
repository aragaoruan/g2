CREATE VIEW [dbo].[vw_avaliacaoalunoregistrada] AS
  SELECT
    aa.id_avaliacaoaluno ,
    aa.id_matricula,
    aa.id_avaliacaoconjuntoreferencia,
    aa.id_avaliacao,
    aa.id_usuariocadastro,
    aa.dt_avaliacao,
    aa.id_situacao,
    aa.id_upload,
    aa.st_tituloavaliacao,
    aa.id_tiponota,
    us.st_nomecompleto AS st_nomeusuario,
    aa.st_nota,
    aa.bl_ativo,
    aa.dt_alteracao,
    CONVERT(VARCHAR(13), aa.dt_cadastro, 103) AS st_dtavaliacao,
    CONVERT(VARCHAR(13), aa.dt_alteracao, 103) AS st_dtalteracao,
    'Adição' AS st_acao,
    2 as id_ordem,
    aa.id_sistema,
    sis.st_sistema,
    aa.id_notaconceitual
  FROM tb_avaliacaoaluno AS aa
    JOIN tb_usuario AS us ON us.id_usuario = aa.id_usuariocadastro
    LEFT JOIN tb_sistema AS sis ON aa.id_sistema = sis.id_sistema
  UNION
  SELECT
    aa.id_avaliacaoaluno ,
    aa.id_matricula,
    aa.id_avaliacaoconjuntoreferencia,
    aa.id_avaliacao,
    aa.id_usuariocadastro,
    aa.dt_avaliacao,
    aa.id_situacao,
    aa.id_upload,
    aa.st_tituloavaliacao,
    aa.id_tiponota,
    us.st_nomecompleto AS st_nomeusuario,
    aa.st_nota,
    aa.bl_ativo,
    aa.dt_alteracao,
    CONVERT(VARCHAR(13), aa.dt_avaliacao, 103) AS st_dtavaliacao,
    CONVERT(VARCHAR(13), aa.dt_alteracao, 103) AS st_dtalteracao,
    'Exclusão' AS st_acao,
    1 as id_ordem,
    aa.id_sistema,
    sis.st_sistema,
    aa.id_notaconceitual
  FROM tb_avaliacaoaluno AS aa
    JOIN tb_usuario AS us ON us.id_usuario = aa.id_usuarioalteracao
    LEFT JOIN tb_sistema AS sis ON aa.id_sistema = sis.id_sistema
  WHERE aa.id_situacao = 87
  UNION
  SELECT
    aa.id_avaliacaoaluno ,
    aa.id_matricula,
    aa.id_avaliacaoconjuntoreferencia,
    aa.id_avaliacao,
    aa.id_usuariocadastro,
    aa.dt_avaliacao,
    aa.id_situacao,
    aa.id_upload,
    aa.st_tituloavaliacao,
    aa.id_tiponota,
    us.st_nomecompleto AS st_nomeusuario,
    aa.st_nota,
    aa.bl_ativo,
    aa.dt_alteracao,
    CONVERT(VARCHAR(13), aa.dt_avaliacao, 103) AS st_dtavaliacao,
    CONVERT(VARCHAR(13), aa.dt_alteracao, 103) AS st_dtalteracao,
    'Exclusão' AS st_acao,
    1 as id_ordem,
    aa.id_sistema,
    sis.st_sistema,
    aa.id_notaconceitual
  FROM tb_avaliacaoaluno AS aa
    JOIN tb_usuario AS us ON us.id_usuario = aa.id_usuarioalteracao
    LEFT JOIN tb_sistema AS sis ON aa.id_sistema = sis.id_sistema
  WHERE aa.bl_ativo = 0
  UNION
  SELECT
    aa.id_avaliacaoaluno ,
    aa.id_matricula,
    aa.id_avaliacaoconjuntoreferencia,
    aa.id_avaliacao,
    aa.id_usuariocadastro,
    aa.dt_avaliacao,
    aa.id_situacao,
    aa.id_upload,
    aa.st_tituloavaliacao,
    aa.id_tiponota,
    us.st_nomecompleto AS st_nomeusuario,
    aa.st_nota,
    aa.bl_ativo,
    aa.dt_alteracao,
    CONVERT(VARCHAR(13), aa.dt_avaliacao, 103) AS st_dtavaliacao,
    CONVERT(VARCHAR(13), aa.dt_alteracao, 103) AS st_dtalteracao,
    'Revisão' AS st_acao,
    2 as id_ordem,
    aa.id_sistema,
    sis.st_sistema,
    aa.id_notaconceitual
  FROM tb_avaliacaoaluno AS aa
    JOIN tb_usuario AS us ON us.id_usuario = aa.id_usuarioalteracao
    LEFT JOIN tb_sistema AS sis ON aa.id_sistema = sis.id_sistema
  WHERE aa.id_situacao = 88
GO

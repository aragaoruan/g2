CREATE VIEW [rel].[vw_tutorespagos] AS
  SELECT
    DISTINCT
    ac.st_tituloexibicao AS st_area-- Area
    ,pp.st_tituloexibicao-- Projeto Pedagogico
    ,ec.st_disciplina-- Disciplina
    ,di.nu_cargahoraria
    ,di.nu_cargahoraria AS st_cargahoraria-- C.H
    ,ec.st_saladeaula-- Sala de aula
    ,ec.nu_alunos
    ,ec.nu_alunos AS st_alunos-- Alunos
    ,ec.st_professor-- Professor
    ,ec.dt_encerramentoprofessor-- Encerramento Sala
    ,ec.dt_encerramentofinanceiro-- Pagamento
    ,es.nu_valorpago-- Valor Pago
    ,ec.id_entidade
  FROM
    vw_encerramentocoordenador AS ec
    INNER JOIN vw_saladeaula AS sa ON sa.id_saladeaula = ec.id_saladeaula
    INNER JOIN tb_encerramentosala AS es ON ec.id_saladeaula = es.id_saladeaula
    INNER JOIN tb_projetopedagogico AS pp ON pp.id_projetopedagogico = sa.id_projetopedagogico
    INNER JOIN tb_disciplinasaladeaula AS ds ON ds.id_saladeaula = sa.id_saladeaula
    INNER JOIN tb_disciplina AS di ON ds.id_disciplina = di.id_disciplina
    INNER JOIN tb_areadisciplina AS ad ON ds.id_disciplina = ad.id_disciplina
    INNER JOIN tb_areaconhecimento AS ac ON ad.id_areaconhecimento = ac.id_areaconhecimento
  WHERE
    ec.dt_encerramentoprofessor IS NOT NULL;
GO


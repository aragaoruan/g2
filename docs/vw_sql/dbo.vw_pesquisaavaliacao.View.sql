CREATE VIEW [dbo].[vw_pesquisaavaliacao] AS
SELECT DISTINCT
av.id_avaliacao,
av.st_avaliacao,
av.id_tipoavaliacao,
CAST ( av.nu_valor AS int ) AS nu_valor,
av.dt_cadastro,
av.id_usuariocadastro,
av.id_situacao,
av.st_descricao,
av.nu_quantquestoes,
av.st_linkreferencia,
ta.st_tipoavaliacao,
s.st_situacao,
av.bl_recuperacao,
NULL AS id_avaliacaorecupera, -- este campo é preenchido manualmente na busca
er.id_entidade,
er.st_nomeentidade AS st_entidade,
av.id_entidade AS id_entidadeavaliacao,
en.st_nomeentidade AS st_entidadeavaliacao
FROM tb_avaliacao av
JOIN tb_tipoavaliacao AS ta ON ta.id_tipoavaliacao = av.id_tipoavaliacao
JOIN tb_situacao AS s ON s.id_situacao = av.id_situacao
JOIN tb_entidade AS en ON en.id_entidade = av.id_entidade
JOIN vw_entidaderecursiva AS er ON av.id_entidade IN(er.id_entidade, er.id_entidadepai)
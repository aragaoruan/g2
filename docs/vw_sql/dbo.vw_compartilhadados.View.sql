CREATE VIEW [dbo].[vw_entidadecompartilhadados]

AS

SELECT e.st_nomeentidade
, cd.id_entidadedestino
, cd.id_entidadeorigem
, cd.id_tipodados,
eo.st_nomeentidade as st_nomentidadeorigem
FROM tb_compartilhadados  as cd
INNER JOIN tb_entidade as
e ON e.id_entidade = cd.id_entidadedestino
JOIN tb_entidade as eo ON eo.id_entidade = cd.id_entidadeorigem
--where cd.id_entidadeorigem = 1

 UNION

SELECT e.st_nomeentidade
  , e.id_entidade  as id_entidadedestino
  , eo.id_entidade  as id_entidadeorigem
  , 1 as id_tipodados,
    eo.st_nomeentidade as st_nomentidadeorigem
FROM tb_entidade as e
  JOIN tb_holdingfiliada AS hf on e.id_entidade = hf.id_entidade
  JOIN tb_holdingfiliada as hf2 on hf.id_holding = hf2.id_holding
  JOIN tb_entidade as eo ON
eo.id_entidade = hf2.id_entidade and e.id_entidade != eo.id_entidade

UNION

SELECT e.st_nomeentidade
  , e.id_entidade  as id_entidadedestino
  , e.id_entidade  as id_entidadeorigem
  , 1 as id_tipodados,
    e.st_nomeentidade as st_nomentidadeorigem
FROM tb_entidade as e
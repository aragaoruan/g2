CREATE VIEW [dbo].[vw_matriculadisciplina] as
SELECT
md.id_matriculadisciplina,
md.id_situacao,
st.st_situacao,
md.id_matricula,
md.id_disciplina,
dc.st_disciplina,
mdd.id_serie,
sr.st_serie,
md.id_evolucao,
ev.st_evolucao,
md.nu_aprovafinal,
ad.id_areaconhecimento,
ac.st_areaconhecimento,
md.bl_obrigatorio,
md.id_situacaoagendamento
from tb_matriculadisciplina as md
JOIN tb_matricula as mt ON mt.id_matricula = md.id_matricula
JOIN tb_modulo as mdl on mdl.id_projetopedagogico = mt.id_projetopedagogico and mdl.bl_ativo = 1
JOIN tb_modulodisciplina as mdd ON mdd.id_modulo = mdl.id_modulo and md.id_disciplina = mdd.id_disciplina and mdd.bl_ativo = 1
JOIN tb_disciplina as dc on dc.id_disciplina = md.id_disciplina
LEFT JOIN tb_areadisciplina as ad ON ad.id_disciplina = mdd.id_disciplina and mdd.id_areaconhecimento = ad.id_areaconhecimento
LEFT JOIN tb_areaconhecimento as ac ON ac.id_areaconhecimento = ad.id_areaconhecimento
LEFT JOIN tb_serie as sr ON sr.id_serie = mdd.id_serie
JOIN tb_situacao as st ON st.id_situacao = md.id_situacao
JOIN tb_evolucao as ev ON ev.id_evolucao = md.id_evolucao

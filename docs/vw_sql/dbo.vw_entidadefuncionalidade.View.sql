
GO
/****** Object:  View [dbo].[vw_entidadefuncionalidade]    Script Date: 10/08/2012 17:41:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_entidadefuncionalidade] as
SELECT	ef.id_entidade,e.st_nomeentidade,f.id_funcionalidade,f.st_funcionalidade,id_funcionalidadepai,f.bl_ativo AS bl_funcionalidadeativo,
		ef.bl_ativo as bl_entidadefuncionalidadeativo,f.id_situacao AS id_situacaofuncionalidade,sitfuncionalidade.st_situacao AS st_situacaofuncionalidade, 
		sitfunentidade.id_situacao as id_situacaoentidadefuncionalidade,sitfunentidade.st_situacao as st_situacaoentidadefuncionalidade,
		f.st_classeflex, f.st_urlicone, f.id_tipofuncionalidade, tf.st_tipofuncionalidade,f.nu_ordem AS nu_ordemfuncionalidade, 
		ef.nu_ordem as nu_ordementidadefuncionalidade,f.st_classeflexbotao, ef.st_label, ef.bl_obrigatorio, ef.bl_visivel, f.id_sistema, s.st_sistema
FROM tb_funcionalidade AS f
							INNER JOIN tb_entidadefuncionalidade	AS ef					ON ef.id_funcionalidade = f.id_funcionalidade
							INNER JOIN tb_situacao					AS sitfuncionalidade	ON f.id_situacao = sitfuncionalidade.id_situacao
							INNER JOIN tb_entidade					AS e					ON e.id_entidade = ef.id_entidade
							INNER JOIN tb_situacao					AS sitfunentidade		ON sitfunentidade.id_situacao = ef.id_situacao
							INNER JOIN tb_tipofuncionalidade		AS tf					ON tf.id_tipofuncionalidade = f.id_tipofuncionalidade
                            INNER JOIN tb_sistema                   AS s                    ON s.id_sistema = f.id_sistema
GO

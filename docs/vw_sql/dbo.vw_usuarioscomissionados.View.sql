USE [G2H_EAD1]
GO

/****** Object:  View [dbo].[vw_usuarioscomissionados]    Script Date: 11/07/2014 16:56:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







ALTER view [dbo].[vw_usuarioscomissionados] as

SELECT DISTINCT
        uper.id_usuario,
        uper.id_entidade,
        us.st_nomecompleto,
				pp.id_perfilpedagogico as id_funcao,
        pp.st_perfilpedagogico AS st_funcao,
        us.st_cpf,
        pp.st_perfilpedagogico

FROM dbo.tb_usuarioperfilentidadereferencia AS uper
JOIN dbo.tb_perfil AS p ON uper.id_perfil = p.id_perfil
JOIN dbo.tb_perfilpedagogico AS pp      ON p.id_perfilpedagogico = pp.id_perfilpedagogico AND pp.id_perfilpedagogico IN (4, 2)
JOIN dbo.tb_usuario AS us       ON uper.id_usuario = us.id_usuario








GO



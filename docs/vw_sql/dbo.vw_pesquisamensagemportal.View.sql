ALTER VIEW [dbo].[vw_pesquisamensagemportal] AS
SELECT
  ms.id_mensagem,
  ms.st_mensagem,
  ms.bl_importante,
  ms.id_usuariocadastro,
  ms.st_texto,
  ms.dt_cadastro AS dt_cadastromsg,
  ms.id_entidade,
  ms.id_situacao,
  st.st_situacao,
  ms.id_projetopedagogico,
  ms.id_areaconhecimento,
  ms.id_turma,
  em.id_evolucao,
  em.dt_envio,
  em.dt_enviar,
  e.st_evolucao,
  pp.st_projetopedagogico,
  ar.st_areaconhecimento,
  tu.st_turma,
  usr.st_nomecompleto,
  em.dt_saida
FROM
  dbo.tb_mensagem AS ms
  INNER JOIN tb_enviomensagem AS em ON em.id_mensagem = ms.id_mensagem AND em.id_tipoenvio = 4
  INNER JOIN tb_evolucao AS e ON e.id_evolucao = em.id_evolucao
  LEFT JOIN dbo.tb_areaconhecimento AS ar ON ar.id_areaconhecimento = ms.id_areaconhecimento
  LEFT JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = ms.id_projetopedagogico
  LEFT JOIN dbo.tb_turma AS tu ON tu.id_turma = ms.id_turma
  INNER JOIN tb_usuario AS usr ON ms.id_usuariocadastro = usr.id_usuario
  INNER JOIN tb_situacao AS st ON ms.id_situacao = st.id_situacao
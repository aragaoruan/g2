
GO
/****** Object:  View [dbo].[vw_matriculaentregadocumento]    Script Date: 10/08/2012 17:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_matriculaentregadocumento] as

select dc.id_documentos, dc.st_documentos,dc.id_tipodocumento, du.id_documentosutilizacao ,us.id_usuario, us.st_nomecompleto , mt.id_matricula, cm.id_contrato,
cr.id_contratoresponsavel,ed.id_entregadocumentos, ed.id_situacao,dp.id_projetopedagogico, 
st_situacao = CASE ed.id_situacao
WHEN ed.id_situacao THEN st.st_situacao
ELSE 'Pendente'
END
from tb_matricula mt
LEFT JOIN tb_documentosprojetopedagogico as dp ON mt.id_projetopedagogico = dp.id_projetopedagogico
JOIN tb_documentos as dc ON (dc.id_tipodocumento = 1 and dp.id_documentos = dc.id_documentos)or (dc.id_entidade = mt.id_entidadeatendimento and dc.id_tipodocumento = 2) and dc.bl_ativo = 1
JOIn tb_documentosutilizacaorelacao as du ON du.id_documentos = dc.id_documentos
JOIN tb_contratomatricula as cm ON cm.id_matricula = mt.id_matricula
JOIN tb_usuario as us ON (us.id_usuario = mt.id_usuario and du.id_documentosutilizacao = 1)
LEFT JOIN tb_entregadocumentos as ed ON ed.id_documentos = dc.id_documentos and ed.id_usuario = us.id_usuario
LEFT JOIN tb_situacao as st ON ed.id_situacao = st.id_situacao
LEFT JOIN tb_contratoresponsavel as cr ON cr.id_contratoresponsavel is null

UNION

select dc.id_documentos, dc.st_documentos,dc.id_tipodocumento, du.id_documentosutilizacao ,us.id_usuario, us.st_nomecompleto , mt.id_matricula, cm.id_contrato, cr.id_contratoresponsavel, 
ed.id_entregadocumentos, ed.id_situacao, dp.id_projetopedagogico, 
st_situacao = CASE ed.id_situacao
WHEN ed.id_situacao THEN st.st_situacao
ELSE 'Pendente'
END
from tb_matricula mt
JOIN tb_documentos as dc ON dc.id_entidade = mt.id_entidadeatendimento and dc.bl_ativo = 1
JOIn tb_documentosutilizacaorelacao as du ON du.id_documentos = dc.id_documentos
JOIN tb_contratomatricula as cm ON cm.id_matricula = mt.id_matricula
JOIN tb_contratoresponsavel as cr ON cm.id_contrato = cr.id_contrato and cr.bl_ativo = 1 and cr.id_tipocontratoresponsavel = 1
JOIN tb_usuario as us ON us.id_usuario = cr.id_usuario and du.id_documentosutilizacao = 2 and mt.id_usuario != cr.id_usuario
LEFT JOIN tb_entregadocumentos as ed ON ed.id_documentos = dc.id_documentos and ed.id_contratoresponsavel = cr.id_contratoresponsavel
LEFT JOIN tb_situacao as st ON ed.id_situacao = st.id_situacao
LEFT JOIN tb_documentosprojetopedagogico as dp ON mt.id_projetopedagogico = dp.id_projetopedagogico
GO

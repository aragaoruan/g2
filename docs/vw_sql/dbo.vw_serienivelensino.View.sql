
GO
/****** Object:  View [dbo].[vw_serienivelensino]    Script Date: 10/08/2012 17:42:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_serienivelensino] AS
select s.*, n.*
from tb_serie s, tb_nivelensino n, tb_serienivelensino sn
where sn.id_serie = s.id_serie and
	  sn.id_nivelensino = n.id_nivelensino
GO

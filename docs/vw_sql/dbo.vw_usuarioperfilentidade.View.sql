CREATE VIEW [dbo].[vw_usuarioperfilentidade] AS
SELECT DISTINCT
	u.id_usuario,
	u.st_cpf,
	u.st_nomecompleto,
	pf.id_perfil,
	pf.st_nomeperfil,
	pe.st_nomeperfil AS st_perfilalias,
	upe.dt_inicio,
	upe.dt_termino,
	upe.id_situacao AS id_situacaoperfil,
	sp2.st_situacao AS st_situacaoperfil,
	e.id_entidade,
	e.st_nomeentidade,
	e.st_urlimglogo,
	upe.bl_ativo,
	pf.id_sistema,
	pf.id_perfilpedagogico,
	pf.id_entidade AS id_entidadecadastro,
	upe.bl_tutorial,
	upe.st_token
	
FROM tb_usuario u
JOIN tb_usuarioperfilentidade AS upe
	ON upe.id_usuario = u.id_usuario
JOIN tb_entidade AS e
	ON e.id_entidade = upe.id_entidade
JOIN tb_situacao AS sp2
	ON sp2.id_situacao = upe.id_situacao
JOIN tb_perfil AS pf
	ON pf.id_perfil = upe.id_perfil
LEFT JOIN tb_perfilentidade AS pe
	ON pe.id_entidade = upe.id_entidade
	AND pe.id_perfil = upe.id_perfil
WHERE u.bl_ativo = 1
AND pf.bl_ativo = 1
AND upe.bl_ativo = 1
AND e.bl_ativo = 1
AND e.id_situacao = 2
GO
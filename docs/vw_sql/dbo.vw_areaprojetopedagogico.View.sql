/****** Object:  View [dbo].[vw_areaprojetopedagogico]    Script Date: 10/08/2012 17:41:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_areaprojetopedagogico] AS
select app.id_areaconhecimento, app.id_projetopedagogico, ac.id_entidade, ac.st_areaconhecimento, ac.st_descricao, m.id_modulo, m.id_moduloanterior, m.st_modulo, m.st_tituloexibicao as st_tituloexibicaomodulo,
pp.st_descricao as st_descricaoprojetopedagogico, pp.st_projetopedagogico, pp.st_tituloexibicao as st_tituloexibicaoprojetopedagogico, pp.id_entidadecadastro,
mo.st_modulo as st_modulopai, mo.st_tituloexibicao as st_tituloexibicaopai, d.st_disciplina, d.st_tituloexibicao as st_tituloexibicaodisciplina,
d.id_disciplina, d.id_entidade as id_entidadedisciplina, d.id_situacao as id_situacaodisciplina, dsne.id_nivelensino, dsne.id_serie
from 
tb_areaprojetopedagogico as app
INNER JOIN tb_areaconhecimento as ac ON ac.id_areaconhecimento = app.id_areaconhecimento and ac.bl_ativo = 1
INNER JOIN tb_projetopedagogico as pp ON pp.id_projetopedagogico = app.id_projetopedagogico and pp.bl_ativo = 1
INNER JOIN tb_modulo as m ON m.id_projetopedagogico = pp.id_projetopedagogico and m.bl_ativo = 1
LEFT JOIN tb_modulo as mo ON mo.id_modulo = mo.id_moduloanterior and mo.bl_ativo = 1
INNER JOIN tb_modulodisciplina as md ON md.id_modulo = m.id_modulo and md.bl_ativo = 1
INNER JOIN tb_disciplinaserienivelensino as dsne ON dsne.id_disciplina = md.id_disciplina
INNER JOIN tb_disciplina as d ON d.id_disciplina = dsne.id_disciplina and d.bl_ativa = 1
GO

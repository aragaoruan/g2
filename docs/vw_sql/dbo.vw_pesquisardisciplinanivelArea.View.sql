
GO
/****** Object:  View [dbo].[vw_pesquisardisciplinanivelArea]    Script Date: 10/08/2012 17:42:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vw_pesquisardisciplinanivelArea] as
SELECT      d.id_disciplina, d.st_disciplina, d.st_descricao, d.id_tipodisciplina, td.st_tipodisciplina, d.nu_identificador, d.nu_cargahoraria, d.nu_creditos, d.nu_codigoparceiro,
            d.bl_ativa, d.bl_compartilhargrupo, d.id_situacao, s.st_situacao, d.id_entidade, e.st_nomeentidade, ne.id_nivelensino, ne.st_nivelensino, ad.id_areaconhecimento, a.st_areaconhecimento
    FROM        tb_disciplina AS d
        INNER JOIN                  tb_entidade AS e ON e.id_entidade = d.id_entidade
        INNER JOIN                  tb_tipodisciplina AS td ON td.id_tipodisciplina = d.id_tipodisciplina
        LEFT JOIN                  tb_disciplinaserienivelensino AS dsne ON dsne.id_disciplina = d.id_disciplina
        LEFT JOIN                  tb_nivelensino AS ne ON ne.id_nivelensino = dsne.id_nivelensino
        INNER JOIN                  tb_situacao AS s ON s.id_situacao = d.id_situacao
        LEFT JOIN                  tb_areadisciplina AS ad ON ad.id_disciplina = d.id_disciplina
        LEFT JOIN                  tb_areaconhecimento AS a ON a.id_areaconhecimento = ad.id_areaconhecimento
        where d.bl_ativa = 1
GO

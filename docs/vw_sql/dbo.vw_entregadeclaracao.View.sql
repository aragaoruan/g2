
GO
/****** Object:  View [dbo].[vw_entregadeclaracao]    Script Date: 10/08/2012 17:41:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_entregadeclaracao] as
SELECT ed.id_entregadeclaracao, ed.id_matricula, ed.id_venda, ed.dt_solicitacao, ed.dt_envio, ed.dt_entrega, ed.dt_cadastro,
ed.id_situacao, s.st_situacao, ed.id_textosistema, ts.st_textosistema
FROM tb_entregadeclaracao as ed
JOIN tb_situacao as s ON ed.id_situacao = s.id_situacao
JOIN tb_textosistema as ts ON ts.id_textosistema = ed.id_textosistema
GO

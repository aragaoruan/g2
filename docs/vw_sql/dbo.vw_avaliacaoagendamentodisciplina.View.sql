CREATE VIEW [dbo].[vw_avaliacaoagendamentodisciplina] AS
  SELECT
    DISTINCT
    ag.id_avaliacaoagendamento,
    ag.id_usuario,
    ag.id_matricula,
    av.id_avaliacao,
    ac.id_tipoprova,
    ag.id_situacao,
    md.id_disciplina,
    ag.bl_automatico,
    ag.id_usuariocadastro,
    ag.id_avaliacaoaplicacao,
    ag.id_entidade,
    ag.bl_ativo,
    ag.nu_presenca,
    ag.dt_cadastro,
    av.id_tipoavaliacao,
    ISNULL(ag.id_tipodeavaliacao, 0) AS id_tipodeavaliacao,
    ag.bl_provaglobal,
    acrf.id_avaliacaoconjuntoreferencia
  FROM
    tb_avaliacaoagendamento AS ag
    JOIN tb_avalagendamentoref AS agr ON ag.id_avaliacaoagendamento = agr.id_avaliacaoagendamento
    JOIN tb_avaliacaoconjuntoreferencia AS acrf ON acrf.id_avaliacaoconjuntoreferencia = agr.id_avaliacaoconjuntoreferencia
    JOIN tb_matricula AS mt ON mt.id_matricula = ag.id_matricula
    JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
    JOIN tb_alocacao AS al ON al.id_matriculadisciplina = md.id_matriculadisciplina AND al.bl_ativo = 1 AND al.id_saladeaula = acrf.id_saladeaula
    JOIN tb_avaliacao AS av ON av.id_avaliacao = ag.id_avaliacao
    JOIN tb_avaliacaoconjuntorelacao AS acr ON acr.id_avaliacao = av.id_avaliacao
    JOIN tb_avaliacaoconjunto AS ac ON ac.id_avaliacaoconjunto = acr.id_avaliacaoconjunto AND ac.id_tipoprova = 2
    JOIN tb_disciplina AS dc ON dc.id_disciplina = md.id_disciplina
    JOIN vw_entidadeesquemaconfiguracao AS cfg ON cfg.st_valor = 1 AND cfg.id_entidade = mt.id_entidadematricula AND cfg.id_itemconfiguracao = 24
  WHERE
    ag.id_situacao != 70

GO

/****** Object:  View [totvs].[vw_atualizafluxusgestor]    Script Date: 04/29/2013 10:48:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 CREATE VIEW [totvs].[vw_atualizafluxusgestor] as
SELECT DISTINCT CONTRATO ,
        PARCELA ,
        CENTRO_CUSTO ,
        CODCOLIGADA ,
        ID_ACORDO_FILHO ,
        IDLAN ,
        IDLAN_ACORDO_PAI ,
        CODCFO ,
        NOME ,
        NOME_FANTASIA ,
        CPF_CNPJ ,
        CONVERT(MONEY,VALOR_ORIGINAL) AS VALOR_ORIGINAL ,
        CONVERT(MONEY,VALOR_LIQUIDO) AS VALOR_LIQUIDO ,
        DATA_EMISSAO ,
        DATA_VENCIMENTO ,
        DATA_ALTERACAO ,
        STATUSLAN ,
        CATEGORIA ,
        TIPO_LAN�AMENTO AS TIPO_LANCAMENTO ,
        HISTORICO ,
        COD_MEIO_PAGAMENTO ,
        MEIO_PAGAMENTO ,
        NUMERO_DOCUMENTO ,
        DATA_BAIXA ,
        CONVERT(MONEY,VALOR_BAIXADO) AS VALOR_BAIXADO ,
        CONTA_CAIXA ,
        NUMERO_CHEQUE_EXTRATO ,
        CODIGO_BANCO_EXTRATO ,
        CODIGO_AGENCIA_EXTRATO ,
        DATA_VENCIMENTO_CHEQUE_EXTRATO ,
        NOSSO_NUMERO,
        lc.id_lancamento,
        lv.id_venda, lco.id_lancamento AS id_lancamentoorigem, lco.id_usuariolancamento, lco.id_entidade, lco.id_entidadelancamento, lco.id_usuariocadastro, mpi.id_meiopagamento
         FROM dbo.tb_lancamentointegracao AS li 
JOIN tb_lancamento AS lco ON lco.id_lancamento = li.id_lancamento
JOIN dbo.tb_entidadeintegracao AS ei ON ei.id_entidade = lco.id_entidade AND ei.id_sistema = 3
JOIN [189.114.56.180].CorporeRM_Int_Act.dbo.vw_fluxus_gestor AS fg ON li.st_codlancamento = CAST(fg.IDLAN AS VARCHAR) AND ei.st_codsistema = CAST(fg.CODCOLIGADA AS varchar)
JOIN dbo.tb_lancamentovenda AS lv ON lv.id_lancamento = li.id_lancamento
LEFT JOIN dbo.tb_meiopagamentointegracao AS mpi ON mpi.st_codsistema = fg.COD_MEIO_PAGAMENTO COLLATE SQL_Latin1_General_CP1_CI_AI   AND mpi.id_entidade  = ei.id_entidade
LEFT JOIN dbo.tb_lancamento AS lc ON lc.id_lancamento = li.id_lancamento AND li.st_codlancamento = fg.IDLAN
WHERE fg.DATA_ALTERACAO  >= DATEADD(MINUTE,-11, GETDATE())
UNION
SELECT DISTINCT CONTRATO ,
        PARCELA ,
        CENTRO_CUSTO ,
        CODCOLIGADA ,
        ID_ACORDO_FILHO ,
        IDLAN ,
        IDLAN_ACORDO_PAI ,
        CODCFO ,
        NOME ,
        NOME_FANTASIA ,
        CPF_CNPJ ,
        CONVERT(MONEY,VALOR_ORIGINAL) AS VALOR_ORIGINAL ,
        CONVERT(MONEY,VALOR_LIQUIDO) AS VALOR_LIQUIDO ,
        DATA_EMISSAO ,
        DATA_VENCIMENTO ,
        DATA_ALTERACAO ,
        STATUSLAN ,
        CATEGORIA ,
        TIPO_LAN�AMENTO AS TIPO_LANCAMENTO ,
        HISTORICO ,
        COD_MEIO_PAGAMENTO ,
        MEIO_PAGAMENTO ,
        NUMERO_DOCUMENTO ,
        DATA_BAIXA ,
        CONVERT(MONEY,VALOR_BAIXADO) AS VALOR_BAIXADO ,
        CONTA_CAIXA ,
        NUMERO_CHEQUE_EXTRATO ,
        CODIGO_BANCO_EXTRATO ,
        CODIGO_AGENCIA_EXTRATO ,
        DATA_VENCIMENTO_CHEQUE_EXTRATO ,
        NOSSO_NUMERO,
        lc.id_lancamento,
        lv.id_venda, lco.id_lancamento AS id_lancamentoorigem, lco.id_usuariolancamento, lco.id_entidade, lco.id_entidadelancamento, lco.id_usuariocadastro, mpi.id_meiopagamento
         FROM dbo.tb_lancamentointegracao AS li 
JOIN tb_lancamento AS lco ON lco.id_lancamento = li.id_lancamento
JOIN dbo.tb_entidadeintegracao AS ei ON ei.id_entidade = lco.id_entidade AND ei.id_sistema = 3
JOIN [189.114.56.180].CorporeRM_Int_Act.dbo.vw_fluxus_gestor AS fg ON (li.st_codlancamento = fg.IDLAN_ACORDO_PAI AND CAST(fg.IDLAN AS VARCHAR) NOT IN (SELECT st_codlancamento FROM dbo.tb_lancamentointegracao))AND ei.st_codsistema = CAST(fg.CODCOLIGADA AS VARCHAR)
JOIN dbo.tb_lancamentovenda AS lv ON lv.id_lancamento = li.id_lancamento
LEFT JOIN dbo.tb_meiopagamentointegracao AS mpi ON mpi.st_codsistema = fg.COD_MEIO_PAGAMENTO COLLATE SQL_Latin1_General_CP1_CI_AI   AND mpi.id_entidade  = ei.id_entidade
LEFT JOIN dbo.tb_lancamento AS lc ON lc.id_lancamento = li.id_lancamento AND li.st_codlancamento = fg.IDLAN
WHERE fg.DATA_ALTERACAO  >= DATEADD(MINUTE,-11, GETDATE())

GO




GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE VIEW [dbo].[vw_categoriaproduto] AS

select c.id_categoria,c.st_nomeexibicao,c.st_categoria,c.id_usuariocadastro,c.id_entidadecadastro,c.id_uploadimagem,c.id_categoriapai,c.id_situacao,c.dt_cadastro,c.bl_ativo
,cp.id_produto
from tb_categoria as c left join tb_categoriaproduto as cp on (c.id_categoria = cp.id_categoria)

GO

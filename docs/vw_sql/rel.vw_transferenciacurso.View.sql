CREATE VIEW [rel].[vw_transferenciacurso] AS
  SELECT
    e.id_entidade,
    va.id_atendente,
    e.st_nomeentidade,
    u_atendente.st_nomecompleto AS st_nomeatendente,
    u.id_usuario,
    u.st_nomecompleto,
    u.st_cpf,
    di.st_rg,
    ce.st_email,
    pt.nu_ddd AS st_ddd,
    pt.nu_telefone AS st_telefone,
    pe.st_cep,
    pe.st_cidade,
    pe.sg_uf,
    pe.st_bairro,
    pe.st_endereco,
    pe.nu_numero AS st_numero,
    pe.st_complemento,
    v.id_venda,
    mp.st_meiopagamento,
    v.nu_valorbruto,
    v.nu_valorliquido,
    l.nu_valor,
    l.nu_quitado,
    l.id_lancamento,
    l.dt_quitado,
    l.dt_vencimento,
    m.dt_cadastro AS dt_transferenciacurso,
    pporigem.st_projetopedagogico AS st_cursoorigem,
    pp.st_projetopedagogico AS st_cursodestino,
    va.nu_valortransferencia AS nu_valoraditivo,
    va.st_observacao AS st_observacaotransferencia
  FROM tb_matricula AS m
    JOIN tb_usuario AS u ON (u.id_usuario = m.id_usuario)
    LEFT JOIN tb_entidade AS e
      ON (e.id_entidade = m.id_entidadematricula)
    LEFT JOIN tb_documentoidentidade AS di
      ON (di.id_usuario = u.id_usuario)
    LEFT JOIN tb_contatosemailpessoaperfil AS cepp
      ON (cepp.id_usuario = u.id_usuario AND cepp.bl_padrao = 1 AND cepp.bl_ativo = 1)
    LEFT JOIN tb_contatosemail AS ce
      ON (ce.id_email = cepp.id_email)
    LEFT JOIN vw_pessoatelefone AS pt
      ON (pt.id_usuario = u.id_usuario
          AND pt.bl_padrao = 1)
    LEFT JOIN vw_pessoaendereco AS pe
      ON (pe.id_usuario = u.id_usuario
          AND pe.bl_padrao = 1
          AND pe.id_tipoendereco = 1)
    LEFT JOIN tb_vendaproduto AS vp
      ON (m.id_vendaproduto = vp.id_vendaproduto)
    LEFT JOIN tb_venda AS v
      ON (vp.id_venda = v.id_venda)
    LEFT JOIN tb_vendaaditivo AS va
      ON (va.id_venda = v.id_venda)
    LEFT JOIN tb_usuario AS u_atendente
      ON (va.id_atendente = u_atendente.id_usuario)
    LEFT JOIN tb_lancamentovenda AS lv
      ON (lv.id_venda = v.id_venda)
    LEFT JOIN tb_lancamento AS l
      ON (l.id_lancamento = lv.id_lancamento)
    LEFT JOIN tb_projetopedagogico AS pp
      ON (pp.id_projetopedagogico = m.id_projetopedagogico)
    LEFT JOIN tb_meiopagamento AS mp
      ON (mp.id_meiopagamento = l.id_meiopagamento)
    OUTER APPLY (
                  SELECT
                    oapp.st_projetopedagogico
                  FROM tb_matricula AS oam
                    LEFT JOIN tb_projetopedagogico AS oapp
                      ON (oapp.id_projetopedagogico = oam.id_projetopedagogico)
                  WHERE oam.id_matricula = m.id_matriculaorigem
                ) AS pporigem
  WHERE m.id_matriculaorigem IS NOT NULL;
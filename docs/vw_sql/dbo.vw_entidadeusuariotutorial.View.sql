
/****** Object:  View [dbo].[vw_entidadeusuariotutorial]    Script Date: 09/04/2015 14:26:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_entidadeusuariotutorial] AS
SELECT DISTINCT
	u.id_entidade,
	u.id_usuario,
	u.id_perfil,
	u.bl_tutorial AS bl_tutorialaluno,
	cast(e.st_valor as bit) AS bl_tutorialentidade
FROM
	tb_usuarioperfilentidade u
LEFT JOIN vw_entidadeesquemaconfiguracao e ON u.id_entidade = e.id_entidade and e.id_itemconfiguracao = 8
GO



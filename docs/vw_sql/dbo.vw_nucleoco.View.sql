CREATE VIEW [dbo].[vw_nucleoco] AS
SELECT
  nu.id_nucleoco,
  nu.id_tipoocorrencia,
  nu.id_entidade,
  nu.id_usuariocadastro,
  nu.id_situacao,
  nu.st_nucleoco,
  nu.dt_cadastro,
  nu.bl_ativo,
  nu.id_textonotificacao,
  nu.bl_notificaatendente,
  nu.bl_notificaresponsavel,
  ti.st_tipoocorrencia,
  si.st_situacao,
  nu.id_assuntocancelamento
FROM
  tb_nucleoco AS nu
  INNER JOIN tb_tipoocorrencia AS ti ON nu.id_tipoocorrencia = ti.id_tipoocorrencia
  INNER JOIN tb_situacao AS si ON si.id_situacao = nu.id_situacao
GO


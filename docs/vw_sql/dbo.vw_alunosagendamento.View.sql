CREATE VIEW [dbo].[vw_alunosagendamento] AS
SELECT DISTINCT
	u.id_usuario,
	u.st_nomecompleto,
	u.st_cpf,
	u.st_email,

	-- m.id_situacaoagendamento,
	situacao.id_situacaoagendamento,

	m.id_matricula,
	ap.id_aplicadorprova,
	ap.st_aplicadorprova,
	avp.bl_ativo,
	avp.dt_cadastro,
	avp.bl_unica,
	avp.dt_antecedenciaminima,
	avp.dt_alteracaolimite,
	avp.dt_aplicacao,
	avp.id_usuariocadastro,
	avp.id_horarioaula,
	avp.nu_maxaplicacao,
	avp.id_endereco,
	avp.id_avaliacaoaplicacao,
	pp.st_projetopedagogico,
	m.id_projetopedagogico,
	u.id_entidade,
	vw_end.sg_uf,
	ta.bl_ativo AS bl_ativoavagendamento,
	ta.id_avaliacaoagendamento,
	situacao.bl_provapordisciplina
FROM vw_pessoa AS u
JOIN tb_matricula AS m
	ON m.id_usuario = u.id_usuario
	AND m.bl_ativo = 1
	AND m.id_entidadeatendimento = u.id_entidade
	--AND m.id_situacaoagendamento = 120
	AND m.id_evolucao = 6
JOIN tb_projetopedagogico AS pp
	ON pp.id_projetopedagogico = m.id_projetopedagogico
outer APPLY(select top 1 gt.id_avaliacao from dbo.vw_avaliacaoaluno AS gt
	where gt.id_matricula = m.id_matricula
	AND gt.id_evolucaodisciplina IN (13, 19)
	AND gt.nu_cargahoraria != 0
	AND gt.id_tipoavaliacao = 4) as aa
LEFT JOIN dbo.tb_avaliacaoagendamento AS ta
	ON ta.id_matricula = m.id_matricula
	AND ta.id_situacao NOT IN (70, 69)
	AND ta.nu_presenca IS NULL
    and ta.id_avaliacao = aa.id_avaliacao
LEFT JOIN tb_avaliacaoaplicacao AS avp
	ON avp.id_avaliacaoaplicacao = ta.id_avaliacaoaplicacao
LEFT JOIN tb_aplicadorprova AS ap
	ON ap.id_aplicadorprova = avp.id_aplicadorprova
LEFT JOIN vw_aplicadorendereco AS vw_end
	ON vw_end.id_aplicadorprova = ap.id_aplicadorprova

-- Verificar se é Prova por Disciplina
LEFT JOIN vw_entidadeesquemaconfiguracao AS ec ON ec.id_entidade = m.id_entidadematricula AND ec.id_itemconfiguracao = 24 AND ec.st_valor = 1

LEFT JOIN tb_matriculadisciplina AS md ON md.id_matricula = m.id_matricula

OUTER APPLY (
  SELECT
    ISNULL(ec.st_valor, 0) AS bl_provapordisciplina,
    (CASE
      WHEN ISNULL(ec.st_valor, 0) = 1 THEN md.id_situacaoagendamento
      ELSE m.id_situacaoagendamento
    END) AS id_situacaoagendamento
) AS situacao

WHERE
  situacao.id_situacaoagendamento IN (120, 177)

GO
/****** Object:  View [dbo].[vw_historico]    Script Date: 10/08/2012 17:42:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_historico] as
select
mt.id_matricula, mt.id_projetopedagogico, aca.id_areaconhecimento as id_areaconhecimentopai, aca.st_areaconhecimento as st_areaconhecimentopai ,ad.id_areaconhecimento, ac.st_areaconhecimento, mdd.id_serie ,md.id_disciplina, md.nu_aprovafinal, dc.nu_cargahoraria, mt.dt_concluinte,
ed.sg_uf, ed.st_cidade,dc.st_disciplina, se.st_serie
-- Adicionado por Ejushiro
,ent.st_nomeentidade, ent.st_razaosocial
--///
from tb_matricula mt
JOIN tb_matriculadisciplina as md ON md.id_matricula = mt.id_matricula
JOIN tb_projetopedagogico as pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
JOIN tb_modulo as mdl ON mdl.id_projetopedagogico = pp.id_projetopedagogico and mdl.bl_ativo = 1
JOIN tb_modulodisciplina as mdd ON mdd.id_modulo = mdl.id_modulo and mdd.id_disciplina = md.id_disciplina and mdd.bl_ativo = 1
JOIN tb_areadisciplina as ad ON ad.id_disciplina = md.id_disciplina
JOIN tb_areaconhecimento as ac ON ac.id_areaconhecimento = ad.id_areaconhecimento and ac.id_tipoareaconhecimento = 2
JOIN tb_disciplinaserienivelensino as dsn ON mdd.id_disciplina = dsn.id_disciplina and mdd.id_nivelensino = dsn.id_nivelensino and mdd.id_serie = dsn.id_serie
LEFT JOIN tb_areaconhecimento as aca ON aca.id_areaconhecimento = ac.id_areaconhecimentopai and aca.id_tipoareaconhecimento = 1
JOIN tb_disciplina as dc ON dc.id_disciplina = md.id_disciplina
LEFT JOIN tb_entidadeendereco as ee ON ee.id_entidade = mt.id_entidadematriz and ee.bl_padrao = 1
LEFT JOIN tb_endereco as ed ON ed.id_endereco = ee.id_endereco
-- Adicionado por Ejushiro
JOIN tb_entidade as ent ON ent.id_entidade = mt.id_entidadematriz
JOIN tb_serie as se ON se.id_serie = dsn.id_serie
--///
GO

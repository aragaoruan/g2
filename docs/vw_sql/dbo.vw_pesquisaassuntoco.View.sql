
GO

/****** Object:  View [dbo].[vw_pesquisaassuntoco]    Script Date: 24/11/2014 14:46:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




 CREATE VIEW [dbo].[vw_pesquisaassuntoco] as
 /*SELECT DISTINCT aco.id_assuntoco
, aco.st_assuntoco
, aco.bl_abertura
, aco.bl_ativo
, aco.dt_cadastro
, aco.id_assuntocopai
, ap.st_assuntoco AS st_assuntocopai
, aco.id_situacao
, st.st_situacao
, tpo.id_tipoocorrencia
, tpo.st_tipoocorrencia
, ae.id_entidade
, aco.id_entidadecadastro,
NULL as id_funcao
,aco.id_textosistema
, na.id_nucleoco
FROM dbo.tb_assuntoco AS aco
LEFT JOIN dbo.tb_assuntoentidadeco AS ae ON ae.id_assuntoco = aco.id_assuntoco
JOIN dbo.tb_situacao AS st ON st.id_situacao = aco.id_situacao
JOIN dbo.tb_tipoocorrencia AS tpo ON tpo.id_tipoocorrencia = aco.id_tipoocorrencia
LEFT JOIN dbo.tb_nucleoco AS nc ON nc.id_entidade = ae.id_entidade
LEFT JOIN dbo.tb_nucleoassuntoco AS na ON na.id_assuntoco = aco.id_assuntoco AND na.bl_ativo = 1 AND na.id_nucleoco = nc.id_nucleoco
LEFT JOIN dbo.tb_assuntoco AS ap ON ap.id_assuntoco = aco.id_assuntocopai

UNION */

SELECT DISTINCT aco.id_assuntoco
, aco.st_assuntoco 
, aco.bl_abertura
, aco.bl_ativo
, aco.dt_cadastro
, aco.id_assuntocopai
,ap.st_assuntoco  AS st_assuntocopai
, aco.id_situacao
, st.st_situacao
, tpo.id_tipoocorrencia
, tpo.st_tipoocorrencia
, aco.id_entidadecadastro AS id_entidade
, aco.id_entidadecadastro,
NULL as id_funcao
,aco.id_textosistema
, na.id_nucleoco
, case WHEN aco.id_assuntocopai is null then aco.id_assuntoco 
	ELSE  aco.id_assuntocopai 
	END as nu_order
FROM dbo.tb_assuntoco AS aco
LEFT JOIN dbo.tb_assuntoentidadeco AS ae ON ae.id_assuntoco = aco.id_assuntoco
JOIN dbo.tb_situacao AS st ON st.id_situacao = aco.id_situacao
JOIN dbo.tb_tipoocorrencia AS tpo ON tpo.id_tipoocorrencia = aco.id_tipoocorrencia
LEFT JOIN dbo.tb_nucleoco AS nc ON nc.id_entidade = aco.id_entidadecadastro
LEFT JOIN dbo.tb_nucleoassuntoco AS na ON na.id_assuntoco = aco.id_assuntoco AND na.bl_ativo = 1 AND na.id_nucleoco = nc.id_nucleoco
LEFT JOIN dbo.tb_assuntoco AS ap ON ap.id_assuntoco = aco.id_assuntocopai

--LEFT JOIN dbo.tb_nucleopessoaco as np ON np.id_assuntoco = ae.id_assuntoco




GO



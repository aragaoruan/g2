CREATE VIEW [dbo].[vw_gridhistorico] AS
  SELECT
    mt.id_matricula
    , mt.id_projetopedagogico
    , pp.st_tituloexibicao
    , mdd.id_serie
    , se.st_serie
    , se.id_serieanterior
--, mdd.id_areaconhecimento AS id_areaconhecimento
--, ac.st_tituloexibicao AS st_areaconhecimento
--, ac.id_tipoareaconhecimento
    ,CAST(mtd.id_disciplina AS VARCHAR(200)) AS id_disciplina
    , dc.st_tituloexibicao AS st_disciplina
    ,nu_aprovafinal = (CASE
                     WHEN mtn.id_notaconceitual IS NOT NULL AND mtn.id_notaconceitual = 1 THEN '-'
                     WHEN mtn.id_notaconceitual IS NOT NULL AND mtn.id_notaconceitual = 2 THEN '-'
                     ELSE CONVERT(VARCHAR(8),mtd.nu_aprovafinal)
                     END
    )
    , pp.nu_notamaxima
    , mtd.dt_conclusao AS dt_conclusaodisciplina
    , dc.nu_cargahoraria AS nu_cargahoraria
    , mtd.id_evolucao
    , ev.st_evolucao
    , mtd.id_situacao
--, CAST (apm.st_cargahoraria AS INT) AS st_cargahorariaaproveitamento
--, apm.st_notaoriginal AS st_notaoriginalaproveitamento
--, apd.st_instituicao AS st_instituicaoaproveitamento
--, apd.sg_uf AS sg_ufaproveitamento
--, mn.st_nomemunicipio AS st_nomemunicipioaproveitamento
--,CAST(CAST(apm.nu_anoconclusao AS VARCHAR(4)) + '-01-01' AS DATE) AS dt_conclusaoaproveitamento
--, ent.st_nomeentidade
--, ed.sg_uf
--, mun.st_nomemunicipio
--, mdd.nu_ordem
--, ap.st_tituloexibicao AS st_areaagregadora
--, ap.id_areaconhecimento AS id_areaagregadora
    , st_semestre = (CASE
                     WHEN CAST(MONTH(sa.dt_abertura) AS VARCHAR(2)) < 07 THEN CAST(YEAR(sa.dt_abertura) AS VARCHAR(4)) + '.1'
                     WHEN CAST(MONTH(sa.dt_abertura) AS VARCHAR(2)) >= 07 THEN CAST(YEAR(sa.dt_abertura) AS VARCHAR(4)) + '.2'
                     ELSE '-'
                     END)
    , st_statusdisciplina = (CASE
                             WHEN mtn.id_notaconceitual IS NOT NULL AND mtn.id_notaconceitual = 1 THEN 'Não Concluido'
                             WHEN mtn.id_notaconceitual IS NOT NULL AND mtn.id_notaconceitual = 2 THEN 'Concluido'
                             WHEN al.id_alocacao IS NULL AND mtd.id_evolucao = 70 THEN 'Trancada'
                             WHEN al.id_alocacao IS NULL AND mtd.id_situacao != 65 THEN 'A cursar'
                             WHEN al.id_alocacao IS NOT NULL AND mtd.id_evolucao = 12 AND mtn.bl_aprovado = 1 THEN 'Aprovada'
                             WHEN al.id_alocacao IS NOT NULL AND mtn.bl_aprovado = 0 AND mtd.id_evolucao = 19 THEN 'Reprovado'
                             WHEN mtd.id_aproveitamento IS NOT NULL AND mtd.id_situacao = 65 THEN 'Isento'

                             WHEN al.id_alocacao IS NOT NULL AND mtd.id_aproveitamento IS NULL
                                  AND (
                                    mtd.nu_aprovafinal IS NULL
                                    OR ea.id_alocacao IS NULL
                                  )
                                  AND (
                                    (
                                      md.id_situacao = 13
                                      AND (
                                        mtn.st_notaead IS NULL
                                        OR mtn.st_notaatividade IS NULL
                                        OR mtn.st_notafinal IS NULL
                                           AND mtn.st_notarecuperacao IS NULL
                                      )
                                    )
                                    OR (
                                      md.id_situacao != 13
                                      AND ea.id_alocacao IS NULL
                                    )
                                  ) THEN 'Cursando'
                             ELSE '-'
                             END)
  FROM tb_matricula AS mt
    JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
    JOIN tb_modulo AS md ON md.id_projetopedagogico = mt.id_projetopedagogico AND md.bl_ativo = 1
    JOIN tb_modulodisciplina AS mdd ON mdd.id_modulo = md.id_modulo AND mdd.bl_ativo = 1
    JOIN tb_matriculadisciplina AS mtd ON mtd.id_matricula = mt.id_matricula and mtd.bl_obrigatorio = 1
    LEFT JOIN dbo.tb_alocacao AS al ON al.id_matriculadisciplina = mtd.id_matriculadisciplina AND al.bl_ativo = 1
    LEFT JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula
    LEFT JOIN tb_aproveitamentomatriculadisciplina AS apm ON apm.id_matricula = mtd.id_matricula AND apm.id_disciplina = mtd.id_disciplina
    LEFT JOIN tb_aproveitamentodisciplina AS apd ON apd.id_aproveitamentodisciplina = apm.id_aproveitamentodisciplina
    JOIN tb_disciplina AS dc ON dc.id_disciplina = mtd.id_disciplina AND dc.id_disciplina = mdd.id_disciplina
    LEFT JOIN tb_serie AS se ON se.id_serie = mdd.id_serie
    LEFT JOIN tb_nivelensino AS ne ON ne.id_nivelensino = mdd.id_nivelensino
    LEFT JOIN tb_areaconhecimento AS ac ON ac.id_areaconhecimento = mdd.id_areaconhecimento
    LEFT JOIN tb_municipio AS mn ON mn.id_municipio = apd.id_municipio
    JOIN tb_entidade AS ent ON ent.id_entidade = mt.id_entidadematriz
    LEFT JOIN tb_entidadeendereco AS eend ON eend.id_entidade = ent.id_entidade AND eend.bl_padrao = 1
    LEFT JOIN tb_endereco AS ed ON ed.id_endereco = eend.id_endereco
    LEFT JOIN tb_municipio AS mun ON mun.id_municipio = ed.id_municipio
    LEFT JOIN tb_evolucao AS ev ON ev.id_evolucao = mtd.id_evolucao
    LEFT JOIN dbo.tb_areaconhecimento AS ap ON ap.id_areaconhecimento = ac.id_areaconhecimentopai AND ap.id_tipoareaconhecimento = 1
    LEFT JOIN tb_encerramentoalocacao AS ea ON ea.id_alocacao = al.id_alocacao
    LEFT JOIN dbo.vw_matriculanota AS mtn ON mtd.id_matriculadisciplina = mtn.id_matriculadisciplina AND mtn.id_alocacao = al.id_alocacao
GO


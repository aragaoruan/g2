CREATE VIEW [dbo].[vw_aplicadorendereco]
AS
SELECT DISTINCT
			  dbo.tb_aplicadorprova.id_aplicadorprova, 
              dbo.tb_aplicadorprova.st_aplicadorprova,
              null as id_aplicadorpessoafisica, 
              null  as st_nomepessoafisica, 
              dbo.tb_entidade.id_entidade as id_aplicadorpessoajuridico, 
              dbo.tb_entidade.st_nomeentidade as st_nomepessoajuridica,   
              entidaderelacionada.id_entidade as id_entidaderelacionada,
              dbo.tb_aplicadorprova.id_entidadecadastro,
              dbo.tb_endereco.id_endereco,
              dbo.tb_endereco.st_endereco,
              dbo.tb_endereco.sg_uf,
              dbo.tb_endereco.id_municipio,
              dbo.tb_aplicadorprova.bl_ativo,
              aa.id_entidade as id_entidadeaplicacao
FROM      dbo.tb_aplicadorprova
JOIN      dbo.tb_aplicadorprovaentidade on dbo.tb_aplicadorprovaentidade.id_aplicadorprova = dbo.tb_aplicadorprova.id_aplicadorprova
JOIN      dbo.tb_entidade as entidaderelacionada on entidaderelacionada.id_entidade=dbo.tb_aplicadorprovaentidade.id_entidade
JOIN dbo.tb_entidade ON dbo.tb_entidade.id_entidade=dbo.tb_aplicadorprova.id_entidadeaplicador
JOIN dbo.tb_entidadeendereco ON (dbo.tb_entidadeendereco.id_entidade=dbo.tb_entidade.id_entidade and dbo.tb_entidadeendereco.bl_padrao=1)
LEFT JOIN dbo.tb_avaliacaoaplicacao as aa ON aa.id_entidade = dbo.tb_entidade.id_entidade
JOIN dbo.tb_endereco ON  
                                dbo.tb_endereco.id_endereco = dbo.tb_entidadeendereco.id_endereco
                                and dbo.tb_endereco.bl_ativo=1

WHERE dbo.tb_endereco.st_endereco IS NOT NULL
UNION
SELECT DISTINCT       
			  dbo.tb_aplicadorprova.id_aplicadorprova, 
              dbo.tb_aplicadorprova.st_aplicadorprova,
              dbo.tb_usuario.id_usuario as id_aplicadorpessoafisica, 
              dbo.tb_usuario.st_nomecompleto as st_nomepessoafisica, 
              null  as id_aplicadorpessoajuridico, 
              null as st_nomepessoajuridica,   
              entidaderelacionada.id_entidade as id_entidaderelacionada,
			  --null as id_entidaderelacionada,
              dbo.tb_aplicadorprova.id_entidadecadastro,
              dbo.tb_endereco.id_endereco,
              dbo.tb_endereco.st_endereco,
              dbo.tb_endereco.sg_uf,
              dbo.tb_endereco.id_municipio,
              dbo.tb_aplicadorprova.bl_ativo,
              aa.id_entidade as id_entidadeaplicacao
FROM      dbo.tb_aplicadorprova
JOIN      dbo.tb_aplicadorprovaentidade on dbo.tb_aplicadorprovaentidade.id_aplicadorprova = dbo.tb_aplicadorprova.id_aplicadorprova
JOIN      dbo.tb_entidade as entidaderelacionada on entidaderelacionada.id_entidade=dbo.tb_aplicadorprovaentidade.id_entidade
JOIN dbo.tb_usuario ON dbo.tb_usuario.id_usuario=dbo.tb_aplicadorprova.id_usuarioaplicador
JOIN dbo.tb_pessoaendereco ON (dbo.tb_pessoaendereco.id_usuario = dbo.tb_usuario.id_usuario and  dbo.tb_pessoaendereco.bl_padrao=0 and dbo.tb_pessoaendereco.id_entidade = dbo.tb_aplicadorprova.id_entidadecadastro /*entidaderelacionada.id_entidade*/)
JOIN dbo.tb_endereco ON dbo.tb_endereco.id_endereco = dbo.tb_pessoaendereco.id_endereco AND dbo.tb_endereco.id_tipoendereco = 6
LEFT JOIN dbo.tb_avaliacaoaplicacao as aa ON aa.id_entidade = dbo.tb_aplicadorprova.id_entidadeaplicador

WHERE dbo.tb_endereco.st_endereco IS NOT NULL
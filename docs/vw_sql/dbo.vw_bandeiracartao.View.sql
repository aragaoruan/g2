CREATE VIEW [dbo].[vw_bandeiracartao] AS
	SELECT	DISTINCT
			cb.st_cartaobandeira,
			cb.id_cartaobandeira,
			cc.id_sistema,
			ce.id_entidade
		FROM
			tb_cartaobandeira cb
			INNER JOIN tb_cartaoconfig cc ON cb.id_cartaobandeira	= cc.id_cartaobandeira
			INNER JOIN tb_contaentidade ce ON ce.id_contaentidade = cc.id_contaentidade
			INNER JOIN tb_entidadecartaoconfig ecc ON ecc.id_cartaoconfig	= cc.id_cartaoconfig
		WHERE ce.id_entidade = 14
		AND cc.id_sistema = 10
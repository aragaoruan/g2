
GO
/****** Object:  View [dbo].[vw_pessoaperfil]    Script Date: 10/08/2012 17:42:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_pessoaperfil] AS
select DISTINCT u.id_usuario, p.id_perfil, p.id_perfilpai, p.id_situacao as id_situacaoperfil, p.id_perfilpedagogico, u.st_nomecompleto, 
	   upe.id_situacao as id_situacaousuarioperfilentidade, sp.st_situacao as st_situacaoperfil, '-' as st_situacaopessoa, upe.dt_inicio, upe.dt_termino, e.id_entidade, '-' as id_situacaopessoa, e.st_nomeentidade
from tb_usuario u
JOIN tb_usuarioperfilentidade upe ON u.id_usuario = upe.id_usuario
JOIN tb_perfil p ON p.id_perfil = upe.id_perfil
JOIN tb_entidade e ON e.id_entidade = upe.id_entidade
--LEFT JOIN tb_pessoa pes ON pes.id_usuario = upe.id_usuario
--LEFT JOIN tb_situacao spess ON spess.id_situacao = pes.id_situacao AND pes.id_entidade = e.id_entidade
JOIN tb_situacao sp ON sp.id_situacao = upe.id_situacao
where 
	  upe.bl_ativo = 1 AND
	  p.bl_ativo = 1 AND
	  u.bl_ativo = 1
GO


GO
/****** Object:  View [dbo].[vw_pesquisarcontrato]    Script Date: 10/08/2012 17:42:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vw_pesquisarcontrato] as
select  u.id_usuario, u.st_nomecompleto, c.id_entidade, c.id_contrato, c.id_evolucao, c.id_situacao, s.st_situacao, e.st_evolucao
from tb_contrato as c
INNER JOIN tb_usuario as u ON u.id_usuario = c.id_usuario
INNER JOIN tb_evolucao as e ON e.id_evolucao = c.id_evolucao
INNER JOIN tb_situacao as s ON s.id_situacao = c.id_situacao
where c.bl_ativo = 1
GO

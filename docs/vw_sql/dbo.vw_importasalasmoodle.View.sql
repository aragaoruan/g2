/**
CREATE vw_importasalasmoodle
-- Busca as salas de aula a serem cadastradas no moodle de acordo com a sua sala de referencia
--@link da documentação http://confluence.unyleya.com.br/pages/viewpage.action?pageId=10883062

	Esse processo deve consultar:
		Salas integradas ao Moodle
		Que não tiveram seu conteúdo importado
		Que tenham uma sala de referência selecionada
		Que a data de abertura da sala no G2 seja menor ou igual a 50 dias

	Mais uma observação: o processo de importação não pode ser executado em uma sala com data de abertura passada.
	Isso é para evitar que o ROBO faça a importação de salas que já estão montadas.
**/
CREATE VIEW [dbo].[vw_importasalasmoodle] AS
  SELECT
    calculo.nu_dias,
    sa.id_saladeaula
-- id da sala de destino
    ,
    sa.st_saladeaula
-- nome da sala de destino
    ,
    di.id_disciplinaintegracao,
    di.st_codsistema
-- id_moodlesaladereferencia
    ,
    si.st_codsistemacurso
-- id_moodlesaladedestino
    ,
    di.st_salareferencia
-- nome sala de referencia
    ,
    di.id_entidade,
    si.id_saladeaulaintegracao,
    sa.id_entidadeintegracao
  FROM
    tb_saladeaulaintegracao AS si
    JOIN tb_saladeaula AS sa ON sa.id_saladeaula = si.id_saladeaula -- sala integrada
                                AND si.bl_conteudo = 0 -- não teve o conteúdo importado
                                AND si.bl_erroimportacao = 0 -- sem erro de importacao
                                AND si.id_disciplinaintegracao IS NOT NULL
-- tenho uma sala de referência selecionada
    JOIN tb_disciplinaintegracao AS di ON di.id_disciplinaintegracao = si.id_disciplinaintegracao
    OUTER APPLY (
                  SELECT
                    DATEDIFF(
                        DAY,
                        GETDATE(),
                        sa.dt_abertura
                    ) AS nu_dias
                ) AS calculo
  WHERE
    calculo.nu_dias <= 50 -- data de abertura da sala no G2 seja Menor ou igual a 50 dias
    AND sa.dt_abertura >=
        GETDATE() -- processo de importação não pode ser executado em uma sala com data de abertura passadaGO
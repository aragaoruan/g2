SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vw_aproveitamentodisciplinainterno] as
select
mt.id_matricula, mta.id_matricula as id_matriculaorigem, mta.id_entidadematricula as id_entidadematriculaorigem, eta.st_nomeentidade as st_nomeentidadematriculaorigem , ed.sg_uf, mn.st_nomemunicipio,
pp.st_projetopedagogico, (pp.nu_notamaxima*mtad.nu_aprovafinal/100) as st_notaoriginal ,mtad.id_disciplina, dca. st_disciplina, dca.nu_cargahoraria,
mtad.id_situacao, sta.st_situacao,mtad.id_evolucao, eva.st_evolucao,mtad.nu_aprovafinal, mtad.dt_conclusao
from tb_matricula as mt
JOIN tb_matricula as mta ON mta.id_usuario = mt.id_usuario and (mta.id_entidadematricula = mt.id_entidadematricula or mta.id_entidadematriz = mt.id_entidadematriz)
JOIN tb_matriculadisciplina as mtd ON mtd.id_matricula = mt.id_matricula and mtd.id_evolucao = 11
JOIN tb_matriculadisciplina as mtad ON mta.id_matricula = mtad.id_matricula and mtd.id_disciplina = mtad.id_disciplina and mtad.id_evolucao = 12
JOIN tb_entidade as eta ON eta.id_entidade = mta.id_entidadematricula
JOIN tb_evolucao as eva ON eva.id_evolucao = mtad.id_evolucao
JOIN tb_situacao as sta ON sta.id_situacao = mtad.id_situacao
JOIN tb_disciplina as dca ON dca.id_disciplina = mtad.id_disciplina
JOIN tb_entidadeendereco as etae ON etae.id_entidade = eta.id_entidade and etae.bl_padrao = 1
JOIN tb_endereco as ed ON ed.id_endereco = etae.id_endereco
JOIN tb_municipio as mn ON mn.id_municipio = ed.id_municipio
JOIN tb_projetopedagogico as PP ON pp.id_projetopedagogico = mta.id_projetopedagogico
GO
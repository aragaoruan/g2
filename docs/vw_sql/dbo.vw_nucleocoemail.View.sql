
GO
/****** Object:  View [dbo].[vw_nucleocoemail]    Script Date: 10/08/2012 17:42:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_nucleocoemail]
AS
SELECT ne.id_nucleoco, ec.id_emailconfig, ec.st_titulo, ec.st_usuario , ec.id_entidade
	FROM dbo.tb_emailconfig AS ec
	LEFT JOIN dbo.tb_nucleocoemail AS ne ON ec.id_emailconfig = ne.id_emailconfig
GO

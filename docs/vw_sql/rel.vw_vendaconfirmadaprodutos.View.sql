

/****** Object:  View [rel].[vw_vendaconfirmadasprodutos]    Script Date: 25/06/2015 16:44:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [rel].[vw_vendaconfirmadasprodutos] AS 
SELECT DISTINCT
	st_nomecompleto,
	st_email,
	st_cpf,
	CAST (nu_ddd AS VARCHAR) + CAST (nu_telefone AS VARCHAR) AS st_telefone,
	st_endereco,
	st_cep,
	st_bairro,
	us.st_complemento,
	nu_numero,
	st_cidade,
	us.sg_uf,
	pd.st_produto,
	pd.id_produto,
	vp.nu_valorliquido,
	vd.nu_valorliquido AS nu_valorliquidovenda,
	vp.nu_valorbruto,
	vd.dt_confirmacao,
	--mp.st_meiopagamento,
	pd.id_tipoproduto,
	vd.id_venda,
	vd.id_evolucao,
	ve.id_entidade,
	vd.dt_cadastro,
	pdc.st_produto AS st_combo,
	pdc.id_produto AS id_produtocombo,
	ve.st_nomeentidade,
	te.id_entidade AS id_entidadepai,
	vd.st_observacao,
	us.id_usuario AS cod_aluno,
	vd.id_cupom,
	us.st_rg, liv.st_isbn,
	CASE cp.nu_desconto
WHEN 100.00 THEN
	'Gratuito'
ELSE
	mp.st_meiopagamento
END AS 'st_meiopagamento'
FROM
	tb_entidade AS te
JOIN dbo.vw_entidaderecursivaid AS ve ON ve.nu_entidadepai = te.id_entidade OR ve.id_entidade = te.id_entidade
JOIN dbo.tb_venda AS vd ON vd.id_entidade = ve.id_entidade
JOIN dbo.vw_pessoa AS us ON us.id_usuario = vd.id_usuario AND us.id_entidade = vd.id_entidade
LEFT JOIN dbo.tb_municipio AS mn ON mn.id_municipio = us.id_municipio
JOIN dbo.tb_vendaproduto AS vp ON vd.id_venda = vp.id_venda
JOIN dbo.tb_produto AS pd ON pd.id_produto = vp.id_produto
LEFT JOIN dbo.tb_lancamentovenda AS lv ON lv.id_venda = vd.id_venda AND bl_entrada = 1
LEFT JOIN dbo.tb_lancamento AS lc ON lc.id_lancamento = lv.id_lancamento
LEFT JOIN dbo.tb_meiopagamento AS mp ON mp.id_meiopagamento = lc.id_meiopagamento
LEFT JOIN tb_produto AS pdc ON pdc.id_produto = vp.id_produtocombo
LEFT JOIN tb_cupom AS cp ON cp.id_cupom = vd.id_cupom
LEFT JOIN dbo.tb_produtolivro AS pl ON pl.id_produto = pd.id_produto
LEFT JOIN dbo.tb_livro AS liv ON liv.id_livro = pl.id_livro
WHERE
	vd.bl_ativo = 1
GO



CREATE VIEW [dbo].[vw_gradetipo] AS
SELECT  DISTINCT sa.dt_abertura,
pp.id_projetopedagogico,
pp.st_projetopedagogico,
dc.st_tituloexibicao AS st_saladeaula,
aa.id_avaliacao ,
/*st_nota = CASE WHEN dc.id_tipodisciplina = 2 AND aa.dt_defesa IS NULL AND aa.bl_ativo = 1
			THEN null
			else CAST(aa.st_nota AS NUMERIC(8,2))
		  END ,
		  Retirada para correção: http://jira.unyleya.com.br/secure/RapidBoard.jspa?rapidView=3&view=detail&selectedIssue=AC-679 */
st_nota = CAST(aa.st_nota AS NUMERIC(8,2)) ,
mdc.id_modulo,
mdl.st_modulo,
mdl.st_tituloexibicao AS st_tituloexibicaomodulo,
aa.id_avaliacaoconjuntoreferencia,
dc.id_disciplina,
dc.st_tituloexibicao AS st_disciplina,
dc.id_tipodisciplina,
dc.st_tituloexibicao AS st_tituloexibicaodisciplina,
mt.id_matricula ,
id_tipoavaliacao = CASE WHEN tt.id_tipoavaliacao IS NULL THEN 5
					ELSE tt.id_tipoavaliacao
				   END ,
aa.st_tipoavaliacao AS st_labeltipo,
nu_notafinal = CASE WHEN dc.id_tipodisciplina = 2 AND aa.dt_defesa IS NULL THEN NULL
				ELSE CAST((md.nu_aprovafinal* pp.nu_notamaxima/100)AS NUMERIC(5,2))
			   END ,
ev.id_evolucao,
ev.st_evolucao,
st.id_situacao,
st.st_situacao,
dc.nu_cargahoraria,
sa.id_saladeaula,
sa.dt_encerramento,
ndm.nu_notatotal,
st_status = CASE WHEN md.id_evolucao = 12 THEN ev.st_evolucao
				WHEN md.id_evolucao = 19 THEN ev.st_evolucao
				WHEN md.id_situacao = 65 THEN 'Crédito Concedido'
				WHEN (ndm.nu_notatotal + nf.nu_notafaltante) >= (pp.nu_notamaxima * pp.nu_percentualaprovacao/100) THEN 'Satisfatório'
				WHEN (ndm.nu_notatotal + nf.nu_notafaltante) < (pp.nu_notamaxima * pp.nu_percentualaprovacao/100) THEN 'Insatisfatório'
				ELSE '-'
			END
/*,bl_status  = CASE WHEN md.id_evolucao = 12 THEN 1
				WHEN md.id_evolucao = 19 THEN 0
				WHEN md.id_situacao = 65 THEN 1
				WHEN (ndm.nu_notatotal + nf.nu_notafaltante) >= (pp.nu_notamaxima * pp.nu_percentualaprovacao/100) THEN 1
				WHEN (ndm.nu_notatotal + nf.nu_notafaltante) < (pp.nu_notamaxima * pp.nu_percentualaprovacao/100) THEN 0
				ELSE 0
			END			*/
,bl_status  =
    (CASE
      -- Se a disciplina nao for obrigatoria entao esta APTO PARA AGENDAMENTO
      WHEN md.bl_obrigatorio = 0 THEN 1
      -- Se a sala nao foi encerrada NAO esta APTO PARA AGENDAMENTO
      WHEN ea.id_encerramentosala IS NULL THEN 0
      -- Calcular notas e aplicar o status
      WHEN (ndm.nu_notatotal + ISNULL(nf.nu_notafaltante,0)) >= (pp.nu_notamaxima * pp.nu_percentualaprovacao/100) THEN 1
      WHEN (ndm.nu_notatotal + ISNULL(nf.nu_notafaltante,0)) < (pp.nu_notamaxima * pp.nu_percentualaprovacao/100) THEN 0
      ELSE 0
   END)

FROM    dbo.tb_matricula AS mt
JOIN dbo.tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
JOIN dbo.tb_disciplina AS dc ON dc.id_disciplina = md.id_disciplina
LEFT JOIN dbo.tb_alocacao AS al ON al.id_matriculadisciplina = md.id_matriculadisciplina AND al.bl_ativo = 1
LEFT JOIN tb_encerramentoalocacao AS ea ON ea.id_alocacao = al.id_alocacao
JOIN dbo.tb_tipoavaliacao AS tt ON tt.id_tipoavaliacao IN  (4,5,6)
LEFT JOIN dbo.vw_avaliacaoaluno AS aa ON aa.id_tipoavaliacao = tt.id_tipoavaliacao AND mt.id_matricula = aa.id_matricula AND aa.id_disciplina = md.id_disciplina AND aa.id_tipoprova = 2 AND aa.id_saladeaula = al.id_saladeaula
JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
JOIN dbo.tb_modulo AS mdl ON mdl.id_projetopedagogico = pp.id_projetopedagogico AND mdl.bl_ativo = 1
JOIN dbo.tb_modulodisciplina AS mdc ON mdc.id_modulo = mdl.id_modulo AND mdc.id_disciplina = dc.id_disciplina   AND mdc.bl_ativo = 1
LEFT JOIN dbo.tb_disciplinasaladeaula AS ds ON aa.id_saladeaula = al.id_saladeaula AND ds.id_disciplina = md.id_disciplina
LEFT JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula
LEFT JOIN dbo.tb_avaliacaodisciplina AS ad ON ad.id_avaliacao = aa.id_avaliacao AND aa.id_disciplina = ad.id_disciplina
JOIN tb_evolucao AS ev ON ev.id_evolucao = md.id_evolucao
JOIN dbo.tb_situacao AS st ON st.id_situacao = md.id_situacao
LEFT JOIN vw_notasdisciplinamatricula AS ndm ON ndm.id_disciplina = md.id_disciplina AND ndm.id_tipoprova = 2 AND ndm.id_matricula = mt.id_matricula
OUTER APPLY ( SELECT    id_matricula ,
				id_disciplina ,
				SUM(CAST(nu_notamax AS FLOAT)) AS nu_notafaltante
						FROM      vw_avaliacaoaluno
						WHERE     bl_ativo IS NULL AND id_matricula = mt.id_matricula AND id_disciplina = md.id_disciplina AND id_tipoprova = 2
							AND id_avaliacaorecupera = 0
							AND id_saladeaula = al.id_saladeaula
						GROUP BY  id_disciplina ,
						id_matricula
			)  AS nf
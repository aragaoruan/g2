
GO

/****** Object:  View [dbo].[vw_vendalancamento]    Script Date: 11/29/2012 09:24:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/**

apagada por ter sido feita atoa

**/
drop view [vw_gerarvendatextolancamento]

CREATE VIEW [dbo].[vw_gerarvendatextolancamento] as

select vd.id_venda, vd.id_usuario AS id_usuariovenda
, us.st_nomecompleto 
, us.st_cpf
,vd.id_evolucao, vd.id_formapagamento
, vd.nu_parcelas, vd.nu_valorliquido, lv.bl_entrada, lv.nu_ordem
, lc. id_lancamento ,
        lc.nu_valor ,
        CAST('0.00' AS NUMERIC(7,2)) AS nu_valoratualizado,
        lc.nu_vencimento ,
        lc.id_tipolancamento ,
        lc.dt_cadastro ,
        lc.id_meiopagamento ,
        lc.id_usuariolancamento ,
        lc.id_entidadelancamento,
        lc.id_entidade ,
        lc.id_usuariocadastro ,
        lc.st_banco ,
        lc.id_cartaoconfig ,
        lc.id_boletoconfig ,
        lc.bl_quitado ,
        lc.dt_vencimento as dt_lancamentovencimento,
        lc.dt_quitado as dt_lancamentodataquitado ,
        lc.dt_emissao ,
        lc.dt_prevquitado ,
        lc.st_emissor ,
        lc.st_coddocumento ,
        lc.nu_juros ,
        lc.nu_desconto ,
        lc.nu_quitado as nu_lancamentovalor,
        
        lc.nu_multa , mp.st_meiopagamento as st_lancamentomeiopagamento,
        fp.nu_juros AS nu_jurosatraso,
        fp.nu_multa AS nu_multaatraso,
        vmv.nu_diavencimento,
        tl.st_tipolancamento,
        lc.bl_ativo,
        li.st_codlancamento

from tb_venda as vd
JOIN dbo.tb_usuario AS us ON us.id_usuario = vd.id_usuario

JOIN tb_lancamentovenda as lv ON lv.id_venda = vd.id_venda

JOIN tb_lancamento as lc ON lc.id_lancamento = lv.id_lancamento

JOIN dbo.tb_formapagamento AS fp ON fp.id_formapagamento = vd.id_formapagamento

LEFT JOIN tb_meiopagamento as mp ON lc.id_meiopagamento = mp.id_meiopagamento

LEFT JOIN tb_vendameiovencimento as vmv ON vd.id_venda = vmv.id_venda and vmv.id_meiopagamento = mp.id_meiopagamento

JOIN dbo.tb_tipolancamento AS tl ON tl.id_tipolancamento = lc.id_tipolancamento

LEFT JOIN dbo.tb_lancamentointegracao AS li ON li.id_lancamento = lc.id_lancamento


GO




GO
/****** Object:  View [totvs].[vw_lancamentos]    Script Date: 10/08/2012 17:42:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [totvs].[vw_lancamentos] AS
SELECT lv.id_venda, lc.id_lancamento, lc.bl_quitado, lc.dt_vencimento, lc.nu_valor, lc.id_usuariolancamento, us.st_cpf, us.st_nomecompleto, FLAN.IDLAN, FLAN.CODCFO, FLAN.CODCCUSTO, flan.CODCOLIGADA, FLAN.STATUSLAN, flan.DATABAIXA AS dt_baixa, CAST(flan.VALORBAIXADO AS NUMERIC(30,5)) AS nu_valorbaixado, flan.VALORORIGINAL, lc.id_entidade FROM dbo.tb_lancamentointegracao lci
INNER JOIN dbo.tb_lancamento lc ON lci.id_lancamento = lc.id_lancamento AND bl_quitado = 0
INNER JOIN dbo.tb_usuario us ON us.id_usuario = lc.id_usuariolancamento
INNER JOIN dbo.tb_lancamentovenda lv ON lc.id_lancamento = lv.id_lancamento
INNER JOIN dbo.tb_entidadeintegracao ei ON lc.id_entidade = ei.id_entidade AND ei.id_sistema = 3
INNER JOIN [189.114.56.180].CorporeRM.dbo.FLAN flan ON 
flan.CODCCUSTO COLLATE Latin1_General_CI_AS = ei.st_codchave COLLATE Latin1_General_CI_AS AND flan.IDLAN = lci.st_codlancamento 
AND flan.STATUSLAN IN (1) AND flan.DATABAIXA IS NOT null
GO

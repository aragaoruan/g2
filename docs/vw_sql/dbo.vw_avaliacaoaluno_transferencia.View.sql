SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_avaliacaoaluno_transferencia]
AS
SELECT DISTINCT
	id_avaliacaoaluno,
	mt.id_matricula,
	us.st_nomecompleto,
	mdl.id_modulo,
	mdl.st_tituloexibicao AS st_tituloexibicaomodulo,
	dc.id_disciplina,
	dc.st_tituloexibicao AS st_tituloexibicaodisciplina,
	aa.st_nota,
	ag.id_avaliacaoagendamento,
	acr.id_avaliacao,
	av.st_avaliacao,
	md.id_situacao,
	st.st_situacao,
	ac.id_tipoprova,
	acrf.id_avaliacaoconjuntoreferencia,
	md.id_evolucao,
	ev.st_evolucao,
	CAST(av.nu_valor AS varchar(100)) AS nu_notamax,
	ac.id_tipocalculoavaliacao,
	av.id_tipoavaliacao,
	ta.st_tipoavaliacao,
	acr.id_avaliacaorecupera,
	pp.nu_notamaxima,
	pp.nu_percentualaprovacao,
	ag.dt_agendamento,
	aa.bl_ativo,
	aa.dt_avaliacao,
	alc.id_saladeaula,
	ui.st_codusuario,
	ac.id_avaliacaoconjunto,
	pp.st_projetopedagogico,
	mt.id_projetopedagogico,
	aa.dt_defesa,
	aa.st_tituloavaliacao,
	mt.id_entidadeatendimento,
	aa.id_upload,
	dc.id_tipodisciplina,
	aa.st_justificativa,
	md.id_matriculadisciplina,
	aa.id_tiponota
	,alc.id_alocacao
FROM tb_matricula AS mt
JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula

JOIN dbo.tb_usuario AS us ON us.id_usuario = mt.id_usuario

JOIN tb_modulo AS mdl ON mdl.id_projetopedagogico = mt.id_projetopedagogico	AND mdl.bl_ativo = 1

JOIN tb_modulodisciplina AS mdc ON mdc.id_modulo = mdl.id_modulo AND md.id_disciplina = mdc.id_disciplina AND mdc.bl_ativo = 1

JOIN tb_alocacao AS alc ON  alc.id_matriculadisciplina = md.id_matriculadisciplina --and alc.bl_ativo = 1
JOIN tb_avaliacaoconjuntoreferencia AS acrf	ON (acrf.id_saladeaula = alc.id_saladeaula)
JOIN tb_disciplinasaladeaula as dsa on dsa.id_saladeaula = acrf.id_saladeaula
JOIN tb_disciplina AS dc ON dc.id_disciplina = mdc.id_disciplina AND dc.id_disciplina = md.id_disciplina and dsa.id_disciplina = dc.id_disciplina
JOIN tb_avaliacaoconjunto AS ac	ON ac.id_avaliacaoconjunto = acrf.id_avaliacaoconjunto AND (acrf.id_saladeaula = alc.id_saladeaula AND id_tipoprova = 2)
JOIN tb_avaliacaoconjuntorelacao AS acr	ON acr.id_avaliacaoconjunto = ac.id_avaliacaoconjunto
JOIN tb_avaliacao AS av	ON av.id_avaliacao = acr.id_avaliacao
JOIN tb_tipoavaliacao AS ta ON ta.id_tipoavaliacao = av.id_tipoavaliacao
LEFT JOIN tb_avaliacaoagendamento AS ag	ON mt.id_matricula = ag.id_matricula AND ag.id_avaliacaoconjuntoreferencia = acrf.id_avaliacaoconjuntoreferencia AND av.id_avaliacao = ag.id_avaliacao
LEFT JOIN tb_avaliacaoaluno AS aa
	ON aa.id_matricula = mt.id_matricula
	AND aa.id_avaliacaoconjuntoreferencia = acrf.id_avaliacaoconjuntoreferencia
	AND aa.id_avaliacao = av.id_avaliacao
	AND aa.bl_ativo = 1
	AND aa.id_situacao IN (86)
JOIN tb_situacao AS st
	ON st.id_situacao = md.id_situacao
JOIN tb_evolucao AS ev
	ON ev.id_evolucao = md.id_evolucao
JOIN tb_projetopedagogico AS PP
	ON pp.id_projetopedagogico = mt.id_projetopedagogico
LEFT JOIN dbo.tb_usuariointegracao AS ui
	ON ui.id_usuario = mt.id_usuario AND (ui.id_entidade = mt.id_entidadeatendimento OR ui.id_entidade = mt.id_entidadematricula) WHERE mt.bl_ativo = 1 

GO
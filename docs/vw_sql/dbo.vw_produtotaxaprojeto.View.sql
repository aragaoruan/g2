
GO
/****** Object:  View [dbo].[vw_produtotaxaprojeto]    Script Date: 10/08/2012 17:42:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_produtotaxaprojeto] as        
SELECT ptp.id_taxa ,
        ptp.id_entidade ,
        ptp.id_produto ,
        ptp.id_projetopedagogico ,
        pd.st_produto ,
        pd.nu_valor,
        pd.nu_valormensal,
        pd.id_tipoproduto FROM dbo.tb_produtotaxaprojeto AS ptp
JOIN dbo.vw_produto AS pd ON pd.id_produto = ptp.id_produto AND id_situacao = 45
JOIN dbo.tb_produtotaxa AS pt ON pt.id_produto = pd.id_produto
JOIN tb_taxa AS tx ON tx.id_taxa = pt.id_taxa
GO

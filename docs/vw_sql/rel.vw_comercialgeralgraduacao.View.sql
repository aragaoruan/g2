ALTER VIEW [rel].[vw_comercialgeralgraduacao] AS
  SELECT DISTINCT
    vwcg.id_entidade,
    vwcg.id_venda,
    vwcg.id_matricula,
    vwcg.st_nomecompleto,
    vwcg.st_cpf,
    vwcg.st_cidade,
    vwcg.sg_uf,
    vwcg.st_areaconhecimento,
    vwcg.st_projetopedagogico,
    vwcg.dt_matricula,
    vwcg.st_evolucao,
    vwcg.st_atendente,
    vwcg.dt_inicio,
    vwcg.st_turma,
    vwcg.nu_valorliquido,
    vwcg.nu_valorboleto,
    vwcg.nu_valoroutros,
    vwcg.nu_parcelas AS st_nuparcelas,
    lce.nu_valorentrada,
    lsp.nu_valorsegundaparcela,
    vpcp.st_campanhacomercial,
    CASE
    WHEN vpcp.st_campanhacomercial IS NOT NULL THEN
      CONCAT (
          CAST (
              vpcp.nu_descontocampanha AS VARCHAR
          ),
          ' %'
      )
    ELSE
      ''
    END AS st_descontocomercial,
    dpcp.nu_porcentagemdescontocomercial,
    dpcp.st_campanhapontualidade,
    CASE
    WHEN dpcp.st_campanhapontualidade IS NOT NULL THEN
      CONCAT (
          CAST (
              dpcp.nu_descontopontualidade AS VARCHAR
          ),
          ' %'
      )
    ELSE
      ''
    END AS st_descontopontualidade,
    vwcg.id_produto,
    vwcg.st_nomeentidade,
    vwcg.id_entidadepai,
    CASE
    WHEN dpcp.id_tipodesconto = 1 THEN
      0
    ELSE
      isnull(lce.nu_valorentrada, 0) + (
        isnull(
            lsp.nu_valorsegundaparcela,
            0
        ) * (
          1 - (
            isnull(dpcp.nu_valordesconto, 0) / 100
          )
        ) * (
          isnull(vwcg.nu_parcelas, 0) - 1
        )
      )
    END AS nu_valordevenda,
      st_afiliado = (
      CASE
      WHEN afi.st_afiliado IS NOT NULL THEN
        afi.st_afiliado
      ELSE
        '-'
      END
    ),
      st_nucleotelemarketing = (
      CASE
      WHEN nt.st_nucleotelemarketing IS NOT NULL THEN
        nt.st_nucleotelemarketing
      ELSE
        '-'
      END
    ),
      st_vendedor = (
      CASE
      WHEN vdr.st_vendedor IS NOT NULL THEN
        vdr.st_vendedor
      ELSE
        '-'
      END
    ),
      st_siteorigem = (
      CASE
      WHEN vd.st_siteorigem IS NOT NULL THEN
        vd.st_siteorigem
      ELSE
        '-'
      END
    ),
    total_disciplinas.nu_disciplinas AS st_totaldisciplinaselecionada,
    vd.nu_creditos AS st_nucreditos,
    vd.st_nomconvenio,
    vd.st_codpromocao
  FROM
    rel.vw_comercialgeral AS vwcg
    JOIN dbo.tb_venda AS vd ON vd.id_venda = vwcg.id_venda
    LEFT JOIN dbo.tb_afiliado AS afi ON afi.id_afiliado = vd.id_afiliado
    LEFT JOIN dbo.tb_nucleotelemarketing AS nt ON nt.id_nucleotelemarketing = vd.id_nucleotelemarketing
    LEFT JOIN dbo.tb_vendedor AS vdr ON vdr.id_vendedor = vd.id_vendedor

    OUTER APPLY (
                  SELECT
                    SUM (nu_valor) AS nu_valorentrada
                  FROM
                    dbo.vw_vendalancamento
                  WHERE
                    id_venda = vwcg.id_venda
                    AND bl_entrada = 1
                ) AS lce

    OUTER APPLY (
                  SELECT
                    nu_valor AS nu_valorsegundaparcela
                  FROM
                    dbo.tb_lancamentovenda lv
                    JOIN dbo.tb_lancamento l ON lv.id_lancamento = l.id_lancamento
                  WHERE
                    lv.id_venda = vwcg.id_venda
                    AND bl_original = 1
                    AND bl_entrada = 0
                  ORDER BY
                    nu_ordem ASC OFFSET 1 ROWS FETCH NEXT 1 ROWS ONLY
                ) AS lsp

    OUTER APPLY (
                  SELECT
                    ISNULL(cp.nu_valordesconto, 0) AS nu_descontopontualidade,
                    vd.nu_descontovalor AS nu_porcentagemdescontocomercial,
                    cp.id_tipodesconto,
                    cp.nu_valordesconto,
                    cp.st_campanhacomercial AS st_campanhapontualidade
                  FROM
                    tb_venda vd
                    LEFT JOIN tb_vendaproduto vp ON vd.id_venda = vp.id_venda
                    LEFT JOIN tb_campanhacomercial cp ON cp.id_campanhacomercial = vd.id_campanhapontualidade
                                                         AND cp.bl_ativo = 1
                  WHERE
                    vd.id_venda = vwcg.id_venda
                ) AS dpcp

    OUTER APPLY (
                  SELECT
                    ISNULL(cp.nu_valordesconto, 0) AS nu_descontocampanha,
                    cp.st_campanhacomercial AS st_campanhacomercial
                  FROM
                    tb_vendaproduto vp
                    LEFT JOIN tb_campanhacomercial cp ON cp.id_campanhacomercial = vp.id_campanhacomercial
                                                         AND cp.bl_ativo = 1
                  WHERE
                    vp.id_venda = vwcg.id_venda
                ) AS vpcp

    OUTER APPLY (
                  SELECT
                    COUNT (
                        predisc.id_prematriculadisciplina
                    ) AS nu_disciplinas
                  FROM
                    tb_prematriculadisciplina AS predisc
                  WHERE
                    predisc.id_venda = vwcg.id_venda
                ) AS total_disciplinas
CREATE VIEW [dbo].[vw_getdefesatcc] AS
SELECT
  aa.dt_defesa,
  md.id_matricula,
  di.id_tipodisciplina,
  md.id_evolucao,
  md.id_matriculadisciplina
FROM tb_matriculadisciplina AS md
  INNER JOIN tb_disciplina AS di ON di.id_disciplina = md.id_disciplina
  INNER JOIN tb_avaliacaoaluno AS aa ON aa.id_matricula = md.id_matricula
GO
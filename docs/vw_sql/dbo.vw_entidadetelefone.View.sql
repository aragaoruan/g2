
GO
/****** Object:  View [dbo].[vw_entidadetelefone]    Script Date: 10/08/2012 17:41:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_entidadetelefone] AS
select ent.id_entidade,ct.id_telefone, tt.id_tipotelefone, 
	   tt.st_tipotelefone, ct.nu_ddi, ct.nu_ddd, ct.nu_telefone, ct.st_descricao, cte.bl_padrao
from  tb_entidade ent, tb_contatostelefoneentidade cte, tb_contatostelefone ct, tb_tipotelefone tt
where ent.id_entidade = cte.id_entidade and
	  ct.id_telefone  = cte.id_telefone and
	  ct.id_tipotelefone = tt.id_tipotelefone
GO

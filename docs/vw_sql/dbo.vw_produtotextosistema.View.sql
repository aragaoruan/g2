
GO
/****** Object:  View [dbo].[vw_produtotextosistema]    Script Date: 10/08/2012 17:42:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_produtotextosistema] as

select p.*, tp.st_tipoproduto, t.id_textosistema,t.st_textosistema, t.st_texto,t.id_textocategoria, tc.st_textocategoria from tb_produto as p
INNER JOIN tb_produtotextosistema as pts on pts.id_produto = p.id_produto and pts.id_entidade = p.id_entidade
INNER JOIN tb_textosistema as t on t.id_textosistema = pts.id_textosistema
INNER JOIN tb_tipoproduto as tp on tp.id_tipoproduto = p.id_tipoproduto
INNER JOIN tb_textocategoria as tc on tc.id_textocategoria = t.id_textocategoria
GO

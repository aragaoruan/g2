/*
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_produtodisciplina]
AS
SELECT DISTINCT
  tp.id_produto,
  tp.id_projetopedagogico,
  td.id_disciplina,
  td.st_tituloexibicao AS st_disciplina,
  case when tm2.nu_cargahoraria is null then td.nu_cargahoraria else tm2.nu_cargahoraria end as nu_cargahoraria  ,
  us.id_usuario AS id_professor,
  us.st_nomecompleto AS st_professor
FROM dbo.tb_produtoprojetopedagogico AS tp
INNER JOIN dbo.tb_projetopedagogico AS tp2
  ON tp.id_projetopedagogico = tp2.id_projetopedagogico
INNER JOIN dbo.tb_modulo AS tm
  ON tp2.id_projetopedagogico = tm.id_projetopedagogico
INNER JOIN dbo.tb_modulodisciplina AS tm2
  ON tm.id_modulo = tm2.id_modulo
  AND tm2.bl_ativo = 1
INNER JOIN dbo.tb_disciplina AS td
  ON td.id_disciplina = tm2.id_disciplina AND id_tipodisciplina != 3
JOIN dbo.tb_disciplinasaladeaula AS ds
  ON ds.id_disciplina = td.id_disciplina
JOIN dbo.tb_areaprojetosala AS aps
  ON aps.id_projetopedagogico = tp.id_projetopedagogico
  AND ds.id_saladeaula = aps.id_saladeaula
LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS uper
  ON uper.id_saladeaula = aps.id_saladeaula
  AND uper.bl_titular = 1 AND uper.bl_ativo = 1
LEFT JOIN tb_perfil AS pf
  ON pf.id_perfil = uper.id_perfil
  AND id_perfilpedagogico = 1
LEFT JOIN dbo.tb_usuario AS us
  ON us.id_usuario = uper.id_usuario

GO*/

CREATE VIEW [dbo].[vw_alunosrenovacao] AS
  WITH _config AS (
      SELECT
        0 AS FALSE,
        1 AS TRUE,
        2 AS DESCONTO_PORCENTAGEM,
        6 AS EVOLUCAO_CURSANDO,
        7 AS EVOLUCAO_AGUARDANDO_NEGOCIACAO,
        9 AS EVOLUCAO_AGUARDANDO_RECEBIMENTO,
        10 AS EVOLUCAO_CONFIRMADA,
        11 AS EVOLUCAO_NAO_ALOCADO,
        17 AS EVOLUCAO_NEGOCIADA,
        19 AS EVOLUCAO_REPROVADO,
        22 AS ITEM_CONFIGURACAO_LINHA_NEGOCIO,
        31 AS ITEM_CONFIGURACAO_PARAMETRO_RENOVACAO,
        53 AS SITUACAO_DISCIPLINA_PENDENTE,
        54 AS SITUACAO_DISCIPLINA_ATIVA,
        -- Esquema de Configuração
        1 AS ESQUEMA_CONF_GRADUACAO,
        CONVERT(DATE, GETDATE()) AS TODAY
  )
  SELECT
    DISTINCT
    mat.id_matricula,
    mat.id_projetopedagogico,
    mat.id_usuario,
    mat.id_periodoletivo,
    mat.id_evolucao,
    mat.id_vendaproduto,
    mat.id_entidadematricula,
    mat.id_turma,
    venda.id_venda,
    venda.dt_ultimaoferta,
    ven_pro.id_produto,
    ven_pro.id_tiposelecao,
    con.id_contrato,
    con.id_contratoregra
  FROM
    tb_matricula AS mat CROSS APPLY _config AS _
    JOIN tb_projetopedagogico AS pro_ped
      ON pro_ped.id_projetopedagogico = mat.id_projetopedagogico
         AND pro_ped.bl_renovar = _.TRUE
    JOIN tb_entidade AS ent
      ON ent.id_entidade = mat.id_entidadematricula
         AND mat.bl_ativo = _.TRUE
         AND mat.id_evolucao = _.EVOLUCAO_CURSANDO
         AND mat.bl_institucional = _.FALSE
    JOIN vw_entidadeesquemaconfiguracao AS esq_conf_ite
      ON esq_conf_ite.id_esquemaconfiguracao =
         ent.id_esquemaconfiguracao
         AND esq_conf_ite.id_itemconfiguracao = _.ITEM_CONFIGURACAO_LINHA_NEGOCIO
         AND esq_conf_ite.st_valor = 2
         AND esq_conf_ite.id_esquemaconfiguracao = _.ESQUEMA_CONF_GRADUACAO
    JOIN tb_contratomatricula AS con_mat
      ON con_mat.id_matricula = mat.id_matricula
         AND con_mat.bl_ativo = _.TRUE
    JOIN tb_contrato AS con
      ON con.id_contrato = con_mat.id_contrato
         AND con.bl_ativo = _.TRUE
    CROSS APPLY
    (
      SELECT
        TOP 1
        _ven.id_venda,
        _ven.dt_ultimaoferta,
        _ven.dt_tentativarenovacao
      FROM
        tb_venda AS _ven
        JOIN tb_vendaproduto AS _ven_pro
          ON _ven.id_venda = _ven_pro.id_venda
             AND _ven_pro.id_matricula = mat.id_matricula
        JOIN tb_contrato AS cont
          ON cont.id_venda = _ven.id_venda
             AND cont.bl_ativo = _.TRUE
-- GII-8558 - Buscar campanha por PORCENTAGEM com valor do desconto 100%
        LEFT JOIN tb_campanhacomercial AS _cc
          ON _cc.id_campanhacomercial = _ven_pro.id_campanhacomercial
             AND _cc.id_tipodesconto = _.DESCONTO_PORCENTAGEM
             AND _cc.nu_valordesconto = 100
      WHERE
        _ven.id_evolucao = _.EVOLUCAO_CONFIRMADA
        AND _ven.bl_ativo = _.TRUE
        -- GII-8558 - Não pode possuir campanha com a regra citada acima
        AND _cc.id_campanhacomercial IS NULL
      ORDER BY
        _ven.id_venda DESC
    ) AS venda
    JOIN tb_vendaproduto AS ven_pro
      ON ven_pro.id_vendaproduto = mat.id_vendaproduto
         AND venda.id_venda IS NOT NULL

         -- Não tenha parcela vencida não paga
         AND
         NOT EXISTS(
             SELECT
               lan.id_lancamento
             FROM
               tb_lancamento AS lan
               INNER JOIN tb_lancamentovenda AS lan_ven
                 ON lan_ven.id_lancamento = lan.id_lancamento
                    AND lan.bl_ativo = _.TRUE
                    AND lan.bl_quitado = _.FALSE
                    AND lan_ven.id_venda = venda.id_venda
             WHERE
               lan.dt_vencimento < _.TODAY
         )

         -- Não pode possuir alguma venda aguardando negociação ou recebimento
         AND
         NOT EXISTS(
             SELECT
               ven.id_venda
             FROM
               tb_venda AS ven
               JOIN tb_vendaproduto AS ven_pro
                 ON ven_pro.id_venda = ven.id_venda
                    AND ven_pro.id_matricula = mat.id_matricula
             WHERE
               ven.id_evolucao IN (
                 _.EVOLUCAO_AGUARDANDO_NEGOCIACAO,
                 _.EVOLUCAO_AGUARDANDO_RECEBIMENTO
               )
               AND ven.bl_ativo = _.TRUE
         )

         -- Não existir inadimplência nas vendas anteriores da matrícula a ser renovada (Parcelas originais e acordadas)
         AND
         NOT EXISTS(
             SELECT
               lc.id_lancamento
             FROM
               tb_venda AS vd
               JOIN tb_vendaproduto AS vdp ON vdp.id_venda = vd.id_venda
               JOIN tb_lancamentovenda AS lc_ven ON lc_ven.id_venda = vd.id_venda
               JOIN tb_lancamento AS lc ON lc.id_lancamento = lc_ven.id_lancamento
             WHERE
               vd.bl_ativo = _.TRUE
               AND lc.bl_ativo = _.TRUE
               AND vd.id_evolucao IN (
                 _.EVOLUCAO_CONFIRMADA,
                 _.EVOLUCAO_NEGOCIADA
               )
               AND lc.bl_quitado = _.FALSE
               AND lc.dt_vencimento < _.TODAY
               AND vdp.id_matricula = mat.id_matricula
               AND vd.id_usuario = mat.id_usuario
         )

         -- A venda deve possuir alguma parcela ativa
         AND EXISTS(
             SELECT
               lan.id_lancamento
             FROM
               tb_lancamento AS lan
               JOIN tb_lancamentovenda AS lan_ven
                 ON lan_ven.id_lancamento =
                    lan.id_lancamento
                    AND lan.bl_ativo = _.TRUE
                    AND lan_ven.id_venda = venda.id_venda
         )
  WHERE
-- GII-6646 / GII-8080 / GII-8757
-- mat.bl_documentacao = _.TRUE
-- O processo de Renovação de Matrícula inicia 10 dias antes da data de término da última oferta na renovação anterior
    _.TODAY >= DATEADD(D, -10, venda.dt_ultimaoferta)

    AND (
      venda.dt_tentativarenovacao IS NULL
      OR venda.dt_tentativarenovacao < _.TODAY
    )

    -- GII-9811 - É necessário possuir disciplinas à cursar
    -- GII-10017 - Ou displinas reprovadas
    AND EXISTS(
        SELECT
          id_matriculadisciplina
        FROM tb_matriculadisciplina
        WHERE
          id_matricula = mat.id_matricula
          AND (
            (
              id_situacao = _.SITUACAO_DISCIPLINA_PENDENTE
              AND id_evolucao = _.EVOLUCAO_NAO_ALOCADO
            )
            OR (
              id_situacao = _.SITUACAO_DISCIPLINA_ATIVA
              AND id_evolucao = _.EVOLUCAO_REPROVADO
            )
          )
    )

CREATE VIEW [dbo].[vw_assuntos] AS
  SELECT
    DISTINCT
    ta2.bl_abertura,
    ta2.bl_autodistribuicao,
    tn2.bl_notificaatendente,
    tn2.bl_notificaresponsavel,
    ta2.id_assuntoco,
    ta2.id_assuntocopai,
    ta.id_entidade,
    ta2.id_situacao,
    ta2.id_textosistema,
    ta2.id_tipoocorrencia,
    ta2.st_assuntoco,
    ta2.bl_ativo,
    ta2.bl_interno
  FROM
    dbo.tb_assuntoentidadeco AS ta
    JOIN dbo.tb_assuntoco AS ta2 ON ta.id_assuntoco = ta2.id_assuntoco
                                    AND ta2.bl_ativo = 1
    JOIN dbo.tb_nucleoassuntoco AS tn ON ta2.id_assuntoco = tn.id_assuntoco
                                         AND tn.bl_ativo = 1
    JOIN dbo.tb_nucleoco AS tn2 ON tn.id_nucleoco = tn2.id_nucleoco
                                   AND ta.id_entidade = tn2.id_entidade
GO


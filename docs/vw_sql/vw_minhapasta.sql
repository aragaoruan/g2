CREATE VIEW vw_minhapasta
AS

SELECT
  mp1.id_minhapasta,
  mp1.id_matricula,
  mp1.id_venda,
  us1.st_nomecompleto,
  mp1.st_contrato,
  mp1.id_usuariocadastro as id_usuario,
  mp1.dt_cadastro as dt_acao,
  ev1.st_evolucao
FROM tb_minhapasta as mp1
JOIN tb_usuario as us1 on us1.id_usuario = mp1.id_usuariocadastro
JOIN tb_evolucao as ev1 on ev1.id_evolucao = 66

UNION

SELECT
  mp2.id_minhapasta,
  mp2.id_matricula,
  mp2.id_venda,
  us2.st_nomecompleto,
  mp2.st_contrato,
  mp2.id_usuarioatualizacao as id_usuario,
  mp2.dt_atualizacao as dt_acao,
  ev2.st_evolucao
  FROM tb_minhapasta as mp2
  JOIN tb_usuario as us2 on us2.id_usuario = mp2.id_usuarioatualizacao
  JOIN tb_evolucao as ev2 on ev2.id_evolucao = 67
where id_usuarioatualizacao is not NULL
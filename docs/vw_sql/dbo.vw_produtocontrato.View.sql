
GO
/****** Object:  View [dbo].[vw_produtocontrato]    Script Date: 10/08/2012 17:42:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_produtocontrato] as

select  p.id_produto, p.id_tipoproduto, p.st_produto, p.bl_ativo, c.id_contrato, c.id_evolucao,

        e.id_entidade, e.st_nomeentidade, s.id_situacao, s.st_situacao, pp.id_projetopedagogico, pp.st_projetopedagogico, pp.bl_turma,

        pv.id_produtovalor, pv.id_tipoprodutovalor,  

        tpv.st_tipoprodutovalor, pv.nu_valor, pv.nu_valormensal, pv.nu_basepropor,

        pp.id_entidadecadastro as id_entidadematriz, vp.id_venda, vp.id_vendaproduto, vp.id_turma

        from

tb_contrato as c

JOIN tb_venda as vd ON vd.id_venda = c.id_venda

JOIN tb_vendaproduto as vp ON vp.id_venda = vd.id_venda

JOIN tb_produto as p on p.id_produto = vp.id_produto

JOIN tb_produtovalor as pv ON pv.id_produto = p.id_produto and pv.dt_inicio <= GETDATE() and (pv.dt_termino >= GETDATE() or pv.dt_termino is null)

JOIN tb_tipoprodutovalor as tpv ON tpv.id_tipoprodutovalor = pv.id_tipoprodutovalor

JOIN tb_entidade as e ON e.id_entidade = p.id_entidade

JOIN tb_situacao as s ON s.id_situacao = p.id_situacao

JOIN tb_produtoprojetopedagogico as ppp ON ppp.id_produto = p.id_produto

JOIN tb_projetopedagogico as pp ON pp.id_projetopedagogico = ppp.id_projetopedagogico

where p.bl_ativo = 1
GO


/****** Object:  View [dbo].[vw_saladeaula]    Script Date: 10/08/2012 17:42:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE VIEW [dbo].[vw_saladeaula] AS
select sa.id_saladeaula ,
        sa.dt_cadastro ,
        sa.st_saladeaula ,
        sa.bl_ativa ,
        sa.id_usuariocadastro ,
        sa.id_modalidadesaladeaula ,
        sa.dt_inicioinscricao ,
        sa.dt_fiminscricao ,
        sa.dt_abertura ,
        sa.st_localizacao ,
        sa.id_tiposaladeaula ,
        sa.dt_encerramento ,
        sa.id_entidade ,
        sa.id_periodoletivo ,
        sa.bl_usardoperiodoletivo ,
        sa.id_situacao ,
        sa.nu_maxalunos ,
        sa.nu_diasencerramento ,
        sa.bl_semencerramento ,
        sa.nu_diasaluno ,
        sa.bl_semdiasaluno , msa.st_modalidadesaladeaula, 
uper.id_perfilreferencia,
 uper.id_usuario, sap.id_saladeaulaprofessor,
sap.bl_titular, sap.nu_maximoalunos, tsa.st_descricao, tsa.st_tiposaladeaula, e.st_nomeentidade, 
pl.st_periodoletivo, pl.dt_inicioinscricao as dt_inicioinscricaoletivo, pl.dt_fiminscricao as dt_fiminscricaoletivo, 
pl.dt_abertura as dt_aberturaletivo, pl.dt_encerramento as dt_encerramentoletivo, s.st_situacao, u.st_nomecompleto as st_nomeprofessor,
d.id_disciplina, d.st_disciplina, d.id_tipodisciplina, pp.id_projetopedagogico, pp.st_projetopedagogico, er.id_entidadepai
,sa.id_categoriasala
,csa.st_categoriasala
from tb_saladeaula as sa
INNER JOIN tb_modalidadesaladeaula as msa ON msa.id_modalidadesaladeaula = sa.id_modalidadesaladeaula
LEFT JOIN tb_usuarioperfilentidadereferencia as uper ON uper.id_saladeaula = sa.id_saladeaula AND uper.bl_ativo =1 AND uper.bl_titular = 1
LEFT JOIN tb_usuario as u ON u.id_usuario = uper.id_usuario
LEFT JOIN tb_saladeaulaprofessor as sap ON sap.id_perfilreferencia = uper.id_perfilreferencia
INNER JOIN tb_tiposaladeaula as tsa ON tsa.id_tiposaladeaula = sa.id_tiposaladeaula
INNER JOIN tb_entidade as e ON e.id_entidade = sa.id_entidade
LEFT JOIN tb_entidaderelacao as er ON er.id_entidade = e.id_entidade AND er.id_entidadepai is null -- Ainda não sei se vai funcionar
LEFT JOIN tb_entidaderelacao as er2 ON er.id_entidadepai = e.id_entidade -- Ainda não sei se vai funcionar
LEFT JOIN tb_disciplinasaladeaula as dsa ON dsa.id_saladeaula = sa.id_saladeaula
LEFT JOIN tb_disciplina as d ON d.id_disciplina = dsa.id_disciplina
LEFT JOIN tb_periodoletivo as pl ON pl.id_periodoletivo = sa.id_periodoletivo and pl.id_entidade = sa.id_entidade
LEFT JOIN tb_areaprojetosala as aps ON aps.id_saladeaula = sa.id_saladeaula 
LEFT JOIN tb_projetopedagogico as pp ON pp.id_projetopedagogico = aps.id_projetopedagogico
INNER JOIN tb_situacao as s ON s.id_situacao = sa.id_situacao
INNER JOIN tb_categoriasala as csa ON csa.id_categoriasala = sa.id_categoriasala
GO

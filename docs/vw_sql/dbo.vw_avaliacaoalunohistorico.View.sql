SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_avaliacaoalunohistorico] AS
SELECT DISTINCT
mt.id_matricula,mdl.id_modulo, mdl.st_tituloexibicao AS st_tituloexibicaomodulo , ad.id_disciplina, dc.st_tituloexibicao AS st_tituloexibicaodisciplina, CAST(REPLACE (aa.st_nota, ',','.') AS numeric (4,2)) AS st_nota
,acr.id_avaliacao,av.st_avaliacao, md.id_situacao,st.st_situacao, ac.id_tipoprova, acrf.id_avaliacaoconjuntoreferencia, md.id_evolucao, ev.st_evolucao, CAST(av.nu_valor AS varchar(100)) AS nu_notamax, ac.id_tipocalculoavaliacao
,av.id_tipoavaliacao, ta.st_tipoavaliacao, acr.id_avaliacaorecupera, pp.nu_notamaxima, pp.nu_percentualaprovacao, aa.bl_ativo, aa.dt_avaliacao, acrf.id_saladeaula, ui.st_codusuario, aa.id_situacao AS id_situacaoavaliacao, sa.st_situacao AS st_situacaoavaliacao, ur.st_nomecompleto AS st_usuariorevisor
FROM tb_matricula AS mt
JOIN tb_matriculadisciplina AS md 
ON md.id_matricula = mt.id_matricula
JOIN tb_modulo AS mdl 
ON mdl.id_projetopedagogico = mt.id_projetopedagogico 
AND mdl.bl_ativo = 1
JOIN tb_modulodisciplina AS mdc 
ON mdc.id_modulo                = mdl.id_modulo 
AND md.id_disciplina        = mdc.id_disciplina 
AND mdc.bl_ativo                = 1
LEFT JOIN tb_alocacao AS alc 
ON alc.id_matriculadisciplina = md.id_matriculadisciplina
JOIN tb_disciplina AS dc 
ON dc.id_disciplina                = mdc.id_disciplina 
AND dc.id_disciplina        = md.id_disciplina
JOIN tb_avaliacaoconjuntoreferencia AS acrf 
ON acrf.dt_inicio <= GETDATE() 
AND (acrf.dt_fim >= GETDATE() OR acrf.dt_fim IS NULL) 
AND ((acrf.id_saladeaula = alc.id_saladeaula) OR (acrf.id_projetopedagogico = mt.id_projetopedagogico))
JOIN tb_avaliacaoconjunto AS ac 
ON ac.id_avaliacaoconjunto = acrf.id_avaliacaoconjunto AND ((acrf.id_saladeaula = alc.id_saladeaula AND ac.id_tipoprova = 2) OR (acrf.id_projetopedagogico = mt.id_projetopedagogico AND ac.id_tipoprova = 1))
JOIN tb_avaliacaoconjuntorelacao AS acr
ON acr.id_avaliacaoconjunto = ac.id_avaliacaoconjunto
JOIN tb_avaliacao AS av 
ON av.id_avaliacao = acr.id_avaliacao
JOIN tb_tipoavaliacao AS ta 
ON ta.id_tipoavaliacao = av.id_tipoavaliacao
JOIN tb_avaliacaodisciplina AS ad 
ON ad.id_disciplina = md.id_disciplina 
AND ad.id_avaliacao = av.id_avaliacao
LEFT JOIN tb_avaliacaoaluno AS aa 
ON aa.id_matricula                                                = mt.id_matricula 
AND aa.id_avaliacaoconjuntoreferencia        = acrf.id_avaliacaoconjuntoreferencia 
AND aa.id_avaliacao                                                = av.id_avaliacao 
AND aa.bl_ativo                                                        = 1
JOIN tb_situacao AS st 
ON st.id_situacao = md.id_situacao
JOIN tb_evolucao AS ev 
ON ev.id_evolucao = md.id_evolucao
LEFT JOIN tb_projetopedagogico AS PP 
ON pp.id_projetopedagogico = mt.id_projetopedagogico
LEFT JOIN dbo.tb_usuariointegracao AS ui ON ui.id_usuario = mt.id_usuario AND (ui.id_entidade = mt.id_entidadeatendimento OR ui.id_entidade = mt.id_entidadematricula)
JOIN dbo.tb_situacao AS sa ON sa.id_situacao = aa.id_situacao
LEFT JOIN dbo.tb_usuario AS ur ON ur.id_usuario = aa.id_usuariorevisor
GO


GO
/****** Object:  View [dbo].[vw_dependenciadisciplinamodulo]    Script Date: 10/08/2012 17:41:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_dependenciadisciplinamodulo] AS
select pp.id_projetopedagogico, td.id_disciplina, d.st_disciplina, td.id_disciplinaprerequisito, dd.st_disciplina as st_disciplinaprerequisito, 
m.id_modulo, m.st_modulo, m.id_moduloanterior, mo.st_modulo as st_moduloanterior,
td.id_trilha, t.id_tipotrilha, t.nu_disciplinassimultaneasmai, t.nu_disciplinassimultaneasmen, 
md.id_nivelensino, ne.st_nivelensino, md.id_serie, s.st_serie, pp.id_entidadecadastro
from tb_projetopedagogico as pp
INNER JOIN tb_modulo as m ON m.id_projetopedagogico = pp.id_projetopedagogico
LEFT JOIN tb_modulo as mo ON mo.id_moduloanterior = m.id_modulo
INNER JOIN tb_modulodisciplina as md ON md.id_modulo = m.id_modulo
INNER JOIN tb_nivelensino as ne ON ne.id_nivelensino = md.id_nivelensino
INNER JOIN tb_serie as s ON s.id_serie = md.id_serie
INNER JOIN tb_disciplinaserienivelensino as dsne ON dsne.id_disciplina = md.id_disciplina and dsne.id_nivelensino = md.id_nivelensino and dsne.id_serie = md.id_serie
INNER JOIN tb_disciplina as d ON d.id_disciplina = md.id_disciplina
INNER JOIN tb_trilha as t ON t.id_trilha = pp.id_trilha
INNER JOIN tb_tipotrilha as tt ON tt.id_tipotrilha = t.id_tipotrilha
INNER JOIN tb_trilhadisciplina as td ON td.id_disciplina = d.id_disciplina
INNER JOIN tb_disciplina as dd ON dd.id_disciplina = td.id_disciplinaprerequisito
LEFT JOIN tb_trilhadisciplina as tds ON tds.id_disciplinaprerequisito = d.id_disciplina AND tds.id_trilha = t.id_trilha
where pp.bl_ativo = 1
GO

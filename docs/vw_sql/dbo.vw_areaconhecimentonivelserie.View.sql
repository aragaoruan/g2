SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_areaconhecimentonivelserie] AS
select distinct ac.id_areaconhecimento, ac.id_entidade, ac.st_areaconhecimento, ac.st_descricao, ac.bl_ativo,
	   ne.st_nivelensino, ne.id_nivelensino, s.id_serie, s.id_serieanterior, s.st_serie, sit.id_situacao, sit.st_situacao, ac.id_areaconhecimentopai
from tb_areaconhecimento ac
LEFT JOIN     tb_areaconhecimentoserienivelensino as acsne ON ac.id_areaconhecimento = acsne.id_areaconhecimento
LEFT JOIN     tb_serienivelensino as sne ON acsne.id_serie = sne.id_serie and acsne.id_nivelensino = sne.id_nivelensino
LEFT JOIN     tb_serie as s ON  sne.id_serie = acsne.id_serie
LEFT JOIN     tb_nivelensino as ne ON  acsne.id_nivelensino = ne.id_nivelensino
JOIN  	 tb_situacao as sit ON ac.id_situacao = sit.id_situacao 
where 
ac.bl_ativo = 1
GO


GO
/****** Object:  View [dbo].[vw_gerarcabecalho]    Script Date: 10/08/2012 17:41:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_gerarcabecalho] as
select
et.id_entidade, et.st_nomeentidade, et.st_razaosocial, et.st_urlimglogo, et.st_urlsite, ct.nu_ddd, ct.nu_telefone, ed.*
from tb_entidade et
LEFT JOIN tb_entidadeendereco as ee ON ee.id_entidade = et.id_entidade and ee.bl_padrao = 1
LEFT JOIN tb_endereco as ed ON ed.id_endereco = ee.id_endereco
LEFT JOIN tb_contatostelefoneentidade as cte ON cte.id_entidade = et.id_entidade and cte.bl_padrao = 1
LEFT JOIN tb_contatostelefone as ct ON ct.id_telefone = cte.id_telefone
GO

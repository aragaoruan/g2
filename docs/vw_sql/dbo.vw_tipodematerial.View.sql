
GO
/****** Object:  View [dbo].[vw_tipodematerial]    Script Date: 10/08/2012 17:42:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_tipodematerial] AS
SELECT tm.id_entidade, tm.id_tipodematerial, tm.st_tipodematerial, s.id_situacao, s.st_situacao
FROM tb_tipodematerial tm, tb_situacao s, tb_entidade e
where s.id_situacao		= tm.id_situacao
AND	  tm.id_entidade	= e.id_entidade
GO

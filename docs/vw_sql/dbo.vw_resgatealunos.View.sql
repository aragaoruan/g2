
/****** Object:  View [dbo].[vw_resgatealunos]    Script Date: 2/9/2013 7:28:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 CREATE VIEW [dbo].[vw_resgatealunos] as
SELECT  DISTINCT mt.id_matricula,
        us.st_nomecompleto,
        mt.id_usuario,
        mt.dt_cadastro,
        us.st_cpf,
        al.id_saladeaula,
        al.id_alocacao,
        sa.st_saladeaula,
        mt.id_projetopedagogico,
        proj.st_projetopedagogico,
        --ac.id_areaconhecimento,
        --ac.st_areaconhecimento,
        mt.id_situacao,
        sit.st_situacao,
        mt.id_evolucao,
        mt.id_turma,
        md.id_disciplina,
        telefone.nu_celular,
		oc.id_ocorrencia,
        ocr.id_usuario as id_usuarioresponsavel,
		oc.id_situacao as id_situacaoocorrencia,
		mt.id_entidadeatendimento as id_entidade,
        CASE WHEN st_nota IS NOT NULL
             THEN CAST(( st_nota / nu_notamax ) * 100 AS INT)
             ELSE NULL
        END AS nu_aproveitamento ,
        
        CASE WHEN lg.dt_cadastro IS NOT NULL
             THEN DATEDIFF(DAY, lg.dt_cadastro, GETDATE())
             ELSE NULL
        END AS nu_diassemacesso,
        (SELECT ocat.st_nomecompleto from tb_usuario ocat WHERE ocr.id_usuario = ocat.id_usuario) as st_usuarioresponsavel
		FROM    
		dbo.tb_matricula AS mt
        JOIN dbo.tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
        JOIN tb_usuario AS us ON us.id_usuario = mt.id_usuario
		JOIN tb_projetopedagogico AS proj ON mt.id_projetopedagogico = proj.id_projetopedagogico
		--JOIN tb_areaprojetopedagogico AS acproj ON proj.id_projetopedagogico = acproj.id_projetopedagogico
		--JOIN tb_areaconhecimento AS ac ON ac.id_areaconhecimento = acproj.id_areaconhecimento
		JOIN tb_pessoa AS p ON us.id_usuario = p.id_usuario
		JOIN tb_situacao AS sit ON sit.id_situacao = mt.id_situacao AND sit.st_tabela = 'tb_matricula'
        JOIN dbo.tb_alocacao AS al ON al.id_matriculadisciplina = md.id_matriculadisciplina
                                      AND al.bl_ativo = 1
        JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula
        JOIN dbo.tb_configuracaoentidade AS ce ON ce.id_entidade = mt.id_entidadeatendimento
        LEFT JOIN dbo.tb_ocorrencia AS oc ON oc.id_matricula = mt.id_matricula  AND oc.id_assuntoco = ce.id_assuntoresgate and oc.id_motivoocorrencia = 1 and oc.id_situacao !=99
        LEFT JOIN tb_ocorrenciaresponsavel as ocr ON ocr.id_ocorrencia = oc.id_ocorrencia and ocr.bl_ativo = 1
		LEFT JOIN dbo.vw_avaliacaoaluno AS av ON av.id_saladeaula = al.id_saladeaula
                                                 AND av.id_matricula = mt.id_matricula AND av.id_matriculadisciplina = md.id_matriculadisciplina
        
        OUTER APPLY ( SELECT TOP 1
                                dt_cadastro
                      FROM      dbo.tb_logacesso
                      WHERE     id_saladeaula = al.id_saladeaula
                                AND id_usuario = mt.id_usuario
                                AND sa.dt_encerramento <= CAST(GETDATE() AS DATE)ORDER BY dt_cadastro DESC
                    ) AS lg
                    
       OUTER APPLY ( SELECT TOP 1
							('(' + ltrim(str(t.nu_ddd)) + ') ' + t.nu_telefone) as nu_celular
							FROM tb_contatostelefone as t
							JOIN tb_contatostelefonepessoa as cpess on cpess.id_usuario = us.id_usuario
							AND t.id_telefone = 3
							) AS telefone            


GO



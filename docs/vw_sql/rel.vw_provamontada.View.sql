
GO

/****** Object:  View [rel].[vw_provamontada]    Script Date: 09/30/2013 10:03:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [rel].[vw_provamontada] as

SELECT  d.bl_provamontada,
		d.id_disciplina,
		d.st_disciplina,
		d.st_tituloexibicao,
		d.id_entidade ,
		case when d.bl_provamontada = 1  then 'Sim' else 'N�o' end as st_provamontada
	
	
	FROM tb_disciplina as d
	WHERE d.bl_ativa = 1

GO



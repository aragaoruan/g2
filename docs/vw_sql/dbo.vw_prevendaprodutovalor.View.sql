
GO
/****** Object:  View [dbo].[vw_prevendaprodutovalor]    Script Date: 10/08/2012 17:42:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vw_prevendaprodutovalor] as
select
ppv.id_prevendaprodutovalor, ppv.id_prevenda, pd.id_produto ,pd.st_produto,tp.st_tipoproduto,pdv.dt_inicio, pdv.dt_termino, pdv.id_produtovalor, pdv.nu_valor, pdv.nu_valormensal
from tb_prevendaprodutovalor as ppv
JOIN tb_produtovalor as pdv ON pdv.id_produtovalor = ppv.id_produtovalor
JOIN tb_produto as pd ON pd.id_produto = pdv.id_produto
JOIN tb_tipoproduto as tp ON tp.id_tipoproduto = pd.id_tipoproduto
GO

CREATE VIEW [dbo].[vw_matricula]
  AS
    WITH _config AS (
        SELECT
          0 AS FALSE,
          1 AS TRUE,
          10 AS VENDA_CONFIRMADA
    )
    SELECT
      mt.id_matricula,
      mt.id_usuario,
      us.st_nomecompleto,
      us.st_cpf,
      ps.st_urlavatar,
      ps.dt_nascimento,
      ps.st_nomepai,
      ps.st_nomemae,
      ps.st_email,
      CAST(ps.nu_ddd AS VARCHAR) + '-' + CAST(ps.nu_telefone AS VARCHAR) AS st_telefone,
      CAST(ps.nu_dddalternativo AS VARCHAR) + '-'
      + CAST(ps.nu_telefonealternativo AS VARCHAR) AS st_telefonealternativo,
      mt.id_matriculaorigem,
      pp.st_projetopedagogico,
      mt.id_situacao,
      st.st_situacao,
      mt.id_evolucao,
      ev.st_evolucao,
      mt.dt_concluinte,
      mtco.dt_colacao,
      mt.id_entidadematriz,
      etmz.st_nomeentidade AS st_entidadematriz,
      mt.id_entidadematricula,
      etm.st_nomeentidade AS st_entidadematricula,
      etm.st_urlportal AS st_urlportalmatricula,
      mt.id_entidadeatendimento,
      eta.st_nomeentidade AS st_entidadeatendimento,
      mt.bl_ativo,
      cm.id_contrato,
      pp.id_projetopedagogico,
      mt.dt_termino,
      mt.dt_cadastro,
      mt.dt_inicio,
      mt.nu_semestreatual,
      tm.id_turma,
      mt.id_turmaorigem,
      tm.st_turma,
      tm.st_codigo,
      tm.dt_inicio AS dt_inicioturma,
      mt.bl_institucional,
      tm.dt_fim AS dt_terminoturma,
      mt.id_situacaoagendamento,
        id_vendaproduto = CASE WHEN mt.id_vendaproduto IS NULL
        THEN vprod.id_vendaproduto
                          ELSE mt.id_vendaproduto
                          END,
      mt.dt_termino AS dt_terminomatricula,
      CAST(RIGHT(master.dbo.fn_varbintohexstr(HASHBYTES('MD5',
                                                        (CAST(us.id_usuario AS VARCHAR(10))
                                                         COLLATE Latin1_General_CI_AI
                                                         + CAST((CASE
                                                                 WHEN mt.id_matricula IS NULL
                                                                   THEN 0
                                                                 ELSE mt.id_matricula
                                                                 END) AS VARCHAR(15))
                                                         COLLATE Latin1_General_CI_AI
                                                         + CAST(5 AS VARCHAR(15))
                                                         COLLATE Latin1_General_CI_AI
                                                         + CAST((0) AS VARCHAR(15))
                                                         COLLATE Latin1_General_CI_AI
                                                         + us.st_login
                                                         + us.st_senha
                                                         + CONVERT(VARCHAR(10), GETDATE(), 103)))),
                 32) AS CHAR(32))
      COLLATE Latin1_General_CI_AI AS st_urlacesso,
      mt.bl_documentacao,
        st_statusdocumentacao = CASE
                                WHEN mt.bl_documentacao = _.TRUE
                                  THEN 'OK'
                                WHEN mt.bl_documentacao = _.FALSE
                                  THEN 'Pendente'
                                ELSE 'Não Analisado'
                                END,
      COALESCE(CASE
               WHEN mt.id_matriculaorigem IS NULL
                 THEN vprod.id_venda
               ELSE vad.id_venda
               END, v.id_venda) AS id_venda,
      eta.st_urlportal,
      eta.st_urlnovoportal,
      mt.st_codcertificacao,
      ps.st_identificacao,
      CONVERT(VARCHAR, mtc.dt_cadastro, 103) AS st_dtcadastrocertificacao,
      CONVERT(VARCHAR, mtc.dt_enviocertificado, 103) AS st_dtenviocertificado,
      CONVERT(VARCHAR, mtc.dt_retornocertificadora, 103) AS st_dtretornocertificadora,
      CONVERT(VARCHAR, mtc.dt_envioaluno, 103) AS st_dtenvioaluno,
      st_codigoacompanhamento,
      mtc.id_indicecertificado,
      mt.id_evolucaocertificacao,
      ap.id_areaconhecimento,
      pp.bl_disciplinacomplementar,
      mt.id_matriculavinculada,
      cr.id_contratoregra,
      trc.id_tiporegracontrato,
      etm.st_siglaentidade,
      mt.bl_academico,
      mtc.dt_cadastro AS dt_cadastrocertificacao,
      mtc.dt_enviocertificado,
      mtc.dt_retornocertificadora,
      mtc.dt_envioaluno,
      vup.st_upload AS st_uploadcontrato,
      canc.id_cancelamento,
      mt.id_aproveitamento,
      vprod.id_produto,
      pp.st_descricao,
      pp.st_imagem,
      mt.id_situacaoagendamentoparcial,
      pp.bl_novoportal,
      YEAR(mt.dt_inicio) AS nu_anomatricula,
      etapa.dt_aceita AS dt_aceitecontrato,
      etmz.id_esquemaconfiguracao,
      vad.id_vendaaditivo,
      pp.st_tituloexibicao,
      mt.dt_ultimaoferta,
      mt.dt_ultimoacesso
    FROM tb_matricula AS mt
      CROSS APPLY _config AS _
      JOIN tb_projetopedagogico AS pp
        ON mt.id_projetopedagogico = pp.id_projetopedagogico
      JOIN tb_areaprojetopedagogico AS ap
        ON ap.id_projetopedagogico = pp.id_projetopedagogico
      JOIN tb_entidade AS etmz
        ON etmz.id_entidade = mt.id_entidadematriz
      JOIN tb_entidade AS etm
        ON etm.id_entidade = mt.id_entidadematricula
      JOIN tb_entidade AS eta
        ON eta.id_entidade = mt.id_entidadeatendimento
      JOIN tb_situacao AS st
        ON st.id_situacao = mt.id_situacao
      JOIN tb_evolucao AS ev
        ON ev.id_evolucao = mt.id_evolucao
      JOIN tb_usuario AS us
        ON us.id_usuario = mt.id_usuario
      LEFT JOIN tb_contratomatricula AS cm
        ON cm.id_matricula = mt.id_matricula
           AND cm.bl_ativo = _.TRUE
      LEFT JOIN vw_pessoa AS ps
        ON mt.id_usuario = ps.id_usuario
           AND mt.id_entidadeatendimento = ps.id_entidade
      LEFT JOIN dbo.tb_contrato AS ct
        ON ct.id_contrato = cm.id_contrato
           AND ct.bl_ativo = _.TRUE
      LEFT JOIN tb_turma AS tm
        ON tm.id_turma = mt.id_turma
      LEFT JOIN tb_matriculacertificacao mtc
        ON mtc.id_matricula = mt.id_matricula
           AND mtc.bl_ativo = _.TRUE
      LEFT JOIN dbo.tb_contratoregra AS cr
        ON cr.id_contratoregra = ct.id_contratoregra
      LEFT JOIN dbo.tb_tiporegracontrato AS trc
        ON trc.id_tiporegracontrato = cr.id_tiporegracontrato
      LEFT JOIN dbo.tb_venda AS v
        ON ct.id_venda = v.id_venda
      LEFT JOIN dbo.tb_upload AS vup
        ON vup.id_upload = v.id_uploadcontrato
      LEFT JOIN dbo.tb_cancelamento AS canc
        ON canc.id_cancelamento = mt.id_cancelamento
-- Status Colação
      LEFT JOIN dbo.tb_matriculacolacao AS mtco
        ON mtco.id_matricula = mt.id_matricula
           AND mtco.bl_ativo = _.TRUE
           AND mtco.bl_presenca = _.TRUE
      OUTER APPLY (SELECT
                     TOP 1
                     tb_vendaproduto.*
                   FROM dbo.tb_vendaproduto
                     JOIN tb_produtoprojetopedagogico AS ppd ON ppd.id_produto = tb_vendaproduto.id_produto
                     JOIN tb_venda AS vend ON vend.id_venda = tb_vendaproduto.id_venda AND
                                              vend.id_evolucao = _.VENDA_CONFIRMADA
                   WHERE tb_vendaproduto.id_matricula = mt.id_matricula
                         AND tb_vendaproduto.bl_ativo = _.TRUE
                   ORDER BY
                     tb_vendaproduto.id_vendaproduto DESC) AS vprod
      LEFT JOIN tb_vendaaditivo AS vad ON vad.id_matricula = mt.id_matricula
      LEFT JOIN tb_pa_marcacaoetapa AS etapa ON etapa.id_venda = COALESCE(CASE
                                                                            WHEN mt.id_matriculaorigem IS NULL
                                                                            THEN vprod.id_venda
                                                                            ELSE vad.id_venda
                                                                            END, v.id_venda)
    WHERE mt.bl_ativo = _.TRUE
GO

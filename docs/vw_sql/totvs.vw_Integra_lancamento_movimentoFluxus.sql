ALTER VIEW totvs.vw_Integra_lancamento_movimentoFluxus AS

SELECT 
		l.bl_quitado AS statusbaixa,
		CASE WHEN l.id_codcoligada IS NOT NULL THEN l.id_codcoligada ELSE ei.st_codsistema END AS codcoligada,
		li.st_codlancamento AS idlan,
		vi.st_codvenda,
		v.id_venda,
		l.id_lancamento
	FROM tb_lancamentointegracao li
	JOIN tb_lancamento l ON li.id_lancamento = l.id_lancamento
	JOIN tb_lancamentovenda lv ON l.id_lancamento = lv.id_lancamento
	JOIN tb_venda v ON v.id_venda = lv.id_venda
	JOIN tb_vendaintegracao vi ON vi.id_venda = lv.id_venda and vi.id_sistema = 29
	JOIN tb_entidadeintegracao ei ON ei.id_entidade = v.id_entidade and ei.id_sistema = 29

	WHERE li.st_codvenda IS NULL
GO
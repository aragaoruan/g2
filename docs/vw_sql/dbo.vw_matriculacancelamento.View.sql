CREATE VIEW [dbo].[vw_matriculacancelamento] AS
  SELECT DISTINCT
    ds.sala,
    ds.OCORRENCIA,
    vw.id_matricula,
    vw.id_projetopedagogico,
    ds.st_tituloexibicao,
    ds.id_evolucao,
    ds.st_evolucao,
    ds.dt_aceitecontrato,
    ds.id_disciplina,
    ds.st_disciplina,
    ds.dt_abertura,
    ds.dt_encerramento,
    ds.nu_cargahoraria,
    ds.bl_cancelamento,
    ds.id_venda,
    ds.dt_cancelamento,
    ds.dt_matricula,
    ds.nu_cargahorariaprojeto,
    ds.nu_valortotalcurso
  FROM
    vw_gradenota AS vw

    OUTER APPLY (
                  SELECT DISTINCT
                    di.id_disciplina,
                    di.st_disciplina,
                    sa.dt_abertura,
                    sa.dt_encerramento,
                    di.nu_cargahoraria,
                    pp.st_tituloexibicao,
                    vwma.dt_aceitecontrato,
                    vwma.id_evolucao,
                    vwma.st_evolucao,
                    ma.bl_cancelamento,
                    vwma.dt_cadastro,
                    vdp.id_venda,
                    mt.dt_cadastro as dt_matricula,
                    oc.dt_cadastroocorrencia as dt_cancelamento,
                    oc.id_ocorrencia as OCORRENCIA,
                    pp.nu_cargahoraria as nu_cargahorariaprojeto,
                    (ISNULL(va.nu_valorapagar, 0) + vdp.nu_valorliquido) AS nu_valortotalcurso,
                    sa.id_saladeaula as sala
                  FROM
                    tb_alocacao AS al
                    JOIN tb_saladeaula AS sa ON al.id_saladeaula = sa.id_saladeaula AND sa.dt_abertura < GETDATE()
                    JOIN tb_matriculadisciplina AS ma ON al.id_matriculadisciplina = ma.id_matriculadisciplina
                    JOIN tb_matricula AS mt ON ma.id_matricula = mt.id_matricula
                    JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
                    JOIN tb_disciplina AS di ON vw.id_disciplina = di.id_disciplina AND di.id_tipodisciplina != 3
                    JOIN vw_matricula AS vwma ON vwma.id_matricula = ma.id_matricula
                    JOIN tb_vendaproduto AS vdp ON vdp.id_venda = vwma.id_venda AND vdp.id_vendaproduto = vwma.id_vendaproduto
                    LEFT JOIN tb_vendaaditivo AS va ON va.id_venda = vwma.id_venda AND vwma.id_matricula = va.id_matricula AND
                                                       va.bl_cancelamento = 0
                    JOIN tb_cancelamento as ca ON vwma.id_cancelamento = ca.id_cancelamento
                    JOIN tb_ocorrencia AS oc ON ca.id_ocorrencia = oc.id_ocorrencia
                  WHERE
                    id_alocacao = vw.id_alocacao
                ) AS ds
GO
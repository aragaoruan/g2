CREATE VIEW [dbo].[vw_censo] AS
  SELECT
-- Matrícula
    mt.id_matricula,
-- Ano matrícula
    YEAR(mt.dt_inicio) AS nu_anomatricula,
-- Ano turma
    YEAR(tm.dt_inicio) AS nu_anocenso,
-- Entidade
    mt.id_entidadematricula AS id_entidade,
-- Tipo de registro (Obrigatório | Fixo: 2)
    40 AS tp_registrocabecalho,
-- ID da IES no INEP
    ci.nu_codigoinstituicao AS id_iesinep,
-- Tipo de arquivo (Obrigatório | Fixo: 1)
    4 AS tp_arquivocabecalho,
-- Tipo de registro (Aluno) (Obrigatório | Fixo: 2)
    41 AS tp_registroaluno,
-- ID do aluno no INEP (Fixo: 12)
    mt.id_matricula AS id_alunoinep,
-- Nome (Obrigatório)
    UPPER(us.st_nomecompleto) AS st_nome,
-- CPF (Obrigatório | Fixo: 11)
    us.st_cpf,
-- Documento de estrangeiro ou passaporte
    pe.st_passaporte AS st_docuestrangeiropassaporte,
-- Data de Nascimento (Obrigatório | Fixo: 11)
    REPLACE(CONVERT(VARCHAR(10), pe.dt_nascimento, 103), '/', '') AS dt_nascimento,
-- Sexo (Obrigatório | Fixo: 1)
    (CASE pe.st_sexo
     WHEN 'M'
       THEN 0
     WHEN 'F'
       THEN 1
     END) AS nu_sexo,
-- Cor/Raça (Obrigatório | Fixo: 1)
    (CASE
     WHEN pdc.id_etnia IS NULL OR pdc.id_etnia = 8 -- G2: Não dispõe da informação
       THEN 6 -- CENSO: Não dispõe da informação
     WHEN pdc.id_etnia = 6 -- G2: Aluno não quis declarar cor/raça
       THEN 0 -- CENSO: Aluno não quis declarar
     WHEN pdc.id_etnia = 3 -- G2: Amarela
       THEN 4 -- CENSO: Amarela
     WHEN pdc.id_etnia = 4 -- G2: Parda
       THEN 3 -- CENSO: Parda
     ELSE pdc.id_etnia
     END) AS nu_corraca,
-- Nome completo da mãe (Obrigatório)
    UPPER(pe.st_nomemae) AS st_nomecompletomae,
-- Nacionalidade (Obrigatório | Fixo: 1)
    (CASE pais.st_nomepais
     WHEN 'Brasil'
       THEN 1 -- Brasileira
     ELSE 3 -- Estrangeira
     END) AS nu_nacionalidade,
-- UF de nascimento (Fixo: 2)
    (CASE
     WHEN pais.st_nomepais != 'Brasil'
       THEN NULL
     WHEN ufnc.nu_codigoibge IS NOT NULL AND munc.nu_codigoibge IS NOT NULL
       THEN ufnc.nu_codigoibge
     ELSE NULL
     END) AS nu_ufnascimento,
-- Município de nascimento (Fixo: 7)
    (CASE
     WHEN pais.st_nomepais != 'Brasil'
       THEN NULL
     WHEN ufnc.nu_codigoibge IS NOT NULL AND munc.nu_codigoibge IS NOT NULL
       THEN munc.nu_codigoibge
     ELSE NULL
     END) AS nu_municipionascimento,
-- País de origem (Obrigatório | Fixo: 3)
    (CASE pais.st_nomepais
     WHEN 'Brasil'
       THEN 'BRA'
     ELSE '0'
     END) AS st_paisorigem,
-- Aluno com deficiência, transtorno global do desenvolvimento ou altas habilidades/superdotação (Obrigatório | Fixo: 1)
    (CASE
     WHEN ISNULL(pdc.id_pessoacomdeficiencia, 2) = 2
       THEN 0
     ELSE 1
     END) AS nu_alunodeftranstsuperdotacao,
-- Tipo de deficiência - Cegueira (Fixo: 1)
    (CASE
     WHEN ISNULL(pdc.id_pessoacomdeficiencia, 2) = 2
       THEN NULL
     WHEN udf1.id_usuariodeficiencia IS NOT NULL
       THEN 1
     ELSE 0
     END) AS nu_tipodeficienciacegueira,
-- Tipo de deficiência - Baixa visão (Fixo: 1)
    (CASE
     WHEN ISNULL(pdc.id_pessoacomdeficiencia, 2) = 2
       THEN NULL
     WHEN udf2.id_usuariodeficiencia IS NOT NULL
       THEN 1
     ELSE 0
     END) AS nu_tipodeficienciabaixavisao,
-- Tipo de deficiência - Surdez (Fixo: 1)
    (CASE
     WHEN ISNULL(pdc.id_pessoacomdeficiencia, 2) = 2
       THEN NULL
     WHEN udf3.id_usuariodeficiencia IS NOT NULL
       THEN 1
     ELSE 0
     END) AS nu_tipodeficienciasurdez,
-- Tipo de deficiência - auditiva (Fixo: 1)
    (CASE
     WHEN ISNULL(pdc.id_pessoacomdeficiencia, 2) = 2
       THEN NULL
     WHEN udf4.id_usuariodeficiencia IS NOT NULL
       THEN 1
     ELSE 0
     END) AS nu_tipodeficienciaauditiva,
-- Tipo de deficiência - física (Fixo: 1)
    (CASE
     WHEN ISNULL(pdc.id_pessoacomdeficiencia, 2) = 2
       THEN NULL
     WHEN udf5.id_usuariodeficiencia IS NOT NULL
       THEN 1
     ELSE 0
     END) AS nu_tipodeficienciafisica,
-- Tipo de deficiência - Surdocegueira (Fixo: 1)
    (CASE
     WHEN ISNULL(pdc.id_pessoacomdeficiencia, 2) = 2
       THEN NULL
     WHEN udf6.id_usuariodeficiencia IS NOT NULL
       THEN 1
     ELSE 0
     END) AS nu_tipodeficienciasurdocegueira,
-- Tipo de deficiência - Múltipla (Fixo: 1)
    (CASE
     WHEN ISNULL(pdc.id_pessoacomdeficiencia, 2) = 2
       THEN NULL
     WHEN udf7.id_usuariodeficiencia IS NOT NULL
       THEN 1
     ELSE 0
     END) AS nu_tipodeficienciamultipla,
-- Tipo de deficiência - Intelectual (Fixo: 1)
    (CASE
     WHEN ISNULL(pdc.id_pessoacomdeficiencia, 2) = 2
       THEN NULL
     WHEN udf8.id_usuariodeficiencia IS NOT NULL
       THEN 1
     ELSE 0
     END) AS nu_tipodeficienciaintelectcual,
-- Tipo de deficiência - Autismo (Transtorno global do desenvolvimento) (Fixo: 1)
    (CASE
     WHEN ISNULL(pdc.id_pessoacomdeficiencia, 2) = 2
       THEN NULL
     WHEN udf9.id_usuariodeficiencia IS NOT NULL
       THEN 1
     ELSE 0
     END) AS nu_tipodeficienciaautismo,
-- Tipo de deficiência - Síndrome de Asperger (Transtorno global do desenvolvimento) (Fixo: 1)
    (CASE
     WHEN ISNULL(pdc.id_pessoacomdeficiencia, 2) = 2
       THEN NULL
     WHEN udf10.id_usuariodeficiencia IS NOT NULL
       THEN 1
     ELSE 0
     END) AS nu_tipodeficienciaasperger,
-- Tipo de deficiência - Síndrome de RETT (Transtorno global do desenvolvimento) (Fixo: 1)
    (CASE
     WHEN ISNULL(pdc.id_pessoacomdeficiencia, 2) = 2
       THEN NULL
     WHEN udf11.id_usuariodeficiencia IS NOT NULL
       THEN 1
     ELSE 0
     END) AS nu_tipodeficienciarett,
-- Tipo de deficiência - Transtorno desintegrativo da infância (Transtorno global do desenvolvimento) (Fixo: 1)
    (CASE
     WHEN ISNULL(pdc.id_pessoacomdeficiencia, 2) = 2
       THEN NULL
     WHEN udf12.id_usuariodeficiencia IS NOT NULL
       THEN 1
     ELSE 0
     END) AS nu_tipodeficienciatranstdesinfancia,
-- Tipo de deficiência - Altas habilidades/ superdotação (Fixo: 1)
    (CASE
     WHEN ISNULL(pdc.id_pessoacomdeficiencia, 2) = 2
       THEN NULL
     WHEN udf13.id_usuariodeficiencia IS NOT NULL
       THEN 1
     ELSE 0
     END) AS nu_tipodeficienciasuperdotacao,
-- Tipo de registro (Curso) (Obrigatório | Fixo: 2)
    42 AS tp_registroalunocurso,
-- Semestre de referência (Fixo: 1)
    NULL nu_semestrereferencia,
-- Código do Curso (Obrigatório)
    cc.nu_codigocurso,
-- Código do pólo do curso a distância
    cp.nu_codigopolo AS nu_codigopolocursodistancia,
-- ID na IES - Identificação única do aluno na IES
    mt.id_matricula AS st_idnaies,
-- Turno do aluno (Fixo: 1)
    NULL AS nu_turnoaluno,
-- Situação de vínculo do aluno ao curso (Obrigatório | Fixo: 1)
    (CASE mt.id_evolucao
     WHEN 6 -- G2: Cursando
       THEN 2 -- CENSO: Cursando
     WHEN 21 -- G2: Trancado
       THEN 3 -- CENSO: Trancado
     WHEN 27 -- G2: Cancelado
       THEN 4 -- CENSO: Desvinculado do curso
     WHEN 40 -- G2: Bloqueado
       THEN 4 -- CENSO: Desvinculado do curso
     WHEN 20 -- G2: Transferido
       THEN 5 -- CENSO: Transferido
     WHEN 81 -- G2: Formado
       THEN 6 -- CENSO: Formado
     WHEN 15 -- G2: Concluinte
       THEN 6 -- CENSO: Formado
     END) AS nu_situacaovinculoalunocurso,
-- Curso origem
    NULL AS nu_cursoorigem,
-- Semestre de conclusão do curso (Fixo: 1)
    SUBSTRING(conclusao.st_semestreconclusao, 2, 1) AS nu_semestreconclusao,
-- Aluno PARFOR (Fixo: 1)
    (CASE pp.id_grauacademico
     WHEN 3 -- G2: Licenciatura
       THEN 0
     END) AS nu_alunoparfor,
-- Semestre de ingresso no curso (Obrigatório | Fixo: 6)
    (CASE
     WHEN (MONTH(tm.dt_inicio) BETWEEN 7 AND 12)
       THEN CONCAT('02', YEAR(tm.dt_inicio))
     ELSE CONCAT('01', YEAR(tm.dt_inicio))
     END) AS nu_semestreingcurso,
-- Tipo de escola que concluiu o Ensino Médio (Obrigatório | Fixo: 1)
    (CASE
     WHEN pdc.id_tipoescola IS NULL
       THEN CASE
            WHEN YEAR(vp.dt_ingresso) < 2013 OR YEAR(tm.dt_inicio) < 2013 OR pais.st_nomepais != 'Brasil'
              THEN 2
            END
     WHEN (pdc.id_tipoescola = 2)
       THEN 0
     ELSE 1
     END) AS nu_tipoescolaconclusaoensinomedio,
-- Forma de ingresso/seleção - Vestibular (Obrigatório | Fixo: 1)
    (CASE
     WHEN vp.id_tiposelecao = 1
       THEN 1
     ELSE 0
     END) AS nu_formaingselecaovestibular,
-- Forma de ingresso/seleção - Enem (Obrigatório | Fixo: 1)
    (CASE
     WHEN vp.id_tiposelecao = 3
       THEN 1
     ELSE 0
     END) AS nu_formaingselecaoenem,
-- Forma de ingresso/seleção - Avaliação Seriada (Obrigatório | Fixo: 1)
    0 AS nu_formaingselecaoavaliacaoseriada,
-- Forma de ingresso/seleção - Seleção Simplificada (Obrigatório | Fixo: 1)
    (CASE
     WHEN vp.id_tiposelecao = 1 OR vp.id_tiposelecao = 7
       THEN 1
     ELSE 0
     END) AS nu_formaingselecaoselecaosimplificada,
-- Forma de ingresso/seleção - Egresso BI/LI (Obrigatório | Fixo: 1)
    0 AS nu_formaingselecaoegressobili,
-- Forma de ingresso/seleção - PEC-G (Fixo: 1)
    0 AS nu_formaingselecaoegressopecg,
-- Forma de ingresso/seleção - Transferência Ex Officio (Obrigatório | Fixo: 1)
    0 AS nu_formaingselecaotrasnfexofficio,
-- Forma de ingresso/seleção - Decisão judicial (Obrigatório | Fixo: 1)
    0 AS nu_formaingselecaodecisaojudicial,
-- Forma de ingresso - Seleção para Vagas Remanescentes (Obrigatório | Fixo: 1)
    (CASE
     WHEN (vp.id_tiposelecao = 2)
       THEN 1
     ELSE 0
     END) AS nu_formaingselecaovagasremanescentes,
-- Forma de ingresso - Seleção para Vagas de Programas Especiais (Obrigatório | Fixo: 1)
    0 AS nu_formaingselecaovagasprogespeciais,
-- Mobilidade acadêmica (Obrigatório se não FEDERAL | Fixo: 1)
    0 AS nu_mobilidadeacademica,
-- Tipo de mobilidade acadêmica (Fixo: 1)
    NULL AS nu_tipomodalidadeacademica,
-- IES destino
    NULL st_iesdestino,
-- Tipo de mobilidade acadêmica internacional (Fixo: 1)
    NULL AS nu_tipomodalidadeacademicainternacional,
-- País destino (Fixo: 3)
    NULL AS st_paisdestino,
-- Programa de reserva de vagas (Obrigatório | Fixo: 1)
    0 AS nu_programareservavagas,
-- Programa de reserva de vagas/açoes afirmativas - Etnico (Fixo: 1)
    NULL AS nu_programareservavagasetnico,
-- Programa de reserva de vagas/ações afirmativas - Pessoa com deficiência (Fixo: 1)
    NULL AS nu_programareservavagaspessoadeficiencia,
-- Programa de reserva de vagas - Estudante procedente de escola pública (Fixo: 1)
    NULL AS nu_programareservavagasestudanteescolapublica,
-- Programa de reserva de vagas/ações afirmativas - Social/renda familiar (Fixo: 1)
    NULL AS nu_programareservavagassocialrendafamiliar,
-- Programa de reserva de vagas/ações afirmativas - Outros (Fixo: 1)
    NULL AS nu_programareservavagasoutros,
-- Financiamento estudantil (Obrigatório se não FEDERAL | Fixo: 1)
    (CASE
     WHEN vp.id_tiposelecao = 6
       THEN 1
     ELSE 0
     END) AS nu_finestud,
-- Financiamento Estudantil Reembolsável - FIES (Fixo: 1)
    NULL AS nu_finestudreembfies,
-- Financiamento Estudantil Reembolsável - Governo Estadual (Fixo: 1)
    NULL AS nu_finestudreembgovest,
-- Financiamento Estudantil Reembolsável -Governo Municipal (Fixo: 1)
    NULL AS nu_finestudreembgovmun,
-- Financiamento Estudantil Reembolsável - IES (Fixo: 1)
    NULL AS nu_finestudreembies,
-- Financiamento Estudantil Reembolsável - Entidades externas (Fixo: 1)
    NULL AS nu_finestudreembentidadesexternas,
-- Tipo de financiamento não reembolsável - ProUni integral (Fixo: 1)
    (CASE
     WHEN vp.id_tiposelecao = 6
       THEN 1
     END) AS nu_finestudreembprouniintegral,
-- Tipo de financiamento não reembolsável - ProUni parcial (Fixo: 1)
    NULL AS nu_finestudreembprouniparcial,
-- Tipo de financiamento não reembolsável - Entidades externas (Fixo: 1)
    NULL AS nu_finestudreembentidadesexternas_2,
-- Tipo de financiamento não reembolsável - Governo estadual (Fixo: 1)
    NULL AS nu_finestudreembgovest_2,
-- Tipo de financiamento não reembolsável - IES (Fixo: 1)
    NULL AS nu_finestudreembies_2,
-- Tipo de financiamento não reembolsável - Governo municipal (Fixo: 1)
    NULL AS nu_finestudreembgovmun_2,
-- Apoio Social (Obrigatório | Fixo: 1)
    0 AS nu_apoiosocial,
-- Tipo de apoio social - Alimentação (Fixo: 1)
    NULL AS nu_tipoapoiosocialalimentacao,
-- Tipo de apoio social - Moradia (Fixo: 1)
    NULL AS nu_tipoapoiosocialmoradia,
-- Tipo de apoio social - Transporte (Fixo: 1)
    NULL AS nu_tipoapoiosocialtransporte,
-- Tipo de apoio social - Material didático (Fixo: 1)
    NULL AS nu_tipoapoiosocialmaterialdidatico,
-- Tipo de apoio social - Bolsa trabalho (Fixo: 1)
    NULL AS nu_tipoapoiosocialbolsatrabalho,
-- Tipo de apoio social - Bolsa permanência (Fixo: 1)
    NULL AS nu_tipoapoiosocialbolsapermanencia,
-- Atividade extracurricular (Obrigatório | Fixo: 1)
    1 AS nu_atvextcurr,
-- Atividade extracurricular - Pesquisa (Fixo: 1)
    1 AS nu_atvextcurrpesquisa,
-- Bolsa/remuneração referente à atividade extracurricular - Pesquisa (Fixo: 1)
    0 AS nu_bolsaremuneracaoatvextcurrpesquisa,
-- Atividade extracurricular - Extensão (Fixo: 1)
    1 AS nu_atvextcurrextensao,
-- Bolsa/remuneração referente à atividade extracurricular - Extensão (Fixo: 1)
    0 AS nu_bolsaremuneracaoatvextcurrextensao,
-- Atividade extracurricular - Monitoria (Fixo: 1)
    0 AS nu_atvextcurrmonitoria,
-- Bolsa/remuneração referente à atividade extracurricular - Monitoria (Fixo: 1)
    NULL AS nu_bolsaremuneracaoatvextcurrmonitoria,
-- Atividade extracurricular - Estágio não obrigatório (Fixo: 1)
    0 AS nu_atvextcurrestnaoobg,
-- Bolsa/remuneração referente à atividade extracurricular - Estágio não obrigatório (Fixo: 1)
    NULL AS nu_bolsaremuneracaoatvextcurrestnaoobg,
-- Carga horária total do curso por aluno (Obrigatório)
    pp.nu_cargahoraria AS nu_cargahorariototalporaluno,
-- Carga horária integralizada pelo aluno (Obrigatório)
    (CASE
     WHEN mt.id_evolucao = 12
       THEN pp.nu_cargahoraria
     ELSE
       (SELECT
          ISNULL(SUM(dc.nu_cargahoraria), 0)
        FROM
          tb_matriculadisciplina AS md
          JOIN tb_disciplina AS dc ON md.id_disciplina = dc.id_disciplina AND dc.bl_cargahorariaintegralizada = 1
        WHERE
          dc.id_tipodisciplina = 1
          AND md.id_matricula = mt.id_matricula
          AND md.id_evolucao = 12
       )
     END) AS nu_cargahorariointegralizadapeloaluno,
    pdc.dt_anoconclusao AS st_anoconclusaoensinomedio,
    tm.dt_inicio AS dt_inicioturma,
    conclusao.st_semestreconclusao
  FROM
    tb_matricula AS mt
    JOIN tb_usuario AS us ON us.id_usuario = mt.id_usuario
    JOIN tb_pessoa AS pe ON pe.id_usuario = mt.id_usuario AND pe.id_entidade = mt.id_entidadematricula
    JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
    JOIN tb_turma AS tm ON tm.id_turma = mt.id_turma
    LEFT JOIN tb_pessoadadoscomplementares AS pdc ON pe.id_usuario = pdc.id_usuario
-- DADOS CENSO
    JOIN tb_censopolo AS cp ON cp.id_entidade = mt.id_entidadematricula
    JOIN tb_censoinstituicao AS ci ON ci.id_censoinstituicao = cp.id_censoinstituicao
    JOIN tb_censocurso AS cc ON cc.id_projetopedagogico = mt.id_projetopedagogico
-- NASCIMENTO
    JOIN tb_pais AS pais ON pe.id_pais = pais.id_pais
    LEFT JOIN tb_uf AS ufnc ON ufnc.sg_uf = pe.sg_ufnascimento
    LEFT JOIN tb_municipio AS munc ON munc.id_municipio = pe.id_municipionascimento
-- GRADUAÇÃO
    JOIN vw_entidadeesquemaconfiguracao AS eec
      ON eec.id_itemconfiguracao = 22
         AND mt.id_entidadematricula = eec.id_entidade
         AND eec.st_valor = 2
-- CONCLUSÃO
    LEFT JOIN tb_matriculacolacao AS mc
      ON mc.id_matricula = mt.id_matricula
         AND mc.bl_ativo = 1
         AND mc.bl_presenca = 1
         AND mc.dt_colacao IS NOT NULL
-- DEFICIÊNCIAS
    LEFT JOIN tb_usuariodeficiencia AS udf1 ON udf1.id_usuario = us.id_usuario AND udf1.id_deficiencia = 1
    LEFT JOIN tb_usuariodeficiencia AS udf2 ON udf2.id_usuario = us.id_usuario AND udf2.id_deficiencia = 2
    LEFT JOIN tb_usuariodeficiencia AS udf3 ON udf3.id_usuario = us.id_usuario AND udf3.id_deficiencia = 3
    LEFT JOIN tb_usuariodeficiencia AS udf4 ON udf4.id_usuario = us.id_usuario AND udf4.id_deficiencia = 4
    LEFT JOIN tb_usuariodeficiencia AS udf5 ON udf5.id_usuario = us.id_usuario AND udf5.id_deficiencia = 5
    LEFT JOIN tb_usuariodeficiencia AS udf6 ON udf6.id_usuario = us.id_usuario AND udf6.id_deficiencia = 6
    LEFT JOIN tb_usuariodeficiencia AS udf7 ON udf7.id_usuario = us.id_usuario AND udf7.id_deficiencia = 7
    LEFT JOIN tb_usuariodeficiencia AS udf8 ON udf8.id_usuario = us.id_usuario AND udf8.id_deficiencia = 8
    LEFT JOIN tb_usuariodeficiencia AS udf9 ON udf9.id_usuario = us.id_usuario AND udf9.id_deficiencia = 9
    LEFT JOIN tb_usuariodeficiencia AS udf10 ON udf10.id_usuario = us.id_usuario AND udf10.id_deficiencia = 10
    LEFT JOIN tb_usuariodeficiencia AS udf11 ON udf11.id_usuario = us.id_usuario AND udf11.id_deficiencia = 11
    LEFT JOIN tb_usuariodeficiencia AS udf12 ON udf12.id_usuario = us.id_usuario AND udf12.id_deficiencia = 12
    LEFT JOIN tb_usuariodeficiencia AS udf13 ON udf13.id_usuario = us.id_usuario AND udf13.id_deficiencia = 13
-- ALIAS PARA SEMESTRE CONCLUSAO
    OUTER APPLY
    (
      SELECT
        (CASE
         WHEN mc.id_matriculacolacao IS NULL OR mt.id_evolucao != 81 -- G2: Formado
           THEN NULL
         WHEN (MONTH(mc.dt_colacao) BETWEEN 7 AND 12)
           THEN CONCAT('02', '/', YEAR(tm.dt_inicio))
         ELSE CONCAT('01', '/', YEAR(tm.dt_inicio))
         END) AS st_semestreconclusao
    ) AS conclusao
-- VENDA
    OUTER APPLY
    (
      SELECT
        TOP 1
        *
      FROM tb_vendaproduto
      WHERE id_matricula = mt.id_matricula AND bl_ativo = 1
      ORDER BY
        dt_cadastro ASC
    ) AS vp
  WHERE
    mt.id_evolucao IN (6, 20, 21, 27, 40, 81, 15)
    AND mt.bl_institucional = 0
 CREATE VIEW vw_logmatricula AS
SELECT  mt.id_matricula ,
us.st_nomecompleto ,
mt.id_usuario ,
lg.id_funcionalidade ,
sl.st_saladeaula ,
fc.st_funcionalidade ,
pf.st_nomeperfil ,
lg.dt_cadastro, mt.bl_institucional
FROM    tb_matricula AS mt
JOIN dbo.tb_logacesso AS lg ON lg.id_usuario = mt.id_usuario
AND lg.id_entidade = mt.id_entidadeatendimento AND lg.id_matricula = mt.id_matricula
JOIN dbo.tb_funcionalidade AS fc ON fc.id_funcionalidade = lg.id_funcionalidade
LEFT JOIN dbo.tb_saladeaula AS sl ON sl.id_saladeaula = lg.id_saladeaula
LEFT JOIN dbo.tb_perfil AS pf ON pf.id_perfil = lg.id_perfil
JOIN tb_usuario AS us ON us.id_usuario = lg.id_usuario

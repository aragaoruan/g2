CREATE VIEW [rel].[vw_cancelamento] AS
SELECT DISTINCT
	vw.st_nomecompleto,
	vw.id_entidadematricula,
	vw.st_entidadematricula,
	vw.id_matricula,
	vw.dt_cadastro AS dt_matricula,
	ac.id_areaconhecimento,
	ac.st_areaconhecimento,
	vw.id_projetopedagogico,
	vw.st_projetopedagogico,
	evCan.st_evolucao,
	evCan.id_evolucao,
	can.dt_solicitacao AS dt_pedidocancelamento,
	can.dt_referencia,
	can.dt_encaminhamentofinanceiro,
	canD.dt_desistiu AS dt_desistiu,
	can.nu_totalutilizado AS nu_valordisponibilizado,
	(
		SELECT
			STUFF(
				(
					SELECT DISTINCT
						' / ' + st_motivo
					FROM
						tb_motivocancelamento AS mc
					JOIN tb_motivo mot ON mc.id_motivo = mot.id_motivo
					AND mc.id_cancelamento = vw.id_cancelamento FOR XML PATH ('')
				),
				1,
				1,
				''
			)
	) AS st_motivos
FROM
	dbo.vw_matricula AS vw
JOIN tb_areaprojetopedagogico AS app ON vw.id_areaconhecimento = app.id_areaconhecimento
JOIN tb_areaconhecimento AS ac ON app.id_areaconhecimento = ac.id_areaconhecimento
JOIN tb_cancelamento AS can ON can.id_cancelamento = vw.id_cancelamento
JOIN tb_evolucao AS evCan ON can.id_evolucao = evCan.id_evolucao
LEFT JOIN tb_cancelamento AS canD ON canD.id_cancelamento = vw.id_cancelamento
AND canD.id_evolucao = 52
GO


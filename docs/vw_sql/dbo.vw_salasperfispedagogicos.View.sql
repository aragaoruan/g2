
GO
/****** Object:  View [dbo].[vw_salasperfispedagogicos]    Script Date: 10/08/2012 17:42:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create VIEW [dbo].[vw_salasperfispedagogicos] AS
SELECT  usr.id_usuario ,
usr.id_perfilreferencia ,
sai.id_saladeaulaintegracao ,
sai.id_saladeaula,
sai.st_codsistemacurso ,
sai.id_sistema ,
nu_codigoperfil = CASE pf.id_perfilpedagogico
WHEN 2 THEN ei.nu_perfilprojeto
WHEN 4 THEN ei.nu_perfildisciplina
WHEN 3 THEN ei.nu_perfilobservador
WHEN 7 THEN ei.nu_perfilsuporte
END
,sa.id_entidade
,us.st_nomecompleto,
sa.st_saladeaula                  
FROM    dbo.tb_usuarioperfilentidadereferencia AS usr
LEFT JOIN dbo.tb_perfilreferenciaintegracao AS pri ON pri.id_perfilreferencia = usr.id_perfilreferencia
JOIN dbo.tb_areaprojetosala AS ars ON ars.id_projetopedagogico = usr.id_projetopedagogico
JOIN dbo.tb_saladeaulaintegracao AS sai ON sai.id_sistema = 6 AND sai.id_saladeaula = ars.id_saladeaula
JOIN tb_perfil AS pf ON pf.id_perfil = usr.id_perfil
AND pf.id_perfilpedagogico = 2
JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = sai.id_saladeaula
JOIN dbo.tb_entidadeintegracao AS ei ON sa.id_entidade = ei.id_entidade AND ei.nu_perfilprojeto IS NOT NULL AND ei.id_sistema = 6
JOIN dbo.tb_usuario AS us ON us.id_usuario = usr.id_usuario AND us.bl_ativo = 1

UNION
SELECT  usr.id_usuario ,
usr.id_perfilreferencia ,
sai.id_saladeaulaintegracao ,
sai.id_saladeaula,
sai.st_codsistemacurso ,
sai.id_sistema ,
nu_codigoperfil = CASE pf.id_perfilpedagogico
WHEN 2 THEN ei.nu_perfilprojeto
WHEN 4 THEN ei.nu_perfildisciplina
WHEN 3 THEN ei.nu_perfilobservador
WHEN 7 THEN ei.nu_perfilsuporte
END
,sa.id_entidade
,us.st_nomecompleto,
sa.st_saladeaula    
FROM    dbo.tb_usuarioperfilentidadereferencia AS usr
LEFT JOIN dbo.tb_perfilreferenciaintegracao AS pri ON pri.id_perfilreferencia = usr.id_perfilreferencia
JOIN dbo.tb_disciplinasaladeaula AS ds ON ds.id_disciplina = usr.id_disciplina
JOIN dbo.tb_saladeaulaintegracao AS sai ON sai.id_sistema = 6 AND sai.id_saladeaula = ds.id_saladeaula
JOIN tb_perfil AS pf ON pf.id_perfil = usr.id_perfil
AND pf.id_perfilpedagogico = 4
JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = sai.id_saladeaula
JOIN dbo.tb_entidadeintegracao AS ei ON ei.id_entidade = sa.id_entidade AND ei.nu_perfildisciplina IS NOT NULL AND ei.id_sistema = 6
JOIN dbo.tb_usuario AS us ON us.id_usuario = usr.id_usuario AND us.bl_ativo = 1
UNION
SELECT  usr.id_usuario ,
usr.id_perfilreferencia ,
sai.id_saladeaulaintegracao ,
sai.id_saladeaula,
sai.st_codsistemacurso ,
sai.id_sistema ,
nu_codigoperfil = CASE pf.id_perfilpedagogico
WHEN 2 THEN ei.nu_perfilprojeto
WHEN 4 THEN ei.nu_perfildisciplina
WHEN 3 THEN ei.nu_perfilobservador
WHEN 7 THEN ei.nu_perfilsuporte
END
,sa.id_entidade
,us.st_nomecompleto,
sa.st_saladeaula    
FROM    dbo.tb_usuarioperfilentidadereferencia AS usr
JOIN dbo.tb_saladeaulaintegracao AS sai ON sai.id_sistema = 6 
JOIN dbo.tb_saladeaulaentidade AS sae ON sae.id_saladeaula = sai.id_saladeaula
JOIN tb_perfil AS pf ON pf.id_perfil = usr.id_perfil
AND pf.id_entidade = sae.id_entidade
AND pf.id_perfilpedagogico = 3
JOIN dbo.tb_entidadeintegracao AS ei ON ei.id_entidade = pf.id_entidade AND ei.nu_perfilobservador IS NOT NULL AND ei.id_sistema = 6
JOIN dbo.tb_usuario AS us ON us.id_usuario = usr.id_usuario AND us.bl_ativo = 1
JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = sai.id_saladeaula
UNION
SELECT  usr.id_usuario ,
usr.id_perfilreferencia ,
sai.id_saladeaulaintegracao ,
sai.id_saladeaula,
sai.st_codsistemacurso ,
sai.id_sistema ,
nu_codigoperfil = CASE pf.id_perfilpedagogico
WHEN 2 THEN ei.nu_perfilprojeto
WHEN 4 THEN ei.nu_perfildisciplina
WHEN 3 THEN ei.nu_perfilobservador
WHEN 7 THEN ei.nu_perfilsuporte
END
,pf.id_entidade
,us.st_nomecompleto,
sa.st_saladeaula    
FROM    dbo.tb_usuarioperfilentidadereferencia AS usr
JOIN dbo.tb_saladeaulaintegracao AS sai ON sai.id_sistema = 6 
JOIN dbo.tb_saladeaulaentidade AS sae ON sae.id_saladeaula = sai.id_saladeaula
JOIN tb_perfil AS pf ON pf.id_perfil = usr.id_perfil
AND pf.id_entidade = sae.id_entidade
AND pf.id_perfilpedagogico = 7
JOIN dbo.tb_entidadeintegracao AS ei ON ei.id_entidade = pf.id_entidade AND ei.nu_perfilsuporte IS NOT NULL AND ei.id_sistema = 6
JOIN dbo.tb_usuario AS us ON us.id_usuario = usr.id_usuario AND us.bl_ativo = 1
JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = sai.id_saladeaula
UNION
SELECT  usr.id_usuario ,
usr.id_perfilreferencia ,
sai.id_saladeaulaintegracao ,
sai.id_saladeaula,
sai.st_codsistemacurso ,
sai.id_sistema ,
nu_codigoperfil = CASE pf.id_perfilpedagogico
WHEN 2 THEN ei.nu_perfilprojeto
WHEN 4 THEN ei.nu_perfildisciplina
WHEN 3 THEN ei.nu_perfilobservador
WHEN 7 THEN ei.nu_perfilsuporte
WHEN 1 THEN 3
END
,sa.id_entidade
,us.st_nomecompleto,
sa.st_saladeaula    
FROM    dbo.tb_usuarioperfilentidadereferencia AS usr
JOIN dbo.tb_saladeaulaintegracao AS sai ON sai.id_sistema = 6 AND  sai.id_saladeaula =  usr.id_saladeaula 
JOIN dbo.tb_saladeaulaentidade AS sae ON sae.id_saladeaula = sai.id_saladeaula
JOIN tb_perfil AS pf ON pf.id_perfil = usr.id_perfil
AND pf.id_entidade = sae.id_entidade
AND pf.id_perfilpedagogico = 1
JOIN dbo.tb_entidadeintegracao AS ei ON ei.id_entidade = pf.id_entidade AND ei.nu_perfilsuporte IS NOT NULL AND ei.id_sistema = 6
JOIN dbo.tb_usuario AS us ON us.id_usuario = usr.id_usuario AND us.bl_ativo = 1
JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = sai.id_saladeaula
GO


GO
/****** Object:  View [dbo].[vw_entidadeconta]    Script Date: 10/08/2012 17:41:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_entidadeconta] as
select ce.id_contaentidade, e.id_entidade, ce.st_agencia, ce.st_conta, b.st_banco, b.st_nomebanco, tc.id_tipodeconta, tc.st_tipodeconta,
b.st_nomebanco +'/'+tc.st_tipodeconta as st_bancoconta, ce.bl_padrao
from tb_entidade e
JOIN tb_contaentidade as ce ON ce.id_entidade = e.id_entidade
JOIN tb_banco as b ON b.st_banco = ce.st_banco
JOIN tb_tipodeconta as tc ON tc.id_tipodeconta = ce.id_tipodeconta
GO

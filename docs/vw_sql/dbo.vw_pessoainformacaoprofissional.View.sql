
GO
/****** Object:  View [dbo].[vw_pessoainformacaoprofissional]    Script Date: 10/08/2012 17:42:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_pessoainformacaoprofissional] AS
select aa.id_areaatuacao, infppess.id_entidade, infppess.id_usuario, 
	   infppess.st_cargo, infppess.st_organizacao, aa.st_areaatuacao, infppess.dt_inicio, infppess.dt_fim
from tb_informacaoprofissionalpessoa as infppess
INNER JOIN tb_areaatuacao as aa ON aa.id_areaatuacao = infppess.id_areaatuacao
INNER JOIN tb_pessoa as p ON p.id_usuario = infppess.id_usuario and p.id_entidade = infppess.id_entidade
GO

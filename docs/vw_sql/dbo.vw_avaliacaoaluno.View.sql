CREATE VIEW [dbo].[vw_avaliacaoaluno] AS
  WITH cfg AS (
      SELECT
        0 AS FALSE,
        1 AS TRUE,
        68 AS SITUACAO_AGENDADO,
        86 AS SITUACAO_ATIVA,
        3 AS TIPO_DISCIPLINA_AMBIENTACAO,
        2 AS TIPO_PROVA_SALA_AULA
  )
  SELECT
    DISTINCT
    id_avaliacaoaluno,
    mat.id_matricula,
    mat.id_entidadematricula AS entidadematricula,
    mat.id_entidadeatendimento AS entidadeatendimento,
    mat.id_projetopedagogico,
    mat.id_entidadeatendimento,
    mat.id_situacaoagendamento,
    mod.id_modulo,
    mat_dis.id_situacao,
    mat_dis.id_evolucao,
    mat_dis.id_matriculadisciplina,
    alo.id_saladeaula,
    ava_con_ref.id_avaliacaoconjuntoreferencia,
    sal_aul.id_categoriasala,
    dis.id_disciplina,
    dis.id_tipodisciplina,
    dis_evo.id_evolucao AS id_evolucaodisciplina,
    ava_con.id_tipoprova,
    ava_con.id_tipocalculoavaliacao,
    ava_con.id_avaliacaoconjunto,
    ava_con_rel.id_avaliacao,
    ava.id_tipoavaliacao,
    ava_age.id_avaliacaoagendamento,
    ava_alu.id_upload,
    ava_alu.id_tiponota,
    ava_alu.id_usuariocadastro AS id_usuariocadavaliacao,
    usu_int.id_entidade AS entidadeintegracao,
    usu_int.id_usuario AS usuario,
    sit_mat.st_situacao AS st_situacaomatricula,
    usu.st_nomecompleto,
    mod.st_tituloexibicao AS st_tituloexibicaomodulo,
    sal_aul.dt_cadastro AS dt_cadastrosala,
    sal_aul.dt_encerramento AS dt_encerramentosala,
    sal_aul.dt_abertura AS dt_aberturasala,
    dis.st_tituloexibicao AS st_tituloexibicaodisciplina,
    dis.nu_cargahoraria,
    dis_evo.st_evolucao AS st_evolucaodisciplina,
    ava.st_avaliacao,
    CAST(ava.nu_valor AS VARCHAR(100)) AS nu_notamax,
    ava.bl_recuperacao,
    tip_ava.st_tipoavaliacao,
    sit_mat_dis.st_situacao,
    evo_mat_dis.st_evolucao,
    pro_ped.nu_notamaxima,
    pro_ped.nu_percentualaprovacao,
    pro_ped.st_projetopedagogico,
    sit_sal_aul.st_situacao AS st_situacaosala,
    ava_age.bl_provaglobal,
    ava_age.dt_agendamento,
    nota_con.st_notaconceitual,
    nota_con.id_notaconceitual,
    ava_alu.st_nota,
    ava_alu.bl_ativo,
    ava_alu.dt_avaliacao,
    ava_alu.dt_defesa,
    UPPER(ava_alu.st_tituloavaliacao) AS st_tituloavaliacao,
    ava_alu.st_justificativa,
    ava_alu.dt_cadastro AS dt_cadastroavaliacao,
    usu_int.st_codusuario,
    usu_int.st_codusuario AS CODUSUARIO,
    usu_cad.st_nomecompleto AS st_usuariocadavaliacao,
    upl.st_upload,
    agc.id_provasrealizadas,
    CONVERT(VARCHAR(11), ava_alu.dt_cadastro, 103) AS dt_cadastrotcc,
    CONVERT(VARCHAR(11), ava_alu.dt_avaliacao, 103) AS st_dtavaliacao,
    (CASE WHEN ava_age_ref.id_avaliacaoconjuntoreferencia = ava_con_ref.id_avaliacaoconjuntoreferencia
      THEN _.TRUE
     ELSE _.FALSE END
    ) bl_agendamento,
    (CASE WHEN ava_con_rel.id_avaliacaorecupera IS NULL
      THEN _.FALSE
     ELSE ava_con_rel.id_avaliacaorecupera END) AS id_avaliacaorecupera,
    mat_dis.id_situacaoagendamento as id_situacaomatdisc,
    alo.id_alocacao,
    alo.id_situacaotcc
  FROM tb_matricula AS mat
    CROSS APPLY cfg AS _
    INNER JOIN tb_usuario AS usu ON usu.id_usuario = mat.id_usuario
    INNER JOIN tb_modulo AS mod ON mod.id_projetopedagogico = mat.id_projetopedagogico
                                   AND mod.bl_ativo = _.TRUE
    INNER JOIN tb_matriculadisciplina AS mat_dis ON mat_dis.id_matricula = mat.id_matricula
                                                    OR mat_dis.id_matriculaoriginal = mat.id_matricula
    INNER JOIN tb_modulodisciplina AS mod_dic ON mod_dic.id_modulo = mod.id_modulo
                                                 AND mat_dis.id_disciplina = mod_dic.id_disciplina
                                                 AND mod_dic.bl_ativo = _.TRUE
    INNER JOIN tb_alocacao AS alo ON alo.id_matriculadisciplina = mat_dis.id_matriculadisciplina
                                     AND alo.bl_ativo = _.TRUE
    INNER JOIN tb_avaliacaoconjuntoreferencia AS ava_con_ref ON ava_con_ref.id_saladeaula = alo.id_saladeaula
                                                                AND ava_con_ref.dt_fim IS NULL
    INNER JOIN tb_saladeaula AS sal_aul ON sal_aul.id_saladeaula = alo.id_saladeaula
    INNER JOIN tb_disciplinasaladeaula AS dis_sal_aul ON dis_sal_aul.id_saladeaula = alo.id_saladeaula
    INNER JOIN tb_disciplina AS dis ON dis.id_disciplina = mat_dis.id_disciplina
    INNER JOIN tb_evolucao AS dis_evo ON dis_evo.id_evolucao = mat_dis.id_evolucao
    INNER JOIN tb_avaliacaoconjunto AS ava_con ON ava_con.id_avaliacaoconjunto = ava_con_ref.id_avaliacaoconjunto
                                                  AND id_tipoprova = _.TIPO_PROVA_SALA_AULA
    INNER JOIN tb_avaliacaoconjuntorelacao AS ava_con_rel ON ava_con_rel.id_avaliacaoconjunto =
                                                             ava_con.id_avaliacaoconjunto
    INNER JOIN tb_avaliacao AS ava ON ava.id_avaliacao = ava_con_rel.id_avaliacao
    INNER JOIN tb_tipoavaliacao AS tip_ava ON tip_ava.id_tipoavaliacao = ava.id_tipoavaliacao
    INNER JOIN tb_situacao AS sit_mat ON mat.id_situacaoagendamento = sit_mat.id_situacao
    INNER JOIN tb_situacao AS sit_mat_dis ON sit_mat_dis.id_situacao = mat_dis.id_situacao
    INNER JOIN tb_situacao AS sit_sal_aul ON sit_sal_aul.id_situacao = sal_aul.id_situacao
    INNER JOIN tb_evolucao AS evo_mat_dis ON evo_mat_dis.id_evolucao = mat_dis.id_evolucao
    INNER JOIN tb_projetopedagogico AS pro_ped ON pro_ped.id_projetopedagogico = mat.id_projetopedagogico
    LEFT JOIN tb_avaliacaoagendamento AS ava_age ON mat.id_matricula = ava_age.id_matricula
                                                    AND ava.id_avaliacao = ava_age.id_avaliacao
                                                    AND ava_age.id_situacao = _.SITUACAO_AGENDADO
                                                    AND ava_age.nu_presenca = _.TRUE
    LEFT JOIN tb_avalagendamentoref AS ava_age_ref ON ava_age_ref.id_avaliacaoagendamento =
                                                      ava_age.id_avaliacaoagendamento
                                                      AND ava_age_ref.id_avaliacaoconjuntoreferencia =
                                                          ava_con_ref.id_avaliacaoconjuntoreferencia
    LEFT JOIN tb_avaliacaoaluno AS ava_alu ON ava_alu.id_matricula = mat.id_matricula
                                              AND ava_alu.id_avaliacaoconjuntoreferencia =
                                                  ava_con_ref.id_avaliacaoconjuntoreferencia
                                              AND ava_alu.id_avaliacao = ava.id_avaliacao
                                              AND ava_alu.bl_ativo = _.TRUE
                                              AND ava_alu.id_situacao = _.SITUACAO_ATIVA
    LEFT JOIN tb_notaconceitual AS nota_con ON ava_alu.id_notaconceitual = nota_con.id_notaconceitual
    LEFT JOIN tb_saladeaulaintegracao AS sal_aul_int ON sal_aul_int.id_saladeaula = sal_aul.id_saladeaula
    LEFT JOIN tb_usuariointegracao AS usu_int ON usu_int.id_usuario = mat.id_usuario
                                                 AND usu_int.id_entidade = sal_aul.id_entidade
                                                 AND usu_int.id_sistema = sal_aul_int.id_sistema
    LEFT JOIN tb_usuario AS usu_cad ON usu_cad.id_usuario = ava_alu.id_usuariocadastro
    LEFT JOIN tb_upload AS upl ON upl.id_upload = ava_alu.id_upload
    OUTER APPLY (
                  SELECT
                    COUNT(id_avaliacaoagendamento) AS id_provasrealizadas
                  FROM tb_avaliacaoagendamento
                  WHERE id_matricula = mat.id_matricula
                        AND nu_presenca = _.TRUE
                        AND bl_ativo = _.FALSE
                        AND id_situacao = _.SITUACAO_AGENDADO
                ) AS agc
  WHERE mat.bl_ativo = _.TRUE
        AND dis.id_tipodisciplina <> _.TIPO_DISCIPLINA_AMBIENTACAO
go



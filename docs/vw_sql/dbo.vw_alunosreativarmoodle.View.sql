CREATE VIEW [dbo].[vw_alunosreativarmoodle] AS
  SELECT
    DISTINCT
    sal_aul.id_saladeaula,
    alo.id_alocacao,
    alo_int.id_alocacaointegracao,
    alo_int.bl_encerrado,
    sal_aul.id_entidade,
    mat.id_matricula,
    mat.id_usuario,
    sal_aul_int.st_codsistemacurso,
    ent_int.nu_perfilalunoencerrado,
    alo_int.id_sistema,
    sal_aul.id_entidadeintegracao
  FROM
    tb_saladeaula AS sal_aul
    INNER JOIN tb_saladeaulaintegracao AS sal_aul_int ON sal_aul_int.id_saladeaula = sal_aul.id_saladeaula
    INNER JOIN tb_alocacao AS alo ON alo.id_saladeaula = sal_aul.id_saladeaula AND alo.bl_ativo = 1
    INNER JOIN tb_alocacaointegracao AS alo_int ON alo_int.id_alocacao = alo.id_alocacao AND alo_int.bl_encerrado = 1
                                                   AND alo_int.id_entidadeintegracao = sal_aul.id_entidadeintegracao
    INNER JOIN tb_matriculadisciplina AS mat_dis ON mat_dis.id_matriculadisciplina = alo.id_matriculadisciplina
    INNER JOIN tb_matricula AS mat ON mat.id_matricula = mat_dis.id_matricula
    INNER JOIN tb_usuariointegracao AS usu_int ON usu_int.id_usuario = mat.id_usuario
                                                  AND usu_int.id_sistema = 6
                                                  AND usu_int.id_entidadeintegracao = sal_aul.id_entidadeintegracao
                                                  AND usu_int.id_entidade = sal_aul.id_entidade
    INNER JOIN tb_entidadeintegracao AS ent_int ON ent_int.id_entidade = sal_aul.id_entidade AND
                                                   ent_int.id_entidadeintegracao = sal_aul.id_entidadeintegracao
                                                   AND ent_int.id_sistema = 6
                                                   AND ent_int.id_situacao = 198
                                                   AND ent_int.bl_ativo = 1
  WHERE
    sal_aul.bl_ativa = 1
    AND sal_aul.id_tiposaladeaula = 2
    AND sal_aul.id_entidade NOT IN (7)
    --Se a sala tiver a data de encerramento + nu_diasextensao(se for null coloca 0) maior que a data de hoje (reaberta)
    AND CAST(
            DATEADD(
                DAY,
                ISNULL(sal_aul.nu_diasextensao, 0),
                sal_aul.dt_encerramento
            ) AS DATE
        ) >= CAST(GETDATE() AS DATE)
GO


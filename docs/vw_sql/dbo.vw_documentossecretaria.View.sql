
GO
/****** Object:  View [dbo].[vw_documentossecretaria]    Script Date: 10/08/2012 17:41:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_documentossecretaria] AS

SELECT doc.id_documentos,doc.bl_digitaliza,doc.st_documentos, doc.bl_obrigatorio, doc.bl_ativo, doc.nu_quantidade,doc.id_entidade
		, doc.id_usuariocadastro, tdoc.id_tipodocumento, tdoc.st_tipodocumento, sit.id_situacao, sit.st_situacao
FROM tb_documentos doc
INNER JOIN tb_tipodocumento tdoc 
	ON doc.id_tipodocumento = tdoc.id_tipodocumento
INNER JOIN tb_situacao sit
	ON doc.id_situacao	= sit.id_situacao
GO

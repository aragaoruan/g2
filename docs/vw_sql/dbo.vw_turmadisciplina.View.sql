 CREATE VIEW dbo.vw_turmadisciplina AS
SELECT DISTINCT
	tm.id_turma,
	tm.st_turma,
	te.id_entidade,
	mdd.id_disciplina,
	tm.id_evolucao,
	tm.id_situacao
FROM
	tb_turma AS tm
JOIN dbo.tb_turmaentidade AS te ON te.id_turma = tm.id_turma
AND te.bl_ativo = 1
JOIN dbo.tb_turmaprojeto AS tp ON tp.id_turma = tm.id_turma
AND tp.bl_ativo = 1
JOIN dbo.tb_modulo AS md ON md.id_projetopedagogico = tp.id_projetopedagogico
AND md.bl_ativo = 1
JOIN dbo.tb_modulodisciplina AS mdd ON mdd.id_modulo = md.id_modulo
AND mdd.bl_ativo = 1
WHERE
	tm.bl_ativo = 1
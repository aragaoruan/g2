
GO
/****** Object:  View [rel].[vw_alunosreprovados]    Script Date: 10/08/2012 17:42:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [rel].[vw_alunosreprovados] AS
SELECT  mt.id_matricula ,
us.st_nomecompleto ,
dc.st_disciplina ,
du.dt_ultimaav ,
na.nu_avaliacoes ,
mt.dt_inicio ,
CAST(ct.nu_ddd AS VARCHAR(2)) + '-'
+ CAST (ct.nu_telefone AS VARCHAR(10)) AS nu_telefone ,
mt.id_entidadeatendimento ,
mt.id_projetopedagogico
FROM    dbo.tb_matricula AS mt
JOIN dbo.tb_matriculadisciplina AS md ON mt.id_matricula = md.id_matricula
AND md.id_evolucao = 19
JOIN dbo.tb_avaliacaoaluno AS aa ON aa.id_matricula = md.id_matricula
AND aa.bl_ativo = 1
JOIN dbo.tb_avaliacaodisciplina AS ad ON aa.id_avaliacao = ad.id_avaliacao
AND ad.id_disciplina = md.id_disciplina
JOIN dbo.tb_disciplina AS dc ON dc.id_disciplina = md.id_disciplina
CROSS APPLY ( SELECT TOP 1
dt_avaliacao AS dt_ultimaav
FROM      dbo.tb_avaliacaoaluno
WHERE     aa.id_avaliacaoaluno = id_avaliacaoaluno
ORDER BY  dt_avaliacao DESC
) du
LEFT JOIN dbo.tb_contatostelefonepessoa AS ctp ON ctp.id_usuario = mt.id_usuario
AND mt.id_entidadeatendimento = ctp.id_entidade
AND ctp.bl_padrao = 1
LEFT JOIN dbo.tb_contatostelefone AS ct ON ct.id_telefone = ctp.id_telefone
JOIN dbo.tb_usuario AS us ON us.id_usuario = mt.id_usuario
CROSS APPLY (SELECT COUNT (id_avaliacao)AS nu_avaliacoes FROM dbo.tb_avaliacaoaluno WHERE id_matricula = aa.id_matricula AND id_avaliacao = aa.id_avaliacao) as na
WHERE   mt.id_evolucao = 6 AND mt.bl_institucional = 0
GO

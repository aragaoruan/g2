
GO
/****** Object:  View [dbo].[vw_perfilpermissoesfuncionalidade]    Script Date: 10/08/2012 17:42:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_perfilpermissoesfuncionalidade] AS

SELECT pe.id_perfil, pe.st_nomeperfil, perf.id_funcionalidade, f.id_funcionalidadepai, f.id_tipofuncionalidade,
	f.st_funcionalidade, fpai.st_funcionalidade as st_funcionalidadepai,
	pf.id_permissao, p.st_nomepermissao, p.st_descricao, CASE WHEN ppf.id_permissao IS NOT NULL THEN 1 ELSE 0 END as selecionado,
	CASE WHEN ppf.id_permissao IS NOT NULL THEN 1 ELSE 0 END as bl_selecionado
FROM tb_perfil pe

JOIN tb_perfilfuncionalidade perf ON  pe.id_perfil = perf.id_perfil and perf.bl_ativo = 1
JOIN tb_permissaofuncionalidade pf ON pf.id_funcionalidade = perf.id_funcionalidade
JOIN tb_funcionalidade as f ON perf.id_funcionalidade = f.id_funcionalidade
JOIN tb_permissao p ON pf.id_permissao = p.id_permissao
LEFT JOIN tb_funcionalidade fpai ON f.id_funcionalidadepai = fpai.id_funcionalidade
LEFT JOIN tb_perfilpermissaofuncionalidade ppf ON ppf.id_perfil = pe.id_perfil AND ppf.id_funcionalidade = perf.id_funcionalidade AND ppf.id_permissao = pf.id_permissao
GO

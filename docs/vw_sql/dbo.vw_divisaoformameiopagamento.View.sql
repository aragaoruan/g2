
GO
/****** Object:  View [dbo].[vw_divisaoformameiopagamento]    Script Date: 10/08/2012 17:41:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_divisaoformameiopagamento] as
select fmp.id_formapagamento, tdf.id_tipodivisaofinanceira, mp.id_meiopagamento, mp.st_descricao, mp.st_meiopagamento
from tb_formameiopagamento as fmp
INNER JOIN tb_tipodivisaofinanceira as tdf ON tdf.id_tipodivisaofinanceira = fmp.id_tipodivisaofinanceira
INNER JOIN tb_meiopagamento as mp ON mp.id_meiopagamento = fmp.id_meiopagamento
GO


GO
/****** Object:  View [rel].[vw_avaliacaodia]    Script Date: 10/08/2012 17:42:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [rel].[vw_avaliacaodia] as
SELECT us.st_nomecompleto, mt.id_matricula,avv.st_avaliacao, av.st_nota, usc.st_nomecompleto AS st_usuariocadastro, DATEADD(dd, 0, DATEDIFF(dd, 0, av.dt_cadastro)) AS dt_cadastro, mt.id_entidadeatendimento  FROM dbo.tb_avaliacaoaluno av
JOIN dbo.tb_matricula mt ON mt.id_matricula = av.id_matricula
JOIN dbo.tb_usuario us ON us.id_usuario = mt.id_matricula
JOIN dbo.tb_usuario usc ON usc.id_usuario = av.id_usuariocadastro
JOIN dbo.tb_projetopedagogico PP ON mt.id_projetopedagogico = pp.id_projetopedagogico
JOIN dbo.tb_avaliacao avv ON avv.id_avaliacao = av.id_avaliacao
WHERE av.bl_ativo = 1 AND av.id_situacao = 86
GO

CREATE VIEW [dbo].[vw_professoressalasencerradas] AS
  SELECT
    DISTINCT
    pri.id_perfilreferenciaintegracao,
    uper.id_perfilreferencia,
    uper.id_saladeaula,
    uper.id_usuario,
    uper.id_entidade,
    sai.st_codsistemacurso,
    ei.id_entidadeintegracao
  FROM
-- Perfil de Professor
    tb_usuarioperfilentidadereferencia AS uper
-- Perfis que tem integração com o Moodle
    JOIN tb_perfilreferenciaintegracao AS pri ON pri.id_perfilreferencia = uper.id_perfilreferencia AND
                                                 pri.id_sistema = 6
                                                 AND pri.bl_encerrado = 0
-- Sala de Aula
    JOIN tb_saladeaula AS sa ON sa.id_saladeaula = uper.id_saladeaula AND sa.id_tiposaladeaula = 2
-- Disciplina Sala
    JOIN tb_disciplinasaladeaula AS ds ON sa.id_saladeaula = ds.id_saladeaula
-- Disciplina
    JOIN tb_disciplina AS dc ON dc.id_disciplina = ds.id_disciplina
-- Entidades que tem integração com o Moodle
    JOIN tb_entidadeintegracao AS ei ON ei.id_entidadeintegracao = sa.id_entidadeintegracao AND
                                        uper.id_entidade = ei.id_entidade AND ei.id_sistema = 6 AND ei.bl_ativo = 1 AND
                                        ei.id_situacao = 198
-- Usuários que tem integração com o Moodle
    JOIN tb_usuariointegracao AS ui ON ui.id_usuario = uper.id_usuario AND ui.id_entidade = uper.id_entidade AND
                                       ui.id_sistema = 6 AND ui.id_entidadeintegracao = ei.id_entidadeintegracao
-- Salas que tem integração com o Moodle
    JOIN tb_saladeaulaintegracao AS sai ON sai.id_saladeaula = sa.id_saladeaula AND
                                           sai.id_saladeaulaintegracao = pri.id_saladeaulaintegracao AND
                                           sai.id_sistema = 6
-- Encerramento da Sala
    LEFT JOIN tb_encerramentosala AS enc ON enc.id_saladeaula = sa.id_saladeaula AND
                                            enc.dt_encerramentoprofessor IS NOT NULL
  WHERE
    uper.bl_ativo = 1
    -- Somente registros que não tiveram tentativa nas últimas 24 horas
    AND (
      pri.dt_tentativaencerramento IS NULL
      OR DATEADD(
             DAY,
             1,
             pri.dt_tentativaencerramento
         ) < GETDATE()
    )
    AND (
-- Salas que a data de encerramento (com extensão) já passou mais de 45 dias
      DATEADD(
          DAY,
          (
            ISNULL(sa.nu_diasextensao, 0) + 45
          ),
          sa.dt_encerramento
      ) < CAST(GETDATE() AS DATE)
      -- Ou salas que possuem encerramento do professor, independente da data
      OR (
        enc.id_encerramentosala IS NOT NULL
        AND dc.id_tipodisciplina = 1
      )
    )
GO


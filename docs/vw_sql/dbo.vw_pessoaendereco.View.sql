
GO
/****** Object:  View [dbo].[vw_pessoaendereco]    Script Date: 10/08/2012 17:42:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE VIEW [dbo].[vw_pessoaendereco] AS

select pe.id_usuario, pe.id_entidade, pe.bl_padrao, st_categoriaendereco, tp.id_tipoendereco, tp.st_tipoendereco,
	   ed.id_endereco, ed.st_endereco, ed.st_complemento, ed.nu_numero, ed.st_bairro, ed.st_cidade, ed.st_estadoprovincia, ed.st_cep, ed.sg_uf,
	   ctend.id_categoriaendereco, mu.id_municipio, mu.st_nomemunicipio, uf.st_uf, ps.id_pais, ps.st_nomepais, ed.bl_ativo
from tb_pessoaendereco as pe join 
	  tb_endereco as ed on (pe.id_endereco = ed.id_endereco)
	 join tb_tipoendereco as tp on (tp.id_tipoendereco = ed.id_tipoendereco)
	 join tb_categoriaendereco as ctend on (ctend.id_categoriaendereco = tp.id_categoriaendereco )
	 left join tb_municipio as mu on (mu.id_municipio = ed.id_municipio)
	 left join tb_uf as uf on (uf.sg_uf = ed.sg_uf)
	 left join tb_pais as ps on (ps.id_pais = ed.id_pais)

GO

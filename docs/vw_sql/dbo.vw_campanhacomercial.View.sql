CREATE VIEW dbo.vw_campanhacomercial AS
  SELECT
    cc.id_campanhacomercial,
    cc.st_campanhacomercial,
    cc.bl_aplicardesconto,
    cc.bl_ativo,
    cc.bl_disponibilidade,
    cc.dt_cadastro,
    cc.dt_fim,
    cc.dt_inicio,
    cc.id_entidade,
    e.st_nomeentidade,
    cc.id_situacao,
    s.st_situacao,
    cc.id_tipodesconto,
    td.st_tipodesconto,
    cc.id_usuariocadastro,
    cc.id_tipocampanha,
    tc.st_tipocampanha,
    cc.nu_disponibilidade,
    cc.st_descricao,
    cc.bl_todosprodutos,
    cc.bl_todasformas,
    cc.id_categoriacampanha,
    ctc.st_categoriacampanha,
    cc.nu_valordesconto,
    cc.nu_diasprazo,
    cc.id_premio,
    pr.st_prefixocupompremio,
    cc.id_finalidadecampanha,
    f.st_finalidadecampanha,
    cc.st_link,
    cc.st_imagem,
    cc.bl_portaldoaluno,
    cc.bl_mobile,
    CASE
    WHEN cc.id_campanhacomercial = vc.id_campanhacomercial
      THEN 1
    ELSE 0
    END AS bl_visualizado,
    CASE
    WHEN GETDATE() BETWEEN cc.dt_inicio AND cc.dt_fim
      THEN 1
    WHEN GETDATE() > cc.dt_inicio AND cc.dt_fim IS NULL
      THEN 1
    ELSE 0 END AS bl_vigente,
    m.id_usuario,
    m.id_matricula
  FROM tb_campanhacomercial AS cc
    JOIN tb_entidade AS e ON (cc.id_entidade = e.id_entidade)
    JOIN tb_situacao AS s ON (cc.id_situacao = s.id_situacao)
    JOIN tb_tipocampanha AS tc ON (cc.id_tipocampanha = tc.id_tipocampanha)
    JOIN tb_matricula AS m ON (m.id_entidadematricula = cc.id_entidade)
    LEFT JOIN tb_finalidadecampanha AS f ON (cc.id_finalidadecampanha = f.id_finalidadecampanha)
    LEFT JOIN tb_premio AS pr ON (cc.id_premio = pr.id_premio)
    LEFT JOIN tb_categoriacampanha AS ctc ON (cc.id_categoriacampanha = ctc.id_categoriacampanha)
    LEFT JOIN tb_tipodesconto AS td ON (cc.id_tipodesconto = td.id_tipodesconto)
    LEFT JOIN tb_visualizacampanha AS vc ON vc.id_campanhacomercial = cc.id_campanhacomercial AND
                                            vc.id_usuario = m.id_usuario
  WHERE (m.id_projetopedagogico IN (
    SELECT
      ppp.id_projetopedagogico
    FROM tb_produtoprojetopedagogico AS ppp
      JOIN tb_campanhaproduto AS cp ON (cp.id_produto = ppp.id_produto AND
                                        cp.id_campanhacomercial = cc.id_campanhacomercial)
  )
         OR
         cc.bl_todosprodutos = 1)
        OR
        (cc.id_campanhacomercial IN (
          SELECT
            xct.id_campanhacomercial
          FROM tb_campanhacategorias AS xct
            JOIN tb_categoria AS cat ON (cat.id_categoria = xct.id_categoria)
            JOIN tb_categoriaproduto AS catp ON (catp.id_categoria = xct.id_categoria AND
                                                 catp.id_categoria = cat.id_categoria)
            JOIN tb_produtoprojetopedagogico AS vp ON (vp.id_produto = catp.id_produto)
            JOIN tb_matricula AS mat ON (mat.id_projetopedagogico = vp.id_projetopedagogico AND
                                         mat.id_matricula = m.id_matricula)
        ))
CREATE VIEW [dbo].[vw_periodoletivoentidade] AS (
	SELECT DISTINCT
		pl.id_periodoletivo,
		pl.st_periodoletivo,
		pl.dt_inicioinscricao,
		pl.dt_fiminscricao,
		pl.dt_abertura,
		pl.dt_encerramento,
		pl.id_tipooferta,
		er.id_entidade,
		pl.id_entidade AS id_entidadeperiodo
	FROM tb_periodoletivo AS pl
	-- Relacionamento adicionado por solicitacao do Analista de Requisitos, por que os periodos letivos devem ser buscados tanto da entidade da sessao, quanto da entidade PAI da sessao (apenas o primeiro pai).
	JOIN vw_entidaderecursiva AS er ON pl.id_entidade IN (er.id_entidade, er.id_entidadepai)
)
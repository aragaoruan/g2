CREATE VIEW [dbo].[vw_encerramentoalunos]
AS
    SELECT DISTINCT
            al.id_alocacao ,
            al.id_saladeaula ,
            mt.id_usuario ,
            md.id_matricula ,
            md.id_disciplina ,
            md.id_matriculadisciplina ,
            dc.id_tipodisciplina ,
            st_nota = CASE WHEN dc.id_tipodisciplina = 1
                                AND aa.st_nota IS NOT NULL THEN aa.st_nota
                           WHEN dc.id_tipodisciplina = 2
                                AND tcc.st_nota IS NOT NULL THEN tcc.st_nota
                           ELSE NULL
                      END ,
            mt.st_nomecompleto ,
            lg.id_logacesso ,
            id_usuarioprofessor ,
            dt_encerramentoprofessor ,
            id_usuariocoordenador ,
            dt_encerramentocoordenador ,
            id_usuariopedagogico ,
            dt_encerramentopedagogico ,
            id_usuariofinanceiro ,
            dt_encerramentofinanceiro ,
            si.id_sistema ,
            sa.id_entidade ,
            ei.nu_perfilalunoencerrado ,
            es.dt_recusa ,
            es.st_motivorecusa ,
            es.id_usuariorecusa,
            id_upload = CASE WHEN dc.id_tipodisciplina = 1 THEN aa.id_upload
                             WHEN dc.id_tipodisciplina = 2 THEN tcc.id_upload
                             ELSE NULL
                        END ,
            aa.id_tiponota,
            sit.st_situacao as situacaotcc,
            sit.id_situacao as id_situacaotcc,
            dc.nu_cargahoraria
    FROM    dbo.tb_alocacao al
            JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula
            JOIN dbo.vw_matriculadisciplina AS md ON md.id_matriculadisciplina = al.id_matriculadisciplina
            JOIN dbo.tb_disciplina AS dc ON dc.id_disciplina = md.id_disciplina
            JOIN dbo.vw_matricula AS mt ON mt.id_matricula = md.id_matricula
                                           AND mt.bl_institucional = 0
                                           AND mt.id_evolucao IN ( 6, 15, 16 )
            OUTER APPLY ( SELECT TOP 1
                                    id_logacesso
                          FROM      dbo.tb_logacesso
                          WHERE     id_saladeaula = al.id_saladeaula
                                    AND id_usuario = mt.id_usuario
                                    AND ( ( sa.id_tiposaladeaula = 2
                                            AND CAST(dt_cadastro AS DATE) <= (DATEADD(day, ISNULL(sa.nu_diasextensao, 0), sa.dt_encerramento)) --sa.dt_encerramento
                                          )
                                          OR sa.id_tiposaladeaula = 1
                                        )
                        ) AS lg -- altera��o para AC-26484
            LEFT JOIN dbo.vw_avaliacaoaluno AS aa ON aa.id_saladeaula = al.id_saladeaula
                                                     AND aa.id_matricula = mt.id_matricula
                                                     AND aa.bl_ativo = 1
                                                     AND aa.id_tipoavaliacao IN (
                                                     5 )
            LEFT JOIN dbo.vw_avaliacaoaluno AS tcc ON tcc.id_saladeaula = al.id_saladeaula
                                                      AND tcc.id_matricula = mt.id_matricula
                                                      AND tcc.bl_ativo = 1
                                                      AND tcc.id_tipoavaliacao IN (
                                                      6 )
            LEFT JOIN dbo.tb_encerramentoalocacao AS ea ON al.id_alocacao = ea.id_alocacao
            LEFT JOIN dbo.tb_encerramentosala AS es ON es.id_encerramentosala = ea.id_encerramentosala
            LEFT JOIN dbo.tb_saladeaulaintegracao AS si ON si.id_saladeaula = al.id_saladeaula
            LEFT JOIN dbo.tb_entidadeintegracao AS ei ON ei.id_entidade = sa.id_entidade
                                                         AND ei.id_sistema = 6
    WHERE   al.bl_ativo = 1
    UNION
    SELECT DISTINCT
            al.id_alocacao ,
            al.id_saladeaula ,
            mt.id_usuario ,
            md.id_matricula ,
            md.id_disciplina ,
            md.id_matriculadisciplina ,
            dc.id_tipodisciplina ,
            st_nota = CASE WHEN dc.id_tipodisciplina = 1
                                AND aa.st_nota IS NOT NULL THEN aa.st_nota
                           WHEN dc.id_tipodisciplina = 2
                                AND tcc.st_nota IS NOT NULL THEN tcc.st_nota
                           ELSE NULL
                      END ,
            mt.st_nomecompleto ,
            lg.id_logacesso ,
            id_usuarioprofessor ,
            dt_encerramentoprofessor ,
            id_usuariocoordenador ,
            dt_encerramentocoordenador ,
            id_usuariopedagogico ,
            dt_encerramentopedagogico ,
            id_usuariofinanceiro ,
            dt_encerramentofinanceiro ,
            si.id_sistema ,
            sa.id_entidade ,
            ei.nu_perfilalunoencerrado ,
            es.dt_recusa ,
            es.st_motivorecusa ,
            es.id_usuariorecusa,
            id_upload = CASE WHEN dc.id_tipodisciplina = 1 THEN aa.id_upload
                             WHEN dc.id_tipodisciplina = 2 THEN tcc.id_upload
                             ELSE NULL
                        END ,
            aa.id_tiponota,
            sit.st_situacao as situacaotcc,
            sit.id_situacao as id_situacaotcc,
            dc.nu_cargahoraria
    FROM    dbo.tb_alocacao al
            JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula
            JOIN dbo.vw_matriculadisciplina AS md ON md.id_matriculadisciplina = al.id_matriculadisciplina
            JOIN dbo.tb_disciplina AS dc ON dc.id_disciplina = md.id_disciplina
            JOIN dbo.vw_matricula AS mt ON mt.id_matricula = md.id_matricula
                                           AND mt.bl_institucional = 0
                                           AND mt.id_evolucao IN ( 6, 15 )
            OUTER APPLY ( SELECT TOP 1
                                    id_logacesso
                          FROM      dbo.tb_logacesso
                          WHERE     id_saladeaula = al.id_saladeaula
                                    AND id_usuario = mt.id_usuario
                                    AND ( ( sa.id_tiposaladeaula = 2
                                            AND CAST(dt_cadastro AS DATE) <= sa.dt_encerramento
                                          )
                                          OR sa.id_tiposaladeaula = 1
                                        )
                        ) AS lg -- altera��o para AC-26484 
            LEFT JOIN dbo.vw_avaliacaoaluno AS aa ON aa.id_saladeaula = al.id_saladeaula
                                                     AND aa.id_matricula = mt.id_matricula
                                                     AND aa.bl_ativo = 1
                                                     AND aa.id_tipoavaliacao IN (
                                                     5 )
            LEFT JOIN dbo.vw_avaliacaoaluno AS tcc ON tcc.id_saladeaula = al.id_saladeaula
                                                      AND tcc.id_matricula = mt.id_matricula
                                                      AND tcc.bl_ativo = 1
                                                      AND tcc.id_tipoavaliacao IN (
                                                      6 )
            JOIN dbo.tb_encerramentoalocacao AS ea ON al.id_alocacao = ea.id_alocacao
            JOIN dbo.tb_encerramentosala AS es ON es.id_encerramentosala = ea.id_encerramentosala
            LEFT JOIN dbo.tb_saladeaulaintegracao AS si ON si.id_saladeaula = al.id_saladeaula
            LEFT JOIN dbo.tb_entidadeintegracao AS ei ON ei.id_entidade = sa.id_entidade
                                                         AND ei.id_sistema = 6
    WHERE   al.bl_ativo = 1


GO



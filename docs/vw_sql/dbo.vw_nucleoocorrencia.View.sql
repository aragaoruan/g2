
GO

/****** Object:  View [dbo].[vw_ocorrencia]    Script Date: 09/04/2013 10:13:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 CREATE VIEW [dbo].[vw_nucleoocorrencia] AS

SELECT DISTINCT 
	oc.id_ocorrencia,
	uso.st_nomecompleto AS st_nomeinteressado,
	oc.id_matricula,
	oc.id_evolucao,
	oc.id_situacao,
	oc.id_usuariointeressado,
	oc.id_categoriaocorrencia,
	oc.id_assuntoco,
	oc.id_saladeaula,
	oc.st_titulo,
	oc.dt_cadastro,
	oc.st_ocorrencia,
	oc.id_entidade,
	dbo.tb_situacao.st_situacao,
	dbo.tb_evolucao.st_evolucao,
	dbo.tb_assuntoco.st_assuntoco,
	dbo.tb_categoriaocorrencia.id_tipoocorrencia,
	dbo.tb_categoriaocorrencia.st_categoriaocorrencia,
	(CAST (CONVERT(VARCHAR,tm.dt_cadastro,103)+ ' �s ' + CONVERT(VARCHAR,tm.dt_cadastro,108) AS VARCHAR(max))) AS st_ultimotramite,
	tm.dt_cadastro AS dt_ultimotramite,
	oresp.id_usuario AS id_usuarioresponsavel,
	st_nomeresponsavel = CASE
				WHEN usr.id_usuario IS NULL 
					THEN 'N�o distribuido'			
					ELSE usr.st_nomecompleto		
					END,
	oc.dt_atendimento,
	asp.st_assuntoco AS st_assuntocopai,
	n.id_nucleoco,
	n.st_nucleoco,
	n.id_nucleofinalidade,
	nf.st_nucleofinalidade,
	(SELECT DISTINCT
		TOP 1 v.id_venda
		FROM 
			tb_ocorrencia AS o
			INNER JOIN tb_assuntoco         AS a  ON o.id_assuntoco  = a.id_assuntoco
			INNER JOIN tb_nucleoassuntoco   AS na ON na.id_assuntoco = a.id_assuntoco
			INNER JOIN tb_nucleoco          AS n  ON n.id_nucleoco   = na.id_nucleoco AND n.id_nucleofinalidade = 2
			INNER JOIN tb_matricula         AS m  ON m.id_matricula  = o.id_matricula
			INNER JOIN tb_vendaproduto      AS vp ON vp.id_matricula = m.id_matricula
			INNER JOIN tb_venda             AS v  ON v.id_venda      = vp.id_venda
		WHERE o.id_ocorrencia = oc.id_ocorrencia) AS id_venda
FROM
	tb_nucleoassuntoco AS na,
	tb_nucleoco AS n,
	tb_nucleofinalidade AS nf,
	dbo.tb_ocorrencia AS oc
	JOIN tb_usuario AS uso ON uso.id_usuario = oc.id_usuariointeressado
	INNER JOIN dbo.tb_situacao ON dbo.tb_situacao.id_situacao = oc.id_situacao
	INNER JOIN dbo.tb_evolucao ON dbo.tb_evolucao.id_evolucao = oc.id_evolucao
	INNER JOIN dbo.tb_categoriaocorrencia ON dbo.tb_categoriaocorrencia.id_categoriaocorrencia = oc.id_categoriaocorrencia
	INNER JOIN dbo.tb_assuntoentidadeco AS aec ON aec.id_entidade = oc.id_entidade
	INNER JOIN dbo.tb_assuntoco ON oc.id_assuntoco = dbo.tb_assuntoco.id_assuntoco AND dbo.tb_assuntoco.id_assuntoco = aec.id_assuntoco
	LEFT JOIN dbo.tb_assuntoco AS asp ON asp.id_assuntoco = dbo.tb_assuntoco.id_assuntocopai
	OUTER APPLY (SELECT TOP 1 id_tramite FROM dbo.tb_tramiteocorrencia WHERE id_ocorrencia = oc.id_ocorrencia ORDER BY id_tramiteocorrencia desc) AS trm
	LEFT JOIN dbo.tb_tramite       AS tm ON tm.id_tramite = trm.id_tramite
	LEFT JOIN dbo.tb_ocorrenciaresponsavel AS oresp ON oresp.id_ocorrencia = oc.id_ocorrencia AND oresp.bl_ativo = 1
	LEFT JOIN dbo.tb_usuario       AS usr ON usr.id_usuario = oresp.id_usuario
	--LEFT JOIN tb_nucleoassuntoco  AS na ON na.id_assuntoco = oc.id_assuntoco
	--LEFT JOIN tb_nucleoco         AS n  ON n.id_nucleoco = na.id_nucleoco
	--LEFT JOIN tb_nucleofinalidade AS nf ON nf.id_nucleofinalidade = n.id_nucleofinalidade

WHERE
	oc.id_assuntoco = na.id_assuntoco		AND
	n.id_nucleoco = na.id_nucleoco			AND
	nf.id_nucleofinalidade = n.id_nucleofinalidade

GO
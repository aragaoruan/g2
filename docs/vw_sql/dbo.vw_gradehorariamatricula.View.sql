CREATE VIEW [dbo].[vw_gradehorariamatricula] AS
SELECT
gh.id_gradehoraria,
gh.st_nomegradehoraria,
gh.dt_iniciogradehoraria,
gh.dt_fimgradehoraria,
it.id_itemgradehoraria,
it.id_unidade,
e.st_nomeentidade AS st_nomeunidade,
it.id_turno,
tn.st_turno,
it.id_diasemana,
ds.st_diasemana,
it.dt_diasemana,
it.id_disciplina,
dp.st_disciplina,
it.id_professor,
us.st_nomecompleto AS st_nomeprofessor,
it.id_turma,
tm.st_turma,
pp.id_projetopedagogico,
pp.st_projetopedagogico,
mt.id_matricula,
gh.dt_cadastro,
it.dt_cadastro AS dt_atualizacao,
it.nu_encontro,
ISNULL(
    mdc.nu_cargahoraria / (
SELECT
ce.nu_horasencontro
FROM
tb_configuracaoentidade AS ce
WHERE
ce.id_entidade = e.id_entidade
),
0
) AS nu_totalencontros,
(
SELECT
COUNT (igh.id_itemgradehoraria)
FROM
tb_itemgradehoraria AS igh
WHERE
igh.id_disciplina = dp.id_disciplina
AND igh.id_unidade = e.id_entidade
AND igh.id_projetopedagogico = pp.id_projetopedagogico
AND igh.bl_ativo = 1
) AS nu_encontros

FROM
dbo.tb_matricula AS mt
JOIN dbo.tb_itemgradehoraria AS it ON it.id_turma = mt.id_turma AND it.bl_ativo = 1
JOIN dbo.tb_gradehoraria AS gh ON gh.id_gradehoraria = it.id_gradehoraria AND gh.bl_ativo = 1 AND gh.id_situacao = 135
JOIN tb_usuario us ON us.id_usuario = it.id_professor
JOIN tb_entidade e ON e.id_entidade = it.id_unidade
JOIN tb_turno AS tn ON tn.id_turno = it.id_turno
JOIN tb_diasemana ds ON ds.id_diasemana = it.id_diasemana
JOIN tb_disciplina dp ON dp.id_disciplina = it.id_disciplina AND dp.bl_ativa = 1 AND dp.id_situacao = 9
JOIN tb_turma tm ON tm.id_turma = it.id_turma AND tm.bl_ativo = 1 AND tm.id_situacao = 83 AND tm.id_evolucao = 46
JOIN tb_projetopedagogico pp ON pp.id_projetopedagogico = it.id_projetopedagogico AND pp.bl_ativo = 1
JOIN tb_modulo AS md ON it.id_projetopedagogico = md.id_projetopedagogico AND md.bl_ativo = 1
JOIN tb_modulodisciplina AS mdc ON mdc.id_modulo = md.id_modulo AND mdc.bl_ativo = 1 and mdc.id_disciplina = dp.id_disciplina
WHERE
mt.id_evolucao = 6
AND mt.bl_ativo = 1
AND mt.id_situacao = 50

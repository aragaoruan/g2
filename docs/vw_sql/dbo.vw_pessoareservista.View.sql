
GO
/****** Object:  View [dbo].[vw_pessoareservista]    Script Date: 10/08/2012 17:42:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_pessoareservista] AS
select dr.id_entidade, dr.id_usuario, dr.st_numero, dr.st_serie, dr.dt_datareservista,
	   mu.id_municipio, mu.st_nomemunicipio, uf.sg_uf, uf.st_uf
from tb_documentodereservista as dr, tb_uf as uf, tb_municipio as mu, tb_pessoa as p
where dr.id_entidade = p.id_entidade and
	  dr.id_usuario = p.id_usuario and
	  dr.sg_uf = uf.sg_uf and
	  dr.id_municipio = mu.id_municipio
GO

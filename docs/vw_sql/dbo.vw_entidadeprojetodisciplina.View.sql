
GO
/****** Object:  View [dbo].[vw_entidadeprojetodisciplina]    Script Date: 10/08/2012 17:41:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_entidadeprojetodisciplina] AS
select d.id_disciplina, d.st_disciplina, d.nu_cargahoraria, d.st_descricao, d.id_situacao, sit.st_situacao, ad.id_areaconhecimento, pe.id_entidade, pp.id_entidadecadastro,ac.st_areaconhecimento, ac.bl_ativo,
	   ne.st_nivelensino, dsn.id_nivelensino, dsn.id_serie, s.st_serie, td.id_tipodisciplina, td.st_tipodisciplina
from tb_projetoentidade as pe
JOIN tb_projetopedagogico as pp ON pp.id_projetopedagogico = pe.id_projetopedagogico
JOIN tb_modulo as md ON pp.id_projetopedagogico = md.id_projetopedagogico
JOIN tb_modulodisciplina as mdc ON mdc.id_modulo = md.id_modulo
JOIN tb_disciplina as d ON mdc.id_disciplina = d.id_disciplina
LEFT JOIN tb_areadisciplina as ad ON ad.id_disciplina = d.id_disciplina
LEFT JOIN tb_areaconhecimento as ac ON ac.id_areaconhecimento = ad.id_areaconhecimento
LEFT JOIN tb_disciplinaserienivelensino as dsn ON dsn.id_disciplina = d.id_disciplina
LEFT JOIN tb_nivelensino as ne ON ne.id_nivelensino = dsn.id_nivelensino
LEFT JOIN tb_serie as s ON s.id_serie = dsn.id_serie
INNER JOIN tb_tipodisciplina as td ON td.id_tipodisciplina = d.id_tipodisciplina
LEFT JOIN tb_situacao as sit ON sit.id_situacao = d.id_situacao
GO

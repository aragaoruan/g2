
GO
/****** Object:  View [dbo].[vw_formameiopagamento]    Script Date: 10/08/2012 17:41:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_formameiopagamento] AS
SELECT fpar.id_formapagamentoaplicacao, mp.id_meiopagamento, mp.st_meiopagamento, mp.st_descricao AS st_descricaomeiopagamento, fp.* FROM tb_formapagamento fp
JOIN tb_formapagamentoaplicacaorelacao fpar ON fpar.id_formapagamento = fp.id_formapagamento 
JOIN tb_formameiopagamento fmp ON fp.id_formapagamento = fmp.id_formapagamento
JOIN tb_meiopagamento mp ON fmp.id_meiopagamento = mp.id_meiopagamento
GO

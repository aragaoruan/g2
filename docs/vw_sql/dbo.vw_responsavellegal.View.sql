
GO
/****** Object:  View [dbo].[vw_responsavellegal]    Script Date: 10/08/2012 17:42:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_responsavellegal] AS
select erl.id_entidaderesponsavellegal, p.st_nomeexibicao, erl.id_entidade, erl.bl_padrao, erl.id_situacao, s.st_situacao,
 erl.id_tipoentidaderesponsavel, terl.st_tipoentidaderesponsavel, erl.id_usuario, erl.st_registro,
 erl.sg_uf, erl.st_orgaoexpeditor
from tb_entidaderesponsavellegal erl, tb_tipoentidaderesponsavellegal terl, tb_situacao s, tb_pessoa p
where erl.id_tipoentidaderesponsavel = terl.id_tipoentidaderesponsavel and
		erl.id_situacao = s.id_situacao and
		erl.id_entidade = p.id_entidade and
		erl.id_usuario = p.id_usuario
GO

CREATE VIEW [rel].[vw_transacaofinanceira] AS
  SELECT
    v.id_atendente,
    v.id_entidade,
    p.st_nomecompleto AS st_nomeatendente,
    pp.st_nomecompleto AS st_nomealuno,
    pp.st_cpf AS st_cpf,
    tp.id_tipoproduto,
    tp.st_tipoproduto,
    CASE WHEN tf.id_statustransacao = 187
      THEN 'Solicitado'
    WHEN tf.id_statustransacao = 191
      THEN 'Enviado'
    WHEN tf.id_statustransacao = 189
      THEN 'Pago'
    ELSE 'Indefinido'
    END AS st_statuslink,
    tf.dt_cadastro AS dt_soliticacao,
    tf.id_mensagem,
    em.dt_envio,
    l.dt_vencimento,
    v.id_venda,
    l.id_lancamento,
    l.nu_valor,
    ed.id_enviodestinatario,
    ed.st_nome AS st_nomecompleto,
    ed.st_endereco
  FROM
    dbo.tb_venda v
    INNER JOIN dbo.vw_pessoa p ON v.id_atendente = p.id_usuario AND v.id_entidade = p.id_entidade
    INNER JOIN dbo.vw_pessoa pp ON v.id_usuario = pp.id_usuario AND v.id_entidade = pp.id_entidade
    INNER JOIN dbo.tb_vendaproduto vp ON vp.id_venda = v.id_venda
    INNER JOIN dbo.tb_produto prod ON prod.id_produto = vp.id_produto
    INNER JOIN dbo.tb_tipoproduto tp ON tp.id_tipoproduto = prod.id_tipoproduto
    INNER JOIN dbo.tb_lancamentovenda lv ON lv.id_venda = v.id_venda
    INNER JOIN dbo.tb_lancamento l ON lv.id_lancamento = l.id_lancamento
    INNER JOIN dbo.tb_transacaofinanceira tf ON tf.id_venda = v.id_venda AND
                                                l.id_transacaoexterna = tf.id_transacaoexterna
    LEFT JOIN dbo.tb_mensagem AS m ON m.id_mensagem = tf.id_mensagem
    LEFT JOIN dbo.tb_enviomensagem AS em ON m.id_mensagem = em.id_mensagem
    LEFT JOIN dbo.tb_enviodestinatario AS ed ON em.id_enviomensagem = ed.id_enviomensagem
  WHERE l.id_meiopagamento = 2

  
CREATE VIEW [rel].[vw_trancamentodisciplina] AS

SELECT
m.id_matricula,
u.st_nomecompleto,
u.st_cpf,
ce.st_email,
d.st_disciplina,
pp.st_projetopedagogico,
a.dt_trancamento,
sa.st_saladeaula,
ut.st_nomecompleto AS st_responsaveltrancamento,
-- Filtros
pp.id_projetopedagogico,
d.id_disciplina

FROM
  tb_alocacao AS a
  LEFT JOIN tb_matriculadisciplina AS md
    ON (a.id_matriculadisciplina = md.id_matriculadisciplina)
  LEFT JOIN tb_matricula AS m
    ON (md.id_matricula = m.id_matricula)
  LEFT JOIN tb_usuario AS u
    ON (m.id_usuario = u.id_usuario)
  LEFT JOIN tb_contatosemailpessoaperfil AS cepp
    ON (u.id_usuario = cepp.id_usuario
      AND cepp.bl_ativo     = 1
      AND cepp.bl_padrao    = 1
      AND cepp.id_entidade  = m.id_entidadematricula)
  JOIN tb_contatosemail AS ce
    ON (ce.id_email = cepp.id_email)
  LEFT JOIN tb_disciplina AS d
    ON (d.id_disciplina = md.id_disciplina)
  LEFT JOIN tb_usuario AS ut
    ON (a.id_usuariotrancamento = ut.id_usuario)
  LEFT JOIN tb_projetopedagogico AS pp
    ON (m.id_projetopedagogico  = pp.id_projetopedagogico)
  LEFT JOIN tb_saladeaula AS sa
    ON (sa.id_saladeaula = a.id_saladeaula)

WHERE a.bl_trancamento = 1;
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_avaliacaoparametrotcc] as
SELECT distinct aa.id_matricula ,
		aa.id_saladeaula,
        aa.id_disciplina ,
        aa.st_nota ,
        aa.id_avaliacao ,
        aa.id_avaliacaoaluno,
        aa.id_avaliacaoconjuntoreferencia ,
        aa.nu_notamax ,
        pt.bl_ativo AS bl_ativoparametro,
        aa.dt_avaliacao ,
        pt.st_parametrotcc ,
        pt.id_parametrotcc,
        pt.nu_peso,
        CAST(((pt.nu_peso * aa.nu_notamax) / 100) AS NUMERIC (5,2)) AS nu_notamaxparam ,
        ap.nu_valor AS nu_notaparametro FROM dbo.vw_avaliacaoaluno AS aa
JOIN dbo.tb_parametrotcc AS pt ON pt.id_projetopedagogico = aa.id_projetopedagogico AND pt.bl_ativo = 1
LEFT JOIN dbo.tb_avaliacaoparametros AS ap ON ap.id_avaliacaoaluno = aa.id_avaliacaoaluno AND pt.id_parametrotcc =ap.id_parametrotcc
WHERE aa.id_tipoavaliacao = 6

GO
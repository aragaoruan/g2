CREATE VIEW [dbo].[vw_produtovalortransferencia] AS
  SELECT
    DISTINCT
    pv.id_produtovalor,
    pro.id_produto,
    ppp.id_projetopedagogico,
    pp.st_projetopedagogico,
    pro.id_entidade,
    pro.id_situacao,
    pv.nu_valor,
    pv.nu_valormensal,
    pv.dt_inicio,
    pv.dt_termino
  FROM
    tb_projetopedagogico AS pp
    JOIN tb_produtoprojetopedagogico AS ppp ON ppp.id_projetopedagogico = pp.id_projetopedagogico
    JOIN tb_produto AS pro ON ppp.id_produto = pro.id_produto AND pro.id_tipoproduto = 1
    JOIN tb_entidade AS e ON pro.id_entidade = e.id_entidade
    JOIN tb_produtovalor AS pv ON pv.id_produto = pro.id_produto
    JOIN tb_tipoprodutovalor AS tpv ON tpv.id_tipoprodutovalor = pv.id_tipoprodutovalor
    JOIN tb_situacao AS s ON pro.id_situacao = s.id_situacao
    LEFT JOIN tb_projetopedagogicoserienivelensino AS ppsne ON ppsne.id_projetopedagogico = pp.id_projetopedagogico
    LEFT JOIN tb_nivelensino AS ne ON ne.id_nivelensino = ppsne.id_nivelensino
    LEFT JOIN tb_areaprojetopedagogico AS areapp ON areapp.id_projetopedagogico = pp.id_projetopedagogico
    LEFT JOIN tb_areaconhecimento AS areac ON areac.id_areaconhecimento = areapp.id_areaconhecimento

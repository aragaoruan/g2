
GO
/****** Object:  View [dbo].[vw_usuarioperfis]    Script Date: 10/08/2012 17:42:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_usuarioperfis] AS



--- View que busca os possiveis usuários para atribuir perfil---

SELECT DISTINCT us.st_nomecompleto, us.bl_ativo, pe.id_perfil, us.id_usuario FROM dbo.tb_usuario US
JOIN dbo.tb_pessoa PS ON ps.id_usuario = us.id_usuario
JOIN dbo.tb_perfilentidade PE ON pe.id_entidade = ps.id_entidade
GO

CREATE VIEW [dbo].[vw_cartacredito] as
SELECT
	cc.id_cartacredito,
	cancelamento.id_cancelamento,
	cc.dt_cadastro,
	cc.nu_valororiginal,
	(SELECT
		ISNULL(CONVERT(FLOAT, SUM(utilizado.nu_valorutilizado)), 0)
	FROM tb_vendacartacredito AS utilizado
	JOIN tb_venda utilizadovenda
		ON utilizadovenda.id_venda = utilizado.id_venda
	WHERE utilizadovenda.id_evolucao NOT IN (7, 8)
	AND utilizado.id_cartacredito = cc.id_cartacredito)
	AS nu_valorutilizado,
	(cc.nu_valororiginal - (SELECT
		ISNULL(CONVERT(FLOAT, SUM(disponivel.nu_valorutilizado)), 0)
	FROM tb_vendacartacredito AS disponivel
	JOIN tb_venda disponivelvenda
		ON disponivelvenda.id_venda = disponivel.id_venda
	WHERE disponivelvenda.id_evolucao NOT IN (7, 8)
	AND disponivel.id_cartacredito = cc.id_cartacredito)
	) AS nu_valordisponivel,
	cc.id_usuariocadastro,
	cc.id_usuario,
	cc.bl_ativo,
	cc.id_situacao,
	s.st_situacao,
	cc.id_entidade,
  cc.dt_validade,
	e.st_nomeentidade
FROM tb_cartacredito AS cc
JOIN tb_situacao s
	ON s.id_situacao = cc.id_situacao
JOIN tb_entidade e
	ON e.id_entidade = cc.id_entidade
LEFT JOIN tb_cancelamento cancelamento
  on cancelamento.id_cancelamento = cc.id_cancelamento

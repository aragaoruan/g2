
GO
/****** Object:  View [dbo].[vw_entidadecontratoresponsavel]    Script Date: 10/08/2012 17:41:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_entidadecontratoresponsavel] as 
SELECT 
cr.id_contratoresponsavel,cr.id_entidaderesponsavel, et.st_razaosocial, cr.id_contrato, id_tipocontratoresponsavel, cr.nu_porcentagem, cr.bl_ativo, 
vd.id_venda
FROM tb_contratoresponsavel as cr
JOIN tb_entidade as et on et.id_entidade = cr.id_entidaderesponsavel
JOIN tb_contrato as ct ON ct.id_contrato = cr.id_contrato
LEFT JOIN tb_venda as vd ON vd.id_venda = ct.id_venda
GO

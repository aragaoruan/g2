
GO
/****** Object:  View [dbo].[vw_pesquisarprofessor]    Script Date: 10/08/2012 17:42:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vw_pesquisarprofessor] as 
select p.id_usuario, p.id_entidade, p.st_nomeexibicao, p.id_situacao, u.st_nomecompleto, e.st_nomeentidade, 
	   per.id_perfil,per.st_nomeperfil, pp.id_perfilpedagogico, pp.st_perfilpedagogico, upe.id_entidadeidentificacao
from tb_pessoa as p
INNER JOIN tb_usuario as u ON u.id_usuario = p.id_usuario
INNER JOIN tb_entidade as e ON e.id_entidade = p.id_entidade
INNER JOIN tb_usuarioperfilentidade as upe ON upe.id_entidadeidentificacao = e.id_entidade and upe.id_usuario = u.id_usuario
INNER JOIN tb_perfil as per ON per.id_perfil = upe.id_perfil and per.id_entidade = p.id_entidade
INNER JOIN tb_perfilpedagogico as pp ON pp.id_perfilpedagogico = per.id_perfilpedagogico
GO

CREATE VIEW [dbo].[vw_sincronizaralunosmoodle] AS
SELECT
	al.id_alocacao,
	sa.st_saladeaula,
	usr.st_nomecompleto,
	sai.id_saladeaula,
	sai.st_codsistemacurso,
	mt.id_usuario,
	sa.id_entidade
FROM
	dbo.tb_alocacao AS al
JOIN dbo.tb_saladeaulaintegracao AS sai ON sai.id_sistema = 6 AND al.id_saladeaula = sai.id_saladeaula
JOIN dbo.tb_matriculadisciplina AS md ON md.id_matriculadisciplina = al.id_matriculadisciplina
JOIN dbo.tb_matricula AS mt ON mt.id_matricula = md.id_matricula AND mt.bl_ativo = 1 AND mt.id_situacao = 50
JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula
JOIN dbo.tb_usuario AS usr ON usr.id_usuario = mt.id_usuario AND usr.bl_ativo = 1
JOIN tb_contatosemailpessoaperfil AS pessoaemail ON pessoaemail.id_usuario = usr.id_usuario
AND pessoaemail.id_entidade = sa.id_entidade and pessoaemail.bl_ativo = 1 and pessoaemail.bl_padrao = 1
JOIN tb_contatosemail AS email ON email.id_email = pessoaemail.id_email AND email.st_email IS NOT NULL
WHERE
	al.id_alocacao NOT IN (
		SELECT
			id_alocacao
		FROM
			dbo.tb_alocacaointegracao
		WHERE
			id_alocacao = al.id_alocacao
	)
AND al.bl_ativo = 1
AND DATEDIFF(
	DAY,
	al.dt_cadastro,
	getdate()
) < 90
GO
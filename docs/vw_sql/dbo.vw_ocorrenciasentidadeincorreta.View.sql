CREATE VIEW dbo.vw_ocorrenciasentidadeincorreta AS

SELECT
  oc.id_ocorrencia,
  oc.id_entidade AS id_entidadeocorrencia,
  nc.id_entidade AS id_entidadenucleoco

FROM
  tb_ocorrencia AS oc
  JOIN tb_assuntoco AS ac ON ac.id_assuntoco = oc.id_assuntoco AND ac.id_tipoocorrencia = 2 AND ac.bl_ativo = 1
  JOIN tb_nucleoassuntoco AS na ON na.id_assuntoco = ac.id_assuntoco AND na.bl_ativo = 1
  JOIN tb_nucleoco AS nc ON nc.id_nucleoco = na.id_nucleoco AND nc.bl_ativo = 1
    AND nc.id_entidade != oc.id_entidade -- Ocorrências incorretas são as que contém entidade diferente do núcleo

WHERE
  oc.id_situacao = 97 -- Pendentes
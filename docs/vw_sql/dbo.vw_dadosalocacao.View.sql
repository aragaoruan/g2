
GO
/****** Object:  View [dbo].[vw_dadosalocacao]    Script Date: 10/08/2012 17:41:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vw_dadosalocacao] as
select
mt.id_matricula, pp.id_projetopedagogico, tl.nu_disciplinassimultaneasmai, tl.nu_disciplinassimultaneasmen, tl.nu_disciplinaspendencia as nu_disciplinassimultaneaspen,COUNT (id_alocacao)as nu_disciplinasalocadas
from tb_matricula as mt
LEFT JOIN tb_matriculadisciplina as md ON md.id_matricula = mt.id_matricula and md.id_evolucao = 13
LEFT JOIN tb_alocacao as al on al.id_matriculadisciplina = md.id_matriculadisciplina and al.bl_ativo = 1
JOIN tb_projetopedagogico as pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
JOIN tb_trilha as tl ON tl.id_trilha = pp.id_trilha
group by mt.id_matricula, pp.id_projetopedagogico, tl.nu_disciplinassimultaneasmai, tl.nu_disciplinassimultaneasmen, tl.nu_disciplinaspendencia
GO

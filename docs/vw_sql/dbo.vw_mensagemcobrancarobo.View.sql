USE [G2_UNY]
GO

/****** Object:  View [dbo].[vw_mensagemcobrancarobo]    Script Date: 30/05/2018 05:32:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[vw_mensagemcobrancarobo] as
SELECT  v.id_venda ,
        v.nu_valorliquido ,
        v.id_usuario,
        l.id_lancamento ,
        l.dt_vencimento ,
        mc.nu_diasatraso ,
        mc.id_textosistema ,
        mc.st_textosistema,
        mc.id_entidade
FROM    tb_lancamento AS l
        JOIN vw_mensagemcobranca AS mc ON mc.id_entidade = l.id_entidade
                                          AND mc.nu_diasatraso = DATEDIFF(DAY,
                                                              l.dt_vencimento,
                                                              GETDATE())
                                          AND mc.bl_ativo = 1
                                          AND l.bl_ativo = 1
        JOIN tb_lancamentovenda AS lv ON lv.id_lancamento = l.id_lancamento
        JOIN tb_venda AS v ON v.id_venda = lv.id_venda
WHERE   l.dt_vencimento < GETDATE() AND ((select count(id_processo) from tb_processamento where id_processo = 2 and cast(dt_processamento as DATE) = CAST(getdate() as DATE)) = 0)  and l.bl_quitado = 0
GO



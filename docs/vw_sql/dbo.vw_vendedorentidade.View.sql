CREATE VIEW dbo.vw_vendedorentidade AS (
  SELECT DISTINCT
    v.id_vendedor,
    v.st_vendedor,
    v.bl_ativo,
    er.id_entidade,
    v.id_entidade AS id_entidadevendedor
  FROM tb_vendedor AS v
    -- Os vendedores devem ser buscados tanto da entidade em que foram cadastrados, quanto da entidade PAI (apenas o primeiro pai).
    JOIN vw_entidaderecursiva AS er ON v.id_entidade IN(er.id_entidade, er.id_entidadepai)
)
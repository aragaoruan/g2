CREATE VIEW [dbo].[vw_produtospremio] AS
SELECT
	pp.id_premioproduto,
	pp.id_usuariocadastro,
	pp.id_produto,
	pd.st_produto,
	pp.id_campanhacomercial,
	pd.st_tipoproduto,
	pd.id_tipoproduto
FROM
	tb_premioproduto as pp
join vw_produto as pd on pd.id_produto = pp.id_produto and pd.bl_ativo = 1
WHERE
	pp.bl_ativo = 1
GO



SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[vw_turmatotalcargahoraria] as
SELECT
	tp.id_projetopedagogico,
	td.id_turma,
	td.st_turma,
	sum(d.nu_cargahoraria) as nu_totalcargahoraria
FROM tb_turmaprojeto tp
join vw_turmadisciplina td on td.id_turma = tp.id_turma
join tb_disciplina d on d.id_disciplina = td.id_disciplina
WHERE tp.bl_ativo = 1
	and d.bl_ativa = 1
 GROUP BY td.id_turma, td.st_turma, tp.id_projetopedagogico;
 GO

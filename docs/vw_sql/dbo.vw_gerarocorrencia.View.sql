
/****** Object:  View [dbo].[vw_gerarocorrencia]    Script Date: 10/08/2012 17:41:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_gerarocorrencia] AS
SELECT us.st_nomecompleto, oc.id_ocorrencia ,
        oc.id_matricula ,
        oc.id_evolucao ,
        oc.id_situacao ,
        oc.id_usuariointeressado ,
        oc.id_categoriaocorrencia ,
        oc.id_assuntoco ,
        oc.id_saladeaula ,
        oc.st_titulo ,
        oc.st_ocorrencia ,
        oc.st_situacao ,
        oc.st_evolucao ,
        oc.st_assuntoco ,
        oc.st_categoriaocorrencia ,
        oc.st_ultimotramite ,
        oc.dt_ultimotramite,oc.st_ocorrencia, oc.st_nomeresponsavel FROM
dbo.vw_ocorrencia AS oc
JOIN dbo.tb_usuario AS us ON us.id_usuario = oc.id_usuariointeressado
GO

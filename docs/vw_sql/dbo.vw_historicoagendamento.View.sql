CREATE VIEW [dbo].[vw_historicoagendamento] AS (

  SELECT
    aa.id_avaliacaoagendamento,
    aa.id_matricula,
    aa.id_tipodeavaliacao,
    (CASE
      WHEN aa.id_tipodeavaliacao = 0 THEN 'Prova Final'
      ELSE 'Recuperação' + ' ' + CAST(aa.id_tipodeavaliacao AS CHAR)
    END) AS st_tipodeavaliacao,
    t.id_tramite,
    t.st_tramite,
    s.id_situacao,
    s.st_situacao,
    t.bl_visivel,
    t.dt_cadastro,
    t.id_entidade,
    aa.bl_automatico,
    t.id_usuario,
    (CASE
      WHEN aa.bl_automatico = 0 THEN us.st_nomecompleto
      ELSE 'Agendamento automático'
    END) AS st_nomecompleto,
    YEAR(aa.dt_cadastro) AS id_ano

    FROM tb_avaliacaoagendamento AS aa
    JOIN tb_tramiteagendamento AS ta ON aa.id_avaliacaoagendamento = ta.id_avaliacaoagendamento
    JOIN tb_tramite AS t ON ta.id_tramite = t.id_tramite AND t.id_tipotramite =  14
    JOIN tb_tipotramite AS tt ON tt.id_tipotramite = t.id_tipotramite AND tt.id_categoriatramite = 5
    JOIN tb_situacao AS s ON ta.id_situacao = s.id_situacao
    JOIN tb_usuario AS us ON t.id_usuario = us.id_usuario

)
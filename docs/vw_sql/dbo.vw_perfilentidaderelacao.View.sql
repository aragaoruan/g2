GO
/****** Object:  View [dbo].[vw_perfilentidaderelacao]    Script Date: 10/08/2012 17:42:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_perfilentidaderelacao] as 
 SELECT DISTINCT e.id_entidade, e.st_nomeentidade, e.bl_ativo, er.id_entidadepai, ec.id_entidadeclasse, ec.st_entidadeclasse, s.id_situacao, s.st_situacao, p.id_perfil, p.st_nomeperfil
 FROM tb_entidade e,
 tb_entidaderelacao er,
 tb_entidadeclasse ec,
 tb_situacao s,
 tb_perfil p
WHERE e.id_entidade = er.id_entidade
  AND ec.id_entidadeclasse = er.id_entidadeclasse
  AND e.id_situacao = s.id_situacao
  AND p.id_entidade = e.id_entidade
UNION
  SELECT DISTINCT e.id_entidade,
  e.st_nomeentidade,
  e.bl_ativo,
  er.id_entidadepai,
  ec.id_entidadeclasse,
  ec.st_entidadeclasse,
  s.id_situacao,
  s.st_situacao,
  p.id_perfil,
  p.st_nomeperfil
  FROM tb_entidade e,
  tb_entidaderelacao er,
  tb_entidadeclasse ec,
  tb_situacao s,
  tb_perfil p,
  tb_perfilentidade pe
  WHERE e.id_entidade = er.id_entidade
  AND ec.id_entidadeclasse = er.id_entidadeclasse
  AND e.id_situacao = s.id_situacao
  AND pe.id_entidade = e.id_entidade
  AND p.id_perfil = pe.id_perfil
GO

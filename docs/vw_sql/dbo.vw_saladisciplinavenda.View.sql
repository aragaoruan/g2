CREATE VIEW [dbo].[vw_saladisciplinavenda] AS
  WITH _config AS (
      SELECT
        0 AS FALSE,
        1 AS TRUE,
        12 AS EVOLUCAO_MAT_DISC_APROVADO,
        13 AS EVOLUCAO_MAT_DISC_CURSANDO,
        54 AS SITUACAO_MAT_DISC_ATIVA,
        55 AS SITUACAO_ALOCACAO_ATIVA,
        65 AS SITUACAO_APROVEITAMENTO_DISCIPLINA
  )

  SELECT
    DISTINCT
    pmd.id_saladeaula,
    sa.st_saladeaula,
    sa.nu_maxalunos,
    (SELECT
       TOP 1
       id_projetopedagogico
     FROM tb_produtoprojetopedagogico AS ppp
     WHERE
       ppp.id_produto = vp.id_produto
       AND ppp.id_entidade = v.id_entidade
    ) AS id_projetopedagogico,
    mtd.id_matricula,
    mtd.id_disciplina,
    mtd.id_matriculadisciplina,
    v.id_usuario,
-- Este campo informa se já possui uma alocação para a mesma disciplina que está ativa
    al.id_alocacao,
    al.id_saladeaula AS id_salaalocacao,
    pmd.bl_alocado,
    v.id_entidade,
    (SELECT
       COUNT(al2.id_alocacao)
     FROM tb_alocacao AS al2
     WHERE
       al2.id_saladeaula = sa.id_saladeaula
       AND al2.bl_ativo = _.TRUE
       AND id_situacao = _.SITUACAO_ALOCACAO_ATIVA
    ) AS nu_alocados,
    pmd.id_venda,
    sa.id_situacao,
    v.id_evolucao AS id_evolucaovenda
  FROM
    tb_matriculadisciplina AS mtd
    CROSS APPLY _config AS _
    JOIN tb_vendaproduto AS vp ON vp.id_matricula = mtd.id_matricula
    JOIN tb_venda AS v ON v.id_venda = vp.id_venda
    JOIN tb_prematriculadisciplina AS pmd ON mtd.id_disciplina = pmd.id_disciplina AND vp.id_venda = pmd.id_venda
    JOIN tb_saladeaula AS sa ON sa.id_saladeaula = pmd.id_saladeaula
-- Buscar somente a última alocação ativa para evitar duplicidade de agendamento
    OUTER APPLY
    (
      SELECT
        TOP 1
        id_alocacao,
        id_saladeaula
      FROM
        tb_alocacao AS al2
        JOIN tb_matriculadisciplina AS md2
          ON md2.id_matriculadisciplina = al2.id_matriculadisciplina
             AND md2.id_situacao = _.SITUACAO_MAT_DISC_ATIVA
             AND md2.id_evolucao IN (_.EVOLUCAO_MAT_DISC_APROVADO, _.EVOLUCAO_MAT_DISC_CURSANDO)
      WHERE
        al2.id_matriculadisciplina = mtd.id_matriculadisciplina
        AND al2.bl_ativo = _.TRUE
        AND al2.id_situacao = _.SITUACAO_ALOCACAO_ATIVA
        AND al2.id_categoriasala = sa.id_categoriasala
      ORDER BY
        dt_cadastro DESC
    ) AS al
  WHERE
-- Ignorar alocacoes com APROVEITAMENTO DE DISCIPLINA
    mtd.id_situacao != _.SITUACAO_APROVEITAMENTO_DISCIPLINA

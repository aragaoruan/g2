
/****** Object:  View [dbo].[vw_simuladousuario]    Script Date: 23/9/2013 12:19:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 CREATE VIEW [dbo].[vw_simuladousuario] as
SELECT ta.id_usuario, ta.dt_agendamento,ta2.st_avaliacao,ts.id_entidade, ts.st_caminho , tu2.st_senhaintegrada AS st_senha, tu2.st_loginintegrado AS st_login, ti.st_codsistema AS st_codigo
FROM dbo.tb_avaliacaoagendamento AS ta
JOIN dbo.tb_avaliacao AS ta2 ON ta.id_avaliacao = ta2.id_avaliacao AND ta2.id_tipoavaliacao = 3
JOIN tb_avaliacaointegracao AS ti ON ti.id_avaliacao = ta2.id_avaliacao
JOIN dbo.tb_usuario AS tu ON tu.id_usuario = ta.id_usuario
JOIN dbo.tb_entidadeintegracao AS ts ON ts.id_sistema = 11 AND ts.id_entidade = ta.id_entidade
LEFT JOIN dbo.tb_usuariointegracao AS tu2 ON tu.id_usuario = tu2.id_usuario AND tu2.id_sistema = 11 AND ta.id_entidade = tu2.id_entidade


GO



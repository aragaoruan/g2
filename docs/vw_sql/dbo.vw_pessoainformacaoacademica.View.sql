
GO
/****** Object:  View [dbo].[vw_pessoainformacaoacademica]    Script Date: 10/08/2012 17:42:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_pessoainformacaoacademica] AS
select infaps.id_entidade, infaps.id_usuario, infaps.st_curso, 
	   infaps.st_nomeinstituicao, ne.id_nivelensino, ne.st_nivelensino
from tb_informacaoacademicapessoa as infaps
	INNER JOIN tb_nivelensino as ne ON ne.id_nivelensino = infaps.id_nivelensino
	INNER JOIN tb_pessoa as p ON p.id_usuario = infaps.id_usuario and p.id_entidade = infaps.id_entidade
GO

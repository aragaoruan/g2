CREATE VIEW vw_disciplinasprr  AS
select distinct mt.id_matricula ,
        md.id_disciplina ,
        st_tituloexibicaodisciplina ,
	dc.st_disciplina,
        st_nota ,
        sdp.nu_valor ,
        sdp.id_vendaproduto ,
        sdp.dt_entrega ,
        sdp.id_produto ,
        cast(aa.dt_cadastrosala as DATETIME) as dt_cadastrosala,
        aa.dt_encerramentosala,
        aa.id_saladeaula,
        vd.id_venda ,
        oc.id_ocorrencia,
        aa.st_projetopedagogico,
        oc.id_usuariointeressado as id_usuario,
        sdp3.st_saladeaula as st_saladeaulaprr,
		bl_cursandoprr = CASE WHEN sdp2.id_alocacao IS NOT NULL THEN 1
		WHEN sdp2.id_alocacao IS NULL THEN 0
		ELSE 1
		END
		  FROM dbo.tb_matricula AS mt
JOIN dbo.tb_matriculadisciplina AS md ON mt.id_matricula = md.id_matricula
JOIN dbo.vw_avaliacaoaluno AS aa ON mt.id_matricula = aa.id_matricula AND aa.id_matriculadisciplina = md.id_matriculadisciplina AND id_tipoavaliacao = 5
JOIN dbo.tb_ocorrencia AS oc ON oc.id_matricula = mt.id_matricula
JOIN dbo.tb_disciplina AS dc ON dc.id_disciplina = md.id_disciplina
LEFT JOIN dbo.tb_venda AS vd ON vd.id_ocorrencia = oc.id_ocorrencia
LEFT JOIN dbo.vw_saladisciplinaprr AS sdp ON sdp.id_disciplina = md.id_disciplina AND mt.id_matricula = sdp.id_matricula AND sdp.id_ocorrencia = oc.id_ocorrencia AND vd.id_venda = sdp.id_venda AND sdp.id_vendaproduto IS NOT NULL
LEFT JOIN dbo.vw_saladisciplinaprr AS sdp2 ON sdp2.id_disciplina = md.id_disciplina AND mt.id_matricula = sdp2.id_matricula AND sdp2.id_alocacao IS NOT null
LEFT JOIN tb_produtoprr as pprr on pprr.id_saladeaula = sdp.id_saladeaula
LEFT JOIN dbo.vw_saladisciplinaprr as sdp3 on sdp3.id_saladeaula = pprr.id_saladeaula;

SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vw_categoria]
AS
    SELECT
  c.id_categoria,
  c.st_nomeexibicao,
  c.st_categoria,
  c.id_usuariocadastro,
  ce.id_entidade AS id_entidadecadastro,
  c.id_uploadimagem,
  c.id_categoriapai,
  c.id_situacao,
  c.dt_cadastro,
  c.bl_ativo,
  cp.id_produto,
  pr.st_produto
FROM dbo.tb_categoria AS c
LEFT OUTER JOIN dbo.tb_categoriaproduto AS cp
  ON c.id_categoria = cp.id_categoria
LEFT OUTER JOIN dbo.tb_produto AS pr
  ON cp.id_produto = pr.id_produto
INNER JOIN dbo.tb_categoriaentidade AS ce
  ON ce.id_categoria = c.id_categoria
GO

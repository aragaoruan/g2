SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_campanhaproduto] as
select
cc.id_campanhacomercial, cc.st_campanhacomercial, cp.id_produto, pd.st_produto, pd.id_tipoproduto, tp.st_tipoproduto
from tb_campanhacomercial as cc
JOIN tb_campanhaproduto as cp ON cp.id_campanhacomercial = cc.id_campanhacomercial
JOIN tb_produto as pd ON pd.id_produto = cp.id_produto
JOIN tb_tipoproduto as tp ON tp.id_tipoproduto = pd.id_tipoproduto
GO

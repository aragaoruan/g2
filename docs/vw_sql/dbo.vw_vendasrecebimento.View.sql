CREATE VIEW [dbo].[vw_vendasrecebimento] AS
  SELECT
    vd.id_usuario,
    us.st_nomecompleto,
    vd.id_venda,
    vd.id_entidade,
    fpa.id_formapagamentoaplicacao
  FROM tb_venda AS vd
    LEFT JOIN dbo.tb_formapagamento AS fp ON fp.id_formapagamento = vd.id_formapagamento
    LEFT JOIN dbo.tb_formapagamentoaplicacaorelacao fpa ON fpa.id_formapagamento = fp.id_formapagamento
    JOIN dbo.tb_entidadefinanceiro AS ef ON ef.id_entidade = vd.id_entidade
    --AND (ef.bl_automatricula = 1 OR fpa.id_formapagamentoaplicacao = 1)
    LEFT JOIN vw_entidadeesquemaconfiguracao eec ON eec.id_entidade = vd.id_entidade AND eec.id_itemconfiguracao = 14
    JOIN dbo.tb_usuario AS us ON us.id_usuario = vd.id_usuario
    JOIN tb_pessoa AS ps ON ps.id_usuario = us.id_usuario AND ps.id_entidade = vd.id_entidade AND ps.bl_ativo = 1
    LEFT JOIN dbo.tb_lancamentovenda AS lv ON lv.id_venda = vd.id_venda AND lv.bl_entrada = 1
    LEFT JOIN dbo.tb_lancamento AS lc ON lc.id_lancamento = lv.id_lancamento
  WHERE vd.id_evolucao = 9
        AND vd.bl_ativo = 1
        AND DATEDIFF(DAY, vd.dt_cadastro, GETDATE()) < 50
        AND ((lc.bl_quitado = 1
              OR lc.dt_prevquitado IS NOT NULL)
             OR vd.nu_valorliquido = 0)
        AND (eec.st_valor = 1 OR fpa.id_formapagamentoaplicacao = 1) AND vd.bl_renovacao = 0


  UNION SELECT
          vd.id_usuario,
          us.st_nomecompleto,
          vd.id_venda,
          vd.id_entidade,
          fpa.id_formapagamentoaplicacao
        FROM tb_venda AS vd
          LEFT JOIN dbo.tb_formapagamento AS fp ON fp.id_formapagamento = vd.id_formapagamento
          LEFT JOIN dbo.tb_formapagamentoaplicacaorelacao fpa ON fpa.id_formapagamento = fp.id_formapagamento
          JOIN dbo.tb_entidadefinanceiro AS ef ON ef.id_entidade = vd.id_entidade
          -- AND ef.bl_automatricula = 1
          LEFT JOIN vw_entidadeesquemaconfiguracao eec
            ON eec.id_entidade = vd.id_entidade AND eec.id_itemconfiguracao = 14 AND eec.st_valor = 1
          JOIN dbo.tb_usuario AS us ON us.id_usuario = vd.id_usuario
          JOIN tb_pessoa AS ps ON ps.id_usuario = us.id_usuario AND ps.id_entidade = vd.id_entidade AND ps.bl_ativo = 1
          LEFT JOIN dbo.tb_lancamentovenda AS lv ON lv.id_venda = vd.id_venda AND lv.bl_entrada = 1
          LEFT JOIN dbo.tb_lancamento AS lc ON lc.id_lancamento = lv.id_lancamento
          JOIN tb_campanhacomercial AS tc
            ON vd.id_campanhacomercial = tc.id_campanhacomercial AND tc.id_categoriacampanha = 3
        WHERE vd.id_evolucao = 9
              AND vd.bl_ativo = 1
              AND DATEDIFF(DAY, vd.dt_cadastro, GETDATE()) < 50 AND vd.bl_renovacao = 0

  UNION

  SELECT DISTINCT
    vd.id_usuario,
    us.st_nomecompleto,
    vd.id_venda,
    vd.id_entidade,
    fpa.id_formapagamentoaplicacao
  FROM dbo.tb_venda AS vd
    LEFT JOIN dbo.tb_formapagamento AS fp ON fp.id_formapagamento = vd.id_formapagamento
    LEFT JOIN dbo.tb_formapagamentoaplicacaorelacao fpa ON fpa.id_formapagamento = fp.id_formapagamento
    JOIN dbo.tb_vendaproduto AS vp ON vd.id_venda = vp.id_venda
    JOIN dbo.tb_produto AS pd ON vp.id_produto = pd.id_produto AND pd.id_tipoproduto = 1
    JOIN dbo.tb_produtoprojetopedagogico AS pp ON pp.id_produto = pd.id_produto
    JOIN dbo.tb_contrato AS ct ON ct.id_venda = vd.id_venda AND ct.id_contrato NOT IN (SELECT id_contrato
                                                                                       FROM dbo.tb_contratomatricula)
    JOIN tb_usuario AS us ON us.id_usuario = vd.id_usuario
    JOIN tb_pessoa AS ps ON ps.id_usuario = us.id_usuario AND ps.id_entidade = vd.id_entidade AND ps.bl_ativo = 1
    LEFT JOIN dbo.tb_lancamentovenda AS lv ON lv.id_venda = vd.id_venda AND lv.bl_entrada = 1
    LEFT JOIN dbo.tb_lancamento AS lc ON lc.id_lancamento = lv.id_lancamento
  WHERE vd.bl_ativo = 1
        AND DATEDIFF(DAY, vd.dt_cadastro, GETDATE()) < 50
        AND ((lc.id_lancamento IS NULL
              OR lc.bl_quitado = 1
              OR lc.dt_prevquitado IS NOT NULL)
             OR vd.nu_valorliquido = 0)
        AND vd.dt_cadastro < DATEADD(MINUTE, -60, GETDATE())
        AND vd.id_evolucao IN (10) AND vd.bl_renovacao = 0

  UNION

  SELECT DISTINCT
    vd.id_usuario,
    us.st_nomecompleto,
    vd.id_venda,
    vd.id_entidade,
    fpa.id_formapagamentoaplicacao
  FROM dbo.tb_venda AS vd
    LEFT JOIN dbo.tb_formapagamento AS fp ON fp.id_formapagamento = vd.id_formapagamento
    LEFT JOIN dbo.tb_formapagamentoaplicacaorelacao fpa ON fpa.id_formapagamento = fp.id_formapagamento
    JOIN dbo.tb_vendaproduto AS vp ON vd.id_venda = vp.id_venda
    JOIN dbo.tb_produto AS pd ON vp.id_produto = pd.id_produto
    JOIN tb_usuario AS us ON us.id_usuario = vd.id_usuario
    JOIN tb_pessoa AS ps ON ps.id_usuario = us.id_usuario AND ps.id_entidade = vd.id_entidade AND ps.bl_ativo = 1
    LEFT JOIN dbo.tb_lancamentovenda AS lv ON lv.id_venda = vd.id_venda AND lv.bl_entrada = 1
    LEFT JOIN dbo.tb_lancamento AS lc ON lc.id_lancamento = lv.id_lancamento
  WHERE vd.bl_ativo = 1
        AND DATEDIFF(DAY, vd.dt_cadastro, GETDATE()) < 50
        AND ((lc.id_lancamento IS NULL
              OR lc.bl_quitado = 1
              OR lc.dt_prevquitado IS NOT NULL)
             OR vd.nu_valorliquido = 0)
        AND vd.dt_cadastro < DATEADD(MINUTE, -60, GETDATE())
        AND vd.id_evolucao IN (44) AND vd.bl_renovacao = 0

GO
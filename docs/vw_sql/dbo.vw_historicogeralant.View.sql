
GO
/****** Object:  View [dbo].[vw_historicogeralant]    Script Date: 10/08/2012 17:42:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create VIEW [dbo].[vw_historicogeralant] as
select
mt.id_matricula, mt.id_projetopedagogico, pp.st_tituloexibicao, mdd.id_serie, se.st_serie, se.id_serieanterior, mdd.id_areaconhecimento, ac.st_tituloexibicao as st_areaconhecimento, ac.id_tipoareaconhecimento
,mtd.id_disciplina, dc.st_tituloexibicao AS st_disciplina, mtd.nu_aprovafinal, pp.nu_notamaxima, mtd.dt_conclusao as dt_conclusaodisciplina ,dc.nu_cargahoraria ,mtd.id_evolucao, mtd.id_situacao
, apm.st_cargahoraria as st_cargahorariaaproveitamento, apm.st_notaoriginal as st_notaoriginalaproveitamento, apd.st_instituicao as st_instituicaoaproveitamento, apd.sg_uf as sg_ufaproveitamento, mn.st_nomemunicipio as st_nomemunicipioaproveitamento ,CAST(cast(apm.nu_anoconclusao AS VARCHAR(4)) + '-01-01' AS date) as dt_conclusaoaproveitamento
, ent.st_nomeentidade, ed.sg_uf, mun.st_nomemunicipio, mdd.nu_ordem, ap.st_tituloexibicao AS st_areaagregadora, ap.id_areaconhecimento AS id_areaagregadora
from tb_matricula as mt
JOIN tb_projetopedagogico as pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_modulo as md ON md.id_projetopedagogico = mt.id_projetopedagogico
JOIN tb_modulodisciplina as mdd on mdd.id_modulo = md.id_modulo
JOIN tb_matriculadisciplina as mtd ON mtd.id_matricula = mt.id_matricula
LEFT JOIN tb_aproveitamentomatriculadisciplina as apm ON apm.id_matricula = mtd.id_matricula and apm.id_disciplina = mtd.id_disciplina
LEFT JOIN tb_aproveitamentodisciplina as apd ON apd.id_aproveitamentodisciplina = apm.id_aproveitamentodisciplina
JOIN tb_disciplina as dc ON dc.id_disciplina = mtd.id_disciplina and dc.id_disciplina = mdd.id_disciplina
JOIN tb_serie as se ON se.id_serie = mdd.id_serie
JOIN tb_nivelensino as ne ON ne.id_nivelensino = mdd.id_nivelensino
JOIN tb_areaconhecimento as ac ON ac.id_areaconhecimento = mdd.id_areaconhecimento
LEFT JOIN tb_municipio as mn ON mn.id_municipio = apd.id_municipio
JOIN tb_entidade as ent ON ent.id_entidade = mt.id_entidadematriz
JOIN tb_entidadeendereco as eend ON eend.id_entidade = ent.id_entidade and eend.bl_padrao = 1
JOIN tb_endereco as ed ON ed.id_endereco = eend.id_endereco
JOIN tb_municipio as mun ON mun.id_municipio = ed.id_municipio
LEFT JOIN dbo.tb_areaconhecimento AS ap ON ap.id_areaconhecimento = ac.id_areaconhecimentopai AND ap.id_tipoareaconhecimento = 1
GO

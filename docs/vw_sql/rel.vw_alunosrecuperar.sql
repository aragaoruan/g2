CREATE VIEW [rel].[vw_alunosrecuperar] as
SELECT DISTINCT
    mat.id_matricula,
    mat.id_entidadematricula,
    usu.id_usuario,
    usu.st_nomecompleto,
    usu.st_cpf,
    usu.st_email,
    usu.nu_ddd,
    usu.nu_telefone,
    usu.nu_dddalternativo,
    usu.nu_telefonealternativo,
    mat.id_areaconhecimento,
    ac.st_areaconhecimento,
    mat.id_projetopedagogico,
    pp.st_projetopedagogico,
    mat.id_evolucao,
    mat.st_evolucao,
    tur.dt_inicio AS dt_inicioturma,
    dis.id_disciplina,
--     dis.st_disciplina,
    sda.id_saladeaula,
    sda.dt_encerramento AS dt_encerramentosala ,
    sda.dt_abertura AS dt_aberturasala,
    dis.id_tipodisciplina,
    mn.st_avaliacaofinal,
    mn.st_avaliacaoead,
    mn.st_avaliacaotcc,
    CAST(mn.st_notafinal AS INT) AS st_notafinal,
    CAST(mn.st_notaead AS INT) AS st_notaead,
    CAST ((md.nu_aprovafinal * pp.nu_notamaxima / 100) AS INT) AS nu_notafinal,
    md.id_situacaotiposervico
  FROM dbo.vw_matricula mat
    JOIN dbo.vw_pessoa AS usu ON usu.id_usuario = mat.id_usuario
    JOIN dbo.tb_areaconhecimento AS ac ON ac.id_areaconhecimento = mat.id_areaconhecimento
    JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mat.id_projetopedagogico
    JOIN dbo.tb_turma AS tur ON tur.id_turma = mat.id_turma
    JOIN dbo.tb_matriculadisciplina AS md ON md.id_matricula = mat.id_matricula and md.id_evolucao != 12 AND md.id_situacaotiposervico IS NOT NULL
    JOIN tb_alocacao AS alc ON alc.id_matriculadisciplina = md.id_matriculadisciplina AND alc.bl_ativo = 1
    JOIN tb_encerramentoalocacao as ea on ea.id_alocacao = alc.id_alocacao
    JOIN tb_saladeaula sda ON sda.id_saladeaula = alc.id_saladeaula
    JOIN dbo.tb_disciplina AS dis ON dis.id_disciplina = md.id_disciplina
    JOIN dbo.vw_matriculanota AS mn ON mn.id_matriculadisciplina = md.id_matriculadisciplina;
GO
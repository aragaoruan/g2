CREATE VIEW [dbo].[vw_smsentidademensagem] as
SELECT  mp.id_mensagempadrao ,
        mp.id_tipoenvio ,
        mp.st_mensagempadrao ,
        mp.st_default, et.id_entidade,  sem.id_smsentidademensagem ,
        sem.id_textosistema ,
        sem.dt_cadastro ,
        sem.bl_ativo ,
        sem.id_usuariocadastro ,
        ts.st_textosistema FROM dbo.tb_mensagempadrao AS mp
JOIN tb_entidade AS et ON et.bl_ativo = 1
LEFT JOIN dbo.tb_smsentidademensagem AS sem ON mp.id_mensagempadrao = sem.id_mensagempadrao AND et.id_entidade = sem.id_entidade
LEFT JOIN dbo.tb_textosistema AS ts ON sem.id_textosistema = ts.id_textosistema
WHERE mp.id_tipoenvio = 5
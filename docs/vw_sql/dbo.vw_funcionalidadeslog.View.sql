create view dbo.vw_funcionalidadeslog as

SELECT DISTINCT
	log.id_funcionalidade,
	fc.st_funcionalidade,
	fc2.id_funcionalidadepai,
	log.id_entidade
FROM
	tb_log AS log
JOIN tb_funcionalidade AS fc ON fc.id_funcionalidade = log.id_funcionalidade AND fc.bl_ativo = 1
JOIN tb_funcionalidade AS fc2 ON fc2.id_funcionalidade = fc.id_funcionalidadepai AND fc2.bl_ativo = 1;
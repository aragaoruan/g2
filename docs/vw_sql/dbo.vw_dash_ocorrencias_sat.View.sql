CREATE VIEW [dbo].[vw_dash_ocorrencias_sat] AS

SELECT
  et.id_entidade,
  ISNULL(op.nu_pendentes, 0) AS nu_pendentes,
  ISNULL(oa.nu_andamento, 0) AS nu_andamento

FROM
  tb_entidade AS et

OUTER APPLY (
  SELECT
    ocp.id_entidade,
    COUNT(DISTINCT ocp.id_ocorrencia) AS nu_pendentes
  FROM
    tb_ocorrencia AS ocp
    JOIN tb_assuntoco AS ac ON ac.id_assuntoco = ocp.id_assuntoco AND ac.id_tipoocorrencia = 2 AND ac.bl_ativo = 1
    JOIN tb_nucleoassuntoco AS na ON na.id_assuntoco = ac.id_assuntoco AND na.bl_ativo = 1
    JOIN tb_nucleoco AS nc ON nc.id_nucleoco = na.id_nucleoco AND nc.id_entidade = ocp.id_entidade AND nc.bl_ativo = 1
  WHERE
    ocp.id_entidade = et.id_entidade
    AND ocp.id_situacao = 97 -- Pendentes
  GROUP BY ocp.id_entidade
) AS op

OUTER APPLY (
  SELECT
    oca.id_entidade,
    COUNT(DISTINCT oca.id_ocorrencia) AS nu_andamento
  FROM
    tb_ocorrencia AS oca
    JOIN tb_assuntoco AS ac ON ac.id_assuntoco = oca.id_assuntoco AND ac.id_tipoocorrencia = 2 AND ac.bl_ativo = 1
    JOIN tb_nucleoassuntoco AS na ON na.id_assuntoco = ac.id_assuntoco AND na.bl_ativo = 1
    JOIN tb_nucleoco AS nc ON nc.id_nucleoco = na.id_nucleoco AND nc.id_entidade = oca.id_entidade AND nc.bl_ativo = 1
  WHERE
    oca.id_entidade = et.id_entidade
    AND oca.id_situacao = 98 -- Em andamento
  GROUP BY oca.id_entidade
) AS oa
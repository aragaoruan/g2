SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_avaliacaoaplicacaorelacao]
AS
SELECT        dbo.tb_avaliacao.id_avaliacao, dbo.tb_avaliacao.st_avaliacao, dbo.tb_avaliacao.id_tipoavaliacao, dbo.tb_avaliacaoaplicacao.id_avaliacaoaplicacao, 
                         dbo.tb_avaliacaoaplicacao.id_aplicadorprova, dbo.tb_avaliacaoaplicacao.id_endereco, dbo.tb_avaliacaoaplicacao.dt_aplicacao,ta.st_tipoavaliacao,situacao.st_situacao
FROM            dbo.tb_avaliacao 
INNER JOIN
                         dbo.tb_avaliacaoaplicacaorelacao ON dbo.tb_avaliacao.id_avaliacao = dbo.tb_avaliacaoaplicacaorelacao.id_avaliacao 
INNER JOIN
                         dbo.tb_avaliacaoaplicacao ON dbo.tb_avaliacaoaplicacaorelacao.id_avaliacaoaplicacao = dbo.tb_avaliacaoaplicacao.id_avaliacaoaplicacao
JOIN tb_situacao as situacao on situacao.id_situacao = dbo.tb_avaliacao.id_situacao
JOIN tb_tipoavaliacao as ta ON dbo.tb_avaliacao.id_tipoavaliacao = ta.id_tipoavaliacao


GO



CREATE VIEW [rel].[vw_relatorioatividadescomplementares] AS
  SELECT
    mat.id_matricula,
    pp.id_projetopedagogico,
    pp.st_projetopedagogico,
    pp.nu_horasatividades,
    pp.nu_horasatividades AS st_totalhorascurso,
    mat.id_evolucao,
    evo.st_evolucao,
    mat.id_usuario,
    u.st_nomecompleto,
    u.st_cpf,
    SUM(atividade.nu_horasconvalidada) AS nu_totalhorasdeferida,
    SUM(atividade.nu_horasconvalidada) AS st_totalhorasdeferida,
    mat.id_entidadematricula AS id_entidade
  FROM
    tb_atividadecomplementar AS atividade
    JOIN tb_matricula AS mat ON mat.id_matricula = atividade.id_matricula
    JOIN tb_evolucao AS evo ON evo.id_evolucao = mat.id_evolucao
    JOIN tb_usuario AS u ON u.id_usuario = mat.id_usuario
    JOIN tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mat.id_projetopedagogico
  GROUP BY
    mat.id_matricula,
    pp.id_projetopedagogico,
    pp.st_projetopedagogico,
    pp.nu_horasatividades,
    mat.id_evolucao,
    evo.st_evolucao,
    mat.id_usuario,
    u.st_nomecompleto,
    u.st_cpf,
    atividade.id_entidadecadastro,
    mat.id_entidadematricula
GO


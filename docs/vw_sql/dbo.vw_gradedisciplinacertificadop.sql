-- vw responsavel por trazer as disciplinas e informacoes do aluno do certificado parcial
-- para ser utilizada pela variavel de sistema #grid_disciplina_certificado_parcial#

CREATE VIEW [dbo].[vw_gradedisciplinacertificadop] AS
  SELECT
    DISTINCT
    cpd.id_certificadoparcial,
    d.st_tituloexibicao,
    cpd.id_disciplina,
    d.nu_cargahoraria,
    CASE
    WHEN MONTH(s.dt_abertura) <= 6 THEN CONCAT(YEAR(s.dt_abertura), '.1')
    WHEN MONTH(s.dt_abertura) > 6 THEN CONCAT(YEAR(s.dt_abertura), '.2')
    END AS st_semestre,
    CONVERT(INT, md.nu_aprovafinal) AS nu_aprovafinal,
    e.st_evolucao,
    md.id_evolucao,
    md.id_matricula
  FROM tb_certificadoparcialdisciplina cpd
    JOIN tb_disciplina d ON cpd.id_disciplina = d.id_disciplina
    JOIN tb_matriculadisciplina md ON d.id_disciplina = md.id_disciplina AND md.id_evolucao = 12
    JOIN tb_alocacao a ON md.id_matriculadisciplina = a.id_matriculadisciplina AND a.bl_ativo = 1
    JOIN tb_evolucao e ON md.id_evolucao = e.id_evolucao
    JOIN tb_saladeaula s ON s.id_saladeaula = a.id_saladeaula
GO;
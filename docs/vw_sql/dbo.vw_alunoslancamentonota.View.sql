SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_alunoslancamentonota]
AS
SELECT Distinct dbo.tb_avaliacaoagendamento.id_avaliacaoagendamento,
						 dbo.tb_avaliacaoagendamento.id_matricula,
						 p.id_usuario,
					     dbo.vw_avaliacaoaplicacao.id_avaliacaoaplicacao,
                         dbo.vw_avaliacaoaplicacao.dt_aplicacao, 
						 dbo.vw_matricula.st_nomecompleto, 
						 a.id_areaconhecimento,
						 a.st_areaconhecimento,
						 dbo.vw_matricula.id_projetopedagogico, 
						 dbo.vw_matricula.st_projetopedagogico, 
                         dbo.vw_matricula.st_evolucao, 	 
						 dbo.vw_matricula.id_situacao,
						  dbo.vw_matricula.st_situacao, 
						  dbo.tb_avaliacaoagendamento.id_situacao AS id_situacaoagendamento,
						   dbo.vw_matricula.id_evolucao, 
                         dbo.vw_avaliacaoaplicacao.st_aplicadorprova,
						  dbo.vw_avaliacaoaplicacao.st_endereco,
						 dbo.vw_avaliacaoaplicacao.sg_uf, p.st_cpf, p.st_email,
						 dbo.vw_avaliacaoaplicacao.id_entidade,dbo.vw_avaliacaoaplicacao.id_aplicadorprova,
						  dbo.tb_avaliacaoagendamento.bl_provaglobal,
						  dbo.tb_avaliacaoagendamento.id_tipodeavaliacao,
						  aval.nu_valor
FROM            dbo.tb_avaliacaoagendamento
					
						 INNER JOIN dbo.vw_avaliacaoaplicacao ON dbo.tb_avaliacaoagendamento.id_avaliacaoaplicacao  = dbo.vw_avaliacaoaplicacao.id_avaliacaoaplicacao 
						 INNER JOIN dbo.vw_matricula on dbo.vw_matricula.id_matricula=dbo.tb_avaliacaoagendamento.id_matricula
						 inner join vw_pessoa as p on p.id_usuario = dbo.tb_avaliacaoagendamento.id_usuario and p.id_entidade=dbo.vw_avaliacaoaplicacao.id_entidade
						 inner join tb_areaprojetopedagogico as ap on ap.id_projetopedagogico=dbo.vw_matricula.id_projetopedagogico
						 inner join tb_areaconhecimento as a on a.id_areaconhecimento=ap.id_areaconhecimento
						 inner join tb_avaliacao as aval on aval.id_avaliacao = dbo.tb_avaliacaoagendamento.id_avaliacao
where dbo.vw_matricula.id_evolucao=6
and dbo.tb_avaliacaoagendamento.id_situacao=68
and dbo.tb_avaliacaoagendamento.nu_presenca=1

GO
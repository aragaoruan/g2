CREATE VIEW [dbo].[vw_saladisciplinatransferencia]
AS
SELECT DISTINCT
  aps.id_saladeaula,
  sa.st_saladeaula,
  sa.nu_maxalunos,
  aps.id_projetopedagogico,
  pp.id_trilha,
  tm2.id_turma,
  ds.id_disciplina,
  se.id_entidade,
  sa.dt_abertura,
  sa.dt_encerramento,
  disc.st_disciplina,
  disc.id_tipodisciplina,
  (SELECT
			COUNT(alc.id_alocacao)
		  FROM tb_alocacao alc,
			   dbo.tb_matriculadisciplina mdc
		  WHERE alc.id_saladeaula = sa.id_saladeaula
		  AND mdc.id_matriculadisciplina = alc.id_matriculadisciplina
		  AND mdc.id_evolucao = 13)		AS nu_alocados

	FROM tb_projetopedagogico AS pp
		JOIN tb_modulo AS md 
		  ON pp.id_projetopedagogico = md.id_projetopedagogico 
		  AND md.bl_ativo = 1
		JOIN tb_modulodisciplina AS tm
		  ON md.id_modulo = tm.id_modulo
		  AND tm.bl_ativo = 1
		JOIN tb_turmaprojeto AS tt
		  ON pp.id_projetopedagogico = tt.id_projetopedagogico
		  AND tt.bl_ativo = 1
		JOIN tb_areaprojetosala AS aps
		  ON aps.id_projetopedagogico = pp.id_projetopedagogico
		JOIN dbo.tb_turma AS tm2
		  ON tm2.id_turma = tt.id_turma
		JOIN tb_saladeaula AS sa
		  ON sa.id_saladeaula = aps.id_saladeaula
		  AND sa.dt_inicioinscricao <= tm2.dt_inicio
		  AND (sa.dt_fiminscricao >= tm2.dt_inicio
		  OR sa.dt_fiminscricao IS NULL)
		  AND sa.bl_ativa = 1
		  AND sa.id_situacao = 8
		  AND sa.id_categoriasala = 1
		JOIN tb_disciplinasaladeaula AS ds
		  ON ds.id_saladeaula = sa.id_saladeaula
		  AND tm.id_disciplina = ds.id_disciplina
		JOIN tb_saladeaulaentidade AS se
		  ON se.id_saladeaula = sa.id_saladeaula
		JOIN tb_disciplina AS disc
		  ON disc.id_disciplina = ds.id_disciplina UNION SELECT DISTINCT
		  aps.id_saladeaula,
		  sa.st_saladeaula,
		  sa.nu_maxalunos,
		  aps.id_projetopedagogico,
		  pp.id_trilha,
		  NULL AS id_turma,
		  ds.id_disciplina,

		  se.id_entidade,
		  sa.dt_abertura,
		  sa.dt_encerramento,
		  disc.st_disciplina,
		  disc.id_tipodisciplina,
		  (SELECT
			COUNT(alc.id_alocacao)
		  FROM tb_alocacao alc,
			   dbo.tb_matriculadisciplina mdc
		  WHERE alc.id_saladeaula = sa.id_saladeaula
		  AND mdc.id_matriculadisciplina = alc.id_matriculadisciplina
		  AND mdc.id_evolucao = 13)
		  AS nu_alocados
		FROM tb_projetopedagogico AS pp
		JOIN tb_modulo AS md
		  ON pp.id_projetopedagogico = md.id_projetopedagogico
		  AND md.bl_ativo = 1
		JOIN tb_modulodisciplina AS tm
		  ON md.id_modulo = tm.id_modulo
		  AND tm.bl_ativo = 1
		JOIN tb_areaprojetosala AS aps
		  ON aps.id_projetopedagogico = pp.id_projetopedagogico
		JOIN tb_saladeaula AS sa
		  ON sa.id_saladeaula = aps.id_saladeaula
		  AND sa.dt_inicioinscricao <= GETDATE()
		  AND (sa.dt_fiminscricao >= GETDATE()
		  OR sa.dt_fiminscricao IS NULL)
		  AND sa.bl_ativa = 1
		  AND sa.id_situacao = 8
		  AND sa.id_categoriasala = 1
		JOIN tb_disciplinasaladeaula AS ds
		  ON ds.id_saladeaula = sa.id_saladeaula
		  AND tm.id_disciplina = ds.id_disciplina
		JOIN tb_saladeaulaentidade AS se
		  ON se.id_saladeaula = sa.id_saladeaula
		JOIN tb_disciplina AS disc
		  ON disc.id_disciplina = ds.id_disciplina



GO



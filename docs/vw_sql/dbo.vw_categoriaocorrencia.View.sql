SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_categoriaocorrencia]
AS
SELECT ce.bl_ativo, ce.dt_cadastro
		, ce.id_categoriaocorrencia
		, ce.id_entidadecadastro
		, ce.id_situacao
		, ce.id_tipoocorrencia
		, ce.id_usuariocadastro
		, ce.st_categoriaocorrencia
		, sit.st_situacao
		, tipo.st_tipoocorrencia
	FROM dbo.tb_categoriaocorrencia AS ce
	INNER JOIN dbo.tb_situacao AS sit ON sit.id_situacao = ce.id_situacao
	INNER JOIN dbo.tb_tipoocorrencia AS tipo ON tipo.id_tipoocorrencia = ce.id_tipoocorrencia
GO
CREATE VIEW [dbo].[vw_avisoboleto]
AS
  SELECT DISTINCT mc.id_mensagemcobranca ,
    lc.id_lancamento ,
    mc.id_textosistema ,
    mc.id_entidade
  FROM dbo.tb_lancamento AS lc
    JOIN dbo.tb_lancamentovenda AS lv ON lc.id_lancamento = lv.id_lancamento
    JOIN dbo.tb_mensagemcobranca AS mc ON lc.id_entidade = mc.id_entidade
    JOIN dbo.tb_vendaproduto AS vp ON vp.id_venda = lv.id_venda
    JOIN dbo.tb_matricula AS mt ON mt.id_vendaproduto = vp.id_vendaproduto AND mt.bl_ativo = 1 AND mt.id_situacao = 50 AND mt.id_evolucao = 6
  WHERE CAST(GETDATE() AS DATE) >= DATEADD(DAY, 1 * mc.nu_diasatraso, lc.dt_vencimento)
    AND lc.id_lancamento NOT IN (
      SELECT id_lancamento
      FROM dbo.tb_enviocobranca AS ec
      WHERE ec.id_lancamento = lc.id_lancamento
      AND ec.id_mensagemcobranca = mc.id_mensagemcobranca
    )
    AND lc.bl_quitado = 0
    AND lc.bl_ativo = 1
    AND lc.id_meiopagamento = 2
    AND mc.bl_antecedencia = 0
    AND mc.dt_cadastro < lc.dt_vencimento and lc.dt_vencimento >= GETDATE()
  UNION
    SELECT DISTINCT mc.id_mensagemcobranca ,
      lc.id_lancamento ,
      mc.id_textosistema ,
      mc.id_entidade
    FROM dbo.tb_lancamento AS lc
      JOIN dbo.tb_lancamentovenda AS lv ON lc.id_lancamento = lv.id_lancamento
      JOIN dbo.tb_mensagemcobranca AS mc ON lc.id_entidade = mc.id_entidade
      JOIN dbo.tb_vendaproduto AS vp ON vp.id_venda = lv.id_venda
      JOIN dbo.tb_matricula AS mt ON mt.id_vendaproduto = vp.id_vendaproduto AND mt.bl_ativo = 1 AND mt.id_situacao = 50 AND mt.id_evolucao = 6
    WHERE lc.dt_vencimento <= DATEADD(DAY, mc.nu_diasatraso, CAST(GETDATE() AS DATE))
      AND lc.id_lancamento NOT IN (
      SELECT id_lancamento
      FROM dbo.tb_enviocobranca AS ec
      WHERE ec.id_lancamento = lc.id_lancamento
      AND ec.id_mensagemcobranca = mc.id_mensagemcobranca
    )
    AND lc.bl_quitado = 0
    AND lc.bl_ativo = 1
    AND lc.id_meiopagamento = 2
    AND mc.bl_antecedencia = 1
    AND mc.dt_cadastro < lc.dt_vencimento and lc.dt_vencimento >= GETDATE()
GO
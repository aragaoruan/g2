CREATE VIEW vw_entregamaterial AS select distinct
em.id_entregamaterial,
vw.st_nomecompleto,
vw.id_matricula,
vw.id_projetopedagogico,
vw.st_projetopedagogico,
md.id_matriculadisciplina,
d.id_disciplina,
d.st_disciplina,
pc.id_lotematerial,
pc.id_pacote,
cast(pc.dt_cadastro as date) as dt_criacaopacote,
pc.id_situacao as id_situacaopacote,
st.st_situacao as st_situacaopacote,
cast(em.dt_entrega as date) as dt_entrega,
cast(em.dt_devolucao as date) as dt_devolucao,
em.id_situacao as id_situacaoentrega,
ste.st_situacao as st_situacaoentrega,
em.id_itemdematerial,
im.st_itemdematerial,
im.id_upload,
vw.id_entidadeatendimento as id_entidade,
ac.id_areaconhecimento,
ac.st_areaconhecimento,
arp.st_codrastreamento as st_codrastreamento
 from
tb_entregamaterial as em
join tb_pacote as pc on pc.id_pacote = em.id_pacote
join tb_situacao as st on st.id_situacao = pc.id_situacao
join tb_situacao as ste on ste.id_situacao = em.id_situacao
join tb_itemdematerial as im on im.id_itemdematerial = em.id_itemdematerial
join tb_matriculadisciplina as md on md.id_matriculadisciplina = em.id_matriculadisciplina
join tb_disciplina as d on d.id_disciplina = md.id_disciplina
join vw_matricula as vw on vw.id_matricula = md.id_matricula
join tb_areaprojetopedagogico as ap on ap.id_projetopedagogico = vw.id_projetopedagogico
join tb_areaconhecimento as ac on ac.id_areaconhecimento = vw.id_areaconhecimento
left join tb_arquivoretornopacote arp on arp.id_pacote = pc.id_pacote;





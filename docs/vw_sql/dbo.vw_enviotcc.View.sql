CREATE VIEW [dbo].[vw_enviotcc] AS
  WITH _conf AS (
      SELECT
        2 AS TIPO_DISCIPLINA_TCC,
        122 AS APTO,
        203 AS ENVIO_PENDENTE,
        205 AS DEVOLVIDO_AO_ALUNO
  )
  SELECT

    vwi.id_matricula,
    vwi.st_nomecompleto,
    vwi.st_cpf,
    vwi.st_nomeexibicao,
    vwi.id_usuario,
    vwi.id_entidade,
    vwi.id_entidadesala,
    vwe.dt_encerramentoprofessor,
    vwi.id_projetopedagogico,
    vwi.st_tituloexibicaoprojeto,
    vwi.id_modulo,
    vwi.st_tituloexibicaomodulo,
    vwi.id_disciplina,
    vwi.st_disciplina,
    s.id_saladeaula,
    s.st_saladeaula,
    vwi.id_professor,
    vwi.st_professor,
    vwi.id_situacaotcc,
    vwa.id_avaliacaoconjuntoreferencia,
    vwa.id_avaliacao,
    vwa.st_justificativa,
    vwi.st_coordenador,
    vwi.id_alocacao
  FROM vw_alunogradeintegracao AS vwi
    CROSS APPLY _conf AS _
    JOIN tb_saladeaula AS s
      ON (vwi.id_saladeaula = s.id_saladeaula)
    JOIN vw_avaliacaoaluno AS vwa
      ON (vwi.id_matricula = vwa.id_matricula AND vwa.id_saladeaula = vwi.id_saladeaula AND vwa.id_tipoavaliacao = 6)
    LEFT JOIN vw_encerramentoalunos AS vwe
      ON (vwi.id_matricula = vwe.id_matricula AND vwa.id_saladeaula = vwe.id_saladeaula)
  WHERE vwi.id_tipodisciplina = _.TIPO_DISCIPLINA_TCC AND (vwi.bl_encerrado = 0 OR vwi.bl_encerrado IS NULL)
        AND vwi.id_situacaotcc IN (_.APTO, _.ENVIO_PENDENTE, _.DEVOLVIDO_AO_ALUNO)
        AND DATEADD(DAY, s.nu_diasextensao, vwa.dt_encerramentosala) > GetDate();
GO



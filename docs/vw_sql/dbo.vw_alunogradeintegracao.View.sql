CREATE VIEW [dbo].[vw_alunogradeintegracao] AS
  SELECT
    DISTINCT
      'dt_abertura' = CASE
                      WHEN sa.id_tiposaladeaula = 2
                        THEN
                          sa.dt_abertura
                      WHEN sa.id_tiposaladeaula = 1
                           AND (
                             CAST(sa.dt_abertura AS DATE) >= CAST(al.dt_inicio AS DATE)
                           )
                        THEN
                          CAST(al.dt_cadastro AS DATE)
                      ELSE
                        CAST(al.dt_inicio AS DATE)
                      END,
    mt.id_usuario,
    us.st_nomecompleto,
    us.st_cpf,
    ps.st_nomeexibicao,
    ps.st_urlavatar,
    ai.bl_encerrado,
    al.id_alocacao,
    mt.id_matricula,
    mt.id_projetopedagogico,
    pp.st_tituloexibicao AS st_tituloexibicaoprojeto,
    ml.id_modulo,
    ml.st_tituloexibicao AS st_tituloexibicaomodulo,
    md.id_evolucao,
    md.id_disciplina,
    dc.st_tituloexibicao AS st_disciplina,
    al.id_saladeaula,
    sa.id_categoriasala,
    mt.id_entidadeatendimento AS id_entidade,
    sa.id_entidade AS id_entidadesala,
      st_codusuario = CASE
                      WHEN ui.st_codusuario IS NOT NULL
                        THEN
                          ui.st_codusuario
                      ELSE
                        ui2.st_codusuario
                      END,
      st_loginintegrado = CASE
                          WHEN ui.st_loginintegrado IS NOT NULL
                            THEN
                              ui.st_loginintegrado
                          ELSE
                            ui2.st_loginintegrado
                          END,
      st_senhaintegrada = CASE
                          WHEN ui.st_senhaintegrada IS NOT NULL
                            THEN
                              ui.st_senhaintegrada
                          ELSE
                            ui2.st_senhaintegrada
                          END,
    si.st_codsistemacurso,
    ai.st_codalocacao,
    ai.id_sistema,
      st_codsistemaent = CASE
                         WHEN ei.st_codsistema IS NOT NULL
                           THEN
                             ei.st_codsistema
                         ELSE
                           ei2.st_codsistema
                         END,
    tu1.id_usuario AS id_professor,
    tu1.st_nomecompleto AS st_professor,
    tu3.st_nomecompleto AS st_coordenador,
      st_caminho = CASE
                   WHEN ei.st_caminho IS NOT NULL
                     THEN
                       ei.st_caminho
                   ELSE
                     ei2.st_caminho
                   END,
    dc.id_tipodisciplina,
    dc.st_imagem AS st_imagemdisciplina,
    sa.st_saladeaula,
    dc.st_descricao AS st_descricaodisciplina,
    'x' AS st_integracao,
      'id_status' = CASE
                    WHEN al.id_alocacao IS NOT NULL
                         AND ai.st_codalocacao IS NULL
                         AND sa.dt_abertura < getdate()
                      THEN
                        4
                    WHEN sa.dt_abertura > CAST(GETDATE() AS DATE)
                      THEN
                        3
                    WHEN CAST(sa.dt_abertura AS DATE) <= CAST(GETDATE() AS DATE)
                         AND (
                           CAST(sa.dt_encerramento AS DATE) >= CAST(GETDATE() AS DATE)
                           OR sa.dt_encerramento IS NULL
                         )
                      THEN
                        1
                    WHEN (
                           sa.dt_encerramento IS NOT NULL
                         )
                         AND (
                           (
                             DATEADD(
                                 DAY,
                                 ISNULL(sa.nu_diasextensao, 0),
                                 sa.dt_encerramento
                             )
                           ) < CAST(GETDATE() AS DATE)
                         )
                      THEN
                        2
                    WHEN sa.dt_encerramento IS NULL
                         AND DATEADD(
                                 DAY,
                                 ISNULL(sa.nu_diasencerramento, 0) + ISNULL(sa.nu_diasaluno, 0) +
                                 ISNULL(sa.nu_diasextensao, 0),
                                 al.dt_inicio
                             ) >= CAST(GETDATE() AS DATE)
                      THEN
                        1
                    WHEN sa.dt_encerramento IS NULL
                         AND DATEADD(
                                 DAY,
                                 ISNULL(sa.nu_diasencerramento, 0) + ISNULL(sa.nu_diasaluno, 0) +
                                 ISNULL(sa.nu_diasextensao, 0),
                                 al.dt_inicio
                             ) < CAST(GETDATE() AS DATE)
                      THEN
                        2
                    ELSE
                      1
                    END,
      'st_status' = CASE
                    WHEN sa.dt_abertura > CAST(GETDATE() AS DATE)
                      THEN
                        'Não Iniciada'
                    WHEN sa.dt_abertura <= CAST(GETDATE() AS DATE)
                         AND (
                           sa.dt_encerramento IS NULL
                           OR (
                             (
                               sa.dt_encerramento >= CAST(GETDATE() AS DATE)
                               OR (
                                    DATEADD(
                                        DAY,
                                        ISNULL(sa.nu_diasextensao, 0),
                                        sa.dt_encerramento
                                    )
                                  ) >= CAST(GETDATE() AS DATE)
                             )
                           )
                         )
                      THEN
                        'Aberta'
                    WHEN sa.dt_encerramento < CAST(GETDATE() AS DATE)
                      THEN
                        'Fechada'
                    ELSE
                      'Erro'
                    END,
      'dt_encerramento' = CASE
                          WHEN sa.bl_semdiasaluno = 1
                               AND sa.bl_semencerramento = 1
                               AND si.id_sistema = 6
                            THEN
                              DATEADD(DAY, 1, GETDATE())
                          WHEN sa.bl_semdiasaluno = 1
                               AND sa.bl_semencerramento = 1
                               AND si.id_sistema = 2
                               AND md.id_evolucao != 12
                            THEN
                              DATEADD(DAY, 1, GETDATE())
                          WHEN sa.bl_semdiasaluno = 1
                               AND sa.bl_semencerramento = 1
                               AND si.id_sistema = 2
                               AND md.id_evolucao = 12
                            THEN
                              DATEADD(DAY, -1, GETDATE())
                          WHEN sa.bl_semdiasaluno = 1
                               AND sa.bl_semencerramento = 0
                            THEN
                              DATEADD(
                                  DAY,
                                  sa.nu_diasencerramento + sa.nu_diasextensao,
                                  sa.dt_encerramento
                              )
                          WHEN sa.bl_semdiasaluno = 0
                               AND sa.bl_semencerramento = 0
                               AND sa.dt_abertura <= al.dt_cadastro
                            THEN
                              DATEADD(
                                  DAY,
                                  sa.nu_diasencerramento + sa.nu_diasaluno + sa.nu_diasextensao,
                                  al.dt_cadastro
                              )
                          WHEN sa.bl_semdiasaluno = 0
                               AND sa.bl_semencerramento = 0
                               AND sa.dt_abertura > al.dt_cadastro
                            THEN
                              DATEADD(
                                  DAY,
                                  sa.nu_diasencerramento + sa.nu_diasaluno + sa.nu_diasextensao,
                                  sa.dt_abertura
                              )
                          WHEN sa.bl_semdiasaluno = 0
                               AND sa.bl_semencerramento = 1
                            THEN
                              DATEADD(DAY, 1, GETDATE())
                          WHEN sa.id_tiposaladeaula = 1
                            THEN
                              DATEADD(
                                  DAY,
                                  CAST(sa.nu_diasaluno AS INT) + CAST(sa.nu_diasextensao AS INT),
                                  CAST(al.dt_inicio AS DATE)
                              )
                          END,
      'dt_encerramentosala' = CASE
                              WHEN sa.id_tiposaladeaula = 2
                                   AND sa.dt_encerramento IS NOT NULL
                                THEN
                                  DATEADD(
                                      DAY,
                                      ISNULL(sa.nu_diasencerramento, 0) + ISNULL(sa.nu_diasaluno, 0) +
                                      ISNULL(sa.nu_diasextensao, 0),
                                      sa.dt_encerramento
                                  )
                              WHEN sa.id_tiposaladeaula = 2
                                   AND sa.dt_encerramento IS NULL
                                THEN
                                  DATEADD(
                                      DAY,
                                      ISNULL(sa.nu_diasencerramento, 0) + ISNULL(sa.nu_diasaluno, 0) +
                                      ISNULL(sa.nu_diasextensao, 0),
                                      al.dt_cadastro
                                  )
                              WHEN sa.id_tiposaladeaula = 1
                                   AND al.dt_inicio IS NOT NULL
                                THEN
                                  DATEADD(
                                      DAY,
                                      ISNULL(sa.nu_diasencerramento, 0) + ISNULL(sa.nu_diasaluno, 0) +
                                      ISNULL(sa.nu_diasextensao, 0),
                                      al.dt_inicio
                                  )
                              WHEN sa.id_tiposaladeaula = 1
                                   AND al.dt_inicio IS NULL
                                THEN
                                  DATEADD(
                                      DAY,
                                      ISNULL(sa.nu_diasencerramento, 0) + ISNULL(sa.nu_diasaluno, 0) +
                                      ISNULL(sa.nu_diasextensao, 0),
                                      al.dt_cadastro
                                  )
                              ELSE
                                NULL
                              END,
      dt_encerramentoportal = CAST(
        (
          CASE -- Periodico
          WHEN sa.id_tiposaladeaula = 2
            THEN
              DATEADD(
                  DAY,
                  ISNULL(sa.nu_diasencerramento, 0) + ISNULL(sa.nu_diasaluno, 0) + ISNULL(sa.nu_diasextensao, 0),
                  sa.dt_encerramento
              ) -- Permanente
          -- Permanente
          WHEN sa.id_tiposaladeaula = 1 --AND sa.dt_abertura > al.dt_cadastro
            THEN
              DATEADD(
                  DAY,
                  ISNULL(sa.nu_diasencerramento, 0) + ISNULL(sa.nu_diasaluno, 0) + ISNULL(sa.nu_diasextensao, 0),
                  al.dt_inicio
              )
          ELSE
            NULL
          END
        ) AS DATE
    ),
    al.id_situacaotcc,
      id_aproparcial = CASE
                       WHEN an.id_avaliacaoaluno IS NULL
                         THEN
                           0
                       ELSE
                         1
                       END,
    sa.id_tiposaladeaula,
    aps.nu_diasacesso,
    sa.id_entidadeintegracao,
    situacao.st_situacao AS st_situacaotcc,
      nu_ordenacao = CASE
                     WHEN (CASE
                           WHEN sa.id_tiposaladeaula = 2
                             THEN
                               sa.dt_abertura
                           WHEN sa.id_tiposaladeaula = 1
                                AND (
                                  CAST(sa.dt_abertura AS DATE) >= CAST(al.dt_inicio AS DATE)
                                )
                             THEN
                               CAST(al.dt_cadastro AS DATE)
                           ELSE
                             CAST(al.dt_inicio AS DATE)
                           END) IS NULL
                       THEN 1
                     ELSE 0
                     END
  FROM
    tb_matricula AS mt
    JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
    JOIN tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
    JOIN tb_modulo AS ml ON ml.id_projetopedagogico = mt.id_projetopedagogico AND ml.bl_ativo = 1
    LEFT JOIN tb_modulodisciplina AS mdc ON mdc.id_modulo = ml.id_modulo AND mdc.id_disciplina = md.id_disciplina AND
                                            mdc.bl_ativo = 1
    JOIN tb_alocacao AS al ON md.id_matriculadisciplina = al.id_matriculadisciplina AND al.bl_ativo = 1
    JOIN tb_disciplina AS dc ON dc.id_disciplina = md.id_disciplina AND mdc.id_disciplina = dc.id_disciplina
    JOIN tb_usuario AS us ON us.id_usuario = mt.id_usuario
    JOIN tb_pessoa AS ps ON ps.id_usuario = mt.id_usuario AND ps.id_entidade = mt.id_entidadeatendimento
    JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula AND sa.bl_ativa = 1
    JOIN tb_areaprojetosala AS aps ON aps.id_saladeaula = sa.id_saladeaula AND
                                      aps.id_projetopedagogico = mt.id_projetopedagogico
    LEFT JOIN tb_saladeaulaintegracao AS si ON sa.id_saladeaula = si.id_saladeaula AND si.id_sistema = 6
    LEFT JOIN tb_alocacaointegracao AS ai ON al.id_alocacao = ai.id_alocacao AND
                                             ai.id_entidadeintegracao = sa.id_entidadeintegracao
    LEFT JOIN tb_usuariointegracao AS ui ON mt.id_usuario = ui.id_usuario AND ai.id_sistema = ui.id_sistema AND
                                            sa.id_entidade = ui.id_entidade AND ui.id_sistema = 6 AND
                                            ui.id_entidadeintegracao = sa.id_entidadeintegracao
    LEFT JOIN dbo.tb_entidadeintegracao AS ei ON ei.id_entidade = sa.id_entidade AND ei.id_sistema = si.id_sistema AND
                                                 ei.id_entidadeintegracao = sa.id_entidadeintegracao AND
                                                 ei.id_sistema IN (6, 11)
    LEFT JOIN tb_usuariointegracao AS ui2 ON mt.id_usuario = ui2.id_usuario AND ai.id_sistema = ui2.id_sistema AND
                                             mt.id_entidadeatendimento = ui2.id_entidade AND ui2.id_sistema = 999 AND
                                             ui2.id_entidadeintegracao = sa.id_entidadeintegracao
    LEFT JOIN dbo.tb_entidadeintegracao AS ei2 ON ei2.id_entidade = mt.id_entidadeatendimento
                                                  AND ei2.id_sistema = si.id_sistema AND
                                                  ei2.id_entidadeintegracao = sa.id_entidadeintegracao
                                                  AND ei2.id_sistema = 999
    LEFT JOIN tb_usuarioperfilentidadereferencia tu ON sa.id_saladeaula = tu.id_saladeaula
                                                       AND tu.bl_titular = 1
                                                       AND tu.bl_ativo = 1
    LEFT JOIN tb_usuario tu1 ON tu.id_usuario = tu1.id_usuario
    LEFT JOIN tb_usuarioperfilentidadereferencia tu2 ON tu2.id_projetopedagogico = mt.id_projetopedagogico
                                                        AND tu2.bl_titular = 1
                                                        AND tu2.bl_ativo = 1
                                                        AND tu2.id_disciplina IS NULL
    LEFT JOIN tb_usuario tu3 ON tu2.id_usuario = tu3.id_usuario
    JOIN tb_situacao situacao ON situacao.id_situacao = al.id_situacaotcc
--verifica se tem aproveitamento de nota
    OUTER APPLY (
                  SELECT
                    TOP 1
                    aa.id_avaliacaoaluno
                  FROM
                    dbo.tb_avaliacaoaluno AS aa
                    JOIN dbo.tb_avaliacaoconjuntoreferencia AS acr ON aa.id_avaliacaoconjuntoreferencia =
                                                                      acr.id_avaliacaoconjuntoreferencia
                  WHERE
                    aa.id_matricula = mt.id_matricula
                    AND acr.id_saladeaula = sa.id_saladeaula
                    AND aa.id_tiponota = 2
                    AND aa.bl_ativo = 1
                ) AS an
  WHERE
    mt.id_evolucao IN (6, 15)
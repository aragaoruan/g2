GO

/****** Object:  View [dbo].[vw_campanhapremioproduto]    Script Date: 14/07/2015 17:04:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_campanhapremioproduto] AS
SELECT
	pd.id_produto,
	pd.nu_valorvenda as nu_valorproduto,
	cc.id_campanhacomercial,
	cc.st_campanhacomercial,
	cc.id_entidade,
	cc.id_premio,
	pm.bl_compraacima,
	pm.nu_comprasacima,
	pm.bl_compraunidade,
	pm.nu_compraunidade,
	pm.st_prefixocupompremio,
	pm.nu_descontopremio,
	pm.id_tipodescontopremio,
	pm.id_tipopremio,
	pm.bl_promocaocumulativa,
	pm.id_cupomcampanha
FROM
	tb_campanhaproduto AS cp
JOIN tb_campanhacomercial AS cc ON cc.id_campanhacomercial = cp.id_campanhacomercial
JOIN vw_produto AS pd ON pd.id_produto = cp.id_produto
join tb_premio as pm on pm.id_premio = cc.id_premio and pm.bl_ativo = 1
WHERE
	cc.id_categoriacampanha = 4 --campanha da categoria premio
AND cc.id_situacao = 41 -- campanha com situa��o ativa
AND cc.bl_ativo = 1 --campanha ativa
AND cc.dt_inicio <= CONVERT (DATE, GETDATE()) --campanha que j� tenha sido iniciada
AND (
	cc.dt_fim >= CONVERT (DATE, GETDATE())
	OR cc.dt_fim IS NULL
) --campanha que n�o tenha sido finalizada ou com prazo indefinido

UNION
--busca todos os produtos vinculados as categorias vinculadas a campanha
SELECT
	pd.id_produto,
	pd.nu_valorvenda as nu_valorproduto,
	cc.id_campanhacomercial,
	cc.st_campanhacomercial,
	cc.id_entidade,
	cc.id_premio,
	pm.bl_compraacima,
	pm.nu_comprasacima,
	pm.bl_compraunidade,
	pm.nu_compraunidade,
	pm.st_prefixocupompremio,
	pm.nu_descontopremio,
	pm.id_tipodescontopremio,
	pm.id_tipopremio,
	pm.bl_promocaocumulativa,
	pm.id_cupomcampanha
FROM
	tb_campanhacategorias AS ct
JOIN tb_categoriaproduto AS cp ON cp.id_categoria = ct.id_categoria
JOIN tb_campanhacomercial AS cc ON cc.id_campanhacomercial = ct.id_campanhacomercial
JOIN vw_produto AS pd ON pd.id_produto = cp.id_produto
join tb_premio as pm on pm.id_premio = cc.id_premio and pm.bl_ativo = 1
AND pd.bl_ativo = 1
WHERE
	cc.id_categoriacampanha = 4 --campanha da categoria premio
AND cc.id_situacao = 41 -- campanha com situa��o ativa
AND cc.bl_ativo = 1 --campanha ativa
AND cc.dt_inicio <= CONVERT (DATE, GETDATE()) --campanha que j� tenha sido iniciada
AND (
	cc.dt_fim >= CONVERT (DATE, GETDATE())
	OR cc.dt_fim IS NULL
) --campanha que n�o tenha sido finalizada ou com prazo indefinido

UNION
-- busca todos os produtos da mesma entidade da campanha caso a campanha esteja com bl_todosprodutos marcado
SELECT
	pd.id_produto,
	pd.nu_valorvenda as nu_valorproduto,
	cc.id_campanhacomercial,
	cc.st_campanhacomercial,
	cc.id_entidade,
	cc.id_premio,
	pm.bl_compraacima,
	pm.nu_comprasacima,
	pm.bl_compraunidade,
	pm.nu_compraunidade,
	pm.st_prefixocupompremio,
	pm.nu_descontopremio,
	pm.id_tipodescontopremio,
	pm.id_tipopremio,
	pm.bl_promocaocumulativa,
	pm.id_cupomcampanha
FROM
	tb_campanhacomercial AS cc
JOIN vw_produto AS pd ON pd.id_entidade = cc.id_entidade
join tb_premio as pm on pm.id_premio = cc.id_premio and pm.bl_ativo = 1
AND pd.bl_ativo = 1
WHERE
	cc.id_categoriacampanha = 4 --campanha da categoria premio
AND cc.id_situacao = 41 -- campanha com situa��o ativa
AND cc.bl_ativo = 1 --campanha ativa
AND cc.dt_inicio <= CONVERT (DATE, GETDATE()) --campanha que j� tenha sido iniciada
AND (
	cc.dt_fim >= CONVERT (DATE, GETDATE())
	OR cc.dt_fim IS NULL
) --campanha que n�o tenha sido finalizada ou com prazo indefinido
AND cc.bl_todosprodutos = 1 --busca todos os produtos
GO



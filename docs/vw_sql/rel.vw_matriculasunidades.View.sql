
GO
/****** Object:  View [rel].[vw_matriculasunidades]    Script Date: 10/08/2012 17:42:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [rel].[vw_matriculasunidades] AS
SELECT DISTINCT
er.id_entidade ,
er.st_nomeentidade ,
nu_matriculas = CASE 
WHEN mtt.nu_matriculas IS NULL THEN 0
ELSE mtt.nu_matriculas
END,        
CASE 
WHEN ec.nu_cursando IS NULL THEN 0
ELSE ec.nu_cursando
END AS nu_cursando ,
CASE 
WHEN eco.nu_concluinte IS NULL THEN 0
ELSE eco.nu_concluinte
END AS nu_concluintes ,
CASE
WHEN  ece.nu_certificados IS NULL THEN 0
ELSE ece.nu_certificados
END  AS nu_certificados ,
CASE 
WHEN et.nu_trancados IS NULL THEN 0
ELSE et.nu_trancados
END AS nu_trancados ,
CASE 
WHEN etr.nu_tranferidos IS NULL THEN 0
ELSE etr.nu_tranferidos
END AS nu_transferidos ,
CASE  
WHEN eca.nu_cancelados IS NULL THEN 0
ELSE eca.nu_cancelados 
END AS nu_cancelados,
er.nu_entidadepai
FROM    dbo.vw_entidaderecursivaid AS er
LEFT JOIN tb_matricula AS mt ON mt.id_entidadeatendimento = er.id_entidade AND mt.bl_ativo = 1 AND mt.bl_institucional = 0
--JOIN dbo.tb_entidade AS em ON em.id_entidade = mt.id_entidadematricula
OUTER APPLY ( SELECT    id_entidadeatendimento,COUNT(ALL id_matricula) AS nu_matriculas
FROM      dbo.tb_matricula mta
WHERE     id_entidadeatendimento = er.id_entidade
GROUP BY id_entidadeatendimento
) AS mtt
OUTER APPLY ( SELECT    COUNT(DISTINCT id_matricula) AS nu_cursando
FROM      dbo.tb_matricula
WHERE     id_entidadeatendimento = er.id_entidade
AND id_evolucao = 6
GROUP BY  id_entidadeatendimento
) AS ec
OUTER APPLY ( SELECT    COUNT(DISTINCT id_matricula) AS nu_concluinte
FROM      dbo.tb_matricula
WHERE     id_entidadeatendimento = er.id_entidade
AND id_evolucao = 15
GROUP BY  id_entidadeatendimento
) AS eco
OUTER APPLY ( SELECT    COUNT(DISTINCT id_matricula) AS nu_certificados
FROM      dbo.tb_matricula
WHERE     id_entidadeatendimento = er.id_entidade
AND id_evolucao = 16
GROUP BY  id_entidadeatendimento
) AS ece
OUTER APPLY ( SELECT    COUNT(DISTINCT id_matricula) AS nu_trancados
FROM      dbo.tb_matricula
WHERE     id_entidadeatendimento = er.id_entidade
AND id_evolucao = 21
GROUP BY  id_entidadeatendimento
) AS et
OUTER APPLY ( SELECT    COUNT(DISTINCT id_matricula) AS nu_tranferidos
FROM      dbo.tb_matricula
WHERE     id_entidadeatendimento = er.id_entidade
AND id_evolucao = 20
GROUP BY  id_entidadeatendimento
) AS etr
OUTER APPLY ( SELECT    COUNT(DISTINCT id_matricula) AS nu_cancelados
FROM      dbo.tb_matricula
WHERE     id_entidadeatendimento = er.id_entidade
AND id_evolucao = 27
GROUP BY  id_entidadeatendimento
) AS eca
GO

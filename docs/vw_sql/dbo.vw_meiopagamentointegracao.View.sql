 CREATE VIEW [dbo].[vw_meiopagamentointegracao] AS
SELECT mp.id_meiopagamento ,
        st_meiopagamento ,
        st_descricao ,
        cb.id_cartaobandeira ,
        st_cartaobandeira ,
        id_meiopagamentointegracao ,
        ss.id_sistema ,
        et.id_entidade ,
        st_codsistema ,
        st_codcontacaixa FROM dbo.tb_meiopagamento AS mp
LEFT JOIN dbo.tb_cartaobandeira AS cb ON mp.id_meiopagamento IN (1, 7)
JOIN dbo.tb_entidade AS et ON mp.id_meiopagamento IS NOT NULL
JOIN dbo.tb_sistema AS ss ON ss.id_sistema = 3
LEFT JOIN dbo.tb_meiopagamentointegracao AS mpi ON mp.id_meiopagamento = mpi.id_meiopagamento AND (mpi.id_cartaobandeira = cb.id_cartaobandeira OR cb.id_cartaobandeira IS NULL) AND (et.id_entidade = mpi.id_entidade) AND ss.id_sistema = mpi.id_sistema



GO



CREATE VIEW [dbo].[vw_salaofertaturma] AS
SELECT DISTINCT
	tm.id_turma,
	te.id_entidade,
	tp.id_projetopedagogico,
	mo.id_modulo,
	mo.st_modulo,
	aps.id_saladeaula,
	sa.st_saladeaula,
	sa.nu_maxalunos,
	sa.bl_ofertaexcepcional,
	pp.id_trilha,
	ds.id_disciplina,
	dc.id_tipodisciplina,
	dc.nu_creditos,
  dc.st_disciplina,
	pl.id_periodoletivo,
	pl.st_periodoletivo,
	pl.dt_inicioinscricao,
	pl.dt_fiminscricao,
	pl.dt_abertura,
	pl.dt_encerramento
FROM dbo.tb_turma AS tm
JOIN tb_turmaprojeto AS tp
	ON tp.id_turma = tm.id_turma
JOIN tb_turmaentidade AS te
	ON te.id_turma = tm.id_turma
JOIN tb_projetopedagogico AS pp
	ON tp.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_areaprojetosala AS aps
	ON aps.id_projetopedagogico = tp.id_projetopedagogico
JOIN vw_periodoletivoentidade AS pl ON pl.id_periodoletivo IN (
		SELECT id_periodoletivo FROM vw_periodoletivoentidade AS plf WHERE plf.dt_abertura >= tm.dt_inicio AND plf.id_entidade = te.id_entidade
	) AND pl.id_entidade = te.id_entidade
JOIN tb_saladeaula AS sa
	ON sa.id_saladeaula = aps.id_saladeaula
	AND sa.id_periodoletivo = pl.id_periodoletivo
	AND sa.bl_ativa = 1
	AND sa.id_situacao = 8
	AND sa.id_categoriasala = 1
JOIN tb_saladeaulaentidade AS se
	ON (se.id_entidade = te.id_entidade AND aps.id_saladeaula = se.id_saladeaula) OR sa.bl_todasentidades = 1
JOIN tb_disciplinasaladeaula AS ds
	ON ds.id_saladeaula = sa.id_saladeaula
JOIN tb_modulo AS mo
	ON mo.id_projetopedagogico = pp.id_projetopedagogico AND mo.bl_ativo = 1
JOIN tb_modulodisciplina AS md
	ON ds.id_disciplina = md.id_disciplina and md.bl_ativo = 1 and md.id_modulo = mo.id_modulo
JOIN tb_disciplina AS dc
	ON dc.id_disciplina = ds.id_disciplina
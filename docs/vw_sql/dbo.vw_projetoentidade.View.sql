
GO
/****** Object:  View [dbo].[vw_projetoentidade]    Script Date: 10/08/2012 17:42:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_projetoentidade] as
SELECT DISTINCT
  p.id_projetopedagogico,
  p.id_situacao,
  p.st_descricao,
  p.bl_ativo,
  p.st_projetopedagogico,
  p.id_entidadecadastro,
  p.st_tituloexibicao,
  p.bl_disciplinacomplementar,
  e.id_entidade,
  e.st_nomeentidade,
  e.st_razaosocial,
  vwperf.st_nomecompleto as st_coordenadortitular
FROM tb_projetopedagogico as p
JOIN tb_projetoentidade as pe on pe.id_projetopedagogico = p.id_projetopedagogico
JOIN tb_entidade as e on e.id_entidade = pe.id_entidade
LEFT JOIN vw_usuarioperfilentidadereferencia AS vwperf ON vwperf.id_projetopedagogico = p.id_projetopedagogico
AND vwperf.id_perfilpedagogico = 2 --AND vwperf.id_entidade = e.id_entidade
 AND vwperf.bl_titular= 1
GO

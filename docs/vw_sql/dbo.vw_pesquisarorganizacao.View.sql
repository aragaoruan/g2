
GO
/****** Object:  View [dbo].[vw_pesquisarorganizacao]    Script Date: 10/08/2012 17:42:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_pesquisarorganizacao]

as

select e.id_entidade, e.st_cnpj, er.id_entidadepai, e.id_entidadecadastro, e.st_nomeentidade, e.st_razaosocial, ec.id_entidadeclasse, e.id_situacao, s.st_situacao, e.bl_ativo
from tb_entidade e
	 LEFT JOIN tb_entidaderelacao er ON er.id_entidade = e.id_entidade and er.id_entidadepai = e.id_entidade
	 LEFT JOIN tb_entidadeclasse ec ON ec.id_entidadeclasse = er.id_entidadeclasse	
	 JOIN tb_situacao s ON s.id_situacao = e.id_situacao
where e.bl_ativo = 1
GO


GO
/****** Object:  View [dbo].[vw_perfisentidade]    Script Date: 10/08/2012 17:42:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_perfisentidade] as
SELECT DISTINCT p.id_perfil, 
    p.st_nomeperfil,
    p.bl_ativo,
    p.bl_padrao,
    p.id_perfilpedagogico,
   -- pe.st_nomeperfil as st_nomeperfilentidade,
    p.id_situacao
    , s.st_situacao, 
  --    pe.id_entidade,
  --    e.st_nomeentidade,
        
    sis.id_sistema,
    sis.st_sistema,
    pp.st_perfilpedagogico,
  --  ec.id_entidadeclasse ,
   --   ec.st_entidadeclasse,
        p.id_entidade AS id_entidadecadastro
        
FROM tb_perfil p
    JOIN tb_situacao s ON p.id_situacao = s.id_situacao
    JOIN tb_sistema sis ON p.id_sistema = sis.id_sistema
    JOIN tb_entidade ecriacao ON ecriacao.id_entidade = p.id_entidade
    LEFT JOIN tb_entidaderelacao ercriacao ON ercriacao.id_entidade = ecriacao.id_entidade
    LEFT JOIN tb_entidadeclasse eccriacao ON eccriacao.id_entidadeclasse = ercriacao.id_entidadeclasse
	LEFT JOIN dbo.tb_perfilpedagogico AS pp ON pp.id_perfilpedagogico = p.id_perfilpedagogico
   -- LEFT JOIN tb_perfilentidade pe ON pe.id_perfil = p.id_perfil
   -- LEFT JOIN tb_entidade e ON pe.id_entidade = e.id_entidade
   -- LEFT JOIN tb_entidaderelacao er ON er.id_entidade = e.id_entidade
   -- LEFT JOIN tb_entidadeclasse ec ON ec.id_entidadeclasse = er.id_entidadeclasse
    WHERE p.bl_ativo = 1
GO

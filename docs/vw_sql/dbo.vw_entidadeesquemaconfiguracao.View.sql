create view [dbo].[vw_entidadeesquemaconfiguracao]
as
SELECT
	ec.id_esquemaconfiguracao, ec.id_usuariocadastro, ec.id_usuarioatualizacao, ec.st_esquemaconfiguracao, ec.dt_cadastro, ec.dt_atualizacao,
	ic.id_itemconfiguracao, ic.st_itemconfiguracao, ic.st_descricao, ic.st_default,
	eci.st_valor,
	e.st_nomeentidade,
	e.id_entidade
FROM tb_entidade e
INNER JOIN tb_esquemaconfiguracao ec
	ON e.id_esquemaconfiguracao = ec.id_esquemaconfiguracao
INNER JOIN tb_esquemaconfiguracaoitem eci
	ON eci.id_esquemaconfiguracao = ec.id_esquemaconfiguracao
INNER JOIN tb_itemconfiguracao ic
	ON ic.id_itemconfiguracao = eci.id_itemconfiguracao
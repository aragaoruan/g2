
GO

/****** Object:  View [rel].[vw_relatoriocupom]    Script Date: 21/07/2014 14:52:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 CREATE VIEW [rel].[vw_relatoriocupom] as
SELECT
DISTINCT
	c.id_cupom,
	c.st_codigocupom,
	v.id_entidade,
	CASE v.id_cupom
		WHEN CONVERT(int, v.id_cupom) THEN 'Sim' ELSE 'Não'
	END AS st_utilizado,
	CASE c.id_tipodesconto
		WHEN 1 THEN CAST('R$ ' + CONVERT(varchar(10), c.nu_desconto) AS varchar(10))
		WHEN 2 THEN CAST(CONVERT(varchar(10), c.nu_desconto) + '%' AS varchar(10))
	END AS nu_valorcupom,
	v.dt_confirmacao,
	v.dt_cadastro,
	v.id_venda,
	v.id_produto,
	v.st_produto,
	v.nu_valorliquido AS nu_valor
FROM [dbo].[tb_campanhacomercial] cc
JOIN [dbo].[tb_cupom] c	ON c.id_campanhacomercial = cc.id_campanhacomercial
LEFT JOIN [rel].[vw_vendaconfirmadasprodutos] v	ON v.id_cupom = c.id_cupom
WHERE c.bl_ativo = 1
AND cc.bl_ativo = 1
GO
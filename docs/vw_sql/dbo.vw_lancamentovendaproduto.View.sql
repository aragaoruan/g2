CREATE VIEW [dbo].[vw_lancamentovendaproduto] AS
  SELECT
    DISTINCT
    usat.id_usuario AS id_atendente,
    usat.st_nomecompleto AS st_atendente,
    usal.st_nomecompleto AS st_aluno,
    usal.st_cpf,
    di.st_rg,
    ce.st_email,
    ct.nu_ddd AS st_ddd,
    ct.nu_telefone AS st_telefone,
    oa_e.st_cep,
    oa_e.st_endereco,
    oa_e.st_bairro,
    oa_e.st_complemento,
    oa_e.nu_numero,
    oa_e.st_cidade,
    oa_e.sg_uf,
    vd.id_entidade,
    vd.id_venda,
    mp.st_meiopagamento,
    usal.id_usuario AS id_aluno,
    vd.dt_confirmacao,
    lc.dt_vencimento,
    lc.dt_quitado AS dt_baixa,
    lc.nu_valor AS nu_valorlancamento,
    CAST(lc.nu_quitado AS DECIMAL(30, 2)) AS nu_valorlancamentopago,
    lc.dt_cadastro,
    lc.id_lancamento,
    lv.nu_ordem,
    vd.nu_parcelas,
    e.st_nomeentidade,
    oa_st_produto.st_produto,
    oa_id_produto.id_produto,
    oa_nu_valorliquido.nu_valorliquido,
    oa_nu_valorbruto.nu_valorbruto,
    oa_nu_valorproduto.nu_valorproduto
  FROM tb_venda AS vd
    JOIN tb_usuario AS usat ON usat.id_usuario = vd.id_usuariocadastro
    JOIN tb_usuario AS usal ON usal.id_usuario = vd.id_usuario
    JOIN tb_vendaproduto AS vpp ON vpp.id_venda = vd.id_venda
    JOIN tb_produto AS pp ON pp.id_produto = vpp.id_produto AND pp.id_tipoproduto = 2
-- Apenas produtos do tipo TAXA
    LEFT JOIN tb_documentoidentidade AS di ON di.id_usuario = vd.id_usuario AND di.id_entidade = vd.id_entidade
    JOIN tb_contatosemailpessoaperfil AS cepp ON cepp.id_usuario = vd.id_usuario
                                                 AND cepp.id_entidade = vd.id_entidade
                                                 AND cepp.bl_padrao = 1
    LEFT JOIN tb_contatosemail AS ce ON ce.id_email = cepp.id_email
    LEFT JOIN tb_contatostelefonepessoa AS ctp ON ctp.id_usuario = vd.id_usuario
                                                  AND ctp.id_entidade = vd.id_entidade
                                                  AND ctp.bl_padrao = 1
    LEFT JOIN tb_contatostelefone AS ct ON ct.id_telefone = ctp.id_telefone
    JOIN tb_lancamentovenda AS lv ON lv.id_venda = vd.id_venda
    JOIN tb_lancamento AS lc ON lc.id_lancamento = lv.id_lancamento
    LEFT JOIN tb_meiopagamento AS mp ON mp.id_meiopagamento = lc.id_meiopagamento
    JOIN tb_entidade AS e ON vd.id_entidade = e.id_entidade
    OUTER APPLY (
                  SELECT
                    STUFF(REPLACE((SELECT
                                     '#!' + LTRIM(RTRIM(pv.nu_valor)) AS 'data()'
                                   FROM tb_vendaproduto AS vp
                                     JOIN tb_produto AS pr ON pr.id_produto = vp.id_produto
                                     LEFT JOIN tb_produtocombo AS pc ON pc.id_produtocombo = vp.id_produtocombo
                                     LEFT JOIN tb_produto AS cb ON cb.id_produto = pc.id_produto
                                     LEFT JOIN tb_produtovalor AS pv ON pv.id_produto = pr.id_produto AND
                                                                        pv.id_tipoprodutovalor = 4
                                   WHERE vp.id_venda = vd.id_venda AND pr.id_tipoproduto = pp.id_tipoproduto

                                   FOR XML PATH ('')), ' #!', ' / '), 1, 2, '') AS nu_valorproduto
                ) AS oa_nu_valorproduto
    OUTER APPLY (
                  SELECT
                    TOP 1
                    e.*
                  FROM tb_pessoaendereco AS pe
                    LEFT JOIN tb_endereco AS e
                      ON (pe.id_endereco = e.id_endereco AND e.bl_ativo = 1)
                  WHERE pe.id_usuario = vd.id_usuario
                        AND pe.bl_padrao = 1
                        AND pe.id_entidade = vd.id_entidade
                  ORDER BY
                    e.id_tipoendereco ASC
                ) AS oa_e
    OUTER APPLY (
                  SELECT
                    STUFF(REPLACE((SELECT
                                     '#!' + LTRIM(RTRIM(p.st_produto)) AS 'data()'
                                   FROM tb_vendaproduto pv
                                     JOIN tb_produto p ON pv.id_produto = p.id_produto
                                   WHERE id_venda = vd.id_venda AND p.id_tipoproduto = pp.id_tipoproduto
                                   FOR XML PATH ('')), ' #!', ' / '), 1, 2, '') AS st_produto
                ) AS oa_st_produto
    OUTER APPLY (
                  SELECT
                    STUFF(REPLACE((SELECT
                                     '#!' + LTRIM(RTRIM(p.id_produto)) AS 'data()'
                                   FROM tb_vendaproduto pv
                                     JOIN tb_produto p ON pv.id_produto = p.id_produto
                                   WHERE id_venda = vd.id_venda AND p.id_tipoproduto = pp.id_tipoproduto
                                   FOR XML PATH ('')), ' #!', ' / '), 1, 2, '') AS id_produto
                ) AS oa_id_produto
    OUTER APPLY (
                  SELECT
                    STUFF(REPLACE((SELECT
                                     '#!' + LTRIM(RTRIM(pv.nu_valorliquido)) AS 'data()'
                                   FROM tb_vendaproduto pv
                                     JOIN tb_produto AS p ON pv.id_produto = p.id_produto
                                   WHERE id_venda = vd.id_venda AND p.id_tipoproduto = pp.id_tipoproduto
                                   FOR XML PATH ('')), ' #!', ' / '), 1, 2, '') AS nu_valorliquido
                ) AS oa_nu_valorliquido
    OUTER APPLY (
                  SELECT
                    STUFF(REPLACE((SELECT
                                     '#!' + LTRIM(RTRIM(pv.nu_valorbruto)) AS 'data()'
                                   FROM tb_vendaproduto pv
                                     JOIN tb_produto AS p ON pv.id_produto = p.id_produto
                                   WHERE id_venda = vd.id_venda AND p.id_tipoproduto = pp.id_tipoproduto
                                   FOR XML PATH ('')), ' #!', ' / '), 1, 2, '') AS nu_valorbruto
                ) AS oa_nu_valorbruto

GO
/****** Object:  View [dbo].[vw_pessoadadosbiomedicos]    Script Date: 10/08/2012 17:42:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_pessoadadosbiomedicos] AS
select dbm.id_entidade, dbm.id_usuario, dbm.st_alergias, dbm.st_deficiencias, dbm.nu_glicemia, dbm.st_necessidadesespeciais,
       et.id_etnia, et.st_etnia, ts.id_tiposanguineo, ts.sg_tiposanguineo
from tb_dadosbiomedicos as dbm
	INNER JOIN tb_etnia as et ON et.id_etnia = dbm.id_etnia
	INNER JOIN tb_tiposanguineo as ts ON ts.id_tiposanguineo = dbm.id_tiposanguineo
	INNER JOIN tb_pessoa as p ON p.id_usuario = dbm.id_usuario AND p.id_entidade = dbm.id_entidade
GO

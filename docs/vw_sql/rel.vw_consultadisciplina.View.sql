CREATE view [rel].[vw_consultadisciplina] AS
SELECT DISTINCT
  mod.id_projetopedagogico,
  proped.st_projetopedagogico,
  dis.id_disciplina,
  dis.st_disciplina,
  dis.st_tituloexibicao,
  dis.nu_cargahoraria,
  discon.bl_ativo AS bl_ativo_conteudo,
  forarq.id_formatoarquivo,
  st_formatoarquivo = (CASE WHEN discon.bl_ativo = 1 THEN forarq.st_formatoarquivo ELSE '-' END),
  st_descricaoconteudo = (CASE WHEN discon.bl_ativo = 1 THEN discon.st_descricaoconteudo ELSE '-' END),
  discon.id_disciplinaconteudo,
  discon.dt_cadastro
FROM tb_disciplina AS dis
JOIN tb_modulodisciplina moddis ON moddis.id_disciplina = dis.id_disciplina
JOIN tb_modulo mod ON mod.id_modulo = moddis.id_modulo
JOIN tb_projetopedagogico proped ON proped.id_projetopedagogico = mod.id_projetopedagogico
JOIN tb_disciplinaconteudo discon ON discon.id_disciplina = dis.id_disciplina
JOIN tb_formatoarquivo forarq ON forarq.id_formatoarquivo = discon.id_formatoarquivo
WHERE dis.bl_ativa = 1
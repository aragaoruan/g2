CREATE VIEW [rel].[vw_historicocertificadoparcial]
AS
SELECT DISTINCT a.id_certificadoparcial,e.st_certificadoparcial,a.id_entidade,f.id_entidadepai,b.st_nomeentidade as st_nomeentidade,d.st_nomecompleto,d.st_cpf,MAX(a.dt_cadastro) as dt_cadastro FROM tb_historicocertificadoparcial a
JOIN tb_certificadoparcial e ON a.id_certificadoparcial = e.id_certificadoparcial
JOIN tb_entidaderelacao f ON f.id_entidade = a.id_entidade
JOIN tb_entidade b ON a.id_entidade = b.id_entidade
JOIN tb_matricula c ON c.id_matricula = a.id_matricula
JOIN tb_usuario d ON d.id_usuario = c.id_usuario
GROUP BY a.id_certificadoparcial,e.st_certificadoparcial,a.id_entidade,f.id_entidadepai,b.st_nomeentidade,d.st_nomecompleto,d.st_cpf
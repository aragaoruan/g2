CREATE VIEW [dbo].[vw_pesquisasaladeaula] AS
select sa.id_saladeaula ,
        sa.dt_cadastro ,
        st_saladeaula ,
        sa.bl_ativa ,
        sa.id_usuariocadastro ,
        sa.id_modalidadesaladeaula ,
        sa.dt_inicioinscricao ,
        sa.dt_fiminscricao ,
        sa.dt_abertura ,
        st_localizacao ,
        sa.id_tiposaladeaula ,
        sa.dt_encerramento ,
        sa.id_entidade ,
        sa.id_periodoletivo ,
        bl_usardoperiodoletivo ,
        sa.id_situacao ,
        nu_maxalunos ,
        nu_diasencerramento ,
        bl_semencerramento ,
        nu_diasaluno ,
        bl_semdiasaluno ,
        nu_diasextensao ,
        st_pontosnegativos ,
        st_pontospositivos ,
        st_pontosmelhorar ,
        st_pontosresgate ,
        st_conclusoesfinais ,
        st_justificativaacima ,
        st_justificativaabaixo, msa.st_modalidadesaladeaula, uper.id_perfilreferencia, uper.id_usuario, sap.id_saladeaulaprofessor,sap.bl_titular, sap.nu_maximoalunos, tsa.st_descricao, tsa.st_tiposaladeaula, e.st_nomeentidade,
        
pl.st_periodoletivo, pl.dt_inicioinscricao as dt_inicioinscricaoletivo, pl.dt_fiminscricao as dt_fiminscricaoletivo, 
pl.dt_abertura as dt_aberturaletivo, pl.dt_encerramento as dt_encerramentoletivo, s.st_situacao, u.st_nomecompleto as st_nomeprofessor,
d.id_disciplina, d.st_disciplina
,sa.id_categoriasala
,catsala.st_categoriasala
,CASE WHEN ((sa.dt_encerramento IS NOT NULL) )                                                               
	THEN DATEADD(day, ISNULL(nu_diasextensao, 0), sa.dt_encerramento)
  END as dt_comextensao,

  -- integra��o
  /*
  case when si.id_saladeaulaintegracao is null then 'Sem integra��o' else 
  (case when (ei.st_nomeintegracao is null or ei.st_nomeintegracao = '') then sis.st_sistema else ei.st_nomeintegracao end)
  end as st_sistema
  */
  sis.st_sistema

from tb_saladeaula as sa
		INNER JOIN tb_modalidadesaladeaula as msa ON msa.id_modalidadesaladeaula = sa.id_modalidadesaladeaula
		LEFT JOIN tb_usuarioperfilentidadereferencia as uper ON uper.id_saladeaula = sa.id_saladeaula AND UPER.bl_ativo = 1 AND UPER.bl_titular = 1
		LEFT JOIN tb_usuario as u ON u.id_usuario = uper.id_usuario
		LEFT JOIN tb_saladeaulaprofessor as sap ON sap.id_perfilreferencia = uper.id_perfilreferencia
		JOIN tb_tiposaladeaula as tsa ON tsa.id_tiposaladeaula = sa.id_tiposaladeaula
		JOIN tb_entidade as e ON e.id_entidade = sa.id_entidade
		--LEFT JOIN tb_entidaderelacao as er ON er.id_entidade = e.id_entidade AND er.id_entidadepai is null -- Ainda n�o sei se vai funcionar
		--LEFT JOIN tb_entidaderelacao as er2 ON er.id_entidadepai = e.id_entidade -- Ainda n�o sei se vai funcionar
		LEFT JOIN tb_disciplinasaladeaula as dsa ON dsa.id_saladeaula = sa.id_saladeaula
		LEFT JOIN tb_disciplina as d ON d.id_disciplina = dsa.id_disciplina
		LEFT JOIN tb_periodoletivo as pl ON pl.id_periodoletivo = sa.id_periodoletivo
		JOIN tb_situacao as s ON s.id_situacao = sa.id_situacao
		JOIN tb_categoriasala as catsala ON catsala.id_categoriasala = sa.id_categoriasala

		-- integra��o
		LEFT JOIN tb_saladeaulaintegracao as si on si.id_saladeaula = sa.id_saladeaula
		LEFT JOIN tb_sistema as sis on si.id_sistema = sis.id_sistema
		-- LEFT JOIN tb_entidadeintegracao as ei on si.id_entidadeintegracao = ei.id_entidadeintegracao and ei.id_sistema = sis.id_sistema 
		

where sa.bl_ativa=1

GO


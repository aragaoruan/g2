
GO
/****** Object:  View [dbo].[vw_entidadeendereco]    Script Date: 10/08/2012 17:41:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_entidadeendereco] AS
select ee.id_entidadeendereco, ee.id_entidade, ee.bl_padrao as bl_enderecoentidadepadrao, 
ee.id_endereco, ed.id_tipoendereco, te.st_tipoendereco, ed.st_endereco, 
ed.st_complemento, ed.nu_numero, ed.st_cidade, mn.id_municipio, mn.st_nomemunicipio, ed.sg_uf, 
ed.st_estadoprovincia, ed.st_cep, ed.id_pais, pa.st_nomepais, ed.st_bairro ,ed.bl_ativo
from tb_entidadeendereco ee
JOIN tb_endereco as ed ON ed.id_endereco = ee.id_endereco
JOIN tb_tipoendereco as te ON te.id_tipoendereco = ed.id_tipoendereco and te.id_categoriaendereco = 4
JOIN tb_uf as uf ON uf.sg_uf = ed.sg_uf
JOIN tb_municipio as mn ON mn.id_municipio = ed.id_municipio and mn.sg_uf = uf.sg_uf
JOIN tb_pais as pa ON pa.id_pais = ed.id_pais
GO

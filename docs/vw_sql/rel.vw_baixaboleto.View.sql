/****** Object:  View [rel].[vw_baixaboleto]    Script Date: 10/10/2014 08:51:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 CREATE VIEW [rel].[vw_baixaboleto]
AS
    SELECT  us.st_nomecompleto ,
            CAST(ct.nu_codintegracao AS VARCHAR) AS st_codintegracao,
            vd.id_venda ,
            er.st_nomeentidade ,
            er.id_entidade ,
            er.id_entidadepai ,
            lc.dt_vencimento ,
            lc.dt_quitado ,
            lc.nu_valor ,
            lc.nu_quitado ,
            lc.nu_juros ,
            lc.nu_desconto ,
            lc.id_lancamento,
            li.st_codlancamento,
            CASE WHEN nu_valor = nu_quitado THEN 0 ELSE 1 END AS nu_difforiginalbaixado
    FROM    dbo.tb_lancamento AS lc
            JOIN dbo.tb_lancamentovenda AS lv ON lc.id_lancamento = lv.id_lancamento
            JOIN tb_venda AS vd ON lv.id_venda = vd.id_venda
            JOIN dbo.tb_usuario AS us ON vd.id_usuario = us.id_usuario
            JOIN dbo.vw_entidaderecursivaid AS er ON er.id_entidade = vd.id_entidade
            LEFT JOIN tb_contrato AS ct ON vd.id_venda = ct.id_venda
            LEFT JOIN tb_lancamentointegracao AS li ON li.id_lancamento = lc.id_lancamento
    WHERE   lc.id_meiopagamento = 2
    UNION
    SELECT  us.st_nomecompleto ,
            ct.nu_codintegracao ,
            vd.id_venda ,
            er.st_nomeentidade ,
            er.id_entidade ,
            er.id_entidade AS id_entidadepai ,
            lc.dt_vencimento ,
            lc.dt_quitado ,
            lc.nu_valor ,
            lc.nu_quitado ,
            lc.nu_juros ,
            lc.nu_desconto ,
            lc.id_lancamento,
            li.st_codlancamento,
            CASE WHEN nu_valor < nu_quitado THEN 1 ELSE 0 END AS nu_difforiginalbaixado
    FROM    dbo.tb_lancamento AS lc
            JOIN dbo.tb_lancamentovenda AS lv ON lc.id_lancamento = lv.id_lancamento
            JOIN tb_venda AS vd ON lv.id_venda = vd.id_venda
            JOIN dbo.tb_usuario AS us ON vd.id_usuario = us.id_usuario
            JOIN dbo.tb_entidade AS er ON er.id_entidade = vd.id_entidade
            LEFT JOIN tb_contrato AS ct ON vd.id_venda = ct.id_venda
            LEFT JOIN tb_lancamentointegracao AS li ON li.id_lancamento = lc.id_lancamento
    WHERE   lc.id_meiopagamento = 2



GO
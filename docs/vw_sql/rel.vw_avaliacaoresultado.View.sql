
GO
/****** Object:  View [rel].[vw_avaliacaoresultado]    Script Date: 10/08/2012 17:42:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [rel].[vw_avaliacaoresultado] AS
SELECT us.st_nomecompleto,mt.id_matricula, dc.st_disciplina, ev.st_evolucao, aa.st_nota, aa.dt_avaliacao, mt.id_projetopedagogico, mt.id_entidadeatendimento  
FROM dbo.tb_matriculadisciplina AS md
JOIN tb_matricula AS mt ON mt.id_matricula = md.id_matricula
JOIN dbo.tb_disciplina AS dc ON dc.id_disciplina = md.id_disciplina
JOIN dbo.tb_avaliacaodisciplina AS ad ON ad.id_disciplina = dc.id_disciplina
JOIN dbo.tb_avaliacaoaluno AS aa ON aa.id_matricula = md.id_matricula AND aa.id_avaliacao = ad.id_avaliacao
JOIN dbo.tb_usuario AS us ON us.id_usuario = mt.id_usuario
JOIN dbo.tb_evolucao AS ev ON ev.id_evolucao = mt.id_evolucao
WHERE aa.bl_ativo = 1 AND aa.id_situacao in (86,87)
GO

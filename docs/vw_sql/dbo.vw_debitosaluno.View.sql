
/****** Object:  View [dbo].[vw_debitosaluno]    Script Date: 10/09/2012 10:17:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_debitosaluno] as
SELECT  vl.id_venda ,
        vl.id_usuariovenda ,
        vl.st_nomecompleto ,
        vl.nu_valor ,
        vl.dt_vencimento ,
        vl.st_meiopagamento ,
        ef.id_textoavisoatraso
FROM    dbo.vw_vendalancamento AS vl
        JOIN dbo.tb_entidadefinanceiro AS ef ON ef.id_entidade = vl.id_entidade
                                                AND ef.nu_avisoatraso IS NOT NULL
                                                AND ef.nu_avisoatraso <> 0
WHERE   GETDATE() >= DATEADD(day, ef.nu_avisoatraso, vl.dt_vencimento)
        AND (( ( vl.id_meiopagamento IN ( 1, 7 )
                AND dt_prevquitado IS NULL
              )
              OR ( vl.id_meiopagamento NOT IN ( 1, 7 )
                   AND vl.dt_quitado IS NULL
                   AND vl.bl_quitado = 0
                 )
            ))



GO



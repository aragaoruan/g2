
GO
/****** Object:  View [dbo].[vw_retorna_detalhes_tabela]    Script Date: 10/08/2012 17:42:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ALTER TABLE [dbo].[usuario] ADD CONSTRAINT UNQ_usuario_usuario UNIQUE ([usuario])

-- select * from vw_retorna_detalhes_tabela where tabela = 'usuario'

-- select * from usuario

CREATE VIEW [dbo].[vw_retorna_detalhes_tabela] AS

SELECT distinct 
					OBJECT_NAME(c.OBJECT_ID) tabela
					,c.name AS coluna
					,c.column_id as id
					,t.name AS tipo
					,s.xtype
					,c.max_length as tamanho
					,cast(prop.value as varchar(500)) as descricao
					,SCHEMA_NAME(t.schema_id) AS _schema
					,CASE WHEN c.is_nullable = 0 THEN 'true' ELSE 'false' END AS obrigatorio
					,CASE WHEN c.is_identity = 1 THEN 'true' ELSE 'false' END AS pk
					/*
					, CASE WHEN c.name IN ('dtcadastro', 'id', 'dtatualiza') THEN 'false' ELSE 'true' END AS insert_obrigatorio
					
					, CASE WHEN c.name IN ('id_cadastrador') THEN 'true' ELSE 'false' END AS update_obrigatorio
					, CASE WHEN c.is_identity = 1 THEN 'true' ELSE 'false' END AS where_obrigatorio

					, CASE WHEN c.name IN ('id') THEN 'true' ELSE 'false' END AS delete_obrigatorio
					*/
					
					FROM SYSOBJECTS s join sys.columns AS c on (s.name = OBJECT_NAME(c.OBJECT_ID) AND S.name NOT LIKE '%sys%' and s.xtype in ('U','V')
					)
					JOIN sys.types AS t ON c.user_type_id=t.user_type_id
					LEFT JOIN sys.extended_properties as prop on prop.major_id = c.OBJECT_ID AND prop.minor_id = C.column_id  
					AND prop.name = 'MS_Description' AND S.name != 'sysdiagrams'
					
				--	and c.name like 'vw_%'
-- select * from SYSOBJECTS where xtype = 'U'
GO

CREATE VIEW [dbo].[vw_encerramentoturmamatricula_pontosoft]
AS

SELECT
	mt.id_usuario,
	mt.id_entidadeatendimento AS id_entidade,
	ti.id_turma,
	ui.st_codusuario,
	0 AS bl_usuario,
	mi.id_matriculaintegracao,
	ui.id_usuariointegracao
FROM
	tb_matricula AS mt
JOIN tb_turma AS tm ON tm.id_turma = mt.id_turma
JOIN tb_turmaintegracao AS ti ON ti.id_turma = mt.id_turma
AND ti.id_sistema = 20
JOIN tb_matriculaintegracao AS mi ON mi.id_matricula = mt.id_matricula
AND mi.id_sistema = 20 and mi.bl_encerrado = 0
JOIN tb_usuariointegracao AS ui ON ui.id_usuario = mt.id_usuario
AND ui.id_entidade = mt.id_entidadeatendimento
AND ui.id_sistema = 20 and ui.bl_encerrado = 0
JOIN tb_entidadeintegracao AS ei ON ei.id_entidade = mt.id_entidadeatendimento
AND ei.id_sistema = 20
WHERE
	mt.id_evolucao = 27
UNION
	SELECT
		mt.id_usuario,
		mt.id_entidadeatendimento AS id_entidade,
		ti.id_turma,
		ui.st_codusuario,
		0 AS bl_usuario,
		mi.id_matriculaintegracao,
		ui.id_usuariointegracao
	FROM
		tb_matricula AS mt
	JOIN tb_turma AS tm ON tm.id_turma = mt.id_turma
	JOIN tb_turmaintegracao AS ti ON ti.id_turma = mt.id_turma
	AND ti.id_sistema = 20
	JOIN tb_matriculaintegracao AS mi ON mi.id_matricula = mt.id_matricula
	AND mi.id_sistema = 20 and mi.bl_encerrado = 0
	JOIN tb_usuariointegracao AS ui ON ui.id_usuario = mt.id_usuario
	AND ui.id_entidade = mt.id_entidadeatendimento
	AND ui.id_sistema = 20 and ui.bl_encerrado = 0
	JOIN tb_entidadeintegracao AS ei ON ei.id_entidade = mt.id_entidadeatendimento
	AND ei.id_sistema = 20
	WHERE
		tm.id_evolucao IN (53, 54)
	UNION
		SELECT
			mt.id_usuario,
			mt.id_entidadeatendimento AS id_entidade,
			ti.id_turma,
			ui.st_codusuario,
			1 AS bl_usuario,
			mi.id_matriculaintegracao,
			ui.id_usuariointegracao
		FROM
			tb_matricula AS mt
		JOIN tb_turma AS tm ON tm.id_turma = mt.id_turma
		JOIN tb_turmaintegracao AS ti ON ti.id_turma = mt.id_turma
		AND ti.id_sistema = 20
		JOIN tb_matriculaintegracao AS mi ON mi.id_matricula = mt.id_matricula
		AND mi.id_sistema = 20
		JOIN tb_usuariointegracao AS ui ON ui.id_usuario = mt.id_usuario
		AND ui.id_entidade = mt.id_entidadeatendimento
		AND ui.id_sistema = 20 and ui.bl_encerrado = 0
		JOIN tb_entidadeintegracao AS ei ON ei.id_entidade = mt.id_entidadeatendimento
		AND ei.id_sistema = 20
		WHERE
			mt.id_usuario NOT IN (
				SELECT
					mt2.id_usuario
				FROM
					tb_matricula AS mt2
				JOIN tb_matriculaintegracao AS mi2 ON mi2.id_matricula = mt2.id_matricula
				AND mi2.id_sistema = 20 and mi2.bl_encerrado = 0
				WHERE
					mt2.id_usuario = mt.id_usuario
				AND mt.id_entidadeatendimento = mt2.id_entidadeatendimento
			)

GO

/****** Object:  View [dbo].[vw_consultapagamento]    Script Date: 12/03/2015 10:23:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[vw_consultapagamento]
AS
SELECT DISTINCT
            es.id_saladeaula ,
            es.id_encerramentosala ,
            id_usuarioprofessor ,
            dt_encerramentoprofessor ,
            CONVERT(VARCHAR, dt_encerramentoprofessor, 103) AS st_encerramentoprofessor ,
            id_usuariocoordenador ,
            dt_encerramentocoordenador ,
            CONVERT(VARCHAR, dt_encerramentocoordenador, 103) AS st_encerramentocoordenador ,
            id_usuariopedagogico ,
            dt_encerramentopedagogico ,
            CONVERT(VARCHAR, dt_encerramentopedagogico, 103) AS st_encerramentopedagogico ,
            id_usuariofinanceiro ,
            dt_encerramentofinanceiro ,
            CONVERT(VARCHAR, dt_encerramentofinanceiro, 103) AS st_encerramentofinanceiro ,
            sa.id_disciplina ,
            sa.st_disciplina ,
            st_saladeaula ,
            sa.id_entidade ,
            sa.id_tipodisciplina ,
            uper.id_usuario AS id_coordenador ,
            al.nu_alunos ,
            tcc.st_nomecompleto AS st_aluno ,
            tcc.st_tituloavaliacao ,
            tcc.id_matricula ,
            tcc.st_upload ,
            tcc.st_projetopedagogico ,
            us.st_nomecompleto AS st_professor ,
            tcc.id_usuario AS id_aluno ,
            es.nu_valorpago
			,dd.nu_cargahoraria
			, sa.id_categoriasala
			
    FROM    dbo.tb_encerramentosala AS es
            JOIN dbo.tb_areaprojetosala AS aps ON aps.id_saladeaula = es.id_saladeaula
            JOIN dbo.tb_usuarioperfilentidadereferencia AS uper ON uper.id_projetopedagogico = aps.id_projetopedagogico
            JOIN vw_saladeaula AS sa ON sa.id_saladeaula = es.id_saladeaula
			JOIN dbo.tb_disciplina as dd ON dd.id_disciplina = sa.id_disciplina
            OUTER APPLY ( SELECT TOP 1
                                    aa.st_nomecompleto ,
                                    aa.st_tituloavaliacao ,
                                    md.id_matricula ,
                                    up.st_upload ,
                                    mat.id_usuario ,
                                    aa.st_projetopedagogico ,
                                    aa.st_nota
                          FROM      dbo.tb_encerramentoalocacao AS ee
                                    JOIN dbo.tb_alocacao AS ali ON ali.id_alocacao = ee.id_alocacao
                                    JOIN dbo.tb_matriculadisciplina AS md ON md.id_matriculadisciplina = ali.id_matriculadisciplina
                                    JOIN dbo.tb_matricula AS mat ON mat.id_matricula = md.id_matricula
                                    LEFT JOIN dbo.vw_avaliacaoaluno AS aa ON aa.id_saladeaula = sa.id_saladeaula
                                                              AND md.id_matricula = aa.id_matricula
                                                              AND aa.id_tipoavaliacao = 6
                                    LEFT JOIN dbo.tb_upload AS up ON aa.id_upload = up.id_upload
                          WHERE     es.id_encerramentosala = ee.id_encerramentosala
                        ) AS tcc
            LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS upr ON upr.id_saladeaula = sa.id_saladeaula
                                                              AND upr.bl_titular = 1
                                                              AND upr.bl_ativo = 1
            LEFT JOIN dbo.tb_usuario AS us ON us.id_usuario = upr.id_usuario
            OUTER APPLY ( SELECT    COUNT(DISTINCT id_alocacao) AS nu_alunos
                          FROM      dbo.tb_encerramentoalocacao
                          WHERE     id_encerramentosala = es.id_encerramentosala
                        ) AS al


GO



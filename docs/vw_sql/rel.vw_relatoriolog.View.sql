CREATE VIEW [rel].[vw_relatoriolog] AS
SELECT
	lg.id_log,
	lg.id_entidade,
	e.st_nomeentidade,
	lg.id_usuario,
	u.st_nomecompleto,
	lg.id_perfil,
	pf.st_nomeperfil,
	lg.id_funcionalidade as id_funcionalidadelog,
	fl.st_funcionalidade as st_funcionalidadelog,
	lg.id_operacao,
	opl.st_descricaooperacaolog,
	lg.id_tabela as id_registro,
	lg.st_tabela,
	lg.st_coluna,
	lg.st_antes,
	lg.st_depois,
	lg.dt_cadastro as dt_registrolog,
	fc.id_funcionalidade AS id_funcionalidadepai,
	fc.st_funcionalidade AS st_funcionalidadepai
FROM
	tb_log lg
JOIN tb_entidade e ON e.id_entidade = lg.id_entidade
JOIN tb_usuario u ON u.id_usuario = lg.id_usuario
JOIN tb_perfil pf ON pf.id_perfil = lg.id_perfil
JOIN vw_funcionalidadeslog fl ON fl.id_funcionalidade = lg.id_funcionalidade
LEFT JOIN tb_funcionalidade fc ON fc.id_funcionalidade = fl.id_funcionalidadepai
LEFT JOIN tb_operacaolog opl ON opl.id_operacaolog = lg.id_operacao
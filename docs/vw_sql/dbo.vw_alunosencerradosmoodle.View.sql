CREATE VIEW [dbo].[vw_alunosencerradosmoodle] AS
  SELECT
    DISTINCT
    sa.id_entidade,
    al.id_alocacao,
    mt.id_matricula,
    mt.id_usuario,
    sai.st_codsistemacurso,
    ei.nu_perfilalunoencerrado,
    sa.id_saladeaula,
    sa.id_entidadeintegracao
  FROM
    dbo.tb_saladeaula AS sa
    JOIN dbo.tb_saladeaulaintegracao AS sai ON sai.id_saladeaula = sa.id_saladeaula
    JOIN dbo.tb_alocacao AS al ON al.id_saladeaula = sa.id_saladeaula
    JOIN dbo.tb_matriculadisciplina AS md ON md.id_matriculadisciplina = al.id_matriculadisciplina
    JOIN dbo.tb_matricula AS mt ON mt.id_matricula = md.id_matricula
    JOIN dbo.tb_usuariointegracao AS ui ON ui.id_usuario = mt.id_usuario
                                           AND ui.id_sistema = 6
                                           AND ui.id_entidade = sa.id_entidade
                                           AND ui.id_entidadeintegracao = sa.id_entidadeintegracao
    JOIN dbo.tb_alocacaointegracao AS ai ON ai.id_alocacao = al.id_alocacao
                                            AND ai.bl_encerrado = 0
                                            AND ai.id_entidadeintegracao = sa.id_entidadeintegracao
    JOIN dbo.tb_entidadeintegracao AS ei ON ei.id_entidadeintegracao = sa.id_entidadeintegracao
                                            AND ei.id_sistema = 6
                                            AND ei.bl_ativo = 1
                                            AND ei.id_situacao = 198 -- Integração ativa
  WHERE CAST(
            DATEADD(
                DAY,
                sa.nu_diasextensao,
                sa.dt_encerramento
            ) AS DATE
        ) < CAST(GETDATE() AS DATE)
        AND ai.bl_encerrado = 0
        AND ai.id_sistema = 6
        AND ei.nu_perfilalunoencerrado != 0
        AND sa.id_entidade NOT IN (134, 12)
        AND sa.id_entidadeintegracao IS NOT NULL
  UNION
  SELECT
    DISTINCT
    sa.id_entidade,
    al.id_alocacao,
    mt.id_matricula,
    mt.id_usuario,
    sai.st_codsistemacurso,
    ei.nu_perfilalunoencerrado,
    sa.id_saladeaula,
    sa.id_entidadeintegracao
  FROM
    dbo.tb_saladeaula AS sa
    JOIN dbo.tb_saladeaulaintegracao AS sai ON sai.id_saladeaula = sa.id_saladeaula
    JOIN dbo.tb_alocacao AS al ON al.id_saladeaula = sa.id_saladeaula
    JOIN dbo.tb_matriculadisciplina AS md ON md.id_matriculadisciplina = al.id_matriculadisciplina
    JOIN dbo.tb_matricula AS mt ON mt.id_matricula = md.id_matricula
    JOIN dbo.tb_usuariointegracao AS ui ON ui.id_usuario = mt.id_usuario
                                           AND ui.id_sistema = 6
                                           AND ui.id_entidade = sa.id_entidade
                                           AND ui.id_entidadeintegracao = sa.id_entidadeintegracao
    JOIN dbo.tb_alocacaointegracao AS ai ON ai.id_alocacao = al.id_alocacao
                                            AND ai.bl_encerrado = 0
                                            AND ai.id_entidadeintegracao = sa.id_entidadeintegracao
    JOIN tb_encerramentoalocacao AS ea ON ea.id_alocacao = al.id_alocacao
    JOIN dbo.tb_entidadeintegracao AS ei ON ei.id_entidadeintegracao = sa.id_entidadeintegracao
                                            AND ei.id_sistema = 6
                                            AND ei.bl_ativo = 1
                                            AND ei.id_situacao = 198 -- Integração ativa
  WHERE sa.id_tiposaladeaula = 1
        AND ai.bl_encerrado = 0
        AND ai.id_sistema = 6
        AND ei.nu_perfilalunoencerrado != 0 --AND sa.id_entidade NOT IN (352, 581)
        --  AND sa.id_entidade = 581
        AND sa.id_entidadeintegracao IS NOT NULL

  UNION
  -- Busca os alunos que receberam nota de aproveitamento na transferencia
  SELECT
    sa.id_entidade,
    al.id_alocacao,
    mt.id_matricula,
    mt.id_usuario,
    sai.st_codsistemacurso,
    ei.nu_perfilalunoencerrado,
    sa.id_saladeaula,
    sa.id_entidadeintegracao
  FROM
    dbo.tb_matricula AS mt
    JOIN dbo.tb_matriculadisciplina AS md ON mt.id_matricula = md.id_matricula
                                             AND mt.id_evolucao = 6
    JOIN dbo.tb_alocacao AS al ON md.id_matriculadisciplina = al.id_matriculadisciplina
    JOIN tb_avaliacaoconjuntoreferencia AS acr ON al.id_saladeaula = acr.id_saladeaula
    JOIN tb_avaliacaoaluno AS aa ON acr.id_avaliacaoconjuntoreferencia = aa.id_avaliacaoconjuntoreferencia
                                    AND aa.id_matricula = md.id_matricula
    JOIN tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula
    JOIN dbo.tb_alocacaointegracao AS ai ON ai.id_alocacao = al.id_alocacao
                                            AND ai.bl_encerrado = 0
                                            AND ai.id_entidadeintegracao = sa.id_entidadeintegracao
                                            AND ai.id_sistema = 6
    JOIN dbo.tb_saladeaulaintegracao AS sai ON sai.id_saladeaula = sa.id_saladeaula
    JOIN dbo.tb_usuariointegracao AS ui ON ui.id_usuario = mt.id_usuario
                                           AND ui.id_sistema = 6
                                           AND ui.id_entidade = sa.id_entidade
                                           AND ui.id_entidadeintegracao = ai.id_entidadeintegracao
    LEFT JOIN dbo.tb_entidadeintegracao AS ei ON ei.id_entidade = sa.id_entidade
                                                 AND ei.id_sistema = 6
                                                 AND ei.bl_ativo = 1
                                                 AND ei.id_situacao = 198
  WHERE aa.id_tiponota = 2
        AND aa.bl_ativo = 1
        AND aa.id_situacao = 86
        AND ei.nu_perfilalunoencerrado != 0
        AND ai.bl_encerrado = 0
        AND sa.id_tiposaladeaula = 2
        AND sa.id_entidadeintegracao IS NOT NULL
GO
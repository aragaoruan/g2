CREATE VIEW [dbo].[vw_saladisciplinaparatransferencia]
AS
SELECT  DISTINCT
			dc.st_disciplina ,
			CAST(mn.st_notaatividade AS INT) as st_notaatividade,
			CAST(mn.st_notaead AS INT) as st_notaead ,
			CAST(mn.st_notatcc AS INT) as st_notatcc ,
			CAST(mn.st_notafinal AS INT) as  st_notafinal ,
			CAST(mn.nu_notatotal AS INT) as  st_notatotal ,
      sa.dt_abertura ,
			sa.dt_encerramento,
			gn.st_status,
			dc.id_tipodisciplina, 
			bl_check = CASE WHEN ((ea.id_alocacao IS NOT NULL)
                            AND ((mn.nu_notatotal + ISNULL(nf.nu_notafaltante, 0)) >=
                                 (pp.nu_notamaxima * pp.nu_percentualaprovacao / 100))
                            OR ((md.id_evolucao = 12))
                           )
                           AND (mn.nu_notatotal IS NOT NULL)
                           AND (ea.id_encerramentosala IS NOT NULL)
                           AND (dc.id_disciplina IN (SELECT
                                                       id_disciplina
                                                     FROM vw_saladisciplinatransferencia
                                                     WHERE id_entidade = vwdt.id_entidade AND
                                                           id_projetopedagogico = vwdt.id_projetopedagogico AND
                                                           id_turma = vwdt.id_turma))
    THEN 1
                 ELSE 0
                 END,
  ea.id_encerramentosala,
  vwdt.id_projetopedagogico AS id_projetotransfere,
  vwdt.id_entidade AS id_entidadetransfere,
  vwdt.id_turma AS id_turmatransfere,
  pp.id_projetopedagogico,
  sa.id_saladeaula,
  sa.st_saladeaula,
  dc.id_disciplina,
  mt.id_matricula,
  sa.id_categoriasala,
  modulo.nu_cargahoraria

    FROM    dbo.tb_matriculadisciplina AS md
            JOIN dbo.tb_matricula AS mt ON md.id_matricula = mt.id_matricula
            JOIN dbo.tb_disciplina AS dc ON dc.id_disciplina = md.id_disciplina
            JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
      JOIN dbo.vw_modulodisciplina AS modulo ON modulo.id_disciplina = md.id_disciplina AND
                                                modulo.id_projetopedagogico = mt.id_projetopedagogico
            LEFT JOIN dbo.tb_alocacao AS al ON al.id_matriculadisciplina = md.id_matriculadisciplina
                                               AND al.bl_ativo = 1
      LEFT JOIN dbo.tb_avaliacaoconjuntoreferencia AS acr ON acr.id_saladeaula = al.id_saladeaula
--AND acr.dt_fim IS NOT null
      LEFT JOIN tb_encerramentoalocacao AS ea ON ea.id_alocacao = al.id_alocacao
            LEFT JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula

            JOIN tb_evolucao AS ev ON ev.id_evolucao = md.id_evolucao
            JOIN dbo.tb_situacao AS st ON st.id_situacao = md.id_situacao
      LEFT JOIN dbo.vw_matriculanota AS mn ON md.id_matriculadisciplina = mn.id_matriculadisciplina
                                              AND mn.id_alocacao = al.id_alocacao
      LEFT JOIN vw_gradenota AS gn ON gn.id_matriculadisciplina = md.id_matriculadisciplina
			LEFT JOIN vw_saladisciplinatransferencia as vwdt on 1 = 1 --vwdt.id_projetopedagogico = pp.id_projetopedagogico
            OUTER APPLY ( SELECT    id_matricula ,
                                    id_disciplina ,
                                    SUM(CAST(nu_notamax AS FLOAT)) AS nu_notafaltante
                          FROM      vw_avaliacaoaluno
                          WHERE     bl_ativo IS NULL
                                    AND id_matricula = mt.id_matricula
                                    AND id_disciplina = md.id_disciplina
                                    AND id_tipoprova = 2
                          GROUP BY  id_disciplina ,
                                    id_matricula
                        ) AS nf


GO



CREATE VIEW [dbo].[vw_resumofinanceiro] AS
  SELECT
    DISTINCT
        l.id_lancamento,
        lv.id_venda,
        lv.bl_entrada,
        l.st_banco,
        l.st_emissor,
    lv.id_vendaaditivo,
        l.dt_prevquitado,
        l.st_coddocumento,
    l.id_usuariolancamento,
        l.id_entidadelancamento,
        ed.st_endereco,
        --ed.st_cidade,
        mu.st_nomemunicipio AS st_cidade,
        ed.st_bairro,
        ed.st_estadoprovincia,
        ed.st_cep ,
        ed.sg_uf,
        ed.nu_numero,
        ed.id_municipio,
        ed.id_tipoendereco,
        l.bl_quitado,
        l.dt_quitado,
    l.st_codigobarras,
        CAST(l.nu_quitado AS DECIMAL(10,2)) AS nu_quitado,
        st_entradaparcela = CASE lv.bl_entrada WHEN 1 THEN 'Entrada' ELSE 'Parcelas' END,
         lv.nu_ordem AS nu_parcela,
        l.id_meiopagamento,
        mp.st_meiopagamento,
        CASE
        WHEN ISNULL(l.nu_juros,0) > 0 or ISNULL(l.nu_desconto,0) > 0
          THEN ISNULL(l.nu_valor, 0) + ISNULL(l.nu_juros, 0) - ISNULL(l.nu_desconto, 0)
        ELSE l.nu_valor
        END AS nu_valor,
        l.nu_vencimento,
        l.dt_vencimento,
    l.st_autorizacao,
    CASE WHEN l.st_ultimosdigitos IS NULL
      THEN si3.st_ultimosdigitos
    ELSE l.st_ultimosdigitos END AS st_ultimosdigitos,
        st_situacao = CASE
                WHEN CAST(l.dt_vencimento AS date) < CAST(GETDATE() AS DATE) AND l.bl_quitado = 0 AND l.id_meiopagamento NOT IN (1,7) AND l.bl_ativo = 1 THEN 'Vencido'
                WHEN CAST(l.dt_vencimento AS DATE) < CAST(GETDATE() AS DATE) AND l.bl_quitado = 0 AND l.id_meiopagamento IN (1,7) AND l.dt_prevquitado   IS NULL AND l.bl_ativo = 1 THEN 'Atrasado'
                WHEN l.id_meiopagamento IN (1,7) AND l.dt_prevquitado IS NOT NULL  AND l.bl_ativo = 1 THEN 'Autorizado'
                WHEN l.bl_quitado = 1  AND l.bl_ativo = 1 THEN 'Pago'
                WHEN l.bl_ativo = 0 THEN 'Cancelado'
                ELSE 'Pendente'
                END,
    CASE WHEN u.id_usuario IS NOT NULL
      THEN u.id_usuario
    WHEN el.id_entidade IS NOT NULL
      THEN el.id_entidade END AS id_usuario,
    CASE WHEN u.st_nomecompleto IS NOT NULL
      THEN u.st_nomecompleto
    WHEN el.st_nomeentidade IS NOT NULL
      THEN el.st_nomeentidade
      COLLATE DATABASE_DEFAULT END AS st_nomecompleto,
        l.id_entidade,
        ef.id_textosistemarecibo,
        ef.id_reciboconsolidado,
        l.st_nossonumero,
    CASE WHEN si3.st_codtransacaooperadora IS NOT NULL
      THEN si3.st_codtransacaooperadora
    WHEN l.st_coddocumento IS NOT NULL
      THEN l.st_coddocumento END AS st_codtransacaooperadora,
        l.bl_original,
        l.bl_ativo,
        l.dt_atualizado,
        l.st_numcheque,
    l.nu_juros,
    l.nu_desconto,
    l.nu_jurosboleto,
    l.nu_descontoboleto,
    l.st_urlboleto,
    l.id_transacaoexterna,
    l.id_acordo,
    v.recorrente_orderid,
    v.dt_cadastro AS dt_venda,
    si3.st_situacao AS st_situacaopedido,
    CASE WHEN si3.st_situacao IS NOT NULL
      THEN si3.st_situacao
    WHEN l.bl_quitado = 0
      THEN 'PENDENTE'
    WHEN l.bl_quitado = 1
      THEN 'PAGA' END AS st_situacaolancamento,
    CASE WHEN sl.id_venda IS NOT NULL
      THEN 1
    ELSE 0 END AS bl_cartaovencendo,
    l.bl_chequedevolvido,
    CASE WHEN rec.bl_recorrente > 0
      THEN 1
    ELSE 0 END AS bl_recorrente,
    evo.id_evolucao AS id_evolucaovenda,
    evo.st_evolucao AS st_evolucaovenda,
    l.id_tipolancamento
  FROM tb_lancamento l
        INNER JOIN tb_lancamentovenda AS lv ON lv.id_lancamento = l.id_lancamento
        INNER JOIN tb_meiopagamento AS mp ON mp.id_meiopagamento = l.id_meiopagamento
        INNER JOIN tb_venda AS v ON v.id_venda = lv.id_venda
        INNER JOIN tb_vendaproduto AS vp ON vp.id_venda = v.id_venda
        INNER JOIN tb_produto AS p ON p.id_produto = vp.id_produto
    INNER JOIN tb_evolucao AS evo ON evo.id_evolucao = v.id_evolucao
        LEFT JOIN tb_entidadefinanceiro ef ON ef.id_entidade = v.id_entidade
--LEFT JOIN tb_transacaolancamento AS tl ON l.id_lancamento = tl.id_lancamento
--LEFT JOIN tb_transacaofinanceira AS tf ON tl.id_transacaofinanceira = tf.id_transacaofinanceira
        LEFT JOIN tb_pedidointegracao AS pint ON pint.id_venda = v.id_venda
--LEFT JOIN tb_situacaointegracao AS si ON si.id_situacaointegracao = pint.id_situacaointegracao
    OUTER APPLY (SELECT
                   TOP 1
                   si2.st_situacao,
                   tf.st_ultimosdigitos,
                   tf.st_codtransacaooperadora
                 FROM tb_transacaolancamento AS tl
                   INNER JOIN tb_transacaofinanceira AS tf ON
                                                             tl.id_transacaofinanceira
                                                             =
                                                             tf.id_transacaofinanceira
                   INNER JOIN tb_situacaointegracao AS si2 ON
                                                             si2.id_situacaointegracao
                                                             =
                                                             tf.id_situacaointegracao
                 WHERE tl.id_lancamento = L.id_lancamento
                 ORDER BY
                   tl.id_transacaofinanceira DESC
                ) AS si3
        OUTER APPLY (
                      SELECT
                        TOP 1
                        st_nomecompleto,
                        us.id_usuario,
                        pe.id_endereco
                      FROM tb_usuario us
                        LEFT JOIN tb_pessoaendereco pe ON pe.id_usuario = us.id_usuario AND pe.bl_padrao = 1 AND
                                                          pe.id_entidade = v.id_entidade
                      WHERE us.id_usuario = id_usuariolancamento
                    ) AS u
    OUTER APPLY (
                  SELECT
                    TOP 1
                    st_nomeentidade,
                    e.id_entidade,
                    ee.id_endereco
                  FROM tb_entidade e
                    JOIN tb_entidadeendereco ee ON e.id_entidade =
                                                   ee.id_entidade
                  WHERE e.id_entidade = id_entidadelancamento
                ) AS el
    OUTER APPLY (
                  SELECT
                    DISTINCT
                    vv.id_venda
                  FROM tb_venda vv
                    INNER JOIN tb_pedidointegracao pit ON pit.id_venda =
                                                          vv.id_venda
                    INNER JOIN tb_lancamentovenda lvs ON lvs.id_venda =
                                                         vv.id_venda
                    INNER JOIN tb_lancamento ls ON ls.id_lancamento =
                                                   lvs.id_lancamento
                  WHERE vv.id_venda = v.id_venda
                        AND pit.id_sistema = 10
                        AND CAST(ls.dt_vencimento AS DATE) >=
                            CAST(pit.dt_vencimentocartao AS DATE)
                        AND
                        DATEDIFF(DAY, GETDATE(), pit.dt_vencimentocartao) <= 30
                ) sl
    OUTER APPLY (
                  SELECT
                    COUNT(id_meiopagamento) AS bl_recorrente
                  FROM tb_lancamento ll
                    INNER JOIN tb_lancamentovenda lvv ON ll.id_lancamento =
                                                         lvv.id_lancamento
                  WHERE lvv.id_venda = v.id_venda
                        AND ll.id_meiopagamento = 11
                        AND ll.bl_quitado = 0
                        AND ll.bl_ativo = 1
                ) rec
    OUTER APPLY (
                  SELECT
                    TOP 1
                    ed.st_bairro,
                    ed.st_endereco,
                    ed.st_estadoprovincia,
                    ed.st_cep,
                    ed.sg_uf,
                    ed.nu_numero,
                    ed.id_tipoendereco,
                    ed.id_municipio
                  FROM tb_endereco AS ed
                  WHERE ed.bl_ativo = 1 AND
                        ed.id_endereco = CASE WHEN u.id_endereco IS NOT NULL
                          THEN u.id_endereco
                                         WHEN el.id_endereco IS NOT NULL
                                           THEN el.id_endereco END
                ) AS ed
    LEFT JOIN tb_municipio AS mu ON ed.id_municipio = mu.id_municipio
GO
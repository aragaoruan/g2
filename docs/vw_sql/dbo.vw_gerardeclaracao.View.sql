CREATE VIEW [dbo].[vw_gerardeclaracao] AS
  SELECT
    DISTINCT
    mt.id_matricula,
    ps.id_usuario AS id_usuarioaluno,
    ps.st_nomecompleto AS st_nomecompletoaluno,
    ps.st_login AS st_loginaluno,
    ps.st_senhaentidade AS st_senhaaluno,
    ps.st_cpf AS st_cpfaluno,
    ps.st_rg AS st_rgaluno,
    ps.st_orgaoexpeditor,
    ps.dt_dataexpedicao AS dt_dataexpedicaoaluno,
    concat(ps.st_nomemae, '/', ps.st_nomepai) AS st_filiacao,
    ps.st_nomemae AS st_mae,
    ps.st_nomepai AS st_pai,
    ps.st_ufnascimento,
    REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
                                        stuff((
                                                SELECT
                                                  ' ' + upper(left(T3.V, 1)) + lower(stuff(T3.V, 1, 1, ''))
                                                FROM (SELECT
                                                        cast(replace((SELECT
                                                                        ps.st_municipionascimento AS '*'
                                                                      FOR XML PATH ('')), ' ', '<X/>') AS XML).query(
                                                            '.')) AS T1(X)
                                                  CROSS APPLY T1.X.nodes('text()') AS T2(X)
                                                  CROSS APPLY (SELECT
                                                                 T2.X.value('.', 'varchar(120)')) AS T3(V)
                                                FOR XML PATH (''), TYPE
                                              ).value('text()[1]', 'varchar(30)'), 1, 1, ''), ' De ', ' de '), ' Da ',
                                    ' da '), ' Do ', ' do '), ' Das ', ' das '), ' Dos ', ' dos ') + '/' +
    ps.sg_ufnascimento AS st_municipionascimento,
    ps.id_pais,
    REPLACE(pa.st_nacionalidade, 'eiro', 'eira') AS st_nacionalidadealuno,
-- Título Eleitor
    ps.st_tituloeleitor,
    ps.st_zonaeleitoral,
    ps.st_municipioeleitoral,
    ps.st_secaoeleitoral,
-- Reservista
    ISNULL(ps.st_certificadoreservista, 'XXXXXXXX') AS st_certificadoreservista,
    ISNULL(CONVERT(VARCHAR, ps.dt_expedicaocertificadoreservista, 103),
           'XXXXXXXX') AS st_dtexpedicaocertificadoreservista,
    ISNULL(ps.st_reparticaoexpedidora, 'XXXXXXXX') AS st_reparticaoexpedidora,
      st_categoriaservicomilitar = (CASE
                                    WHEN ps.id_categoriaservicomilitar = 1
                                      THEN 'SERVIU EXERCITO, MARINHA OU AERONAUTICA'
                                    WHEN ps.id_categoriaservicomilitar = 2
                                      THEN 'TIRO DE GUERRA'
                                    WHEN ps.id_categoriaservicomilitar = 3
                                      THEN 'DISPENSADO'
                                    ELSE 'XXXXXXXX'
                                    END),
    mtdp.dt_diplomagerado,
    mtdp.st_observacao AS st_observacaohistorico,
    mtcl.dt_colacao,
    dc.st_nomeinstituicao,
    dc.dt_anoconclusao AS ano_conclusao_medio,
    dcm.st_nomemunicipio AS st_municipioinstituicao,
      nu_horasaativcomplementar = (CASE
                                   WHEN ativ_comp.nu_horasaluno IS NULL
                                     THEN 0
                                   WHEN ativ_comp.nu_horasaluno > pp.nu_horasatividades
                                     THEN pp.nu_horasatividades
                                   ELSE ativ_comp.nu_horasaluno
                                   END),
    CONVERT(VARCHAR(4), DATEPART(YEAR, mt.dt_concluinte), 103) AS ano_conclusao,
      semestre_conclusao = (
      CASE WHEN
        CONVERT(VARCHAR(2), DATEPART(MONTH, mt.dt_concluinte), 103) < 7
        THEN 1
      WHEN CONVERT(VARCHAR(2), DATEPART(MONTH, mt.dt_concluinte), 103) >= 7
        THEN 2
      ELSE NULL
      END),
    mc.dt_cadastro AS dt_expedicaocertificado,
    CONVERT(CHAR, mt.dt_cadastro, 103) AS st_datamatricula,
    canc.id_cancelamento,
    canc.st_observacao AS st_observacaocancelamento,
    canc.st_observacaocalculo AS st_observacao_calculo,
    CONVERT(CHAR, canc.dt_solicitacao, 103) AS st_solicitacaocancelamento,
    canc.nu_valorcarta AS st_valorcredito,
    CONVERT(CHAR, DATEADD(D, 365, canc.dt_solicitacao), 103) AS st_validadecredito,
    cm.id_contrato,
    ps.dt_nascimento AS dt_nascimentoaluno,
    UPPER(SUBSTRING(ps.st_nomemunicipio, 1, 1))
    + LOWER(SUBSTRING(ps.st_nomemunicipio, 2, 499)) AS st_municipioaluno,
    ps.st_estadoprovincia AS st_ufaluno,
    ps.sg_uf AS sg_ufaluno,
    ps.nu_ddd AS nu_dddaluno,
    ps.nu_telefone AS nu_telefonealuno,
    ps.st_email AS st_emailaluno,
    pp.st_tituloexibicao AS st_projeto,
    pp.st_reconhecimento,
--pp.nu_cargahoraria,
    (SELECT
       SUM(dis.nu_cargahoraria) AS nu_cargahoraria
     FROM tb_matriculadisciplina md
       JOIN tb_matricula m ON m.id_matricula = md.id_matricula
       JOIN tb_disciplina dis ON dis.id_disciplina = md.id_disciplina
     WHERE md.id_matricula = mt.id_matricula
    ) AS nu_cargahoraria,
    ne.st_nivelensino,
    mt.dt_concluinte AS dt_concluintealuno,
    us_coordenador.st_nomecompleto AS st_coordenadorprojeto,
    fl.id_fundamentolegal AS id_fundamentolei,
    fl.nu_numero AS nu_lei,
    flr.id_fundamentolegal AS id_fundamentoresolucao,
    tur.id_turma AS id_turma,
    tur.st_turma,
    tur.dt_inicio AS dt_inicioturma,
    tur.dt_fim AS dt_terminoturma,
    flr.nu_numero AS nu_resolucao,
    flpr.id_fundamentolegal AS id_fundamentoparecer,
    flpr.nu_numero AS nu_parecer,
    flp.id_fundamentolegal AS id_fundamentoportaria,
    flp.nu_numero AS nu_portaria,
    et.id_entidade,
    et.st_nomeentidade,
    et.st_razaosocial,
    et.st_cnpj,
    et.st_urlimglogo,
    et.st_urlsite,
    ps.st_nomepais,
    ps.st_sexo AS sexo_aluno,
    UPPER(tcc.st_tituloavaliacao) AS st_titulomonografia,
    CONVERT(VARCHAR(2), DATEPART(DAY, GETDATE()), 103) + ' de '
    + dbo.fn_mesporextenso(GETDATE()) + ' de '
    + CONVERT(VARCHAR(4), DATEPART(YEAR, GETDATE()), 103) AS st_data,
    DAY(GETDATE()) AS st_dia,
    LOWER(dbo.fn_mesporextenso(GETDATE())) AS st_mes,
    YEAR(GETDATE()) AS st_ano,
--CONVERT(DATE, sa.dt_abertura, 103) AS dt_primeirasala ,
    (
      SELECT
        TOP 1
        ts.dt_abertura
      FROM tb_matriculadisciplina tm
        JOIN tb_alocacao ta ON tm.id_matriculadisciplina = ta.id_matriculadisciplina
        JOIN tb_saladeaula ts ON ta.id_saladeaula = ts.id_saladeaula
      WHERE tm.id_matricula = mt.id_matricula AND tm.bl_obrigatorio = 1
      ORDER BY
        ts.dt_abertura ASC
    ) AS dt_primeirasala,
--CONVERT(DATE, DATEADD(MONTH, 13, sa.dt_abertura), 103) AS dt_previsaofim ,
    (
      SELECT
        TOP 1
--DATEADD(MONTH, 13, ts.dt_abertura)
          'dt_previsaotermino' = CASE WHEN ts.id_categoriasala = 2
          THEN (
            DATEADD(DAY, ISNULL(ts.nu_diasaluno, 0), ts.dt_abertura)
          )
                                 ELSE (
                                   CASE WHEN ts.dt_encerramento IS NOT NULL
                                     THEN (
                                       DATEADD(DAY, ISNULL(ts.nu_diasextensao, 0) + 30, ts.dt_encerramento)
                                     )
                                   WHEN ts.dt_abertura IS NOT NULL
                                     THEN (
                                       DATEADD(DAY, ISNULL(ts.nu_diasextensao, 0) + 30, ts.dt_abertura)
                                     )
                                   ELSE (
                                     NULL
                                   ) END
                                 ) END
      FROM tb_matriculadisciplina tm
        JOIN tb_alocacao ta ON tm.id_matriculadisciplina = ta.id_matriculadisciplina
        JOIN tb_saladeaula ts ON ta.id_saladeaula = ts.id_saladeaula
      WHERE tm.id_matricula = mt.id_matricula AND tm.bl_obrigatorio = 1
      ORDER BY
        dt_previsaotermino DESC,
        ts.dt_abertura DESC,
        ts.dt_encerramento DESC
    ) AS dt_previsaofim,
    UPPER(tcc.st_tituloavaliacao) AS st_titulotcc,
    tcc.dt_aberturasala AS dt_iniciotcc,
    tcc.dt_encerramentosala AS dt_terminotcc,
    tcc.st_nota AS nu_notatcc,
    tcc.dt_cadastroavaliacao AS dt_cadastrotcc,
    tcc.dt_defesa AS dt_defesatcc,
    mt.st_codcertificacao,
    CONVERT(CHAR, GETDATE(), 103) AS st_atual,
    matc.st_codigoacompanhamento AS st_codigorasteamento,
    mt.id_matriculaorigem,
    mto.dt_inicio AS dt_iniciomatriculaorigem,
    CONVERT(VARCHAR(2), DATEPART(DAY, GetDate())) + ' de ' + dbo.fn_mesporextenso(GetDate()) + ' de ' +
    CONVERT(VARCHAR(4), DATEPART(YEAR, GetDate())) AS st_atualextenso,
    CONVERT(VARCHAR(2), DATEPART(DAY, tur.dt_fim)) + ' de ' + dbo.fn_mesporextenso(tur.dt_fim) + ' de ' +
    CONVERT(VARCHAR(4), DATEPART(YEAR, tur.dt_fim)) AS st_terminoturmaextenso,
    CONVERT(VARCHAR(2), DATEPART(DAY, tur.dt_inicio)) + ' de ' + dbo.fn_mesporextenso(tur.dt_inicio) + ' de ' +
    CONVERT(VARCHAR(4), DATEPART(YEAR, tur.dt_inicio)) AS st_inicioturmaextenso,
    CONVERT(CHAR, venda.dt_limiterenovacao, 103) AS st_limiterenovacao,
    primeira_venda.dt_ingresso,
    tpsel.st_descricao AS st_selecaodescricao,
    venda.id_venda AS id_venda,
    venda.nu_mensalidade AS nu_mensalidade,
    venda.nu_mensalidadepon AS nu_mensalidadepontualidade,
    ' - ' AS st_link_grade_portal
-- , ' - ' AS st_municipioufagendamento
    ,
    ac.st_areaconhecimento,
    agendamento.dt_agendamento,
    agendamento.st_horarioaula AS st_horarioaulaagendamento,
    agendamento.st_endereco + ' - ' + agendamento.nu_numero AS st_enderecoagendamento,
    agendamento.st_disciplina AS st_disciplinaagendamento,
    agendamento.id_disciplina AS id_disciplinaagendamento,
    agendamento.st_municipouf AS st_municipioufagendamento,
    CASE
    WHEN pp.bl_novoportal = 1 AND et.st_urlnovoportal IS NOT NULL
      THEN et.st_urlnovoportal
    WHEN pp.bl_novoportal = 0 AND et.st_urlportal IS NOT NULL
      THEN et.st_urlportal
    ELSE 'http://portal.unyleya.edu.br/'
    END AS 'st_url_portal',
      semestre_por_dataatual = (
      CASE WHEN
        CONVERT(VARCHAR(2), DATEPART(MONTH, GETDATE()), 103) < 7
        THEN CONVERT(VARCHAR(4), YEAR(GETDATE())) + '.' + '1'
      WHEN CONVERT(VARCHAR(2), DATEPART(MONTH, GETDATE()), 103) >= 7
        THEN CONVERT(VARCHAR(4), YEAR(GETDATE())) + '.' + '2'
      ELSE NULL
      END),
    mt.nu_semestreatual AS periodo_por_disciplina,
    pp.nu_semestre AS total_periodo_projeto,
    ga.st_grauacademico AS grau_academico_projeto,
    ps.sg_ufnascimento,
    pp.id_grauacademico,
    ga.st_grauacademico,
    areamec.st_areaconhecimento AS st_areaconhecimentomec,
    secretariado.st_nomecompleto AS st_secretariado,
    REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
                                        stuff((
                                                SELECT
                                                  ' ' + upper(left(T3.V, 1)) + lower(stuff(T3.V, 1, 1, ''))
                                                FROM (SELECT
                                                        cast(replace((SELECT
                                                                        munimat.st_nomemunicipio AS '*'
                                                                      FOR XML PATH ('')), ' ', '<X/>') AS XML).query(
                                                            '.')) AS T1(X)
                                                  CROSS APPLY T1.X.nodes('text()') AS T2(X)
                                                  CROSS APPLY (SELECT
                                                                 T2.X.value('.', 'varchar(120)')) AS T3(V)
                                                FOR XML PATH (''), TYPE
                                              ).value('text()[1]', 'varchar(30)'), 1, 1, ''), ' De ', ' de '), ' Da ',
                                    ' da '), ' Do ', ' do '), ' Das ', ' das '), ' Dos ',
            ' dos ') AS st_municipio_entidadematricula
  FROM tb_matricula mt
    JOIN tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
    JOIN tb_entidade AS et ON et.id_entidade = mt.id_entidadeatendimento
    JOIN tb_entidade AS etmat ON etmat.id_entidade = mt.id_entidadematricula
    LEFT JOIN tb_entidadeendereco AS ete ON ete.id_entidade = et.id_entidade
                                            AND ete.bl_padrao = 1
    LEFT JOIN tb_endereco AS etee ON etee.id_endereco = ete.id_endereco
    LEFT JOIN tb_entidadeendereco AS etemat ON etemat.id_entidade = etmat.id_entidade
                                               AND etemat.bl_padrao = 1
    LEFT JOIN tb_endereco AS eteemat ON eteemat.id_endereco = etemat.id_endereco
    JOIN vw_pessoa AS ps ON ps.id_usuario = mt.id_usuario
                            AND ps.id_entidade = mt.id_entidadeatendimento
    LEFT JOIN tb_fundamentolegal AS fl ON fl.id_entidade = mt.id_entidadematriz
                                          AND fl.id_tipofundamentolegal = 1
                                          AND fl.dt_publicacao <= GETDATE()
                                          AND fl.dt_vigencia >= GETDATE()
    LEFT JOIN tb_fundamentolegal AS flp ON flp.id_entidade = mt.id_entidadematriz
                                           AND flp.id_tipofundamentolegal = 2
                                           AND flp.dt_publicacao <= GETDATE()
                                           AND flp.dt_vigencia >= GETDATE()
    LEFT JOIN tb_fundamentolegal AS flr ON flr.id_entidade = mt.id_entidadematriz
                                           AND flr.id_tipofundamentolegal = 3
                                           AND flr.dt_publicacao <= GETDATE()
                                           AND flr.dt_vigencia >= GETDATE()
    LEFT JOIN tb_fundamentolegal AS flpr ON flpr.id_entidade = mt.id_entidadematriz
                                            AND flpr.id_tipofundamentolegal = 4
                                            AND flpr.dt_publicacao <= GETDATE()
                                            AND flpr.dt_vigencia >= GETDATE()
    LEFT JOIN tb_projetopedagogicoserienivelensino AS psn ON psn.id_projetopedagogico = mt.id_projetopedagogico
    LEFT JOIN tb_turma AS tur ON tur.id_turma = mt.id_turma
    JOIN tb_nivelensino AS ne ON ne.id_nivelensino = psn.id_nivelensino
    LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS uper ON uper.id_projetopedagogico = pp.id_projetopedagogico
                                                                AND uper.bl_ativo = 1
                                                                AND uper.bl_titular = 1
    LEFT JOIN dbo.tb_usuario AS us_coordenador ON us_coordenador.id_usuario = uper.id_usuario
    LEFT JOIN tb_matricula AS mto ON mt.id_matriculaorigem = mto.id_matricula
    LEFT JOIN dbo.vw_avaliacaoaluno tcc ON (tcc.id_matricula = mt.id_matricula
                                            AND tcc.id_tipoavaliacao = 6
                                            AND tcc.st_tituloavaliacao IS NOT NULL
                                            AND tcc.id_tipodisciplina = 2
                                            AND tcc.bl_ativo = 1
      )
    LEFT JOIN tb_cancelamento AS canc ON canc.id_cancelamento = mt.id_cancelamento AND mt.id_evolucao = 27
    LEFT JOIN tb_contratomatricula AS cm ON cm.id_matricula = mt.id_matricula
    LEFT JOIN tb_matriculacertificacao AS matc ON matc.id_matricula = mt.id_matricula
    LEFT JOIN tb_areaprojetopedagogico AS app ON pp.id_projetopedagogico = app.id_projetopedagogico
    LEFT JOIN tb_areaconhecimento AS ac ON ac.id_areaconhecimento = app.id_areaconhecimento
    LEFT JOIN tb_pessoadadoscomplementares AS dc ON dc.id_usuario = ps.id_usuario
    LEFT JOIN tb_matriculacertificacao AS mc ON mt.id_matricula = mc.id_matricula
    LEFT JOIN tb_municipio AS dcm ON dc.id_municipioinstituicao = dcm.id_municipio
    LEFT JOIN tb_municipio AS munimat ON munimat.id_municipio = eteemat.id_municipio
    OUTER APPLY (SELECT
                   TOP 1
                   v.id_venda,
                   v.dt_limiterenovacao,
                   format((vp.nu_valorbruto - ((isnull(cm.nu_valordesconto, 0) / 100) * vp.nu_valorbruto)) / 6,
                          '#.00') AS nu_mensalidade,
                   format((((vp.nu_valorbruto - ((isnull(cm.nu_valordesconto, 0) / 100) * vp.nu_valorbruto)) / 6) -
                           (((isnull(pon.nu_valordesconto, 0) / 100) *
                             ((vp.nu_valorbruto - ((isnull(cm.nu_valordesconto, 0) / 100) * vp.nu_valorbruto)) / 6)))),
                          '#.00') AS nu_mensalidadepon
                 FROM tb_vendaproduto AS vp
                   JOIN tb_venda AS v ON v.id_venda = vp.id_venda AND v.dt_limiterenovacao IS NOT NULL
                                         AND vp.id_matricula = mt.id_matricula AND v.id_evolucao IN (7, 9, 10) AND
                                         v.bl_ativo = 1
                   LEFT JOIN tb_campanhacomercial AS cm ON cm.id_campanhacomercial = vp.id_campanhacomercial
                   LEFT JOIN tb_campanhacomercial AS pon ON pon.id_campanhacomercial = v.id_campanhapontualidade
                 ORDER BY
                   v.id_venda DESC) AS venda
    LEFT JOIN tb_vendaproduto AS primeira_venda ON primeira_venda.id_vendaproduto = mt.id_vendaproduto
    LEFT JOIN tb_tiposelecao AS tpsel ON tpsel.id_tiposelecao = primeira_venda.id_tiposelecao
    LEFT JOIN tb_areaconhecimento AS areamec ON (areamec.id_areaconhecimento = pp.id_areaconhecimentomec AND
                                                 areamec.id_origemareaconhecimento = 2)
-- Buscar último diploma gerado
    OUTER APPLY (
                  SELECT
                    TOP 1
                    dt_diplomagerado,
                    st_observacao
                  FROM
                    tb_matriculadiplomacao
                  WHERE
                    bl_ativo = 1
                    AND id_matricula = mt.id_matricula
                  ORDER BY
                    id_matriculadiplomacao DESC
                ) AS mtdp
-- Buscar última data de colação
    OUTER APPLY (
                  SELECT
                    TOP 1
                    dt_colacao
                  FROM
                    tb_matriculacolacao
                  WHERE
                    bl_ativo = 1
                    AND id_matricula = mt.id_matricula
                  ORDER BY
                    id_matriculacolacao DESC
                ) AS mtcl
-- Somar Atividades Complementares DEFERIDAS
    OUTER APPLY (
                  SELECT
                    SUM(nu_horasconvalidada) AS nu_horasaluno
                  FROM
                    tb_atividadecomplementar
                  WHERE
                    id_matricula = mt.id_matricula
                    AND id_situacao = 200
                    AND bl_ativo = 1
                ) AS ativ_comp
-- Buscar último agendamento
    OUTER APPLY (
                  SELECT
                    TOP 1
                    ava.dt_aplicacao AS dt_agendamento,
                    ava.st_horarioaula,
                    ava.st_endereco,
                    ava.st_cidade + ' - ' + ava.sg_uf AS st_municipouf,
                    ava.st_disciplina,
                    ava.id_disciplina,
                    ava.nu_numero
                  FROM
                    vw_avaliacaoagendamento AS ava
                  WHERE
                    ava.id_matricula = mt.id_matricula
                    AND ava.id_situacao = 68
                    AND ava.bl_ativo = 1
                ) AS agendamento
		LEFT JOIN tb_grauacademico AS ga ON ga.id_grauacademico = pp.id_grauacademico
		LEFT JOIN tb_pais pa ON pa.id_pais = ps.id_pais
    LEFT JOIN tb_usuario AS secretariado ON (secretariado.id_usuario = etmat.id_usuariosecretariado)

GO
/****** Object:  View [dbo].[vw_vendausuario]    Script Date: 10/08/2012 17:42:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE VIEW [dbo].[vw_vendausuario] AS
SELECT 
    v.id_usuario, 
    u.st_nomecompleto, 
    u.st_cpf,
    
    v.id_venda,
    v.id_entidade,
    v.id_prevenda,

    v.id_formapagamento,
    fp.st_formapagamento,

    v.id_campanhacomercial,
    cc.st_campanhacomercial,

    v.id_evolucao,
    evo.st_evolucao,
    
    v.id_situacao,
    s.st_situacao,

    v.bl_ativo,

    v.id_atendente,
    v.id_usuariocadastro,
    ucad.st_nomecompleto as st_nomecompletocadastrador,

    v.nu_valorliquido,
    v.nu_valorbruto,
    v.nu_descontoporcentagem,
    v.nu_descontovalor,
    v.nu_juros,
    v.nu_parcelas,
    v.bl_contrato,
    v.dt_cadastro,
    case when vi.st_urlvendaexterna IS NULL then ef.st_linkloja else vi.st_urlvendaexterna end as st_linkloja,
	cat.id_categoriacampanha,
	cat.st_categoriacampanha,
	e.st_nomeentidade

FROM tb_venda v
    JOIN tb_usuario u ON v.id_usuario = u.id_usuario
    JOIN tb_usuario ucad ON v.id_usuariocadastro = ucad.id_usuario
	JOIN tb_entidade as e on e.id_entidade = v.id_entidade
    LEFT JOIN tb_formapagamento fp ON fp.id_formapagamento = v.id_formapagamento
    JOIN tb_evolucao evo ON evo.id_evolucao = v.id_evolucao
    JOIN tb_situacao s ON v.id_situacao = s.id_situacao
    LEFT JOIN tb_campanhacomercial cc ON cc.id_campanhacomercial = v.id_campanhacomercial
	LEFT JOIN tb_categoriacampanha cat ON cc.id_categoriacampanha = cc.id_categoriacampanha
    LEFT JOIN dbo.tb_entidadefinanceiro ef ON ef.id_entidade = v.id_entidade
    LEFT JOIN dbo.tb_vendaintegracao vi ON vi.id_venda = v.id_venda
GO

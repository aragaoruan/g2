CREATE VIEW [dbo].[vw_comissaorecebercoordenador] AS
       SELECT
         DISTINCT
         uper.id_usuario,
         us.st_nomecompleto,
         us.st_cpf,
         uper.id_entidade,
         'Coordenador' AS st_funcao,
         2 AS id_funcao,
         NULL AS id_disciplina,
         NULL AS st_disciplina,
         vp.id_vendaproduto,
         CAST((la.nu_quitado * (uper.nu_porcentagem / 100)) AS FLOAT) AS nu_comissaoreceber,
         la.nu_quitado,
         vp.nu_valorliquido,
         ppd.st_projetopedagogico,
         ppd.id_projetopedagogico,
         vd.dt_confirmacao,
         la.dt_quitado,
         uper.nu_porcentagem
       FROM dbo.tb_venda AS vd
         JOIN tb_lancamentovenda lv ON vd.id_venda = lv.id_venda
         JOIN tb_lancamento la ON lv.id_lancamento = la.id_lancamento AND bl_quitado = 1
         JOIN dbo.tb_vendaproduto AS vp ON vd.id_venda = vp.id_venda
         JOIN dbo.tb_produto AS pd ON vp.id_produto = pd.id_produto
         JOIN dbo.tb_produtoprojetopedagogico AS pp ON pp.id_produto = vp.id_produto
         JOIN dbo.tb_projetopedagogico AS ppd ON ppd.id_projetopedagogico = pp.id_projetopedagogico
         JOIN dbo.tb_modulo AS md ON md.id_projetopedagogico = pp.id_projetopedagogico AND md.bl_ativo = 1
         JOIN dbo.tb_usuarioperfilentidadereferencia AS uper ON md.id_projetopedagogico = uper.id_projetopedagogico
                                                                AND uper.nu_porcentagem IS NOT NULL
         JOIN dbo.tb_usuario AS us ON us.id_usuario = uper.id_usuario
       WHERE vp.id_vendaproduto NOT IN (SELECT
                                          cl.id_vendaproduto
                                        FROM dbo.tb_comissaolancamento AS cl
                                        WHERE
                                          cl.id_vendaproduto = vp.id_vendaproduto AND uper.id_usuario = cl.id_usuario)
go


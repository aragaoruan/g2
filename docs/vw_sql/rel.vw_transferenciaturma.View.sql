CREATE VIEW [rel].[vw_transferenciaturma] AS

SELECT
  mt.id_matricula,
  us.st_nomecompleto,
  us.st_cpf,
  CAST(mt.dt_cadastro AS DATE) AS dt_matricula,
  ev.st_evolucao,
  pp.id_projetopedagogico,
  pp.st_projetopedagogico,
  tm_origem.id_turma AS id_turmaorigem,
  tm_origem.st_turma AS st_turmaorigem,
  tm_destino.id_turma AS id_turmadestino,
  tm_destino.st_turma AS st_turmadestino,
  CAST(tr.dt_cadastro AS DATE) AS dt_transferencia,
  tr_us.st_nomecompleto AS st_responsavel,
  mt.id_entidadematricula AS id_entidade

FROM
  tb_matricula AS mt
  JOIN tb_usuario AS us ON us.id_usuario = mt.id_usuario
  JOIN tb_evolucao AS ev ON ev.id_evolucao = mt.id_evolucao
  JOIN tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
  JOIN tb_turma AS tm_origem ON tm_origem.id_turma = mt.id_turmaorigem
  JOIN tb_turma AS tm_destino ON tm_destino.id_turma = mt.id_turma

  -- Data e responsável pela transferência
  JOIN tb_tramitematricula AS trm ON trm.id_matricula = mt.id_matricula
  JOIN tb_tramite AS tr ON tr.id_tramite = trm.id_tramite AND tr.id_tipotramite = 29
  JOIN tb_usuario AS tr_us ON tr_us.id_usuario = tr.id_usuario
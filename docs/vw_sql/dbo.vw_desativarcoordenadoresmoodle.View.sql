CREATE VIEW [dbo].[vw_desativarcoordenadoresmoodle] AS
  SELECT
    uper.id_usuario,
    uper.id_perfilreferencia,
    u.st_nomecompleto,
    pp.id_projetopedagogico,
    pp.st_projetopedagogico,
    pri.id_perfilreferenciaintegracao,
    pri.id_saladeaulaintegracao,
    pri.bl_encerrado,
    pri.id_sistema,
    usi.st_codusuario,
    sali.st_codsistemacurso,
    usi.id_entidade,
    uper.bl_desativarmoodle,
    sl.id_entidadeintegracao
  FROM
    tb_usuarioperfilentidadereferencia AS uper
    JOIN tb_usuario AS u ON u.id_usuario = uper.id_usuario
    JOIN tb_projetopedagogico AS pp ON pp.id_projetopedagogico = uper.id_projetopedagogico
    JOIN tb_perfilreferenciaintegracao AS pri ON pri.id_perfilreferencia = uper.id_perfilreferencia
                                                 AND pri.id_sistema = 6
                                                 AND pri.bl_encerrado = 0
    JOIN tb_saladeaulaintegracao AS sali ON sali.id_saladeaulaintegracao = pri.id_saladeaulaintegracao
    JOIN tb_saladeaula AS sl ON sl.id_saladeaula = sali.id_saladeaula
    JOIN tb_usuariointegracao AS usi ON usi.id_usuario = uper.id_usuario
                                        AND usi.id_sistema = pri.id_sistema
                                        AND usi.id_entidadeintegracao = sl.id_entidadeintegracao
    JOIN tb_entidadeintegracao AS ei ON ei.id_entidadeintegracao = sl.id_entidadeintegracao
                                        AND usi.id_entidade = ei.id_entidade
                                        AND ei.bl_ativo = 1
                                        AND ei.id_situacao = 198
  WHERE
    uper.bl_desativarmoodle = 1
GO


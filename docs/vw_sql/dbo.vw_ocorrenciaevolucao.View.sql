
GO
/****** Object:  View [dbo].[vw_ocorrenciaevolucao]    Script Date: 10/08/2012 17:42:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_ocorrenciaevolucao] as
SELECT NULL AS id_ocorrencia, ne.id_funcao, ne.id_evolucao, ev.st_evolucao, ne.id_nucleoco  FROM
dbo.tb_nucleoevolucaoco AS ne
JOIN dbo.tb_evolucao AS ev ON ev.id_evolucao = ne.id_evolucao AND ne.id_nucleoco is not null
UNION
SELECT oc.id_ocorrencia AS id_ocorrencia, NULL AS id_funcao, oc.id_evolucao, ev.st_evolucao , null AS id_nucleoco FROM 
dbo.tb_ocorrencia AS oc
JOIN dbo.tb_evolucao AS ev ON ev.id_evolucao = oc.id_evolucao
GO

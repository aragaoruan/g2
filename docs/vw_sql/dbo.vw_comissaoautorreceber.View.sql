
GO

/****** Object:  View [dbo].[vw_comissaoautorreceber]    Script Date: 13/05/2014 14:54:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_comissaoautorreceber] as
SELECT uper.id_usuario, uper.id_entidade,us.st_nomecompleto, 'Autor' AS st_funcao, vp.id_vendaproduto, CAST( (vp.nu_valorliquido * (mdc.nu_ponderacaoaplicada /100) * (uper.nu_porcentagem /100) ) AS SMALLMONEY )AS nu_comissaoreceber, vp.nu_valorliquido, mdc.nu_ponderacaoaplicada, uper.nu_porcentagem  FROM dbo.tb_venda AS vd
JOIN dbo.tb_vendaproduto AS vp ON vd.id_venda = vp.id_venda
JOIN dbo.tb_produto AS pd ON vp.id_produto = pd.id_produto
JOIN dbo.tb_produtoprojetopedagogico AS pp ON pp.id_produto = vp.id_produto
JOIN dbo.tb_modulo AS md ON md.id_projetopedagogico = pp.id_projetopedagogico AND md.bl_ativo = 1
JOIN dbo.tb_modulodisciplina AS mdc ON md.id_modulo = mdc.id_modulo AND mdc.nu_ponderacaoaplicada IS NOT null
JOIN dbo.tb_usuarioperfilentidadereferencia AS uper ON mdc.id_disciplina = uper.id_disciplina AND uper.nu_porcentagem IS NOT null
JOIN dbo.tb_usuario AS us ON us.id_usuario = uper.id_usuario
WHERE vp.id_vendaproduto NOT IN (SELECT cl.id_vendaproduto FROM dbo.tb_comissaolancamento AS cl WHERE cl.id_vendaproduto = vp.id_vendaproduto AND uper.id_usuario = cl.id_usuario )
--EXCEPT

GO



CREATE VIEW dbo.vw_nucleotelemarketingentidade AS (
  SELECT DISTINCT
    nt.id_nucleotelemarketing,
    nt.st_nucleotelemarketing,
    nt.bl_ativo,
    er.id_entidade,
    nt.id_entidade AS id_entidadenucleo
  FROM tb_nucleotelemarketing AS nt
    -- Os núcleos devem ser buscados tanto da entidade em que foram cadastrados, quanto da entidade PAI (apenas o primeiro pai).
    JOIN vw_entidaderecursiva AS er ON nt.id_entidade IN(er.id_entidade, er.id_entidadepai)
)
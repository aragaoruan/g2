
GO
/****** Object:  View [dbo].[vw_vendaatendimento]    Script Date: 10/08/2012 17:42:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_vendaatendimento] as
select
usc.id_usuario as id_usuariocliente, usc.st_nomecompleto as st_nomecliente, usa.id_usuario as id_usuarioatendente, usa.st_nomecompleto as st_nomeatendente,
av.id_nucleotm,NC.st_nucleotm,tm.id_tramite,tm.dt_cadastro as dt_ultimainteracao, vd.id_evolucao, ev.st_evolucao, vd.id_situacao, st.st_situacao, vd.id_entidade, vd.dt_cadastro,
vd.id_venda, vd.id_prevenda, vd.bl_ativo, vd.id_protocolo, vd.dt_agendamento
from tb_venda as vd
JOIN tb_atendentevenda as av ON av.id_venda = vd.id_venda and av.bl_ativo = 1
JOIN tb_situacao as st ON st.id_situacao = vd.id_situacao
JOIN tb_evolucao as ev ON ev.id_evolucao = vd.id_evolucao
LEFT JOIN tb_tramite as tm ON tm.id_tramite =
 (select top 1 tmi.id_tramite 
 from tb_tramitevenda tv, tb_tramite tmi 
 where tmi.id_tramite = tv.id_tramite and tv.id_venda = vd.id_venda
 order by tmi.dt_cadastro desc)
JOIN tb_usuario as usc ON usc.id_usuario = vd.id_usuario
JOIN tb_usuario as usa ON usa.id_usuario = av.id_usuario
JOIN tb_nucleotm as nc ON nc.id_nucleotm = av.id_nucleotm
GO

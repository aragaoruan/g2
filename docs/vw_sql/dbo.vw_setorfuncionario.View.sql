
GO

/****** Object:  View [dbo].[vw_assuntoco]    Script Date: 01/02/2013 14:05:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_setorfuncionario] AS
SELECT DISTINCT 
	sf.id_setorfuncionario,
	sf.id_cargo,
	c.st_cargo,
	sf.id_funcao,
	f.st_funcao,
	sf.id_setor,
	s.st_setor,
	sf.id_usuario,
	u.st_nomecompleto,
	sf.st_ramal
FROM dbo.tb_setorfuncionario AS sf
JOIN dbo.tb_cargo AS c ON c.id_cargo = sf.id_cargo
JOIN dbo.tb_funcao AS f ON f.id_funcao = sf.id_funcao
JOIN dbo.tb_setor AS s ON s.id_setor = sf.id_setor
JOIN dbo.tb_usuario AS u ON u.id_usuario = sf.id_usuario
GO
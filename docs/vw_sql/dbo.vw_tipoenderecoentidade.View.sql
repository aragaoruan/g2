
GO
/****** Object:  View [dbo].[vw_tipoenderecoentidade]    Script Date: 10/08/2012 17:42:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_tipoenderecoentidade] AS
SELECT distinct te.*, ce.st_categoriaendereco, tee.id_entidade FROM tb_tipoendereco te, tb_tipoenderecoentidade tee, tb_categoriaendereco ce
WHERE te.id_tipoendereco = tee.id_tipoendereco AND te.id_categoriaendereco = ce.id_categoriaendereco
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_aproveitaravaliacao] AS
SELECT aa1.id_matricula AS id_matriculaorigem,
aa1.id_disciplina AS id_disciplinaorigem,
aa1.id_avaliacao AS id_avaliacaoorigem,
aa1.id_avaliacaoaluno AS id_avaliacaoalunoorigem,
aa1.st_nota AS st_notaorigem,
aa1.st_tituloexibicaodisciplina AS st_tituloexibicaodisciplinaorigem,
aa2.id_matricula ,
aa2.id_disciplina ,
aa2.st_tituloexibicaodisciplina ,
aa2.st_nota ,
aa2.id_avaliacao ,
aa2.st_avaliacao,
aa2.id_avaliacaoconjuntoreferencia FROM dbo.vw_avaliacaoaluno_transferencia AS aa1
JOIN vw_avaliacaoaluno_transferencia AS aa2 ON aa1.id_disciplina = aa2.id_disciplina AND aa1.id_avaliacao = aa2.id_avaliacao AND aa2.id_matricula != aa1.id_matricula
JOIN dbo.tb_encerramentosala AS es ON es.id_saladeaula = aa1.id_saladeaula WHERE aa1.st_nota IS NOT NULL

GO
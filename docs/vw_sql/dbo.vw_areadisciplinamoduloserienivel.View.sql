/****** Object:  View [dbo].[vw_areadisciplinamoduloserienivel]    Script Date: 10/08/2012 17:41:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vw_areadisciplinamoduloserienivel] as
select d.id_disciplina, st_disciplina, d.st_descricao, acsne.id_areaconhecimento, ac.id_entidade, ac.st_areaconhecimento, ac.bl_ativo,
	   ne.st_nivelensino, sne.id_nivelensino, sne.id_serie, s.id_serieanterior, s.st_serie, m.id_modulo, m.st_modulo, m.st_tituloexibicao,
	   td.id_tipodisciplina, td.st_tipodisciplina
from tb_modulo as m
INNER JOIN tb_modulodisciplina as md ON md.id_modulo = m.id_modulo
LEFT JOIN tb_moduloserienivelensino as msne ON msne.id_modulo = m.id_modulo
INNER JOIN tb_disciplina as d ON d.id_disciplina = md.id_disciplina
INNER JOIN tb_areadisciplina as ad ON ad.id_disciplina = d.id_disciplina
INNER JOIN tb_tipodisciplina as td ON td.id_tipodisciplina = d.id_tipodisciplina
LEFT JOIN tb_areaconhecimentoserienivelensino as acsne ON acsne.id_areaconhecimento = ad.id_areaconhecimento
INNER JOIN tb_areaconhecimento as ac ON ac.id_areaconhecimento = ad.id_areaconhecimento
LEFT JOIN tb_serienivelensino as sne ON sne.id_nivelensino = acsne.id_nivelensino AND sne.id_serie = acsne.id_serie AND
										msne.id_nivelensino = sne.id_nivelensino AND msne.id_serie = sne.id_serie
LEFT JOIN tb_nivelensino as ne ON ne.id_nivelensino = sne.id_nivelensino
LEFT JOIN tb_serie as s ON s.id_serie = sne.id_serie
GO

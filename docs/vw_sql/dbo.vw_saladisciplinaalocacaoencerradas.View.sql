CREATE VIEW [dbo].[vw_saladisciplinaalocacaoencerradas]
AS
SELECT
  aps.id_saladeaula,
  sa.st_saladeaula,
  sa.nu_maxalunos,
  sa.nu_diasaluno,
  aps.id_projetopedagogico,
  pp.id_trilha,
  mt.id_matricula,
  ds.id_disciplina,
  md.id_matriculadisciplina,
  mt.id_usuario,
  al.id_alocacao,
  se.id_entidade,
  (SELECT
    COUNT(alc.id_alocacao)
  FROM tb_alocacao alc,
       dbo.tb_matriculadisciplina mdc
  WHERE alc.id_saladeaula = sa.id_saladeaula
  AND mdc.id_matriculadisciplina = alc.id_matriculadisciplina
  AND mdc.id_evolucao = 13)  AS nu_alocados,
  sa.dt_abertura,
  sa.dt_encerramento,
  sa.id_categoriasala,
  pp.st_projetopedagogico,
  al.bl_ativo,
  DATEADD(DAY, ISNULL(sa.nu_diasextensao,0),sa.dt_encerramento) as dt_encerramentoext
FROM tb_matricula AS mt
JOIN tb_projetopedagogico AS pp
  ON mt.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_areaprojetosala AS aps
  ON aps.id_projetopedagogico = mt.id_projetopedagogico
JOIN tb_saladeaula AS sa
  ON sa.id_saladeaula = aps.id_saladeaula
  AND sa.id_situacao = 8
  AND sa.bl_ativa = 1
JOIN tb_disciplinasaladeaula AS ds
  ON ds.id_saladeaula = sa.id_saladeaula
JOIN tb_matriculadisciplina AS md
  ON md.id_matricula = mt.id_matricula
  AND ds.id_disciplina = md.id_disciplina
JOIN tb_saladeaulaentidade AS se
  ON se.id_saladeaula = sa.id_saladeaula
  AND se.id_entidade = mt.id_entidadeatendimento
LEFT JOIN tb_alocacao al
  ON al.id_matriculadisciplina = md.id_matriculadisciplina
  AND al.id_saladeaula = sa.id_saladeaula
  AND al.bl_ativo = '1'

UNION
  SELECT
  aps.id_saladeaula,
  sa.st_saladeaula,
  sa.nu_maxalunos,
  sa.nu_diasaluno,
  aps.id_projetopedagogico,
  pp.id_trilha,
  mt.id_matricula,
  ds.id_disciplina,
  md.id_matriculadisciplina,
  mt.id_usuario,
  al.id_alocacao,
  ve.id_entidade,
  (SELECT
    COUNT(alc.id_alocacao)
  FROM tb_alocacao alc,
       dbo.tb_matriculadisciplina mdc
  WHERE alc.id_saladeaula = sa.id_saladeaula
  AND mdc.id_matriculadisciplina = alc.id_matriculadisciplina
  AND mdc.id_evolucao = 13)
  AS nu_alocados,
  sa.dt_abertura,
  sa.dt_encerramento,
  sa.id_categoriasala,
  pp.st_projetopedagogico,
  al.bl_ativo,
  DATEADD(DAY, ISNULL(sa.nu_diasextensao,0),sa.dt_encerramento) as dt_encerramentoext
FROM tb_matricula AS mt
JOIN tb_projetopedagogico AS pp
  ON mt.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_areaprojetosala AS aps
  ON aps.id_projetopedagogico = mt.id_projetopedagogico
JOIN tb_saladeaula AS sa
  ON sa.id_saladeaula = aps.id_saladeaula
  AND sa.id_situacao = 8
  AND sa.bl_ativa = 1 AND sa.bl_todasentidades = 1
JOIN tb_disciplinasaladeaula AS ds
  ON ds.id_saladeaula = sa.id_saladeaula
JOIN tb_matriculadisciplina AS md
  ON md.id_matricula = mt.id_matricula
  AND ds.id_disciplina = md.id_disciplina
JOIN vw_entidaderecursivaid AS ve
  ON ve.id_entidade = mt.id_entidadeatendimento
  AND ve.id_entidadepai = sa.id_entidade
LEFT JOIN tb_alocacao al
  ON al.id_matriculadisciplina = md.id_matriculadisciplina
  AND al.id_saladeaula = sa.id_saladeaula
  AND al.bl_ativo = '1'

UNION
  SELECT
  aps.id_saladeaula,
  sa.st_saladeaula,
  sa.nu_maxalunos,
  sa.nu_diasaluno,
  aps.id_projetopedagogico,
  pp.id_trilha,
  mt.id_matricula,
  ds.id_disciplina,
  md.id_matriculadisciplina,
  mt.id_usuario,
  al.id_alocacao,
  ve.id_entidade,
  (SELECT
    COUNT(alc.id_alocacao)
  FROM tb_alocacao alc,
       dbo.tb_matriculadisciplina mdc
  WHERE alc.id_saladeaula = sa.id_saladeaula
  AND mdc.id_matriculadisciplina = alc.id_matriculadisciplina
  AND mdc.id_evolucao = 13)
  AS nu_alocados,
  sa.dt_abertura,
  sa.dt_encerramento,
  sa.id_categoriasala,
  pp.st_projetopedagogico,
  al.bl_ativo,
  DATEADD(DAY, ISNULL(sa.nu_diasextensao,0),sa.dt_encerramento) as dt_encerramentoext
FROM tb_matricula AS mt
JOIN tb_projetopedagogico AS pp
  ON mt.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_areaprojetosala AS aps
  ON aps.id_projetopedagogico = mt.id_projetopedagogico
JOIN tb_saladeaula AS sa
  ON sa.id_saladeaula = aps.id_saladeaula
  AND sa.id_situacao = 8
  AND sa.bl_ativa = 1 AND sa.bl_todasentidades = 1
JOIN tb_disciplinasaladeaula AS ds
  ON ds.id_saladeaula = sa.id_saladeaula
JOIN tb_matriculadisciplina AS md
  ON md.id_matricula = mt.id_matricula
  AND ds.id_disciplina = md.id_disciplina
JOIN tb_entidade AS ve
  ON ve.id_entidade = sa.id_entidade
LEFT JOIN tb_alocacao al
  ON al.id_matriculadisciplina = md.id_matriculadisciplina
  AND al.id_saladeaula = sa.id_saladeaula
  AND al.bl_ativo = '1'


GO



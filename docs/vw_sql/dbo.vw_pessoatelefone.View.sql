
GO

/****** Object:  View [dbo].[vw_pessoatelefone]    Script Date: 11/04/2014 19:14:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 CREATE VIEW [dbo].[vw_pessoatelefone] AS
select ct.id_telefone, ct.nu_telefone, ct.id_tipotelefone, tt.st_tipotelefone, ctp.id_usuario, ctp.id_entidade, ctp.bl_padrao, ct.nu_ddd, ct.nu_ddi, p.st_nomeexibicao
	from tb_contatostelefone as ct
	INNER JOIN tb_contatostelefonepessoa as ctp ON ctp.id_telefone = ct.id_telefone
	INNER JOIN tb_tipotelefone as tt ON tt.id_tipotelefone = ct.id_tipotelefone
	INNER JOIN tb_pessoa as p ON p.id_usuario = ctp.id_usuario and p.id_entidade = ctp.id_entidade
GO

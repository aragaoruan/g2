
GO
/****** Object:  View [dbo].[vw_pesquisarprotocolo]    Script Date: 10/08/2012 17:42:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_pesquisarprotocolo] as
select pt.id_protocolo, pt.dt_cadastro, us.st_nomecompleto from tb_protocolo as pt
JOIN tb_usuario as us ON us.id_usuario = pt.id_usuariocadastro
GO

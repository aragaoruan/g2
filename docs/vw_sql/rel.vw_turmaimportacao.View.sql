CREATE VIEW [rel].[vw_turmaimportacao] AS
  SELECT
    mt.id_usuario,
    mt.id_matricula,
    tm.id_turma,
    tm.st_turma,
    tm.dt_fim,
    en.id_entidade,
    en.st_nomeentidade,
    ps.st_cpf,
    UPPER(ps.st_nomecompleto) AS st_nomecompleto,
    LOWER(ps.st_email) AS st_email
  FROM
    dbo.tb_matricula AS mt
    JOIN dbo.tb_turma AS tm ON mt.id_turma = tm.id_turma
    JOIN dbo.tb_entidade AS en ON en.id_entidade = mt.id_entidadematricula
    JOIN dbo.vw_pessoa AS ps ON ps.id_usuario = mt.id_usuario AND ps.id_entidade = mt.id_entidadeatendimento
/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2014 (12.0.2000)
    Source Database Engine Edition : Microsoft SQL Server Standard Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2014
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/


ALTER VIEW [rel].[vw_alunosnaoalocados] AS
SELECT
  DISTINCT
  pp.id_projetopedagogico,
  pp.st_projetopedagogico,
  mt.id_matricula,
  us.st_nomecompleto,
  us.st_cpf,
  mt.id_turma,
  md.id_matriculadisciplina,
  dc.st_disciplina,
  dc.id_disciplina,
  md.id_evolucao,
  'Não' AS st_alocado,
    st_sala = CASE WHEN sd.id_saladeaula IS NULL
    THEN 'Não'
              ELSE 'Sim' END,
  'Não' AS st_ava,
  mt.id_entidadeatendimento
FROM dbo.tb_matricula AS mt
  JOIN dbo.tb_usuario AS us ON us.id_usuario = mt.id_usuario
  JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
  JOIN dbo.tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula AND md.id_evolucao = 11
  JOIN dbo.tb_disciplina AS dc ON dc.id_disciplina = md.id_disciplina AND dc.id_tipodisciplina != 3
  LEFT JOIN dbo.vw_saladisciplinaturma AS sd ON sd.id_matriculadisciplina = md.id_matriculadisciplina
WHERE mt.bl_ativo = 1 AND mt.id_evolucao = 6 AND NOT exists(SELECT
                                                              id_alocacao
                                                            FROM dbo.tb_alocacao AS AL
                                                            WHERE
                                                              al.id_matriculadisciplina = md.id_matriculadisciplina AND
                                                              al.bl_ativo = 1)
UNION
SELECT
  DISTINCT
  pp.id_projetopedagogico,
  pp.st_projetopedagogico,
  mt.id_matricula,
  us.st_nomecompleto,
  us.st_cpf,
  mt.id_turma,
  md.id_matriculadisciplina,
  dc.st_disciplina,
  dc.id_disciplina,
  md.id_evolucao,
    st_alocado = CASE WHEN al.id_alocacao IS NULL
    THEN 'Não'
                 ELSE 'Sim' END,
  'Sim' AS st_sala,
  'Não' AS st_ava,
  mt.id_entidadeatendimento
FROM dbo.tb_matricula AS mt
  JOIN dbo.tb_usuario AS us ON us.id_usuario = mt.id_usuario
  JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
  JOIN dbo.tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula AND md.id_evolucao = 13
  JOIN dbo.tb_alocacao AS al ON al.id_matriculadisciplina = md.id_matriculadisciplina AND al.bl_ativo = 1
  LEFT JOIN dbo.tb_alocacaointegracao AS ai ON ai.id_alocacao = al.id_alocacao
-- JOIN dbo.tb_alocacao AS al2 ON al2.id_matriculadisciplina = md.id_matriculadisciplina AND al2.bl_ativo = 1
  JOIN dbo.tb_disciplina AS dc ON dc.id_disciplina = md.id_disciplina AND dc.id_tipodisciplina != 3
--LEFT JOIN dbo.vw_saladisciplinaturma AS sd ON sd.id_matriculadisciplina = md.id_matriculadisciplina
WHERE mt.bl_ativo = 1 AND mt.id_evolucao = 6 AND mt.id_turma IS NOT NULL AND ai.id_alocacaointegracao IS NULL


GO




GO
/****** Object:  View [dbo].[vw_prevendadistribuicao]    Script Date: 10/08/2012 17:42:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE VIEW [dbo].[vw_prevendadistribuicao] as
select distinct
        nc.id_nucleotm, pve.id_prevenda ,
        id_formapagamento ,
        id_meiopagamento ,
        pve.id_usuario ,
        pve.id_entidade ,
        st_nomecompleto ,
        st_cpf ,
        st_email ,
        nu_telefonecelular ,
        nu_dddcelular ,
        nu_ddicelular ,
        nu_telefoneresidencial ,
        nu_dddresidencial ,
        nu_ddiresidencial ,
        nu_telefonecomercial ,
        nu_dddcomercial ,
        nu_ddicomercial ,
        st_orgaoexpeditor ,
        id_pais ,
        sg_uf ,
        id_municipio ,
        st_cep ,
        st_endereco ,
        st_bairro ,
        st_complemento ,
        nu_numero ,
        st_sexo ,
        dt_nascimento ,
        id_paisnascimento ,
        sg_ufnascimento ,
        id_municipionascimento ,
        pve.dt_cadastro ,
        nu_lancamentos ,
        nu_juros ,
        nu_desconto ,
        nu_valorliquido ,
        pve.bl_ativo ,
        st_rg ,
        id_tipoendereco ,
        st_contato ,
        st_instituicao ,
        st_graduacao ,
        st_localtrabalho ,
        st_recomendacao ,
        id_nivelensino,
       -- nft.id_usuario AS id_distribuidor,
        COUNT(pvpv.id_produto) AS nu_produtos
from tb_prevenda as pve
JOIN tb_prevendaproduto as pvpv ON pve.id_prevenda = pvpv.id_prevenda
JOIN tb_produtovalor as pv ON pvpv.id_produto = pv.id_produto
JOIN tb_nucleoproduto as np ON pv.id_produto = np.id_produto
JOIN tb_nucleotm as nc ON np.id_nucleotm = nc.id_nucleotm
--JOIN dbo.tb_nucleofuncionariotm AS nft ON nft.id_nucleotm = nc.id_nucleotm AND nft.id_funcao = 3
where pve.id_prevenda not in (SELECT id_prevenda from tb_venda where id_prevenda is not null)
and pve.bl_ativo = 1
GROUP BY
		
		nc.id_nucleotm, pve.id_prevenda ,
        id_formapagamento ,
        id_meiopagamento ,
        pve.id_usuario ,
        pve.id_entidade ,
        st_nomecompleto ,
        st_cpf ,
        st_email ,
        nu_telefonecelular ,
        nu_dddcelular ,
        nu_ddicelular ,
        nu_telefoneresidencial ,
        nu_dddresidencial ,
        nu_ddiresidencial ,
        nu_telefonecomercial ,
        nu_dddcomercial ,
        nu_ddicomercial ,
        st_orgaoexpeditor ,
        id_pais ,
        sg_uf ,
        id_municipio ,
        st_cep ,
        st_endereco ,
        st_bairro ,
        st_complemento ,
        nu_numero ,
        st_sexo ,
        dt_nascimento ,
        id_paisnascimento ,
        sg_ufnascimento ,
        id_municipionascimento ,
        pve.dt_cadastro ,
        nu_lancamentos ,
        nu_juros ,
        nu_desconto ,
        nu_valorliquido ,
        pve.bl_ativo ,
        st_rg ,
        id_tipoendereco ,
        st_contato ,
        st_instituicao ,
        st_graduacao ,
        st_localtrabalho ,
        st_recomendacao ,
        --nft.id_usuario,
        id_nivelensino




GO

CREATE VIEW [dbo].[vw_sincronizarcategoriamoodle] AS
  SELECT
    DISTINCT
    uper.id_perfilreferencia,
    uper.id_usuario,
    uper.id_entidade,
    us.st_nomecompleto,
    ei.nu_contexto AS st_codarea,
    CASE
    WHEN pf.id_perfilpedagogico = 3
      THEN
        ei.nu_perfilobservador
    ELSE
      ei.nu_perfilsuporte
    END AS nu_perfil,
    ei.id_entidadeintegracao
  FROM
    dbo.tb_usuarioperfilentidadereferencia AS uper
    JOIN dbo.tb_usuarioperfilentidade AS upe ON upe.id_entidade = uper.id_entidade
                                                AND uper.id_usuario = upe.id_usuario
                                                AND uper.id_perfil = upe.id_perfil
                                                AND upe.bl_ativo = 1
    JOIN dbo.tb_perfil AS pf ON pf.id_perfil = uper.id_perfil
                                AND pf.id_perfilpedagogico IN (3, 7)
    JOIN dbo.tb_usuario AS us ON us.id_usuario = uper.id_usuario
    JOIN dbo.tb_entidadeintegracao AS ei ON uper.id_entidade = ei.id_entidade
                                            AND ei.id_sistema = 6
                                            AND ei.id_situacao = 198
                                            AND ei.bl_ativo = 1
                                            AND NOT EXISTS(
      SELECT
        usi.id_entidadeintegracao
      FROM
        tb_usuariointegracao AS usi
      WHERE
        usi.id_usuario = uper.id_usuario
        AND usi.id_entidadeintegracao = ei.id_entidadeintegracao
  )
    LEFT JOIN dbo.tb_perfilreferenciaintegracao AS pri ON pri.id_perfilreferencia = uper.id_perfilreferencia
  WHERE
    uper.bl_ativo = 1
GO


CREATE VIEW [dbo].[vw_contratomatriculausuario] AS (

SELECT
  cont.id_contrato,
  cont.id_venda,
  cont.id_usuario,
  contmat.id_matricula,
  pp.st_projetopedagogico,
  evo.st_evolucao,
  mat.id_cancelamento,
  mat.id_evolucao

FROM
  tb_contrato AS cont
JOIN tb_contratomatricula AS contmat
  ON (contmat.id_contrato = cont.id_contrato)
JOIN tb_matricula AS mat
  ON (mat.id_matricula = contmat.id_matricula)
JOIN tb_projetopedagogico AS pp
  ON (pp.id_projetopedagogico = mat.id_projetopedagogico)
JOIN tb_evolucao AS
  evo ON (evo.id_evolucao = mat.id_evolucao)

);
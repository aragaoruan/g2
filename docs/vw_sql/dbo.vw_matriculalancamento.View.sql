/****** Object:  View [dbo].[vw_matriculalancamento]    Script Date: 02/14/2014 10:38:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE VIEW [dbo].[vw_matriculalancamento] AS
SELECT
    mt.id_matricula, 
    us.st_nomecompleto,
    vd.id_venda,
    lc.bl_quitado, 
    lc.dt_cadastro, 
    lc.dt_emissao, 
    lc.dt_prevquitado, 
    lc.dt_quitado, 
    lc.dt_vencimento, 
    lc.id_entidade,
    lc.id_lancamento, 
    lc.id_meiopagamento, 
    mp.st_meiopagamento, 
    lc.id_tipolancamento, 
   -- lc.id_usuariolancamento, 
    
    CASE WHEN lc.id_usuariolancamento IS NOT NULL THEN lc.id_usuariolancamento 
		 WHEN lc.id_entidadelancamento IS NOT NULL THEN lc.id_entidadelancamento END AS id_usuariolancamento, 
   
    CASE WHEN usl.st_nomecompleto IS NOT NULL THEN usl.st_nomecompleto 
		 WHEN el.st_nomeentidade IS NOT NULL THEN el.st_nomeentidade COLLATE DATABASE_DEFAULT END AS st_usuariolancamento,
		 
    lc.nu_valor,
    lc.bl_original
FROM 
    tb_matricula AS mt
    JOIN tb_contratomatricula AS cm ON cm.id_matricula = mt.id_matricula
    JOIN tb_contrato AS ct ON ct.id_contrato = cm.id_contrato
    JOIN tb_venda AS vd ON vd.id_venda = ct.id_venda
    JOIN tb_lancamentovenda AS lv ON lv.id_venda = vd.id_venda
    JOIN tb_lancamento AS lc ON lc.id_lancamento = lv.id_lancamento
    JOIN tb_meiopagamento AS mp ON lc.id_meiopagamento = mp.id_meiopagamento
    JOIN tb_usuario AS us ON us.id_usuario = mt.id_usuario
    --JOIN tb_usuario AS usl ON usl.id_usuario = lc.id_usuariolancamento
    OUTER APPLY (
	   SELECT st_nomecompleto, id_usuario FROM tb_usuario WHERE id_usuario = id_usuariolancamento
	) AS usl
	OUTER APPLY (
	   SELECT st_nomeentidade, id_entidade FROM tb_entidade WHERE id_entidade = id_entidadelancamento
	) AS el
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_campanhavalorproduto] AS
SELECT	
	cc.id_campanhacomercial,
	cc.st_campanhacomercial,
	cc.dt_inicio,
	cc.dt_fim,
	cat.id_categoriacampanha,
	cat.st_categoriacampanha,
	cp.id_produto,
	p.st_produto,
	p.nu_valor,
	CASE td.id_tipodesconto
		WHEN 1 THEN CONVERT(decimal(10,2), cc.nu_valordesconto)
		WHEN 2 THEN CONVERT(decimal(10,2), p.nu_valorvenda * (cc.nu_valordesconto / 100))
	END as nu_valordesconto,
	CASE td.id_tipodesconto 
		WHEN 1 THEN CONVERT(decimal(10,2), p.nu_valorvenda - cc.nu_valordesconto)
		WHEN 2 THEN CONVERT(decimal(10,2), p.nu_valorvenda - (p.nu_valorvenda * (cc.nu_valordesconto/100)))
	END as nu_valorliquido
FROM tb_campanhacomercial cc
JOIN tb_tipodesconto td ON td.id_tipodesconto = cc.id_tipodesconto
JOIN tb_campanhaproduto as cp on cp.id_campanhacomercial = cc.id_campanhacomercial
JOIN vw_produto as p ON p.id_produto = cp.id_produto
JOIN tb_categoriacampanha cat on cat.id_categoriacampanha = cc.id_categoriacampanha
WHERE 
	cc.bl_ativo = 1 --fixo
	and p.bl_ativo = 1 -- fixo
	and cc.dt_inicio <= GETDATE()
	and (cc.dt_fim >= GETDATE() OR cc.dt_fim IS NULL)

UNION

SELECT	
	cc.id_campanhacomercial,
	cc.st_campanhacomercial,
	cc.dt_inicio,
	cc.dt_fim,
	cat.id_categoriacampanha,
	cat.st_categoriacampanha,
	p.id_produto,
	p.st_produto,
	p.nu_valor,
	CASE td.id_tipodesconto
		WHEN 1 THEN CONVERT(decimal(10,2), cc.nu_valordesconto)
		WHEN 2 THEN CONVERT(decimal(10,2), p.nu_valorvenda * (cc.nu_valordesconto / 100))
	END as nu_valordesconto,
	CASE td.id_tipodesconto 
		WHEN 1 THEN CONVERT(decimal(10,2), p.nu_valorvenda - cc.nu_valordesconto)
		WHEN 2 THEN CONVERT(decimal(10,2), p.nu_valorvenda - (p.nu_valorvenda * (cc.nu_valordesconto/100)))
	END as nu_valorliquido
FROM tb_campanhacomercial cc
JOIN tb_tipodesconto td ON td.id_tipodesconto = cc.id_tipodesconto
JOIN vw_produto as p ON p.id_entidade = cc.id_entidade
JOIN tb_categoriacampanha cat on cat.id_categoriacampanha = cc.id_categoriacampanha
WHERE 
	cc.bl_ativo = 1 --fixo
	and p.bl_ativo = 1 -- fixo
	and cc.dt_inicio <= GETDATE()
	and (cc.dt_fim >= GETDATE() OR cc.dt_fim IS NULL)
	AND cc.bl_todosprodutos = 1


	



GO



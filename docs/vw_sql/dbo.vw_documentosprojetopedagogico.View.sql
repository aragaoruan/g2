
GO
/****** Object:  View [dbo].[vw_documentosprojetopedagogico]    Script Date: 10/08/2012 17:41:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_documentosprojetopedagogico] as
select dp.id_documentos, dc.st_documentos, dp.id_projetopedagogico, pp.st_projetopedagogico
from tb_documentosprojetopedagogico as dp
JOIN tb_projetopedagogico as pp ON pp.id_projetopedagogico = dp.id_projetopedagogico
JOIN tb_documentos as dc ON dc.id_documentos = dp.id_documentos
GO

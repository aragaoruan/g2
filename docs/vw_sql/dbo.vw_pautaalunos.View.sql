CREATE VIEW [dbo].[vw_pautaalunos]
	AS
	SELECT DISTINCT 
	u.id_usuario
	,u.st_nomecompleto AS st_aluno 
	, aa.st_tituloexibicaodisciplina as st_disciplina
	, professor.st_nomecompleto as st_professor
	, md.id_disciplina 
	, mt.id_projetopedagogico
	, pp.st_tituloexibicao as st_projetopedagogico
	, se.id_entidade
	FROM tb_usuario u 
	JOIN tb_matricula mt ON mt.id_usuario = u.id_usuario AND mt.id_evolucao = 6 AND mt.bl_ativo = 1 AND mt.bl_institucional = 0
	JOIN vw_matriculadisciplina as md ON md.id_matricula = mt.id_matricula
	JOIN tb_alocacao al ON al.id_matriculadisciplina = md.id_matriculadisciplina AND al.bl_ativo = 1
	JOIN tb_saladeaula sa ON sa.id_saladeaula = al.id_saladeaula AND sa.dt_encerramento IS NOT NULL
	AND (DATEADD(DAY, ISNULL ( sa.nu_diasextensao , 0 ), sa.dt_encerramento) < CAST(GETDATE() AS DATE)) 
	JOIN tb_saladeaulaentidade se ON sa.id_saladeaula = se.id_saladeaula --AND se.id_entidade = 14 
	JOIN vw_avaliacaoaluno AS aa ON aa.id_matriculadisciplina = md.id_matriculadisciplina and aa.id_matricula = mt.id_matricula and
							aa.id_saladeaula = sa.id_saladeaula and aa.st_nota is NULL
							and aa.id_tipoavaliacao = 4
	JOIN tb_projetopedagogico pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
	LEFT JOIN tb_usuarioperfilentidadereferencia uper ON  uper.id_saladeaula = sa.id_saladeaula -- AND uper.id_entidade = se.id_entidade 
	AND uper.bl_titular = 1 AND uper.bl_ativo = 1
	--JOIN tb_perfil p ON p.id_perfil = uper.id_perfil AND p.id_perfilpedagogico = 1 AND p.id_entidade = se.id_entidade
	LEFT JOIN tb_usuario as professor ON professor.id_usuario = uper.id_usuario
	
--WHERE md.id_disciplina = 4105


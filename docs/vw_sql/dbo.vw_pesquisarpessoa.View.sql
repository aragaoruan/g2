SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vw_pesquisarpessoa]
AS
  SELECT
    u.id_usuario,
    p.id_entidade,
    e.st_nomeentidade AS st_entidade,
    u.st_nomecompleto,
    u.st_cpf,
    m.id_municipio,
    p.sg_uf,
    m.st_nomemunicipio,
    ce.st_email,
    p.bl_ativo,
    sit.id_situacao,
    sit.st_situacao,
    p.st_sexo,
    p.dt_nascimento,
    p.st_nomepai,
    p.st_nomemae,
    CAST(ct.nu_ddd AS VARCHAR) + '-' + CAST(ct.nu_telefone AS VARCHAR) AS st_telefone,
    CAST(ct2.nu_ddd AS VARCHAR) + '-'
    + CAST(ct2.nu_telefone AS VARCHAR) AS st_telefonealternativo,
    p.st_identificacao,
    p.st_passaporte,
    rl.st_cpf AS st_cpfresponsavellegal,
    rl.st_nomecompleto AS st_nomeresponsavellegal,
    rl.nu_ddd AS nu_dddresponsavellegal,
    rl.nu_telefone AS nu_telefoneresponsavellegal
  FROM dbo.tb_usuario u
    INNER JOIN dbo.tb_pessoa AS p
      ON p.id_usuario = u.id_usuario
    INNER JOIN dbo.tb_situacao AS sit
      ON p.id_situacao = sit.id_situacao
    INNER JOIN dbo.tb_entidade AS e
      ON e.id_entidade = p.id_entidade
    LEFT JOIN dbo.tb_municipio AS m
      ON m.id_municipio = p.id_municipio
    LEFT JOIN dbo.tb_contatosemailpessoaperfil AS cepp
      ON cepp.id_entidade = p.id_entidade
         AND cepp.id_usuario = p.id_usuario
         AND cepp.bl_padrao = 1
    LEFT JOIN dbo.tb_contatosemail AS ce
      ON ce.id_email = cepp.id_email
--LEFT JOIN tb_usuarioperfilentidade as upe ON upe.id_entidade = p.id_entidade and upe.id_usuario = p.id_usuario
--LEFT JOIN tb_perfil as per ON per.id_entidade = upe.id_entidade and per.id_entidade = cepp.id_entidade and per.id_perfil = cepp.id_perfil -- olha o perfil
    LEFT JOIN tb_contatostelefonepessoa AS ctp
      ON ctp.id_usuario = u.id_usuario
         AND ctp.id_entidade = p.id_entidade
         AND ctp.bl_padrao = 1
    LEFT JOIN tb_contatostelefonepessoa AS ctp2
      ON ctp2.id_usuario = u.id_usuario
         AND ctp2.id_entidade = p.id_entidade
         AND ctp2.bl_padrao = 0
    LEFT JOIN tb_contatostelefone AS ct
      ON ct.id_telefone = ctp.id_telefone
    LEFT JOIN tb_contatostelefone AS ct2
      ON ct2.id_telefone = ctp2.id_telefone
    LEFT JOIN dbo.tb_responsavellegal AS rl
      ON rl.id_usuario = u.id_usuario
         AND rl.bl_ativo = 1
  WHERE u.bl_ativo = 1
GO

GO


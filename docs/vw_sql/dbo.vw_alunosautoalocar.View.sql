CREATE VIEW [dbo].[vw_alunosautoalocar] AS

WITH _config AS (
  SELECT
    557 AS DISC_AMBIENTACAO,
    22 AS LINHA_DE_NEGOCIO,
    2 AS GRADUACAO,
    6 AS MATRICULA_CURSANDO,
    10 AS VENDA_CONFIRMADA,
    CONVERT(DATE, GETDATE()) AS TODAY
)

SELECT 
  pp.id_projetopedagogico,
  pp.st_projetopedagogico,
  mt.id_matricula,
  us.st_nomecompleto,
  us.st_cpf,
  mt.id_turma,
  md.id_evolucao,
  mt.id_entidadeatendimento,
  md.id_matriculadisciplina,
  dc.st_disciplina

FROM
  dbo.tb_matriculadisciplina AS md
  CROSS APPLY _config AS _
  JOIN dbo.tb_matricula AS mt ON md.id_matricula = mt.id_matricula
    AND mt.id_turma IS NOT NULL
    AND mt.bl_ativo = 1
    AND mt.id_evolucao = _.MATRICULA_CURSANDO
  JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico AND pp.bl_autoalocaraluno = 1
  JOIN dbo.tb_usuario AS us ON us.id_usuario = mt.id_usuario
  JOIN dbo.tb_disciplina AS dc ON dc.id_disciplina = md.id_disciplina AND dc.id_disciplina != _.DISC_AMBIENTACAO
  JOIN dbo.vw_entidadeesquemaconfiguracao AS eec ON eec.id_itemconfiguracao = _.LINHA_DE_NEGOCIO
    AND mt.id_entidadeatendimento = eec.id_entidade
    AND eec.st_valor != _.GRADUACAO
		--AND dc.id_tipodisciplina IN (1,2)

WHERE
  md.id_evolucao = 11
  AND (
    md.dt_tentativaalocacao IS NULL
    OR md.dt_tentativaalocacao < _.TODAY
  )
  AND NOT EXISTS (
    SELECT 1 --al.id_matriculadisciplina
    FROM dbo.tb_alocacao AS al
    WHERE al.id_matriculadisciplina = md.id_matriculadisciplina AND al.bl_ativo = 1
  )
  AND EXISTS (
    SELECT 1 -- sla.id_matriculadisciplina
    FROM dbo.vw_saladisciplinaturma AS sla
    WHERE
      sla.id_matricula = mt.id_matricula
      AND sla.id_matriculadisciplina = md.id_matriculadisciplina
      AND sla.id_alocacao IS NULL
      AND sla.nu_maxalunos > sla.nu_alocados
      AND sla.nu_maxalunos > 0
  )

UNION ALL

SELECT 
  pp.id_projetopedagogico,
  pp.st_projetopedagogico,
  mt.id_matricula,
  us.st_nomecompleto,
  us.st_cpf,
  mt.id_turma,
  md.id_evolucao,
  mt.id_entidadeatendimento,
  md.id_matriculadisciplina,
  dc.st_disciplina

FROM
  dbo.tb_matriculadisciplina AS md
  CROSS APPLY _config AS _
  JOIN dbo.tb_matricula AS mt ON md.id_matricula = mt.id_matricula
    AND mt.id_turma IS NULL
    AND mt.bl_ativo = 1
    AND mt.id_evolucao = _.MATRICULA_CURSANDO
  JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico AND pp.bl_autoalocaraluno = 1
  JOIN dbo.tb_usuario AS us ON us.id_usuario = mt.id_usuario
  JOIN dbo.tb_disciplina AS dc ON dc.id_disciplina = md.id_disciplina AND dc.id_disciplina != _.DISC_AMBIENTACAO
  JOIN dbo.vw_entidadeesquemaconfiguracao AS eec ON eec.id_itemconfiguracao = _.LINHA_DE_NEGOCIO
    AND mt.id_entidadeatendimento = eec.id_entidade
    AND eec.st_valor != _.GRADUACAO
		--AND dc.id_tipodisciplina IN (1,2)

WHERE
  md.id_evolucao = 11
  AND (
    md.dt_tentativaalocacao IS NULL
    OR md.dt_tentativaalocacao < _.TODAY
  )
  AND NOT EXISTS (
    SELECT 1 --- al.id_matriculadisciplina
    FROM dbo.tb_alocacao AS al
    WHERE al.id_matriculadisciplina = md.id_matriculadisciplina AND al.bl_ativo = 1
  )
  AND EXISTS (
    SELECT 1 -- id_saladeaula
    FROM dbo.vw_saladisciplinaprojeto AS sla
    WHERE
      sla.id_matricula = mt.id_matricula
      AND sla.id_matriculadisciplina = md.id_matriculadisciplina
      AND sla.id_alocacao IS NULL
  )

UNION ALL

SELECT 
  pp.id_projetopedagogico,
  pp.st_projetopedagogico,
  mt.id_matricula,
  us.st_nomecompleto,
  us.st_cpf,
  mt.id_turma,
  md.id_evolucao,
  mt.id_entidadeatendimento,
  md.id_matriculadisciplina,
  dc.st_disciplina
FROM
  dbo.tb_matriculadisciplina AS md
  CROSS APPLY _config AS _
  JOIN dbo.tb_matricula AS mt ON md.id_matricula = mt.id_matricula
    AND mt.id_turma IS NOT NULL
    AND mt.bl_ativo = 1
    AND mt.id_evolucao = _.MATRICULA_CURSANDO
  JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico --AND pp.bl_autoalocaraluno = 0
  JOIN dbo.tb_usuario AS us ON us.id_usuario = mt.id_usuario
  JOIN dbo.tb_disciplina AS dc ON dc.id_disciplina = md.id_disciplina AND dc.id_disciplina != _.DISC_AMBIENTACAO
  JOIN dbo.vw_entidadeesquemaconfiguracao AS eec ON eec.id_itemconfiguracao = _.LINHA_DE_NEGOCIO
    AND mt.id_entidadeatendimento = eec.id_entidade
    AND eec.st_valor = _.GRADUACAO
WHERE
  md.id_evolucao = 11
  --AND (
  --  md.dt_tentativaalocacao IS NULL
  --  OR md.dt_tentativaalocacao < _.TODAY
  --)
  AND NOT EXISTS (
    SELECT 1 --al.id_matriculadisciplina
    FROM dbo.tb_alocacao AS AL
    WHERE al.id_matriculadisciplina = md.id_matriculadisciplina AND al.bl_ativo = 1
  )
  AND EXISTS (
    SELECT 1 -- sla.id_matriculadisciplina
    FROM dbo.vw_saladisciplinavenda AS sla
    WHERE
      sla.id_matricula = mt.id_matricula AND sla.bl_alocado = 0
      AND sla.id_matriculadisciplina = md.id_matriculadisciplina
      AND sla.id_alocacao IS NULL AND sla.id_evolucaovenda = _.VENDA_CONFIRMADA
  )
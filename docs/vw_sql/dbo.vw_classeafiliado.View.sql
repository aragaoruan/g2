SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_classeafiliado] AS
SELECT DISTINCT 
	c.id_classeafiliado,
	c.id_tipopessoa,
	tp.st_tipopessoa,
	c.id_tipovalorcomissao,
	tv.st_tipovalorcomissao,
	c.id_situacao,
	s.st_situacao,
	c.id_textosistemacontrato,
	t.st_textosistema,
	c.id_entidade,
	c.id_usuariocadastro,
	c.st_classeafiliado,
	c.st_descricao,
	c.dt_cadastro
	,CONVERT(char(20), count (ca.id_contratoafiliado) )  as st_contratoafiliado
	
FROM dbo.tb_classeafiliado AS c
INNER JOIN tb_tipopessoa tp        ON c.id_tipopessoa           = tp.id_tipopessoa
INNER JOIN tb_tipovalorcomissao tv ON c.id_tipovalorcomissao    = tv.id_tipovalorcomissao
INNER JOIN tb_situacao s	       ON c.id_situacao             = s.id_situacao
INNER JOIN tb_textosistema t       ON c.id_textosistemacontrato = t.id_textosistema
LEFT JOIN tb_contratoafiliado ca  ON c.id_classeafiliado       = ca.id_classeafiliado

Group by c.id_classeafiliado,
	c.id_tipopessoa,
	tp.st_tipopessoa,
	c.id_tipovalorcomissao,
	tv.st_tipovalorcomissao,
	c.id_situacao,
	s.st_situacao,
	c.id_textosistemacontrato,
	t.st_textosistema,
	c.id_entidade,
	c.id_usuariocadastro,
	c.st_classeafiliado,
	c.st_descricao,
	c.dt_cadastro,
	ca.id_contratoafiliado

GO
/****** Object:  View [dbo].[vw_saladisciplinaentidade]    Script Date: 26/10/2015 14:44:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[vw_saladisciplinaentidade] as
select ta.id_projetopedagogico, ts3.id_entidade, td1.id_disciplina,ts.st_saladeaula, ts.dt_inicioinscricao, ts.dt_fiminscricao, ts.dt_abertura, ts.dt_encerramento
, tu1.st_nomecompleto AS st_professor, ts1.bl_conteudo, ts2.st_sistema , st_disciplina
,ts.id_saladeaula
,ts.id_periodoletivo
from dbo.tb_disciplinasaladeaula AS td1
JOIN dbo.tb_disciplina AS dc ON td1.id_disciplina = dc.id_disciplina
JOIN tb_saladeaulaentidade ts3 ON td1.id_saladeaula = ts3.id_saladeaula
JOIN dbo.tb_areaprojetosala AS ta
	ON  ta.id_saladeaula = ts3.id_saladeaula AND ta.id_saladeaula = td1.id_saladeaula
JOIN dbo.tb_saladeaula AS ts
	ON td1.id_saladeaula = ts.id_saladeaula AND ta.id_saladeaula = ts.id_saladeaula  AND ts.id_saladeaula = ts3.id_saladeaula
LEFT OUTER JOIN dbo.tb_usuarioperfilentidadereferencia AS tu
	ON ts.id_saladeaula = tu.id_saladeaula AND tu.bl_ativo = 1 AND tu.bl_titular = 1
LEFT OUTER JOIN dbo.tb_usuario AS tu1
	ON tu.id_usuario = tu1.id_usuario
LEFT OUTER JOIN dbo.tb_saladeaulaintegracao AS ts1
	ON ts.id_saladeaula = ts1.id_saladeaula
LEFT OUTER JOIN dbo.tb_sistema AS ts2
	ON ts1.id_sistema = ts2.id_sistema

GO



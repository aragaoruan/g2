CREATE VIEW dbo.vw_alterarmatriculadecursodeprazo
AS
  SELECT
    id_matricula,
    id_usuario,
    id_entidadematriz,
    id_entidadematricula,
    id_evolucao
  FROM (
         SELECT
           TOP 2000
           mt.id_matricula,
           mt.id_usuario,
           mt.id_entidadematriz,
           mt.id_entidadematricula,
           mt.id_evolucao
         FROM
           tb_matricula AS mt
           LEFT JOIN vw_avaliacaoagendamento AS avag ON avag.id_matricula = mt.id_matricula AND avag.bl_ativo = 1 AND
                                                        avag.id_situacao IN (68, 69)
           INNER JOIN tb_turma AS turm ON mt.id_turma = turm.id_turma
           INNER JOIN tb_logacesso AS logacces ON mt.id_usuario = logacces.id_usuario
-- Quantidade de dias que o aluno não acessa o portal
           JOIN vw_entidadeesquemaconfiguracao AS cfg1 ON cfg1.id_entidade = mt.id_entidadematricula AND
                                                          cfg1.id_itemconfiguracao = 28
-- Quantidade de meses a partir da data de início da turma
           JOIN vw_entidadeesquemaconfiguracao AS cfg2 ON cfg2.id_entidade = mt.id_entidadematricula AND
                                                          cfg2.id_itemconfiguracao = 29
         WHERE
-- A matrícula do aluno deve estar na evolução de matrícula "Cursando"
           mt.id_evolucao = 6
           -- O aluno NÃO possuir qualquer agendamento de prova ATIVO
           AND avag.id_avaliacaoagendamento IS NULL
           AND logacces.id_funcionalidade = 312
           AND mt.id_matricula NOT IN (
             SELECT
               DISTINCT
               md.id_matricula
             FROM tb_matriculadisciplina AS md
               JOIN tb_alocacao AS al ON al.id_matriculadisciplina = md.id_matriculadisciplina AND al.bl_ativo = 1 AND
                                         al.id_situacao = 55
               JOIN tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula AND sa.bl_ativa = 1 AND
                                           sa.id_categoriasala = 2
             WHERE
               md.id_matricula = mt.id_matricula
           )
           AND GETDATE() > DATEADD(M, +CONVERT(INT, cfg1.st_valor), turm.dt_inicio)
           AND GETDATE() > DATEADD(D, +CONVERT(INT, cfg2.st_valor), logacces.dt_cadastro)
       ) AS q1
  GROUP BY
    id_matricula,
    id_usuario,
    id_entidadematriz,
    id_entidadematricula,
    id_evolucao
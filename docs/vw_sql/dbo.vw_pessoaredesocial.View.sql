
GO
/****** Object:  View [dbo].[vw_pessoaredesocial]    Script Date: 10/08/2012 17:42:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_pessoaredesocial] AS	  
select rsp.id_entidade, rsp.id_usuario, rsp.id_redesocial, crs.st_id, trs.id_tiporedesocial, trs.st_tiporedesocial 
	from tb_contatosredesocialpessoa as rsp, tb_contatosredesocial as crs, tb_tiporedesocial as trs, tb_pessoa as p
	where rsp.id_redesocial = crs.id_redesocial and
		  crs.id_tiporedesocial = trs.id_tiporedesocial and
		  p.id_usuario = rsp.id_usuario and
		  p.id_entidade = rsp.id_entidade
GO

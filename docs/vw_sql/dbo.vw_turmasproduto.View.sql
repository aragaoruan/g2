
/****** Object:  View [dbo].[vw_turmasproduto]    Script Date: 10/08/2012 17:42:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE VIEW [dbo].[vw_turmasproduto] AS
SELECT DISTINCT
        td.id_turma ,
        td.nu_alunos ,
        td.id_usuariocadastro ,
        id_situacao ,
        id_entidadecadastro ,
        st_turma ,
        st_tituloexibicao ,
        nu_maxalunos ,
        dt_inicioinscricao ,
        dt_fiminscricao ,
        dt_inicio ,
        dt_fim ,
        td.dt_cadastro ,
        td.bl_ativo ,
        ppp.id_projetopedagogico ,
        ppp.id_produto ,
        ppp.id_entidade FROM dbo.vw_turmasdisponiveis AS td
JOIN dbo.tb_produtoprojetopedagogico AS ppp ON ppp.id_projetopedagogico = td.id_projetopedagogico
JOIN dbo.tb_turmaentidade AS te ON te.id_entidade = ppp.id_entidade AND td.id_turma = te.id_turma
GO



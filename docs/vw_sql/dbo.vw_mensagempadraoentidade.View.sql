
GO
/****** Object:  View [dbo].[vw_mensagempadraoentidade]    Script Date: 10/08/2012 17:42:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_mensagempadraoentidade] as
select
mp.id_mensagempadrao, mp.st_mensagempadrao,mp.id_tipoenvio, eem.id_emailconfig, ec.st_conta, eem.id_textosistema, ts.st_textosistema, eem.id_entidade
from tb_mensagempadrao as mp
LEFT JOIN tb_emailentidademensagem as eem ON eem.id_mensagempadrao = mp.id_mensagempadrao
LEFT JOIN tb_emailconfig as ec ON ec.id_emailconfig = eem.id_emailconfig
LEFT JOIN tb_textosistema as ts ON ts.id_textosistema = eem.id_textosistema
GO

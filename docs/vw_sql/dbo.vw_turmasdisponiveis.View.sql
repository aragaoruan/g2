
GO
/****** Object:  View [dbo].[vw_turmasdisponiveis]    Script Date: 10/08/2012 17:42:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_turmasdisponiveis] as
SELECT  id_turmaprojeto ,
        tp.id_turma ,
        id_projetopedagogico ,
        nu_alunos ,
        tm.id_usuariocadastro ,
        id_situacao ,
        id_entidadecadastro ,
        st_turma ,
        st_tituloexibicao ,
        nu_maxalunos ,
        dt_inicioinscricao ,
        dt_fiminscricao ,
        dt_inicio ,
        dt_fim ,
        tm.dt_cadastro ,
        tm.bl_ativo
FROM    dbo.tb_turmaprojeto AS tp
        OUTER APPLY ( SELECT    COUNT(id_matricula) AS nu_alunos
                      FROM      dbo.tb_matricula
                      WHERE     id_turma = tp.id_turma
                    ) AS mt
        JOIN tb_turma AS tm ON tm.id_turma = tp.id_turma
                               AND  CAST(tm.dt_inicioinscricao AS DATE) <= CAST(GETDATE() AS DATE)
                               AND ( CAST(tm.dt_fiminscricao AS DATE) >= CAST(GETDATE() AS DATE)
                                     OR tm.dt_fiminscricao IS NULL
                                   )
                               AND tm.bl_ativo = 1
                               AND mt.nu_alunos < tm.nu_maxalunos
WHERE   tp.bl_ativo = 1
GO

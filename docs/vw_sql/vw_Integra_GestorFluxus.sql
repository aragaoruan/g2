USE [G2_UNY]
GO

/****** Object:  View [totvs].[vw_Integra_GestorFluxus]    Script Date: 12/12/2013 15:17:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE VIEW [totvs].[vw_Integra_GestorFluxus] AS

SELECT
ei.st_codsistema AS CODCOLIGADA_FCFO,
CASE WHEN lanint.st_codresponsavel IS NOT NULL THEN lanint.st_codresponsavel ELSE NULL END AS CODCFO_FCFO,
replace(usr.st_nomecompleto,'''','') COLLATE SQL_Latin1_General_CP1_CI_AI AS NOMEFANTASIA_FCFO,
replace(usr.st_nomecompleto,'''','') COLLATE SQL_Latin1_General_CP1_CI_AI AS NOME_FCFO,
usr.st_cpf COLLATE SQL_Latin1_General_CP1_CI_AI AS CGCCFO_FCFO,
NULL AS INSCRESTADUAL_FCFO,
'1' AS PAGREC_FCFO,
replace(ed.st_endereco,'''','') AS RUA_FCFO,
ed.nu_numero AS NUMERO_FCFO,
CAST(replace(ed.st_complemento,'''','') AS VARCHAR(30)) AS COMPLEMENTO_FCFO,
CAST(replace(ed.st_bairro,'''','') AS VARCHAR(30)) AS BAIRRO_FCFO,
replace(ed.st_cidade,'''','') AS CIDADE_FCFO,
CODETD_FCFO = CASE WHEN ed.sg_uf IS NOT NULL  THEN ed.sg_uf
ELSE 'DF' END,
REPLACE(REPLACE(ed.st_cep,'.',''),'-','') AS CEP_FCFO,
CAST(ctl.nu_ddd AS VARCHAR)+ ' ' + CAST(ctl.nu_telefone AS VARCHAR) AS TELEFONE_FCFO,
NULL AS FAX_FCFO,
replace(ce.st_email,'''','') AS EMAIL_FCFO,
NULL AS CONTATO_FCFO,
CASE WHEN ps.id_situacao = 1 THEN 1 ELSE 0 END AS ATIVO_FCFO,
mun.nu_codigoibge AS CODMUNICIPIO_FCFO,
CASE WHEN ps.id_usuario IS NOT NULL THEN 'F' ELSE 'J' END AS PESSOAFISOUJUR_FCFO,
pa.st_nomepais AS PAIS_FCFO,
NULL AS IDCFO_FCFO,
NULL AS CEI_FCFO,
CASE WHEN ps.id_pais = 22 THEN 0 ELSE 1 END AS NACIONALIDADE_FCFO,
NULL AS TIPOCONTRIBUINTEINSS_FCFO,
CASE WHEN lanint.st_codlancamento IS NOT NULL THEN lanint.st_codlancamento ELSE NULL END AS IDLAN,
CAST (REPLICATE('0',20 -LEN(lc.id_lancamento)) AS VARCHAR) + CAST(lc.id_lancamento AS VARCHAR) AS NUMERODOCUMENTO,
1 AS PAGREC,
CASE WHEN (lc.bl_quitado = 1 AND lc.id_meiopagamento <> 4) OR (lv.bl_entrada = 1 AND lc.id_meiopagamento = 1) THEN 1 ELSE 0 END AS STATUSLAN,
CAST(vd.dt_confirmacao AS DATETIME) AS DATAEMISSAO,
--CASE WHEN vd.dt_confirmacao IS NOT NULL THEN CAST ( ct.dt_ativacao AS DATE)ELSE  CAST (GETDATE() AS DATE) END AS DATAEMISSAO,
--CASE WHEN ct.dt_ativacao IS NOT NULL THEN CONVERT(nvarchar(30), cast ( ct.dt_ativacao as date), 103) ELSE CONVERT(nvarchar(30), cast (GETDATE() as date), 103) END as DATAEMISSAO,
CAST(dt_prevquitado AS DATE) AS DATAPREVBAIXA,
--CONVERT(nvarchar(30),CAST (lc.dt_prevquitado AS DATE),103) as DATAPREVBAIXA,
--CONVERT(nvarchar(30),lc.dt_quitado,103) as DATABAIXA,
CASE WHEN (lc.dt_quitado IS not NULL AND lc.id_meiopagamento <> 4) THEN CAST(lc.dt_quitado AS DATETIME)
WHEN  (lc.dt_quitado IS NULL AND lv.bl_entrada = 1 AND lc.id_meiopagamento = 1) THEN CAST (lc.dt_vencimento AS DATETIME)
WHEN (lc.dt_quitado IS not NULL AND lc.id_meiopagamento = 4) THEN null
END AS DATABAIXA,
lc.nu_valor AS VALORORIGINAL,
CASE when lc.nu_quitado is NOT NULL THEN lc.nu_quitado
WHEN lc.nu_quitado IS NULL AND lv.bl_entrada = 1 AND lc.id_meiopagamento = 1 THEN lc.nu_valor ELSE 0
end AS VALORBAIXADO,
0 AS VALORCAP,
ISNULL(lc.nu_juros, 0) AS VALORJUROS,
ISNULL(lc.nu_desconto,0) AS VALORDESCONTO,
0 AS VALOROP1,
0 AS VALOROP2,
0 AS VALOROP3,
ISNULL( lc.nu_multa, 0) AS VALORMULTA,
0 AS JUROSDIA,
0 AS CAPMENSAL,
0 AS TAXASVENDOR,
0 AS JUROSVENDOR,
mpe.st_codcontacaixa AS CODCXA, -- conta caixa
'034' AS CODTDO, -- C�digo do tipo de documento
CASE WHEN lv.bl_entrada = 1 THEN
'01.01.0001'
ELSE
'01.01.0002'
END AS CODTB1FLX, -- Categoria
CASE WHEN (lc.id_cartaoconfig IS NULL AND lc.id_meiopagamento <> 4)  THEN
(SELECT DISTINCT TOP 1 mpi.st_codsistema FROM dbo.tb_meiopagamentointegracao mpi WHERE mpi.id_meiopagamento = lc.id_meiopagamento  AND mpi.id_sistema = 3)
WHEN (lc.id_cartaoconfig IS NULL AND lc.id_meiopagamento = 4 AND lv.bl_entrada = 1)  THEN '0002'
WHEN (lc.id_cartaoconfig IS NULL AND lc.id_meiopagamento = 4 AND lv.bl_entrada <> 1)  THEN '0003'
ELSE
(SELECT DISTINCT TOP 1 mpi.st_codsistema FROM dbo.tb_meiopagamentointegracao mpi WHERE mpi.id_meiopagamento = lc.id_meiopagamento  AND mpi.id_sistema = 3 AND lc.id_cartaoconfig = cc.id_cartaoconfig AND mpi.id_cartaobandeira = cc.id_cartaobandeira)
END AS CODTB2FLX, -- Forma de pagamento
CAST (ct.dt_ativacao AS DATE) AS DATAOP2,
CAST (vd.id_venda AS VARCHAR) + 'G2U' AS CAMPOALFAOP1,
CAST (lc.id_lancamento AS VARCHAR) + 'G2U' AS CAMPOALFAOP2,
'Lancamento gerado pelo Gestor2 as '+CONVERT(VARCHAR(8), GETDATE(),  114 ) + ' de ' + CONVERT(VARCHAR, GETDATE(),  103  ) AS HISTORICO,
ei.st_codchave AS CODCCUSTO,
NULL AS NUMEROCHEQUE,
CAST (lc.dt_vencimento AS DATE) AS DATAVENCIMENTO,
NULL AS DATAVENCIMENTO_CHEQUE,
NULL AS CODIGOBANCO,
NULL AS CODIGOAGENCIA,
NULL AS CONTACORRENTE,
NULL AS COMPENSADO,
vd.id_venda,
ct.id_contrato,
lv.id_lancamento,
lv.bl_entrada,
lv.nu_ordem,
vd.id_entidade,
ei.st_caminho, ei.st_linkedserver
FROM tb_venda AS vd
LEFT JOIN tb_contrato AS ct ON vd.id_venda = ct.id_venda
JOIN tb_lancamentovenda AS lv ON lv.id_venda = vd.id_venda
JOIN tb_lancamento AS lc ON lc.id_lancamento = lv.id_lancamento
JOIN tb_usuario AS usr ON usr.id_usuario = lc.id_usuariolancamento
JOIN tb_pessoa AS ps ON ps.id_usuario = usr.id_usuario AND ps.id_entidade = vd.id_entidade
JOIN tb_entidadeintegracao AS ei ON ei.id_entidade  = vd.id_entidade AND ei.id_sistema = 3
CROSS APPLY (SELECT TOP 1 id_meiopagamentointegracao ,
id_cartaobandeira ,
id_sistema ,
id_usuariocadastro ,
id_meiopagamento ,
id_entidade ,
st_codsistema ,
dt_cadastro, st_codcontacaixa FROM dbo.tb_meiopagamentointegracao WHERE id_meiopagamento = lc.id_meiopagamento AND id_entidade = vd.id_entidade) AS mpe 
LEFT JOIN dbo.tb_cartaoconfig cc ON cc.id_cartaoconfig = lc.id_cartaoconfig
LEFT JOIN tb_pessoaendereco AS pse ON pse.id_usuario = ps.id_usuario AND ps.id_entidade = pse.id_entidade AND pse.bl_padrao = 1
LEFT JOIN tb_endereco AS ed ON ed.id_endereco = pse.id_endereco
LEFT JOIN tb_municipio AS mun ON ed.id_municipio = mun.id_municipio
LEFT JOIN tb_contatostelefonepessoa AS ctp ON ctp.id_usuario = usr.id_usuario AND ctp.id_entidade = ps.id_entidade AND ctp.bl_padrao = 1
LEFT JOIN tb_contatostelefone AS ctl ON ctl.id_telefone = ctp.id_telefone
LEFT JOIN tb_contatosemailpessoaperfil AS cepp ON cepp.id_usuario = usr.id_usuario AND cepp.id_entidade = ps.id_entidade AND cepp.bl_padrao = 1
LEFT JOIN tb_contatosemail AS ce ON ce.id_email = cepp.id_email
LEFT JOIN tb_pais AS pa ON pa.id_pais = ed.id_pais
LEFT JOIN tb_lancamentointegracao lanint ON lanint.id_lancamento = lc.id_lancamento
WHERE vd.id_evolucao = 10 AND lanint.st_codlancamento IS NULL 
and lc.nu_valor <> 0 AND lc.bl_ativo = 1

UNION

SELECT ei.st_codsistema AS CODCOLIGADA_FCFO,
CASE WHEN lanint.st_codresponsavel IS NOT NULL THEN lanint.st_codresponsavel ELSE NULL END AS CODCFO_FCFO,
replace(usr.st_nomeentidade,'''','') AS NOMEFANTASIA_FCFO,
replace(usr.st_razaosocial,'''','') AS NOME_FCFO,
usr.st_cnpj AS CGCCFO_FCFO,
usr.nu_inscricaoestadual AS INSCRESTADUAL_FCFO,
'1' AS PAGREC_FCFO,
replace(ed.st_endereco,'''','') AS RUA_FCFO,
ed.nu_numero AS NUMERO_FCFO,
CAST(replace(ed.st_complemento,'''','') AS VARCHAR(30)) AS COMPLEMENTO_FCFO,
CAST(replace(ed.st_bairro,'''','') AS VARCHAR(30)) AS BAIRRO_FCFO,
replace(ed.st_cidade,'''','') AS CIDADE_FCFO,
CODETD_FCFO = CASE WHEN ed.sg_uf IS NOT NULL  THEN ed.sg_uf
ELSE 'DF' END,
REPLACE(REPLACE(ed.st_cep,'.',''),'-','') AS CEP_FCFO,
CAST(ctl.nu_ddd AS VARCHAR)+ ' ' + CAST(ctl.nu_telefone AS VARCHAR) AS TELEFONE_FCFO,
NULL AS FAX_FCFO,
null AS EMAIL_FCFO,
NULL AS CONTATO_FCFO,
CASE WHEN usr.id_situacao = 1 THEN 1 ELSE 0 END AS ATIVO_FCFO,
mun.nu_codigoibge AS CODMUNICIPIO_FCFO,
'J'  AS PESSOAFISOUJUR_FCFO,
pa.st_nomepais AS PAIS_FCFO,
NULL AS IDCFO_FCFO,
NULL AS CEI_FCFO,
CASE WHEN ed.id_pais = 22 THEN 0 ELSE 1 END AS NACIONALIDADE_FCFO,
NULL AS TIPOCONTRIBUINTEINSS_FCFO,
CASE WHEN lanint.st_codlancamento IS NOT NULL THEN lanint.st_codlancamento ELSE NULL END AS IDLAN,
CAST (REPLICATE('0',20 -LEN(lc.id_lancamento)) AS VARCHAR) + CAST(lc.id_lancamento AS VARCHAR) AS NUMERODOCUMENTO,
1 AS PAGREC,
CASE WHEN (lc.bl_quitado = 1 AND lc.id_meiopagamento <> 4) OR (lv.bl_entrada = 1 AND lc.id_meiopagamento = 1) THEN 1 ELSE 0 END AS STATUSLAN,
CAST(vd.dt_confirmacao AS DATETIME) AS DATAEMISSAO,
--CASE WHEN vd.dt_confirmacao IS NOT NULL THEN CAST ( ct.dt_ativacao AS DATE)ELSE  CAST (GETDATE() AS DATE) END AS DATAEMISSAO,
--CASE WHEN ct.dt_ativacao IS NOT NULL THEN CONVERT(nvarchar(30), cast ( ct.dt_ativacao as date), 103) ELSE CONVERT(nvarchar(30), cast (GETDATE() as date), 103) END as DATAEMISSAO,
CAST(dt_prevquitado AS DATE) AS DATAPREVBAIXA,
--CONVERT(nvarchar(30),CAST (lc.dt_prevquitado AS DATE),103) as DATAPREVBAIXA,
--CONVERT(nvarchar(30),lc.dt_quitado,103) as DATABAIXA,
CASE WHEN (lc.dt_quitado IS not NULL AND lc.id_meiopagamento <> 4) THEN CAST(lc.dt_quitado AS DATETIME)
WHEN  (lc.dt_quitado IS NULL AND lv.bl_entrada = 1 AND lc.id_meiopagamento = 1) THEN CAST (lc.dt_vencimento AS DATETIME)
WHEN (lc.dt_quitado IS not NULL AND lc.id_meiopagamento = 4) THEN null
END AS DATABAIXA,
lc.nu_valor AS VALORORIGINAL,
CASE when lc.nu_quitado is NOT NULL THEN lc.nu_quitado
WHEN lc.nu_quitado IS NULL AND lv.bl_entrada = 1 AND lc.id_meiopagamento = 1 THEN lc.nu_valor ELSE 0
end AS VALORBAIXADO,
0 AS VALORCAP,
ISNULL(lc.nu_juros, 0) AS VALORJUROS,
ISNULL(lc.nu_desconto,0) AS VALORDESCONTO,
0 AS VALOROP1,
0 AS VALOROP2,
0 AS VALOROP3,
ISNULL( lc.nu_multa, 0) AS VALORMULTA,
0 AS JUROSDIA,
0 AS CAPMENSAL,
0 AS TAXASVENDOR,
0 AS JUROSVENDOR,
mpe.st_codcontacaixa AS CODCXA, -- conta caixa
'034' AS CODTDO, -- C�digo do tipo de documento
CASE WHEN lv.bl_entrada = 1 THEN
'01.01.0001'
ELSE
'01.01.0002'
END AS CODTB1FLX, -- Categoria
CASE WHEN (lc.id_cartaoconfig IS NULL AND lc.id_meiopagamento <> 4)  THEN
(SELECT DISTINCT TOP 1 mpi.st_codsistema FROM dbo.tb_meiopagamentointegracao mpi WHERE mpi.id_meiopagamento = lc.id_meiopagamento  AND mpi.id_sistema = 3)
WHEN (lc.id_cartaoconfig IS NULL AND lc.id_meiopagamento = 4 AND lv.bl_entrada = 1)  THEN '0002'
WHEN (lc.id_cartaoconfig IS NULL AND lc.id_meiopagamento = 4 AND lv.bl_entrada <> 1)  THEN '0003'
ELSE
(SELECT DISTINCT TOP 1 mpi.st_codsistema FROM dbo.tb_meiopagamentointegracao mpi WHERE mpi.id_meiopagamento = lc.id_meiopagamento  AND mpi.id_sistema = 3 AND lc.id_cartaoconfig = cc.id_cartaoconfig AND mpi.id_cartaobandeira = cc.id_cartaobandeira)
END AS CODTB2FLX, -- Forma de pagamento
CAST (ct.dt_ativacao AS DATE) AS DATAOP2,
CAST (vd.id_venda AS VARCHAR) + 'G2U' AS CAMPOALFAOP1,
CAST (lc.id_lancamento AS VARCHAR) + 'G2U' AS CAMPOALFAOP2,
'Lancamento gerado pelo Gestor2 as '+CONVERT(VARCHAR(8), GETDATE(),  114 ) + ' de ' + CONVERT(VARCHAR, GETDATE(),  103  ) AS HISTORICO,
ei.st_codchave AS CODCCUSTO,
NULL AS NUMEROCHEQUE,
CAST (lc.dt_vencimento AS DATE) AS DATAVENCIMENTO,
NULL AS DATAVENCIMENTO_CHEQUE,
NULL AS CODIGOBANCO,
NULL AS CODIGOAGENCIA,
NULL AS CONTACORRENTE,
NULL AS COMPENSADO,
vd.id_venda,
ct.id_contrato,
lv.id_lancamento,
lv.bl_entrada,
lv.nu_ordem,
vd.id_entidade,
ei.st_caminho,
ei.st_linkedserver
FROM tb_venda AS vd
LEFT JOIN tb_contrato AS ct ON vd.id_venda = ct.id_venda
JOIN tb_lancamentovenda AS lv ON lv.id_venda = vd.id_venda
JOIN tb_lancamento AS lc ON lc.id_lancamento = lv.id_lancamento
JOIN tb_entidade AS usr ON usr.id_entidade = lc.id_entidadelancamento
--JOIN tb_pessoa as ps ON ps.id_usuario = usr.id_usuario and ps.id_entidade = vd.id_entidade
JOIN tb_entidadeintegracao AS ei ON ei.id_entidade  = vd.id_entidade AND ei.id_sistema = 3
CROSS APPLY (SELECT TOP 1 id_meiopagamentointegracao ,
id_cartaobandeira ,
id_sistema ,
id_usuariocadastro ,
id_meiopagamento ,
id_entidade ,
st_codsistema ,
dt_cadastro, st_codcontacaixa FROM dbo.tb_meiopagamentointegracao WHERE id_meiopagamento = lc.id_meiopagamento AND id_entidade = vd.id_entidade) AS mpe 
LEFT JOIN dbo.tb_cartaoconfig cc ON cc.id_cartaoconfig = lc.id_cartaoconfig
LEFT JOIN dbo.tb_entidadeendereco AS pse ON pse.id_entidade = usr.id_entidade AND pse.bl_padrao = 1
LEFT JOIN tb_endereco AS ed ON ed.id_endereco = pse.id_endereco
LEFT JOIN tb_municipio AS mun ON ed.id_municipio = mun.id_municipio
LEFT JOIN dbo.tb_contatoentidade AS ce ON ce.id_entidade = usr.id_entidade
LEFT JOIN dbo.tb_contatostelefoneentidadecontato AS ctp ON ctp.id_contatoentidade = ce.id_contatoentidade AND ctp.bl_padrao = 1
LEFT JOIN tb_contatostelefone AS ctl ON ctl.id_telefone = ctp.id_telefone
--LEFT JOIN tb_contatosemailpessoaperfil as cepp ON cepp.id_usuario = usr.id_usuario and cepp.id_entidade = ps.id_entidade and cepp.bl_padrao = 1
--LEFT JOIN tb_contatosemail as ce ON ce.id_email = cepp.id_email
LEFT JOIN tb_pais AS pa ON pa.id_pais = ed.id_pais
LEFT JOIN tb_lancamentointegracao lanint ON lanint.id_lancamento = lc.id_lancamento
WHERE vd.id_evolucao = 10 AND lanint.st_codlancamento IS NULL 
and lc.nu_valor <> 0 AND lc.bl_ativo = 1





GO



CREATE VIEW [dbo].[vw_validaloginportal] AS
SELECT
	us.id_usuario,
	ps.id_entidade,
	ce.st_email,
	us.st_login,
	us.st_cpf,
	da.st_senha AS st_senhaentidade,
	us.st_senha,
	us.st_nomecompleto,
	ps.st_urlavatar,
	cfe.bl_acessoapp
FROM
	dbo.tb_usuario AS us
JOIN dbo.tb_pessoa AS ps ON ps.id_usuario = us.id_usuario
AND ps.bl_ativo = 1
JOIN dbo.tb_contatosemailpessoaperfil AS cep ON cep.id_usuario = ps.id_usuario
AND cep.id_entidade = ps.id_entidade
JOIN dbo.tb_contatosemail AS ce ON ce.id_email = cep.id_email
JOIN dbo.tb_dadosacesso AS da ON da.id_usuario = us.id_usuario
AND da.id_entidade = ps.id_entidade
AND da.bl_ativo = 1
JOIN dbo.tb_configuracaoentidade AS cfe ON cfe.id_entidade = da.id_entidade
GO



GO
/****** Object:  View [dbo].[vw_historicogeral]    Script Date: 10/08/2012 17:42:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_historicogeral] AS
SELECT
mt.id_matricula, mt.id_projetopedagogico, pp.st_tituloexibicao, mdd.id_serie, se.st_serie, se.id_serieanterior, mdd.id_areaconhecimento AS id_areaconhecimento, ac.st_tituloexibicao AS st_areaconhecimento, ac.id_tipoareaconhecimento
,CAST(mtd.id_disciplina AS VARCHAR(200)) AS id_disciplina, dc.st_tituloexibicao AS st_disciplina, mtd.nu_aprovafinal AS nu_aprovafinal, pp.nu_notamaxima, mtd.dt_conclusao AS dt_conclusaodisciplina ,
nu_cargahoraria = case  
WHEN mtd.nu_aprovafinal is NULL THEN NULL 
ELSE dc.nu_cargahoraria
END
,mtd.id_evolucao, mtd.id_situacao
, CAST (apm.st_cargahoraria AS int) AS st_cargahorariaaproveitamento, apm.st_notaoriginal AS st_notaoriginalaproveitamento, apd.st_instituicao AS st_instituicaoaproveitamento, apd.sg_uf AS sg_ufaproveitamento, mn.st_nomemunicipio AS st_nomemunicipioaproveitamento ,CAST(CAST(apm.nu_anoconclusao AS VARCHAR(4)) + '-01-01' AS DATE) AS dt_conclusaoaproveitamento
, ent.st_nomeentidade, ed.sg_uf, mun.st_nomemunicipio, mdd.nu_ordem, ap.st_tituloexibicao AS st_areaagregadora, ap.id_areaconhecimento AS id_areaagregadora
FROM tb_matricula AS mt
JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_modulo AS md ON md.id_projetopedagogico = mt.id_projetopedagogico AND md.bl_ativo = 1
JOIN tb_modulodisciplina AS mdd ON mdd.id_modulo = md.id_modulo AND mdd.bl_ativo = 1
JOIN tb_matriculadisciplina AS mtd ON mtd.id_matricula = mt.id_matricula
LEFT JOIN tb_aproveitamentomatriculadisciplina AS apm ON apm.id_matricula = mtd.id_matricula AND apm.id_disciplina = mtd.id_disciplina
LEFT JOIN tb_aproveitamentodisciplina AS apd ON apd.id_aproveitamentodisciplina = apm.id_aproveitamentodisciplina
JOIN tb_disciplina AS dc ON dc.id_disciplina = mtd.id_disciplina AND dc.id_disciplina = mdd.id_disciplina
JOIN tb_serie AS se ON se.id_serie = mdd.id_serie
JOIN tb_nivelensino AS ne ON ne.id_nivelensino = mdd.id_nivelensino
JOIN tb_areaconhecimento AS ac ON ac.id_areaconhecimento = mdd.id_areaconhecimento
LEFT JOIN tb_municipio AS mn ON mn.id_municipio = apd.id_municipio
LEFT JOIN tb_entidade AS ent ON ent.id_entidade = mt.id_entidadematriz
LEFT JOIN tb_entidadeendereco AS eend ON eend.id_entidade = ent.id_entidade AND eend.bl_padrao = 1
LEFT JOIN tb_endereco AS ed ON ed.id_endereco = eend.id_endereco
LEFT JOIN tb_municipio AS mun ON mun.id_municipio = ed.id_municipio
LEFT JOIN dbo.tb_areaconhecimento AS ap ON ap.id_areaconhecimento = ac.id_areaconhecimentopai AND ap.id_tipoareaconhecimento = 1 
WHERE apm.st_disciplinaoriginal = ac.st_tituloexibicao OR apm.st_disciplinaoriginal IS NULL 

UNION

SELECT
mt.id_matricula, mt.id_projetopedagogico, pp.st_tituloexibicao, mdd.id_serie, se.st_serie, se.id_serieanterior, CAST(HASHBYTES ( 'md5', st_disciplinaoriginal )AS INT) AS id_areaconhecimento, apm.st_disciplinaoriginal AS st_areaconhecimento, ac.id_tipoareaconhecimento
,CAST(mtd.id_disciplina AS VARCHAR(200)) AS id_disciplina, dc.st_tituloexibicao AS st_disciplina, mtd.nu_aprovafinal AS nu_aprovafinal, pp.nu_notamaxima, mtd.dt_conclusao AS dt_conclusaodisciplina ,CAST (apm.st_cargahoraria AS int) AS nu_cargahoraria  ,mtd.id_evolucao, mtd.id_situacao
, CAST (apm.st_cargahoraria AS int) AS st_cargahorariaaproveitamento, REPLACE(apm.st_notaoriginal,',','.') AS st_notaoriginalaproveitamento, apd.st_instituicao AS st_instituicaoaproveitamento, apd.sg_uf AS sg_ufaproveitamento, mn.st_nomemunicipio AS st_nomemunicipioaproveitamento ,CAST(CAST(apm.nu_anoconclusao AS VARCHAR(4)) + '-01-01' AS DATE) AS dt_conclusaoaproveitamento
, ent.st_nomeentidade, ed.sg_uf, mun.st_nomemunicipio, mdd.nu_ordem, ap.st_tituloexibicao AS st_areaagregadora, ap.id_areaconhecimento AS id_areaagregadora
FROM tb_matricula AS mt
JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_modulo AS md ON md.id_projetopedagogico = mt.id_projetopedagogico AND md.bl_ativo = 1
JOIN tb_modulodisciplina AS mdd ON mdd.id_modulo = md.id_modulo AND mdd.bl_ativo = 1
JOIN tb_matriculadisciplina AS mtd ON mtd.id_matricula = mt.id_matricula
JOIN tb_aproveitamentomatriculadisciplina AS apm ON apm.id_matricula = mtd.id_matricula AND apm.id_disciplina = mtd.id_disciplina
JOIN tb_aproveitamentodisciplina AS apd ON apd.id_aproveitamentodisciplina = apm.id_aproveitamentodisciplina
JOIN tb_disciplina AS dc ON dc.id_disciplina = mtd.id_disciplina AND dc.id_disciplina = mdd.id_disciplina
JOIN tb_serie AS se ON se.id_serie = mdd.id_serie
JOIN tb_nivelensino AS ne ON ne.id_nivelensino = mdd.id_nivelensino
JOIN tb_areaconhecimento AS ac ON ac.id_areaconhecimento = mdd.id_areaconhecimento
LEFT JOIN tb_municipio AS mn ON mn.id_municipio = apd.id_municipio
LEFT JOIN tb_entidade AS ent ON ent.id_entidade = mt.id_entidadematriz
LEFT JOIN tb_entidadeendereco AS eend ON eend.id_entidade = ent.id_entidade AND eend.bl_padrao = 1
LEFT JOIN tb_endereco AS ed ON ed.id_endereco = eend.id_endereco
LEFT JOIN tb_municipio AS mun ON mun.id_municipio = ed.id_municipio
LEFT JOIN dbo.tb_areaconhecimento AS ap ON ap.id_areaconhecimento = ac.id_areaconhecimentopai AND ap.id_tipoareaconhecimento = 1 
WHERE apm.st_disciplinaoriginal != ac.st_tituloexibicao AND apm.st_disciplinaoriginal IS NOT NULL 

UNION

SELECT
mt.id_matricula, mt.id_projetopedagogico, pp.st_tituloexibicao, apm.id_serie, se.st_serie, se.id_serieanterior, CAST(HASHBYTES ( 'md5', st_disciplinaoriginal )AS INT) AS id_areaconhecimento, apm.st_disciplinaoriginal AS st_areaconhecimento, 2 AS id_tipoareaconhecimento
,apm.st_disciplinaoriginal AS id_disciplina, apm.st_disciplinaoriginal AS st_disciplina, apm.nu_aproveitamento AS nu_aprovafinal, pp.nu_notamaxima, GETDATE() AS dt_conclusaodisciplina ,CAST (apm.st_cargahoraria AS int) AS nu_cargahoraria ,12 AS id_evolucao, 65 AS id_situacao
, CAST (apm.st_cargahoraria AS int) AS st_cargahorariaaproveitamento, REPLACE(apm.st_notaoriginal,',','.') AS st_notaoriginalaproveitamento, apd.st_instituicao AS st_instituicaoaproveitamento, apd.sg_uf AS sg_ufaproveitamento, mn.st_nomemunicipio AS st_nomemunicipioaproveitamento
, CAST(CAST(apm.nu_anoconclusao AS VARCHAR(4)) + '-01-01' AS DATE) AS dt_conclusaoaproveitamento
, ent.st_nomeentidade, ed.sg_uf, mun.st_nomemunicipio, apm.id_serie AS nu_ordem, ap.st_tituloexibicao AS st_areaagregadora, ap.id_areaconhecimento AS id_areaagregadora
FROM tb_matricula AS mt
JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_aproveitamentomatriculadisciplina AS apm ON apm.id_matricula = mt.id_matricula AND apm.id_disciplina IS NULL
JOIN tb_aproveitamentodisciplina AS apd ON apd.id_aproveitamentodisciplina = apm.id_aproveitamentodisciplina
JOIN tb_serie AS se ON se.id_serie = apm.id_serie
JOIN dbo.tb_projetopedagogicoserienivelensino AS psn ON psn.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_nivelensino AS ne ON ne.id_nivelensino = psn.id_nivelensino
LEFT JOIN tb_municipio AS mn ON mn.id_municipio = apd.id_municipio
JOIN tb_entidade AS ent ON ent.id_entidade = mt.id_entidadematriz
JOIN tb_entidadeendereco AS eend ON eend.id_entidade = ent.id_entidade AND eend.bl_padrao = 1
JOIN tb_endereco AS ed ON ed.id_endereco = eend.id_endereco
JOIN tb_municipio AS mun ON mun.id_municipio = ed.id_municipio
LEFT JOIN dbo.tb_areaconhecimento AS ap ON ap.bl_ativo = 1 AND bl_extras = 1 AND ap.id_entidade = mt.id_entidadematriz
WHERE apm.st_notaoriginal IS NOT null
GO

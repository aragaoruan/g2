CREATE VIEW [dbo].[vw_alunosaptosagendamento] AS (

SELECT
	DISTINCT
	mt.st_nomecompleto,
	mt.id_matricula,
	mt.id_projetopedagogico,

	CASE
	WHEN ava_rec.id_tipodeavaliacao > 0 THEN 'RECUPERAÇÃO'
	ELSE 'PROVA FINAL'
	END AS 'st_avaliacao'

	FROM dbo.vw_alunosagendamento AS alag
	JOIN rel.vw_matricula AS mt ON mt.id_matricula = alag.id_matricula AND mt.id_situacaoagendamento = 120
	LEFT JOIN dbo.vw_avaliacaoagendamento AS avag ON avag.id_matricula = alag.id_matricula AND avag.id_avaliacaoagendamento IN (SELECT TOP 1 id_avaliacaoagendamento FROM dbo.vw_avaliacaoagendamento AS ava WHERE ava.id_matricula = avag.id_matricula ORDER BY id_tipodeavaliacao DESC, id_chamada DESC)
	OUTER APPLY (
	SELECT
	CASE
		WHEN nu_presenca IS NULL THEN id_tipodeavaliacao
		ELSE id_tipodeavaliacao + 1
	END AS id_tipodeavaliacao
	) AS ava_rec
)
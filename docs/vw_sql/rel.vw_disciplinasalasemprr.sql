 CREATE VIEW  rel.vw_disciplinasalasemprr as

select           

pp.id_projetopedagogico,
pp.st_projetopedagogico,
dis.id_disciplina,
dis.st_disciplina,
usu.id_usuario,
usu.st_nomecompleto,
sale.id_entidade
from tb_projetopedagogico as pp
join tb_areaprojetosala as aps on aps.id_projetopedagogico = pp.id_projetopedagogico
join tb_saladeaula as sal on sal.id_saladeaula=aps.id_saladeaula and sal.bl_ativa=1 and sal.id_categoriasala=1
join tb_saladeaulaentidade as sale on sale.id_saladeaula = sal.id_saladeaula
join tb_disciplinasaladeaula as ds on ds.id_saladeaula = sal.id_saladeaula
join tb_disciplina as dis on dis.id_disciplina=ds.id_disciplina
join vw_usuarioperfilentidadereferencia as uper on uper.id_projetopedagogico = aps.id_projetopedagogico and uper.bl_titular=1 and uper.bl_ativo=1
join tb_usuario as usu on usu.id_usuario = uper.id_usuario

EXCEPT

select           

pp.id_projetopedagogico,
pp.st_projetopedagogico,
dis.id_disciplina,
dis.st_disciplina,
usu.id_usuario,
usu.st_nomecompleto,
sale.id_entidade
from tb_projetopedagogico as pp
join tb_areaprojetosala as aps on aps.id_projetopedagogico = pp.id_projetopedagogico
join tb_saladeaula as sal on sal.id_saladeaula=aps.id_saladeaula and sal.bl_ativa=1 and sal.id_categoriasala=2
join tb_saladeaulaentidade as sale on sale.id_saladeaula = sal.id_saladeaula
join tb_disciplinasaladeaula as ds on ds.id_saladeaula = sal.id_saladeaula
join tb_disciplina as dis on dis.id_disciplina=ds.id_disciplina
join vw_usuarioperfilentidadereferencia as uper on uper.id_projetopedagogico = aps.id_projetopedagogico and uper.bl_titular=1 and uper.bl_ativo=1
join tb_usuario as usu on usu.id_usuario = uper.id_usuario

-- vw responsavel por trazer todos os alunos que já estão aptos para gerar
-- o certificado parcial

CREATE VIEW [dbo].[vw_alunosaptoscertificadoparcial] AS
  SELECT
    vw.id_certificadoparcial,
    vw.id_entidadecadastro,
    vw.id_entidade,
    vw.id_evolucao,
    vw.id_usuario,
    vw.st_nomeentidade,
    vw.st_certificadoparcial,
    vw.st_nomecompleto,
    vw.st_cpf,
    vw.id_matricula,
    convert(VARCHAR, vw.dt_impressao, 103) AS dt_impressao
     FROM (
     SELECT DISTINCT
  cp.id_certificadoparcial,
  cp.id_entidadecadastro,
  e.id_entidade,
  md.id_evolucao,
  u.id_usuario,
  e.st_nomeentidade,
  cp.st_certificadoparcial,
  u.st_nomecompleto,
  u.st_cpf,
M.id_matricula,
                ( SELECT COUNT ( DISTINCT cpd2.id_disciplina)
       FROM tb_certificadoparcialdisciplina cpd2
     WHERE cpd2.id_certificadoparcial = cp.id_certificadoparcial) AS nu_totalparcial,
                    ( SELECT COUNT ( DISTINCT cpd2.id_disciplina)
       FROM tb_matriculadisciplina md2
       LEFT JOIN tb_certificadoparcialdisciplina cpd2
         ON cpd2.id_disciplina = md2.id_disciplina
       AND md2.id_evolucao = 12 AND md2.id_situacao = 54
     WHERE cp.id_certificadoparcial = cpd2.id_certificadoparcial
       AND md2.id_matricula = M.id_matricula) AS nu_totalparcialmatricula,
( SELECT MAX (dt_cadastro) FROM tb_historicocertificadoparcial AS hcp
WHERE cpd.id_certificadoparcial = hcp.id_certificadoparcial AND md.id_matricula = hcp.id_matricula
GROUP BY id_matricula ) AS dt_impressao
     FROM tb_certificadoparcial cp
     JOIN tb_certificadoparcialdisciplina cpd ON cp.id_certificadoparcial = cpd.id_certificadoparcial
     JOIN tb_matriculadisciplina md ON cpd.id_disciplina = md.id_disciplina AND md.id_evolucao = 12 AND md.id_situacao = 54
     JOIN tb_matricula M ON md.id_matricula = M.id_matricula
     JOIN tb_usuario u ON M.id_usuario = u.id_usuario
     JOIN tb_entidade e ON M.id_entidadematricula = e.id_entidade
) vw
WHERE vw.nu_totalparcial = vw.nu_totalparcialmatricula
go


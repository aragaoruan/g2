USE [G2_UNY]
GO

/****** Object:  View [dbo].[vw_produtovendaunica]    Script Date: 07/01/2015 17:33:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 CREATE VIEW [dbo].[vw_produtovendaunica] as
SELECT
	tp.id_produto,
	tc2.st_email,
	tv.id_vendaproduto
FROM dbo.tb_produto AS tp
JOIN dbo.tb_vendaproduto AS tv	ON tp.id_produto = tv.id_produto
JOIN dbo.tb_venda AS tv2 ON tv.id_venda = tv2.id_venda
JOIN dbo.tb_contatosemailpessoaperfil AS tc	ON tc.id_usuario = tv2.id_usuario	AND tc.id_entidade = tv2.id_entidade
JOIN dbo.tb_contatosemail AS tc2 ON tc.id_email = tc2.id_email
WHERE tp.bl_unico = 1 and (tv2.id_evolucao = 10 or tv2.id_situacao = 39)


GO
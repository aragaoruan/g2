
GO
/****** Object:  View [dbo].[vw_robomensalidade]    Script Date: 10/08/2012 17:42:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_robomensalidade] as
select distinct
mt.id_matricula, ct.id_contrato, ct.id_venda, pv.nu_valormensal, v.nu_diamensalidade,ent.st_nomeentidade, rc.nu_mesesmensalidade, mt.dt_inicio, cr.id_usuario, cr.nu_porcentagem, ct.id_entidade
,rc.id_contratoregra, ct.nu_bolsa
from tb_matricula as mt
JOIN tb_contratomatricula as cm ON cm.id_matricula = mt.id_matricula
JOIN tb_contrato as ct ON ct.id_contrato = cm.id_contrato
and ct.id_venda not in(select lv.id_venda from tb_lancamento lc , tb_lancamentovenda lv where lc.id_lancamento = lv.id_lancamento and MONTH(getdate()) = MONTH(lc.dt_vencimento))
JOIN tb_contratoregra as rc ON rc.id_contratoregra = ct.id_contratoregra and  rc.id_projetocontratoduracaotipo in (2,3)
JOIN tb_produtoprojetopedagogico as ppp ON ppp.id_projetopedagogico = mt.id_projetopedagogico
JOIN tb_produtovalor as pv ON pv.id_produto = ppp.id_produto and getdate() >= pv.dt_inicio and (getdate() <= pv.dt_termino or pv.dt_termino is null) and pv.id_tipoprodutovalor = 5
JOIN tb_entidade as ent ON ent.id_entidade = ct.id_entidade
JOIN tb_venda as v ON v.id_venda = ct.id_venda
JOIN tb_contratoresponsavel as cr ON cr.id_contrato = ct.id_contrato and cr.id_tipocontratoresponsavel = 1
where
mt.id_evolucao = 6
and mt.id_situacao = 50
and month (getdate()) >= month(dateadd(month, rc.nu_mesesmensalidade, mt.dt_inicio))
GO

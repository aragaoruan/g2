
GO
/****** Object:  View [dbo].[vw_gradetransferencia]    Script Date: 10/08/2012 17:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW  [dbo].[vw_gradetransferencia] AS 
SELECT DISTINCT mto.id_matricula,dc2.st_disciplina, gt.st_nota, pp2.id_projetopedagogico FROM 
dbo.tb_projetopedagogico AS pp2
JOIN dbo.tb_modulo AS md2 ON  md2.id_projetopedagogico = pp2.id_projetopedagogico   AND md2.bl_ativo = 1
JOIN tb_modulodisciplina AS mdc2 ON mdc2.id_modulo = md2.id_modulo AND  mdc2.bl_ativo = 1
JOIN dbo.tb_disciplina AS dc2 ON dc2.id_disciplina = mdc2.id_disciplina
JOIN dbo.tb_matricula AS mto on mto.bl_ativo = 1
LEFT JOIN dbo.tb_modulo AS md ON md.bl_ativo = 1 AND mto.id_projetopedagogico = md.id_projetopedagogico
LEFT JOIN dbo.tb_modulodisciplina AS mdc ON mdc2.id_disciplina = mdc.id_disciplina and mdc.id_modulo = md.id_modulo AND mdc.bl_ativo = 1
LEFT JOIN dbo.tb_matricula AS mt on md.id_projetopedagogico  = mt.id_projetopedagogico AND mto.id_matricula = mt.id_matricula
LEFT JOIN dbo.tb_matriculadisciplina AS mdd ON mdd.id_matricula = mt.id_matricula AND mdc.id_disciplina = mdd.id_disciplina
LEFT JOIN dbo.tb_alocacao AS al ON al.id_matriculadisciplina = mdd.id_matriculadisciplina
LEFT JOIN dbo.tb_encerramentoalocacao AS eal ON eal.id_alocacao = al.id_alocacao
LEFT JOIN dbo.tb_encerramentosala AS es ON es.id_encerramentosala = eal.id_encerramentosala AND al.id_saladeaula = es.id_saladeaula
LEFT JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula AND sa.id_saladeaula = es.id_saladeaula
LEFT JOIN dbo.vw_gradetipo AS gt ON gt.id_saladeaula = sa.id_saladeaula AND gt.st_situacao = 'Satisfatório' AND gt.id_matricula = mt.id_matricula
GO

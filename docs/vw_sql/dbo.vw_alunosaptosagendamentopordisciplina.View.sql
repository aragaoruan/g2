CREATE VIEW [dbo].[vw_alunosaptosagendamentopordisciplina] AS
  SELECT
    DISTINCT
    md.id_matriculadisciplina,
    md.id_matricula,
    md.id_disciplina,
    mt.id_situacao AS id_situacaomatricula,
    sa.id_saladeaula,
    sa.dt_abertura,
    sa.dt_encerramento,
    sa.bl_ativa,
    sa.id_situacao AS id_situacaosala,
    md.id_situacaoagendamento,
    mt.id_entidadematricula AS id_entidade,
    mtn.st_notaatividade,
    mtn.st_notaead,
    mtn.st_notafinal,
    mtn.nu_percentualfinal,
    mtn.bl_aprovado
  FROM
    vw_entidadeesquemaconfiguracao AS ec
-- Somente matrículas "Cursando"
    JOIN tb_matricula AS mt
      ON ec.id_entidade = mt.id_entidadematricula
         AND mt.bl_ativo = 1
         AND mt.id_evolucao = 6
    JOIN tb_matriculadisciplina AS md
      ON mt.id_matricula = md.id_matricula
-- Buscar média do aluno
    JOIN vw_matriculanota AS mtn
      ON mtn.id_matricula = mt.id_matricula
         AND mtn.id_matriculadisciplina = md.id_matriculadisciplina
-- Sala de aula ativa
    JOIN tb_saladeaula AS sa
      ON sa.id_saladeaula = mtn.id_saladeaula
         AND sa.bl_ativa = 1
         AND sa.id_situacao = 8
         -- GII-9226 - Somente salas que estão ativas ou foram encerradas num período de 60 dias
         AND CONVERT(DATE, GETDATE()) BETWEEN sa.dt_abertura AND DATEADD(DAY, 60, sa.dt_encerramento)
  WHERE
-- Retornar somente registros em que a configuração da entidade seja "prova por disciplina"
    ec.id_itemconfiguracao = 24
    AND ec.st_valor = 1
--AND md.id_situacaoagendamento NOT IN (120,177)

CREATE VIEW [dbo].[vw_nucleoassuntoco] AS
  SELECT
    ass.id_assuntoco,
    ass.st_assuntoco,
    ass.bl_ativo,
    ass.bl_abertura,
    st.st_situacao,
    ass.bl_autodistribuicao,
    na.id_nucleoassuntoco,
    nc.id_nucleoco,
    toc.st_tipoocorrencia,
    ap.st_assuntoco AS st_assuntopai,
    toc.id_tipoocorrencia,
    nc.id_textonotificacao,
    nc.id_entidade,
    nc.bl_notificaatendente,
    ass.id_situacao,
    ass.bl_interno,
    ass.bl_recuperacao_resgate,
    ass.bl_cancelamento
  FROM
    dbo.tb_assuntoco AS ass
    JOIN dbo.tb_assuntoentidadeco AS ae ON ae.id_assuntoco = ass.id_assuntoco
    JOIN dbo.tb_nucleoco AS nc ON nc.id_tipoocorrencia = ass.id_tipoocorrencia AND nc.bl_ativo = 1 AND
                                  ae.id_entidade = nc.id_entidade
    JOIN dbo.tb_tipoocorrencia AS toc ON toc.id_tipoocorrencia = ass.id_tipoocorrencia
    LEFT JOIN dbo.tb_nucleoassuntoco AS na ON na.id_assuntoco = ass.id_assuntoco AND nc.id_nucleoco = na.id_nucleoco AND
                                              na.bl_ativo = 1
    JOIN dbo.tb_situacao AS st ON st.id_situacao = ass.id_situacao
    LEFT JOIN dbo.tb_assuntoco AS ap ON ap.id_assuntoco = ass.id_assuntocopai
  WHERE
    ass.bl_ativo = 1
  EXCEPT
  SELECT
    ass.id_assuntoco,
    ass.st_assuntoco,
    ass.bl_ativo,
    ass.bl_abertura,
    st.st_situacao,
    ass.bl_autodistribuicao,
    na.id_nucleoassuntoco,
    nc.id_nucleoco,
    toc.st_tipoocorrencia,
    ap.st_assuntoco AS st_assuntopai,
    toc.id_tipoocorrencia,
    nc.id_textonotificacao,
    nc.id_entidade,
    nc.bl_notificaatendente,
    ass.id_situacao,
    ass.bl_interno,
    ass.bl_recuperacao_resgate,
    ass.bl_cancelamento
  FROM
    dbo.tb_assuntoco AS ass
    JOIN dbo.tb_assuntoentidadeco AS ae ON ae.id_assuntoco = ass.id_assuntoco
    JOIN dbo.tb_nucleoco AS nc ON nc.id_tipoocorrencia = ass.id_tipoocorrencia AND ae.id_entidade = nc.id_entidade
    JOIN dbo.tb_nucleoco AS nce ON nce.id_entidade = nc.id_entidade AND nc.id_nucleoco != nce.id_nucleoco
    JOIN dbo.tb_nucleoassuntoco AS na ON na.id_assuntoco = ass.id_assuntoco AND nc.id_nucleoco = nce.id_nucleoco AND
                                         na.bl_ativo = 1
    JOIN dbo.tb_situacao AS st ON st.id_situacao = ass.id_situacao
    JOIN dbo.tb_tipoocorrencia AS toc ON toc.id_tipoocorrencia = ass.id_tipoocorrencia
    LEFT JOIN dbo.tb_assuntoco AS ap ON ap.id_assuntoco = ass.id_assuntocopai
  WHERE
    ass.bl_ativo = 1
GO
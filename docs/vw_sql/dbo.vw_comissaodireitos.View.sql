ALTER VIEW [dbo].[vw_comissaodireitos] AS
  SELECT DISTINCT
    uper.id_usuario,
    us.st_nomecompleto,
    us.st_cpf,
    uper.id_entidade,
    'Autor' AS st_funcao,
    4 AS id_funcao,
    mdc.id_disciplina,
    dc.st_disciplina,
    vp.id_vendaproduto,
    CAST((vp.nu_valorliquido * (mdc.nu_ponderacaoaplicada / 100) * (uper.nu_porcentagem / 100)) AS FLOAT) AS nu_comissaoreceber,
    vp.nu_valorliquido,
    ppd.st_projetopedagogico,
    ppd.id_projetopedagogico,
    vd.dt_confirmacao,
    mdc.nu_ponderacaoaplicada,
    uper.nu_porcentagem
  FROM dbo.tb_usuarioperfilentidadereferencia AS uper
    JOIN dbo.tb_modulodisciplina AS mdc	ON  mdc.nu_ponderacaoaplicada IS NOT NULL and mdc.id_disciplina = uper.id_disciplina
    JOIN dbo.tb_disciplina AS dc ON dc.id_disciplina = uper.id_disciplina
    JOIN dbo.tb_modulo AS md ON md.id_modulo = mdc.id_modulo AND md.bl_ativo = 1
    JOIN dbo.tb_produtoprojetopedagogico AS pp	ON md.id_projetopedagogico = pp.id_projetopedagogico
    JOIN dbo.tb_projetopedagogico AS ppd ON ppd.id_projetopedagogico = pp.id_projetopedagogico
    JOIN dbo.tb_vendaproduto AS vp	ON  pp.id_produto = vp.id_produto AND vp.nu_valorliquido > 0
    JOIN dbo.tb_venda AS vd ON vd.id_venda = vp.id_venda AND vd.id_evolucao = 10 AND vd.dt_confirmacao IS NOT NULL
    JOIN dbo.tb_produto AS pd	ON vp.id_produto = pd.id_produto
    JOIN dbo.tb_usuario AS us	ON us.id_usuario = uper.id_usuario
  WHERE uper.nu_porcentagem IS NOT NULL
-- go


USE [G2_UNY]
GO

/****** Object:  View [dbo].[vw_matriculaintegracao_pontosoft]    Script Date: 8/20/2015 3:03:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[vw_matriculaintegracao_pontosoft] 
AS 
 
SELECT DISTINCT 
mt.id_usuario, 
ps.st_nomecompleto, 
ps.st_cpf, 
ps.st_sexo, 
CASE WHEN ps.st_identificacao IS NOT NULL THEN ps.st_identificacao 
ELSE '0000' 
END AS st_identificacao, 
mt.id_usuariocadastro, 
mt.id_matricula, 
tm.id_turma,
tm.st_turma, 
tm.st_codigo, 
tm.dt_inicio AS dt_inicioturma, 
mt.dt_inicio AS dt_iniciomatricula, 
mt.dt_termino AS dt_terminomatricula, 
ei.id_entidade, 
ti.id_turmaintegracao, 
ti.st_turmasistema,
e.st_nomeentidade 
FROM 
vw_pessoa AS ps 
JOIN tb_matricula AS mt ON mt.id_usuario = ps.id_usuario AND mt.id_entidadeatendimento = PS.id_entidade 
JOIN tb_pessoa AS pes ON pes.id_usuario = ps.id_usuario AND pes.id_entidade = ps.id_entidade --AND ps.id_usuario IN (10038141) 
JOIN tb_entidadeintegracao AS ei ON ei.id_entidade = mt.id_entidadeatendimento AND id_sistema = 20
JOIN tb_entidade as e on e.id_entidade = mt.id_entidadeatendimento
JOIN tb_turma AS tm ON tm.id_turma = mt.id_turma 
JOIN tb_turmaintegracao AS ti ON ti.id_turma = tm.id_turma AND ti.id_sistema = 20 
LEFT JOIN tb_usuariointegracao AS ui ON ui.id_usuario = ps.id_usuario AND ui.id_entidade = ps.id_entidade AND ui.id_sistema = 20 
WHERE 
(NOT EXISTS ( 
SELECT 
mi.id_matricula 
FROM 
tb_matriculaintegracao AS mi 
WHERE 
mi.id_matricula = mt.id_matricula 
) OR pes.bl_alterado = 1) 
AND ps.st_identificacao IS NOT NULL AND pes.st_identificacao != ''

GO




GO
/****** Object:  View [dbo].[vw_interfacepessoa]    Script Date: 10/08/2012 17:42:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_interfacepessoa] as
select ip.id_usuario, ip.id_entidade, ip.id_tipointerfacepessoa, tip.st_tipointerfacepessoa, tip.id_sistema, ip.st_color
from tb_interfacepessoa as ip
JOIN tb_tipointerfacepessoa as tip ON tip.id_tipointerfacepessoa = ip.id_tipointerfacepessoa
GO

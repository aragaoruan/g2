
GO

/****** Object:  View [dbo].[vw_professordisciplina]    Script Date: 01/12/2014 16:08:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 CREATE VIEW [dbo].[vw_professordisciplina] AS
SELECT DISTINCT upr.id_usuario, upr.st_nomecompleto, dc.id_tipodisciplina,sa.id_entidade, dc.id_disciplina FROM dbo.tb_saladeaula AS sa
JOIN dbo.vw_usuarioperfilentidadereferencia AS upr ON upr.id_saladeaula = sa.id_saladeaula
JOIN dbo.tb_disciplinasaladeaula AS ds ON ds.id_saladeaula = sa.id_saladeaula
JOIN dbo.tb_disciplina AS dc ON dc.id_disciplina = ds.id_disciplina

GO



CREATE VIEW [dbo].[vw_atendentes]
AS
    SELECT  nf.id_nucleofuncionariotm ,
            nf.id_funcao ,
            nf.id_usuario ,
            nf.id_nucleotm ,
            nf.dt_cadastro ,
            nf.bl_ativo ,
            nf.id_usuariocadastro ,
            nf.id_situacao ,
            n.st_nucleotm ,
            f.st_funcao ,
            s.st_situacao ,
            u.st_nomecompleto ,
            n.id_entidade,
            n.id_entidadematriz,
            n.st_cpf
    FROM    "tb_nucleofuncionariotm" AS "nf"
            INNER JOIN "tb_nucleotm" AS "n" ON nf.id_nucleotm = n.id_nucleotm
            INNER JOIN "tb_funcao" AS "f" ON nf.id_funcao = f.id_funcao
            INNER JOIN "tb_situacao" AS "s" ON s.id_situacao = nf.id_situacao
            INNER JOIN "tb_usuario" AS "u" ON u.id_usuario = nf.id_usuario
GO
CREATE VIEW [totvs].[vw_Integra_Venda_GestorFluxus] AS

	SELECT 
		ei.st_codsistema AS CODCOLIGADA_FCFO,
		NULL  AS CODCFO_FCFO,
		REPLACE(usr.st_nomecompleto,'''','') COLLATE SQL_Latin1_General_CP1_CI_AI AS NOMEFANTASIA_FCFO,
		REPLACE(usr.st_nomecompleto,'''','') COLLATE SQL_Latin1_General_CP1_CI_AI AS NOME_FCFO,
		usr.st_cpf COLLATE SQL_Latin1_General_CP1_CI_AI AS CGCCFO_FCFO,
		NULL AS INSCRESTADUAL_FCFO,
		'1' AS PAGREC_FCFO,
		1 AS TIPORUA,
		REPLACE(ed.st_endereco,'''','') AS RUA_FCFO,
		CASE WHEN (COALESCE(CAST( ed.nu_numero  AS VARCHAR), '') = '' OR CAST( ed.nu_numero  AS VARCHAR)  = '0' OR ed.nu_numero IS NULL  )
			THEN 'S/N' 
			ELSE CAST( ed.nu_numero AS VARCHAR) END AS NUMERO_FCFO,
		CAST(REPLACE(ed.st_complemento,'''','') AS VARCHAR(30)) AS COMPLEMENTO_FCFO,
		1 AS TIPOBAIRRO_FCFO,
		CAST(REPLACE(ed.st_bairro,'''','') AS VARCHAR(30)) AS BAIRRO_FCFO,
		REPLACE(ed.st_cidade,'''','') AS CIDADE_FCFO,
		CASE WHEN ed.sg_uf IS NOT NULL  THEN ed.sg_uf
		ELSE 'DF' END AS CODETD_FCFO,
		REPLACE(REPLACE(ed.st_cep,'-',''),'.','') AS CEP_FCFO,
		CAST(ctl.nu_ddd AS VARCHAR)+ ' ' + CAST(ctl.nu_telefone AS VARCHAR) AS TELEFONE_FCFO,
		NULL AS FAX_FCFO,
		REPLACE(ce.st_email,'''','') AS EMAIL_FCFO,
		NULL AS CONTATO_FCFO,
		CASE WHEN ps.id_situacao = 1 THEN 1 ELSE 1 END AS ATIVO_FCFO,
		SUBSTRING ( CAST(mun.id_municipio AS VARCHAR) , 3 , LEN(CAST(mun.id_municipio AS VARCHAR)) ) AS CODMUNICIPIO_FCFO,
		CASE WHEN ps.id_usuario IS NOT NULL THEN 'F' ELSE 'J' END AS PESSOAFISOUJUR_FCFO,
		pa.st_nomepais AS PAIS_FCFO,
		CASE 
			WHEN pa.id_pais = 22 THEN 1
			WHEN pa.id_pais = 116 THEN 2
			WHEN pa.id_pais = 94 THEN 3
			WHEN pa.id_pais = 43 THEN 4
			WHEN pa.id_pais = 49 THEN 5
			WHEN pa.id_pais = 4 THEN 6
			WHEN pa.id_pais = 76 THEN 7
			WHEN pa.id_pais = 10 THEN 8
			WHEN pa.id_pais = 44 THEN 9
			WHEN pa.id_pais = 73 THEN 10
			ELSE 99
		END AS IDPAIS_FCFO ,
		NULL AS IDCFO_FCFO,
		NULL AS CEI_FCFO,
		CASE WHEN ps.id_pais = 22 THEN 0 ELSE 1 END AS NACIONALIDADE_FCFO,
		NULL AS TIPOCONTRIBUINTEINSS_FCFO,
		--PARAMETROS PARA O MOVIMENTO
		NULL AS IDMOV_TMOV,
		'A' AS TIPO_TMOV,
		'F' AS STATUS_TMOV,
		CAST(vd.dt_confirmacao AS DATETIME) AS DATAEMISSAO,
		ei.st_codcpg AS CODCPG_TMOV,
		vd.nu_valorbruto AS VALORBRUTO_TMOV,
		vd.nu_valorliquido AS VALORLIQUIDO_TMOV,
		(vd.nu_valorbruto - vd.nu_valorliquido) AS VALORDESC_TMOV,
		lctgerado.nu_parcelas AS NUMEROLCTGERADO_TMOV,
		lctaberto.nu_parcelas AS NUMEROLCTABERTO_TMOV,
		SUBSTRING ( CAST(emun.id_municipio AS VARCHAR) , 3 , LEN(CAST(emun.id_municipio AS VARCHAR)) ) AS CODMUNICIPIO_TMOV,
		CODETDMUNSERV_TMOV = CASE WHEN ede.sg_uf IS NOT NULL  THEN ede.sg_uf ELSE 'DF' END,
		IDNAT_TMOV = CASE WHEN ed.sg_uf IS NOT NULL AND ed.sg_uf = 'DF' THEN 181 WHEN ed.sg_uf IS NOT NULL AND ed.sg_uf <> 'DF' THEN 182 ELSE 181 END,
		IDNAT2_TMOV = CASE WHEN ed.sg_uf IS NOT NULL AND ed.sg_uf = 'DF' THEN 183 WHEN ed.sg_uf IS NOT NULL AND ed.sg_uf <> 'DF' THEN 184 ELSE 183 END,
		0 AS MOVIMPRESSO_TMOV,
		0 AS DOCIMPRESSO_TMOV,
		0 AS FATIMPRESSA_TMOV,
		'2.2.99' AS CODTMV_TMOV,
		'040' AS CODTDO_TMOV,
		0 AS INTEGRADOAUTOMACAO_TMOV,
		'T' AS INTEGRAAPLICACAO_TMOV,
		CAST(vd.dt_confirmacao AS DATETIME) AS DATALANCAMENTO_TMOV,
		0 AS RECIBONFESTATUS_TMOV,
		0 AS USARATEIOVALORFIN_TMOV,
		0 AS CODCOLCFOAUX_TMOV,
		0 AS GEROUCONTATRABALHO_TMOV,
		0 AS GERADOPORCONTATRABALHO_TMOV,
		0 AS INTEGRADOBONUM_TMOV,
		0 AS FLAPROCESSADO_TMOV,
		0 AS STSEMAIL_TMOV,
		0 AS VINCULADOESTOQUEFL_TMOV,
		1 AS CODFILIAL_TMOV,
		1 AS SERIE_TMOV,
		0 AS GEROUFATURA_TMOV,
		ei.st_codarea AS CODCXA_TMOV,
		ei.st_codsistema AS CODCOLFCO_TMOV,
		1 AS CODFILIALDESTINO_TMOV,
		0 AS GERADOPORLOTE_TMOV,
		0 AS STATUSEXPORTCONT_TMOV,
		REPLICATE('0', 9 - LEN(vd.id_venda)) + RTrim(vd.id_venda) AS NUMEROMOV_TMOV,
		0 AS PERCENTUALDESC_TMOV,
		'Venda enviada pelo gestor as '+CONVERT(VARCHAR(8), GETDATE(),  114 ) + ' de ' + CONVERT(VARCHAR, GETDATE(),  103  ) AS HISTORICOCURTO_TMOV,
		ei.st_codchave AS CODCCUSTO_TMOV,
		vd.nu_valorliquido AS VALOR,
		100 AS PERCENTUAL,
		1 AS NSEQITMOV_TMOV,
		CAST(vd.id_venda AS VARCHAR)+'G2U' AS id_venda,
		CASE WHEN mpcc.st_codsistema IS NOT NULL THEN
		mpcc.st_codsistema
		ELSE
		mpe.st_codsistema 
		END AS CODTB2FLX -- Forma de pagamento

		FROM tb_venda vd
	
		JOIN tb_usuario AS usr ON usr.id_usuario = vd.id_usuario
		JOIN tb_pessoa AS ps ON ps.id_usuario = usr.id_usuario AND ps.id_entidade = vd.id_entidade
	
		CROSS APPLY ( SELECT TOP 1 * 
						FROM tb_entidadeintegracao ei
						WHERE ei.id_entidade  = vd.id_entidade 
							AND ei.id_sistema = 29 
					) AS ei

		CROSS APPLY (SELECT COUNT(l.id_lancamento) AS nu_parcelas FROM tb_lancamentovenda lv
							JOIN tb_lancamento l ON l.id_lancamento = lv.id_lancamento 
						WHERE  vd.id_venda = lv.id_venda
							AND bl_ativo = 1
							--AND bl_original = 1	
					) AS lctgerado

		CROSS APPLY (SELECT COUNT(l.id_lancamento) AS nu_parcelas FROM tb_lancamentovenda lv
							JOIN tb_lancamento l ON l.id_lancamento = lv.id_lancamento 
						WHERE  vd.id_venda = lv.id_venda
							AND l.bl_quitado = 0
							AND l.bl_ativo = 1
							--AND bl_original = 1	
					) AS lctaberto

		OUTER APPLY(SELECT TOP 1 * 
						FROM tb_pessoaendereco AS pseo 
						WHERE pseo.id_usuario = ps.id_usuario 
							AND ps.id_entidade = pseo.id_entidade 
							AND pseo.bl_padrao = 1
					) as pse

		CROSS APPLY (SELECT TOP 1 mpic.id_meiopagamentointegracao ,
			mpic.id_cartaobandeira ,
			mpic.id_sistema ,
			mpic.id_usuariocadastro ,
			mpic.id_meiopagamento ,
			mpic.id_entidade ,
			mpic.st_codsistema ,
			mpic.dt_cadastro, 
			mpic.st_codcontacaixa 
				FROM dbo.tb_meiopagamentointegracao AS mpic 
				JOIN tb_lancamento l ON mpic.id_meiopagamento = l.id_meiopagamento 
				JOIN tb_lancamentovenda lv ON l.id_lancamento = lv.id_lancamento
				WHERE lv.id_venda = vd.id_venda
				AND mpic.id_entidade = vd.id_entidade
				) AS mpe 

		OUTER APPLY (
			SELECT TOP 1 mpi.st_codsistema 
				FROM dbo.tb_meiopagamentointegracao mpi 
					JOIN tb_cartaoconfig AS cc ON mpi.id_cartaobandeira = cc.id_cartaobandeira
					JOIN tb_lancamento l ON mpi.id_meiopagamento = l.id_meiopagamento 
					JOIN tb_lancamentovenda lv ON l.id_lancamento = lv.id_lancamento
				WHERE  mpi.id_sistema = 3 
					AND lv.id_venda = vd.id_venda
					AND mpi.id_entidade = vd.id_entidade
		) AS mpcc
	
		LEFT JOIN tb_endereco AS ed ON ed.id_endereco = pse.id_endereco
		LEFT JOIN tb_municipio AS mun ON ed.id_municipio = mun.id_municipio
		LEFT JOIN tb_contatostelefonepessoa AS ctp ON ctp.id_usuario = usr.id_usuario AND ctp.id_entidade = ps.id_entidade AND ctp.bl_padrao = 1
		LEFT JOIN tb_contatostelefone AS ctl ON ctl.id_telefone = ctp.id_telefone
		LEFT JOIN tb_contatosemailpessoaperfil AS cepp ON cepp.id_usuario = usr.id_usuario AND cepp.id_entidade = ps.id_entidade AND cepp.bl_padrao = 1
		LEFT JOIN tb_contatosemail AS ce ON ce.id_email = cepp.id_email
		LEFT JOIN tb_pais AS pa ON pa.id_pais = ed.id_pais

		OUTER APPLY(SELECT TOP 1 * 
						FROM tb_entidadeendereco AS eeo 
						WHERE eeo.id_entidade = vd.id_entidade
							AND eeo.bl_padrao = 1
					) as peeo
	
		JOIN tb_endereco AS ede ON ede.id_endereco = peeo.id_endereco
		JOIN tb_municipio AS emun ON ede.id_municipio = emun.id_municipio

	WHERE vd.id_evolucao = 10 
		AND vd.id_venda NOT IN (
			SELECT id_venda FROM tb_vendaintegracao WHERE id_sistema  = 29 AND st_codvenda IS NOT NULL
		)
	
GO

GO
/****** Object:  View [dbo].[vw_fundamentolegal]    Script Date: 10/08/2012 17:41:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_fundamentolegal] AS
select fl.*, n.st_nivelensino, tfl.st_tipofundamentolegal, s.st_situacao, u.st_login, u.st_nomecompleto, e.st_nomeentidade
from tb_fundamentolegal as fl
	LEFT JOIN tb_tipofundamentolegal as tfl ON tfl.id_tipofundamentolegal = fl.id_tipofundamentolegal
	INNER JOIN tb_situacao as s ON s.id_situacao = fl.id_situacao
	INNER JOIN tb_usuario as u ON u.id_usuario = fl.id_usuariocadastro
	INNER JOIN tb_entidade as e ON e.id_entidade = fl.id_entidade
	LEFT JOIN tb_nivelensino as n ON n.id_nivelensino = fl.id_nivelensino
GO

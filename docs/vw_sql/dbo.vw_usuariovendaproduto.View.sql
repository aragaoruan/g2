
GO
/****** Object:  View [dbo].[vw_usuariovendaproduto]    Script Date: 10/08/2012 17:42:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vw_usuariovendaproduto] as
select v.id_venda, v.id_usuario, u.st_nomecompleto, p.id_produto, p.st_produto, p.id_entidade
from tb_venda as v
INNER JOIN tb_vendaproduto as vp ON vp.id_venda = v.id_venda
INNER JOIN tb_produto as p ON p.id_produto = vp.id_produto
INNER JOIN tb_usuario as u ON u.id_usuario = v.id_usuario
where v.bl_ativo = 1 and p.bl_ativo = 1
GO

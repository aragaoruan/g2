SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_areaprodutoprojetonivelensino] AS	  
select pe.id_entidade, app.id_areaconhecimento, pp.id_projetopedagogico, pp.st_tituloexibicao,
	   ppp.id_produto,
	   pp.st_projetopedagogico, pp.bl_ativo, ne.id_nivelensino, ne.st_nivelensino, e.st_nomeentidade,
	   sit.st_situacao
from tb_projetoentidade as pe
INNER JOIN tb_projetopedagogico as pp ON pp.id_projetopedagogico = pe.id_projetopedagogico
INNER JOIN tb_areaprojetopedagogico as app ON app.id_projetopedagogico = pp.id_projetopedagogico
INNER JOIN tb_projetopedagogicoserienivelensino as ppsne ON ppsne.id_projetopedagogico = ppsne.id_projetopedagogico 
INNER JOIN tb_produtoprojetopedagogico as ppp ON ppp.id_projetopedagogico = pp.id_projetopedagogico
INNER JOIN tb_nivelensino as ne ON ne.id_nivelensino = ppsne.id_nivelensino AND pp.id_projetopedagogico = ppsne.id_projetopedagogico
INNER JOIN tb_entidade as e ON e.id_entidade = pp.id_entidadecadastro
INNER JOIN tb_situacao as sit ON sit.id_situacao = pp.id_situacao
group by pe.id_entidade, app.id_areaconhecimento, pp.id_projetopedagogico, pp.st_tituloexibicao, 
		 ppp.id_produto,
		 pp.st_projetopedagogico, pp.bl_ativo, ne.id_nivelensino, ne.st_nivelensino, e.st_nomeentidade,
	   sit.st_situacao
GO

create VIEW [dbo].[vw_salaslimitealocacao] AS

SELECT  dados.id_saladeaula,
dados.st_saladeaula,
dados.id_notificacaoentidade,
dados.id_notificacao,
n.st_notificacao,
dados.nu_maxalunos,
dados.nu_alunosalocados,
dados.nu_percentualalunossala,
dados.nu_percentuallimitealunosala,
dados.id_entidade,
uper.id_usuario AS id_usuarioprofessor,
u.st_nomecompleto AS st_nomeprofessor,
ne1.bl_enviaparaemail
FROM tb_notificacao AS n 
JOIN dbo.tb_notificacaoentidade AS ne1 ON ne1.id_notificacao = 1
OUTER APPLY (
		SELECT 
		ne.id_notificacaoentidade,
		ne.id_notificacao,
		ne.id_entidade,
		al.id_saladeaula,
		sal.st_saladeaula,
		sal.nu_maxalunos,
		ne.nu_percentuallimitealunosala,
		COUNT(al.id_alocacao) AS nu_alunosalocados,
		(COUNT(al.id_alocacao)*100/sal.nu_maxalunos) AS nu_percentualalunossala
		FROM  tb_notificacaoentidade AS ne 
		INNER JOIN tb_saladeaulaentidade AS se ON se.id_entidade = ne.id_entidade
		INNER JOIN tb_saladeaula AS sal ON sal.id_saladeaula = se.id_saladeaula AND sal.bl_ativa=1
		INNER JOIN tb_alocacao AS al ON al.id_saladeaula = sal.id_saladeaula AND al.bl_ativo=1
		AND EXISTS (SELECT * FROM dbo.tb_alocacao AS aloc WHERE aloc.id_saladeaula=al.id_saladeaula AND aloc.bl_ativo=1 AND aloc.dt_cadastro BETWEEN GETDATE()-1 AND GETDATE() )
		where  ne.id_notificacaoentidade=ne1.id_notificacaoentidade
		GROUP BY ne.id_notificacao,ne.id_notificacaoentidade,ne.id_entidade, al.id_saladeaula,sal.st_saladeaula,sal.nu_maxalunos, ne.nu_percentuallimitealunosala
		HAVING (COUNT(al.id_alocacao)*100/sal.nu_maxalunos) >= ne.nu_percentuallimitealunosala ) AS dados 
--inner join vw_usuarioperfilentidadereferencia as upe on upe.id_saladeaula=dados.id_saladeaula and nep.id_perfil = upe.id_perfil
left JOIN tb_usuarioperfilentidadereferencia AS uper ON uper.id_saladeaula=dados.id_saladeaula AND uper.bl_titular=1 AND uper.bl_ativo=1
LEFT JOIN tb_usuario AS u ON u.id_usuario=uper.id_usuario
where dados.id_saladeaula is not null
and n.id_notificacao=1









GO



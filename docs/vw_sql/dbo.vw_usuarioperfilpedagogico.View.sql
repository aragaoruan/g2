CREATE VIEW [dbo].[vw_usuarioperfilpedagogico] AS
  SELECT
    DISTINCT
    us.st_nomecompleto,
    us.id_usuario,
    us.st_login,
    us.st_senha,
    dac.st_login AS st_loginentidade,
    dac.st_senha AS st_senhaentidade,
    us.st_email,
    ps.st_nomeexibicao,
    ps.st_urlavatar,
    ps.dt_nascimento,
    upe.id_perfil,
    upe.id_entidade,
    pf.st_nomeperfil,
    ppg.st_perfilpedagogico,
    pf.id_perfilpedagogico,
    upe.dt_inicio,
    upe.dt_termino,
    pp.id_projetopedagogico,
    pp.id_trilha,
    pp.st_tituloexibicao,
    NULL AS id_saladeaula,
    NULL AS st_saladeaula,
    mt.id_matricula,
    mt.id_evolucao AS id_evolucaomatricula,
    tm.dt_inicio AS dt_inicioturma,
    et.st_nomeentidade,
    et.st_razaosocial,
    et.st_urlimglogo,
    ce.bl_bloqueiadisciplina,
    CAST(
        RIGHT(
            master.dbo.fn_varbintohexstr(
                HASHBYTES(
                    'MD5',
                    (
                      CAST(us.id_usuario AS VARCHAR(10))
                      COLLATE Latin1_General_CI_AI + CAST(
                          (
                            CASE
                            WHEN mt.id_matricula IS NULL
                              THEN
                                0
                            ELSE
                              mt.id_matricula
                            END
                          ) AS VARCHAR(15)
                      )
                      COLLATE Latin1_General_CI_AI + CAST(
                          pf.id_perfilpedagogico AS VARCHAR(15)
                      )
                      COLLATE Latin1_General_CI_AI + CAST((0) AS VARCHAR(15))
                      COLLATE Latin1_General_CI_AI + us.st_login + us.st_senha + CONVERT(VARCHAR(10), GETDATE(), 103)
                    )
                )
            ),
            32
        ) AS CHAR(32)
    )
    COLLATE Latin1_General_CI_AI AS st_urlacesso,
    cd.id_entidadeorigem AS id_entidadeportal,
    ce.bl_acessoapp,
    ei.st_codsistema,
    ei.id_sistema,
    ui.st_codusuario AS id_usuariomoodle
  FROM
    vw_pessoa us
    JOIN tb_usuarioperfilentidade AS upe ON upe.id_usuario = us.id_usuario AND us.id_entidade = upe.id_entidade
    JOIN tb_perfil AS pf ON pf.id_perfil = upe.id_perfil AND pf.id_entidade = upe.id_entidade
    JOIN tb_perfilpedagogico AS ppg ON ppg.id_perfilpedagogico = pf.id_perfilpedagogico
--LEFT JOIN tb_usuarioperfilentidadereferencia AS uper ON uper.id_usuario = upe.id_usuario AND uper.id_perfil = upe.id_perfil AND upe.id_entidade = uper.id_entidade AND pf.id_entidade = uper.id_entidade
    LEFT JOIN tb_matricula AS mt ON mt.id_usuario = us.id_usuario AND mt.id_entidadeatendimento = upe.id_entidade AND
                                    ((pf.id_perfilpedagogico = 5 AND mt.bl_institucional = 0) OR
                                     (pf.id_perfilpedagogico = 6 AND mt.bl_institucional = 1)) AND mt.bl_ativo = 1
                                    AND mt.id_evolucao NOT IN (20, 27, 41)
                                    AND (
                                      mt.dt_termino IS NULL
                                      OR mt.dt_termino >= CAST(GETDATE() AS DATE)
                                    )
    LEFT JOIN tb_turma AS tm ON mt.id_turma = tm.id_turma
    LEFT JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
    JOIN tb_entidade AS et ON et.id_entidade = upe.id_entidade
    JOIN tb_pessoa AS ps ON ps.id_usuario = us.id_usuario AND ps.id_entidade = upe.id_entidade
    LEFT JOIN tb_dadosacesso AS dac ON dac.id_usuario = us.id_usuario AND dac.id_entidade = ps.id_entidade AND
                                       dac.bl_ativo = 1
    LEFT JOIN tb_configuracaoentidade AS ce ON ce.id_entidade = et.id_entidade
    LEFT JOIN dbo.tb_compartilhadados AS cd ON cd.id_entidadedestino = et.id_entidade AND cd.id_tipodados = 2
    LEFT JOIN tb_usuariointegracao AS ui ON ui.id_usuario = us.id_usuario AND ui.id_sistema = 6 AND
                                            ui.id_entidade = upe.id_entidade
    LEFT JOIN tb_entidadeintegracao AS ei ON ei.id_sistema = ui.id_sistema AND ei.id_entidade = ui.id_entidade AND
                                             ei.id_entidadeintegracao = ui.id_entidadeintegracao
  WHERE
    upe.bl_ativo = 1
    AND pf.bl_ativo = 1
    AND upe.id_situacao = 15
    AND pf.id_situacao = 4
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_disciplinaserienivel] AS
select d.id_disciplina, d.st_disciplina, d.st_descricao, d.nu_identificador, d.nu_cargahoraria, d.bl_ativa, d.id_situacao, d.id_entidade,
s.id_serie, s.st_serie, s.id_serieanterior, ne.id_nivelensino, ne.st_nivelensino
,d.nu_repeticao
from tb_disciplina d
LEFT JOIN tb_disciplinaserienivelensino as dsn ON d.id_disciplina = dsn.id_disciplina
LEFT JOIN tb_serienivelensino as sne ON dsn.id_nivelensino = sne.id_nivelensino
LEFT JOIN tb_serie as s ON dsn.id_serie = sne.id_serie and sne.id_serie = s.id_serie
LEFT JOIN tb_nivelensino as ne ON sne.id_nivelensino = ne.id_nivelensino
where d.bl_ativa = 1
GO




GO
/****** Object:  View [dbo].[vw_dadospessoais]    Script Date: 10/08/2012 17:41:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_dadospessoais] AS
SELECT us.id_usuario, us.st_cpf, us.st_nomecompleto, us.st_login, us.st_senha, ps.id_entidade, ps.st_urlavatar
		, ce.id_email, ce.st_email
FROM tb_usuario AS us
JOIN tb_pessoa AS ps ON us.id_usuario = ps.id_usuario
LEFT JOIN tb_documentocertidaodenascimento AS dcn 
	ON dcn.id_usuario = ps.id_usuario AND dcn.id_entidade = ps.id_entidade
LEFT JOIN tb_documentodereservista AS dcr 
	ON dcr.id_usuario = ps.id_usuario AND dcr.id_entidade = ps.id_entidade
LEFT JOIN tb_documentoidentidade AS dci 
	ON dci.id_usuario = ps.id_usuario AND dci.id_entidade = ps.id_entidade
LEFT JOIN tb_documentotitulodeeleitor AS dct 
	ON dct.id_usuario = ps.id_usuario AND dct.id_entidade = ps.id_entidade
LEFT JOIN tb_pessoaendereco AS pe 
	ON pe.id_usuario = ps.id_usuario AND pe.id_entidade = ps.id_entidade AND pe.bl_padrao = 1
LEFT JOIN tb_endereco AS ed 
	ON pe.id_endereco = ed.id_endereco
LEFT JOIN tb_contatosemailpessoaperfil AS cep 
	ON cep.id_usuario = us.id_usuario AND cep.id_entidade = ps.id_entidade AND cep.bl_padrao = 1
LEFT JOIN tb_contatosemail AS ce 
	ON cep.id_email = ce.id_email
LEFT JOIN tb_contatostelefonepessoa AS ctp 
	ON ctp.id_entidade = ps.id_entidade AND ctp.id_usuario = us.id_usuario AND ctp.bl_padrao = 1
LEFT JOIN tb_contatostelefone AS ct 
	ON ct.id_telefone = ctp.id_telefone
GO

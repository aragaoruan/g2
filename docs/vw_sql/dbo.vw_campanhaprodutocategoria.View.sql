/**
Esta VW é usada no Weservice do G2
**/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER view [dbo].[vw_campanhaprodutocategoria] as

select c.id_campanhacomercial, c.st_campanhacomercial, c.id_tipodesconto, d.st_tipodesconto, c.nu_valordesconto

-- , c.bl_aplicardesconto
, c.bl_ativo
-- , c.bl_disponibilidade
-- , c.dt_cadastro
, c.id_finalidadecampanha
, f.st_finalidadecampanha
, c.dt_fim
, c.dt_inicio
, c.id_entidade
, c.id_situacao
, s.st_situacao
, c.id_tipocampanha
, tc.st_tipocampanha
, case 
	when GETDATE() between c.dt_inicio and c.dt_fim 
	then 1
	when GETDATE() > c.dt_inicio and c.dt_fim is null
	then 1
	else 0 end as bl_vigente

, p.nu_valorvenda as nu_valororiginal,

case when c.id_tipodesconto = 1 then
cast((p.nu_valorvenda - isnull(c.nu_valordesconto, 0)) as decimal(18,2))
else
cast(( p.nu_valorvenda - (p.nu_valorvenda * (isnull(c.nu_valordesconto, 0)/100)) ) as decimal(18,2))
end as nu_valorvenda
, p.id_produto, p.st_produto, p.id_tipoproduto, tp.st_tipoproduto 

from vw_produto as p 
JOIN tb_tipoproduto as tp ON tp.id_tipoproduto = p.id_tipoproduto
join tb_produtoprojetopedagogico as ppp on (ppp.id_produto = p.id_produto) 
join tb_campanhaproduto as cp on (cp.id_produto = ppp.id_produto)
join tb_campanhacomercial as c on (c.id_campanhacomercial = cp.id_campanhacomercial)
join tb_situacao as s on (c.id_situacao = s.id_situacao)
join tb_tipocampanha as tc on (c.id_tipocampanha = tc.id_tipocampanha)
join tb_finalidadecampanha as f on (f.id_finalidadecampanha = c.id_finalidadecampanha)
join tb_tipodesconto as d on (d.id_tipodesconto = c.id_tipodesconto)

union

select c.id_campanhacomercial, c.st_campanhacomercial, c.id_tipodesconto, d.st_tipodesconto, c.nu_valordesconto

-- , c.bl_aplicardesconto
, c.bl_ativo
-- , c.bl_disponibilidade
-- , c.dt_cadastro
, c.id_finalidadecampanha
, f.st_finalidadecampanha
, c.dt_fim
, c.dt_inicio
, c.id_entidade
, c.id_situacao
, s.st_situacao
, c.id_tipocampanha
, tc.st_tipocampanha
, case 
	when GETDATE() between c.dt_inicio and c.dt_fim 
	then 1
	when GETDATE() > c.dt_inicio and c.dt_fim is null
	then 1
	else 0 end as bl_vigente

, p.nu_valorvenda as nu_valororiginal,

case when c.id_tipodesconto = 1 then
cast((p.nu_valorvenda - isnull(c.nu_valordesconto, 0)) as decimal(18,2))
else
cast(( p.nu_valorvenda - (p.nu_valorvenda * (isnull(c.nu_valordesconto, 0)/100)) ) as decimal(18,2))
end as nu_valorvenda
, p.id_produto, p.st_produto, p.id_tipoproduto, tp.st_tipoproduto 

from vw_produto as p 
JOIN tb_tipoproduto as tp ON tp.id_tipoproduto = p.id_tipoproduto
join tb_categoriaproduto as catp on (catp.id_produto = p.id_produto)
join tb_categoria as cat on (catp.id_categoria = cat.id_categoria) 
join tb_campanhacategorias as cc on (cc.id_categoria = cat.id_categoria)		
join tb_campanhacomercial as c on (c.id_campanhacomercial = cc.id_campanhacomercial)
join tb_situacao as s on (c.id_situacao = s.id_situacao)
join tb_tipocampanha as tc on (c.id_tipocampanha = tc.id_tipocampanha)
join tb_finalidadecampanha as f on (f.id_finalidadecampanha = c.id_finalidadecampanha)
join tb_tipodesconto as d on (d.id_tipodesconto = c.id_tipodesconto)

union

select c.id_campanhacomercial, c.st_campanhacomercial
, c.id_tipodesconto, d.st_tipodesconto, c.nu_valordesconto
, c.bl_ativo
, c.id_finalidadecampanha
, f.st_finalidadecampanha
, c.dt_fim
, c.dt_inicio
, c.id_entidade
, c.id_situacao
, s.st_situacao
, c.id_tipocampanha
, tc.st_tipocampanha
, case 
	when GETDATE() between c.dt_inicio and c.dt_fim 
	then 1
	when GETDATE() > c.dt_inicio and c.dt_fim is null
	then 1
	else 0 end as bl_vigente

, p.nu_valorvenda as nu_valororiginal,

case when c.id_tipodesconto = 1 then
cast((p.nu_valorvenda - isnull(c.nu_valordesconto, 0)) as decimal(18,2))
else
cast(( p.nu_valorvenda - (p.nu_valorvenda * (isnull(c.nu_valordesconto, 0)/100)) ) as decimal(18,2))
end as nu_valorvenda
, p.id_produto, p.st_produto, p.id_tipoproduto, tp.st_tipoproduto 

from vw_produto as p 
JOIN tb_tipoproduto as tp ON tp.id_tipoproduto = p.id_tipoproduto
join tb_campanhacomercial as c on (c.id_entidade = p.id_entidade and c.bl_todosprodutos = 1)
join tb_situacao as s on (c.id_situacao = s.id_situacao)
join tb_tipocampanha as tc on (c.id_tipocampanha = tc.id_tipocampanha)
join tb_finalidadecampanha as f on (f.id_finalidadecampanha = c.id_finalidadecampanha)
join tb_tipodesconto as d on (d.id_tipodesconto = c.id_tipodesconto)
GO

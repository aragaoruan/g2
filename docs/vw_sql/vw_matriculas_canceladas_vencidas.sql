CREATE VIEW [dbo].[vw_matriculas_canceladas_vencidas] AS
  select mat.id_matricula,
    mat.id_usuario,
    mat.id_evolucao,
    mat.id_situacao,
    mat.bl_ativo,
    mat.id_turma,
    mat.id_cancelamento,
    mat.id_entidadeatendimento,
    mat.dt_inicio,
    mat.dt_termino,
    mat.dt_ultimoacesso,
    c.dt_solicitacao,
    c.nu_diascancelamento
  from tb_matricula as mat
join tb_cancelamento as c on c.id_cancelamento = mat.id_cancelamento
where mat.id_evolucao = 6
      and mat.id_situacao = 50
      and mat.bl_ativo = 1
      and DATEADD(day, c.nu_diascancelamento ,c.dt_solicitacao) < GETDATE()
      AND c.id_evolucao = 51
	    AND c.bl_cancelamentofinalizado = 1



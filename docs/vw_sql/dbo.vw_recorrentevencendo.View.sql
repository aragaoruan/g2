
GO

/****** Object:  View [dbo].[vw_recorrentevencendo]    Script Date: 09/04/2014 09:43:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 CREATE VIEW [dbo].[vw_recorrentevencendo]
AS
    SELECT DISTINCT
            p.id_pedidointegracao ,
            p.id_venda ,
            p.dt_vencimentocartao ,
            v.id_entidade,
            v.id_usuario,
            m.id_matricula
    FROM    tb_pedidointegracao p
            INNER JOIN dbo.tb_lancamentovenda lv ON lv.id_venda = p.id_venda
            INNER JOIN dbo.tb_lancamento l ON lv.id_lancamento = lv.id_lancamento
                                              AND l.id_meiopagamento = 11 --recorrente
            INNER JOIN dbo.tb_venda v ON v.id_venda = p.id_venda
            INNER JOIN dbo.tb_vendaproduto vp ON vp.id_venda = v.id_venda
            INNER JOIN dbo.tb_matricula m ON m.id_matricula = vp.id_matricula
    WHERE   DATEDIFF(DAY, GETDATE(), dt_vencimentocartao) = 30

GO



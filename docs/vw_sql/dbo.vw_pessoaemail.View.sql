
GO
/****** Object:  View [dbo].[vw_pessoaemail]    Script Date: 10/08/2012 17:42:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_pessoaemail] AS
select cepp.id_entidade, cepp.id_usuario, 
	   cepp.id_perfil, ctemail.id_email, ctemail.st_email, bl_padrao, cepp.bl_ativo
from tb_contatosemailpessoaperfil as cepp
 INNER JOIN tb_contatosemail as ctemail ON ctemail.id_email = cepp.id_email
 INNER JOIN tb_pessoa as p ON p.id_usuario = cepp.id_usuario AND p.id_entidade = cepp.id_entidade
GO

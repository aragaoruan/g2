CREATE view vw_professoraulasministradas as
	SELECT
		u.id_usuario,
		u.st_nomecompleto,
		igh.id_unidade,
		a.id_areaconhecimento,
		a.st_areaconhecimento,
		e.st_nomeentidade,
		gh.id_gradehoraria,
		igh.dt_diasemana,
		datepart(month,igh.dt_diasemana) as nu_mes,
		datepart(year,igh.dt_diasemana) as nu_ano,
		concat(FORMAT(gh.dt_iniciogradehoraria, 'd/M'),' à ', FORMAT(gh.dt_fimgradehoraria, 'd/MM/yy')) as dt_periodo
	FROM dbo.tb_usuario u
		JOIN tb_usuarioperfilentidade up ON up.id_usuario = u.id_usuario
		JOIN tb_perfil p ON p.id_perfil = up.id_perfil
		JOIN dbo.tb_itemgradehoraria igh ON igh.id_professor = u.id_usuario and igh.bl_ativo = 1 and igh.id_tipoaula = 1
		JOIN dbo.tb_gradehoraria gh ON gh.id_gradehoraria = igh.id_gradehoraria and gh.bl_ativo = 1 and gh.id_situacao = 135
		JOIN dbo.tb_entidade e ON igh.id_unidade = e.id_entidade
		JOIN dbo.tb_areaprojetopedagogico app ON app.id_projetopedagogico = igh.id_projetopedagogico
		JOIN dbo.tb_areaconhecimento a ON a.id_areaconhecimento = app.id_areaconhecimento
	WHERE
		p.id_perfilpedagogico = 1
		AND p.bl_ativo = 1
		AND u.bl_ativo = 1
		AND up.bl_ativo = 1
		AND u.bl_ativo = 1
	GROUP BY
		u.id_usuario,
		u.st_nomecompleto,
		igh.id_unidade,
		app.id_projetopedagogico,
		a.id_areaconhecimento,
		a.st_areaconhecimento,
		e.st_nomeentidade,
		gh.id_gradehoraria,
		gh.dt_iniciogradehoraria,
		gh.dt_fimgradehoraria,
		igh.id_disciplina,
		igh.id_gradehoraria,
		igh.dt_diasemana,
    igh.id_turno
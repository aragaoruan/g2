USE [G2H_EAD1_2014]
GO

/****** Object:  View [dbo].[vw_produtovenda]    Script Date: 03/11/2015 11:54:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_produtovenda]
AS
SELECT 
vpro.id_vendaproduto,   
vpro.id_matricula, pro.id_produto,
pro.st_produto, 
tpro.id_tipoproduto, 
tpro.st_tipoproduto, 
v.id_venda, 
v.bl_ativo, 
vpro.nu_valorbruto, 
vpro.nu_valorliquido, 
vpro.id_turma, 
vpro.bl_ativo AS 'vp.bl_ativo',
CASE 
WHEN vpro.nu_valorbruto = 0 THEN CAST(vpro.nu_desconto * 100 AS NUMERIC(30, 6)) 
WHEN vpro.nu_valorbruto != 0 THEN CAST((vpro.nu_desconto * 100) / vpro.nu_valorbruto AS NUMERIC(30, 6)) 
END AS nu_descontoporcentagem, 
----CAST(((vpro.nu_desconto*100)/vpro.nu_valorbruto) as DECIMAL(5,2)) as nu_descontoporcentagem, 
vpro.nu_desconto AS nu_descontovalor, 
pv.id_tipoprodutovalor, 
tpv.st_tipoprodutovalor, 
cc.id_campanhacomercial, 
cc.st_campanhacomercial, 
pro2.st_produto AS st_produtocombo, 
pro.id_modelovenda, 
mv.st_modelovenda, 
vpro.id_tiposelecao, ts.st_tiposelecao, v.id_usuario, ent.id_entidade
FROM tb_vendaproduto vpro 
JOIN tb_venda v 
ON vpro.id_venda = v.id_venda 
AND vpro.bl_ativo = 1
JOIN tb_produto pro 
ON vpro.id_produto = pro.id_produto 
JOIN tb_tipoproduto tpro 
ON pro.id_tipoproduto = tpro.id_tipoproduto 
LEFT JOIN tb_modelovenda AS mv 
ON (pro.id_modelovenda = mv.id_modelovenda) 
LEFT JOIN tb_produto pro2 
ON pro2.id_produto = vpro.id_produtocombo 
LEFT JOIN tb_campanhacomercial cc 
ON vpro.id_campanhacomercial = cc.id_campanhacomercial 
LEFT JOIN tb_produtovalor pv 
ON pv.id_produto = vpro.id_produto 
AND pv.dt_inicio <= GETDATE() 
AND (pv.dt_termino >= GETDATE() 
OR pv.dt_termino IS NULL) 
AND pv.id_tipoprodutovalor = 4 
LEFT JOIN tb_tipoprodutovalor AS tpv 
ON tpv.id_tipoprodutovalor = pv.id_tipoprodutovalor
LEFT JOIN tb_tiposelecao AS ts ON ts.id_tiposelecao = vpro.id_tiposelecao
LEFT JOIN tb_entidade AS ent ON ent.id_entidade = pro.id_entidade
GO



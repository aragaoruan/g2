SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_aplicadorintegracao] AS
  WITH _config AS (
      SELECT
        1 AS TRUE,
        0 AS FALSE,
        68 AS AGENDADO
  )

  SELECT
    DISTINCT
    aplicador.id_aplicadorprova,
    aplicador.bl_ativo,
    aplicador.bl_sincronizado,
    aplicador.dt_sincronizado,
    aplicador.st_aplicadorprova,
    apendereco.st_endereco,
    municipio.st_nomemunicipio,
    apendereco.sg_uf
  FROM tb_avaliacaoaplicacao AS aplicacao
    CROSS APPLY _config AS _
    JOIN tb_aplicadorprova AS aplicador ON aplicador.id_aplicadorprova = aplicacao.id_aplicadorprova
    JOIN tb_aplicadorprovaentidade AS apentidade ON apentidade.id_aplicadorprova = aplicacao.id_aplicadorprova
    LEFT JOIN vw_aplicadorendereco AS apendereco ON apendereco.id_aplicadorprova = aplicador.id_aplicadorprova
    LEFT JOIN tb_municipio AS municipio ON municipio.id_municipio = apendereco.id_municipio
  WHERE EXISTS(
            SELECT
              id_avaliacaoaplicacao,
              id_avaliacaoagendamento
            FROM tb_avaliacaoagendamento agendamento
            WHERE agendamento.id_avaliacaoaplicacao = aplicacao.id_avaliacaoaplicacao
                  AND agendamento.id_entidade = aplicacao.id_entidade
                  AND bl_ativo = _.TRUE
                  AND agendamento.id_situacao = _.AGENDADO
                  AND agendamento.nu_presenca IS NULL
                  AND agendamento.bl_sincronizado = _.FALSE AND agendamento.dt_sincronizado IS NULL
        )
        AND aplicacao.bl_ativo = _.TRUE

        AND aplicacao.dt_alteracaolimite < GETDATE()
        AND aplicacao.dt_aplicacao >= GETDATE()


GO
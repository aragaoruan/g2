
GO
/****** Object:  View [dbo].[vw_modulodisciplinaprojetotrilha]    Script Date: 10/08/2012 17:42:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_modulodisciplinaprojetotrilha] as
SELECT DISTINCT
	p.st_projetopedagogico,
	m.id_modulo,
	m.st_tituloexibicao AS st_modulo,
	m.bl_ativo,
	m.id_situacao,
	m.id_projetopedagogico,
	d.id_disciplina,
	CASE
	WHEN d.st_tituloexibicao IS NULL THEN
	d.st_disciplina
ELSE
	d.st_tituloexibicao
END AS st_disciplina,
 d.nu_cargahoraria,
 d.nu_creditos,
 d.nu_codigoparceiro,
 d.id_tipodisciplina,
 d.id_entidade,
 t.id_trilha,
 NULL AS id_serie,
 NULL AS id_nivelensino,
 md.id_areaconhecimento
FROM
	tb_modulo AS m
INNER JOIN tb_modulodisciplina AS md ON md.id_modulo = m.id_modulo
AND md.bl_ativo = 1
INNER JOIN tb_disciplina AS d ON d.id_disciplina = md.id_disciplina
INNER JOIN tb_projetopedagogico AS p ON p.id_projetopedagogico = m.id_projetopedagogico
LEFT JOIN tb_trilha AS t ON t.id_trilha = p.id_trilha
WHERE
	m.bl_ativo = 1
GO

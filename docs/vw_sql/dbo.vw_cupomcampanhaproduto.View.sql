
/****** Object:  View [dbo].[vw_cupomcampanhaproduto]    Script Date: 27/11/2014 18:57:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[vw_cupomcampanhaproduto] AS
SELECT 
	c.id_cupom,
	c.st_codigocupom,
	c.dt_inicio,
	c.dt_fim,
	cc.id_campanhacomercial,
	cc.st_campanhacomercial,
	cp.id_produto,
        p.st_produto,
	p.nu_valor,
	CASE td.id_tipodesconto 
		WHEN 1 THEN CONVERT(DECIMAL(10,2), c.nu_desconto)
		WHEN 2 THEN CONVERT(DECIMAL(10,2), p.nu_valorvenda * (c.nu_desconto/100))
	END AS nu_desconto,
	td.id_tipodesconto,
	td.st_tipodesconto,
	nu_valorliquido = CASE  
		WHEN td.id_tipodesconto = 1 AND CONVERT(DECIMAL(10,2), p.nu_valorvenda - c.nu_desconto) > 0 THEN CONVERT(DECIMAL(10,2), p.nu_valorvenda - c.nu_desconto)
		WHEN td.id_tipodesconto = 2 AND CONVERT(DECIMAL(10,2), p.nu_valorvenda - (p.nu_valorvenda * (c.nu_desconto/100))) > 0 THEN CONVERT(DECIMAL(10,2), p.nu_valorvenda - (p.nu_valorvenda * (c.nu_desconto/100)))
	ELSE 0
	END ,
	c.bl_usarvalorminimo,
	c.nu_valorminimo
FROM tb_cupom c
	JOIN tb_campanhacomercial AS cc ON cc.id_campanhacomercial = c.id_campanhacomercial
	JOIN tb_tipodesconto AS td ON td.id_tipodesconto = c.id_tipodesconto
	JOIN tb_campanhaproduto AS cp ON cp.id_campanhacomercial = cc.id_campanhacomercial
	JOIN vw_produto AS p ON p.id_produto = cp.id_produto

	AND c.bl_ativacao = 1 --fixo
	AND c.bl_ativo = 1 --fixo
	AND cc.bl_ativo = 1 --fixo
	AND p.bl_ativo = 1 -- fixo
	AND c.dt_inicio <= GETDATE()
	AND c.dt_fim >= GETDATE()
	AND cc.dt_inicio <= GETDATE()
	AND (cc.dt_fim >= GETDATE() OR cc.dt_fim IS NULL)

	UNION
  
  SELECT 
	c.id_cupom,
	c.st_codigocupom,
	c.dt_inicio,
	c.dt_fim,
	cc.id_campanhacomercial,
	cc.st_campanhacomercial,
	p.id_produto,
        p.st_produto,
	p.nu_valor,
	CASE td.id_tipodesconto 
		WHEN 1 THEN CONVERT(DECIMAL(10,2), c.nu_desconto)
		WHEN 2 THEN CONVERT(DECIMAL(10,2), p.nu_valorvenda * (c.nu_desconto/100))
	END AS nu_desconto,
	td.id_tipodesconto,
	td.st_tipodesconto,
		nu_valorliquido = CASE  
		WHEN td.id_tipodesconto = 1 AND CONVERT(DECIMAL(10,2), p.nu_valorvenda - c.nu_desconto) > 0 THEN CONVERT(DECIMAL(10,2), p.nu_valorvenda - c.nu_desconto)
		WHEN td.id_tipodesconto = 2 AND CONVERT(DECIMAL(10,2), p.nu_valorvenda - (p.nu_valorvenda * (c.nu_desconto/100))) > 0 THEN CONVERT(DECIMAL(10,2), p.nu_valorvenda - (p.nu_valorvenda * (c.nu_desconto/100)))
	ELSE 0 END,
	c.bl_usarvalorminimo,
	c.nu_valorminimo
FROM tb_cupom c
	JOIN tb_campanhacomercial AS cc ON cc.id_campanhacomercial = c.id_campanhacomercial
	JOIN tb_tipodesconto AS td ON td.id_tipodesconto = c.id_tipodesconto
	JOIN dbo.tb_campanhacategorias AS cp ON cp.id_campanhacomercial = cc.id_campanhacomercial
	JOIN dbo.tb_categoriaproduto AS cpd ON cp.id_categoria = cpd.id_categoria
	JOIN vw_produto AS p ON p.id_produto = cpd.id_produto
WHERE 
	c.bl_ativacao = 1 --fixo
	AND c.bl_ativo = 1 --fixo
	AND cc.bl_ativo = 1 --fixo
	AND p.bl_ativo = 1 -- fixo
	AND c.dt_inicio <= GETDATE()
	AND c.dt_fim >= GETDATE()
	AND cc.dt_inicio <= GETDATE()
	AND (cc.dt_fim >= GETDATE() OR cc.dt_fim IS NULL)  


	UNION
  
  SELECT 
	c.id_cupom,
	c.st_codigocupom,
	c.dt_inicio,
	c.dt_fim,
	cc.id_campanhacomercial,
	cc.st_campanhacomercial,
	p.id_produto,
        p.st_produto,
	p.nu_valor,
	CASE td.id_tipodesconto 
		WHEN 1 THEN CONVERT(DECIMAL(10,2), c.nu_desconto)
		WHEN 2 THEN CONVERT(DECIMAL(10,2), p.nu_valorvenda * (c.nu_desconto/100))
	END AS nu_desconto,
	td.id_tipodesconto,
	td.st_tipodesconto,
		nu_valorliquido = CASE  
		WHEN td.id_tipodesconto = 1 AND CONVERT(DECIMAL(10,2), p.nu_valorvenda - c.nu_desconto) > 0 THEN CONVERT(DECIMAL(10,2), p.nu_valorvenda - c.nu_desconto)
		WHEN td.id_tipodesconto = 2 AND CONVERT(DECIMAL(10,2), p.nu_valorvenda - (p.nu_valorvenda * (c.nu_desconto/100))) > 0 THEN CONVERT(DECIMAL(10,2), p.nu_valorvenda - (p.nu_valorvenda * (c.nu_desconto/100)))
	ELSE 0 END,
	c.bl_usarvalorminimo,
	c.nu_valorminimo
FROM tb_cupom c
	JOIN tb_campanhacomercial AS cc ON cc.id_campanhacomercial = c.id_campanhacomercial
	JOIN tb_tipodesconto AS td ON td.id_tipodesconto = c.id_tipodesconto
	JOIN vw_produto AS p ON  cc.id_entidade = p.id_entidade
WHERE 
	c.bl_ativacao = 1 --fixo
	AND c.bl_ativo = 1 --fixo
	AND cc.bl_ativo = 1 --fixo
	AND cc.bl_todosprodutos = 1
	AND p.bl_ativo = 1 -- fixo
	AND c.dt_inicio <= GETDATE()
	AND c.dt_fim >= GETDATE()
	AND cc.dt_inicio <= GETDATE()
	AND (cc.dt_fim >= GETDATE() OR cc.dt_fim IS NULL)  










GO



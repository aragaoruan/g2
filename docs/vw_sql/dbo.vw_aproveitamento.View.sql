SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_aproveitamento] as
select
us.id_usuario, us.st_nomecompleto, ad.st_instituicao,ad.st_curso, amd.st_cargahoraria,amd.st_disciplinaoriginal AS st_disciplina, ad.sg_uf, ad.id_usuariocadastro, ad.id_municipio,ad.id_aproveitamentodisciplina, ad.dt_conclusao, ad.dt_cadastro, ad.bl_ativo
, amd.id_matricula,amd.id_disciplina, md.nu_aprovafinal, mt.id_entidadematricula, et.id_entidade, dc.st_disciplina AS st_disciplinaaproveitada, amd.st_cargahoraria AS st_cargahorariadisciplina, amd.nu_anoconclusao, amd.st_notaoriginal
, amd.id_serie, amd.st_disciplinaoriginal, amd.id_apmatriculadisciplina, pp.st_tituloexibicao, amd.nu_aproveitamento
from tb_aproveitamentodisciplina ad
JOIN tb_aproveitamentomatriculadisciplina as amd ON amd.id_aproveitamentodisciplina = ad.id_aproveitamentodisciplina
JOIN tb_matricula as mt ON mt.id_matricula = amd.id_matricula
JOIN tb_projetopedagogico as pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
JOIN tb_usuario as us ON us.id_usuario = mt.id_usuario
JOIN tb_municipio as mc ON mc.id_municipio = ad.id_municipio
JOIN tb_uf as uf ON uf.sg_uf = ad.sg_uf
JOIN dbo.tb_entidade AS et ON et.id_entidade = mt.id_entidadematricula OR et.id_entidade = id_entidadematriz
LEFT JOIN tb_disciplina as dc ON dc.id_disciplina = amd.id_disciplina
LEFT JOIN tb_matriculadisciplina as md ON md.id_disciplina = amd.id_disciplina and md.id_matricula = amd.id_matricula
GO
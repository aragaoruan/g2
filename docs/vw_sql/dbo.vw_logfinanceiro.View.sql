CREATE VIEW vw_logfinanceiro AS
  SELECT
    lg.id_log AS id_log,
    lg.dt_cadastro AS dt_cadastro,
    concat(lg.st_tabela, '.', lg.st_coluna) AS st_campo,
    lg.st_depois AS st_depois,
    us.st_nomecompleto AS st_usuario,
    et.id_entidade AS id_entidade,
    et.st_nomeentidade AS st_nomeentidade
  FROM tb_log lg
    JOIN tb_entidadefinanceiro ef ON ef.id_entidadefinanceiro = lg.id_tabela
    JOIN tb_entidade et ON et.id_entidade = ef.id_entidade
    JOIN tb_usuario us ON us.id_usuario = lg.id_usuario
  WHERE
    lg.st_tabela = 'tb_entidadefinanceiro'
  UNION

  SELECT
    lg.id_log AS id_log,
    lg.dt_cadastro AS dt_cadastro,
    concat(lg.st_tabela, '.', lg.st_coluna) AS st_campo,
    lg.st_depois AS st_depois,
    us.st_nomecompleto AS st_usuario,
    et.id_entidade AS id_entidade,
    et.st_nomeentidade AS st_nomeentidade
  FROM tb_log lg
    JOIN tb_contaentidade ce ON ce.id_contaentidade = lg.id_tabela
    JOIN tb_entidade et ON et.id_entidade = ce.id_entidade
    JOIN tb_usuario us ON us.id_usuario = lg.id_usuario
  WHERE
    lg.st_tabela = 'tb_contaentidade'
  UNION

  SELECT
    lg.id_log AS id_log,
    lg.dt_cadastro AS dt_cadastro,
    concat(lg.st_tabela, '.', lg.st_coluna) AS st_campo,
    lg.st_depois AS st_depois,
    us.st_nomecompleto AS st_usuario,
    et.id_entidade AS id_entidade,
    et.st_nomeentidade AS st_nomeentidade
  FROM tb_log lg
    JOIN tb_entidadeintegracao ei ON ei.id_entidadeintegracao = lg.id_tabela
    JOIN tb_entidade et ON et.id_entidade = ei.id_entidade
    JOIN tb_usuario us ON us.id_usuario = lg.id_usuario
  WHERE
    lg.st_tabela = 'tb_entidadeintegracao'
  UNION

  SELECT
    lg.id_log AS id_log,
    lg.dt_cadastro AS dt_cadastro,
    concat(lg.st_tabela, '.', lg.st_coluna) AS st_campo,
    lg.st_depois AS st_depois,
    us.st_nomecompleto AS st_usuario,
    et.id_entidade AS id_entidade,
    et.st_nomeentidade AS st_nomeentidade
  FROM tb_log lg
    JOIN tb_cartaoconfig cc ON cc.id_cartaoconfig = lg.id_tabela
    JOIN tb_contaentidade ce ON ce.id_contaentidade = cc.id_contaentidade
    JOIN tb_entidade et ON et.id_entidade = ce.id_entidade
    JOIN tb_usuario us ON us.id_usuario = lg.id_usuario
  WHERE
    lg.st_tabela = 'tb_cartaoconfig'
GO

GO
/****** Object:  View [dbo].[vw_projetoarea]    Script Date: 10/08/2012 17:42:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_projetoarea] AS

SELECT DISTINCT
    ap.id_areaconhecimento,
    ac.st_areaconhecimento,
    ap.id_projetopedagogico,
    pp.st_projetopedagogico,
    pp.st_tituloexibicao,
    null AS tipo,
    pe.id_entidade,
    aps.nu_diasacesso
FROM tb_areaconhecimento ac
JOIN tb_areaprojetopedagogico AS ap ON ap.id_areaconhecimento = ac.id_areaconhecimento
JOIN tb_projetopedagogico AS pp ON ap.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_projetoentidade AS pe ON pp.id_projetopedagogico = pe.id_projetopedagogico
JOIN tb_areaprojetosala AS aps ON aps.id_projetopedagogico = pe.id_projetopedagogico
GO

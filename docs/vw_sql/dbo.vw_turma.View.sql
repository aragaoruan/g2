
GO
/****** Object:  View [dbo].[vw_turma]    Script Date: 10/08/2012 17:42:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_turma] as

SELECT   t.id_turma, t.id_situacao, s.st_situacao, t.id_entidadecadastro, t.st_turma, t.st_tituloexibicao
,t.nu_maxalunos, t.dt_inicioinscricao, t.dt_fiminscricao, t.dt_inicio, t.dt_fim, t.bl_ativo 
FROM tb_turma AS t
JOIN tb_situacao AS s ON s.id_situacao = t.id_situacao
GO


GO
/****** Object:  View [dbo].[vw_matriculacontratovendaprodutolancamento]    Script Date: 10/08/2012 17:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vw_matriculacontratovendaprodutolancamento] as
-- VIEW EM TESTE
SELECT m.id_matricula,m.id_usuario,u.st_nomecompleto,m.id_situacao as id_situacaomatricula,situacaomatricula.st_situacao as st_situacaomatricula,m.id_periodoletivo as id_matriculaperiodoletivo,m.bl_ativo as bl_ativomatricula,
	   m.id_projetopedagogico,pp.st_projetopedagogico,
	   m.id_entidadematricula,ematricula.st_nomeentidade as st_entidadematricula,m.id_entidadeatendimento,eatendimento.st_nomeentidade as st_entidadeatendimento,
	   m.id_entidadematriz,ematriz.st_nomeentidade as st_entidadematriz,m.id_evolucao,mevolucao.st_evolucao as st_evolucaomatricula,
	   c.id_contrato, c.id_entidade as id_entidadecontrato,v.nu_parcelas as nu_parcelascontrato,
	   econtrato.st_nomeentidade as st_entidadecontrato,cevolucao.st_evolucao as st_evolucaocontrato,c.bl_ativo as bl_ativocontrato, c.id_situacao as id_situacaocontrato, situacaocontrato.st_situacao as st_situacaocontrato,
	   m.id_vendaproduto, vp.id_produto,pv.id_produtovalor,p.id_tipoproduto,tp.st_tipoproduto,
	   vp.id_venda,vp.nu_desconto as nu_descontovendaproduto, vp.nu_valorliquido as nu_valorliquidovendaproduto, v.nu_descontoporcentagem as nu_descontoporcentagemvenda, v.nu_descontovalor as nu_descontovalorvenda, 
	   v.nu_juros as nu_jurosvenda, v.nu_valorliquido as  nu_valorliquidovenda, v.nu_valorbruto as nu_valorbrutovenda, v.id_entidade as id_entidadevenda, evenda.st_nomeentidade as st_entidadevenda, v.id_campanhacomercial,cc.st_campanhacomercial as st_campanhacomercialvenda,
	   v.id_formapagamento as id_formapagamentovenda, fp.st_formapagamento as st_formapagamentovenda,
	   lv.id_lancamento,tl.id_tipolancamento,tl.st_tipolancamento,lv.bl_entrada as bl_entradalancamentovenda,l.nu_valor as nu_valorlancamento,l.nu_vencimento as nu_vencimentolancamento,l.id_meiopagamento as id_meiopagamentolancamento,mp.st_meiopagamento as st_meiopagamentolancamento
FROM tb_matricula as m
	INNER JOIN tb_usuario as u on u.id_usuario = m.id_usuario
	INNER JOIN tb_entidade as ematricula on ematricula.id_entidade = m.id_entidadematricula
	INNER JOIN tb_entidade as eatendimento on eatendimento.id_entidade = m.id_entidadeatendimento
	INNER JOIN tb_entidade as ematriz on ematriz.id_entidade = m.id_entidadeatendimento
	INNER JOIN tb_evolucao as mevolucao on mevolucao.id_evolucao = m.id_evolucao
	INNER JOIN tb_projetopedagogico as pp on pp.id_projetopedagogico = m.id_projetopedagogico
	INNER JOIN tb_contratomatricula as cm on cm.id_matricula = m.id_matricula
	INNER JOIN tb_contrato as c on c.id_contrato = cm.id_contrato
	INNER JOIN tb_entidade as econtrato on econtrato.id_entidade = c.id_entidade
	INNER JOIN tb_evolucao as cevolucao on cevolucao.id_evolucao = c.id_evolucao
	INNER JOIN tb_situacao as situacaocontrato on situacaocontrato.id_situacao = c.id_situacao
	INNER JOIN tb_situacao as situacaomatricula on situacaomatricula.id_situacao = m.id_situacao
	-- O INNER JOIN ABAIXO JUNTA A TABELA DE VENDAPRODUTO COM A MATRICULA
	INNER JOIN tb_vendaproduto as vp on vp.id_vendaproduto = m.id_vendaproduto
	--------------------------------------------------------------------------
	INNER JOIN tb_produto as p on p.id_produto = vp.id_produto
	INNER JOIN tb_tipoproduto as tp on tp.id_tipoproduto = p.id_tipoproduto
	INNER JOIN tb_produtoprojetopedagogico as ppp on ppp.id_produto = p.id_produto and m.id_projetopedagogico = ppp.id_projetopedagogico
	INNER JOIN tb_produtovalor as pv on pv.id_produto = p.id_produto
	INNER JOIN tb_venda as v on v.id_venda = vp.id_venda
	INNER JOIN tb_formapagamento as fp on fp.id_formapagamento = v.id_formapagamento
	INNER JOIN tb_entidade as evenda on evenda.id_entidade = v.id_entidade
	INNER JOIN tb_campanhacomercial as cc on cc.id_campanhacomercial = v.id_campanhacomercial
	-- O INNER JOIN ABAIXO JUNTA A TABELA DE LANCAMENTOVENDA COM A TABELA DE VENDA
	LEFT JOIN tb_lancamentovenda as lv on lv.id_venda = v.id_venda
	LEFT JOIN tb_lancamento as l on l.id_lancamento = lv.id_lancamento
	-------------------------------------------------------------------------------
	INNER JOIN tb_meiopagamento as mp on mp.id_meiopagamento = l.id_meiopagamento
	INNER JOIN tb_tipolancamento as tl on tl.id_tipolancamento = l.id_tipolancamento
GO

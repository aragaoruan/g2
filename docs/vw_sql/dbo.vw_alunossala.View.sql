ALTER VIEW [dbo].[vw_alunossala] AS
SELECT DISTINCT al.id_alocacao ,
al.id_saladeaula ,
mt.id_usuario,
mt.st_cpf,
mt.st_email,
mt.st_nomecompleto,
md.id_matricula ,
td.id_disciplina ,
td.id_tipodisciplina,
md.id_matriculadisciplina ,
st_nota,
lg.id_logacesso,
si.id_sistema,
sa.id_entidade,
mt.id_projetopedagogico,
mt.st_projetopedagogico,
mt.st_evolucao,
al.id_situacaotcc,
aa.id_upload,
tu1.id_usuario AS id_professor,
tu1.st_nomecompleto AS st_professor,
eprof.id_email AS id_emailprofessor,
eprof.st_email AS st_emailprofessor,
mt.st_urlacesso,
mt.id_entidadematricula as id_entidadematricula,
al.dt_inicio,
DATEADD(DAY,ISNULL(sa.nu_diasaluno,0), al.dt_inicio) as dt_termino,
sa.id_categoriasala
FROM dbo.tb_alocacao al
JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula
JOIN dbo.tb_matriculadisciplina AS md ON md.id_matriculadisciplina = al.id_matriculadisciplina
JOIN dbo.tb_disciplina AS td ON td.id_disciplina = md.id_disciplina
JOIN dbo.vw_matricula AS mt ON mt.id_matricula = md.id_matricula AND mt.bl_ativo = 1
OUTER APPLY (SELECT TOP 1 id_logacesso FROM dbo.tb_logacesso WHERE id_saladeaula = al.id_saladeaula AND id_usuario = mt.id_usuario ) AS lg
LEFT JOIN  dbo.vw_avaliacaoaluno AS aa ON aa.id_saladeaula = al.id_saladeaula AND aa.id_matricula = mt.id_matricula AND aa.bl_ativo = 1 AND aa.id_tipoavaliacao =6
LEFT JOIN dbo.tb_saladeaulaintegracao AS si ON si.id_saladeaula = al.id_saladeaula
LEFT JOIN tb_usuarioperfilentidadereferencia tu ON al.id_saladeaula = tu.id_saladeaula AND tu.bl_titular = 1 AND tu.bl_ativo = 1
LEFT JOIN tb_usuario tu1 ON tu.id_usuario = tu1.id_usuario
LEFT JOIN tb_contatosemailpessoaperfil AS conte ON conte.id_usuario=tu1.id_usuario AND conte.bl_ativo=1 AND conte.bl_padrao=1 AND tu.id_entidade = conte.id_entidade
LEFT JOIN tb_contatosemail AS eprof ON eprof.id_email=conte.id_email WHERE al.bl_ativo = 1
GO

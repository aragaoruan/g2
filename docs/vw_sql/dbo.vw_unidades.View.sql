
GO
/****** Object:  View [dbo].[vw_unidades]    Script Date: 10/08/2012 17:42:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_unidades] AS
SELECT  er.nu_entidadepai AS id_entidadematriz ,
et.id_entidade,
et.st_nomeentidade ,
et.st_razaosocial ,
et.st_urlsite ,
et.st_cnpj ,
ee.st_endereco ,
ee.st_complemento ,
ee.nu_numero ,
ee.st_nomemunicipio ,
ee.sg_uf ,
ee.st_cep ,
ee.st_nomepais ,
ee.st_bairro ,
etl.nu_ddi ,
etl.nu_ddd ,
etl.nu_telefone
FROM    dbo.vw_entidaderecursivaid AS er
JOIN dbo.tb_entidade AS et ON et.id_entidade = er.id_entidade
JOIN dbo.vw_entidadeendereco AS ee ON ee.id_entidade = et.id_entidade
AND ee.bl_enderecoentidadepadrao = 1
JOIN dbo.vw_entidadetelefone AS etl ON etl.id_entidade = et.id_entidade
AND etl.bl_padrao = 1
GO

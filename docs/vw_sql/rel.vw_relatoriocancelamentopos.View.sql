CREATE VIEW rel.vw_relatoriocancelamentopos AS
SELECT DISTINCT
  vw.st_nomecompleto,
  vw.st_cpf,
  vw.st_email,
  vw.st_projetopedagogico,
  vw.id_matricula,
  can.dt_cadastro,
  evCan.st_evolucao as st_cancelamento,
  vw.id_venda,
  v.dt_confirmacao,
  vdp.nu_valorliquido as nu_valorcurso,
  COALESCE((SELECT SUM(wf.nu_quitado) FROM vw_resumofinanceiro wf
  WHERE wf.id_venda = vw.id_venda and wf.bl_quitado = 1),0) as nu_valorpago,
  can.nu_valordiferenca as nu_valordevido,
  (
    SELECT
      STUFF(
          (
            SELECT DISTINCT
              ' / ' + st_motivo
            FROM
              tb_motivocancelamento AS mc
              JOIN tb_motivo mot ON mc.id_motivo = mot.id_motivo
                                    AND mc.id_cancelamento = vw.id_cancelamento FOR XML PATH ('')
          ),
          1,
          1,
          ''
      )
  ) AS st_motivos,
  evCan.id_evolucao,
  vw.st_evolucao,
  u.st_nomecompleto as st_atendente,
  vw.id_entidadematricula as id_entidade
FROM
  dbo.vw_matricula AS vw
  JOIN tb_areaprojetopedagogico AS app ON vw.id_areaconhecimento = app.id_areaconhecimento
  JOIN tb_areaconhecimento AS ac ON app.id_areaconhecimento = ac.id_areaconhecimento
  JOIN tb_cancelamento AS can ON can.id_cancelamento = vw.id_cancelamento AND can.id_ocorrencia IS NOT NULL
  JOIN tb_evolucao AS evCan ON can.id_evolucao = evCan.id_evolucao
  LEFT JOIN tb_usuario u ON u.id_usuario = can.id_usuariocadastro
  LEFT JOIN tb_venda v ON v.id_venda = vw.id_venda
  LEFT JOIN tb_vendaproduto AS vdp ON vdp.id_venda = vw.id_venda AND vdp.id_vendaproduto = vw.id_vendaproduto
  LEFT JOIN tb_vendaaditivo AS va ON va.id_venda = vw.id_venda AND vw.id_matricula = va.id_matricula AND
                                     va.bl_cancelamento = 0
                                     AND can.id_evolucao IN (49,50,51,52)

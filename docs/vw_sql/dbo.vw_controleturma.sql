CREATE view vw_controleturma as
SELECT
    et.st_nomeentidade as st_unidade,
    et.id_entidade as id_unidade,
    tm.id_turma,
    tm.st_turma,
    tm.dt_inicio,
    tm.dt_fim,
    tm.st_codigo,
    gh.id_gradehoraria,
    gh.st_nomegradehoraria,
    gh.dt_iniciogradehoraria,
    igh.dt_diasemana,
    gh.dt_fimgradehoraria,
    igh.id_professor,
    us.st_nomecompleto as st_professor,
    dc.id_disciplina,
    dc.st_disciplina,
    eci.st_valor,
    md.id_modulo,
    md.st_modulo,
    dc.nu_cargahoraria as nu_cargahorariadisciplina,
    (COUNT(igh.id_itemgradehoraria)*IIF(st_valor = '',0,(CAST(ISNULL(st_valor,0) AS INT)))) as nu_cargahorariagrade,
    (dc.nu_cargahoraria - (COUNT(igh.id_itemgradehoraria)*IIF(st_valor = '',0,(CAST(ISNULL(st_valor,0) AS INT))))) as nu_cargarestante,
    pp.id_projetopedagogico,
    pp.st_projetopedagogico,
    pp.nu_cargahoraria as nu_cargahorariaprojeto,
    tur.st_turno
FROM tb_turma AS tm
JOIN tb_produtoprojetopedagogico AS ppp ON ppp.id_turma =  tm.id_turma
JOIN tb_entidade AS et ON et.id_entidade = tm.id_entidadecadastro
JOIN tb_esquemaconfiguracaoitem AS eci ON eci.id_esquemaconfiguracao = et.id_esquemaconfiguracao AND eci.id_itemconfiguracao = 3
JOIN tb_projetopedagogico as pp on pp.id_projetopedagogico = ppp.id_projetopedagogico
JOIN tb_modulo as md ON md.id_projetopedagogico = pp.id_projetopedagogico and md.bl_ativo = 1
JOIN tb_modulodisciplina as mdc on mdc.id_modulo = md.id_modulo and mdc.bl_ativo = 1
JOIN tb_disciplina AS dc ON dc.id_disciplina = mdc.id_disciplina
LEFT JOIN tb_itemgradehoraria AS igh ON igh.id_turma = tm.id_turma AND igh.bl_ativo = 1 and dc.id_disciplina = igh.id_disciplina
LEFT JOIN tb_gradehoraria AS gh ON gh.id_gradehoraria = igh.id_gradehoraria
LEFT JOIN dbo.tb_turmaturno AS tmt ON tmt.id_turma = tm.id_turma
LEFT JOIN dbo.tb_turno AS tur ON tur.id_turno = tmt.id_turno
LEFT JOIN dbo.tb_usuario AS us ON igh.id_professor = us.id_usuario
GROUP BY et.st_nomeentidade,
    et.st_nomeentidade,
    et.id_entidade,
    tm.id_turma,
    TM.st_turma,
    tm.dt_inicio,
    tm.dt_fim,
    tm.st_codigo,
    gh.id_gradehoraria,
    gh.st_nomegradehoraria,
    gh.dt_iniciogradehoraria,
    gh.dt_fimgradehoraria,
    igh.dt_diasemana,
    igh.id_professor,
    us.st_nomecompleto,
    dc.id_disciplina,
    dc.st_disciplina,
    eci.st_valor,
    dc.nu_cargahoraria,
    md.id_modulo,
    md.st_modulo,
    pp.id_projetopedagogico,
    pp.st_projetopedagogico,
    pp.nu_cargahoraria,
    tur.st_turno
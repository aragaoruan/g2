
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE VIEW [dbo].[vw_produtoarea] AS



select a.id_areaconhecimento
, a.st_areaconhecimento
, a.id_entidade
, a.id_situacao
, s.st_situacao
, pa.id_produtoarea
, pa.id_produto

 from tb_areaconhecimento as a 
 join tb_situacao as s on (s.id_situacao = a.id_situacao)
 join tb_produtoarea as pa on (a.id_areaconhecimento = pa.id_areaconhecimento)
 where a.bl_ativo = 1
GO
-- sp_help vw_areaconhecimento
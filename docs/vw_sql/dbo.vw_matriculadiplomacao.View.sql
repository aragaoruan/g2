CREATE VIEW [dbo].[vw_matriculadiplomacao]
AS
  SELECT
    DISTINCT
    mt.id_matricula,
    mt.id_usuario,
    us.st_nomecompleto,
    us.st_cpf,
    ps.st_email,
    pp.id_projetopedagogico,
    pp.st_projetopedagogico,
    pp.st_reconhecimento,
    vp.id_tiposelecao,
    vp.dt_ingresso,
    mt.id_situacao,
    st.st_situacao,
    mt.id_evolucao,
    ev.st_evolucao,
    mt.dt_concluinte,
    mt.id_entidadematriz,
    etmz.st_nomeentidade AS st_entidadematriz,
    mt.id_entidadematricula,
    etm.st_nomeentidade AS st_entidadematricula,
    mt.id_entidadeatendimento,
    eta.st_nomeentidade AS st_entidadeatendimento,
    mt.bl_ativo,
    mt.dt_termino,
    mt.dt_cadastro,
    mt.dt_inicio,
    tm.id_turma,
    mt.id_turmaorigem,
    tm.st_turma,
    tm.st_codigo,
    tm.dt_inicio AS dt_inicioturma,
    mt.bl_institucional,
    tm.dt_fim AS dt_terminoturma,
    mt.dt_termino AS dt_terminomatricula,
    mt.bl_documentacao,
    mt.bl_academico,
      st_statusdocumentacao = CASE
                              WHEN mt.bl_documentacao = 1
                                THEN 'OK'
                              WHEN mt.bl_documentacao = 0
                                THEN 'Pendente'
                              ELSE 'Não Analisado'
                              END,
    mc.dt_colacao,
    mc.bl_presenca,
    mtd.dt_aptodiplomar,
    mtd.id_usuarioaptodiplomar,
    mtd.dt_diplomagerado,
    mtd.id_usuariodiplomagerado,
    mtd.dt_historicogerado,
    mtd.id_usuariohistoricogerado,
    mtd.dt_equivalenciagerado,
    mtd.id_usuarioequivalenciagerado,
    mtd.dt_enviadoassinatura,
    mtd.id_usuarioenviadoassinatura,
    mtd.dt_retornadoassinatura,
    mtd.id_usuarioretornadoassinatura,
    mtd.dt_enviadosecretaria,
    mtd.id_usuarioenviadosecretaria,
    mtd.dt_enviadoregistro,
    mtd.id_usuarioenviadoregistro,
    mtd.dt_retornoregistro,
    mtd.id_usuarioretornoregistro,
    mtd.dt_enviopolo,
    mtd.id_usuarioenviopolo,
    mtd.dt_entreguealuno,
    mtd.id_usuarioentreguealuno,
    mtd.st_diplomanumero,
    mtd.st_diplomalivro,
    mtd.st_diplomafolha,
    mtd.bl_ativo AS bl_ativodiplomacao,
    mtd.id_matriculadiplomacao,
    mtd.st_cedula,
    mtd.st_motivo,
    mtd.st_observacao,
    mt.id_evolucaodiplomacao,
    mc.id_matriculacolacao,
      bl_segundavia = (CASE
                       WHEN seg_via.id_matriculadiplomacao IS NOT NULL
                         THEN 1
                       ELSE 0
                       END),
    mtd.dt_registroprocesso
  FROM tb_matricula AS mt
    JOIN tb_projetopedagogico AS pp
      ON mt.id_projetopedagogico = pp.id_projetopedagogico
    JOIN tb_areaprojetopedagogico AS ap
      ON ap.id_projetopedagogico = pp.id_projetopedagogico
    JOIN tb_entidade AS etmz
      ON etmz.id_entidade = mt.id_entidadematriz
    JOIN tb_entidade AS etm
      ON etm.id_entidade = mt.id_entidadematricula
    JOIN tb_entidade AS eta
      ON eta.id_entidade = mt.id_entidadeatendimento
    JOIN tb_situacao AS st
      ON st.id_situacao = mt.id_situacao
    JOIN tb_evolucao AS ev
      ON ev.id_evolucao = mt.id_evolucao
    JOIN tb_usuario AS us
      ON us.id_usuario = mt.id_usuario
    LEFT JOIN vw_pessoa AS ps
      ON mt.id_usuario = ps.id_usuario
         AND mt.id_entidadeatendimento = ps.id_entidade
    LEFT JOIN tb_turma AS tm
      ON tm.id_turma = mt.id_turma
    LEFT JOIN tb_matriculadiplomacao mtd
      ON mtd.id_matricula = mt.id_matricula
         AND mtd.bl_ativo = 1
    LEFT JOIN dbo.tb_matriculacolacao AS mc
      ON mc.id_matricula = mt.id_matricula
         --AND mc.id_matricula = mtd.id_matricula
         AND mc.bl_ativo = 1
    LEFT JOIN tb_matriculadiplomacao AS seg_via
      ON seg_via.id_diplomacaoorigem = mtd.id_matriculadiplomacao
-- GII-8420
    LEFT JOIN tb_vendaproduto AS vp
      ON vp.id_vendaproduto = mt.id_vendaproduto
  WHERE mt.bl_ativo = 1

GO

/****** Object:  View [dbo].[vw_disciplinasagendamento]    Script Date: 18/02/2014 17:58:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[vw_disciplinasagendamento]
AS
SELECT       dbo.vw_avaliacaoaluno.id_matricula,
						 dbo.tb_matricula.id_projetopedagogico,
						 dbo.tb_matricula.id_usuario, 
                         ag.id_avaliacaoagendamento, 
						 ag.id_tipodeavaliacao,
						 ag.bl_provaglobal,
						 dbo.vw_avaliacaoaluno.id_avaliacao, dbo.vw_avaliacaoaluno.id_disciplina, 
						 dbo.vw_avaliacaoaluno.st_tituloexibicaodisciplina as st_disciplina, 
                         dbo.vw_avaliacaoaluno.id_tipodisciplina, dbo.vw_avaliacaoaluno.st_nota as nu_notafinal, 
						 dbo.vw_avaliacaoaluno.id_evolucaodisciplina as bl_status,
						 dbo.vw_avaliacaoaluno.st_evolucaodisciplina as st_status,
						 dbo.vw_avaliacaoaluno.id_avaliacaoconjuntoreferencia,
						 (case when agr.id_avaliacaoconjuntoreferencia= dbo.vw_avaliacaoaluno.id_avaliacaoconjuntoreferencia then 1 else 0 end) as bl_agendado
						  --,dbo.vw_avaliacaoaluno.id_entidade
FROM            dbo.vw_avaliacaoaluno 
						--INNER JOIN vw_gradetipo as vg on vg.id_matricula=dbo.vw_avaliacaoaluno.id_matricula
						INNER JOIN dbo.tb_matricula ON dbo.vw_avaliacaoaluno.id_matricula = dbo.tb_matricula.id_matricula 
                         left join dbo.tb_avaliacaoagendamento ag ON dbo.tb_matricula.id_matricula = ag.id_matricula 
						 and ag.id_situacao not in (69,70) and ag.id_avaliacao=dbo.vw_avaliacaoaluno.id_avaliacao--and ag.bl_ativo=1
						 left JOIN dbo.tb_avalagendamentoref agr ON 
                         ag.id_avaliacaoagendamento = agr.id_avaliacaoagendamento and agr.id_avaliacaoconjuntoreferencia =dbo.vw_avaliacaoaluno.id_avaliacaoconjuntoreferencia
					
where dbo.vw_avaliacaoaluno.id_tipoavaliacao=4
--and dbo.vw_avaliacaoaluno.id_matricula=14862



GO



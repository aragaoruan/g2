CREATE VIEW [dbo].[vw_alunosvinculoperfil]
AS
  SELECT DISTINCT
    mt.id_matricula,
    mt.st_nomecompleto,
    mt.st_email,
    mt.id_projetopedagogico,
    mt.st_projetopedagogico,
    vw.id_usuario AS id_usuarioperfil,
    per.id_perfil,
    per.id_perfilpedagogico,
    mt.id_entidadeatendimento AS id_entidade,
    mt.st_entidadematriz,
    mt.dt_inicio,
    mt.st_evolucao,
    mt.st_situacao,
    mt.st_turma,
    et.id_esquemaconfiguracao
  FROM dbo.tb_perfil AS per
    JOIN tb_usuarioperfilentidadereferencia AS vw ON per.id_perfil = vw.id_perfil AND vw.bl_ativo = 1
    JOIN dbo.vw_matricula AS mt ON mt.id_projetopedagogico = vw.id_projetopedagogico
    JOIN tb_entidade as et ON et.id_entidade = mt.id_entidadeatendimento
                              AND mt.bl_ativo = 1
  WHERE  per.id_perfilpedagogico = 2
         AND per.bl_ativo=1
  UNION
  SELECT DISTINCT
    mt.id_matricula,
    mt.st_nomecompleto,
    mt.st_email,
    mt.id_projetopedagogico,
    mt.st_projetopedagogico,
    vw.id_usuario AS id_usuarioperfil,
    per.id_perfil,
    per.id_perfilpedagogico,
    mt.id_entidadeatendimento AS id_entidade,
    mt.st_entidadematriz,
    mt.dt_inicio,
    mt.st_evolucao,
    mt.st_situacao,
    mt.st_turma,
    et.id_esquemaconfiguracao
  FROM dbo.tb_perfil AS per
    JOIN tb_usuarioperfilentidadereferencia AS vw ON per.id_perfil = vw.id_perfil AND vw.bl_ativo = 1
    JOIN dbo.tb_alocacao al ON (vw.id_saladeaula=al.id_saladeaula )
    JOIN dbo.tb_matriculadisciplina AS md ON md.id_matriculadisciplina = al.id_matriculadisciplina
    JOiN dbo.vw_matricula AS mt ON mt.id_matricula = md.id_matricula AND mt.bl_ativo = 1
    JOIN tb_entidade as et ON et.id_entidade = mt.id_entidadeatendimento

  WHERE  per.id_perfilpedagogico = 1
         AND al.bl_ativo=1 AND per.bl_ativo=1

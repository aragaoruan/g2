
GO

/****** Object:  View [dbo].[vw_formapagamento]    Script Date: 28/11/2014 12:14:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[vw_formapagamento] as
SELECT fp.*, s.st_situacao
FROM tb_formapagamento as fp
JOIN tb_situacao as s ON s.id_situacao = fp.id_situacao

GO

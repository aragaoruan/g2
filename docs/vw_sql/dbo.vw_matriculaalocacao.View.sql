CREATE VIEW [dbo].[vw_matriculaalocacao] as
SELECT
  al.id_alocacao,
  md.id_matriculadisciplina,
	us.st_nomecompleto,
	mt.id_usuario,
	mt.id_entidadeatendimento,
	mt.id_matricula,
	mt.dt_ultimoacesso,
	sa.id_saladeaula,
	mt.dt_inicio,
	mt.dt_cadastro,
	sa.dt_encerramento,
	mt.dt_termino AS dt_terminomatricula,
	en.st_nomeentidade,
	dc.id_disciplina,
	dc.st_disciplina,
	md.id_situacaoagendamento
	--,ag.id_avaliacaoagendamento
FROM tb_matricula AS mt
JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
JOIN tb_disciplina AS dc ON dc.id_disciplina = md.id_disciplina
JOIN tb_alocacao AS al ON al.id_matriculadisciplina = md.id_matriculadisciplina AND al.bl_ativo = 1
JOIN tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula
JOIN tb_entidade AS en ON en.id_entidade = mt.id_entidadeatendimento
JOIN tb_usuario AS us ON mt.id_usuario = us.id_usuario
-- Busca o id do ultimo agendamento da matricula em tal disciplina
--LEFT JOIN vw_avaliacaoagendamento AS ag ON ag.id_matricula = mt.id_matricula AND ag.id_disciplina = md.id_disciplina
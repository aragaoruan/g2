
/****** Object:  View [dbo].[vw_alocacao]    Script Date: 10/08/2012 17:41:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_mensagemcobranca] as
select mc.id_mensagemcobranca, mc.bl_ativo, mc.nu_diasatraso, ts.id_textosistema, ts.st_textosistema, ts.id_entidade
from dbo.tb_mensagemcobranca as mc join tb_textosistema as ts on mc.id_textosistema = ts.id_textosistema and mc.id_entidade = ts.id_entidade
GO

CREATE VIEW [dbo].[vw_avaliacaoagendamentopos]
AS
  SELECT
    DISTINCT
    aa.id_avaliacaoagendamento,
    aa.id_matricula,
    us.id_usuario,
    us.st_nomecompleto,
    us.st_cpf,
    av.id_avaliacao,
    av.st_avaliacao,
    aa.dt_agendamento,
    aa.bl_automatico,
    aa.id_situacao,
    st.st_situacao,
    aa.id_avaliacaoaplicacao,
    aa.id_entidade,
    et.st_nomeentidade,
    ap.id_horarioaula,
    ha.st_horarioaula,
    mt.id_projetopedagogico,
    pp.st_projetopedagogico,
    pp.bl_possuiprova,
    aa.bl_provaglobal,
    (CASE
     WHEN pp.bl_possuiprova = 1
       THEN 'SIM'
     ELSE 'NÃO'
     END) AS st_possuiprova,
    aa.bl_ativo,
    ap.st_endereco,
    ap.dt_aplicacao,
    ap.id_aplicadorprova,
    ap.st_aplicadorprova,
    ap.st_cidade,
    aa.nu_presenca,
    aa.id_usuariocadastro,
    aa.id_usuariolancamento,
    usu_lancamento.st_nomecompleto AS st_usuariolancamento,
    (CASE
     WHEN mt.bl_documentacao = 1
       THEN 'OK'
     WHEN mt.bl_documentacao = 0
       THEN 'Pendente'
     ELSE 'Não Analisado'
     END) AS st_documentacao,
    (CASE
     WHEN (ap.dt_aplicacao IS NOT NULL
           AND ap.dt_aplicacao > Getdate()
           AND aa.bl_ativo = 1
           AND aa.nu_presenca IS NULL)
       THEN 'Aguardando aplicação de prova'
     WHEN (ap.dt_aplicacao IS NOT NULL
           AND ap.dt_aplicacao < Getdate()
           AND aa.bl_ativo = 1
           AND aa.id_situacao = 68
           AND aa.nu_presenca IS NULL)
       THEN 'Aguardando lançamento de frequência'
     WHEN (ap.dt_aplicacao IS NOT NULL
           AND aa.id_situacao = 68
           AND aa.bl_ativo = 0
           AND aa.nu_presenca = 1)
       THEN 'Presente'
     WHEN (ap.dt_aplicacao IS NOT NULL
           AND aa.id_situacao = 68
           AND aa.bl_ativo = 0
           AND aa.nu_presenca = 0)
       THEN 'Ausente'
     ELSE 'Indefinida'
     END) AS st_presenca,
    mt.id_evolucao,
    pe.st_email,
    pe.nu_ddi,
    pe.nu_ddd,
    pe.nu_telefone,
    pe.nu_ddialternativo,
    pe.nu_dddalternativo,
    pe.nu_telefonealternativo,
    ha.hr_inicio,
    ha.hr_fim,
    ap.sg_uf,
    ap.st_complemento,
    ap.nu_numero,
    ap.st_bairro,
    ap.st_telefone AS 'st_telefoneaplicador'
    (CASE
     WHEN (pp.st_codprovaintegracao IS NOT NULL
           AND pp.st_codprovaintegracao != '')
       THEN 'Sim'
     ELSE 'Não'
     END) AS st_temprovaintegrada,
    (
      CASE
      WHEN (
        pp.st_codprovaintegracao IS NOT NULL
        AND pp.st_codprovaintegracao != ''
      )
        THEN 1
      ELSE 0
      END
    ) AS bl_temprovaintegrada
  FROM tb_avaliacaoagendamento aa
    INNER JOIN tb_matricula AS mt ON mt.id_matricula = aa.id_matricula
    INNER JOIN tb_entidade AS ett ON ett.id_entidade = mt.id_entidadeatendimento
    INNER JOIN tb_usuario AS us ON us.id_usuario = mt.id_usuario
    INNER JOIN tb_avaliacao AS av ON av.id_avaliacao = aa.id_avaliacao
    INNER JOIN tb_avaliacaoconjuntorelacao AS acr ON acr.id_avaliacao = av.id_avaliacao
    INNER JOIN tb_avaliacaoconjunto AS ac ON ac.id_avaliacaoconjunto = acr.id_avaliacaoconjunto
    INNER JOIN tb_situacao AS st ON st.id_situacao = aa.id_situacao
    INNER JOIN tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
    INNER JOIN vw_pessoa AS pe ON pe.id_usuario = us.id_usuario AND pe.id_entidade = mt.id_entidadematricula
    INNER JOIN tb_esquemaconfiguracao AS ec ON ett.id_esquemaconfiguracao = ec.id_esquemaconfiguracao
    LEFT JOIN vw_avaliacaoaplicacao AS ap
      ON ap.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao AND ap.bl_ativo = 1 AND ap.id_entidade = aa.id_entidade
    LEFT JOIN tb_entidade AS et ON et.id_entidade = ap.id_entidade
    LEFT JOIN tb_horarioaula AS ha ON ha.id_horarioaula = ap.id_horarioaula
    OUTER APPLY (SELECT
                   st_nomecompleto
                 FROM tb_usuario
                 WHERE id_usuario = aa.id_usuariolancamento) AS usu_lancamento
  WHERE aa.id_situacao <> 70
        AND ac.id_tipoprova = 2
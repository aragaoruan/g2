CREATE VIEW [rel].[vw_evasaoaluno] AS
  WITH _config AS
       (SELECT 0    AS FALSE,
			         1    AS TRUE,
			         448  AS FUNCIONALIDADE_VELHO_PORTAL,
			         799  AS FUNCIONALIDADE_NOVO_PORTAL,
			         2038 AS CANCELAMENTO_ASSUNTOS_ACADEMICOS,
			         2039 AS TRANCAMENTO_ASSUNTOS_ACADEMICOS,
			         2361 AS CANCELAMENTO_MATRICULA,
			         2363 AS TRANCAMENTO_MATRICULA,
			         97   AS PENDENTE_OCORRENCIA,
			         98   AS EM_ANDAMENTO_OCORRENCIA,
			         12   AS EVOLUCAO_APROVADO,
			         6    AS EVOLUCAO_CURSANDO,
			         15   AS EVOLUCAO_CONCLUINTE,
			         21   AS EVOLUCAO_TRANCADO,
			         27   AS EVOLUCAO_CANCELADO,
			         40   AS EVOLUCAO_BLOQUEADO,
			         41   AS EVOLUCAO_ANULADO,
			         81   AS EVOLUCAO_FORMADO,
			         82   AS EVOLUCAO_DIPLOMADO,
			         65   AS SITUACAO_ISENTO,
			         10   AS VENDA_CONFIRMADA
       )

 SELECT ea.st_entidade,
        ea.st_nomecompleto,
        ea.st_cpf,
        ea.id_matricula,
        ea.st_evolucao,
        ea.dt_matricula,
        ea.id_projetopedagogico,
        ea.st_projetopedagogico,
        ea.nu_cargahoraria,
        ea.nu_cargahorariaintegralizada,
        ea.id_venda,
        ea.dt_renovacao,
        ea.dt_ultimaoferta,
        s.st_saladeaula,
        ea.id_logacesso,
        ea.dt_ultimoacesso,
        ea.bl_inadimplente,
        ea.st_inadimplente,
        ea.nu_qtdparcelaaberto,
        ea.nu_qtdparcelaaberto AS st_qtdparcelaaberto,
        ea.nu_valortotalaberto,
        ea.bl_ocorrenciacancelamento,
        ea.st_ocorrenciacancelamento,
        ea.id_ocorrenciacancelamento,
        ea.bl_ocorrenciatrancamento,
        ea.st_ocorrenciatrancamento,
        ea.id_ocorrenciatrancamento,
        ea.id_entidade,
        ea.id_usuario,
        ea.id_evolucao,
        ea.id_esquemaconfiguracao
   FROM (SELECT etm.st_nomeentidade AS st_entidade,
                us.st_nomecompleto,
                us.st_cpf,
                m.id_matricula,
                st.st_situacao,
                ev.st_evolucao,
                m.dt_cadastro AS dt_matricula,
                pp.st_projetopedagogico,
                pp.id_projetopedagogico,
                CASE
                     WHEN etmz.id_esquemaconfiguracao = _.TRUE THEN
						             (SELECT COALESCE(SUM(dc.nu_cargahoraria), 0)
						                FROM tb_matriculadisciplina md
                            JOIN tb_disciplina dc
                              ON dc.id_disciplina = md.id_disciplina
                            LEFT JOIN tb_alocacao al
                              ON al.id_matriculadisciplina = md.id_matriculadisciplina
                             AND al.bl_ativo = _.TRUE
                            LEFT JOIN dbo.vw_matriculanota mn
                              ON mn.id_matriculadisciplina = md.id_matriculadisciplina
                             AND mn.id_alocacao = al.id_alocacao
                           WHERE md.id_matricula = m.id_matricula
                             AND (   (al.id_alocacao IS NOT NULL AND md.id_evolucao = _.EVOLUCAO_APROVADO AND mn.bl_aprovado = _.TRUE) /* aprovado */
								                  OR (md.id_aproveitamento IS NOT NULL AND md.id_situacao = _.SITUACAO_ISENTO) /* isento */
								                 )
						             )

						             +

						             (SELECT COALESCE(SUM(atividade.nu_horasconvalidada), 0)
							              FROM tb_atividadecomplementar atividade
							             WHERE atividade.id_matricula = m.id_matricula) /* atividades complementares */
					           ELSE 0
				         END AS nu_cargahorariaintegralizada,
				        (SELECT SUM(dis.nu_cargahoraria)
					         FROM tb_matriculadisciplina md
					         JOIN tb_disciplina dis
					           ON dis.id_disciplina = md.id_disciplina
				          WHERE md.id_matricula = m.id_matricula) AS nu_cargahoraria,

				        COALESCE(CASE
							                WHEN m.id_matriculaorigem IS NULL THEN vprod.id_venda
							                ELSE vad.id_venda
						              END, v.id_venda) AS id_venda,
			          CASE
			               WHEN venreno.bl_renovacao = _.TRUE THEN venreno.dt_confirmacao
					           ELSE NULL
				         END AS dt_renovacao,
			          m.dt_ultimaoferta,
                (SELECT MAX(l.id_logacesso)
                   FROM dbo.tb_logacesso l
                  WHERE (    l.id_funcionalidade = _.FUNCIONALIDADE_VELHO_PORTAL
                         OR (l.id_funcionalidade = _.FUNCIONALIDADE_NOVO_PORTAL
                    AND l.id_matricula  = m.id_matricula))
                    AND l.id_entidade = m.id_entidadematricula
                    AND l.id_usuario  = us.id_usuario) as id_logacesso,
                m.dt_ultimoacesso,
                CASE
                     WHEN COUNT(vrf.nu_parcela) > 0 THEN 1
                     ELSE 0
                 END AS bl_inadimplente,
                CASE
                     WHEN COUNT(vrf.nu_parcela) > 0 THEN 'SIM'
                     ELSE 'NÃO'
                 END AS st_inadimplente,
                COUNT(vrf.nu_parcela) AS nu_qtdparcelaaberto,
                SUM(vrf.nu_valor) AS nu_valortotalaberto,
                CASE
                     WHEN COUNT(oc.id_ocorrencia) > 0 THEN 1
                     ELSE 0
                 END AS bl_ocorrenciacancelamento,
                CASE
                     WHEN COUNT(oc.id_ocorrencia) > 0 THEN 'SIM'
                     ELSE 'NÃO'
                 END AS st_ocorrenciacancelamento,
                COALESCE(CAST(MAX(oc.id_ocorrencia) AS VARCHAR), '-') AS id_ocorrenciacancelamento,
                CASE
                     WHEN COUNT(ot.id_ocorrencia) > 0 THEN 1
                     ELSE 0
                 END AS bl_ocorrenciatrancamento,
                CASE
                     WHEN COUNT(ot.id_ocorrencia) > 0 THEN 'SIM'
                     ELSE 'NÃO'
                 END AS st_ocorrenciatrancamento,
                COALESCE(CAST(MAX(ot.id_ocorrencia) AS VARCHAR), '-') AS id_ocorrenciatrancamento,
                m.id_entidadematricula AS id_entidade,
                ev.id_evolucao,
                etmz.id_esquemaconfiguracao,
                m.id_usuario
		       FROM tb_matricula m
		      CROSS APPLY _config _
		       JOIN tb_projetopedagogico pp
		         ON m.id_projetopedagogico = pp.id_projetopedagogico
		       JOIN tb_areaprojetopedagogico ap
             ON ap.id_projetopedagogico = pp.id_projetopedagogico
           JOIN tb_entidade etmz
             ON etmz.id_entidade = m.id_entidadematriz
           JOIN tb_entidade etm
             ON etm.id_entidade = m.id_entidadematricula
           JOIN tb_entidade eta
             ON eta.id_entidade = m.id_entidadeatendimento
           JOIN tb_situacao st
             ON st.id_situacao = m.id_situacao
           JOIN tb_evolucao ev
             ON ev.id_evolucao = m.id_evolucao
			AND ev.id_evolucao IN (_.EVOLUCAO_CURSANDO,
								   _.EVOLUCAO_CONCLUINTE,
								   _.EVOLUCAO_TRANCADO,
								   _.EVOLUCAO_CANCELADO,
								   _.EVOLUCAO_BLOQUEADO,
								   _.EVOLUCAO_ANULADO,
								   _.EVOLUCAO_FORMADO,
								   _.EVOLUCAO_DIPLOMADO)
           JOIN tb_usuario us
             ON us.id_usuario = m.id_usuario
           LEFT JOIN tb_contratomatricula cm
             ON cm.id_matricula = m.id_matricula
            AND cm.bl_ativo = _.TRUE
           LEFT JOIN dbo.tb_contrato AS ct
             ON ct.id_contrato = cm.id_contrato
            AND ct.bl_ativo = _.TRUE
           LEFT JOIN dbo.tb_venda AS v
             ON ct.id_venda = v.id_venda
          OUTER APPLY (SELECT TOP 1 tb_vendaproduto.*
                         FROM dbo.tb_vendaproduto
                         JOIN tb_produtoprojetopedagogico ppd
                           ON ppd.id_produto = tb_vendaproduto.id_produto
                         JOIN tb_venda vend
                           ON vend.id_venda = tb_vendaproduto.id_venda
                          AND vend.id_evolucao = _.VENDA_CONFIRMADA
                        WHERE tb_vendaproduto.id_matricula = m.id_matricula
                          AND tb_vendaproduto.bl_ativo = _.TRUE
                        ORDER BY tb_vendaproduto.id_vendaproduto DESC) vprod
           LEFT JOIN tb_vendaaditivo vad
             ON vad.id_matricula = m.id_matricula
           LEFT JOIN dbo.vw_resumofinanceiro vrf
             ON vrf.id_venda = COALESCE(CASE
                                             WHEN m.id_matriculaorigem IS NULL THEN vprod.id_venda
                                             ELSE vad.id_venda
                                         END, v.id_venda)
            AND DATEDIFF(DAY, vrf.dt_vencimento, GETDATE()) > 2
            AND vrf.bl_quitado = _.FALSE
            AND vrf.bl_ativo   = _.TRUE

           LEFT JOIN dbo.tb_ocorrencia o
             ON o.id_matricula = m.id_matricula
            AND o.id_entidade  = m.id_entidadematricula
            AND o.id_situacao  IN (_.PENDENTE_OCORRENCIA, _.EM_ANDAMENTO_OCORRENCIA)
            AND o.id_assuntoco IN (_.CANCELAMENTO_ASSUNTOS_ACADEMICOS,
                                   _.TRANCAMENTO_ASSUNTOS_ACADEMICOS,
                                   _.CANCELAMENTO_MATRICULA,
                                   _.TRANCAMENTO_MATRICULA)
           LEFT JOIN dbo.tb_ocorrencia oc
             ON oc.id_ocorrencia = o.id_ocorrencia
            AND oc.id_assuntoco IN (_.CANCELAMENTO_ASSUNTOS_ACADEMICOS,
                                    _.CANCELAMENTO_MATRICULA)
           LEFT JOIN dbo.tb_ocorrencia ot
             ON ot.id_ocorrencia = o.id_ocorrencia
            AND ot.id_assuntoco IN (_.TRANCAMENTO_ASSUNTOS_ACADEMICOS,
                                     _.TRANCAMENTO_MATRICULA)
		   LEFT JOIN dbo.tb_venda venreno
		     ON venreno.id_venda = (COALESCE(CASE WHEN m.id_matriculaorigem IS NULL THEN vprod.id_venda
							                      ELSE vad.id_venda
											  END, v.id_venda))
          WHERE m.bl_ativo = _.TRUE
          GROUP BY etm.st_nomeentidade,
                   us.st_nomecompleto,
                   us.st_cpf,
                   m.id_matricula,
                   st.st_situacao,
                   ev.st_evolucao,
                   m.dt_cadastro,
                   pp.st_projetopedagogico,
                   pp.id_projetopedagogico,
                   etmz.id_esquemaconfiguracao,
                   m.id_entidadematricula,
                   m.id_matriculaorigem,
                   vprod.id_venda,
                   vad.id_venda,
                   v.id_venda,
                   venreno.bl_renovacao,
                   venreno.dt_confirmacao,
                   m.dt_ultimaoferta,
                   m.dt_ultimoacesso,
                   m.id_usuario,
                   us.id_usuario,
                   ev.id_evolucao,
                   _.FALSE,
                   _.TRUE,
                   _.FUNCIONALIDADE_VELHO_PORTAL,
                   _.FUNCIONALIDADE_NOVO_PORTAL,
                   _.CANCELAMENTO_ASSUNTOS_ACADEMICOS,
                   _.TRANCAMENTO_ASSUNTOS_ACADEMICOS,
                   _.CANCELAMENTO_MATRICULA,
                   _.TRANCAMENTO_MATRICULA,
                   _.PENDENTE_OCORRENCIA,
                   _.EM_ANDAMENTO_OCORRENCIA,
                   _.EVOLUCAO_APROVADO,
                   _.SITUACAO_ISENTO,
                   _.VENDA_CONFIRMADA
	      ) ea
   LEFT JOIN dbo.tb_logacesso la
     ON la.id_logacesso = ea.id_logacesso
   LEFT JOIN dbo.tb_saladeaulaentidade sae
     ON sae.id_entidade   = la.id_entidade
    AND sae.id_saladeaula = la.id_saladeaula
   LEFT JOIN dbo.tb_saladeaula s
     ON s.id_saladeaula = sae.id_saladeaula

GO 
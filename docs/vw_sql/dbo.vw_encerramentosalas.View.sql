
GO

/****** Object:  View [dbo].[vw_encerramentosalas]    Script Date: 23/03/2015 09:54:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_encerramentosalas]
AS
SELECT DISTINCT
  es.id_saladeaula,
  es.id_encerramentosala,
  upr.id_usuario as id_usuarioprofessor,
  dt_encerramentoprofessor,
  CONVERT(VARCHAR, dt_encerramentoprofessor, 103) AS st_encerramentoprofessor,
  id_usuariocoordenador,
  dt_encerramentocoordenador,
  CONVERT(VARCHAR, dt_encerramentocoordenador, 103) AS st_encerramentocoordenador,
  id_usuariopedagogico,
  dt_encerramentopedagogico,
  CONVERT(VARCHAR, dt_encerramentopedagogico, 103) AS st_encerramentopedagogico,
  id_usuariofinanceiro,
  dt_encerramentofinanceiro,
  CONVERT(VARCHAR, dt_encerramentofinanceiro, 103) AS st_encerramentofinanceiro,
  sa.id_entidade,
  sa.id_disciplina,
  di.st_disciplina,
  di.nu_cargahoraria,
  st_saladeaula,
  sa.id_tipodisciplina,
  td.st_tipodisciplina,
  NULL AS id_areaconhecimento,
  NULL AS st_areaconhecimento,
  al.nu_alunos,
  --tcc.st_nomecompleto AS st_aluno,
  (case WHEN tcc.st_nomecompleto IS NOT NULL
		THEN tcc.st_nomecompleto
		ELSE tcc.st_nomecompletomatricula
		END) AS st_aluno,
  tcc.st_tituloavaliacao,
  tcc.id_matricula,
  tcc.st_upload,
  --CAST(tcc.st_nota AS NUMERIC(3,0)) AS st_nota,
   (CASE WHEN (tcc.st_nota IS NULL AND tcc.st_nomecompleto IS  NULL)
		  THEN 'Desalocado'
		  ELSE CONVERT(varchar(10),(CAST(ISNULL(tcc.st_nota, 0) AS NUMERIC(3,0))))
		  END) AS st_nota,
  --tcc.st_projetopedagogico,
  (case WHEN tcc.st_projetopedagogico IS NOT NULL
		THEN tcc.st_projetopedagogico
		ELSE tcc.st_projetopedagogicomatricula
		END) AS st_projetopedagogico,
  us.st_nomecompleto AS st_professor,
  tcc.id_usuario AS id_aluno,
  sa.id_categoriasala,
  es.dt_recusa,
  es.id_usuariorecusa,
  es.st_motivorecusa,
  vw_valor.nu_valor,
  es.nu_valorpago
FROM dbo.tb_encerramentosala AS es
JOIN vw_saladeaula AS sa
  ON sa.id_saladeaula = es.id_saladeaula
JOIN dbo.tb_tipodisciplina AS td
  ON sa.id_tipodisciplina = td.id_tipodisciplina
INNER JOIN dbo.tb_disciplina AS di
  ON di.id_disciplina = sa.id_disciplina
OUTER APPLY (SELECT
				TOP 1
				  aa.st_nomecompleto,
				  aa.st_tituloavaliacao,
				  md.id_matricula,
				  up.st_upload,
				  mat.id_usuario,
				  aa.st_nota,
				  aa.st_projetopedagogico,
				  us.st_nomecompleto as st_nomecompletomatricula,
				  pp.st_projetopedagogico as st_projetopedagogicomatricula
				FROM dbo.tb_encerramentoalocacao AS ee
				JOIN dbo.tb_alocacao AS ali
				  ON ali.id_alocacao = ee.id_alocacao
				JOIN dbo.tb_matriculadisciplina AS md
				  ON md.id_matriculadisciplina = ali.id_matriculadisciplina
				JOIN dbo.tb_matricula AS mat
				  ON mat.id_matricula = md.id_matricula
				JOIN tb_usuario AS us
					ON us.id_usuario = mat.id_usuario
				JOIN tb_projetopedagogico AS pp
					ON pp.id_projetopedagogico = mat.id_projetopedagogico
				LEFT JOIN dbo.vw_avaliacaoaluno AS aa
				  ON aa.id_saladeaula = sa.id_saladeaula
				  AND md.id_matricula = aa.id_matricula
				  AND aa.id_tipoavaliacao = 6
				LEFT JOIN dbo.tb_upload AS up
				  ON aa.id_upload = up.id_upload
				WHERE es.id_encerramentosala = ee.id_encerramentosala) AS tcc
LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS upr
  ON upr.id_saladeaula = sa.id_saladeaula
  AND upr.bl_titular = 1 AND upr.bl_ativo = 1
LEFT JOIN dbo.tb_usuario AS us
  ON us.id_usuario = upr.id_usuario
--LEFT JOIN dbo.tb_areadisciplina AS ad ON ad.id_disciplina = sa.id_disciplina
--LEFT JOIN dbo.tb_areaconhecimento AS ac ON ac.id_areaconhecimento = ad.id_areaconhecimento AND AC.id_tipoareaconhecimento = 1
OUTER APPLY (SELECT
  COUNT(DISTINCT id_alocacao) AS nu_alunos
FROM dbo.tb_encerramentoalocacao
WHERE id_encerramentosala = es.id_encerramentosala) AS al
-- totalizacao dos valores
OUTER APPLY (SELECT SUM(nu_valor) AS nu_valor
	FROM dbo.vw_encerramentovalor v
	WHERE v.id_encerramentosala = es.id_encerramentosala
	GROUP BY id_encerramentosala) AS vw_valor
GO



USE [G2_UNY]
GO

/****** Object:  View [dbo].[vw_consultaproduto]    Script Date: 16/12/2014 11:22:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vw_consultaproduto] AS
SELECT
	pd.id_produto,
	pd.st_produto,
	pd.id_tipoproduto,
	pd.id_situacao,
	pd.id_usuariocadastro,
	pd.id_entidade,
	pd.bl_ativo,
	pd.bl_todasformas,
	pd.bl_todascampanhas,
	pd.nu_gratuito,
	pd.id_produtoimagempadrao,
	pd.bl_unico,
	pd.st_descricao,
	pd.st_observacoes,
	pd.st_tipoproduto,
	pd.id_produtovalor,
	pd.nu_valor,
	pd.nu_valormensal,
	pd.nu_valorpromocional,
	pd.id_tipoprodutovalor,
	pd.st_tipoprodutovalor,
	pd.st_situacao,
	pd.id_planopagamento,
	pd.nu_parcelas,
	pd.nu_valorentrada,
	pd.nu_valorparcela,
	pd.st_nomeentidade,
	pd.st_subtitulo,
	pd.st_slug,
	pd.bl_destaque,
	pd.dt_cadastro,
	pd.dt_atualizado,
	pd.dt_iniciopontosprom,
	pd.dt_fimpontosprom,
	pd.nu_pontos,
	pd.nu_pontospromocional,
	pd.nu_estoque,
	cp.id_categoria,
	cpd.id_concurso,
	con.st_concurso,
	tag.id_tag,
	pu.sg_uf,
	pc.id_carreira,
	ev.id_evolucao,
	ev.st_evolucao,
	pd.bl_mostrarpreco,
	ppp.id_projetopedagogico,
	ppp.nu_cargahoraria,
	tm.dt_inicio,
	tn.st_turno,
	ppp.st_disciplina,
	null AS st_diassemana
FROM
	dbo.vw_produto AS pd
LEFT JOIN dbo.tb_categoriaproduto AS cp ON pd.id_produto = cp.id_produto
LEFT JOIN dbo.tb_concursoproduto AS cpd ON cpd.id_produto = pd.id_produto
LEFT JOIN dbo.tb_tagproduto AS tag ON tag.id_produto = tag.id_produto
LEFT JOIN dbo.tb_produtouf AS pu ON pu.id_produto = pd.id_produto
LEFT JOIN dbo.tb_produtocarreira AS pc ON pc.id_produto = pd.id_produto
LEFT JOIN dbo.tb_concurso AS con ON cpd.id_concurso = con.id_concurso
LEFT JOIN tb_produtoprojetopedagogico AS ppp ON ppp.id_produto = pd.id_produto
LEFT JOIN tb_turma AS tm ON ppp.id_turma = tm.id_turma
LEFT JOIN tb_turmaturno AS tt ON tt.id_turma = tm.id_turma
LEFT JOIN dbo.tb_turno AS tn ON tn.id_turno = tt.id_turno
LEFT JOIN tb_evolucao AS ev ON ev.id_evolucao = tm.id_evolucao
WHERE pd.id_situacao = 45

GO



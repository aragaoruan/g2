CREATE VIEW [dbo].[vw_gridcertificado] AS
SELECT DISTINCT
  mt.id_matricula,
  mt.id_projetopedagogico,
  pp.st_tituloexibicao,
  CAST(mtd.id_disciplina AS VARCHAR(200)) AS id_disciplina,
  dc.st_tituloexibicao AS st_disciplina,
  mtd.nu_aprovafinal AS nu_aprovafinal,
  pp.nu_notamaxima,
  mtd.dt_conclusao AS dt_conclusaodisciplina,
  dc.nu_cargahoraria AS nu_cargahoraria,
  mtd.id_evolucao,
  ev.st_evolucao,
  mtd.id_situacao,
  CASE
  WHEN CAST(mtd.nu_aprovafinal AS INT) < 70 THEN ''
  WHEN CAST(mtd.nu_aprovafinal AS INT) < 80 THEN 'Bom'
  WHEN CAST(mtd.nu_aprovafinal AS INT) < 90 THEN 'Ótimo'
  WHEN CAST(mtd.nu_aprovafinal AS INT) <= 100 THEN 'Excelente'
  ELSE ''
  END AS st_conceito,

  op.st_nomecompleto AS st_titularcertificacao,
  
  -- GII-6755
  --st_titularcertificacao = (CASE
  --  -- Se for TCC, buscar o titular cadastrado em Sala de Aula
  --  WHEN dc.id_tipodisciplina = 2 THEN op_tcc.st_nomecompleto
  --  -- Caso contrário, buscar o titular cadastrado em Titular de Certificação
  --  ELSE op.st_nomecompleto
  --END),

  op.st_titulacao,
  mt.id_entidadematricula,
  mt.dt_concluinte,
  dc.id_tipodisciplina,
  --DATEADD(DAY,ISNULL(sa.nu_diasextensao,0), sa.dt_encerramento) as dt_encerramentoextensao
  NULL AS dt_encerramentoextensao

FROM tb_matricula AS mt
JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_matriculadisciplina AS mtd ON mtd.id_matricula = mt.id_matricula AND mtd.bl_obrigatorio = 1
JOIN tb_matricula AS mt2 ON mt2.id_matricula = mtd.id_matriculaoriginal
JOIN tb_areaprojetopedagogico as ar on ar.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_projetopedagogico AS ppcompl ON ppcompl.id_projetopedagogico = mt2.id_projetopedagogico

JOIN tb_alocacao AS al ON al.id_matriculadisciplina = mtd.id_matriculadisciplina and al.bl_ativo = 1
JOIN tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula

LEFT JOIN tb_aproveitamentomatriculadisciplina AS apm ON apm.id_matricula = mtd.id_matricula AND apm.id_disciplina = mtd.id_disciplina
LEFT JOIN tb_aproveitamentodisciplina AS apd ON apd.id_aproveitamentodisciplina = apm.id_aproveitamentodisciplina
JOIN tb_disciplina AS dc ON dc.id_disciplina = mtd.id_disciplina
JOIN tb_entidade AS ent ON ent.id_entidade = mt.id_entidadematriz
JOIN tb_evolucao AS ev ON ev.id_evolucao = mtd.id_evolucao

OUTER APPLY (
  SELECT
    us.st_nomecompleto,
    tit.st_titulacao
  FROM tb_usuarioperfilentidadereferencia AS uper
  JOIN tb_perfil AS pf ON pf.id_entidade = mt.id_entidadematricula AND pf.id_perfilpedagogico = 9 AND uper.id_perfil = pf.id_perfil
  LEFT JOIN tb_usuario AS us ON us.id_usuario = uper.id_usuario
  LEFT JOIN tb_titulacao tit ON uper.id_titulacao = tit.id_titulacao
  WHERE
    uper.bl_titular = 1
    AND uper.bl_ativo = 1
    AND uper.id_usuario IS NOT NULL
    AND uper.id_projetopedagogico = mt2.id_projetopedagogico
    AND uper.id_disciplina = mtd.id_disciplina
    AND (
      ar.id_areaconhecimento = uper.id_areaconhecimento OR (
        mtd.id_matricula != mtd.id_matriculaoriginal
        AND ppcompl.bl_disciplinacomplementar = 1
      )
    )
    AND (
      (mt.dt_concluinte BETWEEN uper.dt_inicio AND uper.dt_fim)
      OR (mt.dt_concluinte >= uper.dt_inicio AND uper.dt_fim IS NULL)
    )
) AS op

OUTER APPLY (
  SELECT
    us.st_nomecompleto,
    tit.st_titulacao
  FROM tb_usuarioperfilentidadereferencia AS uper
  LEFT JOIN tb_usuario AS us ON us.id_usuario = uper.id_usuario
  LEFT JOIN tb_titulacao tit ON uper.id_titulacao = tit.id_titulacao
  WHERE
    uper.bl_titular = 1
    AND uper.bl_ativo = 1
    AND uper.id_usuario IS NOT NULL
    AND uper.id_saladeaula = sa.id_saladeaula
) AS op_tcc

--WHERE
--sa.dt_encerramento IS NOT NULL
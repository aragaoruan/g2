
GO

/****** Object:  View [dbo].[vw_disciplinaintegracaomoodle]    Script Date: 27/06/2014 11:30:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[vw_disciplinaintegracaomoodle] AS
	SELECT  de.id_disciplinaintegracao
			,de.id_disciplina
			,de.id_sistema
			,de.id_usuariocadastro
			,de.st_codsistema
			,de.dt_cadastro
			,de.id_entidade
			,de.bl_ativo 
			,e.st_nomeentidade 
			,s.st_sistema
			from tb_disciplinaintegracao de
			left join tb_entidade e on e.id_entidade = de.id_entidade
			INNER JOIN tb_sistema s on s.id_sistema = de.id_sistema
			--where id_sistema = 6
			




GO



SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_areaprojetoprodutoholding]
AS
SELECT DISTINCT
	ac.id_areaconhecimento,
	ac.st_areaconhecimento,
	ac.st_tituloexibicao AS st_tituloexibicaoarea,
	pp.id_projetopedagogico,
	pp.st_projetopedagogico,
	pp.st_tituloexibicao AS st_tituloexibicaoprojeto,
	pp.nu_cargahoraria,
	plp.nu_parcelas,
	plp.nu_valorparcela,
	plp.nu_valorentrada,
	pe.id_entidade,
	et.st_nomeentidade,
	ed.sg_uf,
	ppp.id_produto,
	pp.id_entidadecadastro AS id_entidadematriz,
	pp.st_descricao,
	pp.st_objetivo,
	pp.st_estruturacurricular,
	pp.st_conteudoprogramatico,
	pp.st_metodologiaavaliacao,
	pp.st_certificacao,
	pp.st_valorapresentacao,
	pp.st_perfilprojeto,
	pp.st_coordenacao,
	pp.st_horario,
	pp.st_publicoalvo,
	pp.st_mercadotrabalho
FROM dbo.tb_projetopedagogico AS pp
JOIN dbo.tb_areaprojetopedagogico AS ap
	ON ap.id_projetopedagogico = pp.id_projetopedagogico
JOIN dbo.tb_areaconhecimento AS ac
	ON ac.id_areaconhecimento = ap.id_areaconhecimento
--AND ac.id_tipoareaconhecimento = 1
JOIN dbo.tb_projetoentidade AS pe
	ON pe.id_projetopedagogico = pp.id_projetopedagogico
	AND pe.bl_ativo = 1
JOIN tb_entidade AS et
	ON et.id_entidade = pe.id_entidade
LEFT JOIN dbo.tb_entidadeendereco AS ee
	ON ee.id_entidade = et.id_entidade
	AND ee.bl_padrao = 1
LEFT JOIN dbo.tb_endereco AS ed
	ON ed.id_endereco = ee.id_endereco
JOIN dbo.vw_produtoprojetotipovalor AS ppp
	ON ppp.id_projetopedagogico = pp.id_projetopedagogico
	AND ppp.id_entidade = pe.id_entidade
	AND ppp.bl_ativo = 1
	AND ppp.id_situacao = 45
LEFT JOIN dbo.vw_projetoplanomatriz AS plp
	ON plp.id_projetopedagogico = pp.id_projetopedagogico
	AND plp.bl_padrao = 1

GO
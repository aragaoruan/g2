CREATE VIEW [dbo].[vw_assuntonucleupessoa]
AS

SELECT
	tb_entidade.st_nomeentidade,
	ap.st_assuntoco AS st_assuntopai,
	aco.id_assuntocopai AS id_assuntopai,
	aco.st_assuntoco AS st_subassunto,
	aco.id_assuntoco AS id_subassunto,
	aco.id_assuntoco AS id_assunto,
	ae.id_entidade AS id_entidade,
	aco.id_entidadecadastro AS id_entidadecadastro,
	ps.id_usuario,
	ps.st_nomecompleto,
	nc.st_nucleoco,
	fn.st_funcao,
	np.id_nucleopessoaco,
	nc.id_nucleoco
FROM dbo.tb_assuntoco AS aco
JOIN dbo.tb_assuntoco AS ap
	ON ap.id_assuntoco = aco.id_assuntocopai
JOIN dbo.tb_assuntoentidadeco AS ae
	ON ae.id_assuntoco = aco.id_assuntoco
JOIN tb_entidade
	ON tb_entidade.id_entidade = ae.id_entidade
JOIN dbo.tb_nucleoassuntoco AS na
	ON na.id_assuntoco = ap.id_assuntoco
JOIN tb_nucleoco AS nc
	ON nc.id_nucleoco = na.id_nucleoco AND nc.id_entidade = ae.id_entidade
JOIN tb_funcao AS fn
	ON fn.id_tipofuncao = 3
JOIN vw_pessoa AS ps
	ON ps.id_entidade = nc.id_entidade
LEFT JOIN tb_nucleopessoaco AS np
	ON np.id_assuntoco = na.id_assuntoco AND np.id_nucleoco = na.id_nucleoco AND np.id_funcao = fn.id_funcao AND ps.id_usuario = np.id_usuario
--WHERE ps.id_usuario = 1 AND na.id_nucleoco = 6 

UNION 

SELECT
	tb_entidade.st_nomeentidade,
	aco.st_assuntoco AS st_assuntopai,
	aco.id_assuntoco AS id_assuntopai,
	NULL AS st_subassunto,
	NULL AS id_subassunto,
	aco.id_assuntoco AS id_assunto,
	ae.id_entidade AS id_entidade,
	aco.id_entidadecadastro AS id_entidadecadastro,
	ps.id_usuario,
	ps.st_nomecompleto,
	nc.st_nucleoco,
	fn.st_funcao,
	np.id_nucleopessoaco,
	nc.id_nucleoco
FROM dbo.tb_assuntoco AS aco
LEFT JOIN dbo.tb_assuntoentidadeco AS ae
	ON ae.id_assuntoco = aco.id_assuntoco
JOIN tb_entidade
	ON tb_entidade.id_entidade = ae.id_entidade
JOIN dbo.tb_nucleoassuntoco AS na
	ON na.id_assuntoco = aco.id_assuntoco
JOIN tb_nucleoco AS nc
	ON nc.id_nucleoco = na.id_nucleoco AND nc.id_entidade = ae.id_entidade
JOIN tb_funcao AS fn
	ON fn.id_tipofuncao = 3
JOIN vw_pessoa AS ps
	ON ps.id_entidade = nc.id_entidade
LEFT JOIN tb_nucleopessoaco AS np
	ON np.id_assuntoco = na.id_assuntoco AND np.id_nucleoco = na.id_nucleoco AND np.id_funcao = fn.id_funcao AND ps.id_usuario = np.id_usuario
WHERE aco.id_assuntocopai IS NULL
--AND ps.id_usuario = 1 AND na.id_nucleoco = 6
--ORDER BY fn.st_funcao, st_assuntopai, st_subassunto
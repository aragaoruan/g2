
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 CREATE VIEW [dbo].[vw_vendaocorrencia] AS

SELECT DISTINCT
	v.id_venda,
    case when count(o.id_ocorrencia) > 0 then cast(1 as bit) else cast(0 as bit) end as bl_temocorrencia
FROM 
	tb_ocorrencia AS o
	INNER JOIN tb_assuntoco         AS a  ON o.id_assuntoco  = a.id_assuntoco
	INNER JOIN tb_nucleoassuntoco   AS na ON na.id_assuntoco = a.id_assuntoco
	INNER JOIN tb_nucleoco          AS n  ON n.id_nucleoco   = na.id_nucleoco AND n.id_nucleofinalidade = 2
	INNER JOIN tb_matricula         AS m  ON m.id_matricula  = o.id_matricula
--  INNER JOIN tb_contratomatricula AS cm ON cm.id_matricula = m.id_matricula
--  INNER JOIN tb_contrato          AS c  ON cm.id_contrato  = c.id_contrato
	INNER JOIN tb_vendaproduto      AS vp ON vp.id_matricula = m.id_matricula
	INNER JOIN tb_venda             AS v  ON v.id_venda      = vp.id_venda
GROUP BY o.id_ocorrencia, v.id_venda

GO

GO

/****** Object:  View [dbo].[vw_vendalancamento]    Script Date: 11/29/2012 09:24:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 CREATE VIEW [dbo].[vw_vendalancamentoquitados] as

select  vd.id_venda, 
		vd.id_usuario, 
		us.st_nomecompleto ,
		vd.id_evolucao, 
		vd.nu_parcelas, 
		vd.nu_valorliquido, 
		vd.id_situacao,

        sum(lc.nu_quitado) as nu_quitado,
        
        (select min(dt_quitado)
			from tb_lancamento l join tb_lancamentovenda lav 
				on (l.id_lancamento = lav.id_lancamento and lav.id_venda = vd.id_venda) 
				where l.bl_quitado = lc.bl_quitado
				and l.bl_ativo = lc.bl_ativo
				and l.dt_quitado is not null) as dt_primeiroquitado,
        
        (select max(dt_quitado) 
			from tb_lancamento l join tb_lancamentovenda lav 
				on (l.id_lancamento = lav.id_lancamento and lav.id_venda = vd.id_venda) 
				where l.bl_quitado = lc.bl_quitado
				and l.bl_ativo = lc.bl_ativo
				and l.dt_quitado is not null) as dt_ultimoquitado,
				
        (select DATENAME(MONTH,min(dt_quitado))+'/'+ CAST(DATEPART(YEAR,min(dt_quitado)) AS VARCHAR) 
			from tb_lancamento l join tb_lancamentovenda lav 
				on (l.id_lancamento = lav.id_lancamento and lav.id_venda = vd.id_venda) 
				where l.bl_quitado = lc.bl_quitado
				and l.bl_ativo = lc.bl_ativo
				and l.dt_quitado is not null) as st_mesanoprimeiroquitado,
        
        (select DATENAME(MONTH,max(dt_quitado))+'/'+ CAST(DATEPART(YEAR,max(dt_quitado)) AS VARCHAR)  
			from tb_lancamento l join tb_lancamentovenda lav 
				on (l.id_lancamento = lav.id_lancamento and lav.id_venda = vd.id_venda) 
				where l.bl_quitado	= lc.bl_quitado
				and l.bl_ativo		= lc.bl_ativo
				and l.dt_quitado	is not null) as st_mesanoultimoquitado
				
from tb_venda as vd

JOIN dbo.tb_usuario AS us ON us.id_usuario = vd.id_usuario

JOIN tb_lancamentovenda as lv ON lv.id_venda = vd.id_venda

JOIN tb_lancamento as lc ON lc.id_lancamento = lv.id_lancamento

WHERE lc.bl_quitado = 1 and lc.bl_ativo = 1 and lc.dt_quitado is not null
group by vd.id_venda, 
		vd.id_usuario, 
		us.st_nomecompleto ,
		vd.id_evolucao, 
		vd.nu_parcelas, 
		vd.nu_valorliquido, 
		vd.id_situacao, lc.bl_quitado, lc.bl_ativo
GO


 
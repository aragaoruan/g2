CREATE VIEW [dbo].[vw_municipioaplicacao] AS

SELECT  DISTINCT
  mu.id_municipio,
  mu.st_nomemunicipio,
  aap.id_entidade,
  mu.sg_uf

FROM
  vw_avaliacaoaplicacao AS aap
  JOIN tb_municipio AS mu ON aap.id_municipio = mu.id_municipio
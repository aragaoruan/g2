
GO
/****** Object:  View [dbo].[vw_projetopedagogico]    Script Date: 10/08/2012 17:42:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE VIEW [dbo].[vw_projetopedagogico] AS
select pp.id_projetopedagogico ,
        pp.st_descricao ,
        pp.nu_idademinimainiciar ,
        pp.bl_ativo ,
        pp.nu_tempominimoconcluir ,
        pp.nu_idademinimaconcluir ,
        pp.nu_tempomaximoconcluir ,
        pp.nu_tempopreviconcluir ,
        pp.st_projetopedagogico ,
        pp.st_tituloexibicao ,
        pp.bl_autoalocaraluno ,
        pp.id_situacao ,
        pp.id_trilha ,
        pp.id_projetopedagogicoorigem ,
        pp.st_nomeversao ,
        pp.id_tipoextracurricular ,
        pp.nu_notamaxima ,
        pp.nu_percentualaprovacao ,
        pp.nu_valorprovafinal ,
        pp.id_entidadecadastro ,
        pp.bl_provafinal ,
        pp.bl_salasemprovafinal ,
        pp.st_objetivo ,
        pp.st_estruturacurricular ,
        pp.st_metodologiaavaliacao ,
        pp.st_certificacao ,
        pp.st_valorapresentacao ,
        pp.nu_cargahoraria ,
        pp.dt_cadastro ,
        pp.id_usuariocadastro ,
        pp.bl_disciplinaextracurricular ,
        pp.id_usuario ,
        pp.st_conteudoprogramatico ,
        pp.id_medidatempoconclusao ,
        pp.id_contratoregra ,
        pp.id_livroregistroentidade ,
        pp.nu_prazoagendamento ,
        pp.st_perfilprojeto ,
        pp.st_coordenacao ,
        pp.st_horario ,
        pp.st_publicoalvo ,
        pp.st_mercadotrabalho ,u.id_usuario as id_usuarioprojeto, u.st_login, u.st_nomecompleto, sit.st_situacao,
        pd.nu_valor AS nu_taxamatricula
from tb_projetopedagogico as pp
LEFT JOIN tb_projetopedagogico as pps ON pps.id_projetopedagogico = pp.id_projetopedagogico AND pps.id_projetopedagogico = pp.id_projetopedagogicoorigem --teste
INNER JOIN tb_usuario as u ON u.id_usuario = pp.id_usuariocadastro
LEFT JOIN tb_situacao as sit ON sit.id_situacao = pp.id_situacao
LEFT JOIN dbo.tb_produtotaxaprojeto AS ptp ON ptp.id_projetopedagogico = pp.id_projetopedagogico
LEFT JOIN dbo.vw_produto AS pd ON pd.id_produto= ptp.id_produto
where pp.bl_ativo = 1
GO

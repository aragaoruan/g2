
GO
/****** Object:  View [totvs].[vw_lancamentosabaixar]    Script Date: 10/08/2012 17:42:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [totvs].[vw_lancamentosabaixar] AS
SELECT lc.id_entidade, lv.id_venda, lc.id_lancamento, lc.bl_quitado, lc.dt_vencimento, lc.nu_valor, FLAN.IDLAN, FLAN.STATUSLAN, flan.DATABAIXA, flan.VALORBAIXADO, flan.VALORORIGINAL FROM dbo.tb_lancamentointegracao lci
INNER JOIN dbo.tb_lancamento lc ON lci.id_lancamento = lc.id_lancamento AND lc.bl_quitado = 0
INNER JOIN dbo.tb_lancamentovenda lv ON lc.id_lancamento = lv.id_lancamento
INNER JOIN dbo.tb_entidadeintegracao ei ON lc.id_entidade = ei.id_entidade AND ei.id_sistema = 3
INNER JOIN [189.114.56.180].CorporeRM_Int_Act.dbo.FLAN flan ON 
flan.CODCCUSTO COLLATE Latin1_General_CI_AS = ei.st_codchave COLLATE Latin1_General_CI_AS AND flan.IDLAN = lci.st_codlancamento AND flan.STATUSLAN = 1
GO

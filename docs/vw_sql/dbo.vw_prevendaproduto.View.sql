
GO
/****** Object:  View [dbo].[vw_prevendaproduto]    Script Date: 10/08/2012 17:42:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_prevendaproduto]
as
select
ppv.id_prevendaproduto, ppv.id_prevenda, pd.id_produto ,pd.st_produto,tp.st_tipoproduto,pdv.dt_inicio, pdv.dt_termino, pdv.id_produtovalor, pdv.nu_valor, pdv.nu_valormensal
from tb_prevendaproduto as ppv
JOIN tb_produto as pd ON pd.id_produto = ppv.id_produto
JOIN tb_produtovalor as pdv ON pdv.id_produto = pd.id_produto
JOIN tb_tipoproduto as tp ON tp.id_tipoproduto = pd.id_tipoproduto
GO

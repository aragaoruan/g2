
/****** Object:  View [dbo].[vw_retornolancamento]    Script Date: 29/10/2014 16:01:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




 CREATE VIEW [dbo].[vw_retornolancamento]
AS
    SELECT  ar.id_arquivoretorno ,
            st_arquivoretorno ,
            ar.id_entidade ,
            ar.bl_ativo AS bl_ativoretorno ,
            ar.st_banco ,
            arl.id_arquivoretornolancamento ,
            arl.st_nossonumero ,
            arl.nu_valor AS nu_valorretorno ,
            arl.nu_desconto AS nu_descontoretorno ,
            arl.nu_juros AS nu_jurosretorno ,
            arl.dt_ocorrencia AS dt_ocorrenciaretorno ,
            arl.bl_quitado AS bl_quitadoretorno ,
            lc.nu_valor ,
            lc.id_meiopagamento ,
            lc.bl_quitado ,
            lc.dt_vencimento ,
            lc.dt_quitado ,
            lc.dt_emissao ,
            lc.dt_prevquitado ,
            id_entidadelancamento ,
            lc.bl_ativo ,
            lc.id_lancamento ,
            lc.st_coddocumento,
            arl.st_carteira,
            arl.dt_quitado AS dt_quitadoretorno,
            arl.nu_valornominal,
            arl.nu_tarifa,
			NULL AS nu_valortotal,
			lc.id_venda
    FROM    dbo.tb_arquivoretorno AS ar
            JOIN dbo.tb_arquivoretornolancamento AS arl ON arl.id_arquivoretorno = ar.id_arquivoretorno
                                                          -- AND CAST(arl.st_carteira AS INT) = 109
            /*LEFT JOIN dbo.tb_lancamento AS lc ON CAST(arl.st_nossonumero AS INT) = lc.id_lancamento
                                                 AND lc.id_meiopagamento = 2
                                                 AND ( lc.nu_valor - 2.70 -ISNULL(lc.nu_desconto,0) ) < arl.nu_valor
                                                 AND ( lc.nu_valor * 1.6 ) >= arl.nu_valor
                                                 AND DATEDIFF(MONTH,
                                                              lc.dt_vencimento,
                                                              arl.dt_ocorrencia) <= 6
                                                 AND DATEDIFF(DAY,
                                                              lc.dt_vencimento,
                                                              arl.dt_ocorrencia) BETWEEN -360
                                                              AND
                                                              100
            LEFT JOIN dbo.tb_lancamentovenda lv ON lc.id_lancamento = lv.id_lancamento
            LEFT JOIN dbo.tb_venda v ON lv.id_venda = v.id_venda */
           
           OUTER APPLY(
				SELECT lc.* , lv.id_venda
					FROM  tb_lancamento AS lc 
						JOIN dbo.tb_lancamentovenda lv ON lc.id_lancamento = lv.id_lancamento
						JOIN dbo.tb_venda v ON lv.id_venda = v.id_venda 
						LEFT JOIN dbo.tb_campanhacomercial cc ON v.id_campanhapontualidade = cc.id_campanhacomercial
										AND lc.dt_vencimento >= arl.dt_ocorrencia
						
						WHERE lc.id_meiopagamento = 2
						AND CAST(arl.st_nossonumero AS INT) = lc.id_lancamento
						
						AND  ( ( lc.nu_valor - (lc.nu_valor*isnull(cc.nu_valordesconto,0))/100)  - 2.70 - ISNULL(lc.nu_desconto,0) ) < arl.nu_valor
                                                 AND ( ( lc.nu_valor - (lc.nu_valor*isnull(cc.nu_valordesconto,0))/100)  * 1.6 ) >= arl.nu_valor
                                                 /*AND DATEDIFF(MONTH,
                                                              lc.dt_vencimento,
                                                              arl.dt_ocorrencia) <= 6
                                                 AND DATEDIFF(DAY,
                                                              lc.dt_vencimento,
                                                              arl.dt_ocorrencia) BETWEEN -360
                                                              AND
                                                              150                               */
           
           ) AS lc 

													  





GO



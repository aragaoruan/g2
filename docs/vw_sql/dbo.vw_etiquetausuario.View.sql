CREATE VIEW [dbo].[vw_etiquetausuario] AS
  SELECT DISTINCT
    vm.id_usuario AS id_usuario,
    vm.st_nomecompleto AS st_nomecompleto,
    vm.st_cpf AS st_cpf,
    vm.id_matricula AS id_matricula,
    ec.st_endereco AS st_endereco,
    ec.nu_numero AS st_numero,
    ec.st_bairro AS st_bairro,
    ec.st_cidade AS st_cidade,
    ec.sg_uf AS st_uf,
    ec.st_cep AS st_cep,
    ec.st_complemento AS st_complemento,
    '-' AS st_servico,
    ps.id_entidade AS id_entidade,
    vm.dt_cadastrocertificacao AS dt_solicitacao,
    vm.dt_envioaluno AS dt_entrega,
    vm.st_evolucao AS st_evolucao,
    ce.st_email AS st_email,
    vm.st_projetopedagogico AS st_projetopedagogico
  FROM
    vw_matricula vm
    JOIN tb_pessoa ps ON ps.id_usuario = vm.id_usuario
    JOIN tb_pessoaendereco pe ON pe.id_usuario = vm.id_usuario
                                 AND pe.id_entidade = ps.id_entidade
    JOIN tb_endereco ec ON ec.id_endereco = pe.id_endereco
    JOIN tb_contatosemailpessoaperfil cp ON cp.id_usuario = vm.id_usuario
                                            AND cp.id_entidade = ps.id_entidade
    JOIN tb_contatosemail ce ON ce.id_email = cp.id_email
  WHERE
    pe.bl_padrao = 1
    AND ec.id_tipoendereco = 5
    AND cp.bl_padrao = 1
    AND cp.bl_ativo = 1
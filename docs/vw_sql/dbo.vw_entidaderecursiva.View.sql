
GO
/****** Object:  View [dbo].[vw_entidaderecursiva]    Script Date: 10/08/2012 17:41:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_entidaderecursiva] AS
WITH rec (id_entidade, st_nomeentidade, id_entidadepai, nu_nivelhierarquia, cadeiaentidades)as (
   select
		e.id_entidade,
		e.st_nomeentidade,
		null as id_entidadepai,
		1 as nu_nivelhierarquia,
		CAST(('|' + CAST(e.id_entidade AS VARCHAR) + '|') AS VARCHAR) as cadeiaentidades
		from vw_entidaderelacao e
   where
		e.bl_ativo = '1'
		AND e.id_situacao = '2'
   --AND (e.id_entidade = e.id_entidadepai OR e.id_entidadepai IS NULL)
   --and e.id_entidadeclasse = 1
   UNION ALL
   select
		e.id_entidade,
		e.st_nomeentidade,
		e.id_entidadepai,
		rec.nu_nivelhierarquia + 1 as nu_nivelhierarquia,
		CAST((rec.cadeiaentidades + CAST(e.id_entidade as VARCHAR)+ '|' ) AS VARCHAR) as cadeiaentidades
		from vw_entidaderelacao e
		INNER JOIN rec ON rec.id_entidade = e.id_entidadepai AND e.id_entidade <> e.id_entidadepai and e.id_entidadeclasse = 1
		WHERE
		e.bl_ativo = '1'
		AND e.id_situacao = '2'
  )select * from rec
GO

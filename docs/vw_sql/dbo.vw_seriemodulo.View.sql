
GO
/****** Object:  View [dbo].[vw_seriemodulo]    Script Date: 10/08/2012 17:42:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_seriemodulo] AS
select pp.id_projetopedagogico, pp.id_entidadecadastro, m.id_modulo, m.id_moduloanterior, 
m.st_modulo, m.st_tituloexibicao as st_tituloexibicaomodulo, dsne.id_disciplina, dsne.id_nivelensino, dsne.id_serie, 
d.st_disciplina, d.st_tituloexibicao
from tb_projetopedagogico as pp
INNER JOIN tb_modulo as m ON m.id_projetopedagogico = pp.id_projetopedagogico AND m.bl_ativo = 1
LEFT JOIN tb_modulo as mo ON mo.id_moduloanterior = m.id_modulo AND mo.bl_ativo = 1
INNER JOIN tb_modulodisciplina as md ON md.id_modulo = m.id_modulo AND md.bl_ativo = 1
INNER JOIN tb_disciplinaserienivelensino as dsne ON dsne.id_disciplina = md.id_disciplina
INNER JOIN tb_disciplina as d ON d.id_disciplina = md.id_disciplina AND d.bl_ativa = 1
where pp.bl_ativo = 1
GO

CREATE VIEW [rel].[vw_mensagemusuario] as

SELECT
	ms.id_mensagem,
	ms.st_mensagem,
	ms.bl_importante,
	ms.id_usuariocadastro,
	ms.st_texto,
	ms.dt_cadastro AS dt_cadastromsg,
	ms.id_entidade,
	ent.st_nomeentidade,
	ms.id_situacao,
	ms.id_projetopedagogico,
	ms.id_areaconhecimento,
	ms.id_turma,
	em.id_evolucao,
	em.dt_envio,
	em.dt_enviar,
	e.st_evolucao,
	ev.id_usuario,
	em.id_sistema,
	em.id_enviomensagem,
	ev.id_enviodestinatario,
	ev.id_evolucao AS id_evolucaodestinatario
FROM
	dbo.tb_mensagem AS ms
INNER JOIN tb_enviomensagem AS em ON em.id_mensagem = ms.id_mensagem
AND em.id_tipoenvio = 4
AND em.id_sistema = 1
INNER JOIN tb_enviodestinatario AS ev ON ev.id_enviomensagem = em.id_enviomensagem
INNER JOIN tb_evolucao AS e ON e.id_evolucao = em.id_evolucao
INNER JOIN tb_entidade AS ent ON ent.id_entidade = ms.id_entidade



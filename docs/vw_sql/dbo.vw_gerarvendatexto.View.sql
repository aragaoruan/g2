CREATE VIEW [dbo].[vw_gerarvendatexto] AS
  SELECT
    vd.id_venda,
    vd.id_entidade,
    lc.id_lancamento,
    lc.id_acordo,
    usv.st_cpf,
    usl.id_usuario,
    vp.id_matricula,
    usv.st_nomecompleto AS st_vendacliente,
    vd.nu_valorbruto AS nu_vendavalorbruto,
    vd.nu_valorliquido AS
    nu_vendavalorliquido,
    vd.nu_descontoporcentagem AS nu_vendadescontoporc,
    vd.nu_descontovalor AS nu_vendadescontovalor,
    lc.nu_valor AS nu_lancamentovalor,
    lc.dt_vencimento AS dt_lancamentovencimento,
    lc.dt_quitado AS dt_lancamentodataquitado,
    lc.nu_quitado AS nu_lancamentoquitado,
    mp.st_meiopagamento AS st_lancamentomeiopagamento,
    usl.st_nomecompleto AS st_lancamentoresponsavel,
    (SELECT
       dbo.fn_mesporextenso(lc.dt_vencimento)) + '/' +
    CAST(DATEPART(YEAR, lc.dt_vencimento) AS VARCHAR) AS st_datavencimento,
    vlq.nu_quitado,
    vlq.dt_primeiroquitado,
    vlq.dt_ultimoquitado,
    vlq.st_mesanoprimeiroquitado,
    vlq.st_mesanoultimoquitado,
    vlq.st_mesanoprimeiroquitado + ' a ' + vlq.st_mesanoultimoquitado AS st_periodoquitado,
    CONVERT(VARCHAR(30), GETDATE(), 103)
      AS st_atual,
    CONVERT(VARCHAR(30), YEAR(GETDATE()) - 1, 103) AS st_ano,
    usv.st_endereco,
    usv.nu_numero,
    usv.st_bairro,
    usv.st_cidade,
    usv.st_cep,
    et.st_nomeentidade,
    CASE WHEN vwps.st_nomecompleto IS NOT NULL
      THEN vwps.st_nomecompleto
    WHEN entl.st_nomeentidade IS NOT NULL
      THEN entl.st_nomeentidade
      COLLATE SQL_Latin1_General_CP1_CI_AS
    END AS st_nomecompletoresponsavel,
    CASE WHEN vwps.st_cpf IS NOT NULL
      THEN vwps.st_cpf
    WHEN entl.st_cnpj IS NOT NULL
      THEN entl.st_cnpj
    END AS st_cpfresponsavel
  FROM tb_venda AS vd
    LEFT JOIN tb_lancamentovenda AS lv ON lv.id_venda = vd.id_venda
    LEFT JOIN tb_lancamento AS lc ON lc.id_lancamento = lv.id_lancamento
    JOIN tb_vendaproduto AS vp ON vp.id_venda = vd.id_venda
--LEFT JOIN tb_matricula as mt on mt.id_vendaproduto = vp.id_vendaproduto
    JOIN vw_pessoa AS usv ON usv.id_usuario = vd.id_usuario AND usv.id_entidade = vd.id_entidade
    LEFT JOIN tb_usuario AS usl ON usl.id_usuario = lc.id_usuariolancamento
    LEFT JOIN tb_meiopagamento AS mp ON mp.id_meiopagamento = lc.id_meiopagamento
    JOIN dbo.tb_entidade AS et ON et.id_entidade = vd.id_entidade
    LEFT JOIN vw_vendalancamentoquitados AS vlq ON (vd.id_venda = vlq.id_venda)
    LEFT JOIN vw_pessoa AS vwps ON vwps.id_usuario = lc.id_usuariolancamento AND vwps.id_entidade = vd.id_entidade
    LEFT JOIN tb_entidade AS entl ON entl.id_entidade = lc.id_entidadelancamento
GO

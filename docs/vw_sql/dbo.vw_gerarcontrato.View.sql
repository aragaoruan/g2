﻿CREATE VIEW [dbo].[vw_gerarcontrato]
AS
  SELECT
    DISTINCT
    ct.id_contrato,
    psr.id_usuario AS id_usuarioresponsavel,
    ps.id_usuario AS id_usuarioaluno,
    ps.st_nomecompleto AS st_nomecompletoaluno,
    ps.st_cpf AS st_cpfaluno,
    ps.st_rg AS st_rgaluno,
    ps.st_estadocivil,
    ps.st_orgaoexpeditor AS st_orgaoexpedidor,
    CONVERT(NVARCHAR(30), ps.dt_dataexpedicao, 103) AS st_dataexpedicaoaluno,
    NULL AS st_numerotitulo,
    NULL AS st_areatitulo,
    NULL AS st_zonatitulo,
    CONVERT(NVARCHAR(30), ps.dt_dataexpedicao, 103) AS st_dataexpedicaotitulo,
    NULL AS st_numeroreservita,
--CAST(ct.nu_bolsa + ' %' AS varchar) AS st_bolsa,
    NULL AS st_numerocertidao,
    NULL AS st_livrocertidao,
    NULL AS st_folhascertidao,
    NULL AS st_descricaocartorio,
    NULL AS st_cargo,
    psr.dt_nascimento AS dt_nascimentoresponsavel,
    psr.st_rg AS st_rgresp,
    psr.st_orgaoexpeditor AS st_orgaoexpedidorresp,
    CONVERT(NVARCHAR(30), psr.dt_dataexpedicao, 103) AS st_dataexpedicaoresp,
    psr.nu_telefone AS nu_telefoneresp,
    psr.st_nomecompleto AS st_nomecompletoresponsavel,
    psr.st_cpf AS st_cpfresponsavel,
--cr.id_tipocontratoresponsavel,
--tcr.st_tipocontratoresponsavel,
    1 AS id_tipocontratoresponsavel_financeiro,
--FINANCEIRO
    'FINANCEIRO' AS st_tipocontratoresponsavel_financeiro,
--FINANCEIRO
    '100%' AS nu_porcentagem,
    psr.st_endereco AS st_enderecoresponsavel,
    psr.st_complemento AS st_complementoresponsavel,
    psr.st_cep AS st_cepresponsavel,
    psr.st_cidade AS st_cidaderesponsavel,
    psr.st_bairro AS st_bairroresponsavel,
    psr.sg_uf AS sg_ufresponsavel,
    ps.dt_nascimento AS dt_nascimentoaluno,
    ps.st_endereco AS st_enderecoaluno,
    ps.st_bairro AS st_bairroaluno,
    ps.st_cep AS st_cepaluno,
    ps.st_nomemunicipio AS st_municipioaluno,
    ps.st_cidade AS st_cidadealuno,
    ps.st_nomepais AS st_paisaluno,
    ps.sg_uf AS st_ufaluno,
    NULL AS st_ufcompletoaluno,
    NULL AS st_nacionalidadealuno,
    ps.id_pais,
    ps.nu_ddd AS nu_dddaluno,
    ps.nu_telefone AS nu_telefonealuno,
    ps.st_nomemae,
    ps.st_nomepai,
    etee.st_endereco AS st_enderecoentidade,
    etee.st_bairro AS st_bairroentidade,
    etee.st_cep AS st_cepentidade,
    NULL AS st_emailentidade,
    '(' + eCtel.nu_ddd + ') ' + eCtel.nu_telefone AS nu_telefoneEntiadeConcat,
    CONVERT(NVARCHAR(30), GETDATE(), 103) AS st_datahora,
    CASE WHEN ps.st_sexo = 'm'
      THEN 'Masculino'
    WHEN ps.st_sexo = 'f'
      THEN 'Feminino'
    ELSE 'Não Informado'
    END AS st_sexoaluno,
--pp.st_tituloexibicao as st_projeto,
    (SELECT
       CONVERT(VARCHAR(1000), STUFF((SELECT
                                       ' / '
                                       + st_tituloexibicao
                                     FROM (SELECT
                                             pp.st_tituloexibicao
                                           FROM
                                             tb_contratomatricula AS ctm
                                             JOIN tb_matricula AS mt ON mt.id_matricula = ctm.id_matricula
                                             JOIN tb_projetopedagogico
                                             AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
                                           WHERE
                                             ctm.id_contrato = ct.id_contrato AND ctm.bl_ativo = 1
                                          ) AS cursosreferencia
                                     FOR
                                     XML PATH ('')
                                    ), 1, 3, ''))
    ) AS st_projeto,
    DAY(GETDATE()) AS nu_dia,
    MONTH(GETDATE()) AS nu_mes,
    YEAR(GETDATE()) AS nu_ano,
    et.id_entidade,
    et.st_nomeentidade,
    et.st_razaosocial,
    et.st_cnpj,
    et.st_urlimglogo,
    et.st_urlsite,
    et.st_nomeentidade AS st_polo,
    usd.st_nomecompleto AS st_diretor,
    uss.st_nomecompleto AS st_secretarioescolar,
    vd.nu_diamensalidade,
    NULL AS st_etnia,
    ps.st_email,
    NULL AS st_tiposanguineo,
    NULL AS st_alergias,
    NULL AS st_glicemia,
    vd.nu_valorliquido nu_valorliquido_venda,
    vd.nu_parcelas AS nu_parcelas_venda,
    vd.nu_creditos,
--mp.st_meiopagamento,
    vd.dt_confirmacao AS dt_confirmacao_venda,
    vd.nu_viascontrato AS nu_viascontrato,
    CONVERT(NVARCHAR(30), GETDATE(), 103) AS dt_aceite,
    vd.id_venda,
    (SELECT
       CONVERT(VARCHAR(1000), STUFF((SELECT
                                       ' , '
                                       + CONVERT(NVARCHAR(30), dt_inicio, 103)
                                     FROM (SELECT
                                             tur.dt_inicio
                                           FROM
                                             tb_turma AS tur
                                           WHERE
                                             ct.id_venda = vd.id_venda
                                             AND vdp.id_venda = vd.id_venda
                                             AND vdp.id_turma = tur.id_turma
                                          ) AS turmareferencia
                                     FOR
                                     XML PATH ('')
                                    ), 1, 3, ''))
    ) AS st_inicio,
    (SELECT
       CONVERT(VARCHAR(1000), STUFF((SELECT
                                       ' , ' + st_turno
                                     FROM (SELECT
                                             turno.st_turno
                                           FROM
                                             tb_turno AS turno
                                             JOIN tb_turmaturno
                                             AS turmaturno ON turmaturno.id_turno = turno.id_turno
                                             JOIN tb_turma AS turma ON turma.id_turma = turmaturno.id_turma
                                           WHERE
                                             vdp.id_turma = turma.id_turma
                                             AND vdp.id_venda = ct.id_venda
                                          ) AS turnoreferencia
                                     FOR
                                     XML PATH ('')
                                    ), 1, 3, ''))
    ) AS st_turno,
    (SELECT
       CONVERT(VARCHAR(1000), STUFF((SELECT
                                       ' , '
                                       + CONVERT(VARCHAR(100), nu_cargahoraria)
                                     FROM (SELECT
                                             ppp.nu_cargahoraria
                                           FROM
                                             tb_produtoprojetopedagogico
                                               AS ppp
                                             JOIN tb_vendaproduto
                                             AS vdp ON ppp.id_produto = vdp.id_produto
                                             JOIN tb_projetopedagogico
                                             AS pp ON pp.id_projetopedagogico = ppp.id_projetopedagogico
                                           WHERE
                                             vdp.id_venda = ct.id_venda
                                          ) AS turmareferencia
                                     FOR
                                     XML PATH ('')
                                    ), 1, 3, ''))
    ) AS nu_cargahoraria,
    (SELECT
       CONVERT(VARCHAR(1000), STUFF((SELECT
                                       ' , '
                                       + st_tituloexibicao
                                     FROM (SELECT
                                             tur.st_tituloexibicao
                                           FROM
                                             tb_turma AS tur
                                           WHERE
                                             ct.id_venda = vdp.id_venda
                                             AND vdp.id_turma = tur.id_turma
                                          ) AS turmareferencia
                                     FOR
                                     XML PATH ('')
                                    ), 1, 3, ''))
    ) AS st_turma
--
    ,
    ctr.id_contratomodelo,
    cpp.st_campanhacomercial AS st_campanhapontualidade,
      st_porcentagemcampanhapontualidade = (CASE
                                            --Se a campanha de pontualidade for do tipo valor, o sistema não deve exibir seu valor em reais. Apresentar um hífen no resultado;
                                            WHEN cpp.id_tipodesconto = 1
                                              THEN '-'
                                            ELSE CONCAT(CAST(ISNULL(cpp.nu_valordesconto, 0) AS INT), '%')
                                            END),
    pa_m.dt_aceita AS st_aceita,
--GII-8375
    vdp.id_matricula

-- Transferencia
    ,
    vad.dt_transferencia,
    vad.id_matriculaorigem,
    vad.st_projetoorigem,
    vad.id_matriculadestino,
    vad.st_projetodestino,
    vad.nu_cargahorariadestino,
    vad.nu_valorliquidotransferencia,
    vad.id_vendaaditivo
  FROM tb_contrato ct
    JOIN tb_venda AS vd ON vd.id_venda = ct.id_venda
--
-- AND vd.id_venda IN (103855, 103915)
--
    JOIN tb_vendaproduto AS vdp ON vdp.id_venda = vd.id_venda
    JOIN tb_produtoprojetopedagogico AS ppp ON ppp.id_produto = vdp.id_produto
    JOIN tb_projetopedagogico AS pp ON pp.id_projetopedagogico = ppp.id_projetopedagogico
--
--select TOP(1) viu.id_contrato, vp.id_produto, ctr.id_contratomodelo, ts.*
--from vw_gerarcontrato AS viu
--JOIN dbo.tb_venda AS vd ON vd.id_venda = viu.id_venda AND viu.id_venda IN (103855, 103915)
--JOIN dbo.tb_vendaproduto AS vp ON vp.id_venda = vd.id_venda
--JOIN dbo.tb_produtoprojetopedagogico AS ppp ON ppp.id_produto = vp.id_produto
--JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = ppp.id_projetopedagogico
    JOIN tb_contratoregra AS ctr ON ctr.id_contratoregra = ct.id_contratoregra
    JOIN dbo.tb_textosistema AS ts ON ts.id_textosistema = ctr.id_contratomodelo
--
    JOIN tb_entidade AS et ON et.id_entidade = ct.id_entidade
    LEFT JOIN tb_entidaderesponsavellegal AS etrd ON etrd.id_entidade = et.id_entidade
                                                     AND etrd.bl_padrao = 1
                                                     AND etrd.id_tipoentidaderesponsavel = 1
    LEFT JOIN tb_usuario AS usd ON usd.id_usuario = etrd.id_usuario
    LEFT JOIN tb_entidaderesponsavellegal AS etrs ON etrs.id_entidade = et.id_entidade
                                                     AND etrs.bl_padrao = 1
                                                     AND etrs.id_tipoentidaderesponsavel = 2
    LEFT JOIN tb_usuario AS uss ON uss.id_usuario = etrd.id_usuario
    LEFT JOIN tb_entidadeendereco AS ete ON ete.id_entidade = et.id_entidade
                                            AND ete.bl_padrao = 1
    LEFT JOIN tb_endereco AS etee ON etee.id_endereco = ete.id_endereco
    LEFT JOIN tb_contatostelefoneentidade AS eTel ON etel.id_entidade = et.id_entidade
    LEFT JOIN tb_contatostelefone AS eCtel ON eCtel.id_telefone = etel.id_telefone
    JOIN vw_pessoa AS ps ON ps.id_usuario = ct.id_usuario
                            AND ps.id_entidade = ct.id_entidade
    LEFT JOIN tb_campanhacomercial AS cpp ON cpp.id_campanhacomercial = vd.id_campanhapontualidade AND cpp.bl_ativo = 1
    OUTER APPLY (SELECT
                   TOP 1
                   psri.id_usuario,
                   psri.st_login,
                   psri.st_loginentidade,
                   psri.st_nomecompleto,
                   psri.st_cpf,
                   psri.id_registropessoa,
                   psri.st_senha,
                   psri.st_senhaentidade,
                   psri.id_entidade,
                   psri.st_sexo,
                   psri.dt_nascimento,
                   psri.sg_ufnascimento,
                   psri.id_municipionascimento,
                   psri.st_cidadenascimento,
                   psri.id_estadocivil,
                   psri.st_estadocivil,
                   psri.id_email,
                   psri.st_email,
                   psri.bl_emailpadrao,
                   psri.st_nomemae,
                   psri.st_nomepai,
                   psri.id_telefone,
                   psri.bl_telefonepadrao,
                   psri.nu_telefone,
                   psri.id_tipotelefone,
                   psri.st_tipotelefone,
                   psri.nu_ddd,
                   psri.nu_ddi,
                   psri.id_telefonealternativo,
                   psri.nu_telefonealternativo,
                   psri.id_tipotelefonealternativo,
                   psri.st_tipotelefonealternativo,
                   psri.nu_dddalternativo,
                   psri.nu_ddialternativo,
                   psri.st_rg,
                   psri.st_orgaoexpeditor,
                   psri.dt_dataexpedicao,
                   psri.id_endereco,
                   psri.bl_enderecopadrao,
                   psri.st_endereco,
                   psri.st_cep,
                   psri.st_bairro,
                   psri.st_complemento,
                   psri.nu_numero,
                   psri.st_estadoprovincia,
                   psri.st_cidade,
                   psri.id_pais,
                   psri.id_municipio,
                   psri.id_tipoendereco,
                   psri.sg_uf,
                   psri.st_nomepais,
                   psri.st_nomemunicipio
                 FROM dbo.vw_lancamento AS lc
                   JOIN vw_pessoa AS psri ON psri.id_usuario = lc.id_usuariolancamento
                                             AND psri.id_entidade = ct.id_entidade
                 WHERE lc.id_venda = vd.id_venda
                ) AS psr
    LEFT JOIN dbo.tb_pa_marcacaoetapa AS pa_m ON pa_m.id_venda = vd.id_venda
--Transferencia
    OUTER APPLY (
                  SELECT
                    TOP 1
                    aditivo.id_vendaaditivo,
                    aditivo.dt_cadastro AS dt_transferencia,
                    mat_destino.id_matriculaorigem AS id_matriculaorigem,
                    pp_origem.st_projetopedagogico AS st_projetoorigem,
                    mat_destino.id_matricula AS id_matriculadestino,
                    pp_destino.st_projetopedagogico AS st_projetodestino,
                    pp_destino.nu_cargahoraria AS nu_cargahorariadestino,
                    aditivo.nu_valortransferencia AS nu_valorliquidotransferencia
                  FROM
                    tb_vendaaditivo AS aditivo
                    JOIN tb_matricula AS mat_destino ON aditivo.id_matricula = mat_destino.id_matricula
                    JOIN tb_matricula AS mat_origem ON mat_destino.id_matriculaorigem = mat_origem.id_matricula
                    JOIN tb_projetopedagogico AS pp_origem ON pp_origem.id_projetopedagogico =
                                                              mat_origem.id_projetopedagogico
                    JOIN tb_projetopedagogico AS pp_destino ON pp_destino.id_projetopedagogico =
                                                               mat_destino.id_projetopedagogico
                  WHERE aditivo.id_venda = vd.id_venda
                  ORDER BY
                    aditivo.dt_cadastro DESC
                ) AS vad


GO
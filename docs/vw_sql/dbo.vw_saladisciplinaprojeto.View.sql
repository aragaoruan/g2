CREATE VIEW [dbo].[vw_saladisciplinaprojeto]
AS
    SELECT  aps.id_saladeaula ,
            sa.st_saladeaula ,
            sa.nu_maxalunos ,
            aps.id_projetopedagogico ,
			pp.st_projetopedagogico,
            pp.id_trilha ,
            mt.id_matricula ,
            ds.id_disciplina ,
            md.id_matriculadisciplina ,
            mt.id_usuario ,
            al.id_alocacao ,
            se.id_entidade ,
			alocados.nu as nu_alocados

    FROM    tb_matricula AS mt
            JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
            JOIN tb_areaprojetosala AS aps ON aps.id_projetopedagogico = mt.id_projetopedagogico
            JOIN tb_saladeaula AS sa ON sa.id_saladeaula = aps.id_saladeaula
                                        AND sa.dt_inicioinscricao <= CAST( GETDATE() AS DATE)
                                        AND ( sa.dt_fiminscricao >= CAST( GETDATE() AS DATE)
                                              OR dt_fiminscricao IS NULL
                                        )
                                        AND sa.bl_ativa = 1
                                        AND sa.id_categoriasala = 1
                                        AND sa.id_situacao = 8
                                        AND sa.bl_vincularturma = 0
            JOIN tb_disciplinasaladeaula AS ds ON ds.id_saladeaula = sa.id_saladeaula
            JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
                                                 AND ds.id_disciplina = md.id_disciplina
                                                 AND md.id_situacao != 65 -- Ignorar alocacoes com APROVEITAMENTO DE DISCIPLINA
            JOIN tb_saladeaulaentidade AS se ON se.id_saladeaula = sa.id_saladeaula
                                                AND se.id_entidade = mt.id_entidadeatendimento
            LEFT JOIN tb_alocacao al ON al.id_matriculadisciplina = md.id_matriculadisciplina AND al.bl_ativo = '1'
			OUTER apply      ( SELECT    COUNT(alc.id_alocacao) as nu
              FROM      tb_alocacao alc ,
                        dbo.tb_matriculadisciplina mdc
              WHERE     alc.id_saladeaula = sa.id_saladeaula
                        AND mdc.id_matriculadisciplina = alc.id_matriculadisciplina
                        AND mdc.id_evolucao = 13 and alc.bl_ativo = 1
            ) AS alocados
                                        where alocados.nu < sa.nu_maxalunos

    UNION ALL

    SELECT  aps.id_saladeaula ,
            sa.st_saladeaula ,
            sa.nu_maxalunos ,
            aps.id_projetopedagogico ,
			pp.st_projetopedagogico,
            pp.id_trilha ,
            mt.id_matricula ,
            ds.id_disciplina ,
            md.id_matriculadisciplina ,
            mt.id_usuario ,
            al.id_alocacao ,
            ve.id_entidade ,
            alocados.nu as nu_alocados
    FROM    tb_matricula AS mt
            JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
            JOIN tb_areaprojetosala AS aps ON aps.id_projetopedagogico = mt.id_projetopedagogico
            JOIN tb_saladeaula AS sa ON sa.id_saladeaula = aps.id_saladeaula
                                        AND sa.dt_inicioinscricao <= CAST( GETDATE() AS DATE)
                                        AND ( sa.dt_fiminscricao >= CAST( GETDATE() AS DATE)
                                              OR dt_fiminscricao IS NULL
                                        )
                                        AND sa.bl_ativa = 1
                                        AND sa.bl_todasentidades = 1
                                        AND sa.id_categoriasala = 1
                                        AND sa.id_situacao = 8
                                        AND sa.bl_vincularturma = 0
            JOIN tb_disciplinasaladeaula AS ds ON ds.id_saladeaula = sa.id_saladeaula
            JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
                                                 AND ds.id_disciplina = md.id_disciplina
                                                 AND md.id_situacao != 65 -- Ignorar alocacoes com APROVEITAMENTO DE DISCIPLINA
            JOIN dbo.tb_entidaderelacao AS ve ON ve.id_entidade = mt.id_entidadeatendimento
                                                 AND ve.id_entidadepai = sa.id_entidade
            LEFT JOIN tb_alocacao al ON al.id_matriculadisciplina = md.id_matriculadisciplina
                                        AND al.bl_ativo = 1
												OUTER apply      ( SELECT    COUNT(alc.id_alocacao) as nu
              FROM      tb_alocacao alc ,
                        dbo.tb_matriculadisciplina mdc
              WHERE     alc.id_saladeaula = sa.id_saladeaula
                        AND mdc.id_matriculadisciplina = alc.id_matriculadisciplina
                        AND mdc.id_evolucao = 13 and alc.bl_ativo = 1
            ) AS alocados
                                        where alocados.nu < sa.nu_maxalunos
    UNION ALL

    SELECT  aps.id_saladeaula ,
            sa.st_saladeaula ,
            sa.nu_maxalunos ,
            aps.id_projetopedagogico ,
			pp.st_projetopedagogico,
            pp.id_trilha ,
            mt.id_matricula ,
            ds.id_disciplina ,
            md.id_matriculadisciplina ,
            mt.id_usuario ,
            al.id_alocacao ,
            ve.id_entidade ,
            alocados.nu as nu_alocados
    FROM    tb_matricula AS mt
            JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
            JOIN tb_areaprojetosala AS aps ON aps.id_projetopedagogico = mt.id_projetopedagogico
            JOIN tb_saladeaula AS sa ON sa.id_saladeaula = aps.id_saladeaula
                                        AND sa.dt_inicioinscricao <= CAST( GETDATE() AS DATE)
                                        AND ( sa.dt_fiminscricao >= CAST( GETDATE() AS DATE)
                                              OR dt_fiminscricao IS NULL
                                        )
                                        AND sa.bl_ativa = 1
                                        AND sa.bl_todasentidades = 1
                                        AND sa.id_categoriasala = 1
                                        AND sa.bl_vincularturma = 0
            JOIN tb_disciplinasaladeaula AS ds ON ds.id_saladeaula = sa.id_saladeaula
            JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
                                                 AND ds.id_disciplina = md.id_disciplina
                                                 AND md.id_situacao != 65 -- Ignorar alocacoes com APROVEITAMENTO DE DISCIPLINA
            JOIN tb_entidade AS ve ON ve.id_entidade = sa.id_entidade
            LEFT JOIN tb_alocacao al ON al.id_matriculadisciplina = md.id_matriculadisciplina
                                        AND al.bl_ativo = 1
												OUTER apply      ( SELECT    COUNT(alc.id_alocacao) as nu
              FROM      tb_alocacao alc ,
                        dbo.tb_matriculadisciplina mdc
              WHERE     alc.id_saladeaula = sa.id_saladeaula
                        AND mdc.id_matriculadisciplina = alc.id_matriculadisciplina
                        AND mdc.id_evolucao = 13 and alc.bl_ativo = 1
            ) AS alocados
                                        where alocados.nu < sa.nu_maxalunos

UNION ALL

SELECT  aps.id_saladeaula ,
            sa.st_saladeaula ,
            sa.nu_maxalunos ,
            aps.id_projetopedagogico ,
			pp.st_projetopedagogico,
            pp.id_trilha ,
            mt.id_matricula ,
            ds.id_disciplina ,
            md.id_matriculadisciplina ,
            mt.id_usuario ,
            al.id_alocacao ,
            se.id_entidade ,
			alocados.nu as nu_alocados

    FROM    tb_matricula AS mt
            JOIN tb_turmasala ts ON ts.id_turma = mt.id_turma
            JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
            JOIN tb_areaprojetosala AS aps ON aps.id_projetopedagogico = mt.id_projetopedagogico
            JOIN tb_saladeaula AS sa ON sa.id_saladeaula = aps.id_saladeaula
                                        AND sa.dt_inicioinscricao <= CAST( GETDATE() AS DATE)
                                        AND ( sa.dt_fiminscricao >= CAST( GETDATE() AS DATE)
                                              OR dt_fiminscricao IS NULL
                                        )
                                        AND sa.bl_ativa = 1
                                        AND sa.id_categoriasala = 1
                                        AND sa.id_situacao = 8
                                        AND sa.bl_vincularturma = 1
                                        AND sa.id_saladeaula = ts.id_saladeaula
            JOIN tb_disciplinasaladeaula AS ds ON ds.id_saladeaula = sa.id_saladeaula
            JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
                                                 AND ds.id_disciplina = md.id_disciplina
                                                 AND md.id_situacao != 65 -- Ignorar alocacoes com APROVEITAMENTO DE DISCIPLINA
            JOIN tb_saladeaulaentidade AS se ON se.id_saladeaula = sa.id_saladeaula
                                                AND se.id_entidade = mt.id_entidadeatendimento
            LEFT JOIN tb_alocacao al ON al.id_matriculadisciplina = md.id_matriculadisciplina AND al.bl_ativo = '1'
			OUTER apply      ( SELECT    COUNT(alc.id_alocacao) as nu
              FROM      tb_alocacao alc ,
                        dbo.tb_matriculadisciplina mdc
              WHERE     alc.id_saladeaula = sa.id_saladeaula
                        AND mdc.id_matriculadisciplina = alc.id_matriculadisciplina
                        AND mdc.id_evolucao = 13 and alc.bl_ativo = 1
            ) AS alocados
                                        where alocados.nu < sa.nu_maxalunos

    UNION ALL

    SELECT  aps.id_saladeaula ,
            sa.st_saladeaula ,
            sa.nu_maxalunos ,
            aps.id_projetopedagogico ,
			pp.st_projetopedagogico,
            pp.id_trilha ,
            mt.id_matricula ,
            ds.id_disciplina ,
            md.id_matriculadisciplina ,
            mt.id_usuario ,
            al.id_alocacao ,
            ve.id_entidade ,
            alocados.nu as nu_alocados
    FROM    tb_matricula AS mt
            JOIN tb_turmasala ts ON ts.id_turma = mt.id_turma
            JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
            JOIN tb_areaprojetosala AS aps ON aps.id_projetopedagogico = mt.id_projetopedagogico
            JOIN tb_saladeaula AS sa ON sa.id_saladeaula = aps.id_saladeaula
                                        AND sa.dt_inicioinscricao <= CAST( GETDATE() AS DATE)
                                        AND ( sa.dt_fiminscricao >= CAST( GETDATE() AS DATE)
                                              OR dt_fiminscricao IS NULL
                                        )
                                        AND sa.bl_ativa = 1
                                        AND sa.bl_todasentidades = 1
                                        AND sa.id_categoriasala = 1
                                        AND sa.id_situacao = 8
                                        AND sa.bl_vincularturma = 1
                                        AND sa.id_saladeaula = ts.id_saladeaula
            JOIN tb_disciplinasaladeaula AS ds ON ds.id_saladeaula = sa.id_saladeaula
            JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
                                                 AND ds.id_disciplina = md.id_disciplina
                                                 AND md.id_situacao != 65 -- Ignorar alocacoes com APROVEITAMENTO DE DISCIPLINA
            JOIN dbo.tb_entidaderelacao AS ve ON ve.id_entidade = mt.id_entidadeatendimento
                                                 AND ve.id_entidadepai = sa.id_entidade
            LEFT JOIN tb_alocacao al ON al.id_matriculadisciplina = md.id_matriculadisciplina
                                        AND al.bl_ativo = 1
												OUTER apply      ( SELECT    COUNT(alc.id_alocacao) as nu
              FROM      tb_alocacao alc ,
                        dbo.tb_matriculadisciplina mdc
              WHERE     alc.id_saladeaula = sa.id_saladeaula
                        AND mdc.id_matriculadisciplina = alc.id_matriculadisciplina
                        AND mdc.id_evolucao = 13 and alc.bl_ativo = 1
            ) AS alocados
                                        where alocados.nu < sa.nu_maxalunos
    UNION ALL

    SELECT  aps.id_saladeaula ,
            sa.st_saladeaula ,
            sa.nu_maxalunos ,
            aps.id_projetopedagogico ,
			pp.st_projetopedagogico,
            pp.id_trilha ,
            mt.id_matricula ,
            ds.id_disciplina ,
            md.id_matriculadisciplina ,
            mt.id_usuario ,
            al.id_alocacao ,
            ve.id_entidade ,
            alocados.nu as nu_alocados
    FROM    tb_matricula AS mt
            JOIN tb_turmasala ts ON ts.id_turma = mt.id_turma
            JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
            JOIN tb_areaprojetosala AS aps ON aps.id_projetopedagogico = mt.id_projetopedagogico
            JOIN tb_saladeaula AS sa ON sa.id_saladeaula = aps.id_saladeaula
                                        AND sa.dt_inicioinscricao <= CAST( GETDATE() AS DATE)
                                        AND ( sa.dt_fiminscricao >= CAST( GETDATE() AS DATE)
                                              OR dt_fiminscricao IS NULL
                                        )
                                        AND sa.bl_ativa = 1
                                        AND sa.bl_todasentidades = 1
                                        AND sa.id_categoriasala = 1
                                        AND sa.bl_vincularturma = 1
                                        AND sa.id_saladeaula = ts.id_saladeaula
            JOIN tb_disciplinasaladeaula AS ds ON ds.id_saladeaula = sa.id_saladeaula
            JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
                                                 AND ds.id_disciplina = md.id_disciplina
                                                 AND md.id_situacao != 65 -- Ignorar alocacoes com APROVEITAMENTO DE DISCIPLINA
            JOIN tb_entidade AS ve ON ve.id_entidade = sa.id_entidade
            LEFT JOIN tb_alocacao al ON al.id_matriculadisciplina = md.id_matriculadisciplina
                                        AND al.bl_ativo = 1
												OUTER apply      ( SELECT    COUNT(alc.id_alocacao) as nu
              FROM      tb_alocacao alc ,
                        dbo.tb_matriculadisciplina mdc
              WHERE     alc.id_saladeaula = sa.id_saladeaula
                        AND mdc.id_matriculadisciplina = alc.id_matriculadisciplina
                        AND mdc.id_evolucao = 13 and alc.bl_ativo = 1
            ) AS alocados
                                        where alocados.nu < sa.nu_maxalunos
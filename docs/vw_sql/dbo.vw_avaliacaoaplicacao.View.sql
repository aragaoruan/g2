CREATE VIEW [dbo].[vw_avaliacaoaplicacao] AS

WITH _config AS (
  SELECT
  1 AS TRUE,
  68 AS AGENDADO,
  69 AS REAGENDADO,
  70 AS CANCELADO
)

SELECT
	DISTINCT
	aa.id_avaliacaoaplicacao,
	aa.id_aplicadorprova,
	aa.id_endereco,
	aa.dt_aplicacao,
	YEAR(aa.dt_aplicacao) AS nu_anoaplicacao,
	MONTH(aa.dt_aplicacao) AS nu_mesaplicacao,
	DAY(aa.dt_aplicacao) AS nu_diaaplicacao,
	app.st_aplicadorprova,
	en.sg_uf,
	en.id_municipio,
	en.st_endereco,
	mp.st_nomemunicipio AS st_cidade,
	en.st_complemento,
	en.nu_numero,
	en.st_bairro,
	aa.id_entidade,
	CAST('('+ct.nu_ddd+')'+ct.nu_telefone AS VARCHAR) AS st_telefone,
	aa.id_horarioaula,
	aa.id_usuariocadastro,
	aa.dt_alteracaolimite,
	aa.dt_antecedenciaminima,
	aa.bl_unica,
	aa.dt_cadastro,
	aa.bl_ativo,
	hr.hr_inicio AS hr_inicioprova,
	hr.hr_fim AS hr_fimprova,
	hr.st_horarioaula,
	CAST(GETDATE() AS DATE) AS data,
	(CASE WHEN (CAST(GETDATE() AS DATE)) BETWEEN aa.dt_antecedenciaminima AND aa.dt_alteracaolimite THEN 1 ELSE 0 END) AS bl_dataativa,
	(CASE WHEN aa.nu_maxaplicacao <= (SELECT COUNT(id_matricula) FROM tb_avaliacaoagendamento WHERE id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao and bl_ativo=1) THEN 1 ELSE 0 END) AS bl_limitealunos,
	-- Numero total de vagas
	aa.nu_maxaplicacao,
	-- Vagas restantes
	(aa.nu_maxaplicacao - (SELECT COUNT(id_avaliacaoagendamento)
	FROM tb_avaliacaoagendamento saa
		WHERE saa.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao
			AND saa.id_entidade = aa.id_entidade
			AND bl_ativo = 1
			AND saa.id_situacao IN(_.AGENDADO, _.REAGENDADO)))
	AS nu_vagasrestantes,
  (SELECT DISTINCT COUNT(distinct saa.id_avaliacaoagendamento) from tb_avaliacaoagendamento as saa
  JOIN tb_avalagendamentoref as saar
    ON saar.id_avaliacaoagendamento = saa.id_avaliacaoagendamento
  INNER JOIN tb_avaliacaoconjuntoreferencia as sacr
    ON sacr.id_avaliacaoconjuntoreferencia = saar.id_avaliacaoconjuntoreferencia
  INNER JOIN tb_alocacao as sal
    ON sal.id_saladeaula = sacr.id_saladeaula and sal.bl_ativo = 1
  INNER JOIN tb_matriculadisciplina as smd
    ON smd.id_matriculadisciplina = sal.id_matriculadisciplina and smd.id_matricula = saa.id_matricula
  WHERE saa.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao
  AND saa.bl_ativo = _.TRUE
  AND saa.id_situacao <> _.CANCELADO)
  AS nu_relatoriosagendamento
FROM dbo.tb_avaliacaoaplicacao AS aa
	CROSS APPLY _config AS _
	INNER JOIN dbo.tb_endereco AS en ON aa.id_endereco = en.id_endereco
	LEFT JOIN tb_municipio AS mp on mp.id_municipio = en.id_municipio
	INNER JOIN dbo.tb_aplicadorprova AS app ON aa.id_aplicadorprova = app.id_aplicadorprova
	INNER JOIN tb_aplicadorprovaentidade AS ape on ape.id_aplicadorprova = aa.id_aplicadorprova AND ape.id_entidade = aa.id_entidade
	INNER JOIN tb_horarioaula AS hr on hr.id_horarioaula = aa.id_horarioaula
	LEFT JOIN dbo.tb_contatostelefonepessoa AS ctp ON ctp.id_usuario = id_usuarioaplicador AND ctp.id_entidade = id_entidadecadastro AND ctp.bl_padrao = 1
	LEFT JOIN dbo.tb_contatostelefoneentidade AS cte ON cte.id_entidade = id_entidadeaplicador AND cte.bl_padrao = 1
	LEFT JOIN dbo.tb_contatostelefone AS ct ON ct.id_telefone = cte.id_telefone OR ct.id_telefone = ctp.id_telefone
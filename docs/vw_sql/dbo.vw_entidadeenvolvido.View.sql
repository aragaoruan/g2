
GO
/****** Object:  View [dbo].[vw_entidadeenvolvido]    Script Date: 10/08/2012 17:41:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_entidadeenvolvido] as 
SELECT ee.*, u.st_nomecompleto, te.st_tipoenvolvido FROM tb_envolvidoentidade as ee
JOIN tb_usuario AS u ON u.id_usuario = ee.id_usuario
JOIN tb_tipoenvolvido AS te ON te.id_tipoenvolvido = ee.id_tipoenvolvido
GO

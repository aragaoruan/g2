
GO
/****** Object:  View [dbo].[vw_entidadebasico]    Script Date: 10/08/2012 17:41:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_entidadebasico] AS
 SELECT e.id_entidade, e.st_nomeentidade, e.bl_ativo, er.id_entidadepai, ec.id_entidadeclasse, ec.st_entidadeclasse, s.id_situacao, s.st_situacao,
CASE WHEN pe.id_perfil IS NULL
THEN p.id_perfil ELSE pe.id_perfil END as id_perfil,
CASE WHEN pe.st_nomeperfil IS NULL
THEN p.st_nomeperfil ELSE pe.st_nomeperfil END as st_nomeperfil
                FROM tb_entidade e
                INNER JOIN tb_entidaderelacao er ON e.id_entidade = er.id_entidade
                INNER JOIN tb_entidadeclasse ec ON ec.id_entidadeclasse = er.id_entidadeclasse
                INNER JOIN tb_situacao s ON e.id_situacao = s.id_situacao
                LEFT JOIN tb_perfilentidade pe ON pe.id_entidade = e.id_entidade
                LEFT JOIN tb_perfil p ON pe.id_perfil = p.id_perfil
            WHERE e.bl_ativo = 1 AND p.bl_ativo = 1
GO

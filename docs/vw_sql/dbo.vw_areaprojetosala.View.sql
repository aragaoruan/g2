
CREATE view [dbo].[vw_areaprojetosala] as
select aps.id_areaprojetosala
, aps.id_saladeaula
, sa.st_saladeaula
, aps.id_areaconhecimento
, ac.st_areaconhecimento
, aps.id_projetopedagogico
, pp.st_projetopedagogico
, pp.st_descricao
, us.st_nomecompleto AS st_coordenadorprojeto
, aps.id_nivelensino
, ne.st_nivelensino
, apsi.st_referencia
, p.id_perfilpedagogico
, aps.nu_diasacesso
from tb_areaprojetosala as aps
JOIN tb_saladeaula as sa ON sa.id_saladeaula = aps.id_saladeaula
LEFT JOIN tb_areaconhecimento as ac ON ac.id_areaconhecimento = aps.id_areaconhecimento
LEFT JOIN tb_projetopedagogico as pp ON pp.id_projetopedagogico = aps.id_projetopedagogico
LEFT JOIN tb_nivelensino as ne ON ne.id_nivelensino = aps.id_nivelensino
LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS uper ON uper.id_projetopedagogico = pp.id_projetopedagogico AND uper.bl_ativo = 1 AND uper.bl_titular = 1
LEFT JOIN dbo.tb_usuario AS us ON us.id_usuario = uper.id_usuario
LEFT JOIN dbo.tb_areaprojetosalaintegracao as apsi ON apsi.id_projetopedagogico = aps.id_projetopedagogico and apsi.id_saladeaula = aps.id_saladeaula
LEFT JOIN dbo.tb_perfil as p ON p.id_perfil = uper.id_perfil --AND p.id_perfilpedagogico = 2

GO



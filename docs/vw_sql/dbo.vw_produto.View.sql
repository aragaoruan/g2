SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[vw_produto]
AS
    SELECT pd.id_produto ,
        pd.st_produto ,
        pd.id_tipoproduto ,
        pd.id_situacao ,
        pd.id_usuariocadastro ,
        pd.id_entidade ,
        pd.bl_ativo ,
        pd.bl_todasformas ,
        pd.bl_todascampanhas ,
        pd.nu_gratuito ,
        pd.id_produtoimagempadrao ,
        pd.bl_unico ,
        pd.st_descricao ,
        pd.bl_todasentidades ,
        tpd.st_tipoproduto ,
        pdv.id_produtovalor ,
        pdv.nu_valor ,
        pdv.nu_valormensal ,
        tpv.id_tipoprodutovalor ,
        tpv.st_tipoprodutovalor ,
        st.st_situacao ,
        pl.id_planopagamento ,
        pl.nu_parcelas ,
        pl.nu_valorentrada ,
        pl.nu_valorparcela ,
        te.st_nomeentidade ,
        pd.st_subtitulo ,
        pd.st_slug ,
        pd.bl_destaque ,
        pd.dt_cadastro ,
        pd.dt_atualizado ,

        pd.dt_iniciopontosprom,

        pd.dt_fimpontosprom ,

        pd.nu_pontos ,
        pd.nu_pontospromocional ,
        pd.nu_estoque ,
        pd.st_observacoes ,
        pd.st_informacoesadicionais ,
        pd.st_estruturacurricular ,
        pd.id_modelovenda ,
        mv.st_modelovenda ,
        pd.bl_mostrarpreco ,

        id_produtovalorvenda = pdv.id_produtovalor,

        nu_valorvenda = CASE WHEN pd.id_modelovenda = 2
                                             THEN pdv.nu_valormensal
                                             ELSE pdv.nu_valor
                                        END,
        null AS nu_valorpromocional,
        ppp.id_turma,
        pts.id_produtotextosistema,
        pts.id_upload,
        pts.id_textosistema,
        fp.id_formadisponibilizacao,
        fp.st_formadisponibilizacao,
        up.st_upload
    FROM tb_produto AS pd
            OUTER APPLY ( SELECT TOP 1
            pvoa.id_produtovalor ,
            pvoa.nu_valor ,
            pvoa.nu_valormensal ,
            pvoa.id_tipoprodutovalor ,
            pvoa.id_produto
        FROM tb_produtovalor AS pvoa
        WHERE     pvoa.id_produto = pd.id_produto
            AND pvoa.dt_inicio <= CAST(GETDATE() AS DATE)
            AND ( pvoa.dt_termino >= CAST(GETDATE() AS DATE)
            OR pvoa.dt_termino IS NULL)
            AND pvoa.id_tipoprodutovalor = 4

                        ) AS pdv

        LEFT JOIN tb_tipoproduto AS tpd ON tpd.id_tipoproduto = pd.id_tipoproduto
        LEFT JOIN tb_tipoprodutovalor AS tpv ON pdv.id_tipoprodutovalor = tpv.id_tipoprodutovalor
        LEFT JOIN tb_situacao AS st ON st.id_situacao = pd.id_situacao
        LEFT JOIN tb_planopagamento AS pl ON pl.id_produto = pd.id_produto
            AND pl.bl_padrao = 1
        JOIN dbo.tb_entidade AS te ON te.id_entidade = pd.id_entidade
        JOIN dbo.tb_modelovenda AS mv ON mv.id_modelovenda = pd.id_modelovenda
        LEFT JOIN dbo.tb_produtoprojetopedagogico AS ppp ON ppp.id_produto = pd.id_produto
        LEFT JOIN dbo.tb_produtotextosistema AS pts
        ON pts.id_produto = pd.id_produto
        LEFT JOIN dbo.tb_formadisponibilizacao AS fp
        ON fp.id_formadisponibilizacao = pts.id_formadisponibilizacao
        LEFT JOIN dbo.tb_upload AS up
        ON up.id_upload = pts.id_upload
    WHERE   pd.bl_ativo = 1
          

GO


GO

/****** Object:  View [dbo].[vw_produtocompartilhado]    Script Date: 30/07/2014 15:58:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 CREATE VIEW [dbo].[vw_produtocompartilhado]
AS
SELECT
	vw.*
FROM vw_produto AS vw
JOIN tb_produto AS p
	ON (vw.id_produto = p.id_produto) AND p.bl_todasentidades = 1
JOIN vw_entidaderecursivaid AS er
	ON er.nu_entidadepai = p.id_entidade UNION SELECT
	vw.*
FROM vw_produto AS vw
JOIN tb_produto AS p
	ON (vw.id_produto = p.id_produto) AND p.bl_todasentidades = 0
JOIN tb_produtoentidade AS pe
	ON pe.id_produto = p.id_produto UNION SELECT
	vw.*
FROM vw_produto AS vw
JOIN tb_produto AS p
	ON (vw.id_produto = p.id_produto)
GO
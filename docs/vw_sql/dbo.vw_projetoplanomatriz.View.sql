
GO
/****** Object:  View [dbo].[vw_projetoplanomatriz]    Script Date: 10/08/2012 17:42:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_projetoplanomatriz] AS
-- View que tras o plano de pagamento de um projeto cadastrado em sua matriz--
SELECT pp.id_projetopedagogico, ppg.id_planopagamento,ppg.bl_padrao ,
        ppg.nu_valorparcela ,
        ppg.nu_valorentrada ,
        ppg.nu_parcelas ,
        ppg.id_produto FROM dbo.tb_projetopedagogico AS pp
JOIN dbo.tb_produtoprojetopedagogico AS ppp ON ppp.id_projetopedagogico = pp.id_projetopedagogico AND ppp.id_entidade = pp.id_entidadecadastro
JOIN dbo.tb_planopagamento AS ppg ON ppg.id_produto = ppp.id_produto
GO

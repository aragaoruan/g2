
GO
/****** Object:  View [dbo].[vw_entidadecontato]    Script Date: 10/08/2012 17:41:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_entidadecontato] AS
select ent.id_entidade, cent.id_contatoentidade,cent.st_funcao, cent.id_usuario, cent.id_entidadecontato, u.st_cpf, u.st_nomecompleto
from  tb_entidade ent, tb_contatoentidade cent, tb_usuario u
where ent.id_entidade = cent.id_entidade and
	  cent.id_usuario = u.id_usuario
GO

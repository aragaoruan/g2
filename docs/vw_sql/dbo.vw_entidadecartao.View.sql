
GO
/****** Object:  View [dbo].[vw_entidadecartao]    Script Date: 10/08/2012 17:41:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_entidadecartao] AS
SELECT ef.id_entidade, cc.id_cartaoconfig,cc.id_cartaobandeira, cb.st_cartaobandeira, cc.id_cartaooperadora, co.st_cartaooperadora, cc.st_contratooperadora, cc.st_gateway, si.id_sistema, si.st_sistema  FROM dbo.tb_entidadefinanceiro AS ef
JOIN dbo.tb_entidadecartaoconfig AS ecc ON ecc.id_entidadefinanceiro = ef.id_entidadefinanceiro
JOIN dbo.tb_cartaoconfig AS cc ON cc.id_cartaoconfig = ecc.id_cartaoconfig
JOIN dbo.tb_cartaobandeira AS cb ON cb.id_cartaobandeira = cc.id_cartaobandeira
JOIN dbo.tb_cartaooperadora AS co ON co.id_cartaooperadora = cc.id_cartaooperadora
JOIN tb_sistema AS si ON si.id_sistema = cc.id_sistema
GO

/****** Object:  View [dbo].[vw_categoria]    Script Date: 23/07/2015 16:54:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[vw_categoria]
AS
   SELECT DISTINCT
            c.id_categoria ,
            c.st_categoria ,
            c.bl_ativo ,
            c.dt_cadastro ,
            c.id_categoriapai ,
            cp.st_categoria AS st_categoriapai ,
            c.id_situacao ,
            st.st_situacao ,
            ce.id_entidade ,
            c.id_entidadecadastro
    FROM    dbo.tb_categoria AS c
            LEFT JOIN dbo.tb_categoriaentidade AS ce ON ce.id_categoria = c.id_categoria
            JOIN dbo.tb_situacao AS st ON st.id_situacao = c.id_situacao
            LEFT JOIN dbo.tb_categoria AS cp ON cp.id_categoria = c.id_categoriapai
GO


CREATE VIEW [dbo].[vw_matriculanotavinculada]
AS
  SELECT
      bl_calcular =
                  CASE
                  WHEN acr.id_avaliacao IS NOT NULL AND aa.st_nota IS NULL
                    THEN 0
                  ELSE 1
                  END,

      bl_aprovado =
                  CASE
                  WHEN aa.st_nota IS NOT NULL AND (ISNULL(CAST(aa.st_nota AS INT), 0)
                                                  ) >= CAST(((pp.nu_notamaxima
                                                              * pp.nu_percentualaprovacao)
                                                             / 100) AS INT)
                    THEN 1
                  WHEN aa.st_nota IS NOT NULL AND (ISNULL(CAST(aa.st_nota AS INT), 0)
                                                  ) < CAST(((pp.nu_notamaxima
                                                             * pp.nu_percentualaprovacao)
                                                            / 100) AS INT)
                    THEN 0
                  WHEN aa.st_nota IS NOT NULL AND ac.id_tipocalculoavaliacao = 2
                       AND ((ISNULL(CAST(aa.st_nota AS INT), 0)
                            ) / 2) >= CAST(((pp.nu_notamaxima * pp.nu_percentualaprovacaovariavel)
                                            / 100) AS INT)
                    THEN 1
                  WHEN aa.st_nota IS NOT NULL AND (ISNULL(CAST(aa.st_nota AS INT), 0)
                                                  ) < CAST(((pp.nu_notamaxima * pp.nu_percentualaprovacaovariavel)
                                                            / 100) AS INT)
                    THEN 0
                  ELSE 0
                  END,

    (CASE
     -- Se tiver RECUPERACAO e o tipo de calculo for MEDIA VARIAVEL, somar todas as notas e dividir por 2
     WHEN aa.st_nota IS NOT NULL AND ac.id_tipocalculoavaliacao = 2 THEN (
       ( ISNULL(CAST(aa.st_nota AS INT), 0)) / 2
     ) ELSE (
      ( ISNULL(CAST(aa.st_nota AS INT), 0)
      )
    )
     END)
    -- trata porque esse valor pode estar salvo como 0
    / (CASE WHEN pp.nu_notamaxima IS NULL OR pp.nu_notamaxima = 0 THEN 1 ELSE pp.nu_notamaxima END) * 100
      AS nu_percentualfinal,


md.id_matriculadisciplina,
    aa.st_nota as st_notafinal,
    dc.id_disciplina,
    mt.id_matricula

  FROM tb_matriculadisciplina AS md
    JOIN tb_matricula AS mt ON md.id_matriculaoriginal = mt.id_matricula
    JOIN dbo.tb_usuario AS us ON us.id_usuario = mt.id_usuario
    JOIN tb_disciplina AS dc ON dc.id_disciplina = md.id_disciplina
    JOIN tb_alocacao AS alc ON alc.id_matriculadisciplina = md.id_matriculadisciplina
                               AND alc.bl_ativo = 1
    JOIN tb_projetopedagogico AS PP ON pp.id_projetopedagogico = mt.id_projetopedagogico
    JOIN tb_avaliacaoconjuntoreferencia AS acrf ON acrf.id_saladeaula = alc.id_saladeaula
                                                   AND acrf.dt_fim IS NULL
    JOIN tb_evolucao AS sdis ON sdis.id_evolucao = md.id_evolucao
    JOIN tb_avaliacaoconjunto AS ac ON ac.id_avaliacaoconjunto = acrf.id_avaliacaoconjunto
                                       AND id_tipoprova = 2
    JOIN tb_avaliacaoconjuntorelacao AS acr ON acr.id_avaliacaoconjunto = ac.id_avaliacaoconjunto
    JOIN tb_avaliacao AS av ON av.id_avaliacao = acr.id_avaliacao
    JOIN tb_tipoavaliacao AS ta ON ta.id_tipoavaliacao = av.id_tipoavaliacao
    LEFT JOIN tb_avaliacaoaluno AS aa ON aa.id_matricula = mt.id_matricula
                                         AND aa.id_avaliacaoconjuntoreferencia = acrf.id_avaliacaoconjuntoreferencia
                                         AND aa.id_avaliacao = av.id_avaliacao
                                         AND aa.bl_ativo = 1
                                         AND aa.id_situacao = 86
    JOIN tb_situacao AS st ON st.id_situacao = md.id_situacao
    JOIN tb_evolucao AS ev ON ev.id_evolucao = md.id_evolucao
  WHERE mt.bl_ativo = 1 AND dc.id_tipodisciplina <> 3;
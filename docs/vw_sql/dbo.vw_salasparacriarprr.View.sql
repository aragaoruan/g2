
GO

/****** Object:  View [dbo].[vw_salasparacriarprr]    Script Date: 09/07/2014 13:06:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 CREATE VIEW [dbo].[vw_salasparacriarprr]
AS
 select  distinct top 5 ve.id_disciplina,
ve.st_disciplina,
'PRR - ' + ve.st_disciplina + ' (' + cast(projeto.id_projetopedagogico as varchar) + ')' as st_saladeaula,
cast(getdate() as date) as dt_inicioinscricao,
null as dt_fiminscricao,
cast(getdate() as date) as dt_abertura,
null as dt_encerramento,
2 as id_categoriasala,
2 as id_modalidadesaladeaula,
1 as id_tiposaladeaula,
null as id_periodoletivo,
0 as bl_usardoperiodoletivo,
8 as id_situacao,
999 as nu_maxalunos,
0 as nu_diasaluno,
0 as nu_diasextensao,
0 as nu_diasencerramento,
1 as bl_semencerramento,
0 as bl_semdiasaluno,
(case when ve.id_tipodisciplina = 1 then 11 else 13 end) as id_avaliacaoconjunto,
projeto.id_projetopedagogico,
ve.id_entidade 
from 
vw_entidadeprojetodisciplina as ve
outer apply (select distinct tp.id_projetopedagogico from tb_modulodisciplina tm 
JOIN tb_modulo as tm1
  ON tm1.id_modulo = tm.id_modulo
  AND tm1.bl_ativo = 1
JOIN tb_projetopedagogico as tp
  ON tm1.id_projetopedagogico = tp.id_projetopedagogico and tp.id_situacao = 7
JOIN tb_projetoentidade as tp1 
  ON tp.id_projetopedagogico = tp1.id_projetopedagogico and tp1.id_entidade=ve.id_entidade
 WHERE tm.bl_ativo = 1
and tm.id_disciplina=ve.id_disciplina
and tp.id_projetopedagogico is not null) as projeto

where ve.id_disciplina not in (select id_disciplina from vw_saladeaula where id_categoriasala=2 and bl_ativa=1 and id_projetopedagogico=projeto.id_projetopedagogico and id_entidade=ve.id_entidade) AND ve.bl_ativo = 1
and ve.id_entidade=14
and ve.st_disciplina not like '%ambientação%'
and ve.bl_ativo=1


GO



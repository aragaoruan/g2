
GO

/****** Object:  View [dbo].[vw_pesquisaavaliacaoaplicacao]    Script Date: 10/12/2013 11:05:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



 CREATE VIEW [dbo].[vw_pesquisaavaliacaoaplicacao] as

select
ap.id_avaliacaoaplicacao,
ap.nu_maxaplicacao, 
ap.id_horarioaula,
ende.sg_uf, 
ap.id_aplicadorprova, 
apl.st_aplicadorprova, 
ent.id_entidade,
ap.id_usuariocadastro,
ap.dt_aplicacao, 
ap.bl_unica,
ap.dt_cadastro,
ap.dt_alteracaolimite, 
ap.dt_antecedenciaminima,
ap.bl_ativo, 
ha.st_horarioaula, 
ende.id_endereco,
ende.st_endereco
from tb_avaliacaoaplicacao as ap
LEFT JOIN tb_aplicadorprova as apl on apl.id_aplicadorprova = ap.id_aplicadorprova
LEFT JOIN   tb_endereco as ende on ende.id_endereco = ap.id_endereco
JOIN tb_horarioaula as ha ON ha.id_horarioaula = ap.id_horarioaula
JOIN tb_aplicadorprovaentidade as aple on aple.id_aplicadorprova=apl.id_aplicadorprova
JOIN tb_entidade as ent on ent.id_entidade = aple.id_entidade




GO



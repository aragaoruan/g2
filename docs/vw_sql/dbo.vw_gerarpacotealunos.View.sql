select mt.id_matricula
,pp.id_projetopedagogico
,it.id_itemdematerial
,it.id_upload
,it.id_situacao as id_situacaoitem
,md.id_matriculadisciplina
,id.id_disciplina
,mt.id_entidadeatendimento as id_entidade
,a.id_areaconhecimento

from 
tb_matricula mt
join tb_projetopedagogico pp on pp.id_projetopedagogico=mt.id_projetopedagogico
join tb_areaprojetopedagogico app on app.id_projetopedagogico = pp.id_projetopedagogico
join tb_areaconhecimento a on a.id_areaconhecimento = app.id_areaconhecimento 
join tb_matriculadisciplina md on md.id_matricula=mt.id_matricula and id_pacote is null
join tb_itemdematerialdisciplina as id on id.id_disciplina=md.id_disciplina
join tb_itemdematerial as it on it.id_itemdematerial=id.id_itemdematerial AND it.id_sit
uacao = 35
JOIN dbo.tb_materialprojeto AS mp ON mp.id_itemdematerial = it.id_itemdematerial AND mp.id_projetopedagogico = pp.id_projetopedagogico
where mt.id_evolucao=6

GO

/****** Object:  View [dbo].[vw_primeiroacesso]    Script Date: 12/17/2013 10:51:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 CREATE VIEW [dbo].[vw_primeiroacesso]
AS
SELECT DISTINCT
	pa.id_venda,
	isnull(dt_aceita, getdate()) as dt_aceita,
	v.id_entidade,
        c.id_contrato,
	u.id_usuario, 
	u.st_login,
	u.st_nomecompleto,
	u.st_cpf,
	
	di.st_rg,
	di.st_orgaoexpeditor,
	di.dt_dataexpedicao,
	
	p.dt_nascimento,
	
	cepp.id_email,
	ce.st_email,
	
	p.sg_uf AS sg_ufnascimento,
	p.id_municipio AS id_municipionascimento,
	p.st_cidade AS st_cidadenascimento,
	
	ctp.id_telefone,
	ct.nu_telefone,
	ct.nu_ddd,
	ct.nu_ddi,
        
        ctp2.id_telefone as id_telefonealternativo,
	ct2.nu_telefone as nu_telefonealternativo,
	ct2.nu_ddd as nu_dddalternativo,
	ct2.nu_ddi as nu_ddialternativo,
	
	pe.id_endereco,
	e.st_endereco,
	e.st_cep,
	e.st_bairro,
	e.st_complemento,
	e.nu_numero,
	e.st_estadoprovincia,
	
	e.id_pais,
	e.id_municipio,
	e.sg_uf,
	pais.st_nomepais,
	pais.st_nacionalidade,
	mu.st_nomemunicipio,
	
	pec.id_endereco as id_enderecocorresp,
	pec.st_endereco as st_enderecocorresp,
	pec.st_cep as st_cepcorresp,
	pec.st_bairro as st_bairrocorresp,
	pec.st_complemento as st_complementocorresp,
	pec.nu_numero as nu_numerocorresp,
	pec.st_estadoprovincia as st_estadoprovinciacorresp,
	pec.id_pais as id_paiscorresp,
	pec.id_municipio as id_municipiocorresp,
	pec.sg_uf as sg_ufcorresp,
	pec.st_nomepais as st_nomepaiscorresp,
	pec.st_nomemunicipio as st_nomemunicipiocorresp
	
FROM tb_venda v 
LEFT JOIN tb_pa_marcacaoetapa AS pa
	ON v.id_venda = pa.id_venda
LEFT JOIN tb_contrato AS c
        ON v.id_venda = c.id_venda
LEFT JOIN tb_usuario AS u
	ON v.id_usuario = u.id_usuario
INNER JOIN tb_pessoa AS p
	ON p.id_usuario = u.id_usuario AND p.bl_ativo = 1 AND v.id_entidade = p.id_entidade
LEFT JOIN tb_contatosemailpessoaperfil AS cepp
	ON cepp.id_usuario = u.id_usuario AND cepp.id_entidade = p.id_entidade AND cepp.bl_ativo = 1 AND cepp.bl_padrao = 1
LEFT JOIN tb_contatosemail AS ce
	ON ce.id_email = cepp.id_email
LEFT JOIN tb_contatostelefonepessoa AS ctp
	ON ctp.id_usuario = u.id_usuario AND ctp.id_entidade = v.id_entidade AND ctp.bl_padrao = 1
LEFT JOIN tb_contatostelefonepessoa AS ctp2
	ON ctp2.id_usuario = u.id_usuario AND ctp2.id_entidade = v.id_entidade AND ctp2.bl_padrao = 0
LEFT JOIN tb_contatostelefone AS ct
	ON ct.id_telefone = ctp.id_telefone
LEFT JOIN tb_contatostelefone AS ct2
	ON ct2.id_telefone = ctp2.id_telefone
OUTER APPLY(SELECT TOP 1 * from vw_pessoaendereco 
	WHERE id_usuario = u.id_usuario AND id_entidade = p.id_entidade AND bl_padrao = 1 AND id_tipoendereco = 1) AS pe

OUTER APPLY(SELECT TOP 1 * from vw_pessoaendereco 
	WHERE id_usuario = u.id_usuario AND id_entidade = p.id_entidade AND bl_padrao = 1 AND id_tipoendereco = 5) AS pec

LEFT JOIN tb_endereco AS e
	ON e.id_endereco = pe.id_endereco AND p.bl_ativo = 1
LEFT JOIN tb_municipio AS mu
	ON mu.id_municipio = e.id_municipio
LEFT JOIN tb_tipotelefone AS tt
	ON tt.id_tipotelefone = ct.id_tipotelefone
LEFT JOIN tb_tipotelefone AS tt2
	ON tt2.id_tipotelefone = ct2.id_tipotelefone
LEFT JOIN dbo.tb_documentoidentidade AS di
	ON di.id_usuario = u.id_usuario AND di.id_entidade = p.id_entidade
LEFT JOIN dbo.tb_pais AS pais
	ON pais.id_pais = e.id_pais

GO

-- select * from tb_projetopedagogicointegracao;
-- select * from tb_horarioaula;
-- select * from tb_itemgradehorariaturma
-- update tb_gradehoraria set bl_sincronizada = 0
CREATE view dbo.vw_gradepontosoft as
select row_number() over (order by st_turmasistema asc) as id, s1.id_projetopedagogicosistema,
	s1.st_turmasistema,
	s1.dt_iniciogradehoraria,
	s1.id_codhorarioacesso,
	s1.id_diasemana,
	s1.st_codigocatraca,
	s1.id_gradehoraria,
	s1.id_turma,
	s1.dt_inicio
from (select 
	ppi.id_projetopedagogicosistema,
	ti.st_turmasistema,
	gh.dt_iniciogradehoraria,
	ha.id_codhorarioacesso,
	ds.id_diasemana,
	cat.st_codigocatraca,
	gh.id_gradehoraria,
	tm.id_turma,
	tm.dt_inicio
FROM dbo.tb_gradehoraria as gh
JOIN dbo.tb_itemgradehoraria AS it ON it.id_gradehoraria = gh.id_gradehoraria AND gh.bl_ativo = 1 AND gh.id_situacao = 149
JOIN dbo.tb_itemgradehorariaturma as ight ON ight.id_itemgradehoraria = it.id_itemgradehoraria
JOIN tb_entidade e ON e.id_entidade = it.id_unidade
JOIN tb_turno AS tn ON tn.id_turno = it.id_turno
JOIN tb_horarioaula as ha on ha.id_turno = tn.id_turno and ha.id_entidade = e.id_entidade
JOIN tb_localaula lc ON lc.id_localaula = it.id_localaula
JOIN tb_catraca as cat ON cat.id_catraca = lc.id_catraca
JOIN tb_diasemana ds ON ds.id_diasemana = it.id_diasemana
JOIN tb_disciplina dp ON dp.id_disciplina = it.id_disciplina  and dp.bl_ativa =1 and dp.id_situacao = 9
JOIN tb_turma tm ON tm.id_turma = ight.id_turma  and tm.bl_ativo = 1  and tm.id_situacao = 83 and tm.id_evolucao = 46
JOIN tb_turmaintegracao as ti on ti.id_turma = tm.id_turma 
JOIN tb_turmaprojeto as tp on tp.id_turma = tm.id_turma
JOIN tb_projetopedagogico AS pp on tp.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_projetopedagogicointegracao as ppi on pp.id_projetopedagogico = ppi.id_projetopedagogico
JOIN tb_entidadeintegracao as ein on ein.id_entidade = e.id_entidade and ein.id_sistema = 20
WHERE gh.bl_sincronizado = 0 AND GETDATE()  BETWEEN cast(gh.dt_iniciogradehoraria as DATE) AND LEFT(CONVERT(nvarchar, dt_fimgradehoraria, 120), 11) + N'23:59:59') as s1
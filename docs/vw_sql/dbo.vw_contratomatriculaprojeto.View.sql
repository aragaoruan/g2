
GO
/****** Object:  View [dbo].[vw_contratomatriculaprojeto]    Script Date: 10/08/2012 17:41:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/**
Autor: Elcio Mauro Guimarães
Data: 15/05/2012


*/

CREATE VIEW [dbo].[vw_contratomatriculaprojeto] AS


select 
  con.id_contrato
, mat.id_matricula
, mat.id_projetopedagogico
, con.id_entidade

from tb_matricula as mat
join tb_contratomatricula as cm on (mat.id_matricula = cm.id_matricula)
join tb_contrato as con on (con.id_contrato = cm.id_contrato)
WHERE con.bl_ativo = 1 and cm.bl_ativo = 1 and mat.bl_ativo = 1
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_avaliacaodisciplina] AS
SELECT ad.*, a.st_avaliacao, a.id_tipoavaliacao, a.nu_valor, a.id_situacao, d.st_disciplina, d.st_descricao, d.st_tituloexibicao FROM 
tb_avaliacaodisciplina ad
JOIN tb_avaliacao a ON a.id_avaliacao = ad.id_avaliacao
JOIN tb_disciplina d ON d.id_disciplina = ad.id_disciplina
GO

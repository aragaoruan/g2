
GO

/****** Object:  View [dbo].[vw_provaspolo]    Script Date: 27/01/2014 13:51:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




 CREATE VIEW [dbo].[vw_provaspolo] as
SELECT
	avp.id_avaliacaoaplicacao
	, ap.id_aplicadorprova
	, ap.st_aplicadorprova
	, CONVERT( VARCHAR(30), avp.dt_aplicacao ,103) AS st_dtaplicacao
	, avp.dt_aplicacao
	, LEFT(CONVERT(VARCHAR,ha.hr_inicio,108),5) as hr_inicio
	, LEFT(CONVERT(VARCHAR,ha.hr_fim,108),5) as hr_fim
	, avp.nu_maxaplicacao as nu_vagasexistentes
	, (SELECT COUNT( id_avaliacaoagendamento) FROM tb_avaliacaoagendamento where id_avaliacaoaplicacao = avp.id_avaliacaoaplicacao AND id_situacao = 68) as total_agendamentos
	, (SELECT COUNT(id_tipodeavaliacao) FROM tb_avaliacaoagendamento where id_avaliacaoaplicacao = avp.id_avaliacaoaplicacao AND id_tipodeavaliacao = 0 AND id_situacao = 68) as nu_provafinal
	, (SELECT COUNT(id_tipodeavaliacao) FROM tb_avaliacaoagendamento where id_avaliacaoaplicacao = avp.id_avaliacaoaplicacao AND id_tipodeavaliacao >= 1 AND id_situacao = 68) as nu_provarecuperacao
	, (avp.nu_maxaplicacao - (SELECT COUNT( id_avaliacaoagendamento) FROM tb_avaliacaoagendamento where id_avaliacaoaplicacao = avp.id_avaliacaoaplicacao AND id_situacao = 68)) as nu_restantes
	,ha.id_entidade
	--, avp.*
	 FROM tb_avaliacaoaplicacao as avp
			INNER JOIN tb_aplicadorprova AS ap on ap.id_aplicadorprova = avp.id_aplicadorprova
			INNER JOIN tb_horarioaula AS ha on ha.id_horarioaula = avp.id_horarioaula 
			
			--WHERE ap.id_aplicadorprova = 1
			--AND avp.dt_aplicacao between ('2013-12-20') AND ('2014-01-24') 
			--AND ha.id_entidade = 14
		
        



GO



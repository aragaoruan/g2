CREATE VIEW vw_ocorrenciaspessoageral AS
  SELECT
    oc.st_nomeinteressado,
    oc.st_email AS st_emailinteressado,
    oc.st_telefone AS st_telefoneinteressado,
    oc.st_projetopedagogico,
    usco.st_nomecompleto AS st_coordenadortitular,
    NULL AS st_nomecompleto,
    np.id_nucleoco,
    oc.id_assuntoco,
    ac.id_assuntocopai,
    np.id_usuario,
    np.id_funcao,
    np.bl_prioritario,
    nc.id_tipoocorrencia,
    nc.id_entidade,
    oc.id_situacao,
    st_nucleoco,
    nc.bl_ativo,
    st_funcao,
    oc.id_ocorrencia,
    oc.id_matricula,
    mt.id_entidadematricula,
    en.st_nomeentidade AS st_entidadematricula,
    oc.st_urlportal,
    oc.id_evolucaomatricula,
    oc.id_evolucao,
    id_usuariointeressado,
    oc.id_categoriaocorrencia,
    oc.id_saladeaula,
    st_titulo,
    st_ocorrencia,
    oc.st_situacao,
    oc.st_evolucao,
    oc.id_ocorrenciaoriginal,
    oc.dt_cadastro AS dt_cadastroocorrencia,
    (CAST(CONVERT(VARCHAR, oc.dt_cadastro, 103) + ' às '
          + CONVERT(VARCHAR, oc.dt_cadastro, 108) AS VARCHAR(MAX))) AS st_cadastroocorrencia,
    oc.st_assuntoco,
    oc.st_assuntocopai,
    oc.id_usuarioresponsavel AS id_atendente,
    oc.st_nomeresponsavel AS st_atendente,
    st_categoriaocorrencia,
    st_ultimotramite,
    dt_ultimotramite,
    oc.st_cpf,
      nu_horastraso =
      CASE
      WHEN (dbo.fn_horasdiasuteis(dt_ultimotramite,
                                  GETDATE())
            - nc.nu_horasmeta) > 0
        THEN (dbo.fn_horasdiasuteis(dt_ultimotramite,
                                    GETDATE())
              - nc.nu_horasmeta)
      ELSE 0
      END,
      nu_porcentagemmeta = 0,
      st_cor =
      CASE
      WHEN oc.id_evolucao = 39
        THEN nc.st_cordevolvida
      WHEN oc.id_evolucao = 32
        THEN nc.st_corinteracao
      WHEN ((dbo.fn_horasdiasuteis(dt_ultimotramite,
                                   GETDATE())
             - nc.nu_horasmeta) >= 0 AND oc.id_situacao <> 99)
        THEN nc.st_coratrasada
      ELSE
        CASE
        WHEN oc.id_situacao = 99
          THEN ''
        ELSE
          nc.st_cordefault
        END
      END,
      st_status =
      CASE
      WHEN oc.id_evolucao = 39
        THEN 'Devolvida para Ditribuição'
      WHEN oc.id_evolucao = 32
        THEN 'Interesado Interagiu'
      WHEN ((dbo.fn_horasdiasuteis(dt_ultimotramite,
                                   GETDATE())
             - nc.nu_horasmeta) >= 0)
        THEN 'Atrasada'
      ELSE ''
      END,
      nu_ordem =
      CASE
      WHEN oc.id_evolucao = 39
        THEN 1
      WHEN oc.id_evolucao = 37
        THEN 2
      WHEN oc.id_evolucao = 28
        THEN 3
      ELSE 4
      END,
    ac.bl_cancelamento,
    ac.bl_trancamento,
    oc.dt_atendimento,
    DATEDIFF(DAY, CAST(oc.dt_cadastro AS DATE),
             CAST(GETDATE() AS DATE)) AS nu_diasaberto,
      bl_resgate =
      CASE
      WHEN tc.id_entidade IS NOT NULL
        THEN 1
      ELSE 0
      END,
      st_urlacesso =
      CASE
      WHEN oc.id_matricula IS NOT NULL
        THEN dbo.fn_urlacesso(oc.id_usuariointeressado, oc.id_matricula, oc.st_login, oc.st_senha, 0)
      ELSE NULL
      END,
    sa.st_saladeaula,
    tu.st_tituloexibicao
  FROM dbo.tb_nucleopessoaco AS np
    JOIN dbo.tb_nucleoco AS nc
      ON nc.id_nucleoco = np.id_nucleoco
    JOIN dbo.tb_nucleoassuntoco AS na
      ON na.id_nucleoco = nc.id_nucleoco
         AND na.bl_ativo = 1
    JOIN dbo.tb_funcao AS fc
      ON fc.id_funcao = np.id_funcao
    JOIN dbo.tb_assuntoco AS ac
      ON ac.id_assuntoco = na.id_assuntoco
    JOIN dbo.vw_ocorrencia AS oc
      ON oc.id_assuntoco = ac.id_assuntoco
         AND (oc.id_entidade = nc.id_entidade)
    LEFT JOIN dbo.tb_matricula AS mt
      ON mt.id_matricula = oc.id_matricula
    LEFT JOIN tb_entidade AS en
      ON en.id_entidade = mt.id_entidadematricula
    LEFT JOIN dbo.tb_configuracaoentidade AS tc
      ON tc.id_entidade = oc.id_entidade
         AND oc.id_assuntoco = tc.id_assuntoresgate
    LEFT JOIN dbo.tb_saladeaula AS sa
      ON sa.id_saladeaula = oc.id_saladeaula
    LEFT JOIN dbo.tb_turma AS tu
      ON tu.id_turma = mt.id_turma
    LEFT JOIN tb_projetopedagogico pp
      ON pp.id_projetopedagogico = oc.id_projetopedagogico
    OUTER APPLY (SELECT
                   u2.st_nomecompleto
                 FROM tb_usuarioperfilentidadereferencia upr
                   JOIN tb_perfil AS per ON per.id_perfil = upr.id_perfil AND per.id_perfilpedagogico = 2
                   JOIN tb_usuario u2 ON upr.id_usuario = u2.id_usuario
                 WHERE pp.id_projetopedagogico = upr.id_projetopedagogico
                       AND upr.bl_titular = 1
                       AND upr.id_entidade = pp.id_entidadecadastro
                       AND upr.bl_ativo = 1
                ) AS usco
  WHERE np.bl_todos = 1

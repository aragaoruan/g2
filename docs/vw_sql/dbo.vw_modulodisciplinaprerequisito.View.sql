
GO
/****** Object:  View [dbo].[vw_modulodisciplinaprerequisito]    Script Date: 10/08/2012 17:42:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_modulodisciplinaprerequisito] as
select 
    m.id_projetopedagogico,
    tt.id_tipotrilha,
    tt.st_tipotrilha,
    md.id_modulo,m.st_tituloexibicao as st_modulotituloexibicao,
    md.id_disciplina,td.id_disciplinaprerequisito,
    CASE WHEN d.bl_ativa = 1 THEN 
    d.st_tituloexibicao ELSE d.st_tituloexibicao + ' (Inativa)' END as st_disciplinatituloexibicao,
    md.id_serie,
    s.st_serie, 
    ne.id_nivelensino ,
    ne.st_nivelensino
from
tb_modulodisciplina as md 
INNER JOIN tb_serie as s on md.id_serie = s.id_serie
INNER JOIN tb_nivelensino as ne on md.id_nivelensino = ne.id_nivelensino
INNER JOIN tb_modulo as m on md.id_modulo = m.id_modulo
INNER JOIN tb_projetopedagogico as pp ON pp.id_projetopedagogico = m.id_projetopedagogico
INNER JOIN tb_trilha as t ON pp.id_trilha = t.id_trilha
INNER JOIN tb_tipotrilha as tt ON t.id_tipotrilha = tt.id_tipotrilha
INNER JOIN tb_disciplina as d on d.id_disciplina = md.id_disciplina
LEFT JOIN tb_trilhadisciplina as td on md.id_disciplina = td.id_disciplina  AND pp.id_trilha = td.id_trilha
GO

/****** Object:  View [dbo].[vw_entidaderelacao]    Script Date: 16/12/2014 11:21:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[vw_entidaderelacao] AS
  SELECT
    e.id_entidade,
    e.st_nomeentidade,
    er.id_entidadepai,
    e.bl_ativo,
    e.id_situacao,
    er.id_entidadeclasse,
    ee.st_tipoendereco,
    ee.st_endereco,
    ee.st_complemento,
    ee.nu_numero,
    ee.st_cidade,
    ee.st_nomemunicipio,
    ee.sg_uf,
    ee.st_cep,
    ee.st_bairro,
    e.st_cnpj,
    e.st_razaosocial,
    e.st_wschave,
    ee.id_municipio,
    uf.st_uf
  FROM dbo.tb_entidade e
    JOIN dbo.tb_entidaderelacao er
      ON er.id_entidade = e.id_entidade AND e.bl_ativo = 1
    LEFT JOIN dbo.vw_entidadeendereco AS ee
      ON e.id_entidade = ee.id_entidade
    LEFT JOIN tb_uf AS uf
      ON uf.sg_uf = ee.sg_uf

GO



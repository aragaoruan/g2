CREATE view vw_gerararquivodeenvio as
select 
p.id_pacote,
REPLACE(REPLACE(vw.st_cpf,'.',''),'-','')  as st_cpf,
vw.st_nomecompleto,
vw.st_endereco,
vw.nu_numero,
vw.st_complemento,
vw.st_bairro,
vw.st_nomemunicipio,
vw.sg_uf,
REPLACE(REPLACE(vw.st_cep,'.',''),'-','') as st_cep,
em.id_itemdematerial,
it.st_itemdematerial,
d.id_disciplina,
d.st_disciplina,
pp.id_projetopedagogico,
pp.st_projetopedagogico,
area.id_areaconhecimento,
area.st_areaconhecimento,
mt.id_entidadeatendimento as id_entidade,
ent.st_nomeentidade,
lm.id_lotematerial,
cast(p.dt_cadastro as date) as dt_criacaopacote
from tb_lotematerial as lm
join tb_pacote as p on p.id_lotematerial = lm.id_lotematerial and p.id_situacao=155
join tb_entregamaterial as em on em.id_pacote = p.id_pacote
join tb_itemdematerial as it on it.id_itemdematerial=em.id_itemdematerial
join tb_matriculadisciplina as md on md.id_matriculadisciplina = em.id_matriculadisciplina and md.id_pacote = p.id_pacote
join tb_disciplina as d on d.id_disciplina = md.id_disciplina
join tb_matricula as mt on mt.id_matricula=em.id_matricula and mt.id_evolucao=6
join tb_entidade as ent on ent.id_entidade = mt.id_entidadeatendimento
join tb_projetopedagogico as pp on pp.id_projetopedagogico=mt.id_projetopedagogico
join tb_areaprojetopedagogico as ar on ar.id_projetopedagogico=pp.id_projetopedagogico
join tb_areaconhecimento as area on area.id_areaconhecimento=ar.id_areaconhecimento --and area.id_entidade = mt.id_entidadeatendimento
join vw_pessoa as vw on vw.id_usuario = mt.id_usuario and vw.id_entidade=mt.id_entidadeatendimento


GO



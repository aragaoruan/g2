CREATE VIEW dbo.vw_recuperaPolosComProjetoPedagogico AS

  SELECT e.id_entidadecadastro, e.id_entidade,e.st_nomeentidade, pp.id_projetopedagogico, pp.st_projetopedagogico
  FROM tb_entidade AS e
    inner join tb_projetoentidade AS pe ON e.id_entidade = pe.id_entidade
    inner join tb_projetopedagogico AS pp ON pe.id_projetopedagogico = pp.id_projetopedagogico
CREATE VIEW [rel].[vw_atendentenegociacao]
AS
SELECT
	U.st_nomecompleto AS 'st_nomealuno',
	US.st_nomecompleto AS 'st_nomeatendente',
	US.id_usuario AS 'id_atendente',
	OP.id_usuario AS 'id_operador',
	OP.st_nomecompleto AS 'st_nomeoperador',
	vd.id_venda,
	p.st_produto,
	vd.nu_parcelas,
	vp.nu_valorliquido,
	CASE
		WHEN vd.nu_valorliquido > 0 THEN (vdlc.nu_valorcartao / vd.nu_valorliquido
			* vp.nu_valorliquido)
		ELSE 0
	END AS nu_valorcartao,
	CASE
		WHEN vd.nu_valorliquido > 0 THEN (vdlb.nu_valorboleto / vd.nu_valorliquido
			* vp.nu_valorliquido)
		ELSE 0
	END AS nu_valorboleto,
	CASE
		WHEN vd.nu_valorliquido > 0 THEN (vdld.nu_valordinheiro / vd.nu_valorliquido
			* vp.nu_valorliquido)
		ELSE 0
	END AS nu_valordinheiro,
	CASE
		WHEN vd.nu_valorliquido > 0 THEN (vdlch.nu_valorcheque / vd.nu_valorliquido
			* vp.nu_valorliquido)
		ELSE 0
	END AS nu_valorcheque,
	CASE
		WHEN vd.nu_valorliquido > 0 THEN (vdle.nu_valorempenho / vd.nu_valorliquido
			* vp.nu_valorliquido)
		ELSE 0
	END AS nu_valorempenho,
	CASE
		WHEN vd.nu_valorliquido > 0 THEN (vdldp.nu_valordeposito / vd.nu_valorliquido
			* vp.nu_valorliquido)
		ELSE 0
	END AS nu_valordeposito,
	CASE
		WHEN vd.nu_valorliquido > 0 THEN (vdlt.nu_valortransferencia / vd.nu_valorliquido
			* vp.nu_valorliquido)
		ELSE 0
	END AS nu_valortransferencia,
	CASE
		WHEN vd.nu_valorliquido > 0 THEN (vdlp.nu_valorpermuta / vd.nu_valorliquido
			* vp.nu_valorliquido)
		ELSE 0
	END AS nu_valorpermuta,
	CASE
		WHEN vd.nu_valorliquido > 0 THEN (vdlbl.nu_valorbolsa / vd.nu_valorliquido
			* vp.nu_valorliquido)
		ELSE 0
	END AS nu_valorbolsa,
	CASE
		WHEN vd.nu_valorliquido > 0 THEN (vdlec.nu_valorestorno / vd.nu_valorliquido
			* vp.nu_valorliquido)
		ELSE 0
	END AS nu_valorestorno,
	CASE
		WHEN vd.nu_valorliquido > 0 THEN (vdlcc.nu_valorcarta / vd.nu_valorliquido
			* vp.nu_valorliquido)
		ELSE 0
	END AS nu_valorcarta,
	E.st_nomeentidade,
	E.id_entidade,
	vd.dt_cadastro
FROM dbo.tb_venda AS vd
OUTER APPLY (
	SELECT
		SUM(nu_valor) AS nu_valorcartao
	FROM dbo.vw_vendalancamento
	WHERE id_venda = vd.id_venda
	AND id_meiopagamento IN (1, 7, 11)
) AS vdlc
OUTER APPLY (
	SELECT
		SUM(nu_valor) AS nu_valorboleto
	FROM dbo.vw_vendalancamento
	WHERE id_venda = vd.id_venda
	AND id_meiopagamento = 2
) AS vdlb
OUTER APPLY (
	SELECT
		SUM(nu_valor) AS nu_valordinheiro
	FROM dbo.vw_vendalancamento
	WHERE id_venda = vd.id_venda
	AND id_meiopagamento = 3
) AS vdld
OUTER APPLY (
	SELECT
		SUM(nu_valor) AS nu_valorcheque
	FROM dbo.vw_vendalancamento
	WHERE id_venda = vd.id_venda
	AND id_meiopagamento = 4
) AS vdlch
OUTER APPLY (
	SELECT
		SUM(nu_valor) AS nu_valorempenho
	FROM dbo.vw_vendalancamento
	WHERE id_venda = vd.id_venda
	AND id_meiopagamento = 5
) AS vdle
OUTER APPLY (
	SELECT
		SUM(nu_valor) AS nu_valordeposito
	FROM dbo.vw_vendalancamento
	WHERE id_venda = vd.id_venda
	AND id_meiopagamento = 6
) AS vdldp
OUTER APPLY (
	SELECT
		SUM(nu_valor) AS nu_valortransferencia
	FROM dbo.vw_vendalancamento
	WHERE id_venda = vd.id_venda
	AND id_meiopagamento = 8
) AS vdlt
OUTER APPLY (
	SELECT
		SUM(nu_valor) AS nu_valorpermuta
	FROM dbo.vw_vendalancamento
	WHERE id_venda = vd.id_venda
	AND id_meiopagamento = 9
) AS vdlp
OUTER APPLY (
	SELECT
		SUM(nu_valor) AS nu_valorbolsa
	FROM dbo.vw_vendalancamento
	WHERE id_venda = vd.id_venda
	AND id_meiopagamento = 10
) AS vdlbl
OUTER APPLY (
	SELECT
		SUM(nu_valor) AS nu_valorestorno
	FROM dbo.vw_vendalancamento
	WHERE id_venda = vd.id_venda
	AND id_meiopagamento = 12
) AS vdlec
OUTER APPLY (
	SELECT
		SUM(nu_valor) AS nu_valorcarta
	FROM dbo.vw_vendalancamento
	WHERE id_venda = vd.id_venda
	AND id_meiopagamento = 13
) AS vdlcc
JOIN dbo.tb_entidade AS E ON E.id_entidade = vd.id_entidade
JOIN dbo.tb_vendaproduto AS vp	ON vp.id_venda = vd.id_venda
-- Kayo Silva - Adicionei esse trecho buscando o contrato da venda e depois o vinculo
-- com a matricula para saber de fato qual a matricula pois o atalho que estava sendo
-- utilizado na tb_vendaproduto não estava sendo atualizado com o id_matricula
-- quando a matricula era transferida, causando o sumiço da venda no relatório
-- a exemplo da issue: FIN-17144
LEFT JOIN dbo.tb_contrato as ct on ct.id_venda = vd.id_venda and ct.id_evolucao = 4 and ct.id_situacao = 44 and ct.bl_ativo = 1
LEFT JOIN dbo.tb_contratomatricula as ctm on ctm.id_contrato = ct.id_contrato and ctm.bl_ativo = 1
LEFT JOIN dbo.tb_matricula AS mt	ON mt.id_matricula = ctm.id_matricula
-- JOIN dbo.tb_matricula AS mt	ON mt.id_matricula = vp.id_matricula
-- fim da alteração
LEFT JOIN dbo.tb_produto AS p ON p.id_produto = vp.id_produto AND mt.id_evolucao = 6
JOIN dbo.tb_usuario AS U ON U.id_usuario = vd.id_usuario LEFT JOIN dbo.tb_usuario AS US	ON US.id_usuario = vd.id_atendente
LEFT JOIN dbo.tb_usuario AS OP ON OP.id_usuario = vd.id_operador;


GO
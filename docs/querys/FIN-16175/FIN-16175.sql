INSERT INTO tb_textovariaveis VALUES (6, 'id_venda', '#lista_numerocheque#', 'gerarListaChequesDevolvidos', 5);

-- campo para identificar o textosistema usado pra listagem de cheques devolvidos
ALTER TABLE tb_entidadefinanceiro ADD id_textochequesdevolvidos INT NULL
  CONSTRAINT FK_tb_entidade_reference_tb_textochequesdevolvidos
  REFERENCES tb_textosistema (id_textosistema)

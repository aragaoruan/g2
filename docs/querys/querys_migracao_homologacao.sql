

INSERT  INTO dbo.tb_entidadeintegracao
        ( id_entidade ,
          id_usuariocadastro ,
          id_sistema ,
          st_codsistema ,
          dt_cadastro ,
          st_codchave ,
          st_codarea ,
          st_caminho ,
          nu_perfilsuporte ,
          nu_perfilprojeto ,
          nu_perfildisciplina ,
          nu_perfilobservador ,
          nu_perfilalunoobs ,
          nu_perfilalunoencerrado ,
          nu_contexto ,
          st_linkedserver
        )
        SELECT  id_entidade ,
                id_usuariocadastro ,
                id_sistema ,
                st_codsistema ,
                dt_cadastro ,
                st_codchave ,
                st_codarea ,
                st_caminho ,
                nu_perfilsuporte ,
                nu_perfilprojeto ,
                nu_perfildisciplina ,
                nu_perfilobservador ,
                nu_perfilalunoobs ,
                nu_perfilalunoencerrado ,
                nu_contexto ,
                st_linkedserver
        FROM    G2H_EAD1_Old.dbo.tb_entidadeintegracao AS te

SELECT  *
FROM    dbo.tb_saladeaulaintegracao AS ts

DELETE  dbo.tb_perfilreferenciaintegracao
DELETE  dbo.tb_saladeaulaintegracao

INSERT  INTO dbo.tb_saladeaulaintegracao
        ( id_saladeaula ,
          id_usuariocadastro ,
          id_sistema ,
          st_codsistemacurso ,
          st_codsistemasala ,
          st_codsistemareferencia ,
          dt_cadastro ,
          bl_conteudo ,
          bl_visivel
        )
        SELECT  id_saladeaula ,
                id_usuariocadastro ,
                id_sistema ,
                st_codsistemacurso ,
                st_codsistemasala ,
                st_codsistemareferencia ,
                dt_cadastro ,
                bl_conteudo ,
                bl_visivel
        FROM    G2H_EAD1_Old.dbo.tb_saladeaulaintegracao AS ts
        WHERE   ts.id_saladeaula IN ( SELECT    id_saladeaula
                                      FROM      dbo.tb_saladeaula AS ts2 )

INSERT  INTO dbo.tb_perfilreferenciaintegracao
        ( id_sistema ,
          id_usuariocadastro ,
          id_perfilreferencia ,
          id_saladeaulaintegracao ,
          st_codsistema ,
          dt_cadastro
        )
        SELECT  id_sistema ,
                id_usuariocadastro ,
                id_perfilreferencia ,
                id_saladeaulaintegracao ,
                st_codsistema ,
                dt_cadastro
        FROM    G2H_EAD1_Old.dbo.tb_perfilreferenciaintegracao AS tp
        WHERE   tp.id_saladeaulaintegracao IN (
                SELECT  id_saladeaulaintegracao
                FROM    dbo.tb_saladeaulaintegracao AS ts )
                OR tp.id_saladeaulaintegracao IS NULL


SELECT  *
FROM    dbo.tb_usuariointegracao AS tu
DELETE  dbo.tb_saladeaulaintegracao

INSERT  INTO dbo.tb_usuariointegracao
        ( id_sistema ,
          id_usuariocadastro ,
          id_usuario ,
          dt_cadastro ,
          st_codusuario ,
          st_senhaintegrada ,
          st_loginintegrado ,
          id_entidade
		          
        )
        SELECT  id_sistema ,
                id_usuariocadastro ,
                id_usuario ,
                dt_cadastro ,
                st_codusuario ,
                st_senhaintegrada ,
                st_loginintegrado ,
                id_entidade
        FROM    G2H_EAD1_Old.dbo.tb_usuariointegracao AS tu



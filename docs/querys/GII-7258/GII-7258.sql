--Script para criar a tabela dbo.tb_tipooferta
CREATE TABLE dbo.tb_tipooferta (
	id_tipooferta INT NOT NULL IDENTITY PRIMARY KEY,
	st_tipooferta VARCHAR(50) NOT NULL
);

--Script para inserir os dados básicos da tabela
SET IDENTITY_INSERT tb_tipooferta ON;
INSERT INTO dbo.tb_tipooferta (id_tipooferta, st_tipooferta) VALUES (1, 'Padrão'), (2, 'Estendida');
SET IDENTITY_INSERT tb_tipooferta OFF;

--Script para inserir uma nova coluna (id_tipooferta) na tabela dbo.tb_periodoletivo
ALTER TABLE dbo.tb_periodoletivo ADD id_tipooferta INT DEFAULT ((1)) NOT NULL;
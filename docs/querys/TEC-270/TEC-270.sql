-- ////////////// ALTERACOES ///////////////////////////////
-- alter table tb_lancamento add nu_cartao integer not null default 1;
-- atualizar a  vw_usuarioperfilentidade

/*
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tb_tiposelecao](
	[id_tiposelecao] [int] NOT NULL,
	[st_tiposelecao] [varchar](200) NOT NULL,
 CONSTRAINT [PK_ID_tiposelecao] PRIMARY KEY CLUSTERED 
(
	[id_tiposelecao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Define os tipos de acesso de um curso ex(PAD,PDS,ENEM)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tb_tiposelecao'
GO


insert into tb_tiposelecao values (1, 'PAD'), (2, 'PDS'), (3, 'ENEM');

alter table tb_vendaproduto add id_tiposelecao int null;
alter table tb_vendaproduto add FOREIGN KEY (id_tiposelecao) references tb_tiposelecao (id_tiposelecao);

*/

-- ATUALIZAR A vw_produtovenda




-- ////////////// FIM ALTERACOES ///////////////////////////////


-- //////////////////////////////////////// ignorar daqui para baixo /////////////////////////////////////////////////////////////////////////

/*
select * -- id_meiopagamento, nu_valor, nu_parcelas, nu_cartao, bl_entrada, nu_ordem, dt_vencimento, dt_prevquitado, dt_quitado, st_coddocumento 
from vw_vendalancamento 
where id_venda = 153660
order by nu_cartao, nu_ordem;

select * from tb_lancamento where id_lancamento = 1696977



-- select * from vw_vendalancamento where id_venda = 153633 order by nu_cartao, nu_ordem;



select * from tb_cartaobandeira;
select * from tb_entidadecartaoconfig;
select * from tb_cartaoconfig where id_contaentidade = 5;
insert into tb_cartaoconfig (id_cartaobandeira, id_cartaooperadora, id_contaentidade, st_gateway, id_sistema) values (2, 1, 5, 'braspag', 7);

select * from tb_sistema where id_sistema in (9, 10, 7);

select * from tb_entidadecartaoconfig where id_entidade = 14;

select * from tb_evolucao where st_tabela = 'tb_venda'



SELECT "cb"."id_cartaobandeira", "cb"."st_cartaobandeira" FROM "tb_cartaobandeira" AS "cb"
 INNER JOIN "tb_cartaoconfig" AS "cc" ON cb.id_cartaobandeira	= cc.id_cartaobandeira
 INNER JOIN "tb_contaentidade" AS "ce" ON ce.id_contaentidade = cc.id_contaentidade
 INNER JOIN "tb_entidadecartaoconfig" AS "ecc" ON ecc.id_cartaoconfig	= cc.id_cartaoconfig WHERE (ce.id_entidade = 14) AND (cc.id_sistema = 7);

 insert into tb_entidadecartaoconfig (id_entidadefinanceiro, id_Cartaoconfig) values (4, 120), (4, 122);

 109
110
120
122


-- select * from tb_venda where id_venda = 153630;



select top 50 * from tb_lancamentovenda;
sp_help tb_lancamento;
update tb_lancamentovenda set nu_ordem = 1 where id_lancamento in (1696529, 1696533);

-- select top 600 dt_vencimento, nu_valor, nu_parcelas, nu_ordem, * from vw_vendalancamento ;
-- select * from tb_evolucao where id_evolucao = 9;




select * from tb_vendaproduto where id_venda = 153630;


select * from tb_entidade where id_entidade = 14;
*/

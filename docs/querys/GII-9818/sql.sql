USE [G2_UNY_RELEASE]
GO

/****** Object:  View [dbo].[vw_comercial_g1_g2]    Script Date: 30/05/2018 09:57:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER VIEW [dbo].[vw_comercial_g1_g2] AS
SELECT
	vw.[Código Entidade],
	vw.[Entidade],
	vw.[Aluno],
	vw.[CPF],
	vw.[UF],
	vw.[Cidade],
	vw.[Segmento],
	vw.[Código Área],
	vw.[Área],
	vw.[Título Curso],
	vw.[Data_Matricula],
	vw.[Vendedor],
	vw.[Atendente],
	vw.[Núcleo de Atendimento],
	vw.[Valor Boleto],
	vw.[Valor Carta Crédito],
	vw.[Valor Bolsa],
	vw.[Valor Negociado],
	vw.[Valor no Cartão de Crédito],
	vw.[Evolução da Matrícula],
	vw.[Código Contrato G1],
	vw.[Código Contrato G2],
	vw.[Transferida outra Entidade ?],
	vw.[Afiliada],
	vw.[id_entidadepai],
	vw.[gptqry_codentidade],
	vw.[dt_matricula],
	vw.[Mátricula],
	vw.[Data do Último Acesso],
	s.st_saladeaula AS 'Sala de Aula'
  FROM (
    SELECT
      id_entidade AS 'Código Entidade' ,
      st_nomeentidade AS Entidade,
      st_nomecompleto AS Aluno ,
      st_cpf AS CPF ,
      sg_uf AS UF ,
      st_cidade AS Cidade,
      st_projetopedagogico AS Curso,
      NULL AS Segmento,
      NULL AS 'Código Área',
      st_areaconhecimento AS 'Área' ,
      st_projetopedagogico AS 'Título Curso',
      CONVERT (CHAR,dt_matricula,103) AS 'Data_Matricula' ,
      st_representante AS Vendedor ,
      st_atendente AS Atendente ,
      NULL AS 'Núcleo de Atendimento',
      nu_valorboleto AS 'Valor Boleto',
      nu_valorcarta AS 'Valor Carta Crédito',
      nu_valorbolsa AS 'Valor Bolsa',
      nu_valorliquido AS 'Valor Negociado' ,
      nu_valorcartaoc AS 'Valor no Cartão de Crédito',
      st_evolucao AS 'Evolução da Matrícula',
      NULL AS 'Código Contrato G1',
      id_venda AS 'Código Contrato G2',
      NULL AS 'Transferida outra Entidade ?',
      NULL AS Afiliada,
      id_entidadepai,
      NULL AS gptqry_codentidade,
      dt_matricula AS 'dt_matricula' ,
      id_matricula AS 'Mátricula',
      id_logacesso,
      dt_ultimoacesso AS 'Data do Último Acesso'
    FROM rel.vw_comercialgeral
    WHERE --id_entidade in (12,19) and
      id_entidadepai IN (12,19, 176, 177, 178, 179, 423, 569, 510, 39, 1036, 1103, 1106)

    UNION

    SELECT
      [Cód. Entidade] AS 'Código Entidade' ,
      Entidade COLLATE SQL_Latin1_General_CP1_CI_AI AS Entidade,
      Aluno COLLATE SQL_Latin1_General_CP1_CI_AI AS Aluno ,
      CPF COLLATE SQL_Latin1_General_CP1_CI_AI AS CPF ,
      UF COLLATE SQL_Latin1_General_CP1_CI_AI AS UF ,
      Cidade COLLATE SQL_Latin1_General_CP1_CI_AI AS Cidade ,
      [Título Curso] COLLATE SQL_Latin1_General_CP1_CI_AI AS Curso ,
      Segmento COLLATE SQL_Latin1_General_CP1_CI_AI AS Segmento ,
      [Cód. Área] AS 'Código Área',
      Área COLLATE SQL_Latin1_General_CP1_CI_AI AS Área,
      [Título Curso] COLLATE SQL_Latin1_General_CP1_CI_AI AS 'Título Curso' ,
      [Data Matrícula] AS 'Data Matrícula',
      Vendedor COLLATE SQL_Latin1_General_CP1_CI_AI AS Vendedor ,
      Atendente COLLATE SQL_Latin1_General_CP1_CI_AI AS Atendente ,
      Núcleo COLLATE SQL_Latin1_General_CP1_CI_AI AS 'Núcleo de Atendimento',
      [Valor Boleto] AS 'Valor Boleto',
      [Valor Carta] AS 'Valor Carta Crédito',
      [Valor Bolsa] AS 'Valor Bolsa',
      [Valor Negociado] AS 'Valor Negociado',
      [Valor no Cartão de Crédito] AS 'Valor no Cartão de Crédito' ,
      [Última Matrícula Evolução] COLLATE SQL_Latin1_General_CP1_CI_AI AS 'Evolução da Matrícula',
      [Cód. Contrato] AS 'Código Contrato G1' ,
      [Identificação Venda G2] AS 'Código Contrato G2' ,
      [Transferida outra Entidade] COLLATE SQL_Latin1_General_CP1_CI_AI AS 'Transferida outra Entidade ?',
      AFILIADA COLLATE SQL_Latin1_General_CP1_CI_AI AS Afiliada,
      NULL AS id_entidadepai,
      gptqry_codentidade,
      gptqry_dt_matricula AS 'dt_matricula'  ,
      [Cód. Matrícula] AS 'Mátricula',
      (SELECT MAX(l.id_logacesso)
                    FROM dbo.tb_logacesso l
                    WHERE (    l.id_funcionalidade = 448
                            OR (l.id_funcionalidade = 799
                    AND l.id_matricula = mt.id_matricula))
                    AND l.id_entidade  = mt.id_entidadematricula
                    AND l.id_usuario   = mt.id_usuario) AS id_logacesso,
      CAST (mt.dt_ultimoacesso AS DATE) AS 'Data do Último Acesso'
    FROM [WIN-MB0LL52UPVD].G1_UNY_RELEASE.rel.vw_com5_comercial_geral vwg1
    LEFT JOIN dbo.tb_matricula mt ON mt.id_matricula = vwg1.[Cód. Matrícula]
    WHERE [Cód. Entidade] IN (112,686,967,966,974,608,868,1289,1412,1546,1491,1583)
) vw

   LEFT JOIN dbo.tb_logacesso la
     ON la.id_logacesso = vw.id_logacesso
   LEFT JOIN dbo.tb_saladeaulaentidade sae
     ON sae.id_entidade   = la.id_entidade
    AND sae.id_saladeaula = la.id_saladeaula
   LEFT JOIN dbo.tb_saladeaula s
     ON s.id_saladeaula = sae.id_saladeaula
GO
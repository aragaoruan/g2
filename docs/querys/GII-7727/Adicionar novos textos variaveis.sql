SET IDENTITY_INSERT dbo.tb_textovariaveis ON

INSERT INTO tb_textovariaveis
(id_textovariaveis, id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel, bl_exibicao)
VALUES
  (447, 3, 'st_pai', '#nome_pai#', NULL, 1, 0),
  (448, 3, 'dt_dataexpedicaoaluno', '#data_expedicao_aluno#', 'converteDataCliente', 2, 0),
  (449, 3, 'st_nomeinstituicao', '#estabelecimento_conclusao#', 'retornaVariaveisUpper', 1, 0),
  (450, 3, 'st_municipioinstituicao', '#municipio _estabelecimento#', NULL, 1, 0),
  (451, 3, 'ano_conclusao_medio', '#ano_conclusao_medio#', NULL, 1, 0),
  (452, 3, 'st_filiacao', '#nome_mae#', NULL, 1, 0),
  (453, 3, 'st_ufnascimento', '#naturalidade_aluno#', NULL, 1, 0),
  (454, 3, 'dt_dataexpedicaoaluno', '#data_expedicao_aluno_DD_MM_AAAA#', 'convertDataToBr', 1, 0),
  (455, 3, 'id_matricula', '#grid_historico_escolar_disciplina#', 'gerarGridHistoricoEscolarDisciplinas', 2, 0),
  (456, 3, 'st_tituloeleitor', '#titulo_eleitor#', NULL, 1, 0),
  (457, 3, 'st_zonaeleitoral', '#zona_eleitoral#', NULL, 1, 0),
  (458, 3, 'st_municipioeleitoral', '#municipio_eleitoral#', NULL, 1, 0),
  (459, 3, 'st_secaoeleitoral', '#secao_eleitoral#', NULL, 1, 0),
  (460, 3, 'st_certificadoreservista', '#certificado_reservista#', NULL, 1, 0),
  (461, 3, 'dt_expedicaocertificadoreservista', '#expedicao_certificado_reservista#', 'convertDataToBr', 1, 0),
  (462, 3, 'id_categoriaservicomilitar', '#categoria_servico_militar#', NULL, 1, 0),
  (463, 3, 'st_reparticaoexpedidora', '#reparticao_expedidora#', NULL, 1, 0),
  (464, 3, 'st_reconhecimento', '#reconhecimento#', NULL, 1, 0),
  (465, 3, 'dt_colacaograu', '#data_colacao_grau#', 'convertDataToBr', 1, 0),
  (466, 3, 'dt_expedicaodiploma', '#expedicao_certificado_reservista#', 'convertDataToBr', 1, 0),
  (467, 3, 'dt_nascimentoaluno', '#data_nascimento_aluno_DD_MM_AAAA#', 'convertDataToBr', 1, 0)

SET IDENTITY_INSERT dbo.tb_textovariaveis OFF
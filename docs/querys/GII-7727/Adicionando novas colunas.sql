ALTER TABLE tb_textosistema
  ADD bl_edicao BIT NOT NULL DEFAULT (1);
ALTER TABLE tb_textovariaveis
  ADD bl_exibicao BIT NOT NULL DEFAULT (1);
UPDATE tb_textovariaveis
SET st_camposubstituir = 'st_rgaluno'
WHERE id_textovariaveis = 6;
UPDATE tb_textovariaveis
SET st_mascara = 'convertDataToBr'
WHERE id_textovariaveis = 419;
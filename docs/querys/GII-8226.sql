/**

Este arquivo precisa de aten��o especial pois n�o � apenas rodar, � preciso verificar onde temos 804 e 805 e mudar para o pr�ximo id_funcionalidade.

Para que a funcionalidade do relat�rio seja exibida no menu em homologa��o, execute a seguinte query contida em g2/docs/querys/Insert_Funcionalidades.sql.


**/


SET IDENTITY_INSERT tb_funcionalidade ON

INSERT INTO "tb_funcionalidade"
("id_funcionalidade",
 "st_funcionalidade",
 "id_funcionalidadepai",
 "bl_ativo",
 "id_situacao",
 "st_classeflex",
 "st_urlicone",
 "id_tipofuncionalidade",
 "nu_ordem",
 "st_classeflexbotao",
 "bl_pesquisa",
 "bl_lista",
 "id_sistema",
 "st_ajuda",
 "st_target",
 "st_urlcaminho",
 "bl_visivel",
 "bl_relatorio",
 "st_urlcaminhoedicao",
 "bl_delete")
VALUES
  (804, 'Relat�rio de Links de Pagamento', 488, 'True', 123, NULL, NULL, 3, 0, NULL, 'False', 'False', 1, NULL, NULL,
   '804', 1, 'True', NULL, 'False');
SET IDENTITY_INSERT tb_funcionalidade OFF;
SET IDENTITY_INSERT tb_funcionalidade ON

INSERT INTO "tb_funcionalidade"
("id_funcionalidade",
 "st_funcionalidade",
 "id_funcionalidadepai",
 "bl_ativo",
 "id_situacao",
 "st_classeflex",
 "st_urlicone",
 "id_tipofuncionalidade",
 "nu_ordem",
 "st_classeflexbotao",
 "bl_pesquisa",
 "bl_lista",
 "id_sistema",
 "st_ajuda",
 "st_target",
 "st_urlcaminho",
 "bl_visivel",
 "bl_relatorio",
 "st_urlcaminhoedicao",
 "bl_delete")
VALUES
  (805, 'Relat�rio de Links de Pagamento para Atendente', 488, 'True', 123, NULL, NULL, 3, 0, NULL, 'False', 'False', 1,
   NULL, NULL, '805', 1, 'True', NULL, 'False');
SET IDENTITY_INSERT tb_funcionalidade OFF;

-- auto-generated definition
CREATE TABLE tb_grauacademico
(
  id_grauacademico INT IDENTITY
    PRIMARY KEY,
  st_grauacademico VARCHAR(30) NOT NULL,
  st_titulo VARCHAR(30) NOT NULL
)
GO
CREATE UNIQUE INDEX tb_grauacademico_id_grauacademico_uindex
  ON tb_grauacademico (id_grauacademico)
GO

INSERT INTO
  tb_grauacademico (st_grauacademico)
VALUES
  ('Superior de Tecnologia', 'TECNÓLOGO'),
  ('Bacharelado', 'BACHAREL'),
  ('Licenciatura', 'LICENCIADO'),
  ('Especialização', 'ESPECIALISTA'),
  ('Mestrado', 'MESTRE'),
  ('Doutorado', 'DOUTOR');
ALTER TABLE tb_projetopedagogico
  ADD id_grauacademico INT DEFAULT NULL
  CONSTRAINT tb_projetopedagogico_tb_grauacademico__fk
  REFERENCES tb_grauacademico (id_grauacademico);
EXEC sp_rename 'tb_grauacademico.st_titulo', st_titulacao, 'COLUMN';


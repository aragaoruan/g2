ALTER TABLE [tb_pedidointegracao] ALTER COLUMN [nu_bandeiracartao] INTEGER NULL;
ALTER TABLE [tb_pedidointegracao] ADD [st_bandeiracartao] VARCHAR(100) NULL;
ALTER TABLE [tb_transacaofinanceira] ADD [st_statuspagarme] VARCHAR(100) NULL;

insert into tb_situacaointegracao (nu_status, st_situacao, id_sistema, dt_cadastro, bl_ativo, st_status) values
(2, 'transação sendo processada', 30, getdate(), 1, 'processing')
,(3, 'transação autorizada. Cliente possui saldo na conta e este valor foi reservado para futura captura, deve acontecer em 5 dias. Caso a transação não seja capturada, a mesma é cancelada automaticamente', 30, getdate(), 1, 'authorized')
,(4, 'transação paga (autorizada e capturada).', 30, getdate(), 1, 'refunded')
,(5, 'transação estornada.', 30, getdate(), 1, 'waiting_payment')
,(6, 'transação não autorizada.', 30, getdate(), 1, 'refused')
,(7, 'transação sofreu chargeback.', 30, getdate(), 1, 'chargedback');


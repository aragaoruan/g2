
-- #cpf_aluno#
SET IDENTITY_INSERT tb_textovariaveis ON;
INSERT INTO tb_textovariaveis (id_textovariaveis, id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
VALUES (777, 4, 'st_cpfaluno', '#cpf_aluno#', 'setaMascaraCPF', 1);
SET IDENTITY_INSERT tb_textovariaveis OFF;

-- #filiacao#
SET IDENTITY_INSERT tb_textovariaveis ON;
INSERT INTO tb_textovariaveis (id_textovariaveis, id_textocategoria, st_camposubstituir, st_textovariaveis, id_tipotextovariavel)
VALUES (778, 4, 'st_filiacao', '#filiacao#', 1);
SET IDENTITY_INSERT tb_textovariaveis OFF;

-- #endereco_nucleo_agendamento#
SET IDENTITY_INSERT tb_textovariaveis ON;
INSERT INTO tb_textovariaveis (id_textovariaveis, id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
VALUES (779, 4, 'id_matricula', '#endereco_nucleo_agendamento#', 'retornaEnderecoNucleoUltimoAgendamento', 2);
SET IDENTITY_INSERT tb_textovariaveis OFF;

-- #horario_agendamento#
SET IDENTITY_INSERT tb_textovariaveis ON;
INSERT INTO tb_textovariaveis (id_textovariaveis, id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
VALUES (780, 4, 'id_matricula', '#horario_agendamento#', 'retornaHorarioUltimoAgendamento', 2);
SET IDENTITY_INSERT tb_textovariaveis OFF;

-- #data_agendamento_aluno#
SET IDENTITY_INSERT tb_textovariaveis ON;
INSERT INTO tb_textovariaveis (id_textovariaveis, id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
VALUES (781, 4, 'id_matricula', '#data_agendamento_aluno#', 'retornaDataUltimoAgendamento', 2);
SET IDENTITY_INSERT tb_textovariaveis OFF;

-- #grid_disciplina_certificado_UCAM#
SET IDENTITY_INSERT tb_textovariaveis ON;
INSERT INTO tb_textovariaveis (id_textovariaveis, id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
VALUES (782, 4, 'id_matricula', '#grid_disciplina_certificado_UCAM#', 'retornarDisciplinaCertificadoUcam', 2);
SET IDENTITY_INSERT tb_textovariaveis OFF;
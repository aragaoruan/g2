-- Variável de semestre por data atual
INSERT INTO dbo.tb_textovariaveis
(id_textocategoria,
 st_camposubstituir,
 st_textovariaveis,
 st_mascara,
 id_tipotextovariavel,
 bl_exibicao
)
VALUES (3, -- id_textocategoria - Declaração
        'semestre_por_dataatual', -- st_camposubstituir - varchar(100)
        '#semestre_por_dataatual#', -- st_textovariaveis - varchar(100)
        NULL, -- st_mascara - varchar(100)
        1, -- id_tipotextovariavel - int
        1 -- bl_exibição - true
)

-- Variável de periodo por disciplina
INSERT INTO dbo.tb_textovariaveis
(id_textocategoria,
 st_camposubstituir,
 st_textovariaveis,
 st_mascara,
 id_tipotextovariavel,
 bl_exibicao
)
VALUES (3, -- id_textocategoria - Declaração
        'periodo_por_disciplina', -- st_camposubstituir - varchar(100)
        '#periodo_por_disciplina#', -- st_textovariaveis - varchar(100)
        NULL, -- st_mascara - varchar(100)
        1, -- id_tipotextovariavel - int
        1 -- bl_exibição - true
)

-- Variável do total do periodo do projeto
INSERT INTO dbo.tb_textovariaveis
(id_textocategoria,
 st_camposubstituir,
 st_textovariaveis,
 st_mascara,
 id_tipotextovariavel,
 bl_exibicao
)
VALUES (3, -- id_textocategoria - Declaração
        'total_periodo_projeto', -- st_camposubstituir - varchar(100)
        '#total_periodo_projeto#', -- st_textovariaveis - varchar(100)
        NULL, -- st_mascara - varchar(100)
        1, -- id_tipotextovariavel - int
        1 -- bl_exibição - true
)

-- Variável do grau academico do projeto
INSERT INTO dbo.tb_textovariaveis
(id_textocategoria,
 st_camposubstituir,
 st_textovariaveis,
 st_mascara,
 id_tipotextovariavel,
 bl_exibicao
)
VALUES (3, -- id_textocategoria - Declaração
        'grau_academico_projeto', -- st_camposubstituir - varchar(100)
        '#grau_academico_projeto#', -- st_textovariaveis - varchar(100)
        NULL, -- st_mascara - varchar(100)
        1, -- id_tipotextovariavel - int
        1 -- bl_exibição - true
)
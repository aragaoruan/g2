-- Este insert habilita a biblioteca no menu do novo Portal. É necessário mudar, para cada entidade, o
-- id doperfil, pois cada perfil é vinculado na entidade. Para recuperar os perfis use a query: select * from tb_perfil
insert into tb_perfilfuncionalidade (id_perfil, id_funcionalidade, bl_ativo) values(831,775,1)
insert into tb_perfilfuncionalidade (id_perfil, id_funcionalidade, bl_ativo) values(832,775,1)
insert into tb_perfilfuncionalidade (id_perfil, id_funcionalidade, bl_ativo) values(840,775,1)
insert into tb_perfilfuncionalidade (id_perfil, id_funcionalidade, bl_ativo) values(841,775,1)

-- Este insert adiciona a integração da biblioteca virtual. Caso este insert não for executado para a entidade desejada, a
-- mensagem de "Integração não configurada" será exibida.
insert into tb_entidadeintegracao (id_entidade, id_usuariocadastro, id_sistema, st_codsistema, dt_cadastro, st_codchave, st_caminho) VALUES(1037,2,23,'Integração PEARSON', GETDATE(), 'PJonSa3eSLhnTqzfcs2b', 'http://unyead.bv3.digitalpages.com.br/user_session/authentication_gateway')
insert into tb_entidadeintegracao (id_entidade, id_usuariocadastro, id_sistema, st_codsistema, dt_cadastro, st_codchave, st_caminho) VALUES(1107,2,23,'Integração PEARSON', GETDATE(), 'PJonSa3eSLhnTqzfcs2b', 'http://unyead.bv3.digitalpages.com.br/user_session/authentication_gateway')
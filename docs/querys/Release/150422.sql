﻿insert into tb_funcionalidade values (
'Atendentes',	338,	1,	123,	'/atendente',	'/img/ico/flag.png',	3,	NULL,	NULL,	1,	0,	1,	NULL,	NULL,	'/atendente',	1,	0,	NULL, 0);

/****** Object:  View [dbo].[vw_atendentes]    Script Date: 26-03-2015 16:41:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[vw_atendentes] AS
SELECT
	nf.id_nucleofuncionariotm,
	nf.id_funcao,
	nf.id_usuario,
	nf.id_nucleotm,
	nf.dt_cadastro,
	nf.bl_ativo,
	nf.id_usuariocadastro,
	nf.id_situacao,
	n.st_nucleotm,
	f.st_funcao,
	s.st_situacao,
	u.st_nomecompleto,
	n.id_entidade,
	u.st_cpf
FROM
	"tb_nucleofuncionariotm" AS "nf"
INNER JOIN "tb_nucleotm" AS "n" ON nf.id_nucleotm = n.id_nucleotm
INNER JOIN "tb_funcao" AS "f" ON nf.id_funcao = f.id_funcao
INNER JOIN "tb_situacao" AS "s" ON s.id_situacao = nf.id_situacao
INNER JOIN "tb_usuario" AS "u" ON u.id_usuario = nf.id_usuario
GO
SET IDENTITY_INSERT dbo.tb_situacao ON;
INSERT INTO tb_situacao (id_situacao, st_situacao, st_tabela, st_campo, st_descricaosituacao) VALUES (165, 'Ativo', 'tb_dashboard', 'id_situacao', 'Dashboard será mostrado na Home');
INSERT INTO tb_situacao (id_situacao, st_situacao, st_tabela, st_campo, st_descricaosituacao) VALUES (166, 'Inativo', 'tb_dashboard', 'id_situacao', 'Dashboard não será mostrado na Home');
SET IDENTITY_INSERT dbo.tb_situacao OFF;
SET IDENTITY_INSERT dbo.tb_tipofuncionalidade ON;
insert into dbo.tb_tipofuncionalidade
        ( id_tipofuncionalidade,st_tipofuncionalidade )
VALUES  ( 9,'Dashboard'  -- st_tipofuncionalidade - varchar(255)
          )
SET IDENTITY_INSERT dbo.tb_tipofuncionalidade OFF;
SET IDENTITY_INSERT dbo.tb_funcionalidade ON;
INSERT INTO tb_funcionalidade (id_funcionalidade, st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao, st_classeflex, id_tipofuncionalidade, bl_pesquisa, bl_lista, id_sistema, st_urlcaminho, bl_visivel, bl_relatorio, bl_delete) VALUES (676, 'Dashboard', 231, 1, 123, '', 3, 0, 0, 1, '/dashboard', 1, 0, 1)
SET IDENTITY_INSERT dbo.tb_funcionalidade OFF;


CREATE TABLE [tb_dashboard](
	[id_dashboard] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[st_dashboard] [varchar](250) NOT NULL,
	[st_descricao] [varchar](max) NULL,
	[st_origem] [varchar](250) NOT NULL,
	[st_tipochart] [varchar](250) NOT NULL,
	[st_xtipo] [varchar](250) NULL,
	[st_xtitulo] [varchar](250) NULL,
	[st_ytipo] [varchar](250) NULL,
	[st_ytitulo] [varchar](250) NULL,
	[st_campolabel] [varchar] (250) NULL,
	[id_situacao] [int] NOT NULL,
	[bl_ativo] [bit] NOT NULL
 CONSTRAINT [PK_TB_DASHBOARD] PRIMARY KEY CLUSTERED 
(
	[id_dashboard] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];

ALTER TABLE [dbo].[tb_dashboard]  WITH CHECK ADD  CONSTRAINT [tb_situacao_tb_dashboard_fk] FOREIGN KEY([id_situacao])
REFERENCES [dbo].[tb_situacao] ([id_situacao]);

SET IDENTITY_INSERT dbo.tb_dashboard ON;
INSERT INTO tb_dashboard (id_dashboard, st_dashboard, st_descricao, st_origem, st_tipochart, st_xtipo, st_xtitulo, st_ytitulo, id_situacao, bl_ativo) VALUES (1, 'Registros pendentes de enviar ao Moodle', 'Mostrará numero de alunos e colaboradores pendentes à serem enviados ao moodle', 'vw_dash_pendentes_moodle', 'column', 'category', 'Pessoas', 'Quantidade', 165, 1);
SET IDENTITY_INSERT dbo.tb_dashboard OFF;



CREATE TABLE [tb_campodashboard](
	[id_campodashboard] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[st_campodashboard] [varchar](250) NOT NULL,
	[st_titulocampo] [varchar](250) NOT NULL,
	[id_dashboard] [int] NOT NULL
 CONSTRAINT [PK_TB_CAMPODASHBOARD] PRIMARY KEY CLUSTERED 
(
	[id_campodashboard] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)

ALTER TABLE [dbo].[tb_campodashboard]  WITH CHECK ADD  CONSTRAINT [tb_situacao_tb_campodashboard_fk] FOREIGN KEY([id_dashboard])
REFERENCES [dbo].[tb_dashboard] ([id_dashboard]);


SET IDENTITY_INSERT dbo.tb_campodashboard ON;
INSERT INTO tb_campodashboard (id_campodashboard, st_campodashboard, st_titulocampo, id_dashboard) VALUES (1, 'nu_alunospendentes', 'Alunos Pendentes', 1);
INSERT INTO tb_campodashboard (id_campodashboard, st_campodashboard, st_titulocampo, id_dashboard) VALUES (2, 'nu_colaboradorespendentes', 'Colaboradores Pendentes', 1);
SET IDENTITY_INSERT dbo.tb_campodashboard OFF;





SET IDENTITY_INSERT dbo.tb_funcionalidade ON;
INSERT INTO tb_funcionalidade (id_funcionalidade, st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao, st_classeflex, id_tipofuncionalidade, bl_pesquisa, bl_lista, id_sistema, st_urlcaminho, bl_visivel, bl_relatorio, bl_delete) VALUES (681, 'Dashboards', NULL, 1, 123, '', 9, 0, 0, 1, '681', 1, 0, 1)
INSERT INTO tb_funcionalidade (id_funcionalidade, st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao, st_classeflex, id_tipofuncionalidade, bl_pesquisa, bl_lista, id_sistema, st_urlcaminho, bl_visivel, bl_relatorio, bl_delete) VALUES (682, 'Dash Pendentes Moodle', 681, 1, 123, '', 9, 0, 0, 1, '682', 1, 0, 1)
SET IDENTITY_INSERT dbo.tb_funcionalidade OFF;


UPDATE dbo.tb_funcionalidade 
SET bl_ativo = 1
WHERE id_funcionalidade = 673;

UPDATE dbo.tb_funcionalidade
SET 
st_classeflex = '', 
st_classeflexbotao = 'br.com.ead1.gestor2.view.conteudo.entidade.cadastrar.Atendente'
WHERE 
id_funcionalidade = 674;

GO

/*
* Alteraзгo tb_usuarioperfilentidadereferencia
*/
alter table tb_usuarioperfilentidadereferencia add bl_desativarmoodle char(1)


/*
* Criaзгo de view para listar os coordenadores que tem que ter papel retirado das salas no moodle
*/

create view vw_desativarcoordenadoresmoodle 
as
select 
uper.id_perfilreferencia,
uper.id_usuario,
u.st_nomecompleto,
pp.id_projetopedagogico,
pp.st_projetopedagogico,
pri.id_perfilreferenciaintegracao,
pri.id_saladeaulaintegracao,
pri.bl_encerrado,
pri.id_sistema,
usi.st_codusuario,
sali.st_codsistemacurso,
usi.id_entidade,
uper.bl_desativarmoodle

from tb_usuarioperfilentidadereferencia as uper
join tb_usuario as u on u.id_usuario = uper.id_usuario
join tb_projetopedagogico as pp on pp.id_projetopedagogico=uper.id_projetopedagogico
join tb_perfilreferenciaintegracao as pri on pri.id_perfilreferencia=uper.id_perfilreferencia and pri.id_sistema=6 and pri.bl_encerrado=0
join tb_saladeaulaintegracao as sali on sali.id_saladeaulaintegracao = pri.id_saladeaulaintegracao
join tb_usuariointegracao as usi on usi.id_usuario = uper.id_usuario and usi.id_sistema=pri.id_sistema
where uper.bl_desativarmoodle = 1


GO
create view [dbo].[vw_gerarpacotealunos]
as
select mt.id_matricula
,pp.id_projetopedagogico
,it.id_itemdematerial
,it.id_upload
,it.id_situacao as id_situacaoitem
,md.id_matriculadisciplina
,id.id_disciplina
,mt.id_entidadeatendimento as id_entidade
,a.id_areaconhecimento

from 
tb_matricula mt
join tb_projetopedagogico pp on pp.id_projetopedagogico=mt.id_projetopedagogico
join tb_areaprojetopedagogico app on app.id_projetopedagogico = pp.id_projetopedagogico
join tb_areaconhecimento a on a.id_areaconhecimento = app.id_areaconhecimento 
join tb_matriculadisciplina md on md.id_matricula=mt.id_matricula and id_pacote is null
join tb_itemdematerialdisciplina as id on id.id_disciplina=md.id_disciplina
join tb_itemdematerial as it on it.id_itemdematerial=id.id_itemdematerial AND it.id_situacao = 35
JOIN dbo.tb_materialprojeto AS mp ON mp.id_itemdematerial = it.id_itemdematerial AND mp.id_projetopedagogico = pp.id_projetopedagogico
where mt.id_evolucao=6
AND NOT EXISTS (SELECT em.id_entregamaterial FROM dbo.tb_entregamaterial AS em
WHERE em.id_matricula = mt.id_matricula AND em.id_itemdematerial = it.id_itemdematerial AND em.bl_ativo = 1)
--and mt.id_matricula=36

go
create view [dbo].[vw_gerararquivodeenvio] as
select 
p.id_pacote,
REPLACE(REPLACE(vw.st_cpf,'.',''),'-','') as st_cpf,
vw.st_nomecompleto,
vw.st_endereco,
vw.nu_numero,
vw.st_complemento,
vw.st_bairro,
vw.st_nomemunicipio,
vw.sg_uf,
REPLACE(REPLACE(vw.st_cep,'.',''),'-','') as st_cep,
em.id_itemdematerial,
it.st_itemdematerial,
d.id_disciplina,
d.st_tituloexibicao as st_disciplina,
pp.id_projetopedagogico,
pp.st_projetopedagogico,
area.id_areaconhecimento,
area.st_areaconhecimento,
mt.id_entidadeatendimento as id_entidade,
ent.st_nomeentidade,
lm.id_lotematerial,
cast(p.dt_cadastro as date) as dt_criacaopacote
from tb_lotematerial as lm
join tb_pacote as p on p.id_lotematerial = lm.id_lotematerial and p.id_situacao=155
join tb_entregamaterial as em on em.id_pacote = p.id_pacote
join tb_itemdematerial as it on it.id_itemdematerial=em.id_itemdematerial
join tb_matriculadisciplina as md on md.id_matriculadisciplina = em.id_matriculadisciplina and md.id_pacote = p.id_pacote
join tb_disciplina as d on d.id_disciplina = md.id_disciplina
join tb_matricula as mt on mt.id_matricula=em.id_matricula and mt.id_evolucao=6
join tb_entidade as ent on ent.id_entidade = mt.id_entidadeatendimento
join tb_projetopedagogico as pp on pp.id_projetopedagogico=mt.id_projetopedagogico
join tb_areaprojetopedagogico as ar on ar.id_projetopedagogico=pp.id_projetopedagogico
join tb_areaconhecimento as area on area.id_areaconhecimento=ar.id_areaconhecimento --and area.id_entidade = mt.id_entidadeatendimento
join vw_pessoa as vw on vw.id_usuario = mt.id_usuario and vw.id_entidade=mt.id_entidadeatendimento


go 

CREATE VIEW [dbo].[vw_atendentes]
AS
SELECT nf.id_nucleofuncionariotm ,
nf.id_funcao ,
nf.id_usuario ,
nf.id_nucleotm ,
nf.dt_cadastro ,
nf.bl_ativo ,
nf.id_usuariocadastro ,
nf.id_situacao ,
n.st_nucleotm ,
f.st_funcao ,
s.st_situacao ,
u.st_nomecompleto ,
n.id_entidade
FROM "tb_nucleofuncionariotm" AS "nf"
INNER JOIN "tb_nucleotm" AS "n" ON nf.id_nucleotm = n.id_nucleotm
INNER JOIN "tb_funcao" AS "f" ON nf.id_funcao = f.id_funcao
INNER JOIN "tb_situacao" AS "s" ON s.id_situacao = nf.id_situacao
            INNER JOIN "tb_usuario" AS "u" ON u.id_usuario = nf.id_usuario
GO
CREATE VIEW [dbo].[vw_atendentes]
AS
    SELECT  nf.id_nucleofuncionariotm ,
            nf.id_funcao ,
            nf.id_usuario ,
            nf.id_nucleotm ,
            nf.dt_cadastro ,
            nf.bl_ativo ,
            nf.id_usuariocadastro ,
            nf.id_situacao ,
            n.st_nucleotm ,
            f.st_funcao ,
            s.st_situacao ,
            u.st_nomecompleto ,
            n.id_entidade
    FROM    "tb_nucleofuncionariotm" AS "nf"
            INNER JOIN "tb_nucleotm" AS "n" ON nf.id_nucleotm = n.id_nucleotm
            INNER JOIN "tb_funcao" AS "f" ON nf.id_funcao = f.id_funcao
            INNER JOIN "tb_situacao" AS "s" ON s.id_situacao = nf.id_situacao
            INNER JOIN "tb_usuario" AS "u" ON u.id_usuario = nf.id_usuario


set identity_insert tb_situacao ON				
INSERT INTO dbo.tb_situacao
(
id_situacao,
st_situacao ,
st_tabela ,
st_campo ,
st_descricaosituacao
)
VALUES ( '167',
'Inativo' , -- st_situacao - varchar(255)
'tb_nucleofuncionariotm' , -- st_tabela - varchar(255)
'id_situacao' , -- st_campo - varchar(255)
'Funcionário de núcleo inativo' -- st_descricaosituacao - varchar(200)
)


set identity_insert tb_situacao OFF

ALTER PROCEDURE [dbo].[sp_tramite] 
@id_categoriatramite INT, 
@id_campo VARCHAR(8000), -- id da chave da tabela de relação com trâmite. Exemplo: tb_tramitematricula - id_campo = id_matricula
@id_tipotramite INT = NULL
AS
BEGIN
SET NOCOUNT ON;

DECLARE
@st_tabela varchar(50),
@st_campo varchar(50),
@id_entregamaterialatual varchar(50),
@sql varchar(max)

-- Busca a tabela e campo da categoria do trâmite
SELECT @st_tabela = st_tabela, @st_campo = st_campo
FROM dbo.tb_categoriatramite
WHERE id_categoriatramite = @id_categoriatramite


-- Se o id_categoriatramite for de entrega de material a SP buscará todos os tramites de entrega de materia daquela matrícula pra o item relacionado
    IF(@id_categoriatramite = 2)
    BEGIN
        DECLARE cursor_entregamaterial CURSOR FOR 
            SELECT en.id_entregamaterial FROM tb_entregamaterial en
                JOIN tb_entregamaterial enId ON en.id_matricula = enId.id_matricula AND en.id_itemdematerial = enId.id_itemdematerial
                    WHERE enId.id_entregamaterial = @id_campo;
            OPEN cursor_entregamaterial;
            
            FETCH NEXT FROM cursor_entregamaterial INTO @id_entregamaterialatual;
                WHILE @@fetch_status = 0
                BEGIN
                    SELECT @id_campo = @id_campo + ', ' + @id_entregamaterialatual;
                    FETCH NEXT FROM cursor_entregamaterial INTO @id_entregamaterialatual
                END        
            CLOSE cursor_entregamaterial;
    DEALLOCATE cursor_entregamaterial;
    END
        -- Busca que traz os tramites de acordo com a categoria e o id informado
        
        SET @sql        = 'SELECT tm.id_tramite, tm.id_upload, up.st_upload, tm.st_tramite, tm.id_tipotramite, tt.st_tipotramite, tt.id_categoriatramite, ct.st_categoriatramite, tm.id_usuario, us.st_nomecompleto,tm.id_entidade, tm.dt_cadastro, tm.bl_visivel, tramiterelacao.'+ @st_campo + ' AS id_campo ';
        SET @sql        = @sql + ' FROM dbo.'+ @st_tabela + ' AS tramiterelacao' ;
        SET @sql        = @sql + ' JOIN dbo.tb_tramite AS tm ON tramiterelacao.id_tramite        = tm.id_tramite';
        SET @sql        = @sql + ' LEFT JOIN dbo.tb_usuario AS US ON us.id_usuario                = tm.id_usuario';
        SET @sql        = @sql + ' LEFT JOIN dbo.tb_upload AS up ON up.id_upload = tm.id_upload';
        SET @sql        = @sql + ' LEFT JOIN dbo.tb_tipotramite as tt ON tt.id_tipotramite                = tm.id_tipotramite';
        SET @sql        = @sql + ' LEFT JOIN dbo.tb_categoriatramite as ct ON ct.id_categoriatramite = tt.id_categoriatramite and ct.id_categoriatramite = '+ cast( @id_categoriatramite as varchar(3) );
        
        SET @sql        = @sql + ' where tramiterelacao.'+ @st_campo +' in ('+ cast( @id_campo as varchar(8000))+')';
        
		IF(@id_tipotramite IS NOT NULL)
		BEGIN
			SET @sql	= @sql + ' AND tm.id_tipotramite = ' + cast( @id_tipotramite as varchar(8000)) ;	
		END
		
        SET @sql		= @sql + ' order by dt_cadastro desc ';
        
        --PRINT @sql
        EXECUTE( @sql )
                
        RETURN (0)
		end

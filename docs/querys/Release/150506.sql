
SET IDENTITY_INSERT dbo.tb_tipoendereco ON;
INSERT INTO tb_tipoendereco (id_tipoendereco, st_tipoendereco, id_categoriaendereco) VALUES (5, 'Correspond�ncia', 3);
SET IDENTITY_INSERT dbo.tb_tipoendereco OFF;

GO

--DESATIVAR IDENTITY TABELA
SET IDENTITY_INSERT tb_funcionalidade ON

insert into tb_funcionalidade 
(id_funcionalidade, st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao,
st_classeflex, st_urlicone, id_tipofuncionalidade, nu_ordem, st_classeflexbotao,
bl_pesquisa, bl_lista, id_sistema, st_ajuda, st_target, st_urlcaminho, bl_visivel, bl_relatorio
, st_urlcaminhoedicao, bl_delete)
values 
(677, 'Quantidade Tentativas recorrente', 488, 1, 123, 
'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa', null, 3, 0, null, 0, 0,1, null, null, 
'677', 1, 1, null, 1)

--ATIVAR IDENTITY TABELA
SET IDENTITY_INSERT tb_funcionalidade OFF

GO

insert tb_textovariaveis
values
(9, 'nu_numerocorresp', '#numero_corresp#', null, 1),
(9, 'st_complementocorresp', '#complemento_corresp#', null, 1),
(9, 'st_bairrocorresp', '#bairro_corresp#', null, 1),
(9, 'st_cepcorresp', '#cep_corresp#', null, 1),
(9, 'sg_ufcorresp', '#uf_corresp#', null, 1),
(9, 'st_nomemunicipiocorresp', '#cidade_corresp#', null, 1),
(9, 'st_enderecocorresp', '#endereco_corresp#', null, 1)

GO

GO


ALTER VIEW [dbo].[vw_primeiroacesso]
AS
SELECT DISTINCT
	pa.id_venda,
	isnull(dt_aceita, getdate()) as dt_aceita,
	v.id_entidade,
        c.id_contrato,
	u.id_usuario, 
	u.st_login,
	u.st_nomecompleto,
	u.st_cpf,
	
	di.st_rg,
	di.st_orgaoexpeditor,
	di.dt_dataexpedicao,
	
	p.dt_nascimento,
	
	cepp.id_email,
	ce.st_email,
	
	p.sg_uf AS sg_ufnascimento,
	p.id_municipio AS id_municipionascimento,
	p.st_cidade AS st_cidadenascimento,
	
	ctp.id_telefone,
	ct.nu_telefone,
	ct.nu_ddd,
	ct.nu_ddi,
        
        ctp2.id_telefone as id_telefonealternativo,
	ct2.nu_telefone as nu_telefonealternativo,
	ct2.nu_ddd as nu_dddalternativo,
	ct2.nu_ddi as nu_ddialternativo,
	
	pe.id_endereco,
	e.st_endereco,
	e.st_cep,
	e.st_bairro,
	e.st_complemento,
	e.nu_numero,
	e.st_estadoprovincia,
	
	e.id_pais,
	e.id_municipio,
	e.sg_uf,
	pais.st_nomepais,
	pais.st_nacionalidade,
	mu.st_nomemunicipio,
	
	pec.id_endereco as id_enderecocorresp,
	pec.st_endereco as st_enderecocorresp,
	pec.st_cep as st_cepcorresp,
	pec.st_bairro as st_bairrocorresp,
	pec.st_complemento as st_complementocorresp,
	pec.nu_numero as nu_numerocorresp,
	pec.st_estadoprovincia as st_estadoprovinciacorresp,
	pec.id_pais as id_paiscorresp,
	pec.id_municipio as id_municipiocorresp,
	pec.sg_uf as sg_ufcorresp,
	pec.st_nomepais as st_nomepaiscorresp,
	pec.st_nomemunicipio as st_nomemunicipiocorresp
	
FROM tb_venda v 
LEFT JOIN tb_pa_marcacaoetapa AS pa
	ON v.id_venda = pa.id_venda
LEFT JOIN tb_contrato AS c
        ON v.id_venda = c.id_venda
LEFT JOIN tb_usuario AS u
	ON v.id_usuario = u.id_usuario
INNER JOIN tb_pessoa AS p
	ON p.id_usuario = u.id_usuario AND p.bl_ativo = 1 AND v.id_entidade = p.id_entidade
LEFT JOIN tb_contatosemailpessoaperfil AS cepp
	ON cepp.id_usuario = u.id_usuario AND cepp.id_entidade = p.id_entidade AND cepp.bl_ativo = 1 AND cepp.bl_padrao = 1
LEFT JOIN tb_contatosemail AS ce
	ON ce.id_email = cepp.id_email
LEFT JOIN tb_contatostelefonepessoa AS ctp
	ON ctp.id_usuario = u.id_usuario AND ctp.id_entidade = v.id_entidade AND ctp.bl_padrao = 1
LEFT JOIN tb_contatostelefonepessoa AS ctp2
	ON ctp2.id_usuario = u.id_usuario AND ctp2.id_entidade = v.id_entidade AND ctp2.bl_padrao = 0
LEFT JOIN tb_contatostelefone AS ct
	ON ct.id_telefone = ctp.id_telefone
LEFT JOIN tb_contatostelefone AS ct2
	ON ct2.id_telefone = ctp2.id_telefone
OUTER APPLY(SELECT TOP 1 * from vw_pessoaendereco 
	WHERE id_usuario = u.id_usuario AND id_entidade = p.id_entidade AND bl_padrao = 1 AND id_tipoendereco = 1) AS pe

OUTER APPLY(SELECT TOP 1 * from vw_pessoaendereco 
	WHERE id_usuario = u.id_usuario AND id_entidade = p.id_entidade AND bl_padrao = 1 AND id_tipoendereco = 5) AS pec

LEFT JOIN tb_endereco AS e
	ON e.id_endereco = pe.id_endereco AND p.bl_ativo = 1
LEFT JOIN tb_municipio AS mu
	ON mu.id_municipio = e.id_municipio
LEFT JOIN tb_tipotelefone AS tt
	ON tt.id_tipotelefone = ct.id_tipotelefone
LEFT JOIN tb_tipotelefone AS tt2
	ON tt2.id_tipotelefone = ct2.id_tipotelefone
LEFT JOIN dbo.tb_documentoidentidade AS di
	ON di.id_usuario = u.id_usuario AND di.id_entidade = p.id_entidade
LEFT JOIN dbo.tb_pais AS pais
	ON pais.id_pais = e.id_pais

GO
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER VIEW [dbo].[vw_resumofinanceiro] AS
    SELECT DISTINCT
        l.id_lancamento,
        lv.id_venda,
        lv.bl_entrada,
        l.st_banco,
        l.st_emissor,
        l.dt_prevquitado,
        l.st_coddocumento,
        ed.st_endereco,
        ed.st_cidade,
        ed.st_bairro,
        ed.st_estadoprovincia,
        ed.st_cep ,
        ed.sg_uf,
        ed.nu_numero,
        l.bl_quitado,
        l.dt_quitado,
        CAST(l.nu_quitado AS DECIMAL(10,2)) AS nu_quitado,
        st_entradaparcela = CASE lv.bl_entrada WHEN 1 THEN 'Entrada' ELSE 'Parcelas' END,
         lv.nu_ordem AS nu_parcela,
        l.id_meiopagamento,
        mp.st_meiopagamento,
        CASE
        WHEN ISNULL(l.nu_juros,0) > 0 or ISNULL(l.nu_desconto,0) > 0
		THEN ISNULL(l.nu_valor,0) + ISNULL(l.nu_juros,0) - ISNULL(l.nu_desconto,0)
		ELSE l.nu_valor
		END as nu_valor,
        l.nu_vencimento,
        l.dt_vencimento,
		l.st_autorizacao,
		case when l.st_ultimosdigitos IS null then tf.st_ultimosdigitos
		-- case when tf.st_ultimosdigitos  IS null then 'N�o informados' else tf.st_ultimosdigitos end
		else l.st_ultimosdigitos end as st_ultimosdigitos,
		-- l.st_ultimosdigitos,
        st_situacao = CASE
                WHEN CAST(l.dt_vencimento AS date) < CAST(GETDATE() AS DATE) AND l.bl_quitado = 0 AND l.id_meiopagamento NOT IN (1,7) AND l.bl_ativo = 1 THEN 'Vencido'
                WHEN CAST(l.dt_vencimento AS DATE) < CAST(GETDATE() AS DATE) AND l.bl_quitado = 0 AND l.id_meiopagamento IN (1,7) AND l.dt_prevquitado   IS NULL AND l.bl_ativo = 1 THEN 'Atrasado'
                WHEN l.id_meiopagamento IN (1,7) AND l.dt_prevquitado IS NOT NULL  AND l.bl_ativo = 1 THEN 'Autorizado'
                WHEN l.bl_quitado = 1  AND l.bl_ativo = 1 THEN 'Pago'
                WHEN l.bl_ativo = 0 THEN 'Cancelado'
                ELSE 'Pendente'
                END,
        --u.id_usuario,
       -- u.st_nomecompleto,

        CASE WHEN u.id_usuario IS NOT NULL THEN u.id_usuario
			 WHEN el.id_entidade IS NOT NULL THEN el.id_entidade END AS id_usuario,

		CASE WHEN u.st_nomecompleto IS NOT NULL THEN u.st_nomecompleto
			 WHEN el.st_nomeentidade IS NOT NULL THEN el.st_nomeentidade COLLATE DATABASE_DEFAULT END AS st_nomecompleto,

        l.id_entidade,
        ef.id_textosistemarecibo,
        ef.id_reciboconsolidado,
        l.st_nossonumero,
        CASE WHEN tf.st_codtransacaooperadora IS NOT NULL THEN tf.st_codtransacaooperadora
			 WHEN l.st_coddocumento IS NOT NULL THEN l.st_coddocumento END AS st_codtransacaooperadora,
        l.bl_original,
        l.bl_ativo,
        l.dt_atualizado,
        l.st_numcheque,
		l.nu_juros,
		l.nu_desconto,
		l.nu_jurosboleto,
		l.nu_descontoboleto,
		l.id_acordo,
		v.recorrente_orderid,
		v.dt_cadastro as dt_venda,
		si.st_situacao AS st_situacaopedido,
		CASE WHEN si3.st_situacao IS NOT NULL THEN si3.st_situacao
			 WHEN l.bl_quitado = 0 THEN 'PENDENTE'
			 WHEN l.bl_quitado = 1 THEN 'PAGA' END AS st_situacaolancamento,

		CASE WHEN sl.id_venda IS NOT NULL THEN 1 ELSE 0 END AS bl_cartaovencendo,
		l.bl_chequedevolvido,
		CASE WHEN rec.bl_recorrente > 0 THEN 1 ELSE 0 END AS bl_recorrente
    FROM tb_lancamento l
        INNER JOIN tb_lancamentovenda AS lv ON lv.id_lancamento = l.id_lancamento
        INNER JOIN tb_meiopagamento AS mp ON mp.id_meiopagamento = l.id_meiopagamento
        INNER JOIN tb_venda AS v ON v.id_venda = lv.id_venda
        --INNER JOIN tb_contrato as c ON c.id_venda = v.id_venda
        INNER JOIN tb_vendaproduto AS vp ON vp.id_venda = v.id_venda
        INNER JOIN tb_produto AS p ON p.id_produto = vp.id_produto
        --LEFT JOIN tb_usuario AS u ON u.id_usuario = l.id_usuariolancamento
        --OUTER APPLY (SELECT TOP 1 * FROM  tb_pessoaendereco where id_usuario = u.id_usuario AND id_entidade = l.id_entidade AND bl_padrao = 1) AS pe
        --LEFT JOIN tb_endereco AS ed ON ed.id_endereco = pe.id_endereco
        LEFT JOIN tb_entidadefinanceiro ef ON ef.id_entidade = v.id_entidade
        LEFT JOIN tb_transacaolancamento AS tl ON l.id_lancamento = tl.id_lancamento
        LEFT JOIN tb_transacaofinanceira AS tf ON tl.id_transacaofinanceira = tf.id_transacaofinanceira
        LEFT JOIN tb_pedidointegracao AS pint ON pint.id_venda = v.id_venda
        LEFT JOIN tb_situacaointegracao AS si ON si.id_situacaointegracao = pint.id_situacaointegracao

        OUTER APPLY ( SELECT TOP 1 si2.st_situacao
							FROM tb_transacaolancamento AS tl
							INNER JOIN tb_transacaofinanceira AS tf ON tl.id_transacaofinanceira = tf.id_transacaofinanceira
							INNER JOIN tb_situacaointegracao AS si2 ON si2.id_situacaointegracao = tf.id_situacaointegracao
							WHERE tl.id_lancamento = L.id_lancamento
							ORDER BY tf.dt_cadastro DESC
					) AS si3

        OUTER APPLY (
		   SELECT st_nomecompleto, us.id_usuario , pe.id_endereco
				FROM tb_usuario us
				JOIN tb_pessoaendereco pe ON pe.id_usuario = us.id_usuario
				WHERE us.id_usuario = id_usuariolancamento
				AND pe.id_entidade = v.id_entidade
				AND bl_padrao = 1
		) AS u

		OUTER APPLY (
		   SELECT TOP 1 st_nomeentidade, e.id_entidade , ee.id_endereco
		   FROM tb_entidade e
		   JOIN tb_entidadeendereco ee ON e.id_entidade = ee.id_entidade
		   WHERE e.id_entidade = id_entidadelancamento
		   --AND ee.id_entidade = v.id_entidade
		   --AND bl_padrao = 1
		) AS el

		OUTER APPLY(
			SELECT DISTINCT vv.id_venda
				FROM tb_venda vv
					INNER JOIN tb_pedidointegracao pit ON pit.id_venda = vv.id_venda
					INNER JOIN tb_lancamentovenda lvs ON lvs.id_venda = vv.id_venda
					INNER JOIN tb_lancamento ls ON ls.id_lancamento = lvs.id_lancamento
				WHERE vv.id_venda = v.id_venda
					AND pit.id_sistema = 10
					AND CAST(ls.dt_vencimento AS DATE) >= CAST(pit.dt_vencimentocartao AS DATE)
					AND DATEDIFF(DAY, GETDATE(), pit.dt_vencimentocartao) <= 30
		) sl
		
		OUTER APPLY(
			SELECT COUNT(id_meiopagamento) as bl_recorrente 
				FROM tb_lancamento ll 
				INNER JOIN tb_lancamentovenda lvv ON ll.id_lancamento = lvv.id_lancamento
				WHERE lvv.id_venda = v.id_venda
				AND ll.id_meiopagamento = 11
				AND ll.bl_quitado = 0
				AND ll.bl_ativo = 1
				--AND ll.dt_vencimento > CAST(GETDATE() AS DATE)
		) rec

		LEFT JOIN tb_endereco AS ed ON ed.id_endereco = CASE WHEN u.id_endereco IS NOT NULL THEN u.id_endereco WHEN el.id_endereco IS NOT NULL THEN el.id_endereco END





GO
	
CREATE VIEW [dbo].[vw_bandeiracartao] AS	  	
	SELECT	DISTINCT
			cb.st_cartaobandeira,
			cb.id_cartaobandeira,
			cc.id_sistema,
			ce.id_entidade
		FROM
			tb_cartaobandeira cb
			INNER JOIN tb_cartaoconfig cc ON cb.id_cartaobandeira	= cc.id_cartaobandeira
			INNER JOIN tb_contaentidade ce ON ce.id_contaentidade = cc.id_contaentidade
			INNER JOIN tb_entidadecartaoconfig ecc ON ecc.id_cartaoconfig	= cc.id_cartaoconfig



		GO
        
	


ALTER VIEW rel.vw_ecommerce AS
SELECT DISTINCT
	us.st_nomecompleto,
	us.st_email,
	us.st_cpf,
	CAST (nu_ddd AS VARCHAR) + CAST (nu_telefone AS VARCHAR) AS st_telefone,
	us.st_endereco,
	us.st_cep,
	us.st_bairro,
	us.st_complemento,
	us.nu_numero,
	us.st_cidade,
	us.sg_uf,
	pd.st_produto,
	pd.id_produto,
	vp.nu_desconto AS nu_descontoproduto,
	vp.nu_valorbruto AS nu_valortabela,
	vp.nu_valorliquido AS nu_valornegociado,
	vd.nu_valorliquido AS nu_valorliquidovenda,
	vd.dt_confirmacao,
	--mp.st_meiopagamento,
	mp.id_meiopagamento,
	pd.id_tipoproduto,
	vd.id_venda,
	vd.id_evolucao,
	ev.st_evolucao,
	ve.id_entidade,
	vd.dt_cadastro,
	pdc.st_produto AS st_combo,
	pdc.id_produto AS id_produtocombo,
	ve.st_nomeentidade,
	te.id_entidade AS id_entidadepai,
	vd.st_observacao,
	us.id_usuario AS cod_aluno,
	vd.id_cupom,
	us_atendente.st_nomecompleto AS st_nomeatendente,
	-----
	(
		SELECT
			COALESCE (cti.st_categoria + ', ', '')
		FROM
			dbo.tb_categoriaproduto AS cpi
		JOIN dbo.tb_categoria AS cti ON cti.id_categoria = cpi.id_categoria
		WHERE
			cpi.id_produto = pd.id_produto FOR XML PATH ('')
	) AS st_categorias,
	cp.id_categoria,
	cu.nu_desconto AS nu_valorcupom,
	cu.st_codigocupom,
	td.st_tipodesconto,
	cc.st_campanhacomercial,
	lvc.nu_parcelas,
	(
		SELECT
			COALESCE (
				ac2.st_areaconhecimento + ', ',
				''
			)
		FROM
			dbo.tb_areaconhecimento ac2
		JOIN dbo.tb_areaprojetopedagogico app2
		JOIN dbo.tb_produtoprojetopedagogico AS ppp ON ppp.id_projetopedagogico = app2.id_projetopedagogico ON ac2.id_areaconhecimento = app2.id_areaconhecimento
		WHERE
			ppp.id_produto = vp.id_produto FOR XML PATH ('')
	) AS st_areaconhecimento,
	CASE cu.nu_desconto
WHEN 100.00 THEN
	'Gratuito'
ELSE
	mp.st_meiopagamento
END AS 'st_meiopagamento'



FROM
	tb_entidade AS te
JOIN dbo.vw_entidaderecursivaid AS ve ON ve.nu_entidadepai = te.id_entidade OR ve.id_entidade = te.id_entidade
JOIN dbo.tb_venda AS vd ON vd.id_entidade = ve.id_entidade
LEFT JOIN dbo.tb_usuario AS us_atendente ON vd.id_atendente = us_atendente.id_usuario
JOIN dbo.tb_evolucao AS ev ON ev.id_evolucao = vd.id_evolucao
JOIN dbo.vw_pessoa AS us ON us.id_usuario = vd.id_usuario AND us.id_entidade = vd.id_entidade
LEFT JOIN dbo.tb_municipio AS mn ON mn.id_municipio = us.id_municipio
JOIN dbo.tb_vendaproduto AS vp ON vd.id_venda = vp.id_venda
JOIN dbo.tb_produto AS pd ON pd.id_produto = vp.id_produto
JOIN tb_cupom AS cu ON cu.id_cupom = vd.id_cupom
JOIN dbo.tb_campanhacomercial AS cc ON cu.id_campanhacomercial = cc.id_campanhacomercial
JOIN dbo.tb_tipodesconto AS td ON td.id_tipodesconto = cu.id_tipodesconto
LEFT JOIN dbo.tb_categoriaproduto AS cp ON cp.id_produto = pd.id_produto
LEFT JOIN dbo.tb_lancamentovenda AS lv ON lv.id_venda = vd.id_venda AND bl_entrada = 1
LEFT JOIN dbo.tb_lancamento AS lc ON lc.id_lancamento = lv.id_lancamento
LEFT JOIN dbo.tb_meiopagamento AS mp ON mp.id_meiopagamento = lc.id_meiopagamento
LEFT JOIN tb_produto AS pdc ON pdc.id_produto = vp.id_produtocombo OUTER APPLY (
	SELECT
		COUNT (lvo.id_lancamento) AS nu_parcelas
	FROM
		dbo.tb_lancamentovenda AS lvo
	WHERE
		lvo.id_venda = vd.id_venda
) AS lvc
WHERE
	vd.bl_ativo = 1





GO


/****** Object:  View [rel].[vw_vendaconfirmadasprodutos]    Script Date: 28/03/2014 14:02:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




ALTER VIEW [rel].[vw_vendaconfirmadasprodutos] as
SELECT DISTINCT
	st_nomecompleto,
	st_email,
	st_cpf,
	CAST (nu_ddd AS VARCHAR) + CAST (nu_telefone AS VARCHAR) AS st_telefone,
	st_endereco,
	st_cep,
	st_bairro,
	us.st_complemento,
	nu_numero,
	st_cidade,
	us.sg_uf,
	pd.st_produto,
	pd.id_produto,
	vp.nu_valorliquido,
	vd.nu_valorliquido AS nu_valorliquidovenda,
	vp.nu_valorbruto,
	vd.dt_confirmacao,
	--mp.st_meiopagamento,
	pd.id_tipoproduto,
	vd.id_venda,
	vd.id_evolucao,
	ve.id_entidade,
	vd.dt_cadastro,
	pdc.st_produto AS st_combo,
	pdc.id_produto AS id_produtocombo,
	ve.st_nomeentidade,
	te.id_entidade AS id_entidadepai,
	vd.st_observacao,
	us.id_usuario AS cod_aluno,
	vd.id_cupom,
	us.st_rg,
	CASE cp.nu_desconto
WHEN 100.00 THEN
	'Gratuito'
ELSE
	mp.st_meiopagamento
END AS 'st_meiopagamento'
FROM
	tb_entidade AS te
JOIN dbo.vw_entidaderecursivaid AS ve ON ve.nu_entidadepai = te.id_entidade OR ve.id_entidade = te.id_entidade
JOIN dbo.tb_venda AS vd ON vd.id_entidade = ve.id_entidade
JOIN dbo.vw_pessoa AS us ON us.id_usuario = vd.id_usuario AND us.id_entidade = vd.id_entidade
LEFT JOIN dbo.tb_municipio AS mn ON mn.id_municipio = us.id_municipio
JOIN dbo.tb_vendaproduto AS vp ON vd.id_venda = vp.id_venda
JOIN dbo.tb_produto AS pd ON pd.id_produto = vp.id_produto
LEFT JOIN dbo.tb_lancamentovenda AS lv ON lv.id_venda = vd.id_venda AND bl_entrada = 1
LEFT JOIN dbo.tb_lancamento AS lc ON lc.id_lancamento = lv.id_lancamento
LEFT JOIN dbo.tb_meiopagamento AS mp ON mp.id_meiopagamento = lc.id_meiopagamento
LEFT JOIN tb_produto AS pdc ON pdc.id_produto = vp.id_produtocombo
LEFT JOIN tb_cupom AS cp ON cp.id_cupom = vd.id_cupom
WHERE
	vd.bl_ativo = 1





GO



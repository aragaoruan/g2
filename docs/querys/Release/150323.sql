﻿/*Scprit para gerar o menu */
SET IDENTITY_INSERT dbo.tb_funcionalidade ON


INSERT  INTO [dbo].[tb_funcionalidade]
        ( id_funcionalidade, [st_funcionalidade] ,
          [id_funcionalidadepai] ,
          [bl_ativo] ,
          [id_situacao] ,
          [st_classeflex] ,
          [st_urlicone] ,
          [id_tipofuncionalidade] ,
          [nu_ordem] ,
          [st_classeflexbotao] ,
          [bl_pesquisa] ,
          [bl_lista] ,
          [id_sistema] ,
          [st_ajuda] ,
          [st_target] ,
          [st_urlcaminho] ,
          [bl_visivel] ,
          [bl_relatorio] ,
          [st_urlcaminhoedicao] ,
          [bl_delete]
        )
VALUES  ( 666,'Pacote/Arquivo de Envio' ,
          98 ,
          1 ,
          123 ,
          'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa' ,
          '/img/ico/block--pencil.png' ,
          3 ,
          NULL ,
          NULL ,
          0 ,
          0 ,
          1 ,
          NULL ,
          NULL ,
          '/material-arquivo-envio' ,
          1 ,
          0 ,
          NULL ,
          0
        )
GO

SET IDENTITY_INSERT dbo.tb_funcionalidade OFF

/* Fim do script para gerar menu */




CREATE TABLE tb_lotematerial
    (
      id_lotematerial INT IDENTITY ,
      dt_cadastro DATETIME2 NULL
                            DEFAULT GETDATE() ,
      CONSTRAINT PK_TB_LOTEMATERIAL PRIMARY KEY ( id_lotematerial )
    )
GO

IF EXISTS ( SELECT  1
            FROM    sys.extended_properties
            WHERE   major_id = OBJECT_ID('tb_lotematerial')
                    AND minor_id = 0 )
    BEGIN 
        DECLARE @CurrentUser sysname 
        SELECT  @CurrentUser = USER_NAME() 
        EXECUTE sp_dropextendedproperty 'MS_Description', 'user', @CurrentUser,
            'table', 'tb_lotematerial' 
 
    END 


SELECT  @CurrentUser = USER_NAME() 
EXECUTE sp_addextendedproperty 'MS_Description',
    'Tabela que cont�m os registros de lots de pacotes de material gerado',
    'user', @CurrentUser, 'table', 'tb_lotematerial'
GO

/*==============================================================*/
/* Table: tb_lotematerial  (FIM)                                */
/*==============================================================*/


/*==============================================================*/
/* Table: tb_pacote                                             */
/*==============================================================*/

ALTER TABLE dbo.tb_pacote ADD id_situacao INT
ALTER TABLE dbo.tb_pacote ADD id_lotematerial INT

/*==============================================================*/
/* Table: tb_pacote  (FIM)                                      */
/*==============================================================*/

/*==============================================================*/
/* Table: tb_entregamaterial                                    */
/*==============================================================*/
ALTER TABLE dbo.tb_entregamaterial ADD id_pacote INT

ALTER TABLE dbo.tb_entregamaterial ADD id_matriculadisciplina INT
ALTER TABLE dbo.tb_entregamaterial
ADD CONSTRAINT FK_TB_MATD_REFERENCE_TB_ENTRE FOREIGN KEY (id_matriculadisciplina)
REFERENCES dbo.tb_matriculadisciplina (id_matriculadisciplina)
GO

ALTER TABLE tb_entregamaterial ALTER COLUMN dt_entrega DATETIME2 NULL


/*==============================================================*/
/* Table: tb_entregamaterial  (FIM)                             */
/*==============================================================*/


/*==============================================================*/
/* Table: tb_matriculadisciplina                                */
/*==============================================================*/

ALTER TABLE dbo.tb_matriculadisciplina ADD id_pacote INT
ALTER TABLE dbo.tb_matriculadisciplina
ADD CONSTRAINT FK_TB_ENTRE_REFERENCE_TB_PAC FOREIGN KEY (id_pacote)
REFERENCES dbo.tb_pacote (id_pacote)
GO


/*==============================================================*/
/* Table: tb_matriculadisciplina  (FIM)                          */
/*==============================================================*/

/*==============================================================*/
/* Table: tb_situacao                                           */
/*==============================================================*/
GO
SET IDENTITY_INSERT dbo.tb_situacao ON
INSERT  INTO tb_situacao
        ( id_situacao ,
          st_situacao ,
          st_tabela ,
          st_campo ,
          st_descricaosituacao
        )
VALUES  ( 155 ,
          'Pendente' ,
          'tb_pacote' ,
          'id_situacao' ,
          'Pacote com envio pendente'
        )

INSERT  INTO tb_situacao
        ( id_situacao ,
          st_situacao ,
          st_tabela ,
          st_campo ,
          st_descricaosituacao
        )
VALUES  ( 156 ,
          'Enviado' ,
          'tb_pacote' ,
          'id_situacao' ,
          'Pacote enviado'
        )

GO
SET IDENTITY_INSERT dbo.tb_situacao OFF

GO



ALTER VIEW [dbo].[vw_gridcertificado]
AS
    SELECT  mt.id_matricula ,
            mt.id_projetopedagogico ,
            pp.st_tituloexibicao ,
            CAST(mtd.id_disciplina AS VARCHAR(200)) AS id_disciplina ,
            dc.st_tituloexibicao AS st_disciplina ,
            mtd.nu_aprovafinal AS nu_aprovafinal ,
            pp.nu_notamaxima ,
            mtd.dt_conclusao AS dt_conclusaodisciplina ,
            dc.nu_cargahoraria AS nu_cargahoraria ,
            mtd.id_evolucao ,
            ev.st_evolucao ,
            mtd.id_situacao ,
            CASE WHEN CAST(mtd.nu_aprovafinal AS INT) < 70 THEN 'Insuficiente'
                 WHEN CAST(mtd.nu_aprovafinal AS INT) < 80 THEN 'Bom'
                 WHEN CAST(mtd.nu_aprovafinal AS INT) < 90 THEN 'Otimo'
                 WHEN CAST(mtd.nu_aprovafinal AS INT) <= 100 THEN 'Excelente'
                 ELSE 'Insuficiente'
            END AS st_conceito ,
            us.st_nomecompleto AS st_titularcertificacao ,
            tit.st_titulacao ,
            mt.id_entidadematricula ,
            mt.dt_concluinte
    FROM    tb_matricula AS mt
            JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
            JOIN tb_matriculadisciplina AS mtd ON mtd.id_matricula = mt.id_matricula
                                                  AND mtd.bl_obrigatorio = 1
            LEFT JOIN tb_aproveitamentomatriculadisciplina AS apm ON apm.id_matricula = mtd.id_matricula
                                                              AND apm.id_disciplina = mtd.id_disciplina
            LEFT JOIN tb_aproveitamentodisciplina AS apd ON apd.id_aproveitamentodisciplina = apm.id_aproveitamentodisciplina
            JOIN tb_disciplina AS dc ON dc.id_disciplina = mtd.id_disciplina
            JOIN tb_entidade AS ent ON ent.id_entidade = mt.id_entidadematriz
            JOIN tb_evolucao AS ev ON ev.id_evolucao = mtd.id_evolucao
            LEFT JOIN tb_perfil AS pf ON pf.id_entidade = mt.id_entidadematricula
                                         AND pf.id_perfilpedagogico = 9
            LEFT JOIN tb_usuarioperfilentidadereferencia AS uper ON uper.id_projetopedagogico = pp.id_projetopedagogico
                                                              AND uper.id_disciplina = mtd.id_disciplina
                                                              AND uper.id_perfil = pf.id_perfil
                                                              AND ( ( mt.dt_concluinte BETWEEN uper.dt_inicio
                                                              AND
                                                              uper.dt_fim )
                                                              OR ( mt.dt_concluinte > uper.dt_inicio
                                                              AND uper.dt_fim IS NULL
                                                              )
                                                              )
                                                              AND uper.bl_titular = 1
                                                              AND uper.bl_ativo = 1
            LEFT JOIN tb_usuario AS us ON us.id_usuario = uper.id_usuario
            LEFT JOIN tb_titulacao tit ON uper.id_titulacao = tit.id_titulacao

GO

ALTER TABLE tb_motivo ADD id_entidadecadastro INT NULL;
ALTER TABLE tb_motivo ADD dt_cadastro DATETIME2 NULL;
ALTER TABLE tb_motivo ADD id_usuariocadastro INT NULL;
ALTER TABLE tb_motivo ADD id_situacao INT NOT NULL;

ALTER TABLE [dbo].[tb_motivo]  
ADD FOREIGN KEY(id_entidadecadastro) REFERENCES [dbo].[tb_entidade] ([id_entidade])
GO

ALTER TABLE [dbo].[tb_motivo]  
ADD FOREIGN KEY(id_usuariocadastro) REFERENCES [dbo].[tb_usuario] ([id_usuario])
GO

ALTER TABLE [dbo].[tb_motivo]  ADD FOREIGN KEY(id_situacao) REFERENCES [dbo].[tb_situacao] ([id_situacao])


-------------------------------------------------------------------------------------

CREATE TABLE tb_motivoentidade
    (
      id_motivoentidade INT NOT NULL
                            IDENTITY(1, 1) ,
      id_usuariocadastro INT NOT NULL ,
      id_motivo INT NOT NULL ,
      id_entidade INT NOT NULL ,
      dt_cadastro DATETIME2 NOT NULL ,
      bl_ativo BIT NOT NULL ,
      id_situacao INT NOT NULL ,
      PRIMARY KEY CLUSTERED ( [id_motivoentidade] ASC )
        WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
               IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
               ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
ON  [PRIMARY]
GO

ALTER TABLE [dbo].[tb_motivoentidade] ADD FOREIGN KEY(id_usuariocadastro)
REFERENCES [dbo].[tb_usuario] ([id_usuario])
GO

ALTER TABLE [dbo].[tb_motivoentidade] ADD FOREIGN KEY(id_motivo)
REFERENCES [dbo].[tb_motivo] ([id_motivo])
GO

ALTER TABLE [dbo].[tb_motivoentidade]  ADD FOREIGN KEY(id_entidade)
REFERENCES [dbo].[tb_entidade] ([id_entidade])
GO

ALTER TABLE [dbo].[tb_motivoentidade]  ADD FOREIGN KEY(id_situacao)
REFERENCES [dbo].[tb_situacao] ([id_situacao])
GO

---------------------------------------------------------------------------------------


GO
SET IDENTITY_INSERT dbo.tb_funcionalidade ON

INSERT  INTO [dbo].[tb_funcionalidade]
        ( [id_funcionalidade] ,
          [st_funcionalidade] ,
          [id_funcionalidadepai] ,
          [bl_ativo] ,
          [id_situacao] ,
          [st_classeflex] ,
          [st_urlicone] ,
          [id_tipofuncionalidade] ,
          [nu_ordem] ,
          [st_classeflexbotao] ,
          [bl_pesquisa] ,
          [bl_lista] ,
          [id_sistema] ,
          [st_ajuda] ,
          [st_target] ,
          [st_urlcaminho] ,
          [bl_visivel] ,
          [bl_relatorio] ,
          [st_urlcaminhoedicao]
        )
VALUES  ( 665 ,
          'Motivo do Cancelamento' ,
          194 ,
          1 ,
          123 ,
          'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa' ,
          'NULL' ,
          3 ,
          0 ,
          'br.com.ead1.gestor2.view.conteudo.financeiro.cadastrar.CadastrarMotivo' ,
          1 ,
          0 ,
          1 ,
          'NULL' ,
          'NULL' ,
          '/motivo' ,
          1 ,
          0 ,
          'NULL'
		 )
GO
GO
SET IDENTITY_INSERT dbo.tb_funcionalidade OFF
--------------------------------------------------------------------------------------------
SET IDENTITY_INSERT dbo.tb_situacao ON
INSERT  INTO tb_situacao
        ( id_situacao, st_situacao ,
          st_tabela ,
          st_campo ,
          st_descricaosituacao
        )
VALUES  ( 153,'Ativo' ,
          'tb_motivo' ,
          'id_situacao' ,
          'Motivo Ativo'
        ),  (154, 'Inativo' ,
          'tb_motivo' ,
          'id_situacao' ,
          'Motivo Inativo'
        );
SET IDENTITY_INSERT dbo.tb_situacao OFF

ALTER TABLE tb_configuracaoentidade ADD st_corentidade VARCHAR(7)
ALTER TABLE tb_entidade ADD st_apelido VARCHAR(15)

ALTER TABLE tb_projetopedagogico ADD st_apelido VARCHAR(15) NULL




SET IDENTITY_INSERT dbo.tb_funcionalidade ON
INSERT  INTO tb_funcionalidade
        ( id_funcionalidade,st_funcionalidade ,
          id_funcionalidadepai ,
          bl_ativo ,
          id_situacao ,
          id_tipofuncionalidade ,
          bl_pesquisa ,
          bl_lista ,
          id_sistema ,
          st_urlcaminho ,
          bl_visivel ,
          bl_relatorio ,
          bl_delete
        )
VALUES  ( 670,'Menu' ,
          '231' ,
          '1' ,
          '123' ,
          '3' ,
          '0' ,
          '0' ,
          '1' ,
          '/funcionalidade-menu' ,
          '1' ,
          '0' ,
          '1'
        )
SET IDENTITY_INSERT dbo.tb_funcionalidade OFF
GO


SET ANSI_NULLS  ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER TABLE dbo.tb_matriculadisciplina ADD id_matriculaoriginal INT NULL
go
UPDATE dbo.tb_matriculadisciplina SET id_matriculaoriginal = id_matricula
go
ALTER TABLE dbo.tb_matriculadisciplina ALTER COLUMN id_matriculaoriginal INT NOT NULL
go
ALTER PROCEDURE [dbo].[sp_matriculadisciplina]
    (
      @id_contrato INT ,
      @bl_retornamatricula BIT = 1
    )
AS
    SET NOCOUNT ON;

    DECLARE @id_matricula INT ,
        @id_projetopedagogico INT ,
        @id_modulo INT ,
        @id_disciplina INT ,
        @bl_obrigatoria BIT



    DECLARE MATRICULA CURSOR
    FOR
        SELECT  id_matricula
        FROM    tb_contratomatricula
        WHERE   id_contrato = @id_contrato
    OPEN MATRICULA
    FETCH NEXT FROM MATRICULA INTO @id_matricula
    WHILE @@FETCH_STATUS = 0
        BEGIN
            DELETE  FROM tb_matriculadisciplina
            WHERE   id_matricula = @id_matricula
            DECLARE PROJETOPEDAGOGICO CURSOR
            FOR
                SELECT  id_projetopedagogico
                FROM    tb_matricula AS m
                WHERE   m.id_matricula = @id_matricula
            OPEN PROJETOPEDAGOGICO
            FETCH NEXT FROM PROJETOPEDAGOGICO INTO @id_projetopedagogico
            WHILE @@FETCH_STATUS = 0
                BEGIN
                    DECLARE MODULO CURSOR
                    FOR
                        SELECT  id_modulo
                        FROM    tb_modulo
                        WHERE   id_projetopedagogico = @id_projetopedagogico
                                AND bl_ativo = 1
                    OPEN MODULO
                    FETCH NEXT FROM MODULO INTO @id_modulo
                    WHILE @@FETCH_STATUS = 0
                        BEGIN
                            DECLARE DISCIPLINA CURSOR
                            FOR
                                SELECT  id_disciplina ,
                                        bl_obrigatoria
                                FROM    tb_modulodisciplina
                                WHERE   id_modulo = @id_modulo
                                        AND bl_ativo = 1
                            OPEN DISCIPLINA
                            FETCH NEXT FROM DISCIPLINA INTO @id_disciplina,
                                @bl_obrigatoria
                            WHILE @@FETCH_STATUS = 0
                                BEGIN
                                    INSERT  INTO tb_matriculadisciplina
                                            ( id_matricula ,
                                              id_evolucao ,
                                              id_disciplina ,
                                              id_situacao ,
                                              nu_aprovafinal ,
                                              bl_obrigatorio ,
                                              id_matriculaoriginal
                                            )
                                    VALUES  ( @id_matricula ,
                                              11 ,
                                              @id_disciplina ,
                                              53 ,
                                              NULL ,
                                              @bl_obrigatoria ,
                                              @id_matricula
                                            )
                                    FETCH NEXT FROM DISCIPLINA INTO @id_disciplina,
                                        @bl_obrigatoria
                                END;
                            CLOSE DISCIPLINA;
                            DEALLOCATE DISCIPLINA;
                            FETCH NEXT FROM MODULO INTO @id_modulo
                        END;
                    CLOSE MODULO;
                    DEALLOCATE MODULO;
                    FETCH NEXT FROM PROJETOPEDAGOGICO INTO @id_projetopedagogico
                END;
            CLOSE PROJETOPEDAGOGICO;
            DEALLOCATE PROJETOPEDAGOGICO;
            FETCH NEXT FROM MATRICULA INTO @id_matricula
        END;
    CLOSE MATRICULA;
    DEALLOCATE MATRICULA;

    IF ( @bl_retornamatricula = 1 )
        BEGIN
            SELECT  id_matriculadisciplina ,
                    id_evolucao ,
                    id_situacao ,
                    id_matricula ,
                    id_disciplina ,
                    nu_aprovafinal ,
                    bl_obrigatorio ,
                    dt_conclusao
            FROM    tb_matriculadisciplina
            WHERE   id_matricula = @id_matricula
        END

--FINAL_PROCEDURE:


    RETURN (0)


GO


SET ANSI_NULLS  ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[sp_matriculadisciplinadireta] ( @id_matricula INT )
AS
    SET NOCOUNT ON;

    DECLARE @id_projetopedagogico INT ,
        @id_modulo INT ,
        @id_disciplina INT ,
        @bl_obrigatoria BIT



    DELETE  FROM tb_matriculadisciplina
    WHERE   id_matricula = @id_matricula

    DECLARE PROJETOPEDAGOGICO CURSOR
    FOR
        SELECT  id_projetopedagogico
        FROM    tb_matricula AS m
        WHERE   m.id_matricula = @id_matricula
    OPEN PROJETOPEDAGOGICO
    FETCH NEXT FROM PROJETOPEDAGOGICO INTO @id_projetopedagogico
    WHILE @@FETCH_STATUS = 0
        BEGIN
            DECLARE MODULO CURSOR
            FOR
                SELECT  id_modulo
                FROM    tb_modulo
                WHERE   id_projetopedagogico = @id_projetopedagogico
                        AND bl_ativo = 1
            OPEN MODULO
            FETCH NEXT FROM MODULO INTO @id_modulo
            WHILE @@FETCH_STATUS = 0
                BEGIN
                    DECLARE DISCIPLINA CURSOR
                    FOR
                        SELECT  id_disciplina ,
                                bl_obrigatoria
                        FROM    tb_modulodisciplina
                        WHERE   id_modulo = @id_modulo
                                AND bl_ativo = 1
                    OPEN DISCIPLINA
                    FETCH NEXT FROM DISCIPLINA INTO @id_disciplina,
                        @bl_obrigatoria
                    WHILE @@FETCH_STATUS = 0
                        BEGIN
                            INSERT  INTO tb_matriculadisciplina
                                    ( id_matricula ,
                                      id_evolucao ,
                                      id_disciplina ,
                                      id_situacao ,
                                      bl_obrigatorio ,
                                      id_matriculaoriginal
                                    )
                            VALUES  ( @id_matricula ,
                                      11 ,
                                      @id_disciplina ,
                                      53 ,
                                      @bl_obrigatoria ,
                                      @id_matricula
                                    )
                            FETCH NEXT FROM DISCIPLINA INTO @id_disciplina,
                                @bl_obrigatoria
                        END;
                    CLOSE DISCIPLINA;
                    DEALLOCATE DISCIPLINA;
                    FETCH NEXT FROM MODULO INTO @id_modulo
                END;
            CLOSE MODULO;
            DEALLOCATE MODULO;
            FETCH NEXT FROM PROJETOPEDAGOGICO INTO @id_projetopedagogico
        END;
    CLOSE PROJETOPEDAGOGICO;
    DEALLOCATE PROJETOPEDAGOGICO;
--FINAL_PROCEDURE:
    SELECT  *
    FROM    tb_matriculadisciplina
    WHERE   id_matricula = @id_matricula

    RETURN (0)

GO




ALTER VIEW [dbo].[vw_encerramentoalunos]
AS
    SELECT DISTINCT
            al.id_alocacao ,
            al.id_saladeaula ,
            mt.id_usuario ,
            md.id_matricula ,
            md.id_disciplina ,
            md.id_matriculadisciplina ,
            dc.id_tipodisciplina ,
            st_nota = CASE WHEN dc.id_tipodisciplina = 1
                                AND aa.st_nota IS NOT NULL THEN aa.st_nota
                           WHEN dc.id_tipodisciplina = 2
                                AND tcc.st_nota IS NOT NULL THEN tcc.st_nota
                           ELSE NULL
                      END ,
            mt.st_nomecompleto ,
            lg.id_logacesso ,
            id_usuarioprofessor ,
            dt_encerramentoprofessor ,
            id_usuariocoordenador ,
            dt_encerramentocoordenador ,
            id_usuariopedagogico ,
            dt_encerramentopedagogico ,
            id_usuariofinanceiro ,
            dt_encerramentofinanceiro ,
            si.id_sistema ,
            sa.id_entidade ,
            ei.nu_perfilalunoencerrado ,
            es.dt_recusa ,
            es.st_motivorecusa ,
            id_upload = CASE WHEN dc.id_tipodisciplina = 1 THEN aa.id_upload
                             WHEN dc.id_tipodisciplina = 2 THEN tcc.id_upload
                             ELSE NULL
                        END ,
            aa.id_tiponota
    FROM    dbo.tb_alocacao al
            JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula
            JOIN dbo.vw_matriculadisciplina AS md ON md.id_matriculadisciplina = al.id_matriculadisciplina
            JOIN dbo.tb_disciplina AS dc ON dc.id_disciplina = md.id_disciplina
            JOIN dbo.vw_matricula AS mt ON mt.id_matricula = md.id_matricula
                                           AND mt.bl_institucional = 0
                                           AND mt.id_evolucao IN ( 6, 15, 16 )
            OUTER APPLY ( SELECT TOP 1
                                    id_logacesso
                          FROM      dbo.tb_logacesso
                          WHERE     id_saladeaula = al.id_saladeaula
                                    AND id_usuario = mt.id_usuario
                                    AND ( ( sa.id_tiposaladeaula = 2
                                            AND CAST(dt_cadastro AS DATE) <= ( DATEADD(DAY,
                                                              ISNULL(sa.nu_diasextensao,
                                                              0),
                                                              sa.dt_encerramento) ) --sa.dt_encerramento
                                          )
                                          OR sa.id_tiposaladeaula = 1
                                        )
                        ) AS lg -- altera��o para AC-26484 
            LEFT JOIN dbo.vw_avaliacaoaluno AS aa ON aa.id_saladeaula = al.id_saladeaula
                                                     AND aa.id_matricula = mt.id_matricula
                                                     AND aa.bl_ativo = 1
                                                     AND aa.id_tipoavaliacao IN (
                                                     5 )
            LEFT JOIN dbo.vw_avaliacaoaluno AS tcc ON tcc.id_saladeaula = al.id_saladeaula
                                                      AND tcc.id_matricula = mt.id_matricula
                                                      AND tcc.bl_ativo = 1
                                                      AND tcc.id_tipoavaliacao IN (
                                                      6 )
            LEFT JOIN dbo.tb_encerramentoalocacao AS ea ON al.id_alocacao = ea.id_alocacao
            LEFT JOIN dbo.tb_encerramentosala AS es ON es.id_encerramentosala = ea.id_encerramentosala
            LEFT JOIN dbo.tb_saladeaulaintegracao AS si ON si.id_saladeaula = al.id_saladeaula
            LEFT JOIN dbo.tb_entidadeintegracao AS ei ON ei.id_entidade = sa.id_entidade
                                                         AND ei.id_sistema = 6
    WHERE   al.bl_ativo = 1
    UNION
    SELECT DISTINCT
            al.id_alocacao ,
            al.id_saladeaula ,
            mt.id_usuario ,
            md.id_matricula ,
            md.id_disciplina ,
            md.id_matriculadisciplina ,
            dc.id_tipodisciplina ,
            st_nota = CASE WHEN dc.id_tipodisciplina = 1
                                AND aa.st_nota IS NOT NULL THEN aa.st_nota
                           WHEN dc.id_tipodisciplina = 2
                                AND tcc.st_nota IS NOT NULL THEN tcc.st_nota
                           ELSE NULL
                      END ,
            mt.st_nomecompleto ,
            lg.id_logacesso ,
            id_usuarioprofessor ,
            dt_encerramentoprofessor ,
            id_usuariocoordenador ,
            dt_encerramentocoordenador ,
            id_usuariopedagogico ,
            dt_encerramentopedagogico ,
            id_usuariofinanceiro ,
            dt_encerramentofinanceiro ,
            si.id_sistema ,
            sa.id_entidade ,
            ei.nu_perfilalunoencerrado ,
            es.dt_recusa ,
            es.st_motivorecusa ,
            id_upload = CASE WHEN dc.id_tipodisciplina = 1 THEN aa.id_upload
                             WHEN dc.id_tipodisciplina = 2 THEN tcc.id_upload
                             ELSE NULL
                        END ,
            aa.id_tiponota
    FROM    dbo.tb_alocacao al
            JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula
            JOIN dbo.vw_matriculadisciplina AS md ON md.id_matriculadisciplina = al.id_matriculadisciplina
            JOIN dbo.tb_disciplina AS dc ON dc.id_disciplina = md.id_disciplina
            JOIN dbo.vw_matricula AS mt ON mt.id_matricula = md.id_matricula
                                           AND mt.bl_institucional = 0
                                           AND mt.id_evolucao IN ( 6, 15 )
            OUTER APPLY ( SELECT TOP 1
                                    id_logacesso
                          FROM      dbo.tb_logacesso
                          WHERE     id_saladeaula = al.id_saladeaula
                                    AND id_usuario = mt.id_usuario
                                    AND ( ( sa.id_tiposaladeaula = 2
                                            AND CAST(dt_cadastro AS DATE) <= sa.dt_encerramento
                                          )
                                          OR sa.id_tiposaladeaula = 1
                                        )
                        ) AS lg -- altera��o para AC-26484 
            LEFT JOIN dbo.vw_avaliacaoaluno AS aa ON aa.id_saladeaula = al.id_saladeaula
                                                     AND aa.id_matricula = mt.id_matricula
                                                     AND aa.bl_ativo = 1
                                                     AND aa.id_tipoavaliacao IN (
                                                     5 )
            LEFT JOIN dbo.vw_avaliacaoaluno AS tcc ON tcc.id_saladeaula = al.id_saladeaula
                                                      AND tcc.id_matricula = mt.id_matricula
                                                      AND tcc.bl_ativo = 1
                                                      AND tcc.id_tipoavaliacao IN (
                                                      6 )
            JOIN dbo.tb_encerramentoalocacao AS ea ON al.id_alocacao = ea.id_alocacao
            JOIN dbo.tb_encerramentosala AS es ON es.id_encerramentosala = ea.id_encerramentosala
            LEFT JOIN dbo.tb_saladeaulaintegracao AS si ON si.id_saladeaula = al.id_saladeaula
            LEFT JOIN dbo.tb_entidadeintegracao AS ei ON ei.id_entidade = sa.id_entidade
                                                         AND ei.id_sistema = 6
    WHERE   al.bl_ativo = 1


GO


GO

/****** Object:  View [dbo].[vw_gerardeclaracao]    Script Date: 09/03/2015 08:52:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







ALTER VIEW [dbo].[vw_gerardeclaracao]
AS
    SELECT DISTINCT
            mt.id_matricula ,
            ps.id_usuario AS id_usuarioaluno ,
            ps.st_nomecompleto AS st_nomecompletoaluno ,
            ps.st_login AS st_loginaluno ,
            ps.st_senhaentidade AS st_senhaaluno ,
            ps.st_cpf AS st_cpfaluno ,
            tur.id_turma AS id_turma ,
            CONVERT(CHAR, mt.dt_cadastro, 103) AS st_datamatricula ,
            canc.st_observacao AS st_observacaocancelamento ,
            canc.st_observacaocalculo AS st_observacao_calculo ,
            ps.st_rg AS st_rgaluno ,
            ps.st_orgaoexpeditor ,
            ps.dt_dataexpedicao AS dt_dataexpedicaoaluno ,
            ps.st_nomemae AS st_filiacao ,
            CONVERT(CHAR, canc.dt_solicitacao, 103) AS st_solicitacaocancelamento ,
            canc.nu_valorcarta AS st_valorcredito ,
            CONVERT(CHAR, DATEADD(D, 365, canc.dt_solicitacao), 103) AS st_validadecredito ,
            canc.id_cancelamento ,
            cm.id_contrato ,
            ps.dt_nascimento AS dt_nascimentoaluno ,
            UPPER(SUBSTRING(ps.st_nomemunicipio, 1, 1))
            + LOWER(SUBSTRING(ps.st_nomemunicipio, 2, 499)) AS st_municipioaluno ,
            ps.st_estadoprovincia AS st_ufaluno ,
            ps.sg_uf AS sg_ufaluno ,
            ps.nu_ddd AS nu_dddaluno ,
            ps.nu_telefone AS nu_telefonealuno ,
            ps.st_email AS st_emailaluno ,
            pp.st_tituloexibicao AS st_projeto ,
	--pp.nu_cargahoraria,
            ( SELECT    SUM(dis.nu_cargahoraria) AS nu_cargahoraria
              FROM      tb_matriculadisciplina md
                        JOIN tb_matricula m ON m.id_matricula = md.id_matricula
                        JOIN tb_disciplina dis ON dis.id_disciplina = md.id_disciplina
              WHERE     md.id_matricula = mt.id_matricula
            ) AS nu_cargahoraria ,
            ne.st_nivelensino ,
            mt.dt_concluinte AS dt_concluintealuno ,
            us_coordenador.st_nomecompleto AS st_coordenadorprojeto ,
            fl.id_fundamentolegal AS id_fundamentolei ,
            fl.nu_numero AS nu_lei ,
            flr.id_fundamentolegal AS id_fundamentoresolucao ,
            tur.st_turma ,
            tur.dt_fim AS dt_terminoturma ,
            tur.dt_inicio AS dt_inicioturma ,
            flr.nu_numero AS nu_resolucao ,
            flpr.id_fundamentolegal AS id_fundamentoparecer ,
            flpr.nu_numero AS nu_parecer ,
            flp.id_fundamentolegal AS id_fundamentoportaria ,
            flp.nu_numero AS nu_portaria ,
            et.id_entidade ,
            et.st_nomeentidade ,
            et.st_razaosocial ,
            et.st_cnpj ,
            et.st_urlimglogo ,
            et.st_urlsite ,
            ps.st_nomepais ,
            ps.st_sexo AS sexo_aluno ,
            UPPER(av.st_tituloavaliacao) AS st_titulomonografia ,
            CONVERT(VARCHAR(2), DATEPART(DAY, GETDATE()), 103) + ' de '
            + dbo.fn_mesporextenso(GETDATE()) + ' de '
            + CONVERT(VARCHAR(4), DATEPART(YEAR, GETDATE()), 103) AS st_data ,
            DAY(GETDATE()) AS st_dia ,
            LOWER(dbo.fn_mesporextenso(GETDATE())) AS st_mes ,
            YEAR(GETDATE()) AS st_ano ,
            CONVERT(DATE, sa.dt_abertura, 103) AS dt_primeirasala ,
            CONVERT(DATE, DATEADD(MONTH, 13, sa.dt_abertura), 103) AS dt_previsaofim ,
            mt.st_codcertificacao ,
            CONVERT(CHAR, GETDATE(), 103) AS st_atual
    FROM    tb_matricula mt
            JOIN tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
            JOIN tb_entidade AS et ON et.id_entidade = mt.id_entidadeatendimento
            LEFT JOIN tb_entidadeendereco AS ete ON ete.id_entidade = et.id_entidade
                                                    AND ete.bl_padrao = 1
            LEFT JOIN tb_endereco AS etee ON etee.id_endereco = ete.id_endereco
            JOIN vw_pessoa AS ps ON ps.id_usuario = mt.id_usuario
                                    AND ps.id_entidade = mt.id_entidadeatendimento
            LEFT JOIN tb_fundamentolegal AS fl ON fl.id_entidade = mt.id_entidadematriz
                                                  AND fl.id_tipofundamentolegal = 1
                                                  AND fl.dt_publicacao <= GETDATE()
                                                  AND fl.dt_vigencia >= GETDATE()
            LEFT JOIN tb_fundamentolegal AS flp ON flp.id_entidade = mt.id_entidadematriz
                                                   AND flp.id_tipofundamentolegal = 2
                                                   AND flp.dt_publicacao <= GETDATE()
                                                   AND flp.dt_vigencia >= GETDATE()
            LEFT JOIN tb_fundamentolegal AS flr ON flr.id_entidade = mt.id_entidadematriz
                                                   AND flr.id_tipofundamentolegal = 3
                                                   AND flr.dt_publicacao <= GETDATE()
                                                   AND flr.dt_vigencia >= GETDATE()
            LEFT JOIN tb_fundamentolegal AS flpr ON flpr.id_entidade = mt.id_entidadematriz
                                                    AND flpr.id_tipofundamentolegal = 4
                                                    AND flpr.dt_publicacao <= GETDATE()
                                                    AND flpr.dt_vigencia >= GETDATE()
            LEFT JOIN tb_projetopedagogicoserienivelensino AS psn ON psn.id_projetopedagogico = mt.id_projetopedagogico
            LEFT JOIN tb_turma AS tur ON tur.id_turma = mt.id_turma
            JOIN tb_nivelensino AS ne ON ne.id_nivelensino = psn.id_nivelensino
            LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS uper ON uper.id_projetopedagogico = pp.id_projetopedagogico
                                                              AND uper.bl_ativo = 1
                                                              AND uper.bl_titular = 1
            LEFT JOIN dbo.tb_usuario AS us_coordenador ON us_coordenador.id_usuario = uper.id_usuario
            OUTER APPLY ( SELECT TOP 1
                                    ts.dt_abertura
                          FROM      tb_matriculadisciplina tm
                                    JOIN tb_alocacao ta ON tm.id_matriculadisciplina = ta.id_matriculadisciplina
                                    JOIN tb_saladeaula ts ON ta.id_saladeaula = ts.id_saladeaula
                          WHERE     tm.id_matricula = mt.id_matricula
                          ORDER BY  ts.dt_abertura DESC
                        ) AS sa
            LEFT JOIN dbo.vw_avaliacaoaluno av ON ( av.id_matricula = mt.id_matricula
                                                    AND av.id_tipoavaliacao = 6
                                                    AND av.st_tituloavaliacao IS NOT NULL
                                                    AND id_tipodisciplina = 2
                                                    AND av.bl_ativo = 1
                                                  )
            LEFT JOIN tb_cancelamento AS canc ON canc.id_cancelamento = mt.id_cancelamento
            LEFT JOIN tb_contratomatricula AS cm ON cm.id_matricula = mt.id_matricula





GO




CREATE VIEW dbo.vw_gerarapacotealunos
AS
    SELECT  mt.id_matricula ,
            pp.id_projetopedagogico ,
            it.id_itemdematerial ,
            it.id_upload ,
            it.id_situacao AS id_situacaoitem ,
            md.id_matriculadisciplina ,
            id.id_disciplina ,
            mt.id_entidadeatendimento AS id_entidade ,
            a.id_areaconhecimento
    FROM    tb_matricula mt
            JOIN tb_projetopedagogico pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
            JOIN tb_areaprojetopedagogico app ON app.id_projetopedagogico = pp.id_projetopedagogico
            JOIN tb_areaconhecimento a ON a.id_areaconhecimento = app.id_areaconhecimento
            JOIN tb_matriculadisciplina md ON md.id_matricula = mt.id_matricula
                                              AND id_pacote IS NULL
            JOIN tb_itemdematerialdisciplina AS id ON id.id_disciplina = md.id_disciplina
            JOIN tb_itemdematerial AS it ON it.id_itemdematerial = id.id_itemdematerial
                                            AND it.id_situacao = 35
            JOIN dbo.tb_materialprojeto AS mp ON mp.id_itemdematerial = it.id_itemdematerial
                                                 AND mp.id_projetopedagogico = pp.id_projetopedagogico
    WHERE   mt.id_evolucao = 6

GO


/****** Object:  View [dbo].[vw_gradenota]    Script Date: 04/03/2015 08:20:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER TABLE dbo.tb_projetopedagogico ADD bl_disciplinacomplementar BIT NOT NULL DEFAULT 0
go
ALTER TABLE dbo.tb_matricula ADD id_matriculavinculada int NULL 


ALTER VIEW [dbo].[vw_gradenota]
AS
    SELECT DISTINCT
            sa.dt_abertura ,
            pp.id_projetopedagogico ,
            pp.st_projetopedagogico ,
            sa.st_saladeaula ,
            mn.st_notatcc ,
            mn.id_tiponotatcc ,
            mn.st_notaead ,
            mn.id_tiponotaead ,
            mn.st_notafinal ,
            mn.id_tiponotafinal ,
            dc.id_disciplina ,
            dc.st_disciplina ,
            dc.id_tipodisciplina ,
            dc.st_tituloexibicao AS st_tituloexibicaodisciplina ,
            mt.id_matricula ,
            md.id_matriculadisciplina ,
            acr.id_avaliacaoconjuntoreferencia ,
            CAST(( md.nu_aprovafinal * pp.nu_notamaxima / 100 ) AS INT) AS nu_notafinal ,
            ev.id_evolucao ,
            ev.st_evolucao ,
            st.id_situacao ,
            st.st_situacao ,
            dc.nu_cargahoraria ,
            sa.id_saladeaula ,
            sa.dt_encerramento ,
            mn.nu_notatotal ,
            sa.id_categoriasala ,
            st_status = CASE WHEN ea.id_alocacao IS NOT NULL
                                  AND md.id_evolucao = 12 THEN ev.st_evolucao
                             WHEN ea.id_alocacao IS NOT NULL
                                  AND md.id_evolucao = 19 THEN ev.st_evolucao
                             WHEN ea.id_alocacao IS NOT NULL
                                  AND md.id_situacao = 65
                             THEN 'Crédito Concedido'
                             WHEN ea.id_alocacao IS NOT NULL
                                  AND ( mn.nu_notatotal + nf.nu_notafaltante ) >= ( pp.nu_notamaxima
                                                              * pp.nu_percentualaprovacao
                                                              / 100 )
                             THEN 'Satisfatório'
                             WHEN ea.id_alocacao IS NOT NULL
                                  AND ( mn.nu_notatotal + nf.nu_notafaltante ) < ( pp.nu_notamaxima
                                                              * pp.nu_percentualaprovacao
                                                              / 100 )
                             THEN 'Insatisfatório'
                             WHEN ea.id_alocacao IS NULL
                                  AND ea.id_encerramentosala IS NULL
                             THEN 'Não encerrado'
                             ELSE '-'
                        END ,
            bl_status = CASE WHEN md.bl_obrigatorio = 0 THEN 1
                             WHEN ( mn.nu_notatotal
                                    + ISNULL(nf.nu_notafaltante, 0) ) >= ( pp.nu_notamaxima
                                                              * pp.nu_percentualaprovacao
                                                              / 100 ) THEN 1
                             WHEN ( mn.nu_notatotal
                                    + ISNULL(nf.nu_notafaltante, 0) ) < ( pp.nu_notamaxima
                                                              * pp.nu_percentualaprovacao
                                                              / 100 ) THEN 0
                             ELSE 0
                        END ,
            bl_complementar = CASE WHEN ( md.id_matricula != md.id_matriculaoriginal )
                                        AND ppcompl.bl_disciplinacomplementar = 1
                                   THEN 1
                                   ELSE 0
                              END
    FROM    dbo.tb_matriculadisciplina AS md
            JOIN dbo.tb_matricula AS mt ON md.id_matricula = mt.id_matricula
            JOIN dbo.tb_disciplina AS dc ON dc.id_disciplina = md.id_disciplina
                                            AND dc.id_tipodisciplina <> 3
            JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
            LEFT JOIN dbo.tb_alocacao AS al ON al.id_matriculadisciplina = md.id_matriculadisciplina
                                               AND al.bl_ativo = 1
            LEFT JOIN dbo.tb_avaliacaoconjuntoreferencia AS acr ON acr.id_saladeaula = al.id_saladeaula --AND acr.dt_fim IS NOT null
            LEFT JOIN tb_encerramentoalocacao AS ea ON ea.id_alocacao = al.id_alocacao
            LEFT JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula
            JOIN tb_evolucao AS ev ON ev.id_evolucao = md.id_evolucao
            JOIN dbo.tb_situacao AS st ON st.id_situacao = md.id_situacao
            LEFT JOIN dbo.vw_matriculanota AS mn ON md.id_matriculadisciplina = mn.id_matriculadisciplina
                                                    AND mn.id_alocacao = al.id_alocacao
            OUTER APPLY ( SELECT    id_matricula ,
                                    id_disciplina ,
                                    SUM(CAST(nu_notamax AS FLOAT)) AS nu_notafaltante
                          FROM      vw_avaliacaoaluno
                          WHERE     bl_ativo IS NULL
                                    AND id_matricula = mt.id_matricula
                                    AND id_disciplina = md.id_disciplina
                                    AND id_tipoprova = 2
                          GROUP BY  id_disciplina ,
                                    id_matricula
                        ) AS nf
            JOIN tb_matricula AS matcompl ON matcompl.id_matricula = md.id_matriculaoriginal
            JOIN tb_projetopedagogico AS ppcompl ON ppcompl.id_projetopedagogico = matcompl.id_projetopedagogico



GO




ALTER VIEW [dbo].[vw_gridcertificado]
AS
    SELECT  mt.id_matricula ,
            mt.id_projetopedagogico ,
            pp.st_tituloexibicao ,
            CAST(mtd.id_disciplina AS VARCHAR(200)) AS id_disciplina ,
            dc.st_tituloexibicao AS st_disciplina ,
            mtd.nu_aprovafinal AS nu_aprovafinal ,
            pp.nu_notamaxima ,
            mtd.dt_conclusao AS dt_conclusaodisciplina ,
            dc.nu_cargahoraria AS nu_cargahoraria ,
            mtd.id_evolucao ,
            ev.st_evolucao ,
            mtd.id_situacao ,
            CASE WHEN CAST(mtd.nu_aprovafinal AS INT) < 70 THEN 'Insuficiente'
                 WHEN CAST(mtd.nu_aprovafinal AS INT) < 80 THEN 'Bom'
                 WHEN CAST(mtd.nu_aprovafinal AS INT) < 90 THEN 'Otimo'
                 WHEN CAST(mtd.nu_aprovafinal AS INT) <= 100 THEN 'Excelente'
                 ELSE 'Insuficiente'
            END AS st_conceito ,
            us.st_nomecompleto AS st_titularcertificacao ,
            tit.st_titulacao ,
            mt.id_entidadematricula ,
            mt.dt_concluinte
    FROM    tb_matricula AS mt
            JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
            JOIN tb_matriculadisciplina AS mtd ON mtd.id_matricula = mt.id_matricula
                                                  AND mtd.bl_obrigatorio = 1
            JOIN tb_matricula AS mt2 ON mt2.id_matricula = mtd.id_matriculaoriginal
            LEFT JOIN tb_aproveitamentomatriculadisciplina AS apm ON apm.id_matricula = mtd.id_matricula
                                                              AND apm.id_disciplina = mtd.id_disciplina
            LEFT JOIN tb_aproveitamentodisciplina AS apd ON apd.id_aproveitamentodisciplina = apm.id_aproveitamentodisciplina
            JOIN tb_disciplina AS dc ON dc.id_disciplina = mtd.id_disciplina
            JOIN tb_entidade AS ent ON ent.id_entidade = mt.id_entidadematriz
            JOIN tb_evolucao AS ev ON ev.id_evolucao = mtd.id_evolucao
            LEFT JOIN tb_perfil AS pf ON pf.id_entidade = mt.id_entidadematricula
                                         AND pf.id_perfilpedagogico = 9
            LEFT JOIN tb_usuarioperfilentidadereferencia AS uper ON uper.id_projetopedagogico = mt2.id_projetopedagogico
                                                              AND uper.id_disciplina = mtd.id_disciplina
                                                              AND uper.id_perfil = pf.id_perfil
                                                              AND ( ( mt.dt_concluinte BETWEEN uper.dt_inicio
                                                              AND
                                                              uper.dt_fim )
                                                              OR ( mt.dt_concluinte > uper.dt_inicio
                                                              AND uper.dt_fim IS NULL
                                                              )
                                                              )
                                                              AND uper.bl_titular = 1
                                                              AND uper.bl_ativo = 1
            LEFT JOIN tb_usuario AS us ON us.id_usuario = uper.id_usuario
            LEFT JOIN tb_titulacao tit ON uper.id_titulacao = tit.id_titulacao


GO


ALTER VIEW [dbo].[vw_matricula]
AS
    SELECT  mt.id_usuario ,
            us.st_nomecompleto ,
            us.st_cpf ,
            ps.st_urlavatar ,
            ps.dt_nascimento ,
            ps.st_nomepai ,
            ps.st_nomemae ,
            ps.st_email ,
            CAST(ps.nu_ddd AS VARCHAR) + '-' + CAST(ps.nu_telefone AS VARCHAR) AS st_telefone ,
            CAST(ps.nu_dddalternativo AS VARCHAR) + '-'
            + CAST(ps.nu_telefonealternativo AS VARCHAR) AS st_telefonealternativo ,
            mt.id_matricula ,
            pp.st_projetopedagogico ,
            mt.id_situacao ,
            st.st_situacao ,
            mt.id_evolucao ,
            ev.st_evolucao ,
            mt.dt_concluinte ,
            mt.id_entidadematriz ,
            etmz.st_nomeentidade AS st_entidadematriz ,
            mt.id_entidadematricula ,
            etm.st_nomeentidade AS st_entidadematricula ,
            mt.id_entidadeatendimento ,
            eta.st_nomeentidade AS st_entidadeatendimento ,
            mt.bl_ativo ,
            cm.id_contrato ,
            pp.id_projetopedagogico ,
            ct.dt_termino ,
            mt.dt_cadastro ,
            mt.dt_inicio ,
            tm.id_turma ,
            tm.st_turma ,
            tm.dt_inicio AS dt_inicioturma ,
            mt.bl_institucional ,
            tm.dt_fim AS dt_terminoturma ,
            mt.id_situacaoagendamento ,
            mt.id_vendaproduto ,
            mt.dt_termino AS dt_terminomatricula ,
            CAST (RIGHT(master.dbo.fn_varbintohexstr(HASHBYTES('MD5',
                                                              ( CAST (us.id_usuario AS VARCHAR(10)) COLLATE Latin1_General_CI_AI
                                                              + CAST (( CASE
                                                              WHEN mt.id_matricula IS NULL
                                                              THEN 0
                                                              ELSE mt.id_matricula
                                                              END ) AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
                                                              + CAST (5 AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
                                                              + CAST (( 0 ) AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
                                                              + us.st_login
                                                              + us.st_senha
                                                              + CONVERT (VARCHAR(10), GETDATE(), 103) ))),
                        32) AS CHAR(32)) COLLATE Latin1_General_CI_AI AS st_urlacesso ,
            mt.bl_documentacao ,
            ct.id_venda ,
            eta.st_urlportal ,
            mt.st_codcertificacao ,
            ps.st_identificacao ,
            CONVERT(VARCHAR, mtc.dt_cadastro, 103) AS st_dtcadastrocertificacao ,
            CONVERT(VARCHAR, mtc.dt_enviocertificado, 103) AS st_dtenviocertificado ,
            CONVERT(VARCHAR, mtc.dt_retornocertificadora, 103) AS st_dtretornocertificadora ,
            CONVERT(VARCHAR, mtc.dt_envioaluno, 103) AS st_dtenvioaluno ,
            st_codigoacompanhamento ,
            mtc.id_indicecertificado ,
            mt.id_evolucaocertificacao ,
            ap.id_areaconhecimento ,
            pp.bl_disciplinacomplementar ,
            mt.id_matriculavinculada
    FROM    tb_matricula mt
            JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
            JOIN tb_areaprojetopedagogico AS ap ON ap.id_projetopedagogico = pp.id_projetopedagogico
            JOIN tb_entidade AS etmz ON etmz.id_entidade = mt.id_entidadematriz
            JOIN tb_entidade AS etm ON etm.id_entidade = mt.id_entidadematricula
            JOIN tb_entidade AS eta ON eta.id_entidade = mt.id_entidadeatendimento
            JOIN tb_situacao AS st ON st.id_situacao = mt.id_situacao
            JOIN tb_evolucao AS ev ON ev.id_evolucao = mt.id_evolucao
            JOIN tb_usuario AS us ON us.id_usuario = mt.id_usuario
            LEFT JOIN tb_contratomatricula AS cm ON cm.id_matricula = mt.id_matricula
                                                    AND cm.bl_ativo = 1
            LEFT JOIN vw_pessoa AS ps ON mt.id_usuario = ps.id_usuario
                                         AND mt.id_entidadeatendimento = ps.id_entidade
            LEFT JOIN dbo.tb_contrato AS ct ON ct.id_contrato = cm.id_contrato
                                               AND ct.bl_ativo = 1
            LEFT JOIN tb_turma AS tm ON tm.id_turma = mt.id_turma
            LEFT JOIN tb_matriculacertificacao mtc ON mtc.id_matricula = mt.id_matricula
                                                      AND mtc.bl_ativo = 1
    WHERE   mt.bl_ativo = 1

GO

/****** Object:  View [dbo].[vw_usuarioperfilpedagogico]    Script Date: 20/10/2014 09:46:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





ALTER VIEW [dbo].[vw_usuarioperfilpedagogico]
AS
    SELECT DISTINCT
            us.st_nomecompleto ,
            us.id_usuario ,
            us.st_login ,
            us.st_senha ,
            dac.st_login AS st_loginentidade ,
            dac.st_senha AS st_senhaentidade ,
            us.st_email ,
            ps.st_nomeexibicao ,
            ps.st_urlavatar ,
            ps.dt_nascimento ,
            upe.id_perfil ,
            upe.id_entidade ,
            pf.st_nomeperfil ,
            ppg.st_perfilpedagogico ,
            pf.id_perfilpedagogico ,
            upe.dt_inicio ,
            upe.dt_termino ,
            pp.id_projetopedagogico ,
            pp.id_trilha ,
            pp.st_tituloexibicao ,
            uper.id_saladeaula ,
            NULL AS st_saladeaula ,
            mt.id_matricula ,
            mt.id_evolucao AS id_evolucaomatricula ,
            tm.dt_inicio AS dt_inicioturma ,
            et.st_nomeentidade ,
            et.st_razaosocial ,
            et.st_urlimglogo ,
            ce.bl_bloqueiadisciplina ,
            CAST (RIGHT(master.dbo.fn_varbintohexstr(HASHBYTES('MD5',
                                                              ( CAST (us.id_usuario AS VARCHAR(10)) COLLATE Latin1_General_CI_AI
                                                              + CAST (( CASE
                                                              WHEN mt.id_matricula IS NULL
                                                              THEN 0
                                                              ELSE mt.id_matricula
                                                              END ) AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
                                                              + CAST (pf.id_perfilpedagogico AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
                                                              + CAST (( 0 ) AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
                                                              + us.st_login
                                                              + us.st_senha
                                                              + CONVERT (VARCHAR(10), GETDATE(), 103) ))),
                        32) AS CHAR(32)) COLLATE Latin1_General_CI_AI AS st_urlacesso ,
            cd.id_entidadeorigem AS id_entidadeportal
    FROM    vw_pessoa us
            JOIN tb_usuarioperfilentidade AS upe ON upe.id_usuario = us.id_usuario
                                                    AND us.id_entidade = upe.id_entidade
            JOIN tb_perfil AS pf ON pf.id_perfil = upe.id_perfil
                                    AND pf.id_entidade = upe.id_entidade
            JOIN tb_perfilpedagogico AS ppg ON ppg.id_perfilpedagogico = pf.id_perfilpedagogico
            LEFT JOIN tb_usuarioperfilentidadereferencia AS uper ON uper.id_usuario = upe.id_usuario
                                                              AND uper.id_perfil = upe.id_perfil
                                                              AND upe.id_entidade = uper.id_entidade
                                                              AND pf.id_entidade = uper.id_entidade
            LEFT JOIN tb_matricula AS mt ON mt.id_usuario = us.id_usuario
                                            AND mt.id_entidadeatendimento = upe.id_entidade
                                            AND ( ( pf.id_perfilpedagogico = 5
                                                    AND mt.bl_institucional = 0
                                                  )
                                                  OR ( pf.id_perfilpedagogico = 6
                                                       AND mt.bl_institucional = 1
                                                     )
                                                )
                                            AND mt.bl_ativo = 1
                                            AND mt.id_evolucao NOT IN ( 20, 27,
                                                              41 )
                                            AND ( mt.dt_termino IS NULL
                                                  OR mt.dt_termino >= CAST (GETDATE() AS DATE)
                                                )
            LEFT JOIN tb_turma AS tm ON mt.id_turma = tm.id_turma
            LEFT JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
            JOIN tb_entidade AS et ON et.id_entidade = upe.id_entidade
            JOIN tb_pessoa AS ps ON ps.id_usuario = us.id_usuario
                                    AND ps.id_entidade = upe.id_entidade
            LEFT JOIN tb_dadosacesso AS dac ON dac.id_usuario = us.id_usuario
                                               AND dac.id_entidade = ps.id_entidade
                                               AND dac.bl_ativo = 1
            LEFT JOIN tb_configuracaoentidade AS ce ON ce.id_entidade = et.id_entidade
            LEFT JOIN dbo.tb_compartilhadados AS cd ON cd.id_entidadedestino = et.id_entidade
                                                       AND cd.id_tipodados = 2
    WHERE   upe.bl_ativo = 1
            AND pf.bl_ativo = 1
            AND upe.id_situacao = 15
            AND pf.id_situacao = 4

GO

/****** Object:  View [rel].[vw_atendentenegociacao]    Script Date: 12/03/2015 14:16:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [rel].[vw_atendentenegociacao]
AS
    SELECT  U.st_nomecompleto AS 'st_nomealuno' ,
            US.st_nomecompleto AS 'st_nomeatendente' ,
            US.id_usuario AS 'id_atendente' ,
            vd.id_venda ,
            p.st_produto ,
            vd.nu_parcelas ,
            vp.nu_valorliquido ,
            CASE WHEN vd.nu_valorliquido > 0
                 THEN ( vdlc.nu_valorcartao / vd.nu_valorliquido
                        * vp.nu_valorliquido )
                 ELSE 0
            END AS nu_valorcartao ,
            CASE WHEN vd.nu_valorliquido > 0
                 THEN ( vdlb.nu_valorboleto / vd.nu_valorliquido
                        * vp.nu_valorliquido )
                 ELSE 0
            END AS nu_valorboleto ,
            CASE WHEN vd.nu_valorliquido > 0
                 THEN ( vdld.nu_valordinheiro / vd.nu_valorliquido
                        * vp.nu_valorliquido )
                 ELSE 0
            END AS nu_valordinheiro ,
            CASE WHEN vd.nu_valorliquido > 0
                 THEN ( vdlch.nu_valorcheque / vd.nu_valorliquido
                        * vp.nu_valorliquido )
                 ELSE 0
            END AS nu_valorcheque ,
            CASE WHEN vd.nu_valorliquido > 0
                 THEN ( vdle.nu_valorempenho / vd.nu_valorliquido
                        * vp.nu_valorliquido )
                 ELSE 0
            END AS nu_valorempenho ,
            CASE WHEN vd.nu_valorliquido > 0
                 THEN ( vdldp.nu_valordeposito / vd.nu_valorliquido
                        * vp.nu_valorliquido )
                 ELSE 0
            END AS nu_valordeposito ,
            CASE WHEN vd.nu_valorliquido > 0
                 THEN ( vdlt.nu_valortransferencia / vd.nu_valorliquido
                        * vp.nu_valorliquido )
                 ELSE 0
            END AS nu_valortransferencia ,
            CASE WHEN vd.nu_valorliquido > 0
                 THEN ( vdlp.nu_valorpermuta / vd.nu_valorliquido
                        * vp.nu_valorliquido )
                 ELSE 0
            END AS nu_valorpermuta ,
            CASE WHEN vd.nu_valorliquido > 0
                 THEN ( vdlbl.nu_valorbolsa / vd.nu_valorliquido
                        * vp.nu_valorliquido )
                 ELSE 0
            END AS nu_valorbolsa ,
            CASE WHEN vd.nu_valorliquido > 0
                 THEN ( vdlec.nu_valorestorno / vd.nu_valorliquido
                        * vp.nu_valorliquido )
                 ELSE 0
            END AS nu_valorestorno ,
            CASE WHEN vd.nu_valorliquido > 0
                 THEN ( vdlcc.nu_valorcarta / vd.nu_valorliquido
                        * vp.nu_valorliquido )
                 ELSE 0
            END AS nu_valorcarta ,
            E.st_nomeentidade ,
            E.id_entidade ,
            vd.dt_cadastro
    FROM    dbo.tb_venda AS vd
            OUTER APPLY ( SELECT    SUM(nu_valor) AS nu_valorcartao
                          FROM      dbo.vw_vendalancamento
                          WHERE     id_venda = vd.id_venda
                                    AND id_meiopagamento IN ( 1, 7, 11 )
                        ) AS vdlc
            OUTER APPLY ( SELECT    SUM(nu_valor) AS nu_valorboleto
                          FROM      dbo.vw_vendalancamento
                          WHERE     id_venda = vd.id_venda
                                    AND id_meiopagamento = 2
                        ) AS vdlb
            OUTER APPLY ( SELECT    SUM(nu_valor) AS nu_valordinheiro
                          FROM      dbo.vw_vendalancamento
                          WHERE     id_venda = vd.id_venda
                                    AND id_meiopagamento = 3
                        ) AS vdld
            OUTER APPLY ( SELECT    SUM(nu_valor) AS nu_valorcheque
                          FROM      dbo.vw_vendalancamento
                          WHERE     id_venda = vd.id_venda
                                    AND id_meiopagamento = 4
                        ) AS vdlch
            OUTER APPLY ( SELECT    SUM(nu_valor) AS nu_valorempenho
                          FROM      dbo.vw_vendalancamento
                          WHERE     id_venda = vd.id_venda
                                    AND id_meiopagamento = 5
                        ) AS vdle
            OUTER APPLY ( SELECT    SUM(nu_valor) AS nu_valordeposito
                          FROM      dbo.vw_vendalancamento
                          WHERE     id_venda = vd.id_venda
                                    AND id_meiopagamento = 6
                        ) AS vdldp
            OUTER APPLY ( SELECT    SUM(nu_valor) AS nu_valortransferencia
                          FROM      dbo.vw_vendalancamento
                          WHERE     id_venda = vd.id_venda
                                    AND id_meiopagamento = 8
                        ) AS vdlt
            OUTER APPLY ( SELECT    SUM(nu_valor) AS nu_valorpermuta
                          FROM      dbo.vw_vendalancamento
                          WHERE     id_venda = vd.id_venda
                                    AND id_meiopagamento = 9
                        ) AS vdlp
            OUTER APPLY ( SELECT    SUM(nu_valor) AS nu_valorbolsa
                          FROM      dbo.vw_vendalancamento
                          WHERE     id_venda = vd.id_venda
                                    AND id_meiopagamento = 10
                        ) AS vdlbl
            OUTER APPLY ( SELECT    SUM(nu_valor) AS nu_valorestorno
                          FROM      dbo.vw_vendalancamento
                          WHERE     id_venda = vd.id_venda
                                    AND id_meiopagamento = 12
                        ) AS vdlec
            OUTER APPLY ( SELECT    SUM(nu_valor) AS nu_valorcarta
                          FROM      dbo.vw_vendalancamento
                          WHERE     id_venda = vd.id_venda
                                    AND id_meiopagamento = 13
                        ) AS vdlcc
            JOIN dbo.tb_entidade AS E ON E.id_entidade = vd.id_entidade
            JOIN dbo.tb_vendaproduto AS vp ON vp.id_venda = vd.id_venda
            JOIN dbo.tb_matricula AS mt ON mt.id_matricula = vp.id_matricula
            JOIN dbo.tb_produto AS p ON p.id_produto = vp.id_produto
                                        AND mt.id_evolucao = 6
            JOIN dbo.tb_usuario AS U ON U.id_usuario = vd.id_usuario
            LEFT JOIN dbo.tb_usuario AS US ON US.id_usuario = vd.id_atendente

	

GO

CREATE VIEW vw_gerarpacotealunos as

select mt.id_matricula
,pp.id_projetopedagogico
,it.id_itemdematerial
,it.id_upload
,it.id_situacao as id_situacaoitem
,md.id_matriculadisciplina
,id.id_disciplina
,mt.id_entidadeatendimento as id_entidade
,a.id_areaconhecimento

from 
tb_matricula mt
join tb_projetopedagogico pp on pp.id_projetopedagogico=mt.id_projetopedagogico
join tb_areaprojetopedagogico app on app.id_projetopedagogico = pp.id_projetopedagogico
join tb_areaconhecimento a on a.id_areaconhecimento = app.id_areaconhecimento 
join tb_matriculadisciplina md on md.id_matricula=mt.id_matricula and id_pacote is null
join tb_itemdematerialdisciplina as id on id.id_disciplina=md.id_disciplina
join tb_itemdematerial as it on it.id_itemdematerial=id.id_itemdematerial AND it.id_situacao = 35
JOIN dbo.tb_materialprojeto AS mp ON mp.id_itemdematerial = it.id_itemdematerial AND mp.id_projetopedagogico = pp.id_projetopedagogico
where mt.id_evolucao=6;



alter VIEW [totvs].[vw_Integra_GestorFluxus] AS

SELECT
ei.st_codsistema AS CODCOLIGADA_FCFO,
CASE WHEN lanint.st_codresponsavel IS NOT NULL THEN lanint.st_codresponsavel ELSE NULL END AS CODCFO_FCFO,
replace(usr.st_nomecompleto,'''','') COLLATE SQL_Latin1_General_CP1_CI_AI AS NOMEFANTASIA_FCFO,
replace(usr.st_nomecompleto,'''','') COLLATE SQL_Latin1_General_CP1_CI_AI AS NOME_FCFO,
usr.st_cpf COLLATE SQL_Latin1_General_CP1_CI_AI AS CGCCFO_FCFO,
NULL AS INSCRESTADUAL_FCFO,
'1' AS PAGREC_FCFO,
replace(ed.st_endereco,'''','') AS RUA_FCFO,
ed.nu_numero AS NUMERO_FCFO,
CAST(replace(ed.st_complemento,'''','') AS VARCHAR(30)) AS COMPLEMENTO_FCFO,
CAST(replace(ed.st_bairro,'''','') AS VARCHAR(30)) AS BAIRRO_FCFO,
replace(ed.st_cidade,'''','') AS CIDADE_FCFO,
CODETD_FCFO = CASE WHEN ed.sg_uf IS NOT NULL  THEN ed.sg_uf
ELSE 'DF' END,
REPLACE(REPLACE(ed.st_cep,'.',''),'-','') AS CEP_FCFO,
CAST(ctl.nu_ddd AS VARCHAR)+ ' ' + CAST(ctl.nu_telefone AS VARCHAR) AS TELEFONE_FCFO,
NULL AS FAX_FCFO,
replace(ce.st_email,'''','') AS EMAIL_FCFO,
NULL AS CONTATO_FCFO,
CASE WHEN ps.id_situacao = 1 THEN 1 ELSE 0 END AS ATIVO_FCFO,
mun.nu_codigoibge AS CODMUNICIPIO_FCFO,
CASE WHEN ps.id_usuario IS NOT NULL THEN 'F' ELSE 'J' END AS PESSOAFISOUJUR_FCFO,
pa.st_nomepais AS PAIS_FCFO,
NULL AS IDCFO_FCFO,
NULL AS CEI_FCFO,
CASE WHEN ps.id_pais = 22 THEN 0 ELSE 1 END AS NACIONALIDADE_FCFO,
NULL AS TIPOCONTRIBUINTEINSS_FCFO,
CASE WHEN lanint.st_codlancamento IS NOT NULL THEN lanint.st_codlancamento ELSE NULL END AS IDLAN,
CAST (REPLICATE('0',20 -LEN(lc.id_lancamento)) AS VARCHAR) + CAST(lc.id_lancamento AS VARCHAR) AS NUMERODOCUMENTO,
1 AS PAGREC,
CASE WHEN (lc.bl_quitado = 1 AND lc.id_meiopagamento <> 4) OR (lv.bl_entrada = 1 AND lc.id_meiopagamento = 1) THEN 1 ELSE 0 END AS STATUSLAN,
CAST(vd.dt_confirmacao AS DATETIME) AS DATAEMISSAO,
--CASE WHEN vd.dt_confirmacao IS NOT NULL THEN CAST ( ct.dt_ativacao AS DATE)ELSE  CAST (GETDATE() AS DATE) END AS DATAEMISSAO,
--CASE WHEN ct.dt_ativacao IS NOT NULL THEN CONVERT(nvarchar(30), cast ( ct.dt_ativacao as date), 103) ELSE CONVERT(nvarchar(30), cast (GETDATE() as date), 103) END as DATAEMISSAO,
CAST(dt_prevquitado AS DATE) AS DATAPREVBAIXA,
--CONVERT(nvarchar(30),CAST (lc.dt_prevquitado AS DATE),103) as DATAPREVBAIXA,
--CONVERT(nvarchar(30),lc.dt_quitado,103) as DATABAIXA,
CASE WHEN (lc.dt_quitado IS not NULL AND lc.id_meiopagamento <> 4) THEN CAST(lc.dt_quitado AS DATETIME)
WHEN  (lc.dt_quitado IS NULL AND lv.bl_entrada = 1 AND lc.id_meiopagamento = 1) THEN CAST (lc.dt_vencimento AS DATETIME)
WHEN (lc.dt_quitado IS not NULL AND lc.id_meiopagamento = 4) THEN null
END AS DATABAIXA,
lc.nu_valor AS VALORORIGINAL,
CASE when lc.nu_quitado is NOT NULL THEN lc.nu_quitado
WHEN lc.nu_quitado IS NULL AND lv.bl_entrada = 1 AND lc.id_meiopagamento = 1 THEN lc.nu_valor ELSE 0
end AS VALORBAIXADO,
0 AS VALORCAP,
ISNULL(lc.nu_juros, 0) AS VALORJUROS,
ISNULL(lc.nu_desconto,0) AS VALORDESCONTO,
0 AS VALOROP1,
0 AS VALOROP2,
0 AS VALOROP3,
ISNULL( lc.nu_multa, 0) AS VALORMULTA,
0 AS JUROSDIA,
0 AS CAPMENSAL,
0 AS TAXASVENDOR,
0 AS JUROSVENDOR,
mpe.st_codcontacaixa AS CODCXA, -- conta caixa
'034' AS CODTDO, -- Código do tipo de documento
CASE WHEN lv.bl_entrada = 1 THEN
'01.01.0001'
ELSE
'01.01.0002'
END AS CODTB1FLX, -- Categoria
CASE WHEN (lc.id_cartaoconfig IS NULL AND lc.id_meiopagamento <> 4)  THEN
(SELECT DISTINCT TOP 1 mpi.st_codsistema FROM dbo.tb_meiopagamentointegracao mpi WHERE mpi.id_meiopagamento = lc.id_meiopagamento  AND mpi.id_sistema = 3)
WHEN (lc.id_cartaoconfig IS NULL AND lc.id_meiopagamento = 4 AND lv.bl_entrada = 1)  THEN '0002'
WHEN (lc.id_cartaoconfig IS NULL AND lc.id_meiopagamento = 4 AND lv.bl_entrada <> 1)  THEN '0003'
ELSE
(SELECT DISTINCT TOP 1 mpi.st_codsistema FROM dbo.tb_meiopagamentointegracao mpi WHERE mpi.id_meiopagamento = lc.id_meiopagamento  AND mpi.id_sistema = 3 AND lc.id_cartaoconfig = cc.id_cartaoconfig AND mpi.id_cartaobandeira = cc.id_cartaobandeira)
END AS CODTB2FLX, -- Forma de pagamento
CAST (ct.dt_ativacao AS DATE) AS DATAOP2,
CAST (vd.id_venda AS VARCHAR) + 'G2U' AS CAMPOALFAOP1,
CAST (lc.id_lancamento AS VARCHAR) + 'G2U' AS CAMPOALFAOP2,
'Lancamento gerado pelo Gestor2 as '+CONVERT(VARCHAR(8), GETDATE(),  114 ) + ' de ' + CONVERT(VARCHAR, GETDATE(),  103  ) AS HISTORICO,
ei.st_codchave AS CODCCUSTO,
NULL AS NUMEROCHEQUE,
CAST (lc.dt_vencimento AS DATE) AS DATAVENCIMENTO,
NULL AS DATAVENCIMENTO_CHEQUE,
NULL AS CODIGOBANCO,
NULL AS CODIGOAGENCIA,
NULL AS CONTACORRENTE,
NULL AS COMPENSADO,
vd.id_venda,
ct.id_contrato,
lv.id_lancamento,
lv.bl_entrada,
lv.nu_ordem,
vd.id_entidade,
ei.st_caminho, ei.st_linkedserver
FROM tb_venda AS vd
LEFT JOIN tb_contrato AS ct ON vd.id_venda = ct.id_venda
JOIN tb_lancamentovenda AS lv ON lv.id_venda = vd.id_venda
JOIN tb_lancamento AS lc ON lc.id_lancamento = lv.id_lancamento
JOIN tb_usuario AS usr ON usr.id_usuario = lc.id_usuariolancamento
JOIN tb_pessoa AS ps ON ps.id_usuario = usr.id_usuario AND ps.id_entidade = vd.id_entidade
JOIN tb_entidadeintegracao AS ei ON ei.id_entidade  = vd.id_entidade AND ei.id_sistema = 3
CROSS APPLY (SELECT TOP 1 id_meiopagamentointegracao ,
id_cartaobandeira ,
id_sistema ,
id_usuariocadastro ,
id_meiopagamento ,
id_entidade ,
st_codsistema ,
dt_cadastro, st_codcontacaixa FROM dbo.tb_meiopagamentointegracao WHERE id_meiopagamento = lc.id_meiopagamento AND id_entidade = vd.id_entidade) AS mpe 
LEFT JOIN dbo.tb_cartaoconfig cc ON cc.id_cartaoconfig = lc.id_cartaoconfig
LEFT JOIN tb_pessoaendereco AS pse ON pse.id_usuario = ps.id_usuario AND ps.id_entidade = pse.id_entidade AND pse.bl_padrao = 1
LEFT JOIN tb_endereco AS ed ON ed.id_endereco = pse.id_endereco
LEFT JOIN tb_municipio AS mun ON ed.id_municipio = mun.id_municipio
LEFT JOIN tb_contatostelefonepessoa AS ctp ON ctp.id_usuario = usr.id_usuario AND ctp.id_entidade = ps.id_entidade AND ctp.bl_padrao = 1
LEFT JOIN tb_contatostelefone AS ctl ON ctl.id_telefone = ctp.id_telefone
LEFT JOIN tb_contatosemailpessoaperfil AS cepp ON cepp.id_usuario = usr.id_usuario AND cepp.id_entidade = ps.id_entidade AND cepp.bl_padrao = 1
LEFT JOIN tb_contatosemail AS ce ON ce.id_email = cepp.id_email
LEFT JOIN tb_pais AS pa ON pa.id_pais = ed.id_pais
LEFT JOIN tb_lancamentointegracao lanint ON lanint.id_lancamento = lc.id_lancamento
WHERE vd.id_evolucao = 10 AND lanint.st_codlancamento IS NULL 
and lc.nu_valor <> 0 AND lc.bl_ativo = 1

UNION

SELECT ei.st_codsistema AS CODCOLIGADA_FCFO,
CASE WHEN lanint.st_codresponsavel IS NOT NULL THEN lanint.st_codresponsavel ELSE NULL END AS CODCFO_FCFO,
replace(usr.st_nomeentidade,'''','') AS NOMEFANTASIA_FCFO,
replace(usr.st_razaosocial,'''','') AS NOME_FCFO,
usr.st_cnpj AS CGCCFO_FCFO,
usr.nu_inscricaoestadual AS INSCRESTADUAL_FCFO,
'1' AS PAGREC_FCFO,
replace(ed.st_endereco,'''','') AS RUA_FCFO,
ed.nu_numero AS NUMERO_FCFO,
CAST(replace(ed.st_complemento,'''','') AS VARCHAR(30)) AS COMPLEMENTO_FCFO,
CAST(replace(ed.st_bairro,'''','') AS VARCHAR(30)) AS BAIRRO_FCFO,
replace(ed.st_cidade,'''','') AS CIDADE_FCFO,
CODETD_FCFO = CASE WHEN ed.sg_uf IS NOT NULL  THEN ed.sg_uf
ELSE 'DF' END,
REPLACE(REPLACE(ed.st_cep,'.',''),'-','') AS CEP_FCFO,
CAST(ctl.nu_ddd AS VARCHAR)+ ' ' + CAST(ctl.nu_telefone AS VARCHAR) AS TELEFONE_FCFO,
NULL AS FAX_FCFO,
null AS EMAIL_FCFO,
NULL AS CONTATO_FCFO,
CASE WHEN usr.id_situacao = 1 THEN 1 ELSE 0 END AS ATIVO_FCFO,
mun.nu_codigoibge AS CODMUNICIPIO_FCFO,
'J'  AS PESSOAFISOUJUR_FCFO,
pa.st_nomepais AS PAIS_FCFO,
NULL AS IDCFO_FCFO,
NULL AS CEI_FCFO,
CASE WHEN ed.id_pais = 22 THEN 0 ELSE 1 END AS NACIONALIDADE_FCFO,
NULL AS TIPOCONTRIBUINTEINSS_FCFO,
CASE WHEN lanint.st_codlancamento IS NOT NULL THEN lanint.st_codlancamento ELSE NULL END AS IDLAN,
CAST (REPLICATE('0',20 -LEN(lc.id_lancamento)) AS VARCHAR) + CAST(lc.id_lancamento AS VARCHAR) AS NUMERODOCUMENTO,
1 AS PAGREC,
CASE WHEN (lc.bl_quitado = 1 AND lc.id_meiopagamento <> 4) OR (lv.bl_entrada = 1 AND lc.id_meiopagamento = 1) THEN 1 ELSE 0 END AS STATUSLAN,
CAST(vd.dt_confirmacao AS DATETIME) AS DATAEMISSAO,
--CASE WHEN vd.dt_confirmacao IS NOT NULL THEN CAST ( ct.dt_ativacao AS DATE)ELSE  CAST (GETDATE() AS DATE) END AS DATAEMISSAO,
--CASE WHEN ct.dt_ativacao IS NOT NULL THEN CONVERT(nvarchar(30), cast ( ct.dt_ativacao as date), 103) ELSE CONVERT(nvarchar(30), cast (GETDATE() as date), 103) END as DATAEMISSAO,
CAST(dt_prevquitado AS DATE) AS DATAPREVBAIXA,
--CONVERT(nvarchar(30),CAST (lc.dt_prevquitado AS DATE),103) as DATAPREVBAIXA,
--CONVERT(nvarchar(30),lc.dt_quitado,103) as DATABAIXA,
CASE WHEN (lc.dt_quitado IS not NULL AND lc.id_meiopagamento <> 4) THEN CAST(lc.dt_quitado AS DATETIME)
WHEN  (lc.dt_quitado IS NULL AND lv.bl_entrada = 1 AND lc.id_meiopagamento = 1) THEN CAST (lc.dt_vencimento AS DATETIME)
WHEN (lc.dt_quitado IS not NULL AND lc.id_meiopagamento = 4) THEN null
END AS DATABAIXA,
lc.nu_valor AS VALORORIGINAL,
CASE when lc.nu_quitado is NOT NULL THEN lc.nu_quitado
WHEN lc.nu_quitado IS NULL AND lv.bl_entrada = 1 AND lc.id_meiopagamento = 1 THEN lc.nu_valor ELSE 0
end AS VALORBAIXADO,
0 AS VALORCAP,
ISNULL(lc.nu_juros, 0) AS VALORJUROS,
ISNULL(lc.nu_desconto,0) AS VALORDESCONTO,
0 AS VALOROP1,
0 AS VALOROP2,
0 AS VALOROP3,
ISNULL( lc.nu_multa, 0) AS VALORMULTA,
0 AS JUROSDIA,
0 AS CAPMENSAL,
0 AS TAXASVENDOR,
0 AS JUROSVENDOR,
mpe.st_codcontacaixa AS CODCXA, -- conta caixa
'034' AS CODTDO, -- Código do tipo de documento
CASE WHEN lv.bl_entrada = 1 THEN
'01.01.0001'
ELSE
'01.01.0002'
END AS CODTB1FLX, -- Categoria
CASE WHEN (lc.id_cartaoconfig IS NULL AND lc.id_meiopagamento <> 4)  THEN
(SELECT DISTINCT TOP 1 mpi.st_codsistema FROM dbo.tb_meiopagamentointegracao mpi WHERE mpi.id_meiopagamento = lc.id_meiopagamento  AND mpi.id_sistema = 3)
WHEN (lc.id_cartaoconfig IS NULL AND lc.id_meiopagamento = 4 AND lv.bl_entrada = 1)  THEN '0002'
WHEN (lc.id_cartaoconfig IS NULL AND lc.id_meiopagamento = 4 AND lv.bl_entrada <> 1)  THEN '0003'
ELSE
(SELECT DISTINCT TOP 1 mpi.st_codsistema FROM dbo.tb_meiopagamentointegracao mpi WHERE mpi.id_meiopagamento = lc.id_meiopagamento  AND mpi.id_sistema = 3 AND lc.id_cartaoconfig = cc.id_cartaoconfig AND mpi.id_cartaobandeira = cc.id_cartaobandeira)
END AS CODTB2FLX, -- Forma de pagamento
CAST (ct.dt_ativacao AS DATE) AS DATAOP2,
CAST (vd.id_venda AS VARCHAR) + 'G2U' AS CAMPOALFAOP1,
CAST (lc.id_lancamento AS VARCHAR) + 'G2U' AS CAMPOALFAOP2,
'Lancamento gerado pelo Gestor2 as '+CONVERT(VARCHAR(8), GETDATE(),  114 ) + ' de ' + CONVERT(VARCHAR, GETDATE(),  103  ) AS HISTORICO,
ei.st_codchave AS CODCCUSTO,
NULL AS NUMEROCHEQUE,
CAST (lc.dt_vencimento AS DATE) AS DATAVENCIMENTO,
NULL AS DATAVENCIMENTO_CHEQUE,
NULL AS CODIGOBANCO,
NULL AS CODIGOAGENCIA,
NULL AS CONTACORRENTE,
NULL AS COMPENSADO,
vd.id_venda,
ct.id_contrato,
lv.id_lancamento,
lv.bl_entrada,
lv.nu_ordem,
vd.id_entidade,
ei.st_caminho,
ei.st_linkedserver
FROM tb_venda AS vd
LEFT JOIN tb_contrato AS ct ON vd.id_venda = ct.id_venda
JOIN tb_lancamentovenda AS lv ON lv.id_venda = vd.id_venda
JOIN tb_lancamento AS lc ON lc.id_lancamento = lv.id_lancamento
JOIN tb_entidade AS usr ON usr.id_entidade = lc.id_entidadelancamento
--JOIN tb_pessoa as ps ON ps.id_usuario = usr.id_usuario and ps.id_entidade = vd.id_entidade
JOIN tb_entidadeintegracao AS ei ON ei.id_entidade  = vd.id_entidade AND ei.id_sistema = 3
CROSS APPLY (SELECT TOP 1 id_meiopagamentointegracao ,
id_cartaobandeira ,
id_sistema ,
id_usuariocadastro ,
id_meiopagamento ,
id_entidade ,
st_codsistema ,
dt_cadastro, st_codcontacaixa FROM dbo.tb_meiopagamentointegracao WHERE id_meiopagamento = lc.id_meiopagamento AND id_entidade = vd.id_entidade) AS mpe 
LEFT JOIN dbo.tb_cartaoconfig cc ON cc.id_cartaoconfig = lc.id_cartaoconfig
LEFT JOIN dbo.tb_entidadeendereco AS pse ON pse.id_entidade = usr.id_entidade AND pse.bl_padrao = 1
LEFT JOIN tb_endereco AS ed ON ed.id_endereco = pse.id_endereco
LEFT JOIN tb_municipio AS mun ON ed.id_municipio = mun.id_municipio
LEFT JOIN dbo.tb_contatoentidade AS ce ON ce.id_entidade = usr.id_entidade
LEFT JOIN dbo.tb_contatostelefoneentidadecontato AS ctp ON ctp.id_contatoentidade = ce.id_contatoentidade AND ctp.bl_padrao = 1
LEFT JOIN tb_contatostelefone AS ctl ON ctl.id_telefone = ctp.id_telefone
--LEFT JOIN tb_contatosemailpessoaperfil as cepp ON cepp.id_usuario = usr.id_usuario and cepp.id_entidade = ps.id_entidade and cepp.bl_padrao = 1
--LEFT JOIN tb_contatosemail as ce ON ce.id_email = cepp.id_email
LEFT JOIN tb_pais AS pa ON pa.id_pais = ed.id_pais
LEFT JOIN tb_lancamentointegracao lanint ON lanint.id_lancamento = lc.id_lancamento
WHERE vd.id_evolucao = 10 AND lanint.st_codlancamento IS NULL 
and lc.nu_valor <> 0 AND lc.bl_ativo = 1
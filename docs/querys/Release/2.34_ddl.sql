-- GII-6756

CREATE TABLE tb_impressaocertificado (

    PRIMARY KEY (id_impressaocertificado),
    CONSTRAINT FK_TB_IMPRESSAOCERTIFICADO_TB_USUARIO
    	FOREIGN KEY (id_usuario)
    	REFERENCES tb_usuario(id_usuario),
    CONSTRAINT FK_TB_IMPRESSAOCERTIFICADO_TB_MATRICULA
    	FOREIGN KEY (id_matricula)
    	REFERENCES tb_matricula(id_matricula),

    id_impressaocertificado   INTEGER NOT NULL IDENTITY,
    st_textocertificado 	    TEXT,
    id_matricula 			        INTEGER,
    bl_ativo 				          BIT,
    dt_cadastro				        DATETIME2,
    dt_inativo				        DATETIME2,
    id_usuario				        INTEGER
)
GO

-- GII-7073

CREATE TABLE tb_turmasala
(
    id_salaturma INT PRIMARY KEY NOT NULL IDENTITY (1,1),
    id_saladeaula INT NOT NULL FOREIGN KEY REFERENCES tb_saladeaula(id_saladeaula) ,
    id_turma INT NOT NULL FOREIGN KEY REFERENCES tb_turma(id_turma),
    dt_cadastro DATETIME DEFAULT getdate()
)
GO

alter table tb_saladeaula add bl_vincularturma bit not null DEFAULT 0;
GO

-- GII-7679

alter table tb_matriculadisciplina add nu_cargahorariaaproveitamento int null
GO

-- GII-7684

ALTER TABLE tb_projetopedagogico ADD st_reconhecimento VARCHAR(300) NULL;
GO
-- AC 27136

alter VIEW [dbo].[vw_saladisciplinaturma]
AS
    SELECT DISTINCT
            aps.id_saladeaula ,
            sa.st_saladeaula ,
            sa.nu_maxalunos ,
            aps.id_projetopedagogico ,
            pp.id_trilha ,
            mt.id_matricula ,
            ds.id_disciplina ,
            md.id_matriculadisciplina ,
            mt.id_usuario ,
            al.id_alocacao ,
            se.id_entidade ,
            dc.id_tipodisciplina,
            ( SELECT    COUNT(alc.id_alocacao)
              FROM      tb_alocacao alc

              WHERE     alc.id_saladeaula = sa.id_saladeaula AND alc.bl_ativo = 1
            ) AS nu_alocados
    FROM    tb_matricula AS mt
            JOIN dbo.tb_turma AS tm ON tm.id_turma = mt.id_turma
            JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
            JOIN tb_areaprojetosala AS aps ON aps.id_projetopedagogico = mt.id_projetopedagogico
            JOIN tb_saladeaulaentidade AS se ON
                                                se.id_entidade = mt.id_entidadeatendimento AND aps.id_saladeaula = se.id_saladeaula
			JOIN tb_saladeaula AS sa ON sa.id_saladeaula = aps.id_saladeaula
                                        AND sa.dt_inicioinscricao <= tm.dt_inicio
                                        AND ( sa.dt_fiminscricao >= tm.dt_inicio
                                              OR sa.dt_fiminscricao IS NULL
                                            )
                                        AND sa.bl_ativa = 1
                                        AND sa.id_situacao = 8
                                        AND sa.id_categoriasala = 1
                                        AND se.id_saladeaula = sa.id_saladeaula
                                        AND sa.bl_vincularturma = 0
            JOIN tb_disciplinasaladeaula AS ds ON ds.id_saladeaula = sa.id_saladeaula
            JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
                                                 AND ds.id_disciplina = md.id_disciplina
                                                 AND md.id_situacao != 65 -- Ignorar alocacoes com APROVEITAMENTO DE DISCIPLINA
            LEFT JOIN tb_alocacao al ON al.id_matriculadisciplina = md.id_matriculadisciplina
                                        AND al.bl_ativo = 1
            LEFT JOIN tb_disciplina as dc ON dc.id_disciplina = ds.id_disciplina
    UNION ALL
    SELECT DISTINCT
            aps.id_saladeaula ,
            sa.st_saladeaula ,
            sa.nu_maxalunos ,
            aps.id_projetopedagogico ,
            pp.id_trilha ,
            mt.id_matricula ,
            ds.id_disciplina ,
            md.id_matriculadisciplina ,
            mt.id_usuario ,
            al.id_alocacao ,
            ve.id_entidade ,
            dc.id_tipodisciplina,
            ( SELECT    COUNT(alc.id_alocacao)
              FROM      tb_alocacao alc

              WHERE     alc.id_saladeaula = sa.id_saladeaula AND alc.bl_ativo = 1

            ) AS nu_alocados
    FROM    tb_matricula AS mt
            JOIN dbo.tb_turma AS tm ON tm.id_turma = mt.id_turma
            JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
            JOIN tb_areaprojetosala AS aps ON aps.id_projetopedagogico = mt.id_projetopedagogico
            JOIN dbo.tb_entidaderelacao AS ve ON ve.id_entidade = mt.id_entidadeatendimento
			JOIN tb_saladeaula AS sa ON sa.id_saladeaula = aps.id_saladeaula
                                        AND sa.dt_inicioinscricao <= tm.dt_inicio
                                        AND ( sa.dt_fiminscricao >= tm.dt_inicio
                                              OR sa.dt_fiminscricao IS NULL
                                            )
                                        AND sa.bl_ativa = 1
                                        AND sa.id_situacao = 8
                                        AND sa.id_categoriasala = 1
                                        AND sa.bl_todasentidades = 1
                                        AND ve.id_entidadepai = sa.id_entidade
                                        AND sa.bl_vincularturma = 0
            JOIN tb_disciplinasaladeaula AS ds ON ds.id_saladeaula = sa.id_saladeaula
            JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
                                                 AND ds.id_disciplina = md.id_disciplina
                                                 AND md.id_situacao != 65 -- Ignorar alocacoes com APROVEITAMENTO DE DISCIPLINA
            LEFT JOIN tb_alocacao al ON al.id_matriculadisciplina = md.id_matriculadisciplina
                                        AND al.bl_ativo = 1
            LEFT JOIN tb_disciplina as dc ON dc.id_disciplina = ds.id_disciplina
    UNION ALL
    SELECT DISTINCT
      aps.id_saladeaula ,
      sa.st_saladeaula ,
      sa.nu_maxalunos ,
      aps.id_projetopedagogico ,
      pp.id_trilha ,
      mt.id_matricula ,
      ds.id_disciplina ,
      md.id_matriculadisciplina ,
      mt.id_usuario ,
      al.id_alocacao ,
      se.id_entidade ,
      dc.id_tipodisciplina,
      ( SELECT    COUNT(alc.id_alocacao)
        FROM      tb_alocacao alc

        WHERE     alc.id_saladeaula = sa.id_saladeaula AND alc.bl_ativo = 1
      ) AS nu_alocados
    FROM    tb_matricula AS mt
      JOIN tb_turmasala ts ON ts.id_turma = mt.id_turma
      JOIN dbo.tb_turma AS tm ON tm.id_turma = mt.id_turma AND ts.id_turma = tm.id_turma
      JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
      JOIN tb_areaprojetosala AS aps ON aps.id_projetopedagogico = mt.id_projetopedagogico
      JOIN tb_saladeaulaentidade AS se ON
                                         se.id_entidade = mt.id_entidadeatendimento AND aps.id_saladeaula = se.id_saladeaula
      JOIN tb_saladeaula AS sa ON sa.id_saladeaula = aps.id_saladeaula
                                  AND sa.bl_ativa = 1
                                  AND sa.id_situacao = 8
                                  AND sa.id_categoriasala = 1
                                  AND se.id_saladeaula = sa.id_saladeaula
                                  AND sa.bl_vincularturma = 1
                                  AND sa.id_saladeaula = ts.id_saladeaula
      JOIN tb_disciplinasaladeaula AS ds ON ds.id_saladeaula = sa.id_saladeaula
      JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
                                           AND ds.id_disciplina = md.id_disciplina
                                           AND md.id_situacao != 65 -- Ignorar alocacoes com APROVEITAMENTO DE DISCIPLINA
      LEFT JOIN tb_alocacao al ON al.id_matriculadisciplina = md.id_matriculadisciplina
                                  AND al.bl_ativo = 1
      LEFT JOIN tb_disciplina as dc ON dc.id_disciplina = ds.id_disciplina
    UNION ALL
    SELECT DISTINCT
      aps.id_saladeaula ,
      sa.st_saladeaula ,
      sa.nu_maxalunos ,
      aps.id_projetopedagogico ,
      pp.id_trilha ,
      mt.id_matricula ,
      ds.id_disciplina ,
      md.id_matriculadisciplina ,
      mt.id_usuario ,
      al.id_alocacao ,
      ve.id_entidade ,
      dc.id_tipodisciplina,
      ( SELECT    COUNT(alc.id_alocacao)
        FROM      tb_alocacao alc

        WHERE     alc.id_saladeaula = sa.id_saladeaula AND alc.bl_ativo = 1

      ) AS nu_alocados
    FROM    tb_matricula AS mt
      JOIN tb_turmasala ts ON ts.id_turma = mt.id_turma
      JOIN dbo.tb_turma AS tm ON tm.id_turma = mt.id_turma AND ts.id_turma = tm.id_turma
      JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
      JOIN tb_areaprojetosala AS aps ON aps.id_projetopedagogico = mt.id_projetopedagogico
      JOIN dbo.tb_entidaderelacao AS ve ON ve.id_entidade = mt.id_entidadeatendimento
      JOIN tb_saladeaula AS sa ON sa.id_saladeaula = aps.id_saladeaula
                                  AND sa.bl_ativa = 1
                                  AND sa.id_situacao = 8
                                  AND sa.id_categoriasala = 1
                                  AND sa.bl_todasentidades = 1
                                  AND ve.id_entidadepai = sa.id_entidade
                                  AND sa.bl_vincularturma = 1
                                  AND sa.id_saladeaula = ts.id_saladeaula
      JOIN tb_disciplinasaladeaula AS ds ON ds.id_saladeaula = sa.id_saladeaula
      JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
                                           AND ds.id_disciplina = md.id_disciplina
                                           AND md.id_situacao != 65 -- Ignorar alocacoes com APROVEITAMENTO DE DISCIPLINA
      LEFT JOIN tb_alocacao al ON al.id_matriculadisciplina = md.id_matriculadisciplina
                                  AND al.bl_ativo = 1
      LEFT JOIN tb_disciplina as dc ON dc.id_disciplina = ds.id_disciplina
	  
GO

-- GII-7778

ALTER VIEW [rel].[vw_trancamentodisciplina] AS

SELECT
m.id_matricula,
u.st_nomecompleto,
u.st_cpf,
ce.st_email,
d.st_disciplina,
pp.st_projetopedagogico,
a.dt_trancamento,
sa.st_saladeaula,
ut.st_nomecompleto AS st_responsaveltrancamento,
-- Filtros
pp.id_projetopedagogico,
d.id_disciplina

FROM
  tb_alocacao AS a
  LEFT JOIN tb_matriculadisciplina AS md
    ON (a.id_matriculadisciplina = md.id_matriculadisciplina)
  LEFT JOIN tb_matricula AS m
    ON (md.id_matricula = m.id_matricula)
  LEFT JOIN tb_usuario AS u
    ON (m.id_usuario = u.id_usuario)
  LEFT JOIN tb_contatosemailpessoaperfil AS cepp
    ON (u.id_usuario = cepp.id_usuario
      AND cepp.bl_ativo     = 1
      AND cepp.bl_padrao    = 1
      AND cepp.id_entidade  = m.id_entidadematricula)
  JOIN tb_contatosemail AS ce
    ON (ce.id_email = cepp.id_email)
  LEFT JOIN tb_disciplina AS d
    ON (d.id_disciplina = md.id_disciplina)
  LEFT JOIN tb_usuario AS ut
    ON (a.id_usuariotrancamento = ut.id_usuario)
  LEFT JOIN tb_projetopedagogico AS pp
    ON (m.id_projetopedagogico  = pp.id_projetopedagogico)
  LEFT JOIN tb_saladeaula AS sa
    ON (sa.id_saladeaula = a.id_saladeaula)

WHERE a.bl_trancamento = 1;
GO


-- GII-7624

 ALTER VIEW [dbo].[vw_saladisciplinaalocacao]
AS
  SELECT
    aps.id_saladeaula,
    sa.st_saladeaula,
    sa.nu_maxalunos,
    sa.nu_diasaluno,
    aps.id_projetopedagogico,
    pp.id_trilha,
    mt.id_matricula,
    ds.id_disciplina,
    md.id_matriculadisciplina,
    mt.id_usuario,
    al.id_alocacao,
    se.id_entidade,
    (SELECT
       COUNT(alc.id_alocacao)
     FROM tb_alocacao alc,
       dbo.tb_matriculadisciplina mdc
     WHERE alc.id_saladeaula = sa.id_saladeaula
           AND mdc.id_matriculadisciplina = alc.id_matriculadisciplina
           AND mdc.id_evolucao = 13)
      AS nu_alocados,
    sa.dt_abertura,
    sa.dt_encerramento,
    sa.id_categoriasala,
    pp.st_projetopedagogico,
    al.bl_ativo,
    DATEADD(DAY, ISNULL(sa.nu_diasextensao,0),sa.dt_encerramento) as dt_encerramentoext
  FROM tb_matricula AS mt
    JOIN tb_projetopedagogico AS pp
      ON mt.id_projetopedagogico = pp.id_projetopedagogico
    JOIN tb_areaprojetosala AS aps
      ON aps.id_projetopedagogico = mt.id_projetopedagogico
    JOIN tb_saladeaula AS sa
      ON sa.id_saladeaula = aps.id_saladeaula
         AND sa.id_situacao = 8
         AND sa.bl_ativa = 1
         AND sa.bl_vincularturma = 0
    JOIN tb_disciplinasaladeaula AS ds
      ON ds.id_saladeaula = sa.id_saladeaula
    JOIN tb_matriculadisciplina AS md
      ON md.id_matricula = mt.id_matricula
         AND ds.id_disciplina = md.id_disciplina
    JOIN tb_saladeaulaentidade AS se
      ON se.id_saladeaula = sa.id_saladeaula
         AND se.id_entidade = mt.id_entidadeatendimento
    LEFT JOIN tb_alocacao al
      ON al.id_matriculadisciplina = md.id_matriculadisciplina
         AND al.id_saladeaula = sa.id_saladeaula
         AND al.bl_ativo = '1'
  WHERE (DATEADD(DAY, ISNULL(sa.nu_diasextensao,0),sa.dt_encerramento) >= CAST(GETDATE() AS DATE))
        OR (dt_encerramento IS NULL)
        OR (al.id_saladeaula = sa.id_saladeaula)

  UNION

  SELECT
    aps.id_saladeaula,
    sa.st_saladeaula,
    sa.nu_maxalunos,
    sa.nu_diasaluno,
    aps.id_projetopedagogico,
    pp.id_trilha,
    mt.id_matricula,
    ds.id_disciplina,
    md.id_matriculadisciplina,
    mt.id_usuario,
    al.id_alocacao,
    ve.id_entidade,
    (SELECT
       COUNT(alc.id_alocacao)
     FROM tb_alocacao alc,
       dbo.tb_matriculadisciplina mdc
     WHERE alc.id_saladeaula = sa.id_saladeaula
           AND mdc.id_matriculadisciplina = alc.id_matriculadisciplina
           AND mdc.id_evolucao = 13)
      AS nu_alocados,
    sa.dt_abertura,
    sa.dt_encerramento,
    sa.id_categoriasala,
    pp.st_projetopedagogico,
    al.bl_ativo,
    DATEADD(DAY, ISNULL(sa.nu_diasextensao,0),sa.dt_encerramento) as dt_encerramentoext
  FROM tb_matricula AS mt
    JOIN tb_projetopedagogico AS pp
      ON mt.id_projetopedagogico = pp.id_projetopedagogico
    JOIN tb_areaprojetosala AS aps
      ON aps.id_projetopedagogico = mt.id_projetopedagogico
    JOIN tb_saladeaula AS sa
      ON sa.id_saladeaula = aps.id_saladeaula
         AND sa.id_situacao = 8
         AND sa.bl_ativa = 1
         AND sa.bl_todasentidades = 1
         AND sa.bl_vincularturma = 0
    JOIN tb_disciplinasaladeaula AS ds
      ON ds.id_saladeaula = sa.id_saladeaula
    JOIN tb_matriculadisciplina AS md
      ON md.id_matricula = mt.id_matricula
         AND ds.id_disciplina = md.id_disciplina
    JOIN vw_entidaderecursivaid AS ve
      ON ve.id_entidade = mt.id_entidadeatendimento
         AND ve.id_entidadepai = sa.id_entidade
    LEFT JOIN tb_alocacao al
      ON al.id_matriculadisciplina = md.id_matriculadisciplina
         AND al.id_saladeaula = sa.id_saladeaula
         AND al.bl_ativo = '1'
  WHERE ((DATEADD(DAY, ISNULL(sa.nu_diasextensao,0),sa.dt_encerramento) >= CAST(GETDATE() AS DATE))
         OR (dt_encerramento IS NULL)
         OR (al.id_saladeaula = sa.id_saladeaula))

  UNION


  SELECT
    aps.id_saladeaula,
    sa.st_saladeaula,
    sa.nu_maxalunos,
    sa.nu_diasaluno,
    aps.id_projetopedagogico,
    pp.id_trilha,
    mt.id_matricula,
    ds.id_disciplina,
    md.id_matriculadisciplina,
    mt.id_usuario,
    al.id_alocacao,
    ve.id_entidade,
    (SELECT
       COUNT(alc.id_alocacao)
     FROM tb_alocacao alc,
       dbo.tb_matriculadisciplina mdc
     WHERE alc.id_saladeaula = sa.id_saladeaula
           AND mdc.id_matriculadisciplina = alc.id_matriculadisciplina
           AND mdc.id_evolucao = 13)
      AS nu_alocados,
    sa.dt_abertura,
    sa.dt_encerramento,
    sa.id_categoriasala,
    pp.st_projetopedagogico,
    al.bl_ativo,
    DATEADD(DAY, ISNULL(sa.nu_diasextensao,0),sa.dt_encerramento) as dt_encerramentoext
  FROM tb_matricula AS mt
    JOIN tb_projetopedagogico AS pp
      ON mt.id_projetopedagogico = pp.id_projetopedagogico
    JOIN tb_areaprojetosala AS aps
      ON aps.id_projetopedagogico = mt.id_projetopedagogico
    JOIN tb_saladeaula AS sa
      ON sa.id_saladeaula = aps.id_saladeaula
         AND sa.id_situacao = 8
         AND sa.bl_ativa = 1
         AND sa.bl_todasentidades = 1
         AND sa.bl_vincularturma = 0
    JOIN tb_disciplinasaladeaula AS ds
      ON ds.id_saladeaula = sa.id_saladeaula
    JOIN tb_matriculadisciplina AS md
      ON md.id_matricula = mt.id_matricula
         AND ds.id_disciplina = md.id_disciplina
    JOIN tb_entidade AS ve
      ON ve.id_entidade = sa.id_entidade
    LEFT JOIN tb_alocacao al
      ON al.id_matriculadisciplina = md.id_matriculadisciplina
         AND al.id_saladeaula = sa.id_saladeaula
         AND al.bl_ativo = '1'
  WHERE ((DATEADD(DAY, ISNULL(sa.nu_diasextensao,0),sa.dt_encerramento) >= CAST(GETDATE() AS DATE))
         OR (dt_encerramento IS NULL)
         OR (al.id_saladeaula = sa.id_saladeaula))

  UNION

  SELECT
    aps.id_saladeaula,
    sa.st_saladeaula,
    sa.nu_maxalunos,
    sa.nu_diasaluno,
    aps.id_projetopedagogico,
    pp.id_trilha,
    mt.id_matricula,
    ds.id_disciplina,
    md.id_matriculadisciplina,
    mt.id_usuario,
    al.id_alocacao,
    se.id_entidade,
    (SELECT
       COUNT(alc.id_alocacao)
     FROM tb_alocacao alc,
       dbo.tb_matriculadisciplina mdc
     WHERE alc.id_saladeaula = sa.id_saladeaula
           AND mdc.id_matriculadisciplina = alc.id_matriculadisciplina
           AND mdc.id_evolucao = 13)
      AS nu_alocados,
    sa.dt_abertura,
    sa.dt_encerramento,
    sa.id_categoriasala,
    pp.st_projetopedagogico,
    al.bl_ativo,
    DATEADD(DAY, ISNULL(sa.nu_diasextensao,0),sa.dt_encerramento) as dt_encerramentoext
  FROM tb_matricula AS mt
    JOIN tb_projetopedagogico AS pp
      ON mt.id_projetopedagogico = pp.id_projetopedagogico
    JOIN tb_areaprojetosala AS aps
      ON aps.id_projetopedagogico = mt.id_projetopedagogico
    JOIN tb_saladeaula AS sa
      ON sa.id_saladeaula = aps.id_saladeaula
         AND sa.id_situacao = 8
         AND sa.bl_ativa = 1
         AND sa.bl_vincularturma = 1
    JOIN tb_disciplinasaladeaula AS ds
      ON ds.id_saladeaula = sa.id_saladeaula
    JOIN tb_turmasala AS ts ON ts.id_turma = mt.id_turma AND ts.id_saladeaula = sa.id_saladeaula
    JOIN tb_matriculadisciplina AS md
      ON md.id_matricula = mt.id_matricula
         AND ds.id_disciplina = md.id_disciplina
    JOIN tb_saladeaulaentidade AS se
      ON se.id_saladeaula = sa.id_saladeaula
         AND se.id_entidade = mt.id_entidadeatendimento
    LEFT JOIN tb_alocacao al
      ON al.id_matriculadisciplina = md.id_matriculadisciplina
         AND al.id_saladeaula = sa.id_saladeaula
         AND al.bl_ativo = '1'
  WHERE (DATEADD(DAY, ISNULL(sa.nu_diasextensao,0),sa.dt_encerramento) >= CAST(GETDATE() AS DATE))
        OR (dt_encerramento IS NULL)
        OR (al.id_saladeaula = sa.id_saladeaula)

  UNION

  SELECT
    aps.id_saladeaula,
    sa.st_saladeaula,
    sa.nu_maxalunos,
    sa.nu_diasaluno,
    aps.id_projetopedagogico,
    pp.id_trilha,
    mt.id_matricula,
    ds.id_disciplina,
    md.id_matriculadisciplina,
    mt.id_usuario,
    al.id_alocacao,
    ve.id_entidade,
    (SELECT
       COUNT(alc.id_alocacao)
     FROM tb_alocacao alc,
       dbo.tb_matriculadisciplina mdc
     WHERE alc.id_saladeaula = sa.id_saladeaula
           AND mdc.id_matriculadisciplina = alc.id_matriculadisciplina
           AND mdc.id_evolucao = 13)
      AS nu_alocados,
    sa.dt_abertura,
    sa.dt_encerramento,
    sa.id_categoriasala,
    pp.st_projetopedagogico,
    al.bl_ativo,
    DATEADD(DAY, ISNULL(sa.nu_diasextensao,0),sa.dt_encerramento) as dt_encerramentoext
  FROM tb_matricula AS mt
    JOIN tb_projetopedagogico AS pp
      ON mt.id_projetopedagogico = pp.id_projetopedagogico
    JOIN tb_areaprojetosala AS aps
      ON aps.id_projetopedagogico = mt.id_projetopedagogico
    JOIN tb_saladeaula AS sa
      ON sa.id_saladeaula = aps.id_saladeaula
         AND sa.id_situacao = 8
         AND sa.bl_ativa = 1
         AND sa.bl_todasentidades = 1
         AND sa.bl_vincularturma = 1
    JOIN tb_disciplinasaladeaula AS ds
      ON ds.id_saladeaula = sa.id_saladeaula
    JOIN tb_turmasala AS ts ON ts.id_turma = mt.id_turma AND ts.id_saladeaula = sa.id_saladeaula
    JOIN tb_matriculadisciplina AS md
      ON md.id_matricula = mt.id_matricula
         AND ds.id_disciplina = md.id_disciplina
    JOIN vw_entidaderecursivaid AS ve
      ON ve.id_entidade = mt.id_entidadeatendimento
         AND ve.id_entidadepai = sa.id_entidade
    LEFT JOIN tb_alocacao al
      ON al.id_matriculadisciplina = md.id_matriculadisciplina
         AND al.id_saladeaula = sa.id_saladeaula
         AND al.bl_ativo = '1'
  WHERE ((DATEADD(DAY, ISNULL(sa.nu_diasextensao,0),sa.dt_encerramento) >= CAST(GETDATE() AS DATE))
         OR (dt_encerramento IS NULL)
         OR (al.id_saladeaula = sa.id_saladeaula))
GO

-- GII-7624

ALTER VIEW [dbo].[vw_saladisciplinaprojeto]
AS
    SELECT  aps.id_saladeaula ,
            sa.st_saladeaula ,
            sa.nu_maxalunos ,
            aps.id_projetopedagogico ,
			pp.st_projetopedagogico,
            pp.id_trilha ,
            mt.id_matricula ,
            ds.id_disciplina ,
            md.id_matriculadisciplina ,
            mt.id_usuario ,
            al.id_alocacao ,
            se.id_entidade ,
			alocados.nu as nu_alocados

    FROM    tb_matricula AS mt
            JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
            JOIN tb_areaprojetosala AS aps ON aps.id_projetopedagogico = mt.id_projetopedagogico
            JOIN tb_saladeaula AS sa ON sa.id_saladeaula = aps.id_saladeaula
                                        AND sa.dt_inicioinscricao <= CAST( GETDATE() AS DATE)
                                        AND ( sa.dt_fiminscricao >= CAST( GETDATE() AS DATE)
                                              OR dt_fiminscricao IS NULL
                                        )
                                        AND sa.bl_ativa = 1
                                        AND sa.id_categoriasala = 1
                                        AND sa.id_situacao = 8
                                        AND sa.bl_vincularturma = 0
            JOIN tb_disciplinasaladeaula AS ds ON ds.id_saladeaula = sa.id_saladeaula
            JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
                                                 AND ds.id_disciplina = md.id_disciplina
                                                 AND md.id_situacao != 65 -- Ignorar alocacoes com APROVEITAMENTO DE DISCIPLINA
            JOIN tb_saladeaulaentidade AS se ON se.id_saladeaula = sa.id_saladeaula
                                                AND se.id_entidade = mt.id_entidadeatendimento
            LEFT JOIN tb_alocacao al ON al.id_matriculadisciplina = md.id_matriculadisciplina AND al.bl_ativo = '1'
			OUTER apply      ( SELECT    COUNT(alc.id_alocacao) as nu
              FROM      tb_alocacao alc ,
                        dbo.tb_matriculadisciplina mdc
              WHERE     alc.id_saladeaula = sa.id_saladeaula
                        AND mdc.id_matriculadisciplina = alc.id_matriculadisciplina
                        AND mdc.id_evolucao = 13 and alc.bl_ativo = 1
            ) AS alocados
                                        where alocados.nu < sa.nu_maxalunos

    UNION ALL

    SELECT  aps.id_saladeaula ,
            sa.st_saladeaula ,
            sa.nu_maxalunos ,
            aps.id_projetopedagogico ,
			pp.st_projetopedagogico,
            pp.id_trilha ,
            mt.id_matricula ,
            ds.id_disciplina ,
            md.id_matriculadisciplina ,
            mt.id_usuario ,
            al.id_alocacao ,
            ve.id_entidade ,
            alocados.nu as nu_alocados
    FROM    tb_matricula AS mt
            JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
            JOIN tb_areaprojetosala AS aps ON aps.id_projetopedagogico = mt.id_projetopedagogico
            JOIN tb_saladeaula AS sa ON sa.id_saladeaula = aps.id_saladeaula
                                        AND sa.dt_inicioinscricao <= CAST( GETDATE() AS DATE)
                                        AND ( sa.dt_fiminscricao >= CAST( GETDATE() AS DATE)
                                              OR dt_fiminscricao IS NULL
                                        )
                                        AND sa.bl_ativa = 1
                                        AND sa.bl_todasentidades = 1
                                        AND sa.id_categoriasala = 1
                                        AND sa.id_situacao = 8
                                        AND sa.bl_vincularturma = 0
            JOIN tb_disciplinasaladeaula AS ds ON ds.id_saladeaula = sa.id_saladeaula
            JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
                                                 AND ds.id_disciplina = md.id_disciplina
                                                 AND md.id_situacao != 65 -- Ignorar alocacoes com APROVEITAMENTO DE DISCIPLINA
            JOIN dbo.tb_entidaderelacao AS ve ON ve.id_entidade = mt.id_entidadeatendimento
                                                 AND ve.id_entidadepai = sa.id_entidade
            LEFT JOIN tb_alocacao al ON al.id_matriculadisciplina = md.id_matriculadisciplina
                                        AND al.bl_ativo = 1
												OUTER apply      ( SELECT    COUNT(alc.id_alocacao) as nu
              FROM      tb_alocacao alc ,
                        dbo.tb_matriculadisciplina mdc
              WHERE     alc.id_saladeaula = sa.id_saladeaula
                        AND mdc.id_matriculadisciplina = alc.id_matriculadisciplina
                        AND mdc.id_evolucao = 13 and alc.bl_ativo = 1
            ) AS alocados
                                        where alocados.nu < sa.nu_maxalunos
    UNION ALL

    SELECT  aps.id_saladeaula ,
            sa.st_saladeaula ,
            sa.nu_maxalunos ,
            aps.id_projetopedagogico ,
			pp.st_projetopedagogico,
            pp.id_trilha ,
            mt.id_matricula ,
            ds.id_disciplina ,
            md.id_matriculadisciplina ,
            mt.id_usuario ,
            al.id_alocacao ,
            ve.id_entidade ,
            alocados.nu as nu_alocados
    FROM    tb_matricula AS mt
            JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
            JOIN tb_areaprojetosala AS aps ON aps.id_projetopedagogico = mt.id_projetopedagogico
            JOIN tb_saladeaula AS sa ON sa.id_saladeaula = aps.id_saladeaula
                                        AND sa.dt_inicioinscricao <= CAST( GETDATE() AS DATE)
                                        AND ( sa.dt_fiminscricao >= CAST( GETDATE() AS DATE)
                                              OR dt_fiminscricao IS NULL
                                        )
                                        AND sa.bl_ativa = 1
                                        AND sa.bl_todasentidades = 1
                                        AND sa.id_categoriasala = 1
                                        AND sa.bl_vincularturma = 0
            JOIN tb_disciplinasaladeaula AS ds ON ds.id_saladeaula = sa.id_saladeaula
            JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
                                                 AND ds.id_disciplina = md.id_disciplina
                                                 AND md.id_situacao != 65 -- Ignorar alocacoes com APROVEITAMENTO DE DISCIPLINA
            JOIN tb_entidade AS ve ON ve.id_entidade = sa.id_entidade
            LEFT JOIN tb_alocacao al ON al.id_matriculadisciplina = md.id_matriculadisciplina
                                        AND al.bl_ativo = 1
												OUTER apply      ( SELECT    COUNT(alc.id_alocacao) as nu
              FROM      tb_alocacao alc ,
                        dbo.tb_matriculadisciplina mdc
              WHERE     alc.id_saladeaula = sa.id_saladeaula
                        AND mdc.id_matriculadisciplina = alc.id_matriculadisciplina
                        AND mdc.id_evolucao = 13 and alc.bl_ativo = 1
            ) AS alocados
                                        where alocados.nu < sa.nu_maxalunos

UNION ALL

SELECT  aps.id_saladeaula ,
            sa.st_saladeaula ,
            sa.nu_maxalunos ,
            aps.id_projetopedagogico ,
			pp.st_projetopedagogico,
            pp.id_trilha ,
            mt.id_matricula ,
            ds.id_disciplina ,
            md.id_matriculadisciplina ,
            mt.id_usuario ,
            al.id_alocacao ,
            se.id_entidade ,
			alocados.nu as nu_alocados

    FROM    tb_matricula AS mt
            JOIN tb_turmasala ts ON ts.id_turma = mt.id_turma
            JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
            JOIN tb_areaprojetosala AS aps ON aps.id_projetopedagogico = mt.id_projetopedagogico
            JOIN tb_saladeaula AS sa ON sa.id_saladeaula = aps.id_saladeaula
                                        AND sa.dt_inicioinscricao <= CAST( GETDATE() AS DATE)
                                        AND ( sa.dt_fiminscricao >= CAST( GETDATE() AS DATE)
                                              OR dt_fiminscricao IS NULL
                                        )
                                        AND sa.bl_ativa = 1
                                        AND sa.id_categoriasala = 1
                                        AND sa.id_situacao = 8
                                        AND sa.bl_vincularturma = 1
                                        AND sa.id_saladeaula = ts.id_saladeaula
            JOIN tb_disciplinasaladeaula AS ds ON ds.id_saladeaula = sa.id_saladeaula
            JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
                                                 AND ds.id_disciplina = md.id_disciplina
                                                 AND md.id_situacao != 65 -- Ignorar alocacoes com APROVEITAMENTO DE DISCIPLINA
            JOIN tb_saladeaulaentidade AS se ON se.id_saladeaula = sa.id_saladeaula
                                                AND se.id_entidade = mt.id_entidadeatendimento
            LEFT JOIN tb_alocacao al ON al.id_matriculadisciplina = md.id_matriculadisciplina AND al.bl_ativo = '1'
			OUTER apply      ( SELECT    COUNT(alc.id_alocacao) as nu
              FROM      tb_alocacao alc ,
                        dbo.tb_matriculadisciplina mdc
              WHERE     alc.id_saladeaula = sa.id_saladeaula
                        AND mdc.id_matriculadisciplina = alc.id_matriculadisciplina
                        AND mdc.id_evolucao = 13 and alc.bl_ativo = 1
            ) AS alocados
                                        where alocados.nu < sa.nu_maxalunos

    UNION ALL

    SELECT  aps.id_saladeaula ,
            sa.st_saladeaula ,
            sa.nu_maxalunos ,
            aps.id_projetopedagogico ,
			pp.st_projetopedagogico,
            pp.id_trilha ,
            mt.id_matricula ,
            ds.id_disciplina ,
            md.id_matriculadisciplina ,
            mt.id_usuario ,
            al.id_alocacao ,
            ve.id_entidade ,
            alocados.nu as nu_alocados
    FROM    tb_matricula AS mt
            JOIN tb_turmasala ts ON ts.id_turma = mt.id_turma
            JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
            JOIN tb_areaprojetosala AS aps ON aps.id_projetopedagogico = mt.id_projetopedagogico
            JOIN tb_saladeaula AS sa ON sa.id_saladeaula = aps.id_saladeaula
                                        AND sa.dt_inicioinscricao <= CAST( GETDATE() AS DATE)
                                        AND ( sa.dt_fiminscricao >= CAST( GETDATE() AS DATE)
                                              OR dt_fiminscricao IS NULL
                                        )
                                        AND sa.bl_ativa = 1
                                        AND sa.bl_todasentidades = 1
                                        AND sa.id_categoriasala = 1
                                        AND sa.id_situacao = 8
                                        AND sa.bl_vincularturma = 1
                                        AND sa.id_saladeaula = ts.id_saladeaula
            JOIN tb_disciplinasaladeaula AS ds ON ds.id_saladeaula = sa.id_saladeaula
            JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
                                                 AND ds.id_disciplina = md.id_disciplina
                                                 AND md.id_situacao != 65 -- Ignorar alocacoes com APROVEITAMENTO DE DISCIPLINA
            JOIN dbo.tb_entidaderelacao AS ve ON ve.id_entidade = mt.id_entidadeatendimento
                                                 AND ve.id_entidadepai = sa.id_entidade
            LEFT JOIN tb_alocacao al ON al.id_matriculadisciplina = md.id_matriculadisciplina
                                        AND al.bl_ativo = 1
												OUTER apply      ( SELECT    COUNT(alc.id_alocacao) as nu
              FROM      tb_alocacao alc ,
                        dbo.tb_matriculadisciplina mdc
              WHERE     alc.id_saladeaula = sa.id_saladeaula
                        AND mdc.id_matriculadisciplina = alc.id_matriculadisciplina
                        AND mdc.id_evolucao = 13 and alc.bl_ativo = 1
            ) AS alocados
                                        where alocados.nu < sa.nu_maxalunos
    UNION ALL

    SELECT  aps.id_saladeaula ,
            sa.st_saladeaula ,
            sa.nu_maxalunos ,
            aps.id_projetopedagogico ,
			pp.st_projetopedagogico,
            pp.id_trilha ,
            mt.id_matricula ,
            ds.id_disciplina ,
            md.id_matriculadisciplina ,
            mt.id_usuario ,
            al.id_alocacao ,
            ve.id_entidade ,
            alocados.nu as nu_alocados
    FROM    tb_matricula AS mt
            JOIN tb_turmasala ts ON ts.id_turma = mt.id_turma
            JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
            JOIN tb_areaprojetosala AS aps ON aps.id_projetopedagogico = mt.id_projetopedagogico
            JOIN tb_saladeaula AS sa ON sa.id_saladeaula = aps.id_saladeaula
                                        AND sa.dt_inicioinscricao <= CAST( GETDATE() AS DATE)
                                        AND ( sa.dt_fiminscricao >= CAST( GETDATE() AS DATE)
                                              OR dt_fiminscricao IS NULL
                                        )
                                        AND sa.bl_ativa = 1
                                        AND sa.bl_todasentidades = 1
                                        AND sa.id_categoriasala = 1
                                        AND sa.bl_vincularturma = 1
                                        AND sa.id_saladeaula = ts.id_saladeaula
            JOIN tb_disciplinasaladeaula AS ds ON ds.id_saladeaula = sa.id_saladeaula
            JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
                                                 AND ds.id_disciplina = md.id_disciplina
                                                 AND md.id_situacao != 65 -- Ignorar alocacoes com APROVEITAMENTO DE DISCIPLINA
            JOIN tb_entidade AS ve ON ve.id_entidade = sa.id_entidade
            LEFT JOIN tb_alocacao al ON al.id_matriculadisciplina = md.id_matriculadisciplina
                                        AND al.bl_ativo = 1
												OUTER apply      ( SELECT    COUNT(alc.id_alocacao) as nu
              FROM      tb_alocacao alc ,
                        dbo.tb_matriculadisciplina mdc
              WHERE     alc.id_saladeaula = sa.id_saladeaula
                        AND mdc.id_matriculadisciplina = alc.id_matriculadisciplina
                        AND mdc.id_evolucao = 13 and alc.bl_ativo = 1
            ) AS alocados
                                        where alocados.nu < sa.nu_maxalunos
										
GO

-- GII-6595

alter VIEW [dbo].[vw_saladisciplinaturma]
AS
    SELECT DISTINCT
            aps.id_saladeaula ,
            sa.st_saladeaula ,
            sa.nu_maxalunos ,
            aps.id_projetopedagogico ,
            pp.id_trilha ,
            mt.id_matricula ,
            ds.id_disciplina ,
            md.id_matriculadisciplina ,
            mt.id_usuario ,
            al.id_alocacao ,
            se.id_entidade ,
            dc.id_tipodisciplina,
            ( SELECT    COUNT(alc.id_alocacao)
              FROM      tb_alocacao alc

              WHERE     alc.id_saladeaula = sa.id_saladeaula AND alc.bl_ativo = 1
            ) AS nu_alocados
    FROM    tb_matricula AS mt
            JOIN dbo.tb_turma AS tm ON tm.id_turma = mt.id_turma
            JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
            JOIN tb_areaprojetosala AS aps ON aps.id_projetopedagogico = mt.id_projetopedagogico
            JOIN tb_saladeaulaentidade AS se ON
                                                se.id_entidade = mt.id_entidadeatendimento AND aps.id_saladeaula = se.id_saladeaula
			JOIN tb_saladeaula AS sa ON sa.id_saladeaula = aps.id_saladeaula
                                        AND sa.dt_inicioinscricao <= tm.dt_inicio
                                        AND ( sa.dt_fiminscricao >= tm.dt_inicio
                                              OR sa.dt_fiminscricao IS NULL
                                            )
                                        AND sa.bl_ativa = 1
                                        AND sa.id_situacao = 8
                                        AND sa.id_categoriasala = 1
                                        AND se.id_saladeaula = sa.id_saladeaula
                                        AND sa.bl_vincularturma = 0
            JOIN tb_disciplinasaladeaula AS ds ON ds.id_saladeaula = sa.id_saladeaula
            JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
                                                 AND ds.id_disciplina = md.id_disciplina
                                                 AND md.id_situacao != 65 -- Ignorar alocacoes com APROVEITAMENTO DE DISCIPLINA
            LEFT JOIN tb_alocacao al ON al.id_matriculadisciplina = md.id_matriculadisciplina
                                        AND al.bl_ativo = 1
            LEFT JOIN tb_disciplina as dc ON dc.id_disciplina = ds.id_disciplina
    UNION ALL
    SELECT DISTINCT
            aps.id_saladeaula ,
            sa.st_saladeaula ,
            sa.nu_maxalunos ,
            aps.id_projetopedagogico ,
            pp.id_trilha ,
            mt.id_matricula ,
            ds.id_disciplina ,
            md.id_matriculadisciplina ,
            mt.id_usuario ,
            al.id_alocacao ,
            ve.id_entidade ,
            dc.id_tipodisciplina,
            ( SELECT    COUNT(alc.id_alocacao)
              FROM      tb_alocacao alc

              WHERE     alc.id_saladeaula = sa.id_saladeaula AND alc.bl_ativo = 1

            ) AS nu_alocados
    FROM    tb_matricula AS mt
            JOIN dbo.tb_turma AS tm ON tm.id_turma = mt.id_turma
            JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
            JOIN tb_areaprojetosala AS aps ON aps.id_projetopedagogico = mt.id_projetopedagogico
            JOIN dbo.tb_entidaderelacao AS ve ON ve.id_entidade = mt.id_entidadeatendimento
			JOIN tb_saladeaula AS sa ON sa.id_saladeaula = aps.id_saladeaula
                                        AND sa.dt_inicioinscricao <= tm.dt_inicio
                                        AND ( sa.dt_fiminscricao >= tm.dt_inicio
                                              OR sa.dt_fiminscricao IS NULL
                                            )
                                        AND sa.bl_ativa = 1
                                        AND sa.id_situacao = 8
                                        AND sa.id_categoriasala = 1
                                        AND sa.bl_todasentidades = 1
                                        AND ve.id_entidadepai = sa.id_entidade
                                        AND sa.bl_vincularturma = 0
            JOIN tb_disciplinasaladeaula AS ds ON ds.id_saladeaula = sa.id_saladeaula
            JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
                                                 AND ds.id_disciplina = md.id_disciplina
                                                 AND md.id_situacao != 65 -- Ignorar alocacoes com APROVEITAMENTO DE DISCIPLINA
            LEFT JOIN tb_alocacao al ON al.id_matriculadisciplina = md.id_matriculadisciplina
                                        AND al.bl_ativo = 1
            LEFT JOIN tb_disciplina as dc ON dc.id_disciplina = ds.id_disciplina
    UNION ALL
    SELECT DISTINCT
      aps.id_saladeaula ,
      sa.st_saladeaula ,
      sa.nu_maxalunos ,
      aps.id_projetopedagogico ,
      pp.id_trilha ,
      mt.id_matricula ,
      ds.id_disciplina ,
      md.id_matriculadisciplina ,
      mt.id_usuario ,
      al.id_alocacao ,
      se.id_entidade ,
      dc.id_tipodisciplina,
      ( SELECT    COUNT(alc.id_alocacao)
        FROM      tb_alocacao alc

        WHERE     alc.id_saladeaula = sa.id_saladeaula AND alc.bl_ativo = 1
      ) AS nu_alocados
    FROM    tb_matricula AS mt
      JOIN tb_turmasala ts ON ts.id_turma = mt.id_turma
      JOIN dbo.tb_turma AS tm ON tm.id_turma = mt.id_turma AND ts.id_turma = tm.id_turma
      JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
      JOIN tb_areaprojetosala AS aps ON aps.id_projetopedagogico = mt.id_projetopedagogico
      JOIN tb_saladeaulaentidade AS se ON
                                         se.id_entidade = mt.id_entidadeatendimento AND aps.id_saladeaula = se.id_saladeaula
      JOIN tb_saladeaula AS sa ON sa.id_saladeaula = aps.id_saladeaula
                                  AND sa.bl_ativa = 1
                                  AND sa.id_situacao = 8
                                  AND sa.id_categoriasala = 1
                                  AND se.id_saladeaula = sa.id_saladeaula
                                  AND sa.bl_vincularturma = 1
                                  AND sa.id_saladeaula = ts.id_saladeaula
      JOIN tb_disciplinasaladeaula AS ds ON ds.id_saladeaula = sa.id_saladeaula
      JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
                                           AND ds.id_disciplina = md.id_disciplina
                                           AND md.id_situacao != 65 -- Ignorar alocacoes com APROVEITAMENTO DE DISCIPLINA
      LEFT JOIN tb_alocacao al ON al.id_matriculadisciplina = md.id_matriculadisciplina
                                  AND al.bl_ativo = 1
      LEFT JOIN tb_disciplina as dc ON dc.id_disciplina = ds.id_disciplina
    UNION ALL
    SELECT DISTINCT
      aps.id_saladeaula ,
      sa.st_saladeaula ,
      sa.nu_maxalunos ,
      aps.id_projetopedagogico ,
      pp.id_trilha ,
      mt.id_matricula ,
      ds.id_disciplina ,
      md.id_matriculadisciplina ,
      mt.id_usuario ,
      al.id_alocacao ,
      ve.id_entidade ,
      dc.id_tipodisciplina,
      ( SELECT    COUNT(alc.id_alocacao)
        FROM      tb_alocacao alc

        WHERE     alc.id_saladeaula = sa.id_saladeaula AND alc.bl_ativo = 1

      ) AS nu_alocados
    FROM    tb_matricula AS mt
      JOIN tb_turmasala ts ON ts.id_turma = mt.id_turma
      JOIN dbo.tb_turma AS tm ON tm.id_turma = mt.id_turma AND ts.id_turma = tm.id_turma
      JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
      JOIN tb_areaprojetosala AS aps ON aps.id_projetopedagogico = mt.id_projetopedagogico
      JOIN dbo.tb_entidaderelacao AS ve ON ve.id_entidade = mt.id_entidadeatendimento
      JOIN tb_saladeaula AS sa ON sa.id_saladeaula = aps.id_saladeaula
                                  AND sa.bl_ativa = 1
                                  AND sa.id_situacao = 8
                                  AND sa.id_categoriasala = 1
                                  AND sa.bl_todasentidades = 1
                                  AND ve.id_entidadepai = sa.id_entidade
                                  AND sa.bl_vincularturma = 1
                                  AND sa.id_saladeaula = ts.id_saladeaula
      JOIN tb_disciplinasaladeaula AS ds ON ds.id_saladeaula = sa.id_saladeaula
      JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
                                           AND ds.id_disciplina = md.id_disciplina
                                           AND md.id_situacao != 65 -- Ignorar alocacoes com APROVEITAMENTO DE DISCIPLINA
      LEFT JOIN tb_alocacao al ON al.id_matriculadisciplina = md.id_matriculadisciplina
                                  AND al.bl_ativo = 1
      LEFT JOIN tb_disciplina as dc ON dc.id_disciplina = ds.id_disciplina
GO	  
-- GII-7766

ALTER VIEW [rel].[vw_matricula]
AS
    SELECT DISTINCT
            mt.id_matricula ,
            mt.dt_inicio ,
            ct.id_contrato ,
            us.st_nomecompleto ,
            us.st_cpf ,
            p.st_email ,
            CAST(p.nu_ddd AS VARCHAR) + '-' + CAST(p.nu_telefone AS VARCHAR) AS st_telefone ,
            ac.id_areaconhecimento ,
            ac.st_areaconhecimento ,
            pp.id_projetopedagogico ,
            pp.st_projetopedagogico ,
            st.id_situacao ,
            st.st_situacao ,
            ev.id_evolucao ,
            ev.st_evolucao ,
            mt.id_situacaoagendamento,
            mt.dt_aptoagendamento,
            usr.st_nomecompleto AS st_representante ,
            usa.st_nomecompleto AS st_atendente ,
            vp.nu_valorliquido AS nu_valor ,
            mt.id_entidadeatendimento ,
            p.sg_uf ,
            tm.st_turma, tm.id_turma,
            tm.dt_inicio AS dt_inicioturma,
            tm.dt_fim AS dt_fimturma,
            CAST(mt.dt_cadastro AS DATE) AS dt_cadastro,
            CAST(mt.dt_concluinte AS DATE) AS dt_concluinte,
            CAST (cmt.dt_cadastro AS DATE) AS dt_certificacao,
                        vp.nu_valorliquido AS nu_valorliquido ,
            vp.nu_valorbruto AS nu_valorproduto ,
            campanha.nu_valordesconto AS nu_valordesconto,
            st_statusdocumentacao = CASE
              WHEN mt.bl_documentacao = 1 THEN 'OK'
              WHEN mt.bl_documentacao = 0 THEN 'Pendente'
              ELSE 'Não Analisado'
			      END
    FROM    dbo.tb_matricula mt
            JOIN dbo.tb_usuario AS us ON us.id_usuario = mt.id_usuario
            INNER JOIN vw_pessoa p ON p.id_entidade = mt.id_entidadeatendimento
                                      AND p.id_usuario = mt.id_usuario
            JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
            JOIN dbo.tb_areaprojetopedagogico AS ap ON ap.id_projetopedagogico = pp.id_projetopedagogico
            OUTER APPLY ( SELECT TOP 1
                                    id_areaconhecimento ,
                                    st_areaconhecimento
                          FROM      dbo.tb_areaconhecimento
                          WHERE     id_areaconhecimento = ap.id_areaconhecimento
                                    AND id_tipoareaconhecimento = 1
                        ) AS ac
           LEFT JOIN dbo.tb_contratomatricula AS cm ON cm.id_matricula = mt.id_matricula
                                                   AND cm.bl_ativo = 1
            LEFT JOIN dbo.tb_contrato AS ct ON ct.id_contrato = cm.id_contrato
                                          AND ct.bl_ativo = 1
            LEFT JOIN dbo.tb_venda AS vd ON ct.id_venda = vd.id_venda
            LEFT JOIN dbo.tb_vendaproduto AS vp ON vp.id_venda = vd.id_venda
            LEFT JOIN dbo.tb_atendentevenda AS av ON av.id_venda = vd.id_venda
            LEFT JOIN dbo.tb_usuario AS usa ON usa.id_usuario = av.id_usuario
            LEFT JOIN dbo.tb_vendaenvolvido AS ve ON ve.id_venda = vd.id_venda
                                                     AND ve.id_funcao = 1
            LEFT JOIN dbo.tb_usuario AS usr ON usr.id_usuario = ve.id_usuario
            JOIN dbo.tb_situacao AS st ON st.id_situacao = mt.id_situacao
            JOIN dbo.tb_evolucao AS ev ON ev.id_evolucao = mt.id_evolucao
            LEFT JOIN dbo.tb_turma AS tm ON tm.id_turma = mt.id_turma
            LEFT JOIN tb_matriculacertificacao AS cmt ON cmt.id_matricula=mt.id_matricula
            LEFT JOIN dbo.tb_campanhacomercial as campanha ON campanha.id_campanhacomercial = vp.id_campanhacomercial
    WHERE   mt.bl_ativo = 1
           AND mt.bl_institucional = 0
GO
										
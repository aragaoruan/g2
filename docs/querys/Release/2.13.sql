alter view [rel].[vw_cargaconcluinte] as

select
 DISTINCT mt.id_usuario
, upper(mt.st_nomecompleto) as st_nomecompleto
, pessoa.dt_nascimento
, mt.id_turma AS id_turma
, mt.st_cpf
, sa.id_categoriasala
, upper(pessoa.st_endereco) as st_endereco
, upper(pessoa.st_bairro) as st_bairro
, pessoa.st_cep
, mp.st_nomemunicipio as st_cidade
, pessoa.sg_uf
, CAST(pessoa.nu_ddd AS VARCHAR) + '-' + CAST( pessoa.nu_telefone AS varchar) as st_telefone
, mt.st_email
, mt.dt_cadastro
, st_sexo = case
        WHEN pessoa.st_sexo like 'F' THEN 'FEMININO'
                 ELSE 'MASCULINO'
        END
, 'C---------' AS separador1
,  mt.id_projetopedagogico
,  UPPER(mt.st_projetopedagogico) as st_projetopedagogico
,  CAST(ppj.nu_cargahoraria as INT)  as nu_cargahoraria
,  CAST(ppj.nu_cargahoraria as VARCHAR(20))  as st_cargahoraria
,  mt.dt_inicioturma AS dt_inicioturma
,  mt.dt_terminoturma AS dt_terminoturma
,  'D---------' AS separador2
,  td.id_disciplina
,  UPPER(td.st_tituloexibicao) as st_disciplina
,  replace((select nu_aprovafinal from tb_matriculadisciplina
              where id_disciplina = td.id_disciplina AND id_matricula=mt.id_matricula)/10,',','.') as nu_notatotal
, replace(CAST((select nu_aprovafinal from tb_matriculadisciplina
              where id_disciplina = td.id_disciplina AND id_matricula=mt.id_matricula)/10 AS DECIMAL(18,1))  ,',','.'  ) as st_notatotal
,  'P---------' AS separador3
, tb_usuario.id_usuario AS id_professor
,  UPPER(tb_usuario.st_nomecompleto) AS st_coordenador
,  UPPER(tbti.st_titulacao) AS st_nivel
,  UPPER(tb_usuario.st_nomecompleto) as st_nomeprofessor
,  mt.id_matricula
,  sa.id_saladeaula AS id_disciplinaturma
,  UPPER(mt.st_evolucao) as st_evolucao
,  tbti.st_titulacao AS st_titulocoordenador
,  td.nu_cargahoraria  AS  st_cargadisciplina
,  mt.dt_concluinte
,  mt.id_entidadematricula
,  (select UPPER(st_tituloavaliacao) from vw_avaliacaoaluno where id_saladeaula=al.id_saladeaula and id_matricula=mt.id_matricula and bl_ativo=1 and id_tipoavaliacao=6) as st_tituloavaliacao
, CASE WHEN mt.id_entidadematricula not in (292,5) THEN 'WP�S'
                 ELSE mt.st_entidadematricula
END st_entidadematricula
, (pessoa.st_rg  + ' - ' + pessoa.st_orgaoexpeditor) AS st_rg,
mt.st_codcertificacao,
('W' + cast(mt.id_turma as varchar)) as st_periodorealizacao,
convert(varchar, mtc.dt_cadastro, 105) as st_dtemissao
FROM dbo.tb_alocacao al
JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula
JOIN dbo.tb_matriculadisciplina AS md ON md.id_matriculadisciplina = al.id_matriculadisciplina
JOIN dbo.tb_disciplina AS td ON td.id_disciplina = md.id_disciplina
JOIN dbo.vw_matricula AS mt ON mt.id_matricula = md.id_matricula AND mt.bl_ativo = 1
JOIN dbo.vw_pessoa as pessoa on pessoa.id_usuario = mt.id_usuario AND pessoa.id_entidade = mt.id_entidadematricula
LEFT JOIN tb_municipio as mp on mp.id_municipio = pessoa.id_municipio
JOIN tb_projetopedagogico as ppj ON ppj.id_projetopedagogico = mt.id_projetopedagogico
LEFT JOIN vw_saladeaula as vw_sala on vw_sala.id_saladeaula=al.id_saladeaula and vw_sala.id_projetopedagogico=ppj.id_projetopedagogico and vw_sala.bl_ativa=1 and vw_sala.bl_titular=1
/*LEFT JOIN tb_usuarioperfilentidadereferencia as ucoor on ucoor.id_projetopedagogico=mt.id_projetopedagogico and ucoor.bl_titular=1
LEFT JOIN dbo.tb_perfil ON dbo.tb_perfil.id_perfil = ucoor.id_perfil
                                   AND dbo.tb_perfil.st_nomeperfil LIKE '%Coordenador de Projeto Pedag�gico%'
LEFT JOIN dbo.tb_usuario ON dbo.tb_usuario.id_usuario = ucoor.id_usuario
LEFT JOIN dbo.tb_titulacao as tbti ON tbti.id_titulacao = tb_usuario.id_titulacao
*/
LEFT JOIN tb_perfil AS pf ON pf.id_entidade = mt.id_entidadematricula
                                         AND pf.id_perfilpedagogico = 9 and pf.bl_ativo = 1
LEFT JOIN tb_usuarioperfilentidadereferencia AS ucoor ON ucoor.id_projetopedagogico = mt.id_projetopedagogico
                                                              AND ucoor.id_disciplina = td.id_disciplina
                                                                                              AND ucoor.id_perfil = pf.id_perfil
                                                              AND ( ( mt.dt_concluinte BETWEEN ucoor.dt_inicio
                                                              AND ucoor.dt_fim ) OR ( mt.dt_concluinte > ucoor.dt_inicio
                                                              AND ucoor.dt_fim IS NULL
                                                              )
                                                              )
                                                              AND ucoor.bl_titular = 1
                                                              AND ucoor.bl_ativo = 1
            LEFT JOIN tb_usuario  ON tb_usuario.id_usuario = ucoor.id_usuario
            LEFT JOIN tb_titulacao tbti ON ucoor.id_titulacao = tbti.id_titulacao
            LEFT JOIN tb_matriculacertificacao mtc on mtc.id_matricula = mt.id_matricula
WHERE al.bl_ativo = 1
and mt.id_evolucao = 15
and sa.id_categoriasala = 1
and td.st_disciplina not like '%AMBIENTA��O%'

GO

ALTER VIEW [rel].[vw_matricula]
AS
    SELECT DISTINCT
            mt.id_matricula ,
            mt.dt_inicio ,
            ct.id_contrato ,
            us.st_nomecompleto ,
            us.st_cpf ,
            p.st_email ,
            CAST(p.nu_ddd AS VARCHAR) + '-' + CAST(p.nu_telefone AS VARCHAR) AS st_telefone ,
            ac.id_areaconhecimento ,
            ac.st_areaconhecimento ,
            pp.id_projetopedagogico ,
            pp.st_projetopedagogico ,
            st.id_situacao ,
            st.st_situacao ,
            ev.id_evolucao ,
            ev.st_evolucao ,
            usr.st_nomecompleto AS st_representante ,
            usa.st_nomecompleto AS st_atendente ,
            vp.nu_valorliquido AS nu_valor ,
            mt.id_entidadeatendimento ,
            p.sg_uf ,
            tm.st_turma, tm.id_turma,
            tm.dt_inicio AS dt_inicioturma,
            tm.dt_fim AS dt_fimturma,
            CAST(mt.dt_cadastro AS DATE) AS dt_cadastro,
            CAST(mt.dt_concluinte AS DATE) AS dt_concluinte,
            CAST (cmt.dt_cadastro AS DATE) AS dt_certificacao,
                        vp.nu_valorliquido AS nu_valorliquido ,
            vp.nu_valorbruto AS nu_valorproduto ,
            campanha.nu_valordesconto AS nu_valordesconto
    FROM    dbo.tb_matricula mt
            JOIN dbo.tb_usuario AS us ON us.id_usuario = mt.id_usuario
            INNER JOIN vw_pessoa p ON p.id_entidade = mt.id_entidadeatendimento
                                      AND p.id_usuario = mt.id_usuario
            JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
            JOIN dbo.tb_areaprojetopedagogico AS ap ON ap.id_projetopedagogico = pp.id_projetopedagogico
            OUTER APPLY ( SELECT TOP 1
                                    id_areaconhecimento ,
                                    st_areaconhecimento
                          FROM      dbo.tb_areaconhecimento
                          WHERE     id_areaconhecimento = ap.id_areaconhecimento
                                    AND id_tipoareaconhecimento = 1
                        ) AS ac
            JOIN dbo.tb_contratomatricula AS cm ON cm.id_matricula = mt.id_matricula
                                                   AND cm.bl_ativo = 1
            JOIN dbo.tb_contrato AS ct ON ct.id_contrato = cm.id_contrato
                                          AND ct.bl_ativo = 1
            JOIN dbo.tb_venda AS vd ON ct.id_venda = vd.id_venda
            JOIN dbo.tb_vendaproduto AS vp ON vp.id_venda = vd.id_venda
            LEFT JOIN dbo.tb_atendentevenda AS av ON av.id_venda = vd.id_venda
            LEFT JOIN dbo.tb_usuario AS usa ON usa.id_usuario = av.id_usuario
            LEFT JOIN dbo.tb_vendaenvolvido AS ve ON ve.id_venda = vd.id_venda
                                                     AND ve.id_funcao = 1
            LEFT JOIN dbo.tb_usuario AS usr ON usr.id_usuario = ve.id_usuario
            JOIN dbo.tb_situacao AS st ON st.id_situacao = mt.id_situacao
            JOIN dbo.tb_evolucao AS ev ON ev.id_evolucao = mt.id_evolucao
            LEFT JOIN dbo.tb_turma AS tm ON tm.id_turma = mt.id_turma
            LEFT JOIN tb_matriculacertificacao AS cmt ON cmt.id_matricula=mt.id_matricula
            LEFT JOIN dbo.tb_campanhacomercial as campanha ON campanha.id_campanhacomercial = vp.id_campanhacomercial
    WHERE   mt.bl_ativo = 1
           -- AND mt.bl_institucional = 0
		   go
ALTER VIEW [dbo].[vw_alunosagendamento] AS
SELECT DISTINCT
        u.id_usuario ,
        u.st_nomecompleto ,
        u.st_cpf ,
        u.st_email ,
        m.id_situacaoagendamento ,
        m.id_matricula ,
        ap.id_aplicadorprova ,
        ap.st_aplicadorprova ,
        avp.bl_ativo ,
        avp.dt_cadastro ,
        avp.bl_unica ,
        avp.dt_antecedenciaminima ,
        avp.dt_alteracaolimite ,
        avp.dt_aplicacao ,
        avp.id_usuariocadastro ,
        avp.id_horarioaula ,
        avp.nu_maxaplicacao ,
        avp.id_endereco ,
        avp.id_avaliacaoaplicacao,
                m.st_projetopedagogico,
                m.id_projetopedagogico,
                u.id_entidade,
                vw_end.sg_uf,
               
 ta.bl_ativo AS bl_ativoavagendamento
                ,ta.id_avaliacaoagendamento
FROM    vw_pessoa AS u
        INNER JOIN vw_matricula AS m ON m.id_usuario = u.id_usuario
                                        AND m.bl_ativo = 1 AND m.id_entidadeatendimento = u.id_entidade AND m.id_situacaoagendamento = 120
                                                                                AND m.id_evolucao=6
        LEFT JOIN dbo.tb_avaliacaoagendamento AS ta ON ta.id_matricula = m.id_matricula AND ta.id_situacao NOT IN (70,69) AND ta.nu_presenca IS NULL
        LEFT JOIN tb_avaliacaoaplicacao AS avp ON avp.id_avaliacaoaplicacao = ta.id_avaliacaoaplicacao
        LEFT JOIN tb_aplicadorprova AS ap ON ap.id_aplicadorprova = avp.id_aplicadorprova
                LEFT JOIN vw_aplicadorendereco AS vw_end ON vw_end.id_aplicadorprova = ap.id_aplicadorprova
            JOIN dbo.vw_avaliacaoaluno AS gt ON gt.id_matricula= m.id_matricula AND gt.id_evolucaodisciplina IN (13,19)
                AND gt.nu_cargahoraria!=0 AND gt.id_tipoavaliacao= 4
/****************************************
***** INSERT na tb_texto categoria ******
****************************************/

SET IDENTITY_INSERT dbo.tb_textocategoria on
INSERT INTO tb_textocategoria
(id_textocategoria, st_textocategoria, st_descricao, st_metodo)
VALUES
 (15, 'Cabe�alho', 'Cabe�alho para textos de sistemas do tipo declara��o', NULL)
,(16, 'Rodap�', 'Rodap� para textos de sistemas do tipo declara��o', NULL)
SET IDENTITY_INSERT dbo.tb_textocategoria off

go
/** Add relacionamento **/


INSERT INTO tb_textoexibicaocategoria
(id_textoexibicao, id_textocategoria)
VALUES
(1, 15),
(1, 16)


go
/** ADD campo id_cabecalho e id_rodape ao texto sistema **/

ALTER TABLE tb_textosistema
ADD id_rodape INT NULL ,
 id_cabecalho INT NULL

go
ALTER TABLE [dbo].[tb_configuracaoentidade] ADD [bl_msgagendamentoportal] BIT NOT NULL DEFAULT (0);

go
ALTER TABLE tb_assuntoco ADD id_assuntocategoria INT NULL;
go
SET IDENTITY_INSERT tb_funcionalidade ON;
INSERT INTO tb_funcionalidade (
id_funcionalidade,
st_funcionalidade,
id_funcionalidadepai,
bl_ativo,
id_situacao,
id_tipofuncionalidade,
st_classeflexbotao,
bl_pesquisa,
bl_lista,
st_urlcaminho,
bl_visivel,
bl_relatorio,
bl_delete)
VALUES(751, 'Alterar Venda', 194, 1, 123, 3,'AlterarVenda' ,1, 0, '/alterar-venda', 1, 0, 0);
SET IDENTITY_INSERT tb_funcionalidade OFF;
GO
CREATE TABLE tb_minhapasta
(
    id_minhapasta INT PRIMARY KEY IDENTITY,
    st_contrato VARCHAR(255) NULL,
    id_matricula INT NOT NULL,
    id_venda INT NOT NULL,
    id_upload INT NULL,
    id_usuario INT NOT NULL,
    id_usuariocadastro INT NOT NULL,
    id_usuarioatualizacao INT NULL,
    id_situacao INT NOT NULL,
    id_evolucao INT NOT NULL,
    bl_ativo INT NOT NULL DEFAULT(1),
    dt_cadastro DATETIME DEFAULT GETDATE(),
    dt_atualizacao DATETIME NULL
    );
go
SET IDENTITY_INSERT dbo.tb_situacao ON
	INSERT INTO tb_situacao (id_situacao, st_situacao, st_tabela, st_campo, st_descricaosituacao) VALUES
	(180, 'Ativo', 'tb_minhapasta', 'id_situacao', 'Contrato ativo para visualização do aluno'),
	(181, 'inativo', 'tb_minhapasta', 'id_situacao', 'Contrato inativo para visualização do aluno')
SET IDENTITY_INSERT dbo.tb_situacao OFF
go
SET IDENTITY_INSERT dbo.tb_evolucao ON
	INSERT tb_evolucao (id_evolucao, st_evolucao, st_tabela, st_campo) VALUES
  (66, 'Adicionou contrato', 'tb_minhapasta', 'id_evolucao'),
  (67, 'Inativou contrato', 'tb_minhapasta', 'id_evolucao'),
  (68, 'Removeu contrato', 'tb_minhapasta', 'id_evolucao')
SET IDENTITY_INSERT dbo.tb_evolucao OFF
go
SET QUOTED_IDENTIFIER OFF;
UPDATE tb_propriedadecamporelatorio SET
st_valor = "SELECT id_evolucao AS id, st_evolucao AS value FROM tb_evolucao WHERE st_tabela = 'tb_matricula' AND st_campo = 'id_evolucao' AND id_evolucao IN (6, 21, 40)"
WHERE id_tipopropriedadecamporel = 5 AND id_camporelatorio IN (
	SELECT id_camporelatorio FROM tb_camporelatorio WHERE id_relatorio = 70 AND st_camporelatorio = 'id_evolucao'
);
SET QUOTED_IDENTIFIER ON;
go
-- AC-27326

-- Alterar a vw de origem dos dados para rel.vw_alunosaptosagendamento
UPDATE tb_relatorio SET st_origem = 'vw_alunosaptosagendamento' WHERE id_relatorio = 70;


-- Remover campo data de conclusao
DELETE FROM tb_camporelatorio WHERE id_relatorio = 70 AND st_camporelatorio = 'dt_concluinte';


-- Remover filtro de situacao
DELETE FROM tb_propriedadecamporelatorio WHERE id_camporelatorio IN (
	SELECT id_camporelatorio FROM tb_camporelatorio WHERE id_relatorio = 70 AND st_camporelatorio = 'id_situacaoagendamento'
);
DELETE FROM tb_camporelatorio WHERE id_relatorio = 70 AND st_camporelatorio = 'id_situacaoagendamento';


-- Alterar busca do combo (FILTRO EVOLUCAO)
SET QUOTED_IDENTIFIER OFF;
UPDATE tb_propriedadecamporelatorio SET
st_valor = "SELECT id_evolucao AS id, st_evolucao AS value FROM tb_evolucao WHERE st_tabela = 'tb_matricula' AND st_campo = 'id_evolucao' AND id_evolucao IN (6, 21, 40)"
WHERE id_tipopropriedadecamporel = 5 AND id_camporelatorio IN (
	SELECT id_camporelatorio FROM tb_camporelatorio WHERE id_relatorio = 70 AND st_camporelatorio = 'id_evolucao'
);
SET QUOTED_IDENTIFIER ON;


-- Inserir a informacao do Tipo de Avaliacao no resultado
INSERT INTO tb_camporelatorio (id_relatorio, st_camporelatorio, st_titulocampo, bl_filtro, bl_exibido, nu_ordem, bl_obrigatorio) VALUES (70, 'st_avaliacao', 'Tipo de Prova', 0, 1, 15, 0);

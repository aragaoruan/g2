alter VIEW [dbo].[vw_enviotcc] as

select 

vwi.id_matricula,
vwi.st_nomecompleto,
vwi.st_cpf,
vwi.st_nomeexibicao,
vwi.id_usuario,
vwi.id_entidade,
vwi.id_entidadesala,
vwe.dt_encerramentoprofessor,

vwi.id_projetopedagogico,
vwi.st_tituloexibicaoprojeto,
vwi.id_modulo,
vwi.st_tituloexibicaomodulo,
vwi.id_disciplina,
vwi.st_disciplina,
s.id_saladeaula,
s.st_saladeaula,
vwi.id_professor,
vwi.st_professor,
vwi.id_situacaotcc,
vwa.id_avaliacaoconjuntoreferencia,
vwa.id_avaliacao,
vwi.st_coordenador,
vwi.id_alocacao

-- sp_help vw_matricula;

  from vw_alunogradeintegracao as vwi 
join tb_saladeaula as s
	on (vwi.id_saladeaula = s.id_saladeaula)
join vw_avaliacaoaluno as vwa 
	on (vwi.id_matricula = vwa.id_matricula and vwa.id_saladeaula = vwi.id_saladeaula and vwa.id_tipoavaliacao = 6)
-- join tb_alocacao as al
--	on (al.id_saladeaula = s.id_saladeaula and al.id_matriculadisciplina = vwa.id_matriculadisciplina and al.bl_ativo = 1)
left join vw_encerramentoalunos as vwe 
	on (vwi.id_matricula = vwe.id_matricula and vwa.id_saladeaula = vwe.id_saladeaula)
where vwi.id_tipodisciplina = 2 and (vwi.bl_encerrado = 0 or vwi.bl_encerrado is null)
and vwi.id_situacaotcc = 122
and DATEADD(day, s.nu_diasextensao, vwa.dt_encerramentosala) > GetDate();
GO


ALTER VIEW [dbo].[vw_matricula]
AS
 SELECT
	mt.id_usuario,
	us.st_nomecompleto,
	us.st_cpf,
	ps.st_urlavatar,
	ps.dt_nascimento,
	ps.st_nomepai,
	ps.st_nomemae,
	ps.st_email,
	CAST(ps.nu_ddd AS VARCHAR) + '-' + CAST(ps.nu_telefone AS VARCHAR) AS st_telefone,
	CAST(ps.nu_dddalternativo AS VARCHAR) + '-'
	+ CAST(ps.nu_telefonealternativo AS VARCHAR) AS st_telefonealternativo,
	mt.id_matricula,
	pp.st_projetopedagogico,
	mt.id_situacao,
	st.st_situacao,
	mt.id_evolucao,
	ev.st_evolucao,
	mt.dt_concluinte,
	mt.id_entidadematriz,
	etmz.st_nomeentidade AS st_entidadematriz,
	mt.id_entidadematricula,
	etm.st_nomeentidade AS st_entidadematricula,
	etm.st_urlportal as st_urlportalmatricula,
	mt.id_entidadeatendimento,
	eta.st_nomeentidade AS st_entidadeatendimento,
	mt.bl_ativo,
	cm.id_contrato,
	pp.id_projetopedagogico,
	mt.dt_termino,
	mt.dt_cadastro,
	mt.dt_inicio,
	tm.id_turma,
	tm.st_turma,
	tm.st_codigo,
	tm.dt_inicio AS dt_inicioturma,
	mt.bl_institucional,
	tm.dt_fim AS dt_terminoturma,
	mt.id_situacaoagendamento,
	mt.id_vendaproduto,
	mt.dt_termino AS dt_terminomatricula,
	CAST(RIGHT(master.dbo.fn_varbintohexstr(HASHBYTES('MD5',
	(CAST(us.id_usuario AS VARCHAR(10)) COLLATE Latin1_General_CI_AI
	+ CAST((CASE
		WHEN mt.id_matricula IS NULL THEN 0
		ELSE mt.id_matricula
	END) AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
	+ CAST(5 AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
	+ CAST((0) AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
	+ us.st_login
	+ us.st_senha
	+ CONVERT(VARCHAR(10), GETDATE(), 103)))),
	32) AS CHAR(32)) COLLATE Latin1_General_CI_AI AS st_urlacesso,
	mt.bl_documentacao,
	vprod.id_venda,
	eta.st_urlportal,
	mt.st_codcertificacao,
	ps.st_identificacao,
	CONVERT(VARCHAR, mtc.dt_cadastro, 103) AS st_dtcadastrocertificacao,
	CONVERT(VARCHAR, mtc.dt_enviocertificado, 103) AS st_dtenviocertificado,
	CONVERT(VARCHAR, mtc.dt_retornocertificadora, 103) AS st_dtretornocertificadora,
	CONVERT(VARCHAR, mtc.dt_envioaluno, 103) AS st_dtenvioaluno,
	st_codigoacompanhamento,
	mtc.id_indicecertificado,
	mt.id_evolucaocertificacao,
	ap.id_areaconhecimento,
	pp.bl_disciplinacomplementar,
	mt.id_matriculavinculada,
	cr.id_contratoregra,
	trc.id_tiporegracontrato,
	etm.st_siglaentidade,
	mt.bl_academico,
	mtc.dt_cadastro AS dt_cadastrocertificacao,
	mtc.dt_enviocertificado,
	mtc.dt_retornocertificadora,
	mtc.dt_envioaluno,
	vup.st_upload AS st_uploadcontrato,
	canc.id_cancelamento,
	mt.id_aproveitamento,
  vprod.id_produto,
  pp.st_descricao,
  pp.st_imagem
FROM tb_matricula mt
JOIN tb_projetopedagogico AS pp
	ON mt.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_areaprojetopedagogico AS ap
	ON ap.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_entidade AS etmz
	ON etmz.id_entidade = mt.id_entidadematriz
JOIN tb_entidade AS etm
	ON etm.id_entidade = mt.id_entidadematricula
JOIN tb_entidade AS eta
	ON eta.id_entidade = mt.id_entidadeatendimento
JOIN tb_situacao AS st
	ON st.id_situacao = mt.id_situacao
JOIN tb_evolucao AS ev
	ON ev.id_evolucao = mt.id_evolucao
JOIN tb_usuario AS us
	ON us.id_usuario = mt.id_usuario
LEFT JOIN tb_contratomatricula AS cm
	ON cm.id_matricula = mt.id_matricula
	AND cm.bl_ativo = 1
LEFT JOIN vw_pessoa AS ps
	ON mt.id_usuario = ps.id_usuario
	AND mt.id_entidadeatendimento = ps.id_entidade
LEFT JOIN dbo.tb_contrato AS ct
	ON ct.id_contrato = cm.id_contrato
	AND ct.bl_ativo = 1
LEFT JOIN tb_turma AS tm
	ON tm.id_turma = mt.id_turma
LEFT JOIN tb_matriculacertificacao mtc
	ON mtc.id_matricula = mt.id_matricula
	AND mtc.bl_ativo = 1
LEFT JOIN dbo.tb_contratoregra AS cr
	ON cr.id_contratoregra = ct.id_contratoregra
LEFT JOIN dbo.tb_tiporegracontrato AS trc
	ON trc.id_tiporegracontrato = cr.id_tiporegracontrato
LEFT JOIN dbo.tb_venda AS v
	ON ct.id_venda = v.id_venda
LEFT JOIN dbo.tb_upload AS vup
	ON vup.id_upload = v.id_uploadcontrato
LEFT JOIN dbo.tb_cancelamento AS canc
	ON canc.id_cancelamento = mt.id_cancelamento
OUTER APPLY (SELECT TOP 1 tb_vendaproduto.* FROM dbo.tb_vendaproduto
	JOIN tb_produtoprojetopedagogico AS ppd ON ppd.id_produto = tb_vendaproduto.id_produto
	JOIN tb_venda AS vend ON vend.id_venda = tb_vendaproduto.id_venda AND vend.id_evolucao = 10 
  WHERE tb_vendaproduto.id_matricula = mt.id_matricula
  AND tb_vendaproduto.bl_ativo = 1
  ORDER BY tb_vendaproduto.id_vendaproduto DESC) AS vprod
WHERE mt.bl_ativo = 1
GO


ALTER VIEW [dbo].[vw_alunogradeintegracao] AS SELECT DISTINCT
            'dt_abertura' = CASE WHEN sa.id_tiposaladeaula = 2 
									THEN sa.dt_abertura
								WHEN sa.id_tiposaladeaula = 1 AND (CAST(sa.dt_abertura AS DATE) >= CAST(al.dt_inicio AS DATE))
									THEN CAST(al.dt_cadastro AS DATE)
								ELSE CAST(al.dt_inicio AS DATE)
									 
                            END ,
            mt.id_usuario ,
            us.st_nomecompleto ,
            us.st_cpf ,
            ps.st_nomeexibicao ,
            ps.st_urlavatar ,
            ai.bl_encerrado ,
            al.id_alocacao ,
            mt.id_matricula ,
            mt.id_projetopedagogico ,
            pp.st_tituloexibicao AS st_tituloexibicaoprojeto ,
            ml.id_modulo ,
            ml.st_tituloexibicao AS st_tituloexibicaomodulo ,
            md.id_evolucao ,
            md.id_disciplina ,
            dc.st_tituloexibicao AS st_disciplina ,
            al.id_saladeaula ,
            sa.id_categoriasala ,
            mt.id_entidadeatendimento AS id_entidade ,
			sa.id_entidade AS id_entidadesala,
            st_codusuario = CASE WHEN ui.st_codusuario IS NOT NULL
                                 THEN ui.st_codusuario
                                 ELSE ui2.st_codusuario
                            END ,
            st_loginintegrado = CASE WHEN ui.st_loginintegrado IS NOT NULL
                                     THEN ui.st_loginintegrado
                                     ELSE ui2.st_loginintegrado
                                END ,
            st_senhaintegrada = CASE WHEN ui.st_senhaintegrada IS NOT NULL
                                     THEN ui.st_senhaintegrada
                                     ELSE ui2.st_senhaintegrada
                                END ,
            si.st_codsistemacurso ,
            ai.st_codalocacao ,
            ai.id_sistema ,
            st_codsistemaent = CASE WHEN ei.st_codsistema IS NOT NULL
                                    THEN ei.st_codsistema
                                    ELSE ei2.st_codsistema
                               END ,
			tu1.id_usuario AS id_professor ,
            tu1.st_nomecompleto AS st_professor ,
            tu3.st_nomecompleto AS st_coordenador ,
            st_caminho = CASE WHEN ei.st_caminho IS NOT NULL
                              THEN ei.st_caminho
                              ELSE ei2.st_caminho
                         END ,
            dc.id_tipodisciplina ,
            dc.st_imagem as st_imagemdisciplina,

            sa.st_saladeaula ,
            dc.st_descricao AS st_descricaodisciplina ,
            'x' AS st_integracao ,
            'id_status' = CASE 
															 WHEN al.id_alocacao is not null and ai.st_codalocacao is null and sa.dt_abertura < getdate() THEN 4
                               WHEN sa.dt_abertura > CAST(GETDATE() AS DATE)
                               THEN 3
                               WHEN CAST(sa.dt_abertura AS DATE) <= CAST(GETDATE() AS DATE)
                                    AND ( CAST(sa.dt_encerramento AS DATE) >= CAST(GETDATE() AS DATE)
                                          OR sa.dt_encerramento IS NULL
                                        ) THEN 1
                               WHEN (sa.dt_encerramento IS NOT NULL) AND ((DATEADD(day, ISNULL(sa.nu_diasextensao, 0), sa.dt_encerramento)) < CAST(GETDATE() AS DATE) ) THEN 2
							   WHEN sa.dt_encerramento IS NULL AND DATEADD(DAY,
                                                      ISNULL(sa.nu_diasencerramento,
                                                             0)
                                                      + ISNULL(sa.nu_diasaluno,
                                                              0)
                                                      + ISNULL(sa.nu_diasextensao,
                                                              0),
    al.dt_inicio) >= CAST(GETDATE() AS DATE) THEN 1
													  WHEN sa.dt_encerramento IS NULL AND DATEADD(DAY,
                                                      ISNULL(sa.nu_diasencerramento,
                                                             0)
                                                      + ISNULL(sa.nu_diasaluno,
                                                              0)
                                                      + ISNULL(sa.nu_diasextensao,
                                                              0),
                                                      al.dt_inicio) < CAST(GETDATE() AS DATE) THEN 2
                               
                               ELSE 1
                          END ,
            'st_status' = CASE WHEN sa.dt_abertura > CAST(GETDATE() AS DATE)
                               THEN 'Não Iniciada'
                              WHEN sa.dt_abertura <= CAST(GETDATE() AS DATE)
                                    AND ( sa.dt_encerramento IS NULL OR
										( (sa.dt_encerramento >= CAST(GETDATE() AS DATE) OR (DATEADD (day, ISNULL(sa.nu_diasextensao, 0), sa.dt_encerramento)) >= CAST(GETDATE() AS DATE) ))
                                        ) THEN 'Aberta'
                               WHEN sa.dt_encerramento < CAST(GETDATE() AS DATE)
                               THEN 'Fechada'
                               ELSE 'Erro'
                          END ,
            'dt_encerramento' = CASE WHEN sa.bl_semdiasaluno = 1
                                          AND sa.bl_semencerramento = 1
                                          AND si.id_sistema = 6
                                     THEN DATEADD(DAY, 1, GETDATE())
                                     WHEN sa.bl_semdiasaluno = 1
                                          AND sa.bl_semencerramento = 1
                                          AND si.id_sistema = 2
                                          AND md.id_evolucao != 12
                                     THEN DATEADD(DAY, 1, GETDATE())
                                     WHEN sa.bl_semdiasaluno = 1
                                          AND sa.bl_semencerramento = 1
                                          AND si.id_sistema = 2
                                          AND md.id_evolucao = 12
                                     THEN DATEADD(DAY, -1, GETDATE())
                                     WHEN sa.bl_semdiasaluno = 1
                                          AND sa.bl_semencerramento = 0
                                     THEN DATEADD(DAY,
                                                  sa.nu_diasencerramento
                                                  + sa.nu_diasextensao,
                                                  sa.dt_encerramento)
                                     WHEN sa.bl_semdiasaluno = 0
                                          AND sa.bl_semencerramento = 0
                                          AND sa.dt_abertura <= al.dt_cadastro
                                     THEN DATEADD(DAY,
                                                  sa.nu_diasencerramento
                                                  + sa.nu_diasaluno
                                                  + sa.nu_diasextensao,
                                                  al.dt_cadastro)
                                     WHEN sa.bl_semdiasaluno = 0
                                          AND sa.bl_semencerramento = 0
                                          AND sa.dt_abertura > al.dt_cadastro
                                     THEN DATEADD(DAY,
                                                  sa.nu_diasencerramento
                                                  + sa.nu_diasaluno
                                                  + sa.nu_diasextensao,
                                                  sa.dt_abertura)
 WHEN sa.bl_semdiasaluno = 0
                                          AND sa.bl_semencerramento = 1
                                     THEN DATEADD(DAY, 1, GETDATE())
									 WHEN sa.id_tiposaladeaula = 1 
										THEN DATEADD(DAY, CAST(sa.nu_diasaluno AS INT) + CAST(sa.nu_diasextensao AS INT) , CAST (al.dt_inicio AS DATE))
                                END ,
            'dt_encerramentosala' = CASE WHEN sa.id_tiposaladeaula = 2 and sa.dt_encerramento is not null
                                         THEN DATEADD(DAY, ISNULL(sa.nu_diasencerramento,0)+ ISNULL(sa.nu_diasaluno,0)+ ISNULL(sa.nu_diasextensao,0),sa.dt_encerramento)
                                         WHEN  sa.id_tiposaladeaula = 2  and sa.dt_encerramento is null
                                         THEN DATEADD(DAY,ISNULL(sa.nu_diasencerramento,0)+ ISNULL(sa.nu_diasaluno,0)+ ISNULL(sa.nu_diasextensao,0),al.dt_cadastro)
                                         WHEN  sa.id_tiposaladeaula = 1 AND al.dt_inicio is not null
                                         THEN DATEADD(DAY,ISNULL(sa.nu_diasencerramento,0)+ ISNULL(sa.nu_diasaluno,0)+ ISNULL(sa.nu_diasextensao,0),al.dt_inicio)
										 WHEN  sa.id_tiposaladeaula = 1 AND al.dt_inicio is null
                                         THEN DATEADD(DAY,ISNULL(sa.nu_diasencerramento,0)+ ISNULL(sa.nu_diasaluno,0)+ ISNULL(sa.nu_diasextensao,0),al.dt_cadastro)
                                         ELSE NULL
                                    END ,
  
    dt_encerramentoportal = CAST(
    (CASE
      -- Periodico
      WHEN sa.id_tiposaladeaula = 2
        THEN DATEADD(DAY, ISNULL(sa.nu_diasencerramento, 0) + ISNULL(sa.nu_diasaluno, 0) + ISNULL(sa.nu_diasextensao, 0), sa.dt_encerramento)
      -- Permanente
      --WHEN sa.id_tiposaladeaula = 1 AND sa.dt_abertura <= al.dt_cadastro
        --THEN DATEADD(DAY, ISNULL(sa.nu_diasencerramento, 0) + ISNULL(sa.nu_diasaluno, 0) + ISNULL(sa.nu_diasextensao, 0), al.dt_cadastro)
      -- Permanente
      WHEN sa.id_tiposaladeaula = 1 --AND sa.dt_abertura > al.dt_cadastro
        THEN DATEADD(DAY, ISNULL(sa.nu_diasencerramento, 0) + ISNULL(sa.nu_diasaluno, 0) + ISNULL(sa.nu_diasextensao, 0), al.dt_inicio)
      ELSE NULL
    END)
  AS DATE),

            al.id_situacaotcc ,
            id_aproparcial = CASE WHEN an.id_avaliacaoaluno IS NULL THEN 0
                                  ELSE 1
                             END
			, sa.id_tiposaladeaula, aps.nu_diasacesso
			--,'' as st_imagemdisciplina
    FROM    tb_matricula AS mt
            JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
            JOIN tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
            JOIN tb_modulo AS ml ON ml.id_projetopedagogico = mt.id_projetopedagogico
                                    AND ml.bl_ativo = 1
            LEFT JOIN tb_modulodisciplina AS mdc ON mdc.id_modulo = ml.id_modulo
                                                    AND mdc.id_disciplina = md.id_disciplina
                                                    AND mdc.bl_ativo = 1
            JOIN tb_alocacao AS al ON md.id_matriculadisciplina = al.id_matriculadisciplina
                                      AND al.bl_ativo = 1
            JOIN tb_disciplina AS dc ON dc.id_disciplina = md.id_disciplina
                                        AND mdc.id_disciplina = dc.id_disciplina
            JOIN tb_usuario AS us ON us.id_usuario = mt.id_usuario
            JOIN tb_pessoa AS ps ON ps.id_usuario = mt.id_usuario
                                    AND ps.id_entidade = mt.id_entidadeatendimento
            JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula
                                            AND sa.bl_ativa = 1
			JOIN tb_areaprojetosala as aps on aps.id_saladeaula = sa.id_saladeaula and aps.id_projetopedagogico = mt.id_projetopedagogico
            LEFT JOIN tb_saladeaulaintegracao AS si ON sa.id_saladeaula = si.id_saladeaula and si.id_sistema IN ( 6,
                                                              11 )
            LEFT JOIN tb_alocacaointegracao AS ai ON al.id_alocacao = ai.id_alocacao
            LEFT JOIN tb_usuariointegracao AS ui ON mt.id_usuario = ui.id_usuario
                                                    AND ai.id_sistema = ui.id_sistema
                                                    AND sa.id_entidade = ui.id_entidade
                                                    AND ui.id_sistema IN ( 6,
                                                              11 )
            LEFT JOIN dbo.tb_entidadeintegracao AS ei ON ei.id_entidade = sa.id_entidade
                                                         AND ei.id_sistema = si.id_sistema
                                                         AND ei.id_sistema IN (
                                                         6, 11 )
            LEFT JOIN tb_usuariointegracao AS ui2 ON mt.id_usuario = ui2.id_usuario
                                                     AND ai.id_sistema = ui2.id_sistema
                                                     AND mt.id_entidadeatendimento = ui2.id_entidade
                                                     AND ui2.id_sistema = 999
            LEFT JOIN dbo.tb_entidadeintegracao AS ei2 ON ei2.id_entidade = mt.id_entidadeatendimento
                                                          AND ei2.id_sistema = si.id_sistema
                                                          AND ei2.id_sistema = 999
            LEFT JOIN tb_usuarioperfilentidadereferencia tu ON sa.id_saladeaula = tu.id_saladeaula
                                                              AND tu.bl_titular = 1
                                                              AND tu.bl_ativo = 1
            LEFT JOIN tb_usuario tu1 ON tu.id_usuario = tu1.id_usuario
            LEFT JOIN tb_usuarioperfilentidadereferencia tu2 ON tu2.id_projetopedagogico = mt.id_projetopedagogico
                                                              AND tu2.bl_titular = 1
                                                              AND tu2.bl_ativo = 1 AND TU2.id_disciplina IS NULL
            LEFT JOIN tb_usuario tu3 ON tu2.id_usuario = tu3.id_usuario

  --verifica se tem aproveitamento de nota
            OUTER APPLY ( SELECT TOP 1
                                    aa.id_avaliacaoaluno
                          FROM      dbo.tb_avaliacaoaluno AS aa
                                    JOIN dbo.tb_avaliacaoconjuntoreferencia AS acr ON aa.id_avaliacaoconjuntoreferencia = acr.id_avaliacaoconjuntoreferencia
                          WHERE     aa.id_matricula = mt.id_matricula
                                    AND acr.id_saladeaula = sa.id_saladeaula
                                    AND aa.id_tiponota = 2
                                    AND aa.bl_ativo = 1
                        ) AS an
    WHERE   mt.id_evolucao IN ( 6, 15 )




GO

ALTER VIEW [rel].[vw_cronogramasalas] as
SELECT DISTINCT
        sa.id_saladeaula ,
        sa.st_saladeaula ,
        sa.dt_inicioinscricao ,
        sa.dt_fiminscricao ,
        sa.dt_abertura ,
        sa.dt_encerramento ,
        cast(dt_atualiza as date) as dt_atualiza,
        st_disciplina ,
        dc.st_tituloexibicao AS st_tituloexibicaodisciplina ,
        st_projetopedagogico ,
        aps.id_projetopedagogico,
        uss.st_nomecompleto AS st_nomeprofessor ,
        usp.id_usuario AS id_coordenador,
        usp.st_nomecompleto AS st_nomecoordenador,
        si.st_codsistemacurso AS st_codava,
        sa.id_entidade,
		sa.id_categoriasala,
    pl.st_periodoletivo,
    pp.bl_disciplinacomplementar,
		CAST(sa.dt_cadastro as date) as dt_cadastro,
		cast(COUNT(ta.id_matriculadisciplina) AS varchar) AS st_alunos
        FROM dbo.tb_saladeaula AS sa
JOIN dbo.tb_areaprojetosala AS aps ON aps.id_saladeaula = sa.id_saladeaula
JOIN dbo.tb_disciplinasaladeaula AS da ON da.id_saladeaula = sa.id_saladeaula
JOIN tb_disciplina AS dc ON dc.id_disciplina = da.id_disciplina
JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = aps.id_projetopedagogico
LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS ups ON ups.id_saladeaula = aps.id_saladeaula AND ups.bl_titular = 1 and ups.bl_ativo=1
LEFT JOIN dbo.tb_usuario AS uss ON uss.id_usuario = ups.id_usuario
LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS upp ON upp.id_projetopedagogico = pp .id_projetopedagogico AND upp.bl_titular = 1 and upp.bl_ativo=1 and upp.id_disciplina is null
LEFT JOIN dbo.tb_perfil AS tbp ON tbp.id_perfil = upp.id_perfil AND tbp.id_perfilpedagogico = 2
LEFT JOIN dbo.tb_usuario AS usp ON usp.id_usuario = upp.id_usuario
LEFT JOIN dbo.tb_saladeaulaintegracao AS si ON si.id_saladeaula = sa.id_saladeaula
LEFT JOIN dbo.tb_alocacao AS ta ON ta.id_saladeaula = sa.id_saladeaula AND ta.bl_ativo = 1
LEFT JOIN tb_periodoletivo as pl on pl.id_periodoletivo = sa.id_periodoletivo
WHERE sa.bl_ativa = 1 AND sa.id_situacao != 74
GROUP by
sa.id_saladeaula,
st_saladeaula ,
        sa.dt_inicioinscricao ,
        sa.dt_fiminscricao ,
        sa.dt_abertura ,
        sa.dt_encerramento ,
        dt_atualiza,
        st_disciplina ,
        dc.st_tituloexibicao ,
        st_projetopedagogico ,
        aps.id_projetopedagogico,
        uss.st_nomecompleto ,
        usp.id_usuario ,
        usp.st_nomecompleto,
        si.st_codsistemacurso,
        sa.id_entidade,
		sa.dt_cadastro,
		sa.id_categoriasala,
  pl.st_periodoletivo,
  pp.bl_disciplinacomplementar

GO

ALTER VIEW [dbo].[vw_gerardeclaracaoimpostorenda] AS
SELECT DISTINCT
  us.id_usuario,
  us.st_cpf AS st_cpfaluno,
  us.st_nomecompleto,
  ed.id_endereco,
  ed.st_endereco,
  ed.nu_numero,
  ed.st_bairro,
  ed.st_cidade,
  ed.st_cep,
  et.st_nomeentidade,
  et.id_entidade,
  ma.id_matricula,
  ct.id_venda
FROM dbo.tb_usuario us
  LEFT JOIN dbo.tb_pessoaendereco AS pe ON pe.id_usuario = us.id_usuario
  JOIN dbo.tb_matricula AS ma ON ma.id_usuario = us.id_usuario
  JOIN dbo.tb_contratomatricula AS cm ON cm.id_matricula = ma.id_matricula
  JOIN dbo.tb_contrato AS ct ON cm.id_contrato = ct.id_contrato
  JOIN dbo.tb_endereco AS ed ON ed.id_endereco = pe.id_endereco AND ed.id_tipoendereco = 1
  LEFT JOIN dbo.tb_entidade AS et ON et.id_entidade = pe.id_entidade
GO

ALTER VIEW [dbo].[vw_avaliacaoagendamento] AS
  SELECT DISTINCT aa.id_avaliacaoagendamento,
                  aa.id_matricula,
                  us.id_usuario,
                  us.st_nomecompleto,
                  us.st_cpf,
                  av.id_avaliacao,
                  av.st_avaliacao,
                  ac.id_tipoprova,
                  tp.st_tipoprova,
                  aa.dt_agendamento,
                  aa.bl_automatico,
                  aa.id_situacao,
                  st.st_situacao,
                  aa.id_avaliacaoaplicacao,
                  aa.id_entidade,
                  et.st_nomeentidade,
                  ap.id_horarioaula,
                  ha.st_horarioaula,
                  mt.id_projetopedagogico,
                  pp.st_projetopedagogico,
                  pp.bl_possuiprova,
                  (CASE
                    WHEN pp.bl_possuiprova = 1
                      THEN 'SIM'
                      ELSE 'NÃO'
                  END) AS st_possuiprova,
                  aai.st_codsistema,
                  aa.bl_ativo,
                  ap.st_endereco,
                  ap.dt_aplicacao,
                  ap.id_aplicadorprova,
                  ap.st_aplicadorprova,
                  ap.st_cidade,
                  aa.nu_presenca,
                  aa.id_usuariocadastro,
                  aa.id_usuariolancamento,
                  usu_lancamento.st_nomecompleto AS st_usuariolancamento,
                  ( CASE
                      WHEN aa.id_tipodeavaliacao IS NULL THEN 0
                      ELSE aa.id_tipodeavaliacao
                    END ) AS id_tipodeavaliacao,
                  ( CASE
                      WHEN ( ap.dt_aplicacao IS NOT NULL
                             AND ap.dt_aplicacao > Getdate()
                             AND aa.bl_ativo = 1
                             AND aa.nu_presenca IS NULL ) THEN 0
                      WHEN ( ap.dt_aplicacao IS NOT NULL
                             AND ap.dt_aplicacao > Getdate()
                             AND aa.bl_ativo = 0
                             AND aa.id_situacao = 68
                             AND aa.nu_presenca IS NOT NULL ) THEN 1
                      WHEN ( ap.dt_aplicacao IS NOT NULL
                             AND ap.dt_aplicacao < Getdate()
                             AND aa.bl_ativo = 1
                             AND aa.id_situacao = 68
                             AND aa.nu_presenca IS NULL ) THEN 2
                      ELSE 3
                    END ) AS id_situacaopresenca
                  ,
                  ( CASE
                      WHEN ( ap.dt_aplicacao IS NOT NULL
                             AND ap.dt_aplicacao > Getdate()
                             AND aa.bl_ativo = 1
                             AND aa.nu_presenca IS NULL ) THEN 'Aguardando aplicação de prova'
                      WHEN ( ap.dt_aplicacao IS NOT NULL
                             AND ap.dt_aplicacao > Getdate()
                             AND aa.bl_ativo = 0
                             AND aa.id_situacao = 68
                             AND aa.nu_presenca IS NOT NULL ) THEN 'Presença lançada'
                      WHEN ( ap.dt_aplicacao IS NOT NULL
                             AND ap.dt_aplicacao < Getdate()
                             AND aa.bl_ativo = 1
                             AND aa.id_situacao = 68
                             AND aa.nu_presenca IS NULL ) THEN 'Aplicação realizada e presença não lançada'
                      ELSE 'Situação Indefinida'
                    END ) AS st_situacaopresenca
                  ,
                  tb_chamada.id_aval AS id_chamada,
                  primeira.id_primeirachamada,
                  primeirapresenca.nu_primeirapresenca,
                  primeira.id_segundachamada AS id_ultimachamada,
                  ultima.nu_ultimapresenca,
                  mt.id_evolucao,
                  aa.bl_provaglobal,
                  ap.dt_antecedenciaminima,
                  ap.dt_alteracaolimite,
                  ( CASE
                      WHEN aa.id_avaliacaoaplicacao IS NULL
                            OR ( ( Cast(Getdate() AS DATE) ) BETWEEN
                                 ap.dt_antecedenciaminima AND
                                       ap.dt_alteracaolimite
                                 AND aa.id_avaliacaoaplicacao =
                                     ap.id_avaliacaoaplicacao
                                 AND aa.bl_ativo = 1 ) THEN 0
                      ELSE 1
                    END ) AS id_mensagens,
                  ( CASE
                      WHEN ( Cast(Getdate() AS DATE) ) < ap.dt_antecedenciaminima THEN 'A data para o agendamento ainda não chegou'
                      WHEN ( Cast(Getdate() AS DATE) ) > ap.dt_alteracaolimite THEN 'Agendamento passou do prazo limite para alteração'
                      WHEN aa.id_avaliacaoaplicacao IS NULL THEN ''
                      ELSE 'Agendamento dentro do prazo limite para alteração'
                    END ) AS st_mensagens,
                  provasrealizadas.id_provasrealizadas,
                  Isnull(temnotafinal.bl_temnotafinal, 0) AS bl_temnotafinal,
                  pe.st_email,
                  pe.nu_ddi,
                  pe.nu_ddd,
                  pe.nu_telefone,
                  pe.nu_ddialternativo,
                  pe.nu_dddalternativo,
                  pe.nu_telefonealternativo,
                  ha.hr_inicio,
                  ha.hr_fim,
                  ap.sg_uf,
                  ap.st_complemento,
                  ap.nu_numero,
                  ap.st_bairro,
                  ap.st_telefone AS 'st_telefoneaplicador',
                  NULL AS id_disciplina,
                  NULL AS st_disciplina
     FROM tb_avaliacaoagendamento aa
         INNER JOIN tb_matricula AS mt ON mt.id_matricula = aa.id_matricula
         INNER JOIN tb_entidade AS ett ON ett.id_entidade = mt.id_entidadeatendimento
         INNER JOIN tb_usuario AS us ON us.id_usuario = mt.id_usuario
         INNER JOIN tb_avaliacao AS av ON av.id_avaliacao = aa.id_avaliacao
         INNER JOIN tb_avaliacaoconjuntorelacao AS acr ON acr.id_avaliacao = av.id_avaliacao
         INNER JOIN tb_avaliacaoconjunto AS ac ON ac.id_avaliacaoconjunto = acr.id_avaliacaoconjunto
         INNER JOIN tb_tipoprova AS tp ON tp.id_tipoprova = ac.id_tipoprova
         INNER JOIN tb_situacao AS st ON st.id_situacao = aa.id_situacao
         INNER JOIN tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
         INNER JOIN vw_pessoa AS pe ON pe.id_usuario = us.id_usuario AND pe.id_entidade = mt.id_entidadematricula
         INNER JOIN tb_esquemaconfiguracao AS ec ON ett.id_esquemaconfiguracao = ec.id_esquemaconfiguracao
         INNER JOIN tb_esquemaconfiguracaoitem AS eci ON eci.id_esquemaconfiguracao = ec.id_esquemaconfiguracao AND eci.id_itemconfiguracao = 24 AND eci.st_valor = 0
         LEFT JOIN vw_avaliacaoaplicacao AS ap ON ap.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao AND ap.bl_ativo = 1 AND ap.id_entidade = aa.id_entidade
         LEFT JOIN tb_entidade AS et ON et.id_entidade = ap.id_entidade
         LEFT JOIN tb_horarioaula AS ha ON ha.id_horarioaula = ap.id_horarioaula
         LEFT JOIN tb_avalagendaintegracao AS aai ON aai.id_avaliacaoagendamento = aa.id_avaliacaoagendamento
         OUTER apply (SELECT st_nomecompleto
                      FROM   tb_usuario
                      WHERE  id_usuario = aa.id_usuariolancamento) AS
                     usu_lancamento
         OUTER apply (SELECT Count(cha.id_avaliacaoagendamento) AS id_aval
                      FROM   tb_avaliacaoagendamento AS cha
                             LEFT JOIN tb_avaliacaoaplicacao AS ch
                                    ON ch.id_avaliacaoaplicacao =
                                       cha.id_avaliacaoaplicacao
                      WHERE  cha.id_matricula = mt.id_matricula
                             AND cha.id_tipodeavaliacao = aa.id_tipodeavaliacao
                             AND cha.id_situacao = 68) AS tb_chamada
         OUTER apply (SELECT Min(id_avaliacaoagendamento) AS id_primeirachamada,
                      Max(id_avaliacaoagendamento) AS id_segundachamada
                      FROM   tb_avaliacaoagendamento
                      WHERE  id_matricula = aa.id_matricula
                             AND id_situacao = 68
                             AND id_tipodeavaliacao = aa.id_tipodeavaliacao) AS
                     primeira
         OUTER apply (SELECT nu_presenca AS nu_primeirapresenca
                      FROM   tb_avaliacaoagendamento
                      WHERE  id_avaliacaoagendamento =
                             (SELECT Min(id_avaliacaoagendamento)
                              FROM   tb_avaliacaoagendamento
                              WHERE  id_matricula = aa.id_matricula
                                     AND id_situacao = 68
                                     AND id_tipodeavaliacao =
                                         aa.id_tipodeavaliacao
                                     )) AS primeirapresenca
         OUTER apply (SELECT nu_presenca AS nu_ultimapresenca
                      FROM   tb_avaliacaoagendamento
                      WHERE  id_avaliacaoagendamento =
                             (SELECT Max(id_avaliacaoagendamento)
                              FROM   tb_avaliacaoagendamento
                              WHERE  id_matricula = aa.id_matricula
                                     AND id_situacao = 68
                                     AND id_tipodeavaliacao =
                                         aa.id_tipodeavaliacao
                                     )) AS ultima
         OUTER apply (SELECT Count(id_avaliacaoagendamento) AS
                             id_provasrealizadas
                      FROM   tb_avaliacaoagendamento
                      WHERE  id_matricula = aa.id_matricula
                             AND nu_presenca = 1
                             AND bl_ativo = 0
                             AND id_situacao = 68) AS provasrealizadas
         OUTER apply (SELECT ( CASE
                                 WHEN st_nota IS NULL THEN 0
                                 ELSE 1
                               END ) AS bl_temnotafinal
                      FROM   tb_avaliacaoaluno
                      WHERE  id_matricula = mt.id_matricula
                             AND id_avaliacao = aa.id_avaliacao
                             AND id_situacao = 86) AS temnotafinal
    WHERE aa.id_situacao <> 70
      AND ac.id_tipoprova = 2

  UNION

  SELECT DISTINCT aa.id_avaliacaoagendamento,
                  aa.id_matricula,
                  us.id_usuario,
                  us.st_nomecompleto,
                  us.st_cpf,
                  av.id_avaliacao,
                  av.st_avaliacao,
                  ac.id_tipoprova,
                  tp.st_tipoprova,
                  aa.dt_agendamento,
                  aa.bl_automatico,
                  aa.id_situacao,
                  st.st_situacao,
                  aa.id_avaliacaoaplicacao,
                  aa.id_entidade,
                  et.st_nomeentidade,
                  ap.id_horarioaula,
                  ha.st_horarioaula,
                  mt.id_projetopedagogico,
                  pp.st_projetopedagogico,
                  pp.bl_possuiprova,
                  (CASE
                    WHEN pp.bl_possuiprova = 1
                      THEN 'SIM'
                      ELSE 'NÃO'
                  END) AS st_possuiprova,    
                  aai.st_codsistema,
                  aa.bl_ativo,
                  ap.st_endereco,
                  ap.dt_aplicacao,
                  ap.id_aplicadorprova,
                  ap.st_aplicadorprova,
                  ap.st_cidade,
                  aa.nu_presenca,
                  aa.id_usuariocadastro,
                  aa.id_usuariolancamento,
                  usu_lancamento.st_nomecompleto AS st_usuariolancamento,
                  ( CASE
                      WHEN aa.id_tipodeavaliacao IS NULL THEN 0
                      ELSE aa.id_tipodeavaliacao
                    END ) AS id_tipodeavaliacao,
                  ( CASE
                      WHEN ( ap.dt_aplicacao IS NOT NULL
                             AND ap.dt_aplicacao > Getdate()
                     AND aa.bl_ativo = 1
                             AND aa.nu_presenca IS NULL ) THEN 0
                      WHEN ( ap.dt_aplicacao IS NOT NULL
                             AND ap.dt_aplicacao > Getdate()
                             AND aa.bl_ativo = 0
                             AND aa.id_situacao = 68
                             AND aa.nu_presenca IS NOT NULL ) THEN 1
                      WHEN ( ap.dt_aplicacao IS NOT NULL
                             AND ap.dt_aplicacao < Getdate()
                             AND aa.bl_ativo = 1
                             AND aa.id_situacao = 68
                             AND aa.nu_presenca IS NULL ) THEN 2
                      ELSE 3
                    END ) AS id_situacaopresenca
                  ,
                  ( CASE
                      WHEN ( ap.dt_aplicacao IS NOT NULL
                             AND ap.dt_aplicacao > Getdate()
                             AND aa.bl_ativo = 1
                             AND aa.nu_presenca IS NULL ) THEN 'Aguardando aplicação de prova'
                      WHEN ( ap.dt_aplicacao IS NOT NULL
                             AND ap.dt_aplicacao > Getdate()
                             AND aa.bl_ativo = 0
                             AND aa.id_situacao = 68
                             AND aa.nu_presenca IS NOT NULL ) THEN 'Presença lançada'
                      WHEN ( ap.dt_aplicacao IS NOT NULL
                             AND ap.dt_aplicacao < Getdate()
                             AND aa.bl_ativo = 1
                             AND aa.id_situacao = 68
                             AND aa.nu_presenca IS NULL ) THEN 'Aplicação realizada e presença não lançada'
                      ELSE 'Situação Indefinida'
                    END ) AS st_situacaopresenca
                  ,
                  tb_chamada.id_aval AS id_chamada,
                  primeira.id_primeirachamada,
                  primeirapresenca.nu_primeirapresenca,
                  primeira.id_segundachamada AS id_ultimachamada,
                  ultima.nu_ultimapresenca,
                  mt.id_evolucao,
                  aa.bl_provaglobal,
                  ap.dt_antecedenciaminima,
                  ap.dt_alteracaolimite,
                  ( CASE
                      WHEN aa.id_avaliacaoaplicacao IS NULL
                            OR ( ( Cast(Getdate() AS DATE) ) BETWEEN
                                       ap.dt_antecedenciaminima AND
                                       ap.dt_alteracaolimite
                                 AND aa.id_avaliacaoaplicacao =
                                     ap.id_avaliacaoaplicacao
                                 AND aa.bl_ativo = 1 ) THEN 0
                      ELSE 1
                    END ) AS id_mensagens,
                  ( CASE
                      WHEN ( Cast(Getdate() AS DATE) ) < ap.dt_antecedenciaminima THEN 'A data para o agendamento ainda não chegou'
                      WHEN ( Cast(Getdate() AS DATE) ) > ap.dt_alteracaolimite THEN 'Agendamento passou do prazo limite para alteração'
                      WHEN aa.id_avaliacaoaplicacao IS NULL THEN ''
                      ELSE 'Agendamento dentro do prazo limite para alteração'
                    END ) AS st_mensagens,
                  provasrealizadas.id_provasrealizadas,
                  Isnull(temnotafinal.bl_temnotafinal, 0) AS bl_temnotafinal,
                  pe.st_email,
                  pe.nu_ddi,
                  pe.nu_ddd,
                  pe.nu_telefone,
                  pe.nu_ddialternativo,
                  pe.nu_dddalternativo,
                  pe.nu_telefonealternativo,
                  ha.hr_inicio,
                  ha.hr_fim,
                  ap.sg_uf,
                  ap.st_complemento,
                  ap.nu_numero,
                  ap.st_bairro,
                  ap.st_telefone AS 'st_telefoneaplicador',
                  md.id_disciplina,
                  dis.st_disciplina
    FROM tb_avaliacaoagendamento aa
         INNER JOIN tb_avalagendamentoref AS agr ON aa.id_avaliacaoagendamento = agr.id_avaliacaoagendamento
         INNER JOIN tb_avaliacaoconjuntoreferencia AS acrf ON acrf.id_avaliacaoconjuntoreferencia = agr.id_avaliacaoconjuntoreferencia
         INNER JOIN tb_matricula AS mt ON mt.id_matricula = aa.id_matricula
         INNER JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
         INNER JOIN tb_disciplina AS dis ON md.id_disciplina = dis.id_disciplina
         INNER JOIN tb_alocacao AS al ON al.id_matriculadisciplina = md.id_matriculadisciplina AND al.bl_ativo = 1 AND al.id_saladeaula = acrf.id_saladeaula
         INNER JOIN tb_entidade AS ett ON ett.id_entidade = mt.id_entidadeatendimento
         INNER JOIN tb_usuario AS us ON us.id_usuario = mt.id_usuario
         INNER JOIN tb_avaliacao AS av ON av.id_avaliacao = aa.id_avaliacao
         INNER JOIN tb_avaliacaoconjuntorelacao AS acr ON acr.id_avaliacao = av.id_avaliacao
         INNER JOIN tb_avaliacaoconjunto AS ac ON ac.id_avaliacaoconjunto = acr.id_avaliacaoconjunto
         INNER JOIN tb_tipoprova AS tp ON tp.id_tipoprova = ac.id_tipoprova
         INNER JOIN tb_situacao AS st ON st.id_situacao = aa.id_situacao
         INNER JOIN tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
         INNER JOIN vw_pessoa AS pe ON pe.id_usuario = us.id_usuario AND pe.id_entidade = mt.id_entidadematricula
         INNER JOIN tb_esquemaconfiguracao AS ec ON ett.id_esquemaconfiguracao = ec.id_esquemaconfiguracao
         INNER JOIN tb_esquemaconfiguracaoitem AS eci ON eci.id_esquemaconfiguracao = ec.id_esquemaconfiguracao AND eci.id_itemconfiguracao = 24 AND eci.st_valor = 1
         LEFT JOIN vw_avaliacaoaplicacao AS ap ON ap.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao AND ap.bl_ativo = 1 AND ap.id_entidade = aa.id_entidade
         LEFT JOIN tb_entidade AS et ON et.id_entidade = ap.id_entidade
         LEFT JOIN tb_horarioaula AS ha ON ha.id_horarioaula = ap.id_horarioaula
         LEFT JOIN tb_avalagendaintegracao AS aai ON aai.id_avaliacaoagendamento = aa.id_avaliacaoagendamento
         OUTER apply (SELECT st_nomecompleto
                      FROM   tb_usuario
                      WHERE  id_usuario = aa.id_usuariolancamento) AS
                     usu_lancamento
         OUTER apply (SELECT Count(cha.id_avaliacaoagendamento) AS id_aval
                      FROM   tb_avaliacaoagendamento AS cha
                             INNER JOIN tb_avalagendamentoref AS cagr ON cagr.id_avaliacaoagendamento = cha.id_avaliacaoagendamento AND cagr.id_avaliacaoconjuntoreferencia = acrf.id_avaliacaoconjuntoreferencia
                             LEFT JOIN tb_avaliacaoaplicacao AS ch ON ch.id_avaliacaoaplicacao = cha.id_avaliacaoaplicacao
                      WHERE  cha.id_matricula = mt.id_matricula
                             AND cha.id_tipodeavaliacao = aa.id_tipodeavaliacao
                             AND cha.id_situacao = 68) AS tb_chamada
         OUTER apply (SELECT Min(id_avaliacaoagendamento) AS id_primeirachamada,
                             Max(id_avaliacaoagendamento) AS id_segundachamada
                      FROM   tb_avaliacaoagendamento
                      WHERE  id_matricula = aa.id_matricula
                             AND id_situacao = 68
                             AND id_tipodeavaliacao = aa.id_tipodeavaliacao) AS
                     primeira
         OUTER apply (SELECT nu_presenca AS nu_primeirapresenca
                      FROM   tb_avaliacaoagendamento
                      WHERE  id_avaliacaoagendamento =
                             (SELECT Min(id_avaliacaoagendamento)
                              FROM   tb_avaliacaoagendamento
                              WHERE  id_matricula = aa.id_matricula
                                     AND id_situacao = 68
                                     AND id_tipodeavaliacao = aa.id_tipodeavaliacao
                                     )) AS primeirapresenca
         OUTER apply (SELECT nu_presenca AS nu_ultimapresenca
               FROM   tb_avaliacaoagendamento
                      WHERE  id_avaliacaoagendamento =
                             (SELECT Max(id_avaliacaoagendamento)
                              FROM   tb_avaliacaoagendamento
                              WHERE  id_matricula = aa.id_matricula
                                     AND id_situacao = 68
                                     AND id_tipodeavaliacao =
                                         aa.id_tipodeavaliacao
                                     )) AS ultima
         OUTER apply (SELECT Count(id_avaliacaoagendamento) AS
                             id_provasrealizadas
                      FROM   tb_avaliacaoagendamento
                      WHERE  id_matricula = aa.id_matricula
                             AND nu_presenca = 1
                             AND bl_ativo = 0
                             AND id_situacao = 68) AS provasrealizadas
         OUTER apply (SELECT ( CASE
                                 WHEN st_nota IS NULL THEN 0
                                 ELSE 1
                               END ) AS bl_temnotafinal
                      FROM   tb_avaliacaoaluno
                      WHERE  id_matricula = mt.id_matricula
                             AND id_avaliacao = aa.id_avaliacao
                             AND id_situacao = 86) AS temnotafinal
    WHERE aa.id_situacao <> 70
      AND ac.id_tipoprova = 2
GO


ALTER VIEW [dbo].[vw_conferenciadisciplina] AS 
SELECT DISTINCT
    tt.id_turma, 
    tp.id_projetopedagogico, 
    tp1.id_entidade, 
    td.id_disciplina, 
    tp.st_projetopedagogico, 
    td.st_disciplina, 
    ts.st_saladeaula, 
    ts.dt_inicioinscricao, 
    ts.dt_fiminscricao, 
    ts.dt_abertura, 
    ts.dt_encerramento, 
    ts.st_professor, 
    ts.bl_conteudo, 
    ts.st_sistema,
    di.st_codsistema,
    di.st_salareferencia,
    (CASE WHEN si.bl_conteudo = 1 THEN 'SIM' ELSE 'NÃO' END) AS st_processada
FROM dbo.tb_projetopedagogico AS tp
INNER JOIN tb_projetoentidade tp1 ON tp.id_projetopedagogico = tp1.id_projetopedagogico
INNER JOIN dbo.tb_modulo AS tm
    ON tp.id_projetopedagogico = tm.id_projetopedagogico AND tm.bl_ativo = 1
INNER JOIN dbo.tb_modulodisciplina AS tm1
    ON tm.id_modulo = tm1.id_modulo AND tm1.bl_ativo = 1
INNER JOIN dbo.tb_turmaprojeto AS tt
    ON tp.id_projetopedagogico = tt.id_projetopedagogico AND tt.bl_ativo = 1
INNER JOIN dbo.tb_turma AS tt1
    ON tt.id_turma = tt1.id_turma
INNER JOIN dbo.tb_disciplina AS td
    ON tm1.id_disciplina = td.id_disciplina
LEFT JOIN vw_saladisciplinaentidade as ts ON CAST(tt1.dt_inicio AS date) BETWEEN
    CAST(ts.dt_inicioinscricao AS date) 
        AND CAST(ts.dt_fiminscricao AS date) 
        AND td.id_disciplina = ts.id_disciplina 
        AND tp1.id_projetopedagogico = ts.id_projetopedagogico 
        AND tp1.id_entidade = ts.id_entidade
LEFT JOIN dbo.tb_saladeaulaintegracao AS si ON si.id_saladeaula = ts.id_saladeaula
LEFT JOIN dbo.tb_disciplinaintegracao AS di ON di.id_disciplinaintegracao = si.id_disciplinaintegracao
GO


ALTER VIEW [dbo].[vw_disciplinaintegracaomoodle] AS
	SELECT  de.id_disciplinaintegracao
			,de.id_disciplina
			,de.id_sistema
			,de.id_usuariocadastro
			,de.st_codsistema
			,de.dt_cadastro
			,de.id_entidade
			,de.bl_ativo 
			,e.st_nomeentidade 
			,de.st_salareferencia
			from tb_disciplinaintegracao de
			left join tb_entidade e on e.id_entidade = de.id_entidade
			where id_sistema = 6
GO

ALTER VIEW [dbo].[vw_gridcertificado] AS
SELECT DISTINCT
  mt.id_matricula,
  mt.id_projetopedagogico,
  pp.st_tituloexibicao,
  CAST(mtd.id_disciplina AS VARCHAR(200)) AS id_disciplina,
  dc.st_tituloexibicao AS st_disciplina,
  mtd.nu_aprovafinal AS nu_aprovafinal,
  pp.nu_notamaxima,
  mtd.dt_conclusao AS dt_conclusaodisciplina,
  dc.nu_cargahoraria AS nu_cargahoraria,
  mtd.id_evolucao,
  ev.st_evolucao,
  mtd.id_situacao,
  CASE
  WHEN CAST(mtd.nu_aprovafinal AS INT) < 70 THEN ''
  WHEN CAST(mtd.nu_aprovafinal AS INT) < 80 THEN 'Bom'
  WHEN CAST(mtd.nu_aprovafinal AS INT) < 90 THEN 'Ótimo'
  WHEN CAST(mtd.nu_aprovafinal AS INT) <= 100 THEN 'Excelente'
  ELSE ''
  END AS st_conceito,

  st_titularcertificacao = (CASE
    -- Se for TCC, buscar o titular cadastrado em Sala de Aula
    WHEN dc.id_tipodisciplina = 2 THEN op_tcc.st_nomecompleto
    -- Caso contrário, buscar o titular cadastrado em Titular de Certificação
    ELSE op.st_nomecompleto
  END),

  op.st_titulacao,
  mt.id_entidadematricula,
  mt.dt_concluinte,
  dc.id_tipodisciplina,
  --DATEADD(DAY,ISNULL(sa.nu_diasextensao,0), sa.dt_encerramento) as dt_encerramentoextensao
  NULL AS dt_encerramentoextensao

FROM tb_matricula AS mt
JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_matriculadisciplina AS mtd ON mtd.id_matricula = mt.id_matricula AND mtd.bl_obrigatorio = 1
JOIN tb_matricula AS mt2 ON mt2.id_matricula = mtd.id_matriculaoriginal
JOIN tb_areaprojetopedagogico as ar on ar.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_projetopedagogico AS ppcompl ON ppcompl.id_projetopedagogico = mt2.id_projetopedagogico

JOIN tb_alocacao AS al ON al.id_matriculadisciplina = mtd.id_matriculadisciplina and al.bl_ativo = 1
JOIN tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula

LEFT JOIN tb_aproveitamentomatriculadisciplina AS apm ON apm.id_matricula = mtd.id_matricula AND apm.id_disciplina = mtd.id_disciplina
LEFT JOIN tb_aproveitamentodisciplina AS apd ON apd.id_aproveitamentodisciplina = apm.id_aproveitamentodisciplina
JOIN tb_disciplina AS dc ON dc.id_disciplina = mtd.id_disciplina
JOIN tb_entidade AS ent ON ent.id_entidade = mt.id_entidadematriz
JOIN tb_evolucao AS ev ON ev.id_evolucao = mtd.id_evolucao

OUTER APPLY (
  SELECT
    us.st_nomecompleto,
    tit.st_titulacao
  FROM tb_usuarioperfilentidadereferencia AS uper
  JOIN tb_perfil AS pf ON pf.id_entidade = mt.id_entidadematricula AND pf.id_perfilpedagogico = 9 AND uper.id_perfil = pf.id_perfil
  LEFT JOIN tb_usuario AS us ON us.id_usuario = uper.id_usuario
  LEFT JOIN tb_titulacao tit ON uper.id_titulacao = tit.id_titulacao
  WHERE
    uper.bl_titular = 1
    AND uper.bl_ativo = 1
    AND uper.id_usuario IS NOT NULL
    AND uper.id_projetopedagogico = mt2.id_projetopedagogico
    AND uper.id_disciplina = mtd.id_disciplina
    AND (
      ar.id_areaconhecimento = uper.id_areaconhecimento OR (
        mtd.id_matricula != mtd.id_matriculaoriginal
        AND ppcompl.bl_disciplinacomplementar = 1
      )
    )
    AND (
      (mt.dt_concluinte BETWEEN uper.dt_inicio AND uper.dt_fim)
      OR (mt.dt_concluinte >= uper.dt_inicio AND uper.dt_fim IS NULL)
    )
) AS op

OUTER APPLY (
  SELECT
    us.st_nomecompleto,
    tit.st_titulacao
  FROM tb_usuarioperfilentidadereferencia AS uper
  LEFT JOIN tb_usuario AS us ON us.id_usuario = uper.id_usuario
  LEFT JOIN tb_titulacao tit ON uper.id_titulacao = tit.id_titulacao
  WHERE
    uper.bl_titular = 1
    AND uper.bl_ativo = 1
    AND uper.id_usuario IS NOT NULL
    AND uper.id_saladeaula = sa.id_saladeaula
) AS op_tcc

--WHERE
--sa.dt_encerramento IS NOT NULL
GO

/**
CREATE vw_importasalasmoodle
-- Busca as salas de aula a serem cadastradas no moodle de acordo com a sua sala de referencia
--@link da documentação http://confluence.unyleya.com.br/pages/viewpage.action?pageId=10883062

	Esse processo deve consultar:
		Salas integradas ao Moodle
		Que não tiveram seu conteúdo importado
		Que tenham uma sala de referência selecionada
		Que a data de abertura da sala no G2 seja menor ou igual a 50 dias

	Mais uma observação: o processo de importação não pode ser executado em uma sala com data de abertura passada. 
	Isso é para evitar que o ROBO faça a importação de salas que já estão montadas.
**/

CREATE VIEW [dbo].[vw_importasalasmoodle] AS

SELECT 
  calculo.nu_dias
, sa.id_saladeaula -- id da sala de destino 
, sa.st_saladeaula -- nome da sala de destino 
, di.id_disciplinaintegracao
, di.st_codsistema -- id_moodlesaladereferencia
, si.st_codsistemacurso -- id_moodlesaladedestino
, di.st_salareferencia -- nome sala de referencia
, di.id_entidade
, si.id_saladeaulaintegracao

 FROM tb_saladeaulaintegracao AS si
	 JOIN tb_saladeaula AS sa ON sa.id_saladeaula = si.id_saladeaula -- sala integrada
	 AND si.bl_conteudo = 0 -- não teve o conteúdo importado
			AND si.id_disciplinaintegracao IS NOT NULL -- tenho uma sala de referência selecionada

	 JOIN tb_disciplinaintegracao AS di ON di.id_disciplinaintegracao = si.id_disciplinaintegracao
			OUTER APPLY (SELECT DATEDIFF(day, GETDATE(), sa.dt_abertura) AS nu_dias ) as calculo

		WHERE calculo.nu_dias <= 50 -- data de abertura da sala no G2 seja Menor ou igual a 50 dias
		AND sa.dt_abertura >= GETDATE() -- processo de importação não pode ser executado em uma sala com data de abertura passada


GO

CREATE VIEW [dbo].[vw_logmatricula] AS 
SELECT  mt.id_matricula ,
        us.st_nomecompleto ,
        mt.id_usuario ,
        lg.id_funcionalidade ,
        sl.st_saladeaula ,
        fc.st_funcionalidade ,
        pf.st_nomeperfil ,
        lg.dt_cadastro, mt.bl_institucional
FROM    tb_matricula AS mt
        JOIN dbo.tb_logacesso AS lg ON lg.id_usuario = mt.id_usuario
                                       AND lg.id_entidade = mt.id_entidadeatendimento
        JOIN dbo.tb_funcionalidade AS fc ON fc.id_funcionalidade = lg.id_funcionalidade
        LEFT JOIN dbo.tb_saladeaula AS sl ON sl.id_saladeaula = lg.id_saladeaula
        LEFT JOIN dbo.tb_perfil AS pf ON pf.id_perfil = lg.id_perfil
        JOIN tb_usuario AS us ON us.id_usuario = lg.id_usuario
GO




ALTER VIEW [dbo].[vw_ocorrencia]
AS
SELECT DISTINCT
  oc.id_ocorrencia,
  uso.st_cpf AS st_cpf,
  uso.st_nomecompleto AS st_nomeinteressado,
  tc1.st_email,
  uso.st_senha, uso.st_login,
  (CAST(tc3.nu_ddd AS VARCHAR) + '-' + CAST(tc3.nu_telefone AS VARCHAR)) AS st_telefone,
  oc.id_matricula,
  em.st_urlportal,
  oc.id_evolucao,
  oc.id_situacao,
  oc.id_usuariointeressado,
  oc.id_categoriaocorrencia,
  oc.id_assuntoco,
  oc.id_saladeaula,
  oc.st_titulo,
  oc.dt_cadastro,
  oc.st_ocorrencia,
  oc.id_entidade,
  dbo.tb_situacao.st_situacao,
  dbo.tb_evolucao.st_evolucao,
  dbo.tb_assuntoco.st_assuntoco,
  dbo.tb_assuntoco.id_tipoocorrencia,
  dbo.tb_categoriaocorrencia.st_categoriaocorrencia,
  (CAST(CONVERT(VARCHAR, oc.dt_ultimotramite, 103) + ' as ' + CONVERT(VARCHAR, oc.dt_ultimotramite, 108) AS VARCHAR(MAX))) AS st_ultimotramite,
  oc.st_tramite,
  oc.dt_ultimotramite AS dt_ultimotramite,
    (case WHEN oc.dt_ultimotramite is null then oc.dt_cadastro else oc.dt_ultimotramite end) AS dt_ultimaacao,
    oresp.id_usuario AS id_usuarioresponsavel,
  st_nomeresponsavel =
                      CASE
WHEN usr.id_usuario IS NULL THEN 'Năo distribuído'
ELSE usr.st_nomecompleto
                      END,
  GETDATE() - 1 AS dt_atendimento,
  asp.st_assuntoco AS st_assuntocopai
  ,pp.st_projetopedagogico
  ,sa.st_saladeaula
  , mt.id_evolucao AS id_evolucaomatricula
  , oc.id_ocorrenciaoriginal,
  oc.st_codissue
FROM dbo.tb_ocorrencia AS oc
JOIN tb_usuario AS uso
  ON uso.id_usuario = oc.id_usuariointeressado
INNER JOIN dbo.tb_situacao
  ON dbo.tb_situacao.id_situacao = oc.id_situacao
INNER JOIN dbo.tb_evolucao
  ON dbo.tb_evolucao.id_evolucao = oc.id_evolucao
INNER JOIN dbo.tb_categoriaocorrencia
  ON dbo.tb_categoriaocorrencia.id_categoriaocorrencia = oc.id_categoriaocorrencia
INNER JOIN dbo.tb_assuntoco
  ON oc.id_assuntoco = dbo.tb_assuntoco.id_assuntoco
LEFT JOIN dbo.tb_assuntoco AS asp
  ON asp.id_assuntoco = dbo.tb_assuntoco.id_assuntocopai
LEFT JOIN dbo.tb_ocorrenciaresponsavel AS oresp
  ON oresp.id_ocorrencia = oc.id_ocorrencia
  AND oresp.bl_ativo = 1
LEFT JOIN dbo.tb_usuario AS usr
  ON usr.id_usuario = oresp.id_usuario
LEFT JOIN tb_contatosemailpessoaperfil tc
  ON uso.id_usuario = tc.id_usuario
  AND tc.bl_padrao = 1
  AND tc.bl_ativo = 1
  AND oc.id_entidade = tc.id_entidade
LEFT JOIN tb_contatosemail tc1
  ON tc.id_email = tc1.id_email
LEFT JOIN tb_contatostelefonepessoa tc2
  ON uso.id_usuario = tc2.id_usuario
  AND tc2.bl_padrao = 1
  AND oc.id_entidade = tc2.id_entidade
LEFT JOIN tb_contatostelefone tc3
  ON tc2.id_telefone = tc3.id_telefone
--alteraçőes para impressăo
LEFT JOIN dbo.tb_matricula AS mt
  ON mt.id_matricula = oc.id_matricula

LEFT JOIN dbo.tb_entidade as em 
  ON em.id_entidade = mt.id_entidadematricula

LEFT JOIN dbo.tb_projetopedagogico AS pp
  ON pp.id_projetopedagogico = mt.id_projetopedagogico
LEFT JOIN dbo.tb_saladeaula AS sa
  ON sa.id_saladeaula = oc.id_saladeaula

GO


ALTER VIEW [dbo].[vw_perfilentidadefuncionalidade]
AS

SELECT
  pe.id_entidade,
  p.id_perfil,
  p.id_perfilpai,
  pf.id_funcionalidade,
  f.st_urlicone,
  p.st_nomeperfil,
  f.st_funcionalidade,
  f.bl_pesquisa,
  ef.st_label,
  f.id_funcionalidadepai,
  f.bl_ativo,
  f.st_classeflex,
  tf.st_tipofuncionalidade,
  f.id_situacao,
  f.id_sistema,
  CASE WHEN ef.nu_ordem IS NOT NULL THEN ef.nu_ordem ELSE f.nu_ordem END AS nu_ordem,
  f.st_classeflexbotao,
  ef.bl_obrigatorio,
  ef.bl_visivel,
  f.bl_lista,
  f.st_ajuda,
  f.id_tipofuncionalidade,
  f.st_target,
  f.st_urlcaminho,
  f.st_urlcaminhoedicao,
  f.bl_visivel AS bl_funcionalidadevisivel,
  f.bl_relatorio,
  f.bl_delete

FROM tb_perfil AS p
  JOIN dbo.tb_perfilentidade AS pe ON pe.id_perfil = p.id_perfil
  JOIN tb_perfilfuncionalidade AS pf ON p.id_perfil = pf.id_perfil AND  pf.bl_ativo = 1
  JOIN tb_entidadefuncionalidade AS ef ON  pf.id_funcionalidade = ef.id_funcionalidade AND ef.id_entidade = pe.id_entidade AND ef.bl_visivel = 1 AND ef.bl_ativo = 1
  JOIN tb_funcionalidade AS f ON f.id_funcionalidade = ef.id_funcionalidade
                                 AND f.id_funcionalidade = pf.id_funcionalidade  --AND f.id_situacao = 123
  JOIN  tb_tipofuncionalidade tf ON f.id_tipofuncionalidade = tf.id_tipofuncionalidade
WHERE
  f.bl_ativo = 1
GO


ALTER view [dbo].[vw_projetoentidade] as
select DISTINCT 
  p.id_projetopedagogico, 
  p.id_situacao, 
  p.st_descricao, 
  p.bl_ativo, 
  p.st_projetopedagogico, 
  p.id_entidadecadastro, 
  p.st_tituloexibicao,
  p.bl_disciplinacomplementar,
  e.id_entidade, 
  e.st_nomeentidade, 
  e.st_razaosocial, 
  vwperf.st_nomecompleto as st_coordenadortitular
from tb_projetopedagogico as p
JOIN tb_projetoentidade as pe on pe.id_projetopedagogico = p.id_projetopedagogico
JOIN tb_entidade as e on e.id_entidade = pe.id_entidade
LEFT JOIN vw_usuarioperfilentidadereferencia AS vwperf ON vwperf.id_projetopedagogico = p.id_projetopedagogico 
AND vwperf.id_perfilpedagogico = 2 --AND vwperf.id_entidade = e.id_entidade
 AND vwperf.bl_titular= 1
GO


ALTER VIEW [rel].[vw_alunosaptosagendamento] AS (

SELECT
DISTINCT
mt.id_turma,
mt.id_evolucao,
mt.st_evolucao,
mt.id_projetopedagogico,
mt.dt_inicio,
mt.st_nomecompleto,
mt.st_cpf,
mt.id_matricula,
mt.id_contrato,
mt.st_email,
mt.st_telefone,
mt.sg_uf,
mt.st_situacao,
mt.st_areaconhecimento,
mt.st_projetopedagogico,
mt.st_turma,
mt.dt_inicioturma,
mt.id_situacaoagendamento,
mt.id_entidadeatendimento,
mt.id_situacao,
ava_rec.id_tipodeavaliacao,

CASE
    WHEN ava_rec.id_tipodeavaliacao > 0 THEN 'RECUPERAÇÃO'
    ELSE 'PROVA FINAL'
END AS 'st_avaliacao',

CASE
  WHEN (ISNULL(ec.st_valor, 0) = 1) THEN CAST(md.dt_aptoagendamento AS DATE)
  ELSE CAST(mt.dt_aptoagendamento AS DATE)
END AS dt_aptoagendamento
--,pp.bl_disciplinacomplementar

FROM rel.vw_matricula AS mt
  
JOIN tb_projetopedagogico pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
  AND pp.bl_disciplinacomplementar = 0

LEFT JOIN dbo.vw_avaliacaoagendamento AS avag ON avag.id_matricula = mt.id_matricula AND avag.id_avaliacaoagendamento IN (SELECT TOP 1 id_avaliacaoagendamento FROM dbo.vw_avaliacaoagendamento AS ava WHERE ava.id_matricula = avag.id_matricula ORDER BY id_tipodeavaliacao DESC, id_chamada DESC)

OUTER APPLY (
    SELECT
    CASE
        WHEN nu_presenca IS NULL THEN id_tipodeavaliacao
        ELSE id_tipodeavaliacao + 1
    END AS id_tipodeavaliacao
) AS ava_rec

-- Esquema configuração (se for prova por disciplina)
LEFT JOIN vw_entidadeesquemaconfiguracao AS ec ON ec.id_entidade = mt.id_entidadeatendimento AND ec.id_itemconfiguracao = 24 AND ec.st_valor = 1

-- Se for prova por disciplina, então verificar se em alguma disciplina, o aluno está apto
OUTER APPLY (
  SELECT TOP 1
    dt_aptoagendamento,
    id_situacaoagendamento
  FROM
    tb_matriculadisciplina AS md2
  WHERE
    md2.id_matricula = mt.id_matricula
    AND md2.id_situacaoagendamento IN (120,177)
  ORDER BY dt_aptoagendamento ASC
) AS md

WHERE
  (ISNULL(ec.st_valor, 0) = 0 AND mt.id_situacaoagendamento = 120)
  OR
  (ISNULL(ec.st_valor, 0) = 1 AND md.id_situacaoagendamento IN (120,177))

)
GO

ALTER VIEW [rel].[vw_cronogramasalas] as
SELECT DISTINCT
    sa.id_saladeaula ,
    sa.st_saladeaula ,
    sa.dt_inicioinscricao ,
    sa.dt_fiminscricao ,
    sa.dt_abertura ,
    sa.dt_encerramento ,
    cast(dt_atualiza as date) as dt_atualiza,
    st_disciplina ,
    dc.st_tituloexibicao AS st_tituloexibicaodisciplina ,
    st_projetopedagogico ,
    aps.id_projetopedagogico,
    uss.st_nomecompleto AS st_nomeprofessor ,
    usp.id_usuario AS id_coordenador,
    usp.st_nomecompleto AS st_nomecoordenador,
    si.st_codsistemacurso AS st_codava,
    sa.id_entidade,
		sa.id_categoriasala,
    pl.st_periodoletivo,
    pp.bl_disciplinacomplementar,
		CAST(sa.dt_cadastro as date) as dt_cadastro,
    di.st_codsistema,
    di.st_salareferencia,
    (CASE WHEN si.bl_conteudo = 1 THEN 'SIM' ELSE 'NÃO' END) AS st_processada,
		CAST(COUNT(ta.id_matriculadisciplina) AS varchar) AS st_alunos
FROM dbo.tb_saladeaula AS sa
JOIN dbo.tb_areaprojetosala AS aps ON aps.id_saladeaula = sa.id_saladeaula
JOIN dbo.tb_disciplinasaladeaula AS da ON da.id_saladeaula = sa.id_saladeaula
JOIN tb_disciplina AS dc ON dc.id_disciplina = da.id_disciplina
JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = aps.id_projetopedagogico
LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS ups ON ups.id_saladeaula = aps.id_saladeaula AND ups.bl_titular = 1 and ups.bl_ativo=1
LEFT JOIN dbo.tb_usuario AS uss ON uss.id_usuario = ups.id_usuario
LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS upp ON upp.id_projetopedagogico = pp .id_projetopedagogico AND upp.bl_titular = 1 and upp.bl_ativo=1 and upp.id_disciplina is null
LEFT JOIN dbo.tb_perfil AS tbp ON tbp.id_perfil = upp.id_perfil AND tbp.id_perfilpedagogico = 2
LEFT JOIN dbo.tb_usuario AS usp ON usp.id_usuario = upp.id_usuario
LEFT JOIN dbo.tb_saladeaulaintegracao AS si ON si.id_saladeaula = sa.id_saladeaula
LEFT JOIN dbo.tb_disciplinaintegracao AS di ON di.id_disciplinaintegracao = si.id_disciplinaintegracao
LEFT JOIN dbo.tb_alocacao AS ta ON ta.id_saladeaula = sa.id_saladeaula AND ta.bl_ativo = 1
LEFT JOIN tb_periodoletivo as pl on pl.id_periodoletivo = sa.id_periodoletivo
WHERE sa.bl_ativa = 1 AND sa.id_situacao != 74
GROUP by
    sa.id_saladeaula,
    st_saladeaula ,
    sa.dt_inicioinscricao ,
    sa.dt_fiminscricao ,
    sa.dt_abertura ,
    sa.dt_encerramento ,
    dt_atualiza,
    st_disciplina ,
    dc.st_tituloexibicao ,
    st_projetopedagogico ,
    aps.id_projetopedagogico,
    uss.st_nomecompleto ,
    usp.id_usuario ,
    usp.st_nomecompleto,
    si.st_codsistemacurso,
    sa.id_entidade,
		sa.dt_cadastro,
    di.st_codsistema,
    di.st_salareferencia,
    si.bl_conteudo,
		sa.id_categoriasala,
    pl.st_periodoletivo,
    pp.bl_disciplinacomplementar
GO


ALTER VIEW [rel].[vw_matricula]
AS
    SELECT DISTINCT
            mt.id_matricula ,
            mt.dt_inicio ,
            ct.id_contrato ,
            us.st_nomecompleto ,
            us.st_cpf ,
            p.st_email ,
            CAST(p.nu_ddd AS VARCHAR) + '-' + CAST(p.nu_telefone AS VARCHAR) AS st_telefone ,
            ac.id_areaconhecimento ,
            ac.st_areaconhecimento ,
            pp.id_projetopedagogico ,
            pp.st_projetopedagogico ,
            st.id_situacao ,
            st.st_situacao ,
            ev.id_evolucao ,
            ev.st_evolucao ,
            mt.id_situacaoagendamento,
            mt.dt_aptoagendamento,
            usr.st_nomecompleto AS st_representante ,
            usa.st_nomecompleto AS st_atendente ,
            vp.nu_valorliquido AS nu_valor ,
            mt.id_entidadeatendimento ,
            p.sg_uf ,
            tm.st_turma, tm.id_turma,
            tm.dt_inicio AS dt_inicioturma,
            tm.dt_fim AS dt_fimturma,
            CAST(mt.dt_cadastro AS DATE) AS dt_cadastro,
            CAST(mt.dt_concluinte AS DATE) AS dt_concluinte,
            CAST (cmt.dt_cadastro AS DATE) AS dt_certificacao,
                        vp.nu_valorliquido AS nu_valorliquido ,
            vp.nu_valorbruto AS nu_valorproduto ,
            campanha.nu_valordesconto AS nu_valordesconto
    FROM    dbo.tb_matricula mt
            JOIN dbo.tb_usuario AS us ON us.id_usuario = mt.id_usuario
            INNER JOIN vw_pessoa p ON p.id_entidade = mt.id_entidadeatendimento
                                      AND p.id_usuario = mt.id_usuario
            JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
            JOIN dbo.tb_areaprojetopedagogico AS ap ON ap.id_projetopedagogico = pp.id_projetopedagogico
            OUTER APPLY ( SELECT TOP 1
                                    id_areaconhecimento ,
                                    st_areaconhecimento
                          FROM      dbo.tb_areaconhecimento
                          WHERE     id_areaconhecimento = ap.id_areaconhecimento
                                    AND id_tipoareaconhecimento = 1
                        ) AS ac
           LEFT JOIN dbo.tb_contratomatricula AS cm ON cm.id_matricula = mt.id_matricula
                                                   AND cm.bl_ativo = 1
            LEFT JOIN dbo.tb_contrato AS ct ON ct.id_contrato = cm.id_contrato
                                          AND ct.bl_ativo = 1
            LEFT JOIN dbo.tb_venda AS vd ON ct.id_venda = vd.id_venda
            LEFT JOIN dbo.tb_vendaproduto AS vp ON vp.id_venda = vd.id_venda
            LEFT JOIN dbo.tb_atendentevenda AS av ON av.id_venda = vd.id_venda
            LEFT JOIN dbo.tb_usuario AS usa ON usa.id_usuario = av.id_usuario
            LEFT JOIN dbo.tb_vendaenvolvido AS ve ON ve.id_venda = vd.id_venda
                                                     AND ve.id_funcao = 1
            LEFT JOIN dbo.tb_usuario AS usr ON usr.id_usuario = ve.id_usuario
            JOIN dbo.tb_situacao AS st ON st.id_situacao = mt.id_situacao
            JOIN dbo.tb_evolucao AS ev ON ev.id_evolucao = mt.id_evolucao
            LEFT JOIN dbo.tb_turma AS tm ON tm.id_turma = mt.id_turma
            LEFT JOIN tb_matriculacertificacao AS cmt ON cmt.id_matricula=mt.id_matricula
            LEFT JOIN dbo.tb_campanhacomercial as campanha ON campanha.id_campanhacomercial = vp.id_campanhacomercial
    WHERE   mt.bl_ativo = 1
           AND mt.bl_institucional = 0
GO






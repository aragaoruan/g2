SET IDENTITY_INSERT dbo.tb_tipoproduto ON
insert into dbo.tb_tipoproduto (id_tipoproduto, st_tabelarelacao, st_tipoproduto) values (9, 'tb_simulado', 'Simulado')
SET IDENTITY_INSERT dbo.tb_tipoproduto off

ALTER TABLE G2H_UNY.dbo.tb_produto ADD st_codigoavaliacao VARCHAR(255) NULL;

SET IDENTITY_INSERT dbo.tb_funcionalidade on
INSERT INTO dbo.tb_funcionalidade
  (
    id_funcionalidade,
    st_funcionalidade,
    id_funcionalidadepai,
    bl_ativo,
    id_situacao,
    st_classeflex,
    st_urlicone,
    id_tipofuncionalidade,
    nu_ordem,
    st_classeflexbotao,
    bl_pesquisa,
    bl_lista,
    id_sistema,
    st_ajuda,
    st_target,
    st_urlcaminho,
    bl_visivel,
    bl_relatorio,
    st_urlcaminhoedicao,
    bl_delete
  )
  VALUES (
    738,
    'Folha de Pagamento - Turmas Regulares', -- st_funcionalidade - varchar(100)
    488, -- id_funcionalidadepai - int
    1, -- bl_ativo - bit
    123, -- id_situacao - int
    'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa', -- st_classeflex - varchar(300)
    '/img/ico/door--arrow.png', -- st_urlicone - varchar(1000)
    3, -- id_tipofuncionalidade - int
    NULL, -- nu_ordem - int
    NULL, -- st_classeflexbotao - varchar(300)
    0, -- bl_pesquisa - bit
    0, -- bl_lista - bit
    1, -- id_sistema - int
    NULL, -- st_ajuda - varchar(max)
    NULL, -- st_target - varchar(100)
    '/relatorio-folha-pagamento', -- st_urlcaminho - varchar(50)
    1, -- bl_visivel - int
    0, -- bl_relatorio - bit
    NULL, -- st_urlcaminhoedicao - varchar(50)
    0  -- bl_delete - bit
  );
SET IDENTITY_INSERT dbo.tb_funcionalidade off

UPDATE tb_textovariaveis
SET st_mascara = 'retornaDataPorExtenso'
WHERE st_textovariaveis = '#dt_matricula#';alter table tb_holdingfiliada add bl_emiterelatorioholding bit NOT NULL default 0;--DESATIVAR IDENTITY TABELA FIN-16826
SET IDENTITY_INSERT tb_permissao ON;

INSERT INTO tb_permissao (id_permissao, st_nomepermissao, st_descricao, bl_ativo) VALUES (31, 'Aba Financeiro', 'Permite o usu�rio acesso a aba financeiro de Pessoa Jur�dica', 1) ;
INSERT tb_permissaofuncionalidade VALUES (286, 31);

--ATIVAR IDENTITY TABELA
SET IDENTITY_INSERT tb_permissao OFF -- GRA-33
--SET IDENTITY_INSERT tb_modelovenda ON;
INSERT INTO tb_modelovenda(id_modelovenda, st_modelovenda) VALUES(3, 'Crédito');
--SET IDENTITY_INSERT tb_modelovenda OFF;

BEGIN TRAN

ALTER TABLE tb_saladeaula
ADD bl_ofertaexcepcional BIT NOT NULL
DEFAULT 0


COMMIT

--sp_help tb_saladeaula

SET IDENTITY_INSERT dbo.tb_mensagempadrao on
insert tb_mensagempadrao (id_mensagempadrao,id_tipoenvio, st_mensagempadrao, st_default, id_textocategoria)
values(39,3,'Email de Confirmação da Renovação', null, null);

INSERT INTO tb_mensagempadrao
(id_mensagempadrao, id_tipoenvio, st_mensagempadrao, st_default, id_textocategoria)
VALUES 
( 37, 3, 'Renovação de matrícula' , 'ALTERAR: Renovaçãoo de matrícula', 1)
SET IDENTITY_INSERT dbo.tb_mensagempadrao off
  
 /**
    ADD campo de data para limite de renova��o da venda do aluno na gradua��o
**/
ALTER TABLE tb_venda
 ADD dt_limiterenovacao DATETIME NULL


 /**
 Adiciona campo que marca se a venda � uma renova��o ou n�o
 **/
 ALTER TABLE tb_venda 
 ADD bl_renovacao BIT NOT NULL DEFAULT 0


 
 
 
/**
Variaveis para envio de renovacao de matricula 
**/

SET IDENTITY_INSERT dbo.tb_textovariaveis on

INSERT INTO tb_textovariaveis
(id_textovariaveis, id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
VALUES
--Grade de disciplinas
(407, 1 , 'id_matricula' , '#grade_disciplina_renovacao#' , 'gerarGridDisciplinasRenovacao', 2),
-- Data de limite de renovacao
(408, 1 , 'st_limiterenovacao' , '#dt_limite_renovacao#' , NULL, 1),
-- Valor da mensalidade
(409, 1 , 'nu_mensalidade' , '#valor_mensalidade#' , 'formataValor', 2),
-- Valor da mensalidade com pontualidade
(410, 1 , 'nu_mensalidadepontualidade' , '#valor_mensalidade_pontualidade#' , 'formataValor', 2),
-- Link para o portal
(411, 1 , 'st_link_grade_portal' , '#link_grade_disciplina_portal#' , 'gerarUrlAcessoGradeDisciplina', 2)

SET IDENTITY_INSERT dbo.tb_textovariaveis off


-- GRA-53

SET IDENTITY_INSERT tb_modelocarteirinha ON;
INSERT INTO tb_modelocarteirinha (id_modelocarteirinha, id_situacao, st_modelocarteirinha, bl_ativo) VALUES (3, 140, 'Graduação', 1);
SET IDENTITY_INSERT tb_modelocarteirinha OFF;

SET IDENTITY_INSERT tb_modelocarteirinhaentidade ON;
INSERT INTO tb_modelocarteirinhaentidade(id_modelocarteirinhaentidade, id_modelocarteirinha, id_entidade) VALUES
(14, 3, 353),
(15, 3, 354),
(16, 3, 400),
(17, 3, 401),
(18, 3, 402);
SET IDENTITY_INSERT tb_modelocarteirinhaentidade OFF;

SET IDENTITY_INSERT dbo.tb_funcionalidade ON
INSERT INTO tb_funcionalidade (
	id_funcionalidade,
	st_funcionalidade,
	id_funcionalidadepai,
	bl_ativo,
	id_situacao,
	st_classeflex,
	st_urlicone,
	id_tipofuncionalidade,
	nu_ordem,
	st_classeflexbotao,
	bl_pesquisa,
	bl_lista,
	id_sistema,
	st_ajuda,
	st_target,
	st_urlcaminho,
	bl_visivel,
	bl_relatorio,
	st_urlcaminhoedicao,
	bl_delete)
VALUES (
	748,
	'Carta de Crédito',
	194, 1, 123,
	'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa',
	'/img/ico/bank.png', 3, NULL,
	'br.com.ead1.gestor2.view.conteudo.financeiro.cadastrar.CadastrarCartaCredito', 0,
	0, 1, NULL, NULL, '/carta-credito', 1, 0, null, 0)
set IDENTITY_INSERT dbo.tb_funcionalidade OFF

-------------------------------------------------------------------------------------

ALTER TABLE tb_holding ADD bl_compartilharcarta BIT DEFAULT 0

-------------------------------------------------------------------------------------

CREATE TRIGGER tg_datas_sala_graduacao
ON tb_periodoletivo
FOR UPDATE
AS
BEGIN

    DECLARE
        @ID_PERIODOLETIVO INT,
        @DT_INICIOINSCRICAO DATETIME,
        @DT_FIMINSCRICAO DATETIME,
        @DT_ABERTURA DATETIME,
        @DT_ENCERRAMENTO DATETIME

    SELECT
        @ID_PERIODOLETIVO = id_periodoletivo,
        @DT_INICIOINSCRICAO = dt_inicioinscricao,
        @DT_FIMINSCRICAO = dt_fiminscricao,
        @DT_ABERTURA = dt_abertura,
        @DT_ENCERRAMENTO = dt_encerramento
    FROM INSERTED

    UPDATE tb_saladeaula

    SET
        dt_inicioinscricao = @DT_INICIOINSCRICAO,
        dt_fiminscricao = @DT_FIMINSCRICAO,
        dt_abertura = @DT_ABERTURA,
        dt_encerramento = @DT_ENCERRAMENTO

    WHERE id_saladeaula IN (
        SELECT DISTINCT

        sa.id_saladeaula

        FROM tb_saladeaula AS sa
        JOIN tb_entidade AS en ON en.id_entidade = sa.id_entidade
        LEFT JOIN tb_saladeaulaentidade SE ON se.id_entidade = en.id_entidade AND sa.id_saladeaula = se.id_entidade
        JOIN tb_esquemaconfiguracao AS ec ON en.id_esquemaconfiguracao = ec.id_esquemaconfiguracao
        JOIN tb_esquemaconfiguracaoitem AS eci ON ec.id_esquemaconfiguracao = eci.id_esquemaconfiguracao
        JOIN tb_itemconfiguracao AS ic ON ic.id_itemconfiguracao = eci.id_itemconfiguracao

        WHERE
            sa.id_periodoletivo = @ID_PERIODOLETIVO
            AND ic.id_itemconfiguracao = 22
            AND st_valor = 2
    )
END

ALTER TABLE dbo.tb_projetopedagogico ADD id_horarioaula int

alter table dbo.tb_projetopedagogico
   add constraint FK_TB_PROJE_REFERENCE_TB_HORAR foreign key (id_horarioaula)
      references dbo.tb_horarioaula (id_horarioaula)

ALTER TABLE dbo.tb_produto ADD st_subtitulo VARCHAR(300)
ALTER TABLE dbo.tb_produto ADD st_slug VARCHAR(200)
ALTER TABLE dbo.tb_produto ADD bl_destaque BIT NOT NULL DEFAULT 0
ALTER TABLE dbo.tb_produto ADD dt_cadastro DATETIME2 NOT NULL DEFAULT GETDATE()
ALTER TABLE dbo.tb_produto ADD dt_atualizado DATETIME2 NOT NULL DEFAULT GETDATE()
ALTER TABLE dbo.tb_produto ADD dt_iniciopontosprom DATETIME2 NOT NULL DEFAULT GETDATE()
ALTER TABLE dbo.tb_produto ADD dt_fimpontosprom DATETIME2 NOT NULL DEFAULT GETDATE()
ALTER TABLE dbo.tb_produto ADD nu_pontos int
ALTER TABLE dbo.tb_produto ADD nu_pontospromocional int
ALTER TABLE dbo.tb_produto ADD nu_estoque NUMERIC(30,2) 

execute sp_addextendedproperty 'MS_Description', 
   'Subtitulo/Cargo do produto',
   'user', 'dbo', 'table', 'tb_produto', 'column', 'st_subtitulo'
go
execute sp_addextendedproperty 'MS_Description', 
   'Fim dos pontos promocionais',
   'user', 'dbo', 'table', 'tb_produto', 'column', 'dt_fimpontosprom'
go

execute sp_addextendedproperty 'MS_Description', 
   'Inicio dos pontos promocionais',
   'user', 'dbo', 'table', 'tb_produto', 'column', 'dt_iniciopontosprom'
go
execute sp_addextendedproperty 'MS_Description', 
   'Numero de pontos do produto',
   'user', 'dbo', 'table', 'tb_produto', 'column', 'nu_pontospromocional'
go
execute sp_addextendedproperty 'MS_Description', 
   'Numero de pontos do produto',
   'user', 'dbo', 'table', 'tb_produto', 'column', 'nu_pontos'
go
execute sp_addextendedproperty 'MS_Description', 
   'Estoque ou vagas do produto',
   'user', 'dbo', 'table', 'tb_produto', 'column', 'nu_estoque'
go
execute sp_addextendedproperty 'MS_Description', 
   'Campo que define se esse produto deve ser destacado no site',
   'user', 'dbo', 'table', 'tb_produto', 'column', 'bl_destaque'
go

ALTER TABLE tb_contrato add dt_entregadocumentacao DATE
ALTER TABLE tb_contrato add bl_documentacaocompleta BIT DEFAULT ((0))


begin tran
SET IDENTITY_INSERT dbo.tb_funcionalidade on
INSERT INTO tb_funcionalidade (
  id_funcionalidade,
  st_funcionalidade,
  id_funcionalidadepai,
  bl_ativo,
  id_situacao,
  st_classeflex,
  st_urlicone,
  id_tipofuncionalidade,
  nu_ordem,
  st_classeflexbotao,
  bl_pesquisa,
  bl_lista,
  id_sistema,
  st_ajuda,
  st_target,
  st_urlcaminho,
  bl_visivel,
  bl_relatorio,
  st_urlcaminhoedicao
)
VALUES (
  747,
  'Núcleo de Atendentes',
  338,
  1,
  123,
  'br.com.ead1.gestor2.view.conteudo.entidade.cadastrar.NucleoAtendentes',
  '/img/ico/money--arrow.png',
  3,
  NULL,
  'NucleoAtendentes',
  1,
  0,
  1,
  NULL,
  NULL,
  '/nucleo-atendentes',
  1,
  0,
  NULL
);
GO
SET IDENTITY_INSERT dbo.tb_funcionalidade off
commit
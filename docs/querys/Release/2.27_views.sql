ALTER VIEW [rel].[vw_cronogramasalas] as
SELECT DISTINCT
sa.id_saladeaula ,
st_saladeaula ,
sa.dt_inicioinscricao ,
sa.dt_fiminscricao ,
sa.dt_abertura ,
sa.dt_encerramento ,
cast(dt_atualiza as date) as dt_atualiza,
st_disciplina ,
dc.st_tituloexibicao AS st_tituloexibicaodisciplina ,
st_projetopedagogico ,
aps.id_projetopedagogico,
uss.st_nomecompleto AS st_nomeprofessor ,
usp.id_usuario AS id_coordenador,
usp.st_nomecompleto AS st_nomecoordenador,
si.st_codsistemacurso AS st_codava,
sa.id_entidade,
sa.id_categoriasala,
CAST(sa.dt_cadastro as date) as dt_cadastro,
cast(COUNT(ta.id_matriculadisciplina) AS varchar) AS st_alunos,
CASE DATEPART(MONTH, pl.dt_abertura)
WHEN 1 THEN 'Janeiro'
WHEN 2 THEN 'Fevereiro'
WHEN 3 THEN 'Março'
WHEN 4 THEN 'Abril'
WHEN 5 THEN 'Maio'
WHEN 6 THEN 'Junho'
WHEN 7 THEN 'Julho'
WHEN 8 THEN 'Agosto'
WHEN 9 THEN 'Setembro'
WHEN 10 THEN 'Outubro'
WHEN 11 THEN 'Novembro'
WHEN 12 THEN 'Dezembro'
END AS st_mesoferta
FROM dbo.tb_saladeaula AS sa
JOIN dbo.tb_areaprojetosala AS aps ON aps.id_saladeaula = sa.id_saladeaula
JOIN dbo.tb_disciplinasaladeaula AS da ON da.id_saladeaula = sa.id_saladeaula
JOIN tb_disciplina AS dc ON dc.id_disciplina = da.id_disciplina
JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = aps.id_projetopedagogico
LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS ups ON ups.id_saladeaula = aps.id_saladeaula AND ups.bl_titular = 1 and ups.bl_ativo=1
LEFT JOIN dbo.tb_usuario AS uss ON uss.id_usuario = ups.id_usuario
LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS upp ON upp.id_projetopedagogico = pp .id_projetopedagogico AND upp.bl_titular = 1 and upp.bl_ativo=1 and upp.id_disciplina is null
LEFT JOIN dbo.tb_perfil AS tbp ON tbp.id_perfil = upp.id_perfil AND tbp.id_perfilpedagogico = 2
LEFT JOIN dbo.tb_usuario AS usp ON usp.id_usuario = upp.id_usuario
LEFT JOIN dbo.tb_saladeaulaintegracao AS si ON si.id_saladeaula = sa.id_saladeaula
LEFT JOIN dbo.tb_alocacao AS ta ON ta.id_saladeaula = sa.id_saladeaula AND ta.bl_ativo = 1
LEFT JOIN dbo.tb_periodoletivo as pl ON sa.id_periodoletivo = pl.id_periodoletivo
WHERE sa.bl_ativa = 1 AND sa.id_situacao != 74
GROUP by
sa.id_saladeaula,
st_saladeaula ,
sa.dt_inicioinscricao ,
sa.dt_fiminscricao ,
sa.dt_abertura ,
sa.dt_encerramento ,
dt_atualiza,
st_disciplina ,
dc.st_tituloexibicao ,
st_projetopedagogico ,
aps.id_projetopedagogico,
uss.st_nomecompleto ,
usp.id_usuario ,
usp.st_nomecompleto,
si.st_codsistemacurso,
sa.id_entidade,
sa.dt_cadastro,
pl.dt_abertura,
sa.id_categoriasala;
go
ALTER VIEW [dbo].[vw_alocacaoalunosala]
AS

SELECT DISTINCT
mt.id_matricula,
pp.id_projetopedagogico,
pp.st_projetopedagogico,
mdt.id_disciplina,
mdt.st_disciplina,
salan.id_alocacao AS id_alocacaonormal,
salan.id_saladeaula AS id_saladeaulanormal,
salan.st_saladeaula AS st_saladeaulanormal,
salan.id_matriculadisciplina AS id_matriculadisciplinanor,
salan.dt_abertura AS dt_aberturanormal,
salap.id_alocacao AS id_alocacaoprr,
salap.id_saladeaula AS id_saladeaulaprr,
salap.st_saladeaula AS st_saladeaulaprr,
salap.id_matriculadisciplina AS id_matriculadisciplinaprr,
salap.dt_abertura AS dt_aberturaprr,
mdt.id_modulo,
mt.id_usuario,
mdt.id_trilha,
mdt.id_tipodisciplina,
salan.dt_inicioalocacao AS dt_inicioalocacaonormal,
salan.nu_diasextensao AS nu_diasextensaonormal,
salan.nu_diasaluno AS nu_diasalunonormal,
salan.id_tiposaladeaula AS id_tiposalanormal,
salap.dt_inicioalocacao AS dt_inicioalocacaoprr,
salap.nu_diasextensao AS nu_diasextensaoprr,
salap.nu_diasaluno AS nu_diasalunoprr,
salap.id_tiposaladeaula AS id_tiposalaprr,
salan.nu_diasacessoextencao AS nu_diasacessoatualn,
salap.nu_diasacessoextencao AS nu_diasacessoatualp,
md.id_situacao as id_situacaomatriculadisciplina
FROM tb_matricula AS mt
JOIN tb_projetopedagogico AS pp
ON mt.id_projetopedagogico = pp.id_projetopedagogico
JOIN vw_modulodisciplinaprojetotrilha AS mdt
ON mdt.id_projetopedagogico = pp.id_projetopedagogico
AND mdt.bl_ativo = 1
JOIN tb_matriculadisciplina as md on md.id_matricula = mt.id_matricula  AND md.id_disciplina = mdt.id_disciplina
OUTER APPLY (SELECT
  sal.id_saladeaula,
  sal.st_saladeaula,
  al.id_alocacao,
  sal.id_categoriasala,
  md.id_matriculadisciplina,
  sal.dt_abertura,
  al.dt_inicio AS dt_inicioalocacao,
  sal.nu_diasextensao,
  al.nu_diasextensao AS nu_diasaluno,
  sal.id_tiposaladeaula,
  DATEDIFF(DAY,GETDATE(),(DATEADD(DAY,(al.nu_diasextensao+sal.nu_diasextensao),al.dt_inicio))) AS nu_diasacessoextencao
  FROM tb_saladeaula AS sal
  JOIN vw_saladisciplinaalocacao AS sa
  ON sa.id_matricula = mt.id_matricula
  AND sa.id_alocacao IS NOT NULL
  AND sa.id_categoriasala = 1
  AND sa.id_disciplina = mdt.id_disciplina
  JOIN tb_alocacao al
  ON al.id_matriculadisciplina = md.id_matriculadisciplina
  AND al.id_saladeaula = sa.id_saladeaula
  AND al.bl_ativo = '1'
  WHERE ((sal.dt_encerramento >= CAST(GETDATE() AS DATE))
  OR (sal.dt_encerramento IS NULL)
  OR (al.id_saladeaula = sa.id_saladeaula))
  --AND sal.id_situacao = 8
  AND sal.bl_ativa = 1
  AND sal.id_categoriasala = 1
  AND sal.id_saladeaula = sa.id_saladeaula) AS salan
  OUTER APPLY (SELECT
    sal.id_saladeaula,
    sal.st_saladeaula,
    al.id_alocacao,
    sal.id_categoriasala,
    md.id_matriculadisciplina,
    sal.dt_abertura,
    al.dt_inicio AS dt_inicioalocacao,
    sal.nu_diasextensao,
    al.nu_diasextensao AS nu_diasaluno,
    sal.id_tiposaladeaula,
    DATEDIFF(DAY,GETDATE(),(DATEADD(DAY,(al.nu_diasextensao+sal.nu_diasextensao),al.dt_inicio))) AS nu_diasacessoextencao
    FROM tb_saladeaula AS sal
    JOIN vw_saladisciplinaalocacao AS sa
    ON sa.id_matricula = mt.id_matricula
    AND sa.id_alocacao IS NOT NULL
    AND sa.id_categoriasala = 2
    JOIN tb_alocacao al
    ON al.id_matriculadisciplina = md.id_matriculadisciplina
    AND al.id_saladeaula = sa.id_saladeaula
    AND al.bl_ativo = '1'
    WHERE ((sal.dt_encerramento >= CAST(GETDATE() AS DATE))
    OR (sal.dt_encerramento IS NULL)
    OR (al.id_saladeaula = sa.id_saladeaula))
    --AND sal.id_situacao = 8
    AND sal.bl_ativa = 1
    AND sal.id_categoriasala = 2
    AND sal.id_saladeaula = sa.id_saladeaula) AS salap
    GO
    ALTER VIEW [rel].[vw_provasporpolo] AS
    SELECT
    ap.id_aplicadorprova,
    aa.id_avaliacaoaplicacao,
    ape.id_entidade,
    ap.st_aplicadorprova,
    aa.dt_aplicacao,
    ha.hr_inicio,
    ha.hr_fim,
    aa.nu_maxaplicacao,
    (SELECT COUNT(id_avaliacaoagendamento) FROM tb_avaliacaoagendamento saa WHERE saa.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao AND saa.id_situacao IN(68,69)  AND ape.id_entidade = saa.id_entidade) AS nu_totalagendamentos,
    (SELECT COUNT(id_avaliacaoagendamento) FROM tb_avaliacaoagendamento saa2 WHERE saa2.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao AND saa2.id_tipodeavaliacao = 0 AND saa2.id_situacao IN(68,69)  AND ape.id_entidade = saa2.id_entidade) AS nu_agendamentoprovafinal,
    (SELECT COUNT(id_avaliacaoagendamento) FROM tb_avaliacaoagendamento saa3 WHERE saa3.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao AND saa3.id_tipodeavaliacao > 0 AND saa3.id_situacao IN(68,69)  AND ape.id_entidade = saa3.id_entidade) AS nu_agendamentorecuperacao,
    (nu_maxaplicacao - (SELECT COUNT(id_avaliacaoagendamento) FROM tb_avaliacaoagendamento saa4 WHERE saa4.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao AND saa4.id_situacao IN(68,69) AND ape.id_entidade = saa4.id_entidade)) AS nu_vagasrestantes
    FROM tb_aplicadorprova ap
    JOIN tb_aplicadorprovaentidade ape on ape.id_aplicadorprova = ap.id_aplicadorprova
    JOIN tb_avaliacaoaplicacao aa on aa.id_aplicadorprova = ap.id_aplicadorprova
    JOIN tb_avaliacao a on a.id_entidade = ape.id_entidade AND a.id_tipoavaliacao IN(1,2,3,4)
    JOIN tb_horarioaula ha on ha.id_horarioaula = aa.id_horarioaula
    go
    ALTER VIEW [dbo].[vw_enviomensagem] as
    --View para envio de Mensagens
    SELECT      em.id_enviomensagem, em.id_mensagem, em.id_tipoenvio, em.id_emailconfig, em.id_evolucao,
    em.dt_cadastro, em.dt_enviar, dt_tentativa,
    ed.id_enviodestinatario, ed.id_usuario,m.st_caminhoanexo,
    CASE
    WHEN em.id_tipoenvio = 4 THEN em.dt_cadastro
    ELSE em.dt_envio END AS dt_envio,
    CASE
    WHEN ed.id_usuario IS NULL   THEN ed.st_nome
    ELSE us.st_nomecompleto
    END AS st_nome,
    CASE
    WHEN ed.id_usuario IS NULL   THEN ed.st_endereco
    ELSE ce.st_email
    END AS st_endereco,
    ed.id_tipodestinatario, m.st_mensagem, m.st_texto, ed.nu_telefone, p.id_entidade,
    e.st_evolucao, te.st_tipoenvio,
    e2.st_evolucao AS st_evolucaodestinatario , e2.id_evolucao AS id_evolucaodestinatario

    FROM        tb_enviomensagem                AS em
    INNER JOIN  tb_enviodestinatario            AS ed   ON ed.id_enviomensagem = em.id_enviomensagem
    INNER JOIN  tb_mensagem                     AS m    ON m.id_mensagem = em.id_mensagem
    INNER JOIN  tb_evolucao                     AS e    ON e.id_evolucao = em.id_evolucao
    INNER JOIN  tb_tipoenvio                    AS te   ON em.id_tipoenvio = te.id_tipoenvio
    LEFT JOIN   tb_evolucao                     AS e2   ON e2.id_evolucao = ed.id_evolucao
    LEFT JOIN   tb_pessoa                       AS p    ON p.id_usuario = ed.id_usuario AND p.id_entidade = m.id_entidade
    LEFT JOIN   tb_usuario						AS us   ON us.id_usuario = p.id_usuario
    LEFT JOIN   tb_contatosemailpessoaperfil    AS cepp ON cepp.id_usuario = ed.id_usuario AND cepp.id_entidade = m.id_entidade AND cepp.bl_ativo = 1 AND cepp.bl_padrao = 1
    LEFT JOIN   tb_contatosemail                AS ce   ON ce.id_email = cepp.id_email
    LEFT JOIN tb_emailconfig as ec on ec.id_emailconfig = em.id_emailconfig
    where ((em.id_emailconfig is not null and ec.bl_erro = 0) or em.id_tipoenvio <> 3) and m.id_entidade <> 334
    GO
    ALTER VIEW [dbo].[vw_gerarvendatexto] AS
    SELECT
    vd.id_venda,
    vd.id_entidade,
    lc.id_lancamento,
    usv.st_cpf,
    usl.id_usuario,
    null as id_matricula,
    usv.st_nomecompleto AS st_vendacliente,
    vd.nu_valorbruto AS nu_vendavalorbruto,
    vd.nu_valorliquido AS
    nu_vendavalorliquido,
    vd.nu_descontoporcentagem AS nu_vendadescontoporc,
    vd.nu_descontovalor AS nu_vendadescontovalor,
    lc.nu_valor AS nu_lancamentovalor,
    lc.dt_vencimento AS dt_lancamentovencimento,
    lc.dt_quitado AS dt_lancamentodataquitado,
    lc.nu_quitado AS nu_lancamentoquitado,
    mp.st_meiopagamento AS st_lancamentomeiopagamento,
    usl.st_nomecompleto AS st_lancamentoresponsavel,
    (SELECT dbo.fn_mesporextenso(lc.dt_vencimento))+'/'+ CAST(DATEPART(YEAR,lc.dt_vencimento) AS VARCHAR)AS st_datavencimento
    ,
    vlq.nu_quitado,
    vlq.dt_primeiroquitado,
    vlq.dt_ultimoquitado,
    vlq.st_mesanoprimeiroquitado,
    vlq.st_mesanoultimoquitado,
    vlq.st_mesanoprimeiroquitado + ' a ' + vlq.st_mesanoultimoquitado AS st_periodoquitado,
    CONVERT(VARCHAR(30),GETDATE() , 103)
    AS st_atual,
    CONVERT(VARCHAR(30),YEAR(GETDATE())-1 , 103) AS st_ano,
    usv.st_endereco,
    usv.nu_numero,
    usv.st_bairro,
    usv.st_cidade,
    usv.st_cep,
    et.st_nomeentidade,

    CASE WHEN vwps.st_nomecompleto IS NOT NULL THEN vwps.st_nomecompleto
    WHEN entl.st_nomeentidade IS NOT NULL THEN entl.st_nomeentidade COLLATE SQL_Latin1_General_CP1_CI_AS
    END AS st_nomecompletoresponsavel,

    CASE WHEN vwps.st_cpf IS NOT NULL THEN vwps.st_cpf
    WHEN entl.st_cnpj IS NOT NULL THEN entl.st_cnpj
    END AS st_cpfresponsavel

    FROM tb_venda AS vd
    LEFT JOIN tb_lancamentovenda AS lv ON lv.id_venda = vd.id_venda
    LEFT JOIN tb_lancamento AS lc ON lc.id_lancamento = lv.id_lancamento
    --JOIN tb_vendaproduto as vp ON vp.id_venda = vd.id_venda
    --LEFT JOIN tb_matricula as mt on mt.id_vendaproduto = vp.id_vendaproduto
    JOIN vw_pessoa  AS usv ON usv.id_usuario = vd.id_usuario and usv.id_entidade = vd.id_entidade
    LEFT JOIN tb_usuario AS usl ON usl.id_usuario = lc.id_usuariolancamento
    LEFT JOIN tb_meiopagamento AS mp ON mp.id_meiopagamento = lc.id_meiopagamento
    JOIN dbo.tb_entidade AS et ON et.id_entidade = vd.id_entidade
    LEFT JOIN vw_vendalancamentoquitados AS vlq ON (vd.id_venda = vlq.id_venda)
    LEFT JOIN vw_pessoa AS vwps ON vwps.id_usuario = lc.id_usuariolancamento AND vwps.id_entidade = vd.id_entidade
    LEFT JOIN tb_entidade AS entl ON entl.id_entidade = lc.id_entidadelancamento
    GO
    CREATE view [rel].[vw_alunosrenovados] as

    SELECT usu.st_nomecompleto, usu.st_cpf, proped.st_projetopedagogico,
    proped.id_projetopedagogico, tur.st_turma, tur.id_turma, venpro.id_matricula,
    ven.dt_cadastro, TRY_CAST(ven.dt_limiterenovacao AS datetime2) AS dt_limiterenovacao,
    ven.dt_confirmacao, vwpe.st_email
    FROM
    tb_venda AS ven
    JOIN tb_usuario AS usu ON usu.id_usuario = ven.id_usuario
    JOIN vw_pessoaemail AS vwpe ON vwpe.id_usuario = usu.id_usuario
    JOIN tb_vendaproduto AS venpro ON venpro.id_venda = ven.id_venda
    JOIN tb_turma AS tur ON tur.id_turma = venpro.id_turma
    JOIN tb_turmaprojeto AS turpro ON turpro.id_turma = tur.id_turma
    JOIN tb_projetopedagogico AS proped ON proped.id_projetopedagogico = turpro.id_projetopedagogico
    WHERE ven.bl_renovacao = 1
    go
    ALTER VIEW [rel].[vw_cronogramasalas] as
    SELECT DISTINCT
    sa.id_saladeaula ,
    sa.st_saladeaula ,
    sa.dt_inicioinscricao ,
    sa.dt_fiminscricao ,
    sa.dt_abertura ,
    sa.dt_encerramento ,
    cast(dt_atualiza as date) as dt_atualiza,
    st_disciplina ,
    dc.st_tituloexibicao AS st_tituloexibicaodisciplina ,
    st_projetopedagogico ,
    aps.id_projetopedagogico,
    uss.st_nomecompleto AS st_nomeprofessor ,
    usp.id_usuario AS id_coordenador,
    usp.st_nomecompleto AS st_nomecoordenador,
    si.st_codsistemacurso AS st_codava,
    sa.id_entidade,
    sa.id_categoriasala,
    pl.st_periodoletivo,
    CAST(sa.dt_cadastro as date) as dt_cadastro,
    cast(COUNT(ta.id_matriculadisciplina) AS varchar) AS st_alunos
    FROM dbo.tb_saladeaula AS sa
    JOIN dbo.tb_areaprojetosala AS aps ON aps.id_saladeaula = sa.id_saladeaula
    JOIN dbo.tb_disciplinasaladeaula AS da ON da.id_saladeaula = sa.id_saladeaula
    JOIN tb_disciplina AS dc ON dc.id_disciplina = da.id_disciplina
    JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = aps.id_projetopedagogico
    LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS ups ON ups.id_saladeaula = aps.id_saladeaula AND ups.bl_titular = 1 and ups.bl_ativo=1
    LEFT JOIN dbo.tb_usuario AS uss ON uss.id_usuario = ups.id_usuario
    LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS upp ON upp.id_projetopedagogico = pp .id_projetopedagogico AND upp.bl_titular = 1 and upp.bl_ativo=1 and upp.id_disciplina is null
    LEFT JOIN dbo.tb_perfil AS tbp ON tbp.id_perfil = upp.id_perfil AND tbp.id_perfilpedagogico = 2
    LEFT JOIN dbo.tb_usuario AS usp ON usp.id_usuario = upp.id_usuario
    LEFT JOIN dbo.tb_saladeaulaintegracao AS si ON si.id_saladeaula = sa.id_saladeaula
    LEFT JOIN dbo.tb_alocacao AS ta ON ta.id_saladeaula = sa.id_saladeaula AND ta.bl_ativo = 1
    LEFT JOIN tb_periodoletivo as pl on pl.id_periodoletivo = sa.id_periodoletivo
    WHERE sa.bl_ativa = 1 AND sa.id_situacao != 74
    GROUP by
    sa.id_saladeaula,
    st_saladeaula ,
    sa.dt_inicioinscricao ,
    sa.dt_fiminscricao ,
    sa.dt_abertura ,
    sa.dt_encerramento ,
    dt_atualiza,
    st_disciplina ,
    dc.st_tituloexibicao ,
    st_projetopedagogico ,
    aps.id_projetopedagogico,
    uss.st_nomecompleto ,
    usp.id_usuario ,
    usp.st_nomecompleto,
    si.st_codsistemacurso,
    sa.id_entidade,
    sa.dt_cadastro,
    sa.id_categoriasala,
    pl.st_periodoletivo
    go
    alter VIEW [rel].[vw_provasporpolo] AS
    SELECT
    ap.id_aplicadorprova,
    aa.id_avaliacaoaplicacao,
    ape.id_entidade,
    ap.st_aplicadorprova,
    aa.dt_aplicacao,
    ha.hr_inicio,
    ha.hr_fim,
    aa.nu_maxaplicacao,
    (SELECT COUNT(id_avaliacaoagendamento) FROM tb_avaliacaoagendamento saa WHERE saa.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao AND saa.id_situacao IN(68,69)  AND ape.id_entidade = saa.id_entidade) AS nu_totalagendamentos,
    (SELECT COUNT(id_avaliacaoagendamento) FROM tb_avaliacaoagendamento saa2 WHERE saa2.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao AND saa2.id_tipodeavaliacao = 0 AND saa2.id_situacao IN(68,69)  AND ape.id_entidade = saa2.id_entidade) AS nu_agendamentoprovafinal,
    (SELECT COUNT(id_avaliacaoagendamento) FROM tb_avaliacaoagendamento saa3 WHERE saa3.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao AND saa3.id_tipodeavaliacao > 0 AND saa3.id_situacao IN(68,69)  AND ape.id_entidade = saa3.id_entidade) AS nu_agendamentorecuperacao,
    (nu_maxaplicacao - (SELECT COUNT(id_avaliacaoagendamento) FROM tb_avaliacaoagendamento saa4 WHERE saa4.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao AND saa4.id_situacao IN(68,69) AND ape.id_entidade = saa4.id_entidade)) AS nu_vagasrestantes
    FROM tb_aplicadorprova ap
    JOIN tb_aplicadorprovaentidade ape on ape.id_aplicadorprova = ap.id_aplicadorprova
    JOIN tb_avaliacaoaplicacao aa on aa.id_aplicadorprova = ap.id_aplicadorprova
    JOIN tb_avaliacao a on a.id_entidade = ape.id_entidade AND a.id_tipoavaliacao IN(1,2,3,4)
    JOIN tb_horarioaula ha on ha.id_horarioaula = aa.id_horarioaula
    go
    CREATE VIEW [totvs].[vw_Integra_Produto_Venda_GestorFluxus] AS

    SELECT
    ei.st_codsistema AS CODCOLIGADA_FCFO,
    vi.st_codvenda AS IDMOV_TITMOV,
    NULL AS NSEQITMOV_TITMOV,
    NULL AS NUMEROSEQUENCIAL_TITMOV,
    p.st_codigoprodutoexterno AS IDPRD_TITMOV,
    1 AS QUANTIDADE_TITMOV,
    vp.nu_valorbruto AS PRECOUNITARIO_TITMOV,
    vp.nu_valorbruto AS PRECOTABELA_TITMOV,
    CAST(vd.dt_confirmacao AS DATETIME) AS DATAEMISSAO_TITMOV,
    'UN' AS CODUND_TITMOV,
    1 AS QUANTIDADEARECEBER_TITMOV,
    vp.nu_valorbruto AS VALORUNITARIO_TITMOV,
    vp.nu_valorbruto AS VALORFINANCEIRO_TITMOV,
    --ei.st_codsistema AS CODCOLFCO_TITMOV,
    0 AS ALIQORDENACAO_TITMOV,
    1 AS QUANTIDADEORIGINAL_TITMOV,
    CASE WHEN p.id_tipoproduto = 6 AND ped.sg_uf IS NOT NULL AND ped.sg_uf = 'DF'	THEN 181
    WHEN p.id_tipoproduto = 6 AND ped.sg_uf IS NOT NULL AND ped.sg_uf <> 'DF'	THEN 182
    WHEN p.id_tipoproduto <> 6 AND ped.sg_uf IS NOT NULL AND ped.sg_uf = 'DF'	THEN 183
    WHEN p.id_tipoproduto <> 6 AND ped.sg_uf IS NOT NULL AND ped.sg_uf <> 'DF' THEN 184
    END AS IDNAT_TITMOV,
    0 AS FLAG_TITMOV,
    0 AS FATORCONVUND_TITMOV,
    vp.nu_valorliquido AS VALORTOTALITEM_TITMOV,
    1 AS CODFILIAL_TITMOV,
    0 AS QUANTIDADESEPARADA_TITMOV,
    0 AS PERCENTCOMISSAO_TITMOV,
    0 AS COMISSAOREPRES_TITMOV,
    0 AS VALORESCRITURACAO_TITMOV,
    0 AS VALORFINPEDIDO_TITMOV,
    0 AS VALORPFRM1_TITMOV,
    0 AS VALORPFRM2_TITMOV,
    0 AS PRECOEDITADO_TITMOV,
    1 AS QTDEVOLUMEUNITARIO_TITMOV,
    '040' AS CST_TITMOV,
    0 AS VALORDESCCONDICONALITM_TITMOV,
    0 AS VALORDESPCONDICIONALITM_TITMOV,
    CAST(vd.dt_confirmacao AS DATETIME) AS DATAORCAMENTO_TITMOV,
    9999 AS CODTBORCAMENTO_TITMOV,
    0 AS VALORUNTORCAMENTO_TITMOV,
    0 AS VALSERVICONFE_TITMOV,
    '01.001' AS CODLOC_TITMOV,
    0 AS VALORBEM_TITMOV,
    vp.nu_valorliquido AS VALORLIQUIDO_TITMOV,
    SUBSTRING ( CAST(emun.id_municipio AS VARCHAR) , 3 , LEN(CAST(emun.id_municipio AS VARCHAR)) ) AS CODMUNSERVICO_TITMOV,
    CODETDMUNSERV_TITMOV = CASE WHEN ede.sg_uf IS NOT NULL  THEN ede.sg_uf ELSE 'DF' END,
    (vp.nu_valorbruto - vp.nu_desconto) AS RATEIOCCUSTODEPTO_TITMOV,
    vp.nu_valorbruto AS VALORBRUTOITEM_TITMOV,
    vp.nu_valorbruto AS VALORBRUTOITEMORIG_TITMOV,
    ei.st_codsistema AS CODCOLTBORCAMENTO_TITMOV,
    1 AS QUANTIDADETOTAL_TITMOV,
    0 AS PRODUTOSUBSTITUTO_TITMOV,
    0 AS PRECOUNITARIOSELEC_TITMOV,
    'Venda enviada pelo gestor as '+CONVERT(VARCHAR(8), GETDATE(),  114 ) + ' de ' + CONVERT(VARCHAR, GETDATE(),  103  ) AS HISTORICOCURTO,
    vp.nu_valorliquido AS VALOR,
    100 AS PERCENTUAL,
    ei.st_codchave AS CODCCUSTO,
    vd.id_venda,
    p.id_produto,
    vd.id_entidade

    FROM tb_venda vd
    CROSS APPLY ( SELECT TOP 1 *
      FROM tb_entidadeintegracao ei
      WHERE ei.id_entidade  = vd.id_entidade
      AND ei.id_sistema = 29
    ) AS ei
    JOIN tb_vendaproduto AS vp ON vd.id_venda = vp.id_venda
    JOIN tb_produto AS p ON p.id_produto = vp.id_produto
    JOIN tb_vendaintegracao vi ON vi.id_venda = vd.id_venda AND vi.id_sistema = 29 AND vi.st_codvenda IS NOT NULL
    OUTER APPLY(SELECT TOP 1 *
      FROM tb_entidadeendereco AS eeo
      WHERE eeo.id_entidade = vd.id_entidade
      AND eeo.bl_padrao = 1
    ) as peeo

    JOIN tb_endereco AS ede ON ede.id_endereco = peeo.id_endereco
    JOIN tb_municipio AS emun ON ede.id_municipio = emun.id_municipio

    OUTER APPLY(
      SELECT mun.sg_uf
      FROM tb_usuario usr
      JOIN tb_pessoa AS ps ON ps.id_usuario = usr.id_usuario AND ps.id_entidade = vd.id_entidade
      JOIN tb_pessoaendereco  AS pseo ON pseo.id_usuario = ps.id_usuario AND ps.id_entidade = pseo.id_entidade AND pseo.bl_padrao = 1
      JOIN tb_endereco AS ed ON ed.id_endereco = pseo.id_endereco
      JOIN tb_municipio AS mun ON ed.id_municipio = mun.id_municipio
      WHERE usr.id_usuario = vd.id_usuario
    ) ped

    WHERE vd.id_venda NOT IN (
      SELECT
      id_venda
      FROM tb_vendaprodutointegracao
      WHERE id_sistema = 29
      AND id_venda = vd.id_venda
      AND id_produto = p.id_produto
      AND id_entidade = vd.id_entidade
    )

    --WHERE vd.id_venda = 112656
    GO
    create VIEW [totvs].[vw_Integra_Venda_GestorFluxus] AS

    SELECT
    ei.st_codsistema AS CODCOLIGADA_FCFO,
    NULL  AS CODCFO_FCFO,
    REPLACE(usr.st_nomecompleto,'''','') COLLATE SQL_Latin1_General_CP1_CI_AI AS NOMEFANTASIA_FCFO,
    REPLACE(usr.st_nomecompleto,'''','') COLLATE SQL_Latin1_General_CP1_CI_AI AS NOME_FCFO,
    usr.st_cpf COLLATE SQL_Latin1_General_CP1_CI_AI AS CGCCFO_FCFO,
    NULL AS INSCRESTADUAL_FCFO,
    '1' AS PAGREC_FCFO,
    1 AS TIPORUA,
    REPLACE(ed.st_endereco,'''','') AS RUA_FCFO,
    CASE WHEN (COALESCE(CAST( ed.nu_numero  AS VARCHAR), '') = '' OR CAST( ed.nu_numero  AS VARCHAR)  = '0' OR ed.nu_numero IS NULL  )
    THEN 'S/N'
    ELSE CAST( ed.nu_numero AS VARCHAR) END AS NUMERO_FCFO,
    CAST(REPLACE(ed.st_complemento,'''','') AS VARCHAR(30)) AS COMPLEMENTO_FCFO,
    1 AS TIPOBAIRRO_FCFO,
    CAST(REPLACE(ed.st_bairro,'''','') AS VARCHAR(30)) AS BAIRRO_FCFO,
    REPLACE(ed.st_cidade,'''','') AS CIDADE_FCFO,
    CASE WHEN ed.sg_uf IS NOT NULL  THEN ed.sg_uf
    ELSE 'DF' END AS CODETD_FCFO,
    REPLACE(REPLACE(ed.st_cep,'-',''),'.','') AS CEP_FCFO,
    CAST(ctl.nu_ddd AS VARCHAR)+ ' ' + CAST(ctl.nu_telefone AS VARCHAR) AS TELEFONE_FCFO,
    NULL AS FAX_FCFO,
    REPLACE(ce.st_email,'''','') AS EMAIL_FCFO,
    NULL AS CONTATO_FCFO,
    CASE WHEN ps.id_situacao = 1 THEN 1 ELSE 1 END AS ATIVO_FCFO,
    SUBSTRING ( CAST(mun.id_municipio AS VARCHAR) , 3 , LEN(CAST(mun.id_municipio AS VARCHAR)) ) AS CODMUNICIPIO_FCFO,
    CASE WHEN ps.id_usuario IS NOT NULL THEN 'F' ELSE 'J' END AS PESSOAFISOUJUR_FCFO,
    pa.st_nomepais AS PAIS_FCFO,
    CASE
    WHEN pa.id_pais = 22 THEN 1
    WHEN pa.id_pais = 116 THEN 2
    WHEN pa.id_pais = 94 THEN 3
    WHEN pa.id_pais = 43 THEN 4
    WHEN pa.id_pais = 49 THEN 5
    WHEN pa.id_pais = 4 THEN 6
    WHEN pa.id_pais = 76 THEN 7
    WHEN pa.id_pais = 10 THEN 8
    WHEN pa.id_pais = 44 THEN 9
    WHEN pa.id_pais = 73 THEN 10
    ELSE 99
    END AS IDPAIS_FCFO ,
    NULL AS IDCFO_FCFO,
    NULL AS CEI_FCFO,
    CASE WHEN ps.id_pais = 22 THEN 0 ELSE 1 END AS NACIONALIDADE_FCFO,
    NULL AS TIPOCONTRIBUINTEINSS_FCFO,
    --PARAMETROS PARA O MOVIMENTO
    NULL AS IDMOV_TMOV,
    'A' AS TIPO_TMOV,
    'F' AS STATUS_TMOV,
    CAST(vd.dt_confirmacao AS DATETIME) AS DATAEMISSAO,
    ei.st_codcpg AS CODCPG_TMOV,
    vd.nu_valorbruto AS VALORBRUTO_TMOV,
    vd.nu_valorliquido AS VALORLIQUIDO_TMOV,
    (vd.nu_valorbruto - vd.nu_valorliquido) AS VALORDESC_TMOV,
    lctgerado.nu_parcelas AS NUMEROLCTGERADO_TMOV,
    lctaberto.nu_parcelas AS NUMEROLCTABERTO_TMOV,
    SUBSTRING ( CAST(emun.id_municipio AS VARCHAR) , 3 , LEN(CAST(emun.id_municipio AS VARCHAR)) ) AS CODMUNICIPIO_TMOV,
    CODETDMUNSERV_TMOV = CASE WHEN ede.sg_uf IS NOT NULL  THEN ede.sg_uf ELSE 'DF' END,
    IDNAT_TMOV = CASE WHEN ed.sg_uf IS NOT NULL AND ed.sg_uf = 'DF' THEN 181 WHEN ed.sg_uf IS NOT NULL AND ed.sg_uf <> 'DF' THEN 182 ELSE 181 END,
    IDNAT2_TMOV = CASE WHEN ed.sg_uf IS NOT NULL AND ed.sg_uf = 'DF' THEN 183 WHEN ed.sg_uf IS NOT NULL AND ed.sg_uf <> 'DF' THEN 184 ELSE 183 END,
    0 AS MOVIMPRESSO_TMOV,
    0 AS DOCIMPRESSO_TMOV,
    0 AS FATIMPRESSA_TMOV,
    '2.2.99' AS CODTMV_TMOV,
    '040' AS CODTDO_TMOV,
    0 AS INTEGRADOAUTOMACAO_TMOV,
    'T' AS INTEGRAAPLICACAO_TMOV,
    CAST(vd.dt_confirmacao AS DATETIME) AS DATALANCAMENTO_TMOV,
    0 AS RECIBONFESTATUS_TMOV,
    0 AS USARATEIOVALORFIN_TMOV,
    0 AS CODCOLCFOAUX_TMOV,
    0 AS GEROUCONTATRABALHO_TMOV,
    0 AS GERADOPORCONTATRABALHO_TMOV,
    0 AS INTEGRADOBONUM_TMOV,
    0 AS FLAPROCESSADO_TMOV,
    0 AS STSEMAIL_TMOV,
    0 AS VINCULADOESTOQUEFL_TMOV,
    1 AS CODFILIAL_TMOV,
    1 AS SERIE_TMOV,
    0 AS GEROUFATURA_TMOV,
    ei.st_codarea AS CODCXA_TMOV,
    ei.st_codsistema AS CODCOLFCO_TMOV,
    1 AS CODFILIALDESTINO_TMOV,
    0 AS GERADOPORLOTE_TMOV,
    0 AS STATUSEXPORTCONT_TMOV,
    REPLICATE('0', 9 - LEN(vd.id_venda)) + RTrim(vd.id_venda) AS NUMEROMOV_TMOV,
    0 AS PERCENTUALDESC_TMOV,
    'Venda enviada pelo gestor as '+CONVERT(VARCHAR(8), GETDATE(),  114 ) + ' de ' + CONVERT(VARCHAR, GETDATE(),  103  ) AS HISTORICOCURTO_TMOV,
    ei.st_codchave AS CODCCUSTO_TMOV,
    vd.nu_valorliquido AS VALOR,
    100 AS PERCENTUAL,
    1 AS NSEQITMOV_TMOV,
    CAST(vd.id_venda AS VARCHAR)+'G2U' AS id_venda,
    CASE WHEN mpcc.st_codsistema IS NOT NULL THEN
    mpcc.st_codsistema
    ELSE
    mpe.st_codsistema
    END AS CODTB2FLX -- Forma de pagamento

    FROM tb_venda vd

    JOIN tb_usuario AS usr ON usr.id_usuario = vd.id_usuario
    JOIN tb_pessoa AS ps ON ps.id_usuario = usr.id_usuario AND ps.id_entidade = vd.id_entidade

    CROSS APPLY ( SELECT TOP 1 *
      FROM tb_entidadeintegracao ei
      WHERE ei.id_entidade  = vd.id_entidade
      AND ei.id_sistema = 29
    ) AS ei

    CROSS APPLY (SELECT COUNT(l.id_lancamento) AS nu_parcelas FROM tb_lancamentovenda lv
    JOIN tb_lancamento l ON l.id_lancamento = lv.id_lancamento
    WHERE  vd.id_venda = lv.id_venda
    AND bl_ativo = 1
    --AND bl_original = 1
  ) AS lctgerado

  CROSS APPLY (SELECT COUNT(l.id_lancamento) AS nu_parcelas FROM tb_lancamentovenda lv
  JOIN tb_lancamento l ON l.id_lancamento = lv.id_lancamento
  WHERE  vd.id_venda = lv.id_venda
  AND l.bl_quitado = 0
  AND l.bl_ativo = 1
  --AND bl_original = 1
) AS lctaberto

OUTER APPLY(SELECT TOP 1 *
  FROM tb_pessoaendereco AS pseo
  WHERE pseo.id_usuario = ps.id_usuario
  AND ps.id_entidade = pseo.id_entidade
  AND pseo.bl_padrao = 1
) as pse

CROSS APPLY (SELECT TOP 1 mpic.id_meiopagamentointegracao ,
  mpic.id_cartaobandeira ,
  mpic.id_sistema ,
  mpic.id_usuariocadastro ,
  mpic.id_meiopagamento ,
  mpic.id_entidade ,
  mpic.st_codsistema ,
  mpic.dt_cadastro,
  mpic.st_codcontacaixa
  FROM dbo.tb_meiopagamentointegracao AS mpic
  JOIN tb_lancamento l ON mpic.id_meiopagamento = l.id_meiopagamento
  JOIN tb_lancamentovenda lv ON l.id_lancamento = lv.id_lancamento
  WHERE lv.id_venda = vd.id_venda
  AND mpic.id_entidade = vd.id_entidade
) AS mpe

OUTER APPLY (
  SELECT TOP 1 mpi.st_codsistema
  FROM dbo.tb_meiopagamentointegracao mpi
  JOIN tb_cartaoconfig AS cc ON mpi.id_cartaobandeira = cc.id_cartaobandeira
  JOIN tb_lancamento l ON mpi.id_meiopagamento = l.id_meiopagamento
  JOIN tb_lancamentovenda lv ON l.id_lancamento = lv.id_lancamento
  WHERE  mpi.id_sistema = 3
  AND lv.id_venda = vd.id_venda
  AND mpi.id_entidade = vd.id_entidade
) AS mpcc

LEFT JOIN tb_endereco AS ed ON ed.id_endereco = pse.id_endereco
LEFT JOIN tb_municipio AS mun ON ed.id_municipio = mun.id_municipio
LEFT JOIN tb_contatostelefonepessoa AS ctp ON ctp.id_usuario = usr.id_usuario AND ctp.id_entidade = ps.id_entidade AND ctp.bl_padrao = 1
LEFT JOIN tb_contatostelefone AS ctl ON ctl.id_telefone = ctp.id_telefone
LEFT JOIN tb_contatosemailpessoaperfil AS cepp ON cepp.id_usuario = usr.id_usuario AND cepp.id_entidade = ps.id_entidade AND cepp.bl_padrao = 1
LEFT JOIN tb_contatosemail AS ce ON ce.id_email = cepp.id_email
LEFT JOIN tb_pais AS pa ON pa.id_pais = ed.id_pais

OUTER APPLY(SELECT TOP 1 *
  FROM tb_entidadeendereco AS eeo
  WHERE eeo.id_entidade = vd.id_entidade
  AND eeo.bl_padrao = 1
) as peeo

JOIN tb_endereco AS ede ON ede.id_endereco = peeo.id_endereco
JOIN tb_municipio AS emun ON ede.id_municipio = emun.id_municipio

WHERE vd.id_evolucao = 10
AND vd.id_venda NOT IN (
  SELECT id_venda FROM tb_vendaintegracao WHERE id_sistema  = 29 AND st_codvenda IS NOT NULL
)

GO
CREATE VIEW totvs.vw_Integra_lancamento_movimentoFluxus AS

SELECT
l.bl_quitado AS statusbaixa,
CASE WHEN l.id_codcoligada IS NOT NULL THEN l.id_codcoligada ELSE ei.st_codsistema END AS codcoligada,
li.st_codlancamento AS idlan,
vi.st_codvenda,
v.id_venda,
l.id_lancamento
FROM tb_lancamentointegracao li
JOIN tb_lancamento l ON li.id_lancamento = l.id_lancamento
JOIN tb_lancamentovenda lv ON l.id_lancamento = lv.id_lancamento
JOIN tb_venda v ON v.id_venda = lv.id_venda
JOIN tb_vendaintegracao vi ON vi.id_venda = lv.id_venda and vi.id_sistema = 29
JOIN tb_entidadeintegracao ei ON ei.id_entidade = v.id_entidade and ei.id_sistema = 29

WHERE li.st_codvenda IS NULL
GO
--incremento
ALTER VIEW [dbo].[vw_gradenota] AS

SELECT DISTINCT
  sa.id_saladeaula,
  sa.dt_abertura,
  pp.id_projetopedagogico,
  pp.st_projetopedagogico,
  sa.st_saladeaula,
  mn.st_notatcc,
  mn.id_tiponotatcc,
  mn.st_notaead,
  mn.id_tiponotaead,
  mn.st_notaatividade,
  mn.id_tiponotaatividade,
  CAST(mn.st_notafinal AS INT) AS st_notafinal,
  mn.id_tiponotafinal,
  mn.id_avaliacaoatividade,
  dc.id_disciplina,
  dc.st_disciplina,
  dc.id_tipodisciplina,
  dc.st_tituloexibicao AS st_tituloexibicaodisciplina,
  mt.id_matricula,
  md.id_matriculadisciplina,
  md.id_aproveitamento,
  acr.id_avaliacaoconjuntoreferencia,
  CAST ((md.nu_aprovafinal * pp.nu_notamaxima / 100) AS INT) AS nu_notafinal,
  pp.nu_percentualaprovacao,
  ev.id_evolucao,
  ev.st_evolucao,
  st.id_situacao,
  st.st_situacao,
  dc.nu_cargahoraria,

  --DATA DE ENCERRAMENTO RECEBE SEU VALOR DE ACORDO COM A CATEGORIA DE SALA
  dt_encerramento = CAST(
    (CASE
      -- Periodico
      WHEN sa.id_tiposaladeaula = 2
        THEN DATEADD(DAY, ISNULL(sa.nu_diasencerramento, 0) + ISNULL(sa.nu_diasaluno, 0) + ISNULL(sa.nu_diasextensao, 0), sa.dt_encerramento)
      -- Permanente
      WHEN sa.id_tiposaladeaula = 1 AND sa.dt_abertura <= al.dt_cadastro
        THEN DATEADD(DAY, ISNULL(sa.nu_diasencerramento, 0) + ISNULL(sa.nu_diasaluno, 0) + ISNULL(sa.nu_diasextensao, 0), al.dt_cadastro)
      -- Permanente
      WHEN sa.id_tiposaladeaula = 1 AND sa.dt_abertura > al.dt_cadastro
        THEN DATEADD(DAY, ISNULL(sa.nu_diasencerramento, 0) + ISNULL(sa.nu_diasaluno, 0) + ISNULL(sa.nu_diasextensao, 0), al.dt_inicio)
      ELSE '2016-12-01'
    END)
  AS DATE),

  CAST ((mn.nu_notatotal) AS INT) AS nu_notatotal,
  sa.id_categoriasala,
  ap.dt_cadastro AS dt_cadastroaproveitamento,
  ap.id_usuario AS id_usuarioaproveitamento,
  u.st_nomecompleto AS st_nomeusuarioaproveitamento,
  md.st_disciplinaoriginal,
  ea.id_encerramentosala,

  -- AO ATUALIZAR AS STRINGS DO st_status, ATUALIZAR O ARQUIVO G2\Constante\StatusGradeNota.php
  st_status = (CASE
    WHEN ea.id_alocacao IS NOT NULL AND md.id_evolucao = 12 THEN ev.st_evolucao
    WHEN ea.id_alocacao IS NOT NULL AND md.id_evolucao = 19 THEN ev.st_evolucao
    WHEN ea.id_alocacao IS NOT NULL AND md.id_situacao = 65 THEN 'Crédito Concedido'
    WHEN ea.id_alocacao IS NOT NULL AND (mn.nu_notatotal + ISNULL(nf.nu_notafaltante, 0)) >= (pp.nu_notamaxima * pp.nu_percentualaprovacao / 100) THEN 'Satisfatório'
    WHEN ea.id_alocacao IS NOT NULL AND (mn.nu_notatotal + ISNULL(nf.nu_notafaltante, 0)) < (pp.nu_notamaxima * pp.nu_percentualaprovacao / 100) THEN 'Insatisfatório'
    WHEN md.id_aproveitamento IS NOT NULL AND md.id_situacao = 65 THEN 'Aproveitamento de Disciplina'
    WHEN ea.id_alocacao IS NULL AND ea.id_encerramentosala IS NULL THEN 'Não encerrado'
    ELSE '-'
  END),

  -- Define se o aluno está apto para o agendamento nesta disciplina
  bl_status = (CASE
    -- Se a disciplina não for obrigatória, então está APTO
    WHEN md.bl_obrigatorio = 0 THEN 1
    -- Se a disciplina tiver aproveitamento de nota, então está APTO
    WHEN mn.id_tiponotaead IS NOT NULL AND mn.id_tiponotaead = 2 THEN 1
    -- Se a sala não foi encerrada, então NÃO está APTO
    WHEN ea.id_encerramentosala IS NULL THEN 0
    -- Se a nota for Satisfatória, então está APTO
    WHEN (mn.nu_notatotal + ISNULL(nf.nu_notafaltante, 0)) >= (pp.nu_notamaxima * pp.nu_percentualaprovacao / 100) THEN 1
    -- Por fim, NÃO está APTO
    ELSE 0
  END),

  bl_complementar = (CASE
    WHEN md.id_matricula != md.id_matriculaoriginal AND ppcompl.bl_disciplinacomplementar = 1 THEN 1
    ELSE 0
  END),

  DATEADD(DAY, ISNULL(sa.nu_diasextensao, 0), sa.dt_encerramento) AS dt_encerramentoextensao,
  al.dt_inicio,

  st_statusdisciplina = (CASE
    WHEN al.id_alocacao IS NULL AND md.id_situacao != 65 THEN 'Não Iniciada'
    WHEN ea.id_alocacao IS NOT NULL AND md.id_evolucao = 12 AND mn.bl_aprovado = 1 THEN 'Aprovada'
    WHEN ea.id_alocacao IS NOT NULL AND mn.bl_aprovado = 0 AND md.id_evolucao = 19 THEN 'Reprovado'
    WHEN md.id_aproveitamento IS NOT NULL AND md.id_situacao = 65 THEN 'Isento'
    WHEN al.id_alocacao IS NOT NULL AND md.id_aproveitamento IS NULL
      AND (
        md.nu_aprovafinal IS NULL
        OR ea.id_alocacao IS NULL
      )
      AND (
        (
          md.id_situacao = 13
          AND (
            mn.st_notaead IS NULL
            OR mn.st_notaatividade IS NULL
            OR mn.st_notafinal IS NULL
            AND mn.st_notarecuperacao IS NULL
          )
        )
        OR (
          md.id_situacao != 13
          AND ea.id_alocacao IS NULL
        )
      )
    THEN 'Cursando'
    ELSE '-'
  END),

  mn.id_tiponotarecuperacao,
  mn.st_notarecuperacao,
  mn.st_avaliacaoatividade,
  mn.st_avaliacaoead,
  mn.st_avaliacaofinal,
  mn.st_avaliacaorecuperacao,
  vwmodd.id_modulo,
  vwmodd.st_modulo,
  pp.nu_notamaxima,
  nf.nu_notafaltante,
  mn.bl_calcular AS bl_mostratcc

FROM
  dbo.tb_matriculadisciplina AS md
  JOIN dbo.tb_matricula AS mt ON md.id_matricula = mt.id_matricula
  JOIN dbo.tb_disciplina AS dc ON dc.id_disciplina = md.id_disciplina AND dc.id_tipodisciplina <> 3
  JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
  LEFT JOIN dbo.tb_alocacao AS al ON al.id_matriculadisciplina = md.id_matriculadisciplina AND al.bl_ativo = 1
  LEFT JOIN dbo.tb_avaliacaoconjuntoreferencia AS acr ON acr.id_saladeaula = al.id_saladeaula --AND acr.dt_fim IS NOT null
  LEFT JOIN tb_encerramentoalocacao AS ea ON ea.id_alocacao = al.id_alocacao
  LEFT JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula
  JOIN tb_evolucao AS ev ON ev.id_evolucao = md.id_evolucao
  JOIN dbo.tb_situacao AS st ON st.id_situacao = md.id_situacao
  LEFT JOIN dbo.vw_matriculanota AS mn ON md.id_matriculadisciplina = mn.id_matriculadisciplina AND mn.id_alocacao = al.id_alocacao

  OUTER APPLY (
    SELECT
      id_matricula,
      id_disciplina,
      SUM(CAST(nu_notamax AS FLOAT)) AS nu_notafaltante

      FROM
        vw_avaliacaoaluno_simples AS avs

      WHERE
        bl_ativo IS NULL
        AND avs.id_matricula = mt.id_matricula
        AND avs.id_disciplina = md.id_disciplina
        AND avs.id_tipoprova = 2
        AND avs.id_avaliacaorecupera = 0
        AND avs.id_saladeaula = al.id_saladeaula

      GROUP BY
        id_disciplina,
        id_matricula
  ) AS nf

  JOIN tb_matricula AS matcompl ON matcompl.id_matricula = md.id_matriculaoriginal
  JOIN tb_projetopedagogico AS ppcompl ON ppcompl.id_projetopedagogico = matcompl.id_projetopedagogico
  LEFT JOIN dbo.tb_aproveitamento AS ap ON ap.id_aproveitamento = md.id_aproveitamento
  LEFT JOIN tb_usuario AS u ON u.id_usuario = ap.id_usuario
  LEFT JOIN vw_modulodisciplina AS vwmodd ON vwmodd.id_disciplina = dc.id_disciplina AND vwmodd.id_projetopedagogico = mt.id_projetopedagogico AND vwmodd.bl_ativo = 1
  -- LEFT JOIN tb_modulodisciplina AS modd ON modd.id_disciplina = dc.id_disciplina AND modd.bl_ativo = 1

GO

ALTER VIEW [dbo].[vw_matriculanota]
AS
SELECT
  mt.id_matricula,
  md.id_matriculadisciplina,
  al.id_saladeaula,
  al.id_alocacao,
  ea.id_encerramentosala,
  -- atividade = 1 - Atividade Presencial
  atividade.id_avaliacao AS id_avaliacaoatividade,
	atividade.id_disciplina AS id_disciplinaatividade,
  atividade.st_nota AS st_notaatividade,
  atividade.id_tiponota AS id_tiponotaatividade,
  atividade.st_avaliacao AS st_avaliacaoatividade,
  atividade.nu_notamax AS nu_notamaxatividade,
  -- recuperacao = 2
  recuperacao.id_avaliacao AS id_avaliacaorecuperacao,
	recuperacao.id_disciplina AS id_disciplinarecuperacao,
  recuperacao.st_nota AS st_notarecuperacao,
  recuperacao.id_tiponota AS id_tiponotarecuperacao,
  recuperacao.st_avaliacao AS st_avaliacaorecuperacao,
  recuperacao.nu_notamax AS nu_notamaxrecuperacao,
  -- FINAL = 4
  final.id_avaliacao AS id_avaliacaofinal,
  final.id_disciplina AS id_disciplinafinal,
  final.st_tituloexibicaodisciplina AS st_disciplinafinal,
  final.st_nota AS st_notafinal,
  final.id_tipoavaliacao,
  final.id_tiponota AS id_tiponotafinal,
  final.st_avaliacao AS st_avaliacaofinal,
  final.nu_notamax AS nu_notamaxfinal,
  -- EAD = 5
  ead.id_avaliacao AS id_avaliacaoead,
  ead.id_disciplina AS id_disciplinaead,
  ead.st_nota AS st_notaead,
  ead.id_tiponota AS id_tiponotaead,
  ead.st_avaliacao AS st_avaliacaoead,
  ead.nu_notamax AS nu_notamaxead,
  ead.id_avaliacaoaluno,
  --TCC = 6
  tcc.id_avaliacao AS id_avaliacaotcc,
  tcc.id_disciplina AS id_disciplinatcc,
  tcc.st_nota AS st_notatcc,
  tcc.id_tiponota AS id_tiponotatcc,
	tcc.st_avaliacao AS st_avaliacaotcc,
  tcc.nu_notamax AS nu_notamaxtcc,

  /*(ISNULL(CAST(final.st_nota AS INT), 0)
  + ISNULL(CAST(atividade.st_nota AS int), 0)
  + ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
  + ISNULL(CAST(ead.st_nota AS INT), 0)) AS nu_notatotal,

  */
  nu_notatotal = CASE WHEN recuperacao.st_nota is NOT NULL AND recuperacao.id_tipocalculoavaliacao = 2
							THEN ( (ISNULL(CAST(final.st_nota AS INT), 0)
						   + ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
						   + ISNULL(CAST(atividade.st_nota AS int), 0)
						   + ISNULL(CAST(recuperacao.st_nota AS INT), 0)
						   + ISNULL(CAST(ead.st_nota AS INT), 0))/2  )
						  WHEN  recuperacao.st_nota is NOT NULL
							THEN (ISNULL(CAST(final.st_nota AS INT), 0)
								+ ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
								+ ISNULL(CAST(atividade.st_nota AS int), 0)
								+ ISNULL(CAST(recuperacao.st_nota AS INT), 0)
								+ ISNULL(CAST(ead.st_nota AS INT), 0))
						  ELSE
							   (ISNULL(CAST(final.st_nota AS INT), 0)
									+ ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
									+ ISNULL(CAST(atividade.st_nota AS int), 0)
									+ ISNULL(CAST(ead.st_nota AS INT), 0))

				 END,
	CAST(((pp.nu_notamaxima * pp.nu_percentualaprovacao) / 100) AS INT) nu_notaaprovacao,

  bl_calcular =
               CASE
                 WHEN ead.id_avaliacao IS NOT NULL AND ead.st_nota IS NULL THEN 0
                 WHEN atividade.id_avaliacao IS NOT NULL AND atividade.st_nota IS NULL THEN 0
                 WHEN final.id_avaliacao IS NOT NULL AND final.st_nota IS NULL THEN 0
                 WHEN tcc.id_avaliacao IS NOT NULL AND tcc.dt_defesa IS NULL THEN 0
                 ELSE 1
               END,
  bl_aprovado =
               CASE
                 WHEN recuperacao.st_nota is null AND (ISNULL(CAST(final.st_nota AS INT), 0)
                   + ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
                   + ISNULL(CAST(atividade.st_nota AS int), 0)
                   + ISNULL(CAST(ead.st_nota AS INT), 0)) >= CAST(((pp.nu_notamaxima
                   * pp.nu_percentualaprovacao)
                   / 100) AS INT) THEN 1
                 WHEN  recuperacao.st_nota is null AND (ISNULL(CAST(final.st_nota AS INT), 0)
                   + ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
                   + ISNULL(CAST(atividade.st_nota AS int), 0)
                   + ISNULL(CAST(ead.st_nota AS INT), 0)) < CAST(((pp.nu_notamaxima
                   * pp.nu_percentualaprovacao)
                   / 100) AS INT) THEN 0
								 WHEN recuperacao.st_nota is NOT NULL AND recuperacao.id_tipocalculoavaliacao = 2
									 AND ( (ISNULL(CAST(final.st_nota AS INT), 0)
										 + ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
										 + ISNULL(CAST(atividade.st_nota AS int), 0)
										 + ISNULL(CAST(recuperacao.st_nota AS INT), 0)
										 + ISNULL(CAST(ead.st_nota AS INT), 0))/2  ) >= CAST(((pp.nu_notamaxima * pp.nu_percentualaprovacaovariavel)
										 / 100) AS INT) THEN 1
									WHEN  recuperacao.st_nota is NOT NULL AND (ISNULL(CAST(final.st_nota AS INT), 0)
										+ ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
										+ ISNULL(CAST(atividade.st_nota AS int), 0)
										+ ISNULL(CAST(recuperacao.st_nota AS INT), 0)
										+ ISNULL(CAST(ead.st_nota AS INT), 0)) < CAST(((pp.nu_notamaxima * pp.nu_percentualaprovacaovariavel )
										/ 100) AS INT) THEN 0
									ELSE 0
               END,
	(CASE
		-- Se tiver RECUPERACAO e o tipo de calculo for MEDIA VARIAVEL, somar todas as notas e dividir por 2
		WHEN recuperacao.st_nota IS NOT NULL AND recuperacao.id_tipocalculoavaliacao = 2 THEN (
			( ISNULL(CAST(final.st_nota AS INT), 0)
				+ ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
				+ ISNULL(CAST(ead.st_nota AS INT), 0)
				+ ISNULL(CAST(atividade.st_nota AS INT), 0)
				+ ISNULL(CAST(recuperacao.st_nota AS INT), 0)
			) / 2
		) ELSE (
			( ISNULL(CAST(final.st_nota AS INT), 0)
				+ ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
				+ ISNULL(CAST(ead.st_nota AS INT), 0)
				+ ISNULL(CAST( (CASE WHEN recuperacao.st_nota IS NOT NULL THEN recuperacao.st_nota ELSE atividade.st_nota END) AS INT), 0)
			)
		)
	END)
	-- trata porque esse valor pode estar salvo como 0
	/ (CASE WHEN pp.nu_notamaxima IS NULL OR pp.nu_notamaxima = 0 THEN 1 ELSE pp.nu_notamaxima END) * 100
	AS nu_percentualfinal

				FROM tb_matriculadisciplina AS md
				JOIN tb_matricula AS mt
				  ON md.id_matricula = mt.id_matricula
				JOIN dbo.tb_projetopedagogico AS pp
				  ON mt.id_projetopedagogico = pp.id_projetopedagogico
				JOIN dbo.tb_alocacao AS al
				  ON al.id_matriculadisciplina = md.id_matriculadisciplina
				  AND al.bl_ativo = 1
				LEFT JOIN dbo.vw_avaliacaoaluno_simples AS ead
				  ON ead.id_matriculadisciplina = md.id_matriculadisciplina
				  AND al.id_saladeaula = ead.id_saladeaula
				  AND ead.id_tipoavaliacao = 5
				LEFT JOIN dbo.vw_avaliacaoaluno_simples AS atividade
				  ON atividade.id_matriculadisciplina = md.id_matriculadisciplina
				  AND al.id_saladeaula = atividade.id_saladeaula
				  AND atividade.id_tipoavaliacao = 1
				LEFT JOIN tb_encerramentoalocacao AS ea ON ea.id_alocacao = al.id_alocacao

				OUTER APPLY (SELECT TOP 1
							  aaf.st_nota,
							  aaf.id_avaliacao,
							  aaf.id_disciplina,
							  aaf.st_tituloexibicaodisciplina,
							  aaf.id_tipoavaliacao
							  , aaf.id_tiponota
							  , aaf.st_avaliacao
							  ,aaf.nu_notamax
							FROM dbo.vw_avaliacaoaluno_simples AS aaf
							WHERE md.id_matriculadisciplina = aaf.id_matriculadisciplina
							AND aaf.id_tipoavaliacao = 4
							ORDER BY cast(aaf.st_nota as NUMERIC) DESC) AS final
				LEFT JOIN dbo.vw_avaliacaoaluno_simples AS tcc
				  ON tcc.id_matriculadisciplina = md.id_matriculadisciplina
				  AND al.id_saladeaula = tcc.id_saladeaula
				  AND tcc.id_tipoavaliacao = 6

				  --add recuperacao
					  OUTER APPLY (
					    SELECT TOP 1
							  rec.st_nota,
							  rec.id_avaliacao,
							  rec.id_disciplina,
							  rec.st_tituloexibicaodisciplina,
							  rec.id_tipoavaliacao,
							  rec.id_tiponota,
							  rec.st_avaliacao,
							  rec.id_tipocalculoavaliacao,
							  rec.nu_notamax
							FROM dbo.vw_avaliacaoaluno_simples AS rec
							WHERE md.id_matriculadisciplina = rec.id_matriculadisciplina
							AND rec.id_tipoavaliacao = 2
							ORDER BY cast(rec.st_nota as NUMERIC) DESC) AS recuperacao

GO

ALTER VIEW [dbo].[vw_alunossala] AS
SELECT DISTINCT al.id_alocacao ,
al.id_saladeaula ,
mt.id_usuario,
mt.st_cpf,
mt.st_email,
mt.st_nomecompleto,
md.id_matricula ,
td.id_disciplina ,
td.id_tipodisciplina,
md.id_matriculadisciplina ,
st_nota,
lg.id_logacesso,
si.id_sistema,
sa.id_entidade,
mt.id_projetopedagogico,
mt.st_projetopedagogico,
mt.st_evolucao,
al.id_situacaotcc,
aa.id_upload,
tu1.id_usuario AS id_professor,
tu1.st_nomecompleto AS st_professor,
eprof.id_email AS id_emailprofessor,
eprof.st_email AS st_emailprofessor,
mt.st_urlacesso,
mt.id_entidadematricula as id_entidadematricula,
al.dt_inicio,
DATEADD(DAY,ISNULL(sa.nu_diasaluno,0), al.dt_inicio) as dt_termino,
sa.id_categoriasala
FROM dbo.tb_alocacao al
JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula
JOIN dbo.tb_matriculadisciplina AS md ON md.id_matriculadisciplina = al.id_matriculadisciplina
JOIN dbo.tb_disciplina AS td ON td.id_disciplina = md.id_disciplina
JOIN dbo.vw_matricula AS mt ON mt.id_matricula = md.id_matricula AND mt.bl_ativo = 1
OUTER APPLY (SELECT TOP 1 id_logacesso FROM dbo.tb_logacesso WHERE id_saladeaula = al.id_saladeaula AND id_usuario = mt.id_usuario ) AS lg
LEFT JOIN  dbo.vw_avaliacaoaluno AS aa ON aa.id_saladeaula = al.id_saladeaula AND aa.id_matricula = mt.id_matricula AND aa.bl_ativo = 1 AND aa.id_tipoavaliacao =6
LEFT JOIN dbo.tb_saladeaulaintegracao AS si ON si.id_saladeaula = al.id_saladeaula
LEFT JOIN tb_usuarioperfilentidadereferencia tu ON al.id_saladeaula = tu.id_saladeaula AND tu.bl_titular = 1 AND tu.bl_ativo = 1
LEFT JOIN tb_usuario tu1 ON tu.id_usuario = tu1.id_usuario
LEFT JOIN tb_contatosemailpessoaperfil AS conte ON conte.id_usuario=tu1.id_usuario AND conte.bl_ativo=1 AND conte.bl_padrao=1 AND tu.id_entidade = conte.id_entidade
LEFT JOIN tb_contatosemail AS eprof ON eprof.id_email=conte.id_email WHERE al.bl_ativo = 1
GO
CREATE view [rel].[vw_consultadisciplina] AS
SELECT DISTINCT mod.id_projetopedagogico, proped.st_projetopedagogico, dis.id_disciplina,
   dis.st_disciplina, dis.st_tituloexibicao, dis. nu_cargahoraria, forarq.id_formatoarquivo,
   forarq.st_formatoarquivo, discon.st_descricaoconteudo, discon.id_disciplinaconteudo, discon.dt_cadastro
FROM tb_disciplina AS dis
JOIN tb_modulodisciplina moddis ON moddis.id_disciplina = dis.id_disciplina
JOIN tb_modulo mod ON mod.id_modulo = moddis.id_modulo
JOIN tb_projetopedagogico proped ON proped.id_projetopedagogico = mod.id_projetopedagogico
JOIN tb_disciplinaconteudo discon ON discon.id_disciplina = dis.id_disciplina
JOIN tb_formatoarquivo forarq ON forarq.id_formatoarquivo = discon.id_formatoarquivo
WHERE dis.bl_ativa = 1

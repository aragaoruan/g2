BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_permissao on
INSERT INTO tb_permissao(
  id_permissao,
  st_nomepermissao,
  st_descricao,
  bl_ativo)
values(
  34,
  'Alterar nota Concluintes e Certificados',
  'Permite ao usuário alterar as notas dos alunos conluintes e certificados.',
  1)
SET IDENTITY_INSERT dbo.tb_permissao off
COMMIT


INSERT INTO tb_permissaofuncionalidade(
  id_funcionalidade,
  id_permissao)
VALUES(
  272,--Gerência Notas
  34);--Alterar nota Concluintes e Certificados
  go
  SET IDENTITY_INSERT tb_funcionalidade ON;
INSERT into tb_funcionalidade
(id_funcionalidade, st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao,
st_classeflex, st_urlicone, id_tipofuncionalidade, nu_ordem, st_classeflexbotao,
bl_pesquisa, bl_lista, id_sistema, st_ajuda, st_target, st_urlcaminho, bl_visivel, bl_relatorio
, st_urlcaminhoedicao, bl_delete)
VALUES
  (777, 'Meus Documentos', null, 1, 182, 'app.meusdocumentos', 'icon-icon_disc', 1, 1, null, 1, 1, 4, null, null, 'app.meusdocumentos', 1, 0, null, 0);
SET IDENTITY_INSERT tb_funcionalidade OFF;

go

SET IDENTITY_INSERT tb_funcionalidade ON;

INSERT into tb_funcionalidade
(id_funcionalidade, st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao,
st_classeflex, st_urlicone, id_tipofuncionalidade, nu_ordem, st_classeflexbotao,
bl_pesquisa, bl_lista, id_sistema, st_ajuda, st_target, st_urlcaminho, bl_visivel, bl_relatorio
, st_urlcaminhoedicao, bl_delete)
VALUES
  (776, 'Primeiro Acesso', null, 1, 182, 'app.primeiro-acesso', 'icon-icon_menu icon-icon_mens', 1, 1, null, 1, 1, 4, null, null, 'app.primeiro-acesso', 1, 0, null, 0);

SET IDENTITY_INSERT tb_funcionalidade OFF;

GO

ALTER TABLE tb_logacesso ADD st_parametros TEXT NULL;
ALTER TABLE tb_logacesso ADD id_matricula INT NULL;

set IDENTITY_INSERT tb_funcionalidade on

--Meus Dados
insert into tb_funcionalidade(id_funcionalidade,
                              st_funcionalidade,
                              id_funcionalidadepai,
                              bl_ativo, id_situacao,
                              st_classeflex, st_urlicone,
                              id_tipofuncionalidade, nu_ordem,
                              st_classeflexbotao, bl_pesquisa, bl_lista,
                              id_sistema, st_ajuda, st_target, st_urlcaminho,
                              bl_visivel, bl_relatorio, st_urlcaminhoedicao,
                              bl_delete)
    VALUES (771,'Meus Dados', null, 1, 182, 'app.meusdados', NULL, 1, NULL , NULL, 0, 0, 4,'', NULL, 'app.meusdados', '1', 0, null, 0)

-- Curso
insert into tb_funcionalidade(id_funcionalidade,
                              st_funcionalidade,
                              id_funcionalidadepai,
                              bl_ativo, id_situacao,
                              st_classeflex, st_urlicone,
                              id_tipofuncionalidade, nu_ordem,
                              st_classeflexbotao, bl_pesquisa, bl_lista,
                              id_sistema, st_ajuda, st_target, st_urlcaminho,
                              bl_visivel, bl_relatorio, st_urlcaminhoedicao,
                              bl_delete)
    VALUES (772,'Curso', null, 1, 182, 'app.curso', NULL, 1, NULL , NULL, 0, 0, 4,'', NULL, 'app.curso', '1', 0, null, 0)

--Disciplina
insert into tb_funcionalidade(id_funcionalidade,
                              st_funcionalidade,
                              id_funcionalidadepai,
                              bl_ativo, id_situacao,
                              st_classeflex, st_urlicone,
                              id_tipofuncionalidade, nu_ordem,
                              st_classeflexbotao, bl_pesquisa, bl_lista,
                              id_sistema, st_ajuda, st_target, st_urlcaminho,
                              bl_visivel, bl_relatorio, st_urlcaminhoedicao,
                              bl_delete)
    VALUES (773,'Disciplina', null, 1, 182, 'app.disciplina', NULL, 1, NULL , NULL, 0, 0, 4,'', NULL, 'app.disciplina', '1', 0, null, 0)


--Novo Atendimento
insert into tb_funcionalidade(id_funcionalidade,
                              st_funcionalidade,
                              id_funcionalidadepai,
                              bl_ativo, id_situacao,
                              st_classeflex, st_urlicone,
                              id_tipofuncionalidade, nu_ordem,
                              st_classeflexbotao, bl_pesquisa, bl_lista,
                              id_sistema, st_ajuda, st_target, st_urlcaminho,
                              bl_visivel, bl_relatorio, st_urlcaminhoedicao,
                              bl_delete)
    VALUES (774,'Novo Atendimento', 768, 1, 182, 'app.novo-atendimento', NULL, 1, NULL , NULL, 0, 0, 4,'', NULL, 'app.novo-atendimento', '1', 0, null, 0)

set IDENTITY_INSERT tb_funcionalidade off

go


ALTER TABLE tb_projetopedagogico ADD st_imagem VARCHAR(255) NULL;
go
SET IDENTITY_INSERT tb_funcionalidade ON;
INSERT INTO tb_funcionalidade (id_funcionalidade, st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao, st_classeflex, st_urlicone, id_tipofuncionalidade, st_classeflexbotao, bl_pesquisa, bl_lista, id_sistema, st_urlcaminho, bl_visivel, bl_relatorio, bl_delete)
VALUES(723, 'Extrato Pagamento', 313, 1, 123, '/secretaria/extrato-pagamento', '/imagens/icones/semfoto.png', 1, 'ajax', 0, 0, 4, '/secretaria/extrato-pagamento', 1, 0, 0);
SET IDENTITY_INSERT tb_funcionalidade OFF;
GO

--SCRIPT PARA ADICIONAR NOVA COLUNA "bl_possuiprova" NA TABLE "tb_projetopedagogico"
ALTER TABLE tb_projetopedagogico ADD bl_possuiprova BIT DEFAULT ((0)) NOT NULL;

--------------------------------------------------------------------------------

--DESATIVAR IDENTITY TABELA FIN-16826
SET IDENTITY_INSERT tb_permissao ON;

-- INSERE A PERMISSÃO 'Possui Prova Montada'
INSERT INTO tb_permissao (id_permissao, st_nomepermissao, st_descricao, bl_ativo)
VALUES (38, 'Possui Prova Montada', 'Permite ao usuario visualizar a opcao -Possui prova montada-', 1) ;

--INSERE O NUMERO DA FUNCIONALIDADE E O NUMERO DA PERMISSAO
INSERT tb_permissaofuncionalidade VALUES (19, 38);

--ATIVAR IDENTITY TABELA
SET IDENTITY_INSERT tb_permissao OFF

go

/****************
Add field st_salareferencia
Varchar (255)
*********************/


ALTER TABLE tb_disciplinaintegracao
 ADD st_salareferencia VARCHAR(255) NULL


/****************
Add field id_disciplinaintegracao
INT - NULL
*********************/
ALTER TABLE tb_saladeaulaintegracao
 ADD id_disciplinaintegracao INT NULL


GO

BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_permissao on
insert into tb_permissao (id_permissao,st_nomepermissao, st_descricao, bl_ativo) VALUES (35,'Titulação', 'Permite alterar a titulação no cadastro de usuário', 1);
SET IDENTITY_INSERT dbo.tb_permissao off
commit
insert into tb_permissaofuncionalidade (id_funcionalidade, id_permissao) VALUES (8, 35);

GO
ALTER TABLE tb_transacaofinanceira ADD
            id_statustransacao INT,
            st_codigoautorizacao VARCHAR(100),
            nu_valorenviado MONEY,
            nu_valorautorizado MONEY,
            nu_valorrecusado MONEY,
            st_meiopagamento VARCHAR(50),
            st_urlboleto VARCHAR(250),
            st_boletocoditobarras VARCHAR(250),
            st_urlpostback VARCHAR(250),
            dt_vencimento DATETIME,
            st_nsu varchar(100),
            id_transacaoexterna varchar(150)

        ALTER TABLE tb_lancamento ADD
            st_urlboleto VARCHAR(250),
            st_codigobarras VARCHAR(250),
            id_transacaoexterna varchar(150)


        INSERT tb_situacao (id_situacao, st_situacao, st_tabela, st_campo, st_descricaosituacao) VALUES
            (187, 'processing', 'tb_transacaofinanceira', 'id_statustransacao', 'transação sendo processada.'),
            (188, 'authorized', 'tb_transacaofinanceira', 'id_statustransacao', 'transação autorizada. Cliente possui saldo na conta e este valor foi reservado para futura captura, deve acontecer em 5 dias. Caso a transação não seja capturada, a mesma é cancelada automaticamente.'),
            (189, 'paid', 'tb_transacaofinanceira', 'id_statustransacao', 'transação paga (autorizada e capturada).'),
            (190, 'refunded', 'tb_transacaofinanceira', 'id_statustransacao', 'transação paga (autorizada e capturada).'),
            (191, 'waiting_payment', 'tb_transacaofinanceira', 'id_statustransacao', 'transação estornada.'),
            (192, 'pending_refund', 'tb_transacaofinanceira', 'id_statustransacao', 'transação paga com boleto aguardando para ser estornada.'),
            (193, 'refused', 'tb_transacaofinanceira', 'id_statustransacao', 'transação não autorizada.'),
    (194, 'chargedback', 'tb_transacaofinanceira', 'id_statustransacao', 'transação sofreu chargeback.')

GO
-- GII-6868

ALTER TABLE tb_matricula ADD dt_aptoagendamento DATETIME NULL;
ALTER TABLE tb_matriculadisciplina ADD dt_aptoagendamento DATETIME NULL;
GO
 /*****
	INSERT na tb_processo para salvar o rob� de importa��o
	de sala de aula do moodle a partir de uma sala de refer�ncia
 ****/
 
 INSERT INTO tb_processo 
		 (id_processo, st_processo, st_classe)
		 VALUES
		 (4, 'Importar Salas Moodle de Sala Referência' , null)



/**
	Indexes para tabela tb_erro
**/
CREATE NONCLUSTERED INDEX [idx_sistema_processo] ON [dbo].[tb_erro]
(
        [id_sistema] ASC,
        [id_processo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

GO



insert into tb_permissao (id_permissao, st_nomepermissao, st_descricao, bl_ativo) values (39,'Confirmar Recebimento','Permite confirmar o recebimento na tela de negociação padrão',1);
INSERT  INTO tb_permissaofuncionalidade (id_funcionalidade, id_permissao) VALUES (627, 39);

GO

create table tb_recebedor(
  id_recebedor int not null PRIMARY KEY IDENTITY,
  st_recebedor VARCHAR(255) NOT NULL,
  id_recebedorexterno VARCHAR(255) NULL
);

alter table dbo.tb_entidadeintegracao ADD id_recebedor int null FOREIGN KEY REFERENCES tb_recebedor(id_recebedor);


update tb_sistema set st_sistema = 'Cartão' where id_sistema = 7
update tb_sistema set st_sistema = 'Cartão Recorrente' where id_sistema = 10

GO
-- GII-7161

-- bl_documentacao
ALTER TABLE tb_matricula DROP CONSTRAINT DF__tb_matric__bl_do__62E1DC45;
ALTER TABLE tb_matricula ALTER COLUMN bl_documentacao BIT NULL;
ALTER TABLE tb_matricula ADD CONSTRAINT DF__tb_matric__bl_do__62E1DC45 DEFAULT NULL FOR bl_documentacao;

-- bl_academico
ALTER TABLE tb_matricula DROP CONSTRAINT DF__tb_matric__bl_ac__5B60B60E;
ALTER TABLE tb_matricula ALTER COLUMN bl_academico BIT NULL;
ALTER TABLE tb_matricula ADD CONSTRAINT DF__tb_matric__bl_ac__5B60B60E DEFAULT NULL FOR bl_academico;

-- UPDATE
UPDATE tb_matricula SET bl_documentacao = NULL WHERE bl_documentacao = 0;
UPDATE tb_matricula SET bl_academico = NULL WHERE bl_academico = 0;

GO
insert into tb_formapagamentoaplicacao (id_formapagamentoaplicacao, st_formapagamentoaplicacao) VALUES (6,'Venda Automática');
go
ALTER TABLE tb_projetopedagogico
ADD st_boasvindascurso VARCHAR(120);

ALTER TABLE tb_pessoa
ADD st_descricaocoordenador VARCHAR(160);

-- PERMISSÕES

SET IDENTITY_INSERT tb_funcionalidade ON
INSERT INTO tb_funcionalidade (id_funcionalidade, st_urlcaminho, id_funcionalidadepai, st_funcionalidade,
bl_ativo, id_situacao, id_tipofuncionalidade, bl_pesquisa, bl_lista, bl_visivel, bl_relatorio, bl_delete, id_sistema)
VALUES (
769, -- id_funcionalidade: Novo ID da funcionalidade. Quem roda a SQL em produção fica com a responsabilidade de adicionar um novo ID manualmente.
'769', -- st_urlcaminho: Geralmente, a url da funcionalidade que será exibida depois do #/. Aqui é apenas uma string com o id da funcionalidade, visto que nesse caso, estamos adicionando uma funcionalidade interna.
8, -- id_funcionalidadepai: A funcionalidade 'superior' na hierarquia do menu.
'Cadastro Descrição do Coordenador', -- st_funcionalidade: descrição da funcionalidade.
1, -- bl_ativo: funcionalidade ativa
123, -- id_situacao: funcionalidade em HTML
7, -- id_tipofuncionalidade: funcionalidade interna.
0, -- bl_pesquisa
0, -- bl_lista
0, -- bl_visivel
0, -- bl_relatorio
0, -- bl_delete
1); --id_sistema: 1 (G2S)
SET IDENTITY_INSERT tb_funcionalidade OFF
 
INSERT INTO tb_permissao (st_nomepermissao, st_descricao, bl_ativo) VALUES (
'Cadastro Descrição do Coordenador',
'Permite cadastrar a descrição do Coordenador de Curso na tela Pessoa / Cadastros / Usuários. A descrição é exibida na tela de boas vindas do novo portal.',
1
);

INSERT INTO tb_permissaofuncionalidade (id_funcionalidade, id_permissao) VALUES (
769,
36
);
GO
--DESATIVAR IDENTITY TABELA
SET IDENTITY_INSERT tb_funcionalidade ON;
SET IDENTITY_INSERT tb_situacao ON;

INSERT tb_situacao (id_situacao, st_situacao, st_tabela, st_campo, st_descricaosituacao) VALUES(182, 'Novo Portal', 'tb_funcionalidade', 'id_situacao', 'Funcionalidade Novo Portal');

INSERT into tb_funcionalidade
(id_funcionalidade, st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao,
st_classeflex, st_urlicone, id_tipofuncionalidade, nu_ordem, st_classeflexbotao,
bl_pesquisa, bl_lista, id_sistema, st_ajuda, st_target, st_urlcaminho, bl_visivel, bl_relatorio
, st_urlcaminhoedicao, bl_delete)
VALUES
  (764, 'Mensagens', null, 1, 182, 'app.mensagens', 'icon-icon_menu icon-icon_mens', 1, 1, null, 1, 1, 4, null, null, 'app.mensagens', 1, 0, null, 0),
  (765, 'Cursos', null, 2, 182, 'login.selecionar-curso', 'icon-icon_disc', 1, 1, null, 1, 1, 4, null, null, 'login.selecionar-curso', 1, 0, null, 0),
  (766, 'Calendário', null, 3, 182, 'app.calendario', 'icon-icon_disc', 1, 1, null, 1, 1, 4, null, null, 'app.calendario', 1, 0, null, 0),
  (767, 'Financeiro', null, 4, 182, 'app.financeiro', 'icon-icon_financa', 1, 1, null, 1, 1, 4, null, null, 'app.financeiro', 1, 0, null, 0),
  (768, 'Atendimento', null, 5, 182, 'app.chamados', 'icon-icon_atend', 1, 1, null, 1, 1, 4, null, null, 'app.chamados', 1, 0, null, 0);

INSERT tb_entidadefuncionalidade VALUES
  (14, 764, 1, 5, NULL, 0, 1, 1),
  (14, 765, 1, 5, NULL, 0, 1, 2),
  (14, 766, 1, 5, NULL, 0, 1, 3),
  (14, 767, 1, 5, NULL, 0, 1, 4),
  (14, 768, 1, 5, NULL, 0, 1, 5);


INSERT tb_perfilfuncionalidade VALUES
  (75, 764, 1),
  (75, 765, 1),
  (75, 766, 1),
  (75, 767, 1),
  (75, 768, 1);

--ATIVAR IDENTITY TABELA
SET IDENTITY_INSERT tb_funcionalidade OFF;
SET IDENTITY_INSERT tb_situacao OFF;
GO
ALTER TABLE tb_entidadeintegracao
ADD st_codchavewsuny varchar(8000) NULL  

GO
-- OBS.: Em produção, verificar o último id_funcionalidade cadastrado e alterar.

SET IDENTITY_INSERT tb_funcionalidade ON

INSERT INTO tb_funcionalidade
(
id_funcionalidade,
st_funcionalidade,
bl_ativo,
st_classeflex,
id_situacao,
st_urlicone,
id_tipofuncionalidade,
nu_ordem,
bl_pesquisa,
bl_lista,
id_sistema,
st_urlcaminho,
bl_visivel,
bl_relatorio,
bl_delete
)
VALUES
(770,
'Grade de Notas',
1,
'app.grade-notas',
182,
'icon-icon_nota',
1,
1,
1,
1,
4,
'app.grade-notas',
1,
0,
0
);

SET IDENTITY_INSERT tb_funcionalidade OFF

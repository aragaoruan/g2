﻿BEGIN TRAN


      CREATE TABLE dbo.tb_formadisponibilizacao (
          id_formadisponibilizacao INT IDENTITY(1,1) PRIMARY KEY,
          st_formadisponibilizacao VARCHAR(250) NOT NULL,
          bl_ativo INT NOT NULL DEFAULT(1),
          dt_cadastro DATETIME2 DEFAULT CURRENT_TIMESTAMP
      )

      DROP TABLE tb_produtotextosistema

      CREATE TABLE dbo.tb_produtotextosistema (
          id_produtotextosistema INT IDENTITY(1,1) PRIMARY KEY,
          id_textosistema INT NULL,
          id_entidade INT NULL,
          id_produto INT NULL,
          id_upload INT NULL,
          id_formadisponibilizacao INT NULL
      )

      SET IDENTITY_INSERT dbo.tb_formadisponibilizacao ON;
            INSERT INTO dbo.tb_formadisponibilizacao
                  (
                    id_formadisponibilizacao,
                    st_formadisponibilizacao,
                    bl_ativo ,
                    dt_cadastro
                  ) VALUES (
                  1,
                  'Envio pelos Correios' , -- st_formadisponibilizacao - varchar(250)
                  1 , -- bl_ativo - int
                    GETDATE()  -- dt_cadastro - datetime2
                  );
            INSERT INTO dbo.tb_formadisponibilizacao
                  (
                    id_formadisponibilizacao,
                    st_formadisponibilizacao ,
                    bl_ativo ,
                    dt_cadastro
                  ) VALUES (
                    2,
                    'Busca na Instituição' , -- st_formadisponibilizacao - varchar(250)
                    1 , -- bl_ativo - int
                    GETDATE()  -- dt_cadastro - datetime2
                  );
            INSERT INTO dbo.tb_formadisponibilizacao
                  (
                    id_formadisponibilizacao,
                    st_formadisponibilizacao ,
                    bl_ativo ,
                    dt_cadastro
                  ) VALUES (
                    3,
                    'Emissão Online' , -- st_formadisponibilizacao - varchar(250)
                    1 , -- bl_ativo - int
                    GETDATE()  -- dt_cadastro - datetime2
                  )
      SET IDENTITY_INSERT dbo.tb_formadisponibilizacao Off;

	  -- AC-26962

-- CRIANDO UMA FUNCIONALIDADE PARA PODER CRIAR AS PERMISSOES
SET IDENTITY_INSERT tb_funcionalidade ON;
INSERT INTO tb_funcionalidade (id_funcionalidade, st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao, st_classeflex, st_urlicone, id_tipofuncionalidade, nu_ordem, st_classeflexbotao, bl_pesquisa, bl_lista, id_sistema, st_ajuda, st_target, st_urlcaminho, bl_visivel, bl_relatorio, st_urlcaminhoedicao, bl_delete)
VALUES (701, 'Matrículas (Evolução)', '157', '1', '123', '', '', '7',  NULL,  NULL, '0', '0', '1', '',  NULL, '701', '1', '0',  NULL, '0');
SET IDENTITY_INSERT tb_funcionalidade OFF;


-- CRIANDO AS NOVAS PERMISSOES
SET IDENTITY_INSERT tb_permissao ON;
INSERT INTO tb_permissao (id_permissao, st_nomepermissao, st_descricao, bl_ativo) VALUES
(14, 'Bloquear', 'Permitir alterar evolução da matrícula para Bloquear', 1),
(15, 'Trancar', 'Permitir alterar evolução da matrícula para Trancar', 1),
(16, 'Cancelar', 'Permitir alterar evolução da matrícula para Cancelar', 1),
(17, 'Anular', 'Permitir alterar evolução da matrícula para Anular', 1),
(18, 'Transferir de Entidade', 'Permitir alterar evolução da matrícula para Transferido de Entidade', 1),
(19, 'Liberar Acesso', 'Permitir alterar evolução da matrícula para Liberar Acesso', 1);
SET IDENTITY_INSERT tb_permissao OFF;


-- ATRIBUINDO AS PERMISSOES CRIADAS NA FUNCIONALIDADE
INSERT INTO tb_permissaofuncionalidade (id_funcionalidade, id_permissao) VALUES
(701, 14),
(701, 15),
(701, 16),
(701, 17),
(701, 18),
(701, 19);


-- INSERIR PERMISSOES NO PERFIL ADMINISTRADOR DA PLATAFORMA
INSERT INTO tb_perfilpermissaofuncionalidade (id_perfil, id_funcionalidade, id_permissao) VALUES 
(1, 701, 14),
(1, 701, 15),
(1, 701, 16),
(1, 701, 17),
(1, 701, 18),
(1, 701, 19);

ALTER TABLE tb_configuracaoentidade
ADD id_assuntoduplicidade INT NULL

SET IDENTITY_INSERT dbo.tb_funcionalidade ON;
INSERT INTO dbo.tb_funcionalidade (id_funcionalidade, st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao, st_classeflex, st_urlicone, id_tipofuncionalidade, nu_ordem, st_classeflexbotao, bl_pesquisa, bl_lista, id_sistema, st_ajuda, st_target, st_urlcaminho, bl_visivel, bl_relatorio, st_urlcaminhoedicao, bl_delete)
VALUES (702, 'Alunos Aptos para o Agendamento', 257, 1, 123, '', '/img/ico/report.png', 3, NULL, NULL, 0, 0, 1, NULL, NULL, '702', 1, 1, NULL, 0);
SET IDENTITY_INSERT dbo.tb_funcionalidade OFF;

/**
Alteraзгo na tabela de matricula add o campo bl_academico
**/

ALTER TABLE tb_matricula
ADD bl_academico bit NOT NULL DEFAULT(0)



	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Marca se o aluno estб apto, academicamente a ser certificado' 
	, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE'
	,@level1name=N'tb_matricula', @level2type=N'COLUMN',@level2name=N'bl_academico'
GO

SET IDENTITY_INSERT tb_funcionalidade ON

insert into tb_funcionalidade 
(id_funcionalidade, st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao,
st_classeflex, st_urlicone, id_tipofuncionalidade, nu_ordem, st_classeflexbotao,
bl_pesquisa, bl_lista, id_sistema, st_ajuda, st_target, st_urlcaminho, bl_visivel, bl_relatorio
, st_urlcaminhoedicao, bl_delete)
values 
(698, 'Relatório Resumo da Venda ', 488, 1, 123, 
'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa', null, 3, 0, null, 0, 0,1, null, null, 
'resumo-venda', 1, 0, null, 1)

--ATIVAR IDENTITY TABELA
SET IDENTITY_INSERT tb_funcionalidade OFF 

GO

CREATE table tb_formatoarquivo (
id_formatoarquivo int primary key,
st_formatoarquivo varchar(30),
st_extensaoarquivo varchar(10)
);

--Insert de tipos inicial para funcionalidade
INSERT INTO tb_formatoarquivo (id_formatoarquivo, st_formatoarquivo, st_extensaoarquivo)
VALUES (1, 'PDF', '.pdf'), (2, 'ZIP', '.zip');

create table tb_disciplinaconteudo (
id_disciplinaconteudo int primary key identity(1,1),
id_disciplina int,
st_descricaoconteudo varchar(100),
id_formatoarquivo int,
id_usuariocadastro int,
bl_ativo bit default 1,
dt_cadastro datetime default getdate(),
constraint FK_TB_DISCIPLINA_CONTEUDO_TB_DISCIPLINA foreign key (id_disciplina) references tb_disciplina(id_disciplina) on update cascade on delete no action,
	constraint FK_TB_DISCIPLINA_CONTEUDO_TB_USUARIO foreign key (id_usuariocadastro) references tb_usuario(id_usuario) on update no action on delete no action,
	constraint FK_TB_DISCIPLINA_CONTEUDO_TB_FORMATOARQUIVO foreign key (id_formatoarquivo) references tb_formatoarquivo(id_formatoarquivo) on update no action on delete no action
)
go


ALTER TABLE [dbo].[tb_areaconhecimento]
ADD [st_imagemarea] varchar(255) NULL
GO

ALTER TABLE [dbo].[tb_entidade]
ADD [st_urlimgapp] varchar(255) NULL
GO

alter table tb_enviodestinatario
add bl_entrega bit default 0

alter table tb_configuracaoentidade add bl_acessoapp bit not null default 0

SET IDENTITY_INSERT tb_permissao ON;
insert into tb_permissao (id_permissao,st_nomepermissao, st_descricao, bl_ativo) values (23,'Editar atendente da venda.', 'Permite editar o atendente responsável pela venda na tela de negociação.',1 )
SET IDENTITY_INSERT tb_permissao OFF;

insert into tb_permissaofuncionalidade (id_funcionalidade, id_permissao) VALUES (627,23)

insert into tb_perfilpermissaofuncionalidade(id_perfil, id_funcionalidade, id_permissao) values (3, 627, 23)
go

update tb_funcionalidade set st_urlcaminho = '/relatorio/provas-por-polo', bl_pesquisa=0 where id_funcionalidade = 591

--ROLLBACK
--commit




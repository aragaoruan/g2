
----------------------AC-27097-------------------------

CREATE VIEW [dbo].[vw_historicoavaliacaoaluno]
AS
  SELECT DISTINCT
    id_avaliacaoaluno,
    mt.id_matricula,
    us.st_nomecompleto,
    mdl.id_modulo,
    mdl.st_tituloexibicao AS st_tituloexibicaomodulo,
    dc.id_disciplina,
    dc.st_tituloexibicao AS st_tituloexibicaodisciplina,
    aa.st_nota,
    ag.id_avaliacaoagendamento,
    acr.id_avaliacao,
    av.st_avaliacao,
    md.id_situacao,
    st.st_situacao,
    ac.id_tipoprova,
    acrf.id_avaliacaoconjuntoreferencia,
    (case when avref.id_avaliacaoconjuntoreferencia=acrf.id_avaliacaoconjuntoreferencia then 1 else 0 end) bl_agendamento,
    md.id_evolucao,
    ev.st_evolucao,
    CAST(av.nu_valor AS varchar(100)) AS nu_notamax,
    ac.id_tipocalculoavaliacao,
    av.id_tipoavaliacao,
    ta.st_tipoavaliacao,
    (case when acr.id_avaliacaorecupera is null then 0 else acr.id_avaliacaorecupera end) as id_avaliacaorecupera,
    ag.bl_provaglobal,
    pp.nu_notamaxima,
    pp.nu_percentualaprovacao,
    ag.dt_agendamento,
    aa.bl_ativo,
    aa.dt_avaliacao,
    alc.id_saladeaula,
    ui.st_codusuario,
    ac.id_avaliacaoconjunto,
    pp.st_projetopedagogico,
    mt.id_projetopedagogico,
    aa.dt_defesa,
    aa.st_tituloavaliacao,
    mt.id_entidadeatendimento,
    aa.id_upload,
    dc.id_tipodisciplina,
    aa.st_justificativa,
    md.id_matriculadisciplina,
    aa.id_tiponota,
    sda.dt_cadastro as dt_cadastrosala,
    sda.dt_encerramento as dt_encerramentosala,
    aa.dt_cadastro as dt_cadastroavaliacao,
    aa.id_usuariocadastro as id_usuariocadavaliacao,
    usc.st_nomecompleto  as st_usuariocadavaliacao,
    sdis.id_evolucao as id_evolucaodisciplina,
    sdis.st_evolucao as st_evolucaodisciplina
  FROM tb_matriculadisciplina AS md
    JOIN tb_matricula AS mt ON md.id_matricula = mt.id_matricula
    JOIN dbo.tb_usuario AS us ON us.id_usuario = mt.id_usuario
    JOIN tb_modulo AS mdl ON mdl.id_projetopedagogico = mt.id_projetopedagogico	AND mdl.bl_ativo = 1
    JOIN tb_modulodisciplina AS mdc ON mdc.id_modulo = mdl.id_modulo AND md.id_disciplina = mdc.id_disciplina AND mdc.bl_ativo = 1
    JOIN tb_alocacao AS alc ON  alc.id_matriculadisciplina = md.id_matriculadisciplina and alc.bl_ativo = 1
    JOIN tb_avaliacaoconjuntoreferencia AS acrf	ON (acrf.id_saladeaula = alc.id_saladeaula) AND (acrf.dt_fim IS NULL OR acrf.dt_fim >= GETDATE())
    JOIN tb_disciplinasaladeaula as dsa on dsa.id_saladeaula = acrf.id_saladeaula
    JOIN tb_saladeaula sda on sda.id_saladeaula = acrf.id_saladeaula
    JOIN tb_disciplina AS dc ON dc.id_disciplina = mdc.id_disciplina AND dc.id_disciplina = md.id_disciplina and dsa.id_disciplina = dc.id_disciplina
    JOIN tb_evolucao as sdis on sdis.id_evolucao = md.id_evolucao
    JOIN tb_avaliacaoconjunto AS ac	ON ac.id_avaliacaoconjunto = acrf.id_avaliacaoconjunto AND (acrf.id_saladeaula = alc.id_saladeaula AND id_tipoprova = 2)
    JOIN tb_avaliacaoconjuntorelacao AS acr	ON acr.id_avaliacaoconjunto = ac.id_avaliacaoconjunto
    JOIN tb_avaliacao AS av	ON av.id_avaliacao = acr.id_avaliacao
    JOIN tb_tipoavaliacao AS ta ON ta.id_tipoavaliacao = av.id_tipoavaliacao
    LEFT JOIN tb_avaliacaoagendamento AS ag	ON mt.id_matricula = ag.id_matricula AND av.id_avaliacao = ag.id_avaliacao AND ag.id_situacao=68 and ag.nu_presenca=1
    LEFT JOIN tb_avalagendamentoref as avref on avref.id_avaliacaoagendamento=ag.id_avaliacaoagendamento and avref.id_avaliacaoconjuntoreferencia = acrf.id_avaliacaoconjuntoreferencia
    LEFT JOIN tb_avaliacaoaluno AS aa
      ON aa.id_matricula = mt.id_matricula
         AND aa.id_avaliacaoconjuntoreferencia = acrf.id_avaliacaoconjuntoreferencia
         AND aa.id_avaliacao = av.id_avaliacao
    JOIN tb_situacao AS st
      ON st.id_situacao = md.id_situacao
    JOIN tb_evolucao AS ev
      ON ev.id_evolucao = md.id_evolucao
    JOIN tb_projetopedagogico AS PP
      ON pp.id_projetopedagogico = mt.id_projetopedagogico
    LEFT JOIN dbo.tb_usuariointegracao AS ui
      ON ui.id_usuario = mt.id_usuario AND (ui.id_entidade = mt.id_entidadeatendimento OR ui.id_entidade = mt.id_entidadematricula)
    LEFT JOIN tb_usuario as usc on usc.id_usuario= aa.id_usuariocadastro
  WHERE mt.bl_ativo = 1

----------------------AC-27256-------------------------

CREATE VIEW [dbo].[vw_saladeaulaperfilprojetointegracao]
AS
  SELECT  pp.st_projetopedagogico ,
    us.id_usuario ,
    us.st_nomecompleto ,
    us.st_cpf ,
    us.st_login ,
    us.st_senha ,
    e.id_entidade ,
    e.st_nomeentidade ,
    ei.st_codsistema ,
    ei.st_caminho ,
    usi.st_codusuario ,
    usi.st_senhaintegrada ,
    usi.st_loginintegrado ,
    s.id_saladeaula ,
    s.id_tiposaladeaula ,
    s.st_saladeaula ,
    s.dt_abertura ,
    s.id_categoriasala ,
    CONVERT (VARCHAR(MAX), s.dt_abertura, 103) AS st_abertura ,
    st_encerramento = CASE WHEN s.dt_encerramento IS NULL THEN '-'
                      ELSE CONVERT (VARCHAR(MAX), s.dt_encerramento, 103)
                      END ,
    s.dt_inicioinscricao ,
    s.dt_fiminscricao ,
    s.bl_ativa AS bl_ativosala ,
    ts.st_tiposaladeaula ,
    sai.st_codsistemacurso ,
    sai.st_codsistemasala ,
    sai.st_codsistemareferencia ,
    sis.id_sistema ,
    perf.id_perfil ,
    perf.st_nomeperfil ,
    perfped.id_perfilpedagogico ,
    perfped.st_perfilpedagogico ,
    dis.id_disciplina ,
    dis.st_disciplina ,
    dis.id_disciplina AS id_areaconhecimento ,
    dis.st_disciplina AS st_areaconhecimento ,
    dis.id_tipodisciplina,
    pp.id_projetopedagogico ,
    'id_status' = CASE WHEN s.dt_abertura > CAST (GETDATE() AS DATE) THEN 3 -- n�o iniciada
                  WHEN (s.dt_encerramento IS NOT NULL) AND ((DATEADD(day, ISNULL(s.nu_diasextensao, 0), s.dt_encerramento)) < CAST(GETDATE() AS DATE) ) THEN 2
                  WHEN s.dt_abertura <= CAST (GETDATE() AS DATE)
                       AND ( s.dt_encerramento IS NULL OR ((DATEADD(day, ISNULL(s.nu_diasextensao, 0), s.dt_encerramento)) >= CAST(GETDATE() AS DATE)) ) THEN 1

                  ELSE 1
                  END ,
    /*'st_status' = CASE WHEN s.dt_abertura > CAST (GETDATE() AS DATE)
                       THEN 'N�o Iniciada'
                       WHEN s.dt_abertura <= CAST (GETDATE() AS DATE)
                            AND ( s.dt_encerramento >= CAST (GETDATE() AS DATE)
                                  OR s.dt_encerramento IS NULL
                                ) THEN 'Aberta'
                       WHEN s.dt_encerramento < CAST (GETDATE() AS DATE)
                       THEN 'Fechada'
                       ELSE 'Erro'
                  END*/
    'st_status' = CASE WHEN s.dt_abertura > CAST (GETDATE() AS DATE)THEN 'Não Iniciada'
                  WHEN s.dt_abertura <= CAST (GETDATE() AS DATE)
                       AND ( s.dt_encerramento IS NULL OR ((DATEADD(day, ISNULL(s.nu_diasextensao, 0), s.dt_encerramento)) >= CAST(GETDATE() AS DATE)) ) THEN 'Aberta'
                  WHEN (s.dt_encerramento IS NOT NULL) AND ((DATEADD(day, ISNULL(s.nu_diasextensao, 0), s.dt_encerramento)) < CAST(GETDATE() AS DATE) )
                    THEN 'Fechada'
                  ELSE 'Erro'
                  END
  FROM    tb_perfil AS perf
    JOIN tb_usuarioperfilentidadereferencia AS uper ON perf.id_perfil = uper.id_perfil
                                                       AND uper.bl_ativo = 1
    JOIN dbo.tb_perfilreferenciaintegracao AS pri ON pri.id_perfilreferencia = uper.id_perfilreferencia
    JOIN tb_usuario AS us ON uper.id_usuario = us.id_usuario
    JOIN tb_usuarioperfilentidade AS upe ON upe.id_usuario = us.id_usuario
                                            AND upe.id_entidade = uper.id_entidade
                                            AND upe.id_perfil = uper.id_perfil
                                            AND upe.bl_ativo = 1
    JOIN tb_perfilpedagogico AS perfped ON perfped.id_perfilpedagogico = perf.id_perfilpedagogico
    JOIN dbo.tb_projetopedagogico AS pp ON pp.bl_ativo = 1
                                           AND pp.id_projetopedagogico = uper.id_projetopedagogico
    JOIN dbo.tb_areaprojetosala AS aps ON aps.id_projetopedagogico = pp.id_projetopedagogico
    JOIN tb_saladeaulaintegracao AS sai ON pri.id_saladeaulaintegracao = sai.id_saladeaulaintegracao
                                           AND aps.id_saladeaula = sai.id_saladeaula
    JOIN tb_saladeaula AS s ON sai.id_saladeaula = s.id_saladeaula
                               AND s.bl_ativa = 1
                               AND s.id_situacao = 8
    JOIN tb_entidade AS e ON e.id_entidade = s.id_entidade
    JOIN tb_entidadeintegracao AS ei ON ei.id_entidade = e.id_entidade
                                        AND sai.id_sistema = ei.id_sistema
    LEFT JOIN tb_usuariointegracao AS usi ON usi.id_usuario = us.id_usuario
                                             AND usi.id_entidade = s.id_entidade
                                             AND usi.id_sistema = ei.id_sistema
    JOIN tb_tiposaladeaula AS ts ON ts.id_tiposaladeaula = s.id_tiposaladeaula
    JOIN tb_sistema AS sis ON sis.id_sistema = sai.id_sistema
                              AND sis.id_sistema = ei.id_sistema
                              AND sis.id_sistema = usi.id_sistema
    JOIN tb_disciplinasaladeaula AS dsa ON dsa.id_saladeaula = s.id_saladeaula --                                     AND sai.id_saladeaula = dsa.id_saladeaula
    JOIN tb_disciplina AS dis ON dis.id_disciplina = dsa.id_disciplina
  WHERE   perf.id_perfilpedagogico = 2
  UNION
  SELECT  pp.st_projetopedagogico ,
    us.id_usuario ,
    us.st_nomecompleto ,
    us.st_cpf ,
    us.st_login ,
    us.st_senha ,
    e.id_entidade ,
    e.st_nomeentidade ,
    ei.st_codsistema ,
    ei.st_caminho ,
    usi.st_codusuario ,
    usi.st_senhaintegrada ,
    usi.st_loginintegrado ,
    s.id_saladeaula ,
    s.id_tiposaladeaula ,
    s.st_saladeaula ,
    s.dt_abertura ,
    s.id_categoriasala ,
    CONVERT (VARCHAR(MAX), s.dt_abertura, 103) AS st_abertura ,
    st_encerramento = CASE WHEN s.dt_encerramento IS NULL THEN '-'
                      ELSE CONVERT (VARCHAR(MAX), s.dt_encerramento, 103)
                      END ,
    s.dt_inicioinscricao ,
    s.dt_fiminscricao ,
    s.bl_ativa AS bl_ativosala ,
    ts.st_tiposaladeaula ,
    sai.st_codsistemacurso ,
    sai.st_codsistemasala ,
    sai.st_codsistemareferencia ,
    sis.id_sistema ,
    perf.id_perfil ,
    perf.st_nomeperfil ,
    perfped.id_perfilpedagogico ,
    perfped.st_perfilpedagogico ,
    dis.id_disciplina ,
    dis.st_disciplina ,
    dis.id_disciplina AS id_areaconhecimento ,
    dis.st_disciplina AS st_areaconhecimento ,
    dis.id_tipodisciplina,
    pp.id_projetopedagogico ,
    /*'id_status' = CASE WHEN s.dt_abertura > CAST (GETDATE() AS DATE)
                       THEN 3
                       WHEN s.dt_abertura <= CAST (GETDATE() AS DATE)
                            AND ( s.dt_encerramento >= CAST (GETDATE() AS DATE)
                                  OR s.dt_encerramento IS NULL
                                ) THEN 1
                       WHEN s.dt_encerramento < CAST (GETDATE() AS DATE)
                       THEN 2
                       ELSE 1
                  END*/
    'id_status' = CASE WHEN s.dt_abertura > CAST (GETDATE() AS DATE) THEN 3 -- n�o iniciada
                  WHEN (s.dt_encerramento IS NOT NULL) AND ((DATEADD(day, ISNULL(s.nu_diasextensao, 0), s.dt_encerramento)) < CAST(GETDATE() AS DATE) ) THEN 2
                  WHEN s.dt_abertura <= CAST (GETDATE() AS DATE)
                       AND ( s.dt_encerramento IS NULL OR ((DATEADD(day, ISNULL(s.nu_diasextensao, 0), s.dt_encerramento)) >= CAST(GETDATE() AS DATE)) ) THEN 1

                  ELSE 1
                  END,
    'st_status' = CASE WHEN s.dt_abertura > CAST (GETDATE() AS DATE)THEN 'Não Iniciada'
                  WHEN s.dt_abertura <= CAST (GETDATE() AS DATE)
                       AND ( s.dt_encerramento IS NULL OR ((DATEADD(day, ISNULL(s.nu_diasextensao, 0), s.dt_encerramento)) >= CAST(GETDATE() AS DATE)) ) THEN 'Aberta'
                  WHEN (s.dt_encerramento IS NOT NULL) AND ((DATEADD(day, ISNULL(s.nu_diasextensao, 0), s.dt_encerramento)) < CAST(GETDATE() AS DATE) )
                    THEN 'Fechada'
                  ELSE 'Erro'
                  END
  FROM    tb_perfil AS perf
    JOIN tb_usuarioperfilentidadereferencia AS uper ON perf.id_perfil = uper.id_perfil
                                                       AND uper.bl_ativo = 1 --JOIN dbo.tb_perfilreferenciaintegracao AS pri ON pri.id_perfilreferencia = uper.id_perfilreferencia
    JOIN tb_usuario AS us ON uper.id_usuario = us.id_usuario
    JOIN tb_usuarioperfilentidade AS upe ON upe.id_usuario = us.id_usuario
                                            AND upe.id_entidade = uper.id_entidade
                                            AND upe.id_perfil = uper.id_perfil
                                            AND upe.bl_ativo = 1
    JOIN tb_perfilpedagogico AS perfped ON perfped.id_perfilpedagogico = perf.id_perfilpedagogico
    JOIN dbo.tb_projetoentidade AS pe ON pe.id_entidade = uper.id_entidade
    JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = pe.id_projetopedagogico
                                           AND pp.bl_ativo = 1
    JOIN dbo.tb_areaprojetosala AS aps ON pp.id_projetopedagogico = aps.id_projetopedagogico
    JOIN dbo.tb_saladeaulaentidade AS sei ON sei.id_entidade = uper.id_entidade
                                             AND aps.id_saladeaula = sei.id_saladeaula
    JOIN tb_saladeaula AS s ON s.id_saladeaula = sei.id_saladeaula
                               AND s.bl_ativa = 1
                               AND s.id_situacao = 8
    LEFT JOIN dbo.tb_saladeaulaintegracao AS sai ON sai.id_saladeaula = s.id_saladeaula
    JOIN tb_entidade AS e ON e.id_entidade = s.id_entidade
    LEFT JOIN tb_entidadeintegracao AS ei ON ei.id_entidade = e.id_entidade
                                             AND sai.id_sistema = ei.id_sistema
    LEFT JOIN tb_usuariointegracao AS usi ON usi.id_usuario = us.id_usuario
                                             AND usi.id_entidade = s.id_entidade
                                             AND usi.id_sistema = ei.id_sistema
    JOIN tb_tiposaladeaula AS ts ON ts.id_tiposaladeaula = s.id_tiposaladeaula
    LEFT JOIN tb_sistema AS sis ON sis.id_sistema = sai.id_sistema
                                   AND sis.id_sistema = ei.id_sistema --AND sis.id_sistema = usi.id_sistema
    JOIN tb_disciplinasaladeaula AS dsa ON dsa.id_saladeaula = s.id_saladeaula
                                           AND s.id_saladeaula = dsa.id_saladeaula
    JOIN tb_disciplina AS dis ON dis.id_disciplina = dsa.id_disciplina
  WHERE   perf.id_perfilpedagogico IN ( 3, 7 )
  UNION
  SELECT  pp.st_projetopedagogico ,
    us.id_usuario ,
    us.st_nomecompleto ,
    us.st_cpf ,
    us.st_login ,
    us.st_senha ,
    e.id_entidade ,
    e.st_nomeentidade ,
    ei.st_codsistema ,
    ei.st_caminho ,
    usi.st_codusuario ,
    usi.st_senhaintegrada ,
    usi.st_loginintegrado ,
    s.id_saladeaula ,
    s.id_tiposaladeaula ,
    s.st_saladeaula ,
    s.dt_abertura ,
    s.id_categoriasala ,
    CONVERT (VARCHAR(MAX), s.dt_abertura, 103) AS st_abertura ,
    st_encerramento = CASE WHEN s.dt_encerramento IS NULL THEN '-'
                      ELSE CONVERT (VARCHAR(MAX), s.dt_encerramento, 103)
                      END ,
    s.dt_inicioinscricao ,
    s.dt_fiminscricao ,
    s.bl_ativa AS bl_ativosala ,
    ts.st_tiposaladeaula ,
    sai.st_codsistemacurso ,
    sai.st_codsistemasala ,
    sai.st_codsistemareferencia ,
    sis.id_sistema ,
    perf.id_perfil ,
    perf.st_nomeperfil ,
    perfped.id_perfilpedagogico ,
    perfped.st_perfilpedagogico ,
    dis.id_disciplina ,
    dis.st_disciplina ,
    dis.id_disciplina AS id_areaconhecimento ,
    dis.st_disciplina AS st_areaconhecimento ,
    dis.id_tipodisciplina,
    pp.id_projetopedagogico ,
    'id_status' = CASE WHEN s.dt_abertura > CAST (GETDATE() AS DATE) THEN 3 -- n�o iniciada
                  WHEN (s.dt_encerramento IS NOT NULL) AND ((DATEADD(day, ISNULL(s.nu_diasextensao, 0), s.dt_encerramento)) < CAST(GETDATE() AS DATE) ) THEN 2
                  WHEN s.dt_abertura <= CAST (GETDATE() AS DATE)
                       AND ( s.dt_encerramento IS NULL OR ((DATEADD(day, ISNULL(s.nu_diasextensao, 0), s.dt_encerramento)) >= CAST(GETDATE() AS DATE)) ) THEN 1

                  ELSE 1
                  END ,
    'st_status' = CASE WHEN s.dt_abertura > CAST (GETDATE() AS DATE)THEN 'Não Iniciada'
                  WHEN s.dt_abertura <= CAST (GETDATE() AS DATE)
                       AND ( s.dt_encerramento IS NULL OR ((DATEADD(day, ISNULL(s.nu_diasextensao, 0), s.dt_encerramento)) >= CAST(GETDATE() AS DATE)) ) THEN 'Aberta'
                  WHEN (s.dt_encerramento IS NOT NULL) AND ((DATEADD(day, ISNULL(s.nu_diasextensao, 0), s.dt_encerramento)) < CAST(GETDATE() AS DATE) )
                    THEN 'Fechada'
                  ELSE 'Erro'
                  END
  FROM    tb_perfil AS perf
    JOIN tb_usuarioperfilentidadereferencia AS uper ON perf.id_perfil = uper.id_perfil
                                                       AND uper.bl_ativo = 1 --JOIN dbo.tb_perfilreferenciaintegracao AS pri ON pri.id_perfilreferencia = uper.id_perfilreferencia
    JOIN tb_usuario AS us ON uper.id_usuario = us.id_usuario
    JOIN tb_usuarioperfilentidade AS upe ON upe.id_usuario = us.id_usuario
                                            AND upe.id_entidade = uper.id_entidade
                                            AND upe.id_perfil = uper.id_perfil
                                            AND upe.bl_ativo = 1
    JOIN tb_perfilpedagogico AS perfped ON perfped.id_perfilpedagogico = perf.id_perfilpedagogico
    JOIN dbo.tb_projetoentidade AS pe ON pe.id_entidade = uper.id_entidade
    JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = pe.id_projetopedagogico
                                           AND pp.bl_ativo = 1
    JOIN dbo.tb_areaprojetosala AS aps ON pp.id_projetopedagogico = aps.id_projetopedagogico
    JOIN tb_saladeaula AS s ON s.id_saladeaula = aps.id_saladeaula
                               AND s.bl_ativa = 1
                               AND s.id_situacao = 8 AND pe.id_entidade = s.id_entidade
    LEFT JOIN dbo.tb_saladeaulaintegracao AS sai ON sai.id_saladeaula = s.id_saladeaula
    JOIN tb_entidade AS e ON e.id_entidade = s.id_entidade
    LEFT JOIN tb_entidadeintegracao AS ei ON ei.id_entidade = e.id_entidade
                                             AND sai.id_sistema = ei.id_sistema
    LEFT JOIN tb_usuariointegracao AS usi ON usi.id_usuario = us.id_usuario
                                             AND usi.id_entidade = s.id_entidade
                                             AND usi.id_sistema = ei.id_sistema
    JOIN tb_tiposaladeaula AS ts ON ts.id_tiposaladeaula = s.id_tiposaladeaula
    LEFT JOIN tb_sistema AS sis ON sis.id_sistema = sai.id_sistema
                                   AND sis.id_sistema = ei.id_sistema --AND sis.id_sistema = usi.id_sistema
    JOIN tb_disciplinasaladeaula AS dsa ON dsa.id_saladeaula = s.id_saladeaula
                                           AND s.id_saladeaula = dsa.id_saladeaula
    JOIN tb_disciplina AS dis ON dis.id_disciplina = dsa.id_disciplina
  WHERE   perf.id_perfilpedagogico IN ( 3, 7 )

----------------------AC-27256-------------------------

CREATE VIEW [dbo].[vw_saladeaulaprofessorintegracao] AS
  SELECT DISTINCT
    us.id_usuario ,
    us.st_nomecompleto ,
    us.st_cpf ,
    us.st_login ,
    us.st_senha ,
    uper.id_entidade ,
    e.st_nomeentidade ,
    ei.st_codsistema ,
    ei.st_caminho ,
    usi.st_codusuario ,
    usi.st_senhaintegrada ,
    usi.st_loginintegrado ,
    s.id_saladeaula ,
    s.id_tiposaladeaula ,
    s.st_saladeaula ,
    s.dt_abertura ,
    s.id_categoriasala ,
    CONVERT(VARCHAR(MAX), s.dt_abertura, 103) AS st_abertura ,
    st_encerramento = CASE WHEN s.dt_encerramento IS NULL THEN '-'
                      ELSE CONVERT(VARCHAR(MAX), s.dt_encerramento, 103)
                      END ,
    s.dt_inicioinscricao ,
    s.dt_fiminscricao ,
    ts.st_tiposaladeaula ,
    sai.st_codsistemacurso ,
    sai.st_codsistemasala ,
    sai.st_codsistemareferencia ,
    sis.id_sistema ,
    perf.id_perfil ,
    perf.st_nomeperfil ,
    perfped.id_perfilpedagogico ,
    perfped.st_perfilpedagogico ,
    dis.id_disciplina ,
    dis.st_disciplina ,
    dis.id_disciplina AS id_areaconhecimento ,
    dis.st_disciplina AS st_areaconhecimento ,
    dis.id_tipodisciplina,
    /*'id_status' =
                 CASE
                   WHEN s.dt_abertura > CAST(GETDATE() AS DATE) THEN 3
                   WHEN s.dt_abertura <= CAST(GETDATE() AS DATE) AND
                     (s.dt_encerramento >= CAST(GETDATE() AS DATE) OR
                     s.dt_encerramento IS NULL) THEN 1
                   WHEN s.dt_encerramento < CAST(GETDATE() AS DATE) THEN 2
                   ELSE 1
                 END,
    'st_status' =
                 CASE
                   WHEN s.dt_abertura > CAST(GETDATE() AS DATE) THEN 'N�o Iniciada'
                   WHEN s.dt_abertura <= CAST(GETDATE() AS DATE) AND
                     (s.dt_encerramento >= CAST(GETDATE() AS DATE) OR
                     s.dt_encerramento IS NULL) THEN 'Aberta'
                   WHEN s.dt_encerramento < CAST(GETDATE() AS DATE) THEN 'Fechada'
                   ELSE 'Erro'
                 END*/
    'id_status' = CASE WHEN s.dt_abertura > CAST (GETDATE() AS DATE)
      THEN 3 -- n�o iniciada
                  WHEN ( s.dt_encerramento IS NOT NULL )
                       AND ( ( DATEADD(DAY,
                                       ISNULL(s.nu_diasextensao,
                                              0),
                                       s.dt_encerramento) ) < CAST(GETDATE() AS DATE) )
                    THEN 2
                  WHEN s.dt_abertura <= CAST (GETDATE() AS DATE)
                       AND ( s.dt_encerramento IS NULL
                             OR ( ( DATEADD(DAY,
                                            ISNULL(s.nu_diasextensao,
                                                   0),
                                            s.dt_encerramento) ) >= CAST(GETDATE() AS DATE) )
                       ) THEN 1
                  ELSE 1
                  END ,
    'st_status' = CASE WHEN s.dt_abertura > CAST (GETDATE() AS DATE)
      THEN 'Não Iniciada'
                  WHEN s.dt_abertura <= CAST (GETDATE() AS DATE)
                       AND ( s.dt_encerramento IS NULL
                             OR ( ( DATEADD(DAY,
                                            ISNULL(s.nu_diasextensao,
                                                   0),
                                            s.dt_encerramento) ) >= CAST(GETDATE() AS DATE) )
                       ) THEN 'Aberta'
                  WHEN ( s.dt_encerramento IS NOT NULL )
                       AND ( ( DATEADD(DAY,
                                       ISNULL(s.nu_diasextensao,
                                              0),
                                       s.dt_encerramento) ) < CAST(GETDATE() AS DATE) )
                    THEN 'Fechada'
                  ELSE 'Erro'
                  END
  FROM    tb_usuario AS us --JOIN tb_pessoa AS pe ON pe.id_usuario = us.id_usuario
    JOIN tb_usuarioperfilentidadereferencia AS uper ON uper.id_usuario = us.id_usuario
                                                       AND uper.bl_ativo = 1
    JOIN tb_perfil AS perf ON perf.id_perfil = uper.id_perfil
                              AND perf.id_perfilpedagogico = 1
    JOIN tb_perfilpedagogico AS perfped ON perfped.id_perfilpedagogico = perf.id_perfilpedagogico
    JOIN tb_saladeaula AS s ON uper.id_saladeaula = s.id_saladeaula
    JOIN tb_tiposaladeaula AS ts ON ts.id_tiposaladeaula = s.id_tiposaladeaula
    JOIN tb_disciplinasaladeaula AS dsa ON dsa.id_saladeaula = uper.id_saladeaula
    JOIN tb_disciplina AS dis ON dis.id_disciplina = dsa.id_disciplina
    JOIN tb_entidade AS e ON uper.id_entidade = e.id_entidade
    LEFT JOIN tb_saladeaulaintegracao AS sai ON sai.id_saladeaula = s.id_saladeaula
    LEFT JOIN dbo.tb_perfilreferenciaintegracao AS pri ON pri.id_saladeaulaintegracao = sai.id_saladeaulaintegracao
                                                          AND pri.id_perfilreferencia = uper.id_perfilreferencia
    LEFT JOIN tb_entidadeintegracao AS ei ON ei.id_entidade = e.id_entidade
                                             AND sai.id_sistema = ei.id_sistema
    LEFT JOIN tb_usuariointegracao AS usi ON usi.id_usuario = us.id_usuario
                                             AND usi.id_entidade = e.id_entidade
                                             AND usi.id_sistema = sai.id_sistema
    LEFT JOIN tb_sistema AS sis ON sis.id_sistema = sai.id_sistema
                                   AND sis.id_sistema = ei.id_sistema
                                   AND sis.id_sistema = usi.id_sistema


-----------------------vw_alunoslancamentonota------------------------

CREATE VIEW [dbo].[vw_alunoslancamentonota]
AS
  SELECT Distinct dbo.tb_avaliacaoagendamento.id_avaliacaoagendamento,
    dbo.tb_avaliacaoagendamento.id_matricula,
    p.id_usuario,
    dbo.vw_avaliacaoaplicacao.id_avaliacaoaplicacao,
    dbo.vw_avaliacaoaplicacao.dt_aplicacao,
    dbo.vw_matricula.st_nomecompleto,
    a.id_areaconhecimento,
    a.st_areaconhecimento,
    dbo.vw_matricula.id_projetopedagogico,
    dbo.vw_matricula.st_projetopedagogico,
    dbo.vw_matricula.st_evolucao,
    dbo.vw_matricula.id_situacao,
    dbo.vw_matricula.st_situacao,
    dbo.tb_avaliacaoagendamento.id_situacao AS id_situacaoagendamento,
    dbo.vw_matricula.id_evolucao,
    dbo.vw_avaliacaoaplicacao.st_aplicadorprova,
    dbo.vw_avaliacaoaplicacao.st_endereco,
    dbo.vw_avaliacaoaplicacao.sg_uf, p.st_cpf, p.st_email,
    dbo.vw_avaliacaoaplicacao.id_entidade,dbo.vw_avaliacaoaplicacao.id_aplicadorprova,
    dbo.tb_avaliacaoagendamento.bl_provaglobal,
    dbo.tb_avaliacaoagendamento.id_tipodeavaliacao,
    aval.nu_valor
  FROM            dbo.tb_avaliacaoagendamento

    INNER JOIN dbo.vw_avaliacaoaplicacao ON dbo.tb_avaliacaoagendamento.id_avaliacaoaplicacao  = dbo.vw_avaliacaoaplicacao.id_avaliacaoaplicacao
    INNER JOIN dbo.vw_matricula on dbo.vw_matricula.id_matricula=dbo.tb_avaliacaoagendamento.id_matricula
    inner join vw_pessoa as p on p.id_usuario = dbo.tb_avaliacaoagendamento.id_usuario and p.id_entidade=dbo.vw_avaliacaoaplicacao.id_entidade
    inner join tb_areaprojetopedagogico as ap on ap.id_projetopedagogico=dbo.vw_matricula.id_projetopedagogico
    inner join tb_areaconhecimento as a on a.id_areaconhecimento=ap.id_areaconhecimento
    inner join tb_avaliacao as aval on aval.id_avaliacao = dbo.tb_avaliacaoagendamento.id_avaliacao
  where dbo.vw_matricula.id_evolucao=6
        and dbo.tb_avaliacaoagendamento.id_situacao=68
        and dbo.tb_avaliacaoagendamento.nu_presenca=1


-----------------------vw_entidaderelacao------------------------

CREATE VIEW [dbo].[vw_entidaderelacao]
  WITH SCHEMABINDING AS
  SELECT
    e.id_entidade,
    e.st_nomeentidade,
    er.id_entidadepai,
    e.bl_ativo,
    e.id_situacao,
    er.id_entidadeclasse
  FROM dbo.tb_entidade e
    JOIN dbo.tb_entidaderelacao er
      ON er.id_entidade = e.id_entidade AND e.bl_ativo = 1


-----------------------vw_entidaderecursiva------------------------

CREATE VIEW [dbo].[vw_entidaderecursiva] AS
  WITH rec (id_entidade, st_nomeentidade, id_entidadepai, nu_nivelhierarquia, cadeiaentidades)as (
    select
      e.id_entidade,
      e.st_nomeentidade,
      null as id_entidadepai,
      1 as nu_nivelhierarquia,
      CAST(('|' + CAST(e.id_entidade AS VARCHAR) + '|') AS VARCHAR) as cadeiaentidades
    from vw_entidaderelacao e
    where
      e.bl_ativo = '1'
      AND e.id_situacao = '2'
    --AND (e.id_entidade = e.id_entidadepai OR e.id_entidadepai IS NULL)
    --and e.id_entidadeclasse = 1
    UNION ALL
    select
      e.id_entidade,
      e.st_nomeentidade,
      e.id_entidadepai,
      rec.nu_nivelhierarquia + 1 as nu_nivelhierarquia,
      CAST((rec.cadeiaentidades + CAST(e.id_entidade as VARCHAR)+ '|' ) AS VARCHAR) as cadeiaentidades
    from vw_entidaderelacao e
      INNER JOIN rec ON rec.id_entidade = e.id_entidadepai AND e.id_entidade <> e.id_entidadepai and e.id_entidadeclasse = 1
    WHERE
      e.bl_ativo = '1'
      AND e.id_situacao = '2'
  )select * from rec


-----------------------vw_entidaderecursivaid------------------------

CREATE VIEW [dbo].[vw_entidaderecursivaid] AS
  WITH rec (
      id_entidade,
      st_nomeentidade,
      id_entidadepai,
      nu_entidadepai
  )AS (
    SELECT DISTINCT
      e.id_entidade,
      e.st_nomeentidade,
      e.id_entidadepai,
      CAST(e.id_entidadepai AS VARCHAR) AS nu_entidadepai
    FROM dbo.vw_entidaderelacao e
    where e.id_situacao = 2
    UNION ALL
    SELECT
      e.id_entidade,
      e.st_nomeentidade,
      e.id_entidadepai,
      nu_entidadepai
    FROM dbo.vw_entidaderelacao AS e
      JOIN rec AS r ON (e.id_entidadepai = r.id_entidade) AND e.id_entidade != e.id_entidadepai
    where e.id_situacao = 2
  )SELECT DISTINCT
     id_entidade,
     st_nomeentidade,id_entidadepai,
     nu_entidadepai
   FROM rec


-----------------------vw_matricula------------------------

CREATE VIEW [dbo].[vw_matricula]
AS
  SELECT  mt.id_usuario ,
    us.st_nomecompleto ,
    us.st_cpf ,
    ps.st_urlavatar ,
    ps.dt_nascimento ,
    ps.st_nomepai ,
    ps.st_nomemae ,
    ps.st_email ,
    CAST(ps.nu_ddd AS VARCHAR) + '-' + CAST(ps.nu_telefone AS VARCHAR) AS st_telefone ,
    CAST(ps.nu_dddalternativo AS VARCHAR) + '-'
    + CAST(ps.nu_telefonealternativo AS VARCHAR) AS st_telefonealternativo ,
    mt.id_matricula ,
    pp.st_projetopedagogico ,
    mt.id_situacao ,
    st.st_situacao ,
    mt.id_evolucao ,
    ev.st_evolucao ,
    mt.dt_concluinte ,
    mt.id_entidadematriz ,
    etmz.st_nomeentidade AS st_entidadematriz ,
    mt.id_entidadematricula ,
    etm.st_nomeentidade AS st_entidadematricula ,
    mt.id_entidadeatendimento ,
    eta.st_nomeentidade AS st_entidadeatendimento ,
    mt.bl_ativo ,
    cm.id_contrato ,
    pp.id_projetopedagogico ,
    mt.dt_termino ,
    mt.dt_cadastro ,
    mt.dt_inicio ,
    tm.id_turma ,
    tm.st_turma ,
    tm.dt_inicio AS dt_inicioturma ,
    mt.bl_institucional ,
    tm.dt_fim AS dt_terminoturma ,
    mt.id_situacaoagendamento ,
    mt.id_vendaproduto ,
    mt.dt_termino AS dt_terminomatricula ,
    CAST (RIGHT(master.dbo.fn_varbintohexstr(HASHBYTES('MD5',
                                                       ( CAST (us.id_usuario AS VARCHAR(10)) COLLATE Latin1_General_CI_AI
                                                         + CAST (( CASE
                                                                   WHEN mt.id_matricula IS NULL
                                                                     THEN 0
                                                                   ELSE mt.id_matricula
                                                                   END ) AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
                                                         + CAST (5 AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
                                                         + CAST (( 0 ) AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
                                                         + us.st_login
                                                         + us.st_senha
                                                         + CONVERT (VARCHAR(10), GETDATE(), 103) ))),
                32) AS CHAR(32)) COLLATE Latin1_General_CI_AI AS st_urlacesso ,
    mt.bl_documentacao ,
    ct.id_venda ,
    eta.st_urlportal ,
    mt.st_codcertificacao ,
    ps.st_identificacao ,
    CONVERT(VARCHAR, mtc.dt_cadastro, 103) AS st_dtcadastrocertificacao ,
    CONVERT(VARCHAR, mtc.dt_enviocertificado, 103) AS st_dtenviocertificado ,
    CONVERT(VARCHAR, mtc.dt_retornocertificadora, 103) AS st_dtretornocertificadora ,
    CONVERT(VARCHAR, mtc.dt_envioaluno, 103) AS st_dtenvioaluno ,
    st_codigoacompanhamento ,
    mtc.id_indicecertificado ,
    mt.id_evolucaocertificacao ,
    ap.id_areaconhecimento ,
    pp.bl_disciplinacomplementar ,
    mt.id_matriculavinculada,
    cr.id_contratoregra,
    trc.id_tiporegracontrato,
    etm.st_siglaentidade,
    mt.bl_academico,
    mtc.dt_cadastro as dt_cadastrocertificacao ,
    mtc.dt_enviocertificado,
    mtc.dt_retornocertificadora ,
    mtc.dt_envioaluno,
    vup.st_upload as st_uploadcontrato
  FROM    tb_matricula mt
    JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
    JOIN tb_areaprojetopedagogico AS ap ON ap.id_projetopedagogico = pp.id_projetopedagogico
    JOIN tb_entidade AS etmz ON etmz.id_entidade = mt.id_entidadematriz
    JOIN tb_entidade AS etm ON etm.id_entidade = mt.id_entidadematricula
    JOIN tb_entidade AS eta ON eta.id_entidade = mt.id_entidadeatendimento
    JOIN tb_situacao AS st ON st.id_situacao = mt.id_situacao
    JOIN tb_evolucao AS ev ON ev.id_evolucao = mt.id_evolucao
    JOIN tb_usuario AS us ON us.id_usuario = mt.id_usuario
    LEFT JOIN tb_contratomatricula AS cm ON cm.id_matricula = mt.id_matricula
                                            AND cm.bl_ativo = 1
    LEFT JOIN vw_pessoa AS ps ON mt.id_usuario = ps.id_usuario
                                 AND mt.id_entidadeatendimento = ps.id_entidade
    LEFT JOIN dbo.tb_contrato AS ct ON ct.id_contrato = cm.id_contrato
                                       AND ct.bl_ativo = 1
    LEFT JOIN tb_turma AS tm ON tm.id_turma = mt.id_turma
    LEFT JOIN tb_matriculacertificacao mtc ON mtc.id_matricula = mt.id_matricula
                                              AND mtc.bl_ativo = 1
    LEFT JOIN dbo.tb_contratoregra AS cr ON cr.id_contratoregra = ct.id_contratoregra
    LEFT JOIN dbo.tb_tiporegracontrato AS trc ON trc.id_tiporegracontrato = cr.id_tiporegracontrato
    LEFT JOIN dbo.tb_venda AS v ON ct.id_venda = v.id_venda
    LEFT JOIN dbo.tb_upload AS vup ON vup.id_upload = v.id_uploadcontrato
  WHERE   mt.bl_ativo = 1;

-----------------------vw_modulodisciplina------------------------

CREATE VIEW [dbo].[vw_modulodisciplina]
AS
  SELECT p.id_projetopedagogico ,
    p.st_projetopedagogico ,
    md.id_modulodisciplina ,
    md.id_modulo ,
    m.st_modulo ,
    md.id_disciplina ,
    d.st_disciplina ,
    ar.id_areaconhecimento ,
    CASE WHEN ar.st_areaconhecimento IS NULL THEN 'Não informado'
    ELSE ar.st_areaconhecimento
    END AS st_areaconhecimento ,
    md.id_serie ,
    s.st_serie ,
    md.id_nivelensino ,
    ns.st_nivelensino ,
    md.bl_obrigatoria ,
    md.nu_ordem ,
    md.bl_ativo ,
    md.nu_ponderacaocalculada ,
    md.nu_ponderacaoaplicada ,
    md.id_usuarioponderacao ,
    md.dt_cadastroponderacao ,
    md.nu_importancia ,
    d.nu_cargahoraria AS nu_cargahorariad ,
    d.nu_repeticao ,
    d.sg_uf AS st_estudio ,
    CASE WHEN md.nu_cargahoraria IS NULL THEN d.nu_cargahoraria
    ELSE md.nu_cargahoraria
    END AS nu_cargahoraria
  FROM    tb_modulodisciplina AS md
    INNER JOIN tb_modulo AS m ON m.id_modulo = md.id_modulo
                                 AND m.bl_ativo = 1
    INNER JOIN tb_projetopedagogico AS p ON m.id_projetopedagogico = p.id_projetopedagogico
    INNER JOIN tb_disciplina AS d ON md.id_disciplina = d.id_disciplina
    LEFT JOIN tb_serie AS s ON s.id_serie = md.id_serie
    LEFT JOIN tb_nivelensino AS ns ON ns.id_nivelensino = md.id_nivelensino
    LEFT JOIN tb_areaconhecimento AS ar ON ar.id_areaconhecimento = md.id_areaconhecimento
  WHERE   md.bl_ativo = 1

-----------------------vw_pautaalunos------------------------

CREATE VIEW [dbo].[vw_pautaalunos]
AS
  SELECT DISTINCT
    u.id_usuario
    ,u.st_nomecompleto AS st_aluno
    , aa.st_tituloexibicaodisciplina as st_disciplina
    , professor.st_nomecompleto as st_professor
    , md.id_disciplina
    , mt.id_projetopedagogico
    , pp.st_tituloexibicao as st_projetopedagogico
    , se.id_entidade
  FROM tb_usuario u
    JOIN tb_matricula mt ON mt.id_usuario = u.id_usuario AND mt.id_evolucao = 6 AND mt.bl_ativo = 1 AND mt.bl_institucional = 0
    JOIN vw_matriculadisciplina as md ON md.id_matricula = mt.id_matricula
    JOIN tb_alocacao al ON al.id_matriculadisciplina = md.id_matriculadisciplina AND al.bl_ativo = 1
    JOIN tb_saladeaula sa ON sa.id_saladeaula = al.id_saladeaula AND sa.dt_encerramento IS NOT NULL
                             AND (DATEADD(DAY, ISNULL ( sa.nu_diasextensao , 0 ), sa.dt_encerramento) < CAST(GETDATE() AS DATE))
    JOIN tb_saladeaulaentidade se ON sa.id_saladeaula = se.id_saladeaula --AND se.id_entidade = 14
    JOIN vw_avaliacaoaluno AS aa ON aa.id_matriculadisciplina = md.id_matriculadisciplina and aa.id_matricula = mt.id_matricula and
                                    aa.id_saladeaula = sa.id_saladeaula and aa.st_nota is NULL
                                    and aa.id_tipoavaliacao = 4
    JOIN tb_projetopedagogico pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
    LEFT JOIN tb_usuarioperfilentidadereferencia uper ON  uper.id_saladeaula = sa.id_saladeaula -- AND uper.id_entidade = se.id_entidade
                                                          AND uper.bl_titular = 1 AND uper.bl_ativo = 1
    --JOIN tb_perfil p ON p.id_perfil = uper.id_perfil AND p.id_perfilpedagogico = 1 AND p.id_entidade = se.id_entidade
    LEFT JOIN tb_usuario as professor ON professor.id_usuario = uper.id_usuario

--WHERE md.id_disciplina = 4105

-----------------------vw_produtovenda------------------------

CREATE VIEW [dbo].[vw_produtovenda]
AS
  SELECT
    vpro.id_vendaproduto,
    vpro.id_matricula, pro.id_produto,
    pro.st_produto,
    tpro.id_tipoproduto,
    tpro.st_tipoproduto,
    v.id_venda,
    v.bl_ativo,
    vpro.nu_valorbruto,
    vpro.nu_valorliquido,
    vpro.id_turma,
    vpro.bl_ativo AS 'vp.bl_ativo',
    CASE
    WHEN vpro.nu_valorbruto = 0 THEN CAST(vpro.nu_desconto * 100 AS NUMERIC(30, 6))
    WHEN vpro.nu_valorbruto != 0 THEN CAST((vpro.nu_desconto * 100) / vpro.nu_valorbruto AS NUMERIC(30, 6))
    END AS nu_descontoporcentagem,
    ----CAST(((vpro.nu_desconto*100)/vpro.nu_valorbruto) as DECIMAL(5,2)) as nu_descontoporcentagem,
    vpro.nu_desconto AS nu_descontovalor,
    pv.id_tipoprodutovalor,
    tpv.st_tipoprodutovalor,
    cc.id_campanhacomercial,
    cc.st_campanhacomercial,
    pro2.st_produto AS st_produtocombo,
    pro.id_modelovenda,
    mv.st_modelovenda,
    vpro.id_tiposelecao, ts.st_tiposelecao, v.id_usuario, ent.id_entidade
  FROM tb_vendaproduto vpro
    JOIN tb_venda v
      ON vpro.id_venda = v.id_venda
         AND vpro.bl_ativo = 1
    JOIN tb_produto pro
      ON vpro.id_produto = pro.id_produto
    JOIN tb_tipoproduto tpro
      ON pro.id_tipoproduto = tpro.id_tipoproduto
    LEFT JOIN tb_modelovenda AS mv
      ON (pro.id_modelovenda = mv.id_modelovenda)
    LEFT JOIN tb_produto pro2
      ON pro2.id_produto = vpro.id_produtocombo
    LEFT JOIN tb_campanhacomercial cc
      ON vpro.id_campanhacomercial = cc.id_campanhacomercial
    LEFT JOIN tb_produtovalor pv
      ON pv.id_produto = vpro.id_produto
         AND pv.dt_inicio <= GETDATE()
         AND (pv.dt_termino >= GETDATE()
              OR pv.dt_termino IS NULL)
         AND pv.id_tipoprodutovalor = 4
    LEFT JOIN tb_tipoprodutovalor AS tpv
      ON tpv.id_tipoprodutovalor = pv.id_tipoprodutovalor
    LEFT JOIN tb_tiposelecao AS ts ON ts.id_tiposelecao = vpro.id_tiposelecao
    LEFT JOIN tb_entidade AS ent ON ent.id_entidade = pro.id_entidade

-----------------------vw_saladisciplinaentidade------------------------

CREATE view [dbo].[vw_saladisciplinaentidade] as
  select ta.id_projetopedagogico, ts3.id_entidade, td1.id_disciplina,ts.st_saladeaula, ts.dt_inicioinscricao, ts.dt_fiminscricao, ts.dt_abertura, ts.dt_encerramento
    , tu1.st_nomecompleto AS st_professor, ts1.bl_conteudo, ts2.st_sistema , st_disciplina
    ,ts.id_saladeaula
    ,ts.id_periodoletivo
  from dbo.tb_disciplinasaladeaula AS td1
    JOIN dbo.tb_disciplina AS dc ON td1.id_disciplina = dc.id_disciplina
    JOIN tb_saladeaulaentidade ts3 ON td1.id_saladeaula = ts3.id_saladeaula
    JOIN dbo.tb_areaprojetosala AS ta
      ON  ta.id_saladeaula = ts3.id_saladeaula AND ta.id_saladeaula = td1.id_saladeaula
    JOIN dbo.tb_saladeaula AS ts
      ON td1.id_saladeaula = ts.id_saladeaula AND ta.id_saladeaula = ts.id_saladeaula  AND ts.id_saladeaula = ts3.id_saladeaula
    LEFT OUTER JOIN dbo.tb_usuarioperfilentidadereferencia AS tu
      ON ts.id_saladeaula = tu.id_saladeaula AND tu.bl_ativo = 1 AND tu.bl_titular = 1
    LEFT OUTER JOIN dbo.tb_usuario AS tu1
      ON tu.id_usuario = tu1.id_usuario
    LEFT OUTER JOIN dbo.tb_saladeaulaintegracao AS ts1
      ON ts.id_saladeaula = ts1.id_saladeaula
    LEFT OUTER JOIN dbo.tb_sistema AS ts2
      ON ts1.id_sistema = ts2.id_sistema

-----------------------vw_atendentenegociacao------------------------

CREATE VIEW [rel].[vw_atendentenegociacao]
AS
  SELECT
    U.st_nomecompleto AS 'st_nomealuno',
    US.st_nomecompleto AS 'st_nomeatendente',
    US.id_usuario AS 'id_atendente',
    OP.id_usuario AS 'id_operador',
    OP.st_nomecompleto AS 'st_nomeoperador',
    vd.id_venda,
    p.st_produto,
    vd.nu_parcelas,
    vp.nu_valorliquido,
    CASE
    WHEN vd.nu_valorliquido > 0 THEN (vdlc.nu_valorcartao / vd.nu_valorliquido
                                      * vp.nu_valorliquido)
    ELSE 0
    END AS nu_valorcartao,
    CASE
    WHEN vd.nu_valorliquido > 0 THEN (vdlb.nu_valorboleto / vd.nu_valorliquido
                                      * vp.nu_valorliquido)
    ELSE 0
    END AS nu_valorboleto,
    CASE
    WHEN vd.nu_valorliquido > 0 THEN (vdld.nu_valordinheiro / vd.nu_valorliquido
                                      * vp.nu_valorliquido)
    ELSE 0
    END AS nu_valordinheiro,
    CASE
    WHEN vd.nu_valorliquido > 0 THEN (vdlch.nu_valorcheque / vd.nu_valorliquido
                                      * vp.nu_valorliquido)
    ELSE 0
    END AS nu_valorcheque,
    CASE
    WHEN vd.nu_valorliquido > 0 THEN (vdle.nu_valorempenho / vd.nu_valorliquido
                                      * vp.nu_valorliquido)
    ELSE 0
    END AS nu_valorempenho,
    CASE
    WHEN vd.nu_valorliquido > 0 THEN (vdldp.nu_valordeposito / vd.nu_valorliquido
                                      * vp.nu_valorliquido)
    ELSE 0
    END AS nu_valordeposito,
    CASE
    WHEN vd.nu_valorliquido > 0 THEN (vdlt.nu_valortransferencia / vd.nu_valorliquido
                                      * vp.nu_valorliquido)
    ELSE 0
    END AS nu_valortransferencia,
    CASE
    WHEN vd.nu_valorliquido > 0 THEN (vdlp.nu_valorpermuta / vd.nu_valorliquido
                                      * vp.nu_valorliquido)
    ELSE 0
    END AS nu_valorpermuta,
    CASE
    WHEN vd.nu_valorliquido > 0 THEN (vdlbl.nu_valorbolsa / vd.nu_valorliquido
                                      * vp.nu_valorliquido)
    ELSE 0
    END AS nu_valorbolsa,
    CASE
    WHEN vd.nu_valorliquido > 0 THEN (vdlec.nu_valorestorno / vd.nu_valorliquido
                                      * vp.nu_valorliquido)
    ELSE 0
    END AS nu_valorestorno,
    CASE
    WHEN vd.nu_valorliquido > 0 THEN (vdlcc.nu_valorcarta / vd.nu_valorliquido
                                      * vp.nu_valorliquido)
    ELSE 0
    END AS nu_valorcarta,
    E.st_nomeentidade,
    E.id_entidade,
    vd.dt_cadastro
  FROM dbo.tb_venda AS vd
    OUTER APPLY (
                  SELECT
                    SUM(nu_valor) AS nu_valorcartao
                  FROM dbo.vw_vendalancamento
                  WHERE id_venda = vd.id_venda
                        AND id_meiopagamento IN (1, 7, 11)
                ) AS vdlc
    OUTER APPLY (
                  SELECT
                    SUM(nu_valor) AS nu_valorboleto
                  FROM dbo.vw_vendalancamento
                  WHERE id_venda = vd.id_venda
                        AND id_meiopagamento = 2
                ) AS vdlb
    OUTER APPLY (
                  SELECT
                    SUM(nu_valor) AS nu_valordinheiro
                  FROM dbo.vw_vendalancamento
                  WHERE id_venda = vd.id_venda
                        AND id_meiopagamento = 3
                ) AS vdld
    OUTER APPLY (
                  SELECT
                    SUM(nu_valor) AS nu_valorcheque
                  FROM dbo.vw_vendalancamento
                  WHERE id_venda = vd.id_venda
                        AND id_meiopagamento = 4
                ) AS vdlch
    OUTER APPLY (
                  SELECT
                    SUM(nu_valor) AS nu_valorempenho
                  FROM dbo.vw_vendalancamento
                  WHERE id_venda = vd.id_venda
                        AND id_meiopagamento = 5
                ) AS vdle
    OUTER APPLY (
                  SELECT
                    SUM(nu_valor) AS nu_valordeposito
                  FROM dbo.vw_vendalancamento
                  WHERE id_venda = vd.id_venda
                        AND id_meiopagamento = 6
                ) AS vdldp
    OUTER APPLY (
                  SELECT
                    SUM(nu_valor) AS nu_valortransferencia
                  FROM dbo.vw_vendalancamento
                  WHERE id_venda = vd.id_venda
                        AND id_meiopagamento = 8
                ) AS vdlt
    OUTER APPLY (
                  SELECT
                    SUM(nu_valor) AS nu_valorpermuta
                  FROM dbo.vw_vendalancamento
                  WHERE id_venda = vd.id_venda
                        AND id_meiopagamento = 9
                ) AS vdlp
    OUTER APPLY (
                  SELECT
                    SUM(nu_valor) AS nu_valorbolsa
                  FROM dbo.vw_vendalancamento
                  WHERE id_venda = vd.id_venda
                        AND id_meiopagamento = 10
                ) AS vdlbl
    OUTER APPLY (
                  SELECT
                    SUM(nu_valor) AS nu_valorestorno
                  FROM dbo.vw_vendalancamento
                  WHERE id_venda = vd.id_venda
                        AND id_meiopagamento = 12
                ) AS vdlec
    OUTER APPLY (
                  SELECT
                    SUM(nu_valor) AS nu_valorcarta
                  FROM dbo.vw_vendalancamento
                  WHERE id_venda = vd.id_venda
                        AND id_meiopagamento = 13
                ) AS vdlcc
    JOIN dbo.tb_entidade AS E ON E.id_entidade = vd.id_entidade
    JOIN dbo.tb_vendaproduto AS vp	ON vp.id_venda = vd.id_venda
    -- Kayo Silva - Adicionei esse trecho buscando o contrato da venda e depois o vinculo
    -- com a matricula para saber de fato qual a matricula pois o atalho que estava sendo
    -- utilizado na tb_vendaproduto não estava sendo atualizado com o id_matricula
    -- quando a matricula era transferida, causando o sumiço da venda no relatório
    -- a exemplo da issue: FIN-17144
    join dbo.tb_contrato as ct on ct.id_venda = vd.id_venda and ct.id_evolucao = 4 and ct.id_situacao = 44 and ct.bl_ativo = 1
    join dbo.tb_contratomatricula as ctm on ctm.id_contrato = ct.id_contrato and ctm.bl_ativo = 1
    JOIN dbo.tb_matricula AS mt	ON mt.id_matricula = ctm.id_matricula
    --JOIN dbo.tb_matricula AS mt	ON mt.id_matricula = vp.id_matricula
    --fim da alteração
    JOIN dbo.tb_produto AS p ON p.id_produto = vp.id_produto AND mt.id_evolucao = 6
    JOIN dbo.tb_usuario AS U ON U.id_usuario = vd.id_usuario LEFT JOIN dbo.tb_usuario AS US	ON US.id_usuario = vd.id_atendente
    LEFT JOIN dbo.tb_usuario AS OP ON OP.id_usuario = vd.id_operador;

-----------------------vw_ecommerce------------------------

CREATE VIEW [rel].[vw_ecommerce] AS
  SELECT
    us.st_nomecompleto,
    us.st_email,
    us.st_cpf,
    CAST(nu_ddd AS VARCHAR) + CAST(nu_telefone AS VARCHAR) AS st_telefone,
    ede.st_endereco +' Nro. '+ ede.nu_numero +' '+ ede.st_complemento AS st_endereco,
    ede.st_cep,
    ede.st_bairro,
    ede.st_complemento,
    ede.nu_numero,
    ede.st_nomemunicipio AS st_cidade,
    ede.sg_uf,
    pd.st_produto,
    pd.id_produto,
    vp.nu_desconto AS nu_descontoproduto,
    vp.nu_valorbruto AS nu_valortabela,
    vp.nu_valorliquido AS nu_valornegociado,
    vd.nu_valorliquido AS nu_valorliquidovenda,
    CONVERT(VARCHAR(30), vd.dt_confirmacao, 103) AS dt_confirmacaoshow,
    vd.dt_confirmacao,
    --mp.st_meiopagamento,
    mp.id_meiopagamento,
    pd.id_tipoproduto,
    vd.id_venda,
    vd.id_evolucao,
    ev.st_evolucao,
    ve.id_entidade,
    CONVERT(VARCHAR(30), vd.dt_cadastro, 103) AS dt_cadastroshow,
    vd.dt_cadastro,
    pdc.st_produto AS st_combo,
    pdc.id_produto AS id_produtocombo,
    ve.st_nomeentidade,
    te.id_entidade AS id_entidadepai,
    vd.st_observacao,
    us.id_usuario AS cod_aluno,
    vd.id_cupom,
    us_atendente.st_nomecompleto
      AS st_nomeatendente,
    CONVERT(VARCHAR(30), us.dt_nascimento, 103) AS dt_nascimento,
    -----
    --(
    --  SELECT COALESCE(cti.st_categoria + ', ', '')
    --  FROM
    --    dbo.tb_categoriaproduto AS cpi
    --    JOIN dbo.tb_categoria                                     AS cti ON cti.id_categoria = cpi.id_categoria
    --  WHERE
    --    cpi.id_produto = pd.id_produto
    --  FOR XML PATH ('')
    --)
    NULL AS st_categorias,
    cp.id_categoria,
    cu.nu_desconto
      AS nu_valorcupom,
    cu.st_codigocupom,
    td.st_tipodesconto,
    cc.st_campanhacomercial,
    lvc.nu_parcelas,
    --(
    --  SELECT COALESCE(
    --    ac2.st_areaconhecimento + ', ',
    --    ''
    --)
    --  FROM
    --    dbo.tb_areaconhecimento ac2
    --    JOIN dbo.tb_areaprojetopedagogico app2
    --    JOIN dbo.tb_produtoprojetopedagogico AS ppp ON ppp.id_projetopedagogico = app2.id_projetopedagogico
    --      ON ac2.id_areaconhecimento = app2.id_areaconhecimento
    --  WHERE
    --    ppp.id_produto = vp.id_produto
    --  FOR XML PATH ('')
    --)
    NULL
      AS st_areaconhecimento,
    CASE cu.nu_desconto
    WHEN 100.00 THEN 'Gratuito'
    ELSE mp.st_meiopagamento
    END AS 'st_meiopagamento',
    st_status =
              CASE
              WHEN vd.id_evolucao = 10 THEN 'Confirmada'
              WHEN vd.id_evolucao = 9 THEN 'Aguardando Recebimento'
              WHEN vd.id_evolucao = 7 AND
                   ts.id_transacaofinanceira IS NOT NULL THEN 'Cartão Recusado'
              WHEN vd.id_evolucao = 7 AND
                   ts.id_transacaofinanceira IS NULL THEN 'Abandonada'
              ELSE ev.st_evolucao
              END


  FROM tb_entidade AS te
    JOIN dbo.vw_entidaderecursivaid AS ve
      ON ve.nu_entidadepai = te.id_entidade
    JOIN dbo.tb_venda AS vd
      ON vd.id_entidade = ve.id_entidade
    LEFT JOIN dbo.tb_usuario AS us_atendente
      ON vd.id_atendente = us_atendente.id_usuario
    JOIN dbo.tb_evolucao AS ev
      ON ev.id_evolucao = vd.id_evolucao
    JOIN dbo.vw_pessoa AS us
      ON us.id_usuario = vd.id_usuario
         AND us.id_entidade = vd.id_entidade
    LEFT JOIN dbo.tb_municipio AS mn
      ON mn.id_municipio = us.id_municipio
    JOIN dbo.tb_vendaproduto AS vp
      ON vd.id_venda = vp.id_venda
    JOIN dbo.tb_produto AS pd
      ON pd.id_produto = vp.id_produto
    LEFT JOIN tb_cupom AS cu
      ON cu.id_cupom = vd.id_cupom
    LEFT JOIN dbo.tb_campanhacomercial AS cc
      ON cu.id_campanhacomercial = cc.id_campanhacomercial
    LEFT JOIN dbo.tb_tipodesconto AS td
      ON td.id_tipodesconto = cu.id_tipodesconto
    LEFT JOIN dbo.tb_categoriaproduto AS cp
      ON cp.id_produto = pd.id_produto
    LEFT JOIN dbo.tb_lancamentovenda AS lv
      ON lv.id_venda = vd.id_venda
         AND bl_entrada = 1
    LEFT JOIN dbo.tb_lancamento AS lc
      ON lc.id_lancamento
         = lv.id_lancamento
    LEFT JOIN dbo.tb_meiopagamento AS mp
      ON mp.id_meiopagamento = lc.id_meiopagamento
    LEFT JOIN tb_produto AS pdc
      ON pdc.id_produto = vp.id_produtocombo
    OUTER APPLY (SELECT
                   COUNT(lvo.id_lancamento) AS nu_parcelas
                 FROM dbo.tb_lancamentovenda AS lvo
                 WHERE lvo.id_venda = vd.id_venda) AS lvc
    OUTER APPLY (SELECT TOP 1
                   tf.id_transacaofinanceira
                 FROM dbo.tb_transacaofinanceira AS tf
                 WHERE tf.id_venda = vd.id_venda
                       AND tf.nu_status = 2) AS ts
    OUTER APPLY (SELECT
                   ed.*, mn.st_nomemunicipio
                 FROM tb_endereco AS ed
                   JOIN tb_municipio AS mn
                     ON mn.id_municipio = ed.id_municipio
                 WHERE ed.id_endereco = vd.id_enderecoentrega) AS ede
  WHERE vd.bl_ativo = 1 UNION SELECT
                                us.st_nomecompleto,
                                us.st_email,
                                us.st_cpf,
                                CAST(nu_ddd AS VARCHAR) + CAST(nu_telefone AS VARCHAR) AS st_telefone,
                                ede.st_endereco +' Nro. '+ ede.nu_numero +' '+ ede.st_complemento AS st_endereco,
                                ede.st_cep,
                                ede.st_bairro,
                                ede.st_complemento,
                                ede.nu_numero,
                                ede.st_nomemunicipio AS st_cidade,
                                ede.sg_uf,
                                pd.st_produto,
                                pd.id_produto,
                                vp.nu_desconto AS nu_descontoproduto,
                                vp.nu_valorbruto AS nu_valortabela,
                                vp.nu_valorliquido AS nu_valornegociado,
                                vd.nu_valorliquido AS nu_valorliquidovenda,
                                CONVERT(VARCHAR(30), vd.dt_confirmacao, 103) AS dt_confirmacaoshow,
                                vd.dt_confirmacao,
                                --mp.st_meiopagamento,
                                mp.id_meiopagamento,
                                pd.id_tipoproduto,
                                vd.id_venda,
                                vd.id_evolucao,
                                ev.st_evolucao,
                                te.id_entidade,
                                CONVERT(VARCHAR(30), vd.dt_cadastro, 103) AS dt_cadastroshow,
                                vd.dt_cadastro,
                                pdc.st_produto AS st_combo,
                                pdc.id_produto AS id_produtocombo,
                                te.st_nomeentidade,
                                te.id_entidade AS id_entidadepai,
                                vd.st_observacao,
                                us.id_usuario AS cod_aluno,
                                vd.id_cupom,
                                us_atendente.st_nomecompleto
                                  AS st_nomeatendente,
                                CONVERT(VARCHAR(30), us.dt_nascimento, 103) AS dt_nascimento,
                                -----
                                (SELECT
                                COALESCE(cti.st_categoria + ', ', '')
                                 FROM dbo.tb_categoriaproduto AS cpi
                                   JOIN dbo.tb_categoria AS cti
                                     ON cti.id_categoria = cpi.id_categoria
                                 WHERE cpi.id_produto = pd.id_produto
                                 FOR XML PATH (''))
                                  AS st_categorias,
                                cp.id_categoria,
                                cu.nu_desconto
                                  AS nu_valorcupom,
                                cu.st_codigocupom,
                                td.st_tipodesconto,
                                cc.st_campanhacomercial,
                                lvc.nu_parcelas,
                                --(
                                --  SELECT COALESCE(
                                --    ac2.st_areaconhecimento + ', ',
                                --    ''
                                --)
                                --  FROM
                                --    dbo.tb_areaconhecimento ac2
                                --    JOIN dbo.tb_areaprojetopedagogico app2
                                --    JOIN dbo.tb_produtoprojetopedagogico AS ppp ON ppp.id_projetopedagogico = app2.id_projetopedagogico
                                --      ON ac2.id_areaconhecimento = app2.id_areaconhecimento
                                --  WHERE
                                --    ppp.id_produto = vp.id_produto
                                --  FOR XML PATH ('')
                                --)
                                NULL
                                  AS st_areaconhecimento,
                                CASE cu.nu_desconto
                                WHEN 100.00 THEN 'Gratuito'
                                ELSE mp.st_meiopagamento
                                END AS 'st_meiopagamento',
                                st_status =
                                          CASE
                                          WHEN vd.id_evolucao = 10 THEN 'Confirmada'
                                          WHEN vd.id_evolucao = 9 THEN 'Aguardando Recebimento'
                                          WHEN vd.id_evolucao = 7 AND
                                               ts.id_transacaofinanceira IS NOT NULL THEN 'Cartão Recusado'
                                          WHEN vd.id_evolucao = 7 AND
                                               ts.id_transacaofinanceira IS NULL THEN 'Abandonada'
                                          ELSE ev.st_evolucao
                                          END


                              FROM tb_entidade AS te
                                -- JOIN dbo.vw_entidaderecursivaid AS ve ON ve.nu_entidadepai = te.id_entidade OR ve.id_entidade = te.id_entidade
                                JOIN dbo.tb_venda AS vd
                                  ON vd.id_entidade = te.id_entidade
                                LEFT JOIN dbo.tb_usuario AS us_atendente
                                  ON vd.id_atendente = us_atendente.id_usuario
                                JOIN dbo.tb_evolucao AS ev
                                  ON ev.id_evolucao = vd.id_evolucao
                                JOIN dbo.vw_pessoa AS us
                                  ON us.id_usuario = vd.id_usuario
                                     AND us.id_entidade = vd.id_entidade
                                LEFT JOIN dbo.tb_municipio AS mn
                                  ON mn.id_municipio = us.id_municipio
                                JOIN dbo.tb_vendaproduto AS vp
                                  ON vd.id_venda = vp.id_venda
                                JOIN dbo.tb_produto AS pd
                                  ON pd.id_produto = vp.id_produto
                                LEFT JOIN tb_cupom AS cu
                                  ON cu.id_cupom = vd.id_cupom
                                LEFT JOIN dbo.tb_campanhacomercial AS cc
                                  ON cu.id_campanhacomercial = cc.id_campanhacomercial
                                LEFT JOIN dbo.tb_tipodesconto AS td
                                  ON td.id_tipodesconto = cu.id_tipodesconto
                                LEFT JOIN dbo.tb_categoriaproduto AS cp
                                  ON cp.id_produto = pd.id_produto
                                LEFT JOIN dbo.tb_lancamentovenda AS lv
                                  ON lv.id_venda = vd.id_venda
                                     AND bl_entrada = 1
                                LEFT JOIN dbo.tb_lancamento AS lc
                                  ON lc.id_lancamento
                                     = lv.id_lancamento
                                LEFT JOIN dbo.tb_meiopagamento AS mp
                                  ON mp.id_meiopagamento = lc.id_meiopagamento
                                LEFT JOIN tb_produto AS pdc
                                  ON pdc.id_produto = vp.id_produtocombo
                                OUTER APPLY (SELECT
                                               COUNT(lvo.id_lancamento) AS nu_parcelas
                                             FROM dbo.tb_lancamentovenda AS lvo
                                             WHERE lvo.id_venda = vd.id_venda) AS lvc
                                OUTER APPLY (SELECT TOP 1
                                               tf.id_transacaofinanceira
                                             FROM dbo.tb_transacaofinanceira AS tf
                                             WHERE tf.id_venda = vd.id_venda
                                                   AND tf.nu_status = 2) AS ts
                                OUTER APPLY (SELECT
                                               ed.*, mn.st_nomemunicipio
                                             FROM tb_endereco AS ed
                                               JOIN tb_municipio AS mn
                                                 ON mn.id_municipio = ed.id_municipio
                                             WHERE ed.id_endereco = vd.id_enderecoentrega) AS ede
                              WHERE vd.bl_ativo = 1

-----------------------vw_matriculas_canceladas_vencidas------------------------

CREATE VIEW [dbo].[vw_matriculas_canceladas_vencidas] AS
select mat.id_matricula,
mat.id_usuario,
mat.id_evolucao,
mat.id_situacao,
mat.bl_ativo,
mat.id_turma,
mat.id_cancelamento,
mat.id_entidadeatendimento,
mat.dt_inicio,
mat.dt_termino,
mat.dt_ultimoacesso,
c.dt_solicitacao,
c.nu_diascancelamento
from tb_matricula as mat
join tb_cancelamento as c on c.id_cancelamento = mat.id_cancelamento
where mat.id_evolucao = 6
and mat.id_situacao = 50
and mat.bl_ativo = 1
and DATEADD(day, c.nu_diascancelamento ,c.dt_solicitacao) < GETDATE()
AND c.id_evolucao = 51
AND c.bl_cancelamentofinalizado = 1
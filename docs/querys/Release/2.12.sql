SET IDENTITY_INSERT dbo.tb_funcionalidade on
INSERT INTO tb_funcionalidade (
id_funcionalidade,
st_funcionalidade,
id_funcionalidadepai,
bl_ativo,
id_situacao,
st_classeflex,
st_urlicone,
id_tipofuncionalidade,
nu_ordem,
st_classeflexbotao,
bl_pesquisa,
bl_lista,
id_sistema,
st_ajuda,
st_target,
st_urlcaminho,
bl_visivel,
bl_relatorio,
st_urlcaminhoedicao
)
VALUES (
693,
'Autorizacao de Entrada',
157,
1,
123,
'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa',
'/img/ico/money--arrow.png',
3,
NULL,
NULL,
0,
0,
1,
NULL,
NULL,
'/matricula/autorizacao-de-entrada',
1,
0,
NULL
)
GO
SET IDENTITY_INSERT dbo.tb_funcionalidade off


--create table

CREATE TABLE [dbo].[tb_autorizacaoentrada] (
[id_autorizacaoentrada] int NOT NULL IDENTITY(1,1) ,
[id_usuario] int NOT NULL ,
[id_usuariocadastro] int NOT NULL ,
[dt_cadastro] datetime NULL DEFAULT getDate() ,
[bl_ativo] bit NOT NULL ,
[id_textosistema] int NULL ,
[id_entidade] int NOT NULL ,
PRIMARY KEY ([id_autorizacaoentrada]),
CONSTRAINT [Fk_Tb_autorizacaoentrada_Tb_usuario] FOREIGN KEY ([id_usuario]) REFERENCES [dbo].[tb_usuario] ([id_usuario]) ON DELETE NO ACTION ON UPDATE NO ACTION,
CONSTRAINT [Fk_Tb_autorizacaoentrada_Tb_usuario2] FOREIGN KEY ([id_usuariocadastro]) REFERENCES [dbo].[tb_usuario] ([id_usuario]) ON DELETE NO ACTION ON UPDATE NO ACTION,
CONSTRAINT [FK_Tb_autorizacaoentrada_Tb_textosistema] FOREIGN KEY ([id_textosistema]) REFERENCES [dbo].[tb_textosistema] ([id_textosistema]) ON DELETE NO ACTION ON UPDATE NO ACTION,
CONSTRAINT [Fk_Tb_autorizacaoentrada_Tb_entidade] FOREIGN KEY ([id_entidade]) REFERENCES [dbo].[tb_entidade] ([id_entidade]) ON DELETE NO ACTION ON UPDATE NO ACTION
)

GO


--insert mensagem padrao
SET IDENTITY_INSERT dbo.tb_mensagempadrao on
insert into tb_mensagempadrao (id_mensagempadrao, id_tipoenvio, st_mensagempadrao, st_default) values (32,4,'Autorizacao de Entrada', NULL)
GO
SET IDENTITY_INSERT dbo.tb_mensagempadrao OFF
GO
ALTER TABLE dbo.tb_turma ADD nu_maxfreepass INT NULL
GO
ALTER TABLE dbo.tb_turno ADD hr_inicio TIME NULL
GO
ALTER TABLE tb_lancamento ADD nu_cartao INT
go
ALTER TABLE dbo.tb_turno ADD hr_termino TIME NULL

UPDATE  dbo.tb_turno
SET     hr_inicio = '08:00' ,
        hr_termino = '12:00'
WHERE   id_turno = 1
GO
UPDATE  dbo.tb_turno
SET     hr_inicio = '14:00' ,
        hr_termino = '18:00'
WHERE   id_turno = 2
GO
UPDATE  dbo.tb_turno
SET     hr_inicio = '19:00' ,
        hr_termino = '23:00'
WHERE   id_turno = 3
GO
UPDATE  dbo.tb_turno
SET     hr_inicio = '08:00' ,
        hr_termino = '23:00'
WHERE   id_turno = 4
GO
SET IDENTITY_INSERT tb_notificacao ON

  INSERT INTO dbo.tb_notificacao
          ( id_notificacao ,
            st_notificacao ,
            dt_cadastro
          )
  VALUES  ( 2 , -- id_notificacao - int
            'Contrato Sem Matricula Vinculada' , -- st_notificacao - varchar(50)
            GETDATE()  -- dt_cadastro - datetime
          )

GO
SET IDENTITY_INSERT tb_notificacao OFF
GO


INSERT INTO dbo.tb_funcionalidade
        ( st_funcionalidade ,
          id_funcionalidadepai ,
          bl_ativo ,
          id_situacao ,
          st_classeflex ,
          st_urlicone ,
          id_tipofuncionalidade ,
          nu_ordem ,
          st_classeflexbotao ,
          bl_pesquisa ,
          bl_lista ,
          id_sistema ,
          st_ajuda ,
          st_target ,
          st_urlcaminho ,
          bl_visivel ,
          bl_relatorio ,
          st_urlcaminhoedicao ,
          bl_delete
        )
VALUES  ( 'Prorroga��o' , -- st_funcionalidade - varchar(100)
          157 , -- id_funcionalidadepai - int
          1 , -- bl_ativo - bit
          123 , -- id_situacao - int
          'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa' , -- st_classeflex - varchar(300)
          '/img/ico/door--arrow.png' , -- st_urlicone - varchar(1000)
          3 , -- id_tipofuncionalidade - int
          NULL , -- nu_ordem - int
          NULL , -- st_classeflexbotao - varchar(300)
          0 , -- bl_pesquisa - bit
          0 , -- bl_lista - bit
          1 , -- id_sistema - int
          NULL , -- st_ajuda - varchar(max)
          NULL , -- st_target - varchar(100)
          '/prorrogacao' , -- st_urlcaminho - varchar(50)
          1 , -- bl_visivel - int
          0 , -- bl_relatorio - bit
          NULL , -- st_urlcaminhoedicao - varchar(50)
          0  -- bl_delete - bit
        )
		

		GO
        
		ALTER TABLE [dbo].[tb_comissaolancamento] ADD [bl_autorizado] bit NULL DEFAULT 0
GO




    ALTER TABLE tb_holding
    add id_usuariocadastro int

    ALTER TABLE tb_holdingfiliada
    add id_usuariocadastro int

    ALTER TABLE tb_holdingfiliada ALTER COLUMN
    id_situacao INT

    ALTER TABLE tb_holding
    add id_situacao int
GO
SET IDENTITY_INSERT dbo.tb_situacao on
  INSERT INTO dbo.tb_situacao
            ( id_situacao,
              st_situacao ,
              st_tabela ,
              st_campo ,
              st_descricaosituacao
            )
    VALUES  ( 168,
              'Ativo' , -- st_situacao - varchar(255)
              'tb_holding' , -- st_tabela - varchar(255)
              'id_situacao' , -- st_campo - varchar(255)
              'Holding Ativa'  -- st_descricaosituacao - varchar(200)
            )

    INSERT INTO dbo.tb_situacao
            ( id_situacao,
              st_situacao ,
              st_tabela ,
              st_campo ,
              st_descricaosituacao
            )
    VALUES  ( 169,
              'Inativo' , -- st_situacao - varchar(255)
              'tb_holding' , -- st_tabela - varchar(255)
              'id_situacao' , -- st_campo - varchar(255)
              'Holding Inativo'  -- st_descricaosituacao - varchar(200)
            )

SET IDENTITY_INSERT dbo.tb_situacao off
SET IDENTITY_INSERT dbo.tb_funcionalidade oN
    INSERT INTO dbo.tb_funcionalidade
          ( id_funcionalidade ,
            st_funcionalidade ,
            id_funcionalidadepai ,
            bl_ativo ,
            id_situacao ,
            st_classeflex ,
            st_urlicone ,
            id_tipofuncionalidade ,
            nu_ordem ,
            st_classeflexbotao ,
            bl_pesquisa ,
            bl_lista ,
            id_sistema ,
            st_ajuda ,
            st_target ,
            st_urlcaminho ,
            bl_visivel ,
            bl_relatorio ,
            st_urlcaminhoedicao ,
            bl_delete
          )
    VALUES  ( 694,
              'holding' , -- st_funcionalidade - varchar(100)
              231 , -- id_funcionalidadepai - int
              1 , -- bl_ativo - bit
              123 , -- id_situacao - int
              '' , -- st_classeflex - varchar(300)
              '/img/ico/flag.png' , -- st_urlicone - varchar(1000)
              3 , -- id_tipofuncionalidade - int
              NULL , -- nu_ordem - int
              NULL , -- st_classeflexbotao - varchar(300)
              0 , -- bl_pesquisa - bit
              0 , -- bl_lista - bit
              1 , -- id_sistema - int
              NULL , -- st_ajuda - varchar(max)
              NULL , -- st_target - varchar(100)
              '/holding' , -- st_urlcaminho - varchar(50)
              1 , -- bl_visivel - int
              0 , -- bl_relatorio - bit
              NULL , -- st_urlcaminhoedicao - varchar(50)
              0  -- bl_delete - bit
            )

    INSERT INTO dbo.tb_funcionalidade
          ( id_funcionalidade,
            st_funcionalidade ,
            id_funcionalidadepai ,
            bl_ativo ,
            id_situacao ,
            st_classeflex ,
            st_urlicone ,
            id_tipofuncionalidade ,
            nu_ordem ,
            st_classeflexbotao ,
            bl_pesquisa ,
            bl_lista ,
            id_sistema ,
            st_ajuda ,
            st_target ,
            st_urlcaminho ,
            bl_visivel ,
            bl_relatorio ,
            st_urlcaminhoedicao ,
            bl_delete
          )
    VALUES  ( 696,
              'Holding Filiada' , -- st_funcionalidade - varchar(100)
              231 , -- id_funcionalidadepai - int
              1 , -- bl_ativo - bit
              123 , -- id_situacao - int
              '' , -- st_classeflex - varchar(300)
              '/img/ico/flag.png' , -- st_urlicone - varchar(1000)
              3 , -- id_tipofuncionalidade - int
              NULL , -- nu_ordem - int
              NULL , -- st_classeflexbotao - varchar(300)
              0 , -- bl_pesquisa - bit
              0 , -- bl_lista - bit
              1 , -- id_sistema - int
              NULL , -- st_ajuda - varchar(max)
              NULL , -- st_target - varchar(100)
              '/holding-filiada' , -- st_urlcaminho - varchar(50)
              1 , -- bl_visivel - int
              0 , -- bl_relatorio - bit
              NULL , -- st_urlcaminhoedicao - varchar(50)
              0  -- bl_delete - bit
            )
SET IDENTITY_INSERT dbo.tb_funcionalidade off
GO
ALTER TABLE tb_venda ADD dt_atualizado DATETIME2 DEFAULT GETDATE()

go

INSERT INTO dbo.tb_sistema
        (id_sistema, st_sistema, bl_ativo, st_conexao )
VALUES  (22, 'Hubspot', -- st_sistema - varchar(255)
          1, -- bl_ativo - bit
          'NULL'  -- st_conexao - varchar(2000)
          )

 ------------------------------------------------------------------
          INSERT INTO dbo.tb_entidadeintegracao
        ( id_entidade ,
          id_usuariocadastro ,
          id_sistema ,
          st_codsistema ,
          dt_cadastro ,
          st_codchave ,
          st_codarea ,
          st_caminho ,
          nu_perfilsuporte ,
          nu_perfilprojeto ,
          nu_perfildisciplina ,
          nu_perfilobservador ,
          nu_perfilalunoobs ,
          nu_perfilalunoencerrado ,
          nu_contexto ,
          st_linkedserver
        )
VALUES  ( 12 , -- id_entidade - int
          1 , -- id_usuariocadastro - int
          22 , -- id_sistema - int
          '' , -- st_codsistema - varchar(max)
          GETDATE() , -- dt_cadastro - datetime2
          '' , -- st_codchave - varchar(max)
          '' , -- st_codarea - varchar(100)
          '' , -- st_caminho - varchar(max)
          NULL , -- nu_perfilsuporte - numeric
          NULL , -- nu_perfilprojeto - numeric
          NULL , -- nu_perfildisciplina - numeric
          NULL , -- nu_perfilobservador - numeric
          NULL , -- nu_perfilalunoobs - numeric
          0 , -- nu_perfilalunoencerrado - int
          0 , -- nu_contexto - int
          ''  -- st_linkedserver - varchar(20)
        )

go

CREATE TRIGGER trg_UpdateDtAtualizaTbVenda
ON dbo.tb_venda
AFTER UPDATE
AS
    UPDATE dbo.tb_venda
    SET dt_atualizado = GETDATE()
    WHERE id_venda IN (SELECT DISTINCT id_venda FROM Inserted)
GO

 

create VIEW [dbo].[vw_comissaodireitos] AS
SELECT DISTINCT
	uper.id_usuario,
	us.st_nomecompleto,
	us.st_cpf,
	uper.id_entidade,
	'Autor' AS st_funcao,
	4 AS id_funcao,
	mdc.id_disciplina,
	dc.st_disciplina,
	vp.id_vendaproduto,
	CAST((vp.nu_valorliquido * (mdc.nu_ponderacaoaplicada / 100) * (uper.nu_porcentagem / 100)) AS DECIMAL) AS nu_comissaoreceber,
	vp.nu_valorliquido,
	ppd.st_projetopedagogico,
	ppd.id_projetopedagogico,
	vd.dt_confirmacao
	--uper.nu_porcentagem
FROM dbo.tb_usuarioperfilentidadereferencia AS uper
JOIN dbo.tb_modulodisciplina AS mdc	ON  mdc.nu_ponderacaoaplicada IS NOT NULL and mdc.id_disciplina = uper.id_disciplina
JOIN dbo.tb_disciplina AS dc ON dc.id_disciplina = uper.id_disciplina
JOIN dbo.tb_modulo AS md ON md.id_modulo = mdc.id_modulo AND md.bl_ativo = 1
JOIN dbo.tb_produtoprojetopedagogico AS pp	ON md.id_projetopedagogico = pp.id_projetopedagogico
JOIN dbo.tb_projetopedagogico AS ppd ON ppd.id_projetopedagogico = pp.id_projetopedagogico
JOIN dbo.tb_vendaproduto AS vp	ON  pp.id_produto = vp.id_produto AND vp.nu_valorliquido > 0
JOIN dbo.tb_venda AS vd ON vd.id_venda = vp.id_venda AND vd.id_evolucao = 10 AND vd.dt_confirmacao IS NOT NULL
JOIN dbo.tb_produto AS pd	ON vp.id_produto = pd.id_produto
JOIN dbo.tb_usuario AS us	ON us.id_usuario = uper.id_usuario
WHERE uper.nu_porcentagem IS NOT NULL

--UNION

--SELECT DISTINCT
--	uper.id_usuario,
--	us.st_nomecompleto,
--	us.st_cpf,
--	uper.id_entidade,
--	'Coordenador' AS st_funcao,
--	2 AS id_funcao,
--	NULL AS id_disciplina,
--	NULL AS st_disciplina,
--	vp.id_vendaproduto,
--	CAST((vp.nu_valorliquido * (uper.nu_porcentagem / 100)) AS DECIMAL) AS nu_comissaoreceber,
--	vp.nu_valorliquido
--	--uper.nu_porcentagem
--FROM dbo.tb_venda AS vd
--JOIN dbo.tb_vendaproduto AS vp	ON vd.id_venda = vp.id_venda
--JOIN dbo.tb_produto AS pd	ON vp.id_produto = pd.id_produto
--JOIN dbo.tb_produtoprojetopedagogico AS pp	ON pp.id_produto = vp.id_produto
--JOIN dbo.tb_modulo AS md	ON md.id_projetopedagogico = pp.id_projetopedagogico AND md.bl_ativo = 1
--JOIN dbo.tb_usuarioperfilentidadereferencia AS uper	ON md.id_projetopedagogico = uper.id_projetopedagogico AND uper.nu_porcentagem IS NOT NULL
--JOIN dbo.tb_usuario AS us	ON us.id_usuario = uper.id_usuario
--WHERE vp.id_vendaproduto NOT IN (SELECT
--	cl.id_vendaproduto
--FROM dbo.tb_comissaolancamento AS cl
--WHERE cl.id_vendaproduto = vp.id_vendaproduto AND uper.id_usuario = cl.id_usuario)







GO

CREATE FUNCTION [dbo].[fn_extratocomissaodireitos]
(	
@id_usuario INT,
@id_entidade INT,
@dt_inicio DATE,
@dt_termino DATETIME2
)
RETURNS TABLE 
AS


RETURN 
(
SELECT  DISTINCT crs.st_disciplina ,
        CAST(crs.nu_valor AS FLOAT) AS nu_valor,
        'Sub Total' AS st_tipo,
        ps.id_usuario, 
		crs.id_disciplina,
		crs.nu_valorvenda
		FROM tb_pessoa AS ps
		CROSS APPLY (
			SELECT 
				sum(cr.nu_comissaoreceber) AS nu_valor, 
				cr.id_usuario, 
				cr.st_disciplina, 
				cr.id_disciplina,
				cr.nu_valorliquido as nu_valorvenda
			FROM dbo.vw_comissaodireitos AS cr 
			WHERE ps.id_usuario = cr.id_usuario 
			AND @id_entidade = cr.id_entidade 
			AND dt_confirmacao BETWEEN @dt_inicio  AND @dt_termino AND cr.id_funcao = 4
			GROUP BY cr.id_usuario, cr.st_disciplina, cr.id_disciplina, cr.nu_valorliquido
		) AS crs 

UNION
SELECT DISTINCT 
        dc.st_disciplina ,
        CAST(lc.nu_valor AS FLOAT) AS nu_valor,
        'Adiantamento' AS st_tipo ,
        cl.id_usuario,
		cl.id_disciplina,
		null as nu_valorvenda
FROM    dbo.tb_comissaolancamento AS cl
        JOIN dbo.tb_lancamento AS lc ON cl.id_lancamento = lc.id_lancamento 
			AND lc.dt_vencimento BETWEEN @dt_inicio 
			AND DATEADD(hh, 23, DATEADD(mi, 59, DATEADD(ss, 59, @dt_termino))) 
        JOIN dbo.tb_disciplina AS dc ON cl.id_disciplina = dc.id_disciplina
        LEFT JOIN dbo.tb_vendaproduto AS vp ON vp.id_vendaproduto = cl.id_vendaproduto
		LEFT JOIN dbo.tb_venda AS vd ON vd.id_venda = vp.id_venda AND vd.id_entidade = @id_entidade
        LEFT JOIN dbo.tb_produtoprojetopedagogico AS pp ON pp.id_produto = vp.id_produto
        LEFT JOIN dbo.tb_projetopedagogico AS ppd ON ppd.id_projetopedagogico = pp.id_projetopedagogico
WHERE   cl.bl_adiantamento = 1 AND cl.id_usuario = @id_usuario
UNION
SELECT DISTINCT 
        st_disciplina ,
        CAST(nu_saldo AS FLOAT) AS nu_valor ,
        'Saldo Anterior' AS st_tipo ,
        id_usuario, 
		id_disciplina,
		null as nu_valorvenda
FROM    dbo.fn_saldocomissaodireitos(
	@id_usuario,
	@id_entidade, 
	@dt_inicio, 
	null)
)
GO

CREATE FUNCTION [dbo].[fn_saldocomissaodireitos_coordenador] (
	@id_usuario INT,
	@id_entidade INT,
	@dt_inicio DATE,
	@dt_fim DATE
) RETURNS TABLE AS RETURN (
	SELECT
		us.id_usuario,
		ps.id_entidade,
		(ISNULL(lcc.nu_soma, 0)) AS nu_saldo,
		lcc.nu_soma AS c,
		NULL AS a,
		uper.id_projetopedagogico,
		pp.st_projetopedagogico
	FROM
		dbo.tb_usuario AS us
	JOIN tb_pessoa AS ps ON ps.id_usuario = us.id_usuario	AND ps.id_usuario = @id_usuario	AND ps.id_entidade = @id_entidade
	JOIN dbo.tb_usuarioperfilentidadereferencia AS uper ON uper.id_entidade = ps.id_entidade AND uper.id_usuario = ps.id_usuario AND uper.id_projetopedagogico IS NOT NULL
	JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = uper.id_projetopedagogico 
OUTER APPLY (
		SELECT
			SUM (cr.nu_comissaoreceber) AS nu_soma,
			cr.id_usuario,
			cr.id_projetopedagogico,
			cr.st_projetopedagogico
		FROM
			vw_comissaoreceber AS cr
		WHERE
			cr.id_usuario = us.id_usuario
		AND @id_entidade = cr.id_entidade
		AND pp.id_projetopedagogico = uper.id_projetopedagogico
		AND cr.dt_confirmacao < @dt_inicio
		GROUP BY
			cr.id_usuario,
			cr.st_projetopedagogico,
			cr.id_projetopedagogico
	) AS lcc 
--OUTER APPLY ( SELECT   cl.id_usuario, 
	--                         SUM(ISNULL(lc.nu_valor,0)) AS nu_soma,cl.id_disciplina
	--              FROM      dbo.tb_comissaolancamento AS cl
	--                        JOIN dbo.tb_lancamento AS lc ON cl.id_lancamento = lc.id_lancamento
	--                        JOIN dbo.tb_disciplina AS dc ON cl.id_disciplina = dc.id_disciplina
	--              WHERE     lc.id_entidade = ps.id_entidade
	--                        AND cl.bl_adiantamento = 1
	--                        AND cl.id_usuario = us.id_usuario AND cl.id_disciplina = uper.id_disciplina
	--                        AND lc.dt_vencimento < @dt_inicio GROUP BY cl.id_usuario, cl.id_disciplina
	--            ) AS lca
)

GO




CREATE FUNCTION [dbo].[fn_extratocomissaodireitos_coordenador] (
	@id_usuario INT,
	@id_entidade INT,
	@dt_inicio DATE,
	@dt_termino DATETIME2
) RETURNS TABLE AS RETURN (
	SELECT DISTINCT
		crs.id_projetopedagogico,
		CAST (crs.nu_valor AS FLOAT) AS nu_valor,
		'Sub Total' AS st_tipo,
		ps.id_usuario,
		crs.st_projetopedagogico,
		crs.nu_valorvenda
	FROM
		tb_pessoa AS ps CROSS APPLY (
			SELECT
				SUM (cr.nu_comissaoreceber) AS nu_valor,
				cr.id_usuario,
				cr.st_projetopedagogico,
				cr.id_projetopedagogico,
				cr.nu_valorliquido AS nu_valorvenda
			FROM
				dbo.vw_comissaodireitos AS cr
			WHERE
				ps.id_usuario = cr.id_usuario
			AND @id_entidade = cr.id_entidade
			AND dt_confirmacao BETWEEN @dt_inicio
			AND @dt_termino
			AND cr.id_funcao = 2
			GROUP BY
				cr.id_usuario,
				cr.st_projetopedagogico,
				cr.id_projetopedagogico,
				cr.nu_valorliquido
		) AS crs
	UNION
		SELECT DISTINCT
			id_projetopedagogico,
			CAST (nu_saldo AS FLOAT) AS nu_valor,
			'Saldo Anterior' AS st_tipo,
			id_usuario,
			st_projetopedagogico,
			NULL AS nu_valorvenda
		FROM
			dbo.fn_saldocomissaodireitos_coordenador (
				@id_usuario,
				@id_entidade,
				@dt_inicio,
				NULL
			)
)
GO






ALTER FUNCTION [dbo].[fn_saldocomissaodireitos]
    (
      @id_usuario INT ,
      @id_entidade INT ,
      @dt_inicio DATE ,
      @dt_fim DATE

    )
RETURNS TABLE
AS
RETURN
    ( SELECT    us.id_usuario ,
                ps.id_entidade ,
                (ISNULL(lcc.nu_soma,0) - ISNULL(lca.nu_soma,0)) AS nu_saldo
				,lcc.nu_soma AS c 
				,lca.nu_soma AS a,
				uper.id_disciplina,
				dc.st_disciplina

      FROM      dbo.tb_usuario AS us

                JOIN tb_pessoa AS ps ON ps.id_usuario = us.id_usuario
                                        AND ps.id_usuario = @id_usuario
                                        AND ps.id_entidade = @id_entidade
				JOIN dbo.tb_usuarioperfilentidadereferencia AS uper ON uper.id_entidade = ps.id_entidade AND uper.id_usuario = ps.id_usuario AND uper.id_disciplina IS NOT NULL
                JOIN dbo.tb_disciplina AS dc ON dc.id_disciplina = uper.id_disciplina
                OUTER APPLY (SELECT sum(cr.nu_comissaoreceber) AS nu_soma,cr.id_usuario, cr.st_disciplina, cr.id_disciplina 
                              FROM      vw_comissaoreceber AS cr
                              WHERE    cr.id_usuario = us.id_usuario AND @id_entidade = cr.id_entidade AND cr.id_disciplina = uper.id_disciplina
                                        AND cr.dt_confirmacao < @dt_inicio  GROUP BY cr.id_usuario, cr.st_disciplina, cr.id_disciplina 
                            ) AS lcc
                OUTER APPLY ( SELECT   cl.id_usuario, 
                                         SUM(ISNULL(lc.nu_valor,0)) AS nu_soma,cl.id_disciplina
                              FROM      dbo.tb_comissaolancamento AS cl
                                        JOIN dbo.tb_lancamento AS lc ON cl.id_lancamento = lc.id_lancamento
                                        JOIN dbo.tb_disciplina AS dc ON cl.id_disciplina = dc.id_disciplina
                              WHERE     lc.id_entidade = ps.id_entidade
                                        AND cl.bl_adiantamento = 1
                                        AND cl.id_usuario = us.id_usuario AND cl.id_disciplina = uper.id_disciplina
                                        AND lc.dt_vencimento < @dt_inicio GROUP BY cl.id_usuario, cl.id_disciplina
                            ) AS lca
               

    )





GO

ALTER VIEW [dbo].[vw_comissaoreceber] AS
SELECT DISTINCT
	uper.id_usuario,
	us.st_nomecompleto,
	us.st_cpf,
	uper.id_entidade,
	'Autor' AS st_funcao,
	4 AS id_funcao,
	mdc.id_disciplina,
	dc.st_disciplina,
	vp.id_vendaproduto,
	CAST((vp.nu_valorliquido * (mdc.nu_ponderacaoaplicada / 100) * (uper.nu_porcentagem / 100)) AS DECIMAL) AS nu_comissaoreceber,
	vp.nu_valorliquido,
	ppd.st_projetopedagogico,
	ppd.id_projetopedagogico,
	vd.dt_confirmacao
	--uper.nu_porcentagem
FROM dbo.tb_usuarioperfilentidadereferencia AS uper
JOIN dbo.tb_modulodisciplina AS mdc	ON  mdc.nu_ponderacaoaplicada IS NOT NULL AND mdc.id_disciplina = uper.id_disciplina
JOIN dbo.tb_disciplina AS dc ON dc.id_disciplina = uper.id_disciplina
JOIN dbo.tb_modulo AS md ON md.id_modulo = mdc.id_modulo AND md.bl_ativo = 1
JOIN dbo.tb_produtoprojetopedagogico AS pp	ON md.id_projetopedagogico = pp.id_projetopedagogico
JOIN dbo.tb_projetopedagogico AS ppd ON ppd.id_projetopedagogico = pp.id_projetopedagogico
JOIN dbo.tb_vendaproduto AS vp	ON  pp.id_produto = vp.id_produto AND vp.nu_valorliquido > 0
JOIN dbo.tb_venda AS vd ON vd.id_venda = vp.id_venda AND vd.id_evolucao = 10 AND vd.dt_confirmacao IS NOT NULL
JOIN dbo.tb_produto AS pd	ON vp.id_produto = pd.id_produto
JOIN dbo.tb_usuario AS us	ON us.id_usuario = uper.id_usuario
WHERE uper.nu_porcentagem IS NOT NULL AND  NOT EXISTS (SELECT
	cl.id_vendaproduto
FROM dbo.tb_comissaolancamento AS cl
WHERE cl.id_vendaproduto = vp.id_vendaproduto AND uper.id_usuario = cl.id_usuario)

UNION

SELECT DISTINCT
	uper.id_usuario,
	us.st_nomecompleto,
	us.st_cpf,
	uper.id_entidade,
	'Coordenador' AS st_funcao,
	2 AS id_funcao,
	NULL AS id_disciplina,
	NULL AS st_disciplina,
	vp.id_vendaproduto,
	CAST((vp.nu_valorliquido * (uper.nu_porcentagem / 100)) AS DECIMAL) AS nu_comissaoreceber,
	vp.nu_valorliquido,
	ppd.st_projetopedagogico,
	ppd.id_projetopedagogico,
	vd.dt_confirmacao
FROM dbo.tb_venda AS vd
JOIN dbo.tb_vendaproduto AS vp	ON vd.id_venda = vp.id_venda
JOIN dbo.tb_produto AS pd	ON vp.id_produto = pd.id_produto
JOIN dbo.tb_produtoprojetopedagogico AS pp	ON pp.id_produto = vp.id_produto
JOIN dbo.tb_projetopedagogico AS ppd ON ppd.id_projetopedagogico = pp.id_projetopedagogico
JOIN dbo.tb_modulo AS md	ON md.id_projetopedagogico = pp.id_projetopedagogico AND md.bl_ativo = 1
JOIN dbo.tb_usuarioperfilentidadereferencia AS uper	ON md.id_projetopedagogico = uper.id_projetopedagogico AND uper.nu_porcentagem IS NOT NULL
JOIN dbo.tb_usuario AS us	ON us.id_usuario = uper.id_usuario
WHERE vp.id_vendaproduto NOT IN (SELECT
	cl.id_vendaproduto
FROM dbo.tb_comissaolancamento AS cl
WHERE cl.id_vendaproduto = vp.id_vendaproduto AND uper.id_usuario = cl.id_usuario)

GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[vw_alunogradeintegracao]
AS
    SELECT DISTINCT
            'dt_abertura' = CASE WHEN sa.id_tiposaladeaula = 2
                                 THEN sa.dt_abertura
                                 WHEN sa.id_tiposaladeaula = 1
                                      AND ( CAST(sa.dt_abertura AS DATE) >= CAST(al.dt_inicio AS DATE) )
                                 THEN CAST(al.dt_cadastro AS DATE)
                                 ELSE CAST(al.dt_inicio AS DATE)
                            END ,
            mt.id_usuario ,
            us.st_nomecompleto ,
            us.st_cpf ,
            ps.st_nomeexibicao ,
            ps.st_urlavatar ,
            ai.bl_encerrado ,
            al.id_alocacao ,
            mt.id_matricula ,
            mt.id_projetopedagogico ,
            pp.st_tituloexibicao AS st_tituloexibicaoprojeto ,
            ml.id_modulo ,
            ml.st_tituloexibicao AS st_tituloexibicaomodulo ,
            md.id_evolucao ,
            md.id_disciplina ,
            dc.st_tituloexibicao AS st_disciplina ,
            al.id_saladeaula ,
            sa.id_categoriasala ,
            mt.id_entidadeatendimento AS id_entidade ,
            st_codusuario = CASE WHEN ui.st_codusuario IS NOT NULL
                                 THEN ui.st_codusuario
                                 ELSE ui2.st_codusuario
                            END ,
            st_loginintegrado = CASE WHEN ui.st_loginintegrado IS NOT NULL
                                     THEN ui.st_loginintegrado
                                     ELSE ui2.st_loginintegrado
                                END ,
            st_senhaintegrada = CASE WHEN ui.st_senhaintegrada IS NOT NULL
                                     THEN ui.st_senhaintegrada
                                     ELSE ui2.st_senhaintegrada
                                END ,
            si.st_codsistemacurso ,
            ai.st_codalocacao ,
            ai.id_sistema ,
            st_codsistemaent = CASE WHEN ei.st_codsistema IS NOT NULL
                                    THEN ei.st_codsistema
                                    ELSE ei2.st_codsistema
                               END ,
            tu1.st_nomecompleto AS st_professor ,
            tu3.st_nomecompleto AS st_coordenador ,
            st_caminho = CASE WHEN ei.st_caminho IS NOT NULL
                              THEN ei.st_caminho
                              ELSE ei2.st_caminho
                         END ,
            dc.id_tipodisciplina ,
            sa.st_saladeaula ,
            dc.st_descricao AS st_descricaodisciplina ,
            'x' AS st_integracao ,
            'id_status' = CASE WHEN al.id_alocacao IS NOT NULL
                                    AND ai.st_codalocacao IS NULL
                                    AND sa.dt_abertura < GETDATE() THEN 4
                               WHEN sa.dt_abertura > CAST(GETDATE() AS DATE)
                               THEN 3
                               WHEN CAST(sa.dt_abertura AS DATE) <= CAST(GETDATE() AS DATE)
                                    AND ( CAST(sa.dt_encerramento AS DATE) >= CAST(GETDATE() AS DATE)
                                          OR sa.dt_encerramento IS NULL
                                        ) THEN 1
                               WHEN ( sa.dt_encerramento IS NOT NULL )
                                    AND ( ( DATEADD(DAY,
                                                    ISNULL(sa.nu_diasextensao,
                                                           0),
                                                    sa.dt_encerramento) ) < CAST(GETDATE() AS DATE) )
                               THEN 2
                               WHEN sa.dt_encerramento IS NULL
                                    AND DATEADD(DAY,
                                                ISNULL(sa.nu_diasencerramento,
                                                       0)
                                                + ISNULL(sa.nu_diasaluno, 0)
                                                + ISNULL(sa.nu_diasextensao, 0),
                                                al.dt_inicio) >= CAST(GETDATE() AS DATE)
                               THEN 1
                               WHEN sa.dt_encerramento IS NULL
                                    AND DATEADD(DAY,
                                                ISNULL(sa.nu_diasencerramento,
                                                       0)
                                                + ISNULL(sa.nu_diasaluno, 0)
                                                + ISNULL(sa.nu_diasextensao, 0),
                                                al.dt_inicio) < CAST(GETDATE() AS DATE)
                               THEN 2
                               ELSE 1
                          END ,
            'st_status' = CASE WHEN sa.dt_abertura > CAST(GETDATE() AS DATE)
                               THEN 'N�o Iniciada'
                               WHEN sa.dt_abertura <= CAST(GETDATE() AS DATE)
                                    AND ( sa.dt_encerramento IS NULL
                                          OR ( (sa.dt_encerramento >= CAST(GETDATE() AS DATE)
                                               OR ( DATEADD(DAY,
                                                            ISNULL(sa.nu_diasextensao,
                                                              0),
                                                            sa.dt_encerramento) ) >= CAST(GETDATE() AS DATE) )
                                             )
                                        ) THEN 'Aberta'
                               WHEN sa.dt_encerramento < CAST(GETDATE() AS DATE)
                               THEN 'Fechada'
                               ELSE 'Erro'
                          END ,
            'dt_encerramento' = CASE WHEN sa.bl_semdiasaluno = 1
                                          AND sa.bl_semencerramento = 1
                                          AND si.id_sistema = 6
                                     THEN DATEADD(DAY, 1, GETDATE())
                                     WHEN sa.bl_semdiasaluno = 1
                                          AND sa.bl_semencerramento = 1
                                          AND si.id_sistema = 2
                                          AND md.id_evolucao != 12
                                     THEN DATEADD(DAY, 1, GETDATE())
                                     WHEN sa.bl_semdiasaluno = 1
                                          AND sa.bl_semencerramento = 1
                                          AND si.id_sistema = 2
                                          AND md.id_evolucao = 12
                                     THEN DATEADD(DAY, -1, GETDATE())
                                     WHEN sa.bl_semdiasaluno = 1
                                          AND sa.bl_semencerramento = 0
                                     THEN DATEADD(DAY,
                                                  ISNULL(sa.nu_diasencerramento,
                                                             0)

                                                      + ISNULL(sa.nu_diasextensao,
                                                              0) + ISNULL(al.nu_diasextensao,0),
                                                  sa.dt_encerramento)

                                     WHEN sa.bl_semdiasaluno = 0
                                          AND sa.bl_semencerramento = 0
                                          AND  id_tiposaladeaula = 2
                                     THEN DATEADD(DAY,
                                                  ISNULL(sa.nu_diasencerramento,
                                                             0)
                                                      + ISNULL(sa.nu_diasaluno,
                                                              0)
                                                      + ISNULL(sa.nu_diasextensao,
                                                              0) + ISNULL(al.nu_diasextensao,0),
                                                  sa.dt_abertura)
                                     WHEN sa.bl_semdiasaluno = 0
                                          AND sa.bl_semencerramento = 1
                                     THEN DATEADD(DAY, 1, GETDATE())
                                     WHEN sa.id_tiposaladeaula = 1
                                     THEN DATEADD(DAY,
                                                   + ISNULL(sa.nu_diasaluno,
                                                              0)
                                                      + ISNULL(sa.nu_diasextensao,
                                                              0) + ISNULL(al.nu_diasextensao,0),
                                                  CAST (al.dt_inicio AS DATE))
                                END ,
            'dt_encerramentosala' = CASE WHEN sa.dt_encerramento IS NOT NULL
                                         THEN DATEADD(DAY,
                                                      ISNULL(sa.nu_diasencerramento,
                                                             0)
                                                      + ISNULL(sa.nu_diasaluno,
                                                              0)
                                                      + ISNULL(sa.nu_diasextensao,
                                                              0) + ISNULL(al.nu_diasextensao,0),
                                                      sa.dt_encerramento)

                                         WHEN sa.dt_encerramento IS NULL

                                         THEN DATEADD(DAY,
                                                      ISNULL(sa.nu_diasencerramento,
                                                             0)
                                                      + ISNULL(sa.nu_diasaluno,
                                                              0)
                                                      + ISNULL(sa.nu_diasextensao,
                                                              0)+ ISNULL(al.nu_diasextensao,0), al.dt_inicio)
                                         ELSE null
                                    END ,
            al.id_situacaotcc ,
            id_aproparcial = CASE WHEN an.id_avaliacaoaluno IS NULL THEN 0
                                  ELSE 1
                             END ,
            sa.id_tiposaladeaula
    FROM    tb_matricula AS mt
            JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
            JOIN tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
            JOIN tb_modulo AS ml ON ml.id_projetopedagogico = mt.id_projetopedagogico
                                    AND ml.bl_ativo = 1
            LEFT JOIN tb_modulodisciplina AS mdc ON mdc.id_modulo = ml.id_modulo
                                                    AND mdc.id_disciplina = md.id_disciplina
                                                    AND mdc.bl_ativo = 1
            JOIN tb_alocacao AS al ON md.id_matriculadisciplina = al.id_matriculadisciplina
                                      AND al.bl_ativo = 1
            JOIN tb_disciplina AS dc ON dc.id_disciplina = md.id_disciplina
                                        AND mdc.id_disciplina = dc.id_disciplina
            JOIN tb_usuario AS us ON us.id_usuario = mt.id_usuario
            JOIN tb_pessoa AS ps ON ps.id_usuario = mt.id_usuario
                                    AND ps.id_entidade = mt.id_entidadeatendimento
            JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula
                                            AND sa.bl_ativa = 1
            LEFT JOIN tb_saladeaulaintegracao AS si ON sa.id_saladeaula = si.id_saladeaula
            LEFT JOIN tb_alocacaointegracao AS ai ON al.id_alocacao = ai.id_alocacao
            LEFT JOIN tb_usuariointegracao AS ui ON mt.id_usuario = ui.id_usuario
                                                    AND ai.id_sistema = ui.id_sistema
                                                    AND sa.id_entidade = ui.id_entidade
                                                    AND ui.id_sistema IN ( 6,
                                                              11 )
            LEFT JOIN dbo.tb_entidadeintegracao AS ei ON ei.id_entidade = sa.id_entidade
                                                         AND ei.id_sistema = si.id_sistema
                                                         AND ei.id_sistema IN (
                                                         6, 11 )
            LEFT JOIN tb_usuariointegracao AS ui2 ON mt.id_usuario = ui2.id_usuario
                                                     AND ai.id_sistema = ui2.id_sistema
                                                     AND mt.id_entidadeatendimento = ui2.id_entidade
                                                     AND ui2.id_sistema = 15
            LEFT JOIN dbo.tb_entidadeintegracao AS ei2 ON ei2.id_entidade = mt.id_entidadeatendimento
                                                          AND ei2.id_sistema = si.id_sistema
                                                          AND ei2.id_sistema = 15
            LEFT JOIN tb_usuarioperfilentidadereferencia tu ON sa.id_saladeaula = tu.id_saladeaula
                                                              AND tu.bl_titular = 1
                                                              AND tu.bl_ativo = 1
            LEFT JOIN tb_usuario tu1 ON tu.id_usuario = tu1.id_usuario
            LEFT JOIN tb_usuarioperfilentidadereferencia tu2 ON tu2.id_projetopedagogico = mt.id_projetopedagogico
                                                              AND tu2.bl_titular = 1
                                                              AND tu2.bl_ativo = 1
            LEFT JOIN tb_usuario tu3 ON tu2.id_usuario = tu3.id_usuario

  --verifica se tem aproveitamento de nota
            OUTER APPLY ( SELECT TOP 1
                                    aa.id_avaliacaoaluno
                          FROM      dbo.tb_avaliacaoaluno AS aa
                                    JOIN dbo.tb_avaliacaoconjuntoreferencia AS acr ON aa.id_avaliacaoconjuntoreferencia = acr.id_avaliacaoconjuntoreferencia
                          WHERE     aa.id_matricula = mt.id_matricula
                                    AND acr.id_saladeaula = sa.id_saladeaula
                                    AND aa.id_tiponota = 2
                                    AND aa.bl_ativo = 1
                        ) AS an
    WHERE   mt.id_evolucao IN ( 6, 15 )
GO




GO

CREATE VIEW [dbo].[vw_emissoesautorizacaoentrada] AS 
SELECT DISTINCT
	us.id_usuario,
	us.st_nomecompleto,
	us.st_cpf,
	COUNT (ae.id_autorizacaoentrada) AS nu_totalemissao,
	us.id_entidade
FROM vw_pessoa AS us
left join	tb_autorizacaoentrada AS ae on us.id_usuario = ae.id_usuario and ae.bl_ativo = 1
GROUP BY
	us.id_usuario,
	us.st_nomecompleto,
	us.st_cpf,
	us.id_entidade

	GO


ALTER VIEW [dbo].[vw_matricula]
AS
    SELECT  mt.id_usuario ,
            us.st_nomecompleto ,
            us.st_cpf ,
            ps.st_urlavatar ,
            ps.dt_nascimento ,
            ps.st_nomepai ,
            ps.st_nomemae ,
            ps.st_email ,
            CAST(ps.nu_ddd AS VARCHAR) + '-' + CAST(ps.nu_telefone AS VARCHAR) AS st_telefone ,
            CAST(ps.nu_dddalternativo AS VARCHAR) + '-'
            + CAST(ps.nu_telefonealternativo AS VARCHAR) AS st_telefonealternativo ,
            mt.id_matricula ,
            pp.st_projetopedagogico ,
            mt.id_situacao ,
            st.st_situacao ,
            mt.id_evolucao ,
            ev.st_evolucao ,
            mt.dt_concluinte ,
            mt.id_entidadematriz ,
            etmz.st_nomeentidade AS st_entidadematriz ,
            mt.id_entidadematricula ,
            etm.st_nomeentidade AS st_entidadematricula ,
            mt.id_entidadeatendimento ,
            eta.st_nomeentidade AS st_entidadeatendimento ,
            mt.bl_ativo ,
            cm.id_contrato ,
            pp.id_projetopedagogico ,
            mt.dt_termino ,
            mt.dt_cadastro ,
            mt.dt_inicio ,
            tm.id_turma ,
            tm.st_turma ,
            tm.dt_inicio AS dt_inicioturma ,
            mt.bl_institucional ,
            tm.dt_fim AS dt_terminoturma ,
            mt.id_situacaoagendamento ,
            mt.id_vendaproduto ,
            mt.dt_termino AS dt_terminomatricula ,
            CAST (RIGHT(master.dbo.fn_varbintohexstr(HASHBYTES('MD5',
                                                              ( CAST (us.id_usuario AS VARCHAR(10)) COLLATE Latin1_General_CI_AI
                                                              + CAST (( CASE
                                                              WHEN mt.id_matricula IS NULL
                                                              THEN 0
                                                              ELSE mt.id_matricula
                                                              END ) AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
                                                              + CAST (5 AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
                                                              + CAST (( 0 ) AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
                                                              + us.st_login
                                                              + us.st_senha
                                                              + CONVERT (VARCHAR(10), GETDATE(), 103) ))),
                        32) AS CHAR(32)) COLLATE Latin1_General_CI_AI AS st_urlacesso ,
            mt.bl_documentacao ,
            ct.id_venda ,
            eta.st_urlportal ,
            mt.st_codcertificacao ,
            ps.st_identificacao ,
            CONVERT(VARCHAR, mtc.dt_cadastro, 103) AS st_dtcadastrocertificacao ,
            CONVERT(VARCHAR, mtc.dt_enviocertificado, 103) AS st_dtenviocertificado ,
            CONVERT(VARCHAR, mtc.dt_retornocertificadora, 103) AS st_dtretornocertificadora ,
            CONVERT(VARCHAR, mtc.dt_envioaluno, 103) AS st_dtenvioaluno ,
            st_codigoacompanhamento ,
            mtc.id_indicecertificado ,
            mt.id_evolucaocertificacao ,
            ap.id_areaconhecimento ,
            pp.bl_disciplinacomplementar ,
            mt.id_matriculavinculada,
			cr.id_contratoregra,
			trc.id_tiporegracontrato
    FROM    tb_matricula mt
            JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
            JOIN tb_areaprojetopedagogico AS ap ON ap.id_projetopedagogico = pp.id_projetopedagogico
            JOIN tb_entidade AS etmz ON etmz.id_entidade = mt.id_entidadematriz
            JOIN tb_entidade AS etm ON etm.id_entidade = mt.id_entidadematricula
            JOIN tb_entidade AS eta ON eta.id_entidade = mt.id_entidadeatendimento
            JOIN tb_situacao AS st ON st.id_situacao = mt.id_situacao
            JOIN tb_evolucao AS ev ON ev.id_evolucao = mt.id_evolucao
            JOIN tb_usuario AS us ON us.id_usuario = mt.id_usuario
            LEFT JOIN tb_contratomatricula AS cm ON cm.id_matricula = mt.id_matricula
                                                    AND cm.bl_ativo = 1
            LEFT JOIN vw_pessoa AS ps ON mt.id_usuario = ps.id_usuario
                                         AND mt.id_entidadeatendimento = ps.id_entidade
            LEFT JOIN dbo.tb_contrato AS ct ON ct.id_contrato = cm.id_contrato
                                               AND ct.bl_ativo = 1
            LEFT JOIN tb_turma AS tm ON tm.id_turma = mt.id_turma
            LEFT JOIN tb_matriculacertificacao mtc ON mtc.id_matricula = mt.id_matricula
                                                      AND mtc.bl_ativo = 1
            LEFT JOIN dbo.tb_contratoregra AS cr ON cr.id_contratoregra = ct.id_contratoregra
            LEFT JOIN dbo.tb_tiporegracontrato AS trc ON trc.id_tiporegracontrato = cr.id_tiporegracontrato
    WHERE   mt.bl_ativo = 1



GO



/****** Object:  View [dbo].[vw_usuarioperfilpedagogico]    Script Date: 20/10/2014 09:46:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





ALTER VIEW [dbo].[vw_usuarioperfilpedagogico] AS 
SELECT DISTINCT
	us.st_nomecompleto,
	us.id_usuario,
	us.st_login,
	us.st_senha,
	dac.st_login AS st_loginentidade,
	dac.st_senha AS st_senhaentidade,
	us.st_email,
	ps.st_nomeexibicao,
	ps.st_urlavatar,
	ps.dt_nascimento,
	upe.id_perfil,
	upe.id_entidade,
	pf.st_nomeperfil,
	ppg.st_perfilpedagogico,
	pf.id_perfilpedagogico,
	upe.dt_inicio,
	upe.dt_termino,
	pp.id_projetopedagogico,
	pp.id_trilha,
	pp.st_tituloexibicao,
	uper.id_saladeaula,
	NULL AS st_saladeaula,
	mt.id_matricula,
	mt.id_evolucao as id_evolucaomatricula,
	tm.dt_inicio AS dt_inicioturma,
	et.st_nomeentidade,
	et.st_razaosocial,
	et.st_urlimglogo,
	ce.bl_bloqueiadisciplina,
	CAST (
		RIGHT (
			master.dbo.fn_varbintohexstr (
				HASHBYTES (
					'MD5',
					(
						CAST (us.id_usuario AS VARCHAR(10)) COLLATE Latin1_General_CI_AI + CAST (
							(
								CASE
								WHEN mt.id_matricula IS NULL THEN
									0
								ELSE
									mt.id_matricula
								END
							) AS VARCHAR (15)
						) COLLATE Latin1_General_CI_AI + CAST (
							pf.id_perfilpedagogico AS VARCHAR (15)
						) COLLATE Latin1_General_CI_AI + CAST ((0) AS VARCHAR(15)) COLLATE Latin1_General_CI_AI + us.st_login + us.st_senha + CONVERT (VARCHAR(10), GETDATE(), 103)
					)
				)
			),
			32
		) AS CHAR (32)
	) COLLATE Latin1_General_CI_AI AS st_urlacesso,
	cd.id_entidadeorigem AS id_entidadeportal
FROM
	vw_pessoa us
JOIN tb_usuarioperfilentidade AS upe ON upe.id_usuario = us.id_usuario
AND us.id_entidade = upe.id_entidade
JOIN tb_perfil AS pf ON pf.id_perfil = upe.id_perfil AND pf.id_entidade = upe.id_entidade
JOIN tb_perfilpedagogico AS ppg ON ppg.id_perfilpedagogico = pf.id_perfilpedagogico
LEFT JOIN tb_usuarioperfilentidadereferencia AS uper ON uper.id_usuario = upe.id_usuario
AND uper.id_perfil = upe.id_perfil
AND upe.id_entidade = uper.id_entidade AND pf.id_entidade = uper.id_entidade
LEFT JOIN tb_matricula AS mt ON mt.id_usuario = us.id_usuario
AND mt.id_entidadeatendimento = upe.id_entidade
AND (
	(
		pf.id_perfilpedagogico = 5
		AND mt.bl_institucional = 0
	)
	OR (
		pf.id_perfilpedagogico = 6
		AND mt.bl_institucional = 1
	)
)
AND mt.bl_ativo = 1
AND mt.id_evolucao NOT IN (20, 27, 41)
AND (
	mt.dt_termino IS NULL
	OR mt.dt_termino >= CAST (GETDATE() AS DATE)
)
LEFT JOIN tb_turma AS tm ON mt.id_turma = tm.id_turma
LEFT JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_entidade AS et ON et.id_entidade = upe.id_entidade
JOIN tb_pessoa AS ps ON ps.id_usuario = us.id_usuario
AND ps.id_entidade = upe.id_entidade
LEFT JOIN tb_dadosacesso AS dac ON dac.id_usuario = us.id_usuario
AND dac.id_entidade = ps.id_entidade
AND dac.bl_ativo = 1
LEFT JOIN tb_configuracaoentidade AS ce ON ce.id_entidade = et.id_entidade
LEFT JOIN dbo.tb_compartilhadados AS cd ON cd.id_entidadedestino = et.id_entidade
AND cd.id_tipodados = 2
WHERE
	upe.bl_ativo = 1
AND pf.bl_ativo = 1
AND upe.id_situacao = 15
AND pf.id_situacao = 4

GO

ALTER VIEW [dbo].[vw_usuarioscomissionados] as

SELECT DISTINCT
        uper.id_usuario,
        uper.id_entidade,
        us.st_nomecompleto,
				pp.id_perfilpedagogico as id_funcao,
        pp.st_perfilpedagogico AS st_funcao,
        us.st_cpf,
        pp.st_perfilpedagogico

FROM dbo.tb_usuarioperfilentidadereferencia AS uper
JOIN dbo.tb_perfil AS p ON uper.id_perfil = p.id_perfil
JOIN dbo.tb_perfilpedagogico AS pp      ON p.id_perfilpedagogico = pp.id_perfilpedagogico AND pp.id_perfilpedagogico IN (4, 2)
JOIN dbo.tb_usuario AS us       ON uper.id_usuario = us.id_usuario

GO

ALTER VIEW [dbo].[vw_vendalancamento] as

select DISTINCT vd.id_venda, vd.id_usuario AS id_usuariovenda

/**
Este campo na tela de venda exibe o Respons�vel Financeiro, mas antes n�o mostrava do campo certo
**/
, case when usf.st_nomecompleto is null then us.st_nomecompleto else usf.st_nomecompleto end as st_nomecompleto
, us.st_nomecompleto as st_nomeusuario
, vd.id_evolucao, vd.id_formapagamento
, vd.nu_parcelas
, vd.nu_valorliquido, lv.bl_entrada, lv.nu_ordem, lc. id_lancamento ,
        lc.nu_valor ,
        CAST('0.00' AS NUMERIC(7,2)) AS nu_valoratualizado,
        lc.nu_vencimento ,
        lc.id_tipolancamento ,
        lc.dt_cadastro ,
        lc.id_meiopagamento ,
        lc.id_usuariolancamento ,
        lc.id_entidadelancamento,
        lc.id_entidade ,
        lc.id_usuariocadastro ,
        lc.st_banco ,
        lc.id_cartaoconfig ,
        lc.id_boletoconfig ,
        lc.bl_quitado ,
        lc.dt_vencimento ,
        lc.dt_quitado ,
        lc.dt_emissao ,
        lc.dt_prevquitado ,
        lc.st_emissor ,
        lc.st_coddocumento ,
        lc.nu_juros ,
        lc.nu_desconto ,
        lc.nu_quitado ,
        lc.nu_multa , 
        lc.nu_verificacao ,
        mp.st_meiopagamento,
        fp.nu_juros AS nu_jurosatraso,
        fp.nu_multa AS nu_multaatraso,
        vmv.nu_diavencimento,
        tl.st_tipolancamento,
        lc.bl_ativo,
        li.st_codlancamento,
        lc.bl_original,
        vd.recorrente_orderid,
        lc.st_retornoverificacao,
		tc.id_campanhacomercial,
		lc.nu_cartao
from tb_venda as vd

JOIN dbo.tb_usuario AS us ON us.id_usuario = vd.id_usuario

JOIN tb_lancamentovenda as lv ON lv.id_venda = vd.id_venda

JOIN tb_lancamento as lc ON lc.id_lancamento = lv.id_lancamento AND lc.bl_ativo = 1

JOIN dbo.tb_formapagamento AS fp ON fp.id_formapagamento = vd.id_formapagamento

left JOIN dbo.tb_usuario AS usf ON usf.id_usuario = lc.id_usuariolancamento

LEFT JOIN tb_meiopagamento as mp ON lc.id_meiopagamento = mp.id_meiopagamento

LEFT JOIN tb_vendameiovencimento as vmv ON vd.id_venda = vmv.id_venda and vmv.id_meiopagamento = mp.id_meiopagamento

JOIN dbo.tb_tipolancamento AS tl ON tl.id_tipolancamento = lc.id_tipolancamento

LEFT JOIN dbo.tb_lancamentointegracao AS li ON li.id_lancamento = lc.id_lancamento

LEFT JOIN tb_campanhacomercial as tc  ON vd.id_campanhacomercial = tc.id_campanhacomercial and tc.id_categoriacampanha = 3

GO



/****** Object:  View [rel].[vw_matricula]    Script Date: 07/04/2015 12:08:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





ALTER VIEW [rel].[vw_matricula]
AS
    SELECT DISTINCT
            mt.id_matricula ,
            mt.dt_inicio ,
            ct.id_contrato ,
            us.st_nomecompleto ,
            us.st_cpf ,
            p.st_email ,
            CAST(p.nu_ddd AS VARCHAR) + '-' + CAST(p.nu_telefone AS VARCHAR) AS st_telefone ,
            ac.id_areaconhecimento ,
            ac.st_areaconhecimento ,
            pp.id_projetopedagogico ,
            pp.st_projetopedagogico ,
			st.id_situacao ,
            st.st_situacao ,
			ev.id_evolucao ,
            ev.st_evolucao ,
            usr.st_nomecompleto AS st_representante ,
            usa.st_nomecompleto AS st_atendente ,
            vp.nu_valorliquido AS nu_valor ,
            mt.id_entidadeatendimento ,
            p.sg_uf ,
            tm.st_turma, tm.id_turma,
			cast(mt.dt_cadastro as date) as dt_cadastro,
			cast(mt.dt_concluinte as date) as dt_concluinte
    FROM    dbo.tb_matricula mt
            JOIN dbo.tb_usuario AS us ON us.id_usuario = mt.id_usuario
            INNER JOIN vw_pessoa p ON p.id_entidade = mt.id_entidadeatendimento
                                      AND p.id_usuario = mt.id_usuario
            JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
            JOIN dbo.tb_areaprojetopedagogico AS ap ON ap.id_projetopedagogico = pp.id_projetopedagogico
            OUTER APPLY ( SELECT TOP 1
                                    id_areaconhecimento ,
                                    st_areaconhecimento
                          FROM      dbo.tb_areaconhecimento
                          WHERE     id_areaconhecimento = ap.id_areaconhecimento
                                    AND id_tipoareaconhecimento = 1
                        ) AS ac
            JOIN dbo.tb_contratomatricula AS cm ON cm.id_matricula = mt.id_matricula
                                                   AND cm.bl_ativo = 1
            JOIN dbo.tb_contrato AS ct ON ct.id_contrato = cm.id_contrato
                                          AND ct.bl_ativo = 1
            JOIN dbo.tb_venda AS vd ON ct.id_venda = vd.id_venda
            JOIN dbo.tb_vendaproduto AS vp ON vp.id_venda = vd.id_venda
            LEFT JOIN dbo.tb_atendentevenda AS av ON av.id_venda = vd.id_venda
            LEFT JOIN dbo.tb_usuario AS usa ON usa.id_usuario = av.id_usuario
            LEFT JOIN dbo.tb_vendaenvolvido AS ve ON ve.id_venda = vd.id_venda
                                                     AND ve.id_funcao = 1
            LEFT JOIN dbo.tb_usuario AS usr ON usr.id_usuario = ve.id_usuario
            JOIN dbo.tb_situacao AS st ON st.id_situacao = mt.id_situacao
            JOIN dbo.tb_evolucao AS ev ON ev.id_evolucao = mt.id_evolucao
            LEFT JOIN dbo.tb_turma AS tm ON tm.id_turma = mt.id_turma
    WHERE   mt.bl_ativo = 1
           -- AND mt.bl_institucional = 0






GO



/****** Object:  View [rel].[vw_vendaconfirmadasprodutos]    Script Date: 28/03/2014 14:02:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




ALTER VIEW [rel].[vw_vendaconfirmadasprodutos] as
SELECT  st_nomecompleto ,
        st_email ,
        st_cpf ,
        CAST(nu_ddd AS VARCHAR) + CAST(nu_telefone AS VARCHAR) AS st_telefone ,
        st_endereco ,
        st_cep ,
        st_bairro ,
        st_complemento ,
        nu_numero ,
        st_cidade ,
        us.sg_uf ,
        pd.st_produto ,
        pd.id_produto ,
        vp.nu_valorliquido ,
        vd.nu_valorliquido AS nu_valorliquidovenda ,
        vd.dt_confirmacao ,
        mp.st_meiopagamento ,
        pd.id_tipoproduto ,
		vd.id_venda,
        vd.id_evolucao ,
        ve.id_entidade,
        vd.dt_cadastro,
		pdc.st_produto as st_combo,
		pdc.id_produto as id_produtocombo,
		ve.st_nomeentidade,
		te.id_entidade AS id_entidadepai,
		vd.st_observacao,
		vd.id_cupom
FROM    tb_entidade AS te
        JOIN dbo.vw_entidaderecursivaid AS ve ON ve.nu_entidadepai = te.id_entidade OR ve.id_entidade = te.id_entidade
		JOIN dbo.tb_venda AS vd ON vd.id_entidade = ve.id_entidade
        JOIN dbo.vw_pessoa AS us ON us.id_usuario = vd.id_usuario
                                    AND us.id_entidade = vd.id_entidade
        LEFT JOIN dbo.tb_municipio AS mn ON mn.id_municipio = us.id_municipio
        JOIN dbo.tb_vendaproduto AS vp ON vd.id_venda = vp.id_venda
        JOIN dbo.tb_produto AS pd ON pd.id_produto = vp.id_produto
        LEFT JOIN dbo.tb_lancamentovenda AS lv ON lv.id_venda = vd.id_venda
                                                  AND bl_entrada = 1
        LEFT JOIN dbo.tb_lancamento AS lc ON lc.id_lancamento = lv.id_lancamento
        LEFT JOIN dbo.tb_meiopagamento AS mp ON mp.id_meiopagamento = lc.id_meiopagamento
		LEFT JOIN tb_produto as pdc ON pdc.id_produto =vp.id_produtocombo WHERE vd.bl_ativo = 1





GO


 ALTER VIEW [dbo].[vw_resumofinanceiro] AS
    SELECT DISTINCT
        l.id_lancamento,
        lv.id_venda,
        lv.bl_entrada,
        l.st_banco,
        l.st_emissor,
        l.dt_prevquitado,
        l.st_coddocumento,
        ed.st_endereco,
        ed.st_cidade,
        ed.st_bairro,
        ed.st_estadoprovincia,
        ed.st_cep ,
        ed.sg_uf,
        ed.nu_numero,
        ed.id_tipoendereco,
        l.bl_quitado,
        l.dt_quitado,
        CAST(l.nu_quitado AS DECIMAL(10,2)) AS nu_quitado,
        st_entradaparcela = CASE lv.bl_entrada WHEN 1 THEN 'Entrada' ELSE 'Parcelas' END,
         lv.nu_ordem AS nu_parcela,
        l.id_meiopagamento,
        mp.st_meiopagamento,
        CASE
        WHEN ISNULL(l.nu_juros,0) > 0 OR ISNULL(l.nu_desconto,0) > 0
		THEN ISNULL(l.nu_valor,0) + ISNULL(l.nu_juros,0) - ISNULL(l.nu_desconto,0)
		ELSE l.nu_valor
		END AS nu_valor,
        l.nu_vencimento,
        l.dt_vencimento,
		l.st_autorizacao,
		CASE WHEN l.st_ultimosdigitos IS NULL THEN tf.st_ultimosdigitos
		-- case when tf.st_ultimosdigitos  IS null then 'N�o informados' else tf.st_ultimosdigitos end
		ELSE l.st_ultimosdigitos END AS st_ultimosdigitos,
		-- l.st_ultimosdigitos,
        st_situacao = CASE
                WHEN CAST(l.dt_vencimento AS DATE) < CAST(GETDATE() AS DATE) AND l.bl_quitado = 0 AND l.id_meiopagamento NOT IN (1,7) AND l.bl_ativo = 1 THEN 'Vencido'
                WHEN CAST(l.dt_vencimento AS DATE) < CAST(GETDATE() AS DATE) AND l.bl_quitado = 0 AND l.id_meiopagamento IN (1,7) AND l.dt_prevquitado   IS NULL AND l.bl_ativo = 1 THEN 'Atrasado'
                WHEN l.id_meiopagamento IN (1,7) AND l.dt_prevquitado IS NOT NULL  AND l.bl_ativo = 1 THEN 'Autorizado'
                WHEN l.bl_quitado = 1  AND l.bl_ativo = 1 THEN 'Pago'
                WHEN l.bl_ativo = 0 THEN 'Cancelado'
                ELSE 'Pendente'
                END,
        --u.id_usuario,
       -- u.st_nomecompleto,

        CASE WHEN u.id_usuario IS NOT NULL THEN u.id_usuario
			 WHEN el.id_entidade IS NOT NULL THEN el.id_entidade END AS id_usuario,

		CASE WHEN u.st_nomecompleto IS NOT NULL THEN u.st_nomecompleto
			 WHEN el.st_nomeentidade IS NOT NULL THEN el.st_nomeentidade COLLATE DATABASE_DEFAULT END AS st_nomecompleto,

        l.id_entidade,
        ef.id_textosistemarecibo,
        ef.id_reciboconsolidado,
        l.st_nossonumero,
        CASE WHEN tf.st_codtransacaooperadora IS NOT NULL THEN tf.st_codtransacaooperadora
			 WHEN l.st_coddocumento IS NOT NULL THEN l.st_coddocumento END AS st_codtransacaooperadora,
        l.bl_original,
        l.bl_ativo,
        l.dt_atualizado,
        l.st_numcheque,
		l.nu_juros,
		l.nu_desconto,
		l.nu_jurosboleto,
		l.nu_descontoboleto,
		l.id_acordo,
		v.recorrente_orderid,
		v.dt_cadastro AS dt_venda,
		si.st_situacao AS st_situacaopedido,
		CASE WHEN si3.st_situacao IS NOT NULL THEN si3.st_situacao
			 WHEN l.bl_quitado = 0 THEN 'PENDENTE'
			 WHEN l.bl_quitado = 1 THEN 'PAGA' END AS st_situacaolancamento,

		CASE WHEN sl.id_venda IS NOT NULL THEN 1 ELSE 0 END AS bl_cartaovencendo,
		l.bl_chequedevolvido,
		CASE WHEN rec.bl_recorrente > 0 THEN 1 ELSE 0 END AS bl_recorrente
    FROM tb_lancamento l
        INNER JOIN tb_lancamentovenda AS lv ON lv.id_lancamento = l.id_lancamento
        INNER JOIN tb_meiopagamento AS mp ON mp.id_meiopagamento = l.id_meiopagamento
        INNER JOIN tb_venda AS v ON v.id_venda = lv.id_venda
        --INNER JOIN tb_contrato as c ON c.id_venda = v.id_venda
        INNER JOIN tb_vendaproduto AS vp ON vp.id_venda = v.id_venda
        INNER JOIN tb_produto AS p ON p.id_produto = vp.id_produto
        --LEFT JOIN tb_usuario AS u ON u.id_usuario = l.id_usuariolancamento
        --OUTER APPLY (SELECT TOP 1 * FROM  tb_pessoaendereco where id_usuario = u.id_usuario AND id_entidade = l.id_entidade AND bl_padrao = 1) AS pe
        --LEFT JOIN tb_endereco AS ed ON ed.id_endereco = pe.id_endereco
        LEFT JOIN tb_entidadefinanceiro ef ON ef.id_entidade = v.id_entidade
        LEFT JOIN tb_transacaolancamento AS tl ON l.id_lancamento = tl.id_lancamento
        LEFT JOIN tb_transacaofinanceira AS tf ON tl.id_transacaofinanceira = tf.id_transacaofinanceira
        LEFT JOIN tb_pedidointegracao AS pint ON pint.id_venda = v.id_venda
        LEFT JOIN tb_situacaointegracao AS si ON si.id_situacaointegracao = pint.id_situacaointegracao

        OUTER APPLY ( SELECT TOP 1 si2.st_situacao
							FROM tb_transacaolancamento AS tl
							INNER JOIN tb_transacaofinanceira AS tf ON tl.id_transacaofinanceira = tf.id_transacaofinanceira
							INNER JOIN tb_situacaointegracao AS si2 ON si2.id_situacaointegracao = tf.id_situacaointegracao
							WHERE tl.id_lancamento = L.id_lancamento
							ORDER BY tf.dt_cadastro DESC
					) AS si3

        OUTER APPLY (
		   SELECT st_nomecompleto, us.id_usuario , pe.id_endereco
				FROM tb_usuario us
				JOIN tb_pessoaendereco pe ON pe.id_usuario = us.id_usuario
				WHERE us.id_usuario = id_usuariolancamento
				AND pe.id_entidade = v.id_entidade
				AND bl_padrao = 1
		) AS u

		OUTER APPLY (
		   SELECT TOP 1 st_nomeentidade, e.id_entidade , ee.id_endereco
		   FROM tb_entidade e
		   JOIN tb_entidadeendereco ee ON e.id_entidade = ee.id_entidade
		   WHERE e.id_entidade = id_entidadelancamento
		   --AND ee.id_entidade = v.id_entidade
		   --AND bl_padrao = 1
		) AS el

		OUTER APPLY(
			SELECT DISTINCT vv.id_venda
				FROM tb_venda vv
					INNER JOIN tb_pedidointegracao pit ON pit.id_venda = vv.id_venda
					INNER JOIN tb_lancamentovenda lvs ON lvs.id_venda = vv.id_venda
					INNER JOIN tb_lancamento ls ON ls.id_lancamento = lvs.id_lancamento
				WHERE vv.id_venda = v.id_venda
					AND pit.id_sistema = 10
					AND CAST(ls.dt_vencimento AS DATE) >= CAST(pit.dt_vencimentocartao AS DATE)
					AND DATEDIFF(DAY, GETDATE(), pit.dt_vencimentocartao) <= 30
		) sl
		
		OUTER APPLY(
			SELECT COUNT(id_meiopagamento) AS bl_recorrente 
				FROM tb_lancamento ll 
				INNER JOIN tb_lancamentovenda lvv ON ll.id_lancamento = lvv.id_lancamento
				WHERE lvv.id_venda = v.id_venda
				AND ll.id_meiopagamento = 11
				AND ll.bl_quitado = 0
				AND ll.bl_ativo = 1
				--AND ll.dt_vencimento > CAST(GETDATE() AS DATE)
		) rec

		LEFT JOIN tb_endereco AS ed ON ed.id_endereco = CASE WHEN u.id_endereco IS NOT NULL THEN u.id_endereco WHEN el.id_endereco IS NOT NULL THEN el.id_endereco END






GO

Alter VIEW [dbo].[vw_horarioaula] AS
SELECT DISTINCT
	h.id_horarioaula,
	h.id_codhorarioacesso,
	h.st_codhorarioacesso,
	h.st_horarioaula,
	h.hr_inicio,
	h.hr_fim,
	t.id_turno,
	st_turno,
	id_entidade,
	bl_ativo,
	(CASE WHEN segunda.id_diasemana IS NOT NULL THEN 1 ELSE 0 END) AS bl_segunda,
	(CASE WHEN terca.id_diasemana IS NOT NULL THEN 1 ELSE 0 END) AS bl_terca,
	(CASE WHEN quarta.id_diasemana IS NOT NULL THEN 1 ELSE 0 END) AS bl_quarta,
	(CASE WHEN quinta.id_diasemana IS NOT NULL THEN 1 ELSE 0 END) AS bl_quinta,
	(CASE WHEN sexta.id_diasemana IS NOT NULL THEN 1 ELSE 0 END) AS bl_sexta,
	(CASE WHEN sabado.id_diasemana IS NOT NULL THEN 1 ELSE 0 END) AS bl_sabado,
	(CASE WHEN domingo.id_diasemana IS NOT NULL THEN 1 ELSE 0 END) AS bl_domingo
	
FROM  
	dbo.tb_horarioaula h
	INNER JOIN dbo.tb_turno t ON t.id_turno = h.id_turno
      
    OUTER APPLY ( SELECT DISTINCT hd1.id_diasemana FROM dbo.tb_horariodiasemana hd1
                   WHERE hd1.id_horarioaula = h.id_horarioaula AND hd1.id_diasemana = 1
                    ) AS segunda
                    
	OUTER APPLY ( SELECT DISTINCT hd2.id_diasemana FROM dbo.tb_horariodiasemana hd2
                   WHERE hd2.id_horarioaula = h.id_horarioaula AND hd2.id_diasemana = 2
                    ) AS terca
                    
	OUTER APPLY ( SELECT DISTINCT hd3.id_diasemana FROM dbo.tb_horariodiasemana hd3
                   WHERE hd3.id_horarioaula = h.id_horarioaula AND hd3.id_diasemana = 3
                    ) AS quarta
                    
    OUTER APPLY ( SELECT DISTINCT hd4.id_diasemana FROM dbo.tb_horariodiasemana hd4
                   WHERE hd4.id_horarioaula = h.id_horarioaula AND hd4.id_diasemana = 4
                    ) AS quinta
                    
    OUTER APPLY ( SELECT DISTINCT hd5.id_diasemana FROM dbo.tb_horariodiasemana hd5
                   WHERE hd5.id_horarioaula = h.id_horarioaula AND hd5.id_diasemana = 5
                    ) AS sexta
                    
    OUTER APPLY ( SELECT DISTINCT hd6.id_diasemana FROM dbo.tb_horariodiasemana hd6
                   WHERE hd6.id_horarioaula = h.id_horarioaula AND hd6.id_diasemana = 6
                    ) AS sabado
                    
    OUTER APPLY ( SELECT DISTINCT hd7.id_diasemana FROM dbo.tb_horariodiasemana hd7
                   WHERE hd7.id_horarioaula = h.id_horarioaula AND hd7.id_diasemana = 7
                    ) AS domingo
                    
					WHERE bl_ativo = 1


GO
UPDATE dbo.tb_funcionalidade SET st_urlcaminho = '/item-de-material-presencial/entrega' WHERE id_funcionalidade = 679
GO


ALTER VIEW [dbo].[vw_usuarioperfilentidadereferencia]
AS
SELECT 
		upr.id_perfilreferencia
		,upr.id_usuario
		,upr.id_entidade
		,upr.id_perfil
		,upr.id_areaconhecimento
		,upr.id_projetopedagogico
        ,pp.st_projetopedagogico
		,upr.id_saladeaula
		,upr.id_disciplina
		,upr.id_livro
		,upr.bl_ativo
		,upr.bl_titular
		,upr.bl_autor
		,u.st_nomecompleto,
		sa.st_saladeaula,
		pr.id_perfilreferenciaintegracao
		,upr.nu_porcentagem,
		pf.id_perfilpedagogico
	FROM dbo.tb_usuarioperfilentidadereferencia AS upr
	JOIN tb_perfil AS pf ON pf.id_perfil = upr.id_perfil
	INNER JOIN dbo.tb_usuario AS u ON u.id_usuario = upr.id_usuario
    LEFT JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = upr.id_projetopedagogico
    LEFT JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = upr.id_saladeaula
    LEFT JOIN dbo.tb_perfilreferenciaintegracao AS pr ON pr.id_perfilreferencia = upr.id_perfilreferencia AND upr.id_saladeaula IS NOT NULL
	WHERE upr.bl_ativo = 1

	GO

    ALTER TABLE dbo.tb_itemdematerial ADD nu_qtdestoque INT NULL

	GO

	alter  VIEW [dbo].[vw_materialturmadisciplina] AS

SELECT im.id_itemdematerial ,
       im.st_itemdematerial ,
       im.id_situacao ,
       im.id_tipodematerial ,
       im.dt_cadastro ,
       im.id_usuariocadastro ,
       im.nu_qtdepaginas ,
       im.nu_peso ,
       im.id_upload ,
       im.nu_valor ,
       im.bl_portal ,
       im.nu_encontro ,
       im.id_professor ,
       im.bl_ativo ,
       im.id_entidade,
	   im.nu_qtdestoque,
mat.st_nomeentidade AS st_entidade,
dis.id_disciplina AS id_disciplina,
dis.st_disciplina AS st_disciplina,
us.st_nomecompleto AS st_professor,
tip.st_tipodematerial AS st_tipodematerial,
sit.st_situacao AS st_situacao,
tur.id_turma,
tur.st_turma
FROM tb_itemdematerial AS im
JOIN tb_itemdematerialdisciplina AS idi ON idi.id_itemdematerial = im.id_itemdematerial
JOIN tb_itemdematerialturma AS imt ON im.id_itemdematerial = imt.id_material AND imt.bl_ativo = 1
JOIN tb_turma AS tur ON tur.id_turma = imt.id_turma
JOIN tb_entidade AS mat ON mat.id_entidade = im.id_entidade
JOIN tb_disciplina AS dis ON dis.id_disciplina = idi.id_disciplina
JOIN tb_usuario AS us ON us.id_usuario = im.id_professor
JOIN tb_tipodematerial AS tip ON tip.id_tipodematerial = im.id_tipodematerial
JOIN tb_situacao AS sit ON sit.id_situacao = im.id_situacao

	GO
    

ALTER FUNCTION [dbo].[fn_extratocomissaodireitos]
(	
@id_usuario INT,
@id_entidade INT,
@dt_inicio DATE,
@dt_termino DATETIME2
)
RETURNS TABLE 
AS


RETURN 
(
SELECT  DISTINCT crs.st_disciplina ,
        CAST(crs.nu_valor AS FLOAT) AS nu_valor,
        'Sub Total' AS st_tipo,
        ps.id_usuario, 
		crs.id_disciplina,
		crs.nu_valorvenda
		FROM tb_pessoa AS ps
		CROSS APPLY (
SELECT 
				SUM(cr.nu_comissaoreceber) AS nu_valor, 
				SUM(cr.nu_valorliquido) AS nu_valorvenda,
				cr.id_usuario, 
				cr.st_disciplina, 
				cr.id_disciplina
			FROM dbo.vw_comissaodireitos AS cr 
			WHERE ps.id_usuario = cr.id_usuario 
			AND @id_entidade = cr.id_entidade 
			AND dt_confirmacao BETWEEN @dt_inicio  AND @dt_termino AND cr.id_funcao = 4
			GROUP BY cr.id_usuario, cr.st_disciplina, cr.id_disciplina
		) AS crs 
		WHERE ps.id_usuario = @id_usuario

UNION
SELECT DISTINCT 
        dc.st_disciplina ,
        CAST(lc.nu_valor AS FLOAT) AS nu_valor,
        'Adiantamento' AS st_tipo ,
        cl.id_usuario,
		cl.id_disciplina,
		NULL AS nu_valorvenda
FROM    dbo.tb_comissaolancamento AS cl
        JOIN dbo.tb_lancamento AS lc ON cl.id_lancamento = lc.id_lancamento 
			AND lc.dt_vencimento BETWEEN @dt_inicio 
			AND DATEADD(hh, 23, DATEADD(mi, 59, DATEADD(ss, 59, @dt_termino))) 
        JOIN dbo.tb_disciplina AS dc ON cl.id_disciplina = dc.id_disciplina
        LEFT JOIN dbo.tb_vendaproduto AS vp ON vp.id_vendaproduto = cl.id_vendaproduto
		LEFT JOIN dbo.tb_venda AS vd ON vd.id_venda = vp.id_venda AND vd.id_entidade = @id_entidade
        LEFT JOIN dbo.tb_produtoprojetopedagogico AS pp ON pp.id_produto = vp.id_produto
        LEFT JOIN dbo.tb_projetopedagogico AS ppd ON ppd.id_projetopedagogico = pp.id_projetopedagogico
WHERE   cl.bl_adiantamento = 1 AND cl.id_usuario = @id_usuario
UNION
SELECT DISTINCT 
        st_disciplina ,
        CAST(nu_saldo AS FLOAT) AS nu_valor ,
        'Saldo Anterior' AS st_tipo ,
        id_usuario, 
		id_disciplina,
		NULL AS nu_valorvenda
FROM    dbo.fn_saldocomissaodireitos(
	@id_usuario,
	@id_entidade, 
	@dt_inicio, 
	NULL)
)

go
ALTER TABLE [dbo].[tb_sistema] ADD [st_conexao] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
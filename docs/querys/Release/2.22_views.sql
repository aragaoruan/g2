
--docs/vw_sql/dbo.vw_alunosagendamento.View.sql
ALTER VIEW [dbo].[vw_alunosagendamento] as
SELECT DISTINCT
	u.id_usuario,
	u.st_nomecompleto,
	u.st_cpf,
	u.st_email,
	m.id_situacaoagendamento,
	m.id_matricula,
	ap.id_aplicadorprova,
	ap.st_aplicadorprova,
	avp.bl_ativo,
	avp.dt_cadastro,
	avp.bl_unica,
	avp.dt_antecedenciaminima,
	avp.dt_alteracaolimite,
	avp.dt_aplicacao,
	avp.id_usuariocadastro,
	avp.id_horarioaula,
	avp.nu_maxaplicacao,
	avp.id_endereco,
	avp.id_avaliacaoaplicacao,
	pp.st_projetopedagogico,
	m.id_projetopedagogico,
	u.id_entidade,
	vw_end.sg_uf,
	ta.bl_ativo AS bl_ativoavagendamento,
	ta.id_avaliacaoagendamento
FROM vw_pessoa AS u
	JOIN tb_matricula AS m
		ON m.id_usuario = u.id_usuario
			 AND m.bl_ativo = 1
			 AND m.id_entidadeatendimento = u.id_entidade
			 AND m.id_situacaoagendamento = 120
			 AND m.id_evolucao = 6
	JOIN tb_projetopedagogico AS pp
		ON pp.id_projetopedagogico = m.id_projetopedagogico
	outer APPLY(select top 1 gt.id_avaliacao from dbo.vw_avaliacaoaluno AS gt
	where gt.id_matricula = m.id_matricula
				AND gt.id_evolucaodisciplina IN (13, 19)
				AND gt.nu_cargahoraria != 0
				AND gt.id_tipoavaliacao = 4) as aa
	LEFT JOIN dbo.tb_avaliacaoagendamento AS ta
		ON ta.id_matricula = m.id_matricula
			 AND ta.id_situacao NOT IN (70, 69)
			 AND ta.nu_presenca IS NULL
			 and ta.id_avaliacao = aa.id_avaliacao and ta.bl_ativo = 1
	LEFT JOIN tb_avaliacaoaplicacao AS avp
		ON avp.id_avaliacaoaplicacao = ta.id_avaliacaoaplicacao
	LEFT JOIN tb_aplicadorprova AS ap
		ON ap.id_aplicadorprova = avp.id_aplicadorprova
	LEFT JOIN vw_aplicadorendereco AS vw_end
		ON vw_end.id_aplicadorprova = ap.id_aplicadorprova

--docs/vw_sql/dbo.vw_alunosrenovacao.View.sql
ALTER VIEW vw_alunosrenovacao
AS

SELECT DISTINCT
	mat.id_matricula,
	mat.id_projetopedagogico,
	mat.id_usuario
	, mat.id_periodoletivo
	, mat.id_evolucao
	, mat.id_vendaproduto
	, mat.id_entidadematricula
	, mat.id_turma
	, venda.id_venda
	, vp.id_produto
	, vp.id_tiposelecao
	, c.id_contrato
	, c.id_contratoregra
FROM tb_matricula AS mat
	JOIN tb_entidade AS e ON e.id_entidade = mat.id_entidadematricula AND mat.bl_ativo = 1 AND mat.id_evolucao = 6 AND mat.bl_institucional = 0
	JOIN tb_esquemaconfiguracaoitem AS eci ON eci.id_esquemaconfiguracao = e.id_esquemaconfiguracao AND eci.st_valor = 2
	JOIN tb_itemconfiguracao AS ic ON ic.id_itemconfiguracao = eci.id_itemconfiguracao AND ic.id_itemconfiguracao = 22
	JOIN tb_contratomatricula AS cm ON cm.id_matricula = mat.id_matricula AND  cm.bl_ativo = 1
	JOIN tb_contrato AS c ON c.id_contrato = cm.id_contrato AND c.bl_ativo = 1
	--JOIN tb_venda    AS v ON v.id_venda = c.id_venda AND v.dt_limiterenovacao IS NULL
	OUTER APPLY (SELECT TOP 1 v.id_venda FROM tb_venda v
		JOIN tb_vendaproduto vp ON v.id_venda = vp.id_venda AND vp.id_matricula = mat.id_matricula
		JOIN tb_contrato cont ON cont.id_venda = v.id_venda AND cont.bl_ativo = 1
	where v.id_evolucao = 10 AND v.bl_ativo = 1
							 ORDER BY v.id_venda DESC
							) AS venda
	JOIN tb_vendaproduto as vp ON vp.id_vendaproduto = mat.id_vendaproduto

																AND NOT EXISTS (SELECT lan.id_lancamento FROM tb_lancamento as lan
		JOIN tb_lancamentovenda as lv ON lv.id_lancamento = lan.id_lancamento AND lan.bl_ativo = 1 AND lan.bl_quitado = 0
																		 AND lv.id_venda = venda.id_venda
	)
																and not EXISTS (select * from tb_saladeaula as sa
		JOIN tb_alocacao as al on al.id_saladeaula = sa.id_saladeaula AND al.bl_ativo = 1
		JOIN tb_matriculadisciplina as md on md.id_matriculadisciplina = al.id_matriculadisciplina and md.id_matricula = mat.id_matricula
	where sa.dt_abertura > getdate())
																AND NOT EXISTS (SELECT vd.id_venda FROM tb_venda as vd
		JOIN tb_vendaproduto as vpp on vpp.id_venda = vd.id_venda and vpp.id_matricula = mat.id_matricula
	WHERE vd.id_evolucao = 7 and vd.dt_limiterenovacao > getdate())

AND venda.id_venda IS NOT NULL



--docs/vw_sql/dbo.vw_avaliacaoagendamento.View.sql

ALTER VIEW [dbo].[vw_avaliacaoagendamento] AS
SELECT DISTINCT
	aa.id_avaliacaoagendamento,
	aa.id_matricula,
	us.id_usuario,
	us.st_nomecompleto,
	av.id_avaliacao,
	av.st_avaliacao,
	ac.id_tipoprova,
	tp.st_tipoprova,
	aa.dt_agendamento,
	aa.id_situacao,
	st.st_situacao,
	aa.id_avaliacaoaplicacao,
	aa.id_entidade,
	et.st_nomeentidade,
	ap.id_horarioaula,
	ha.st_horarioaula,
	mt.id_projetopedagogico,
	pp.st_projetopedagogico,
	aai.st_codsistema
	,aa.bl_ativo,
	ap.st_endereco,
	ap.dt_aplicacao,
	ap.id_aplicadorprova,
	ap.st_aplicadorprova,
	ap.st_cidade,
	aa.nu_presenca,
	aa.id_usuariolancamento,
		usu_lancamento.st_nomecompleto AS st_usuariolancamento,
		(CASE WHEN aa.id_tipodeavaliacao IS NULL THEN 0 ELSE aa.id_tipodeavaliacao END) AS id_tipodeavaliacao,
		(CASE WHEN (ap.dt_aplicacao IS NOT NULL AND ap.dt_aplicacao>GETDATE() AND aa.bl_ativo=1 AND aa.nu_presenca IS NULL) THEN 0
		 WHEN (ap.dt_aplicacao IS NOT NULL AND ap.dt_aplicacao>GETDATE() AND aa.bl_ativo=0 AND aa.id_situacao=68 AND aa.nu_presenca IS NOT NULL) THEN 1
		 WHEN (ap.dt_aplicacao IS NOT NULL AND ap.dt_aplicacao<GETDATE() AND aa.bl_ativo=1 AND aa.id_situacao=68 AND aa.nu_presenca IS NULL) THEN 2
		 ELSE 3 END) AS id_situacaopresenca,
		(CASE WHEN (ap.dt_aplicacao IS NOT NULL AND ap.dt_aplicacao>GETDATE() AND aa.bl_ativo=1 AND aa.nu_presenca IS NULL) THEN 'Aguardando aplicação de prova'
		 WHEN (ap.dt_aplicacao IS NOT NULL AND ap.dt_aplicacao>GETDATE() AND aa.bl_ativo=0 AND aa.id_situacao=68 AND aa.nu_presenca IS NOT NULL) THEN 'Presença lançada'
		 WHEN (ap.dt_aplicacao IS NOT NULL AND ap.dt_aplicacao<GETDATE() AND aa.bl_ativo=1 AND aa.id_situacao=68 AND aa.nu_presenca IS NULL) THEN 'Aplicação realizada e presença não lançada'
		 ELSE 'Situação Indefinida' END) AS st_situacaopresenca,
		tb_chamada.id_aval AS id_chamada,
	primeira.id_primeirachamada,
	primeirapresenca.nu_primeirapresenca,
		primeira.id_segundachamada AS id_ultimachamada,
	ultima.nu_ultimapresenca,
	mt.id_evolucao,
	aa.bl_provaglobal,
	ap.dt_antecedenciaminima,
	ap.dt_alteracaolimite,

		(CASE
		 WHEN aa.id_avaliacaoaplicacao IS NULL OR ((CAST(GETDATE() AS DATE)) BETWEEN ap.dt_antecedenciaminima AND ap.dt_alteracaolimite AND aa.id_avaliacaoaplicacao = ap.id_avaliacaoaplicacao AND aa.bl_ativo = 1)
			 THEN 0
		 ELSE 1
		 END) AS id_mensagens,

		(CASE
		 WHEN (CAST(GETDATE() AS DATE)) < ap.dt_antecedenciaminima
			 THEN 'A data para o agendamento ainda não chegou'
		 WHEN (CAST(GETDATE() AS DATE)) > ap.dt_alteracaolimite
			 THEN 'Agendamento passou do prazo limite para alteração'
		 WHEN aa.id_avaliacaoaplicacao IS NULL
			 THEN ''
		 ELSE
			 'Agendamento dentro do prazo limite para alteração'
		 END) AS st_mensagens,

	provasrealizadas.id_provasrealizadas,
		isnull(temnotafinal.bl_temnotafinal,0) as bl_temnotafinal,
	vw_pessoa.st_email,
	vw_pessoa.nu_telefone,
	vw_pessoa.nu_ddd,
	vw_pessoa.nu_ddi,
	ha.hr_inicio,
	ha.hr_fim
	,ap.sg_uf
	,ap.st_complemento
	,ap.nu_numero
	,ap.st_bairro
	, ap.st_telefone as 'st_telefoneaplicador'

from tb_avaliacaoagendamento aa
	JOIN tb_matricula as mt ON mt.id_matricula = aa.id_matricula
	JOIN tb_usuario as us ON us.id_usuario = mt.id_usuario
	JOIN tb_avaliacao as av ON av.id_avaliacao = aa.id_avaliacao
	INNER JOIN tb_avaliacaoconjuntorelacao as acr ON acr.id_avaliacao = av.id_avaliacao
	INNER JOIN tb_avaliacaoconjunto as ac on ac.id_avaliacaoconjunto = acr.id_avaliacaoconjunto
	JOIN tb_tipoprova as tp ON tp.id_tipoprova = ac.id_tipoprova
	INNER JOIN tb_situacao as st ON st.id_situacao = aa.id_situacao
	JOIN tb_projetopedagogico as pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
	JOIN vw_pessoa on vw_pessoa.id_usuario = us.id_usuario and vw_pessoa.id_entidade = mt.id_entidadematricula
	LEFT JOIN vw_avaliacaoaplicacao as ap ON ap.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao AND ap.bl_ativo = 1 and ap.id_entidade=aa.id_entidade
	LEFT JOIN tb_entidade as et ON et.id_entidade = ap.id_entidade
	LEFT JOIN tb_horarioaula as ha ON ha.id_horarioaula = ap.id_horarioaula
	LEFT JOIN tb_avalagendaintegracao as aai ON aai.id_avaliacaoagendamento = aa.id_avaliacaoagendamento
	OUTER APPLY (select st_nomecompleto from tb_usuario where id_usuario = aa.id_usuariolancamento) as usu_lancamento
	OUTER APPLY (select count(cha.id_avaliacaoagendamento) as id_aval from tb_avaliacaoagendamento as cha left join tb_avaliacaoaplicacao as ch on ch.id_avaliacaoaplicacao=cha.id_avaliacaoaplicacao where cha.id_matricula=mt.id_matricula and cha.id_tipodeavaliacao = aa.id_tipodeavaliacao and cha.id_situacao=68) as tb_chamada
	OUTER APPLY (select min(id_avaliacaoagendamento) as id_primeirachamada, max(id_avaliacaoagendamento) as id_segundachamada from tb_avaliacaoagendamento where id_matricula=aa.id_matricula and id_situacao=68 and id_tipodeavaliacao = aa.id_tipodeavaliacao) as primeira
	OUTER APPLY (select nu_presenca as nu_primeirapresenca from tb_avaliacaoagendamento where id_avaliacaoagendamento = (select min(id_avaliacaoagendamento) from tb_avaliacaoagendamento where id_matricula=aa.id_matricula and id_situacao=68 and id_tipodeavaliacao = aa.id_tipodeavaliacao) ) as primeirapresenca
	OUTER APPLY (select nu_presenca as nu_ultimapresenca from tb_avaliacaoagendamento where id_avaliacaoagendamento = (select max(id_avaliacaoagendamento) from tb_avaliacaoagendamento where id_matricula=aa.id_matricula and id_situacao=68 and id_tipodeavaliacao = aa.id_tipodeavaliacao) ) as ultima
	OUTER APPLY (select count(id_avaliacaoagendamento) as id_provasrealizadas from tb_avaliacaoagendamento where id_matricula=aa.id_matricula and nu_presenca=1 and bl_ativo=0 and id_situacao=68) as provasrealizadas
	OUTER APPLY (select (case when st_nota is null then 0 else 1 end) as bl_temnotafinal from tb_avaliacaoaluno where id_matricula=mt.id_matricula and id_avaliacao=aa.id_avaliacao and id_situacao=86) as temnotafinal
WHERE aa.id_situacao<>70 AND ac.id_tipoprova = 2

--docs/vw_sql/dbo.vw_avaliacaoaplicacao.View.sql

ALTER VIEW [dbo].[vw_avaliacaoaplicacao] AS
SELECT
	distinct
	aa.id_avaliacaoaplicacao,
	aa.id_aplicadorprova,
	aa.id_endereco,
	aa.dt_aplicacao,
	app.st_aplicadorprova,
	en.sg_uf,
	en.id_municipio,
	en.st_endereco,
	mp.st_nomemunicipio AS st_cidade,
	en.st_complemento,
	en.nu_numero,
	en.st_bairro,
	aa.id_entidade,
	CAST('('+ct.nu_ddd+')'+ct.nu_telefone AS VARCHAR) AS st_telefone,
	aa.id_horarioaula,
	aa.id_usuariocadastro,
	aa.dt_alteracaolimite,
	aa.dt_antecedenciaminima,
	aa.bl_unica,
	aa.dt_cadastro,
	aa.bl_ativo,
	hr.hr_inicio AS hr_inicioprova,
	hr.hr_fim AS hr_fimprova,
	hr.st_horarioaula,
	CAST(GETDATE() AS DATE) AS data,
	(CASE WHEN (CAST(GETDATE() AS DATE)) BETWEEN aa.dt_antecedenciaminima AND aa.dt_alteracaolimite THEN 1 ELSE 0 END) AS bl_dataativa,
	(CASE WHEN aa.nu_maxaplicacao <= (SELECT COUNT(id_matricula) FROM tb_avaliacaoagendamento WHERE id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao and bl_ativo=1) THEN 1 ELSE 0 END) AS bl_limitealunos,
	-- Numero total de vagas
	aa.nu_maxaplicacao,
	-- Vagas restantes
	(aa.nu_maxaplicacao - (SELECT COUNT(id_avaliacaoagendamento) FROM tb_avaliacaoagendamento saa WHERE saa.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao AND bl_ativo = 1 AND saa.id_situacao IN(68,69))) AS nu_vagasrestantes
FROM dbo.tb_avaliacaoaplicacao AS aa
	INNER JOIN dbo.tb_endereco AS en ON aa.id_endereco = en.id_endereco
	LEFT JOIN tb_municipio AS mp on mp.id_municipio = en.id_municipio
	INNER JOIN dbo.tb_aplicadorprova AS app ON aa.id_aplicadorprova = app.id_aplicadorprova
	INNER JOIN tb_aplicadorprovaentidade AS ape on ape.id_aplicadorprova = aa.id_aplicadorprova
	INNER JOIN tb_horarioaula AS hr on hr.id_horarioaula = aa.id_horarioaula
	LEFT JOIN dbo.tb_contatostelefonepessoa AS ctp ON ctp.id_usuario = id_usuarioaplicador AND ctp.id_entidade = id_entidadecadastro AND ctp.bl_padrao = 1
	LEFT JOIN dbo.tb_contatostelefoneentidade AS cte ON cte.id_entidade = id_entidadeaplicador AND cte.bl_padrao = 1
	LEFT JOIN dbo.tb_contatostelefone AS ct ON ct.id_telefone = cte.id_telefone OR ct.id_telefone = ctp.id_telefone

--docs/vw_sql/dbo.vw_cartacredito.View.sql
ALTER VIEW [dbo].[vw_cartacredito] as
SELECT
	cc.id_cartacredito,
	cancelamento.id_cancelamento,
	cc.dt_cadastro,
	cc.nu_valororiginal,
	(SELECT
	ISNULL(CONVERT(FLOAT, SUM(utilizado.nu_valorutilizado)), 0)
	 FROM tb_vendacartacredito AS utilizado
		 JOIN tb_venda utilizadovenda
			 ON utilizadovenda.id_venda = utilizado.id_venda
	 WHERE utilizadovenda.id_evolucao NOT IN (7, 8)
				 AND utilizado.id_cartacredito = cc.id_cartacredito)
		AS nu_valorutilizado,
	(cc.nu_valororiginal - (SELECT
	ISNULL(CONVERT(FLOAT, SUM(disponivel.nu_valorutilizado)), 0)
													FROM tb_vendacartacredito AS disponivel
														JOIN tb_venda disponivelvenda
															ON disponivelvenda.id_venda = disponivel.id_venda
													WHERE disponivelvenda.id_evolucao NOT IN (7, 8)
																AND disponivel.id_cartacredito = cc.id_cartacredito)
	) AS nu_valordisponivel,
	cc.id_usuariocadastro,
	cc.id_usuario,
	cc.bl_ativo,
	cc.id_situacao,
	s.st_situacao,
	cc.id_entidade,
	cc.dt_validade,
	e.st_nomeentidade
FROM tb_cartacredito AS cc
	JOIN tb_situacao s
		ON s.id_situacao = cc.id_situacao
	JOIN tb_entidade e
		ON e.id_entidade = cc.id_entidade
	LEFT JOIN tb_cancelamento cancelamento
		on cancelamento.id_cancelamento = cc.id_cancelamento

--docs/vw_sql/dbo.vw_comercial_g1_g2.sql
ALTER VIEW [dbo].[vw_comercial_g1_g2]
AS
--72625
SELECT

	id_entidade AS 'Código Entidade' ,
	st_nomeentidade AS Entidade,
	st_nomecompleto AS Aluno ,
	st_cpf AS CPF ,
	sg_uf AS UF ,
	st_cidade AS Cidade,
	st_projetopedagogico AS Curso,
	NULL AS Segmento,
	NULL AS 'Código Área',
	st_areaconhecimento AS 'Área' ,
	st_projetopedagogico AS 'Título Curso',
	CONVERT (CHAR,dt_matricula,103) AS 'Data_Matricula' ,
	st_representante AS Vendedor ,
	st_atendente AS Atendente ,
	NULL AS 'Núcleo de Atendimento',
	nu_valorboleto AS 'Valor Boleto',
	nu_valorcarta AS 'Valor Carta Crédito',
	nu_valorbolsa AS 'Valor Bolsa',
	nu_valorliquido AS 'Valor Negociado' ,
	nu_valorcartaoc AS 'Valor no Cartão de Crédito',
	st_evolucao AS 'Evolução da Matrícula',
	NULL AS 'Código Contrato G1',
	id_venda AS 'Código Contrato G2',
	NULL AS 'Transferida outra Entidade ?',
	NULL AS Afiliada,
	id_entidadepai,
	NULL AS gptqry_codentidade,
	dt_matricula AS 'dt_matricula' ,
	id_matricula AS 'Mátricula'
FROM rel.vw_comercialgeral
WHERE --id_entidade in (12,19) and
	id_entidadepai IN (12,19, 176, 177, 178, 179, 423, 569, 570, 510, 512)

UNION

SELECT
	[Cód. Entidade] AS 'Código Entidade' ,
	Entidade COLLATE SQL_Latin1_General_CP1_CI_AI AS Entidade,
	Aluno COLLATE SQL_Latin1_General_CP1_CI_AI AS Aluno ,
	CPF COLLATE SQL_Latin1_General_CP1_CI_AI AS CPF ,
	UF COLLATE SQL_Latin1_General_CP1_CI_AI AS UF ,
	Cidade COLLATE SQL_Latin1_General_CP1_CI_AI AS Cidade ,
	[Título Curso] COLLATE SQL_Latin1_General_CP1_CI_AI AS Curso ,
	Segmento COLLATE SQL_Latin1_General_CP1_CI_AI AS Segmento ,
	[Cód. Área] AS 'Código Área',
	Área COLLATE SQL_Latin1_General_CP1_CI_AI AS Área,
	[Título Curso] COLLATE SQL_Latin1_General_CP1_CI_AI AS 'Título Curso' ,
	[Data Matrícula] AS 'Data Matrícula',
	Vendedor COLLATE SQL_Latin1_General_CP1_CI_AI AS Vendedor ,
	Atendente COLLATE SQL_Latin1_General_CP1_CI_AI AS Atendente ,
	Núcleo COLLATE SQL_Latin1_General_CP1_CI_AI AS 'Núcleo de Atendimento',
	[Valor Boleto] AS 'Valor Boleto',
	[Valor Carta] AS 'Valor Carta Crédito',
	[Valor Bolsa] AS 'Valor Bolsa',
	[Valor Negociado] AS 'Valor Negociado',
	[Valor no Cartão de Crédito] AS 'Valor no Cartão de Crédito' ,
	[Última Matrícula Evolução] COLLATE SQL_Latin1_General_CP1_CI_AI AS 'Evolução da Matrícula',
	[Cód. Contrato] AS 'Código Contrato G1' ,
	[Identificação Venda G2] AS 'Código Contrato G2' ,
	[Transferida outra Entidade] COLLATE SQL_Latin1_General_CP1_CI_AI AS 'Transferida outra Entidade ?',
	AFILIADA COLLATE SQL_Latin1_General_CP1_CI_AI AS Afiliada,
	NULL AS id_entidadepai,
	gptqry_codentidade,
	gptqry_dt_matricula AS 'dt_matricula'  ,
	[Cód. Matrícula] AS 'Mátricula'
FROM [WIN-MB0LL52UPVD].POSEADVIRTUAL.rel.vw_com5_comercial_geral
WHERE [Cód. Entidade] IN (112,686,967,966,974,608,868,1289,1412,1546,1491,1583)

/*
[Cód. Entidade]
1546 Pos Combinada - AVM no G1
1491 AMM/AVM G1
1583 AVM Graduação
*/


--docs/vw_sql/dbo.vw_gerardeclaracao.View.sql

ALTER VIEW [dbo].[vw_gerardeclaracao]
AS
SELECT DISTINCT
	mt.id_matricula ,
	ps.id_usuario AS id_usuarioaluno ,
	ps.st_nomecompleto AS st_nomecompletoaluno ,
	ps.st_login AS st_loginaluno ,
	ps.st_senhaentidade AS st_senhaaluno ,
	ps.st_cpf AS st_cpfaluno ,
	ps.st_rg AS st_rgaluno ,
	ps.st_orgaoexpeditor ,
	ps.dt_dataexpedicao AS dt_dataexpedicaoaluno ,
	ps.st_nomemae AS st_filiacao ,
	CONVERT(CHAR, mt.dt_cadastro, 103) AS st_datamatricula ,
	canc.id_cancelamento ,
	canc.st_observacao AS st_observacaocancelamento ,
	canc.st_observacaocalculo AS st_observacao_calculo ,
	CONVERT(CHAR, canc.dt_solicitacao, 103) AS st_solicitacaocancelamento ,
	canc.nu_valorcarta AS st_valorcredito ,
	CONVERT(CHAR, DATEADD(D, 365, canc.dt_solicitacao), 103) AS st_validadecredito ,
	cm.id_contrato ,
	ps.dt_nascimento AS dt_nascimentoaluno ,
	UPPER(SUBSTRING(ps.st_nomemunicipio, 1, 1))
	+ LOWER(SUBSTRING(ps.st_nomemunicipio, 2, 499)) AS st_municipioaluno ,
	ps.st_estadoprovincia AS st_ufaluno ,
	ps.sg_uf AS sg_ufaluno ,
	ps.nu_ddd AS nu_dddaluno ,
	ps.nu_telefone AS nu_telefonealuno ,
	ps.st_email AS st_emailaluno ,
	pp.st_tituloexibicao AS st_projeto ,
	--pp.nu_cargahoraria,
	( SELECT    SUM(dis.nu_cargahoraria) AS nu_cargahoraria
		FROM      tb_matriculadisciplina md
			JOIN tb_matricula m ON m.id_matricula = md.id_matricula
			JOIN tb_disciplina dis ON dis.id_disciplina = md.id_disciplina
		WHERE     md.id_matricula = mt.id_matricula
	) AS nu_cargahoraria ,
	ne.st_nivelensino ,
	mt.dt_concluinte AS dt_concluintealuno ,
	us_coordenador.st_nomecompleto AS st_coordenadorprojeto ,
	fl.id_fundamentolegal AS id_fundamentolei ,
	fl.nu_numero AS nu_lei ,
	flr.id_fundamentolegal AS id_fundamentoresolucao ,
	tur.id_turma AS id_turma ,
	tur.st_turma ,
	tur.dt_inicio AS dt_inicioturma ,
	tur.dt_fim AS dt_terminoturma ,
	flr.nu_numero AS nu_resolucao ,
	flpr.id_fundamentolegal AS id_fundamentoparecer ,
	flpr.nu_numero AS nu_parecer ,
	flp.id_fundamentolegal AS id_fundamentoportaria ,
	flp.nu_numero AS nu_portaria ,
	et.id_entidade ,
	et.st_nomeentidade ,
	et.st_razaosocial ,
	et.st_cnpj ,
	et.st_urlimglogo ,
	et.st_urlsite ,
	ps.st_nomepais ,
	ps.st_sexo AS sexo_aluno ,
	UPPER(tcc.st_tituloavaliacao) AS st_titulomonografia ,
	CONVERT(VARCHAR(2), DATEPART(DAY, GETDATE()), 103) + ' de '
	+ dbo.fn_mesporextenso(GETDATE()) + ' de '
	+ CONVERT(VARCHAR(4), DATEPART(YEAR, GETDATE()), 103) AS st_data ,
	DAY(GETDATE()) AS st_dia ,
	LOWER(dbo.fn_mesporextenso(GETDATE())) AS st_mes ,
	YEAR(GETDATE()) AS st_ano ,

	--CONVERT(DATE, sa.dt_abertura, 103) AS dt_primeirasala ,
	(
		SELECT TOP 1 ts.dt_abertura
		FROM tb_matriculadisciplina tm
			JOIN tb_alocacao ta ON tm.id_matriculadisciplina = ta.id_matriculadisciplina
			JOIN tb_saladeaula ts ON ta.id_saladeaula = ts.id_saladeaula
		WHERE tm.id_matricula = mt.id_matricula AND tm.bl_obrigatorio = 1
		ORDER BY ts.dt_abertura ASC
	) AS dt_primeirasala,

	--CONVERT(DATE, DATEADD(MONTH, 13, sa.dt_abertura), 103) AS dt_previsaofim ,
	(
		SELECT TOP 1
			--DATEADD(MONTH, 13, ts.dt_abertura)

				'dt_previsaotermino' = CASE WHEN ts.id_categoriasala = 2 THEN (
				DATEADD(day, ISNULL(ts.nu_diasaluno, 0), ts.dt_abertura)
			) ELSE (
				CASE WHEN ts.dt_encerramento IS NOT NULL THEN (
					DATEADD(day, ISNULL(ts.nu_diasextensao, 0) + 30, ts.dt_encerramento)
				) WHEN ts.dt_abertura IS NOT NULL THEN (
					DATEADD(day, ISNULL(ts.nu_diasextensao, 0) + 30, ts.dt_abertura)
				) ELSE (
					NULL
				) END
			) END

		FROM tb_matriculadisciplina tm
			JOIN tb_alocacao ta ON tm.id_matriculadisciplina = ta.id_matriculadisciplina
			JOIN tb_saladeaula ts ON ta.id_saladeaula = ts.id_saladeaula
		WHERE tm.id_matricula = mt.id_matricula AND tm.bl_obrigatorio = 1
		ORDER BY dt_previsaotermino DESC, ts.dt_abertura DESC, ts.dt_encerramento DESC
	) AS dt_previsaofim,

	UPPER(tcc.st_tituloavaliacao) AS st_titulotcc,
	tcc.dt_aberturasala AS dt_iniciotcc,
	tcc.dt_encerramentosala AS dt_terminotcc,
	tcc.st_nota AS nu_notatcc,
	tcc.dt_cadastroavaliacao AS dt_cadastrotcc,
	tcc.dt_defesa AS dt_defesatcc,

	mt.st_codcertificacao ,
	CONVERT(CHAR, GETDATE(), 103) AS st_atual
	, matc.st_codigoacompanhamento AS st_codigorasteamento,

	mt.id_matriculaorigem,
	mto.dt_inicio AS dt_iniciomatriculaorigem
	, CONVERT(VARCHAR(2), DATEPART(day, GetDate()))     + ' de ' +  dbo.fn_mesporextenso(GetDate())   + ' de ' +  CONVERT(VARCHAR(4), DATEPART(year, GetDate())) as st_atualextenso,
	CONVERT(VARCHAR(2), DATEPART(day, tur.dt_fim))     + ' de ' +  dbo.fn_mesporextenso(tur.dt_fim)   + ' de ' +  CONVERT(VARCHAR(4), DATEPART(year, tur.dt_fim)) as st_terminoturmaextenso,
	CONVERT(VARCHAR(2), DATEPART(day, tur.dt_inicio))     + ' de ' +  dbo.fn_mesporextenso(tur.dt_inicio)   + ' de ' +  CONVERT(VARCHAR(4), DATEPART(year, tur.dt_inicio)) as st_inicioturmaextenso

	, CONVERT(CHAR, venda.dt_limiterenovacao, 103) AS st_limiterenovacao
	, venda.id_venda AS id_venda
	, venda.nu_mensalidade AS nu_mensalidade
	, venda.nu_mensalidadepon AS nu_mensalidadepontualidade
	, ' - ' AS st_link_grade_portal

FROM    tb_matricula mt
	JOIN tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
	JOIN tb_entidade AS et ON et.id_entidade = mt.id_entidadeatendimento
	LEFT JOIN tb_entidadeendereco AS ete ON ete.id_entidade = et.id_entidade
																					AND ete.bl_padrao = 1
	LEFT JOIN tb_endereco AS etee ON etee.id_endereco = ete.id_endereco
	JOIN vw_pessoa AS ps ON ps.id_usuario = mt.id_usuario
													AND ps.id_entidade = mt.id_entidadeatendimento
	LEFT JOIN tb_fundamentolegal AS fl ON fl.id_entidade = mt.id_entidadematriz
																				AND fl.id_tipofundamentolegal = 1
																				AND fl.dt_publicacao <= GETDATE()
																				AND fl.dt_vigencia >= GETDATE()
	LEFT JOIN tb_fundamentolegal AS flp ON flp.id_entidade = mt.id_entidadematriz
																				 AND flp.id_tipofundamentolegal = 2
																				 AND flp.dt_publicacao <= GETDATE()
																				 AND flp.dt_vigencia >= GETDATE()
	LEFT JOIN tb_fundamentolegal AS flr ON flr.id_entidade = mt.id_entidadematriz
																				 AND flr.id_tipofundamentolegal = 3
																				 AND flr.dt_publicacao <= GETDATE()
																				 AND flr.dt_vigencia >= GETDATE()
	LEFT JOIN tb_fundamentolegal AS flpr ON flpr.id_entidade = mt.id_entidadematriz
																					AND flpr.id_tipofundamentolegal = 4
																					AND flpr.dt_publicacao <= GETDATE()
																					AND flpr.dt_vigencia >= GETDATE()
	LEFT JOIN tb_projetopedagogicoserienivelensino AS psn ON psn.id_projetopedagogico = mt.id_projetopedagogico
	LEFT JOIN tb_turma AS tur ON tur.id_turma = mt.id_turma
	JOIN tb_nivelensino AS ne ON ne.id_nivelensino = psn.id_nivelensino
	LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS uper ON uper.id_projetopedagogico = pp.id_projetopedagogico
																															AND uper.bl_ativo = 1
																															AND uper.bl_titular = 1
	LEFT JOIN dbo.tb_usuario AS us_coordenador ON us_coordenador.id_usuario = uper.id_usuario

	LEFT JOIN tb_matricula AS mto ON mt.id_matriculaorigem = mto.id_matricula

	LEFT JOIN dbo.vw_avaliacaoaluno tcc ON ( tcc.id_matricula = mt.id_matricula
																					 AND tcc.id_tipoavaliacao = 6
																					 AND tcc.st_tituloavaliacao IS NOT NULL
																					 AND tcc.id_tipodisciplina = 2
																					 AND tcc.bl_ativo = 1
		)
	LEFT JOIN tb_cancelamento AS canc ON canc.id_cancelamento = mt.id_cancelamento AND mt.id_evolucao = 27
	LEFT JOIN tb_contratomatricula AS cm ON cm.id_matricula = mt.id_matricula
	LEFT JOIN tb_matriculacertificacao AS matc ON matc.id_matricula = mt.id_matricula

	OUTER APPLY (SELECT TOP 1 v.id_venda , v.dt_limiterenovacao ,
								 format((vp.nu_valorbruto - ((cm.nu_valordesconto / 100 ) * vp.nu_valorbruto))/6, '#.00') AS nu_mensalidade,
								 format((((vp.nu_valorbruto - ((cm.nu_valordesconto / 100 ) * vp.nu_valorbruto))/6) - (((pon.nu_valordesconto / 100 ) * ((vp.nu_valorbruto - ((cm.nu_valordesconto / 100 ) * vp.nu_valorbruto))/6)))), '#.00') AS nu_mensalidadepon

							 FROM tb_vendaproduto AS vp
								 JOIN tb_venda AS v ON v.id_venda = vp.id_venda AND v.dt_limiterenovacao IS NOT NULL
																			 AND vp.id_matricula = mt.id_matricula AND v.id_evolucao = 7 AND v.bl_ativo = 1
								 LEFT JOIN tb_campanhacomercial AS cm ON cm.id_campanhacomercial = vp.id_campanhacomercial
								 LEFT JOIN tb_campanhacomercial as pon ON pon.id_campanhacomercial = v.id_campanhapontualidade

							 ORDER BY v.id_venda DESC) AS venda

--docs/vw_sql/dbo.vw_matricula.View.sql

ALTER VIEW [dbo].[vw_matricula]
AS
SELECT
	mt.id_usuario,
	us.st_nomecompleto,
	us.st_cpf,
	ps.st_urlavatar,
	ps.dt_nascimento,
	ps.st_nomepai,
	ps.st_nomemae,
	ps.st_email,
	CAST(ps.nu_ddd AS VARCHAR) + '-' + CAST(ps.nu_telefone AS VARCHAR) AS st_telefone,
	CAST(ps.nu_dddalternativo AS VARCHAR) + '-'
	+ CAST(ps.nu_telefonealternativo AS VARCHAR) AS st_telefonealternativo,
	mt.id_matricula,
	pp.st_projetopedagogico,
	mt.id_situacao,
	st.st_situacao,
	mt.id_evolucao,
	ev.st_evolucao,
	mt.dt_concluinte,
	mt.id_entidadematriz,
	etmz.st_nomeentidade AS st_entidadematriz,
	mt.id_entidadematricula,
	etm.st_nomeentidade AS st_entidadematricula,
	mt.id_entidadeatendimento,
	eta.st_nomeentidade AS st_entidadeatendimento,
	mt.bl_ativo,
	cm.id_contrato,
	pp.id_projetopedagogico,
	mt.dt_termino,
	mt.dt_cadastro,
	mt.dt_inicio,
	tm.id_turma,
	tm.st_turma,
	tm.dt_inicio AS dt_inicioturma,
	mt.bl_institucional,
	tm.dt_fim AS dt_terminoturma,
	mt.id_situacaoagendamento,
	mt.id_vendaproduto,
	mt.dt_termino AS dt_terminomatricula,
	CAST(RIGHT(master.dbo.fn_varbintohexstr(HASHBYTES('MD5',
																										(CAST(us.id_usuario AS VARCHAR(10)) COLLATE Latin1_General_CI_AI
																										 + CAST((CASE
																														 WHEN mt.id_matricula IS NULL THEN 0
																														 ELSE mt.id_matricula
																														 END) AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
																										 + CAST(5 AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
																										 + CAST((0) AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
																										 + us.st_login
																										 + us.st_senha
																										 + CONVERT(VARCHAR(10), GETDATE(), 103)))),
						 32) AS CHAR(32)) COLLATE Latin1_General_CI_AI AS st_urlacesso,
	mt.bl_documentacao,
	ct.id_venda,
	eta.st_urlportal,
	mt.st_codcertificacao,
	ps.st_identificacao,
	CONVERT(VARCHAR, mtc.dt_cadastro, 103) AS st_dtcadastrocertificacao,
	CONVERT(VARCHAR, mtc.dt_enviocertificado, 103) AS st_dtenviocertificado,
	CONVERT(VARCHAR, mtc.dt_retornocertificadora, 103) AS st_dtretornocertificadora,
	CONVERT(VARCHAR, mtc.dt_envioaluno, 103) AS st_dtenvioaluno,
	st_codigoacompanhamento,
	mtc.id_indicecertificado,
	mt.id_evolucaocertificacao,
	ap.id_areaconhecimento,
	pp.bl_disciplinacomplementar,
	mt.id_matriculavinculada,
	cr.id_contratoregra,
	trc.id_tiporegracontrato,
	etm.st_siglaentidade,
	mt.bl_academico,
	mtc.dt_cadastro AS dt_cadastrocertificacao,
	mtc.dt_enviocertificado,
	mtc.dt_retornocertificadora,
	mtc.dt_envioaluno,
	vup.st_upload AS st_uploadcontrato,
	canc.id_cancelamento,
	mt.id_aproveitamento,
	vprod.id_produto
FROM tb_matricula mt
	JOIN tb_projetopedagogico AS pp
		ON mt.id_projetopedagogico = pp.id_projetopedagogico
	JOIN tb_areaprojetopedagogico AS ap
		ON ap.id_projetopedagogico = pp.id_projetopedagogico
	JOIN tb_entidade AS etmz
		ON etmz.id_entidade = mt.id_entidadematriz
	JOIN tb_entidade AS etm
		ON etm.id_entidade = mt.id_entidadematricula
	JOIN tb_entidade AS eta
		ON eta.id_entidade = mt.id_entidadeatendimento
	JOIN tb_situacao AS st
		ON st.id_situacao = mt.id_situacao
	JOIN tb_evolucao AS ev
		ON ev.id_evolucao = mt.id_evolucao
	JOIN tb_usuario AS us
		ON us.id_usuario = mt.id_usuario
	LEFT JOIN tb_contratomatricula AS cm
		ON cm.id_matricula = mt.id_matricula
			 AND cm.bl_ativo = 1
	LEFT JOIN vw_pessoa AS ps
		ON mt.id_usuario = ps.id_usuario
			 AND mt.id_entidadeatendimento = ps.id_entidade
	LEFT JOIN dbo.tb_contrato AS ct
		ON ct.id_contrato = cm.id_contrato
			 AND ct.bl_ativo = 1
	LEFT JOIN tb_turma AS tm
		ON tm.id_turma = mt.id_turma
	LEFT JOIN tb_matriculacertificacao mtc
		ON mtc.id_matricula = mt.id_matricula
			 AND mtc.bl_ativo = 1
	LEFT JOIN dbo.tb_contratoregra AS cr
		ON cr.id_contratoregra = ct.id_contratoregra
	LEFT JOIN dbo.tb_tiporegracontrato AS trc
		ON trc.id_tiporegracontrato = cr.id_tiporegracontrato
	LEFT JOIN dbo.tb_venda AS v
		ON ct.id_venda = v.id_venda
	LEFT JOIN dbo.tb_upload AS vup
		ON vup.id_upload = v.id_uploadcontrato
	LEFT JOIN dbo.tb_cancelamento AS canc
		ON canc.id_cancelamento = mt.id_cancelamento
	LEFT JOIN dbo.tb_vendaproduto AS vprod
		ON vprod.id_vendaproduto = mt.id_vendaproduto
WHERE mt.bl_ativo = 1;

--docs/vw_sql/dbo.vw_pesquisasaladeaula.View.sql

ALTER VIEW [dbo].[vw_pesquisasaladeaula] AS
select sa.id_saladeaula ,
	sa.dt_cadastro ,
	st_saladeaula ,
	sa.bl_ativa ,
	sa.id_usuariocadastro ,
	sa.id_modalidadesaladeaula ,
	sa.dt_inicioinscricao ,
	sa.dt_fiminscricao ,
	sa.dt_abertura ,
	st_localizacao ,
	sa.id_tiposaladeaula ,
	sa.dt_encerramento ,
	sa.id_entidade ,
	sa.id_periodoletivo ,
	bl_usardoperiodoletivo ,
	sa.id_situacao ,
	nu_maxalunos ,
	nu_diasencerramento ,
	bl_semencerramento ,
	nu_diasaluno ,
	bl_semdiasaluno ,
	nu_diasextensao ,
	st_pontosnegativos ,
	st_pontospositivos ,
	st_pontosmelhorar ,
	st_pontosresgate ,
	st_conclusoesfinais ,
	st_justificativaacima ,
	st_justificativaabaixo, msa.st_modalidadesaladeaula, uper.id_perfilreferencia, uper.id_usuario, sap.id_saladeaulaprofessor,sap.bl_titular, sap.nu_maximoalunos, tsa.st_descricao, tsa.st_tiposaladeaula, e.st_nomeentidade,

	pl.st_periodoletivo, pl.dt_inicioinscricao as dt_inicioinscricaoletivo, pl.dt_fiminscricao as dt_fiminscricaoletivo,
											 pl.dt_abertura as dt_aberturaletivo, pl.dt_encerramento as dt_encerramentoletivo, s.st_situacao, u.st_nomecompleto as st_nomeprofessor,
	d.id_disciplina, d.st_disciplina
	,sa.id_categoriasala
	,catsala.st_categoriasala
	,CASE WHEN ((sa.dt_encerramento IS NOT NULL) )
	THEN DATEADD(day, ISNULL(nu_diasextensao, 0), sa.dt_encerramento)
	 END as dt_comextensao,
	-- integração
	/*
  case when si.id_saladeaulaintegracao is null then 'Sem integração' else
  (case when (ei.st_nomeintegracao is null or ei.st_nomeintegracao = '') then sis.st_sistema else ei.st_nomeintegracao end)
  end as st_sistema
  */
	sis.st_sistema
from tb_saladeaula as sa
	INNER JOIN tb_modalidadesaladeaula as msa ON msa.id_modalidadesaladeaula = sa.id_modalidadesaladeaula
	LEFT JOIN tb_usuarioperfilentidadereferencia as uper ON uper.id_saladeaula = sa.id_saladeaula AND UPER.bl_ativo = 1 AND UPER.bl_titular = 1
	LEFT JOIN tb_usuario as u ON u.id_usuario = uper.id_usuario
	LEFT JOIN tb_saladeaulaprofessor as sap ON sap.id_perfilreferencia = uper.id_perfilreferencia
	JOIN tb_tiposaladeaula as tsa ON tsa.id_tiposaladeaula = sa.id_tiposaladeaula
	JOIN tb_entidade as e ON e.id_entidade = sa.id_entidade
	--LEFT JOIN tb_entidaderelacao as er ON er.id_entidade = e.id_entidade AND er.id_entidadepai is null -- Ainda não sei se vai funcionar
  --LEFT JOIN tb_entidaderelacao as er2 ON er.id_entidadepai = e.id_entidade -- Ainda não sei se vai funcionar
	LEFT JOIN tb_disciplinasaladeaula as dsa ON dsa.id_saladeaula = sa.id_saladeaula
	LEFT JOIN tb_disciplina as d ON d.id_disciplina = dsa.id_disciplina
	LEFT JOIN tb_periodoletivo as pl ON pl.id_periodoletivo = sa.id_periodoletivo
	JOIN tb_situacao as s ON s.id_situacao = sa.id_situacao
	JOIN tb_categoriasala as catsala ON catsala.id_categoriasala = sa.id_categoriasala
	-- integração
	LEFT JOIN tb_saladeaulaintegracao as si on si.id_saladeaula = sa.id_saladeaula
	LEFT JOIN tb_sistema as sis on si.id_sistema = sis.id_sistema
-- LEFT JOIN tb_entidadeintegracao as ei on si.id_entidadeintegracao = ei.id_entidadeintegracao and ei.id_sistema = sis.id_sistema
where sa.bl_ativa=1


--docs/vw_sql/dbo.vw_salaofertarenovacao.sql

CREATE VIEW [dbo].[vw_salaofertarenovacao] AS
SELECT DISTINCT
	pe.id_entidade,
	mo.id_modulo,
	mo.st_modulo,
	aps.id_saladeaula,
	sa.st_saladeaula,
	sa.nu_maxalunos,
	sa.bl_ofertaexcepcional,
	pp.id_trilha,
	pp.id_projetopedagogico,
	ds.id_disciplina,
	dc.id_tipodisciplina,
	dc.nu_creditos,
	dc.st_disciplina,
	pl.id_periodoletivo,
	pl.st_periodoletivo,
	pl.dt_inicioinscricao,
	pl.dt_fiminscricao,
	pl.dt_abertura,
	pl.dt_encerramento
FROM tb_projetopedagogico AS pp
  JOIN tb_projetoentidade as  pe on pe.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_areaprojetosala AS aps
	ON aps.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_saladeaula AS sa
	ON sa.id_saladeaula = aps.id_saladeaula

	AND sa.bl_ativa = 1
	AND sa.id_situacao = 8
	AND sa.id_categoriasala = 1
	--AND se.id_saladeaula = sa.id_saladeaula
JOIN tb_saladeaulaentidade AS se
	ON (se.id_entidade = pe.id_entidade AND aps.id_saladeaula = se.id_saladeaula) OR sa.bl_todasentidades = 1
JOIN tb_periodoletivo AS pl ON pl.id_periodoletivo IN (SELECT top 5 id_periodoletivo FROM tb_periodoletivo AS plf WHERE plf.dt_abertura >= getdate() AND  sa.id_periodoletivo = plf.id_periodoletivo ORDER BY plf.dt_abertura ASC)
JOIN tb_disciplinasaladeaula AS ds
	ON ds.id_saladeaula = sa.id_saladeaula
JOIN tb_modulo AS mo
	ON mo.id_projetopedagogico = pp.id_projetopedagogico AND mo.bl_ativo = 1
JOIN tb_modulodisciplina AS md
	ON ds.id_disciplina = md.id_disciplina and md.bl_ativo = 1 and mo.id_modulo = md.id_modulo
JOIN tb_disciplina AS dc
	ON dc.id_disciplina = ds.id_disciplina


	--docs/vw_sql/rel.vw_relatorioprodutovendacupom.View.sql
ALTER VIEW [rel].[vw_relatorioprodutovendacupom] as
SELECT
	ROW_NUMBER() OVER (ORDER BY c.id_cupom) AS st_ordem,
	p.id_produto,
	p.st_produto,
	ct.id_categoria,
	ct.st_categoria,
	pdv.nu_valor,
	vp.nu_desconto,
	vp.nu_valorliquido,
	vp.nu_valorbruto,
	v.id_entidade,
	e.st_nomeentidade,
	v.id_venda,
	CASE v.id_cupom
	WHEN CONVERT(int, v.id_cupom) THEN v.dt_cadastro ELSE NULL
	END AS dt_utilizacao,
	cc.id_campanhacomercial,
	cc.st_campanhacomercial,
	c.id_cupom,
	c.st_codigocupom,
	CASE v.id_cupom
	WHEN CONVERT(int, v.id_cupom) THEN 'Sim' ELSE 'Não'
	END AS st_utilizado,
	CASE c.bl_unico
	WHEN 1 THEN 'Sim' ELSE 'Não'
	END AS st_unico,
	c.dt_cadastro AS dt_criacaocupom,
	c.dt_fim AS dt_validadecupom,
	CASE
	WHEN c.dt_fim > GETDATE() THEN 'Sim' ELSE 'Não'
	END AS st_valido,
	(SELECT
	urc.st_nomecompleto
	 FROM tb_usuario urc
	 WHERE urc.id_usuario = c.id_usuariocadastro)
																					AS st_nomeresponsavelcupom,
	uc.id_usuario,
	uc.st_nomecompleto,
	uc.st_cpf,
	pc.sg_uf,
	c.id_tipodesconto,
	CASE c.id_tipodesconto
	WHEN 1 THEN c.nu_desconto ELSE NULL
	END AS nu_valordesconto,
	CASE c.id_tipodesconto
	WHEN 2 THEN c.nu_desconto ELSE NULL
	END AS nu_valordescontoporcentagem,
	CASE c.id_tipodesconto
	WHEN 1 THEN CAST(ISNULL(c.nu_desconto, 0) AS smallmoney) -- ISNULL(pdv.nu_valor, 0) - ISNULL(c.nu_desconto, 0)
	WHEN 2 THEN CAST(ISNULL(pdv.nu_valor, 0) * (ISNULL(c.nu_desconto, 0) / 100) AS smallmoney)
	END AS nu_valordescontocupom,
	h.id_holding,
	h.st_holding
FROM tb_produto p
	join tb_categoriaproduto ctp ON ctp.id_produto = p.id_produto
	join tb_categoria ct ON ct.id_categoria = ctp.id_categoria
	JOIN tb_produtovalor pdv ON pdv.id_produto = p.id_produto
	JOIN tb_vendaproduto vp ON p.id_produto = vp.id_produto
	JOIN tb_venda v ON v.id_venda = vp.id_venda
	JOIN tb_entidade e ON e.id_entidade = v.id_entidade
	JOIN tb_usuario uc ON uc.id_usuario = v.id_usuario
	JOIN tb_pessoa pc ON uc.id_usuario = pc.id_usuario AND pc.id_entidade = v.id_entidade
	LEFT JOIN tb_cupom c ON c.id_cupom = v.id_cupom
	LEFT JOIN tb_campanhacomercial cc ON (cc.id_campanhacomercial = v.id_campanhacomercial OR c.id_campanhacomercial = cc.id_campanhacomercial)
	JOIN tb_holdingfiliada as hf on hf.id_entidade = e.id_entidade
	JOIN tb_holding as h on h.id_holding = hf.id_holding
--ORDER BY c.id_cupom ASC


--docs/vw_sql/vw_entidadesmesmaholdingparams

CREATE view vw_entidadesmesmaholdingparams as
select
	hf.id_entidade,
  e2.st_nomeentidade,
	e.id_entidade as id_entidadeparceira,
	e.st_nomeentidade as st_nomeentidadeparceira,
	h.bl_compartilharcarta as bl_cartacompartilhada
from tb_holdingfiliada hf
join tb_holdingfiliada hf2 on hf2.id_holding = hf.id_holding
join tb_entidade e on e.id_entidade = hf2.id_entidade
join tb_entidade e2 on hf.id_entidade = e2.id_entidade
join tb_holding as h on hf.id_holding = h.id_holding;

--COLOCADO PELO RELEASE
ALTER VIEW [dbo].[vw_vendausuario] AS
SELECT
	v.id_usuario,
	u.st_nomecompleto,
	u.st_cpf,
	v.id_venda,
	v.id_entidade,
	v.id_prevenda,
	v.id_formapagamento,
	fp.st_formapagamento,
	v.id_campanhacomercial,
	cc.st_campanhacomercial,
	v.id_evolucao,
	evo.st_evolucao,
	v.id_situacao,
	s.st_situacao,
	v.bl_ativo,
	v.id_atendente,
	v.id_usuariocadastro,
	ucad.st_nomecompleto as st_nomecompletocadastrador,
	v.nu_valorliquido,
	v.nu_valorbruto,
	v.nu_descontoporcentagem,
	v.nu_descontovalor,
	v.nu_juros,
	v.nu_parcelas,
	v.bl_contrato,
	v.dt_cadastro,
	case when vi.st_urlvendaexterna IS NULL then ef.st_linkloja else vi.st_urlvendaexterna end as st_linkloja,
	cat.id_categoriacampanha,
	cat.st_categoriacampanha,
	e.st_nomeentidade
FROM tb_venda v
	JOIN tb_usuario u ON v.id_usuario = u.id_usuario
	JOIN tb_usuario ucad ON v.id_usuariocadastro = ucad.id_usuario
	JOIN tb_entidade as e on e.id_entidade = v.id_entidade
	LEFT JOIN tb_formapagamento fp ON fp.id_formapagamento = v.id_formapagamento
	JOIN tb_evolucao evo ON evo.id_evolucao = v.id_evolucao
	JOIN tb_situacao s ON v.id_situacao = s.id_situacao
	LEFT JOIN tb_campanhacomercial cc ON cc.id_campanhacomercial = v.id_campanhacomercial
	LEFT JOIN tb_categoriacampanha cat ON cc.id_categoriacampanha = cc.id_categoriacampanha
	LEFT JOIN dbo.tb_entidadefinanceiro ef ON ef.id_entidade = v.id_entidade
	LEFT JOIN dbo.tb_vendaintegracao vi ON vi.id_venda = v.id_venda


CREATE view vw_professoraulasministradas as
	SELECT
		u.id_usuario,
		u.st_nomecompleto,
		igh.id_unidade,
		a.id_areaconhecimento,
		a.st_areaconhecimento,
		e.st_nomeentidade,
		gh.id_gradehoraria,
		igh.dt_diasemana,
		datepart(month,igh.dt_diasemana) as nu_mes,
		datepart(year,igh.dt_diasemana) as nu_ano,
		concat(FORMAT(gh.dt_iniciogradehoraria, 'd/M'),' à ', FORMAT(gh.dt_fimgradehoraria, 'd/MM/yy')) as dt_periodo
	FROM dbo.tb_usuario u
		JOIN tb_usuarioperfilentidade up ON up.id_usuario = u.id_usuario
		JOIN tb_perfil p ON p.id_perfil = up.id_perfil
		JOIN dbo.tb_itemgradehoraria igh ON igh.id_professor = u.id_usuario and igh.bl_ativo = 1
		JOIN dbo.tb_gradehoraria gh ON gh.id_gradehoraria = igh.id_gradehoraria and gh.bl_ativo = 1
		JOIN dbo.tb_entidade e ON igh.id_unidade = e.id_entidade
		JOIN dbo.tb_areaprojetopedagogico app ON app.id_projetopedagogico = igh.id_projetopedagogico
		JOIN dbo.tb_areaconhecimento a ON a.id_areaconhecimento = app.id_areaconhecimento
	WHERE
		p.id_perfilpedagogico = 1
		AND p.bl_ativo = 1
		AND u.bl_ativo = 1
		AND up.bl_ativo = 1
		AND u.bl_ativo = 1
	GROUP BY
		u.id_usuario,
		u.st_nomecompleto,
		igh.id_unidade,
		app.id_projetopedagogico,
		a.id_areaconhecimento,
		a.st_areaconhecimento,
		e.st_nomeentidade,
		gh.id_gradehoraria,
		gh.dt_iniciogradehoraria,
		gh.dt_fimgradehoraria,
		igh.id_disciplina,
		igh.id_gradehoraria,
		igh.dt_diasemana

CREATE VIEW vw_entidadesmesmaholding AS
	select
		distinct
		hf.id_entidade,
		e.id_entidade as id_entidadeparceira,
		e.st_nomeentidade as st_nomeentidadeparceira
	from tb_holdingfiliada hf
		join tb_holdingfiliada hf2 on hf2.id_holding = hf.id_holding
		join tb_entidade e on e.id_entidade = hf2.id_entidade
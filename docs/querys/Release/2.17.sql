INSERT into tb_permissao (id_permissao, st_nomepermissao,st_descricao, bl_ativo) values (28, 'Gerar segunda via de contrato.', 'Permite o usuário gere segunda via de contrato.',1 );

INSERT into tb_permissaofuncionalidade (id_funcionalidade, id_permissao) VALUES (627,28);

ALTER TABLE tb_venda ADD nu_viascontrato INT DEFAULT 0;

ALTER TABLE dbo.tb_venda ADD id_operador INT FOREIGN KEY REFERENCES tb_usuario(id_usuario) NULL;

SET IDENTITY_INSERT dbo.tb_funcionalidade on
INSERT INTO tb_funcionalidade (
id_funcionalidade,
st_funcionalidade,
id_funcionalidadepai,
bl_ativo,
id_situacao,
st_classeflex,
st_urlicone,
id_tipofuncionalidade,
nu_ordem,
st_classeflexbotao,
bl_pesquisa,
bl_lista,
id_sistema,
st_ajuda,
st_target,
st_urlcaminho,
bl_visivel,
bl_relatorio,
st_urlcaminhoedicao
)
VALUES (
722,
'Comercial Geral GRADUAÇÃO',
469,
1,
123,
'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa',
'/img/ico/money--arrow.png',
3,
NULL,
NULL,
0,
0,
1,
NULL,
NULL,
'722',
1,
1,
NULL
);
GO
SET IDENTITY_INSERT dbo.tb_funcionalidade off

go
ALTER TABLE tb_banco ADD bl_ativo bit NOT NULL DEFAULT ((1));
go
ALTER TABLE tb_contapessoa ADD nu_cnpj VARCHAR(25);
go
ALTER TABLE tb_contapessoa ADD id_tipopessoa int;
go
SET IDENTITY_INSERT dbo.tb_funcionalidade ON
insert into tb_funcionalidade values (
714 'Imprimir Tirinhas',	562,	1,	123,	'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa',	'/img/ico/flag.png',	3,	NULL,	'',	0,	0,	1,	NULL,	NULL,	'/grade-horaria-tirinhas',	1,	0,	NULL, 0);
SET IDENTITY_INSERT dbo.tb_funcionalidade OFF
go

DROP PROCEDURE dbo.sp_email_alunos_nao_enviados_pontosoft;
GO;

ALTER PROCEDURE dbo.sp_email_alunos_nao_enviados_pontosoft
AS

  EXEC msdb.dbo.sp_send_dbmail
      @profile_name = ''SQL Server Mail'',
      @recipients =''ludmilla.flores@unyleya.com.br'',
      @query = ''execute as login = ''''elton.gomes''''
	  SET NOCOUNT ON
		select st_nomeentidade as entidade,
		st_nomecompleto as nome,
		id_usuario as "id do usuario",
		id_matricula as "id da matricula",
		dt_inicioturma as "data da matricula",
		st_turma as turma
		from vw_matriculaintegracao_pontosoft'',
  @body_format = ''HTML'',
  @query_attachment_filename = ''Relatorio_alunos_nao_enviados_pontosoft.csv'',
  @subject = ''Alunos não enviados enviados para o banco do pontosoft'',
  @attach_query_result_as_file = 1,
  @query_result_header = 1,
  @query_result_width = 32767,
  @query_result_separator = ''	'',
  @exclude_query_output = 0,
  @append_query_error = 1,
  @query_result_no_padding =1;
GO;
CREATE VIEW [dbo].[vw_entidadecompartilhadados]

AS

SELECT e.st_nomeentidade
, cd.id_entidadedestino
, cd.id_entidadeorigem
, cd.id_tipodados,
eo.st_nomeentidade as st_nomentidadeorigem
FROM tb_compartilhadados  as cd
INNER JOIN tb_entidade as
e ON e.id_entidade = cd.id_entidadedestino
JOIN tb_entidade as eo ON eo.id_entidade = cd.id_entidadeorigem
--where cd.id_entidadeorigem = 1

 UNION

SELECT e.st_nomeentidade
  , e.id_entidade  as id_entidadedestino
  , eo.id_entidade  as id_entidadeorigem
  , 1 as id_tipodados,
    eo.st_nomeentidade as st_nomentidadeorigem
FROM tb_entidade as e
  JOIN tb_holdingfiliada AS hf on e.id_entidade = hf.id_entidade
  JOIN tb_holdingfiliada as hf2 on hf.id_holding = hf2.id_holding
  JOIN tb_entidade as eo ON
eo.id_entidade = hf2.id_entidade and e.id_entidade != eo.id_entidade

UNION

SELECT e.st_nomeentidade
  , e.id_entidade  as id_entidadedestino
  , e.id_entidade  as id_entidadeorigem
  , 1 as id_tipodados,
    e.st_nomeentidade as st_nomentidadeorigem
FROM tb_entidade as e

ALTER VIEW [dbo].[vw_disciplinaserienivel] AS
select d.id_disciplina, d.st_disciplina, d.st_descricao, d.nu_identificador, d.nu_cargahoraria, d.bl_ativa, d.id_situacao, d.id_entidade,
s.id_serie, s.st_serie, s.id_serieanterior, ne.id_nivelensino, ne.st_nivelensino
,d.nu_repeticao
from tb_disciplina d
LEFT JOIN tb_disciplinaserienivelensino as dsn ON d.id_disciplina = dsn.id_disciplina
LEFT JOIN tb_serienivelensino as sne ON dsn.id_nivelensino = sne.id_nivelensino
LEFT JOIN tb_serie as s ON dsn.id_serie = sne.id_serie and sne.id_serie = s.id_serie
LEFT JOIN tb_nivelensino as ne ON sne.id_nivelensino = ne.id_nivelensino
where d.bl_ativa = 1
GO
ALTER VIEW [rel].[vw_atendentenegociacao]
AS
    SELECT  U.st_nomecompleto AS 'st_nomealuno' ,
            US.st_nomecompleto AS 'st_nomeatendente' ,
            US.id_usuario AS 'id_atendente' ,
			OP.id_usuario AS 'id_operador',
			OP.st_nomecompleto AS 'st_nomeoperador',
            vd.id_venda ,
            p.st_produto ,
            vd.nu_parcelas ,
            vp.nu_valorliquido ,
            CASE WHEN vd.nu_valorliquido > 0
                 THEN ( vdlc.nu_valorcartao / vd.nu_valorliquido
                        * vp.nu_valorliquido )
                 ELSE 0
            END AS nu_valorcartao ,
            CASE WHEN vd.nu_valorliquido > 0
                 THEN ( vdlb.nu_valorboleto / vd.nu_valorliquido
                        * vp.nu_valorliquido )
                 ELSE 0
            END AS nu_valorboleto ,
            CASE WHEN vd.nu_valorliquido > 0
                 THEN ( vdld.nu_valordinheiro / vd.nu_valorliquido
                        * vp.nu_valorliquido )
                 ELSE 0
            END AS nu_valordinheiro ,
            CASE WHEN vd.nu_valorliquido > 0
                 THEN ( vdlch.nu_valorcheque / vd.nu_valorliquido
                        * vp.nu_valorliquido )
                 ELSE 0
            END AS nu_valorcheque ,
            CASE WHEN vd.nu_valorliquido > 0
                 THEN ( vdle.nu_valorempenho / vd.nu_valorliquido
                        * vp.nu_valorliquido )
                 ELSE 0
            END AS nu_valorempenho ,
            CASE WHEN vd.nu_valorliquido > 0
                 THEN ( vdldp.nu_valordeposito / vd.nu_valorliquido
                        * vp.nu_valorliquido )
                 ELSE 0
            END AS nu_valordeposito ,
            CASE WHEN vd.nu_valorliquido > 0
                 THEN ( vdlt.nu_valortransferencia / vd.nu_valorliquido
                        * vp.nu_valorliquido )
                 ELSE 0
            END AS nu_valortransferencia ,
            CASE WHEN vd.nu_valorliquido > 0
                 THEN ( vdlp.nu_valorpermuta / vd.nu_valorliquido
                        * vp.nu_valorliquido )
                 ELSE 0
            END AS nu_valorpermuta ,
            CASE WHEN vd.nu_valorliquido > 0
                 THEN ( vdlbl.nu_valorbolsa / vd.nu_valorliquido
                        * vp.nu_valorliquido )
                 ELSE 0
            END AS nu_valorbolsa ,
            CASE WHEN vd.nu_valorliquido > 0
                 THEN ( vdlec.nu_valorestorno / vd.nu_valorliquido
                        * vp.nu_valorliquido )
                 ELSE 0
            END AS nu_valorestorno ,
            CASE WHEN vd.nu_valorliquido > 0
                 THEN ( vdlcc.nu_valorcarta / vd.nu_valorliquido
                        * vp.nu_valorliquido )
                 ELSE 0
            END AS nu_valorcarta ,
            E.st_nomeentidade ,
            E.id_entidade ,
            vd.dt_cadastro
    FROM    dbo.tb_venda AS vd
            OUTER APPLY ( SELECT    SUM(nu_valor) AS nu_valorcartao
                          FROM      dbo.vw_vendalancamento
                          WHERE     id_venda = vd.id_venda
                                    AND id_meiopagamento IN ( 1, 7, 11 )
                        ) AS vdlc
            OUTER APPLY ( SELECT    SUM(nu_valor) AS nu_valorboleto
                          FROM      dbo.vw_vendalancamento
                          WHERE     id_venda = vd.id_venda
                                    AND id_meiopagamento = 2
                        ) AS vdlb
            OUTER APPLY ( SELECT    SUM(nu_valor) AS nu_valordinheiro
                          FROM      dbo.vw_vendalancamento
                          WHERE     id_venda = vd.id_venda
                                    AND id_meiopagamento = 3
                        ) AS vdld
            OUTER APPLY ( SELECT    SUM(nu_valor) AS nu_valorcheque
                          FROM      dbo.vw_vendalancamento
                          WHERE     id_venda = vd.id_venda
                                    AND id_meiopagamento = 4
                        ) AS vdlch
            OUTER APPLY ( SELECT    SUM(nu_valor) AS nu_valorempenho
                          FROM      dbo.vw_vendalancamento
                          WHERE     id_venda = vd.id_venda
                                    AND id_meiopagamento = 5
                        ) AS vdle
            OUTER APPLY ( SELECT    SUM(nu_valor) AS nu_valordeposito
                          FROM      dbo.vw_vendalancamento
                          WHERE     id_venda = vd.id_venda
                                    AND id_meiopagamento = 6
                        ) AS vdldp
            OUTER APPLY ( SELECT    SUM(nu_valor) AS nu_valortransferencia
                          FROM      dbo.vw_vendalancamento
                          WHERE     id_venda = vd.id_venda
                                    AND id_meiopagamento = 8
                        ) AS vdlt
            OUTER APPLY ( SELECT    SUM(nu_valor) AS nu_valorpermuta
                          FROM      dbo.vw_vendalancamento
                          WHERE     id_venda = vd.id_venda
                                    AND id_meiopagamento = 9
                        ) AS vdlp
            OUTER APPLY ( SELECT    SUM(nu_valor) AS nu_valorbolsa
                          FROM      dbo.vw_vendalancamento
                          WHERE     id_venda = vd.id_venda
                                    AND id_meiopagamento = 10
                        ) AS vdlbl
            OUTER APPLY ( SELECT    SUM(nu_valor) AS nu_valorestorno
                          FROM      dbo.vw_vendalancamento
                          WHERE     id_venda = vd.id_venda
                                    AND id_meiopagamento = 12
                        ) AS vdlec
            OUTER APPLY ( SELECT    SUM(nu_valor) AS nu_valorcarta
                          FROM      dbo.vw_vendalancamento
                          WHERE     id_venda = vd.id_venda
                                    AND id_meiopagamento = 13
                        ) AS vdlcc
            JOIN dbo.tb_entidade AS E ON E.id_entidade = vd.id_entidade
            JOIN dbo.tb_vendaproduto AS vp ON vp.id_venda = vd.id_venda
            JOIN dbo.tb_matricula AS mt ON mt.id_matricula = vp.id_matricula
            JOIN dbo.tb_produto AS p ON p.id_produto = vp.id_produto
                                        AND mt.id_evolucao = 6
            JOIN dbo.tb_usuario AS U ON U.id_usuario = vd.id_usuario
            LEFT JOIN dbo.tb_usuario AS US ON US.id_usuario = vd.id_atendente
			LEFT JOIN dbo.tb_usuario AS OP ON OP.id_usuario = vd.id_operador;

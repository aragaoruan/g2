-- Adicionando campo novo no lançamento
alter table tb_lancamento add id_sistemacobranca integer null;

ALTER TABLE tb_lancamento
ADD CONSTRAINT FK_lancamento_sistema FOREIGN KEY (id_sistemacobranca)
    REFERENCES tb_sistema(id_sistema);
GO
ALTER TABLE [tb_pedidointegracao] ALTER COLUMN [nu_bandeiracartao] INTEGER NULL;
ALTER TABLE [tb_pedidointegracao] ADD [st_bandeiracartao] VARCHAR(100) NULL;
ALTER TABLE [tb_transacaofinanceira] ADD [st_statuspagarme] VARCHAR(100) NULL;
alter table tb_situacaointegracao add st_status varchar(30);
go
insert into tb_situacaointegracao (nu_status, st_situacao, id_sistema, dt_cadastro, bl_ativo, st_status) values
(2, 'transação sendo processada', 30, getdate(), 1, 'processing')
,(3, 'transação autorizada. Cliente possui saldo na conta e este valor foi reservado para futura captura, deve acontecer em 5 dias. Caso a transação não seja capturada, a mesma é cancelada automaticamente', 30, getdate(), 1, 'authorized')
,(4, 'transação paga (autorizada e capturada).', 30, getdate(), 1, 'refunded')
,(5, 'transação estornada.', 30, getdate(), 1, 'waiting_payment')
,(6, 'transação não autorizada.', 30, getdate(), 1, 'refused')
,(7, 'transação sofreu chargeback.', 30, getdate(), 1, 'chargedback');

GO
--Script para criar a tabela dbo.tb_tipooferta
CREATE TABLE dbo.tb_tipooferta (
	id_tipooferta INT NOT NULL IDENTITY PRIMARY KEY,
	st_tipooferta VARCHAR(50) NOT NULL
);

--Script para inserir os dados básicos da tabela
SET IDENTITY_INSERT tb_tipooferta ON;
INSERT INTO dbo.tb_tipooferta (id_tipooferta, st_tipooferta) VALUES (1, 'Padrão'), (2, 'Estendida');
SET IDENTITY_INSERT tb_tipooferta OFF;

--Script para inserir uma nova coluna (id_tipooferta) na tabela dbo.tb_periodoletivo
ALTER TABLE dbo.tb_periodoletivo ADD id_tipooferta INT DEFAULT ((1)) NOT NULL;

GO
/**********************************************************************
* ADD coluna nu_semestre tb_projetopedagogico
*              GII-7048
**********************************************************************/


ALTER TABLE tb_projetopedagogico
ADD nu_semestre INT NULL


ALTER TABLE tb_modulodisciplina
ADD nu_obrigatorioalocacao INT NULL,
nu_disponivelapartirdo INT NULL,
nu_percentualsemestre numeric(5, 2) NULL
GO


CREATE TABLE tb_classeafiliacao (
    id_classeafiliacao INT NOT NULL PRIMARY KEY,
    st_classeafiliacao VARCHAR(255) NOT NULL
);

CREATE TABLE tb_afiliado (
    id_afiliado INT NOT NULL PRIMARY KEY,
    st_afiliado VARCHAR(255) NOT NULL
);

CREATE TABLE tb_afiliadoclasseentidade (
    id_afiliado INT NOT NULL FOREIGN KEY REFERENCES tb_afiliado(id_afiliado),
    id_classeafiliacao INT NOT NULL FOREIGN KEY REFERENCES tb_classeafiliacao(id_classeafiliacao),
    id_entidade INT NOT NULL FOREIGN KEY REFERENCES tb_entidade(id_entidade),
    CONSTRAINT pk_afiliadoclasseentidade PRIMARY KEY (id_afiliado, id_classeafiliacao, id_entidade)
);

CREATE TABLE tb_nucleotelemarketing (
    id_nucleotelemarketing INT NOT NULL PRIMARY KEY,
    st_nucleotelemarketing VARCHAR(255) NOT NULL,
    bl_ativo BIT NOT NULL,
    id_entidade INT NOT NULL FOREIGN KEY REFERENCES tb_entidade(id_entidade)
);

CREATE TABLE tb_vendedor (
    id_vendedor INT NOT NULL PRIMARY KEY,
    st_vendedor VARCHAR(255) NOT NULL,
    bl_ativo BIT NOT NULL,
    id_entidade INT NOT NULL FOREIGN KEY REFERENCES tb_entidade(id_entidade)
);

ALTER TABLE tb_venda ADD id_afiliado INT NULL FOREIGN KEY REFERENCES tb_afiliado(id_afiliado);
ALTER TABLE tb_venda ADD id_nucleotelemarketing INT NULL FOREIGN KEY REFERENCES tb_nucleotelemarketing(id_nucleotelemarketing);
ALTER TABLE tb_venda ADD id_vendedor INT NULL FOREIGN KEY REFERENCES tb_vendedor(id_vendedor);
ALTER TABLE tb_venda ADD st_siteorigem VARCHAR(255) NULL;
GO
SET IDENTITY_INSERT tb_permissao ON;
INSERT INTO tb_permissao (id_permissao, st_nomepermissao, st_descricao, bl_ativo) VALUES (40, 'Alterar Núcleo Telemarketing', 'Permitir alterar o Núcleo de Telemarketing da Negociação - Graduação', 1);
INSERT INTO tb_permissao (id_permissao, st_nomepermissao, st_descricao, bl_ativo) VALUES (41, 'Alterar Vendedor', 'Permitir alterar o Vendedor da Negociação - Graduação', 1);
SET IDENTITY_INSERT tb_permissao OFF;

INSERT INTO tb_permissaofuncionalidade (id_funcionalidade, id_permissao) VALUES (760, 40);
INSERT INTO tb_permissaofuncionalidade (id_funcionalidade, id_permissao) VALUES (760, 41);
GO
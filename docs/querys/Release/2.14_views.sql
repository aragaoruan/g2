﻿BEGIN TRAN
go
alter VIEW [dbo].[vw_enviarmensagemaluno] AS 
SELECT
	ed.id_enviomensagem,
	ed.id_usuario,
	ed.st_endereco,
	ed.id_matricula,
	em.id_mensagem,
	em.id_tipoenvio,
	ed.id_evolucao,
	em.dt_cadastro,
	ms.st_mensagem,
	ms.bl_importante,
	em.dt_enviar,
	ms.st_texto,
	ms.dt_cadastro AS dt_cadastromsg
	,ms.id_entidade
	,ed.id_sistema,
	ed.bl_entrega

FROM
	dbo.tb_enviodestinatario AS ed
INNER JOIN dbo.tb_enviomensagem AS em ON ed.id_enviomensagem = em.id_enviomensagem
AND em.id_tipoenvio = 4
INNER JOIN dbo.tb_mensagem AS ms ON ms.id_mensagem = em.id_mensagem
WHERE
	(
		CAST (em.dt_enviar AS DATE) <= CAST (GETDATE() AS DATE)
	)
AND (ed.id_evolucao IN(42, 43))
AND ms.id_situacao = 147
GO

alter VIEW [dbo].[vw_matricula]
AS
    SELECT  mt.id_usuario ,
            us.st_nomecompleto ,
            us.st_cpf ,
            ps.st_urlavatar ,
            ps.dt_nascimento ,
            ps.st_nomepai ,
            ps.st_nomemae ,
            ps.st_email ,
            CAST(ps.nu_ddd AS VARCHAR) + '-' + CAST(ps.nu_telefone AS VARCHAR) AS st_telefone ,
            CAST(ps.nu_dddalternativo AS VARCHAR) + '-'
            + CAST(ps.nu_telefonealternativo AS VARCHAR) AS st_telefonealternativo ,
            mt.id_matricula ,
            pp.st_projetopedagogico ,
            mt.id_situacao ,
            st.st_situacao ,
            mt.id_evolucao ,
            ev.st_evolucao ,
            mt.dt_concluinte ,
            mt.id_entidadematriz ,
            etmz.st_nomeentidade AS st_entidadematriz ,
            mt.id_entidadematricula ,
            etm.st_nomeentidade AS st_entidadematricula ,
            mt.id_entidadeatendimento ,
            eta.st_nomeentidade AS st_entidadeatendimento ,
            mt.bl_ativo ,
            cm.id_contrato ,
            pp.id_projetopedagogico ,
            mt.dt_termino ,
            mt.dt_cadastro ,
            mt.dt_inicio ,
            tm.id_turma ,
            tm.st_turma ,
            tm.dt_inicio AS dt_inicioturma ,
            mt.bl_institucional ,
            tm.dt_fim AS dt_terminoturma ,
            mt.id_situacaoagendamento ,
            mt.id_vendaproduto ,
            mt.dt_termino AS dt_terminomatricula ,
            CAST (RIGHT(master.dbo.fn_varbintohexstr(HASHBYTES('MD5',
                                                              ( CAST (us.id_usuario AS VARCHAR(10)) COLLATE Latin1_General_CI_AI
                                                              + CAST (( CASE
                                                              WHEN mt.id_matricula IS NULL
                                                              THEN 0
                                                              ELSE mt.id_matricula
                                                              END ) AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
                                                              + CAST (5 AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
                                                              + CAST (( 0 ) AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
                                                              + us.st_login
                                                              + us.st_senha
                                                              + CONVERT (VARCHAR(10), GETDATE(), 103) ))),
                        32) AS CHAR(32)) COLLATE Latin1_General_CI_AI AS st_urlacesso ,
            mt.bl_documentacao ,
            ct.id_venda ,
            eta.st_urlportal ,
            mt.st_codcertificacao ,
            ps.st_identificacao ,
            CONVERT(VARCHAR, mtc.dt_cadastro, 103) AS st_dtcadastrocertificacao ,
            CONVERT(VARCHAR, mtc.dt_enviocertificado, 103) AS st_dtenviocertificado ,
            CONVERT(VARCHAR, mtc.dt_retornocertificadora, 103) AS st_dtretornocertificadora ,
            CONVERT(VARCHAR, mtc.dt_envioaluno, 103) AS st_dtenvioaluno ,
            st_codigoacompanhamento ,
            mtc.id_indicecertificado ,
            mt.id_evolucaocertificacao ,
            ap.id_areaconhecimento ,
            pp.bl_disciplinacomplementar ,
            mt.id_matriculavinculada,
            cr.id_contratoregra,
            trc.id_tiporegracontrato,
            etm.st_siglaentidade,
            mt.bl_academico
    FROM    tb_matricula mt
            JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
            JOIN tb_areaprojetopedagogico AS ap ON ap.id_projetopedagogico = pp.id_projetopedagogico
            JOIN tb_entidade AS etmz ON etmz.id_entidade = mt.id_entidadematriz
            JOIN tb_entidade AS etm ON etm.id_entidade = mt.id_entidadematricula
            JOIN tb_entidade AS eta ON eta.id_entidade = mt.id_entidadeatendimento
            JOIN tb_situacao AS st ON st.id_situacao = mt.id_situacao
            JOIN tb_evolucao AS ev ON ev.id_evolucao = mt.id_evolucao
            JOIN tb_usuario AS us ON us.id_usuario = mt.id_usuario
            LEFT JOIN tb_contratomatricula AS cm ON cm.id_matricula = mt.id_matricula
                                                    AND cm.bl_ativo = 1
            LEFT JOIN vw_pessoa AS ps ON mt.id_usuario = ps.id_usuario
                                         AND mt.id_entidadeatendimento = ps.id_entidade
            LEFT JOIN dbo.tb_contrato AS ct ON ct.id_contrato = cm.id_contrato
                                               AND ct.bl_ativo = 1
            LEFT JOIN tb_turma AS tm ON tm.id_turma = mt.id_turma
            LEFT JOIN tb_matriculacertificacao mtc ON mtc.id_matricula = mt.id_matricula
                                                      AND mtc.bl_ativo = 1
            LEFT JOIN dbo.tb_contratoregra AS cr ON cr.id_contratoregra = ct.id_contratoregra
            LEFT JOIN dbo.tb_tiporegracontrato AS trc ON trc.id_tiporegracontrato = cr.id_tiporegracontrato
    WHERE   mt.bl_ativo = 1;


	GO

	ALTER VIEW [dbo].[vw_ocorrencia]
AS

SELECT DISTINCT
  oc.id_ocorrencia,
  uso.st_cpf AS st_cpf,
  uso.st_nomecompleto AS st_nomeinteressado,
  tc1.st_email,
  uso.st_senha, uso.st_login,
  (CAST(tc3.nu_ddd AS VARCHAR) + '-' + CAST(tc3.nu_telefone AS VARCHAR)) AS st_telefone,
  oc.id_matricula,
  oc.id_evolucao,
  oc.id_situacao,
  oc.id_usuariointeressado,
  oc.id_categoriaocorrencia,
  oc.id_assuntoco,
  oc.id_saladeaula,
  oc.st_titulo,
  oc.dt_cadastro,
  oc.st_ocorrencia,
  oc.id_entidade,
  dbo.tb_situacao.st_situacao,
  dbo.tb_evolucao.st_evolucao,
  dbo.tb_assuntoco.st_assuntoco,
  dbo.tb_assuntoco.id_tipoocorrencia,
  dbo.tb_categoriaocorrencia.st_categoriaocorrencia,
  (CAST(CONVERT(VARCHAR, oc.dt_ultimotramite, 103) + ' as ' + CONVERT(VARCHAR, oc.dt_ultimotramite, 108) AS VARCHAR(MAX))) AS st_ultimotramite,
  oc.st_tramite,
  oc.dt_ultimotramite AS dt_ultimotramite,
  oresp.id_usuario AS id_usuarioresponsavel,
  st_nomeresponsavel =
                      CASE
WHEN usr.id_usuario IS NULL THEN 'Năo distribuído'
ELSE usr.st_nomecompleto
                      END,
  GETDATE() - 1 AS dt_atendimento,
  asp.st_assuntoco AS st_assuntocopai
  ,pp.st_projetopedagogico
  ,sa.st_saladeaula
  , mt.id_evolucao AS id_evolucaomatricula
  , oc.id_ocorrenciaoriginal
FROM dbo.tb_ocorrencia AS oc
JOIN tb_usuario AS uso
  ON uso.id_usuario = oc.id_usuariointeressado
INNER JOIN dbo.tb_situacao
  ON dbo.tb_situacao.id_situacao = oc.id_situacao
INNER JOIN dbo.tb_evolucao
  ON dbo.tb_evolucao.id_evolucao = oc.id_evolucao
INNER JOIN dbo.tb_categoriaocorrencia
  ON dbo.tb_categoriaocorrencia.id_categoriaocorrencia = oc.id_categoriaocorrencia
INNER JOIN dbo.tb_assuntoco
  ON oc.id_assuntoco = dbo.tb_assuntoco.id_assuntoco
LEFT JOIN dbo.tb_assuntoco AS asp
  ON asp.id_assuntoco = dbo.tb_assuntoco.id_assuntocopai
LEFT JOIN dbo.tb_ocorrenciaresponsavel AS oresp
  ON oresp.id_ocorrencia = oc.id_ocorrencia
  AND oresp.bl_ativo = 1
LEFT JOIN dbo.tb_usuario AS usr
  ON usr.id_usuario = oresp.id_usuario
LEFT JOIN tb_contatosemailpessoaperfil tc
  ON uso.id_usuario = tc.id_usuario
  AND tc.bl_padrao = 1
  AND tc.bl_ativo = 1
  AND oc.id_entidade = tc.id_entidade
LEFT JOIN tb_contatosemail tc1
  ON tc.id_email = tc1.id_email
LEFT JOIN tb_contatostelefonepessoa tc2
  ON uso.id_usuario = tc2.id_usuario
  AND tc2.bl_padrao = 1
  AND oc.id_entidade = tc2.id_entidade
LEFT JOIN tb_contatostelefone tc3
  ON tc2.id_telefone = tc3.id_telefone
--alteraçőes para impressăo
LEFT JOIN dbo.tb_matricula AS mt
  ON mt.id_matricula = oc.id_matricula
LEFT JOIN dbo.tb_projetopedagogico AS pp
  ON pp.id_projetopedagogico = mt.id_projetopedagogico
LEFT JOIN dbo.tb_saladeaula AS sa
  ON sa.id_saladeaula = oc.id_saladeaula




GO


alter VIEW [rel].[vw_matricula]
AS
    SELECT DISTINCT
            mt.id_matricula ,
            mt.dt_inicio ,
            ct.id_contrato ,
            us.st_nomecompleto ,
            us.st_cpf ,
            p.st_email ,
            CAST(p.nu_ddd AS VARCHAR) + '-' + CAST(p.nu_telefone AS VARCHAR) AS st_telefone ,
            ac.id_areaconhecimento ,
            ac.st_areaconhecimento ,
            pp.id_projetopedagogico ,
            pp.st_projetopedagogico ,
			st.id_situacao ,
            st.st_situacao ,
			ev.id_evolucao ,
            ev.st_evolucao ,
            usr.st_nomecompleto AS st_representante ,
            usa.st_nomecompleto AS st_atendente ,
            vp.nu_valorliquido AS nu_valor ,
            mt.id_entidadeatendimento ,
            p.sg_uf ,
            tm.st_turma, tm.id_turma,
			cast(mt.dt_cadastro as date) as dt_cadastro,
			cast(mt.dt_concluinte as date) as dt_concluinte
    FROM    dbo.tb_matricula mt
            JOIN dbo.tb_usuario AS us ON us.id_usuario = mt.id_usuario
            INNER JOIN vw_pessoa p ON p.id_entidade = mt.id_entidadeatendimento
                                      AND p.id_usuario = mt.id_usuario
            JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
            JOIN dbo.tb_areaprojetopedagogico AS ap ON ap.id_projetopedagogico = pp.id_projetopedagogico
            OUTER APPLY ( SELECT TOP 1
                                    id_areaconhecimento ,
                                    st_areaconhecimento
                          FROM      dbo.tb_areaconhecimento
                          WHERE     id_areaconhecimento = ap.id_areaconhecimento
                                    AND id_tipoareaconhecimento = 1
                        ) AS ac
            JOIN dbo.tb_contratomatricula AS cm ON cm.id_matricula = mt.id_matricula
                                                   AND cm.bl_ativo = 1
            JOIN dbo.tb_contrato AS ct ON ct.id_contrato = cm.id_contrato
                                          AND ct.bl_ativo = 1
            JOIN dbo.tb_venda AS vd ON ct.id_venda = vd.id_venda
            JOIN dbo.tb_vendaproduto AS vp ON vp.id_venda = vd.id_venda
            LEFT JOIN dbo.tb_atendentevenda AS av ON av.id_venda = vd.id_venda
            LEFT JOIN dbo.tb_usuario AS usa ON usa.id_usuario = av.id_usuario
            LEFT JOIN dbo.tb_vendaenvolvido AS ve ON ve.id_venda = vd.id_venda
                                                     AND ve.id_funcao = 1
            LEFT JOIN dbo.tb_usuario AS usr ON usr.id_usuario = ve.id_usuario
            JOIN dbo.tb_situacao AS st ON st.id_situacao = mt.id_situacao
            JOIN dbo.tb_evolucao AS ev ON ev.id_evolucao = mt.id_evolucao
            LEFT JOIN dbo.tb_turma AS tm ON tm.id_turma = mt.id_turma
    WHERE   mt.bl_ativo = 1
           -- AND mt.bl_institucional = 0

go
alter VIEW [dbo].[vw_validaloginportal] AS 
SELECT
	us.id_usuario,
	ps.id_entidade,
	ce.st_email,
	us.st_login,
	da.st_senha AS st_senhaentidade,
	us.st_senha,
	us.st_nomecompleto,
	ps.st_urlavatar,
	cfe.bl_acessoapp
FROM
	dbo.tb_usuario AS us
JOIN dbo.tb_pessoa AS ps ON ps.id_usuario = us.id_usuario
AND ps.bl_ativo = 1
JOIN dbo.tb_contatosemailpessoaperfil AS cep ON cep.id_usuario = ps.id_usuario
AND cep.id_entidade = ps.id_entidade
JOIN dbo.tb_contatosemail AS ce ON ce.id_email = cep.id_email
JOIN dbo.tb_dadosacesso AS da ON da.id_usuario = us.id_usuario
AND da.id_entidade = ps.id_entidade
AND da.bl_ativo = 1
JOIN dbo.tb_configuracaoentidade AS cfe ON cfe.id_entidade = da.id_entidade
GO



ALTER VIEW rel.vw_alunosconcluintes AS

SELECT  mt.id_matricula ,
        us.st_nomecompleto ,
        ps.dt_nascimento ,
        DATEDIFF(YEAR, ps.dt_nascimento, GETDATE()) AS st_idade,
        mt.dt_concluinte,
        mt.id_projetopedagogico,
        mt.id_entidadeatendimento AS id_entidade,

        -- AC-27063 INICIO ALTERACOES
        us.st_cpf,
        (CONCAT(ps.st_rg, ' - ', ps.st_orgaoexpeditor)) AS st_rg,
        mt.st_projetopedagogico,

        CASE mt.bl_documentacao
        WHEN 1 THEN 'OK'
        ELSE 'Pendente'
        END AS st_documentacao,

        CASE mt.bl_academico
        WHEN 1 THEN 'OK'
        ELSE 'Pendente'
        END AS st_situacaoacademica,

        '' AS st_tcc,
        '' AS st_cargahoraria,
        '' AS st_disciplina,
        '' AS st_nota,
        '' AS st_certificadoapto,
        '' AS st_observacao
        -- AC-27063 FIM ALTERACOES

FROM    dbo.vw_matricula mt
        JOIN dbo.tb_usuario AS us ON us.id_usuario = mt.id_usuario
        JOIN dbo.vw_pessoa AS ps ON ps.id_usuario = us.id_usuario AND ps.id_entidade = mt.id_entidadeatendimento
WHERE   mt.id_evolucao = 15 AND mt.bl_ativo = 1 AND dt_concluinte IS NOT NULL AND mt.bl_institucional = 0

GO

Alter view [rel].[vw_cargaconcluinte] as

SELECT
 DISTINCT mt.id_usuario
, upper(mt.st_nomecompleto) as st_nomecompleto
, pessoa.dt_nascimento
, mt.id_turma AS id_turma
, mt.st_cpf
, sa.id_categoriasala
, upper(pessoa.st_endereco) as st_endereco
, upper(pessoa.st_bairro) as st_bairro
, pessoa.st_cep
, mp.st_nomemunicipio as st_cidade
, pessoa.sg_uf
, CAST(pessoa.nu_ddd AS VARCHAR) + '-' + CAST( pessoa.nu_telefone AS varchar) as st_telefone
, mt.st_email
, mt.dt_cadastro
, st_sexo = case
        WHEN pessoa.st_sexo like 'F' THEN 'FEMININO'
                 ELSE 'MASCULINO'
        END
, 'C---------' AS separador1 
,  mt.id_projetopedagogico
,  UPPER(mt.st_projetopedagogico) as st_projetopedagogico
,  CAST(ppj.nu_cargahoraria as INT)  as nu_cargahoraria
,  CAST(ppj.nu_cargahoraria as VARCHAR(20))  as st_cargahoraria
,  mt.dt_inicioturma AS dt_inicioturma
,  mt.dt_terminoturma AS dt_terminoturma
,  'D---------' AS separador2 
,  td.id_disciplina
,  UPPER(td.st_tituloexibicao) as st_disciplina
,  replace((select nu_aprovafinal from tb_matriculadisciplina 
              where id_disciplina = td.id_disciplina AND id_matricula=mt.id_matricula)/10,',','.') as nu_notatotal
, replace(CAST((select nu_aprovafinal from tb_matriculadisciplina  
              where id_disciplina = td.id_disciplina AND id_matricula=mt.id_matricula)/10 AS DECIMAL(18,1))  ,',','.'  ) as st_notatotal
,  'P---------' AS separador3 
, tb_usuario.id_usuario AS id_professor
,  UPPER(tb_usuario.st_nomecompleto) AS st_coordenador
,  UPPER(tbti.st_titulacao) AS st_nivel
,  UPPER(tb_usuario.st_nomecompleto) as st_nomeprofessor
,  mt.id_matricula 
,  sa.id_saladeaula AS id_disciplinaturma
,  UPPER(mt.st_evolucao) as st_evolucao
,  tbti.st_titulacao AS st_titulocoordenador
,  td.nu_cargahoraria  AS  st_cargadisciplina
,  mt.dt_concluinte
,  mt.id_entidadematricula
,  (select UPPER(st_tituloavaliacao) from vw_avaliacaoaluno where id_saladeaula=al.id_saladeaula and id_matricula=mt.id_matricula and bl_ativo=1 and id_tipoavaliacao=6) as st_tituloavaliacao
, CASE WHEN mt.id_entidadematricula not in (292,5) THEN 'WPÓS'
                 ELSE mt.st_entidadematricula
END st_entidadematricula
, (pessoa.st_rg  + ' - ' + pessoa.st_orgaoexpeditor) AS st_rg,
mt.st_codcertificacao,
('W' + cast(mt.id_turma as varchar)) as st_periodorealizacao,
convert(varchar, mtc.dt_cadastro, 105) as st_dtemissao
FROM dbo.tb_alocacao al
JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula
JOIN dbo.tb_matriculadisciplina AS md ON md.id_matriculadisciplina = al.id_matriculadisciplina
JOIN dbo.tb_disciplina AS td ON td.id_disciplina = md.id_disciplina
JOIN dbo.vw_matricula AS mt ON mt.id_matricula = md.id_matricula AND mt.bl_ativo = 1
JOIN dbo.vw_pessoa as pessoa on pessoa.id_usuario = mt.id_usuario AND pessoa.id_entidade = mt.id_entidadematricula
LEFT JOIN tb_municipio as mp on mp.id_municipio = pessoa.id_municipio
JOIN tb_projetopedagogico as ppj ON ppj.id_projetopedagogico = mt.id_projetopedagogico AND ppj.bl_disciplinacomplementar = 0
LEFT JOIN vw_saladeaula as vw_sala on vw_sala.id_saladeaula=al.id_saladeaula and vw_sala.id_projetopedagogico=ppj.id_projetopedagogico and vw_sala.bl_ativa=1 and vw_sala.bl_titular=1
/*LEFT JOIN tb_usuarioperfilentidadereferencia as ucoor on ucoor.id_projetopedagogico=mt.id_projetopedagogico and ucoor.bl_titular=1 
LEFT JOIN dbo.tb_perfil ON dbo.tb_perfil.id_perfil = ucoor.id_perfil
                                   AND dbo.tb_perfil.st_nomeperfil LIKE '%Coordenador de Projeto Pedag�gico%'
LEFT JOIN dbo.tb_usuario ON dbo.tb_usuario.id_usuario = ucoor.id_usuario 
LEFT JOIN dbo.tb_titulacao as tbti ON tbti.id_titulacao = tb_usuario.id_titulacao
*/
LEFT JOIN tb_perfil AS pf ON pf.id_entidade = mt.id_entidadematricula
                                         AND pf.id_perfilpedagogico = 9 and pf.bl_ativo = 1
LEFT JOIN tb_usuarioperfilentidadereferencia AS ucoor ON ucoor.id_projetopedagogico = mt.id_projetopedagogico
                                                              AND ucoor.id_disciplina = td.id_disciplina
                                                                                              AND ucoor.id_perfil = pf.id_perfil
                                                              AND ( ( mt.dt_concluinte BETWEEN ucoor.dt_inicio
                                                              AND ucoor.dt_fim ) OR ( mt.dt_concluinte > ucoor.dt_inicio
                                                              AND ucoor.dt_fim IS NULL
                                                              )
                                                              )
                                                              AND ucoor.bl_titular = 1
                                                              AND ucoor.bl_ativo = 1
            LEFT JOIN tb_usuario  ON tb_usuario.id_usuario = ucoor.id_usuario
            LEFT JOIN tb_titulacao tbti ON ucoor.id_titulacao = tbti.id_titulacao
            LEFT JOIN tb_matriculacertificacao mtc on mtc.id_matricula = mt.id_matricula
WHERE al.bl_ativo = 1
and mt.id_evolucao = 15
and mt.id_evolucaocertificacao = 62
and sa.id_categoriasala = 1
and td.st_disciplina not like '%AMBIENTAÇÃO%'

go


alter view [rel].[vw_provasporpolo] as 
select 
	ap.id_aplicadorprova,
	aa.id_avaliacaoaplicacao,
	ape.id_entidade,
	ap.st_aplicadorprova, 
	aa.dt_aplicacao, 
	ha.hr_inicio, 
	ha.hr_fim, 
	aa.nu_maxaplicacao,
	(select count(id_avaliacaoagendamento) from tb_avaliacaoagendamento saa where saa.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao) as nu_totalagendamentos,
	(select count(id_avaliacaoagendamento) from tb_avaliacaoagendamento saa2 where saa2.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao and saa2.id_tipodeavaliacao = 0) as nu_agendamentoprovafinal,
	(select count(id_avaliacaoagendamento) from tb_avaliacaoagendamento saa3 where saa3.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao and saa3.id_tipodeavaliacao > 0) as nu_agendamentorecuperacao,
	(nu_maxaplicacao - (select count(id_avaliacaoagendamento) from tb_avaliacaoagendamento saa4 where saa4.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao)) as nu_vagasrestantes
from tb_aplicadorprova ap
join tb_aplicadorprovaentidade ape on ape.id_aplicadorprova = ap.id_aplicadorprova
join tb_avaliacaoaplicacao aa on aa.id_aplicadorprova = ap.id_aplicadorprova
join tb_avaliacao a on a.id_entidade = ape.id_entidade and a.id_tipoavaliacao in(1,2,3,4)
join tb_horarioaula ha on ha.id_horarioaula = aa.id_horarioaula


--ROLLBACK
--commit

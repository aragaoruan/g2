SELECT * FROM dbo.vw_vendasrecebimento WHERE id_venda = 157967

SELECT* FROM dbo.tb_formapagamentoaplicacao

SELECT * FROM dbo.tb_formapagamento WHERE id_entidade = 41
UPDATE tb_funcionalidade
   SET st_urlcaminho = '/relatorio-comercial-ecommerce'
      ,bl_relatorio = 0
 WHERE id_funcionalidade = 641
 GO
 alter table tb_saladeaula
add id_usuariocancelamento int null

alter table tb_saladeaula
add dt_cancelamento datetime null
constraint FK_TB_SALADEAULA_REFERENCE_TB_USUARIO foreign key (id_usuariocancelamento) references tb_usuario(id_usuario)


--query para adicionar nova permissao
set identity_insert tb_permissao on;
insert into tb_permissao (id_permissao, st_nomepermissao, st_descricao, bl_ativo)
values (13, 'Cancelar', 'Permitir usuario alterar situacao para cancelado', 1);
set identity_insert tb_permissao off;

--query para vincular a funcionalidade sala de aula a nova permissao criada
insert into tb_permissaofuncionalidade (id_funcionalidade, id_permissao) values (23, 13);
GO


/*
 * SQL PARA FUNCIONALIDADE DE REGISTRO DE NOTIFICACAO DE PERCENTUAL DE LIMITE DE ALUNOS EM SALA DE AULA
 *
 * issue http://jira.unyleya.com.br/browse/AC-26849
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */

set identity_insert dbo.tb_funcionalidade on

INSERT INTO tb_funcionalidade
(id_funcionalidade,
 st_funcionalidade,
 id_funcionalidadepai,
 bl_ativo,
 id_situacao,
 st_classeflex,
 st_urlicone,
 id_tipofuncionalidade,
 nu_ordem,
 st_classeflexbotao,
 bl_pesquisa,
 bl_lista,
 id_sistema,
 st_ajuda,
 st_target,
 st_urlcaminho,
 bl_visivel,
 bl_relatorio,
 st_urlcaminhoedicao,
 bl_delete)
VALUES (688,
        'Notifica��es',
        231,
        1,
        123,
        'br.com.ead1.gestor2.view.conteudo.entidade.cadastrar.CadastrarEntidadeFuncionalidade',
        '/img/ico/ui-check-boxes.png',
        3,
        null,
        null,
        0,
        0,
        1,
        'http://wiki.unyleya.com.br/index.php/Funcionalidades',
        null,
        '/organizacao/notificacoes',
        1,
        0,
        null,
        0)

set identity_insert dbo.tb_funcionalidade off


create table tb_notificacao (
        id_notificacao int primary key identity(1,1),
        st_notificacao varchar(50),
        dt_cadastro datetime default getdate()
);


create table tb_notificacaoentidade (
        id_notificacaoentidade int primary key identity(1,1),
        id_notificacao int,
        id_entidade int,
        bl_enviaparaemail bit,
        nu_percentuallimitealunosala int null,
        dt_cadastro datetime default getdate(),
        constraint FK_TB_NOTIFICACAO_ENTIDADE_TB_NOTIFICACAO foreign key (id_notificacao) references tb_notificacao(id_notificacao),
        constraint FK_TB_NOTIFICACAO_ENTIDADE_TB_ENTIDADE foreign key (id_entidade) references tb_entidade (id_entidade)
);

create table tb_notificacaoentidadeperfil (
        id_notificacaoentidadeperfil int primary key identity(1,1),
        id_notificacaoentidade int,
        id_perfil int,
        constraint FK_TB_NOTIFICACAO_ENTIDADE_PERFIL_TB_NOTIFICACAO_ENTIDADE foreign key (id_notificacaoentidade) references tb_notificacaoentidade(id_notificacaoentidade),
        constraint FK_TB_NOTIFICACAO_ENTIDADE_PERFIL_TB_PERFIL foreign key (id_perfil) references tb_perfil (id_perfil)
);

set identity_insert dbo.tb_notificacao on

INSERT INTO tb_notificacao (id_notificacao,st_notificacao,dt_cadastro)
VALUES (1,'Limite de Aluno em Sala de Aula','2015-05-14 15:01:34')

set identity_insert dbo.tb_notificacao OFF

GO
set identity_insert dbo.tb_funcionalidade on
INSERT INTO dbo.tb_funcionalidade
        (id_funcionalidade, 
		  st_funcionalidade ,
          id_funcionalidadepai ,
          bl_ativo ,
          id_situacao ,
          st_classeflex ,
          st_urlicone ,
          id_tipofuncionalidade ,
          nu_ordem ,
          st_classeflexbotao ,
          bl_pesquisa ,
          bl_lista ,
          id_sistema ,
          st_ajuda ,
          st_target ,
          st_urlcaminho ,
          bl_visivel ,
          bl_relatorio ,
          st_urlcaminhoedicao ,
          bl_delete
        )
VALUES  ( 679,
          'Entrega de Material Presencial' , -- st_funcionalidade - varchar(100)
          98 , -- id_funcionalidadepai - int
          1 , -- bl_ativo - bit
          123 , -- id_situacao - int
          '' , -- st_classeflex - varchar(300)
          '/img/ico/pencil.png' , -- st_urlicone - varchar(1000)
          3 , -- id_tipofuncionalidade - int
          NULL , -- nu_ordem - int
          'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa' , -- st_classeflexbotao - varchar(300)
          0 , -- bl_pesquisa - bit
          0 , -- bl_lista - bit
          1 , -- id_sistema - int
          NULL , -- st_ajuda - varchar(max)
          NULL , -- st_target - varchar(100)
          '/entrega-de-material-presencial' , -- st_urlcaminho - varchar(50)
          1 , -- bl_visivel - int
          0 , -- bl_relatorio - bit
          NULL , -- st_urlcaminhoedicao - varchar(50)
          0  -- bl_delete - bit
        )

set identity_insert dbo.tb_funcionalidade OFF
GO




/*********************************************************************************************

� necess�ria a cria��o da pasta materiaispresenciais dentro da pasta upload e dar a permiss�o.
/public/upload/materiaispresenciais/

**********************************************************************************************/
alter table tb_itemdematerial add nu_valor numeric null;
alter table tb_itemdematerial add bl_portal bit null;
alter table tb_itemdematerial add nu_encontro int null;
alter table tb_itemdematerial add id_professor int null;
alter table tb_itemdematerial add bl_ativo bit null;
alter table tb_itemdematerial add id_entidade int null;

ALTER TABLE [dbo].tb_itemdematerial  WITH CHECK ADD FOREIGN KEY(id_professor)
REFERENCES [dbo].tb_usuario (id_usuario)

ALTER TABLE [dbo].tb_itemdematerial  WITH CHECK ADD FOREIGN KEY(id_entidade)
REFERENCES [dbo].tb_entidade (id_entidade)

create table tb_itemdematerialturma (
id_itemdematerialturma int not null IDENTITY(1,1),
id_material int not null,
id_turma int not null,
bl_ativo bit null)

ALTER TABLE [dbo].tb_itemdematerialturma  WITH CHECK ADD FOREIGN KEY(id_material)
REFERENCES [dbo].tb_itemdematerial (id_itemdematerial)

ALTER TABLE [dbo].tb_itemdematerialturma  WITH CHECK ADD FOREIGN KEY(id_turma)
REFERENCES [dbo].tb_turma (id_turma)


ALTER TABLE dbo.tb_itemdematerial ALTER COLUMN nu_valor NUMERIC (18,2)
-------------------------------------------------------------------------------
go
CREATE VIEW [dbo].[vw_materialturmadisciplina] AS
SELECT im.*,
mat.st_nomeentidade as st_entidade,
dis.id_disciplina as id_disciplina,
dis.st_disciplina as st_disciplina,
us.st_nomecompleto as st_professor,
tip.st_tipodematerial as st_tipodematerial,
sit.st_situacao as st_situacao,
tur.id_turma,
tur.st_turma
FROM tb_itemdematerial as im
JOIN tb_itemdematerialdisciplina AS idi on idi.id_itemdematerial = im.id_itemdematerial
JOIN tb_itemdematerialturma AS imt on im.id_itemdematerial = imt.id_material AND imt.bl_ativo = 1
JOIN tb_turma AS tur on tur.id_turma = imt.id_turma
JOIN tb_entidade AS mat on mat.id_entidade = im.id_entidade
JOIN tb_disciplina AS dis on dis.id_disciplina = idi.id_disciplina
JOIN tb_usuario AS us on us.id_usuario = im.id_professor
JOIN tb_tipodematerial AS tip ON tip.id_tipodematerial = im.id_tipodematerial
JOIN tb_situacao AS sit ON sit.id_situacao = im.id_situacao


go

--------------------------------------------------------------------------------
set identity_insert dbo.tb_funcionalidade on
INSERT INTO dbo.tb_funcionalidade
        ( id_funcionalidade,
		  st_funcionalidade ,
          id_funcionalidadepai ,
          bl_ativo ,
          id_situacao ,
          st_classeflex ,
          st_urlicone ,
          id_tipofuncionalidade ,
          nu_ordem ,
          st_classeflexbotao ,
          bl_pesquisa ,
          bl_lista ,
          id_sistema ,
          st_ajuda ,
          st_target ,
          st_urlcaminho ,
          bl_visivel ,
          bl_relatorio ,
          st_urlcaminhoedicao ,
          bl_delete
        )
VALUES  ( 675,
          'Item de Material Presencial' , -- st_funcionalidade - varchar(100)
          568 , -- id_funcionalidadepai - int
          1 , -- bl_ativo - bit
          123 , -- id_situacao - int
          '' , -- st_classeflex - varchar(300)
          '/img/ico/people.png' , -- st_urlicone - varchar(1000)
          3 , -- id_tipofuncionalidade - int
          NULL , -- nu_ordem - int
          'br.com.ead1.gestor2.view.conteudo.pedagogico.cadastrar.ItemdeMaterialPresencial' , -- st_classeflexbotao - varchar(300)
          0 , -- bl_pesquisa - bit
          0 , -- bl_lista - bit
          1 , -- id_sistema - int
          NULL , -- st_ajuda - varchar(max)
          NULL , -- st_target - varchar(100)
          '/item-de-material-presencial' , -- st_urlcaminho - varchar(50)
          1 , -- bl_visivel - int
          0 , -- bl_relatorio - bit
          NULL , -- st_urlcaminhoedicao - varchar(50)
          0  -- bl_delete - bit
        )
set identity_insert dbo.tb_funcionalidade off

-------------------------------------------------------------------------------------
go

ALTER VIEW [dbo].[vw_turmadisciplina] AS
SELECT DISTINCT
	tm.id_turma,
	tm.st_turma,
	te.id_entidade,
	mdd.id_disciplina,
	disc.st_disciplina,
	tm.id_evolucao,
	tm.id_situacao,
	disc.id_situacao AS id_situacaodisciplina
FROM
	tb_turma AS tm
JOIN dbo.tb_turmaentidade AS te ON te.id_turma = tm.id_turma
AND te.bl_ativo = 1
JOIN dbo.tb_turmaprojeto AS tp ON tp.id_turma = tm.id_turma
AND tp.bl_ativo = 1
JOIN dbo.tb_modulo AS md ON md.id_projetopedagogico = tp.id_projetopedagogico
AND md.bl_ativo = 1
JOIN dbo.tb_modulodisciplina AS mdd ON mdd.id_modulo = md.id_modulo
AND mdd.bl_ativo = 1
JOIN dbo.tb_disciplina AS disc ON disc.id_disciplina = mdd.id_disciplina
AND disc.bl_ativa = 1
WHERE
	tm.bl_ativo = 1
GO

--Executar para salvar backup das tabelas e dados
exec sp_rename tb_itemgradehoraria, tb_itemgradehorariaOLD;
exec sp_rename tb_itemgradehorariaturma, tb_itemgradehorariaturmaOLD;



go

CREATE TABLE [dbo].[tb_itemgradehoraria] (
[id_itemgradehoraria] int NOT NULL IDENTITY(1,1) ,
[id_gradehoraria] int NOT NULL ,
[id_unidade] int NOT NULL ,
[id_entidadecadastro] int NOT NULL ,
[id_turno] int NOT NULL ,
[id_turma] int NOT NULL ,
[id_projetopedagogico] int NOT NULL ,
[id_diasemana] int NOT NULL ,
[dt_diasemana] date NOT NULL ,
[id_professor] int NOT NULL ,
[id_disciplina] int NOT NULL ,
[bl_ativo] bit NOT NULL,
[dt_cadastro] datetime2 NULL DEFAULT (getdate()) ,
[st_observacao] varchar(255) NULL ,
[nu_encontro] int NULL ,
PRIMARY KEY ([id_itemgradehoraria]),
CONSTRAINT [FK_tb_itemgradehoraria_x_tb_gradehoraria] FOREIGN KEY ([id_gradehoraria]) REFERENCES [dbo].[tb_gradehoraria] ([id_gradehoraria]) ON DELETE NO ACTION ON UPDATE NO ACTION,
CONSTRAINT [FK_tb_itemgradehoraria_x_tb_entidade] FOREIGN KEY ([id_unidade]) REFERENCES [dbo].[tb_entidade] ([id_entidade]) ON DELETE NO ACTION ON UPDATE NO ACTION,
CONSTRAINT [FK_tb_itemgradehoraria_x_tb_entidadecadastro] FOREIGN KEY ([id_entidadecadastro]) REFERENCES [dbo].[tb_entidade] ([id_entidade]) ON DELETE NO ACTION ON UPDATE NO ACTION,
CONSTRAINT [FK_tb_itemgradehoraria_x_tb_turno] FOREIGN KEY ([id_turno]) REFERENCES [dbo].[tb_turno] ([id_turno]) ON DELETE NO ACTION ON UPDATE NO ACTION,
CONSTRAINT [FK_tb_itemgradehoraria_x_tb_turma] FOREIGN KEY ([id_turma]) REFERENCES [dbo].[tb_turma] ([id_turma]) ON DELETE NO ACTION ON UPDATE NO ACTION,
CONSTRAINT [FK_tb_itemgradehoraria_x_tb_projetopedagogico] FOREIGN KEY ([id_projetopedagogico]) REFERENCES [dbo].[tb_projetopedagogico] ([id_projetopedagogico]) ON DELETE NO ACTION ON UPDATE NO ACTION,
CONSTRAINT [FK_tb_itemgradehoraria_x_tb_diasemana] FOREIGN KEY ([id_diasemana]) REFERENCES [dbo].[tb_diasemana] ([id_diasemana]) ON DELETE NO ACTION ON UPDATE NO ACTION,
CONSTRAINT [FK_tb_itemgradehoraria_x_tb_usuario] FOREIGN KEY ([id_professor]) REFERENCES [dbo].[tb_usuario] ([id_usuario]) ON DELETE NO ACTION ON UPDATE NO ACTION,
CONSTRAINT [FK_tb_itemgradehoraria_x_tb_disciplina] FOREIGN KEY ([id_disciplina]) REFERENCES [dbo].[tb_disciplina] ([id_disciplina]) ON DELETE NO ACTION ON UPDATE NO ACTION
)

GO

BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_funcionalidade ON
INSERT INTO [dbo].[tb_funcionalidade] (
	[id_funcionalidade],
	[st_funcionalidade],
	[id_funcionalidadepai],
	[bl_ativo],
	[id_situacao],
	[id_tipofuncionalidade],
	[st_classeflexbotao],
	[bl_pesquisa],
	[bl_lista],
	[id_sistema],
	[st_urlcaminho],
	[bl_visivel],
	[bl_relatorio]
)
VALUES
	(
		678,
		'Grade Hor�ria por Projeto e Entidade',
		257,
		1,
		123,
		3,
		'br.com.ead1.gestor2.view.conteudo.secretaria.cadastrar.GradeHorariaProjetoEntidade',
		0,
		0,
		1,
		'/relatorio-grade-projeto-entidade',
		1,
		0
	)
GO

SET IDENTITY_INSERT dbo.tb_funcionalidade OFF

COMMIT

GO

ALTER TABLE dbo.tb_entidade ADD st_urllogoutportal VARCHAR(255) NULL

go
SET IDENTITY_INSERT tb_mensagempadrao ON
INSERT INTO tb_mensagempadrao (id_mensagempadrao, id_tipoenvio, st_mensagempadrao, st_default) VALUES (31, 3, 'Altera��o cadastro usu�rio', 'Mensagem Aviso de Altera��o no Cadastro de Usu�rio');
SET IDENTITY_INSERT tb_mensagempadrao OFF

GO

ALTER TABLE dbo.tb_matricula
ADD dt_ultimoacesso DATETIME NULL;




--AC-27137

CREATE view [dbo].[vw_avaliacaoaluno] as
SELECT DISTINCT
  ui.st_codusuario as CODUSUARIO,
  ui.id_entidade as ENTIDADEINTEGRACAO,
  ui.id_usuario as USUARIO,
  mt.id_entidadematricula as ENTIDADEMATRICULA,
  mt.id_entidadeatendimento AS ENTIDADEATENDIMENTO,
  id_avaliacaoaluno ,
  mt.id_matricula ,
  us.st_nomecompleto ,
  mdl.id_modulo ,
  mdl.st_tituloexibicao AS st_tituloexibicaomodulo ,
  dc.id_disciplina ,
  dc.st_tituloexibicao AS st_tituloexibicaodisciplina ,
  aa.st_nota ,
  ag.id_avaliacaoagendamento ,
  acr.id_avaliacao ,
  av.st_avaliacao ,
  md.id_situacao ,
  st.st_situacao ,
  ac.id_tipoprova ,
  acrf.id_avaliacaoconjuntoreferencia ,
  ( CASE WHEN avref.id_avaliacaoconjuntoreferencia = acrf.id_avaliacaoconjuntoreferencia
    THEN 1
    ELSE 0
    END ) bl_agendamento ,
  md.id_evolucao ,
  ev.st_evolucao ,
  CAST(av.nu_valor AS VARCHAR(100)) AS nu_notamax ,
  ac.id_tipocalculoavaliacao ,
  av.id_tipoavaliacao ,
  ta.st_tipoavaliacao ,
  ( CASE WHEN acr.id_avaliacaorecupera IS NULL THEN 0
    ELSE acr.id_avaliacaorecupera
    END ) AS id_avaliacaorecupera ,
  ag.bl_provaglobal ,
  pp.nu_notamaxima ,
  pp.nu_percentualaprovacao ,
  ag.dt_agendamento ,
  aa.bl_ativo ,
  aa.dt_avaliacao ,
  alc.id_saladeaula ,
  ui.st_codusuario ,
  ac.id_avaliacaoconjunto ,
  pp.st_projetopedagogico ,
  mt.id_projetopedagogico ,
  aa.dt_defesa ,
  aa.st_tituloavaliacao ,
  mt.id_entidadeatendimento ,
  aa.id_upload ,
  dc.id_tipodisciplina ,
  aa.st_justificativa ,
  md.id_matriculadisciplina ,
  aa.id_tiponota ,
  sda.dt_cadastro AS dt_cadastrosala ,
  sda.dt_abertura AS dt_aberturasala ,
  sda.dt_encerramento AS dt_encerramentosala ,
  sda.id_categoriasala,
  aa.dt_cadastro AS dt_cadastroavaliacao ,
  aa.id_usuariocadastro AS id_usuariocadavaliacao ,
  usc.st_nomecompleto AS st_usuariocadavaliacao ,
  sdis.id_evolucao AS id_evolucaodisciplina ,
  sdis.st_evolucao AS st_evolucaodisciplina ,
  dc.nu_cargahoraria ,
  agc.id_provasrealizadas ,
  CONVERT(VARCHAR(11), aa.dt_cadastro, 103) AS dt_cadastrotcc ,
  CONVERT(VARCHAR(11), aa.dt_avaliacao, 103) as st_dtavaliacao,
  st_sala.st_situacao AS st_situacaosala,
  up.st_upload
FROM    tb_matricula AS mt
  JOIN dbo.tb_usuario AS us ON us.id_usuario = mt.id_usuario
  JOIN tb_modulo AS mdl ON mdl.id_projetopedagogico = mt.id_projetopedagogico
                           AND mdl.bl_ativo = 1
  JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
  JOIN tb_modulodisciplina AS mdc ON mdc.id_modulo = mdl.id_modulo
                                     AND md.id_disciplina = mdc.id_disciplina
                                     AND mdc.bl_ativo = 1
  JOIN tb_alocacao AS alc ON alc.id_matriculadisciplina = md.id_matriculadisciplina
                             AND alc.bl_ativo = 1
  JOIN tb_avaliacaoconjuntoreferencia AS acrf ON acrf.id_saladeaula = alc.id_saladeaula
                                                 AND acrf.dt_fim IS NULL
  --OR acrf.dt_fim >= GETDATE()
  JOIN tb_saladeaula sda ON sda.id_saladeaula = alc.id_saladeaula
  JOIN tb_disciplinasaladeaula AS dsa ON dsa.id_saladeaula = alc.id_saladeaula
  JOIN tb_disciplina AS dc ON dc.id_disciplina = md.id_disciplina
  JOIN tb_evolucao AS sdis ON sdis.id_evolucao = md.id_evolucao
  JOIN tb_avaliacaoconjunto AS ac ON ac.id_avaliacaoconjunto = acrf.id_avaliacaoconjunto
                                     AND id_tipoprova = 2
  JOIN tb_avaliacaoconjuntorelacao AS acr ON acr.id_avaliacaoconjunto = ac.id_avaliacaoconjunto
  JOIN tb_avaliacao AS av ON av.id_avaliacao = acr.id_avaliacao
  JOIN tb_tipoavaliacao AS ta ON ta.id_tipoavaliacao = av.id_tipoavaliacao
  LEFT JOIN tb_avaliacaoagendamento AS ag ON mt.id_matricula = ag.id_matricula
                                             AND av.id_avaliacao = ag.id_avaliacao
                                             AND ag.id_situacao = 68
                                             AND ag.nu_presenca = 1
  LEFT JOIN tb_avalagendamentoref AS avref ON avref.id_avaliacaoagendamento = ag.id_avaliacaoagendamento
                                              AND avref.id_avaliacaoconjuntoreferencia = acrf.id_avaliacaoconjuntoreferencia
  LEFT JOIN tb_avaliacaoaluno AS aa ON aa.id_matricula = mt.id_matricula
                                       AND aa.id_avaliacaoconjuntoreferencia = acrf.id_avaliacaoconjuntoreferencia
                                       AND aa.id_avaliacao = av.id_avaliacao
                                       AND aa.bl_ativo = 1
                                       AND aa.id_situacao = 86
  JOIN tb_situacao AS st ON st.id_situacao = md.id_situacao
  JOIN tb_evolucao AS ev ON ev.id_evolucao = md.id_evolucao
  JOIN tb_projetopedagogico AS PP ON pp.id_projetopedagogico = mt.id_projetopedagogico
  OUTER APPLY ( SELECT    COUNT(id_avaliacaoagendamento) AS id_provasrealizadas
                FROM      tb_avaliacaoagendamento
                WHERE     id_matricula = mt.id_matricula
                          AND nu_presenca = 1
                          AND bl_ativo = 0
                          AND id_situacao = 68
              ) AS agc
  LEFT JOIN dbo.tb_usuariointegracao AS ui ON ui.id_usuario = mt.id_usuario AND ( ui.id_entidade = sda.id_entidade)
  LEFT JOIN tb_usuario AS usc ON usc.id_usuario = aa.id_usuariocadastro
  JOIN tb_situacao AS st_sala ON st_sala.id_situacao = sda.id_situacao
  LEFT JOIN tb_upload up ON up.id_upload = aa.id_upload
WHERE   mt.bl_ativo = 1 and dc.id_tipodisciplina <> 3

--AC-27137

CREATE VIEW [dbo].[vw_gerardeclaracao]
AS
  SELECT DISTINCT
    mt.id_matricula ,
    ps.id_usuario AS id_usuarioaluno ,
    ps.st_nomecompleto AS st_nomecompletoaluno ,
    ps.st_login AS st_loginaluno ,
    ps.st_senhaentidade AS st_senhaaluno ,
    ps.st_cpf AS st_cpfaluno ,
    ps.st_rg AS st_rgaluno ,
    ps.st_orgaoexpeditor ,
    ps.dt_dataexpedicao AS dt_dataexpedicaoaluno ,
    ps.st_nomemae AS st_filiacao ,
    CONVERT(CHAR, mt.dt_cadastro, 103) AS st_datamatricula ,
    canc.id_cancelamento ,
    canc.st_observacao AS st_observacaocancelamento ,
    canc.st_observacaocalculo AS st_observacao_calculo ,
    CONVERT(CHAR, canc.dt_solicitacao, 103) AS st_solicitacaocancelamento ,
    canc.nu_valorcarta AS st_valorcredito ,
    CONVERT(CHAR, DATEADD(D, 365, canc.dt_solicitacao), 103) AS st_validadecredito ,
    cm.id_contrato ,
    ps.dt_nascimento AS dt_nascimentoaluno ,
    UPPER(SUBSTRING(ps.st_nomemunicipio, 1, 1))
    + LOWER(SUBSTRING(ps.st_nomemunicipio, 2, 499)) AS st_municipioaluno ,
    ps.st_estadoprovincia AS st_ufaluno ,
    ps.sg_uf AS sg_ufaluno ,
    ps.nu_ddd AS nu_dddaluno ,
    ps.nu_telefone AS nu_telefonealuno ,
    ps.st_email AS st_emailaluno ,
    pp.st_tituloexibicao AS st_projeto ,
    --pp.nu_cargahoraria,
    ( SELECT    SUM(dis.nu_cargahoraria) AS nu_cargahoraria
      FROM      tb_matriculadisciplina md
        JOIN tb_matricula m ON m.id_matricula = md.id_matricula
        JOIN tb_disciplina dis ON dis.id_disciplina = md.id_disciplina
      WHERE     md.id_matricula = mt.id_matricula
    ) AS nu_cargahoraria ,
    ne.st_nivelensino ,
    mt.dt_concluinte AS dt_concluintealuno ,
    us_coordenador.st_nomecompleto AS st_coordenadorprojeto ,
    fl.id_fundamentolegal AS id_fundamentolei ,
    fl.nu_numero AS nu_lei ,
    flr.id_fundamentolegal AS id_fundamentoresolucao ,
    tur.id_turma AS id_turma ,
    tur.st_turma ,
    tur.dt_inicio AS dt_inicioturma ,
    tur.dt_fim AS dt_terminoturma ,
    flr.nu_numero AS nu_resolucao ,
    flpr.id_fundamentolegal AS id_fundamentoparecer ,
    flpr.nu_numero AS nu_parecer ,
    flp.id_fundamentolegal AS id_fundamentoportaria ,
    flp.nu_numero AS nu_portaria ,
    et.id_entidade ,
    et.st_nomeentidade ,
    et.st_razaosocial ,
    et.st_cnpj ,
    et.st_urlimglogo ,
    et.st_urlsite ,
    ps.st_nomepais ,
    ps.st_sexo AS sexo_aluno ,
    UPPER(tcc.st_tituloavaliacao) AS st_titulomonografia ,
    CONVERT(VARCHAR(2), DATEPART(DAY, GETDATE()), 103) + ' de '
    + dbo.fn_mesporextenso(GETDATE()) + ' de '
    + CONVERT(VARCHAR(4), DATEPART(YEAR, GETDATE()), 103) AS st_data ,
    DAY(GETDATE()) AS st_dia ,
    LOWER(dbo.fn_mesporextenso(GETDATE())) AS st_mes ,
    YEAR(GETDATE()) AS st_ano ,

    --CONVERT(DATE, sa.dt_abertura, 103) AS dt_primeirasala ,
    (
      SELECT TOP 1 ts.dt_abertura
      FROM tb_matriculadisciplina tm
        JOIN tb_alocacao ta ON tm.id_matriculadisciplina = ta.id_matriculadisciplina
        JOIN tb_saladeaula ts ON ta.id_saladeaula = ts.id_saladeaula
      WHERE tm.id_matricula = mt.id_matricula AND tm.bl_obrigatorio = 1
      ORDER BY ts.dt_abertura ASC
    ) AS dt_primeirasala,

    --CONVERT(DATE, DATEADD(MONTH, 13, sa.dt_abertura), 103) AS dt_previsaofim ,
    (
      SELECT TOP 1
        --DATEADD(MONTH, 13, ts.dt_abertura)

          'dt_previsaotermino' = CASE WHEN ts.id_categoriasala = 2 THEN (
          DATEADD(day, ISNULL(ts.nu_diasaluno, 0), ts.dt_abertura)
        ) ELSE (
          CASE WHEN ts.dt_encerramento IS NOT NULL THEN (
            DATEADD(day, ISNULL(ts.nu_diasextensao, 0) + 30, ts.dt_encerramento)
          ) WHEN ts.dt_abertura IS NOT NULL THEN (
            DATEADD(day, ISNULL(ts.nu_diasextensao, 0) + 30, ts.dt_abertura)
          ) ELSE (
            NULL
          ) END
        ) END

      FROM tb_matriculadisciplina tm
        JOIN tb_alocacao ta ON tm.id_matriculadisciplina = ta.id_matriculadisciplina
        JOIN tb_saladeaula ts ON ta.id_saladeaula = ts.id_saladeaula
      WHERE tm.id_matricula = mt.id_matricula AND tm.bl_obrigatorio = 1
      ORDER BY dt_previsaotermino DESC, ts.dt_abertura DESC, ts.dt_encerramento DESC
    ) AS dt_previsaofim,

    UPPER(tcc.st_tituloavaliacao) AS st_titulotcc,
    tcc.dt_aberturasala AS dt_iniciotcc,
    tcc.dt_encerramentosala AS dt_terminotcc,
    tcc.st_nota AS nu_notatcc,
    tcc.dt_cadastroavaliacao AS dt_cadastrotcc,
    tcc.dt_defesa AS dt_defesatcc,

    mt.st_codcertificacao ,
    CONVERT(CHAR, GETDATE(), 103) AS st_atual
    , matc.st_codigoacompanhamento AS st_codigorasteamento,

    mt.id_matriculaorigem,
    mto.dt_inicio AS dt_iniciomatriculaorigem


  FROM    tb_matricula mt
    JOIN tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
    JOIN tb_entidade AS et ON et.id_entidade = mt.id_entidadeatendimento
    LEFT JOIN tb_entidadeendereco AS ete ON ete.id_entidade = et.id_entidade
                                            AND ete.bl_padrao = 1
    LEFT JOIN tb_endereco AS etee ON etee.id_endereco = ete.id_endereco
    JOIN vw_pessoa AS ps ON ps.id_usuario = mt.id_usuario
                            AND ps.id_entidade = mt.id_entidadeatendimento
    LEFT JOIN tb_fundamentolegal AS fl ON fl.id_entidade = mt.id_entidadematriz
                                          AND fl.id_tipofundamentolegal = 1
                                          AND fl.dt_publicacao <= GETDATE()
                                          AND fl.dt_vigencia >= GETDATE()
    LEFT JOIN tb_fundamentolegal AS flp ON flp.id_entidade = mt.id_entidadematriz
                                           AND flp.id_tipofundamentolegal = 2
                                           AND flp.dt_publicacao <= GETDATE()
                                           AND flp.dt_vigencia >= GETDATE()
    LEFT JOIN tb_fundamentolegal AS flr ON flr.id_entidade = mt.id_entidadematriz
                                           AND flr.id_tipofundamentolegal = 3
                                           AND flr.dt_publicacao <= GETDATE()
                                           AND flr.dt_vigencia >= GETDATE()
    LEFT JOIN tb_fundamentolegal AS flpr ON flpr.id_entidade = mt.id_entidadematriz
                                            AND flpr.id_tipofundamentolegal = 4
                                            AND flpr.dt_publicacao <= GETDATE()
                                            AND flpr.dt_vigencia >= GETDATE()
    LEFT JOIN tb_projetopedagogicoserienivelensino AS psn ON psn.id_projetopedagogico = mt.id_projetopedagogico
    LEFT JOIN tb_turma AS tur ON tur.id_turma = mt.id_turma
    JOIN tb_nivelensino AS ne ON ne.id_nivelensino = psn.id_nivelensino
    LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS uper ON uper.id_projetopedagogico = pp.id_projetopedagogico
                                                                AND uper.bl_ativo = 1
                                                                AND uper.bl_titular = 1
    LEFT JOIN dbo.tb_usuario AS us_coordenador ON us_coordenador.id_usuario = uper.id_usuario

    LEFT JOIN tb_matricula AS mto ON mt.id_matriculaorigem = mto.id_matricula

    LEFT JOIN dbo.vw_avaliacaoaluno tcc ON ( tcc.id_matricula = mt.id_matricula
                                             AND tcc.id_tipoavaliacao = 6
                                             AND tcc.st_tituloavaliacao IS NOT NULL
                                             AND tcc.id_tipodisciplina = 2
                                             AND tcc.bl_ativo = 1
      )
    LEFT JOIN tb_cancelamento AS canc ON canc.id_cancelamento = mt.id_cancelamento AND mt.id_evolucao = 27
    LEFT JOIN tb_contratomatricula AS cm ON cm.id_matricula = mt.id_matricula
    LEFT JOIN tb_matriculacertificacao AS matc ON matc.id_matricula = mt.id_matricula


--COM-11526

CREATE VIEW [dbo].[vw_cartacredito] as
SELECT
	cc.id_cartacredito,
	cancelamento.id_cancelamento,
	cc.dt_cadastro,
	cc.nu_valororiginal,
	(SELECT
		ISNULL(CONVERT(FLOAT, SUM(utilizado.nu_valorutilizado)), 0)
	FROM tb_vendacartacredito AS utilizado
	JOIN tb_venda utilizadovenda
		ON utilizadovenda.id_venda = utilizado.id_venda
	WHERE utilizadovenda.id_evolucao NOT IN (7, 8)
	AND utilizado.id_cartacredito = cc.id_cartacredito)
	AS nu_valorutilizado,
	(cc.nu_valororiginal - (SELECT
		ISNULL(CONVERT(FLOAT, SUM(disponivel.nu_valorutilizado)), 0)
	FROM tb_vendacartacredito AS disponivel
	JOIN tb_venda disponivelvenda
		ON disponivelvenda.id_venda = disponivel.id_venda
	WHERE disponivelvenda.id_evolucao NOT IN (7, 8)
	AND disponivel.id_cartacredito = cc.id_cartacredito)
	) AS nu_valordisponivel,
	cc.id_usuariocadastro,
	cc.id_usuario,
	cc.bl_ativo,
	cc.id_situacao,
	s.st_situacao,
	cc.id_entidade,
	e.st_nomeentidade
FROM tb_cartacredito AS cc
JOIN tb_situacao s
	ON s.id_situacao = cc.id_situacao
JOIN tb_entidade e
	ON e.id_entidade = cc.id_entidade
JOIN tb_cancelamento cancelamento
  on cancelamento.id_cancelamento = cc.id_cancelamento;
GO

--COM-11658

CREATE view vw_controleturma as
SELECT
    et.st_nomeentidade as st_unidade,
    et.id_entidade as id_unidade,
    tm.id_turma,
    tm.st_turma,
    tm.dt_inicio,
    tm.dt_fim,
    tm.st_codigo,
    gh.id_gradehoraria,
    gh.st_nomegradehoraria,
    gh.dt_iniciogradehoraria,
    igh.dt_diasemana,
    gh.dt_fimgradehoraria,
    igh.id_professor,
    us.st_nomecompleto as st_professor,
    dc.id_disciplina,
    dc.st_disciplina,
    eci.st_valor,
    md.id_modulo,
    md.st_modulo,
    dc.nu_cargahoraria as nu_cargahorariadisciplina,
    (COUNT(igh.id_itemgradehoraria)*IIF(st_valor = '',0,(CAST(ISNULL(st_valor,0) AS INT)))) as nu_cargahorariagrade,
    (dc.nu_cargahoraria - (COUNT(igh.id_itemgradehoraria)*IIF(st_valor = '',0,(CAST(ISNULL(st_valor,0) AS INT))))) as nu_cargarestante,
    pp.id_projetopedagogico,
    pp.st_projetopedagogico,
    pp.nu_cargahoraria as nu_cargahorariaprojeto,
    tur.st_turno
FROM tb_turma AS tm
JOIN tb_produtoprojetopedagogico AS ppp ON ppp.id_turma =  tm.id_turma
JOIN tb_entidade AS et ON et.id_entidade = tm.id_entidadecadastro
JOIN tb_esquemaconfiguracaoitem AS eci ON eci.id_esquemaconfiguracao = et.id_esquemaconfiguracao AND eci.id_itemconfiguracao = 3
JOIN tb_projetopedagogico as pp on pp.id_projetopedagogico = ppp.id_projetopedagogico
JOIN tb_modulo as md ON md.id_projetopedagogico = pp.id_projetopedagogico and md.bl_ativo = 1
JOIN tb_modulodisciplina as mdc on mdc.id_modulo = md.id_modulo and mdc.bl_ativo = 1
JOIN tb_disciplina AS dc ON dc.id_disciplina = mdc.id_disciplina
LEFT JOIN tb_itemgradehoraria AS igh ON igh.id_turma = tm.id_turma AND igh.bl_ativo = 1 and dc.id_disciplina = igh.id_disciplina
LEFT JOIN tb_gradehoraria AS gh ON gh.id_gradehoraria = igh.id_gradehoraria
LEFT JOIN dbo.tb_turmaturno AS tmt ON tmt.id_turma = tm.id_turma
LEFT JOIN dbo.tb_turno AS tur ON tur.id_turno = tmt.id_turno
LEFT JOIN dbo.tb_usuario AS us ON igh.id_professor = us.id_usuario
GROUP BY et.st_nomeentidade,
    et.st_nomeentidade,
    et.id_entidade,
    tm.id_turma,
    TM.st_turma,
    tm.dt_inicio,
    tm.dt_fim,
    tm.st_codigo,
    gh.id_gradehoraria,
    gh.st_nomegradehoraria,
    gh.dt_iniciogradehoraria,
    gh.dt_fimgradehoraria,
    igh.dt_diasemana,
    igh.id_professor,
    us.st_nomecompleto,
    dc.id_disciplina,
    dc.st_disciplina,
    eci.st_valor,
    dc.nu_cargahoraria,
    md.id_modulo,
    md.st_modulo,
    pp.id_projetopedagogico,
    pp.st_projetopedagogico,
    pp.nu_cargahoraria,
    tur.st_turno

--COM-11526

CREATE VIEW rel.vw_relatoriocartacredito AS
SELECT
  mat.id_matricula AS 'id_matricula',
  u.st_nomecompleto AS 'st_nomecompleto',
  sit.id_situacao AS 'id_situacao',
  sit.st_situacao AS 'st_situacao',
  canc.dt_solicitacao AS 'dt_solicitacao',
  canc.nu_valortotal AS 'nu_valortotal',
  DATEADD(YEAR, 1, carta.dt_cadastro) as 'dt_validade',
  carta.nu_valororiginal,
  carta.nu_valorutilizado,
  carta.nu_valordisponivel,
  canc.nu_valordevolucao AS 'nu_valordevolucao',
  canc.id_cancelamento AS 'id_cancelamento',
  carta.id_entidade AS 'id_entidade',
  e.st_nomeentidade as 'st_entidade',
  t.st_turma as 'st_turma',
  u.st_cpf AS 'st_cpf',
  carta.id_cartacredito
FROM
  tb_matricula AS mat
  JOIN tb_usuario AS u ON u.id_usuario = mat.id_usuario
  JOIN tb_cancelamento AS canc ON canc.id_cancelamento = mat.id_cancelamento
  JOIN dbo.vw_cartacredito AS carta ON carta.id_cancelamento = canc.id_cancelamento
  JOIN tb_situacao AS sit ON sit.id_situacao = carta.id_situacao
  JOIN tb_entidade as e on e.id_entidade = carta.id_entidade
  JOIN tb_turma as t on t.id_turma = mat.id_turma

  --TEC-464

CREATE VIEW [dbo].[vw_ocorrencia]
AS

SELECT DISTINCT
  oc.id_ocorrencia,
  uso.st_cpf AS st_cpf,
  uso.st_nomecompleto AS st_nomeinteressado,
  tc1.st_email,
  uso.st_senha, uso.st_login,
  (CAST(tc3.nu_ddd AS VARCHAR) + '-' + CAST(tc3.nu_telefone AS VARCHAR)) AS st_telefone,
  oc.id_matricula,
  oc.id_evolucao,
  oc.id_situacao,
  oc.id_usuariointeressado,
  oc.id_categoriaocorrencia,
  oc.id_assuntoco,
  oc.id_saladeaula,
  oc.st_titulo,
  oc.dt_cadastro,
  oc.st_ocorrencia,
  oc.id_entidade,
  dbo.tb_situacao.st_situacao,
  dbo.tb_evolucao.st_evolucao,
  dbo.tb_assuntoco.st_assuntoco,
  dbo.tb_assuntoco.id_tipoocorrencia,
  dbo.tb_categoriaocorrencia.st_categoriaocorrencia,
  (CAST(CONVERT(VARCHAR, oc.dt_ultimotramite, 103) + ' as ' + CONVERT(VARCHAR, oc.dt_ultimotramite, 108) AS VARCHAR(MAX))) AS st_ultimotramite,
  oc.st_tramite,
  oc.dt_ultimotramite AS dt_ultimotramite,
  oresp.id_usuario AS id_usuarioresponsavel,
  st_nomeresponsavel =
                      CASE
WHEN usr.id_usuario IS NULL THEN 'Năo distribuído'
ELSE usr.st_nomecompleto
                      END,
  GETDATE() - 1 AS dt_atendimento,
  asp.st_assuntoco AS st_assuntocopai
  ,pp.st_projetopedagogico
  ,sa.st_saladeaula
  , mt.id_evolucao AS id_evolucaomatricula
  , oc.id_ocorrenciaoriginal,
  oc.st_codissue
FROM dbo.tb_ocorrencia AS oc
JOIN tb_usuario AS uso
  ON uso.id_usuario = oc.id_usuariointeressado
INNER JOIN dbo.tb_situacao
  ON dbo.tb_situacao.id_situacao = oc.id_situacao
INNER JOIN dbo.tb_evolucao
  ON dbo.tb_evolucao.id_evolucao = oc.id_evolucao
INNER JOIN dbo.tb_categoriaocorrencia
  ON dbo.tb_categoriaocorrencia.id_categoriaocorrencia = oc.id_categoriaocorrencia
INNER JOIN dbo.tb_assuntoco
  ON oc.id_assuntoco = dbo.tb_assuntoco.id_assuntoco
LEFT JOIN dbo.tb_assuntoco AS asp
  ON asp.id_assuntoco = dbo.tb_assuntoco.id_assuntocopai
LEFT JOIN dbo.tb_ocorrenciaresponsavel AS oresp
  ON oresp.id_ocorrencia = oc.id_ocorrencia
  AND oresp.bl_ativo = 1
LEFT JOIN dbo.tb_usuario AS usr
  ON usr.id_usuario = oresp.id_usuario
LEFT JOIN tb_contatosemailpessoaperfil tc
  ON uso.id_usuario = tc.id_usuario
  AND tc.bl_padrao = 1
  AND tc.bl_ativo = 1
  AND oc.id_entidade = tc.id_entidade
LEFT JOIN tb_contatosemail tc1
  ON tc.id_email = tc1.id_email
LEFT JOIN tb_contatostelefonepessoa tc2
  ON uso.id_usuario = tc2.id_usuario
  AND tc2.bl_padrao = 1
  AND oc.id_entidade = tc2.id_entidade
LEFT JOIN tb_contatostelefone tc3
  ON tc2.id_telefone = tc3.id_telefone
--alteraçőes para impressăo
LEFT JOIN dbo.tb_matricula AS mt
  ON mt.id_matricula = oc.id_matricula
LEFT JOIN dbo.tb_projetopedagogico AS pp
  ON pp.id_projetopedagogico = mt.id_projetopedagogico
LEFT JOIN dbo.tb_saladeaula AS sa
  ON sa.id_saladeaula = oc.id_saladeaula
BEGIN TRANSACTION

--GII-6628

SET IDENTITY_INSERT tb_permissao ON
insert into tb_permissao (id_permissao, st_nomepermissao, st_descricao)
values(43, 'Editar Aproveitamento', 'Permite editar um aproveitamento de disciplina');
SET IDENTITY_INSERT tb_permissao OFF

insert into tb_permissaofuncionalidade (id_funcionalidade, id_permissao) VALUES (744, 43);







COMMIT
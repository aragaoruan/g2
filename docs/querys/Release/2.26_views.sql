-- Faltando:
-- vw_clientevenda

ALTER view [dbo].[vw_clientevenda] AS
SELECT DISTINCT
	vd.id_usuario,
	us.st_nomecompleto,
	us.st_cpf,
  pe.st_email,
	ev.id_evolucao,
	ev.st_evolucao,
	vd.id_venda,
	vd.nu_valorliquido,
	vd.bl_contrato,
	nu_valorbruto,
	vd.dt_cadastro,
	st.id_situacao,
	st.st_situacao,
	et.id_entidade,
	et.st_nomeentidade,
	ct.id_evolucao AS id_evolucaocontrato,
	ct.id_situacao AS id_situacaocontrato,
	ef.id_textosistemarecibo,
	ef.id_reciboconsolidado
FROM
	tb_venda AS vd
JOIN tb_usuario AS us ON us.id_usuario = vd.id_usuario
JOIN tb_evolucao AS ev ON ev.id_evolucao = vd.id_evolucao
JOIN tb_situacao AS st ON st.id_situacao = vd.id_situacao
JOIN tb_entidade AS et ON et.id_entidade = vd.id_entidade
LEFT JOIN vw_pessoa as pe on pe.id_usuario = us.id_usuario and pe.id_entidade = vd.id_entidade
LEFT JOIN tb_contrato AS ct ON ct.id_venda = vd.id_venda
LEFT JOIN tb_entidadefinanceiro ef ON ef.id_entidade = et.id_entidade
WHERE
	vd.bl_ativo = 1 and us.bl_ativo = 1
GO

ALTER VIEW [dbo].[vw_gerarcontrato]
AS
    SELECT DISTINCT
            ct.id_contrato ,
            psr.id_usuario AS id_usuarioresponsavel ,
            ps.id_usuario AS id_usuarioaluno ,
            ps.st_nomecompleto AS st_nomecompletoaluno ,
            ps.st_cpf AS st_cpfaluno ,
            ps.st_rg AS st_rgaluno ,
            ps.st_estadocivil ,
            ps.st_orgaoexpeditor AS st_orgaoexpedidor ,
            CONVERT(NVARCHAR(30), ps.dt_dataexpedicao, 103) AS st_dataexpedicaoaluno ,
            NULL AS st_numerotitulo ,
            NULL AS st_areatitulo ,
            NULL AS st_zonatitulo ,
            CONVERT(NVARCHAR(30), ps.dt_dataexpedicao, 103) AS st_dataexpedicaotitulo ,
            NULL AS st_numeroreservita ,
--CAST(ct.nu_bolsa + ' %' AS varchar) AS st_bolsa,
            NULL AS st_numerocertidao ,
            NULL AS st_livrocertidao ,
            NULL AS st_folhascertidao ,
            NULL AS st_descricaocartorio ,
            NULL AS st_cargo ,
            psr.dt_nascimento AS dt_nascimentoresponsavel ,
            psr.st_rg AS st_rgresp ,
            psr.st_orgaoexpeditor AS st_orgaoexpedidorresp ,
            CONVERT(NVARCHAR(30), psr.dt_dataexpedicao, 103) AS st_dataexpedicaoresp ,
            psr.nu_telefone AS nu_telefoneresp ,
            psr.st_nomecompleto AS st_nomecompletoresponsavel ,
            psr.st_cpf AS st_cpfresponsavel ,
--cr.id_tipocontratoresponsavel,
--tcr.st_tipocontratoresponsavel,
            1 AS id_tipocontratoresponsavel_financeiro ,--FINANCEIRO
            'FINANCEIRO' AS st_tipocontratoresponsavel_financeiro ,--FINANCEIRO
            '100%' AS nu_porcentagem ,
            psr.st_endereco AS st_enderecoresponsavel ,
            psr.st_complemento AS st_complementoresponsavel ,
            psr.st_cep AS st_cepresponsavel ,
            psr.st_cidade AS st_cidaderesponsavel ,
            psr.st_bairro AS st_bairroresponsavel ,
            psr.sg_uf AS sg_ufresponsavel ,
            ps.dt_nascimento AS dt_nascimentoaluno ,
            ps.st_endereco AS st_enderecoaluno ,
            ps.st_bairro AS st_bairroaluno ,
            ps.st_cep AS st_cepaluno ,
            ps.st_nomemunicipio AS st_municipioaluno ,
            ps.st_cidade AS st_cidadealuno ,
            ps.st_nomepais AS st_paisaluno ,
            ps.sg_uf AS st_ufaluno ,
            NULL AS st_ufcompletoaluno ,
            NULL AS st_nacionalidadealuno ,
            ps.nu_ddd AS nu_dddaluno ,
            ps.nu_telefone AS nu_telefonealuno ,
            ps.st_nomemae ,
            ps.st_nomepai ,
            etee.st_endereco AS st_enderecoentidade ,
            etee.st_bairro AS st_bairroentidade ,
            etee.st_cep AS st_cepentidade ,
            null AS st_emailentidade ,
            '(' + eCtel.nu_ddd + ') ' + eCtel.nu_telefone AS nu_telefoneEntiadeConcat ,
            CONVERT(NVARCHAR(30), GETDATE(), 103) AS st_datahora ,
            CASE WHEN ps.st_sexo = 'm' THEN 'Masculino'
                 WHEN ps.st_sexo = 'f' THEN 'Feminino'
                 ELSE 'Não Informado'
            END AS st_sexoaluno ,
--pp.st_tituloexibicao as st_projeto,
            ( SELECT    CONVERT(VARCHAR(1000), STUFF(( SELECT ' / '
                                                              + st_tituloexibicao
                                                       FROM   ( SELECT
                                                              pp.st_tituloexibicao
                                                              FROM
                                                              tb_contratomatricula as ctm
                                                              join tb_matricula as mt on mt.id_matricula = ctm.id_matricula
                                                              JOIN tb_projetopedagogico
                                                              AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
                                                  WHERE
                          ctm.id_contrato = ct.id_contrato and ctm.bl_ativo = 1
                                                              ) AS cursosreferencia
                                                     FOR
                                                       XML PATH('')
                                                     ), 1, 3, ''))
            ) AS st_projeto ,
            DAY(GETDATE()) AS nu_dia ,
            MONTH(GETDATE()) AS nu_mes ,
            YEAR(GETDATE()) AS nu_ano ,
            et.id_entidade ,
            et.st_nomeentidade ,
            et.st_razaosocial ,
            et.st_cnpj ,
            et.st_urlimglogo ,
            et.st_urlsite ,
            et.st_nomeentidade AS st_polo ,
            usd.st_nomecompleto AS st_diretor ,
            uss.st_nomecompleto AS st_secretarioescolar ,
            vd.nu_diamensalidade ,
            NULL AS st_etnia ,
            ps.st_email ,
            NULL AS st_tiposanguineo ,
            NULL AS st_alergias ,
            NULL AS st_glicemia ,
            vd.nu_valorliquido nu_valorliquido_venda ,
            vd.nu_parcelas AS nu_parcelas_venda ,
--mp.st_meiopagamento,
            vd.dt_confirmacao AS dt_confirmacao_venda ,
            vd.nu_viascontrato AS nu_viascontrato,
            CONVERT(NVARCHAR(30), GETDATE(), 103) AS dt_aceite ,
            vd.id_venda ,
            ( SELECT    CONVERT(VARCHAR(1000), STUFF(( SELECT ' , '
                                                              + CONVERT(NVARCHAR(30), dt_inicio, 103)
                                                       FROM   ( SELECT
                                                              tur.dt_inicio
                                                              FROM
                                                              tb_turma AS tur
                                                              WHERE
                                                              ct.id_venda = vd.id_venda
                                                              AND vdp.id_venda = vd.id_venda
                                                              AND vdp.id_turma = tur.id_turma
                                                              ) AS turmareferencia
                                                     FOR
                                                       XML PATH('')
                                                     ), 1, 3, ''))
            ) AS st_inicio ,
            ( SELECT    CONVERT(VARCHAR(1000), STUFF(( SELECT ' , ' + st_turno
                                                       FROM   ( SELECT
                                                              turno.st_turno
                                                              FROM
                                                              tb_turno AS turno
                                                              JOIN tb_turmaturno
                                                              AS turmaturno ON turmaturno.id_turno = turno.id_turno
                                                              JOIN tb_turma AS turma ON turma.id_turma = turmaturno.id_turma
                                                              WHERE
                                                              vdp.id_turma = turma.id_turma
                                                              AND vdp.id_venda = ct.id_venda
                                                              ) AS turnoreferencia
                                                     FOR
                                                       XML PATH('')
                                                     ), 1, 3, ''))
            ) AS st_turno ,
            ( SELECT    CONVERT(VARCHAR(1000), STUFF(( SELECT ' , '
                                                              + CONVERT(VARCHAR(100), nu_cargahoraria)
                                                   FROM   ( SELECT
                                                              ppp.nu_cargahoraria
                                                   FROM
                                                              tb_produtoprojetopedagogico
                                                              AS ppp
                                                              JOIN tb_vendaproduto
                                                              AS vdp ON ppp.id_produto = vdp.id_produto
                                                              JOIN tb_projetopedagogico
                                                              AS pp ON pp.id_projetopedagogico = ppp.id_projetopedagogico
                                                              WHERE
                                                              vdp.id_venda = ct.id_venda
                                                              ) AS turmareferencia
                                                     FOR
                                                       XML PATH('')
                                                     ), 1, 3, ''))
            ) AS nu_cargahoraria ,
            ( SELECT    CONVERT(VARCHAR(1000), STUFF(( SELECT ' , '
                                                              + st_tituloexibicao
                                                       FROM   ( SELECT
                                                              tur.st_tituloexibicao
                                                              FROM
                                                              tb_turma AS tur
                                                              WHERE
                                                              ct.id_venda = vdp.id_venda
                                                              AND vdp.id_turma = tur.id_turma
                                                              ) AS turmareferencia
                                                     FOR
                                                       XML PATH('')
                                                     ), 1, 3, ''))
            ) AS st_turma
			--
            ,
            ctr.id_contratomodelo,
            cpp.st_campanhacomercial as st_campanhapontualidade
			,pa_m.dt_aceita as st_aceita
			--
    FROM    tb_contrato ct
            JOIN tb_venda AS vd ON vd.id_venda = ct.id_venda
			--
			-- AND vd.id_venda IN (103855, 103915)
			--
            JOIN tb_vendaproduto AS vdp ON vdp.id_venda = vd.id_venda
            JOIN tb_produtoprojetopedagogico AS ppp ON ppp.id_produto = vdp.id_produto
            JOIN tb_projetopedagogico AS pp ON pp.id_projetopedagogico = ppp.id_projetopedagogico
--

--select TOP(1) viu.id_contrato, vp.id_produto, ctr.id_contratomodelo, ts.*
--from vw_gerarcontrato AS viu
--JOIN dbo.tb_venda AS vd ON vd.id_venda = viu.id_venda AND viu.id_venda IN (103855, 103915)
--JOIN dbo.tb_vendaproduto AS vp ON vp.id_venda = vd.id_venda
--JOIN dbo.tb_produtoprojetopedagogico AS ppp ON ppp.id_produto = vp.id_produto
--JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = ppp.id_projetopedagogico
            JOIN tb_contratoregra AS ctr ON ctr.id_contratoregra = ct.id_contratoregra
            JOIN dbo.tb_textosistema AS ts ON ts.id_textosistema = ctr.id_contratomodelo

--
            JOIN tb_entidade AS et ON et.id_entidade = ct.id_entidade
            LEFT JOIN tb_entidaderesponsavellegal AS etrd ON etrd.id_entidade = et.id_entidade
                                                             AND etrd.bl_padrao = 1
                                                             AND etrd.id_tipoentidaderesponsavel = 1
            LEFT JOIN tb_usuario AS usd ON usd.id_usuario = etrd.id_usuario
            LEFT JOIN tb_entidaderesponsavellegal AS etrs ON etrs.id_entidade = et.id_entidade
                                                         AND etrs.bl_padrao = 1
                                                             AND etrs.id_tipoentidaderesponsavel = 2
            LEFT JOIN tb_usuario AS uss ON uss.id_usuario = etrd.id_usuario
            LEFT JOIN tb_entidadeendereco AS ete ON ete.id_entidade = et.id_entidade
                                                    AND ete.bl_padrao = 1
            LEFT JOIN tb_endereco AS etee ON etee.id_endereco = ete.id_endereco
            LEFT JOIN tb_contatostelefoneentidade AS eTel ON etel.id_entidade = et.id_entidade
            LEFT JOIN tb_contatostelefone AS eCtel ON eCtel.id_telefone = etel.id_telefone
            JOIN vw_pessoa AS ps ON ps.id_usuario = ct.id_usuario
                                    AND ps.id_entidade = ct.id_entidade
            left join tb_campanhacomercial as cpp on cpp.id_campanhacomercial = vd.id_campanhapontualidade and cpp.bl_ativo = 1
            OUTER APPLY ( SELECT TOP 1
                                    psri.id_usuario ,
                                    psri.st_login ,
                                    psri.st_loginentidade ,
                                    psri.st_nomecompleto ,
                                    psri.st_cpf ,
                                    psri.id_registropessoa ,
                                    psri.st_senha ,
                                    psri.st_senhaentidade ,
                                    psri.id_entidade ,
                                    psri.st_sexo ,
                                    psri.dt_nascimento ,
                                    psri.sg_ufnascimento ,
                                    psri.id_municipionascimento ,
                                    psri.st_cidadenascimento ,
                                    psri.id_estadocivil ,
                                    psri.st_estadocivil ,
                                    psri.id_email ,
                                    psri.st_email ,
                                    psri.bl_emailpadrao ,
                                    psri.st_nomemae ,
                                    psri.st_nomepai ,
                                    psri.id_telefone ,
                                    psri.bl_telefonepadrao ,
                                    psri.nu_telefone ,
                                    psri.id_tipotelefone ,
                                    psri.st_tipotelefone ,
                                    psri.nu_ddd ,
                                    psri.nu_ddi ,
                                    psri.id_telefonealternativo ,
                                    psri.nu_telefonealternativo ,
                                    psri.id_tipotelefonealternativo ,
                                    psri.st_tipotelefonealternativo ,
                                    psri.nu_dddalternativo ,
                                    psri.nu_ddialternativo ,
                                    psri.st_rg ,
                                    psri.st_orgaoexpeditor ,
                                    psri.dt_dataexpedicao ,
                                    psri.id_endereco ,
                                    psri.bl_enderecopadrao ,
                                    psri.st_endereco ,
                                    psri.st_cep ,
                                    psri.st_bairro ,
                                    psri.st_complemento ,
                                    psri.nu_numero ,
                                    psri.st_estadoprovincia ,
                                    psri.st_cidade ,
                                    psri.id_pais ,
                                    psri.id_municipio ,
                                    psri.id_tipoendereco ,
                                    psri.sg_uf ,
                                    psri.st_nomepais ,
                                    psri.st_nomemunicipio
                          FROM      dbo.vw_lancamento AS lc
                                    JOIN vw_pessoa AS psri ON psri.id_usuario = lc.id_usuariolancamento
                                                              AND psri.id_entidade = ct.id_entidade
                 WHERE     lc.id_venda = vd.id_venda
                        ) AS psr

            LEFT JOIN dbo.tb_pa_marcacaoetapa AS pa_m ON pa_m.id_venda = vd.id_venda

            GO
            ALTER VIEW [dbo].[vw_historicoagendamento] AS (

              SELECT
                aa.id_avaliacaoagendamento,
                aa.id_matricula,
                aa.id_tipodeavaliacao,
                (CASE
                  WHEN aa.id_tipodeavaliacao = 0 THEN 'Prova Final'
                  ELSE 'Recuperação' + ' ' + CAST(aa.id_tipodeavaliacao AS CHAR)
                END) AS st_tipodeavaliacao,
                t.id_tramite,
                t.st_tramite,
                s.id_situacao,
                s.st_situacao,
                t.bl_visivel,
                t.dt_cadastro,
                t.id_entidade,
                aa.bl_automatico,
                t.id_usuario,
                (CASE
                  WHEN aa.bl_automatico = 0 THEN us.st_nomecompleto
                  ELSE 'Agendamento automático'
                END) AS st_nomecompleto,
                YEAR(aa.dt_cadastro) AS id_ano

                FROM tb_avaliacaoagendamento AS aa
                JOIN tb_tramiteagendamento AS ta ON aa.id_avaliacaoagendamento = ta.id_avaliacaoagendamento
                JOIN tb_tramite AS t ON ta.id_tramite = t.id_tramite AND t.id_tipotramite =  14
                JOIN tb_tipotramite AS tt ON tt.id_tipotramite = t.id_tipotramite AND tt.id_categoriatramite = 5
                JOIN tb_situacao AS s ON ta.id_situacao = s.id_situacao
                JOIN tb_usuario AS us ON t.id_usuario = us.id_usuario

            )
go
ALTER VIEW [dbo].[vw_resumofinanceiro] AS
     SELECT DISTINCT
        l.id_lancamento,
        lv.id_venda,
        lv.bl_entrada,
        l.st_banco,
        l.st_emissor,
        l.dt_prevquitado,
        l.st_coddocumento,
        ed.st_endereco,
        --ed.st_cidade,
        mu.st_nomemunicipio AS st_cidade,
        ed.st_bairro,
        ed.st_estadoprovincia,
        ed.st_cep ,
        ed.sg_uf,
        ed.nu_numero,
        ed.id_municipio,
        ed.id_tipoendereco,
        l.bl_quitado,
        l.dt_quitado,
        CAST(l.nu_quitado AS DECIMAL(10,2)) AS nu_quitado,
        st_entradaparcela = CASE lv.bl_entrada WHEN 1 THEN 'Entrada' ELSE 'Parcelas' END,
         lv.nu_ordem AS nu_parcela,
        l.id_meiopagamento,
        mp.st_meiopagamento,
        CASE
        WHEN ISNULL(l.nu_juros,0) > 0 or ISNULL(l.nu_desconto,0) > 0
		THEN ISNULL(l.nu_valor,0) + ISNULL(l.nu_juros,0) - ISNULL(l.nu_desconto,0)
		ELSE l.nu_valor
		END as nu_valor,
        l.nu_vencimento,
        l.dt_vencimento,
		l.st_autorizacao,
		case when l.st_ultimosdigitos IS null then tf.st_ultimosdigitos
		-- case when tf.st_ultimosdigitos  IS null then 'Não informados' else tf.st_ultimosdigitos end
		else l.st_ultimosdigitos end as st_ultimosdigitos,
		-- l.st_ultimosdigitos,
        st_situacao = CASE
                WHEN CAST(l.dt_vencimento AS date) < CAST(GETDATE() AS DATE) AND l.bl_quitado = 0 AND l.id_meiopagamento NOT IN (1,7) AND l.bl_ativo = 1 THEN 'Vencido'
                WHEN CAST(l.dt_vencimento AS DATE) < CAST(GETDATE() AS DATE) AND l.bl_quitado = 0 AND l.id_meiopagamento IN (1,7) AND l.dt_prevquitado   IS NULL AND l.bl_ativo = 1 THEN 'Atrasado'
                WHEN l.id_meiopagamento IN (1,7) AND l.dt_prevquitado IS NOT NULL  AND l.bl_ativo = 1 THEN 'Autorizado'
                WHEN l.bl_quitado = 1  AND l.bl_ativo = 1 THEN 'Pago'
                WHEN l.bl_ativo = 0 THEN 'Cancelado'
                ELSE 'Pendente'
                END,
        --u.id_usuario,
       -- u.st_nomecompleto,

        CASE WHEN u.id_usuario IS NOT NULL THEN u.id_usuario
			 WHEN el.id_entidade IS NOT NULL THEN el.id_entidade END AS id_usuario,

		CASE WHEN u.st_nomecompleto IS NOT NULL THEN u.st_nomecompleto
			 WHEN el.st_nomeentidade IS NOT NULL THEN el.st_nomeentidade COLLATE DATABASE_DEFAULT END AS st_nomecompleto,

        l.id_entidade,
        ef.id_textosistemarecibo,
        ef.id_reciboconsolidado,
        l.st_nossonumero,
        CASE WHEN tf.st_codtransacaooperadora IS NOT NULL THEN tf.st_codtransacaooperadora
			 WHEN l.st_coddocumento IS NOT NULL THEN l.st_coddocumento END AS st_codtransacaooperadora,
        l.bl_original,
        l.bl_ativo,
        l.dt_atualizado,
        l.st_numcheque,
		l.nu_juros,
		l.nu_desconto,
		l.nu_jurosboleto,
		l.nu_descontoboleto,
		l.id_acordo,
		v.recorrente_orderid,
		v.dt_cadastro as dt_venda,
		si.st_situacao AS st_situacaopedido,
		CASE WHEN si3.st_situacao IS NOT NULL THEN si3.st_situacao
			 WHEN l.bl_quitado = 0 THEN 'PENDENTE'
			 WHEN l.bl_quitado = 1 THEN 'PAGA' END AS st_situacaolancamento,

		CASE WHEN sl.id_venda IS NOT NULL THEN 1 ELSE 0 END AS bl_cartaovencendo,
		l.bl_chequedevolvido,
		CASE WHEN rec.bl_recorrente > 0 THEN 1 ELSE 0 END AS bl_recorrente
    FROM tb_lancamento l
        INNER JOIN tb_lancamentovenda AS lv ON lv.id_lancamento = l.id_lancamento
        INNER JOIN tb_meiopagamento AS mp ON mp.id_meiopagamento = l.id_meiopagamento
        INNER JOIN tb_venda AS v ON v.id_venda = lv.id_venda
        --INNER JOIN tb_contrato as c ON c.id_venda = v.id_venda
        INNER JOIN tb_vendaproduto AS vp ON vp.id_venda = v.id_venda
        INNER JOIN tb_produto AS p ON p.id_produto = vp.id_produto
        --LEFT JOIN tb_usuario AS u ON u.id_usuario = l.id_usuariolancamento
        --OUTER APPLY (SELECT TOP 1 * FROM  tb_pessoaendereco where id_usuario = u.id_usuario AND id_entidade = l.id_entidade AND bl_padrao = 1) AS pe
     --LEFT JOIN tb_endereco AS ed ON ed.id_endereco = pe.id_endereco
        LEFT JOIN tb_entidadefinanceiro ef ON ef.id_entidade = v.id_entidade
        LEFT JOIN tb_transacaolancamento AS tl ON l.id_lancamento = tl.id_lancamento
        LEFT JOIN tb_transacaofinanceira AS tf ON tl.id_transacaofinanceira = tf.id_transacaofinanceira
        LEFT JOIN tb_pedidointegracao AS pint ON pint.id_venda = v.id_venda
        LEFT JOIN tb_situacaointegracao AS si ON si.id_situacaointegracao = pint.id_situacaointegracao

        OUTER APPLY ( SELECT TOP 1 si2.st_situacao
							FROM tb_transacaolancamento AS tl
							INNER JOIN tb_transacaofinanceira AS tf ON tl.id_transacaofinanceira = tf.id_transacaofinanceira
							INNER JOIN tb_situacaointegracao AS si2 ON si2.id_situacaointegracao = tf.id_situacaointegracao
							WHERE tl.id_lancamento = L.id_lancamento
							ORDER BY tf.dt_cadastro DESC
					) AS si3

        OUTER APPLY (
		   SELECT st_nomecompleto, us.id_usuario , pe.id_endereco
				FROM tb_usuario us
				LEFT JOIN tb_pessoaendereco pe ON pe.id_usuario = us.id_usuario AND bl_padrao = 1 AND pe.id_entidade = v.id_entidade
				WHERE us.id_usuario = id_usuariolancamento
		) AS u

		OUTER APPLY (
		   SELECT TOP 1 st_nomeentidade, e.id_entidade , ee.id_endereco
		   FROM tb_entidade e
		   JOIN tb_entidadeendereco ee ON e.id_entidade = ee.id_entidade
		   WHERE e.id_entidade = id_entidadelancamento
		   --AND ee.id_entidade = v.id_entidade
		   --AND bl_padrao = 1
		) AS el

		OUTER APPLY(
			SELECT DISTINCT vv.id_venda
				FROM tb_venda vv
					INNER JOIN tb_pedidointegracao pit ON pit.id_venda = vv.id_venda
					INNER JOIN tb_lancamentovenda lvs ON lvs.id_venda = vv.id_venda
					INNER JOIN tb_lancamento ls ON ls.id_lancamento = lvs.id_lancamento
				WHERE vv.id_venda = v.id_venda
					AND pit.id_sistema = 10
					AND CAST(ls.dt_vencimento AS DATE) >= CAST(pit.dt_vencimentocartao AS DATE)
					AND DATEDIFF(DAY, GETDATE(), pit.dt_vencimentocartao) <= 30
		) sl

		OUTER APPLY(
			SELECT COUNT(id_meiopagamento) as bl_recorrente
				FROM tb_lancamento ll
				INNER JOIN tb_lancamentovenda lvv ON ll.id_lancamento = lvv.id_lancamento
				WHERE lvv.id_venda = v.id_venda
				AND ll.id_meiopagamento = 11
				AND ll.bl_quitado = 0
				AND ll.bl_ativo = 1
				--AND ll.dt_vencimento > CAST(GETDATE() AS DATE)
		) rec
		OUTER APPLY (
								SELECT top 1
					ed.st_bairro,
					ed.st_endereco,
					ed.st_estadoprovincia,
					ed.st_cep ,
					ed.sg_uf,
					ed.nu_numero,
					ed.id_tipoendereco,
					ed.id_municipio
				FROM tb_endereco AS ed
					where ed.id_endereco = CASE WHEN u.id_endereco IS NOT NULL THEN u.id_endereco WHEN el.id_endereco IS NOT NULL THEN el.id_endereco END
				) as ed
		LEFT JOIN tb_municipio AS mu ON ed.id_municipio = mu.id_municipio
GO
alter VIEW vw_avaliacaoprimeiroagendamento AS (

  SELECT DISTINCT
    ma.id_matricula,
    ma.id_alocacao,
    ma.id_matriculadisciplina,
    ma.id_usuario,
    ma.st_nomecompleto,
    ma.id_entidadeatendimento,
    ma.st_nomeentidade,
    av.id_avaliacao,
    av.id_tipoavaliacao,
    av.st_avaliacao,
    ma.id_disciplina,
    ma.st_disciplina,
    av.id_avaliacaoconjuntoreferencia

  -- Buscar somente de alunos que possuem alocacao ativa
  FROM vw_matriculaalocacao AS ma

  JOIN tb_matricula AS m
    ON m.id_matricula = ma.id_matricula
    AND m.bl_ativo = 1
    AND m.id_evolucao = 6

  -- Buscar avaliacoes do aluno
  JOIN vw_avaliacaoaluno AS av
    ON av.id_matricula = ma.id_matricula
    AND av.id_disciplina = ma.id_disciplina
    AND av.id_avaliacaorecupera = 0
    AND av.bl_recuperacao = 0
    AND av.id_tipoavaliacao IN (1,4)
    AND av.id_evolucaodisciplina IN (13, 19)
    AND av.nu_cargahoraria != 0

  -- Somente alunos que estao na lista de APTOS
  JOIN vw_alunosagendamento AS aa ON aa.id_matricula = ma.id_matricula

  -- Procurar o agendamento do aluno em tal avaliacao/disciplina
  LEFT JOIN vw_avaliacaoagendamento AS ag
    ON ag.id_matricula = ma.id_matricula
    AND ag.id_disciplina = ma.id_disciplina
    AND ag.id_avaliacao = av.id_avaliacao

  WHERE
  -- Aluno nao pode ter nenhum agendamento desta disciplina
  ag.id_avaliacaoagendamento IS NULL
  -- A situacao do agendamento tem que ser APTO
  AND ma.id_situacaoagendamento = 120

)
go
alter VIEW [dbo].[vw_avaliacaoagendamento] AS
SELECT DISTINCT
	aa.id_avaliacaoagendamento,
	aa.id_matricula,
	us.id_usuario,
	us.st_nomecompleto,
	us.st_cpf,
	av.id_avaliacao,
	av.st_avaliacao,
	ac.id_tipoprova,
	tp.st_tipoprova,
	aa.dt_agendamento,
	aa.bl_automatico,
	aa.id_situacao,
	st.st_situacao,
	aa.id_avaliacaoaplicacao,
	aa.id_entidade,
	et.st_nomeentidade,
	ap.id_horarioaula,
	ha.st_horarioaula,
	mt.id_projetopedagogico,
	pp.st_projetopedagogico,
	aai.st_codsistema,
	aa.bl_ativo,
	ap.st_endereco,
	ap.dt_aplicacao,
	ap.id_aplicadorprova,
	ap.st_aplicadorprova,
	ap.st_cidade,
	aa.nu_presenca,
	aa.id_usuariocadastro,
	aa.id_usuariolancamento,
	usu_lancamento.st_nomecompleto AS st_usuariolancamento,
	(CASE
		WHEN aa.id_tipodeavaliacao IS NULL THEN 0
		ELSE aa.id_tipodeavaliacao
	END) AS id_tipodeavaliacao,
	(CASE
		WHEN (ap.dt_aplicacao IS NOT NULL AND
			ap.dt_aplicacao > GETDATE() AND
			aa.bl_ativo = 1 AND
			aa.nu_presenca IS NULL) THEN 0
		WHEN (ap.dt_aplicacao IS NOT NULL AND
			ap.dt_aplicacao > GETDATE() AND
			aa.bl_ativo = 0 AND
			aa.id_situacao = 68 AND
			aa.nu_presenca IS NOT NULL) THEN 1
		WHEN (ap.dt_aplicacao IS NOT NULL AND
			ap.dt_aplicacao < GETDATE() AND
			aa.bl_ativo = 1 AND
			aa.id_situacao = 68 AND
			aa.nu_presenca IS NULL) THEN 2
		ELSE 3
	END) AS id_situacaopresenca,
	(CASE
		WHEN (ap.dt_aplicacao IS NOT NULL AND
			ap.dt_aplicacao > GETDATE() AND
			aa.bl_ativo = 1 AND
			aa.nu_presenca IS NULL) THEN 'Aguardando aplicação de prova'
		WHEN (ap.dt_aplicacao IS NOT NULL AND
			ap.dt_aplicacao > GETDATE() AND
			aa.bl_ativo = 0 AND
			aa.id_situacao = 68 AND
			aa.nu_presenca IS NOT NULL) THEN 'Presença lançada'
		WHEN (ap.dt_aplicacao IS NOT NULL AND
			ap.dt_aplicacao < GETDATE() AND
			aa.bl_ativo = 1 AND
			aa.id_situacao = 68 AND
			aa.nu_presenca IS NULL) THEN 'Aplicação realizada e presença não lançada'
		ELSE 'Situação Indefinida'
	END) AS st_situacaopresenca,
	tb_chamada.id_aval AS id_chamada,
	primeira.id_primeirachamada,
	primeirapresenca.nu_primeirapresenca,
	primeira.id_segundachamada AS id_ultimachamada,
	ultima.nu_ultimapresenca,
	mt.id_evolucao,
	aa.bl_provaglobal,
	ap.dt_antecedenciaminima,
	ap.dt_alteracaolimite,

	(CASE
		WHEN aa.id_avaliacaoaplicacao IS NULL OR
			((CAST(GETDATE() AS DATE)) BETWEEN ap.dt_antecedenciaminima AND ap.dt_alteracaolimite AND
			aa.id_avaliacaoaplicacao = ap.id_avaliacaoaplicacao AND
			aa.bl_ativo = 1) THEN 0
		ELSE 1
	END) AS id_mensagens,

	(CASE
		WHEN (CAST(GETDATE() AS DATE)) < ap.dt_antecedenciaminima THEN 'A data para o agendamento ainda não chegou'
		WHEN (CAST(GETDATE() AS DATE)) > ap.dt_alteracaolimite THEN 'Agendamento passou do prazo limite para alteração'
		WHEN aa.id_avaliacaoaplicacao IS NULL THEN ''
		ELSE 'Agendamento dentro do prazo limite para alteração'
	END) AS st_mensagens,

	provasrealizadas.id_provasrealizadas,
	ISNULL(temnotafinal.bl_temnotafinal, 0) AS bl_temnotafinal,
	pe.st_email,
	pe.nu_ddi,
	pe.nu_ddd,
	pe.nu_telefone,
	pe.nu_ddialternativo,
	pe.nu_dddalternativo,
	pe.nu_telefonealternativo,
	ha.hr_inicio,
	ha.hr_fim,
	ap.sg_uf,
	ap.st_complemento,
	ap.nu_numero,
	ap.st_bairro,
	ap.st_telefone AS 'st_telefoneaplicador',
	NULL AS id_disciplina

FROM tb_avaliacaoagendamento aa
JOIN tb_matricula AS mt
	ON mt.id_matricula = aa.id_matricula
JOIN tb_entidade AS ett
	ON ett.id_entidade = mt.id_entidadeatendimento
JOIN tb_usuario AS us
	ON us.id_usuario = mt.id_usuario
JOIN tb_avaliacao AS av
	ON av.id_avaliacao = aa.id_avaliacao
INNER JOIN tb_avaliacaoconjuntorelacao AS acr
	ON acr.id_avaliacao = av.id_avaliacao
INNER JOIN tb_avaliacaoconjunto AS ac
	ON ac.id_avaliacaoconjunto = acr.id_avaliacaoconjunto
JOIN tb_tipoprova AS tp
	ON tp.id_tipoprova = ac.id_tipoprova
INNER JOIN tb_situacao AS st
	ON st.id_situacao = aa.id_situacao
JOIN tb_projetopedagogico AS pp
	ON pp.id_projetopedagogico = mt.id_projetopedagogico
JOIN vw_pessoa AS pe
	ON pe.id_usuario = us.id_usuario
	AND pe.id_entidade = mt.id_entidadematricula
JOIN tb_esquemaconfiguracao AS ec
	ON ett.id_esquemaconfiguracao = ec.id_esquemaconfiguracao
JOIN tb_esquemaconfiguracaoitem AS eci
	ON eci.id_esquemaconfiguracao = ec.id_esquemaconfiguracao
	AND eci.id_itemconfiguracao = 24
	AND eci.st_valor = 0
LEFT JOIN vw_avaliacaoaplicacao AS ap
	ON ap.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao
	AND ap.bl_ativo = 1
	AND ap.id_entidade = aa.id_entidade
LEFT JOIN tb_entidade AS et
	ON et.id_entidade = ap.id_entidade
LEFT JOIN tb_horarioaula AS ha
	ON ha.id_horarioaula = ap.id_horarioaula
LEFT JOIN tb_avalagendaintegracao AS aai
	ON aai.id_avaliacaoagendamento = aa.id_avaliacaoagendamento
OUTER APPLY (SELECT
	st_nomecompleto
FROM tb_usuario
WHERE id_usuario = aa.id_usuariolancamento) AS usu_lancamento
OUTER APPLY (SELECT
	COUNT(cha.id_avaliacaoagendamento) AS id_aval
FROM tb_avaliacaoagendamento AS cha
LEFT JOIN tb_avaliacaoaplicacao AS ch
	ON ch.id_avaliacaoaplicacao = cha.id_avaliacaoaplicacao
WHERE cha.id_matricula = mt.id_matricula
AND cha.id_tipodeavaliacao = aa.id_tipodeavaliacao
AND cha.id_situacao = 68) AS tb_chamada
OUTER APPLY (SELECT
	MIN(id_avaliacaoagendamento) AS id_primeirachamada,
	MAX(id_avaliacaoagendamento) AS id_segundachamada
FROM tb_avaliacaoagendamento
WHERE id_matricula = aa.id_matricula
AND id_situacao = 68
AND id_tipodeavaliacao = aa.id_tipodeavaliacao) AS primeira
OUTER APPLY (SELECT
	nu_presenca AS nu_primeirapresenca
FROM tb_avaliacaoagendamento
WHERE id_avaliacaoagendamento = (SELECT
	MIN(id_avaliacaoagendamento)
FROM tb_avaliacaoagendamento
WHERE id_matricula = aa.id_matricula
AND id_situacao = 68
AND id_tipodeavaliacao = aa.id_tipodeavaliacao)) AS primeirapresenca
OUTER APPLY (SELECT
	nu_presenca AS nu_ultimapresenca
FROM tb_avaliacaoagendamento
WHERE id_avaliacaoagendamento = (SELECT
	MAX(id_avaliacaoagendamento)
FROM tb_avaliacaoagendamento
WHERE id_matricula = aa.id_matricula
AND id_situacao = 68
AND id_tipodeavaliacao = aa.id_tipodeavaliacao)) AS ultima
OUTER APPLY (SELECT
	COUNT(id_avaliacaoagendamento) AS id_provasrealizadas
FROM tb_avaliacaoagendamento
WHERE id_matricula = aa.id_matricula
AND nu_presenca = 1
AND bl_ativo = 0
AND id_situacao = 68) AS provasrealizadas
OUTER APPLY (SELECT
	(CASE
		WHEN st_nota IS NULL THEN 0
		ELSE 1
	END) AS bl_temnotafinal
FROM tb_avaliacaoaluno
WHERE id_matricula = mt.id_matricula
AND id_avaliacao = aa.id_avaliacao
AND id_situacao = 86) AS temnotafinal
WHERE aa.id_situacao <> 70
AND ac.id_tipoprova = 2 UNION SELECT DISTINCT
	aa.id_avaliacaoagendamento,
	aa.id_matricula,
	us.id_usuario,
	us.st_nomecompleto,
	us.st_cpf,
	av.id_avaliacao,
	av.st_avaliacao,
	ac.id_tipoprova,
	tp.st_tipoprova,
	aa.dt_agendamento,
	aa.bl_automatico,
	aa.id_situacao,
	st.st_situacao,
	aa.id_avaliacaoaplicacao,
	aa.id_entidade,
	et.st_nomeentidade,
	ap.id_horarioaula,
	ha.st_horarioaula,
	mt.id_projetopedagogico,
	pp.st_projetopedagogico,
	aai.st_codsistema,
	aa.bl_ativo,
	ap.st_endereco,
	ap.dt_aplicacao,
	ap.id_aplicadorprova,
	ap.st_aplicadorprova,
	ap.st_cidade,
	aa.nu_presenca,
	aa.id_usuariocadastro,
	aa.id_usuariolancamento,
	usu_lancamento.st_nomecompleto AS st_usuariolancamento,
	(CASE
		WHEN aa.id_tipodeavaliacao IS NULL THEN 0
		ELSE aa.id_tipodeavaliacao
	END) AS id_tipodeavaliacao,
	(CASE
		WHEN (ap.dt_aplicacao IS NOT NULL AND
			ap.dt_aplicacao > GETDATE() AND
			aa.bl_ativo = 1 AND
			aa.nu_presenca IS NULL) THEN 0
		WHEN (ap.dt_aplicacao IS NOT NULL AND
			ap.dt_aplicacao > GETDATE() AND
			aa.bl_ativo = 0 AND
			aa.id_situacao = 68 AND
			aa.nu_presenca IS NOT NULL) THEN 1
		WHEN (ap.dt_aplicacao IS NOT NULL AND
			ap.dt_aplicacao < GETDATE() AND
			aa.bl_ativo = 1 AND
			aa.id_situacao = 68 AND
			aa.nu_presenca IS NULL) THEN 2
		ELSE 3
	END) AS id_situacaopresenca,
	(CASE
		WHEN (ap.dt_aplicacao IS NOT NULL AND
			ap.dt_aplicacao > GETDATE() AND
			aa.bl_ativo = 1 AND
			aa.nu_presenca IS NULL) THEN 'Aguardando aplicação de prova'
		WHEN (ap.dt_aplicacao IS NOT NULL AND
			ap.dt_aplicacao > GETDATE() AND
			aa.bl_ativo = 0 AND
			aa.id_situacao = 68 AND
			aa.nu_presenca IS NOT NULL) THEN 'Presença lançada'
		WHEN (ap.dt_aplicacao IS NOT NULL AND
			ap.dt_aplicacao < GETDATE() AND
			aa.bl_ativo = 1 AND
			aa.id_situacao = 68 AND
			aa.nu_presenca IS NULL) THEN 'Aplicação realizada e presença não lançada'
		ELSE 'Situação Indefinida'
	END) AS st_situacaopresenca,
	tb_chamada.id_aval AS id_chamada,
	primeira.id_primeirachamada,
	primeirapresenca.nu_primeirapresenca,
	primeira.id_segundachamada AS id_ultimachamada,
	ultima.nu_ultimapresenca,
	mt.id_evolucao,
	aa.bl_provaglobal,
	ap.dt_antecedenciaminima,
	ap.dt_alteracaolimite,

	(CASE
		WHEN aa.id_avaliacaoaplicacao IS NULL OR
			((CAST(GETDATE() AS DATE)) BETWEEN ap.dt_antecedenciaminima AND ap.dt_alteracaolimite AND
			aa.id_avaliacaoaplicacao = ap.id_avaliacaoaplicacao AND
			aa.bl_ativo = 1) THEN 0
		ELSE 1
	END) AS id_mensagens,

	(CASE
		WHEN (CAST(GETDATE() AS DATE)) < ap.dt_antecedenciaminima THEN 'A data para o agendamento ainda não chegou'
		WHEN (CAST(GETDATE() AS DATE)) > ap.dt_alteracaolimite THEN 'Agendamento passou do prazo limite para alteração'
		WHEN aa.id_avaliacaoaplicacao IS NULL THEN ''
		ELSE 'Agendamento dentro do prazo limite para alteração'
	END) AS st_mensagens,

	provasrealizadas.id_provasrealizadas,
	ISNULL(temnotafinal.bl_temnotafinal, 0) AS bl_temnotafinal,
	pe.st_email,
	pe.nu_ddi,
	pe.nu_ddd,
	pe.nu_telefone,
	pe.nu_ddialternativo,
	pe.nu_dddalternativo,
	pe.nu_telefonealternativo,
	ha.hr_inicio,
	ha.hr_fim,
	ap.sg_uf,
	ap.st_complemento,
	ap.nu_numero,
	ap.st_bairro,
	ap.st_telefone AS 'st_telefoneaplicador',
	md.id_disciplina

FROM tb_avaliacaoagendamento aa
JOIN tb_avalagendamentoref AS agr
	ON aa.id_avaliacaoagendamento = agr.id_avaliacaoagendamento
JOIN tb_avaliacaoconjuntoreferencia AS acrf
	ON acrf.id_avaliacaoconjuntoreferencia = agr.id_avaliacaoconjuntoreferencia
JOIN tb_matricula AS mt
	ON mt.id_matricula = aa.id_matricula
JOIN tb_matriculadisciplina AS md
	ON md.id_matricula = mt.id_matricula
JOIN tb_alocacao AS al
	ON al.id_matriculadisciplina = md.id_matriculadisciplina
	AND al.bl_ativo = 1
	AND al.id_saladeaula = acrf.id_saladeaula
JOIN tb_entidade AS ett
	ON ett.id_entidade = mt.id_entidadeatendimento
JOIN tb_usuario AS us
	ON us.id_usuario = mt.id_usuario
JOIN tb_avaliacao AS av
	ON av.id_avaliacao = aa.id_avaliacao
INNER JOIN tb_avaliacaoconjuntorelacao AS acr
	ON acr.id_avaliacao = av.id_avaliacao
INNER JOIN tb_avaliacaoconjunto AS ac
	ON ac.id_avaliacaoconjunto = acr.id_avaliacaoconjunto
JOIN tb_tipoprova AS tp
	ON tp.id_tipoprova = ac.id_tipoprova
INNER JOIN tb_situacao AS st
	ON st.id_situacao = aa.id_situacao
JOIN tb_projetopedagogico AS pp
	ON pp.id_projetopedagogico = mt.id_projetopedagogico
JOIN vw_pessoa AS pe
	ON pe.id_usuario = us.id_usuario
	AND pe.id_entidade = mt.id_entidadematricula
JOIN tb_esquemaconfiguracao AS ec
	ON ett.id_esquemaconfiguracao = ec.id_esquemaconfiguracao
JOIN tb_esquemaconfiguracaoitem AS eci
	ON eci.id_esquemaconfiguracao = ec.id_esquemaconfiguracao
	AND eci.id_itemconfiguracao = 24
	AND eci.st_valor = 1
LEFT JOIN vw_avaliacaoaplicacao AS ap
	ON ap.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao
	AND ap.bl_ativo = 1
	AND ap.id_entidade = aa.id_entidade
LEFT JOIN tb_entidade AS et
	ON et.id_entidade = ap.id_entidade
LEFT JOIN tb_horarioaula AS ha
	ON ha.id_horarioaula = ap.id_horarioaula
LEFT JOIN tb_avalagendaintegracao AS aai
	ON aai.id_avaliacaoagendamento = aa.id_avaliacaoagendamento
OUTER APPLY (SELECT
	st_nomecompleto
FROM tb_usuario
WHERE id_usuario = aa.id_usuariolancamento) AS usu_lancamento
OUTER APPLY (SELECT
	COUNT(cha.id_avaliacaoagendamento) AS id_aval
FROM tb_avaliacaoagendamento AS cha
JOIN tb_avalagendamentoref AS cagr ON cagr.id_avaliacaoagendamento = cha.id_avaliacaoagendamento and cagr.id_avaliacaoconjuntoreferencia = acrf.id_avaliacaoconjuntoreferencia
LEFT JOIN tb_avaliacaoaplicacao AS ch
	ON ch.id_avaliacaoaplicacao = cha.id_avaliacaoaplicacao
WHERE cha.id_matricula = mt.id_matricula
AND cha.id_tipodeavaliacao = aa.id_tipodeavaliacao
AND cha.id_situacao = 68) AS tb_chamada
OUTER APPLY (SELECT
	MIN(id_avaliacaoagendamento) AS id_primeirachamada,
	MAX(id_avaliacaoagendamento) AS id_segundachamada
FROM tb_avaliacaoagendamento
WHERE id_matricula = aa.id_matricula
AND id_situacao = 68
AND id_tipodeavaliacao = aa.id_tipodeavaliacao) AS primeira
OUTER APPLY (SELECT
	nu_presenca AS nu_primeirapresenca
FROM tb_avaliacaoagendamento
WHERE id_avaliacaoagendamento = (SELECT
	MIN(id_avaliacaoagendamento)
FROM tb_avaliacaoagendamento
WHERE id_matricula = aa.id_matricula
AND id_situacao = 68
AND id_tipodeavaliacao = aa.id_tipodeavaliacao)) AS primeirapresenca
OUTER APPLY (SELECT
	nu_presenca AS nu_ultimapresenca
FROM tb_avaliacaoagendamento
WHERE id_avaliacaoagendamento = (SELECT
	MAX(id_avaliacaoagendamento)
FROM tb_avaliacaoagendamento
WHERE id_matricula = aa.id_matricula
AND id_situacao = 68
AND id_tipodeavaliacao = aa.id_tipodeavaliacao)) AS ultima
OUTER APPLY (SELECT
	COUNT(id_avaliacaoagendamento) AS id_provasrealizadas
FROM tb_avaliacaoagendamento
WHERE id_matricula = aa.id_matricula
AND nu_presenca = 1
AND bl_ativo = 0
AND id_situacao = 68) AS provasrealizadas
OUTER APPLY (SELECT
	(CASE
		WHEN st_nota IS NULL THEN 0
		ELSE 1
	END) AS bl_temnotafinal
FROM tb_avaliacaoaluno
WHERE id_matricula = mt.id_matricula
AND id_avaliacao = aa.id_avaliacao
AND id_situacao = 86) AS temnotafinal
WHERE aa.id_situacao <> 70
AND ac.id_tipoprova = 2

go
ALTER VIEW [dbo].[vw_avaliacaoaplicacao] AS
SELECT
	distinct
	aa.id_avaliacaoaplicacao,
	aa.id_aplicadorprova,
	aa.id_endereco,
	aa.dt_aplicacao,
	YEAR(aa.dt_aplicacao) AS nu_anoaplicacao,
	MONTH(aa.dt_aplicacao) AS nu_mesaplicacao,
	DAY(aa.dt_aplicacao) AS nu_diaaplicacao,
	app.st_aplicadorprova,
	en.sg_uf,
	en.id_municipio,
	en.st_endereco,
	mp.st_nomemunicipio AS st_cidade,
	en.st_complemento,
	en.nu_numero,
	en.st_bairro,
	aa.id_entidade,
	CAST('('+ct.nu_ddd+')'+ct.nu_telefone AS VARCHAR) AS st_telefone,
	aa.id_horarioaula,
	aa.id_usuariocadastro,
	aa.dt_alteracaolimite,
	aa.dt_antecedenciaminima,
	aa.bl_unica,
	aa.dt_cadastro,
	aa.bl_ativo,
	hr.hr_inicio AS hr_inicioprova,
	hr.hr_fim AS hr_fimprova,
	hr.st_horarioaula,
	CAST(GETDATE() AS DATE) AS data,
	(CASE WHEN (CAST(GETDATE() AS DATE)) BETWEEN aa.dt_antecedenciaminima AND aa.dt_alteracaolimite THEN 1 ELSE 0 END) AS bl_dataativa,
	(CASE WHEN aa.nu_maxaplicacao <= (SELECT COUNT(id_matricula) FROM tb_avaliacaoagendamento WHERE id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao and bl_ativo=1) THEN 1 ELSE 0 END) AS bl_limitealunos,
	-- Numero total de vagas
	aa.nu_maxaplicacao,
	-- Vagas restantes
	(aa.nu_maxaplicacao - (SELECT COUNT(id_avaliacaoagendamento) FROM tb_avaliacaoagendamento saa WHERE saa.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao AND bl_ativo = 1 AND saa.id_situacao IN(68,69))) AS nu_vagasrestantes
FROM dbo.tb_avaliacaoaplicacao AS aa
	INNER JOIN dbo.tb_endereco AS en ON aa.id_endereco = en.id_endereco
	LEFT JOIN tb_municipio AS mp on mp.id_municipio = en.id_municipio
	INNER JOIN dbo.tb_aplicadorprova AS app ON aa.id_aplicadorprova = app.id_aplicadorprova
	INNER JOIN tb_aplicadorprovaentidade AS ape on ape.id_aplicadorprova = aa.id_aplicadorprova
	INNER JOIN tb_horarioaula AS hr on hr.id_horarioaula = aa.id_horarioaula
	LEFT JOIN dbo.tb_contatostelefonepessoa AS ctp ON ctp.id_usuario = id_usuarioaplicador AND ctp.id_entidade = id_entidadecadastro AND ctp.bl_padrao = 1
	LEFT JOIN dbo.tb_contatostelefoneentidade AS cte ON cte.id_entidade = id_entidadeaplicador AND cte.bl_padrao = 1
	LEFT JOIN dbo.tb_contatostelefone AS ct ON ct.id_telefone = cte.id_telefone OR ct.id_telefone = ctp.id_telefone

--DESATIVAR IDENTITY TABELA
SET IDENTITY_INSERT tb_funcionalidade ON

INSERT INTO tb_funcionalidade
(id_funcionalidade, st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao,
st_classeflex, st_urlicone, id_tipofuncionalidade, nu_ordem, st_classeflexbotao, bl_pesquisa,
 bl_lista, id_sistema, st_ajuda, st_target, st_urlcaminho, bl_visivel, bl_relatorio,
st_urlcaminhoedicao, bl_delete)
VALUES
(762, 'Alunos Renovados', 257, 1, 123,
'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa', null, 3, 0, null, 0,
0,1, null, null, 762, 1, 1, null, 0)

--ATIVAR IDENTITY TABELA
SET IDENTITY_INSERT tb_funcionalidade OFF
go
insert into tb_textovariaveis
(id_textocategoria, st_camposubstituir, st_textovariaveis, id_tipotextovariavel)
values (4,'st_areaconhecimento','#area_conhecimento#',1)
go
-- GII-6472
-- Evolucao
SET IDENTITY_INSERT tb_evolucao ON;
INSERT INTO tb_evolucao (id_evolucao, st_evolucao, st_tabela, st_campo) VALUES (69, 'Decurso de Prazo', 'tb_matricula', 'id_evolucao');
SET IDENTITY_INSERT tb_evolucao OFF;
go
-- Permissao
SET IDENTITY_INSERT tb_permissao ON;
INSERT INTO tb_permissao (id_permissao, st_nomepermissao, st_descricao, bl_ativo) VALUES (37, 'Decurso de Prazo', 'Permitir alterar evolução da matrícula para Decurso de Prazo', 1);
SET IDENTITY_INSERT tb_permissao OFF;
go
-- Permissao / Funcionalidade
-- Chave composta
INSERT INTO tb_permissaofuncionalidade (id_funcionalidade, id_permissao) VALUES (701, 37);
go
SET IDENTITY_INSERT tb_permissao ON
go
INSERT tb_permissao (id_permissao, st_nomepermissao, st_descricao, bl_ativo) VALUES(33, 'Alterar Atendente', 'Permitir alterar o atendente da negociação - Graduação', 1);
go
SET IDENTITY_INSERT tb_permissao OFF
go
INSERT tb_permissaofuncionalidade (id_funcionalidade, id_permissao) VALUES (760, 33)
go
UPDATE tb_textovariaveis SET st_textovariaveis = '#cpf_cnpj_responsavel_financeiro#', st_mascara = 'setaMascaraCpfCnpj'
WHERE id_textovariaveis = 385
go
insert into tb_textovariaveis
(id_textocategoria, st_camposubstituir, st_textovariaveis, id_tipotextovariavel)
values (4,'st_areaconhecimento','#area_conhecimento#',1)
go
--incremento
--DESATIVAR IDENTITY TABELA
SET IDENTITY_INSERT tb_funcionalidade ON --usar essa linha só em produção

INSERT INTO tb_funcionalidade
  (id_funcionalidade,  st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao,
  st_classeflex, st_urlicone, id_tipofuncionalidade, nu_ordem, st_classeflexbotao, bl_pesquisa,
   bl_lista, id_sistema, st_ajuda, st_target, st_urlcaminho, bl_visivel, bl_relatorio,
  st_urlcaminhoedicao, bl_delete)
VALUES
  (763, 'Consulta de Disciplina', 257, 1, 123,
   'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa',
   null, 3, 0, null, 0, 0, 1, null, null, '/relatorio-consulta-disciplina', 1, 0, null, 0)

--ATIVAR IDENTITY TABELA
SET IDENTITY_INSERT tb_funcionalidade OFF --usar essa linha só em produção
go
--altera 1 row
  UPDATE tb_formaingresso
  SET st_formaingresso = 'PDS'
  WHERE id_formaingresso = 4 --ID em Homologação e Produção
go
SET IDENTITY_INSERT tb_sistema ON

INSERT INTO tb_sistema (id_sistema, st_sistema, bl_ativo, st_conexao, st_chaveacesso) VALUES(29, 'Fluxus - Integração Venda', 1, NULL, NULL)

--ATIVAR IDENTITY TABELA
SET IDENTITY_INSERT tb_sistema OFF

CREATE TABLE tb_formapagamentointegracao (
	id_formapagamentointegracao INT PRIMARY KEY,
	id_sistema INT ,
	id_usuariocadastro INT,
	id_formapagamento INT,
	id_entidade INT,
	st_codsistema VARCHAR(2),
	st_codchave VARCHAR(5)
)

CREATE TABLE tb_vendaprodutointegracao (
	id_vendaprodutointegracao INT PRIMARY KEY,
	id_sistema INT,
	id_entidade INT,
	id_usuariocadastro INT,
	id_venda INT,
	id_produto INT,
	st_codvenda VARCHAR(20),
	st_codigoprodutoexterno VARCHAR(20),
	dt_cadastro DATETIME,
	dt_sincronizado DATETIME
)

ALTER TABLE tb_produto ADD st_codigoprodutoexterno  VARCHAR(20)
ALTER TABLE tb_vendaintegracao ADD st_codvenda  VARCHAR(20)
ALTER TABLE tb_lancamentointegracao ADD st_codvenda  VARCHAR(20)
ALTER TABLE tb_vendaintegracao ADD st_codresponsavel VARCHAR(20)
ALTER TABLE tb_entidadeintegracao ADD st_codcpg VARCHAR(20)
go


INSERT INTO tb_textovariaveis
(id_textocategoria, st_camposubstituir,
st_textovariaveis, st_mascara, id_tipotextovariavel)
VALUES
(2, 'id_venda','#planopagamento_graduacao#', 'gerarGridPlanoPagamentoGraduacao',2)

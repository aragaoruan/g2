/* Criando vw específica para o relatório de alunos aptos. */
create VIEW [rel].[vw_alunosaptosagendamento] AS (

SELECT
DISTINCT
mt.id_turma,
mt.id_evolucao,
mt.st_evolucao,
mt.id_projetopedagogico,
mt.dt_inicio,
mt.st_nomecompleto,
mt.st_cpf,
mt.id_matricula,
mt.id_contrato,
mt.st_email,
mt.st_telefone,
mt.sg_uf,
mt.st_situacao,
mt.st_areaconhecimento,
mt.st_projetopedagogico,
mt.st_turma,
mt.dt_inicioturma,
mt.id_situacaoagendamento,
mt.id_entidadeatendimento,
mt.id_situacao,
ava_rec.id_tipodeavaliacao,

CASE
    WHEN ava_rec.id_tipodeavaliacao > 0 THEN 'RECUPERAÇÃO'
    ELSE 'PROVA FINAL'
END AS 'st_avaliacao'

FROM rel.vw_matricula AS mt
LEFT JOIN dbo.vw_avaliacaoagendamento AS avag ON avag.id_matricula = mt.id_matricula AND avag.id_avaliacaoagendamento IN (SELECT TOP 1 id_avaliacaoagendamento FROM dbo.vw_avaliacaoagendamento AS ava WHERE ava.id_matricula = avag.id_matricula ORDER BY id_tipodeavaliacao DESC, id_chamada DESC)
OUTER APPLY (
    SELECT
    CASE
        WHEN nu_presenca IS NULL THEN id_tipodeavaliacao
        ELSE id_tipodeavaliacao + 1
    END AS id_tipodeavaliacao
) AS ava_rec

WHERE mt.id_situacaoagendamento = 120

)
go
alter VIEW [rel].[vw_cronogramasalas] as
SELECT DISTINCT
        sa.id_saladeaula ,
        st_saladeaula ,
        dt_inicioinscricao ,
        dt_fiminscricao ,
        dt_abertura ,
        dt_encerramento ,
        cast(dt_atualiza as date) as dt_atualiza,
        st_disciplina ,
        dc.st_tituloexibicao AS st_tituloexibicaodisciplina ,
        st_projetopedagogico ,
        aps.id_projetopedagogico,
        uss.st_nomecompleto AS st_nomeprofessor ,
        usp.id_usuario AS id_coordenador,
        usp.st_nomecompleto AS st_nomecoordenador,
        si.st_codsistemacurso AS st_codava,
        sa.id_entidade,
		sa.id_categoriasala,
		CAST(sa.dt_cadastro as date) as dt_cadastro,
		cast(COUNT(ta.id_matriculadisciplina) AS varchar) AS st_alunos
        FROM dbo.tb_saladeaula AS sa
JOIN dbo.tb_areaprojetosala AS aps ON aps.id_saladeaula = sa.id_saladeaula
JOIN dbo.tb_disciplinasaladeaula AS da ON da.id_saladeaula = sa.id_saladeaula
JOIN tb_disciplina AS dc ON dc.id_disciplina = da.id_disciplina
JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = aps.id_projetopedagogico
LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS ups ON ups.id_saladeaula = aps.id_saladeaula AND ups.bl_titular = 1 and ups.bl_ativo=1
LEFT JOIN dbo.tb_usuario AS uss ON uss.id_usuario = ups.id_usuario
LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS upp ON upp.id_projetopedagogico = pp .id_projetopedagogico AND upp.bl_titular = 1 and upp.bl_ativo=1 and upp.id_disciplina is null
LEFT JOIN dbo.tb_perfil AS tbp ON tbp.id_perfil = upp.id_perfil AND tbp.id_perfilpedagogico = 2
LEFT JOIN dbo.tb_usuario AS usp ON usp.id_usuario = upp.id_usuario
LEFT JOIN dbo.tb_saladeaulaintegracao AS si ON si.id_saladeaula = sa.id_saladeaula
LEFT JOIN dbo.tb_alocacao AS ta ON ta.id_saladeaula = sa.id_saladeaula AND ta.bl_ativo = 1
WHERE sa.bl_ativa = 1 AND sa.id_situacao != 74
GROUP by
sa.id_saladeaula,
st_saladeaula ,
        dt_inicioinscricao ,
        dt_fiminscricao ,
        dt_abertura ,
        dt_encerramento ,
        dt_atualiza,
        st_disciplina ,
        dc.st_tituloexibicao ,
        st_projetopedagogico ,
        aps.id_projetopedagogico,
        uss.st_nomecompleto ,
        usp.id_usuario ,
        usp.st_nomecompleto,
        si.st_codsistemacurso,
        sa.id_entidade,
		sa.dt_cadastro,
		sa.id_categoriasala
    go
    CREATE VIEW vw_minhapasta
    AS

    SELECT
      mp1.id_minhapasta,
      mp1.id_matricula,
      mp1.id_venda,
      us1.st_nomecompleto,
      mp1.st_contrato,
      mp1.id_usuariocadastro as id_usuario,
      mp1.dt_cadastro as dt_acao,
      ev1.st_evolucao
    FROM tb_minhapasta as mp1
    JOIN tb_usuario as us1 on us1.id_usuario = mp1.id_usuariocadastro
    JOIN tb_evolucao as ev1 on ev1.id_evolucao = 66

    UNION

    SELECT
      mp2.id_minhapasta,
      mp2.id_matricula,
      mp2.id_venda,
      us2.st_nomecompleto,
      mp2.st_contrato,
      mp2.id_usuarioatualizacao as id_usuario,
      mp2.dt_atualizacao as dt_acao,
      ev2.st_evolucao
      FROM tb_minhapasta as mp2
      JOIN tb_usuario as us2 on us2.id_usuario = mp2.id_usuarioatualizacao
      JOIN tb_evolucao as ev2 on ev2.id_evolucao = 67
    where id_usuarioatualizacao is not NULL
    go
    ALTER VIEW [dbo].[vw_matricula]
AS
 SELECT
	mt.id_usuario,
	us.st_nomecompleto,
	us.st_cpf,
	ps.st_urlavatar,
	ps.dt_nascimento,
	ps.st_nomepai,
	ps.st_nomemae,
	ps.st_email,
	CAST(ps.nu_ddd AS VARCHAR) + '-' + CAST(ps.nu_telefone AS VARCHAR) AS st_telefone,
	CAST(ps.nu_dddalternativo AS VARCHAR) + '-'
	+ CAST(ps.nu_telefonealternativo AS VARCHAR) AS st_telefonealternativo,
	mt.id_matricula,
	pp.st_projetopedagogico,
	mt.id_situacao,
	st.st_situacao,
	mt.id_evolucao,
	ev.st_evolucao,
	mt.dt_concluinte,
	mt.id_entidadematriz,
	etmz.st_nomeentidade AS st_entidadematriz,
	mt.id_entidadematricula,
	etm.st_nomeentidade AS st_entidadematricula,
	mt.id_entidadeatendimento,
	eta.st_nomeentidade AS st_entidadeatendimento,
	mt.bl_ativo,
	cm.id_contrato,
	pp.id_projetopedagogico,
	mt.dt_termino,
	mt.dt_cadastro,
	mt.dt_inicio,
	tm.id_turma,
	tm.st_turma,
	tm.st_codigo,
	tm.dt_inicio AS dt_inicioturma,
	mt.bl_institucional,
	tm.dt_fim AS dt_terminoturma,
	mt.id_situacaoagendamento,
	mt.id_vendaproduto,
	mt.dt_termino AS dt_terminomatricula,
	CAST(RIGHT(master.dbo.fn_varbintohexstr(HASHBYTES('MD5',
	(CAST(us.id_usuario AS VARCHAR(10)) COLLATE Latin1_General_CI_AI
	+ CAST((CASE
		WHEN mt.id_matricula IS NULL THEN 0
		ELSE mt.id_matricula
	END) AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
	+ CAST(5 AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
	+ CAST((0) AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
	+ us.st_login
	+ us.st_senha
	+ CONVERT(VARCHAR(10), GETDATE(), 103)))),
	32) AS CHAR(32)) COLLATE Latin1_General_CI_AI AS st_urlacesso,
	mt.bl_documentacao,
	vprod.id_venda,
	eta.st_urlportal,
	mt.st_codcertificacao,
	ps.st_identificacao,
	CONVERT(VARCHAR, mtc.dt_cadastro, 103) AS st_dtcadastrocertificacao,
	CONVERT(VARCHAR, mtc.dt_enviocertificado, 103) AS st_dtenviocertificado,
	CONVERT(VARCHAR, mtc.dt_retornocertificadora, 103) AS st_dtretornocertificadora,
	CONVERT(VARCHAR, mtc.dt_envioaluno, 103) AS st_dtenvioaluno,
	st_codigoacompanhamento,
	mtc.id_indicecertificado,
	mt.id_evolucaocertificacao,
	ap.id_areaconhecimento,
	pp.bl_disciplinacomplementar,
	mt.id_matriculavinculada,
	cr.id_contratoregra,
	trc.id_tiporegracontrato,
	etm.st_siglaentidade,
	mt.bl_academico,
	mtc.dt_cadastro AS dt_cadastrocertificacao,
	mtc.dt_enviocertificado,
	mtc.dt_retornocertificadora,
	mtc.dt_envioaluno,
	vup.st_upload AS st_uploadcontrato,
	canc.id_cancelamento,
	mt.id_aproveitamento,
  vprod.id_produto
FROM tb_matricula mt
JOIN tb_projetopedagogico AS pp
	ON mt.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_areaprojetopedagogico AS ap
	ON ap.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_entidade AS etmz
	ON etmz.id_entidade = mt.id_entidadematriz
JOIN tb_entidade AS etm
	ON etm.id_entidade = mt.id_entidadematricula
JOIN tb_entidade AS eta
	ON eta.id_entidade = mt.id_entidadeatendimento
JOIN tb_situacao AS st
	ON st.id_situacao = mt.id_situacao
JOIN tb_evolucao AS ev
	ON ev.id_evolucao = mt.id_evolucao
JOIN tb_usuario AS us
	ON us.id_usuario = mt.id_usuario
LEFT JOIN tb_contratomatricula AS cm
	ON cm.id_matricula = mt.id_matricula
	AND cm.bl_ativo = 1
LEFT JOIN vw_pessoa AS ps
	ON mt.id_usuario = ps.id_usuario
	AND mt.id_entidadeatendimento = ps.id_entidade
LEFT JOIN dbo.tb_contrato AS ct
	ON ct.id_contrato = cm.id_contrato
	AND ct.bl_ativo = 1
LEFT JOIN tb_turma AS tm
	ON tm.id_turma = mt.id_turma
LEFT JOIN tb_matriculacertificacao mtc
	ON mtc.id_matricula = mt.id_matricula
	AND mtc.bl_ativo = 1
LEFT JOIN dbo.tb_contratoregra AS cr
	ON cr.id_contratoregra = ct.id_contratoregra
LEFT JOIN dbo.tb_tiporegracontrato AS trc
	ON trc.id_tiporegracontrato = cr.id_tiporegracontrato
LEFT JOIN dbo.tb_venda AS v
	ON ct.id_venda = v.id_venda
LEFT JOIN dbo.tb_upload AS vup
	ON vup.id_upload = v.id_uploadcontrato
LEFT JOIN dbo.tb_cancelamento AS canc
	ON canc.id_cancelamento = mt.id_cancelamento
OUTER APPLY (SELECT TOP 1 tb_vendaproduto.* FROM dbo.tb_vendaproduto
	JOIN tb_produtoprojetopedagogico AS ppd ON ppd.id_produto = tb_vendaproduto.id_produto
	JOIN tb_venda AS vend ON vend.id_venda = tb_vendaproduto.id_venda AND vend.id_evolucao = 10
  WHERE tb_vendaproduto.id_matricula = mt.id_matricula
  AND tb_vendaproduto.bl_ativo = 1
  ORDER BY tb_vendaproduto.id_vendaproduto DESC) AS vprod
WHERE mt.bl_ativo = 1
GO
USE [G2_UNY]
GO

/****** Object:  View [rel].[vw_comercialgeral]    Script Date: 7/22/2016 2:49:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





/*Cláusula "cm.bl_ativo = 1" comentada, com o objetivo de pagar a matricula original*/

 ALTER VIEW [rel].[vw_comercialgeral] as
SELECT DISTINCT
	CAST (mt.dt_cadastro AS DATE) AS dt_matricula,
	--app.id_areaconhecimento ,
	sg_uf,
	ve.id_entidade,
	ve.st_nomeentidade,
	ps.st_nomecompleto,
	ps.st_cpf,
	st_cidade,
	st_projetopedagogico,
	(
		SELECT
			COALESCE (
				ac2.st_areaconhecimento + ', ',
				''
			)
		FROM
			dbo.tb_areaconhecimento ac2
		JOIN dbo.tb_areaprojetopedagogico app2 ON ac2.id_areaconhecimento = app2.id_areaconhecimento
		WHERE
			app2.id_projetopedagogico = mt.id_projetopedagogico FOR XML PATH ('')
	) AS st_areaconhecimento,
	(
		SELECT
			COALESCE (
				tc.st_categoria + ', ',
				''
			)
		from tb_categoriaproduto tbcp
LEFT JOIN tb_categoria tc ON tc.id_categoria = tbcp.id_categoria
		WHERE
			tbcp.id_produto = pdt.id_produto FOR XML PATH ('')
	) AS st_categoria,

	--st_areaconhecimento,
	tm.st_turma,
	tm.dt_inicio,
	dt_fim,
	usr.st_nomecompleto AS st_representante,
	usa.st_nomecompleto AS st_atendente,
	mt.id_matricula,
	ev.st_evolucao,
	st.st_situacao,
	vdl.nu_parcelas,
	vdlb.nu_valorboleto,
	vdlcc.nu_valorcartaoc,
	vdlcd.nu_valorcartaod,
	vdlcr.nu_valorcartaor,
	vdlc.nu_valorcheque,
	vdlo.nu_valoroutros,
	pd.nu_valorliquido,
	vdlct.nu_valorcarta,
	vdlbl.nu_valorbolsa,
	vd.id_venda,
	vdlv.dt_vencimento AS dt_vencimentoultima,
	pdc.st_produto AS st_combo,
	pdc.id_produto AS id_produtocombo,
	ve.st_nomeentidade AS st_nomeentidaderec,
	et.id_entidade AS id_entidadepai,
	pdt.id_produto,
	fpa.id_formapagamentoaplicacao,
	fpa.st_formapagamentoaplicacao
FROM
	tb_entidade AS et
JOIN dbo.vw_entidaderecursivaid AS ve ON ve.nu_entidadepai = et.id_entidade
OR ve.id_entidade = et.id_entidade
JOIN dbo.tb_matricula AS mt ON ve.id_entidade = mt.id_entidadematricula and mt.id_evolucao not in (41)
AND mt.bl_ativo = 1
JOIN dbo.tb_evolucao AS ev ON ev.id_evolucao = mt.id_evolucao
JOIN dbo.tb_situacao AS st ON st.id_situacao = mt.id_situacao
JOIN dbo.tb_areaprojetopedagogico AS app ON app.id_projetopedagogico = mt.id_projetopedagogico
JOIN dbo.tb_areaconhecimento AS aa ON aa.id_areaconhecimento = app.id_areaconhecimento -- AND aa.id_tipoareaconhecimento = 1
JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
LEFT JOIN tb_turma AS tm ON tm.id_turma = mt.id_turma
JOIN dbo.vw_pessoa AS ps ON ps.id_usuario = mt.id_usuario
AND ps.id_entidade = ve.id_entidade
JOIN dbo.tb_contratomatricula AS cm ON cm.id_matricula = mt.id_matricula --AND cm.bl_ativo = 1
JOIN dbo.tb_contrato AS ct ON ct.id_contrato = cm.id_contrato
JOIN dbo.tb_venda AS vd ON vd.id_venda = ct.id_venda
LEFT JOIN dbo.tb_usuario AS usa ON usa.id_usuario = vd.id_atendente
LEFT JOIN dbo.tb_vendaenvolvido AS ver ON ver.id_venda = vd.id_venda
AND ver.id_funcao = 5
LEFT JOIN dbo.tb_usuario AS usr ON usr.id_usuario = ver.id_usuario CROSS APPLY (
	SELECT
		COUNT (id_lancamento) AS nu_parcelas
	FROM
		dbo.vw_vendalancamento
	WHERE
		id_venda = vd.id_venda
) AS vdl OUTER APPLY (
	SELECT
		SUM (nu_valor) AS nu_valorboleto
	FROM
		dbo.vw_vendalancamento
	WHERE
		id_venda = vd.id_venda
	AND id_meiopagamento = 2
) AS vdlb OUTER APPLY (
	SELECT
		SUM (nu_valor) AS nu_valorcartaoc
	FROM
		dbo.vw_vendalancamento
	WHERE
		id_venda = vd.id_venda
	AND id_meiopagamento = 1
) AS vdlcc OUTER APPLY (
	SELECT
		SUM (nu_valor) AS nu_valorcartaod
	FROM
		dbo.vw_vendalancamento
	WHERE
		id_venda = vd.id_venda
	AND id_meiopagamento = 7
) AS vdlcd OUTER APPLY (
	SELECT
		SUM (nu_valor) AS nu_valorcartaor
	FROM
		dbo.vw_vendalancamento
	WHERE
		id_venda = vd.id_venda
	AND id_meiopagamento = 11
) AS vdlcr OUTER APPLY (
	SELECT
		SUM (nu_valor) AS nu_valorcheque
	FROM
		dbo.vw_vendalancamento
	WHERE
		id_venda = vd.id_venda
	AND id_meiopagamento = 4
) AS vdlc OUTER APPLY (
	SELECT
		SUM (nu_valor) AS nu_valoroutros
	FROM
		dbo.vw_vendalancamento
	WHERE
		id_venda = vd.id_venda
	AND id_meiopagamento NOT IN (2, 1, 7, 11, 4, 13, 10)
) AS vdlo OUTER APPLY (
	SELECT
		SUM (nu_valor) AS nu_valorcarta
	FROM
		dbo.vw_vendalancamento
	WHERE
		id_venda = vd.id_venda
	AND id_meiopagamento = 13
) AS vdlct OUTER APPLY (
	SELECT
		SUM (nu_valor) AS nu_valorbolsa
	FROM
		dbo.vw_vendalancamento
	WHERE
		id_venda = vd.id_venda
	AND id_meiopagamento = 10
) AS vdlbl
JOIN dbo.tb_vendaproduto AS pd ON pd.id_venda = vd.id_venda
AND pd.id_vendaproduto = mt.id_vendaproduto
JOIN dbo.tb_produto AS pdt ON pdt.id_produto = pd.id_produto
AND pdt.id_modelovenda = 1
LEFT JOIN tb_produto AS pdc ON pdc.id_produto = pd.id_produtocombo
CROSS APPLY (
	SELECT
		TOP 1 dt_vencimento
	FROM
		dbo.vw_vendalancamento
	WHERE
		id_venda = vd.id_venda
	ORDER BY
		dt_vencimento DESC
) AS vdlv
join tb_formapagamentoaplicacaorelacao as fpar on fpar.id_formapagamento = vd.id_formapagamento
join tb_formapagamentoaplicacao as fpa on fpa.id_formapagamentoaplicacao = fpar.id_formapagamentoaplicacao
GO

﻿/** MENU DA FUNCIONALIDADE DE ACESSO DO PROFESSOR EM LOTE
	  G2 > Pedag�gico > Gest�o de Sala > Professores
**/


SET IDENTITY_INSERT dbo.tb_funcionalidade ON
INSERT  INTO [dbo].[tb_funcionalidade]
        ( [id_funcionalidade] ,
          [st_funcionalidade] ,
          [id_funcionalidadepai] ,
          [bl_ativo] ,
          [id_situacao] ,
          [st_classeflex] ,
          [st_urlicone] ,
          [id_tipofuncionalidade] ,
          [st_classeflexbotao] ,
          [bl_pesquisa] ,
          [bl_lista] ,
          [id_sistema] ,
          [st_urlcaminho] ,
          [bl_visivel]
        )
VALUES  ( 690 ,
          'Professores' ,
          562 ,
          1 ,
          123 ,
          'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa' ,
          '/img/ico/flag.png' ,
          3 ,
          NULL ,
          0 ,
          0 ,
          1 ,
          '/professores' ,
          1
        )

GO
SET IDENTITY_INSERT dbo.tb_funcionalidade OFF
GO

SET IDENTITY_INSERT tb_funcionalidade ON;

INSERT  INTO tb_funcionalidade
        ( id_funcionalidade ,
          st_funcionalidade ,
          id_funcionalidadepai ,
          bl_ativo ,
          id_situacao ,
          st_classeflex ,
          st_urlicone ,
          id_tipofuncionalidade ,
          nu_ordem ,
          st_classeflexbotao ,
          bl_pesquisa ,
          bl_lista ,
          id_sistema ,
          st_ajuda ,
          st_target ,
          st_urlcaminho ,
          bl_visivel ,
          bl_relatorio ,
          st_urlcaminhoedicao ,
          bl_delete
        )
VALUES  ( 692 ,
          'Dividir Sala' ,
          562 ,
          1 ,
          123 ,
          'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa' ,
          '/img/ico/door.png' ,
          3 ,
          NULL ,
          'br.com.ead1.gestor2.view.conteudo.pedagogico.cadastrar.CadastrarSalaDeAula' ,
          1 ,
          0 ,
          1 ,
          NULL ,
          NULL ,
          '/sala-de-aula/dividir-sala' ,
          1 ,
          1 ,
          NULL ,
          0
        );

SET IDENTITY_INSERT tb_funcionalidade OFF;

GO



BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_categoriaendereco on
INSERT INTO [dbo].tb_categoriaendereco
([id_categoriaendereco]
,[st_categoriaendereco])
VALUES

(5,'Agendamento')

GO
SET IDENTITY_INSERT dbo.tb_categoriaendereco off

COMMIT


/************************************************
INSERINDO NOVA TIPO DE ENDERE�O
************************************************/
BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_tipoendereco on
INSERT INTO [dbo].tb_tipoendereco
([id_tipoendereco]
,[st_tipoendereco]
,[id_categoriaendereco])
VALUES

(6,'Aplicador de prova', 5)

GO
SET IDENTITY_INSERT dbo.tb_tipoendereco off

COMMIT


GO

create VIEW [dbo].[vw_salaslimitealocacao] AS

SELECT  dados.id_saladeaula,
dados.st_saladeaula,
dados.id_notificacaoentidade,
dados.id_notificacao,
n.st_notificacao,
dados.nu_maxalunos,
dados.nu_alunosalocados,
dados.nu_percentualalunossala,
dados.nu_percentuallimitealunosala,
dados.id_entidade,
uper.id_usuario AS id_usuarioprofessor,
u.st_nomecompleto AS st_nomeprofessor,
nep.id_perfil AS id_perfilenviar,
upere.id_usuario as id_usuarioenviar,
upere.st_nomecompleto as st_usuarioenviar,
dados.bl_enviaparaemail
FROM tb_notificacao AS n 
JOIN dbo.tb_notificacaoentidade AS ne1 ON ne1.id_notificacao = n.id_notificacao 
INNER JOIN tb_notificacaoentidadeperfil AS nep ON nep.id_notificacaoentidade =ne1.id_notificacaoentidade 		
OUTER APPLY (
		SELECT 
		ne.id_notificacaoentidade,
		ne.id_notificacao,
		ne.id_entidade,
		ne.bl_enviaparaemail,
		al.id_saladeaula,
		sal.st_saladeaula,
		sal.nu_maxalunos,
		ne.nu_percentuallimitealunosala,
		COUNT(al.id_alocacao) AS nu_alunosalocados,
		(COUNT(al.id_alocacao)*100/sal.nu_maxalunos) AS nu_percentualalunossala
		FROM  tb_notificacaoentidade AS ne 
		INNER JOIN tb_saladeaulaentidade AS se ON se.id_entidade = ne.id_entidade
		INNER JOIN tb_saladeaula AS sal ON sal.id_saladeaula = se.id_saladeaula AND sal.bl_ativa=1
		INNER JOIN tb_alocacao AS al ON al.id_saladeaula = sal.id_saladeaula AND al.bl_ativo=1
		AND EXISTS (SELECT * FROM dbo.tb_alocacao AS aloc WHERE aloc.id_saladeaula=al.id_saladeaula AND aloc.bl_ativo=1 AND aloc.dt_cadastro BETWEEN GETDATE()-1 AND GETDATE() )
		WHERE ne.id_notificacao = 1 AND ne1.id_entidade =ne.id_entidade
		GROUP BY ne.id_notificacao,ne.id_notificacaoentidade,ne.id_entidade,ne.bl_enviaparaemail, al.id_saladeaula,sal.st_saladeaula,sal.nu_maxalunos, ne.nu_percentuallimitealunosala
		HAVING (COUNT(al.id_alocacao)*100/sal.nu_maxalunos) > ne.nu_percentuallimitealunosala ) AS dados
LEFT JOIN tb_usuarioperfilentidadereferencia AS uper ON uper.id_saladeaula=dados.id_saladeaula AND uper.bl_titular=1 AND uper.bl_ativo=1
LEFT JOIN tb_usuario AS u ON u.id_usuario=uper.id_usuario
inner Join vw_usuarioperfilentidadereferencia as upere on upere.id_perfil=nep.id_perfil and upere.id_entidade=dados.id_entidade




GO


 alter VIEW [dbo].[vw_saladisciplinaalocacao]
AS
SELECT
  aps.id_saladeaula,
  sa.st_saladeaula,
  sa.nu_maxalunos,
  aps.id_projetopedagogico,
  pp.id_trilha,
  mt.id_matricula,
  ds.id_disciplina,
  md.id_matriculadisciplina,
  mt.id_usuario,
  al.id_alocacao,
  se.id_entidade,
  (SELECT
    COUNT(alc.id_alocacao)
  FROM tb_alocacao alc,
       dbo.tb_matriculadisciplina mdc
  WHERE alc.id_saladeaula = sa.id_saladeaula
  AND mdc.id_matriculadisciplina = alc.id_matriculadisciplina
  AND mdc.id_evolucao = 13)
  AS nu_alocados,
  sa.dt_abertura,
  sa.dt_encerramento,
  sa.id_categoriasala,
  pp.st_projetopedagogico,
  al.bl_ativo
FROM tb_matricula AS mt
JOIN tb_projetopedagogico AS pp
  ON mt.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_areaprojetosala AS aps
  ON aps.id_projetopedagogico = mt.id_projetopedagogico
JOIN tb_saladeaula AS sa
  ON sa.id_saladeaula = aps.id_saladeaula
  AND sa.id_situacao = 8
  AND sa.bl_ativa = 1
JOIN tb_disciplinasaladeaula AS ds
  ON ds.id_saladeaula = sa.id_saladeaula
JOIN tb_matriculadisciplina AS md
  ON md.id_matricula = mt.id_matricula
  AND ds.id_disciplina = md.id_disciplina
JOIN tb_saladeaulaentidade AS se
  ON se.id_saladeaula = sa.id_saladeaula
  AND se.id_entidade = mt.id_entidadeatendimento
LEFT JOIN tb_alocacao al
  ON al.id_matriculadisciplina = md.id_matriculadisciplina
  AND al.id_saladeaula = sa.id_saladeaula
  AND al.bl_ativo = '1'
WHERE (sa.dt_encerramento >= CAST(GETDATE() AS DATE))
OR (dt_encerramento IS NULL)
OR (al.id_saladeaula = sa.id_saladeaula)
  UNION
  SELECT
  aps.id_saladeaula,
  sa.st_saladeaula,
  sa.nu_maxalunos,
  aps.id_projetopedagogico,
  pp.id_trilha,
  mt.id_matricula,
  ds.id_disciplina,
  md.id_matriculadisciplina,
  mt.id_usuario,
  al.id_alocacao,
  ve.id_entidade,
  (SELECT
    COUNT(alc.id_alocacao)
  FROM tb_alocacao alc,
       dbo.tb_matriculadisciplina mdc
  WHERE alc.id_saladeaula = sa.id_saladeaula
  AND mdc.id_matriculadisciplina = alc.id_matriculadisciplina
  AND mdc.id_evolucao = 13)
  AS nu_alocados,
  sa.dt_abertura,
  sa.dt_encerramento,
  sa.id_categoriasala,
  pp.st_projetopedagogico,
  al.bl_ativo
FROM tb_matricula AS mt
JOIN tb_projetopedagogico AS pp
  ON mt.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_areaprojetosala AS aps
  ON aps.id_projetopedagogico = mt.id_projetopedagogico
JOIN tb_saladeaula AS sa
  ON sa.id_saladeaula = aps.id_saladeaula
  AND sa.id_situacao = 8
  AND sa.bl_ativa = 1 AND sa.bl_todasentidades = 1
JOIN tb_disciplinasaladeaula AS ds
  ON ds.id_saladeaula = sa.id_saladeaula
JOIN tb_matriculadisciplina AS md
  ON md.id_matricula = mt.id_matricula
  AND ds.id_disciplina = md.id_disciplina
JOIN vw_entidaderecursivaid AS ve
  ON ve.id_entidade = mt.id_entidadeatendimento
  AND ve.id_entidadepai = sa.id_entidade
LEFT JOIN tb_alocacao al
  ON al.id_matriculadisciplina = md.id_matriculadisciplina
  AND al.id_saladeaula = sa.id_saladeaula
  AND al.bl_ativo = '1'
WHERE ((sa.dt_encerramento >= CAST(GETDATE() AS DATE))
OR (dt_encerramento IS NULL)
OR (al.id_saladeaula = sa.id_saladeaula))
  UNION
  SELECT
  aps.id_saladeaula,
  sa.st_saladeaula,
  sa.nu_maxalunos,
  aps.id_projetopedagogico,
  pp.id_trilha,
  mt.id_matricula,
  ds.id_disciplina,
  md.id_matriculadisciplina,
  mt.id_usuario,
  al.id_alocacao,
  ve.id_entidade,
  (SELECT
    COUNT(alc.id_alocacao)
  FROM tb_alocacao alc,
       dbo.tb_matriculadisciplina mdc
  WHERE alc.id_saladeaula = sa.id_saladeaula
  AND mdc.id_matriculadisciplina = alc.id_matriculadisciplina
  AND mdc.id_evolucao = 13)
  AS nu_alocados,
  sa.dt_abertura,
  sa.dt_encerramento,
  sa.id_categoriasala,
  pp.st_projetopedagogico,
  al.bl_ativo
FROM tb_matricula AS mt
JOIN tb_projetopedagogico AS pp
  ON mt.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_areaprojetosala AS aps
  ON aps.id_projetopedagogico = mt.id_projetopedagogico
JOIN tb_saladeaula AS sa
  ON sa.id_saladeaula = aps.id_saladeaula
  AND sa.id_situacao = 8
  AND sa.bl_ativa = 1 AND sa.bl_todasentidades = 1
JOIN tb_disciplinasaladeaula AS ds
  ON ds.id_saladeaula = sa.id_saladeaula
JOIN tb_matriculadisciplina AS md
  ON md.id_matricula = mt.id_matricula
  AND ds.id_disciplina = md.id_disciplina
JOIN tb_entidade AS ve
  ON ve.id_entidade = sa.id_entidade
LEFT JOIN tb_alocacao al
  ON al.id_matriculadisciplina = md.id_matriculadisciplina
  AND al.id_saladeaula = sa.id_saladeaula
  AND al.bl_ativo = '1'
WHERE ((sa.dt_encerramento >= CAST(GETDATE() AS DATE))
OR (dt_encerramento IS NULL)
OR (al.id_saladeaula = sa.id_saladeaula))

GO


CREATE view vw_saladeaulaquantitativos as
select distinct sa.id_saladeaula,
        sa.dt_cadastro,
        sa.st_saladeaula,
        sa.bl_ativa,
        sa.id_usuariocadastro,
        sa.id_modalidadesaladeaula,
        sa.dt_inicioinscricao,
        sa.dt_fiminscricao,
        sa.dt_abertura,
        sa.st_localizacao,
        sa.id_tiposaladeaula,
        sa.dt_encerramento,
        sa.id_entidade,
        sa.id_periodoletivo,
        sa.bl_usardoperiodoletivo,
        sa.id_situacao,
        sa.nu_maxalunos,
        sa.nu_diasencerramento,
        sa.bl_semencerramento,
        sa.nu_diasaluno,
        sa.bl_semdiasaluno,
        msa.st_modalidadesaladeaula,
        dsa.id_disciplina,
	   (select count(id_alocacao) from tb_alocacao al where al.id_saladeaula = sa.id_saladeaula and al.bl_ativo = 1) as nu_alunos
from tb_saladeaula as sa
INNER JOIN tb_modalidadesaladeaula as msa ON msa.id_modalidadesaladeaula = sa.id_modalidadesaladeaula
INNER JOIN tb_tiposaladeaula as tsa ON tsa.id_tiposaladeaula = sa.id_tiposaladeaula
INNER JOIN tb_entidade as e ON e.id_entidade = sa.id_entidade
INNER JOIN tb_situacao as s ON s.id_situacao = sa.id_situacao
INNER JOIN tb_categoriasala as csa ON csa.id_categoriasala = sa.id_categoriasala
INNER JOIN tb_alocacao alo on alo.id_saladeaula = sa.id_saladeaula
LEFT JOIN tb_disciplinasaladeaula dsa on dsa.id_saladeaula = sa.id_saladeaula

GO

CREATE VIEW [dbo].vw_professorsaladeaulalote as


SELECT DISTINCT sda.id_entidade
, sda.id_saladeaula
, sda.st_saladeaula
, e.st_nomeentidade
, dsa.id_disciplina
, ds.st_disciplina
, sda.dt_abertura
, sda.dt_encerramento
, u.id_usuario
, uper.id_perfilreferencia
, pf.id_perfilpedagogico
, u.st_nomecompleto
, uper.bl_ativo
, pf.id_perfil
, pf.st_nomeperfil
, uper.bl_titular
, CASE WHEN uper.bl_titular = 1 THEN 'Sim'
	   WHEN uper.bl_titular = 0 THEN 'N�o'
	   ELSE ' - '
	   END as st_titular		
, CONVERT (VARCHAR, sda.dt_abertura, 103) as st_dtabertura
, CONVERT (VARCHAR, sda.dt_encerramento, 103) as st_dtencerramento

FROM tb_saladeaula AS sda
JOIN tb_disciplinasaladeaula as dsa on dsa.id_saladeaula = sda.id_saladeaula
JOIN tb_disciplina as ds on ds.id_disciplina = dsa.id_disciplina
JOIN tb_entidade as e on e.id_entidade = sda.id_entidade
LEFT JOIN tb_usuarioperfilentidadereferencia as uper on sda.id_saladeaula = uper.id_saladeaula 
LEFT JOIN tb_usuario as u ON uper.id_usuario = u.id_usuario
LEFT JOIN tb_perfil AS pf ON pf.id_perfil = uper.id_perfil AND pf.id_perfilpedagogico = 1

LEFT JOIN dbo.tb_perfilreferenciaintegracao AS pri ON pri.id_perfilreferencia = uper.id_perfilreferencia
LEFT JOIN dbo.tb_saladeaulaintegracao AS si ON pri.id_saladeaulaintegracao = si.id_saladeaulaintegracao
LEFT JOIN dbo.tb_usuariointegracao AS ui ON ui.id_usuario = u.id_usuario AND ui.id_entidade = sda.id_entidade AND ui.id_sistema = si.id_sistema
LEFT JOIN dbo.tb_entidadeintegracao AS ei ON ei.id_entidade = sda.id_entidade AND ei.id_sistema = si.id_sistema
GO
ALTER view  [dbo].[vw_mensagemusuario]
as
SELECT
	ms.id_mensagem,
	ms.st_mensagem,
	ms.bl_importante,
	ms.id_usuariocadastro,
	ms.st_texto,
	ms.dt_cadastro AS dt_cadastromsg
	,ms.id_entidade,
	ms.id_situacao,
	ms.id_projetopedagogico,
	ms.id_areaconhecimento,
	ms.id_turma,
	em.id_evolucao,
	em.dt_envio,
	em.dt_enviar,
	e.st_evolucao,
	ev.id_usuario,
	em.id_sistema,
	em.id_enviomensagem,
	ev.id_enviodestinatario,
	ev.id_evolucao as id_evolucaodestinatario
	FROM
dbo.tb_mensagem AS ms
INNER JOIN tb_enviomensagem AS em ON em.id_mensagem = ms.id_mensagem AND em.id_tipoenvio = 4 and em.id_sistema=1
inner join tb_enviodestinatario as ev on ev.id_enviomensagem=em.id_enviomensagem
inner JOIN tb_evolucao AS e ON e.id_evolucao = em.id_evolucao



GO
alter VIEW [dbo].[vw_gradenota]
AS
    SELECT DISTINCT
            sa.dt_abertura ,
            pp.id_projetopedagogico ,
            pp.st_projetopedagogico ,
            sa.st_saladeaula ,
            mn.st_notatcc ,
            mn.id_tiponotatcc ,
            mn.st_notaead ,
            mn.id_tiponotaead ,
            mn.st_notafinal ,
            mn.id_tiponotafinal ,
            dc.id_disciplina ,
            dc.st_disciplina ,
            dc.id_tipodisciplina ,
            dc.st_tituloexibicao AS st_tituloexibicaodisciplina ,
            mt.id_matricula ,
            md.id_matriculadisciplina ,
            acr.id_avaliacaoconjuntoreferencia ,
            CAST(( md.nu_aprovafinal * pp.nu_notamaxima / 100 ) AS INT) AS nu_notafinal ,
            ev.id_evolucao ,
            ev.st_evolucao ,
            st.id_situacao ,
            st.st_situacao ,
            dc.nu_cargahoraria ,
            sa.id_saladeaula ,
            sa.dt_encerramento ,
            mn.nu_notatotal ,
            sa.id_categoriasala ,
            st_status = CASE WHEN ea.id_alocacao IS NOT NULL
                                  AND md.id_evolucao = 12 THEN ev.st_evolucao
                             WHEN ea.id_alocacao IS NOT NULL
                                  AND md.id_evolucao = 19 THEN ev.st_evolucao
                             WHEN ea.id_alocacao IS NOT NULL
                                  AND md.id_situacao = 65
                             THEN 'Crédito Concedido'
                             WHEN ea.id_alocacao IS NOT NULL
                                  AND ( mn.nu_notatotal + nf.nu_notafaltante ) >= ( pp.nu_notamaxima
                                                              * pp.nu_percentualaprovacao
                                                              / 100 )
                             THEN 'Satisfatório'
                             WHEN ea.id_alocacao IS NOT NULL
                                  AND ( mn.nu_notatotal + nf.nu_notafaltante ) < ( pp.nu_notamaxima
                                                              * pp.nu_percentualaprovacao
                                                              / 100 )
                             THEN 'Insatisfatório'
                             WHEN ea.id_alocacao IS NULL
                                  AND ea.id_encerramentosala IS NULL
                             THEN 'N�o encerrado'
                             ELSE '-'
                        END ,
            bl_status = CASE WHEN md.bl_obrigatorio = 0 THEN 1
                             WHEN ( mn.nu_notatotal
                                    + ISNULL(nf.nu_notafaltante, 0) ) >= ( pp.nu_notamaxima
                                                              * pp.nu_percentualaprovacao
                                                              / 100 ) THEN 1
                             WHEN ( mn.nu_notatotal
                                    + ISNULL(nf.nu_notafaltante, 0) ) < ( pp.nu_notamaxima
                                                              * pp.nu_percentualaprovacao
                                                              / 100 ) THEN 0
                             ELSE 0
                        END ,
            bl_complementar = CASE WHEN ( md.id_matricula != md.id_matriculaoriginal )
                                        AND ppcompl.bl_disciplinacomplementar = 1
                                   THEN 1
                                   ELSE 0
                              END

			, DATEADD(DAY,ISNULL(sa.nu_diasextensao,0), sa.dt_encerramento) as dt_encerramentoextensao
    FROM    dbo.tb_matriculadisciplina AS md
            JOIN dbo.tb_matricula AS mt ON md.id_matricula = mt.id_matricula
            JOIN dbo.tb_disciplina AS dc ON dc.id_disciplina = md.id_disciplina
                                            AND dc.id_tipodisciplina <> 3
            JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
            LEFT JOIN dbo.tb_alocacao AS al ON al.id_matriculadisciplina = md.id_matriculadisciplina
                                               AND al.bl_ativo = 1
            LEFT JOIN dbo.tb_avaliacaoconjuntoreferencia AS acr ON acr.id_saladeaula = al.id_saladeaula --AND acr.dt_fim IS NOT null
            LEFT JOIN tb_encerramentoalocacao AS ea ON ea.id_alocacao = al.id_alocacao
            LEFT JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula
            JOIN tb_evolucao AS ev ON ev.id_evolucao = md.id_evolucao
            JOIN dbo.tb_situacao AS st ON st.id_situacao = md.id_situacao
            LEFT JOIN dbo.vw_matriculanota AS mn ON md.id_matriculadisciplina = mn.id_matriculadisciplina
                                                    AND mn.id_alocacao = al.id_alocacao
            OUTER APPLY ( SELECT    id_matricula ,
                                    id_disciplina ,
                                    SUM(CAST(nu_notamax AS FLOAT)) AS nu_notafaltante
                          FROM      vw_avaliacaoaluno
                          WHERE     bl_ativo IS NULL
                                    AND id_matricula = mt.id_matricula
                                    AND id_disciplina = md.id_disciplina
                                    AND id_tipoprova = 2
                          GROUP BY  id_disciplina ,
                                    id_matricula
                        ) AS nf
            JOIN tb_matricula AS matcompl ON matcompl.id_matricula = md.id_matriculaoriginal
            JOIN tb_projetopedagogico AS ppcompl ON ppcompl.id_projetopedagogico = matcompl.id_projetopedagogico




GO

ALTER VIEW [dbo].[vw_aplicadorendereco]
AS
SELECT        dbo.tb_aplicadorprova.id_aplicadorprova, 
			  dbo.tb_aplicadorprova.st_aplicadorprova,
			  dbo.tb_usuario.id_usuario as id_aplicadorpessoafisica, 
			  dbo.tb_usuario.st_nomecompleto as st_nomepessoafisica, 
			  dbo.tb_entidade.id_entidade as id_aplicadorpessoajuridico, 
			  dbo.tb_entidade.st_nomeentidade as st_nomepessoajuridica,   
			  entidaderelacionada.id_entidade as id_entidaderelacionada,
			  dbo.tb_aplicadorprova.id_entidadecadastro,
			  dbo.tb_endereco.id_endereco,
			  dbo.tb_endereco.st_endereco,
			  dbo.tb_endereco.sg_uf,
			  dbo.tb_endereco.id_municipio

FROM      dbo.tb_aplicadorprova
JOIN	  dbo.tb_aplicadorprovaentidade on dbo.tb_aplicadorprovaentidade.id_aplicadorprova = dbo.tb_aplicadorprova.id_aplicadorprova
JOIN	  dbo.tb_entidade as entidaderelacionada on entidaderelacionada.id_entidade=dbo.tb_aplicadorprovaentidade.id_entidade
LEFT JOIN dbo.tb_usuario ON dbo.tb_usuario.id_usuario=dbo.tb_aplicadorprova.id_usuarioaplicador
LEFT JOIN dbo.tb_pessoaendereco ON (dbo.tb_pessoaendereco.id_usuario = dbo.tb_usuario.id_usuario and  dbo.tb_pessoaendereco.bl_padrao=0 and dbo.tb_pessoaendereco.id_entidade=entidaderelacionada.id_entidade)
LEFT JOIN dbo.tb_entidade ON dbo.tb_entidade.id_entidade=dbo.tb_aplicadorprova.id_entidadeaplicador
LEFT JOIN dbo.tb_entidadeendereco ON (dbo.tb_entidadeendereco.id_entidade=dbo.tb_entidade.id_entidade and dbo.tb_entidadeendereco.bl_padrao=1)
LEFT JOIN dbo.tb_endereco ON ((dbo.tb_endereco.id_endereco = dbo.tb_pessoaendereco.id_endereco AND dbo.tb_endereco.id_tipoendereco = 6)OR 
								dbo.tb_endereco.id_endereco = dbo.tb_entidadeendereco.id_endereco) 
								and dbo.tb_endereco.bl_ativo=1

WHERE dbo.tb_endereco.st_endereco IS NOT NULL


begin transaction

UPDATE tb_evolucao SET st_tabela = 'tb_motivocancelamento_null' WHERE id_evolucao = 48;
go

-- AC-27139



INSERT INTO tb_textovariaveis ( id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
VALUES
( 3, 'id_matricula', '#grid_disciplina_sala_PRR#', 'gerarGridDisciplinasSalaPRR', 2)
-- (406, 3, 'id_matricula', '#grid_atividades_presenciais#', 'gerarGridAtividadesPresenciais', 2);



go

/***********************************************************************************
************************************************************************************
					ADD FUNCIONALIDADE
************************************************************************************
***********************************************************************************/


SET IDENTITY_INSERT dbo.tb_funcionalidade on
INSERT INTO [dbo].[tb_funcionalidade]
([id_funcionalidade]
,[st_funcionalidade]
,[id_funcionalidadepai]
,[bl_ativo]
,[id_situacao]
,[st_classeflex]
,[st_urlicone]
,[id_tipofuncionalidade]
,[st_classeflexbotao]
,[bl_pesquisa]
,[bl_lista]
,[id_sistema]
,[st_urlcaminho])
VALUES

(749,'Edição lançamento de frequência',590 ,1 ,123 ,NULL ,NULL ,7 ,null ,0,0, 1, 749)

GO
SET IDENTITY_INSERT dbo.tb_funcionalidade off




/***********************************************************************************
************************************************************************************
					ADD PERMISSAO
************************************************************************************
***********************************************************************************/

SET IDENTITY_INSERT dbo.tb_permissao on
INSERT INTO [dbo].tb_permissao
(id_permissao,
st_nomepermissao,
st_descricao,
bl_ativo)
VALUES
(32
,'Editar lançaamento de frequência'
,'Permite ao usuário editar um lançaamento de frequencia. As vezes a mesma é lançaada errada e deve ser possível corrigir.'
, 1)
GO
SET IDENTITY_INSERT dbo.tb_permissao off




/***********************************************************************************
************************************************************************************
					ADD PERMISSAO FUNCIONALIDADE
************************************************************************************
***********************************************************************************/

INSERT INTO tb_permissaofuncionalidade
	(id_funcionalidade, id_permissao)
	VALUES
	(749, 32)



/***********************************************************************************
************************************************************************************
				ADD TIPO TRAMITE LANCAMENTO DE FREQUENCIA
************************************************************************************
***********************************************************************************/

	SET IDENTITY_INSERT dbo.tb_tipotramite on
		INSERT INTO tb_tipotramite
			(id_tipotramite , id_categoriatramite, st_tipotramite)
		VALUES
			(23, 5, 'Lançamento de frequência')
	SET IDENTITY_INSERT dbo.tb_tipotramite off




/***********************************************************************************
************************************************************************************
				ADD SITUACAO LANCAMENTO DE FREQUENCIA
************************************************************************************
***********************************************************************************/

	SET IDENTITY_INSERT dbo.tb_situacao on
		INSERT INTO tb_situacao
			(id_situacao , st_situacao, st_tabela, st_campo, st_descricaosituacao)
		VALUES
			(178, 'Presente', 'tb_tramiteagendamento' , 'id_situacao' , 'Presença lançada como : PRESENTE')
		   ,(179, 'Ausente', 'tb_tramiteagendamento' , 'id_situacao' , 'Presença lançada como : AUSENTE')
	SET IDENTITY_INSERT dbo.tb_situacao off




INSERT INTO
tb_textovariaveis (id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
VALUES (
3,
'st_municipioufagendamento',
'#municipio_UF_agendamento#',
'gerarStringMunicipioUfAgendamento',
2
);


INSERT INTO
tb_textovariaveis (id_textocategoria, st_camposubstituir, st_textovariaveis, id_tipotextovariavel)
VALUES (
3,
'st_areaconhecimento',
'#area_conhecimento#',
1
);


insert into tb_textovariaveis ( id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
    values (4,'st_pai' ,'#nome_pai#', 'retornaVariaveisUpper', 1);
insert into tb_textovariaveis (id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
    values (4,'st_filiacao' ,'#nome_mae#', 'retornaVariaveisUpper', 1);
insert into tb_textovariaveis (id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
    values (4,'st_ufnascimento' ,'#naturalidade_aluno#', 'retornaVariaveisUpper', 1);
insert into tb_textovariaveis (id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
    values (4,'id_pais' ,'#nacionalidade_aluno#', 'retornaNacionalidade', 1);
insert into tb_textovariaveis (id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
    values (4,'dt_dataexpedicaoaluno' ,'#data_expedicao_aluno_DD_MM_AAAA#', 'converterDataUsaBr', 1);
insert into tb_textovariaveis (id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
    values ( 4,'st_nomeinstituicao' ,'#estabelecimento_conclusao#', 'retornaVariaveisUpper', 1);
insert into tb_textovariaveis (id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
    values (4,'ano_conclusao_medio' ,'#ano_conclusao_medio#', null, 1);
insert into tb_textovariaveis (id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
    values ( 4,'dt_expedicaocertificado' ,'#data_expedicao_certificado#', 'converterDataUsaBr', 1);
insert into tb_textovariaveis (id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
    values ( 4,'ano_conclusao' ,'#ano_conclusao#', null, 1);
insert into tb_textovariaveis (id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
    values (4,'semestre_conclusao' ,'#semestre_conclusao#', null, 1);
go


INSERT INTO tb_textovariaveis
(id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
VALUES
(4 , 'id_matricula' , '#grid_disciplina_historico_escolar#' , 'gerarGridHistoricoEscolar' , 2)
go
-- GRA-121

SET IDENTITY_INSERT tb_etnia ON;
INSERT INTO tb_etnia(id_etnia, st_etnia) VALUES (6, 'Não Desejo Informar');
SET IDENTITY_INSERT tb_etnia OFF;

-- GRA-123
UPDATE tb_funcionalidade SET st_funcionalidade = 'Isenção de Disciplina' WHERE id_funcionalidade = 744;

ALTER TABLE tb_pessoadadoscomplementares ADD sg_ufinstituicao CHAR(2) FOREIGN KEY REFERENCES tb_uf(sg_uf);
ALTER TABLE  tb_pessoadadoscomplementares ADD id_municipioinstituicao NUMERIC(30) FOREIGN KEY REFERENCES tb_municipio(id_municipio);

insert into tb_textovariaveis (id_textocategoria,st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel) values(
   4, 'st_municipioinstituicao','#municipio _estabelecimento#', 'retornaVariaveisUpper', 1
);
go


/**
 ADD CAMPO nu_semestre na tb_matriculadisciplina
**/

--ALTER TABLE tb_matriculadisciplina
--ADD nu_semestre INT NULL

go
/**
 ADD CAMPO nu_semestre na tb_venda
**/
--ALTER TABLE tb_venda
--ADD nu_semestre INT NULL
--go
INSERT INTO tb_textovariaveis
( id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
VALUES
--Valor do semestre
(3 , 'id_matricula' , '#valor_semestre#' , 'retornaValorSemestre', 2),
-- Valor da entrada
(3 , 'id_matricula' , '#valor_entrada#' , 'retornaValorEntrada', 2),
-- Valor da primeira parcela
(3 , 'id_matricula' , '#valor_mensalidade#' , 'retornaValorParcela', 2),
-- Número de parcelas
(3 , 'id_matricula' , '#qtd_parcelas#' , 'retornaNumeroParcelas', 2),
-- Coeficiente de Rendimento
(3 , 'id_matricula' , '#CR_acumulado#' , 'retornaCoeficienteRendimento', 2)

--GRA-78

SET IDENTITY_INSERT tb_situacao ON;
INSERT INTO tb_situacao(id_situacao, st_situacao, st_tabela, st_campo, st_descricaosituacao)
VALUES(177, 'Apto para Recuperação', 'tb_matriculadisciplina', 'id_situacaoagendamento', 'Define se o aluno está apto para agendar a prova de recuperação quando o agendamento for por disciplina.')
SET IDENTITY_INSERT tb_situacao OFF;
GO
ALTER TABLE tb_matriculadisciplina ADD id_situacaoagendamento INT NOT NULL DEFAULT (119); -- 119 = Nao Apto para Agendamento
GO
ALTER TABLE tb_matriculadisciplina WITH CHECK ADD CONSTRAINT [tb_matriculadisciplina_tb_situacao_FK] FOREIGN KEY([id_situacaoagendamento])
REFERENCES tb_situacao (id_situacao);
GO
CREATE NONCLUSTERED INDEX [idx_id_avaliacaoconjuntoreferencia]
ON [dbo].[tb_avalagendamentoref] ([id_avaliacaoagendamento],[id_avaliacaoconjuntoreferencia]);
go
ALTER TABLE tb_venda
ADD dt_tentativarenovacao DATE NULL

--rollback

--commit

GO





ALTER VIEW [dbo].[vw_enviarmensagemaluno] AS 
SELECT
	ed.id_enviomensagem,
	ed.id_usuario,
	ed.st_endereco,
	ed.id_matricula,
	em.id_mensagem,
	em.id_tipoenvio,
	ed.id_evolucao,
	em.dt_cadastro,
	ms.st_mensagem,
	ms.bl_importante,
	em.dt_enviar,
	ms.st_texto,
	ms.dt_cadastro AS dt_cadastromsg
	,ms.id_entidade
	,ed.id_sistema

FROM
	dbo.tb_enviodestinatario AS ed
INNER JOIN dbo.tb_enviomensagem AS em ON ed.id_enviomensagem = em.id_enviomensagem
AND em.id_tipoenvio = 4
INNER JOIN dbo.tb_mensagem AS ms ON ms.id_mensagem = em.id_mensagem
WHERE
	(
		CAST (em.dt_enviar AS DATE) <= CAST (GETDATE() AS DATE)
	)
AND (ed.id_evolucao IN(42, 43))
AND ms.id_situacao = 147



GO
CREATE VIEW [dbo].[vw_perfisentidadenotificacao]
AS
    SELECT DISTINCT
            n.id_notificacao ,
            n.st_notificacao ,
            vper.id_entidade ,
            ne.bl_enviaparaemail ,
            ne.nu_percentuallimitealunosala ,
            vper.id_perfil ,
            vper.st_nomeperfil ,
            ne.id_notificacaoentidade ,
            id_notificacaoentidadeperfil ,
            n.dt_cadastro
    FROM    vw_perfilentidaderelacao vper
	        JOIN tb_notificacao n ON n.id_notificacao IS NOT null
            LEFT JOIN tb_notificacaoentidade AS ne ON ne.id_entidade = vper.id_entidade and n.id_notificacao = ne.id_notificacao
            LEFT JOIN tb_notificacaoentidadeperfil AS nep ON nep.id_notificacaoentidade = ne.id_notificacaoentidade AND nep.id_perfil = vper.id_perfil

			GO
            

			GO
alter VIEW [dbo].[vw_categoria]
AS
    SELECT
  c.id_categoria,
  c.st_nomeexibicao,
  c.st_categoria,
  c.id_usuariocadastro,
  ce.id_entidade AS id_entidadecadastro,
  c.id_uploadimagem,
  c.id_categoriapai,
  c.id_situacao,
  c.dt_cadastro,
  c.bl_ativo,
  cp.id_produto,
  pr.st_produto
FROM dbo.tb_categoria AS c
LEFT OUTER JOIN dbo.tb_categoriaproduto AS cp
  ON c.id_categoria = cp.id_categoria
LEFT OUTER JOIN dbo.tb_produto AS pr
  ON cp.id_produto = pr.id_produto
INNER JOIN dbo.tb_categoriaentidade AS ce
  ON ce.id_categoria = c.id_categoria
GO
GO



ALTER VIEW [dbo].[vw_categoriaproduto] AS

SELECT
  c.id_categoria,
  c.st_nomeexibicao,
  c.st_categoria,
  c.id_usuariocadastro,
  ce.id_entidade AS id_entidadecadastro,
  c.id_uploadimagem,
  c.id_categoriapai,
  c.id_situacao,
  c.dt_cadastro,
  c.bl_ativo,
  cp.id_produto,
  p.st_produto
FROM
  tb_categoria AS c
	LEFT JOIN tb_categoriaproduto AS cp ON (c.id_categoria = cp.id_categoria)
	JOIN dbo.tb_categoriaentidade AS ce ON ce.id_categoria = c.id_categoria
	JOIN dbo.tb_produto AS p ON cp.id_produto = p.id_produto


GO


GO


CREATE VIEW [dbo].[vw_desativarprofessormoodle] 
AS
SELECT 
uper.id_perfilreferencia,
uper.id_usuario,
sali.st_codsistemacurso,
usi.id_entidade,
uper.bl_desativarmoodle


FROM tb_usuarioperfilentidadereferencia AS uper
join tb_usuario as u on u.id_usuario = uper.id_usuario
join tb_perfil as pr on pr.id_perfil = uper.id_perfil
join tb_perfilpedagogico as pp on pp.id_perfilpedagogico = pr.id_perfilpedagogico  AND pp.id_perfilpedagogico = 1
join tb_perfilreferenciaintegracao as pri on pri.id_perfilreferencia=uper.id_perfilreferencia and pri.id_sistema=6
join tb_saladeaulaintegracao as sali on sali.id_saladeaulaintegracao = pri.id_saladeaulaintegracao
join tb_usuariointegracao as usi on usi.id_usuario = uper.id_usuario and usi.id_sistema=pri.id_sistema
WHERE uper.bl_desativarmoodle = 1

GO

ALTER view [dbo].[vw_gerararquivodeenvio] as
select 
p.id_pacote,
REPLACE(REPLACE(vw.st_cpf,'.',''),'-','')  as st_cpf,
vw.st_nomecompleto,
vw.st_endereco,
vw.nu_numero,
vw.st_complemento,
vw.st_bairro,
vw.st_nomemunicipio,
vw.sg_uf,
REPLACE(REPLACE(vw.st_cep,'.',''),'-','') as st_cep,
em.id_itemdematerial,
it.st_itemdematerial,
d.id_disciplina,
d.st_disciplina,
pp.id_projetopedagogico,
pp.st_projetopedagogico,
area.id_areaconhecimento,
area.st_areaconhecimento,
mt.id_entidadeatendimento as id_entidade,
ent.st_nomeentidade,
lm.id_lotematerial,
cast(p.dt_cadastro as date) as dt_criacaopacote
from tb_lotematerial as lm
join tb_pacote as p on p.id_lotematerial = lm.id_lotematerial and p.id_situacao=155
join tb_entregamaterial as em on em.id_pacote = p.id_pacote
join tb_itemdematerial as it on it.id_itemdematerial=em.id_itemdematerial
join tb_matriculadisciplina as md on md.id_matriculadisciplina = em.id_matriculadisciplina and md.id_pacote = p.id_pacote
join tb_disciplina as d on d.id_disciplina = md.id_disciplina
join tb_matricula as mt on mt.id_matricula=em.id_matricula and mt.id_evolucao=6
join tb_entidade as ent on ent.id_entidade = mt.id_entidadeatendimento
join tb_projetopedagogico as pp on pp.id_projetopedagogico=mt.id_projetopedagogico
join tb_areaprojetopedagogico as ar on ar.id_projetopedagogico=pp.id_projetopedagogico
join tb_areaconhecimento as area on area.id_areaconhecimento=ar.id_areaconhecimento --and area.id_entidade = mt.id_entidadeatendimento
join vw_pessoa as vw on vw.id_usuario = mt.id_usuario and vw.id_entidade=mt.id_entidadeatendimento


GO


alter VIEW vw_gridcertificado AS
SELECT 
	DISTINCT
	mt.id_matricula,
	mt.id_projetopedagogico,
	pp.st_tituloexibicao,
	CAST(mtd.id_disciplina AS VARCHAR(200)) AS id_disciplina,
	dc.st_tituloexibicao AS st_disciplina,
	mtd.nu_aprovafinal AS nu_aprovafinal,
	pp.nu_notamaxima,
	mtd.dt_conclusao AS dt_conclusaodisciplina,
	dc.nu_cargahoraria AS nu_cargahoraria,
	mtd.id_evolucao,
	ev.st_evolucao,
	mtd.id_situacao,
	CASE	
		WHEN CAST(mtd.nu_aprovafinal AS INT) < 70 THEN 'Insuficiente'
		WHEN CAST(mtd.nu_aprovafinal AS INT) < 80 THEN 'Bom'
		WHEN CAST(mtd.nu_aprovafinal AS INT) < 90 THEN '�timo'
		WHEN CAST(mtd.nu_aprovafinal AS INT) <= 100 THEN 'Excelente'
		ELSE 'Insuficiente'
	END AS st_conceito,
	op.st_nomecompleto AS st_titularcertificacao,
	op.st_titulacao,
	mt.id_entidadematricula,
	mt.dt_concluinte
FROM tb_matricula AS mt
JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_matriculadisciplina AS mtd ON mtd.id_matricula = mt.id_matricula AND mtd.bl_obrigatorio = 1
JOIN tb_matricula AS mt2 ON mt2.id_matricula = mtd.id_matriculaoriginal
LEFT JOIN tb_aproveitamentomatriculadisciplina AS apm ON apm.id_matricula = mtd.id_matricula AND apm.id_disciplina = mtd.id_disciplina
LEFT JOIN tb_aproveitamentodisciplina AS apd ON apd.id_aproveitamentodisciplina = apm.id_aproveitamentodisciplina
JOIN tb_disciplina AS dc ON dc.id_disciplina = mtd.id_disciplina
JOIN tb_entidade AS ent ON ent.id_entidade = mt.id_entidadematriz
JOIN tb_evolucao AS ev ON ev.id_evolucao = mtd.id_evolucao
outer apply (
        select us.st_nomecompleto, tit.st_titulacao from tb_usuarioperfilentidadereferencia AS uper
        JOIN tb_perfil AS pf ON pf.id_entidade = mt.id_entidadematricula AND pf.id_perfilpedagogico = 9 AND uper.id_perfil = pf.id_perfil
        LEFT JOIN tb_usuario AS us ON us.id_usuario = uper.id_usuario
        LEFT JOIN tb_titulacao tit ON uper.id_titulacao = tit.id_titulacao
        where uper.id_projetopedagogico = mt2.id_projetopedagogico AND uper.id_disciplina = mtd.id_disciplina 
        AND ((mt.dt_concluinte BETWEEN uper.dt_inicio AND uper.dt_fim) OR (mt.dt_concluinte > uper.dt_inicio AND uper.dt_fim IS NULL)) 
        AND uper.bl_titular = 1 AND uper.bl_ativo = 1 AND uper.id_usuario IS NOT NULL
        ) as op


GO

ALTER VIEW [dbo].[vw_matricula]
AS
    SELECT  mt.id_usuario ,
            us.st_nomecompleto ,
            us.st_cpf ,
            ps.st_urlavatar ,
            ps.dt_nascimento ,
            ps.st_nomepai ,
            ps.st_nomemae ,
            ps.st_email ,
            CAST(ps.nu_ddd AS VARCHAR) + '-' + CAST(ps.nu_telefone AS VARCHAR) AS st_telefone ,
            CAST(ps.nu_dddalternativo AS VARCHAR) + '-'
            + CAST(ps.nu_telefonealternativo AS VARCHAR) AS st_telefonealternativo ,
            mt.id_matricula ,
            pp.st_projetopedagogico ,
            mt.id_situacao ,
            st.st_situacao ,
            mt.id_evolucao ,
            ev.st_evolucao ,
            mt.dt_concluinte ,
            mt.id_entidadematriz ,
            etmz.st_nomeentidade AS st_entidadematriz ,
            mt.id_entidadematricula ,
            etm.st_nomeentidade AS st_entidadematricula ,
            mt.id_entidadeatendimento ,
            eta.st_nomeentidade AS st_entidadeatendimento ,
            mt.bl_ativo ,
            cm.id_contrato ,
            pp.id_projetopedagogico ,
            ct.dt_termino ,
            mt.dt_cadastro ,
            mt.dt_inicio ,
            tm.id_turma ,
            tm.st_turma ,
            tm.dt_inicio AS dt_inicioturma ,
            mt.bl_institucional ,
            tm.dt_fim AS dt_terminoturma ,
            mt.id_situacaoagendamento ,
            mt.id_vendaproduto ,
            mt.dt_termino AS dt_terminomatricula ,
            CAST (RIGHT(master.dbo.fn_varbintohexstr(HASHBYTES('MD5',
                                                              ( CAST (us.id_usuario AS VARCHAR(10)) COLLATE Latin1_General_CI_AI
                                                              + CAST (( CASE
                                                              WHEN mt.id_matricula IS NULL
                                                              THEN 0
                                                              ELSE mt.id_matricula
                                                              END ) AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
                                                              + CAST (5 AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
                                                              + CAST (( 0 ) AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
                                                              + us.st_login
                                                              + us.st_senha
                                                              + CONVERT (VARCHAR(10), GETDATE(), 103) ))),
                        32) AS CHAR(32)) COLLATE Latin1_General_CI_AI AS st_urlacesso ,
            mt.bl_documentacao ,
            ct.id_venda ,
            eta.st_urlportal ,
            mt.st_codcertificacao ,
            ps.st_identificacao ,
            CONVERT(VARCHAR, mtc.dt_cadastro, 103) AS st_dtcadastrocertificacao ,
            CONVERT(VARCHAR, mtc.dt_enviocertificado, 103) AS st_dtenviocertificado ,
            CONVERT(VARCHAR, mtc.dt_retornocertificadora, 103) AS st_dtretornocertificadora ,
            CONVERT(VARCHAR, mtc.dt_envioaluno, 103) AS st_dtenvioaluno ,
            st_codigoacompanhamento ,
            mtc.id_indicecertificado ,
            mt.id_evolucaocertificacao ,
            ap.id_areaconhecimento ,
            pp.bl_disciplinacomplementar ,
            mt.id_matriculavinculada,
			cr.id_contratoregra,
			trc.id_tiporegracontrato
    FROM    tb_matricula mt
            JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
            JOIN tb_areaprojetopedagogico AS ap ON ap.id_projetopedagogico = pp.id_projetopedagogico
            JOIN tb_entidade AS etmz ON etmz.id_entidade = mt.id_entidadematriz
            JOIN tb_entidade AS etm ON etm.id_entidade = mt.id_entidadematricula
            JOIN tb_entidade AS eta ON eta.id_entidade = mt.id_entidadeatendimento
            JOIN tb_situacao AS st ON st.id_situacao = mt.id_situacao
            JOIN tb_evolucao AS ev ON ev.id_evolucao = mt.id_evolucao
            JOIN tb_usuario AS us ON us.id_usuario = mt.id_usuario
            LEFT JOIN tb_contratomatricula AS cm ON cm.id_matricula = mt.id_matricula
                                                    AND cm.bl_ativo = 1
            LEFT JOIN vw_pessoa AS ps ON mt.id_usuario = ps.id_usuario
                                         AND mt.id_entidadeatendimento = ps.id_entidade
            LEFT JOIN dbo.tb_contrato AS ct ON ct.id_contrato = cm.id_contrato
                                               AND ct.bl_ativo = 1
            LEFT JOIN tb_turma AS tm ON tm.id_turma = mt.id_turma
            LEFT JOIN tb_matriculacertificacao mtc ON mtc.id_matricula = mt.id_matricula
                                                      AND mtc.bl_ativo = 1
            LEFT JOIN dbo.tb_contratoregra AS cr ON cr.id_contratoregra = ct.id_contratoregra
            LEFT JOIN dbo.tb_tiporegracontrato AS trc ON trc.id_tiporegracontrato = cr.id_tiporegracontrato
    WHERE   mt.bl_ativo = 1;

	GO





	
create view  vw_mensagemusuario
as
SELECT
	ms.id_mensagem,
	ms.st_mensagem,
	ms.bl_importante,
	ms.id_usuariocadastro,
	ms.st_texto,
	ms.dt_cadastro AS dt_cadastromsg
	,ms.id_entidade,
	ms.id_situacao,
	ms.id_projetopedagogico,
	ms.id_areaconhecimento,
	ms.id_turma,
	ev.id_evolucao,
	em.dt_envio,
	em.dt_enviar,
	e.st_evolucao,
	ev.id_usuario,
	ev.id_sistema,
	em.id_enviomensagem,
	ev.id_enviodestinatario
	FROM
dbo.tb_mensagem AS ms
INNER JOIN tb_enviomensagem AS em ON em.id_mensagem = ms.id_mensagem AND em.id_tipoenvio = 4
inner join tb_enviodestinatario as ev on ev.id_enviomensagem=em.id_enviomensagem and ev.id_sistema=1
INNER JOIN tb_evolucao AS e ON e.id_evolucao = ev.id_evolucao

GO


UPDATE tb_textovariaveis SET st_camposubstituir = 'st_terminoturmaextenso' WHERE id_textocategoria = 3 and id_textovariaveis = 234;
UPDATE tb_textovariaveis SET st_camposubstituir = 'st_atualextenso' WHERE id_textocategoria = 3 and id_textovariaveis = 362;
DELETE FROM tb_textovariaveis WHERE id_textovariaveis = 161 and id_textocategoria = 3 and st_textovariaveis = '#cpf_aluno#';
DELETE FROM tb_textovariaveis WHERE id_textovariaveis = 165 and id_textocategoria = 3 and st_textovariaveis = '#rg_aluno#';

go
SET IDENTITY_INSERT dbo.tb_funcionalidade ON
INSERT INTO tb_funcionalidade (
    id_funcionalidade,
    st_funcionalidade,
    id_funcionalidadepai,
    bl_ativo,
    id_situacao,
    st_classeflex,
    st_urlicone,
    id_tipofuncionalidade,
    nu_ordem,
    st_classeflexbotao,
    bl_pesquisa,
    bl_lista,
    id_sistema,
    st_ajuda,
    st_target,
    st_urlcaminho,
    bl_visivel,
    bl_relatorio,
    st_urlcaminhoedicao,
    bl_delete)
VALUES (
    745,
   'Relatório Folha de Pagamento - Por Matéria',
    488,
    1,
    123,
    'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa',
    '/img/ico/flag.png',
    3,
    NULL,
    '',
    0,
    0,
    1,
    NULL,
    NULL,
    '/relatorio-calculo-pagamento-materia',
    1,
    0,
    NULL,
    0);
SET IDENTITY_INSERT dbo.tb_funcionalidade OFF

GO



INSERT tb_itemconfiguracao (id_itemconfiguracao, st_itemconfiguracao, st_descricao, st_default) VALUES
(19, 'Quantidade Padrão de Disciplinas por Período', 'Quantidade Padr�o de Disciplinas por Per�odo - Apenas para gradua��o', ''),
(20, 'Quantidade M�nima de Disciplinas por Período', 'Quantidade M�nima de Disciplinas por Per�odo - Apenas para gradua��o', ''),
(21, 'Quantidade M�xima de Disciplinas por Período', 'Quantidade M�xima de Disciplinas por Per�odo - Apenas para gradua��o', '')

go

/***
*** TABELA LINHA DE NEGOCIO
**/

CREATE TABLE tb_linhadenegocio
(
id_linhadenegocio INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
st_linhadenegocio varchar(255) NOT NULL
);


/***
*** INSERINDO DADOS NA TABELA LINHA DE NEGOCIO
**/
BEGIN TRAN
SET IDENTITY_INSERT dbo.tb_linhadenegocio ON
INSERT INTO dbo.tb_linhadenegocio
(id_linhadenegocio, st_linhadenegocio)
VALUES( 1 , 'Concurso'),
(2, 'Graduação'),
(3, 'Pós Graduação')
SET IDENTITY_INSERT dbo.tb_linhadenegocio OFF
COMMIT

/***
*** INSERINDO ITEM DE CONFIGURA��O PARA LINHA DE NEGOCIO
**/
INSERT tb_itemconfiguracao (id_itemconfiguracao, st_itemconfiguracao, st_descricao, st_default) VALUES
(22, 'Linha de negócio', 'Linha de negócio da entidade', '')

go

--- Atualização do arquivo

SET IDENTITY_INSERT dbo.tb_funcionalidade on
INSERT INTO dbo.tb_funcionalidade
        ( id_funcionalidade,
          st_funcionalidade ,
          id_funcionalidadepai ,
          bl_ativo ,
          id_situacao ,
          st_classeflex ,
          st_urlicone ,
          id_tipofuncionalidade ,
          nu_ordem ,
          st_classeflexbotao ,
          bl_pesquisa ,
          bl_lista ,
          id_sistema ,
          st_ajuda ,
          st_target ,
          st_urlcaminho ,
          bl_visivel ,
          bl_relatorio ,
          st_urlcaminhoedicao ,
          bl_delete
        )
VALUES  (744, 'Aproveitamento de Disciplina', 157,	1, 123,	'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa',	'/img/ico/door--arrow.png', 3,	NULL,	'LancarAproveitamento',	0,	0,	1,	NULL,	NULL,	'/lancar-aproveitamento',	1,	0,	NULL,	0
        );
SET IDENTITY_INSERT dbo.tb_funcionalidade off

---------------------------------------------------------

CREATE TABLE tb_aproveitamento (
	id_aproveitamento INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	id_matricula INT NOT NULL FOREIGN KEY REFERENCES dbo.tb_matricula(id_matricula),
	dt_cadastro DATETIME DEFAULT GETDATE(),
	id_usuario INT NOT NULL FOREIGN KEY REFERENCES dbo.tb_usuario (id_usuario),
	bl_ativo bit DEFAULT 1
);

----------------------------------------------------------

ALTER TABLE dbo.tb_matriculadisciplina ADD id_usuarioaproveitamento INT NULL FOREIGN KEY REFERENCES dbo.tb_usuario(id_usuario);
ALTER TABLE dbo.tb_matriculadisciplina ADD st_disciplinaoriginal varchar(255) NULL;
ALTER TABLE dbo.tb_matriculadisciplina ADD st_instituicaoaproveitamento varchar(255) NULL;
ALTER TABLE dbo.tb_matriculadisciplina ADD sg_uf char(2) FOREIGN KEY REFERENCES dbo.tb_uf(sg_uf);
ALTER TABLE dbo.tb_matriculadisciplina ADD id_aproveitamento INT NULL FOREIGN KEY REFERENCES tb_aproveitamento(id_aproveitamento);

-----------------------------------------------------------

CREATE TABLE tb_ocorrenciaaproveitamento (
id_ocorrenciaaproveitamento INT PRIMARY KEY IDENTITY (1,1),
id_ocorrencia INT NOT NULL FOREIGN KEY REFERENCES dbo.tb_ocorrencia(id_ocorrencia),
id_aproveitamento INT NOT NULL FOREIGN KEY REFERENCES tb_aproveitamento(id_aproveitamento),
bl_ativo BIT DEFAULT 1,
dt_cadastro DATETIME DEFAULT GETDATE(),
id_usuariocadastro INT NOT NULL FOREIGN KEY REFERENCES dbo.tb_usuario(id_usuario)
);

------------------------------------------------------------

ALTER TABLE tb_matricula ADD id_aproveitamento INT NULL FOREIGN KEY REFERENCES dbo.tb_aproveitamento(id_aproveitamento);

------------------------------------------------------------

-- CRIAR TABELA GRUPODISCIPLINA
CREATE TABLE tb_grupodisciplina(
    id_grupodisciplina INT NOT NULL IDENTITY(1,1),
    st_grupodisciplina VARCHAR(100) NOT NULL,
    CONSTRAINT [PK_TB_GRUPODISCIPLINA] PRIMARY KEY CLUSTERED (
	    [id_grupodisciplina] ASC
    )
    WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY];


-- INSERIR REGISTROS NA TABELA CRIADA
SET IDENTITY_INSERT tb_grupodisciplina ON;
INSERT INTO tb_grupodisciplina(id_grupodisciplina, st_grupodisciplina) VALUES
(1, 'D1'),
(2, 'D2'),
(3, 'D3'),
(4, 'D4'),
(5, 'D5');
SET IDENTITY_INSERT tb_grupodisciplina OFF;


--ADICIONAR A FK NA TABELA DISCIPLINA
ALTER TABLE tb_disciplina
ADD id_grupodisciplina INT,
CONSTRAINT TB_DISCIPLINA_TB_GRUPODISCIPLINA FOREIGN KEY (id_grupodisciplina) REFERENCES tb_grupodisciplina(id_grupodisciplina);

go

INSERT INTO tb_funcionalidade (id_funcionalidade, st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao, st_classeflex, st_urlicone, id_tipofuncionalidade, nu_ordem, st_classeflexbotao, bl_pesquisa, bl_lista, id_sistema, st_ajuda, st_target, st_urlcaminho, bl_visivel, bl_relatorio, st_urlcaminhoedicao, bl_delete)
    VALUES (735, 'Negociação - Graduação', 346, 1, 123, 'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa', '/img/ico/flag.png', 3, null, 'CadastrarVendaGraduacao', 1, 0, 1,'', null, '/venda-graduacao', 1,0,null,0);

go

alter table tb_venda add nu_creditos int null;
go
CREATE TABLE tb_prematriculadisciplina (
	id_prematriculadisciplina INT IDENTITY(1,1) NOT NULL,
	id_venda INT NOT NULL,
	id_disciplina INT NOT NULL,
	id_saladeaula INT NOT NULL
	CONSTRAINT [tb_prematriculadisciplina_pk] PRIMARY KEY CLUSTERED
	(
		[id_prematriculadisciplina] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE tb_prematriculadisciplina WITH CHECK ADD  CONSTRAINT [TB_PREMATRICULADISCIPLINA_TB_VENDA] FOREIGN KEY(id_venda)
REFERENCES tb_venda (id_venda);

ALTER TABLE tb_prematriculadisciplina WITH CHECK ADD  CONSTRAINT [TB_PREMATRICULADISCIPLINA_TB_DISCIPLINA] FOREIGN KEY(id_disciplina)
REFERENCES tb_disciplina (id_disciplina);

ALTER TABLE tb_prematriculadisciplina WITH CHECK ADD  CONSTRAINT [TB_PREMATRICULADISCIPLINA_TB_SALADEAULA] FOREIGN KEY(id_saladeaula)
REFERENCES tb_saladeaula (id_saladeaula);

/**
** CREATE TABLE tb_meiopagamentograduacao
**/
CREATE TABLE tb_meiopagamentograduacao
(
id_meiopagamentograduacao int NOT NULL IDENTITY(1,1),
st_meiopagamentograduacao varchar(255),
st_descricao varchar(255)
,CONSTRAINT PK_tb_meiopgamentograduacao_id_meiopagamentograduacao PRIMARY KEY CLUSTERED (id_meiopagamentograduacao)
)


/**
** INSERT na tb_meiopagamentograduacao
**/
SET IDENTITY_INSERT dbo.tb_meiopagamentograduacao on
INSERT INTO tb_meiopagamentograduacao
(id_meiopagamentograduacao, st_meiopagamentograduacao, st_descricao)
VALUES (
1, 'Cartão Crédito' , 'Pagamento por meio de cartão de crédito'),
(2, 'Boleto', 'Boleto Bancário')
SET IDENTITY_INSERT dbo.tb_meiopagamentograduacao off
go

ADD campo de data para limite de renova��o da venda do aluno na gradua��o
**/

ALTER TABLE tb_venda
ADD dt_limiterenovacao DATETIME NULL


/**
Add mensagem padrao para envio de email de recupera��o
**/

SET IDENTITY_INSERT dbo.tb_mensagempadrao on
INSERT INTO tb_mensagempadrao
(id_mensagempadrao, id_tipoenvio, st_mensagempadrao, st_default, id_textocategoria)
VALUES
( 37, 3, 'Renovação de matrícula' , 'ALTERAR: Renovação de matrícula', 1)
go
SET IDENTITY_INSERT dbo.tb_mensagempadrao off

ALTER TABLE tb_venda NOCHECK constraint all;
ALTER TABLE tb_venda ADD id_ocorrenciaaproveitamento int default null;
ALTER TABLE tb_venda CHECK constraint ALL;
ALTER TABLE tb_venda ADD CONSTRAINT FK_VENDA_OCORRENCIAAPROVEITAMENTO_OCORRENCIA FOREIGN KEY (id_ocorrenciaaproveitamento) REFERENCES dbo.tb_ocorrencia(id_ocorrencia);


INSERT INTO dbo.tb_motivoocorrencia
        ( id_motivoocorrencia ,
          st_motivoocorrencia
        )
VALUES  ( 3 , -- id_motivoocorrencia - int
          'Aproveitamento de Crédito'  -- st_motivoocorrencia - varchar(100)
        );
go
UPDATE dbo.tb_situacao SET st_situacao = 'Aproveitamento de disciplina' WHERE id_situacao = 65

go

INSERT tb_itemconfiguracao (id_itemconfiguracao, st_itemconfiguracao, st_descricao, st_default) VALUES
(23, 'Realizar 1º agendamento automaticamente', 'Realizar 1º agendamento automaticamente', ''),
(24, 'Avaliações feitas por disciplina', 'Avaliações feitas por disciplina', ''),
(25, 'Utiliza recuperação com média variável', 'Utiliza recuperação com média variável', '')

go

SET IDENTITY_INSERT dbo.tb_tipocalculoavaliacao ON

INSERT INTO tb_tipocalculoavaliacao
(id_tipocalculoavaliacao , st_tipocalculoavaliacao)
VALUES
(2, 'Média variável')

SET IDENTITY_INSERT dbo.tb_tipocalculoavaliacao OFF

go

ALTER TABLE tb_projetopedagogico
ADD nu_percentualaprovacaovariavel NUMERIC NULL

alter table tb_saladeaula add bl_ofertaexcepcional bit default 0

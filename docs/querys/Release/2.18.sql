
----------------------AC-27266-------------------------

BEGIN TRAN
SET IDENTITY_INSERT dbo.tb_funcionalidade ON
INSERT INTO tb_funcionalidade (
	id_funcionalidade,
	st_funcionalidade,
	id_funcionalidadepai,
	bl_ativo,
	id_situacao,
	st_classeflex,
	st_urlicone,
	id_tipofuncionalidade,
	nu_ordem,
	st_classeflexbotao,
	bl_pesquisa,
	bl_lista,
	id_sistema,
	st_ajuda,
	st_target,
	st_urlcaminho,
	bl_visivel,
	bl_relatorio,
	st_urlcaminhoedicao
)
VALUES
	(
		726,
		'Período letivo',
		568,
		1,
		123,
		'br.com.ead1.gestor2.view.conteudo.entidade.cadastrar.PeriodoLetivo',
		'/img/ico/money--arrow.png',
		3,
		NULL,
		'PeriodoLetivo',
		0,
		0,
		1,
		NULL,
		NULL,
		'/periodo-letivo',
		1,
		0,
		NULL
	);

SET IDENTITY_INSERT dbo.tb_funcionalidade OFF
COMMIT

----------------------COM-11555-------------------------

BEGIN TRAN
SET IDENTITY_INSERT dbo.tb_funcionalidade ON
INSERT INTO tb_funcionalidade (
  id_funcionalidade,
  st_funcionalidade,
  id_funcionalidadepai,
  bl_ativo,
  id_situacao,
  st_classeflex,
  st_urlicone,
  id_tipofuncionalidade,
  nu_ordem,
  st_classeflexbotao,
  bl_pesquisa,
  bl_lista,
  id_sistema,
  st_ajuda,
  st_target,
  st_urlcaminho,
  bl_visivel,
  bl_relatorio,
  st_urlcaminhoedicao
)
VALUES
  (
    720,
    'Imprimir Grade Horária',
    562,
    1,
    123,
    'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa',
    '/img/ico/flag.png',
    3,
    NULL,
    '',
    0,
    0,
    1,
    NULL,
    NULL,
    '/grade-horaria-entidade',
    1,
    0,
    NULL
  );

SET IDENTITY_INSERT dbo.tb_funcionalidade OFF
COMMIT

----------------------AC-26907-------------------------

set identity_insert dbo.tb_mensagempadrao ON

insert into tb_mensagempadrao (id_mensagempadrao,id_tipoenvio,st_mensagempadrao,st_default) values (33,3,'Agendamento Liberado Prova Final', null)
insert into tb_mensagempadrao (id_mensagempadrao, id_tipoenvio,st_mensagempadrao,st_default) values (34,3,'Agendamento Liberado Prova Recuperação', null)

set identity_insert dbo.tb_mensagempadrao OFF

----------------------AC-27133-------------------------

SET IDENTITY_INSERT tb_textovariaveis ON;

INSERT INTO tb_textovariaveis (id_textovariaveis, id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
VALUES
(388, 3, 'id_matricula', '#endereco_nucleo_agendamento#', 'retornaEnderecoNucleoUltimoAgendamento', 2),
(389, 3, 'id_matricula', '#data_agendamento_aluno#', 'retornaDataUltimoAgendamento', 2),
(390, 3, 'id_matricula', '#horario_agendamento#', 'retornaHorarioUltimoAgendamento', 2);

SET IDENTITY_INSERT tb_textovariaveis OFF;

----------------------AC-27199-------------------------

BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_funcionalidade on
INSERT INTO [dbo].[tb_funcionalidade]
([id_funcionalidade]
  ,[st_funcionalidade]
  ,[id_funcionalidadepai]
  ,[bl_ativo]
  ,[id_situacao]
  ,[st_classeflex]
  ,[st_urlicone]
  ,[id_tipofuncionalidade]
  ,[st_classeflexbotao]
  ,[bl_pesquisa]
  ,[bl_lista]
  ,[id_sistema]
  ,[st_urlcaminho]
  ,[bl_delete]
  ,[bl_visivel])
VALUES

  (727,'Pauta de Alunos',257,1,123,'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa','/img/ico/flag.png',3,null,0,0, 1, '/pauta-alunos', 0, 1)

SET IDENTITY_INSERT dbo.tb_funcionalidade off

COMMIT

----------------------COM-11467-------------------------

INSERT INTO dbo.tb_itemconfiguracao
( id_itemconfiguracao ,
  st_itemconfiguracao ,
  st_descricao ,
  st_default
)
VALUES  (  17 , -- id_itemconfiguracao - int
           'Solicitar atualização de contrato na loja' , -- st_itemconfiguracao - nvarchar(100)
           'Solicitar atualização de contrato na loja' , -- st_descricao - nvarchar(255)
           ''  -- st_default - nvarchar(200)
);

--deve-se inserir o o esquemaconfiguraçãoitem para determinado esquema, aqui estou inserindo para o esquema
--3, que em homologação é imp

SET IDENTITY_INSERT dbo.tb_esquemaconfiguracao ON

insert into tb_esquemaconfiguracao (id_esquemaconfiguracao, id_usuariocadastro, id_usuarioatualizacao, st_esquemaconfiguracao, dt_cadastro, dt_atualizacao)
values (3,1,3,'IMP','2015-09-15 10:43:42','2015-11-23 11:17:48')

SET IDENTITY_INSERT dbo.tb_esquemaconfiguracao OFF
commit

INSERT INTO dbo.tb_esquemaconfiguracaoitem
( id_esquemaconfiguracao ,
  id_itemconfiguracao ,
  st_valor
)
VALUES  ( 3 , -- id_esquemaconfiguracao - int
          17 , -- id_itemconfiguracao - int
          '1'  -- st_valor - varchar(max)
);


----------------------COM-11470-------------------------

SET IDENTITY_INSERT dbo.tb_tipotramite ON;
INSERT INTO dbo.tb_tipotramite
(id_tipotramite,
 id_categoriatramite ,
 st_tipotramite
)
VALUES  (  18,
           3 , -- id_categoriatramite - int
           'Adicionando produto a venda'  -- st_tipotramite - varchar(30)
);


INSERT INTO dbo.tb_tipotramite
(id_tipotramite,
 id_categoriatramite ,
 st_tipotramite
)
VALUES  (19,
         3 , -- id_categoriatramite - int
         'Removendo produto da venda'  -- st_tipotramite - varchar(30)
);

INSERT INTO dbo.tb_tipotramite
( id_tipotramite,
  id_categoriatramite ,
  st_tipotramite
)
VALUES  ( 20,
          3 , -- id_categoriatramite - int
          'Alterando atendente da venda'  -- st_tipotramite - varchar(30)
);

INSERT INTO dbo.tb_tipotramite
( id_tipotramite,
  id_categoriatramite ,
  st_tipotramite)
VALUES(21, 3,'Alterando lancamento da venda');

SET IDENTITY_INSERT dbo.tb_tipotramite OFF;

----------------------COM-11533-------------------------

CREATE TABLE tb_justificativaviacontrato (
  id_justivicativaviacontrato INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
  id_venda INT NOT NULL,
  nu_viacontrato INT NOT NULL,
  st_justificativa VARCHAR(1000) NOT NULL,
  dt_cadastro DATETIME DEFAULT GETDATE(),
  id_usuariocadastro INT FOREIGN KEY REFERENCES dbo.tb_usuario(id_usuario)
);


SET IDENTITY_INSERT dbo.tb_tipotramite ON;
INSERT INTO dbo.tb_tipotramite
( id_tipotramite,
  id_categoriatramite ,
  st_tipotramite
)
VALUES  ( 17,
          3, -- id_categoriatramite - int
          'Contrato'  -- st_tipotramite - varchar(30)
);
SET IDENTITY_INSERT dbo.tb_tipotramite OFF;

----------------------COM-11635-------------------------

ALTER TABLE dbo.tb_vendaproduto ADD bl_ativo BIT DEFAULT 1;

BEGIN TRANSACTION
UPDATE dbo.tb_vendaproduto SET bl_ativo = 1
COMMIT

commit;

----------------------FIN-16842-------------------------

begin tran
SET IDENTITY_INSERT dbo.tb_funcionalidade on
INSERT INTO tb_funcionalidade (
  id_funcionalidade,
  st_funcionalidade,
  id_funcionalidadepai,
  bl_ativo,
  id_situacao,
  st_classeflex,
  st_urlicone,
  id_tipofuncionalidade,
  nu_ordem,
  st_classeflexbotao,
  bl_pesquisa,
  bl_lista,
  id_sistema,
  st_ajuda,
  st_target,
  st_urlcaminho,
  bl_visivel,
  bl_relatorio,
  st_urlcaminhoedicao
)
VALUES (
  728,
  'Perguntas Frequentes',
  null,
  1,
  123,
  '/perguntas-frequentes',
  '/img/ico/money--arrow.png',
  1,
  NULL,
  'ajax',
  1,
  0,
  4,
  NULL,
  NULL,
  '728',
  1,
  0,
  NULL
);

SET IDENTITY_INSERT dbo.tb_funcionalidade off
COMMIT

----------------------FIN-17063-------------------------

begin tran
SET IDENTITY_INSERT dbo.tb_funcionalidade on
INSERT INTO tb_funcionalidade (
  id_funcionalidade,
  st_funcionalidade,
  id_funcionalidadepai,
  bl_ativo,
  id_situacao,
  st_classeflex,
  st_urlicone,
  id_tipofuncionalidade,
  nu_ordem,
  st_classeflexbotao,
  bl_pesquisa,
  bl_lista,
  id_sistema,
  st_ajuda,
  st_target,
  st_urlcaminho,
  bl_visivel,
  bl_relatorio,
  st_urlcaminhoedicao
)
VALUES (
  724,
  'Perguntas Frequentes',
  301,
  1,
  123,
  'br.com.ead1.gestor2.view.conteudo.entidade.cadastrar.PerguntasFrequentes',
  '/img/ico/money--arrow.png',
  3,
  NULL,
  'PerguntasFrequentes',
  1,
  0,
  1,
  NULL,
  NULL,
  '/perguntas-frequentes',
  1,
  0,
  NULL
);

SET IDENTITY_INSERT dbo.tb_funcionalidade off
COMMIT

--------- [dbo].[tb_funcionalidade] ---------
begin tran
update tb_funcionalidade set bl_delete = 1 where id_funcionalidade = 724
commit
--------- FIM [dbo].[tb_funcionalidade] ---------

--------- [dbo].[tb_categoriapergunta] ---------
CREATE TABLE [dbo].[tb_esquemapergunta] (
  [id_esquemapergunta] int NOT NULL IDENTITY(1,1) ,
  [st_esquemapergunta] varchar(100) NOT NULL ,
  [dt_cadastro] datetime2 NULL ,
  [bl_ativo] bit NULL ,
  PRIMARY KEY ([id_esquemapergunta])
)

ALTER TABLE [dbo].[tb_esquemapergunta] ADD DEFAULT getDate() FOR [dt_cadastro]

ALTER TABLE [dbo].[tb_esquemapergunta] ADD DEFAULT 1 FOR [bl_ativo]

ALTER TABLE [dbo].[tb_esquemapergunta]
ADD [id_entidadecadastro] int NOT NULL

ALTER TABLE [dbo].[tb_esquemapergunta] ADD CONSTRAINT [FK_tb_esquemapergunta_tb_entidade] FOREIGN KEY ([id_entidadecadastro]) REFERENCES [dbo].[tb_entidade] ([id_entidade]) ON DELETE NO ACTION ON UPDATE NO ACTION


--------- FIM [dbo].[tb_esquemapergunta] ---------

--------- [dbo].[tb_categoriapergunta] ---------
CREATE TABLE [dbo].[tb_categoriapergunta] (
  [id_categoriapergunta] int NOT NULL IDENTITY(1,1) ,
  [st_categoriapergunta] varchar(100) NOT NULL ,
  [bl_ativo] bit NULL ,
  [dt_cadastro] datetime2 NULL ,
  PRIMARY KEY ([id_categoriapergunta])
)

ALTER TABLE [dbo].[tb_categoriapergunta]
ADD [id_esquemapergunta] int NOT NULL

ALTER TABLE [dbo].[tb_categoriapergunta] ADD CONSTRAINT [FK_tb_esquemapergunta_tb_categoriapergunta] FOREIGN KEY ([id_esquemapergunta]) REFERENCES [dbo].[tb_esquemapergunta] ([id_esquemapergunta]) ON DELETE NO ACTION ON UPDATE NO ACTION

ALTER TABLE [dbo].[tb_categoriapergunta] ADD DEFAULT getDate() FOR [dt_cadastro]

ALTER TABLE [dbo].[tb_categoriapergunta] ADD DEFAULT 1 FOR [bl_ativo]

ALTER TABLE [dbo].[tb_categoriapergunta]
ADD [id_entidadecadastro] int NOT NULL

ALTER TABLE [dbo].[tb_categoriapergunta] ADD CONSTRAINT [FK_tb_categoriapergunta_tb_entidade] FOREIGN KEY ([id_entidadecadastro]) REFERENCES [dbo].[tb_entidade] ([id_entidade]) ON DELETE NO ACTION ON UPDATE NO ACTION



--------- FIM [dbo].[tb_categoriapergunta] ---------



--------- [dbo].[tb_pergunta] ---------
CREATE TABLE [dbo].[tb_pergunta] (
  [id_pergunta] int NOT NULL IDENTITY(1,1),
  [st_pergunta] text NOT NULL ,
  [st_resposta] text NOT NULL ,
  [bl_ativo] bit NULL ,
  [dt_cadastro] datetime2 NULL ,
  [id_categoriapergunta] int NULL ,
  PRIMARY KEY ([id_pergunta]),
  CONSTRAINT [FK_tb_pergunta_tb_categoriapergunta] FOREIGN KEY ([id_categoriapergunta]) REFERENCES [dbo].[tb_categoriapergunta] ([id_categoriapergunta]) ON DELETE NO ACTION ON UPDATE NO ACTION
)


ALTER TABLE [dbo].[tb_pergunta] ADD DEFAULT getDate() FOR [dt_cadastro]

ALTER TABLE [dbo].[tb_pergunta] ADD DEFAULT 1 FOR [bl_ativo]

ALTER TABLE [dbo].[tb_pergunta]
ADD [id_entidadecadastro] int NOT NULL

ALTER TABLE [dbo].[tb_pergunta] ADD CONSTRAINT [FK_tb_entidade_tb_pergunta] FOREIGN KEY ([id_entidadecadastro]) REFERENCES [dbo].[tb_entidade] ([id_entidade]) ON DELETE NO ACTION ON UPDATE NO ACTION

----------------------FIN-17064-------------------------

ALTER TABLE [dbo].[tb_entidade]
ADD [id_esquemapergunta] int NULL

ALTER TABLE [dbo].[tb_entidade] ADD CONSTRAINT [tb_usuario_tb_esquemapergunta] FOREIGN KEY ([id_esquemapergunta]) REFERENCES [dbo].[tb_esquemapergunta] ([id_esquemapergunta]) ON DELETE NO ACTION ON UPDATE NO ACTION


----------------------FIN-17139-------------------------

BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_permissao on
insert into tb_permissao (id_permissao, st_nomepermissao,st_descricao,bl_ativo) values(
  29,'Imprimir Contrato','Permite imprimir o contrato.',1
)
SET IDENTITY_INSERT dbo.tb_permissao off
commit

insert into tb_permissaofuncionalidade (id_funcionalidade, id_permissao) values (275,29);

--------------------INSERT FUNCIONALIDADES---------------------------

begin transaction

insert into [tb_entidadefuncionalidade] (id_entidade, id_funcionalidade, bl_ativo,id_situacao)
  (select 2,id_funcionalidade, 1,5 from tb_funcionalidade where id_funcionalidade not in (select id_funcionalidade from tb_entidadefuncionalidade where id_entidade = 2))

insert into [tb_perfilfuncionalidade] (id_perfil, id_funcionalidade, bl_ativo)
  (select 3,id_funcionalidade, 3 from tb_funcionalidade where id_funcionalidade not in (select id_funcionalidade from tb_perfilfuncionalidade where id_perfil = 3)AND id_tipofuncionalidade !=9)

insert into [tb_perfilentidade] (id_perfil, id_entidade)
  select 3,id_entidade from tb_entidade where id_entidade not in
                                              (select id_entidade from tb_perfilentidade where id_perfil = 3)

insert into [tb_perfilentidade] (id_perfil, id_entidade)
  select 1,id_entidade from tb_entidade where id_entidade not in
                                              (select id_entidade from tb_perfilentidade where id_perfil = 1)


insert into [tb_perfilfuncionalidade] (id_perfil, id_funcionalidade, bl_ativo)
  (select 1,id_funcionalidade, 3 from tb_funcionalidade where id_funcionalidade not in (select id_funcionalidade from tb_perfilfuncionalidade where id_perfil = 1))

insert into [tb_entidadefuncionalidade] (id_entidade, id_funcionalidade, bl_ativo,id_situacao)
  (select 14,id_funcionalidade, 1,5 from tb_funcionalidade where id_funcionalidade not in (select id_funcionalidade from tb_entidadefuncionalidade where id_entidade = 14))

insert into [tb_perfilfuncionalidade] (id_perfil, id_funcionalidade, bl_ativo)
  (select 3,id_funcionalidade, 3 from tb_funcionalidade where id_funcionalidade not in (select id_funcionalidade from tb_perfilfuncionalidade where id_perfil = 3))
insert into tb_permissaofuncionalidade (id_funcionalidade, id_permissao)
  select fc.id_funcionalidade, pm.id_permissao from tb_funcionalidade fc, tb_permissao pm where fc.id_funcionalidade not in (select id_funcionalidade from tb_permissaofuncionalidade)
                                                                                                and fc.id_tipofuncionalidade not in (1,4,5,6) and pm.id_permissao in (1,2,3) AND fc.id_sistema = 1

insert into tb_perfilpermissaofuncionalidade (id_funcionalidade, id_perfil,id_permissao)
  select pf.id_funcionalidade, 1 , pf.id_permissao from tb_permissaofuncionalidade pf where pf.id_funcionalidade not in (select id_funcionalidade from tb_perfilpermissaofuncionalidade)

commit
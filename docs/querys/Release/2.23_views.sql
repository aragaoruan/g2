CREATE VIEW [dbo].[vw_alunosaptosagendamento] AS (

SELECT
	DISTINCT
	mt.st_nomecompleto,
	mt.id_matricula,
	mt.id_projetopedagogico,

	CASE
	WHEN ava_rec.id_tipodeavaliacao > 0 THEN 'RECUPERAÇÃO'
	ELSE 'PROVA FINAL'
	END AS 'st_avaliacao'

	FROM dbo.vw_alunosagendamento AS alag
	JOIN rel.vw_matricula AS mt ON mt.id_matricula = alag.id_matricula AND mt.id_situacaoagendamento = 120
	LEFT JOIN dbo.vw_avaliacaoagendamento AS avag ON avag.id_matricula = alag.id_matricula AND avag.id_avaliacaoagendamento IN (SELECT TOP 1 id_avaliacaoagendamento FROM dbo.vw_avaliacaoagendamento AS ava WHERE ava.id_matricula = avag.id_matricula ORDER BY id_tipodeavaliacao DESC, id_chamada DESC)
	OUTER APPLY (
	SELECT
	CASE
		WHEN nu_presenca IS NULL THEN id_tipodeavaliacao
		ELSE id_tipodeavaliacao + 1
	END AS id_tipodeavaliacao
	) AS ava_rec
)
GO
ALTER VIEW vw_gradenota
AS
    SELECT DISTINCT
            sa.id_saladeaula ,
            sa.dt_abertura ,
            pp.id_projetopedagogico ,
            pp.st_projetopedagogico ,
            sa.st_saladeaula ,
            mn.st_notatcc ,
            mn.id_tiponotatcc ,
            mn.st_notaead ,
            mn.id_tiponotaead ,
            mn.st_notaatividade ,
            mn.id_tiponotaatividade ,
            mn.st_notafinal ,
            mn.id_tiponotafinal ,
            mn.id_avaliacaoatividade ,
            dc.id_disciplina ,
            dc.st_disciplina ,
            dc.id_tipodisciplina ,
            dc.st_tituloexibicao AS st_tituloexibicaodisciplina ,
            mt.id_matricula ,
            md.id_matriculadisciplina ,
			md.id_aproveitamento,
            acr.id_avaliacaoconjuntoreferencia ,
            CAST (( md.nu_aprovafinal * pp.nu_notamaxima / 100 ) AS INT) AS nu_notafinal ,
            ev.id_evolucao ,
            ev.st_evolucao ,
            st.id_situacao ,
            st.st_situacao ,
            dc.nu_cargahoraria ,
            sa.dt_encerramento ,
            CAST ((mn.nu_notatotal) AS INT) AS nu_notatotal,
            sa.id_categoriasala ,
			ap.dt_cadastro AS dt_cadastroaproveitamento,
			ap.id_usuario AS id_usuarioaproveitamento,
			u.st_nomecompleto AS st_nomeusuarioaproveitamento,
			md.st_disciplinaoriginal,
			-- AO ATUALIZAR AS STRINGS DO st_status, ATUALIZAR O ARQUIVO G2\Constante\StatusGradeNota.php
            st_status = CASE WHEN ea.id_alocacao IS NOT NULL
                                  AND md.id_evolucao = 12 THEN ev.st_evolucao
                             WHEN ea.id_alocacao IS NOT NULL
                                  AND md.id_evolucao = 19 THEN ev.st_evolucao
                             WHEN ea.id_alocacao IS NOT NULL
                                  AND md.id_situacao = 65
                             THEN 'Crédito Concedido'
                             WHEN ea.id_alocacao IS NOT NULL
                                  AND ( mn.nu_notatotal + nf.nu_notafaltante ) >= ( pp.nu_notamaxima
                                                              * pp.nu_percentualaprovacao
                                                              / 100 )
                             THEN 'Satisfatório'
                             WHEN ea.id_alocacao IS NOT NULL
                                  AND ( mn.nu_notatotal + nf.nu_notafaltante ) < ( pp.nu_notamaxima
                                                              * pp.nu_percentualaprovacao
                                                              / 100 )
                             THEN 'Insatisfatório'
							 WHEN md.id_aproveitamento IS NOT NULL
                                  AND md.id_situacao = 65
                             THEN 'Aproveitamento de Disciplina'
                             WHEN ea.id_alocacao IS NULL
                                  AND ea.id_encerramentosala IS NULL
                             THEN 'Não encerrado'

                             ELSE '-'
                        END ,
            bl_status = CASE WHEN md.bl_obrigatorio = 0 THEN 1
                             WHEN ( mn.nu_notatotal
                                    + ISNULL(nf.nu_notafaltante, 0) ) >= ( pp.nu_notamaxima
                                                              * pp.nu_percentualaprovacao
                                                              / 100 ) THEN 1
                             WHEN ( mn.nu_notatotal
                                    + ISNULL(nf.nu_notafaltante, 0) ) < ( pp.nu_notamaxima
                                                              * pp.nu_percentualaprovacao
                                                              / 100 ) THEN 0
                             ELSE 0
                        END ,
            bl_complementar = CASE WHEN ( md.id_matricula != md.id_matriculaoriginal )
                                        AND ppcompl.bl_disciplinacomplementar = 1
                                   THEN 1
                    ELSE 0
                              END ,
            DATEADD(DAY, ISNULL(sa.nu_diasextensao, 0), sa.dt_encerramento) AS dt_encerramentoextensao,
            al.dt_inicio,
			st_statusdisciplina = CASE WHEN al.id_alocacao IS NULL
									AND md.id_situacao != 65
                                  THEN 'Não Iniciada'
                             WHEN ea.id_alocacao IS NOT NULL
                                  AND md.id_evolucao = 12
								  AND mn.bl_aprovado = 1
								   THEN 'Aprovada'
							 WHEN ea.id_alocacao IS NOT NULL
								  AND mn.bl_aprovado = 0
								  AND md.id_evolucao = 19
                             THEN 'Reprovado'
							 WHEN md.id_aproveitamento IS NOT NULL
                                  AND md.id_situacao = 65
                             THEN 'Isento'
                             WHEN al.id_alocacao IS NOT NULL
								  AND md.id_aproveitamento IS NULL
                                  AND( md.nu_aprovafinal IS NULL OR ea.id_alocacao IS NULL)
								  AND ( (md.id_situacao = 13 AND (mn.st_notaead IS NULL OR mn.st_notaatividade IS NULL OR mn.st_notafinal IS NULL AND mn.st_notarecuperacao IS NULL))
										OR  (md.id_situacao != 13 AND ea.id_alocacao IS NULL) )
                             THEN 'Cursando'


                             ELSE '-'
                        END ,
				mn.id_tiponotarecuperacao,
				mn.st_notarecuperacao,
				mn.st_avaliacaoatividade,
				mn.st_avaliacaoead,
				mn.st_avaliacaofinal,
				mn.st_avaliacaorecuperacao

    FROM    dbo.tb_matriculadisciplina AS md
            JOIN dbo.tb_matricula AS mt ON md.id_matricula = mt.id_matricula
            JOIN dbo.tb_disciplina AS dc ON dc.id_disciplina = md.id_disciplina
                                            AND dc.id_tipodisciplina <> 3
            JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
            LEFT JOIN dbo.tb_alocacao AS al ON al.id_matriculadisciplina = md.id_matriculadisciplina
                                               AND al.bl_ativo = 1
            LEFT JOIN dbo.tb_avaliacaoconjuntoreferencia AS acr ON acr.id_saladeaula = al.id_saladeaula --AND acr.dt_fim IS NOT null
            LEFT JOIN tb_encerramentoalocacao AS ea ON ea.id_alocacao = al.id_alocacao
            LEFT JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula
            JOIN tb_evolucao AS ev ON ev.id_evolucao = md.id_evolucao
            JOIN dbo.tb_situacao AS st ON st.id_situacao = md.id_situacao
            LEFT JOIN dbo.vw_matriculanota AS mn ON md.id_matriculadisciplina = mn.id_matriculadisciplina
                                                    AND mn.id_alocacao = al.id_alocacao
            OUTER APPLY ( SELECT    id_matricula ,
                                    id_disciplina ,
                                    SUM(CAST(nu_notamax AS FLOAT)) AS nu_notafaltante
                          FROM      vw_avaliacaoaluno
                          WHERE     bl_ativo IS NULL
                                    AND id_matricula = mt.id_matricula
                                    AND id_disciplina = md.id_disciplina
                                    AND id_tipoprova = 2
									AND id_avaliacaorecupera = 0

                          GROUP BY  id_disciplina ,
                                    id_matricula
                        ) AS nf
            JOIN tb_matricula AS matcompl ON matcompl.id_matricula = md.id_matriculaoriginal
            JOIN tb_projetopedagogico AS ppcompl ON ppcompl.id_projetopedagogico = matcompl.id_projetopedagogico
			LEFT JOIN dbo.tb_aproveitamento AS ap ON ap.id_aproveitamento = md.id_aproveitamento
			LEFT JOIN tb_usuario AS u ON u.id_usuario = ap.id_usuario
go

ALTER VIEW [dbo].[vw_pessoa]
AS
    SELECT  u.id_usuario ,
            u.st_login ,
            CASE WHEN uss.id_dadosacesso IS NULL THEN u.st_login
                 ELSE uss.st_login
            END AS st_loginentidade ,
            u.st_nomecompleto ,
            u.st_cpf ,
            u.id_registropessoa ,
            u.st_senha ,
            CASE WHEN uss.id_dadosacesso IS NULL
                 THEN CAST(u.st_senha AS VARCHAR)
                 ELSE CAST(uss.st_senha AS VARCHAR)
            END AS st_senhaentidade ,
            p.id_entidade ,
            p.st_sexo ,
            p.dt_nascimento ,
            uf.st_uf as st_ufnascimento,
            p.sg_ufnascimento AS sg_ufnascimento ,
            mun.st_nomemunicipio as st_municipionascimento,
            p.id_municipionascimento AS id_municipionascimento ,
            p.st_cidade AS st_cidadenascimento ,
            p.id_estadocivil ,
            ec.st_estadocivil ,
            cepp.id_email ,
            ce.st_email ,
            cepp.bl_padrao AS bl_emailpadrao ,
            p.st_nomemae ,
            p.st_nomepai ,
            tel1.id_telefone ,
            1 AS bl_telefonepadrao ,
            tel1.nu_telefone ,
            tel1.id_tipotelefone ,
            tel1.st_tipotelefone ,
            tel1.nu_ddd ,
            tel1.nu_ddi ,
            tel2.id_telefone AS id_telefonealternativo ,
            tel2.nu_telefone AS nu_telefonealternativo ,
            tel2.id_tipotelefone AS id_tipotelefonealternativo ,
            tel2.st_tipotelefone AS st_tipotelefonealternativo ,
            tel2.nu_ddd AS nu_dddalternativo ,
            tel2.nu_ddi AS nu_ddialternativo ,
            di.st_rg ,
            di.st_orgaoexpeditor ,
            di.dt_dataexpedicao ,
            pe.id_endereco ,
            1 AS bl_enderecopadrao ,
            pe.st_endereco ,
            pe.st_cep ,
            pe.st_bairro ,
            pe.st_complemento ,
            pe.nu_numero ,
            pe.st_estadoprovincia ,
            CASE WHEN pe.st_cidade IS NOT NULL THEN pe.st_cidade
                 ELSE pe.st_nomemunicipio
            END AS st_cidade ,
            pe.id_pais ,
            pe.id_municipio ,
            pe.id_tipoendereco ,
            pe.sg_uf ,
            pe.st_nomepais ,
            pe.st_nomemunicipio ,
            p.st_identificacao ,
            p.st_urlavatar,
			tbti.id_titulacao,
			tbti.st_titulacao
    FROM    tb_usuario AS u
            INNER JOIN tb_pessoa AS p ON p.id_usuario = u.id_usuario
                                         AND p.bl_ativo = 1
            LEFT JOIN tb_dadosacesso AS uss ON u.id_usuario = uss.id_usuario
                                               AND uss.bl_ativo = 1
                                               AND uss.id_entidade = p.id_entidade
                                               AND uss.id_entidade = p.id_entidade
            LEFT JOIN tb_contatosemailpessoaperfil AS cepp ON cepp.id_usuario = u.id_usuario
                                                              AND cepp.id_entidade = p.id_entidade
                                                              AND cepp.bl_ativo = 1
                                                              AND cepp.bl_padrao = 1
            LEFT JOIN tb_contatosemail AS ce ON ce.id_email = cepp.id_email
            OUTER APPLY ( SELECT TOP 1
                                    ct2.id_telefone ,
                                    ct2.id_tipotelefone ,
                                    ct2.nu_ddd ,
                                    ct2.nu_ddi ,
                                    ct2.nu_telefone ,
                                    tt2.st_tipotelefone
                          FROM      tb_contatostelefonepessoa AS ctp2
                                    LEFT JOIN tb_contatostelefone AS ct2 ON ct2.id_telefone = ctp2.id_telefone
                                    LEFT JOIN tb_tipotelefone AS tt2 ON tt2.id_tipotelefone = ct2.id_tipotelefone
                          WHERE     ctp2.id_usuario = u.id_usuario
                                    AND ctp2.id_entidade = p.id_entidade
                                    AND ctp2.bl_padrao = 1
                        ) AS tel1
            OUTER APPLY ( SELECT TOP 1
                                    ct2.id_telefone ,
                                    ct2.id_tipotelefone ,
                                    ct2.nu_ddd ,
                                    ct2.nu_ddi ,
                                    ct2.nu_telefone ,
                                    tt2.st_tipotelefone
                          FROM      tb_contatostelefonepessoa AS ctp2
                                    LEFT JOIN tb_contatostelefone AS ct2 ON ct2.id_telefone = ctp2.id_telefone
                                    LEFT JOIN tb_tipotelefone AS tt2 ON tt2.id_tipotelefone = ct2.id_tipotelefone
                          WHERE     ctp2.id_usuario = u.id_usuario
                                    AND ctp2.id_entidade = p.id_entidade
                                    AND ctp2.bl_padrao = 0
                        ) AS tel2
            OUTER APPLY ( SELECT TOP 1
                                    e1.* ,
                                    mu.st_nomemunicipio ,
                                    pa.st_nomepais
                          FROM      tb_pessoaendereco AS pe1
                                    LEFT JOIN tb_endereco AS e1 ON e1.id_endereco = pe1.id_endereco
                                                              AND e1.bl_ativo = 1
                                    LEFT JOIN tb_municipio AS mu ON mu.id_municipio = e1.id_municipio
                                    LEFT JOIN dbo.tb_pais AS pa ON pa.id_pais = e1.id_pais
                          WHERE     pe1.id_usuario = u.id_usuario
                                    AND pe1.id_entidade = p.id_entidade
                                    AND pe1.bl_padrao = 1
                        ) AS pe

            LEFT JOIN tb_estadocivil AS ec ON ec.id_estadocivil = p.id_estadocivil
            LEFT JOIN dbo.tb_documentoidentidade AS di ON di.id_usuario = u.id_usuario
                                                          AND di.id_entidade = p.id_entidade
			LEFT JOIN dbo.tb_titulacao as tbti ON tbti.id_titulacao = u.id_titulacao
      LEFT JOIN dbo.tb_municipio as mun on p.id_municipionascimento = mun.id_municipio
      LEFT JOIN dbo.tb_uf as uf  on p.sg_ufnascimento = uf.sg_uf
go

alter VIEW [dbo].[vw_gerardeclaracao]
AS
    SELECT DISTINCT
            mt.id_matricula ,
            ps.id_usuario AS id_usuarioaluno ,
            ps.st_nomecompleto AS st_nomecompletoaluno ,
            ps.st_login AS st_loginaluno ,
            ps.st_senhaentidade AS st_senhaaluno ,
            ps.st_cpf AS st_cpfaluno ,
            ps.st_rg AS st_rgaluno ,
            ps.st_orgaoexpeditor ,
            ps.dt_dataexpedicao AS dt_dataexpedicaoaluno ,
            ps.st_nomemae AS st_filiacao ,
            ps.st_nomepai AS st_pai,
            ps.st_ufnascimento,
            ps.st_municipionascimento,
            ps.id_pais,
            dc.st_nomeinstituicao,
            dc.dt_anoconclusao AS ano_conclusao_medio,
            dcm.st_nomemunicipio as st_municipioinstituicao,
            CONVERT(VARCHAR(4), DATEPART(YEAR, mt.dt_concluinte), 103) as ano_conclusao,
            semestre_conclusao = (
                                  CASE WHEN
                                    CONVERT(VARCHAR(2), DATEPART(MONTH, mt.dt_concluinte), 103)<7
                                    THEN 1
                                  WHEN CONVERT(VARCHAR(2), DATEPART(MONTH, mt.dt_concluinte), 103)>=7
                                    THEN 2
                                  ELSE NULL
                                  END),
            mc.dt_cadastro AS dt_expedicaocertificado,
            CONVERT(CHAR, mt.dt_cadastro, 103) AS st_datamatricula ,
            canc.id_cancelamento ,
            canc.st_observacao AS st_observacaocancelamento ,
            canc.st_observacaocalculo AS st_observacao_calculo ,
            CONVERT(CHAR, canc.dt_solicitacao, 103) AS st_solicitacaocancelamento ,
            canc.nu_valorcarta AS st_valorcredito ,
            CONVERT(CHAR, DATEADD(D, 365, canc.dt_solicitacao), 103) AS st_validadecredito ,
            cm.id_contrato ,
            ps.dt_nascimento AS dt_nascimentoaluno ,
            UPPER(SUBSTRING(ps.st_nomemunicipio, 1, 1))
            + LOWER(SUBSTRING(ps.st_nomemunicipio, 2, 499)) AS st_municipioaluno ,
            ps.st_estadoprovincia AS st_ufaluno ,
            ps.sg_uf AS sg_ufaluno ,
            ps.nu_ddd AS nu_dddaluno ,
            ps.nu_telefone AS nu_telefonealuno ,
            ps.st_email AS st_emailaluno ,
            pp.st_tituloexibicao AS st_projeto ,
	--pp.nu_cargahoraria,
            ( SELECT    SUM(dis.nu_cargahoraria) AS nu_cargahoraria
              FROM      tb_matriculadisciplina md
                        JOIN tb_matricula m ON m.id_matricula = md.id_matricula
                        JOIN tb_disciplina dis ON dis.id_disciplina = md.id_disciplina
              WHERE     md.id_matricula = mt.id_matricula
            ) AS nu_cargahoraria ,
            ne.st_nivelensino ,
            mt.dt_concluinte AS dt_concluintealuno ,
            us_coordenador.st_nomecompleto AS st_coordenadorprojeto ,
            fl.id_fundamentolegal AS id_fundamentolei ,
            fl.nu_numero AS nu_lei ,
            flr.id_fundamentolegal AS id_fundamentoresolucao ,
            tur.id_turma AS id_turma ,
            tur.st_turma ,
            tur.dt_inicio AS dt_inicioturma ,
            tur.dt_fim AS dt_terminoturma ,
            flr.nu_numero AS nu_resolucao ,
            flpr.id_fundamentolegal AS id_fundamentoparecer ,
            flpr.nu_numero AS nu_parecer ,
            flp.id_fundamentolegal AS id_fundamentoportaria ,
            flp.nu_numero AS nu_portaria ,
            et.id_entidade ,
            et.st_nomeentidade ,
            et.st_razaosocial ,
            et.st_cnpj ,
            et.st_urlimglogo ,
            et.st_urlsite ,
            ps.st_nomepais ,
            ps.st_sexo AS sexo_aluno ,
            UPPER(tcc.st_tituloavaliacao) AS st_titulomonografia ,
            CONVERT(VARCHAR(2), DATEPART(DAY, GETDATE()), 103) + ' de '
            + dbo.fn_mesporextenso(GETDATE()) + ' de '
            + CONVERT(VARCHAR(4), DATEPART(YEAR, GETDATE()), 103) AS st_data ,
       DAY(GETDATE()) AS st_dia ,
            LOWER(dbo.fn_mesporextenso(GETDATE())) AS st_mes ,
            YEAR(GETDATE()) AS st_ano ,

            --CONVERT(DATE, sa.dt_abertura, 103) AS dt_primeirasala ,
            (
                SELECT TOP 1 ts.dt_abertura
                FROM tb_matriculadisciplina tm
                JOIN tb_alocacao ta ON tm.id_matriculadisciplina = ta.id_matriculadisciplina
                JOIN tb_saladeaula ts ON ta.id_saladeaula = ts.id_saladeaula
                WHERE tm.id_matricula = mt.id_matricula AND tm.bl_obrigatorio = 1
                ORDER BY ts.dt_abertura ASC
            ) AS dt_primeirasala,

            --CONVERT(DATE, DATEADD(MONTH, 13, sa.dt_abertura), 103) AS dt_previsaofim ,
            (
                SELECT TOP 1
                --DATEADD(MONTH, 13, ts.dt_abertura)

                'dt_previsaotermino' = CASE WHEN ts.id_categoriasala = 2 THEN (
                    DATEADD(day, ISNULL(ts.nu_diasaluno, 0), ts.dt_abertura)
                ) ELSE (
                    CASE WHEN ts.dt_encerramento IS NOT NULL THEN (
                        DATEADD(day, ISNULL(ts.nu_diasextensao, 0) + 30, ts.dt_encerramento)
                    ) WHEN ts.dt_abertura IS NOT NULL THEN (
                        DATEADD(day, ISNULL(ts.nu_diasextensao, 0) + 30, ts.dt_abertura)
                    ) ELSE (
                        NULL
                    ) END
                ) END

                FROM tb_matriculadisciplina tm
                JOIN tb_alocacao ta ON tm.id_matriculadisciplina = ta.id_matriculadisciplina
                JOIN tb_saladeaula ts ON ta.id_saladeaula = ts.id_saladeaula
                WHERE tm.id_matricula = mt.id_matricula AND tm.bl_obrigatorio = 1
                ORDER BY dt_previsaotermino DESC, ts.dt_abertura DESC, ts.dt_encerramento DESC
            ) AS dt_previsaofim,

            UPPER(tcc.st_tituloavaliacao) AS st_titulotcc,
            tcc.dt_aberturasala AS dt_iniciotcc,
            tcc.dt_encerramentosala AS dt_terminotcc,
            tcc.st_nota AS nu_notatcc,
            tcc.dt_cadastroavaliacao AS dt_cadastrotcc,
            tcc.dt_defesa AS dt_defesatcc,

            mt.st_codcertificacao ,
            CONVERT(CHAR, GETDATE(), 103) AS st_atual
			, matc.st_codigoacompanhamento AS st_codigorasteamento,

			mt.id_matriculaorigem,
			mto.dt_inicio AS dt_iniciomatriculaorigem
			, CONVERT(VARCHAR(2), DATEPART(day, GetDate()))     + ' de ' +  dbo.fn_mesporextenso(GetDate())   + ' de ' +  CONVERT(VARCHAR(4), DATEPART(year, GetDate())) as st_atualextenso,
			 CONVERT(VARCHAR(2), DATEPART(day, tur.dt_fim))     + ' de ' +  dbo.fn_mesporextenso(tur.dt_fim)   + ' de ' +  CONVERT(VARCHAR(4), DATEPART(year, tur.dt_fim)) as st_terminoturmaextenso,
			 CONVERT(VARCHAR(2), DATEPART(day, tur.dt_inicio))     + ' de ' +  dbo.fn_mesporextenso(tur.dt_inicio)   + ' de ' +  CONVERT(VARCHAR(4), DATEPART(year, tur.dt_inicio)) as st_inicioturmaextenso

			, CONVERT(CHAR, venda.dt_limiterenovacao, 103) AS st_limiterenovacao
			, venda.id_venda AS id_venda
			, venda.nu_mensalidade AS nu_mensalidade
			, venda.nu_mensalidadepon AS nu_mensalidadepontualidade
			, ' - ' AS st_link_grade_portal
			, ' - ' AS st_municipioufagendamento
			, ac.st_areaconhecimento

    FROM    tb_matricula mt
            JOIN tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
            JOIN tb_entidade AS et ON et.id_entidade = mt.id_entidadeatendimento
            LEFT JOIN tb_entidadeendereco AS ete ON ete.id_entidade = et.id_entidade
                                                    AND ete.bl_padrao = 1
            LEFT JOIN tb_endereco AS etee ON etee.id_endereco = ete.id_endereco
            JOIN vw_pessoa AS ps ON ps.id_usuario = mt.id_usuario
                                    AND ps.id_entidade = mt.id_entidadeatendimento
            LEFT JOIN tb_fundamentolegal AS fl ON fl.id_entidade = mt.id_entidadematriz
                                                  AND fl.id_tipofundamentolegal = 1
                                                AND fl.dt_publicacao <= GETDATE()
                                                  AND fl.dt_vigencia >= GETDATE()
            LEFT JOIN tb_fundamentolegal AS flp ON flp.id_entidade = mt.id_entidadematriz
                                                   AND flp.id_tipofundamentolegal = 2
                                                   AND flp.dt_publicacao <= GETDATE()
                                                   AND flp.dt_vigencia >= GETDATE()
            LEFT JOIN tb_fundamentolegal AS flr ON flr.id_entidade = mt.id_entidadematriz
                                                   AND flr.id_tipofundamentolegal = 3
          AND flr.dt_publicacao <= GETDATE()
                                                   AND flr.dt_vigencia >= GETDATE()
            LEFT JOIN tb_fundamentolegal AS flpr ON flpr.id_entidade = mt.id_entidadematriz
                                                    AND flpr.id_tipofundamentolegal = 4
                                                    AND flpr.dt_publicacao <= GETDATE()
                                                    AND flpr.dt_vigencia >= GETDATE()
            LEFT JOIN tb_projetopedagogicoserienivelensino AS psn ON psn.id_projetopedagogico = mt.id_projetopedagogico
            LEFT JOIN tb_turma AS tur ON tur.id_turma = mt.id_turma
            JOIN tb_nivelensino AS ne ON ne.id_nivelensino = psn.id_nivelensino
            LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS uper ON uper.id_projetopedagogico = pp.id_projetopedagogico
                                                              AND uper.bl_ativo = 1
                                                              AND uper.bl_titular = 1
            LEFT JOIN dbo.tb_usuario AS us_coordenador ON us_coordenador.id_usuario = uper.id_usuario

            LEFT JOIN tb_matricula AS mto ON mt.id_matriculaorigem = mto.id_matricula

            LEFT JOIN dbo.vw_avaliacaoaluno tcc ON ( tcc.id_matricula = mt.id_matricula
                                                    AND tcc.id_tipoavaliacao = 6
                                                    AND tcc.st_tituloavaliacao IS NOT NULL
                                                    AND tcc.id_tipodisciplina = 2
                                                    AND tcc.bl_ativo = 1
                                                  )
            LEFT JOIN tb_cancelamento AS canc ON canc.id_cancelamento = mt.id_cancelamento AND mt.id_evolucao = 27
            LEFT JOIN tb_contratomatricula AS cm ON cm.id_matricula = mt.id_matricula
			LEFT JOIN tb_matriculacertificacao AS matc ON matc.id_matricula = mt.id_matricula
			LEFT JOIN tb_areaprojetopedagogico AS app ON pp.id_projetopedagogico = app.id_projetopedagogico
			LEFT JOIN tb_areaconhecimento AS ac ON ac.id_areaconhecimento = app.id_areaconhecimento
      LEFT JOIN tb_pessoadadoscomplementares as dc on dc.id_usuario = ps.id_usuario
      LEFT JOIN tb_matriculacertificacao as mc on mt.id_matricula = mc.id_matricula
			LEFT JOIN tb_municipio as dcm on dc.id_municipioinstituicao = dcm.id_municipio
			OUTER APPLY (SELECT TOP 1 v.id_venda , v.dt_limiterenovacao ,
							format((vp.nu_valorbruto - ((cm.nu_valordesconto / 100 ) * vp.nu_valorbruto))/6, '#.00') AS nu_mensalidade,
							format((((vp.nu_valorbruto - ((cm.nu_valordesconto / 100 ) * vp.nu_valorbruto))/6) - (((pon.nu_valordesconto / 100 ) * ((vp.nu_valorbruto - ((cm.nu_valordesconto / 100 ) * vp.nu_valorbruto))/6)))), '#.00') AS nu_mensalidadepon

							FROM tb_vendaproduto AS vp
							JOIN tb_venda AS v ON v.id_venda = vp.id_venda AND v.dt_limiterenovacao IS NOT NULL
							AND vp.id_matricula = mt.id_matricula AND v.id_evolucao in (7, 9) AND v.bl_ativo = 1
							LEFT JOIN tb_campanhacomercial AS cm ON cm.id_campanhacomercial = vp.id_campanhacomercial
							LEFT JOIN tb_campanhacomercial as pon ON pon.id_campanhacomercial = v.id_campanhapontualidade

							ORDER BY v.id_venda DESC) AS venda


GO





go
      CREATE PROCEDURE [dbo].[sp_matricula_turma]
      @id_entidade INT = NULL,
      @id_turma INT = NULL ,
      @dt_datainformada DATE,
      @id_evolucao INT = NULL,
      @id_entidadepai INT

      AS


      BEGIN
      SET NOCOUNT ON;

      DECLARE @SQLQuery VARCHAR(MAX)

      SET @SQLQuery = 'SELECT  DISTINCT

      		p.id_produto,
      		p.st_produto ,
      		e.id_entidade,
      		e.id_entidadecadastro,
      		e.st_nomeentidade,
      		pp.id_projetopedagogico,
      		pp.st_projetopedagogico,
      		ppp.nu_cargahoraria,
      		t.id_turma,
      		t.st_turma,
      		ac.id_areaconhecimento,
      		ac.st_areaconhecimento,
      		CONVERT(VARCHAR,t.dt_inicio,103) as dt_inicio ,
      		ev.id_evolucao,
      		st_evolucao,
      		(qtd6.qtd + can6.qtd) AS nu_dia6,
      		(qtd5.qtd + can5.qtd) AS nu_dia5,
      		(qtd4.qtd + can4.qtd) AS nu_dia4,
      		(qtd3.qtd + can3.qtd) AS nu_dia3,
      		(qtd2.qtd + can2.qtd) AS nu_dia2,
      		(qtd1.qtd + can1.qtd) AS nu_dia1,
      		''' + CONVERT(VARCHAR, @dt_datainformada, 103) + ''' AS dt_atual

      FROM
      		dbo.tb_entidade AS e
      		JOIN dbo.vw_entidaderecursivaid AS eri ON eri.id_entidade = e.id_entidade OR eri.nu_entidadepai = e.id_entidade
      		JOIN dbo.tb_venda AS vd ON vd.id_entidade = e.id_entidade
      		JOIN dbo.tb_vendaproduto AS vp ON  vd.id_venda = vp.id_venda
      		JOIN dbo.tb_produto AS p ON  vp.id_produto = p.id_produto
      								AND p.bl_ativo = 1
      		JOIN dbo.tb_produtoprojetopedagogico AS ppp ON ppp.id_produto = p.id_produto
      		JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = ppp.id_projetopedagogico
      		JOIN dbo.tb_turma AS t ON t.id_turma = vp.id_turma AND t.bl_ativo = 1 and t.id_evolucao in (45,46) and t.dt_inicio > ''' + CAST(@dt_datainformada AS VARCHAR) + '''
      		JOIN dbo.tb_evolucao AS ev ON ev.id_evolucao = t.id_evolucao
      		JOIN dbo.tb_produtoarea as pa on p.id_produto = pa.id_produto
      		JOIN dbo.tb_areaconhecimento as ac on ac.id_areaconhecimento = pa.id_areaconhecimento
      		CROSS APPLY(
      					SELECT COUNT(vpd.id_matricula) AS qtd
      					FROM tb_vendaproduto AS vpd
      					JOIN tb_venda as vd2 ON vd2.id_venda = vpd.id_venda and vd2.id_evolucao = 10
      					JOIN tb_matricula as mt2 on mt2.id_matricula = vpd.id_matricula and mt2.id_evolucao = 6
      					WHERE vpd.id_produto = vp.id_produto
      							AND cast(vd2.dt_confirmacao as date) <= ''' + CAST(@dt_datainformada AS VARCHAR) + '''
      					) AS qtd1
      		CROSS APPLY(
      					SELECT COUNT(vpd.id_matricula) AS qtd
      					FROM tb_vendaproduto AS vpd
      					JOIN tb_venda as vd2 ON vd2.id_venda = vpd.id_venda and vd2.id_evolucao = 10
      					JOIN tb_matricula as mt2 on mt2.id_matricula = vpd.id_matricula and mt2.id_evolucao = 6
      					WHERE vpd.id_produto = vp.id_produto
      							AND cast(vd2.dt_confirmacao as date)  <= DATEADD(D,-1, ''' + CAST(@dt_datainformada AS VARCHAR) + ''')
      					) AS qtd2
      		CROSS APPLY(
      					SELECT COUNT(vpd.id_matricula) AS qtd
      					FROM tb_vendaproduto AS vpd
      					JOIN tb_venda as vd2 ON vd2.id_venda = vpd.id_venda and vd2.id_evolucao = 10
      					JOIN tb_matricula as mt2 on mt2.id_matricula = vpd.id_matricula and mt2.id_evolucao = 6
      					WHERE vpd.id_produto = vp.id_produto
      							AND cast(vd2.dt_confirmacao as date)  <= DATEADD(D,-2, ''' + CAST(@dt_datainformada AS VARCHAR) + ''')
      					) AS qtd3
      		CROSS APPLY(
      					SELECT COUNT(vpd.id_matricula) AS qtd
      					FROM tb_vendaproduto AS vpd
      					JOIN tb_venda as vd2 ON vd2.id_venda = vpd.id_venda and vd2.id_evolucao = 10
      					JOIN tb_matricula as mt2 on mt2.id_matricula = vpd.id_matricula and mt2.id_evolucao = 6
      					WHERE vpd.id_produto = vp.id_produto
      							AND cast(vd2.dt_confirmacao as date)  <= DATEADD(D,-3,''' + CAST(@dt_datainformada AS VARCHAR) + ''')
      					) AS qtd4
      		CROSS APPLY(
      					SELECT COUNT(vpd.id_matricula) AS qtd
      					FROM tb_vendaproduto AS vpd
      					JOIN tb_venda as vd2 ON vd2.id_venda = vpd.id_venda and vd2.id_evolucao = 10
      					JOIN tb_matricula as mt2 on mt2.id_matricula = vpd.id_matricula and mt2.id_evolucao = 6
      					WHERE vpd.id_produto = vp.id_produto
      							AND cast(vd2.dt_confirmacao as date)  <= DATEADD(D,-4, ''' + CAST(@dt_datainformada AS VARCHAR) + ''')
      					) AS qtd5
      		CROSS APPLY(
      					SELECT COUNT(vpd.id_matricula) AS qtd
      					FROM tb_vendaproduto AS vpd
      					JOIN tb_venda as vd2 ON vd2.id_venda = vpd.id_venda and vd2.id_evolucao = 10
      					JOIN tb_matricula as mt2 on mt2.id_matricula = vpd.id_matricula and mt2.id_evolucao = 6
      					WHERE vpd.id_produto = vp.id_produto
      							AND cast(vd2.dt_confirmacao as date)  <= DATEADD(D,-5, ''' + CAST(@dt_datainformada AS VARCHAR) + ''')
      					) AS qtd6

      		CROSS APPLY(
      					SELECT COUNT(vpd.id_matricula) AS qtd
      					FROM tb_vendaproduto AS vpd
      					JOIN tb_venda as vd2 ON vd2.id_venda = vpd.id_venda and vd2.id_evolucao = 10
      					JOIN tb_matricula as mt2 on mt2.id_matricula = vpd.id_matricula and mt2.id_evolucao = 27
      					JOIN tb_cancelamento as ca on ca.id_cancelamento = mt2.id_cancelamento
      					WHERE vpd.id_produto = vp.id_produto
      							AND ca.dt_solicitacao between DATEADD(D,1,'''+CAST(@dt_datainformada AS VARCHAR)+''') and DATEADD(D,(31-(select Day('''+CAST(@dt_datainformada AS VARCHAR)+'''))), ''' + CAST(@dt_datainformada AS VARCHAR) + ''')
      					) AS can1
      		CROSS APPLY(
      					SELECT COUNT(vpd.id_matricula) AS qtd
      					FROM tb_vendaproduto AS vpd
      					JOIN tb_venda as vd2 ON vd2.id_venda = vpd.id_venda and vd2.id_evolucao = 10
      					JOIN tb_matricula as mt2 on mt2.id_matricula = vpd.id_matricula and mt2.id_evolucao = 27
      					JOIN tb_cancelamento as ca on ca.id_cancelamento = mt2.id_cancelamento
      					WHERE vpd.id_produto = vp.id_produto
      							AND ca.dt_solicitacao between '''+CAST(@dt_datainformada AS VARCHAR)+''' and DATEADD(D,(32-(select Day('''+CAST(@dt_datainformada AS VARCHAR)+'''))), ''' + CAST(@dt_datainformada AS VARCHAR) + ''')
      					) AS can2
      		CROSS APPLY(
      					SELECT COUNT(vpd.id_matricula) AS qtd
      					FROM tb_vendaproduto AS vpd
      					JOIN tb_venda as vd2 ON vd2.id_venda = vpd.id_venda and vd2.id_evolucao = 10
      					JOIN tb_matricula as mt2 on mt2.id_matricula = vpd.id_matricula and mt2.id_evolucao = 27
      					JOIN tb_cancelamento as ca on ca.id_cancelamento = mt2.id_cancelamento
      					WHERE vpd.id_produto = vp.id_produto
      							AND ca.dt_solicitacao between DATEADD(D,-1,'''+CAST(@dt_datainformada AS VARCHAR)+''') and DATEADD(D,(33-(select Day('''+CAST(@dt_datainformada AS VARCHAR)+'''))), ''' + CAST(@dt_datainformada AS VARCHAR) + ''')
      					) AS can3
      		CROSS APPLY(
      					SELECT COUNT(vpd.id_matricula) AS qtd
      					FROM tb_vendaproduto AS vpd
      					JOIN tb_venda as vd2 ON vd2.id_venda = vpd.id_venda and vd2.id_evolucao = 10
      					JOIN tb_matricula as mt2 on mt2.id_matricula = vpd.id_matricula and mt2.id_evolucao = 27
      					JOIN tb_cancelamento as ca on ca.id_cancelamento = mt2.id_cancelamento
      					WHERE vpd.id_produto = vp.id_produto
      							AND ca.dt_solicitacao between DATEADD(D,-2,'''+CAST(@dt_datainformada AS VARCHAR)+''') and DATEADD(D,(34-(select Day('''+CAST(@dt_datainformada AS VARCHAR)+'''))), ''' + CAST(@dt_datainformada AS VARCHAR) + ''')
      					) AS can4
      		CROSS APPLY(
      					SELECT COUNT(vpd.id_matricula) AS qtd
      					FROM tb_vendaproduto AS vpd
      					JOIN tb_venda as vd2 ON vd2.id_venda = vpd.id_venda and vd2.id_evolucao = 10
      					JOIN tb_matricula as mt2 on mt2.id_matricula = vpd.id_matricula and mt2.id_evolucao = 27
      					JOIN tb_cancelamento as ca on ca.id_cancelamento = mt2.id_cancelamento
      					WHERE vpd.id_produto = vp.id_produto
      							AND ca.dt_solicitacao between DATEADD(D,-3,'''+CAST(@dt_datainformada AS VARCHAR)+''') and DATEADD(D,(35-(select Day('''+CAST(@dt_datainformada AS VARCHAR)+'''))), ''' + CAST(@dt_datainformada AS VARCHAR) + ''')
      					) AS can5
      		CROSS APPLY(
      					SELECT COUNT(vpd.id_matricula) AS qtd
      					FROM tb_vendaproduto AS vpd
      					JOIN tb_venda as vd2 ON vd2.id_venda = vpd.id_venda and vd2.id_evolucao = 10
      					JOIN tb_matricula as mt2 on mt2.id_matricula = vpd.id_matricula and mt2.id_evolucao = 27
      					JOIN tb_cancelamento as ca on ca.id_cancelamento = mt2.id_cancelamento
      					WHERE vpd.id_produto = vp.id_produto
      							AND ca.dt_solicitacao between DATEADD(D,-4,'''+CAST(@dt_datainformada AS VARCHAR)+''') and DATEADD(D,(36-(select Day('''+CAST(@dt_datainformada AS VARCHAR)+'''))), ''' + CAST(@dt_datainformada AS VARCHAR) + ''')
      					) AS can6

      			WHERE	eri.nu_entidadepai = ' + CAST(@id_entidadepai AS VARCHAR)


      IF (@id_entidade IS NOT NULL) BEGIN
      SET @SQLQuery = @SQLQuery + ' and e.id_entidade = ' + CAST(@id_entidade AS VARCHAR);
      END


      IF (@id_turma IS NOT NULL) BEGIN
      SET @SQLQuery = @SQLQuery + ' and t.id_turma = ' + CAST(@id_turma AS VARCHAR);
			END


      IF (@id_evolucao IS NOT NULL) BEGIN
      SET @SQLQuery = @SQLQuery + ' and t.id_evolucao = ' + CAST(@id_evolucao AS VARCHAR);
      END

      SET @SQLQuery = @SQLQuery + ' order by ac.st_areaconhecimento asc, e.st_nomeentidade asc'

      EXECUTE (@SQLQuery)

      END

      GO
			VIEW [dbo].[vw_alunosrenovacao]
			AS

			SELECT DISTINCT
			mat.id_matricula,
			mat.id_projetopedagogico,
			mat.id_usuario
			, mat.id_periodoletivo
			, mat.id_evolucao
			, mat.id_vendaproduto
			, mat.id_entidadematricula
			, mat.id_turma
			, venda.id_venda
			, vp.id_produto
			, vp.id_tiposelecao
			, c.id_contrato
			, c.id_contratoregra
			 FROM tb_matricula AS mat
			JOIN tb_entidade AS e ON e.id_entidade = mat.id_entidadematricula AND mat.bl_ativo = 1 AND mat.id_evolucao = 6 AND mat.bl_institucional = 0
			JOIN tb_esquemaconfiguracaoitem AS eci ON eci.id_esquemaconfiguracao = e.id_esquemaconfiguracao AND eci.st_valor = 2
			JOIN tb_itemconfiguracao AS ic ON ic.id_itemconfiguracao = eci.id_itemconfiguracao AND ic.id_itemconfiguracao = 22
			JOIN tb_contratomatricula AS cm ON cm.id_matricula = mat.id_matricula AND  cm.bl_ativo = 1
			JOIN tb_contrato AS c ON c.id_contrato = cm.id_contrato AND c.bl_ativo = 1
			JOIN tb_venda    AS v ON v.id_venda = c.id_venda AND (v.dt_tentativarenovacao IS NULL OR v.dt_tentativarenovacao < convert(date, getdate()))
			OUTER APPLY (SELECT TOP 1 v.id_venda FROM tb_venda v
							JOIN tb_vendaproduto vp ON v.id_venda = vp.id_venda AND vp.id_matricula = mat.id_matricula
							JOIN tb_contrato cont ON cont.id_venda = v.id_venda AND cont.bl_ativo = 1
							where v.id_evolucao = 10 AND v.bl_ativo = 1
							ORDER BY v.id_venda DESC
							) AS venda
			JOIN tb_vendaproduto as vp ON vp.id_vendaproduto = mat.id_vendaproduto

			AND NOT EXISTS (SELECT lan.id_lancamento FROM tb_lancamento as lan
									JOIN tb_lancamentovenda as lv ON lv.id_lancamento = lan.id_lancamento AND lan.bl_ativo = 1 AND lan.bl_quitado = 0
									AND lv.id_venda = venda.id_venda
									)
			and not EXISTS (select * from tb_saladeaula as sa
									JOIN tb_alocacao as al on al.id_saladeaula = sa.id_saladeaula AND al.bl_ativo = 1
									JOIN tb_matriculadisciplina as md on md.id_matriculadisciplina = al.id_matriculadisciplina and md.id_matricula = mat.id_matricula
									where sa.dt_abertura > getdate())
			AND NOT EXISTS (SELECT vd.id_venda FROM tb_venda as vd
									JOIN tb_vendaproduto as vpp on vpp.id_venda = vd.id_venda and vpp.id_matricula = mat.id_matricula
									 WHERE vd.id_evolucao in ( 7, 9 ) AND vd.bl_ativo = 1 and vd.dt_limiterenovacao > getdate())

			AND venda.id_venda IS NOT NULL


			GO




alter VIEW [dbo].[vw_historicoagendamento]
AS
SELECT        dbo.tb_avaliacaoagendamento.id_avaliacaoagendamento
, dbo.tb_avaliacaoagendamento.id_matricula
, dbo.tb_avaliacaoagendamento.id_tipodeavaliacao
, (case when  dbo.tb_avaliacaoagendamento.id_tipodeavaliacao = 0
	then 'Prova Final'
	else 'Recuperação'+' '+ CAST(dbo.tb_avaliacaoagendamento.id_tipodeavaliacao as char)
	end) as st_tipodeavaliacao
,dbo.tb_tramite.id_tramite
, dbo.tb_tramite.st_tramite
, dbo.tb_situacao.id_situacao
, dbo.tb_situacao.st_situacao
, dbo.tb_tramite.bl_visivel
, dbo.tb_tramite.dt_cadastro
, dbo.tb_tramite.id_entidade
, dbo.tb_tramite.id_usuario
, tb_usuario.st_nomecompleto
, year(dbo.tb_avaliacaoagendamento.dt_cadastro) as id_ano
		FROM dbo.tb_avaliacaoagendamento
		INNER JOIN dbo.tb_tramiteagendamento ON dbo.tb_avaliacaoagendamento.id_avaliacaoagendamento = dbo.tb_tramiteagendamento.id_avaliacaoagendamento
		INNER JOIN  dbo.tb_tramite ON dbo.tb_tramiteagendamento.id_tramite = dbo.tb_tramite.id_tramite AND tb_tramite.id_tipotramite =  14
		INNER JOIN dbo.tb_tipotramite ON tb_tipotramite.id_tipotramite = tb_tramite.id_tipotramite AND tb_tipotramite.id_categoriatramite = 5
		INNER JOIN dbo.tb_situacao ON dbo.tb_tramiteagendamento.id_situacao = dbo.tb_situacao.id_situacao
		INNER JOIN tb_usuario on dbo.tb_tramite.id_usuario = tb_usuario.id_usuario



GO

alter VIEW [dbo].[vw_matricula]

AS
 SELECT
	mt.id_usuario,
	us.st_nomecompleto,
	us.st_cpf,
	ps.st_urlavatar,
	ps.dt_nascimento,
	ps.st_nomepai,
	ps.st_nomemae,
	ps.st_email,
	CAST(ps.nu_ddd AS VARCHAR) + '-' + CAST(ps.nu_telefone AS VARCHAR) AS st_telefone,
	CAST(ps.nu_dddalternativo AS VARCHAR) + '-'
	+ CAST(ps.nu_telefonealternativo AS VARCHAR) AS st_telefonealternativo,
	mt.id_matricula,
	pp.st_projetopedagogico,
	mt.id_situacao,
	st.st_situacao,
	mt.id_evolucao,
	ev.st_evolucao,
	mt.dt_concluinte,
	mt.id_entidadematriz,
	etmz.st_nomeentidade AS st_entidadematriz,
	mt.id_entidadematricula,
	etm.st_nomeentidade AS st_entidadematricula,
	mt.id_entidadeatendimento,
	eta.st_nomeentidade AS st_entidadeatendimento,
	mt.bl_ativo,
	cm.id_contrato,
	pp.id_projetopedagogico,
	mt.dt_termino,
	mt.dt_cadastro,
	mt.dt_inicio,
	tm.id_turma,
	tm.st_turma,
	tm.dt_inicio AS dt_inicioturma,
	mt.bl_institucional,
	tm.dt_fim AS dt_terminoturma,
	mt.id_situacaoagendamento,
	mt.id_vendaproduto,
	mt.dt_termino AS dt_terminomatricula,
	CAST(RIGHT(master.dbo.fn_varbintohexstr(HASHBYTES('MD5',
	(CAST(us.id_usuario AS VARCHAR(10)) COLLATE Latin1_General_CI_AI
	+ CAST((CASE
		WHEN mt.id_matricula IS NULL THEN 0
		ELSE mt.id_matricula
	END) AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
	+ CAST(5 AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
	+ CAST((0) AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
	+ us.st_login
	+ us.st_senha
	+ CONVERT(VARCHAR(10), GETDATE(), 103)))),
	32) AS CHAR(32)) COLLATE Latin1_General_CI_AI AS st_urlacesso,
	mt.bl_documentacao,
	vprod.id_venda,
	eta.st_urlportal,
	mt.st_codcertificacao,
	ps.st_identificacao,
	CONVERT(VARCHAR, mtc.dt_cadastro, 103) AS st_dtcadastrocertificacao,
	CONVERT(VARCHAR, mtc.dt_enviocertificado, 103) AS st_dtenviocertificado,
	CONVERT(VARCHAR, mtc.dt_retornocertificadora, 103) AS st_dtretornocertificadora,
	CONVERT(VARCHAR, mtc.dt_envioaluno, 103) AS st_dtenvioaluno,
	st_codigoacompanhamento,
	mtc.id_indicecertificado,
	mt.id_evolucaocertificacao,
	ap.id_areaconhecimento,
	pp.bl_disciplinacomplementar,
	mt.id_matriculavinculada,
	cr.id_contratoregra,
	trc.id_tiporegracontrato,
	etm.st_siglaentidade,
	mt.bl_academico,
	mtc.dt_cadastro AS dt_cadastrocertificacao,
	mtc.dt_enviocertificado,
	mtc.dt_retornocertificadora,
	mtc.dt_envioaluno,
	vup.st_upload AS st_uploadcontrato,
	canc.id_cancelamento,
	mt.id_aproveitamento,
  vprod.id_produto
FROM tb_matricula mt
JOIN tb_projetopedagogico AS pp
	ON mt.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_areaprojetopedagogico AS ap
	ON ap.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_entidade AS etmz
	ON etmz.id_entidade = mt.id_entidadematriz
JOIN tb_entidade AS etm
	ON etm.id_entidade = mt.id_entidadematricula
JOIN tb_entidade AS eta
	ON eta.id_entidade = mt.id_entidadeatendimento
JOIN tb_situacao AS st
	ON st.id_situacao = mt.id_situacao
JOIN tb_evolucao AS ev
	ON ev.id_evolucao = mt.id_evolucao
JOIN tb_usuario AS us
	ON us.id_usuario = mt.id_usuario
LEFT JOIN tb_contratomatricula AS cm
	ON cm.id_matricula = mt.id_matricula
	AND cm.bl_ativo = 1
LEFT JOIN vw_pessoa AS ps
	ON mt.id_usuario = ps.id_usuario
	AND mt.id_entidadeatendimento = ps.id_entidade
LEFT JOIN dbo.tb_contrato AS ct
	ON ct.id_contrato = cm.id_contrato
	AND ct.bl_ativo = 1
LEFT JOIN tb_turma AS tm
	ON tm.id_turma = mt.id_turma
LEFT JOIN tb_matriculacertificacao mtc
	ON mtc.id_matricula = mt.id_matricula
	AND mtc.bl_ativo = 1
LEFT JOIN dbo.tb_contratoregra AS cr
	ON cr.id_contratoregra = ct.id_contratoregra
LEFT JOIN dbo.tb_tiporegracontrato AS trc
	ON trc.id_tiporegracontrato = cr.id_tiporegracontrato
LEFT JOIN dbo.tb_venda AS v
	ON ct.id_venda = v.id_venda
LEFT JOIN dbo.tb_upload AS vup
	ON vup.id_upload = v.id_uploadcontrato
LEFT JOIN dbo.tb_cancelamento AS canc
	ON canc.id_cancelamento = mt.id_cancelamento
OUTER APPLY (SELECT TOP 1 tb_vendaproduto.* FROM dbo.tb_vendaproduto
	JOIN tb_produtoprojetopedagogico AS ppd ON ppd.id_produto = tb_vendaproduto.id_produto
	JOIN tb_venda AS vend ON vend.id_venda = tb_vendaproduto.id_venda AND vend.id_evolucao = 10
  WHERE tb_vendaproduto.id_matricula = mt.id_matricula
  AND tb_vendaproduto.bl_ativo = 1
  ORDER BY tb_vendaproduto.id_vendaproduto DESC) AS vprod
WHERE mt.bl_ativo = 1;

go

alter VIEW [dbo].[vw_matriculagradesala] AS
SELECT DISTINCT mt.id_usuario, us.st_nomecompleto
, mt.id_matricula
, pp.id_projetopedagogico, pp.st_projetopedagogico, uspp.st_nomecompleto AS st_coordenadorprojeto
, md.id_modulo, md.st_modulo, md.st_tituloexibicao AS st_moduloexibicao
, mdd.id_disciplina, dc.st_disciplina, dc.st_tituloexibicao AS st_disciplinaexibicao, dc.nu_cargahoraria
, ds.id_saladeaula, sa.st_saladeaula, usp.st_nomecompleto AS st_professortitular,
sa.dt_abertura ,
sa.dt_encerramento,
 mdc.id_matriculadisciplina, mdc.id_situacao, st.st_situacao, mdc.id_evolucao, ev.st_evolucao,(mdc.nu_aprovafinal*pp.nu_notamaxima/100) AS nu_notafinal
--, sum(cast ( aa.st_nota as numeric(5,2) )) as nu_notatemp
, em.st_situacaoentrega
FROM
tb_matricula AS mt
JOIN tb_matriculadisciplina AS mdc ON mt.id_matricula = mdc.id_matricula
JOIN tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
JOIN tb_modulo AS md ON md.id_projetopedagogico = mt.id_projetopedagogico AND md.bl_ativo = 1
JOIN tb_modulodisciplina AS mdd ON mdd.id_modulo = md.id_modulo AND mdc.id_disciplina = mdd.id_disciplina AND mdd.bl_ativo = 1
JOIN tb_disciplina AS dc ON dc.id_disciplina = mdd.id_disciplina AND dc.id_disciplina = mdc.id_disciplina
JOIN dbo.tb_usuario us ON us.id_usuario = mt.id_usuario
LEFT JOIN tb_alocacao AS al ON al.id_matriculadisciplina = mdc.id_matriculadisciplina AND al.bl_ativo = 1
LEFT JOIN tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula
LEFT JOIN tb_disciplinasaladeaula AS ds ON ds.id_disciplina = mdd.id_disciplina AND ds.id_saladeaula = al.id_saladeaula
LEFT JOIN tb_situacao AS st ON st.id_situacao = mdc.id_situacao
LEFT JOIN tb_evolucao AS ev ON ev.id_evolucao = mdc.id_evolucao
LEFT JOIN tb_avaliacaoconjuntoreferencia AS acr ON acr.dt_inicio <= GETDATE() AND acr.dt_fim >= GETDATE() AND (acr.id_projetopedagogico = mt.id_projetopedagogico OR acr.id_saladeaula = al.id_saladeaula)
LEFT JOIN tb_avaliacaoaluno AS aa ON aa.id_avaliacaoconjuntoreferencia = acr.id_avaliacaoconjuntoreferencia AND mt.id_matricula = aa.id_matricula AND aa.bl_ativo = 1
LEFT JOIN tb_avaliacaodisciplina AS ad ON ad.id_avaliacao = aa.id_avaliacao AND ad.id_disciplina = mdc.id_disciplina
LEFT JOIN dbo.vw_entregamaterial AS em ON em.id_matricula = mt.id_matricula AND em.id_disciplina = dc.id_disciplina --AND md.id_modulo = em.id_modulo AND em.bl_ativo = 1
LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS uper ON uper.id_saladeaula = sa.id_saladeaula AND uper.bl_titular = 1 AND uper.bl_ativo = 1
LEFT JOIN dbo.tb_usuario AS usp ON usp.id_usuario = uper.id_usuario
RIGHT JOIN dbo.tb_usuarioperfilentidadereferencia AS uperp ON uperp.id_projetopedagogico = pp.id_projetopedagogico AND uperp.bl_titular = 1 AND uperp.bl_ativo = 1
RIGHT JOIN tb_perfil AS perfil ON perfil.id_perfil = uperp.id_perfil  AND  perfil.id_perfilpedagogico = 2
RIGHT JOIN dbo.tb_usuario AS uspp ON uspp.id_usuario = uperp.id_usuario


GO

ALTER VIEW [dbo].[vw_matriculanota]
AS
SELECT
  mt.id_matricula,
  md.id_matriculadisciplina,
  al.id_saladeaula,
  id_alocacao,
  -- atividade = 1 - Atividade Presencial
  atividade.id_avaliacao AS id_avaliacaoatividade,
  atividade.st_nota AS st_notaatividade,
  atividade.id_tiponota AS id_tiponotaatividade,
  atividade.st_avaliacao AS st_avaliacaoatividade,
  -- EAD = 5
  ead.id_avaliacao AS id_avaliacaoead,
  ead.st_nota AS st_notaead,
  ead.id_tiponota AS id_tiponotaead,
  ead.st_avaliacao AS st_avaliacaoead,
  -- FINAL = 4
  final.id_avaliacao AS id_avaliacaofinal,
  final.id_disciplina AS id_disciplinafinal,
  final.st_tituloexibicaodisciplina AS st_disciplinafinal,
  final.st_nota AS st_notafinal,
  final.id_tipoavaliacao,
  final.id_tiponota AS id_tiponotafinal,
  final.st_avaliacao AS st_avaliacaofinal,

  tcc.id_avaliacao AS id_avaliacaotcc,
  tcc.st_nota AS st_notatcc,
  tcc.id_tiponota AS id_tiponotatcc,

  -- recuperacao = 2
  recuperacao.id_avaliacao AS id_avaliacaorecuperacao,
  recuperacao.st_nota AS st_notarecuperacao,
  recuperacao.id_tiponota AS id_tiponotarecuperacao,
  recuperacao.st_avaliacao AS st_avaliacaorecuperacao,
  recuperacao.id_tipocalculoavaliacao,
  /*(ISNULL(CAST(final.st_nota AS INT), 0)
  + ISNULL(CAST(atividade.st_nota AS int), 0)
  + ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
  + ISNULL(CAST(ead.st_nota AS INT), 0)) AS nu_notatotal,

  */
  nu_notatotal = CASE WHEN recuperacao.st_nota is NOT NULL AND recuperacao.id_tipocalculoavaliacao = 2
							THEN ( (ISNULL(CAST(final.st_nota AS INT), 0)
						   + ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
						   + ISNULL(CAST(atividade.st_nota AS int), 0)
						   + ISNULL(CAST(recuperacao.st_nota AS INT), 0)
						   + ISNULL(CAST(ead.st_nota AS INT), 0))/2  )
						  WHEN  recuperacao.st_nota is NOT NULL
							THEN (ISNULL(CAST(final.st_nota AS INT), 0)
								+ ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
								+ ISNULL(CAST(atividade.st_nota AS int), 0)
								+ ISNULL(CAST(recuperacao.st_nota AS INT), 0)
								+ ISNULL(CAST(ead.st_nota AS INT), 0))
						  ELSE
							   (ISNULL(CAST(final.st_nota AS INT), 0)
									+ ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
									+ ISNULL(CAST(atividade.st_nota AS int), 0)
									+ ISNULL(CAST(ead.st_nota AS INT), 0))

				 END,
	CAST(((pp.nu_notamaxima * pp.nu_percentualaprovacao) / 100) AS INT) nu_notaaprovacao,
  bl_calcular =
               CASE
                 WHEN ead.id_avaliacao IS NOT NULL AND
                   ead.st_nota IS NULL THEN 0
                 WHEN atividade.id_avaliacao IS NOT NULL AND
                   atividade.st_nota IS NULL THEN 0
                 WHEN final.id_avaliacao IS NOT NULL AND
                   final.st_nota IS NULL THEN 0
                 WHEN tcc.id_avaliacao IS NOT NULL AND
                   tcc.dt_defesa IS NULL THEN 0
                 ELSE 1
               END,
  bl_aprovado =
               CASE
                 WHEN recuperacao.st_nota is null AND (ISNULL(CAST(final.st_nota AS INT), 0)
                   + ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
                   + ISNULL(CAST(atividade.st_nota AS int), 0)
                   + ISNULL(CAST(ead.st_nota AS INT), 0)) >= CAST(((pp.nu_notamaxima
                   * pp.nu_percentualaprovacao)
                   / 100) AS INT) THEN 1
                 WHEN  recuperacao.st_nota is null AND (ISNULL(CAST(final.st_nota AS INT), 0)
                   + ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
                   + ISNULL(CAST(atividade.st_nota AS int), 0)
                   + ISNULL(CAST(ead.st_nota AS INT), 0)) < CAST(((pp.nu_notamaxima
                   * pp.nu_percentualaprovacao)
                   / 100) AS INT) THEN 0
				 WHEN recuperacao.st_nota is NOT NULL AND recuperacao.id_tipocalculoavaliacao = 2
					 AND ( (ISNULL(CAST(final.st_nota AS INT), 0)
					   + ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
					   + ISNULL(CAST(atividade.st_nota AS int), 0)
					   + ISNULL(CAST(recuperacao.st_nota AS INT), 0)
					   + ISNULL(CAST(ead.st_nota AS INT), 0))/2  ) >= CAST(((pp.nu_notamaxima * pp.nu_percentualaprovacaovariavel)
					   / 100) AS INT) THEN 1
				  WHEN  recuperacao.st_nota is NOT NULL AND (ISNULL(CAST(final.st_nota AS INT), 0)
						+ ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
						+ ISNULL(CAST(atividade.st_nota AS int), 0)
						+ ISNULL(CAST(recuperacao.st_nota AS INT), 0)
						+ ISNULL(CAST(ead.st_nota AS INT), 0)) < CAST(((pp.nu_notamaxima * pp.nu_percentualaprovacaovariavel )
						/ 100) AS INT) THEN 0
                 ELSE 0

               END,



              (CASE
                WHEN recuperacao.id_tipocalculoavaliacao = 2 THEN (
                    ( ISNULL(CAST(final.st_nota AS INT), 0)
                    + ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
                    + ISNULL(CAST(ead.st_nota AS INT), 0)
                    + ISNULL(CAST(atividade.st_nota AS INT), 0)
                    + ISNULL(CAST(recuperacao.st_nota AS INT), 0)
                  )
                   -- Divide por 2 se tiver nota de recuperacao, se nao, divide por 1 (mantem igual)
                  / (CASE WHEN recuperacao.st_nota IS NOT NULL THEN 2 ELSE 1 END)
                ) ELSE (
                    ( ISNULL(CAST(final.st_nota AS INT), 0)
                    + ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
                    + ISNULL(CAST(ead.st_nota AS INT), 0)
                    + ISNULL(CAST( (CASE WHEN recuperacao.st_nota IS NOT NULL THEN recuperacao.st_nota ELSE atividade.st_nota END) AS INT), 0)
                  )
                )
              END)
              -- trata porque esse valor pode estar salvo como 0
              / (CASE WHEN pp.nu_notamaxima IS NULL OR pp.nu_notamaxima = 0 THEN 1 ELSE pp.nu_notamaxima END) * 100
              AS nu_percentualfinal

				FROM tb_matriculadisciplina AS md
				JOIN tb_matricula AS mt
				  ON md.id_matricula = mt.id_matricula
				JOIN dbo.tb_projetopedagogico AS pp
				  ON mt.id_projetopedagogico = pp.id_projetopedagogico
				JOIN dbo.tb_alocacao AS al
				  ON al.id_matriculadisciplina = md.id_matriculadisciplina
				  AND al.bl_ativo = 1
				LEFT JOIN dbo.vw_avaliacaoaluno AS ead
				  ON ead.id_matriculadisciplina = md.id_matriculadisciplina
				  AND al.id_saladeaula = ead.id_saladeaula
				  AND ead.id_tipoavaliacao = 5
				LEFT JOIN dbo.vw_avaliacaoaluno AS atividade
				  ON atividade.id_matriculadisciplina = md.id_matriculadisciplina
				  AND al.id_saladeaula = atividade.id_saladeaula
				  AND atividade.id_tipoavaliacao = 1

				OUTER APPLY (SELECT TOP 1
							  aaf.st_nota,
							  aaf.id_avaliacao,
							  aaf.id_disciplina,
							  aaf.st_tituloexibicaodisciplina,
							  aaf.id_tipoavaliacao
							  , aaf.id_tiponota
							  , aaf.st_avaliacao
							FROM dbo.vw_avaliacaoaluno AS aaf
							WHERE md.id_matriculadisciplina = aaf.id_matriculadisciplina
							AND aaf.id_tipoavaliacao = 4
							ORDER BY cast(aaf.st_nota as NUMERIC) DESC) AS final
				LEFT JOIN dbo.vw_avaliacaoaluno AS tcc
				  ON tcc.id_matriculadisciplina = md.id_matriculadisciplina
				  AND al.id_saladeaula = tcc.id_saladeaula
				  AND tcc.id_tipoavaliacao = 6

				  --add recuperacao
				 LEFT JOIN dbo.vw_avaliacaoaluno AS recuperacao
				  ON recuperacao.id_matriculadisciplina = md.id_matriculadisciplina
				  AND al.id_saladeaula = recuperacao.id_saladeaula
				  AND recuperacao.id_tipoavaliacao = 2

GO

create VIEW dbo.vw_periodoletivoentidade AS
	SELECT DISTINCT
		pl.id_periodoletivo,
		pl.st_periodoletivo,
		pl.dt_inicioinscricao,
		pl.dt_fiminscricao,
		pl.dt_abertura,
		pl.dt_encerramento,
		er.id_entidade,
		pl.id_entidade AS id_entidadeperiodo
	FROM tb_periodoletivo AS pl
	-- Relacionamento adicionado por solicitacao do Analista de Requisitos, por que os periodos letivos devem ser buscados tanto da entidade da sessao, quanto da entidade PAI da sessao (apenas o primeiro pai).
	JOIN vw_entidaderecursiva AS er ON pl.id_entidade IN(er.id_entidade, er.id_entidadepai)

go

alter VIEW [dbo].[vw_salaoferta] AS
SELECT DISTINCT
	se.id_entidade,
	pp.id_projetopedagogico,
	mo.id_modulo,
	mo.st_modulo,
	aps.id_saladeaula,
	sa.st_saladeaula,
	sa.nu_maxalunos,
	sa.bl_ofertaexcepcional,
	pp.id_trilha,
	ds.id_disciplina,
	dc.id_tipodisciplina,
  dc.nu_creditos,
	pl.id_periodoletivo,
	pl.st_periodoletivo,
	pl.dt_inicioinscricao,
	pl.dt_fiminscricao,
	pl.dt_abertura,
	pl.dt_encerramento,
	md.nu_ordem
FROM tb_projetopedagogico AS pp
JOIN tb_projetoentidade as pe on pe.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_areaprojetosala AS aps
	ON aps.id_projetopedagogico = pp.id_projetopedagogico
JOIN vw_periodoletivoentidade AS pl ON pe.id_entidade = pl.id_entidade
JOIN tb_saladeaula AS sa
	ON sa.id_saladeaula = aps.id_saladeaula
	AND sa.id_periodoletivo = pl.id_periodoletivo
	AND sa.bl_ativa = 1
	AND sa.id_situacao = 8
	AND sa.id_categoriasala = 1
	--AND se.id_saladeaula = sa.id_saladeaula
JOIN tb_saladeaulaentidade AS se
	ON (aps.id_saladeaula = se.id_saladeaula OR sa.bl_todasentidades = 1) and pe.id_entidade = se.id_entidade
JOIN tb_disciplinasaladeaula AS ds
	ON ds.id_saladeaula = sa.id_saladeaula
JOIN tb_modulo AS mo
	ON mo.id_projetopedagogico = pp.id_projetopedagogico and mo.bl_ativo = 1
JOIN tb_modulodisciplina AS md
	ON ds.id_disciplina = md.id_disciplina and md.bl_ativo = 1 and md.id_modulo = mo.id_modulo
JOIN tb_disciplina AS dc
	ON dc.id_disciplina = ds.id_disciplina
go
alter VIEW [dbo].[vw_salaofertarenovacao] AS
SELECT DISTINCT
	pe.id_entidade,
	mo.id_modulo,
	mo.st_modulo,
	aps.id_saladeaula,
	sa.st_saladeaula,
	sa.nu_maxalunos,
	sa.bl_ofertaexcepcional,
	pp.id_trilha,
	pp.id_projetopedagogico,
	ds.id_disciplina,
	dc.id_tipodisciplina,
	dc.nu_creditos,
	dc.st_disciplina,
	pl.id_periodoletivo,
	pl.st_periodoletivo,
	pl.dt_inicioinscricao,
	pl.dt_fiminscricao,
	pl.dt_abertura,
	pl.dt_encerramento
FROM tb_projetopedagogico AS pp
  JOIN tb_projetoentidade as  pe on pe.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_areaprojetosala AS aps
	ON aps.id_projetopedagogico = pp.id_projetopedagogico
	JOIN vw_periodoletivoentidade AS pl ON pl.id_periodoletivo IN (
		SELECT TOP 5 id_periodoletivo FROM vw_periodoletivoentidade AS plf WHERE plf.dt_abertura >= GETDATE() AND plf.id_entidade = pe.id_entidade ORDER BY plf.dt_abertura ASC
	) AND pl.id_entidade = pe.id_entidade
JOIN tb_saladeaula AS sa
	ON sa.id_saladeaula = aps.id_saladeaula
	AND sa.id_periodoletivo = pl.id_periodoletivo
	AND sa.bl_ativa = 1
	AND sa.id_situacao = 8
	AND sa.id_categoriasala = 1
JOIN tb_saladeaulaentidade AS se
	ON (se.id_entidade = pe.id_entidade AND aps.id_saladeaula = se.id_saladeaula) OR sa.bl_todasentidades = 1
JOIN tb_disciplinasaladeaula AS ds
	ON ds.id_saladeaula = sa.id_saladeaula
JOIN tb_modulo AS mo
	ON mo.id_projetopedagogico = pp.id_projetopedagogico AND mo.bl_ativo = 1
JOIN tb_modulodisciplina AS md
	ON ds.id_disciplina = md.id_disciplina and md.bl_ativo = 1 and mo.id_modulo = md.id_modulo
JOIN tb_disciplina AS dc
	ON dc.id_disciplina = ds.id_disciplina
go
alter VIEW [dbo].[vw_salaofertaturma] AS
SELECT DISTINCT
	tm.id_turma,
	te.id_entidade,
	tp.id_projetopedagogico,
	mo.id_modulo,
	mo.st_modulo,
	aps.id_saladeaula,
	sa.st_saladeaula,
	sa.nu_maxalunos,
	sa.bl_ofertaexcepcional,
	pp.id_trilha,
	ds.id_disciplina,
	dc.id_tipodisciplina,
	dc.nu_creditos,
  dc.st_disciplina,
	pl.id_periodoletivo,
	pl.st_periodoletivo,
	pl.dt_inicioinscricao,
	pl.dt_fiminscricao,
	pl.dt_abertura,
	pl.dt_encerramento
FROM dbo.tb_turma AS tm
JOIN tb_turmaprojeto AS tp
	ON tp.id_turma = tm.id_turma
JOIN tb_turmaentidade AS te
	ON te.id_turma = tm.id_turma
JOIN tb_projetopedagogico AS pp
	ON tp.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_areaprojetosala AS aps
	ON aps.id_projetopedagogico = tp.id_projetopedagogico
JOIN vw_periodoletivoentidade AS pl ON pl.id_periodoletivo IN (
		SELECT TOP 5 id_periodoletivo FROM vw_periodoletivoentidade AS plf WHERE plf.dt_abertura >= tm.dt_inicio AND plf.id_entidade = te.id_entidade ORDER BY plf.dt_abertura ASC
	) AND pl.id_entidade = te.id_entidade
JOIN tb_saladeaula AS sa
	ON sa.id_saladeaula = aps.id_saladeaula
	AND sa.id_periodoletivo = pl.id_periodoletivo
	AND sa.bl_ativa = 1
	AND sa.id_situacao = 8
	AND sa.id_categoriasala = 1
JOIN tb_saladeaulaentidade AS se
	ON (se.id_entidade = te.id_entidade AND aps.id_saladeaula = se.id_saladeaula) OR sa.bl_todasentidades = 1
JOIN tb_disciplinasaladeaula AS ds
	ON ds.id_saladeaula = sa.id_saladeaula
JOIN tb_modulo AS mo
	ON mo.id_projetopedagogico = pp.id_projetopedagogico AND mo.bl_ativo = 1
JOIN tb_modulodisciplina AS md
	ON ds.id_disciplina = md.id_disciplina and md.bl_ativo = 1 and md.id_modulo = mo.id_modulo
JOIN tb_disciplina AS dc
	ON dc.id_disciplina = ds.id_disciplina
GO
alter VIEW [rel].[vw_cronogramasalas] as
SELECT DISTINCT
        sa.id_saladeaula ,
        st_saladeaula ,
        dt_inicioinscricao ,
        dt_fiminscricao ,
        dt_abertura ,
        dt_encerramento ,
        cast(dt_atualiza as date) as dt_atualiza,
        st_disciplina ,
        dc.st_tituloexibicao AS st_tituloexibicaodisciplina ,
        st_projetopedagogico ,
        aps.id_projetopedagogico,
        uss.st_nomecompleto AS st_nomeprofessor ,
        usp.id_usuario AS id_coordenador,
        usp.st_nomecompleto AS st_nomecoordenador,
        si.st_codsistemacurso AS st_codava,
        sa.id_entidade,
		sa.id_categoriasala,
		CAST(sa.dt_cadastro as date) as dt_cadastro,
		cast(COUNT(ta.id_matriculadisciplina) AS varchar) AS st_alunos
        FROM dbo.tb_saladeaula AS sa
JOIN dbo.tb_areaprojetosala AS aps ON aps.id_saladeaula = sa.id_saladeaula
JOIN dbo.tb_disciplinasaladeaula AS da ON da.id_saladeaula = sa.id_saladeaula
JOIN tb_disciplina AS dc ON dc.id_disciplina = da.id_disciplina
JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = aps.id_projetopedagogico
LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS ups ON ups.id_saladeaula = aps.id_saladeaula AND ups.bl_titular = 1 and ups.bl_ativo=1
LEFT JOIN dbo.tb_usuario AS uss ON uss.id_usuario = ups.id_usuario
LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS upp ON upp.id_projetopedagogico = pp .id_projetopedagogico AND upp.bl_titular = 1 and upp.bl_ativo=1 and upp.id_disciplina is null
LEFT JOIN dbo.tb_perfil AS tbp ON tbp.id_perfil = upp.id_perfil AND tbp.id_perfilpedagogico = 2
LEFT JOIN dbo.tb_usuario AS usp ON usp.id_usuario = upp.id_usuario
LEFT JOIN dbo.tb_saladeaulaintegracao AS si ON si.id_saladeaula = sa.id_saladeaula
LEFT JOIN dbo.tb_alocacao AS ta ON ta.id_saladeaula = sa.id_saladeaula AND ta.bl_ativo = 1
WHERE sa.bl_ativa = 1
GROUP by
sa.id_saladeaula,
st_saladeaula ,
        dt_inicioinscricao ,
        dt_fiminscricao ,
        dt_abertura ,
        dt_encerramento ,
        dt_atualiza,
        st_disciplina ,
        dc.st_tituloexibicao ,
        st_projetopedagogico ,
        aps.id_projetopedagogico,
        uss.st_nomecompleto ,
        usp.id_usuario ,
        usp.st_nomecompleto,
        si.st_codsistemacurso,
        sa.id_entidade,
		sa.dt_cadastro,
		sa.id_categoriasala
GO
alter VIEW [dbo].[vw_projetoarea] AS

SELECT DISTINCT
    ap.id_areaconhecimento,
    ac.st_areaconhecimento,
    ap.id_projetopedagogico,
    pp.st_projetopedagogico,
    pp.st_tituloexibicao,
    null AS tipo,
    pe.id_entidade,
    aps.nu_diasacesso
FROM tb_areaconhecimento ac
JOIN tb_areaprojetopedagogico AS ap ON ap.id_areaconhecimento = ac.id_areaconhecimento
JOIN tb_projetopedagogico AS pp ON ap.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_projetoentidade AS pe ON pp.id_projetopedagogico = pe.id_projetopedagogico
JOIN tb_areaprojetosala AS aps ON aps.id_projetopedagogico = pe.id_projetopedagogico
GO
ALTER VIEW [dbo].[vw_gradetipotcc] AS
	SELECT  DISTINCT sa.dt_abertura,
		pp.id_projetopedagogico,
		pp.st_projetopedagogico,
		sa.st_saladeaula,
		aa.id_avaliacao ,
			st_nota = CASE WHEN dc.id_tipodisciplina = 2 AND aa.dt_defesa IS NULL AND aa.bl_ativo = 1
			THEN NULL
								ELSE CAST(aa.st_nota AS NUMERIC(8,2))
								END ,
		mdc.id_modulo,
		mdl.st_modulo,
			mdl.st_tituloexibicao AS st_tituloexibicaomodulo,
		md.id_disciplina,
			dc.st_tituloexibicao AS st_disciplina,
			dc.st_tituloexibicao AS st_tituloexibicaodisciplina,
		mt.id_matricula ,
			id_tipoavaliacao = CASE WHEN tt.id_tipoavaliacao IS NULL THEN 5
												 ELSE tt.id_tipoavaliacao
												 END ,
			aa.st_tipoavaliacao AS st_labeltipo,
			nu_notafinal = CASE WHEN dc.id_tipodisciplina = 2 AND aa.dt_defesa IS NULL THEN NULL
										 ELSE CAST((md.nu_aprovafinal* pp.nu_notamaxima/100)AS NUMERIC(5,2))
										 END ,
		ev.st_evolucao,
		st.st_situacao,
		dc.nu_cargahoraria,
		sa.id_saladeaula,
		sa.dt_encerramento,
		ndm.nu_notatotal,
			st_status = CASE WHEN  ea.id_alocacao IS NOT NULL AND  md.id_evolucao = 12 THEN ev.st_evolucao
									WHEN  ea.id_alocacao IS NOT NULL AND  md.id_evolucao = 19 THEN ev.st_evolucao
									WHEN  ea.id_alocacao IS NOT NULL AND  md.id_situacao = 65 THEN 'Cr�dito Concedido'
									WHEN  ea.id_alocacao IS NOT NULL AND  (ndm.nu_notatotal + nf.nu_notafaltante) >= (pp.nu_notamaxima * pp.nu_percentualaprovacao/100) THEN 'Satisfat�rio'
									WHEN  ea.id_alocacao IS NOT NULL AND  (ndm.nu_notatotal + nf.nu_notafaltante) < (pp.nu_notamaxima * pp.nu_percentualaprovacao/100) THEN 'Insatisfat�rio'
									ELSE '-'
									END
		,bl_status  = CASE WHEN md.id_evolucao = 12 THEN 1
									WHEN md.id_evolucao = 19 THEN 0
									WHEN md.id_situacao = 65 THEN 1
									WHEN (ndm.nu_notatotal + nf.nu_notafaltante) >= (pp.nu_notamaxima * pp.nu_percentualaprovacao/100) THEN 1
									WHEN (ndm.nu_notatotal + nf.nu_notafaltante) < (pp.nu_notamaxima * pp.nu_percentualaprovacao/100) THEN 0
									ELSE 0
									END
									, st_statusdisciplina = CASE
							WHEN al.id_alocacao IS NULL
							    AND md.id_situacao != 65
                                  THEN 'N�o Iniciada'
                             WHEN ea.id_alocacao IS NOT NULL
                                  AND md.id_evolucao = 12
								  AND ndm.bl_aprovado = 1
								   THEN 'Aprovada'
							 WHEN ea.id_alocacao IS NOT NULL
								  AND ndm.bl_aprovado = 0
								  AND md.id_evolucao = 19
                             THEN 'Reprovado'
							 WHEN md.id_aproveitamento IS NOT NULL
                                  AND md.id_situacao = 65
                             THEN 'Isento'
                             WHEN al.id_alocacao IS NOT NULL
								  AND md.id_aproveitamento IS NULL
                                  AND( md.nu_aprovafinal IS NULL OR ea.id_alocacao IS NULL)
								  AND ( (md.id_situacao = 13 AND (ndm.st_notaead IS NULL OR ndm.st_notaatividade IS NULL OR ndm.st_notafinal IS NULL AND ndm.st_notarecuperacao IS NULL))
										OR  (md.id_situacao != 13 AND ea.id_alocacao IS NULL) )
                             THEN 'Cursando'


                             ELSE '-'
                        END
	, md.id_situacao
    , nu_ordem = CASE WHEN tt.id_tipoavaliacao = 1
                                  THEN 1
					  WHEN tt.id_tipoavaliacao =  4 THEN 2
					  WHEN tt.id_tipoavaliacao =  5 THEN 3
					  WHEN tt.id_tipoavaliacao = 2 THEN 4
					  ELSE 10
					  END
	FROM    dbo.tb_matricula AS mt
		JOIN dbo.tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
		JOIN dbo.tb_disciplina AS dc ON dc.id_disciplina = md.id_disciplina
		LEFT JOIN dbo.tb_alocacao AS al ON al.id_matriculadisciplina = md.id_matriculadisciplina AND al.bl_ativo = 1
		LEFT JOIN tb_encerramentoalocacao AS ea ON ea.id_alocacao = al.id_alocacao
		JOIN dbo.tb_tipoavaliacao AS tt ON tt.id_tipoavaliacao IN  (1,4,5,6)
		LEFT JOIN dbo.vw_avaliacaoaluno AS aa ON aa.id_tipoavaliacao = tt.id_tipoavaliacao AND mt.id_matricula = aa.id_matricula AND aa.id_disciplina = md.id_disciplina AND aa.id_tipoprova = 2 AND aa.id_saladeaula = al.id_saladeaula
		JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
		JOIN dbo.tb_modulo AS mdl ON mdl.id_projetopedagogico = pp.id_projetopedagogico AND mdl.bl_ativo = 1
		JOIN dbo.tb_modulodisciplina AS mdc ON mdc.id_modulo = mdl.id_modulo AND mdc.id_disciplina = dc.id_disciplina   AND mdc.bl_ativo = 1
		LEFT JOIN dbo.tb_disciplinasaladeaula AS ds ON aa.id_saladeaula = al.id_saladeaula AND ds.id_disciplina = md.id_disciplina
		LEFT JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula
		LEFT JOIN dbo.tb_avaliacaodisciplina AS ad ON ad.id_avaliacao = aa.id_avaliacao AND aa.id_disciplina = ad.id_disciplina
		JOIN tb_evolucao AS ev ON ev.id_evolucao = md.id_evolucao
		JOIN dbo.tb_situacao AS st ON st.id_situacao = md.id_situacao
		LEFT JOIN vw_matriculanota AS ndm ON ndm.id_matriculadisciplina = md.id_matriculadisciplina and ndm.id_alocacao = al.id_alocacao
		OUTER APPLY ( SELECT    id_matricula ,
                                        id_disciplina ,
                                        SUM(CAST(nu_notamax AS FLOAT)) AS nu_notafaltante
                              FROM      vw_avaliacaoaluno
                              WHERE     bl_ativo IS NULL
                                        AND id_matricula = mt.id_matricula
                                        AND id_disciplina = md.id_disciplina
                                        AND id_tipoprova = 2
										 AND id_avaliacaorecupera = 0
                              GROUP BY  id_disciplina ,
                                        id_matricula
                            ) AS nf

GO
CREATE VIEW [dbo].[vw_alunosaptosagendamentopordisciplina] AS

SELECT DISTINCT
  md.id_matriculadisciplina,
  md.id_matricula,
  md.id_disciplina,
  mt.id_situacao AS id_situacaomatricula,
  sa.id_saladeaula,
  sa.dt_abertura,
  sa.dt_encerramento,
  sa.bl_ativa,
  sa.id_situacao AS id_situacaosala,
  md.id_situacaoagendamento,
  mt.id_entidadematricula AS id_entidade,

  mtn.st_notaatividade,
  mtn.st_notaead,
  mtn.st_notafinal,
  mtn.nu_percentualfinal,
  mtn.bl_aprovado

FROM vw_entidadeesquemaconfiguracao AS ec

JOIN tb_matricula AS mt ON  ec.id_entidade = mt.id_entidadematricula
JOIN tb_matriculadisciplina AS md ON mt.id_matricula = md.id_matricula

-- Busca media do aluno
JOIN vw_matriculanota AS mtn ON mtn.id_matricula = mt.id_matricula AND mtn.id_matriculadisciplina = md.id_matriculadisciplina
JOIN tb_saladeaula AS sa ON sa.id_saladeaula = mtn.id_saladeaula AND sa.bl_ativa = 1 AND sa.id_situacao = 8 AND GETDATE() BETWEEN sa.dt_abertura AND sa.dt_encerramento

--Retorna somente registros em que a entidade possui prova por disciplina
WHERE ec.id_itemconfiguracao = 24 AND ec.st_valor = 1 AND md.id_situacaoagendamento NOT IN (120,177)
GO
ALTER VIEW [dbo].[vw_matriculaalocacao] as
SELECT
  al.id_alocacao,
  md.id_matriculadisciplina,
	us.st_nomecompleto,
	mt.id_usuario,
	mt.id_entidadeatendimento,
	mt.id_matricula,
	mt.dt_ultimoacesso,
	sa.id_saladeaula,
	mt.dt_inicio,
	mt.dt_cadastro,
	sa.dt_encerramento,
	mt.dt_termino AS dt_terminomatricula,
	en.st_nomeentidade,
	dc.id_disciplina,
	dc.st_disciplina,
	md.id_situacaoagendamento
	--,ag.id_avaliacaoagendamento
FROM tb_matricula AS mt
JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
JOIN tb_disciplina AS dc ON dc.id_disciplina = md.id_disciplina
JOIN tb_alocacao AS al ON al.id_matriculadisciplina = md.id_matriculadisciplina AND al.bl_ativo = 1
JOIN tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula
JOIN tb_entidade AS en ON en.id_entidade = mt.id_entidadeatendimento
JOIN tb_usuario AS us ON mt.id_usuario = us.id_usuario
-- Busca o id do ultimo agendamento da matricula em tal disciplina
--LEFT JOIN vw_avaliacaoagendamento AS ag ON ag.id_matricula = mt.id_matricula AND ag.id_disciplina = md.id_disciplina
GO
alter VIEW [dbo].[vw_avaliacaoaluno] as
SELECT DISTINCT
  ui.st_codusuario as CODUSUARIO,
  ui.id_entidade as ENTIDADEINTEGRACAO,
  ui.id_usuario as USUARIO,
  mt.id_entidadematricula as ENTIDADEMATRICULA,
  mt.id_entidadeatendimento AS ENTIDADEATENDIMENTO,
  id_avaliacaoaluno ,
  mt.id_matricula ,
  us.st_nomecompleto ,
  mdl.id_modulo ,
  mdl.st_tituloexibicao AS st_tituloexibicaomodulo ,
  dc.id_disciplina ,
  dc.st_tituloexibicao AS st_tituloexibicaodisciplina ,
  aa.st_nota ,
  ag.id_avaliacaoagendamento ,
  acr.id_avaliacao ,
  av.st_avaliacao ,
  md.id_situacao ,
  st.st_situacao ,
  ac.id_tipoprova ,
  acrf.id_avaliacaoconjuntoreferencia ,
  ( CASE WHEN avref.id_avaliacaoconjuntoreferencia = acrf.id_avaliacaoconjuntoreferencia
    THEN 1
    ELSE 0
    END ) bl_agendamento ,
  md.id_evolucao ,
  ev.st_evolucao ,
  CAST(av.nu_valor AS VARCHAR(100)) AS nu_notamax ,
  ac.id_tipocalculoavaliacao ,
  av.id_tipoavaliacao ,
  ta.st_tipoavaliacao ,
  ( CASE WHEN acr.id_avaliacaorecupera IS NULL THEN 0
    ELSE acr.id_avaliacaorecupera
    END ) AS id_avaliacaorecupera ,
  ag.bl_provaglobal ,
  pp.nu_notamaxima ,
  pp.nu_percentualaprovacao ,
  ag.dt_agendamento ,
  aa.bl_ativo ,
  aa.dt_avaliacao ,
  alc.id_saladeaula ,
  ui.st_codusuario ,
  ac.id_avaliacaoconjunto ,
  pp.st_projetopedagogico ,
  mt.id_projetopedagogico ,
  aa.dt_defesa ,
  aa.st_tituloavaliacao ,
  mt.id_entidadeatendimento ,
  aa.id_upload ,
  dc.id_tipodisciplina ,
  aa.st_justificativa ,
  md.id_matriculadisciplina ,
  aa.id_tiponota ,
  sda.dt_cadastro AS dt_cadastrosala ,
  sda.dt_encerramento AS dt_encerramentosala ,
  sda.dt_abertura AS dt_aberturasala,
  aa.dt_cadastro AS dt_cadastroavaliacao ,
  aa.id_usuariocadastro AS id_usuariocadavaliacao ,
  usc.st_nomecompleto AS st_usuariocadavaliacao ,
  sdis.id_evolucao AS id_evolucaodisciplina ,
  sdis.st_evolucao AS st_evolucaodisciplina ,
  dc.nu_cargahoraria ,
  agc.id_provasrealizadas ,
  CONVERT(VARCHAR(11), aa.dt_cadastro, 103) AS dt_cadastrotcc ,
  CONVERT(VARCHAR(11), aa.dt_avaliacao, 103) as st_dtavaliacao,
  st_sala.st_situacao AS st_situacaosala,
  up.st_upload,
  sda.id_categoriasala,
  av.bl_recuperacao
FROM    tb_matricula AS mt
  JOIN dbo.tb_usuario AS us ON us.id_usuario = mt.id_usuario
  JOIN tb_modulo AS mdl ON mdl.id_projetopedagogico = mt.id_projetopedagogico
                           AND mdl.bl_ativo = 1
  JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
  JOIN tb_modulodisciplina AS mdc ON mdc.id_modulo = mdl.id_modulo
                                     AND md.id_disciplina = mdc.id_disciplina
                                     AND mdc.bl_ativo = 1
  JOIN tb_alocacao AS alc ON alc.id_matriculadisciplina = md.id_matriculadisciplina
                             AND alc.bl_ativo = 1
  JOIN tb_avaliacaoconjuntoreferencia AS acrf ON acrf.id_saladeaula = alc.id_saladeaula
                                                 AND acrf.dt_fim IS NULL
  --OR acrf.dt_fim >= GETDATE()
  JOIN tb_saladeaula sda ON sda.id_saladeaula = alc.id_saladeaula
  JOIN tb_disciplinasaladeaula AS dsa ON dsa.id_saladeaula = alc.id_saladeaula
  JOIN tb_disciplina AS dc ON dc.id_disciplina = md.id_disciplina
  JOIN tb_evolucao AS sdis ON sdis.id_evolucao = md.id_evolucao
  JOIN tb_avaliacaoconjunto AS ac ON ac.id_avaliacaoconjunto = acrf.id_avaliacaoconjunto
                                     AND id_tipoprova = 2
  JOIN tb_avaliacaoconjuntorelacao AS acr ON acr.id_avaliacaoconjunto = ac.id_avaliacaoconjunto
  JOIN tb_avaliacao AS av ON av.id_avaliacao = acr.id_avaliacao
  JOIN tb_tipoavaliacao AS ta ON ta.id_tipoavaliacao = av.id_tipoavaliacao
  LEFT JOIN tb_avaliacaoagendamento AS ag ON mt.id_matricula = ag.id_matricula
                                             AND av.id_avaliacao = ag.id_avaliacao
                                             AND ag.id_situacao = 68
                                             AND ag.nu_presenca = 1
  LEFT JOIN tb_avalagendamentoref AS avref ON avref.id_avaliacaoagendamento = ag.id_avaliacaoagendamento
                                      AND avref.id_avaliacaoconjuntoreferencia = acrf.id_avaliacaoconjuntoreferencia
  LEFT JOIN tb_avaliacaoaluno AS aa ON aa.id_matricula = mt.id_matricula
                                       AND aa.id_avaliacaoconjuntoreferencia = acrf.id_avaliacaoconjuntoreferencia
                                       AND aa.id_avaliacao = av.id_avaliacao
                                       AND aa.bl_ativo = 1
                                       AND aa.id_situacao = 86
  JOIN tb_situacao AS st ON st.id_situacao = md.id_situacao
  JOIN tb_evolucao AS ev ON ev.id_evolucao = md.id_evolucao
  JOIN tb_projetopedagogico AS PP ON pp.id_projetopedagogico = mt.id_projetopedagogico
  OUTER APPLY ( SELECT    COUNT(id_avaliacaoagendamento) AS id_provasrealizadas
                FROM      tb_avaliacaoagendamento
                WHERE     id_matricula = mt.id_matricula
                          AND nu_presenca = 1
                          AND bl_ativo = 0
                          AND id_situacao = 68
              ) AS agc
  LEFT JOIN dbo.tb_saladeaulaintegracao AS si ON si.id_saladeaula = sda.id_saladeaula
  LEFT JOIN dbo.tb_usuariointegracao AS ui ON ui.id_usuario = mt.id_usuario and ( ui.id_entidade = sda.id_entidade) and ui.id_sistema = si.id_sistema
  LEFT JOIN tb_usuario AS usc ON usc.id_usuario = aa.id_usuariocadastro
  JOIN tb_situacao AS st_sala ON st_sala.id_situacao = sda.id_situacao
  LEFT JOIN tb_upload up ON up.id_upload = aa.id_upload

WHERE   mt.bl_ativo = 1 and dc.id_tipodisciplina <> 3
GO
ALTER VIEW [dbo].[vw_avaliacaoagendamento] AS
SELECT DISTINCT
	aa.id_avaliacaoagendamento,
	aa.id_matricula,
	us.id_usuario,
	us.st_nomecompleto,
	av.id_avaliacao,
	av.st_avaliacao,
	ac.id_tipoprova,
	tp.st_tipoprova,
	aa.dt_agendamento,
	aa.id_situacao,
	st.st_situacao,
	aa.id_avaliacaoaplicacao,
	aa.id_entidade,
	et.st_nomeentidade,
	ap.id_horarioaula,
	ha.st_horarioaula,
	mt.id_projetopedagogico,
	pp.st_projetopedagogico,
	aai.st_codsistema,
	aa.bl_ativo,
	ap.st_endereco,
	ap.dt_aplicacao,
	ap.id_aplicadorprova,
	ap.st_aplicadorprova,
	ap.st_cidade,
	aa.nu_presenca,
	aa.id_usuariolancamento,
	usu_lancamento.st_nomecompleto AS st_usuariolancamento,
	(CASE
		WHEN aa.id_tipodeavaliacao IS NULL THEN 0
		ELSE aa.id_tipodeavaliacao
	END) AS id_tipodeavaliacao,
	(CASE
		WHEN (ap.dt_aplicacao IS NOT NULL AND
			ap.dt_aplicacao > GETDATE() AND
			aa.bl_ativo = 1 AND
			aa.nu_presenca IS NULL) THEN 0
		WHEN (ap.dt_aplicacao IS NOT NULL AND
			ap.dt_aplicacao > GETDATE() AND
			aa.bl_ativo = 0 AND
			aa.id_situacao = 68 AND
			aa.nu_presenca IS NOT NULL) THEN 1
		WHEN (ap.dt_aplicacao IS NOT NULL AND
			ap.dt_aplicacao < GETDATE() AND
			aa.bl_ativo = 1 AND
			aa.id_situacao = 68 AND
			aa.nu_presenca IS NULL) THEN 2
		ELSE 3
	END) AS id_situacaopresenca,
	(CASE
		WHEN (ap.dt_aplicacao IS NOT NULL AND
			ap.dt_aplicacao > GETDATE() AND
			aa.bl_ativo = 1 AND
			aa.nu_presenca IS NULL) THEN 'Aguardando aplicação de prova'
		WHEN (ap.dt_aplicacao IS NOT NULL AND
			ap.dt_aplicacao > GETDATE() AND
			aa.bl_ativo = 0 AND
			aa.id_situacao = 68 AND
			aa.nu_presenca IS NOT NULL) THEN 'Presença lançada'
		WHEN (ap.dt_aplicacao IS NOT NULL AND
			ap.dt_aplicacao < GETDATE() AND
			aa.bl_ativo = 1 AND
			aa.id_situacao = 68 AND
			aa.nu_presenca IS NULL) THEN 'Aplicação realizada e presença não lançada'
		ELSE 'Situação Indefinida'
	END) AS st_situacaopresenca,
	tb_chamada.id_aval AS id_chamada,
	primeira.id_primeirachamada,
	primeirapresenca.nu_primeirapresenca,
	primeira.id_segundachamada AS id_ultimachamada,
	ultima.nu_ultimapresenca,
	mt.id_evolucao,
	aa.bl_provaglobal,
	ap.dt_antecedenciaminima,
	ap.dt_alteracaolimite,

	(CASE
		WHEN aa.id_avaliacaoaplicacao IS NULL OR
			((CAST(GETDATE() AS DATE)) BETWEEN ap.dt_antecedenciaminima AND ap.dt_alteracaolimite AND
			aa.id_avaliacaoaplicacao = ap.id_avaliacaoaplicacao AND
			aa.bl_ativo = 1) THEN 0
		ELSE 1
	END) AS id_mensagens,

	(CASE
		WHEN (CAST(GETDATE() AS DATE)) < ap.dt_antecedenciaminima THEN 'A data para o agendamento ainda não chegou'
		WHEN (CAST(GETDATE() AS DATE)) > ap.dt_alteracaolimite THEN 'Agendamento passou do prazo limite para alteração'
		WHEN aa.id_avaliacaoaplicacao IS NULL THEN ''
		ELSE 'Agendamento dentro do prazo limite para alteração'
	END) AS st_mensagens,

	provasrealizadas.id_provasrealizadas,
	ISNULL(temnotafinal.bl_temnotafinal, 0) AS bl_temnotafinal,
	vw_pessoa.st_email,
	vw_pessoa.nu_telefone,
	vw_pessoa.nu_ddd,
	vw_pessoa.nu_ddi,
	ha.hr_inicio,
	ha.hr_fim,
	ap.sg_uf,
	ap.st_complemento,
	ap.nu_numero,
	ap.st_bairro,
	ap.st_telefone AS 'st_telefoneaplicador',
	NULL AS id_disciplina

FROM tb_avaliacaoagendamento aa
JOIN tb_matricula AS mt
	ON mt.id_matricula = aa.id_matricula
JOIN tb_entidade AS ett
	ON ett.id_entidade = mt.id_entidadeatendimento
JOIN tb_usuario AS us
	ON us.id_usuario = mt.id_usuario
JOIN tb_avaliacao AS av
	ON av.id_avaliacao = aa.id_avaliacao
INNER JOIN tb_avaliacaoconjuntorelacao AS acr
	ON acr.id_avaliacao = av.id_avaliacao
INNER JOIN tb_avaliacaoconjunto AS ac
	ON ac.id_avaliacaoconjunto = acr.id_avaliacaoconjunto
JOIN tb_tipoprova AS tp
	ON tp.id_tipoprova = ac.id_tipoprova
INNER JOIN tb_situacao AS st
	ON st.id_situacao = aa.id_situacao
JOIN tb_projetopedagogico AS pp
	ON pp.id_projetopedagogico = mt.id_projetopedagogico
JOIN vw_pessoa
	ON vw_pessoa.id_usuario = us.id_usuario
	AND vw_pessoa.id_entidade = mt.id_entidadematricula
JOIN tb_esquemaconfiguracao AS ec
	ON ett.id_esquemaconfiguracao = ec.id_esquemaconfiguracao
JOIN tb_esquemaconfiguracaoitem AS eci
	ON eci.id_esquemaconfiguracao = ec.id_esquemaconfiguracao
	AND eci.id_itemconfiguracao = 24
	AND eci.st_valor = 0
LEFT JOIN vw_avaliacaoaplicacao AS ap
	ON ap.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao
	AND ap.bl_ativo = 1
	AND ap.id_entidade = aa.id_entidade
LEFT JOIN tb_entidade AS et
	ON et.id_entidade = ap.id_entidade
LEFT JOIN tb_horarioaula AS ha
	ON ha.id_horarioaula = ap.id_horarioaula
LEFT JOIN tb_avalagendaintegracao AS aai
	ON aai.id_avaliacaoagendamento = aa.id_avaliacaoagendamento
OUTER APPLY (SELECT
	st_nomecompleto
FROM tb_usuario
WHERE id_usuario = aa.id_usuariolancamento) AS usu_lancamento
OUTER APPLY (SELECT
	COUNT(cha.id_avaliacaoagendamento) AS id_aval
FROM tb_avaliacaoagendamento AS cha
LEFT JOIN tb_avaliacaoaplicacao AS ch
	ON ch.id_avaliacaoaplicacao = cha.id_avaliacaoaplicacao
WHERE cha.id_matricula = mt.id_matricula
AND cha.id_tipodeavaliacao = aa.id_tipodeavaliacao
AND cha.id_situacao = 68) AS tb_chamada
OUTER APPLY (SELECT
	MIN(id_avaliacaoagendamento) AS id_primeirachamada,
	MAX(id_avaliacaoagendamento) AS id_segundachamada
FROM tb_avaliacaoagendamento
WHERE id_matricula = aa.id_matricula
AND id_situacao = 68
AND id_tipodeavaliacao = aa.id_tipodeavaliacao) AS primeira
OUTER APPLY (SELECT
	nu_presenca AS nu_primeirapresenca
FROM tb_avaliacaoagendamento
WHERE id_avaliacaoagendamento = (SELECT
	MIN(id_avaliacaoagendamento)
FROM tb_avaliacaoagendamento
WHERE id_matricula = aa.id_matricula
AND id_situacao = 68
AND id_tipodeavaliacao = aa.id_tipodeavaliacao)) AS primeirapresenca
OUTER APPLY (SELECT
	nu_presenca AS nu_ultimapresenca
FROM tb_avaliacaoagendamento
WHERE id_avaliacaoagendamento = (SELECT
	MAX(id_avaliacaoagendamento)
FROM tb_avaliacaoagendamento
WHERE id_matricula = aa.id_matricula
AND id_situacao = 68
AND id_tipodeavaliacao = aa.id_tipodeavaliacao)) AS ultima
OUTER APPLY (SELECT
	COUNT(id_avaliacaoagendamento) AS id_provasrealizadas
FROM tb_avaliacaoagendamento
WHERE id_matricula = aa.id_matricula
AND nu_presenca = 1
AND bl_ativo = 0
AND id_situacao = 68) AS provasrealizadas
OUTER APPLY (SELECT
	(CASE
		WHEN st_nota IS NULL THEN 0
		ELSE 1
	END) AS bl_temnotafinal
FROM tb_avaliacaoaluno
WHERE id_matricula = mt.id_matricula
AND id_avaliacao = aa.id_avaliacao
AND id_situacao = 86) AS temnotafinal
WHERE aa.id_situacao <> 70
AND ac.id_tipoprova = 2 UNION SELECT DISTINCT
	aa.id_avaliacaoagendamento,
	aa.id_matricula,
	us.id_usuario,
	us.st_nomecompleto,
	av.id_avaliacao,
	av.st_avaliacao,
	ac.id_tipoprova,
	tp.st_tipoprova,
	aa.dt_agendamento,
	aa.id_situacao,
	st.st_situacao,
	aa.id_avaliacaoaplicacao,
	aa.id_entidade,
	et.st_nomeentidade,
	ap.id_horarioaula,
	ha.st_horarioaula,
	mt.id_projetopedagogico,
	pp.st_projetopedagogico,
	aai.st_codsistema,
	aa.bl_ativo,
	ap.st_endereco,
	ap.dt_aplicacao,
	ap.id_aplicadorprova,
	ap.st_aplicadorprova,
	ap.st_cidade,
	aa.nu_presenca,
	aa.id_usuariolancamento,
	usu_lancamento.st_nomecompleto AS st_usuariolancamento,
	(CASE
		WHEN aa.id_tipodeavaliacao IS NULL THEN 0
		ELSE aa.id_tipodeavaliacao
	END) AS id_tipodeavaliacao,
	(CASE
		WHEN (ap.dt_aplicacao IS NOT NULL AND
			ap.dt_aplicacao > GETDATE() AND
			aa.bl_ativo = 1 AND
			aa.nu_presenca IS NULL) THEN 0
		WHEN (ap.dt_aplicacao IS NOT NULL AND
			ap.dt_aplicacao > GETDATE() AND
			aa.bl_ativo = 0 AND
			aa.id_situacao = 68 AND
			aa.nu_presenca IS NOT NULL) THEN 1
		WHEN (ap.dt_aplicacao IS NOT NULL AND
			ap.dt_aplicacao < GETDATE() AND
			aa.bl_ativo = 1 AND
			aa.id_situacao = 68 AND
			aa.nu_presenca IS NULL) THEN 2
		ELSE 3
	END) AS id_situacaopresenca,
	(CASE
		WHEN (ap.dt_aplicacao IS NOT NULL AND
			ap.dt_aplicacao > GETDATE() AND
			aa.bl_ativo = 1 AND
			aa.nu_presenca IS NULL) THEN 'Aguardando aplicação de prova'
		WHEN (ap.dt_aplicacao IS NOT NULL AND
			ap.dt_aplicacao > GETDATE() AND
			aa.bl_ativo = 0 AND
			aa.id_situacao = 68 AND
			aa.nu_presenca IS NOT NULL) THEN 'Presença lançada'
		WHEN (ap.dt_aplicacao IS NOT NULL AND
			ap.dt_aplicacao < GETDATE() AND
			aa.bl_ativo = 1 AND
			aa.id_situacao = 68 AND
			aa.nu_presenca IS NULL) THEN 'Aplicação realizada e presença não lançada'
		ELSE 'Situação Indefinida'
	END) AS st_situacaopresenca,
	tb_chamada.id_aval AS id_chamada,
	primeira.id_primeirachamada,
	primeirapresenca.nu_primeirapresenca,
	primeira.id_segundachamada AS id_ultimachamada,
	ultima.nu_ultimapresenca,
	mt.id_evolucao,
	aa.bl_provaglobal,
	ap.dt_antecedenciaminima,
	ap.dt_alteracaolimite,

	(CASE
		WHEN aa.id_avaliacaoaplicacao IS NULL OR
			((CAST(GETDATE() AS DATE)) BETWEEN ap.dt_antecedenciaminima AND ap.dt_alteracaolimite AND
			aa.id_avaliacaoaplicacao = ap.id_avaliacaoaplicacao AND
			aa.bl_ativo = 1) THEN 0
		ELSE 1
	END) AS id_mensagens,

	(CASE
		WHEN (CAST(GETDATE() AS DATE)) < ap.dt_antecedenciaminima THEN 'A data para o agendamento ainda não chegou'
		WHEN (CAST(GETDATE() AS DATE)) > ap.dt_alteracaolimite THEN 'Agendamento passou do prazo limite para alteração'
		WHEN aa.id_avaliacaoaplicacao IS NULL THEN ''
		ELSE 'Agendamento dentro do prazo limite para alteração'
	END) AS st_mensagens,

	provasrealizadas.id_provasrealizadas,
	ISNULL(temnotafinal.bl_temnotafinal, 0) AS bl_temnotafinal,
	vw_pessoa.st_email,
	vw_pessoa.nu_telefone,
	vw_pessoa.nu_ddd,
	vw_pessoa.nu_ddi,
	ha.hr_inicio,
	ha.hr_fim,
	ap.sg_uf,
	ap.st_complemento,
	ap.nu_numero,
	ap.st_bairro,
	ap.st_telefone AS 'st_telefoneaplicador',
	md.id_disciplina

FROM tb_avaliacaoagendamento aa
JOIN tb_avalagendamentoref AS agr
	ON aa.id_avaliacaoagendamento = agr.id_avaliacaoagendamento
JOIN tb_avaliacaoconjuntoreferencia AS acrf
	ON acrf.id_avaliacaoconjuntoreferencia = agr.id_avaliacaoconjuntoreferencia
JOIN tb_matricula AS mt
	ON mt.id_matricula = aa.id_matricula
JOIN tb_matriculadisciplina AS md
	ON md.id_matricula = mt.id_matricula
JOIN tb_alocacao AS al
	ON al.id_matriculadisciplina = md.id_matriculadisciplina
	AND al.bl_ativo = 1
	AND al.id_saladeaula = acrf.id_saladeaula
JOIN tb_entidade AS ett
	ON ett.id_entidade = mt.id_entidadeatendimento
JOIN tb_usuario AS us
	ON us.id_usuario = mt.id_usuario
JOIN tb_avaliacao AS av
	ON av.id_avaliacao = aa.id_avaliacao
INNER JOIN tb_avaliacaoconjuntorelacao AS acr
	ON acr.id_avaliacao = av.id_avaliacao
INNER JOIN tb_avaliacaoconjunto AS ac
	ON ac.id_avaliacaoconjunto = acr.id_avaliacaoconjunto
JOIN tb_tipoprova AS tp
	ON tp.id_tipoprova = ac.id_tipoprova
INNER JOIN tb_situacao AS st
	ON st.id_situacao = aa.id_situacao
JOIN tb_projetopedagogico AS pp
	ON pp.id_projetopedagogico = mt.id_projetopedagogico
JOIN vw_pessoa
	ON vw_pessoa.id_usuario = us.id_usuario
	AND vw_pessoa.id_entidade = mt.id_entidadematricula
JOIN tb_esquemaconfiguracao AS ec
	ON ett.id_esquemaconfiguracao = ec.id_esquemaconfiguracao
JOIN tb_esquemaconfiguracaoitem AS eci
	ON eci.id_esquemaconfiguracao = ec.id_esquemaconfiguracao
	AND eci.id_itemconfiguracao = 24
	AND eci.st_valor = 1
LEFT JOIN vw_avaliacaoaplicacao AS ap
	ON ap.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao
	AND ap.bl_ativo = 1
	AND ap.id_entidade = aa.id_entidade
LEFT JOIN tb_entidade AS et
	ON et.id_entidade = ap.id_entidade
LEFT JOIN tb_horarioaula AS ha
	ON ha.id_horarioaula = ap.id_horarioaula
LEFT JOIN tb_avalagendaintegracao AS aai
	ON aai.id_avaliacaoagendamento = aa.id_avaliacaoagendamento
OUTER APPLY (SELECT
	st_nomecompleto
FROM tb_usuario
WHERE id_usuario = aa.id_usuariolancamento) AS usu_lancamento
OUTER APPLY (SELECT
	COUNT(cha.id_avaliacaoagendamento) AS id_aval
FROM tb_avaliacaoagendamento AS cha
JOIN tb_avalagendamentoref AS cagr ON cagr.id_avaliacaoagendamento = cha.id_avaliacaoagendamento and cagr.id_avaliacaoconjuntoreferencia = acrf.id_avaliacaoconjuntoreferencia
LEFT JOIN tb_avaliacaoaplicacao AS ch
	ON ch.id_avaliacaoaplicacao = cha.id_avaliacaoaplicacao
WHERE cha.id_matricula = mt.id_matricula
AND cha.id_tipodeavaliacao = aa.id_tipodeavaliacao
AND cha.id_situacao = 68) AS tb_chamada
OUTER APPLY (SELECT
	MIN(id_avaliacaoagendamento) AS id_primeirachamada,
	MAX(id_avaliacaoagendamento) AS id_segundachamada
FROM tb_avaliacaoagendamento
WHERE id_matricula = aa.id_matricula
AND id_situacao = 68
AND id_tipodeavaliacao = aa.id_tipodeavaliacao) AS primeira
OUTER APPLY (SELECT
	nu_presenca AS nu_primeirapresenca
FROM tb_avaliacaoagendamento
WHERE id_avaliacaoagendamento = (SELECT
	MIN(id_avaliacaoagendamento)
FROM tb_avaliacaoagendamento
WHERE id_matricula = aa.id_matricula
AND id_situacao = 68
AND id_tipodeavaliacao = aa.id_tipodeavaliacao)) AS primeirapresenca
OUTER APPLY (SELECT
	nu_presenca AS nu_ultimapresenca
FROM tb_avaliacaoagendamento
WHERE id_avaliacaoagendamento = (SELECT
	MAX(id_avaliacaoagendamento)
FROM tb_avaliacaoagendamento
WHERE id_matricula = aa.id_matricula
AND id_situacao = 68
AND id_tipodeavaliacao = aa.id_tipodeavaliacao)) AS ultima
OUTER APPLY (SELECT
	COUNT(id_avaliacaoagendamento) AS id_provasrealizadas
FROM tb_avaliacaoagendamento
WHERE id_matricula = aa.id_matricula
AND nu_presenca = 1
AND bl_ativo = 0
AND id_situacao = 68) AS provasrealizadas
OUTER APPLY (SELECT
	(CASE
		WHEN st_nota IS NULL THEN 0
		ELSE 1
	END) AS bl_temnotafinal
FROM tb_avaliacaoaluno
WHERE id_matricula = mt.id_matricula
AND id_avaliacao = aa.id_avaliacao
AND id_situacao = 86) AS temnotafinal
WHERE aa.id_situacao <> 70
AND ac.id_tipoprova = 2
GO
CREATE VIEW vw_avaliacaoprimeiroagendamento AS (

  SELECT DISTINCT
    ma.id_matricula,
    ma.id_alocacao,
    ma.id_matriculadisciplina,
    ma.id_usuario,
    ma.st_nomecompleto,
    ma.id_entidadeatendimento,
    ma.st_nomeentidade,
    av.id_avaliacao,
    av.id_tipoavaliacao,
    av.st_avaliacao,
    ma.id_disciplina,
    ma.st_disciplina,
    av.id_avaliacaoconjuntoreferencia

  -- Buscar somente de alunos que possuem alocacao ativa
  FROM vw_matriculaalocacao AS ma

  JOIN tb_matricula AS m
    ON m.id_matricula = ma.id_matricula
    AND m.bl_ativo = 1
    AND m.id_evolucao = 6

  -- Buscar avaliacoes do aluno
  JOIN vw_avaliacaoaluno AS av
    ON av.id_matricula = ma.id_matricula
    AND av.id_disciplina = ma.id_disciplina
    AND av.id_avaliacaorecupera = 0
    AND av.bl_recuperacao = 0
    AND av.id_tipoavaliacao IN (1,4)
    AND av.id_evolucaodisciplina IN (13, 19)
    AND av.nu_cargahoraria != 0

  -- Somente alunos que estao na lista de APTOS
  --JOIN vw_alunosagendamento AS aa ON aa.id_matricula = ma.id_matricula

  -- Procurar o agendamento do aluno em tal avaliacao/disciplina
  LEFT JOIN vw_avaliacaoagendamento AS ag
    ON ag.id_matricula = ma.id_matricula
    AND ag.id_disciplina = ma.id_disciplina
    AND ag.id_avaliacao = av.id_avaliacao

  WHERE
  -- Aluno nao pode ter nenhum agendamento desta disciplina
  ag.id_avaliacaoagendamento IS NULL
  -- A situacao do agendamento tem que ser APTO
  AND ma.id_situacaoagendamento = 120

)
GO

ALTER view [dbo].[vw_avaliacaoaluno_simples] as
SELECT DISTINCT
  mt.id_entidadematricula ,
  id_avaliacaoaluno ,
  mt.id_matricula ,
  us.st_nomecompleto ,
  aa.st_nota ,
  acr.id_avaliacao ,
  av.st_avaliacao ,
  md.id_situacao ,
  st.st_situacao ,
  ac.id_tipoprova ,
  md.id_evolucao ,
  ev.st_evolucao ,
  alc.id_saladeaula,
  aa.bl_ativo,
  md.id_disciplina,
  dc.st_tituloexibicao as st_tituloexibicaodisciplina,
  CAST(av.nu_valor AS VARCHAR(100)) AS nu_notamax ,
  ac.id_tipocalculoavaliacao ,
  av.id_tipoavaliacao ,
  ta.st_tipoavaliacao ,
  ( CASE WHEN acr.id_avaliacaorecupera IS NULL THEN 0
    ELSE acr.id_avaliacaorecupera
    END ) AS id_avaliacaorecupera ,
  pp.nu_notamaxima ,
  pp.nu_percentualaprovacao ,
  ac.id_avaliacaoconjunto ,
  pp.st_projetopedagogico ,
  mt.id_projetopedagogico ,
  aa.dt_defesa ,
  aa.st_tituloavaliacao ,
  mt.id_entidadeatendimento ,
  md.id_matriculadisciplina ,
  aa.id_tiponota ,
  sdis.id_evolucao AS id_evolucaodisciplina ,
  sdis.st_evolucao AS st_evolucaodisciplina ,
  dc.nu_cargahoraria
FROM  tb_matriculadisciplina AS md
  JOIN tb_matricula AS mt ON md.id_matricula = mt.id_matricula
  JOIN dbo.tb_usuario AS us ON us.id_usuario = mt.id_usuario
  JOIN tb_disciplina as dc on dc.id_disciplina = md.id_disciplina
  JOIN tb_alocacao AS alc ON alc.id_matriculadisciplina = md.id_matriculadisciplina
                             AND alc.bl_ativo = 1
   JOIN tb_projetopedagogico AS PP ON pp.id_projetopedagogico = mt.id_projetopedagogico
  JOIN tb_avaliacaoconjuntoreferencia AS acrf ON acrf.id_saladeaula = alc.id_saladeaula
                                                 AND acrf.dt_fim IS NULL
  --JOIN tb_disciplinasaladeaula AS dsa ON dsa.id_saladeaula = alc.id_saladeaula
  JOIN tb_evolucao AS sdis ON sdis.id_evolucao = md.id_evolucao
  JOIN tb_avaliacaoconjunto AS ac ON ac.id_avaliacaoconjunto = acrf.id_avaliacaoconjunto
                                     AND id_tipoprova = 2
  JOIN tb_avaliacaoconjuntorelacao AS acr ON acr.id_avaliacaoconjunto = ac.id_avaliacaoconjunto
  JOIN tb_avaliacao AS av ON av.id_avaliacao = acr.id_avaliacao
  JOIN tb_tipoavaliacao AS ta ON ta.id_tipoavaliacao = av.id_tipoavaliacao
  LEFT JOIN tb_avaliacaoaluno AS aa ON aa.id_matricula = mt.id_matricula
                                       AND aa.id_avaliacaoconjuntoreferencia = acrf.id_avaliacaoconjuntoreferencia
                                       AND aa.id_avaliacao = av.id_avaliacao
                                       AND aa.bl_ativo = 1
                                       AND aa.id_situacao = 86
  JOIN tb_situacao AS st ON st.id_situacao = md.id_situacao
  JOIN tb_evolucao AS ev ON ev.id_evolucao = md.id_evolucao
WHERE   mt.bl_ativo = 1 and dc.id_tipodisciplina <> 3



GO
ALTER VIEW [dbo].[vw_matriculanota]
AS
SELECT
  mt.id_matricula,
  md.id_matriculadisciplina,
  al.id_saladeaula,
  id_alocacao,
  -- atividade = 1 - Atividade Presencial
  atividade.id_avaliacao AS id_avaliacaoatividade,
  atividade.st_nota AS st_notaatividade,
  atividade.id_tiponota AS id_tiponotaatividade,
  atividade.st_avaliacao AS st_avaliacaoatividade,
  -- EAD = 5
  ead.id_avaliacao AS id_avaliacaoead,
  ead.st_nota AS st_notaead,
  ead.id_tiponota AS id_tiponotaead,
  ead.st_avaliacao AS st_avaliacaoead,
  -- FINAL = 4
  final.id_avaliacao AS id_avaliacaofinal,
  final.id_disciplina AS id_disciplinafinal,
  final.st_tituloexibicaodisciplina AS st_disciplinafinal,
  final.st_nota AS st_notafinal,
  final.id_tipoavaliacao,
  final.id_tiponota AS id_tiponotafinal,
  final.st_avaliacao AS st_avaliacaofinal,

  tcc.id_avaliacao AS id_avaliacaotcc,
  tcc.st_nota AS st_notatcc,
  tcc.id_tiponota AS id_tiponotatcc,

  -- recuperacao = 2
  recuperacao.id_avaliacao AS id_avaliacaorecuperacao,
  recuperacao.st_nota AS st_notarecuperacao,
  recuperacao.id_tiponota AS id_tiponotarecuperacao,
  recuperacao.st_avaliacao AS st_avaliacaorecuperacao,

  /*(ISNULL(CAST(final.st_nota AS INT), 0)
  + ISNULL(CAST(atividade.st_nota AS int), 0)
  + ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
  + ISNULL(CAST(ead.st_nota AS INT), 0)) AS nu_notatotal,

  */
  nu_notatotal = CASE WHEN recuperacao.st_nota is NOT NULL AND recuperacao.id_tipocalculoavaliacao = 2
							THEN ( (ISNULL(CAST(final.st_nota AS INT), 0)
						   + ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
						   + ISNULL(CAST(atividade.st_nota AS int), 0)
						   + ISNULL(CAST(recuperacao.st_nota AS INT), 0)
						   + ISNULL(CAST(ead.st_nota AS INT), 0))/2  )
						  WHEN  recuperacao.st_nota is NOT NULL
							THEN (ISNULL(CAST(final.st_nota AS INT), 0)
								+ ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
								+ ISNULL(CAST(atividade.st_nota AS int), 0)
								+ ISNULL(CAST(recuperacao.st_nota AS INT), 0)
								+ ISNULL(CAST(ead.st_nota AS INT), 0))
						  ELSE
							   (ISNULL(CAST(final.st_nota AS INT), 0)
									+ ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
									+ ISNULL(CAST(atividade.st_nota AS int), 0)
									+ ISNULL(CAST(ead.st_nota AS INT), 0))

				 END,
	CAST(((pp.nu_notamaxima * pp.nu_percentualaprovacao) / 100) AS INT) nu_notaaprovacao,
  bl_calcular =
               CASE
                 WHEN ead.id_avaliacao IS NOT NULL AND
                   ead.st_nota IS NULL THEN 0
                 WHEN atividade.id_avaliacao IS NOT NULL AND
                   atividade.st_nota IS NULL THEN 0
                 WHEN final.id_avaliacao IS NOT NULL AND
                   final.st_nota IS NULL THEN 0
                 WHEN tcc.id_avaliacao IS NOT NULL AND
                   tcc.dt_defesa IS NULL THEN 0
                 ELSE 1
               END,
  bl_aprovado =
               CASE
                 WHEN recuperacao.st_nota is null AND (ISNULL(CAST(final.st_nota AS INT), 0)
                   + ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
                   + ISNULL(CAST(atividade.st_nota AS int), 0)
                   + ISNULL(CAST(ead.st_nota AS INT), 0)) >= CAST(((pp.nu_notamaxima
                   * pp.nu_percentualaprovacao)
                   / 100) AS INT) THEN 1
                 WHEN  recuperacao.st_nota is null AND (ISNULL(CAST(final.st_nota AS INT), 0)
                   + ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
                   + ISNULL(CAST(atividade.st_nota AS int), 0)
                   + ISNULL(CAST(ead.st_nota AS INT), 0)) < CAST(((pp.nu_notamaxima
                   * pp.nu_percentualaprovacao)
                   / 100) AS INT) THEN 0
				 WHEN recuperacao.st_nota is NOT NULL AND recuperacao.id_tipocalculoavaliacao = 2
					 AND ( (ISNULL(CAST(final.st_nota AS INT), 0)
					   + ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
					   + ISNULL(CAST(atividade.st_nota AS int), 0)
					   + ISNULL(CAST(recuperacao.st_nota AS INT), 0)
					   + ISNULL(CAST(ead.st_nota AS INT), 0))/2  ) >= CAST(((pp.nu_notamaxima * pp.nu_percentualaprovacaovariavel)
					   / 100) AS INT) THEN 1
				  WHEN  recuperacao.st_nota is NOT NULL AND (ISNULL(CAST(final.st_nota AS INT), 0)
						+ ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
						+ ISNULL(CAST(atividade.st_nota AS int), 0)
						+ ISNULL(CAST(recuperacao.st_nota AS INT), 0)
						+ ISNULL(CAST(ead.st_nota AS INT), 0)) < CAST(((pp.nu_notamaxima * pp.nu_percentualaprovacaovariavel )
						/ 100) AS INT) THEN 0
                 ELSE 0

               END,

              (CASE
                -- Se tiver RECUPERACAO e o tipo de calculo for MEDIA VARIAVEL, somar todas as notas e dividir por 2
                WHEN recuperacao.st_nota IS NOT NULL AND recuperacao.id_tipocalculoavaliacao = 2 THEN (
                  ( ISNULL(CAST(final.st_nota AS INT), 0)
                    + ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
                    + ISNULL(CAST(ead.st_nota AS INT), 0)
                    + ISNULL(CAST(atividade.st_nota AS INT), 0)
                    + ISNULL(CAST(recuperacao.st_nota AS INT), 0)
                  ) / 2
                ) ELSE (
                  ( ISNULL(CAST(final.st_nota AS INT), 0)
                    + ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
                    + ISNULL(CAST(ead.st_nota AS INT), 0)
                    + ISNULL(CAST( (CASE WHEN recuperacao.st_nota IS NOT NULL THEN recuperacao.st_nota ELSE atividade.st_nota END) AS INT), 0)
                  )
                )
              END)
              -- trata porque esse valor pode estar salvo como 0
              / (CASE WHEN pp.nu_notamaxima IS NULL OR pp.nu_notamaxima = 0 THEN 1 ELSE pp.nu_notamaxima END) * 100
              AS nu_percentualfinal

				FROM tb_matriculadisciplina AS md
				JOIN tb_matricula AS mt
				  ON md.id_matricula = mt.id_matricula
				JOIN dbo.tb_projetopedagogico AS pp
				  ON mt.id_projetopedagogico = pp.id_projetopedagogico
				JOIN dbo.tb_alocacao AS al
				  ON al.id_matriculadisciplina = md.id_matriculadisciplina
				  AND al.bl_ativo = 1
				LEFT JOIN dbo.vw_avaliacaoaluno_simples AS ead
				  ON ead.id_matriculadisciplina = md.id_matriculadisciplina
				  AND al.id_saladeaula = ead.id_saladeaula
				  AND ead.id_tipoavaliacao = 5
				LEFT JOIN dbo.vw_avaliacaoaluno_simples AS atividade
				  ON atividade.id_matriculadisciplina = md.id_matriculadisciplina
				  AND al.id_saladeaula = atividade.id_saladeaula
				  AND atividade.id_tipoavaliacao = 1

				OUTER APPLY (SELECT TOP 1
							  aaf.st_nota,
							  aaf.id_avaliacao,
							  aaf.id_disciplina,
							  aaf.st_tituloexibicaodisciplina,
							  aaf.id_tipoavaliacao
							  , aaf.id_tiponota
							  , aaf.st_avaliacao
							FROM dbo.vw_avaliacaoaluno_simples AS aaf
							WHERE md.id_matriculadisciplina = aaf.id_matriculadisciplina
							AND aaf.id_tipoavaliacao = 4
							ORDER BY cast(aaf.st_nota as NUMERIC) DESC) AS final
				LEFT JOIN dbo.vw_avaliacaoaluno_simples AS tcc
				  ON tcc.id_matriculadisciplina = md.id_matriculadisciplina
				  AND al.id_saladeaula = tcc.id_saladeaula
				  AND tcc.id_tipoavaliacao = 6

				  --add recuperacao
				 LEFT JOIN dbo.vw_avaliacaoaluno_simples AS recuperacao
				  ON recuperacao.id_matriculadisciplina = md.id_matriculadisciplina
				  AND al.id_saladeaula = recuperacao.id_saladeaula
				  AND recuperacao.id_tipoavaliacao = 2




GO
ALTER VIEW [dbo].[vw_gradetipotcc] AS
	SELECT  DISTINCT sa.dt_abertura,
		pp.id_projetopedagogico,
		pp.st_projetopedagogico,
		sa.st_saladeaula,
		aa.id_avaliacao ,
			st_nota = CASE WHEN dc.id_tipodisciplina = 2 AND aa.dt_defesa IS NULL AND aa.bl_ativo = 1
			THEN NULL
								ELSE CAST(aa.st_nota AS NUMERIC(8,2))
								END ,
		mdc.id_modulo,
		mdl.st_modulo,
			mdl.st_tituloexibicao AS st_tituloexibicaomodulo,
		md.id_disciplina,
			dc.st_tituloexibicao AS st_disciplina,
			dc.st_tituloexibicao AS st_tituloexibicaodisciplina,
		mt.id_matricula ,
			id_tipoavaliacao = CASE WHEN tt.id_tipoavaliacao IS NULL THEN 5
												 ELSE tt.id_tipoavaliacao
												 END ,
			aa.st_tipoavaliacao AS st_labeltipo,
			nu_notafinal = CASE WHEN dc.id_tipodisciplina = 2 AND aa.dt_defesa IS NULL THEN NULL
										 ELSE CAST((md.nu_aprovafinal* pp.nu_notamaxima/100)AS NUMERIC(5,2))
										 END ,
		ev.st_evolucao,
		st.st_situacao,
		dc.nu_cargahoraria,
		sa.id_saladeaula,
		sa.dt_encerramento,
		ndm.nu_notatotal,
			st_status = CASE WHEN  ea.id_alocacao IS NOT NULL AND  md.id_evolucao = 12 THEN ev.st_evolucao
									WHEN  ea.id_alocacao IS NOT NULL AND  md.id_evolucao = 19 THEN ev.st_evolucao
									WHEN  ea.id_alocacao IS NOT NULL AND  md.id_situacao = 65 THEN 'Crédito Concedido'
									WHEN  ea.id_alocacao IS NOT NULL AND  (ndm.nu_notatotal + nf.nu_notafaltante) >= (pp.nu_notamaxima * pp.nu_percentualaprovacao/100) THEN 'Satisfatório'
									WHEN  ea.id_alocacao IS NOT NULL AND  (ndm.nu_notatotal + nf.nu_notafaltante) < (pp.nu_notamaxima * pp.nu_percentualaprovacao/100) THEN 'Insatisfatório'
									ELSE '-'
									END
		,bl_status  = CASE WHEN md.id_evolucao = 12 THEN 1
									WHEN md.id_evolucao = 19 THEN 0
									WHEN md.id_situacao = 65 THEN 1
									WHEN (ndm.nu_notatotal + nf.nu_notafaltante) >= (pp.nu_notamaxima * pp.nu_percentualaprovacao/100) THEN 1
									WHEN (ndm.nu_notatotal + nf.nu_notafaltante) < (pp.nu_notamaxima * pp.nu_percentualaprovacao/100) THEN 0
									ELSE 0
									END
									, st_statusdisciplina = CASE
							WHEN al.id_alocacao IS NULL
							    AND md.id_situacao != 65
                                  THEN 'Não Iniciada'
                             WHEN ea.id_alocacao IS NOT NULL
                                  AND md.id_evolucao = 12
								  AND ndm.bl_aprovado = 1
								   THEN 'Aprovada'
							 WHEN ea.id_alocacao IS NOT NULL
								  AND ndm.bl_aprovado = 0
								  AND md.id_evolucao = 19
                             THEN 'Reprovado'
							 WHEN md.id_aproveitamento IS NOT NULL
                                  AND md.id_situacao = 65
                             THEN 'Isento'
                             WHEN al.id_alocacao IS NOT NULL
								  AND md.id_aproveitamento IS NULL
                                  AND( md.nu_aprovafinal IS NULL OR ea.id_alocacao IS NULL)
								  AND ( (md.id_situacao = 13 AND (ndm.st_notaead IS NULL OR ndm.st_notaatividade IS NULL OR ndm.st_notafinal IS NULL AND ndm.st_notarecuperacao IS NULL))
										OR  (md.id_situacao != 13 AND ea.id_alocacao IS NULL) )
                             THEN 'Cursando'


                             ELSE '-'
                        END
	, md.id_situacao
    , nu_ordem = CASE WHEN tt.id_tipoavaliacao = 1
                                  THEN 1
					  WHEN tt.id_tipoavaliacao =  4 THEN 2
					  WHEN tt.id_tipoavaliacao =  5 THEN 3
					  WHEN tt.id_tipoavaliacao = 2 THEN 4
					  ELSE 10
					  END
	FROM    dbo.tb_matricula AS mt
		JOIN dbo.tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
		JOIN tb_evolucao AS ev ON ev.id_evolucao = md.id_evolucao
		JOIN dbo.tb_situacao AS st ON st.id_situacao = md.id_situacao
		JOIN dbo.tb_disciplina AS dc ON dc.id_disciplina = md.id_disciplina
		LEFT JOIN dbo.tb_alocacao AS al ON al.id_matriculadisciplina = md.id_matriculadisciplina AND al.bl_ativo = 1
		LEFT JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula
		LEFT JOIN tb_encerramentoalocacao AS ea ON ea.id_alocacao = al.id_alocacao
		JOIN dbo.tb_tipoavaliacao AS tt ON tt.id_tipoavaliacao IN  (1,4,5,6)
		LEFT JOIN dbo.vw_avaliacaoaluno_simples AS aa ON aa.id_tipoavaliacao = tt.id_tipoavaliacao and aa.id_matriculadisciplina = md.id_matriculadisciplina
		--AND mt.id_matricula = aa.id_matricula AND aa.id_disciplina = md.id_disciplina AND aa.id_tipoprova = 2 AND aa.id_alocacao = al.id_alocacao
		JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
		JOIN dbo.tb_modulo AS mdl ON mdl.id_projetopedagogico = pp.id_projetopedagogico AND mdl.bl_ativo = 1
		JOIN dbo.tb_modulodisciplina AS mdc ON mdc.id_modulo = mdl.id_modulo AND mdc.id_disciplina = dc.id_disciplina   AND mdc.bl_ativo = 1
		LEFT JOIN vw_matriculanota AS ndm ON ndm.id_matriculadisciplina = md.id_matriculadisciplina and ndm.id_alocacao = al.id_alocacao

		OUTER APPLY ( SELECT    id_matricula ,
                                        id_disciplina ,
                                        SUM(CAST(nu_notamax AS FLOAT)) AS nu_notafaltante
                              FROM      vw_avaliacaoaluno_simples
                              WHERE     bl_ativo IS NULL
                                        AND id_matricula = mt.id_matricula
                                        AND id_disciplina = md.id_disciplina
                                        AND id_tipoprova = 2
										 AND id_avaliacaorecupera = 0
                              GROUP BY  id_disciplina ,
                                        id_matricula
                            ) AS nf


GO
alter VIEW [dbo].[vw_pesquisaavaliacao] AS
SELECT DISTINCT
av.id_avaliacao,
av.st_avaliacao,
av.id_tipoavaliacao,
CAST ( av.nu_valor AS int ) AS nu_valor,
av.dt_cadastro,
av.id_usuariocadastro,
av.id_situacao,
av.st_descricao,
av.nu_quantquestoes,
av.st_linkreferencia,
ta.st_tipoavaliacao,
s.st_situacao,
av.bl_recuperacao,
NULL AS id_avaliacaorecupera, -- este campo é preenchido manualmente na busca
er.id_entidade,
er.st_nomeentidade AS st_entidade,
av.id_entidade AS id_entidadeavaliacao,
en.st_nomeentidade AS st_entidadeavaliacao
FROM tb_avaliacao av
JOIN tb_tipoavaliacao AS ta ON ta.id_tipoavaliacao = av.id_tipoavaliacao
JOIN tb_situacao AS s ON s.id_situacao = av.id_situacao
JOIN tb_entidade AS en ON en.id_entidade = av.id_entidade
JOIN vw_entidaderecursiva AS er ON av.id_entidade IN(er.id_entidade, er.id_entidadepai)
go
ALTER VIEW [dbo].[vw_gerardeclaracao]
AS
    SELECT DISTINCT
            mt.id_matricula ,
            ps.id_usuario AS id_usuarioaluno ,
            ps.st_nomecompleto AS st_nomecompletoaluno ,
            ps.st_login AS st_loginaluno ,
            ps.st_senhaentidade AS st_senhaaluno ,
            ps.st_cpf AS st_cpfaluno ,
            ps.st_rg AS st_rgaluno ,
            ps.st_orgaoexpeditor ,
            ps.dt_dataexpedicao AS dt_dataexpedicaoaluno ,
            ps.st_nomemae AS st_filiacao ,
            ps.st_nomepai AS st_pai,
            ps.st_ufnascimento,
            ps.st_municipionascimento,
            ps.id_pais,
            dc.st_nomeinstituicao,
            dc.dt_anoconclusao AS ano_conclusao_medio,
            dcm.st_nomemunicipio as st_municipioinstituicao,
            CONVERT(VARCHAR(4), DATEPART(YEAR, mt.dt_concluinte), 103) as ano_conclusao,
            semestre_conclusao = (
                                  CASE WHEN
                                    CONVERT(VARCHAR(2), DATEPART(MONTH, mt.dt_concluinte), 103)<7
                                    THEN 1
                                  WHEN CONVERT(VARCHAR(2), DATEPART(MONTH, mt.dt_concluinte), 103)>=7
                                    THEN 2
                                  ELSE NULL
                                  END),
            mc.dt_cadastro AS dt_expedicaocertificado,
            CONVERT(CHAR, mt.dt_cadastro, 103) AS st_datamatricula ,
            canc.id_cancelamento ,
            canc.st_observacao AS st_observacaocancelamento ,
            canc.st_observacaocalculo AS st_observacao_calculo ,
            CONVERT(CHAR, canc.dt_solicitacao, 103) AS st_solicitacaocancelamento ,
            canc.nu_valorcarta AS st_valorcredito ,
            CONVERT(CHAR, DATEADD(D, 365, canc.dt_solicitacao), 103) AS st_validadecredito ,
            cm.id_contrato ,
            ps.dt_nascimento AS dt_nascimentoaluno ,
            UPPER(SUBSTRING(ps.st_nomemunicipio, 1, 1))
            + LOWER(SUBSTRING(ps.st_nomemunicipio, 2, 499)) AS st_municipioaluno ,
            ps.st_estadoprovincia AS st_ufaluno ,
            ps.sg_uf AS sg_ufaluno ,
            ps.nu_ddd AS nu_dddaluno ,
            ps.nu_telefone AS nu_telefonealuno ,
            ps.st_email AS st_emailaluno ,
            pp.st_tituloexibicao AS st_projeto ,
	--pp.nu_cargahoraria,
            ( SELECT    SUM(dis.nu_cargahoraria) AS nu_cargahoraria
              FROM      tb_matriculadisciplina md
                        JOIN tb_matricula m ON m.id_matricula = md.id_matricula
                        JOIN tb_disciplina dis ON dis.id_disciplina = md.id_disciplina
              WHERE     md.id_matricula = mt.id_matricula
            ) AS nu_cargahoraria ,
            ne.st_nivelensino ,
            mt.dt_concluinte AS dt_concluintealuno ,
            us_coordenador.st_nomecompleto AS st_coordenadorprojeto ,
            fl.id_fundamentolegal AS id_fundamentolei ,
            fl.nu_numero AS nu_lei ,
            flr.id_fundamentolegal AS id_fundamentoresolucao ,
            tur.id_turma AS id_turma ,
            tur.st_turma ,
            tur.dt_inicio AS dt_inicioturma ,
            tur.dt_fim AS dt_terminoturma ,
            flr.nu_numero AS nu_resolucao ,
            flpr.id_fundamentolegal AS id_fundamentoparecer ,
            flpr.nu_numero AS nu_parecer ,
            flp.id_fundamentolegal AS id_fundamentoportaria ,
            flp.nu_numero AS nu_portaria ,
            et.id_entidade ,
            et.st_nomeentidade ,
            et.st_razaosocial ,
            et.st_cnpj ,
            et.st_urlimglogo ,
            et.st_urlsite ,
            ps.st_nomepais ,
            ps.st_sexo AS sexo_aluno ,
            UPPER(tcc.st_tituloavaliacao) AS st_titulomonografia ,
            CONVERT(VARCHAR(2), DATEPART(DAY, GETDATE()), 103) + ' de '
            + dbo.fn_mesporextenso(GETDATE()) + ' de '
            + CONVERT(VARCHAR(4), DATEPART(YEAR, GETDATE()), 103) AS st_data ,
       DAY(GETDATE()) AS st_dia ,
            LOWER(dbo.fn_mesporextenso(GETDATE())) AS st_mes ,
            YEAR(GETDATE()) AS st_ano ,

            --CONVERT(DATE, sa.dt_abertura, 103) AS dt_primeirasala ,
            (
                SELECT TOP 1 ts.dt_abertura
                FROM tb_matriculadisciplina tm
                JOIN tb_alocacao ta ON tm.id_matriculadisciplina = ta.id_matriculadisciplina
                JOIN tb_saladeaula ts ON ta.id_saladeaula = ts.id_saladeaula
                WHERE tm.id_matricula = mt.id_matricula AND tm.bl_obrigatorio = 1
                ORDER BY ts.dt_abertura ASC
            ) AS dt_primeirasala,

            --CONVERT(DATE, DATEADD(MONTH, 13, sa.dt_abertura), 103) AS dt_previsaofim ,
            (
                SELECT TOP 1
                --DATEADD(MONTH, 13, ts.dt_abertura)

                'dt_previsaotermino' = CASE WHEN ts.id_categoriasala = 2 THEN (
                    DATEADD(day, ISNULL(ts.nu_diasaluno, 0), ts.dt_abertura)
                ) ELSE (
                    CASE WHEN ts.dt_encerramento IS NOT NULL THEN (
                        DATEADD(day, ISNULL(ts.nu_diasextensao, 0) + 30, ts.dt_encerramento)
                    ) WHEN ts.dt_abertura IS NOT NULL THEN (
                        DATEADD(day, ISNULL(ts.nu_diasextensao, 0) + 30, ts.dt_abertura)
                    ) ELSE (
                        NULL
                    ) END
                ) END

                FROM tb_matriculadisciplina tm
                JOIN tb_alocacao ta ON tm.id_matriculadisciplina = ta.id_matriculadisciplina
                JOIN tb_saladeaula ts ON ta.id_saladeaula = ts.id_saladeaula
                WHERE tm.id_matricula = mt.id_matricula AND tm.bl_obrigatorio = 1
                ORDER BY dt_previsaotermino DESC, ts.dt_abertura DESC, ts.dt_encerramento DESC
            ) AS dt_previsaofim,

            UPPER(tcc.st_tituloavaliacao) AS st_titulotcc,
            tcc.dt_aberturasala AS dt_iniciotcc,
            tcc.dt_encerramentosala AS dt_terminotcc,
            tcc.st_nota AS nu_notatcc,
            tcc.dt_cadastroavaliacao AS dt_cadastrotcc,
            tcc.dt_defesa AS dt_defesatcc,

            mt.st_codcertificacao ,
            CONVERT(CHAR, GETDATE(), 103) AS st_atual
			, matc.st_codigoacompanhamento AS st_codigorasteamento,

			mt.id_matriculaorigem,
			mto.dt_inicio AS dt_iniciomatriculaorigem
			, CONVERT(VARCHAR(2), DATEPART(day, GetDate()))     + ' de ' +  dbo.fn_mesporextenso(GetDate())   + ' de ' +  CONVERT(VARCHAR(4), DATEPART(year, GetDate())) as st_atualextenso,
			 CONVERT(VARCHAR(2), DATEPART(day, tur.dt_fim))     + ' de ' +  dbo.fn_mesporextenso(tur.dt_fim)   + ' de ' +  CONVERT(VARCHAR(4), DATEPART(year, tur.dt_fim)) as st_terminoturmaextenso,
			 CONVERT(VARCHAR(2), DATEPART(day, tur.dt_inicio))     + ' de ' +  dbo.fn_mesporextenso(tur.dt_inicio)   + ' de ' +  CONVERT(VARCHAR(4), DATEPART(year, tur.dt_inicio)) as st_inicioturmaextenso

			, CONVERT(CHAR, venda.dt_limiterenovacao, 103) AS st_limiterenovacao
			, venda.id_venda AS id_venda
			, venda.nu_mensalidade AS nu_mensalidade
			, venda.nu_mensalidadepon AS nu_mensalidadepontualidade
			, ' - ' AS st_link_grade_portal
			, ' - ' AS st_municipioufagendamento
			, ac.st_areaconhecimento

    FROM    tb_matricula mt
            JOIN tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
            JOIN tb_entidade AS et ON et.id_entidade = mt.id_entidadeatendimento
            LEFT JOIN tb_entidadeendereco AS ete ON ete.id_entidade = et.id_entidade
                                                    AND ete.bl_padrao = 1
            LEFT JOIN tb_endereco AS etee ON etee.id_endereco = ete.id_endereco
            JOIN vw_pessoa AS ps ON ps.id_usuario = mt.id_usuario
                                    AND ps.id_entidade = mt.id_entidadeatendimento
            LEFT JOIN tb_fundamentolegal AS fl ON fl.id_entidade = mt.id_entidadematriz
                                                  AND fl.id_tipofundamentolegal = 1
                                                AND fl.dt_publicacao <= GETDATE()
                                                  AND fl.dt_vigencia >= GETDATE()
            LEFT JOIN tb_fundamentolegal AS flp ON flp.id_entidade = mt.id_entidadematriz
                                                   AND flp.id_tipofundamentolegal = 2
                                                   AND flp.dt_publicacao <= GETDATE()
                                                   AND flp.dt_vigencia >= GETDATE()
            LEFT JOIN tb_fundamentolegal AS flr ON flr.id_entidade = mt.id_entidadematriz
                                                   AND flr.id_tipofundamentolegal = 3
          AND flr.dt_publicacao <= GETDATE()
                                                   AND flr.dt_vigencia >= GETDATE()
            LEFT JOIN tb_fundamentolegal AS flpr ON flpr.id_entidade = mt.id_entidadematriz
                                                    AND flpr.id_tipofundamentolegal = 4
                                                    AND flpr.dt_publicacao <= GETDATE()
                                                    AND flpr.dt_vigencia >= GETDATE()
            LEFT JOIN tb_projetopedagogicoserienivelensino AS psn ON psn.id_projetopedagogico = mt.id_projetopedagogico
            LEFT JOIN tb_turma AS tur ON tur.id_turma = mt.id_turma
            JOIN tb_nivelensino AS ne ON ne.id_nivelensino = psn.id_nivelensino
            LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS uper ON uper.id_projetopedagogico = pp.id_projetopedagogico
                                                              AND uper.bl_ativo = 1
                                                              AND uper.bl_titular = 1
            LEFT JOIN dbo.tb_usuario AS us_coordenador ON us_coordenador.id_usuario = uper.id_usuario

            LEFT JOIN tb_matricula AS mto ON mt.id_matriculaorigem = mto.id_matricula

            LEFT JOIN dbo.vw_avaliacaoaluno tcc ON ( tcc.id_matricula = mt.id_matricula
                                                    AND tcc.id_tipoavaliacao = 6
                                                    AND tcc.st_tituloavaliacao IS NOT NULL
                                                    AND tcc.id_tipodisciplina = 2
                                                    AND tcc.bl_ativo = 1
                                                  )
            LEFT JOIN tb_cancelamento AS canc ON canc.id_cancelamento = mt.id_cancelamento AND mt.id_evolucao = 27
            LEFT JOIN tb_contratomatricula AS cm ON cm.id_matricula = mt.id_matricula
			LEFT JOIN tb_matriculacertificacao AS matc ON matc.id_matricula = mt.id_matricula
			LEFT JOIN tb_areaprojetopedagogico AS app ON pp.id_projetopedagogico = app.id_projetopedagogico
			LEFT JOIN tb_areaconhecimento AS ac ON ac.id_areaconhecimento = app.id_areaconhecimento
      LEFT JOIN tb_pessoadadoscomplementares as dc on dc.id_usuario = ps.id_usuario
      LEFT JOIN tb_matriculacertificacao as mc on mt.id_matricula = mc.id_matricula
			LEFT JOIN tb_municipio as dcm on dc.id_municipioinstituicao = dcm.id_municipio
			OUTER APPLY (SELECT TOP 1 v.id_venda , v.dt_limiterenovacao ,
							format((vp.nu_valorbruto - ((isnull(cm.nu_valordesconto,0) / 100 ) * vp.nu_valorbruto))/6, '#.00') AS nu_mensalidade,
							format((((vp.nu_valorbruto - ((isnull(cm.nu_valordesconto,0) / 100 ) * vp.nu_valorbruto))/6) - (((isnull(pon.nu_valordesconto,0) / 100 ) * ((vp.nu_valorbruto - ((isnull(cm.nu_valordesconto,0) / 100 ) * vp.nu_valorbruto))/6)))), '#.00') AS nu_mensalidadepon

							FROM tb_vendaproduto AS vp
							JOIN tb_venda AS v ON v.id_venda = vp.id_venda AND v.dt_limiterenovacao IS NOT NULL
							AND vp.id_matricula = mt.id_matricula AND v.id_evolucao in (7, 9, 10) AND v.bl_ativo = 1
							LEFT JOIN tb_campanhacomercial AS cm ON cm.id_campanhacomercial = vp.id_campanhacomercial
							LEFT JOIN tb_campanhacomercial as pon ON pon.id_campanhacomercial = v.id_campanhapontualidade

							ORDER BY v.id_venda DESC) AS venda



GO

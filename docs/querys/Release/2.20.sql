
--AC-27068
/**
	INSERINDO MENSAGEM PADRAO
**/

BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_mensagempadrao on
INSERT INTO tb_mensagempadrao
(id_mensagempadrao, id_tipoenvio, st_mensagempadrao , st_default)
VALUES (37, 3, 'Envio código de rastreamento - Certificado' , 'ALTERAR - Cadastrar texto para envio de código de rastreamento do certificado')
SET IDENTITY_INSERT dbo.tb_mensagempadrao off
COMMIT


SELECT * FROM tb_textocategoria
SELECT * FROM tb_textovariaveis where id_textocategoria = 4

--APESAR DE ESTAR NO ARQUIVO DE SQL ESTA INFORMAÇÃO JA ESTA INSERIDA ATUALMENTE NA PRODUÇÃO E NO RELEASE.

BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_textovariaveis on
INSERT INTO tb_textovariaveis
(id_textovariaveis, id_textocategoria, st_camposubstituir , st_textovariaveis , st_mascara , id_tipotextovariavel)
VALUES (405, 4, 'st_codigorasteamento' , '#codigo_rasteamento_certificado' , null, 1)
SET IDENTITY_INSERT dbo.tb_mensagempadrao off
COMMIT


--COM-11633
alter table tb_turma ALTER COLUMN st_turma varchar(255);
alter table tb_turma ALTER COLUMN st_tituloexibicao varchar(255);

--COM-11658

SET IDENTITY_INSERT dbo.tb_funcionalidade on
INSERT INTO dbo.tb_funcionalidade
  (
    id_funcionalidade,
    st_funcionalidade,
    id_funcionalidadepai,
    bl_ativo,
    id_situacao,
    st_classeflex,
    st_urlicone,
    id_tipofuncionalidade,
    nu_ordem,
    st_classeflexbotao,
    bl_pesquisa,
    bl_lista,
    id_sistema,
    st_ajuda,
    st_target,
    st_urlcaminho,
    bl_visivel,
    bl_relatorio,
    st_urlcaminhoedicao,
    bl_delete
  )
  VALUES (
    736,
    'Controle de Turma', -- st_funcionalidade - varchar(100)
    257, -- id_funcionalidadepai - int
    1, -- bl_ativo - bit
    123, -- id_situacao - int
    'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa', -- st_classeflex - varchar(300)
    '/img/ico/door--arrow.png', -- st_urlicone - varchar(1000)
    3, -- id_tipofuncionalidade - int
    NULL, -- nu_ordem - int
    NULL, -- st_classeflexbotao - varchar(300)
    0, -- bl_pesquisa - bit
    0, -- bl_lista - bit
    1, -- id_sistema - int
    NULL, -- st_ajuda - varchar(max)
    NULL, -- st_target - varchar(100)
    '/relatorio-controle-turma-grade-horaria', -- st_urlcaminho - varchar(50)
    1, -- bl_visivel - int
    0, -- bl_relatorio - bit
    NULL, -- st_urlcaminhoedicao - varchar(50)
    0  -- bl_delete - bit
  );
SET IDENTITY_INSERT dbo.tb_funcionalidade off

--AC-27137

SET IDENTITY_INSERT tb_textovariaveis ON;

INSERT INTO tb_textovariaveis (id_textovariaveis, id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
VALUES
(398, 3, 'dt_iniciotcc', '#data_inicio_TCC#', 'retornaDataPorExtenso', 1),
(399, 3, 'dt_terminotcc', '#data_termino_TCC#', 'retornaDataPorExtenso', 1),
(400, 3, 'nu_notatcc', '#nota_TCC#', NULL, 1),
(401, 3, 'dt_cadastrotcc', '#data_postagem_TCC#', 'retornaDataPorExtenso', 1),
(402, 3, 'dt_defesatcc', '#data_apresentacao_TCC#', 'retornaDataPorExtenso', 1);

SET IDENTITY_INSERT tb_textovariaveis OFF;

-- SQLs AC-27366

-- Verificar o último id_funcionalidade cadastrado no banco e substituir o id_funcionalidade pelo número seguinte.

SET IDENTITY_INSERT tb_funcionalidade ON

INSERT INTO tb_funcionalidade (id_funcionalidade, st_urlcaminho, id_funcionalidadepai, st_funcionalidade,
bl_ativo, id_situacao, id_tipofuncionalidade, bl_pesquisa, bl_lista, bl_visivel, bl_relatorio, bl_delete, id_sistema)
VALUES (
739,
'739',
305,
'Edição da Data Concluinte na Tela Conclusão',
1,
123,
7,
0,
0,
0,
0,
0,
1);

SET IDENTITY_INSERT tb_funcionalidade OFF


INSERT INTO tb_permissao (st_nomepermissao, st_descricao, bl_ativo) VALUES (
	'Editar Data Concluinte na tela Conclusão',
	'Permite editar a data concluinte da matrícula na tela Conclusão. Alterando também a data concluinte exibida no certificado.',
	1
);


INSERT INTO tb_permissaofuncionalidade (id_funcionalidade, id_permissao) VALUES (
    739,
    30
);

--permissao geral para funcionalidades para o perfil 1

begin transaction

insert into [tb_entidadefuncionalidade] (id_entidade, id_funcionalidade, bl_ativo,id_situacao)
(select 2,id_funcionalidade, 1,5 from tb_funcionalidade where id_funcionalidade not in (select id_funcionalidade from tb_entidadefuncionalidade where id_entidade = 2))

insert into [tb_perfilfuncionalidade] (id_perfil, id_funcionalidade, bl_ativo)
(select 3,id_funcionalidade, 3 from tb_funcionalidade where id_funcionalidade not in (select id_funcionalidade from tb_perfilfuncionalidade where id_perfil = 3)AND id_tipofuncionalidade !=9)

insert into [tb_perfilentidade] (id_perfil, id_entidade)
select 3,id_entidade from tb_entidade where id_entidade not in 
(select id_entidade from tb_perfilentidade where id_perfil = 3)

insert into [tb_perfilentidade] (id_perfil, id_entidade)
select 1,id_entidade from tb_entidade where id_entidade not in 
(select id_entidade from tb_perfilentidade where id_perfil = 1)


insert into [tb_perfilfuncionalidade] (id_perfil, id_funcionalidade, bl_ativo)
(select 1,id_funcionalidade, 3 from tb_funcionalidade where id_funcionalidade not in (select id_funcionalidade from tb_perfilfuncionalidade where id_perfil = 1))

insert into [tb_entidadefuncionalidade] (id_entidade, id_funcionalidade, bl_ativo,id_situacao)
(select 14,id_funcionalidade, 1,5 from tb_funcionalidade where id_funcionalidade not in (select id_funcionalidade from tb_entidadefuncionalidade where id_entidade = 14))

insert into [tb_perfilfuncionalidade] (id_perfil, id_funcionalidade, bl_ativo)
(select 3,id_funcionalidade, 3 from tb_funcionalidade where id_funcionalidade not in (select id_funcionalidade from tb_perfilfuncionalidade where id_perfil = 3))
insert into tb_permissaofuncionalidade (id_funcionalidade, id_permissao)
select fc.id_funcionalidade, pm.id_permissao from tb_funcionalidade fc, tb_permissao pm where fc.id_funcionalidade not in (select id_funcionalidade from tb_permissaofuncionalidade)
and fc.id_tipofuncionalidade not in (1,4,5,6) and pm.id_permissao in (1,2,3) AND fc.id_sistema = 1

insert into tb_perfilpermissaofuncionalidade (id_funcionalidade, id_perfil,id_permissao)
select pf.id_funcionalidade, 1 , pf.id_permissao from tb_permissaofuncionalidade pf where pf.id_funcionalidade not in (select id_funcionalidade from tb_perfilpermissaofuncionalidade)

commit


--TEC-464

-- alteração na tb_ocorrencia
ALTER TABLE [dbo].[tb_ocorrencia]
ADD [st_codissue] varchar(50) NULL
GO

--criando o sistema no banco
SET IDENTITY_INSERT dbo.tb_sistema on

insert into tb_sistema (id_sistema, st_sistema, bl_ativo, st_conexao) values(28,'Jira',1,'http://jira.unyleya.com.br/rest/api/2/issue');

SET IDENTITY_INSERT dbo.tb_sistema off
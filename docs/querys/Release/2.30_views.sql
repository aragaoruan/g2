CREATE VIEW dbo.vw_afiliadoclasseentidade AS (

  SELECT
    a.id_afiliado,
    a.st_afiliado,
    ca.id_classeafiliacao,
    ca.st_classeafiliacao,
    ace.id_entidade
  FROM tb_afiliado AS a
  JOIN tb_afiliadoclasseentidade AS ace ON ace.id_afiliado = a.id_afiliado
  JOIN tb_classeafiliacao AS ca ON ca.id_classeafiliacao = ace.id_classeafiliacao

)

GO

ALTER VIEW [dbo].[vw_avaliacaoagendamento] AS
  SELECT DISTINCT aa.id_avaliacaoagendamento,
                  aa.id_matricula,
                  us.id_usuario,
                  us.st_nomecompleto,
                  us.st_cpf,
                  av.id_avaliacao,
                  av.st_avaliacao,
                  ac.id_tipoprova,
                  tp.st_tipoprova,
                  aa.dt_agendamento,
                  aa.bl_automatico,
                  aa.id_situacao,
                  st.st_situacao,
                  aa.id_avaliacaoaplicacao,
                  aa.id_entidade,
                  et.st_nomeentidade,
                  ap.id_horarioaula,
                  ha.st_horarioaula,
                  mt.id_projetopedagogico,
                  pp.st_projetopedagogico,
                  pp.bl_possuiprova,
                  (CASE
                    WHEN pp.bl_possuiprova = 1
                      THEN 'SIM'
                      ELSE 'NÃO'
                  END) AS st_possuiprova,
                  aai.st_codsistema,
                  aa.bl_ativo,
                  ap.st_endereco,
                  ap.dt_aplicacao,
                  ap.id_aplicadorprova,
                  ap.st_aplicadorprova,
                  ap.st_cidade,
                  aa.nu_presenca,
                  aa.id_usuariocadastro,
                  aa.id_usuariolancamento,
                  usu_lancamento.st_nomecompleto AS st_usuariolancamento,
                  ( CASE
                      WHEN aa.id_tipodeavaliacao IS NULL THEN 0
                      ELSE aa.id_tipodeavaliacao
                    END ) AS id_tipodeavaliacao,
                  ( CASE
                      WHEN ( ap.dt_aplicacao IS NOT NULL
                             AND ap.dt_aplicacao > Getdate()
                             AND aa.bl_ativo = 1
                             AND aa.nu_presenca IS NULL ) THEN 0
                      WHEN ( ap.dt_aplicacao IS NOT NULL
                             AND ap.dt_aplicacao > Getdate()
                             AND aa.bl_ativo = 0
                             AND aa.id_situacao = 68
                             AND aa.nu_presenca IS NOT NULL ) THEN 1
                      WHEN ( ap.dt_aplicacao IS NOT NULL
                             AND ap.dt_aplicacao < Getdate()
                             AND aa.bl_ativo = 1
                             AND aa.id_situacao = 68
                             AND aa.nu_presenca IS NULL ) THEN 2
                      ELSE 3
                    END ) AS id_situacaopresenca
                  ,
                  ( CASE
                      WHEN ( ap.dt_aplicacao IS NOT NULL
                             AND ap.dt_aplicacao > Getdate()
                             AND aa.bl_ativo = 1
                             AND aa.nu_presenca IS NULL ) THEN 'Aguardando aplicação de prova'
                      WHEN ( ap.dt_aplicacao IS NOT NULL
                             AND ap.dt_aplicacao > Getdate()
                             AND aa.bl_ativo = 0
                             AND aa.id_situacao = 68
                             AND aa.nu_presenca IS NOT NULL ) THEN 'Presença lançada'
                      WHEN ( ap.dt_aplicacao IS NOT NULL
                             AND ap.dt_aplicacao < Getdate()
                             AND aa.bl_ativo = 1
                             AND aa.id_situacao = 68
                             AND aa.nu_presenca IS NULL ) THEN 'Aplicação realizada e presença não lançada'
                      ELSE 'Situação Indefinida'
                    END ) AS st_situacaopresenca
                  ,
                  tb_chamada.id_aval AS id_chamada,
                  primeira.id_primeirachamada,
                  primeirapresenca.nu_primeirapresenca,
                  primeira.id_segundachamada AS id_ultimachamada,
                  ultima.nu_ultimapresenca,
                  mt.id_evolucao,
                  aa.bl_provaglobal,
                  ap.dt_antecedenciaminima,
                  ap.dt_alteracaolimite,
                  ( CASE
                      WHEN aa.id_avaliacaoaplicacao IS NULL
                            OR ( ( Cast(Getdate() AS DATE) ) BETWEEN
                                 ap.dt_antecedenciaminima AND
                                       ap.dt_alteracaolimite
                                 AND aa.id_avaliacaoaplicacao =
                                     ap.id_avaliacaoaplicacao
                                 AND aa.bl_ativo = 1 ) THEN 0
                      ELSE 1
                    END ) AS id_mensagens,
                  ( CASE
                      WHEN ( Cast(Getdate() AS DATE) ) < ap.dt_antecedenciaminima THEN 'A data para o agendamento ainda não chegou'
                      WHEN ( Cast(Getdate() AS DATE) ) > ap.dt_alteracaolimite THEN 'Agendamento passou do prazo limite para alteração'
                      WHEN aa.id_avaliacaoaplicacao IS NULL THEN ''
                      ELSE 'Agendamento dentro do prazo limite para alteração'
                    END ) AS st_mensagens,
                  provasrealizadas.id_provasrealizadas,
                  Isnull(temnotafinal.bl_temnotafinal, 0) AS bl_temnotafinal,
                  pe.st_email,
                  pe.nu_ddi,
                  pe.nu_ddd,
                  pe.nu_telefone,
                  pe.nu_ddialternativo,
                  pe.nu_dddalternativo,
                  pe.nu_telefonealternativo,
                  ha.hr_inicio,
                  ha.hr_fim,
                  ap.sg_uf,
                  ap.st_complemento,
                  ap.nu_numero,
                  ap.st_bairro,
                  ap.st_telefone AS 'st_telefoneaplicador',
                  NULL AS id_disciplina,
                  NULL AS st_disciplina
     FROM tb_avaliacaoagendamento aa
         INNER JOIN tb_matricula AS mt ON mt.id_matricula = aa.id_matricula
         INNER JOIN tb_entidade AS ett ON ett.id_entidade = mt.id_entidadeatendimento
         INNER JOIN tb_usuario AS us ON us.id_usuario = mt.id_usuario
         INNER JOIN tb_avaliacao AS av ON av.id_avaliacao = aa.id_avaliacao
         INNER JOIN tb_avaliacaoconjuntorelacao AS acr ON acr.id_avaliacao = av.id_avaliacao
         INNER JOIN tb_avaliacaoconjunto AS ac ON ac.id_avaliacaoconjunto = acr.id_avaliacaoconjunto
         INNER JOIN tb_tipoprova AS tp ON tp.id_tipoprova = ac.id_tipoprova
         INNER JOIN tb_situacao AS st ON st.id_situacao = aa.id_situacao
         INNER JOIN tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
         INNER JOIN vw_pessoa AS pe ON pe.id_usuario = us.id_usuario AND pe.id_entidade = mt.id_entidadematricula
         INNER JOIN tb_esquemaconfiguracao AS ec ON ett.id_esquemaconfiguracao = ec.id_esquemaconfiguracao
         INNER JOIN tb_esquemaconfiguracaoitem AS eci ON eci.id_esquemaconfiguracao = ec.id_esquemaconfiguracao AND eci.id_itemconfiguracao = 24 AND eci.st_valor = 0
         LEFT JOIN vw_avaliacaoaplicacao AS ap ON ap.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao AND ap.bl_ativo = 1 AND ap.id_entidade = aa.id_entidade
         LEFT JOIN tb_entidade AS et ON et.id_entidade = ap.id_entidade
         LEFT JOIN tb_horarioaula AS ha ON ha.id_horarioaula = ap.id_horarioaula
         LEFT JOIN tb_avalagendaintegracao AS aai ON aai.id_avaliacaoagendamento = aa.id_avaliacaoagendamento
         OUTER apply (SELECT st_nomecompleto
                      FROM   tb_usuario
                      WHERE  id_usuario = aa.id_usuariolancamento) AS
                     usu_lancamento
         OUTER apply (SELECT Count(cha.id_avaliacaoagendamento) AS id_aval
                      FROM   tb_avaliacaoagendamento AS cha
                             LEFT JOIN tb_avaliacaoaplicacao AS ch
                                    ON ch.id_avaliacaoaplicacao =
                                       cha.id_avaliacaoaplicacao
                      WHERE  cha.id_matricula = mt.id_matricula
                             AND cha.id_tipodeavaliacao = aa.id_tipodeavaliacao
                             AND cha.id_situacao = 68) AS tb_chamada
         OUTER apply (SELECT Min(id_avaliacaoagendamento) AS id_primeirachamada,
                      Max(id_avaliacaoagendamento) AS id_segundachamada
                      FROM   tb_avaliacaoagendamento
                      WHERE  id_matricula = aa.id_matricula
                             AND id_situacao = 68
                             AND id_tipodeavaliacao = aa.id_tipodeavaliacao) AS
                     primeira
         OUTER apply (SELECT nu_presenca AS nu_primeirapresenca
                      FROM   tb_avaliacaoagendamento
                      WHERE  id_avaliacaoagendamento =
                             (SELECT Min(id_avaliacaoagendamento)
                              FROM   tb_avaliacaoagendamento
                              WHERE  id_matricula = aa.id_matricula
                                     AND id_situacao = 68
                                     AND id_tipodeavaliacao =
                                         aa.id_tipodeavaliacao
                                     )) AS primeirapresenca
         OUTER apply (SELECT nu_presenca AS nu_ultimapresenca
                      FROM   tb_avaliacaoagendamento
                      WHERE  id_avaliacaoagendamento =
                             (SELECT Max(id_avaliacaoagendamento)
                              FROM   tb_avaliacaoagendamento
                              WHERE  id_matricula = aa.id_matricula
                                     AND id_situacao = 68
                                     AND id_tipodeavaliacao =
                                         aa.id_tipodeavaliacao
                                     )) AS ultima
         OUTER apply (SELECT Count(id_avaliacaoagendamento) AS
                             id_provasrealizadas
                      FROM   tb_avaliacaoagendamento
                      WHERE  id_matricula = aa.id_matricula
                             AND nu_presenca = 1
                             AND bl_ativo = 0
                             AND id_situacao = 68) AS provasrealizadas
         OUTER apply (SELECT ( CASE
                                 WHEN st_nota IS NULL THEN 0
                                 ELSE 1
                               END ) AS bl_temnotafinal
                      FROM   tb_avaliacaoaluno
                      WHERE  id_matricula = mt.id_matricula
                             AND id_avaliacao = aa.id_avaliacao
                             AND id_situacao = 86) AS temnotafinal
    WHERE aa.id_situacao <> 70
      AND ac.id_tipoprova = 2

  UNION

  SELECT DISTINCT aa.id_avaliacaoagendamento,
                  aa.id_matricula,
                  us.id_usuario,
                  us.st_nomecompleto,
                  us.st_cpf,
                  av.id_avaliacao,
                  av.st_avaliacao,
                  ac.id_tipoprova,
                  tp.st_tipoprova,
                  aa.dt_agendamento,
                  aa.bl_automatico,
                  aa.id_situacao,
                  st.st_situacao,
                  aa.id_avaliacaoaplicacao,
                  aa.id_entidade,
                  et.st_nomeentidade,
                  ap.id_horarioaula,
                  ha.st_horarioaula,
                  mt.id_projetopedagogico,
                  pp.st_projetopedagogico,
                  pp.bl_possuiprova,
                  (CASE
                    WHEN pp.bl_possuiprova = 1
                      THEN 'SIM'
                      ELSE 'NÃO'
                  END) AS st_possuiprova,
                  aai.st_codsistema,
                  aa.bl_ativo,
                  ap.st_endereco,
                  ap.dt_aplicacao,
                  ap.id_aplicadorprova,
                  ap.st_aplicadorprova,
                  ap.st_cidade,
                  aa.nu_presenca,
                  aa.id_usuariocadastro,
                  aa.id_usuariolancamento,
                  usu_lancamento.st_nomecompleto AS st_usuariolancamento,
                  ( CASE
                      WHEN aa.id_tipodeavaliacao IS NULL THEN 0
                      ELSE aa.id_tipodeavaliacao
                    END ) AS id_tipodeavaliacao,
                  ( CASE
                      WHEN ( ap.dt_aplicacao IS NOT NULL
                             AND ap.dt_aplicacao > Getdate()
                     AND aa.bl_ativo = 1
                             AND aa.nu_presenca IS NULL ) THEN 0
                      WHEN ( ap.dt_aplicacao IS NOT NULL
                             AND ap.dt_aplicacao > Getdate()
                             AND aa.bl_ativo = 0
                             AND aa.id_situacao = 68
                             AND aa.nu_presenca IS NOT NULL ) THEN 1
                      WHEN ( ap.dt_aplicacao IS NOT NULL
                             AND ap.dt_aplicacao < Getdate()
                             AND aa.bl_ativo = 1
                             AND aa.id_situacao = 68
                             AND aa.nu_presenca IS NULL ) THEN 2
                      ELSE 3
                    END ) AS id_situacaopresenca
                  ,
                  ( CASE
                      WHEN ( ap.dt_aplicacao IS NOT NULL
                             AND ap.dt_aplicacao > Getdate()
                             AND aa.bl_ativo = 1
                             AND aa.nu_presenca IS NULL ) THEN 'Aguardando aplicação de prova'
                      WHEN ( ap.dt_aplicacao IS NOT NULL
                             AND ap.dt_aplicacao > Getdate()
                             AND aa.bl_ativo = 0
                             AND aa.id_situacao = 68
                             AND aa.nu_presenca IS NOT NULL ) THEN 'Presença lançada'
                      WHEN ( ap.dt_aplicacao IS NOT NULL
                             AND ap.dt_aplicacao < Getdate()
                             AND aa.bl_ativo = 1
                             AND aa.id_situacao = 68
                             AND aa.nu_presenca IS NULL ) THEN 'Aplicação realizada e presença não lançada'
                      ELSE 'Situação Indefinida'
                    END ) AS st_situacaopresenca
                  ,
                  tb_chamada.id_aval AS id_chamada,
                  primeira.id_primeirachamada,
                  primeirapresenca.nu_primeirapresenca,
                  primeira.id_segundachamada AS id_ultimachamada,
                  ultima.nu_ultimapresenca,
                  mt.id_evolucao,
                  aa.bl_provaglobal,
                  ap.dt_antecedenciaminima,
                  ap.dt_alteracaolimite,
                  ( CASE
                      WHEN aa.id_avaliacaoaplicacao IS NULL
                            OR ( ( Cast(Getdate() AS DATE) ) BETWEEN
                                       ap.dt_antecedenciaminima AND
                                       ap.dt_alteracaolimite
                                 AND aa.id_avaliacaoaplicacao =
                                     ap.id_avaliacaoaplicacao
                                 AND aa.bl_ativo = 1 ) THEN 0
                      ELSE 1
                    END ) AS id_mensagens,
                  ( CASE
                      WHEN ( Cast(Getdate() AS DATE) ) < ap.dt_antecedenciaminima THEN 'A data para o agendamento ainda não chegou'
                      WHEN ( Cast(Getdate() AS DATE) ) > ap.dt_alteracaolimite THEN 'Agendamento passou do prazo limite para alteração'
                      WHEN aa.id_avaliacaoaplicacao IS NULL THEN ''
                      ELSE 'Agendamento dentro do prazo limite para alteração'
                    END ) AS st_mensagens,
                  provasrealizadas.id_provasrealizadas,
                  Isnull(temnotafinal.bl_temnotafinal, 0) AS bl_temnotafinal,
                  pe.st_email,
                  pe.nu_ddi,
                  pe.nu_ddd,
                  pe.nu_telefone,
                  pe.nu_ddialternativo,
                  pe.nu_dddalternativo,
                  pe.nu_telefonealternativo,
                  ha.hr_inicio,
                  ha.hr_fim,
                  ap.sg_uf,
                  ap.st_complemento,
                  ap.nu_numero,
                  ap.st_bairro,
                  ap.st_telefone AS 'st_telefoneaplicador',
                  md.id_disciplina,
                  dis.st_disciplina
    FROM tb_avaliacaoagendamento aa
         INNER JOIN tb_avalagendamentoref AS agr ON aa.id_avaliacaoagendamento = agr.id_avaliacaoagendamento
         INNER JOIN tb_avaliacaoconjuntoreferencia AS acrf ON acrf.id_avaliacaoconjuntoreferencia = agr.id_avaliacaoconjuntoreferencia
         INNER JOIN tb_matricula AS mt ON mt.id_matricula = aa.id_matricula
         INNER JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
         INNER JOIN tb_disciplina AS dis ON md.id_disciplina = dis.id_disciplina
         INNER JOIN tb_alocacao AS al ON al.id_matriculadisciplina = md.id_matriculadisciplina AND al.bl_ativo = 1 AND al.id_saladeaula = acrf.id_saladeaula
         INNER JOIN tb_entidade AS ett ON ett.id_entidade = mt.id_entidadeatendimento
         INNER JOIN tb_usuario AS us ON us.id_usuario = mt.id_usuario
         INNER JOIN tb_avaliacao AS av ON av.id_avaliacao = aa.id_avaliacao
         INNER JOIN tb_avaliacaoconjuntorelacao AS acr ON acr.id_avaliacao = av.id_avaliacao
         INNER JOIN tb_avaliacaoconjunto AS ac ON ac.id_avaliacaoconjunto = acr.id_avaliacaoconjunto
         INNER JOIN tb_tipoprova AS tp ON tp.id_tipoprova = ac.id_tipoprova
         INNER JOIN tb_situacao AS st ON st.id_situacao = aa.id_situacao
         INNER JOIN tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
         INNER JOIN vw_pessoa AS pe ON pe.id_usuario = us.id_usuario AND pe.id_entidade = mt.id_entidadematricula
         INNER JOIN tb_esquemaconfiguracao AS ec ON ett.id_esquemaconfiguracao = ec.id_esquemaconfiguracao
         INNER JOIN tb_esquemaconfiguracaoitem AS eci ON eci.id_esquemaconfiguracao = ec.id_esquemaconfiguracao AND eci.id_itemconfiguracao = 24 AND eci.st_valor = 1
         LEFT JOIN vw_avaliacaoaplicacao AS ap ON ap.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao AND ap.bl_ativo = 1 AND ap.id_entidade = aa.id_entidade
         LEFT JOIN tb_entidade AS et ON et.id_entidade = ap.id_entidade
         LEFT JOIN tb_horarioaula AS ha ON ha.id_horarioaula = ap.id_horarioaula
         LEFT JOIN tb_avalagendaintegracao AS aai ON aai.id_avaliacaoagendamento = aa.id_avaliacaoagendamento
         OUTER apply (SELECT st_nomecompleto
                      FROM   tb_usuario
                      WHERE  id_usuario = aa.id_usuariolancamento) AS
                     usu_lancamento
         OUTER apply (SELECT Count(cha.id_avaliacaoagendamento) AS id_aval
                      FROM   tb_avaliacaoagendamento AS cha
                             INNER JOIN tb_avalagendamentoref AS cagr ON cagr.id_avaliacaoagendamento = cha.id_avaliacaoagendamento AND cagr.id_avaliacaoconjuntoreferencia = acrf.id_avaliacaoconjuntoreferencia
                             LEFT JOIN tb_avaliacaoaplicacao AS ch ON ch.id_avaliacaoaplicacao = cha.id_avaliacaoaplicacao
                      WHERE  cha.id_matricula = mt.id_matricula
                             AND cha.id_tipodeavaliacao = aa.id_tipodeavaliacao
                             AND cha.id_situacao = 68) AS tb_chamada
         OUTER apply (SELECT Min(id_avaliacaoagendamento) AS id_primeirachamada,
                             Max(id_avaliacaoagendamento) AS id_segundachamada
                      FROM   tb_avaliacaoagendamento
                      WHERE  id_matricula = aa.id_matricula
                             AND id_situacao = 68
                             AND id_tipodeavaliacao = aa.id_tipodeavaliacao) AS
                     primeira
         OUTER apply (SELECT nu_presenca AS nu_primeirapresenca
                      FROM   tb_avaliacaoagendamento
                      WHERE  id_avaliacaoagendamento =
                             (SELECT Min(id_avaliacaoagendamento)
                              FROM   tb_avaliacaoagendamento
                              WHERE  id_matricula = aa.id_matricula
                                     AND id_situacao = 68
                                     AND id_tipodeavaliacao = aa.id_tipodeavaliacao
                                     )) AS primeirapresenca
         OUTER apply (SELECT nu_presenca AS nu_ultimapresenca
               FROM   tb_avaliacaoagendamento
                      WHERE  id_avaliacaoagendamento =
                             (SELECT Max(id_avaliacaoagendamento)
                              FROM   tb_avaliacaoagendamento
                              WHERE  id_matricula = aa.id_matricula
                                     AND id_situacao = 68
                                     AND id_tipodeavaliacao =
                                         aa.id_tipodeavaliacao
                                     )) AS ultima
         OUTER apply (SELECT Count(id_avaliacaoagendamento) AS
                             id_provasrealizadas
                      FROM   tb_avaliacaoagendamento
                      WHERE  id_matricula = aa.id_matricula
                             AND nu_presenca = 1
                             AND bl_ativo = 0
                             AND id_situacao = 68) AS provasrealizadas
         OUTER apply (SELECT ( CASE
                                 WHEN st_nota IS NULL THEN 0
                                 ELSE 1
                               END ) AS bl_temnotafinal
                      FROM   tb_avaliacaoaluno
                      WHERE  id_matricula = mt.id_matricula
                             AND id_avaliacao = aa.id_avaliacao
                             AND id_situacao = 86) AS temnotafinal
    WHERE aa.id_situacao <> 70
      AND ac.id_tipoprova = 2
GO
ALTER VIEW [dbo].[vw_modulodisciplina]
AS
  SELECT p.id_projetopedagogico ,
    p.st_projetopedagogico ,
    md.id_modulodisciplina ,
    md.id_modulo ,
    m.st_modulo ,
    md.id_disciplina ,
    d.st_disciplina ,
    ar.id_areaconhecimento ,
    CASE WHEN ar.st_areaconhecimento IS NULL THEN 'N�o informado'
    ELSE ar.st_areaconhecimento
    END AS st_areaconhecimento ,
    md.id_serie ,
    s.st_serie ,
    md.id_nivelensino ,
    ns.st_nivelensino ,
    md.bl_obrigatoria ,
    md.nu_ordem ,
    md.bl_ativo ,
    md.nu_ponderacaocalculada ,
    md.nu_ponderacaoaplicada ,
    md.id_usuarioponderacao ,
    md.dt_cadastroponderacao ,
    md.nu_importancia ,
    d.nu_cargahoraria AS nu_cargahorariad ,
    d.nu_repeticao ,
    d.sg_uf AS st_estudio ,
    CASE WHEN md.nu_cargahoraria IS NULL
			THEN d.nu_cargahoraria
			ELSE md.nu_cargahoraria
			END AS nu_cargahoraria
	, md.nu_obrigatorioalocacao
	, md.nu_disponivelapartirdo
  FROM    tb_modulodisciplina AS md
    INNER JOIN tb_modulo AS m ON m.id_modulo = md.id_modulo
                                 AND m.bl_ativo = 1
    INNER JOIN tb_projetopedagogico AS p ON m.id_projetopedagogico = p.id_projetopedagogico
    INNER JOIN tb_disciplina AS d ON md.id_disciplina = d.id_disciplina
    LEFT JOIN tb_serie AS s ON s.id_serie = md.id_serie
    LEFT JOIN tb_nivelensino AS ns ON ns.id_nivelensino = md.id_nivelensino
    LEFT JOIN tb_areaconhecimento AS ar ON ar.id_areaconhecimento = md.id_areaconhecimento
  WHERE   md.bl_ativo = 1
GO

CREATE VIEW dbo.vw_nucleotelemarketingentidade AS (
  SELECT DISTINCT
    nt.id_nucleotelemarketing,
    nt.st_nucleotelemarketing,
    nt.bl_ativo,
    er.id_entidade,
    nt.id_entidade AS id_entidadenucleo
  FROM tb_nucleotelemarketing AS nt
    -- Os núcleos devem ser buscados tanto da entidade em que foram cadastrados, quanto da entidade PAI (apenas o primeiro pai).
    JOIN vw_entidaderecursiva AS er ON nt.id_entidade IN(er.id_entidade, er.id_entidadepai)
)

GO

alter VIEW [dbo].[vw_periodoletivoentidade] AS (
	SELECT DISTINCT
		pl.id_periodoletivo,
		pl.st_periodoletivo,
		pl.dt_inicioinscricao,
		pl.dt_fiminscricao,
		pl.dt_abertura,
		pl.dt_encerramento,
		pl.id_tipooferta,
		er.id_entidade,
		pl.id_entidade AS id_entidadeperiodo
	FROM tb_periodoletivo AS pl
	-- Relacionamento adicionado por solicitacao do Analista de Requisitos, por que os periodos letivos devem ser buscados tanto da entidade da sessao, quanto da entidade PAI da sessao (apenas o primeiro pai).
	JOIN vw_entidaderecursiva AS er ON pl.id_entidade IN (er.id_entidade, er.id_entidadepai)
)

GO
alter  VIEW [dbo].[vw_salaoferta] AS
SELECT DISTINCT
	se.id_entidade,
	pp.id_projetopedagogico,
	mo.id_modulo,
	mo.st_modulo,
	ISNULL(md.nu_obrigatorioalocacao, 0) AS nu_obrigatorioalocacao,
	ISNULL(md.nu_disponivelapartirdo, 0) AS nu_disponivelapartirdo,
	md.nu_percentualsemestre,
	aps.id_saladeaula,
	sa.st_saladeaula,
	sa.nu_maxalunos,
	sa.bl_ofertaexcepcional,
	pp.id_trilha,
	ds.id_disciplina,
	dc.id_tipodisciplina,
  dc.nu_creditos,
	pl.id_periodoletivo,
	pl.st_periodoletivo,
	pl.dt_inicioinscricao,
	pl.dt_fiminscricao,
	pl.dt_abertura,
	pl.dt_encerramento,
	pl.id_tipooferta,
	md.nu_ordem
FROM tb_projetopedagogico AS pp
JOIN tb_projetoentidade as pe on pe.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_areaprojetosala AS aps
	ON aps.id_projetopedagogico = pp.id_projetopedagogico
JOIN vw_periodoletivoentidade AS pl ON pe.id_entidade = pl.id_entidade
JOIN tb_saladeaula AS sa
	ON sa.id_saladeaula = aps.id_saladeaula
	AND sa.id_periodoletivo = pl.id_periodoletivo
	AND sa.bl_ativa = 1
	AND sa.id_situacao = 8
	AND sa.id_categoriasala = 1
	--AND se.id_saladeaula = sa.id_saladeaula
JOIN tb_saladeaulaentidade AS se
	ON (aps.id_saladeaula = se.id_saladeaula OR sa.bl_todasentidades = 1) and pe.id_entidade = se.id_entidade
JOIN tb_disciplinasaladeaula AS ds
	ON ds.id_saladeaula = sa.id_saladeaula
JOIN tb_modulo AS mo
	ON mo.id_projetopedagogico = pp.id_projetopedagogico and mo.bl_ativo = 1
JOIN tb_modulodisciplina AS md
	ON ds.id_disciplina = md.id_disciplina and md.bl_ativo = 1 and md.id_modulo = mo.id_modulo
JOIN tb_disciplina AS dc
	ON dc.id_disciplina = ds.id_disciplina
GO
ALTER VIEW [dbo].[vw_salaofertaturma] AS
SELECT DISTINCT
	tm.id_turma,
	te.id_entidade,
	tp.id_projetopedagogico,
	mo.id_modulo,
	mo.st_modulo,
	aps.id_saladeaula,
	sa.st_saladeaula,
	sa.nu_maxalunos,
	sa.bl_ofertaexcepcional,
	pp.id_trilha,
	ds.id_disciplina,
	dc.id_tipodisciplina,
	dc.nu_creditos,
  dc.st_disciplina,
	pl.id_periodoletivo,
	pl.st_periodoletivo,
	pl.dt_inicioinscricao,
	pl.dt_fiminscricao,
	pl.dt_abertura,
	pl.dt_encerramento
FROM dbo.tb_turma AS tm
JOIN tb_turmaprojeto AS tp
	ON tp.id_turma = tm.id_turma
JOIN tb_turmaentidade AS te
	ON te.id_turma = tm.id_turma
JOIN tb_projetopedagogico AS pp
	ON tp.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_areaprojetosala AS aps
	ON aps.id_projetopedagogico = tp.id_projetopedagogico
JOIN vw_periodoletivoentidade AS pl ON pl.id_periodoletivo IN (
		SELECT id_periodoletivo FROM vw_periodoletivoentidade AS plf WHERE plf.dt_abertura >= tm.dt_inicio AND plf.id_entidade = te.id_entidade
	) AND pl.id_entidade = te.id_entidade
JOIN tb_saladeaula AS sa
	ON sa.id_saladeaula = aps.id_saladeaula
	AND sa.id_periodoletivo = pl.id_periodoletivo
	AND sa.bl_ativa = 1
	AND sa.id_situacao = 8
	AND sa.id_categoriasala = 1
JOIN tb_saladeaulaentidade AS se
	ON (se.id_entidade = te.id_entidade AND aps.id_saladeaula = se.id_saladeaula) OR sa.bl_todasentidades = 1
JOIN tb_disciplinasaladeaula AS ds
	ON ds.id_saladeaula = sa.id_saladeaula
JOIN tb_modulo AS mo
	ON mo.id_projetopedagogico = pp.id_projetopedagogico AND mo.bl_ativo = 1
JOIN tb_modulodisciplina AS md
	ON ds.id_disciplina = md.id_disciplina and md.bl_ativo = 1 and md.id_modulo = mo.id_modulo
JOIN tb_disciplina AS dc
	ON dc.id_disciplina = ds.id_disciplina
GO

ALTER VIEW [dbo].[vw_vendalancamento] AS
    SELECT DISTINCT vd.id_venda, vd.id_usuario AS id_usuariovenda
            -- Este campo na tela de venda exibe o Responsável Financeiro, mas antes não mostrava do campo certo
           , CASE
                WHEN usf.st_nomecompleto IS NULL THEN
                    us.st_nomecompleto
                ELSE
                    usf.st_nomecompleto
                END AS st_nomecompleto
           , us.st_nomecompleto AS st_nomeusuario
           , vd.id_evolucao
           , vd.id_formapagamento
           , vd.nu_parcelas
           , vd.nu_valorliquido
           , lv.bl_entrada
           , lv.nu_ordem
           , lc.id_lancamento
           , lc.nu_valor
           , CAST('0.00' AS NUMERIC(7,2)) AS nu_valoratualizado
           , lc.nu_vencimento
           , lc.id_tipolancamento
           , lc.dt_cadastro
           , lc.id_meiopagamento
           , lc.id_usuariolancamento
           , lc.id_entidadelancamento
           , vd.id_entidade
           , lc.id_usuariocadastro
           , lc.st_banco
           , lc.id_cartaoconfig
           , lc.id_boletoconfig
           , lc.bl_quitado
           , lc.dt_vencimento
           , lc.dt_quitado
           , lc.dt_emissao
           , lc.dt_prevquitado
           , lc.st_emissor
           , lc.st_coddocumento
           , lc.nu_juros
           , lc.nu_desconto
           , lc.nu_quitado
           , lc.nu_multa
           , lc.nu_verificacao
           , mp.st_meiopagamento
           , fp.nu_juros AS nu_jurosatraso
           , fp.nu_multa AS nu_multaatraso
           , vmv.nu_diavencimento
           , tl.st_tipolancamento
           , lc.bl_ativo
           , li.st_codlancamento
           , lc.bl_original
           , vd.recorrente_orderid
           , lc.st_retornoverificacao
           , tc.id_campanhacomercial
           , lc.nu_cartao
           , NULL AS st_urlpagamento -- campo fake, gerado no php
		   , lc.id_sistemacobranca
      FROM tb_venda AS vd
INNER JOIN tb_usuario AS us ON us.id_usuario = vd.id_usuario
INNER JOIN tb_lancamentovenda AS lv ON lv.id_venda = vd.id_venda
INNER JOIN tb_lancamento AS lc ON lc.id_lancamento = lv.id_lancamento AND lc.bl_ativo = 1
INNER JOIN tb_formapagamento AS fp ON fp.id_formapagamento = vd.id_formapagamento
 LEFT JOIN tb_usuario AS usf ON usf.id_usuario = lc.id_usuariolancamento
 LEFT JOIN tb_meiopagamento AS mp ON lc.id_meiopagamento = mp.id_meiopagamento
 LEFT JOIN tb_vendameiovencimento AS vmv ON vd.id_venda = vmv.id_venda AND vmv.id_meiopagamento = mp.id_meiopagamento
INNER JOIN tb_tipolancamento AS tl ON tl.id_tipolancamento = lc.id_tipolancamento
 LEFT JOIN tb_lancamentointegracao AS li ON li.id_lancamento = lc.id_lancamento
 LEFT JOIN tb_campanhacomercial AS tc  ON vd.id_campanhacomercial = tc.id_campanhacomercial AND tc.id_categoriacampanha = 3


GO
CREATE VIEW dbo.vw_vendedorentidade AS (
  SELECT DISTINCT
    v.id_vendedor,
    v.st_vendedor,
    v.bl_ativo,
    er.id_entidade,
    v.id_entidade AS id_entidadevendedor
  FROM tb_vendedor AS v
    -- Os vendedores devem ser buscados tanto da entidade em que foram cadastrados, quanto da entidade PAI (apenas o primeiro pai).
    JOIN vw_entidaderecursiva AS er ON v.id_entidade IN(er.id_entidade, er.id_entidadepai)
	)

GO

ALTER VIEW [rel].[vw_comercialgeral] as
SELECT DISTINCT
  CAST (mt.dt_cadastro AS DATE) AS dt_matricula,
  --app.id_areaconhecimento ,
  ps.sg_uf,
  ve.id_entidade,
  ve.st_nomeentidade,
  ps.st_nomecompleto,
  ps.st_cpf,
  st_cidade,
  st_projetopedagogico,
  mt.id_projetopedagogico,
  (
    SELECT
      COALESCE (
          ac2.st_areaconhecimento + ', ',
          ''
      )
    FROM
      dbo.tb_areaconhecimento ac2
      JOIN dbo.tb_areaprojetopedagogico app2 ON ac2.id_areaconhecimento = app2.id_areaconhecimento
    WHERE
      app2.id_projetopedagogico = mt.id_projetopedagogico FOR XML PATH ('')
  ) AS st_areaconhecimento,
  (
    SELECT
      COALESCE (
          tc.st_categoria + ', ',
          ''
      )
    from tb_categoriaproduto tbcp
      LEFT JOIN tb_categoria tc ON tc.id_categoria = tbcp.id_categoria
    WHERE
      tbcp.id_produto = pdt.id_produto FOR XML PATH ('')
  ) AS st_categoria,

  --st_areaconhecimento,
  tm.st_turma,
  tm.id_turma,
  tm.dt_inicio,
  tm.dt_fim,
  usr.st_nomecompleto AS st_representante,
  usa.st_nomecompleto AS st_atendente,
  mt.id_matricula,
  ev.id_evolucao,
  ev.st_evolucao,
  st.st_situacao,
  vdl.nu_parcelas,
  vdlb.nu_valorboleto,
  vdlcc.nu_valorcartaoc,
  vdlcd.nu_valorcartaod,
  vdlcr.nu_valorcartaor,
  vdlc.nu_valorcheque,
  vdlo.nu_valoroutros,
  pd.nu_valorliquido,
  vdlct.nu_valorcarta,
  vdlbl.nu_valorbolsa,
  vd.id_venda,
  vdlv.dt_vencimento AS dt_vencimentoultima,
  pdc.st_produto AS st_combo,
  pdc.id_produto AS id_produtocombo,
  ve.st_nomeentidade AS st_nomeentidaderec,
  et.id_entidade AS id_entidadepai,
  pdt.id_produto,
  '1' as id_formapagamentoaplicacao,
  'todas' as st_formapagamentoaplicacao,
  st_afiliado = (CASE WHEN afi.st_afiliado IS NOT NULL THEN afi.st_afiliado ELSE '-' END),
  st_nucleotelemarketing = (CASE WHEN nt.st_nucleotelemarketing IS NOT NULL THEN nt.st_nucleotelemarketing ELSE '-' END),
  st_vendedor = (CASE WHEN vdr.st_vendedor IS NOT NULL THEN vdr.st_vendedor ELSE '-' END),
  st_siteorigem = (CASE WHEN vd.st_siteorigem IS NOT NULL THEN vd.st_siteorigem ELSE '-' END)
FROM
  tb_entidade AS et
  JOIN dbo.vw_entidaderecursivaid AS ve ON ve.nu_entidadepai = et.id_entidade
                                           OR ve.id_entidade = et.id_entidade
  JOIN dbo.tb_matricula AS mt ON ve.id_entidade = mt.id_entidadematricula and mt.id_evolucao not in (41)
                                 AND mt.bl_ativo = 1
  JOIN dbo.tb_evolucao AS ev ON ev.id_evolucao = mt.id_evolucao
  JOIN dbo.tb_situacao AS st ON st.id_situacao = mt.id_situacao
  JOIN dbo.tb_areaprojetopedagogico AS app ON app.id_projetopedagogico = mt.id_projetopedagogico
  JOIN dbo.tb_areaconhecimento AS aa ON aa.id_areaconhecimento = app.id_areaconhecimento -- AND aa.id_tipoareaconhecimento = 1
  JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
  LEFT JOIN tb_turma AS tm ON tm.id_turma = mt.id_turma
  JOIN dbo.vw_pessoa AS ps ON ps.id_usuario = mt.id_usuario
                              AND ps.id_entidade = ve.id_entidade
  JOIN dbo.tb_contratomatricula AS cm ON cm.id_matricula = mt.id_matricula --AND cm.bl_ativo = 1
  JOIN dbo.tb_contrato AS ct ON ct.id_contrato = cm.id_contrato
  JOIN dbo.tb_venda AS vd ON vd.id_venda = ct.id_venda and vd.bl_transferidoentidade = 0
  LEFT JOIN dbo.tb_afiliado AS afi ON afi.id_afiliado = vd.id_afiliado
  LEFT JOIN dbo.tb_nucleotelemarketing AS nt ON nt.id_nucleotelemarketing = vd.id_nucleotelemarketing
  LEFT JOIN dbo.tb_vendedor AS vdr ON vdr.id_vendedor = vd.id_vendedor
  LEFT JOIN dbo.tb_usuario AS usa ON usa.id_usuario = vd.id_atendente
  LEFT JOIN dbo.tb_vendaenvolvido AS ver ON ver.id_venda = vd.id_venda
                                            AND ver.id_funcao = 5
  LEFT JOIN dbo.tb_usuario AS usr ON usr.id_usuario = ver.id_usuario CROSS APPLY (
    SELECT
                                                                                     COUNT (id_lancamento) AS nu_parcelas
                                                                                   FROM
                                                                                     dbo.vw_vendalancamento
                                                                                   WHERE
                                                                                     id_venda = vd.id_venda
                                                                                 ) AS vdl OUTER APPLY (
   SELECT
                                                                                   SUM (nu_valor) AS nu_valorboleto
                                                                                                        FROM
                                                                                                          dbo.vw_vendalancamento
                                                                                                        WHERE
                                                                                                          id_venda = vd.id_venda
                                                                                                          AND id_meiopagamento = 2
                                                                                                      ) AS vdlb OUTER APPLY (
                                                                                                                              SELECT
                                                                                                                                SUM (nu_valor) AS nu_valorcartaoc
                                                                                                                              FROM
                                                                                                                                dbo.vw_vendalancamento
                                                                                                                              WHERE
                                                                                                                                id_venda = vd.id_venda
                                                                                                                                AND id_meiopagamento = 1
                                                                                                                            ) AS vdlcc OUTER APPLY (
                                                                                                                                                     SELECT
                                                                                                                                                       SUM (nu_valor) AS nu_valorcartaod
                                                                                                                                                     FROM
                                                                                                                                                       dbo.vw_vendalancamento
                                                                                                                                                     WHERE
                                                                                                                                                       id_venda = vd.id_venda
                                                                                                                                                       AND id_meiopagamento = 7
                                                                                                                                                   ) AS vdlcd OUTER APPLY (
                                                                                                                                                                          SELECT
                                                                                                                                                                              SUM (nu_valor) AS nu_valorcartaor
                                                                                                                                                                            FROM
                            dbo.vw_vendalancamento
                                                                                                                                                                            WHERE
                                                                                                                                                                              id_venda = vd.id_venda
                                                                                                                                                                              AND id_meiopagamento = 11
                                                                                                                                                                          ) AS vdlcr OUTER APPLY (
                                                                                                                                                                                                   SELECT
                                                                                                                                                                                                     SUM (nu_valor) AS nu_valorcheque
                                                                                                                                                                                                   FROM
                                                                                                                                                                                                     dbo.vw_vendalancamento
                                                                                                                                                                                                   WHERE
                                                                                                                                                                                                     id_venda = vd.id_venda
                                                                                                                                                                                                     AND id_meiopagamento = 4
                                                                                                                                                                                                 ) AS vdlc OUTER APPLY (
                                                                                                                                                                                                                         SELECT
                                                                                                                                                                                                                           SUM (nu_valor) AS nu_valoroutros
                                                                                                                                                                                                                         FROM
            dbo.vw_vendalancamento
                                                                                                                                                                                                                         WHERE
                                                                                                                                                                                                                           id_venda = vd.id_venda
                                                                                                                                                             AND id_meiopagamento NOT IN (2, 1, 7, 11, 4, 13, 10)
                                                                                                                                                                                                                       ) AS vdlo OUTER APPLY (
                                                                                                                                                                                                                                               SELECT
                                                                                                                                                                                                                                                 SUM (nu_valor) AS nu_valorcarta
                                                                                                                                                                                                                                               FROM
                                                                                                                                                                                                                                                 dbo.vw_vendalancamento
                                                                                                                                                                                                                                               WHERE
                                                                                                                                                                                                                                                 id_venda = vd.id_venda
                                                                                                                                                                                                                                                 AND id_meiopagamento = 13
                                                                                                                                                                                                                                             ) AS vdlct OUTER APPLY (
                                                                                                                                                                                                                                                                      SELECT
                                                                                                                                                                                                                                                                        SUM (nu_valor) AS nu_valorbolsa
                                                                                                                                                                                                                                                                      FROM
                                                                                                              dbo.vw_vendalancamento
                                                                                                                                                                                                                                                                      WHERE
                                                                                                                                      id_venda = vd.id_venda
                                                                                                                                                                                                                                                                        AND id_meiopagamento = 10
                                                                                                                                                                                                                                                                    ) AS vdlbl
  JOIN dbo.tb_vendaproduto AS pd ON pd.id_venda = vd.id_venda
                                    AND pd.id_vendaproduto = mt.id_vendaproduto
  JOIN dbo.tb_produto AS pdt ON pdt.id_produto = pd.id_produto
                                AND pdt.id_modelovenda in (1,3)
  LEFT JOIN tb_produto AS pdc ON pdc.id_produto = pd.id_produtocombo
  CROSS APPLY (
                SELECT
                  TOP 1 dt_vencimento
                FROM
                  dbo.vw_vendalancamento
                WHERE
                  id_venda = vd.id_venda
                ORDER BY
                  dt_vencimento DESC
              ) AS vdlv
GO
ALTER VIEW [rel].[vw_comercialgeralgraduacao] AS

SELECT DISTINCT
  vwcg.id_entidade,
  vwcg.id_venda,
  vwcg.id_matricula,
  vwcg.st_nomecompleto,
  vwcg.st_cpf,
  vwcg.st_cidade,
  vwcg.sg_uf,
  vwcg.st_areaconhecimento,
  vwcg.st_projetopedagogico,
  vwcg.dt_matricula,
  vwcg.st_evolucao,
  vwcg.st_atendente,
  vwcg.dt_inicio,
  vwcg.st_turma,
  vwcg.id_produto,
  vwcg.st_nomeentidade,
  vwcg.id_entidadepai,
  vwcg.nu_valorliquido,

  vdlb.nu_valorboleto,
  vdlo.nu_valoroutros,
  vdl.nu_parcelas as st_nuparcelas,

  lce.nu_valorentrada,
  lsp.nu_valorsegundaparcela,
  dpcp.nu_porcentagemdescontocomercial,
  dpcp.nu_descontocampanha,

  CASE WHEN dpcp.id_tipodesconto = 1
    THEN 0
  ELSE vdlv.nu_valorvenda
  END AS nu_valordevenda,
  st_afiliado = (CASE WHEN afi.st_afiliado IS NOT NULL THEN afi.st_afiliado ELSE '-' END),
  st_nucleotelemarketing = (CASE WHEN nt.st_nucleotelemarketing IS NOT NULL THEN nt.st_nucleotelemarketing ELSE '-' END),
  st_vendedor = (CASE WHEN vdr.st_vendedor IS NOT NULL THEN vdr.st_vendedor ELSE '-' END),
  st_siteorigem = (CASE WHEN vd.st_siteorigem IS NOT NULL THEN vd.st_siteorigem ELSE '-' END)

FROM rel.vw_comercialgeral as vwcg
  JOIN dbo.tb_venda AS vd ON vd.id_venda = vwcg.id_venda
  LEFT JOIN dbo.tb_afiliado AS afi ON afi.id_afiliado = vd.id_afiliado
  LEFT JOIN dbo.tb_nucleotelemarketing AS nt ON nt.id_nucleotelemarketing = vd.id_nucleotelemarketing
  LEFT JOIN dbo.tb_vendedor AS vdr ON vdr.id_vendedor = vd.id_vendedor
  JOIN tb_projetopedagogico pp ON vwcg.id_projetopedagogico = pp.id_projetopedagogico AND pp.bl_renovar = 1
  OUTER APPLY (
                SELECT
                  SUM (nu_valor) AS nu_valorvenda
                FROM
                  dbo.tb_lancamentovenda lv
                  JOIN dbo.tb_lancamento l ON lv.id_lancamento = l.id_lancamento
                WHERE
                  lv.id_venda = vwcg.id_venda
                  AND l.bl_original = 1
              ) AS vdlv

  OUTER APPLY (
                SELECT
                  SUM (nu_valor) AS nu_valorboleto
                FROM
                  dbo.tb_lancamentovenda lv
                  JOIN dbo.tb_lancamento l ON lv.id_lancamento = l.id_lancamento
                WHERE
                  lv.id_venda = vwcg.id_venda
                  AND l.bl_original = 1
                  AND id_meiopagamento = 2

              ) AS vdlb

  OUTER APPLY (
                SELECT
                  SUM (nu_valor) AS nu_valoroutros
                FROM
                  dbo.tb_lancamentovenda lv
                  JOIN dbo.tb_lancamento l ON lv.id_lancamento = l.id_lancamento
                WHERE
                  id_venda = vwcg.id_venda
                  AND id_meiopagamento NOT IN (2)
                  AND bl_original = 1
              ) AS vdlo

  OUTER APPLY (
                SELECT
                  COUNT(l.id_lancamento) AS nu_parcelas
                FROM
                  dbo.tb_lancamentovenda lv
                  JOIN dbo.tb_lancamento l ON lv.id_lancamento = l.id_lancamento
                WHERE
                  lv.id_venda = vwcg.id_venda
                  AND l.bl_original = 1
              ) AS vdl

  OUTER APPLY (
                SELECT
                  SUM(nu_valor) AS nu_valorentrada
                FROM
                  dbo.tb_lancamentovenda lv
                  JOIN dbo.tb_lancamento l ON lv.id_lancamento = l.id_lancamento
                WHERE
                  id_venda = vwcg.id_venda
                  AND bl_entrada = 1
                  AND bl_original = 1
              ) as lce

  OUTER APPLY (
                SELECT
                  MAX(nu_valor) AS nu_valorsegundaparcela
                FROM
                  dbo.tb_lancamentovenda lv
                  JOIN dbo.tb_lancamento l ON lv.id_lancamento = l.id_lancamento
                WHERE
                  id_venda = vwcg.id_venda
                  AND bl_original = 1
                  AND bl_entrada = 0
                --ORDER BY
                -- nu_ordem ASC
                --OFFSET  1 ROWS
                --FETCH NEXT 1 ROWS ONLY
              ) AS lsp

  OUTER APPLY (
                SELECT
                  ISNULL( cp.nu_valordesconto, 0 ) AS nu_descontocampanha,
                  vd.nu_descontovalor AS nu_porcentagemdescontocomercial,
                  cp.id_tipodesconto,
                  cp.nu_valordesconto
                FROM
                  tb_venda vd
                  LEFT JOIN tb_campanhacomercial cp ON
                                                      cp.id_campanhacomercial = vd.id_campanhapontualidade
                                                      AND cp.bl_ativo = 1
   AND cp.id_situacao = 41
                WHERE vd.id_venda = vwcg.id_venda
              ) AS dpcp
GO

GO

ALTER VIEW [rel].[vw_recebimento]
AS
SELECT  DISTINCT
    et.st_razaosocial ,
    et.st_nomeentidade ,
    mt.id_matricula ,
    CONVERT(VARCHAR(MAX), mt.dt_cadastro, 103) AS st_datamatricula ,
    mt.dt_cadastro ,
    vd.id_venda ,
    pp.st_projetopedagogico ,
    pp.id_projetopedagogico ,
    tm.st_turma ,
    tm.id_turma ,
    ps.st_nomecompleto ,
    ps.id_usuario ,
    ps.nu_ddd + ps.nu_telefone AS st_telefone ,
    ps.st_cpf ,
    ps.st_email ,
    ps.st_endereco ,
    ps.st_cep ,
    ps.st_bairro ,
    ps.st_complemento ,
    ps.nu_numero ,
    ps.sg_uf ,
    ps.st_cidade ,
    CASE WHEN lc.nu_quitado IS NULL THEN '0'
    ELSE LEFT(lc.nu_quitado, ( LEN(lc.nu_quitado) - 3 ))
    END AS 'nu_valorbaixadog2' ,
    CASE WHEN vd.nu_valorliquido = 0 THEN CONVERT(MONEY, 0)
    ELSE CONVERT(MONEY, CAST(( lc.nu_quitado * vp.nu_valorliquido
                            / vd.nu_valorliquido ) AS NUMERIC(9,
                                              2)))
    END AS nu_proporcionalbaixado ,
    CASE WHEN lv.nu_ordem IS NULL
      AND lv.bl_entrada = 1 THEN 0
    ELSE lv.nu_ordem
    END AS 'nu_ordem' ,
    CONVERT(VARCHAR(12), lc.dt_quitado, 103) AS st_databaixa ,
    CONVERT(VARCHAR(12), lc.dt_vencimento, 103) AS st_datavencimento ,
    --st.st_situacao ,
    vd.id_entidade ,
    lc.id_lancamento ,
    CAST(lc.dt_quitado AS DATE) AS dt_pagamento ,
    tb_situacao.st_situacao,
    st_afiliado = (CASE WHEN afi.st_afiliado IS NOT NULL THEN afi.st_afiliado ELSE '-' END),
    st_nucleotelemarketing = (CASE WHEN nt.st_nucleotelemarketing IS NOT NULL THEN nt.st_nucleotelemarketing ELSE '-' END),
    st_vendedor = (CASE WHEN vdr.st_vendedor IS NOT NULL THEN vdr.st_vendedor ELSE '-' END),
    st_siteorigem = (CASE WHEN vd.st_siteorigem IS NOT NULL THEN vd.st_siteorigem ELSE '-' END)

    --te.st_campo AS evolucao_original_st_campo,--'EVOLU��O ORIGINAL',
    -- te1.st_campo AS evolucao_final_st_campo,--'EVOLU��O FINAL',
    --ts.st_campo AS situacao_original_st_campo,--'SITUA��O ORIGINAL',
    --ts1.st_campo AS situacao_final_st_campo--'SITUA��O FINAL'
FROM    tb_venda AS vd
    LEFT JOIN dbo.tb_afiliado AS afi ON afi.id_afiliado = vd.id_afiliado
    LEFT JOIN dbo.tb_nucleotelemarketing AS nt ON nt.id_nucleotelemarketing = vd.id_nucleotelemarketing
    LEFT JOIN dbo.tb_vendedor AS vdr ON vdr.id_vendedor = vd.id_vendedor
    JOIN dbo.tb_entidade AS et ON et.id_entidade = vd.id_entidade
    JOIN dbo.tb_entidadeintegracao AS ei ON ei.id_entidade = vd.id_entidade
                                        AND ei.id_sistema = 3
    JOIN dbo.tb_contrato AS ct ON ct.id_venda = vd.id_venda
    JOIN dbo.tb_contratomatricula AS cm ON cm.id_contrato = ct.id_contrato AND cm.bl_ativo = 1
    JOIN dbo.tb_matricula AS mt ON mt.id_matricula = cm.id_matricula
                              -- AND mt.bl_ativo = 1
    JOIN dbo.tb_situacao AS st ON st.id_situacao = mt.id_situacao
    JOIN dbo.vw_produtoprojetotipovalor AS pptv ON pptv.id_projetopedagogico = mt.id_projetopedagogico
                                               AND pptv.id_entidade = mt.id_entidadematricula
    JOIN dbo.tb_vendaproduto AS vp ON vp.id_venda = vd.id_venda
                                  AND vp.id_produto = pptv.id_produto
    JOIN dbo.vw_pessoa AS ps ON ps.id_usuario = mt.id_usuario
                            AND ps.id_entidade = mt.id_entidadeatendimento
    LEFT JOIN dbo.tb_turma AS tm ON tm.id_turma = mt.id_turma
    JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
    JOIN dbo.tb_lancamentovenda AS lv ON lv.id_venda = vd.id_venda
    JOIN dbo.tb_lancamento AS lc ON lc.id_lancamento = lv.id_lancamento
    LEFT JOIN dbo.tb_lancamentointegracao AS li ON lv.id_lancamento = li.id_lancamento
                                               AND li.id_lancamento = lc.id_lancamento
    JOIN dbo.tb_situacao ON dbo.tb_situacao.id_situacao = vd.id_situacao
                        AND dbo.tb_situacao.st_tabela = 'tb_venda'
--left JOIN vw_matricularecursiva vm ON vm.id_matriculaorigem = mt.id_matricula
--left JOIN tb_matricula tm1 ON tm1.id_matricula = vm.id_matricula
--left JOIN tb_evolucao te ON te.id_evolucao = tm1.id_evolucao
--LEFT JOIN tb_situacao ts ON ts.id_situacao = tm1.id_situacao

--LEFT JOIN vw_matricularecursiva vm1ON vm1.nu_matriculaorigem = mt.id_matricula
--left JOIN tb_matricula tm2 ON tm2.id_matricula = vm1.id_matricula
--left JOIN tb_evolucao te1 ON te1.id_evolucao = tm2.id_evolucao
--LEFT JOIN tb_situacao ts1 ON ts1.id_situacao = tm2.id_situacao
WHERE   mt.id_evolucao <> 20
        AND lc.bl_quitado = 1
        AND lc.bl_ativo = 1
GO






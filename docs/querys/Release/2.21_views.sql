ALTER VIEW [dbo].[vw_pesquisasaladeaula] AS
select sa.id_saladeaula ,
        sa.dt_cadastro ,
        st_saladeaula ,
        sa.bl_ativa ,
        sa.id_usuariocadastro ,
        sa.id_modalidadesaladeaula ,
        sa.dt_inicioinscricao ,
        sa.dt_fiminscricao ,
        sa.dt_abertura ,
        st_localizacao ,
        sa.id_tiposaladeaula ,
        sa.dt_encerramento ,
        sa.id_entidade ,
        sa.id_periodoletivo ,
        bl_usardoperiodoletivo ,
        sa.id_situacao ,
        nu_maxalunos ,
        nu_diasencerramento ,
        bl_semencerramento ,
        nu_diasaluno ,
        bl_semdiasaluno ,
        nu_diasextensao ,
        st_pontosnegativos ,
        st_pontospositivos ,
        st_pontosmelhorar ,
        st_pontosresgate ,
        st_conclusoesfinais ,
        st_justificativaacima ,
        st_justificativaabaixo, msa.st_modalidadesaladeaula, uper.id_perfilreferencia, uper.id_usuario, sap.id_saladeaulaprofessor,sap.bl_titular, sap.nu_maximoalunos, tsa.st_descricao, tsa.st_tiposaladeaula, e.st_nomeentidade,

pl.st_periodoletivo, pl.dt_inicioinscricao as dt_inicioinscricaoletivo, pl.dt_fiminscricao as dt_fiminscricaoletivo,
pl.dt_abertura as dt_aberturaletivo, pl.dt_encerramento as dt_encerramentoletivo, s.st_situacao, u.st_nomecompleto as st_nomeprofessor,
d.id_disciplina, d.st_disciplina
,sa.id_categoriasala
,catsala.st_categoriasala
,CASE WHEN ((sa.dt_encerramento IS NOT NULL) )
	THEN DATEADD(day, ISNULL(nu_diasextensao, 0), sa.dt_encerramento)
  END as dt_comextensao
  , sis.st_sistema
from tb_saladeaula as sa
		INNER JOIN tb_modalidadesaladeaula as msa ON msa.id_modalidadesaladeaula = sa.id_modalidadesaladeaula
		LEFT JOIN tb_usuarioperfilentidadereferencia as uper ON uper.id_saladeaula = sa.id_saladeaula AND UPER.bl_ativo = 1 AND UPER.bl_titular = 1
		LEFT JOIN tb_usuario as u ON u.id_usuario = uper.id_usuario
		LEFT JOIN tb_saladeaulaprofessor as sap ON sap.id_perfilreferencia = uper.id_perfilreferencia
		JOIN tb_tiposaladeaula as tsa ON tsa.id_tiposaladeaula = sa.id_tiposaladeaula
		JOIN tb_entidade as e ON e.id_entidade = sa.id_entidade
		--LEFT JOIN tb_entidaderelacao as er ON er.id_entidade = e.id_entidade AND er.id_entidadepai is null -- Ainda não sei se vai funcionar
		--LEFT JOIN tb_entidaderelacao as er2 ON er.id_entidadepai = e.id_entidade -- Ainda não sei se vai funcionar
		LEFT JOIN tb_disciplinasaladeaula as dsa ON dsa.id_saladeaula = sa.id_saladeaula
		LEFT JOIN tb_disciplina as d ON d.id_disciplina = dsa.id_disciplina
		LEFT JOIN tb_periodoletivo as pl ON pl.id_periodoletivo = sa.id_periodoletivo
		JOIN tb_situacao as s ON s.id_situacao = sa.id_situacao
		JOIN tb_categoriasala as catsala ON catsala.id_categoriasala = sa.id_categoriasala
		LEFT JOIN tb_saladeaulaintegracao as sai on (sai.id_saladeaula = sa.id_saladeaula)
		LEFT JOIN tb_sistema as sis on (sis.id_sistema = sai.id_sistema)
where sa.bl_ativa=1

go

ALTER VIEW [dbo].[vw_gerardeclaracao]
AS
  SELECT
    mt.id_matricula,
    ps.id_usuario                                            AS id_usuarioaluno,
    ps.st_nomecompleto                                       AS st_nomecompletoaluno,
    ps.st_login                                              AS st_loginaluno,
    ps.st_senhaentidade                                      AS st_senhaaluno,
    ps.st_cpf                                                AS st_cpfaluno,
    tur.id_turma                                             AS id_turma,
    CONVERT(CHAR, mt.dt_cadastro, 103)                       AS st_datamatricula,
    canc.st_observacao                                       AS st_observacaocancelamento,
    canc.st_observacaocalculo                                AS st_observacao_calculo,
    ps.st_rg                                                 AS st_rgaluno,
    ps.st_orgaoexpeditor,
    ps.dt_dataexpedicao                                      AS dt_dataexpedicaoaluno,
    ps.st_nomemae                                            AS st_filiacao,
    CONVERT(CHAR, canc.dt_solicitacao, 103)                  AS st_solicitacaocancelamento,
    canc.nu_valorcarta                                       AS st_valorcredito,
    CONVERT(CHAR, ct.dt_validade, 103)                       AS st_validadecredito,
    canc.id_cancelamento,
    cm.id_contrato,
    ps.dt_nascimento                                         AS dt_nascimentoaluno,
    UPPER(SUBSTRING(ps.st_nomemunicipio, 1, 1)) +
    LOWER(SUBSTRING(ps.st_nomemunicipio, 2, 499))            AS st_municipioaluno,
    ps.st_estadoprovincia                                    AS st_ufaluno,
    ps.sg_uf                                                 AS sg_ufaluno,
    ps.nu_ddd                                                AS nu_dddaluno,
    ps.nu_telefone                                           AS nu_telefonealuno,
    ps.st_email                                              AS st_emailaluno,
    pp.st_tituloexibicao                                     AS st_projeto,
    --pp.nu_cargahoraria,
    (SELECT sum(dis.nu_cargahoraria) AS nu_cargahoraria
     FROM
       tb_matriculadisciplina md
       JOIN tb_matricula m ON m.id_matricula = md.id_matricula
       JOIN tb_disciplina dis ON dis.id_disciplina = md.id_disciplina
     WHERE md.id_matricula = mt.id_matricula)                AS nu_cargahoraria,
    ne.st_nivelensino,
    mt.dt_concluinte                                         AS dt_concluintealuno,
    us_coordenador.st_nomecompleto                           AS st_coordenadorprojeto,
    fl.id_fundamentolegal                                    AS id_fundamentolei,
    fl.nu_numero                                             AS nu_lei,
    flr.id_fundamentolegal                                   AS id_fundamentoresolucao,
    tur.st_turma,
    tur.dt_fim                                               AS dt_terminoturma,
    tur.dt_inicio                                            AS dt_inicioturma,
    flr.nu_numero                                            AS nu_resolucao,
    flpr.id_fundamentolegal                                  AS id_fundamentoparecer,
    flpr.nu_numero                                           AS nu_parecer,
    flp.id_fundamentolegal                                   AS id_fundamentoportaria,
    flp.nu_numero                                            AS nu_portaria,
    et.id_entidade,
    et.st_nomeentidade,
    et.st_razaosocial,
    et.st_cnpj,
    et.st_urlimglogo,
    et.st_urlsite,
    ps.st_nomepais,
    ps.st_sexo                                               AS sexo_aluno,
    UPPER(av.st_tituloavaliacao)                             AS st_titulomonografia,
    CONVERT(VARCHAR(2), DATEPART(DAY, GETDATE()), 103) + ' de ' +
    dbo.fn_mesporextenso(GETDATE()) + ' de ' +
    CONVERT(VARCHAR(4), DATEPART(YEAR, GETDATE()), 103)      AS st_data,
    DAY(GETDATE())                                           AS st_dia,
    LOWER(dbo.fn_mesporextenso(GETDATE()))                   AS st_mes,
    YEAR(GETDATE())                                          AS st_ano,
    CONVERT(DATE, sa.dt_abertura, 103)                       AS dt_primeirasala,
    CONVERT(DATE, DATEADD(MONTH, 13, sa.dt_abertura), 103)   AS dt_previsaofim,
    mt.st_codcertificacao,
    CONVERT(CHAR, GETDATE(), 103)                            AS st_atual,

	DATENAME(day, GetDate())     + ' de ' +  DATENAME(month, GetDate())   + ' de ' +  DATENAME(year, GetDate()) as st_atualextenso,
	DATENAME(day, tur.dt_fim)     + ' de ' +  DATENAME(month, tur.dt_fim)   + ' de ' +  DATENAME(year, tur.dt_fim) as dt_terminoturmaextenso,
	DATENAME(day, tur.dt_inicio)     + ' de ' +  DATENAME(month, tur.dt_inicio)   + ' de ' +  DATENAME(year, tur.dt_inicio) as dt_inicioturmaextenso


  FROM tb_matricula mt
    JOIN tb_projetopedagogico AS pp
      ON pp.id_projetopedagogico = mt.id_projetopedagogico
    JOIN tb_entidade AS et
      ON et.id_entidade = mt.id_entidadeatendimento
    LEFT JOIN tb_entidadeendereco AS ete
      ON ete.id_entidade = et.id_entidade
         AND ete.bl_padrao = 1
    LEFT JOIN tb_endereco AS etee
      ON etee.id_endereco = ete.id_endereco
    JOIN vw_pessoa AS ps ON ps.id_usuario = mt.id_usuario AND ps.id_entidade = mt.id_entidadeatendimento
    LEFT JOIN tb_fundamentolegal AS fl
      ON fl.id_entidade = mt.id_entidadematriz
         AND fl.id_tipofundamentolegal = 1
         AND fl.dt_publicacao <= GETDATE()
         AND fl.dt_vigencia >= GETDATE()
    LEFT JOIN tb_fundamentolegal AS flp
      ON flp.id_entidade = mt.id_entidadematriz
         AND flp.id_tipofundamentolegal = 2
         AND flp.dt_publicacao <= GETDATE()
         AND flp.dt_vigencia >= GETDATE()
    LEFT JOIN tb_fundamentolegal AS flr
      ON flr.id_entidade = mt.id_entidadematriz
         AND flr.id_tipofundamentolegal = 3
         AND flr.dt_publicacao <= GETDATE()
         AND flr.dt_vigencia >= GETDATE()
    LEFT JOIN tb_fundamentolegal AS flpr
      ON flpr.id_entidade = mt.id_entidadematriz
         AND flpr.id_tipofundamentolegal = 4
         AND flpr.dt_publicacao <= GETDATE()
         AND flpr.dt_vigencia >= GETDATE()
    LEFT JOIN tb_projetopedagogicoserienivelensino AS psn
      ON psn.id_projetopedagogico = mt.id_projetopedagogico
    LEFT JOIN tb_turma AS tur
      ON tur.id_turma = mt.id_turma
    JOIN tb_nivelensino AS ne
      ON ne.id_nivelensino = psn.id_nivelensino
    LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS uper
      ON uper.id_projetopedagogico = pp.id_projetopedagogico
         AND uper.bl_ativo = 1
         AND uper.bl_titular = 1
    LEFT JOIN dbo.tb_usuario AS us_coordenador
      ON us_coordenador.id_usuario = uper.id_usuario
    OUTER APPLY (SELECT TOP 1 ts.dt_abertura
                 FROM tb_matriculadisciplina tm
                   JOIN tb_alocacao ta
                     ON tm.id_matriculadisciplina = ta.id_matriculadisciplina
                   JOIN tb_saladeaula ts
                     ON ta.id_saladeaula = ts.id_saladeaula
                 WHERE tm.id_matricula = mt.id_matricula
                 ORDER BY ts.dt_abertura DESC) AS sa
    LEFT JOIN dbo.vw_avaliacaoaluno av
      ON (av.id_matricula = mt.id_matricula
          AND av.id_tipoavaliacao = 6
          AND av.st_tituloavaliacao IS NOT NULL
          AND id_tipodisciplina = 2
          AND av.bl_ativo = 1)
    LEFT JOIN tb_cancelamento AS canc
      ON canc.id_cancelamento = mt.id_cancelamento
    LEFT JOIN tb_contratomatricula AS cm
      ON cm.id_matricula = mt.id_matricula
    LEFT JOIN dbo.tb_cartacredito AS ct ON ct.id_cancelamento = canc.id_cancelamento

  go

  --atualização do arquivo


  CREATE view [dbo].[vw_pesquisardisciplinanivelArea] AS
  SELECT      d.id_disciplina, d.st_disciplina, d.st_descricao, d.id_tipodisciplina, td.st_tipodisciplina, d.nu_identificador, d.nu_cargahoraria, d.nu_creditos, d.nu_codigoparceiro,
              d.bl_ativa, d.bl_compartilhargrupo, d.id_situacao, s.st_situacao, d.id_entidade, d.id_grupodisciplina, e.st_nomeentidade, ne.id_nivelensino, ne.st_nivelensino, ad.id_areaconhecimento, a.st_areaconhecimento
      FROM        tb_disciplina AS d
          INNER JOIN                  tb_entidade AS e ON e.id_entidade = d.id_entidade
          INNER JOIN                  tb_tipodisciplina AS td ON td.id_tipodisciplina = d.id_tipodisciplina
          LEFT JOIN                  tb_disciplinaserienivelensino AS dsne ON dsne.id_disciplina = d.id_disciplina
          LEFT JOIN                  tb_nivelensino AS ne ON ne.id_nivelensino = dsne.id_nivelensino
          INNER JOIN                  tb_situacao AS s ON s.id_situacao = d.id_situacao
          LEFT JOIN                  tb_areadisciplina AS ad ON ad.id_disciplina = d.id_disciplina
          LEFT JOIN                  tb_areaconhecimento AS a ON a.id_areaconhecimento = ad.id_areaconhecimento
          where d.bl_ativa = 1

  GO
  CREATE VIEW vw_alunosrenovacao
  AS

  SELECT DISTINCT
  mat.id_matricula,
  mat.id_projetopedagogico,
  mat.id_usuario
  , mat.id_periodoletivo
  , mat.id_evolucao
  , mat.id_vendaproduto
  , mat.id_entidadematricula
  , mat.id_turma
  , v.id_venda
  ,vp.id_produto
  , vp.id_tiposelecao
  , c.id_contrato
   FROM tb_matricula AS mat
  JOIN tb_entidade AS e ON e.id_entidade = mat.id_entidadematricula AND mat.bl_ativo = 1 AND mat.id_evolucao = 6 AND mat.bl_institucional = 0
  JOIN tb_esquemaconfiguracaoitem AS eci ON eci.id_esquemaconfiguracao = e.id_esquemaconfiguracao AND eci.st_valor = 2
  JOIN tb_itemconfiguracao AS ic ON ic.id_itemconfiguracao = eci.id_itemconfiguracao AND ic.id_itemconfiguracao = 22
  JOIN tb_contratomatricula AS cm ON cm.id_matricula = mat.id_matricula
  JOIN tb_contrato AS c ON c.id_contrato = cm.id_contrato
  JOIN tb_venda    AS v ON v.id_venda = c.id_venda AND v.dt_limiterenovacao IS NULL
  JOIN tb_vendaproduto as vp ON vp.id_vendaproduto = mat.id_vendaproduto
  JOIN tb_lancamentovenda AS lv ON lv.id_venda = v.id_venda
  JOIN tb_lancamento AS lan ON lan.id_lancamento = lv.id_lancamento AND  lan.bl_ativo = 1 AND lan.bl_quitado = 1
  and not EXISTS (select * from tb_saladeaula as sa
  						JOIN tb_alocacao as al on al.id_saladeaula = sa.id_saladeaula AND al.bl_ativo = 1
  						JOIN tb_matriculadisciplina as md on md.id_matriculadisciplina = al.id_matriculadisciplina and md.id_matricula = mat.id_matricula
  						where sa.dt_abertura > getdate())

create view vw_salaoferta  as
SELECT DISTINCT
	se.id_entidade,
	pp.id_projetopedagogico,
	mo.id_modulo,
	mo.st_modulo,
	aps.id_saladeaula,
	sa.st_saladeaula,
	sa.nu_maxalunos,
	sa.bl_ofertaexcepcional,
	pp.id_trilha,
	ds.id_disciplina,
	dc.id_tipodisciplina,
  dc.nu_creditos,
	pl.id_periodoletivo,
	pl.st_periodoletivo,
	pl.dt_inicioinscricao,
	pl.dt_fiminscricao,
	pl.dt_abertura,
	pl.dt_encerramento,
	md.nu_ordem
FROM tb_projetopedagogico AS pp
JOIN tb_projetoentidade as pe on pe.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_areaprojetosala AS aps
	ON aps.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_periodoletivo as pl on pe.id_entidade = pl.id_entidade
JOIN tb_saladeaula AS sa
	ON sa.id_saladeaula = aps.id_saladeaula
	AND sa.id_periodoletivo = pl.id_periodoletivo
	AND sa.bl_ativa = 1
	AND sa.id_situacao = 8
	AND sa.id_categoriasala = 1
	--AND se.id_saladeaula = sa.id_saladeaula
JOIN tb_saladeaulaentidade AS se
	ON (aps.id_saladeaula = se.id_saladeaula OR sa.bl_todasentidades = 1) and pe.id_entidade = se.id_entidade
JOIN tb_disciplinasaladeaula AS ds
	ON ds.id_saladeaula = sa.id_saladeaula
JOIN tb_modulo AS mo
	ON mo.id_projetopedagogico = pp.id_projetopedagogico and mo.bl_ativo = 1
JOIN tb_modulodisciplina AS md
	ON ds.id_disciplina = md.id_disciplina and md.bl_ativo = 1 and md.id_modulo = mo.id_modulo
JOIN tb_disciplina AS dc
	ON dc.id_disciplina = ds.id_disciplina

  go

  CREATE VIEW [dbo].[vw_salaofertaturma] AS
  SELECT DISTINCT
  	tm.id_turma,
  	te.id_entidade,
  	tp.id_projetopedagogico,
  	mo.id_modulo,
  	mo.st_modulo,
  	aps.id_saladeaula,
  	sa.st_saladeaula,
  	sa.nu_maxalunos,
  	sa.bl_ofertaexcepcional,
  	pp.id_trilha,
  	ds.id_disciplina,
  	dc.id_tipodisciplina,
  	dc.nu_creditos,
    dc.st_disciplina,
  	pl.id_periodoletivo,
  	pl.st_periodoletivo,
  	pl.dt_inicioinscricao,
  	pl.dt_fiminscricao,
  	pl.dt_abertura,
  	pl.dt_encerramento
  FROM dbo.tb_turma AS tm
  JOIN tb_turmaprojeto AS tp
  	ON tp.id_turma = tm.id_turma
  JOIN tb_turmaentidade AS te
  	ON te.id_turma = tm.id_turma
  JOIN tb_projetopedagogico AS pp
  	ON tp.id_projetopedagogico = pp.id_projetopedagogico
  JOIN tb_areaprojetosala AS aps
  	ON aps.id_projetopedagogico = tp.id_projetopedagogico
  JOIN tb_periodoletivo AS pl ON pl.id_periodoletivo IN (
  		SELECT TOP 5 id_periodoletivo FROM tb_periodoletivo AS plf WHERE plf.dt_abertura >= tm.dt_inicio AND plf.id_entidade = te.id_entidade ORDER BY plf.dt_abertura ASC
  	) AND pl.id_entidade = te.id_entidade
  JOIN tb_saladeaula AS sa
  	ON sa.id_saladeaula = aps.id_saladeaula
  	AND sa.id_periodoletivo = pl.id_periodoletivo
  	AND sa.bl_ativa = 1
  	AND sa.id_situacao = 8
  	AND sa.id_categoriasala = 1
  JOIN tb_saladeaulaentidade AS se
  	ON (se.id_entidade = te.id_entidade AND aps.id_saladeaula = se.id_saladeaula) OR sa.bl_todasentidades = 1
  JOIN tb_disciplinasaladeaula AS ds
  	ON ds.id_saladeaula = sa.id_saladeaula
  JOIN tb_modulo AS mo
  	ON mo.id_projetopedagogico = pp.id_projetopedagogico AND mo.bl_ativo = 1
  JOIN tb_modulodisciplina AS md
  	ON ds.id_disciplina = md.id_disciplina and md.bl_ativo = 1 and md.id_modulo = mo.id_modulo
  JOIN tb_disciplina AS dc
  	ON dc.id_disciplina = ds.id_disciplina

    go

    ALTER VIEW [dbo].[vw_gradenota]
    AS
        SELECT DISTINCT
                sa.id_saladeaula ,
                sa.dt_abertura ,
                pp.id_projetopedagogico ,
                pp.st_projetopedagogico ,
                sa.st_saladeaula ,
                mn.st_notatcc ,
                mn.id_tiponotatcc ,
                mn.st_notaead ,
                mn.id_tiponotaead ,
                mn.st_notaatividade ,
                mn.id_tiponotaatividade ,
                mn.st_notafinal ,
                mn.id_tiponotafinal ,
                mn.id_avaliacaoatividade ,
                dc.id_disciplina ,
                dc.st_disciplina ,
                dc.id_tipodisciplina ,
                dc.st_tituloexibicao AS st_tituloexibicaodisciplina ,
                mt.id_matricula ,
                md.id_matriculadisciplina ,
    			md.id_aproveitamento,
                acr.id_avaliacaoconjuntoreferencia ,
                CAST (( md.nu_aprovafinal * pp.nu_notamaxima / 100 ) AS INT) AS nu_notafinal ,
                ev.id_evolucao ,
                ev.st_evolucao ,
                st.id_situacao ,
                st.st_situacao ,
                dc.nu_cargahoraria ,
                sa.dt_encerramento ,
                mn.nu_notatotal ,
                sa.id_categoriasala ,
    			ap.dt_cadastro AS dt_cadastroaproveitamento,
    			ap.id_usuario AS id_usuarioaproveitamento,
    			u.st_nomecompleto AS st_nomeusuarioaproveitamento,
                st_status = CASE WHEN ea.id_alocacao IS NOT NULL
                                      AND md.id_evolucao = 12 THEN ev.st_evolucao
                                 WHEN ea.id_alocacao IS NOT NULL
                                      AND md.id_evolucao = 19 THEN ev.st_evolucao
                                 WHEN ea.id_alocacao IS NOT NULL
                                      AND md.id_situacao = 65
                                 THEN 'Crédito Concedido'
                                 WHEN ea.id_alocacao IS NOT NULL
                                      AND ( mn.nu_notatotal + nf.nu_notafaltante ) >= ( pp.nu_notamaxima
                                                                  * pp.nu_percentualaprovacao
                                                                  / 100 )
                                 THEN 'Satisfatório'
                                 WHEN ea.id_alocacao IS NOT NULL
                                      AND ( mn.nu_notatotal + nf.nu_notafaltante ) < ( pp.nu_notamaxima
                                                                  * pp.nu_percentualaprovacao
                                                                  / 100 )
                                 THEN 'Insatisfatório'
    							 WHEN md.id_aproveitamento IS NOT NULL
                                      AND md.id_situacao = 65
                                 THEN 'Aproveitamento de Disciplina'
                                 WHEN ea.id_alocacao IS NULL
                                      AND ea.id_encerramentosala IS NULL
                                 THEN 'Não encerrado'

                                 ELSE '-'
                            END ,
                bl_status = CASE WHEN md.bl_obrigatorio = 0 THEN 1
                                 WHEN ( mn.nu_notatotal
                                        + ISNULL(nf.nu_notafaltante, 0) ) >= ( pp.nu_notamaxima
                                                                  * pp.nu_percentualaprovacao
                                                                  / 100 ) THEN 1
                                 WHEN ( mn.nu_notatotal
                                        + ISNULL(nf.nu_notafaltante, 0) ) < ( pp.nu_notamaxima
                                                                  * pp.nu_percentualaprovacao
                                                                  / 100 ) THEN 0
                                 ELSE 0
                            END ,
                bl_complementar = CASE WHEN ( md.id_matricula != md.id_matriculaoriginal )
                                            AND ppcompl.bl_disciplinacomplementar = 1
                                       THEN 1
                                       ELSE 0
                                  END ,
                DATEADD(DAY, ISNULL(sa.nu_diasextensao, 0), sa.dt_encerramento) AS dt_encerramentoextensao
        FROM    dbo.tb_matriculadisciplina AS md
                JOIN dbo.tb_matricula AS mt ON md.id_matricula = mt.id_matricula
                JOIN dbo.tb_disciplina AS dc ON dc.id_disciplina = md.id_disciplina
                                                AND dc.id_tipodisciplina <> 3
                JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
                LEFT JOIN dbo.tb_alocacao AS al ON al.id_matriculadisciplina = md.id_matriculadisciplina
                                                   AND al.bl_ativo = 1
                LEFT JOIN dbo.tb_avaliacaoconjuntoreferencia AS acr ON acr.id_saladeaula = al.id_saladeaula --AND acr.dt_fim IS NOT null
                LEFT JOIN tb_encerramentoalocacao AS ea ON ea.id_alocacao = al.id_alocacao
                LEFT JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula
                JOIN tb_evolucao AS ev ON ev.id_evolucao = md.id_evolucao
                JOIN dbo.tb_situacao AS st ON st.id_situacao = md.id_situacao
                LEFT JOIN dbo.vw_matriculanota AS mn ON md.id_matriculadisciplina = mn.id_matriculadisciplina
                                                        AND mn.id_alocacao = al.id_alocacao
                OUTER APPLY ( SELECT    id_matricula ,
                                        id_disciplina ,
                                        SUM(CAST(nu_notamax AS FLOAT)) AS nu_notafaltante
                              FROM      vw_avaliacaoaluno
                              WHERE     bl_ativo IS NULL
                                        AND id_matricula = mt.id_matricula
                                        AND id_disciplina = md.id_disciplina
                                        AND id_tipoprova = 2
                              GROUP BY  id_disciplina ,
                                        id_matricula
                            ) AS nf
                JOIN tb_matricula AS matcompl ON matcompl.id_matricula = md.id_matriculaoriginal
                JOIN tb_projetopedagogico AS ppcompl ON ppcompl.id_projetopedagogico = matcompl.id_projetopedagogico
    			LEFT JOIN dbo.tb_aproveitamento AS ap ON ap.id_aproveitamento = md.id_aproveitamento
    			LEFT JOIN tb_usuario AS u ON u.id_usuario = ap.id_usuario;

          go

          ALTER VIEW [dbo].[vw_matricula]

AS
SELECT
	mt.id_usuario,
	us.st_nomecompleto,
	us.st_cpf,
	ps.st_urlavatar,
	ps.dt_nascimento,
	ps.st_nomepai,
	ps.st_nomemae,
	ps.st_email,
	CAST(ps.nu_ddd AS VARCHAR) + '-' + CAST(ps.nu_telefone AS VARCHAR) AS st_telefone,
	CAST(ps.nu_dddalternativo AS VARCHAR) + '-'
	+ CAST(ps.nu_telefonealternativo AS VARCHAR) AS st_telefonealternativo,
	mt.id_matricula,
	pp.st_projetopedagogico,
	mt.id_situacao,
	st.st_situacao,
	mt.id_evolucao,
	ev.st_evolucao,
	mt.dt_concluinte,
	mt.id_entidadematriz,
	etmz.st_nomeentidade AS st_entidadematriz,
	mt.id_entidadematricula,
	etm.st_nomeentidade AS st_entidadematricula,
	mt.id_entidadeatendimento,
	eta.st_nomeentidade AS st_entidadeatendimento,
	mt.bl_ativo,
	cm.id_contrato,
	pp.id_projetopedagogico,
	mt.dt_termino,
	mt.dt_cadastro,
	mt.dt_inicio,
	tm.id_turma,
	tm.st_turma,
	tm.dt_inicio AS dt_inicioturma,
	mt.bl_institucional,
	tm.dt_fim AS dt_terminoturma,
	mt.id_situacaoagendamento,
	mt.id_vendaproduto,
	mt.dt_termino AS dt_terminomatricula,
	CAST(RIGHT(master.dbo.fn_varbintohexstr(HASHBYTES('MD5',
	(CAST(us.id_usuario AS VARCHAR(10)) COLLATE Latin1_General_CI_AI
	+ CAST((CASE
		WHEN mt.id_matricula IS NULL THEN 0
		ELSE mt.id_matricula
	END) AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
	+ CAST(5 AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
	+ CAST((0) AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
	+ us.st_login
	+ us.st_senha
	+ CONVERT(VARCHAR(10), GETDATE(), 103)))),
	32) AS CHAR(32)) COLLATE Latin1_General_CI_AI AS st_urlacesso,
	mt.bl_documentacao,
	--ct.id_venda,
	vprod.id_venda,
	eta.st_urlportal,
	mt.st_codcertificacao,
	ps.st_identificacao,
	CONVERT(VARCHAR, mtc.dt_cadastro, 103) AS st_dtcadastrocertificacao,
	CONVERT(VARCHAR, mtc.dt_enviocertificado, 103) AS st_dtenviocertificado,
	CONVERT(VARCHAR, mtc.dt_retornocertificadora, 103) AS st_dtretornocertificadora,
	CONVERT(VARCHAR, mtc.dt_envioaluno, 103) AS st_dtenvioaluno,
	st_codigoacompanhamento,
	mtc.id_indicecertificado,
	mt.id_evolucaocertificacao,
	ap.id_areaconhecimento,
	pp.bl_disciplinacomplementar,
	mt.id_matriculavinculada,
	cr.id_contratoregra,
	trc.id_tiporegracontrato,
	etm.st_siglaentidade,
	mt.bl_academico,
	mtc.dt_cadastro AS dt_cadastrocertificacao,
	mtc.dt_enviocertificado,
	mtc.dt_retornocertificadora,
	mtc.dt_envioaluno,
	vup.st_upload AS st_uploadcontrato,
	canc.id_cancelamento,
	mt.id_aproveitamento,
	vprod.id_produto
FROM tb_matricula mt
JOIN tb_projetopedagogico AS pp
	ON mt.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_areaprojetopedagogico AS ap
	ON ap.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_entidade AS etmz
	ON etmz.id_entidade = mt.id_entidadematriz
JOIN tb_entidade AS etm
	ON etm.id_entidade = mt.id_entidadematricula
JOIN tb_entidade AS eta
	ON eta.id_entidade = mt.id_entidadeatendimento
JOIN tb_situacao AS st
	ON st.id_situacao = mt.id_situacao
JOIN tb_evolucao AS ev
	ON ev.id_evolucao = mt.id_evolucao
JOIN tb_usuario AS us
	ON us.id_usuario = mt.id_usuario
LEFT JOIN tb_contratomatricula AS cm
	ON cm.id_matricula = mt.id_matricula
	AND cm.bl_ativo = 1
LEFT JOIN vw_pessoa AS ps
	ON mt.id_usuario = ps.id_usuario
	AND mt.id_entidadeatendimento = ps.id_entidade
LEFT JOIN dbo.tb_contrato AS ct
	ON ct.id_contrato = cm.id_contrato
	AND ct.bl_ativo = 1
LEFT JOIN tb_turma AS tm
	ON tm.id_turma = mt.id_turma
LEFT JOIN tb_matriculacertificacao mtc
	ON mtc.id_matricula = mt.id_matricula
	AND mtc.bl_ativo = 1
LEFT JOIN dbo.tb_contratoregra AS cr
	ON cr.id_contratoregra = ct.id_contratoregra
LEFT JOIN dbo.tb_tiporegracontrato AS trc
	ON trc.id_tiporegracontrato = cr.id_tiporegracontrato
LEFT JOIN dbo.tb_venda AS v
	ON ct.id_venda = v.id_venda
LEFT JOIN dbo.tb_upload AS vup
	ON vup.id_upload = v.id_uploadcontrato
LEFT JOIN dbo.tb_cancelamento AS canc
	ON canc.id_cancelamento = mt.id_cancelamento
OUTER APPLY (SELECT TOP 1 tb_vendaproduto.* FROM dbo.tb_vendaproduto
  WHERE tb_vendaproduto.id_matricula = mt.id_matricula
  AND tb_vendaproduto.bl_ativo = 1
  ORDER BY tb_vendaproduto.id_vendaproduto DESC) AS vprod
  /*LEFT JOIN dbo.tb_vendaproduto AS vprod
  ON vprod.id_vendaproduto = mt.id_vendaproduto */
WHERE mt.bl_ativo = 1

GO

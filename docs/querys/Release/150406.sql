-- AC-26705-portal-visualisar-materiais.sql
SET IDENTITY_INSERT tb_funcionalidade ON

INSERT INTO tb_funcionalidade (
id_funcionalidade,
st_funcionalidade,
id_funcionalidadepai,
bl_ativo,
id_situacao,
st_classeflex,
st_urlicone,
id_tipofuncionalidade,
nu_ordem,
st_classeflexbotao,
bl_pesquisa,
bl_lista,
id_sistema,
st_ajuda,
st_target,
st_urlcaminho,
bl_visivel,
bl_relatorio,
st_urlcaminhoedicao,
bl_delete)
VALUES (672,
'Materiais',
313,
0,
123,
'/matricula/materiais',
'/imagens/icones/bookmark.png',
1,
null,
'ajax',
0,
0,
4,
null,
null,
'672',
1,
0,
null,
1);

SET IDENTITY_INSERT tb_funcionalidade OFF

---- AC-26753 - Consulta Pagamento.sql
/** INSERINDO MENU **/

BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_funcionalidade on
INSERT INTO [dbo].[tb_funcionalidade]
([id_funcionalidade]
,[st_funcionalidade]
,[id_funcionalidadepai]
,[bl_ativo]
,[id_situacao]
,[st_classeflex]
,[st_urlicone]
,[id_tipofuncionalidade]
,[st_classeflexbotao]
,[bl_pesquisa]
,[bl_lista]
, [id_sistema]
,[st_urlcaminho]
,[bl_visivel] 
,[bl_delete]
)
VALUES

(669,'Consulta Pagamento',449,1,123,'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa','/img/ico/books.png',3,null,0,0 , 1 , '/consulta-pagamento' , 1, 1)


GO
SET IDENTITY_INSERT dbo.tb_funcionalidade off

COMMIT

----COM-11065.sql

alter table tb_entidade add id_uploadloja int null
alter table tb_configuracaoentidade add st_corprimarialoja varchar(7) null
alter table tb_configuracaoentidade add st_corsecundarialoja varchar(7) null
alter table tb_configuracaoentidade add st_corterciarialoja varchar(7) null

--COM-11275
alter table tb_localaula add nu_maxalunos integer not null  default 1

--COM-11277
alter table tb_disciplina add st_apelido varchar (15) null

-- FIN-16670-quantidade-transacoes-recorrente-funcionalidade-674.sql

insert into tb_funcionalidade values 
('Quantidade Transações - Recorrente', 488, 1, 123, 
'', null, 3, 0, null, 0, 0,1, null, null, 
'quantidade-transacoes-recorrente', 1, 0, null, 1)

begin transaction

insert into [tb_entidadefuncionalidade] (id_entidade, id_funcionalidade, bl_ativo,id_situacao)
(select 2,id_funcionalidade, 1,5 from tb_funcionalidade where id_funcionalidade not in (select id_funcionalidade from tb_entidadefuncionalidade where id_entidade = 2))

insert into [tb_perfilfuncionalidade] (id_perfil, id_funcionalidade, bl_ativo)
(select 3,id_funcionalidade, 3 from tb_funcionalidade where id_funcionalidade not in (select id_funcionalidade from tb_perfilfuncionalidade where id_perfil = 3))


insert into [tb_perfilfuncionalidade] (id_perfil, id_funcionalidade, bl_ativo)
(select 1,id_funcionalidade, 3 from tb_funcionalidade where id_funcionalidade not in (select id_funcionalidade from tb_perfilfuncionalidade where id_perfil = 1))

insert into [tb_entidadefuncionalidade] (id_entidade, id_funcionalidade, bl_ativo,id_situacao)
(select 14,id_funcionalidade, 1,5 from tb_funcionalidade where id_funcionalidade not in (select id_funcionalidade from tb_entidadefuncionalidade where id_entidade = 14))

insert into [tb_perfilfuncionalidade] (id_perfil, id_funcionalidade, bl_ativo)
(select 3,id_funcionalidade, 3 from tb_funcionalidade where id_funcionalidade not in (select id_funcionalidade from tb_perfilfuncionalidade where id_perfil = 3))
insert into tb_permissaofuncionalidade (id_funcionalidade, id_permissao)
select fc.id_funcionalidade, pm.id_permissao from tb_funcionalidade fc, tb_permissao pm where fc.id_funcionalidade not in (select id_funcionalidade from tb_permissaofuncionalidade)
and fc.id_tipofuncionalidade not in (1,4,5,6) and pm.id_permissao in (1,2,3) AND fc.id_sistema = 1

insert into tb_perfilpermissaofuncionalidade (id_funcionalidade, id_perfil,id_permissao)
select pf.id_funcionalidade, 1 , pf.id_permissao from tb_permissaofuncionalidade pf where pf.id_funcionalidade not in (select id_funcionalidade from tb_perfilpermissaofuncionalidade)

commit

----- vw_consultapagamento.View.sql

/****** Object:  View [dbo].[vw_consultapagamento]    Script Date: 12/03/2015 10:23:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create VIEW [dbo].[vw_consultapagamento]
AS
SELECT DISTINCT
            es.id_saladeaula ,
            es.id_encerramentosala ,
            id_usuarioprofessor ,
            dt_encerramentoprofessor ,
            CONVERT(VARCHAR, dt_encerramentoprofessor, 103) AS st_encerramentoprofessor ,
            id_usuariocoordenador ,
            dt_encerramentocoordenador ,
            CONVERT(VARCHAR, dt_encerramentocoordenador, 103) AS st_encerramentocoordenador ,
            id_usuariopedagogico ,
            dt_encerramentopedagogico ,
            CONVERT(VARCHAR, dt_encerramentopedagogico, 103) AS st_encerramentopedagogico ,
            id_usuariofinanceiro ,
            dt_encerramentofinanceiro ,
            CONVERT(VARCHAR, dt_encerramentofinanceiro, 103) AS st_encerramentofinanceiro ,
            sa.id_disciplina ,
            sa.st_disciplina ,
            st_saladeaula ,
            sa.id_entidade ,
            sa.id_tipodisciplina ,
            uper.id_usuario AS id_coordenador ,
            al.nu_alunos ,
            tcc.st_nomecompleto AS st_aluno ,
            tcc.st_tituloavaliacao ,
            tcc.id_matricula ,
            tcc.st_upload ,
            tcc.st_projetopedagogico ,
            us.st_nomecompleto AS st_professor ,
            tcc.id_usuario AS id_aluno ,
            es.nu_valorpago
			,dd.nu_cargahoraria
			, sa.id_categoriasala
			
    FROM    dbo.tb_encerramentosala AS es
            JOIN dbo.tb_areaprojetosala AS aps ON aps.id_saladeaula = es.id_saladeaula
            JOIN dbo.tb_usuarioperfilentidadereferencia AS uper ON uper.id_projetopedagogico = aps.id_projetopedagogico
            JOIN vw_saladeaula AS sa ON sa.id_saladeaula = es.id_saladeaula
			JOIN dbo.tb_disciplina as dd ON dd.id_disciplina = sa.id_disciplina
            OUTER APPLY ( SELECT TOP 1
                                    aa.st_nomecompleto ,
                                    aa.st_tituloavaliacao ,
                                    md.id_matricula ,
                                    up.st_upload ,
                                    mat.id_usuario ,
                                    aa.st_projetopedagogico ,
                                    aa.st_nota
                          FROM      dbo.tb_encerramentoalocacao AS ee
                                    JOIN dbo.tb_alocacao AS ali ON ali.id_alocacao = ee.id_alocacao
                                    JOIN dbo.tb_matriculadisciplina AS md ON md.id_matriculadisciplina = ali.id_matriculadisciplina
                                    JOIN dbo.tb_matricula AS mat ON mat.id_matricula = md.id_matricula
                                    LEFT JOIN dbo.vw_avaliacaoaluno AS aa ON aa.id_saladeaula = sa.id_saladeaula
                                                              AND md.id_matricula = aa.id_matricula
                                                              AND aa.id_tipoavaliacao = 6
                                    LEFT JOIN dbo.tb_upload AS up ON aa.id_upload = up.id_upload
                          WHERE     es.id_encerramentosala = ee.id_encerramentosala
                        ) AS tcc
            LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS upr ON upr.id_saladeaula = sa.id_saladeaula
                                                              AND upr.bl_titular = 1
                                                              AND upr.bl_ativo = 1
            LEFT JOIN dbo.tb_usuario AS us ON us.id_usuario = upr.id_usuario
            OUTER APPLY ( SELECT    COUNT(DISTINCT id_alocacao) AS nu_alunos
                          FROM      dbo.tb_encerramentoalocacao
                          WHERE     id_encerramentosala = es.id_encerramentosala
                        ) AS al


GO

---- vw_encerramentosalas.View.sql

/****** Object:  View [dbo].[vw_encerramentosalas]    Script Date: 23/03/2015 09:54:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[vw_encerramentosalas]
AS
SELECT DISTINCT
  es.id_saladeaula,
  es.id_encerramentosala,
  upr.id_usuario as id_usuarioprofessor,
  dt_encerramentoprofessor,
  CONVERT(VARCHAR, dt_encerramentoprofessor, 103) AS st_encerramentoprofessor,
  id_usuariocoordenador,
  dt_encerramentocoordenador,
  CONVERT(VARCHAR, dt_encerramentocoordenador, 103) AS st_encerramentocoordenador,
  id_usuariopedagogico,
  dt_encerramentopedagogico,
  CONVERT(VARCHAR, dt_encerramentopedagogico, 103) AS st_encerramentopedagogico,
  id_usuariofinanceiro,
  dt_encerramentofinanceiro,
  CONVERT(VARCHAR, dt_encerramentofinanceiro, 103) AS st_encerramentofinanceiro,
  sa.id_entidade,
  sa.id_disciplina,
  di.st_disciplina,
  di.nu_cargahoraria,
  st_saladeaula,
  sa.id_tipodisciplina,
  td.st_tipodisciplina,
  NULL AS id_areaconhecimento,
  NULL AS st_areaconhecimento,
  al.nu_alunos,
  --tcc.st_nomecompleto AS st_aluno,
  (case WHEN tcc.st_nomecompleto IS NOT NULL
		THEN tcc.st_nomecompleto
		ELSE tcc.st_nomecompletomatricula
		END) AS st_aluno,
  tcc.st_tituloavaliacao,
  tcc.id_matricula,
  tcc.st_upload,
  --CAST(tcc.st_nota AS NUMERIC(3,0)) AS st_nota,
   (CASE WHEN (tcc.st_nota IS NULL AND tcc.st_nomecompleto IS  NULL)
		  THEN 'Desalocado'
		  ELSE CONVERT(varchar(10),(CAST(ISNULL(tcc.st_nota, 0) AS NUMERIC(3,0)))) 
		  END) AS st_nota,
  --tcc.st_projetopedagogico,
  (case WHEN tcc.st_projetopedagogico IS NOT NULL
		THEN tcc.st_projetopedagogico
		ELSE tcc.st_projetopedagogicomatricula
		END) AS st_projetopedagogico,
  us.st_nomecompleto AS st_professor,
  tcc.id_usuario AS id_aluno,
  sa.id_categoriasala,
  es.dt_recusa,
  es.id_usuariorecusa,
  es.st_motivorecusa,
  vw_valor.nu_valor,
  es.nu_valorpago
FROM dbo.tb_encerramentosala AS es
JOIN vw_saladeaula AS sa
  ON sa.id_saladeaula = es.id_saladeaula
JOIN dbo.tb_tipodisciplina AS td
  ON sa.id_tipodisciplina = td.id_tipodisciplina
INNER JOIN dbo.tb_disciplina AS di
  ON di.id_disciplina = sa.id_disciplina
OUTER APPLY (SELECT
				TOP 1
				  aa.st_nomecompleto,
				  aa.st_tituloavaliacao,
				  md.id_matricula,
				  up.st_upload,
				  mat.id_usuario,
				  aa.st_nota,
				  aa.st_projetopedagogico,
				  mat.st_nomecompleto as st_nomecompletomatricula,
				  mat.st_projetopedagogico as st_projetopedagogicomatricula
				FROM dbo.tb_encerramentoalocacao AS ee
				JOIN dbo.tb_alocacao AS ali
				  ON ali.id_alocacao = ee.id_alocacao
				JOIN dbo.tb_matriculadisciplina AS md
				  ON md.id_matriculadisciplina = ali.id_matriculadisciplina
				JOIN dbo.vw_matricula AS mat
				  ON mat.id_matricula = md.id_matricula
				LEFT JOIN dbo.vw_avaliacaoaluno AS aa
				  ON aa.id_saladeaula = sa.id_saladeaula
				  AND md.id_matricula = aa.id_matricula
				  AND aa.id_tipoavaliacao = 6
				LEFT JOIN dbo.tb_upload AS up
				  ON aa.id_upload = up.id_upload
				WHERE es.id_encerramentosala = ee.id_encerramentosala) AS tcc
LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS upr
  ON upr.id_saladeaula = sa.id_saladeaula
  AND upr.bl_titular = 1 AND upr.bl_ativo = 1
LEFT JOIN dbo.tb_usuario AS us
  ON us.id_usuario = upr.id_usuario --LEFT JOIN dbo.tb_areadisciplina AS ad ON ad.id_disciplina = sa.id_disciplina
--LEFT JOIN dbo.tb_areaconhecimento AS ac ON ac.id_areaconhecimento = ad.id_areaconhecimento AND AC.id_tipoareaconhecimento = 1
OUTER APPLY (SELECT
  COUNT(DISTINCT id_alocacao) AS nu_alunos
FROM dbo.tb_encerramentoalocacao
WHERE id_encerramentosala = es.id_encerramentosala) AS al


-- totalizacao dos valores
OUTER APPLY (SELECT SUM(nu_valor) AS nu_valor
	FROM dbo.vw_encerramentovalor v 
	WHERE v.id_encerramentosala = es.id_encerramentosala
	GROUP BY id_encerramentosala) AS vw_valor

GO

------ vw_consultatransacoesrecorrente.View.sql
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [rel].[vw_consultatransacoesrecorrente] AS
SELECT DISTINCT
	vw.id_venda,
	CAST(vw.id_venda AS VARCHAR)+ 'G2U' AS st_pedido,
	vw.st_nomecompleto,
	vw.id_meiopagamento,
	vw.st_meiopagamento,
	co.st_cartaooperadora,
	cb.st_cartaobandeira,
	vw.nu_valor,
	vw.nu_quitado,
	vw.dt_vencimento,
	vw.dt_quitado,
	vw.st_situacao,
	l.nu_tentativabraspag,
	tf.st_mensagem,
	tf.id_situacaointegracao,
	vw.nu_parcela,
	si3.st_situacao AS st_situacaointegracao,
	vw.bl_quitado
FROM
	dbo.vw_resumofinanceiro vw
	JOIN dbo.tb_transacaofinanceira tf ON vw.id_venda = tf.id_venda
	JOIN dbo.tb_transacaolancamento tl ON vw.id_lancamento = tl.id_lancamento
	JOIN dbo.tb_lancamento l ON vw.id_lancamento = l.id_lancamento
	LEFT JOIN dbo.tb_cartaobandeira cb ON tf.id_cartaobandeira = cb.id_cartaobandeira
	LEFT JOIN dbo.tb_cartaooperadora co ON tf.id_cartaooperadora = co.id_cartaooperadora
	LEFT JOIN dbo.tb_situacaointegracao si ON tf.id_situacaointegracao = si.id_situacaointegracao
	
OUTER APPLY ( SELECT TOP 1 si2.st_situacao  
       FROM tb_transacaolancamento AS tl 
       INNER JOIN tb_transacaofinanceira AS tf ON tl.id_transacaofinanceira = tf.id_transacaofinanceira
       INNER JOIN tb_situacaointegracao AS si2 ON si2.id_situacaointegracao = tf.id_situacaointegracao
       WHERE tl.id_lancamento = L.id_lancamento
       ORDER BY tf.dt_cadastro DESC
     ) AS si3
     	
WHERE vw.id_meiopagamento = 11

GO

------ vw_ecommerce.View.sql

ALTER VIEW rel.vw_ecommerce AS
SELECT DISTINCT
        st_nomecompleto ,
        st_email ,
        st_cpf ,
        CAST(nu_ddd AS VARCHAR) + CAST(nu_telefone AS VARCHAR) AS st_telefone ,
        st_endereco ,
        st_cep ,
        st_bairro ,
        us.st_complemento ,
        nu_numero ,
        st_cidade ,
        us.sg_uf ,
        pd.st_produto ,
        pd.id_produto ,
		vp.nu_desconto AS nu_descontoproduto,
		vp.nu_valorbruto AS nu_valortabela,
        vp.nu_valorliquido AS nu_valornegociado,
        vd.nu_valorliquido AS nu_valorliquidovenda ,
        vd.dt_confirmacao ,
        mp.st_meiopagamento ,
		mp.id_meiopagamento ,
        pd.id_tipoproduto ,
	vd.id_venda,
        vd.id_evolucao ,
		ev.st_evolucao,
        ve.id_entidade,
        vd.dt_cadastro,
        pdc.st_produto AS st_combo,
        pdc.id_produto AS id_produtocombo,
        ve.st_nomeentidade,
        te.id_entidade AS id_entidadepai,
        vd.st_observacao,
        us.id_usuario AS cod_aluno,
		vd.id_cupom,
		-----

		 (SELECT COALESCE(cti.st_categoria + ', ','')
			FROM dbo.tb_categoriaproduto AS cpi
			JOIN dbo.tb_categoria AS cti
			ON cti.id_categoria = cpi.id_categoria
			WHERE cpi.id_produto = pd.id_produto
			FOR XML PATH('')) AS st_categorias,
			cp.id_categoria,
			cu.nu_desconto AS nu_valorcupom, cu.st_codigocupom, td.st_tipodesconto, cc.st_campanhacomercial, lvc.nu_parcelas,
			(SELECT COALESCE(ac2.st_areaconhecimento + ', ','')
			FROM dbo.tb_areaconhecimento ac2
			JOIN dbo.tb_areaprojetopedagogico app2
			JOIN dbo.tb_produtoprojetopedagogico AS ppp ON ppp.id_projetopedagogico = app2.id_projetopedagogico
			ON ac2.id_areaconhecimento = app2.id_areaconhecimento
			WHERE ppp.id_produto = vp.id_produto
			FOR XML PATH('')) AS st_areaconhecimento


				

FROM    tb_entidade AS te
        JOIN dbo.vw_entidaderecursivaid AS ve ON ve.nu_entidadepai = te.id_entidade OR ve.id_entidade = te.id_entidade
	JOIN dbo.tb_venda AS vd ON vd.id_entidade = ve.id_entidade
	JOIN dbo.tb_evolucao AS ev ON ev.id_evolucao = vd.id_evolucao 
        JOIN dbo.vw_pessoa AS us ON us.id_usuario = vd.id_usuario AND us.id_entidade = vd.id_entidade
        LEFT JOIN dbo.tb_municipio AS mn ON mn.id_municipio = us.id_municipio
        JOIN dbo.tb_vendaproduto AS vp ON vd.id_venda = vp.id_venda
        JOIN dbo.tb_produto AS pd ON pd.id_produto = vp.id_produto
		JOIN tb_cupom AS cu ON cu.id_cupom = vd.id_cupom
		JOIN dbo.tb_campanhacomercial AS cc ON cu.id_campanhacomercial = cc.id_campanhacomercial
		JOIN dbo.tb_tipodesconto AS td ON td.id_tipodesconto = cu.id_tipodesconto
		LEFT JOIN dbo.tb_categoriaproduto AS cp ON cp.id_produto = pd.id_produto
        LEFT JOIN dbo.tb_lancamentovenda AS lv ON lv.id_venda = vd.id_venda AND bl_entrada = 1
        LEFT JOIN dbo.tb_lancamento AS lc ON lc.id_lancamento = lv.id_lancamento
        LEFT JOIN dbo.tb_meiopagamento AS mp ON mp.id_meiopagamento = lc.id_meiopagamento
	LEFT JOIN tb_produto AS pdc ON pdc.id_produto =vp.id_produtocombo 
	OUTER APPLY (SELECT COUNT(lvo.id_lancamento) AS nu_parcelas FROM dbo.tb_lancamentovenda AS lvo WHERE lvo.id_venda = vd.id_venda) AS lvc
	WHERE vd.bl_ativo = 1





GO

----- vw_quantidade_transacoes_cartao_credito.sql

create VIEW [rel].[vw_quantidade_transacoes_cartao_credito2] AS
SELECT DISTINCT 
	e.st_nomeentidade, e.id_entidade, 
	tf.id_venda AS nu_totaltransacoes, 
	visa.id_venda AS nu_visa , 
	masterc.id_venda AS nu_master , 
	amex.id_venda AS nu_amex, 
	elo.id_venda AS nu_elo,
	total.valor AS nu_totalsomatorio,
	dt_transacao.dt_cadastro

FROM tb_transacaofinanceira tf 
	JOIN tb_venda v ON tf.id_venda = v.id_venda 
	JOIN tb_entidade e ON e.id_entidade = v.id_entidade
	CROSS APPLY (SELECT SUM(l.nu_valor) AS valor FROM tb_lancamentovenda lv 
						JOIN tb_lancamento l ON lv.id_lancamento = l.id_lancamento
					WHERE l.id_meiopagamento = 1 
						AND lv.id_venda = v.id_venda 
					GROUP BY lv.id_venda
				) AS total
	CROSS APPLY ( SELECT CAST(tff.dt_cadastro AS DATE) AS dt_cadastro
		FROM tb_transacaofinanceira tff
		WHERE tff.id_venda = v.id_venda) AS dt_transacao
	OUTER APPLY ( SELECT id_venda FROM tb_lancamentovenda lv 
						JOIN tb_lancamento l ON lv.id_lancamento = l.id_lancamento
						JOIN tb_cartaoconfig cc ON l.id_cartaoconfig = cc.id_cartaoconfig
						JOIN tb_cartaobandeira cb ON cc.id_cartaobandeira = cb.id_cartaobandeira 
						JOIN tb_contaentidade ce ON cc.id_contaentidade = ce.id_contaentidade
					WHERE l.id_meiopagamento = 1 
						AND lv.id_venda = v.id_venda 
						AND cb.id_cartaobandeira = 1
					GROUP BY lv.id_venda
				) AS visa
	OUTER APPLY ( SELECT id_venda FROM tb_lancamentovenda lv 
						JOIN tb_lancamento l ON lv.id_lancamento = l.id_lancamento
						JOIN tb_cartaoconfig cc ON l.id_cartaoconfig = cc.id_cartaoconfig
						JOIN tb_cartaobandeira cb ON cc.id_cartaobandeira = cb.id_cartaobandeira 
						JOIN tb_contaentidade ce ON cc.id_contaentidade = ce.id_contaentidade
					WHERE l.id_meiopagamento = 1 
						AND lv.id_venda = v.id_venda 
						AND cb.id_cartaobandeira = 2
					GROUP BY lv.id_venda
				) AS masterc
	OUTER APPLY ( SELECT id_venda FROM tb_lancamentovenda lv 
						JOIN tb_lancamento l ON lv.id_lancamento = l.id_lancamento
						JOIN tb_cartaoconfig cc ON l.id_cartaoconfig = cc.id_cartaoconfig
						JOIN tb_cartaobandeira cb ON cc.id_cartaobandeira = cb.id_cartaobandeira 
						JOIN tb_contaentidade ce ON cc.id_contaentidade = ce.id_contaentidade
					WHERE l.id_meiopagamento = 1 
						AND lv.id_venda = v.id_venda 
						AND cb.id_cartaobandeira = 3
					GROUP BY lv.id_venda
				) AS amex
	OUTER APPLY ( SELECT id_venda FROM tb_lancamentovenda lv 
						JOIN tb_lancamento l ON lv.id_lancamento = l.id_lancamento
						JOIN tb_cartaoconfig cc ON l.id_cartaoconfig = cc.id_cartaoconfig
						JOIN tb_cartaobandeira cb ON cc.id_cartaobandeira = cb.id_cartaobandeira 
						JOIN tb_contaentidade ce ON cc.id_contaentidade = ce.id_contaentidade
					WHERE l.id_meiopagamento = 1 
						AND lv.id_venda = v.id_venda 
						AND cb.id_cartaobandeira = 4
					GROUP BY lv.id_venda
				) AS elo
WHERE tf.nu_status = '0'
--AND v.id_entidade = 14
--GROUP BY e.st_nomeentidade, e.id_entidade
--, dt_transacao.dt_cadastro
--ORDER BY e.st_nomeentidade

ALTER view [dbo].[vw_gerarvendatexto] AS
SELECT
vd.id_venda,
vd.id_entidade,
lc.id_lancamento,
usv.st_cpf,
usl.id_usuario,
ma.id_matricula,

usv.st_nomecompleto AS st_vendacliente,
vd.nu_valorbruto AS nu_vendavalorbruto,
vd.nu_valorliquido AS nu_vendavalorliquido,
vd.nu_descontoporcentagem AS nu_vendadescontoporc,
vd.nu_descontovalor AS nu_vendadescontovalor,

lc.nu_valor AS nu_lancamentovalor,
lc.dt_vencimento AS dt_lancamentovencimento,
lc.dt_quitado AS dt_lancamentodataquitado,
lc.nu_quitado AS nu_lancamentoquitado,
mp.st_meiopagamento AS st_lancamentomeiopagamento,
usl.st_nomecompleto AS st_lancamentoresponsavel,
(SELECT dbo.fn_mesporextenso(lc.dt_vencimento))+'/'+ CAST(DATEPART(YEAR,lc.dt_vencimento) AS VARCHAR)AS st_datavencimento,

vlq.nu_quitado,
vlq.dt_primeiroquitado,
vlq.dt_ultimoquitado,
vlq.st_mesanoprimeiroquitado,
vlq.st_mesanoultimoquitado,
vlq.st_mesanoprimeiroquitado + ' a ' + vlq.st_mesanoultimoquitado AS st_periodoquitado,
CONVERT(VARCHAR(30),GETDATE() , 103) AS st_atual,
CONVERT(VARCHAR(30),YEAR(GETDATE())-1 , 103) AS st_ano,

en.st_endereco,
en.nu_numero,
en.st_bairro,
en.st_cidade,
en.st_cep,

et.st_nomeentidade

FROM tb_venda AS vd
LEFT JOIN tb_lancamentovenda AS lv ON lv.id_venda = vd.id_venda
LEFT JOIN tb_lancamento AS lc ON lc.id_lancamento = lv.id_lancamento
JOIN tb_usuario AS usv ON usv.id_usuario = vd.id_usuario
LEFT JOIN tb_usuario AS usl ON usl.id_usuario = lc.id_usuariolancamento 
LEFT JOIN tb_meiopagamento AS mp ON mp.id_meiopagamento = lc.id_meiopagamento
LEFT JOIN dbo.tb_matricula AS ma ON ma.id_usuario = usl.id_usuario
JOIN dbo.tb_entidade AS et ON et.id_entidade = vd.id_entidade
LEFT JOIN dbo.tb_pessoaendereco AS tp ON tp.id_usuario = vd.id_usuario AND vd.id_entidade = tp.id_entidade AND tp.bl_padrao =1
LEFT JOIN dbo.tb_endereco AS en ON en.id_endereco = tp.id_endereco
LEFT JOIN vw_vendalancamentoquitados AS vlq ON (vd.id_venda = vlq.id_venda)











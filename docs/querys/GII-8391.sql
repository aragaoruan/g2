-- GII-8391 --
CREATE TABLE tb_tramitesaladeaula(
  id_tramitesaladeaula INT IDENTITY (1,1) PRIMARY KEY,
  id_tramite INT NOT NULL FOREIGN KEY REFERENCES dbo.tb_tramite(id_tramite),
  id_saladeaula INT NOT NULL FOREIGN KEY REFERENCES dbo.tb_saladeaula(id_saladeaula),
  id_usuario INT NOT NULL FOREIGN KEY REFERENCES dbo.tb_usuario(id_usuario)
);
SET IDENTITY_INSERT dbo.tb_categoriatramite ON
INSERT INTO dbo.tb_categoriatramite(id_categoriatramite, st_categoriatramite, st_tabela, st_campo, id_tipomanual)
    VALUES(6, 'Sala de Aula', 'tb_tramitesaladeaula', 'id_saladeaula', NULL);
SET IDENTITY_INSERT dbo.tb_categoriatramite OFF
SET IDENTITY_INSERT dbo.tb_tipotramite ON
INSERT INTO dbo.tb_tipotramite(id_tipotramite, id_categoriatramite, st_tipotramite)
    VALUES(31, 6, 'Situação do TCC');
SET IDENTITY_INSERT dbo.tb_tipotramite OFF
-- END GII-8391 --
alter VIEW [dbo].[vw_enviotcc] as

select 

vwi.id_matricula,
vwi.st_nomecompleto,
vwi.st_cpf,
vwi.st_nomeexibicao,
vwi.id_usuario,
vwi.id_entidade,
vwi.id_entidadesala,
vwe.dt_encerramentoprofessor,

vwi.id_projetopedagogico,
vwi.st_tituloexibicaoprojeto,
vwi.id_modulo,
vwi.st_tituloexibicaomodulo,
vwi.id_disciplina,
vwi.st_disciplina,
s.id_saladeaula,
s.st_saladeaula,
vwi.id_professor,
vwi.st_professor,
vwi.id_situacaotcc,
vwa.id_avaliacaoconjuntoreferencia,
vwa.id_avaliacao,
vwi.st_coordenador,
vwi.id_alocacao

-- sp_help vw_matricula;

  from vw_alunogradeintegracao as vwi 
join tb_saladeaula as s
	on (vwi.id_saladeaula = s.id_saladeaula)
join vw_avaliacaoaluno as vwa 
	on (vwi.id_matricula = vwa.id_matricula and vwa.id_saladeaula = vwi.id_saladeaula and vwa.id_tipoavaliacao = 6)
-- join tb_alocacao as al
--	on (al.id_saladeaula = s.id_saladeaula and al.id_matriculadisciplina = vwa.id_matriculadisciplina and al.bl_ativo = 1)
left join vw_encerramentoalunos as vwe 
	on (vwi.id_matricula = vwe.id_matricula and vwa.id_saladeaula = vwe.id_saladeaula)
where vwi.id_tipodisciplina = 2 and (vwi.bl_encerrado = 0 or vwi.bl_encerrado is null)
and vwi.id_situacaotcc = 122
and DATEADD(day, s.nu_diasextensao, vwa.dt_encerramentosala) > GetDate();
GO


-- sp_help [vw_alunogradeintegracao]

-- sp_help tb_alocacao
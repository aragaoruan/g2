-- Insert da funcionalidade
INSERT INTO tb_funcionalidade 
(st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao, st_classeflex,                   st_urlicone, id_tipofuncionalidade, nu_ordem, st_classeflexbotao, bl_pesquisa, bl_lista, id_sistema, st_ajuda, st_target,                        st_urlcaminho, bl_visivel, bl_relatorio, st_urlcaminhoedicao)
VALUES 
('Plano Pagamento',                  316,        1,           3,           '#', '/imagens/icones/semfoto.png',                     1,     NULL,             'ajax',           1,        0,          4,     NULL,      NULL,         '/financeiro/plano-pagamento',          1,            0,                NULL);

-- Verificar se o id_textocategoria é 12
INSERT INTO tb_textocategoria (st_textocategoria, st_descricao, st_metodo) 
	VALUES ('Plano de Pagamento', 'Texto sistema para a impressão dos planos de pagamento', 'gerarContrato');

-- insert das variaveis, fazendo uma copia da categoria 'Contrato'
INSERT INTO tb_textovariaveis SELECT 12,st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel FROM tb_textovariaveis WHERE id_textocategoria = 2

-- vinculando a categoria com a exibição
INSERT INTO tb_textoexibicaocategoria VALUES (1, 12);
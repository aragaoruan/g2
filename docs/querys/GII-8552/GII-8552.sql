INSERT INTO
  tb_textovariaveis (
    id_textocategoria,
    st_camposubstituir,
    st_textovariaveis,
    st_mascara,
    id_tipotextovariavel
  )
VALUES (
  3,
  'id_matricula',
  '#titulacao_diploma#',
  'gerarTitulacao',
  1
);
INSERT INTO
  tb_textovariaveis (
    id_textocategoria,
    st_camposubstituir,
    st_textovariaveis,
    st_mascara,
    id_tipotextovariavel
  )
VALUES (
  3,
  'id_matricula',
  '#titulacao_projeto#',
  'gerarTitulacaoProjeto',
  1
);
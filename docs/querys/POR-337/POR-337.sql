IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'bl_atendimentovirtual'  AND Object_ID = Object_ID(N'dbo.tb_projetopedagogico'))
BEGIN
    alter table tb_projetopedagogico add bl_atendimentovirtual integer DEFAULT 0 not null;
END

ALTER VIEW [dbo].[vw_matriculanota]
AS
SELECT
  mt.id_matricula,
  md.id_matriculadisciplina,
  al.id_saladeaula,
  al.id_alocacao,
  ea.id_encerramentosala,
  -- atividade = 1 - Atividade Presencial
  atividade.id_avaliacao AS id_avaliacaoatividade,
	atividade.id_disciplina AS id_disciplinaatividade,
  atividade.st_nota AS st_notaatividade,
  atividade.id_tiponota AS id_tiponotaatividade,
  atividade.st_avaliacao AS st_avaliacaoatividade,
  atividade.nu_notamax AS nu_notamaxatividade,
  -- recuperacao = 2
  recuperacao.id_avaliacao AS id_avaliacaorecuperacao,
	recuperacao.id_disciplina AS id_disciplinarecuperacao,
  recuperacao.st_nota AS st_notarecuperacao,
  recuperacao.id_tiponota AS id_tiponotarecuperacao,
  recuperacao.st_avaliacao AS st_avaliacaorecuperacao,
  recuperacao.nu_notamax AS nu_notamaxrecuperacao,
  -- FINAL = 4
  final.id_avaliacao AS id_avaliacaofinal,
  final.id_disciplina AS id_disciplinafinal,
  final.st_tituloexibicaodisciplina AS st_disciplinafinal,
  final.st_nota AS st_notafinal,
  final.id_tipoavaliacao,
  final.id_tiponota AS id_tiponotafinal,
  final.st_avaliacao AS st_avaliacaofinal,
  final.nu_notamax AS nu_notamaxfinal,
  -- EAD = 5
  ead.id_avaliacao AS id_avaliacaoead,
  ead.id_disciplina AS id_disciplinaead,
  ead.st_nota AS st_notaead,
  ead.id_tiponota AS id_tiponotaead,
  ead.st_avaliacao AS st_avaliacaoead,
  ead.nu_notamax AS nu_notamaxead,
  ead.id_avaliacaoaluno,
  --TCC = 6
  tcc.id_avaliacao AS id_avaliacaotcc,
  tcc.id_disciplina AS id_disciplinatcc,
  tcc.st_nota AS st_notatcc,
  tcc.id_tiponota AS id_tiponotatcc,
	tcc.st_avaliacao AS st_avaliacaotcc,
  tcc.nu_notamax AS nu_notamaxtcc,

  /*(ISNULL(CAST(final.st_nota AS INT), 0)
  + ISNULL(CAST(atividade.st_nota AS int), 0)
  + ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
  + ISNULL(CAST(ead.st_nota AS INT), 0)) AS nu_notatotal,

  */
  nu_notatotal = CASE WHEN recuperacao.st_nota is NOT NULL AND recuperacao.id_tipocalculoavaliacao = 2
							THEN ( (ISNULL(CAST(final.st_nota AS INT), 0)
						   + ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
						   + ISNULL(CAST(atividade.st_nota AS int), 0)
						   + ISNULL(CAST(recuperacao.st_nota AS INT), 0)
						   + ISNULL(CAST(ead.st_nota AS INT), 0))/2  )
						  WHEN  recuperacao.st_nota is NOT NULL
							THEN (ISNULL(CAST(final.st_nota AS INT), 0)
								+ ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
								+ ISNULL(CAST(atividade.st_nota AS int), 0)
								+ ISNULL(CAST(recuperacao.st_nota AS INT), 0)
								+ ISNULL(CAST(ead.st_nota AS INT), 0))
						  ELSE
							   (ISNULL(CAST(final.st_nota AS INT), 0)
									+ ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
									+ ISNULL(CAST(atividade.st_nota AS int), 0)
									+ ISNULL(CAST(ead.st_nota AS INT), 0))

				 END,
	CAST(((pp.nu_notamaxima * pp.nu_percentualaprovacao) / 100) AS INT) nu_notaaprovacao,

  bl_calcular =
               CASE
                 WHEN ead.id_avaliacao IS NOT NULL AND
                   ead.st_nota IS NULL THEN 0
                 WHEN atividade.id_avaliacao IS NOT NULL AND
                   atividade.st_nota IS NULL THEN 0
                 WHEN final.id_avaliacao IS NOT NULL AND
                   final.st_nota IS NULL THEN 0
                 WHEN tcc.id_avaliacao IS NOT NULL AND
                   tcc.dt_defesa IS NULL THEN 0
                 ELSE 1
               END,
  bl_aprovado =
               CASE
                 WHEN recuperacao.st_nota is null AND (ISNULL(CAST(final.st_nota AS INT), 0)
                   + ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
                   + ISNULL(CAST(atividade.st_nota AS int), 0)
                   + ISNULL(CAST(ead.st_nota AS INT), 0)) >= CAST(((pp.nu_notamaxima
                   * pp.nu_percentualaprovacao)
                   / 100) AS INT) THEN 1
                 WHEN  recuperacao.st_nota is null AND (ISNULL(CAST(final.st_nota AS INT), 0)
                   + ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
                   + ISNULL(CAST(atividade.st_nota AS int), 0)
                   + ISNULL(CAST(ead.st_nota AS INT), 0)) < CAST(((pp.nu_notamaxima
                   * pp.nu_percentualaprovacao)
                   / 100) AS INT) THEN 0
								 WHEN recuperacao.st_nota is NOT NULL AND recuperacao.id_tipocalculoavaliacao = 2
									 AND ( (ISNULL(CAST(final.st_nota AS INT), 0)
										 + ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
										 + ISNULL(CAST(atividade.st_nota AS int), 0)
										 + ISNULL(CAST(recuperacao.st_nota AS INT), 0)
										 + ISNULL(CAST(ead.st_nota AS INT), 0))/2  ) >= CAST(((pp.nu_notamaxima * pp.nu_percentualaprovacaovariavel)
										 / 100) AS INT) THEN 1
									WHEN  recuperacao.st_nota is NOT NULL AND (ISNULL(CAST(final.st_nota AS INT), 0)
										+ ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
										+ ISNULL(CAST(atividade.st_nota AS int), 0)
										+ ISNULL(CAST(recuperacao.st_nota AS INT), 0)
										+ ISNULL(CAST(ead.st_nota AS INT), 0)) < CAST(((pp.nu_notamaxima * pp.nu_percentualaprovacaovariavel )
										/ 100) AS INT) THEN 0
									ELSE 0
               END,
	(CASE
		-- Se tiver RECUPERACAO e o tipo de calculo for MEDIA VARIAVEL, somar todas as notas e dividir por 2
		WHEN recuperacao.st_nota IS NOT NULL AND recuperacao.id_tipocalculoavaliacao = 2 THEN (
			( ISNULL(CAST(final.st_nota AS INT), 0)
				+ ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
				+ ISNULL(CAST(ead.st_nota AS INT), 0)
				+ ISNULL(CAST(atividade.st_nota AS INT), 0)
				+ ISNULL(CAST(recuperacao.st_nota AS INT), 0)
			) / 2
		) ELSE (
			( ISNULL(CAST(final.st_nota AS INT), 0)
				+ ISNULL(CAST(tcc.st_nota AS NUMERIC), 0)
				+ ISNULL(CAST(ead.st_nota AS INT), 0)
				+ ISNULL(CAST( (CASE WHEN recuperacao.st_nota IS NOT NULL THEN recuperacao.st_nota ELSE atividade.st_nota END) AS INT), 0)
			)
		)
	END)
	-- trata porque esse valor pode estar salvo como 0
	/ (CASE WHEN pp.nu_notamaxima IS NULL OR pp.nu_notamaxima = 0 THEN 1 ELSE pp.nu_notamaxima END) * 100
	AS nu_percentualfinal

				FROM tb_matriculadisciplina AS md
				JOIN tb_matricula AS mt
				  ON md.id_matricula = mt.id_matricula
				JOIN dbo.tb_projetopedagogico AS pp
				  ON mt.id_projetopedagogico = pp.id_projetopedagogico
				JOIN dbo.tb_alocacao AS al
				  ON al.id_matriculadisciplina = md.id_matriculadisciplina
				  AND al.bl_ativo = 1
				LEFT JOIN dbo.vw_avaliacaoaluno_simples AS ead
				  ON ead.id_matriculadisciplina = md.id_matriculadisciplina
				  AND al.id_saladeaula = ead.id_saladeaula
				  AND ead.id_tipoavaliacao = 5
				LEFT JOIN dbo.vw_avaliacaoaluno_simples AS atividade
				  ON atividade.id_matriculadisciplina = md.id_matriculadisciplina
				  AND al.id_saladeaula = atividade.id_saladeaula
				  AND atividade.id_tipoavaliacao = 1
				LEFT JOIN tb_encerramentoalocacao AS ea ON ea.id_alocacao = al.id_alocacao

				OUTER APPLY (SELECT TOP 1
							  aaf.st_nota,
							  aaf.id_avaliacao,
							  aaf.id_disciplina,
							  aaf.st_tituloexibicaodisciplina,
							  aaf.id_tipoavaliacao
							  , aaf.id_tiponota
							  , aaf.st_avaliacao
							  ,aaf.nu_notamax
							FROM dbo.vw_avaliacaoaluno_simples AS aaf
							WHERE md.id_matriculadisciplina = aaf.id_matriculadisciplina
							AND aaf.id_tipoavaliacao = 4
							ORDER BY cast(aaf.st_nota as NUMERIC) DESC) AS final
				LEFT JOIN dbo.vw_avaliacaoaluno_simples AS tcc
				  ON tcc.id_matriculadisciplina = md.id_matriculadisciplina
				  AND al.id_saladeaula = tcc.id_saladeaula
				  AND tcc.id_tipoavaliacao = 6

				  --add recuperacao
					  OUTER APPLY (
					    SELECT TOP 1
							  rec.st_nota,
							  rec.id_avaliacao,
							  rec.id_disciplina,
							  rec.st_tituloexibicaodisciplina,
							  rec.id_tipoavaliacao,
							  rec.id_tiponota,
							  rec.st_avaliacao,
							  rec.id_tipocalculoavaliacao,
							  rec.nu_notamax
							FROM dbo.vw_avaliacaoaluno_simples AS rec
							WHERE md.id_matriculadisciplina = rec.id_matriculadisciplina
							AND rec.id_tipoavaliacao = 2
							ORDER BY cast(rec.st_nota as NUMERIC) DESC) AS recuperacao
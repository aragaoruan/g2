--ADICIONANDO MENU DA NOVA FUNCIONALIDADE
SET IDENTITY_INSERT tb_funcionalidade ON
INSERT INTO tb_funcionalidade (id_funcionalidade, st_urlcaminho, id_funcionalidadepai, st_funcionalidade,
                               bl_ativo, id_situacao, id_tipofuncionalidade, bl_pesquisa, bl_lista, bl_visivel, bl_relatorio, bl_delete, id_sistema)
VALUES (
  846, -- id_funcionalidade: Novo ID da funcionalidade. Quem roda a SQL em produ��o fica com a responsabilidade de adicionar um novo ID manualmente.
'/vincular-disciplina-complementar', -- st_urlcaminho: Geralmente, a url da funcionalidade que ser� exibida depois do #/. Aqui � apenas uma string com o id da funcionalidade, visto que nesse caso, estamos adicionando uma funcionalidade interna.
157, -- id_funcionalidadepai: A funcionalidade 'superior' na hierarquia do menu.
'Vincular Disciplina Complementar', -- st_funcionalidade: descri��o da funcionalidade.
1, -- bl_ativo: funcionalidade ativa
123, -- id_situacao: funcionalidade em HTML
3, -- id_tipofuncionalidade
0, -- bl_pesquisa
0, -- bl_lista
1, -- bl_visivel
0, -- bl_relatorio
0, -- bl_delete
1); --id_sistema: 1 (G2S)
SET IDENTITY_INSERT tb_funcionalidade OFF


--ADICIONANDO COLUNA dt_matriculavinculada na tabela tb_matricula
ALTER TABLE tb_matricula ADD dt_matriculavinculada datetime2 DEFAULT getdate() NULL;
EXEC sp_addextendedproperty 'MS_Description', 'Data que a matricula foi vinculada', 'SCHEMA', 'dbo', 'TABLE', 'tb_matricula', 'COLUMN', 'dt_matriculavinculada';


--CRIANDO TABELA HISTORICOVINCULARMATRICULA
CREATE TABLE tb_historicovincularmatricula
(
  id_historicovincularmatricula INT PRIMARY KEY NOT NULL IDENTITY,
  id_matriculavinculada INT NOT NULL,
  id_matricula INT NOT NULL,
  id_usuario INT NOT NULL,
  dt_historicovincularmatricula DATETIME NOT NULL,
  st_historicovincularmatricula VARCHAR(255) NOT NULL ,
  bl_vinculada BIT DEFAULT 0 NOT NULL,

  CONSTRAINT tb_historicovinculardisciplina_tb_matricula_id_matriculavinculada_fk
    FOREIGN KEY(id_matriculavinculada) REFERENCES tb_matricula(id_matricula),

  CONSTRAINT tb_historicovinculardisciplina_tb_matricula_id_matricula_fk
    FOREIGN KEY(id_matricula) REFERENCES tb_matricula(id_matricula),

  CONSTRAINT tb_historicovinculardisciplina_tb_usuario_id_usuario_fk
    FOREIGN KEY(id_usuario) REFERENCES tb_usuario(id_usuario),
);
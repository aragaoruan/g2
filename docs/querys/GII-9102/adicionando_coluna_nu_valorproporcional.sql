ALTER TABLE tb_vendaaditivo ADD nu_valorproporcional NUMERIC(30, 2) DEFAULT 0;

UPDATE tb_vendaaditivo SET nu_valorproporcional = 0;
-- POR-212 --
SET IDENTITY_INSERT dbo.tb_mensagempadrao ON
INSERT INTO dbo.tb_mensagempadrao(id_mensagempadrao, id_tipoenvio, st_mensagempadrao, st_default, id_textocategoria)
    VALUES(45, 3, 'Devolução de TCC ao Aluno', 'Mensagem de aviso ao aluno sobre a devolução do TCC pelo Orientador', null),
      (46, 3, 'Avaliação do TCC pelo Orientador', 'Mensagem de aviso ao aluno sobre a Avaliação do seu TCC pelo Orientador', null),
      (48, 3, 'Devolução de TCC ao Tutor', 'Mensagem de aviso ao tutor sobre a Avaliação do TCC do aluno pelo Coordenador/Pedagógico', null);
SET IDENTITY_INSERT dbo.tb_mensagempadrao OFF
-- END POR-212 --
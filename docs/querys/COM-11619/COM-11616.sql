
alter table tb_projetopedagogicointegracao add id_entidade int;

ALTER TABLE dbo.tb_projetopedagogicointegracao ADD CONSTRAINT
	tb_projetopedagogicointegracao_tb_entidade_FK FOREIGN KEY
	(
	id_entidade
	) REFERENCES dbo.tb_entidade
	(
	id_entidade
	) ON UPDATE  NO ACTION
	 ON DELETE  NO ACTION
GO

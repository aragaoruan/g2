BEGIN TRANSACTION
insert into tb_fatorrh (st_fatorrh) values ('Positivo'),('Negativo')
COMMIT

BEGIN TRANSACTION
create table tb_fatorrh (id_fatorrh int primary key IDENTITY(1,1), st_fatorrh varchar(8))
COMMIT

BEGIN TRANSACTION
ALTER TABLE tb_dadosbiomedicos
ADD FOREIGN KEY (id_fatorrh) REFERENCES tb_fatorrh(id_fatorrh);
COMMIT

BEGIN TRANSACTION
alter table tb_dadosbiomedicos add id_fatorrh int
COMMIT
BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_funcionalidade on
INSERT INTO [dbo].[tb_funcionalidade]
([id_funcionalidade]
,[st_funcionalidade]
,[id_funcionalidadepai]
,[bl_ativo]
,[id_situacao]
,[st_classeflex]
,[st_urlicone]
,[id_tipofuncionalidade]
,[st_classeflexbotao]
,[bl_pesquisa]
,[bl_lista])
VALUES

(500,'Afiliação',4,1,3,'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa','/img/ico/flag.png',2,null,0,0),
(501,'Classe de Afiliado',500,1,3,'br.com.ead1.gestor2.view.conteudo.telemarketing.cadastrar.CadastrarClasseAfiliado','/img/ico/flag-blue.png',3,null,1,0),
(502,'Classe de Afiliado',501,1,3,'br.com.ead1.gestor2.view.conteudo.telemarketing.cadastrar.classeafiliado.AbaClasseAfiliado',null,4,null,0,0),
(503,'Classe de Afiliado',502,1,3,'br.com.ead1.gestor2.view.conteudo.telemarketing.cadastrar.classeafiliado.basicos.GrupoClasseAfiliado',null,5,null,0,0)

GO
SET IDENTITY_INSERT dbo.tb_funcionalidade off
ROLLBACK
COMMIT


select * from tb_funcionalidade

SELECT * FROM dbo.tb_tipofuncionalidade
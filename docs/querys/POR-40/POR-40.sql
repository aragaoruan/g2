ALTER TABLE tb_logacesso ADD st_parametros TEXT NULL;
ALTER TABLE tb_logacesso ADD id_matricula INT NULL;

set IDENTITY_INSERT tb_funcionalidade on

--Meus Dados
insert into tb_funcionalidade(id_funcionalidade,
                              st_funcionalidade,
                              id_funcionalidadepai,
                              bl_ativo, id_situacao,
                              st_classeflex, st_urlicone,
                              id_tipofuncionalidade, nu_ordem,
                              st_classeflexbotao, bl_pesquisa, bl_lista,
                              id_sistema, st_ajuda, st_target, st_urlcaminho,
                              bl_visivel, bl_relatorio, st_urlcaminhoedicao,
                              bl_delete)
    VALUES (771,'Meus Dados', null, 1, 182, 'app.meusdados', NULL, 1, NULL , NULL, 0, 0, 4,'', NULL, 'app.meusdados', '1', 0, null, 0)

-- Curso
insert into tb_funcionalidade(id_funcionalidade,
                              st_funcionalidade,
                              id_funcionalidadepai,
                              bl_ativo, id_situacao,
                              st_classeflex, st_urlicone,
                              id_tipofuncionalidade, nu_ordem,
                              st_classeflexbotao, bl_pesquisa, bl_lista,
                              id_sistema, st_ajuda, st_target, st_urlcaminho,
                              bl_visivel, bl_relatorio, st_urlcaminhoedicao,
                              bl_delete)
    VALUES (772,'Curso', null, 1, 182, 'app.curso', NULL, 1, NULL , NULL, 0, 0, 4,'', NULL, 'app.curso', '1', 0, null, 0)

--Disciplina
insert into tb_funcionalidade(id_funcionalidade,
                              st_funcionalidade,
                              id_funcionalidadepai,
                              bl_ativo, id_situacao,
                              st_classeflex, st_urlicone,
                              id_tipofuncionalidade, nu_ordem,
                              st_classeflexbotao, bl_pesquisa, bl_lista,
                              id_sistema, st_ajuda, st_target, st_urlcaminho,
                              bl_visivel, bl_relatorio, st_urlcaminhoedicao,
                              bl_delete)
    VALUES (773,'Disciplina', null, 1, 182, 'app.disciplina', NULL, 1, NULL , NULL, 0, 0, 4,'', NULL, 'app.disciplina', '1', 0, null, 0)


--Novo Atendimento
insert into tb_funcionalidade(id_funcionalidade,
                              st_funcionalidade,
                              id_funcionalidadepai,
                              bl_ativo, id_situacao,
                              st_classeflex, st_urlicone,
                              id_tipofuncionalidade, nu_ordem,
                              st_classeflexbotao, bl_pesquisa, bl_lista,
                              id_sistema, st_ajuda, st_target, st_urlcaminho,
                              bl_visivel, bl_relatorio, st_urlcaminhoedicao,
                              bl_delete)
    VALUES (774,'Novo Atendimento', 768, 1, 182, 'app.novo-atendimento', NULL, 1, NULL , NULL, 0, 0, 4,'', NULL, 'app.novo-atendimento', '1', 0, null, 0)

set IDENTITY_INSERT tb_funcionalidade off
CREATE TABLE dbo.tb_declaracaoaluno (
id_declaracao INT IDENTITY(1,1) PRIMARY KEY,
id_usuario int not null,
id_entidade int not null,
dt_cadastro datetime default getdate()
);

CREATE TABLE dbo.tb_produtodeclaracaoaluno (
id_produtodeclaracaoaluno INT IDENTITY(1,1) PRIMARY KEY,
id_produto int not null,
id_declaracao int not null,
);

ALTER TABLE tb_configuracaoentidade ADD id_assuntorequerimento INT NULL;
ALTER TABLE dbo.tb_ocorrencia ADD id_venda int;


UPDATE tb_configuracaoentidade SET id_categoriaocorrencia = 30, id_assuntorequerimento = 1950 where id_entidade = 19;


SET IDENTITY_INSERT dbo.tb_funcionalidade ON;
INSERT INTO dbo.tb_funcionalidade
        ( id_funcionalidade ,
		      st_funcionalidade ,
          id_funcionalidadepai ,
          bl_ativo ,
          id_situacao ,
          st_classeflex ,
          st_urlicone ,
          id_tipofuncionalidade ,
          nu_ordem ,
          st_classeflexbotao ,
          bl_pesquisa ,
          bl_lista ,
          id_sistema ,
          st_ajuda ,
          st_target ,
          st_urlcaminho ,
          bl_visivel ,
          bl_relatorio ,
          st_urlcaminhoedicao ,
          bl_delete
        )
VALUES  ( 700 ,
		      'Requerimento de Serviços' , -- st_funcionalidade - varchar(100)
          313 , -- id_funcionalidadepai - int
          1 , -- bl_ativo - bit
          123 , -- id_situacao - int
          '/secretaria/requerimento' , -- st_classeflex - varchar(300)
          '' , -- st_urlicone - varchar(1000)
          1 , -- id_tipofuncionalidade - int
          NULL , -- nu_ordem - int
          '' , -- st_classeflexbotao - varchar(300)
          0 , -- bl_pesquisa - bit
          1 , -- bl_lista - bit
          4 , -- id_sistema - int
          NULL , -- st_ajuda - varchar(max)
          NULL , -- st_target - varchar(100)
          '/secretaria/requerimento' , -- st_urlcaminho - varchar(50)
          1 , -- bl_visivel - int
          0 , -- bl_relatorio - bit
          NULL , -- st_urlcaminhoedicao - varchar(50)
          0  -- bl_delete - bit
        )
SET IDENTITY_INSERT dbo.tb_funcionalidade OFF;

SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vw_produto]
AS
    SELECT  pd.id_produto ,
            pd.st_produto ,
            pd.id_tipoproduto ,
            pd.id_situacao ,
            pd.id_usuariocadastro ,
            pd.id_entidade ,
            pd.bl_ativo ,
            pd.bl_todasformas ,
            pd.bl_todascampanhas ,
            pd.nu_gratuito ,
            pd.id_produtoimagempadrao ,
            pd.bl_unico ,
            pd.st_descricao ,
            pd.bl_todasentidades ,
            tpd.st_tipoproduto ,
            pdv.id_produtovalor ,
            pdv.nu_valor ,
            pdv.nu_valormensal ,
            tpv.id_tipoprodutovalor ,
            tpv.st_tipoprodutovalor ,
            st.st_situacao ,
            pl.id_planopagamento ,
            pl.nu_parcelas ,
            pl.nu_valorentrada ,
            pl.nu_valorparcela ,
            te.st_nomeentidade ,
            pd.st_subtitulo ,
            pd.st_slug ,
            pd.bl_destaque ,
            pd.dt_cadastro ,
            pd.dt_atualizado ,
           (
				CASE WHEN pdvp.dt_inicio IS NULL
					THEN pd.dt_iniciopontosprom
					ELSE pdvp.dt_inicio
				END
            ) AS dt_iniciopontosprom,
            --pd.dt_iniciopontosprom ,
            (
            CASE WHEN pdvp.dt_termino IS NULL
				THEN pd.dt_fimpontosprom
				ELSE pdvp.dt_termino
				END
            ) AS dt_fimpontosprom,
            --pd.dt_fimpontosprom ,
            pd.nu_pontos ,
            pd.nu_pontospromocional ,
            pd.nu_estoque ,
            pd.st_observacoes ,
            pd.st_informacoesadicionais ,
            pd.st_estruturacurricular ,
            pd.id_modelovenda ,
            mv.st_modelovenda ,
            pd.bl_mostrarpreco ,
            id_produtovalorvenda = CASE WHEN pdvp.id_produtovalor IS NULL
                                        THEN pdv.id_produtovalor
                                        ELSE pdvp.id_produtovalor
                                   END ,
            nu_valorvenda = CASE WHEN pdvp.id_produtovalor IS NULL
                                 THEN ( CASE WHEN pd.id_modelovenda = 2
                                             THEN pdv.nu_valormensal
                                             ELSE pdv.nu_valor
                                        END )
                                 ELSE ( CASE WHEN pd.id_modelovenda = 2
                                             THEN pdvp.nu_valormensal
                                             ELSE pdvp.nu_valor
                                        END )
                            END ,
            pdvp.nu_valor AS nu_valorpromocional,
			ppp.id_turma,
			pts.id_produtotextosistema,
			pts.id_upload,
			pts.id_textosistema,
			fp.id_formadisponibilizacao,
			fp.st_formadisponibilizacao,
			up.st_upload
    FROM    tb_produto AS pd
            OUTER APPLY ( SELECT TOP 1
                                    pvoa.id_produtovalor ,
                                    pvoa.nu_valor ,
                                    pvoa.nu_valormensal ,
                                    pvoa.id_tipoprodutovalor ,
                                    pvoa.id_produto
                          FROM      tb_produtovalor AS pvoa
                          WHERE     pvoa.id_produto = pd.id_produto
                                    AND pvoa.dt_inicio <= CAST(GETDATE() AS DATE)
                                    AND ( CAST(pvoa.dt_termino AS DATE) >= CAST(GETDATE() AS DATE)
                                          OR pvoa.dt_termino IS NULL)
                                          AND pvoa.id_tipoprodutovalor = 4

                        ) AS pdv
            OUTER APPLY ( SELECT TOP 1
                                    id_produtovalor ,
                                    nu_valor ,
                                    nu_valormensal ,
                                    dt_inicio ,
                                    dt_termino ,
                                    id_produto
                          FROM      tb_produtovalor AS pvo
                          WHERE     pvo.id_produto = pd.id_produto
                                    AND pvo.dt_inicio <= CAST(GETDATE() AS DATE)
                                    AND ( CAST(pvo.dt_termino AS DATE) >= CAST(GETDATE() AS DATE)
                                          OR pvo.dt_termino IS NULL
                                        )
                                    AND pvo.id_tipoprodutovalor = 7
                                    AND pvo.nu_valor > 0.00
                        ) AS pdvp
            LEFT JOIN tb_tipoproduto AS tpd ON tpd.id_tipoproduto = pd.id_tipoproduto
            LEFT JOIN tb_tipoprodutovalor AS tpv ON pdv.id_tipoprodutovalor = tpv.id_tipoprodutovalor
            LEFT JOIN tb_situacao AS st ON st.id_situacao = pd.id_situacao
            LEFT JOIN tb_planopagamento AS pl ON pl.id_produto = pd.id_produto
                                                 AND pl.bl_padrao = 1
            JOIN dbo.tb_entidade AS te ON te.id_entidade = pd.id_entidade
            JOIN dbo.tb_modelovenda AS mv ON mv.id_modelovenda = pd.id_modelovenda
			LEFT JOIN dbo.tb_produtoprojetopedagogico AS ppp ON ppp.id_produto = pd.id_produto
			LEFT JOIN dbo.tb_produtotextosistema AS pts ON pts.id_produto = pd.id_produto
			LEFT JOIN dbo.tb_formadisponibilizacao AS fp ON fp.id_formadisponibilizacao = pts.id_formadisponibilizacao
			LEFT JOIN dbo.tb_upload AS up ON up.id_upload = pts.id_upload
    WHERE   pd.bl_ativo = 1



GO


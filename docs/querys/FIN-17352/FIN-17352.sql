CREATE TABLE tb_minhapasta
(
    id_minhapasta INT PRIMARY KEY IDENTITY,
    st_contrato VARCHAR(255) NULL,
    id_matricula INT NOT NULL,
    id_venda INT NOT NULL,
    id_upload INT NULL,
    id_usuario INT NOT NULL,
    id_usuariocadastro INT NOT NULL,
    id_usuarioatualizacao INT NULL,
    id_situacao INT NOT NULL,
    id_evolucao INT NOT NULL,
    bl_ativo INT NOT NULL DEFAULT(1),
    dt_cadastro DATETIME DEFAULT GETDATE(),
    dt_atualizacao DATETIME NULL
    );

SET IDENTITY_INSERT dbo.tb_situacao ON
	INSERT INTO tb_situacao (id_situacao, st_situacao, st_tabela, st_campo, st_descricaosituacao) VALUES
	(180, 'Ativo', 'tb_minhapasta', 'id_situacao', 'Contrato ativo para visualização do aluno'),
	(181, 'inativo', 'tb_minhapasta', 'id_situacao', 'Contrato inativo para visualização do aluno'),
SET IDENTITY_INSERT dbo.tb_situacao OFF

SET IDENTITY_INSERT dbo.tb_situacao ON
	INSERT tb_evolucao (id_evolucao, st_evolucao, st_tabela, st_campo) VALUES
  (66, 'Adicionou contrato', 'tb_minhapasta', 'id_evolucao')
  (67, 'Inativou contrato', 'tb_minhapasta', 'id_evolucao')
  (68, 'Removeu contrato', 'tb_minhapasta', 'id_evolucao')
SET IDENTITY_INSERT dbo.tb_situacao OFF

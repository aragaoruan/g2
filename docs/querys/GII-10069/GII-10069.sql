--CRIANDO COLUNA
ALTER TABLE tb_ocorrencia ADD bl_confirmarciencia bit DEFAULT 0 NULL;

-- ATUALIZANDO TODAS PARA CONFIRMAR CIENCIA 0
UPDATE tb_ocorrencia
SET bl_confirmarciencia = 0;

-- ATUALIZANDO TODAS AS OCORRENCIAS COM A DATA DE CADASTRO MENOR QUE 05/06/2018
UPDATE tb_ocorrencia
SET bl_confirmarciencia = 1

WHERE id_ocorrencia IN (
  SELECT
  id_ocorrencia
  FROM tb_ocorrencia
  WHERE id_situacao = 99 AND dt_cadastro < '2018-06-05'
);
-- Para tabelas

SELECT 
	tabela.name, 
	coluna.name,
	convert(nvarchar(255),
	'/**'+
	CHAR(13)+'* @var '+(case tipo.name
	       when 'bit' then 'boolean'
	       when 'varchar' then 'string'
	       when 'int' then 'integer'
	       when 'numeric' then 'decimal'
               else tipo.name
	end)+' $'+coluna.name+
	CHAR(13)+'* @Column(name="'+coluna.name+'", type="'+
	(case tipo.name
	       when 'bit' then 'boolean'
	       when 'varchar' then 'string'
	       when 'int' then 'integer'
	       when 'numeric' then 'decimal'
               else tipo.name
	end)
	+'", nullable='+
	(case coluna.is_nullable
	       when 1 then 'true'
	       when 0 then 'false'
	end) +
	(case coluna.max_length
	       when -1 then ''
	       else ', length='+convert(varchar, coluna.max_length)+')'
        end) +
	CHAR(13)+'*/'+
	CHAR(13)+' private $'+coluna.name+';') as coluna,
	tipo.name,
	coluna.is_nullable
from sys.tables tabela
join sys.columns coluna on tabela.object_id = coluna.object_id
JOIN sys.types tipo on tipo.system_type_id = coluna.system_type_id
where tabela.name = 'tb_itemgradehorariaturma'

-- Para views

SELECT
	tabela.name,
	CONVERT(nvarchar(255),
	'/**' + (CASE coluna.is_identity
		WHEN 1 THEN '@Id '
		ELSE ''
	END) +
	CHAR(13) + '* @var ' + (CASE tipo.name
		WHEN 'bit' THEN 'boolean'
		WHEN 'varchar' THEN 'string'
		WHEN 'int' THEN 'integer'
		WHEN 'numeric' THEN 'decimal' ELSE tipo.name
	END) + ' $' + coluna.name +
	CHAR(13) + '* @Column(name="' + coluna.name + '", type="' +
	(CASE tipo.name
   WHEN 'bit'
     THEN 'boolean'
   WHEN 'varchar'
     THEN 'string'
   WHEN 'int'
     THEN 'integer'
   WHEN 'numeric'
     THEN 'decimal'
   ELSE tipo.name
   END)
	+ '", nullable=' +
	(CASE coluna.is_nullable
		WHEN 1 THEN 'true'
		WHEN 0 THEN 'false'
	END) +
	(CASE coluna.max_length
		WHEN -1 THEN '' ELSE ', length=' + CONVERT(varchar, coluna.max_length) + ')'
	END) +
	CHAR(13) + '*/' +
	CHAR(13) + ' private $' + coluna.name + ';') AS coluna,
	CONVERT(nvarchar(500),
	CHAR(13) +
	'/**' +
	CHAR(13) + '* @return ' + (CASE tipo.name
		WHEN 'bit' THEN 'boolean'
		WHEN 'varchar' THEN 'string'
		WHEN 'int' THEN 'integer'
		WHEN 'numeric' THEN 'decimal' ELSE tipo.name
	END) + ' ' + coluna.name +
	CHAR(13) + '*/' +
	CHAR(13) + 'public function get' + UPPER(LEFT(coluna.name, 1)) + RIGHT(coluna.name, LEN(coluna.name) - 1) + '() {' +
	CHAR(13) + 'return $this->' + coluna.name + ';' +
	CHAR(13) + '}' +
	CHAR(13) +
	CHAR(13) +
	'/**' +
	CHAR(13) + '* @param ' + coluna.name +
	CHAR(13) + '*/' +
	CHAR(13) + 'public function set' + UPPER(LEFT(coluna.name, 1)) + RIGHT(coluna.name, LEN(coluna.name) - 1) + '($' + coluna.name + ') {' +
	CHAR(13) + '$this->' + coluna.name + ' = ' + '$' + coluna.name + ';' +
	CHAR(13) + 'return $this;' +
	CHAR(13) + '}' +
	CHAR(13)) AS getsandsets,
	tipo.name,
	coluna.is_nullable,
	coluna.is_identity
FROM sys.views tabela
JOIN sys.columns coluna
	ON tabela.object_id = coluna.object_id
JOIN sys.types tipo
	ON tipo.system_type_id = coluna.system_type_id
WHERE tabela.name = 'vw_projetoentidade'

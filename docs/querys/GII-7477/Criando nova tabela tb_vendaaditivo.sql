CREATE TABLE tb_vendaaditivo (
    id_vendaaditivo INT NOT NULL PRIMARY KEY IDENTITY(1,1),
    id_venda INT NOT NULL FOREIGN KEY REFERENCES tb_venda(id_venda),
    id_matricula INT NOT NULL FOREIGN KEY REFERENCES tb_matricula(id_matricula),
    id_usuario INT NOT NULL FOREIGN KEY REFERENCES tb_usuario(id_usuario),
    dt_cadastro DATETIME DEFAULT GETDATE()
);
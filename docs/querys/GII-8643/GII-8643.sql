SET IDENTITY_INSERT tb_funcionalidade ON

INSERT INTO "tb_funcionalidade"
("id_funcionalidade",
 "st_funcionalidade",
 "id_funcionalidadepai",
 "bl_ativo",
 "id_situacao",
 "st_classeflex",
 "st_urlicone",
 "id_tipofuncionalidade",
 "nu_ordem",
 "st_classeflexbotao",
 "bl_pesquisa",
 "bl_lista",
 "id_sistema",
 "st_ajuda",
 "st_target",
 "st_urlcaminho",
 "bl_visivel",
 "bl_relatorio",
 "st_urlcaminhoedicao",
 "bl_delete")
VALUES ((SELECT
           max(id_funcionalidade) + 1
         FROM tb_funcionalidade), 'Contatos do Site - Atendente', 338, 'True', 123, NULL, NULL, 3, 0, NULL, 'False',
                                                                  'False', 1, NULL, NULL, '/contatos-site-atendente', 1,
        0, NULL, 'False');
SET IDENTITY_INSERT tb_funcionalidade OFF

/*

delete from tb_perfilfuncionalidade where id_funcionalidade in (813,812,811);

delete from tb_funcionalidade where id_funcionalidade in (813,812,811);

select * from tb_funcionalidade where st_funcionalidade like '%Contatos do Site%'

-- update tb_funcionalidade set bl_relatorio = 0 where id_funcionalidade = 814

*/
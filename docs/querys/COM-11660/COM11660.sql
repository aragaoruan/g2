SET IDENTITY_INSERT dbo.tb_funcionalidade on
INSERT INTO dbo.tb_funcionalidade
  (
    id_funcionalidade,
    st_funcionalidade,
    id_funcionalidadepai,
    bl_ativo,
    id_situacao,
    st_classeflex,
    st_urlicone,
    id_tipofuncionalidade,
    nu_ordem,
    st_classeflexbotao,
    bl_pesquisa,
    bl_lista,
    id_sistema,
    st_ajuda,
    st_target,
    st_urlcaminho,
    bl_visivel,
    bl_relatorio,
    st_urlcaminhoedicao,
    bl_delete
  )
  VALUES (
    738
    'Folha de Pagamento - Turmas Regulares', -- st_funcionalidade - varchar(100)
    488, -- id_funcionalidadepai - int
    1, -- bl_ativo - bit
    123, -- id_situacao - int
    'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa', -- st_classeflex - varchar(300)
    '/img/ico/door--arrow.png', -- st_urlicone - varchar(1000)
    3, -- id_tipofuncionalidade - int
    NULL, -- nu_ordem - int
    NULL, -- st_classeflexbotao - varchar(300)
    0, -- bl_pesquisa - bit
    0, -- bl_lista - bit
    1, -- id_sistema - int
    NULL, -- st_ajuda - varchar(max)
    NULL, -- st_target - varchar(100)
    '/relatorio-folha-pagamento', -- st_urlcaminho - varchar(50)
    1, -- bl_visivel - int
    0, -- bl_relatorio - bit
    NULL, -- st_urlcaminhoedicao - varchar(50)
    0  -- bl_delete - bit
  );
SET IDENTITY_INSERT dbo.tb_funcionalidade off

CREATE view vw_professoraulasministradas as
SELECT
		u.id_usuario,
		u.st_nomecompleto,
		igh.id_unidade,
		a.id_areaconhecimento,
		a.st_areaconhecimento,
		e.st_nomeentidade,
		gh.id_gradehoraria,
		igh.dt_diasemana,
		datepart(month,igh.dt_diasemana) as nu_mes,
		datepart(year,igh.dt_diasemana) as nu_ano,
		concat(FORMAT(gh.dt_iniciogradehoraria, 'd/M'),' à ', FORMAT(gh.dt_fimgradehoraria, 'd/MM/yy')) as dt_periodo
	FROM dbo.tb_usuario u
		JOIN tb_usuarioperfilentidade up ON up.id_usuario = u.id_usuario
		JOIN tb_perfil p ON p.id_perfil = up.id_perfil
		JOIN dbo.tb_itemgradehoraria igh ON igh.id_professor = u.id_usuario and igh.bl_ativo = 1
		JOIN dbo.tb_gradehoraria gh ON gh.id_gradehoraria = igh.id_gradehoraria and gh.bl_ativo = 1
		JOIN dbo.tb_entidade e ON igh.id_unidade = e.id_entidade
		JOIN dbo.tb_areaprojetopedagogico app ON app.id_projetopedagogico = igh.id_projetopedagogico
		JOIN dbo.tb_areaconhecimento a ON a.id_areaconhecimento = app.id_areaconhecimento
	WHERE
		p.id_perfilpedagogico = 1
		AND p.bl_ativo = 1
		AND u.bl_ativo = 1
		AND up.bl_ativo = 1
		AND u.bl_ativo = 1
	GROUP BY
		u.id_usuario,
		u.st_nomecompleto,
		igh.id_unidade,
		app.id_projetopedagogico,
		a.id_areaconhecimento,
		a.st_areaconhecimento,
		e.st_nomeentidade,
		gh.id_gradehoraria,
		gh.dt_iniciogradehoraria,
		gh.dt_fimgradehoraria,
		igh.id_disciplina,
		igh.id_gradehoraria,
		igh.dt_diasemana


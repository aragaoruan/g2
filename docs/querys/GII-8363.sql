SET IDENTITY_INSERT tb_funcionalidade ON

INSERT INTO "tb_funcionalidade"
("id_funcionalidade",
 "st_funcionalidade",
 "id_funcionalidadepai",
 "bl_ativo",
 "id_situacao",
 "st_classeflex",
 "st_urlicone",
 "id_tipofuncionalidade",
 "nu_ordem",
 "st_classeflexbotao",
 "bl_pesquisa",
 "bl_lista",
 "id_sistema",
 "st_ajuda",
 "st_target",
 "st_urlcaminho",
 "bl_visivel",
 "bl_relatorio",
 "st_urlcaminhoedicao",
 "bl_delete")
VALUES
  (806, 'Promo��es', NULL, 1, 182, '/promocoes', '/imagens/icones/semfoto.png', 1, NULL, 'ajax', 0, 0, 4, NULL, NULL,
   '806', 1, 0, NULL, 0);
SET IDENTITY_INSERT tb_funcionalidade OFF

INSERT INTO tb_perfilfuncionalidade (id_perfil, id_funcionalidade, bl_ativo)
  (SELECT
     id_perfil,
     806,
     1
   FROM tb_perfil
   WHERE id_perfilpedagogico = 5 AND id_perfil NOT IN (SELECT
                                                         id_perfil
                                                       FROM tb_perfilfuncionalidade
                                                       WHERE id_funcionalidade = 806))


INSERT INTO [tb_entidadefuncionalidade] (id_entidade, id_funcionalidade, bl_ativo, id_situacao)
  (SELECT
     id_entidade,
     806,
     1,
     5
   FROM tb_entidade
   WHERE id_entidade NOT IN (SELECT
                               id_entidade
                             FROM tb_entidadefuncionalidade
                             WHERE id_funcionalidade = 806))

UPDATE tb_funcionalidade
SET id_situacao = 182, st_classeflex = 'app.promocoes', st_urlicone = 'icon-icon_disc', nu_ordem = 1,
  st_urlcaminho = 'app.promocoes'
WHERE id_funcionalidade = 806;

-- select max(id_funcionalidade) from tb_funcionalidade
-- select * from tb_funcionalidade where id_sistema = 4
/*
select * from tb_funcionalidade where id_funcionalidade = 806;
SELECT fc.* 
                  FROM tb_perfil AS pf
                  JOIN tb_perfilfuncionalidade AS ppf ON ppf.id_perfil = pf.id_perfil 
                                                     AND ppf.bl_ativo = 1
                  JOIN tb_entidadefuncionalidade AS ef ON ef.id_entidade = pf.id_entidade 
                                                      AND ef.bl_visivel = 1 
                                                      AND ef.bl_ativo = 1
                  JOIN tb_funcionalidade AS fc ON fc.id_funcionalidade = ppf.id_funcionalidade 
                                              AND fc.id_funcionalidade = ef.id_funcionalidade 
                                              AND fc.bl_ativo = 1 
                                              AND fc.id_sistema = 4
                WHERE pf.id_entidade = 14 
                  AND pf.id_perfilpedagogico = 5 
                  AND pf.bl_ativo = 1 
                  AND fc.id_situacao = 182
				  */
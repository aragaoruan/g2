/*
* Altera��o tb_usuarioperfilentidadereferencia
*/
alter table tb_usuarioperfilentidadereferencia add bl_desativarmoodle char(1)


/*
* Cria��o de view para listar os coordenadores que tem que ter papel retirado das salas no moodle
*/

create view vw_desativarcoordenadoresmoodle 
as
select 
uper.id_perfilreferencia,
uper.id_usuario,
u.st_nomecompleto,
pp.id_projetopedagogico,
pp.st_projetopedagogico,
pri.id_perfilreferenciaintegracao,
pri.id_saladeaulaintegracao,
pri.bl_encerrado,
pri.id_sistema,
usi.st_codusuario,
sali.st_codsistemacurso,
usi.id_entidade,
uper.bl_desativarmoodle

from tb_usuarioperfilentidadereferencia as uper
join tb_usuario as u on u.id_usuario = uper.id_usuario
join tb_projetopedagogico as pp on pp.id_projetopedagogico=uper.id_projetopedagogico
join tb_perfilreferenciaintegracao as pri on pri.id_perfilreferencia=uper.id_perfilreferencia and pri.id_sistema=6 and pri.bl_encerrado=0
join tb_saladeaulaintegracao as sali on sali.id_saladeaulaintegracao = pri.id_saladeaulaintegracao
join tb_usuariointegracao as usi on usi.id_usuario = uper.id_usuario and usi.id_sistema=pri.id_sistema
where uper.bl_desativarmoodle = 1


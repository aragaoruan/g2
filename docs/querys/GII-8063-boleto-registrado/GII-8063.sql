SET IDENTITY_INSERT tb_mensagempadrao ON;
INSERT INTO [tb_mensagempadrao]
(id_mensagempadrao, id_tipoenvio, st_mensagempadrao, st_default, id_textocategoria)
VALUES (44, 3, 'Envio do Boleto (Taxas e serviços)', NULL, NULL);
INSERT INTO [tb_mensagempadrao]
(id_mensagempadrao, id_tipoenvio, st_mensagempadrao, st_default, id_textocategoria)
VALUES (47, 3, 'FACULDADE UNYLEYA ENVIA: BOLETO A-C-O-R-D-O', NULL, NULL);
SET IDENTITY_INSERT tb_mensagempadrao OFF;
INSERT INTO [tb_textovariaveis]
(id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel, bl_exibicao)
VALUES (6, 'id_venda', '#tipo_de_produto#', 'listaProdutosVendaSimples', 2, 1);
SET IDENTITY_INSERT tb_permissao ON
INSERT INTO tb_permissao (id_permissao, st_nomepermissao, st_descricao, bl_ativo) VALUES (
  51,
  'Editar Data Vencimento na tela de Negociação',
  'Permite editar a data de vencimento do boleto na tela de negociação.',
  1
);
SET IDENTITY_INSERT tb_permissao OFF

INSERT INTO tb_permissaofuncionalidade (id_funcionalidade, id_permissao) VALUES (
  627,
  51
);
ALTER TABLE tb_transacaofinanceira
  ADD id_mensagem INT NULL;
CREATE INDEX idx_tb_transacaofinanceira_id_mensagem
  ON tb_transacaofinanceira (id_mensagem);
CREATE INDEX idx_tb_transacaofinanceira_id_transacaoexterna
  ON tb_transacaofinanceira (id_transacaoexterna);
CREATE INDEX idx_tb_lancamento_id_transacaoexterna
  ON tb_lancamento (id_transacaoexterna);
CREATE INDEX idx_tb_lancamento_id_sistemacobranca
  ON tb_lancamento (id_sistemacobranca);

-- deletando a
-- [
--   {
--     "id_textovariaveis": 436,
--     "id_textocategoria": 6,
--     "st_camposubstituir": "id_venda",
--     "st_textovariaveis": "#boleto_pdf#",
--     "st_mascara": "gerarPdfBoletos",
--     "id_tipotextovariavel": 2
--   }
-- ]
DELETE tb_textovariaveis
WHERE id_textovariaveis = 436
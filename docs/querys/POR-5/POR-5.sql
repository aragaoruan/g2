/****** Object:  View [dbo].[vw_matricula]    Script Date: 07/11/2016 15:09:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[vw_matricula]
AS
 SELECT
	mt.id_usuario,
	us.st_nomecompleto,
	us.st_cpf,
	ps.st_urlavatar,
	ps.dt_nascimento,
	ps.st_nomepai,
	ps.st_nomemae,
	ps.st_email,
	CAST(ps.nu_ddd AS VARCHAR) + '-' + CAST(ps.nu_telefone AS VARCHAR) AS st_telefone,
	CAST(ps.nu_dddalternativo AS VARCHAR) + '-'
	+ CAST(ps.nu_telefonealternativo AS VARCHAR) AS st_telefonealternativo,
	mt.id_matricula,
	pp.st_projetopedagogico,
	mt.id_situacao,
	st.st_situacao,
	mt.id_evolucao,
	ev.st_evolucao,
	mt.dt_concluinte,
	mt.id_entidadematriz,
	etmz.st_nomeentidade AS st_entidadematriz,
	mt.id_entidadematricula,
	etm.st_nomeentidade AS st_entidadematricula,
	etm.st_urlportal as st_urlportalmatricula,
	mt.id_entidadeatendimento,
	eta.st_nomeentidade AS st_entidadeatendimento,
	mt.bl_ativo,
	cm.id_contrato,
	pp.id_projetopedagogico,
	mt.dt_termino,
	mt.dt_cadastro,
	mt.dt_inicio,
	tm.id_turma,
	tm.st_turma,
	tm.st_codigo,
	tm.dt_inicio AS dt_inicioturma,
	mt.bl_institucional,
	tm.dt_fim AS dt_terminoturma,
	mt.id_situacaoagendamento,
	mt.id_vendaproduto,
	mt.dt_termino AS dt_terminomatricula,
	CAST(RIGHT(master.dbo.fn_varbintohexstr(HASHBYTES('MD5',
	(CAST(us.id_usuario AS VARCHAR(10)) COLLATE Latin1_General_CI_AI
	+ CAST((CASE
		WHEN mt.id_matricula IS NULL THEN 0
		ELSE mt.id_matricula
	END) AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
	+ CAST(5 AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
	+ CAST((0) AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
	+ us.st_login
	+ us.st_senha
	+ CONVERT(VARCHAR(10), GETDATE(), 103)))),
	32) AS CHAR(32)) COLLATE Latin1_General_CI_AI AS st_urlacesso,
	mt.bl_documentacao,
	vprod.id_venda,
	eta.st_urlportal,
	mt.st_codcertificacao,
	ps.st_identificacao,
	CONVERT(VARCHAR, mtc.dt_cadastro, 103) AS st_dtcadastrocertificacao,
	CONVERT(VARCHAR, mtc.dt_enviocertificado, 103) AS st_dtenviocertificado,
	CONVERT(VARCHAR, mtc.dt_retornocertificadora, 103) AS st_dtretornocertificadora,
	CONVERT(VARCHAR, mtc.dt_envioaluno, 103) AS st_dtenvioaluno,
	st_codigoacompanhamento,
	mtc.id_indicecertificado,
	mt.id_evolucaocertificacao,
	ap.id_areaconhecimento,
	pp.bl_disciplinacomplementar,
	mt.id_matriculavinculada,
	cr.id_contratoregra,
	trc.id_tiporegracontrato,
	etm.st_siglaentidade,
	mt.bl_academico,
	mtc.dt_cadastro AS dt_cadastrocertificacao,
	mtc.dt_enviocertificado,
	mtc.dt_retornocertificadora,
	mtc.dt_envioaluno,
	vup.st_upload AS st_uploadcontrato,
	canc.id_cancelamento,
	mt.id_aproveitamento,
  vprod.id_produto,
  pp.st_descricao,
  pp.st_imagem
FROM tb_matricula mt
JOIN tb_projetopedagogico AS pp
	ON mt.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_areaprojetopedagogico AS ap
	ON ap.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_entidade AS etmz
	ON etmz.id_entidade = mt.id_entidadematriz
JOIN tb_entidade AS etm
	ON etm.id_entidade = mt.id_entidadematricula
JOIN tb_entidade AS eta
	ON eta.id_entidade = mt.id_entidadeatendimento
JOIN tb_situacao AS st
	ON st.id_situacao = mt.id_situacao
JOIN tb_evolucao AS ev
	ON ev.id_evolucao = mt.id_evolucao
JOIN tb_usuario AS us
	ON us.id_usuario = mt.id_usuario
LEFT JOIN tb_contratomatricula AS cm
	ON cm.id_matricula = mt.id_matricula
	AND cm.bl_ativo = 1
LEFT JOIN vw_pessoa AS ps
	ON mt.id_usuario = ps.id_usuario
	AND mt.id_entidadeatendimento = ps.id_entidade
LEFT JOIN dbo.tb_contrato AS ct
	ON ct.id_contrato = cm.id_contrato
	AND ct.bl_ativo = 1
LEFT JOIN tb_turma AS tm
	ON tm.id_turma = mt.id_turma
LEFT JOIN tb_matriculacertificacao mtc
	ON mtc.id_matricula = mt.id_matricula
	AND mtc.bl_ativo = 1
LEFT JOIN dbo.tb_contratoregra AS cr
	ON cr.id_contratoregra = ct.id_contratoregra
LEFT JOIN dbo.tb_tiporegracontrato AS trc
	ON trc.id_tiporegracontrato = cr.id_tiporegracontrato
LEFT JOIN dbo.tb_venda AS v
	ON ct.id_venda = v.id_venda
LEFT JOIN dbo.tb_upload AS vup
	ON vup.id_upload = v.id_uploadcontrato
LEFT JOIN dbo.tb_cancelamento AS canc
	ON canc.id_cancelamento = mt.id_cancelamento
OUTER APPLY (SELECT TOP 1 tb_vendaproduto.* FROM dbo.tb_vendaproduto
	JOIN tb_produtoprojetopedagogico AS ppd ON ppd.id_produto = tb_vendaproduto.id_produto
	JOIN tb_venda AS vend ON vend.id_venda = tb_vendaproduto.id_venda AND vend.id_evolucao = 10
  WHERE tb_vendaproduto.id_matricula = mt.id_matricula
  AND tb_vendaproduto.bl_ativo = 1
  ORDER BY tb_vendaproduto.id_vendaproduto DESC) AS vprod
WHERE mt.bl_ativo = 1
GO

/****** Object:  View [dbo].[vw_ocorrencia]    Script Date: 10/11/2016 14:21:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[vw_ocorrencia]
AS
SELECT DISTINCT
  oc.id_ocorrencia,
  uso.st_cpf AS st_cpf,
  uso.st_nomecompleto AS st_nomeinteressado,
  tc1.st_email,
  uso.st_senha, uso.st_login,
  (CAST(tc3.nu_ddd AS VARCHAR) + '-' + CAST(tc3.nu_telefone AS VARCHAR)) AS st_telefone,
  oc.id_matricula,
  em.st_urlportal,
  oc.id_evolucao,
  oc.id_situacao,
  oc.id_usuariointeressado,
  oc.id_categoriaocorrencia,
  oc.id_assuntoco,
  oc.id_saladeaula,
  oc.st_titulo,
  oc.dt_cadastro,
  oc.st_ocorrencia,
  oc.id_entidade,
  dbo.tb_situacao.st_situacao,
  dbo.tb_evolucao.st_evolucao,
  dbo.tb_assuntoco.st_assuntoco,
  dbo.tb_assuntoco.id_tipoocorrencia,
  dbo.tb_categoriaocorrencia.st_categoriaocorrencia,
  (CAST(CONVERT(VARCHAR, oc.dt_ultimotramite, 103) + ' as ' + CONVERT(VARCHAR, oc.dt_ultimotramite, 108) AS VARCHAR(MAX))) AS st_ultimotramite,
  oc.st_tramite,
  oc.dt_ultimotramite AS dt_ultimotramite,
    (case WHEN oc.dt_ultimotramite is null then oc.dt_cadastro else oc.dt_ultimotramite end) AS dt_ultimaacao,
    oresp.id_usuario AS id_usuarioresponsavel,
  st_nomeresponsavel =
                      CASE
WHEN usr.id_usuario IS NULL THEN 'Năo distribuído'
ELSE usr.st_nomecompleto
                      END,
  GETDATE() - 1 AS dt_atendimento,
  asp.st_assuntoco AS st_assuntocopai
  ,pp.st_projetopedagogico
  ,sa.st_saladeaula
  , mt.id_evolucao AS id_evolucaomatricula
  , oc.id_ocorrenciaoriginal,
  oc.st_codissue
FROM dbo.tb_ocorrencia AS oc
JOIN tb_usuario AS uso
  ON uso.id_usuario = oc.id_usuariointeressado
INNER JOIN dbo.tb_situacao
  ON dbo.tb_situacao.id_situacao = oc.id_situacao
INNER JOIN dbo.tb_evolucao
  ON dbo.tb_evolucao.id_evolucao = oc.id_evolucao
INNER JOIN dbo.tb_categoriaocorrencia
  ON dbo.tb_categoriaocorrencia.id_categoriaocorrencia = oc.id_categoriaocorrencia
INNER JOIN dbo.tb_assuntoco
  ON oc.id_assuntoco = dbo.tb_assuntoco.id_assuntoco
LEFT JOIN dbo.tb_assuntoco AS asp
  ON asp.id_assuntoco = dbo.tb_assuntoco.id_assuntocopai
LEFT JOIN dbo.tb_ocorrenciaresponsavel AS oresp
  ON oresp.id_ocorrencia = oc.id_ocorrencia
  AND oresp.bl_ativo = 1
LEFT JOIN dbo.tb_usuario AS usr
  ON usr.id_usuario = oresp.id_usuario
LEFT JOIN tb_contatosemailpessoaperfil tc
  ON uso.id_usuario = tc.id_usuario
  AND tc.bl_padrao = 1
  AND tc.bl_ativo = 1
  AND oc.id_entidade = tc.id_entidade
LEFT JOIN tb_contatosemail tc1
  ON tc.id_email = tc1.id_email
LEFT JOIN tb_contatostelefonepessoa tc2
  ON uso.id_usuario = tc2.id_usuario
  AND tc2.bl_padrao = 1
  AND oc.id_entidade = tc2.id_entidade
LEFT JOIN tb_contatostelefone tc3
  ON tc2.id_telefone = tc3.id_telefone
--alteraçőes para impressăo
LEFT JOIN dbo.tb_matricula AS mt
  ON mt.id_matricula = oc.id_matricula

LEFT JOIN dbo.tb_entidade as em
  ON em.id_entidade = mt.id_entidadematricula

LEFT JOIN dbo.tb_projetopedagogico AS pp
  ON pp.id_projetopedagogico = mt.id_projetopedagogico
LEFT JOIN dbo.tb_saladeaula AS sa
  ON sa.id_saladeaula = oc.id_saladeaula
GO





/****** Object:  View [dbo].[vw_ocorrenciaspessoa]    Script Date: 10/11/2016 14:20:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







ALTER VIEW [dbo].[vw_ocorrenciaspessoa]
AS
SELECT
  oc.st_nomeinteressado,
  oc.st_email AS st_emailinteressado,
  oc.st_telefone AS st_telefoneinteressado,
  oc.st_projetopedagogico,
  NULL AS st_nomecompleto,
  np.id_nucleoco,
  oc.id_assuntoco,
  ac.id_assuntocopai,
  np.id_usuario,
  np.id_funcao,
  np.bl_prioritario,
  nc.id_tipoocorrencia,
  nc.id_entidade,
  oc.id_situacao,
  st_nucleoco,
  nc.bl_ativo,
  st_funcao,
  oc.id_ocorrencia,
  oc.id_matricula,
  oc.st_urlportal,
  oc.id_evolucaomatricula AS id_evolucaomatricula,
  oc.id_evolucao,
  id_usuariointeressado,
  oc.id_categoriaocorrencia,
  oc.id_saladeaula,
  st_titulo,
  st_ocorrencia,
  oc.st_situacao,
  oc.st_evolucao,
  oc.dt_cadastro AS dt_cadastroocorrencia,
  (CAST(CONVERT(VARCHAR, oc.dt_cadastro, 103) + ' às '
  + CONVERT(VARCHAR, oc.dt_cadastro, 108) AS VARCHAR(MAX))) AS st_cadastroocorrencia,
  oc.st_assuntoco,
  oc.st_assuntocopai,
  oc.id_usuarioresponsavel AS id_atendente,
  oc.st_nomeresponsavel AS st_atendente,
  st_categoriaocorrencia,
  st_ultimotramite,
  dt_ultimotramite,
  oc.st_cpf,
  nu_horastraso =
                 CASE
                   WHEN (dbo.fn_horasdiasuteis(dt_ultimotramite,
                     GETDATE())
                     - nc.nu_horasmeta) > 0 THEN (dbo.fn_horasdiasuteis(dt_ultimotramite,
                     GETDATE())
                     - nc.nu_horasmeta)
                   ELSE 0
                 END,
  nu_porcentagemmeta = 0,
  st_cor =
          CASE
            WHEN oc.id_evolucao = 39 THEN nc.st_cordevolvida
            WHEN oc.id_evolucao = 32 THEN nc.st_corinteracao
            WHEN ((dbo.fn_horasdiasuteis(dt_ultimotramite,
              GETDATE())
              - nc.nu_horasmeta) >= 0) THEN nc.st_coratrasada
            ELSE nc.st_cordefault
          END,
  st_status =
             CASE
               WHEN oc.id_evolucao = 39 THEN 'Devolvida para Ditribuição'
               WHEN oc.id_evolucao = 32 THEN 'Interesado Interagiu'
               WHEN ((dbo.fn_horasdiasuteis(dt_ultimotramite,
                 GETDATE())
                 - nc.nu_horasmeta) >= 0) THEN 'Atrasada'
               ELSE ''
             END,
  nu_ordem =
            CASE
              WHEN oc.id_evolucao = 39 THEN 1
              WHEN oc.id_evolucao = 37 THEN 2
              WHEN oc.id_evolucao = 28 THEN 3
              ELSE 4
            END,
  ac.bl_cancelamento,
  ac.bl_trancamento,
  oc.dt_atendimento,
  DATEDIFF(DAY, CAST(oc.dt_cadastro AS DATE),
  CAST(GETDATE() AS DATE)) AS nu_diasaberto,
  bl_resgate =
              CASE
                WHEN tc.id_entidade IS NOT NULL THEN 1
                ELSE 0
              END,
  st_urlacesso =
                CASE
                  WHEN oc.id_matricula IS NOT NULL THEN dbo.fn_urlacesso(oc.id_usuariointeressado,oc.id_matricula,oc.st_login, oc.st_senha, 0)
                  ELSE ''
                END,
  sa.st_saladeaula
FROM dbo.tb_nucleopessoaco AS np
JOIN dbo.tb_nucleoco AS nc
  ON nc.id_nucleoco = np.id_nucleoco
JOIN dbo.tb_funcao AS fc
  ON fc.id_funcao = np.id_funcao
JOIN dbo.tb_assuntoco AS ac
  ON (ac.id_assuntoco = np.id_assuntoco)
JOIN dbo.vw_ocorrencia AS oc
  ON oc.id_assuntoco = ac.id_assuntoco
  AND oc.id_entidade = nc.id_entidade
LEFT JOIN dbo.tb_configuracaoentidade AS tc
  ON tc.id_entidade = oc.id_entidade
  AND oc.id_assuntoco = tc.id_assuntoresgate
LEFT JOIN dbo.tb_saladeaula AS sa
  ON sa.id_saladeaula = oc.id_saladeaula
UNION
SELECT
  oc.st_nomeinteressado,
  oc.st_email AS st_emailinteressado,
  oc.st_telefone AS st_telefoneinteressado,
  oc.st_projetopedagogico,
  NULL AS st_nomecompleto,
  np.id_nucleoco,
  oc.id_assuntoco,
  ac.id_assuntocopai,
  np.id_usuario,
  np.id_funcao,
  np.bl_prioritario,
  nc.id_tipoocorrencia,
  nc.id_entidade,
  oc.id_situacao,
  st_nucleoco,
  nc.bl_ativo,
  st_funcao,
  oc.id_ocorrencia,
  oc.id_matricula,
  oc.st_urlportal,
  oc.id_evolucaomatricula,
  oc.id_evolucao,
  id_usuariointeressado,
  oc.id_categoriaocorrencia,
  oc.id_saladeaula,
  st_titulo,
  st_ocorrencia,
  oc.st_situacao,
  oc.st_evolucao,
  oc.dt_cadastro AS dt_cadastroocorrencia,
  (CAST(CONVERT(VARCHAR, oc.dt_cadastro, 103) + ' às '
  + CONVERT(VARCHAR, oc.dt_cadastro, 108) AS VARCHAR(MAX))) AS st_cadastroocorrencia,
  oc.st_assuntoco,
  oc.st_assuntocopai,
  oc.id_usuarioresponsavel AS id_atendente,
  oc.st_nomeresponsavel AS st_atendente,
  st_categoriaocorrencia,
  st_ultimotramite,
  dt_ultimotramite,
  oc.st_cpf,
  nu_horastraso =
                 CASE
                   WHEN (dbo.fn_horasdiasuteis(dt_ultimotramite,
                     GETDATE())
                     - nc.nu_horasmeta) > 0 THEN (dbo.fn_horasdiasuteis(dt_ultimotramite,
                     GETDATE())
                     - nc.nu_horasmeta)
                   ELSE 0
                 END,
  nu_porcentagemmeta = 0,
  st_cor =
          CASE
            WHEN oc.id_evolucao = 39 THEN nc.st_cordevolvida
            WHEN oc.id_evolucao = 32 THEN nc.st_corinteracao
            WHEN ((dbo.fn_horasdiasuteis(dt_ultimotramite,
              GETDATE())
              - nc.nu_horasmeta) >= 0) THEN nc.st_coratrasada
            ELSE nc.st_cordefault
          END,
  st_status =
             CASE
               WHEN oc.id_evolucao = 39 THEN 'Devolvida para Ditribuição'
               WHEN oc.id_evolucao = 32 THEN 'Interesado Interagiu'
               WHEN ((dbo.fn_horasdiasuteis(dt_ultimotramite,
                 GETDATE())
                 - nc.nu_horasmeta) >= 0) THEN 'Atrasada'
               ELSE ''
             END,
  nu_ordem =
            CASE
              WHEN oc.id_evolucao = 39 THEN 1
              WHEN oc.id_evolucao = 37 THEN 2
              WHEN oc.id_evolucao = 28 THEN 3
              ELSE 4
            END,
  ac.bl_cancelamento,
  ac.bl_trancamento,
  oc.dt_atendimento,
  DATEDIFF(DAY, CAST(oc.dt_cadastro AS DATE),
  CAST(GETDATE() AS DATE)) AS nu_diasaberto,
  bl_resgate =
              CASE
                WHEN tc.id_entidade IS NOT NULL THEN 1
                ELSE 0
              END,
  st_urlacesso =
                CASE
                  WHEN oc.id_matricula IS NOT NULL THEN dbo.fn_urlacesso(oc.id_usuariointeressado,oc.id_matricula,oc.st_login, oc.st_senha, 0)
                  ELSE ''
                END,
  sa.st_saladeaula
FROM dbo.tb_nucleopessoaco AS np
JOIN dbo.tb_nucleoco AS nc
  ON nc.id_nucleoco = np.id_nucleoco
JOIN dbo.tb_funcao AS fc
  ON fc.id_funcao = np.id_funcao
--JOIN dbo.tb_usuario AS us
--  ON us.id_usuario = np.id_usuario
JOIN dbo.tb_assuntoco AS ac
  ON (np.id_assuntoco = ac.id_assuntocopai)
JOIN dbo.vw_ocorrencia AS oc
  ON oc.id_assuntoco = ac.id_assuntoco
  AND oc.id_entidade = nc.id_entidade
LEFT JOIN dbo.tb_configuracaoentidade AS tc
  ON tc.id_entidade = oc.id_entidade
  AND oc.id_assuntoco = tc.id_assuntoresgate
LEFT JOIN dbo.tb_saladeaula AS sa
  ON sa.id_saladeaula = oc.id_saladeaula
UNION
SELECT
  oc.st_nomeinteressado,
  oc.st_email AS st_emailinteressado,
  oc.st_telefone AS st_telefoneinteressado,
  oc.st_projetopedagogico,
  NULL AS st_nomecompleto,
  np.id_nucleoco,
  oc.id_assuntoco,
  ac.id_assuntocopai,
  np.id_usuario,
  np.id_funcao,
  np.bl_prioritario,
  nc.id_tipoocorrencia,
  nc.id_entidade,
  oc.id_situacao,
  st_nucleoco,
  nc.bl_ativo,
  st_funcao,
  oc.id_ocorrencia,
  oc.id_matricula,
  oc.st_urlportal,
  oc.id_evolucaomatricula,
  oc.id_evolucao,
  id_usuariointeressado,
  oc.id_categoriaocorrencia,
  oc.id_saladeaula,
  st_titulo,
  st_ocorrencia,
  oc.st_situacao,
  oc.st_evolucao,
  oc.dt_cadastro AS dt_cadastroocorrencia,
  (CAST(CONVERT(VARCHAR, oc.dt_cadastro, 103) + ' às '
  + CONVERT(VARCHAR, oc.dt_cadastro, 108) AS VARCHAR(MAX))) AS st_cadastroocorrencia,
  oc.st_assuntoco,
  oc.st_assuntocopai,
  oc.id_usuarioresponsavel AS id_atendente,
  oc.st_nomeresponsavel AS st_atendente,
  st_categoriaocorrencia,
  st_ultimotramite,
  dt_ultimotramite,
  oc.st_cpf,
  nu_horastraso =
                 CASE
                   WHEN (dbo.fn_horasdiasuteis(dt_ultimotramite,
                     GETDATE())
                     - nc.nu_horasmeta) > 0 THEN (dbo.fn_horasdiasuteis(dt_ultimotramite,
                     GETDATE())
                     - nc.nu_horasmeta)
                   ELSE 0
                 END,
  nu_porcentagemmeta = 0,
  st_cor =
          CASE
            WHEN oc.id_evolucao = 39 THEN nc.st_cordevolvida
            WHEN oc.id_evolucao = 32 THEN nc.st_corinteracao
            WHEN ((dbo.fn_horasdiasuteis(dt_ultimotramite,
              GETDATE())
              - nc.nu_horasmeta) >= 0) THEN nc.st_coratrasada
            ELSE nc.st_cordefault
          END,
  st_status =
             CASE
               WHEN oc.id_evolucao = 39 THEN 'Devolvida para Ditribuição'
               WHEN oc.id_evolucao = 32 THEN 'Interesado Interagiu'
               WHEN ((dbo.fn_horasdiasuteis(dt_ultimotramite,
                 GETDATE())
                 - nc.nu_horasmeta) >= 0) THEN 'Atrasada'
               ELSE ''
             END,
  nu_ordem =
            CASE
              WHEN oc.id_evolucao = 39 THEN 1
              WHEN oc.id_evolucao = 37 THEN 2
              WHEN oc.id_evolucao = 28 THEN 3
              ELSE 4
            END,
  ac.bl_cancelamento,
  ac.bl_trancamento,
  oc.dt_atendimento,
  DATEDIFF(DAY, CAST(oc.dt_cadastro AS DATE),
  CAST(GETDATE() AS DATE)) AS nu_diasaberto,
  bl_resgate =
              CASE
                WHEN tc.id_entidade IS NOT NULL THEN 1
                ELSE 0
              END,
  st_urlacesso =
                CASE
                  WHEN oc.id_matricula IS NOT NULL THEN dbo.fn_urlacesso(oc.id_usuariointeressado,oc.id_matricula,oc.st_login, oc.st_senha, 0)
                  ELSE NULL
                END,
  sa.st_saladeaula
FROM dbo.tb_nucleopessoaco AS np
JOIN dbo.tb_nucleoco AS nc
  ON nc.id_nucleoco = np.id_nucleoco
JOIN dbo.tb_nucleoassuntoco AS na
  ON na.id_nucleoco = nc.id_nucleoco
  AND na.bl_ativo = 1
JOIN dbo.tb_funcao AS fc
  ON fc.id_funcao = np.id_funcao
--JOIN dbo.tb_usuario AS us
--  ON us.id_usuario = np.id_usuario
JOIN dbo.tb_assuntoco AS ac
  ON ac.id_assuntoco = na.id_assuntoco
JOIN dbo.vw_ocorrencia AS oc
  ON oc.id_assuntoco = ac.id_assuntoco
  AND (oc.id_entidade = nc.id_entidade
  )
LEFT JOIN dbo.tb_configuracaoentidade AS tc
  ON tc.id_entidade = oc.id_entidade
  AND oc.id_assuntoco = tc.id_assuntoresgate


LEFT JOIN dbo.tb_saladeaula AS sa
  ON sa.id_saladeaula = oc.id_saladeaula
WHERE np.bl_todos = 1











GO



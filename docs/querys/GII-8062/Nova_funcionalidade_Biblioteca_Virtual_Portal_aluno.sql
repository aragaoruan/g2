--DESATIVAR IDENTITY TABELA
SET IDENTITY_INSERT tb_funcionalidade ON --usar essa linha só em produção
INSERT INTO tb_funcionalidade
(id_funcionalidade, st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao, st_classeflex, st_urlicone,
 id_tipofuncionalidade, nu_ordem, st_classeflexbotao, bl_pesquisa, bl_lista, id_sistema, st_ajuda, st_target, st_urlcaminho,
 bl_visivel, bl_relatorio, st_urlcaminhoedicao, bl_delete)
VALUES
  (794, 'Biblioteca do Polo', 313, 1, 123, '/Pergamum/consulta-titulos', NULL, 2, NULL, 'redirect', 1, 1, 4, NULL,
   '_blank',
   '/Pergamum/consulta-titulos', 1, 0, NULL, 0)

--ATIVAR IDENTITY TABELA
SET IDENTITY_INSERT tb_funcionalidade OFF --usar essa linha só em produção
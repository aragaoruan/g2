SET IDENTITY_INSERT tb_situacao ON;
INSERT INTO tb_situacao VALUES
  (208, 'Em Negociação', 'tb_prevenda', 'id_situacao', 'Pré-Venda em Negociação'),
  (209, 'Distribuída', 'tb_prevenda', 'id_situacao', 'Pré-Venda distribuída à um atendente');
SET IDENTITY_INSERT tb_situacao OFF;
SET IDENTITY_INSERT tb_categoriatramite ON;
INSERT INTO tb_categoriatramite (id_categoriatramite, st_categoriatramite, st_tabela, st_campo, id_tipomanual)
VALUES (7, 'Distribuição', 'tb_prevenda', 'id_prevenda', NULL);
SET IDENTITY_INSERT tb_categoriatramite OFF;
SET IDENTITY_INSERT tb_tipotramite ON;
INSERT INTO [tb_tipotramite] (id_tipotramite, id_categoriatramite, st_tipotramite)
VALUES (32, 7, 'Interação na Pré Venda');
SET IDENTITY_INSERT tb_tipotramite OFF;
CREATE TABLE tb_tramiteprevenda (
  id_tramiteprevenda INT IDENTITY (1, 1) NOT NULL,
  id_tramite INT NOT NULL,
  id_prevenda INT NOT NULL,
)


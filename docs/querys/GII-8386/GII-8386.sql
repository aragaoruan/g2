CREATE TABLE tb_acordopagarme (
  id_acordopagarme int IDENTITY (1, 1) NOT NULL,
  id_acordo int  NOT NULL,
  id_lancamento int  NOT NULL,
  id_assinatura int NOT NULL,
  dt_cadastro datetime NOT NULL
)
CREATE NONCLUSTERED INDEX [tb_avaliacaoconjuntorelacao_id_avaliacao] ON [dbo].[tb_avaliacaoconjuntorelacao]
(
	[id_avaliacao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO


CREATE NONCLUSTERED INDEX [tb_avaliacaoconjuntoreferencia_id_saladeaula] ON [dbo].[tb_avaliacaoconjuntoreferencia]
(
	[id_saladeaula] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

/****** Object:  Index [missing_index_14990]    Script Date: 15/03/2018 13:26:58 ******/
DROP INDEX [missing_index_14990] ON [dbo].[tb_avaliacaoconjuntoreferencia]
GO
﻿SET IDENTITY_INSERT dbo.tb_funcionalidade ON;
GO

INSERT INTO dbo.tb_funcionalidade
            (id_funcionalidade,
             st_funcionalidade,
             bl_ativo,
             id_situacao,
             st_classeflex,
             st_urlicone,
             id_tipofuncionalidade,
             bl_pesquisa,
             bl_lista,
             id_sistema,
             st_urlcaminho,
             bl_visivel,
             bl_relatorio,
             bl_delete)
     VALUES (833,
             'Biblioteca do Polo',
             1,
             182,
             'app.biblioteca-polo',
             'icon-icon_livro',
             1,
             1,
             1,
             4,
             'app.biblioteca-polo',
             1,
             0,
             0);

GO

/* Alteração para a ordenação do Menu no Novo Portal */
UPDATE dbo.tb_funcionalidade SET nu_ordem = NULL WHERE id_situacao       = 182;
UPDATE dbo.tb_funcionalidade SET nu_ordem = 1    WHERE id_funcionalidade = 764;
UPDATE dbo.tb_funcionalidade SET nu_ordem = 2    WHERE id_funcionalidade = 767;
UPDATE dbo.tb_funcionalidade SET nu_ordem = 3    WHERE id_funcionalidade = 768;
UPDATE dbo.tb_funcionalidade SET nu_ordem = 4    WHERE id_funcionalidade = 770;
UPDATE dbo.tb_funcionalidade SET nu_ordem = 5    WHERE id_funcionalidade = 775;
UPDATE dbo.tb_funcionalidade SET nu_ordem = 6    WHERE id_funcionalidade = 833;
UPDATE dbo.tb_funcionalidade SET nu_ordem = 7    WHERE id_funcionalidade = 777;
UPDATE dbo.tb_funcionalidade SET nu_ordem = 8    WHERE id_funcionalidade = 806;
UPDATE dbo.tb_funcionalidade SET nu_ordem = 9    WHERE id_funcionalidade = 807;
UPDATE dbo.tb_funcionalidade SET nu_ordem = 10   WHERE id_funcionalidade = 815;
UPDATE tb_finalidadecampanha SET st_finalidadecampanha = 'E-commerce' WHERE id_finalidadecampanha = 3;

IF NOT EXISTS (SELECT st_finalidadecampanha FROM tb_finalidadecampanha WHERE st_finalidadecampanha = 'Venda e E-commerce')
BEGIN
    INSERT INTO tb_finalidadecampanha values ('Venda e E-commerce', 1)
END;
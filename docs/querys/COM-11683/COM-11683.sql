SET IDENTITY_INSERT dbo.tb_funcionalidade ON
INSERT INTO tb_funcionalidade (
    id_funcionalidade,
    st_funcionalidade,
    id_funcionalidadepai,
    bl_ativo,
    id_situacao,
    st_classeflex,
    st_urlicone,
    id_tipofuncionalidade,
    nu_ordem,
    st_classeflexbotao,
    bl_pesquisa,
    bl_lista,
    id_sistema,
    st_ajuda,
    st_target,
    st_urlcaminho,
    bl_visivel,
    bl_relatorio,
    st_urlcaminhoedicao,
    bl_delete)
VALUES (
    745,
   'Relatório Folha de Pagamento - Por Matéria',
    488,
    1,
    123,
    'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa',
    '/img/ico/flag.png',
    3,
    NULL,
    '',
    0,
    0,
    1,
    NULL,
    NULL,
    '/relatorio-calculo-pagamento-materia',
    1,
    0,
    NULL,
    0);
SET IDENTITY_INSERT dbo.tb_funcionalidade OFF

CREATE VIEW [rel].[vw_comercialgeral] as
SELECT DISTINCT
	CAST (mt.dt_cadastro AS DATE) AS dt_matricula,
	--app.id_areaconhecimento ,
	sg_uf,
	ve.id_entidade,
	ve.st_nomeentidade,
	ps.st_nomecompleto,
	ps.st_cpf,
	st_cidade,
	st_projetopedagogico,
	(
		SELECT
			COALESCE (
				ac2.st_areaconhecimento + ', ',
				''
			)
		FROM
			dbo.tb_areaconhecimento ac2
		JOIN dbo.tb_areaprojetopedagogico app2 ON ac2.id_areaconhecimento = app2.id_areaconhecimento
		WHERE
			app2.id_projetopedagogico = mt.id_projetopedagogico FOR XML PATH ('')
	) AS st_areaconhecimento,
	--st_areaconhecimento,
	tm.id_turma,
	tm.st_turma,
	tm.dt_inicio,
	dt_fim,
	usr.st_nomecompleto AS st_representante,
	usa.st_nomecompleto AS st_atendente,
	mt.id_matricula,
	ev.st_evolucao,
	st.st_situacao,
	vdl.nu_parcelas,
	vdlb.nu_valorboleto,
	vdlcc.nu_valorcartaoc,
	vdlcd.nu_valorcartaod,
	vdlcr.nu_valorcartaor,
	vdlc.nu_valorcheque,
	vdlo.nu_valoroutros,
	pd.nu_valorliquido,
	vdlct.nu_valorcarta,
	vdlbl.nu_valorbolsa,
	vd.id_venda,
	vdlv.dt_vencimento AS dt_vencimentoultima,
	pdc.st_produto AS st_combo,
	pdc.id_produto AS id_produtocombo,
	ve.st_nomeentidade AS st_nomeentidaderec,
	et.id_entidade AS id_entidadepai,
	pdt.id_produto
FROM
	tb_entidade AS et
JOIN dbo.vw_entidaderecursivaid AS ve ON ve.nu_entidadepai = et.id_entidade
OR ve.id_entidade = et.id_entidade
JOIN dbo.tb_matricula AS mt ON ve.id_entidade = mt.id_entidadematricula
AND mt.bl_ativo = 1
JOIN dbo.tb_evolucao AS ev ON ev.id_evolucao = mt.id_evolucao
JOIN dbo.tb_situacao AS st ON st.id_situacao = mt.id_situacao
JOIN dbo.tb_areaprojetopedagogico AS app ON app.id_projetopedagogico = mt.id_projetopedagogico
JOIN dbo.tb_areaconhecimento AS aa ON aa.id_areaconhecimento = app.id_areaconhecimento -- AND aa.id_tipoareaconhecimento = 1
JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
LEFT JOIN tb_turma AS tm ON tm.id_turma = mt.id_turma
JOIN dbo.vw_pessoa AS ps ON ps.id_usuario = mt.id_usuario
AND ps.id_entidade = ve.id_entidade
JOIN dbo.tb_contratomatricula AS cm ON cm.id_matricula = mt.id_matricula --AND cm.bl_ativo = 1
JOIN dbo.tb_contrato AS ct ON ct.id_contrato = cm.id_contrato
JOIN dbo.tb_venda AS vd ON vd.id_venda = ct.id_venda
LEFT JOIN dbo.tb_usuario AS usa ON usa.id_usuario = vd.id_atendente
LEFT JOIN dbo.tb_vendaenvolvido AS ver ON ver.id_venda = vd.id_venda
AND ver.id_funcao = 5
LEFT JOIN dbo.tb_usuario AS usr ON usr.id_usuario = ver.id_usuario CROSS APPLY (
	SELECT
		COUNT (id_lancamento) AS nu_parcelas
	FROM
		dbo.vw_vendalancamento
	WHERE
		id_venda = vd.id_venda
) AS vdl OUTER APPLY (
	SELECT
		SUM (nu_valor) AS nu_valorboleto
	FROM
		dbo.vw_vendalancamento
	WHERE
		id_venda = vd.id_venda
	AND id_meiopagamento = 2
) AS vdlb OUTER APPLY (
	SELECT
		SUM (nu_valor) AS nu_valorcartaoc
	FROM
		dbo.vw_vendalancamento
	WHERE
		id_venda = vd.id_venda
	AND id_meiopagamento = 1
) AS vdlcc OUTER APPLY (
	SELECT
		SUM (nu_valor) AS nu_valorcartaod
	FROM
		dbo.vw_vendalancamento
	WHERE
		id_venda = vd.id_venda
	AND id_meiopagamento = 7
) AS vdlcd OUTER APPLY (
	SELECT
		SUM (nu_valor) AS nu_valorcartaor
	FROM
		dbo.vw_vendalancamento
	WHERE
		id_venda = vd.id_venda
	AND id_meiopagamento = 11
) AS vdlcr OUTER APPLY (
	SELECT
		SUM (nu_valor) AS nu_valorcheque
	FROM
		dbo.vw_vendalancamento
	WHERE
		id_venda = vd.id_venda
	AND id_meiopagamento = 4
) AS vdlc OUTER APPLY (
	SELECT
		SUM (nu_valor) AS nu_valoroutros
	FROM
		dbo.vw_vendalancamento
	WHERE
		id_venda = vd.id_venda
	AND id_meiopagamento NOT IN (2, 1, 7, 11, 4, 13, 10)
) AS vdlo OUTER APPLY (
	SELECT
		SUM (nu_valor) AS nu_valorcarta
	FROM
		dbo.vw_vendalancamento
	WHERE
		id_venda = vd.id_venda
	AND id_meiopagamento = 13
) AS vdlct OUTER APPLY (
	SELECT
		SUM (nu_valor) AS nu_valorbolsa
	FROM
		dbo.vw_vendalancamento
	WHERE
		id_venda = vd.id_venda
	AND id_meiopagamento = 10
) AS vdlbl
JOIN dbo.tb_vendaproduto AS pd ON pd.id_venda = vd.id_venda
AND pd.id_vendaproduto = mt.id_vendaproduto
JOIN dbo.tb_produto AS pdt ON pdt.id_produto = pd.id_produto
AND pdt.id_modelovenda = 1
LEFT JOIN tb_produto AS pdc ON pdc.id_produto = pd.id_produtocombo CROSS APPLY (
	SELECT
		TOP 1 dt_vencimento
	FROM
		dbo.vw_vendalancamento
	WHERE
		id_venda = vd.id_venda
	ORDER BY
		dt_vencimento DESC
) AS vdlv
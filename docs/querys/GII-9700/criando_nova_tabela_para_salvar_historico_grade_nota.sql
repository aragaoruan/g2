CREATE TABLE dbo.tb_historicomatriculagradenota (
  id_historicogradenota INT IDENTITY (1, 1) NOT NULL PRIMARY KEY,
  id_matricula INT NOT NULL,
  id_projetopedagogico INT NOT NULL,
  id_tramitematricula INT NOT NULL,
  dt_cadastro DATETIME NOT NULL,
  st_htmlgrade VARCHAR(MAX) NOT NULL
);
ALTER TABLE dbo.tb_historicomatriculagradenota
  WITH CHECK ADD CONSTRAINT [FK_TB_HIST_GRADE_MAT]
FOREIGN KEY (id_matricula)
REFERENCES dbo.tb_matricula (id_matricula);
ALTER TABLE dbo.tb_historicomatriculagradenota
  WITH CHECK ADD CONSTRAINT [FK_TB_HIST_GRADE_PROJ_PEDAG]
FOREIGN KEY (id_projetopedagogico)
REFERENCES dbo.tb_projetopedagogico (id_projetopedagogico);
ALTER TABLE dbo.tb_historicomatriculagradenota
  WITH CHECK ADD CONSTRAINT [FK_TB_HIST_GRADE_TRAM_MAT]
FOREIGN KEY (id_tramitematricula)
REFERENCES dbo.tb_tramitematricula (id_tramitematricula);
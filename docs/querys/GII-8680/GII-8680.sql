/*
Criando coluna id_assuntoco_resgate e id_assunto_recuperacao
na tb_nucleoco como FK de tb_assunco
 */
ALTER TABLE tb_nucleoco ADD id_assuntocoresgate INT NULL
ALTER TABLE tb_nucleoco ADD id_assuntorecuperacao INT NULL
ALTER TABLE tb_nucleoco
  ADD CONSTRAINT FK_TB_NUCLE_REFERENCE_TB_ASSUNTOCO_id_assuntoco_resgate
FOREIGN KEY (id_assuntocoresgate) REFERENCES tb_assuntoco (id_assuntoco)
ALTER TABLE dbo.tb_nucleoco
  ADD CONSTRAINT FK_TB_NUCLE_REFERENCE_ASSUNTOCO_id_assunto_recuperacao
FOREIGN KEY (id_assuntorecuperacao) REFERENCES tb_assuntoco (id_assuntoco);
ALTER TABLE tb_vendaaditivo
  ADD nu_valorapagar NUMERIC(30, 2);
ALTER TABLE tb_vendaaditivo
  ADD id_meiopagamento INT;
ALTER TABLE tb_vendaaditivo
  ADD id_campanhacomercial INT;
ALTER TABLE tb_vendaaditivo
  ADD nu_desconto NUMERIC(30, 2);
ALTER TABLE tb_vendaaditivo
  ADD nu_quantidadeparcelas INT;
ALTER TABLE tb_vendaaditivo
  ADD nu_valortransferencia NUMERIC(30, 2);
ALTER TABLE tb_vendaaditivo
  ADD dt_primeiraparcela DATETIME NULL;
ALTER TABLE tb_vendaaditivo
  ADD id_atendente INT;
ALTER TABLE tb_vendaaditivo
  ADD st_observacao VARCHAR(255) NULL;

--renomeando coluna "nu_valorliquido" da tabela "tb_vendaaditivo" para "nu_valorcursoorigem"
USE G2_DEV;
GO
EXEC sp_rename 'tb_vendaaditivo.nu_valorliquido', 'nu_valorcursoorigem', 'COLUMN';
GO
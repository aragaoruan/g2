
SET IDENTITY_INSERT dbo.tb_permissao ON
INSERT INTO dbo.tb_permissao(id_permissao, st_nomepermissao, st_descricao, bl_ativo)
  VALUES(52, 'Devolver TCC', 'Permite devolver TCC para análise do Tutor', 1);  
SET IDENTITY_INSERT dbo.tb_permissao OFF

INSERT INTO dbo.tb_permissaofuncionalidade(id_funcionalidade, id_permissao)
  VALUES(450, 52), (453, 52);
  
INSERT INTO dbo.tb_perfilpermissaofuncionalidade(id_perfil, id_funcionalidade, id_permissao)
  VALUES(115, 453, 52), (115, 450, 52), (102, 453, 52), (102, 450, 52);

ALTER view [dbo].[vw_clientevenda] AS
SELECT DISTINCT
	vd.id_usuario,
	us.st_nomecompleto,
	us.st_cpf,
  pe.st_email,
	ev.id_evolucao,
	ev.st_evolucao,
	vd.id_venda,
	vd.nu_valorliquido,
	vd.bl_contrato,
	nu_valorbruto,
	vd.dt_cadastro,
	st.id_situacao,
	st.st_situacao,
	et.id_entidade,
	et.st_nomeentidade,
	ct.id_evolucao AS id_evolucaocontrato,
	ct.id_situacao AS id_situacaocontrato,
	ef.id_textosistemarecibo,
	ef.id_reciboconsolidado
FROM
	tb_venda AS vd
JOIN tb_usuario AS us ON us.id_usuario = vd.id_usuario
JOIN tb_evolucao AS ev ON ev.id_evolucao = vd.id_evolucao
JOIN tb_situacao AS st ON st.id_situacao = vd.id_situacao
JOIN tb_entidade AS et ON et.id_entidade = vd.id_entidade
LEFT JOIN vw_pessoa as pe on pe.id_usuario = us.id_usuario and pe.id_entidade = vd.id_entidade
LEFT JOIN tb_contrato AS ct ON ct.id_venda = vd.id_venda
LEFT JOIN tb_entidadefinanceiro ef ON ef.id_entidade = et.id_entidade
WHERE
	vd.bl_ativo = 1 and us.bl_ativo = 1

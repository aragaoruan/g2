/**
	INSERINDO MENSAGEM PADRAO
**/

BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_mensagempadrao on
INSERT INTO tb_mensagempadrao 
(id_mensagempadrao, id_tipoenvio, st_mensagempadrao , st_default)
VALUES (37, 3, 'Envio c�digo de rastreamento - Certificado' , 'ALTERAR - Cadastrar texto para envio de c�digo de rastreamento do certificado')
SET IDENTITY_INSERT dbo.tb_mensagempadrao off
COMMIT


SELECT * FROM tb_textocategoria
SELECT * FROM tb_textovariaveis where id_textocategoria = 4

/**
	INSERINDO a vari�vel #codigo_rasteamento_certificado
**/

BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_textovariaveis on
INSERT INTO tb_textovariaveis 
(id_textovariaveis, id_textocategoria, st_camposubstituir , st_textovariaveis , st_mascara , id_tipotextovariavel)
VALUES (378, 4, 'st_codigorasteamento' , '#codigo_rasteamento_certificado' , null, 1)
SET IDENTITY_INSERT dbo.tb_mensagempadrao off
COMMIT
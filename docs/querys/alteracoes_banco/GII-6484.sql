-- GII-6484

ALTER TABLE tb_configuracaoentidade ADD id_assuntoisencao INT NULL;

ALTER TABLE [dbo].[tb_configuracaoentidade]
WITH CHECK ADD CONSTRAINT [FK_CONFIGURACAOENTIDADE_ASSUNTOISENCAO] FOREIGN KEY([id_assuntoisencao])
REFERENCES [dbo].[tb_assuntoco] ([id_assuntoco]);

-- Atualizar
UPDATE tb_configuracaoentidade SET id_assuntoisencao = 2271 WHERE id_entidade IN (
353,
354,
400,
401,
402,
654,
655,
656,
657,
658
);
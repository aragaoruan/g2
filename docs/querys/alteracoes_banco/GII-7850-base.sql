/* SQL de inserção da funcionalidade no banco. */

SET IDENTITY_INSERT tb_funcionalidade ON
INSERT INTO tb_funcionalidade (id_funcionalidade, st_urlcaminho, id_funcionalidadepai, st_funcionalidade,
                               bl_ativo, id_situacao, id_tipofuncionalidade, bl_pesquisa, bl_lista, bl_visivel, bl_relatorio, bl_delete, id_sistema)
VALUES (
  816,
-- id_funcionalidade: Novo ID da funcionalidade. Quem roda a SQL em produção fica com a responsabilidade de adicionar um novo ID manualmente.
'/cancelamento-pos-graduacao',
-- st_urlcaminho: Geralmente, a url da funcionalidade que será exibida depois do #/. Aqui é apenas uma string com o id da funcionalidade, visto que nesse caso, estamos adicionando uma funcionalidade interna.
194, -- id_funcionalidadepai: A funcionalidade 'superior' na hierarquia do menu.
'Cancelamento Pós', -- st_funcionalidade: descrição da funcionalidade.
1, -- bl_ativo: funcionalidade ativa
123, -- id_situacao: funcionalidade em HTML
3, -- id_tipofuncionalidade
0, -- bl_pesquisa
0, -- bl_lista
1, -- bl_visivel
0, -- bl_relatorio
  0, -- bl_delete
  1); --id_sistema: 1 (G2S)
SET IDENTITY_INSERT tb_funcionalidade OFF

SET IDENTITY_INSERT tb_permissao ON
INSERT INTO tb_permissao (id_permissao, st_nomepermissao, st_descricao, bl_ativo) VALUES (
  55,
  'Cálculo Financeiro',
  'Permite o acesso ao cáuculo financeiro (Financeiro > Cadastros > Cancelamento Pós)',
  1
);
SET IDENTITY_INSERT tb_permissao OFF

INSERT INTO tb_permissaofuncionalidade (id_funcionalidade, id_permissao) VALUES (
  816,
  1
);
INSERT INTO tb_permissaofuncionalidade (id_funcionalidade, id_permissao) VALUES (
  816,
  2
);
INSERT INTO tb_permissaofuncionalidade (id_funcionalidade, id_permissao) VALUES (
  816,
  3
);
INSERT INTO tb_permissaofuncionalidade (id_funcionalidade, id_permissao) VALUES (
  816,
  55
);

--DROP Constraint da coluna evolução com a tabela, essa contraint havia sido criada 3x
ALTER TABLE [dbo].[tb_cancelamento]
  DROP CONSTRAINT [FK__tb_cancel__id_ev__6865AB71];
ALTER TABLE [dbo].[tb_cancelamento]
  DROP CONSTRAINT [FK__tb_cancel__id_ev__74CB8256];

-- Adicionando coluna para registrara data de cadastro do dado
ALTER TABLE [dbo].[tb_cancelamento]
  ADD [dt_cadastro] DATETIME2 NOT NULL DEFAULT GETDATE();
ALTER TABLE [dbo].[tb_cancelamento]
  ADD [id_usuariocadastro] INT NULL;
ALTER TABLE [dbo].[tb_cancelamento]
  ADD [id_ocorrencia] INT NULL;
ALTER TABLE [dbo].[tb_cancelamento]
  ADD CONSTRAINT [FK__tb_cancel__tb_usuario]
FOREIGN KEY ([id_usuariocadastro]) REFERENCES [dbo].[tb_usuario] ([id_usuario])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
ALTER TABLE [dbo].[tb_cancelamento]
  ADD CONSTRAINT [FK_tb_cancel_tb_ocorrencia]
FOREIGN KEY ([id_ocorrencia]) REFERENCES [dbo].[tb_ocorrencia] ([id_ocorrencia])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

-- Alteração na tb_nucleoco GII-9007
ALTER TABLE [dbo].[tb_nucleoco]
  ADD [id_assuntocancelamento] INT NULL;
GO

ALTER TABLE [dbo].[tb_nucleoco]
  ADD CONSTRAINT [FK_TB_NUCLE_REFERENCE_TB_ASSUNTO_id_assuntocancelamento]
FOREIGN KEY ([id_assuntocancelamento]) REFERENCES [dbo].[tb_assuntoco] ([id_assuntoco])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
GO

-- NOTA: executar CREATE da vw_contratomatriculausuario.sql

-- Adicionando campo id_usuário para o cancelamento;
ALTER TABLE [dbo].[tb_cancelamento]
	ADD [id_usuario] INT,
	FOREIGN KEY ([id_usuario]) REFERENCES [dbo].[tb_usuario]([id_usuario]);

-- Adicionando campo id_linhadenegocio para a tb_motivo;
ALTER TABLE [dbo].[tb_motivo]
  ADD [id_linhadenegocio] INT
  FOREIGN KEY ([id_linhadenegocio]) REFERENCES [dbo].[tb_linhadenegocio][(id_linhadenegocio)];


/* Insert com os motivos de cancelamento solicitados pelo cliente. Note que os valores devem ser alterados de acordo
com os valores contidos no banco de produção ao subir a demanda. */
INSERT INTO tb_motivo (st_motivo, bl_ativo, id_entidadecadastro, id_usuariocadastro, id_situacao)
VALUES
('Não se adaptou à modalidade EAD ou teve dificuldades com a utilização da internet, horário de funcionamento', 1, 14, 1, 153),
('Atendimento precário ou dificuldades de comunicação com a instituição', 1, 14, 1, 153),
('Atraso no recebimento de login e senha', 1, 14, 1, 153),
('Material didático inadequado ou desatualizado', 1, 14, 1, 153),
('Atraso no recebimento do material impresso', 1, 14, 1, 153),
('Problemas com tutor, orientador ou coordenador', 1, 14, 1, 153),
('Plataforma com inconsistências ou incompleta', 1, 14, 1, 153),
('Curso não foi oferecido', 1, 14, 1, 153),
('Motivos pessoais: financeiro, saúde, falta de tempo, etc', 1, 14, 1, 153),
('Não respondeu', 1, 14, 1, 153),
('Outros', 1, 14, 1, 153);

-- Atualizando campo para linha de negócio 3 (pós graduação) para os motivos adicionados acima.
UPDATE tb_motivo SET id_linhadenegocio = 3
  WHERE id_entidadecadastro = 14;

--Criando um campo a mais na tb_vendaaditivo, esse campo e usando na vw_matriculacancelamento. Para diferenciar os
-- aditivos,  de forma que nao inclui os aditivos do cancelamento nos calculos
ALTER TABLE tb_vendaaditivo ADD bl_cancelamento BIT DEFAULT 0 NULL

-- Permitindo nu_diascancelamento e dt_solicitacao NULL.
ALTER TABLE tb_cancelamento ALTER COLUMN nu_diascancelamento INT NULL;
ALTER TABLE tb_cancelamento ALTER COLUMN dt_solicitacao datetime2 NULL;

ALTER TABLE tb_lancamento ADD bl_cancelamento BIT DEFAULT 0 NULL;
ALTER TABLE tb_cancelamento ADD bl_concluir BIT DEFAULT 0 NULL;
ALTER TABLE tb_cancelamento ADD bl_finalizar BIT DEFAULT 0 NULL;


ALTER TABLE tb_logcancelamento ADD bl_ressarcimento BIT DEFAULT 0 NULL;
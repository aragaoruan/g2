insert into tb_funcionalidade values (644,
'Pesquisar Aluno',	313,	1,	3,	'/matricula/pesquisar-aluno',	'/imagens/icones/semfoto.png',	1,	NULL,	'ajax',	0,	0,	4,	NULL,	'_blank',	'/matricula/pesquisar-aluno',	1,	0,	NULL)


-----------

Create VIEW dbo.vw_alunosvinculoperfil
AS
select distinct 
mt.id_matricula,
mt.st_nomecompleto,
mt.st_email,
mt.id_projetopedagogico,
mt.st_projetopedagogico,
vw.id_usuario as id_usuarioperfil,
per.id_perfil,
per.id_perfilpedagogico,
vw.id_entidade,
mt.dt_inicio,
mt.st_evolucao,
mt.st_situacao,
mt.st_turma
from dbo.tb_alocacao al
JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula
JOIN dbo.tb_matriculadisciplina AS md ON md.id_matriculadisciplina = al.id_matriculadisciplina
JOIN dbo.vw_matricula AS mt ON mt.id_matricula = md.id_matricula AND mt.bl_ativo = 1
join tb_usuarioperfilentidadereferencia as vw on vw.id_saladeaula=sa.id_saladeaula and vw.bl_ativo=1 and vw.id_entidade=mt.id_entidadematricula
join tb_perfil as per on per.id_perfil = vw.id_perfil and per.bl_ativo=1
where  per.id_perfilpedagogico not in (5,6)
GO

UPDATE tb_textovariaveis SET st_mascara = 'retornaDataPorExtenso' WHERE id_textovariaveis = 210;

----

SET IDENTITY_INSERT tb_textovariaveis ON;
INSERT INTO tb_textovariaveis
(id_textovariaveis, id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
VALUES
(394, 3, 'st_solicitacaocancelamento', '#data_cancelamento#', 'retornaDataPorExtenso', 1),
(395, 3, 'dt_iniciomatriculaorigem', '#data_inicio_matricula_origem#', 'retornaDataPorExtenso', 1),
(396, 3, 'id_matricula', '#professor_titular_TCC#', 'retornaProfessorTitularTCC', 2),
(397, 3, 'id_matricula', '#professor_certificacao_TCC#', 'retornaProfessorCertificacaoTCC', 2);
SET IDENTITY_INSERT tb_textovariaveis OFF;
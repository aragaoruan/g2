begin tran
SET IDENTITY_INSERT dbo.tb_funcionalidade on
INSERT INTO tb_funcionalidade (
id_funcionalidade,
st_funcionalidade,
id_funcionalidadepai,
bl_ativo,
id_situacao,
st_classeflex,
st_urlicone,
id_tipofuncionalidade,
nu_ordem,
st_classeflexbotao,
bl_pesquisa,
bl_lista,
id_sistema,
st_ajuda,
st_target,
st_urlcaminho,
bl_visivel,
bl_relatorio,
st_urlcaminhoedicao
)
VALUES (
724,
'Perguntas Frequentes',
301,
1,
123,
'br.com.ead1.gestor2.view.conteudo.entidade.cadastrar.PerguntasFrequentes',
'/img/ico/money--arrow.png',
3,
NULL,
'PerguntasFrequentes',
1,
0,
1,
NULL,
NULL,
'/perguntas-frequentes',
1,
0,
NULL
);
GO
SET IDENTITY_INSERT dbo.tb_funcionalidade off
COMMIT

--------- [dbo].[tb_funcionalidade] ---------
begin tran
update tb_funcionalidade set bl_delete = 1 where id_funcionalidade = 724
commit
--------- FIM [dbo].[tb_funcionalidade] ---------

--------- [dbo].[tb_categoriapergunta] ---------
CREATE TABLE [dbo].[tb_esquemapergunta] (
[id_esquemapergunta] int NOT NULL IDENTITY(1,1) ,
[st_esquemapergunta] varchar(100) NOT NULL ,
[dt_cadastro] datetime2 NULL ,
[bl_ativo] bit NULL ,
PRIMARY KEY ([id_esquemapergunta])
)

GO

ALTER TABLE [dbo].[tb_esquemapergunta] ADD DEFAULT getDate() FOR [dt_cadastro]
GO

ALTER TABLE [dbo].[tb_esquemapergunta] ADD DEFAULT 1 FOR [bl_ativo]
GO

ALTER TABLE [dbo].[tb_esquemapergunta]
ADD [id_entidadecadastro] int NOT NULL
GO

ALTER TABLE [dbo].[tb_esquemapergunta] ADD CONSTRAINT [FK_tb_esquemapergunta_tb_entidade] FOREIGN KEY ([id_entidadecadastro]) REFERENCES [dbo].[tb_entidade] ([id_entidade]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO


--------- FIM [dbo].[tb_esquemapergunta] ---------

--------- [dbo].[tb_categoriapergunta] ---------
CREATE TABLE [dbo].[tb_categoriapergunta] (
[id_categoriapergunta] int NOT NULL IDENTITY(1,1) ,
[st_categoriapergunta] varchar(100) NOT NULL ,
[bl_ativo] bit NULL ,
[dt_cadastro] datetime2 NULL ,
PRIMARY KEY ([id_categoriapergunta])
)

GO

ALTER TABLE [dbo].[tb_categoriapergunta]
ADD [id_esquemapergunta] int NOT NULL
GO

ALTER TABLE [dbo].[tb_categoriapergunta] ADD CONSTRAINT [FK_tb_esquemapergunta_tb_categoriapergunta] FOREIGN KEY ([id_esquemapergunta]) REFERENCES [dbo].[tb_esquemapergunta] ([id_esquemapergunta]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

ALTER TABLE [dbo].[tb_categoriapergunta] ADD DEFAULT getDate() FOR [dt_cadastro]
GO

ALTER TABLE [dbo].[tb_categoriapergunta] ADD DEFAULT 1 FOR [bl_ativo]
GO

ALTER TABLE [dbo].[tb_categoriapergunta]
ADD [id_entidadecadastro] int NOT NULL
GO

ALTER TABLE [dbo].[tb_categoriapergunta] ADD CONSTRAINT [FK_tb_categoriapergunta_tb_entidade] FOREIGN KEY ([id_entidadecadastro]) REFERENCES [dbo].[tb_entidade] ([id_entidade]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO



--------- FIM [dbo].[tb_categoriapergunta] ---------



--------- [dbo].[tb_pergunta] ---------
CREATE TABLE [dbo].[tb_pergunta] (
[id_pergunta] int NOT NULL IDENTITY(1,1),
[st_pergunta] text NOT NULL ,
[st_resposta] text NOT NULL ,
[bl_ativo] bit NULL ,
[dt_cadastro] datetime2 NULL ,
[id_categoriapergunta] int NULL ,
PRIMARY KEY ([id_pergunta]),
CONSTRAINT [FK_tb_pergunta_tb_categoriapergunta] FOREIGN KEY ([id_categoriapergunta]) REFERENCES [dbo].[tb_categoriapergunta] ([id_categoriapergunta]) ON DELETE NO ACTION ON UPDATE NO ACTION
)

GO


ALTER TABLE [dbo].[tb_pergunta] ADD DEFAULT getDate() FOR [dt_cadastro]
GO

ALTER TABLE [dbo].[tb_pergunta] ADD DEFAULT 1 FOR [bl_ativo]
GO

ALTER TABLE [dbo].[tb_pergunta]
ADD [id_entidadecadastro] int NOT NULL
GO

ALTER TABLE [dbo].[tb_pergunta] ADD CONSTRAINT [FK_tb_entidade_tb_pergunta] FOREIGN KEY ([id_entidadecadastro]) REFERENCES [dbo].[tb_entidade] ([id_entidade]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO


--------- FIM [dbo].[tb_pergunta] ---------



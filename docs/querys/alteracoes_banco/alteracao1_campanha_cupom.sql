if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('tb_campanhacategoris') and o.name = 'FK_TB_CAMPA_REFERENCE_TB_CATEG')
alter table tb_campanhacategoris
   drop constraint FK_TB_CAMPA_REFERENCE_TB_CATEG
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('tb_campanhacategoris') and o.name = 'FK_TB_CAMPA_REFERENCE_TB_CAMPA')
alter table tb_campanhacategoris
   drop constraint FK_TB_CAMPA_REFERENCE_TB_CAMPA
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('tb_campanhacategoris')
            and   name  = 'Reference_729_FK'
            and   indid > 0
            and   indid < 255)
   drop index tb_campanhacategoris.Reference_729_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('tb_campanhacategoris')
            and   name  = 'Reference_728_FK'
            and   indid > 0
            and   indid < 255)
   drop index tb_campanhacategoris.Reference_728_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('tb_campanhacategoris')
            and   type = 'U')
   drop table tb_campanhacategoris
go

/*==============================================================*/
/* Table: tb_campanhacategoris                                  */
/*==============================================================*/
create table tb_campanhacategoris (
   id_campanhacategoris int                  identity,
   id_categoria         int                  null,
   id_campanhacomercial int                  null,
   constraint PK_TB_CAMPANHACATEGORIS primary key (id_campanhacategoris)
)
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('tb_campanhacategoris')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'id_campanhacomercial')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'tb_campanhacategoris', 'column', 'id_campanhacomercial'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Chave prim�ria da tabela.',
   'user', @CurrentUser, 'table', 'tb_campanhacategoris', 'column', 'id_campanhacomercial'
go

/*==============================================================*/
/* Index: Reference_728_FK                                      */
/*==============================================================*/
create index Reference_728_FK on tb_campanhacategoris (
id_categoria ASC
)
go

/*==============================================================*/
/* Index: Reference_729_FK                                      */
/*==============================================================*/
create index Reference_729_FK on tb_campanhacategoris (
id_campanhacomercial ASC
)
go

alter table tb_campanhacategoris
   add constraint FK_TB_CAMPA_REFERENCE_TB_CATEG foreign key (id_categoria)
      references dbo.tb_categoria (id_categoria)
go

alter table tb_campanhacategoris
   add constraint FK_TB_CAMPA_REFERENCE_TB_CAMPA foreign key (id_campanhacomercial)
      references dbo.tb_campanhacomercial (id_campanhacomercial)
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('tb_cupom') and o.name = 'FK_TB_CUPOM_REFERENCE_TB_USUAR')
alter table tb_cupom
   drop constraint FK_TB_CUPOM_REFERENCE_TB_USUAR
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('tb_cupom') and o.name = 'FK_TB_CUPOM_REFERENCE_TB_TIPOD')
alter table tb_cupom
   drop constraint FK_TB_CUPOM_REFERENCE_TB_TIPOD
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('tb_cupom') and o.name = 'FK_TB_CUPOM_REFERENCE_TB_CAMPA')
alter table tb_cupom
   drop constraint FK_TB_CUPOM_REFERENCE_TB_CAMPA
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('tb_cupom')
            and   name  = 'Reference_732_FK'
            and   indid > 0
            and   indid < 255)
   drop index tb_cupom.Reference_732_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('tb_cupom')
            and   name  = 'Reference_731_FK'
            and   indid > 0
            and   indid < 255)
   drop index tb_cupom.Reference_731_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('tb_cupom')
            and   name  = 'Reference_730_FK'
            and   indid > 0
            and   indid < 255)
   drop index tb_cupom.Reference_730_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('tb_cupom')
            and   type = 'U')
   drop table tb_cupom
go

/*==============================================================*/
/* Table: tb_cupom                                              */
/*==============================================================*/
create table tb_cupom (
   id_cupom             int                  identity,
   st_codigocupom       varchar(22)          not null,
   st_prefixo           varchar(11)          not null,
   st_complemento       varchar(11)          not null,
   nu_desconto          numeric(30,2)        not null,
   dt_inicio            datetime2            not null,
   dt_fim               datetime2            not null,
   bl_ativacao          bit                  not null,
   bl_unico             bit                  not null,
   bl_ativo             bit                  not null,
   dt_cadastro          datetime2            not null default getdate(),
   id_usuariocadastro   int                  not null,
   id_tipodesconto      int                  not null,
   id_campanhacomercial int                  not null,
   constraint PK_TB_CUPOM primary key (id_cupom)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('tb_cupom') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'tb_cupom' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   'Tabela com os cupons de campanhas comerciais
   ', 
   'user', @CurrentUser, 'table', 'tb_cupom'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('tb_cupom')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'id_cupom')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'tb_cupom', 'column', 'id_cupom'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Chave primaria',
   'user', @CurrentUser, 'table', 'tb_cupom', 'column', 'id_cupom'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('tb_cupom')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'st_codigocupom')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'tb_cupom', 'column', 'st_codigocupom'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Codigo do cupom que deve ser informado pelo cliente na compra',
   'user', @CurrentUser, 'table', 'tb_cupom', 'column', 'st_codigocupom'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('tb_cupom')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'st_prefixo')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'tb_cupom', 'column', 'st_prefixo'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Prefixo do codigo cupom',
   'user', @CurrentUser, 'table', 'tb_cupom', 'column', 'st_prefixo'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('tb_cupom')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'st_complemento')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'tb_cupom', 'column', 'st_complemento'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Complemento do codigo do cupom',
   'user', @CurrentUser, 'table', 'tb_cupom', 'column', 'st_complemento'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('tb_cupom')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'nu_desconto')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'tb_cupom', 'column', 'nu_desconto'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Valor do desconto do cupom',
   'user', @CurrentUser, 'table', 'tb_cupom', 'column', 'nu_desconto'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('tb_cupom')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'dt_inicio')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'tb_cupom', 'column', 'dt_inicio'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Data de inicia��o do uso do cupom',
   'user', @CurrentUser, 'table', 'tb_cupom', 'column', 'dt_inicio'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('tb_cupom')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'dt_fim')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'tb_cupom', 'column', 'dt_fim'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Data de finaliza��o do uso do cupom',
   'user', @CurrentUser, 'table', 'tb_cupom', 'column', 'dt_fim'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('tb_cupom')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'bl_ativacao')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'tb_cupom', 'column', 'bl_ativacao'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Define se o cupom est� ativo',
   'user', @CurrentUser, 'table', 'tb_cupom', 'column', 'bl_ativacao'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('tb_cupom')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'bl_unico')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'tb_cupom', 'column', 'bl_unico'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Define se o cupom pode ser utilizado mais de uma vez',
   'user', @CurrentUser, 'table', 'tb_cupom', 'column', 'bl_unico'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('tb_cupom')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'id_tipodesconto')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'tb_cupom', 'column', 'id_tipodesconto'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Chave prim�ria da tabela.',
   'user', @CurrentUser, 'table', 'tb_cupom', 'column', 'id_tipodesconto'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('tb_cupom')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'id_campanhacomercial')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'tb_cupom', 'column', 'id_campanhacomercial'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Chave prim�ria da tabela.',
   'user', @CurrentUser, 'table', 'tb_cupom', 'column', 'id_campanhacomercial'
go

/*==============================================================*/
/* Index: Reference_730_FK                                      */
/*==============================================================*/
create index Reference_730_FK on tb_cupom (
id_usuariocadastro ASC
)
go

/*==============================================================*/
/* Index: Reference_731_FK                                      */
/*==============================================================*/
create index Reference_731_FK on tb_cupom (
id_tipodesconto ASC
)
go

/*==============================================================*/
/* Index: Reference_732_FK                                      */
/*==============================================================*/
create index Reference_732_FK on tb_cupom (
id_campanhacomercial ASC
)
go

alter table tb_cupom
   add constraint FK_TB_CUPOM_REFERENCE_TB_USUAR foreign key (id_usuariocadastro)
      references dbo.tb_usuario (id_usuario)
go

alter table tb_cupom
   add constraint FK_TB_CUPOM_REFERENCE_TB_TIPOD foreign key (id_tipodesconto)
      references dbo.tb_tipodesconto (id_tipodesconto)
go

alter table tb_cupom
   add constraint FK_TB_CUPOM_REFERENCE_TB_CAMPA foreign key (id_campanhacomercial)
      references dbo.tb_campanhacomercial (id_campanhacomercial)
go

/**
	INSERINDO MENU DA FUNCIONALIDADE
**/

BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_funcionalidade on
INSERT INTO [dbo].[tb_funcionalidade]
([id_funcionalidade]
,[st_funcionalidade]
,[id_funcionalidadepai]
,[bl_ativo]
,[id_situacao]
,[st_classeflex]
,[st_urlicone]
,[id_tipofuncionalidade]
,[st_classeflexbotao]
,[bl_pesquisa]
,[bl_lista]
,[id_sistema]
,[bl_relatorio]
,[st_urlcaminho]
,[bl_visivel])
VALUES

(642,'Ocorrências de alunos',428,1,123,'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa','/img/ico/flag.png',3,'ocorrencia-aluno',0,0, 1, 0, '/ocorrencia-aluno', 1)

GO
SET IDENTITY_INSERT dbo.tb_funcionalidade off

COMMIT


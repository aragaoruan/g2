-- GII-9773

BEGIN TRAN;

DECLARE @ENTPROJETO INT = 2;
DECLARE @ENTPRODUTO INT = 14;
DECLARE @DISCNOVA INT = 12325;

DECLARE @USERG2 INT = 9999999;

------------------
------------------
-- Clonar Projetos
------------------
------------------
IF OBJECT_ID('tempdb.dbo.#TempPPClone', 'U') IS NOT NULL DROP TABLE #TempPPClone;
SELECT pp.*
INTO #TempPPClone
FROM
  tb_projetopedagogico AS pp
  JOIN tb_projetoentidade AS pe
    ON pe.id_projetopedagogico = pp.id_projetopedagogico
       AND pe.id_entidade = @ENTPRODUTO
       AND pe.bl_ativo = 1
       AND pe.id_situacao = 51
WHERE
  pp.id_entidadecadastro = @ENTPROJETO
  AND pp.bl_ativo = 1
  AND pp.id_situacao = 7
  AND pp.id_projetopedagogico IN (1593)

-- Atualizar valores
UPDATE #TempPPClone
SET
  id_usuariocadastro = @USERG2,
  id_projetopedagogicoorigem = id_projetopedagogico,
  dt_cadastro = GETDATE(),
  st_projetopedagogico = CONCAT(st_projetopedagogico, ' 2018');

-- Remover o campo id_projetopedagogico para que, ao clonar, gerar um novo ID
ALTER TABLE #TempPPClone DROP COLUMN id_projetopedagogico;

-- Preparar FOREACH
DECLARE @IDPPREF INT = 0;
DECLARE @IDPPNEW INT;

WHILE(1 = 1) BEGIN
  SELECT @IDPPREF = MIN(id_projetopedagogicoorigem)
  FROM #TempPPClone WHERE id_projetopedagogicoorigem > @IDPPREF
  IF @IDPPREF IS NULL BREAK

  -- Clonar o Projeto
  INSERT INTO tb_projetopedagogico SELECT * FROM #TempPPClone;

  -- Pegar o ID do Projeto clone
  SET @IDPPNEW = SCOPE_IDENTITY();
  IF @IDPPNEW IS NULL CONTINUE;

  --SELECT * FROM tb_projetopedagogico WHERE id_projetopedagogico = @IDPPNEW;

  -------------------
  -------------------
  -- Clonar Entidades
  -------------------
  -------------------
  IF OBJECT_ID('tempdb.dbo.#TEMP_PP_ENT', 'U') IS NOT NULL DROP TABLE #TEMP_PP_ENT;
  SELECT *
  INTO #TEMP_PP_ENT
  FROM tb_projetoentidade
  WHERE
    id_projetopedagogico = @IDPPREF
    AND bl_ativo = 1
    AND id_situacao = 51;

  ALTER TABLE #TEMP_PP_ENT DROP COLUMN id_projetoentidade;
  UPDATE #TEMP_PP_ENT SET
    id_projetopedagogico = @IDPPNEW,
    id_usuariocadastro = @USERG2,
    dt_cadastro = GETDATE();

  INSERT INTO tb_projetoentidade SELECT * FROM #TEMP_PP_ENT;

  --SELECT * FROM tb_projetoentidade WHERE id_projetopedagogico = @IDPPNEW;

  ----------------------------
  ----------------------------
  -- Clonar Série/Nível/Ensino
  ----------------------------
  ----------------------------
  IF OBJECT_ID('tempdb.dbo.#TEMP_PP_SER', 'U') IS NOT NULL DROP TABLE #TEMP_PP_SER;
  SELECT *
  INTO #TEMP_PP_SER
  FROM tb_projetopedagogicoserienivelensino
  WHERE id_projetopedagogico = @IDPPREF;

  UPDATE #TEMP_PP_SER SET id_projetopedagogico = @IDPPNEW;

  INSERT INTO tb_projetopedagogicoserienivelensino SELECT * FROM #TEMP_PP_SER;

  --SELECT * FROM tb_projetopedagogicoserienivelensino WHERE id_projetopedagogico = @IDPPNEW

  -------------------------------
  -------------------------------
  -- Clonar Áreas de Conhecimento
  -------------------------------
  -------------------------------
  IF OBJECT_ID('tempdb.dbo.#TEMP_PP_AREA', 'U') IS NOT NULL DROP TABLE #TEMP_PP_AREA;
  SELECT *
  INTO #TEMP_PP_AREA
  FROM tb_areaprojetopedagogico
  WHERE id_projetopedagogico = @IDPPREF;

  UPDATE #TEMP_PP_AREA SET id_projetopedagogico = @IDPPNEW;

  INSERT INTO tb_areaprojetopedagogico SELECT * FROM #TEMP_PP_AREA;

  --SELECT * FROM tb_areaprojetopedagogico WHERE id_projetopedagogico = @IDPPNEW

  -----------------------
  -----------------------
  -- Clonar Coordenadores
  -----------------------
  -----------------------
  IF OBJECT_ID('tempdb.dbo.#TEMP_PP_COOR', 'U') IS NOT NULL DROP TABLE #TEMP_PP_COOR;
  SELECT *
  INTO #TEMP_PP_COOR
  FROM tb_usuarioperfilentidadereferencia
  WHERE
    id_projetopedagogico = @IDPPREF
    AND id_entidade IN (@ENTPRODUTO, @ENTPROJETO)
    AND bl_ativo = 1;

  ALTER TABLE #TEMP_PP_COOR DROP COLUMN id_perfilreferencia;
  UPDATE #TEMP_PP_COOR SET id_projetopedagogico = @IDPPNEW;

  DECLARE @TEMPREF INT = 0;

  WHILE (1 = 1) BEGIN
    SELECT TOP(1) @TEMPREF = id_usuario FROM #TEMP_PP_COOR;
    IF @@ROWCOUNT = 0 BREAK;

    INSERT INTO tb_usuarioperfilentidadereferencia SELECT TOP(1) * FROM #TEMP_PP_COOR;

    DELETE TOP(1) FROM #TEMP_PP_COOR;
  END

  --SELECT * FROM tb_usuarioperfilentidadereferencia WHERE id_projetopedagogico = @IDPPNEW

  -----------------
  -----------------
  -- Clonar Módulos
  -----------------
  -----------------
  IF OBJECT_ID('tempdb.dbo.#TEMP_MOD_REF', 'U') IS NOT NULL DROP TABLE #TEMP_MOD_REF;
  SELECT *
  INTO #TEMP_MOD_REF
  FROM tb_modulo
  WHERE
    id_projetopedagogico = @IDPPREF
    AND bl_ativo = 1
    AND id_situacao = 33;

  -- Preparar FOREACH
  DECLARE @IDMODREF INT = 0;
  DECLARE @IDMODNEW INT;
  DECLARE @FIRST INT = 1;

  WHILE (1 = 1) BEGIN
    SELECT TOP(1) @IDMODREF = id_modulo FROM #TEMP_MOD_REF;
    IF @@ROWCOUNT = 0 BREAK;

    IF OBJECT_ID('tempdb.dbo.#TEMP_MOD_NEW', 'U') IS NOT NULL DROP TABLE #TEMP_MOD_NEW;
    SELECT TOP(1) * INTO #TEMP_MOD_NEW FROM #TEMP_MOD_REF;
    ALTER TABLE #TEMP_MOD_NEW DROP COLUMN id_modulo;
    UPDATE #TEMP_MOD_NEW SET id_projetopedagogico = @IDPPNEW;

    INSERT INTO tb_modulo SELECT TOP(1) * FROM #TEMP_MOD_NEW;

    DELETE TOP(1) FROM #TEMP_MOD_REF;

    SET @IDMODNEW = SCOPE_IDENTITY();
    IF @IDMODNEW IS NULL CONTINUE;

    -- Clonar Disciplians do Módulo
    IF OBJECT_ID('tempdb.dbo.#TEMP_MOD_DISC', 'U') IS NOT NULL DROP TABLE #TEMP_MOD_DISC;
    SELECT md.*
    INTO #TEMP_MOD_DISC
    FROM
      tb_modulodisciplina AS md
      JOIN tb_disciplina AS dc
        ON dc.id_disciplina = md.id_disciplina
           AND dc.id_tipodisciplina = 1
           AND dc.st_disciplina NOT LIKE '%Metodologia da Pesquisa e da Produ__o Cient_fica%'
    WHERE
      id_modulo = @IDMODREF
      AND md.bl_ativo = 1;

    ALTER TABLE #TEMP_MOD_DISC DROP COLUMN id_modulodisciplina;
    UPDATE #TEMP_MOD_DISC SET id_modulo = @IDMODNEW;

    IF @FIRST = 1 BEGIN
      SET @FIRST = 0;
      -- Criar nova disciplina MANUAL da demanda
      INSERT INTO #TEMP_MOD_DISC (
        id_modulo,
        id_disciplina,
        id_serie,
        bl_ativo,
        bl_obrigatoria,
        nu_ponderacaocalculada,
        nu_ponderacaoaplicada,
        nu_cargahoraria
      ) VALUES (
        @IDMODNEW,
        @DISCNOVA,
        11,
        1,
        1,
        0.00,
        0.00,
        (SELECT nu_cargahoraria FROM tb_disciplina WHERE id_disciplina = @DISCNOVA)
      )
    END;

    INSERT INTO tb_modulodisciplina SELECT * FROM #TEMP_MOD_DISC;
  END;

  --SELECT * FROM tb_modulo WHERE id_projetopedagogico = @IDPPNEW;
  --SELECT * FROM tb_modulodisciplina WHERE id_modulo IN (SELECT id_modulo FROM tb_modulo WHERE id_projetopedagogico = @IDPPNEW);
  --SELECT * FROM tb_modulodisciplina WHERE id_disciplina = @DISCNOVA;

  -----------------------
  -----------------------
  -- Clonar Parâmetro TCC
  -----------------------
  -----------------------
  IF OBJECT_ID('tempdb.dbo.#TEMP_PP_TCC', 'U') IS NOT NULL DROP TABLE #TEMP_PP_TCC;
  SELECT *
  INTO #TEMP_PP_TCC
  FROM tb_parametrotcc
  WHERE
    id_projetopedagogico = @IDPPREF
    AND bl_ativo = 1;

  ALTER TABLE #TEMP_PP_TCC DROP COLUMN id_parametrotcc;
  UPDATE #TEMP_PP_TCC SET id_projetopedagogico = @IDPPNEW;

  INSERT INTO tb_parametrotcc SELECT * FROM #TEMP_PP_TCC;

  --SELECT * FROM tb_parametrotcc WHERE id_projetopedagogico = @IDPPNEW;

  ------------------
  ------------------
  -- Clonar Produtos
  ------------------
  ------------------
  IF OBJECT_ID('tempdb.dbo.#TEMP_PROD_REF', 'U') IS NOT NULL DROP TABLE #TEMP_PROD_REF;
  SELECT pr.*
  INTO #TEMP_PROD_REF
  FROM
    tb_produto AS pr
    JOIN tb_produtoprojetopedagogico AS ppp ON ppp.id_produto = pr.id_produto AND ppp.id_projetopedagogico = @IDPPREF
  WHERE
    pr.id_entidade = @ENTPRODUTO
    AND bl_ativo = 1
    AND id_situacao = 45;

  -- Preparar FOREACH
  DECLARE @IDPRODREF INT = 0;
  DECLARE @IDPRODNEW INT;

  WHILE (1 = 1) BEGIN
    SELECT TOP(1) @IDPRODREF = id_produto FROM #TEMP_PROD_REF;
    IF @@ROWCOUNT = 0 BREAK;

    IF OBJECT_ID('tempdb.dbo.#TEMP_PROD_NEW', 'U') IS NOT NULL DROP TABLE #TEMP_PROD_NEW;
    SELECT TOP(1) * INTO #TEMP_PROD_NEW FROM #TEMP_PROD_REF;
    ALTER TABLE #TEMP_PROD_NEW DROP COLUMN id_produto;
    UPDATE #TEMP_PROD_NEW SET st_produto = CONCAT(st_produto, ' 2018');

    INSERT INTO tb_produto SELECT TOP(1) * FROM #TEMP_PROD_NEW;

    DELETE TOP(1) FROM #TEMP_PROD_REF;

    SET @IDPRODNEW = SCOPE_IDENTITY();
    IF @IDPRODNEW IS NULL CONTINUE;

    -- Clonar Projetos do Produto
    IF OBJECT_ID('tempdb.dbo.#TEMP_PROD_PP', 'U') IS NOT NULL DROP TABLE #TEMP_PROD_PP;
    SELECT *
    INTO #TEMP_PROD_PP
    FROM tb_produtoprojetopedagogico
    WHERE id_produto = @IDPRODREF;

    ALTER TABLE #TEMP_PROD_PP DROP COLUMN id_produtoprojetopedagogico;
    UPDATE #TEMP_PROD_PP SET
      id_projetopedagogico = @IDPPNEW,
      id_produto = @IDPRODNEW;

    INSERT INTO tb_produtoprojetopedagogico SELECT * FROM #TEMP_PROD_PP;

    -- Clonar Valores do Produto
    IF OBJECT_ID('tempdb.dbo.#TEMP_PROD_VAL', 'U') IS NOT NULL DROP TABLE #TEMP_PROD_VAL;
    SELECT *
    INTO #TEMP_PROD_VAL
    FROM tb_produtovalor
    WHERE id_produto = @IDPRODREF;

    ALTER TABLE #TEMP_PROD_VAL DROP COLUMN id_produtovalor;
    UPDATE #TEMP_PROD_VAL SET
      id_produto = @IDPRODNEW,
      id_usuariocadastro = @USERG2,
      dt_cadastro = GETDATE();

    INSERT INTO tb_produtovalor SELECT * FROM #TEMP_PROD_VAL;

  -- Clonar Categorias do Produto
    IF OBJECT_ID('tempdb.dbo.#TEMP_PROD_CAT', 'U') IS NOT NULL DROP TABLE #TEMP_PROD_CAT;
    SELECT *
    INTO #TEMP_PROD_CAT
    FROM tb_categoriaproduto
    WHERE id_produto = @IDPRODREF

    UPDATE #TEMP_PROD_CAT SET
      id_produto = @IDPRODNEW,
      id_usuariocadastro = @USERG2,
      dt_cadastro = GETDATE();

    INSERT INTO tb_categoriaproduto SELECT * FROM #TEMP_PROD_CAT;

    -- Clonar Áreas de Conhecimento do Produto
    INSERT INTO tb_produtoarea
    SELECT
      @IDPRODNEW AS id_produto,
      id_areaconhecimento
    FROM tb_produtoarea
    WHERE id_produto = @IDPRODREF

    -- Clonar Entidade do Produto
    IF OBJECT_ID('tempdb.dbo.#TEMP_PROD_ENT', 'U') IS NOT NULL DROP TABLE #TEMP_PROD_ENT;
    SELECT *
    INTO #TEMP_PROD_ENT
    FROM tb_produtoentidade
    WHERE id_produto = @IDPRODREF;

    ALTER TABLE #TEMP_PROD_ENT DROP COLUMN id_produtoentidade;
    UPDATE #TEMP_PROD_ENT SET id_produto = @IDPRODNEW;

    INSERT INTO tb_produtoentidade SELECT * FROM #TEMP_PROD_ENT;
  END;

  --SELECT * FROM tb_produto WHERE id_produto IN (SELECT id_produto FROM tb_produtoprojetopedagogico WHERE id_projetopedagogico = @IDPPNEW);

END;

IF OBJECT_ID('tempdb.dbo.#TempPPClone', 'U') IS NOT NULL DROP TABLE #TempPPClone;
IF OBJECT_ID('tempdb.dbo.#TEMP_PP_ENT', 'U') IS NOT NULL DROP TABLE #TEMP_PP_ENT;
IF OBJECT_ID('tempdb.dbo.#TEMP_PP_SER', 'U') IS NOT NULL DROP TABLE #TEMP_PP_SER;
IF OBJECT_ID('tempdb.dbo.#TEMP_PP_AREA', 'U') IS NOT NULL DROP TABLE #TEMP_PP_AREA;
IF OBJECT_ID('tempdb.dbo.#TEMP_PP_COOR', 'U') IS NOT NULL DROP TABLE #TEMP_PP_COOR;
IF OBJECT_ID('tempdb.dbo.#TEMP_MOD_REF', 'U') IS NOT NULL DROP TABLE #TEMP_MOD_REF;
IF OBJECT_ID('tempdb.dbo.#TEMP_MOD_NEW', 'U') IS NOT NULL DROP TABLE #TEMP_MOD_NEW;
IF OBJECT_ID('tempdb.dbo.#TEMP_MOD_DISC', 'U') IS NOT NULL DROP TABLE #TEMP_MOD_DISC;
IF OBJECT_ID('tempdb.dbo.#TEMP_PP_TCC', 'U') IS NOT NULL DROP TABLE #TEMP_PP_TCC;
IF OBJECT_ID('tempdb.dbo.#TEMP_PROD_REF', 'U') IS NOT NULL DROP TABLE #TEMP_PROD_REF;
IF OBJECT_ID('tempdb.dbo.#TEMP_PROD_NEW', 'U') IS NOT NULL DROP TABLE #TEMP_PROD_NEW;
IF OBJECT_ID('tempdb.dbo.#TEMP_PROD_PP', 'U') IS NOT NULL DROP TABLE #TEMP_PROD_PP;
IF OBJECT_ID('tempdb.dbo.#TEMP_PROD_VAL', 'U') IS NOT NULL DROP TABLE #TEMP_PROD_VAL;
IF OBJECT_ID('tempdb.dbo.#TEMP_PROD_CAT', 'U') IS NOT NULL DROP TABLE #TEMP_PROD_CAT;
IF OBJECT_ID('tempdb.dbo.#TEMP_PROD_ENT', 'U') IS NOT NULL DROP TABLE #TEMP_PROD_ENT;

ROLLBACK;
/* Identificando disciplinas que são estágio. */
SELECT * FROM tb_disciplina WHERE st_disciplina LIKE 'Estágio%';

/* Atualizando bl_coeficienterendimento de disciplinas de estágio.
UPDATE tb_disciplina SET bl_coeficienterendimento = 0 WHERE st_disciplina LIKE 'Estágio%';
*/

/* Identificando disciplinas que são Projetos e Práticas. */
SELECT * FROM tb_disciplina WHERE st_disciplina LIKE '%projetos e práticas%';

/* Atualizando bl_coeficienterendimento de disciplinas de Projetos e Práticas.
UPDATE tb_disciplina SET bl_coeficienterendimento = 0 WHERE st_disciplina LIKE '%projetos e práticas%';
*/
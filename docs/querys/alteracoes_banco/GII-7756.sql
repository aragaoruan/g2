-- Criar tabela de tipo de atividade complementar
CREATE TABLE [dbo].[tb_tipoatividadecomplementar] (
  [id_tipoatividadecomplementar] INT NOT NULL IDENTITY (1, 1),
  [st_tipoatividadecomplementar] VARCHAR(50) NOT NULL,
  [id_usuariocadastro] INT NOT NULL,
  [id_entidadecadastro] INT NOT NULL,
  [dt_cadastro] DATETIME2 NULL DEFAULT getdate(),
  [bl_ativo] BIT NULL DEFAULT 1,
  PRIMARY KEY ([id_tipoatividadecomplementar]),
  CONSTRAINT [fk_tipoatividade_usuario] FOREIGN KEY ([id_usuariocadastro]) REFERENCES [dbo].[tb_usuario] ([id_usuario])
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT [fk_tipoatividade_entidade] FOREIGN KEY ([id_entidadecadastro]) REFERENCES [dbo].[tb_entidade] ([id_entidade])
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

--INSERT DA FUNCIONALIDADE--
SET IDENTITY_INSERT tb_funcionalidade ON;
INSERT INTO tb_funcionalidade
(id_funcionalidade, st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao,
 st_classeflex, st_urlicone, id_tipofuncionalidade, nu_ordem, st_classeflexbotao, bl_pesquisa,
 bl_lista, id_sistema, st_ajuda, st_target, st_urlcaminho, bl_visivel, bl_relatorio,
 st_urlcaminhoedicao, bl_delete)
VALUES
  (797, 'Tipo de Atividade Complementar', 244, 1, 123,
        'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa', NULL, 3, 0, 'TipoAtividadeComplementar', 0,
   0, 1, NULL, NULL, 'atividade-complementar/tipo', 1, 0, NULL, 0);
SET IDENTITY_INSERT tb_funcionalidade OFF;

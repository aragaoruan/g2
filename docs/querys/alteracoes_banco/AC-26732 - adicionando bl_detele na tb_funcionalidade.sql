/**
Adicionando campo bl_delete na tb_funcionalidade
**/
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.tb_funcionalidade ADD
	bl_delete bit NOT NULL CONSTRAINT DF_tb_funcionalidade_bl_delete DEFAULT 1
GO
ALTER TABLE dbo.tb_funcionalidade SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


/**
alterando a funcionalidade de envio de mensagem no portal para n�o ter o bot�o pesquisa
**/
update tb_funcionalidade set bl_delete = 0 where id_funcionalidade = 664
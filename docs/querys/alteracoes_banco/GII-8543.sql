-- Trocando a funcionalidade de relatório para secretaria
UPDATE [dbo].[tb_funcionalidade]
SET st_funcionalidade = 'Recuperar Aluno',
  id_funcionalidadepai = 244, -- Id da funcionalidade Avaliações, novo pai dessa funcionalidade
  st_urlcaminho = '/recuperar-aluno'
WHERE id_funcionalidade = 778  -- Id da funcionalidade  'Alunos a Recuperar'
GO;


--Modificando a tabela tb_matriculadisciplina
ALTER TABLE dbo.tb_matriculadisciplina ADD id_situacaotiposervico INT NULL
ALTER TABLE dbo.tb_matriculadisciplina ADD CONSTRAINT tb_matriculadisciplina___fk_tb_situacao
FOREIGN KEY (id_situacaotiposervico) REFERENCES tb_situacao (id_situacao);

-- Inserindo os tipos de servico ta tb_situacao
SET IDENTITY_INSERT dbo.tb_situacao ON
INSERT INTO tb_situacao ("id_situacao" ,"st_situacao", "st_tabela", "st_campo", "st_descricaosituacao")
VALUES (210, 'Resgate', 'tb_matriculadisciplina', 'id_situacaotiposervico', 'Alunos que precisam fazer um resgate'),
  (211, 'Recuperação', 'tb_matriculadisciplina', 'id_situacaotiposervico', 'Alunos que precisam fazer uma recuperação');
SET IDENTITY_INSERT dbo.tb_situacao OFF;


-- Atualizar os registros com o tipo serviço para RECUPERAÇÃO
UPDATE tb_matriculadisciplina SET id_situacaotiposervico = 211
WHERE id_matriculadisciplina IN  (SELECT DISTINCT md.id_matriculadisciplina
                                  FROM dbo.vw_matricula mat
																	JOIN vw_entidadeesquemaconfiguracao AS esquema ON esquema.id_entidade = mat.id_entidadematricula AND esquema.id_itemconfiguracao = 22
                                  JOIN dbo.vw_pessoa AS usu ON usu.id_usuario = mat.id_usuario
                                  JOIN dbo.tb_areaconhecimento AS ac ON ac.id_areaconhecimento = mat.id_areaconhecimento
                                  JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mat.id_projetopedagogico
                                  JOIN dbo.tb_turma AS tur ON tur.id_turma = mat.id_turma
                                  JOIN dbo.tb_matriculadisciplina AS md ON md.id_matricula = mat.id_matricula AND md.id_evolucao != 12 AND md.id_situacaotiposervico IS NULL
                                  JOIN tb_alocacao AS alc ON alc.id_matriculadisciplina = md.id_matriculadisciplina AND alc.bl_ativo = 1
                                  JOIN tb_encerramentoalocacao AS ea ON ea.id_alocacao = alc.id_alocacao
                                  JOIN tb_saladeaula sda ON sda.id_saladeaula = alc.id_saladeaula
                                  JOIN dbo.tb_disciplina AS dis ON dis.id_disciplina = md.id_disciplina
                                  JOIN dbo.vw_matriculanota AS mn ON mn.id_matriculadisciplina = md.id_matriculadisciplina
                                  WHERE (md.nu_aprovafinal * pp.nu_notamaxima / 100) < 70 AND mn.st_notafinal IS NOT NULL AND mn.st_notafinal < 30 AND mn.st_notaead > 10 );


-- Atualizar os registros com o tipo serviço para RESGATE
UPDATE tb_matriculadisciplina SET id_situacaotiposervico = 210
WHERE id_matriculadisciplina IN (SELECT DISTINCT md.id_matriculadisciplina
                                  FROM dbo.vw_matricula mat
																	JOIN vw_entidadeesquemaconfiguracao AS esquema ON esquema.id_entidade = mat.id_entidadematricula AND esquema.id_itemconfiguracao = 22
                                  JOIN dbo.vw_pessoa AS usu ON usu.id_usuario = mat.id_usuario
                                  JOIN dbo.tb_areaconhecimento AS ac ON ac.id_areaconhecimento = mat.id_areaconhecimento
                                  JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mat.id_projetopedagogico
                                  JOIN dbo.tb_turma AS tur ON tur.id_turma = mat.id_turma
                                  JOIN dbo.tb_matriculadisciplina AS md ON md.id_matricula = mat.id_matricula AND md.id_evolucao != 12
                                  JOIN tb_alocacao AS alc ON alc.id_matriculadisciplina = md.id_matriculadisciplina AND alc.bl_ativo = 1
                                  JOIN tb_encerramentoalocacao AS ea ON ea.id_alocacao = alc.id_alocacao
                                  JOIN tb_saladeaula sda ON sda.id_saladeaula = alc.id_saladeaula
                                  JOIN dbo.tb_disciplina AS dis ON dis.id_disciplina = md.id_disciplina
                                  JOIN dbo.vw_matriculanota AS mn ON mn.id_matriculadisciplina = md.id_matriculadisciplina
                                WHERE (md.nu_aprovafinal * pp.nu_notamaxima / 100) < 70 AND md.id_situacaotiposervico IS NULL);
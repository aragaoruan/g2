-- POR-260
--
SET IDENTITY_INSERT tb_funcionalidade ON;
INSERT INTO tb_funcionalidade
(id_funcionalidade, st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao, st_classeflex, st_urlicone,
 id_tipofuncionalidade, nu_ordem, st_classeflexbotao, bl_pesquisa, bl_lista, id_sistema, st_ajuda, st_target,
 st_urlcaminho, bl_visivel, bl_relatorio, st_urlcaminhoedicao, bl_delete)
VALUES
  (815, 'Atividade Complementar', NULL, 1, 182, 'app.atividade-complementar', 'icon-icon_disc', 1, 1, NULL, 1, 1, 4,
   NULL, NULL, 'app.atividade-complementar', 1, 0, NULL, 0);
SET IDENTITY_INSERT tb_funcionalidade OFF;
--
-- Funcionalidade
--INSERT tb_entidadefuncionalidade
--(id_entidade, id_funcionalidade, bl_ativo, id_situacao, bl_obrigatorio, bl_visivel, nu_ordem)
--VALUES (14, 815, 1, 5, 0, 1, 1)
--
-- Perfil de Aluno
--INSERT INTO tb_perfilfuncionalidade (id_perfil, id_funcionalidade)
--VALUES (75, 815);
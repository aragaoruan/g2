-- GII-7961
--
--
SET IDENTITY_INSERT tb_textovariaveis ON;
INSERT INTO tb_textovariaveis
(id_textovariaveis, id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel, bl_exibicao)
VALUES (481, 3, 'dt_concluintealuno', '#data_concluinte_texto#', 'retornaDataPorExtenso', 1, 1);
SET IDENTITY_INSERT tb_textovariaveis OFF;
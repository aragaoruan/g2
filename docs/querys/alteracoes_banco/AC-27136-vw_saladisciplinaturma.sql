alter VIEW [dbo].[vw_saladisciplinaturma]
AS
    SELECT DISTINCT
            aps.id_saladeaula ,
            sa.st_saladeaula ,
            sa.nu_maxalunos ,
            aps.id_projetopedagogico ,
            pp.id_trilha ,
            mt.id_matricula ,
            ds.id_disciplina ,
            md.id_matriculadisciplina ,
            mt.id_usuario ,
            al.id_alocacao ,
            se.id_entidade ,
            dc.id_tipodisciplina,
            ( SELECT    COUNT(alc.id_alocacao)
              FROM      tb_alocacao alc

              WHERE     alc.id_saladeaula = sa.id_saladeaula AND alc.bl_ativo = 1
            ) AS nu_alocados
    FROM    tb_matricula AS mt
            JOIN dbo.tb_turma AS tm ON tm.id_turma = mt.id_turma
            JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
            JOIN tb_areaprojetosala AS aps ON aps.id_projetopedagogico = mt.id_projetopedagogico
            JOIN tb_saladeaulaentidade AS se ON
                                                se.id_entidade = mt.id_entidadeatendimento AND aps.id_saladeaula = se.id_saladeaula
			JOIN tb_saladeaula AS sa ON sa.id_saladeaula = aps.id_saladeaula
                                        AND sa.dt_inicioinscricao <= tm.dt_inicio
                                        AND ( sa.dt_fiminscricao >= tm.dt_inicio
                                              OR sa.dt_fiminscricao IS NULL
                                            )
                                        AND sa.bl_ativa = 1
                                        AND sa.id_situacao = 8
                                        AND sa.id_categoriasala = 1
                                        AND se.id_saladeaula = sa.id_saladeaula
                                        AND sa.bl_vincularturma = 0
            JOIN tb_disciplinasaladeaula AS ds ON ds.id_saladeaula = sa.id_saladeaula
            JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
                                                 AND ds.id_disciplina = md.id_disciplina
                                                 AND md.id_situacao != 65 -- Ignorar alocacoes com APROVEITAMENTO DE DISCIPLINA
            LEFT JOIN tb_alocacao al ON al.id_matriculadisciplina = md.id_matriculadisciplina
                                        AND al.bl_ativo = 1
            LEFT JOIN tb_disciplina as dc ON dc.id_disciplina = ds.id_disciplina
    UNION ALL
    SELECT DISTINCT
            aps.id_saladeaula ,
            sa.st_saladeaula ,
            sa.nu_maxalunos ,
            aps.id_projetopedagogico ,
            pp.id_trilha ,
            mt.id_matricula ,
            ds.id_disciplina ,
            md.id_matriculadisciplina ,
            mt.id_usuario ,
            al.id_alocacao ,
            ve.id_entidade ,
            dc.id_tipodisciplina,
            ( SELECT    COUNT(alc.id_alocacao)
              FROM      tb_alocacao alc

              WHERE     alc.id_saladeaula = sa.id_saladeaula AND alc.bl_ativo = 1

            ) AS nu_alocados
    FROM    tb_matricula AS mt
            JOIN dbo.tb_turma AS tm ON tm.id_turma = mt.id_turma
            JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
            JOIN tb_areaprojetosala AS aps ON aps.id_projetopedagogico = mt.id_projetopedagogico
            JOIN dbo.tb_entidaderelacao AS ve ON ve.id_entidade = mt.id_entidadeatendimento
			JOIN tb_saladeaula AS sa ON sa.id_saladeaula = aps.id_saladeaula
                                        AND sa.dt_inicioinscricao <= tm.dt_inicio
                                        AND ( sa.dt_fiminscricao >= tm.dt_inicio
                                              OR sa.dt_fiminscricao IS NULL
                                            )
                                        AND sa.bl_ativa = 1
                                        AND sa.id_situacao = 8
                                        AND sa.id_categoriasala = 1
                                        AND sa.bl_todasentidades = 1
                                        AND ve.id_entidadepai = sa.id_entidade
                                        AND sa.bl_vincularturma = 0
            JOIN tb_disciplinasaladeaula AS ds ON ds.id_saladeaula = sa.id_saladeaula
            JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
                                                 AND ds.id_disciplina = md.id_disciplina
                                                 AND md.id_situacao != 65 -- Ignorar alocacoes com APROVEITAMENTO DE DISCIPLINA
            LEFT JOIN tb_alocacao al ON al.id_matriculadisciplina = md.id_matriculadisciplina
                                        AND al.bl_ativo = 1
            LEFT JOIN tb_disciplina as dc ON dc.id_disciplina = ds.id_disciplina
    UNION ALL
    SELECT DISTINCT
      aps.id_saladeaula ,
      sa.st_saladeaula ,
      sa.nu_maxalunos ,
      aps.id_projetopedagogico ,
      pp.id_trilha ,
      mt.id_matricula ,
      ds.id_disciplina ,
      md.id_matriculadisciplina ,
      mt.id_usuario ,
      al.id_alocacao ,
      se.id_entidade ,
      dc.id_tipodisciplina,
      ( SELECT    COUNT(alc.id_alocacao)
        FROM      tb_alocacao alc

        WHERE     alc.id_saladeaula = sa.id_saladeaula AND alc.bl_ativo = 1
      ) AS nu_alocados
    FROM    tb_matricula AS mt
      JOIN tb_turmasala ts ON ts.id_turma = mt.id_turma
      JOIN dbo.tb_turma AS tm ON tm.id_turma = mt.id_turma AND ts.id_turma = tm.id_turma
      JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
      JOIN tb_areaprojetosala AS aps ON aps.id_projetopedagogico = mt.id_projetopedagogico
      JOIN tb_saladeaulaentidade AS se ON
                                         se.id_entidade = mt.id_entidadeatendimento AND aps.id_saladeaula = se.id_saladeaula
      JOIN tb_saladeaula AS sa ON sa.id_saladeaula = aps.id_saladeaula
                                  AND sa.bl_ativa = 1
                                  AND sa.id_situacao = 8
                                  AND sa.id_categoriasala = 1
                                  AND se.id_saladeaula = sa.id_saladeaula
                                  AND sa.bl_vincularturma = 1
                                  AND sa.id_saladeaula = ts.id_saladeaula
      JOIN tb_disciplinasaladeaula AS ds ON ds.id_saladeaula = sa.id_saladeaula
      JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
                                           AND ds.id_disciplina = md.id_disciplina
                                           AND md.id_situacao != 65 -- Ignorar alocacoes com APROVEITAMENTO DE DISCIPLINA
      LEFT JOIN tb_alocacao al ON al.id_matriculadisciplina = md.id_matriculadisciplina
                                  AND al.bl_ativo = 1
      LEFT JOIN tb_disciplina as dc ON dc.id_disciplina = ds.id_disciplina
    UNION ALL
    SELECT DISTINCT
      aps.id_saladeaula ,
      sa.st_saladeaula ,
      sa.nu_maxalunos ,
      aps.id_projetopedagogico ,
      pp.id_trilha ,
      mt.id_matricula ,
      ds.id_disciplina ,
      md.id_matriculadisciplina ,
      mt.id_usuario ,
      al.id_alocacao ,
      ve.id_entidade ,
      dc.id_tipodisciplina,
      ( SELECT    COUNT(alc.id_alocacao)
        FROM      tb_alocacao alc

        WHERE     alc.id_saladeaula = sa.id_saladeaula AND alc.bl_ativo = 1

      ) AS nu_alocados
    FROM    tb_matricula AS mt
      JOIN tb_turmasala ts ON ts.id_turma = mt.id_turma
      JOIN dbo.tb_turma AS tm ON tm.id_turma = mt.id_turma AND ts.id_turma = tm.id_turma
      JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
      JOIN tb_areaprojetosala AS aps ON aps.id_projetopedagogico = mt.id_projetopedagogico
      JOIN dbo.tb_entidaderelacao AS ve ON ve.id_entidade = mt.id_entidadeatendimento
      JOIN tb_saladeaula AS sa ON sa.id_saladeaula = aps.id_saladeaula
                                  AND sa.bl_ativa = 1
                                  AND sa.id_situacao = 8
                                  AND sa.id_categoriasala = 1
                                  AND sa.bl_todasentidades = 1
                                  AND ve.id_entidadepai = sa.id_entidade
                                  AND sa.bl_vincularturma = 1
                                  AND sa.id_saladeaula = ts.id_saladeaula
      JOIN tb_disciplinasaladeaula AS ds ON ds.id_saladeaula = sa.id_saladeaula
      JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
                                           AND ds.id_disciplina = md.id_disciplina
                                           AND md.id_situacao != 65 -- Ignorar alocacoes com APROVEITAMENTO DE DISCIPLINA
      LEFT JOIN tb_alocacao al ON al.id_matriculadisciplina = md.id_matriculadisciplina
                                  AND al.bl_ativo = 1
      LEFT JOIN tb_disciplina as dc ON dc.id_disciplina = ds.id_disciplina
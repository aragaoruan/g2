SET IDENTITY_INSERT dbo.tb_funcionalidade on
INSERT INTO dbo.tb_funcionalidade
        ( id_funcionalidade,
          st_funcionalidade ,
          id_funcionalidadepai ,
          bl_ativo ,
          id_situacao ,
          st_classeflex ,
          st_urlicone ,
          id_tipofuncionalidade ,
          nu_ordem ,
          st_classeflexbotao ,
          bl_pesquisa ,
          bl_lista ,
          id_sistema ,
          st_ajuda ,
          st_target ,
          st_urlcaminho ,
          bl_visivel ,
          bl_relatorio ,
          st_urlcaminhoedicao ,
          bl_delete
        )
VALUES  (744, 'Aproveitamento de Disciplina', 157,	1, 123,	'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa',	'/img/ico/door--arrow.png', 3,	NULL,	'LancarAproveitamento',	0,	0,	1,	NULL,	NULL,	'/lancar-aproveitamento',	1,	0,	NULL,	0
        );
SET IDENTITY_INSERT dbo.tb_funcionalidade off

---------------------------------------------------------

CREATE TABLE tb_aproveitamento (
	id_aproveitamento INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	id_matricula INT NOT NULL FOREIGN KEY REFERENCES dbo.tb_matricula(id_matricula),
	dt_cadastro DATETIME DEFAULT GETDATE(),
	id_usuario INT NOT NULL FOREIGN KEY REFERENCES dbo.tb_usuario (id_usuario),
	bl_ativo bit DEFAULT 1
);

----------------------------------------------------------

ALTER TABLE dbo.tb_matriculadisciplina ADD id_usuarioaproveitamento INT NULL FOREIGN KEY REFERENCES dbo.tb_usuario(id_usuario);
ALTER TABLE dbo.tb_matriculadisciplina ADD st_disciplinaoriginal varchar(255) NULL;
ALTER TABLE dbo.tb_matriculadisciplina ADD st_instituicaoaproveitamento varchar(255) NULL;
ALTER TABLE dbo.tb_matriculadisciplina ADD sg_uf char(2) FOREIGN KEY REFERENCES dbo.tb_uf(sg_uf);
ALTER TABLE dbo.tb_matriculadisciplina ADD id_aproveitamento INT NULL FOREIGN KEY REFERENCES tb_aproveitamento(id_aproveitamento);

-----------------------------------------------------------

CREATE TABLE tb_ocorrenciaaproveitamento (
id_ocorrenciaaproveitamento INT PRIMARY KEY IDENTITY (1,1),
id_ocorrencia INT NOT NULL FOREIGN KEY REFERENCES dbo.tb_ocorrencia(id_ocorrencia),
id_aproveitamento INT NOT NULL FOREIGN KEY REFERENCES tb_aproveitamento(id_aproveitamento),
bl_ativo BIT DEFAULT 1,
dt_cadastro DATETIME DEFAULT GETDATE(),
id_usuariocadastro INT NOT NULL FOREIGN KEY REFERENCES dbo.tb_usuario(id_usuario)
);

------------------------------------------------------------

ALTER TABLE tb_matricula ADD id_aproveitamento INT NULL FOREIGN KEY REFERENCES dbo.tb_aproveitamento(id_aproveitamento);

------------------------------------------------------------

SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
GO

ALTER VIEW [dbo].[vw_matricula]

AS
SELECT
	mt.id_usuario,
	us.st_nomecompleto,
	us.st_cpf,
	ps.st_urlavatar,
	ps.dt_nascimento,
	ps.st_nomepai,
	ps.st_nomemae,
	ps.st_email,
	CAST(ps.nu_ddd AS VARCHAR) + '-' + CAST(ps.nu_telefone AS VARCHAR) AS st_telefone,
	CAST(ps.nu_dddalternativo AS VARCHAR) + '-'
	+ CAST(ps.nu_telefonealternativo AS VARCHAR) AS st_telefonealternativo,
	mt.id_matricula,
	pp.st_projetopedagogico,
	mt.id_situacao,
	st.st_situacao,
	mt.id_evolucao,
	ev.st_evolucao,
	mt.dt_concluinte,
	mt.id_entidadematriz,
	etmz.st_nomeentidade AS st_entidadematriz,
	mt.id_entidadematricula,
	etm.st_nomeentidade AS st_entidadematricula,
	mt.id_entidadeatendimento,
	eta.st_nomeentidade AS st_entidadeatendimento,
	mt.bl_ativo,
	cm.id_contrato,
	pp.id_projetopedagogico,
	mt.dt_termino,
	mt.dt_cadastro,
	mt.dt_inicio,
	tm.id_turma,
	tm.st_turma,
	tm.dt_inicio AS dt_inicioturma,
	mt.bl_institucional,
	tm.dt_fim AS dt_terminoturma,
	mt.id_situacaoagendamento,
	mt.id_vendaproduto,
	mt.dt_termino AS dt_terminomatricula,
	CAST(RIGHT(master.dbo.fn_varbintohexstr(HASHBYTES('MD5',
	(CAST(us.id_usuario AS VARCHAR(10)) COLLATE Latin1_General_CI_AI
	+ CAST((CASE
		WHEN mt.id_matricula IS NULL THEN 0
		ELSE mt.id_matricula
	END) AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
	+ CAST(5 AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
	+ CAST((0) AS VARCHAR(15)) COLLATE Latin1_General_CI_AI
	+ us.st_login
	+ us.st_senha
	+ CONVERT(VARCHAR(10), GETDATE(), 103)))),
	32) AS CHAR(32)) COLLATE Latin1_General_CI_AI AS st_urlacesso,
	mt.bl_documentacao,
	ct.id_venda,
	eta.st_urlportal,
	mt.st_codcertificacao,
	ps.st_identificacao,
	CONVERT(VARCHAR, mtc.dt_cadastro, 103) AS st_dtcadastrocertificacao,
	CONVERT(VARCHAR, mtc.dt_enviocertificado, 103) AS st_dtenviocertificado,
	CONVERT(VARCHAR, mtc.dt_retornocertificadora, 103) AS st_dtretornocertificadora,
	CONVERT(VARCHAR, mtc.dt_envioaluno, 103) AS st_dtenvioaluno,
	st_codigoacompanhamento,
	mtc.id_indicecertificado,
	mt.id_evolucaocertificacao,
	ap.id_areaconhecimento,
	pp.bl_disciplinacomplementar,
	mt.id_matriculavinculada,
	cr.id_contratoregra,
	trc.id_tiporegracontrato,
	etm.st_siglaentidade,
	mt.bl_academico,
	mtc.dt_cadastro AS dt_cadastrocertificacao,
	mtc.dt_enviocertificado,
	mtc.dt_retornocertificadora,
	mtc.dt_envioaluno,
	vup.st_upload AS st_uploadcontrato,
	canc.id_cancelamento,
	mt.id_aproveitamento
FROM tb_matricula mt
JOIN tb_projetopedagogico AS pp
	ON mt.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_areaprojetopedagogico AS ap
	ON ap.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_entidade AS etmz
	ON etmz.id_entidade = mt.id_entidadematriz
JOIN tb_entidade AS etm
	ON etm.id_entidade = mt.id_entidadematricula
JOIN tb_entidade AS eta
	ON eta.id_entidade = mt.id_entidadeatendimento
JOIN tb_situacao AS st
	ON st.id_situacao = mt.id_situacao
JOIN tb_evolucao AS ev
	ON ev.id_evolucao = mt.id_evolucao
JOIN tb_usuario AS us
	ON us.id_usuario = mt.id_usuario
LEFT JOIN tb_contratomatricula AS cm
	ON cm.id_matricula = mt.id_matricula
	AND cm.bl_ativo = 1
LEFT JOIN vw_pessoa AS ps
	ON mt.id_usuario = ps.id_usuario
	AND mt.id_entidadeatendimento = ps.id_entidade
LEFT JOIN dbo.tb_contrato AS ct
	ON ct.id_contrato = cm.id_contrato
	AND ct.bl_ativo = 1
LEFT JOIN tb_turma AS tm
	ON tm.id_turma = mt.id_turma
LEFT JOIN tb_matriculacertificacao mtc
	ON mtc.id_matricula = mt.id_matricula
	AND mtc.bl_ativo = 1
LEFT JOIN dbo.tb_contratoregra AS cr
	ON cr.id_contratoregra = ct.id_contratoregra
LEFT JOIN dbo.tb_tiporegracontrato AS trc
	ON trc.id_tiporegracontrato = cr.id_tiporegracontrato
LEFT JOIN dbo.tb_venda AS v
	ON ct.id_venda = v.id_venda
LEFT JOIN dbo.tb_upload AS vup
	ON vup.id_upload = v.id_uploadcontrato
LEFT JOIN dbo.tb_cancelamento AS canc
	ON canc.id_cancelamento = mt.id_cancelamento
WHERE mt.bl_ativo = 1;
GO


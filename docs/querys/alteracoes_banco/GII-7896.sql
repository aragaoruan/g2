/**
	GII-7896
	Alterando nome do relatório
**/

BEGIN TRANSACTION

UPDATE tb_funcionalidade
SET st_funcionalidade = 'Relatório de Agendamento'
WHERE id_funcionalidade = 761

COMMIT
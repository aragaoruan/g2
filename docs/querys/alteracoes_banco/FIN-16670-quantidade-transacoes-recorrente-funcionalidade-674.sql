-- 674 = id_funcionalidade

insert into tb_funcionalidade values 
('Quantidade Transações - Recorrente', 488, 1, 123, 
'', null, 3, 0, null, 0, 0,1, null, null, 
'quantidade-transacoes-recorrente', 1, 0, null, 1)

begin transaction

insert into [tb_entidadefuncionalidade] (id_entidade, id_funcionalidade, bl_ativo,id_situacao)
(select 2,id_funcionalidade, 1,5 from tb_funcionalidade where id_funcionalidade not in (select id_funcionalidade from tb_entidadefuncionalidade where id_entidade = 2))

insert into [tb_perfilfuncionalidade] (id_perfil, id_funcionalidade, bl_ativo)
(select 3,id_funcionalidade, 3 from tb_funcionalidade where id_funcionalidade not in (select id_funcionalidade from tb_perfilfuncionalidade where id_perfil = 3))


insert into [tb_perfilfuncionalidade] (id_perfil, id_funcionalidade, bl_ativo)
(select 1,id_funcionalidade, 3 from tb_funcionalidade where id_funcionalidade not in (select id_funcionalidade from tb_perfilfuncionalidade where id_perfil = 1))

insert into [tb_entidadefuncionalidade] (id_entidade, id_funcionalidade, bl_ativo,id_situacao)
(select 14,id_funcionalidade, 1,5 from tb_funcionalidade where id_funcionalidade not in (select id_funcionalidade from tb_entidadefuncionalidade where id_entidade = 14))

insert into [tb_perfilfuncionalidade] (id_perfil, id_funcionalidade, bl_ativo)
(select 3,id_funcionalidade, 3 from tb_funcionalidade where id_funcionalidade not in (select id_funcionalidade from tb_perfilfuncionalidade where id_perfil = 3))
insert into tb_permissaofuncionalidade (id_funcionalidade, id_permissao)
select fc.id_funcionalidade, pm.id_permissao from tb_funcionalidade fc, tb_permissao pm where fc.id_funcionalidade not in (select id_funcionalidade from tb_permissaofuncionalidade)
and fc.id_tipofuncionalidade not in (1,4,5,6) and pm.id_permissao in (1,2,3) AND fc.id_sistema = 1

insert into tb_perfilpermissaofuncionalidade (id_funcionalidade, id_perfil,id_permissao)
select pf.id_funcionalidade, 1 , pf.id_permissao from tb_permissaofuncionalidade pf where pf.id_funcionalidade not in (select id_funcionalidade from tb_perfilpermissaofuncionalidade)

commit

-- Inserindo novo campo na tb_entidade.
ALTER TABLE tb_entidade
ADD id_usuariosecretariado INT FOREIGN KEY REFERENCES tb_usuario(id_usuario);

-- Adicionando variável de sistema.
INSERT INTO tb_textovariaveis (
id_textocategoria,
st_camposubstituir,
st_textovariaveis,
st_mascara,
id_tipotextovariavel,
bl_exibicao)
VALUES (3, 'st_secretariado', '#secretario_entidade#', null, 1, 1);
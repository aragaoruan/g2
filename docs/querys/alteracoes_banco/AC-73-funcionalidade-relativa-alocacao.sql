INSERT INTO tb_permissao (id_permissao, st_nomepermissao, st_descricao, bl_ativo)
VALUES 
(8, 'Alocar', 'Alocação de alunos em sala de aula', 1),
(9, 'Integrar', 'Integração de alunos', 1),
(9, 'PRR', 'Programa de recuperação de alunos', 1);

--SELECT * from tb_permissao
--where id_permissao IN(8,9,10);

--inserir dados em permissao funcionalidade
insert INTO tb_permissaofuncionalidade values 
(611, 8), (611, 9), (611, 10);

--SELECT * FROM tb_permissaofuncionalidade
--where id_funcionalidade = 611

--inserir dados em perfil permissao funcionalidade
INSERT INTO tb_perfilpermissaofuncionalidade
VALUES (1, 611, 8), (1, 611, 9), (1, 611, 10);

--SELECT TOP 10 * from tb_perfilpermissaofuncionalidade
--where id_funcionalidade = 611

/**************************************************************************************************************
			INSERINDO NOVA VARIAVEL
**************************************************************************************************************/

BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_textovariaveis on
INSERT INTO tb_textovariaveis
(id_textovariaveis, id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
VALUES
(433, 4 , 'id_matricula' , '#grid_disciplina_historico_escolar#' , 'gerarGridHistoricoEscolar' , 2)
SET IDENTITY_INSERT dbo.tb_textovariaveis off
--ROLLBACK
COMMIT
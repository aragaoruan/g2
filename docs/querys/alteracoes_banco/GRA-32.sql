ALTER TABLE tb_pessoadadoscomplementares ADD sg_ufinstituicao CHAR(2) FOREIGN KEY REFERENCES tb_uf(sg_uf);
ALTER TABLE  tb_pessoadadoscomplementares ADD id_municipioinstituicao NUMERIC(30) FOREIGN KEY REFERENCES tb_municipio(id_municipio);

insert into tb_textovariaveis (id_textovariaveis, id_textocategoria,st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel) values(
   432, 4, 'st_municipioinstituicao','#municipio _estabelecimento#', 'retornaVariaveisUpper', 1
);
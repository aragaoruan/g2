-- GII-9286
BEGIN TRAN

UPDATE tb_matricula

SET nu_semestreatual = (
  CASE
  WHEN divisao.nu_disciplinas >= 3 AND divisao.resto < 3
    THEN divisao.resultado
  ELSE divisao.resultado + 1
  END
)

FROM
  tb_matricula AS mt
  JOIN tb_entidade AS en
    ON en.id_entidade = mt.id_entidadematricula
  JOIN tb_esquemaconfiguracao AS ec
    ON ec.id_esquemaconfiguracao = en.id_esquemaconfiguracao
       -- Graduação
       AND ec.id_esquemaconfiguracao = 1
  OUTER APPLY
  (
    SELECT
      COUNT(md.id_disciplina) AS nu_disciplinas,
      COUNT(md.id_disciplina) / 5 AS resultado,
      COUNT(md.id_disciplina) % 5 AS resto
    FROM
      tb_matriculadisciplina AS md
      JOIN tb_disciplina AS d
        ON d.id_disciplina = md.id_disciplina
           AND bl_disciplinasemestre = 1
    WHERE
      id_matricula = mt.id_matricula
      AND md.id_evolucao IN (12, 13)
  ) AS divisao
WHERE
  mt.bl_ativo = 1
  AND mt.id_evolucao = 6 -- Cursando
  AND mt.nu_semestreatual IS NULL
--AND mt.dt_cadastro BETWEEN '2018-01-01' AND '2018-03-08'
ROLLBACK
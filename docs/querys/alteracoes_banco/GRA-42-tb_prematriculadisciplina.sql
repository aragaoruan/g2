-- GRA 42

CREATE TABLE tb_prematriculadisciplina (
	id_prematriculadisciplina INT IDENTITY(1,1) NOT NULL,
	id_venda INT NOT NULL,
	id_disciplina INT NOT NULL,
	id_saladeaula INT NOT NULL
	CONSTRAINT [tb_prematriculadisciplina_pk] PRIMARY KEY CLUSTERED
	(
		[id_prematriculadisciplina] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE tb_prematriculadisciplina WITH CHECK ADD  CONSTRAINT [TB_PREMATRICULADISCIPLINA_TB_VENDA] FOREIGN KEY(id_venda)
REFERENCES tb_venda (id_venda);

ALTER TABLE tb_prematriculadisciplina WITH CHECK ADD  CONSTRAINT [TB_PREMATRICULADISCIPLINA_TB_DISCIPLINA] FOREIGN KEY(id_disciplina)
REFERENCES tb_disciplina (id_disciplina);

ALTER TABLE tb_prematriculadisciplina WITH CHECK ADD  CONSTRAINT [TB_PREMATRICULADISCIPLINA_TB_SALADEAULA] FOREIGN KEY(id_saladeaula)
REFERENCES tb_saladeaula (id_saladeaula);
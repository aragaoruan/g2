-- GII-9376
--
-- Corrigir código ibge dos municípios
UPDATE tb_municipio SET nu_codigoibge = id_municipio WHERE nu_codigoibge = 2147483647;
--
CREATE TABLE tb_censoinstituicao (
  id_censoinstituicao INT NOT NULL IDENTITY (1, 1) PRIMARY KEY,
  dt_cadastro DATETIME NOT NULL DEFAULT GETDATE(),
  bl_ativo BIT NOT NULL DEFAULT 1,
  nu_codigoinstituicao INT NOT NULL,
  st_censoinstituicao VARCHAR(120) NOT NULL
);
--
CREATE TABLE tb_censopolo (
  id_censopolo INT NOT NULL IDENTITY (1, 1) PRIMARY KEY,
  dt_cadastro DATETIME NOT NULL DEFAULT GETDATE(),
  bl_ativo BIT NOT NULL DEFAULT 1,
  id_entidade INT NOT NULL FOREIGN KEY REFERENCES tb_entidade (id_entidade),
  id_censoinstituicao INT NOT NULL FOREIGN KEY REFERENCES tb_censoinstituicao (id_censoinstituicao),
  nu_codigopolo INT NOT NULL
);
--
CREATE TABLE tb_censocurso (
  id_censocurso INT NOT NULL IDENTITY (1, 1) PRIMARY KEY,
  dt_cadastro DATETIME NOT NULL DEFAULT GETDATE(),
  bl_ativo BIT NOT NULL DEFAULT 1,
  id_projetopedagogico INT NOT NULL FOREIGN KEY REFERENCES tb_projetopedagogico (id_projetopedagogico),
  nu_codigocurso INT NOT NULL
);
--
--
SET IDENTITY_INSERT tb_censoinstituicao ON;
INSERT INTO
  tb_censoinstituicao (id_censoinstituicao, nu_codigoinstituicao, st_censoinstituicao)
VALUES
  (1, 3876, 'FACULDADE UNYLEYA')
SET IDENTITY_INSERT tb_censoinstituicao OFF;
--
--
SET IDENTITY_INSERT tb_censopolo ON;
INSERT INTO
  tb_censopolo (id_censopolo, id_entidade, id_censoinstituicao, nu_codigopolo)
VALUES
  (1, 353, 1, 1065770), -- ASA SUL
  (2, 654, 1, 1071275), -- CAMPOS DOS GOYTACAZES II - RJ
  (3, 655, 1, 1071235), -- MONTES CLAROS
  (4, 656, 1, 1071240), -- NOVA LIMA
  (5, 402, 1, 1085555), -- AGUAS CLARAS
  (6, 400, 1, 1085558), -- RIBEIRAO PRETO
  (7, 401, 1, 1085559), -- BELA VISTA
  (8, 657, 1, 1071238), -- SANTOS
  (9, 354, 1, 1001444), -- UNIDADE SEDE
  (10, 658, 1, 1070965); -- VITORIA
SET IDENTITY_INSERT tb_censopolo OFF;
--
--
SET IDENTITY_INSERT tb_censocurso ON;
INSERT INTO
  tb_censocurso (id_censocurso, id_projetopedagogico, nu_codigocurso)
VALUES
  (1, 4542, 5000943), -- GESTÃO PÚBLICA
  (2, 4478, 5000944), -- ADMINISTRAÇÃO
  (3, 4545, 1259286), -- HISTÓRIA
  (4, 4540, 5000945), -- GESTÃO DE RECURSOS HUMANOS
  (5, 4543, 1260202), -- LOGÍSTICA
  (6, 4526, 88088), -- PEDAGOGIA
  (7, 4539, 1259287), -- GESTÃO AMBIENTAL
  (8, 4544, 5000947), -- MARKETING
  (9, 4546, 1259288), -- LETRAS - LÍNGUA PORTUGUESA
  (10, 4541, 5000946), -- GESTÃO HOSPITALAR
  (11, 5822, 1332340); -- CIÊNCIAS CONTÁBEIS
SET IDENTITY_INSERT tb_censocurso OFF;
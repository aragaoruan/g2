-- Criando a tabela que irá armazenar os certificados parciais

CREATE TABLE G2_DEV.dbo.tb_certificadoparcial
(
    id_certificadoparcial INT PRIMARY KEY NOT NULL IDENTITY,
    st_certificadoparcial VARCHAR(255) NOT NULL,
    id_textosistema INT NOT NULL,
    dt_iniciovigencia DATETIME NOT NULL,
    dt_fimvigencia DATETIME NULL,
    id_usuariocadastro INT NOT NULL,
    dt_cadastro DATETIME NOT NULL,
    bl_gerado BIT DEFAULT 0 NOT NULL,
    id_entidadecadastro INT NOT NULL
    CONSTRAINT tb_certificadoparcial_tb_textosistema_id_textosistema_fk FOREIGN KEY (id_textosistema) REFERENCES tb_textosistema (id_textosistema),
    CONSTRAINT tb_certificadoparcial_tb_usuario_id_usuario_fk FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario),
    CONSTRAINT tb_certificadoparcial_tb_entidade_id_entidade_fk FOREIGN KEY (id_entidadecadastro) REFERENCES tb_entidade (id_entidade)
)

-- Criando a tabela de ligação dos certificados parciais com as disciplinas
CREATE TABLE G2_DEV.dbo.tb_certificadoparcialdisciplina
(
    id_certificadoparcialdisciplina INT PRIMARY KEY NOT NULL IDENTITY,
    id_certificadoparcial INT NOT NULL,
    id_disciplina INT NOT NULL,
    CONSTRAINT tb_certificadoparcialdisciplina_tb_certificadoparcial_id_certificadoparcial_fk FOREIGN KEY (id_certificadoparcial) REFERENCES tb_certificadoparcial (id_certificadoparcial),
    CONSTRAINT tb_certificadoparcialdisciplina_tb_disciplina_id_disciplina_fk FOREIGN KEY (id_disciplina) REFERENCES tb_disciplina (id_disciplina)
)

-- Criando a tabela de historico para guardar os certificados gerais
CREATE TABLE G2_DEV.dbo.tb_historicocertificadoparcial
(
  id_historicocertificadoparcial INT PRIMARY KEY NOT NULL IDENTITY,
  id_certificadoparcial INT NOT NULL,
  id_matricula INT NOT NULL,
  id_entidade INT NOT NULL,
  id_usuariocadastro INT,
  st_conteudo VARCHAR(MAX) NOT NULL,
  st_registro VARCHAR(255) NOT NULL,
  dt_cadastro DATETIME NOT NULL,
  CONSTRAINT tb_historicocertificadoparcial_tb_certificadoparcial_id_certificadoparcial_fk FOREIGN KEY (id_certificadoparcial) REFERENCES tb_certificadoparcial (id_certificadoparcial),
  CONSTRAINT tb_historicocertificadoparcial_tb_matricula_id_matricula_fk FOREIGN KEY (id_matricula) REFERENCES tb_matricula (id_matricula),
  CONSTRAINT tb_historicocertificadoparcial_tb_entidade_id_entidade_fk FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade),
  CONSTRAINT tb_historicocertificadoparcial_tb_usuario_id_usuario_fk FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
)
-- GII-7961
-- Adicionar novas variáveis
SET IDENTITY_INSERT dbo.tb_textovariaveis ON;
INSERT INTO tb_textovariaveis
(id_textovariaveis, id_textocategoria, st_camposubstituir, st_textovariaveis, id_tipotextovariavel, bl_exibicao)
VALUES
  (479, 3, 'nu_horasaativcomplementar', '#horas_atividades_complementares#', 1, 1);
SET IDENTITY_INSERT dbo.tb_textovariaveis OFF;
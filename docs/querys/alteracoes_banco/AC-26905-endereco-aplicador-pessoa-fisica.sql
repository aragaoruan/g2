/************************************************
INSERINDO NOVA CATEGORIA DE ENDERE�O
************************************************/

BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_categoriaendereco on
INSERT INTO [dbo].tb_categoriaendereco
([id_categoriaendereco]
,[st_categoriaendereco])
VALUES

(5,'Agendamento')

GO
SET IDENTITY_INSERT dbo.tb_categoriaendereco off

COMMIT


/************************************************
INSERINDO NOVA TIPO DE ENDERE�O
************************************************/
BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_tipoendereco on
INSERT INTO [dbo].tb_tipoendereco
([id_tipoendereco]
,[st_tipoendereco]
,[id_categoriaendereco])
VALUES

(6,'Aplicador de prova', 5)

GO
SET IDENTITY_INSERT dbo.tb_tipoendereco off

COMMIT



sp_help tb_aplicadorprova

sp_help tb_endereco

SELECT * FROM Vw_PessoaEndereco where id_usuario = 1 and id_tipoendereco = 6
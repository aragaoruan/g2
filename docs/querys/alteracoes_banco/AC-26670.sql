USE [G2H_EAD1]
GO

/****** Object:  View [dbo].[vw_gridcertificado]    Script Date: 12/03/2015 16:33:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[vw_gridcertificado] AS
SELECT
	mt.id_matricula,
	mt.id_projetopedagogico,
	pp.st_tituloexibicao,
	CAST(mtd.id_disciplina AS varchar(200)) AS id_disciplina,
	dc.st_tituloexibicao AS st_disciplina,
	mtd.nu_aprovafinal AS nu_aprovafinal,
	pp.nu_notamaxima,
	mtd.dt_conclusao AS dt_conclusaodisciplina,
	dc.nu_cargahoraria AS nu_cargahoraria,
	mtd.id_evolucao,
	ev.st_evolucao,
	mtd.id_situacao,
	CASE
		WHEN CAST(mtd.nu_aprovafinal AS int) < 70 THEN 'Insuficiente'
		WHEN CAST(mtd.nu_aprovafinal AS int) < 80 THEN 'Bom'
		WHEN CAST(mtd.nu_aprovafinal AS int) < 90 THEN 'Otimo'
		WHEN CAST(mtd.nu_aprovafinal AS int) <= 100 THEN 'Excelente'
		else 'Insuficiente'
	END AS st_conceito,
	us.st_nomecompleto as st_titularcertificacao,
	tit.st_titulacao,
	mt.id_entidadematricula,
	mt.dt_concluinte
FROM tb_matricula AS mt
JOIN tb_projetopedagogico AS pp ON mt.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_matriculadisciplina AS mtd ON mtd.id_matricula = mt.id_matricula AND mtd.bl_obrigatorio = 1
LEFT JOIN tb_aproveitamentomatriculadisciplina AS apm ON apm.id_matricula = mtd.id_matricula AND apm.id_disciplina = mtd.id_disciplina
LEFT JOIN tb_aproveitamentodisciplina AS apd ON apd.id_aproveitamentodisciplina = apm.id_aproveitamentodisciplina
JOIN tb_disciplina AS dc ON dc.id_disciplina = mtd.id_disciplina
JOIN tb_entidade AS ent ON ent.id_entidade = mt.id_entidadematriz
JOIN tb_evolucao AS ev ON ev.id_evolucao = mtd.id_evolucao
LEFT JOIN tb_perfil as pf ON pf.id_entidade = mt.id_entidadematricula AND pf.id_perfilpedagogico = 9
LEFT JOIN tb_usuarioperfilentidadereferencia as uper ON uper.id_projetopedagogico = pp.id_projetopedagogico AND uper.id_disciplina = mtd.id_disciplina AND uper.id_perfil = pf.id_perfil AND ((mt.dt_concluinte BETWEEN uper.dt_inicio AND uper.dt_fim) or (mt.dt_concluinte > uper.dt_inicio AND uper.dt_fim IS NULL)) AND uper.bl_titular = 1 AND uper.bl_ativo = 1
LEFT JOIN tb_usuario as us on us.id_usuario = uper.id_usuario
LEFT JOIN tb_titulacao tit ON uper.id_titulacao = tit.id_titulacao

GO


ALTER TABLE tb_projetopedagogico
ADD st_boasvindascurso VARCHAR(120);

ALTER TABLE tb_pessoa
ADD st_descricaocoordenador VARCHAR(160);

-- PERMISSÕES

SET IDENTITY_INSERT tb_funcionalidade ON
INSERT INTO tb_funcionalidade (id_funcionalidade, st_urlcaminho, id_funcionalidadepai, st_funcionalidade,
bl_ativo, id_situacao, id_tipofuncionalidade, bl_pesquisa, bl_lista, bl_visivel, bl_relatorio, bl_delete, id_sistema)
VALUES (
769, -- id_funcionalidade: Novo ID da funcionalidade. Quem roda a SQL em produção fica com a responsabilidade de adicionar um novo ID manualmente.
'769', -- st_urlcaminho: Geralmente, a url da funcionalidade que será exibida depois do #/. Aqui é apenas uma string com o id da funcionalidade, visto que nesse caso, estamos adicionando uma funcionalidade interna.
8, -- id_funcionalidadepai: A funcionalidade 'superior' na hierarquia do menu.
'Cadastro Descrição do Coordenador', -- st_funcionalidade: descrição da funcionalidade.
1, -- bl_ativo: funcionalidade ativa
123, -- id_situacao: funcionalidade em HTML
7, -- id_tipofuncionalidade: funcionalidade interna.
0, -- bl_pesquisa
0, -- bl_lista
0, -- bl_visivel
0, -- bl_relatorio
0, -- bl_delete
1); --id_sistema: 1 (G2S)
SET IDENTITY_INSERT tb_funcionalidade OFF
 
INSERT INTO tb_permissao (st_nomepermissao, st_descricao, bl_ativo) VALUES (
'Cadastro Descrição do Coordenador',
'Permite cadastrar a descrição do Coordenador de Curso na tela Pessoa / Cadastros / Usuários. A descrição é exibida na tela de boas vindas do novo portal.',
1
);

INSERT INTO tb_permissaofuncionalidade (id_funcionalidade, id_permissao) VALUES (
769,
36
);

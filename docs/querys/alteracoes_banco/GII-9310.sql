-- Inserindo relatório CENSO.

SET IDENTITY_INSERT tb_funcionalidade ON
 
INSERT INTO "tb_funcionalidade"
("id_funcionalidade",
"st_funcionalidade",
"id_funcionalidadepai",
"bl_ativo",
"id_situacao",
"st_classeflex",
"st_urlicone",
"id_tipofuncionalidade",
"nu_ordem",
"st_classeflexbotao",
"bl_pesquisa",
"bl_lista",
"id_sistema",
"st_ajuda",
"st_target",
"st_urlcaminho",
"bl_visivel",
"bl_relatorio",
"st_urlcaminhoedicao",
"bl_delete")
VALUES
(834, 'CENSO', 257, 'True', 123, NULL, NULL, 3, 0, NULL, 'False', 'False', 1, NULL, NULL, '834', 1, 'True', NULL, 'False'),
(835, 'Enade', 257, 'True', 123, NULL, NULL, 3, 0, NULL, 'False', 'False', 1, NULL, NULL, '835', 1, 'True', NULL, 'False');
 
SET IDENTITY_INSERT tb_funcionalidade OFF

/* Inserindo manualmente funcionalidade no perfil DDS_TODAS_ADMINISTRADOR
INSERT INTO tb_perfilfuncionalidade (id_perfil, id_funcionalidade, bl_ativo)
VALUES (3, 834, 1), (3, 835, 1); */
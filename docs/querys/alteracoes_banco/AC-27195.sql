INSERT INTO tb_funcionalidade

(st_funcionalidade,
id_funcionalidadepai,
bl_ativo,
id_situacao,
id_tipofuncionalidade,
bl_pesquisa,
bl_lista,
id_sistema,
st_urlcaminho,
bl_visivel,
bl_relatorio,
bl_delete)

VALUES (
'Usuário Dados Complementares',
561,
1,
123,
3,
0,
0,
1,
'/cadastro-dados-complementares',
1,
0,
0
);

ALTER TABLE tb_pessoa ADD
st_tituloeleitor VARCHAR(30),
st_certificadoreservista VARCHAR(30),
id_municipionascimento NUMERIC(30),
sg_ufnascimento CHAR(2),
CONSTRAINT [fk_tb_pessoa_tb_municipio] FOREIGN KEY (id_municipionascimento) REFERENCES tb_municipio(id_municipio),
CONSTRAINT [fk_tb_pessoa_tb_uf] FOREIGN KEY (sg_ufnascimento) REFERENCES tb_uf(sg_uf);

INSERT INTO tb_etnia (st_etnia)
VALUES ('Preto'), ('Amarelo'), ('Pardo'), ('Indígena');

CREATE TABLE [dbo].[tb_tipoescola](
	[id_tipoescola] [int] IDENTITY(1,1) NOT NULL,
	[st_tipoescola] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED
(
	[id_tipoescola] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 95) ON [PRIMARY]
) ON [PRIMARY]

INSERT INTO tb_tipoescola (st_tipoescola) VALUES ('Pública'),('Particular');

CREATE TABLE [dbo].[tb_formaingresso](
	[id_formaingresso] [int] IDENTITY(1,1) NOT NULL,
	[st_formaingresso] [varchar](50) NULL,
PRIMARY KEY CLUSTERED
(
	[id_formaingresso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 95) ON [PRIMARY]
) ON [PRIMARY];

INSERT INTO tb_formaingresso (st_formaingresso) VALUES
 ('Vestibular'), ('PAD'), ('ENEM'), ('PCS'), ('Transferência'), ('Matricula com Insenção de Disciplinas');

CREATE TABLE [dbo].[tb_pessoacomdeficiencia](
	[id_pessoacomdeficiencia] [int] IDENTITY(1,1) NOT NULL,
	[st_pessoacomdeficiencia] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED
(
	[id_pessoacomdeficiencia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 95) ON [PRIMARY]
) ON [PRIMARY];

INSERT INTO tb_pessoacomdeficiencia (st_pessoacomdeficiencia) VALUES ('Sim'), ('Não');

CREATE TABLE [dbo].[tb_descricaodeficiencia](
	[id_descricaodeficiencia] [int] IDENTITY(1,1) NOT NULL,
	[id_usuario] [int] NULL,
	[st_descricaodeficiencia] [varchar](200) NULL,
PRIMARY KEY CLUSTERED
(
	[id_descricaodeficiencia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 95) ON [PRIMARY]
) ON [PRIMARY];



CREATE TABLE [dbo].[tb_pessoadadoscomplementares](
	[id_pessoadadoscomplementares] [int] IDENTITY(1,1) NOT NULL,
	[id_usuario] [int] NOT NULL,
	[id_tipoescola] [int] NOT NULL,
	[st_nomeinstituicao] [varchar](140) NOT NULL,
	[id_formaingresso] [int] NOT NULL,
	[id_etnia] [int] NOT NULL,
	[id_pessoacomdeficiencia] [int] NOT NULL,
	[dt_anoconclusao] [char](4) NULL,
	[st_descricaodeficiencia] [varchar](200) NULL,
	[st_curso] [varchar](100) NULL,
	[dt_ingresso] [varchar](10) NULL,
PRIMARY KEY CLUSTERED
(
	[id_pessoadadoscomplementares] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 95) ON [PRIMARY]
) ON [PRIMARY];


CREATE VIEW [dbo].[vw_pessoadadoscomplementares] AS

SELECT DISTINCT

u.id_usuario,
p.id_entidade,
p.dt_nascimento,
p.id_pais,
p.st_sexo,
p.st_nomemae,
p.st_nomepai,
p.st_tituloeleitor,
p.st_certificadoreservista,
ia.st_curso,
ia.st_nomeinstituicao,
u.st_nomecompleto,
u.st_cpf,
di.st_rg,
di.st_orgaoexpeditor,
di.dt_dataexpedicao,
pais.st_nomepais,
ne.st_nivelensino,
t.st_titulacao,
uf.st_uf AS st_ufnascimento,
mun.st_nomemunicipio AS st_municipionascimento

FROM tb_pessoa as p
LEFT JOIN tb_informacaoacademicapessoa AS ia ON p.id_usuario = ia.id_usuario
INNER JOIN tb_usuario as u ON p.id_usuario = u.id_usuario
LEFT JOIN tb_documentoidentidade as di ON p.id_usuario = di.id_usuario
LEFT JOIN tb_pais as pais ON p.id_pais = pais.id_pais
LEFT JOIN tb_nivelensino AS ne ON ne.id_nivelensino = ia.id_nivelensino
LEFT JOIN tb_titulacao AS t ON t.id_titulacao = u.id_titulacao
LEFT JOIN tb_uf AS uf ON uf.sg_uf = p.sg_ufnascimento
LEFT JOIN tb_municipio AS mun ON mun.id_municipio = p.id_municipionascimento
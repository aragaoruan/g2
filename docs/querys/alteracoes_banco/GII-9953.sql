-- GII-9953
-- Ajustar os campos que traz o nome da mãe nas categorias "Certificado" e "Declarações", mantendo a nomenclatura como o do nome do pai já existente
UPDATE tb_textovariaveis SET st_camposubstituir = 'st_mae' WHERE id_textovariaveis IN (416, 452)

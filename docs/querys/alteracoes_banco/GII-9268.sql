-- GII-9268
-- Criando a mensagem padrão para ligar com o texto de sistema da demanda.
SET IDENTITY_INSERT tb_mensagempadrao ON;
INSERT INTO tb_mensagempadrao (id_mensagempadrao,id_tipoenvio, st_mensagempadrao)
    VALUES (51, 3, 'Lançamento de Nota na Prova Final (Pós)');
SET IDENTITY_INSERT tb_mensagempadrao OFF;


-- Criando a variavel
-- tive que utilizar o dt_agendamento para entrar dentro do metodo calcularDataRecurso
SET IDENTITY_INSERT tb_textovariaveis ON;
INSERT INTO tb_textovariaveis (id_textovariaveis, id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
VALUES (775, 1, 'dt_agendamento', '#dt_limite_recurso_nota_prova#', 'calcularDataRecurso', 2);
SET IDENTITY_INSERT tb_textovariaveis OFF;
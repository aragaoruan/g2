
/*
 * SQL PARA FUNCIONALIDADE DE REGISTRO DE NOTIFICACAO DE PERCENTUAL DE LIMITE DE ALUNOS EM SALA DE AULA
 *
 * issue http://jira.unyleya.com.br/browse/AC-26849
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */

set identity_insert dbo.tb_funcionalidade on

INSERT INTO tb_funcionalidade
(id_funcionalidade,
 st_funcionalidade,
 id_funcionalidadepai,
 bl_ativo,
 id_situacao,
 st_classeflex,
 st_urlicone,
 id_tipofuncionalidade,
 nu_ordem,
 st_classeflexbotao,
 bl_pesquisa,
 bl_lista,
 id_sistema,
 st_ajuda,
 st_target,
 st_urlcaminho,
 bl_visivel,
 bl_relatorio,
 st_urlcaminhoedicao,
 bl_delete)
VALUES (688,
        'Notificações',
        231,
        true,
        123,
        'br.com.ead1.gestor2.view.conteudo.entidade.cadastrar.CadastrarEntidadeFuncionalidade',
        '/img/ico/ui-check-boxes.png',
        3,
        null,
        null,
        false,
        false,
        1,
        'http://wiki.unyleya.com.br/index.php/Funcionalidades',
        null,
        '/organizacao/notificacoes',
        1,
        false,
        null,
        false)

set identity_insert dbo.tb_funcionalidade off


create table tb_notificacao (
        id_notificacao int primary key identity(1,1),
        st_notificacao varchar(50),
        dt_cadastro datetime default getdate()
);


create table tb_notificacaoentidade (
        id_notificacaoentidade int primary key identity(1,1),
        id_notificacao int,
        id_entidade int,
        bl_enviaparaemail bit,
        nu_percentuallimitealunosala int null,
        dt_cadastro datetime default getdate(),
        constraint FK_TB_NOTIFICACAO_ENTIDADE_TB_NOTIFICACAO foreign key (id_notificacao) references tb_notificacao(id_notificacao),
        constraint FK_TB_NOTIFICACAO_ENTIDADE_TB_ENTIDADE foreign key (id_entidade) references tb_entidade (id_entidade)
);

create table tb_notificacaoentidadeperfil (
        id_notificacaoentidadeperfil int primary key identity(1,1),
        id_notificacaoentidade int,
        id_perfil int,
        constraint FK_TB_NOTIFICACAO_ENTIDADE_PERFIL_TB_NOTIFICACAO_ENTIDADE foreign key (id_notificacaoentidade) references tb_notificacaoentidade(id_notificacaoentidade),
        constraint FK_TB_NOTIFICACAO_ENTIDADE_PERFIL_TB_PERFIL foreign key (id_perfil) references tb_perfil (id_perfil)
);

set identity_insert dbo.tb_notificacao on

INSERT INTO tb_notificacao (id_notificacao,st_notificacao,dt_cadastro)
VALUES (1,'Limite de Aluno em Sala de Aula','2015-05-14 15:01:34')

set identity_insert dbo.tb_notificacao off
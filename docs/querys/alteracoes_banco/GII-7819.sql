-- GII-7819

SET IDENTITY_INSERT tb_tipotramite ON;
INSERT INTO tb_tipotramite (id_tipotramite, id_categoriatramite, st_tipotramite) VALUES (29, 1, 'Transferência de Turma');
SET IDENTITY_INSERT tb_tipotramite OFF;

UPDATE tb_tramite SET id_tipotramite = 29 WHERE id_tipotramite = 11 AND st_tramite LIKE 'Transferido da turma%';
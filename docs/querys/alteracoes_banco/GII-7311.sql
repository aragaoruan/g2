/**
              LEMBRAR DE CONFERIR O CADASTRO NA TB_ENTIDADEINTEGRACAO PARA O SISTEMA ACTOR E FLUXOS
              E SEUS RESPECTIVOS CODIGOS DE ACESSO.
 */

alter table tb_pessoa add bl_sincronizarg1 bit default 0;
alter table tb_pessoa add bl_sincronizarfluxus bit default 0;

----------------------------------------------------------------------------------------------

ALTER VIEW [dbo].[vw_pessoa]
AS
    SELECT  u.id_usuario ,
            u.st_login ,
            CASE WHEN uss.id_dadosacesso IS NULL THEN u.st_login
                 ELSE uss.st_login
            END AS st_loginentidade ,
            u.st_nomecompleto ,
            u.st_cpf ,
            u.id_registropessoa ,
            u.st_senha ,
            CASE WHEN uss.id_dadosacesso IS NULL
                 THEN CAST(u.st_senha AS VARCHAR)
                 ELSE CAST(uss.st_senha AS VARCHAR)
            END AS st_senhaentidade ,
            p.id_entidade ,
            p.st_sexo ,
            p.dt_nascimento ,
            uf.st_uf as st_ufnascimento,
            p.sg_ufnascimento AS sg_ufnascimento ,
            mun.st_nomemunicipio as st_municipionascimento,
            p.id_municipionascimento AS id_municipionascimento ,
            p.st_cidade AS st_cidadenascimento ,
            p.id_pais AS id_paisnascimento,
            p.id_estadocivil ,
            ec.st_estadocivil ,
            cepp.id_email ,
            ce.st_email ,
            cepp.bl_padrao AS bl_emailpadrao ,
            p.st_nomemae ,
            p.st_nomepai ,
            tel1.id_telefone ,
            1 AS bl_telefonepadrao ,
            tel1.nu_telefone ,
            tel1.id_tipotelefone ,
            tel1.st_tipotelefone ,
            tel1.nu_ddd ,
            tel1.nu_ddi ,
            tel2.id_telefone AS id_telefonealternativo ,
            tel2.nu_telefone AS nu_telefonealternativo ,
            tel2.id_tipotelefone AS id_tipotelefonealternativo ,
            tel2.st_tipotelefone AS st_tipotelefonealternativo ,
            tel2.nu_ddd AS nu_dddalternativo ,
            tel2.nu_ddi AS nu_ddialternativo ,
            di.st_rg ,
            di.st_orgaoexpeditor ,
            di.dt_dataexpedicao ,
            pe.id_endereco ,
            1 AS bl_enderecopadrao ,
            pe.st_endereco ,
            pe.st_cep ,
            pe.st_bairro ,
            pe.st_complemento ,
            pe.nu_numero ,
            pe.st_estadoprovincia ,
            CASE WHEN pe.st_cidade IS NOT NULL THEN pe.st_cidade
                 ELSE pe.st_nomemunicipio
            END AS st_cidade ,
            pe.id_pais ,
            pe.id_municipio ,
            pe.id_tipoendereco ,
            pe.sg_uf ,
            pe.st_nomepais ,
            pe.st_nomemunicipio ,
            p.st_identificacao ,
            p.st_urlavatar,
			tbti.id_titulacao,
			tbti.st_titulacao
    FROM    tb_usuario AS u
            INNER JOIN tb_pessoa AS p ON p.id_usuario = u.id_usuario
                                         AND p.bl_ativo = 1
            LEFT JOIN tb_dadosacesso AS uss ON u.id_usuario = uss.id_usuario
                                               AND uss.bl_ativo = 1
                                               AND uss.id_entidade = p.id_entidade
                                               AND uss.id_entidade = p.id_entidade
            LEFT JOIN tb_contatosemailpessoaperfil AS cepp ON cepp.id_usuario = u.id_usuario
                                                              AND cepp.id_entidade = p.id_entidade
                                                              AND cepp.bl_ativo = 1
                                                              AND cepp.bl_padrao = 1
            LEFT JOIN tb_contatosemail AS ce ON ce.id_email = cepp.id_email
            OUTER APPLY ( SELECT TOP 1
                                    ct2.id_telefone ,
                                    ct2.id_tipotelefone ,
                                    ct2.nu_ddd ,
                                    ct2.nu_ddi ,
                                    ct2.nu_telefone ,
                                    tt2.st_tipotelefone
                          FROM      tb_contatostelefonepessoa AS ctp2
                                    LEFT JOIN tb_contatostelefone AS ct2 ON ct2.id_telefone = ctp2.id_telefone
                                    LEFT JOIN tb_tipotelefone AS tt2 ON tt2.id_tipotelefone = ct2.id_tipotelefone
                          WHERE     ctp2.id_usuario = u.id_usuario
                                    AND ctp2.id_entidade = p.id_entidade
                                    AND ctp2.bl_padrao = 1
                        ) AS tel1
            OUTER APPLY ( SELECT TOP 1
                                    ct2.id_telefone ,
                                    ct2.id_tipotelefone ,
                                    ct2.nu_ddd ,
                                    ct2.nu_ddi ,
                                    ct2.nu_telefone ,
                                    tt2.st_tipotelefone
                          FROM      tb_contatostelefonepessoa AS ctp2
                                    LEFT JOIN tb_contatostelefone AS ct2 ON ct2.id_telefone = ctp2.id_telefone
                                    LEFT JOIN tb_tipotelefone AS tt2 ON tt2.id_tipotelefone = ct2.id_tipotelefone
                          WHERE     ctp2.id_usuario = u.id_usuario
                                    AND ctp2.id_entidade = p.id_entidade
                                    AND ctp2.bl_padrao = 0
                        ) AS tel2
            OUTER APPLY ( SELECT TOP 1
                                    e1.* ,
                                    mu.st_nomemunicipio ,
                                    pa.st_nomepais
                          FROM      tb_pessoaendereco AS pe1
                                    LEFT JOIN tb_endereco AS e1 ON e1.id_endereco = pe1.id_endereco
                                                              AND e1.bl_ativo = 1
                                    LEFT JOIN tb_municipio AS mu ON mu.id_municipio = e1.id_municipio
                                    LEFT JOIN dbo.tb_pais AS pa ON pa.id_pais = e1.id_pais
                          WHERE     pe1.id_usuario = u.id_usuario
                                    AND pe1.id_entidade = p.id_entidade
                                    AND pe1.bl_padrao = 1
                        ) AS pe

            LEFT JOIN tb_estadocivil AS ec ON ec.id_estadocivil = p.id_estadocivil
            LEFT JOIN dbo.tb_documentoidentidade AS di ON di.id_usuario = u.id_usuario
                                                          AND di.id_entidade = p.id_entidade
			LEFT JOIN dbo.tb_titulacao as tbti ON tbti.id_titulacao = u.id_titulacao
      LEFT JOIN dbo.tb_municipio as mun on p.id_municipionascimento = mun.id_municipio
      LEFT JOIN dbo.tb_uf as uf  on p.sg_ufnascimento = uf.sg_uf
GO
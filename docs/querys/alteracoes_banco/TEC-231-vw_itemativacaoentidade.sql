CREATE VIEW vw_itemativacaoentidade AS (
	SELECT
	ia.id_itemativacao,
	ia.st_itemativacao,
	iae.id_entidade,
	iae.id_itemativacaoentidade,
	iae.id_situacao,
	s.st_situacao,
	iae.id_usuarioatualizacao,
	iae.dt_atualizacao,
	CONVERT(VARCHAR(MAX), ia.st_descricao) AS st_descricao,
	ia.st_responsavel,
	ia.st_caminhosistema
	FROM
	tb_itemativacao AS ia
	LEFT JOIN
	tb_itemativacaoentidade AS iae
	LEFT JOIN tb_situacao AS s
	ON iae.id_situacao = s.id_situacao
	ON ia.id_itemativacao = iae.id_itemativacao

	UNION
	SELECT
	ia.id_itemativacao,
	ia.st_itemativacao,
	e.id_entidade,
	NULL AS id_itemativacaoentidade,
	NULL AS id_situacao,
	NULL AS st_situacao,
	NULL AS id_usuarioatualizacao,
	NULL AS dt_atualizacao,
	CONVERT(VARCHAR(MAX), ia.st_descricao) AS st_descricao,
	ia.st_responsavel,
	ia.st_caminhosistema
	FROM
	tb_itemativacao AS ia
	JOIN tb_entidade AS e ON e.bl_ativo = 1

	WHERE ia.bl_ativo = 1
	AND id_itemativacao NOT IN (SELECT iae.id_itemativacao FROM tb_itemativacaoentidade AS iae WHERE iae.id_entidade = e.id_entidade)
);
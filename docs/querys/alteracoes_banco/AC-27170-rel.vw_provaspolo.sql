CREATE VIEW [rel].[vw_provasporpolo] AS
SELECT
	ap.id_aplicadorprova,
	aa.id_avaliacaoaplicacao,
	ape.id_entidade,
	ap.st_aplicadorprova,
	aa.dt_aplicacao,
	ha.hr_inicio,
	ha.hr_fim,
	aa.nu_maxaplicacao,
	(SELECT COUNT(id_avaliacaoagendamento) FROM tb_avaliacaoagendamento saa WHERE saa.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao AND bl_ativo = 1 AND saa.id_situacao IN(68,69)) AS nu_totalagendamentos,
	(SELECT COUNT(id_avaliacaoagendamento) FROM tb_avaliacaoagendamento saa2 WHERE saa2.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao AND saa2.id_tipodeavaliacao = 0 AND bl_ativo = 1 AND saa2.id_situacao IN(68,69)) AS nu_agendamentoprovafinal,
	(SELECT COUNT(id_avaliacaoagendamento) FROM tb_avaliacaoagendamento saa3 WHERE saa3.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao AND saa3.id_tipodeavaliacao > 0 AND bl_ativo = 1 AND saa3.id_situacao IN(68,69)) AS nu_agendamentorecuperacao,
	(nu_maxaplicacao - (SELECT COUNT(id_avaliacaoagendamento) FROM tb_avaliacaoagendamento saa4 WHERE saa4.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao AND bl_ativo = 1 AND saa4.id_situacao IN(68,69))) AS nu_vagasrestantes
FROM tb_aplicadorprova ap
JOIN tb_aplicadorprovaentidade ape on ape.id_aplicadorprova = ap.id_aplicadorprova
JOIN tb_avaliacaoaplicacao aa on aa.id_aplicadorprova = ap.id_aplicadorprova
JOIN tb_avaliacao a on a.id_entidade = ape.id_entidade AND a.id_tipoavaliacao IN(1,2,3,4)
JOIN tb_horarioaula ha on ha.id_horarioaula = aa.id_horarioaula

BEGIN TRAN

-- Define todos os moodles que estão cadastrados como padrão
UPDATE tb_entidadeintegracao
SET st_titulo = 'Moodle Padrão',
  id_situacao = 198,
  bl_ativo = 1
WHERE id_sistema = 6;

-- Seta o id_entidadeintegracao padrão (o unico que deve ter no banco antes do multiplos moodle) em todas as salas
UPDATE sl
SET sl.id_entidadeintegracao = ei.id_entidadeintegracao
FROM
  tb_saladeaula AS sl
  JOIN tb_saladeaulaintegracao AS sli ON sli.id_saladeaula = sl.id_saladeaula
                                         AND sli.id_sistema = 6
  JOIN tb_entidadeintegracao AS ei ON ei.id_entidade = sl.id_entidade
                                      AND ei.id_sistema = 6
                                      AND ei.bl_ativo = 1
                                      AND ei.id_situacao = 198
WHERE
  sl.id_entidadeintegracao IS NULL;

--altera os registros de usuario integracao setado o o id do moodle na tabela
UPDATE usi
SET usi.id_entidadeintegracao = ei.id_entidadeintegracao
FROM
  tb_usuariointegracao AS usi
  JOIN tb_entidadeintegracao AS ei ON ei.id_entidade = usi.id_entidade
                                      AND ei.id_sistema = 6
                                      AND ei.bl_ativo = 1
                                      AND ei.id_situacao = 198
                                      AND NOT EXISTS(
    SELECT
      id_usuariointegracao
    FROM
      tb_usuariointegracao
    WHERE
      id_sistema = 6
      AND id_usuario = usi.id_usuario
      AND id_entidade = usi.id_entidade
      AND id_entidadeintegracao = ei.id_entidadeintegracao
)
WHERE
  usi.id_sistema = 6
  AND usi.id_entidadeintegracao IS NULL;

-- Altera as alocações existes com o id da entidade integracao da sala de aula
UPDATE ali
SET ali.id_entidadeintegracao = sl.id_entidadeintegracao
FROM
  tb_alocacaointegracao AS ali
  JOIN tb_alocacao AS al ON al.id_alocacao = ali.id_alocacao
  JOIN tb_saladeaula AS sl ON sl.id_saladeaula = al.id_saladeaula
                              AND sl.id_entidadeintegracao IS NOT NULL;


-- Vincula todas as salas de referencias ao seu respectivo moodle padrão.
 UPDATE dsci
SET dsci.id_entidadeintegracao = ei.id_entidadeintegracao
FROM
	tb_disciplinaintegracao AS dsci
JOIN tb_entidadeintegracao AS ei ON ei.id_entidade = dsci.id_entidade
WHERE
	ei.bl_ativo = 1
AND ei.id_situacao = 198;
ROLLBACK
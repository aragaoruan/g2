/***
*** TABELA LINHA DE NEGOCIO 
**/

CREATE TABLE tb_linhadenegocio
(
id_linhadenegocio INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
st_linhadenegocio varchar(255) NOT NULL
);


/***
*** INSERINDO DADOS NA TABELA LINHA DE NEGOCIO 
**/
BEGIN TRAN
SET IDENTITY_INSERT dbo.tb_linhadenegocio ON
INSERT INTO dbo.tb_linhadenegocio
(id_linhadenegocio, st_linhadenegocio)
VALUES( 1 , 'Concurso'),
(2, 'Gradua��o'),
(3, 'P�s Gradua��o')
SET IDENTITY_INSERT dbo.tb_linhadenegocio OFF
COMMIT

/***
*** INSERINDO ITEM DE CONFIGURA��O PARA LINHA DE NEGOCIO
**/
INSERT tb_itemconfiguracao (id_itemconfiguracao, st_itemconfiguracao, st_descricao, st_default) VALUES 
(22, 'Linha de neg�cio', 'Linha de neg�cio da entidade', '')




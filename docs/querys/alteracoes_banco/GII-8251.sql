-- GII-7829
ALTER TABLE tb_matriculadiplomacao
  ADD st_cedula VARCHAR(20) NULL,
  st_motivo VARCHAR(300) NULL,
  id_diplomacaoorigem INT NULL,
  FOREIGN KEY (id_diplomacaoorigem) REFERENCES tb_matriculadiplomacao (id_matriculadiplomacao);
SET IDENTITY_INSERT tb_evolucao ON;
INSERT INTO tb_evolucao (id_evolucao, st_evolucao, st_tabela, st_campo) VALUES
  (84, '2° Via do Diploma Gerado', 'tb_matricula', 'id_evolucaodiplomacao'),
  (85, '2° Via do Histórico Gerado', 'tb_matricula', 'id_evolucaodiplomacao'),
  (86, '2° Via do Quadro de Equivalência Gerado', 'tb_matricula', 'id_evolucaodiplomacao');
SET IDENTITY_INSERT tb_evolucao OFF;
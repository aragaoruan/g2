-- Criando tb_impressaocertificado

CREATE TABLE tb_impressaocertificado (

    PRIMARY KEY (id_impressaocertificado),
    CONSTRAINT FK_TB_IMPRESSAOCERTIFICADO_TB_USUARIO
    	FOREIGN KEY (id_usuario)
    	REFERENCES tb_usuario(id_usuario),
    CONSTRAINT FK_TB_IMPRESSAOCERTIFICADO_TB_MATRICULA
    	FOREIGN KEY (id_matricula)
    	REFERENCES tb_matricula(id_matricula),

    id_impressaocertificado   INTEGER NOT NULL IDENTITY,
    st_textocertificado 	    TEXT,
    id_matricula 			        INTEGER,
    bl_ativo 				          BIT,
    dt_cadastro				        DATETIME2,
    dt_inativo				        DATETIME2,
    id_usuario				        INTEGER
);
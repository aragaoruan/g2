CREATE TABLE dbo.tb_departamento (
 id_departamento INT IDENTITY(1,1) NOT NULL,
 st_departamento VARCHAR(255) NOT NULL,
  CONSTRAINT [tb_departamento_pk] PRIMARY KEY CLUSTERED
 ([id_departamento] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
 ON [PRIMARY]
) ON [PRIMARY];

SET IDENTITY_INSERT dbo.tb_departamento ON;

INSERT INTO dbo.tb_departamento (id_departamento, st_departamento) VALUES
(1, 'Acadêmico'),
(2, 'Comercial'),
(3, 'Financeiro'),
(4, 'Tecnológico');

SET IDENTITY_INSERT dbo.tb_departamento OFF;

ALTER TABLE [dbo].[tb_itemativacao] ADD id_departamento INT NULL;
ALTER TABLE [dbo].[tb_itemativacao]  WITH CHECK ADD  CONSTRAINT [FK_TB_ITEMATIV_REFERENCE_TB_DEPART] FOREIGN KEY([id_departamento])
REFERENCES [dbo].[tb_departamento] ([id_departamento]);

UPDATE tb_itemativacao
  set id_departamento= 1 where id_itemativacao BETWEEN '1' AND '6';
  set id_departamento= 2 where id_itemativacao BETWEEN '7' AND '12';
  set id_departamento= 3 where id_itemativacao BETWEEN '13' AND '18';
  set id_departamento= 4 where id_itemativacao BETWEEN '19' AND '26';
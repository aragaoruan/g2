BEGIN TRANSACTION
SET IDENTITY_INSERT [dbo].[tb_itemconfiguracao] ON

INSERT INTO [dbo].[tb_itemconfiguracao] ([id_itemconfiguracao], [st_itemconfiguracao], [st_descricao], [st_default])
VALUES ('31', 'Renovação Automática a Partir da',
        'Este campo permite definir o parâmetro de quando se iniciará o processo de renovação do aluno com base na data de confirmação da venda.',
        '5');
SET IDENTITY_INSERT [dbo].[tb_itemconfiguracao] OFF

COMMIT



--vincula o item com o esquema de configuração da graduação
INSERT INTO [dbo].[tb_esquemaconfiguracaoitem] ([id_esquemaconfiguracao], [id_itemconfiguracao], [st_valor])
VALUES ('1', '31', '5');
CREATE TABLE tb_origemareaconhecimento (
	id_origemareaconhecimento INT IDENTITY (1,1) PRIMARY KEY,
	st_origemareaconhecimento VARCHAR(140)
);

SET IDENTITY_INSERT tb_origemareaconhecimento ON;

INSERT INTO tb_origemareaconhecimento
(id_origemareaconhecimento, st_origemareaconhecimento)
VALUES
(1, 'CAPES'),
(2, 'MEC');

SET IDENTITY_INSERT tb_origemareaconhecimento OFF;

ALTER TABLE tb_areaconhecimento ADD
id_origemareaconhecimento INTEGER DEFAULT 1 WITH VALUES;

ALTER TABLE tb_areaconhecimento
ADD CONSTRAINT FK_tb_areaconhecimento_tb_origemareaconhecimento
FOREIGN KEY (id_origemareaconhecimento) REFERENCES tb_origemareaconhecimento(id_origemareaconhecimento);

ALTER TABLE tb_projetopedagogico ADD
id_areaconhecimentomec INTEGER DEFAULT null;

-- Inserindo novas áreas de conhcimento conforme especificadas.

INSERT INTO tb_areaconhecimento

(id_situacao, st_descricao, bl_ativo, st_areaconhecimento, id_entidade, dt_cadastro,
 id_usuariocadastro, id_areaconhecimentopai, id_tipoareaconhecimento, st_tituloexibicao, id_origemareaconhecimento)

VALUES

(6, 'Ciências, matemática e computação', 1, 'Ciências, matemática e computação', 2, GETDATE(), 1, null, 1, 'Ciências, matemática e computação', 2),
(6,	'Ciências sociais, negócios e direito',	1, 'Ciências sociais, negócios e direito', 2, GETDATE(), 1, null, 1, 'Ciências sociais, negócios e direito', 2),
(6, 'Educação', 1, 'Educação', 2, GETDATE(), 1, null, 1, 'Educação', 2),
(6, 'Humanidades e artes', 1, 'Humanidades e artes', 2, GETDATE(), 1, null, 1, 'Humanidades e artes', 2),
(6,'Saúde e bem estar social', 1, 'Saúde e bem estar social', 2, GETDATE(), 1, null, 1, 'Saúde e bem estar social', 2);
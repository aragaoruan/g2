-- GII-8915
ALTER TABLE tb_nucleoco
  ADD id_holdingcompartilhamento INTEGER,
  FOREIGN KEY (id_holdingcompartilhamento) REFERENCES tb_holding (id_holding);
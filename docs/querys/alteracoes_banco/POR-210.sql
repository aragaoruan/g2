/************************************************************************
**** Adicionando novas situações
**** POR-210
*************************************************************************/
BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_situacao ON

INSERT INTO tb_situacao
([id_situacao],
 [st_situacao],
 [st_tabela],
 [st_campo],
 [st_descricaosituacao])
VALUES
  (203, 'Pendente', 'tb_alocacao', 'id_situacaotcc', 'Aguardando envio de TCC pelo aluno'),
  (204, 'Em análise', 'tb_alocacao', 'id_situacaotcc', 'TCC enviado pelo aluno aguardando análise do tutor'),
  (205, 'Devolvido ao aluno', 'tb_alocacao', 'id_situacaotcc',
   'TCC avaliado pelo tutor e devolvido para o aluno para reformulação'),
  (206, 'Avaliado pelo Orientador', 'tb_alocacao', 'id_situacaotcc', 'TCC enviado pelo aluno e avaliado pelo orientador')

SET IDENTITY_INSERT dbo.tb_situacao OFF

COMMIT
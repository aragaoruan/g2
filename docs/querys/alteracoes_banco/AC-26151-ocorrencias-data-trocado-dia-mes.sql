/**
 * Historia AC-26151: Alteracao feita para verificacao de data registrada
 * @author: Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 * Campo adicionado para receber a data do Zend para ser verificada posteriormente
*/

ALTER table tb_ocorrencia
add dt_cadastroocorrencia date;
ALTER TABLE tb_transacaofinanceira ADD
            id_statustransacao INT,
            st_codigoautorizacao VARCHAR(100),
            nu_valorenviado MONEY,
            nu_valorautorizado MONEY,
            nu_valorrecusado MONEY,
            st_meiopagamento VARCHAR(50),
            st_urlboleto VARCHAR(250),
            st_boletocoditobarras VARCHAR(250),
            st_urlpostback VARCHAR(250),
            dt_vencimento DATETIME,
            st_nsu varchar(100),
            id_transacaoexterna varchar(150)

        ALTER TABLE tb_lancamento ADD
            st_urlboleto VARCHAR(250),
            st_codigobarras VARCHAR(250,
            id_transacaoexterna varchar(150)


        INSERT tb_situacao (id_situacao, st_situacao, st_tabela, st_campo, st_descricaosituacao) VALUES
            (187, 'processing', 'tb_transacaofinanceira', 'id_statustransacao', 'transação sendo processada.'),
            (188, 'authorized', 'tb_transacaofinanceira', 'id_statustransacao', 'transação autorizada. Cliente possui saldo na conta e este valor foi reservado para futura captura, deve acontecer em 5 dias. Caso a transação não seja capturada, a mesma é cancelada automaticamente.'),
            (189, 'paid', 'tb_transacaofinanceira', 'id_statustransacao', 'transação paga (autorizada e capturada).'),
            (190, 'refunded', 'tb_transacaofinanceira', 'id_statustransacao', 'transação paga (autorizada e capturada).'),
            (191, 'waiting_payment', 'tb_transacaofinanceira', 'id_statustransacao', 'transação estornada.'),
            (192, 'pending_refund', 'tb_transacaofinanceira', 'id_statustransacao', 'transação paga com boleto aguardando para ser estornada.'),
            (193, 'refused', 'tb_transacaofinanceira', 'id_statustransacao', 'transação não autorizada.'),
    (194, 'chargedback', 'tb_transacaofinanceira', 'id_statustransacao', 'transação sofreu chargeback.')
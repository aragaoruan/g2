-- Criando coluna que vai servir para mostrar ou não o botão minhas provas

ALTER TABLE tb_configuracaoentidade ADD bl_minhasprovas BIT DEFAULT 0 NULL
EXEC sp_addextendedproperty 'MS_Description', 'Coluna responsavel por controlar qual entidade vai exibir o botao minhas provas.', 'SCHEMA', 'dbo', 'TABLE', 'tb_configuracaoentidade', 'COLUMN', 'bl_minhasprovas';

--Seta o valor 1 por default na entidade 14
UPDATE tb_configuracaoentidade SET tb_configuracaoentidade.bl_minhasprovas = 1 WHERE id_entidade = 14;
--Criando o item de configuração para utilizar as mensagens padrão da entidade mãe


INSERT INTO tb_itemconfiguracao
(id_itemconfiguracao,st_itemconfiguracao, st_descricao, st_default)
VALUES
  (33, 'Utilizar Mensagem Padrão da Entidade Mãe', 'Utilizar Mensagem Padrão da Entidade Mãe', 1);


INSERT INTO tb_esquemaconfiguracaoitem
(id_esquemaconfiguracao, id_itemconfiguracao, st_valor)
VALUES
(1,33,1);

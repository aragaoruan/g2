-- Cria a tabela de atividade complementar
-- Cria a tabela de atividade complementar
CREATE TABLE [dbo].[tb_atividadecomplementar] (
  [id_atividadecomplementar] INT NOT NULL IDENTITY (1, 1),
  [st_tituloatividade] VARCHAR(100) NOT NULL,
  [id_tipoatividade] INT NOT NULL,
  [st_resumoatividade] VARCHAR(300) NULL,
  [id_upload] INT NOT NULL,
  [st_caminhoarquivo] VARCHAR(200) NOT NULL,
  [nu_horasconvalidada] FLOAT NULL,
  [id_situacao] INT NOT NULL,
  [dt_cadastro] DATETIME2 NOT NULL DEFAULT getDate(),
  [bl_ativo] BIT NOT NULL DEFAULT 1,
  [id_matricula] INT NOT NULL,
  [id_entidadecadastro] INT NOT NULL,
  [dt_analisado] DATETIME2 NULL,
  [id_usuarioanalise] INT NULL,
  PRIMARY KEY ([id_atividadecomplementar]),
  CONSTRAINT [FK_ATIVIDADE_TIPOATIVIDADE] FOREIGN KEY ([id_tipoatividade]) REFERENCES [dbo].[tb_tipoatividadecomplementar] (
    [id_tipoatividadecomplementar]
  )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT [FK_ATIVIDADE_UPLOAD] FOREIGN KEY ([id_upload]) REFERENCES [dbo].[tb_upload] ([id_upload])
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT [FK_ATIVIDADE_SITUACAO] FOREIGN KEY ([id_situacao]) REFERENCES [dbo].[tb_situacao] ([id_situacao])
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT [FK_ATIVIDADE_MATRICULA] FOREIGN KEY ([id_matricula]) REFERENCES [dbo].[tb_matricula] ([id_matricula])
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT [FK_ATIVIDADE_ENTIDADECAD] FOREIGN KEY ([id_entidadecadastro]) REFERENCES [dbo].[tb_entidade] ([id_entidade])
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT [FK_ATIVIDADE_USUARIOANALISE] FOREIGN KEY ([id_usuarioanalise]) REFERENCES [dbo].[tb_usuario] ([id_usuario])
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

-- insert da funcionalidade
SET IDENTITY_INSERT tb_funcionalidade ON;
INSERT INTO tb_funcionalidade (
  id_funcionalidade,
  st_funcionalidade,
  id_funcionalidadepai,
  bl_ativo,
  id_situacao,
  st_classeflex,
  st_urlicone,
  id_tipofuncionalidade,
  nu_ordem,
  st_classeflexbotao,
  bl_pesquisa,
  bl_lista,
  id_sistema,
  st_ajuda,
  st_target,
  st_urlcaminho,
  bl_visivel,
  bl_relatorio,
  st_urlcaminhoedicao,
  bl_delete
)
VALUES
  (
    801,
    'Atividades Complementares',
    313,
    1,
    123,
    '/atividade-complementar',
    '/imagens/icones/meusdados.png',
    2,
    0,
    'ajax',
    1,
    1,
    4,
    NULL,
    NULL,
    '801',
    1,
    0,
    NULL,
    0
  );
SET IDENTITY_INSERT tb_funcionalidade OFF;

--INSERT das situações
SET IDENTITY_INSERT tb_situacao ON;
INSERT INTO [dbo].[tb_situacao] (
  [id_situacao],
  [st_situacao],
  [st_tabela],
  [st_campo],
  [st_descricaosituacao]
)
VALUES
  (
    200,
    'Deferido',
    'tb_atividadecomplementar',
    'id_situacao',
    'Atividade complementar aceita'
  ),
  (
    201,
    'Indeferido',
    'tb_atividadecomplementar',
    'id_situacao',
    'Atividade complementar não aceita'
  ),
  (
    202,
    'Em Análise',
    'tb_atividadecomplementar',
    'id_situacao',
    'Atividade complementar não aceita'
  );
SET IDENTITY_INSERT tb_situacao OFF;







/**********************************************************************
* ADD coluna nu_semestre tb_projetopedagogico
*              GII-7048
**********************************************************************/

BEGIN TRANSACTION

ALTER TABLE tb_projetopedagogico
ADD nu_semestre INT NULL


ALTER TABLE tb_modulodisciplina
ADD nu_obrigatorioalocacao INT NULL,
nu_disponivelapartirdo INT NULL,
nu_percentualsemestre numeric(5, 2) NULL

COMMIT



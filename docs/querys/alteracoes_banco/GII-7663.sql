/**
** Alteração para GII-7663
** Inserindo novo tipo de trámite da matricula
**/

BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_tipotramite on

INSERT INTO tb_tipotramite
( [id_tipotramite],
  [id_categoriatramite],
  [st_tipotramite])
  
  VALUES(
  28,
  1,
  'Trancamento de disciplina'
  ) 

 SET IDENTITY_INSERT dbo.tb_tipotramite off

 COMMIT
--Incluindo a coluna id_sistema, que irá servir como uma forma
--de saber a origem que teve a avaliação
--Exemplo:  Se veio do gerenciar-nota/G2,  se veio do Portal do Aluno...
ALTER TABLE tb_avaliacaoaluno
  ADD id_sistema INT NULL

ALTER TABLE tb_avaliacaoaluno
  ADD CONSTRAINT FK_TB_AVALI_REFERENCE_TB_SISTEMA
FOREIGN KEY (id_sistema) REFERENCES tb_sistema (id_sistema)
-- GII-7183

SET IDENTITY_INSERT tb_permissao ON;
INSERT INTO tb_permissao (id_permissao, st_nomepermissao, st_descricao, bl_ativo) VALUES (40, 'Alterar Núcleo Telemarketing', 'Permitir alterar o Núcleo de Telemarketing da Negociação - Graduação', 1);
INSERT INTO tb_permissao (id_permissao, st_nomepermissao, st_descricao, bl_ativo) VALUES (41, 'Alterar Vendedor', 'Permitir alterar o Vendedor da Negociação - Graduação', 1);
SET IDENTITY_INSERT tb_permissao OFF;

INSERT INTO tb_permissaofuncionalidade (id_funcionalidade, id_permissao) VALUES (760, 40);
INSERT INTO tb_permissaofuncionalidade (id_funcionalidade, id_permissao) VALUES (760, 41);
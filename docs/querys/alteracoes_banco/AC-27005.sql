
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [rel].[vw_matricula]
AS
    SELECT DISTINCT
            mt.id_matricula ,
            mt.dt_inicio ,
            ct.id_contrato ,
            us.st_nomecompleto ,
            us.st_cpf ,
            p.st_email ,
            CAST(p.nu_ddd AS VARCHAR) + '-' + CAST(p.nu_telefone AS VARCHAR) AS st_telefone ,
            ac.id_areaconhecimento ,
            ac.st_areaconhecimento ,
            pp.id_projetopedagogico ,
            pp.st_projetopedagogico ,
            st.id_situacao ,
            st.st_situacao ,
            ev.id_evolucao ,
            ev.st_evolucao ,
            usr.st_nomecompleto AS st_representante ,
            usa.st_nomecompleto AS st_atendente ,
            vp.nu_valorliquido AS nu_valor ,
            mt.id_entidadeatendimento ,
            p.sg_uf ,
            tm.st_turma, tm.id_turma,
            tm.dt_inicio AS dt_inicioturma,
            tm.dt_fim AS dt_fimturma,
            CAST(mt.dt_cadastro AS DATE) AS dt_cadastro,
            CAST(mt.dt_concluinte AS DATE) AS dt_concluinte,
            CAST (cmt.dt_cadastro AS DATE) AS dt_certificacao,
                        vp.nu_valorliquido AS nu_valorliquido ,
            vp.nu_valorbruto AS nu_valorproduto ,
            campanha.nu_valordesconto AS nu_valordesconto
    FROM    dbo.tb_matricula mt
            JOIN dbo.tb_usuario AS us ON us.id_usuario = mt.id_usuario
            INNER JOIN vw_pessoa p ON p.id_entidade = mt.id_entidadeatendimento
                                      AND p.id_usuario = mt.id_usuario
            JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
            JOIN dbo.tb_areaprojetopedagogico AS ap ON ap.id_projetopedagogico = pp.id_projetopedagogico
            OUTER APPLY ( SELECT TOP 1
                                    id_areaconhecimento ,
                                    st_areaconhecimento
                          FROM      dbo.tb_areaconhecimento
                          WHERE     id_areaconhecimento = ap.id_areaconhecimento
                                    AND id_tipoareaconhecimento = 1
                        ) AS ac
            JOIN dbo.tb_contratomatricula AS cm ON cm.id_matricula = mt.id_matricula
                                                   AND cm.bl_ativo = 1
            JOIN dbo.tb_contrato AS ct ON ct.id_contrato = cm.id_contrato
                                          AND ct.bl_ativo = 1
            JOIN dbo.tb_venda AS vd ON ct.id_venda = vd.id_venda
            JOIN dbo.tb_vendaproduto AS vp ON vp.id_venda = vd.id_venda
            LEFT JOIN dbo.tb_atendentevenda AS av ON av.id_venda = vd.id_venda
            LEFT JOIN dbo.tb_usuario AS usa ON usa.id_usuario = av.id_usuario
            LEFT JOIN dbo.tb_vendaenvolvido AS ve ON ve.id_venda = vd.id_venda
                                                     AND ve.id_funcao = 1
            LEFT JOIN dbo.tb_usuario AS usr ON usr.id_usuario = ve.id_usuario
            JOIN dbo.tb_situacao AS st ON st.id_situacao = mt.id_situacao
            JOIN dbo.tb_evolucao AS ev ON ev.id_evolucao = mt.id_evolucao
            LEFT JOIN dbo.tb_turma AS tm ON tm.id_turma = mt.id_turma
            LEFT JOIN tb_matriculacertificacao AS cmt ON cmt.id_matricula=mt.id_matricula
            LEFT JOIN dbo.tb_campanhacomercial as campanha ON campanha.id_campanhacomercial = vp.id_campanhacomercial
    WHERE   mt.bl_ativo = 1
           -- AND mt.bl_institucional = 0


GO



-- GII-7829
ALTER TABLE tb_matriculadiplomacao
  ADD st_diplomanumero VARCHAR(10) NULL,
  st_diplomalivro VARCHAR(10) NULL,
  st_diplomafolha VARCHAR(10) NULL;
SET IDENTITY_INSERT tb_permissao ON;
INSERT INTO tb_permissao (id_permissao, st_nomepermissao, st_descricao, bl_ativo) VALUES
  (50, 'Editar Nº do Registro do Diploma',
   'Permite alterar o número do Registro do Diploma na funcionalidade de Diplomação', 1);
SET IDENTITY_INSERT tb_permissao OFF;
INSERT INTO tb_permissaofuncionalidade (id_funcionalidade, id_permissao) VALUES (787, 50);
ALTER TABLE [dbo].[tb_venda]
  ADD [utm_source] VARCHAR(120) NULL
GO
IF ((SELECT
       COUNT(*)
     FROM fn_listextendedproperty('MS_Description',
                                  'SCHEMA', N'dbo',
                                  'TABLE', N'tb_venda',
                                  'COLUMN', N'utm_source')) > 0)
  EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Mecanismo de busca.'
  , @level0type = 'SCHEMA', @level0name = N'dbo'
  , @level1type = 'TABLE', @level1name = N'tb_venda'
  , @level2type = 'COLUMN', @level2name = N'utm_source'
ELSE
  EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Mecanismo de busca.'
  , @level0type = 'SCHEMA', @level0name = N'dbo'
  , @level1type = 'TABLE', @level1name = N'tb_venda'
  , @level2type = 'COLUMN', @level2name = N'utm_source'
GO
ALTER TABLE [dbo].[tb_venda]
  ADD [utm_medium] VARCHAR(120) NULL
GO
IF ((SELECT
       COUNT(*)
     FROM fn_listextendedproperty('MS_Description',
                                  'SCHEMA', N'dbo',
                                  'TABLE', N'tb_venda',
                                  'COLUMN', N'utm_medium')) > 0)
  EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Para identificar um meio, como e-mail ou cpc.'
  , @level0type = 'SCHEMA', @level0name = N'dbo'
  , @level1type = 'TABLE', @level1name = N'tb_venda'
  , @level2type = 'COLUMN', @level2name = N'utm_medium'
ELSE
  EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Para identificar um meio, como e-mail ou cpc.'
  , @level0type = 'SCHEMA', @level0name = N'dbo'
  , @level1type = 'TABLE', @level1name = N'tb_venda'
  , @level2type = 'COLUMN', @level2name = N'utm_medium'
GO
ALTER TABLE [dbo].[tb_venda]
  ADD [utm_campaign] VARCHAR(120) NULL
GO
IF ((SELECT
       COUNT(*)
     FROM fn_listextendedproperty('MS_Description',
                                  'SCHEMA', N'dbo',
                                  'TABLE', N'tb_venda',
                                  'COLUMN', N'utm_campaign')) > 0)
  EXEC sp_updateextendedproperty @name = N'MS_Description',
                                 @value = N'Usado para análise de palavras-chave. Use para identificar um produto específico ou uma campanha estratégica'
  , @level0type = 'SCHEMA', @level0name = N'dbo'
  , @level1type = 'TABLE', @level1name = N'tb_venda'
  , @level2type = 'COLUMN', @level2name = N'utm_campaign'
ELSE
  EXEC sp_addextendedproperty @name = N'MS_Description',
                              @value = N'Usado para análise de palavras-chave. Use para identificar um produto específico ou uma campanha estratégica'
  , @level0type = 'SCHEMA', @level0name = N'dbo'
  , @level1type = 'TABLE', @level1name = N'tb_venda'
  , @level2type = 'COLUMN', @level2name = N'utm_campaign'
GO
ALTER TABLE [dbo].[tb_venda]
  ADD [utm_term] VARCHAR(120) NULL
GO
IF ((SELECT
       COUNT(*)
     FROM fn_listextendedproperty('MS_Description',
                                  'SCHEMA', N'dbo',
                                  'TABLE', N'tb_venda',
                                  'COLUMN', N'utm_term')) > 0)
  EXEC sp_updateextendedproperty @name = N'MS_Description',
                                 @value = N'Usado para pesquisa paga. Use para anotar as palavras-chave desse anúncio.'
  , @level0type = 'SCHEMA', @level0name = N'dbo'
  , @level1type = 'TABLE', @level1name = N'tb_venda'
  , @level2type = 'COLUMN', @level2name = N'utm_term'
ELSE
  EXEC sp_addextendedproperty @name = N'MS_Description',
                              @value = N'Usado para pesquisa paga. Use para anotar as palavras-chave desse anúncio.'
  , @level0type = 'SCHEMA', @level0name = N'dbo'
  , @level1type = 'TABLE', @level1name = N'tb_venda'
  , @level2type = 'COLUMN', @level2name = N'utm_term'
GO
ALTER TABLE [dbo].[tb_venda]
  ADD [utm_content] VARCHAR(120) NULL
GO
IF ((SELECT
       COUNT(*)
     FROM fn_listextendedproperty('MS_Description',
                                  'SCHEMA', N'dbo',
                                  'TABLE', N'tb_venda',
                                  'COLUMN', N'utm_content')) > 0)
  EXEC sp_updateextendedproperty @name = N'MS_Description',
                                 @value = N'Use para diferenciar anúncios ou links que apontam para o mesmo URL.'
  , @level0type = 'SCHEMA', @level0name = N'dbo'
  , @level1type = 'TABLE', @level1name = N'tb_venda'
  , @level2type = 'COLUMN', @level2name = N'utm_content'
ELSE
  EXEC sp_addextendedproperty @name = N'MS_Description',
                              @value = N'Use para diferenciar anúncios ou links que apontam para o mesmo URL.'
  , @level0type = 'SCHEMA', @level0name = N'dbo'
  , @level1type = 'TABLE', @level1name = N'tb_venda'
  , @level2type = 'COLUMN', @level2name = N'utm_content'
GO
ALTER TABLE [dbo].[tb_venda]
  ADD [origem_organica] VARCHAR(120) NULL
GO
IF ((SELECT
       COUNT(*)
     FROM fn_listextendedproperty('MS_Description',
                                  'SCHEMA', N'dbo',
                                  'TABLE', N'tb_venda',
                                  'COLUMN', N'origem_organica')) > 0)
  EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Salva uma string qualquer'
  , @level0type = 'SCHEMA', @level0name = N'dbo'
  , @level1type = 'TABLE', @level1name = N'tb_venda'
  , @level2type = 'COLUMN', @level2name = N'origem_organica'
ELSE
  EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Salva uma string qualquer'
  , @level0type = 'SCHEMA', @level0name = N'dbo'
  , @level1type = 'TABLE', @level1name = N'tb_venda'
  , @level2type = 'COLUMN', @level2name = N'origem_organica'
GO




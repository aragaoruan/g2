CREATE VIEW [dbo].[vw_nucleopessoaco] AS 
SELECT 
      np.id_nucleopessoaco,
      nc.st_nucleoco,
      np.id_nucleoco,
      ac.id_assuntoco,
      ac.st_assuntoco,
      ac.id_assuntocopai,
      ap.st_assuntoco AS st_assuntocopai,
      np.id_assuntoco AS id_assuntocooriginal,
      nf.id_nucleofinalidade,
      nf.st_nucleofinalidade,
      np.id_usuario,
      np.id_funcao,
      np.id_usuariocadastro,
      np.dt_cadastro,
      np.bl_prioritario, 
      us.st_nomecompleto,
      fc.st_funcao,
      nc.id_entidade,
      nc.id_textonotificacao,
      np.bl_todos
FROM dbo.tb_nucleoco AS nc 
JOIN dbo.tb_nucleoassuntoco AS na ON na.id_nucleoco = nc.id_nucleoco
LEFT JOIN dbo.tb_assuntoco AS ac ON ac.id_assuntoco = na.id_assuntoco 
JOIN dbo.tb_nucleopessoaco AS np ON nc.id_nucleoco = np.id_nucleoco AND (ac.id_assuntoco = np.id_assuntoco OR ac.id_assuntocopai = np.id_assuntoco or np.bl_todos = 1) 
INNER JOIN dbo.tb_usuario AS us ON np.id_usuario = us.id_usuario 
INNER JOIN dbo.tb_funcao  AS fc ON fc.id_funcao = np.id_funcao
LEFT JOIN dbo.tb_assuntoco AS ap ON ap.id_assuntoco = ac.id_assuntocopai
LEFT JOIN dbo.tb_nucleofinalidade AS nf ON nc.id_nucleofinalidade = nf.id_nucleofinalidade
WHERE nc.bl_ativo = 1 
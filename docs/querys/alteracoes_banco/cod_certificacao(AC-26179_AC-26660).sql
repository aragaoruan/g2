
ALTER TABLE dbo.tb_entidade ADD st_siglaentidade VARCHAR(20) NULL


create table tb_matriculacertificacao (
id_indicecertificado int identity(20000,1) primary key,
id_entidade int not null,
id_matricula int not null,
bl_ativo bit not null,
st_siglaentidade varchar(200) not null,
nu_ano int not null)

alter table tb_matricula add st_codcertificacao varchar(200) null


----------------------

USE [G2H_EAD1]
GO

/****** Object:  View [dbo].[vw_matricula]    Script Date: 30/12/2014 20:44:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






ALTER VIEW [dbo].[vw_matricula] AS
SELECT
	mt.id_usuario,
	us.st_nomecompleto,
	us.st_cpf,
	ps.dt_nascimento,
	ps.st_nomepai,
	ps.st_nomemae,
	ps.st_email,
	CAST(ps.nu_ddd AS VARCHAR) + '-' + CAST(ps.nu_telefone AS VARCHAR) AS st_telefone,
	CAST(ps.nu_dddalternativo AS VARCHAR) + '-' + CAST(ps.nu_telefonealternativo AS VARCHAR) AS st_telefonealternativo,
	mt.id_matricula,
	pp.st_projetopedagogico,
	mt.id_situacao,
	st.st_situacao,
	mt.id_evolucao,
	ev.st_evolucao,
	mt.dt_concluinte,
	mt.id_entidadematriz,
	etmz.st_nomeentidade AS st_entidadematriz,
	mt.id_entidadematricula,
	etm.st_nomeentidade AS st_entidadematricula,
	mt.id_entidadeatendimento,
	eta.st_nomeentidade AS st_entidadeatendimento,
	mt.bl_ativo,
	cm.id_contrato,
	pp.id_projetopedagogico,
	ct.dt_termino,
	mt.dt_cadastro,
	mt.dt_inicio,
	tm.id_turma,
	tm.st_turma,
	tm.dt_inicio AS dt_inicioturma,
	mt.bl_institucional,
	tm.dt_fim AS dt_terminoturma,
	mt.id_situacaoagendamento,
	mt.id_vendaproduto,
	mt.dt_termino AS dt_terminomatricula
	,CAST ( RIGHT (master.dbo.fn_varbintohexstr (
					HASHBYTES (
					 'MD5',
					 (
					  CAST (us.id_usuario AS VARCHAR(10)) COLLATE Latin1_General_CI_AI + CAST (
					   (
						CASE
						WHEN mt.id_matricula IS NULL THEN
						 0
						ELSE
						 mt.id_matricula
						END
					   ) AS VARCHAR (15)
					  ) COLLATE Latin1_General_CI_AI + CAST (
					   5 AS VARCHAR (15)
					  ) COLLATE Latin1_General_CI_AI + CAST ((0) AS VARCHAR(15)) COLLATE Latin1_General_CI_AI + us.st_login + us.st_senha + CONVERT (VARCHAR(10), GETDATE(), 103)
					 )
					)
				   ),
				   32
				  ) AS CHAR (32)
				 ) COLLATE Latin1_General_CI_AI AS st_urlacesso
				 ,mt.bl_documentacao, ct.id_venda,eta.st_urlportal,mt.st_codcertificacao
FROM tb_matricula mt
JOIN tb_projetopedagogico AS pp
	ON mt.id_projetopedagogico = pp.id_projetopedagogico
JOIN tb_entidade AS etmz
	ON etmz.id_entidade = mt.id_entidadematriz
JOIN tb_entidade AS etm
	ON etm.id_entidade = mt.id_entidadematricula
JOIN tb_entidade AS eta
	ON eta.id_entidade = mt.id_entidadeatendimento
JOIN tb_situacao AS st
	ON st.id_situacao = mt.id_situacao
JOIN tb_evolucao AS ev
	ON ev.id_evolucao = mt.id_evolucao
JOIN tb_usuario AS us
	ON us.id_usuario = mt.id_usuario
LEFT JOIN tb_contratomatricula AS cm
	ON cm.id_matricula = mt.id_matricula AND cm.bl_ativo = 1
LEFT JOIN vw_pessoa AS ps
	ON mt.id_usuario = ps.id_usuario AND mt.id_entidadeatendimento = ps.id_entidade
LEFT JOIN dbo.tb_contrato AS ct
	ON ct.id_contrato = cm.id_contrato AND ct.bl_ativo = 1
LEFT JOIN tb_turma AS tm
	ON tm.id_turma = mt.id_turma
WHERE mt.bl_ativo = 1









GO







SET IDENTITY_INSERT tb_permissao ON

INSERT tb_permissao (id_permissao, st_nomepermissao, st_descricao, bl_ativo) VALUES(33, 'Alterar Atendente', 'Permitir alterar o atendente da negociação - Graduação', 1);

SET IDENTITY_INSERT tb_permissao OFF

INSERT tb_permissaofuncionalidade (id_funcionalidade, id_permissao) VALUES (760, 33)


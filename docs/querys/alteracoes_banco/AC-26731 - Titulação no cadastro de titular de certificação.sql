
BEGIN TRANSACTION
GO
CREATE TABLE dbo.tb_titulacao
	(
	id_titulacao int NOT NULL IDENTITY (1, 1),
	st_titulacao varchar(200) NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.tb_titulacao ADD CONSTRAINT
	PK_tb_titulacao PRIMARY KEY CLUSTERED 
	(
	id_titulacao
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.tb_titulacao SET (LOCK_ESCALATION = TABLE)
GO
COMMIT




/***
		INSERINDO OS DADOS NA TABELA FRIA

**/

USE [G2H_EAD1]
GO

INSERT INTO [dbo].[tb_titulacao]
           ([st_titulacao])
     VALUES
           ('Especialista')
		   ,('Mestre')
		   ,('Doutor')
GO



/****
		ALTERANDO A TABELA USUARIO E O CADASTRO DE TITULACAO DO USUARIO
		
***/
UPDATE tb_usuario SET st_titulacao = null

ALTER TABLE dbo.tb_usuario DROP 
							COLUMN st_titulacao ;
ALTER TABLE dbo.tb_usuario ADD
	id_titulacao int NULL


	/** ADICIONANDO FK**/

	BEGIN TRANSACTION
GO
ALTER TABLE dbo.tb_titulacao SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.tb_usuario ADD CONSTRAINT
	FK_tb_usuario_tb_titulacao FOREIGN KEY
	(
	id_titulacao
	) REFERENCES dbo.tb_titulacao
	(
	id_titulacao
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tb_usuario SET (LOCK_ESCALATION = TABLE)
GO
COMMIT



/***
		ALTERANDO tb_UsuarioPerfilEntidadeReferencia (ADD CAMPO id_titulacao)

**/

ALTER TABLE dbo.tb_usuarioperfilentidadereferencia ADD
	id_titulacao int NULL



	/** ADICIONANDO FK tb_usuarioperfilentidadereferencia**/

BEGIN TRANSACTION
GO
ALTER TABLE dbo.tb_usuarioperfilentidadereferencia SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.tb_usuarioperfilentidadereferencia ADD CONSTRAINT
	FK_tb_usuarioperfilentidadereferencia_tb_titulacao FOREIGN KEY
	(
	id_titulacao
	) REFERENCES dbo.tb_titulacao
	(
	id_titulacao
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tb_usuarioperfilentidadereferencia SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
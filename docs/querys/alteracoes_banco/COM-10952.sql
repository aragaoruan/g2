-- INSERT FUNCIONALIDADE --
BEGIN TRANSACTION;
SET IDENTITY_INSERT dbo.tb_funcionalidade ON;

INSERT INTO [dbo].[tb_funcionalidade] ([id_funcionalidade],
[st_funcionalidade],
[id_funcionalidadepai],
[bl_ativo],
[id_situacao],
[id_tipofuncionalidade],
[st_classeflexbotao],
[bl_pesquisa],
[bl_lista],
[id_sistema],
[st_urlcaminho],
[bl_visivel],
[bl_relatorio])
	VALUES (646, 'Grade Horaria', 562, 1, 123, 3, 'br.com.ead1.gestor2.view.conteudo.secretaria.cadastrar.GradeHoraria', 1, 0, 1, '/grade-horaria', 1, 0);

GO
SET IDENTITY_INSERT dbo.tb_funcionalidade OFF;
 
COMMIT;




-- Create tb_gradehoraria --
CREATE TABLE [dbo].[tb_gradehoraria] (
  [id_gradehoraria] int NOT NULL IDENTITY(1,1) ,
  [bl_ativo] bit NOT NULL ,
  [st_nomegradehoraria] varchar(100) NOT NULL ,
  [dt_iniciogradehoraria] datetime NOT NULL ,
  [dt_fimgradehoraria] datetime NULL ,
  [dt_cadastro] datetime2 NOT NULL DEFAULT getDate(),
  [id_situacao] int NOT NULL ,
  PRIMARY KEY ([id_gradehoraria]),
  CONSTRAINT [FK_Tb_gradehoraria_Tb_situacao] FOREIGN KEY ([id_situacao]) REFERENCES [dbo].[tb_situacao] ([id_situacao]) ON DELETE NO ACTION ON UPDATE NO ACTION
);

GO

-- Tabela de Situação INSERT das situaçãos --
INSERT INTO [dbo].[tb_situacao]
       ([st_situacao]
       ,[st_tabela]
       ,[st_campo]
       ,[st_descricaosituacao])
 VALUES
       ('Ativo'
       ,'tb_gradehoraria'
       ,'id_situacao'
       ,'Situação para Grade Horaria'),
    ('Inativo'
       ,'tb_gradehoraria'
       ,'id_situacao'
       ,'Situação para Grade Horaria'),
    ('Em Revisão'
       ,'tb_gradehoraria'
       ,'id_situacao'
       ,'Situação para Grade Horaria');



 -- Create da tb_itemgradehoraria --
CREATE TABLE [dbo].[tb_itemgradehoraria] (
[id_itemgradehoraria] int NOT NULL IDENTITY(1,1) ,
[id_gradehoraria] int NOT NULL ,
[nu_quantidadeaula] int NOT NULL ,
[nu_numeroaula] int NOT NULL ,
[bl_ativo] bit NOT NULL ,
[dt_cadastro] datetime2 NOT NULL DEFAULT getDate() ,
[id_unidade] int NOT NULL ,
[id_entidadecadastro] int NOT NULL ,
[id_turno] int NOT NULL ,
[id_localaula] int NOT NULL ,
[id_diasemana] int NOT NULL ,
[id_disciplina] int NOT NULL ,
[id_professor] int NOT NULL ,
PRIMARY KEY ([id_itemgradehoraria]),
CONSTRAINT [FK_Tb_itemgradehoraria_Tb_itemgradehoraria] FOREIGN KEY ([id_gradehoraria]) REFERENCES [dbo].[tb_gradehoraria] ([id_gradehoraria]) ON DELETE NO ACTION ON UPDATE NO ACTION,
CONSTRAINT [FK_Tb_itemgradehoraria_Tb_entidade_Unidade] FOREIGN KEY ([id_unidade]) REFERENCES [dbo].[tb_entidade] ([id_entidade]) ON DELETE NO ACTION ON UPDATE NO ACTION,
CONSTRAINT [FK_Tb_itemgradehoraria_Tb_entidade] FOREIGN KEY ([id_entidadecadastro]) REFERENCES [dbo].[tb_entidade] ([id_entidade]) ON DELETE NO ACTION ON UPDATE NO ACTION,
CONSTRAINT [FK_Tb_itemgradehoraria_Tb_turno] FOREIGN KEY ([id_turno]) REFERENCES [dbo].[tb_turno] ([id_turno]) ON DELETE NO ACTION ON UPDATE NO ACTION,
CONSTRAINT [FK_Tb_itemgradehoraria_Tb_localaula] FOREIGN KEY ([id_localaula]) REFERENCES [dbo].[tb_localaula] ([id_localaula]) ON DELETE NO ACTION ON UPDATE NO ACTION,
CONSTRAINT [FK_Tb_itemgradehoraria_Tb_diasemana] FOREIGN KEY ([id_diasemana]) REFERENCES [dbo].[tb_diasemana] ([id_diasemana]) ON DELETE NO ACTION ON UPDATE NO ACTION,
CONSTRAINT [FK_Tb_itemgradehoraria_Tb_disciplina] FOREIGN KEY ([id_disciplina]) REFERENCES [dbo].[tb_disciplina] ([id_disciplina]) ON DELETE NO ACTION ON UPDATE NO ACTION,
CONSTRAINT [FK_Tb_itemgradehoraria_Tb_usuario] FOREIGN KEY ([id_professor]) REFERENCES [dbo].[tb_usuario] ([id_usuario]) ON DELETE NO ACTION ON UPDATE NO ACTION
)

GO

-- Create tb_itemgradehorariaturma --
CREATE TABLE [dbo].[tb_itemgradehorariaturma] (
[id_itemgradehorariaturma] int NOT NULL IDENTITY(1,1),
[id_itemgradehoraria] int NOT NULL ,
[id_turma] int NOT NULL ,
[bl_ativo] bit NOT NULL ,
[dt_cadastro] datetime2 NOT NULL DEFAULT getDate() ,
PRIMARY KEY ([id_itemgradehorariaturma]),
CONSTRAINT [FK_Tb_itemgradehorariaturma_Tb_itemgradehoraria] FOREIGN KEY ([id_itemgradehoraria]) REFERENCES [dbo].[tb_itemgradehoraria] ([id_itemgradehoraria]) ON DELETE NO ACTION ON UPDATE NO ACTION,
CONSTRAINT [FK_Tb_itemgradehorariaturma_Tb_turma] FOREIGN KEY ([id_turma]) REFERENCES [dbo].[tb_turma] ([id_turma]) ON DELETE NO ACTION ON UPDATE NO ACTION
)

GO


-- Alteração da tabela de grade horaria --
ALTER TABLE [dbo].[tb_gradehoraria]
ADD [id_entidade] int NULL
GO

ALTER TABLE [dbo].[tb_gradehoraria] ADD CONSTRAINT [FK_Tb_gradehoraria_Tb_entidade] FOREIGN KEY ([id_entidade]) REFERENCES [dbo].[tb_entidade] ([id_entidade]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO


USE [G2_UNY]
GO

/****** Object:  View [rel].[vw_ecommerce]    Script Date: 26/03/2015 13:50:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [rel].[vw_ecommerce] AS
SELECT DISTINCT
        st_nomecompleto ,
        st_email ,
        st_cpf ,
        CAST(nu_ddd AS VARCHAR) + CAST(nu_telefone AS VARCHAR) AS st_telefone ,
        st_endereco ,
        st_cep ,
        st_bairro ,
        us.st_complemento ,
        nu_numero ,
        st_cidade ,
        us.sg_uf ,
        pd.st_produto ,
        pd.id_produto ,
		vp.nu_desconto AS nu_descontoproduto,
		vp.nu_valorbruto AS nu_valortabela,
        vp.nu_valorliquido AS nu_valornegociado,
        vd.nu_valorliquido AS nu_valorliquidovenda ,
        vd.dt_confirmacao ,
        mp.st_meiopagamento ,
		mp.id_meiopagamento,
        pd.id_tipoproduto ,
	vd.id_venda,
        vd.id_evolucao ,
		ev.st_evolucao,
        ve.id_entidade,
        vd.dt_cadastro,
        pdc.st_produto AS st_combo,
        pdc.id_produto AS id_produtocombo,
        ve.st_nomeentidade,
        te.id_entidade AS id_entidadepai,
        vd.st_observacao,
        us.id_usuario AS cod_aluno,
		vd.id_cupom,
		-----

		 (SELECT COALESCE(cti.st_categoria + ', ','')
			FROM dbo.tb_categoriaproduto AS cpi
			JOIN dbo.tb_categoria AS cti
			ON cti.id_categoria = cpi.id_categoria
			WHERE cpi.id_produto = pd.id_produto
			FOR XML PATH('')) AS st_categorias,
			cp.id_categoria,
			cu.nu_desconto AS nu_valorcupom, cu.st_codigocupom, td.st_tipodesconto, cc.st_campanhacomercial, lvc.nu_parcelas,
			(SELECT COALESCE(ac2.st_areaconhecimento + ', ','')
			FROM dbo.tb_areaconhecimento ac2
			JOIN dbo.tb_areaprojetopedagogico app2
			JOIN dbo.tb_produtoprojetopedagogico AS ppp ON ppp.id_projetopedagogico = app2.id_projetopedagogico
			ON ac2.id_areaconhecimento = app2.id_areaconhecimento
			WHERE ppp.id_produto = vp.id_produto
			FOR XML PATH('')) AS st_areaconhecimento




FROM    tb_entidade AS te
        JOIN dbo.vw_entidaderecursivaid AS ve ON ve.nu_entidadepai = te.id_entidade OR ve.id_entidade = te.id_entidade
	JOIN dbo.tb_venda AS vd ON vd.id_entidade = ve.id_entidade
	JOIN dbo.tb_evolucao AS ev ON ev.id_evolucao = vd.id_evolucao
        JOIN dbo.vw_pessoa AS us ON us.id_usuario = vd.id_usuario AND us.id_entidade = vd.id_entidade
        LEFT JOIN dbo.tb_municipio AS mn ON mn.id_municipio = us.id_municipio
        JOIN dbo.tb_vendaproduto AS vp ON vd.id_venda = vp.id_venda
        JOIN dbo.tb_produto AS pd ON pd.id_produto = vp.id_produto
		LEFT JOIN tb_cupom AS cu ON cu.id_cupom = vd.id_cupom
		LEFT JOIN dbo.tb_campanhacomercial AS cc ON cu.id_campanhacomercial = cc.id_campanhacomercial
		LEFT JOIN dbo.tb_tipodesconto AS td ON td.id_tipodesconto = cu.id_tipodesconto
		LEFT JOIN dbo.tb_categoriaproduto AS cp ON cp.id_produto = pd.id_produto
        LEFT JOIN dbo.tb_lancamentovenda AS lv ON lv.id_venda = vd.id_venda AND bl_entrada = 1
        LEFT JOIN dbo.tb_lancamento AS lc ON lc.id_lancamento = lv.id_lancamento
        LEFT JOIN dbo.tb_meiopagamento AS mp ON mp.id_meiopagamento = lc.id_meiopagamento
	LEFT JOIN tb_produto AS pdc ON pdc.id_produto =vp.id_produtocombo
	OUTER APPLY (SELECT COUNT(lvo.id_lancamento) AS nu_parcelas FROM dbo.tb_lancamentovenda AS lvo WHERE lvo.id_venda = vd.id_venda) AS lvc
	WHERE vd.bl_ativo = 1






GO



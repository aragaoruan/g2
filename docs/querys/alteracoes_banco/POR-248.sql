ALTER TABLE tb_assuntoco ADD
  bl_interno BIT DEFAULT 0,
  bl_recuperacao_resgate BIT DEFAULT 0;

UPDATE tb_assuntoco SET bl_interno = 0, bl_recuperacao_resgate = 0;

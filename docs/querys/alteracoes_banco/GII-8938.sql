ALTER TABLE tb_cancelamento ADD dt_referencia DATETIME NULL
GO

ALTER TABLE tb_cancelamento ADD dt_encaminhamentofinanceiro DATETIME NULL
GO

ALTER TABLE tb_cancelamento ADD dt_desistiu DATETIME NULL
GO


SET IDENTITY_INSERT tb_funcionalidade ON;

INSERT INTO tb_funcionalidade (
id_funcionalidade,
st_funcionalidade,
id_funcionalidadepai,
bl_ativo,
id_situacao,
id_tipofuncionalidade,
bl_pesquisa,
bl_lista,
id_sistema,
st_urlcaminho,
bl_visivel,
bl_relatorio,
bl_delete)
VALUES (
830,
'Cancelamento Pós-Graduação',
488,
1,
123,
3,
0,
0,
1,
'/rel-cancelamento-pos-graduacao',
1,
1,
0);
SET IDENTITY_INSERT tb_funcionalidade OFF;
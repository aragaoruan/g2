/**
Incluindo variaveis do sistema
Demanda GII-8553
**/

SET IDENTITY_INSERT dbo.tb_textovariaveis ON

INSERT INTO tb_textovariaveis
(id_textovariaveis, id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
VALUES
  (757, 1, 'st_disciplina', '#nome_disciplina#', NULL, 1), --id era 756
  (758, 1, 'dt_abertura', '#dt_inicio_sala#', 'convertDataToBr', 1), --id era 757
  (759, 1, 'dt_encerramento', '#dt_termino_sala#', 'convertDataToBr', 1) --id era 758

SET IDENTITY_INSERT dbo.tb_textovariaveis OFF


INSERT INTO tb_mensagempadrao
(id_mensagempadrao, id_tipoenvio, st_mensagempadrao, st_default, id_textocategoria)
VALUES (49, 3, 'Alocação PRR', NULL, 1);

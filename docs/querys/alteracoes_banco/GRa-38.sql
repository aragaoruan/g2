CREATE TRIGGER tg_datas_sala_graduacao
ON tb_periodoletivo
FOR UPDATE
AS
BEGIN

    DECLARE
        @ID_PERIODOLETIVO INT,
        @DT_INICIOINSCRICAO DATETIME,
        @DT_FIMINSCRICAO DATETIME,
        @DT_ABERTURA DATETIME,
        @DT_ENCERRAMENTO DATETIME

    SELECT
        @ID_PERIODOLETIVO = id_periodoletivo,
        @DT_INICIOINSCRICAO = dt_inicioinscricao,
        @DT_FIMINSCRICAO = dt_fiminscricao,
        @DT_ABERTURA = dt_abertura,
        @DT_ENCERRAMENTO = dt_encerramento
    FROM INSERTED

    UPDATE tb_saladeaula

    SET
        dt_inicioinscricao = @DT_INICIOINSCRICAO,
        dt_fiminscricao = @DT_FIMINSCRICAO,
        dt_abertura = @DT_ABERTURA,
        dt_encerramento = @DT_ENCERRAMENTO

    WHERE id_saladeaula IN (
        SELECT DISTINCT

        sa.id_saladeaula

        FROM tb_saladeaula AS sa
        JOIN tb_entidade AS en ON en.id_entidade = sa.id_entidade
        LEFT JOIN tb_saladeaulaentidade SE ON se.id_entidade = en.id_entidade AND sa.id_saladeaula = se.id_entidade
        JOIN tb_esquemaconfiguracao AS ec ON en.id_esquemaconfiguracao = ec.id_esquemaconfiguracao
        JOIN tb_esquemaconfiguracaoitem AS eci ON ec.id_esquemaconfiguracao = eci.id_esquemaconfiguracao
        JOIN tb_itemconfiguracao AS ic ON ic.id_itemconfiguracao = eci.id_itemconfiguracao

        WHERE
            sa.id_periodoletivo = @ID_PERIODOLETIVO
            AND ic.id_itemconfiguracao = 22
            AND st_valor = 2
    )

END
/**
** Data de envio ao polo na diplomação
**/


BEGIN TRANSACTION
SET IDENTITY_INSERT tb_funcionalidade ON
INSERT INTO tb_funcionalidade (id_funcionalidade, st_urlcaminho, id_funcionalidadepai, st_funcionalidade,
                               bl_ativo, id_situacao, id_tipofuncionalidade, bl_pesquisa, bl_lista, bl_visivel, bl_relatorio, bl_delete, id_sistema)
VALUES (
  790,
  '790',
  787,
  'Edição da Data Envio ao polo na Tela Diplomação',
  1,
  123,
  7,
  0,
  0,
  0,
  0,
  0,
  1);
SET IDENTITY_INSERT tb_funcionalidade OFF

SET IDENTITY_INSERT tb_permissao ON
INSERT INTO tb_permissao (id_permissao, st_nomepermissao, st_descricao, bl_ativo) VALUES (
  44,
  'Editar Data Envio ao Polo do Aluno na tela Diplomação',
  'Permite editar a data de envio do diploma ao polo do aluno.',
  1
);
SET IDENTITY_INSERT tb_permissao OFF


INSERT INTO tb_permissaofuncionalidade (id_funcionalidade, id_permissao) VALUES (
  790,
  44
);
IF @@ERROR <> 0
  ROLLBACK
ELSE
  COMMIT



/**
** Data de entregue ao aluno 
**/


BEGIN TRANSACTION
SET IDENTITY_INSERT tb_funcionalidade ON
INSERT INTO tb_funcionalidade (id_funcionalidade, st_urlcaminho, id_funcionalidadepai, st_funcionalidade,
                               bl_ativo, id_situacao, id_tipofuncionalidade, bl_pesquisa, bl_lista, bl_visivel, bl_relatorio, bl_delete, id_sistema)
VALUES (
  791,
  '791',
  787,
  'Edição da Data Entregue ao Aluno na Tela Diplomação',
  1,
  123,
  7,
  0,
  0,
  0,
  0,
  0,
  1);
SET IDENTITY_INSERT tb_funcionalidade OFF

SET IDENTITY_INSERT tb_permissao ON
INSERT INTO tb_permissao (id_permissao, st_nomepermissao, st_descricao, bl_ativo) VALUES (
  45,
  'Editar Data Entregue ao Aluno na tela Diplomação',
  'Permite editar a data que o diploma foi entregue ao aluno.',
  1
);
SET IDENTITY_INSERT tb_permissao OFF


INSERT INTO tb_permissaofuncionalidade (id_funcionalidade, id_permissao) VALUES (
  791,
  45
);
IF @@ERROR <> 0
  ROLLBACK
ELSE
  COMMIT
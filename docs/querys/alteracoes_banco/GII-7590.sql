CREATE NONCLUSTERED INDEX idx_sistema_entidade
  ON [dbo].[tb_usuariointegracao] ([id_sistema], [id_entidade])
INCLUDE ([id_usuario])

CREATE NONCLUSTERED INDEX [idx_blativo_tiponota]
  ON [dbo].[tb_avaliacaoaluno] ([bl_ativo], [id_tiponota])
INCLUDE ([id_avaliacaoconjuntoreferencia])

CREATE NONCLUSTERED INDEX [<Name of Missing Index, sysname,>]
  ON [dbo].[tb_alocacaointegracao] ([bl_encerrado])
INCLUDE ([id_alocacao])
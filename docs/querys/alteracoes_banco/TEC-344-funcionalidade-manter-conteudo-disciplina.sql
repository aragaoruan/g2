
CREATE table tb_formatoarquivo (
id_formatoarquivo int primary key,
st_formatoarquivo varchar(30),
st_extensaoarquivo varchar(10)
);

--Insert de tipos inicial para funcionalidade
INSERT INTO tb_formatoarquivo (id_formatoarquivo, st_formatoarquivo, st_extensaoarquivo)
VALUES (1, 'PDF', '.pdf'), (2, 'ZIP', '.zip');

create table tb_disciplinaconteudo (
id_disciplinaconteudo int primary key identity(1,1),
id_disciplina int,
st_descricaoconteudo varchar(100),
id_formatoarquivo int,
id_usuariocadastro int,
bl_ativo bit default 1,
dt_cadastro datetime default getdate(),
constraint FK_TB_DISCIPLINA_CONTEUDO_TB_DISCIPLINA foreign key (id_disciplina) references tb_disciplina(id_disciplina) on update cascade on delete n
o action,
	constraint FK_TB_DISCIPLINA_CONTEUDO_TB_USUARIO foreign key (id_usuariocadastro) references tb_usuario(id_usuario) on update no action on delete no action,
	constraint FK_TB_DISCIPLINA_CONTEUDO_TB_FORMATOARQUIVO foreign key (id_formatoarquivo) references tb_formatoarquivo(id_formatoarquivo) on update no action on delete no action
);
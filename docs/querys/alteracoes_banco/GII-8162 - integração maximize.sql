/**
	Alterações para integraçãoo com o sistema de provas MAXIMIZE
	GII-8162
**/
BEGIN TRANSACTION

ALTER TABLE tb_projetopedagogico
  ADD st_codprovaintegracao VARCHAR(30) NULL,
  st_provaintegracao VARCHAR(255) NULL

ALTER TABLE tb_disciplina
  ADD st_codprovaintegracao VARCHAR(30) NULL,
  st_codprovarecuperacaointegracao VARCHAR(30) NULL,
  st_provaintegracao VARCHAR(255) NULL,
  st_provarecuperacaointegracao VARCHAR(255) NULL

ALTER TABLE tb_aplicadorprova
  ADD dt_sincronizado DATETIME2 NULL,
  bl_sincronizado BIT NOT NULL DEFAULT 0

ALTER TABLE tb_avaliacaoagendamento
  ADD dt_sincronizado DATETIME2 NULL,
  bl_sincronizado BIT NOT NULL DEFAULT 0

/***
  Adiciona novo sistema
  SCRIPT PARA HOMOLOGACAO E RELEASE
  O SCRIPT ABAIXO DEVE SER RODADO APENAS PARA AMBIENTES DE DESENVOLVIMENTO E HOMOLOGACAO
*/

SET IDENTITY_INSERT dbo.tb_sistema ON
INSERT INTO
  tb_sistema
  (id_sistema, st_sistema, bl_ativo, st_conexao, st_chaveacesso)
VALUES
  (32, 'Maximize', 1, 'https://facunyleyah.fabricadeprovas.com.br', NULL)
SET IDENTITY_INSERT dbo.tb_sistema OFF


COMMIT


/***
  ***************** PRODUCAO *********************************************
  O scrip abaixo deve ser executado APENAS para o ambiente de *** PRODUCAO****
*/

SET IDENTITY_INSERT dbo.tb_sistema ON
INSERT INTO
  tb_sistema
  (id_sistema, st_sistema, bl_ativo, st_conexao, st_chaveacesso)
VALUES
  (32, 'Maximize', 1, 'https://unyleya.fabricadeprovas.com.br', NULL)
SET IDENTITY_INSERT dbo.tb_sistema OFF


COMMIT


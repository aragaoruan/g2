/**
Altera��o na tabela de matricula add o campo bl_academico
**/

ALTER TABLE tb_matricula
ADD bl_academico bit NOT NULL DEFAULT(0)



	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Marca se o aluno est� apto, academicamente a ser certificado' 
	, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE'
	,@level1name=N'tb_matricula', @level2type=N'COLUMN',@level2name=N'bl_academico'
GO
CREATE TABLE [dbo].[tb_enviocobranca](
 [id_enviocobranca] [int] NOT NULL,
 [id_mensagemcobranca] [int] NOT NULL,
 [id_mensagem] [int] NOT NULL,
 [id_lancamento] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
 [id_enviocobranca] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]



--VW_avisoboleto
ALTER VIEW [dbo].[vw_avisoboleto] as
SELECT mc.id_mensagemcobranca ,
        lc.id_lancamento ,
        mc.id_textosistema ,
        mc.id_entidade
FROM    dbo.tb_lancamento AS lc
        JOIN dbo.tb_lancamentovenda AS lv 
        ON lc.id_lancamento = lv.id_lancamento
		JOIN dbo.tb_mensagemcobranca AS mc
		ON lc.id_entidade = mc.id_entidade
WHERE CAST(GETDATE() AS DATE) = DATEADD(DAY,CASE
			WHEN mc.bl_antecedencia = 1 THEN -1
		ELSE 1
			END * mc.nu_diasatraso, 
			lc.dt_vencimento)
            AND lc.id_lancamento NOT IN (
            SELECT  id_lancamento
            FROM    dbo.tb_enviocobranca AS ec
            WHERE   ec.id_lancamento = lc.id_lancamento
                    AND ec.id_mensagemcobranca = mc.id_mensagemcobranca )
            AND lc.bl_quitado = 0
            AND lc.bl_ativo = 1
            AND lc.id_meiopagamento = 2
                                                                           
                                        
-- Variaveis
INSERT INTO dbo.tb_textovariaveis
        ( id_textocategoria ,
          st_camposubstituir ,
          st_textovariaveis ,
          st_mascara ,
          id_tipotextovariavel
        )
VALUES  ( 6 , -- id_textocategoria - venda
          'id_lancamento' , -- st_camposubstituir - varchar(100)
          '#link_boleto_parcela#' , -- st_textovariaveis - varchar(100)
          'gerarLinkBoletoParcela' , -- st_mascara - varchar(100)
          5  -- id_tipotextovariavel - int
        )
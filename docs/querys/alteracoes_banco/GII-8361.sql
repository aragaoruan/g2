ALTER VIEW [dbo].[vw_saladeaulaprofessorintegracao] AS
SELECT
  DISTINCT
  us.id_usuario,
  us.st_nomecompleto,
  us.st_cpf,
  us.st_login,
  us.st_senha,
  uper.id_entidade,
  e.st_nomeentidade,
  ei.st_codsistema,
  ei.st_caminho,
  usi.st_codusuario,
  usi.st_senhaintegrada,
  usi.st_loginintegrado,
  s.id_saladeaula,
  s.id_tiposaladeaula,
  s.st_saladeaula,
  s.dt_abertura,
  s.id_categoriasala,
  CONVERT(VARCHAR(MAX), s.dt_abertura, 103) AS st_abertura,
    st_encerramento = CASE WHEN s.dt_encerramento IS NULL
    THEN '-'
                      ELSE CONVERT(VARCHAR(MAX), s.dt_encerramento, 103)
                      END,
  s.dt_inicioinscricao,
  s.dt_fiminscricao,
  s.id_situacao AS id_situacaosala,
  ts.st_tiposaladeaula,
  sai.st_codsistemacurso,
  sai.st_codsistemasala,
  sai.st_codsistemareferencia,
  sis.id_sistema,
  perf.id_perfil,
  perf.st_nomeperfil,
  perfped.id_perfilpedagogico,
  perfped.st_perfilpedagogico,
  dis.id_disciplina,
  dis.st_disciplina,
  dis.id_disciplina AS id_areaconhecimento,
  dis.st_disciplina AS st_areaconhecimento,
  dis.id_tipodisciplina,
/*'id_status' =
             CASE
               WHEN s.dt_abertura > CAST(GETDATE() AS DATE) THEN 3
               WHEN s.dt_abertura <= CAST(GETDATE() AS DATE) AND
                 (s.dt_encerramento >= CAST(GETDATE() AS DATE) OR
                 s.dt_encerramento IS NULL) THEN 1
               WHEN s.dt_encerramento < CAST(GETDATE() AS DATE) THEN 2
               ELSE 1
             END,
'st_status' =
             CASE
               WHEN s.dt_abertura > CAST(GETDATE() AS DATE) THEN 'N?o Iniciada'
               WHEN s.dt_abertura <= CAST(GETDATE() AS DATE) AND
                 (s.dt_encerramento >= CAST(GETDATE() AS DATE) OR
                 s.dt_encerramento IS NULL) THEN 'Aberta'
               WHEN s.dt_encerramento < CAST(GETDATE() AS DATE) THEN 'Fechada'
               ELSE 'Erro'
             END*/
    'id_status' = CASE WHEN s.dt_abertura > CAST(GETDATE() AS DATE)
    THEN 3 -- n?o iniciada
                  WHEN (s.dt_encerramento IS NOT NULL)
                       AND ((DATEADD(DAY,
                                     ISNULL(s.nu_diasextensao,
                                            0),
                                     s.dt_encerramento)) < CAST(GETDATE() AS DATE))
                    THEN 2
                  WHEN s.dt_abertura <= CAST(GETDATE() AS DATE)
                       AND (s.dt_encerramento IS NULL
                            OR ((DATEADD(DAY,
                                         ISNULL(s.nu_diasextensao,
                                                0),
                                         s.dt_encerramento)) >= CAST(GETDATE() AS DATE))
                       )
                    THEN 1
                  ELSE 1
                  END,
    'st_status' = CASE WHEN s.dt_abertura > CAST(GETDATE() AS DATE)
    THEN 'N�o Iniciada'
                  WHEN s.dt_abertura <= CAST(GETDATE() AS DATE)
                       AND (s.dt_encerramento IS NULL
                            OR ((DATEADD(DAY,
                                         ISNULL(s.nu_diasextensao,
                                                0),
                                         s.dt_encerramento)) >= CAST(GETDATE() AS DATE))
                       )
                    THEN 'Aberta'
                  WHEN (s.dt_encerramento IS NOT NULL)
                       AND ((DATEADD(DAY,
                                     ISNULL(s.nu_diasextensao,
                                            0),
                                     s.dt_encerramento)) < CAST(GETDATE() AS DATE))
                    THEN 'Fechada'
                  ELSE 'Erro'
                  END
FROM tb_usuario AS us --JOIN tb_pessoa AS pe ON pe.id_usuario = us.id_usuario
  JOIN tb_usuarioperfilentidadereferencia AS uper ON uper.id_usuario = us.id_usuario
                                                     AND uper.bl_ativo = 1
  JOIN tb_perfil AS perf ON perf.id_perfil = uper.id_perfil
                            AND perf.id_perfilpedagogico = 1
  JOIN tb_perfilpedagogico AS perfped ON perfped.id_perfilpedagogico = perf.id_perfilpedagogico
  JOIN tb_saladeaula AS s ON uper.id_saladeaula = s.id_saladeaula
  JOIN tb_tiposaladeaula AS ts ON ts.id_tiposaladeaula = s.id_tiposaladeaula
  JOIN tb_disciplinasaladeaula AS dsa ON dsa.id_saladeaula = uper.id_saladeaula
  JOIN tb_disciplina AS dis ON dis.id_disciplina = dsa.id_disciplina AND dis.id_situacao = 9
  JOIN tb_entidade AS e ON uper.id_entidade = e.id_entidade
  LEFT JOIN tb_saladeaulaintegracao AS sai ON sai.id_saladeaula = s.id_saladeaula
  LEFT JOIN dbo.tb_perfilreferenciaintegracao AS pri ON pri.id_saladeaulaintegracao = sai.id_saladeaulaintegracao
                                                        AND pri.id_perfilreferencia = uper.id_perfilreferencia
  LEFT JOIN tb_entidadeintegracao AS ei ON ei.id_entidade = e.id_entidade
                                           AND sai.id_sistema = ei.id_sistema
  LEFT JOIN tb_usuariointegracao AS usi ON usi.id_usuario = us.id_usuario
                                           AND usi.id_entidade = e.id_entidade
                                           AND usi.id_sistema = sai.id_sistema
  LEFT JOIN tb_sistema AS sis ON sis.id_sistema = sai.id_sistema
                                 AND sis.id_sistema = ei.id_sistema
                                 AND sis.id_sistema = usi.id_sistema
GO

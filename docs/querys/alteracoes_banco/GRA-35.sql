-- GRA-35

BEGIN TRAN;


-- CRIAR TABELA GRUPODISCIPLINA
CREATE TABLE tb_grupodisciplina(
    id_grupodisciplina INT NOT NULL IDENTITY(1,1),
    st_grupodisciplina VARCHAR(100) NOT NULL,
    CONSTRAINT [PK_TB_GRUPODISCIPLINA] PRIMARY KEY CLUSTERED (
	    [id_grupodisciplina] ASC
    )
    WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY];


-- INSERIR REGISTROS NA TABELA CRIADA
SET IDENTITY_INSERT tb_grupodisciplina ON;
INSERT INTO tb_grupodisciplina(id_grupodisciplina, st_grupodisciplina) VALUES
(1, 'D1'),
(2, 'D2'),
(3, 'D3'),
(4, 'D4'),
(5, 'D5');
SET IDENTITY_INSERT tb_grupodisciplina OFF;


--ADICIONAR A FK NA TABELA DISCIPLINA
ALTER TABLE tb_disciplina
ADD id_grupodisciplina INT,
CONSTRAINT TB_DISCIPLINA_TB_GRUPODISCIPLINA FOREIGN KEY (id_grupodisciplina) REFERENCES tb_grupodisciplina(id_grupodisciplina);



COMMIT;
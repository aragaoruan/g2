-- GII-9535
SET IDENTITY_INSERT tb_funcionalidade ON;
INSERT INTO tb_funcionalidade (
  id_funcionalidade,
  st_funcionalidade,
  id_funcionalidadepai,
  bl_ativo,
  id_situacao,
  id_tipofuncionalidade,
  id_sistema,
  st_urlcaminho,
  bl_visivel,
  bl_pesquisa,
  bl_lista,
  bl_relatorio
)
VALUES (844, 'Alunos Matriculados - GRADUAÇÃO', 257, 1, 123, 3, 1, '844', 1, 0, 0, 1)
SET IDENTITY_INSERT tb_funcionalidade OFF;
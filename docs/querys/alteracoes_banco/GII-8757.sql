-- GII-8757
ALTER TABLE tb_matricula
  ADD
  dt_ultimaoferta DATE NULL;
ALTER TABLE tb_venda
  ADD
  dt_ultimaoferta DATE NULL,
  st_errorenovacao VARCHAR(300) NULL;
--
--
--
UPDATE tb_matricula
SET dt_ultimaoferta =
(
  CASE
  WHEN sala.dt_ultimasala > oferta.dt_ultimaoferta
    THEN cast(sala.dt_ultimasala AS DATE)
  ELSE cast(oferta.dt_ultimaoferta AS DATE)
  END
)
FROM
  tb_matricula AS mat
  JOIN tb_usuario AS us ON us.id_usuario = mat.id_usuario
  CROSS APPLY
  (
    SELECT
      0 AS FALSE,
      1 AS TRUE,
      2 AS DESCONTO_PORCENTAGEM,
      6 AS EVOLUCAO_CURSANDO,
      7 AS EVOLUCAO_AGUARDANDO_NEGOCIACAO,
      9 AS EVOLUCAO_AGUARDANDO_RECEBIMENTO,
      10 AS EVOLUCAO_CONFIRMADA,
      17 AS NEGOCIADA,
      22 AS ITEM_CONFIGURACAO_LINHA_NEGOCIO,
      31 AS ITEM_CONFIGURACAO_PARAMETRO_RENOVACAO,
      CONVERT(DATE, GETDATE()) AS TODAY
  ) AS _
  INNER JOIN tb_projetopedagogico AS pro_ped
    ON pro_ped.id_projetopedagogico = mat.id_projetopedagogico
       AND pro_ped.bl_renovar = _.TRUE
  INNER JOIN tb_entidade AS ent
    ON ent.id_entidade = mat.id_entidadematricula
       AND mat.bl_ativo = _.TRUE
       AND mat.id_evolucao = _.EVOLUCAO_CURSANDO
       AND mat.bl_institucional = _.FALSE
  JOIN vw_entidadeesquemaconfiguracao AS esq_conf_ite
    ON esq_conf_ite.id_esquemaconfiguracao = ent.id_esquemaconfiguracao
       AND esq_conf_ite.id_itemconfiguracao = _.ITEM_CONFIGURACAO_LINHA_NEGOCIO
       AND esq_conf_ite.st_valor = 2
  JOIN vw_entidadeesquemaconfiguracao AS econfig
    ON econfig.id_entidade = mat.id_entidadematricula AND
       econfig.id_itemconfiguracao = _.ITEM_CONFIGURACAO_PARAMETRO_RENOVACAO
  INNER JOIN tb_contratomatricula AS con_mat
    ON con_mat.id_matricula = mat.id_matricula
       AND con_mat.bl_ativo = _.TRUE
  INNER JOIN tb_contrato AS con
    ON con.id_contrato = con_mat.id_contrato
       AND con.bl_ativo = _.TRUE
  INNER JOIN tb_venda AS ven
    ON ven.id_venda = con.id_venda
       AND (
         ven.dt_tentativarenovacao IS NULL
         OR ven.dt_tentativarenovacao < _.TODAY
       )
  OUTER APPLY
  (
    SELECT
      TOP 1
      _ven.id_venda,
      _ven.dt_confirmacao
    FROM
      tb_venda _ven
      INNER JOIN tb_vendaproduto _ven_pro
        ON _ven.id_venda = _ven_pro.id_venda
           AND _ven_pro.id_matricula = mat.id_matricula
      INNER JOIN tb_contrato cont
        ON cont.id_venda = _ven.id_venda
           AND cont.bl_ativo = _.TRUE
-- GII-8558 - Buscar campanha por PORCENTAGEM com valor do desconto 100%
      LEFT JOIN tb_campanhacomercial AS _cc
        ON _cc.id_campanhacomercial = _ven_pro.id_campanhacomercial
           AND _cc.id_tipodesconto = _.DESCONTO_PORCENTAGEM
           AND _cc.nu_valordesconto = 100
    WHERE
      _ven.id_evolucao = _.EVOLUCAO_CONFIRMADA
      AND _ven.bl_ativo = _.TRUE
      -- GII-8558 - Não pode possuir campanha com a regra citada acima
      AND _cc.id_campanhacomercial IS NULL
    ORDER BY
      _ven.id_venda DESC
  ) AS venda
  INNER JOIN tb_vendaproduto AS ven_pro
    ON ven_pro.id_vendaproduto = mat.id_vendaproduto
       AND venda.id_venda IS NOT NULL
  OUTER APPLY
  (
    SELECT
      max(sa.dt_encerramento) AS dt_ultimasala
    FROM tb_matriculadisciplina AS md
      JOIN tb_alocacao AS al
        ON al.id_matriculadisciplina = md.id_matriculadisciplina AND al.bl_ativo = 1
      JOIN tb_saladeaula AS sa
        ON sa.id_saladeaula = al.id_saladeaula
    WHERE md.id_matricula = mat.id_matricula
  ) AS sala
  OUTER APPLY
  (
    SELECT
      max(ofertas.dt_encerramento) AS dt_ultimaoferta
    FROM tb_matricula AS mt5
      OUTER APPLY
      (
        SELECT
          TOP 5
          *
        FROM vw_periodoletivoentidade
        WHERE
          dt_abertura >= venda.dt_confirmacao AND id_entidade = mt5.id_entidadematricula AND
          id_tipooferta = 1
        ORDER BY
          dt_abertura ASC
      ) AS ofertas
    WHERE mat.id_matricula = mt5.id_matricula
  ) AS oferta
--
--
--
UPDATE tb_venda
SET dt_ultimaoferta =
(
  CASE
  WHEN sala.dt_ultimasala > oferta.dt_ultimaoferta
    THEN cast(sala.dt_ultimasala AS DATE)
  ELSE cast(oferta.dt_ultimaoferta AS DATE)
  END
)
FROM
  tb_matricula AS mat
  JOIN tb_usuario AS us ON us.id_usuario = mat.id_usuario
  CROSS APPLY
  (
    SELECT
      0 AS FALSE,
      1 AS TRUE,
      2 AS DESCONTO_PORCENTAGEM,
      6 AS EVOLUCAO_CURSANDO,
      7 AS EVOLUCAO_AGUARDANDO_NEGOCIACAO,
      9 AS EVOLUCAO_AGUARDANDO_RECEBIMENTO,
      10 AS EVOLUCAO_CONFIRMADA,
      17 AS NEGOCIADA,
      22 AS ITEM_CONFIGURACAO_LINHA_NEGOCIO,
      31 AS ITEM_CONFIGURACAO_PARAMETRO_RENOVACAO,
      CONVERT(DATE, GETDATE()) AS TODAY
  ) AS _
  INNER JOIN tb_projetopedagogico AS pro_ped
    ON pro_ped.id_projetopedagogico = mat.id_projetopedagogico
       AND pro_ped.bl_renovar = _.TRUE
  INNER JOIN tb_entidade AS ent
    ON ent.id_entidade = mat.id_entidadematricula
       AND mat.bl_ativo = _.TRUE
       AND mat.id_evolucao = _.EVOLUCAO_CURSANDO
       AND mat.bl_institucional = _.FALSE
  JOIN vw_entidadeesquemaconfiguracao AS esq_conf_ite
    ON esq_conf_ite.id_esquemaconfiguracao = ent.id_esquemaconfiguracao
       AND esq_conf_ite.id_itemconfiguracao = _.ITEM_CONFIGURACAO_LINHA_NEGOCIO
       AND esq_conf_ite.st_valor = 2
  JOIN vw_entidadeesquemaconfiguracao AS econfig
    ON econfig.id_entidade = mat.id_entidadematricula AND
       econfig.id_itemconfiguracao = _.ITEM_CONFIGURACAO_PARAMETRO_RENOVACAO
  INNER JOIN tb_contratomatricula AS con_mat
    ON con_mat.id_matricula = mat.id_matricula
       AND con_mat.bl_ativo = _.TRUE
  INNER JOIN tb_contrato AS con
    ON con.id_contrato = con_mat.id_contrato
       AND con.bl_ativo = _.TRUE
  INNER JOIN tb_venda AS ven
    ON ven.id_venda = con.id_venda
       AND (
         ven.dt_tentativarenovacao IS NULL
         OR ven.dt_tentativarenovacao < _.TODAY
       )
  OUTER APPLY
  (
    SELECT
      TOP 1
      _ven.id_venda,
      _ven.dt_confirmacao
    FROM
      tb_venda _ven
      INNER JOIN tb_vendaproduto _ven_pro
        ON _ven.id_venda = _ven_pro.id_venda
           AND _ven_pro.id_matricula = mat.id_matricula
      INNER JOIN tb_contrato cont
        ON cont.id_venda = _ven.id_venda
           AND cont.bl_ativo = _.TRUE
-- GII-8558 - Buscar campanha por PORCENTAGEM com valor do desconto 100%
      LEFT JOIN tb_campanhacomercial AS _cc
        ON _cc.id_campanhacomercial = _ven_pro.id_campanhacomercial
           AND _cc.id_tipodesconto = _.DESCONTO_PORCENTAGEM
           AND _cc.nu_valordesconto = 100
    WHERE
      _ven.id_evolucao = _.EVOLUCAO_CONFIRMADA
      AND _ven.bl_ativo = _.TRUE
      -- GII-8558 - Não pode possuir campanha com a regra citada acima
      AND _cc.id_campanhacomercial IS NULL
    ORDER BY
      _ven.id_venda DESC
  ) AS venda
  INNER JOIN tb_vendaproduto AS ven_pro
    ON ven_pro.id_vendaproduto = mat.id_vendaproduto
       AND venda.id_venda IS NOT NULL
  OUTER APPLY
  (
    SELECT
      max(sa.dt_encerramento) AS dt_ultimasala
    FROM tb_matriculadisciplina AS md
      JOIN tb_alocacao AS al
        ON al.id_matriculadisciplina = md.id_matriculadisciplina AND al.bl_ativo = 1
      JOIN tb_saladeaula AS sa
        ON sa.id_saladeaula = al.id_saladeaula
    WHERE md.id_matricula = mat.id_matricula
  ) AS sala
  OUTER APPLY
  (
    SELECT
      max(ofertas.dt_encerramento) AS dt_ultimaoferta
    FROM tb_matricula AS mt5
      OUTER APPLY
      (
        SELECT
          TOP 5
          *
        FROM vw_periodoletivoentidade
        WHERE
          dt_abertura >= venda.dt_confirmacao AND id_entidade = mt5.id_entidadematricula AND
          id_tipooferta = 1
        ORDER BY
          dt_abertura ASC
      ) AS ofertas
    WHERE mat.id_matricula = mt5.id_matricula
  ) AS oferta
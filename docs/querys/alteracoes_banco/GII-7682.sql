ALTER TABLE tb_pessoa ADD st_zonaeleitoral VARCHAR(3) NULL;
ALTER TABLE tb_pessoa ADD st_secaoeleitoral VARCHAR(4) NULL;
ALTER TABLE tb_pessoa ADD st_municipioeleitoral VARCHAR(100) NULL;
ALTER TABLE tb_pessoa ADD dt_expedicaocertificadoreservista DATETIME NULL;
ALTER TABLE tb_pessoa ADD id_categoriaservicomilitar int NULL;
ALTER TABLE tb_pessoa ADD st_reparticaoexpedidora VARCHAR(100) NULL;

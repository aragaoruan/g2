 /*****
	INSERT na tb_processo para salvar o rob� de importa��o
	de sala de aula do moodle a partir de uma sala de refer�ncia
 ****/
 
 INSERT INTO tb_processo 
		 (id_processo, st_processo, st_classe)
		 VALUES
		 (4, 'Importar Salas Moodle de Sala Refer�ncia' , null)



/**
	Indexes para tabela tb_erro
**/
CREATE NONCLUSTERED INDEX [idx_sistema_processo] ON [dbo].[tb_erro]
(
        [id_sistema] ASC,
        [id_processo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
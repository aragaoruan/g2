ALTER TABLE dbo.tb_vendaproduto ADD dt_entrega DATE NULL
GO

execute sp_addextendedproperty 'MS_Description', 
   'data que o produto deve ser entrgue (ex: sala de prr)',
   'user', 'dbo', 'table', 'tb_vendaproduto', 'column', 'dt_entrega'
go

ALTER TABLE tb_venda ADD id_ocorrencia INT NULL

execute sp_addextendedproperty 'MS_Description', 
   'ocorrencia que gerou a venda',
   'user', 'dbo', 'table', 'tb_venda', 'column', 'id_ocorrencia'
GO

ALTER TABLE dbo.tb_alocacao ADD dt_inicio DATE NOT NULL DEFAULT GETDATE()

GO

execute sp_addextendedproperty 'MS_Description', 
   'Data em que o aluno come�a a acessar a sala',
   'user', 'dbo', 'table', 'tb_alocacao', 'column', 'dt_inicio'
go

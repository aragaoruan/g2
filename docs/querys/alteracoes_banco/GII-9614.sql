--Criando o aplicativo mobile no tb_sistema, para poder rastrear na tabela de log.

SET IDENTITY_INSERT tb_sistema ON
INSERT INTO tb_sistema (id_sistema, st_sistema, bl_ativo, st_conexao, st_chaveacesso)
VALUES (38, 'Aplicativo Mobile', 1, NULL, NULL);
SET IDENTITY_INSERT tb_sistema OFF

-- Criando a coluna na tb_logacesso para fazer o ligamento com a tb_sistema.
-- OBS: Todos os campos desta tabela não tem chave estrangeira. Para manter o padrão, também não foi criado a FK da id_sistema
ALTER TABLE G2_DEV.dbo.tb_logacesso ADD id_sistema INT NULL
EXEC sp_addextendedproperty 'MS_Description', 'tabela para identificar a origem do log', 'SCHEMA', 'dbo', 'TABLE', 'tb_logacesso', 'COLUMN', 'id_sistema';

--Criando coluna para salvar o caminho da url completa. Necessário para suportar as rotas do aplicativo
ALTER TABLE G2_DEV.dbo.tb_logacesso ADD st_urlcompleta VARCHAR(255) NULL
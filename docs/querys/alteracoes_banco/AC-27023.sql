
/****** Object:  View [rel].[vw_cargaconcluinte]    Script Date: 02/07/2015 13:50:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE view [rel].[vw_cargaconcluinte] as

select
 DISTINCT mt.id_usuario
, upper(mt.st_nomecompleto) as st_nomecompleto
, pessoa.dt_nascimento
, mt.id_turma AS id_turma
, mt.st_cpf
, sa.id_categoriasala
, upper(pessoa.st_endereco) as st_endereco
, upper(pessoa.st_bairro) as st_bairro
, pessoa.st_cep
, mp.st_nomemunicipio as st_cidade
, pessoa.sg_uf
, CAST(pessoa.nu_ddd AS VARCHAR) + '-' + CAST( pessoa.nu_telefone AS varchar) as st_telefone
, mt.st_email
, mt.dt_cadastro
, st_sexo = case
        WHEN pessoa.st_sexo like 'F' THEN 'FEMININO'
                 ELSE 'MASCULINO'
        END
, 'C---------' AS separador1
,  mt.id_projetopedagogico
,  UPPER(mt.st_projetopedagogico) as st_projetopedagogico
,  CAST(ppj.nu_cargahoraria as INT)  as nu_cargahoraria
,  CAST(ppj.nu_cargahoraria as VARCHAR(20))  as st_cargahoraria
,  mt.dt_inicioturma AS dt_inicioturma
,  mt.dt_terminoturma AS dt_terminoturma
,  'D---------' AS separador2
,  td.id_disciplina
,  UPPER(td.st_tituloexibicao) as st_disciplina
,  replace((select nu_aprovafinal from tb_matriculadisciplina
              where id_disciplina = td.id_disciplina AND id_matricula=mt.id_matricula)/10,',','.') as nu_notatotal
, replace(CAST((select nu_aprovafinal from tb_matriculadisciplina
              where id_disciplina = td.id_disciplina AND id_matricula=mt.id_matricula)/10 AS DECIMAL(18,1))  ,',','.'  ) as st_notatotal
,  'P---------' AS separador3
, tb_usuario.id_usuario AS id_professor
,  UPPER(tb_usuario.st_nomecompleto) AS st_coordenador
,  UPPER(tbti.st_titulacao) AS st_nivel
,  UPPER(tb_usuario.st_nomecompleto) as st_nomeprofessor
,  mt.id_matricula
,  sa.id_saladeaula AS id_disciplinaturma
,  UPPER(mt.st_evolucao) as st_evolucao
,  tbti.st_titulacao AS st_titulocoordenador
,  td.nu_cargahoraria  AS  st_cargadisciplina
,  mt.dt_concluinte
,  mt.id_entidadematricula
,  (select UPPER(st_tituloavaliacao) from vw_avaliacaoaluno where id_saladeaula=al.id_saladeaula and id_matricula=mt.id_matricula and bl_ativo=1 and id_tipoavaliacao=6) as st_tituloavaliacao
, CASE WHEN mt.id_entidadematricula not in (292,5) THEN 'WPÓS'
                 ELSE mt.st_entidadematricula
END st_entidadematricula
, (pessoa.st_rg  + ' - ' + pessoa.st_orgaoexpeditor) AS st_rg,
mt.st_codcertificacao,
('W' + cast(mt.id_turma as varchar)) as st_periodorealizacao,
convert(varchar, mtc.dt_cadastro, 105) as st_dtemissao
FROM dbo.tb_alocacao al
JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula
JOIN dbo.tb_matriculadisciplina AS md ON md.id_matriculadisciplina = al.id_matriculadisciplina
JOIN dbo.tb_disciplina AS td ON td.id_disciplina = md.id_disciplina
JOIN dbo.vw_matricula AS mt ON mt.id_matricula = md.id_matricula AND mt.bl_ativo = 1
JOIN dbo.vw_pessoa as pessoa on pessoa.id_usuario = mt.id_usuario AND pessoa.id_entidade = mt.id_entidadematricula
LEFT JOIN tb_municipio as mp on mp.id_municipio = pessoa.id_municipio
JOIN tb_projetopedagogico as ppj ON ppj.id_projetopedagogico = mt.id_projetopedagogico
LEFT JOIN vw_saladeaula as vw_sala on vw_sala.id_saladeaula=al.id_saladeaula and vw_sala.id_projetopedagogico=ppj.id_projetopedagogico and vw_sala.bl_ativa=1 and vw_sala.bl_titular=1
/*LEFT JOIN tb_usuarioperfilentidadereferencia as ucoor on ucoor.id_projetopedagogico=mt.id_projetopedagogico and ucoor.bl_titular=1
LEFT JOIN dbo.tb_perfil ON dbo.tb_perfil.id_perfil = ucoor.id_perfil
                                   AND dbo.tb_perfil.st_nomeperfil LIKE '%Coordenador de Projeto Pedagógico%'
LEFT JOIN dbo.tb_usuario ON dbo.tb_usuario.id_usuario = ucoor.id_usuario
LEFT JOIN dbo.tb_titulacao as tbti ON tbti.id_titulacao = tb_usuario.id_titulacao
*/
LEFT JOIN tb_perfil AS pf ON pf.id_entidade = mt.id_entidadematricula
                                         AND pf.id_perfilpedagogico = 9 and pf.bl_ativo = 1
LEFT JOIN tb_usuarioperfilentidadereferencia AS ucoor ON ucoor.id_projetopedagogico = mt.id_projetopedagogico
                                                              AND ucoor.id_disciplina = td.id_disciplina
                                                                                              AND ucoor.id_perfil = pf.id_perfil
                                                              AND ( ( mt.dt_concluinte BETWEEN ucoor.dt_inicio
                                                              AND ucoor.dt_fim ) OR ( mt.dt_concluinte > ucoor.dt_inicio
                                                              AND ucoor.dt_fim IS NULL
                                                              )
                                                              )
                                                              AND ucoor.bl_titular = 1
                                                              AND ucoor.bl_ativo = 1
            LEFT JOIN tb_usuario  ON tb_usuario.id_usuario = ucoor.id_usuario
            LEFT JOIN tb_titulacao tbti ON ucoor.id_titulacao = tbti.id_titulacao
            LEFT JOIN tb_matriculacertificacao mtc on mtc.id_matricula = mt.id_matricula
WHERE al.bl_ativo = 1
and mt.id_evolucao = 15
and sa.id_categoriasala = 1
and td.st_disciplina not like '%AMBIENTAÇÃO%'

GO



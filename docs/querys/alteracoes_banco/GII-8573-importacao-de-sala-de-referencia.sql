BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_funcionalidade ON
INSERT INTO tb_funcionalidade (
  id_funcionalidade,
  st_funcionalidade,
  id_funcionalidadepai,
  bl_ativo,
  id_situacao,
  st_classeflex,
  st_urlicone,
  id_tipofuncionalidade,
  nu_ordem,
  st_classeflexbotao,
  bl_pesquisa,
  bl_lista,
  id_sistema,
  st_ajuda,
  st_target,
  st_urlcaminho,
  bl_visivel,
  bl_relatorio,
  st_urlcaminhoedicao
)
VALUES (
  822,
  'Importação de Sala de Referência',
  301,
  1,
  123,
  'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa',
  null,
  3,
  NULL,
  NULL,
  0,
  0,
  1,
  NULL,
  NULL,
  'importacao-sala-referencia',
  1,
  0,
  NULL
);

GO
SET IDENTITY_INSERT dbo.tb_funcionalidade off
ROLLBACK
COMMIT

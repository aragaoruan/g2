--DESATIVAR IDENTITY TABELA
SET IDENTITY_INSERT tb_funcionalidade ON;
SET IDENTITY_INSERT tb_situacao ON;

INSERT tb_situacao (id_situacao, st_situacao, st_tabela, st_campo, st_descricaosituacao) VALUES(182, 'Novo Portal', 'tb_funcionalidade', 'id_situacao', 'Funcionalidade Novo Portal');

INSERT into tb_funcionalidade
(id_funcionalidade, st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao,
st_classeflex, st_urlicone, id_tipofuncionalidade, nu_ordem, st_classeflexbotao,
bl_pesquisa, bl_lista, id_sistema, st_ajuda, st_target, st_urlcaminho, bl_visivel, bl_relatorio
, st_urlcaminhoedicao, bl_delete)
VALUES
  (764, 'Mensagens', null, 1, 182, 'app.mensagens', 'icon-icon_menu icon-icon_mens', 1, 1, null, 1, 1, 4, null, null, 'app.mensagens', 1, 0, null, 0),
  (765, 'Cursos', null, 2, 182, 'login.selecionar-curso', 'icon-icon_disc', 1, 1, null, 1, 1, 4, null, null, 'login.selecionar-curso', 1, 0, null, 0),
  (766, 'Calendário', null, 3, 182, 'app.calendario', 'icon-icon_disc', 1, 1, null, 1, 1, 4, null, null, 'app.calendario', 1, 0, null, 0),
  (767, 'Financeiro', null, 4, 182, 'app.financeiro', 'icon-icon_financa', 1, 1, null, 1, 1, 4, null, null, 'app.financeiro', 1, 0, null, 0),
  (768, 'Atendimento', null, 5, 182, 'app.chamados', 'icon-icon_atend', 1, 1, null, 1, 1, 4, null, null, 'app.chamados', 1, 0, null, 0);

INSERT tb_entidadefuncionalidade VALUES
  (14, 764, 1, 5, NULL, 0, 1, 1),
  (14, 765, 1, 5, NULL, 0, 1, 2),
  (14, 766, 1, 5, NULL, 0, 1, 3),
  (14, 767, 1, 5, NULL, 0, 1, 4),
  (14, 768, 1, 5, NULL, 0, 1, 5);


INSERT tb_perfilfuncionalidade VALUES
  (75, 764, 1),
  (75, 765, 1),
  (75, 766, 1),
  (75, 767, 1),
  (75, 768, 1);

--ATIVAR IDENTITY TABELA
SET IDENTITY_INSERT tb_funcionalidade OFF;
SET IDENTITY_INSERT tb_situacao OFF;
ALTER TABLE [dbo].[tb_usuariointegracao]
  ADD [id_entidadeintegracao] INT NULL
GO

ALTER TABLE [dbo].[tb_usuariointegracao]
  ADD CONSTRAINT [FK_tb_usuariointegracao_tb_entidadeintegracao]
FOREIGN KEY ([id_entidadeintegracao]) REFERENCES [dbo].[tb_entidadeintegracao] ([id_entidadeintegracao])
  ON UPDATE NO ACTION
GO

CREATE UNIQUE INDEX [NonClusteredIndex-20140424-110128]
  ON [dbo].[tb_usuariointegracao]
  ([id_sistema] ASC, [id_usuario] ASC, [id_entidade] ASC, [id_entidadeintegracao] ASC)
INCLUDE ([st_codusuario], [st_senhaintegrada], [st_loginintegrado])
  WITH (FILLFACTOR = 90, IGNORE_DUP_KEY = ON, DROP_EXISTING = ON)
  ON [PRIMARY]
GO

CREATE UNIQUE INDEX [idx_sistema_usuario_entidade]
  ON [dbo].[tb_usuariointegracao]
  ([id_sistema] ASC, [id_usuario] ASC, [id_entidade] ASC, [id_entidadeintegracao] ASC)
INCLUDE ([st_codusuario], [st_senhaintegrada], [st_loginintegrado], [id_usuariocadastro], [dt_cadastro], [bl_encerrado])
  WITH (FILLFACTOR = 90, IGNORE_DUP_KEY = ON, DROP_EXISTING = ON)
  ON [PRIMARY]
GO

EXEC sp_rename N'[dbo].[tb_usuariointegracao].[idx_sistema_usuario_entidade]',
               N'idx_sistema_usuario_entidade_entidadeintegracao', 'INDEX'
GO


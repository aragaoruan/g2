SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tb_matriculacertificacao](
	[id_indicecertificado] [int] IDENTITY(20000,1) NOT NULL,
	[id_entidade] [int] NOT NULL,
	[id_matricula] [int] NOT NULL,
	[bl_ativo] [bit] NOT NULL,
	[nu_ano] [int] NOT NULL,
	[st_siglaentidade] [varchar](200) NULL,
	[dt_cadastro] [datetime] NULL,
	[dt_enviocertificado] [datetime] NULL,
	[dt_retornocertificadora] [datetime] NULL,
	[dt_envioaluno] [datetime] NULL,
	[st_codigoacompanhamento] [varchar](13) NULL,
	[id_usuarioenviocertificado] [int] NULL,
	[id_usuarioretornocertificadora] [int] NULL,
	[id_usuarioenvioaluno] [int] NULL,
	[id_usuariocadastro] [int] NULL,
PRIMARY KEY CLUSTERED
(
	[id_indicecertificado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

ALTER TABLE [dbo].[tb_matriculacertificacao] ADD  DEFAULT (getdate()) FOR [dt_cadastro]
GO

ALTER TABLE [dbo].[tb_matriculacertificacao] ADD  DEFAULT (NULL) FOR [dt_enviocertificado]
GO

ALTER TABLE [dbo].[tb_matriculacertificacao] ADD  DEFAULT (NULL) FOR [dt_retornocertificadora]
GO

ALTER TABLE [dbo].[tb_matriculacertificacao] ADD  DEFAULT (NULL) FOR [dt_envioaluno]
GO

ALTER TABLE [dbo].[tb_matriculacertificacao]  WITH CHECK ADD  CONSTRAINT [FK_TB_MAT_CERT_USUARIO_CADASTRO_CERT_REF_TB_USUARIO] FOREIGN KEY([id_usuariocadastro])
REFERENCES [dbo].[tb_usuario] ([id_usuario])
GO

ALTER TABLE [dbo].[tb_matriculacertificacao] CHECK CONSTRAINT [FK_TB_MAT_CERT_USUARIO_CADASTRO_CERT_REF_TB_USUARIO]
GO

ALTER TABLE [dbo].[tb_matriculacertificacao]  WITH CHECK ADD  CONSTRAINT [FK_TB_MAT_CERT_USUARIO_ENVIO_ALUNO_REF_TB_USUARIO] FOREIGN KEY([id_usuarioenvioaluno])
REFERENCES [dbo].[tb_usuario] ([id_usuario])
GO

ALTER TABLE [dbo].[tb_matriculacertificacao] CHECK CONSTRAINT [FK_TB_MAT_CERT_USUARIO_ENVIO_ALUNO_REF_TB_USUARIO]
GO

ALTER TABLE [dbo].[tb_matriculacertificacao]  WITH CHECK ADD  CONSTRAINT [FK_TB_MAT_CERT_USUARIO_ENVIO_CERT_REF_TB_USUARIO] FOREIGN KEY([id_usuarioenviocertificado])
REFERENCES [dbo].[tb_usuario] ([id_usuario])
GO

ALTER TABLE [dbo].[tb_matriculacertificacao] CHECK CONSTRAINT [FK_TB_MAT_CERT_USUARIO_ENVIO_CERT_REF_TB_USUARIO]
GO

ALTER TABLE [dbo].[tb_matriculacertificacao]  WITH CHECK ADD  CONSTRAINT [FK_TB_MAT_CERT_USUARIO_RETORNO_CERTIFICADORA_REF_TB_USUARIO] FOREIGN KEY([id_usuarioretornocertificadora])
REFERENCES [dbo].[tb_usuario] ([id_usuario])
GO

ALTER TABLE [dbo].[tb_matriculacertificacao] CHECK CONSTRAINT [FK_TB_MAT_CERT_USUARIO_RETORNO_CERTIFICADORA_REF_TB_USUARIO]
GO

--------------------------------------------------------------------------------------------------------------------------------

ALTER table tb_matricula alter column st_codcertificacao varchar(40)

SET IDENTITY_INSERT tb_evolucao ON
insert into tb_evolucao (id_evolucao, st_evolucao, st_tabela, st_campo)
VALUES
(60, 'Não Apto', 'tb_matricula', 'id_evolucaocertificacao'),
(61, 'Apto a Gerar Certificado', 'tb_matricula', 'id_evolucaocertificacao'),
(62, 'Certificado Gerado', 'tb_matricula', 'id_evolucaocertificacao'),
(63, 'Enviado a Certificadora', 'tb_matricula', 'id_evolucaocertificacao'),
(64, 'Retornado da Certificadora', 'tb_matricula', 'id_evolucaocertificacao'),
(65, 'Enviado ao Aluno', 'tb_matricula', 'id_evolucaocertificacao');
SET IDENTITY_INSERT tb_evolucao OFF

--------------------------------------------------------------------------------------------------------------------------------

alter table tb_matricula
add id_evolucaocertificacao int null default 60;

UPDATE dbo.tb_matricula SET id_evolucaocertificacao = 60 WHERE id_evolucao <> 15
update dbo.tb_matricula set id_evolucaocertificacao = 61 WHERE id_evolucao = 15

alter table tb_matricula
add constraint FK_TB_MATRI_CAMPO_EVOLUCAO_CERT_REFERENCE_TB_EVOLUCAO FOREIGN KEY (id_evolucaocertificacao) REFERENCES tb_evolucao(id_evolucao)


ALTER TABLE dbo.tb_entidadefinanceiro ADD id_textoimpostorenda INT
GO

ALTER TABLE dbo.tb_entidadefinanceiro
   ADD CONSTRAINT FK_TB_ENTIDADEFINANCEIRO_REFERENCE_TB_TEXTOSISTEMA FOREIGN KEY (id_textoimpostorenda)
      REFERENCES dbo.tb_textosistema (id_textosistema)
go
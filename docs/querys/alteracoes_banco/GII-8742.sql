-- GII-8742
--
-- Criar table para os dados de deficiência
CREATE TABLE tb_deficiencia (
  id_deficiencia INT NOT NULL IDENTITY (1, 1) PRIMARY KEY,
  st_deficiencia VARCHAR(60) NOT NULL,
  st_descricao VARCHAR(120) NULL,
  bl_ativo BIT NOT NULL DEFAULT (1)
);
--
--
-- Inserir registros de deficiência
SET IDENTITY_INSERT tb_deficiencia ON;
INSERT INTO tb_deficiencia (id_deficiencia, st_deficiencia, st_descricao) VALUES
  (1, 'Cegueira', NULL),
  (2, 'Baixa visão', NULL),
  (3, 'Surdez', NULL),
  (4, 'Auditiva', NULL),
  (5, 'Física', NULL),
  (6, 'Surdocegueira', NULL),
  (7, 'Múltipla', NULL),
  (8, 'Intelectual', NULL),
  (9, 'Autismo', 'Transtorno global do desenvolvimento'),
  (10, 'Síndrome de Asperger', 'Transtorno global do desenvolvimento'),
  (11, 'Síndrome de RETT', 'Transtorno global do desenvolvimento'),
  (12, 'Transtorno desintegrativo da infância', 'Transtorno global do desenvolvimento'),
  (13, 'Altas habilidades / superdotação', NULL);
SET IDENTITY_INSERT tb_deficiencia OFF;
--
--
-- Criar tabela para relacionar Deficiência com Usuario
CREATE TABLE tb_usuariodeficiencia (
  id_usuariodeficiencia INT NOT NULL IDENTITY (1, 1) PRIMARY KEY,
  id_usuario INT NOT NULL FOREIGN KEY REFERENCES tb_usuario (id_usuario),
  id_deficiencia INT NOT NULL FOREIGN KEY REFERENCES tb_deficiencia (id_deficiencia)
)
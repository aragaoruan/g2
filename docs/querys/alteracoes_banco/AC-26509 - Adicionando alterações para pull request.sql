/******************************************************************************
**   ADICIONANDO NOVO TIPO DE TR�MITE
**
******************************************************************************/
BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_tipotramite on
INSERT INTO [dbo].[tb_tipotramite]
([id_tipotramite] ,
[id_categoriatramite],
[st_tipotramite]
)
VALUES
(16 , 1, 'Documenta��o')

GO
SET IDENTITY_INSERT dbo.tb_tipotramite off
--ROLLBACK
COMMIT
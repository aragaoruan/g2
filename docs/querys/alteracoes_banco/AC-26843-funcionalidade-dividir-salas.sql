
SET identity_insert tb_funcionalidade OFF;

INSERT INTO tb_funcionalidade
(id_funcionalidade,
st_funcionalidade,
id_funcionalidadepai,
bl_ativo,
id_situacao,
st_classeflex,
st_urlicone,
id_tipofuncionalidade,
nu_ordem,
st_classeflexbotao,
bl_pesquisa,
bl_lista,
id_sistema,
st_ajuda,
st_target,
st_urlcaminho,
bl_visivel,
bl_relatorio,
st_urlcaminhoedicao,
bl_delete)
VALUES
(692,
'Dividir Sala',
562,
1,
123,
'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa',
'/img/ico/door.png',
3,
NULL,
'br.com.ead1.gestor2.view.conteudo.pedagogico.cadastrar.CadastrarSalaDeAula',
1,
0,
1,
NULL,
NULL,
'/sala-de-aula/dividir-sala',
1,
1,
NULL,
0);

SET identity_insert tb_funcionalidade ON;

alter table tb_entidadefinanceiro add nu_multacomcarta numeric(5,0) null
alter table tb_entidadefinanceiro add nu_multasemcarta numeric(5,0) null

--------------------------------------

USE [G2H_EAD1]
GO

/****** Object:  View [dbo].[vw_contaentidadefinanceiro]    Script Date: 23/02/2015 16:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER VIEW [dbo].[vw_contaentidadefinanceiro] AS
SELECT DISTINCT
	 c.id_contaentidade ,
        c.id_entidade ,
        c.id_tipodeconta ,
        c.st_banco ,
        c.st_digitoagencia ,
        c.st_agencia ,
        c.st_conta ,
        c.bl_padrao ,
        c.st_digitoconta, ef.id_entidadefinanceiro,
	ef.nu_diaspagamentoentrada,
	ef.bl_automatricula,
	ef.nu_multasemcarta,
	ef.nu_multacomcarta,
	tc.st_tipodeconta,
	b.st_nomebanco
FROM
	tb_contaentidade c
LEFT JOIN tb_entidadefinanceiro ef ON c.id_entidade = ef.id_entidade
LEFT JOIN tb_tipodeconta tc ON c.id_tipodeconta = tc.id_tipodeconta
LEFT JOIN tb_banco b ON c.st_banco = b.id_banco

GO



-- GII-7050

SET IDENTITY_INSERT tb_funcionalidade ON;
INSERT INTO tb_funcionalidade (id_funcionalidade, st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao, id_tipofuncionalidade, bl_pesquisa, bl_lista, id_sistema, st_urlcaminho, bl_visivel, bl_relatorio, bl_delete)
VALUES (779, 'Transferência de Turma', 157, 1, 123, 3, 0, 0, 1, '/transferencia-turma', 1, 0, 0);
SET IDENTITY_INSERT tb_funcionalidade OFF;

ALTER TABLE tb_matricula ADD id_turmaorigem INT NULL FOREIGN KEY REFERENCES tb_turma (id_turma)
CREATE VIEW [dbo].[vw_gradenota] as
  SELECT DISTINCT
    sa.dt_abertura ,
    pp.id_projetopedagogico ,
    pp.st_projetopedagogico ,
    sa.st_saladeaula ,
    mn.st_notatcc ,
    mn.id_tiponotatcc ,
    mn.st_notaead ,
    mn.id_tiponotaead ,
    mn.st_notaatividade ,
    mn.id_tiponotaatividade ,
    mn.st_notafinal ,
    mn.id_tiponotafinal ,
    dc.id_disciplina ,
    dc.st_disciplina ,
    dc.id_tipodisciplina ,
      dc.st_tituloexibicao AS st_tituloexibicaodisciplina ,
    mt.id_matricula ,
    md.id_matriculadisciplina ,
    acr.id_avaliacaoconjuntoreferencia ,
      CAST(( md.nu_aprovafinal * pp.nu_notamaxima / 100 ) AS INT) AS nu_notafinal ,
    ev.id_evolucao ,
    ev.st_evolucao ,
    st.id_situacao ,
    st.st_situacao ,
    dc.nu_cargahoraria ,
    sa.id_saladeaula ,
    sa.dt_encerramento ,
    mn.nu_notatotal ,
    sa.id_categoriasala ,

      st_status = CASE WHEN ea.id_alocacao IS NOT NULL
                            AND md.id_evolucao = 12 THEN ev.st_evolucao
                  WHEN ea.id_alocacao IS NOT NULL
                       AND md.id_evolucao = 19 THEN ev.st_evolucao
                  WHEN ea.id_alocacao IS NOT NULL
                       AND md.id_situacao = 65
                    THEN 'Crédito Concedido'
                  WHEN ea.id_alocacao IS NOT NULL
                       AND ( mn.nu_notatotal + nf.nu_notafaltante ) >= ( pp.nu_notamaxima
                                                                         * pp.nu_percentualaprovacao
                                                                         / 100 )
                    THEN 'Satisfatório'
                  WHEN ea.id_alocacao IS NOT NULL
                       AND ( mn.nu_notatotal + nf.nu_notafaltante ) < ( pp.nu_notamaxima
                                                                        * pp.nu_percentualaprovacao
                                                                        / 100 )
                    THEN 'Insatisfatório'
                  WHEN ea.id_alocacao IS NULL
                       AND ea.id_encerramentosala IS NULL
                    THEN 'Não encerrado'
                  ELSE '-'
                  END ,
      bl_status = CASE WHEN md.bl_obrigatorio = 0 THEN 1
                  WHEN ( mn.nu_notatotal
                         + ISNULL(nf.nu_notafaltante, 0) ) >= ( pp.nu_notamaxima
                                                                * pp.nu_percentualaprovacao
                                                                / 100 ) THEN 1
                  WHEN ( mn.nu_notatotal
                         + ISNULL(nf.nu_notafaltante, 0) ) < ( pp.nu_notamaxima
                                                               * pp.nu_percentualaprovacao
                                                               / 100 ) THEN 0
                  ELSE 0
                  END ,
      bl_complementar = CASE WHEN ( md.id_matricula != md.id_matriculaoriginal )
                                  AND ppcompl.bl_disciplinacomplementar = 1
        THEN 1
                        ELSE 0
                        END

    , DATEADD(DAY,ISNULL(sa.nu_diasextensao,0), sa.dt_encerramento) as dt_encerramentoextensao
  FROM    dbo.tb_matriculadisciplina AS md
    JOIN dbo.tb_matricula AS mt ON md.id_matricula = mt.id_matricula
    JOIN dbo.tb_disciplina AS dc ON dc.id_disciplina = md.id_disciplina
                                    AND dc.id_tipodisciplina <> 3
    JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
    LEFT JOIN dbo.tb_alocacao AS al ON al.id_matriculadisciplina = md.id_matriculadisciplina
                                       AND al.bl_ativo = 1
    LEFT JOIN dbo.tb_avaliacaoconjuntoreferencia AS acr ON acr.id_saladeaula = al.id_saladeaula --AND acr.dt_fim IS NOT null
    LEFT JOIN tb_encerramentoalocacao AS ea ON ea.id_alocacao = al.id_alocacao
    LEFT JOIN dbo.tb_saladeaula AS sa ON sa.id_saladeaula = al.id_saladeaula
    JOIN tb_evolucao AS ev ON ev.id_evolucao = md.id_evolucao
    JOIN dbo.tb_situacao AS st ON st.id_situacao = md.id_situacao
    LEFT JOIN dbo.vw_matriculanota AS mn ON md.id_matriculadisciplina = mn.id_matriculadisciplina
                                            AND mn.id_alocacao = al.id_alocacao
    OUTER APPLY ( SELECT    id_matricula ,
                    id_disciplina ,
                    SUM(CAST(nu_notamax AS FLOAT)) AS nu_notafaltante
                  FROM      vw_avaliacaoaluno
                  WHERE     bl_ativo IS NULL
                            AND id_matricula = mt.id_matricula
                            AND id_disciplina = md.id_disciplina
                            AND id_tipoprova = 2
                            AND id_avaliacaorecupera = 0
                  GROUP BY  id_disciplina ,
                    id_matricula
                ) AS nf
    JOIN tb_matricula AS matcompl ON matcompl.id_matricula = md.id_matriculaoriginal
    JOIN tb_projetopedagogico AS ppcompl ON ppcompl.id_projetopedagogico = matcompl.id_projetopedagogico;
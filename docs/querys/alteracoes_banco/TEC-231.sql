	CREATE TABLE tb_itemativacao (
	id_itemativacao INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
	st_itemativacao VARCHAR(150),
	st_descricao TEXT,
	st_responsavel VARCHAR(50),
	st_caminhosistema VARCHAR(250),
	bl_ativo BIT NOT NULL
	);

	INSERT INTO tb_itemativacao (st_itemativacao, st_descricao, st_responsavel, st_caminhosistema, bl_ativo) VALUES
	('Entidade - Dados Básicos', 'Uma entidade pode ser criada para que os usuários comecem a utilizar. Isso não é complexo mas para que essa entidade comece a fazer vendas e matrículas várias informações são necessárias e devem ser prestadas posteriormente.', 'Usuário', 'Organização >> Cadastros >> Pessoa jurídica. Escolha a opção Adicionar e informe os dados solicitados para que a entidade seja criada.', 1),
	('Perfis  - De Terceiro', 'As pessoas precisam de perfis para acessar o sistema. Esses perfis podem ser próprios da entidade ou então compartilhados de outras.  Se houver o intesse em usar um perfil de outra entidade é necessário que ele seja liberado explicitamente.', 'DDS', 'A DDS criará os perfis mediante solicitação. Essa solicitação é feita através do sistema de ocorrências do G2 e deve indicar quais os sistemas e perfis que serão necessários.', 1),
	('Perfis - Próprios', 'A entidade pode  desejar trabalhar com perfis próprios. Esses perfis podem ser criados e administrados na própria entidade.', 'Usuário', 'Pessoas >> Cadastro >> Perfil', 1),
	('Usuários Administrativos', 'Para que uma pessoa acesse o sistema através de um perfil compartilhado (administrador de unyleya, por exemplo) é necessário que ela tenha o acesso liberado na entidade que é a dona do perfil.', 'DDS', 'A DDS vinculará os usuários aos perfis mediante solicitação. Essa solicitação é feita através do sistema de ocorrências do G2 e deve indicar quem são as pessoas e quais os perfis que elas devem ter.', 1),
	('Área de Conhecimento - De Terceiros', 'Todos os projetos pedagógicos e discipinas são vinculados a pelo menos uma área de conhecimento. Essas áreas de conhecimento podem ser próprias ou de terceiros.', 'Usuário', 'Na entidade que é dona da área de Conhecimento - Pedagógico >> Cadastros >> Área de conhecimento Escolha uma das Áreas e vá na opção Organizações. Informe dentre as entidades listadas as que poderão utilizar a área de conhecimento.', 1),
	('Área de Conhecimento - Própria', 'Se a entidade vai utilizar uma área de conhecimento própria é necessário cadastrá-la.', 'Usuário', 'Pedagógico >> Cadastros >> Área Conhecimento', 1),
	('Produtos - De terceiros', 'O que a entidade vende são produtos. Esses produtos podem ser próprios ou de terceiros. Para usar os produtos de terceiros é necessário que eles sejam compartilhados.', 'Usuário', 'Na entidade que é dona do produto - Financeiro >> Cadastros >> Produtos. Escolha um dos produtos e vá na opção Organizações. Informe que deseje vincular a todas a entidades, automaticamente.', 1),
	('Produtos - Próprios', 'A entidade pode querer criar e comercializar produtos próprios e para isso eles devem ser cadastrados.', 'Usuário', 'Financeiro >> Cadastros >> Produtos', 1),
	('Projeto Pedagógico - De terceiros', 'A maioria dos produtos vendidos são cursos ou Projetos pedagógicos. A entidade pode querer vender  projetos pedagógicos de outras entidades. Nesse caso é necessário que haja a liberação explicita na entidade que é dona do projeto.', 'Usuário', 'Na entidade que é dona do projeto pedagógico - Organização >> Cadastros >> Relação. Uma vez na tela, escolha a entidade e na lista de projetos que serão apresentados escolha os que interessam.', 1),
	('Projeto Pedagógico - Próprio', 'A entidade pode querer desenvolver os próprios projetos pedagógicos. Esse projeto poderá usar disciplinas compartilhadas de outras entidades ou próprias.', 'Usuário', 'Pedagógico >> Cadastros >> Projetos Pedagógicos', 1),
	('Sala de aula - De terceiros', 'O acesso ao LMS é feito a partir de uma sala de aula. A entidade pode querer utilizar salas de aula de outras entidades. Nesse caso é necessário que haja a liberação explicita na entidade que é dona da sala de aula.', 'Usuário', 'Na entidade que é dona da sala de aula - Pedagógico >> Gestão de Salas >> Salas de aula. Acessse uma sala de aula. Vá até Organizações e assinale as entidades que podem usar essa sala de aula.', 1),
	('Sala de Aula - Própria', 'Se a entidade vai trabalhar com as suas próprias salas de aula é necessário cadastrá-las.', 'Usuário', 'Pedagógico >> Gestão de Salas >> Salas de aula', 1),
	('Turma - De terceiros', 'Turma, para o G2, é uma forma de classificar e controlar a entrada de alunos. Se a entidade utilizar o conceito de turma, poderá optar por utilizar as próprias turmas ou de terceiros. Em caso de terceiros é necessário que haja uma liberação explícita', 'Usuário', 'Na entidade que é dona da Turma - Pedagógico >> Cadastros >> Turma. Acessse uma sala de Turma. Vá até Organizações e assinale as entidades que podem usar essa sala de aula.', 1),
	('Turma - Própria', 'Se a entidade vai trabalhar com as suas próprias Turmas é necessário cadastrá-las.', 'Usuário', 'Pedagógico >> Cadastros >> Turma', 1),
	('Forma de Pagamento', 'Para que uma venda posser realizada é necessário que haja forma de pagamento cadastrada. Pelo menos uma.', 'Usuário', 'Financeiro >> Cadastros >> Formas de Pagamento', 1),
	('Portal Aluno - Endereço *', 'Se a entidade vai utilizar um portal de aluno é necessário que o endereço do portal seja cadastrado. Se isso não for feito não será possível para o aluno, ao final da compra, ser direcionado para o portal.', 'Usuário', '"Organização >> Cadastros >> Pessoa jurídica. Escolha uma entidade, vá na aba Dados Básicos e informe o endereço do Portal do aluno.OBS: Obter o endereço junto ao Marketing."', 1),
	('Portal Aluno - Configurações', 'Se a entidade vai utilizar um portal de aluno é necessário dizer para o sistema quais os recursos e funcionalidades que serão exibidos / disponibilizados.', 'Usuário', 'Organização >> Cadastros >> Pessoa jurídica. Escolha uma entidade, vá na aba Configurações e informe como o Portal do aluno vai funcionar.', 1),
	('Financeiro - Boleto', 'Se a entidade vai fazer vendas utilizando boletos é necessário fazer o cadastramento dos dados de geração do boleto.', 'Usuário', '"Organização >> Cadatros >> Pessoa jurídica --> aba financeiro. Informe os dados dados da conta e os dados de boleto.OBS: Obter os dados junto ao financeiro ou pedir para eles cadastrarem."', 1),
	('Financeiro - Cartão', 'Se a entidade vai fazer vendas utilizando cartão de crédito é necessário cadastrar os dados de integração com a Braspag.', 'Usuário', '"Organização >> Cadatros >> Pessoa jurídica --> aba financeiro. Informe os dados dados do Cartão.OBS: Obter os dados junto ao financeiro ou pedir para eles cadastrarem."', 1),
	('Integração Totvs', 'Para que as vendas realizadas no G2 sejam enviadas para o Totvs é necessário que a integração entre os dois sistemas esteja configurada.', 'Usuário', '"Organização >> Configurações >> IntegraçãoOBS: Obter os dados junto ao financeiro ou pedir para eles cadastrarem."', 1),
	('Conta de Email', 'As contas de email devem ser configuradas na entidade para que sistema possa fazer os disparos previstos.', 'Usuário', 'Organização >> Cadastros >> Contas de e-mail', 1),
	('Textos do sistema', 'Os textos que serão apresentados ao usuários devem ser cadatrados nessa entidade para que sejam coerentes e customizados. Se os textos não forem configurados as funcionalidades correspondentes não funcionarão. Ex: Os contratos não serão gerados, os email não serão enviados.', 'Usuário', 'Organização >> Cadastros >> Textos do Sistema', 1),
	('Mensagens Padrão', 'As mensagens que serão apresentadas / enviadas pelo sistema devem ser configuradas na própria entidade. É aqui que serão selecionadas as contas de email e os textos de sistema.', 'Usuário', 'Organização >> Configurações >> Mensagem Padrão', 1),
	('Integração BB', 'Se a entidade vai trabalhar com o Blackboard, é necessário configurar inicalmente a integração entre os dois sistemas para essa entidade. Para isso é necessário informar dentro do G2 a chave de integração obtida junto a BB. É essa chave que permitirá que o BB saiba qual é a entidade que está chamando a sala de aula e possa se configurar para isso.', 'DDS', 'Essa integração está sendo feita pela DDS mediante solicitação feita no sistema de ocorrências. Vamos desenvolver uma funcionalidade para que seja feita pelo próprio usuário.', 1),
	('Integração Moodle', 'Se a entidade vai trabalhar com o Moodle é necessário configurar inicalmente a integração entre os dois sistemas para essa entidade. Para isso é necessário informar, dentro do G2, a chave de integração obtida junto ao Moodle. Se isso não for feito a entidade não conseguirá integrar ao Moodle.', 'Usuário', 'Organização >> Configurações >> Integração. Escolha a opção Moodle e informe os dados necessários.', 1),
	('Entidade - Configurações báscias como Logo, css, etc.)', 'A configuração da entidade deve ser feita para que o portal consiga buscar as imagens necessárias.', 'Usuário', 'Organização >> Cadastros >> Pessoa jurídica. Escolha uma entidade, vá na aba Configurações e informe as imagens que serão apresentadas.', 1)




	CREATE TABLE tb_itemativacaoentidade (
	id_itemativacaoentidade INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
	id_itemativacao INT NOT NULL,
	id_entidade INT NOT NULL,
	id_situacao INT NOT NULL,
	id_usuarioatualizacao INT NOT NULL,
	dt_atualizacao DATETIME NOT NULL
	);

	ALTER TABLE tb_itemativacaoentidade WITH CHECK ADD CONSTRAINT [tb_itemativacaoentidade_tb_itemativacao_fk] FOREIGN KEY([id_itemativacao])
	REFERENCES tb_itemativacao ([id_itemativacao]);

	ALTER TABLE tb_itemativacaoentidade WITH CHECK ADD CONSTRAINT [tb_itemativacaoentidade_tb_entidade_fk] FOREIGN KEY([id_entidade])
	REFERENCES tb_entidade ([id_entidade]);

	ALTER TABLE tb_itemativacaoentidade WITH CHECK ADD CONSTRAINT [tb_itemativacaoentidade_tb_situacao_fk] FOREIGN KEY([id_situacao])
	REFERENCES tb_situacao ([id_situacao]);

	ALTER TABLE tb_itemativacaoentidade WITH CHECK ADD CONSTRAINT [tb_itemativacaoentidade_tb_usuario_fk] FOREIGN KEY([id_usuarioatualizacao])
	REFERENCES tb_usuario ([id_usuario]);

	SET IDENTITY_INSERT dbo.tb_situacao ON
	INSERT INTO tb_situacao (id_situacao, st_situacao, st_tabela, st_campo, st_descricaosituacao) VALUES
	(150, 'NOK', 'tb_itemativacaoentidade', 'id_situacao', 'Situação Não-OK'),
	(151, 'OK', 'tb_itemativacaoentidade', 'id_situacao', 'Situação OK'),
	(152, 'Não se aplica', 'tb_itemativacaoentidade', 'id_situacao', 'Situação não se aplica')
	SET IDENTITY_INSERT dbo.tb_situacao OFF
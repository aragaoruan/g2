-- AC-27595

SET IDENTITY_INSERT tb_funcionalidade ON;
INSERT INTO tb_funcionalidade
(id_funcionalidade, st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao, id_tipofuncionalidade, id_sistema, st_urlcaminho, bl_visivel, bl_pesquisa, bl_lista) VALUES
(761, 'Alunos Agendados', 257, 1, 123, 3, 1, '/relatorio-alunos-agendados', 1, 0, 0)
SET IDENTITY_INSERT tb_funcionalidade OFF;
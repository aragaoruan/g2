/*Scprit para gerar o menu */

INSERT INTO [dbo].[tb_funcionalidade]
           ([st_funcionalidade]
           ,[id_funcionalidadepai]
           ,[bl_ativo]
           ,[id_situacao]
           ,[st_classeflex]
           ,[st_urlicone]
           ,[id_tipofuncionalidade]
           ,[nu_ordem]
           ,[st_classeflexbotao]
           ,[bl_pesquisa]
           ,[bl_lista]
           ,[id_sistema]
           ,[st_ajuda]
           ,[st_target]
           ,[st_urlcaminho]
           ,[bl_visivel]
           ,[bl_relatorio]
           ,[st_urlcaminhoedicao]
           ,[bl_delete])
     VALUES
           ('Pacote/Arquivo de Envio'
           ,98
           ,1
           ,123
           ,'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa'
           ,'/img/ico/block--pencil.png'
           ,3
           ,null
           ,NULL
           ,0
           ,0
           ,1
           ,null
           ,null
           ,'/material-arquivo-envio'
           ,1
           ,0
           ,null
           ,0)
GO

/* Fim do script para gerar menu */


/*==============================================================*/
/* Table: tb_lotematerial                                       */
/*==============================================================*/
if exists (select 1
            from  sysobjects
           where  id = object_id('tb_lotematerial')
            and   type = 'U')
   drop table tb_lotematerial
go


create table tb_lotematerial (
   id_lotematerial      int                  identity,
   dt_cadastro          datetime2            null default getdate(),
   constraint PK_TB_LOTEMATERIAL primary key (id_lotematerial)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('tb_lotematerial') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'tb_lotematerial' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   'Tabela que cont�m os registros de lots de pacotes de material gerado', 
   'user', @CurrentUser, 'table', 'tb_lotematerial'
go

/*==============================================================*/
/* Table: tb_lotematerial  (FIM)                                */
/*==============================================================*/


/*==============================================================*/
/* Table: tb_pacote                                             */
/*==============================================================*/

alter table dbo.tb_pacote add id_situacao int
alter table dbo.tb_pacote add id_lotematerial int

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.tb_pacote') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', 'tb_pacote' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   'Tabela de pacotes de material que ser�o enviados aos alunos', 
   'user', 'dbo', 'table', 'tb_pacote'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.tb_pacote')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'id_matricula')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'tb_pacote', 'column', 'id_matricula'

end


execute sp_addextendedproperty 'MS_Description', 
   'chave prim�ria.',
   'user', 'dbo', 'table', 'tb_pacote', 'column', 'id_matricula'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.tb_pacote')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'id_situacao')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'tb_pacote', 'column', 'id_situacao'

end


execute sp_addextendedproperty 'MS_Description', 
   'Chave prim�ria da tabela.',
   'user', 'dbo', 'table', 'tb_pacote', 'column', 'id_situacao'
go

alter table dbo.tb_pacote
   add constraint FK_TB_PACOT_REFERENCE_TB_MATRI foreign key (id_matricula)
      references dbo.tb_matricula (id_matricula)
go

alter table dbo.tb_pacote
   add constraint FK_TB_PACOT_REFERENCE_TB_SITUA foreign key (id_situacao)
      references dbo.tb_situacao (id_situacao)
go

alter table dbo.tb_pacote
   add constraint FK_TB_PACOT_REFERENCE_TB_LOTEM foreign key (id_lotematerial)
      references tb_lotematerial (id_lotematerial)
go

/*==============================================================*/
/* Table: tb_pacote  (FIM)                                      */
/*==============================================================*/

/*==============================================================*/
/* Table: tb_entregamaterial                                    */
/*==============================================================*/
alter table dbo.tb_entregamaterial add id_pacote int


if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.tb_entregamaterial') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', 'tb_entregamaterial' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   'Tabela que define quais materiais j� foram entregues relativo a uma matricula', 
   'user', 'dbo', 'table', 'tb_entregamaterial'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.tb_entregamaterial')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'id_matricula')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'tb_entregamaterial', 'column', 'id_matricula'

end


execute sp_addextendedproperty 'MS_Description', 
   'Matricula para qual foi entregue o material',
   'user', 'dbo', 'table', 'tb_entregamaterial', 'column', 'id_matricula'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.tb_entregamaterial')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'id_usuariocadastro')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'tb_entregamaterial', 'column', 'id_usuariocadastro'

end


execute sp_addextendedproperty 'MS_Description', 
   'Usu�rio que entregou o material',
   'user', 'dbo', 'table', 'tb_entregamaterial', 'column', 'id_usuariocadastro'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.tb_entregamaterial')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'id_situacao')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'tb_entregamaterial', 'column', 'id_situacao'

end


execute sp_addextendedproperty 'MS_Description', 
   'Chave prim�ria da tabela.',
   'user', 'dbo', 'table', 'tb_entregamaterial', 'column', 'id_situacao'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.tb_entregamaterial')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'id_itemdematerial')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'tb_entregamaterial', 'column', 'id_itemdematerial'

end


execute sp_addextendedproperty 'MS_Description', 
   'Chave prim�ria da tabela.',
   'user', 'dbo', 'table', 'tb_entregamaterial', 'column', 'id_itemdematerial'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.tb_entregamaterial')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'dt_entrega')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'tb_entregamaterial', 'column', 'dt_entrega'

end


execute sp_addextendedproperty 'MS_Description', 
   'Data de entrega do material',
   'user', 'dbo', 'table', 'tb_entregamaterial', 'column', 'dt_entrega'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.tb_entregamaterial')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'dt_devolucao')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'tb_entregamaterial', 'column', 'dt_devolucao'

end


execute sp_addextendedproperty 'MS_Description', 
   'Data que o material foi devolvido',
   'user', 'dbo', 'table', 'tb_entregamaterial', 'column', 'dt_devolucao'
go

alter table dbo.tb_entregamaterial
   add constraint FK_TB_ENTRE_FK_TB_ENT_TB_SITUA foreign key (id_situacao)
      references dbo.tb_situacao (id_situacao)
go

alter table dbo.tb_entregamaterial
   add constraint FK_TB_ENTRE_REFERENCE_TB_ITEMD foreign key (id_itemdematerial)
      references dbo.tb_itemdematerial (id_itemdematerial)
go

alter table dbo.tb_entregamaterial
   add constraint FK_TB_ENTRE_REFERENCE_TB_MATRI foreign key (id_matricula)
      references dbo.tb_matricula (id_matricula)
go

alter table dbo.tb_entregamaterial
   add constraint FK_TB_ENTRE_REFERENCE_TB_PACOT foreign key (id_pacote)
      references dbo.tb_pacote (id_pacote)
go


alter table dbo.tb_entregamaterial add id_matriculadisciplina int
alter table dbo.tb_entregamaterial
   add constraint FK_TB_MATD_REFERENCE_TB_ENTRE foreign key (id_matriculadisciplina)
      references dbo.tb_matriculadisciplina (id_matriculadisciplina)
go

 ALTER TABLE tb_entregamaterial ALTER COLUMN dt_entrega datetime2 null


/*==============================================================*/
/* Table: tb_entregamaterial  (FIM)                             */
/*==============================================================*/


/*==============================================================*/
/* Table: tb_matriculadisciplina                                */
/*==============================================================*/

alter table dbo.tb_matriculadisciplina add id_pacote int
alter table dbo.tb_matriculadisciplina
   add constraint FK_TB_ENTRE_REFERENCE_TB_PAC foreign key (id_pacote)
      references dbo.tb_pacote (id_pacote)
go


/*==============================================================*/
/* Table: tb_matriculadisciplina  (FIM)                          */
/*==============================================================*/

/*==============================================================*/
/* Table: tb_situacao                                           */
/*==============================================================*/

insert into tb_situacao
(id_situacao,st_situacao,st_tabela,st_campo,st_descricaosituacao)
values
(155,'Pendente','tb_pacote','id_situacao','Pacote com envio pendente')

insert into tb_situacao
(id_situacao,st_situacao,st_tabela,st_campo,st_descricaosituacao)
values
(156,'Enviado','tb_pacote','id_situacao','Pacote enviado')


select * from tb_entregamaterial
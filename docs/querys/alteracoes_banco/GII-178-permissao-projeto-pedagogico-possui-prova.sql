--SCRIPT PARA ADICIONAR NOVA COLUNA "bl_possuiprova" NA TABLE "tb_projetopedagogico"
ALTER TABLE tb_projetopedagogico ADD bl_possuiprova BIT DEFAULT ((0)) NOT NULL;

--------------------------------------------------------------------------------

--DESATIVAR IDENTITY TABELA FIN-16826
SET IDENTITY_INSERT tb_permissao ON;

-- INSERE A PERMISSÃO 'Possui Prova Montada'
INSERT INTO tb_permissao (id_permissao, st_nomepermissao, st_descricao, bl_ativo)
VALUES (38, 'Possui Prova Montada', 'Permite ao usuario visualizar a opcao -Possui prova montada-', 1) ;

--INSERE O NUMERO DA FUNCIONALIDADE E O NUMERO DA PERMISSAO
INSERT tb_permissaofuncionalidade VALUES (19, 38);

--ATIVAR IDENTITY TABELA
SET IDENTITY_INSERT tb_permissao OFF
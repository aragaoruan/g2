if exists (select 1
            from  sysobjects
           where  id = object_id('tb_concurso')
            and   type = 'U')
   drop table tb_concurso
go

/*==============================================================*/
/* Table: tb_concurso                                           */
/*==============================================================*/
create table tb_concurso (
   id_concurso          int                  identity,
   st_concurso          varchar(Max)         not null,
   st_slug              varchar(200)         null,
   st_imagem            varchar(Max)         null,
   dt_cadastro          datetime             not null default getdate(),
   constraint PK_TB_CONCURSO primary key (id_concurso)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('tb_concurso') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'tb_concurso' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   'Tabela de concursos', 
   'user', @CurrentUser, 'table', 'tb_concurso'
go
if exists (select 1
            from  sysobjects
           where  id = object_id('tb_carreira')
            and   type = 'U')
   drop table tb_carreira
go

/*==============================================================*/
/* Table: tb_carreira                                           */
/*==============================================================*/
create table tb_carreira (
   id_carreira          int                  identity,
   st_carreira          varchar(200)         not null,
   dt_cadastro          datetime             not null default getdate(),
   constraint PK_TB_CARREIRA primary key (id_carreira)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('tb_carreira') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'tb_carreira' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   'Tabela de carreiras que esse produto atende (ex: fiscal, agente)', 
   'user', @CurrentUser, 'table', 'tb_carreira'
go
if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('tb_concursoproduto') and o.name = 'FK_TB_CONCU_REFERENCE_TB_CONCU')
alter table tb_concursoproduto
   drop constraint FK_TB_CONCU_REFERENCE_TB_CONCU
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('tb_concursoproduto') and o.name = 'FK_TB_CONCU_REFERENCE_TB_PRODU')
alter table tb_concursoproduto
   drop constraint FK_TB_CONCU_REFERENCE_TB_PRODU
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('tb_concursoproduto')
            and   name  = 'Reference_710_FK'
            and   indid > 0
            and   indid < 255)
   drop index tb_concursoproduto.Reference_710_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('tb_concursoproduto')
            and   name  = 'Reference_709_FK'
            and   indid > 0
            and   indid < 255)
   drop index tb_concursoproduto.Reference_709_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('tb_concursoproduto')
            and   name  = 'unique_produto_concurso'
            and   indid > 0
            and   indid < 255)
   drop index tb_concursoproduto.unique_produto_concurso
go

if exists (select 1
            from  sysobjects
           where  id = object_id('tb_concursoproduto')
            and   type = 'U')
   drop table tb_concursoproduto
go

/*==============================================================*/
/* Table: tb_concursoproduto                                    */
/*==============================================================*/
create table tb_concursoproduto (
   id_concursoproduto   int                  identity,
   id_concurso          int                  null,
   id_produto           int                  null,
   constraint PK_TB_CONCURSOPRODUTO primary key nonclustered (id_concursoproduto)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('tb_concursoproduto') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'tb_concursoproduto' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   'Tabela de rela��o de produto e concurso', 
   'user', @CurrentUser, 'table', 'tb_concursoproduto'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('tb_concursoproduto')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'id_produto')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'tb_concursoproduto', 'column', 'id_produto'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Chave prim�ria da tabela.',
   'user', @CurrentUser, 'table', 'tb_concursoproduto', 'column', 'id_produto'
go

/*==============================================================*/
/* Index: unique_produto_concurso                               */
/*==============================================================*/
create unique clustered index unique_produto_concurso on tb_concursoproduto (
id_concurso ASC,
id_produto ASC
)
go

/*==============================================================*/
/* Index: Reference_709_FK                                      */
/*==============================================================*/
create index Reference_709_FK on tb_concursoproduto (
id_concurso ASC
)
go

/*==============================================================*/
/* Index: Reference_710_FK                                      */
/*==============================================================*/
create index Reference_710_FK on tb_concursoproduto (
id_produto ASC
)
go

alter table tb_concursoproduto
   add constraint FK_TB_CONCU_REFERENCE_TB_CONCU foreign key (id_concurso)
      references tb_concurso (id_concurso)
go

alter table tb_concursoproduto
   add constraint FK_TB_CONCU_REFERENCE_TB_PRODU foreign key (id_produto)
      references dbo.tb_produto (id_produto)
go
if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('tb_produtocarreira') and o.name = 'FK_TB_PRODU_REFERENCE_TB_CARRE')
alter table tb_produtocarreira
   drop constraint FK_TB_PRODU_REFERENCE_TB_CARRE
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('tb_produtocarreira') and o.name = 'FK_TB_PRODUCAR_REFERENCE_TB_PRODU')
alter table tb_produtocarreira
   drop constraint FK_TB_PRODUCAR_REFERENCE_TB_PRODU
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('tb_produtocarreira')
            and   name  = 'Reference_716_FK'
            and   indid > 0
            and   indid < 255)
   drop index tb_produtocarreira.Reference_716_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('tb_produtocarreira')
            and   name  = 'Reference_715_FK'
            and   indid > 0
            and   indid < 255)
   drop index tb_produtocarreira.Reference_715_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('tb_produtocarreira')
            and   name  = 'unique_produto_carreira'
            and   indid > 0
            and   indid < 255)
   drop index tb_produtocarreira.unique_produto_carreira
go

if exists (select 1
            from  sysobjects
           where  id = object_id('tb_produtocarreira')
            and   type = 'U')
   drop table tb_produtocarreira
go

/*==============================================================*/
/* Table: tb_produtocarreira                                    */
/*==============================================================*/
create table tb_produtocarreira (
   id_produtocarreira   int                  identity,
   id_carreira          int                  null,
   id_produto           int                  null,
   constraint PK_TB_PRODUTOCARREIRA primary key nonclustered (id_produtocarreira)
)
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('tb_produtocarreira')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'id_produto')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'tb_produtocarreira', 'column', 'id_produto'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Chave prim�ria da tabela.',
   'user', @CurrentUser, 'table', 'tb_produtocarreira', 'column', 'id_produto'
go

/*==============================================================*/
/* Index: unique_produto_carreira                               */
/*==============================================================*/
create unique clustered index unique_produto_carreira on tb_produtocarreira (
id_carreira ASC,
id_produto ASC
)
go

/*==============================================================*/
/* Index: Reference_715_FK                                      */
/*==============================================================*/
create index Reference_715_FK on tb_produtocarreira (
id_carreira ASC
)
go

/*==============================================================*/
/* Index: Reference_716_FK                                      */
/*==============================================================*/
create index Reference_716_FK on tb_produtocarreira (
id_produto ASC
)
go

alter table tb_produtocarreira
   add constraint FK_TB_PRODU_REFERENCE_TB_CARRE foreign key (id_carreira)
      references tb_carreira (id_carreira)
go

alter table tb_produtocarreira
   add constraint FK_TB_PRODUCAR_REFERENCE_TB_PRODU foreign key (id_produto)
      references dbo.tb_produto (id_produto)
go
if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('tb_produtouf') and o.name = 'FK_TB_PRODU_REFERENCE_TB_UF')
alter table tb_produtouf
   drop constraint FK_TB_PRODU_REFERENCE_TB_UF
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('tb_produtouf') and o.name = 'FK_TB_PRODUUF_REFERENCE_TB_PRODU')
alter table tb_produtouf
   drop constraint FK_TB_PRODUUF_REFERENCE_TB_PRODU
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('tb_produtouf')
            and   name  = 'Reference_713_FK'
            and   indid > 0
            and   indid < 255)
   drop index tb_produtouf.Reference_713_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('tb_produtouf')
            and   name  = 'Reference_712_FK'
            and   indid > 0
            and   indid < 255)
   drop index tb_produtouf.Reference_712_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('tb_produtouf')
            and   name  = 'unique_produto_uf'
            and   indid > 0
            and   indid < 255)
   drop index tb_produtouf.unique_produto_uf
go

if exists (select 1
            from  sysobjects
           where  id = object_id('tb_produtouf')
            and   type = 'U')
   drop table tb_produtouf
go

/*==============================================================*/
/* Table: tb_produtouf                                          */
/*==============================================================*/
create table tb_produtouf (
   tb_produtouf         int                  identity,
   sg_uf                char(2)              null,
   id_produto           int                  null,
   constraint PK_TB_PRODUTOUF primary key nonclustered (tb_produtouf)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('tb_produtouf') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'tb_produtouf' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   'Tabela q relaciona produto e uf', 
   'user', @CurrentUser, 'table', 'tb_produtouf'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('tb_produtouf')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sg_uf')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'tb_produtouf', 'column', 'sg_uf'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Tabela que possui os estados',
   'user', @CurrentUser, 'table', 'tb_produtouf', 'column', 'sg_uf'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('tb_produtouf')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'id_produto')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'tb_produtouf', 'column', 'id_produto'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Chave prim�ria da tabela.',
   'user', @CurrentUser, 'table', 'tb_produtouf', 'column', 'id_produto'
go

/*==============================================================*/
/* Index: unique_produto_uf                                     */
/*==============================================================*/
create unique clustered index unique_produto_uf on tb_produtouf (
sg_uf DESC,
id_produto DESC
)
go

/*==============================================================*/
/* Index: Reference_712_FK                                      */
/*==============================================================*/
create index Reference_712_FK on tb_produtouf (
sg_uf ASC
)
go

/*==============================================================*/
/* Index: Reference_713_FK                                      */
/*==============================================================*/
create index Reference_713_FK on tb_produtouf (
id_produto ASC
)
go

alter table tb_produtouf
   add constraint FK_TB_PRODU_REFERENCE_TB_UF foreign key (sg_uf)
      references dbo.tb_uf (sg_uf)
go

alter table tb_produtouf
   add constraint FK_TB_PRODUUF_REFERENCE_TB_PRODU foreign key (id_produto)
      references dbo.tb_produto (id_produto)
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('tb_produtoentidade') and o.name = 'FK_TB_PRODUNT_REFERENCE_TB_ENTID')
alter table tb_produtoentidade
   drop constraint FK_TB_PRODUNT_REFERENCE_TB_ENTID
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('tb_produtoentidade') and o.name = 'FK_TB_PRODUENTI_REFERENCE_TB_PRODU')
alter table tb_produtoentidade
   drop constraint FK_TB_PRODUENTI_REFERENCE_TB_PRODU
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('tb_produtoentidade')
            and   name  = 'Reference_718_FK'
            and   indid > 0
            and   indid < 255)
   drop index tb_produtoentidade.Reference_718_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('tb_produtoentidade')
            and   name  = 'Reference_717_FK'
            and   indid > 0
            and   indid < 255)
   drop index tb_produtoentidade.Reference_717_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('tb_produtoentidade')
            and   type = 'U')
   drop table tb_produtoentidade
go

/*==============================================================*/
/* Table: tb_produtoentidade                                    */
/*==============================================================*/
create table tb_produtoentidade (
   id_produtoentidade   int                  identity,
   id_entidade          int                  null,
   id_produto           int                  null,
   constraint PK_TB_PRODUTOENTIDADE primary key (id_produtoentidade)
)
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('tb_produtoentidade')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'id_entidade')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'tb_produtoentidade', 'column', 'id_entidade'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Chave prim�ria da tabela.',
   'user', @CurrentUser, 'table', 'tb_produtoentidade', 'column', 'id_entidade'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('tb_produtoentidade')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'id_produto')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'tb_produtoentidade', 'column', 'id_produto'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Chave prim�ria da tabela.',
   'user', @CurrentUser, 'table', 'tb_produtoentidade', 'column', 'id_produto'
go

/*==============================================================*/
/* Index: Reference_717_FK                                      */
/*==============================================================*/
create index Reference_717_FK on tb_produtoentidade (
id_entidade ASC
)
go

/*==============================================================*/
/* Index: Reference_718_FK                                      */
/*==============================================================*/
create index Reference_718_FK on tb_produtoentidade (
id_produto ASC
)
go

alter table tb_produtoentidade
   add constraint FK_TB_PRODUNT_REFERENCE_TB_ENTID foreign key (id_entidade)
      references dbo.tb_entidade (id_entidade)
go

alter table tb_produtoentidade
   add constraint FK_TB_PRODUENTI_REFERENCE_TB_PRODU foreign key (id_produto)
      references dbo.tb_produto (id_produto)
go

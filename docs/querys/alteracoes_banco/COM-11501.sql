alter VIEW [dbo].[vw_turmaprojetopedagogico] AS
SELECT
p.id_projetopedagogico,
p.st_projetopedagogico,
p.st_tituloexibicao,
tp.id_turma,
t.id_entidadecadastro,
t.st_turma,
tp.bl_ativo,
tur.id_turno
FROM
tb_projetopedagogico p
INNER JOIN tb_turmaprojeto tp ON tp.id_projetopedagogico = p.id_projetopedagogico
INNER JOIN tb_turma t ON tp.id_turma = t.id_turma
INNER JOIN tb_turmaturno tt on t.id_turma = tt.id_turma
INNER JOIN tb_turno tur on tur.id_turno = tt.id_turno
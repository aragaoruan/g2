--Criando a coluna para marcar que a renovação não ocorreu porque não havia sala
ALTER TABLE [dbo].[tb_venda]
  ADD [bl_semsalarenovacao] BIT NULL DEFAULT 0;

-- Cria a funcionalidade
--DESATIVAR IDENTITY TABELA
SET IDENTITY_INSERT tb_funcionalidade ON

INSERT INTO tb_funcionalidade
(id_funcionalidade, st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao,
 st_classeflex, st_urlicone, id_tipofuncionalidade, nu_ordem, st_classeflexbotao, bl_pesquisa,
 bl_lista, id_sistema, st_ajuda, st_target, st_urlcaminho, bl_visivel, bl_relatorio,
 st_urlcaminhoedicao, bl_delete)
VALUES
  (793, 'Alunos Não Renovados', 257, 1, 123,
        'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa', NULL, 3, 0, NULL, 0,
   0, 1, NULL, NULL, 793, 1, 1, NULL, 0)

--ATIVAR IDENTITY TABELA
SET IDENTITY_INSERT tb_funcionalidade OFF

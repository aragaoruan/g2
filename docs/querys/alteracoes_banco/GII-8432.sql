ALTER TABLE [dbo].[tb_alocacaointegracao]
  ADD [id_entidadeintegracao] INT NULL
GO

ALTER TABLE [dbo].[tb_alocacaointegracao]
  ADD CONSTRAINT [FK_TB_ALOCA_REFERENCE_TB_ENTIDADEINTEGRACAO] FOREIGN KEY ([id_entidadeintegracao])
REFERENCES [dbo].[tb_entidadeintegracao] ([id_entidadeintegracao])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
GO


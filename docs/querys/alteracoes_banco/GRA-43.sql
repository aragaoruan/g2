--DROP TABLE tb_meiopgamentograduacao

/**
** CREATE TABLE tb_meiopagamentograduacao
**/
CREATE TABLE tb_meiopagamentograduacao
(
id_meiopagamentograduacao int NOT NULL IDENTITY(1,1),
st_meiopagamentograduacao varchar(255),
st_descricao varchar(255)
,CONSTRAINT PK_tb_meiopgamentograduacao_id_meiopagamentograduacao PRIMARY KEY CLUSTERED (id_meiopagamentograduacao)
)


/**
** INSERT na tb_meiopagamentograduacao
**/
SET IDENTITY_INSERT dbo.tb_meiopagamentograduacao on
INSERT INTO tb_meiopagamentograduacao
(id_meiopagamentograduacao, st_meiopagamentograduacao, st_descricao)
VALUES (
1, 'Cart�o Cr�dito' , 'Pagamento por meio de cart�o de cr�dito'),
(2, 'Boleto', 'Boleto Banc�rio')
SET IDENTITY_INSERT dbo.tb_meiopagamentograduacao off


--SELECT * FROM tb_meiopgamentograduacao
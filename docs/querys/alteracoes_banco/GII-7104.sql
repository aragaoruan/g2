create table tb_recebedor(
  id_recebedor int not null PRIMARY KEY IDENTITY,
  st_recebedor VARCHAR(255) NOT NULL,
  id_recebedorexterno VARCHAR(255) NULL
);

alter table dbo.tb_entidadeintegracao ADD id_recebedor int null FOREIGN KEY REFERENCES tb_recebedor(id_recebedor);


update tb_sistema set st_sistema = 'Cartão' where id_sistema = 7
update tb_sistema set st_sistema = 'Cartão Recorrente' where id_sistema = 10
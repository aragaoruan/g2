SET IDENTITY_INSERT tb_funcionalidade ON

INSERT INTO "tb_funcionalidade"
("id_funcionalidade",
"st_funcionalidade",
"id_funcionalidadepai",
"bl_ativo",
"id_situacao",
"st_classeflex",
"st_urlicone",
"id_tipofuncionalidade",
"nu_ordem",
"st_classeflexbotao",
"bl_pesquisa",
"bl_lista",
"id_sistema",
"st_ajuda",
"st_target",
"st_urlcaminho",
"bl_visivel",
"bl_relatorio",
"st_urlcaminhoedicao",
"bl_delete")
VALUES (786, 'Alunos Disciplina Trancada', 257, 'True', 123, NULL, NULL, 3, 0, NULL, 'False', 'False', 1, NULL, NULL, '786', 1, 'True', NULL, 'False');

SET IDENTITY_INSERT tb_funcionalidade OFF

-- GII-7161

-- bl_documentacao
ALTER TABLE tb_matricula DROP CONSTRAINT DF__tb_matric__bl_do__62E1DC45;
ALTER TABLE tb_matricula ALTER COLUMN bl_documentacao BIT NULL;
ALTER TABLE tb_matricula ADD CONSTRAINT DF__tb_matric__bl_do__62E1DC45 DEFAULT NULL FOR bl_documentacao;

-- bl_academico
ALTER TABLE tb_matricula DROP CONSTRAINT DF__tb_matric__bl_ac__5B60B60E;
ALTER TABLE tb_matricula ALTER COLUMN bl_academico BIT NULL;
ALTER TABLE tb_matricula ADD CONSTRAINT DF__tb_matric__bl_ac__5B60B60E DEFAULT NULL FOR bl_academico;

-- UPDATE
UPDATE tb_matricula SET bl_documentacao = NULL WHERE bl_documentacao = 0;
UPDATE tb_matricula SET bl_academico = NULL WHERE bl_academico = 0;
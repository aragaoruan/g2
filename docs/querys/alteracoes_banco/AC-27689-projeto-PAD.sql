/********************[AC-27689]*****************************/ 
/**
1. Adicionar a coluna  'bl_renovar' na tb_projetopedagogico para indicar que um Projeto de PAD
**/

ALTER TABLE tb_projetopedagogico
ADD bl_renovar BIT NOT NULL DEFAULT 1 


/**
2. Marcar como bl_renovar= 1 os Projetos Pedag�gicos que n�o s�o PAD em produ��o.
Resumindo, todos os PP da gradua��o ser�o marcados com bl_renovar = 1, exceto os PP PAD.
********************************************************************************
Pesquisando atuais projetos PAD:
|	SELECT * from tb_projetopedagogico				|	
|		where st_projetopedagogico like '% - PAD%'  |
**/


BEGIN TRANSACTION

UPDATE tb_projetopedagogico SET bl_renovar = 0
WHERE id_projetopedagogico in (SELECT id_projetopedagogico from tb_projetopedagogico
where st_projetopedagogico like '% - PAD%')

--ROLLBACK
COMMIT

/** MENU DA FUNCIONALIDADE DE ACESSO DO PROFESSOR EM LOTE
	  G2 > Pedag�gico > Gest�o de Sala > Professores
**/

BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_funcionalidade on
INSERT INTO [dbo].[tb_funcionalidade]
([id_funcionalidade]
,[st_funcionalidade]
,[id_funcionalidadepai]
,[bl_ativo]
,[id_situacao]
,[st_classeflex]
,[st_urlicone]
,[id_tipofuncionalidade]
,[st_classeflexbotao]
,[bl_pesquisa]
,[bl_lista]
,[id_sistema]
,[st_urlcaminho]
,[bl_visivel]
)
VALUES

(690,'Professores',562,1,123,'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa','/img/ico/flag.png',3,null,0,0, 1, '/professores' , 1)

GO
SET IDENTITY_INSERT dbo.tb_funcionalidade off

COMMIT




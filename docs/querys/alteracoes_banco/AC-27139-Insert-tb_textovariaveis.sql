-- AC-27139

SET IDENTITY_INSERT tb_textovariaveis ON;

INSERT INTO tb_textovariaveis (id_textovariaveis, id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
VALUES
(406, 3, 'id_matricula', '#grid_disciplina_sala_PRR#', 'gerarGridDisciplinasSalaPRR', 2)
-- (406, 3, 'id_matricula', '#grid_atividades_presenciais#', 'gerarGridAtividadesPresenciais', 2);

SET IDENTITY_INSERT tb_textovariaveis OFF;

-- alterando tabela tb_log, inserindo colunas definidas na reuni�o da sprint --
ALTER TABLE [dbo].[tb_log] ADD [st_coluna] varchar(100) NULL;
ALTER TABLE [dbo].[tb_log] ADD [id_operacao] int NULL;


-- criando a tabela de opera��es do log --
CREATE TABLE [dbo].[tb_operacaolog] (
[id_operacaolog] int NOT NULL IDENTITY(1,1) ,
[st_nomeoperacaolog] varchar(10) NOT NULL ,
[st_descricaooperacaolog] varchar(255) NULL ,
PRIMARY KEY ([id_operacaolog])
)

GO

-- Insert na tb_operacaolog --
BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_operacaolog on
insert into dbo.tb_operacaolog (id_operacaolog, st_nomeoperacaolog, st_descricaooperacaolog) VALUES
(1, 'update', 'Altera��o de registro.'),
(2, 'insert', 'Inser��o de registro.'),
(3, 'delete', 'Dele��o de registro.')
GO
SET IDENTITY_INSERT dbo.tb_operacaolog off

COMMIT


-- Criando o relacionamento com a tb_operacaolog --
ALTER TABLE [dbo].[tb_log] ADD CONSTRAINT [FK_tb_log_id_operacao_tb_operacaolog] FOREIGN KEY ([id_operacao]) REFERENCES [dbo].[tb_operacaolog] ([id_operacaolog]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO



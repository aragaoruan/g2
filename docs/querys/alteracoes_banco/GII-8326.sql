CREATE VIEW [dbo].[vw_errocontrato] AS
  SELECT
    v.id_venda,
    u.st_nomecompleto,
    e.st_nomeentidade,
    m.dt_aceita
  FROM tb_pa_marcacaoetapa m
    JOIN tb_venda v ON v.id_venda = m.id_venda
    JOIN tb_usuario u ON u.id_usuario = v.id_usuario
    JOIN tb_entidade e ON e.id_entidade = v.id_entidade
  WHERE v.id_evolucao = 10
        AND v.bl_renovacao = 0
        AND v.bl_ativo = 1
        AND v.bl_contratocorrigido IS NULL
        OR v.bl_contratocorrigido = 0
           AND m.dt_aceita IS NOT NULL
           AND v.dt_confirmacao BETWEEN '2017-01-10' AND '2017-08-18'
           AND m.dt_aceita BETWEEN '2017-01-10' AND '2017-08-18'
           AND v.id_entidade IN (
    353, --Polo UNYLEYA ?  Asa Sul
         354, --Polo UNYLEYA ? Rio de Janeiro
         400, --Polo UNYLEYA ?  Ribeir�o Preto
         401, --Polo UNYLEYA ? S�o Paulo
         402, --Polo UNYLEYA ? �guas Claras
         654, --Polo UNYLEYA ? Campos
         655, --Polo UNYLEYA  ? Montes Claros
         656, --Polo UNYLEYA ? Belo Horizonte
         657, --Polo UNYLEYA  ? Santos
         658, --Polo UNYLEYA  ? Vit�ria
         1008, --Polo UNYLEYA ? Salvador
               1009, --Polo UNYLEYA ? Recife
               1010, --Polo UNYLEYA ? Porto Alegre
               1011, --Polo UNYLEYA ? Goi�nia
               1018, --Polo UNYLEYA ? Cuiab�
               1019, --Polo UNYLEYA ? Manaus
               1020, --Polo UNYLEYA ? Uberl�ndia
               1021, --Polo UNYLEYA ? Juiz de Fora
               1062, --Polo UNYLEYA ? Fortaleza
               1063, --Polo UNYLEYA ? Campo Grande
               1064, --Polo UNYLEYA ? Caruaru
    1065, --Polo UNYLEYA ? Petrolina
    1066, --Polo UNYLEYA ? Caxias do Sul
    1067, --Polo UNYLEYA ? Curitiba
    1068  --Polo UNYLEYA ? Florian�polis
  );
GO


ALTER TABLE tb_venda
  ADD bl_contratocorrigido BIT DEFAULT 0;
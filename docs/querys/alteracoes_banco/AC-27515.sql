/****************************************
***** INSERT na tb_texto categoria ******
****************************************/
BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_textocategoria on
INSERT INTO tb_textocategoria 
(id_textocategoria, st_textocategoria, st_descricao, st_metodo)
VALUES
 (15, 'Cabe�alho', 'Cabe�alho para textos de sistemas do tipo declara��o', NULL)
,(16, 'Rodap�', 'Rodap� para textos de sistemas do tipo declara��o', NULL)
SET IDENTITY_INSERT dbo.tb_textocategoria off
COMMIT

/** Add relacionamento **/

BEGIN TRANSACTION 
INSERT INTO tb_textoexibicaocategoria
(id_textoexibicao, id_textocategoria)
VALUES 
(1, 15),
(1, 16)
COMMIT


/** ADD campo id_cabecalho e id_rodape ao texto sistema **/
BEGIN TRANSACTION 
ALTER TABLE tb_textosistema
ADD id_rodape INT NULL , 
 id_cabecalho INT NULL

 COMMIT

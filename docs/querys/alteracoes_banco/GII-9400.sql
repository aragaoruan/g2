-- Criando o menu Encaminhar Ocorrências

SET IDENTITY_INSERT tb_funcionalidade ON

INSERT INTO tb_funcionalidade
(id_funcionalidade, st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao,
 st_classeflex, st_urlicone, id_tipofuncionalidade, nu_ordem, st_classeflexbotao, bl_pesquisa,
 bl_lista, id_sistema, st_ajuda, st_target, st_urlcaminho, bl_visivel, bl_relatorio,
 st_urlcaminhoedicao, bl_delete)
VALUES
  (845, 'Encaminhar Ocorrências', 428, 1, 123,
        'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa', '/img/ico/keyboard.png', 3, NULL, NULL, 0,
   0, 1, NULL, NULL, 'encaminhar-ocorrencias', 1, 0, NULL, 0);

SET IDENTITY_INSERT tb_funcionalidade OFF
INSERT INTO tb_funcionalidade

(st_funcionalidade,
id_funcionalidadepai,
bl_ativo,
id_situacao,
id_tipofuncionalidade,
bl_pesquisa,
bl_lista,
id_sistema,
st_urlcaminho,
bl_visivel,
bl_relatorio,
bl_delete)

VALUES (
'Gerar Etiqueta', /*Nome da funcionalidade*/
157, /*Id da funcionalidade Pai (Cadastros)*/
1, /*bl_ativo (sim)*/
123, /*Id da situação: 123 (funcionalidade em HTML)*/
3, /*Id do tipo da funcionalidade: 3 (PerfilSubMenuLateralTO)*/
0, /*bl_pesquisa: false*/
0, /*bl_lista: false*/
1, /*id_sistema: 1 (Gestor)*/
'/gerar-etiqueta', /*url caminho (será utilizada na chamada do controller / renderização do html.*/
1, /*bl_visivel (sim)*/
0, /*bl_relatorio (nope)*/
0 /*bl_delete (nope).*/
);


CREATE VIEW [dbo].[vw_geraretiquetas] AS

SELECT

ed.id_entregadeclaracao,
ed.id_matricula,
ed.id_situacao,
ed.dt_solicitacao,
mat.id_usuario,
mat.id_entidadematricula,
u.st_nomecompleto,
u.st_cpf,
ts.st_textosistema,
en.st_endereco,
en.st_complemento,
en.sg_uf,
en.st_bairro,
en.st_cep,
en.nu_numero,
mun.st_nomemunicipio,
ce.st_email

FROM tb_entregadeclaracao as ed
LEFT JOIN tb_matricula as mat ON ed.id_matricula = mat.id_matricula
LEFT JOIN tb_usuario as u ON u.id_usuario = mat.id_usuario
LEFT JOIN tb_textosistema as ts ON ts.id_textosistema = ed.id_textosistema
LEFT JOIN tb_pessoaendereco as pe ON pe.id_usuario = mat.id_usuario
LEFT JOIN tb_endereco as en ON en.id_endereco = pe.id_endereco
LEFT JOIN tb_municipio as mun ON mun.id_municipio = en.id_municipio
LEFT JOIN tb_contatosemailpessoaperfil AS cepp ON (cepp.id_usuario = mat.id_usuario AND cepp.bl_ativo = 1 AND cepp.bl_padrao = 1)
LEFT JOIN tb_contatosemail AS ce ON ce.id_email = cepp.id_email
WHERE en.bl_ativo = 1 AND en.id_tipoendereco = 1 AND ed.id_situacao = 79;

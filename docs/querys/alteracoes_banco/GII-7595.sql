SET IDENTITY_INSERT dbo.tb_funcionalidade ON

INSERT INTO "tb_funcionalidade"
("id_funcionalidade",
 "st_funcionalidade",
 "id_funcionalidadepai",
 "bl_ativo",
 "id_situacao",
 "st_classeflex",
 "st_urlicone",
 "id_tipofuncionalidade",
 "nu_ordem",
 "st_classeflexbotao",
 "bl_pesquisa",
 "bl_lista",
 "id_sistema",
 "st_ajuda",
 "st_target",
 "st_urlcaminho",
 "bl_visivel",
 "bl_relatorio",
 "st_urlcaminhoedicao",
 "bl_delete")

VALUES (784, 'Transferência de Curso', 257, 1, 123, NULL, NULL, 3, 0, NULL, 0, 0, 1, NULL, NULL, '784', 1, 1, NULL, 0);
SET IDENTITY_INSERT dbo.tb_funcionalidade OFF
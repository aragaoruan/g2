USE [G2H_EAD1]
GO

/****** Object: Table [dbo].[tb_cancelamento] Script Date: 20/01/2015 16:17:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tb_cancelamento](
[id_cancelamento] [int] IDENTITY(1,1) NOT NULL,
[id_evolucao] [int] NOT NULL,
[dt_solicitacao] [datetime2](7) NOT NULL CONSTRAINT [DF__tb_cancelamento__dt_soli] DEFAULT (getdate()),
[nu_diascancelamento] [int] NOT NULL,
[st_observacao] [varchar](255) NULL,
[nu_valorbruto] [numeric](10, 2) NULL,
[nu_valornegociado] [numeric](10, 2) NULL,
[nu_valordiferenca] [numeric](10, 2) NULL,
[nu_cargahoraria] [int] NULL,
[nu_valorhoraaula] [numeric](10, 2) NULL,
[nu_valormaterial] [numeric](10, 2) NULL,
[nu_valorutilizado
material] [numeric](10, 2) NULL,
	[nu_valordesembolsado] [numeric](10, 2) NULL,
	[nu_cargahorariacursada] [int] NULL,
	[nu_valortotal] [numeric](10, 2) NULL,
	[nu_multaporcentagem] [numeric](10, 2) NULL,
	[nu_valormulta] [numeric](10, 2) NULL,
	[nu_totalutilizado] [numeric](10, 2) NULL,
	[nu_valordevolucao] [numeric](10, 2) NULL,
	[st_observacaocalculo] [varchar](255) NULL,
	[bl_cartagerada] [bit] NULL DEFAULT ((0)),
	[bl_cancelamentofinalizado] [bit] NULL DEFAULT ((0)),
 CONSTRAINT [tb_cancelamento_pk] PRIMARY KEY CLUSTERED 
(
	[id_cancelamento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tb_cancelamento]  WITH CHECK ADD FOREIGN KEY([id_evolucao])
REFERENCES [dbo].[tb_evolucao] ([id_evolucao])
GO
---------------------------------------------------------------------------------------------------------------------------

CREATE TABLE [dbo].[tb_motivo](
	[id_motivo] [int] IDENTITY(1,1) NOT NULL,
	[st_motivo] [varchar](255) NULL,
	[bl_ativo] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_motivo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


---------------------------------------------------------------------------------------------------------------------------

CREATE TABLE [dbo].[tb_motivocancelamento](
	[id_motivocancelamento] [int] IDENTITY(1,1) NOT NULL,
	[id_cancelamento] [int] NOT NULL,
	[id_motivo] [int] NOT NULL,
	[id_usuariocadastro] [int] NOT NULL,
	[dt_cadastro] [datetime2](7) NOT NULL CONSTRAINT [DF__tb_motivocanc__dt_ca]  DEFAULT (getdate()),
	[bl_ativo] [bit] NOT NULL CONSTRAINT [bl_ativo_tb_motivocancelamento]  DEFAULT ((1)),
 CONSTRAINT [tb_motivocancelamento_pk] PRIMARY KEY CLUSTERED 
(
	[id_motivocancelamento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tb_motivocancelamento]  WITH CHECK ADD FOREIGN KEY([id_cancelamento])
REFERENCES [dbo].[tb_cancelamento] ([id_cancelamento])
GO

ALTER TABLE [dbo].[tb_motivocancelamento]  WITH CHECK ADD FOREIGN KEY([id_motivo])
REFERENCES [dbo].[tb_motivo] ([id_motivo])
GO

ALTER TABLE [dbo].[tb_motivocancelamento]  WITH CHECK ADD FOREIGN KEY([id_usuariocadastro])
REFERENCES [dbo].[tb_usuario] ([id_usuario])
GO

---------------------------------------------------------------------------------------------------------------------------


CREATE TABLE [dbo].[tb_cartacredito](
	[id_cartacredito] [int] IDENTITY(1,1) NOT NULL,
	[nu_valororiginal] [numeric](18, 2) NOT NULL,
	[dt_cadastro] [datetime2](7) NOT NULL CONSTRAINT [DF__tb_carta__dt_cada]  DEFAULT (getdate()),
	[id_usuariocadastro] [int] NOT NULL,
	[id_situacao] [int] NOT NULL,
	[id_usuario] [int] NOT NULL,
	[bl_ativo] [bit] NOT NULL DEFAULT ((1)),
	[id_entidade] [int] NOT NULL,
	[id_cancelamento] [int] NULL,
 CONSTRAINT [tb_carta_pk] PRIMARY KEY CLUSTERED 
(
	[id_cartacredito] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tb_cartacredito]  WITH CHECK ADD FOREIGN KEY([id_situacao])
REFERENCES [dbo].[tb_situacao] ([id_situacao])
GO

ALTER TABLE [dbo].[tb_cartacredito]  WITH CHECK ADD FOREIGN KEY([id_usuariocadastro])
REFERENCES [dbo].[tb_usuario] ([id_usuario])
GO

ALTER TABLE [dbo].[tb_cartacredito]  WITH CHECK ADD  CONSTRAINT [FK_tb_cartacredito_reference_tb_cancelamento] FOREIGN KEY([id_cancelamento])
REFERENCES [dbo].[tb_cancelamento] ([id_cancelamento])
GO

ALTER TABLE [dbo].[tb_cartacredito] CHECK CONSTRAINT [FK_tb_cartacredito_reference_tb_cancelamento]
GO

ALTER TABLE [dbo].[tb_cartacredito]  WITH CHECK ADD  CONSTRAINT [tb_cartacredito_tb_entidade_fk] FOREIGN KEY([id_entidade])
REFERENCES [dbo].[tb_entidade] ([id_entidade])
GO

ALTER TABLE [dbo].[tb_cartacredito] CHECK CONSTRAINT [tb_cartacredito_tb_entidade_fk]
GO

ALTER TABLE [dbo].[tb_cartacredito]  WITH CHECK ADD  CONSTRAINT [tb_cartacredito_tb_usuario_fk] FOREIGN KEY([id_usuario])
REFERENCES [dbo].[tb_usuario] ([id_usuario])
GO

ALTER TABLE [dbo].[tb_cartacredito] CHECK CONSTRAINT [tb_cartacredito_tb_usuario_fk]
GO

---------------------------------------------------------------------------------------------------------------------------

CREATE TABLE [dbo].[tb_vendacartacredito](
	[id_vendacartacredito] [int] IDENTITY(1,1) NOT NULL,
	[id_cartacredito] [int] NOT NULL,
	[id_venda] [int] NOT NULL,
	[nu_valorutilizado] [numeric](18, 2) NULL,
 CONSTRAINT [tb_vendacartacredito_pk] PRIMARY KEY CLUSTERED 
(
	[id_vendacartacredito] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tb_vendacartacredito]  WITH CHECK ADD FOREIGN KEY([id_cartacredito])
REFERENCES [dbo].[tb_cartacredito] ([id_cartacredito])
GO

ALTER TABLE [dbo].[tb_vendacartacredito]  WITH CHECK ADD FOREIGN KEY([id_venda])
REFERENCES [dbo].[tb_venda] ([id_venda])
GO

---------------------------------------------------------------------------------------------------------------------------

INSERT INTO tb_situacao (st_situacao, st_tabela, st_campo, st_descricaosituacao)
	VALUES ('Ativa', 'tb_cartacredito', 'id_situacao', 'Carta de Crédito pode ser utilizada')
INSERT INTO tb_situacao (st_situacao, st_tabela, st_campo, st_descricaosituacao)
	VALUES ('Inativa', 'tb_cartacredito', 'id_situacao', 'Carta de Crédito já foi utilizada')

---------------------------------------------------------------------------------------------------------------------------

insert into tb_motivo (st_motivo, bl_ativo) values ('Não se adaptou à modalidade EAD ou teve dificuldades com a utilização da internet.', 1); 
insert into tb_motivo (st_motivo, bl_ativo) values ('Atendimento precário ou dificuldades de comunicação com a instituição.', 1); 
insert into tb_motivo (st_motivo, bl_ativo) values ('Atraso no recebimento de Login e Senha.', 1); 
insert into tb_motivo (st_motivo, bl_ativo) values ('Matrial didático inadequado ou desatualizado.', 1); 
insert into tb_motivo (st_motivo, bl_ativo) values ('Curso Insatisfatório ou não atende à expectativa.', 1); 
insert into tb_motivo (st_motivo, bl_ativo) values ('Atraso no recebimento do Matrial impresso.', 1); 
insert into tb_motivo (st_motivo, bl_ativo) values ('Problemas com o Tutor, Orientador ou Coordenador.', 1); 
insert into tb_motivo (st_motivo, bl_ativo) values ('Plataforma com inconsistências ou incompleta.', 1); 
insert into tb_motivo (st_motivo, bl_ativo) values ('Curso não oferecido.', 1); 
insert into tb_motivo (st_motivo, bl_ativo) values ('Motivos Pessoais: Financeiros, Saúde, Falta de tempo, etc.', 1); 
insert into tb_motivo (st_motivo, bl_ativo) values ('Não Respondeu.', 1); 

---------------------------------------------------------------------------------------------------------------------------

insert into tb_evolucao (st_evolucao, st_tabela, st_campo) values ('Normal','tb_motivocancelamento','id_evolucao');
insert into tb_evolucao (st_evolucao, st_tabela, st_campo) values ('Pedido de Cancelamento','tb_motivocancelamento','id_evolucao');
insert into tb_evolucao (st_evolucao, st_tabela, st_campo) values ('Processo de Cancelamento','tb_motivocancelamento','id_evolucao');
insert into tb_evolucao (st_evolucao, st_tabela, st_campo) values ('Finalizado','tb_motivocancelamento','id_evolucao');
insert into tb_evolucao (st_evolucao, st_tabela, st_campo) values ('Desistiu','tb_motivocancelamento','id_evolucao');

---------------------------------------------------------------------------------------------------------------------------
USE [G2H_EAD1]
GO

/****** Object:  Table [dbo].[tb_logcancelamento]    Script Date: 29/01/2015 11:58:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tb_logcancelamento](
	[id_logcancelamento] [int] IDENTITY(1,1) NOT NULL,
	[id_matricula] [int] NOT NULL,
	[id_cancelamento] [int] NOT NULL,
	[id_usuariocadastro] [int] NOT NULL,
	[dt_cadastro] [datetime2](7) NULL,
	[id_evolucao] [int] NULL,
	[id_situacao] [int] NULL,
	[st_motivo] [varchar](1000) NULL,
	[id_evolucaocancelamento] [int] NULL,
	[nu_valorproporcionalcursado] [decimal](18, 2) NULL,
	[nu_valordevolucao] [decimal](18, 2) NULL,
	[bl_cartacredito] [bit] NULL,
	[nu_valormaterialutilizado] [decimal](18, 2) NULL,
	[nu_cargahorariautilizada] [int]NULL,
	[nu_percentualmulta] [decimal](18, 2) NULL,
PRIMARY KEY CLUSTERED
(
	[id_logcancelamento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tb_logcancelamento]  WITH CHECK ADD FOREIGN KEY([id_cancelamento])
REFERENCES [dbo].[tb_cancelamento] ([id_cancelamento])
GO

ALTER TABLE [dbo].[tb_logcancelamento]  WITH CHECK ADD FOREIGN KEY([id_evolucao])
REFERENCES [dbo].[tb_evolucao] ([id_evolucao])
GO

ALTER TABLE [dbo].[tb_logcancelamento]  WITH CHECK ADD FOREIGN KEY([id_evolucaocancelamento])
REFERENCES [dbo].[tb_evolucao] ([id_evolucao])
GO

ALTER TABLE [dbo].[tb_logcancelamento]  WITH CHECK ADD FOREIGN KEY([id_matricula])
REFERENCES [dbo].[tb_matricula] ([id_matricula])
GO

ALTER TABLE [dbo].[tb_logcancelamento]  WITH CHECK ADD FOREIGN KEY([id_situacao])
REFERENCES [dbo].[tb_situacao] ([id_situacao])
GO

ALTER TABLE [dbo].[tb_logcancelamento]  WITH CHECK ADD FOREIGN KEY([id_usuariocadastro])
REFERENCES [dbo].[tb_usuario] ([id_usuario])
GO



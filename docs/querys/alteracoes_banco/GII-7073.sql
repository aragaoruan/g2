CREATE TABLE tb_turmasala
(
    id_salaturma INT PRIMARY KEY NOT NULL IDENTITY (1,1),
    id_saladeaula INT NOT NULL FOREIGN KEY REFERENCES tb_saladeaula(id_saladeaula) ,
    id_turma INT NOT NULL FOREIGN KEY REFERENCES tb_turma(id_turma),
    dt_cadastro DATETIME DEFAULT getdate()
)

alter table tb_saladeaula add bl_vincularturma bit not null DEFAULT 0;
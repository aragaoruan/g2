ALTER TABLE dbo.tb_prematriculadisciplina ADD dt_cadastro DATETIME2 DEFAULT getdate() NULL;
ALTER TABLE tb_prematriculadisciplina ADD id_usuariocadastro INT FOREIGN KEY REFERENCES tb_usuario(id_usuario);

SET IDENTITY_INSERT tb_tipotramite ON;
insert into tb_tipotramite (id_tipotramite,id_categoriatramite, st_tipotramite) VALUES (27,3, 'Alterando disciplina na grade');
SET IDENTITY_INSERT tb_tipotramite OFF;
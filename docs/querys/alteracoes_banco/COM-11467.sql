SET IDENTITY_INSERT dbo.tb_itemconfiguracao ON
INSERT INTO dbo.tb_itemconfiguracao
        ( id_itemconfiguracao ,
          st_itemconfiguracao ,
          st_descricao ,
          st_default
        )
VALUES  (  17 , -- id_itemconfiguracao - int
          'Solicitar atualização de contrato na loja' , -- st_itemconfiguracao - nvarchar(100)
          'Solicitar atualização de contrato na loja' , -- st_descricao - nvarchar(255)
          ''  -- st_default - nvarchar(200)
        );

SET IDENTITY_INSERT dbo.tb_itemconfiguracao OFF

 //deve-se inserir o o esquemaconfiguraçãoitem para determinado esquema, aqui estou inserindo para o esquema
 //3, que em homologação é imp

SET IDENTITY_INSERT dbo.tb_esquemaconfiguracaoitem ON
 		INSERT INTO dbo.tb_esquemaconfiguracaoitem
        ( id_esquemaconfiguracao ,
          id_itemconfiguracao ,
          st_valor
        )
VALUES  ( 3 , -- id_esquemaconfiguracao - int
          17 , -- id_itemconfiguracao - int
          '1'  -- st_valor - varchar(max)
        );
SET IDENTITY_INSERT dbo.tb_esquemaconfiguracaoitem OFF
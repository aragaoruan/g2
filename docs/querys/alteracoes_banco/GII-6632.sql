ALTER VIEW [dbo].[vw_alocacaoalunosala]
AS

SELECT DISTINCT
  mt.id_matricula,
  pp.id_projetopedagogico,
  pp.st_projetopedagogico,
  mdt.id_disciplina,
  mdt.st_disciplina,
  salan.id_alocacao AS id_alocacaonormal,
  salan.id_saladeaula AS id_saladeaulanormal,
  salan.st_saladeaula AS st_saladeaulanormal,
  salan.id_matriculadisciplina AS id_matriculadisciplinanor,
  salan.dt_abertura AS dt_aberturanormal,
  salap.id_alocacao AS id_alocacaoprr,
  salap.id_saladeaula AS id_saladeaulaprr,
  salap.st_saladeaula AS st_saladeaulaprr,
  salap.id_matriculadisciplina AS id_matriculadisciplinaprr,
  salap.dt_abertura AS dt_aberturaprr,
  mdt.id_modulo,
  mt.id_usuario,
  mdt.id_trilha,
  mdt.id_tipodisciplina,
  salan.dt_inicioalocacao AS dt_inicioalocacaonormal,
  salan.nu_diasextensao AS nu_diasextensaonormal,
  salan.nu_diasaluno AS nu_diasalunonormal,
  salan.id_tiposaladeaula AS id_tiposalanormal,
   salap.dt_inicioalocacao AS dt_inicioalocacaoprr,
  salap.nu_diasextensao AS nu_diasextensaoprr,
  salap.nu_diasaluno AS nu_diasalunoprr,
  salap.id_tiposaladeaula AS id_tiposalaprr,
  salan.nu_diasacessoextencao AS nu_diasacessoatualn,
  salap.nu_diasacessoextencao AS nu_diasacessoatualp,
  md.id_situacao as id_situacaomatriculadisciplina
FROM tb_matricula AS mt
JOIN tb_projetopedagogico AS pp
  ON mt.id_projetopedagogico = pp.id_projetopedagogico
JOIN vw_modulodisciplinaprojetotrilha AS mdt
  ON mdt.id_projetopedagogico = pp.id_projetopedagogico
  AND mdt.bl_ativo = 1
JOIN tb_matriculadisciplina as md on md.id_matricula = mt.id_matricula  AND md.id_disciplina = mdt.id_disciplina
OUTER APPLY (SELECT
  sal.id_saladeaula,
  sal.st_saladeaula,
  al.id_alocacao,
  sal.id_categoriasala,
  md.id_matriculadisciplina,
  sal.dt_abertura,
  al.dt_inicio AS dt_inicioalocacao,
  sal.nu_diasextensao,
  al.nu_diasextensao AS nu_diasaluno,
  sal.id_tiposaladeaula,
   DATEDIFF(DAY,GETDATE(),(DATEADD(DAY,(al.nu_diasextensao+sal.nu_diasextensao),al.dt_inicio))) AS nu_diasacessoextencao
FROM tb_saladeaula AS sal
JOIN vw_saladisciplinaalocacao AS sa
  ON sa.id_matricula = mt.id_matricula
  AND sa.id_alocacao IS NOT NULL
  AND sa.id_categoriasala = 1
  AND sa.id_disciplina = mdt.id_disciplina
JOIN tb_alocacao al
  ON al.id_matriculadisciplina = md.id_matriculadisciplina
  AND al.id_saladeaula = sa.id_saladeaula
  AND al.bl_ativo = '1'
WHERE ((sal.dt_encerramento >= CAST(GETDATE() AS DATE))
OR (sal.dt_encerramento IS NULL)
OR (al.id_saladeaula = sa.id_saladeaula))
--AND sal.id_situacao = 8
AND sal.bl_ativa = 1
AND sal.id_categoriasala = 1
AND sal.id_saladeaula = sa.id_saladeaula) AS salan
OUTER APPLY (SELECT
  sal.id_saladeaula,
  sal.st_saladeaula,
  al.id_alocacao,
  sal.id_categoriasala,
  md.id_matriculadisciplina,
  sal.dt_abertura,
    al.dt_inicio AS dt_inicioalocacao,
  sal.nu_diasextensao,
  al.nu_diasextensao AS nu_diasaluno,
    sal.id_tiposaladeaula,
   DATEDIFF(DAY,GETDATE(),(DATEADD(DAY,(al.nu_diasextensao+sal.nu_diasextensao),al.dt_inicio))) AS nu_diasacessoextencao
FROM tb_saladeaula AS sal
JOIN vw_saladisciplinaalocacao AS sa
  ON sa.id_matricula = mt.id_matricula
  AND sa.id_alocacao IS NOT NULL
  AND sa.id_categoriasala = 2
JOIN tb_alocacao al
  ON al.id_matriculadisciplina = md.id_matriculadisciplina
  AND al.id_saladeaula = sa.id_saladeaula
  AND al.bl_ativo = '1'
WHERE ((sal.dt_encerramento >= CAST(GETDATE() AS DATE))
OR (sal.dt_encerramento IS NULL)
OR (al.id_saladeaula = sa.id_saladeaula))
--AND sal.id_situacao = 8
AND sal.bl_ativa = 1
AND sal.id_categoriasala = 2
AND sal.id_saladeaula = sa.id_saladeaula) AS salap
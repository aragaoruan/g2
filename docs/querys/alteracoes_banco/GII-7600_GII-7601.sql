/* Insert das funcionalidades das issues GII-7600 e GII-7601
Antes de executar em produção, verificar o último ID cadastrado no banco
e substituir pelos valores seguintes correspondentes. */

INSERT INTO "tb_funcionalidade"
("id_funcionalidade", "st_funcionalidade", "id_funcionalidadepai",
 "bl_ativo", "id_situacao", "st_classeflex", "st_urlicone",
 "id_tipofuncionalidade", "nu_ordem", "st_classeflexbotao", "bl_pesquisa",
 "bl_lista", "id_sistema", "st_ajuda", "st_target", "st_urlcaminho",
 "bl_visivel", "bl_relatorio", "st_urlcaminhoedicao", "bl_delete")

VALUES (782, 'Lançamento da Venda Produto', 488, 'True',
             123, NULL, NULL, 3, NULL, NULL, 'False', 'False', 1,
        NULL, NULL, '/relatorio-lancamento-venda-produto', 1, 'False', NULL, 'False');
INSERT INTO "tb_funcionalidade"
("id_funcionalidade", "st_funcionalidade", "id_funcionalidadepai",
 "bl_ativo", "id_situacao", "st_classeflex", "st_urlicone",
 "id_tipofuncionalidade", "nu_ordem", "st_classeflexbotao", "bl_pesquisa",
 "bl_lista", "id_sistema", "st_ajuda", "st_target", "st_urlcaminho",
 "bl_visivel", "bl_relatorio", "st_urlcaminhoedicao", "bl_delete")

VALUES (781, 'Lançamento da Venda Produto por Atendente', 488, 'True',
             123, NULL, NULL, 3, NULL, NULL, 'False', 'False', 1,
        NULL, NULL, '/relatorio-lancamento-venda-produto/atendente', 1, 'False', NULL, 'False');
CREATE VIEW rel.vw_relatoriocancelamento
AS

SELECT  e.st_nomeentidade ,
        e.id_entidade AS id_entidade ,
        c.dt_solicitacao ,
        v.id_venda ,
        vwmat.id_matricula ,
        vwmat.st_nomecompleto ,
        vwmat.st_cpf ,
        vwmat.dt_cadastro AS dt_matricula ,
        vwmat.st_email ,
        a.id_areaconhecimento ,
        a.st_areaconhecimento ,
        vwmat.id_projetopedagogico ,
        vwmat.st_projetopedagogico ,
        c.bl_cartagerada ,
        nu_valordevolucao = CASE WHEN bl_cartagerada = 0 
                                      THEN c.nu_valordevolucao 
                                        ELSE NULL
                                          END,
        nu_valorcartacredito = CASE WHEN bl_cartagerada = 1
                                      THEN c.nu_valorcarta
                                        ELSE NULL
                                          END,
        vwmat.st_evolucao ,
        SUBSTRING(( SELECT  ', ' + motivo.st_motivo
                    FROM    dbo.tb_motivocancelamento AS motivocancelamento
                            JOIN tb_motivo AS
 motivo ON motivo.id_motivo = motivocancelamento.id_motivo
                                                        AND motivocancelamento.id_cancelamento = c.id_cancelamento
                  FOR
                    XML PATH('')
                  ), 2, 1000) AS st_motivo,
        c.bl_cancelamentofinalizado ,
        c.nu_diascancelamento ,
        DATEADD(DAY, c.nu_diascancelamento, c.dt_solicitacao) AS dt_cancelamento ,
        v.id_atendente ,
        ua.st_nomecompleto AS st_nomeatendente ,
        v.nu_valorliquido ,
        st_opcaocancelamento = CASE WHEN bl_cartagerada = 1
                                    THEN 'Carta de Crédito'
                                    ELSE 'Devolução'
                               END ,
        st_evolucaocancelamento = CASE WHEN bl_cancelamentofinalizado = 1
                                       THEN 'Finalizado'
                                       ELSE 'Em Andamento'
                                  END,
		dt_finalizacao
FROM    tb_cancelamento AS c
        JOIN dbo.vw_matricula AS vwmat ON c.id_cancelamento = vwmat.id_cancelamento
        JOIN tb_entidade AS e ON e.id_entidade = vwmat.id_entidadematricula
        JOIN dbo.tb_venda AS v ON v.id_venda = vwmat.id_venda
        JOIN tb_areaconhecimento AS a ON a.id_areaconhecimento = vwmat.id_areaconhecimento
        JOIN dbo.tb_evolucao AS ev ON ev.id_evolucao = c.id_evolucao
        LEFT JOIN tb_usuario AS ua ON ua.id_usuario = v.id_atendente

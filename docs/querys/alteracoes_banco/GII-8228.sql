-- GII-8228
ALTER TABLE tb_matriculadiplomacao
  ADD
  dt_equivalenciagerado DATE NULL,
  id_usuarioequivalenciagerado INT NULL,
  CONSTRAINT FK_USUARIO_EQUIV FOREIGN KEY (id_usuarioequivalenciagerado) REFERENCES tb_usuario (id_usuario);
SET IDENTITY_INSERT tb_evolucao ON;
INSERT INTO tb_evolucao (id_evolucao, st_evolucao, st_tabela, st_campo)
VALUES (83, 'Quadro de Equivalência Gerado', 'tb_matricula', 'id_evolucaodiplomacao');
SET IDENTITY_INSERT tb_evolucao OFF;
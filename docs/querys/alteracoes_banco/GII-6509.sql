ALTER VIEW [rel].[vw_cronogramasalas] as
SELECT DISTINCT
        sa.id_saladeaula ,
        st_saladeaula ,
        sa.dt_inicioinscricao ,
        sa.dt_fiminscricao ,
        sa.dt_abertura ,
        sa.dt_encerramento ,
        cast(dt_atualiza as date) as dt_atualiza,
        st_disciplina ,
        dc.st_tituloexibicao AS st_tituloexibicaodisciplina ,
        st_projetopedagogico ,
        aps.id_projetopedagogico,
        uss.st_nomecompleto AS st_nomeprofessor ,
        usp.id_usuario AS id_coordenador,
        usp.st_nomecompleto AS st_nomecoordenador,
        si.st_codsistemacurso AS st_codava,
        sa.id_entidade,
		sa.id_categoriasala,
		CAST(sa.dt_cadastro as date) as dt_cadastro,
		cast(COUNT(ta.id_matriculadisciplina) AS varchar) AS st_alunos,
	CASE DATEPART(MONTH, pl.dt_abertura)
WHEN 1 THEN 'Janeiro'
WHEN 2 THEN 'Fevereiro'
WHEN 3 THEN 'Março'
WHEN 4 THEN 'Abril'
WHEN 5 THEN 'Maio'
WHEN 6 THEN 'Junho'
WHEN 7 THEN 'Julho'
WHEN 8 THEN 'Agosto'
WHEN 9 THEN 'Setembro'
WHEN 10 THEN 'Outubro'
WHEN 11 THEN 'Novembro'
WHEN 12 THEN 'Dezembro'
END AS st_mesoferta
        FROM dbo.tb_saladeaula AS sa
JOIN dbo.tb_areaprojetosala AS aps ON aps.id_saladeaula = sa.id_saladeaula
JOIN dbo.tb_disciplinasaladeaula AS da ON da.id_saladeaula = sa.id_saladeaula
JOIN tb_disciplina AS dc ON dc.id_disciplina = da.id_disciplina
JOIN dbo.tb_projetopedagogico AS pp ON pp.id_projetopedagogico = aps.id_projetopedagogico
LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS ups ON ups.id_saladeaula = aps.id_saladeaula AND ups.bl_titular = 1 and ups.bl_ativo=1
LEFT JOIN dbo.tb_usuario AS uss ON uss.id_usuario = ups.id_usuario
LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS upp ON upp.id_projetopedagogico = pp .id_projetopedagogico AND upp.bl_titular = 1 and upp.bl_ativo=1 and upp.id_disciplina is null
LEFT JOIN dbo.tb_perfil AS tbp ON tbp.id_perfil = upp.id_perfil AND tbp.id_perfilpedagogico = 2
LEFT JOIN dbo.tb_usuario AS usp ON usp.id_usuario = upp.id_usuario
LEFT JOIN dbo.tb_saladeaulaintegracao AS si ON si.id_saladeaula = sa.id_saladeaula
LEFT JOIN dbo.tb_alocacao AS ta ON ta.id_saladeaula = sa.id_saladeaula AND ta.bl_ativo = 1
LEFT JOIN dbo.tb_periodoletivo as pl ON sa.id_periodoletivo = pl.id_periodoletivo
WHERE sa.bl_ativa = 1 AND sa.id_situacao != 74
GROUP by
sa.id_saladeaula,
st_saladeaula ,
        sa.dt_inicioinscricao ,
        sa.dt_fiminscricao ,
        sa.dt_abertura ,
        sa.dt_encerramento ,
        dt_atualiza,
        st_disciplina ,
        dc.st_tituloexibicao ,
        st_projetopedagogico ,
        aps.id_projetopedagogico,
        uss.st_nomecompleto ,
        usp.id_usuario ,
        usp.st_nomecompleto,
        si.st_codsistemacurso,
        sa.id_entidade,
		sa.dt_cadastro,
		pl.dt_abertura,
		sa.id_categoriasala;
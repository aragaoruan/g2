/***
*** INSERT tb_textovariaveis
*** GII-7470
**/


BEGIN TRAN

SET IDENTITY_INSERT dbo.tb_textovariaveis ON

INSERT INTO tb_textovariaveis
([id_textovariaveis]
  , [id_textocategoria]
  , [st_camposubstituir]
  , [st_textovariaveis]
  , [st_mascara]
  , [id_tipotextovariavel])

VALUES
  (439, 2, 'dt_transferencia', '#data_transferencia_curso#', 'convertDataToBr', 2)
  , (440, 2, 'id_matriculaorigem', '#matricula_origem#', NULL, 1)
  , (441, 2, 'st_projetoorigem', '#nome_projeto_origem#', NULL, 1)
  , (442, 2, 'id_matriculadestino', '#matricula_destino#', NULL, 1)
  , (443, 2, 'st_projetodestino', '#nome_projeto_destino#', NULL, 1)
  , (444, 2, 'nu_cargahorariadestino', '#cargahoraria_destino#', NULL, 1)
  , (445, 2, 'nu_valorliquidotransferencia', '#nu_valorliquido_transferencia#', 'formataValor', 2)
  , (446, 2, 'id_vendaaditivo', '#planopagamento_transferencia#', 'gerarGridParcelasTransferencia', 2)

SET IDENTITY_INSERT dbo.tb_textovariaveis OFF

IF @@ERROR > 0
  ROLLBACK
ELSE
  COMMIT

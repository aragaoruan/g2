CREATE VIEW [dbo].[vw_historicoavaliacaoaluno]
AS
SELECT DISTINCT
	id_avaliacaoaluno,
	mt.id_matricula,
	us.st_nomecompleto,
	mdl.id_modulo,
	mdl.st_tituloexibicao AS st_tituloexibicaomodulo,
	dc.id_disciplina,
	dc.st_tituloexibicao AS st_tituloexibicaodisciplina,
	aa.st_nota,
	ag.id_avaliacaoagendamento,
	acr.id_avaliacao,
	av.st_avaliacao,
	md.id_situacao,
	st.st_situacao,
	ac.id_tipoprova,
	acrf.id_avaliacaoconjuntoreferencia,
	(case when avref.id_avaliacaoconjuntoreferencia=acrf.id_avaliacaoconjuntoreferencia then 1 else 0 end) bl_agendamento,
	md.id_evolucao,
	ev.st_evolucao,
	CAST(av.nu_valor AS varchar(100)) AS nu_notamax,
	ac.id_tipocalculoavaliacao,
	av.id_tipoavaliacao,
	ta.st_tipoavaliacao,
	(case when acr.id_avaliacaorecupera is null then 0 else acr.id_avaliacaorecupera end) as id_avaliacaorecupera,
	ag.bl_provaglobal,
	pp.nu_notamaxima,
	pp.nu_percentualaprovacao,
	ag.dt_agendamento,
	aa.bl_ativo,
	aa.dt_avaliacao,
	alc.id_saladeaula,
	ui.st_codusuario,
	ac.id_avaliacaoconjunto,
	pp.st_projetopedagogico,
	mt.id_projetopedagogico,
	aa.dt_defesa,
	aa.st_tituloavaliacao,
	mt.id_entidadeatendimento,
	aa.id_upload,
	dc.id_tipodisciplina,
	aa.st_justificativa,
	md.id_matriculadisciplina,
	aa.id_tiponota,
  sda.dt_cadastro as dt_cadastrosala,
  sda.dt_encerramento as dt_encerramentosala,
  aa.dt_cadastro as dt_cadastroavaliacao,
  aa.id_usuariocadastro as id_usuariocadavaliacao,
  usc.st_nomecompleto  as st_usuariocadavaliacao,
    sdis.id_evolucao as id_evolucaodisciplina,
  sdis.st_evolucao as st_evolucaodisciplina
FROM tb_matriculadisciplina AS md
JOIN tb_matricula AS mt ON md.id_matricula = mt.id_matricula
JOIN dbo.tb_usuario AS us ON us.id_usuario = mt.id_usuario
JOIN tb_modulo AS mdl ON mdl.id_projetopedagogico = mt.id_projetopedagogico	AND mdl.bl_ativo = 1
JOIN tb_modulodisciplina AS mdc ON mdc.id_modulo = mdl.id_modulo AND md.id_disciplina = mdc.id_disciplina AND mdc.bl_ativo = 1
JOIN tb_alocacao AS alc ON  alc.id_matriculadisciplina = md.id_matriculadisciplina and alc.bl_ativo = 1
JOIN tb_avaliacaoconjuntoreferencia AS acrf	ON (acrf.id_saladeaula = alc.id_saladeaula) AND (acrf.dt_fim IS NULL OR acrf.dt_fim >= GETDATE())
JOIN tb_disciplinasaladeaula as dsa on dsa.id_saladeaula = acrf.id_saladeaula
JOIN tb_saladeaula sda on sda.id_saladeaula = acrf.id_saladeaula
JOIN tb_disciplina AS dc ON dc.id_disciplina = mdc.id_disciplina AND dc.id_disciplina = md.id_disciplina and dsa.id_disciplina = dc.id_disciplina
JOIN tb_evolucao as sdis on sdis.id_evolucao = md.id_evolucao
JOIN tb_avaliacaoconjunto AS ac	ON ac.id_avaliacaoconjunto = acrf.id_avaliacaoconjunto AND (acrf.id_saladeaula = alc.id_saladeaula AND id_tipoprova = 2)
JOIN tb_avaliacaoconjuntorelacao AS acr	ON acr.id_avaliacaoconjunto = ac.id_avaliacaoconjunto
JOIN tb_avaliacao AS av	ON av.id_avaliacao = acr.id_avaliacao
JOIN tb_tipoavaliacao AS ta ON ta.id_tipoavaliacao = av.id_tipoavaliacao
LEFT JOIN tb_avaliacaoagendamento AS ag	ON mt.id_matricula = ag.id_matricula AND av.id_avaliacao = ag.id_avaliacao AND ag.id_situacao=68 and ag.nu_presenca=1
LEFT JOIN tb_avalagendamentoref as avref on avref.id_avaliacaoagendamento=ag.id_avaliacaoagendamento and avref.id_avaliacaoconjuntoreferencia = acrf.id_avaliacaoconjuntoreferencia
LEFT JOIN tb_avaliacaoaluno AS aa
	ON aa.id_matricula = mt.id_matricula
	AND aa.id_avaliacaoconjuntoreferencia = acrf.id_avaliacaoconjuntoreferencia
	AND aa.id_avaliacao = av.id_avaliacao
JOIN tb_situacao AS st
	ON st.id_situacao = md.id_situacao
JOIN tb_evolucao AS ev
	ON ev.id_evolucao = md.id_evolucao
JOIN tb_projetopedagogico AS PP
	ON pp.id_projetopedagogico = mt.id_projetopedagogico
LEFT JOIN dbo.tb_usuariointegracao AS ui
	ON ui.id_usuario = mt.id_usuario AND (ui.id_entidade = mt.id_entidadeatendimento OR ui.id_entidade = mt.id_entidadematricula)
LEFT JOIN tb_usuario as usc on usc.id_usuario= aa.id_usuariocadastro
WHERE mt.bl_ativo = 1

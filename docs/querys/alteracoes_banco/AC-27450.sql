/***********************************************************************************
************************************************************************************
					ADD FUNCIONALIDADE
************************************************************************************
***********************************************************************************/
				
BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_funcionalidade on
INSERT INTO [dbo].[tb_funcionalidade]
([id_funcionalidade]
,[st_funcionalidade]
,[id_funcionalidadepai]
,[bl_ativo]
,[id_situacao]
,[st_classeflex]
,[st_urlicone]
,[id_tipofuncionalidade]
,[st_classeflexbotao]
,[bl_pesquisa]
,[bl_lista]
,[id_sistema]
,[st_urlcaminho])
VALUES

(749,'Edi��o lan�amento de frequ�ncia',590 ,1 ,123 ,NULL ,NULL ,7 ,null ,0,0, 1, 749)

GO
SET IDENTITY_INSERT dbo.tb_funcionalidade off

COMMIT


/***********************************************************************************
************************************************************************************
					ADD PERMISSAO
************************************************************************************
***********************************************************************************/
BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_permissao on
INSERT INTO [dbo].tb_permissao
(id_permissao,
st_nomepermissao, 
st_descricao,
bl_ativo)
VALUES
(32
,'Editar lan�amento de frequ�ncia'
,'Permite ao usu�rio editar um lan�amento de frequencia. As vezes a mesma � lan�ada errada e deve ser poss�vel corrigir.'
, 1)
GO
SET IDENTITY_INSERT dbo.tb_permissao off

COMMIT


/***********************************************************************************
************************************************************************************
					ADD PERMISSAO FUNCIONALIDADE
************************************************************************************
***********************************************************************************/
BEGIN TRANSACTION
INSERT INTO tb_permissaofuncionalidade 
	(id_funcionalidade, id_permissao)
	VALUES
	(749, 32)
COMMIT


/***********************************************************************************
************************************************************************************
				ADD TIPO TRAMITE LANCAMENTO DE FREQUENCIA
************************************************************************************
***********************************************************************************/
BEGIN TRANSACTION
	SET IDENTITY_INSERT dbo.tb_tipotramite on
		INSERT INTO tb_tipotramite
			(id_tipotramite , id_categoriatramite, st_tipotramite)
		VALUES
			(23, 5, 'Lan�amento de frequ�ncia')
	SET IDENTITY_INSERT dbo.tb_tipotramite off
COMMIT



/***********************************************************************************
************************************************************************************
				ADD SITUACAO LANCAMENTO DE FREQUENCIA
************************************************************************************
***********************************************************************************/
BEGIN TRANSACTION
	SET IDENTITY_INSERT dbo.tb_situacao on
		INSERT INTO tb_situacao
			(id_situacao , st_situacao, st_tabela, st_campo, st_descricaosituacao)
		VALUES
			(178, 'Presente', 'tb_tramiteagendamento' , 'id_situacao' , 'Presen�a lan�ada como : PRESENTE')
		   ,(179, 'Ausente', 'tb_tramiteagendamento' , 'id_situacao' , 'Presen�a lan�ada como : AUSENTE')
	SET IDENTITY_INSERT dbo.tb_situacao off
COMMIT
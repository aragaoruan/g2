-- criação da funcionalidade
INSERT INTO dbo.tb_funcionalidade
        ( st_funcionalidade ,
          id_funcionalidadepai ,
          bl_ativo ,
          id_situacao ,
          st_classeflex ,
          st_urlicone ,
          id_tipofuncionalidade ,
          nu_ordem ,
          st_classeflexbotao ,
          bl_pesquisa ,
          bl_lista ,
          id_sistema ,
          st_ajuda ,
          st_target ,
          st_urlcaminho ,
          bl_visivel ,
          bl_relatorio ,
          st_urlcaminhoedicao
        )
VALUES  ( 'Regra de Pagamento' , -- st_funcionalidade - varchar(100)
          301 , -- id_funcionalidadepai - int
          1 , -- bl_ativo - bit
          123 , -- id_situacao - int
          '' , -- st_classeflex - varchar(300)
          '' , -- st_urlicone - varchar(1000)
          3 , -- id_tipofuncionalidade - int
          0 , -- nu_ordem - int
          '' , -- st_classeflexbotao - varchar(300)
          0 , -- bl_pesquisa - bit
          0 , -- bl_lista - bit
          1 , -- id_sistema - int
          '' , -- st_ajuda - varchar(max)
          '' , -- st_target - varchar(100)
          'regra-pagamento' , -- st_urlcaminho - varchar(50)
          1 , -- bl_visivel - int
          0 , -- bl_relatorio - bit
          ''  -- st_urlcaminhoedicao - varchar(50)
        )


CREATE TABLE tb_cargahoraria(
    id_cargahoraria INT NOT NULL IDENTITY (1,1),
    nu_cargahoraria NUMERIC NULL
)

CREATE TABLE dbo.tb_tiporegrapagamento (
	id_tiporegrapagamento INT NOT NULL IDENTITY PRIMARY KEY,
	st_tiporegrapagamento VARCHAR(512) NOT NULL
);

INSERT INTO tb_tiporegrapagamento VALUES 
( 'Pagamento por Entidade'),
( 'Pagamento por Área do Conhecimento'),
( 'Pagamento por Projeto Pedaógico'),
( 'Pagamento por Professor'),
( 'Pagamento por Professor no Projeto Pedagógico')

CREATE TABLE tb_regrapagamento(
    id_regrapagamento int NOT NULL IDENTITY (1,1),
    id_tiporegrapagamento int not null,
    id_entidade int not NULL,
    id_area int NULL,
    id_projetopedagogico int NULL,
    id_professor int NULL, 
    id_cargahoraria int not NULL,
    nu_valor numeric not NULL,
    id_tipodisciplina int not NULL,
    dt_cadastro datetime NOT null,
    bl_ativo tinyint
)

ALTER TABLE dbo.tb_regrapagamento
ADD FOREIGN KEY (id_tiporegrapagamento)
REFERENCES tb_tiporegrapagamento(id_tiporegrapagamento)

-- vw_regrapagamento
CREATE VIEW dbo.vw_regrapagamento AS
SELECT
        rp.id_regrapagamento,
        rp.id_tiporegrapagamento,
        trp.st_tiporegrapagamento,
	rp.dt_cadastro,
	rp.id_areaconhecimento,
	st_areaconhecimento,
	rp.id_cargahoraria,
	ch.nu_cargahoraria,
	rp.id_entidade,
	st_nomeentidade AS st_entidade,
	rp.id_professor,
	u.st_nomecompleto as st_professor,
	rp.id_projetopedagogico,
	st_projetopedagogico,
	rp.id_tipodisciplina,
	st_tipodisciplina,
	rp.nu_valor,
        rp.bl_ativo
FROM dbo.tb_regrapagamento rp
LEFT JOIN dbo.tb_tiporegrapagamento trp ON rp.id_tiporegrapagamento = trp.id_tiporegrapagamento
LEFT JOIN dbo.tb_areaconhecimento ac ON rp.id_areaconhecimento = ac.id_areaconhecimento
LEFT JOIN dbo.tb_cargahoraria ch ON rp.id_cargahoraria = ch.id_cargahoraria
LEFT JOIN dbo.tb_entidade e ON rp.id_entidade = e.id_entidade
LEFT JOIN dbo.tb_usuario u ON rp.id_professor = u.id_usuario
LEFT JOIN dbo.tb_projetopedagogico pp ON rp.id_projetopedagogico = pp.id_projetopedagogico
LEFT JOIN dbo.tb_tipodisciplina td ON rp.id_tipodisciplina = td.id_tipodisciplina

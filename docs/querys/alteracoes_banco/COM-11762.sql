-- Script para vincular os modelos de carteirinhas com as entidades abaixo
-- 176 - AVM Pós Presencial Asa Sul
-- 177 - AVM Pós Presencial Asa Norte
-- 178 - AVM Pós Presencial Taguatinga
-- 423 - AVM Pós Presencial Sudoeste
-- 179 - AVM Pós Presencial Águas Claras

begin tran

insert into tb_modelocarteirinhaentidade (id_modelocarteirinha, id_entidade)
select mc.id_modelocarteirinha, e.id_entidade from tb_modelocarteirinha mc
outer apply (select id_entidade from tb_entidade where id_entidade in(176,177,178,179,423)) as e


commit

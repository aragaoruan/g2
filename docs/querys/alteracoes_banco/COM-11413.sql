ALTER TABLE tb_venda ADD dt_atualizado DATETIME2 DEFAULT GETDATE();

-------------------------------------------------------------------

INSERT INTO dbo.tb_sistema
        (id_sistema, st_sistema, bl_ativo, st_conexao )
VALUES  (22, 'Hubspot', -- st_sistema - varchar(255)
          1, -- bl_ativo - bit
          'NULL'  -- st_conexao - varchar(2000)
          )

 ------------------------------------------------------------------
          INSERT INTO dbo.tb_entidadeintegracao
        ( id_entidade ,
          id_usuariocadastro ,
          id_sistema ,
          st_codsistema ,
          dt_cadastro ,
          st_codchave ,
          st_codarea ,
          st_caminho ,
          nu_perfilsuporte ,
          nu_perfilprojeto ,
          nu_perfildisciplina ,
          nu_perfilobservador ,
          nu_perfilalunoobs ,
          nu_perfilalunoencerrado ,
          nu_contexto ,
          st_linkedserver
        )
VALUES  ( 12 , -- id_entidade - int
          1 , -- id_usuariocadastro - int
          22 , -- id_sistema - int
          '' , -- st_codsistema - varchar(max)
          GETDATE() , -- dt_cadastro - datetime2
          '' , -- st_codchave - varchar(max)
          '' , -- st_codarea - varchar(100)
          '' , -- st_caminho - varchar(max)
          NULL , -- nu_perfilsuporte - numeric
          NULL , -- nu_perfilprojeto - numeric
          NULL , -- nu_perfildisciplina - numeric
          NULL , -- nu_perfilobservador - numeric
          NULL , -- nu_perfilalunoobs - numeric
          0 , -- nu_perfilalunoencerrado - int
          0 , -- nu_contexto - int
          ''  -- st_linkedserver - varchar(20)
        )

---------------------------------------------------------------------------
TRIGGER PARA ATUALIZAR A DATA SEMPRE QUE HOUVER UM UPDATE

CREATE TRIGGER trg_UpdateDtAtualizaTbVenda
ON dbo.tb_venda
AFTER UPDATE
AS
    UPDATE dbo.tb_venda
    SET dt_atualizado = GETDATE()
    WHERE id_venda IN (SELECT DISTINCT id_venda FROM Inserted)
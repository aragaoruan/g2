/****** Object:  View [dbo].[vw_aplicadorprova]    Script Date: 16/07/2015 09:54:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[vw_aplicadorprova] as
SELECT
ap.id_aplicadorprova,
ap.id_entidadeaplicador,
ap.id_usuarioaplicador,
ap.id_entidadecadastro,
ap.st_aplicadorprova
, ap.bl_ativo
,id_tipopessoa = case isnull(ap.id_usuarioaplicador, 0)
					WHEN 0
						then 2
					ELSE 1
				 END
,st_tipopessoa = case isnull(ap.id_usuarioaplicador, 0)
					WHEN 0
						then 'Jur�dica'
					ELSE 'F�sica'
				 END
, ( CASE WHEN ap.id_usuarioaplicador is not null
				THEN
					(SELECT st_nomecompleto COLLATE LATIN1_GENERAL_CI_AS FROM tb_usuario where id_usuario = ap.id_usuarioaplicador)
				ELSE
					(SELECT st_nomeentidade COLLATE LATIN1_GENERAL_CI_AS as st_nome FROM tb_entidade where id_entidade = ap.id_entidadeaplicador)
				END) as st_local

FROM
tb_aplicadorprova AS ap




GO



INSERT INTO tb_funcionalidade (
id_funcionalidade,
st_funcionalidade,
id_funcionalidadepai,
bl_ativo,
id_situacao,
st_classeflex,
st_urlicone,
id_tipofuncionalidade,
nu_ordem,
st_classeflexbotao,
bl_pesquisa,
bl_lista,
id_sistema,
st_ajuda,
st_target,
st_urlcaminho,
bl_visivel,
bl_relatorio,
st_urlcaminhoedicao,
bl_delete)
VALUES (
748,
'Carta de Crédito',
194, 1, 123,
'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa',
'/img/ico/bank.png', 3, NULL,
'br.com.ead1.gestor2.view.conteudo.financeiro.cadastrar.CadastrarCartaCredito', 0,
0, 1, NULL, NULL, '/carta-credito', 1, 0, null, 0;

-------------------------------------------------------------------------------------

CREATE view vw_entidadesmesmaholdingparams as
select
	hf.id_entidade,
  e2.st_nomeentidade,
	e.id_entidade as id_entidadeparceira,
	e.st_nomeentidade as st_nomeentidadeparceira,
	h.bl_compartilharcarta as bl_cartacompartilhada
from tb_holdingfiliada hf
join tb_holdingfiliada hf2 on hf2.id_holding = hf.id_holding
join tb_entidade e on e.id_entidade = hf2.id_entidade
join tb_entidade e2 on hf.id_entidade = e2.id_entidade
join tb_holding as h on hf.id_holding = h.id_holding;

-------------------------------------------------------------------------------------

ALTER VIEW [dbo].[vw_cartacredito] as
SELECT
	cc.id_cartacredito,
	cancelamento.id_cancelamento,
	cc.dt_cadastro,
	cc.nu_valororiginal,
	(SELECT
		ISNULL(CONVERT(FLOAT, SUM(utilizado.nu_valorutilizado)), 0)
	FROM tb_vendacartacredito AS utilizado
	JOIN tb_venda utilizadovenda
		ON utilizadovenda.id_venda = utilizado.id_venda
	WHERE utilizadovenda.id_evolucao NOT IN (7, 8)
	AND utilizado.id_cartacredito = cc.id_cartacredito)
	AS nu_valorutilizado,
	(cc.nu_valororiginal - (SELECT
		ISNULL(CONVERT(FLOAT, SUM(disponivel.nu_valorutilizado)), 0)
	FROM tb_vendacartacredito AS disponivel
	JOIN tb_venda disponivelvenda
		ON disponivelvenda.id_venda = disponivel.id_venda
	WHERE disponivelvenda.id_evolucao NOT IN (7, 8)
	AND disponivel.id_cartacredito = cc.id_cartacredito)
	) AS nu_valordisponivel,
	cc.id_usuariocadastro,
	cc.id_usuario,
	cc.bl_ativo,
	cc.id_situacao,
	s.st_situacao,
	cc.id_entidade,
  cc.dt_validade,
	e.st_nomeentidade
FROM tb_cartacredito AS cc
JOIN tb_situacao s
	ON s.id_situacao = cc.id_situacao
JOIN tb_entidade e
	ON e.id_entidade = cc.id_entidade
LEFT JOIN tb_cancelamento cancelamento
  on cancelamento.id_cancelamento = cc.id_cancelamento


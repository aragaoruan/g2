CREATE TABLE tb_justificativaviacontrato (
	id_justivicativaviacontrato INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	id_venda INT NOT NULL,
	nu_viacontrato INT NOT NULL,
	st_justificativa VARCHAR(1000) NOT NULL,
	dt_cadastro DATETIME DEFAULT GETDATE(),
	id_usuariocadastro INT FOREIGN KEY REFERENCES dbo.tb_usuario(id_usuario)
);


SET IDENTITY_INSERT dbo.tb_tipotramite ON;
INSERT INTO dbo.tb_tipotramite
        ( id_tipotramite,
          id_categoriatramite ,
          st_tipotramite
        )
VALUES  ( 17,
          3, -- id_categoriatramite - int
          'Contrato'  -- st_tipotramite - varchar(30)
        );
SET IDENTITY_INSERT dbo.tb_tipotramite OFF;


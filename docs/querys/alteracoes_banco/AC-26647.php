/**
SQL AC-26647
*/


ALTER TABLE dbo.tb_usuarioperfilentidadereferencia ADD dt_inicio DATETIME NULL, dt_fim DATETIME NULL;


CREATE VIEW vw_disciplinaperfilpedagogicoentidade AS
SELECT dc.id_disciplina, dc.st_disciplina, pe.id_entidade, pp.id_projetopedagogico, pf.id_perfil, pf.id_perfilpedagogico, us.id_usuario, us.st_nomecompleto FROM dbo.tb_projetopedagogico AS pp
JOIN dbo.tb_modulo AS md ON md.id_projetopedagogico = pp.id_projetopedagogico AND md.bl_ativo = 1
JOIN dbo.tb_modulodisciplina AS mdd ON mdd.id_modulo = md.id_modulo AND mdd.bl_ativo = 1
JOIN dbo.tb_disciplina AS dc ON dc.id_disciplina = mdd.id_disciplina
JOIN dbo.tb_projetoentidade AS pe ON pe.id_projetopedagogico = pp.id_projetopedagogico
LEFT JOIN dbo.tb_perfil AS pf ON pf.id_entidade = pe.id_entidade
LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS upr ON pf.id_perfil = upr.id_perfil AND upr.id_disciplina = dc.id_disciplina AND upr.id_projetopedagogico = pp.id_projetopedagogico
LEFT JOIN dbo.tb_usuario AS us ON us.id_usuario = upr.id_usuario


INSERT INTO tb_funcionalidade
(st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao, id_tipofuncionalidade, bl_pesquisa, bl_lista, id_sistema, st_urlcaminho, bl_visivel, bl_relatorio)
VALUES ('Titular Certificação', 568, 1, 123, 3, 0, 0, 1, '/titular-certificacao', 1, 0)

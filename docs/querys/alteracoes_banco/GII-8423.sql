ALTER TABLE [dbo].[tb_entidadeintegracao]
  ADD [bl_ativo] BIT NULL DEFAULT 1
GO

ALTER TABLE [dbo].[tb_entidadeintegracao]
  ADD [id_situacao] INT NULL
GO

ALTER TABLE [dbo].[tb_entidadeintegracao]
  ADD [st_titulo] VARCHAR(140) NULL
GO

ALTER TABLE [dbo].[tb_entidadeintegracao]
  ADD CONSTRAINT [FK_tb_entidadeintegracao_tb_situacao]
FOREIGN KEY ([id_situacao]) REFERENCES [dbo].[tb_situacao] ([id_situacao])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
GO


BEGIN TRAN
SET IDENTITY_INSERT dbo.tb_situacao ON
INSERT INTO tb_situacao (
  id_situacao,
  st_situacao,
  st_tabela,
  st_campo,
  st_descricaosituacao
) VALUES (
  198,
  'Ativa',
  'tb_entidadeintegracao',
  'id_situacao',
  'Integração Ativa'
),
  (
    199,
    'Inativa',
    'tb_entidadeintegracao',
    'id_situacao',
    'Integração Inativa'
  )
SET IDENTITY_INSERT dbo.tb_situacao OFF
COMMIT

BEGIN TRAN
UPDATE tb_entidadeintegracao
SET st_titulo = 'Moodle Padrão',
  id_situacao = 198,
  bl_ativo = 1
WHERE id_sistema = 6;

COMMIT

ALTER TABLE dbo.tb_ocorrencia ADD st_tramite VARCHAR(max);
ALTER TABLE dbo.tb_ocorrencia ADD dt_ultimotramite DATETIME2;


UPDATE dbo.tb_ocorrencia SET st_tramite = vw.st_tramite, dt_ultimotramite = vw.dt_ultimotramite FROM dbo.tb_ocorrencia AS oc
JOIN dbo.vw_ocorrencia2 vw ON oc.id_ocorrencia = vw.id_ocorrencia

GO


s


/****** Object:  Trigger [dbo].[AtualizaCampoIdentificacao]    Script Date: 12/03/2015 17:59:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[ultimo_tramite] ON [dbo].[tb_tramiteocorrencia]
    AFTER INSERT
AS
    BEGIN
        SET NOCOUNT ON
        BEGIN
	    
            DECLARE @id_tramite INT
            DECLARE @id_ocorrencia INT

            SELECT  @id_ocorrencia = Inserted.id_ocorrencia ,
                    @id_tramite = Inserted.id_tramite
            FROM    Inserted
	    
            DECLARE @st_tramite VARCHAR(MAX)
            DECLARE @dt_ultimotramite DATETIME2

            BEGIN
                SELECT  @st_tramite = tm.st_tramite ,
                        @dt_ultimotramite = tm.dt_cadastro
                FROM    tb_tramite AS tm
                WHERE   tm.id_tramite = @id_tramite

                UPDATE  dbo.tb_ocorrencia
                SET     st_tramite = @st_tramite ,
                        dt_ultimotramite = @dt_ultimotramite
                WHERE   id_ocorrencia = @id_ocorrencia 
            END
        END
    END;
GO


USE [G2_UNY]

GO

SET ANSI_PADDING ON

DROP INDEX [pesquisa] ON [dbo].[tb_ocorrencia]

GO

CREATE NONCLUSTERED INDEX [pesquisa] ON [dbo].[tb_ocorrencia]
(
	[id_evolucao] ASC,
	[id_situacao] ASC,
	[id_assuntoco] ASC,
	[id_entidade] ASC
)
INCLUDE ( 	[id_ocorrencia],
	[id_matricula],
	[id_usuariointeressado],
	[id_categoriaocorrencia],
	[id_saladeaula],
	[st_titulo],
	[st_ocorrencia],
	[id_ocorrenciaoriginal],
	[dt_cadastro],
	[st_tramite],
	[dt_ultimotramite]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF,  ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]

GO

USE [G2_UNY]

GO

SET ANSI_PADDING ON


GO

CREATE NONCLUSTERED INDEX [idx_ocorrenciapessoa] ON [dbo].[tb_ocorrencia]
(
	[id_assuntoco] ASC,
	[id_entidade] ASC
)
INCLUDE ( 	[id_ocorrencia],
	[id_matricula],
	[id_evolucao],
	[id_situacao],
	[id_usuariointeressado],
	[id_categoriaocorrencia],
	[id_saladeaula],
	[st_titulo],
	[st_ocorrencia],
	[dt_cadastro],
	[st_tramite],
	[dt_ultimotramite]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = ON, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]

GO



alter table tb_avaliacaoaluno
add id_usuarioalteracao int NULL DEFAULT NULL
add dt_alteracao datetime null default getdate()
constraint FK_TB_AVALIALUNO_REFERENCE_TB_USUARIO foreign key (id_usuarioalteracao) references tb_usuario (id_usuario);
ALTER TABLE dbo.tb_projetopedagogico ADD id_horarioaula int
GO

alter table dbo.tb_projetopedagogico
   add constraint FK_TB_PROJE_REFERENCE_TB_HORAR foreign key (id_horarioaula)
      references dbo.tb_horarioaula (id_horarioaula)
go
-- GII-6505

UPDATE tb_matricula SET id_situacaoagendamento = 119 WHERE id_matricula IN (

  SELECT
    mt.id_matricula

  FROM
    tb_matricula AS mt

  WHERE
    mt.id_evolucao = 6
    AND mt.id_situacaoagendamento = 120
    AND EXISTS (SELECT gt.id_matricula FROM vw_gradenota AS gt WHERE gt.id_matricula = mt.id_matricula AND gt.bl_status = 0)

);
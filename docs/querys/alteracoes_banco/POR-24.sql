-- POR-24
SET IDENTITY_INSERT tb_funcionalidade ON
INSERT INTO tb_funcionalidade
(id_funcionalidade, st_funcionalidade, bl_ativo, id_situacao, st_classeflex, st_urlicone, id_tipofuncionalidade, bl_pesquisa, bl_lista, id_sistema, st_urlcaminho, bl_visivel, bl_relatorio, bl_delete)
VALUES
  (775, 'Biblioteca Virtual', 1, 182, 'app.biblioteca-virtual', 'icon-icon_livro', 1, 1, 1, 4, 'app.biblioteca-virtual',
   1, 0, 0)
SET IDENTITY_INSERT tb_funcionalidade OFF
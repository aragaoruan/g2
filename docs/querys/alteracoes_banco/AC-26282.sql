/**
**Adicionando situa��es para  a tabela de mensagem
**/

BEGIN TRANSACTION
INSERT INTO tb_situacao  (st_situacao, st_tabela, st_campo, st_descricaosituacao)
VALUES ('Ativa', 'tb_mensagem', 'id_situacao','A mensagem est� ativa' ),
('Inativa', 'tb_mensagem', 'id_situacao','A mensagem est� inativa' )

COMMIT


/**
** Alterando a tabela de mensagens para receber novos parametros para pesquisa
**/

BEGIN TRANSACTION

ALTER TABLE dbo.tb_mensagem 
ADD id_situacao INT NOT NULL DEFAULT (147) ,
id_projetopedagogico INT NULL ,
id_areaconhecimento INT NULL,
id_turma INT NULL

COMMIT


/**
	FK's
**/

/*
   sexta-feira, 20 de fevereiro de 2015 00:05:48
   User: denise.xavier
   Server: 54.207.67.154
   Database: G2H_EAD1
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.tb_areaconhecimento SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.tb_projetopedagogico SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.tb_situacao SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.tb_mensagem ADD CONSTRAINT
	FK_TB_MENSA_REFERENCE_TB_SITUACAO FOREIGN KEY
	(
	id_situacao
	) REFERENCES dbo.tb_situacao
	(
	id_situacao
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tb_mensagem ADD CONSTRAINT
	FK_TB_MENSA_REFERENCE_TB_PROJETO FOREIGN KEY
	(
	id_projetopedagogico
	) REFERENCES dbo.tb_projetopedagogico
	(
	id_projetopedagogico
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tb_mensagem ADD CONSTRAINT
	FK_TB_MENSA_REFERENCE_TB_AREA FOREIGN KEY
	(
	id_areaconhecimento
	) REFERENCES dbo.tb_areaconhecimento
	(
	id_areaconhecimento
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO

ALTER TABLE dbo.tb_mensagem ADD CONSTRAINT
	FK_TB_MENSA_REFERENCE_TB_TURMA FOREIGN KEY
	(
	id_turma
	) REFERENCES dbo.tb_turma
	(
	id_turma
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO

ALTER TABLE dbo.tb_mensagem SET (LOCK_ESCALATION = TABLE)
GO
COMMIT



/**
** Adicionando menu 
**/

BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_funcionalidade on
INSERT INTO [dbo].[tb_funcionalidade]
([id_funcionalidade]
,[st_funcionalidade]
,[id_funcionalidadepai]
,[bl_ativo]
,[id_situacao]
,[st_classeflex]
,[st_urlicone]
,[id_tipofuncionalidade]
,[st_classeflexbotao]
,[bl_pesquisa]
,[bl_lista]
,[id_sistema]
,[st_urlcaminho]
,[bl_visivel]
,[bl_relatorio])
VALUES

(664,'Enviar Mensagem - Portal do Aluno',428,1,123,'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa','/img/ico/flag.png',3,'CadastrarMensagemPortal',1,0, 1,
'/mensagem-portal', 1, 0)

GO
SET IDENTITY_INSERT dbo.tb_funcionalidade off
--ROLLBACK
COMMIT




/****
	Adicionando situa��o na tb_situacao
***/
SET IDENTITY_INSERT tb_situacao ON
INSERT INTO tb_situacao 
				(id_situacao
				, st_situacao
				, st_tabela
				, st_campo
				, st_descricaosituacao)
	VALUES 
	(195, 'N�o Apto - Documenta��o Incompleta', 'tb_matricula', 'id_situacaoagendamentoparcial', 'Aluno n�o est� com a documenta��o completa e n�o pode prosseguir com o agendamento' ),
	(196, 'N�o Apto - Nota insuficiente', 'tb_matricula', 'id_situacaoagendamentoparcial', 'Aluno n�o tem nota suficiente para realizar o agendamento da prova' ),
	(197, 'Apto - Agendamento liberado', 'tb_matricula', 'id_situacaoagendamentoparcial', 'Aluno possui todos os requisitos para realizar o agendamento da prova' )

SET IDENTITY_INSERT tb_situacao OFF


/** ADD o campo na tabela tb_matricula  **/
ALTER TABLE tb_matricula
	ADD id_situacaoagendamentoparcial INT NULL --//NULL porque � usado apenas na P�s Gradua��o

/** ADD a documenta��o do campo na tabela tb_matricula  **/
	EXEC sys.sp_addextendedproperty 
 @name = 'MS_Description',
 @value = 'Marca se existe algum impedimento extra para o agendamento do aluno na P�s Gradua��o' ,
 @level0type = 'SCHEMA', 
 @level0name = 'dbo', 
 @level1type = N'TABLE',
 @level1name = 'tb_matricula',
 @level2type = 'COLUMN',
 @level2name = 'id_situacaoagendamentoparcial';



 /** Atualiza todas as matriculas de entidades da pos-graduacao que 
 j� possuam id_situacaoagendamento = 120 (apto) para apto parcialmente tamb�m (id_situacaoagendamentoparcial = 197) **/

 UPDATE tb_matricula SET id_situacaoagendamentoparcial = 197 -- APTO PARCIAL
	 WHERE id_situacaoagendamento = 120
	 AND id_entidadematricula in (SELECT id_entidade FROM vw_entidadeesquemaconfiguracao 
									 WHERE id_itemconfiguracao = 22
									 AND st_valor = 3)
 




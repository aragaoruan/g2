-- Altera a tabela de disciplina integração adicionando as colunas que farão o relacionamento entre a disciplina (sala referencia e moodle)
ALTER TABLE [dbo].[tb_disciplinaintegracao]
  ADD [id_entidadeintegracao] INT NULL
GO

ALTER TABLE [dbo].[tb_disciplinaintegracao]
  ADD CONSTRAINT [FK_tb_disciplinaintegracao_tb_entidadeintegracao] FOREIGN KEY ([id_entidadeintegracao])
REFERENCES [dbo].[tb_entidadeintegracao] ([id_entidadeintegracao])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
GO


-- Altera a tabela de sala de aula adicionando uma coluna nova
ALTER TABLE [dbo].[tb_saladeaula]
  ADD [id_entidadeintegracao] INT NULL
GO

ALTER TABLE [dbo].[tb_saladeaula]
  ADD CONSTRAINT [FK_tb_saladeaula_tb_entidadeintegracao] FOREIGN KEY ([id_entidadeintegracao])
REFERENCES [dbo].[tb_entidadeintegracao] ([id_entidadeintegracao])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
GO


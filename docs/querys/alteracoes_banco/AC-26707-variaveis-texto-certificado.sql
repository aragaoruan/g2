
set IDENTITY_INSERT dbo.tb_textovariaveis on

insert into dbo.tb_textovariaveis (id_textovariaveis, id_textocategoria, st_camposubstituir, st_textovariaveis,
st_mascara, id_tipotextovariavel)
values
(363, 4, 'st_titulomonografia', '#titulo_monografia#', NULL, 1),
(364, 4, 'id_matricula', '#grid_notas_disciplinas#', 'gerarGridNotasDisciplina',2),
(365, 4, 'dt_inicioturma', '#inicioturma#', 'converteDataCliente', 2)

set IDENTITY_INSERT dbo.tb_textovariaveis off
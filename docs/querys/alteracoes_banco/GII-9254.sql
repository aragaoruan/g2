-- Criando a tabela que irá guardas as notas do tipo conceitual e não mais numericos

CREATE TABLE G2_DEV.dbo.tb_notaconceitual
(
  id_notaconceitual INT PRIMARY KEY NOT NULL IDENTITY,
  st_notaconceitual VARCHAR(255) NOT NULL,
  dt_cadastro DATETIME NOT NULL
)
CREATE UNIQUE INDEX tb_notaconceitual_id_notaconceitual_uindex ON G2_DEV.dbo.tb_notaconceitual (id_notaconceitual)
CREATE UNIQUE INDEX tb_notaconceitual_st_notaconceitual_uindex ON G2_DEV.dbo.tb_notaconceitual (st_notaconceitual)


-- Inserindo os valores padrões

SET IDENTITY_INSERT  tb_notaconceitual ON
INSERT INTO tb_notaconceitual (id_notaconceitual, st_notaconceitual, dt_cadastro) VALUES (1, 'Não Concluido', GETDATE ());
INSERT INTO tb_notaconceitual (id_notaconceitual, st_notaconceitual, dt_cadastro) VALUES (2, 'Concluido', GETDATE ());
SET IDENTITY_INSERT  tb_notaconceitual ON


-- Modificando a tabela tb_avaliacaoaluno para receber as notas conceituais

ALTER TABLE G2_DEV.dbo.tb_avaliacaoaluno ADD id_notaconceitual INT NULL
ALTER TABLE G2_DEV.dbo.tb_avaliacaoaluno
  ADD CONSTRAINT FK_TB_AVALI_REFERENCE_TB_NOTACONCEITUAL
FOREIGN KEY (id_notaconceitual) REFERENCES tb_notaconceitual (id_notaconceitual)

SET IDENTITY_INSERT tb_funcionalidade ON
INSERT INTO tb_funcionalidade (id_funcionalidade, st_urlcaminho, id_funcionalidadepai, st_funcionalidade,
                               bl_ativo, id_situacao, id_tipofuncionalidade, bl_pesquisa, bl_lista, bl_visivel, bl_relatorio, bl_delete, id_sistema)
VALUES (
  841,
-- id_funcionalidade: Novo ID da funcionalidade. Quem roda a SQL em produção fica com a responsabilidade de adicionar um novo ID manualmente.
'/relatorio-log-aluno',
-- st_urlcaminho: Geralmente, a url da funcionalidade que será exibida depois do #/. Caso seja um relatório do sistema deverá colocar o id_funcionalidade.
257, -- id_funcionalidadepai: A funcionalidade 'superior' na hierarquia do menu.
'Consultar Log por Aluno', -- st_funcionalidade: descrição da funcionalidade.
1, -- bl_ativo: funcionalidade ativa
123, -- id_situacao: funcionalidade em HTML
3, -- id_tipofuncionalidade
0, -- bl_pesquisa
0, -- bl_lista
1, -- bl_visivel
0, -- bl_relatorio (1 = relatorio do sistema)
0, -- bl_delete
1); --id_sistema: 1 (G2S)
SET IDENTITY_INSERT tb_funcionalidade OFF
-- AC-27326

-- Alterar a vw de origem dos dados para rel.vw_alunosaptosagendamento
UPDATE tb_relatorio SET st_origem = 'vw_alunosaptosagendamento' WHERE id_relatorio = 70;


-- Remover campo data de conclusao
DELETE FROM tb_camporelatorio WHERE id_relatorio = 70 AND st_camporelatorio = 'dt_concluinte';


-- Remover filtro de situacao
DELETE FROM tb_propriedadecamporelatorio WHERE id_camporelatorio IN (
	SELECT id_camporelatorio FROM tb_camporelatorio WHERE id_relatorio = 70 AND st_camporelatorio = 'id_situacaoagendamento'
);
DELETE FROM tb_camporelatorio WHERE id_relatorio = 70 AND st_camporelatorio = 'id_situacaoagendamento';


-- Alterar busca do combo (FILTRO EVOLUCAO)
SET QUOTED_IDENTIFIER OFF;
UPDATE tb_propriedadecamporelatorio SET
st_valor = "SELECT id_evolucao AS id, st_evolucao AS value FROM tb_evolucao WHERE st_tabela = 'tb_matricula' AND st_campo = 'id_evolucao' AND id_evolucao IN (6, 21, 40)"
WHERE id_tipopropriedadecamporel = 5 AND id_camporelatorio IN (
	SELECT id_camporelatorio FROM tb_camporelatorio WHERE id_relatorio = 70 AND st_camporelatorio = 'id_evolucao'
);
SET QUOTED_IDENTIFIER ON;


-- Inserir a informacao do Tipo de Avaliacao no resultado
INSERT INTO tb_camporelatorio (id_relatorio, st_camporelatorio, st_titulocampo, bl_filtro, bl_exibido, nu_ordem, bl_obrigatorio) VALUES (70, 'st_avaliacao', 'Tipo de Prova', 0, 1, 15, 0);

--query para adicionar nova permissao
set identity_insert tb_permissao on;
insert into tb_permissao (id_permissao, st_nomepermissao, st_descricao, bl_ativo)
values (13, 'Cancelar', 'Permitir usuario alterar situacao para cancelado', 1);
set identity_insert tb_permissao off;

--query para vincular a funcionalidade sala de aula a nova permissao criada
insert into tb_permissaofuncionalidade (id_funcionalidade, id_permissao) values (23, 13);
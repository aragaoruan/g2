-- query para adicionar nova permissao

set identity_insert tb_permissao on;
insert into tb_permissao (id_permissao, st_nomepermissao, st_descricao, bl_ativo)
values (53, 'Gerar Venda', 'Permitir usuario gerar vendas', 1);
set identity_insert tb_permissao off;

--query para vincular a funcionalidade sala de aula a nova permissao criada

insert into tb_permissaofuncionalidade (id_funcionalidade, id_permissao)
values (429 , 53);

-- query para adicionar nova permissao
set identity_insert tb_permissao on;
insert into tb_permissao (id_permissao, st_nomepermissao, st_descricao, bl_ativo)
values (54, 'Vincular Ocorências', 'Permitir vincular ocorrência', 1);
set identity_insert tb_permissao off;

--query para vincular a funcionalidade sala de aula a nova permissao criada

insert into tb_permissaofuncionalidade (id_funcionalidade, id_permissao)
values (429 , 54);
/*
Utilizar
	st_codsistema	como C�digo da coligada
	st_codarea		como c�digo conta caixa da entidade
	st_codchave		como c�digo de custo
SELECT
	st_codsistema AS codcoligada,
	ei.st_codarea AS codcxa,
	ei.st_codchave AS codccusto
FROM tb_entidadeintegracao ei
	WHERE id_sistema = 29
**/

--DESATIVAR IDENTITY TABELA
SET IDENTITY_INSERT tb_sistema ON

INSERT INTO tb_sistema (id_sistema, st_sistema, bl_ativo, st_conexao, st_chaveacesso) VALUES(29, 'Fluxus - Integração Venda', 1, NULL, NULL)

--ATIVAR IDENTITY TABELA
SET IDENTITY_INSERT tb_sistema OFF

CREATE TABLE tb_formapagamentointegracao (
	id_formapagamentointegracao INT PRIMARY KEY,
	id_sistema INT ,
	id_usuariocadastro INT,
	id_formapagamento INT,
	id_entidade INT,
	st_codsistema VARCHAR(2),
	st_codchave VARCHAR(5)
)

CREATE TABLE tb_vendaprodutointegracao (
	id_vendaprodutointegracao INT PRIMARY KEY,
	id_sistema INT,
	id_entidade INT,
	id_usuariocadastro INT,
	id_venda INT,
	id_produto INT,
	st_codvenda VARCHAR(20),
	st_codigoprodutoexterno VARCHAR(20),
	dt_cadastro DATETIME,
	dt_sincronizado DATETIME
)

ALTER TABLE tb_produto ADD st_codigoprodutoexterno AS VARCHAR(20) GO
ALTER TABLE tb_vendaintegracao ADD st_codvenda AS VARCHAR(20) GO
ALTER TABLE tb_lancamentointegracao ADD st_codvenda AS VARCHAR(20) GO
ALTER TABLE tb_vendaintegracao ADD st_codresponsavel VARCHAR(20)
ALTER TABLE tb_entidadeintegracao ADD st_codcpg VARCHAR(20)
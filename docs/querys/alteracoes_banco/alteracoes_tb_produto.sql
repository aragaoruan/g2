ALTER TABLE dbo.tb_produto ADD st_subtitulo VARCHAR(300)
GO
ALTER TABLE dbo.tb_produto ADD st_slug VARCHAR(200)
GO
ALTER TABLE dbo.tb_produto ADD bl_destaque BIT NOT NULL DEFAULT 0
GO
ALTER TABLE dbo.tb_produto ADD dt_cadastro DATETIME2 NOT NULL DEFAULT GETDATE()
GO
ALTER TABLE dbo.tb_produto ADD dt_atualizado DATETIME2 NOT NULL DEFAULT GETDATE()
GO
ALTER TABLE dbo.tb_produto ADD dt_iniciopontosprom DATETIME2 NOT NULL DEFAULT GETDATE()
GO
ALTER TABLE dbo.tb_produto ADD dt_fimpontosprom DATETIME2 NOT NULL DEFAULT GETDATE()
GO
ALTER TABLE dbo.tb_produto ADD nu_pontos int 
GO
ALTER TABLE dbo.tb_produto ADD nu_pontospromocional int 
GO
ALTER TABLE dbo.tb_produto ADD nu_estoque NUMERIC(30,2) 
GO
execute sp_addextendedproperty 'MS_Description', 
   'Subtitulo/Cargo do produto',
   'user', 'dbo', 'table', 'tb_produto', 'column', 'st_subtitulo'
go
execute sp_addextendedproperty 'MS_Description', 
   'Fim dos pontos promocionais',
   'user', 'dbo', 'table', 'tb_produto', 'column', 'dt_fimpontosprom'
go

execute sp_addextendedproperty 'MS_Description', 
   'Inicio dos pontos promocionais',
   'user', 'dbo', 'table', 'tb_produto', 'column', 'dt_iniciopontosprom'
go
execute sp_addextendedproperty 'MS_Description', 
   'Numero de pontos do produto',
   'user', 'dbo', 'table', 'tb_produto', 'column', 'nu_pontospromocional'
go
execute sp_addextendedproperty 'MS_Description', 
   'Numero de pontos do produto',
   'user', 'dbo', 'table', 'tb_produto', 'column', 'nu_pontos'
go
execute sp_addextendedproperty 'MS_Description', 
   'Estoque ou vagas do produto',
   'user', 'dbo', 'table', 'tb_produto', 'column', 'nu_estoque'
go
execute sp_addextendedproperty 'MS_Description', 
   'Campo que define se esse produto deve ser destacado no site',
   'user', 'dbo', 'table', 'tb_produto', 'column', 'bl_destaque'
go



-- CREATE VIEW das alterações na vw_gerardeclaracao.

CREATE VIEW [dbo].[vw_gerardeclaracao]
AS
    SELECT DISTINCT
            mt.id_matricula ,
            ps.id_usuario AS id_usuarioaluno ,
            ps.st_nomecompleto AS st_nomecompletoaluno ,
            ps.st_login AS st_loginaluno ,
            ps.st_senhaentidade AS st_senhaaluno ,
            ps.st_cpf AS st_cpfaluno ,
            ps.st_rg AS st_rgaluno ,
            ps.st_orgaoexpeditor ,
            ps.dt_dataexpedicao AS dt_dataexpedicaoaluno ,
            ps.st_nomemae AS st_filiacao ,
            CONVERT(CHAR, mt.dt_cadastro, 103) AS st_datamatricula ,
            canc.id_cancelamento ,
            canc.st_observacao AS st_observacaocancelamento ,
            canc.st_observacaocalculo AS st_observacao_calculo ,
            CONVERT(CHAR, canc.dt_solicitacao, 103) AS st_solicitacaocancelamento ,
            canc.nu_valorcarta AS st_valorcredito ,
            CONVERT(CHAR, DATEADD(D, 365, canc.dt_solicitacao), 103) AS st_validadecredito ,
            cm.id_contrato ,
            ps.dt_nascimento AS dt_nascimentoaluno ,
            UPPER(SUBSTRING(ps.st_nomemunicipio, 1, 1))
            + LOWER(SUBSTRING(ps.st_nomemunicipio, 2, 499)) AS st_municipioaluno ,
            ps.st_estadoprovincia AS st_ufaluno ,
            ps.sg_uf AS sg_ufaluno ,
            ps.nu_ddd AS nu_dddaluno ,
            ps.nu_telefone AS nu_telefonealuno ,
            ps.st_email AS st_emailaluno ,
            pp.st_tituloexibicao AS st_projeto ,
	--pp.nu_cargahoraria,
            ( SELECT    SUM(dis.nu_cargahoraria) AS nu_cargahoraria
              FROM      tb_matriculadisciplina md
                        JOIN tb_matricula m ON m.id_matricula = md.id_matricula
                        JOIN tb_disciplina dis ON dis.id_disciplina = md.id_disciplina
              WHERE     md.id_matricula = mt.id_matricula
            ) AS nu_cargahoraria ,
            ne.st_nivelensino ,
            mt.dt_concluinte AS dt_concluintealuno ,
            us_coordenador.st_nomecompleto AS st_coordenadorprojeto ,
            fl.id_fundamentolegal AS id_fundamentolei ,
            fl.nu_numero AS nu_lei ,
            flr.id_fundamentolegal AS id_fundamentoresolucao ,
            tur.id_turma AS id_turma ,
            tur.st_turma ,
            tur.dt_inicio AS dt_inicioturma ,
            tur.dt_fim AS dt_terminoturma ,
            flr.nu_numero AS nu_resolucao ,
            flpr.id_fundamentolegal AS id_fundamentoparecer ,
            flpr.nu_numero AS nu_parecer ,
            flp.id_fundamentolegal AS id_fundamentoportaria ,
            flp.nu_numero AS nu_portaria ,
            et.id_entidade ,
            et.st_nomeentidade ,
            et.st_razaosocial ,
            et.st_cnpj ,
            et.st_urlimglogo ,
            et.st_urlsite ,
            ps.st_nomepais ,
            ps.st_sexo AS sexo_aluno ,
            UPPER(tcc.st_tituloavaliacao) AS st_titulomonografia ,
            CONVERT(VARCHAR(2), DATEPART(DAY, GETDATE()), 103) + ' de '
            + dbo.fn_mesporextenso(GETDATE()) + ' de '
            + CONVERT(VARCHAR(4), DATEPART(YEAR, GETDATE()), 103) AS st_data ,
            DAY(GETDATE()) AS st_dia ,
            LOWER(dbo.fn_mesporextenso(GETDATE())) AS st_mes ,
            YEAR(GETDATE()) AS st_ano ,

            --CONVERT(DATE, sa.dt_abertura, 103) AS dt_primeirasala ,
            (
                SELECT TOP 1 ts.dt_abertura
                FROM tb_matriculadisciplina tm
                JOIN tb_alocacao ta ON tm.id_matriculadisciplina = ta.id_matriculadisciplina
                JOIN tb_saladeaula ts ON ta.id_saladeaula = ts.id_saladeaula
                WHERE tm.id_matricula = mt.id_matricula AND tm.bl_obrigatorio = 1
                ORDER BY ts.dt_abertura ASC
            ) AS dt_primeirasala,

            --CONVERT(DATE, DATEADD(MONTH, 13, sa.dt_abertura), 103) AS dt_previsaofim ,
            (
                SELECT TOP 1
                --DATEADD(MONTH, 13, ts.dt_abertura)

                'dt_previsaotermino' = CASE WHEN ts.id_categoriasala = 2 THEN (
                    DATEADD(day, ISNULL(ts.nu_diasaluno, 0), ts.dt_abertura)
                ) ELSE (
                    CASE WHEN ts.dt_encerramento IS NOT NULL THEN (
                        DATEADD(day, ISNULL(ts.nu_diasextensao, 0) + 30, ts.dt_encerramento)
                    ) WHEN ts.dt_abertura IS NOT NULL THEN (
                        DATEADD(day, ISNULL(ts.nu_diasextensao, 0) + 30, ts.dt_abertura)
                    ) ELSE (
                        NULL
                    ) END
                ) END

                FROM tb_matriculadisciplina tm
                JOIN tb_alocacao ta ON tm.id_matriculadisciplina = ta.id_matriculadisciplina
                JOIN tb_saladeaula ts ON ta.id_saladeaula = ts.id_saladeaula
                WHERE tm.id_matricula = mt.id_matricula AND tm.bl_obrigatorio = 1
                ORDER BY dt_previsaotermino DESC, ts.dt_abertura DESC, ts.dt_encerramento DESC
            ) AS dt_previsaofim,

            UPPER(tcc.st_tituloavaliacao) AS st_titulotcc,
            tcc.dt_aberturasala AS dt_iniciotcc,
            tcc.dt_encerramentosala AS dt_terminotcc,
            tcc.st_nota AS nu_notatcc,
            tcc.dt_cadastroavaliacao AS dt_cadastrotcc,
            tcc.dt_defesa AS dt_defesatcc,

            mt.st_codcertificacao ,
            CONVERT(CHAR, GETDATE(), 103) AS st_atual
			, matc.st_codigoacompanhamento AS st_codigorasteamento,

			mt.id_matriculaorigem,
			mto.dt_inicio AS dt_iniciomatriculaorigem
			, CONVERT(VARCHAR(2), DATEPART(day, GetDate()))     + ' de ' +  dbo.fn_mesporextenso(GetDate())   + ' de ' +  CONVERT(VARCHAR(4), DATEPART(year, GetDate())) as st_atualextenso,
			 CONVERT(VARCHAR(2), DATEPART(day, tur.dt_fim))     + ' de ' +  dbo.fn_mesporextenso(tur.dt_fim)   + ' de ' +  CONVERT(VARCHAR(4), DATEPART(year, tur.dt_fim)) as st_terminoturmaextenso,
			 CONVERT(VARCHAR(2), DATEPART(day, tur.dt_inicio))     + ' de ' +  dbo.fn_mesporextenso(tur.dt_inicio)   + ' de ' +  CONVERT(VARCHAR(4), DATEPART(year, tur.dt_inicio)) as st_inicioturmaextenso

			, CONVERT(CHAR, venda.dt_limiterenovacao, 103) AS st_limiterenovacao
			, venda.id_venda AS id_venda
			, venda.nu_mensalidade AS nu_mensalidade
			, venda.nu_mensalidadepon AS nu_mensalidadepontualidade
			, ' - ' AS st_link_grade_portal
			, ' - ' AS st_municipioufagendamento
			, ac.st_areaconhecimento

    FROM    tb_matricula mt
            JOIN tb_projetopedagogico AS pp ON pp.id_projetopedagogico = mt.id_projetopedagogico
            JOIN tb_entidade AS et ON et.id_entidade = mt.id_entidadeatendimento
            LEFT JOIN tb_entidadeendereco AS ete ON ete.id_entidade = et.id_entidade
                                                    AND ete.bl_padrao = 1
            LEFT JOIN tb_endereco AS etee ON etee.id_endereco = ete.id_endereco
            JOIN vw_pessoa AS ps ON ps.id_usuario = mt.id_usuario
                                    AND ps.id_entidade = mt.id_entidadeatendimento
            LEFT JOIN tb_fundamentolegal AS fl ON fl.id_entidade = mt.id_entidadematriz
                                                  AND fl.id_tipofundamentolegal = 1
                                                  AND fl.dt_publicacao <= GETDATE()
                                                  AND fl.dt_vigencia >= GETDATE()
            LEFT JOIN tb_fundamentolegal AS flp ON flp.id_entidade = mt.id_entidadematriz
                                                   AND flp.id_tipofundamentolegal = 2
                                                   AND flp.dt_publicacao <= GETDATE()
                                                   AND flp.dt_vigencia >= GETDATE()
            LEFT JOIN tb_fundamentolegal AS flr ON flr.id_entidade = mt.id_entidadematriz
                                                   AND flr.id_tipofundamentolegal = 3
                                                   AND flr.dt_publicacao <= GETDATE()
                                                   AND flr.dt_vigencia >= GETDATE()
            LEFT JOIN tb_fundamentolegal AS flpr ON flpr.id_entidade = mt.id_entidadematriz
                                                    AND flpr.id_tipofundamentolegal = 4
                                                    AND flpr.dt_publicacao <= GETDATE()
                                                    AND flpr.dt_vigencia >= GETDATE()
            LEFT JOIN tb_projetopedagogicoserienivelensino AS psn ON psn.id_projetopedagogico = mt.id_projetopedagogico
            LEFT JOIN tb_turma AS tur ON tur.id_turma = mt.id_turma
            JOIN tb_nivelensino AS ne ON ne.id_nivelensino = psn.id_nivelensino
            LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS uper ON uper.id_projetopedagogico = pp.id_projetopedagogico
                                                              AND uper.bl_ativo = 1
                                                              AND uper.bl_titular = 1
            LEFT JOIN dbo.tb_usuario AS us_coordenador ON us_coordenador.id_usuario = uper.id_usuario

            LEFT JOIN tb_matricula AS mto ON mt.id_matriculaorigem = mto.id_matricula

            LEFT JOIN dbo.vw_avaliacaoaluno tcc ON ( tcc.id_matricula = mt.id_matricula
                                                    AND tcc.id_tipoavaliacao = 6
                                                    AND tcc.st_tituloavaliacao IS NOT NULL
                                                    AND tcc.id_tipodisciplina = 2
                                                    AND tcc.bl_ativo = 1
                                                  )
            LEFT JOIN tb_cancelamento AS canc ON canc.id_cancelamento = mt.id_cancelamento AND mt.id_evolucao = 27
            LEFT JOIN tb_contratomatricula AS cm ON cm.id_matricula = mt.id_matricula
			LEFT JOIN tb_matriculacertificacao AS matc ON matc.id_matricula = mt.id_matricula
			LEFT JOIN tb_areaprojetopedagogico AS app ON pp.id_projetopedagogico = app.id_projetopedagogico
			LEFT JOIN tb_areaconhecimento AS ac ON ac.id_areaconhecimento = app.id_areaconhecimento

			OUTER APPLY (SELECT TOP 1 v.id_venda , v.dt_limiterenovacao ,
							format((vp.nu_valorbruto - ((cm.nu_valordesconto / 100 ) * vp.nu_valorbruto))/6, '#.00') AS nu_mensalidade,
							format((((vp.nu_valorbruto - ((cm.nu_valordesconto / 100 ) * vp.nu_valorbruto))/6) - (((pon.nu_valordesconto / 100 ) * ((vp.nu_valorbruto - ((cm.nu_valordesconto / 100 ) * vp.nu_valorbruto))/6)))), '#.00') AS nu_mensalidadepon

							FROM tb_vendaproduto AS vp
							JOIN tb_venda AS v ON v.id_venda = vp.id_venda AND v.dt_limiterenovacao IS NOT NULL
							AND vp.id_matricula = mt.id_matricula AND v.id_evolucao = 7 AND v.bl_ativo = 1
							LEFT JOIN tb_campanhacomercial AS cm ON cm.id_campanhacomercial = vp.id_campanhacomercial
							LEFT JOIN tb_campanhacomercial as pon ON pon.id_campanhacomercial = v.id_campanhapontualidade

							ORDER BY v.id_venda DESC) AS venda

			OUTER APPLY (SELECT TOP 1 v.nu_valorbruto, v.nu_valorliquido
			, (SELECT vle.nu_valor FROM vw_vendalancamento vle WHERE vle.bl_entrada = 1 AND vle.id_venda = v.id_venda) AS nu_valorentrada
			, (SELECT vlp.nu_valor FROM vw_vendalancamento vlp WHERE vlp.bl_entrada = 0 AND vlp.nu_ordem = 2 AND vlp.id_venda = v.id_venda) AS nu_valorparcela
			, (SELECT count(lan.id_lancamento) FROM tb_lancamento as lan
												JOIN tb_lancamentovenda AS lv ON lv.id_venda = v.id_venda
																AND lan.id_lancamento = lv.id_lancamento ) AS nu_parcelas

			FROM tb_vendaproduto AS vp
			JOIN tb_venda AS v ON v.id_venda = vp.id_venda
				AND vp.id_matricula = mt.id_matricula AND v.id_evolucao = 10 AND v.bl_ativo = 1

			ORDER BY v.id_venda DESC) AS venda_confirmada;


-- INSERT das novas variáveis

-- SET IDENTITY_INSERT tb_textovariaveis ON

INSERT INTO
tb_textovariaveis (id_textovariaveis, id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
VALUES (
418,
3,
'st_municipioufagendamento',
'#municipio_UF_agendamento#',
'gerarStringMunicipioUfAgendamento',
2
);


INSERT INTO
tb_textovariaveis (id_textovariaveis, id_textocategoria, st_camposubstituir, st_textovariaveis, id_tipotextovariavel)
VALUES (
419,
3,
'st_areaconhecimento',
'#area_conhecimento#',
1
);
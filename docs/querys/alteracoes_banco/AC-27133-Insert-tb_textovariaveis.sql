-- AC-27133

SET IDENTITY_INSERT tb_textovariaveis ON;

INSERT INTO tb_textovariaveis (id_textovariaveis, id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
VALUES
(388, 3, 'id_matricula', '#endereco_nucleo_agendamento#', 'retornaEnderecoNucleoUltimoAgendamento', 2),
(389, 3, 'id_matricula', '#data_agendamento_aluno#', 'retornaDataUltimoAgendamento', 2),
(390, 3, 'id_matricula', '#horario_agendamento#', 'retornaHorarioUltimoAgendamento', 2);

SET IDENTITY_INSERT tb_textovariaveis OFF;
/* GII-9348 */

/* Alterando tb_disciplina, adicionando novos campos. */

ALTER TABLE tb_disciplina
ADD bl_coeficienterendimento BIT;

ALTER TABLE tb_disciplina
ADD bl_cargahorariaintegralizada BIT;

/* Atualizando disciplinas já existentes. */

UPDATE tb_disciplina SET bl_coeficienterendimento = 1;
UPDATE tb_disciplina SET bl_cargahorariaintegralizada = 1;
BEGIN TRAN;
ALTER TABLE tb_informacaoacademicapessoa
  ALTER COLUMN st_curso VARCHAR(255) NULL;
ALTER TABLE tb_informacaoacademicapessoa
  ALTER COLUMN st_nomeinstituicao VARCHAR(255) NULL;
COMMIT;
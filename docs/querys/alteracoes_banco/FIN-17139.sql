BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_permissao on
insert into tb_permissao (id_permissao, st_nomepermissao,st_descricao,bl_ativo) values(
29,'Imprimir Contrato','Permite imprimir o contrato.',1
)
SET IDENTITY_INSERT dbo.tb_permissao off
commit



insert into tb_permissaofuncionalidade (id_funcionalidade, id_permissao) values (275,29);
/**
ADICIONANDO mensagem padr�o no banco para agendamento de prova de recupera��o
Banco novo = da produ��o ainda n�o tem
**/


BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_mensagempadrao ON
INSERT INTO tb_mensagempadrao
(id_mensagempadrao, id_tipoenvio, st_mensagempadrao, st_default, id_textocategoria)
VALUES (34, 3,  'Agendamento Liberado Prova Recupera��o' , NULL, 10)
SET IDENTITY_INSERT dbo.tb_mensagempadrao OFF

COMMIT
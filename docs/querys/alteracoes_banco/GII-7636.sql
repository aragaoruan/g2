-- Criando tb_finalidadecampanha e adicionando valores.

CREATE TABLE tb_finalidadecampanha (
	PRIMARY KEY		(id_finalidadecampanha),
	id_finalidadecampanha 	INTEGER NOT NULL IDENTITY,
	st_finalidadecampanha		VARCHAR (140),
	bl_ativo                BIT NOT NULL
);

INSERT INTO tb_finalidadecampanha (st_finalidadecampanha, bl_ativo)
	VALUES ('Venda', 1), ('Transferência de Curso', 1);

/* Adicionando coluna id_finalidadecampanha na tb_campanhacomercial
   e adicionando foreign key. */

ALTER TABLE tb_campanhacomercial ADD
	id_finalidadecampanha		INTEGER;

ALTER TABLE tb_campanhacomercial ADD
	CONSTRAINT FK_TB_CAMPANHA_COMERCIAL_TB_FINALIDADE_CAMPANHA
		FOREIGN KEY (id_finalidadecampanha)
		REFERENCES tb_finalidadecampanha(id_finalidadecampanha);

-- Atualizando todas as campanhas comerciais existentes para id_finalidadecampanha 1 (Venda)

UPDATE tb_campanhacomercial
	SET 	id_finalidadecampanha = 1
	WHERE 	id_finalidadecampanha IS NULL;

-- INSERT FUNCIONALIDADE PAI --
BEGIN TRANSACTION ;
SET IDENTITY_INSERT dbo.tb_funcionalidade ON ;

INSERT INTO [dbo].[tb_funcionalidade] (
	[id_funcionalidade],
	[st_funcionalidade],
	[id_funcionalidadepai],
	[bl_ativo],
	[st_classeflex],
	[id_situacao],
	[id_tipofuncionalidade],
	[st_classeflexbotao],
	[bl_pesquisa],
	[bl_lista],
	[id_sistema],
	[st_urlcaminho],
	[bl_visivel],
	[bl_relatorio]

)
VALUES
	(
		650,
		'Grade Horaria',
		NULL,
		1,
		'#',
		3,
		1,
		'ajax',
		0,
		0,
		4,
		'650',
		1,
		0
	);
	GO


SET IDENTITY_INSERT dbo.tb_funcionalidade OFF;

COMMIT;




-- INSERT FUNCIONALIDADE FILHA--
BEGIN TRANSACTION ;
SET IDENTITY_INSERT dbo.tb_funcionalidade ON ;

INSERT INTO [dbo].[tb_funcionalidade] (
	[id_funcionalidade],
	[st_funcionalidade],
	[id_funcionalidadepai],
	[bl_ativo],
	[id_situacao],
	[id_tipofuncionalidade],
	[st_classeflexbotao],
	[bl_pesquisa],
	[bl_lista],
	[id_sistema],
	[st_urlcaminho],
	[bl_visivel],
	[bl_relatorio],
	[st_classeflex]
)
VALUES
	(
		649,
		'Grade Horaria',
		650,
		1,
		3,
		1,
		'ajax',
		0,
		0,
		4,
		'649',
		1,
		0,
		'/grade-horaria'
	);
	GO


SET IDENTITY_INSERT dbo.tb_funcionalidade OFF;

COMMIT;



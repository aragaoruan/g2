/**
**** INSERTS DE PERMISSAO DE FUNCIONALIDADE PARA FUNCIONALIDADE DE ALOCAR ALUNO EM SALA ENCERRADA
**** GII-7468
**/


SET IDENTITY_INSERT tb_permissao ON
INSERT INTO tb_permissao 
(id_permissao, st_nomepermissao, st_descricao, bl_ativo)
VALUES
(42, 'Listar Salas Encerradas Aloca��o', 'Lista tamb�m as salas encerradas como op��o para aloca��o do aluno', 1)

SET IDENTITY_INSERT tb_permissao OFF


INSERT INTO tb_permissaofuncionalidade (id_funcionalidade, id_permissao) 
VALUES (207, 42 );



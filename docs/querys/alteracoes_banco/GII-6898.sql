/***
*** INSERT FUNCIONALIDADE Diplomação
*** GII-6898
***/



BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_funcionalidade ON
INSERT INTO [dbo].[tb_funcionalidade]
([id_funcionalidade]
  , [st_funcionalidade]
  , [id_funcionalidadepai]
  , [bl_ativo]
  , [id_situacao]
  , [st_classeflex]
  , [st_urlicone]
  , [id_tipofuncionalidade]
  , [st_classeflexbotao]
  , [bl_pesquisa]
  , [bl_lista]
  , [id_sistema]
  , [st_urlcaminho]
  , [bl_visivel]
)
VALUES
  (787, 'Diplomação', 157, 1, 123, 'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa', NULL, 3, NULL, 0, 0,
   1, 'diplomacao', 1)


GO
SET IDENTITY_INSERT dbo.tb_funcionalidade OFF
IF @@ERROR <> 0
  ROLLBACK
ELSE
  COMMIT



/***
*** Add coluna id_evolucaodiplomacao
*** na tb_matricula
***/

BEGIN TRAN

ALTER TABLE tb_matricula
  ADD id_evolucaodiplomacao INT NULL

IF @@ERROR <> 0
  ROLLBACK
ELSE
  COMMIT


/***
*** Add evoluções para a id_evolucaodiplomacao
*** na tb_evolucao
***/

BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_evolucao ON
INSERT INTO [dbo].[tb_evolucao]
([id_evolucao],
 [st_evolucao],
 [st_campo],
 [st_tabela]
)
VALUES
  (71, 'Apto a Diplomar', 'id_evolucaodiplomacao', 'tb_matricula'),
  (72, 'Diploma Gerado', 'id_evolucaodiplomacao', 'tb_matricula'),
  (73, 'Enviado para Assinatura', 'id_evolucaodiplomacao', 'tb_matricula'),
  (74, 'Retornado da Assinatura', 'id_evolucaodiplomacao', 'tb_matricula'),
  (75, 'Enviado para Secretaria Geral', 'id_evolucaodiplomacao', 'tb_matricula'),
  (76, 'Enviado para Registro', 'id_evolucaodiplomacao', 'tb_matricula'),
  (77, 'Retornado do Registro', 'id_evolucaodiplomacao', 'tb_matricula'),
  (78, 'Enviado ao Pólo do Aluno', 'id_evolucaodiplomacao', 'tb_matricula'),
  (79, 'Entregue ao Aluno', 'id_evolucaodiplomacao', 'tb_matricula'),
  (80, 'Histórico Gerado', 'id_evolucaodiplomacao', 'tb_matricula')

GO
SET IDENTITY_INSERT dbo.tb_evolucao OFF
IF @@ERROR <> 0
  ROLLBACK
ELSE
  COMMIT


/*********************************************
**** TABELA tb_matriculadiplomacao
**** Datas para processo de diplomação
*********************************************/

BEGIN TRANSACTION

CREATE TABLE
  dbo.tb_matriculadiplomacao
(
  [id_matriculadiplomacao] [INT] IDENTITY (1, 1) NOT NULL,
  [id_entidade] [INT] NOT NULL,
  [id_matricula] [INT] NOT NULL,
  [dt_cadastro] [DATETIME] NOT NULL,
  [id_usuariocadastro] [INT] NOT NULL,
  [dt_aptodiplomar] [DATETIME] NULL,
  [id_usuarioaptodiplomar] [INT] NULL,
  [dt_diplomagerado] [DATETIME] NULL,
  [id_usuariodiplomagerado] [INT] NULL,
  [dt_enviadoassinatura] [DATETIME] NULL,
  [id_usuarioenviadoassinatura] [INT] NULL,
  [dt_retornadoassinatura] [DATETIME] NULL,
  [id_usuarioretornadoassinatura] [INT] NULL,
  [dt_enviadosecretaria] [DATETIME] NULL,
  [id_usuarioenviadosecretaria] [INT] NULL,
  [dt_enviadoregistro] [DATETIME] NULL,
  [id_usuarioenviadoregistro] [INT] NULL,
  [dt_retornoregistro] [DATETIME] NULL,
  [id_usuarioretornoregistro] [INT] NULL,
  [dt_enviopolo] [DATETIME] NULL,
  [id_usuarioenviopolo] [INT] NULL,
  [dt_entreguealuno] [DATETIME] NULL,
  [id_usuarioentreguealuno] [INT] NULL,
  [dt_historicogerado] [DATETIME] NULL,
  [id_usuariohistoricogerado] [INT] NULL,
  [bl_ativo] [BIT] NOT NULL DEFAULT 1
)

ALTER TABLE [dbo].[tb_matriculadiplomacao]
  ADD
  CONSTRAINT [PK_tb_matriculadiplomacao] PRIMARY KEY CLUSTERED
    (
      [id_matriculadiplomacao]
    )
    ON [PRIMARY]

ALTER TABLE [dbo].[tb_matriculadiplomacao]
  ADD CONSTRAINT tb_matriculadiplomacao_id_usuariocadastro_FK FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)

ALTER TABLE [dbo].[tb_matriculadiplomacao]
  ADD CONSTRAINT tb_matriculadiplomacao_id_usuarioaptodiplomar_FK FOREIGN KEY (id_usuarioaptodiplomar) REFERENCES tb_usuario (id_usuario)

ALTER TABLE [dbo].[tb_matriculadiplomacao]
  ADD CONSTRAINT tb_matriculadiplomacao_id_usuariodiplomagerado_FK FOREIGN KEY (id_usuariodiplomagerado) REFERENCES tb_usuario (id_usuario)

ALTER TABLE [dbo].[tb_matriculadiplomacao]
  ADD CONSTRAINT tb_matriculadiplomacao_id_usuarioenviadoassinatura_FK FOREIGN KEY (id_usuarioenviadoassinatura) REFERENCES tb_usuario (id_usuario)

ALTER TABLE [dbo].[tb_matriculadiplomacao]
  ADD CONSTRAINT tb_matriculadiplomacao_id_usuarioretornadoassinatura_FK FOREIGN KEY (id_usuarioretornadoassinatura) REFERENCES tb_usuario (id_usuario)

ALTER TABLE [dbo].[tb_matriculadiplomacao]
  ADD CONSTRAINT tb_matriculadiplomacao_id_usuarioenviadosecretaria_FK FOREIGN KEY (id_usuarioenviadosecretaria) REFERENCES tb_usuario (id_usuario)

ALTER TABLE [dbo].[tb_matriculadiplomacao]
  ADD CONSTRAINT tb_matriculadiplomacao_id_usuarioenviadoregistro_FK FOREIGN KEY (id_usuarioenviadoregistro) REFERENCES tb_usuario (id_usuario)

ALTER TABLE [dbo].[tb_matriculadiplomacao]
  ADD CONSTRAINT tb_matriculadiplomacao_id_usuarioretornoregistro_FK FOREIGN KEY (id_usuarioretornoregistro) REFERENCES tb_usuario (id_usuario)

ALTER TABLE [dbo].[tb_matriculadiplomacao]
  ADD CONSTRAINT tb_matriculadiplomacao_id_usuarioenviopolo_FK FOREIGN KEY (id_usuarioenviopolo) REFERENCES tb_usuario (id_usuario)

ALTER TABLE [dbo].[tb_matriculadiplomacao]
  ADD CONSTRAINT tb_matriculadiplomacao_id_usuarioentreguealuno_FK FOREIGN KEY (id_usuarioentreguealuno) REFERENCES tb_usuario (id_usuario)

ALTER TABLE [dbo].[tb_matriculadiplomacao]
  ADD CONSTRAINT tb_matriculadiplomacao_id_usuariohistoricogerado_FK FOREIGN KEY (id_usuariohistoricogerado) REFERENCES tb_usuario (id_usuario)

IF @@ERROR <> 0
  ROLLBACK
ELSE
  COMMIT



/*********************************************
**** TABELA tb_matriculacolacao - GII-7744
**** Data e presença da colação
*********************************************/
BEGIN TRANSACTION

CREATE TABLE
  dbo.tb_matriculacolacao
(
  [id_matriculacolacao] [INT] IDENTITY (1, 1) NOT NULL,
  [id_matricula] [INT] NOT NULL,
  [dt_colacao] [DATETIME] NOT NULL,
  [dt_cadastro] [DATETIME] NOT NULL,
  [id_usuariocadastro] [INT] NOT NULL,
  [bl_presenca] [BIT] NULL,
  [bl_ativo] [BIT] NOT NULL DEFAULT 1,
)

ALTER TABLE [dbo].tb_matriculacolacao
  ADD
  CONSTRAINT [PK_tb_matriculacolacao] PRIMARY KEY CLUSTERED
    (
      [id_matriculacolacao]
    )
    ON [PRIMARY]

ALTER TABLE [dbo].[tb_matriculacolacao]
  ADD CONSTRAINT tb_matriculadiplomacao_id_usuariocadastro_FK FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)

ALTER TABLE [dbo].[tb_matriculacolacao]
  ADD CONSTRAINT tb_matriculadiplomacao_id_matricula_FK FOREIGN KEY (id_matricula) REFERENCES tb_matricula (id_matricula)

IF @@ERROR <> 0
  ROLLBACK
ELSE
  COMMIT
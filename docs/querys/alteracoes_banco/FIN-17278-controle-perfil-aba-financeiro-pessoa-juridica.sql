--DESATIVAR IDENTITY TABELA FIN-16826
SET IDENTITY_INSERT tb_permissao ON;

INSERT INTO tb_permissao (id_permissao, st_nomepermissao, st_descricao, bl_ativo) VALUES (31, 'Aba Financeiro', 'Permite o usu�rio acesso a aba financeiro de Pessoa Jur�dica', 1) ;
INSERT tb_permissaofuncionalidade VALUES (286, 31);

--ATIVAR IDENTITY TABELA
SET IDENTITY_INSERT tb_permissao OFF 
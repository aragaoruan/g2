BEGIN TRAN

-- APAGA AS CONSTRAINTS
ALTER TABLE [dbo].[tb_vendaproduto]
  DROP CONSTRAINT [FK__tb_vendap__id_ti__28A02C17];
ALTER TABLE [dbo].[tb_pessoadadoscomplementares]
  DROP CONSTRAINT [FK_tb_pessoadadoscomplementares_tb_tiposelecao];
ALTER TABLE [dbo].[tb_tiposelecao]
  DROP CONSTRAINT [FK_tb_tiposelecao_tb_usuario];

-- APAGA A TABELA
DROP TABLE [dbo].[tb_tiposelecao];

-- CRIA A TABELA NOVAMENTE
CREATE TABLE [dbo].[tb_tiposelecao] (
  [id_tiposelecao] INT NOT NULL IDENTITY (1, 1),
  [st_tiposelecao] VARCHAR(100) NOT NULL,
  [st_descricao] VARCHAR(200) NULL,
  [bl_ativo] BIT NOT NULL DEFAULT 1,
  [dt_cadastro] DATE NOT NULL DEFAULT getDate(),
  [id_usuariocadastro] INT NULL,
  PRIMARY KEY ([id_tiposelecao]),
  CONSTRAINT [FK_tb_tiposelecao_tb_usuario] FOREIGN KEY ([id_usuariocadastro]) REFERENCES [dbo].[tb_usuario] ([id_usuario])
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)


-- INSERE OS REGISTROS NA TABELA
SET IDENTITY_INSERT tb_tiposelecao ON
INSERT INTO [tb_tiposelecao] ([id_tiposelecao], [st_tiposelecao], [bl_ativo], [dt_cadastro], [id_usuariocadastro], [st_descricao])
VALUES
  (1, 'VESTIBULAR', '1', '2017-8-18 11:59:42.850000', 1, NULL),
  (2, 'PDS', '1', '2017-8-18 11:59:42.850000', 1, NULL),
  (3, 'ENEM', '1', '2017-8-18 11:59:42.850000', 1, NULL),
  (5, 'TRANSFERÊNCIA', '1', '2017-8-18 11:59:42.850000', 1, NULL),
  (6, 'PROUNI', '1', '2017-8-18 11:59:42.850000', 1, NULL),
  (7, 'ANÁLISE DE HISTÓRICO', '1', '2017-8-18 11:59:42.850000', 1, NULL);
SET IDENTITY_INSERT tb_tiposelecao OFF


-- CRIA NOVAMENTE AS CONSTRAINTS
ALTER TABLE [dbo].[tb_vendaproduto]
  ADD CONSTRAINT [FK__tb_vendap__id_ti__28A02C17] FOREIGN KEY ([id_tiposelecao])
REFERENCES [dbo].[tb_tiposelecao] ([id_tiposelecao])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

-- CRIANDO A COLUNA QUE FARÁ O RELACIONAMENTO COM tb_tiposelecao
ALTER TABLE [dbo].[tb_pessoadadoscomplementares]
  ADD [id_tiposelecao] INT NULL;
GO

-- CRIANDO A CONSTRAINT COM A TABELA tb_tiposelecao
ALTER TABLE [dbo].[tb_pessoadadoscomplementares]
  ADD CONSTRAINT [FK_tb_pessoadadoscomplementares_tb_tiposelecao] FOREIGN KEY ([id_tiposelecao]) REFERENCES [dbo].[tb_tiposelecao] ([id_tiposelecao])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
GO


-- Atualiza todos os registros com os seus novos respectivos id's de forma de ingresso
-- VESTIBUlAR 1
UPDATE tb_pessoadadoscomplementares
SET id_tiposelecao = 1
WHERE
  id_formaingresso = 1;

-- PDS 2
UPDATE tb_pessoadadoscomplementares
SET id_tiposelecao = 2
WHERE
  id_formaingresso = 4;

-- ENEM 3
UPDATE tb_pessoadadoscomplementares
SET id_tiposelecao = 3
WHERE
  id_formaingresso = 3;

-- TRANFERENCIA 5
UPDATE tb_pessoadadoscomplementares
SET id_tiposelecao = 5
WHERE
  id_formaingresso = 5;

-- PAD 8
UPDATE tb_pessoadadoscomplementares
SET id_tiposelecao = 1
WHERE
  id_formaingresso = 2;

-- REMOVE A COLUNA id_formaigresso
ALTER TABLE [dbo].[tb_pessoadadoscomplementares]
  DROP COLUMN [id_formaingresso]
GO

--RENOMEIA A COLUNA PARA O NOME AGRADAVEL
EXEC sp_rename N'[dbo].[tb_pessoadadoscomplementares].[id_tiposelecao]', N'id_formaingresso', 'COLUMN'
GO


--TODO VERIFICAR ANTE DE EXECUTAR
COMMIT
--ROLLBACK
--INSERT DA FUNCIONALIDADE--
--DESATIVAR IDENTITY TABELA
SET IDENTITY_INSERT tb_funcionalidade ON
INSERT INTO tb_funcionalidade
(id_funcionalidade, st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao,
 st_classeflex, st_urlicone, id_tipofuncionalidade, nu_ordem, st_classeflexbotao, bl_pesquisa,
 bl_lista, id_sistema, st_ajuda, st_target, st_urlcaminho, bl_visivel, bl_relatorio,
 st_urlcaminhoedicao, bl_delete)
VALUES
  (796, 'Formas de Ingresso', 231, 1, 123,
        'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa', NULL, 3, 0, 'FormaIngresso', 1,
   0, 1, NULL, NULL, '/tipo-selecao', 1, 0, NULL, 1)
--ATIVAR IDENTITY TABELA
SET IDENTITY_INSERT tb_funcionalidade OFF

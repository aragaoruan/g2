-- OBS.: Em produção, verificar o último id_funcionalidade cadastrado e alterar.

SET IDENTITY_INSERT tb_funcionalidade ON

INSERT INTO tb_funcionalidade
(
id_funcionalidade,
st_funcionalidade,
bl_ativo,
st_classeflex,
id_situacao,
st_urlicone,
id_tipofuncionalidade,
nu_ordem,
bl_pesquisa,
bl_lista,
id_sistema,
st_urlcaminho,
bl_visivel,
bl_relatorio,
bl_delete
)
VALUES
(770,
'Grade de Notas',
1,
'app.grade-notas',
182,
'icon-icon_nota',
1,
1,
1,
1,
4,
'app.grade-notas',
1,
0,
0
);

SET IDENTITY_INSERT tb_funcionalidade OFF

-- O G2 não exibia a opção de setar a funcionalidade no perfil.
-- Adicionei manualmente no tb_perfilfuncionalidade por meio do SQL:
-- 	INSERT INTO tb_perfilfuncionalidade
-- 	VALUES (75, 770, 1);
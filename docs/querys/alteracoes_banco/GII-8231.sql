--INSERT DA FUNCIONALIDADE--
SET IDENTITY_INSERT tb_funcionalidade ON;
INSERT INTO tb_funcionalidade
(id_funcionalidade, st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao,
 st_classeflex, st_urlicone, id_tipofuncionalidade, nu_ordem, st_classeflexbotao, bl_pesquisa,
 bl_lista, id_sistema, st_ajuda, st_target, st_urlcaminho, bl_visivel, bl_relatorio,
 st_urlcaminhoedicao, bl_delete)
VALUES
  (803, 'Validação das Atividades Complementares', 157, 1, 123,
        'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa', NULL, 3, 0, 'AtividadeComplementar', 0,
   0, 1, NULL, NULL, 'atividade-complementar', 1, 0, NULL, 0);
SET IDENTITY_INSERT tb_funcionalidade OFF;
ALTER TABLE [dbo].[tb_atividadecomplementar]
  ADD [st_observacaoavaliador] VARCHAR(300) NULL
GO


-- SQLs AC-27366

-- Verificar o último id_funcionalidade cadastrado no banco e substituir o id_funcionalidade pelo número seguinte.

SET IDENTITY_INSERT tb_funcionalidade ON

INSERT INTO tb_funcionalidade (id_funcionalidade, st_urlcaminho, id_funcionalidadepai, st_funcionalidade,
bl_ativo, id_situacao, id_tipofuncionalidade, bl_pesquisa, bl_lista, bl_visivel, bl_relatorio, bl_delete, id_sistema)
VALUES (
739,
'739',
305,
'Edição da Data Concluinte na Tela Conclusão',
1,
123,
7,
0,
0,
0,
0,
0,
1);

SET IDENTITY_INSERT tb_funcionalidade OFF


INSERT INTO tb_permissao (st_nomepermissao, st_descricao, bl_ativo) VALUES (
	'Editar Data Concluinte na tela Conclusão',
	'Permite editar a data concluinte da matrícula na tela Conclusão. Alterando também a data concluinte exibida no certificado.',
	1
);


INSERT INTO tb_permissaofuncionalidade (id_funcionalidade, id_permissao) VALUES (
    739,
    30
);
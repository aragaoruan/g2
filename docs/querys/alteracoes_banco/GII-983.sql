CREATE TABLE [dbo].[tb_disciplinaorigem] (
  [id_disciplinaorigem] INT NOT NULL IDENTITY (1, 1),
  [bl_ativo] BIT NOT NULL DEFAULT 1,
  [dt_cadastro] DATETIME2 NOT NULL DEFAULT getDate(),
  [id_usuariocadastro] INT NOT NULL,
  [id_aluno] INT NOT NULL,
  [st_disciplina] VARCHAR(140) NOT NULL,
  [nu_cargahoraria] INT NULL,
  [st_instituicao] VARCHAR(140) NOT NULL,
  [sg_uf] CHAR(2) NOT NULL,
  PRIMARY KEY ([id_disciplinaorigem]),
  CONSTRAINT [FK_tb_disciplinaorigem_tb_usuario_cadastro] FOREIGN KEY ([id_usuariocadastro]) REFERENCES [dbo].[tb_usuario] ([id_usuario])
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT [FK_tb_disciplinaorigem_tb_usuario_aluno] FOREIGN KEY ([id_aluno]) REFERENCES [dbo].[tb_usuario] ([id_usuario])
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT [FK_tb_disciplinaorigem_tb_uf] FOREIGN KEY ([sg_uf]) REFERENCES [dbo].[tb_uf] ([sg_uf])
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);
GO

CREATE TABLE [dbo].[tb_discmatriculadiscorigem] (
  [id_discmatriculadiscorigem] INT NOT NULL IDENTITY (1, 1),
  [dt_cadastro] DATETIME2 NOT NULL DEFAULT getDate(),
  [id_usuariocadastro] INT NOT NULL,
  [id_entidadecadastro] INT NOT NULL,
  [id_matriculadisciplina] INT NOT NULL,
  [id_disciplinaorigem] INT NOT NULL,
  PRIMARY KEY ([id_discmatriculadiscorigem]),
  CONSTRAINT [FK_tb_discmatriculadisorigem_tb_usuario] FOREIGN KEY ([id_usuariocadastro]) REFERENCES [dbo].[tb_usuario] ([id_usuario])
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT [FK_tb_discmatriculadisorigem_tb_entidade] FOREIGN KEY ([id_entidadecadastro]) REFERENCES [dbo].[tb_entidade] ([id_entidade])
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT [FK_tb_discmatriculadisorigem_tb_matriculadisciplina] FOREIGN KEY ([id_matriculadisciplina]) REFERENCES [dbo].[tb_matriculadisciplina] ([id_matriculadisciplina])
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT [FK_tb_discmatriculadisorigem_tb_disciplinaorigem] FOREIGN KEY ([id_disciplinaorigem]) REFERENCES [dbo].[tb_disciplinaorigem] ([id_disciplinaorigem])
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);
GO

/*
**********************************************************************************
**** ATANÇÃO O SCRIPT ABAIXO DEVERÁ SER EXECUTADO NA ORDEM EM QUE SE ENCONTRA ****
**********************************************************************************
*/

-- Cria a coluna temporaria --
ALTER TABLE tb_disciplinaorigem
  ADD id_matriculadisciplinatemp INT NULL;

-- Abre transação --
BEGIN TRAN

-- Faz o insert das disciplinas de origem --
INSERT INTO tb_disciplinaorigem (
  st_disciplina,
  st_instituicao,
  sg_uf,
  nu_cargahoraria,
  id_usuariocadastro,
  id_aluno,
  id_matriculadisciplinatemp
) (
  SELECT
    mtdisc.st_disciplinaoriginal,
    mtdisc.st_instituicaoaproveitamento,
    mtdisc.sg_uf,
    mtdisc.nu_cargahorariaaproveitamento,
    ap.id_usuario,
    mt.id_usuario,
    mtdisc.id_matriculadisciplina
  FROM
    tb_matriculadisciplina AS mtdisc
    JOIN tb_matricula AS mt ON mt.id_matricula = mtdisc.id_matricula
    JOIN tb_aproveitamento AS ap ON mtdisc.id_aproveitamento = ap.id_aproveitamento
  WHERE
    mtdisc.st_disciplinaoriginal IS NOT NULL
);

-- insere na tb_discmatriculadiscorigem --
-- o relacionamento entre --
-- a tb_disciplinaorigem e tb_matriculadisciplina --
INSERT INTO tb_discmatriculadiscorigem (id_usuariocadastro, id_entidadecadastro, id_matriculadisciplina, id_disciplinaorigem)
  (SELECT
     dco.id_usuariocadastro,
     mt.id_entidadematricula,
     mtd.id_matriculadisciplina,
     dco.id_disciplinaorigem
   FROM
     tb_disciplinaorigem AS dco
     JOIN tb_matriculadisciplina AS mtd ON mtd.id_matriculadisciplina = dco.id_matriculadisciplinatemp
     JOIN tb_matricula AS mt ON mt.id_matricula = mtd.id_matricula);

-- rollback --
COMMIT


-- remove a coluna temporaria --
ALTER TABLE tb_disciplinaorigem
  DROP COLUMN id_matriculadisciplinatemp;

/*
**********************************************************************************
******************************* FIM **********************************************
**********************************************************************************
*/





begin tran
SET IDENTITY_INSERT dbo.tb_funcionalidade on
INSERT INTO tb_funcionalidade (
id_funcionalidade,
st_funcionalidade,
id_funcionalidadepai,
bl_ativo,
id_situacao,
st_classeflex,
st_urlicone,
id_tipofuncionalidade,
nu_ordem,
st_classeflexbotao,
bl_pesquisa,
bl_lista,
id_sistema,
st_ajuda,
st_target,
st_urlcaminho,
bl_visivel,
bl_relatorio,
st_urlcaminhoedicao
)
VALUES (
728,
'Perguntas Frequentes',
null,
1,
123,
'/perguntas-frequentes',
'/img/ico/money--arrow.png',
1,
NULL,
'ajax',
1,
0,
4,
NULL,
NULL,
'728',
1,
0,
NULL
);
GO
SET IDENTITY_INSERT dbo.tb_funcionalidade off
COMMIT

-- alteração na tb_ocorrencia
ALTER TABLE [dbo].[tb_ocorrencia]
ADD [st_codissue] varchar(50) NULL
GO

--criando o sistema no banco
SET IDENTITY_INSERT dbo.tb_sistema on

insert into tb_sistema (id_sistema, st_sistema, bl_ativo, st_conexao) values(28,'Jira',1,'http://jira.unyleya.com.br/rest/api/2/issue');

SET IDENTITY_INSERT dbo.tb_sistema off



--criando a integração do g2 com o jira
insert into tb_entidadeintegracao
(
	id_entidade,
	id_usuariocadastro,
	id_sistema,
	dt_cadastro,
	st_caminho,
	st_codsistema,
	st_codchave,
	st_nomeintegracao
)
values
(
	2,
	1,
	28,
	getdate(),
	'http://jira.unyleya.com.br/rest/api/2/issue',
	'kayo.silva',
	'kayoleandro',
	'G2xJira'
)
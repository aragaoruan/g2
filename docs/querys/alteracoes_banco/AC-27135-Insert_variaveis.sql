SET IDENTITY_INSERT tb_textovariaveis ON;

INSERT INTO tb_textovariaveis
(id_textovariaveis, id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
VALUES
(391, 3, 'dt_previsaofim', '#previsao_termino#', 'retornaMesAnoPorExtenso', 1),
(392, 3, 'dt_terminoturma', '#data_termino_turma#', 'retornaMesAnoPorExtenso', 1),
(393, 3, 'dt_inicioturma', '#data_inicio_turma#', 'retornaDataPorExtenso', 1);

SET IDENTITY_INSERT tb_textovariaveis OFF;

UPDATE tb_textovariaveis SET
st_camposubstituir = 'dt_primeirasala', st_mascara = 'retornaDataPorExtenso', id_tipotextovariavel = 1
WHERE id_textovariaveis = 261;

-- GII-8667
-- Remover registros não utilizados
DELETE FROM tb_tiposanguineo
WHERE id_tiposanguineo IN (5, 6, 7, 8);

-- Atualizar registros que serão utilizados
UPDATE tb_tiposanguineo
SET sg_tiposanguineo = 'A'
WHERE id_tiposanguineo = 1;
UPDATE tb_tiposanguineo
SET sg_tiposanguineo = 'B'
WHERE id_tiposanguineo = 2;
UPDATE tb_tiposanguineo
SET sg_tiposanguineo = 'AB'
WHERE id_tiposanguineo = 3;
UPDATE tb_tiposanguineo
SET sg_tiposanguineo = 'O'
WHERE id_tiposanguineo = 4;

-- Ajustar coluna da tabela
ALTER TABLE tb_tiposanguineo
  ALTER COLUMN sg_tiposanguineo VARCHAR(2) NOT NULL;

-- Adicionar campos na tabela
ALTER TABLE tb_pessoa
  ADD id_tiposanguineo INT NULL
  CONSTRAINT tb_pessoa_tb_tiposanguineo_fk
  FOREIGN KEY (id_tiposanguineo)
  REFERENCES tb_tiposanguineo (id_tiposanguineo),
  sg_fatorrh CHAR(1) NULL;

-- Cadastrar a variável ausente
SET IDENTITY_INSERT tb_textovariaveis ON;
INSERT INTO tb_textovariaveis (id_textovariaveis, id_textocategoria, st_camposubstituir, st_textovariaveis, id_tipotextovariavel, bl_exibicao)
VALUES (482, 2, 'st_fatorrh', '#fator_rh#', 1, 1);
SET IDENTITY_INSERT tb_textovariaveis OFF;

alter table tb_cancelamento add nu_multaporcentagemcarta decimal(5,2) NULL;
alter table tb_cancelamento add nu_multaporcentagemdevolucao decimal(18,2) NULL;
alter table tb_cancelamento add nu_valormultacarta decimal(5,2) NULL;
alter table tb_cancelamento add nu_valormultadevolucao decimal(18,2) NULL;
alter table tb_cancelamento add nu_valorcarta decimal (18,2)
alter table tb_cancelamento drop column nu_valordesembolsado
alter table tb_cancelamento drop column nu_valormulta
alter table tb_cancelamento drop column nu_multaporcentagem

-------------------------------------------------------------------------------------------------

USE [G2H_EAD1]
GO

/****** Object:  View [dbo].[vw_gerardeclaracao]    Script Date: 25/02/2015 15:44:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[vw_gerardeclaracao]

as

SELECT DISTINCT

mt.id_matricula,
ps.id_usuario AS id_usuarioaluno,
ps.st_nomecompleto AS st_nomecompletoaluno,
ps.st_login AS st_loginaluno,
ps.st_senhaentidade AS st_senhaaluno,
ps.st_cpf AS st_cpfaluno,
tur.id_turma AS id_turma,
CONVERT(CHAR, mt.dt_cadastro, 103) AS st_datamatricula,
canc.st_observacao as st_observacaocancelamento,
canc.st_observacaocalculo as st_observacao_calculo,
ps.st_rg AS st_rgaluno,
ps.st_orgaoexpeditor,
ps.dt_dataexpedicao AS dt_dataexpedicaoaluno,
ps.st_nomemae AS st_filiacao,
CONVERT(CHAR, canc.dt_solicitacao, 103) AS st_solicitacaocancelamento,
	canc.nu_valorcarta AS st_valorcredito,
	CONVERT(CHAR, DATEADD(D, 365, canc.dt_solicitacao), 103) AS st_validadecredito,
	canc.id_cancelamento,
	cm.id_contrato,
	ps.dt_nascimento AS dt_nascimentoaluno,
	UPPER(SUBSTRING(ps.st_nomemunicipio, 1, 1)) +
	LOWER(SUBSTRING(ps.st_nomemunicipio, 2, 499)) AS st_municipioaluno,
	ps.st_estadoprovincia AS st_ufaluno,
	ps.sg_uf AS sg_ufaluno,
	ps.nu_ddd AS nu_dddaluno,
	ps.nu_telefone AS nu_telefonealuno,
	ps.st_email as st_emailaluno,
	pp.st_tituloexibicao AS st_projeto,
	pp.nu_cargahoraria,
	ne.st_nivelensino,
	mt.dt_concluinte AS dt_concluintealuno,
	us_coordenador.st_nomecompleto AS st_coordenadorprojeto,
	fl.id_fundamentolegal AS id_fundamentolei,
	fl.nu_numero AS nu_lei,
	flr.id_fundamentolegal AS id_fundamentoresolucao,
	tur.st_turma,
	tur.dt_fim AS dt_terminoturma,
	tur.dt_inicio as dt_inicioturma,
	flr.nu_numero AS nu_resolucao,
	flpr.id_fundamentolegal AS id_fundamentoparecer,
	flpr.nu_numero AS nu_parecer,
	flp.id_fundamentolegal AS id_fundamentoportaria,
	flp.nu_numero AS nu_portaria,
	et.id_entidade,
	et.st_nomeentidade,
	et.st_razaosocial,
	et.st_cnpj,
	et.st_urlimglogo,
	et.st_urlsite,
	ps.st_nomepais,
	ps.st_sexo AS sexo_aluno,
	av.st_tituloavaliacao AS st_titulomonografia,
	CONVERT(VARCHAR(2), DATEPART(DAY, GETDATE()),103) + ' de ' +
	dbo.fn_mesporextenso(GETDATE()) + ' de ' +
	CONVERT(VARCHAR(4), DATEPART(YEAR, GETDATE()),103) AS st_data,
	DAY(GETDATE()) AS st_dia,
	dbo.fn_mesporextenso(GETDATE()) AS st_mes ,
	YEAR(GETDATE()) AS st_ano,
	CONVERT(DATE, sa.dt_abertura, 103) AS dt_primeirasala,
	CONVERT(DATE, DATEADD(MONTH, 13, sa.dt_abertura), 103) AS dt_previsaofim,
	mt.st_codcertificacao, CONVERT(CHAR,GETDATE(),103) AS st_atual
FROM tb_matricula mt
JOIN tb_projetopedagogico AS pp
	ON pp.id_projetopedagogico = mt.id_projetopedagogico
JOIN tb_entidade AS et
	ON et.id_entidade = mt.id_entidadeatendimento
LEFT JOIN tb_entidadeendereco AS ete
	ON ete.id_entidade = et.id_entidade
	AND ete.bl_padrao = 1
LEFT JOIN tb_endereco AS etee
	ON etee.id_endereco = ete.id_endereco
JOIN vw_pessoa as ps on ps.id_usuario = mt.id_usuario and ps.id_entidade = mt.id_entidadeatendimento
LEFT JOIN tb_fundamentolegal AS fl
	ON fl.id_entidade = mt.id_entidadematriz
	AND fl.id_tipofundamentolegal = 1
	AND fl.dt_publicacao <= GETDATE()
	AND fl.dt_vigencia >= GETDATE()
LEFT JOIN tb_fundamentolegal AS flp
	ON flp.id_entidade = mt.id_entidadematriz
	AND flp.id_tipofundamentolegal = 2
	AND flp.dt_publicacao <= GETDATE()
	AND flp.dt_vigencia >= GETDATE()
LEFT JOIN tb_fundamentolegal AS flr
	ON flr.id_entidade = mt.id_entidadematriz
	AND flr.id_tipofundamentolegal = 3
	AND flr.dt_publicacao <= GETDATE()
	AND flr.dt_vigencia >= GETDATE()
LEFT JOIN tb_fundamentolegal AS flpr
	ON flpr.id_entidade = mt.id_entidadematriz
	AND flpr.id_tipofundamentolegal = 4
	AND flpr.dt_publicacao <= GETDATE()
	AND flpr.dt_vigencia >= GETDATE()
LEFT JOIN tb_projetopedagogicoserienivelensino AS psn
	ON psn.id_projetopedagogico = mt.id_projetopedagogico
LEFT JOIN tb_turma AS tur
	ON tur.id_turma = mt.id_turma
JOIN tb_nivelensino AS ne
ON ne.id_nivelensino = psn.id_nivelensino
LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS uper
	ON uper.id_projetopedagogico = pp.id_projetopedagogico
	AND uper.bl_ativo = 1
	AND uper.bl_titular = 1
LEFT JOIN dbo.tb_usuario AS us_coordenador
	ON us_coordenador.id_usuario = uper.id_usuario
OUTER APPLY (SELECT TOP 1
	ts.dt_abertura
FROM tb_matriculadisciplina tm
JOIN tb_alocacao ta
	ON tm.id_matriculadisciplina = ta.id_matriculadisciplina
JOIN tb_saladeaula ts
	ON ta.id_saladeaula = ts.id_saladeaula
WHERE tm.id_matricula = mt.id_matricula
ORDER BY ts.dt_abertura DESC) AS sa
LEFT JOIN dbo.vw_avaliacaoaluno av
	ON (av.id_matricula = mt.id_matricula
	AND av.id_tipoavaliacao = 6
	AND av.st_tituloavaliacao IS NOT NULL
	AND id_tipodisciplina = 2
	AND av.bl_ativo = 1)
LEFT JOIN tb_cancelamento AS canc
	ON canc.id_cancelamento = mt.id_cancelamento
LEFT JOIN tb_contratomatricula AS cm
	ON cm.id_matricula = mt.id_matricula
GO


----------------------------------------------------------------------------

insert into tb_permissao values ('12','Editar Multa do Cancelamento', 'Permitir a variação da multa de cancelamento', 1)
insert into tb_permissaofuncionalidade values (656, 12)

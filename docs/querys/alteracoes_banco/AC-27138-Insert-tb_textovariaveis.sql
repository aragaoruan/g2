-- AC-27138

SET IDENTITY_INSERT tb_textovariaveis ON;

INSERT INTO tb_textovariaveis (id_textovariaveis, id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
VALUES
(403, 3, 'id_matricula', '#grid_disciplina_projeto_pedagogico#', 'gerarGridDisciplinasProjeto', 2),
(404, 3, 'id_matricula', '#grid_disciplina_encerrada_com_nota#', 'gerarGridDisciplinasEncerradasComNota', 2);

SET IDENTITY_INSERT tb_textovariaveis OFF;
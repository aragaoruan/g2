
CREATE TABLE "tb_usuariodevice"
(
   id_usuariodevice int PRIMARY KEY NOT NULL,
   st_key varchar(250),
   id_usuario int,
   st_infodevice varchar(250),
   dt_cadastro datetime DEFAULT (getdate()),
   CONSTRAINT FK_TB_USUARIO_DEVICE_TB_USUARIO FOREIGN KEY (id_usuario) REFERENCES "dbo"."tb_usuario"(id_usuario) ON DELETE CASCADE ON UPDATE CASCADE
)
-- GII-8672
ALTER TABLE tb_matriculadiplomacao
  ADD st_observacao VARCHAR(2000) NULL;

-- Adicionar novas variáveis
SET IDENTITY_INSERT dbo.tb_textovariaveis ON;
INSERT INTO tb_textovariaveis
(id_textovariaveis, id_textocategoria, st_camposubstituir, st_textovariaveis, id_tipotextovariavel, bl_exibicao)
VALUES
  (480, 3, 'st_observacaohistorico', '#observacao_historico_escolar#', 1, 1);
SET IDENTITY_INSERT dbo.tb_textovariaveis OFF;
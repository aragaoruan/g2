USE [G2H_EAD1_2014]
GO

/****** Object:  View [rel].[vw_provasporpolo]    Script Date: 07/07/2015 13:22:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE view [rel].[vw_provasporpolo] as 
select 
	ap.id_aplicadorprova,
	aa.id_avaliacaoaplicacao,
	ape.id_entidade,
	ap.st_aplicadorprova, 
	aa.dt_aplicacao, 
	ha.hr_inicio, 
	ha.hr_fim, 
	aa.nu_maxaplicacao,
	(select count(id_avaliacaoagendamento) from tb_avaliacaoagendamento saa where saa.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao) as nu_totalagendamentos,
	(select count(id_avaliacaoagendamento) from tb_avaliacaoagendamento saa2 where saa2.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao and saa2.id_tipodeavaliacao = 0) as nu_agendamentoprovafinal,
	(select count(id_avaliacaoagendamento) from tb_avaliacaoagendamento saa3 where saa3.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao and saa3.id_tipodeavaliacao > 0) as nu_agendamentorecuperacao,
	(nu_maxaplicacao - (select count(id_avaliacaoagendamento) from tb_avaliacaoagendamento saa4 where saa4.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao)) as nu_vagasrestantes
from tb_aplicadorprova ap
join tb_aplicadorprovaentidade ape on ape.id_aplicadorprova = ap.id_aplicadorprova
join tb_avaliacaoaplicacao aa on aa.id_aplicadorprova = ap.id_aplicadorprova
join tb_avaliacao a on a.id_entidade = ape.id_entidade and a.id_tipoavaliacao in(1,2,3,4)
join tb_horarioaula ha on ha.id_horarioaula = aa.id_horarioaula


GO



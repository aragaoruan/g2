/**
	INSERINDO VARIAVEIS PARA TEXTO SISTEMA CATEGORIA (3)-DECLARAÇÕES
	**/

BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_textovariaveis on
 
 INSERT INTO tb_textovariaveis
 (id_textovariaveis, id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
 VALUES
 --Valor do semestre
 (413, 3 , 'id_matricula' , '#valor_semestre#' , 'retornaValorSemestre', 2),
 -- Valor da entrada
(414, 3 , 'id_matricula' , '#valor_entrada#' , 'retornaValorEntrada', 2),
 -- Valor da primeira parcela
(415, 3 , 'id_matricula' , '#valor_mensalidade#' , 'retornaValorParcela', 2),
 -- Número de parcelas
(416, 3 , 'id_matricula' , '#qtd_parcelas#' , 'retornaNumeroParcelas', 2),
-- Coeficiente de Rendimento
(417, 3 , 'id_matricula' , '#CR_acumulado#' , 'retornaCoeficienteRendimento', 2)
 SET IDENTITY_INSERT dbo.tb_textovariaveis off

 COMMIT
 --ROLLBACK


/**
 ADD CAMPO nu_semestre na tb_matriculadisciplina
**/

ALTER TABLE tb_matriculadisciplina
ADD nu_semestre INT NULL 

/**
 ADD CAMPO nu_semestre na tb_venda 
**/
ALTER TABLE tb_venda
ADD nu_semestre INT NULL 



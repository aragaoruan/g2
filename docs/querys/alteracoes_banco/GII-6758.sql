BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_permissao on
insert into tb_permissao (id_permissao,st_nomepermissao, st_descricao, bl_ativo) VALUES (35,'Titulação', 'Permite alterar a titulação no cadastro de usuário', 1);
SET IDENTITY_INSERT dbo.tb_permissao off
commit
insert into tb_permissaofuncionalidade (id_funcionalidade, id_permissao) VALUES (8, 35);
/**
Variaveis para envio de renovacao de matricula 
**/

SET IDENTITY_INSERT dbo.tb_textovariaveis on

INSERT INTO tb_textovariaveis
(id_textovariaveis, id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
VALUES
--Grade de disciplinas
(407, 1 , 'id_matricula' , '#grade_disciplina_renovacao#' , 'gerarGridDisciplinasRenovacao', 2),
-- Data de limite de renovacao
(408, 1 , 'st_limiterenovacao' , '#dt_limite_renovacao#' , NULL, 1),
-- Valor da mensalidade
(409, 1 , 'nu_mensalidade' , '#valor_mensalidade#' , 'formataValor', 2),
-- Valor da mensalidade com pontualidade
(410, 1 , 'nu_mensalidadepontualidade' , '#valor_mensalidade_pontualidade#' , 'formataValor', 2),
-- Link para o portal
(411, 1 , 'st_link_grade_portal' , '#link_grade_disciplina_portal#' , 'gerarUrlAcessoGradeDisciplina', 2)

SET IDENTITY_INSERT dbo.tb_textovariaveis off










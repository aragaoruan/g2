/*******************************************************************************************
***   GII-8098 - Novo item de configuração
***   Esquema de configuração
********************************************************************************************/

BEGIN TRANSACTION

INSERT INTO
  tb_itemconfiguracao
  (id_itemconfiguracao,
   st_itemconfiguracao,
   st_descricao,
   st_default
  )
VALUES
  (30,
   'Quantidade de dias para enviar aviso ao aluno',
   'Quantidade de dias para enviar o aviso ao aluno',
   ''
  )

COMMIT




/** Mensagem Padrão **/

BEGIN TRANSACTION

SET IDENTITY_INSERT dbo.tb_mensagempadrao ON
INSERT INTO
  tb_mensagempadrao
  (id_mensagempadrao,
   id_tipoenvio,
   st_mensagempadrao,
   st_default,
   id_textocategoria
  )
VALUES
  (43,
   3,
   'Aviso sobre agendamento',
   'ALTERAR: Aviso sobre agendamento',
   1
  )
SET IDENTITY_INSERT dbo.tb_mensagempadrao OFF

COMMIT


/**
 * Autor: Rafael Bruno (RBD) <rafael.oliveira@gmail.com>
 * Historia Link: http://jira.unyleya.com.br/browse/AC-26073
 * Historia: AC-26073
 */

CREATE VIEW [rel].vw_tcc_sem_arquivo_nota_superior as
SELECT DISTINCT
	al.*,
	upe.id_entidade
FROM vw_avaliacaoaluno al
JOIN tb_matricula m
	ON m.id_matricula = al.id_matricula
JOIN tb_usuarioperfilentidade upe
	ON m.id_usuario = upe.id_usuario
WHERE CONVERT(int, (nu_percentualaprovacao * nu_notamax / 100)) <= CONVERT(int, st_nota)
AND id_upload IS NULL AND id_tipoavaliacao = 6 AND id_tipodisciplina = 2
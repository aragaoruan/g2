-- Categoria: Certificado
INSERT INTO tb_textovariaveis (
  id_textocategoria,
  st_camposubstituir,
  st_textovariaveis,
  st_mascara,
  id_tipotextovariavel,
  bl_exibicao
)

VALUES (4, 'st_areaconhecimentomec', '#area_conhecimento_mec#', null, 1, 1);

-- Categoria: Declaração
INSERT INTO tb_textovariaveis (
  id_textocategoria,
  st_camposubstituir,
  st_textovariaveis,
  st_mascara,
  id_tipotextovariavel,
  bl_exibicao
)

VALUES (3, 'st_areaconhecimentomec', '#area_conhecimento_mec#', null, 1, 1);
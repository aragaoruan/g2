-- GII-8917
SET IDENTITY_INSERT dbo.tb_funcionalidade ON;
INSERT INTO tb_funcionalidade
(id_funcionalidade, st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao, id_tipofuncionalidade, bl_pesquisa,
 bl_lista, id_sistema, st_urlcaminho, bl_visivel, bl_relatorio, bl_delete)
VALUES (831, 'Transferência de Polo', 157, 1, 123, 3, 0, 0, 1, '/transferencia-polo', 1, 0, 0);
SET IDENTITY_INSERT dbo.tb_funcionalidade OFF;
--
--
SET IDENTITY_INSERT tb_permissao ON;
INSERT INTO tb_permissao (id_permissao, st_nomepermissao, st_descricao, bl_ativo)
VALUES (56, 'Transferir Aluno Inadimplente', 'Permite fazer a transferência de polo de alunos inadimplentes', 1);
SET IDENTITY_INSERT tb_permissao OFF;
--
--
INSERT INTO tb_permissaofuncionalidade (id_funcionalidade, id_permissao)
VALUES (831, 56);
--
--
CREATE TABLE tb_transferenciapolo (
  id_transferenciapolo INT IDENTITY (1, 1) NOT NULL,
  dt_cadastro DATETIME NOT NULL DEFAULT GETDATE(),
  bl_ativo BIT NOT NULL DEFAULT 1,
  id_usuariocadastro INT NOT NULL FOREIGN KEY REFERENCES tb_usuario (id_usuario),
  id_matricula INT NOT NULL FOREIGN KEY REFERENCES tb_matricula (id_matricula),
  id_entidadeorigem INT NOT NULL FOREIGN KEY REFERENCES tb_entidade (id_entidade),
  id_entidadedestino INT NOT NULL FOREIGN KEY REFERENCES tb_entidade (id_entidade),
  id_vendaorigem INT NOT NULL FOREIGN KEY REFERENCES tb_venda (id_venda),
  id_vendadestino INT NOT NULL FOREIGN KEY REFERENCES tb_venda (id_venda)
);

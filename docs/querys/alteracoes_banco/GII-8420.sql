-- GII-8420
SET IDENTITY_INSERT tb_textovariaveis ON;
INSERT INTO tb_textovariaveis (id_textovariaveis, id_textocategoria, st_camposubstituir, st_textovariaveis, id_tipotextovariavel, st_mascara, bl_exibicao)
VALUES
  (475, 3, 'st_selecaodescricao', '#forma_ingresso#', 1, NULL, 1),
  (476, 3, 'st_reconhecimento', '#reconhecimento_curso#', 1, NULL, 1),
  (477, 3, 'dt_ingresso', '#ano_ingresso#', 2, 'retornarAno', 0),
  (478, 3, 'dt_ingresso', '#semestre_ingresso#', 2, 'retornarSemestre', 0);
SET IDENTITY_INSERT tb_textovariaveis OFF;
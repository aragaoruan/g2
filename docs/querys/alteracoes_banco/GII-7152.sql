-- GII-7152

CREATE TABLE tb_classeafiliacao (
    id_classeafiliacao INT NOT NULL PRIMARY KEY,
    st_classeafiliacao VARCHAR(255) NOT NULL
);

CREATE TABLE tb_afiliado (
    id_afiliado INT NOT NULL PRIMARY KEY,
    st_afiliado VARCHAR(255) NOT NULL
);

CREATE TABLE tb_afiliadoclasseentidade (
    id_afiliado INT NOT NULL FOREIGN KEY REFERENCES tb_afiliado(id_afiliado),
    id_classeafiliacao INT NOT NULL FOREIGN KEY REFERENCES tb_classeafiliacao(id_classeafiliacao),
    id_entidade INT NOT NULL FOREIGN KEY REFERENCES tb_entidade(id_entidade),
    CONSTRAINT pk_afiliadoclasseentidade PRIMARY KEY (id_afiliado, id_classeafiliacao, id_entidade)
);

CREATE TABLE tb_nucleotelemarketing (
    id_nucleotelemarketing INT NOT NULL PRIMARY KEY,
    st_nucleotelemarketing VARCHAR(255) NOT NULL,
    bl_ativo BIT NOT NULL,
    id_entidade INT NOT NULL FOREIGN KEY REFERENCES tb_entidade(id_entidade)
);

CREATE TABLE tb_vendedor (
    id_vendedor INT NOT NULL PRIMARY KEY,
    st_vendedor VARCHAR(255) NOT NULL,
    bl_ativo BIT NOT NULL,
    id_entidade INT NOT NULL FOREIGN KEY REFERENCES tb_entidade(id_entidade)
);

ALTER TABLE tb_venda ADD id_afiliado INT NULL FOREIGN KEY REFERENCES tb_afiliado(id_afiliado);
ALTER TABLE tb_venda ADD id_nucleotelemarketing INT NULL FOREIGN KEY REFERENCES tb_nucleotelemarketing(id_nucleotelemarketing);
ALTER TABLE tb_venda ADD id_vendedor INT NULL FOREIGN KEY REFERENCES tb_vendedor(id_vendedor);
ALTER TABLE tb_venda ADD st_siteorigem VARCHAR(255) NULL;
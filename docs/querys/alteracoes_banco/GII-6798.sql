/**	
** INSERT FUNCIONALIDADE [ GII-6798 ]
** TRANCAMENTO DE DISCIPLINAS
**/

BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_funcionalidade on
INSERT INTO [dbo].[tb_funcionalidade]
([id_funcionalidade]
,[st_funcionalidade]
,[id_funcionalidadepai]
,[bl_ativo]
,[id_situacao]
,[st_classeflex]
,[st_urlicone]
,[id_tipofuncionalidade]
,[st_classeflexbotao]
,[bl_pesquisa]
,[bl_lista]
,[id_sistema]
,[st_urlcaminho]
,[bl_visivel]
,[bl_relatorio]
,[bl_delete])
VALUES

(783,'Trancamento de disciplina',157,1,123,'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa','',3,null,0,0, 1, 'trancamento-disciplina', 1,0,0)


GO
SET IDENTITY_INSERT dbo.tb_funcionalidade off

COMMIT

/**
** Add campos na tb_alocacao
**/

BEGIN TRANSACTION

ALTER TABLE tb_alocacao 
ADD bl_trancamento bit NOT NULL DEFAULT 0
, id_usuariotrancamento INT NULL
, dt_trancamento DATETIME NULL


COMMIT 


/**
** Add nova evolucao [TRANCADA] para a tb_matriculadisciplina
**/
BEGIN TRANSACTION

SET IDENTITY_INSERT dbo.tb_evolucao on
INSERT INTO 
	tb_evolucao
		(id_evolucao, st_evolucao, st_tabela, st_campo)
	VALUES 
		(70, 'Trancada', 'tb_matriculadisciplina' , 'id_evolucao')
SET IDENTITY_INSERT dbo.tb_evolucao off

COMMIT



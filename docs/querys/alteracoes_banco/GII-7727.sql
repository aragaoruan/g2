-- GII-7727
INSERT INTO
  tb_itemconfiguracao (id_itemconfiguracao, st_itemconfiguracao, st_descricao, st_default)
VALUES
  (26, 'Historico Escolar', 'Historico Escolar', ''),
  (27, 'Diploma Graduação', 'Diploma Graduação', '');
INSERT INTO tb_esquemaconfiguracaoitem
(id_esquemaconfiguracao, id_itemconfiguracao, st_valor)
VALUES
  (1, 26, 1487),
  (1, 27, 1488);

-- Novas Colunas
ALTER TABLE tb_textosistema
  ADD bl_edicao BIT NOT NULL DEFAULT (1);
ALTER TABLE tb_textovariaveis
  ADD bl_exibicao BIT NOT NULL DEFAULT (1);

-- Atualizar variáveis
UPDATE tb_textovariaveis
SET st_camposubstituir = 'st_rgaluno'
WHERE id_textovariaveis = 6;
UPDATE tb_textovariaveis
SET st_mascara = 'convertDataToBr'
WHERE id_textovariaveis = 419;

-- Adicionar novas variáveis
SET IDENTITY_INSERT dbo.tb_textovariaveis ON;
INSERT INTO tb_textovariaveis
(id_textovariaveis, id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel, bl_exibicao)
VALUES
  (447, 3, 'st_pai', '#nome_pai#', NULL, 1, 0),
  (448, 3, 'dt_dataexpedicaoaluno', '#data_expedicao_aluno#', 'converteDataCliente', 2, 1),
  (449, 3, 'st_nomeinstituicao', '#estabelecimento_conclusao#', 'retornaVariaveisUpper', 1, 1),
  (450, 3, 'st_municipioinstituicao', '#municipio _estabelecimento#', NULL, 1, 1),
  (451, 3, 'ano_conclusao_medio', '#ano_conclusao_medio#', NULL, 1, 1),
  (452, 3, 'st_filiacao', '#nome_mae#', NULL, 1, 1),
  (453, 3, 'st_ufnascimento', '#naturalidade_aluno#', NULL, 1, 1),
  (454, 3, 'dt_dataexpedicaoaluno', '#data_expedicao_aluno_DD_MM_AAAA#', 'convertDataToBr', 1, 1),
  (455, 3, 'id_matricula', '#grid_historico_escolar_disciplina#', 'gerarGridHistoricoEscolarDisciplinas', 2, 1),
  (456, 3, 'st_tituloeleitor', '#titulo_eleitor#', NULL, 1, 1),
  (457, 3, 'st_zonaeleitoral', '#zona_eleitoral#', NULL, 1, 1),
  (458, 3, 'st_municipioeleitoral', '#municipio_eleitoral#', NULL, 1, 1),
  (459, 3, 'st_secaoeleitoral', '#secao_eleitoral#', NULL, 1, 1),
  (460, 3, 'st_certificadoreservista', '#certificado_reservista#', NULL, 1, 1),
  (461, 3, 'st_dtexpedicaocertificadoreservista', '#data_expedicao_reservista#', NULL, 1, 1),
  (462, 3, 'st_categoriaservicomilitar', '#categoria_servico_militar#', NULL, 1, 1),
  (463, 3, 'st_reparticaoexpedidora', '#reparticao_expedidora#', NULL, 1, 1),
  (464, 3, 'st_reconhecimento', '#reconhecimento#', NULL, 1, 1),
  (465, 3, 'dt_colacao', '#data_colacao_grau#', 'convertDataToBr', 1, 1),
  (466, 3, 'dt_diplomagerado', '#data_expedicao_diploma#', 'convertDataToBr', 1, 1),
  (467, 3, 'dt_nascimentoaluno', '#data_nascimento_aluno_DD_MM_AAAA#', 'convertDataToBr', 1, 1);
SET IDENTITY_INSERT dbo.tb_textovariaveis OFF;
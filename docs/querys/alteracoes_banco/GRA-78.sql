--GRA-78

SET IDENTITY_INSERT tb_situacao ON;
INSERT INTO tb_situacao(id_situacao, st_situacao, st_tabela, st_campo, st_descricaosituacao)
VALUES(177, 'Apto para Recuperação', 'tb_matriculadisciplina', 'id_situacaoagendamento', 'Define se o aluno está apto para agendar a prova de recuperação quando o agendamento for por disciplina.')
SET IDENTITY_INSERT tb_situacao OFF;

ALTER TABLE tb_matriculadisciplina ADD id_situacaoagendamento INT NOT NULL DEFAULT (119); -- 119 = Nao Apto para Agendamento

ALTER TABLE tb_matriculadisciplina WITH CHECK ADD CONSTRAINT [tb_matriculadisciplina_tb_situacao_FK] FOREIGN KEY([id_situacaoagendamento])
REFERENCES tb_situacao (id_situacao);

CREATE NONCLUSTERED INDEX [idx_id_avaliacaoconjuntoreferencia]
ON [dbo].[tb_avalagendamentoref] ([id_avaliacaoagendamento],[id_avaliacaoconjuntoreferencia]);
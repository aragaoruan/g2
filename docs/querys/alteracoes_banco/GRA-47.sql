ALTER TABLE tb_venda NOCHECK constraint all;
ALTER TABLE tb_venda ADD id_ocorrenciaaproveitamento int default null;
ALTER TABLE tb_venda CHECK constraint ALL;
ALTER TABLE tb_venda ADD CONSTRAINT FK_VENDA_OCORRENCIAAPROVEITAMENTO_OCORRENCIA FOREIGN KEY (id_ocorrenciaaproveitamento) REFERENCES dbo.tb_ocorrencia(id_ocorrencia);


INSERT INTO dbo.tb_motivoocorrencia
        ( id_motivoocorrencia ,
          st_motivoocorrencia
        )
VALUES  ( 3 , -- id_motivoocorrencia - int
          'Aproveitamento de Crédito'  -- st_motivoocorrencia - varchar(100)
        );

-- AC-26962

-- CRIANDO UMA FUNCIONALIDADE PARA PODER CRIAR AS PERMISSOES
SET IDENTITY_INSERT tb_funcionalidade ON;
INSERT INTO tb_funcionalidade (id_funcionalidade, st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao, st_classeflex, st_urlicone, id_tipofuncionalidade, nu_ordem, st_classeflexbotao, bl_pesquisa, bl_lista, id_sistema, st_ajuda, st_target, st_urlcaminho, bl_visivel, bl_relatorio, st_urlcaminhoedicao, bl_delete)
VALUES (701, 'Matrículas (Evolução)', '157', '1', '123', '', '', '7',  NULL,  NULL, '0', '0', '1', '',  NULL, '701', '1', '0',  NULL, '0');
SET IDENTITY_INSERT tb_funcionalidade OFF;


-- CRIANDO AS NOVAS PERMISSOES
SET IDENTITY_INSERT tb_permissao ON;
INSERT INTO tb_permissao (id_permissao, st_nomepermissao, st_descricao, bl_ativo) VALUES
(14, 'Bloquear', 'Permitir alterar evolução da matrícula para Bloquear', 1),
(15, 'Trancar', 'Permitir alterar evolução da matrícula para Trancar', 1),
(16, 'Cancelar', 'Permitir alterar evolução da matrícula para Cancelar', 1),
(17, 'Anular', 'Permitir alterar evolução da matrícula para Anular', 1),
(18, 'Transferir de Entidade', 'Permitir alterar evolução da matrícula para Transferido de Entidade', 1),
(19, 'Liberar Acesso', 'Permitir alterar evolução da matrícula para Liberar Acesso', 1);
SET IDENTITY_INSERT tb_permissao OFF;


-- ATRIBUINDO AS PERMISSOES CRIADAS NA FUNCIONALIDADE
INSERT INTO tb_permissaofuncionalidade (id_funcionalidade, id_permissao) VALUES
(701, 14),
(701, 15),
(701, 16),
(701, 17),
(701, 18),
(701, 19);


-- INSERIR PERMISSOES NO PERFIL ADMINISTRADOR DA PLATAFORMA
INSERT INTO tb_perfilpermissaofuncionalidade (id_perfil, id_funcionalidade, id_permissao) VALUES 
(1, 701, 14),
(1, 701, 15),
(1, 701, 16),
(1, 701, 17),
(1, 701, 18),
(1, 701, 19);
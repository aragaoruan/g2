-- GII-6472

-- Evolucao
SET IDENTITY_INSERT tb_evolucao ON;
INSERT INTO tb_evolucao (id_evolucao, st_evolucao, st_tabela, st_campo) VALUES (69, 'Decurso de Prazo', 'tb_matricula', 'id_evolucao');
SET IDENTITY_INSERT tb_evolucao OFF;

-- Permissao
SET IDENTITY_INSERT tb_permissao ON;
INSERT INTO tb_permissao (id_permissao, st_nomepermissao, st_descricao, bl_ativo) VALUES (37, 'Decurso de Prazo', 'Permitir alterar evolução da matrícula para Decurso de Prazo', 1);
SET IDENTITY_INSERT tb_permissao OFF;

-- Permissao / Funcionalidade
-- Chave composta
INSERT INTO tb_permissaofuncionalidade (id_funcionalidade, id_permissao) VALUES (701, 37);
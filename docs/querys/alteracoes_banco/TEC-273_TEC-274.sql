SET IDENTITY_INSERT dbo.tb_situacao ON;
INSERT INTO tb_situacao (id_situacao, st_situacao, st_tabela, st_campo, st_descricaosituacao) VALUES (165, 'Ativo', 'tb_dashboard', 'id_situacao', 'Dashboard será mostrado na Home');
INSERT INTO tb_situacao (id_situacao, st_situacao, st_tabela, st_campo, st_descricaosituacao) VALUES (166, 'Inativo', 'tb_dashboard', 'id_situacao', 'Dashboard não será mostrado na Home');
SET IDENTITY_INSERT dbo.tb_situacao OFF;

SET IDENTITY_INSERT dbo.tb_funcionalidade ON;
INSERT INTO tb_funcionalidade (id_funcionalidade, st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao, st_classeflex, id_tipofuncionalidade, bl_pesquisa, bl_lista, id_sistema, st_urlcaminho, bl_visivel, bl_relatorio, bl_delete) VALUES (676, 'Dashboard', 231, 1, 123, '', 3, 0, 0, 1, '/dashboard', 1, 0, 1)
SET IDENTITY_INSERT dbo.tb_funcionalidade OFF;


CREATE TABLE [tb_dashboard](
	[id_dashboard] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[st_dashboard] [varchar](250) NOT NULL,
	[st_descricao] [varchar](max) NULL,
	[st_origem] [varchar](250) NOT NULL,
	[st_tipochart] [varchar](250) NOT NULL,
	[st_xtipo] [varchar](250) NULL,
	[st_xtitulo] [varchar](250) NULL,
	[st_ytipo] [varchar](250) NULL,
	[st_ytitulo] [varchar](250) NULL,
	[st_campolabel] [varchar] (250) NULL,
	[id_situacao] [int] NOT NULL,
	[id_funcionalidade] [int] NULL,
	[bl_ativo] [bit] NOT NULL
 CONSTRAINT [PK_TB_DASHBOARD] PRIMARY KEY CLUSTERED 
(
	[id_dashboard] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];

ALTER TABLE [dbo].[tb_dashboard]  WITH CHECK ADD  CONSTRAINT [tb_situacao_tb_dashboard_fk] FOREIGN KEY([id_situacao])
REFERENCES [dbo].[tb_situacao] ([id_situacao]);

ALTER TABLE [dbo].[tb_dashboard]  WITH CHECK ADD  CONSTRAINT [tb_funcionalidade_tb_dashboard_fk] FOREIGN KEY([id_funcionalidade])
REFERENCES [dbo].[tb_funcionalidade] ([id_funcionalidade]);

SET IDENTITY_INSERT dbo.tb_dashboard ON;
INSERT INTO tb_dashboard (id_dashboard, st_dashboard, st_descricao, st_origem, st_tipochart, st_xtipo, st_xtitulo, st_ytitulo, id_situacao, bl_ativo) VALUES (1, 'Registros pendentes de enviar ao Moodle', 'Mostrará numero de alunos e colaboradores pendentes à serem enviados ao moodle', 'vw_dash_pendentes_moodle', 'column', 'category', 'Pessoas', 'Quantidade', 165, 1);
SET IDENTITY_INSERT dbo.tb_dashboard OFF;



CREATE TABLE [tb_campodashboard](
	[id_campodashboard] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[st_campodashboard] [varchar](250) NOT NULL,
	[st_titulocampo] [varchar](250) NOT NULL,
	[id_dashboard] [int] NOT NULL
 CONSTRAINT [PK_TB_CAMPODASHBOARD] PRIMARY KEY CLUSTERED 
(
	[id_campodashboard] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)

ALTER TABLE [dbo].[tb_campodashboard]  WITH CHECK ADD  CONSTRAINT [tb_situacao_tb_campodashboard_fk] FOREIGN KEY([id_dashboard])
REFERENCES [dbo].[tb_dashboard] ([id_dashboard]);


SET IDENTITY_INSERT dbo.tb_campodashboard ON;
INSERT INTO tb_campodashboard (id_campodashboard, st_campodashboard, st_titulocampo, id_dashboard) VALUES (1, 'nu_alunospendentes', 'Alunos Pendentes', 1);
INSERT INTO tb_campodashboard (id_campodashboard, st_campodashboard, st_titulocampo, id_dashboard) VALUES (2, 'nu_colaboradorespendentes', 'Colaboradores Pendentes', 1);
SET IDENTITY_INSERT dbo.tb_campodashboard OFF;





SET IDENTITY_INSERT dbo.tb_funcionalidade ON;
INSERT INTO tb_funcionalidade (id_funcionalidade, st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao, st_classeflex, id_tipofuncionalidade, bl_pesquisa, bl_lista, id_sistema, st_urlcaminho, bl_visivel, bl_relatorio, bl_delete) VALUES (681, 'Dashboards', NULL, 1, 123, '', 9, 0, 0, 1, '681', 1, 0, 1)
INSERT INTO tb_funcionalidade (id_funcionalidade, st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao, st_classeflex, id_tipofuncionalidade, bl_pesquisa, bl_lista, id_sistema, st_urlcaminho, bl_visivel, bl_relatorio, bl_delete) VALUES (682, 'Dash Pendentes Moodle', 681, 1, 123, '', 9, 0, 0, 1, '682', 1, 0, 1)
SET IDENTITY_INSERT dbo.tb_funcionalidade OFF;
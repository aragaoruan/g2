-- GII-7074

SET IDENTITY_INSERT tb_funcionalidade ON;

INSERT INTO tb_funcionalidade (id_funcionalidade, st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao, st_classeflex, st_urlicone, id_tipofuncionalidade, nu_ordem, st_classeflexbotao, bl_pesquisa, bl_lista, id_sistema, st_ajuda, st_target, st_urlcaminho, bl_visivel, bl_relatorio, st_urlcaminhoedicao, bl_delete)
VALUES (785, 'Salas Não Encerradas', 257, 1, 123, '', null, 3, 0, null, 0, 0, 1, null, null, '785', 1, 1, null, 0);

SET IDENTITY_INSERT tb_funcionalidade OFF;

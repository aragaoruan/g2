/* AC-27018 */


/* Inserção da nova funcionalidade nos menus. */

SET IDENTITY_INSERT tb_funcionalidade ON

INSERT INTO tb_funcionalidade 

(id_funcionalidade,st_funcionalidade,id_funcionalidadepai,bl_ativo,id_situacao,id_tipofuncionalidade,id_sistema,
st_urlcaminho,bl_visivel,st_classeflex,bl_pesquisa,bl_lista)
VALUES (703,'Relatórios',5,1,123,2,1,703,1,'',0,0);

INSERT INTO tb_funcionalidade 

(id_funcionalidade,st_funcionalidade,id_funcionalidadepai,bl_ativo,id_situacao,id_tipofuncionalidade,id_sistema,
st_urlcaminho,bl_visivel,st_classeflex,bl_pesquisa,bl_lista)
VALUES (704,'Gestão de Ocorrências',703,1,123,3,1,'/gestao-de-ocorrencias',1,'',0,0);


SET IDENTITY_INSERT tb_funcionalidade OFF


/* "Atualizar" o menu com as novas funcionalidades. */


begin transaction

insert into [tb_entidadefuncionalidade] (id_entidade, id_funcionalidade, bl_ativo,id_situacao)
(select 2,id_funcionalidade, 1,5 from tb_funcionalidade where id_funcionalidade not in (select id_funcionalidade from tb_entidadefuncionalidade where id_entidade = 2))

insert into [tb_perfilfuncionalidade] (id_perfil, id_funcionalidade, bl_ativo)
(select 3,id_funcionalidade, 3 from tb_funcionalidade where id_funcionalidade not in (select id_funcionalidade from tb_perfilfuncionalidade where id_perfil = 3)AND id_tipofuncionalidade !=9)


insert into [tb_perfilfuncionalidade] (id_perfil, id_funcionalidade, bl_ativo)
(select 1,id_funcionalidade, 3 from tb_funcionalidade where id_funcionalidade not in (select id_funcionalidade from tb_perfilfuncionalidade where id_perfil = 1))

insert into [tb_entidadefuncionalidade] (id_entidade, id_funcionalidade, bl_ativo,id_situacao)
(select 14,id_funcionalidade, 1,5 from tb_funcionalidade where id_funcionalidade not in (select id_funcionalidade from tb_entidadefuncionalidade where id_entidade = 14))

insert into [tb_perfilfuncionalidade] (id_perfil, id_funcionalidade, bl_ativo)
(select 3,id_funcionalidade, 3 from tb_funcionalidade where id_funcionalidade not in (select id_funcionalidade from tb_perfilfuncionalidade where id_perfil = 3))
insert into tb_permissaofuncionalidade (id_funcionalidade, id_permissao)
select fc.id_funcionalidade, pm.id_permissao from tb_funcionalidade fc, tb_permissao pm where fc.id_funcionalidade not in (select id_funcionalidade from tb_permissaofuncionalidade)
and fc.id_tipofuncionalidade not in (1,4,5,6) and pm.id_permissao in (1,2,3) AND fc.id_sistema = 1

insert into tb_perfilpermissaofuncionalidade (id_funcionalidade, id_perfil,id_permissao)
select pf.id_funcionalidade, 1 , pf.id_permissao from tb_permissaofuncionalidade pf where pf.id_funcionalidade not in (select id_funcionalidade from tb_perfilpermissaofuncionalidade)

commit


/* Criando a VwGestaoDeOcorrencias */

CREATE VIEW [dbo].[vw_gestaodeocorrencias]
AS

SELECT DISTINCT
  oc.id_ocorrencia,
  uso.st_cpf AS st_cpf,
  uso.st_nomecompleto AS st_nomeinteressado,
  tc1.st_email,
  uso.st_senha, uso.st_login,
  (CAST(tc3.nu_ddd AS VARCHAR) + '-' + CAST(tc3.nu_telefone AS VARCHAR)) AS st_telefone,
  oc.id_matricula,
  oc.id_evolucao,
  oc.id_situacao,
  oc.id_usuariointeressado,
  oc.id_categoriaocorrencia,
  oc.id_assuntoco,
  oc.id_saladeaula,
  oc.st_titulo,
  oc.dt_cadastro,
  oc.st_ocorrencia,
  oc.id_entidade,
  dbo.tb_situacao.st_situacao,
  dbo.tb_evolucao.st_evolucao,
  dbo.tb_assuntoco.st_assuntoco,
  dbo.tb_assuntoco.id_tipoocorrencia,
  dbo.tb_categoriaocorrencia.st_categoriaocorrencia,
  (CAST(CONVERT(VARCHAR, oc.dt_ultimotramite, 103) + ' às ' + CONVERT(VARCHAR, oc.dt_ultimotramite, 108) AS VARCHAR(MAX))) AS st_ultimotramite,
  (CAST(CONVERT(VARCHAR, oc.dt_cadastro, 103) + ' às ' + CONVERT(VARCHAR, oc.dt_cadastro, 108) AS VARCHAR(MAX))) AS st_cadastro,
  oc.st_tramite,
  oc.dt_ultimotramite AS dt_ultimotramite,
  oresp.id_usuario AS id_usuarioresponsavel,
  st_nomeresponsavel =
                      CASE
                        WHEN usr.id_usuario IS NULL THEN 'Não distribuído'
                        ELSE usr.st_nomecompleto
                      END,
  GETDATE() - 1 AS dt_atendimento,
  asp.st_assuntoco AS st_assuntocopai
  ,pp.st_projetopedagogico
  ,sa.st_saladeaula
  , mt.id_evolucao AS id_evolucaomatricula
  , oc.id_ocorrenciaoriginal
  , nc.id_nucleoco
FROM dbo.tb_ocorrencia AS oc
JOIN tb_usuario AS uso
  ON uso.id_usuario = oc.id_usuariointeressado
INNER JOIN dbo.tb_situacao
  ON dbo.tb_situacao.id_situacao = oc.id_situacao
INNER JOIN dbo.tb_evolucao
  ON dbo.tb_evolucao.id_evolucao = oc.id_evolucao
INNER JOIN dbo.tb_categoriaocorrencia
  ON dbo.tb_categoriaocorrencia.id_categoriaocorrencia = oc.id_categoriaocorrencia
INNER JOIN dbo.tb_assuntoco
  ON oc.id_assuntoco = dbo.tb_assuntoco.id_assuntoco
LEFT JOIN dbo.tb_assuntoco AS asp
  ON asp.id_assuntoco = dbo.tb_assuntoco.id_assuntocopai
LEFT JOIN dbo.tb_ocorrenciaresponsavel AS oresp
  ON oresp.id_ocorrencia = oc.id_ocorrencia
  AND oresp.bl_ativo = 1
LEFT JOIN dbo.tb_usuario AS usr
  ON usr.id_usuario = oresp.id_usuario
LEFT JOIN tb_contatosemailpessoaperfil tc
  ON uso.id_usuario = tc.id_usuario
  AND tc.bl_padrao = 1
  AND tc.bl_ativo = 1
  AND oc.id_entidade = tc.id_entidade
LEFT JOIN tb_contatosemail tc1
  ON tc.id_email = tc1.id_email
LEFT JOIN tb_contatostelefonepessoa tc2
  ON uso.id_usuario = tc2.id_usuario
  AND tc2.bl_padrao = 1
  AND oc.id_entidade = tc2.id_entidade
LEFT JOIN tb_contatostelefone tc3
  ON tc2.id_telefone = tc3.id_telefone
--alterações para impressão
LEFT JOIN dbo.tb_matricula AS mt
  ON mt.id_matricula = oc.id_matricula
LEFT JOIN dbo.tb_projetopedagogico AS pp
  ON pp.id_projetopedagogico = mt.id_projetopedagogico
LEFT JOIN dbo.tb_saladeaula AS sa
  ON sa.id_saladeaula = oc.id_saladeaula

LEFT JOIN dbo.tb_nucleoassuntoco AS na
  ON na.id_assuntoco = oc.id_assuntoco and na.bl_ativo = 1

LEFT JOIN dbo.tb_nucleoco AS nc
  ON nc.id_nucleoco = na.id_nucleoco and nc.id_entidade = oc.id_entidade



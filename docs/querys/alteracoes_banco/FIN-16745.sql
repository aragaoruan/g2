SET IDENTITY_INSERT dbo.tb_textosistema ON;
INSERT INTO tb_textosistema (id_textosistema,
id_usuario, id_entidade,
id_textoexibicao,
id_textocategoria, id_situacao,
st_textosistema,
st_texto, dt_inicio,
dt_fim, dt_cadastro,
id_orientacaotexto, bl_cabecalho)

VALUES (691, 10757, 14, 2, 5, 60, 'E-mail alteracao cadastro usuario', '<!DOCTYPE html><html><head></head><body><p>Prezado Aluno,</p><p>&nbsp;</p><p>Confirmamos a altera&ccedil;&atilde;o no seu cadastro de usu&aacute;rio no nosso sistema.</p><p>&nbsp;</p><p>&nbsp;</p></body></html>', '2015-04-29', '2050-10-20', '16:12:17.0000000', 1, NULL);
);
SET IDENTITY_INSERT dbo.tb_textosistema OFF;

SET IDENTITY_INSERT tb_mensagempadrao ON;
INSERT INTO tb_mensagempadrao (id_mensagempadrao, id_tipoenvio, st_mensagempadrao, st_default) VALUES (31, 3, 'Alteração cadastro usuário', 'Mensagem Aviso de Alteração no Cadastro de Usuário');
SET IDENTITY_INSERT tb_mensagempadrao OFF;
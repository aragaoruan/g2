CREATE VIEW rel.vw_alunosconcluintes AS

SELECT  mt.id_matricula ,
        us.st_nomecompleto ,
        ps.dt_nascimento ,
        DATEDIFF(YEAR, ps.dt_nascimento, GETDATE()) AS st_idade,
        mt.dt_concluinte,
        mt.id_projetopedagogico,
        mt.id_entidadeatendimento AS id_entidade,

        -- AC-27063 INICIO ALTERACOES
        us.st_cpf,
        (CONCAT(ps.st_rg, ' - ', ps.st_orgaoexpeditor)) AS st_rg,
        mt.st_projetopedagogico,

        CASE mt.bl_documentacao
        WHEN 1 THEN 'OK'
        ELSE 'Pendente'
        END AS st_documentacao,

        CASE mt.bl_academico
        WHEN 1 THEN 'OK'
        ELSE 'Pendente'
        END AS st_situacaoacademica,

        '' AS st_tcc,
        '' AS st_cargahoraria,
        '' AS st_disciplina,
        '' AS st_nota,
        '' AS st_certificadoapto,
        '' AS st_observacao
        -- AC-27063 FIM ALTERACOES

FROM    dbo.vw_matricula mt
        JOIN dbo.tb_usuario AS us ON us.id_usuario = mt.id_usuario
        JOIN dbo.vw_pessoa AS ps ON ps.id_usuario = us.id_usuario AND ps.id_entidade = mt.id_entidadeatendimento
WHERE   mt.id_evolucao = 15 AND mt.bl_ativo = 1 AND dt_concluinte IS NOT NULL AND mt.bl_institucional = 0

-- GII-9473
INSERT INTO
  tb_saladeaulaentidade (id_saladeaula, id_entidade)
  SELECT
    sa.id_saladeaula,
    en.id_entidade
  FROM
    tb_saladeaula AS sa,
    tb_entidade AS en
  WHERE
    en.id_esquemaconfiguracao = 1
    AND en.bl_ativo = 1
    AND en.id_situacao = 2
    AND sa.id_entidade = 352
    AND sa.bl_ativa = 1
    AND sa.id_situacao = 8
    AND sa.dt_abertura > CAST(GETDATE() AS DATE)
    AND sa.id_saladeaula NOT IN (
      SELECT
        id_saladeaula
      FROM
        tb_saladeaulaentidade
      WHERE
        id_saladeaula = sa.id_saladeaula
        AND id_entidade = en.id_entidade
    )
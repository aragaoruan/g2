-- AC-27137

SET IDENTITY_INSERT tb_textovariaveis ON;

INSERT INTO tb_textovariaveis (id_textovariaveis, id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
VALUES
(398, 3, 'dt_iniciotcc', '#data_inicio_TCC#', 'retornaDataPorExtenso', 1),
(399, 3, 'dt_terminotcc', '#data_termino_TCC#', 'retornaDataPorExtenso', 1),
(400, 3, 'nu_notatcc', '#nota_TCC#', NULL, 1),
(401, 3, 'dt_cadastrotcc', '#data_postagem_TCC#', 'retornaDataPorExtenso', 1),
(402, 3, 'dt_defesatcc', '#data_apresentacao_TCC#', 'retornaDataPorExtenso', 1);

SET IDENTITY_INSERT tb_textovariaveis OFF;
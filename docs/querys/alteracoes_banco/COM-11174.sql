BEGIN
	TRAN
SET IDENTITY_INSERT dbo.tb_funcionalidade ON INSERT INTO tb_funcionalidade (
	id_funcionalidade,
	st_funcionalidade,
	id_funcionalidadepai,
	bl_ativo,
	id_situacao,
	st_classeflex,
	st_urlicone,
	id_tipofuncionalidade,
	nu_ordem,
	st_classeflexbotao,
	bl_pesquisa,
	bl_lista,
	id_sistema,
	st_ajuda,
	st_target,
	st_urlcaminho,
	bl_visivel,
	bl_relatorio,
	st_urlcaminhoedicao
)
VALUES
	(
		660,
		'Relatório Individual Atendentes',
		195,
		1,
		123,
		'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa',
		'/img/ico/money--arrow.png',
		3,
		NULL,
		NULL,
		0,
		0,
		1,
		NULL,
		NULL,
		660,
		1,
		1,
		NULL
	);
GO
SET IDENTITY_INSERT dbo.tb_funcionalidade OFF COMMIT

--CRIAÇÃO DOS RELATÓRIOS DE ATENDENTES
begin tran
INSERT INTO tb_camporelatorio (id_relatorio, st_camporelatorio, st_titulocampo, bl_filtro, bl_exibido, nu_ordem, bl_obrigatorio)
 SELECT
		90,
		tbc.st_camporelatorio,
		tbc.st_titulocampo,
		tbc.bl_filtro,
		tbc.bl_exibido,
		tbc.nu_ordem,
		tbc.bl_obrigatorio
	FROM tb_camporelatorio AS tbc
	WHERE tbc.id_relatorio = 80

	commit
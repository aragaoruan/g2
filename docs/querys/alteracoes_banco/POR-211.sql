/***
*** Alterando a o valor default da situacao inicial do TCC para 203 (Pendente)
***/

BEGIN TRAN
ALTER TABLE tb_alocacao
  DROP CONSTRAINT DF__tb_alocac__id_si__04D4A3C0
ALTER TABLE tb_alocacao
  ADD CONSTRAINT DF__tb_alocac__id_situacaotcc DEFAULT 203 FOR id_situacaotcc
COMMIT
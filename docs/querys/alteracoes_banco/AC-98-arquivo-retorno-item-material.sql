/*****************************************************************************************************
				 Menu para a funcionalidade "ARQUIVO RETORNO DO ITEM DE MATERIAL"
******************************************************************************************************/

BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_funcionalidade on
INSERT INTO [dbo].[tb_funcionalidade]
([id_funcionalidade]
,[st_funcionalidade]
,[id_funcionalidadepai]
,[bl_ativo]
,[id_situacao]
,[st_classeflex]
,[st_urlicone]
,[id_tipofuncionalidade]
,[st_classeflexbotao]
,[bl_pesquisa]
,[bl_lista]
,[id_sistema]
,[st_ajuda]
,[st_urlcaminho]
,[bl_visivel]
,[bl_relatorio]
,[bl_delete]
)
VALUES

(671,'Arquivo Retorno', 98,1 ,123,'',null,3,null,0,0 , 1, null, '/arquivo-retorno-material', 1, 0, 1 )

GO
SET IDENTITY_INSERT dbo.tb_funcionalidade off

COMMIT



/*****************************************************************************************************
				TABELA ARQUIVO RETORNO MATERIAL
*****************************************************************************************************/
USE [G2H_EAD1]
GO

/****** Object:  Table [dbo].[tb_arquivoretornomaterial]    Script Date: 27/03/2015 15:12:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tb_arquivoretornomaterial](
	[id_arquivoretornomaterial] [int] IDENTITY(1,1) NOT NULL,
	[id_entidade] [int] NOT NULL,
	[st_arquivoretornomaterial] [varchar](500) NOT NULL,
	[dt_cadastro] [datetime] NOT NULL,
	[id_usuariocadastro] [int] NOT NULL,
	[id_situacao] [int] NOT NULL,
	[bl_ativo] [bit] NOT NULL,
	[id_upload] [int] NOT NULL
 CONSTRAINT [PK_tb_arquivoretornomaterial] PRIMARY KEY CLUSTERED 
(
	[id_arquivoretornomaterial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

ALTER TABLE [dbo].[tb_arquivoretornomaterial] ADD  CONSTRAINT [DF_tb_arquivoretornomaterial_dt_cadastro]  DEFAULT (getdate()) FOR [dt_cadastro]
GO

ALTER TABLE [dbo].[tb_arquivoretornomaterial]  WITH CHECK ADD  CONSTRAINT [FK_tb_arquivoretornomaterial_tb_pessoa] FOREIGN KEY([id_usuariocadastro])
REFERENCES [dbo].[tb_usuario] ([id_usuario])
GO

ALTER TABLE [dbo].[tb_arquivoretornomaterial] CHECK CONSTRAINT [FK_tb_arquivoretornomaterial_tb_pessoa]
GO

ALTER TABLE [dbo].[tb_arquivoretornomaterial]  WITH CHECK ADD  CONSTRAINT [FK_tb_arquivoretornomaterial_tb_situacao] FOREIGN KEY([id_situacao])
REFERENCES [dbo].[tb_situacao] ([id_situacao])
GO

ALTER TABLE [dbo].[tb_arquivoretornomaterial] CHECK CONSTRAINT [FK_tb_arquivoretornomaterial_tb_situacao]
GO

ALTER TABLE [dbo].[tb_arquivoretornomaterial]  WITH CHECK ADD  CONSTRAINT [FK_tb_arquivoretornomaterial_tb_upload] FOREIGN KEY([id_upload])
REFERENCES [dbo].[tb_upload] ([id_upload])
GO

ALTER TABLE [dbo].[tb_arquivoretornomaterial] CHECK CONSTRAINT [FK_tb_arquivoretornomaterial_tb_upload]
GO

/*****************************************************************************************************
							TABELA ARQUIVO RETORNO MATERIAL - PACOTES
*****************************************************************************************************/
USE [G2H_EAD1]
GO

/****** Object:  Table [dbo].[tb_arquivoretornopacote]    Script Date: 27/03/2015 16:04:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tb_arquivoretornopacote](
	[id_arquivoretornopacote] [int] IDENTITY(1,1) NOT NULL,
	[id_arquivoretornomaterial] [int] NOT NULL,
	[id_pacote] [int] NOT NULL,
	[id_itemmaterial] [int] NOT NULL,
	[st_codrastreamento] [varchar](60) NULL,
	[dt_postagem] [datetime] NULL,
	[dt_devolucao] [datetime] NULL,
	[st_resultado] [varchar](200) NULL,
	[id_situacaopostagem] [int] NOT NULL,
 CONSTRAINT [PK_tb_arquivoretornopacote] PRIMARY KEY CLUSTERED 
(
	[id_arquivoretornopacote] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

ALTER TABLE [dbo].[tb_arquivoretornopacote]  WITH CHECK ADD  CONSTRAINT [FK_tb_arquivoretornopacote_tb_arquivoretornomaterial1] FOREIGN KEY([id_arquivoretornomaterial])
REFERENCES [dbo].[tb_arquivoretornomaterial] ([id_arquivoretornomaterial])
GO

ALTER TABLE [dbo].[tb_arquivoretornopacote] CHECK CONSTRAINT [FK_tb_arquivoretornopacote_tb_arquivoretornomaterial1]
GO

ALTER TABLE [dbo].[tb_arquivoretornopacote]  WITH CHECK ADD  CONSTRAINT [FK_tb_arquivoretornopacote_tb_pacote1] FOREIGN KEY([id_pacote])
REFERENCES [dbo].[tb_pacote] ([id_pacote])
GO

ALTER TABLE [dbo].[tb_arquivoretornopacote] CHECK CONSTRAINT [FK_tb_arquivoretornopacote_tb_pacote1]
GO

ALTER TABLE [dbo].[tb_arquivoretornopacote]  WITH CHECK ADD  CONSTRAINT [FK_tb_arquivoretornopacote_tb_itemdematerial1] FOREIGN KEY([id_itemdematerial])
REFERENCES [dbo].[tb_itemdematerial] ([id_itemdematerial])
GO

ALTER TABLE [dbo].[tb_arquivoretornopacote] CHECK CONSTRAINT [FK_tb_arquivoretornopacote_tb_itemdematerial1]
GO

ALTER TABLE [dbo].[tb_arquivoretornopacote]  WITH CHECK ADD  CONSTRAINT [FK_tb_arquivoretornopacote_tb_situacao] FOREIGN KEY([id_situacaopostagem])
REFERENCES [dbo].[tb_situacao] ([id_situacao])
GO

ALTER TABLE [dbo].[tb_arquivoretornopacote] CHECK CONSTRAINT [FK_tb_arquivoretornopacote_tb_situacao]
GO


/*****************************************************************************************************
							INSERT DE SITUA��ES PARA  TABELA tb_arquivoretornomaterial
*****************************************************************************************************/

BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_situacao on
INSERT INTO tb_situacao 
(id_situacao, st_situacao, st_tabela, st_campo, st_descricaosituacao)
VALUES
(161, 'Ativo' , 'tb_arquivoretornomaterial' , 'id_situacao' , 'Arquivo de retorno de material ativo'),
(162, 'Inativo' , 'tb_arquivoretornomaterial' , 'id_situacao' , 'Arquivo de retorno de material inativo')
SET IDENTITY_INSERT dbo.tb_situacao on
COMMIT


/*****************************************************************************************************
							INSERT DE SITUA��ES PARA  TABELA tb_arquivoretornopacote
*****************************************************************************************************/

BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_situacao on
INSERT INTO tb_situacao 
(id_situacao, st_situacao, st_tabela, st_campo, st_descricaosituacao)
VALUES
(163, 'Enviado' , 'tb_arquivoretornopacote' , 'id_situacaopostagem' , 'Situa��o da postagem ENVIADO'),
(164, 'N�o Enviado' , 'tb_arquivoretornopacote' , 'id_situacaopostagem' , 'Situa��o da postagem N�O ENVIADO')
SET IDENTITY_INSERT dbo.tb_situacao on
COMMIT




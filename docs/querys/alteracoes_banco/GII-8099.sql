/*****************************************************************************************************************************
** GII-8099
** Variáveis
*****************************************************************************************************************************/

BEGIN TRANSACTION

SET IDENTITY_INSERT dbo.tb_textovariaveis ON

INSERT INTO tb_textovariaveis
(id_textovariaveis, id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel, bl_exibicao)
VALUES
  (468, 1, 'dt_agendamento', '#data_agendamento_aluno#', 'convertDataToBr', 1, 1), -- alter
  (469, 1, 'st_horarioaulaagendamento', '#horario_agendamento#', NULL, 1, 1), -- alter
  (470, 1, 'st_enderecoagendamento', '#endereco_nucleo_agendamento#', NULL, 1, 1),
  (471, 1, 'st_disciplinaagendamento', '#disciplina_agendamento#', NULL, 1, 1),
  (472, 1, 'st_municipioufagendamento', '#municipiouf_agendamento#', NULL, 1, 1)

SET IDENTITY_INSERT dbo.tb_textovariaveis OFF

COMMIT


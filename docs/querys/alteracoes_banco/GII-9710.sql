SET IDENTITY_INSERT tb_permissao ON;
INSERT INTO tb_permissao (id_permissao, st_nomepermissao, st_descricao, bl_ativo)
VALUES (
  58,
  'Editar Valores do Produto',
  'Permite a edição dos valores do produto na tela Financeiro > Produto.',
  1
);
SET IDENTITY_INSERT tb_permissao OFF;

INSERT INTO tb_permissaofuncionalidade (id_funcionalidade, id_permissao) VALUES (214, 58);

-- Teste: inserindo permissão para o perfil 3 - DDS TODAS ADMINISTRADOR.
-- INSERT INTO tb_perfilpermissaofuncionalidade VALUES (3, 214, 58);
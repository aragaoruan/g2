-- criação da funcionalidade
INSERT INTO dbo.tb_funcionalidade
        ( st_funcionalidade ,
          id_funcionalidadepai ,
          bl_ativo ,
          id_situacao ,
          st_classeflex ,
          st_urlicone ,
          id_tipofuncionalidade ,
          nu_ordem ,
          st_classeflexbotao ,
          bl_pesquisa ,
          bl_lista ,
          id_sistema ,
          st_ajuda ,
          st_target ,
          st_urlcaminho ,
          bl_visivel ,
          bl_relatorio ,
          st_urlcaminhoedicao
        )
VALUES  ( 'Enviar venda' , -- st_funcionalidade - varchar(100)
          195 , -- id_funcionalidadepai - int
          1 , -- bl_ativo - bit
          123 , -- id_situacao - int
          '' , -- st_classeflex - varchar(300)
          '' , -- st_urlicone - varchar(1000)
          3 , -- id_tipofuncionalidade - int
          0 , -- nu_ordem - int
          '' , -- st_classeflexbotao - varchar(300)
          0 , -- bl_pesquisa - bit
          0 , -- bl_lista - bit
          1 , -- id_sistema - int
          '' , -- st_ajuda - varchar(max)
          '' , -- st_target - varchar(100)
          'envio-venda' , -- st_urlcaminho - varchar(50)
          1 , -- bl_visivel - int
          0 , -- bl_relatorio - bit
          ''  -- st_urlcaminhoedicao - varchar(50)
        )
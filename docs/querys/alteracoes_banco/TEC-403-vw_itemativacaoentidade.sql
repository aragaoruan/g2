CREATE VIEW [dbo].[vw_itemativacaoentidade] AS (
 SELECT
 ia.id_itemativacao,
 ia.st_itemativacao,
 ia.id_departamento,
 iae.id_entidade,
 iae.id_itemativacaoentidade,
 iae.id_situacao,
 s.st_situacao,
 iae.id_usuarioatualizacao,
 iae.dt_atualizacao,
 CONVERT(VARCHAR(MAX), ia.st_descricao) AS st_descricao,
 ia.st_responsavel,
 ia.st_caminhosistema
 FROM
 tb_itemativacao AS ia
 LEFT JOIN
 tb_itemativacaoentidade AS iae
 LEFT JOIN tb_situacao AS s
 ON iae.id_situacao = s.id_situacao
 ON ia.id_itemativacao = iae.id_itemativacao

 UNION
 SELECT
 ia.id_itemativacao,
 ia.st_itemativacao,
 ia.id_departamento,
 e.id_entidade,
 NULL AS id_itemativacaoentidade,
 NULL AS id_situacao,
 NULL AS st_situacao,
 NULL AS id_usuarioatualizacao,
 NULL AS dt_atualizacao,
 CONVERT(VARCHAR(MAX), ia.st_descricao) AS st_descricao,
 ia.st_responsavel,
 ia.st_caminhosistema
 FROM
 tb_itemativacao AS ia
 JOIN tb_entidade AS e ON e.bl_ativo = 1

 WHERE ia.bl_ativo = 1
 AND id_itemativacao NOT IN (SELECT iae.id_itemativacao FROM tb_itemativacaoentidade AS iae WHERE iae.id_entidade = e.id_entidade)
);

SET IDENTITY_INSERT tb_funcionalidade ON;
INSERT INTO tb_funcionalidade (id_funcionalidade, st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao, id_tipofuncionalidade, bl_pesquisa, bl_lista, st_urlcaminho, bl_visivel, bl_relatorio, bl_delete)
VALUES(713, 'Preparação da Entidade', 301, 1, 123, 3, 0, 0, '/preparacao-entidade', 1, 0, 0);
SET IDENTITY_INSERT tb_funcionalidade OFF;
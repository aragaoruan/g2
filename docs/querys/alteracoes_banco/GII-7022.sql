alter table tb_horarioaula add id_tipoaula integer FOREIGN KEY REFERENCES tb_tipoaula(id_tipoaula);
alter table tb_tipoaula add bl_ativo bit not null DEFAULT (1);

ALTER VIEW [dbo].[vw_horarioaula] AS
SELECT DISTINCT
	h.id_horarioaula,
	h.id_codhorarioacesso,
	h.st_codhorarioacesso,
	h.st_horarioaula,
	h.hr_inicio,
	h.hr_fim,
	t.id_turno,
	st_turno,
	id_entidade,
	h.bl_ativo,
  ta.id_tipoaula,
  ta.st_tipoaula,
	(CASE WHEN segunda.id_diasemana IS NOT NULL THEN 1 ELSE 0 END) AS bl_segunda,
	(CASE WHEN terca.id_diasemana IS NOT NULL THEN 1 ELSE 0 END) AS bl_terca,
	(CASE WHEN quarta.id_diasemana IS NOT NULL THEN 1 ELSE 0 END) AS bl_quarta,
	(CASE WHEN quinta.id_diasemana IS NOT NULL THEN 1 ELSE 0 END) AS bl_quinta,
	(CASE WHEN sexta.id_diasemana IS NOT NULL THEN 1 ELSE 0 END) AS bl_sexta,
	(CASE WHEN sabado.id_diasemana IS NOT NULL THEN 1 ELSE 0 END) AS bl_sabado,
	(CASE WHEN domingo.id_diasemana IS NOT NULL THEN 1 ELSE 0 END) AS bl_domingo

FROM
	dbo.tb_horarioaula h
	INNER JOIN dbo.tb_turno t ON t.id_turno = h.id_turno

    OUTER APPLY ( SELECT DISTINCT hd1.id_diasemana FROM dbo.tb_horariodiasemana hd1
                   WHERE hd1.id_horarioaula = h.id_horarioaula AND hd1.id_diasemana = 1
                    ) AS segunda

	OUTER APPLY ( SELECT DISTINCT hd2.id_diasemana FROM dbo.tb_horariodiasemana hd2
                   WHERE hd2.id_horarioaula = h.id_horarioaula AND hd2.id_diasemana = 2
                    ) AS terca

	OUTER APPLY ( SELECT DISTINCT hd3.id_diasemana FROM dbo.tb_horariodiasemana hd3
                   WHERE hd3.id_horarioaula = h.id_horarioaula AND hd3.id_diasemana = 3
                    ) AS quarta

    OUTER APPLY ( SELECT DISTINCT hd4.id_diasemana FROM dbo.tb_horariodiasemana hd4
                   WHERE hd4.id_horarioaula = h.id_horarioaula AND hd4.id_diasemana = 4
                    ) AS quinta

    OUTER APPLY ( SELECT DISTINCT hd5.id_diasemana FROM dbo.tb_horariodiasemana hd5
                   WHERE hd5.id_horarioaula = h.id_horarioaula AND hd5.id_diasemana = 5
                    ) AS sexta

    OUTER APPLY ( SELECT DISTINCT hd6.id_diasemana FROM dbo.tb_horariodiasemana hd6
                   WHERE hd6.id_horarioaula = h.id_horarioaula AND hd6.id_diasemana = 6
                    ) AS sabado

    OUTER APPLY ( SELECT DISTINCT hd7.id_diasemana FROM dbo.tb_horariodiasemana hd7
                   WHERE hd7.id_horarioaula = h.id_horarioaula AND hd7.id_diasemana = 7
                    ) AS domingo

    LEFT JOIN tb_tipoaula as ta on h.id_tipoaula = ta.id_tipoaula
					WHERE h.bl_ativo = 1


update tb_tipoaula set (st_tipoaula) value ('Regular') where id_tipoaula = 1;
insert into tb_tipoaula (st_tipoaula) values ('Monitoria');
insert into tb_tipoaula (st_tipoaula) values ('Tutoria');


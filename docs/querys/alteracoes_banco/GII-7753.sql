-- GII-7753
SET IDENTITY_INSERT tb_evolucao ON;
INSERT INTO tb_evolucao (id_evolucao, st_evolucao, st_tabela, st_campo) VALUES
  (81, 'Formado', 'tb_matricula', 'id_evolucao'),
  (82, 'Diplomado', 'tb_matricula', 'id_evolucao');
SET IDENTITY_INSERT tb_evolucao OFF;
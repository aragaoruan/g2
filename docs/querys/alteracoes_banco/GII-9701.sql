SET IDENTITY_INSERT tb_permissao ON

INSERT INTO tb_permissao (id_permissao, st_nomepermissao, st_descricao, bl_ativo)
VALUES
(59,
'Exibir botão de histórico',
'Permite que o botão histórico seja exibido na tela.',
1);

SET IDENTITY_INSERT tb_permissao OFF;

INSERT INTO tb_permissaofuncionalidade (id_funcionalidade, id_permissao)
VALUES (275, 59);


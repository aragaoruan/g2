SET IDENTITY_INSERT tb_funcionalidade ON;
INSERT INTO [tb_funcionalidade]
(id_funcionalidade, st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao, st_classeflex, st_urlicone,
 id_tipofuncionalidade, nu_ordem, st_classeflexbotao, bl_pesquisa, bl_lista, id_sistema, st_ajuda, st_target, st_urlcaminho,
 bl_visivel, bl_relatorio, st_urlcaminhoedicao, bl_delete
)
VALUES (808, 'Promoções', NULL, 1, 123, '/promocoes', NULL, 1, NULL, 'ajax', 1, 1, 4, NULL, NULL, 808, 1, 0, NULL, 0);
SET IDENTITY_INSERT tb_funcionalidade OFF;
INSERT INTO tb_perfilfuncionalidade (id_perfil, id_funcionalidade, bl_ativo)
  (SELECT
     id_perfil,
     808,
     1
   FROM tb_perfil
   WHERE id_perfilpedagogico = 5 AND id_perfil NOT IN
                                     (SELECT
                                        id_perfil
                                      FROM tb_perfilfuncionalidade
                                      WHERE id_funcionalidade = 808))


INSERT INTO [tb_entidadefuncionalidade] (id_entidade, id_funcionalidade, bl_ativo, id_situacao)
  (SELECT
     id_entidade,
     808,
     1,
     5
   FROM tb_entidade
   WHERE id_entidade NOT IN
         (SELECT
            id_entidade
          FROM tb_entidadefuncionalidade
          WHERE id_funcionalidade = 808))

CREATE TABLE tb_visualizacampanha (
  id_visualizacampanha INT IDENTITY (1, 1) NOT NULL,
  id_campanhacomercial INT NULL,
  id_usuario INT NOT NULL,
  dt_cadastro DATETIME2 NOT NULL DEFAULT (getdate()),
)
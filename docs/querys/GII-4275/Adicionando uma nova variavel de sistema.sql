SET IDENTITY_INSERT tb_textovariaveis ON;

INSERT INTO tb_textovariaveis
(id_textovariaveis, id_textocategoria, st_camposubstituir, st_textovariaveis, st_mascara, id_tipotextovariavel)
VALUES
  (438, 6, 'id_acordo', '#link_cartao_acordo#', 'gerarLinkCartaoVendaAcordo', 5);

SET IDENTITY_INSERT tb_textovariaveis OFF;
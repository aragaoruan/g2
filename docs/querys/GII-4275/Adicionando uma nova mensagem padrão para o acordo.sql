/**
ADICIONANDO mensagem padr�o no banco para envio de link para pagamento via Cart�o de Cr�dito - ACORDO
**/

BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_mensagempadrao ON
INSERT INTO tb_mensagempadrao
(id_mensagempadrao,id_tipoenvio, st_mensagempadrao, st_default, id_textocategoria)
VALUES (40, 3, 'Envio de link para pagamento via Cartão de Crédito - ACORDO', NULL, NULL)
SET IDENTITY_INSERT dbo.tb_mensagempadrao OFF

COMMIT
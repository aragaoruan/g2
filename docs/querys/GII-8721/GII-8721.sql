CREATE TABLE tb_censo
(
  id_matricula INT PRIMARY KEY NOT NULL,
  tp_registrocabecalho INT DEFAULT 40,
  id_iesinep INT DEFAULT 000000008376,
  tp_arquivocabecalho INT DEFAULT 4,
  tp_registroaluno INT DEFAULT 41,
  id_alunoinep INT DEFAULT NULL,
  st_nome VARCHAR(120),
  st_cpf VARCHAR(11),
  st_docuestrangeiropassaporte VARCHAR(20) DEFAULT NULL,
  dt_nascimento VARCHAR(10),
  nu_sexo TINYINT,
  nu_corraca TINYINT,
  st_nomecompletomae VARCHAR(120),
  nu_nacionalidade TINYINT,
  nu_ufnascimento INT,
  nu_municipionascimento INT,
  st_paisorigem VARCHAR(3),
  nu_alunodeftranstsuperdotacao TINYINT,
  nu_tipodeficienciacegueira TINYINT DEFAULT NULL,
  nu_tipodeficienciabaixavisao TINYINT DEFAULT NULL,
  nu_tipodeficienciasurdez TINYINT DEFAULT NULL,
  nu_tipodeficienciaauditiva TINYINT DEFAULT NULL,
  nu_tipodeficienciafisica TINYINT DEFAULT NULL,
  nu_tipodeficienciasurdocegueira TINYINT DEFAULT NULL,
  nu_tipodeficienciamultipla TINYINT DEFAULT NULL,
  nu_tipodeficienciaintelectcual TINYINT DEFAULT NULL,
  nu_tipodeficienciaautismo TINYINT DEFAULT NULL,
  nu_tipodeficienciaasperger TINYINT DEFAULT NULL,
  nu_tipodeficienciarett TINYINT DEFAULT NULL,
  nu_tipodeficienciatranstdesinfancia TINYINT,
  nu_tipodeficienciasuperdotacao TINYINT,
  tp_registroalunocurso INT DEFAULT 42,
  nu_semestrereferencia INT DEFAULT NULL,
  nu_codigocurso INT,
  nu_codigopolocursodistancia INT DEFAULT NULL,
  st_idnaies VARCHAR(20),
  nu_turnoaluno TINYINT,
  nu_situacaovinculoalunocurso TINYINT,
  nu_cursoorigem INT,
  nu_semestreconclusao TINYINT DEFAULT NULL,
  nu_alunoparfor TINYINT DEFAULT NULL,
  nu_semestreingcurso VARCHAR(6),
  nu_tipoescolaconclusaoensinomedio TINYINT,
  nu_formaingselecaovestibular TINYINT,
  nu_formaingselecaoenem TINYINT,
  nu_formaingselecaoavaliacaoseriada TINYINT,
  nu_formaingselecaoselecaosimplificada TINYINT,
  nu_formaingselecaoegressobili TINYINT,
  nu_formaingselecaoegressopecg TINYINT,
  nu_formaingselecaotrasnfexofficio TINYINT,
  nu_formaingselecaodecisaojudicial TINYINT,
  nu_formaingselecaovagasremanescentes TINYINT,
  nu_formaingselecaovagasprogespeciais TINYINT,
  nu_mobilidadeacademica TINYINT,
  nu_tipomodalidadeacademica TINYINT DEFAULT NULL,
  st_iesdestino INT DEFAULT NULL,
  nu_tipomodalidadeacademicainternacional TINYINT DEFAULT NULL,
  st_paisdestino VARCHAR(3) DEFAULT NULL,
  nu_programareservavagas TINYINT,
  nu_programareservavagasetnico TINYINT DEFAULT NULL,
  nu_programareservavagaspessoadeficiencia TINYINT DEFAULT NULL,
  nu_programareservavagasestudanteescolapublica TINYINT DEFAULT NULL,
  nu_programareservavagassocialrendafamiliar TINYINT DEFAULT NULL,
  nu_programareservavagasoutros TINYINT DEFAULT NULL,
  nu_finestud TINYINT DEFAULT NULL,
  nu_finestudreembfies TINYINT DEFAULT NULL,
  nu_finestudreembgovest TINYINT DEFAULT NULL,
  nu_finestudreembgovmun TINYINT DEFAULT NULL,
  nu_finestudreembies TINYINT DEFAULT NULL,
  nu_finestudreembentidadesexternas TINYINT DEFAULT NULL,
  nu_finestudreembprouniintegral TINYINT DEFAULT NULL,
  nu_finestudreembprouniparcial TINYINT DEFAULT NULL,
  nu_finestudreembentidadesexternas_2 TINYINT DEFAULT NULL,
  nu_finestudreembgovest_2 TINYINT DEFAULT NULL,
  nu_finestudreembies_2 TINYINT DEFAULT NULL,
  nu_finestudreembgovmun_2 TINYINT DEFAULT NULL,
  nu_apoiosocial TINYINT,
  nu_tipoapoiosocialalimentacao TINYINT DEFAULT NULL,
  nu_tipoapoiosocialmoradia TINYINT DEFAULT NULL,
  nu_tipoapoiosocialtransporte TINYINT DEFAULT NULL,
  nu_tipoapoiosocialmaterialdidatico TINYINT DEFAULT NULL,
  nu_tipoapoiosocialbolsatrabalho TINYINT DEFAULT NULL,
  nu_tipoapoiosocialbolsapermanencia TINYINT DEFAULT NULL,
  nu_atvextcurr TINYINT,
  nu_atvextcurrpesquisa TINYINT DEFAULT NULL,
  nu_bolsaremuneracaoatvextcurrpesquisa TINYINT DEFAULT NULL,
  nu_atvextcurrextensao TINYINT DEFAULT NULL,
  nu_bolsaremuneracaoatvextcurrextensao TINYINT DEFAULT NULL,
  nu_atvextcurrmonitoria TINYINT DEFAULT NULL,
  nu_bolsaremuneracaoatvextcurrmonitoria TINYINT DEFAULT NULL,
  nu_atvextcurrestnaoobg TINYINT DEFAULT NULL,
  nu_bolsaremuneracaoatvextcurrestnaoobg TINYINT DEFAULT NULL,
  nu_cargahorariototalporaluno INT,
  nu_cargahorariointegralizadapeloaluno INT,
  nu_anocenso INT,
  nu_anomatricula INT
);

-- inserir dados na tabela do CENSO
INSERT INTO tb_censo
  SELECT
    DISTINCT
    TOP 5000
-- id matricula X
    matricula.id_matricula AS "Matricula",
-- cabeçalho
-- "Tipo de registro" X
    40,
--fixo
-- "ID da IES no INEP" X
    000000003876,
--fixo]
-- "Tipo de arquivo" X
    4,
--fixo
-- REGISTRO DO ALUNO - REGISTRO 41
-- "Tipo de registro" X
    41,
--fixo
-- TODO verificar ID do aluno no INEP
-- "ID do aluno no INEP" X
    0,
--opcional
-- "Nome" X
    pessoa.st_nomeexibicao,
--obrigatório
-- "CPF" X
    usuario.st_cpf,
--obrigatório
-- "Documento de estrangeiro ou passaporte" X
    0,
--condicional(aluno estrangeiro)
-- "Data de Nascimento" X
    CASE
    WHEN ((DAY(pessoa.dt_nascimento) BETWEEN 1 AND 9) AND (MONTH(pessoa.dt_nascimento) BETWEEN 1 AND 9))
      THEN (CONCAT('0', DAY(pessoa.dt_nascimento), '0', MONTH(pessoa.dt_nascimento), YEAR(pessoa.dt_nascimento)))
    WHEN (DAY(pessoa.dt_nascimento) BETWEEN 1 AND 9)
      THEN (CONCAT('0', DAY(pessoa.dt_nascimento), MONTH(pessoa.dt_nascimento), YEAR(pessoa.dt_nascimento)))
    WHEN (MONTH(pessoa.dt_nascimento) BETWEEN 1 AND 9)
      THEN (CONCAT(DAY(pessoa.dt_nascimento), '0', MONTH(pessoa.dt_nascimento), YEAR(pessoa.dt_nascimento)))
    ELSE (CONCAT(DAY(pessoa.dt_nascimento), MONTH(pessoa.dt_nascimento), YEAR(pessoa.dt_nascimento)))
    END,
--obrigatório
-- "Sexo" X
    CASE pessoa.st_sexo
    WHEN 'M'
      THEN 0
    WHEN 'F'
      THEN 1
    END,
-- "Cor/Raça" X
    CASE -- conversão para tabela do INEP
    WHEN pessoadadoscomplementare.id_etnia = 3
      THEN 4 -- amarela
    WHEN pessoadadoscomplementare.id_etnia = 4
      THEN 3 -- pardo
    WHEN pessoadadoscomplementare.id_etnia = 6
      THEN 0 -- não informado
    WHEN pessoadadoscomplementare.id_etnia IS NULL
      THEN 0 -- não informado
    ELSE pessoadadoscomplementare.id_etnia
    END,
-- "Nome completo da mãe" X
    pessoa.st_nomemae,
--obrigatório
-- "Nacionalidade" X
    CASE pais.st_nomepais
    WHEN 'Brasil'
      THEN 1 -- 1 - Brasileira
    ELSE 3 -- 3 - Estrangeira
    END,
--obrigatório(1 - Brasileira, 2 - Brasileira - Nascido no exterior ou naturalizado, 3 - Estrangeira)
-- "UF de nascimento" X
    0,
--opcional(só pode informar caso brasileiro)
-- "Município de nascimento" X
    0,
-- TODO pegar das tabelas do INEP caso vá preencher --condicional(Este campo é obrigatório se informada a UF de nascimento. - O código do município deverá ser validado na tabela corporativa TC_MUNICIPIO.)
-- não temos alunos estrangeiros
-- "País de origem" X
    'BRA',
--obrigatório(BRA caso brasileiro, seguir tabela ISO-3166-1)
-- Foi definido que neste primeiro momento nã trataremos alunos com deficiência
-- "Aluno com deficiência, transtorno global do desenvolvimento ou altas habilidades/superdotação" X
    0,
--obrigatório
-- "Tipo de deficiência - Cegueira" X
    0,
--condicional(obrigatório se informada a opção "Sim" no campo "Aluno com deficiência")
-- "Tipo de deficiência - Baixa visão" X
    0,
--condicional(obrigatório se informada a opção "Sim" no campo "Aluno com deficiência")
-- "Tipo de deficiência - Surdez" X
    0,
--condicional(obrigatório se informada a opção "Sim" no campo "Aluno com deficiência")
-- "Tipo de deficiência - auditiva" X
    0,
--condicional(obrigatório se informada a opção "Sim" no campo "Aluno com deficiência")
-- "Tipo de deficiência - física" X
    0,
--condicional(obrigatório se informada a opção "Sim" no campo "Aluno com deficiência")
-- "Tipo de deficiência - Surdocegueira" X
    0,
--condicional(obrigatório se informada a opção "Sim" no campo "Aluno com deficiência")
-- "Tipo de deficiência - Múltipla" X
    0,
--condicional(obrigatório se informada a opção "Sim" no campo "Aluno com deficiência")
-- "Tipo de deficiência - Intelectual" X
    0,
--condicional(obrigatório se informada a opção "Sim" no campo "Aluno com deficiência")
-- "Tipo de deficiência - Autismo(Transtorno global do desenvolvimento)" X
    0,
--condicional(obrigatório se informada a opção "Sim" no campo "Aluno com deficiência")
-- "Tipo de deficiência - Síndrome de Asperger(Transtorno global do desenvolvimento)" X
    0,
--condicional(obrigatório se informada a opção "Sim" no campo "Aluno com deficiência")
-- "Tipo de deficiência - Síndrome de RETT(Transtorno global do desenvolvimento)" X
    0,
--condicional(obrigatório se informada a opção "Sim" no campo "Aluno com deficiência")
-- "Tipo de deficiência - Transtorno desintegrativo da infância(Transtorno global do desenvolvimento)" X
    0,
--condicional(obrigatório se informada a opção "Sim" no campo "Aluno com deficiência")
-- "Tipo de deficiência - Altas habilidades/superdotação" X
    0,
--condicional(obrigatório se informada a opção "Sim" no campo "Aluno com deficiência")
--REGISTRO DO VÍNCULO ALUNO COM CURSO - REGISTRO 42
-- "Tipo de registro" X
    42,
--fixo
-- "Semestre de referência" X
    NULL,
--condicional(obrigatório para Federais)
-- "Código do curso" X
    matricula.id_projetopedagogico,
-- "Código do pólo do curso a distância" X
    matricula.id_entidadematricula,
--condicional(Este campo é obrigatório e somente poderá ser informado se o curso for de modalidade "Curso a distância")
-- "ID na IES - Identificação única do aluno na IES" X
    pessoa.id_usuario,
--opcional(código do aluno na IES)
-- "Turno do aluno" X
    0,
--condicional(somente para curso presencial)
--obrigatório(Valores válidos:
-- 2 – Cursando(6),
-- 3 – Matrícula trancada(21),
-- 4 – Desvinculado do curso,
-- 5 - Transferido para outro curso da mesma IES(20),
-- 6 – Formado(81 ou 15),
-- 7 – Falecido, - A situação “6 - Formado" NÃO poderá ser informada se o curso for do tipo Área Básica do curso )
-- "Situação de vínculo do aluno ao curso" X
    CASE matricula.id_evolucao
    WHEN 6
      THEN 2
    WHEN 21
      THEN 3
    WHEN 20
      THEN 5
    WHEN 81
      THEN 6
    WHEN 15
      THEN 6
    END,
-- TODO verificar se é o nosso ou o deles -- fazer JOIN matricula, projeto pedagógico(caso o nosso)
-- "Curso de origem" X
    0,
--condicional(A situação de vínculo do aluno no curso de  onde  foi transferido (curso de origem do aluno) deverá ser “5 – Transferido para outro curso da mesma IES”.)
-- "Semestre de conclusao" X
    CASE
    WHEN (MONTH(matricula.dt_termino) BETWEEN 1 AND 6)
      THEN 1
    WHEN (MONTH(matricula.dt_termino) BETWEEN 6 AND 12)
      THEN 2
    END,
--obrigatório(Deverá ser obrigatoriamente preenchido no seguinte formato "SSAAAA".   Exemplo:012014 )
-- "Aluno PARFOR"
    NULL,
-- "Semestre de ingresso no curso" X
    CASE
    WHEN (MONTH(matricula.dt_inicio) BETWEEN 1 AND 9)
      THEN (CONCAT('0', MONTH(matricula.dt_inicio), YEAR(matricula.dt_inicio)))
    WHEN (MONTH(matricula.dt_inicio) BETWEEN 10 AND 12)
      THEN (CONCAT(MONTH(matricula.dt_inicio), YEAR(matricula.dt_inicio)))
    END,
--obrigatório(Deverá ser obrigatoriamente preenchido no seguinte formato "SSAAAA".   Exemplo:012014 )
-- nosso: 1 - público, 2 - privado
-- "Tipo de escola que concluiu o Ensino Médio" X
    CASE
    WHEN (pessoadadoscomplementare.id_tipoescola = 2)
      THEN 0
    WHEN (pessoadadoscomplementare.id_tipoescola IS NULL)
      THEN 2
    ELSE pessoadadoscomplementare.id_tipoescola
    END,
--obrigatório(0 - Privado, 1 - Público, 2 - Não dispõe da informação)
-- TODO parse tb_vendaproduto.id_tiposelecao(CASE)
-- "Forma de ingresso/seleção - Vestibular" X
    CASE
    WHEN (vendaproduto.id_tiposelecao = 1)
      THEN 1
    WHEN (vendaproduto.id_tiposelecao = 3)
      THEN 1
    ELSE 0
    END,
--obrigatório(0 não, 1 sim)
-- "Forma de ingresso/seleção - Enem" X
    CASE
    WHEN (vendaproduto.id_tiposelecao = 3)
      THEN 1
    ELSE 0
    END,
--obrigatório(0 não, 1 sim)
-- "Forma de ingresso/seleção - Avaliação Seriada" X
    0,
--obrigatório(0 não, 1 sim)
-- "Forma de ingresso/seleção - Seleção Simplificada" X
    CASE
    WHEN (vendaproduto.id_tiposelecao = 7)
      THEN 1
    WHEN (vendaproduto.id_tiposelecao = 1)
      THEN 1
    ELSE 0
    END,
--obrigatório(0 não, 1 sim)
-- "Forma de ingresso/seleção - Egresso BI/LI" X
    0,
--obrigatório(0 não, 1 sim)
-- "Forma de ingresso/seleção - PEC-G"
    0,
--condicional(- Este campo somente poderá ser informado se a nacionalidade do aluno for igual a "estrangeira". )
-- "Forma de ingresso/seleção - Transferência Ex Officio" X
    0,
--obrigatório(- Este campo somente poderá ser informado se a nacionalidade do aluno for igual a "estrangeira". )
-- "Forma de ingresso/seleção - Decisão judicial" X
    0,
--obrigatório(- Este campo somente poderá ser informado se a nacionalidade do aluno for igual a "estrangeira". )
-- "Forma de ingresso/seleção - Seleção para Vagas Remanescentes" X
    CASE
    WHEN (vendaproduto.id_tiposelecao = 2)
      THEN 1
    ELSE 0
    END,
--obrigatório(- Este campo somente poderá ser informado se a nacionalidade do aluno for igual a "estrangeira". )
-- "Forma de ingresso/seleção - Seleção para Vagas de Programas Especiais" X
    0,
--obrigatório(- Este campo somente poderá ser informado se a nacionalidade do aluno for igual a "estrangeira". )
-- "Mobilidade acadêmica" X
    NULL,
--condicional(Obrigatório para Federais)
-- "Tipo de mobilidade acadêmica" X
    NULL,
--condicional(Obrigatório para Federais)
-- "IES destino" X
    0,
--condicional(Campo de preenchimento obrigatório quando informada a opção “1 - Nacional” no campo Tipo de mobilidade acadêmica.)
-- "Tipo de mobilidade acadêmica internacional" X
    0,
--condicional(Campo de preenchimento obrigatório se informada a opção “1 – SIM” no campo Mobilidade Acadêmica.)
-- "País destino" X
    NULL,
--condicional(Campo de preenchimento obrigatório quando informada qualquer opção no campo Tipo de mobilidade acadêmica internacional.)
-- "Programa de reserva de vagas" X
    0,
--obrigatório(- Quando informado "Sim" neste campo, é obrigatório que pelo menos um tipo de programa de reserva de vagas/ações afirmativas seja informado "1-Sim".)
-- "Programa de reserva de vagas/açoes afirmativas - Etnico" X
    0,
--condicional(Este campo é obrigatório e somente poderá ser informado se informada a opção “1 – Sim” no campo Programa de reserva de vagas/ações afirmativas.)
-- "Programa de reserva de vagas/açoes afirmativas - Pessoa com deficiência" X
    0,
--condicional(Este campo é obrigatório e somente poderá ser informado se informada a opção “1 – Sim” no campo Programa de reserva de vagas/ações afirmativas.)
-- "Programa de reserva de vagas/açoes afirmativas - Estudante procedente de escola pública" X
    0,
--condicional(Este campo é obrigatório e somente poderá ser informado se informada a opção “1 – Sim” no campo Programa de reserva de vagas/ações afirmativas.)
-- "Programa de reserva de vagas/açoes afirmativas - Social/renda familiar" X
    0,
--condicional(Este campo é obrigatório e somente poderá ser informado se informada a opção “1 – Sim” no campo Programa de reserva de vagas/ações afirmativas.)
-- "Programa de reserva de vagas/açoes afirmativas - Outros" X
    0,
--condicional(Este campo é obrigatório e somente poderá ser informado se informada a opção “1 – Sim” no campo Programa de reserva de vagas/ações afirmativas.)
-- "Financiamento estudantil" X
    CASE
    WHEN (vendaproduto.id_tiposelecao = 6)
      THEN 1
    ELSE 0
    END,
--condicional(não informar caso Federal)
-- "Financiamento Estudantil Reembolsável - FIES" X
    0,
--condicional(Este campo é obrigatório e somente poderá ser informado se informada a opção “1 – Sim” no campo Financiamento estudantil.)
-- "Financiamento Estudantil Reembolsável - governo Estadual" X
    0,
--condicional(Este campo é obrigatório e somente poderá ser informado se informada a opção “1 – Sim” no campo Financiamento estudantil.)
-- "Financiamento Estudantil Reembolsável - governo mun" X
    0,
--condicional(Este campo é obrigatório e somente poderá ser informado se informada a opção “1 – Sim” no campo Financiamento estudantil.)
-- "Financiamento Estudantil Reembolsável - IES" X
    0,
--condicional(Este campo é obrigatório e somente poderá ser informado se informada a opção “1 – Sim” no campo Financiamento estudantil.)
-- "Financiamento Estudantil Reembolsável - Entidades externas" X
    0,
--condicional(Este campo é obrigatório e somente poderá ser informado se informada a opção “1 – Sim” no campo Financiamento estudantil.)
-- "Financiamento Estudantil Reembolsável - ProUni integral" X
    CASE
    WHEN (vendaproduto.id_tiposelecao = 6)
      THEN 1
    ELSE 0
    END,
--condicional(Este campo é obrigatório e somente poderá ser informado se informada a opção “1 – Sim” no campo Financiamento estudantil.)
-- "Financiamento Estudantil Reembolsável - ProUni parcial" X
    0,
--condicional(Este campo é obrigatório e somente poderá ser informado se informada a opção “1 – Sim” no campo Financiamento estudantil.)
-- "Financiamento Estudantil Reembolsável - Entidades externas" X
    0,
--condicional(Este campo é obrigatório e somente poderá ser informado se informada a opção “1 – Sim” no campo Financiamento estudantil.)
-- "Financiamento Estudantil Reembolsável - governo estadual" X
    0,
--condicional(Este campo é obrigatório e somente poderá ser informado se informada a opção “1 – Sim” no campo Financiamento estudantil.)
-- "Financiamento Estudantil Reembolsável - IES" X
    0,
--condicional(Este campo é obrigatório e somente poderá ser informado se informada a opção “1 – Sim” no campo Financiamento estudantil.)
-- "Financiamento Estudantil Reembolsável - governo mun" X
    0,
--condicional(Este campo é obrigatório e somente poderá ser informado se informada a opção “1 – Sim” no campo Financiamento estudantil.)
-- "Apoio Social" X
    0,
--obrigatório(Quando informado "Sim" neste campo, é obrigatório que pelo menos um tipo de apoio social seja informado.)
-- "Tipo de apoio social - Alimentação" X
    0,
--condicional(Este campo é obrigatório e somente poderá ser informado se informada a opção “1 – Sim” no campo Apoio social.)
-- "Tipo de apoio social - Moradia" X
    0,
--condicional(Este campo é obrigatório e somente poderá ser informado se informada a opção “1 – Sim” no campo Apoio social.)
-- "Tipo de apoio social - Transporte" X
    0,
--condicional(Este campo é obrigatório e somente poderá ser informado se informada a opção “1 – Sim” no campo Apoio social.)
-- "Tipo de apoio social - Material didático" X
    0,
--condicional(Este campo é obrigatório e somente poderá ser informado se informada a opção “1 – Sim” no campo Apoio social.)
-- "Tipo de apoio social - Bolsa trabalho" X
    0,
--condicional(Este campo é obrigatório e somente poderá ser informado se informada a opção “1 – Sim” no campo Apoio social.)
-- "Tipo de apoio social - Bolsa permanência" X
    0,
--condicional(Este campo é obrigatório e somente poderá ser informado se informada a opção “1 – Sim” no campo Apoio social.)
-- TODO verificar com Toffolo
-- "Atividade extracurricular" X
    0,
--obrigatório(- Quando informado "Sim" neste campo, é obrigatório que pelo menos um tipo de atividade extracurricular seja informado.)
-- "Atividade extracurricular - Pesquisa" X
    0,
--condicional(Este campo é obrigatório e somente poderá ser informado se informada a opção “1 – Sim” no campo Atividade extracurricular.)
-- "Bolsa/remuneração referente à atividade extracurricular - Pesquisa" X
    0,
--condicional(Este campo é obrigatório e somente poderá ser informado se informada a opção “1 – Sim” no campo Atividade extracurricular.)
--"Atividade extracurricular - Extensão"X
    0,
--condicional(Este campo é obrigatório e somente poderá ser informado se informada a opção “1 – Sim” no campo Atividade extracurricular.)
-- "Bolsa/remuneração referente à atividade extracurricular - Extensão" X
    0,
--condicional(Este campo é obrigatório e somente poderá ser informado se informada a opção “1 – Sim” no campo Atividade extracurricular.)
-- "Atividade extracurricular - Monitoria" X
    0,
--condicional(Este campo é obrigatório e somente poderá ser informado se informada a opção “1 – Sim” no campo Atividade extracurricular.)
-- "Bolsa/remuneração referente à atividade extracurricular - Monitoria" X
    0,
--condicional(Este campo é obrigatório e somente poderá ser informado se informada a opção “1 – Sim” no campo Atividade extracurricular.)
-- "Atividade extracurricular - Estágio não obrigatório" X
    0,
--condicional(Este campo é obrigatório e somente poderá ser informado se informada a opção “1 – Sim” no campo Atividade extracurricular.)
-- "Bolsa/remuneração referente à atividade extracurricular - Estágio não obrigatório" X
    0,
--condicional(Este campo é obrigatório e somente poderá ser informado se informada a opção “1 – Sim” no campo Atividade extracurricular.)
-- "Carga horária total do curso por aluno" X
    projetopedagogico.nu_cargahoraria,
--obrigatório(numérico)
-- TODO verificar(Coeficiente de rendimento acumulado)
-- "Carga horária integralizada pelo aluno" X
    CASE
    WHEN (matricula.id_evolucao = 12)
      THEN projetopedagogico.nu_cargahoraria
    ELSE (SELECT
            sum(nu_cargahoraria)
          FROM
            tb_matriculadisciplina AS matriculadisciplina
            JOIN tb_disciplina AS disciplina ON matriculadisciplina.id_disciplina = disciplina.id_disciplina
          WHERE id_matricula = matricula.id_matricula AND id_evolucao = 12)
    END,
--obrigatório(numérico)
-- "ano"
    2017,
-- "ano de matrícula"
    YEAR(matricula.dt_inicio)
  FROM
    tb_matricula AS matricula
    JOIN tb_matriculadisciplina AS matriculadisciplina ON matricula.id_matricula = matriculadisciplina.id_matricula
    JOIN tb_usuario AS usuario ON matricula.id_usuario = usuario.id_usuario
    JOIN tb_pessoa AS pessoa
      ON pessoa.id_usuario = matricula.id_usuario AND
         pessoa.id_entidade = matricula.id_entidadematricula
    JOIN tb_pais AS pais ON pessoa.id_pais = pais.id_pais
    JOIN tb_municipio AS municipio ON pessoa.id_municipio = municipio.id_municipio
    JOIN tb_projetopedagogico AS projetopedagogico
      ON matricula.id_projetopedagogico = projetopedagogico.id_projetopedagogico
    OUTER APPLY (
                  SELECT
                    TOP 1
                    *
                  FROM tb_vendaproduto
                  WHERE id_matricula = matricula.id_matricula AND bl_ativo = 1
                  ORDER BY
                    dt_cadastro ASC
                ) AS vendaproduto
    LEFT JOIN tb_pessoadadoscomplementares AS pessoadadoscomplementare
      ON pessoa.id_usuario = pessoadadoscomplementare.id_usuario
    JOIN vw_entidadeesquemaconfiguracao AS eec
      ON eec.id_itemconfiguracao = 22
         AND matricula.id_entidadeatendimento = eec.id_entidade
         AND eec.st_valor = 2
  WHERE
    matricula.id_evolucao IN (6, 21, 20, 81, 15) -- cursando;
    AND NOT EXISTS(
        SELECT
          id_matricula
        FROM tb_censo AS ref
        WHERE ref.id_matricula = matricula.id_matricula
    )
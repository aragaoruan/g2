-- Criar coluna nova
ALTER TABLE
tb_vendaproduto
  ADD
  dt_ingresso DATETIME DEFAULT NULL;

-- xupinhar dados da tabela de dados complementares e colocar na recem criada coluna
UPDATE
  vendaproduto
SET
  vendaproduto.dt_ingresso = dc.dt_ingresso
FROM
  tb_pessoadadoscomplementares AS dc
  JOIN tb_matricula AS mat ON dc.id_usuario = mat.id_usuario
  JOIN tb_vendaproduto AS vendaproduto ON vendaproduto.id_matricula = mat.id_matricula
WHERE
  vendaproduto.id_matricula = mat.id_matricula;

-- excluir coluna em dados complementares, evitando redundância
-- excluir campo após cópia
-- ALTER TABLE tb_pessoadadoscomplementares DROP COLUMN dt_ingresso;
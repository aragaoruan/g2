-- ----------------------------
-- Table structure for tb_matriculaintegracao
-- ----------------------------
DROP TABLE [dbo].[tb_matriculaintegracao]
GO
CREATE TABLE [dbo].[tb_matriculaintegracao] (
[id_matriculaintegracao] int NOT NULL IDENTITY(1,1) ,
[id_matricula] int NOT NULL ,
[id_matriculasistema] int NOT NULL ,
[id_sistema] int NOT NULL ,
[bl_encerrado] bigint NOT NULL DEFAULT ((0)) 
)


GO
DBCC CHECKIDENT(N'[dbo].[tb_matriculaintegracao]', RESEED, 1548)
GO
IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
'SCHEMA', N'dbo', 
'TABLE', N'tb_matriculaintegracao', 
'COLUMN', N'bl_encerrado')) > 0) 
EXEC sp_updateextendedproperty @name = N'MS_Description', @value = N'Campo para a integração com a catraca'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'tb_matriculaintegracao'
, @level2type = 'COLUMN', @level2name = N'bl_encerrado'
ELSE
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Campo para a integração com a catraca'
, @level0type = 'SCHEMA', @level0name = N'dbo'
, @level1type = 'TABLE', @level1name = N'tb_matriculaintegracao'
, @level2type = 'COLUMN', @level2name = N'bl_encerrado'
GO

-- ----------------------------
-- Indexes structure for table tb_matriculaintegracao
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table tb_matriculaintegracao
-- ----------------------------
ALTER TABLE [dbo].[tb_matriculaintegracao] ADD PRIMARY KEY ([id_matriculaintegracao])
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[tb_matriculaintegracao]
-- ----------------------------
ALTER TABLE [dbo].[tb_matriculaintegracao] ADD FOREIGN KEY ([id_matricula]) REFERENCES [dbo].[tb_matricula] ([id_matricula]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[tb_matriculaintegracao] ADD FOREIGN KEY ([id_sistema]) REFERENCES [dbo].[tb_sistema] ([id_sistema]) ON DELETE CASCADE ON UPDATE CASCADE
GO

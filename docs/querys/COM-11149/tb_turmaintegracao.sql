-- ----------------------------
-- Table structure for tb_turmaintegracao
-- ----------------------------
DROP TABLE [dbo].[tb_turmaintegracao]
GO
CREATE TABLE [dbo].[tb_turmaintegracao] (
[id_turmaintegracao] int NOT NULL IDENTITY(1,1) ,
[id_turma] int NOT NULL ,
[st_turmasistema] int NOT NULL ,
[id_sistema] int NOT NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[tb_turmaintegracao]', RESEED, 1020)
GO

-- ----------------------------
-- Indexes structure for table tb_turmaintegracao
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table tb_turmaintegracao
-- ----------------------------
ALTER TABLE [dbo].[tb_turmaintegracao] ADD PRIMARY KEY ([id_turmaintegracao])
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[tb_turmaintegracao]
-- ----------------------------
ALTER TABLE [dbo].[tb_turmaintegracao] ADD FOREIGN KEY ([id_sistema]) REFERENCES [dbo].[tb_sistema] ([id_sistema]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[tb_turmaintegracao] ADD FOREIGN KEY ([id_turma]) REFERENCES [dbo].[tb_turma] ([id_turma]) ON DELETE CASCADE ON UPDATE CASCADE
GO

SET IDENTITY_INSERT tb_funcionalidade ON
INSERT INTO tb_funcionalidade (
  id_funcionalidade,
  st_urlcaminho,
  id_funcionalidadepai,
  st_funcionalidade,
  bl_ativo,
  id_situacao,
  id_tipofuncionalidade,
  bl_pesquisa,
  bl_lista,
  bl_visivel,
  bl_relatorio,
  bl_delete,
  id_sistema,
  st_classeflexbotao ) VALUES (
  837,
-- id_funcionalidade: Novo ID da funcionalidade. Quem roda a SQL em produ��o fica com a responsabilidade de adicionar um novo ID manualmente.
'/configuracao-certificado-parcial',
-- st_urlcaminho: Geralmente, a url da funcionalidade que ser� exibida depois do #/. Aqui � apenas uma string com o id da funcionalidade, visto que nesse caso, estamos adicionando uma funcionalidade interna.
568, -- id_funcionalidadepai: A funcionalidade 'superior' na hierarquia do menu.
'Configurar Certificado Parcial', -- st_funcionalidade: descri��o da funcionalidade.
1, -- bl_ativo: funcionalidade ativa
123, -- id_situacao: funcionalidade em HTML
3, -- id_tipofuncionalidade
1, -- bl_pesquisa
0, -- bl_lista
1, -- bl_visivel
0, -- bl_relatorio
0, -- bl_delete
1,
'CadastroConfiguracaoCertificadoParcial'); --id_sistema: 1 (G2S)
SET IDENTITY_INSERT tb_funcionalidade OFF
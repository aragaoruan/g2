-- id_mensagempadrao => 50
SET IDENTITY_INSERT dbo.tb_mensagempadrao;
GO

INSERT INTO dbo.tb_mensagempadrao (id_mensagempadrao, id_tipoenvio, st_mensagempadrao, st_default, id_textocategoria)
     VALUES (50, 3, 'Renovação - Negociação Inadimplência', NULL, NULL);
GO

ALTER TABLE dbo.tb_nucleoco ADD id_assuntorenovacao INT;
GO

ALTER TABLE dbo.tb_nucleoco WITH CHECK 
  ADD CONSTRAINT FK_TB_NUCLE_REFERENCE_TB_ASSUNTO_id_assuntorenovacao FOREIGN KEY(id_assuntorenovacao)
	  REFERENCES dbo.tb_assuntoco (id_assuntoco);
GO

ALTER TABLE dbo.tb_nucleoco CHECK 
	  CONSTRAINT FK_TB_NUCLE_REFERENCE_TB_ASSUNTO_id_assuntorenovacao;
GO
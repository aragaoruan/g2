SET IDENTITY_INSERT dbo.tb_funcionalidade on
INSERT INTO dbo.tb_funcionalidade
  (
    id_funcionalidade,
    st_funcionalidade,
    id_funcionalidadepai,
    bl_ativo,
    id_situacao,
    st_classeflex,
    st_urlicone,
    id_tipofuncionalidade,
    nu_ordem,
    st_classeflexbotao,
    bl_pesquisa,
    bl_lista,
    id_sistema,
    st_ajuda,
    st_target,
    st_urlcaminho,
    bl_visivel,
    bl_relatorio,
    st_urlcaminhoedicao,
    bl_delete
  )
  VALUES (
    736,
    'Controle de Turma', -- st_funcionalidade - varchar(100)
    257, -- id_funcionalidadepai - int
    1, -- bl_ativo - bit
    123, -- id_situacao - int
    'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa', -- st_classeflex - varchar(300)
    '/img/ico/door--arrow.png', -- st_urlicone - varchar(1000)
    3, -- id_tipofuncionalidade - int
    NULL, -- nu_ordem - int
    NULL, -- st_classeflexbotao - varchar(300)
    0, -- bl_pesquisa - bit
    0, -- bl_lista - bit
    1, -- id_sistema - int
    NULL, -- st_ajuda - varchar(max)
    NULL, -- st_target - varchar(100)
    '/relatorio-controle-turma-grade-horaria', -- st_urlcaminho - varchar(50)
    1, -- bl_visivel - int
    0, -- bl_relatorio - bit
    NULL, -- st_urlcaminhoedicao - varchar(50)
    0  -- bl_delete - bit
  );
SET IDENTITY_INSERT dbo.tb_funcionalidade off
alter table tb_lancamento add nu_assinatura int
-- Este alter table serve para ver em qual assinatura que a parcela foi paga
alter table tb_lancamento add nu_cobranca int
-- Este alter table foi necessario para marcar a parcela onde a assinatura foi renovada ap�s ser cancelada.
update tb_lancamento set tb_lancamento.nu_cobranca = lx.nu_ordem from (select lv.nu_ordem, lv.id_lancamento from tb_lancamentovenda as lv) as lx where tb_lancamento.id_lancamento = lx.id_lancamento
-- o update ir� igualar o novo atributo com o numero da parcela. Desta forma, quando ocorrer a verifica��o do recorrente, n�o ir� bugar

-- A partir de agora, a verifica��o do recorrente se dar� a partir do atributo nu_cobranca. Quando o plano for cancelado e o mesmo for
-- reativado, o nu_cobranca ir� marcar a parcela corretamente para a verifica��o do recorrente
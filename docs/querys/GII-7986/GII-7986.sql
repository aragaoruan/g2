SET IDENTITY_INSERT tb_funcionalidade ON;

INSERT INTO tb_funcionalidade (
  id_funcionalidade,
  st_funcionalidade,
  id_funcionalidadepai,
  bl_ativo,
  id_situacao,
  st_classeflex,
  st_urlicone,
  id_tipofuncionalidade,
  nu_ordem,
  st_classeflexbotao,
  bl_pesquisa,
  bl_lista,
  id_sistema,
  st_ajuda,
  st_target,
  st_urlcaminho,
  bl_visivel,
  bl_relatorio,
  st_urlcaminhoedicao,
  bl_delete
) VALUES (
  821,
  'Arquivo de Migração',
  257,
  1,
  123,
  '/relatorio/download-censo',
  '/img/ico/report.png',
  3,
  null,
  null,
  0,
  0,
  1,
  null,
  null,
  '/relatorio/download-censo',
  1,
  0,
  null,
  0
);

SET IDENTITY_INSERT tb_funcionalidade OFF;

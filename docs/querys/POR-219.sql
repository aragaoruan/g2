-- POR-219 --
SET IDENTITY_INSERT tb_situacao ON
INSERT INTO dbo.tb_situacao(id_situacao, st_situacao, st_tabela, st_campo, st_descricaosituacao)
    VALUES(207, 'Devolvido ao tutor', 'tb_alocacao', 'id_situacaotcc', 'TCC avaliado pelo coordenador ou pedagógico e devolvido ao tutor para reformulação');
SET IDENTITY_INSERT tb_situacao OFF
-- END POR-219 --
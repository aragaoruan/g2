-- Adicionar o campo bl_ativo para exclusão lógica
ALTER TABLE dbo.tb_horarioaula ADD bl_ativo INT NOT NULL DEFAULT(1);


-- Editar Menu
UPDATE dbo.tb_funcionalidade SET id_situacao = 123, st_urlcaminho = '/horario-de-aula' WHERE id_funcionalidade = 25


-- VW Horario Aula, com dias da semana
CREATE VIEW dbo.vw_horarioaula AS
SELECT DISTINCT
	h.id_horarioaula,
	h.st_horarioaula,
	hr_inicio,
	hr_fim,
	t.id_turno,
	st_turno,
	id_entidade,
	bl_ativo,
	(CASE WHEN segunda.id_diasemana IS NOT NULL THEN 1 ELSE 0 END) AS bl_segunda,
	(CASE WHEN terca.id_diasemana IS NOT NULL THEN 1 ELSE 0 END) AS bl_terca,
	(CASE WHEN quarta.id_diasemana IS NOT NULL THEN 1 ELSE 0 END) AS bl_quarta,
	(CASE WHEN quinta.id_diasemana IS NOT NULL THEN 1 ELSE 0 END) AS bl_quinta,
	(CASE WHEN sexta.id_diasemana IS NOT NULL THEN 1 ELSE 0 END) AS bl_sexta,
	(CASE WHEN sabado.id_diasemana IS NOT NULL THEN 1 ELSE 0 END) AS bl_sabado,
	(CASE WHEN domingo.id_diasemana IS NOT NULL THEN 1 ELSE 0 END) AS bl_domingo

FROM
	dbo.tb_horarioaula h
	INNER JOIN dbo.tb_turno t ON t.id_turno = h.id_turno

    OUTER APPLY ( SELECT DISTINCT hd1.id_diasemana FROM dbo.tb_horariodiasemana hd1
                   WHERE hd1.id_horarioaula = h.id_horarioaula AND hd1.id_diasemana = 1
                    ) AS segunda

	OUTER APPLY ( SELECT DISTINCT hd2.id_diasemana FROM dbo.tb_horariodiasemana hd2
                   WHERE hd2.id_horarioaula = h.id_horarioaula AND hd2.id_diasemana = 2
                    ) AS terca

	OUTER APPLY ( SELECT DISTINCT hd3.id_diasemana FROM dbo.tb_horariodiasemana hd3
                   WHERE hd3.id_horarioaula = h.id_horarioaula AND hd3.id_diasemana = 3
                    ) AS quarta

    OUTER APPLY ( SELECT DISTINCT hd4.id_diasemana FROM dbo.tb_horariodiasemana hd4
                   WHERE hd4.id_horarioaula = h.id_horarioaula AND hd4.id_diasemana = 4
                    ) AS quinta

    OUTER APPLY ( SELECT DISTINCT hd5.id_diasemana FROM dbo.tb_horariodiasemana hd5
                   WHERE hd5.id_horarioaula = h.id_horarioaula AND hd5.id_diasemana = 5
                    ) AS sexta

    OUTER APPLY ( SELECT DISTINCT hd6.id_diasemana FROM dbo.tb_horariodiasemana hd6
                   WHERE hd6.id_horarioaula = h.id_horarioaula AND hd6.id_diasemana = 6
                    ) AS sabado

    OUTER APPLY ( SELECT DISTINCT hd7.id_diasemana FROM dbo.tb_horariodiasemana hd7
                   WHERE hd7.id_horarioaula = h.id_horarioaula AND hd7.id_diasemana = 7
                    ) AS domingo

					WHERE bl_ativo = 1
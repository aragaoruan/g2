/***********************************************************************************
					ADD FUNCIONALIDADE
***********************************************************************************/

BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_funcionalidade ON
INSERT INTO [dbo].[tb_funcionalidade]
([id_funcionalidade],
 [st_funcionalidade]
  , [id_funcionalidadepai]
  , [bl_ativo]
  , [id_situacao]
  , [st_classeflex]
  , [st_urlicone]
  , [id_tipofuncionalidade]
  , [st_classeflexbotao]
  , [bl_pesquisa]
  , [bl_lista]
  , [id_sistema]
  , [st_urlcaminho])
VALUES
  (792, 'Edição Colação Grau', 788, 1, 123, NULL, NULL, 7, NULL, 0, 0, 1, 792)

GO
SET IDENTITY_INSERT dbo.tb_funcionalidade OFF

COMMIT


/***********************************************************************************
					ADD PERMISSAO
***********************************************************************************/

BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_permissao ON

INSERT INTO tb_permissao (id_permissao, st_nomepermissao, st_descricao, bl_ativo)
VALUES (46, 'Editar dados da colação de Grau',
        'Permite ao usuário editar os campos da data da colação e do Presente/Ausente.', 1)

SET IDENTITY_INSERT dbo.tb_permissao OFF

COMMIT


/***********************************************************************************
					ADD PERMISSAO FUNCIONALIDADE
***********************************************************************************/

BEGIN TRANSACTION
INSERT INTO tb_permissaofuncionalidade
(id_funcionalidade, id_permissao)
VALUES
  (792, 46)
COMMIT
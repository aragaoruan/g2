SET IDENTITY_INSERT tb_permissao ON
INSERT INTO tb_permissao (id_permissao, st_nomepermissao, st_descricao, bl_ativo) VALUES
  (47, 'Aba Financeiro - Visualizar',
   'Permite ao usuário acesso a aba financeiro de Pessoa Jurídica para visualização somente', 1);
SET IDENTITY_INSERT tb_permissao OFF

INSERT INTO tb_permissaofuncionalidade (id_funcionalidade, id_permissao) VALUES (286, 47);
-- INSERTS GII-8004 --
SET IDENTITY_INSERT tb_funcionalidade ON
INSERT INTO tb_funcionalidade
(id_funcionalidade, st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao,
 st_classeflex, st_urlicone, id_tipofuncionalidade, nu_ordem, st_classeflexbotao, bl_pesquisa,
 bl_lista, id_sistema, st_ajuda, st_target, st_urlcaminho, bl_visivel, bl_relatorio,
 st_urlcaminhoedicao, bl_delete)
VALUES
  (795, 'Etiquetas', 563, 1, 123,
        'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa', NULL, 3, 0, NULL, 0,
   0, 1, NULL, NULL, '/etiqueta', 1, 0, NULL, 0)
SET IDENTITY_INSERT tb_funcionalidade OFF
SET IDENTITY_INSERT tb_permissao ON
INSERT INTO tb_permissao (id_permissao, st_nomepermissao, st_descricao, bl_ativo) VALUES
  (48, 'Serviço de Declaração', 'Permite ao usuário acesso ao Serviço de Declaração no Menu Etiquetas', 1),
  (49, 'Serviço de Certificação', 'Permite ao usuário acesso ao Serviço de Certificação no Menu Etiquetas', 1);
SET IDENTITY_INSERT tb_permissao OFF
INSERT INTO tb_permissaofuncionalidade (id_funcionalidade, id_permissao) VALUES (795, 48), (795, 49);
INSERT INTO tb_perfilpermissaofuncionalidade (id_perfil, id_funcionalidade, id_permissao)
VALUES (741, 795, 48), (741, 795, 49), (738, 795, 48), (738, 795, 49), (29, 795, 49), (104, 795, 48);
-- END --

alter table tb_lancamento add dt_notificacao date null;

/**
Favor observar se o 34 ser� realmente inserido pois � usado no pr�ximo insert
**/
insert into tb_itemconfiguracao values (34, 'N�mero de dias para cobran�a do Cart�o Recorrente', 'Ap�s vencida, ser� enviado um e-mail lembrando ao cliente da parcela ap�s um n�mero predefinido de dias. O e-mail ser� repetido obedecendo este n�mero de dias.', '5');

insert into tb_esquemaconfiguracaoitem select id_esquemaconfiguracao, 34, '5' from tb_esquemaconfiguracao

SET IDENTITY_INSERT dbo.tb_mensagempadrao ON;
insert into tb_mensagempadrao (id_mensagempadrao, id_tipoenvio, st_mensagempadrao, st_default, id_textocategoria) values (54, 3, 'Mensagem de cobran�a recorrente vencido', '<!DOCTYPE html>
<html>
<head>
</head>
<body>
<p>Prezado(a) Aluno(a),</p>
<p>No compromisso de mant&ecirc;-lo sempre atualizado, informamos que at&eacute; a data da emiss&atilde;o deste aviso, n&atilde;o registramos a confirma&ccedil;&atilde;o do pagamento da parcela com vencimento em&nbsp;#lancamento_vencimento# no valor de&nbsp;#lancamento_valor#</p>
<p>Sugerimos que acesse a plataforma portal do aluno https://portal.ead1.com.br/portal selecione no Item de menu &lsquo;Financeiro&rsquo; a op&ccedil;&atilde;o &lsquo;Pagamento&rsquo; para atualizar suas informa&ccedil;&otilde;es de pagamento.</p>
<p>Quaisquer d&uacute;vidas estamos &agrave; disposi&ccedil;&atilde;o atrav&eacute;s do Servi&ccedil;o de Aten&ccedil;&atilde;o ao Aluno &ndash; SAA, atrav&eacute;s da plataforma (https://portal.ead1.com.br/portal), ou pelos telefones 61 3029-9740 ou 3044-7004</p>
<p>Agradecemos sua aten&ccedil;&atilde;o e seu interesse em fazer parte da nossa institui&ccedil;&atilde;o.</p>
<p>Atenciosamente,<br />Departamento Financeiro</p>
</body>
</html>', 6);
SET IDENTITY_INSERT dbo.tb_mensagempadrao OFF;

/**

Daqui para baixo deve ser ignorado

-- select * from tb_entidade where id_entidade = 12

select * from vw_pessoa where id_usuario =5004


sp_help tb_lancamento


-- update tb_lancamento set dt_notificacao = null where id_lancamento = 1095493
-- update tb_contatosemail set st_email = 'elciomgdf@gmail.com' where id_email in (7584, 4272)

select * from vw_gerarvendatexto where id_venda = 3418 AND id_lancamento = 2232956 

select * from tb_textovariaveis where id_textocategoria = 6 and st_textovariaveis like '%#lancamento_valor#%'

select * from tb_esquemaconfiguracaoitem where id_esquemaconfiguracao = 2 and id_itemconfiguracao = 33

select * from tb_mensagempadrao where id_mensagempadrao = 54
-- update tb_mensagempadrao set id_textocategoria = 6  where id_mensagempadrao = 54

select top 100 vw.id_venda, vw.id_lancamento, vw.st_nomecompleto, eci.st_valor, (getdate() - cast(eci.st_valor as int)) as dt_limite_notificacao, vw.nu_ordem, vw.nu_assinatura, vw.dt_emissao, vw.dt_vencimento, vw.dt_quitado, vw.nu_valor 
from vw_vendalancamento as vw  join tb_lancamento as l on (l.id_lancamento = vw.id_lancamento)
join tb_entidade as e on vw.id_entidade = e.id_entidade
join tb_esquemaconfiguracaoitem as eci on e.id_esquemaconfiguracao = eci.id_esquemaconfiguracao and eci.id_itemconfiguracao = 34
where vw.id_meiopagamento = 11 
and vw.id_evolucao = 9 
and vw.bl_quitado = 0 
and vw.dt_quitado is null 
and vw.dt_vencimento < getdate() 
and (l.dt_notificacao is null or l.dt_notificacao < getdate() - cast(eci.st_valor as int))
and vw.bl_ativo = 1
order by vw.id_venda, vw.nu_ordem
-- and 
-- select * from tb_evolucao where id_evolucao in (9,10)
-- select * from tb_situacao where st_tabela = 'tb_lancamento'

-- sp_help tb_lancamento

select eci.*, e.* from tb_entidade as e join tb_esquemaconfiguracaoitem as eci on e.id_esquemaconfiguracao = eci.id_esquemaconfiguracao and eci.id_itemconfiguracao = 34



select * from tb_esquemaconfiguracao
select * from tb_esquemaconfiguracaoitem
select * from tb_itemconfiguracao





-- sp_help tb_esquemaconfiguracaoitem

select * from tb_mensagempadrao
select * from tb_textocategoria
-- insert into tb_textocategoria values (17, 'Parcelas', 'Texto para enviar informa��es sobre as parcelas', 
*/
BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.tb_permissao on
INSERT INTO tb_permissao(
  id_permissao,
  st_nomepermissao,
  st_descricao,
  bl_ativo)
values(
  34,
  'Alterar nota Concluintes e Certificados',
  'Permite ao usuário alterar as notas dos alunos conluintes e certificados.',
  1)
SET IDENTITY_INSERT dbo.tb_permissao off
COMMIT


INSERT INTO tb_permissaofuncionalidade(
  id_funcionalidade,
  id_permissao)
VALUES(
  272,--Gerência Notas
  34);--Alterar nota Concluintes e Certificados
-- Inserção do campo que guardará o texto do boleto
ALTER TABLE dbo.tb_entidadefinanceiro
ADD id_textoenvioboleto int null
FOREIGN KEY (id_textoenvioboleto)
REFERENCES tb_textosistema(id_textosistema)

-- Variavel do TextoSistema venda, que retornará o link do boleto
INSERT INTO [dbo].[tb_textovariaveis]
           ([id_textocategoria]
           ,[st_camposubstituir]
           ,[st_textovariaveis]
           ,[st_mascara]
           ,[id_tipotextovariavel])
     VALUES
           (6 --venda
           ,'id_lancamento'
           ,'#link_boleto_parcela#'
           ,'gerarLinkBoletoParcela'
           ,2)
GO

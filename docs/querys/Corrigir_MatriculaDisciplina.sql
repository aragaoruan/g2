INSERT INTO tb_matriculadisciplina (id_matricula
, id_evolucao
, id_disciplina
, id_situacao
, nu_aprovafinal
, bl_obrigatorio)

SELECT tm.id_matricula
,11
, td.id_disciplina
, 53
, null
,tm2.bl_obrigatoria
FROM tb_matricula tm
JOIN tb_modulo tm1 ON tm.id_projetopedagogico = tm1.id_projetopedagogico AND tm1.bl_ativo = 1
JOIN tb_modulodisciplina tm2 ON tm1.id_modulo = tm2.id_modulo AND tm2.bl_ativo = 1
AND tm2.id_disciplina NOT IN (SELECT id_disciplina FROM tb_matriculadisciplina WHERE id_matricula = tm.id_matricula)
JOIN tb_disciplina td ON tm2.id_disciplina = td.id_disciplina where tm.id_projetopedagogico = 1345
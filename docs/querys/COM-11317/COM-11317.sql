BEGIN TRANSACTION
  SET IDENTITY_INSERT dbo.tb_notificacao ON;
        INSERT INTO dbo.tb_notificacao
                ( id_notificacao ,
                  st_notificacao ,
                  dt_cadastro
                )
        VALUES  ( 2 , -- id_notificacao - int
                  'Contrato Sem Matricula Vinculada' , -- st_notificacao - varchar(50)
                  GETDATE()  -- dt_cadastro - datetime
                )
    SET IDENTITY_INSERT dbo.tb_notificacao OFF;
COMMIT;
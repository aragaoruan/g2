--DESATIVAR IDENTITY TABELA
SET IDENTITY_INSERT tb_funcionalidade ON --usar essa linha só em produção

INSERT INTO tb_funcionalidade
  (id_funcionalidade,  st_funcionalidade, id_funcionalidadepai, bl_ativo, id_situacao,
  st_classeflex, st_urlicone, id_tipofuncionalidade, nu_ordem, st_classeflexbotao, bl_pesquisa,
   bl_lista, id_sistema, st_ajuda, st_target, st_urlcaminho, bl_visivel, bl_relatorio,
  st_urlcaminhoedicao, bl_delete)
VALUES
  (763, 'Consulta de Disciplina', 257, 1, 123,
   'br.com.ead1.gestor2.view.conteudo.masterconteudo.MasterCapa',
   null, 3, 0, null, 0, 0, 1, null, null, '/relatorio-consulta-disciplina', 1, 0, null, 0)

--ATIVAR IDENTITY TABELA
SET IDENTITY_INSERT tb_funcionalidade OFF --usar essa linha só em produção

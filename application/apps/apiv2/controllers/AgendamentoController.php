<?php

class ApiV2_AgendamentoController extends Ead1_AppApi implements \Ead1\Api
{
    /**
     * @var \G2\Negocio\GerenciaProva
     */
    private $negocio;

    public function init()
    {
        $this->negocio = new \G2\Negocio\GerenciaProva();

        return parent::init();
    }

    /**
     * @SWG\Definition(
     *   definition="AvaliacaoAgendamento",
     *   @SWG\Property(property="id_avaliacaoagendamento",type="integer"),
     *   @SWG\Property(property="id_situacao",type="integer"),
     *   @SWG\Property(property="id_tipodeavaliacao",type="integer"),
     *   @SWG\Property(property="id_avaliacao",type="integer"),
     *   @SWG\Property(property="st_avaliacao",type="string"),
     *   @SWG\Property(property="st_nomecompleto",type="string"),
     *   @SWG\Property(property="st_situacao",type="string"),
     *   @SWG\Property(property="dt_aplicacao",type="string"),
     *   @SWG\Property(property="st_aplicadorprova",type="string"),
     *   @SWG\Property(property="st_endereco",type="string"),
     *   @SWG\Property(property="nu_numero",type="integer"),
     *   @SWG\Property(property="st_complemento",type="string"),
     *   @SWG\Property(property="st_bairro",type="string"),
     *   @SWG\Property(property="st_nomemunicipio",type="string"),
     *   @SWG\Property(property="sg_uf",type="string"),
     *   @SWG\Property(property="nu_presenca",type="integer"),
     *   @SWG\Property(property="hr_inicio",type="string"),
     *   @SWG\Property(property="hr_fim",type="string"),
     *   @SWG\Property(property="year",type="string"),
     *   @SWG\Property(property="month",type="string"),
     *   @SWG\Property(property="day",type="string")
     * )
     *
     * @SWG\Get(path="/agendamento",
     *   tags={"agendamento"},
     *   summary="Retorna todos os agendamentos de uma matrícula",
     *   description="Necessário Login.",
     *   operationId="getAll",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="id_matricula",
     *     in="query",
     *     description="Id da Matrícula",
     *     required=false,
     *     type="integer",
     *     example="0"
     *   ),
     *
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/AvaliacaoAgendamento")
     *         )
     *   ),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="Nenhum registro encontrado", @SWG\Schema(ref="#/definitions/Error"))
     *
     * )))
     *
     */
    public function indexAction()
    {
        try {
            $id_matricula = $this->getParam('id_matricula', false);

            if (!$id_matricula) {
                throw new \Exception("Informe a Matrícula");
            }

            $results = $this->negocio->findByVwAvaliacaoAgendamento(array(
                'id_matricula' => $id_matricula,
                'id_situacao' => array(
                    \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_LIBERADO,
                    \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_AGENDADO,
                    \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_REAGENDADO
                )
            ), array('id_avaliacaoagendamento' => 'ASC'));

            if (!$results) {
                throw new \Zend_Http_Exception("Nenhum resultado encontrado!");
            }

            $response = array();

            foreach ($results as $item) {
                $response[] = array(
                    'id_avaliacaoagendamento' => $item->getId_avaliacaoagendamento(),
                    'id_avaliacao' => $item->getId_avaliacao(),
                    'id_situacao' => $item->getId_situacao(),
                    'id_tipodeavaliacao' => $item->getId_tipodeavaliacao(),
                    'st_avaliacao' => $item->getSt_avaliacao(),
                    'st_nomecompleto' => $item->getSt_nomecompleto(),
                    'st_situacao' => $item->getSt_situacao(),
                    'st_aplicadorprova' => $item->getSt_aplicadorprova(),
                    'st_endereco' => $item->getSt_endereco(),
                    'nu_numero' => $item->getNu_numero(),
                    'st_complemento' => $item->getSt_complemento(),
                    'st_bairro' => $item->getSt_bairro(),
                    'st_nomemunicipio' => $item->getSt_cidade(),
                    'sg_uf' => $item->getSg_uf(),
                    'nu_presenca' => $item->getNu_presenca(),
                    'dt_aplicacao' => $item->getDt_aplicacao() ? $this->negocio->converterData($item->getDt_aplicacao(), 'Y-m-d') : null,
                    'hr_inicio' => $item->getHr_inicio() ? $this->negocio->converterData($item->getHr_inicio(), 'H:i') : null,
                    'hr_fim' => $item->getHr_fim() ? $this->negocio->converterData($item->getHr_fim(), 'H:i') : null,
                    'year' => $item->getDt_aplicacao() ? $this->negocio->converterData($item->getDt_aplicacao(), 'Y') : null,
                    'month' => $item->getDt_aplicacao() ? $this->negocio->converterData($item->getDt_aplicacao(), 'm') : null,
                    'day' => $item->getDt_aplicacao() ? $this->negocio->converterData($item->getDt_aplicacao(), 'd') : null,
                );
            }

            $this->json($response);
        } catch (\Zend_Http_Exception $e) {
            $this->getResponse()->setHttpResponseCode(404);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 404);
            $this->json($error->toArray());
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }
    }

    public function getAction()
    {
        // TODO: Implement getAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     *
     * @SWG\Definition(
     *   definition="AvaliacaoAgendamentoEdit",
     *   @SWG\Property(property="id_avaliacaoagendamento",type="integer"),
     *   @SWG\Property(property="id_avaliacaoaaplicacao",type="integer"),
     *   @SWG\Property(property="id_situacao",type="integer"),
     *   @SWG\Property(property="id_tipodeavaliacao",type="integer"),
     *   @SWG\Property(property="id_avaliacao",type="integer"),
     *   @SWG\Property(property="nu_presenca",type="integer")
     * )
     *
     * @SWG\Post(path="/agendamento/create",
     *   tags={"agendamento"},
     *   summary="Realiza um novo agendamento.",
     *   description="Necessário Login.",
     *   operationId="create",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     description="Objeto com os dados do agendamento",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/AvaliacaoAgendamentoEdit")
     *   ),
     *   @SWG\Response(response=201,description="Operação efetuada com sucesso",
     *     @SWG\Schema(ref="#/definitions/AvaliacaoAgendamento")
     *  ),
     *   @SWG\Response(response=400, description="Dados inválidos para o cadastro",
     *     @SWG\Schema(ref="#/definitions/Error")
     *  ),
     * )))))
     *
     */
    public function createAction()
    {
        try {
            $params = Zend_Json::decode($this->getRequest()->getRawBody());

            if (!isset($params['id_matricula']) || !$params['id_matricula']) {
                throw new \Exception("Informe a Matrícula");
            }

            if (!isset($params['id_avaliacaoaplicacao']) || !$params['id_avaliacaoaplicacao']) {
                throw new \Exception("Informe a Aplicação do agendamento");
            }

            $params['id_entidade'] = $this->getuser()->getentidade_id();
            $params['id_usuario'] = $this->getuser()->getuser_id();

            // id_tipodeavaliacao define se a prova é de RECUPERAÇÃO, sendo:
            //  0 = Normal,
            //  1 = Recuperação 1,
            //  2 = Recuperação 2,
            //  ETC
            if (!isset($params['id_tipodeavaliacao'])) {
                throw new \Exception("Informe o tipo de avaliação.");
            }

            $mensageiro = $this->negocio->salvarAgendamentoPortal($params);

            if (($mensageiro instanceof Ead1_Mensageiro) && $mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Exception($mensageiro->getText());
            }

            $response = $this->negocio->find('\G2\Entity\VwAvaliacaoAgendamento', $mensageiro->getMensagem()['id_avaliacaoagendamento']);

            $this->json($this->negocio->toArrayEntity($response));
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }
    }

    public function updateAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, "Método ainda não implementado", 404);
        $this->_helper->json($error->toArray());
    }

    public function deleteAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, "Método ainda não implementado", 404);
        $this->_helper->json($error->toArray());
    }

    public function pagingAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, "Método ainda não implementado", 404);
        $this->_helper->json($error->toArray());
    }

    /**
     * @SWG\Definition(
     *   definition="DisciplinasAgendamento",
     *   @SWG\Property(property="id_disciplina",type="integer"),
     *   @SWG\Property(property="st_tituloexibicao",type="string")
     * )
     *
     * @SWG\Get(path="/agendamento/disciplinas",
     *   tags={"agendamento"},
     *   summary="Retorna a lista de disciplinas do agendamento de uma matricula",
     *   description="Necessário Login",
     *   operationId="disciplinas",
     *   produces={"application/json"},
     *
     *   @SWG\Parameter(
     *     name="id_matricula",
     *     in="query",
     *     description="Id da Matrícula",
     *     required=true,
     *     type="integer",
     *     example="0"
     *   ),
     *
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/DisciplinasAgendamento")
     *         )
     *   ),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="Nenhum registro encontrado", @SWG\Schema(ref="#/definitions/Error"))
     *
     * )))
     *
     *
     */
    public function disciplinasAction()
    {
        try {
            $id_matricula = $this->getParam('id_matricula', false);

            if (!$id_matricula) {
                throw new \Exception("Informe a Matrícula");
            }

            $negocio = new \G2\Negocio\AvaliacaoAluno();

            $results = $negocio->findByAlunoAvaliacao(array(
                'id_matricula' => $id_matricula
            ));

            if (!$results) {
                throw new \Zend_Http_Exception("Nenhum resultado encontrado!");
            }

            $response = array();

            foreach ($results as $item) {
                $response[$item->getId_disciplina()] = array(
                    'id_disciplina' => $item->getId_disciplina(),
                    'st_tituloexibicao' => $item->getSt_tituloexibicaodisciplina()
                );
            }

            $this->json(array_values($response));
        } catch (\Zend_Http_Exception $e) {
            $this->getResponse()->setHttpResponseCode(404);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 404);
            $this->json($error->toArray());
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }
    }

    /**
     * @SWG\Definition(
     *   definition="DatasDisponiveis",
     *   @SWG\Property(property="id_avaliacaoaplicacao",type="integer"),
     *   @SWG\Property(property="st_aplicadorprova",type="string"),
     *   @SWG\Property(property="st_endereco",type="string"),
     *   @SWG\Property(property="nu_numero",type="integer"),
     *   @SWG\Property(property="st_complemento",type="string"),
     *   @SWG\Property(property="st_bairro",type="string"),
     *   @SWG\Property(property="st_nomemunicipio",type="string"),
     *   @SWG\Property(property="sg_uf",type="string"),
     *   @SWG\Property(property="dt_aplicacao",type="string"),
     *   @SWG\Property(property="hr_inicio",type="string"),
     *   @SWG\Property(property="hr_fim",type="string"),
     *   @SWG\Property(property="year",type="string"),
     *   @SWG\Property(property="month",type="string"),
     *   @SWG\Property(property="day",type="string")
     * )
     *
     * @SWG\Get(path="/agendamento/datas-disponiveis",
     *   tags={"agendamento"},
     *   summary="Retorna as datas disponíveis para o agendamento da Pós",
     *   description="Necessário Login",
     *   operationId="datas-disponiveis",
     *   produces={"application/json"},
     *
     *   @SWG\Parameter(
     *     name="sg_uf",
     *     in="query",
     *     description="Sigla do UF",
     *     required=true,
     *     type="string",
     *     example="DF"
     *   ),
     *   @SWG\Parameter(
     *     name="id_municipio",
     *     in="query",
     *     description="Id do Município",
     *     required=true,
     *     type="integer",
     *     example="5300108"
     *   ),
     *
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/DatasDisponiveis")
     *         )
     *   ),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="Nenhum registro encontrado", @SWG\Schema(ref="#/definitions/Error"))
     *
     * )))
     *
     */
    public function datasDisponiveisAction()
    {
        try {
            $sg_uf = $this->getParam('sg_uf');
            $id_municipio = $this->getParam('id_municipio');

            if (!$sg_uf) {
                throw new \Exception("Informe o UF");
            }

            if (!$id_municipio) {
                throw new \Exception("Informe o Município");
            }

            $results = $this->negocio->findVwAplicacoes(array(
                'sg_uf' => $sg_uf,
                'id_municipio' => $id_municipio,
                'id_entidade' => $this->getuser()->getentidade_id(),
            ));

            if (!$results) {
                throw new \Zend_Http_Exception("Não há datas disponíveis para agendamento.");
            }

            $response = array();

            foreach ($results as $item) {
                $response[] = array(
                    'id_avaliacaoaplicacao' => $item->getId_avaliacaoaplicacao(),
                    'st_aplicadorprova' => $item->getSt_aplicadorprova(),
                    'st_endereco' => $item->getSt_endereco(),
                    'nu_numero' => $item->getNu_numero(),
                    'st_complemento' => $item->getSt_complemento(),
                    'st_bairro' => $item->getSt_bairro(),
                    'st_nomemunicipio' => $item->getSt_cidade(),
                    'sg_uf' => $item->getSg_uf(),
                    'dt_aplicacao' => $this->negocio->converterData($item->getDt_aplicacao(), 'Y-m-d'),
                    'hr_inicio' => $this->negocio->converterData($item->getHr_inicioprova(), 'H:i'),
                    'hr_fim' => $this->negocio->converterData($item->getHr_fimprova(), 'H:i'),
                    'year' => $this->negocio->converterData($item->getDt_aplicacao(), 'Y'),
                    'month' => $this->negocio->converterData($item->getDt_aplicacao(), 'm'),
                    'day' => $this->negocio->converterData($item->getDt_aplicacao(), 'd')
                );
            }

            $this->json($response);
        } catch (\Zend_Http_Exception $e) {
            $this->getResponse()->setHttpResponseCode(404);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 404);
            $this->json($error->toArray());
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }
    }

    /**
     * @SWG\Get(path="/get-agendamento-comprovante",
     *   tags={"agendamento"},
     *   summary="Retorna os detalhes do agendamento",
     *   description="Retorna o último agendamento que ainda possui comprovante disponível.",
     *   operationId="getAgendamentoComprovante",
     *   produces={"application/json", "text/html"},
     *   @SWG\Parameter(
     *     name="id_matricula",
     *     in="query",
     *     description="ID da Matrícula",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response="default", description="Operação efetuada com sucesso"),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     * )
     */
    public function getAgendamentoComprovanteAction()
    {
        try {
            $id_matricula = $this->getParam('id_matricula', false);

            if (!$id_matricula) {
                throw new \Exception("Informe a Matrícula");
            }

            $result = $this->negocio->retornarAgendamentoComprovanteDisponivel($id_matricula);

            if (!($result instanceof \G2\Entity\VwAvaliacaoAgendamento)) {
                throw new \Zend_Http_Exception("Nenhum resultado encontrado!");
            }

            $response = $this->negocio->toArrayEntity($result);

            // Campos de data
            $response['dt_agendamento'] = $this->negocio->converterData($response['dt_agendamento'], 'Y-m-d');
            $response['dt_aplicacao'] = $this->negocio->converterData($response['dt_aplicacao'], 'Y-m-d');

            $this->json($response);
        } catch (\Zend_Http_Exception $e) {
            $this->getResponse()->setHttpResponseCode(404);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 404);
            $this->json($error->toArray());
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }
    }

}

<?php

class ApiV2_DownloadController extends Ead1_AppApi
{

    private $secret;

    public function init()
    {

        $this->secret = md5('57695d052a65aed581c7cbd98ac83f88');

        try {

            $st_token = $this->_getParam('st_token', false);
            if (!$st_token && $this->getRequest()->getActionName() != 'auth') {
                throw new \Exception("acesso inválido!");
            }

            if ($this->getRequest()->getActionName() != 'auth') {

                $encrypted = base64_decode($st_token);
                $decrypted = unserialize(mcrypt_decrypt(
                    MCRYPT_RIJNDAEL_256,
                    $this->secret,
                    $encrypted,
                    MCRYPT_MODE_ECB));

                if (!$decrypted)
                    throw new \Exception("Token inválido");

                if (!$decrypted || !$decrypted['st_action'])
                    throw new \Exception("Formato de Token inválido");


                if ($decrypted['st_action'] != $this->getRequest()->getActionName())
                    throw new \Exception("Você está tentando baixar um arquivo com o token de outro");


                $now = new \DateTime();
                $validade = new \DateTime($decrypted['dt_validade']);
                $interval = $validade->diff($now);
                if (!$interval->format('%I'))
                    throw new \Exception("O Arquivo expirou, favor autenticar-se novamente!");
            }

        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }

    }

    /**
     *
     * @SWG\Definition(
     *   definition="DownloadResponse",
     *   @SWG\Property(property="st_action",type="string"),
     *   @SWG\Property(property="st_token",type="string"),
     *   @SWG\Property(property="dt_validade",type="string")
     * )
     *
     *
     * @SWG\Get(path="/download/auth/download-action/{download-action}",
     *   tags={"download"},
     *   summary="Retorna todos os Usuários",
     *   description="Você precisa estar logado para usar este método. Ele aceita alguns parâmetros como filtro.",
     *   operationId="getAll",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="path",
     *     name="download-action",
     *     description="Action para download",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso",
     *     @SWG\Items(ref="#/definitions/DownloadResponse")),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="Nenhum registro encontrado", @SWG\Schema(ref="#/definitions/Error"))
     *
     * )))))
     *
     */
    public function authAction()
    {

        try {

            if ($this->getRequest()->isOptions()) {
                $this->json(array());
            }

            //Se o request não for get, joga exceção
            if (!$this->getRequest()->isGet()) {
                throw new Exception("Método inválido, por favor faça a requisição usando GET");
            }

            //Pega o parametro download_action. Se não existir, seta false.
            $downloadAction = $this->getParam('download_action', false);
            //Se não houver download action, exceção pedindo o objetivo do download.
            if (!$downloadAction)
                throw new \Exception("Informe qual arquivo é o objetivo de download!");

            //Pega o user_token.
            $header = $this->_request->getHeader('user_token');
            $tokenObj = JWT::decode($header, null, array('HS256'));

            if (!$tokenObj->user_id)
                throw new \Exception("Você precisa estar logado para acessar este método!");

            $dt = new DateTime();
            $dt->add(new DateInterval('PT1H'));

            $token = array(
                'st_action' => $downloadAction,
                'dt_validade' => $dt->format('Y-m-d H:i:s'),
                'id_usuario' => $tokenObj->user_id
            );
            $encrypted = base64_encode(mcrypt_encrypt(
                MCRYPT_RIJNDAEL_256,
                $this->secret,
                serialize($token),
                MCRYPT_MODE_ECB
            ));
            //Garante que a string retornada seja URL-safe
            $encrypted_base64 = rawurlencode($encrypted);

            $this->json(array(
                'st_action' => $downloadAction,
                'st_token' => $encrypted_base64,
                'dt_validade' => $dt,
            ));

        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }

    }


    /**
     *
     * @SWG\Get(path="/download/plano-pagamento",
     *   tags={"download"},
     *   summary="Faz o download do Plano de pagamento via PDF",
     *   description="É obrigatório pegar o token de acesso via download/auth.",
     *   operationId="getAll",
     *   produces={"application/json", "application/pdf"},
     *   @SWG\Parameter(
     *     in="query",
     *     name="st_token",
     *     description="Token de acesso ao método de download",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso"),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="Nenhum registro encontrado", @SWG\Schema(ref="#/definitions/Error"))
     *
     * )))
     *
     */
    public function planoPagamentoAction()
    {
        try {
            $idEntidade = $this->_getParam('id_entidade');
            $idMatricula = $this->_getParam('id_matricula');
            $planoPagamentoExiste = false;

            $textoBO = new TextoSistemaBO();

            $to = new TextoSistemaTO();
            $to->setId_textocategoria(G2\Constante\TextoCategoria::PLANO_PAGAMENTO);
            $to->setId_entidade($idEntidade);
            $to->fetch(false, true, true);

            if (!$to->getSt_texto()) {
                $texto = "Nenhum texto sistema para Plano de Pagamento foi encontrado.";
            }

            if ($idMatricula) {
                $matriculaContrato = new ContratoMatriculaTO();
                $matriculaContrato->setId_matricula($idMatricula);
                $matriculaContrato->fetch(false, true, true);

                if ($matriculaContrato->getId_contrato()) {
                    $firstMessage = $textoBO->gerarHTML($to, array(
                        'id_contrato' => $matriculaContrato->getId_contrato()
                    ));
                    if ($firstMessage->getFirstMensagem() instanceof TextoSistemaTO) {
                        $planoPagamentoExiste = true;
                        $texto = $firstMessage->getFirstMensagem()->getSt_texto();
                    } else {
                        $texto = 'Nenhum texto foi encontrado para esta entidade (' . $idEntidade . ').';
                    }
                } else {
                    $texto = 'Nenhum contrato localizado para a matrícula ' . $idMatricula . '.';
                }
            } else {
                $texto = "Não foi possível localizar o código da matricula.";
            }

            if ($planoPagamentoExiste) {
                $pdf = new Ead1_Pdf();
                $pdf->carregarHTML(utf8_encode($texto));
                $pdf->gerarPdf('plano-de-pagamento.pdf');
                exit;
            } else {
                throw new Exception($texto);
            }

        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     *
     * @SWG\Get(path="/download/imposto-renda",
     *   tags={"download"},
     *   summary="Gera o dados em PDF e retornar para download do Imposto de Renda",
     *   description="É obrigatório pegar o token de acesso via download/auth.",
     *   operationId="getAll",
     *   produces={"application/json", "application/pdf"},
     *   @SWG\Parameter(
     *     in="query",
     *     name="st_token",
     *     description="Token de acesso ao método de download",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     in="query",
     *     name="id_matricula",
     *     description="Id da matricula",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     in="query",
     *     name="id_entidade",
     *     description="Id da entidade",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso"),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="Nenhum registro encontrado", @SWG\Schema(ref="#/definitions/Error"))
     *
     * )))
     *
     */
    public function impostoRendaAction()
    {
        try {
            $idEntidade = $this->_getParam('id_entidade');
            $id_matricula = $this->_getParam('id_matricula');

            if (!$id_matricula) {
                throw new \Exception("Não foi possível localizar o código da matricula.");
            }

            if (!$idEntidade) {
                throw new \Exception("Não foi possível localizar o código da entidade.");
            }


            $EntidadeFinBO = new EntidadeFinanceiroTO();
            $EntidadeFinBO->setId_entidade($idEntidade);
            $EntidadeFinBO->fetch(false, true, true);

            $bo = new TextoSistemaBO();
            $textoSistemaTO = new TextoSistemaTO();
            $textoSistemaTO->setId_textosistema($EntidadeFinBO->getId_textoimpostorenda()
                ? $EntidadeFinBO->getId_textoimpostorenda() : 175);

            $textoSistemaTO->fetch(true, true, true);
            if (!$textoSistemaTO->getSt_texto()) {
                throw new Exception("Texto sistema não localizado.");
            }

            $vwGerarDeclaracaoImpostoRendaTO = new VwGerarDeclaracaoImpostoRendaTO();
            $vwGerarDeclaracaoImpostoRendaTO->setId_matricula($id_matricula);
            $vwGerarDeclaracaoImpostoRendaTO->fetch(false, true, true);

            if (!$vwGerarDeclaracaoImpostoRendaTO) {
                $params['id_venda'] = $vwGerarDeclaracaoImpostoRendaTO->getId_venda();
                $result = $bo->gerarHTML($textoSistemaTO, $params)->subtractMensageiro();

                $texto = $result->mensagem[0]->getSt_texto();

                $str = "Bras&iacute;lia";
                $str_lg = strlen($str) + 2;
                $data_str = substr($texto, strripos($texto, $str) + $str_lg, 10);
                $data = new Zend_Date($data_str, 'pt_BR');
                $data_loc = $data->get("d 'de' MMMM 'de' YYYY");
                $texto_alt = str_replace($data_str, $data_loc, $texto);

                $retorno = $texto_alt;
            } else {
                $retorno = '<div class="container cabecalho prepend-top erro span-24" >';
                $retorno .= '<p><strong>Prezado(a) Aluno</strong>,</p>';
                $retorno .= '<p>Não existem dados financeiros para geração da declaração de Imposto de Renda.</p>';
                $retorno .= '</div>';
            }

            if (!isset($retorno) && empty($result)) {
                throw new Exception("Não foi possível gerar o imposto de renda.");
            }

            $pdf = new Ead1_Pdf();
            $pdf->carregarHTML(utf8_encode($retorno));
            $pdf->gerarPdf('imposto-de-renda.pdf');
            exit;


        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }

    }

    /**
     *
     * @SWG\Get(path="/download/tabela-preco",
     *   tags={"download"},
     *   summary="Faz o download da tabela de preço via PDF",
     *   description="É obrigatório pegar o token de acesso via download/auth.",
     *   operationId="getAll",
     *   produces={"application/json", "application/pdf"},
     *   @SWG\Parameter(
     *     in="query",
     *     name="st_token",
     *     description="Token de acesso ao método de download",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     in="query",
     *     name="id_entidade",
     *     description="Id da entidade",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso"),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="Nenhum registro encontrado", @SWG\Schema(ref="#/definitions/Error"))
     *
     * )))
     *
     */
    public function tabelaPrecoAction()
    {
        try {
            $id_entidade = $this->_getParam('id_entidade');

            if (!$id_entidade) {
                throw new \Exception("Não foi possível localizar o código da entidade.");
            }

            $configEntidade = new \G2\Negocio\ConfiguracaoEntidade();
            $entidade = $configEntidade->findConfiguracaoEntidade($id_entidade);
            if ($entidade["id_tabelapreco"]['id_upload']) {
                $uploadNegocio = new \G2\Negocio\Upload();
                $upload = $uploadNegocio->findUpload($entidade['id_tabelapreco']['id_upload']);

                $url = 'upload/' . $id_entidade . '/' . $upload->getSt_upload();
                $result = array('url' => 'upload/' . $id_entidade . '/' . $upload->getSt_upload());

                // Configuramos os headers que serão enviados para o browser
                header("Content-Type: application/pdf"); // informa o tipo do arquivo ao navegador
                header("Content-Length: " . filesize($url)); // informa o tamanho do arquivo ao navegador
                header("Content-Disposition: attachment; filename=" . basename($url)); // informa ao navegador que é tipo anexo e faz abrir a janela de download, tambem informa o nome do arquivo
                readfile($url); // lê o arquivo
                exit;

            }
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }
    }

    /**
     *
     * @SWG\Get(path="/download/contrato",
     *   tags={"download"},
     *   summary="Retorna um JSON com todos os contratos disponiveis, para fazer donwload na aplicaçao",
     *   description="É obrigatório pegar o token de acesso via download/auth.",
     *   operationId="getAll",
     *   produces={"application/json", "application/pdf"},
     *   @SWG\Parameter(
     *     in="query",
     *     name="st_token",
     *     description="Token de acesso ao método de download",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     in="query",
     *     name="id_usuario",
     *     description="Id da usuario",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     in="query",
     *     name="id_matricula",
     *     description="Id da matricula",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso"),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="Nenhum registro encontrado", @SWG\Schema(ref="#/definitions/Error"))
     *
     * )))
     *
     */
    public function contratoAction()
    {
        try {
            $minhaPastaNegocio = new \G2\Negocio\MinhaPasta();
            $matriculaNegocio = new \G2\Negocio\Matricula();
            $portalMinhaPastaBO = new PortalMinhaPastaBO();

            $id_usuario = $this->_getParam('id_usuario');
            $id_matricula = $this->_getParam('id_matricula');

            if (!$id_usuario) {
                throw new \Exception("Informe o id do usuario!");
            }

            if (!$id_matricula) {
                throw new \Exception("Informe o id da matricula!");
            }

            $venda = $matriculaNegocio->getRepository('\G2\Entity\Matricula')->retornaVendaMatricula(array(
                    'id_matricula' => $id_matricula)
            );
            //se não achar a venda pesquisa na tb_vendaaditivo
            if (!$venda) {
                $negocio = new \G2\Negocio\Negocio();
                $vendaaditivo = $negocio->findOneBy('\G2\Entity\VendaAditivo', array(
                        'id_matricula' => $id_matricula)
                );
                if ($vendaaditivo instanceof G2\Entity\VendaAditivo) {
                    $venda['id_venda'] = $vendaaditivo->getId_venda()->getId_venda();
                }
            }

            //Lista arquivos que estao salvos no banco
            $arrListaArquivosBanco = $minhaPastaNegocio->retornaMinhaPasta(array(
                'id_situacao' => G2\Constante\Situacao::TB_MINHAPASTA_ATIVO,
                'id_usuario' => $id_usuario,
                'id_venda' => $venda['id_venda']
            ));

            $listaArquivosBanco = array();

            foreach ($arrListaArquivosBanco AS $key => $item) {
                if (!$item['st_contratoUrl']) {
                    $arrListaArquivosBanco[$key]['url'] = 'upload/minhapasta/'
                        . $item['id_venda'] . '/' . $item['st_contrato'];
                } else {
                    $arrListaArquivosBanco[$key]['url'] = 'upload/minhapasta/'
                        . $item['id_venda'] . '/' . $item['st_contratoUrl'];
                }
                /**
                 * Não coloca os dados do contrato num array para pegar sempre o último contrato da venda do banco,
                 * descartando assim os outros contratos gerados indevidamente.
                 */
                $listaArquivosBanco[0] = $arrListaArquivosBanco[$key];
            }

            //Lista arquivos que estao salvos somente no servidor
            $listaArquivosServidor = $portalMinhaPastaBO->retornarAquivosMinhaPasta(
                null,
                $listaArquivosBanco,
                $id_matricula,
                true
            );

            $result = array_merge($listaArquivosServidor, $listaArquivosBanco);

            $this->json($result);
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }
    }


}

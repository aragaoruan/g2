<?php

/**
 * @SWG\Swagger(
 *     schemes={"http"},
 *     basePath="/api-v2",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Portal/G2 API",
 *         description="API do G2s e Portal do aluno para Aplicativos",
 *         termsOfService="http://g2tec.ead1.net/termos.html",
 *         @SWG\Contact(
 *             email="suporte@unyleya.com.br"
 *         ),
 *         @SWG\License(
 *             name="Apache 2.0",
 *             url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *         )
 *     )
 * )
 */
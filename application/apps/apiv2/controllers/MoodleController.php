<?php

class ApiV2_MoodleController extends Ead1_AppApi implements \Ead1\Api
{
    /**
     *
     * @SWG\Definition(
     *     definition="MoodleGetTokenResponse",
     *     @SWG\Property(property="token",type="integer", example="999888sddhjkjkjde"),
     *     @SWG\Property(property="userid",type="integer", example="999"),
     *     @SWG\Property(property="st_loginintegrado",type="string", example="usuario.exemplo"),
     *     @SWG\Property(property="id_entidade",type="integer", example="999"),
     *     @SWG\Property(property="siteurl",type="integer", example="http://mdl.ead1.net"),
     * ),
     *
     *
     * @SWG\Get(path="/moodle/get-token",
     *   tags={"moodle"},
     *   summary="Com base na matrícula, retorna o token de acesso ao WS",
     *   description="Você precisa estar logado para usar este método. Ele aceita alguns parâmetros como filtro.",
     *   operationId="get-token-by-matricula",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="query",
     *     name="id_saladeaula",
     *     description="ID da Sala de Aula para que possamos pegar a plataforma correta",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     in="query",
     *     name="id_matricula",
     *     description="ID da matrícula do Aluno",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso",
     *         @SWG\Schema(
     *             @SWG\Items(ref="#/definitions/MoodleGetTokenResponse")
     *         )
     *   ),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="Nenhum registro encontrado", @SWG\Schema(ref="#/definitions/Error"))
     *
     * )))
     *
     */
    public function getTokenAction()
    {

        try {

            $id_matricula = $this->_getParam('id_matricula', false);
            if (!$id_matricula) {
                throw new \Exception("Informe a Matrícula do Aluno");
            }

            $id_saladeaula = $this->_getParam('id_saladeaula', false);
            if (!$id_saladeaula) {
                throw new \Exception("Informe a Sala de Aula");
            }

            //consulta a sala de aula
            $salaAula = new \G2\Entity\SalaDeAula();
            $salaAula->find($id_saladeaula);

            if (!$salaAula->getId_entidade() && !$salaAula->getid_entidadeintegracao()) {
                throw new \Exception("Sala de Aula não encontrada");
            }

            //autentica no moodle
            $moodleWs = new MoodleAlocacaoWebServices(
                $salaAula->getId_entidade(),
                $salaAula->getid_entidadeintegracao()->getId_entidadeintegracao()
            );

            $result = $moodleWs->getWsToken($id_matricula, $this->getuser()->getuser_id());

            if ($result->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                throw new \Exception($result->getFirstMensagem());
            }

            $this->json((array)$result->getFirstMensagem());

        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }

    }

    /**
     *
     * @SWG\Definition(
     *     definition="MoodleGetTokenAcessoResponse",
     *     @SWG\Property(property="loginurl",type="integer", example="http://mdl.ead1.net/auth/userkey/login.php?key=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx")
     * ),
     *
     *
     * @SWG\Get(path="/moodle/get-token-acesso",
     *   tags={"moodle"},
     *   summary="Com base na matrícula, retorna a URL de acesso ao Moodle",
     *   description="Você precisa estar logado para usar este método. Ele aceita alguns parâmetros como filtro.",
     *   operationId="get-token-by-acesso",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="query",
     *     name="id_saladeaula",
     *     description="ID da Sala de Aula para que possamos pegar a plataforma correta",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     in="query",
     *     name="st_loginintegrado",
     *     description="Login do Usuário no Moodle",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso",
     *         @SWG\Schema(
     *             @SWG\Items(ref="#/definitions/MoodleGetTokenAcessoResponse")
     *         )
     *   ),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="Nenhum registro encontrado", @SWG\Schema(ref="#/definitions/Error"))
     *
     * )))
     *
     */
    public function getTokenAcessoAction()
    {

        try {

            $st_loginintegrado = $this->_getParam('st_loginintegrado', false);
            if (!$st_loginintegrado) {
                throw new \Exception("Informe o Login do aluno no Moodle");
            }


            $id_saladeaula = $this->_getParam('id_saladeaula', false);
            if (!$id_saladeaula) {
                throw new \Exception("Informe a Sala de Aula");
            }

            $salaDeAula = new \G2\Entity\SalaDeAula();
            $salaDeAula->find($id_saladeaula);

            if (!$salaDeAula->getId_entidade() && !$salaDeAula->getid_entidadeintegracao()) {
                throw new \Exception("Sala de Aula não encontrada");
            }

            $moodleWs = new MoodleAlocacaoWebServices(
                $salaDeAula->getId_entidade(),
                $salaDeAula->getid_entidadeintegracao()->getId_entidadeintegracao()
            );

            $result = $moodleWs->requestTokenAcesso($salaDeAula->getId_entidade(), $st_loginintegrado);

            if ($result->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                throw new \Exception($result->getFirstMensagem());
            }

            $this->json(array('loginurl' => $result->getFirstMensagem()));

        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }


    }

    /**
     * @SWG\Definition(
     *     definition="MoodlelistRecentActivityResponse",
     *     @SWG\Property(property="id",type="integer", example="1234"),
     *     @SWG\Property(property="name",type="string", example="Atividade de Contextualização e Atualidades"),
     *     @SWG\Property(property="added",type="timestamp", example="1436465629"),
     *     @SWG\Property(property="modname",type="string", example="assign"),
     *     @SWG\Property(property="url",type="string", example="/mod/assign/view.php"),
     *     @SWG\Property(property="since",type="string", example="Tuesday, 22 Nov 2016, 13:01"),
     *     @SWG\Property(property="data",type="string", example="22/11/2016"),
     * ),
     * @SWG\Get(path="/moodle/list-recent-activity",
     *   tags={"moodle"},
     *   summary="Retorna as atividades recentes do moodle",
     *   description="Lista as atividades do curso no moodle que foram alteradas ou criadas nos 2 ultimos dias.",
     *   operationId="list-recent-activity",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="query",
     *     name="idMatricula",
     *     description="ID da matricula. É obrigatorio para buscar as atividades corretas do curso",
     *     required=true,
     *     type="integer"
     *   ),
     *     @SWG\Parameter(
     *     in="query",
     *     name="idSaladeaula",
     *     description="ID da sala de aula no G2S. Esse parametro é obrigatorio para buscar pelo curso integrado",
     *     required=true,
     *     type="integer"
     *   ),
     *    @SWG\Response(response="default",description="Operação efetuada com sucesso",
     *         @SWG\Schema(
     *             @SWG\Items(ref="#/definitions/MoodlelistRecentActivityResponse")
     *         )
     *   ),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     *
     * )))
     *
     */
    public function listRecentActivityAction()
    {
        try {

            $idMatricula = $this->_getParam('idMatricula', false);
            $idSalaDeAula = $this->_getParam('idSaladeaula', false);
            $idCourse = $this->_getParam('stCodsistemasala', false);

            if ((!$idCourse || !$idSalaDeAula)) {
                throw new \Exception("Informe a Sala de Aula ou a Sala de Aula do Moodle");
            }

            if (!$idMatricula) {
                throw new \Exception("Informe a Matricula");
            }

            $matricula = new \G2\Entity\Matricula();
            $matricula->find($idMatricula);

            /** @var \G2\Entity\SalaDeAula $salaDeAula */
            $salaDeAula = new \G2\Entity\SalaDeAula();
            $salaDeAula->find($idSalaDeAula);

            if (!$salaDeAula && !$salaDeAula->getid_entidadeintegracao()) {
                throw new Exception("Sala de aula não integrada ao moodle.");
            }

            if ($matricula->getId_entidadematricula()) {
                $moodle = new MoodleUnyleyaWebServices(
                    $matricula->getId_entidadematricula(),
                    $salaDeAula->getid_entidadeintegracao()->getId_entidadeintegracao()
                );

                $mensageiro = $moodle->listRecentActivities((int)$idMatricula, $idSalaDeAula, $idCourse);

                if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new \Exception($mensageiro->getFirstMensagem());
                }

                $this->json($mensageiro->getMensagem());
            }


        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }
    }

    /**
     * Método responsável por retornar um item da Model/Collection/Consulta baseado no ID
     */
    public function indexAction()
    {
    }

    /**
     * Método responsável por retornar um item da Model/Collection/Consulta baseado no ID
     */
    public function getAction()
    {
    }

    /**
     * Método responsável por inserir dados na Model
     */
    public function createAction()
    {
    }

    /**
     * Método responsável por retornar atualizar os dados na Model
     */
    public function updateAction()
    {
    }

    /**
     * Método responsável por excluir os dados da Model/Collection/Consulta
     */
    public function deleteAction()
    {
    }

    /**
     * Método responsável por retornar todos os dados da Model/Collection/Consulta por página
     */
    public function pagingAction()
    {
    }

}

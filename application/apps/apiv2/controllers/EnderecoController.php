<?php
class ApiV2_EnderecoController extends Ead1_AppApi implements \Ead1\Api
{
    /**
     * @var \G2\Negocio\Endereco
     */
    private $negocio;

    public function init()
    {
        $this->negocio = new \G2\Negocio\Endereco();
        return parent::init();
    }

    public function indexAction()
    {
        // TODO: Implement getAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    public function getAction()
    {
        // TODO: Implement getAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     *
     * @SWG\Post(path="/endereco/create",
     *   tags={"endereço"},
     *   summary="Cria uma novo endereço.",
     *   description="Cria um novo endereço e vincula com a pessoa.",
     *   operationId="create",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     description="Objeto com os dados do endereço a ser inserido e do usuario a ser vinculado",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/Endereco")
     *   ),
     *   @SWG\Response(response=201,description="Operação efetuada com sucesso",
     *     @SWG\Schema(ref="#/definitions/Endereco")
     *  ),
     *   @SWG\Response(response=400, description="Dados inválidos para o cadastro",
     *     @SWG\Schema(ref="#/definitions/Error")
     *  ),
     * )))))
     *
     */
    public function createAction()
    {
        try {
            $params = Zend_Json::decode($this->getRequest()->getRawBody());
            $params['id_entidade'] = isset($params['id_entidade']) && $params['id_entidade'] ? $params['id_entidade'] : $this->getuser()->getentidade_id();
            $params['id_usuario'] = isset($params['id_usuario']) && $params['id_usuario'] ? $params['id_usuario'] : $this->getuser()->getuser_id();


            $endereco = $this->negocio->editarEnderecoPessoa($params);
            if ($endereco instanceof Ead1_Mensageiro && $endereco->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Exception($endereco->getText());
            }
            //verifica se retornou uma instancia da entity
            if ($endereco->getFirstMensagem() instanceof \G2\Entity\Endereco) {
                $this->getResponse()->setHttpResponseCode(201);

                $ng_pessoaendereco = new \G2\Negocio\PessoaEndereco();
                $result = $ng_pessoaendereco->retornaVwPessoaEnderecoOne(array(
                    'id_endereco' => $endereco->getFirstMensagem()->getId_endereco()
                ), true);
                $this->json($result);
            }
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }
    }


    /**
     *
     * @SWG\Post(path="/endereco/update",
     *   tags={"endereço"},
     *   summary="Altera umendereço.",
     *   description="Altera um endereço e vincula com a pessoa.",
     *   operationId="create",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     description="Objeto com os dados do endereço a ser alterado e do usuario a ser vinculado",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/Endereco")
     *   ),
     *   @SWG\Response(response=201,description="Operação efetuada com sucesso",
     *     @SWG\Schema(ref="#/definitions/Endereco")
     *  ),
     *   @SWG\Response(response=400, description="Dados inválidos para o cadastro",
     *     @SWG\Schema(ref="#/definitions/Error")
     *  ),
     * )))))
     *
     */
    public function updateAction()
    {
        $this->createAction();
    }

    public function deleteAction()
    {
        // TODO: Implement getAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    public function pagingAction()
    {
        // TODO: Implement pagingAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     * @SWG\Get(path="/endereco/vw-pessoa-endereco",
     *   tags={"endereco"},
     *   summary="Retorna um registro da tabela vw_pessoaendereco baseado no id_usuario e no id_tipoendereco",
     *   description="Você precisa estar logado para usar este método. Retorna um registro na tabela tb_pessoa baseado no id_usuario",
     *   operationId="dadosPessoaEndereco",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="id_usuario",
     *     in="query",
     *     description="ID do Usuário, não obrigatório, caso não informado pega o valor do token.",
     *     required=false,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="id_tipoendereco",
     *     in="query",
     *     description="Tipo do endereço",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso", @SWG\Schema(ref="#/definitions/VwPessoaEndereco")),
     *   @SWG\Response(response=400, description="ID inválido", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="Usuário não encontrado", @SWG\Schema(ref="#/definitions/Error"))
     * )))))
     *
     */
    public function vwPessoaEnderecoAction()
    {
        try {
            $id_usuario = $this->_getParam('id_usuario', $this->getuser()->getuser_id());
            if (!$id_usuario) {
                throw new \Exception("Você precisa informar o id do usuario");
            }

            $id_tipoendereco = $this->_getParam('id_tipoendereco', null);
            if (!$id_tipoendereco){
                throw new \Exception("Você precisa informar o tipo de endereco");
            }

            $negocioPessoa = new \G2\Negocio\PessoaEndereco();
            $pessoaEndereco = $negocioPessoa->retornaVwPessoaEnderecoOne(array(
                'id_usuario' => $id_usuario,
                'id_tipoendereco' => $id_tipoendereco,
                'bl_padrao' => 1
            ), false);

            if ($pessoaEndereco->getTipo() === Ead1_IMensageiro::ERRO) {
                throw new \Zend_Http_Exception("Nenhum dado encontrado!");
            }

            $this->json($negocioPessoa->toArrayEntity($pessoaEndereco->getFirstMensagem()));
        } catch (\Zend_Http_Exception $e) {
            $this->getResponse()->setHttpResponseCode(404);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 404);
            $this->json($error->toArray());
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }
    }

    /**
     * @SWG\Definition(
     *   definition="País",
     *   @SWG\Property(property="id_pais",type="integer"),
     *   @SWG\Property(property="st_nomepais",type="string")
     * )
     *
     * @SWG\Get(path="/endereco/paises",
     *   tags={"endereco"},
     *   summary="Retorna a lista de países",
     *   description="Necessário Login",
     *   operationId="paises",
     *   produces={"application/json"},
     *
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/Pais")
     *         )
     *   ),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="Nenhum registro encontrado", @SWG\Schema(ref="#/definitions/Error"))
     *
     * )))
     *
     */
    public function paisAction()
    {
        try {
            $negocio = new \G2\Negocio\Negocio();
            $pagination = $negocio->paging('\G2\Entity\Pais', array(), array('st_nomepais', 'ASC'), 1, 9999, false);
            if (!$pagination->gettotal()) {
                throw new \Zend_Http_Exception("Nenhum resultado encontrado!");
            }
            $this->json($pagination->getdata());
        } catch (\Zend_Http_Exception $e) {
            $this->getResponse()->setHttpResponseCode(404);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 404);
            $this->json($error->toArray());
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }
    }

    /**
     * @SWG\Definition(
     *   definition="Uf",
     *   @SWG\Property(property="sg_uf",type="string"),
     *   @SWG\Property(property="st_uf",type="string")
     * )
     *
     * @SWG\Get(path="/endereco/uf",
     *   tags={"endereco"},
     *   summary="Retorna a lista de uf",
     *   description="Necessário Login",
     *   operationId="uf",
     *   produces={"application/json"},
     *
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/Uf")
     *         )
     *   ),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="Nenhum registro encontrado", @SWG\Schema(ref="#/definitions/Error"))
     *
     * )))
     *
     */
    public function ufAction()
    {
        try {
            $negocio = new \G2\Negocio\Negocio();
            $pagination = $negocio->paging('\G2\Entity\Uf', array(), array('st_uf', 'ASC'), 1, 9999, false);
            if (!$pagination->gettotal()) {
                throw new \Zend_Http_Exception("Nenhum resultado encontrado!");
            }
            $this->json($pagination->getdata());
        } catch (\Zend_Http_Exception $e) {
            $this->getResponse()->setHttpResponseCode(404);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 404);
            $this->json($error->toArray());
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }
    }

    /**
     * @SWG\Definition(
     *   definition="Municipio",
     *   @SWG\Property(property="id_municipio",type="integer"),
     *   @SWG\Property(property="st_nomemunicipio",type="string")
     * )
     *
     * @SWG\Get(path="/endereco/municipio",
     *   tags={"endereco"},
     *   summary="Retorna a lista de municipios baseado no parametro sg_uf",
     *   description="Necessário Login",
     *   operationId="municipio",
     *   produces={"application/json"},
     *
     *   @SWG\Parameter(
     *     name="sg_uf",
     *     in="query",
     *     description="Sigla do UF com 2 caracteres",
     *     required=true,
     *     type="string",
     *     example="MG"
     *   ),
     *
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/Municipio")
     *         )
     *   ),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="Nenhum registro encontrado", @SWG\Schema(ref="#/definitions/Error"))
     *
     * )))
     *
     */
    public function municipioAction()
    {
        try {
            $sg_uf = $this->getParam('sg_uf');
            if (!$sg_uf) {
                throw new \Exception("Informe o UF");
            }
            $negocio = new \G2\Negocio\Negocio();
            $pagination = $negocio->paging('\G2\Entity\Municipio', array(
                'sg_uf' => $sg_uf
            ), array('st_nomemunicipio', 'ASC'), 1, 9999, false);
            if (!$pagination->gettotal()) {
                throw new \Zend_Http_Exception("Nenhum resultado encontrado!");
            }
            $this->json($pagination->getdata());
        } catch (\Zend_Http_Exception $e) {
            $this->getResponse()->setHttpResponseCode(404);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 404);
            $this->json($error->toArray());
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }
    }

    /**
     * @SWG\Get(path="/endereco/by-cep",
     *   tags={"endereco"},
     *   summary="Retorna um endereço baseado no CEP",
     *   description="Necessário Login",
     *   operationId="byCep",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="st_cep",
     *     in="query",
     *     description="String correspondente ao CEP",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso", @SWG\Schema(ref="#/definitions/VwPessoaEndereco")),
     *   @SWG\Response(response=400, description="ID inválido", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="CEP não encontrado ou inválido", @SWG\Schema(ref="#/definitions/Error"))
     * )))))
     *
     */
    public function byCepAction()
    {
        try {
            $st_cep = $this->getParam('st_cep');
            if (!$st_cep) {
                throw new \Exception("É necessário informar o CEP");
            }
            $negocio = new \G2\Negocio\Pessoa();
            $result = $negocio->getEnderecoBYCep($st_cep, true);
            if ($result instanceof Ead1_Mensageiro && $result->getTipo() == Ead1_IMensageiro::AVISO) {
                throw new \Zend_Http_Exception($result->getFirstMensagem());
            } else if (!$result) {
                throw new \Zend_Http_Exception("Nenhum resultado encontrado!");
            }
            // Retornar somente campos relativos ao CEP
            $result = array(
                'id_pais' => $result->getId_pais(),
                'st_nomepais' => $result->getSt_nomepais(),
                'st_cep' => $result->getSt_cep(),
                'sg_uf' => $result->getSg_uf(),
                'st_uf' => $result->getSt_uf(),
                'id_municipio' => $result->getId_municipio(),
                'st_nomemunicipio' => $result->getSt_nomemunicipio(),
                'st_bairro' => $result->getSt_bairro(),
                'st_endereco' => $result->getSt_endereco()
            );
            $this->json($result);
        } catch (\Zend_Http_Exception $e) {
            $this->getResponse()->setHttpResponseCode(404);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 404);
            $this->json($error->toArray());
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }
    }
}
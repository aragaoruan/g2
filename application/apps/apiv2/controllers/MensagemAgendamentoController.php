<?php

use Ead1\Api;

class ApiV2_MensagemAgendamentoController extends Ead1_AppApi implements Api
{

    /**
     * 
     *
     * @SWG\Definition(
     *   definition="VwEnviarMensagemAlunoResponse",
     *   allOf={
     *     @SWG\Schema(ref="#/definitions/VwEnviarMensagemAluno")
     *
     *   }
     *   ,@SWG\Property(property="group",type="string")
     * )
     * 
     * 
     * @SWG\Get(path="/mensagem-agendamento",
     *   tags={"mensagem-agendamento"},
     *   summary="Retorna os dados da Vw_EnviarMensagemAluno baseado no usuário que está conectado",
     *   description="Você precisa estar logado para usar este método. Ele não tem parâmetros.",
     *   operationId="getAll",
     *   produces={"application/json"},
     *
     *   @SWG\Parameter(
     *     name="id_evolucao",
     *     in="query",
     *     description="ID da Evolução das mensagens a serem recuperadas, opcional, quando quiser novas mensagens, envie 42.  Se quiser todas as mensagens, não informe este parâmetro.",
     *     required=false,
     *     type="integer",
     *     example="42"
     *   ),
     *   @SWG\Parameter(
     *     name="bl_entrega",
     *     in="query",
     *     description="Status da Mensagem, define se ja foi entregue ou não,  opcional. Quando quiser novas mensagens, envie 0. Se quiser todas as mensagens, não informe este parâmetro.",
     *     required=true,
     *     type="boolean",
     *     example="0"
     *   ),
     *
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso",
     *     @SWG\Schema(type="array",@SWG\Items(ref="#/definitions/VwEnviarMensagemAlunoResponse"))
     *   ),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error"))
     *
     * )))
     *
     *
     */
    public function indexAction()
    {

        try {

            $arg = array(
                "id_usuario" => $this->getuser()->getuser_id()
            );

            $id_evolucao = $this->getParam('id_evolucao', null);
            if($id_evolucao!==null)
                $arg['id_evolucao'] = $id_evolucao;

            $bl_entrega = $this->getParam('bl_entrega', null);
            if($bl_entrega!==null)
                $arg['bl_entrega'] = $bl_entrega;

            $mensagemNegocio = new \G2\Negocio\MensagemPortal;
            $mensagens = $mensagemNegocio->findVwEnviarMensagemAluno($arg, true);

            if ($mensagens) {
                $arrIdEnviomensagens = array_unique(array_column($mensagens, 'id_enviomensagem'));
                $mensagemNegocio->updateEnvioDestinatarioEntregueEmLote($arrIdEnviomensagens);

                    foreach ($mensagens as $chave => $mensagem){
                        $date = explode(" ",$mensagem['dt_cadastromsg']);
                        $mensagens[$chave]['group']     = substr($date[0],0,7).'-01';
                    }

                $this->json($mensagens);
            } else {
                $this->json(array());
            }



        } catch(\Exception $e){

            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR,$e->getMessage(),400);
            $this->json($error->toArray());

        }

    }

    /**
     *
     *
     * @SWG\Get(path="/mensagem-agendamento/get",
     *   tags={"mensagem-agendamento"},
     *   summary="Retorna os dados da Vw_EnviarMensagemAluno baseado no id_enviomensagem",
     *   description="Você precisa estar logado para usar este método.",
     *   operationId="get",
     *   produces={"application/json"},
     *
     *   @SWG\Parameter(
     *     name="id_enviomensagem",
     *     in="query",
     *     description="ID do envio da mensagem.",
     *     required=true,
     *     type="boolean",
     *     example="0"
     *   ),
     *   @SWG\Parameter(
     *     name="id_matricula",
     *     in="query",
     *     description="ID da Matrícula.",
     *     required=true,
     *     type="boolean",
     *     example="0"
     *   ),
     *
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso",
     *     @SWG\Schema(ref="#/definitions/VwEnviarMensagemAluno")
     *   ),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error"))
     *
     * )))
     *
     */
    public function getAction()
    {
        try {

            $arg = array(
                "id_usuario" => $this->getuser()->getuser_id()
            );

            $id_enviomensagem = $this->getParam('id_enviomensagem', null);
            if(!$id_enviomensagem)
                throw new \Exception('Você precisa informar o id_enviomensagem');

            $id_matricula = $this->getParam('id_matricula', null);
            if(!$id_matricula)
                throw new \Exception('Você precisa informar o id_matricula');

                $arg['id_matricula'] = $id_matricula;
                $arg['id_enviomensagem'] = $id_enviomensagem;

            $mensagemNegocio = new \G2\Negocio\MensagemPortal;
            $mensagens = $mensagemNegocio->findVwEnviarMensagemAluno($arg, true);

            if ($mensagens) {
                $this->json($mensagens[0]);
            } else {
                $this->json(array());
            }

        } catch(\Exception $e){

            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR,$e->getMessage(),400);
            $this->json($error->toArray());

        }
    }

    public function createAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING,"Método ainda não implementado",404);
        $this->_helper->json($error->toArray());
    }


    /**
     *
     * @SWG\Definition(
     *     definition="MensagemAgendamentoUpdateEvolucaoResponse",
     *     @SWG\Property(property="id_enviomensagem",type="integer"),
     *     @SWG\Property(property="id_matricula",type="integer"),
     *     @SWG\Property(property="id_evolucao",type="integer")
     * ),
     *
     * @SWG\Put(path="/mensagem-agendamento/update-evolucao",
     *   tags={"mensagem-agendamento"},
     *   summary="Atualiza a evolução da Mensagem Agendamento",
     *   description="Você precisa estar logado para usar este método.",
     *   operationId="update-evolucao",
     *   produces={"application/json"},
     *
     *   @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Dados para Atualização",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/MensagemAgendamentoUpdateEvolucaoResponse")
     *   ),
     *
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso"),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error"))
     *
     * )))
     *
     *
     */
    public function updateEvolucaoAction()
    {

        try {
            $params = $data = Zend_Json::decode($this->getRequest()->getRawBody());

            if(!isset($params['id_enviomensagem']) || !$params['id_enviomensagem'])
                throw new \Exception('O parãmetro id_enviomensagem é obrigatório');

            if(!isset($params['id_matricula']) || !$params['id_matricula'])
                throw new \Exception('O parãmetro id_matricula é obrigatório');

            if(!isset($params['id_evolucao']) || !$params['id_evolucao'])
                throw new \Exception('O parãmetro id_evolucao é obrigatório');


            $mensagemNegocio = new \G2\Negocio\MensagemPortal();
            if (!$mensagemNegocio->atualizarMensagem($params)) {
                throw new \Exception("Não foi possível atualizar a Mensagem");
            }

        } catch(\Exception $e){

            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR,$e->getMessage(),400);
            $this->json($error->toArray());

        }

    }

    public function deleteAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING,"Método ainda não implementado",404);
        $this->_helper->json($error->toArray());
    }

    public function pagingAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING,"Método ainda não implementado",404);
        $this->_helper->json($error->toArray());
    }

    public function headAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING,"Método ainda não implementado",404);
        $this->_helper->json($error->toArray());
    }

    public function updateAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING,"Método ainda não implementado",404);
        $this->_helper->json($error->toArray());
    }


}
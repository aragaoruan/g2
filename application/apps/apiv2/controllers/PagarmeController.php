<?php

/**
 * Class AppApi_BoletoController
 * Classe controller para gerar o boleto
 */
class ApiV2_PagarmeController extends \Zend_Controller_Action
{


    public function init()
    {
        $this->_helper->viewRenderer->setNoRender();
    }

    /**
     *
     *
     * @SWG\Get(path="/pagarme/post-back/id/{id}/origem/{origem}",
     *   tags={"pagarme"},
     *   summary="Recebe a requisição da Pagar.me",
     *   description="Processa uma requisição da Pagar.me ou retorna erro caso não encontre",
     *   operationId="post-back",
     *   produces={"application/json"},
     *
     *     @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Id da operação",
     *     required=true,
     *     type="integer"
     *   ),
     *     @SWG\Parameter(
     *     name="origem",
     *     in="path",
     *     description="Origem da Venda, pode ser G1 ou G2",
     *     required=true,
     *     type="string"
     *   ),
     *
     *   @SWG\Response(response="default", description="Operação efetuada com sucesso"),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=200, description="Nenhum registro encontrado", @SWG\Schema(ref="#/definitions/Error"))
     *
     * )))
     *
     *
     */
    public function subscriptionPostBackAction()
    {
        try {


            $this->_getParam('subscription', null);

            $lanTO = new LancamentoTO();
//            $lanTO->set


            $vendaBO = new VendaBO();
            $mevenda = $vendaBO->quitarCredito(array($lanTO));

//            $bo = new \G2\Negocio\Financeiro();
//            $bo->arrayToEntity()





//            $id = $this->getParam('id');
//            //verifica se o parametro id foi passado
//            if (!$id) {
//                throw new \Exception("Parametro ID não informado.");
//            }
//
//            //gera o boleto
//            $res = $this->negocio->gerarPdfBoleto($id);
//
//            //verifica se conseguiu gerar o boleto
//            if ($res->getType() != Ead1_IMensageiro::TYPE_SUCESSO) {
//                throw new \Exception('Arquivo PDF não pode ser gerado. ' . $res->getText());
//            }
//
//            //seta a resposta
//            $this->getResponse()->appendBody(json_encode(array(
//                'path' => $res->getFirstMensagem(),
//                'host' => \Ead1_Ambiente::geral()->st_url_gestor2
//            )));


        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }

    }

}
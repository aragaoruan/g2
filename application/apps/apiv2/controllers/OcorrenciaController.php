<?php


/**
 * Class AppApi_OcorrenciaController
 */
class ApiV2_OcorrenciaController extends Ead1_AppApi implements \Ead1\Api
{
    /**
     * @var \G2\Negocio\Ocorrencia
     */
    private $negocio;

    public function init()
    {
        $this->negocio = new \G2\Negocio\Ocorrencia();
        return parent::init();
    }

    /**
     *
     * @SWG\Definition(
     *   definition="VwOcorrenciaResponse",
     *   allOf={
     *     @SWG\Schema(ref="#/definitions/VwOcorrencia")
     *
     *   }
     *   ,@SWG\Property(property="group",type="string")
     * )
     *
     *
     *
     * @SWG\Get(path="/ocorrencia",
     *   tags={"ocorrencia"},
     *   summary="Retorna todas as ocorrências",
     *   description="Você precisa estar logado para usar este método.",
     *   operationId="getAll",
     *   produces={"application/json"},
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/VwOcorrencia")
     *         )
     *   ),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     *
     * )))
     *
     */
    public function indexAction()
    {

        try {


            if (!$this->getuser()->getentidade_id())
                throw new \Exception("Token inválido, user_id ou entidade_id não informado.");

            $negocio = new \G2\Negocio\Ocorrencia();
            $ocorrencias = $negocio->getOcorrenciasAlunos(
                $this->getuser()->getuser_id(),
                $this->getuser()->getentidade_id()
            );
            $this->json($ocorrencias);

        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }

    }


    /**
     *
     * @SWG\Post(path="/ocorrencia/create",
     *   tags={"ocorrencia"},
     *   summary="Cria uma nova ocorrencia.",
     *   description="Cria uma nova ocorrencia.",
     *   operationId="create",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     description="Objeto com os dados da ocorrencia a serem inseridos",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/Ocorrencia")
     *   ),
     *   @SWG\Response(response=201,description="Operação efetuada com sucesso",
     *     @SWG\Schema(ref="#/definitions/Ocorrencia")
     *  ),
     *   @SWG\Response(response=400, description="Dados inválidos para o cadastro",
     *     @SWG\Schema(ref="#/definitions/Error")
     *  ),
     * )))))
     *
     */
    public function createAction()
    {
        try {

            $params = $this->getAllParams();
            $body = $this->getRequest()->getRawBody();
            $data = Zend_Json::decode($body);

            if (is_array($data)) {
                $params = array_merge($params, $data);
            }

            unset($params['module'], $params['controller'], $params['action']);

            $matriculaNegocio = new \G2\Negocio\Matricula();
            $primeiroAcessoNegocio = new \G2\Negocio\PrimeiroAcesso();

            $arquivo = null;
            if (isset($_FILES['files']['tmp_name']) && $_FILES['files']['tmp_name'] != '') {
                $arquivo = $_FILES['files'];
            }

            //verifica se foi enviada matricula e pesquisa o id_entidadematricula para vincular a ocorrencia a entidade
            if (isset($params['id_matricula']) && !empty($params['id_matricula'])) {
                $matricula = $matriculaNegocio->retornaMatricula(array(
                    'id_matricula' => $params['id_matricula']),
                    true
                );
                $params['id_entidade'] = $matricula['id_entidadematricula'];
            }

            /**
             * Caso seja uma ocorrencia da tela de primeiro acesso, entra e pesquisa pela entidade os dados cadastrados
             * para ocorrencia, setando os dados na variavel para salvar.
             */
            if (isset($params['bl_primeiroacesso']) && !empty($params['bl_primeiroacesso'])) {
                unset($params['bl_primeiroacesso']);
                $paDadosCadastrais = $primeiroAcessoNegocio->findPADadosCadastrais(array(
                    'id_entidade' => $params['id_entidade']
                ), true);

                $idCategoria = $paDadosCadastrais['id_categoriaocorrencia']['id_categoriaocorrencia'];
                $params['st_titulo'] = 'Primeiro Acesso - Alterações de dados no Primeiro Acesso';
                $params['id_textomensagem'] = $paDadosCadastrais['id_textomensagem']['id_textosistema'];
                $params['id_assuntoco'] = $paDadosCadastrais['id_assuntoco']['id_assuntoco'];
                $params['id_categoriaocorrencia'] = $idCategoria;
                $params['id_evolucao'] = \G2\Constante\Evolucao::AGUARDANDO_ATENDIMENTO;
                $params['id_situacao'] = \G2\Constante\Situacao::TB_OCORRENCIA_PENDENTE;
            }

            //Verifica se não foi passado o valores.
            if (!array_key_exists('id_usuariocadastro', $params) || empty($params['id_usuariocadastro'])) {
                $params['id_usuariocadastro'] = $this->getuser()->getuser_id();
            }
            if (!array_key_exists('id_usuariointeressado', $params) || empty($params['id_usuariointeressado'])) {
                $params['id_usuariointeressado'] = $this->getuser()->getuser_id();
            }
            if (!array_key_exists('id_entidade', $params) || empty($params['id_entidade'])) {
                $params['id_entidade'] = $this->getuser()->getentidade_id();
            }

            /*
             * Verifica se o id_categoriaocorrencia não foi informado, caso isso aconteça, o sistema deverá recuperar
             * o primeiro registro na base de dados e setar como valor para a ocorrencia.
             * Esta regra foi definida em conversa no dia 03/08/2016 em particular com Felipe Toffolo
             */
            if (!array_key_exists('id_categoriaocorrencia', $params)
                || empty($params['id_categoriaocorrencia'])
            ) {
                $categoriaDefault = $this->negocio->getFirstCategoriaOcorrencia(array(
                    'id_entidadecadastro' => $params['id_entidade'],
                    'bl_ativo' => true,
                    'id_tipoocorrencia' => \G2\Constante\TipoOcorrencia::ALUNO
                ));

                if ($categoriaDefault) {
                    $params['id_categoriaocorrencia'] = $categoriaDefault->getid_categoriaocorrencia();
                }
            }

            $ocorrencia = $this->negocio->salvarOcorrencia(array('ocorrencia' => $params, 'arquivo' => $arquivo));
            if (($ocorrencia instanceof Ead1_Mensageiro)
                && $ocorrencia->getTipo() != Ead1_IMensageiro::SUCESSO
            ) {
                throw new Exception($ocorrencia->getText());
            }
            //verifica se retornou uma instancia da entity de ocorrencia
            if ($ocorrencia->getFirstMensagem() instanceof \G2\Entity\Ocorrencia) {
                $this->getResponse()->setHttpResponseCode(201);
                $this->json($this->negocio->toArrayEntity($ocorrencia->getFirstMensagem()));
            }

        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }
    }


    /**
     * @SWG\Post(path="/ocorrencia/encerrar",
     *   tags={"ocorrencia"},
     *   summary="Encerra uma ocorrência",
     *   description="Você precisa estar logado para usar este método.",
     *   operationId="encerrar",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="query",
     *     name="id_ocorrencia",
     *     description="ID da ocorrência",
     *     required=true,
     *     type="integer"
     *   ),
     *
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso",
     *      @SWG\Schema(ref="#/definitions/VwOcorrenciaResponse")
     * ),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     *
     * )))
     *
     */
    public function encerrarAction()
    {

        try {
            $params = Zend_Json::decode($this->getRequest()->getRawBody());
            if (!isset($params['id_ocorrencia']) || !$params['id_ocorrencia'])
                throw new \Exception("Informe a Ocorrência.");

//            $result = $this->negocio
//                ->solicitarEncerramento(array('id_ocorrencia' => $params['id_ocorrencia']), $this->getuser());
            $to = new OcorrenciaTO();
            $to->setId_ocorrencia($params['id_ocorrencia']);
            $to->setId_evolucao(\G2\Constante\Evolucao::ENCERRAMENTO_REALIZADO_INTERESSADO);
            $to->setId_situacao(\G2\Constante\Situacao::TB_OCORRENCIA_ENCERRADA);


            $bo = new PortalCAtencaoBO();
            $result = $bo->alterarOcorrencia($to, null, $this->getuser(), false);

            if ($result->getType() == Ead1_IMensageiro::TYPE_SUCESSO) {
                $ocorrencias = $this->negocio
                    ->getOcorrenciasAlunos(
                        $this->getuser()->getuser_id(),
                        $this->getuser()->getentidade_id(),
                        $to->getId_ocorrencia()
                    );
                $this->json($ocorrencias[0]);
            } else {
                throw new \Exception('Erro ao tentar solicitar encerramento da ocorrencia.');
            }

        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }

    }

    /**
     * @SWG\Post(path="/ocorrencia/reabrir",
     *   tags={"ocorrencia"},
     *   summary="Reabre uma ocorrência",
     *   description="Você precisa estar logado para usar este método.",
     *   operationId="reabrir",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="query",
     *     name="id_ocorrencia",
     *     description="ID da ocorrência",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso",
     *     @SWG\Schema(ref="#/definitions/VwOcorrenciaResponse")),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     *
     * )))
     *
     */
    public function reabrirAction()
    {

        try {

            $params = Zend_Json::decode($this->getRequest()->getRawBody());
            if (!isset($params['id_ocorrencia']) || !$params['id_ocorrencia'])
                throw new \Exception("Informe a Ocorrência.");

            $result = $this->negocio->reabrirOcorrencia(array('id_ocorrencia' => $params['id_ocorrencia']));
            if ($result instanceof \G2\Entity\VwOcorrenciasPessoa) {
                $ocorrencias = $this->negocio->getOcorrenciasAlunos(
                    $this->getuser()->getuser_id(),
                    $this->getuser()->getentidade_id(),
                    $result->getId_ocorrencia()
                );
                $this->json($ocorrencias[0]);
            } else {
                throw new \Exception('Erro ao tentar solicitar encerramento da ocorrencia.');
            }

        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }

    }

    /**
     * @SWG\Get(path="/ocorrencia/categorias",
     *   tags={"ocorrencia"},
     *   summary="Lista as categorias de uma ocorrencia",
     *   description="Lista todas as categorias para a criação de uma ocorrencia.",
     *   operationId="categorias",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="query",
     *     name="id_tipoocorrencia",
     *     description="ID do tipo da ocorrencia, define se é um aluno ou professor, se não passado valor padrão assume 1",
     *     required=false,
     *     type="integer"
     *   ),
     *     @SWG\Parameter(
     *     in="query",
     *     name="id_entidadecadastro",
     *     description="ID da entidade referente as categorias, se não informado padrão é definido com valor da sesssao.",
     *     required=false,
     *     type="integer"
     *   ),
     *    @SWG\Response(response="default",description="Operação efetuada com sucesso",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/CategoriaOcorrencia")
     *         )
     *   ),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     *
     * )))
     *
     */
    public function categoriasAction()
    {

        try {

            $arrReturn = [];

            $id_entidadecadastro = $this
                ->getParam('id_entidadecadastro', $this->getuser()->getentidade_id());
            $id_tipoocorrencia = $this
                ->getParam('id_tipoocorrencia', 1);

            $result = $this->negocio->getCategoriaOcorrencia(array(
                'id_entidadecadastro' => $id_entidadecadastro
            , 'bl_ativo' => true
            , 'id_tipoocorrencia' => $id_tipoocorrencia
            ));


            if ($result) {
                foreach ($result as $item) {
                    $arrReturn[] = array(
                        'id_categoriaocorrencia' => $item->getid_categoriaocorrencia(),
                        'st_categoriaocorrencia' => $item->getst_categoriaocorrencia()
                    );
                }
            }

            $this->json($arrReturn);

        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }

    }

    /**
     *
     *
     * @SWG\Post(path="/ocorrencia/assuntos",
     *   tags={"ocorrencia"},
     *   summary="Retorna os assuntos",
     *   description="Você precisa estar logado para usar este método.",
     *   operationId="categorias",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="query"
     *     name="id_entidade",
     *     description="ID da Entidade, caso não informada, o token informará a entidade",
     *     required=false,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     in="query"
     *     name="id_assuntocopai",
     *     description="ID do assunto pai caso necessário padrão null",
     *     required=false,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     in="query"
     *     name="bl_abertura",
     *     description="Boolean 0/1 padrão 1",
     *     required=false,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     in="query"
     *     name="id_tipoocorrencia",
     *     description="ID do tipo da ocorrência, caso não informada o tipo 1 será retornado",
     *     required=false,
     *     type="integer"
     *   ),
     *      @SWG\Parameter(
     *     in="query"
     *     name="id_situacao",
     *     description="ID da situação, caso não informada, valor padrão será 91 correspondente a situação ativa.",
     *     required=false,
     *     type="integer"
     *   ),
     *     @SWG\Parameter(
     *     in="query"
     *     name="bl_interno",
     *     description="Parâmetro que define se o assunto não será visível pelo aluno, se não informado o parametro
     * é desconsiderado",
     *     required=false,
     *     type="boolean"
     *   ),
     *
     *
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/VwAssuntos")
     *         )
     *   ),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     *
     * )))
     *
     */
    public function assuntosAction()
    {

        try {

            $id_entidade = $this->getParam('id_entidade', $this->getuser()->getentidade_id());
            $id_assuntocopai = $this->getParam('id_assuntocopai', null);
            $bl_abertura = $this->getParam('bl_abertura', true);
            $id_tipoocorrencia = $this->getParam('id_tipoocorrencia', \G2\Constante\TipoOcorrencia::ALUNO);
            $id_situacao = $this
                ->getParam('id_situacao', \G2\Constante\Situacao::TB_ASSUNTOCO_ATIVO);
            $bl_interno = $this->getParam('bl_interno', null);

            $params = array(
                'bl_ativo' => true
            , 'id_entidade' => $id_entidade
            , 'bl_abertura' => $bl_abertura
            , 'id_tipoocorrencia' => $id_tipoocorrencia
            , 'id_assuntocopai' => $id_assuntocopai
            , 'id_situacao' => $id_situacao
            , "bl_interno" => $bl_interno
            );


            if (is_null($bl_interno)) {
                unset($params["bl_interno"]);
            }

            $arrReturn = $this->negocio->getAssuntosOcorrencia($params);

            $this->json($arrReturn);

        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }

    }


    /**
     * Método responsável por retornar um item da Model/Collection/Consulta baseado no ID
     */
    public function getAction()
    {

        try {

            $id_ocorrencia = $this->getParam('id', false);
            if (!$id_ocorrencia)
                throw new \Exception("Informe o ID da Ocorrência.");

            $ocorrencias = $this->negocio
                ->getOcorrenciasAlunos(
                    $this->getuser()->getuser_id(),
                    $this->getuser()->getentidade_id(),
                    $id_ocorrencia
                );

            if (!$ocorrencias) {
                throw new \Zend_Http_Exception("Ocorrência não encontrada");
            }
            $this->json($ocorrencias[0]);

        } catch (\Zend_Http_Exception $e) {

            $this->getResponse()->setHttpResponseCode(404);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 404);
            $this->json($error->toArray());

        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }

    }


    /**
     * Método responsável por retornar atualizar os dados na Model
     */
    public function updateAction()
    {
        // TODO: Implement updateAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     * Método responsável por excluir os dados da Model/Collection/Consulta
     */
    public function deleteAction()
    {
        // TODO: Implement deleteAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     * Método responsável por retornar todos os dados da Model/Collection/Consulta por página
     */
    public function pagingAction()
    {
        // TODO: Implement pagingAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }


}
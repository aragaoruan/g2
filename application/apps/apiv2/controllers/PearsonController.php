<?php

class ApiV2_PearsonController extends Ead1_AppApi implements \Ead1\Api
{
    /**
     * @SWG\Definition(
     *   definition="IntegracaoPearson",
     *   @SWG\Property(property="st_caminho",type="string"),
     *   @SWG\Property(property="st_token",type="string"),
     *   @SWG\Property(property="id_usuario",type="integer"),
     *   @SWG\Property(property="st_usuario",type="string"),
     *   @SWG\Property(property="st_login",type="string"),
     * )
     *
     * @SWG\Get(path="/pearson/get",
     *   tags={"pearson"},
     *   summary="Retorna a integração de um usuario/entidade com a biblioteca virtual Pearson",
     *   description="Necessário Login. Os dados id_usuario e id_entidade são buscados na matricula passada por parametro.",
     *   operationId="get",
     *   produces={"application/json"},
     *
     *   @SWG\Parameter(
     *     in="query",
     *     name="id_matricula",
     *     description="Id da Matrícula",
     *     required=true,
     *     type="integer",
     *     example="0"
     *   ),
     *
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso",
     *     @SWG\Schema(ref="#/definitions/IntegracaoPearson")
     *   ),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error"))
     *
     * )))
     *
     */
    public function getAction()
    {
        try {
            $id_matricula = $this->getParam('id_matricula', false);

            if (!$id_matricula) {
                throw new \Exception("Informe a Matrícula");
            }

            $negocio = new \G2\Negocio\EntidadeIntegracao();

            $en_matricula = $negocio->find('\G2\Entity\Matricula', $id_matricula);

            if (!$en_matricula) {
                throw new \Exception("Matrícula não encontrada");
            }

            $integracao = $negocio->retornarIntegracaoPearson($en_matricula->getId_entidadematricula(), $en_matricula->getId_usuario()->getId_usuario());

            if (!$integracao) {
                throw new \Exception('A integração com a biblioteca virtual não está configurada.');
            }

            $integracao['st_login'] = md5($integracao['st_usuario'] . $integracao['st_token']);

            $this->json($integracao);
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }
    }

    /**
     * Método responsável por inserir dados na Model
     */
    public function createAction()
    {
        // TODO: Implement createAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }


    /**
     * Método responsável por retornar todos os dados da Model/Collection/Consulta
     */
    public function indexAction()
    {
        // TODO: Implement indexAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     * Método responsável por retornar atualizar os dados na Model
     */
    public function updateAction()
    {
        // TODO: Implement updateAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     * Método responsável por excluir os dados da Model/Collection/Consulta
     */
    public function deleteAction()
    {
        // TODO: Implement deleteAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     * Método responsável por retornar todos os dados da Model/Collection/Consulta por página
     */
    public function pagingAction()
    {
        // TODO: Implement pagingAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

}
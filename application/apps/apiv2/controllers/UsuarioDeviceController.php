<?php

class ApiV2_UsuarioDeviceController extends Ead1_AppApi
{

    /**
     *
     * @SWG\Post(path="/usuario-device/create",
     *   tags={"usuario-device"},
     *   summary="Insere um novo registro na tabela tb_usuariodevice",
     *   description="Você precisa estar logado para usar este método. Faz o insert dos dados na tabela tb_usuariodevice. O campo id_usuariodevice é ignorado",
     *   operationId="create",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     description="Objeto com os dados do UsuarioDevice a serem inseridos",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/UsuarioDevice")
     *   ),
     *   @SWG\Response(response=201,description="Operação efetuada com sucesso", @SWG\Schema(ref="#/definitions/UsuarioDevice")),
     *   @SWG\Response(response=400, description="Dados inválidos para o cadastro", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="Usuário não encontrado", @SWG\Schema(ref="#/definitions/Error"))
     * )))))
     *
     */
    public function createAction()
    {

        try {

            $params = Zend_Json::decode($this->getRequest()->getRawBody());
            if (!isset($params['st_key']) || !$params['st_key'])
                throw new \Exception("Informe o parâmetro st_key");

            if (!isset($params['st_infodevice']) || !$params['st_infodevice'])
                throw new \Exception("Informe o parâmetro st_infodevice");


            $negocio = new \G2\Negocio\Negocio();
            $ref = $negocio->getReference('\G2\Entity\Usuario', $this->getuser()->getuser_id());
            $entity = new \G2\Entity\UsuarioDevice();
            $entity->setId_usuario($ref);
            $entity->setSt_key($params['st_key']);
            $entity->setSt_infodevice($params['st_infodevice']);
            $entity->setDt_cadastro(new DateTime());
            $save = $negocio->save($entity);
            $result = $negocio->toArrayEntity($save);
            $this->json($result);

        } catch (\Zend_Http_Exception $e) {
            $this->getResponse()->setHttpResponseCode(404);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 404);
            $this->json($error->toArray());
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }

    }

    /**
     *
     * @SWG\Delete(path="/usuario-device/delete/id/{st_key}",
     *   tags={"usuario-device"},
     *   summary="Exclui um registroo da tabela tb_usuariodevice baseado no id_usuario",
     *   description="Você precisa estar logado para usar este método. Faz a exclusão do Usuário da base de dados ou o torna inativo",
     *   operationId="delete",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="st_key",
     *     in="path",
     *     description="Chave do dispositivo",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response="204", description="Operação efetuada com sucesso"),
     *   @SWG\Response(response=400, description="ID inválido", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="Usuário não encontrado", @SWG\Schema(ref="#/definitions/Error"))
     * ))))
     *
     */
    public function deleteAction()
    {

        try {

            $st_key = $this->_getParam('id', null);
            if (!$st_key)
                throw new \Exception("Você precisa informar a Chave");

            $where = array(
                'st_key' => $st_key,
                'id_usuario' => $this->getuser()->getuser_id()
            );

            $negocio = new \G2\Negocio\Negocio();
            $result = $negocio->findOneBy('\G2\Entity\UsuarioDevice', $where);

            if ($result) {
                $negocio->delete($result);
            }
            $this->getResponse()->setHttpResponseCode(204)->appendBody('Registro removido');
        } catch (\Zend_Http_Exception $e) {
            $this->getResponse()->setHttpResponseCode(404);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 404);
            $this->json($error->toArray());
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }

    }


}
<?php

class ApiV2_GradeNotaController extends Ead1_AppApi implements \Ead1\Api
{

    /**
     *
     * @SWG\Definition(
     *     definition="VwGradeNotaDisciplinasMoodleGradesResponse",
     *     @SWG\Property(property="dategraded", type="integer"),
     *     @SWG\Property(property="datesubmitted", type="string"),
     *     @SWG\Property(property="feedback", type="integer"),
     *     @SWG\Property(property="feedbackformat", type="integer"),
     *     @SWG\Property(property="grade", type="integer"),
     *     @SWG\Property(property="locked", type="integer"),
     *     @SWG\Property(property="hidden", type="integer"),
     *     @SWG\Property(property="locked", type="integer"),
     *     @SWG\Property(property="overridden", type="integer"),
     *     @SWG\Property(property="str_feedback", type="string"),
     *     @SWG\Property(property="str_grade", type="string"),
     *     @SWG\Property(property="str_long_grade", type="string"),
     *     @SWG\Property(property="userid", type="integer"),
     *     @SWG\Property(property="usermodified", type="integer")
     * ),
     * @SWG\Definition(
     *     definition="VwGradeNotaDisciplinasMoodleResponse",
     *     @SWG\Property(property="activityid", type="string"),
     *     @SWG\Property(property="grademax", type="integer"),
     *     @SWG\Property(property="grademin", type="integer"),
     *     @SWG\Property(property="gradepass", type="integer"),
     *     @SWG\Property(property="hidden", type="integer"),
     *     @SWG\Property(property="itemnumber", type="integer"),
     *     @SWG\Property(property="locked", type="integer"),
     *     @SWG\Property(property="name", type="string"),
     *     @SWG\Property(property="scaleid", type="integer"),
     *     @SWG\Property(property="grades", type="array", @SWG\Items(ref="#/definitions/VwGradeNotaDisciplinasMoodleGradesResponse"))
     * ),
     *
     * @SWG\Definition(
     *      definition="VwGradeNotaDisciplinasResponse",
     *      @SWG\Property(property="ar_notasmoodle", type="array", @SWG\Items(ref="#/definitions/VwGradeNotaDisciplinasMoodleResponse")),
     *      @SWG\Property(property="ar_notasmoodle_prr", type="array", @SWG\Items(ref="#/definitions/VwGradeNotaDisciplinasMoodleResponse")),
     *      @SWG\Property(property="dt_inicio", type="string"),
     *      @SWG\Property(property="dt_abertura", type="string"),
     *      @SWG\Property(property="dt_encerramento", type="string"),
     *      @SWG\Property(property="dt_encerramentoextensao", type="string"),
     *      @SWG\Property(property="dt_cadastroaproveitamento", type="string"),
     *      @SWG\Property(property="dt_defesatcc", type="string"),
     *      @SWG\Property(property="id_saladeaula", type="integer"),
     *      @SWG\Property(property="st_saladeaula", type="string"),
     *      @SWG\Property(property="id_disciplina", type="integer"),
     *      @SWG\Property(property="id_alocacao", type="integer"),
     *      @SWG\Property(property="id_projetopedagogico", type="integer"),
     *      @SWG\Property(property="st_projetopedagogico", type="string"),
     *      @SWG\Property(property="st_notatcc", type="string"),
     *      @SWG\Property(property="nu_notafaltante", type="float"),
     *      @SWG\Property(property="nu_notamaxima", type="float"),
     *      @SWG\Property(property="nu_percentualaprovacao", type="float"),
     *      @SWG\Property(property="st_notaead", type="string"),
     *      @SWG\Property(property="st_notafinal", type="string"),
     *      @SWG\Property(property="st_disciplina", type="string"),
     *      @SWG\Property(property="id_tipodisciplina", type="integer"),
     *      @SWG\Property(property="st_tituloexibicaodisciplina", type="string"),
     *      @SWG\Property(property="id_matricula", type="integer"),
     *      @SWG\Property(property="nu_notafinal", type="float"),
     *      @SWG\Property(property="id_evolucao", type="integer"),
     *      @SWG\Property(property="st_evolucao", type="string"),
     *      @SWG\Property(property="id_situacao", type="integer"),
     *      @SWG\Property(property="st_situacao", type="string"),
     *      @SWG\Property(property="nu_cargahoraria", type="float"),
     *      @SWG\Property(property="nu_notatotal", type="float"),
     *      @SWG\Property(property="id_encerramentosala", type="integer"),
     *      @SWG\Property(property="st_status", type="string"),
     *      @SWG\Property(property="bl_status", type="boolean"),
     *      @SWG\Property(property="id_matriculadisciplina", type="integer"),
     *      @SWG\Property(property="id_avaliacaoconjuntoreferencia", type="integer"),
     *      @SWG\Property(property="id_tiponotatcc", type="integer"),
     *      @SWG\Property(property="id_tiponotafinal", type="integer"),
     *      @SWG\Property(property="id_tiponotaead", type="integer"),
     *      @SWG\Property(property="id_tiponotarecuperacao", type="integer"),
     *      @SWG\Property(property="bl_complementar", type="boolean"),
     *      @SWG\Property(property="st_notaatividade", type="string"),
     *      @SWG\Property(property="st_notarecuperacao", type="string"),
     *      @SWG\Property(property="st_nomeusuarioaproveitamento", type="string"),
     *      @SWG\Property(property="id_modulo", type="integer"),
     *      @SWG\Property(property="st_modulo", type="string"),
     *      @SWG\Property(property="id_categoriasala", type="integer"),
     *      @SWG\Property(property="st_statusdisciplina", type="string"),
     *      @SWG\Property(property="st_avaliacaoatividade", type="string"),
     *      @SWG\Property(property="st_avaliacaoead", type="string"),
     *      @SWG\Property(property="st_avaliacaofinal", type="string"),
     *      @SWG\Property(property="st_avaliacaorecuperacao", type="string"),
     *      @SWG\Property(property="id_aproveitamento", type="integer"),
     *      @SWG\Property(property="st_disciplinaoriginal", type="string"),
     *      @SWG\Property(property="st_status_prr", type="string"),
     *      @SWG\Property(property="st_notaead_prr", type="string"),
     *      @SWG\Property(property="st_notatcc_prr", type="string"),
     *      @SWG\Property(property="st_saladeaula_prr", type="string"),
     *      @SWG\Property(property="nu_notafinal_prr", type="float"),
     *      @SWG\Property(property="nu_cargahoraria_prr", type="string"),
     *      @SWG\Property(property="id_disciplina_prr", type="integer"),
     *      @SWG\Property(property="st_disciplina_prr", type="string"),
     *      @SWG\Property(property="id_matriculadisciplina_prr", type="integer"),
     *      @SWG\Property(property="dt_inicio_prr", type="string"),
     *      @SWG\Property(property="dt_abertura_prr", type="string"),
     *      @SWG\Property(property="dt_encerramento_prr", type="string"),
     *      @SWG\Property(property="id_tiponotatcc_prr", type="integer"),
     *      @SWG\Property(property="id_tiponotafinal_prr", type="integer"),
     *      @SWG\Property(property="id_tiponotaead_prr", type="integer"),
     *      @SWG\Property(property="id_tiponotarecuperacao_prr", type="integer")
     * ),
     * @SWG\Definition(
     *     definition="VwGradeNotaResponse",
     *     @SWG\Property(property="id_modulo", type="integer"),
     *     @SWG\Property(property="st_modulo", type="string"),
     *     @SWG\Property(property="disciplinas", type="array", @SWG\Items(ref="#/definitions/VwGradeNotaDisciplinasResponse"))
     * ),
     */


    /**
     * @SWG\Get(path="/grade-nota",
     *   tags={"grade-nota"},
     *   summary="Retorna os dados da Grade de Notas vw_gradenota",
     *   description="Você precisa estar logado para usar este método. Ele aceita alguns parâmetros como filtro.",
     *   operationId="getAll",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="query",
     *     name="sort",
     *     description="Campo para ordenação",
     *     required=false,
     *     default="st_disciplina",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     in="query",
     *     name="direction",
     *     description="Define se vai ordenar os dados como crescente ou decrescente ASC/DESC",
     *     required=false,
     *     default="ASC",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     in="query",
     *     name="limit",
     *     description="Quantidade de registros que quer no retorno",
     *     required=false,
     *     default=50,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     in="query",
     *     name="offset",
     *     description="Registro inicial a ser buscado, padrão 0",
     *     required=false,
     *     default=0,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     in="query",
     *     name="gradenota",
     *     description="JSON com os dados para filtro da grade de notas",
     *     required=true, type="string",
     *     @SWG\Schema(ref="#/definitions/GradeNota")
     *   ),
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/VwGradeNota")
     *         )
     *   ),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="Nenhum registro encontrado", @SWG\Schema(ref="#/definitions/Error"))
     *
     * )))
     *
     */
    public function indexAction()
    {

        try {

            $sort = $this->_getParam('sort', 'st_disciplina');
            $direction = strtoupper($this->_getParam('direction', 'ASC'));
            $offset = $this->_getParam('offset', 0);
            $limit = $this->_getParam('limit', null);

            if (!in_array($direction, $this->directions))
                throw new \Exception("Direction inválida");

            $gradenota = Zend_Json::decode($this->_getParam('gradenota', null));
            if (!$gradenota)
                throw new \Exception("Você precisa informar o objeto gradenota");

            if (!is_array($gradenota))
                throw new \Exception("grade-nota não é um Json válido");

            $negocio = new \G2\Negocio\Matricula();
            $arrGradeNotas = $negocio->retornaGradeNotas($gradenota, array($sort => $direction), $limit, $offset, true);

            $this->json($arrGradeNotas);

        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }

    }


    /**
     *
     * @SWG\Get(path="/grade-nota/vw-grade-nota-agrupada",
     *      tags={"grade-nota"},
     *      summary="Retorna dados da VwGradeNotaAgrupada com detalhes",
     *      description="Você precisa estar logado para usar este método",
     *      operationId="getVwGradeNotaAgrupadaDetalhada",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          in="query",
     *          name="id_matricula",
     *          description="ID matrícula do aluno a ter as notas agrupadas retornada.",
     *          required="true",
     *          type="integer"
     *      ),
     *
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/VwGradeNotaResponse")
     *         )
     *   ),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="Nenhum registro encontrado", @SWG\Schema(ref="#/definitions/Error"))
     *
     * )))
     */
    public function getVwGradeNotaAgrupadaAction()
    {

        try {
            $arrParams = array('id_matricula' => $this->getParam('id_matricula'));
            $negocio = new \G2\Negocio\Matricula();
            $arrGradeNotas = $negocio->retornaVwGradeNotaAgrupada($arrParams);
            $this->json($arrGradeNotas);
        } catch (Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }

    }

    /**
     *
     * @SWG\Get(path="/grade-nota/vw-grade-nota-agrupada-detalhada",
     *      tags={"grade-nota"},
     *      summary="Retorna dados da VwGradeNotaAgrupada com detalhes",
     *      description="Você precisa estar logado para usar este método",
     *      operationId="getVwGradeNotaAgrupadaDetalhada",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          in="query",
     *          name="id_matricula",
     *          description="ID matrícula do aluno a ter as notas agrupadas retornada.",
     *          required="true",
     *          type="integer"
     *      ),
     *      @SWG\Parameter(
     *          in="query",
     *          name="id_alocacao",
     *          description="ID da alocação do aluno a ter as notas agrupadas retornada.",
     *          type="integer"
     *      ),
     *
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/VwGradeNotaResponse")
     *         )
     *   ),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="Nenhum registro encontrado", @SWG\Schema(ref="#/definitions/Error"))
     *
     * )))
     */
    public function getVwGradeNotaAgrupadaDetalhadaAction()
    {

        try {
            $arrParams = array('id_matricula' => $this->getParam('id_matricula'), 'id_disciplina' => $this->getParam('id_disciplina'));
            $negocio = new \G2\Negocio\Matricula();
            $arrGradeNotas = $negocio->retornaVwGradeNotaAgrupada($arrParams, true);
            $this->json($arrGradeNotas);
        } catch (Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }

    }

    /**
     * Método responsável por retornar um item da Model/Collection/Consulta baseado no ID
     */
    public function getAction()
    {
        // TODO: Implement getAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     * Método responsável por inserir dados na Model
     */
    public function createAction()
    {
        // TODO: Implement createAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     * Método responsável por retornar atualizar os dados na Model
     */
    public function updateAction()
    {
        // TODO: Implement updateAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     * Método responsável por excluir os dados da Model/Collection/Consulta
     */
    public function deleteAction()
    {
        // TODO: Implement deleteAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     * Método responsável por retornar todos os dados da Model/Collection/Consulta por página
     */
    public function pagingAction()
    {
        // TODO: Implement pagingAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

}

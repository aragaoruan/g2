<?php

class ApiV2_CursoAlunoController extends Ead1_AppApi implements \Ead1\Api
{

    /**
     *
     * @SWG\Definition(
     *     definition="CursoAlunoCursosResponse",
     *     @SWG\Property(property="id_curso",type="integer"),
     *     @SWG\Property(property="st_curso",type="string"),
     *     @SWG\Property(property="id_matricula",type="integer"),
     *     @SWG\Property(property="dt_inicioturma",type="date"),
     *     @SWG\Property(property="st_imagemarea",type="string")
     *     @SWG\Property(property="id_entidade",type="integer")
     *     @SWG\Property(property="st_descricao",type="string"),
     *     @SWG\Property(property="st_imagem",type="string"),
     * ),
     *
     * @SWG\Definition(
     *     definition="CursoAlunoResponse",
     *     @SWG\Property(property="id_entidade",type="integer"),
     *     @SWG\Property(property="st_entidade",type="string"),
     *     @SWG\Property(property="st_logo",type="string"),
     *     @SWG\Property(property="st_urlsite",type="string"),
     *     @SWG\Property(property="cursos",type="array", @SWG\Items(ref="#/definitions/CursoAlunoCursosResponse"))
     * ),
     */

    /**
     * @SWG\Get(path="/curso-aluno",
     *   tags={"curso-aluno"},
     *   summary="Retorna os cursos que o aluno tem acesso divididos por entidade",
     *   description="Você precisa estar logado para usar este método. Ele não tem parâmetros.",
     *   operationId="getAll",
     *   produces={"application/json"},
     *     @SWG\Parameter(
     *     in="query",
     *     name="bl_novoportal",
     *     description="Decide se o dispositivo deve listar cursos disponíveis ao novo portal",
     *     required=true,
     *     type="boolean"
     *   ),
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/CursoAlunoResponse")
     *         )
     *   ),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     *
     * )))
     */
    public function indexAction()
    {

        try {

            $params = array();

            if ($this->hasParam('bl_novoportal')) {
                $params = array('bl_novoportal' => ($this->getParam('bl_novoportal') ? true : false));
            };

            $business = new \G2\Negocio\Matricula();
            $result = $business->retornarCursosByAluno(
                $this->getuser()->getuser_id(),
                $this->getuser()->getentidade_id(),
                $params
            ); //busca os dados

            $this->json($result);

        } catch (\Exception $e) {

            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());

        }

    }


    /**
     * Método responsável por retornar um item da Model/Collection/Consulta baseado no ID
     */
    public function getAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, "Método ainda não implementado", 404);
        $this->_helper->json($error->toArray());
    }

    /**
     * Método responsável por inserir dados na Model
     */
    public function createAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, "Método ainda não implementado", 404);
        $this->_helper->json($error->toArray());
    }

    /**
     * Método responsável por retornar atualizar os dados na Model
     */
    public function updateAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, "Método ainda não implementado", 404);
        $this->_helper->json($error->toArray());
    }

    /**
     * Método responsável por excluir os dados da Model/Collection/Consulta
     */
    public function deleteAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, "Método ainda não implementado", 404);
        $this->_helper->json($error->toArray());
    }

    /**
     * Método responsável por retornar todos os dados da Model/Collection/Consulta por página
     */
    public function pagingAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, "Método ainda não implementado", 404);
        $this->_helper->json($error->toArray());
    }

    /**
     * @SWG\Get(path="/curso-aluno/get-sala-aula-by-matricula",
     *   tags={"curso-aluno"},
     *   summary="Retorna todas as salas vinculadas a uma matricula",
     *   description="Lista as salas de aulas vinculadas a uma matricula.",
     *   operationId="getAll",
     *   produces={"application/json"},
     *     @SWG\Parameter(
     *     in="query",
     *     name="id_matricula",
     *     description="ID da matrícula.",
     *     required=true,
     *     type="integer"
     *   ),
     *    @SWG\Response(response="default",description="Operação efetuada com sucesso",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/VwSalaDisciplinaAlocacao")
     *         )
     *   ),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     *
     * )))
     *
     */
    public function getSalaAulaByMatriculaAction()
    {
        try {
            $id_matricula = $this->getParam('id_matricula');

            if (!$id_matricula) {
                throw new Exception("Parametro id_matricula é obrigatório.");
            }

            $negocio = new \G2\Negocio\SalaDeAula();
            $result = $negocio->retornaVwSalaDisciplinaAlocacao(array(
                'id_matricula' => $id_matricula,
                'bl_ativo' => true
            ));

            if (count($result)) {
                foreach ($result as $index => $data) {
                    $result[$index] = $negocio->toArrayEntity($data);
                }
            }

            $this->json($result);

        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }

    }

    /**
     *
     * @SWG\Get(path="/curso-aluno/verificar-matricula-aluno",
     *   tags={"curso-aluno"},
     *   summary="Verifica se o aluno esta matriculado em determinada matricula, retornando os dados basicos da matricula caso o aluno esteja vinculado a matricula",
     *   description="Você precisa estar logado para usar este método. Parametro esperado id_matricula",
     *   operationId="getAll",
     *   produces={"application/json"},
     *      @SWG\Parameter(
     *     in="query",
     *     name="id_matricula",
     *     description="ID da matrícula.",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso",
     *         @SWG\Schema(ref="#/definitions/CursoAlunoCursosResponse")
     *   ),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     *
     * )))
     */
    public function verificarMatriculaAlunoAction()
    {
        try {

            $id_matricula = $this->getParam('id_matricula');

            $params = array(
                "id_matricula" => $id_matricula,
            );


            $negocio = new \G2\Negocio\Matricula();
            $result = $negocio->retornarCursosByAluno(
                $this->getuser()->getuser_id(),
                $this->getuser()->getentidade_id(),
                $params
            );

            if (!$result) {
                throw new Exception("Matrícula não esta vinculada ao aluno ou não existe.");
            }

            $this->json($result[0]["cursos"][0]);

        } catch (Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }
    }

}

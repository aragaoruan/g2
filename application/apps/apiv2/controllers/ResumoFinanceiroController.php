<?php

/**
 * Class AppApi_ResumoFinanceiroController
 * Controller REST para financeiro
 */
class ApiV2_ResumoFinanceiroController extends Ead1_AppApi implements \Ead1\Api
{
    /**
     * @SWG\Definition(
     *     definition="ResumoFinanceiroResponse",
     *     @SWG\Property(property="bl_ativo",type="boolean"),
     *     @SWG\Property(property="bl_cartaovencendo",type="boolean"),
     *     @SWG\Property(property="bl_chequedevolvido",type="boolean"),
     *     @SWG\Property(property="bl_entrada",type="boolean"),
     *     @SWG\Property(property="bl_original",type="boolean"),
     *     @SWG\Property(property="bl_quitado",type="boolean"),
     *     @SWG\Property(property="bl_recorrente",type="boolean"),
     *     @SWG\Property(property="dt_atualizado",type="string"),
     *     @SWG\Property(property="dt_inicioturma",type="string"),
     *     @SWG\Property(property="dt_prevquitado",type="string"),
     *     @SWG\Property(property="dt_quitado",type="string"),
     *     @SWG\Property(property="dt_vencimento",type="string"),
     *     @SWG\Property(property="dt_venda",type="string"),
     *     @SWG\Property(property="id_acordo",type="integer"),
     *     @SWG\Property(property="id_curso",type="integer"),
     *     @SWG\Property(property="id_entidade",type="integer"),
     *     @SWG\Property(property="id_lancamento",type="integer"),
     *     @SWG\Property(property="id_matricula",type="integer"),
     *     @SWG\Property(property="id_meiopagamento",type="integer"),
     *     @SWG\Property(property="id_reciboconsolidado",type="integer"),
     *     @SWG\Property(property="id_statuslancamento",type="integer"),
     *     @SWG\Property(property="id_textosistemarecibo",type="integer"),
     *     @SWG\Property(property="id_tipoendereco",type="integer"),
     *     @SWG\Property(property="id_usuario",type="integer"),
     *     @SWG\Property(property="id_venda",type="integer"),
     *     @SWG\Property(property="nu_cheque",type="number"),
     *     @SWG\Property(property="nu_desconto",type="number"),
     *     @SWG\Property(property="nu_descontoboleto",type="number"),
     *     @SWG\Property(property="nu_juros",type="number"),
     *     @SWG\Property(property="bl_chequedevolvido",type="boolean"),
     *     @SWG\Property(property="nu_jurosboleto",type="number"),
     *     @SWG\Property(property="nu_numero",type="number"),
     *     @SWG\Property(property="nu_ordemparcela",type="number"),
     *     @SWG\Property(property="nu_parcela",type="number"),
     *     @SWG\Property(property="nu_quitado",type="number"),
     *     @SWG\Property(property="nu_valor",type="number"),
     *     @SWG\Property(property="nu_vencimento",type="number"),
     *     @SWG\Property(property="recorrente_orderid",type="string"),
     *     @SWG\Property(property="sg_uf",type="string"),
     *     @SWG\Property(property="st_autorizacao",type="string"),
     *     @SWG\Property(property="st_bairro",type="string"),
     *     @SWG\Property(property="st_banco",type="string"),
     *     @SWG\Property(property="st_cep",type="string"),
     *     @SWG\Property(property="st_cidade",type="string"),
     *     @SWG\Property(property="st_coddocumento",type="string"),
     *     @SWG\Property(property="st_codtransacaooperadora",type="string"),
     *     @SWG\Property(property="st_curso",type="string"),
     *     @SWG\Property(property="st_emissor",type="string"),
     *     @SWG\Property(property="st_endereco",type="string"),
     *     @SWG\Property(property="st_entradaparcela",type="string"),
     *     @SWG\Property(property="st_estadoprovincia",type="string"),
     *     @SWG\Property(property="st_imagemarea",type="string"),
     *     @SWG\Property(property="st_link",type="string"),
     *     @SWG\Property(property="st_meiopagamento",type="string"),
     *     @SWG\Property(property="st_nomecompleto",type="string"),
     *     @SWG\Property(property="st_nossonumero",type="string"),
     *     @SWG\Property(property="st_numcheque",type="string"),
     *     @SWG\Property(property="st_situacao",type="string"),
     *     @SWG\Property(property="st_situacaolancamento",type="string"),
     *     @SWG\Property(property="st_situacaopedido",type="string"),
     *     @SWG\Property(property="st_statuslancamento",type="string"),
     *     @SWG\Property(property="st_ultimosdigitos",type="string")
     *     @SWG\Property(property="st_urlloja",type="string")
     * ),
     *
     *
     * @SWG\Get(path="/resumo-financeiro",
     *   tags={"resumo-financeiro"},
     *   summary="Retorna o resumo financeiro do usuario",
     *   description="Retorna o resumo financeiro do usuario",
     *   operationId="getAll",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="id_lancamento",
     *     in="query",
     *     description="ID do Lançamento",
     *     required=false,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="id_statuslancamento",
     *     in="query",
     *     description="ID do Status da parcela",
     *     required=false,
     *     type="integer"
     *   ),
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/ResumoFinanceiroResponse")
     *         )
     *   ),
     *   @SWG\Response(response=400, description="Ocorreu algum erro ao tentar buscar o resumo financeiro", @SWG\Schema(ref="#/definitions/Error"))
     * )
     *
     */
    public function indexAction()
    {
        try {
            $idUser = $this->getuser()->getuser_id();//pega o id do usuario
            $idEntidade = $this->getuser()->getentidade_id();//pega o id da entidade

            $params = array();

            $id_lancamento = $this->_getParam('id_lancamento', null);
            if($id_lancamento)
                $params['id_lancamento'] = $id_lancamento;

            $id_statuslancamento = $this->_getParam('id_statuslancamento', null);
            if($id_statuslancamento)
                $params['id_statuslancamento'] = $id_statuslancamento;

            $negocio = new \G2\Negocio\Financeiro();
            $result = $negocio->retornarResumoFinanceiroByIdUsuario($idUser, $idEntidade, $params);

            $this->json($result);

        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }

    }


    /**
     * @SWG\Get(path="/resumo-financeiro/get/id/{id_lancamento}",
     *   tags={"resumo-financeiro"},
     *   summary="Retorna um registro do resumo financeiro baseado no lançamento",
     *   description="Você precisa estar logado para usar este método.",
     *   operationId="get",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="id_lancamento",
     *     in="path",
     *     description="ID do Lançamento",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response="default", description="Operação efetuada com sucesso", @SWG\Schema(ref="#/definitions/ResumoFinanceiroResponse")),
     *   @SWG\Response(response=400, description="ID inválido", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="Lançamento não encontrado", @SWG\Schema(ref="#/definitions/Error"))
     * )))))
     *
     */
    public function getAction()
    {
        try {
            $idUser = $this->getuser()->getuser_id();//pega o id do usuario
            $idEntidade = $this->getuser()->getentidade_id();//pega o id da entidade


            $params = array();

            $params['id_lancamento'] = $this->_getParam('id', null);

            if(!$params['id_lancamento'])
                throw new \Zend_Http_Exception("Lançamento não encontrado");

            $negocio = new \G2\Negocio\Financeiro();
            $result = $negocio->retornarResumoFinanceiroByIdUsuario($idUser, $idEntidade, $params);

            if($result)
                $this->json($result[0]);

        } catch (\Zend_Http_Exception $e) {
            $this->getResponse()->setHttpResponseCode(404);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 404);
            $this->json($error->toArray());
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }
    }

    /**
     * Método responsável por inserir dados na Model
     */
    public function createAction()
    {
        // TODO: Implement createAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     * Método responsável por retornar atualizar os dados na Model
     */
    public function updateAction()
    {
        // TODO: Implement updateAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     * Método responsável por excluir os dados da Model/Collection/Consulta
     */
    public function deleteAction()
    {
        // TODO: Implement deleteAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     * Método responsável por retornar todos os dados da Model/Collection/Consulta por página
     */
    public function pagingAction()
    {
        // TODO: Implement pagingAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     *
     * @SWG\Definition(
     *     definition="ExistePlanoPagamentoResponse",
     *     @SWG\Property(property="planoPagamentoExiste",type="boolean"),
     *     @SWG\Property(property="status", type="string")),
     * @SWG\Get(path="/resumo-financeiro/existe-plano-pagamento",
     *    tags={"resumo-financeiro"},
     *    summary="Resumão",
     *    description="Você precisa estar logado para usar este método.",
     *    produces={"application/json"},
     *    @SWG\Parameter(
     *      name="id_matricula",
     *      in="path",
     *      description="ID Matrícula",
     *      required=true,
     *      type="integer"
     *   ),
     *   @SWG\Parameter(
     *      name="id_entidade",
     *      in="path",
     *      description="ID Entidade",
     *      required="true",
     *      type="integer"
     *   ),
     *   @SWG\Response(response=200, description="Operação efetuada com sucesso"),
     *   @SWG\Response(response=404, description="Lançamento não encontrado")
     * )))))
     *
     */

    public function existePlanoPagamentoAction(){

        $id_entidade = $this->_getParam('id_entidade');
        $id_matricula = $this->_getParam('id_matricula');
        $planoPagamentoExiste = false;
        $statusPlanoPagamento = '';

        $textoBO = new TextoSistemaBO();

        $to = new TextoSistemaTO();
        $to->setId_textocategoria(G2\Constante\TextoCategoria::PLANO_PAGAMENTO);
        $to->setId_entidade($id_entidade);
        $to->fetch(false, true, true);

        if (!$to->getSt_texto()) {
            $statusPlanoPagamento = "Nenhum texto de sistema para plano de pagamento foi encontrado.";
        }

        if ($id_matricula) {
            $matriculaContrato = new ContratoMatriculaTO();
            $matriculaContrato->setId_matricula($id_matricula);
            $matriculaContrato->fetch(false, true, true);

            if ($matriculaContrato->getId_contrato()) {
                $firstMessage = $textoBO->gerarHTML($to, array('id_contrato' => $matriculaContrato->getId_contrato()));
                if ($firstMessage->getFirstMensagem() instanceof TextoSistemaTO) {
                    $planoPagamentoExiste = true;
                    $statusPlanoPagamento = 'Plano de pagamento existente!';
                } else {
                    $statusPlanoPagamento = 'Nenhum texto foi encontrado para esta entidade';
                }
            } else {
                $statusPlanoPagamento = 'Nenhum contrato localizado para a matrícula ' . Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula . '.';
            }
        } else {
            $statusPlanoPagamento = "Não foi possível localizar o código da matricula.";
        }

        $this->json(array('existePlanoPagamento' => $planoPagamentoExiste,
            'status' => $statusPlanoPagamento));
    }
}
<?php

class ApiV2_UsuarioController extends Ead1_AppApi implements \Ead1\Api
{

    /**
     * @SWG\Get(path="/usuario",
     *   tags={"usuario"},
     *   summary="Retorna todos os Usuários",
     *   description="Você precisa estar logado para usar este método. Ele aceita alguns parâmetros como filtro.",
     *   operationId="getAll",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="query",
     *     name="sort",
     *     description="Campo para ordenação",
     *     required=false,
     *     default="st_nomecompleto",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     in="query",
     *     name="direction",
     *     description="Define se vai ordenar os dados como crescente ou decrescente ASC/DESC",
     *     required=false,
     *     default="ASC",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     in="query",
     *     name="limit",
     *     description="Quantidade de registros que quer no retorno",
     *     required=false,
     *     default=50,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     in="query",
     *     name="offset",
     *     description="Registro inicial a ser buscado, padrão 0",
     *     required=false,
     *     default=0,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     in="query",
     *     name="usuario",
     *     description="JSON com os dados do Usuário para ser usada como filtro, campos texto serão pesquisados com LIKE",
     *     required=true, type="string",
     *     @SWG\Schema(ref="#/definitions/Usuario")
     *   ),
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/Usuario")
     *         )
     *   ),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="Nenhum registro encontrado", @SWG\Schema(ref="#/definitions/Error"))
     *
     * )))
     *
     */
    public function indexAction()
    {

        try {

            $sort = $this->_getParam('sort', 'st_nomecompleto');
            $direction = strtoupper($this->_getParam('direction', 'ASC'));
            $offset = $this->_getParam('offset', 0);
            $limit = $this->_getParam('limit', null);

            if (!in_array($direction, $this->directions))
                throw new \Exception("Direction inválida");

            $usuario = Zend_Json::decode($this->_getParam('usuario', null));
            if (!$usuario)
                throw new \Exception("Você precisa informar o objeto usuario");

            if (!is_array($usuario))
                throw new \Exception("usuario não é um Json válido");

            $us = new \G2\Entity\Usuario();
            $uss = $us->findByCustom($usuario, array($sort => $direction), $limit, $offset, false);
            if (!$uss)
                throw new \Zend_Http_Exception("Nenhum registro encontrado");

            $this->json($uss);


        } catch (\Zend_Http_Exception $e) {

            $this->getResponse()->setHttpResponseCode(404);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 404);
            $this->json($error->toArray());

        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }

    }

    /**
     * @SWG\Get(path="/usuario/get/id/{id_usuario}",
     *   tags={"usuario"},
     *   summary="Retorna um registro da tabela tb_usuario baseado no id_usuario",
     *   description="Você precisa estar logado para usar este método. Retorna um registro na tabela tb_usuario baseado no id_usuario",
     *   operationId="get",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="id_usuario",
     *     in="path",
     *     description="ID do Usuário",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso", @SWG\Schema(ref="#/definitions/Usuario")),
     *   @SWG\Response(response=400, description="ID inválido", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="Usuário não encontrado", @SWG\Schema(ref="#/definitions/Error"))
     * )))))
     *
     */
    public function getAction()
    {

        try {

            $id_usuario = $this->_getParam('id', null);
            if (!$id_usuario)
                throw new \Exception("Você precisa informar o id");

            $us = new UsuarioTO();
            $us->setId_usuario($id_usuario);
            $us->fetch(true, true, true);
            $this->json($us->toArray());

        } catch (\Zend_Http_Exception $e) {
            $this->getResponse()->setHttpResponseCode(404);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 404);
            $this->json($error->toArray());
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }

    }

    /**
     * @SWG\Post(path="/usuario/create",
     *   tags={"usuario"},
     *   summary="Insere um novo registro na tabela tb_usuario",
     *   description="Você precisa estar logado para usar este método. Faz o insert dos dados do Usuário. O campo id_usuario é ignorado",
     *   operationId="create",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     description="Objeto com os dados do Usuário a serem inseridos",
     *     required=false,
     *     @SWG\Schema(ref="#/definitions/Usuario")
     *   ),
     *   @SWG\Response(response=201,description="Operação efetuada com sucesso", @SWG\Schema(ref="#/definitions/Usuario")),
     *   @SWG\Response(response=400, description="Dados inválidos para o cadastro", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="Usuário não encontrado", @SWG\Schema(ref="#/definitions/Error"))
     * )))))
     *
     */
    public function createAction()
    {

        try {

            $params = Zend_Json::decode($this->getRequest()->getRawBody());
            if (!$params)
                throw new \Exception("Você precisa enviar os dados so Usuário a serem salvos");

            unset($params['id_usuario']);

            $usn = new \G2\Negocio\Pessoa();
            $us = $usn->salvarUsuario($params);

            $this->getResponse()->setHttpResponseCode(201);
            $this->json($us[0]->toArray());

        } catch (\Zend_Http_Exception $e) {
            $this->getResponse()->setHttpResponseCode(404);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 404);
            $this->json($error->toArray());
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }

    }

    /**
     * @SWG\Put(path="/usuario/update/id/{id_usuario}",
     *   tags={"usuario"},
     *   summary="Atualiza o registro do Usuário na tabela tb_usuario",
     *   description="Você precisa estar logado para usar este método. Faz o update dos dados do Usuário",
     *   operationId="update",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="id_usuario",
     *     in="path",
     *     description="ID do Usuário a ser atualizado",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     description="Objeto com os dados do Usuário a serem atualizados",
     *     required=false,
     *     @SWG\Schema(ref="#/definitions/Usuario")
     *   ),
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso", @SWG\Schema(ref="#/definitions/Usuario")),
     *   @SWG\Response(response=400, description="Dados inválidos para atualização", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="Usuario não encontrado", @SWG\Schema(ref="#/definitions/Error"))
     * )))))
     *
     */
    public function updateAction()
    {

        try {

            $id_usuario = (int)$this->_getParam('id', null);
            if (!$id_usuario)
                throw new \Exception("Você precisa informar o id");

            $params = Zend_Json::decode($this->getRequest()->getRawBody());
            if (!$params)
                throw new \Exception("Você precisa enviar os dados so Usuário a serem salvos");

            unset($params['id_usuario'], $params['st_senha']);

            $us = new UsuarioTO($params);
            $us->setId_usuario($id_usuario);

            $bo = new PessoaBO();
            $bo->editarUsuario($us);

            $us->fetch(true, true, true);

            $this->json($us->toArray());

        } catch (\Zend_Http_Exception $e) {
            $this->getResponse()->setHttpResponseCode(404);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 404);
            $this->json($error->toArray());
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }

    }

    /**
     * @SWG\Delete(path="/usuario/delete/id/{id_usuario}",
     *   tags={"usuario"},
     *   summary="Exclui um Usuário da tabela tb_usuario baseado no id_usuario",
     *   description="Você precisa estar logado para usar este método. Faz a exclusão do Usuário da base de dados ou o torna inativo",
     *   operationId="delete",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="id_usuario",
     *     in="path",
     *     description="ID do Usuário",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso"),
     *   @SWG\Response(response=400, description="ID inválido", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="Usuário não encontrado", @SWG\Schema(ref="#/definitions/Error"))
     * ))))
     *
     */
    public function deleteAction()
    {

        try {

            $id_usuario = $this->_getParam('id', null);
            if (!$id_usuario)
                throw new \Exception("Você precisa informar o id");

            $us = new UsuarioTO();
            $us->setId_usuario($id_usuario);
            $us->fetch(true, true, true);
            $us->setBl_ativo(false);

            $bo = new PessoaBO();
            $bo->editarUsuario($us);

        } catch (\Zend_Http_Exception $e) {
            $this->getResponse()->setHttpResponseCode(404);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 404);
            $this->json($error->toArray());
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }

    }


    /**
     *
     *
     * @SWG\Definition(
     *   definition="UsuarioPagination",
     *   allOf={
     *     @SWG\Schema(ref="#/definitions/Pagination"),
     *     @SWG\Schema(type="object",@SWG\Property(property="data",type="array",@SWG\Items(ref="#/definitions/Usuario")))
     *   }
     * )
     *
     * @SWG\Get(path="/usuario/paging",
     *   tags={"usuario"},
     *   summary="Retorna os Usuários paginados",
     *   description="Você precisa estar logado para usar este método. Ele aceita alguns parâmetros como filtro.",
     *   operationId="paging",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="query",
     *     name="perPage",
     *     description="Total por página",
     *     required=false,
     *     default=25,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     in="query",
     *     name="page",
     *     description="Página que se quer retornar",
     *     required=false,
     *     default=1,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     in="query",
     *     name="sort",
     *     description="Campo para ordenação",
     *     required=false,
     *     default="st_nomecompleto",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     in="query",
     *     name="direction",
     *     description="Define se vai ordenar os dados como crescente ou decrescente ASC/DESC",
     *     required=false,
     *     default="ASC",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     in="query",
     *     name="usuario",
     *     description="JSON com os dados do Usuário para ser usada como filtro, campos texto serão pesquisados com LIKE exemplo",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/Usuario")
     *   ),
     *   @SWG\Response(response="default",description="Dados do Usuário paginado", @SWG\Schema(ref="#/definitions/UsuarioPagination")),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="Nenhum registro encontrado", @SWG\Schema(ref="#/definitions/Error"))
     * )
     *)))))
     *
     */
    public function pagingAction()
    {

        try {

            $usuario = Zend_Json::decode($this->_getParam('usuario', null));
            if (!$usuario)
                throw new \Exception("Você precisa informar o objeto usuario");


            if (!is_array($usuario))
                throw new \Exception("usuario não é um Json válido");

            $sort = $this->_getParam('sort', 'st_nomecompleto');
            $direction = strtoupper($this->_getParam('direction', 'ASC'));

            if (!in_array($direction, $this->directions))
                throw new \Exception("Direction inválida");


            $us = new \G2\Entity\Usuario();
            $uss = $us->paging($usuario, array($sort, $direction), $this->_getParam('page', 1), $this->_getParam('perPage', 25), false);
            if (!$uss->gettotal())
                throw new \Zend_Http_Exception("Nenhum registro encontrado");

            $this->json($uss->toArray());


        } catch (\Zend_Http_Exception $e) {

            $this->getResponse()->setHttpResponseCode(404);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), $this->getResponse()->getHttpResponseCode());
            $this->json($error->toArray());

        } catch (\Exception $e) {

            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), $this->getResponse()->getHttpResponseCode());
            $this->json($error->toArray());

        }

    }

    /**
     * @SWG\Put(path="/usuario/altera-senha/",
     *   tags={"usuario"},
     *   summary="Atualiza o registro do Usuário na tabela tb_usuario",
     *   description="Você precisa estar logado para usar este método. Faz o update dos dados do Usuário",
     *   operationId="update",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="id_usuario",
     *     in="path",
     *     description="ID do Usuário a ser atualizado",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     description="Objeto com os dados do Usuário a serem atualizados",
     *     required=false,
     *     @SWG\Schema(ref="#/definitions/Usuario")
     *   ),
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso", @SWG\Schema(ref="#/definitions/Usuario")),
     *   @SWG\Response(response=400, description="Dados inválidos para atualização", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="Usuario não encontrado", @SWG\Schema(ref="#/definitions/Error"))
     * )))))
     *
     */
    public function alteraSenhaAction()
    {
        $params = Zend_Json::decode($this->getRequest()->getRawBody());

        try {
            if ($params['id_usuario'] == $this->getuser()->getuser_id()) {
                $id_usuario = $params['id_usuario'];
            } else {
                throw new \Exception("Você não pode alterar a senha de outro usuário.");
            }

            $id_entidade = $params['id_entidade'];
            $st_senha_nova = $params['st_senha'];

            $login = new PortalLoginBO();
            $mensageiro = $login->salvarRedefinicaoSenha($id_usuario, $id_entidade, $st_senha_nova, $st_senha_nova, false);
            $this->json($mensageiro->toArray());

        } catch (\Zend_Http_Exception $e) {

            $this->getResponse()->setHttpResponseCode(404);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), $this->getResponse()->getHttpResponseCode());
            $this->json($error->toArray());

        } catch (\Exception $e) {

            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), $this->getResponse()->getHttpResponseCode());
            $this->json($error->toArray());

        }
    }


    /**
     *
     * @SWG\Post(path="/usuario/upload-avatar",
     *   tags={"tcc"},
     *   summary="Faz o upload do Avatar do Aluno",
     *   description="Você precisa estar logado para usar este método.",
     *   operationId="upload-avatar",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="formData",
     *     name="file",
     *     description="Imagem do Usuário",
     *     required=true,type="file"
     *   ),
     *   @SWG\Response(response=201,description="Operação efetuada com sucesso", @SWG\Schema(ref="#/definitions/Ead1_Mensageiro")),
     *   @SWG\Response(response=400, description="Dados inválidos para o cadastro", @SWG\Schema(ref="#/definitions/Error")),
     * )))))
     *
     */
    public function uploadAvatarAction()
    {
        try {

            $avatar = null;

            if (isset($_FILES['file']['tmp_name'])) {
                $avatar = $_FILES['file'];
            }

            if (!$avatar)
                throw new \Exception("Arquivo não informado!");

            $id = $this->getuser()->getuser_id();

            if (!$id)
                throw new \Exception("Id não informado!");

            $pe = new \G2\Negocio\Pessoa();
            $st_urlavatar = null;
            $resultado = $pe->findOneBy('\G2\Entity\VwPessoa', array(
                'id_entidade' => $this->getuser()->getentidade_id(),
                'id_usuario' => $this->getuser()->getuser_id()
            ));
            $url_avatar = (!empty($resultado)) ? $resultado->getSt_urlavatar() : null;
            $imagem = $pe->uploadAvatar($avatar, $this->getuser()->getuser_id(), $url_avatar);
            if ($imagem['type'] == 'success') {
                $pe->salvaAvatarPessoa(array(
                    'id_usuario' => $this->getuser()->getuser_id(),
                    'id_entidade' => $this->getuser()->getentidade_id(),
                    'st_urlavatar' => "/upload/usuario/" . $imagem['file']
                ));
                $st_urlavatar = "/upload/usuario/" . $imagem['file'];
            } else {
                throw new \Exception($imagem['text']);
            }

            $mensageiro = new Ead1_Mensageiro($st_urlavatar, Ead1_IMensageiro::SUCESSO);

            $this->json($mensageiro->toArray());

        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(
                \G2\Utils\Error::ERROR,
                $e->getMessage(),
                $this->getResponse()->getHttpResponseCode()
            );
            $this->json($error->toArray());
        }
    }


}

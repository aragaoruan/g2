<?php

class ApiV2_PessoaController extends Ead1_AppApi implements \Ead1\Api
{


    public function indexAction()
    {
        // TODO: Implement getAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     * @SWG\Get(path="/pessoa/get",
     *   tags={"pessoa"},
     *   summary="Retorna um registro da tabela tb_pessoa baseado no id_usuario",
     *   description="Você precisa estar logado para usar este método. Retorna um registro na tabela tb_pessoa baseado no id_usuario",
     *   operationId="get",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="id_usuario",
     *     in="query",
     *     description="ID do Usuário, não obrigatório, caso não informado pega o valor do token.",
     *     required=false,
     *     type="integer"
     *   ),
     *     @SWG\Parameter(
     *     name="id_entidade",
     *     in="query",
     *     description="ID da entidade, não obrigatório, caso não informado pega o valor do token.",
     *     required=false,
     *     type="integer"
     *   ),
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso", @SWG\Schema(ref="#/definitions/Pessoa")),
     *   @SWG\Response(response=400, description="ID inválido", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="Usuário não encontrado", @SWG\Schema(ref="#/definitions/Error"))
     * )))))
     *
     */
    public function getAction()
    {

        try {
            $id_usuario = $this->_getParam('id_usuario', $this->getuser()->getuser_id());
            if (!$id_usuario)
                throw new \Exception("Você precisa informar o id do usuario");

            $id_entidade = $this->_getParam('id_entidade', $this->getuser()->getentidade_id());
            if (!$id_entidade)
                throw new \Exception("Você precisa informar o id da entidade");

            $us = new PessoaTO();
            $us->setId_usuario($id_usuario);
            $us->setId_entidade($id_entidade);
            $us->fetch(true, true, true);

            if (!$us)
                throw new \Zend_Http_Exception("Nenhum dado encontrado!");

            $this->json($us->toArray());

        } catch (\Zend_Http_Exception $e) {
            $this->getResponse()->setHttpResponseCode(404);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 404);
            $this->json($error->toArray());
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }

    }

    public function createAction()
    {
        // TODO: Implement getAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    public function updateAction()
    {
        // TODO: Implement getAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    public function deleteAction()
    {
        // TODO: Implement getAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    public function pagingAction()
    {
        // TODO: Implement getAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     * @SWG\Get(path="/pessoa/dados-pessoa",
     *   tags={"pessoa"},
     *   summary="Retorna um registro da tabela tb_pessoa baseado no id_usuario",
     *   description="Você precisa estar logado para usar este método. Retorna um registro na tabela tb_pessoa baseado no id_usuario",
     *   operationId="getAll",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="id_usuario",
     *     in="query",
     *     description="ID do Usuário, não obrigatório, caso não informado pega o valor do token.",
     *     required=false,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="id_entidade",
     *     in="query",
     *     description="ID da entidade, não obrigatório, caso não informado pega o valor do token.",
     *     required=false,
     *     type="integer"
     *   ),
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso", @SWG\Schema(ref="#/definitions/VwPessoa")),
     *   @SWG\Response(response=400, description="ID inválido", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="Usuário não encontrado", @SWG\Schema(ref="#/definitions/Error"))
     * )))))
     *
     */
    public function dadosPessoaAction()
    {
        try {
            $id_usuario = $this->_getParam('id_usuario', $this->getuser()->getuser_id());
            if (!$id_usuario)
                throw new \Exception("Você precisa informar o id do usuario");

            $id_entidade = $this->_getParam('id_entidade', $this->getuser()->getentidade_id());
            if (!$id_entidade)
                throw new \Exception("Você precisa informar o id da entidade");

            $negocioPessoa = new \G2\Negocio\Pessoa;
            $pessoa = $negocioPessoa->retornaVwPessoaOne(array(
                'id_usuario' => $id_usuario,
                'id_entidade' => $id_entidade
            ), true);

            /*
             * verifica se a resposta do método retornaVwPessoaOne é um mensageiro e diferente de sucesso
             * ou se a variável veio vazia
             */
            if (
                ($pessoa instanceof Ead1_Mensageiro && $pessoa->getType() !== Ead1_IMensageiro::TYPE_SUCESSO)
                || !$pessoa) {
                throw new \Zend_Http_Exception("Nenhum dado encontrado!");
            }

            $this->json($pessoa);

        } catch (\Zend_Http_Exception $e) {
            $this->getResponse()->setHttpResponseCode(404);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 404);
            $this->json($error->toArray());
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }

    }
}

<?php

/**
 * Class ApiV2_TccController
 */
class ApiV2_TccController extends Ead1_AppApi implements \Ead1\Api
{

    /**
     *
     * @SWG\Get(path="/tcc/verificar-envio-tcc",
     *   tags={"tcc"},
     *   summary="Verifica se o Aluno tem TCC a ser enviado",
     *   description="Você precisa estar logado para usar este método.",
     *   operationId="verificar-envio-tcc",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="query",
     *     name="id_matricula",
     *     description="ID da Matrícula",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response=201,description="Operação efetuada com sucesso", @SWG\Schema(ref="#/definitions/VwEnvioTcc")),
     *   @SWG\Response(response=400, description="Dados inválidos para o cadastro", @SWG\Schema(ref="#/definitions/Error")),
     * )))))
     *
     */
    public function verificarEnvioTccAction(){

        try {

            $params = $this->getAllParams();
            unset($params['module'], $params['controller'], $params['action']);
            if (!$params['coduser'] || !$params['course']) {
                throw new \Exception("É necessário configurar no Moodle a passagem dos parâmetros 
                    das variáveis coduser e course");
            }

            $negocio = new \G2\Negocio\Avaliacao();
            $alunoGradeIntegracao = $negocio->findOneBy('\G2\Entity\VwAlunoGradeIntegracao', array(
                'st_codusuario'       => $params['coduser'],
                'st_codsistemacurso'  => $params['course']
            ));


            if ($alunoGradeIntegracao instanceof \G2\Entity\VwAlunoGradeIntegracao) {

                $situacaoTcc = $alunoGradeIntegracao->getId_situacaotcc();

                if ($situacaoTcc === \G2\Constante\Situacao::TB_ALOCACAO_EM_ANALISE
                || $situacaoTcc === \G2\Constante\Situacao::TB_ALOCACAO_AVALIADO_PELO_ORIENTADOR
                || $situacaoTcc === \G2\Constante\Situacao::TB_ALOCACAO_DEVOLVIDO_AO_TUTOR) {
                    return $this->json(array('id_situacaotcc' => $situacaoTcc));
                }

                $paramsEnvioTcc = array(
                    'id_matricula'  => $alunoGradeIntegracao->getId_matricula(),
                    'id_usuario'    => $alunoGradeIntegracao->getId_usuario()
                );

            } else {
                throw new \Exception("Aluno não possui integração.");
            }

            $ne = new \G2\Negocio\MensagemPortal();
            $msgsTcc = $ne->findVwEnvioTcc($paramsEnvioTcc);

            if ($msgsTcc && !empty($msgsTcc)) {
                $msgTcc = $msgsTcc[0];
                $idSituacaoTcc = $msgTcc->getId_situacaotcc();

                if ($idSituacaoTcc === \G2\Constante\Situacao::TB_ALOCACAO_PENDENTE
                    || $idSituacaoTcc === \G2\Constante\Situacao::TB_ALOCACAO_DEVOLVIDO_AO_ALUNO) {
                    if ($msgsTcc) {
                        $ear = $msgsTcc[0]->toBackboneArray();
                        unset($ear['em']);
                        $this->json($ear);
                    }
                } else {
                    throw new \Exception("Nenhum envio de TCC disponível");
                }
            } else {
                throw new \Exception("Nenhum envio de TCC disponível");
            }

        } catch (Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }

    }

    /**
     *
     * @SWG\Post(path="/tcc/enviar-tcc",
     *   tags={"tcc"},
     *   summary="Faz o upload do TCC do Aluno",
     *   description="Você precisa estar logado para usar este método.",
     *   operationId="enviar-tcc",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="formData",
     *     name="st_tituloavaliacao",
     *     description="Título da Avaliação",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     in="formData",
     *     name="id_matricula",
     *     description="ID da Matrícula",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     in="formData",
     *     name="file",
     *     description="Arquivo de TCC",
     *     required=true,type="file"
     *   ),
     *   @SWG\Response(response=201,description="Operação efetuada com sucesso", @SWG\Schema(ref="#/definitions/Ead1_Mensageiro")),
     *   @SWG\Response(response=400, description="Dados inválidos para o cadastro", @SWG\Schema(ref="#/definitions/Error")),
     * )))))
     *
     */
    public function enviarTccAction(){

        try {

            $arquivo = null;
            $st_tituloavaliacao = $this->getParam('st_tituloavaliacao', false);
            $id_matricula = $this->getParam('id_matricula', false);
            $id_usuario = $this->getuser()->getuser_id();

            if (isset($_FILES['file']['tmp_name']) && $_FILES['file']['tmp_name'] != '') {
                $arquivo = $_FILES['file'];
            }

            if(!$arquivo)
                throw new \Exception("O arquivo de TCC não foi enviado!");

            if(!$st_tituloavaliacao)
                throw new \Exception("Informe o título do seu TCC!");

            if(!$id_matricula)
                throw new \Exception("Informe o ID da Matrícula!");

            if(!$id_usuario)
                throw new \Exception("Informe o ID do Usuário!");

            $bo = new TCCBO();
            $retorno = $bo->enviarTcc($id_matricula,$id_usuario,$st_tituloavaliacao,$arquivo);
            if($retorno->getTipo()!==Ead1_IMensageiro::SUCESSO)
                throw new \Exception($retorno->getFirstMensagem());

            $this->json($retorno->toArray());

        } catch (Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }

    }


    public function indexAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     * Método responsável por retornar um item da Model/Collection/Consulta baseado no ID
     */
    public function getAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     * Método responsável por inserir dados na Model
     */
    public function createAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     * Método responsável por retornar atualizar os dados na Model
     */
    public function updateAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     * Método responsável por excluir os dados da Model/Collection/Consulta
     */
    public function deleteAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     * Método responsável por retornar todos os dados da Model/Collection/Consulta por página
     */
    public function pagingAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }
}

<?php

class ApiV2_LogController extends Ead1_AppApi implements \Ead1\Api
{
    /**
     *
     * @SWG\Definition(
     *   definition="LogResponse",
     *   @SWG\Property(property="retorno",type="boolean"),
     *   @SWG\Property(property="messagem",type="string"),
     *   @SWG\Property(property="token",type="string"),
     *   @SWG\Property(property="message",type="string"),
     *   @SWG\Property(property="type",type="string"),
     * )
     * @SWG\Definition(
     *   definition="Log",
     *   @SWG\Property(property="id_usuario",type="integer"),
     *   @SWG\Property(property="id_entidade",type="integer"),
     *   @SWG\Property(property="id_saladeaula",type="integer"),
     *   @SWG\Property(property="rota",type="string"),
     *   @SWG\Property(property="st_parametros",type="object")
     * )
     */


    public function indexAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, "Método ainda não implementado", 404);
        $this->_helper->json($error->toArray());
    }

    public function getAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, "Método ainda não implementado", 404);
        $this->_helper->json($error->toArray());
    }

    /**
     *
     * @SWG\Post(path="/log/create",
     *   tags={"log"},
     *   summary="Insere um novo log da funcionalidade",
     *   description="Você precisa estar logado para usar este método.",
     *   operationId="create",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="body",
     *     name="log",
     *     description="JSON com os dados",
     *     required=true,
     *     type="string",
     *     @SWG\Schema(
     *          ref="#/definitions/Log"
     *      )
     *   ),
     *   @SWG\Response(response=201,description="Operação efetuada com sucesso",
     *      @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/Log")
     *         )
     *   ),
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso", @SWG\Schema(ref="#/definitions/LogResponse")),
     *   @SWG\Response(response=400, description="Dados invalidos, cadastro nao efetuado", @SWG\Schema(ref="#/definitions/Error"))
     *   ),
     * )))))
     *
     */
    public function createAction()
    {
        try {
            //recebe os dados para cadastro
            $params = \Zend_Json::decode($this->getRequest()->getRawBody());

            /**
             * Verificaçoes em geral para evitar erros na montagem do dados.
             * Caso nao exista algum parametro cai no throw
             */
            if (!isset($params['id_usuario']) && empty($params['id_usuario'])) {
                throw new \Exception('O Id do usuário é obrigatório');
            }

            if (!isset($params['id_entidade']) && empty($params['id_entidade'])) {
                throw new \Exception('O Id da entidade é obrigatório');
            }

            if (!isset($params['rota']) && empty($params['rota'])) {
                throw new \Exception('A rota é obrigatório');
            }

            $pessoaNegocio = new \G2\Negocio\Pessoa();
            $matriculaNegocio = new \G2\Negocio\Matricula();
            $logAcessoNegocio = new \G2\Negocio\LogAcesso();
            $funcionalidadeNegocio = new \G2\Negocio\Funcionalidade();

            /**
             * Separa os valores enviados exemplo: app.financeiro.form, nesse caso vamos precisar somente dos
             * dois primeiros valores.
             * 0 => app
             * 1 => financeiro
             * 2 => form|show|QualquerOutroValor
             */
            $rota = explode('.', $params['rota']);
            $rotaFinal = $rota[0];
            //Verifica caso a rota nao for completa
            if (isset($rota[1]) && !empty($rota[1])) {
                $rotaFinal .= '.' . $rota[1];
            }

            //busca a funcionalidade
            $result = $funcionalidadeNegocio->findOneBy('G2\Entity\Funcionalidade', array(
                'st_urlcaminho' => $rotaFinal
            ));

            //Monta os dados para salvar
            $logAcesso = array(
                'id_usuario' => $params['id_usuario'],
                'id_entidade' => $params['id_entidade'],
                'id_funcionalidade' => $result ? $result->getId_funcionalidade() : 0,
                'id_sistema' => $params['id_sistema'],
                'st_urlcompleta' => $params['rota']
            );

            //Armazena a id matricula caso exista
            if (isset($params['st_parametros']['idMatriculaAtiva'])
                && !empty($params['st_parametros']['idMatriculaAtiva'])) {
                $id_matricula = $params['st_parametros']['idMatriculaAtiva'];

                $logAcesso['id_matricula'] = $id_matricula;
                unset($params['st_parametros']['idMatriculaAtiva']);

                /**
                 * Para que o log seja incluido na entidade da matricula do aluno, efetuo a busca abaixo caso tenha a
                 * matricula do aluno
                 */
                $matricula = $matriculaNegocio->retornaMatricula(array('id_matricula' => $id_matricula), true);

                if (is_array($matricula)
                    && isset($matricula['id_entidadematricula'])
                    && !empty($matricula['id_entidadematricula'])
                ) {
                    $logAcesso['id_entidade'] = $matricula['id_entidadematricula'];
                }
            }

            $usuario = $pessoaNegocio->retornaVwUsuarioPerfilEntidade(array(
                'id_usuario' => $params['id_usuario'],
                'id_entidade' => $logAcesso['id_entidade']
            ), true);

            if (is_array($usuario) && !empty($usuario)) {
                $logAcesso['id_perfil'] = $usuario['id_perfil'];
            }

            //Armazena a sala de aula caso exista
            if (isset($params['st_parametros']['idSalaDeAula']) && !empty($params['st_parametros']['idSalaDeAula'])) {
                $logAcesso['id_saladeaula'] = $params['st_parametros']['idSalaDeAula'];
                unset($params['st_parametros']['idSalaDeAula']);
            }

            //recebe os demais parametros e preenche
            if (isset($params['st_parametros']) && !empty($params['st_parametros'])) {
                $logAcesso['st_parametros'] = (string)json_encode($params['st_parametros']);
            }

            //salva os dados
            $result = $logAcessoNegocio->salvaLogAcesso($logAcesso, true);
            $this->getResponse()
                ->setHttpResponseCode(201)
                ->appendBody(json_encode($result->getFirstMensagem()));

        } catch (Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }
    }

    public function updateAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, "Método ainda não implementado", 404);
        $this->_helper->json($error->toArray());
    }

    public function deleteAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, "Método ainda não implementado", 404);
        $this->_helper->json($error->toArray());
    }

    public function pagingAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, "Método ainda não implementado", 404);
        $this->_helper->json($error->toArray());
    }

}

<?php

/**
 * Class AppApi_DisciplinaController
 */
class ApiV2_DisciplinaAlunoController extends Ead1_AppApi implements \Ead1\Api
{

    /**
     *
     *
     * @SWG\Definition(
     *     definition="DisciplinasAlunoMoodleMoodleResponse",
     *     @SWG\Property(property="cmid",type="integer", example="12"),
     *     @SWG\Property(property="instance",type="integer", example="5"),
     *     @SWG\Property(property="modname",type="string", example="page"),
     *     @SWG\Property(property="state",type="integer", example="0"),
     *     @SWG\Property(property="timecompleted",type="integer", example="1465829484"),
     *     @SWG\Property(property="tracking",type="integer", example="1")
     * ),
     *
     *
     * @SWG\Definition(
     *     definition="DisciplinasAlunoMoodleResponse",
     *     @SWG\Property(property="nu_concluidas",type="integer", example="40"),
     *     @SWG\Property(property="nu_naoconcluidas",type="integer", example="60"),
     *     @SWG\Property(property="nu_percentualconcluido",type="integer", example="40"),
     *     @SWG\Property(property="ar_atividades",type="array",@SWG\Items(ref="#/definitions/DisciplinasAlunoMoodleMoodleResponse")))
     * ),
     *
     *
     * @SWG\Definition(
     *     definition="DisciplinasAlunoResponse",
     *     @SWG\Property(property="id_disciplina",type="integer", example="8028"),
     *     @SWG\Property(property="id_saladeaula",type="integer", example="40734"),
     *     @SWG\Property(property="st_disciplina",type="string", example="Sociologia e Educação"),
     *     @SWG\Property(property="id_status",type="integer", example="2"),
     *     @SWG\Property(property="st_status",type="string", example="Não Iniciada"),
     *     @SWG\Property(property="id_categoriasala",type="integer", example="1"),
     *     @SWG\Property(property="st_coordenador",type="string"),
     *     @SWG\Property(property="id_professor",type="integer"),
     *     @SWG\Property(property="st_professor",type="string"),
     *     @SWG\Property(property="nu_diaspassados",type="integer", description="Quantidade de dias comparando a data atual com a data de início da sala", example="12"),
     *     @SWG\Property(property="nu_diassala",type="integer", description="Quantidade de dias em que a sala ficará aberta", example="90"),
     *     @SWG\Property(property="nu_diaspercentual",type="integer", description="Percentual de dias desde que a sala foi aberta", example="12"),
     *     @SWG\Property(property="st_statuscomplete",type="string", description="Texto com base nos dias da sala e no que ele fez no moodle, pode ser Em dia, Atrasado, Adiantado ou Completo", example="Em dia"),
     *
     *
     *     @SWG\Property(property="ar_moodle",type="array",@SWG\Items(ref="#/definitions/DisciplinasAlunoMoodleResponse")))
     * ),
     *
     *
     * @SWG\Get(path="/disciplina-aluno",
     *   tags={"disciplina-aluno"},
     *   summary="Retorna as disciplinas do Aluno",
     *   description="Você precisa estar logado para usar este método",
     *   operationId="getAll",
     *   produces={"application/json"},
     *
     *   @SWG\Parameter(
     *     name="id_entidade",
     *     in="query",
     *     description="ID da Entidade",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="id_matricula",
     *     in="query",
     *     description="ID da Matrícula",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="bl_moodle",
     *     in="query",
     *     description="1 ou 0 para definir se o sistema deve ir ao Moodle buscar os dados da Disciplina",
     *     required=false,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="id_saladeaula",
     *     in="query",
     *     description="Id da Sala de Aula, opcional",
     *     required=false,
     *     type="integer"
     *   ),
     *
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/DisciplinasAlunoResponse")
     *         )
     *   ),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="Nenhum registro encontrado", @SWG\Schema(ref="#/definitions/Error"))
     *
     * )))
     *
     *
     */
    public function indexAction()
    {

        try {

            $id_entidade = (int)$this->_getParam('id_entidade', null);
            if (!$id_entidade)
                throw new \Exception("Você precisa informar o id da Entidade");

            $id_matricula = (int)$this->_getParam('id_matricula', null);
            if (!$id_matricula)
                throw new \Exception("Você precisa informar o id da Matrícula");

            $id_saladeaula = (int)$this->_getParam('id_saladeaula', null);

            $bl_moodle = (int)$this->_getParam('bl_moodle', 0);

            $negocio = new \G2\Negocio\Disciplina();
            $result = $negocio->retornarDisciplinasAluno($this->getuser()->getuser_id(), array(
                'id_saladeaula' => $id_saladeaula,
                'id_entidade' => $id_entidade,
                'id_matricula' => $id_matricula,
            ), $bl_moodle);

            if (!$result) {
                throw new \Zend_Http_Exception("Nenhum Curso encontrado!");
            }


            $this->json($result);

        } catch (\Zend_Http_Exception $e) {
            $this->getResponse()->setHttpResponseCode(404);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 404);
            $this->json($error->toArray());
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }

    }


    /**
     *
     *
     * @SWG\Get(path="/disciplina-aluno/list-conteudo-disciplina",
     *   tags={"disciplina-aluno"},
     *   summary="Retorna a lista de conteúdos para das disciplinas",
     *   description="Você precisa estar logado para usar este método",
     *   operationId="list-conteudo-disciplina",
     *   produces={"application/json"},
     *
     *   @SWG\Parameter(
     *     name="id_disciplina",
     *     in="query",
     *     description="ID da Disciplina",
     *     required=true,
     *     type="integer"
     *   ),
     *
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/DisciplinaConteudo")
     *         )
     *   ),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="Nenhum registro encontrado", @SWG\Schema(ref="#/definitions/Error"))
     *
     * )))
     *
     *
     */
    public function listConteudoDisciplinaAction()
    {

        try {

            $id_disciplina = (int)$this->_getParam('id_disciplina', null);
            if (!$id_disciplina) {
                throw new \Exception("Você precisa informar o id da Disciplina");
            }

            $negocio = new \G2\Negocio\Disciplina();
            $pagination = $negocio->paging('\G2\Entity\DisciplinaConteudo',
                array('bl_ativo' => true, 'id_disciplina' => $id_disciplina), null, $page = 1, 50000, false);
            if (!$pagination->gettotal()) {
                throw new \Zend_Http_Exception("Nenhum resultado encontrado!");
            }

            $this->json($pagination->getdata());

        } catch (\Zend_Http_Exception $e) {
            $this->getResponse()->setHttpResponseCode(404);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 404);
            $this->json($error->toArray());
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }

    }


    /**
     * Fiquei na dúvida aqui @elcio
     */
    public function getAction()
    {
        try {

            if (!$this->getRequest()->isOptions()) {
                $negocio = new \G2\Negocio\GerenciaSalas();

                $params = $this->getRequest()->getParams();
                unset($params['module'], $params['controller'], $params['action']);

                $result = $negocio->retornaUnicoRegistroVwAlunoGradeIntegracao($params);
                if (!$result) {
                    throw new \Zend_Http_Exception("Nenhum resultado encontrado!");
                }

                $toArray = $negocio->toArrayEntity($result);
                $toArray['dt_abertura'] = $result->getDt_abertura()->format('d/m/Y');

                $dt = new \DateTime($result->getDt_encerramentosala());
                $toArray['dt_encerramentosala'] = $dt->format('d/m/Y');

                $this->_helper->json($toArray);
            }
        } catch (\Zend_Http_Exception $e) {
            $this->getResponse()->setHttpResponseCode(404);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 404);
            $this->json($error->toArray());
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }
    }


    /**
     * Método responsável por inserir dados na Model
     */
    public function createAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, "Método ainda não implementado", 404);
        $this->_helper->json($error->toArray());
    }

    /**
     * Método responsável por retornar atualizar os dados na Model
     */
    public function updateAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, "Método ainda não implementado", 404);
        $this->_helper->json($error->toArray());
    }

    /**
     * Método responsável por excluir os dados da Model/Collection/Consulta
     */
    public function deleteAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, "Método ainda não implementado", 404);
        $this->_helper->json($error->toArray());
    }

    /**
     * Método responsável por retornar todos os dados da Model/Collection/Consulta por página
     */
    public function pagingAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, "Método ainda não implementado", 404);
        $this->_helper->json($error->toArray());
    }


}
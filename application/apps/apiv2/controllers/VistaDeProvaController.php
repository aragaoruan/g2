<?php
/**
 * Created by PhpStorm.
 * User: alexalexandre
 * Date: 22/12/17
 * Time: 14:04
 */

class ApiV2_VistaDeProvaController extends Ead1_AppApi implements \Ead1\Api
{
    /**
     * @var \G2\Negocio\Maximize $negocio
     */
    private $negocio;

    public function init()
    {
        $this->negocio = new \G2\Negocio\Maximize();

        return parent::init(); // TODO: Change the autogenerated stub
    }

    public function indexAction()
    {
        $res = $this->negocio->verificaAlunoIntegrado($this->getParam("idUsuario"));
        if (!is_array($res)) {
            $this->json(["url" => $res]);
        }
        $this->json($res);
    }

    /**
     * Função responsável por verificar se a entidade do aluno logada tem permissão
     * Para exibir ou não o botão vista de provas. No novo portal
     * POR-395
     */
    public function verificarPermissaoAction()
    {
        $configuracaoEntidade = new \G2\Negocio\ConfiguracaoEntidade();
        $resultConfigEntidade = $configuracaoEntidade->findConfiguracaoEntidade($this->getParam("id_entidade"), true);
        if ($resultConfigEntidade['bl_minhasprovas']) {
            $this->json(["bl_minhasprovas" => true]);
        }

        $this->json(["bl_minhasprovas" => false]);
    }

    public function createAction()
    {
        // TODO: Implement createAction() method.
    }

    public function updateAction()
    {
        // TODO: Implement updateAction() method.
    }

    public function deleteAction()
    {
        // TODO: Implement deleteAction() method.
    }

    public function getAction()
    {
        // TODO: Implement getAction() method.
    }

    public function pagingAction()
    {
        // TODO: Implement pagingAction() method.
    }

}

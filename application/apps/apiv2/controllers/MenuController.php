<?php

use Ead1\Api;

class ApiV2_MenuController extends Ead1_AppApi implements Api {

    public function init()
    {
        return parent::init();
    }

    /**
     * Método responsável por criar um item na Model
     */
    public function indexAction() {
        // TODO: Implement getAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     * Método responsável por criar um item na Model
     */
    public function createAction()
    {
        // TODO: Implement getAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     * Método responsável por retornar um item da Model
     */
    public function getAction()
    {
        // TODO: Implement getAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     * Método responsável por retornar atualizar os dados na Model
     */
    public function updateAction()
    {
        // TODO: Implement updateAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     * Método responsável por excluir os dados da Model
     */
    public function deleteAction()
    {
        // TODO: Implement deleteAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     * Método responsável por retornar todos os dados da Model
     */
    public function pagingAction()
    {
        // TODO: Implement pagingAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     *
     *   @SWG\Definition(
     *     definition="MenuResponse",
     *     @SWG\Property(property="id_perfil",type="integer"),
     *     @SWG\Property(property="id_perfilpai",type="integer"),
     *     @SWG\Property(property="st_nomeperfil",type="string"),
     *     @SWG\Property(property="bl_ativo",type="boolean"),
     *     @SWG\Property(property="id_situacao", type="integer"),
     *     @SWG\Property(property="id_entidade", type="integer"),
     *     @SWG\Property(property="bl_padrao", type="boolean"),
     *     @SWG\Property(property="id_entidadeclasse", type="integer"),
     *     @SWG\Property(property="dt_cadastro", type="datetime"),
     *     @SWG\Property(property="id_usuariocadastro", type="integer"),
     *     @SWG\Property(property="id_perfilpedagogico", type="integer"),
     *     @SWG\Property(property="id_sistema", type="integer"),
     *     @SWG\Property(property="id_funcionalidade", type="integer"),
     *     @SWG\Property(property="st_label", type="string"),
     *     @SWG\Property(property="bl_obrigatorio", type="boolean"),
     *     @SWG\Property(property="bl_visivel", type="boolean"),
     *     @SWG\Property(property="nu_ordem", type="integer"),
     *     @SWG\Property(property="st_funcionalidade", type="string"),
     *     @SWG\Property(property="id_funcionalidadepai", type="integer"),
     *     @SWG\Property(property="st_classeflex", type="string"),
     *     @SWG\Property(property="st_urlicone", type="string"),
     *     @SWG\Property(property="id_tipofuncionalidade", type="integer"),
     *     @SWG\Property(property="st_classeflexbotao", type="string"),
     *     @SWG\Property(property="bl_pesquisa", type="boolean"),
     *     @SWG\Property(property="bl_lista", type="boolean"),
     *     @SWG\Property(property="st_ajuda", type="string"),
     *     @SWG\Property(property="st_target", type="string"),
     *     @SWG\Property(property="st_urlcaminho", type="string"),
     *     @SWG\Property(property="bl_relatorio", type="boolean"),
     *     @SWG\Property(property="st_urlcaminhoedicao", type="string"),
     *     @SWG\Property(property="bl_delete", type="boolean"),
     *   ),
     */

    /**
     *  @SWG\Get(path="/menu/menu-side-bar",
     *   tags={"menu"},
     *   summary="Retorna o menu do novo portal baseado na entidade",
     *   description="Retorno do menu para o novo portal",
     *   operationId="get",
     *   produces={"application/json"},
     *
     *     @SWG\Parameter(
     *     name="id_entidade",
     *     in="query",
     *     description="id_entidade",
     *     required=true,
     *     type="integer"
     *   ),
     *
     *
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/MenuResponse")
     *         )
     *   ),
     *
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     *
     * )))
     */
    public function menuSideBarAction() {
        try {

            $negocio = new \G2\Negocio\Funcionalidade();
            /** @var Ead1_Mensageiro $mensageiro */
            $mensageiro = $negocio->retornarMenuNovoPortal($this->_getParam('id_entidade', null));

            if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Exception('Não foi possível retornar os menus');
            }

            $this->json($mensageiro->getMensagem());

        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }
    }
}
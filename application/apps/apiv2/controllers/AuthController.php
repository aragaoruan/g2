<?php

class ApiV2_AuthController extends Ead1_AppApi
{
    /**
     *
     * @SWG\Definition(
     *   definition="AuthResponse",
     *   @SWG\Property(property="retorno",type="boolean"),
     *   @SWG\Property(property="messagem",type="string"),
     *   @SWG\Property(property="token",type="string"),
     *   @SWG\Property(property="message",type="string"),
     *   @SWG\Property(property="type",type="string"),
     * )
     * @SWG\Definition(
     *   definition="AuthUser",
     *   @SWG\Property(property="login",type="string"),
     *   @SWG\Property(property="senha",type="string"),
     *     @SWG\Property(property="entidadeId",type="integer")
     * )
     *
     * @SWG\Post(path="/auth/login",
     *   tags={"auth"},
     *   summary="Loga o Usuário no sistema",
     *   description="Faz o login do Usuário no sistema baseado em nome de Usuário e senha",
     *   operationId="loginAction",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="user",
     *     in="body",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/AuthUser")
     *   ),
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso", @SWG\Schema(ref="#/definitions/AuthResponse")),
     *   @SWG\Response(response=400, description="Nome de Usuário e/ou senha inválidos", @SWG\Schema(ref="#/definitions/Error"))
     * )))
     *
     */
    public function loginAction()
    {

        try {

            $usuariologado = false;
            $params = $data = Zend_Json::decode($this->getRequest()->getRawBody());

            if (!isset($params['entidadeId']) || !$params['entidadeId'])
                throw new \Exception("Entidade inválida!");

            if (isset($params['login']) && isset($params['senha'])) {
                $user = new \G2\Entity\VwValidaLoginPortal();
                $user->setSt_login($params['login']);
                $user->setSt_email($params['login']);
                $user->setSt_senha(md5($params['senha']));
                $user->setSt_senhaentidade($params['senha']);
                $user->setBl_acessoapp(true);

                $login = new \G2\Negocio\Auth();
                $auth = $login->retornaSelectLoginUsuarioPortal($user);
                if($auth)
                    $usuariologado = $login->login($auth, $params['entidadeId']);

                if($usuariologado)
                    $this->json($usuariologado);



            } else {
                throw new \Exception('Usuário e senha obrigatórios.');
            }
            throw new \Exception('Nenhum usuário encontrado com os dados enviados.');


        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }

    }

    /**
     * @SWG\Get(path="/auth/logout",
     *   tags={"auth"},
     *   summary="Faz o logout do Usuário no sistema",
     *   description="Faz o logout do Usuário no sistema",
     *   operationId="logoutAction",
     *   produces={"application/json"},
     *   parameters={},
     *   @SWG\Response(response="default", description="Operação efetuada com sucesso"),
     *   @SWG\Response(response=400, description="Ocorreu algum erro ao tentar fazer o logout do Usuário", @SWG\Schema(ref="#/definitions/Error"))
     * )
     *
     */
    public function logoutAction()
    {
    }


    /**
     *
     * @SWG\Definition(
     *  definition="AuthRecuperarSenhaResponse",
     *  @SWG\Property(property="retorno",type="boolean"),
     *  @SWG\Property(property="message",type="string"),
     *  @SWG\Property(property="type",type="string"),
     * ),
     * @SWG\Definition(
     *   definition="AuthRecuperarSenhaRequest",
     *   @SWG\Property(property="st_email",type="string"),
     *   @SWG\Property(property="id_entidade",type="integer")
     * )
     *
     * @SWG\Post(path="/auth/recuperar-senha",
     *   tags={"auth"},
     *   summary="Recupera a senha do usuário",
     *   description="Faz a recuperação de senha do usuário",
     *   operationId="recuperarSenhaAction",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/AuthRecuperarSenhaRequest")
     *   ),
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso", @SWG\Schema(ref="#/definitions/AuthRecuperarSenhaResponse")),
     *   @SWG\Response(response=400, description="Erro ao tentar recuperar senha", @SWG\Schema(ref="#/definitions/Error"))
     * )))
     *
     */
    public function recuperarSenhaAction()
    {

        try {

            if (!$this->getRequest()->isOptions()) {

                $params = \Zend_Json::decode($this->getRequest()->getRawBody());

                unset($params['module'], $params['controller'], $params['action']);

                //instancia a TO e a BO do portal
                $vwPessoaTO = new VwPessoaTO();
                $loginBO = new PortalLoginBO();
                //Seta os parametros na to
                $vwPessoaTO->setSt_email(isset($params['st_email']) ? $params['st_email'] : NULL);
                $vwPessoaTO->setId_entidade(isset($params['id_entidade']) ? $params['id_entidade'] : NULL);

                //chama o método do recuperar senha da BO
                $return = $loginBO->recuperarSenha($vwPessoaTO);

                $this->json([
                    'retorno' => (bool)$return->getTipo(),
                    'message' => $return->getFirstMensagem(),
                    'type' => $return->getType()
                ]);

            }

        } catch (\Exception $e) {

            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());

        }

    }


    /**
     *
     * @SWG\Definition(
     *   definition="AuthAutenticacaoDiretaRequest",
     *   @SWG\Property(property="id_usuariooriginal",type="integer", description="ID do Professor/Tutor que deseja logar como o Aluno"),
     *   @SWG\Property(property="id_entidade",type="integer", description="ID da Entidade do Aluno"),
     *   @SWG\Property(property="id_usuario",type="integer", description="ID do Aluno"),
     *   @SWG\Property(property="st_urlacesso",type="string", description="Token temporário do aluno"),
     *   @SWG\Property(property="bl_gravalog",type="boolean", description="define se grava log ou não")
     * )
     *
     * @SWG\Post(path="/auth/autenticacao-direta",
     *   tags={"auth"},
     *   summary="Faz o login do acessar como Aluno",
     *   operationId="autenticacaoDiretaAction",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/AuthAutenticacaoDiretaRequest")
     *   ),
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso", @SWG\Schema(ref="#/definitions/AuthResponse")),
     *   @SWG\Response(response=400, description="Erro ao tentar recuperar senha", @SWG\Schema(ref="#/definitions/Error"))
     * )))
     *
     */
    public function autenticacaoDiretaAction()
    {
        try {

            $params = Zend_Json::decode($this->getRequest()->getRawBody());
            if (!$params)
                throw new \Exception("Você precisa enviar os dados so Usuário a serem salvos");

            if(!isset($params['id_usuariooriginal']) || !$params['id_usuariooriginal']) throw new \Exception("O id_usuariooriginal é Obrigatório!");
            if(!isset($params['id_entidade']) || !$params['id_entidade']) throw new \Exception("O id_entidade é Obrigatório!");
            if(!isset($params['id_usuario']) || !$params['id_usuario']) throw new \Exception("O id_usuario é Obrigatório!");
            if(!isset($params['st_urlacesso']) || !$params['st_urlacesso']) throw new \Exception("A st_urlacesso é Obrigatória!");
            if(!isset($params['bl_gravalog'])) $params['bl_gravalog'] = 'sim';

            $usTO = new VwUsuarioPerfilPedagogicoTO();
            $usTO->setId_usuario($params['id_usuario']);
            $usTO->setSt_urlacesso($params['st_urlacesso']);
            $usTO->setId_entidade($params['id_entidade']);
            $usTO->setId_entidadeportal($params['id_entidade']);
            $usTO->fetch(false, true, true);

            $l = $params['bl_gravalog']; // não se se será aqui a utilização dessa flag

            $uOri = new \G2\Entity\Usuario();
            $uOri->find($params['id_usuariooriginal']);
            if(!$uOri->getSt_nomecompleto())
                throw new \Exception("Usuário que deseja acessar como o Aluno não foi encontrado!");

            if($usTO->getId_matricula()==false){
                throw new \Exception("Usuário não encontrado, favor verificar se a URL ainda é válida no G2!");
            }

            $user = new \G2\Entity\VwValidaLoginPortal();
            $user->setSt_login($usTO->getSt_login());
            $user->setSt_email($usTO->getSt_email());
            $user->setSt_senha($usTO->getSt_senha());
            $user->setSt_senhaentidade($usTO->getSt_senhaentidade());
            $user->setBl_acessoapp(true);

            $login = new \G2\Negocio\Auth();
            $auth = $login->retornaSelectLoginUsuarioPortal($user);

            $usuariologado = $login->login($auth, $usTO->getId_entidade(), $uOri);

            if($usuariologado)
                $this->json($usuariologado);


        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }
    }

}
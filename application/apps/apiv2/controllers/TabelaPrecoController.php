<?php

/**
 * Class AppApi_TabelaPrecoController
 * Controller REST para Tabela Preco
 */
class ApiV2_TabelaPrecoController extends Ead1_AppApi implements \Ead1\Api
{
  /**
   * @SWG\Definition(
   *     definition="TabelaPrecoResponse",
   *     @SWG\Property(property="id_entidade",type="integer"),
   * ),
   *
   *
   * @SWG\Get(path="/tabela-preco/",
   *   tags={"tabela-preco"},
   *   summary="Retorna um registro da tabela de preço",
   *   description="Você precisa estar logado para usar este método.",
   *   operationId="get",
   *   produces={"application/json"},
   *   @SWG\Parameter(
   *     name="id_entidade",
   *     in="query",
   *     description="ID da entidade da matricula do usuario",
   *     required=true,
   *     type="integer"
   *   ),
   *   @SWG\Response(response="default", description="Operação efetuada com sucesso", @SWG\Schema(ref="#/definitions/TabelaPrecoResponse")),
   *   @SWG\Response(response=400, description="ID inválido", @SWG\Schema(ref="#/definitions/Error")),
   *   @SWG\Response(response=404, description="A Tabela Preço não foi encontrada", @SWG\Schema(ref="#/definitions/Error"))
   * )))))
   *
   */
  public function indexAction()
  {
    try {
      $id_entidade = $this->_getParam('id_entidade');
      if (!$id_entidade)
        throw new \Exception("Você precisa informar o id da entidade");

      $entidadeNegocio = new \G2\Negocio\Entidade();
      $entidade = $entidadeNegocio->findEntidade($id_entidade, true);

      $configuracaoEntidadeNegocio = new \G2\Negocio\ConfiguracaoEntidade();
      $configuracaoEntidade = $configuracaoEntidadeNegocio->findConfiguracaoEntidade($id_entidade, true);

      if (isset($configuracaoEntidade['id_tabelapreco']) && !empty($configuracaoEntidade['id_tabelapreco'])) {
        $uploadNegocio = new \G2\Negocio\Upload();
        $upload = $uploadNegocio->findUpload($configuracaoEntidade['id_tabelapreco']['id_upload']);
      }
      
      if (isset($upload) && !empty($upload)) {
        $result = array(
            'type' => \Ead1_IMensageiro::SUCESSO,
        );
      } else {
        $result = array(
            'type' => \Ead1_IMensageiro::ERRO,
            'data' => "Não foi possível localizar configurações para tabela de preço, entre em contato com {$entidade['st_nomeentidade']}."
        );
      }

      $this->json(array($result));

    } catch (\Exception $e) {
      $this->getResponse()->setHttpResponseCode(400);
      $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
      $this->json($error->toArray());
    }
  }

  public function getAction()
  {
    // TODO: Implement indexAction() method.
    $this->getResponse()->setHttpResponseCode(404);
    $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
    $this->json($error->toArray());
  }

  /**
   * Método responsável por inserir dados na Model
   */
  public function createAction()
  {
    // TODO: Implement createAction() method.
    $this->getResponse()->setHttpResponseCode(404);
    $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
    $this->json($error->toArray());
  }

  /**
   * Método responsável por retornar atualizar os dados na Model
   */
  public function updateAction()
  {
    // TODO: Implement updateAction() method.
    $this->getResponse()->setHttpResponseCode(404);
    $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
    $this->json($error->toArray());
  }

  /**
   * Método responsável por excluir os dados da Model/Collection/Consulta
   */
  public function deleteAction()
  {
    // TODO: Implement deleteAction() method.
    $this->getResponse()->setHttpResponseCode(404);
    $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
    $this->json($error->toArray());
  }

  /**
   * Método responsável por retornar todos os dados da Model/Collection/Consulta por página
   */
  public function pagingAction()
  {
    // TODO: Implement pagingAction() method.
    $this->getResponse()->setHttpResponseCode(404);
    $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
    $this->json($error->toArray());
  }
}
<?php

/**
 * Class AppApi_ResumoFinanceiroController
 * Controller REST para financeiro
 */
class ApiV2_CampanhaComercialController extends Ead1_AppApi implements \Ead1\Api
{
    /**
     * @SWG\Get(path="/campanha-comercial",
     *   tags={"campanha-comercial"},
     *   summary="Retorna as campanhas comerciais ativas",
     *   description="Retorna as campanhas comerciais ativas",
     *   operationId="getAll",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="bl_portaldoaluno",
     *     in="query",
     *     description="Se é para o portal ou não",
     *     required=false,
     *     type="boolean"
     *   ),
     *   @SWG\Parameter(
     *     name="bl_mobile",
     *     in="query",
     *     description="Se é para o app ou não",
     *     required=false,
     *     type="boolean"
     *   ),
     *   @SWG\Parameter(
     *     name="order",
     *     in="query",
     *     description="Columa pela qual quer ordenar",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="direction",
     *     in="query",
     *     description="ASC ou DESC",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="columns",
     *     in="query",
     *     description="Quais colunas quer retornar",
     *     required=false,
     *     type="array"
     *   ),
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/VwCampanhaComercial")
     *         )
     *   ),
     *   @SWG\Response(response=400, description="Ocorreu algum erro ao tentar buscar o resumo financeiro",
     *     @SWG\Schema(ref="#/definitions/Error"))
     * )
     *
     */
    public function indexAction()
    {
        try {

            $id_matricula = $this->getParam('id_matricula', null);
            if (!$id_matricula) {
                throw new \Exception('Você precisa informar o id_matricula');
            }

            $idEntidade = $this->getuser()->getentidade_id();//pega o id da entidade

            $params = array();
            $params['id_matricula'] = $id_matricula;
            $params['bl_vigente'] = 1;
            $params['id_situacao'] = \G2\Constante\Situacao::TB_CAMPANHACOMERCIAL_ATIVO;
            $params['bl_portaldoaluno'] = $this->_getParam('bl_portaldoaluno', null);
            $params['bl_mobile'] = $this->_getParam('bl_mobile', null);
            $params['order'] = $this->_getParam('order', null);
            $params['direction'] = $this->_getParam('direction', null);
            $params['columns'] = $this->_getParam('columns', null);
            $negocio = new \G2\Negocio\CampanhaComercial();
            $result = $negocio->retornarVwCampanhaComercial($params, $idEntidade, false);

            $this->json($result);

        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }

    }


    /**
     * @SWG\Get(path="/resumo-financeiro/get/id/{id_lancamento}/bl_novoportal/{bl_novoportal}/bl_mobile/{bl_mobile}",
     *   tags={"resumo-financeiro"},
     *   summary="Retorna um registro do resumo financeiro baseado no lançamento",
     *   description="Você precisa estar logado para usar este método.",
     *   operationId="get",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="ID do Lançamento",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response="default", description="Operação efetuada com sucesso",
     *     @SWG\Schema(ref="#/definitions/ResumoFinanceiroResponse")),
     *   @SWG\Response(response=400, description="ID inválido", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="Lançamento não encontrado", @SWG\Schema(ref="#/definitions/Error"))
     * )))))
     *
     */
    public function getAction()
    {
        try {

            $idEntidade = $this->getuser()->getentidade_id();//pega o id da entidade

            $id_campanhacomercial = $this->_getParam('id', null);
            if (!$id_campanhacomercial) {
                throw new \Exception('Você precisa informar o id_campanhacomercial');
            }

            $params = array();
            $params['id_campanhacomercial'] = $id_campanhacomercial;
            $params['bl_vigente'] = 1;
            $params['id_situacao'] = \G2\Constante\Situacao::TB_CAMPANHACOMERCIAL_ATIVO;
            $params['bl_portaldoaluno'] = $this->_getParam('bl_portaldoaluno', null);
            $params['bl_mobile'] = $this->_getParam('bl_mobile', null);

            $negocio = new \G2\Negocio\CampanhaComercial();
            $result = $negocio->retornaVwCampanhaComercial($params, $idEntidade, false);

            $params['id_usuario'] = $this->getuser()->getuser_id();
            $negocio->marcarCampanhaViualizada($params);

            if (!$result) {
                throw new \Exception("Nenhuma campanha encontrada");
            }
            $this->json($result);

        } catch (\Zend_Http_Exception $e) {
            $this->getResponse()->setHttpResponseCode(404);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 404);
            $this->json($error->toArray());
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }
    }


    /**
     * Método responsável por retornar todos os dados da Model/Collection/Consulta por página
     */
    public function countAction()
    {
        try {
            $id_matricula = $this->getParam('id_matricula', null);
            if (!$id_matricula) {
                throw new \Exception('Você precisa informar o id_matricula');
            }

            $idEntidade = $this->getuser()->getentidade_id();//pega o id da entidade

            $params = array();
            $params['id_matricula'] = $id_matricula;
            $params['bl_vigente'] = 1;
            $params['id_situacao'] = \G2\Constante\Situacao::TB_CAMPANHACOMERCIAL_ATIVO;
            $params['bl_portaldoaluno'] = $this->_getParam('bl_portaldoaluno', null);
            $params['bl_mobile'] = $this->_getParam('bl_mobile', null);
            $params['bl_visualizado'] = $this->_getParam('bl_visualizado', 0);

            $negocio = new \G2\Negocio\CampanhaComercial();
            $result = $negocio->retornarQuantidadeCampanha($params, $idEntidade);

            if ($result) {
                $this->json($result);
            }

        } catch (\Zend_Http_Exception $e) {
            $this->getResponse()->setHttpResponseCode(404);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 404);
            $this->json($error->toArray());
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }
    }

    /**
     * Método responsável por inserir dados na Model
     */
    public function createAction()
    {
        // TODO: Implement createAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     * Método responsável por retornar atualizar os dados na Model
     */
    public function updateAction()
    {
        // TODO: Implement updateAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     * Método responsável por excluir os dados da Model/Collection/Consulta
     */
    public function deleteAction()
    {
        // TODO: Implement deleteAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     * Método responsável por retornar todos os dados da Model/Collection/Consulta por página
     */
    public function pagingAction()
    {
        // TODO: Implement pagingAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

}

<?php

class ApiV2_TipoAtividadeComplementarController extends Ead1_AppApi implements \Ead1\Api
{
    /**
     * @var \G2\Negocio\AtividadeComplementar
     */
    private $negocio;

    public function init()
    {
        $this->negocio = new \G2\Negocio\Negocio();
        return parent::init();
    }

    /**
     * @SWG\Definition(
     *   definition="TipoAtividadeComplementar",
     *   @SWG\Property(property="id_tipoatividadecomplementar",type="integer"),
     *   @SWG\Property(property="st_tipoatividadecomplementar",type="string"),
     *   @SWG\Property(property="id_usuariocadastro",type="integer"),
     *   @SWG\Property(property="id_entidadecadastro",type="integer"),
     *   @SWG\Property(property="dt_cadastro",type="string"),
     *   @SWG\Property(property="st_resumo",type="string"),
     *   @SWG\Property(property="bl_ativo",type="boolean")
     * )
     *
     * @SWG\Get(path="/tipo-atividade-complementar",
     *   tags={"atividade-commplementar"},
     *   summary="Retorna os tipos de atividade complementar",
     *   description="Necessário Login.",
     *   operationId="getAll",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="id_matricula",
     *     in="query",
     *     description="Id da Matrícula",
     *     required=false,
     *     type="integer",
     *     example="0"
     *   ),
     *
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/TipoAtividadeComplementar")
     *         )
     *   ),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="Nenhum registro encontrado", @SWG\Schema(ref="#/definitions/Error"))
     *
     * )))
     *
     */
    public function indexAction()
    {
        try {
            $results = $this->negocio->findBy('\G2\Entity\TipoAtividadeComplementar', array(
                'bl_ativo' => true
            ), array('st_tipoatividadecomplementar' => 'ASC'));

            if (!$results) {
                throw new \Zend_Http_Exception('Nenhum resultado encontrado!');
            }

            $this->json($this->negocio->toArrayEntity($results));
        } catch (\Zend_Http_Exception $e) {
            $this->getResponse()->setHttpResponseCode(404);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 404);
            $this->json($error->toArray());
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }
    }

    public function getAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    public function createAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    public function updateAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, "Método ainda não implementado", 404);
        $this->_helper->json($error->toArray());
    }

    public function deleteAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, "Método ainda não implementado", 404);
        $this->_helper->json($error->toArray());
    }

    public function pagingAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, "Método ainda não implementado", 404);
        $this->_helper->json($error->toArray());
    }

}

<?php

class ApiV2_SalaAulaCursoController extends Ead1_AppApi
{

    /**
     * @var \G2\Negocio\SalaDeAula
     */
    private $negocio;

    public function init()
    {
        $this->negocio = new \G2\Negocio\SalaDeAula();
        parent::init();
    }


    /**
     * TODO verificar se essa api esta sendo utilizada em outro lugar, este método foi criando em CursoAlunoController
     */
    public function indexAction()
    {
        $params = $this->getAllParams();

        unset($params['controller'], $params['action'], $params['module']);
        $result = $this->negocio->retornaVwSalaDisciplinaAlocacao($params);
        $arrReturn = [];
        if ($result) {
            foreach ($result as $sala) {
                if (!is_null($sala->getId_alocacao())) {
                    $arrReturn[] = $this->negocio->toArrayEntity($sala);
                }
            }
        }
        unset($result);
        $this->getResponse()
            ->appendBody(json_encode($arrReturn));


    }

    public function getAction()
    {

    }

    public function postAction()
    {

    }

    public function putAction()
    {

    }

    public function deleteAction()
    {

    }

    public function headAction()
    {

    }
}
<?php

class ApiV2_PrimeiroAcessoController extends Ead1_AppApi implements \Ead1\Api
{
  /**
   *
   * @SWG\Definition(
   *   definition="PrimeiroAcessoResponse",
   *   @SWG\Property(property="retorno",type="boolean"),
   *   @SWG\Property(property="messagem",type="string"),
   *   @SWG\Property(property="type",type="string"),
   * )
   * @SWG\Definition(
   *   definition="PrimeiroAcesso",
   *   @SWG\Property(property="id_entidade",type="integer"),
   *   @SWG\Property(property="id_matricula",type="integer"),
   * )
   *
   *
   * @SWG\Get(path="/primeiro-acesso/",
   *   tags={"primeiro-acesso"},
   *   summary="Retorna o texto de contrato para o aceite no primeiro acesso",
   *   description="Você precisa estar logado para usar este método.",
   *   operationId="get",
   *   produces={"application/json"},
   *   @SWG\Parameter(
   *     name="id_matricula",
   *     in="query",
   *     description="ID da matricula do usuario",
   *     required=true,
   *     type="integer"
   *   ),
   *   @SWG\Response(response="default", description="Operação efetuada com sucesso", @SWG\Schema(ref="#/definitions/PrimeiroAcessoResponse")),
   *   @SWG\Response(response=400, description="ID inválido", @SWG\Schema(ref="#/definitions/Error")),
   *   @SWG\Response(response=404, description="Os dados não foram encontrados", @SWG\Schema(ref="#/definitions/Error"))
   * )))))
   *
   */
  public function indexAction()
  {
    try {
      $id_matricula = $this->getRequest()->getParam('id_matricula');

      if (!$id_matricula)
        throw new \Exception("Você precisa informar o id da matricula");

      $negocio = new \G2\Negocio\Negocio();
      $textoSistemaBO = new TextoSistemaBO();
      $contratoNegocio = new \G2\Negocio\Contrato();
      $matriculaNegocio = new \G2\Negocio\Matricula();
      $ocorrenciaNegocio = new \G2\Negocio\Ocorrencia();
      $textoSistemaNegocio = new \G2\Negocio\TextoSistema();
      $primeiroAcessoNegocio = new \G2\Negocio\PrimeiroAcesso();

      //busca dos dados da matricula
      $matricula = $matriculaNegocio->getRepository('\G2\Entity\Matricula')->retornaVendaMatricula(array('id_matricula' => $id_matricula));

      //busca os dados do contrato
      $contrato = $contratoNegocio->retornaContratoVenda($matricula['id_venda'], $matricula['id_entidade']);
      $contrato = $negocio->toArrayEntity($contrato);

      //busca o texto do contrato
      $dadosContrato = $textoSistemaNegocio->retornaTextoSistema(array(
          'id_entidade' => $matricula['id_entidade'],
          'id_textocategoria' => \G2\Constante\TextoCategoria::CONTRATO
      ), true);

      if (!isset($dadosContrato))
        throw new \Exception("Não existe contrato cadastrado para esta entidade.");

      $textoMensagem = new TextoSistemaTO();
      $textoMensagem->setId_textosistema($dadosContrato['id_textosistema']);
      $textoMensagem->fetch(true, true, true);
      $paramsTextoSistema = array(
          'id_contrato' => $contrato['id_contrato']
      );

      //gera o contrato com os dados do aluno
      $textoMensagem = $textoSistemaBO->gerarContrato($textoMensagem, $paramsTextoSistema);
      if ($textoMensagem->getTipo() == Ead1_IMensageiro::AVISO) {
        throw new \Exception("Não foi possível gerar o contrato do aluno.");
      }
      $retornoDados = $textoMensagem->getFirstMensagem()->toArray();

      /**
       * Verifica se existe alguma ocorrencia relacionada a essa matricula e ao primeiro acesso
       * Caso exista retorna true para desabilitar o aceite no contrato ate que a ocorrencia seja fechada
       */
      $ocorrenciaPrimeiroAcesso = $primeiroAcessoNegocio->retornaOcorrenciaPrimeiroAcesso(array(
          'id_entidade' => $matricula['id_entidade'],
          'id_matricula' => $matricula['id_matricula']
      ));

      $retornoDados['bl_ocorrencia'] = false;
      if ($ocorrenciaPrimeiroAcesso) {
        $retornoDados['bl_ocorrencia'] = true;
        $retornoDados['id_ocorrencia'] = $ocorrenciaPrimeiroAcesso['id_ocorrencia'];
      }

      $this->json($retornoDados);
    } catch (\Exception $e) {
      $this->getResponse()->setHttpResponseCode(400);
      $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
      $this->json($error->toArray());
    }
  }

  public function getAction()
  {
    $this->getResponse()->setHttpResponseCode(404);
    $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, "Método ainda não implementado", 404);
    $this->_helper->json($error->toArray());
  }

  public function createAction()
  {
    $params = \Zend_Json::decode($this->getRequest()->getRawBody());

    $primeiraAcessoNegocio = new \G2\Negocio\PrimeiroAcesso();

    $mensageiro = $primeiraAcessoNegocio->salvarPrimeiroAcesso($params['id_matricula']);
    $this->json($mensageiro->toArray());
  }

  public function updateAction()
  {
    $this->getResponse()->setHttpResponseCode(404);
    $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, "Método ainda não implementado", 404);
    $this->_helper->json($error->toArray());
  }

  public function deleteAction()
  {
    $this->getResponse()->setHttpResponseCode(404);
    $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, "Método ainda não implementado", 404);
    $this->_helper->json($error->toArray());
  }

  public function pagingAction()
  {
    $this->getResponse()->setHttpResponseCode(404);
    $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, "Método ainda não implementado", 404);
    $this->_helper->json($error->toArray());
  }

}

<?php


/**
 * Class AppApi_InteracaoController
 * Controller para interações no app.
 */
class ApiV2_InteracaoController extends Ead1_AppApi implements \Ead1\Api
{
    /**
     * @var \G2\Negocio\Ocorrencia
     */
    private $negocio;

    public function init()
    {
        $this->negocio = new \G2\Negocio\Ocorrencia();
        return parent::init();
    }

    /**
     * @SWG\Get(path="/interacao",
     *   tags={"interacao"},
     *   summary="Retorna todas as interações de uma ocorrência",
     *   description="Você precisa estar logado para usar este método. Ele aceita alguns parâmetros como filtro.",
     *   operationId="getAll",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="query",
     *     name="id_ocorrencia",
     *     description="Id da Ocorência",
     *     required=false,
     *     type="integer"
     *   ),
     *     @SWG\Parameter(
     *     in="query",
     *     name="bl_visivel",
     *     description="Valor que define se é para retornar somente as interações marcadas como visivel, se valor não for informado, mostra todas.",
     *     required=false,
     *     type="boolean"
     *   ),
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/VwOcorrenciaInteracao")
     *         )
     *   ),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     *
     * )))
     *
     */
    public function indexAction()
    {

        try {

            $params['id_ocorrencia'] = $this->getParam('id_ocorrencia', false);
            $params['bl_visivel'] = $this->getParam('bl_visivel', true);
            if (!$params['id_ocorrencia'])
                throw new \Exception("Informe a Ocorrência");

            $negocio = new \G2\Negocio\Ocorrencia();
            $interacoes = $negocio->retornaVwOcorrenciaInteracao($params, false);
            $this->json($interacoes);

        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }

    }


    /**
     *
     * @SWG\Post(path="/interacao/create",
     *   tags={"interacao"},
     *   summary="Insere uma nova interação vinculada a uma determinada ocorrencia, recebe um arquivo e anexa a interaçao",
     *   description="Você precisa estar logado para usar este método. Cria um novo tramite vinculado
     * a determinada ocorrencia.",
     *   operationId="create",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="query",
     *     name="tramite",
     *     description="JSON com os dados do tramite",
     *     required=false,
     *     type="string",
     *      @SWG\Schema(
     *          ref="#/definitions/Tramite"
     *      )
     *   ),
     *   @SWG\Response(response=201,description="Operação efetuada com sucesso",
     *      @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/VwOcorrenciaInteracao")
     *         )
     *   ),
     *   @SWG\Response(response=400, description="Dados inválidos para o cadastro",
     *     @SWG\Schema(ref="#/definitions/Error")
     *   ),
     * )))))
     *
     */
    public function createAction()
    {
        try {
            $params = $this->getRequest()->getParams();
            $paramsApp = \Zend_Json::decode($this->getRequest()->getRawBody());

            unset($params['action'], $params['controller'], $params['module']);
            unset($paramsApp['action'], $paramsApp['controller'], $paramsApp['module']);

            $arquivo = null;
            if (isset($_FILES['file']['tmp_name']) && $_FILES['file']['tmp_name'] != '') {
                $arquivo = $_FILES['file'];
            }


            $result = null;
            $this->negocio = new \G2\Negocio\Ocorrencia();

            if (!$params && ($paramsApp && $paramsApp != null)) {

                if (!$paramsApp['id_ocorrencia']) {
                    throw new \Exception('A o id_ocorrencia e obrigatorio');
                }

                //verifica se o parametro foi passado para definir que a interação será visivel pelo usuário
                $bl_visivel = (array_key_exists('bl_visivel', $paramsApp) ? $paramsApp['bl_visivel'] : null);
                $result = $this->negocio->gerarNovaInteracao(
                    array(
                        'ocorrencia' => $paramsApp['ocorrencia'],
                        'st_tramite' => $paramsApp['st_tramite'],
                        'id_entidade' => isset($paramsApp['id_entidade']) ? $paramsApp['id_entidade'] : $this->getuser()->getentidade_id(),
                        'id_usuario' => $this->getuser()->getuser_id()
                    ),
                    $arquivo,
                    true,
                    $bl_visivel,
                    SistemaTO::PORTAL_ALUNO
                );

            } else {

                if (!$params['id_ocorrencia']) {
                    throw new \Exception('A o id_ocorrencia e obrigatorio');
                }

                //Pesquisa a ocorrencia da interaçao
                $params['ocorrencia'] = $this->negocio->retornaOcorrencia(array('id_ocorrencia' => $params['id_ocorrencia']), false);

                if ($params['ocorrencia'] && $params) {
                    //verifica se o parametro foi passado para definir que a interação será visivel pelo usuário
                    $bl_visivel = (array_key_exists('bl_visivel', $params) ? $params['bl_visivel'] : null);
                    $result = $this->negocio->gerarNovaInteracao(
                        array(
                            'ocorrencia' => $params,
                            'st_tramite' => $params['st_tramite'],
                            'id_entidade' => isset($params['id_entidade']) ? $params['id_entidade'] : $this->getuser()->getentidade_id(),
                            'id_usuario' => $this->getuser()->getuser_id()
                        ),
                        $arquivo,
                        true,
                        $bl_visivel,
                        SistemaTO::PORTAL_ALUNO
                    );


                } else {
                    $this->getResponse()
                        ->setHttpResponseCode(400)
                        ->appendBody(json_encode("Parâmetros incorretos."));
                }
            }

            if ($result->getType() == Ead1_IMensageiro::TYPE_SUCESSO) {
                $this->getResponse()
                    ->setHttpResponseCode(201)
                    ->appendBody(json_encode($result->getFirstMensagem()));

            } else {
                $this->getResponse()
                    ->setHttpResponseCode(400)
                    ->appendBody(json_encode($result->toArrayAll()));
            }


        } catch (Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }

    }


    /**
     * Método responsável por retornar um item da Model/Collection/Consulta baseado no ID
     */
    public function getAction()
    {
        // TODO: Implement getAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     * Método responsável por retornar atualizar os dados na Model
     */
    public function updateAction()
    {
        // TODO: Implement updateAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     * Método responsável por excluir os dados da Model/Collection/Consulta
     */
    public function deleteAction()
    {
        // TODO: Implement deleteAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     * Método responsável por retornar todos os dados da Model/Collection/Consulta por página
     */
    public function pagingAction()
    {
        // TODO: Implement pagingAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

}

<?php

use \G2\Transformer\AtividadeComplementar\AtividadeAlunoTransformer;

class ApiV2_AtividadeComplementarController extends Ead1_AppApi implements \Ead1\Api
{
    /**
     * @var \G2\Negocio\AtividadeComplementar
     */
    private $negocio;

    public function init()
    {
        $this->negocio = new \G2\Negocio\AtividadeComplementar();

        return parent::init();
    }

    /**
     * @SWG\Definition(
     *   definition="AtividadeComplementarTransformer",
     *   @SWG\Property(property="id_atividadecomplementar",type="integer"),
     *   @SWG\Property(property="id_situacao",type="integer"),
     *   @SWG\Property(property="nu_horasconvalidada",type="integer"),
     *   @SWG\Property(property="st_resumo",type="string"),
     *   @SWG\Property(property="st_situacao",type="string"),
     *   @SWG\Property(property="st_tipoatividade",type="string"),
     *   @SWG\Property(property="st_tituloatividade",type="string"),
     *   @SWG\Property(property="url_arquivo",type="string")
     * )
     *
     * @SWG\Get(path="/atividade-complementar",
     *   tags={"atividade-complementar"},
     *   summary="Retorna as atividades complementares de uma matrícula",
     *   description="Necessário Login.",
     *   operationId="getAll",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="id_matricula",
     *     in="query",
     *     description="Id da Matrícula",
     *     required=true,
     *     type="integer",
     *     example="0"
     *   ),
     *
     *   @SWG\Response(response="default",description="Operação efetuada com sucesso",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/AtividadeComplementarTransformer")
     *         )
     *   ),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     *   @SWG\Response(response=404, description="Nenhum registro encontrado", @SWG\Schema(ref="#/definitions/Error"))
     *
     * )))
     *
     */
    public function indexAction()
    {
        try {
            $id_matricula = $this->getParam('id_matricula', false);

            if (!$id_matricula) {
                throw new \Exception('Informe a Matrícula');
            }

            $results = $this->negocio->findBy('\G2\Entity\AtividadeComplementar', array(
                'id_matricula' => $id_matricula,
                'bl_ativo' => true
            ), array('id_atividadecomplementar' => 'DESC'));

            if (!$results) {
                throw new \Zend_Http_Exception('Nenhum resultado encontrado!');
            }

            $response = (new AtividadeAlunoTransformer($results))->getTransformer();

            $this->json($response);
        } catch (\Zend_Http_Exception $e) {
            $this->getResponse()->setHttpResponseCode(404);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 404);
            $this->json($error->toArray());
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }
    }

    public function getAction()
    {
        // TODO: Implement getAction() method.
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, 'Método não implementado', 404);
        $this->json($error->toArray());
    }

    /**
     *
     * @SWG\Definition(
     *   definition="AtividadeComplementar",
     *   @SWG\Property(property="id_atividadecomplementar",type="integer"),
     *   @SWG\Property(property="st_tituloatividade",type="string"),
     *   @SWG\Property(property="st_resumoatividade",type="integer"),
     *   @SWG\Property(property="id_upload",type="integer"),
     *   @SWG\Property(property="id_situacao",type="integer"),
     *   @SWG\Property(property="nu_presenca",type="integer")
     * )
     *
     * @SWG\Post(path="/atividade-complementar/create",
     *   tags={"atividade-complementar"},
     *   summary="Salvar uma nova atividade complementar.",
     *   description="Necessário Login.",
     *   operationId="create",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     description="Objeto com os dados da atividade",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/AtividadeComplementar")
     *   ),
     *   @SWG\Response(response=201,description="Operação efetuada com sucesso",
     *     @SWG\Schema(ref="#/definitions/AtividadeComplementar")
     *  ),
     *   @SWG\Response(response=400, description="Dados inválidos para o cadastro",
     *     @SWG\Schema(ref="#/definitions/Error")
     *  ),
     * )))))
     *
     */
    public function createAction()
    {
        try {
            $params = Zend_Json::decode($this->getRequest()->getRawBody());
            $params = array_merge($params ?: array(), $this->getAllParams() ?: array());

            $adapter = new Zend_File_Transfer_Adapter_Http();
            $file = $adapter->getFileInfo();
            $anexo = $file ? $file['anexo'] : null;

            if (empty($params['id_entidadecadastro'])) {
                $params['id_entidadecadastro'] = $this->getuser()->getentidade_id();
            }

            $response = $this->negocio->salvarAtividade($params, $anexo);

            if (!($response instanceof \G2\Entity\AtividadeComplementar)) {
                throw new Exception('Ops! Houve um erro inesperado, objeto não esperado.');
            }

            $this->json($this->negocio->toArrayEntity($response));
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }
    }

    public function updateAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, "Método ainda não implementado", 404);
        $this->_helper->json($error->toArray());
    }

    public function deleteAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, "Método ainda não implementado", 404);
        $this->_helper->json($error->toArray());
    }

    public function pagingAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $error = new \G2\Utils\Error(\G2\Utils\Error::WARNING, "Método ainda não implementado", 404);
        $this->_helper->json($error->toArray());
    }

    /**
     * @SWG\Get(path="/get-horas-projeto",
     *   tags={"atividade-complementar"},
     *   summary="Retorna o total de horas do projeto",
     *   description="Retorna o total de horas de atividades complementares necessárias para projeto pedagógico",
     *   operationId="getHorasProjetoAction",
     *   produces={"application/json", "text/html"},
     *   @SWG\Parameter(
     *     name="id_matricula",
     *     in="query",
     *     description="ID da Matrícula",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response="default", description="Operação efetuada com sucesso"),
     *   @SWG\Response(response=400, description="Parâmetros inválidos", @SWG\Schema(ref="#/definitions/Error")),
     * )
     */
    public function getHorasProjetoAction()
    {
        try {
            $id_matricula = $this->getParam('id_matricula', false);

            if (!$id_matricula) {
                throw new \Exception('Informe a Matrícula');
            }

            $en_matricula = $this->negocio->find('\G2\Entity\Matricula', $id_matricula);

            if (!($en_matricula instanceof \G2\Entity\Matricula)) {
                throw new \Exception('Matrícula não encontrada');
            }

            $en_projetopedagogico = $en_matricula->getId_projetopedagogico();

            $response = array(
                'id_projetopedagogico' => $en_projetopedagogico->getId_projetopedagogico(),
                'st_projetopedagogico' => $en_projetopedagogico->getSt_projetopedagogico(),
                'nu_horasatividades' => $en_projetopedagogico->getNu_horasatividades()
            );

            $this->json($response);
        } catch (\Zend_Http_Exception $e) {
            $this->getResponse()->setHttpResponseCode(404);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 404);
            $this->json($error->toArray());
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }
    }

}

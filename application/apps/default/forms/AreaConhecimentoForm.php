<?php


use G2\Negocio\AreaConhecimento;

class AreaConhecimentoForm extends Zend_Form
{

    private $negocio;
//    private $negocioTipoAreaConhecimento;
    public $imgArea;

    public function __construct(array $options = null)
    {

        $this->negocio = new \G2\Negocio\Negocio();
//        $this->negocioTipoAreaConhecimento = new G2\Negocio\TipoAreaConhecimento();

        $this->addElement('hidden', 'id', array('value' => ''));

        $this->addElement('text', 'st_areaconhecimento', array(
            'required' => true,
            'class' => 'input-block-level',
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(3, 250))
            )
        ));
        $this->st_areaconhecimento->addErrorMessage('A área de conhecimento é obrigatória!');

        $this->addElement('text', 'st_tituloexibicao', array(
            'required' => true,
            'class' => 'input-block-level',
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(3, 400))
            )
        ));
        $this->st_tituloexibicao->addErrorMessage('O título de exibição é obrigatório');

        $this->addElement('textarea', 'st_descricao', array(
            'required' => true,
            'class' => 'span12',
            'id' => 'st_descricao',
            'rows' => 6,
        ));
        $this->st_descricao->addErrorMessage('A descrição é obrigatória');

        $id_situacao = new Zend_Form_Element_Select('id_situacao');
        $id_situacao->autoInsertNotEmptyValidator();
        $id_situacao->setAttribs(array(
            'class' => 'span12',
        ));
        $this->addElement($id_situacao);


        $id_tipoareaconhecimento = new Zend_Form_Element_Select('id_tipoareaconhecimento');
        $id_tipoareaconhecimento->setAttribs(array(
            'class' => 'span12',
        ));
        $this->addElement($id_tipoareaconhecimento);


        //botão de submit
        $this->addElement('submit', 'btnSbmit', array(
            'class' => 'btn btn-success  pull-right',
            'name' => 'Salvar',
            'id' => 'btnSbmit'
        ));


        //popula os selects
        $this->populateSituacao();
        $this->populateTipoAreaConhecimento();

        if (is_array($options)) {
            $this->populateForm($options);
        }

        //remove os decorators
        foreach ($this->getElements() as $element) {
            $element->removeDecorator('HtmlTag');
            $element->removeDecorator('DtDdWrapper');
            $element->removeDecorator('label');
        }
    }

    private function populateForm($params)
    {
        $negocioArea = new AreaConhecimento();
        $result = $negocioArea->findAreaConhecimento($params['id']);

        if ($result) {
            $this->getElement('id')->setValue($params['id']);
            $this->getElement('st_areaconhecimento')->setValue($result->getSt_areaconhecimento());
            $this->getElement('st_tituloexibicao')->setValue($result->getSt_tituloexibicao());
            $this->getElement('id_situacao')->setValue($result->getId_situacao());
            // $this->getElement('bl_ativo')->setValue($result->getBl_ativo());
            $this->getElement('st_descricao')->setValue($result->getSt_descricao());
            //  $this->getElement('id_entidade')->setValue($result->getId_entidade());
            //  $this->getElement('id_usuariocadastro')->setValue($result->getId_usuariocadastro());
            // $this->getElement('id_areaconhecimentopai')->setValue($result->getId_areaconhecimentopai());
            $this->getElement('id_tipoareaconhecimento')->setValue($result->getId_tipoareaconhecimento());

            $this->imgArea = $result->getSt_imagemarea();
        }

    }

    /**
     * Popula Select situacao
     * @param void
     * @return object Zend_Form_Element_Select
     */
    private function populateSituacao()
    {
        $situacao = $this->getElement('id_situacao');
        $result = $this->negocio->findBy('G2\Entity\Situacao', array('st_tabela' => 'tb_areaconhecimento'));
        if ($result) {
            foreach ($result as $row) {
                $situacao->addMultiOption($row->getId_situacao(), $row->getSt_situacao());
            }
        }
    }


    /**
     * Popula Select TipoAreaConhecimento
     * @param void
     * @return object Zend_Form_Element_Select
     */
    private function populateTipoAreaConhecimento()
    {

        $tipoareaconhecimento = $this->getElement('id_tipoareaconhecimento');
        $result = $this->negocio->findAll('G2\Entity\TipoAreaConhecimento');
        if ($result) {
            foreach ($result as $row) {
                $tipoareaconhecimento->addMultiOption($row->getId_tipoareaconhecimento(), $row->getSt_tipoareaconhecimento());
            }
        }
    }


}
<?php

/**
 * Form responsável por gerar o filtro dos relatórios
 * @since 2013-09-25
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @package forms
 */
class RelatorioForm extends Zend_Form
{

    /**
     * Seta os inputs que estarão no fieldset de filtros
     * @var array $displayGroup
     */
    public $displayGroup = array();
    public $fieldsDependent = array();
    private $defaultErrorMenssage = "Campo %campo obrigatório.";

    public function __construct(array $options = null)
    {
        parent::__construct();

        if ($options) {
            //cria o campo hidden com o valor do matodo que será renderizado no submit
            if ($options['id_funcionalidade']) {
                $this->addElement('hidden', 'id_funcionalidade', array('value' => $options['id_funcionalidade']));
                unset($options['id_funcionalidade']);
            }

            //Gera os inputs
            $this->getInputs($options);
        }
        //Remove os decorators
        foreach ($this->getElements() as $element) {
            $element->removeDecorator('HtmlTag');
            $element->removeDecorator('DtDdWrapper');
        }
    }

    /**
     * Percorre o array e cria os respectivos elementos
     * @param array $params
     * @throws Zend_Exception
     */
    private function getInputs(array $params)
    {
        //verifica se tem index no array
        if ($params) {
            //percorre o array
            foreach ($params as $row) {
                //verifica se o index to esta preenchido
                if (isset($row['propriedades']["interface_flex"])) {
                    //verifica o tipo e chama seu respecivo metodo
                    switch ($row['propriedades']["interface_flex"]) {
                        case 'FiltroData':
                            $this->geraFiltroData($row);
                            break;
                        case 'FiltroEntreData':
                            $this->geraFiltroEntreData($row);
                            break;
                        case 'FiltroEntreNumero':
                            $this->geraFiltroEntreNumero($row);
                            break;
                        case 'FiltroHidden':
                            $this->geraFiltroHidden($row);
                            break;
                        case 'FiltroIgualData':
                            $this->geraFiltroData($row);
                            break;
                        case 'FiltroIgualDropDown':
                            $this->geraFiltroIgualDropDown($row);
                            break;
                        case 'FiltroIgualNumero':
                            $this->geraFiltroIgualNumero($row);
                            break;
                        case 'FiltroIgualText':
                            $this->geraFiltroIgualText($row);
                            break;
                        case 'FiltroMaiorData':
                            $this->geraFiltroMaiorData($row);
                            break;
                        case 'FiltroMaiorNumero':
                            $this->geraFiltroMaiorNumero($row);
                            break;
                        case 'FiltroMenorData':
                            $this->geraFiltroMenorData($row);
                            break;
                        case 'FiltroMenorNumero':
                            $this->geraFiltroMenorNumero($row);
                            break;
                        case 'FiltroNumero':
                            $this->geraFiltroIgualNumero($row);
                            break;
                        default :
                            throw new Zend_Exception("Não foi possível criar campo de filtro " . $row['propriedades']["interface_flex"], 0);
                            break;
                    }
                } else {
                    $this->geraFiltroHidden($row);
                }
            }
        }
    }

    /**
     * Gera input para FiltroMenorData
     * @param array $params
     */
    private function geraFiltroMenorData($params)
    {
        $this->getTxtInput($params['st_camporelatorio'], $params['st_titulocampo'] . " Menor", $params['st_camporelatorio'], $params['st_camporelatorio'] . '[valor1]', null, $params['bl_obrigatorio']);
        if (isset($params['propriedades']['operacaorelatorio'])) {
            $this->getHiddenInput($params['st_camporelatorio'] . 'operador', $params['st_camporelatorio'] . 'operador', $params['st_camporelatorio'] . '[operador]', $params['propriedades']['operacaorelatorio']);
        }
        array_push($this->displayGroup, $params['st_camporelatorio'], $params['st_camporelatorio'] . 'operador');
    }

    /**
     * Gera input para FiltroMenorNumero
     * @param Object $params
     */
    private function geraFiltroMenorNumero($params)
    {
        $this->getTxtInput($params['st_camporelatorio'], $params['st_titulocampo'] . " Menor", $params['st_camporelatorio'], $params['st_camporelatorio'] . '[valor1]', null, $params['bl_obrigatorio']);
        if (isset($params['propriedades']['operacaorelatorio'])) {
            $this->getHiddenInput($params['st_camporelatorio'] . 'operador', $params['st_camporelatorio'] . 'operador', $params['st_camporelatorio'] . '[operador]', $params['propriedades']['operacaorelatorio']);
        }
        array_push($this->displayGroup, $params['st_camporelatorio'], $params['st_camporelatorio'] . 'operador');
    }

    /**
     * Gera input para FiltroMairoNumero
     * @param object $params
     */
    private function geraFiltroMaiorNumero($params)
    {
        $this->getTxtInput($params['st_camporelatorio'], $params['st_titulocampo'] . " Maior", $params['st_camporelatorio'], $params['st_camporelatorio'] . '[valor1]', null, $params['bl_obrigatorio']);
        if (isset($params['propriedades']['operacaorelatorio'])) {
            $this->getHiddenInput($params['st_camporelatorio'] . 'operador', $params['st_camporelatorio'] . 'operador', $params['st_camporelatorio'] . '[operador]', $params['propriedades']['operacaorelatorio']);
        }
        array_push($this->displayGroup, $params['st_camporelatorio'], $params['st_camporelatorio'] . 'operador');
    }

    /*
     * Gera input para FiltroMaiorData
     * @param object $params
     */

    private function geraFiltroMaiorData($params)
    {
        $this->getTxtInput($params['st_camporelatorio'], $params['st_titulocampo'] . " Maior", $params['st_camporelatorio'], $params['st_camporelatorio'] . '[valor1]', null, $params['bl_obrigatorio']);
        if (isset($params['propriedades']['operacaorelatorio'])) {
            $this->getHiddenInput($params['st_camporelatorio'] . 'operador', $params['st_camporelatorio'] . 'operador', $params['st_camporelatorio'] . '[operador]', $params['propriedades']['operacaorelatorio']);
        }
        array_push($this->displayGroup, $params['st_camporelatorio'], $params['st_camporelatorio'] . 'operador');
    }

    /**
     * Gera input para FiltroIgualText
     * @param object $params
     */
    private function geraFiltroIgualText($params)
    {
        $this->getTxtInput($params['st_camporelatorio'], $params['st_titulocampo'], $params['st_camporelatorio'] . "[valor1]", $params['st_camporelatorio'] . "[valor1]", 'span12', $params['bl_obrigatorio']);
        if (isset($params['propriedades']['operacaorelatorio'])) {
            $this->getHiddenInput($params['st_camporelatorio'] . 'operador', $params['st_camporelatorio'] . 'operador', $params['st_camporelatorio'] . '[operador]', $params['propriedades']['operacaorelatorio']);
        }
        array_push($this->displayGroup, $params['st_camporelatorio'], $params['st_camporelatorio'] . 'operador');
    }

    /**
     * Gera input Hidden
     * @param object $params
     */
    private function geraFiltroHidden($params)
    {
        $this->getHiddenInput($params['st_camporelatorio'], $params['st_camporelatorio'], $params['st_camporelatorio'] . '[valor1]', $params['propriedades']['default'], $params['bl_obrigatorio']);
        if (isset($params['propriedades']['operacaorelatorio'])) {
            $this->getHiddenInput($params['st_camporelatorio'] . 'operador', $params['st_camporelatorio'] . 'operador', $params['st_camporelatorio'] . '[operador]', $params['propriedades']['operacaorelatorio']);
        }
        array_push($this->displayGroup, $params['st_camporelatorio'], $params['st_camporelatorio'] . 'operador');
    }

    /**
     * Gera inpu para FiltroIgualNumero
     * @param object $params
     * @return Zend_Form_Element_Text
     */
    private function geraFiltroIgualNumero($params)
    {
        $this->getTxtInput($params['st_camporelatorio'], $params['st_titulocampo'], $params['st_camporelatorio'], "{$params['st_camporelatorio']}[valor1]", null, $params['bl_obrigatorio']);
        if (isset($params['propriedades']['operacaorelatorio'])) {
            $this->getHiddenInput($params['st_camporelatorio'] . 'operador', $params['st_camporelatorio'] . 'operador', $params['st_camporelatorio'] . '[operador]', $params['propriedades']['operacaorelatorio']);
        }
        array_push($this->displayGroup, $params['st_camporelatorio'], $params['st_camporelatorio'] . 'operador');
    }

    /**
     * Gera inputs para FiltroEntreData
     * @param object $params
     * @return Zend_Form_Element_Text
     */
    private function geraFiltroEntreNumero($params)
    {
        $this->getTxtInput($params['st_camporelatorio'], "{$params['st_titulocampo']} Entre", null, "{$params['st_camporelatorio']}[valor1]", 'numeric', $params['bl_obrigatorio']);
        //segundo elemento
        $this->getTxtInput("{$params['st_camporelatorio']}txtSegundoNumero", 'e', null, "{$params['st_camporelatorio']}[valor2]", 'numeric', $params['bl_obrigatorio']);
        $this->getHiddenInput("operador{$params['st_camporelatorio']}", null, "{$params['st_camporelatorio']}[operador]", $params['propriedades']['operacaorelatorio']);
        //atribui os elementos no array para ser colocado no fieldset
        array_push($this->displayGroup, $params['st_camporelatorio'], "{$params['st_camporelatorio']}txtSegundoNumero", "operador{$params['st_camporelatorio']}");
    }

    /**
     * Gera inputs para FiltroEntreData
     * @param object $params
     * @return Zend_Form_Element_Text
     */
    private function geraFiltroData($params)
    {
        $this->getTxtInput($params['st_camporelatorio'], $params['st_titulocampo'], $params['st_camporelatorio'] . "[valor1]", $params['st_camporelatorio'] . "[valor1]", 'datepicker maskDate', $params['bl_obrigatorio']);
        if (isset($params['propriedades']['operacaorelatorio'])) {
            $this->getHiddenInput($params['st_camporelatorio'] . 'operador', $params['st_camporelatorio'] . 'operador', $params['st_camporelatorio'] . '[operador]', $params['propriedades']['operacaorelatorio']);
        }
        array_push($this->displayGroup, $params['st_camporelatorio'], $params['st_camporelatorio'] . 'operador');
    }

    /**
     * Gera inputs para FiltroEntreData
     * @param object $params
     * @return Zend_Form_Element_Text
     */
    private function geraFiltroEntreData($params)
    {
        $rangeEntreDatas = null;
        if (isset($params['id_tipopropriedadecamporel']) && $params['id_tipopropriedadecamporel'] == 9) {
            $rangeEntreDatas = $params['st_valor'];
        }
        //elemento inicial
        $this->getTxtInput($params['st_camporelatorio'], "{$params['st_titulocampo']} Entre", null, "{$params['st_camporelatorio']}[valor1]", 'datepicker maskDate relatorio_dt_min', $params['bl_obrigatorio'], $rangeEntreDatas, $params['st_camporelatorio']);
        //segundo elemento
        $this->getTxtInput("{$params['st_camporelatorio']}dateField2", 'e', null, "{$params['st_camporelatorio']}[valor2]", 'datepicker maskDate relatorio_dt_max', $params['bl_obrigatorio'], $rangeEntreDatas, $params['st_camporelatorio'], true);
        $this->getHiddenInput("operador{$params['st_camporelatorio']}", null, "{$params['st_camporelatorio']}[operador]", $params['propriedades']['operacaorelatorio']);
        //atribui os elementos no array para ser colocado no fieldset
        array_push($this->displayGroup, $params['st_camporelatorio'], "{$params['st_camporelatorio']}dateField2", "operador{$params['st_camporelatorio']}");
    }

    /**
     * Gera campos de filtro FiltroIgualDropDown
     * @param object $params
     * @return Zend_Form_Element_Select
     */
    private function geraFiltroIgualDropDown($params)
    {
        if (isset($params['st_campopai'])) {
            $params['propriedades']['campo_pai'] = $params['st_campopai'];
            $params['propriedades']['id_self'] = $params['id_camporelatorio'];
        }

        $bootstrapClass = 'span6';
        if (isset($params['id_tipopropriedadecamporel']) && $params['id_tipopropriedadecamporel'] == 10 && $params['st_valor'] == 1) {
            $bootstrapClass .= ' select2';
        }

        //cria o elemento
        $this->getSelectInput($params['st_camporelatorio'], $params['st_titulocampo'], null, $params['st_camporelatorio'] . "[valor1]", $params['propriedades'], $bootstrapClass, $params['bl_obrigatorio']);
        $this->getHiddenInput("operador{$params['st_camporelatorio']}", null, "{$params['st_camporelatorio']}[operador]", $params['propriedades']['operacaorelatorio']);
        //atribui o elemento no array para ser colocado no fieldset
        array_push($this->displayGroup, $params['st_camporelatorio'], "operador{$params['st_camporelatorio']}");
    }

    /**
     * Método abstrato para criar elemento de texto
     * @param string $name
     * @param string $label
     * @param string $attrId
     * @param string $attrName
     * @param string $cssClass
     * @param boolean $required
     * @param int $dateRange
     * @param string $fieldRef
     * @param bool $clearfix
     */
    private function getTxtInput(
        $name,
        $label,
        $attrId = null,
        $attrName = null,
        $cssClass = null,
        $required = false,
        $dateRange = null,
        $fieldRef = null,
        $clearfix = false
    )
    {
        $txtInput = new Zend_Form_Element_Text($name);

        if ($attrId) {
            $txtInput->setAttrib('id', $attrId);
        }

        if ($required) {
            $txtInput->setRequired(true)
                ->addErrorMessage(str_replace("%campo", $label, $this->defaultErrorMenssage))
                ->setAttrib('required', 'required');

            $label .= ' *';
        }

        $txtInput->setLabel($label);

        $txtInput->setAttribs(array(
            'name' => $attrName,
            'class' => $cssClass
        ));

        // Definir validações
        $this->setValidators($txtInput);

        if ($name == $fieldRef . 'dateField2') {
            $txtInput->setAttrib('data-minimo', $fieldRef . '[valor1]');
        } else {
            $txtInput->setAttrib('data-maximo', $fieldRef . '[valor2]');
        }

        if ($dateRange) {
            $txtInput->setAttrib('data-range', $dateRange);
        }

        $txtInput->setDecorators(array(
            'ViewHelper',
            'Errors',
            'Description',
            array(
                array(
                    'inner' => 'HtmlTag'
                ),
                array(
                    'tag' => 'div',
                    'class' => 'field-item-div'
                )
            ),
            'Label',
            array(
                array(
                    'outter' => 'HtmlTag'
                ),
                array(
                    'tag' => 'div',
                    'class' => 'label-item-div ' . ($clearfix ? 'clearfix' : '')
                )
            )
        ));
        $this->addElement($txtInput);
    }

    /**
     * @decription Adicionar validações e máscaras aos inputs dependendo do seu nome
     * @author Caio Eduardo <caioedut@gmail.com>
     * @date 2017-06-29
     * @param \Zend_Form_Element $input
     */
    public function setValidators(&$input)
    {
        $name = $input->getName();

        // CPF
        if (substr($name, 0, 6) == 'st_cpf') {
            $input->setAttribs(array(
                'pattern' => '\d{3}\.\d{3}\.\d{3}-\d{2}',
                'placeholder' => '___.___.___-__',
                'data-mask' => '999.999.999-99'
            ));
        }

        // CNPJ
        if (substr($name, 0, 7) == 'st_cnpj') {
            $input->setAttribs(array(
                'pattern' => '\d{2}\.\d{3}\.\d{3}\/\d{4}-\d{2}',
                'placeholder' => '___.___.___-__',
                'data-mask' => '99.999.999/9999-99'
            ));
        }

        // E-MAIL
        if (substr($name, 0, 8) == 'st_email') {
            $input->setAttribs(array(
                'pattern' => '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}'
            ));
        }

        // DATAS
        if (substr($name, 0, 3) == 'dt_') {
            $input->setAttribs(array(
                'pattern' => '\d{2}/\d{2}/\d{4}',
                'placeholder' => '__/__/____',
                'data-mask' => '99/99/9999'
            ));
        }
    }

    /**
     * Metodo abstrato para criar elemento select
     * @param string $name
     * @param string $label
     * @param string $attId
     * @param string $attrName
     * @param array $propriedades
     * @param string $cssClass
     * @param boolean $required
     */
    private function getSelectInput($name, $label, $attId = null, $attrName = null, array $propriedades = null, $cssClass = null, $required = false)
    {
        $dropDownList = new Zend_Form_Element_Select($name);

        if ($attId) {
            $dropDownList->setAttrib('id', $attId);
        }

        if ($required) {
            $dropDownList->setRequired(true)
                ->addErrorMessage(str_replace("%campo", $label, $this->defaultErrorMenssage))
                ->setAttrib('required', 'required');

            $label .= ' *';
        }

        $dropDownList->setLabel($label);

        $dropDownList->setAttribs(array(
            'name' => $attrName,
            'class' => $cssClass
        ));

        if (isset($propriedades['campo_pai'])) {
            $dropDownList->setAttrib('data-idself', $propriedades['id_self']);
            $dropDownList->setAttrib('data-campopai', $propriedades['campo_pai']);
        }

        //Verifica se tem valor padrão e seta o valor senão define valor padrão
        if (isset($propriedades['default'])) {
            $dropDownList->addMultiOption('', $propriedades['default']);
        } else {
            $dropDownList->addMultiOption('', 'Selecione');
        }
        //varifica se tem a chave de consulta com os valores
        if (isset($propriedades['consulta'])) {
            foreach ($propriedades['consulta'] as $value) {
                //print_r($_SERVER['REQUEST_URI']);exit;
                //print_r($_SESSION["geral"]);exit;
                $dropDownList->addMultiOption($value['id'], $value['label']);
                $funcionalidade = substr($_SERVER['REQUEST_URI'], strripos($_SERVER['REQUEST_URI'], '/') + 1);
                //print_r($funcionalidade);exit;
//                if($funcionalidade == '490' || $funcionalidade == '470') {
//                    $dropDownList->setValue($_SESSION["geral"]["id_entidade"]); // Para que a opção ficasse "selected"
//                }

            }
        }
        $dropDownList->setDecorators(array(
            'ViewHelper',
            'Errors',
            'Description',
            array(
                array(
                    'inner' => 'HtmlTag'
                ),
                array(
                    'tag' => 'div',
                    'class' => 'field-item-div'
                )
            ),
            'Label',
            array(
                array(
                    'outter' => 'HtmlTag'
                ),
                array(
                    'tag' => 'div',
                    'class' => 'label-item-div-select '
                )
            )
        ));
        $this->addElement($dropDownList);
    }

    /**
     *
     * @param string $name
     * @param string $attId
     * @param string $attName
     * @param string $valor
     * @param string $cssClass
     */
    private function getHiddenInput($name, $attId, $attName, $valor = null, $cssClass = null)
    {
        $hidden = new Zend_Form_Element_Hidden($name);
        $hidden->setAttrib('id', $attId)
            ->setAttrib('class', $cssClass)
            ->setAttrib('name', $attName)
            ->setValue($valor)
            ->setDecorators(array(
                'ViewHelper',
                'Errors',
                'Description',
                array(
                    array(
                        'inner' => 'HtmlTag'
                    ),
                    array(
                        'tag' => 'div',
                        'class' => 'field-item-div'
                    )
                ),
                'Label',
                array(
                    array(
                        'outter' => 'HtmlTag'
                    ),
                    array(
                        'tag' => 'div',
                        'class' => 'label-item-div'
                    )
                )
            ));
        $this->addElement($hidden);
    }

}

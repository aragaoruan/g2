<?php

/**
 * 
 * @since 2014-2-28
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @package default
 * @subpackage form
 */
use \G2\Negocio\CampanhaComercial as Negocio;

class CupomForm extends Zend_Form
{

    private $negocio;

    public function init ()
    {
        $this->negocio = new Negocio();

        $id_cupom = new Zend_Form_Element_Hidden('id_cupom');
        $this->addElement($id_cupom);

        $st_prefixo = new Zend_Form_Element_Text("st_prefixo");
        $st_prefixo->setAttribs(array(
                    'maxlength' => 11,
                    'required' => 'required',
                    'title' => 'Prefixo do Cupom',
                    'class' => 'span12'
                ))->setRequired(true)
                ->addErrorMessage('O campo Prefixo do Cupom é obrigatório.')
                ->addValidators(array(
                    array(
                        'validator' => 'NotEmpty'
                    ),
                    array(
                        'validator' => 'stringLength',
                        'options' => array(3, 11)
                    )
        ));
        $this->addElement($st_prefixo);

        $nu_desconto = new Zend_Form_Element_Text("nu_desconto");
        $nu_desconto->setAttribs(array(
                    'required' => 'required',
                    'title' => 'Desconto',
                    'class' => 'span12 numeric'
                ))->setRequired(true)
                ->addErrorMessage('O campo Desconto é obrigatório.')
                ->addValidators(array(
                    array(
                        'validator' => 'NotEmpty'
                    )
        ));
        $this->addElement($nu_desconto);

        $dt_inicio = new Zend_Form_Element_Text("dt_inicio");
        $dt_inicio->setAttribs(array(
                    'required' => 'required',
                    'maxlength' => 10,
                    'class' => 'datepicker_range_min maskDate desabilitactrlv span8',
                    'title' => 'Data Início',
                    'data-maximo' => 'dt_fim'
                ))
                ->setRequired(true)
                ->addErrorMessage('O campo Data Início é obrigatório.')
                ->addValidators(array(
                    array(
                        'validator' => 'NotEmpty'
                    )
        ));
        $this->addElement($dt_inicio);

        $dt_fim = new Zend_Form_Element_Text("dt_fim");
        $dt_fim->setAttribs(array(
                    'required' => 'required',
                    'maxlength' => 10,
                    'class' => 'datepicker_range_max maskDate desabilitactrlv span8',
                    'title' => 'Data Fim',
                    'data-maximo' => 'dt_inicio'
                ))
                ->setRequired(true)
                ->addErrorMessage('O campo Data Fim é obrigatório.')
                ->addValidators(array(
                    array(
                        'validator' => 'NotEmpty'
                    )
        ));
        $this->addElement($dt_fim);

        $arrayHoras = $this->retornaArrayHoras(); //Recupera array de horas

        $hora_ini = new Zend_Form_Element_Select('hora_ini');
        $hora_ini->setAttribs(array(
                    'required' => 'required',
                    'class' => 'span12',
                    'title' => 'Hora Início'
                ))
                ->setRequired(true)
                ->addErrorMessage('O campo Hora Início é obrigatório.')
                ->addValidators(array(
                    array(
                        'validator' => 'NotEmpty'
                    )
        ));
        $hora_ini->addMultiOptions($arrayHoras);
        $this->addElement($hora_ini);

        $hora_fim = new Zend_Form_Element_Select('hora_fim');
        $hora_fim->setAttribs(array(
                    'required' => 'required',
                    'class' => 'span12',
                    'title' => 'Hora Fim'
                ))
                ->setRequired(true)
                ->addErrorMessage('O campo Hora Início é obrigatório.')
                ->addValidators(array(
                    array(
                        'validator' => 'NotEmpty'
                    )
        ));
        $hora_fim->addMultiOptions($arrayHoras);
        $this->addElement($hora_fim);


        $tipoCodigo = new Zend_Form_Element_Select('st_tipocodigo');
        $tipoCodigo->setAttribs(array(
                    'required' => 'required',
                    'class' => 'span12',
                    'title' => 'Código'
                ))->setRequired(true)
                ->addErrorMessage('O campo Código é obrigatório.')
                ->addValidators(array(
                    array(
                        'validator' => 'NotEmpty'
                    )
        ));
        $tipoCodigo->addMultiOptions(array(
            0 => 'Fixo',
            1 => 'Aleatório'
        ));
        $this->addElement($tipoCodigo);

        $st_complemento = new Zend_Form_Element_Text("st_complemento");
        $st_complemento->setAttribs(array(
                    'required' => 'required',
                    'maxlength' => 5,
                    'class' => 'span12',
                    'title' => 'Complemento do Cupom'
                ))->setRequired(true)
                ->addErrorMessage('O campo Complemento do Cupom é obrigatório.')
                ->addValidators(array(
                    array(
                        'validator' => 'NotEmpty'
                    ),
                    array(
                        'validator' => 'stringLength',
                        'options' => array(5, 5)
                    )
        ));
        $this->addElement($st_complemento);


        $bl_ativacao = new Zend_Form_Element_Checkbox('bl_ativacao');
        $bl_ativacao->setAttribs(array(
                    'checked' => 'checked',
                    'title' => 'Ativar na criação'
                ))->setRequired(true)
                ->addErrorMessage('O campo Ativar na criação é obrigatório.');
        $this->addElement($bl_ativacao);

        $bl_unico = new Zend_Form_Element_Checkbox('bl_unico');
        $bl_unico->setAttribs(array(
                    'checked' => 'checked',
                    'title' => 'Utilizar somente uma vez'
                ))->setRequired(true)
                ->addErrorMessage('O campo Utilizar somente uma vez é obrigatório.');
        $this->addElement($bl_unico);

        $id_tipodesconto = new Zend_Form_Element_Select('id_tipodesconto');
        $id_tipodesconto->setAttribs(array(
                    'class' => 'span10',
                    'title' => 'Tipo de Desconto',
                    'required' => 'required'
                ))->setRequired(true)
                ->addErrorMessage('O campo Tipo de Desconto é obrigatório.');
        $this->addElement($id_tipodesconto);
        $this->populaTipoDesconto();

        //remove os decorators
        foreach ($this->getElements() as $element) {
            $element->removeDecorator('HtmlTag');
            $element->removeDecorator('DtDdWrapper');
            $element->removeDecorator('label');
        }
    }

    /**
     * Monta array com string de horas de 00:00 até 23:59
     * @return array
     */
    public function retornaArrayHoras ()
    {
        $arrReturn = array();
        for ($i = 0; $i < 24; $i++) {
            for ($j = 0; $j < 60; $j++) {
                $hora = str_pad($i, 2, "0", STR_PAD_LEFT) . ":" . str_pad($j, 2, "0", STR_PAD_LEFT);
                $arrReturn[$hora] = $hora;
            }
        }
        return $arrReturn;
    }

    /**
     * Retorna tipo de desconto
     */
    private function populaTipoDesconto ()
    {
        $tipos = $this->negocio->retornarTipoDesconto();
        $elemento = $this->getElement('id_tipodesconto');
        if ($tipos) {
            $elemento->addMultiOption('', 'Selecione');
            foreach ($tipos as $tipo) {
                $elemento->addMultiOption($tipo->getId_tipodesconto(), $tipo->getSt_tipodesconto());
            }
        }
    }

}

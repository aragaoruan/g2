<?php

/**
 * Form gerador dos inputs dos formulários dinamicos
 * @since 2013-09-17
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @package forms
 */
use G2\Negocio\TextoSistema;

class TextosSistemaForm extends Zend_Form
{

    private $negocio;

    public function __construct (array $options = null)
    {
        $this->negocio = new TextoSistema();

        $this->addElement('hidden', 'id', array('value' => ''));


        $st_textosistema = new Zend_Form_Element_Text('st_textosistema', array(
            'class' => 'input-block-level',
            'title' => 'Título'
        ));
        $st_textovariaveis = new Zend_Form_Element_Text('st_textovariaveis', array(
            'class' => 'input-block-level',
            'title' => 'Variável'
        ));
        $st_textosistema->addValidator('StringLength', array(0, 200))
                ->setRequired(true)
                ->addErrorMessage('O campo Título é obrigatório.');


        $st_texto = new Zend_Form_Element_Textarea('st_texto', array(
            'class' => 'input-block-level',
            'rows' => 12,
            'id' => 'editor',
            'title' => 'Texto'
        ));
        $st_texto->setRequired(true)
                ->addErrorMessage('O campo Texto é obrigatório.');


        $dt_inicio = new Zend_Form_Element_Text('dt_inicio', array(
            'class' => 'span12 datepicker maskDate',
            'title' => 'Início'
        ));
        $dt_inicio->setRequired(true)
                ->addErrorMessage('O campo Data Inicio é obrigatório.');


        $dt_fim = new Zend_Form_Element_Text('dt_fim', array(
            'class' => 'span12 datepicker maskDate',
            'title' => 'Fim'
        ));


        $id_orientacaotexto = new Zend_Form_Element_Select('id_orientacaotexto');
        $id_orientacaotexto->setAttribs(array(
                    'class' => 'span12',
                    'title' => 'Orientação'
                ))
                ->addMultiOption(null, 'Selecione')
                ->addMultiOption(1, 'Retrato')
                ->addMultiOption(2, 'Paisagem');


        $id_textoexibicao = new Zend_Form_Element_Select('id_textoexibicao');
        $id_textoexibicao->setAttribs(array(
                    'class' => 'span12',
                    'title' => 'Exibição'
                ))
                ->addMultiOption(null, 'Selecione');

        $id_textocategoria = new Zend_Form_Element_Select('id_textocategoria');
        $id_textocategoria->setAttribs(array(
            'class' => 'span12',
            'title' => 'Categoria Texto'
        ));


        $variaveis = new Zend_Form_Element_Multiselect('variaveis');
        $variaveis->setAttribs(array(
            'class' => 'span12',
            'title' => 'Variáveis'
        ));

        //campo de cabecalho e rodapé
        $id_cabecalho = new Zend_Form_Element_Select('id_cabecalho');
        $id_cabecalho->setAttribs(array(
            'class' => 'span12',
            'title' => 'Cabeçalho'
        ));

        $id_rodape = new Zend_Form_Element_Select('id_rodape');
        $id_rodape->setAttribs(array(
            'class' => 'span12',
            'title' => 'Rodapé'
        ));

        //botão de submit
        $this->addElement('button', 'btnSbmit', array(
            'class' => 'btn btn-success',
            'name' => 'Salvar',
            'id' => 'btnSbmit'
        ));

        $this->addElement($st_textosistema);
        $this->addElement($st_texto);
        $this->addElement($dt_inicio);
        $this->addElement($dt_fim);
        $this->addElement($id_orientacaotexto);
        $this->addElement($id_textoexibicao);
        $this->addElement($id_textocategoria);
        $this->addElement($variaveis);
        $this->addElement($st_textovariaveis);
        $this->addElement($id_cabecalho);
        $this->addElement($id_rodape);

        //popular os combos de cabeçalho e rodapé
        $this->popularCabecalhoRodape();
        //popula os selects
        $this->populateTextoExibicao();
        if (is_array($options)) {
            if (empty($options['id'])) {
                $id_textocategoria->setAttrib('disabled', 'disabled')->addMultiOption(null, 'Selecione a Exibição');
                $st_textovariaveis->setAttrib('readonly', 'readonly');
                $variaveis->setAttrib('disabled', 'disabled')->addMultiOption(null, 'Selecione a Categoria do Texto');
            } else {
                $id_textocategoria->addMultiOption(null, 'Selecione');
            }
            $this->populateForm($options);
        }

        //remove os decorators
        foreach ($this->getElements() as $element) {
            $element->removeDecorator('HtmlTag');
            $element->removeDecorator('DtDdWrapper');
            $element->removeDecorator('label');
        }
    }

    private function populateForm ($params)
    {
        if (isset($params['id'])) {
            $result = $this->negocio->findTextoSistema($params['id']);
            if ($result) {
                $this->getElement('id')->setValue($params['id']);
                $this->getElement('st_textosistema')->setValue($result->getSt_Textosistema());
                $this->getElement('id_orientacaotexto')->setValue($result->getId_orientacaotexto()->getId_orientacaotexto());
                $this->getElement('id_textoexibicao')->setValue($result->getId_textoexibicao()->getId_textoexibicao());
                $this->getElement('st_texto')->setValue($result->getSt_texto());
                if ($result->getDt_inicio()) {
                    $this->getElement('dt_inicio')->setValue($result->getDt_inicio()->format('d/m/Y'));
                }
                if ($result->getDt_fim()) {
                    $this->getElement('dt_fim')->setValue($result->getDt_fim()->format('d/m/Y'));
                }
                $this->populateTextoCategoria(array(
                    'id_textoexibicao' => $result->getId_textoexibicao()->getId_textoexibicao(),
                    'id_textocategoria' => $result->getId_textocategoria()->getId_textocategoria()
                ));
                $this->populateVariaveis(array('id_textocategoria' => $result->getId_textocategoria()->getId_textocategoria(), 'bl_exibicao' => 1));
                if($result->getId_rodape() instanceof  \G2\Entity\TextoSistema)
                    $this->getElement('id_rodape')->setValue($result->getId_rodape()->getId_textosistema());
                if($result->getId_cabecalho() instanceof  \G2\Entity\TextoSistema)
                $this->getElement('id_cabecalho')->setValue($result->getId_cabecalho()->getId_textosistema());
            }
        }
    }

    /**
     * Popula Select
     * @param void
     * @return object Zend_Form_Element_Select
     */
    private function populateTextoExibicao ()
    {
        $textoExibicao = $this->getElement('id_textoexibicao');
        $result = $this->negocio->findAll('G2\Entity\TextoExibicao');
        if ($result) {
            foreach ($result as $row) {
                $textoExibicao->addMultiOption($row->getId_textoexibicao(), $row->getSt_textoexibicao());
            }
        }
    }

    /**
     * Popula Select
     * @param void
     * @return object Zend_Form_Element_Select
     */
    private function populateTextoCategoria (array $params = null)
    {
        $textoCategoria = $this->getElement('id_textocategoria');
        $result = $this->negocio->find('G2\Entity\TextoExibicao', $params['id_textoexibicao']);
        if ($result) {
            foreach ($result->getTextoCategoria() as $row) {
                $textoCategoria->addMultiOption($row->getId_textocategoria(), $row->getSt_textocategoria());
            }
        }
        $textoCategoria->setValue($params['id_textocategoria']);
    }

    private function populateVariaveis (array $params = null)
    {
        $variaveis = $this->getElement('variaveis');
        $result = $this->negocio->findBy('G2\Entity\TextoVariaveis', $params,array('st_textovariaveis' => 'ASC'));
        if ($result) {
            foreach ($result as $row) {
                $variaveis->addMultiOption($row->getId_textovariaveis(), $row->getSt_textovariaveis());
            }
        }
    }

    private function popularCabecalhoRodape ()
    {
        $paramsRodape = array('id_textocategoria' => \G2\Constante\TextoCategoria::RODAPE
                             ,'id_entidade' => $this->negocio->sessao->id_entidade);

        $paramsCabecalho = array('id_textocategoria' => \G2\Constante\TextoCategoria::CABECALHO
                             ,'id_entidade' => $this->negocio->sessao->id_entidade);

        $rodape = $this->getElement('id_rodape');
        $result = $this->negocio->findBy('G2\Entity\TextoSistema', $paramsRodape);
        $rodape->addMultiOption(null, 'Selecione');
        if ($result) {
            foreach ($result as $row) {
                $rodape->addMultiOption($row->getId_textosistema(), $row->getSt_textosistema());
            }
        }

        $cabecalho = $this->getElement('id_cabecalho');
        $result = $this->negocio->findBy('G2\Entity\TextoSistema', $paramsCabecalho);
        $cabecalho->addMultiOption(null, 'Selecione');
        if ($result) {
            foreach ($result as $row) {
                $cabecalho->addMultiOption($row->getId_textosistema(), $row->getSt_textosistema());
            }
        }
    }


}

<?php

/**
 * Form para aplicacao avaliacao
 * @since 04-12-2013
 * @author Débora Castro <debora.castro@unyleya.com.br>
 * @package forms
 */
use G2\Negocio\AplicacaoAvaliacao;

class AplicacaoAvaliacaoForm extends Zend_Form
{

    private $negocio;

    public function __construct($param = null)
    {
        $this->negocio = new AplicacaoAvaliacao();
        $this->addElement('hidden', 'id_avaliacaoaplicacao', array('value' => ''));
        $this->addElement('hidden', 'idMunicipio', array('value' => ''));

        $sg_uf = new Zend_Form_Element_Select('sg_uf');
        $sg_uf->addMultiOption('', '--')
            ->setAttribs(array(
                'class' => 'span12'
            ));
        $this->addElement($sg_uf);

        $id_municipio = new Zend_Form_Element_Select('id_municipio');
        $id_municipio->setAttribs(array(
            'class' => 'span12'
        ));
        $this->addElement($id_municipio);

        $id_aplicadorprova = new Zend_Form_Element_Select('id_aplicadorprova');
        $id_aplicadorprova->addMultiOption('', '--')
            ->setRequired(true)
            ->setAttribs(array(
                'class' => 'span12',
                'required' => 'required'
            ))
            ->setErrorMessageSeparator('<br>')
            ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($id_aplicadorprova);

        $id_endereco = new Zend_Form_Element_Select('id_endereco');
        $id_endereco->setRequired(true)
            ->setAttribs(array(
                'class' => 'span12',
                'required' => 'required'
            ))
            ->setErrorMessageSeparator('<br>')
            ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($id_endereco);

        $id_horarioaula = new Zend_Form_Element_Select('id_horarioaula');
        $id_horarioaula->setRequired(true)
            ->setAttribs(array(
                'class' => 'span12',
                'required' => 'required'
            ))
            ->setErrorMessageSeparator('<br>')
            ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($id_horarioaula);

        $dt_aplicacao = new Zend_Form_Element_Text('dt_aplicacao');
        $dt_aplicacao->setRequired(true)
            ->setAttribs(array(
                'class' => 'span12 maskDate datepicker',
                'required' => 'required',
                'readonly' => 'readonly',
                'placeholder' => '__/__/____',
                'style' => 'background: #fff; cursor: pointer;'
            ));
        $this->addElement($dt_aplicacao);

        $this->addElement('checkbox', 'bl_unica', array(
            'name' => 'bl_unica',
            'checkedValue' => 1,
            'uncheckedValue' => 0)
        );

        $dt_antecedenciaminima = new Zend_Form_Element_Text('dt_antecedenciaminima');
        $dt_antecedenciaminima->setRequired(true)
            ->setAttribs(array(
                'class' => 'span12 maskDate datepicker',
                'required' => 'required',
                'readonly' => 'readonly',
                'placeholder' => '__/__/____',
                'style' => 'background: #fff; cursor: pointer;'
            ))
            ->setErrorMessageSeparator('<br>')
            ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($dt_antecedenciaminima);

        $dt_alteracaolimite = new Zend_Form_Element_Text('$dt_alteracaolimite');
        $dt_alteracaolimite->setRequired(true)
            ->setAttribs(array(
                'class' => 'span12 maskDate datepicker',
                'required' => 'required',
                'readonly' => 'readonly',
                'placeholder' => '__/__/____',
                'style' => 'background: #fff; cursor: pointer;'
            ))
            ->setErrorMessageSeparator('<br>')
            ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($dt_alteracaolimite);

        $nu_maxaplicacao = new Zend_Form_Element_Text('nu_maxaplicacao');
        $nu_maxaplicacao->setValue(1)
            ->setRequired(true)
            ->setAttribs(array(
                'class' => 'span6',
                'required' => 'required'
            ))
            ->setErrorMessageSeparator('<br>')
            ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($nu_maxaplicacao);

        //botão de submit
        $this->addElement('submit', 'btnSbmit', array(
            'class' => 'btn btn-success  pull-right',
            'name' => 'Salvar',
            'id' => 'btnSbmit'
        ));

        //popula os selects
        $this->populateUF();
        //$this->populateHorario();
        //$this->populateAplicador();

        if ($param['id_avaliacaoaplicacao'] != '') {
            $this->populateForm($param);
        }

        //remove os decorators
        foreach ($this->getElements() as $element) {
            $element->removeDecorator('HtmlTag');
            $element->removeDecorator('DtDdWrapper');
            $element->removeDecorator('label');
        }
    }

    /**
     * Popula Select UF
     * @param void
     * @return object Zend_Form_Element_Select
     */
    private function populateUF()
    {
        $sg_uf = $this->getElement('sg_uf');
        $result = $this->negocio->findAll('G2\Entity\Uf');
        if ($result) {
            foreach ($result as $row) {
                $sg_uf->addMultiOption($row->getSg_uf(), $row->getSt_uf());
            }
        }
    }

    private function populateHorario()
    {
        $id_horarioaula = $this->getElement('id_horarioaula');
        $params['id_entidade'] = $this->negocio->sessao->id_entidade;
        $result = $this->negocio->findBy('G2\Entity\HorarioAula', $params);
        if ($result) {
            foreach ($result as $row) {
                $id_horarioaula->addMultiOption($row->getId_horarioaula(), $row->getSt_horarioaula());
            }
        }
    }

    private function populateAplicador($param)
    {
        $id_aplicadorprova = $this->getElement('id_aplicadorprova');
        $result = $this->negocio->findEnderecoAplicador($param);
        if ($result) {
            foreach ($result as $row) {
                $id_aplicadorprova->addMultiOption($row->getId_aplicadorprova(), $row->getSt_aplicadorprova());
            }
        }
    }

    private function populateEndereco($param)
    {
        $id_endereco = $this->getElement('id_endereco');
        $result = $this->negocio->findEnderecoAplicador($param);
        if ($result) {
            foreach ($result as $row) {
                $id_endereco->addMultiOption($row->getId_endereco(), $row->getSt_endereco());
            }
        }
    }

    private function populateForm($param)
    {
        $result = $this->negocio->find('G2\Entity\VwAvaliacaoAplicacao', $param['id_avaliacaoaplicacao']);
        if ($result) {
            $this->addElement('hidden', 'id_avaliacaoaplicacao', array('value' => $param['id_avaliacaoaplicacao']));
            $this->getElement('sg_uf')->setValue($result->getSg_uf());
            $this->addElement('hidden', 'idMunicipio', array('value' => $result->getId_municipio()));
//            $this->getElement('id_municipio')->setValue($result->getId_municipio());
            $this->getElement('id_aplicadorprova')->setValue($result->getId_aplicadorprova());
            $this->populateAplicador(array(
                'sg_uf' => $result->getSg_uf(),
                'id_municipio' => $result->getId_municipio(),
                'bl_ativo' => 1
            ));
            $this->getElement('id_horarioaula')->setValue($result->getId_horarioaula());
            if ($result->getBl_unica() == 1) {
                $this->getElement('bl_unica')->setAttrib('checked', 'checked');
            }
            if ($result->getDt_aplicacao()) {
                $this->getElement('dt_aplicacao')->setValue($result->getDt_aplicacao()->format('d/m/Y'));
            }
            if ($result->getDt_antecedenciaminima()) {
                $this->getElement('dt_antecedenciaminima')->setValue($result->getDt_antecedenciaminima()->format('d/m/Y'));
            }
            if ($result->getDt_alteracaolimite()) {
                $this->getElement('dt_alteracaolimite')->setValue($result->getDt_alteracaolimite()->format('d/m/Y'));
            }
            $this->getElement('nu_maxaplicacao')->setValue($result->getNu_maxaplicacao());
            $this->populateEndereco(array('id_endereco' => $result->getId_endereco()));
        }
    }

//    
//     if ($result->getDt_inicio()) {
//                $this->getElement('dt_inicio')->setValue($result->getDt_inicio()->format('d/m/Y'));
//            }
//            if ($result->getDt_fim()) {
//                $this->getElement('dt_fim')->setValue($result->getDt_fim()->format('d/m/Y'));
//            }
//            $this->populateTextoCategoria(array(
//                'id_textoexibicao' => $result->getId_textoexibicao()->getId_textoexibicao(),
//                'id_textocategoria' => $result->getId_textocategoria()->getId_textocategoria()
//            ));
}

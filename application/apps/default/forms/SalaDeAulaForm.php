<?php

/**
 * Form gerador dos inputs dos formulários dinamicos
 * @since 14/07/2014
 * @author Débora Castro <debora.castro@unyleya.com.br>
 * @package forms
 */
use G2\Negocio\GerenciaSalas;

class SalaDeAulaForm extends Zend_Form {

    private $negocio;

    public function __construct($options = null) {

        $this->negocio = new \G2\Negocio\GerenciaSalas();

        //Retirado
        $id_sistema = new Zend_Form_Element_Select('id_sistema');
        $id_sistema->addMultiOptions(array('2' => 'Actor', '6' => 'Moodle', '11'=>'A+', '15'=>'BlackBoard'));
        $this->addElement($id_sistema);

        $id_periodoletivo = new Zend_Form_Element_Select('id_periodoletivo',array('class'=>'span12'));
        $id_periodoletivo->addMultiOption('', '--')
                ->setRequired(true)
                ->setAttrib('required', 'required')
                ->setErrorMessageSeparator('<br>')
                ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($id_periodoletivo);

        $id_modalidadesaladeaula = new Zend_Form_Element_Select('id_modalidadesaladeaula',array('class'=>'span12'));
        $id_modalidadesaladeaula->addMultiOption('', '--')
                ->setRequired(true)
                ->setAttrib('required', 'required')
                ->setErrorMessageSeparator('<br>')
                ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($id_modalidadesaladeaula);

        $id_tiposaladeaula = new Zend_Form_Element_Select('id_tiposaladeaula',array('class'=>'span12'));
        $id_tiposaladeaula->addMultiOption('', '--')
                ->setRequired(true)
                ->setAttrib('required', 'required')
                ->setErrorMessageSeparator('<br>')
                ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($id_tiposaladeaula);

        $id_situacao = new Zend_Form_Element_Select('id_situacao',array('class'=>'span12'));
        $id_situacao->addMultiOption('', '--')
                ->setRequired(true)
                ->setAttrib('required', 'required')
                ->setErrorMessageSeparator('<br>')
                ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($id_situacao);
 
        $this->addElement('hidden', 'idSalaDeAulaEdit', array('value' => ''));

        $st_saladeaula = new Zend_Form_Element_Text('st_saladeaula', array('class'=>'span12')  );
        $st_saladeaula->setRequired(true)
                ->setAttrib('required', 'required')
                ->setErrorMessageSeparator('<br>')
                ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($st_saladeaula);

        $id_avaliacaoconjunto = new Zend_Form_Element_Select('id_avaliacaoconjunto',array('class'=>'span12'));
        $id_avaliacaoconjunto->addMultiOption('', '--')
                ->setRequired(true)
                ->setAttrib('required', 'required')
                ->setErrorMessageSeparator('<br>')
                ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($id_avaliacaoconjunto);


        $id_categoriasala = new Zend_Form_Element_Select('id_categoriasala',array('class'=>'span12'));
        $id_categoriasala->addMultiOption('', '--')
                ->setRequired(true)
                ->setAttrib('required', 'required')
                ->setErrorMessageSeparator('<br>')
                ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($id_categoriasala);

        $this->populateSituacao();
        $this->populatePeriodo();
        $this->populateModalidade();
        $this->populateTipo();
        $this->populateAvaliacao();
        $this->populateCategoria();

        $idSala = $options['id_saladeaula'];
        if (!empty($idSala)) {
            $this->populateForm($idSala);
        }

        foreach ($this->getElements() as $element) {
            $element->removeDecorator('HtmlTag');
            $element->removeDecorator('DtDdWrapper');
            $element->removeDecorator('label');
        }
    }

    private function populateForm($id) {
        $to = new \SalaDeAulaIntegracaoTO();
        $to->setId_saladeaula($id);
        $to->fetch(false,true,true);
        
        $toSala = new \SalaDeAulaTO();
        $toSala->setId_saladeaula($id);
        $toSala->fetch(true,true,true);

//        $result = $this->negocio->findBySalaDeAula(array('id_saladeaula' => $id));
        $resultAva = $this->negocio->findByVwAvaliacaoConjuntoReferencia(array('id_saladeaula' => $id));

//        if ($result) {
            $this->getElement('idSalaDeAulaEdit')->setValue($id);
//            foreach ($result as $row) {
                $this->getElement('id_situacao')->setValue($toSala->getId_situacao());
                $this->getElement('st_saladeaula')->setValue($toSala->getSt_saladeaula());
                $this->getElement('id_periodoletivo')->setValue($toSala->getId_periodoletivo());
                $this->getElement('id_modalidadesaladeaula')->setValue($toSala->getId_modalidadesaladeaula());
                $this->getElement('id_tiposaladeaula')->setValue($toSala->getId_tiposaladeaula());
                $this->getElement('id_categoriasala')->setValue($toSala->getId_categoriasala());
                if ($resultAva) {
                    foreach ($resultAva as $rows) {
                        $this->getElement('id_avaliacaoconjunto')->setValue($rows->getId_avaliacaoconjunto());
                    }
                }
                $this->getElement('id_sistema')->setValue($to->getId_sistema() ? $to->getId_sistema() : null);
//            }
//        }
    }

    /**
     * Popula Select situacao
     * @param void
     * @return object Zend_Form_Element_Select
     */
    private function populateSituacao() {
        $situacao = $this->getElement('id_situacao');
        $result = $this->negocio->findBy('G2\Entity\Situacao', array('st_tabela' => 'tb_saladeaula'));
        if ($result) {
            foreach ($result as $row) {
                $situacao->addMultiOption($row->getId_situacao(), $row->getSt_situacao());
            }
        }
    }

    /**
     * Popula Select Perido Letivo
     * @param void
     * @return object Zend_Form_Element_Select
     */
    private function populatePeriodo() {
        $periodo_letivo = $this->getElement('id_periodoletivo');
        $result = $this->negocio->findByPeriodo();
        if ($result) {
            foreach ($result as $row) {
                $periodo_letivo->addMultiOption($row->getId_periodoletivo(), $row->getSt_periodoletivo());
            }
        }
    }

    /**
     * Popula Select Modalidade Sala de Aula
     * @param void
     * @return object Zend_Form_Element_Select
     */
    private function populateModalidade() {
        $modalidadesaladeaula = $this->getElement('id_modalidadesaladeaula');
        $result = $this->negocio->findByModalidade();
        if ($result) {
            foreach ($result as $row) {
                $modalidadesaladeaula->addMultiOption($row->getId_modalidadesaladeaula(), $row->getSt_modalidadesaladeaula());
            }
        }
    }

    /**
     * Popula Select Tipos de Sala
     * @param void
     * @return object Zend_Form_Element_Select
     */
    private function populateTipo() {
        $tiposala = $this->getElement('id_tiposaladeaula');
        $result = $this->negocio->findByTipoSala();
        if ($result) {
            foreach ($result as $row) {
                $tiposala->addMultiOption($row->getId_tiposaladeaula(), $row->getSt_tiposaladeaula());
            }
        }
    }

    private function populateAvaliacao() {
        $avaliacao = $this->getElement('id_avaliacaoconjunto');
        $result = $this->negocio->findByAvaliacao();
        if ($result) {
            foreach ($result as $row) {
                $avaliacao->addMultiOption($row->getId_avaliacaoconjunto(), $row->getSt_avaliacaoconjunto());
            }
        }
    }

    private function populateCategoria() {
        $periodo_letivo = $this->getElement('id_categoriasala');
        $result = $this->negocio->findAll('G2\Entity\CategoriaSala');
        if ($result) {
            foreach ($result as $row) {
                $periodo_letivo->addMultiOption($row->getId_categoriasala(), $row->getSt_categoriasala());
            }
        }
    }

}

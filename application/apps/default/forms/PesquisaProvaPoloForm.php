<?php

/**
 * Pesquisa de provas por polo (relatório)
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class PesquisaProvaPoloForm extends Zend_Form {


    public function init()
    {
        parent::init();

        $sg_uf = new Zend_Form_Element_Select('sg_uf', array('class' => 'span2'));
        $sg_uf->setLabel('UF:')
            ->addMultiOption('', 'Selecione');
        $this->addElement($sg_uf);

        $id_municipio = new Zend_Form_Element_Select('id_municipio', array('class' => 'span2'));
        $id_municipio->setLabel('Município:');
        $this->addElement($id_municipio);

        $id_aplicador = new Zend_Form_Element_Select('id_aplicadorprova', array('class' => 'span3'));
        $id_aplicador->setLabel('Pólo:')
                ->addMultiOption('', 'Selecione');
        $this->addElement($id_aplicador);


        $dt_inicio = new Zend_Form_Element_Text('dt_inicio', array('class' => 'span2 datepicker maskDate'));
        $dt_inicio->setLabel('Data de início:');
        $this->addElement($dt_inicio);

        $dt_termino = new Zend_Form_Element_Text('dt_termino', array('class' => 'span2 datepicker maskDate'));
        $dt_termino->setLabel('Data de término:');
        $this->addElement($dt_termino);


        $this->populateAplicador();
        $this->populateUf();


        //remove os decorators
        foreach ($this->getElements() as $element) {
            $element->removeDecorator('HtmlTag');
            $element->removeDecorator('DtDdWrapper');
        }
    }

    public function populateAplicador()
    {
        $bo = new PesquisarBO();
        $aplicador = $bo->retornaLocaisProva();

        foreach ($aplicador as $value) {
            $this->getElement('id_aplicadorprova')->addMultiOption($value['id_value'], $value['st_text']);
        }
    }


    public function populateUf()
    {
        $bo = new PesquisarBO();
        $uf = $bo->retornarSelectUf();

        foreach ($uf as $value) {
            $this->getElement('sg_uf')->addMultiOption($value['valor'], $value['label']);
        }
    }

}

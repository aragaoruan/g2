<?php

/**
 * Form gerador dos inputs dos formulários dinamicos
 * @since 2013-10-14
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 * @package forms
 */
use G2\Negocio\PrimeiroAcesso as PrimeiroAcessoNegocio;

class PABoasVindasForm extends Zend_Form {

    private $negocio;
    private $entidadeNegocio;

    public function __construct(array $options = null) {

        $this->negocio = new PrimeiroAcessoNegocio();
        $this->entidadeNegocio = new \G2\Negocio\Entidade();

        $this->addElement('hidden', 'id_pa_boasvindas', array('value' => ''));
        $this->addElement('hidden', 'id_usuariocadastro', array('value' => $this->negocio->sessao->id_usuario));
        //$this->addElement('hidden', 'id_textosistema', array('value' => $this->));

        $this->addElement('textarea', 'st_texto', array(
            'required' => true,
            'class' => 'tinymce',
            'rows' => 6,
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(3, 400))
            )
        ));
        $this->st_texto->addErrorMessage('O texto é obrigatório');

        $variaveis = new Zend_Form_Element_Multiselect('variaveis');
        $variaveis->setAttribs(array(
            'class' => 'span12',
        ));
        $this->addElement($variaveis);
        
        $id_entidade = new Zend_Form_Element_Select('id_entidade');
        $this->addElement($id_entidade);

        //botão de submit
        $this->addElement('submit', 'btnSbmit', array(
            'class' => 'btn btn-success  pull-right',
            'name' => 'Salvar',
            'id' => 'btnSbmit'
        ));

        //popula os selects
        $this->populateEntidade();
        $this->populateVariaveis();

        if (is_array($options)) {
            $this->populateForm($options);
        }

        //remove os decorators
        foreach ($this->getElements() as $element) {
            $element->removeDecorator('HtmlTag');
            $element->removeDecorator('DtDdWrapper');
            $element->removeDecorator('label');
        }
    }

    private function populateForm($params) {
        $result = $this->negocio->findPABoasVindasByEntidadeSessao();
        if ($result) {
            $this->getElement('id_pa_boasvindas')->setValue($params['id_pa_boasvindas']);

            $this->getElement('st_texto')->setValue($result->getId_textosistema()->getSt_texto());
            $this->getElement('st_tituloexibicao')->setValue($result->getSt_tituloexibicao());
            $this->getElement('id_situacao')->setValue($result->getId_situacao());
            // $this->getElement('bl_ativo')->setValue($result->getBl_ativo());
            $this->getElement('st_descricao')->setValue($result->getSt_descricao());
            //  $this->getElement('id_entidade')->setValue($result->getId_entidade());
            //  $this->getElement('id_usuariocadastro')->setValue($result->getId_usuariocadastro());
            // $this->getElement('id_areaconhecimentopai')->setValue($result->getId_areaconhecimentopai());
            $this->getElement('id_tipoareaconhecimento')->setValue($result->getId_tipoareaconhecimento());
        }
    }

    /**
     * Popula Select TipoAreaConhecimento
     * @param void
     * @return object Zend_Form_Element_Select
     */
    private function populateEntidade() {

        $entidade = $this->getElement('id_entidade');
        $resultado = $this->em->findAll('G2\Entity\Entidade');
        if ($resultado) {
            foreach ($resultado as $obj) {
                $entidade->addMultiOption($obj->getId_entidade(), $obj->getSt_entidade());
            }
        }
    }

    /**
     * Popula Select TextoVariaveis
     * @param void
     * @return object Zend_Form_Element_Select
     */
    private function populateVariaveis() {
        $variaveis = $this->getElement('variaveis');
        $result = $this->negocio->findBy('G2\Entity\TextoVariaveis', array('id_textocategoria' => G2\Constante\TextoCategoria::PRIMEIRO_ACESSO));
        if ($result) {
            foreach ($result as $row) {
                $variaveis->addMultiOption($row->getId_textovariaveis(), $row->getSt_textovariaveis());
            }
        }
    }

}

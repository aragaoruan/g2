<?php

/**
 * Pesquisa de alunos para lançamento de nota
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class PesquisaLancamentoPorAlunoForm extends Zend_Form {

    public function init() {

        parent::init();

        $id_nom_email = new Zend_Form_Element_Text('st_str', array('class' => 'span12'));
        $id_nom_email->setLabel('Nome, E-mail, CPF:');
        $this->addElement($id_nom_email);

        $sg_uf = new Zend_Form_Element_Select('sg_uf', array('class' => 'span12'));
        $sg_uf->setLabel('UF:')
                ->addMultiOption('', 'Todos');
        $this->addElement($sg_uf);

        $id_municipio = new Zend_Form_Element_Select('id_municipio', array('class' => 'span12'));
        $id_municipio->setLabel('Município:')
                ->addMultiOption('', 'Todos');
        $this->addElement($id_municipio);

        $id_aplicador = new Zend_Form_Element_Select('id_aplicadorprova', array('class' => 'span12'));
        $id_aplicador->setLabel('Local de Prova:')
                ->addMultiOption('', '--');
        $this->addElement($id_aplicador);

        $dt_aplicacao = new Zend_Form_Element_Text('dt_aplicacao', array('class' => 'datepicker maskDate dataAplicacao span12'));
        $dt_aplicacao->setLabel('Data de Aplicação:');
        $this->addElement($dt_aplicacao);

        $this->populateAplicador();
        $this->populateUf();


        //remove os decorators
        foreach ($this->getElements() as $element) {
            $element->removeDecorator('HtmlTag');
            $element->removeDecorator('DtDdWrapper');
        }
    }

    //nao mais utilizado
    public function populateAplicador() {
//        $bo = new PesquisarBO();
//        $aplicador = $bo->retornaLocaisProva();
//
//        foreach ($aplicador as $value) {
//            $this->getElement('id_aplicadorprova')->addMultiOption($value['id_value'], $value['st_text']);
//        }
    }

    public function populateUf() {
        $bo = new PesquisarBO();
        $uf = $bo->retornarSelectUf();

        foreach ($uf as $value) {
            $this->getElement('sg_uf')->addMultiOption($value['valor'], $value['label']);
        }
    }

}

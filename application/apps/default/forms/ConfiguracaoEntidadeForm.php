<?php

/**
 * Form para Cadastro Rapido Pessoa
 * @since 2013-13-11
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @package forms
 */
use G2\Negocio\ConfiguracaoEntidade;

class ConfiguracaoEntidadeForm extends Zend_Form
{

    private $_negocio;

    public function init()
    {
        $this->_negocio = new ConfiguracaoEntidade();

        //Tipo de banco
        $id_modelogradenotas = new Zend_Form_Element_Select('id_modelogradenotas');
        $id_modelogradenotas->addMultiOption(null, 'Selecione um modelo grade')
            ->setAttribs(array(
                'required' => 'required',
                'class' => 'span12'
            ))->setRequired(true)
            ->addErrorMessage('O campo modelo grade é obrigatório.');
        $this->addElement($id_modelogradenotas);

        //Populate Inputs
        $this->populateModeloGrade();

        //remove os decorators
        foreach ($this->getElements() as $element) {
            $element->removeDecorator('HtmlTag');
            $element->removeDecorator('DtDdWrapper');
            $element->removeDecorator('label');
        }
    }

    /**
     * Populate Select Tipo de conta
     */
    private function populateModeloGrade()
    {
        $result = $this->_negocio->findAll('G2\Entity\ModeloGradesDeNotas');
        $elemento = $this->getElement('id_modelogradenotas');
        if ($result) {
            foreach ($result as $row) {
                $elemento->addMultiOption($row->getId_modelogradenotas(), $row->getSt_modelogradenotas());
            }
        }
    }

    /**
     * Populate Select of UF
     */
    private function populateBanco()
    {
        $result = $this->_negocio->findAll('G2\Entity\Banco');
        $elemento = $this->getElement('st_banco');
        if ($result) {
            foreach ($result as $row) {
                $elemento->addMultiOption($row->getSt_banco(), $row->getSt_nomebanco());
            }
        }
    }

    /**
     * Populate Select of UF
     */
    private function populateTipoCartao()
    {
        $result = $this->_negocio->findAll('G2\Entity\CartaoBandeira');
        $elemento = $this->getElement('id_cartaobandeira');
        if ($result) {
            foreach ($result as $row) {
                $elemento->addMultiOption($row->getId_cartaobandeira(), $row->getSt_cartaobandeira());
            }
        }
    }
}
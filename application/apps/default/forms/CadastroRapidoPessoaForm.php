<?php

/**
 * Form para Cadastro Rapido Pessoa
 * @since 2013-13-11
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @package forms
 */
use G2\Negocio\Pessoa;

class CadastroRapidoPessoaForm extends Zend_Form
{
    /** @var \G2\Negocio\Pessoa $_negocio */
    private $_negocio;

    public function init()
    {
        $this->_negocio = new Pessoa();

        $this->addElement('hidden', 'id', array('value' => ''));

        //CPF
        $st_cpf = new Zend_Form_Element_Text('st_cpf');
        $st_cpf->setAttribs(array(
            'required' => 'required'
        ))
            ->setRequired(true)
            ->addErrorMessage('O campo CPF é obrigatório.');
        $this->addElement($st_cpf);


        $dt_nascimento = new Zend_Form_Element_Text('dt_nascimento');
        $dt_nascimento->setAttribs(array(
            'required' => 'required',
            'class' => 'maskDate span9',
            'style' => 'width:100px'
        ))
            ->setRequired(true)
            ->addErrorMessage('O campo Data de Nascimento é obrigatório.');
        $this->addElement($dt_nascimento);

        //Nome
        $st_nomecompleto = new Zend_Form_Element_Text('st_nomecompleto');
        $st_nomecompleto->setAttribs(array(
            'required' => 'required',
            'class' => 'span12'
        ))
            ->addValidator('StringLength', array(10, 255))
            ->setRequired(true)
            ->addErrorMessage('O campo Nome Completo é obrigatório.');
        $this->addElement($st_nomecompleto);

        //Login
        $st_login = new Zend_Form_Element_Text('st_login');
        $st_login->addValidator('StringLength', array(10, 255))
            ->setAttribs(array(
                'autocomplete' => 'off',
                'class' => 'span12'
            ));
        $this->addElement($st_login);


        //idEndereço
        $id_endereco = new Zend_Form_Element_Hidden('id_endereco');
        $this->addElement($id_endereco);

        $id_endereco_corresp = new Zend_Form_Element_Hidden('id_endereco_corresp');
        $this->addElement($id_endereco_corresp);

        //Nacionalidade
        $id_nacionalidade = new Zend_Form_Element_Select('id_nacionalidade');
        $id_nacionalidade->addMultiOption(null, 'Selecione')
            ->setAttribs(array(
                'class' => 'span12',
                'required' => 'required',
                'title' => 'Nacionalidade'
            ))
            ->setRequired(true)
            ->addErrorMessage('O campo Nacionalidade é obrigatório.')
            ->setValue(22);
        $this->addElement($id_nacionalidade);

        //Dados Bancarios

        //Banco
        $id_banco = new Zend_Form_Element_Select('id_banco');
        $id_banco->addMultiOption(null, 'Selecione')
            ->setAttribs(array(
                'class' => 'span12 form-control',
                'title' => 'Banco'
            ));
        $this->addElement($id_banco);


        //País
        $id_pais = new Zend_Form_Element_Select('id_pais');
        $id_pais->addMultiOption(null, 'Selecione')
            ->setAttribs(array(
                'class' => 'span12 form-control',
                'title' => 'Pais',
                'required' => 'required'
            ))
            ->addErrorMessage('O campo País é obrigatório.')
            ->setValue(22);
        $this->addElement($id_pais);

        $id_pais_corresp = new Zend_Form_Element_Select('id_pais_corresp');
        $id_pais_corresp->addMultiOption(null, 'Selecione')
            ->setAttribs(array(
                'class' => 'span12 form-control',
                'required' => 'required',
                'title' => 'Pais'
            ))
            ->setRequired(true)
            ->addErrorMessage('O campo País é obrigatório.')
            ->setValue(22);
        $this->addElement($id_pais_corresp);

//        //CEP
//        $st_cep = new Zend_Form_Element_Text('st_cep');
//        $st_cep->setAttribs(array(
//            'style' => 'width:85px',
////                    'class' => 'span7'
//        ))->addValidator('StringLength', array(10, 10))
//            ->addErrorMessage('O campo CEP é obrigatório.');
//        $this->addElement($st_cep);

        $st_cep_corresp = new Zend_Form_Element_Text('st_cep_corresp');
        $st_cep_corresp->setAttribs(array(
            'required' => 'required',
            'style' => 'width:85px',
            'required' => 'required'
        ))->addValidator('StringLength', array(10, 10))
            ->setRequired(true)
            ->addErrorMessage("O campo CEP é obrigatório.");
        $this->addElement($st_cep_corresp);

        //UF
        $sg_uf = new Zend_Form_Element_Select('sg_uf');
        $sg_uf->addMultiOption('', 'Selecione')
            ->setAttrib('class', 'form-control span12')
            ->setAttrib('title', 'UF');
        $this->addElement($sg_uf);

        $sg_uf_corresp = new Zend_Form_Element_Select('sg_uf_corresp');
        $sg_uf_corresp->addMultiOption('', 'Selecione')
            ->setAttrib('class', 'form-control span12')
            ->setAttrib('title', 'UF');
        $this->addElement($sg_uf_corresp);

        $sg_ufnascimento = new Zend_Form_Element_Select('sg_ufnascimento');
        $sg_ufnascimento->addMultiOption('', 'Selecione')
            ->setAttribs(array(
                'class' => 'span12',
                'title' => 'Estado de Nascimento'
            ))->addErrorMessage('O campo Estado de Nascimento é obrigatório.');
        $this->addElement($sg_ufnascimento);

//        $uf_documento = new Zend_Form_Element_Select('uf_documento');
//        $uf_documento->setAttribs(array(
//            'class' => 'span12',
//            'required' => 'required',
//            'title' => 'Estado'
//        ))
//            ->addMultiOption(null, 'Selecione');
//        $this->addElement($uf_documento);
        //Municipio
        $id_municipio = new Zend_Form_Element_Select('id_municipio');
        $id_municipio->addMultiOption(null, 'Selecione o UF')
            ->setAttribs(array(
                'class' => 'span12',
                'title' => 'Municipio'
            ))->setRequired(false);
        //->addErrorMessage('O campo Municipio é obrigatório.');
        $this->addElement($id_municipio);

        $id_municipio_corresp = new Zend_Form_Element_Select('id_municipio_corresp');
        $id_municipio_corresp->addMultiOption(null, 'Selecione o UF')
            ->setAttribs(array(
                'required' => 'required',
                'class' => 'span12',
                'title' => 'Municipio'
            ))->setRequired(true)
            ->addErrorMessage('O campo Municipio é obrigatório.');
        $this->addElement($id_municipio_corresp);

        $id_municipionascimento = new Zend_Form_Element_Select('id_municipionascimento');
        $id_municipionascimento->addMultiOption(null, 'Selecione o UF')
            ->setAttribs(array(
                'class' => 'span12',
                'title' => 'Município de Nascimento'
            ))->addErrorMessage('O campo Município de Nascimento é obrigatório.');
        $this->addElement($id_municipionascimento);

        //St_endereco
        $st_endereco = new Zend_Form_Element_Text('st_endereco');
        $st_endereco->setAttribs(array(
            'required' => 'required',
            'class' => 'span10'
        ))
            ->addErrorMessage('O campo Endereço é obrigatório.');
        $this->addElement($st_endereco);

        $st_endereco_corresp = new Zend_Form_Element_Text('st_endereco_corresp');
        $st_endereco_corresp->setAttribs(array(
            'required' => 'required',
            'class' => 'span10',
        ))->setRequired(true)
            ->addErrorMessage("O campo Endereço é obrigatório.");
        $this->addElement($st_endereco_corresp);

        //Tipo endereço
        $id_tipoendereco = new Zend_Form_Element_Hidden('id_tipoendereco');
        $this->addElement($id_tipoendereco);

        $id_tipoendereco_corresp = new Zend_Form_Element_Hidden('id_tipoendenreco_corresp');
        $this->addElement($id_tipoendereco_corresp);

        //Cidade
//        $st_cidade = new Zend_Form_Element_Text('st_cidade');
//        $st_cidade->setAttribs(array(
//            'required' => 'required',
//            'class' => 'span10'
//        ))->setRequired(true)
//            ->addErrorMessage('O campo Cidade é obrigatório.');
//        $this->addElement($st_cidade);
        //Bairro
        $st_bairro = new Zend_Form_Element_Text('st_bairro');
        $st_bairro->setAttribs(array(
            'required' => 'required',
            'class' => 'span10'
        ))
            ->addErrorMessage('O campo Bairro é obrigatório.');
        $this->addElement($st_bairro);

        $st_bairro_corresp = new Zend_Form_Element_Text('st_bairro_corresp');
        $st_bairro_corresp->setAttribs(array(
            'required' => 'required',
            'class' => 'span10'
        ))->setRequired(true)
            ->addErrorMessage("O campo Bairro é obrigatório.");
        $this->addElement($st_bairro_corresp);

        //Numero
        $nu_numero = new Zend_Form_Element_Text('nu_numero');
        $nu_numero->setAttribs(array(
            'required' => 'required',
            'class' => 'span4'
        ))
            ->addErrorMessage('O campo Número é obrigatório.');
        $this->addElement($nu_numero);

        $nu_numero_corresp = new Zend_Form_Element_Text('nu_numero_corresp');
        $nu_numero_corresp->setAttribs(array(
            'required' => 'required',
            'class' => 'span4'
        ))->setRequired(true)
            ->addErrorMessage("O campo número é obrigatório.");
        $this->addElement($nu_numero_corresp);

        //Complemento
        $st_complemento = new Zend_Form_Element_Text('st_complemento');
        $st_complemento->setAttribs(array(
            'class' => 'span10'
        ));
        $this->addElement($st_complemento);

        $st_complemento_corresp = new Zend_Form_Element_Text('st_complemento_corresp');
        $st_complemento_corresp->setAttribs(array(
            'class' => 'span10'
        ));
        $this->addElement($st_complemento_corresp);

        //Telefones
        $id_telefone = new Zend_Form_Element_Hidden('id_telefone');
        $this->addElement($id_telefone);


        $id_telefone2 = new Zend_Form_Element_Hidden('id_telefone2');
        $this->addElement($id_telefone2);

        //DDI
        $nu_ddi = new Zend_Form_Element_Text("nu_ddi");
        $nu_ddi->setAttribs(array(
            'class' => 'span6',
            'required' => 'required'
        ));
        $this->addElement($nu_ddi);

        $nu_ddi2 = new Zend_Form_Element_Text("nu_ddi2");
        $nu_ddi2->setAttribs(array(
            'class' => 'span6',
        ));
        $this->addElement($nu_ddi2);

        //DDD
        $nu_ddd = new Zend_Form_Element_Text("nu_ddd");
        $nu_ddd->setAttribs(array(
            'class' => 'span6',
            'required' => 'required'
        ));
        $this->addElement($nu_ddd);

        $nu_ddd2 = new Zend_Form_Element_Text("nu_ddd2");
        $nu_ddd2->setAttribs(array(
            'class' => 'span6'
        ));
        $this->addElement($nu_ddd2);

        //Telefone
        $nu_telefone = new Zend_Form_Element_Text("nu_telefone");
        $nu_telefone->setAttribs(array(
            'class' => 'span6',
            'required' => 'required'
        ));
        $this->addElement($nu_telefone);

        //Telefone
        $nu_telefone2 = new Zend_Form_Element_Text("nu_telefone2");
        $nu_telefone2->setAttribs(array(
            'class' => 'span6'
        ));
        $this->addElement($nu_telefone2);

        //Tipo endereço
        $id_tipotelefone = new Zend_Form_Element_Select('id_tipotelefone');
        $id_tipotelefone->addMultiOption(null, 'Tipo Telefone')
            ->setAttribs(array(
                'class' => 'span12',
                'required' => 'required',
                'title' => 'Tipo Telefone'
            ));
        $this->addElement($id_tipotelefone);

        $id_tipotelefone2 = new Zend_Form_Element_Select('id_tipotelefone2');
        $id_tipotelefone2->addMultiOption(null, 'Tipo Telefone')
            ->setAttribs(array(
                'title' => 'Tipo Telefone',
                'class' => 'span12'
            ));
        $this->addElement($id_tipotelefone2);

        //Email
        $id_email = new Zend_Form_Element_Hidden('id_email');
        $this->addElement($id_email);


        $st_email = new Zend_Form_Element_Text('st_email');
        $st_email->setAttribs(array(
            'class' => 'span5',
            'required' => 'required'
        ))
            ->setRequired(true)
            ->addErrorMessage('O e-mail já está cadastrado ou o campo está em branco. Tente novamente.')
            ->addValidator('EmailAddress', TRUE);
//                ->addValidator(new Zend_Validate_Db_NoRecordExists('tb_contatosemail', 'st_email'));
        $this->addElement($st_email);


        $id_registropessoa = new Zend_Form_Element_Select('id_registropessoa');
        $id_registropessoa->setAttribs(array(
            'class' => '',
            'required' => 'required'
        ))->setRequired(true)
            ->addErrorMessage('O campo Registro é obrigatório.');
        $this->addElement($id_registropessoa);

        //Nível de ensino do usuário
        $id_nivelensino = new Zend_Form_Element_Select('id_nivelensino');
        $id_nivelensino->setAttribs(array(
            'class' => 'span12',
            'title' => 'Grau de Instrução'
        ));
        $this->addElement($id_nivelensino);

        // Tipo Sanguíneo
        $id_tiposanguineo = new Zend_Form_Element_Select('id_tiposanguineo');
        $id_tiposanguineo->setAttribs(array(
            'class' => 'span12',
            'title' => 'Tipo Sanguíneo'
        ));
        $this->addElement($id_tiposanguineo);

        //Titulção
        $id_titulacao = new Zend_Form_Element_Select('id_titulacao');
        $id_titulacao->setAttribs(array(
            'class' => 'span12',
            'title' => 'Titulação'
        ));
        $this->addElement($id_titulacao);

        //Categoria servico militar
        $id_categoriaservicomilitar = new Zend_Form_Element_Select('id_categoriaservicomilitar');
        $id_categoriaservicomilitar->setAttribs(array(
            'class' => 'span12',
            'title' => 'Categoria do Serviço Militar'
        ));
        $this->addElement($id_categoriaservicomilitar);


        //Populate Inputs
        $this->populateIdPais('id_pais');
        $this->populateIdPais('id_pais_corresp');
        $this->populateIdBanco('id_banco');

        $this->populateIdPais('id_nacionalidade');
        $this->populateSgUf('sg_uf');
        $this->populateSgUf('sg_uf_corresp');
        $this->populateSgUf('sg_ufnascimento');
        //$this->populateTipoEndereco();
        $this->populateTipoTelefone('id_tipotelefone');
        $this->populateTipoTelefone('id_tipotelefone2');
        $this->populeRegistroPessoa();
        $this->populaNivelEnsino();
        $this->populaTitulacao();
        $this->populaCategoriaMilitar();
        $this->populaTipoSanguineo();

        //remove os decorators
        foreach ($this->getElements() as $element) {
            $element->removeDecorator('HtmlTag');
            $element->removeDecorator('DtDdWrapper');
            $element->removeDecorator('label');
        }
    }

    /**
     * Popula o Select de Nível de Ensino
     */
    private function populaNivelEnsino()
    {
        $result = $this->_negocio->findAll('G2\Entity\NivelEnsino');
        $elemento = $this->getElement('id_nivelensino');
        if ($result) {
            $elemento->addMultiOption(NULL, "Selecione");
            foreach ($result as $nivel) {
                $elemento->addMultiOption($nivel->getId_nivelensino(), $nivel->getSt_nivelensino());
            }
        }
    }

    /**
     * Popula o Select de Tipo Sanguíneo
     */
    private function populaTipoSanguineo()
    {
        /** @var G2\Entity\TipoSanguineo[] $result */
        $results = $this->_negocio->findAll('\G2\Entity\TipoSanguineo');
        $element = $this->getElement('id_tiposanguineo');

        if ($results) {
            $element->addMultiOption(null, 'Não Informado');
            foreach ($results as $item) {
                $element->addMultiOption($item->getId_tiposanguineo(), $item->getSg_tiposanguineo());
            }
        }
    }

    /**
     * Popula o Select Banco
     */
    private function populateIdBanco()
    {
        $result = $this->_negocio->findby('G2\Entity\Banco', array('bl_ativo' => true));
        $elemento = $this->getElement('id_banco');
        if ($result) {
            $elemento->addMultiOption(NULL, "Selecione");
            foreach ($result as $nivel) {
                $elemento->addMultiOption($nivel->getId_banco(), $nivel->getSt_nomebanco());
            }
        }
    }

    /**
     * Populate Select of Pais
     * @param string $elem
     */
    private function populateIdPais($elem)
    {
        $result = $this->_negocio->findAll('G2\Entity\Pais');
        $elemento = $this->getElement($elem);
        if ($result) {
            foreach ($result as $row) {
                if ($row->getId_pais() != 0)
                    $elemento->addMultiOption($row->getId_pais(), $row->getSt_nomepais());
            }
        }
    }

    /**
     * Populate Select of UF
     */
    private function populateSgUf($field)
    {
        $result = $this->_negocio->findBy('G2\Entity\Uf', array(), array('st_uf' => 'ASC'));
        $elemento = $this->getElement($field);
        if ($result) {
            foreach ($result as $row) {
                $elemento->addMultiOption($row->getSg_uf(), $row->getSt_uf());
            }
        }
    }

    public function populateIdMunicipio($sgUf = null)
    {
        $result = $this->_negocio->findBy('G2\Entity\Municipio', array('sg_uf' => $sgUf), array('st_nomemunicipio' => 'ASC'));
        $elemento = $this->getElement('id_municipio');
        if ($result) {
            foreach ($result as $row) {
                $elemento->addMultiOption($row->getId_municipio(), $row->getSt_nomemunicipio());
            }
        }
    }

    /**
     * Populate Select of Tipo Endereço
     */
    private function populateTipoEndereco()
    {
        $result = $this->_negocio->findBy('G2\Entity\TipoEndereco', array('id_categoriaendereco' => 3));
        $elemento = $this->getElement('id_tipoendereco');
        if ($result) {
            foreach ($result as $row) {
                $elemento->addMultiOption($row->getId_tipoendereco(), $row->getSt_tipoendereco());
            }
        }
    }

    /**
     * Populate Select of Tipo Telefone
     * @param string $elemento Name Element
     */
    private function populateTipoTelefone($elemento)
    {
        $result = $this->_negocio->findAll('G2\Entity\TipoTelefone');
        $elemento = $this->getElement($elemento);
        if ($result) {
            foreach ($result as $row) {
                $elemento->addMultiOption($row->getId_tipotelefone(), $row->getSt_tipotelefone());
            }
        }
    }

    /**
     * Popula o select de registro pessoa
     */
    private function populeRegistroPessoa()
    {
        $result = $this->_negocio->findAll('G2\Entity\RegistroPessoa');
        $elemento = $this->getElement('id_registropessoa');
        if ($result) {
            $elemento->addMultiOption(NULL, 'Selecione');
            foreach ($result as $row) {
                $elemento->addMultiOption($row->getId_registropessoa(), $row->getSt_registropessoa());
            }
        }
    }


    /**
     * Popula o Select de Titulação
     */
    private function populaTitulacao()
    {
        $result = $this->_negocio->findAll('G2\Entity\Titulacao');
        $elemento = $this->getElement('id_titulacao');
        if ($result) {
            $elemento->addMultiOption(NULL, 'Selecione');
            foreach ($result as $row) {
                $elemento->addMultiOption($row->getId_titulacao(), $row->getSt_titulacao());
            }
        }

    }

    private function populaCategoriaMilitar(){
        $result = \G2\Constante\CategoriaServicoMilitar::getArray();
        $elemento = $this->getElement('id_categoriaservicomilitar');
        if ($result) {
            $elemento->addMultiOption(NULL, 'Selecione');
            foreach ($result as $key=>$row) {
                $elemento->addMultiOption($key, $row);
            }
        }
    }

}

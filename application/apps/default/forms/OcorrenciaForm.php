<?php

/**
 * Classe Formulario Ocorrencia
 *
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class OcorrenciaForm extends Zend_Form {

    public function init()
    {

        $id_ocorrencia = new Zend_Form_Element_Hidden('id_ocorrencia');
        $this->addElement($id_ocorrencia);

        $id_matricula = new Zend_Form_Element_Text('id_matricula');
        $id_matricula->setLabel('ID Matricula: ');
        $this->addElement($id_matricula);

        $id_evolucao = new Zend_Form_Element_Select('id_evolucao', array('class'=>'span12'));
        $id_evolucao->setLabel('Evolução: ')
                ->addMultiOptions(G2\Constante\Evolucao::getArrayOcorrencia())
                ->setRequired(true)
                ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($id_evolucao);

        $id_situacao = new Zend_Form_Element_Select('id_situacao');
        $id_situacao->setLabel('ID Situacao: ')
                ->addMultiOptions(G2\Constante\Situacao::getArrayOcorrencia())
                ->setRequired(true)
                ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($id_situacao);

        $id_usuariointeressado = new Zend_Form_Element_Text('id_usuariointeressado', array('readonly' => 'readonly'));
        $id_usuariointeressado->setLabel('ID Usuário Interessado: ')
                ->setRequired(true)
                ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($id_usuariointeressado);

        $id_categoriaocorrencia = new Zend_Form_Element_Select('id_categoriaocorrencia');
        $id_categoriaocorrencia->setRegisterInArrayValidator(false)
                ->setLabel('Categoria Ocorrencia: ');
        $this->addElement($id_categoriaocorrencia);

        $id_assuntoco = new Zend_Form_Element_Select('id_assuntoco', array('class'=>'span12'));
        $id_assuntoco->setLabel('SubAssunto: ')
                ->setRegisterInArrayValidator(false);
        $this->addElement($id_assuntoco);

        $id_saladeaula = new Zend_Form_Element_Text('id_saladeaula');
        $id_saladeaula->setLabel('ID Sala de Aula: ');
        $this->addElement($id_saladeaula);

        $st_titulo = new Zend_Form_Element_Text('st_titulo');
        $st_titulo->setLabel('Titulo: ')
                ->setRequired(true)
                ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($st_titulo);
        
        $st_ocorrencia = new Zend_Form_Element_Textarea('st_ocorrencia');
        $st_ocorrencia->setLabel('Texto Ocorrência: ')
                ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($st_ocorrencia);
        
        $id_ocorrenciaoriginal = new Zend_Form_Element_Text('id_ocorrenciaoriginal');
        $id_ocorrenciaoriginal->setLabel('ID Ocorrência Original: ');
        $this->addElement($id_ocorrenciaoriginal);
        
        $id_entidade = new Zend_Form_Element_Text('id_entidade');
        $id_entidade->setLabel('ID Entidade: ');
        $this->addElement($id_entidade);
        
        $dt_cadastro = new Zend_Form_Element_Text('dt_cadastro');
        $dt_cadastro->setLabel('dt_cadastro');
        $this->addElement($dt_cadastro);
        
        $id_usuariocadastro = new Zend_Form_Element_Text('id_usuariocadastro');
        $id_usuariocadastro->setLabel('ID Usuário Cadastro');
        $this->addElement($id_usuariocadastro);
        
        $id_motivoocorrencia = new Zend_Form_Element_Select('id_motivoocorrencia');
        $id_motivoocorrencia->setLabel('Motivo Ocorrência: ')
                ->addMultiOptions(G2\Constante\MotivoOcorrencia::getArray());
        $this->addElement($id_motivoocorrencia);
        
        $dt_atendimento = new Zend_Form_Element_Text('dt_atendimento');
        $dt_atendimento->setLabel('Data do Atendimento: ');
        $this->addElement($dt_atendimento);
        
        
        //remove os decorators
        foreach ($this->getElements() as $element) {
            $element->removeDecorator('HtmlTag');
            $element->removeDecorator('DtDdWrapper');
        }
    }

}

<?php

/**
 * Description of PesquisaOcorrencia
 *
 * @author rafael.bruno
 */
class PesquisaOcorrenciaForm extends Zend_Form
{

    private $id_funcao;
    private $id_nucleoco;

    public function init()
    {
        parent::init();

        $st_nomeinteressado = new Zend_Form_Element_Text('st_nomeinteressado', array('class' => 'span12'));
        $st_nomeinteressado->setLabel('Nome:');
        $this->addElement($st_nomeinteressado);

        $id_ocorrencia = new Zend_Form_Element_Text('id_ocorrencia', array('class' => 'span12'));
        $id_ocorrencia->setLabel('Código:');
        $this->addElement($id_ocorrencia);

        $st_emailinteressado = new Zend_Form_Element_Text('st_emailinteressado', array('class' => 'span12'));
        $st_emailinteressado->setLabel('E-mail:');
        $this->addElement($st_emailinteressado);

        $dt_cadastroocorrencia_inicio = new Zend_Form_Element_Text('dt_cadastroocorrencia_inicio', array(
            'class' => 'span12 datepicker maskDate',
            'placeholder' => '__/__/____'
        ));
        $dt_cadastroocorrencia_inicio->setLabel('Criada entre:');
        $this->addElement($dt_cadastroocorrencia_inicio);

        $dt_cadastroocorrencia_fim = new Zend_Form_Element_Text('dt_cadastroocorrencia_fim', array(
            'class' => 'span12 datepicker maskDate',
            'placeholder' => '__/__/____'
        ));
        $dt_cadastroocorrencia_fim->setLabel('e:');

        $this->addElement($dt_cadastroocorrencia_fim);

        $id_assuntopai = new Zend_Form_Element_Select('id_assuntocopai', array('class' => 'span12'));
        $id_assuntopai->setLabel('Assunto:')
            ->addMultiOption('', 'Todos');
        $this->addElement($id_assuntopai);

        $id_assunto = new Zend_Form_Element_Select('id_assuntoco', array('class' => 'span12'));
        $id_assunto->setLabel('Subassunto:');

        $this->addElement($id_assunto);

        $id_funcao = new Zend_Form_Element_Select('id_funcao', array('class' => 'span12'));
        $id_funcao->setRequired(true)
            ->setAttrib('required', 'required')
            ->setErrorMessageSeparator('<br>')
            ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $id_funcao->setLabel('Minha Função:');
        //$id_funcao->removeDecorator('Label');

        $this->addElement($id_funcao);

        $id_atendente = new Zend_Form_Element_Select('id_atendente', array('class' => 'span12'));
        $id_atendente->setLabel('Atendente:')
            ->addMultiOption('', 'Todos');
        $this->addElement($id_atendente);

        $id_evolucao = new Zend_Form_Element_Select('id_evolucao', array('class' => 'span12'));
        $id_evolucao->setLabel('Evolução:')
            ->addMultiOption('', 'Todos');
        $this->addElement($id_evolucao);

        $id_situacao = new Zend_Form_Element_Select('id_situacao', array('class' => 'span12', 'required' => 'required'));
        $id_situacao->setLabel('Situação:')
            ->setRequired(true);
        $this->addElement($id_situacao);

        $id_entidade = new Zend_Form_Element_Select('id_entidade', array('class' => 'span12'));
        $id_entidade->setLabel('Entidade:');
        $this->addElement($id_entidade);

        $this->populateFuncao();
        $this->populateAssunto();
        $this->populateAtendente();
        $this->populateEvolucao();
        $this->populateSituacao();
        $this->populateEntidade();

        //remove os decorators
        foreach ($this->getElements() as $element) {
            $element->removeDecorator('HtmlTag');
            $element->removeDecorator('DtDdWrapper');
        }
    }

    public function populateFuncao()
    {
        $ocorrencia = new G2\Negocio\Ocorrencia();
        $funcoes = $ocorrencia->findFuncaoCo();

        foreach ($funcoes as $value) {
            $this->getElement('id_funcao')
                ->addMultiOption($value['id_funcao'], $value['st_funcao'] . '-' . $value['st_nucleoco']);
        }

        //seta uma opcao inicial
        if (count($funcoes)) {
            $this->id_funcao = $funcoes[0]['id_funcao'];
            $this->id_nucleoco = $funcoes[0]['id_nucleoco'];
        }
        $this->getElement('id_funcao')->setValue($this->id_funcao);
    }

    public function populateAssunto()
    {
        $ocorrencia = new G2\Negocio\Ocorrencia();
        if ($this->id_nucleoco) {
            $result = $ocorrencia->findAssuntoCo(array('id_nucleoco' => $this->id_nucleoco));

            foreach ($result as $value) {
                $this->getElement('id_assuntocopai')->addMultiOption($value['id_assuntoco'], $value['st_assuntoco']);
            }
        }
    }

    public function populateAtendente()
    {
        $ocorrencia = new G2\Negocio\Ocorrencia();
        if ($this->id_nucleoco && $this->id_funcao) {
            $result = $ocorrencia->findByAtendenteCo(array('id_nucleoco' => $this->id_nucleoco));

            foreach ($result as $value) {
                $this->getElement('id_atendente')->addMultiOption($value['id_usuario'], $value['st_nomecompleto']);
            }
        }
        //se for atentende de setor, seleciona o proprio atendente e desabilita esse combo
        if ($this->id_funcao == FuncaoTO::ATENDENTE_SETOR_CA) {
            $this->getElement('id_atendente')->setValue($ocorrencia->sessao->id_usuario);
            $this->getElement('id_atendente')->setAttrib('disabled', 'disabled');
        }
    }

    public function populateEvolucao()
    {
        $this->getElement('id_evolucao')
            ->addMultiOptions(G2\Constante\Evolucao::getArrayOcorrencia());
    }

    public function populateSituacao()
    {
        $this->getElement('id_situacao')
            ->addMultiOption(null, 'Todos')
            //opcao padrao pedido pelo cliente para definir alerta de obrigatoriedade de escolha
            ->addMultiOption(0, ' -- Selecione --')
            ->addMultiOptions(G2\Constante\Situacao::getArrayOcorrencia())
            ->setValue(0);
    }

    public function populateEntidade()
    {
        $ng_holding = new G2\Negocio\Holding();

        /** @var \G2\Entity\VwEntidadesMesmaHolding[] $results */
        $results = $ng_holding->retornarEntidadesHolding();

        $elem = $this->getElement('id_entidade');
        //$elem->addMultiOption('', 'Todas');

        $current = false;

        foreach ($results as $result) {
            $elem->addMultiOption($result->getId_entidadeparceira(), $result->getSt_nomeentidadeparceira());

            if ($result->getId_entidadeparceira() == $ng_holding->sessao->id_entidade) {
                $current = true;
            }
        }

        // Caso a entidade da sessão não estiver no combo, adicioná-la manualmente
        // (em caso de configuração não feita)
        if (!$current) {
            /** @var \G2\Entity\Entidade $en_entidade */
            $en_entidade = $ng_holding->find('\G2\Entity\Entidade', $ng_holding->sessao->id_entidade);
            $elem->addMultiOption($en_entidade->getId_entidade(), $en_entidade->getSt_nomeentidade());
        }

        $elem->setValue($ng_holding->sessao->id_entidade);
    }

}

<?php

/**
 * Form gerador dos inputs dos formulários dinamicos
 * @since 11-10-2013
 * @author Débora Castro <debora.castro@unyleya.com.br>
 * @package forms
 */
class PesquisaDisciplinaForm extends Zend_Form
{

    private $negocio;

    public function __construct()
    {

        $this->negocio = new \G2\Negocio\GerenciaSalas();

        $this->addElement('hidden', 'id_disciplina', array('value' => ''));

        $this->addElement('text', 'st_disciplina', array('class' => 'span12', 'pattern' => '.{3,}', 'placeholder' => 'Pesquise por pelo menos 3 caracteres da disciplina'
        ));

        $id_organizacao = new Zend_Form_Element_Select('id_entidade', array('class' => 'span12'));
        $id_organizacao
            //->addMultiOption('', '--')
            ->setLabel('Organização: ')
            ->setAttrib('required', 'required')
            ->setErrorMessageSeparator('<br>')
            ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($id_organizacao);

//        $id_areaconhecimento = new Zend_Form_Element_Select('id_areaconhecimento', array('class' => 'span2'));
//        $id_areaconhecimento->addMultiOption('', '--')
//            ->setLabel('Área: ');
//        $this->addElement($id_areaconhecimento);

        $id_serie = new Zend_Form_Element_Select('id_serie', array('class' => 'span12'));
        $id_serie->addMultiOption('', '--')
            ->setLabel('Serie: ');
        $this->addElement($id_serie);

        //botão de submit
//        $this->addElement('submit', 'btnPesq', array(
//            'class' => 'btn btn-info bt-sm  pull-right',
//            'name' => 'Pesquisar',
//            'id' => 'btnPesq'
//        ));
        //popula os selects
        $this->populateOrganizacao();
        $this->populateSerie();

        //remove os decorators
        foreach ($this->getElements() as $element) {
            $element->removeDecorator('HtmlTag');
            $element->removeDecorator('DtDdWrapper');
        }
    }

    private function populateOrganizacao()
    {
        $organizacao = $this->getElement('id_entidade');
        $result = $this->negocio->findByOrganizacaoDisciplina();
        if ($result) {
            foreach ($result as $row) {
                $organizacao->addMultiOption($row->getId_entidade(), $row->getSt_nomeentidade());
            }
        }
    }

    private function populateSerie()
    {
        $periodo_letivo = $this->getElement('id_serie');

        $result = $this->negocio->findAll('\G2\Entity\Serie');
        if ($result) {
            foreach ($result as $row) {
                $periodo_letivo->addMultiOption($row->getId_serie(), $row->getSt_serie());
            }
        }
    }

}

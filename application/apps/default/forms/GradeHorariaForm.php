<?php

/**
 * Class GradeHorariaForm
 * Formulario para funcionalidade de GradeHoraria
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class GradeHorariaForm extends Zend_Form
{
    public function init()
    {

        $id_gradehoraria = new Zend_Form_Element_Hidden('id_gradehoraria');
        $this->addElement($id_gradehoraria);


        $st_nomegrade = new Zend_Form_Element_Text('st_nomegradehoraria');
        $st_nomegrade->setAttribs(array(
            'class' => 'span12',
            'required' => 'required',
            'title' => 'Nome da Grade'
        ))->setRequired(true)
            ->addErrorMessage('Campo Nome da Grade é obrigatório.');
        $this->addElement($st_nomegrade);

        $dt_inicial = new Zend_Form_Element_Text('dt_iniciogradehoraria');
        $dt_inicial->setAttribs(array(
            'class' => 'span8',
            'required' => 'required',
            'placeholder' => 'Data inicial',
            'title' => 'Data inicial',
        ));

        $this->addElement($dt_inicial);


        $dt_termino = new Zend_Form_Element_Text('dt_fimgradehoraria');
        $dt_termino->setAttribs(array(
            'class' => 'span8',
            'required' => 'required',
            'placeholder' => 'Data final',
            'title' => 'Data final',
        ));

        $this->addElement($dt_termino);


        $id_situacao = new Zend_Form_Element_Select('id_situacao');
        $id_situacao->setAttribs(array(
            'class' => 'span12',
            'title' => 'Situação',
            'required' => 'required'
        ))->setRequired(true)
            ->addErrorMessage('O campo Situação é obrigatório.');
        $this->addElement($id_situacao);
        $this->populaSituacao();


        //remove os decorators
        foreach ($this->getElements() as $element) {
            $element->removeDecorator('HtmlTag');
            $element->removeDecorator('DtDdWrapper');
            $element->removeDecorator('label');
        }

    }

    /**
     * Popula dados do select de situação
     */
    private function populaSituacao()
    {
        $negocio = new \G2\Negocio\Situacao();
        $result = $negocio->retornaSituacaoFindBy(array('st_tabela' => 'tb_gradehoraria'));
        $elemento = $this->getElement('id_situacao');
        if ($result->getType() == 'success') {
            $elemento->addMultiOption('', 'Selecione');
            foreach ($result->getMensagem() as $row) {
                $elemento->addMultiOption($row['id_situacao'], $row['st_situacao']);
            }
        }
    }

} 
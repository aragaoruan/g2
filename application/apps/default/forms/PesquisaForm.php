<?php

/**
 * Form gerador dos inputs dos formulários dinamicos
 * @since 2013-07-25
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @package forms
 */
class PesquisaForm extends Zend_Form
{

    private $groupSearch;

    public function __construct($options = null)
    {
        parent::__construct();

        $this->setMethod('post')
            ->setAttrib('id', 'formPesquisa')
            ->setAttrib('onsubmit', 'return false')
            ->setAction('/pesquisa/pesquisar');

        //cria o campo hidden com o valor do matodo que será renderizado no submit
        if ($options['actionPesquisar']) {
//            $this->addElement('hidden', 'actionMethod', array('value' => $options['actionPesquisar']));
            $actionMethod = new Zend_Form_Element_Hidden('actionMethod');
            $actionMethod->setValue($options['actionPesquisar']);
            $actionMethod->setDecorators(array(
                'ViewHelper',
                'Errors',
                'Description',
                array(
                    array(
                        'inner' => 'HtmlTag'
                    ),
                    array(
                        'tag' => 'div',
                        'class' => 'field-item-div'
                    )
                ),
                'Label',
                array(
                    array(
                        'outter' => 'HtmlTag'
                    ),
                    array(
                        'tag' => 'div',
                        'class' => 'label-item-div'
                    )
                )
            ));
            $this->addElement($actionMethod);


            unset($options['actionPesquisar']);
        }

        //Cria campo hidden com valor do tipo
        if ($options['tipo']) {
//            $this->addElement('hidden', 'to', array('value' => $options['to']));
            $to = new Zend_Form_Element_Hidden('tipo');
            $to->setValue($options['tipo']);
            $to->setDecorators(array(
                'ViewHelper',
                'Errors',
                'Description',
                array(
                    array(
                        'inner' => 'HtmlTag'
                    ),
                    array(
                        'tag' => 'div',
                        'class' => 'field-item-div'
                    )
                ),
                'Label',
                array(
                    array(
                        'outter' => 'HtmlTag'
                    ),
                    array(
                        'tag' => 'div',
                        'class' => 'label-item-div'
                    )
                )
            ));
            $this->addElement($to);


            unset($options['tipo']);
        }

        //Cria campo hidden com valor da To
        if ($options['to']) {
//            $this->addElement('hidden', 'to', array('value' => $options['to']));
            $to = new Zend_Form_Element_Hidden('to');
            $to->setValue($options['to']);
            $to->setDecorators(array(
                'ViewHelper',
                'Errors',
                'Description',
                array(
                    array(
                        'inner' => 'HtmlTag'
                    ),
                    array(
                        'tag' => 'div',
                        'class' => 'field-item-div'
                    )
                ),
                'Label',
                array(
                    array(
                        'outter' => 'HtmlTag'
                    ),
                    array(
                        'tag' => 'div',
                        'class' => 'label-item-div'
                    )
                )
            ));
            $this->addElement($to);


            unset($options['to']);
        }


        //gera o input padrão de busca geral
        $this->getTxtSearch($options);
        //gera os campos dinamicos
        $this->geraInputsFormulario($options);

        //botão de submit
        $btSubmit = new Zend_Form_Element_Submit('btnPesquisar');
        $btSubmit->setName('Pesquisar')
            ->setAttrib('id', 'btnPesquisar')
            ->setAttrib('name', 'btnPesquisar')
            ->setAttrib('class', 'btn-custom btn')
            ->setDecorators(array(
                'ViewHelper',
                'Errors',
                'Description',
                array(
                    array(
                        'inner' => 'HtmlTag'
                    ),
                    array(
                        'tag' => 'div',
                        'class' => 'container-btnSubmit'
                    )
                ),
            ));
        $this->addElement($btSubmit);
        //botão de adicionar
        if ($options['funcionalidade']) {
            $btAdd = new Zend_Form_Element_Button('btnAddNew');
            $btAdd->setName('Adicionar')
                ->setAttrib('id', 'btnAddNew')
                ->setAttrib('name', 'btnAddNew')
                ->setAttrib('class', 'addNew btn')
                ->setDecorators(array(
                    'ViewHelper',
                    'Errors',
                    'Description',
                    array(
                        array(
                            'inner' => 'HtmlTag'
                        ),
                        array(
                            'tag' => 'div',
                            'class' => 'container-btnAdd'
                        )
                    ),
                ));

            if ($options['funcionalidade']['id_situacao']['id_situacao'] == 123) {
                $btAdd->setAttrib('rel', $options['funcionalidade']['st_urlcaminho']);
            }

            $this->addElement($btAdd);
        }


        //remove os decorators
        foreach ($this->getElements() as $element) {
            $element->removeDecorator('HtmlTag');
            $element->removeDecorator('DtDdWrapper');
        }
    }

    /**
     * Gera percorre o array dos inputs e gera o respectivo input
     * @param array $arrInputs
     * @return void
     */
    private function geraInputsFormulario($arrInputs)
    {
        unset($arrInputs['funcionalidade']);
        if ($arrInputs) {
            foreach ($arrInputs as $valueInput) {
                switch ($valueInput['type']) {
                    case 'DateField' :
                        $this->getInputText($valueInput, true);
                        break;
                    case 'CheckBox':
                        $this->getCheckBox($valueInput);
                        //agrupa os campos da busca padrão em um fieldset
                        if ($this->groupSearch) {
                            //Verifica se o checkbox criado esta no grupo dos checks da pesquisa generica e adiciona o atributo de checked
                            if (in_array($valueInput['name'], $this->groupSearch)) {
                                $this->getElement($valueInput['name'])
                                    ->setAttrib('checked', 'checked');
                            }
                            $this->addDisplayGroup($this->groupSearch, 'search');
                        }
                        break;
                    case 'DropDownList':
                        $this->getSelect($valueInput);
                        break;
                    case 'TextInput':
                        $this->getInputText($valueInput);
                        break;
                    default :
                        return null;
                        break;
                }
            }
        }
    }

    /**
     * Cria o input de texto
     * @param array $arrProperties Array com propriedades do input
     * @return Zend_Form_Element_Text
     */
    private function getInputText($arrProperties, $stDate = false)
    {
        $input = new Zend_Form_Element_Text($arrProperties['name']);
        //seta a label
        if ($arrProperties['label']) {
            $input->setLabel($arrProperties['label']);
        }
        //seta o value
        if ($arrProperties['value']) {
            $input->setAttrib('value', $arrProperties['value']);
        }
        if ($arrProperties['type'] == 'DateField') {
            $input->setAttrib('placeholder', 'dd/mm/yyyy');
        }
        if ($stDate == true) {
            $input->setAttrib('pattern', '\d{2}\/\d{2}\/\d{4}');
            $input->setAttrib('class', 'maskDate datepicker');
        }
        $input->setDecorators(array(
            'ViewHelper',
            'Errors',
            'Description',
            array(
                array(
                    'inner' => 'HtmlTag'
                ),
                array(
                    'tag' => 'div',
                    'class' => 'field-item-div'
                )
            ),
            'Label',
            array(
                array(
                    'outter' => 'HtmlTag'
                ),
                array(
                    'tag' => 'div',
                    'class' => 'label-item-div'
                )
            )
        ));
        $this->addElement($input);
    }

    /**
     * Cria o input de texto
     * @param array $arrProperties Array com propriedades do input
     * @return Zend_Form_Element_Checkbox
     */
    private function getCheckBox($arrProperties)
    {
        $checkbox = new Zend_Form_Element_Checkbox($arrProperties['name']);
        //seta a label
        if ($arrProperties['label']) {
            $checkbox->setLabel($arrProperties['label']);
        }
        //seta o value
        if ($arrProperties['value']) {
            $checkbox->setAttrib('value', $arrProperties['value']);
        }
        if (substr($arrProperties['name'], 0, 3) != 'bl_') {
            $class = 'search';
        } else {
            $class = null;
        }
        $checkbox->setDecorators(array(
            'ViewHelper',
            'Errors',
            'Description',
            array(
                array(
                    'inner' => 'HtmlTag'
                ),
                array(
                    'tag' => 'div',
                    'class' => 'field-item-div'
                )
            ),
            'Label',
            array(
                array(
                    'outter' => 'HtmlTag'
                ),
                array(
                    'tag' => 'div',
                    'class' => 'label-item-div-check ' . $class
                )
            )
        ));
        $this->addElement($checkbox);
    }

    /**
     * Cria o select
     * @param array $arrProperties Array com propriedades do input
     * @return Zend_Form_Element_Select
     */
    private function getSelect($arrProperties)
    {
        $select = new Zend_Form_Element_Select($arrProperties['name']);
        //seta a label
        if ($arrProperties['label']) {
            $select->setLabel($arrProperties['label']);
        }
        //seta as options
        if ($arrProperties['value']) {
            $select->addMultiOption('', 'Todos');
            //verifica se o um objeto e percorre o objeto
            if (is_object($arrProperties['value'])) {
                foreach ($arrProperties['value']->mensagem as $rowValeu) {
                    $select->addMultiOption($rowValeu->id_tipocampanha, $rowValeu->st_tipocampanha);
                }
            } else {
                foreach ($arrProperties['value'] as $rowValeu) {
                    $select->addMultiOption($rowValeu['valor'], $rowValeu['label']);
                }
            }
        }

        $select->setDecorators(array(
            'ViewHelper',
            'Errors',
            'Description',
            array(
                array(
                    'inner' => 'HtmlTag'
                ),
                array(
                    'tag' => 'div',
                    'class' => 'field-item-div'
                )
            ),
            'Label',
            array(
                array(
                    'outter' => 'HtmlTag'
                ),
                array(
                    'tag' => 'div',
                    'class' => 'label-item-div-select '
                )
            )
        ));

        $this->addElement($select);
    }

    /**
     * Verifica se tem inputs do tipo CheckBox e cria o campo de busca geral
     * @param array $options
     * @return Zend_Form_Element_Text
     */
    private function getTxtSearch($options)
    {
        unset($options['funcionalidade']);
        $setField = false;
        $arrGroup = array();
        if ($options) {
            foreach ($options as $field) {
                if ($field['type'] == 'CheckBox' && substr($field['name'], 0, 3) != 'bl_') {
                    $setField = true;
                    $arrGroup[] = $field['name'];
                }
            }
        }
        if ($setField) {
            $this->groupSearch = array_merge(array('txtSearch'), $arrGroup);
            $txtSearch = new Zend_Form_Element_Text('txtSearch');
            $txtSearch->setAttrib('class', 'input-search')
                ->setAttrib('maxlength', 100)
                ->setLabel('Busca:')
                ->setName('txtSearch')
                ->setAttrib('style', 'width: 400px;')
                ->setDecorators(array(
                    'ViewHelper',
                    'Errors',
                    'Description',
                    array(
                        array(
                            'inner' => 'HtmlTag'
                        ),
                        array(
                            'tag' => 'div',
                            'class' => 'field-item-div'
                        )
                    ),
                    'Label',
                    array(
                        array(
                            'outter' => 'HtmlTag'
                        ),
                        array(
                            'tag' => 'div',
                            'class' => 'label-item-div-input-search'
                        )
                    )
                ));
            $this->addElement($txtSearch);
        }
    }

}

<?php

/**
 * 
 * @since 2013-01-21
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @package forms
 */
use \G2\Negocio\Livro as LivroNegocio;

class LivroForm extends Zend_Form
{

    public function init ()
    {

        $id_livro = new Zend_Form_Element_Hidden('id');
        $this->addElement($id_livro);

        $st_livro = new Zend_Form_Element_Text('st_livro');
        $st_livro->setAttribs(array(
                    'class' => 'span12',
                    'maxlength' => 250,
                    'required' => 'required'
                ))
                ->setRequired(true)
                ->addErrorMessage('O campo Título é obrigatório.');
        $this->addElement($st_livro);

        $st_isbn = new Zend_Form_Element_Text('st_isbn');
        $st_isbn->setAttribs(array(
                    'class' => 'span12',
                    'required' => 'required',
                    'maxlength' => 13,
                ))->setRequired(true)
                ->addErrorMessage('O campo ISBN é obrigatório.');
        $this->addElement($st_isbn);

        $id_situacao = new Zend_Form_Element_Select('id_situacao');
        $id_situacao->setAttribs(array(
                    'class' => 'span12',
                    'required' => 'required'
                ))->setRequired(true)
                ->addErrorMessage('O campo Situação é obrigatório.');
        $this->addElement($id_situacao);

        $id_livrocolecao = new Zend_Form_Element_Select('id_livrocolecao');
        $id_livrocolecao->setAttribs(array(
                    'class' => 'span12',
                    'required' => 'required'
                ))->setRequired(true)
                ->addErrorMessage('O campo Coleção é obrigatório.');
        $this->addElement($id_livrocolecao);

        $id_tipolivro = new Zend_Form_Element_Select('id_tipolivro');
        $id_tipolivro->setAttribs(array(
                    'class' => 'span12',
                    'required' => 'required'
                ))->setRequired(true)
                ->addErrorMessage('O campo Tipo Livro é obrigatório.');
        $this->addElement($id_tipolivro);


        $st_codigocontrole = new Zend_Form_Element_Text('st_codigocontrole');
        $st_codigocontrole->setAttribs(array(
                    'class' => 'span12',
                    'maxlength' => 50,
                    'required' => 'required'
                ))->setRequired(true)
                ->addErrorMessage('O campo Código Controler é obrigatório.');
        $this->addElement($st_codigocontrole);

        $st_edicao = new Zend_Form_Element_Text('st_edicao');
        $st_edicao->setAttribs(array(
            'class' => 'span12',
            'maxlength' => 50
        ));
        $this->addElement($st_edicao);


        $nu_anolancamento = new Zend_Form_Element_Select('nu_anolancamento');
        $nu_anolancamento->setAttribs(array(
                    'class' => 'span12',
                    'required' => 'required'
                ))->setRequired(true)
                ->addErrorMessage('O campo Ano de Lançamento é obrigatório.');
        $this->addElement($nu_anolancamento);

        $nu_pagina = new Zend_Form_Element_Text('nu_pagina');
        $nu_pagina->setAttribs(array(
            'class' => 'span8 numeric',
            'maxlength' => 4
        ));
        $this->addElement($nu_pagina);


        $nu_peso = new Zend_Form_Element_Text('nu_peso');
        $nu_peso->setAttribs(array(
                    'class' => 'span7 numeric',
                    'maxlength' => 4
                ))
                ->setValue(0);
        $this->addElement($nu_peso);



        //popula situação
        $this->populaSituacao();
        $this->populaColecao();
        $this->populaTipoLivro();
        $this->populaAnoLancamento();

        //remove os decorators
        foreach ($this->getElements() as $element) {
            $element->removeDecorator('HtmlTag');
            $element->removeDecorator('DtDdWrapper');
            $element->removeDecorator('label');
        }
    }

    /**
     * Popula dados do select de situação
     */
    private function populaSituacao ()
    {
        $negocio = new \G2\Negocio\Situacao();
        $result = $negocio->retornaSituacaoFindBy(array('st_tabela' => 'tb_livro'));
        $elemento = $this->getElement('id_situacao');
        if ($result->getType() == 'success') {
            $elemento->addMultiOption('', 'Selecione');
            foreach ($result->getMensagem() as $row) {
                $elemento->addMultiOption($row['id_situacao'], $row['st_situacao']);
            }
        }
    }

    /**
     * Popula dados do select com coleção
     */
    private function populaColecao ()
    {
        $negocio = new LivroNegocio();
        $result = $negocio->findAllLivroColecao();
        $elemento = $this->getElement('id_livrocolecao');
        if ($result->getType() == 'success') {
            $elemento->addMultiOption('', 'Selecione');
            foreach ($result->getMensagem() as $row) {
                $elemento->addMultiOption($row['id_livrocolecao'], $row['st_livrocolecao']);
            }
        }
    }

    /**
     * Popula o select de tipo de livro
     */
    private function populaTipoLivro ()
    {
        $negoco = new LivroNegocio();
        $result = $negoco->findAllTipoLivro();
        $elemento = $this->getElement('id_tipolivro');
        if ($result->getType() == 'success') {
            $elemento->addMultiOption('', 'Selecione');
            foreach ($result->getMensagem() as $row) {
                $elemento->addMultiOption($row['id_tipolivro'], $row['st_tipolivro']);
            }
        }
    }

    /**
     * popula ano lancamento 
     */
    private function populaAnoLancamento ()
    {
        $anoAtual = intval(date("Y"));
        $elemento = $this->getElement('nu_anolancamento');
        $elemento->addMultiOption('', 'Selecione');
        for ($i = $anoAtual; $i >= 1990; $i--) {
            $elemento->addMultiOption($i, $i);
        }
//        $elemento->setValue($anoAtual);
    }

}

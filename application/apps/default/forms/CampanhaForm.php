<?php

/**
 *
 * @since 2014-19-02
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @package default
 * @subpackage form
 */
use \G2\Negocio\CampanhaComercial as Negocio;

class CampanhaForm extends Zend_Form
{

    /**
     * @var type
     */
    private $negocio;

    public function init ()
    {

        $this->negocio = new Negocio();

        $id_campanhacomercial = new Zend_Form_Element_Hidden('id');
        $this->addElement($id_campanhacomercial);

        $st_campanhacomercial = new Zend_Form_Element_Text('st_campanhacomercial');
        $st_campanhacomercial->setAttribs(array(
                    'maxlength' => 30,
                    'required' => 'required',
                    'title' => 'Título',
                    'class' => 'span12'
                ))
                ->setRequired(true)
                ->addErrorMessage('O campo Título é obrigatório.')
                ->addValidators(array(
                    array(
                        'validator' => 'NotEmpty'
                    ),
                    array(
                        'validator' => 'stringLength',
                        'options' => array(6, 30)
                    )
        ));
        $this->addElement($st_campanhacomercial);

        $bl_aplicardesconto = new Zend_Form_Element_Checkbox('bl_aplicardesconto');
        $bl_aplicardesconto->setAttrib('title', 'Aplicar automaticamente');
        $this->addElement($bl_aplicardesconto);


        $bl_disponibilidade = new Zend_Form_Element_Select('bl_disponibilidade');
        $bl_disponibilidade->setAttribs(array(
                    'required' => 'required',
                    'title' => 'Disponibilidade',
                    'class' => 'span12'
                ))->setRequired(true)
                ->addErrorMessage('O campo Disponibilidade é obrigatório.')
                ->addMultiOptions(array(
                    '' => 'Selecione',
                    '0' => 'Indefinida',
                    '1' => 'Definida',
        ));
        $this->addElement($bl_disponibilidade);

        $dt_fim = new Zend_Form_Element_Text('dt_fim');
        $dt_fim->setAttribs(array(
            'required' => 'required',
            'maxlength' => 10,
            'class' => 'maskDate datepicker span8'
        ));
        $this->addElement($dt_fim);

        $dt_inicio = new Zend_Form_Element_Text('dt_inicio');
        $dt_inicio->setAttribs(array(
                    'required' => 'required',
                    'maxlength' => 10,
                    'class' => 'maskDate datepicker span8'
                ))
                ->setRequired(true)
                ->addErrorMessage('O campo Data Início é obrigatório.')
                ->addValidators(array(
                    array(
                        'validator' => 'NotEmpty'
                    )
        ));
        $this->addElement($dt_inicio);

        $id_situacao = new Zend_Form_Element_Select('id_situacao');
        $id_situacao->setAttribs(array(
                    'class' => 'span12',
                    'title' => 'Situação',
                    'required' => 'required'
                ))->setRequired(true)
                ->addErrorMessage('O campo Situação é obrigatório.');
        $this->addElement($id_situacao);
        $this->populaSituacao();

        $id_tipodesconto = new Zend_Form_Element_Select('id_tipodesconto');
        $id_tipodesconto->setAttribs(array(
                    'class' => 'span9',
                    'title' => 'Tipo de Desconto',
//                    'required' => 'required'
                ))->setRequired(true)
                ->addErrorMessage('O campo Tipo de Desconto é obrigatório.');
        $this->addElement($id_tipodesconto);
        $this->populaTipoDesconto();

        $id_tipocampanha = new Zend_Form_Element_Select('id_tipocampanha');
        $id_tipocampanha->setAttribs(array(
            'required' => 'required',
            'class' => 'span12',
            'title' => 'Tipo de Campanha'
        ));
        $this->addElement($id_tipocampanha);
        $this->populaTipoCampanha();

        $id_finalidadecampanha = new Zend_Form_Element_Select('id_finalidadecampanha');
        $id_finalidadecampanha->setAttribs(array(
                                'title'     => 'Finalidade',
                                'class'     => 'span12',
                                'required'  => 'required'))
                              ->addErrorMessage('O campo Finalidade é obrigatório!');

        $this->addElement($id_finalidadecampanha);
        $this->populaFinalidadeCampanha();

        $nu_disponibilidade = new Zend_Form_Element_Text('nu_disponibilidade');
        $nu_disponibilidade->setAttribs(array(
            'class' => 'span12',
            'title' => 'Num Disponibilidade'
        ));
        $this->addElement($nu_disponibilidade);


        $st_descricao = new Zend_Form_Element_Textarea('st_descricao');
        $st_descricao->setAttribs(array(
                    'class' => 'span12',
                    'required' => 'required',
                    'rows' => 2,
                    'title' => 'Descrição',
                    'maxlength' => 1000,
                ))
                ->setRequired(true)
                ->addErrorMessage('O campo Descrição é obrigatório.')
                ->addValidators(array(
                    array(
                        'validator' => 'NotEmpty'
                    ),
                    array(
                        'validator' => 'stringLength',
                        'options' => array(6, 1000)
                    )
        ));
        $this->addElement($st_descricao);

        $bl_todosprodutos = new Zend_Form_Element_Checkbox('bl_todosprodutos');
        $bl_todosprodutos->setOptions(array(
            'title' => 'Todos os Produtos'
        ));
        $this->addElement($bl_todosprodutos);

        $bl_todasformas = new Zend_Form_Element_Checkbox('bl_todasformas');
        $bl_todasformas->setOptions(array(
            'title' => 'Todas as Formas de Pagamento'
        ));
        $this->addElement($bl_todasformas);

        $id_categoriacampanha = new Zend_Form_Element_Select('id_categoriacampanha');
        $id_categoriacampanha->setAttribs(array(
            'required' => 'required',
            'class' => 'span12',
            'title' => 'Categoria'
        ));
        $this->addElement($id_categoriacampanha);
        $this->populaCategoriaCampanha();

        $nu_valormin = new Zend_Form_Element_Text('nu_valormin');
        $nu_valormin->setAttribs(array(
            'title' => 'Valor mínimo',
//            'class' => 'span6'
        ));
        $this->addElement($nu_valormin);

        $nu_valormax = new Zend_Form_Element_Text('nu_valormax');
        $nu_valormax->setAttribs(array(
            'title' => 'Valor máximo',
//            'class' => 'span6'
        ));
        $this->addElement($nu_valormax);


        $nu_diasprazo = new Zend_Form_Element_Text('nu_diasprazo');
        $nu_diasprazo->setAttribs(array(
            'title' => 'Número de dias disponivel gratis',
            'class' => 'span2 numeric desabilitactrlv ',
            'min' => '1',
        ));
        $this->addElement($nu_diasprazo);

        //remove os decorators
        foreach ($this->getElements() as $element) {
            $element->removeDecorator('HtmlTag');
            $element->removeDecorator('DtDdWrapper');
            $element->removeDecorator('label');
        }
    }

    /**
     * Popula dados do select de situação
     */
    private function populaSituacao ()
    {
        $negocio = new \G2\Negocio\Situacao();
        $result = $negocio->retornaSituacaoFindBy(array('st_tabela' => 'tb_campanhacomercial'));
        $elemento = $this->getElement('id_situacao');
        if ($result->getType() == 'success') {
            $elemento->addMultiOption('', 'Selecione');
            foreach ($result->getMensagem() as $row) {
                $elemento->addMultiOption($row['id_situacao'], $row['st_situacao']);
            }
        }
    }

    /**
     * Popula dados do select de tipo de campanha
     */
    private function populaTipoCampanha ()
    {
        $tipos = $this->negocio->retornarTipoCampanha(array('bl_ativo' => true));
        $elemento = $this->getElement('id_tipocampanha');
        if ($tipos) {
            $elemento->addMultiOption('', 'Selecione');
            foreach ($tipos as $tipo) {
                $elemento->addMultiOption($tipo->getId_tipocampanha(), $tipo->getSt_tipocampanha());
            }
        }
    }

    /**
     * Pupla dados do select de categoria campanha
     */
    private function populaCategoriaCampanha ()
    {
        $categorias = $this->negocio->retornarCategoriaCampanha();
        $elemento = $this->getElement('id_categoriacampanha');
        if ($categorias) {
            $elemento->addMultiOption('', 'Selecione');
            foreach ($categorias as $categoria) {
                $elemento->addMultiOption($categoria->getId_categoriacampanha(), $categoria->getSt_categoriacampanha());
            }
        }
    }

    /**
     * Retorna tipo de desconto
     */
    private function populaTipoDesconto ()
    {
        $tipos = $this->negocio->retornarTipoDesconto();
        $elemento = $this->getElement('id_tipodesconto');
        if ($tipos) {
            $elemento->addMultiOption('', 'Selecione');
            foreach ($tipos as $tipo) {
                $elemento->addMultiOption($tipo->getId_tipodesconto(), $tipo->getSt_tipodesconto());
            }
        }
    }

    private function populaFinalidadeCampanha() {

        $negocio = new \G2\Negocio\FinalidadeCampanha();
        $finalidadesCampanha = $negocio->retornaFinalidadesCampanhaAtivas();

        $el = $this->getElement('id_finalidadecampanha');
        $el->addMultiOption('', 'Selecione');

        if (isset($finalidadesCampanha) && $finalidadesCampanha) {
            foreach ($finalidadesCampanha as $finalidadeCampanha) {
                $el->addMultiOption($finalidadeCampanha['id_finalidadecampanha'],
                                    $finalidadeCampanha['st_finalidadecampanha']);
            }
        }
    }

}

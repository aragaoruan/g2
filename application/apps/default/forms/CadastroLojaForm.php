<?php

/**
 * Formulario para cadastro de pessoa na loja
 */
class CadastroLojaForm extends Zend_Form
{

    private $_negocio;

    const erro = "Campo de preenchimento obrigatório!";

    public function init()
    {
        $this->_negocio = new G2\Negocio\Pessoa();

        $st_nome = new Zend_Form_Element_Text('st_nome');
        $st_nome->setAttribs(array(
            'required' => 'required',
            'class' => 'form-control',
            'placeholder' => 'Digite seu nome'
        ))
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->setRequired(true)
            ->addErrorMessage(self::erro);
        $this->addElement($st_nome);

        $st_sobrenome = new Zend_Form_Element_Text('st_sobrenome');
        $st_sobrenome->setAttribs(array(
            'required' => 'required',
            'class' => 'form-control',
            'placeholder' => 'Digite seu sobrenome'
        ))
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->setRequired(true)
            ->addErrorMessage(self::erro);
        $this->addElement($st_sobrenome);

        //CPF
        $st_cpf = new Zend_Form_Element_Text('st_cpf');
        $st_cpf->setAttribs(array(
            'required' => 'required',
            'class' => 'form-control',
            'placeholder' => 'Digite seu CPF',
            'title' => 'Somente números'
        ))
            ->setRequired(true)
            ->addErrorMessage('O campo CPF é obrigatório.');
        $this->addElement($st_cpf);

        //Email
        $st_email = new Zend_Form_Element_Text('st_email');
        $st_email->setAttribs(array(
            'class' => 'form-control',
            'required' => 'required',
            'placeholder' => 'Digite seu email'
        ))
            ->setRequired(true)
            ->addErrorMessage('O campo E-mail é obrigatório.');
        $this->addElement($st_email);


        //DDD
//        $nu_dddresidencial = new Zend_Form_Element_Text("nu_dddresidencial");
//        $nu_dddresidencial->setAttribs(array(
//            'class' => 'form-control',
//            'placeholder' => 'DDD',
//            'maxlength' => 2
//        ));
//        $this->addElement($nu_dddresidencial);

        //Telefone
//        $nu_telefoneresidencial = new Zend_Form_Element_Text("nu_telefoneresidencial");
//        $nu_telefoneresidencial->setAttribs(array(
//            'class' => 'form-control',
//            'placeholder' => 'Digite o nº telefone',
//            'maxlength' => 9
//        ));
//        $this->addElement($nu_telefoneresidencial);


        $nu_dddcelular = new Zend_Form_Element_Text("nu_dddcelular");
        $nu_dddcelular->setAttribs(array(
            'class' => 'form-control',
            'required' => 'required',
            'placeholder' => 'DDD',
            'maxlength' => 2
        ));
        $this->addElement($nu_dddcelular);


        $nu_telefonecelular = new Zend_Form_Element_Text("nu_telefonecelular");
        $nu_telefonecelular->setAttribs(array(
            'class' => 'form-control',
            'required' => 'required',
            'placeholder' => 'Digite o nº telefone',
            'maxlength' => 9
        ));
        $this->addElement($nu_telefonecelular);


        //CEP
//        $st_cep = new Zend_Form_Element_Text('st_cep');
//        $st_cep->setAttribs(array(
//            'required' => 'required',
//            'class' => 'form-control',
//            'placeholder' => 'Digite o CEP',
//            'maxlength' => 10
//        ))->addValidator('StringLength', array(10, 10))
//            ->setRequired(true)
//            ->addErrorMessage('O campo CEP é obrigatório.');
//        $this->addElement($st_cep);


        //St_endereco
//        $st_endereco = new Zend_Form_Element_Text('st_endereco');
//        $st_endereco->setAttribs(array(
//            'required' => 'required',
//            'class' => 'form-control',
//            'placeholder' => 'Digite seu endereço'
//        ))->setRequired(true)
//            ->addErrorMessage('O campo Endereço é obrigatório.');
//        $this->addElement($st_endereco);

        //Numero
//        $nu_numero = new Zend_Form_Element_Text('nu_numero');
//        $nu_numero->setAttribs(array(
//            'required' => 'required',
//            'class' => 'form-control',
//            'placeholder' => 'Número'
//        ))->setRequired(true)
//            ->addErrorMessage('O campo Número é obrigatório.');
//        $this->addElement($nu_numero);

//        $st_bairro = new Zend_Form_Element_Text('st_bairro');
//        $st_bairro->setAttribs(array(
//            'required' => 'required',
//            'class' => 'form-control',
//            'placeholder' => 'Bairro'
//        ))->setRequired(true)
//            ->addErrorMessage('O campo Bairro é obrigatório.');
//        $this->addElement($st_bairro);


        //UF
        $sg_uf = new Zend_Form_Element_Select('sg_uf');
        $sg_uf->addMultiOption('', 'Selecione')
            ->setAttrib('class', 'form-control')
            ->setAttrib('title', 'UF')
            ->setAttrib('required', 'required');
        $this->addElement($sg_uf);
        $this->populateSgUf();


        //Complemento
//        $st_complemento = new Zend_Form_Element_Text('st_complemento');
//        $st_complemento->setAttribs(array(
//            'class' => 'form-control',
//            'placeholder' => 'Complemento'
//        ));
//        $this->addElement($st_complemento);

        //Municipio
        $id_municipio = new Zend_Form_Element_Select('id_municipio');
        $id_municipio->addMultiOption(null, 'Selecione o UF')
            ->setAttribs(array(
                'required' => 'required',
                'class' => 'form-control',
                'title' => 'Municipio'
            ))->setRequired(true)
            ->addErrorMessage('O campo Cidade é obrigatório.');
        $this->addElement($id_municipio);

        $id_email = new Zend_Form_Element_Hidden('id_email');
        $this->addElement($id_email);

//        $id_endereco = new Zend_Form_Element_Hidden('id_endereco');
//        $this->addElement($id_endereco);

        $id_telefonecelular = new Zend_Form_Element_Hidden('id_telefonecelular');
        $this->addElement($id_telefonecelular);

        $st_senha = new Zend_Form_Element_Password('st_senha');
        $st_senha->setAttribs(array(
            'class' => 'form-control',
            'maxlength' => 12,
            'required' => 'required',
            'placeholder' => 'Digite sua senha'
        ))
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('StringLength', array(6, 15))
            ->setRequired(true)
            ->addErrorMessage('O campo Senha é obrigatório.');
        $this->addElement($st_senha);

        $st_confirmarsenha = new Zend_Form_Element_Password('st_confirmarsenha');
        $st_confirmarsenha->setAttribs(array(
            'class' => 'form-control',
            'maxlength' => 15,
            'required' => 'required',
            'placeholder' => 'Confirme a senha'
        ))
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('StringLength', array(6, 15))
            ->setRequired(true)
            ->addErrorMessage('O campo Confirmar Senha é obrigatório.');
        $this->addElement($st_confirmarsenha);

        //remove os decorators
        foreach ($this->getElements() as $element) {
            $element->removeDecorator('HtmlTag');
            $element->removeDecorator('DtDdWrapper');
            $element->removeDecorator('label');
        }
    }

    /**
     *
     * @param type $field
     */
    private function populateSgUf()
    {
        $result = $this->_negocio->findAll('G2\Entity\Uf');
        $elemento = $this->getElement('sg_uf');
        if ($result) {
            foreach ($result as $row) {
                $elemento->addMultiOption($row->getSg_uf(), $row->getSt_uf());
            }
        }
    }

}

<?php

/**
 * Form gerador dos inputs dos formulários dinamicos
 * Débora Castro <debora.castro@unyleya.com>
 * @package forms
 */
use G2\Negocio\AreaConhecimentoSerieNivelEnsino;

class AreaConhecimentoSerieNivelEnsinoForm extends Zend_Form
{

    private $negocio;

    public function __construct (array $options = null)
    {

        $this->negocio = new AreaConhecimentoSerieNivelEnsino();

        $this->addElement('hidden', 'id', array('value' => ''));

        $id_nivelensino = new Zend_Form_Element_Select('id_nivelensino');
        $id_nivelensino->autoInsertNotEmptyValidator();
        $id_nivelensino->setAttribs(array(
                    'class' => 'span12',
                ));
        $this->addElement($id_nivelensino);


        //botão de submit

        $this->addElement('button', 'btnSbmit', array(
            'type'          => 'submit',
            'buttonType'    => 'success',
            'icon'          => 'ok',
            'escape'        => false
        ));


        //popula os selects
        $this->populateNivelEnsino();

        if (is_array($options)) {
            $this->populateForm($options);
        }

        //remove os decorators
        foreach ($this->getElements() as $element) {
            $element->removeDecorator('HtmlTag');
            $element->removeDecorator('DtDdWrapper');
            $element->removeDecorator('label');
        }
    }

    private function populateForm ($params)
    {
        $result = $this->negocio->findAreaConhecimento($params['id']);
        if ($result) {
            $this->getElement('id')->setValue($params['id']);
            $this->getElement('id_nivelensino')->setValue($result->getId_nivelensino());
        }
    }

    /**
     * Popula Select NivelEnsino
     * @param void
     * @return object Zend_Form_Element_Select
     */
    private function populateNivelEnsino()
    {
        $nivel = $this->getElement('id_nivelensino');
        $result = $this->negocio->findBy('G2\Entity\NivelEnsino', array('id_nivelensino' => $nivel));
        if ($result) {
            foreach ($result as $row) {
                $nivel->addMultiOption($row->getId_nivelensino(), $row->getSt_nivelensino());
            }
        }
    }

}
<?php

/**
 * Form para cadastro de locais de prova
 * @since 02-12-2013
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @package forms
 */
use G2\Constante\TipoPessoa;

class AplicadorProvaForm extends Zend_Form
{

    private $negocio;

    public function __construct(array $options = null)
    {

        $this->negocio = new \G2\Negocio\AplicadorProva();

        $this->addElement('hidden', 'id', array('value' => ''));

        $id_tipo_pessoa = new Zend_Form_Element_Select('id_tipopessoa', array('class' => 'span3'));
        $id_tipo_pessoa->addMultiOptions(
            array(0 => '--'
            , TipoPessoa::FISICA => TipoPessoa::getArrayTipoPessoa(1)
            , TipoPessoa::JURIDICA => TipoPessoa::getArrayTipoPessoa(2)
            ))
            ->setLabel('Tipo: ')
            ->setAttrib('required', 'required')
            ->setErrorMessageSeparator('<br>')
            ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($id_tipo_pessoa);


        $st_aplicadorprova = new Zend_Form_Element_Text('st_aplicadorprova', array('class' => 'span12'));
        $st_aplicadorprova->setLabel('Aplicador de Prova: ')
            ->setRequired(true)
            ->setAttrib('required', 'required')
            ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($st_aplicadorprova);


        //<input name="search" id="search" type="text" class="span12" placeholder="Digite o Nome, E-mail ou CPF" autocomplete="off">
        $search = new Zend_Form_Element_Text('search', array('class' => 'span12'
        , 'placeholder' => 'Digite o Nome, E-mail ou CPF'
        , 'autocomplete' => 'off'));
        $search->setLabel('Pessoa: ');
        $this->addElement($search);


        //<input name="searchEntidade" id="searchEntidade" type="text" class="span12" placeholder="Digite CNPJ ou Razão Social" autocomplete="off">
        $searchEntidade = new Zend_Form_Element_Text('searchEntidade', array('class' => 'span12'
        , 'placeholder' => 'Digite CNPJ ou Razão Social'
        , 'autocomplete' => 'off'));
        $searchEntidade->setLabel('Entidade: ');
        $this->addElement($searchEntidade);


        $id_usuario_aplicador = new Zend_Form_Element_Hidden('id_usuarioaplicador');
        $this->addElement($id_usuario_aplicador);


        $id_entidade_aplicador = new Zend_Form_Element_Hidden('id_entidadeaplicador');
        $this->addElement($id_entidade_aplicador);

        if (is_array($options) && $options['id'] != null) {
            $this->populateForm($options);
        }

        $element = $this->getElement('id');
        $element->removeDecorator('label');

        //remove os decorators
        foreach ($this->getElements() as $element) {
            $element->removeDecorator('HtmlTag');
            $element->removeDecorator('DtDdWrapper');

//            $element->removeDecorator('label');
        }
    }

    private function populateForm($params)
    {
        $result = $this->negocio->findVwAplicadorProva($params['id']);
        if ($result) {
            $this->getElement('id')->setValue($params['id']);
            $this->getElement('st_aplicadorprova')->setValue($result->getSt_aplicadorprova());
            $this->getElement('id_tipopessoa')->setValue($result->getId_tipopessoa());
            $this->getElement('id_usuarioaplicador')->setValue($result->getId_usuarioaplicador());
            $this->getElement('id_entidadeaplicador')->setValue($result->getId_entidadeaplicador());
            if ($result->getId_entidadeaplicador()) {
                $this->getElement('searchEntidade')->setValue($result->getSt_local());
            } else {
                $this->getElement('search')->setValue($result->getSt_local());
            }
        }
    }
}
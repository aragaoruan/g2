<?php

/**
 * Form gerador dos inputs dos formulários dinamicos
 * @since 19-12-2013
 * @author Débora Castro <debora.castro@unyleya.com.br>
 * @package forms
 */
use G2\Negocio\GerenciaProva;

class GerenciaProvaForm extends Zend_Form {

    private $negocio;

    public function __construct() {
        $this->negocio = new \G2\Negocio\GerenciaProva();

    }

}

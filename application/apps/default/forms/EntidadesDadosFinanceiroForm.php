<?php

/**
 * Form para Cadastro Rapido Pessoa
 * @since 2013-13-11
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @package forms
 */
use G2\Negocio\Pessoa;

class EntidadesDadosFinanceiroForm extends Zend_Form
{
    /** @var G2\Negocio\Pessoa $_negocio */
    private $_negocio;

    public function init()
    {
        $this->_negocio = new Pessoa();

        //ID Conta
        $id_conta = new Zend_Form_Element_Hidden('id_conta');
        $this->addElement($id_conta);

        //Tipo de conta
        $id_tipodeconta = new Zend_Form_Element_Select('id_tipodeconta');
        $id_tipodeconta->addMultiOption(null, 'Selecione um tipo de conta')
            ->setAttribs(array(
                'required' => 'required',
                'class' => 'span12'
            ))->setRequired(true)
            ->addErrorMessage('O campo tipo de conta é obrigatório.');
        $this->addElement($id_tipodeconta);

        //Tipo de banco
        $st_banco = new Zend_Form_Element_Select('st_banco');
        $st_banco->addMultiOption(null, 'Selecione um banco')
            ->setAttribs(array(
                'required' => 'required',
                'class' => 'span12'
            ))->setRequired(true)
            ->addErrorMessage('O campo Banco é obrigatório.');
        $this->addElement($st_banco);

        $id_cartaobandeira = new Zend_Form_Element_Select('id_cartaobandeira');
        $id_cartaobandeira->addMultiOption(null, 'Selecione um tipo cartão')
            ->setAttribs(array(
                'required' => 'required',
                'disabled' => 'disabled'
            ))->setRequired(true)
            ->addErrorMessage('O campo tipo de cartão é obrigatório.');
        $this->addElement($id_cartaobandeira);

        $id_sistema = new Zend_Form_Element_Select('id_sistema');
        $id_sistema->addMultiOption(null, 'Selecione um sistema')
            ->setAttribs(array(
                'required' => 'required',
            ))->setRequired(true)
            ->addErrorMessage('O campo sistema é obrigatório.');
        $this->addElement($id_sistema);

        //Populate Inputs
        $this->populateTipoDeConta();
        $this->populateBanco();
        $this->populateTipoCartao();
        $this->populateSistema();

        //remove os decorators
        foreach ($this->getElements() as $element) {
            $element->removeDecorator('HtmlTag');
            $element->removeDecorator('DtDdWrapper');
            $element->removeDecorator('label');
        }
    }

    /**
     * Populate Select Tipo de conta
     */
    private function populateTipoDeConta()
    {
        $result = $this->_negocio->findAll('G2\Entity\TipoConta');
        $elemento = $this->getElement('id_tipodeconta');
        if ($result) {
            foreach ($result as $row) {
                $elemento->addMultiOption($row->getId_tipodeconta(), $row->getSt_tipodeconta());
            }
        }
    }

    /**
     * Populate Select of UF
     */
    private function populateBanco()
    {
        $result = $this->_negocio->findAll('G2\Entity\Banco');
        $elemento = $this->getElement('st_banco');
        if ($result) {
            foreach ($result as $row) {
                $elemento->addMultiOption($row->getSt_banco(), $row->getSt_banco() . ' - ' . $row->getSt_nomebanco());
            }
        }
    }

    /**
     * Populate Select of UF
     */
    private function populateTipoCartao()
    {
        $result = $this->_negocio->findAll('G2\Entity\CartaoBandeira');
        $elemento = $this->getElement('id_cartaobandeira');
        if ($result) {
            foreach ($result as $row) {
                $elemento->addMultiOption($row->getId_cartaobandeira(), $row->getSt_cartaobandeira());
            }
        }
    }

    private function populateSistema(){
        $result = $this->_negocio->findBy('G2\Entity\Sistema', array('id_sistema' => array(7, 10)), array('st_sistema' => 'ASC'));
        //
        $elemento = $this->getElement('id_sistema');
        if ($result) {
            foreach ($result as $row) {
                $elemento->addMultiOption($row->getId_sistema(), $row->getSt_sistema());
            }
        }
    }
}
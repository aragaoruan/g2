<?php

/**
 * Form gerador dos inputs dos formulários dinamicos
 * @since 07-10-2013
 * @author Débora Castro <debora.castro@unyleya.com.br>
 * @package forms
 */
use G2\Negocio\GerenciaSalas;

class GerenciaSalasForm extends Zend_Form {

    private $negocio;

    public function __construct() {
        $this->negocio = new \G2\Negocio\GerenciaSalas();

        $id_sistema = new Zend_Form_Element_Select('id_sistema');
        $id_sistema->addMultiOptions(array(\G2\Constante\Sistema::ACTOR => 'Actor', \G2\Constante\Sistema::MOODLE=> 'Moodle'));
        $this->addElement($id_sistema);

        $id_periodoletivo = new Zend_Form_Element_Select('id_periodoletivo');
        $id_periodoletivo->addMultiOption('', '--')
                ->setRequired(true)
                ->setAttrib('required', 'required')
                ->setErrorMessageSeparator('<br>')
                ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($id_periodoletivo);

        $id_modalidadesaladeaula = new Zend_Form_Element_Select('id_modalidadesaladeaula');
        $id_modalidadesaladeaula->addMultiOption('', '--')
                ->setRequired(true)
                ->setValue(\G2\Constante\ModalidadeSalaDeAula::DISTANCIA)
                ->setAttrib('required', 'required')
                ->setErrorMessageSeparator('<br>')
                ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($id_modalidadesaladeaula);

        $id_tiposaladeaula = new Zend_Form_Element_Select('id_tiposaladeaula');
        $id_tiposaladeaula->addMultiOption('', '--')
                ->setRequired(true)
                ->setAttrib('required', 'required')
                ->setErrorMessageSeparator('<br>')
                ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($id_tiposaladeaula);

        $nu_maxalunos = new Zend_Form_Element_Text('nu_maxalunos', array('class' => 'input-mini numeric'));
        $nu_maxalunos->setValue(999)
            ->setAttrib('maxlength', 4)
            ->setRequired(true)
            ->setAttrib('required', 'required')
            ->setErrorMessageSeparator('<br>')
            ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($nu_maxalunos);

        $id_situacao = new Zend_Form_Element_Select('id_situacao');
        $id_situacao->addMultiOption('', '--')
                ->setRequired(true)
                ->setValue(\G2\Constante\Situacao::TB_SALADEAULA_ATIVA)
                ->setAttrib('required', 'required')
                ->setErrorMessageSeparator('<br>')
                ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($id_situacao);

        $nu_diasaluno = new Zend_Form_Element_Text('nu_diasaluno',array('class' => 'input-mini numeric'));
        $nu_diasaluno->setAttrib('maxlength', 4)
            ->setRequired(true)
            ->setErrorMessageSeparator('<br>')
            ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($nu_diasaluno);

        $this->addElement('checkbox', 'bl_semdiasaluno', array(
            'class' => 'checkbox'
                )
        );

        $nu_diasencerramento = new Zend_Form_Element_Text('nu_diasencerramento', array('class' => 'input-mini numeric'));
        $nu_diasencerramento->setAttrib('maxlength', 4)
            ->setRequired(true)
            ->setErrorMessageSeparator('<br>')
            ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($nu_diasencerramento);

        $this->addElement('checkbox', 'bl_semencerramento', array(
            'class' => 'checkbox',
            'value' => ''
                )
        );
        $st_saladeaula = new Zend_Form_Element_Text('st_saladeaula');
        $st_saladeaula->setRequired(true)
                ->setAttrib('required', 'required')
                ->setAttrib('maxlength', 255)
                ->setErrorMessageSeparator('<br>')
                ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($st_saladeaula);

        //botão de disciplina
        $this->addElement('reset', 'btnDisciplina', array(
            'class' => 'btn btn-small',
            'name' => 'Disciplina',
            'id' => 'btnDisciplina'
        ));

        $dt_inicioinscricao = new Zend_Form_Element_Text('dt_inicioinscricao', array('class' => 'dateg maskDate dataInicio'));
        $dt_inicioinscricao->setRequired(true)
                ->setAttrib('required', 'required')
                ->setErrorMessageSeparator('<br>')
                ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($dt_inicioinscricao);

        $dt_fiminscricao = new Zend_Form_Element_Text('dt_fiminscricao', array('class' => 'dateg maskDate dataFim'));
        $dt_fiminscricao->setRequired(true)
                ->setAttrib('onchange', 'validarDatasInicioFim($(this).attr(\'name\'));')
                ->setErrorMessageSeparator('<br>')
                ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($dt_fiminscricao);

        $dt_abertura = new Zend_Form_Element_Text('dt_abertura', array('class' => 'dateg maskDate dataAbertura'));
        $dt_abertura->setRequired(true)
                ->setAttrib('required', 'required')
                ->setErrorMessageSeparator('<br>')
                ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($dt_abertura);

        $dt_encerramento = new Zend_Form_Element_Text('dt_encerramento', array('class' => 'dateg maskDate dataEncerramento'));
        $dt_encerramento->setRequired(true)
                ->setAttrib('onchange', 'validarDatasAberturaEncerra($(this).attr(\'name\'));', 'pattern=""')
                ->setErrorMessageSeparator('<br>')
                ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($dt_encerramento);

        $this->addElement('reset', 'btnProjeto', array(
            'class' => 'btn btn-small',
            'name' => 'Projeto',
            'id' => 'btnProjeto'
        ));

        $id_avaliacaoconjunto = new Zend_Form_Element_Select('id_avaliacaoconjunto', array('class' => 'span2'));
        $id_avaliacaoconjunto->addMultiOption('', '--')
                ->setRequired(true)
                ->setAttrib('required', 'required')
                ->setErrorMessageSeparator('<br>')
                ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($id_avaliacaoconjunto);

        $id_usuario = new Zend_Form_Element_Select('id_usuario', array('class' => 'widthgerencia'));
        $id_usuario->addMultiOption('', '--');
        $this->addElement($id_usuario);

        $id_categoriasala = new Zend_Form_Element_Select('id_categoriasala');
        $id_categoriasala->addMultiOption('', '--')
                ->setRequired(true)
                ->setAttrib('required', 'required')
                ->setErrorMessageSeparator('<br>')
                ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($id_categoriasala);

        //popula os selects
        $this->populateSituacao();
        $this->populatePeriodo();
        $this->populateModalidade();
        $this->populateTipo();
        $this->populateAvaliacao();
        $this->populateProfessor();
        $this->populateCategoria();

        //remove os decorators
        foreach ($this->getElements() as $element) {
            $element->removeDecorator('HtmlTag');
            $element->removeDecorator('DtDdWrapper');
            $element->removeDecorator('label');
        }
    }

    /**
     * Popula Select situacao
     * @param void
     * @return object Zend_Form_Element_Select
     */
    private function populateSituacao() {
        $situacao = $this->getElement('id_situacao');
        $result = $this->negocio->findBy('G2\Entity\Situacao', array('st_tabela' => 'tb_saladeaula'));
        if ($result) {
            foreach ($result as $row) {
                $situacao->addMultiOption($row->getId_situacao(), $row->getSt_situacao());
            }
        }
    }

    /**
     * Popula Select Perido Letivo
     * @param void
     * @return object Zend_Form_Element_Select
     */
    private function populatePeriodo() {
        $periodo_letivo = $this->getElement('id_periodoletivo');
        $result = $this->negocio->findByPeriodo();
        if ($result) {
            foreach ($result as $row) {
                $periodo_letivo->addMultiOption($row->getId_periodoletivo(), $row->getSt_periodoletivo());
            }
        }
    }

    /**
     * Popula Select Modalidade Sala de Aula
     * @param void
     * @return object Zend_Form_Element_Select
     */
    private function populateModalidade() {
        $modalidadesaladeaula = $this->getElement('id_modalidadesaladeaula');
        $result = $this->negocio->findByModalidade();
        if ($result) {
            foreach ($result as $row) {
                $modalidadesaladeaula->addMultiOption($row->getId_modalidadesaladeaula(), $row->getSt_modalidadesaladeaula());
            }
        }
    }

    /**
     * Popula Select Tipos de Sala
     * @param void
     * @return object Zend_Form_Element_Select
     */
    private function populateTipo() {
        $tiposala = $this->getElement('id_tiposaladeaula');
        $result = $this->negocio->findByTipoSala();
        if ($result) {
            foreach ($result as $row) {
                $tiposala->addMultiOption($row->getId_tiposaladeaula(), $row->getSt_tiposaladeaula());
            }
        }
    }

    private function populateAvaliacao() {
        $avaliacao = $this->getElement('id_avaliacaoconjunto');
        $result = $this->negocio->findByAvaliacao();
        if ($result) {
            foreach ($result as $row) {
                $avaliacao->addMultiOption($row->getId_avaliacaoconjunto(), $row->getSt_avaliacaoconjunto());
            }
        }
    }

    private function populateProfessor() {
        $professor = $this->getElement('id_usuario');
        $result = $this->negocio->findByProfessor();
        if ($result) {
            foreach ($result as $row) {
                $professor->addMultiOption($row->getId_usuario() . '#' . $row->getId_perfil(), $row->getSt_nomecompleto());
            }
        }
    }

    private function populateCategoria() {
        $periodo_letivo = $this->getElement('id_categoriasala');
        $result = $this->negocio->findAll('G2\Entity\CategoriaSala');
        if ($result) {
            foreach ($result as $row) {
                $periodo_letivo->addMultiOption($row->getId_categoriasala(), $row->getSt_categoriasala());
            }
        }
    }

}

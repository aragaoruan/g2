<?php

/**
 * Form gerador dos inputs dos formulários dinamicos
 * @since 15-07-2014
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @package forms
 */
class PesquisaProjetoForm extends Zend_Form
{

    private $negocio;

    public function __construct()
    {

        $this->negocio = new \G2\Negocio\Transferencia();

        $this->setDecorators(array(
            'ViewHelper',
            'Errors',
            array('Description', array('tag' => 'p', 'class' => 'description')),
            array('HtmlTag', array('class' => 'form-div')),
            array('Label', array('class' => 'form-label'))
        ));

        $this->addElement('hidden', 'id_projeto', array('value' => ''));

        $st_projetopedagogico = new Zend_Form_Element_Text('st_projetopedagogico', array(
            'class' => 'span12',
            'pattern' => '.{3,}',
            'placeholder' => 'Pesquise por pelo menos 3 caracteres do projeto pedagógico'
        ));
        $st_projetopedagogico->setAttrib('required', 'required');
        $this->addElement($st_projetopedagogico);

        //area
        $id_areaconhecimento = new Zend_Form_Element_Select('id_areaconhecimento', array('class' => 'span12'));
        $id_areaconhecimento->addMultiOption('', '--')
            ->setLabel('Área: ');
        $this->addElement($id_areaconhecimento);

        //NívelEnsino
        $id_nivelensino = new Zend_Form_Element_Select('id_nivelensino', array('class' => 'span12'));
        $id_nivelensino->addMultiOption('', '--')
            ->setLabel('Nível Ensino: ');
        $this->addElement($id_nivelensino);

        //Unidade
        $id_organizacao = new Zend_Form_Element_Select('id_entidade', array('class' => 'span12'));
        $id_organizacao->addMultiOption('', '--')
            ->setLabel('Unidade: ')
            ->setAttrib('required', 'required')
            ->setErrorMessageSeparator('<br>')
            ->addErrorMessage(G2\Constante\MensagemSistema::FORM_CAMPO_OBRIGATORIO);
        $this->addElement($id_organizacao);





        //popula os selects
        $this->populateOrganizacao();
        $this->populateNivelEnsino();
        $this->populateArea();

        //remove os decorators
        foreach ($this->getElements() as $element) {
            $element->removeDecorator('HtmlTag');
            $element->removeDecorator('DtDdWrapper');
        }
    }

    private function populateOrganizacao()
    {
        $organizacao = $this->getElement('id_entidade');
        $bo = new EntidadeBO();

        $result = $bo->retornaEntidadeRecursivo();
        if ($result->getTipo() == Ead1_IMensageiro::SUCESSO) {
            foreach ($result->getMensagem() as $row) {
                $organizacao->addMultiOption($row->getId_entidade(), $row->getSt_nomeentidade());
            }
        }
    }

    private function populateNivelEnsino()
    {
        $nivel = $this->getElement('id_nivelensino');

        $result = $this->negocio->findAll('\G2\Entity\NivelEnsino');
        if ($result) {
            foreach ($result as $row) {
                $nivel->addMultiOption($row->getId_nivelensino(), $row->getSt_nivelensino());
            }
        }
    }

    private function populateArea()
    {
        $area = $this->getElement('id_areaconhecimento');
        $result = $this->negocio->findByVwAreaConhecimento();
        if ($result) {
            foreach ($result as $row) {
                $area->addMultiOption($row->getId_areaconhecimento(), $row->getSt_areaconhecimento());
            }
        }
    }

}

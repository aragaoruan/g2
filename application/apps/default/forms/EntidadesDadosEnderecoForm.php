<?php

/**
 * Form para Cadastro Rapido Pessoa
 * @since 2013-13-11
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @package forms
 */
use G2\Negocio\Pessoa;

class EntidadesDadosEnderecoForm extends Zend_Form
{

    private $_negocio;

    public function init()
    {
        $this->_negocio = new Pessoa();

        //idEndereço
        $id_endereco = new Zend_Form_Element_Hidden('id_endereco');
        $this->addElement($id_endereco);

        //País
        $id_pais = new Zend_Form_Element_Select('id_pais');
        $id_pais->addMultiOption(null, 'Selecione')
            ->setAttrib('class', 'span12');
        $this->addElement($id_pais);

        //CEP
        $st_cep = new Zend_Form_Element_Text('st_cep');
        $st_cep->setAttribs(array(
            'required' => 'required',
            'style' => 'width:85px',
//                    'class' => 'span7'
        ))->addValidator('StringLength', array(10, 10))
            ->setRequired(true)
            ->addErrorMessage('O campo CEP é obrigatório.');
        $this->addElement($st_cep);

        //UF
        $sg_uf = new Zend_Form_Element_Select('sg_uf');
        $sg_uf->addMultiOption(null, 'Selecione')
            ->setAttrib('class', 'span12 select-item');
        $this->addElement($sg_uf);

        //Municipio
        $id_municipio = new Zend_Form_Element_Select('id_municipio');
        $id_municipio->addMultiOption(null, 'Selecione o UF')
            ->setAttribs(array(
                'required' => 'required',
                'class' => 'span8 limpar-no-cep'
            ))->setRequired(true)
            ->addErrorMessage('O campo Municipio é obrigatório.');
        $this->addElement($id_municipio);

        //St_endereco
        $st_endereco = new Zend_Form_Element_Text('st_endereco');
        $st_endereco->setAttribs(array(
            'required' => 'required',
            'class' => 'span12'
        ))->setRequired(true)
            ->addErrorMessage('O campo Endereço é obrigatório.');
        $this->addElement($st_endereco);

        //Tipo endereço
        $id_tipoendereco = new Zend_Form_Element_Select('id_tipoendereco');
        $id_tipoendereco->addMultiOption(null, 'Selecione')
            ->setAttribs(array(
                'required' => 'required',
                'class' => 'span12 select-item limpar-no-cep'
            ))->setRequired(true)
            ->addErrorMessage('O campo Tipo de Endereço é obrigatório.');
        $this->addElement($id_tipoendereco);

        //Cidade
        $st_cidade = new Zend_Form_Element_Text('st_cidade');
        $st_cidade->setAttribs(array(
            'required' => 'required',
            'class' => 'span12'
        ))->setRequired(true)
            ->addErrorMessage('O campo Cidade é obrigatório.');
        $this->addElement($st_cidade);

        //Bairro
        $st_bairro = new Zend_Form_Element_Text('st_bairro');
        $st_bairro->setAttribs(array(
            'required' => 'required',
            'class' => 'span12'
        ))->setRequired(true)
            ->addErrorMessage('O campo Bairro é obrigatório.');
        $this->addElement($st_bairro);

        //Numero
        $nu_numero = new Zend_Form_Element_Text('nu_numero');
        $nu_numero->setAttribs(array(
            'required' => 'required',
            'class' => 'span4'
        ))->setRequired(true)
            ->addErrorMessage('O campo Número é obrigatório.');
        $this->addElement($nu_numero);

        //Complemento
        $st_complemento = new Zend_Form_Element_Text('st_complemento');
        $st_complemento->setAttribs(array(
            'class' => 'span12'
        ));
        $this->addElement($st_complemento);

        //Populate Inputs
        $this->populateSgUf();
        $this->populateTipoEndereco();

        //remove os decorators
        foreach ($this->getElements() as $element) {
            $element->removeDecorator('HtmlTag');
            $element->removeDecorator('DtDdWrapper');
            $element->removeDecorator('label');
        }
    }

    /**
     * Populate Select of UF
     */
    private function populateSgUf()
    {
        $result = $this->_negocio->findAll('G2\Entity\Uf');
        $elemento = $this->getElement('sg_uf');
        if ($result) {
            foreach ($result as $row) {
                $elemento->addMultiOption($row->getSg_uf(), $row->getSt_uf());
            }
        }
    }

    public function populateIdMunicipio($sgUf = null)
    {
        $result = $this->_negocio->findBy('G2\Entity\Municipio', array('sg_uf' => $sgUf));
        $elemento = $this->getElement('id_municipio');
        if ($result) {
            foreach ($result as $row) {
                $elemento->addMultiOption($row->getId_municipio(), $row->getSt_nomemunicipio());
            }
        }
    }

    /**
     * Populate Select of Tipo Endereço
     */
    private function populateTipoEndereco()
    {
        $result = $this->_negocio->findBy('G2\Entity\TipoEndereco', array('id_categoriaendereco' => 4));
        $elemento = $this->getElement('id_tipoendereco');
        if ($result) {
            foreach ($result as $row) {
                $elemento->addMultiOption($row->getId_tipoendereco(), $row->getSt_tipoendereco());
            }
        }
    }

    /**
     * Populate Select of Tipo Telefone
     * @param string $elemento Name Element
     */
    private function populateTipoTelefone($elemento)
    {
        $result = $this->_negocio->findAll('G2\Entity\TipoTelefone');
        $elemento = $this->getElement($elemento);
        if ($result) {
            foreach ($result as $row) {
                $elemento->addMultiOption($row->getId_tipotelefone(), $row->getSt_tipotelefone());
            }
        }
    }
}
<?php

/**
 * Pesquisa de alunos aptos ao agendamento
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class PesquisaAlunoGerenciaProvaForm extends Zend_Form {


    public function init()
    {
        parent::init();

        $id_nom_email = new Zend_Form_Element_Text('st_str', array('class' => 'span5'));
        $id_nom_email->setLabel('Nome, E-mail, CPF:');
        $this->addElement($id_nom_email);

        $sg_uf = new Zend_Form_Element_Select('sg_uf', array('class' => 'span2'));
        $sg_uf->setLabel('UF:')
            ->addMultiOption('', 'Todos');
        $this->addElement($sg_uf);

        $id_aplicador = new Zend_Form_Element_Select('id_aplicadorprova', array('class' => 'span3'));
        $id_aplicador->setLabel('Local de Prova:')
                ->addMultiOption('', 'Todos');
        $this->addElement($id_aplicador);

        $dt_aplicacao = new Zend_Form_Element_Select('dt_aplicacao', array('class' => 'span2'));
        $dt_aplicacao->setLabel('Data da aplicação:');

        $this->addElement($dt_aplicacao);



        $this->populateAplicador();
        $this->populateUf();


        //remove os decorators
        foreach ($this->getElements() as $element) {
            $element->removeDecorator('HtmlTag');
            $element->removeDecorator('DtDdWrapper');
        }
    }

    public function populateAplicador()
    {
        $bo = new PesquisarBO();
        $aplicador = $bo->retornaLocaisProva();

        foreach ($aplicador as $value) {
            $this->getElement('id_aplicadorprova')->addMultiOption($value['id_value'], $value['st_text']);
        }
    }


    public function populateUf()
    {
        $bo = new PesquisarBO();
        $uf = $bo->retornarSelectUf();

        foreach ($uf as $value) {
            $this->getElement('sg_uf')->addMultiOption($value['valor'], $value['label']);
        }
    }

}

<?php

/**
 *
 * @since 2013-18-12
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @package forms
 */
class DisciplinaForm extends Zend_Form
{

    public function init ()
    {

        $id_disciplina = new Zend_Form_Element_Hidden('id');
        $this->addElement($id_disciplina);

        $st_disciplina = new Zend_Form_Element_Text('st_disciplina');
        $st_disciplina->setAttribs(array(
                    'required' => 'required',
                    'class' => 'span12'
                ))
                ->setRequired(true)
                ->addErrorMessage('O campo Título é obrigatório.');
        $this->addElement($st_disciplina);

        $st_descricao = new Zend_Form_Element_Textarea('st_descricao');
        $st_descricao->setAttribs(array(
            'class' => 'span12',
            'rows' => 2,
            'required' => 'required'
        ));
        $this->addElement($st_descricao);

        $st_ementacertificado = new Zend_Form_Element_Textarea('st_ementacertificado');
        $st_ementacertificado->setAttribs(array(
            'class' => 'span12',
            'rows' => 2
        ));
        $this->addElement($st_ementacertificado);

        $nu_identificador = new Zend_Form_Element_Text('nu_identificador');
        $nu_identificador->setAttribs(array(
            'class' => 'span12 numeric'
        ));
        $this->addElement($nu_identificador);

        $nu_cargahoraria = new Zend_Form_Element_Text('nu_cargahoraria');
        $nu_cargahoraria->setAttribs(array(
                    'required' => 'required',
                    'class' => 'span12 numeric',
                    'maxlength' => 4
                ))
                ->setRequired(true)
                ->addErrorMessage('O campo Carga Horária é obrigatório.');
        $this->addElement($nu_cargahoraria);

        $nu_creditos = new Zend_Form_Element_Text('nu_creditos');
        $nu_creditos->setAttribs(array(
            'class' => 'span12 numeric',
            'maxlength' => 5
        ));
        $this->addElement($nu_creditos);

        $nu_codigoparceiro = new Zend_Form_Element_Text('nu_codigoparceiro');
        $nu_codigoparceiro->setAttribs(array(
            'class' => 'span12 numeric'
        ));
        $this->addElement($nu_codigoparceiro);

        $bl_compartilhargrupo = new Zend_Form_Element_Select('bl_compartilhargrupo');
        $bl_compartilhargrupo->setAttribs(array(
                    'required' => 'required',
                    'class' => 'span12',
                ))
                ->setRequired(true)
                ->addErrorMessage('Você precisa informar se deseja Compartilhar com Grupo!')
                ->addMultiOptions(array(1 => 'Sim', 0 => 'Não'))
                ->setValue(1);
        $this->addElement($bl_compartilhargrupo);

        $bl_provamontada = new Zend_Form_Element_Select('bl_provamontada');
        $bl_provamontada->setAttribs(array(
                    'class' => 'span12',
                ))
                ->addMultiOptions(array(1 => 'Sim', 0 => 'Não'))
                ->setValue(1);
        $this->addElement($bl_provamontada);

        $bl_disciplinasemestre = new Zend_Form_Element_Select('bl_disciplinasemestre');
        $bl_disciplinasemestre->setAttribs(array(
            'class' => 'span12',
        ))
            ->addMultiOptions(array(1 => 'Sim', 0 => 'Não'))
            ->setValue(1);
        $this->addElement($bl_disciplinasemestre);

        $id_situacao = new Zend_Form_Element_Select('id_situacao');
        $id_situacao->setAttribs(array(
                    'class' => 'span12',
                    'required' => 'required',
                ))
                ->setRequired(true)
                ->addErrorMessage('Você precisa informar a Situação!');
        $this->addElement($id_situacao);

        $id_tipodisciplina = new Zend_Form_Element_Select('id_tipodisciplina');
        $id_tipodisciplina->setAttribs(array(
                    'class' => 'span12',
                    'required' => 'required',
                ))
                ->setRequired(true)
                ->addErrorMessage('Você precisa informar o Tipo de Disciplina!');
        $this->addElement($id_tipodisciplina);

        $st_tituloexibicao = new Zend_Form_Element_Text('st_tituloexibicao');
        $st_tituloexibicao->setAttribs(array(
            'class' => 'span12'
        ));
        $this->addElement($st_tituloexibicao);

        $nu_repeticao = new Zend_Form_Element_Text("nu_repeticao");
        $nu_repeticao->setAttribs(array(
                    'class' => 'span2 numeric',
                    'maxlength' => 1,
                    'max' => 3,
                    'min' => 1
                ))
                ->setValue(1);
        $this->addElement($nu_repeticao);


        $sg_uf = new Zend_Form_Element_Select('sg_uf');
        $sg_uf->setAttribs(array(
                    'class' => 'span3',
                ))
                ->addMultiOptions(array(
                    '' => 'Selecione',
                    'DF' => 'DF',
                    'RJ' => 'RJ',
                    'SP' => 'SP',
        ));
        $this->addElement($sg_uf);

        $bl_coeficienterendimento = new Zend_Form_Element_Select('bl_coeficienterendimento');
        $bl_coeficienterendimento->setAttribs(array(
            'required' => 'required',
            'class' => 'span12',
        ))
            ->setRequired(true)
            ->addErrorMessage('É necessário informar se a disciplina será contabilizada no cálculo do coeficiente 
                de rendimento.')
            ->addMultiOptions(array(1 => 'Sim', 0 => 'Não'))
            ->setValue(1);
        $this->addElement($bl_coeficienterendimento);

        $bl_cargahorariaintegralizada = new Zend_Form_Element_Select('bl_cargahorariaintegralizada');
        $bl_cargahorariaintegralizada->setAttribs(array(
            'required' => 'required',
            'class' => 'span12',
        ))
            ->setRequired(true)
            ->addErrorMessage('É necessário informar se a disciplina será contabilizada na 
                carga horária integralizada.')
            ->addMultiOptions(array(1 => 'Sim', 0 => 'Não'))
            ->setValue(1);
        $this->addElement($bl_cargahorariaintegralizada);

        //popula situação
        $this->populaSituacao();
        //popula Tipo de Situação
        $this->populaTipoDisciplina();

        //remove os decorators
        foreach ($this->getElements() as $element) {
            $element->removeDecorator('HtmlTag');
            $element->removeDecorator('DtDdWrapper');
            $element->removeDecorator('label');
        }
    }

    /**
     * Popula dados do select de situação
     */
    private function populaSituacao ()
    {
        $negocio = new \G2\Negocio\Situacao();
        $result = $negocio->retornaSituacaoFindBy(array('st_tabela' => 'tb_disciplina'));
        $elemento = $this->getElement('id_situacao');
        if ($result->getType() == 'success') {
            $elemento->addMultiOption('', 'Selecione');
            foreach ($result->getMensagem() as $row) {
                $elemento->addMultiOption($row['id_situacao'], $row['st_situacao']);
            }
        }
    }

    private function populaTipoDisciplina ()
    {
        $negocio = new \G2\Negocio\Disciplina();
        $result = $negocio->getTipoDisciplina();
        $elemento = $this->getElement('id_tipodisciplina');
        if ($result) {
            $elemento->addMultiOption('', 'Selecione');
            foreach ($result as $row) {
                $elemento->addMultiOption($row->getId_tipodisciplina(), $row->getSt_tipodisciplina());
            }
        }
    }

}

<?php


/**
 *
 * @author actor3
 *        
 *        
 */

class Default_MinasAulaController extends Ead1_ControllerRestrita {

		public function  indexAction(){
			
		}
		
		public function insertAction(){
			$bo = new MinasAulaBO();
			$retorno = $bo->inserir(new MinasAulaTO($this->_getAllParams()));
			Zend_Debug::dump($retorno);
			
			exit;
		}
		public function updateAction(){
			$bo = new MinasAulaBO();
			$to = new MinasAulaTO($this->_getAllParams());
			$validador = new Zend_Validate(new Zend_Validate_Int(), new Zend_Validate_NotEmpty());
			if($validador->isValid($to->getId_minasaula())){
				$to->fetch(true, true, true);
				$this->view->dados = new Ead1_Mensageiro($to);
			}
// 			$retorno = $bo->atualizar(new MinasAulaTO($this->_getAllParams()));
		}
		public function deleteAction(){
			$bo = new MinasAulaBO();
			$retorno = $bo->deletar(new MinasAulaTO($this->_getAllParams()));
			if($retorno->getTipo() == Ead1_IMensageiro::SUCESSO){
				$this->_redirect('/minasaula/consulta');
			}
			die;
		}
		public function listarAction(){
			$bo = new MinasAulaBO();
			$this->view->mensageiro = $bo->listar(new MinasAulaTO($this->_getAllParams()));
		}
		
		public function consultaAction(){
			$to = new MinasAulaTO();
			$this->view->mensageiro = new Ead1_Mensageiro($to->fetch());
		}
		public function atualizarAction(){
			$bo  = new MinasAulaBO();
			$validador = new Zend_Validate();
			$validador->addValidator(new Zend_Validate_Int())->addValidator(new Zend_Validate_NotEmpty());
			if($validador->isValid($this->_getParam('id_minasaula')))
			{
				$mensageiro = $bo->atualizar(new MinasAulaTO($this->_getAllParams()));
				if($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO){
					$this->_redirect("/minasaula/consulta");
				}
			}else{
				echo "Erro";
				die;
			}
// 			Zend_Debug::dump($this->_getAllParams());
		}
		public function alienAction(){
			
			$objeto = new MinasAulaTO();
			Zend_Debug::dump($objeto);
			Zend_Debug::dump(clone $objeto);
			die;
		}
}

?>
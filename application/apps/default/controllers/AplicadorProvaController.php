<?php

/**
 * Classe de controller MVC para a configuração do aplicador de prova
 * @author Denise Xavier <denise.xavoer@unyleya.com.br>
 * @package application
 * @subpackage controller
 */
class Default_AplicadorProvaController extends Ead1_ControllerRestrita
{

    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\AplicadorProva();
    }

    public function indexAction()
    {

    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('aplicador-prova/index.js');
    }

    public function getAction()
    {
        $this->getResponse()
            ->appendBody("From getAction() returning the requested article");
    }

    public function postAction()
    {
        $this->getResponse()
            ->appendBody("From postAction() creating the requested article");
    }

    public function putAction()
    {
        $this->getResponse()
            ->appendBody("From putAction() updating the requested article");
    }

    public function recarregaArvoreAction()
    {
        if ($this->_getParam('id_aplicadorprova')):
            $result = array();
            $this->_helper->viewRenderer->setNoRender(true);
            $negocio = new G2\Negocio\AplicadorProva();
            $retorno = $negocio->findBy('G2\Entity\AplicadorProvaEntidade', array('id_aplicadorprova' => $this->_getParam('id_aplicadorprova')));
            if (!empty($retorno)) {
                foreach ($retorno as $entidade) {
                    $result[] = /*array('id_entidade' => */
                        $entidade->getId_entidade()->getId_entidade();
                }
            }
            echo Zend_Json_Encoder::encode($result);
        endif;

    }

    public function retornaEnderecoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $negocio = new G2\Negocio\AplicadorProva();
        try {
            $id_usuario = $this->getParam('id_usuarioaplicador', false);
            if($id_usuario):
                $find = array('id_usuario' => $id_usuario, 'id_entidade' => $negocio->sessao->id_entidade
                                ,'id_tipoendereco' => 6);
                $result = $negocio->findVwPessoaEnderecoAplicador($find);
            else:
                $entity = new \G2\Entity\VwPessoaEndereco();
                $result =  $negocio->toArrayEntity($entity);
            endif;
            $this->getResponse()->appendBody(json_encode($result));
        } catch (Exception $e) {
            throw $e;
        }

    }
}

<?php

/**
 * Classe de controller MVC para controlar Enc. Financeiro
 * @author Rafael Rocha <rafael.rocha.mg@gmail.com>
 * 
 * @package application
 * @subpackage controller
 */
class Default_EncerramentoFinanceiroController extends Ead1_ControllerRestrita {

    private $_bo;
    private $_secretariaBO;

    public function init() {
        parent::init();
        parent::setupDoctrine();
        $this->_bo = new MatriculaBO();
        $this->_secretariaBO = new SecretariaBO();
    }

    public function jsAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('encerramento-financeiro/index.js');
    }

    public function indexAction() {
        
    }

    public function salvarAction() {
        $tramiteTO = new TramiteTO();
        $tramiteTO->setBl_visivel(1);
        $tramiteTO->setDt_cadastro(new Zend_Date());
        $tramiteTO->setId_entidade($tramiteTO->getSessao()->id_entidade);
        //$tramiteTO->setId_tipotramite(9); // Matricula manual
        $tramiteTO->setId_usuario($tramiteTO->getSessao()->id_usuario);
        $tramiteTO->setSt_tramite($this->getParam('st_tramite'));

        $tramiteBO = new TramiteBO();
        $mensageiro = $tramiteBO->cadastrarTramite($tramiteTO, CategoriaTramiteTO::MATRICULA, $this->getParam('id_matricula'));

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $this->_helper->json(array(
                'type' => Ead1_IMensageiro::TYPE_SUCESSO,
                'title' => Ead1_IMensageiro::TITLE_SUCESSO,
                'text' => 'Tramite cadastrado com sucesso.',
            ));
        } else {
            $this->_helper->json(array(
                'type' => Ead1_IMensageiro::TYPE_ERRO,
                'title' => Ead1_IMensageiro::TITLE_ERRO,
                'text' => $mensageiro->getCodigo()
            ));
        }
    }

}

<?php

/**
 * Controller para gerenciar Campanha
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-02-18
 * @package default
 * @subpackage controllers
 */
class Default_CampanhaComercialController extends Ead1_ControllerRestrita
{

    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new G2\Negocio\CampanhaComercial();
    }

    public function indexAction()
    {
        $form = new CampanhaForm();
        $this->view->formCupom = new CupomForm();
        $id = $this->_getParam('id');
        if ($id) {
            $result = $this->negocio->findCampanha($id);
            if ($result) {
                $dtFim = new DateTime($result->getDt_fim());
                $dtInicio = new DateTime($result->getDt_inicio());
                $arrResult = array(
                    'id' => $result->getId_campanhacomercial(),
                    'id_campanhacomercial' => $result->getId_campanhacomercial(),
                    'st_campanhacomercial' => $result->getSt_campanhacomercial(),
                    'bl_aplicardesconto' => $result->getBl_aplicardesconto(),
                    'bl_ativo' => $result->getBl_ativo(),
                    'bl_disponibilidade' => $result->getBl_disponibilidade() ? 1 : 0,
                    'dt_cadastro' => $result->getDt_cadastro(),
                    'dt_fim' => $result->getDt_fim() ? $dtFim->format('d/m/Y H:i') : NULL,
                    'dt_inicio' => $result->getDt_inicio() ? $dtInicio->format('d/m/Y H:i') : NULL,
                    'nu_disponibilidade' => $result->getNu_disponibilidade(),
                    'st_descricao' => $result->getSt_descricao(),
                    'bl_todosprodutos' => $result->getBl_todosprodutos(),
                    'bl_todasformas' => $result->getBl_todasformas(),
                    'id_entidade' => $result->getId_entidade() ? $result->getId_entidade()->getId_entidade() : NULL,
                    'id_situacao' => $result->getId_situacao() ? $result->getId_situacao()->getId_situacao() : NULL,
                    'id_tipodesconto' => $result->getId_tipodesconto() ? $result->getId_tipodesconto()->getId_tipodesconto() : NULL,
                    'id_usuariocadastro' => $result->getId_usuariocadastro() ? $result->getId_usuariocadastro()->getId_usuario() : NULL,
                    'id_tipocampanha' => $result->getId_tipocampanha() ? $result->getId_tipocampanha()->getId_tipocampanha() : NULL,
                    'id_categoriacampanha' => $result->getId_categoriacampanha() ? $result->getId_categoriacampanha()->getId_categoriacampanha() : NULL,
                    'nu_valordesconto' => $result->getNu_valordesconto(),
                    'nu_diasprazo' => $result->getNu_diasprazo(),
                    'id_premio' => $result->getId_premio() ? $result->getId_premio()->getId_premio() : null,
                    'id_finalidadecampanha' => $result->getId_finalidadecampanha() ? $result->getId_finalidadecampanha()->getId_finalidadecampanha() : NULL,
                    'st_link' => $result->getSt_link() ? $result->getSt_link() : NULL,
                    'bl_mobile' => $result->getBl_mobile() ? $result->getBl_mobile() : NULL,
                    'bl_portaldoaluno' => $result->getBl_portaldoaluno() ? $result->getBl_portaldoaluno() : NULL,
                    'st_imagem' => $result->getSt_imagem() ? $result->getSt_imagem() : NULL
                );
                $form->populate($arrResult);
                $this->view->dadosCampanha = $arrResult;
            }
        }
        $this->view->form = $form;
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
    }

    public function tabCupomAction()
    {
        $cupomForm = new CupomForm();
        $this->view->formCupom = $cupomForm;
    }

    public function salvaArrayFormasPagamentoAction()
    {
        $request = $this->getRequest();
        $arrResult = array();
        if ($request->isPost()) {
            $data = $request->getPost();
            $arrResult = $this->negocio->salvarArrayCampanhaFormaPagamento($data['data']);
        }
        $this->_helper->json($arrResult);
    }

    public function salvaArrayProdutosAction()
    {
        $request = $this->getRequest();
        $arrResult = array();
        if ($request->isPost()) {
            $data = $request->getPost();
            $arrResult = $this->negocio->salvarArrayCampanhaProdutos($data['data']);
        }
        $this->_helper->json($arrResult);
    }

    public function salvaCampanhaCategoriaAction()
    {
        $request = $this->getRequest();
        $arrResult = array();
        if ($request->isPost()) {
            $data = $request->getPost();
            $arrResult = $this->negocio->salvarArrayCampanhaCategoria($data['data']);
        }
        $this->_helper->json($arrResult);
    }

    public function retornarCuponsAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = array();
        if ($params) {
            $result = $this->negocio->retornarDadosListaTabCupom($params);
        }
        $this->_helper->json($result);
    }

    /**
     * Action para gerar relatório de cupons vinculados a uma campanha
     * @throws Zend_Exception
     * @throws Exception
     */
    public function xlsCupomAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        try {
            $idCampanha = $this->getParam('idCampanha'); //Recebe o parametro
            //Monta o array do paramentro para consultar os cupons
            $resultCupons = $this->negocio->retornarDadosListaTabCupom(array('id_campanhacomercial' => $idCampanha));
            $xls = new Ead1_GeradorXLS(); //instancia o xls
            $sessao = new Zend_Session_Namespace('geral'); //pega os dados da sessão
            $loginBO = new LoginBO(); //instancia a bo de login
            //recupera os dados do usuario logado
            $mensageiro = $loginBO->retornaDadosUsuario($sessao->id_usuario, $sessao->id_entidade, $sessao->id_perfil);
            //verifica se o usuario esta logando
            if ($mensageiro->getCodigo() == Ead1_IMensageiro::AVISO) {
                throw new Exception('Erro ao gerar o documento. Tente novamente por favor');
            }
            //recupera os dados retornados
            $usuarioTO = $mensageiro->getFirstMensagem();
            //verifica se existe cupom
            if (!$resultCupons) {
                throw new Exception('Erro ao gerar o documento. Não há cupons vinculados!');
            } else { //se existir cupom
                //recupera o array com o cabeçalho
                $arXlsHeaderTO = $this->getCabecalhosXls();
                $dados = array(); //cria uma variavel para armazenar os dados
                foreach ($resultCupons as $cupom) { // percorre os dados e trata as informações
                    $dados[] = array(
                        'id_cupom' => $cupom['id'],
                        'st_codigocupom' => $cupom['st_codigocupom'],
                        'nu_desconto' => $cupom['nu_desconto'],
                        'st_tipodesconto' => $cupom['st_tipodesconto'],
                        'dt_inicio' => $cupom['dt_inicio'],
                        'dt_fim' => $cupom['dt_fim'],
                        'st_campanhacomercial' => $cupom['st_campanhacomercial'],
                        'st_ativacao' => $cupom['bl_ativacao'] ? 'Sim' : 'Não',
                        'st_unico' => $cupom['bl_unico'] ? 'Sim' : 'Não'
                    );
                }
                //encapsula os dados
                $arTO = Ead1_TO_Dinamico::encapsularTo($dados, new ParametroRelatorioTO(), false, false, true);
                //instancia o xls configuration to
                $x = new XLSConfigurationTO();
                //seta os atributos
                $x->setTitle('Relatórios de Cupons - Campanha Comercial: ' . $resultCupons[0]['st_campanhacomercial']);
                $x->setFooter('Gerado por: ' . $usuarioTO->st_nomecompleto . ' - ' . $usuarioTO->st_cpf . ' Data: ' . date('d/m/Y H:i:s'));
                $x->setFilename('relatorio-cupom-' . str_replace(' ', '-', $resultCupons[0]['st_campanhacomercial']));
                //gera o xls
                $xls->geraXLS($arXlsHeaderTO, $arTO, $x);
            }
        } catch (Exception $ex) {
            throw new Zend_Exception($ex->getMessage());
        }
    }

    /**
     * Monta o cabeçalho do xls
     * @return \XLSHeaderTO
     */
    private function getCabecalhosXls()
    {
        $arrCabecalho = array(
            'id_cupom' => 'Id Cupom',
            'st_codigocupom' => 'Código do Cupom',
            'nu_desconto' => 'Desconto',
            'st_tipodesconto' => 'Tipo de Desconto',
            'dt_inicio' => 'Inicio',
            'dt_fim' => 'Término',
            'st_campanhacomercial' => 'Campanha',
            'st_ativacao' => 'Ativo',
            'st_unico' => 'Usar uma vez'
        );

        $arrRetorno = array();
        foreach ($arrCabecalho as $key => $value) {
            $xlsHeaderTO = new XLSHeaderTO();
            $xlsHeaderTO->setSt_header($value);
            $xlsHeaderTO->setSt_par($key);
            $arrRetorno[] = $xlsHeaderTO;
        }
        return $arrRetorno;
    }

    public function deletarArrayFormaPagamentoAction()
    {

        $params = $this->getAllParams();

        if (isset($params["data"])) {

            unset($params['controller'], $params['action'], $params['module']);

            $params = $params['data'];

            try {

                foreach ($params as $param) {

                    $delete = $this->negocio->deletaCampanhaFormaPagamento($param['id_campanhacomercial'], $param['id_formapagamento']);

                    if ($delete == false) {
                        continue;
                    }
                }

                $return = true;

            } catch (Exception $e) {

                $return = false;
            }

            if ($return != false) {
                $this->_helper->json(array(
                    'type' => 'success',
                    'title' => 'Ação bem sucedida!',
                    'text' => 'Forma de pagamento inserida com sucesso.'
                ));
            }

        } else {
            $this->_helper->json(array(
                'type' => 'warning',
                'title' => 'Alerta!',
                'text' => 'Nada foi removido.'
            ));
        }

    }

    public function verificaCampanhasPontualidadeAction()
    {
        $id_campanha = $this->getParam('id_campanha');
        $result = $this->negocio->retornarCampanhaPontualidadePadrao(array(
            'bl_aplicardesconto' => true,
            'id_entidade' => $this->negocio->getId_entidade(),
            'bl_ativo' => true
        ));
        $arrReturn = array();
        if ($result) {
            if ($id_campanha != $result->getId_campanhacomercial()) {
                $arrReturn = array(
                    'id_campanha' => $result->getId_campanhacomercial(),
                    'st_campanha' => $result->getSt_campanhacomercial()
                );
            }

        }
        $this->_helper->json($arrReturn);
    }


    public function retornaCuponsAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $params['dt_atual'] = date('Y-m-d H:m:s');
        $retorno = $this->negocio->retornaCampanhasCupom($params);
        $this->_helper->json($retorno);


    }

    public function retornaPremioAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $retorno = $this->negocio->retornaPremio($params);
        $this->_helper->json($retorno);
    }

    public function salvaCampanhaPremioAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $retorno = $this->negocio->salvaPremio($params['data']);
        $retorno = $this->negocio->toArrayEntity($retorno);
        $this->_helper->json($retorno);

    }

    public function retornaProdutoPremioSelecionadoAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $retorno = $this->negocio->retornaProdutoPremioSelecionado($params);
        $this->_helper->json($retorno);
    }

    public function pesquisarProdutoAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $retorno = $this->negocio->retornaProdutoLike($params);
        $this->_helper->json($retorno);
    }
}

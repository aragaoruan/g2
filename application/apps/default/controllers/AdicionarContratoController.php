<?php

class Default_AdicionarContratoController extends Ead1_ControllerRestrita
{


    /**
     * @var \G2\Negocio\Matricula
     */
    private $matricula;

    public function init()
    {
        parent::init();
        parent::setupDoctrine();
        $this->matricula = new \G2\Negocio\Matricula();
    }

    public function indexAction()
    {
        $this->view->situacaoAtiva = \G2\Constante\Situacao::TB_MINHAPASTA_ATIVO;
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('adicionar-contrato/index.js');
    }

}


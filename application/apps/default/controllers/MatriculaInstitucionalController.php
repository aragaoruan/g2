<?php

class Default_MatriculaInstitucionalController extends Ead1_ControllerRestrita
{

    private $_bo;
    private $_secretariaBO;

    public function init()
    {
        parent::init();
        parent::setupDoctrine();
        $this->_bo = new MatriculaBO();
        $this->_secretariaBO = new SecretariaBO();
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('matricula-institucional/index.js');
    }

    public function indexAction()
    {
    }

    public function projetosPedagogicosAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $negocio = new \G2\Negocio\ProjetoPedagogico();
        $return = $negocio->findProjetoEntidade(['id_entidade' => $negocio->sessao->id_entidade , 'bl_ativo' => true]);
        $result = array();
        $order = array();
        foreach ($return->getMensagem() as $key => $value) {
            if( $value->getId_projetopedagogico()->getId_situacao()->getId_situacao() == \G2\Constante\Situacao::TB_PROJETOPEDAGOGICO_ATIVO){
                $result[$key]['id_projetopedagogico'] = $value->getId_projetopedagogico()->getId_projetopedagogico();
                $result[$key]['st_projetopedagogico'] = $value->getId_projetopedagogico()->getSt_projetopedagogico();
                $order[$key] = $value->getId_projetopedagogico()->getSt_projetopedagogico();
            }
        }
        array_values($result);
        array_multisort($order, SORT_ASC, $result);
        echo json_encode($result);
    }

    public function findTurmasDisponiveisByProjetoCompartilhadoAction(){
        $negocio = new \G2\Negocio\Matricula();
        $mensageiro = $negocio->retornaTurmasDisponiveisCompartilhadas(array('id_projetopedagogico' => $this->_getParam('id_projetopedagogico')));
        $arrReturn = array();
        if ($mensageiro && is_array($mensageiro)) {
            foreach ($mensageiro as $row) {
                $arrReturn[] = array(
                    'id_turmaprojeto' => $row['id_turmaprojeto'],
                    'id_turma' => $row['id_turma'],
                    'id_projetopedagogico' => $row['id_projetopedagogico'],
                    'st_turma' => $row['st_turma'],
                );
            }
        }
        $this->_helper->json($arrReturn);
    }

}


<?php

/**
 * Class AtendenteController
 * Controller para Atendente
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 * @since 2015-03-25
 * @package application
 * @subpackage controller
 */
class Default_AtendenteController extends Ead1_ControllerRestrita
{
    public function indexAction()
    {
        $id = $this->_getParam('id');
        if($id){
            $negocio = new \G2\Negocio\VwAtendente();
            $result = $negocio->findByAtendenteId($id);
            $this->view->dados = $result->getFirstMensagem();
//            Zend_Debug::dump($this->view->dados);exit;
        }
    }

    public function salvarAtendenteAction()
    {
        $dados = $this->getRequest()->getParams();
        $negocio = new \G2\Negocio\VwAtendente();
        $result = $negocio->salvarAtendente($dados);

        return $this->_helper->json($result);
    }

    public function pesquisaNucleoAction()
    {

        $this->_helper->viewRenderer->setNoRender(true);
        $nucleoTb = new \G2\Negocio\NucleoTm();
        $result = $nucleoTb->retornarNucleo();

        if ($result instanceof \Ead1_Mensageiro) {
            die('entrou');
        }
        $retorno = '';
        if ($result) {
            foreach ($result as $key => $linha) {
                $retorno .= '<li class="id_nucleotm" id="' . $linha->getId_nucleotm() . '">';
                $retorno .= '<a href="#" id="' . $linha->getId_nucleotm() . '">' . $linha->getSt_nucleotm() . '</a>';
                $retorno .= '</li>';
            }
        }
        echo $retorno;
    }

    public function pesquisaNucleoUsuarioAction(){

        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module'], $params['id']);

        $nucleoTb = new \G2\Negocio\NucleoTm();
        $result = $nucleoTb->retornarNucleo($params);

        if ($result instanceof \Ead1_Mensageiro) {
            die('');
        }
        $retorno = '';
        if ($result) {
            foreach ($result as $key => $linha) {
                $retorno .= '<li class="id_nucleotm" id="' . $linha->getId_nucleotm() . '">';
                $retorno .= '<a href="#" id="' . $linha->getId_nucleotm() . '">' . $linha->getSt_nucleotm() . '</a>';
                $retorno .= '</li>';
            }
        }
        echo $retorno;


       /* $negocio = new \G2\Negocio\VwAtendente();
        $result = $negocio->pegaNucleosUsuario($params);*/

        return $this->_helper->json($result);
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
    }
}
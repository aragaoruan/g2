<?php

use G2\Negocio\EntregaDocumentos;

/**
 * Controller para funcionalidade de Entrega de Documentos
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @since  15-10-2014
 * @package application
 * @subpackage controller
 */
class Default_EntregaDocumentosController extends Ead1_ControllerRestrita {

    /**
     * @var \G2\Negocio\EntregaDocumentos
     */
    private $negocio;

    public function init() {
        parent::init();
        $this->negocio = new \G2\Negocio\EntregaDocumentos();
    }

    public function indexAction() {
       // $this->_helper->layout()->disableLayout();

        echo $this->view->headScript()
            ->appendScript($this->view->render('/entrega-documentos/index.js'));
        echo $this->view->headScript()
            ->appendScript('/js/jquery.form.js');
    }

    public function jsAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('entrega-documentos/index.js');
    }

    public function salvarAction() {

        $result = $this->negocio->salvarEntregaDocumentos($this->getAllParams());
        $this->_helper->json($result);
    }


    public function salvarRevisaoAction() {

        $result = $this->negocio->salvarRevisao($this->getAllParams());
        $this->_helper->json($result);
    }

    public function salvarMarcacaoAcademicoAction(){
        $result = $this->negocio->salvarMarcacaoAcademico($this->getAllParams());
        $this->_helper->json($result);
    }

    public function retornaTramitesAction()
    {
        $tramiteBO = new TramiteBO();
        $mensageiro = $tramiteBO->retornarTramiteSp(CategoriaTramiteTO::MATRICULA
            , array(0 => $this->getParam('id_matricula'))
            , 16  ); //Trâmite de documentação
        $tramites = array();
        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            foreach ($mensageiro->getMensagem() as $tramite) {
                $tramite->setSt_tramite(nl2br($tramite->getSt_tramite()));
                $tramites[] = $tramite->toBackboneArray();
            }
        }
        $this->_helper->json($tramites);
    }

    public function getAction() {
        $this->getResponse()
                ->appendBody("From getAction() returning the requested article");
    }

    public function postAction() {
        $this->getResponse()
                ->appendBody("From postAction() creating the requested article");
    }

    public function putAction() {
        $this->getResponse()
                ->appendBody("From putAction() updating the requested article");
    }

    public function salvarObservacaoAction()
    {
        $response = array();

        $st_observacaoentregadocumentos = $this->getParam('st_observacaoentregadocumentos');
        $id_matricula = $this->getParam('id_matricula');

        if ($st_observacaoentregadocumentos && $id_matricula) {
            $response = $this->negocio->salvarObservacao($id_matricula, $st_observacaoentregadocumentos);
        }

        $this->_helper->json($response);
    }

}

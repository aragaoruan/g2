<?php

/**
 * Classe de controller MVC para controlar cadastros de novas turmas
 * @author Helder Fernandes Silva <helder.silva@unyleya.com.br>
 * @since 2014-21-08
 * @package application
 * @subpackage controller
 */
class Default_TurmaController extends Ead1_ControllerRestrita
{
    /**
     * @var G2\Negocio\Turma $negocio
     */
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Turma();
    }

    public function indexAction()
    {
        $id = $this->_getParam('id');
        if ($id) {
            $resultado = $this->negocio->findTurma($id);
            $turno = $this->negocio->findOneBy('\G2\Entity\TurmaTurno', array('id_turma' => $resultado->getId_turma()));
            $arrResult = array(
                'id_turma' => $resultado->getId_turma(),
                'dt_inicioinscricao' => $resultado->getDt_inicioinscricao() ? $resultado->getDt_inicioinscricao()->format('d/m/Y') : NULL,
                'dt_fiminscricao' => $resultado->getDt_fiminscricao() ? $resultado->getDt_fiminscricao()->format('d/m/Y') : NULL,
                'dt_inicio' => $resultado->getDt_inicio() ? $resultado->getDt_inicio()->format('d/m/Y') : NULL,
                'dt_fim' => $resultado->getDt_fim() ? $resultado->getDt_fim()->format('d/m/Y') : NULL,
                'dt_cadastro' => $resultado->getDt_cadastro(),
                'nu_maxalunos' => $resultado->getNu_maxalunos(),
                'bl_ativo' => $resultado->getBl_ativo(),
                'st_turma' => $resultado->getSt_turma(),
                'st_codigo' => $resultado->getSt_codigo(),
                'st_tituloexibicao' => $resultado->getSt_tituloexibicao(),
                'id_usuariocadastro' => $resultado->getId_usuariocadastro() ? $resultado->getId_usuariocadastro()->getId_usuario() : NULL,
                'id_situacao' => $resultado->getId_situacao() ? $resultado->getId_situacao()->getId_situacao() : NULL,
                'id_evolucao' => $resultado->getId_evolucao() ? $resultado->getId_evolucao()->getId_evolucao() : NULL,
                'id_entidadecadastro' => $resultado->getId_entidadecadastro() ? $resultado->getId_entidadecadastro()->getId_entidade() : NULL,
                'nu_maxfreepass' => $resultado->getNu_maxfreepass()
            );

            if($resultado->getNu_maxfreepass()){
                $negocioVenda = new \G2\Negocio\Venda();
                $resultado = $negocioVenda->verificaMatriculasFreePassDisponiveis($resultado->getId_turma());
                $arrResult['saldoFreePass'] = $resultado['RestanteFreePass'];
            }else{
                $arrResult['saldoFreePass'] = '';
            }




            if ($turno != null) {
                $arrResult['id_turno'] = $turno->getId_turno() ? $turno->getId_turno()->getId_turno() : NULL;
            } else {
                $arrResult['id_turno'] = NULL;
            }
            $this->view->turma = json_encode($arrResult);
        } else {
            $turno = new \G2\Entity\TurmaTurno();
            $arrResult = array(
                'id_turno' => null,
                'saldoFreePass' => null
            );
            $this->view->turma = json_encode($arrResult);
        }
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
    }

    public function enviaDadosTabelaAction()
    {
        if ($this->getRequest()->isPost()) {

            try {

                $body = $this->getRequest()->getPost();
                $result = $this->negocio->enviaDadosTabela($body);

                if($result['nu_maxfreepass']){
                    $negocioVenda = new \G2\Negocio\Venda();
                    $resultado = $negocioVenda->verificaMatriculasFreePassDisponiveis($result['id_turma']);
                    $result['saldoFreePass'] = $resultado['RestanteFreePass'];
                }else{
                    $result['saldoFreePass'] = '';
                }
                $this->_helper->json($result);
            } catch (\Exception $e) {
                $this->getResponse()->setHttpResponseCode(400);
                $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
                $this->_helper->json($mensageiro->toArray());

            }

        } else {
            $this->_helper->json("Requisição Invalida!");
        }
    }

    public function finalizarAction()
    {
        if ($this->getRequest()->isPost()) {
            $body = $this->getRequest()->getPost();
            $result = $this->negocio->finalizar($body['id_turma']);
            $this->_helper->json($result->toArray());
        } else {
            $this->_helper->json("Requisição Invalida!");
        }
    }

    public function retornaTurmaProjetoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $id_turma = $this->getParam('id');
        $return = $this->negocio->findProjetoTurma($id_turma);
        $array = array();
        foreach ($return as $key => $ret) {
            $array[$key] = $this->negocio->toArrayEntity($ret);
            $array[$key]['id'] = $ret->getId_projetopedagogico();
            $array[$key]['st_projetopedagogico'] = $ret->getSt_tituloexibicao();
        }
        $this->_helper->json($array);
    }

    public function recarregaArvoreAction()
    {
        $result = array();
        $negocioTurmaEntidade = new G2\Negocio\Turma();
        $retorno = $negocioTurmaEntidade->findByTurmaEntidade(array('id_turma' => $this->getParam('idturma'), 'bl_ativo' => true));
        if (!empty($retorno)) {
            foreach ($retorno as $key => $entidade) {
                $result[$key] = $entidade->getId_entidade()->getId_entidade();
            }
        }
        $this->_helper->json($result);
    }

    public function retornaHorariosAction()
    {
        $id_turma = intval($this->getParam('id'));
        $this->_helper->viewRenderer->setNoRender(true);
        $negocioHorario = new G2\Negocio\VwHorarioAula();
        $resultado = $negocioHorario->findHorario();
        $consulta = null;
        if (!empty($resultado)) {
            foreach ($resultado as $key => $entidade) {
                if ($id_turma != 0) {
                    $consulta = $this->negocio->findOneBy('\G2\Entity\TurmaHorario', array('id_horarioaula' => $entidade->getId_horarioaula(), 'bl_ativo' => true, 'id_turma' => $id_turma));
                    if ($consulta) {
                        $result[$key]['ck_selectedHorario'] = true;
                    } else {
                        $result[$key]['ck_selectedHorario'] = false;
                    }
                } else {
                    $result[$key]['ck_selectedHorario'] = false;
                }
                $result[$key]['id_horarioaula'] = $entidade->getId_horarioaula();
                $result[$key]['hr_inicio'] = substr($entidade->getHr_inicio(), 0, 8);
                $result[$key]['hr_fim'] = substr($entidade->getHr_fim(), 0, 8);
                $result[$key]['id_turno'] = $entidade->getId_turno();
                $result[$key]['id_entidade'] = $entidade->getId_entidade();
                $dias = array();
                if ($entidade->getBl_segunda() == 1) {
                    array_push($dias, "Seg");
                };
                if ($entidade->getBl_terca() == 1) {
                    array_push($dias, "Ter");
                };
                if ($entidade->getBl_quarta() == 1) {
                    array_push($dias, "Qua");
                }
                if ($entidade->getBl_quinta() == 1) {
                    array_push($dias, "Qui");
                };
                if ($entidade->getBl_sexta() == 1) {
                    array_push($dias, "Sex");
                };
                if ($entidade->getBl_sabado() == 1) {
                    array_push($dias, "Sab");
                };
                if ($entidade->getBl_domingo() == 1) {
                    array_push($dias, "Dom");
                };
                $result[$key]['dias'] = implode(" - ", $dias);
                $result[$key]['st_horarioaula'] = $entidade->getSt_horarioaula();
                $result[$key]['st_turno'] = $entidade->getSt_turno();
            }
        }
        $this->_helper->json($result);
    }

    public function deletaProjetoAction()
    {
        $id = intval($this->getParam('id'));
        $idturma = intval($this->getParam('proj'));

        if (!is_int($id)) {
            $this->getResponse()->setHttpResponseCode(400)->setBody('Não posso apagar um registro com ID inválido!');
        } else {
            $array = array(
                'id_projetopedagogico' => $id,
                'id_turma' => $idturma,
                'bl_ativo' => false
            );
            $result = $this->negocio->deletaProjetoTurma($array);
            if ($result) {
                $this->_helper->json($result);
            } else {
                $this->getResponse()->setHttpResponseCode(400)->appendBody('Erro ao tentar remover registro');
            }
        }
    }

    public function enviaEmailAction()
    {

        try {
            $this->_helper->viewRenderer->setNoRender(true);
            $retorno = $this->negocio->enviaEmail($this->getParam('idturma'));
            if ($retorno) {
                return $this->_helper->json($retorno);
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function retornarTurmaMaiorCargaHorariaAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        //pega o parametro
        $params = $this->limpaGetAllParams($this->getAllParams());
        //chama o metodo da negocio e passa o parametro
        $mensageiro = $this->negocio->retornarTurmaMaiorCargaHoraria($params);
        //se o retorno for com sucesso passa somente os dados
        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->appendBody(json_encode($mensageiro->getFirstMensagem()));
            $this->getResponse()->setHttpResponseCode(200);
        } else {
            $this->getResponse()->appendBody($mensageiro->getText());
            $this->getResponse()->setHttpResponseCode(500);
        }
    }

    public function retornarVwTurmaAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocio->retornarVwTurma($params);
        if ($result) {
            foreach ($result as $key => $turma) {
                $result[$key] = $this->negocio->toArrayEntity($turma);
            }
        }
        $this->_helper->json($result);
    }

    public function retornarVwTurmaDisciplinaAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocio->retornarVwTurmaDisciplina($params);
        if ($result) {
            foreach ($result as $key => $turma) {
                $result[$key] = $this->negocio->toArrayEntity($turma);
            }
        }
        $this->_helper->json($result);
    }

    public function getAlunosTurmaCarteirinhaAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocio->retornarAlunosTurmaCarteirinha($params);
        $this->_helper->json($result);
    }

    public function getAlunosTurmaCarteirinhaComboAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocio->retornarAlunosTurmaCarteirinhaCombo($params);
        $this->_helper->json($result);
    }

    /**
     * @history TEC-241
     * @description Retorna turmas disponiveis de acordo com um produto
     */
    public function getTurmasByProdutoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $idProduto = $this->getParam('id_produto');
        $idTurma = $this->getParam('id_turma');
        $idSituacao = $this->getParam('id_situacao');

        if ($idProduto) {
            $results = $this->negocio->getAllByProduto($idProduto, $idTurma, $idSituacao ? $idSituacao : \G2\Constante\Situacao::TB_TURMA_ATIVA);
            $mensageiro = array();
            foreach ($results as $result) {
                $mensageiro[] = $this->negocio->toArrayEntity($result);
            }

            $this->_helper->json($mensageiro);
        }
    }

    public function retornarDiaSemanaByTurmaAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $id_turma = $this->getParam('id_turma');

        $mensageiro = $this->negocio->retornarDiaSemanaByTurma($id_turma);
        if ($mensageiro->getType() == Ead1_IMensageiro::TYPE_SUCESSO) {
            $arrReturn = array();
            foreach ($mensageiro->getMensagem() as $key => $row) {
                $arrReturn[] = array(
                    'id_diasemana' => (int)$row['id_diasemana'],
                    'st_diasemana' => $row['st_diasemana'],
                    'id_tipoaula'=>(int)$row['id_tipoaula'],
                    'st_tipoaula'=>$row['st_tipoaula'],
                    'id_turno'=>$row['id_turno'],
                    'st_turno'=>$row['st_turno']

                );
            }
            $this->getResponse()->appendBody(json_encode($arrReturn));
            $this->getResponse()->setHttpResponseCode(200);
        } else {
            $this->getResponse()->appendBody(json_encode($mensageiro->getText()));
            $this->getResponse()->setHttpResponseCode(400);
        }

    }


}

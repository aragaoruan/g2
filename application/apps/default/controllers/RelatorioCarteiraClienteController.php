<?php

class Default_RelatorioCarteiraClienteController extends Ead1_ControllerRestrita
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {

    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
    }

    public function retornaRelatorioAction()
    {
        $this->negocio = new G2\Negocio\CarteiraCliente();
        $params = $this->limpaGetAllParams($this->getAllParams());

        $result = $this->negocio->executaRelatorio($params);
        $this->view->dados = $result['dados'];
        $this->view->params = $result['params'];
        $this->view->relatorio = true;

        $this->render('relatorio');
    }

    public function gerarXlsAction()
    {
        $this->negocio = new G2\Negocio\CarteiraCliente();
        $params = $this->limpaGetAllParams($this->getAllParams());

        $result = $this->negocio->executaRelatorio($params);
        $this->view->dados = $result['dados'];
        $this->view->params = $result['params'];
        $this->view->nomeUsuario = $this->negocio->sessao->nomeUsuario;
        $this->view->dataGeracao = date("d/m/Y à\s H:i:s");

    }

    public function impressaoAction()
    {
        $this->negocio = new G2\Negocio\CarteiraCliente();
        $params = $this->limpaGetAllParams($this->getAllParams());

        $result = $this->negocio->executaRelatorio($params);
        $this->view->dados = $result['dados'];
        $this->view->params = $result['params'];
        $this->view->nomeUsuario = $this->negocio->sessao->nomeUsuario;
        $this->view->dataGeracao = date("d/m/Y à\s H:i:s");

    }
}
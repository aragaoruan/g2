<?php
/**
 * Método que retorna dados do PHP
 * @author edermariano
 *
 */
class Default_PhpController extends Ead1_Controller{
	
	/**
	 * dados do servidor PHP
	 */
	public function indexAction(){
		phpinfo();exit;
	}
	
	/**
	 * Dados da aplicação
	 */
	public function aplicacaoAction(){
		echo "<h1>Aplicação Info:</h1>";
		echo "<ul>";
		echo "<li><b>Ambiente:</b> " . Ead1_Ambiente::getAmbiente() . "</li>";
		echo "<li><b>Server Name:</b> " . $_SERVER['SERVER_NAME'] . "</li>";
		echo "<li><b>Base URL:</b> " . Ead1_Ambiente::geral()->baseUrl . "</li>";
		echo "<li><b>DB:</b> " . Ead1_Ambiente::application()->resources->db->params->dbname . "</li>";
		echo "</ul><hr />";
		exit;
	}
	
	/**
	 * Método que retorna o ambiente em questão
	 */
	public function ambienteAction(){
		echo "<h1>Ambiente Info:</h1>";
		echo "<ul>";
		echo "<li><b>TAG:</b> " . Ead1_Ambiente::getAmbiente() . "</li>";
		echo "</ul>";
		exit;
	}
	
	/**
	 * Método que retorna informações do SERVER
	 */
	public function serverAction(){
		echo "<h1>Server Info:</h1>";
		Zend_Debug::dump($_SERVER, "Server");
		exit;
	}
	
	/**
	 * Método que retorna informações do Zend_Framework em questão
	 */
	public function zfAction(){
		echo "<h1>Zend Framework Info:</h1>";
		echo "<ul>";
		echo "<li><b>Version:</b> " . Zend_Version::VERSION . "</li>";
		echo "</ul>";
		exit;
	}
	
	/**
	 * Método que retorna informações do SVN
	 */
	public function svnAction(){
		$saida = shell_exec('svn info');
		$arinfo = explode("\n", $saida);
		
		echo "<h1>SVN Info:</h1><ul>";
		echo "<li><b>" . str_replace(":", ": </b>", $arinfo[4]) . "</li>";
		echo "<li><b>" . str_replace(":", ": </b>", $arinfo[7]) . "</li>";
		echo "<li><b>" . str_replace(":", ": </b>", $arinfo[8]) . "</li>";
		echo "<li><b>" . str_replace(":", ": </b>", $arinfo[9]) . "</li>";
		echo "</ul>";
		exit;
	}	
	
	public function geralAction(){
		
		$saida = shell_exec('svn info');
		$arinfo = explode("\n", $saida);
		
		echo "<h1>Ambiente Info:</h1>";
		echo "<ul>";
		echo "<li><b>TAG:</b> " . Ead1_Ambiente::getAmbiente() . "</li>";
		echo "</ul><hr />";
		echo "<h1>Server Info:</h1>";
		Zend_Debug::dump($_SERVER, "Server");
		
		echo "<hr /><h1>Zend Framework Info:</h1>";
		echo "<ul>";
		echo "<li><b>Version:</b> " . Zend_Version::VERSION . "</li>";
		echo "</ul>";
		echo "<hr /><h1>SVN Info:</h1><ul>";
		echo "<li><b>" . str_replace(":", ": </b>", $arinfo[4]) . "</li>";
		echo "<li><b>" . str_replace(":", ": </b>", $arinfo[7]) . "</li>";
		echo "<li><b>" . str_replace(":", ": </b>", $arinfo[8]) . "</li>";
		echo "<li><b>" . str_replace(":", ": </b>", $arinfo[9]) . "</li>";
		echo "</ul>";
		exit;
	}
}
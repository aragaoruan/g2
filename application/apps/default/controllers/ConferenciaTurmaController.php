<?php

/**
 * Controller para ConferenciaTurma
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2014-11-19
 * @package application
 * @subpackage controller
 */


class Default_ConferenciaTurmaController extends Ead1_ControllerRestrita
{
    private $negocio;

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('conferencia-turma/index.js');
    }

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Negocio();
    }

    public function indexAction()
    {
    }

    public function getProjetoPedagogicoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $idTurma = $this->getParam('id_turma');

        if ($idTurma) {
            $turmaBO = new TurmaBO();
            $vw = new VwConferenciaProjetoConsolidadoTO();
            $vw->setId_turma($idTurma);
            $vw->set_idEntidade($this->negocio->sessao->id_entidade);

            $results = $turmaBO->retornarProjetosTurmas($vw, "id_turma = {$idTurma}", "st_projetopedagogico");

            $this->_helper->json($results);
        }
    }

    public function getDisciplinaProjetoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $idTurma = $this->getParam('id_turma');
        $idProjeto = $this->getParam('id_projetopedagogico');

        if ($idTurma && $idProjeto) {
            $turmaBO = new TurmaBO();
            $vw = new VwConferenciaTurmasTO();
            $vw->set_idTurma($idTurma);
            $vw->set_idProjetopedagogico($idProjeto);

            $results = $turmaBO->retornarConferenciaTurmas($vw, array(
                "id_turma = {$idTurma}",
                "id_projetopedagogico = {$idProjeto}",
                "id_entidade = {$vw->getSessao()->id_entidade}"
            ));

            $json = array();
            if (!empty($results->mensagem)) {
                foreach ($results->mensagem as $result) {
                    $json[] = array(
                        'id_turma' => $result->getId_turma(),
                        'id_projetopedagogico' => $result->getId_projetopedagogico(),
                        'id_entidade' => $result->getId_entidade(),
                        'id_disciplina' => $result->getId_disciplina(),
                        'st_disciplina' => $result->getSt_disciplina(),
                        'st_saladeaula' => $result->getSt_saladeaula(),
                        'st_projetopedagogico' => $result->getSt_projetopedagogico(),
                        'dt_inicioinscricao' => $result->getDt_inicioinscricao() ? date('d/m/Y',
                            $result->getDt_inicioinscricao()->getTimestamp()) : null,
                        'dt_fiminscricao' => $result->getDt_fiminscricao() ? date('d/m/Y',
                            $result->getDt_fiminscricao()->getTimestamp()) : null,
                        'dt_abertura' => $result->getDt_abertura() ? date('d/m/Y',
                            $result->getDt_abertura()->getTimestamp()) : null,
                        'dt_encerramento' => $result->getDt_encerramento() ? date('d/m/Y',
                            $result->getDt_encerramento()->getTimestamp()) : null,
                        'st_professor' => $result->getSt_professor(),
                        'st_coordenadorpp' => $result->getSt_coordenadorpp(),
                        'st_codsistema' => $result->getSt_codsistema(),
                        'st_salareferencia' => $result->getSt_salareferencia(),
                        'st_processada' => $result->getSt_processada(),
                        'st_nomemoodle' => $result->getst_Nomemoodle(),
                    );
                }
            }

            $this->getResponse()->appendBody(json_encode($json));
        }
    }

}

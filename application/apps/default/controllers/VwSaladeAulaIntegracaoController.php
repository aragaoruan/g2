<?php
/**
 * Created by PhpStorm.
 * User: helder.silva
 * Date: 12/12/2014
 * Time: 10:23
 */

/**
 * Description of SalaAulaIntegracaoController
 *
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class Default_VwSaladeAulaIntegracaoController extends Ead1_Controller
{

    /** @var  \G2\Negocio\VwSaladeAulaIntegracao */
    private $negocio;

    public function init()
    {
        $this->negocio = new \G2\Negocio\VwSaladeAulaIntegracao();
    }

    public function indexAction()
    {

    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
    }

    /**
     * Função para retornar as salas que contém determinadas disciplinas de determinada entidade
     */

    public function findSalaByEntityDiscAction()
    {
        $resultado = $this->negocio->retornaSalaByEntityDisc(array('id_disciplina' => $this->getParam('id_disciplina')));
        $arrReturn = array();
        if ($resultado) {
            foreach ($resultado as $row) {
                $arrReturn[] = array(
                    'id_saladeaula' => $row->getId_saladeaula(),
                    'dt_abertura' => ($row->getDt_abertura() ? date_format($row->getDt_abertura(), 'd/m/Y') : 'Sem Data'),
                    'dt_encerramento' => ($row->getDt_encerramento() ? date_format($row->getDt_encerramento(), 'd/m/Y') : 'Sem Data'),
                    'id_situacao' => $row->getId_situacao(),
                    'st_situacao' => $row->getSt_situacao(),
                    'id_modalidadesaladeaula' => $row->getId_modalidadesaladeaula(),
                    'id_sistema' => $row->getId_sistema(),
                    'id_saladeaulaintegracao' => $row->getId_saladeaulaintegracao(),
                    'id_entidade' => $row->getId_entidade(),
                    'bl_ativa' => $row->getBl_ativa(),
                    'st_saladeaula' => $row->getSt_saladeaula(),
                    '"st_modalidadesaladeaula' => $row->getSt_modalidadesaladeaula(),
                    'st_sistema' => $row->getSt_sistema(),
                );
            }
        }
        $this->_helper->json($arrReturn);
    }

    /**
     * Função para integra a sala de aula, chama a negocio passando os parametros necessários
     */


    public function integrarSalaAulaAction()
    {
        if ($this->getRequest()->isPost()) {
            $body = $this->getRequest()->getPost();
            $result = $this->negocio->integrarSalaExistente($body);
//            Zend_Debug::dump($result);exit;
            if ($result->getType() == Ead1_IMensageiro::TYPE_SUCESSO) {
                $array = new Ead1_Mensageiro($this->negocio->toArrayEntity($result->getFirstMensagem()), Ead1_IMensageiro::TYPE_SUCESSO);
                $array->setText("Sala integrada com sucesso!");
                $this->_helper->json($array->toArrayAll());
            } else {
                $this->_helper->json($result->toArrayAll());
            }
        } else {
            $this->_helper->json("Requisição Invalida!");
        }
        $this->getResponse()->appendBody(json_encode($result));
    }

    /**
     * Função para verificar se a sala já está integrada, para definir
     * se o botão que integra salas existentes estará habilitado.
     */

    public function verificaSalaIntegradaAction()
    {
        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);

        $result = $this->negocio->verificaSalaIntegracao($params);
        if ($result) {
            /**
             * @var int $key
             * @var \G2\Entity\SalaDeAulaIntegracao $item
             */
            foreach ($result as $key => $item) {
                $result[$key] = $item->toBackboneArray();
                $result[$key]["id_disciplinaintegracao"] = $item->getId_disciplinaintegracao()
                    ? $item->getId_disciplinaintegracao()->getId_disciplinaintegracao() : null;
            }
        }

        $this->_helper->json($result);
    }
}

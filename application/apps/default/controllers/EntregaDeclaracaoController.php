<?php

/**
 * Controller para funcionalidade de Entrega de Declaraçao
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 * @since  14-01-2015
 * @package application
 * @subpackage controller
 */
class Default_EntregaDeclaracaoController extends Ead1_ControllerRestrita
{


    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        echo $this->view->headScript()
            ->appendScript($this->view->render('/entrega-declaracao/index.js'));
        echo $this->view->headScript()
            ->appendScript('/js/jquery.form.js');
    }

    public function salvarEntregaDeclaracaoAction()
    {
        $declaracao = new \G2\Negocio\EntregaDeclaracao();
        $params = $this->getAllParams();
        unset($params['module']);
        unset($params['action']);
        unset($params['controller']);
        $result = $declaracao->salvarEntregaDeclaracao($params);
        $this->_helper->json($result);
    }

}

<?php

/**
 * Classe de controller MVC para controlar Situação
 * @author Elcio Guimarães <elcioguimaraes@gmail.com>
 *
 * @package application
 * @subpackage controller
 */
class Default_SituacaoController extends Ead1_ControllerRestrita {
    public function init() {
        parent::init();
        parent::setupDoctrine();
    }

    public function jsAction(){
        $this->_helper->viewRenderer->setNoRender(true);
        echo   $this->view->render('situacao/index.js');
    }

    public function indexAction() {

    }

}

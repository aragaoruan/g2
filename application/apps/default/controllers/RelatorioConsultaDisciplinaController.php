<?php

/**
 * Controller Relatorio Consulta Disciplina
 * @author Rafael Leite <rafael.leite@unyleya.com.br>
 * @since 2015-09-26
 * @package application
 * @subpackage controller
 */
//use G2\Negocio\Disciplina;

class Default_RelatorioConsultaDisciplinaController extends Ead1_ControllerRestrita
{

    /**
     * @var \G2\Negocio\VwConsultaDisciplina
     */
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\VwConsultaDisciplina();
    }

    public function indexAction()
    {
        $js = $this->view->render('relatorio-consulta-disciplina/index.js');
        echo $this->view->headScript()->appendScript($js);

    }

    public function pesquisaRelatorioAction()
    {

        try {
            $params = $this->limpaGetAllParams($this->getAllParams());
            $this->_helper->json($this->negocio->retornaResultadoPesquisa($params));
        } catch(\Exception $e){
            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json($e->getMessage());
        }


    }

    public function gerarXlsAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocio->retornaResultadoPesquisa($params);
        $this->view->result = $result;
    }
}
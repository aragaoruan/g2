<?php

/**
 * Created by PhpStorm.
 * User: kayo.silva
 * Date: 18/08/2017
 * Time: 14:50
 */
class Default_TipoSelecaoController extends Ead1_ControllerRestrita
{
    /**
     * @var \G2\Negocio\TipoSelecao
     */
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\TipoSelecao();
    }


    public function indexAction()
    {
        $idTipoSelecao = $this->getParam("id");

        if ($idTipoSelecao) {
            $result = $this->negocio->find("\G2\Entity\TipoSelecao", $idTipoSelecao);
            if ($result instanceof \G2\Entity\TipoSelecao) {

                $this->view->habilitarEdicao = $this->negocio->verificarAptoEditar($idTipoSelecao);
                $this->view->dados = $result->toBackboneArray();
            }
        }
        echo $this->view->headScript()
            ->appendScript($this->view->render($this->getRequest()->getControllerName() . '/index.js'));
    }

}

<?php
class Default_FuncionalidadesController extends Ead1_Controller{
	
	public function indexAction(){}
	
	
	public function crudAction(){
		
	}
	
	
	public function sincronizarareasAction(){
		$ro = new SincronizadorRO();
		$to = new MatriculaTO();
		if(!$this->_getParam('id_entidade')){
			echo "Informe o id da entidade!";
			exit;
		}
		if(!$this->_getParam('id_sistema')){
			echo "Informe o id do sistema!";
			exit;
		}
		if(!$to->getSessao()->id_entidade){
			echo "Você tem que estar logado!";
			exit;
		}
		Zend_Debug::dump($ro->sincronizarAreas($this->_getParam('id_entidade'), $this->_getParam('id_sistema')));
		
		exit;
	}
}
<?php

use G2\Negocio\Transferencia;

/**
 * Controller para transferencia de projeto pedagogico
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @since  11-07-2014
 * @package application
 * @subpackage controller
 */
class Default_TransfereProjetoController extends Ead1_ControllerRestrita
{

    /**
     * @var \G2\Negocio\Transferencia
     */
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Transferencia();
    }

    public function indexAction()
    {
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('transfere-projeto/index.js');
    }

    public function retornoPesquisaProjetoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $resultado = $this->negocio->findByPesquisaProjetoModal($this->getAllParams());
        $arrReturn = array();
        $arrPP = array();
        if ($resultado) {
            foreach ($resultado as $row) {

                if (in_array($row->getId_projetopedagogico(), $arrPP))
                    continue;

                $arrReturn[] = array(
                    'id_projetopedagogico' => $row->getId_projetopedagogico(),
                    'st_projetopedagogico' => $row->getSt_projetopedagogico(),
                    'st_areaconhecimento' => $row->getSt_areaconhecimento(),
                    'st_nivelensino' => $row->getSt_nivelensino(),
                    'st_nomeentidade' => $row->getSt_nomeentidade(),
                    'st_situacao' => $row->getSt_situacao(),
                    'bl_turma' => ($row->getBl_turma() ? 1 : 0)
                );

                $arrPP[] = $row->getId_projetopedagogico();
            }
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }


    public function getVwTurmasDisponivesAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());

        $mensageiro = $this->negocio->retornaTurmas($params);
        $arrReturn = array();

        if ($mensageiro && is_array($mensageiro)) {
            foreach ($mensageiro as $row) {
                $arrReturn[] = array(
                    'id_turmaprojeto' => $row['id_turmaprojeto'],
                    'id_projetopedagogico' => $row['id_projetopedagogico'],
                    'id_turma' => $row['id_turma'],
                    'st_turma' => $row['st_turma'],
                    'st_tituloexibicao' => $row['st_tituloexibicao']
                );
            }
        }
        $this->_helper->json($arrReturn);
    }

    public function retornarDisciplinasOrigemAction()
    {
        if ($this->getParam('bl_ordenar', false))
            $arrReturn = $this->negocio->retornarGridOrigemOrdenada($this->getAllParams());
        else {
            $resultado = $this->negocio->retornarGridOrigem($this->getAllParams());
            $arrReturn = array();
            if ($resultado) {
                foreach ($resultado as $row) {
                    $arrReturn[] = $row->_toArray();
                }
            }
        }

        $this->_helper->json($arrReturn);
    }

    public function retornarDisciplinasDestinoAction()
    {
        $arrReturn = $this->negocio->retornarGridDestino($this->getAllParams());
        $this->_helper->json($arrReturn);
    }

    public function salvarTransferenciaAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);


        $data = $this->limpaGetAllParams($this->getAllParams());
        $negocioMat = new \G2\Negocio\Matricula();
        $return = $this->negocio->transferirAluno($data);

        if ($return->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $params = array('id_matricula' => $data['id_matriculaorigem']);
            $notasAgrupadas = $negocioMat->retornaVwGradeNotaAgrupada($params);
            $this->view->notasAgrupadas = $notasAgrupadas;
            $html = $this->view->render('matricula/historico-matricula-grade-nota.phtml');
            $negocioMat->salvarHistoricoMatriculaGradeNota($params, $html);
        }

        return $this->_helper->json($return);
    }

    public function calcularValorTransferenciaAction()
    {
        $data = $this->limpaGetAllParams($this->getRequest()->getParams());
        $return = $this->negocio->calcularValorNovoContrato($data, $this->negocio->getId_entidade());
        return $this->_helper->json($return);
    }

    public function imprimirSimulacaoAction()
    {
        $data = $this->limpaGetAllParams($this->getRequest()->getParams());
        $arrData = (array)json_decode($data['data']);
        $return = $this->negocio->imprimirSimulacao($arrData);
        $this->view->dados = $return;
    }

    public function getAreasAction()
    {
        $response = array();
        $results = $this->negocio->findByVwAreaConhecimento();

        if ($results) {
            foreach ($results as $item) {
                $response[] = array(
                    'id_areaconhecimento' => $item->getId_areaconhecimento(),
                    'st_areaconhecimento' => $item->getSt_areaconhecimento()
                );
            }
        }

        $this->_helper->json($response);
    }

    public function getNiveisAction()
    {
        $response = array();
        $results = $this->negocio->findAll('\G2\Entity\NivelEnsino');

        if ($results) {
            foreach ($results as $item) {
                $response[] = array(
                    'id_nivelensino' => $item->getId_nivelensino(),
                    'st_nivelensino' => $item->getSt_nivelensino()
                );
            }
        }

        $this->_helper->json($response);
    }

    public function getEntidadesAction()
    {
        $response = array();

        $entidadeBo = new \EntidadeBO();
        $mensageiro = $entidadeBo->retornaEntidadeRecursivo();
        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            foreach ($mensageiro->getMensagem() as $item) {
                if ($item instanceof \EntidadeTO && $item->getBl_ativo()) {
                    $response[] = array(
                        'id_entidade' => $item->getId_entidade(),
                        'st_nomeentidade' => $item->getSt_nomeentidade()
                    );
                }
            }
        }

        $this->_helper->json($response);
    }

    public function validaSalaTransferenciaAction()
    {
        $data = $this->limpaGetAllParams($this->getAllParams());
        $mensageiro = $this->negocio->verificaSalaDisponivelAlocacaoTransferencia($data);

        $this->_helper->json($mensageiro);
    }

    public function getMeioPagamentoAction()
    {
        $meiosPagamento = \G2\Constante\MeioPagamento::getMeiosPagamentoTransferenciaArray();

        foreach ($meiosPagamento as $key => $value) {
            $response[] = array(
                'id_meiopagamento' => $key,
                'st_meiopagamento' => $value
            );
        }

        return $this->_helper->json($response);
    }

    public function getCampanhaAction()
    {
        $ngCampanhas = new \G2\Negocio\CampanhaComercial();
        $response = $ngCampanhas->getCampanhaTransferencia();
        return $this->_helper->json($response);
    }

    public function getAtendenteAction()
    {
        $ngVwAtendente = new \G2\Negocio\VwAtendente();
        $mensageiro = $ngVwAtendente->findByAtendentePorEntidade();
        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            foreach ($mensageiro->getMensagem() as $value) {
                $response[] = array(
                    'id_atendente' => $value->getId_usuario(),
                    'st_atendente' => $value->getSt_nomecompleto());
            }
        }

        return $this->_helper->json($response);
    }
}

<?php

/**
 * Classe de controller MVC para controlar os dados da Vw_TextoSistema
 * @author Elcio Guimarães <elcioguimaraes@gmail.com>
 * 
 * @package application
 * @subpackage controller
 */
class Default_VwTextoSistemaController extends Ead1_ControllerRestrita {
    
    public function init() {
        parent::init();
        parent::setupDoctrine();
    }

    public function jsAction(){ 
        $this->_helper->viewRenderer->setNoRender(true);
        echo   $this->view->render('vw-texto-sistema/index.js');
    }
    
    public function indexAction() {
        
    }
    
}


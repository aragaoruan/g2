<?php
/**
 * Created by PhpStorm.
 * User: aragaoruan
 * Date: 03/04/18
 * Time: 14:59
 */

class Default_ConfiguracaoCertificadoParcialController extends Ead1_ControllerRestrita
{
    /**
     * @var \G2\Negocio\ConfiguracaoCertificadoParcial
     */
    private $negocio;

    public function init()
    {
        parent::init();
        parent::setupDoctrine();
        $this->negocio = new \G2\Negocio\ConfiguracaoCertificadoParcial;
    }

    public function indexAction()
    {
        $idCertificadoParcial = $this->getParam('id');
        $result = $this->negocio->certificadoParcialIndex($idCertificadoParcial);
        $this->view->certificadoParcial = $result;
    }

    public function retornarDisciplinasAction()
    {

        $idCertificadoParcial = $this->getParam('id_certificadoparcial');
        $where = array(
            "id_certificadoparcial" => $idCertificadoParcial
        );
        $this->_helper->json($this->negocio->certificadoParcialDisciplinas($where));
    }

    public function salvarCertificadoParcialAction()
    {

        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);

        if (!empty($params)) {
            $mensageiro = $this->negocio->salvarCertificadoParcial($params);
            if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $this->getResponse()->appendBody(json_encode($mensageiro->toArray()));
                $this->getResponse()->setHttpResponseCode(200);
            }

            $this->_helper->json($mensageiro);
        }
        $this->_helper->json(array(
            'type' => Ead1_IMensageiro::TYPE_ERRO,
            'title' => Ead1_IMensageiro::TITLE_ERRO,
            'text' => 'Ocorreu um erro ao cadastrar certificado',
        ));
    }

    public function retornaTextoSistemaAction()
    {

        $arrReturn = array();
        $result = $this->negocio->retornaTextoSistema();
        $arrReturn = array();
        if ($result) {
            foreach ($result as $row) {
                $arrReturn[] = array(
                    'id' => $row['id_textosistema'],
                    'st_textosistema' => $row['st_textosistema'],
                    'id_entidade' => $row['id_entidade'],
                );
            }
        }
        $this->_helper->json($arrReturn);
    }

    public function retornaDisciplinaAction()
    {
        $arrReturn = array();
        $result = $this->negocio->retornaDisciplina();
        $arrReturn = array();
        if ($result) {
            foreach ($result as $row) {
                $arrReturn[] = array(
                    'id_disciplina' => $row['id_disciplina'],
                    'st_disciplina' => $row['st_disciplina'],
                    'id_entidade' => $row['id_entidade'],
                );
            }
        }
        $this->_helper->json($arrReturn);
    }

}

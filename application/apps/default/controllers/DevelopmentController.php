<?php

/**
 * Class Default_DevelopmentController
 *
 * Controller criada para parar o bug do PHPUnit com o Storm
 */
class Default_DevelopmentController extends \Zend_Controller_Action
{

    public function init(){
        $this->getRequest()->setActionName('x');
    }

    public function xAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();

    }

}


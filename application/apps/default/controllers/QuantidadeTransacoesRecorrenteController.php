<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Resende
 * Date: 24/03/15
 * Time: 15:27
 */

class Default_QuantidadeTransacoesRecorrenteController extends Ead1_ControllerRestrita
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {

    }
//retornarVwEntidadeRecursaId
    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
    }

    public function retornaRelatorioAction()
    {
        $this->negocio = new G2\Negocio\QuantidadeTransacoesRecorrente();

        $this->_helper->json($this->negocio->executaRelatorio($this->limpaGetAllParams($this->getAllParams())));
    }

    public function gerarXlsAction()
    {
        $this->negocio = new G2\Negocio\QuantidadeTransacoesRecorrente();
        $this->view->dados = $this->negocio->executaRelatorio($this->limpaGetAllParams($this->getAllParams()));

    }
} 
<?php
/**
 * Classe para exemplos de WebServices
 * @author edermariano
 *
 * @package application
 * @subpackage controller
 */
class Default_WebServiceController extends Ead1_Controller{
	
	/**
	 * Método para listagem dos webservices
	 */
	public function indexAction(){
		
	}
	
	/**
	 * Método com WebService de CEP
	 */
	public function cepAction(){
		
	}

	/**
	 * Método que recebe a resposta do webService para cep
	 */
	public function resultcepAction(){
		$cep = $this->_getParam('cep');
		$webservice_url     = 'http://webservice.kinghost.net/web_cep.php';
		$webservice_query    = array(
		    'auth'    => '3122116928bb5f19cdead9865faef878', //Chave de autenticação do WebService - Consultar seu painel de controle
		    'formato' => 'query_string', //Valores possíveis: xml, query_string ou javascript
		    'cep'     => $cep //CEP que será pesquisado
		);
		
		//Forma URL
		$webservice_url .= '?';
		foreach($webservice_query as $get_key => $get_value){
		    $webservice_url .= $get_key.'='.urlencode($get_value).'&';
		}
		
		parse_str(file_get_contents($webservice_url), $resultado);
		
		Zend_Debug::dump($cep, "CEP digitado:");
		Zend_Debug::dump($resultado, "Dados Retornados:");
		exit;
		
		switch($resultado['resultado']){  
		    case '2':  
		        $texto = " 
		    Cidade com logradouro único 
		    <b>Cidade: </b> ".$resultado['cidade']." 
		    <b>UF: </b> ".$resultado['uf']." 
		        ";    
		    break;  
		      
		    case '1':  
		        $texto = " 
		    Cidade com logradouro completo 
		    <b>Tipo de Logradouro: </b> ".$resultado['tipo_logradouro']." 
		    <b>Logradouro: </b> ".$resultado['logradouro']." 
		    <b>Bairro: </b> ".$resultado['bairro']." 
		    <b>Cidade: </b> ".$resultado['cidade']." 
		    <b>UF: </b> ".$resultado['uf']." 
		        ";  
		    break;  
		      
		    default:  
		        $texto = "Falha ao buscar cep: ".$resultado['resultado'];  
		    break;  
		}
		
		echo $texto;
		exit;
	}
	
	
	/**
	 * Método que recebe a resposta do webService para cep no formato JSON
	 */
	public function resultcepJsonAction(){
		
		$cep = $this->_getParam('cep');
		$webservice_url     = 'http://webservice.kinghost.net/web_cep.php';
		$webservice_query    = array(
		    'auth'    => '3122116928bb5f19cdead9865faef878', //Chave de autenticação do WebService - Consultar seu painel de controle
		    'formato' => 'query_string', //Valores possíveis: xml, query_string ou javascript
		    'cep'     => $cep //CEP que será pesquisado
		);
		
		//Forma URL
		$webservice_url .= '?';
		foreach($webservice_query as $get_key => $get_value){
		    $webservice_url .= $get_key.'='.urlencode($get_value).'&';
		}
		parse_str(file_get_contents($webservice_url), $resultado);

		$mensageiro = new Ead1_Mensageiro('Cep não encontrado!', Ead1_IMensageiro::AVISO);
		if($resultado){
			if($resultado['resultado']){
				$resultado = array_map("utf8_encode", $resultado);
				$mensageiro = new Ead1_Mensageiro($resultado, Ead1_IMensageiro::SUCESSO);
			} 
		}
		
		die($this->_helper->json($mensageiro));
		
	}
}
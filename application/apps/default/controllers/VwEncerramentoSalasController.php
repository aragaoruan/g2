<?php

/**
 * Classe de controller MVC para controlar os dados da Vw_EncerramentoSalas
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 *
 * @package application
 * @subpackage controller
 */
class Default_VwEncerramentoSalasController extends Ead1_ControllerRestrita
{

    /** @var SecretariaBO bo */
    private $bo;
    private $boSala;

    public function init()
    {
        parent::init();
        parent::setupDoctrine();
        $this->bo = new SecretariaBO();
        $this->boSala = new SalaDeAulaBO();
    }

    public function indexAction()
    {

    }

    public function salvarEncerramentoAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $arrEncerramento = $this->getParam('encerramento');

        $tipo_liberacao = $this->getParam('tipo_liberacao');
        switch ($tipo_liberacao) {
            case 'financeiro':
                $return = $this->bo->salvarArrEncerramentoSala_Financeiro($arrEncerramento);
                break;
            case 'coordenador':
                $return = $this->bo->salvarArrEncerramentoSala_Coordenador($arrEncerramento);
                break;
            case 'pedagogico':
                $return = $this->bo->salvarArrEncerramentoSala_Pedagogico($arrEncerramento);
                break;
            default:
                $return = '';
                break;
        }
        return $this->_helper->json($return);
    }

    public function retornarEncerramentoAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $tipo_liberacao = $this->getParam('tipo_liberacao');
        $id_categoriasala = $this->getParam('id_categoriasala');
        $id_tipodisciplina = $this->getParam('id_tipodisciplina');
        $id_professor = $this->getParam('id_professor');
        $bl_recusa = $this->getParam('bl_recusado', false);

        $arReturn = $this->boSala->retornarEncerramento($tipo_liberacao, $id_categoriasala, $id_tipodisciplina, $id_professor, $bl_recusa);
        $this->getResponse()->appendBody(Zend_Json::encode($arReturn));
    }

    public function retornarDadosDetalhamentoSalaAction()
    {
        $params = $this->getAllParams();
        $this->view->dadosDetalhe = $this->boSala->retornarDadosDetalhamentoSala($params['id_encerramentosala']);
    }

    public function retornarLinkInteracoesAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $intanceTO = $this->getParam('to');
        $to = new $intanceTO($this->getParam('obj'));
        $arReturn = $this->bo->gerarLinkResumoInteracoes($to);
        $this->getResponse()->appendBody(Zend_Json::encode($arReturn));
    }


    public function retornarLinkRelatorioAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $intanceTO = $this->getParam('to');
        $to = new $intanceTO($this->getParam('obj'));
        $arReturn = $this->bo->gerarLinkRelatorioMoodleEncerramento($to);
        $this->getResponse()->appendBody(Zend_Json::encode($arReturn));
    }
}

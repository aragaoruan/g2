<?php
/**
 * Controller to Alterar Negociacao
 *
 * @author Vinícius Alcântara <vinicius.alcantara@unyleya.com.br>
 * @since 2016-05-10
 * @package application
 * @subpackage controller
 */

class Default_AlterarVendaController extends Ead1_Controller
{
    /**
     * @var \G2\Negocio\Venda $negocio
     */
    private $_negocio;

    public function init()
    {
        parent::init();
        $this->_negocio = new \G2\Negocio\Venda();
    }

    public function indexAction()
    {
        echo $this->view->headLink()
            ->appendStylesheet('/css/auto-complete-pessoa.css');
        $id = $this->getParam('id');

        $alterarvenda = NULL;

        if ($id) {
            /** @var \G2\Entity\Venda $result */
            $result = $this->_negocio->findVenda($id);
            if ($result) {
                $alterarvenda = array(
                    'id_venda' => $result->getId_venda(),
                    'dt_cadastro' => $result->getDt_cadastro(),
                    'nu_descontoporcentagem' => $result->getNu_descontoporcentagem(),
                    'nu_descontovalor' => $result->getNu_descontovalor(),
                    'nu_juros' => $result->getNu_juros(),
                    'bl_ativo' => $result->getBl_ativo(),
                    'id_usuariocadastro' => $result->getId_usuariocadastro(),
                    'nu_valorliquido' => $result->getNu_valorliquido(),
                    'nu_valorbruto' => $result->getNu_valorbruto(),
                    'id_entidade' => $result->getId_entidade(),
                    'nu_parcelas' => $result->getNu_parcelas(),
                    'bl_contrato' => $result->getBl_contrato(),
                    'nu_diamensalidade' => $result->getNu_diamensalidade(),
                    'id_origemvenda' => $result->getId_origemvenda(),
                    'dt_agendamento' => $result->getDt_agendamento(),
                    'dt_confirmacao' => $result->getDt_confirmacao(),
                    'st_observacao' => $result->getSt_observacao(),
                    'nu_valoratualizado' => $result->getNu_valoratualizado(),
                    'id_campanhacomercial' => $result->getId_campanhacomercial() ? $result->getId_campanhacomercial()->getId_campanhacomercial() : NULL,
                    'id_evolucao' => $result->getId_evolucao() ? $result->getId_evolucao()->getId_evolucao() : NULL,
                    'id_situacao' => $result->getId_situacao() ? $result->getId_situacao()->getId_situacao() : NULL,
                    'id_usuario' => $result->getId_usuario() ? $result->getId_usuario()->getId_usuario() : NULL,
                    'id_tipocampanha' => $result->getId_tipocampanha() ? $result->getId_tipocampanha()->getId_tipocampanha() : NULL,
                    'id_atendente' => $result->getId_atendente() ? $result->getId_atendente()->getId_usuario() : NULL,
                    'st_atendente' => $result->getId_atendente() ? $result->getId_atendente()->getSt_nomecompleto() : NULL,
                    'id_campanhapontualidade' => $result->getId_campanhapontualidade() ? $result->getId_campanhapontualidade()->getId_campanhacomercial() : NULL,
                );
            }
        }
        $this->view->form = new CadastroRapidoPessoaForm();
        //monta o json para a view
        $this->view->alterarvenda = json_encode($alterarvenda);
    }
    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('alterar-venda/index.js');
    }

    public function alterarVendaAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $params = $this->limpaGetAllParams($this->getAllParams());
        $mensagem = $this->_negocio->salvarAlteracoesVenda($params);
        return $this->_helper->json($mensagem);
    }

}
<?php

/**
 * Description of FolhadeNumerosController
 *
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class Default_FolhaDeNumerosController extends Ead1_ControllerRestrita
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {

    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
    }

    /**
     * Retorna a propria entidade e sua filhas
     */
    public function retornaEntidadeAction()
    {
        $this->_negocio = new G2\Negocio\FolhadeNumeros();
        $where = array(
            'nu_entidadepai' => $this->_negocio->sessao->id_entidade,
        );
        $data = array();

        $result = $this->_negocio->findVwEntidadeRecursivaId($where);

        foreach ($result as $entity) {
            $toArray = $this->_negocio->toArrayEntity($entity);
            array_push($data, $toArray);
        }
        $this->_helper->json($data);
    }


    /**
     * Retorna a turma de acordo com a entidade selecionada
     */

    public function retornaTurmaAction()
    {
        $this->_negocio = new G2\Negocio\FolhadeNumeros();
        $where = array(
            'id_entidadecadastro' => $this->getParam('id')
        );
        $data = array();

        $result = $this->_negocio->retornaTurmaEntidade($where);
        foreach ($result as $entity) {
            $toArray = $this->_negocio->toArrayEntity($entity);
            array_push($data, $toArray);
        }

        $this->_helper->json($data);
    }

    public function retornaRelatorioAction()
    {
        $this->negocio = new G2\Negocio\FolhadeNumeros();
        $parametros = $this->limpaGetAllParams($this->getAllParams());
        $chave = $this->retornaDados($parametros);
        $arrCoisa = [];
        foreach ($chave as $arr) {
            $arrCoisa[] = $arr;
        }
        $this->_helper->json($arrCoisa);
    }

    public function gerarXlsAction()
    {
        $this->negocio = new G2\Negocio\FolhadeNumeros();
        $parametros = $this->limpaGetAllParams($this->getAllParams());
        $chave = $this->retornaDados($parametros);
        $this->view->dados = $chave;
        $this->view->tipo = $parametros['tipo'];

    }

    public function retornaDados($parametros){
        $chave = array();

        $resultado = $this->negocio->executaRelatorio($parametros);

        foreach ($resultado as $result) {
            if (array_key_exists($result['id_areaconhecimento'], $chave)) {
                array_push($chave[$result['id_areaconhecimento']]['produtos'], $result);
            } else {
                $chave[$result['id_areaconhecimento']] = array(
                    'id_areaconhecimento' => $result['id_areaconhecimento'],
                    'st_areaconhecimento' => $result['st_areaconhecimento'],
                    'dia_1' =>  (new Zend_Date($result['dt_atual'], null, 'pt-BR'))->sub('0',Zend_Date::DAY, 'pt-br')->toString('dd/MM/YYYY'),
                    'dia_2' =>  (new Zend_Date($result['dt_atual'], null, 'pt-BR'))->sub('1',Zend_Date::DAY, 'pt-br')->toString('dd/MM/YYYY'),
                    'dia_3' =>  (new Zend_Date($result['dt_atual'], null, 'pt-BR'))->sub('2',Zend_Date::DAY, 'pt-br')->toString('dd/MM/YYYY'),
                    'dia_4' =>  (new Zend_Date($result['dt_atual'], null, 'pt-BR'))->sub('3',Zend_Date::DAY, 'pt-br')->toString('dd/MM/YYYY'),
                    'dia_5' =>  (new Zend_Date($result['dt_atual'], null, 'pt-BR'))->sub('4',Zend_Date::DAY, 'pt-br')->toString('dd/MM/YYYY'),
                    'dia_6' =>  (new Zend_Date($result['dt_atual'], null, 'pt-BR'))->sub('5',Zend_Date::DAY, 'pt-br')->toString('dd/MM/YYYY'),
                    'produtos' => array(
                        0 => $result
                    )
                );
            }
        }

        return $chave;
    }


}
<?php

class Default_DocumentosController extends Ead1_ControllerRestrita

{

    private $_bo;
    private $_secretariaBO;

    public function init()
    {
        parent::init();
        parent::setupDoctrine();
        $this->_bo = new SecretariaBO();
        $this->_secretariaBO = new SecretariaBO();
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('documentos/index.js');
    }

    public function indexAction()
    {
    }

    public function getTipoDocumentoAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        $negocio = new \G2\Negocio\TipoDocumento();
        $mensageiro = $negocio->findAll();

        array_unshift($mensageiro, array(
            'id_tipodocumento' => '',
            'st_tipodocumento' => '[Selecione]'
        ));

        echo json_encode($mensageiro);
    }

    public function getObrigatoriedadeAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        $negocio = new \G2\Negocio\DocumentosObrigatoriedade();
        $mensageiro = $negocio->findAll();

        array_unshift($mensageiro, array(
            'id_documentosobrigatoriedade' => '',
            'st_documentosobrigatoriedade' => '[Selecione]'
        ));

        echo json_encode($mensageiro);
    }

    public function getCategoriaAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        $negocio = new \G2\Negocio\DocumentosCategoria();
        $mensageiro = $negocio->findAll();

        array_unshift($mensageiro, array(
            'id_documentoscategoria' => '',
            'st_documentoscategoria' => '[Selecione]'
        ));

        echo json_encode($mensageiro);
    }

    public function getSituacaoAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        $negocio = new \G2\Negocio\Situacao();
        $mensageiro = $negocio->retornaSituacaoFindBy(array('st_tabela' => 'tb_documentos'));

        array_unshift($mensageiro->mensagem, array(
            'id_situacao' => '',
            'st_situacao' => '[Selecione]'
        ));

        echo json_encode($mensageiro->mensagem);
    }

    public function getUtilizacaoAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        $negocio = new \G2\Negocio\DocumentosUtilizacao();
        $mensageiro = $negocio->findAll();

        echo json_encode($mensageiro);
    }

    public function projetosPedagogicosAction($idDocumentos = false) {
        $this->_helper->viewRenderer->setNoRender(true);

        $negocio = new \G2\Negocio\Negocio();

        $to = new VwProdutoProjetoTipoValorTO();
        $to->setId_entidade($negocio->sessao->id_entidade); // pega a entidade da sessão

        $bo = new ProjetoPedagogicoBO();
        $mensageiro = $bo->retornarVwProdutoProjetoTipoValor($to);


        echo json_encode($mensageiro->mensagem);
    }

}


<?php
/**
 * Classe de controller MVC para controlar locais de aula
 * @author paulo.silva <paulo.silva@unyleya.com.br>
 * 
 * @package application
 * @subpackage controller
 */

class Default_LocalAulaController extends Ead1_ControllerRestrita {
    
    public function init() {
        parent::init();
        parent::setupDoctrine();
    }

    public function jsAction(){ 
        $this->_helper->viewRenderer->setNoRender(true);
        echo   $this->view->render('local-aula/index.js');
    }
    
    public function indexAction() {
        
        //$this->view->locais = $this->em->getRepository('G2\Entity\LocalAula')->findAll();
        
    }
}
<?php

/**
 * Classe de controller MVC para controlar locais de aula
 * @author paulo.silva <paulo.silva@unyleya.com.br>
 * 
 * @package application
 * @subpackage controller
 */
class Default_MenuController extends Ead1_ControllerRestrita {

    public function init() {
        parent::init();
        parent::setupDoctrine();
    }

    public function jsAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('menu/index.js');
    }

    public function jsonAction() {
	Zend_Session::start();
    	$sessao = Zend_Session::namespaceGet('geral');
    	
    	try {
    			
    		$loginBO = new LoginBO();
    	
    		if(isset($sessao['id_entidade']) && isset($sessao['id_usuario']) && isset($sessao['id_perfil'])) {
    			$selecionarPerfilTO = new SelecionarPerfilTO();
    			$selecionarPerfilTO->setId_entidade($sessao['id_entidade']);
    			$selecionarPerfilTO->setId_usuario($sessao['id_usuario']);
    			$selecionarPerfilTO->setId_perfil($sessao['id_perfil']);
    			$perfil = $loginBO->selecaoPerfil($selecionarPerfilTO);
    			
    		} else {
    			$this->view->perfil = false;
    			$this->_forward('login','index');
    			$mensageiro = new Ead1_Mensageiro("Usuário não está logado",Ead1_IMensageiro::ERRO);
    		}
    	} catch(Zend_Exception $e) {
    	
    	}
       
        $arr = array();
        
        foreach($perfil->menuprincipal as $menu) {
           $arr[] = new MenuItem($menu);
        }

        $this->_helper->json($arr);
        
        
    }

    public function indexAction() {
Zend_Session::start();
    	$sessao = Zend_Session::namespaceGet('geral');
    	
    	try {
    			
    		$loginBO = new LoginBO();
    	
    		if(isset($sessao['id_entidade']) && isset($sessao['id_usuario']) && isset($sessao['id_perfil'])) {
    			$selecionarPerfilTO = new SelecionarPerfilTO();
    			$selecionarPerfilTO->setId_entidade($sessao['id_entidade']);
    			$selecionarPerfilTO->setId_usuario($sessao['id_usuario']);
    			$selecionarPerfilTO->setId_perfil($sessao['id_perfil']);
    			$perfil = $loginBO->selecaoPerfil($selecionarPerfilTO);
    			
    		} else {
    			$this->view->perfil = false;
    			$this->_forward('login','index');
    			$mensageiro = new Ead1_Mensageiro("Usuário não está logado",Ead1_IMensageiro::ERRO);
    		}
    	} catch(Zend_Exception $e) {
    	
    	}
       
        $arr = array();
        
        foreach($perfil->menuprincipal as $menu) {
           $arr[] = new MenuItem($menu);
        }
        
        $this->view->menuData = json_encode($arr);
        
    }

}

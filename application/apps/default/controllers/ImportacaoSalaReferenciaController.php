<?php
/**
 * Created by PhpStorm.
 * User: alex.alexandre <alex.alexandre@unyleya.com.br>
 * Date: 25/10/2017
 * Time: 16:53
 */

/**
 * Class Default_ImportacaoSalaReferenciaController
 *
 * Essa classe é responsavel por fazer o controle completo do fluxo
 * de importação das salas de referencias entre um moodle e outro
 */
class Default_ImportacaoSalaReferenciaController extends Ead1_Controller
{

    private $disciplina;

    public function init()
    {
        parent::init();
        $this->disciplina = new \G2\Negocio\Disciplina;
    }

    public function indexAction()
    {

    }

    /**
     * Essa função recebe os dados via post para repassar para as outras funções responsáveis
     * Por fazer o tratamento do excel e de validar as informações
     *
     * @throws Exception
     */
    public function recebeDadosAction()
    {
        try {
            $this->_helper->viewRenderer->setNoRender(true);

            /**
             * Recebe o arquivo via post, envia para a função responsável por fazer o tratamento
             * E cria um array na variavel '$salasReferencia' com o conteúdo do excel
             */
            $inputFileName = $_FILES['arquivoXLS']['tmp_name'];
            $salasReferencia = array_filter($this->tratarExcel($inputFileName));

            $dados = array(
                'moodleOrigem' => $this->getRequest()->getParam('select-moodle-origem'),
                'moodleDestino' => $this->getRequest()->getParam('select-moodle-destino'),
                'salasT' => array_filter($salasReferencia)
            );

            $valido = $this->disciplina->validaDados($dados);

            if ($valido) {
                $salasI = $this->disciplina->importarSalaTranferenia($dados);

                if ($salasI) {
                    $this->getResponse()
                        ->appendBody(Zend_Json::encode($salasI))
                        ->setHttpResponseCode(200);
                }

            }

            $this->getResponse()
                ->setHttpResponseCode(400);

        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Essa função recebe um excel, faz o tratamento adequado e retorna em formato de array
     *
     * @param $inputFileName
     * @return array
     */
    private function tratarExcel($inputFileName)
    {
        //Carregamento do arquivo inicial e load para a verificação
        $inputFileType = \PHPExcel_IOFactory::identify($inputFileName);
        $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
        $objReader->setReadDataOnly();
        $objPHPExcel = $objReader->load($inputFileName);
        $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

        $maxCell = $objPHPExcel->getActiveSheet()->getHighestRowAndColumn();

        $sheetContentData = $objPHPExcel->getActiveSheet()->rangeToArray('A1:' . $maxCell['column'] . $maxCell['row']);
        $sheetContentData = array_filter($sheetContentData);
        return $sheetContentData;
    }

}

<?php

/**
 * Controller to Pessoa
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2013-12-11
 * @package application
 * @subpackage controller
 * @update Denise Xavier <denise.xavier@unyleya.com.br> in 30/12/2014
 */
use G2\Negocio\Pessoa;


class Default_RelacaoEntidadeController extends Ead1_ControllerRestrita
{

    private $_negocio;
    private $_negocioEntidade;

    public function init()
    {
        parent::init();
        $this->_negocio = new Pessoa();
        $this->_negocioEntidade = new \G2\Negocio\Entidade();
    }

    /**
     * Action to js backbone
     */
    public function indexJsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render($this->_getParam('controller') . '/index.js');
    }

    public function indexAction()
    {
        /*$this->_helper->viewRenderer->setNoRender();
        echo $this->view->render($this->_getParam('controller') . '/index.js');*/

    }

    // Recupera todos as entidades para montar o combo com as Unidades
    public function pesquisarEntidadeFilhaAction() {
        //$result = $this->_negocioEntidade->pesquisarEntidadeFilha();
        /* $bo  = new PesquisarBO();
         $to = new VwEntidadeClasseTO();
        // $result = $bo->verificarEntidadeFilha(array('id_entidadepai' => $to->getSessao()->id_entidade));
         $to->setId_entidadepai($to->getSessao()->id_entidade);
         $result = $bo->verificarEntidadeFilha($to);*/
        $ro = new LoginRO();
        $result =  $ro->entidadeFilhas();
        $this->_helper->json($result->getMensagem());
    }

    // Preenche o combo com a Relação que a unidade tem com o Projeto pedagógico
    public function retornaRelacoesAction() {
        $bo = new EntidadeBO();
        $unidade = $this->_getParam('unidade_id');
        $eTO = new EntidadeClasseTO();
        $eTO->setId_entidadeclasse($unidade);
        $this->_helper->json($bo->retornaClasses($eTO)->getMensagem());
    }

    // Retorna os projetos pedagógicos cadastrados
    public function retornarProjetoPedagogicoAction() {
        $eTO = new EntidadeTO();
        $bo = new ProjetoPedagogicoBO();
        $ppTO = new ProjetoPedagogicoTO();

        $this->_helper->json($bo->retornarProjetoPedagogico($ppTO,$eTO)->getMensagem());
    }

    // Retorna os projetos pedagógicos p/ unidade/entidade específica
    public function retornarProjetoPedagogicoDaUnidadeAction() {
        $unidade = $this->_getParam('unidade_id');
        $peTO = new ProjetoEntidadeTO();
        $peTO->setId_entidade($unidade);
        $bo = new EntidadeBO();

        // Pode ser que dê pau se vier só 1 item? Transformar em Array se getMensagem vier um hash simples!?
        $this->_helper->json($bo->retornarProjetoEntidade($peTO)->getMensagem());
    }

    // Salva as alterações na Relação do Projeto Pedagógico
    public function salvarEntidadeRelacaoAction() {
        $params = $this->getAllParams();

        // SetUp EntidadeRelacao
        $erTO = new EntidadeRelacaoTO();
        $erTO->setId_entidade($params['entidade']);
        $erTO->fetch(false, true, true);

        // SetUp projs pedagogicos
        $projs_pedagogico = array();
        if (isset($params['projetos_pedagogicos']) && $params['projetos_pedagogicos']) {
            foreach ($params['projetos_pedagogicos'] as $projeto_pedagogico) {
                $ppTO = new ProjetoPedagogicoTO();
                $ppTO->setId_projetopedagogico($projeto_pedagogico);
                array_push($projs_pedagogico, $ppTO);
            }
        }

        // Operador instance
        $eRO = new EntidadeRO();

        // Se existir entidade pai edita relação
        if ($erTO->getId_entidadepai())
            return $this->_helper->json($eRO->editarEntidadeRelacao($erTO, $projs_pedagogico)->getMensagem());

        return $this->_helper->json($eRO->cadastrarEntidadeRelacao($erTO, $projs_pedagogico)->getMensagem());
    }
}
<?php

/**
 * Classe de controller inicial do sistema
 * @author eder.mariano
 *
 * @package application
 * @subpackage controller
 */
class Default_IndexController extends Zend_Controller_Action
{

    protected $doctrineContainer;


    public function jsAction ()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('index/dashboard.js');
    }

    public function dashboardAction() {
        $this->_helper->viewRenderer('dashboard');
    }

    /**
     * Método que renderiza a view inicial do sistema
     */
    public function indexAction()
    {
        Zend_Session::start();
        $this->_helper->layout->setLayout('layout');
        $sessao = Zend_Session::namespaceGet('geral');

        try {

            $loginBO = new LoginBO();

            if (isset($sessao['id_entidade']) && isset($sessao['id_usuario']) && isset($sessao['id_perfil'])) {
                $selecionarPerfilTO = new SelecionarPerfilTO();
                $selecionarPerfilTO->setId_entidade($sessao['id_entidade']);
                $selecionarPerfilTO->setId_usuario($sessao['id_usuario']);
                $selecionarPerfilTO->setId_perfil($sessao['id_perfil']);
                $perfil = $loginBO->selecaoPerfil($selecionarPerfilTO);
                $this->view->perfil = $perfil;

                $this->view->sessaoUsuario = Zend_Session::namespaceGet('geral');

                $arr = array();
                foreach ($perfil->menuprincipal as $menu) {
                    // Nao listar menus (principais) sem filhos vazios
                    if (!$menu->filhos)
                        continue;
                    else {
                        // Verifica tambem se os submeus tem filhos
                        foreach ($menu->filhos as $kf => $filho) {
                            if (!$filho->filhos) {
                                unset($menu->filhos[$kf]);
                            }
                        }
                    }
                    $arr[] = new MenuItem($menu);
                }

                $this->view->menuData = json_encode($arr);
            } else {
                $this->view->perfil = false;
                $this->forward('login', 'index');
                $mensageiro = new Ead1_Mensageiro("Usuário não está logado", Ead1_IMensageiro::ERRO);
            }
        } catch (Zend_Exception $e) {
        //TODO: whats the point of catching an exception if you won't do shit about it?
        }
    }

    public function loginAction()
    {
        $this->_helper->layout->setLayout('layout');
    }

    public function logoutAction()
    {
        $ro = new LoginRO();
        $ro->sair();
        $this->_helper->redirector('index');
    }

    public function authAction()
    {
        $request = new Zend_Controller_Request_Http();

        if ($request->isPost()) {
            $bo = new LoginBO();
            $to = new LoginTO();

            $username = $request->getPost('username');
            $password = $request->getPost('password');

            $to->setUsuario($username);
            $to->setSenha($password);

            $this->view->entidades = $bo->logar($to);

            if ($this->getRequest()->isXmlHttpRequest()) {
                $arrEntidades = $this->ordenaArrayEntidades($this->view->entidades->getMensagem());
                $this->view->entidades->setMensagem($arrEntidades);
                $this->_helper->json($this->view->entidades);
            } else {
                $this->forward('index');
            }
        } else {
            $this->redirect('/');
        }
    }

    /**
     * Recebe um array com as entidades e perfils e monta o array ordenado
     * @param array $entidades
     * @return array
     */
    private function ordenaArrayEntidades(array $entidades)
    {
        $arrEntidades = array();
        foreach ($entidades as $entidade) {
            //verifica se a imagem existe no diretório
            if (isset($entidade['st_urllogoentidade']) && !file_exists(realpath(APPLICATION_PATH . '/../public/' . $entidade['st_urllogoentidade']))) {
                $entidade['st_urllogoentidade'] = '/upload/entidade/entidade_foto_generica.jpg';
            }
            $arrEntidades[] = $entidade;
        }
        return $arrEntidades;
    }

    public function loginDiretoAction(){

        try {

            $st_token = $this->_getParam('token', false);
            $hash     = $this->_getParam('hash', false);
            if(!$st_token)
                throw new \Exception("Token inválido");

            $bo = new LoginBO();
            $mensageiro = $bo->logarPorToken($st_token);

            if($mensageiro->getTipo()==\Ead1_IMensageiro::SUCESSO){

                if(!$hash){
                    $this->_helper->redirector('index');
                } else {
                    $url = $this->_helper->url->url(array('module' => 'default',
                        'controller' => 'index', 'action' => 'index'));
                    $this->_helper->redirector->gotoUrl($url.'#'.$hash);
                }

            } else {
                die($mensageiro->getFirstMensagem());
            }

        }catch (\Exception $e){
            die($e->getMessage());
        }

    }

    public function changeProfileAction ()
    {

        Zend_Session::start();

        $this->_helper->layout->setLayout('layout');

        $bo = new LoginBO();

        if (!$bo->usuarioLogado()) {
            $this->redirect('/');
        }

        $this->view->entidades = $bo->pegarPerfisUsuarioLogado();

        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->json($this->view->entidades->mensagem);
        }
//$this->_helper->redirector('auth','index');
    }

    public function selectProfileAction()
    {
        //Zend_Session::start();
        //$session = Zend_Session::namespaceGet('geral');
        $id_perfil = $this->_getParam('id_perfil');
        $id_entidade = $this->_getParam('id_entidade');
        $id_usuario = \Ead1_Sessao::getSessaoGeral()->id_usuario;


        if (!$id_usuario) {
            $this->redirect('/');
        }

        $to = new SelecionarPerfilTO();
        $to->setId_entidade($id_entidade);
        $to->setId_perfil($id_perfil);
        $to->setId_usuario($id_usuario);
        $to->setId_linhadenegocio(null);

        $bo = new LoginBO();
        $result = $bo->selecaoPerfil($to);

        $arr = array();

        foreach ($result->menuprincipal as $menu) {
            $arr[] = new MenuItem($menu);
        }
        if ($this->getRequest()->isXmlHttpRequest()) {
            $result->menuprincipal = $arr;
            $this->_helper->json($result);
        } else {
            $this->_helper->redirector('index');
        }
    }

    public function checksessionAction()
    {
        Zend_Session::start();

        $bo = new LoginBO();
        if (array_key_exists('geral', $_SESSION))
            $geral = $_SESSION['geral'];
        else
            $geral = array();

        /**
         * Alteracao por Rafael Bruno (RBD) em 10/12/2014, para evitar warning
         * de chave de array nao existente.
         */
        if ((boolean)\Ead1_Sessao::getSessaoGeral()->id_usuario) {
            $dados['usuarioLogado'] = true;
            $dados['entidades'] = $bo->pegarPerfisUsuarioLogado()->mensagem;

            $usuario = $geral;

            if ($usuario) {
                $usuario['nomeUsuario'] = utf8_encode($geral['nomeUsuario']);

                if(!empty($usuario['st_nomeperfil'])){
                    $usuario['st_nomeperfil'] = utf8_encode($geral['st_nomeperfil']);
                }
            }
            if(isset($usuario['id_entidade'])) {
                $usuario['id_entidade'] = $geral['id_entidade'];
                $usuario['st_nomeentidade'] = $dados['entidades'][$geral['id_entidade']]['st_nomeentidade'];
            }

            $dados['usuario'] = $usuario;

            if (isset($usuario['id_entidade']) && isset($usuario['id_usuario']) && isset($usuario['id_perfil'])) {
                $selecionarPerfilTO = new SelecionarPerfilTO();
                $selecionarPerfilTO->setId_entidade($usuario['id_entidade']);
                $selecionarPerfilTO->setId_usuario($usuario['id_usuario']);
                $selecionarPerfilTO->setId_perfil($usuario['id_perfil']);
                $perfil = $bo->selecaoPerfil($selecionarPerfilTO);
                $this->view->perfil = $perfil;
                $arr = array();
                foreach ($perfil->menuprincipal as $menu) {
                    $arr[] = new MenuItem($menu);
                }
                $dados['menu'] = $arr;
            } else {
                $dados['menu'] = false;
            }
        } else {
            $dados['usuarioLogado'] = false;
        }

        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->json($dados);
        } else {
            die('<pre>' . print_r($dados, true) . '</pre>');
        }
    }

    /**
     * @deprecated
     */
    public function antigoDadosAcesso()
    {

        try {

            $idUsuarioEncode = $this->getRequest()->getParam('q');
            $idEntidadeEncode = $this->getRequest()->getParam('e');
            $dataHojeEncode = $this->getRequest()->getParam('d');
            $idUsuario = base64_decode($idUsuarioEncode);
            $idEntidade = base64_decode($idEntidadeEncode);
            $dataHoje = base64_decode($dataHojeEncode);
            Zend_Debug::dump($idUsuario);
            Zend_Debug::dump($idEntidade);
            Zend_Debug::dump($dataHoje);
            Zend_Debug::dump($this->getAllParams());
            exit;
            if ($dataHoje != date('Y.m.d')) {
                throw new DomainException('A data de validade para recuperação de senha expirou. Por favor solicite novamente a sua instituição.');
            }

            $loginBO = new LoginBO();
            $pessoaBO = new PessoaDAO();
            $dadosEmailROW = $pessoaBO->retornaEmailPerfil($idUsuario, $idEntidade);

            $mensagemFila = new Ead1_Email_Fila_Mensagem($idEntidade, $idUsuario);

            $mensagemFila->addDestinatario($dadosEmailROW->st_email, $dadosEmailROW->st_nomecompleto)
                ->setAssunto('Dados Pessoais')
                ->carregaMensagemPadrao(MensagemPadraoTO::DADOS_ACESSO, array('id_entidade' => $idEntidade, 'id_usuario' => $idUsuario));

            $fila = new Ead1_Email_Fila();
            $fila->addMensagem($mensagemFila, 2);

            if ($fila->salvar(true) === false) {
                throw new DomainException('Não foi possível enviar o e-mail com os seus dados pessoais.');
            }

            $this->view->mensagem = 'Seus dados de acesso foram encaminhados para seu endereço de e-mail';
        } catch (DomainException $domainExc) {
            $this->view->mensagem = $domainExc->getMessage();
        } catch (Exception $exc) {
            Zend_Debug::dump($exc);
            $this->view->mensagem = 'Erro ao tentar recuperar a senha do usuário. Por favor tente novamente';
        }
    }

    /**
     * Action do Formulário de Recuperação de Senha
     */
    public function esqueciminhasenhaAction()
    {

    }

    /**
     * Action que valida a recuperação de senha
     */
    public function validarecuperacaosenhaAction()
    {
        try {
            $mensageiro = new Ead1_Mensageiro();
            if (!$this->_hasParam('email') && (!$this->hasParam('cpf') && !$this->hasParam('login'))) {
                THROW new Zend_Validate_Exception("É Necessário Informar o Email e Outro Campo para Recuperar a Senha.");
            }
            $vwPessoaTO = new VwPessoaTO();
            $vwPessoaTO->setSt_login($this->_getParam('login') ? $this->_getParam('login') : null);
            $vwPessoaTO->setSt_cpf($this->_getParam('cpf') ? $this->_getParam('cpf') : null);
            $vwPessoaTO->setSt_email($this->_getParam('email') ? $this->_getParam('email') : null);
            $loginBO = new LoginBO();
            $mensageiro = $loginBO->recuperarSenhaPessoa($vwPessoaTO);
        } catch (Zend_Exception $e) {
            $mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        $this->resposta($mensageiro);
    }

    /**
     * Action de Recuperação de Senha
     * - param q -> id_usuario em base 64
     * - param e -> id_entidade em base 64
     * - param d -> string de data de autenticação em base 64
     */
    public function recuperarSenhaAction()
    {
        try {
            $loginBO = new LoginBO();
            $idUsuarioEncode = $this->getRequest()->getParam('q');
            $idEntidadeEncode = $this->getRequest()->getParam('e');
            $dataAutenticacaoEncode = $this->getRequest()->getParam('d');
            $idUsuario = (int)base64_decode($idUsuarioEncode);
            $idEntidade = (int)base64_decode($idEntidadeEncode);
            $dataAutenticacao = base64_decode($dataAutenticacaoEncode);
            $loginBO->validaParamsRecuperarSenha($dataAutenticacao, $idUsuario, $idEntidade);
            $mensagemBO = new MensagemBO();
            $pessoaTO = new PessoaTO();
            $pessoaTO->setId_entidade($idEntidade);
            $pessoaTO->setId_usuario($idUsuario);
            $mensagemBO->procedimentoGerarEnvioMensagemPadraoEntidade(MensagemPadraoTO::DADOS_ACESSO, $pessoaTO, TipoEnvioTO::EMAIL, true)
                ->subtractMensageiro();
            $this->view->mensagem = "Fique de Olho na sua Caixa de Entrada, o Email com seus Dados de Acesso Deve Chegar em Breve.";
        } catch (Zend_Validate_Exception $e) {
            $this->view->mensagem = $e->getMessage();
        } catch (Zend_Exception $e) {
            $this->view->mensagem = "Erro ao Recuperar os Dados de Acesso.";
        }
    }

    public function geraEntidadesAction()
    {

    }

}

<?php

/**
 * Classe de controller MVC para controlar a Integração Externa
 * @author Felipe Pastor
 *
 * @package default
 * @subpackage controller
 */
class Default_IntegracaoExternaController extends Ead1_ControllerRestrita
{

    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new G2\Negocio\IntegracaoExterna();
        $this->_helper->viewRenderer->setNoRender();
    }

    public function indexAction()
    {

    }

    public function integracaoPontosoftAction()
    {
//        return $this->negocio->integrarPontoSoft();
    }

}
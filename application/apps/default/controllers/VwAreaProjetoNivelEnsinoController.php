<?php

/**
 * Classe de controller MVC para controlar os dados da Vw_AreaProjetoNivelEnsino
 * @author Elcio Guimarães <elcioguimaraes@gmail.com>
 *
 * @package application
 * @subpackage controller
 */
class Default_VwAreaProjetoNivelEnsinoController extends Ead1_ControllerRestrita
{

    public function init()
    {
        parent::init();
        parent::setupDoctrine();
    }

    public function jsAction()
    {
//         $this->_helper->viewRenderer->setNoRender(true);
//         echo   $this->view->render('vw-texto-sistema/index.js');
    }

    public function indexAction()
    {

    }

    public function pesquisarAction()
    {

        try {

            $apneTO = new VwAreaProjetoNivelEnsinoTO($this->limpaGetAllParams($this->_getAllParams()));
            $apneTO->setSt_projetopedagogico(utf8_decode($apneTO->getSt_projetopedagogico()));

            $ro = new ProjetoPedagogicoRO();
            $mretorno = $ro->retonarAreaProjetoNivelEnsino($apneTO);
            if ($mretorno->getTipo() == Ead1_IMensageiro::SUCESSO) {

                $arrayprojetos = array();
                foreach ($mretorno->getMensagem() as $to) {
                    $arrayprojetos[] = $to->toArray();
                }

                $retorno = new \Ead1_Mensageiro($arrayprojetos, \Ead1_IMensageiro::SUCESSO);
                $retorno->setText(count($arrayprojetos) . " Projetos encontrado(s)");
            } else {
                $retorno = $mretorno;
            }


        } catch (Exception $e) {
            $retorno = new \Ead1_Mensageiro('Erro ao Pesquisar - ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        die(json_encode($retorno));


    }

}


<?php

/**
 * Class Controller OcorrenciaAluno
 * @author Denise Xavier  <denise.xavier@unyleya.com.br>
 */
class Default_OcorrenciaAlunoController extends Ead1_ControllerRestrita
{

    private $_negocio;

    public function init()
    {
        parent::init();
        $this->_negocio = new G2\Negocio\Ocorrencia();
    }

    /**
     * Action index
     */
    public function indexAction()
    {
        echo $this->view->headScript()
            ->appendScript($this->view->render('/entrega-documentos/index.js'));
    }

    /**
     * Carrega o script para funcionalide de ocorrencia de alunos
     * @return javascript
     */
    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('ocorrencia-aluno/index.js');
    }


    /**
     * Action responsavel por retornar assuntos para abertura de ocorrências de alunos
     * @param json data
     * @return json data
     */
    public function findAssuntoAction()
    {
        $dataArray = $this->_negocio->findAssuntoAluno(array('id_tipoocorrencia' => $this->getParam('id_tipoocorrencia')));
        array_unshift($dataArray, array("id_assuntoco" => "", "st_assuntoco" => '- Selecione assunto -'));
        $this->_helper->json($dataArray);
    }
}

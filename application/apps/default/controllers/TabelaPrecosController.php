<?php

/**
 * Controller API para alterar Tabela de Preços
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 * @since  08/01/2015
 * @package application
 * @subpackage controller
 */
class Default_TabelaPrecosController extends Ead1_ControllerRestrita
{

    /**
     * @var \G2\Negocio\ConfiguracaoEntidade $negocio
     */
    private $negocio;

    public function init() {
        parent::init();
        $this->negocio = new \G2\Negocio\ConfiguracaoEntidade();
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('tabela-precos/index.js');
    }

    public function indexAction() {
    }

    public function uploadAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        if (isset($_FILES['file-0']['name'])) {
            $arquivo = $_FILES['file-0'];

            // Validar extensao do arquivo
            $fileExtension = strtolower(pathinfo($arquivo['name'], PATHINFO_EXTENSION));
            $extensions = array('pdf', 'jpg', 'jpeg', 'png');

            $error = array();

            if ($arquivo['size'] > 20971520) {
                $error['error'][] = array(
                    'title' => 'Tamanho do arquivo!',
                    'text' => 'O tamanho do arquivo excede o permitido, Somente serão aceitos arquivos menores de 20MB!',
                    'type' => 'error'
                );
            } else if (!in_array($fileExtension, $extensions)) {
                $error['error'][] = array(
                    'title' => 'Formato do arquivo!',
                    'text' => 'O formato do arquivo não é permitido, Somente serão aceitas extensões: PDF, JPG e PNG!',
                    'type' => 'error'
                );
            }

            if ($error) {
                $this->_helper->json($error);
            } else {
                $mensageiro = $this->negocio->salvarAnexoTabelaPreco($arquivo);
                $this->_helper->json($mensageiro->getCodigo());
            }

        }
    }


}

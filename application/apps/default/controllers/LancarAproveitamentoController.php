<?php

/**
 * @author Helder Silva <helder.silva@unyleya.com.br> 2016-01-22
 * @package application
 * @subpackage controller
 */
class Default_LancarAproveitamentoController extends Ead1_ControllerRestrita
{

    /**
     * @var \G2\Negocio\Ocorrencia $negocio
     */
    private $negocio;

    /**
     * @var \G2\Negocio\Aproveitamento $_negocio
     */
    private $_negocio;

    public function init()
    {
        parent::init();
        parent::setupDoctrine();
        $this->negocio = new \G2\Negocio\Ocorrencia();
        $this->_negocio = new \G2\Negocio\Aproveitamento();
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        echo $this->view->render('lancar-aproveitamento/index.js');
        echo $this->view->render('lancar-aproveitamento/disciplina-origem.js');
    }

    public function indexAction()
    {
        echo $this->view->headLink()
            ->appendStylesheet('/css/auto-complete-pessoa.css');
        $this->view->lancarAproveitamento = '';

        $this->view->form = new CadastroRapidoPessoaForm();
    }

    /**
     * Retorna a ocorrencia formatada.
     */
    public function retornaOcorrenciaByMatriculaAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocio->retornaOcorrencia($params, true);
        foreach ($result as $key => $value) {
            $result[$key]['st_tituloOcorrencia'] = $value['id_ocorrencia'] . ' - ' . $value['st_titulo'];
            $result[$key]['id'] = $value['id_ocorrencia'];
        }
        $this->_helper->json($result);
    }

    /**
     * Retorna as ocorrencias relacionadas formatadas
     */
    public function retornaOcorrenciasRelacionadasAproveitamentoAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocio->retornaOcorrenciasRelacionadasAproveitamento($params);
        $arrRetorno = [];
        foreach ($result as $key => $value) {
            $arrRetorno[$key] = $this->negocio->toArrayEntity($value);
            $arrRetorno[$key]['st_tituloOcorrencia'] = $value->getId_ocorrencia() . ' - ' . $value->getSt_titulo();
            $arrRetorno[$key]['id'] = $value->getId_ocorrencia();
            $arrRetorno[$key]['editable'] = false;
        }
        $this->_helper->json($arrRetorno);
    }

    /**
     * Formata os dados das disciplinas e módulos retornados de acordo com o formato exigido nas composites do
     * quadro de equivalência de disciplinas
     */
    public function retornaModulosDisciplinasAproveitamentoAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $retorno = $this->_negocio->retornaModulosDisciplinasAproveitamento($params['data']);
        $arr = [];
        $arrRetorno = [];
        foreach ($retorno as $item) {
            if (array_key_exists($item['id_modulo'], $arrRetorno)) {
                array_push($arrRetorno[$item['id_modulo']]['disciplinas'], $item);
            } else {
                $arr['id'] = $item['id_modulo'];
                $arr['nome'] = $item['st_modulo'];
                $arr['disciplinas'] = array();
                array_push($arr['disciplinas'], $item);
                $arrRetorno[$item['id_modulo']] = $arr;
            }
        }
        $this->_helper->json(array_values($arrRetorno));
    }

    /**
     * Salva o aproveitamento e suas ocorrencias.
     */
    public function saveAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $retorno = $this->_negocio->saveAproveitamento($params);
        $this->_helper->json($retorno);
    }
}

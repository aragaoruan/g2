<?php

/**
 * Classe de controller MVC para controlar cadastros de novos concursos
 * @author Helder Fernandes Silva <helder.silva@unyleya.com.br>
 * @since 2014-10-02
 * @package application
 * @subpackage controller
 */
Class Default_ConcursoController extends Ead1_ControllerRestrita
{
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Concurso();
    }

    public function indexAction()
    {
        $id = $this->_getParam('id');
        $return = null;
        if ($id) {
            $resultado = $this->negocio->retornaConcurso($id);
            $resultadoArr = $this->negocio->toArrayEntity($resultado);
            $resultadoArr['dt_prova'] = $resultado->getDt_prova() ? $resultado->getDt_prova()->format('d/m/Y') : NULL;
            $resultadoArr['dt_inicioinscricao'] = $resultado->getDt_inicioinscricao() ? $resultado->getDt_inicioinscricao()->format('d/m/Y') : NULL;
            $resultadoArr['dt_fiminscricao'] = $resultado->getDt_fiminscricao() ? $resultado->getDt_fiminscricao()->format('d/m/Y') : NULL;
            $resultadoArr['dt_cadastro'] = $resultado->getDt_cadastro() ? $resultado->getDt_cadastro()->format('d/m/Y') : NULL;
            $resultadoArr['id_entidade'] = $resultado->getId_entidade() ? $resultado->getId_entidade()->getId_entidade() : NULL;
            $resultadoArr['id_situacao'] = $resultado->getId_situacao() ? $resultado->getId_situacao()->getId_situacao() : NULL;
            $resultadoArr['st_imagem'] = $resultado->getSt_imagem() ? $resultado->getSt_imagem() :'';
            $return = json_encode($resultadoArr);
        }
        $this->view->dados = $return;
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
    }

    /**
     * Retorna todos os produtos da entidade
     * @author Helder Silva <helder.silva@unyleya.com.br>@abstract
     */

    public function findProdutosEntityAction()
    {
        $negocio = new \G2\Negocio\ProdutoConcurso();
        $resultado = $negocio->findAllProduto();
        $this->_helper->json($resultado->getMensagem());
    }

    public function salvarDadosConcursoAction()
    {
        if ($this->getRequest()->isPost()) {

            $body = $this->getRequest()->getPost();
            $result = $this->negocio->enviarDadosConcurso($body, $_FILES);
            $array = $this->negocio->toArrayEntity($result);
            $this->_helper->json($array);
        } else {
            $this->_helper->json("Requisição Invalida!");
        }
        $this->getResponse()->appendBody(json_encode($result));
    }
}

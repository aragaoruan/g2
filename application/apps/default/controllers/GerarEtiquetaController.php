<?php

class Default_GerarEtiquetaController extends Ead1_Controller {

    private $negocio;
    private $repositoryName = 'G2\Entity\VwGerarEtiquetas';

    public function init()
    {
        parent::init();
        $this->negocio = new G2\Negocio\EntregaDeclaracao();
    }

    public function indexAction(){

    }

    public function jsAction(){
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('gerar-etiqueta/index.js');
    }

    public function gerarXlsAction(){

        //Forma 1: verificar se existem os params. Caso não existam, coloco string vazia..
        $params = $this->getAllParams();

        if(isset($params['impressao']) && $params['impressao']){
            $result = $this->negocio->findOneBy($this->repositoryName, array('id_entregadeclaracao' => $params['id_entregadeclaracao']));

            $ed = new \G2\Entity\EntregaDeclaracao();
            $eds = $ed->findBy(array('id_entregadeclaracao' => $params['id_entregadeclaracao']));
            $idSit = new G2\Entity\Situacao();
            $idSit->setId_situacao(80);
            $eds[0]->setid_situacao($this->negocio->find('\G2\Entity\Situacao', $idSit));
            $eds[0]->setdt_entrega(new DateTime());

            $this->negocio->save($eds[0]);

            ob_start();

            $html = '<div>';
            $html .= '<table>';
            $html .= '<tr><td>'. $this->mask($result->getSt_cpf(), '###.###.###-##');
            $html .= '<br>'. $result->getSt_nomecompleto();
            $html .= '<br>'. $result->getSt_endereco() .' - '. $result->getNu_numero();
            $html .= '<br>'. $result->getSt_bairro() .' - '. $result->getSt_nomemunicipio() .' - '. $result->getSg_uf();
            if($result->getSt_complemento()) {
                $html .= '<br>'. $result->getSt_complemento();
            }
            $html .= '<br>' .$result->getSt_cep().'</td></tr>';
            $html .= '</table>';
            $html .= '<div>';

            ob_get_contents();
            ob_end_clean();

            echo $html;
            exit;
        }

        //Se for array, significa múltiplas etiquetas.
        if(is_array($params['id_entregadeclaracao'])) {
            $results = $this->negocio->findBy($this->repositoryName, array('id_entregadeclaracao' => $params['id_entregadeclaracao']));

            $idsEntregaDeclaracao = array();

            foreach($results as $res){
                if($res instanceof \G2\Entity\VwGerarEtiquetas)
                array_push($idsEntregaDeclaracao, $res->getId_entregadeclaracao());
            }

            $ed = new \G2\Entity\EntregaDeclaracao();
            $eds = $ed->findBy(array('id_entregadeclaracao' => $idsEntregaDeclaracao));

            if($eds){
                foreach($eds as $ent){
                    if($ent instanceof \G2\Entity\EntregaDeclaracao){
                        $idSit = new G2\Entity\Situacao();
                        $idSit->setId_situacao(80);
                        $ent->setid_situacao($this->negocio->find('\G2\Entity\Situacao', $idSit));
                        $this->negocio->save($ent);
                    }
                }
            }

            //Remove duplicatas.
            foreach($results as $k => $v){
                foreach($results as $key => $value){
                    if($k != $key && $v->getSt_nomecompleto() == $value->getSt_nomecompleto()){
                        unset($results[$k]);
                    }
                }
            };

            $objPHPExcel = new PHPExcel();

            $objPHPExcel->getProperties()->setTitle('Etiquetas')
                ->setDescription('Etiquetas geradas pelo G2S.');

            $i = 1;

            foreach($results as $result){
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, $this->mask($result->getSt_cpf(), '###.###.###-##')); $i++;
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, $result->getSt_nomecompleto()); $i++;
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, $result->getSt_endereco() .' - '. $result->getNu_numero()); $i++;
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, $result->getSt_bairro() .' - '. $result->getSt_nomemunicipio() .' - '. $result->getSg_uf()); $i++;
                if($result->getSt_complemento()){
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, $result->getSt_complemento()); $i++;
                }
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, 'CEP: ' .$result->getSt_cep()); $i++;
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, ''); $i++;
            }

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

            header('Content-type: application/vnd.ms-excel;');
            header('Content-Disposition: attachment; filename="etiquetas.xls"');
            $objWriter->save('php://output');
            exit();
        }

        //Senão, uma única etiqueta será gerada.

        else {
            $result = $this->negocio->findOneBy($this->repositoryName, array('id_entregadeclaracao' => $params['id_entregadeclaracao']));

            $ed = new \G2\Entity\EntregaDeclaracao();
            $eds = $ed->findBy(array('id_entregadeclaracao' => $params['id_entregadeclaracao']));
            $idSit = new G2\Entity\Situacao();
            $idSit->setId_situacao(80);
            $eds[0]->setid_situacao($this->negocio->find('\G2\Entity\Situacao', $idSit));
            $eds[0]->setdt_entrega(new DateTime());

            $this->negocio->save($eds[0]);

            $objPHPExcel = new PHPExcel();

            $objPHPExcel->getProperties()->setTitle('Etiquetas')
                ->setDescription('Etiquetas geradas pelo G2S.');

            $i = 1;

            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, $this->mask($result->getSt_cpf(), '###.###.###-##')); $i++;
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, $result->getSt_nomecompleto()); $i++;
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, $result->getSt_endereco() .' - '. $result->getNu_numero()); $i++;
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, $result->getSt_bairro() .' - '. $result->getSt_nomemunicipio() .' - '. $result->getSg_uf()); $i++;
            if($result->getSt_compemento()) {
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, $result->getSt_complemento()); $i++;
            }
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, 'CEP: ' .$result->getSt_cep()); $i++;
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, '');

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

            header('Content-type: application/vnd.ms-excel;');
            header('Content-Disposition: attachment; filename="etiquetas.xls"');
            $objWriter->save('php://output');
            exit();
        }
    }

    function mask($val, $mask)
    {
        $maskared = '';
        $k = 0;
        for($i = 0; $i<=strlen($mask)-1; $i++)
        {
            if($mask[$i] == '#')
            {
                if(isset($val[$k]))
                    $maskared .= $val[$k++];
            }
            else
            {
                if(isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }

}

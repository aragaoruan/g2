<?php

/**
 * Classe de controller MVC para controlar os dados da Vw_AreaProjetoNivelEnsino
 * @author Elcio Guimarães <elcioguimaraes@gmail.com>
 * @author Kayo Silva <kayo.silva@unyleya.com.br> 2014-21-01
 * @package application
 * @subpackage controller
 */
class Default_LivroController extends Ead1_ControllerRestrita
{

    private $form;
    private $negocio;

    public function init ()
    {
        parent::init();
        parent::setupDoctrine();
        $this->form = new LivroForm();
        $this->negocio = new G2\Negocio\Livro();
    }

    public function jsAction ()
    {
        $this->_helper->viewRenderer->setNoRender();
        echo $this->view->render('livro/index.js');
    }

    public function indexAction ()
    {
        $disciplinaNegocio = new G2\Negocio\Disciplina();
        $this->view->form = $this->form;
        $this->view->autores = $disciplinaNegocio->retornarDadosCoordenadorDisciplina(array(
            'id_perfilpedagogico' => 8,
            'id_situacaoperfil' => 15
        ));
//        $perfil = $disciplinaNegocio->retornaPerfilDisciplinaByEntidadeSessao();
        $perfil = $this->negocio->retornaIdPerfilAutor(array('id_perfilpedagogico' => 8, 'id_situacao' => 4));
//        $this->view->idPerfil = ($perfil ? ($perfil instanceof \G2\Negocio\Perfil ? $perfil->getId_perfil() : $perfil[0]->getId_perfil()) : null);
        $this->view->idPerfil = ($perfil ? $perfil->getId_perfil() : null);
        $id = $this->_getParam('id');
        if ($id) {
            $livro = $this->negocio->findLivroById($id);
            if ($livro->getType() == 'success') {
                $data = $livro->getMensagem();
                $data['id'] = $data['id_livro'];
                $this->form->populate($data);
            } else {
                $this->view->mensagem = $livro->getText();
            }
        }
    }

    public function pesquisarAction ()
    {

        try {

            $to = new VwPesquisarLivroTO();
            $to->montaToDinamico($this->limpaGetAllParams($this->_getAllParams()));
            $to->setSt_livro(utf8_decode($to->getSt_livro()));

            $ro = new LivroRO();
            $mretorno = $ro->pesquisarLivro($to);
            if ($mretorno->getTipo() == Ead1_IMensageiro::SUCESSO) {

                $array = array();
                foreach ($mretorno->getMensagem() as $to) {
                    $array[] = $to->toArray();
                }

                $retorno = new \Ead1_Mensageiro($array, \Ead1_IMensageiro::SUCESSO);
                $retorno->setText(count($array) . " Livros encontrado(s)");
            } else {
                $retorno = $mretorno;
            }
        } catch (Exception $e) {
            $retorno = new \Ead1_Mensageiro('Erro ao Pesquisar - ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        die(json_encode($retorno));
    }

    public function salvaLivroEntidadeAction ()
    {
        //Recupera o request
        $request = $this->getRequest();
        //Verifica se é um post
        if ($request->isPost()) {
            //Recupera os parametros do post
            $data = $request->getPost();
            $result = $this->negocio->salvaLivroEntidade($data['id_livro'], $data['data']);

            $this->_helper->json(array(
                'type' => $result->getType(),
                'title' => $result->getTitle(),
                'text' => $result->getText()
            ));
        }
    }

}

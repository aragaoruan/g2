<?php

/**
 * Classe de controller MVC para controlar a configuração do primeiro acesso
 * @author rafael.rocha <rafael.rocha@unyleya.com.br>
 * 
 * @package application
 * @subpackage controller
 */
class Default_PABoasVindasController extends Ead1_ControllerRestrita {

    public function indexAction() {
        $this->_helper->viewRenderer->setNoRender(false);
    }

    public function jsAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('primeiro-acesso/index.js');
    }

}

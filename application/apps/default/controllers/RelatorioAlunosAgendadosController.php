<?php

class Default_RelatorioAlunosAgendadosController extends Ead1_Controller
{
    /**
     * @var \G2\Negocio\Relatorio
     */
    private $negocio;

    public function init()
    {
        $this->negocio = new \G2\Negocio\Relatorio();
    }

    public function indexAction()
    {
    }

    public function retornaDadosCombosAction()
    {
        $params = $this->getAllParams();
        unset($params['module'], $params['controller'], $params['action'], $params['order'], $params['sort']);

        $order = $this->getParam('order') ? $this->getParam('order') : 'dt_aplicacao';
        $sort = $this->getParam('sort') ? $this->getParam('sort') : 'ASC';

        $results = $this->negocio->findBy('\G2\Entity\VwAvaliacaoAplicacao', $params, array($order => $sort));

        foreach ($results as &$entity) {
            $entity = array(
                'id_avaliacaoaplicacao' => $entity->getId_avaliacaoaplicacao(),
                'dt_aplicacao' => $this->negocio->converterData($entity->getDt_aplicacao(), 'd/m/Y'),
                'nu_anoaplicacao' => $entity->getNu_anoaplicacao(),
                'nu_mesaplicacao' => $entity->getNu_mesaplicacao(),
                'nu_diaaplicacao' => $entity->getNu_diaaplicacao(),
                'st_mesaplicacao' => $this->negocio->stringMes($entity->getNu_mesaplicacao()),
                'id_entidade' => $entity->getId_entidade(),
                'nu_relatoriosagendamento' => $entity->getNu_relatoriosagendamento()
            );
        }
        $this->_helper->json($results);
    }

    public function retornaProjetosAction()
    {
        $params = array('id_entidade' => array($this->negocio->sessao->id_entidade));

        $id_entidade = $this->getParam('id_entidade');

        if ($id_entidade) {
            $params['id_entidade'][] = $id_entidade;
        }

        try {
            $response = array();

            /** @var \G2\Entity\VwProjetoEntidade[] $results */
            $results = $this->negocio->findBy('\G2\Entity\VwProjetoEntidade', $params, array(
                'st_projetopedagogico' => 'ASC'
            ));

            if (is_array($results) && $results) {
                foreach ($results as $entity) {
                    $response[$entity->getId_projetopedagogico()] = array(
                        'id_projetopedagogico' => $entity->getId_projetopedagogico(),
                        'st_projetopedagogico' => $entity->getSt_projetopedagogico()
                    );
                }
            }

            $this->_helper->json(array_values($response));
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(500)->appendBody($e->getMessage());
        }
    }

    public function pesquisarAction()
    {
        $linhadenegocio = (new \G2\Negocio\EsquemaConfiguracao())
            ->retornaItemPorEntidade(
                \G2\Constante\ItemConfiguracao::LINHA_DE_NEGOCIO,
                $this->negocio->sessao->id_entidade
            );

        $id_linhadenegocio = ($linhadenegocio && isset($linhadenegocio['st_valor']))
            ? (int)$linhadenegocio['st_valor']
            : 0;

        $params = $this->limpaGetAllParams($this->getAllParams());
        unset($params['order'], $params['sort'], $params['nu_registros']);

        $results = $this->negocio->alunosAgendados($params, $id_linhadenegocio);

        if ($this->getParam('type')) {
            $cabecalho = array(
                'st_nomecompleto' => "Nome",
                'st_cpf' => "CPF",
                'st_email' => "E-mail",
                'st_telefone' => "Telefone Principal",
                'st_telefonealternativo' => "Telefone Secundário",
                'st_projetopedagogico' => "Curso",
                'st_possuiprova' => "Possui Prova",
                'st_aplicadorprova' => "Local de Prova",
                'st_horarioaula' => "Horário",
                'dt_aplicacao' => "Data da Aplicação",
                'dt_agendamento' => "Data da Realização do Agendamento",
                'st_responsavel' => "Responsável pelo Agendamento",
                'st_avaliacao' => "Tipo de Avaliação",
                'st_disciplinas' => "Disciplinas",
                'bl_temprovaintegrada' => "Prova Integrada"
            );
            if ($id_linhadenegocio === \G2\Constante\LinhaDeNegocio::POS_GRADUACAO) {
                $cabecalho['st_presenca'] = 'Status do Aluno';
                $cabecalho['st_documentacao'] = 'Documentação';
            }

            $this->negocio->export(
                $results,
                $cabecalho,
                $this->getParam('type'),
                'Relatório de agendamento'
            );
        }

        $this->_helper->json($results);
    }

}

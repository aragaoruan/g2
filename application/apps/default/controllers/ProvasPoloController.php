<?php

use G2\Negocio\ProvasPolo;

/**
 * Controller para relatorio de provas por polo
 * @author Denise Xavie <denise.xavier@unyleya.com.br>
 * @since  23-01-2014
 * @package application
 * @subpackage controller
 */
class Default_ProvasPoloController extends Ead1_ControllerRestrita
{

    /**
     * @var \G2\Negocio\ProvasPolo
     */
    private $negocio;

    /**
     * @var \G2\Negocio\Entidade
     */
    private $entidadeNegocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\ProvasPolo();
        $this->entidadeNegocio = new \G2\Negocio\Entidade();
    }

    public function indexAction()
    {

    }

    public function pesquisaProvasPoloAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $zd = new Zend_Date();

        $params = $this->getRequest()->getParams();

        $results = $this->negocio->pesquisaProvasPolo($params);
        $return = array();

        if ($results) {
            foreach ($results as $key => $result) {
                $return[$key] = $this->negocio->toArrayEntity($result);
                $return[$key]['dt_aplicacao'] = $result->getDt_aplicacao()->format('d/m/Y');
                $return[$key]['hr_fim'] = $zd->set($result->getHr_fim())->toString('H:m');
                $return[$key]['hr_inicio'] = $zd->set($result->getHr_inicio())->toString('H:m');
            }
        }

        $this->_helper->json($return);
    }

    /**
     * Gera XLS de acordo com as provas agendadas
     */
    public function gerarXlsAction()
    {
        try {
            if ($this->getParam('id_avaliacaoaplicacao')) {
                $array['id_avaliacaoaplicacao'] = $this->getParam('id_avaliacaoaplicacao');
                $this->view->dados = $this->negocio->pesquisaVwAlunosAgendamento($array);
                $xls = $this->view->render('provas-polo/xls-aplicacao.phtml');
                header('Content-Type: text/html; charset=UTF-8');
                header("Content-type: application/x-msexcel");
                header('Content-Disposition: attachment; filename="Relatorio_Aplicações_' . date('d/M/Y H:i:s') . '.xls"');
                header("Content-Description: Relatório de provas por pólos, alunos! " . date('d/M/Y H:i:s'));
                echo $xls;
            }

            exit();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * Gerar pdf da lista de presençass
     */
    public function gerarPdfAction()
    {
        try {
            // Verifica se os parâmetros foram passados de forma corretamente
            if ($this->getParam('id_avaliacaoaplicacao') && $this->getParam('id_entidade')) {

                //Preenche um array novo para pesquisa por parâmetros
                $array['id_avaliacaoaplicacao'] = $this->getParam('id_avaliacaoaplicacao');

                //Inicia a pesquisa com o array iniciado anteriormente
                $this->view->dados = $this->negocio->pesquisaVwAlunosAgendamento($array);

                //Pesquisa os dados da entidade e armazena na variável para uso posterior
                $dadosEntidade = $this->entidadeNegocio->findEntidade($this->getParam('id_entidade'));

                //Pesquisa o esquema de configuração da entidade pela VW (caminho feliz)
                $dadosEsquemaConfiguracaoEntidade = $this->entidadeNegocio
                    ->retornarVwEntidadeEsquemaConfiguracao($this->getParam('id_entidade'), [
                        'id_itemconfiguracao' => 22
                    ]);

                //Retorna um array com apenas os dados necessários para a geração do pdf de forma customizada
                $this->view->dadosEntidade = [
                    'st_urlimglogo' => $dadosEntidade->getStr_urlimglogoentidade(),
                    'linha_negocio' => [
                        'st_descricao' => $dadosEsquemaConfiguracaoEntidade[0]->getSt_descricao(),
                        'st_valor' => $dadosEsquemaConfiguracaoEntidade[0]->getSt_valor()
                    ]
                ];

                //Inicia a biblioteca de criação de PDF e passa como parâmetro o arquivo de visualização para geração de
                //pdf a partir de dele
                $ead1PDF = new Ead1_Pdf();
                $ead1PDF->carregarHTML(utf8_encode($this->view->render('provas-polo/pdf-lista.phtml')))
                    ->configuraPapel('a4', 'landscape');
                $ead1PDF->gerarPdf('Lista_presença_provas' . ' ' . date('d_m_Y_H_i_s') . '.pdf');
                exit();
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function gerarListaAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        try {
            if ($this->getParam('id_avaliacaoaplicacao')) {
                $array['id_avaliacaoaplicacao'] = $this->getParam('id_avaliacaoaplicacao');
                $dados = $this->negocio->relatorioAlunosAgendamento($array);

                $zd = new \Zend_Date();

                $this->view->stAplicadorProva = $dados[0]['avaliacao']->getSt_aplicadorprova();
                $this->view->dtAplicacao = $dados[0]['avaliacao']->getDt_aplicacao()->format('d/m/Y');
                $this->view->hrInicio = $zd->set($dados[0]['avaliacao']->getHr_inicio())->toString('H:m');
                $this->view->hrTermino = $zd->set($dados[0]['avaliacao']->getHr_fim())->toString('H:m');

                $this->view->dados = $dados;
                echo $this->view->render('provas-polo/lista.phtml');
            }
        } catch (Exception $e) {
//            echo $e->getMessage();
//            exit();
        }
    }

    /**
     * Gera XLS de presença da aplicação
     */
    public function gerarXlsPresencaAction()
    {
        try {
            if ($this->getParam('id_avaliacaoaplicacao')) {
                $array['id_avaliacaoaplicacao'] = $this->getParam('id_avaliacaoaplicacao');
                //$array['bl_ativo'] = 1;
                $gerenciaProvaNegocio = new \G2\Negocio\GerenciaProva();
                $this->view->dados = $gerenciaProvaNegocio->findByVwAvaliacaoAgendamento($array);
                $xls = $this->view->render('provas-polo/xls-presenca.phtml');
                header('Content-Type: text/html; charset=UTF-8');
                header("Content-type: application/x-msexcel");
                header('Content-Disposition: attachment; filename="Relatorio_Presenca_Agendamento_' . date('d/M/Y H:i:s') . '.xls"');
                header("Content-Description: Relatório de presença aluno agendamento " . date('d/M/Y H:i:s'));
                echo $xls;
            }

            exit();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

}

<?php
/**
 * Created by PhpStorm.
 * User: aragaoruan
 * Date: 12/04/18
 * Time: 16:39
 */

class Default_CertificadoParcialController extends Ead1_ControllerRestrita
{
    /**
     * @var \G2\Negocio\CertificadoParcial
     */
    private $negocio;

    public function init()
    {
        parent::init();
        parent::setupDoctrine();
        $this->negocio = new \G2\Negocio\CertificadoParcial;
    }

    public function indexAction()
    {

    }

    public function retornaCertificadoParcialAction()
    {
        $this->_helper->json($this->negocio->retornaCertificadoParcial());
    }

    public function retornaPolosAction()
    {
        $this->_helper->json($this->negocio->retornaPolos());
    }

    public function retornaAlunosAptosCertificadoParcialAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $this->_helper->json($this->negocio->retornaAlunosAptosCertificadoParcial($params));
    }

    public function visualizarCertificadoAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $textoSistemaTO = $this->negocio->visualizarCertificado($params);

        $textoBo = new \TextoSistemaBO();
        $mensageiro = $textoBo->gerarHTML($textoSistemaTO, array(
            'id_matricula' => $params['id_matricula'],
            'id_certificadoparcial' => $params['id_certificadoparcial'],
            'id_entidade' => $params['id_entidade']
        ))->subtractMensageiro();

        if (array_key_exists('visualizar', $params)) {
            echo '<img src="/img/background/draft.jpg" 
                        style="position: fixed; top: 50%; left: 50%; margin: -495px 0 0 -600px; z-index: -1;" />';
        }

        if (array_key_exists('gerar', $params)) {
            $this->negocio->salvarCertificadoParcial($params, $mensageiro->getFirstMensagem()->getSt_texto());
        }

        echo $mensageiro->getFirstMensagem()->getSt_texto();
        exit;

    }

    public function retornaHistoricoAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $this->_helper->json($this->negocio->retornaHistorico($params));
    }

    public function verificarSiglaAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $this->_helper->json($this->negocio->verificarSigla($params));
    }

}

<?php

/**
 * Controller responsavel por gerar modelos do bacbone
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2013-10-08
 * @package application
 * @subpackage controller
 */
class Default_ModeloBackboneController extends Ead1_Controller
{

    public function init()
    {
        //Chama as configurações do doctrine
        $this->setupDoctrine();
        $this->getResponse()->setHeader('Content-Type', 'application/javascript', true);
    }

    /**
     * Gera uma string com atributos para modelo do backbone de acordo com a entity
     * @param string $GET_ ['entity'] Nome da Entidade
     * @param string $GET_ ['controllerApi'] Controller API a ser chamada
     * @return view
     */
    public function indexAction()
    {
        try {
            //Desabilita o layout da aplicação
            $this->_helper->layout->disableLayout();
            //recupera os parametros
            $entity = $this->getParam('entity');
            $controller = $this->getParam('controllerApi');

            $modificacaoEntity = filemtime(APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . "library" . DIRECTORY_SEPARATOR . "G2" . DIRECTORY_SEPARATOR . "Entity" . DIRECTORY_SEPARATOR . $entity . '.php');

            $caminhoJSgerado = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . "public" . DIRECTORY_SEPARATOR . "gestor2" . DIRECTORY_SEPARATOR . "js" . DIRECTORY_SEPARATOR . "backbone" . DIRECTORY_SEPARATOR . "models" . DIRECTORY_SEPARATOR . "cache" . DIRECTORY_SEPARATOR . $entity . '.js';

            if (file_exists($caminhoJSgerado) && $modificacaoEntity == filemtime($caminhoJSgerado)) {
                $content = file_get_contents($caminhoJSgerado);
            } else {
                $repoEntity = "G2\\Entity\\" . $entity;
                //Recupera os atributos da entity
                $metas = $this->em->getClassMetadata($repoEntity);
                //Seta as variaveis da view
                $this->view->entity = $entity;
                $this->view->controllerApi = $controller;
                $this->view->attributes = $metas->reflFields;


                $content = "/*\n"
                    . "============================================\n"
                    . " ARQUIVO GERADO AUTOMATICAMENTE, NÃO EDITAR\n"
                    . " cache gerado em: " . date("d/m/Y H:i:s") . "\n"
                    . "============================================\n"
                    . "*/\n\n";

                $content .= "var " . $entity . "= Backbone.Model.extend({\n"
                    . "defaults: {\n";

                if ($metas->reflFields):
                    foreach ($metas->reflFields as $key => $attr):
                        $content .= "\t" . $key . ": '',\n";
                    endforeach;
                endif;

                $content .= "},";

                if ($controller):
                    $content .= "\nurl: function() {\n";
                    $content .= "\t\treturn this.id ? '/api/" . $controller . "/' + this.id : '/api/" . $controller . "';\n";
                    $content .= "\t}\n";
                endif;

                $content .= "});";
                $content = str_replace(",\n}", "\n}", $content);

                @touch($caminhoJSgerado, $modificacaoEntity);
                file_put_contents($caminhoJSgerado, $content);
                @touch($caminhoJSgerado, $modificacaoEntity);
            }

            $this->view->content = $content;

        } catch (Exception $e) {
            throw new Zend_Exception($e->getMessage());
        }
    }

}

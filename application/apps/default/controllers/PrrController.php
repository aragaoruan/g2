<?php

/**
 * Classe PrrController
 *
 * @author Rafael Bruno <rafael.oliveira@unyleya.com.br>
 */
class Default_PrrController extends Ead1_ControllerRestrita {

    private $_negocio;
    
    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $this->view->abaDisciplina = $this->view->render('prr/aba-disciplina.phtml');
        $this->view->abaPagamento = $this->view->render('prr/aba-pagamento.phtml');
        $this->view->abaMatricula = $this->view->render('prr/aba-matricula.phtml');
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(TRUE);
        echo $this->view->render('prr/index.js');
    }

    public function retornaMeiosPagamentoPrrAction()
    {

        $meiosPagamento = \G2\Constante\MeioPagamento::getMeiosPagamentoPrrArray();

        foreach ($meiosPagamento as $key => $value) {
            $data[] = array('id_meiopagamento' => $key, 'st_meiopagamento' => $value);
        }

        return $this->_helper->json($data);
    }

    public function enviarMensagemCobrancaAction()
    {
        $this->_negocio = new \G2\Negocio\Negocio();
        $data = $this->getRequest()->getPost();
        
        switch ($data['params']['id_meiopagamento']) {
            case \G2\Constante\MeioPagamento::CARTAO_CREDITO:
                $result = \G2\G2Mensagem::gerarMensagemEnvioCartao($data['params']);
                break;

            case \G2\Constante\MeioPagamento::BOLETO:
                $result = \G2\G2Mensagem::gerarMensagemEnvioBoleto($data['params']);
                break;
        }

        return $this->_helper->json($result);
    }

}

<?php

class Default_UltimoAcessoAlunoController extends Ead1_ControllerRestrita
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {

    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
    }

    public function retornaRelatorioAction()
    {
        $this->negocio = new G2\Negocio\UltimoAcessoAluno();
        $params = $this->limpaGetAllParams($this->getAllParams());
        $detalhado = isset($params['detalhado']) && $params['detalhado'] == 1 ? true : false;

        unset($params['detalhado']);

        $result = $this->negocio->executaRelatorio($params, $detalhado);
        $this->_helper->json($result);
    }

    public function gerarXlsAction()
    {
        $this->negocio = new G2\Negocio\UltimoAcessoAluno();

        $params = $this->limpaGetAllParams($this->getAllParams());
        $detalhado = isset($params['detalhado']) && $params['detalhado'] == 1 ? true : false;

        unset($params['detalhado']);
        $this->view->detalhado = $detalhado;
        $this->view->dados = $this->negocio->executaRelatorio($params, $detalhado);

    }

    public function jsDivisaoDeCarteiraAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/divisao-de-carteira.js');
    }

    public function divisaoDeCarteiraAction()
    {
    }

}
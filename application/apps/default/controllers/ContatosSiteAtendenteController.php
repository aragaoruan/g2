<?php

/**
 * Classe de controller para Contatos do Site
 * @author Elcio Guimarães <elcioguimaraes@gmail.com>
 * @since 2017-11-07
 */
use G2\Negocio\Situacao,
    G2\Negocio\Prevenda;

class Default_ContatosSiteAtendenteController extends Ead1_ControllerRestrita
{

    /**
     * Prevenda Negocio
     * @var G2\Negocio\Prevenda
     */
    private $_negocio;

    public function init()
    {
        parent::init();
        $this->_negocio = new Prevenda();
    }

    public function indexAction()
    {
        $situacao = new Situacao();
        $this->view->arrSituacao = $situacao->retornaSituacaoFindBy(array('st_tabela' => 'tb_prevenda'));
    }

    /**
     * Action para js do backbone
     */
    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('contatos-site-atendente/index.js');
    }


}

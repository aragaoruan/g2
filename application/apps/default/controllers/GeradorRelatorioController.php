<?php

/**
 * Controller para Gerador De Relatorio
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2014-11-11
 * @package application
 * @subpackage controller
 */


class Default_GeradorRelatorioController extends Ead1_ControllerRestrita
{
    private $negocio;

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('gerador-relatorio/index.js');
    }

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Negocio();
    }

    public function indexAction()
    {
    }

} 
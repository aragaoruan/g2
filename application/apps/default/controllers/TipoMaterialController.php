<?php

/**
 * Controller para Tipos de Material
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @package default
 * @subpackage controller
 * @since 2013-09-11
 */
class Default_TipoMaterialController extends Ead1_ControllerRestrita
{

    public function jsAction ()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('tipo-material/index.js');
    }

    public function indexAction ()
    {
        
    }

}
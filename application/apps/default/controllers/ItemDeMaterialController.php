<?php

class Default_ItemDeMaterialController extends Ead1_ControllerRestrita

{

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('item-de-material/index.js');
    }

    public function indexAction()
    {
    }

    public function getTipoMaterialAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $negocio = new \G2\Negocio\Negocio();
        $results = $negocio->findBy('G2\Entity\TipodeMaterial', array(
            'id_entidade' => $negocio->sessao->id_entidade,
            'id_situacao' => 13 // 13 = Ativo
        ));

        $mensageiro = array();
        foreach ($results as $key => $result) {
            $mensageiro[] = $negocio->toArrayEntity($result);
        }

        array_unshift($mensageiro, array(
            'id_tipodematerial' => '',
            'st_tipodematerial' => '[Selecione]',
            'id_situacao' => null,
            'id_entidade' => null
        ));

        $this->getResponse()->appendBody(json_encode($mensageiro));
    }

    public function getSituacaoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $negocio = new \G2\Negocio\Situacao();
        $results = $negocio->findBy('G2\Entity\Situacao', array(
            'st_tabela' => 'tb_itemdematerial'
        ));

        $mensageiro = array();
        foreach ($results as $key => $result) {
            $mensageiro[] = $negocio->toArrayEntity($result);
        }

        array_unshift($mensageiro, array(
            'id_situacao' => '',
            'st_situacao' => '[Selecione]',
            'st_tabela' => '',
            'st_campo' => '',
            'st_descricaosituacao' => ''
        ));

        $this->getResponse()->appendBody(json_encode($mensageiro));
    }

    public function projetosSelecionadosAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
    }

    public function projetosPedagogicosAction($idDocumentos = false)
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $negocio = new \G2\Negocio\Negocio();

        $to = new VwProdutoProjetoTipoValorTO();
        $to->setId_entidade($negocio->sessao->id_entidade); // pega a entidade da sessão

        $bo = new ProjetoPedagogicoBO();
        $mensageiro = $bo->retornarVwProdutoProjetoTipoValor($to);


        echo json_encode($mensageiro->mensagem);
    }

    public function getDisciplinasEntidadeAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $negocio = new \G2\Negocio\Disciplina();
        $param = $this->getAllParams();
        $results = $negocio->pesquisaDisciplinaEntidade($param);
        $entity = new \G2\Entity\Disciplina();

        if ($results->getTipo() == \Ead1_IMensageiro::SUCESSO) {
            $results = $results->getMensagem();
//            $results = $results[0];
        } else {
            $results = $negocio->toArrayEntity($entity);
        }

        $this->getResponse()->appendBody(json_encode($results));
    }

    public function getProjetosPedagogicosDisciplinaAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $negocio = new \G2\Negocio\Disciplina();
        $param = $this->getAllParams();
        $results = $negocio->pesquisaProjetosDisciplina($param);
        $entity = new \G2\Entity\ProjetoPedagogico();

        if ($results->getTipo() == \Ead1_IMensageiro::SUCESSO) {
            $results = $results->getMensagem();
//            $results = $results[0];
        } else {
            $results = $negocio->toArrayEntity($entity);
        }

        $this->getResponse()->appendBody(json_encode($results));
    }

    public function getDisciplinasEditAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $negocio = new \G2\Negocio\Negocio();
        $result = array();
        $id = $this->getParam('id_itemdematerial');
        $disciplinas = $negocio->findBy('\G2\Entity\ItemDeMaterialDisciplina', array(
            'id_itemdematerial' => $id
        ));
        foreach ($disciplinas as $disciplina) {
            $result[] = array('id_disciplina' => $disciplina->getId_disciplina()->getId_disciplina(), 'st_disciplina' => $disciplina->getId_disciplina()->getSt_disciplina());
        }
        $this->getResponse()->appendBody(json_encode($result));
    }

    public function getProjetosEditAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $negocio = new \G2\Negocio\Negocio();
        $result = array();
        $id = $this->getParam('id_itemdematerial');
        $projetos = $negocio->findBy('\G2\Entity\MaterialProjeto', array(
            'id_itemdematerial' => $id
        ));
        foreach ($projetos as $projeto) {
            $result[] = array('id_projetopedagogico' => $projeto->getId_projetopedagogico()->getId_projetopedagogico(), 'st_projetopedagogico' => $projeto->getId_projetopedagogico()->getSt_projetopedagogico());
        }
        $this->getResponse()->appendBody(json_encode($result));
    }


}


<?php

class Default_SalaDeAulaIntegracaoController extends Ead1_ControllerRestrita
{
    /** @var $negocio G2\Negocio\SalaDeAulaIntegracao */
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\SalaDeAulaIntegracao();
    }

    public function atualizaDisciplinaIntegracaoSalaAction()
    {
        $idSalaDeAula = $this->getParam('id_saladeaula');
        $idDisciplinaIntegracao = $this->getParam('id_disciplinaintegracao');
        $result = $this->negocio->atualizaDisciplinaIntegracao($idSalaDeAula, $idDisciplinaIntegracao);
        return $this->json($result);
    }

    public function retronaSalaDeAulaIntegracaoAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        try {
            $params = $this->getAllParams();
            unset($params['controller'], $params['action'], $params['module']);
            $result = $this->negocio->retornaSalaDeAulaIntegracao($params);
            if (!$result) {
                return $this->getResponse()->setHttpResponseCode(404)
                    ->appendBody("Nenhum resultado encontrado.");
            }
            return $this->json($result);
        } catch (Exception $e) {
            return $this->getResponse()->setHttpResponseCode(500)
                ->appendBody($e->getMessage());
        }
    }

}

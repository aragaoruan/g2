<?php

/**
 * Controller do Endereco
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 * @since 2014-08-20
 * @package application
 * @subpackage controller
 */
use G2\Negocio\Conta;

class Default_ContaController extends Ead1_ControllerRestrita
{

    private $_negocio;

    public function init()
    {
        parent::init();
        $this->_negocio = new Conta();
    }

    public function getContaEntidadeFinanceiroAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        try {
            $id_entidade = $this->getParam('id');

            if ($id_entidade) {
                $result = $this->_negocio->retornaVwContaEntidadeFinanceiro($id_entidade);
                $this->getResponse()->appendBody(json_encode($result));
            }
        } catch (Exception $e) {
            throw $e;
        }
    }
}
<?php

/**
 * Controller para Dashboard
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2015-03-30
 * @package application
 * @subpackage controller
 */
class Default_DashboardController extends Ead1_RestRestrita
{
    /**
     * @var \G2\Negocio\Dashboard $negocio
     */
    private $negocio;

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('dashboard/index.js');
    }

    public function init()
    {
        $this->negocio = new \G2\Negocio\Dashboard();
    }

    public function indexAction()
    {
    }

    public function getBySessionAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $params = $this->getAllParams();
        $orderBy = $this->getParam('order_by') ? array($this->getParam('order_by') => $this->getParam('sort') ? $this->getParam('sort') : 'ASC') : null;
        unset($params['module'], $params['controller'], $params['action'], $params['order_by'], $params['sort']);

        $mensageiro = $this->negocio->getBySession($params, $orderBy);
        $this->_helper->json($mensageiro);
    }

    public function getJsonAction()
    {
        $id_dashboard = $this->getParam('id_dashboard', null);// $this->getAllParams();
        $dashboardData = [];
        if ($id_dashboard) {
            $dashboardData = $this->negocio->getJson($id_dashboard);
        }
        $this->_helper->json($dashboardData);
    }

    public function getJsonPreviewAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $params = $this->getAllParams();
        unset($params['module'], $params['controller'], $params['action']);

        $mensageiro = $this->negocio->getJsonPreview($params);
        $this->_helper->json($mensageiro);
    }

} 
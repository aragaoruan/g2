<?php

/**
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2015-04-10
 */
class Default_RelatorioGradeProjetoEntidadeController extends Ead1_ControllerRestrita
{
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\GradeHoraria();
    }


    public function indexAction()
    {

    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        echo $this->view->render($this->_getParam('controller') . '/index.js');
    }

    public function retornarProjetosAction()
    {
        $id_entidade = $this->getParam('id_entidade');
        $result = $this->negocio->retornarProjetosRelatorioGrade($id_entidade);
        $this->_helper->json($result);
    }

    public function gerarXlsRelatorioAction()
    {
        if ($this->getRequest()->isPost()) {
            $this->getResponse()->setRawHeader("Content-Type: application/vnd.ms-excel; charset=UTF-8")
                ->setRawHeader("Content-Disposition: attachment; filename=grade-horaria.xls")
                ->setRawHeader("Content-Transfer-Encoding: binary")
                ->setRawHeader("Expires: 0")
                ->setRawHeader("Cache-Control: must-revalidate, post-check=0, pre-check=0")
                ->setRawHeader("Pragma: public")
                ->sendResponse();

            $params = $this->getRequest()->getPost();
            if ($params) {
                $result = $this->negocio->retornarDadosRelatorioGradeHoraria($params);
                $this->view->dados = $result;
            }
        }
    }

    public function retornoRelatorioPrintAction()
    {
        $params = $this->limpaGetAllParams($this->getRequest()->getParams());

        if ($params) {
            $result = $this->negocio->retornarDadosRelatorioGradeHoraria($params);
            $this->view->dados = $result;
        }
    }

}
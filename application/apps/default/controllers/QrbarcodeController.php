<?php
/**
 * Classe para geração de imagens de código de barras e testes
 * @author Eder Lamar
 *
 */
class Default_QrbarcodeController extends Ead1_ControllerRestrita {
	
	/**
	 * Action para gerar QR-Code
	 */
	public function qrcodeAction(){
		$qr = new Ead1_Qrcode_Gerador();
		$qr->text($this->_getParam('chave'));
		//$qr->sms(6178157937, 'Rockfeller John');
		echo "<p><img src='".$qr->getLink()."' border='0'/></p>";
		exit;
	}

	/**
	 * Action para gerar imagem do código de barras
	 */
	public function barcodeAction(){
		$new_code = new Ead1_Barcode_Gerador($this->_getParam('chave'),0);
		exit;
	}

	/**
	 * Action para testar código de barras
	 */
	public function provabarcodeAction(){
		$filter = new Zend_Filter_Digits();
		$dado = $filter->filter($this->_getParam('chave'));
		if(strlen($dado) < 20){
			$qtdCaracteresFaltantes = 20 - strlen($dado);
			$caracteres = "";
			for($qtdCaracteresFaltantes = $qtdCaracteresFaltantes; $qtdCaracteresFaltantes != 0; $qtdCaracteresFaltantes--){
					$caracteres .= "0";
			}
			$dado = $caracteres . $dado;
		}
		echo "<h1>Teste Eder</h1><hr /><p><img src='http://g2.local.com.br:10088/teste/barcode/chave/".$this->_getParam('chave')."' border='0'/><br />$dado</p>";
		exit;
	}
	
	/**
	 * Action para comparar Bar Code e Qr-Code
	 */
	public function qrbarcodeAction(){
		$dado = $this->_getParam('chave');
		$qr = new Ead1_Qrcode_Gerador();
		$qr->text($dado);
		echo "<center><h1>Teste Código de Barras e QR-Code</h1><hr /><p><img src='http://g2.local.com.br:10088/qrbarcode/zfbarcode/chave/".$dado."' border='0'/><hr /><img src='".$qr->getLink()."' border='0'/><br />$dado</p></center>";
		exit;
	}
	
	public function zfbarcodeAction(){
		$options = array('text' => $this->_getParam('chave'), 'barHeight' => 40);
		$barcode = new Zend_Barcode_Object_Code128($options);
		$renderer = new Zend_Barcode_Renderer_Image();
		$renderer->setBarcode($barcode);
		echo $renderer->render();
		exit;
	}
}
<?php

/**
 * Classe de controller MVC para controlar cadastros de novas turmas
 * @author Helder Fernandes Silva <helder.silva@unyleya.com.br>
 * @since 2014-18-09
 * @package application
 * @subpackage controller
 */
Class Default_OrganizacaoController extends Ead1_ControllerRestrita
{


    public function indexAction()
    {

    }

    public function funcionalidadesAction()
    {
        $su = new Ead1_Mensageiro();
        $this->view->superUser = $su->getSu();
    }

    public function funcionalidadesJsAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        echo $this->view->render($this->_getParam('controller') . '/funcionalidades.js');
    }

    public function notificacoesAction()
    {
        //Conteudo totalmente implementado em Backbone em notificacoes.phtml
    }

    /**
     * Action que retona um ou mais perfis buscando por parte do nome do perfil usando
     * parametros correspondentes aos atributos da vw_perfisentidadenotificacao.
     *
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     */
    public function retornaVwPerfisEntidadeNotificacaoAction()
    {
        $array = array();

        $perfilNegocio = new \G2\Negocio\Perfil();
        $params = $this->getRequest()->getParams();

        //retirando parametros nao necessarios para consulta
        unset($params['module'], $params['action'], $params['controller']);

        $result = $perfilNegocio->retornaVwPerfisEntidadeNotificacao($params);

        foreach ($result as $key => $value) {
            $array[] = $perfilNegocio->toArrayEntity($value);
        }

        $this->_helper->json($array);
    }

    /**
     * Metodo que recebe uma collection com as informacoes da notificacao, alem de
     * subinformacoes como a lista de perfis que recebera a notificacao.
     *
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     */
    public function salvarDadosNotificacaoEntidadeAction()
    {

        $result = array();

        if ($this->getRequest()->isPost()) {

            $data = Zend_Json::decode($this->getRequest()->getRawBody());

            $negocio = new \G2\Negocio\Organizacao();
            $res = $negocio->salvarDadosNotificacaoEntidade($data[0]);

            foreach ($res as $key => $value){
                $result[] = $negocio->toArrayEntity($value);
            }
        }

        $this->_helper->json($result);
    }

}

<?php

/**
 * Classe de controller MVC para controlar RegraPagamento
 * @author Rafael Rocha <rafael.rocha.mg@gmail.com>
 *
 * @package application
 * @subpackage controller
 */
class Default_RecebimentoController extends Ead1_ControllerRestrita
{

    /**
     * @var \G2\Negocio\Recebimento
     */
    private $negocio;

    public function init()
    {
        ini_set('display_errors', E_ERROR);
        parent::init();
        parent::setupDoctrine();
        $this->negocio = new \G2\Negocio\Recebimento();
        $this->_helper->getHelper('viewRenderer')->setNoRender();
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('recebimento/index.js');
    }

    public function indexAction()
    {
        $this->_helper->viewRenderer->setNoRender(false);
        //echo $this->view->headScript()->appendFile('/recebimento/js');
    }

    public function retornarResumoFinanceiroAction()
    {
        try {
            if ($this->getParam('id_venda')) {

                $params = array('id_venda' => $this->getParam('id_venda'));

                if ($this->getParam('id_meiopagamento')) {
                    $params['id_meiopagamento'] = $this->getParam('id_meiopagamento');
                }

                if ($this->getParam('bl_chequedevolvido')) {
                    $params['bl_chequedevolvido'] = $this->getParam('bl_chequedevolvido');
                }

                if ($this->getParam('id_tipoendereco')) {
                    $params['id_tipoendereco'] = $this->getParam('id_tipoendereco');
                }

                $params['bl_ativo'] = $this->getParam('bl_ativo', 1);

                $vws = $this->negocio
                    ->findBy('G2\Entity\VwResumoFinanceiro', $params, ['nu_parcela' => 'ASC']);

                $retorno = array();
                $primeiraParcelaAcordo = NULL;
                foreach ($vws as $key => $vw) {
                    //$retorno[] = $this->negocio->toArrayEntity($vw);
                    $retorno[$key] = $vw->toBackboneArray();
                    $retorno[$key]['parcela_vencida'] = false;

                    $dt_atual = new DateTime();
                    //se a parcela tiver vencida recebe TRUE para desativar o botão
                    if (
                        strtotime($vw->getDt_vencimento()
                            ->format('Y-m-d')) < strtotime($dt_atual->format('Y-m-d'))
                    ) {
                        //parcela vencida
                        $retorno[$key]['parcela_vencida'] = true;
                    }

                    //modificando o nome do meio de pagamento
                    if (
                        $retorno[$key]['id_meiopagamento'] == \G2\Constante\MeioPagamento::CARTAO_CREDITO
                        && $retorno[$key]['id_acordo'] != NULL
                    ) {
                        $retorno[$key]['st_meiopagamento'] .= ' - Acordo';
                    }

                    //o botão será inserido quando for a primeira parcela de um acordo de cartão de crédito
                    if (
                        $this->getParam('id_meiopagamento') == \G2\Constante\MeioPagamento::CARTAO_CREDITO
                        && $retorno[$key]['bl_original'] == 0
                        && $primeiraParcelaAcordo != $retorno[$key]['id_acordo']
                    ) {

                        $primeiraParcelaAcordo = $retorno[$key]['id_acordo'];
                        $retorno[$key]['primeira_parcela_acordo'] = 1;

                        $paramsFindBy = array(
                            'id_entidade' => $retorno[$key]['id_entidade'],
                            'id_mensagempadrao' => \G2\Constante\MensagemPadrao::ENVIO_CARTAO_CREDITO_ACORDO
                        );
                        $emailEntidadeMensagem = $this->negocio->findOneBy(
                            '\G2\Entity\EmailEntidadeMensagem',
                            $paramsFindBy
                        );

                        if ($emailEntidadeMensagem->getId_textosistema()->getId_textosistema()) {
                            $retorno[$key]['id_textosistemaacordo'] =
                                $emailEntidadeMensagem->getId_textosistema()->getId_textosistema();
                        } else {
                            throw new Zend_Exception('Texto de sistema não cadastrado para essa entidade.');
                        }

                        //array com os parametros
                        $arrParamsLinkPagamento = array(
                            'id_venda' => $retorno[$key]['id_venda'],
                            'id_acordo' => $retorno[$key]['id_acordo'],
                            'id_lancamento' => $retorno[$key]['id_lancamento']
                        );
                        $paramsLinkPagamento = json_encode($arrParamsLinkPagamento);

                        //cifrando os parametros
                        $securityKey = $this->config()->security->key;
                        $paramsCifrado = \util\Cipher::encrypt($paramsLinkPagamento, $securityKey);
                        $retorno[$key]['parametros_cifrados'] = $paramsCifrado;

                    } else {
                        $retorno[$key]['primeira_parcela_acordo'] = 0;
                    }

                    //Verificando se e um lancamento oriundo de transferencia
                    if (!empty($retorno[$key]['id_vendaaditivo'])) {
                        $retorno[$key]['st_meiopagamento'] = $retorno[$key]['st_meiopagamento']
                            . ' - Transferência de Curso';
                    }
                }

                echo json_encode($retorno);
            } else {
                throw new Zend_Exception('O Código da Venda deve ser informada.');
            }
        } catch (Exception $e) {
            $this->getResponse()
                ->appendBody(json_encode(array('title' => 'Erro', 'type' => 'error', 'text' => $e->getMessage())))
                ->setHttpResponseCode(500);
        }
    }

    public function processoAtivacaoAutomaticaAction()
    {
        $this->getResponse()->setHeader('ContentType', 'application/json');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost(); //get data
            if (array_key_exists('id_venda', $data) && !empty($data['id_venda'])) {
                $mensageiro = $this->negocio->processoAtivacaoAutomatica($data['id_venda']);
                $this->getResponse()->appendBody($mensageiro->toJson());
            } else {
                $this->getResponse()
                    ->appendBody(json_encode(array(
                        'title' => 'Confirmação da venda',
                        'type' => 'error',
                        'text' => 'Necessário informar o código da venda.'
                    )));
            }
        } else {
            $this->getResponse()
                ->appendBody(json_encode(array(
                    'title' => 'Erro!',
                    'type' => 'error',
                    'text' => 'Operação inválida, a tipo de requisição deve ser um post.'
                )));
        }
    }

    public function confirmarRecebimentoAction()
    {
        $this->getResponse()->setHeader('ContentType', 'application/json');
        if (!$this->getParam('id_venda')) {
            throw new Zend_Exception('id_venda não encontrado.');
        }
        $mensageiro = $this->negocio->confirmarRecebimento($this->getParam('id_venda'));
        $this->getResponse()->appendBody($mensageiro->toJson());
    }

    public function ativarMatriculaAction()
    {
        $this->getResponse()->setHeader('ContentType', 'application/json');
        if (!$this->getParam('id_venda')) {
            throw new Zend_Exception('id_venda não encontrado.');
        }

        $mensageiro = $this->negocio->ativarMatricula($this->getParam('id_venda'));
        $this->getResponse()->appendBody($mensageiro->toJson());
    }

    public function pagarDinheiroAction()
    {
        $this->getResponse()->setHeader('ContentType', 'application/json');
        if (!$this->getParam('id_lancamento')) {
            throw new Zend_Exception('id_lancamento não encontrado.');
        }
        $mensageiro = $this->negocio->pagarDinheiro($this->getParam('id_lancamento'));
        $this->getResponse()->appendBody($mensageiro->toJson());
    }

    public function pagarEmpenhoAction()
    {
        $this->getResponse()->setHeader('ContentType', 'application/json');
        if (!$this->getParam('id_lancamento')) {
            throw new Zend_Exception('id_lancamento não encontrado.');
        }

        $mensageiro = $this->negocio->pagarEmpenho($this->getParam('id_lancamento'));
        $this->getResponse()->appendBody($mensageiro->toJson());
    }

    public function pagarTransferenciaAction()
    {
        $this->getResponse()->setHeader('ContentType', 'application/json');
        if (!$this->getParam('id_lancamento')) {
            throw new Zend_Exception('id_lancamento não encontrado.');
        }
        $mensageiro = $this->negocio->pagarTransferencia($this->getParam('id_lancamento'));
        $this->getResponse()->appendBody($mensageiro->toJson());
    }

    public function pagarBoletoAction()
    {
        $this->getResponse()->setHeader('ContentType', 'application/json');
        if (!$this->getParam('id_lancamento')) {
            throw new Zend_Exception('id_lancamento não encontrado.');
        }
        $lancamentoTO = new LancamentoTO();
        $lancamentoTO->setId_lancamento($this->getParam('id_lancamento'));
        $lancamentoTO->setDt_quitado($this->getParam('dt_quitado'));
        $lancamentoTO->setNu_quitado($this->getParam('nu_quitado'));
        $lancamentoTO->setBl_ativo(true);
        $lancamentoTO->setBl_quitado(true);
        $lancamentoTO->setBl_baixadofluxus(true);
        $lancamentoTO->setId_usuarioquitacao(Ead1_Sessao::getSessaoGeral()->id_usuario);
        $lancamentoTO->setNu_jurosboleto($this->getParam('nu_jurosboleto'));
        $lancamentoTO->setNu_descontoboleto($this->getParam('nu_descontoboleto'));

        $mensageiro = $this->negocio->pagarBoleto($lancamentoTO);
        $this->getResponse()->appendBody($mensageiro->toJson());
    }

    /**
     * Action para pagar cheque
     * Alterado por Kayo Silva <kayo.silva@unyleya.com.br> em 2014-10-19
     * Adicionado st_numcheque
     * @throws Zend_Exception
     */
    public function pagarChequeAction()
    {
        try {
            $this->getResponse()->setHeader('ContentType', 'application/json');
            if (!$this->getParam('id_lancamento')) {
                throw new Zend_Exception('id_lancamento não encontrado.');
            }

            $lancamentoTO = new LancamentoTO();
            $lancamentoTO->setId_lancamento($this->getParam('id_lancamento'));
            $lancamentoTO->setDt_quitado($this->getParam('dt_quitado'));
            $lancamentoTO->setNu_quitado($this->getParam('nu_quitado'));
            $lancamentoTO->setId_lancamento($this->getParam('id_lancamento'));
            $lancamentoTO->setBl_ativo(true);
            $lancamentoTO->setBl_quitado(true);
            $lancamentoTO->setSt_numcheque($this->getParam('st_numcheque'));

            $mensageiro = $this->negocio->pagarCheque($lancamentoTO);
            $this->getResponse()->appendBody($mensageiro->toJson());

        } catch (Exception $message) {
            $this->getResponse()->appendBody(json_encode(array(
                'title' => 'Pagamento em cheque',
                'type' => 'error',
                'text' => $message->getMessage()
            )));
        }
    }

    public function pagarDepositoAction()
    {
        $this->getResponse()->setHeader('ContentType', 'application/json');
        if (!$this->getParam('id_lancamento')) {
            throw new Zend_Exception('id_lancamento não encontrado.');
        }
        $mensageiro = $this->negocio->pagarDeposito($this->getParam('id_lancamento'));
        $this->getResponse()->appendBody($mensageiro->toJson());
    }

    /**
     * Alterado por Kayo Silva <kayo.silva@unyleya.com.br> em 2014-10-19
     * Adicionando alguns atributos que foram criados na tabela
     * @throws Zend_Exception
     */
    public function pagarCartaoDebitoAction()
    {
        $this->getResponse()->setHeader('ContentType', 'application/json');

        $dados = json_decode($this->getParam('lancamentos'), true);
        $parcelas = json_decode($this->getParam('parcelas'), true);


        $arrayLancamentos = array();
        if ($dados) {
            foreach ($dados as $data) {
                $to = new LancamentoTO();
                $to->setId_lancamento($data['id_lancamento']);
                $to->setId_cartaoconfig(isset($data['id_cartaoconfig']) ? $data['id_cartaoconfig'] : null);
                $to->setSt_autorizacao(isset($data['st_autorizacao']) ? $data['st_autorizacao'] : null);
                $to->setSt_ultimosdigitos(isset($data['digitos']) ? $data['digitos'] : null);
                $to->setBl_ativo(true);
                $to->setBl_quitado(true);

                if ($this->getParam('st_coddocumento')) {
                    $to->setSt_coddocumento($this->getParam('st_coddocumento'));
                } else if (isset($data['st_coddocumento'])) {
                    $to->setSt_coddocumento($data['st_coddocumento']);
                }
                if ($this->getParam('dt_prevquitado')) {
                    $to->setDt_prevquitado($this->getParam('dt_prevquitado'));
                } else if (isset($data['st_coddocumento'])) {
                    $to->setDt_prevquitado($this->negocio->converteDataBancoZendDate($data['dt_prevquitado']));
                }

                if ($this->getParam('nu_quitado')) {
                    $to->setNu_quitado($this->getParam('nu_quitado'));
                } else if (isset($data['nu_quitado'])) {
                    $to->setNu_quitado($data['nu_quitado']);
                }

                if ($this->getParam('id_meiopagamento')) {
                    $to->setNu_quitado($this->getParam('id_meiopagamento'));
                } else if (isset($data['id_meiopagamento'])) {
                    $to->setId_meiopagamento($data['id_meiopagamento']);
                }

                $arrayLancamentos[] = $to;
            }
        }

        if (count($arrayLancamentos) > 0) {

            $mensageiro = $this->negocio->pagarCartaoDebito($arrayLancamentos);
            if ($parcelas && $mensageiro->getType() == Ead1_IMensageiro::TYPE_SUCESSO) {

                if ($parcelas) {
                    $negocioVenda = new \G2\Negocio\Venda();
                    if ($negocioVenda->salvarArrayLancamentoDadosRecebimentoCartao($parcelas)) {
                        $mensageiro->addMensagem('Dados do cartão salvo nos lançamentos.');
                    }
                }
            }

            $this->getResponse()->appendBody($mensageiro->toJson());
        } else {
            throw new Zend_Exception('id_lancamento não encontrado.');
        }
    }

    public function pagarCartaoCreditoAction()
    {
        $this->getResponse()->setHeader('ContentType', 'application/json');
        $dados = json_decode($this->getRequest()->getRawBody(), true);
        $arrayLancamentos = array();

        foreach ($dados as $data) {
            $to = new LancamentoTO();
            $to->setId_lancamento($data['id_lancamento']);
            $to->setBl_ativo(true);
            $to->setBl_quitado(true);
            $to->setSt_coddocumento($data['st_coddocumento']);
            $to->setDt_prevquitado(new Zend_Date($data['dt_prevquitado']));
            $to->setId_meiopagamento($data['id_meiopagamento']);

            $arrayLancamentos[] = $to;
        }

        if (count($arrayLancamentos) > 0) {
            $mensageiro = $this->negocio->pagarCartaoCredito($arrayLancamentos);
            $this->getResponse()->appendBody($mensageiro->toJson());

        } else {
            throw new Zend_Exception('id_lancamento não encontrado.');
        }
    }

    public function getMeiosPagamentoVendaAction()
    {
        $this->getResponse()
            ->appendBody(json_encode(
                $this->negocio->retornarMeiosPagamentoDaVenda($this->getParam('id_venda', null))
            ));
    }

    public function enviarBoletosVendaAction()
    {
        if ($this->getParam('id_venda')) {
            $mensageiro = $this->negocio->enviarBoleto($this->getParam('id_lancamento'));
            if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $mensagemBO = new MensagemBO();
                $to = new VendaTO();
                $to->setId_venda($this->getParam('id_venda'));
                $to->fetch(true, true, true);
                $mensageiro = $mensagemBO
                    ->procedimentoGerarEnvioMensagemPadraoEntidade(MensagemPadraoTO::ENVIO_BOLETO, $to);
                return $this->getResponse()->appendBody($mensageiro->toJson());
            }
            return new \Ead1_Mensageiro(null, Ead1_IMensageiro::SUCESSO, null);
        } else {
            throw new Zend_Exception("Necessário informar o id_venda");
        }
    }

    public function enviarBoletoAction()
    {
        if (!$this->getParam('id_lancamento')) {
            throw new Zend_Exception("Necessário informar o id_lancamento");
        }
        $result = $this->negocio->enviarBoleto($this->getParam('id_lancamento'));
        $result->setCodigo(null);
        $this->getResponse()->appendBody($result->toJson());
    }

    public function enviarLinkCartaoCreditoAction()
    {
        if (!$this->getParam('id_venda')) {
            throw new Zend_Exception("Necessário informar o id_venda");
        }
        $this->getResponse()
            ->appendBody($this->negocio->enviarLinkCartaoCredito($this->getParam('id_venda'))->toJson());
    }

    public function enviarLinkCartaoCreditoAcordoAction()
    {
        if (!$this->getParam('id_venda')) {
            throw new Zend_Exception("Necessário informar o id_venda");
        }
        $id_venda = $this->getParam('id_venda');
        $id_acordo = $this->getParam('id_acordo');
        $id_lancamento = $this->getParam('id_lancamento');
        $this->getResponse()
            ->appendBody($this->negocio
                ->enviarLinkCartaoCreditoAcordo($id_venda, $id_acordo, $id_lancamento)->toJson()
            );

    }

    public function cancelarTransacaoCartaoCreditoAction()
    {
        if (!$this->getParam('id_venda')) {
            throw new Zend_Exception("Necessário informar o id_lancamento");
        }
        $this->getResponse()
            ->appendBody($this->negocio
                ->cancelarTransacaoCartaoCredito($this->getParam('id_venda'))->toJson()
            );
    }

    public function gerarListaChequesDevolvidosAction()
    {
        if ($this->getParam('lancamentos')) {
            $textoSistemaBO = new TextoSistemaBO();
            $efTO = new EntidadeFinanceiroTO();

            $efTO->setId_entidade(Ead1_Sessao::getSessaoGeral()->id_entidade);
            $efTO->fetch(false, true, true);

            $to = new TextoSistemaTO();

            $to->setId_textosistema($efTO->getId_textochequesdevolvidos());
            $to->fetch(true, true, true);

            echo $textoSistemaBO->gerarTexto($to,
                array(
                    'id_venda' => 84010,
                    'lancamentos' => $this->getParam('lancamentos')
                )
            )->getFirstMensagem()->getSt_texto();
        } else {
            die("Nenhuma venda ou lançamento informados.");
        }
    }

    public function enviarEmailListaChequesDevolvidosAction()
    {
        try {
            if ($this->getParam('lancamentos')) {
                $efTO = new EntidadeFinanceiroTO();

                $lancamentos = $this->getParam('lancamentos');
                $vwTO = new VwResumoFinanceiroTO();
                $vwTO->setId_lancamento($lancamentos[0]);
                $vwTO = $vwTO->fetch(false, true, false);

                $vendaTO = new VendaTO();
                $vendaTO->setId_venda($vwTO->getId_venda());
                $vendaTO->fetch(true, true, true);

                $efTO->setId_entidade(Ead1_Sessao::getSessaoGeral()->id_entidade);
                $efTO->fetch(false, true, true);

                $to = new TextoSistemaTO();

                $to->setId_textosistema($efTO->getId_textochequesdevolvidos());
                $to->fetch(true, true, true);

                $params = array(
                    'id_venda' => $vwTO->getId_venda(),
                    'lancamentos' => $this->getParam('lancamentos')
                );

                $emailConfigTO = new EmailConfigTO();
                $emailConfigTO->setId_entidade($vendaTO->getId_entidade());
                $emailConfigTO->fetch(false, true, true);
                $id_emailconfig = $emailConfigTO->getId_emailconfig();

                $mensagemBO = new MensagemBO();
                $mensageiro = $mensagemBO->enviarNotificacaoTextoSistema(
                    $to,
                    $params,
                    $id_emailconfig,
                    $vendaTO->getId_usuario()
                );
                echo json_encode($mensageiro->toArray());
            } else {
                $mensageiro = new Ead1_Mensageiro();
                echo json_encode($mensageiro->setMensageiro(
                    "Nenhuma venda ou lançamento informados.",
                    Ead1_IMensageiro::ERRO,
                    null
                )->toArray());
            }
        } catch (Exception $e) {
            $mensageiro = new Ead1_Mensageiro();
            echo json_encode($mensageiro->setMensageiro(
                $e->getMessage(),
                Ead1_IMensageiro::ERRO,
                null
            )->toArray());
        }
    }

    public function gerarBoletoAction()
    {
        try {

            $id_lancamento = $this->_getParam('id_lancamento', null);


            if (!$id_lancamento)
                throw new \Exception('Parâmetros informados não aplicados', 400);

            $negocio = new \G2\Negocio\BoletoPHP();
            $result = $negocio->emitirBoleto($id_lancamento, true);

            /*
             * verifica se o retorno é a instancia da entity de lançamento,
             * se for a instancia significa que retornou
             * o objeto então posso tentar fazer o toArray() no objeto, baseado na entity que esta
             * extendendo da classe
             * G2Entity
             *
             * Analisando o método emitir boleto para o parametro soDados == true, é possível que o método retorne
             * um objeto da entity de lançamento ou um array com os dados
             */
            if ($result instanceof \G2\Entity\Lancamento) {
                $this->json($result->toArray());
            } else {
                $this->json($result);
            }

        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->json($error->toArray());
        }
    }

    /**
     * Método que exibe as informações na aba Cartão Recorrente do menu Financeiro > Recebimento
     * @param none
     * @return Zend_View
     */
    public function cartaoRecorrenteAction(){
        try {
            $this->_helper->viewRenderer->setNoRender(false);
            $params = $this->getAllParams();

            if (empty($params['id_venda'])) {
                throw new Exception('Informe o ID da venda para recuperar as informações da assinatura.');
            }

            $dadosVenda = $this->negocio->recuperarInfoCartaoRecorrente($params['id_venda']);
            $this->view->data = $dadosVenda;
            $this->view->urlloja = Ead1_Ambiente::geral()->st_url_loja;
        } catch (Exception $e) {
            $this->view->erro = $e->getMessage();
        }
    }

}

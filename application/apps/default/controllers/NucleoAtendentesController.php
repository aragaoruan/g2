<?php

/**
 * Controller para funcionalidade Núcleo Atendentes
 * @author Vinícius Avelino Alcântara <vinicius.alcantara@unyleya.com.br>
 * @since 2016-02-24
 * @package application
 * @subpackage controller
 */
class Default_NucleoAtendentesController extends Ead1_Controller
{
    public function indexAction()
    {
        $id = $this->_getParam('id');

        if ($id) {
            $negocio = new G2\Negocio\NucleoTm();
            $result = $negocio->retornarNucleoTmById($id);
            $result = $negocio->toJsonEntity($result);
            $this->view->dados = $result;
        }
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
    }

    public function salvarNucleoAtendenteAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        //quando utilizar o save do backbone, lembre-se estamos trabalhando com REST, e é dessa
        // forma que pegamos os parametros que são enviados pra cá
        $body = $this->getRequest()->getRawBody();
        $params = Zend_Json_Decoder::decode($body);

        $nucleotm = new G2\Negocio\NucleoTm();
        $mensagem = $nucleotm->salvarNucleoAtendente($params);
        if ($mensagem->getTipo() == \Ead1_IMensageiro::SUCESSO) {
            //como estamos trabalhando com rest, o backbone espera receber sempre o objeto ou array de objetos,
            //nesse caso objeto
            return $this->getResponse()->setHttpResponseCode(201)->appendBody(json_encode($mensagem->getFirstMensagem()));
        } else {
            return $this->getResponse()->setHttpResponseCode(400)->appendBody(json_encode($mensagem->getFirstMensagem()));
        }
        //return $mensagem;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: aragaoruan
 * Date: 24/05/18
 * Time: 15:17
 */

class Default_VincularDisciplinaComplementarController extends Ead1_ControllerRestrita
{
    /**
     * @var \G2\Negocio\VincularDisciplinaComplementar
     */
    private $negocio;
    private $mensageiro;
    public function init()
    {
        parent::init();
        parent::setupDoctrine();
        $this->negocio = new \G2\Negocio\VincularDisciplinaComplementar();
        $this->mensageiro = new \Ead1_Mensageiro();
    }

    public function indexAction()
    {

    }

    /**Função que retorna matriculas cursando a partir de um id_ususario.
     * feature GII-9563
     * @throws Zend_Controller_Action_Exception
     * @throws Zend_Controller_Response_Exception
     * @author Ruan Aragão <ruan.aragao@unyleya.com.br>
     */
    public function retornarMatriculasAction()
    {
        try {
            $params = $this->limpaGetAllParams($this->getAllParams());

            if (array_key_exists('id_usuario', $params)) {
                $result = $this->negocio->retornarMatriculas($params['id_usuario']);
                $this->_helper->json($result);

            }

            $this->getResponse()->setHttpResponseCode(500);
            $this->_helper->json($this->mensageiro->setMensageiro(
                'Por favor informe um usuario.',
                \Ead1_IMensageiro::AVISO)
            );

        } catch (Exception $e) {
            $this->getResponse()->setHttpResponseCode(500);
            $this->_helper->json($e->getMessage());
        }
    }

    /** Função que retorna matricula complementar com evolução cursando e concluinte passando id_usuario
     * feature GII-9563
     * @throws Zend_Controller_Action_Exception
     * @throws Zend_Controller_Response_Exception
     * @author Ruan Aragão <ruan.aragao@unyleya.com.br>
     */
    public function retornaMatriculaComplementarAction()
    {
        try {
            $params = $this->limpaGetAllParams($this->getAllParams());

            if (array_key_exists('id_usuario', $params)) {
                $result = $this->negocio->retornaMatriculaComplementar($params['id_usuario']);
                $this->_helper->json($result);

            }

            $this->getResponse()->setHttpResponseCode(500);
            $this->_helper->json($this->mensageiro->setMensageiro(
                'Por favor informe um usuario.',
                \Ead1_IMensageiro::AVISO)
            );

        } catch (Exception $e) {
            $this->getResponse()->setHttpResponseCode(500);
            $this->_helper->json($e->getMessage());
        }
    }

    /**Função que retorna matricula complementar vinculada passando id_matricula
     * feature GII-9563
     * @throws Zend_Controller_Action_Exception
     * @throws Zend_Controller_Response_Exception
     * @author Ruan Aragão <ruan.aragao@unyleya.com.br>
     */
    public function retornarMatriculaComplementarVinculadaAction()
    {
        try {
            $params = $this->limpaGetAllParams($this->getAllParams());

            if (array_key_exists('id_matricula', $params)) {
                $result = $this->negocio->retornarMatriculaComplementarVinculada($params['id_matricula']);
                $this->_helper->json($result);

            }

            $this->getResponse()->setHttpResponseCode(500);
            $this->_helper->json($this->mensageiro->setMensageiro(
                'Por favor informe uma matricula.',
                \Ead1_IMensageiro::AVISO)
            );

        } catch (Exception $e) {
            $this->getResponse()->setHttpResponseCode(500);
            $this->_helper->json($e->getMessage());
        }
    }

    /**Função que vincula uma ou varias matriculas complementares em uma matricula
     * @throws Zend_Controller_Action_Exception
     * @throws Zend_Controller_Response_Exception
     */
    public function vincularMatriculasAction()
    {
        try {

            $params = $this->limpaGetAllParams($this->getAllParams());

            if (array_key_exists('id_matricula', $params) && array_key_exists('id_disciplinacomplementar', $params)) {
                $result = $this->negocio->vincularMatriculas($params);
                $this->_helper->json($result);

                if ($result->getTipo() == Ead1_IMensageiro::SUCESSO) {
                    $this->getResponse()->setHttpResponseCode(201);
                    $this->_helper->json($result);
                }

            }

            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json($this->mensageiro->setMensageiro(
                'Por favor informe uma matricula ou matricula complementar.',
                \Ead1_IMensageiro::AVISO)
            );

        } catch (Exception $e) {
            $this->getResponse()->setHttpResponseCode(500);
            $this->_helper->json($e->getMessage());
        }
    }

    public function desvincularMatriculaAction()
    {
        try {

            $params = $this->limpaGetAllParams($this->getAllParams());

            if (array_key_exists('id_matricula', $params) && array_key_exists('id_disciplinacomplementar', $params)) {
                $result = $this->negocio->desvincularMatricula($params);
                $this->_helper->json($result);

                if ($result->getTipo() == Ead1_IMensageiro::SUCESSO) {
                    $this->getResponse()->setHttpResponseCode(201);
                    $this->_helper->json($result);
                }

            }

            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json($this->mensageiro->setMensageiro(
                'Por favor informe uma matricula ou matricula complementar.',
                \Ead1_IMensageiro::AVISO)
            );

        } catch (Exception $e) {
            $this->getResponse()->setHttpResponseCode(500);
            $this->_helper->json($e->getMessage());
        }
    }

    /**Função que retornar o historico de disciplinas vinculadas e desvinculadas
     * @throws Zend_Controller_Action_Exception
     * @throws Zend_Controller_Response_Exception
     */
    public function retornarHistoricoAction()
    {
        try {
            $params = $this->limpaGetAllParams($this->getAllParams());

            if (array_key_exists('id_matricula', $params)) {
                $result = $this->negocio->retornarHistorico($params['id_matricula']);
                $this->_helper->json($result);

            }

            $this->getResponse()->setHttpResponseCode(500);
            $this->_helper->json($this->mensageiro->setMensageiro(
                'Por favor informe uma matricula.',
                \Ead1_IMensageiro::AVISO)
            );

        } catch (Exception $e) {
            $this->getResponse()->setHttpResponseCode(500);
            $this->_helper->json($e->getMessage());
        }
    }
}

<?php

/**
 * Controller para gerar o relat�rio de Gest�o de Ocorr�ncias.
 * @author Marcus Pereira <marcus.pereira@unyleya.com.br>
 * @package application
 * @subpackage controller
 */

class Default_GestaoDeOcorrenciasController extends Ead1_Controller {

    public function jsAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('gestao-de-ocorrencias/index.js');
    }

    public function init() {

    }

    public function indexAction(){


    }
}
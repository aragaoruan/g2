<?php

/**
 * Classe de controller da pesquisa dininâmicas do sistema
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2013-07-24
 * @update 2013-11-06
 * @package application
 * @subpackage controller
 */

use Ead1\Doctrine\EntitySerializer;
use Ead1\Pesquisa;
use G2\Negocio\Funcionalidade;

class Default_PesquisaController extends Ead1_ControllerRestrita
{

    /**
     * @var PesquisarBO PesquisarBO
     */
    private $_pesquisarBo;

    public function init()
    {
        parent::init();
        # Instancia a BO
        $this->_pesquisarBo = new PesquisarBO();
    }

    /**
     * Index do formulário dinamico
     * @param
     * @return view
     */
    public function indexAction()
    {
        $funcionalidadeNegocio = new Funcionalidade();
        $em = $this->_pesquisarBo->em;
        $serialize = new EntitySerializer($em);
        $this->_helper->layout->disableLayout();
        $pesquisa = new Pesquisa(); // Instância a classe Ead1\Pesquisa

        $tipoBusca = $this->getParam('tipo'); // Recupera o parâmetro pela rota 'tipo'

        if (!empty($tipoBusca)) {
            $pesquisa->setTipo($tipoBusca);

            $returnBo = $this->_pesquisarBo->retornarCamposPesquisa($pesquisa);

            if ($returnBo) {
                $resultFuncionalidade = $funcionalidadeNegocio->findFuncionalidade($returnBo->getId_funcionalidade());
                $grid = $returnBo->getCamposGrid();
                $arrGrid = array();
                if ($grid) {
                    $i = 0;
                    foreach ($grid as $rowGrid) {
                        if ($rowGrid->visible) {
                            $arrGrid[$i]['label'] = $rowGrid->label;
                            $arrGrid[$i]['name'] = $rowGrid->field;
                            $arrGrid[$i]['visible'] = $rowGrid->visible;
                            $arrGrid[$i]['cell'] = 'string';
                            $arrGrid[$i]['editable'] = false;
                            $i++;
                        }
                    }
                }

                #manda a grid para view
                $this->view->gridCampos = array('grid' => $arrGrid);

                $arrFilter = $this->getArrFilter($returnBo);
                $arrFilter['funcionalidade'] = $serialize->toArray($resultFuncionalidade);
                $arrFilter['to'] = $returnBo->getTo_envia();
                $arrFilter['tipo'] = $returnBo->getTipo();

                if ($arrFilter) {
                    //instancia o form e passa como parametro o array com os inputs
                    $this->view->form = new \PesquisaForm($arrFilter); //forms
                }
            }
        }
    }

    /**
     * Percorre o objeto da BO e retorna o array com os campos que serï¿½o gerados para o filtro
     * @param obj $objFilter Objeto retornado da BO
     * @return array
     */
    private function getArrFilter($objFilter)
    {
        $arrayReturn = array();
        if ($objFilter) {
            foreach ($objFilter as $key => $row) {
                if ($key == 'metodo') {
                    $arrayReturn['actionPesquisar'] = $row;
                }
                if ($key == 'filtros') {
                    foreach ($row as $i => $rowFilter) {
                        $arrayReturn[$i]['label'] = $rowFilter->st_label;
                        $arrayReturn[$i]['type'] = $rowFilter->st_tipo;
                        $arrayReturn[$i]['value'] = $rowFilter->valor;
                        $arrayReturn[$i]['name'] = $rowFilter->st_propriedade;
                    }
                }
            }
        }
        return $arrayReturn;
    }

    /**
     * Action de pesquisar, recebe os parametros da pesquisa dinamica e executa respectivo método
     * @param PesquisaForm $request
     * @return json
     */
    public function pesquisarAction()
    {
        try {
            $this->_helper->viewRenderer->setNoRender();
            $params = $this->limpaGetAllParams($this->getRequest()->getParams());
            if ($params) {
                # forms
                $form = new PesquisaForm();
                # valida o form
                if ($form->isValid($params)) {
                    #limpa espaços antes de depois da string da busca \t \n por exemplo
                    if (isset($params['txtSearch']))
                        $params['txtSearch'] = trim($params['txtSearch']);

                    //Unset no campo que não será utilizado no restante da funcionalidade
                    unset($params['tipo']);

                    #renderiza o respectivo metodo da consulta
                    $method = $params['actionMethod'];
                    unset($params['actionMethod']);

                    // Verifica parametros de paginacao
                    $limit = (int)$this->getParam('per_page');
                    $offset = (intval($this->getParam('page')) - 1) * $limit;

                    $sortBy = $this->getParam('sort_by');
                    $order = $this->getParam('order');
                    if ($sortBy) {
                        $orderBy = array(
                            $sortBy => $order ? $order : 'ASC'
                        );
                    } else
                        $orderBy = null;
                    $grid = $this->{$method}($params, $orderBy, $limit, $offset);
                    // Verifica se e paginado ou nao
                    if (isset($grid->rows))
                        $entities = $grid->rows;
                    else
                        $entities = $grid;

                    if ($entities) {
                        foreach ($entities as $i => $row) {
                            $j = 0;
                            foreach ($row as $colum => $valor) {
                                if ($j == 0)
                                    $entities[$i]['id'] = $row[$colum];
                                if ($valor instanceof DateTime) {
                                    $entities[$i][$colum] = $valor->format("d/m/Y");
                                } else {
                                    /* Remove tags HTML possívelmente inseridas
                                    com o TinyMCE, deixando os textos sem formatação,
                                    porém legíveis em telas que exibem apenas uma prévia.
                                    */
                                    $entities[$i][$colum] = strip_tags(html_entity_decode($entities[$i][$colum]));
                                }
                                $j++;
                            }
                        }

                        if (isset($grid->rows))
                            $grid->rows = $entities;
                        else
                            $grid = $entities;

                        $this->getResponse()
                            ->setHttpResponseCode(200)
                            ->setHeader('Content-type', 'application/json', true)
                            ->appendBody(json_encode($grid));
                    } else {
                        $this->getResponse()->setHttpResponseCode(204)->appendBody('Nenhum resultado encontrado.');
                    }
                }
            }
        } catch (Exception $e) {
            $this->getResponse()
                ->setHttpResponseCode(500)
                ->appendBody($e->getMessage());
        }
    }

    /**
     * Metodo que retorna dados de Pesquisa de Organizações (Grupo, Instituição, Núcleo, Polo)
     * @param array $params
     * @return array
     */
    public function pesquisarOrganizacao(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        #seta os parametros
        if (isset($params['bl_grupo']) && !empty($params['bl_grupo'])) {
            $params['st_nomeentidade'] = $params['txtSearch'];
        }
        return $this->_pesquisarBo->retornaPesquisaPessoaJuridica($params, $orderBy, $limit, $offset);
    }

    /**
     * Metodo que retorna os dados da Pesquisa de protocolo
     * @param array $params
     * @return array
     */
    public function pesquisarProtocolo(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        return $this->_pesquisarBo->retornaPesquisarProtocolo($params, $orderBy, $limit, $offset);
    }

    /**
     * Pesquisa de Assunto do Central de Ocorrências
     * @param array $params
     * @return array
     */
    public function pesquisarAssuntoCo(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        if ($params['st_assuntoco']) {
            $params['st_assuntoco'] = $params['txtSearch'];
        }
        if ($params['st_assuntocopai']) {
            $params['st_assuntocopai'] = $params['txtSearch'];
        }
        return $this->_pesquisarBo->retornaPesquisaAssuntoCo($params, $orderBy, $limit, $offset);
    }

    /**
     * Pesquisa de Mensagem Motivacional
     * @param array $params
     * @return array
     */
    public function pesquisarMensagemMotivacional(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        if ($params['st_mensagemmotivacional']) {
            $params['st_mensagemmotivacional'] = $params['txtSearch'];
        }
        return $this->_pesquisarBo->retornaPesquisaMensgemMotivacional($params, $orderBy, $limit, $offset);
    }

    /**
     * Metodo que retorna os dados da Pesquisa de pessoa juridica
     * @param array $params
     * @return array
     */
    public function pesquisarPessoaJuridica(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        if ($params['st_nomeentidade']) {
            $params['st_nomeentidade'] = $params['txtSearch'];
        }
        if ($params['st_razaosocial']) {
            $params['st_razaosocial'] = $params['txtSearch'];
        }
        if ($params['st_cnpj']) {
            $params['st_cnpj'] = $params['txtSearch'];
        }

        return $this->_pesquisarBo->retornaPesquisaPessoaJuridica($params, $orderBy, $limit, $offset);
    }

    /**
     * Método para fazer o download do ZIP com os dados para o CENSO
     */
    public function gerarCenso()
    {
        $pessoaNogocio = new \G2\Negocio\Censo();
        $pessoaNogocio->gerarArquivoAluno();
    }

    /**
     * Metodo que retorna dados da pesquisa de pessoa
     * @param array $params Parametros da busca
     * @return array
     */
    public function pesquisarPessoa(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        if ($params['st_nomecompleto']) {
            $params['st_nomecompleto'] = $params['txtSearch'];
        }
        if ($params['st_email']) {
            $params['st_email'] = $params['txtSearch'];
        }
        if ($params['st_cpf']) {
            $params['st_cpf'] = $params['txtSearch'];
        }
        if ($params['id_usuario']) {
            $params['id_usuario'] = $params['txtSearch'];
        }
        if ($params['id_situacao']) {
            $params['id_usuario'] = $params['txtSearch'];
        }

        $pessoaNogocio = new \G2\Negocio\Pessoa();
        return $pessoaNogocio->retornaPesquisarPessoa($params, $orderBy, $limit, $offset);
    }

    public function pesquisarTelemarketing(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        return array();
    }

    /**
     * Pesquisa a regra de contrato
     * @param array $params
     * @return array
     */
    public function pesquisarRegraContrato(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        if ($params['st_contratoregra']) {
            $params['st_contratoregra'] = $params['txtSearch'];
        }
        return $this->_pesquisarBo->retornaPesquisaRegraContrato($params, $orderBy, $limit, $offset);
    }

    /**
     * Pesquisa relatórios
     * @param PesquisaRelatorioTO $prTO
     * @return array
     */
    public function pesquisarRelatorio(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        if ($params['st_relatorio']) {
            $params['st_relatorio'] = $params['txtSearch'];
        }
        return $this->_pesquisarBo->retornaPesquisaRelatorio($params, $orderBy, $limit, $offset);
    }

    /**
     * Pesquisa a venda
     * @param PesquisarVendaTO pvrTO
     * @return array
     */
    public function pesquisarVendaGraduacao(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        if ($params['st_nomecompleto']) {
            $params['st_nomecompleto'] = $params['txtSearch'];
        }
        return $this->_pesquisarBo->retornaPesquisaVendaGraduacao($params, $orderBy, $limit, $offset);
    }

    /**
     * Pesquisa a venda
     * @param PesquisarVendaTO pvrTO
     * @return array
     */
    public function pesquisarVenda(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        if (isset($params['st_nomecompleto']) && $params['st_nomecompleto']) {
            $params['st_nomecompleto'] = $params['txtSearch'];
        }
        if (isset($params['st_cpf']) && $params['st_cpf']) {
            $params['st_cpf'] = $params['txtSearch'];
        }
        if (isset($params['st_email']) && $params['st_email']) {
            $params['st_email'] = $params['txtSearch'];
        }
        return $this->_pesquisarBo->retornaPesquisaVenda($params, $orderBy, $limit, $offset);
    }

    /**
     * Metodo que retorna os dados da Pesquisa de Avaliacao
     * @param PesquisarTurmaTO $to
     * @return array
     */
    public function pesquisarTurma(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        if ($params['st_turma']) {
            $params['st_turma'] = $params['txtSearch'];
        }
        return $this->_pesquisarBo->retornaPesquisaTurma($params, $orderBy, $limit, $offset);
    }

    public function pesquisarPerfil(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        return array();
    }

    /**
     * Método que pesquisa as disciplinas
     * @param array $params
     * @return array
     */
    public function pesquisarDisciplina(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        if ($params['st_disciplina']) {
            $params['st_disciplina'] = $params['txtSearch'];
        }
        if ($params['st_areaconhecimento']) {
            $params['st_areaconhecimento'] = $params['txtSearch'];
        }
        return $this->_pesquisarBo->retornaPesquisarDisciplina($params, $orderBy, $limit, $offset);
    }

    /**
     * Método que pesquisa as Áreas de conhecimento
     * @param array $params
     * @return array
     */
    public function pesquisarArea(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        if ($params['st_areaconhecimento']) {
            $params['st_areaconhecimento'] = $params['txtSearch'];
        }
        return $this->_pesquisarBo->retornaPesquisarArea($params, $orderBy, $limit, $offset);
    }

    /**
     * Método que pesquisa as salas de aula de uma entidade
     * @param array $params
     * @return array
     */
    public function pesquisarSalaDeAula(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        //Aqui
        if ($params['st_disciplina']) {
            $params['st_disciplina'] = $params['txtSearch'];
        }
        if ($params['st_saladeaula']) {
            $params['st_saladeaula'] = $params['txtSearch'];
        }
        if ($params['st_sistema']) {
            $params['st_sistema'] = $params['txtSearch'];
        }
        if ($params['st_professor']) {
            $params['st_nomeprofessor'] = $params['txtSearch'];
        }
        return $this->_pesquisarBo->retornaPesquisaSalaDeAula($params, $orderBy, $limit, $offset);
    }

    /**
     * Método que pesquisa o aproveitamento de uma entidade
     * @param array $params
     * @return array
     */
    public function pesquisarAproveitamento(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        if ($params['st_nomecompleto']) {
            $params['st_nomecompleto'] = $params['txtSearch'];
        }
        if ($params['st_disciplinaoriginal']) {
            $params['st_disciplinaoriginal'] = $params['txtSearch'];
        }
        if ($params['st_tituloexibicao']) {
            $params['st_tituloexibicao'] = $params['txtSearch'];
        }
        return $this->_pesquisarBo->retornaPesquisaAproveitamento($params, $orderBy, $limit, $offset);
    }

    /**
     * Método que pesquisa campanha
     * @param array $params
     * @return array
     */
    public function pesquisarCampanhaComercial(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        return $this->_pesquisarBo->retornaPesquisaCampanhaComercial($params, $orderBy, $limit, $offset);
    }

    /**
     * Método que pesquisa o conjunto de avaliacao
     * @param array $params
     * @return array
     */
    public function pesquisarConjuntoAvaliacao(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        if ($params['st_avaliacaoconjunto']) {
            $params['st_avaliacaoconjunto'] = $params['txtSearch'];
        }
        return $this->_pesquisarBo->retornaPesquisaConjuntoAvaliacao($params, $orderBy, $limit, $offset);
    }

    /**
     *
     * Método que pesquisa os projetos pedagógicos
     * @param array $params
     * @return array
     */
    public function pesquisarProjetoPedagogico(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        if ($params['st_descricao']) {
            $params['st_descricao'] = $params['txtSearch'];
        }
        if ($params['st_projetopedagogico']) {
            $params['st_projetopedagogico'] = $params['txtSearch'];
        }

        return $this->_pesquisarBo->retornaPesquisaProjetoPedagogico($params, $orderBy, $limit, $offset);
    }

    /**
     * Método que retorna dados da Pesquisa de Afiliado
     * @param array $params
     * @return array
     */
    public function pesquisarContratoAfiliado(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        if ($params['id_contratoafiliado']) {
            $params['id_contratoafiliado'] = $params['txtSearch'];
        }
        if ($params['st_contratoafiliado']) {
            $params['st_contratoafiliado'] = $params['txtSearch'];
        }

        return $this->_pesquisarBo->retornaPesquisaContratoAfiliado($params, $orderBy, $limit, $offset);
    }

    /**
     *
     * Pesquisa o contrato
     * @param array $params
     * @return array
     */
    public function pesquisarContrato(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        if ($params['st_nomecompleto']) {
            $params['st_nomecompleto'] = $params['txtSearch'];
        }
        return $this->_pesquisarBo->retornaPesquisaContrato($params, $orderBy, $limit, $offset);
    }

    /**
     * Metodo que retorna os dados da pesquisaProduto
     * @param array $params
     * @return array
     */
    public function pesquisarProduto(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        if ($params['st_produto']) {
            $params['st_produto'] = $params['txtSearch'];
        }
        return $this->_pesquisarBo->retornaPesquisaProduto($params, $orderBy, $limit, $offset);
    }

    /**
     * Metodo que retorna os dados da Pesquisa de Item de Material
     * @param array $params
     * @return array
     */
    public function pesquisarItemMaterial(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        if ($params['st_itemdematerial']) {
            $params['st_itemdematerial'] = $params['txtSearch'];
        }

        return $this->_pesquisarBo->retornaPesquisaItemMaterial($params, $orderBy, $limit, $offset);
    }

    /**
     * Pesquisa a Forma de Pagamento
     * @param array $params
     * @return array
     */
    public function pesquisarFormaDePagamento(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        if ($params['st_formapagamento']) {
            $params['st_formapagamento'] = $params['txtSearch'];
        }
        return $this->_pesquisarBo->retornaPesquisaFormaDePagamento($params, $orderBy, $limit, $offset);
    }

    /**
     * Metodo que retorna os dados da Pesquisa de Agendamento de Avaliacao
     * @param array $params
     * @return array
     */
    public function pesquisarAgendamentoAvaliacao(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        if ($params['st_avaliacao']) {
            $params['st_avaliacao'] = $params['txtSearch'];
        }
        return $this->_pesquisarBo->pesquisarAgendamentoAvaliacao($params, $orderBy, $limit, $offset);
    }

    /**
     * Metodo que retorna os dados da Pesquisa de textos do sistema
     * @param array $params
     * @return array
     */
    public function pesquisarTextosSistema(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        if ($params['st_textosistema']) {
            $params['st_textosistema'] = $params['txtSearch'];
        }
        return $this->_pesquisarBo->pesquisarTextosSistema($params, $orderBy, $limit, $offset);
    }

    /**
     * Metodo que retorna os dados da Pesquisa de aplicacao de Avaliacao
     * @param array $params
     * @return array
     */
    public function pesquisarAplicacaoAvaliacao(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        if (!isset($params['bl_ativo']))
            $params['bl_ativo'] = 1;
        return $this->_pesquisarBo->retornaPesquisaAplicacaoAvaliacao($params, $orderBy, $limit, $offset);
    }

    /**
     * Metodo que retorna os dados da Pesquisa de Avaliacao
     * @param array $params
     * @return array
     */
    public function pesquisarAvaliacao(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        if ($params['st_avaliacao']) {
            $params['st_avaliacao'] = $params['txtSearch'];
        }
        return $this->_pesquisarBo->retornaPesquisaAvaliacao($params, $orderBy, $limit, $offset);
    }

    /**
     * Metodo dpara retornar dados da pesquisa de Arquivos Retorno
     * @param array $params
     * @return array
     */
    public function pesquisarArquivoRetorno(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        if ($params['st_arquivoretorno']) {
            $params['st_arquivoretorno'] = $params['txtSearch'];
        }
        return $this->_pesquisarBo->retornaPesquisaArquivoRetorno($params, $orderBy, $limit, $offset);
    }

    /**
     * Pesquisa de Categorias de Produtos do Financeiro
     * @param array $params
     * @return array
     */
    public function pesquisarCategoria(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        if ($params['st_categoria']) {
            $params['st_categoria'] = $params['txtSearch'];
        }
        if ($params['st_categoriapai']) {
            $params['st_categoriapai'] = $params['txtSearch'];
        }
        return $this->_pesquisarBo->retornaPesquisaCategoria($params, $orderBy, $limit, $offset);
    }

    /**
     * Pesquisa de Classes de Afiliados
     * @param array $params
     * @return array
     */
    public function pesquisarClasseAfiliado(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        return $this->_pesquisarBo->retornaPesquisaClasseAfiliado($params, $orderBy, $limit, $offset);
    }

    /**
     * Pesquisa de Setores
     * @param array $params
     * @return array
     */
    public function pesquisarSetor(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        return $this->_pesquisarBo->retornaPesquisaSetor($params, $orderBy, $limit, $offset);
    }

    /**
     * Pesquisa de Livros
     * @param array $params
     * @return array
     */
    public function pesquisarLivro(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        return $this->_pesquisarBo->retornaPesquisaLivro($params, $orderBy, $limit, $offset);
    }

    public function pesquisarAplicadorProva(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        return $this->_pesquisarBo->retornaPesquisaAplicadorProva($params, $orderBy, $limit, $offset);
    }

    /**
     * Retorna dados da pesquisa de concurso
     * @param array $params
     */
    public function pesquisarConcursos(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {

        if ($params['st_concurso']) {
            $params['st_concurso'] = $params['txtSearch'];
        }
        return $this->_pesquisarBo->retornaPesquisaConcurso($params, $orderBy, $limit, $offset);
    }

    /**
     * Retorna pesquisa de grade horaria
     * @param array $params
     * @return array
     * @throws Zend_Exception
     */
    public function pesquisarGrade(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        if ($params['st_nomegradehoraria']) {
            $params['st_nomegradehoraria'] = $params['txtSearch'];
        }
        $negocio = new \G2\Negocio\GradeHoraria();
        return $negocio->retornarPesquisaGradeHoraria($params, $orderBy, $limit, $offset);

    }


    /**
     * Retorna dados da pesquisa de mensagens cadastradas para exibição no portal
     * @param array $params
     */
    public function pesquisarMensagemPortal(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        if ($params['st_mensagem']) {
            $params['st_mensagem'] = $params['txtSearch'];
        }
        if ($params['st_projetopedagogico']) {
            $params['st_projetopedagogico'] = $params['txtSearch'];
        }

        return $this->_pesquisarBo->retornaPesquisaMensagemPortal($params, $orderBy, $limit, $offset);
    }

    /**
     * Retorna pesquisa de Motivos
     * @param array $params
     * @return array
     * @throws Zend_Exception
     */
    public function pesquisarMotivo(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        if ($params['st_motivo']) {
            $params['st_motivo'] = $params['txtSearch'];
        }
        if (!$params['id_situacao']) {
            unset($params['id_situacao']);
        }

        $negocio = new \G2\Negocio\Motivo();
        return $negocio->retornarPesquisaMotivo($params, $orderBy, $limit, $offset);
    }

    /**
     * Metodo que retorna dados da pesquisa de pessoa
     * @param array $params Parametros da busca
     * @return array
     */
    public function pesquisarAtendente(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        if ($params['st_nomecompleto']) {
            $params['st_nomecompleto'] = $params['txtSearch'];
        }
        if ($params['st_cpf']) {
            $params['st_cpf'] = $params['txtSearch'];
        }
        if (!$params['id_nucleotm']) {
            unset($params['id_nucleotm']);
        }
        if (!$params['id_situacao']) {
            unset($params['id_situacao']);
        }

        $pessoaNogocio = new \G2\Negocio\VwAtendente();
        return $pessoaNogocio->retornarAtendentes($params, $orderBy, $limit, $offset);
    }

    /**
     * Metodo que retorna dados da pesquisa de pessoa
     * @param array $params Parametros da busca
     * @return array
     */
    public function pesquisarHolding($params)
    {
        if ($params['st_holding']) {
            $params['st_holding'] = $params['txtSearch'];
        }

        if (!$params['id_situacao']) {
            unset($params['id_situacao']);
        }

        $pessoaNogocio = new \G2\Negocio\Holding();
        return $pessoaNogocio->retornarHolding($params);
    }

    /**
     * @param $params
     * @return mixed
     * @throws Zend_Exception
     */
    public function pesquisarEsquemaConfiguracao(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        $esquemaNegocio = new \G2\Negocio\EsquemaConfiguracao();
        return $esquemaNegocio->retornarEsquema($params, $orderBy, $limit, $offset);
    }

    /**
     * Retorna a pesquisa de perguntas frequentes.
     * @param array $params
     * @return mixed
     * @throws Zend_Exception
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     */
    public function pesquisarPerguntasFrequentes(array $params = array(), $orderBy = null, $limit = 0, $offset = 0)
    {
        if ($params['st_esquemapergunta']) {
            $params['st_esquemapergunta'] = $params['txtSearch'];
        }
        $negocio = new \G2\Negocio\PerguntasFrequentes();
        return $negocio->retornarPesquisaPerguntasFrequentes($params, $orderBy, $limit, $offset);
    }

    /**
     * Retorna a pesquisa de núcleos.
     * @param array $params
     * @return mixed
     * @throws Zend_Exception
     * @author Vinícius Avelino Alcântara <vinicius.alcantara@unyleya.com.br>
     */
    public function pesquisarNucleoTm(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {

        if ($params['st_nucleotm']) {
            $params['st_nucleotm'] = $params['txtSearch'];
        }


        $negocio = new \G2\Negocio\NucleoTm();
        $return = $negocio->retornarPesquisaNucleo($params, $orderBy, $limit, $offset);

        $this->_helper->json($return);
    }

    /**
     * Pesquisa a venda para a funcionalidade Alterar Negociacao
     * @param array $params
     * @param array|null $orderBy
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function pesquisarVendaAlterarNegociacao(array $params, array $orderBy = null, $limit = 0, $offset = 0)
    {
        if ($params['st_nomecompleto']) {
            $params['st_nomecompleto'] = $params['txtSearch'];
        }
        $params['id_evolucao'] = \G2\Constante\Evolucao::TB_VENDA_CONFIRMADA;
        return $this->_pesquisarBo->retornaPesquisaVenda($params, $orderBy, $limit, $offset);
    }

    /**
     * Retorna a pesquisa da forma de ingresso
     * @param array $params
     * @param array|null $orderBy
     * @param int $limit
     * @param int $offset
     * @return array|null
     */
    public function pesquisarFormaIngresso(array $params, array $orderBy = null, $limit = 10, $offset = 0)
    {
        $negocio = new \G2\Negocio\TipoSelecao();
        return $negocio->retornarPesquisa($params, $orderBy, $limit, $offset);

    }

    /**
     * @param array $params
     * @param array|null $orderBy
     * @param int $limit
     * @param int $offset
     * @return array
     * @throws Exception
     */
    public function pesquisaCertificacaoParcial(array $params, array $orderBy = null, $limit = 10, $offset = 0)
    {
        $negocio = new \G2\Negocio\ConfiguracaoCertificadoParcial();
        return $negocio->retornaCertificadoParcial($params);
    }


}

<?php


use  \G2\Utils\Helper;

/**
 * Class Default_DiplomacaoController
 * Classe de controle para funcionalidade de Diplomação
 */
class Default_DiplomacaoController extends Ead1_ControllerRestrita
{
    /**
     * @var \G2\Negocio\Diplomacao $negocio
     */
    public $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Diplomacao();
    }

    public function indexAction()
    {
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('diplomacao/index.js');
    }

    public function retornaAlunosDiplomacaoAction()
    {
        $results = $this->negocio->pesquisaAlunosDiplomacao($this->getAllParams());

        $response = (new \G2\Transformer\MatriculaDiplomacao\MatriculaDiplomacaoTransformer($results))
            ->getTransformer();

        $this->_helper->json($response);
    }

    public function salvarEtapaAction()
    {
        try {
            $result = $this->negocio->salvarEtapaArray($this->limpaGetAllParams($this->getAllParams()));
            if ($result->getType() != Ead1_IMensageiro::TYPE_SUCESSO) {
                throw  new Exception($result->getText());
            }
            $this->_helper->json($result);
        } catch (Exception $e) {
            return $this->getResponse()
                ->setHttpResponseCode(500)
                ->appendBody($e->getMessage());
        }
    }

    public function gerarXlsAction()
    {
        header('Content-disposition: attachment; filename="newfile.xml"');
        header('Content-type: "text/xml"; charset="utf8"');
        file_put_contents('newfile.xml', $this->getAllParams()['data']);
        readfile('\xls.xls');
    }

    public function gerarDocumentoAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        $documento = $this->getParam('documento');
        $id_matricula = $this->getParam('id_matricula');
        $download = $this->getParam('download');
        $origem = $this->getParam('origem');

        if (!$id_matricula) {
            echo 'É necessário informar a matrícula.';
        }

        if (!$documento) {
            echo 'É necessário informar o documento.';
        }

        if ($this->getParam('draft')) {
            echo '<img src="/img/background/draft.jpg"'
                . 'style="position: fixed; top: 50%; left: 50%; margin: -495px 0 0 -600px; z-index: -1;" />';
        }

        if ($id_matricula && $documento) {
            echo $this->negocio->gerarDocumento($id_matricula, $documento, $download, $origem);
        }
    }
}

<?php

class Default_ActorserviceController extends Ead1_Controller
{


    public function indexAction()
    {
    	/*
    	 * PESSOA
    	 * 
    	$client = new Zend_Rest_Client('http://actor.localhost.com.br/services/pessoa.php');
		
		 $client->retornapessoa()
				  ->codpessoa(4781)
				  ->chavea('c81e728d9d4c2f636f067f89cc14862c')
				  ->chave('692d72b78b302e036fd67e546d26f557');
		$dados =  $client->get()->retornapessoa);
    	Zend_Debug::dump($dados);
    	

		$client	->inserirpessoa()
				->chavea('c81e728d9d4c2f636f067f89cc14862c')
				->chave('692d72b78b302e036fd67e546d26f557')
				->dados(array('nompessoa'=>'Criado pelo WebService', 'numcpf'=>'8888888888'));
		//Zend_Debug::dump($client->post());
		$dados =  $client->post()->inserirpessoa);
    	Zend_Debug::dump($dados);
		
		
		$client	->atualizarpessoa()
				->chavea('c81e728d9d4c2f636f067f89cc14862c')
				->chave('692d72b78b302e036fd67e546d26f557')
				->dados(array('codpessoa'=>$dados['response']['registros']['0']['codpessoa'], 'nompessoa'=>$dados['response']['registros']['0']['nompessoa'].' e Atualizado pelo WebService', 'numcpf'=>'89285243676'));
		//Zend_Debug::dump($client->post());
		$dados =  $client->post()->atualizarpessoa);
    	Zend_Debug::dump($dados);
    	*/
    	

    	
    	

    	
   	
    }
    
    
    public function disciplinaAction(){
    	

        $client = new Zend_Rest_Client('http://actor.admescolar.com.br/services/disciplina.php');
		
    	/*
    	 * Criando UMA DISCIPLINA no Actor 2
    	 * */
    	try {
			
			$client	->inserirdisciplina()
					->chavea('c81e728d9d4c2f636f067f89cc14862c')
					->chave('692d72b78b302e036fd67e546d26f557')
					->dados(
						array(
							 'nommodulo'	=>'Criado pelo WebService Eder'
							/*,'nommodulosite'=>'Criado pelo WebService Eder'
							,'codarea'		=>'2'
							,'desmodulo'	=>'Descrição do módulo Criado pelo WebService Eder'
							,'desurl'		=>'http://www.google.com'
							,'numhoras'		=>'450'*/
						)
					);
			$dados =  $client->post()->inserirdisciplina;
			echo '<fieldset><legend>Criando UMA DISCIPLINA no Actor 2</legend>';
    		Zend_Debug::dump($dados);
    		Zend_Debug::dump(empty($dados->retorno));
    		Zend_Debug::dump($dados->retorno == 'sucesso');
    		echo '</fieldset>';
		} catch (exception $exception) {
    		Zend_Debug::dump($exception);
		}
    	exit;
		$codmodulo = $dados['disciplina']['codmodulo'];
    	
    	/*
    	 * Alterando UMA DISCIPLINA no Actor 2
    	 * */
    		try {
			
			$client	->atualizardisciplina()
					->chavea('c81e728d9d4c2f636f067f89cc14862c')
					->chave('692d72b78b302e036fd67e546d26f557')
					->dados(
						array(
							 'codmodulo'	=>$codmodulo
							,'nommodulo'	=>' Alterado pelo WebService '
							,'nommodulosite'=>' Alterado pelo WebService '
							,'codarea'		=>'2'
							,'desmodulo'	=>'Descrição do módulo Criado pelo WebService'
							,'desurl'		=>'http://www.globo.com'
							,'numhoras'		=>'450'
						)
					);
			$dados =  $client->post()->atualizardisciplina;
			echo '<fieldset><legend>Alterando UMA DISCIPLINA no Actor 2</legend>';
    		Zend_Debug::dump($dados);
    		echo '</fieldset>';
	
		} catch (exception $exception) {
			Zend_Debug::dump($exception);
		}
		
		
            /*
    	 * Recuperando UMA DISCIPLINA no Actor 2
    	 * */
    	 try {
			 $client->retornadisciplina()
					  ->codmodulo($codmodulo)
					  ->chavea('c81e728d9d4c2f636f067f89cc14862c')
					  ->chave('692d72b78b302e036fd67e546d26f557');
			$dados =  $client->get()->retornadisciplina;
			echo '<fieldset><legend>Recuperando UMA DISCIPLINA no Actor 2</legend>';
    		Zend_Debug::dump($dados);
    		echo '</fieldset>';
    	} catch (exception $exception) {
			Zend_Debug::dump($exception);
		}
		
         /*
    	 * Listando os módulos no Actor 2
    	 * */
    	 try {
			 $client->listardisciplina()
					  ->chavea('c81e728d9d4c2f636f067f89cc14862c')
					  ->chave('692d72b78b302e036fd67e546d26f557');
			$dados =  $client->get()->listardisciplina;
			echo '<fieldset><legend>Listando AS DISCIPLINAS no Actor 2</legend>';
    		Zend_Debug::dump($dados);
    		echo '</fieldset>';
    	} catch (exception $exception) {
			Zend_Debug::dump($exception);
		}
    	exit;
    }

    public function saladeaulaAction(){
    	
    	$client = new Zend_Rest_Client('http://actor.admescolar.com.br/services/saladeaula.php');
    	
    	/*
    	 * INSERINDO UMA DISCIPLINA
    	*/ 
		try {
			
			$client	->inserirsaladeaula()
					->chavea('c81e728d9d4c2f636f067f89cc14862c')
					->chave('692d72b78b302e036fd67e546d26f557')
					->dados(
						array(
							 'codmodulosaladeaula' 	=>'3921' // Obrigatório, ele indica qual módulo será usado para a saladeaula.
							,'destitulo'			=>'Criado pelo WebService'
							,'datiniciocurso' 		=>'15/12/2010'
							,'datterminocurso'		=>'15/12/2015'
							,'datinicioturma' 		=>'15/12/2011' // OPCIONAL
							,'datterminoturma'		=>'15/12/2014' // OPCIONAL
							,'nomturma'				=>'Nome da Turma' // OPCIONAL
							//,'codcurso'				=>'7042' // INFORMANDO O CODCURSO NESSA PARTE, O SISTEMA VAI INSERIR APENAS A TURMA, NÃO INFORMANDO, ELE INSERE TUDO
							,'codarea'				=>'2'
							,'descurso'				=>'Descrição do curso Criado pelo WebService'
							,'desurl'				=>'http://www.globo.com'
							,'numhoras'				=>'450'
							,'codnivel'				=>'2'
							,'desemail'				=>'elciomgdf@gmail.com'
						)
					);
			$dados =  $client->post()->inserirsaladeaula;
			echo '<fieldset><legend>INSERINDO UMA SALA DE AULA NO ACTOR 2</legend>';
    		Zend_Debug::dump($dados);
    		echo '</fieldset>';
		} catch (exception $exception) {
			Zend_Debug::dump($exception);
		}
    	 
		exit;
		// ATUALIZANDO UMA DISCIPLINA NO ACTOR 2
		// vamos usar o codcurso inserido como exemplo
		$codcurso = $dados["saladeaula"]["curso"]["codcurso"];
		$codturma = $dados["saladeaula"]["turma"]["codturma"];
		
		try {
			
			$client	->atualizarsaladeaula()
					->chavea('c81e728d9d4c2f636f067f89cc14862c')
					->chave('692d72b78b302e036fd67e546d26f557')
					->dados(
						array(
						
							 'codcurso'				=>$codcurso
							,'destitulo'			=>'Atualizado pelo WebService usando o programa '
							,'descurso'				=>'Descrição do módulo Criado pelo WebService'
							,'datiniciocurso' 		=>'15/12/2010'
							,'datterminocurso'		=>'15/12/2015'
							,'codarea'				=>'2'
							,'codnivel'				=>'2'
							,'desurl'				=>'http://www.globo.com'
							,'numhoras'				=>'450'
							,'flasituacao'			=>'A'
							,'desemail'				=>'elciomgdf@gmail.com'
							
							,'codturma'				=>$codturma // INFORMANDO O CODTURMA VC ALTERA A DISCIPLINA
							,'nomturma'				=>'Novo nome da turma'
							,'datinicioturma'		=>'10/10/2010'
							,'datterminoturma'		=>'10/10/2020'
							
						)
					);
			$dados =  $client->post()->atualizarsaladeaula;
			echo '<fieldset><legend>ATUALIZANDO UMA SALA DE AULA NO ACTOR 2</legend>';
    		Zend_Debug::dump($dados);
    		echo '</fieldset>';	
		} catch (exception $exception) {
			Zend_Debug::dump($exception);
		}
		
		// LISTANDO AS DISCIPLINAS NO ACTOR 2
		
        try {
			 $client->listarsaladeaula()
					  ->codcurso($codcurso)
			 		  ->chavea('c81e728d9d4c2f636f067f89cc14862c')
					  ->chave('692d72b78b302e036fd67e546d26f557');
			$dados =  $client->get()->listarsaladeaula;
			echo '<fieldset><legend>LISTANDO AS SALAS DE AULA NO ACTOR 2</legend>';
    		Zend_Debug::dump($dados);
    		echo '</fieldset>';
    	} catch (exception $exception) {
    		Zend_Debug::dump($exception);
		}
		
       	// RECUPERANDO UMA DISCIPLINA NO ACTOR 2
		try {
			 $client->retornasaladeaula()
					  ->codcurso($codcurso)->codturma($codturma)
			 		  ->chavea('c81e728d9d4c2f636f067f89cc14862c')
					  ->chave('692d72b78b302e036fd67e546d26f557');
			$dados =  $client->get()->retornasaladeaula;
			echo '<fieldset><legend>RECUPERANDO UMA SALA DE AULA NO ACTOR 2</legend>';
    		Zend_Debug::dump($dados);
    		echo '</fieldset>';
    	} catch (exception $exception) {
    		Zend_Debug::dump($exception);
		}
    	
    }


}


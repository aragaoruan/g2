<?php

/**
 * Classe de controller MVC para controlar Matriculas
 * @author Rafael Rocha <rafael.rocha.mg@gmail.com>
 *
 * @package application
 * @subpackage controller
 */
class Default_MatriculaController extends Ead1_ControllerRestrita
{

    private $_bo;
    private $_secretariaBO;

    /**
     * @var \G2\Negocio\PaMarcacaoEtapa
     */
    private $_paMarcacaoEtapa;

    /**
     * @var \G2\Negocio\Matricula
     */
    private $negocio;
    /**
     * @var \G2\Negocio\Ocorrencia()
     */
    private $_ocorrencia;

    public function init()
    {
        parent::init();
        parent::setupDoctrine();
        $this->_bo = new MatriculaBO();
        $this->_secretariaBO = new SecretariaBO();
        $this->negocio = new \G2\Negocio\Matricula();
        $this->_paMarcacaoEtapa = new \G2\Negocio\PaMarcacaoEtapa();
        $this->_ocorrencia = new \G2\Negocio\Ocorrencia();
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('matricula/index.js');
        echo $this->view->render('matricula/material.js');
        echo $this->view->render('matricula/ocorrencia.js');
        echo $this->view->render('matricula/aba-data-termino.js');
        echo $this->view->render('matricula/avaliacao-agendamento.js');
    }

    public function indexAction()
    {
    }

    public function retornarVwMatriculaGradeSalaAction()
    {
        $vws = new VwMatriculaGradeSalaTO();
        $vws->setId_matricula($this->getParam('id_matricula'));
        $vws = $vws->fetch();
        foreach ($vws as $key => $vw) {
            $vws[$key] = $vw->toBackboneArray();
        }
        $this->_helper->json($vws);
    }

    public function retornarVwGradeNotaAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $notasAgrupadas = $this->negocio->retornaVwGradeNotaAgrupada($params);
        $this->_helper->json(array_values($notasAgrupadas));
    }

    public function retornarVwGradeNotaPorStatusDisciplinaAction() {
        $params['id_matricula'] = $this->getParam('id_matricula');
        $result = $this->negocio->retornaVwGradeNotaPorStatusDisciplina($params);
        $this->_helper->json($result);
    }

    public function retornarTramitesAction()
    {

        $tramiteNegocio = new \G2\Negocio\Tramite();

        $where = array();
        $idTipoTramite = null;
        $historicos = array();

        if ($this->getParam('id_matricula')) {
            $where['id_matricula'] = $this->getParam('id_matricula');
            $historicos = $tramiteNegocio->getTramiteMatriculaHistoricoMatriculaGradeNota($where['id_matricula']);
        }

        if ($this->getParam('id_tipotramite')) {
            $idTipoTramite = $this->getParam('id_tipotramite');
        }

        $mensageiro = $tramiteNegocio->retornarTramiteSp(CategoriaTramiteTO::MATRICULA, $where, $idTipoTramite);
        $tramites = array();
        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            foreach ($mensageiro->getMensagem() as $tramite) {
                $tramite->setSt_tramite(nl2br($tramite->getSt_tramite()));
                $array = $tramite->toBackboneArray();
                $array['st_nomecompleto'] == null ? $array['st_nomecompleto'] = 'Sistema' : $array['st_nomecompleto'];

                foreach ($historicos as $historico) {
                    if ($historico->getId_tramitematricula()->getId_tramite()->getId_tramite() == $tramite->getId_tramite()) {
                        $array['id_tramitematricula'] = $historico->getId_tramitematricula()->getId_tramitematricula();
                        $array['st_htmlgrade'] = $historico->getSt_htmlgrade();
                        $array['id_historicogradenota'] = $historico->getId_historicogradenota();
                        break;
                    }
                }
                $tramites[] = $array;
            }
        }

        $this->_helper->json($tramites);
    }

    public function retornarVwLogMatriculaAction()
    {
        $logBO = new LogBO();

        $to = new VwLogMatriculaTO();
        $to->setId_matricula($this->getParam('id_matricula'));
        $dt_inicio = $this->getParam('dt_inicio') && $this->getParam('dt_inicio') != 'undefined' ? new Zend_Date($logBO->converteDataBanco($this->getParam('dt_inicio'))) : new Zend_Date();
        $dt_fim = $this->getParam('dt_fim') && $this->getParam('dt_fim') != 'undefined' ? new Zend_Date($logBO->converteDataBanco($this->getParam('dt_fim'))) : new Zend_Date();

        $mensageiro = $logBO->listarVwLogMatricula($to, $dt_inicio, $dt_fim);

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            foreach ($mensageiro->getMensagem() as $log) {
                $logs[] = $log->toBackboneArray();
            }
        } else {
            $logs = array();
        }
        $this->_helper->json($logs);
    }

    public function alterarAction()
    {
        $params = $this->getAllParams();
        $this->_helper->viewRenderer->setNoRender(true);

        $result = $this->negocio->alterarEvolucao($params);

        if ($result->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $notasAgrupadas = $this->negocio->retornaVwGradeNotaAgrupada($params);
            $this->view->notasAgrupadas = $notasAgrupadas;
            $html = $this->view->render('matricula/historico-matricula-grade-nota.phtml');

            $this->negocio->salvarHistoricoMatriculaGradeNota($params, $html);
        }

        $id_matricula = array(
            $this->getParam('id_matricula', false)
        );
        $st_acao = $this->getParam('st_acao', false);
        $st_acao = $st_acao === \G2\Constante\Acao::CANCELAR ? true : false;
        if ($result->getTipo() == Ead1_IMensageiro::SUCESSO && $id_matricula && $st_acao) {

            $msg = $this->_ocorrencia->roboEncerrarOcorrencia($id_matricula);

            if ($msg->getTipo() != Ead1_IMensageiro::SUCESSO) {
                $this->_helper->json($msg);
            }
        }

        $this->_helper->json($result);
    }

    public function retornarResponsavelFinanceiroAction()
    {
        $negocio = new \G2\Negocio\Financeiro();

        $id_mat = $this->getParam('id_matricula');
        if (isset($id_mat) && $id_mat != '') {
            $mensageiro = $negocio->retornarResposavelFinanceiro($id_mat);

            if ($mensageiro != null) {
                $this->_helper->json(array(
                    'type' => Ead1_IMensageiro::TYPE_SUCESSO,
                    'title' => Ead1_IMensageiro::TITLE_SUCESSO,
                    'text' => $mensageiro));
            } else {
                $this->_helper->json(array(
                    'type' => Ead1_IMensageiro::TYPE_AVISO,
                    'title' => Ead1_IMensageiro::TITLE_AVISO,
                    'text' => 'Nenhum responsável financeiro localizado.'
                ));
            };
        } else {
            $this->_helper->json(array(
                'type' => Ead1_IMensageiro::TYPE_AVISO,
                'title' => Ead1_IMensageiro::TITLE_AVISO,
                'text' => 'Nenhum código de matrícula informado'
            ));
        }
    }

    public function retornarMatriculasAction()
    {

        $matriculaTO = new VwMatriculaTO();
        $matriculaTO->setId_usuario(Ead1_Sessao::getSessaoUsuario()->id_usuario);
        $matriculas = $matriculaTO->fetch();

        if (count($matriculas) > 0) {

            $retorno = array();
            foreach ($matriculas as $to) {
                $retorno[] = $to->toArray();
            }

            $this->_helper->json(array(
                    'type' => Ead1_IMensageiro::TYPE_SUCESSO,
                    'title' => Ead1_IMensageiro::TITLE_SUCESSO,
                    'text' => Ead1_IMensageiro::MENSAGEM_SUCESSO,
                    'mensagem' => $retorno)
            );
        } else {
            $this->_helper->json(array(
                'type' => Ead1_IMensageiro::TYPE_AVISO,
                'title' => Ead1_IMensageiro::TITLE_AVISO,
                'text' => 'Nenhum código de matrícula informado'
            ));
        }
    }

    /**
     * Refatoração Neemias Santos <neemias.santos@unyleya.com.br>
     * Data 29/06/2015
     *
     */
    public function retornarVwResumoFinanceiroAction()
    {

        $data = $this->limpaGetAllParams($this->getRequest()->getParams());

        $return = $this->negocio->findVwResumoFinanceiro($data);

        return $this->_helper->json($return);

    }

    public function retornarLancamentosAtrasadosAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        if ($this->getParam('id_venda')) {
            $sql = 'SELECT DISTINCT COUNT(vw.id_lancamento) as nu_atrasados, '
                . ' MAX(DATEDIFF(DAY, vw.dt_vencimento, GETDATE())) as nu_diasatraso '
                . ' FROM vw_resumofinanceiro vw WHERE vw.id_venda = '
                . $this->getParam('id_venda') . ' AND DATEDIFF(DAY, vw.dt_vencimento, GETDATE()) > 2 AND vw.bl_quitado = 0 AND vw.bl_ativo = 1';
            $result = $this->em->getConnection()->query($sql)->fetchAll();

            echo json_encode($result[0]);
        }
    }

    public function salvarTramiteMatriculaAction()
    {
//        $tramiteTO = new TramiteTO();
//        $tramiteTO->setBl_visivel(1);
//        $tramiteTO->setDt_cadastro(new Zend_Date());
//        $tramiteTO->setId_entidade($tramiteTO->getSessao()->id_entidade);
//        //$tramiteTO->setId_tipotramite(9); // Matricula manual
//        $tramiteTO->setId_usuario($tramiteTO->getSessao()->id_usuario);
//        $tramiteTO->setSt_tramite($this->getParam('st_tramite'));
//
//        $tramiteBO = new TramiteBO();
//        $mensageiro = $tramiteBO->cadastrarTramite($tramiteTO, CategoriaTramiteTO::MATRICULA, $this->getParam('id_matricula'));

        $negocio = new \G2\Negocio\Tramite();

        $data['st_tramite'] = $this->getParam('st_tramite');
        $data['id_campo'] = $this->getParam('id_matricula');
        $data['id_categoriatramite'] = CategoriaTramiteTO::MATRICULA;

        if ($_FILES) {
            $mensageiro = $negocio->salvarTramite($data, $_FILES);
        } else {
            $mensageiro = $negocio->salvarTramite($data);
        }

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $this->_helper->json(array(
                'type' => Ead1_IMensageiro::TYPE_SUCESSO,
                'title' => Ead1_IMensageiro::TITLE_SUCESSO,
                'text' => 'Tramite cadastrado com sucesso.',
            ));
        } else {
            $this->_helper->json(array(
                'type' => Ead1_IMensageiro::TYPE_ERRO,
                'title' => Ead1_IMensageiro::TITLE_ERRO,
                'text' => $mensageiro->getText()
            ));
        }
    }

    public function processaNotaDisciplinaAction()
    {
        $avaliacao = new \G2\Negocio\Avaliacao();
        $avaliacaoaluno = new \G2\Entity\AvaliacaoAluno();
        $avaliacaoaluno->setId_matricula($this->getParam('id_matricula', null));
        $avaliacaoaluno->setId_avaliacaoconjuntoreferencia($this->getParam('id_avaliacaoconjuntoreferencia', null));

        $mensageiro = $avaliacao->processaNotaDisciplina($avaliacaoaluno);

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            if ($this->getParam('id_matriculadisciplina')) {
                $negocio = new G2\Negocio\Negocio();
                $matriculaDisciplina = $negocio->findOneBy('G2\Entity\MatriculaDisciplina', array('id_matriculadisciplina' => $this->getParam('id_matriculadisciplina', null)));
                $VwGradeNota = $negocio->findBy('G2\Entity\VwGradeNota', array('id_matricula' => $this->getParam('id_matricula', null), 'id_disciplina' => $matriculaDisciplina->getId_disciplina()));

                $st_notafinal = '';
                $st_notaAtividade = '';
                $st_notaead = '';
                $st_notaead_prr = '';
                $st_notarecuperacao = '';
                $st_notatcc = '';
                $nu_notafinal = '';
                $st_status = '';
                $temPrr = false;

                //pega as notas da disciplina que está sendo processada e seta para a alteração da grade pelo JS
                foreach ($VwGradeNota AS $gradenota) {

                    if ($st_notafinal == '') {
                        $st_notafinal = $gradenota->getSt_notafinal();
                    } else {
                        if ($gradenota->getSt_notafinal() > $st_notafinal) {
                            $st_notafinal = $gradenota->getSt_notafinal();
                        }
                    }

                    if ($st_notaAtividade == '') {
                        $st_notaAtividade = (int)$gradenota->getSt_notaatividade();
                    } else {
                        if ($gradenota->getSt_notaatividade() > $st_notaAtividade) {
                            $st_notaAtividade = (int)$gradenota->getSt_notaatividade();
                        }
                    }

                    if ($gradenota->getId_categoriasala() == 1) {
                        $st_notaead = (int)$gradenota->getSt_notaead();
                        $st_status = $gradenota->getSt_status();
                    } elseif ($gradenota->getId_categoriasala() == 2) {
                        $st_notaead_prr = (int)$gradenota->getSt_notaead();
                        $st_status = $gradenota->getSt_status();
                        $temPrr = true;
                    }

                    if ($st_notarecuperacao == '') {
                        $st_notarecuperacao = (int)$gradenota->getSt_notarecuperacao();
                    } else {
                        if ($gradenota->getSt_notarecuperacao() > $st_notarecuperacao) {
                            $st_notarecuperacao = (int)$gradenota->getSt_notarecuperacao();
                        }
                    }

                    if ($st_notatcc == '') {
                        $st_notatcc = (int)$gradenota->getSt_notatcc();
                    } else {
                        if ($gradenota->getSt_notafinal() > $st_notatcc) {
                            $st_notatcc = (int)$gradenota->getSt_notatcc();
                        }
                    }

                    if ($nu_notafinal == '') {
                        $nu_notafinal = $gradenota->getNu_notafinal();
                    } else {
                        if ($gradenota->getSt_notafinal() > $nu_notafinal) {
                            $nu_notafinal = $gradenota->getNu_notafinal();
                        }
                    }

                }

                //envia os dados setados por JSON
                $this->_helper->json(array(
                    'type' => Ead1_IMensageiro::TYPE_SUCESSO,
                    'title' => Ead1_IMensageiro::TITLE_SUCESSO,
                    'text' => $mensageiro->getText(),
                    'st_notafinal' => $st_notafinal,
                    'st_notaead' => $st_notaead,
                    'st_notaAtividade' => $st_notaAtividade,
                    'st_notaead_prr' => $st_notaead_prr,
                    'st_notatcc' => $st_notatcc,
                    'st_notarecuperacao' => $st_notarecuperacao,
                    'nu_notafinal' => $nu_notafinal,
                    'st_status' => $st_status,
                    'temPrr' => $temPrr
                ));

            } else {
                $this->_helper->json(array(
                    'type' => Ead1_IMensageiro::TYPE_SUCESSO,
                    'title' => Ead1_IMensageiro::TITLE_SUCESSO,
                    'text' => $mensageiro->getText()
                ));
            }

        } else {
            $this->_helper->json(array(
                'type' => Ead1_IMensageiro::TYPE_ERRO,
                'title' => Ead1_IMensageiro::TITLE_ERRO,
                'text' => $mensageiro->getCodigo()
            ));
        }

        $this->_helper->json($mensageiro);
    }

    public function corrigirMatriculasAction()
    {
        try {
            $id_projetopedagogico = $this->getParam('id_projetopedagogico');

            $negocio = new \G2\Negocio\Matricula();
            $result = $negocio->corrigirMatriculaDisciplina($id_projetopedagogico);

            if ($result) {
                $this->_helper->json(array(
                    'type' => Ead1_IMensageiro::TYPE_SUCESSO,
                    'title' => Ead1_IMensageiro::TITLE_SUCESSO,
                    'text' => "Matrículas corrigidas com sucesso!",
                ));
            } else {
                $this->_helper->json(array(
                    'type' => Ead1_IMensageiro::TYPE_AVISO,
                    'title' => Ead1_IMensageiro::TITLE_AVISO,
                    'text' => "Não foi possível corrigir as Matrículas!",
                ));
            }
        } catch (Exception $e) {
            $this->_helper->json(array(
                'type' => Ead1_IMensageiro::TYPE_ERRO,
                'title' => Ead1_IMensageiro::TITLE_ERRO,
                'text' => "Ocorreu um Erro ao tentar corrigir as Matrículas, favor entrar em contato com o suporte!",
            ));
        }
    }

    public function retornaHistoricoAlocacaoAction()
    {

        $matricula = new \G2\Negocio\Matricula();

        /** @var \G2\Entity\Alocacao[] $vws * */
        $vws = $matricula->findByAlocacao($this->getRequest()->getQuery(), array('dt_cadastro' => 'DESC'));
        $return = array();

        if ($vws && is_array($vws)) {
            foreach ($vws as $key => $vw) {
                $data_cadastro = ($vw->getDt_cadastro() instanceof DateTime ? $vw->getDt_cadastro() : new Zend_Date($vw->getDt_cadastro()));
                $data_inicio = $vw->getDt_inicio() != null ? ($vw->getDt_inicio() instanceof DateTime ? $vw->getDt_inicio() : new Zend_Date($vw->getDt_inicio())) : null;
                $dt_trancamento = $vw->getDt_trancamento() != null ? ($vw->getDt_trancamento() instanceof DateTime ? $vw->getDt_trancamento() : new Zend_Date($vw->getDt_trancamento())) : null;

                $return[$key]['id_saladeaula'] = $vw->getId_saladeaula();
                $return[$key]['st_saladeaula'] = $vw->getId_saladeaula()->getSt_saladeaula();
                $return[$key]['dt_cadasto'] = $matricula->converterData($data_cadastro, 'd/m/Y H:m:s');
                $return[$key]['dt_abertura'] = $vw->getId_saladeaula()->getDt_abertura() instanceof DateTime ?
                    $matricula->converterData($vw->getId_saladeaula()->getDt_abertura(), 'd/m/Y') : $vw->getId_saladeaula()->getDt_abertura();
                $return[$key]['dt_encerramento'] = $vw->getId_saladeaula()->getDt_encerramento() instanceof DateTime ?
                    $matricula->converterData($vw->getId_saladeaula()->getDt_encerramento(), 'd/m/Y') : $vw->getId_saladeaula()->getDt_encerramento();
                $return[$key]['dt_inicio'] = $data_inicio instanceof Zend_Date ? $matricula->converterData($data_inicio, 'dd/MM/Y') : ' -- ';
                $return[$key]['st_nomecompleto'] = $vw->getId_usuariocadastro() instanceof \G2\Entity\Usuario ? $vw->getId_usuariocadastro()->getSt_nomecompleto() : ' Auto alocar';
                $return[$key]['st_situacao'] = $vw->getBl_ativo() ? ' Ativo ' : 'Inativo';
                $return[$key]['st_disciplina'] = $vw->getId_matriculadisciplina()->getDisciplina() instanceof \G2\Entity\Disciplina ?
                    $vw->getId_matriculadisciplina()->getDisciplina()->getSt_disciplina() : ' - ';
                $return[$key]['dt_trancamento'] = $dt_trancamento ? $matricula->converterData($dt_trancamento, 'd/m/Y H:m:s') : ' - ';
                $return[$key]['st_usuariotrancamento'] = $vw->getId_usuariotrancamento() instanceof \G2\Entity\Usuario ? $vw->getId_usuariotrancamento()->getSt_nomecompleto() : ' - ';
            }
        }

        $this->_helper->json($return);
    }

    public function historicoMensagemAction()
    {
        try {
            $mensagem = new \G2\Negocio\Mensagem();
            $result = $mensagem->historicoMensagem($this->_getParam('id_matricula'));

            $this->_helper->json(array(
                'type' => Ead1_IMensageiro::TYPE_SUCESSO,
                'title' => Ead1_IMensageiro::TITLE_SUCESSO,
                'text' => Ead1_IMensageiro::MENSAGEM_SUCESSO,
                'mensagem' => $result
            ));
        } catch (Zend_Exception $exception) {
            $this->_helper->json(array(
                'type' => Ead1_IMensageiro::TYPE_ERRO,
                'title' => Ead1_IMensageiro::TITLE_ERRO,
                'text' => 'Ocorreu um erro ao retornar historico de mensagens, favor entrar em contato com o suporte!'
            ));
        }
    }

    public function alterarDataTerminoAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
        $result = $this->negocio->alterarDataTermino($data);
        if ($result->getType() == Ead1_IMensageiro::TYPE_SUCESSO) {
            $this->getResponse()->setHttpResponseCode(201)->appendBody(json_encode($data));
        } else {
            $this->getResponse()->setHttpResponseCode(400)->appendBody($result->getText());
        }
    }

    public function retornaMatriculaParamsAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        $params = $this->limpaGetAllParams($this->getRequest()->getParams());
        $result = $this->negocio->consultaVwMatriculaJson($params);

        $this->_helper->Json($result);

    }

    public function retornaMatriculaContratoAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $params = $this->limpaGetAllParams($this->getRequest()->getParams());
        $result = $this->negocio->consultaVwMatriculaContrato($params);
        $this->_helper->Json($result);

    }

    public function prorrogaAlunosGrupoAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        $params = $this->limpaGetAllParams($this->getRequest()->getParams());
        $result = $this->negocio->alterarDataTerminoCombo($params);

        $this->_helper->Json($result);


    }

    /**
     * Action criada para retornar dados especificos para tela de matricula, onde o id da sessao
     * é desprezado ao retornar as matriculas.
     */

    public function retornarMatriculasByParamsAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        try {

            $params = $this->getAllParams();
            $params = $this->limpaGetAllParams($params);
            $retorno = $this->negocio->consultarDadosSecretariaMatricula($params);

            $this->_helper->json($retorno);

        } catch (Exception $e) {
            $this->getResponse()->setHttpResponseCode(500)->appendBody(json_encode(array(
                'type' => 'error',
                'title' => 'Erro',
                'text' => $e->getMessage()
            )));
        }
    }

    public function retornarMatriculaByUsuarioAction()
    {
        $params = $this->getAllParams();
        $params = $this->limpaGetAllParams($params);
        $params['id_situacao'] = \G2\Constante\Situacao::TB_MATRICULA_ATIVA;
        $result = $this->negocio->consultaVwMatricula($params);

        if (count($result) > 0) {
            $result = $this->negocio->toArrayEntity($result[0]);
        }
        $this->_helper->json($result);
    }

    /**
     * Retorna as permissoes de alterar a evolucao da matricula do usuario logado na entidade logada
     */
    public function permissoesAlterarEvolucaoAction()
    {
        $id_funcionalidade = 701;
        $ar_permissoes = \G2\Constante\Permissao::getArrayMatriculaEvolucao();

        $results = array();

        foreach ($ar_permissoes as $id_permissao => $label) {
            $entity = $this->negocio->findOneBy('\G2\Entity\PerfilPermissaoFuncionalidade', array(
                'id_perfil' => $this->negocio->sessao->id_perfil,
                'id_funcionalidade' => $id_funcionalidade,
                'id_permissao' => $id_permissao
            ));

            if ($entity instanceof \G2\Entity\PerfilPermissaoFuncionalidade) {
                $item = $this->negocio->toArrayEntity($entity);
                $item['st_nomepermissao'] = $label;
                array_push($results, $item);
            }
        }

        $this->_helper->json($results);
    }

    /**
     * Retorna data aceite de uma venda
     */
    public function getDataAceiteAction()
    {

        $result = $this->_paMarcacaoEtapa->getDataAceite($this->getParam('id_venda'));
        $result = $this->_paMarcacaoEtapa->toArrayEntity($result);
        $this->_helper->json($result);

    }

    public function imprimirHistoricoGradeNotaAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->getAllParams();
        $params = $this->limpaGetAllParams($params);

        try {
            $tramiteNegocio = new \G2\Negocio\Tramite();
            $historico = $tramiteNegocio->getTramiteMatriculaHistoricoMatriculaGradeNota($params['id_matricula'], $params['id_historicogradenota'], null, 1);

            $this->view->htmlGrade = (null != $historico) ? $historico->getSt_htmlgrade() : null;

        } catch (Exception $e) {
            $this->view->htmlGrade = $e->getMessage();
        }

        echo $this->view->render('matricula/imprimir-historico-matricula-grade-nota.phtml');
    }

    /**
     * Função que autentica o usuario do G2 no moodle
     * @throws Zend_Controller_Action_Exception
     * @throws Zend_Exception
     * @throws Zend_Session_Exception
     * @throws \Doctrine\ORM\ORMException
     */
    public function autenticarLogMoodleAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocio->autenticarLogMoodle($params);
        $this->_helper->json($result);

    }

    /**
     * Função para autenticar e gerar link para o redirecionar para o relatorio de log do aluno
     * @throws Zend_Controller_Action_Exception
     * @throws Zend_Controller_Response_Exception
     */
    public function buscarTccAction()
    {

        $params = $this->limpaGetAllParams($this->getAllParams());

        if (array_key_exists('id_matricula', $params)) {
            $result = $this->negocio->buscarTcc($params);
            $this->_helper->json($result);
        }

        $this->getResponse()->setHttpResponseCode(400);
        $this->_helper->json(
            new \Ead1_Mensageiro("Matrícula é obrigatorio", \Ead1_IMensageiro::AVISO)
        );

    }
}

<?php

/**
 * Classe de controller de Robos do Sistema
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package application
 * @subpackage controller
 */
class Default_RoboController extends Ead1_Controller
{
    /**
     * Método que inicializa as configurações gerais do ambiente
     */
    public function init()
    {
        parent::init();
        $this->validarPermissaoExecRobo();
        $this->bo = new RoboBO();
        $this->setupDoctrine();

        /*
         * Verifica extensao e habilita o background-job newRelic.
         */
        if (extension_loaded('newrelic')) {
            newrelic_background_job(true);
        }
    }

    /**
     * Variavel que contem a instancia do Robo BO
     * @var RoboBO
     */
    private $bo;

    /**
     * Método que renderiza a view inicial
     */
    public function indexAction()
    {
        exit;
    }

    /**
     * Action que dispara automaticamente os robos fixos, ou informados pela url
     */
    public function cronAction()
    {
        $this->view->metodos = array();
        $this->view->actions = get_class_methods($this);
        $arrMetodos = array('mensagenscobranca', 'lancamento', 'enviomensagem', 'processoativacaoautomatica'

        , 'sincronizarsalasblackboard'

        );
        if ($this->_hasParam('metodos')) {
            $arrMetodos = $this->_getParam('metodos');
        }
        $this->view->metodos = $arrMetodos;
    }

    /**
     * Metodo que roda o Robo gerador de Lançamentos
     */
    public function lancamentoAction()
    {
        try {
            $mensageiro = $this->bo->geraLancamentos();
            if ($mensageiro->getTipo() == Ead1_IMensageiro::AVISO) {
                $this->view->aviso = $mensageiro->mensagem[0];
            } else {
                $mensagem = $mensageiro->getMensagem();
                $this->view->info = $mensagem['info'];
                $this->view->jaCadastrados = (!empty($mensagem['jacadastrados']) ? $mensagem['jacadastrados'] : null);
                if ($mensagem['dados']) {
                    if (array_key_exists('sucesso', $mensagem['dados'])) {
                        $this->view->sucesso = $mensagem['dados']['sucesso'];
                    }
                    if (array_key_exists('erro', $mensagem['dados'])) {
                        $this->view->erro = $mensagem['dados']['erro'];
                    }
                } else {
                    $this->view->sucesso = null;
                    $this->view->erro = null;
                }
            }
            $this->render('lancamento');
        } catch (Zend_Exception $e) {
            $this->view->erro = $e->getMessage();
            $this->render('errolancamento');
        }
    }

    /**
     * Action do robo de envio de mensagens
     */
    public function enviomensagemAction()
    {
        $mensagemBO = new MensagemBO();
        $mensageiro = $mensagemBO->procedimentoEnviarMensagem();
        $this->view->mensagem = $mensageiro;
        $this->view->log = $mensagemBO->getLog();
    }


    /**
     * Registra o Envio de Mensagens de Cobrança
     *
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     */
    public function mensagenscobrancaAction()
    {

        $negocio = new \G2\Negocio\MensagemCobranca();
        $negocio->registrarMensagensCobranca(array('id_lancamento' => $this->_getParam('id_lancamento')));

    }


    /**
     * Action que verifica o vencimento dos alunos e os marca como Retornados
     */
    public function verificarenovacaoalunoAction()
    {
        $this->view->mensagem = $this->bo->verificaRenovacaoAluno();
    }

    /**
     * Verifica se rotinas de cartão com possíveis erros
     */
    public function verificapagamentocartaoAction()
    {
        ini_set('display_errors', 1);
        $this->view->mensagem = $this->bo->verificaPagamentoCartao();
    }

    /**
     * Verifica os pagamentos Braspag
     */
    public function verificapagamentocartaorecorrenteAction()
    {
        ini_set('display_errors', 1);
        $verificarPagamento = new \G2\Negocio\VerificarPagamentoRecorrente();
        $this->view->mensagem = $verificarPagamento->verificaPagamentocartaoRecorrente($this->getAllParams());
        Zend_Debug::dump($this->view->mensagem->getMensagem(), 'Verificando pagamento Recorrente!');
        exit;
    }

    /**
     * Verifica os pagamentos Pagarme
     * Pode verificar pagamentos Braspag caso o ID seja do formato Braspag
     */
    public function verificapagamentocartaorecorrentepagarmeAction()
    {
        ini_set('display_errors', 1);
        $verificarPagamento = new \G2\Negocio\VerificarPagamentoRecorrente();
        $params = $this->getAllParams();
        $params['id_sistemacobranca'] = \G2\Constante\Sistema::PAGARME;
        $this->view->mensagem = $verificarPagamento->verificaPagamentocartaoRecorrente($params);
        Zend_Debug::dump($this->view->mensagem->getMensagem(), 'Verificando pagamento Recorrente!');
        exit;
    }

    public function gerarlancamentoassinaturaAction()
    {
        ini_set('display_errors', 1);
        $this->ajax();
        $this->view->mensagem = $this->bo->gerarLancamentoAssinatura();
        Zend_Debug::dump($this->view->mensagem->getMensagem(), 'Gerando lançamentos de Assinaturas!');
        return;
    }

    /**
     * Action que sincroniza perfis no moodle
     */
    public function sincronizacaomoodleAction()
    {

        $moodleBO = new MoodleBO();
        $mensageiro = $moodleBO->sincronizacaoMoodle();
        Zend_Debug::dump($mensageiro);
        exit;
//        $this->view->mensagem = $mensageiro;

    }

    /**
     * Action que altera o papel do aluno no moodle
     */
    public function alterarpapelalunomoodleAction()
    {
        $moodleBO = new MoodleBO();
        $mensageiro = $moodleBO->alterarPapelAluno(null, $this->limpaGetAllParams($this->getAllParams()));
        $this->view->mensagem = $mensageiro;
    }

    public function processoativacaoautomaticaAction()
    {
        try {
            $mensageiro = $this->bo->processoAtivacaoAutomatica();
            if ($mensageiro->getTipo() == Ead1_IMensageiro::AVISO) {
                $this->view->aviso = $mensageiro->mensagem[0];
            } else {
                $mensagem = $mensageiro->getMensagem();
                $this->view->info = $mensagem['info'];
                if ($mensagem['dados']) {
                    if (array_key_exists('sucesso', $mensagem['dados'])) {
                        $this->view->sucesso = $mensagem['dados']['sucesso'];
                    }
                    if (array_key_exists('erro', $mensagem['dados'])) {
                        $this->view->erro = $mensagem['dados']['erro'];
                    }
                } else {
                    $this->view->sucesso = null;
                    $this->view->erro = null;
                }
            }
        } catch (Exception $e) {
            $this->view->erro = $e->getMessage();
        }
    }

    public function baixarlancamentosAction()
    {
        try {
            $service = new TotvsLancamentosWebServices();
            $mensageiro = $service->alimentarLancamentosBaixados();
            if ($mensageiro->getTipo() == Ead1_IMensageiro::AVISO) {
                $this->view->aviso = $mensageiro->mensagem[0];
            } elseif ($mensageiro->getTipo() == Ead1_IMensageiro::ERRO) {
                throw new Zend_Exception($mensageiro->getFirstMensagem());
            } else {
                $mensagem = $mensageiro->getMensagem();
                $this->view->info = $mensagem['info'];
                if ($mensagem['dados']) {
                    if (array_key_exists('sucesso', $mensagem['dados'])) {
                        $this->view->sucesso = $mensagem['dados']['sucesso'];
                    }
                    if (array_key_exists('erro', $mensagem['dados'])) {
                        $this->view->erro = $mensagem['dados']['erro'];
                    }
                } else {
                    $this->view->sucesso = null;
                    $this->view->erro = null;
                }
            }
            $this->render('lancamentosbaixados');
        } catch (Zend_Exception $e) {
            $this->view->erro = $e->getMessage();
            $this->render('errolancamentosbaixados');
        }
    }

    /**
     * Action que sincroniza os alunos não alocados que estão aptos a serem
     * alocados
     */
    public function sincronizarAutoAlocarAction()
    {
        $this->ajax();

        $salaAulaBO = new SalaDeAulaBO();
        $response = $salaAulaBO->sincronizarAutoAlocar();

        if ($response instanceof \Ead1_Mensageiro) {
            echo $response->getText();
        } else if (!empty($response['results'])) {
            echo count($response['success']) . ' alocação(ões) realizada(s) com sucesso num total de ' . count($response['results']) . ' registro(s).';

            if (count($response['error'])) {
                $this->view->fields = array(
                    'id_matricula' => 'Matrícula',
                    'st_nomecompleto' => 'Aluno',
                    'st_disciplina' => 'Disciplina',
                    'id_matriculadisciplina' => 'id_matriculadisciplina',
                    'message' => 'Status',
                );
                $this->view->values = $response['error'];
                $this->view->title = 'Erros';
                echo $this->view->render('robo/output.phtml');
            }
        } else {
            echo 'Não há alocações pendentes.';
        }
    }


    /**
     * Método para reintegrar os alunos que deixaram de ser integrados devido
     * ao bug inserido no commit do módulo de integração do H7
     * @deprecated Não esta sendo executado
     */
    public function reintegrarAlunosPerdidosAction()
    {
        ini_set('display_errors', 1);
        try {
            $vwReintegrarAlunosTO = new VwReintegrarAlunosTO();
            $dao = new SalaDeAulaDAO();

            $alunos = $dao->retornaVwReintegrarAlunos($vwReintegrarAlunosTO);
            $bo = new SalaDeAulaBO();

            foreach ($alunos as $itemIntegrar) {

                $alocacaoTO = new AlocacaoTO();

                $alocacaoTO->setId_alocacao($itemIntegrar['id_alocacao']);
                $alocacaoTO->setId_evolucao($itemIntegrar['id_evolucao']);
                $alocacaoTO->setId_matriculadisciplina($itemIntegrar['id_matriculadisciplina']);
                $alocacaoTO->setId_saladeaula($itemIntegrar['id_saladeaula']);
                $alocacaoTO->setBl_ativo($itemIntegrar['bl_ativo']);

                if ($bo->salvarIntegracaoAlunoWS($alocacaoTO))
                    echo 'Integrado' . $alocacaoTO->getId_alocacao() . '<br/>';

            }
        } catch (Zend_Exception $e) {

        }


    }


    /**
     * Método que altera a visibilidade das salas de aula do moodle
     *
     * @todo Coloca o método no robô após testar no moodle
     * Esperando o moodle ser atualizado para versão 2.5 para testes
     * @deprecated Não esta sendo executado
     */
    public function alteravisibilidadesalamoodleAction()
    {
        $moodleBO = new MoodleBO();
        $mensageiro = $moodleBO->alteraSalaVisivelMoodle();
        $this->view->mensagem = $mensageiro;
    }

    /**
     *
     */
    public function sincronizarDadosFluxusGestorAction()
    {
        $contrato = $this->_request->getParam('contrato');
        $fluxusBO = new FluxusBO();
        $fluxusBO->sincronizarDadosFluxusGestor($contrato);
        die();
    }

    /**
     * Método responsavel por gerar lanacamento de vendas
     */
    public function robofluxusAction()
    {

        $mensageiro = new Ead1_Mensageiro();

        try {
            $negocio = new \G2\Negocio\TotvsVwIntegraGestorFluxus();
            $mensageiroRetorno = $negocio->integrar(array());
            if ($mensageiroRetorno->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $mensageiro->setMensageiro('Lançamentos executados com sucesso', Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro->setMensageiro($mensageiroRetorno->getFirstMensagem(), Ead1_IMensageiro::ERRO);
            }

            $this->_helper->json($mensageiro);

        } catch (Zend_Exception $e) {
            $mensageiro->setMensageiro('Erro ao tentar excutar lançamentos', Ead1_IMensageiro::ERRO);
            $this->_helper->json($mensageiro);
        }
    }

    public function sincronizarDadosG2LiteFluxusAction()
    {
        $negocio = new \G2\Negocio\TotvsVwIntegraGestorFluxus();
        $id_venda = $this->_getParam('id_venda', null);
        Zend_Debug::dump($negocio->integrar((!empty($id_venda) ? array('campoalfaop1' => $this->_getParam('id_venda', null) . 'G2U') : array())), __CLASS__ . '(' . __LINE__ . ')');
        exit;
    }

    public function sincronizarDadosFluxusGestorWSAction()
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);
        ini_set('display_errors', 1);

        $negocio = new \G2\Negocio\TotvsVwIntegraGestorFluxus();
        $idVenda = $this->_getParam('id_venda', null);
        $return = $negocio->sincronizarDadosFluxusGestorWS(
            array(
                'contrato' => !empty($idVenda) ? str_replace('G2U', '', $idVenda) . 'G2U' : null,
                'maxResults' => (int)$this->getParam('top', null),
                'status_atualizacao' => $this->getParam('statusatualizacao', null)
            ));
        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->json($return);
        } else {
            $this->_helper->viewRenderer->setNoRender(true);
            echo "<h1>Resultados:</h1>";
            echo $return['message'] . '<br><br>';
            foreach ($return['data'] as $data) {
                echo $data . '<br>';
            }
        }
    }


    /**
     * Método que sincroniza as notas recebidas via url do moodle.
     * Caso em testes locais seja retornado o um erro de limite de string,
     * provavelmente será pelo nome do campo id_avaliacaoconjuntoreferencia
     * (não acontece no servidor).
     *
     * Débora Castro - debora.castro@unyleya.com.br
     * AC-570 [21-11-2013]
     * @deprecated Não esta sendo executado
     */
    public function sincronizanotasAction()
    {
        try {
            $this->_helper->viewRenderer->setNoRender();
            $bo = new AvaliacaoBO();
            $retorno = $bo->sincronizaNotaDoMoodle($this->_request->getParams());

            if ($retorno instanceof \Ead1_Mensageiro
                && $retorno->getTipo() != Ead1_IMensageiro::SUCESSO
            ) {
                return false;
            }
        } catch (Exception $e) {
            throw $e;
        }
        return true;
    }

    function atualiza_nota($url)
    {
        //codaluno=34&codsala=2
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    public function retornarVendaLancamentoAction()
    {
        $vlTO = new VendaTO();
        $bo = new VendaBO();
        $vlTO->setId_venda(33170);
        //\Zend_Debug::dump($vlTO,__CLASS__.'('.__LINE__.')');exit;
        $dados = $bo->confirmaRecebimento($vlTO);
        \Zend_Debug::dump($dados, __CLASS__ . '(' . __LINE__ . ')');
        exit;
    }

    public function receberDinheiroLancamentoAction()
    {
        $bo = new VendaBO();
        $lTO = new LancamentoTO();
        $lTO->setId_lancamento(1130320);
        \Zend_Debug::dump($bo->receberDinheiroLancamento($lTO), __CLASS__ . '(' . __LINE__ . ')');
        exit;
    }

    public function sincronizarFluxusPendentesAction()
    {
        $bo = new SincronizadorBO();
        \Zend_Debug::dump($bo->lancamentosPendentesFluxus(), __CLASS__ . '(' . __LINE__ . ')');
        exit;
    }


    /**
     * Sincroniza as Salas de Aula com o BlackBoard
     *
     */
    public function sincronizarsalasblackboardAction()
    {

//     	exit;
        $this->ajax();

        $bo = new SalaDeAulaBO();
        try {

            // buscar as integrações do BlackBoard
            $bo = new IntegracaoBO();
            $ei = new EntidadeIntegracaoTO();
            $ei->setId_sistema(SistemaTO::BLACKBOARD);
            $arrIntegracao = $ei->fetch(false, false, false);
            if ($arrIntegracao) {

                foreach ($arrIntegracao as $ie) {
                    $key = json_decode($ie->getSt_codchave());
                    if ($key == false) {
                        echo "Entidade {$ie->getId_entidade()} sem integração com o BlackBoard<br>";
                        continue;
                    }

                    echo "<fieldset><legend>Entidade {$ie->getId_entidade()}</legend>";
                    $retorno = $bo->integrarSalas($ie->getId_entidade(), SistemaTO::BLACKBOARD);
                    foreach ($retorno->getMensagem() as $mensagem) {
                        echo $mensagem . '<br>';
                    }
                    echo "</fieldset>";

                } // foreach($arrIntegracao as $ie){

            } //$arrIntegracao

        } catch (Exception $e) {
            echo $e->getMessage() . '<br>';
        }

    }


    /**
     *
     * Verifica se as salas de aula estão cadastradas no BlackBoard
     *
     */
    public function verificarsalasblackboardAction()
    {
        die('Rotina feita pelo sistema');
        $this->ajax();

        $bo = new SalaDeAulaBO();
        try {

            // buscar as integrações do BlackBoard
            $bo = new IntegracaoBO();
            $ei = new EntidadeIntegracaoTO();
            $ei->setId_sistema(SistemaTO::BLACKBOARD);
            $arrIntegracao = $ei->fetch(false, false, false);
            if ($arrIntegracao) {
                foreach ($arrIntegracao as $ie) {
                    $key = json_decode($ie->getSt_codchave());
                    if ($key == false) {
                        echo "Entidade {$ie->getId_entidade()} sem integração com o BlackBoard<br>";
                        continue;
                    }

                    echo "<fieldset><legend>Entidade {$ie->getId_entidade()}</legend>";
                    $retorno = $bo->verificarSalas($ie->getId_entidade(), SistemaTO::BLACKBOARD);
                    foreach ($retorno->getMensagem() as $mensagem) {
                        echo $mensagem . '<br>';
                    }
                    echo "</fieldset>";
                } // foreach($arrIntegracao as $ie){
            } //$arrIntegracao

        } catch (Exception $e) {
            Zend_Debug::dump($e, __CLASS__ . '(' . __LINE__ . ')');
        }

    }


    public function inativarusuariocursoblackboardAction()
    {
        $this->ajax();
        try {

            // buscar as integrações do BlackBoard
            $bo = new IntegracaoBO();
            $ei = new EntidadeIntegracaoTO();
            $ei->setId_sistema(SistemaTO::BLACKBOARD);
//            $ei->setId_entidade(12);
            $arrIntegracao = $ei->fetch(false, false, false);
            if ($arrIntegracao) {
                foreach ($arrIntegracao as $ie) {
                    $key = json_decode($ie->getSt_codchave());
                    if ($key == false) {
                        echo "Entidade {$ie->getId_entidade()} sem integração com o BlackBoard<br>";
                        continue;
                    }

                    echo "<fieldset><legend>Entidade {$ie->getId_entidade()}</legend>";
                    $retorno = $bo->inativarUsuarioCurso($ie->getId_entidade(), SistemaTO::BLACKBOARD);
                    foreach ($retorno->getMensagem() as $mensagem) {
                        echo $mensagem . '<br>';
                    }
                    echo "</fieldset>";
                } // foreach($arrIntegracao as $ie){
            } //$arrIntegracao

        } catch (Exception $e) {
            Zend_Debug::dump($e, __CLASS__ . '(' . __LINE__ . ')');
        }

    }


    /**
     * Integra os Alunos ao BlackBoard
     */
    public function matricularalunoblackboardAction()
    {
        $this->ajax();
        try {


            $bo = new IntegracaoBO();
            $ei = new EntidadeIntegracaoTO();
            $ei->setId_sistema(SistemaTO::BLACKBOARD);
//            $ei->setId_entidade(115);
            $arrIntegracao = $ei->fetch(false, false, false);
            if ($arrIntegracao) {
                foreach ($arrIntegracao as $ie) {
                    $key = json_decode($ie->getSt_codchave());
                    if ($key == false) {
                        echo "Entidade {$ie->getId_entidade()} sem integração com o BlackBoard<br>";
                        continue;
                    }

                    echo "<fieldset><legend>Entidade {$ie->getId_entidade()}</legend>";
                    $retorno = $bo->integrarAlunos($ie->getId_entidade(), SistemaTO::BLACKBOARD, 120);
                    foreach ($retorno->getMensagem() as $mensagem) {
                        echo $mensagem . '<br>';
                    }
                    echo "</fieldset>";
                } // foreach($arrIntegracao as $ie){
            } //$arrIntegracao

        } catch (Exception $e) {
            Zend_Debug::dump($e, __CLASS__ . '(' . __LINE__ . ')');
        }

    }

    /**
     * Integra os Alunos ao BlackBoard alocados na ultima hora
     */
    public function matricularalunoblackboardhoraAction()
    {
        $this->ajax();
        try {

            // buscar as integrações do BlackBoard
            $bo = new IntegracaoBO();
            $ei = new EntidadeIntegracaoTO();
            $ei->setId_sistema(SistemaTO::BLACKBOARD);
            $arrIntegracao = $ei->fetch(false, false, false);
            if ($arrIntegracao) {
                foreach ($arrIntegracao as $ie) {
                    $key = json_decode($ie->getSt_codchave());
                    if ($key == false) {
                        echo "Entidade {$ie->getId_entidade()} sem integração com o BlackBoard<br>";
                        continue;
                    }

                    echo "<fieldset><legend>Entidade {$ie->getId_entidade()}</legend>";
                    $retorno = $bo->integrarAlunos($ie->getId_entidade(), SistemaTO::BLACKBOARD, 1);
                    foreach ($retorno->getMensagem() as $mensagem) {
                        echo $mensagem . '<br>';
                    }
                    echo "</fieldset>";
                } // foreach($arrIntegracao as $ie){
            } //$arrIntegracao

        } catch (Exception $e) {
            Zend_Debug::dump($e, __CLASS__ . '(' . __LINE__ . ')');
        }

    }

    /**
     * Confere e tenta corrigir os Alunos que não receberam seus grupos no BB
     */
    public function alunosgrupoblackboardAction()
    {

        try {

            $bo = new RoboBO();
            $me = $bo->corrigirAlunosGrupo(null);
            if ($me->getTipo() == Ead1_IMensageiro::SUCESSO) {
                foreach ($me->getMensagem() as $aluno) {
                    echo $aluno;
                }
            } else {
                echo $me->getFirstMensagem();
            }

        } catch (Exception $e) {
            \Zend_Debug::dump($e, __CLASS__ . '(' . __LINE__ . ')');
            exit;
        }
        exit;
    }

    public function testePosconfirmacaovendaAction()
    {
        ini_set('max_execution_time', 0);

        $idVenda = $this->_getParam('idVenda', null);

        if (empty($idVenda)) {
            die("Nenhuma venda foi informada");
        }

        $vendaTO = new \VendaTO();
        $vendaTO->setId_venda($idVenda);
        $vendaTO->fetch(true, true, true);

        $bo = new \PosConfirmacaoVendaBO();
        $bo->procedimentoPosConfirmacaoVenda($vendaTO);
        die();
    }

    public function testeAction()
    {
        ini_set('max_execution_time', 0);
        $boletoNegocio = new \G2\Negocio\BoletoPHP();
        $boletoNegocio->gerarPdfBoletosVenda(33464);
    }

    public function gerarPdfContratoAction()
    {
        $paBO = new PrimeiroAcessoBO();
        $paBO->gerarContratosInexistentes($paBO->lerId());
        exit;
    }

    public function gerarPdfContratoVendaAction()
    {
        $id_venda = $this->limpaGetAllParams($this->getAllParams());
        if ($id_venda) {

            if (is_array($id_venda['id_venda']) && $id_venda['id_venda']) {
                foreach ($id_venda['id_venda'] as $value) {
                    $negocio = new \G2\Negocio\Negocio();
                    $venda = $negocio->find('\G2\Entity\Venda', $value);
                    if ($venda) {
                        $paBO = new PrimeiroAcessoBO();
                        $contrato = $paBO->gerarPDFContrato($value);
                        if ($contrato->getTipo() == Ead1_IMensageiro::SUCESSO) {
                            echo "Contrato referente a venda " . $value . "gerado com sucesso!<br>";
                        } else {
                            echo "Problema ao gerar contrato referente a venda " . $value . "!<br>";
                        }
                    } else {
                        echo 'Venda ' . $value . ' não encontrada!';
                    }
                }
            } else {
                if (array_key_exists('id_venda', $id_venda) && $id_venda) {
                    $negocio = new \G2\Negocio\Negocio();
                    $venda = $negocio->find('\G2\Entity\Venda', $id_venda['id_venda']);
                    if ($venda) {
                        $paBO = new PrimeiroAcessoBO();
                        $contrato = $paBO->gerarPDFContrato($id_venda['id_venda']);
                        if ($contrato->getTipo() == Ead1_IMensageiro::SUCESSO) {
                            echo "Contrato referente a venda " . $id_venda['id_venda'] . "gerado com sucesso!<br>";
                        } else {
                            echo "Problema ao gerar contrato referente a venda " . $id_venda['id_venda'] . "!";
                        }
                    } else {
                        echo 'Venda ' . $id_venda['id_venda'] . ' não encontrada!';
                    }
                } else {
                    echo "Verifique os parâmetros passados! Id da venda não encontrado";
                }
            }
        } else {
            echo "Verifique os parâmetros passados!<br><br> Para utilizar a funcionalidade, insira no fim da url:<br>Uma venda: /id_venda/xxxxx <br> Varias vendas: ?id_venda[]=xxxxx&id_venda[]=yyyyy... ";
        }
        die;
    }

    public function gerarPdfContratoVendaEmLoteAction()
    {
        //recebe os parâmetros passados por GET
        $params = $this->limpaGetAllParams($this->getAllParams());

        //verifica se foi passado algum pârametro
        if (empty($params)) {
            echo "<b>Verifique os parâmetros passados</b>!<br><br>" .
                "Para utilizar a funcionalidade, insira no fim da url:<br><br><b>Uma entidade:</b> ?entidade=xxx<br><br>" .
                "A evolução padrão pesquisada é de venda 'Ativa', evolução '6',<br>" .
                "se quiser que adicionar como parâmetro <b>outras evoluções:</b> &evolucao[]=xx&/evolucao[]=xx&/evolucao[]=xx...";
            die;
        }

        //verifica se foi passado o número da entidade
        if (empty($params['entidade'])) {
            echo "Entidade não informada.";
            die;
        }

        $id_entidade = $params['entidade'];

        //verifica se foi passado alguma outra evolução para filtrar junto com a "ativa"
        $evolucoes = "6";
        if (isset($params['evolucao'])) {
            foreach ($params['evolucao'] as $evolucao) {
                $evolucoes .= ',' . $evolucao;
            }
        }

        //pesquisa as matriculas de acordo com os parâmetros passados
        $ngMatricula = new \G2\Negocio\Matricula();
        $arrayVendas = $ngMatricula->getRepository('\G2\Entity\Matricula')
            ->retornaMatriculasByEntidadeEvolucao($id_entidade, $evolucoes);

        //verifica se retornou algum registros
        if (empty($arrayVendas)) {
            echo "Sem vendas para essa entidade e esses parametros passados.";
            die;
        }

        //se retornou, pesquisa a venda e gera o contrato na pasta do aluno
        foreach ($arrayVendas AS $id_venda) {
            $venda = $ngMatricula->find('\G2\Entity\Venda', $id_venda['id_venda']);
            if ($venda) {
                $paBO = new PrimeiroAcessoBO();
                $contrato = $paBO->gerarPDFContrato($id_venda['id_venda']);
                if ($contrato->getTipo() == Ead1_IMensageiro::SUCESSO) {
                    echo "Contrato referente a venda " . $id_venda['id_venda'] . " gerado com sucesso!<br>";
                } else {
                    echo "Problema ao gerar contrato referente a venda " . $id_venda['id_venda'] . "!";
                }
            } else {
                echo 'Venda ' . $id_venda['id_venda'] . ' não encontrada!';
            }
        }
        die;
    }

    public function testePastaAction()
    {
        echo "{$_SERVER['DOCUMENT_ROOT']}" . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . "upload" . DIRECTORY_SEPARATOR . "minhapasta" . DIRECTORY_SEPARATOR;
        exit;
    }

    public function baixarParcelaFluxusAction()
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $negocio = new \G2\Negocio\TotvsVwIntegraGestorFluxus();
        $return = $negocio->baixarParcelaFluxus($this->getAllParams());

        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->json($return);
        } else {
            $this->_helper->viewRenderer->setNoRender(true);
            echo "<h1>Resultados:</h1>";
            \Zend_Debug::dump($return, __CLASS__ . '(' . __LINE__ . ')');
        }

        exit(0);
    }

    /**
     * Robô que verifica as alocacoes transferidas e não desalocadas e faz a desalocacao da mesma
     * @deprecated Não esta sendo executado
     */
    public function desalocarsalasdisciplinaAction()
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);
        $this->ajax();
        $bo = new SalaDeAulaBO();
        echo $bo->desalocarSalasMatriculaDisciplinaRobo()->getFirstMensagem();
    }

    /**
     * Robô que sincroniza alunos do moodle após a transferencia
     */
    public function sincronizaalusnoswsAction()
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);
        $this->ajax();
        $bo = new MoodleBO();

        $retorno = $bo->sincronizacaoAlunosMoodle();
        var_dump($retorno->getMensagem());
        die;
    }

    public function gerarMensagemVencimentoRecorrenteAction()
    {
        try {
            $this->bo->gerarMensagemVencimentoCartao();
            die('Mensagens geradas com sucesso');
        } catch (Exception $ex) {
            die('Erro ao gerar mensagens ' . $ex->getMessage());
        }
    }

    public function gerarpdfboletosvendaAction()
    {
        $boletoPHP = new \G2\Negocio\BoletoPHP();
        $idVenda = $this->_getParam('id_venda', null);
        if (empty($idVenda)) {
            die('idVenda não informado');
        }
        $boletoPHP->gerarPdfBoletosVenda($idVenda);

        $file = 'upload' . DIRECTORY_SEPARATOR . 'boletos' . DIRECTORY_SEPARATOR . $idVenda . DIRECTORY_SEPARATOR . 'boletos.pdf';
        header('Content-type: application/pdf');
        header('Content-Disposition: inline; filename="boletos.pdf"');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($file));
        header('Accept-Ranges: bytes');

        @readfile($file);

        exit;
    }

    public function verificarDadosAlunosIntegracaoPontosoftAction()
    {
        $integracaoExterna = new \G2\Negocio\IntegracaoExterna();

        $resultadoIntegracao = $integracaoExterna->sincronizarDadosAlunosPontosoft();

        $this->view->resultadoIntegracao = $resultadoIntegracao;
    }


    public function sincronizaGradePontoSoftAction()
    {
        $entidade = new \G2\Negocio\Negocio();

        //Buscas as entidades que estão registradas para enviar os alunos ao pontosoft
        $entidadeIntegracao = $entidade->findBy('\G2\Entity\EntidadeIntegracao', array('id_sistema' => 20));

        foreach ($entidadeIntegracao as $value) {
            $this->ajax();
            $integracaoExterna = new \G2\Negocio\IntegracaoExterna($value->getId_entidade(), true);
            $integracaoExterna->salvarGradePontoSoft();
        }
    }

    /**
     * Método responsável por identificar salas de aula reabertas
     * (alteração na data de encerramento ou adicionando os dias de extensão)
     * Buscar os alunos com papel encerrado e alterar seu papel para aluno ativo.
     * AC-26617
     * Débora Castro <debora.castro@unyleya.com.br>
     */
    public function reativarAlunosEncerradosMoodleAction()
    {

        $negocioSala = new \G2\Negocio\SalaDeAula();

        $alunos = $negocioSala->retornaVwAlunosReativarMoodle();

        $alocacaoBO = new MoodleBO();

        if (is_array($alunos) && !empty($alunos)) {

            $encerramento = $alocacaoBO->alteraAlunosSalaReaberta($alunos)->getMensagem();
            if (is_array($encerramento) && isset($encerramento['userid'])) {
                echo 'Perfil aluno encerrado inativado com sucesso!';
            } else {
                var_dump($encerramento);
                die;
            }

        } else {
            echo 'Sem alunos para reativar!';
        }


        exit;
//        $this->view->mensagem = $encerramento;


    }

    public function verificarCancelamentoMatriculaPontosoftAction()
    {

        $entidade = new \G2\Negocio\Negocio();

        //Buscas as entidades que estão registradas para enviar os alunos ao pontosoft
        $entidadeIntegracao = $entidade->findBy('\G2\Entity\EntidadeIntegracao', array('id_sistema' => 20));

        foreach ($entidadeIntegracao as $value) {
            $integracaoExterna = new \G2\Negocio\IntegracaoExterna($value->getId_entidade(), true);

            if ($integracaoExterna->sincronizarMatriculasTurmasFinalizada(20))
                die(true);
            else
                die(false);
        }
    }


    /**
     * Método responsável por retirar papel de coordenador pedagógico do moodle,
     * para coordenadores inativos em um determinado projeto
     * Busca os coordenadores com bl_desativarmoodle e perfil de integracao ativo
     * AC-26723
     * Débora Castro <debora.castro@unyleya.com.br>
     */
    public function removerPapelCoordenadorMoodleAction()
    {

        $negocioSala = new \G2\Negocio\ProjetoPedagogico();
        $params = $this->limpaGetAllParams($this->getAllParams());
        if (!is_array($params)) {
            $params = null;
        }

        $coordenadores = $negocioSala->retornaVwDesativarCoordenadoresMoodle($params);

        $Moodle = new MoodleBO();

        if (is_array($coordenadores) && !empty($coordenadores)) {

            $encerramento = $Moodle->retirarCoordenadoresSalaMoodle($coordenadores)->getMensagem();
            if (is_array($encerramento) && isset($encerramento['userid'])) {
                echo 'Perfil dos coordenadores inativados com sucesso!';
            } else {
                Zend_Debug::dump($encerramento);
            }

        } else {
            echo 'Sem coordenadores para remover perfil!';
        }
        exit;
    }

    /**
     * Retira o papel de profesor do moodle,
     * para professores que foram removidos em lote das salas de aula
     * Busca pelos professores com bl_desativarmoodle e e com perfil de professor - apos a remocao o bl_desativar volta a ser 0
     * AC-26882
     * @deprecated Não esta sendo executado
     */
    public function removerPapelProfessorMoodleAction()
    {

        $negocioSala = new \G2\Negocio\SalaDeAula();
        $professores = $negocioSala->retornaVwDesativarProfessoresMoodle();

        if (is_array($professores) && !empty($professores)) {

            $moodleBO = new MoodleBO();
            $encerramento = $moodleBO->desvinculaProfessorSalaMoodle($professores)->getMensagem();
            if (is_array($encerramento))
                echo $encerramento[0];

        } else {
            echo 'Sem professores para remover perfil!';
        }
        exit;
    }

    /**
     * Método responsável por criar uma notificaçao com salas que tem limite de alunos excedido (conforme condições previamente cadastradas)
     * Busca todos os perfis cadastrados para esta notificacao e gera mensagem interna e por e-mail (caso tenha essa configuração)
     * AC-26880
     * Débora Castro <debora.castro@unyleya.com.br>
     */
    public function notificacaoLimiteAlunosSalaAction()
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);
        $mensagemNegocio = new \G2\Negocio\Mensagem();
        $resposta = $mensagemNegocio->gerarNotificacaoLimiteSalaUsuario();
        echo $resposta->getFirstMensagem();
        exit;
    }

    /**
     * @description Função responsável por gerar entidades, com opção de ser minificado ou não.
     * @return bool
     */
    public function gerarJsEntidadesAction()
    {
        if ($this->getRequest()->getModuleName() == 'default') {
            $dirEntity = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . "library" . DIRECTORY_SEPARATOR . "G2" . DIRECTORY_SEPARATOR . "Entity";
            $dirBuildJS = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . "public" . DIRECTORY_SEPARATOR . "gestor2" . DIRECTORY_SEPARATOR . "build" . DIRECTORY_SEPARATOR . "js";

            if ($handle = opendir($dirEntity)) {
                $arquivoGeralJS = $dirBuildJS . DIRECTORY_SEPARATOR . 'all_entities.js';

                $handle = opendir($dirEntity);

                $content = '';
                $devMode = false;

                if ($this->getParam('dev') == 1) {
                    $devMode = true;
                    $content = "/*\n"
                        . "============================================\n"
                        . " ARQUIVO GERADO AUTOMATICAMENTE, NÃO EDITAR\n"
                        . " RESPONSÁVEL POR AGRUPAR TODAS AS MODELOS\n"
                        . " cache gerado em: " . date("d/m/Y H:i:s") . "\n"
                        . "============================================\n"
                        . "*/\n\n";
                }
                while (false !== ($file = readdir($handle))) {
                    if ($file != "." && $file != ".." && !is_dir($dirEntity . DIRECTORY_SEPARATOR . $file)) {
                        $baseName = basename($dirEntity . DIRECTORY_SEPARATOR . $file, ".php");
                        $repoEntity = "G2\\Entity\\" . $baseName;
                        //Recupera os atributos da entity
                        $metas = $this->em->getClassMetadata($repoEntity);

                        if ($devMode == 1) {
                            $content .= "var " . $baseName . " = Backbone.Model.extend({\n"
                                . "defaults: {\n";
                        } else {
                            $content .= "var " . $baseName . " = Backbone.Model.extend({defaults: {";
                        }

                        if ($metas->reflFields) {
                            $endArray = array_keys($metas->reflFields);
                            foreach ($metas->reflFields as $key => $attr) {
                                $comma = ',';

                                if ($key === end($endArray))
                                    $comma = '';

                                if ($devMode == 1) {
                                    $content .= "\t" . $key . ": ''" . $comma . "\n";
                                } else {
                                    $content .= $key . ": ''" . $comma . "";
                                }
                            }
                        }

                        if ($devMode == 1) {
                            $content .= "},\n";
                            $content .= "url: function() {\n";
                            $content .= "\treturn this.id ? '/api/" . $this->camelToDashed($baseName) . "/' + this.id : '/api/" . $this->camelToDashed($baseName) . "';\n";
                            $content .= "}\n";
                            $content .= "});\n";
                        } else {
                            $content .= "},url: function() {return this.id ? '/api/" . $this->camelToDashed($baseName) . "/' + this.id : '/api/" . $this->camelToDashed($baseName) . "';}});";
                        }
                    }
                }

                if (file_put_contents($arquivoGeralJS, $content)) {
                    closedir($handle);
                    echo 'Arquivo gerado com sucesso. Modo dev: ' . ($devMode ? 'Sim' : 'Não');

                } else {
                    echo 'Erro ao gerar arquivo. Sem permissão na pasta de destino.';
                }
            }
        }

        exit;
    }

    /**
     * Método responsável por integrar o G2S ao hubspot
     * @author helder silva <helder.silva@unyleya.com.br>
     */
    public function integrarGestorHubspotAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $negocioHubspot = new \G2\Negocio\IntegracaoHubspot();
        $result = $negocioHubspot->enviaDadosHubspot();
        if ($result) {
            echo 'Dados enviados para hubspot com sucesso!';
            exit;
        } else {
            echo 'Erro ao enviar arquivos para o hubspot';
            exit;
        }
    }

    public function gerarVersaoSistemaAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        if ($this->getParam('v') != null) {
            $versao = $this->getParam("v");

            try {
                $data = "{\n" .
                    "\t" . '"versao":' . '"' . $versao . '"' .
                    "\n}";

                if (file_put_contents(APPLICATION_PATH . '/../app.json', $data))
                    die('Versão gerado com sucesso');

            } catch (Exception $e) {
                throw new Exception($e->getMessage());
            }
        }
        die('Erro ao gerar versão');
    }

    /**
     * Job para envio de mensagens nao entregues aos devices que usam aplicativo portal do aluno
     *
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     * @throws Zend_Exception
     */
    public function zendJobPushNotificacaoAppAction()
    {

        $negocio = new \G2\Negocio\Negocio();

        $params = $negocio->getCurrentJobParams();

        $whereAnd = array();
        $whereLike = array();
        $whereIn = array();

        $alunos = Zend_Json::decode($params['alunos']);
        if ($alunos) {
            $whereIn['id_usuario'] = array_unique(array_column($alunos, 'id_usuario'));
        }

        $negocio = new \G2\Negocio\MensagemPortal();
        $res = $negocio->pushMensagensNotificacaoAppRegistrarEntrega($whereAnd, $whereLike, $whereIn);

        \Zend_Debug::dump($res);

        die('end');
    }

    public function zendJobLogSistemaAction()
    {
        $negocio = new \G2\Negocio\Log();
        $params = $negocio->getCurrentJobParams();
        $result = $negocio->processaJobGerarLog($params);
        \Zend_Debug::dump($result, 'result: ');
        die('end');
    }

    /**
     * Alterar a evolução das matriculas canceladas após x dias de acesso.
     */
    public function executarMatriculasCanceladasVencidasAction()
    {
        $negocio = new \G2\Negocio\Cancelamento();
        $negocio->retornaMatriculasCanceladasVencidas();
        die();
    }

    /**
     * Renovação automatica de matricula do aluno
     */
    public function renovaMatriculaGraduacaoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $params = array();

        if ($this->getParam('id_matricula')) {
            $params['id_matricula'] = $this->getParam('id_matricula');
        }

        $negocio = new \G2\Negocio\VendaGraduacao();
        $return = $negocio->renovacaoAutomaticaMatricula($params);
        echo $return->getFirstMensagem();
        exit;
    }

    public function zendJobCriarOcorrenciaJiraAction()
    {
        $negocio = new \G2\Negocio\JiraIssues();
        $params = $negocio->getCurrentJobParams();
        $result = $negocio->enviarIssue($params);
        \Zend_Debug::dump($result, 'result: ');
        die('end');
    }

    public function zendJobCriarInteracaoOcorrenciaJiraAction()
    {
        $negocio = new \G2\Negocio\JiraIssues();
        $params = $negocio->getCurrentJobParams();
        $result = $negocio->enviarInteracao($params);
        \Zend_Debug::dump($result, 'result: ');
        die('end');
    }

    /**
     * Action que sincroniza os alunos não alocados que estão aptos a serem
     * alocados
     */
    public function autoAlocarMatriculasAction()
    {
        $mensageiro = new Ead1_Mensageiro();

        $params = ZendJobQueue::getCurrentJobParams();

        $matriculas = isset($params['matriculas']) ? $params['matriculas'] : null;
        $id_venda = isset($params['id_venda']) ? $params['id_venda'] : null;

        if (is_array($matriculas) && $matriculas) {

            $ng_matricula = new \G2\Negocio\Matricula();

            foreach ($matriculas as $matricula) {
                $to = new MatriculaTO($matricula);
                $msg = $ng_matricula->alocarAutomatico($to, false, $id_venda);
                $mensageiro->addMensagem($msg->getFirstMensagem());
            }

        } else {
            $mensageiro->setMensageiro('Nenhuma matrícula informada.', \Ead1_IMensageiro::AVISO);
        }

        $this->_helper->json($mensageiro);
    }

    /**
     * Finaliza a renovação da matrícula
     */
    public function finalizarRenovacaoMatriculaAction()
    {
        echo 'Matriculas de renovação ativadas: <br><br>';

        try {
            $ng_matricula = new \G2\Negocio\Matricula();

            $matriculas = $ng_matricula->getRepository('\G2\Entity\VwMatricula')
                ->retornaMatriculasRenovacaoFinalizar();

            if (empty($matriculas)) {
                echo 'Nenhuma matricula para ser ativada!';

                # finaliza o processo informando ao SO que a funcionalidade
                # foi executada com sucesso
                exit(0);
            }

            $arrResult = array();

            foreach ($matriculas as $matricula) {

                # busca por albuma chamada à sessao
                # isso pode estar gerando o erro de não ter alterar os dados
                $resultado = $ng_matricula->finalizaMatriculaRenovacao($matricula);

                if ('success' == $resultado->getType()) {
                    printf('Matricula %s ativada com sucesso!<br>', $matricula['id_matricula']);
                } else {
                    printf('Matricula %s não ativada!<br>', $matricula['id_matricula']);
                }
            }

            # finaliza o processo informando ao SO que a funcionalidade
            # foi executada com sucesso
            exit(0);

        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function verificarAlunosAptosAgendamentoAction()
    {
        $today = date('Y-m-d');

        $negocio = new \G2\Negocio\GerenciaProva();
        $result = $negocio->tornarAlunosAptosAgendamentoPorDisciplina(array(), \G2\Constante\Usuario::SISTEMA_G2);

        $this->_helper->json($result);
    }

    public function primeiroAgendamentoAction()
    {
        $mensageiro = new \Ead1_Mensageiro();

        $id_matricula = $this->getParam('id_matricula');
        $params = array();

        if ($id_matricula) {
            $params['id_matricula'] = $id_matricula;
        }

        $negocio = new \G2\Negocio\GerenciaProva();

        $results = $negocio->agendarAlunosAptosPrimeiroAgendamento($params);
        $mensageiro->setMensagem($results);

        $this->_helper->json($mensageiro);
    }

    public function integrarvendafluxusAction()
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);
        ini_set('display_errors', 1);

        $negocio = new \G2\Negocio\TotvsVwIntegraGestorFluxus();
        $idVenda = $this->_getParam('id_venda', null);
        $return = $negocio->integrarVendaFluxus(
            array(
                'id_venda' => !empty($idVenda) ? str_replace('G2U', '', $idVenda) . 'G2U' : null,
            ),
            $this->_getParam('limit', null));
        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->json($return);
        } else {
            $this->_helper->viewRenderer->setNoRender(true);
            \Zend_Debug::dump($return, __CLASS__ . '(' . __LINE__ . ')');
            exit;
            echo "<h1>Resultados:</h1>";
            echo $return['message'] . '<br><br>';
            foreach ($return['data'] as $data) {
                echo $data . '<br>';
            }
        }
    }

    public function integrarprodutovendaAction()
    {
        try {
            set_time_limit(0);
            ini_set('max_execution_time', 0);
            ini_set('display_errors', 1);

            $negocio = new \G2\Negocio\TotvsVwIntegraGestorFluxus();
            $idVenda = $this->_getParam('id_venda', null);
            $return = $negocio->integrarProdutoVenda(
                array(
                    'id_venda' => !empty($idVenda) ? str_replace('G2U', '', $idVenda) : null,
                ),
                $this->_getParam('limit', null));
            if ($this->getRequest()->isXmlHttpRequest()) {
                $this->_helper->json($return);
            } else {
                $this->_helper->viewRenderer->setNoRender(true);
                echo "<h1>Resultados:</h1>";
                echo $return['message'] . '<br><br>';
                \Zend_Debug::dump($return, __CLASS__ . '(' . __LINE__ . ')');
                exit;
                foreach ($return['data'] as $data) {
                    echo $data . '<br>';
                }
            }
        } catch (Exception $message) {
            \Zend_Debug::dump($message, __CLASS__ . '(' . __LINE__ . ')');
            exit;
        }
    }

    public function integrarlancamentovendafluxusAction()
    {
        try {
            set_time_limit(0);
            ini_set('max_execution_time', 0);
            ini_set('display_errors', 1);

            $negocio = new \G2\Negocio\TotvsVwIntegraGestorFluxus();
            $idVenda = $this->_getParam('id_venda', null);
            $return = $negocio->integrarLancamentoVenda(
                array(
                    'id_venda' => !empty($idVenda) ? str_replace('G2U', '', $idVenda) : null,
                ),
                $this->_getParam('limit', null));
            if ($this->getRequest()->isXmlHttpRequest()) {
                $this->_helper->json($return);
            } else {
                $this->_helper->viewRenderer->setNoRender(true);
                echo "<h1>Resultados:</h1>";
                echo $return['message'] . '<br><br>';
                \Zend_Debug::dump($return, __CLASS__ . '(' . __LINE__ . ')');
                exit;
                foreach ($return['data'] as $data) {
                    echo $data . '<br>';
                }
            }
        } catch (Exception $message) {
            \Zend_Debug::dump($message, __CLASS__ . '(' . __LINE__ . ')');
            exit;
        }
    }

    public function postbackAction()
    {

        try {
            $this->_helper->layout->disableLayout();
            $pagarMe = new G2\Negocio\Pagarme();

            $params = $this->getAllParams();
            if (!empty($params['current_status']) && $params['current_status'] === 'waiting_payment') {
                $mensageiro = $pagarMe->atualizarStatusBoleto($params);
            } else {
                $mensageiro = $pagarMe->quitarTransacao($params);
            }

            if ($mensageiro->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                $this->getResponse()->setHttpResponseCode(400);
            }

            die('ok');

        } catch (\Exception $message) {
            $this->getResponse()->setHttpResponseCode(400);
            die($message->getMessage());
        }
    }

    public function createAccountBankAction()
    {

    }

    public function createAccountAction()
    {
        $pagarMe = new G2\Negocio\Pagarme();
        $return = $pagarMe->createAccountBank($this->getAllParams());
        $this->_helper->json(array('retorno' => $return));
    }

    public function allAccountsAction()
    {
        $pagarMe = new G2\Negocio\Pagarme();
        $this->_helper->json($pagarMe->getAllAccountBank($this->getAllParams()));
    }

    /**
     * Importa conteúdo para salas de aula do moodle
     * a partir de uma sala de referencia
     */
    public function importasalasmoodleAction()
    {
        //configurações para evitar estouro de execução
        set_time_limit(0);
        ini_set('max_execution_time', 0);
        ini_set('display_errors', 1);


        $parametros = $this->limpaGetAllParams($this->getAllParams());
        if ($parametros && array_key_exists('max', $parametros)) {
            $max_execute = $parametros['max'];
        } else {
            $max_execute = 150;
        }

        $negocio = new \G2\Negocio\SalaDeAula();
        $return = $negocio->importarSalaDeAulaMoodle($max_execute);
        $this->view->mensagem = $return;
    }

    public function sincronizarVendedoresAction()
    {

        $config = Ead1_Ambiente::geral();
        $ws = new actor_Vendedor($config->get('webservicecodentidade'), $config->get('webservicechave'), rtrim($config->get('wsURL'), '/') . '/');

        $dados = $ws->retornarVendedores();
        $this->view->dados = array();

        if ($dados && $dados['status'] == 1) {
            if (isset($dados['vendedores']) && $dados['vendedores']) {
                $ng_vendedor = new \G2\Negocio\Vendedor();
                foreach ($dados['vendedores'] as $data) {
                    $this->view->dados[] = $ng_vendedor->salvar($data);
                }
            } else {
                echo 'Nenhum vendedor para ser sincronizado.';
                exit;
            }
        } else {
            echo 'Erro ao sincronizar vendedores.';
            exit;
        }
    }

    public function sincronizarAfiliadosAction()
    {

        $config = Ead1_Ambiente::geral();
        $ws = new actor_Afiliados($config->get('webservicecodentidade'), $config->get('webservicechave'), rtrim($config->get('wsURL'), '/') . '/');

        $dados = $ws->retornarAfiliados();
        $this->view->dados = array();

        if ($dados && $dados['status'] = 1) {

            if (isset($dados['afiliados']) && $dados['afiliados']) {

                $ng_afiliado = new G2\Negocio\Afiliado();
                foreach ($dados['afiliados'] as $data) {
                    $this->view->dados[] = $ng_afiliado->salvaAfiliado($data);
                }
            } else {
                echo 'Nenhum afiliado para ser sincronizado.';
                exit;
            }
        } else {
            echo 'Erro ao sincronizar afiliados.';
            exit;
        }
    }

    public function sincronizarNucleoTelemarketingAction()
    {

        $config = Ead1_Ambiente::geral();
        $ws = new actor_NucleoTelemarketing($config->get('webservicecodentidade'), $config->get('webservicechave'), rtrim($config->get('wsURL'), '/') . '/');

        $dados = $ws->retornarNucleoTelemarketing();
        $this->view->dados = array();

        if ($dados && $dados['status'] == 1) {
            if (isset($dados['nucleos']) && $dados['nucleos']) {
                $ng_nucleotelemarketing = new G2\Negocio\NucleoTelemarketing();

                foreach ($dados['nucleos'] as $data) {
                    $this->view->dados[] = $ng_nucleotelemarketing->salvaNucleoTelemarketing($data);
                }

            } else {
                echo 'Nunhum núcleo de telemarketing para ser sincronizado.';
                exit;
            }
        } else {
            echo 'Erro ao sincronizar os núcleos de telemarketing.';
            exit;
        }
    }

    public function reenviarpostbackAction()
    {
        $pagarMe = new \G2\Negocio\Pagarme();
        $pagarMe->reenviarPostback($this->getParam('id_transacao', null), $this->getParam('id_postback', null), $this->getParam('id_entidade', null));
        die;
    }

    public function reenviarpostbackloteAction()
    {
        $pagarMe = new \G2\Negocio\Pagarme();
        $pagarMe->reenviarpostbacklote($this->getParam('id_entidade', null), $this->getParam('id_transacao', null));
        die;
    }

    public function testeCartaoAction()
    {

    }

    public function transactionCreditCardAction()
    {
        $pagarMe = new G2\Negocio\Pagarme();
        $this->_helper->json($pagarMe->transactionCreditCard($this->getAllParams()));
    }

    public function recorrenteAction()
    {
        $pagarMe = new G2\Negocio\Pagarme();
        $pagarMe->transactionSubscription();
    }

    public function corrigirEntidadeOcorrenciasAction()
    {
        $negocio = new \G2\Negocio\Ocorrencia();
        $response = $negocio->corrigirEntidadeOcorrencias();

        if ($response['results']) {
            echo count($response['success']) . ' ocorrência(s) ajustada(s) num total de ' . count($response['results']) . ' ocorrência(s) incorreta(s).';
        } else {
            echo 'Nenhuma ocorrência com entidade incorreta encontrada.';
        }

        exit;
    }

    public function removerProfessoresSalasEncerradasAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        $negocio = new \G2\Negocio\Professores();
        $response = $negocio->removerProfessoresSalasEncerradasMoodle($this->limpaGetAllParams($this->getAllParams()));

        if ($response['results']) {
            echo count($response['success']) . ' perfil(s) de professor(es) removido(s) num total de ' . count($response['results']) . ' registro(s).';

            if (count($response['error'])) {
                $this->view->fields = array(
                    'message' => 'Status',
                    'id_perfilreferenciaintegracao' => 'id_perfilreferenciaintegracao',
                    'id_perfilreferencia' => 'id_perfilreferencia',
                    'id_saladeaula' => 'id_saladeaula',
                    'id_usuario' => 'id_usuario',
                    'id_entidade' => 'id_entidade',
                    'st_codsistemacurso' => 'st_codsistemacurso',
                    'id_entidadeintegracao' => 'id_entidadeintegracao'
                );
                $this->view->values = $response['error'];
                $this->view->title = 'Erros';
                echo $this->view->render('robo/output.phtml');
            }
        } else {
            echo 'Nenhum perfil de professor com sala já encerrada encontrado.';
        }
    }

    /**
     * @author: helder Silva <helder.silva@unyleya.com.br>
     * Executa o job de sincronização de usuários
     */
    public function zendJobSincronizaPessoaAction()
    {
        $negocio = new \G2\Negocio\Pessoa();
        $params = $negocio->getCurrentJobParams();
        $result = $negocio->processaSincronizacao($params);
        \Zend_Debug::dump($result, 'result: ');
        die('end');
    }

    /**
     * @author: Helder Silva <helder.silva@unyleya.com.br>
     * Verifica se existe um aluno com a evolução cursando em turma com data de
     * encerramento vencida de torna ele concluinte
     *
     * Pode ser passado um parametro max para definir o manualmente o máximo de
     * registros a ser executado de uma única vez.
     */
    public function verificarAlunoTurmaConcluinteAction()
    {

        $parametros = $this->limpaGetAllParams($this->getAllParams());
        if ($parametros && array_key_exists('max', $parametros)) {
            $max_execute = $parametros['max'];
        } else {
            $max_execute = 100;
        }

        $negocio = new \G2\Negocio\Matricula();
        $resultado = $negocio->tornarMatriculaConcluinteTurmaEncerrada($max_execute);
        if (is_array($resultado)) {
            $this->view->resultado = $resultado;
        } else {
            $this->view->mensagem = $resultado->getMensagem()[0];
        }
    }

    /**
     * Robô que envia os alunos agendados no G2 para o sistema de provas.
     * @throws Exception
     */
    public function agendarAlunosMaximizeAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        $negocio = new \G2\Negocio\Maximize();
        $mensageiro = $negocio->agendarAlunosMaximize();

        if ($mensageiro instanceof Ead1_Mensageiro) {
            /** @var Ead1_Mensageiro $item */
            $arrMensagens = [];
            if ($mensageiro->getType() == Ead1_IMensageiro::TYPE_SUCESSO) {
                foreach ($mensageiro->getMensagem() as $item) {
                    $arrMensagens[]["message"] = json_encode($item) . "<br>";
                }
            } else {
                $arrMensagens[]["message"] = $mensageiro->getText() . "<br>";
            }

            $this->view->fields = array(
                'message' => 'Status',
            );
            $this->view->values = $arrMensagens;
            $this->view->title = 'Robô ' . count($arrMensagens);
            echo $this->view->render('robo/output.phtml');
        } else {
            echo 'Nenhum agendamento para ser integrado ao sistemas de provas.';
        }
    }

    /**
     * Robô responsável por alterar matrículas para a evolução Decurso de Prazo
     * @access public
     * @author Arthur Luiz Lara Quites <arthur.quites@unyleya.com.br>
     */
    public function alterarMatriculaDecursoDePrazoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $matricula = new \G2\Negocio\Matricula();
        $matriculas_alterar = $matricula->findAll('\G2\Entity\VwAlterarMatriculaDecursoDePrazo');
        if (!empty($matriculas_alterar)) {
            $resultado = $matricula->alterarMatriculaDecursoDePrazo($matriculas_alterar);
            echo "Matrículas alteradas: ";
            print_r($resultado['sucesso']);
            echo "Matrículas que falharam: ";
            print_r($resultado['erro']);
        } else {
            echo "Não existe nenhuma matrícula a ser alterada para Decurso de Prazo.";
        }
    }

    public function getallpostbacksAction()
    {
        $pagarme = new \G2\Negocio\Pagarme();
        $pagarme->setApiKey(14);
        echo '<pre>';
        var_dump($pagarme->getAllPostbacks(2014196));
        die;
    }

    public function testAction()
    {
        try {

            $smtp = 'smtp.gmail.com';

            $config = array(
                'auth' => 'login',
                'username' => 'unyleyadev@gmail.com',
                'password' => 'unyleya00',
                'ssl' => 'ssl',
                'port' => '465'
            );

            $mailTransport = new Zend_Mail_Transport_Smtp($smtp, $config);

            $mail = new Zend_Mail();
            $mail->setFrom('gabriel.resende@unyleya.com.br');
            $mail->addTo('gabriel.resende@unyleya.com.br');
            $mail->setBodyText('teste');
            $mail->setSubject('teste');
            $mail->send($mailTransport);

            echo "Email enviado com SUCESSSO!";
        } catch (Exception $e) {
            echo($e->getMessage());
        }
        die;
    }

    public function enviaMensagemAvisoAgendamentoAction()
    {
        $this->ajax();

        $negocio = new \G2\Negocio\GerenciaProva();
        $response = $negocio->enviaMensagemAvisoAgendamento();

        if ($response instanceof \Ead1_Mensageiro) {
            echo $response->getText();
        } else if (!empty($response['results'])) {
            echo count($response['success']) . ' email(s) enviado(s) com sucesso num total de ' . count($response['results']) . ' registro(s).';

            if (count($response['error'])) {
                $this->view->fields = array(
                    'message' => 'Status',
                    'id_matricula' => 'Matrícula',
                    'st_nomecompleto' => 'Aluno',
                    'st_disciplina' => 'Disciplina',
                    'id_avaliacaoagendamento' => 'id_avaliacaoagendamento'
                );
                $this->view->values = $response['error'];
                $this->view->title = 'Erros';
                echo $this->view->render('robo/output.phtml');
            }
        } else {
            echo 'Não há e-mails para serem enviados.';
        }
    }

    public function processarCensoAction()
    {
        $this->ajax();

        $params = $this->limpaGetAllParams($this->getAllParams());

        try {
            $negocio = new \G2\Negocio\Censo();
            $response = $negocio->gerarDadosCenso($params);

            echo 'Resultados: ' . count($response['results']) . '<br/>';
            echo 'Sucessos: ' . count($response['success']) . '<br/>';
            echo 'Erros: ' . count($response['error']) . '<br/>';

            if (!empty($response['error'])) {
                $this->view->fields = array(
                    'id_matricula' => 'Matrícula',
                    'st_nomeentidade' => 'Entidade',
                    'st_nome' => 'Aluno',
                    'message' => 'Status'
                );

                $this->view->title = 'Erros';
                $this->view->values = $response['error'];

                echo $this->view->render('robo/output.phtml');
            }
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(500)->appendBody($e->getMessage());
        }
    }

    public function boletopdfAction()
    {
        $this->ajax();
        $ch = curl_init();


        curl_setopt($ch, CURLOPT_URL, 'https://api.pagar.me/1/boletos/live_cjb3jod1q1z0uqk3dugbjtysb');

        // define que o conteúdo obtido deve ser retornado em vez de exibido
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $retorno = curl_exec($ch);
        curl_close($ch);

        $mpdf = new \Ead1_IMPdf();
        $mpdf->carregarHTML('<style type="text/css">
                            body { font-size: 10px }
                            .barcode { height: 50px; font-size: 1px}
                        </style>');
        $mpdf->carregarHTML($retorno);

        $mpdf->gerarPdf('boleto.pdf',
            'upload/boletos/' . 1,
            'I');
    }

    /**
     * Robo responsável por enviar de uma mensagem automática via (e-mail e/ou sms)
     * aos alunos inadimplentes e apto a renovar academicamente alertando a divida
     * abrindo automaticamente uma ocorrência ao setor responsável (financeiro - cobranças)
     * @access public
     * @author Janilson Mendes <janilson.silva@unyleya.com.br>
     */
    public function enviarAvisoAlunosInadimplentesAction()
    {
        $this->ajax();
        $negocio = new \G2\Negocio\VwAlunosForaRenovacao();

        $response = $negocio->retornaAlunosForaRenovacao();

        if ($response instanceof \Ead1_Mensageiro) {
            echo $response->getText();
        } else if (!empty($response['results'])) {
            echo count($response['success']), ' ocorrência(s) feita(s) com sucesso num total de ',
            count($response['results']), ' registro(s).';

            if (count($response['error'])) {
                $this->view->fields = array('message' => 'Status',
                    'id_matricula' => 'Matrícula',
                    'st_nomecompleto' => 'Aluno',);

                $this->view->values = $response['error'];
                $this->view->title = 'Erros';
                echo $this->view->render('robo/output.phtml');
            }
        } else {
            echo 'Não há ocorrências para serem enviados.';
        }
    }


    /**
     * TODO Robô temporário para sincronizar as notas de alguns alunos
     * @throws Exception
     */
    public function sincronizaMaximizeTempAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        try {
            $negocio = new \G2\Negocio\Maximize();

            $result = $negocio->getem()
                ->getConnection()
                ->executeQuery("SELECT TOP 10 * FROM tb_sincronizamaximizetemp WHERE bl_sincronizado = 0")
                ->fetchAll();

            if ($result) {
                foreach ($result as $item) {
                    $getAvaliacao = $negocio->getAvaliacaoAgendamento([
                        'cod_prova' => $item['codProva'],
                        'id_usuario' => $item['codigoUsuarioSistemaOrigem']
                    ]);

                    try {
                        $notas = $negocio->prepararNotas($item["pontuacaoObtida"], $getAvaliacao);
                    } catch (Exception $e) {
                        Zend_Debug::dump($e->getMessage());
                        continue;
                    }

                    $result = $negocio->salvarNotas(
                        $notas,
                        null,
                        null
                    );
                    if ($result->getType() === Ead1_IMensageiro::TYPE_SUCESSO) {
                        $stmt = $negocio->getem()->getConnection()
                            ->prepare("UPDATE tb_sincronizamaximizetemp SET bl_sincronizado = 1 WHERE id = {$item["id"]}");
                        $stmt->execute();
                    }

                    Zend_Debug::dump($result->getText());
                }
            }


        } catch (Exception $exception) {
            Zend_Debug::dump($exception->getMessage());
            $stringLog = (date('Y-m-d H:i:s') . ' in '
                . $exception->getFile() . ':(' . $exception->getLine() . ') - ' . $exception->getMessage());
            \G2\Utils\LogSistema::enviarLogNewRelic($stringLog);
        }
    }


    /**
     * Robô que utiliza o job queue do zend para notificar os alunos que a sua nota foi lançada,
     * esse robô é disparado em \G2\Negocio\Maximize::salvarNotas
     * @throws Exception
     */
    public function notificarAlunoNotaMaximizeAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        try {
            $avaliacaoAlunoNegocio = new \G2\Negocio\AvaliacaoAluno();
            $params = $avaliacaoAlunoNegocio->getCurrentJobParams();

            $result = $avaliacaoAlunoNegocio->notificarAluno($params["id_matricula"], $params["id_entidade"]);
            Zend_Debug::dump($result);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function encerrarOccorrenciaMatriculaCanceladaAction()
    {
        $this->ajax();
        $_ocorrencia = new \G2\Negocio\Ocorrencia();
        $mensageiro = $_ocorrencia->roboEncerrarOcorrencia();
        if ($mensageiro instanceof Ead1_Mensageiro) {
            /** @var Ead1_Mensageiro $item */
            $arrMensagens = [];

            $arrMensagens[]["message"] = $mensageiro->getText() . "<br>";

            $this->view->fields = array(
                'message' => 'Status',
            );
            $this->view->values = $arrMensagens;
            $this->view->title = 'Encerrar Ocorrencias Matriculas Canceladas';
            echo $this->view->render('robo/output.phtml');
        } else {
            echo 'Nenhum agendamento para ser integrado ao sistemas de provas.';
        }
    }

    /**
     * Robô que envia mensagens de cobrança quando um pagamento recorrente não é pago
     */
    public function notificarPagamentoRecorrenteEmAtrasoAction()
    {
        try {
            $neg = new G2\Negocio\Venda();
            $lancamentos = $neg->notificarPagamentoRecorrenteEmAtraso();
            if (empty($lancamentos)) {
                $this->view->aviso = "Nenhuma parcela pendente encontrada para envio";
            } else {
                $this->view->error = $lancamentos['error'];
                $this->view->success = $lancamentos['success'];


                $html = $this->view->render('robo/notificar-pagamento-recorrente-em-atraso.phtml');

                $factory = new Ead1_Mail_Config_Factory('id_emailconfig = 6');
                $mail = new Zend_Mail('UTF-8');
                $mail->addTo('pagarmedds@unyleya.com.br', 'Pagamentos UnyLeya');
                $mail->addTo('elciomgdf@gmail.com', 'Elcio Guimarães');
                $mail->setBodyHtml($html);
                $mail->setSubject(strtoupper(Ead1_Ambiente::getAmbiente()) . ': G2S Notificações Cartão Recorrente em atraso');
                $factory->sendMail($mail);

                $this->_helper->viewRenderer->setNoRender(true);
                echo $html;

            }
        } catch (Exception $e) {
            $this->view->aviso = $e->getMessage();
        }
    }
}

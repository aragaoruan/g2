<?php

/**
 * Controller para Mensagem Portal
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @package default
 * @subpackage controller
 * @since 2015-02-12
 */

use G2\Negocio\MensagemPortal;

class Default_MensagemPortalController extends Ead1_ControllerRestrita
{
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new MensagemPortal();
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('mensagem-portal/index.js');
    }

    public function indexAction()
    {
        $id = $this->getParam('id', null);
        if (!empty($id)) {
            $dados = $this->negocio->findVwMensagemPortal($id);
            foreach ($this->negocio->toArrayEntity($dados->getMensagem()[0]) as $key => $value) {
                $this->view->$key = $value;
            }
        }
        $this->view->id = $id;
        echo $this->view->headScript()
            ->appendScript($this->view->render('/mensagem-portal/index.js'));
        echo $this->view->headScript()
            ->appendScript('/js/jquery.form.js');
    }

    public function getTurmaByProjetoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $negocio = new \G2\Negocio\MensagemPortal();
        $mensageiro = $negocio->findByVwTurmaProjeto($this->getParam('id_projetopedagogico'));
        $results = array();
        foreach ($mensageiro as $entity) {
            $results[] = $negocio->toArrayEntity($entity);
        }

        if (!$results) {
            $results = new \Ead1_Mensageiro();
            $results->setMensageiro('Nenhuma turma encontrada', \Ead1_IMensageiro::AVISO, null);
        }

        $this->_helper->json($results);
    }


    public function salvarMensagemPortalAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $negocio = new \G2\Negocio\MensagemPortal();
        $result = $negocio->salvarMensagemPortalPorEntidade($this->getAllParams());
        $this->getResponse()->setHttpResponseCode(201);
        $this->_helper->json($result);
    }

}

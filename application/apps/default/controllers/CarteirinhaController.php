<?php

/**
 * Class Default_CarteirinhaController
 * Controller para Carteirinha
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Default_CarteirinhaController extends Ead1_ControllerRestrita
{

    /**
     * @var \G2\Negocio\Pessoa
     */
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Pessoa();
    }

    public function montaCarteirinhaAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());

        $result = $this->negocio->retornarDadosCarteirinha($params);

        $possui_integracao = $this->negocio->findOneBy('\G2\Entity\EntidadeIntegracao', array(
                'id_entidade' => $this->negocio->sessao->id_entidade,
                'id_sistema'  => 20
            )
        );

        // Verifica se a entidade tem integracao com PontoSoft e salva o dados
        if ($possui_integracao) {
            $integacaoExterna = new \G2\Negocio\IntegracaoExterna(Ead1_Sessao::getObjetoSessaoGeral()->getid_entidade(), true);
            $integacaoExterna->salvarDadosAlunoPontoSoft($result, 20);
        }

        $tramite = new \G2\Negocio\Tramite();
        $tramite->salvarTramiteMatricula('Impressão de carteirinha', $result['id_matricula'], \G2\Constante\TipoTramite::CARTEIRINHA);

        // Descobrir o HTML MODELO da carteirinha
        $ng_contratoregra = new \G2\Negocio\ContratoRegra();
        $modelo = $ng_contratoregra->getModeloCarteirinha($result['id_modelocarteirinha']);

        $this->view->dados = $result;
        $this->view->html .= $this->view->render('/carteirinha/header-carteirinha.phtml');
        $this->view->html .= $this->view->render('/carteirinha/modelos/' . $modelo);
        $this->view->html .= $this->view->render('/carteirinha/footer-carteirinha.phtml');
    }

    public function barCodeAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        if ($this->getParam('text')) {
            echo $this->view->barCode($this->getParam('text'),40,false,'#F2F3F3');
        } else {
            echo 'Parametro text não encontrado.';
        }
    }

    public function imprimirLoteAction()
    {
        $params = json_decode($this->getParam('usuarios'));
        $html = '';

        $semCarteirinha = false;
        if ($params) {
            $html .= $this->view->render('/carteirinha/header-carteirinha.phtml');
            foreach ($params as $param) {
                $matricula = $this->negocio->findOneBy('\G2\Entity\VwMatricula', array(
                    'id_usuario'             => $param->id_usuario,
                    'id_turma'               => $param->id_turma,
                    'id_entidadeatendimento' => $this->negocio->sessao->id_entidade,
                    'bl_ativo'               => true,
                    'id_evolucao'            => G2\Constante\Evolucao::TB_MATRICULA_CURSANDO
                ));

                if ($matricula) {
                    $contrato = $this->negocio->findOneBy('\G2\Entity\VwContratoCarteirinha', array(
                        'id_contrato' => $matricula->getId_contrato()
                    ));

                    if ($contrato) {
                        $arrData = array(
                            'id_usuario'           => $param->id_usuario,
                            'id_turma'             => $param->id_turma,
                            'id_entidade'          => $this->negocio->sessao->id_entidade,
                            'id_modelocarteirinha' => $contrato->getId_modelocarteirinha(),
                            'id_matricula'         => $matricula->getId_matricula()
                        );

                        $result = $this->negocio->retornarDadosCarteirinha($arrData);

                        // Descobrir o HTML MODELO da carteirinha
                        $ng_contratoregra = new \G2\Negocio\ContratoRegra();
                        $modelo = $ng_contratoregra->getModeloCarteirinha($contrato->getId_modelocarteirinha());

                        //GERA TRAMITE NA IMPRESSÃO DA CERTEIRINHA

                        $tramite = new \G2\Negocio\Tramite();
                        $tramite->salvarTramiteMatricula('Impressão de carteirinha', $matricula->getId_matricula(), \G2\Constante\TipoTramite::CARTEIRINHA);

                        $this->view->dados = $result;
                        $html .= $this->view->render('/carteirinha/modelos/' . $modelo);
                    } else {
                        print_r('<br><br> Nome do aluno: ' . $matricula->getSt_nomecompleto() . ' - Matricula: ' . $matricula->getId_matricula());
                        $semCarteirinha = true;
                    }
                }
            }

            $html .= $this->view->render('/carteirinha/footer-carteirinha.phtml');
        }

        if ($semCarteirinha) {
            $this->view->html = "Nenhum dado para impressão. <br>Verifique se os usuários abaixo estão devidamente matriculados e se a Regra de Contrato esta configurada com o Modelo da Carteirinha.";
        } else {
            $this->view->html = $html;
        }

    }
}

<?php

/**
 * Description of Default MaterialArquivoEnvioController
 *
 * @author Debora Castro <debora.castro@unyleya.com.br>
 */
class Default_MaterialArquivoEnvioController extends Ead1_ControllerRestrita

{

    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Material();
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('material-arquivo-envio/index.js');
    }

    public function indexAction()
    {
        $this->negocio = new \G2\Negocio\Material();
    }

    public function materialGerarPacoteAction()
    {

        set_time_limit(0);
        ini_set('max_execution_time', 0);
        ini_set('display_errors', 1);

        $result = array();
        $this->negocio = new \G2\Negocio\Material();

        if ($this->getRequest()->isPost()) {
            $result = $this->negocio->gerarPacoteAlunos($this->getAllParams());
        }
        $this->_helper->json($result);
    }

    public function pesquisarPacoteMaterialAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $array = array();
        $this->negocio = new \G2\Negocio\Material();

        $result = $this->negocio->pesquisarVwEntregaMaterial($this->getAllParams());
        if (is_array($result) && !empty($result)) {
            foreach ($result as $row) {
                $row['dt_criacaopacote'] = date_format($row['dt_criacaopacote'], 'd/m/Y');
                $row['dt_entrega'] = $row['dt_entrega'] != null ? date_format($row['dt_entrega'], 'd/m/Y') : '';
                array_push($array, $row);
            }
        }
        $this->getResponse()->appendBody(json_encode($array));
    }

    public function salvarDataEntregaMaterialAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->negocio = new \G2\Negocio\Material();

        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json_Decoder::decode($body);
        $result = $this->negocio->salvarDataEntregaMaterial($data);


        $this->_helper->json($result);
    }

    public function materialGerarArquivoEnvioAction()
    {

       $this->_helper->viewRenderer->setNoRender();
        $this->negocio = new \G2\Negocio\Material();


        //Recupera os parametros
        $params = $this->limpaGetAllParams($this->getAllParams());
        $sessao = new Zend_Session_Namespace('geral'); //pega os dados da sessão
        $loginBO = new LoginBO(); //instancia a bo de login
        $xls = new Ead1_GeradorXLS(); //Instancia o XLS

        //recupera os dados do usuario logado
        $usuario = $loginBO->retornaDadosUsuario($sessao->id_usuario, $sessao->id_entidade, $sessao->id_perfil);
        $usuarioTO = $usuario->getFirstMensagem(); //Pega a primeira mensagem do mensageiro


        //Monta o array do cabeçalho
        $cabecalhos = array(
            'id_pacote' => 'Código do Pacote',
            'st_cpf' => 'CPF',
            'st_nomecompleto' => 'Nome do Aluno',
            'st_endereco' => 'Logradouro',
            'nu_numero' => 'Número',
            'st_complemento' => 'Complemento',
            'st_bairro' => 'Bairro',
            'st_nomemunicipio' => 'Cidade',
            'sg_uf' => 'UF',
            'st_cep' => 'CEP',
            'id_itemdematerial' => 'Código Item',
            'st_disciplina' => 'Nome da Disciplina',
            'st_projetopedagogico' => 'Projeto Pedagógico',
            'st_areaconhecimento' => 'Área',
            'st_nomeentidade' => 'Entidade'
        );

        $arXlsHeaderTO = array();
        foreach ($cabecalhos as $key => $value) {
            $xlsHeaderTO = new XLSHeaderTO();
            $xlsHeaderTO->setSt_header(utf8_decode($value));
            $xlsHeaderTO->setSt_par($key);
            $arXlsHeaderTO[] = $xlsHeaderTO;
        }
        //Busca os dados
        $result = $this->negocio->gerarArquivoDeEnvio($params);

        //Verifica se retornou resultado
        if ($result) {
            $arrParametroTO = Ead1_TO_Dinamico::encapsularTo($result, new ParametroRelatorioTO(), false, false, false);
            $x = new XLSConfigurationTO();
            $x->setTitle("Arquivo de Postagem");
            $x->setFooter('Gerado por: ' . $usuarioTO->getSt_nomecompleto() . ' Data: ' . date('d/m/Y H:i:s'));
            $x->setFilename('arquivo-de-envio');
            $xls->geraXLS($arXlsHeaderTO, $arrParametroTO, $x, false);
        }else{

            echo '<h2>Sem resultados para este período!</h2>';
        }
    }

}


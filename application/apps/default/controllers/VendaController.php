<?php

/**
 * Class Default_VendaController
 * @author Rafael Bruno
 */
class Default_VendaController extends Ead1_ControllerRestrita
{

    /**
     * @var \G2\Negocio\Venda $negocio
     */
    private $_negocio;

    public function init()
    {
        parent::init();
        $this->_negocio = new \G2\Negocio\Venda();
    }

    public function indexAction()
    {
        echo $this->view->headLink()
            ->appendStylesheet('/css/auto-complete-pessoa.css');
        $id = $this->getParam('id');

        $venda = NULL;

        if ($id) {
            $result = $this->_negocio->findVenda($id);
            if ($result) {
                $venda = array(
                    'id_venda' => $result->getIdVenda(),
                    'dt_cadastro' => $result->getDtCadastro(),
                    'nu_descontoporcentagem' => $result->getNuDescontoporcentagem(),
                    'nu_descontovalor' => $result->getNuDescontovalor(),
                    'nu_juros' => $result->getNuJuros(),
                    'bl_ativo' => $result->getBlAtivo(),
                    'id_usuariocadastro' => $result->getIdUsuariocadastro(),
                    'nu_valorliquido' => $result->getNuValorliquido(),
                    'nu_valorbruto' => $result->getNuValorbruto(),
                    'id_entidade' => $result->getIdEntidade(),
                    'nu_parcelas' => $result->getNuParcelas(),
                    'bl_contrato' => $result->getBlContrato(),
                    'nu_diamensalidade' => $result->getNuDiamensalidade(),
                    'id_origemvenda' => $result->getIdOrigemvenda(),
                    'dt_agendamento' => $result->getDtAgendamento(),
                    'dt_confirmacao' => $result->getDtConfirmacao(),
                    'st_observacao' => $result->getStObservacao(),
                    'nu_valoratualizado' => $result->getNuValoratualizado(),
                    'recorrete_orderid' => $result->getRecorreteOrderid(),
                    'id_formapagamento' => $result->getIdFormapagamento() ? $result->getIdFormapagamento()->getId_formapagamento() : NULL,
                    'id_campanhacomercial' => $result->getIdCampanhacomercial() ? $result->getIdCampanhacomercial()->getId_campanhacomercial() : NULL,
                    'id_evolucao' => $result->getIdEvolucao() ? $result->getIdEvolucao()->getId_evolucao() : NULL,
                    'id_situacao' => $result->getIdSituacao() ? $result->getIdSituacao()->getId_situacao() : NULL,
                    'id_usuario' => $result->getIdUsuario() ? $result->getIdUsuario()->getId_usuario() : NULL,
                    'id_tipocampanha' => $result->getIdTipocampanha() ? $result->getIdTipocampanha()->getId_tipocampanha() : NULL,
                    'id_prevenda' => $result->getIdPrevenda(),
                    'id_protocolo' => $result->getIdProtocolo() ? $result->getIdProtocolo()->getIdProtocolo() : NULL,
                    'id_enderecoentrega' => $result->getIdEnderecoentrega() ? $result->getIdEnderecoentrega()->getId_endereco() : NULL,
                    'id_ocorrencia' => $result->getIdOcorrencia() ? $result->getIdOcorrencia()->getId_ocorrencia() : null ,
                    'id_cupom' => $result->getIdCupom() ? $result->getIdCupom()->getId_cupom() : NULL,
                    'st_cupom' => $result->getIdCupom() ? $result->getIdCupom()->getSt_codigocupom() : NULL,
                    'id_atendente' => $result->getIdAtendente() ? $result->getIdAtendente()->getId_usuario() : NULL,
                    'st_atendente' => $result->getIdAtendente() ? $result->getIdAtendente()->getSt_nomecompleto() : NULL,
                    'id_campanhapontualidade' => $result->getIdCampanhaPontualidade() ? $result->getIdCampanhaPontualidade()->getId_campanhacomercial() : NULL,
                    'nu_viascontrato' => $result->getNuViasContrato() ? $result->getNuViasContrato() : NULL
                );
            }
        }
        $this->view->form = new CadastroRapidoPessoaForm();
        //monta o json para a view
        $this->view->venda = json_encode($venda);
    }

    /**
     * Renderiza o javascript para REST
     */
    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render($this->_getParam('controller') . '/index.js');
    }

    public function salvarVendaPrrAction()
    {

        if ($this->getRequest()->isPost()) {

            $post = $this->getRequest()->getPost();

            $this->_negocio = new G2\Negocio\Venda();

            $result = $this->_negocio->salvarVendaPrr($post);

            $this->_helper->json($this->_negocio->toArrayEntity($result));
        }
    }

    public function verificarVendaOcorrenciaAction(){

        if ($this->getRequest()->isPost()) {

            $post = $this->getRequest()->getPost();

            $this->_negocio = new G2\Negocio\Venda();

            $result = $this->_negocio->verificarVendaOcorrencia($post);
            if ( $result ) {
                $this->_helper->json($this->_negocio->toArrayEntity($result));
            } else {
                $this->_helper->json(array());
            }

        }
    }

    public function salvarVendaOcorrenciaAction(){

        if ($this->getRequest()->isPost()) {

            $post = $this->getRequest()->getPost();

            $this->_negocio = new G2\Negocio\Venda();

            $result = $this->_negocio->salvarVendaOcorrencia($post);

            $this->_helper->json($this->_negocio->toArrayEntity($result));
        }
    }

    public function findVwResumoFinanceiroAction()
    {
        $params = $this->_getParam('params');
        $data = array();

        $this->_negocio = new G2\Negocio\Venda();
        $entity = new G2\Entity\VwResumoFinanceiro();
        $result = $this->_negocio->findVwResumoFinanceiro($params);

        foreach ($result as $key => $entity) {
            $data[$key] = $this->_negocio->toArrayEntity($entity);
            $data[$key]['dt_vencimento'] = $entity->getDt_vencimento()->format('d-m-Y');

            if ($data[$key]['dt_quitado']) {
                $data[$key]['dt_quitado'] = $entity->getDt_quitado()->format('d-m-Y');
            }

            $data[$key]['id'] = $entity->getId_lancamento();
        }

        $this->_helper->json($data);
    }

    public function salvarLancamentosPrrAction()
    {
        $this->_negocio = new G2\Negocio\Venda();

        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();
            $result = $this->_negocio->salvarLancamentoPrr($post['params']);
            return $this->_helper->json($result);
        }
    }

    public function cancelarLancamentoAction()
    {
        $this->_negocio = new G2\Negocio\Venda();
        $entitys = array();
        $params = $this->getRequest()->getPost();
        $result = $this->_negocio->cancelarLancamentos($params['params']);

        foreach ($result as $entity) {
            $entitys[] = $this->_negocio->toArrayEntity($entity);
        }

        return $this->_helper->json($entitys);
    }

    public function confirmarVendaPrrAction()
    {
        $this->_negocio = new \G2\Negocio\Venda();
        $post = $this->getRequest()->getPost();
        $result = $this->_negocio->confirmarVendaPrr($post['params']);
        $this->_helper->json($result);
    }

    public function abonarVendaPrrAction()
    {

        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();
            $this->_negocio = new \G2\Negocio\Venda();
            $result = $this->_negocio->abonarVendaPrr($post['params']);
            $entity = $this->_negocio->toArrayEntity($result);
            $this->_helper->json($entity);
        }
    }

    public function quitarBoletoAction()
    {

        $this->_negocio = new \G2\Negocio\Venda();
        $entity = $this->_negocio->quitarBoleto($this->getRequest()->getParam('params'));
        $result = $this->_negocio->toArrayEntity($entity);

        $this->_helper->json($result);
    }

    public function cancelarAssinaturaAction()
    {

    }

    public function cancelarAssinaturaJsAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/cancelar-assinatura-js.js');
    }

    public function retornaSpCampanhaVendaProdutoAction()
    {
        $negocio = new \G2\Negocio\Venda();
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $negocio->retornarSpCampanhaVendaProduto($params);
        $this->_helper->json($result);
    }

    /**
     * Verifica se ja existe o contrato armazendo para tal venda
     */
    public function verificaEspelhoAction()
    {
        try {
            $params = $this->limpaGetAllParams($this->getAllParams());
            $file = realpath(APPLICATION_PATH . '/../public/upload/minhapasta/') . '/' . $params['id_venda'] . '/ContratoNegociacao' . $params['id_venda'] . '.pdf';
            if (file_exists($file))
                $this->_helper->json(array('existe' => true));
            else
                $this->_helper->json(array('existe' => false));
        } catch (\Exception $e) {
            $this->_helper->json($e->getMessage());

        }
    }

    public function contabilizaViaContratoAction()
    {
        try {
            $params = $this->limpaGetAllParams($this->getAllParams());
            $result = $this->_negocio->contabilizaViaContrato($params['id_venda'], $params['nu_viascontrato']);
            $this->json($result);
        } catch (\Exception $e) {
            $this->_helper->json($e->getMessage());
        }
    }

    /**
     * Gera o espelho do contrato, grava no diretório da venda caso seja confirmação de venda. Se não só gera o contrato para visualização
     */
    public function gerarEspelhoAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        try {
            $params = $this->limpaGetAllParams($this->getAllParams());
            $path = realpath(APPLICATION_PATH . '/../public/upload/minhapasta/') . '/' . (int)$params['id_venda'] . '/';
            $to = new TextoSistemaTO();
            $to->setId_textosistema($params['id_textosistema']);
            $bo = new TextoSistemaBO();
//            $html = $bo->gerarHTML($to, array('id_contrato' => $params['id_contrato']))->getFirstMensagem()->getSt_texto();
            $mensageiro = $bo->gerarHTML($to, array('id_contrato' => $params['id_contrato']));//->getFirstMensagem()->getSt_texto();

            if (($mensageiro instanceof Ead1_Mensageiro) && $mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Exception($mensageiro->getText());
            }
            $html = $mensageiro->getFirstMensagem()->getSt_texto();

            $mpdf = new \Ead1_IMPdf();
            $mpdf->carregarHTML($html)->configuraPapel();


            if (isset($params['confirmacao']) && ($params['confirmacao'] == "true" || $params['confirmacao'] === true)) {
                if (is_dir($path)) {
                    $mpdf->gerarPdf('ContratoNegociacao' . $params['id_venda'] . '.pdf', $path, 'F');
                } else {

                    if (!mkdir($path, 0777)) {
                        throw new Exception("Erro ao criar diretório, reveja as permissões de pasta.");
                    }

                    if (!is_dir($path)) {
                        throw new Exception('Erro ao criar pasta para o contrato!');
                    }
                    $oldmask = umask(0);
                    umask($oldmask);

                    $nome_arquivo = 'ContratoNegociacao' . $params['id_venda'] . '.pdf';
                    $mpdf->gerarPdf($nome_arquivo, $path, 'F');

                    $minhaPastaNegocio = new \G2\Negocio\MinhaPasta();
                    $minhaPastaNegocio->salvarContratoMinhaPasta($nome_arquivo, $params['id_venda'], null, null);
                }
                $this->_helper->json($arrReturn = array('type' => 'success', 'title' => 'Contrato', 'text' => 'Espelho do contrato gerado com sucesso!'));
            } else {
                $mpdf->gerarPdf('ContratoNegociacao' . $params['id_venda'] . '.pdf');
            }
        } catch (Exception $e) {
            $this->getResponse()->setHttpResponseCode(400)
                ->appendBody(json_encode(array(
                    'title' => 'Erro!',
                    'text' => $e->getMessage(),
                    'type' => 'warning'
                )));
        }
    }

    public function salvarVendaAction()
    {
        //Pega todos os parametros e limpa eles tirando modules, action e controller
        $params = $this->limpaGetAllParams($this->getAllParams());

        $mensageiro = $this->_negocio->procedimentoSalvarVenda($params);

        if ($mensageiro->getTipo() == Ead1_IMensageiro::ERRO) {
            $arrReturn = array('type' => $mensageiro->getType(), 'title' => $mensageiro->getTitle(), 'text' => $mensageiro->getMensagem());
            $this->_helper->json($arrReturn);
        }

        $arrReturn = array();
        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $result = $mensageiro->getFirstMensagem();
            $venda = array();

            //Monta o array da venda
            if ($result instanceof \G2\Entity\Venda) {
                $venda = array(
                    'id_venda' => $result->getIdVenda(),
                    'dt_cadastro' => $result->getDtCadastro(),
                    'nu_descontoporcentagem' => $result->getNuDescontoporcentagem(),
                    'nu_descontovalor' => $result->getNuDescontovalor(),
                    'nu_juros' => $result->getNuJuros(),
                    'bl_ativo' => $result->getBlAtivo(),
                    'id_usuariocadastro' => $result->getIdUsuariocadastro(),
                    'nu_valorliquido' => $result->getNuValorliquido(),
                    'nu_valorbruto' => $result->getNuValorbruto(),
                    'id_entidade' => $result->getIdEntidade(),
                    'nu_parcelas' => $result->getNuParcelas(),
                    'bl_contrato' => $result->getBlContrato(),
                    'nu_diamensalidade' => $result->getNuDiamensalidade(),
                    'id_origemvenda' => $result->getIdOrigemvenda(),
                    'dt_agendamento' => $result->getDtAgendamento(),
                    'dt_confirmacao' => $result->getDtConfirmacao(),
                    'st_observacao' => $result->getStObservacao(),
                    'nu_valoratualizado' => $result->getNuValoratualizado(),
                    'recorrete_orderid' => $result->getRecorreteOrderid(),
                    'id_formapagamento' => $result->getIdFormapagamento() ? $result->getIdFormapagamento()->getId_formapagamento() : NULL,
                    'id_campanhacomercial' => $result->getIdCampanhacomercial() ? $result->getIdCampanhacomercial()->getId_campanhacomercial() : NULL,
                    'id_evolucao' => $result->getIdEvolucao() ? $result->getIdEvolucao()->getId_evolucao() : NULL,
                    'id_situacao' => $result->getIdSituacao() ? $result->getIdSituacao()->getId_situacao() : NULL,
                    'id_usuario' => $result->getIdUsuario() ? $result->getIdUsuario()->getId_usuario() : NULL,
                    'id_tipocampanha' => $result->getIdTipocampanha() ? $result->getIdTipocampanha()->getId_tipocampanha() : NULL,
                    'id_prevenda' => $result->getIdPrevenda(),
                    'id_protocolo' => $result->getIdProtocolo() ? $result->getIdProtocolo()->getIdProtocolo() : NULL,
                    'id_enderecoentrega' => $result->getIdEnderecoentrega() ? $result->getIdEnderecoentrega()->getId_endereco() : NULL,
                    'id_ocorrencia' => $result->getId_Ocorrencia() ? $result->getId_Ocorrencia()->getId_ocorrencia() : null ,
                    'id_cupom' => $result->getIdCupom() ? $result->getIdCupom()->getId_cupom() : NULL,
                    'id_atendente' => $result->getIdAtendente() ? $result->getIdAtendente()->getId_usuario() : NULL,
                );
            }
            $arrReturn['venda'] = $venda;

            //Busca dados de VwProdutoVenda
            $resultProdutoVenda = $this->_negocio->retornarVwProdutoVenda(array('id_venda' => $venda['id_venda']));
            if ($resultProdutoVenda) {
                foreach ($resultProdutoVenda as $key => $row) {
                    $resultProdutoVenda[$key] = $this->_negocio->toArrayEntity($row);
                }
            }
            $arrReturn['produto_venda'] = $resultProdutoVenda;


            //busca dados de VwVendaLancamento
            $lancamentos = $this->_negocio->retornaVwVendaLancamento(array('id_venda' => $venda['id_venda']));
            if ($lancamentos) {
                foreach ($lancamentos as $key => $row) {
                    $dt_vencimento = $row->getDtvencimento();
                    if ($dt_vencimento instanceof DateTime) {
                        $dt_vencimento = $dt_vencimento->format("d/m/Y");
                    }

                    $lancamentos[$key] = $this->_negocio->toArrayEntity($row);
                    $lancamentos[$key]['dt_vencimento'] = $dt_vencimento;
                }
            }
            $arrReturn['lancamentos'] = $lancamentos;


            $evolucao = $this->_negocio->retornarVwVendaUsuarioByVenda($venda['id_venda']);
            if ($evolucao) {
                $evolucao = array(
                    'id_evolucao' => $evolucao->getId_evolucao(),
                    'st_evolucao' => $evolucao->getSt_evolucao(),
                    'id_situacao' => $evolucao->getId_situacao(),
                    'st_situacao' => $evolucao->getSt_situacao()
                );
            }
            $arrReturn['evolucao'] = $evolucao;

            //busca dados da carta de crédito
            $resultCarta = $this->_negocio->retornarVendaCartaCreditoByVenda($venda['id_venda']);
            $arrReturn['carta_credito'] = $resultCarta;

            $arrReturn = array('type' => $mensageiro->getType(), 'title' => $mensageiro->getTitle(), 'text' => $arrReturn);

            if (array_key_exists('iniciandoVenda', $params) && $params['iniciandoVenda'] == 'true') {
                $params['id_venda'] = $venda['id_venda'];
                $this->_negocio->registraTramite($params);
            }


        } else {
            $arrReturn = array('type' => $mensageiro->getType(), 'title' => $mensageiro->getTitle(), 'text' => $mensageiro->getMensagem());
        }
        $this->_helper->json($arrReturn);
    }

    public function retornarFuncionarioNucleoAction()
    {
        //Instancia a BO e a TO
        $bo = new NucleotmBO();
        $to = new NucleoFuncionariotmTO();
        //seta os valores
        $to->setId_funcao(1);
        $to->setBl_ativo(true);

        //busca os dados
        $result = $bo->retornarFuncionarioNucleo($to);

        $arrResult = array();
        //verifica se veio sucesso
        if ($result->getTipo() == Ead1_IMensageiro::SUCESSO) {
            foreach ($result->getMensagem() as $i => $row) {
                $arrResult[$i] = $row->toArray();
                $dt_cadastro = $row->getDt_cadastro();
                $arrResult[$i]['dt_cadastro'] = $dt_cadastro->toString('d/M/Y');
            }
        }

        //retorna
        $this->_helper->json($arrResult);
    }

    public function salvarConfirmacaoPagamentoAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        if (array_key_exists('lancamentos', $params)) {
            $result = $this->_negocio->salvarConfirmacaoPagamentoByArrayLancamentos($params['lancamentos']);
            $arrReturn = array();
            if ($result) {
                $arrReturn['venda'] = $this->_negocio->toArrayEntity($result['venda']);
                $mensageiro = new Ead1_Mensageiro($arrReturn, Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new Ead1_Mensageiro('Nenhum lançamento retornado.', Ead1_IMensageiro::AVISO);
            }

            $this->_helper->json($mensageiro->toArrayAll());
        }
    }

    public function retornarContratoRegraVendaAction()
    {
        $idVenda = $this->getParam('id_venda'); //Pegao id_venda
        $arrReturn = array();
        if ($idVenda) {//verifica se o id_venda foi passado

            $contratoNegocio = new \G2\Negocio\Contrato(); //cria a instancia da negocio
            $result = $contratoNegocio->retornaContratoVenda($idVenda); //busca os dados do contrato
            if ($result && $result->getId_contratoregra()) {//verifica se tem resultaldo e se tem contrato regra
                $contratoRegra = $result->getId_contratoregra(); //atribui o objeto de contrato regra a variavel

                //monta o array para retorno
                $arrReturn = array(
                    "id_contratoregra" => $contratoRegra->getId_contratoregra(),
                    "st_contratoregra" => $contratoRegra->getSt_contratoregra(),
                    "nu_contratoduracao" => $contratoRegra->getNu_contratoduracao(),
                    "nu_mesesmulta" => $contratoRegra->getNu_mesesmulta(),
                    "bl_proporcaomes" => $contratoRegra->getBl_proporcaomes(),
                    "nu_contratomultavalor" => $contratoRegra->getNu_contratomultavalor(),
                    "bl_renovarcontrato" => $contratoRegra->getBl_renovarcontrato(),
                    "nu_mesesmensalidade" => $contratoRegra->getNu_mesesmensalidade(),
                    "bl_ativo" => $contratoRegra->getBl_ativo(),
                    "dt_cadastro" => $contratoRegra->getDt_cadastro(),
                    "nu_tempoextensao" => $contratoRegra->getNu_tempoextensao(),
                    "id_contratomodelo" => $contratoRegra->getId_contratomodelo(),
                    "id_extensaomodelo" => $contratoRegra->getId_extensaomodelo(),
                    "id_projetocontratoduracaotipo" => $contratoRegra->getId_projetocontratoduracaotipo() ? $contratoRegra->getId_projetocontratoduracaotipo()->getId_projetocontratoduracaotipo() : NULL,
                    "id_entidade" => $contratoRegra->getId_entidade() ? $contratoRegra->getId_entidade()->getId_entidade() : NULL,
                    "id_usuariocadastro" => $contratoRegra->getId_usuariocadastro() ? $contratoRegra->getId_usuariocadastro()->getId_usuario() : NULL,
                );
            }
        }
        $this->_helper->json($arrReturn); //retorna o json
    }

    public function verificaVagasFreepassAction()
    {
        $body = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->_negocio->verificaMatriculasFreePassDisponiveis($body['id_turma']);
        if ($result['retorno'] == true) {
            $mensageiro = new Ead1_Mensageiro(array("totalFreePass" => $result['TotalFreePass'], "restanteFreePass" => $result['RestanteFreePass']), Ead1_IMensageiro::SUCESSO);
        } else {
            $mensageiro = new Ead1_Mensageiro(array("totalFreePass" => $result['TotalFreePass'], "restanteFreePass" => $result['RestanteFreePass']), Ead1_IMensageiro::AVISO);
        }
        $this->_helper->json($mensageiro->toArrayAll());
    }

    /**
     * Efetua o upload do contrato gerado na venda, salvo na pasta public/upload/minhapasta/id_venda
     */
    public function uploadContratoAction()
    {
        try {

            $venda = new \G2\Negocio\Venda();

            $this->_helper->viewRenderer->setNoRender(true);

            $id_venda = $this->getParam('id_venda');
            $id_matricula = $this->getParam('id_matricula');

            if ($id_venda && isset($_FILES['arquivo']['name'])) {
                $mensageiro = $venda->salvarArquivoContrato($id_venda, $_FILES['arquivo'], $id_matricula);
            } else {
                $mensageiro = new \Ead1_Mensageiro("Arquivo ou Venda não informados!", \Ead1_IMensageiro::AVISO);
            }

        } catch (\Exception $e) {
            $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        $this->_helper->json($mensageiro);
    }

    /**
     * @throws Exception
     * Salva a justificativa e contabiliza o numero de vias do contrato.
     */
    public function salvaJustificativaAction()
    {
        $post = $this->limpaGetAllParams($this->getAllParams());
        $retorno = $this->_negocio->salvaJustificativa($post);
        if ($retorno) {
            $result = $this->_negocio->contabilizaViaContrato($post['id_venda'], $retorno['nu_viacontrato'], $retorno['st_justificativa']);
            if ($result) {
                $retorno['nu_viascontrato'] = $result['nu_viascontrato'];
            }
        }
        $this->_helper->json($retorno);
    }

    /**
     * @throws Exception
     * Registra o tramite
     */
    public function registraTramiteAction()
    {
        $post = $this->limpaGetAllParams($this->getAllParams());
        $retorno = $this->_negocio->registraTramite($post);
        $this->_helper->json($retorno);
    }

    /**
     * Esse metodo foi criado para retornar uma venda já que o metodo de salvar a venda estava retornando o valor
     * anterior ao commit.
     */
    public function retornaVendaAction()
    {
        try {
            $post = $this->limpaGetAllParams($this->getAllParams());

            if (empty($post['id_venda'])) {
                throw new Exception("id_venda não informado.");
            }


            /** @var \G2\Entity\Venda $result */
            $result = $this->_negocio->findVenda($post['id_venda']);
            if ($result) {
                $venda = array(
                    'id_venda' => $result->getId_venda(),
                    'dt_cadastro' => $result->getDt_cadastro(),
                    'nu_descontoporcentagem' => $result->getNu_descontoporcentagem(),
                    'nu_descontovalor' => $result->getNu_descontovalor(),
                    'nu_juros' => $result->getNu_Juros(),
                    'bl_ativo' => $result->getBl_ativo(),
                    'id_usuariocadastro' => $result->getId_Usuariocadastro(),
                    'nu_valorliquido' => $result->getNu_Valorliquido(),
                    'nu_valorbruto' => $result->getNu_Valorbruto(),
                    'id_entidade' => $result->getId_Entidade(),
                    'nu_parcelas' => $result->getNu_Parcelas(),
                    'bl_contrato' => $result->getBl_Contrato(),
                    'nu_diamensalidade' => $result->getNu_Diamensalidade(),
                    'id_origemvenda' => $result->getId_Origemvenda(),
                    'dt_agendamento' => $result->getDt_Agendamento(),
                    'dt_confirmacao' => $result->getDt_Confirmacao(),
                    'st_observacao' => $result->getSt_Observacao(),
                    'nu_valoratualizado' => $result->getNu_Valoratualizado(),
                    'recorrete_orderid' => $result->getRecorreteOrderid(),
                    'id_formapagamento' => $result->getId_Formapagamento() ? $result->getId_Formapagamento()->getId_formapagamento() : NULL,
                    'id_campanhacomercial' => $result->getId_Campanhacomercial() ? $result->getId_Campanhacomercial()->getId_campanhacomercial() : NULL,
                    'id_evolucao' => $result->getId_Evolucao() ? $result->getId_Evolucao()->getId_evolucao() : NULL,
                    'id_situacao' => $result->getId_Situacao() ? $result->getId_Situacao()->getId_situacao() : NULL,
                    'id_usuario' => $result->getId_Usuario() ? $result->getId_Usuario()->getId_usuario() : NULL,
                    'id_tipocampanha' => $result->getId_Tipocampanha() ? $result->getId_Tipocampanha()->getId_tipocampanha() : NULL,
                    'id_prevenda' => $result->getId_Prevenda(),
                    'id_protocolo' => $result->getId_Protocolo() ? $result->getId_Protocolo()->getid_protocolo() : NULL,
                    'id_enderecoentrega' => $result->getId_enderecoentrega() ? $result->getId_enderecoentrega()->getId_endereco() : NULL,
                    'id_ocorrencia' => $result->getId_Ocorrencia() ? $result->getId_Ocorrencia()->getId_ocorrencia() : null ,
                    'id_cupom' => $result->getId_Cupom() ? $result->getId_Cupom()->getId_cupom() : NULL,
                    'st_cupom' => $result->getId_Cupom() ? $result->getId_Cupom()->getSt_codigocupom() : NULL,
                    'id_atendente' => $result->getId_Atendente() ? $result->getId_Atendente()->getId_usuario() : NULL,
                    'st_atendente' => $result->getId_Atendente() ? $result->getId_Atendente()->getSt_nomecompleto() : NULL,
                    'id_campanhapontualidade' => $result->getId_CampanhaPontualidade() ? $result->getId_CampanhaPontualidade()->getId_campanhacomercial() : NULL,
                    'nu_viascontrato' => $result->getNu_ViasContrato() ? $result->getNu_ViasContrato() : NULL
                );
            }
            $this->_helper->json($venda);
        } catch (Exception $e) {
            $this->_helper->viewRenderer->setNoRender();
            $this->getResponse()->setHttpResponseCode(400)
                ->appendBody($e->getMessage());
        }
    }

    public function removeProdutoAction()
    {
        $body = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->_negocio->removeProduto($body['id_vendaproduto']);
        if ($result) {
            $mensageiro = new \Ead1_Mensageiro('Sucesso', \Ead1_IMensageiro::SUCESSO);
        }
        $this->_helper->json($mensageiro);
    }

    public function retornarHistoricoCreditoAction()
    {
        $id_cartacredito = $this->getRequest()->getParam('id_cartacredito');
        $cartacredito = new G2\Negocio\CartaCredito();
        $result = $cartacredito->retornaHistoricoCartaCredito(array("id_cartacredito" => $id_cartacredito), array("id_venda" => "ASC"));
        $retorno = array();
        foreach ($result as $dados) {
            $retorno[] = $cartacredito->toArrayEntity($dados);
        }
        $cont = sizeof($retorno);
        if ($cont >= 1) {
            for ($i = 0; $i < $cont; $i++) {
                if ($i == 0) {
                    $retorno[$i]['nu_valordisponivel'] = $retorno[$i]['nu_valororiginal'] - $retorno[$i]['nu_valorutilizado'];
                } else {
                    $retorno[$i]['nu_valordisponivel'] = $retorno[$i - 1]['nu_valordisponivel'] - $retorno[$i]['nu_valorutilizado'];
                }
                if ($retorno[$i]['nu_valordisponivel'] < 0) {
                    $retorno[$i]['nu_valordisponivel'] = 0;
                }
            }

            return $this->_helper->json($retorno);
        } else {
            return $this->_helper->json($retorno);
        }
    }

    public function salvarLancamentoAction()
    {

        try {

            $this->_negocio = new G2\Negocio\Venda();

            if ($this->getRequest()->isPost()) {
                $post = $this->getRequest()->getPost();

                if (empty($post['params'])) {
                    throw new Exception('Parâmetros no formato inválido');
                }

                $result = $this->_negocio->salvarLancamento($post['params']);
                return $this->_helper->json(new \Ead1_Mensageiro($result, \Ead1_IMensageiro::SUCESSO));
            }
        } catch (Exception $error) {
            $this->getResponse()->setHttpResponseCode(500);
            return $this->_helper->json(new \Ead1_Mensageiro($error->getMessage(), \Ead1_IMensageiro::ERRO));
        }

    }

}

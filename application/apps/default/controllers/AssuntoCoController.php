<?php

/**
 * Classe para cadastro de assuntos da Central de Atenção
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @size 20/10/2014
 */
class Default_AssuntoCoController extends Ead1_ControllerRestrita
{

    /** @var  \G2\Negocio\AssuntoCo */
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\AssuntoCo();
    }

    /**
     * Retorna assuntos e sub-assuntos para cadastro na CA
     */
    public function retornarVwPesquisaAssuntocoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $params = $this->limpaGetAllParams($this->getAllParams());


        $result = $this->negocio->findByVwPesquisaAssuntoCo($params);
        $arrReturn = array();
        if ($result) {
            foreach ($result as $row) {
                $arrReturn[] = array(
                    'id' => $row['id_assuntoco'],
                    'id_assuntoco' => $row['id_assuntoco'],
                    'id_assuntocopai' => $row['id_assuntocopai'],
                    'st_assuntoco' => ($row['id_assuntocopai'] == NULL ? ($row['st_assuntoco']) : ''),
                    'st_subassuntoco' => ($row['id_assuntocopai'] != NULL ? ($row['st_assuntoco']) : ''),
                    'st_tipoocorrencia' => $row['st_tipoocorrencia'],
                    'st_situacao' => $row['st_situacao']
                );
            }
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    /**
     * Retorna tipo de ocorrência (combos tipo interessado)
     */
    public function tipoOcorrenciaAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $result = $this->negocio->findTipoOcorrencia();
        $this->_helper->json($result->getMensagem());
        //$this->getResponse()->appendBody(json_encode($result));

    }

    public function recarregaArvoreAction()
    {
        $result = array();
        $retorno = $this->negocio->findByAssuntoEntidadeCo(array('id_assuntoco' => $this->getParam('id_assuntoco')));
        if (!empty($retorno) && is_array($retorno)) {
            foreach ($retorno as $key => $entidade) {
                $result[$key] = $entidade->getId_entidade()->getId_entidade();
            }
        }
        $this->_helper->json($result);
    }

    /**
     * Retorna textos sistemas para combos
     */
    public function textoSistemaAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $result = $this->negocio->findTextoSistema($this->getAllParams());
        $this->_helper->json($result->getMensagem());

    }

    public function salvarAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        try {
            $result = $this->negocio->salvar($this->getAllParams());

            if ($result->getTipo() != Ead1_IMensageiro::SUCESSO) {
                $this->getResponse()->setHttpResponseCode(400);
                $this->_helper->json($result);
            }

            $this->getResponse()->setHttpResponseCode(201);
            $this->_helper->json($result);
        } catch (Exception $e) {
            $this->getResponse()->setHttpResponseCode(500)
                ->setBody($e->getMessage());
        }
    }

    public function indexAction()
    {
        echo $this->view->headScript()
            ->appendScript($this->view->render('/assunto-co/index.js'));
        echo $this->view->headScript()
            ->appendScript('/js/jquery.form.js');
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('assunto-co/index.js');
    }

    public function getAction()
    {
        $this->getResponse()
            ->appendBody("From getAction() returning the requested article");
    }

    public function postAction()
    {
        $this->getResponse()
            ->appendBody("From postAction() creating the requested article");
    }

    public function putAction()
    {
        $this->getResponse()
            ->appendBody("From putAction() updating the requested article");
    }

}

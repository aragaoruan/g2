<?php
class Default_DecodeController extends Ead1_Controller{
	
	public function indexAction(){}
	
	public function gerarAction(){
		try{
			$encode = "UTF-8";
			$params = $this->_request->getParams();
			if(empty($params['tabela'])){
				THROW new Zend_Validate_Exception('Informe o Nome da Tabela.');
			}
			$tabela = strtolower($params['tabela']);
			$desc = Zend_Db_Table_Abstract::getDefaultAdapter()->describeTable($tabela);
			if(empty($desc)){
				THROW new Zend_Validate_Exception('Tabela não encontrada.');
			}
			$primary = array();
			foreach ($desc as $col){
				if($col["PRIMARY"]){
					$primary[] = $col["COLUMN_NAME"];
				}
			}
			if(empty($primary)){
				$keys = array_keys($desc);
				$primary[] = $keys[0];
			}
			$dao = new PessoaDAO();
			$dados = $dao->db()->query("SELECT * FROM $tabela");
			if(empty($dados)){
				THROW new Zend_Validate_Exception('Nenhum Registro Encontrado!');
			}
			foreach ($dados as $dado){
				$where = '';
				foreach ($primary as $key){
					if(!array_key_exists($key, $dado)){
						THROW new Zend_Validate_Exception('Chave Primaria Não Encontrada!');
					}
					$arrWhere = array();
					$arrWhere[] = "$key = ".$dado[$key];
				}
				$where = implode(' AND ', $arrWhere);
				if(!$where){
					THROW new Zend_Validate_Exception('Erro ao Montar Where!');
				}
				$arrCamposUpdate = array();
				foreach ($dado as $campo => $valor){
					$explode = explode('_', $campo);
					$init = $explode[0];
					$decode = '';
					switch($init){
						case 'st':
							if(mb_detect_encoding($valor) != $encode){
								$decode = utf8_decode($valor);
								$arrCamposUpdate[] = "set $campo = '$decode'";
							}
							break;
					}
				}
				if(!empty($arrCamposUpdate)){
					$update = implode(" ,", $arrCamposUpdate);
					$dao->db()->query("UPDATE $tabela $update WHERE $where");
				}
			}
			$this->view->response = 'Dados Decodificados com Sucesso!';
		}catch (Zend_Validate_Exception $e){
			$this->view->response = $e->getMessage();
		}catch (Zend_Exception $e){
			$this->view->response = 'Erro ao Decodificar! Erro: '.$e->getMessage();
		}
	}
	
}
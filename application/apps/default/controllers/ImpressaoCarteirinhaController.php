<?php

/**
 * Classe de controller MVC para controlar a Integração
 *
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 *
 * @package default
 * @subpackage controller
 */
class Default_ImpressaoCarteirinhaController extends Ead1_ControllerRestrita
{

    /**
     * @var \G2\Negocio\VwEcommerce
     */
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Matricula();
    }

    public function indexAction()
    {

    }

    public function gerarXlsAction()
    {
        $params = json_decode($this->getParam('usuarios'));
        $this->view->result = $params;
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
    }
}
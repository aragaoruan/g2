<?php

/**
 * Controller to Pessoa
 *
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2013-12-11
 * @package application
 * @subpackage controller
 */

use G2\Negocio\Pessoa;

class Default_PessoaController extends Ead1_Controller
{

    /**
     * @var Pessoa $_negocio
     */
    private $_negocio;

    /**
     * @var \G2\Negocio\Entidade
     */
    private $_negocioEntidade;

    /**
     * @var \G2\Negocio\ContaEntidade
     */
    private $_negocioContaEntidade;

    public function init()
    {
        parent::init();
        $this->_negocio = new Pessoa();
        $this->_negocioEntidade = new \G2\Negocio\Entidade();
        $this->_negocioContaEntidade = new \G2\Negocio\ContaEntidade();
    }

    /**
     * Action to form
     */
    public function cadastroRapidoAction()
    {
        $id = $this->getParam('id');
        $form = new CadastroRapidoPessoaForm();
        $this->view->dados = NULL;
        if ($id) {
            $result = $this->_negocio->getDadosCadastroRapidoUsuario($id);

//            if (isset($result['sg_uf'])) {
//                $form->populateIdMunicipio($result['sg_uf']);
//            }

            $form->populate($result);

            if (!empty($result['st_urlavatar'])
                && file_exists(realpath(APPLICATION_PATH . "/../public" . $result['st_urlavatar']))
            ) {
                $result['st_urlavatar'] = $result['st_urlavatar'] . '?_=' . uniqid();
            } else {
                $result['st_urlavatar'] = '/upload/usuario/usuario_foto_generica.jpg';
            }

            $this->view->st_urlavatar = $result['st_urlavatar'];
            $this->view->dados = $result;
        }

        $this->view->renderBackbone = true;
        $this->view->form = $form;
    }

    /**
     * Action to js backbone
     */
    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        echo $this->view->render($this->_getParam('controller') . '/cadastro-rapido.js');
    }

    /**
     * Action para retornar endereço principal de pessoa
     *
     * @return json
     */
    public function getEnderecoPrincipalPessoaAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $id_usuario = $this->getParam('id_usuario');
        $id_entidade = $this->getParam('id_entidade');
        $result = $this->_negocio->getEnderecoPessoa(array(
            'id_usuario' => $id_usuario,
            'id_entidade' => $id_entidade
        ));
        $this->getResponse()->appendBody(json_encode($result));
    }

    /**
     * Action para pesquisar endereço por cep
     *
     * @return json
     */
    public function getEnderecoByCepAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $st_cep = $this->getParam('st_cep');
        $primeiro = $this->getParam('primeiro');

        $result = $this->_negocio->getEnderecoBYCep($st_cep, $primeiro);

        $this->getResponse()->appendBody(json_encode($result));
    }

    /**
     * Action para retornar municipio por uf especifico
     *
     * @return json
     */
    public function getMunicipioByUfAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $sg_uf = $this->getParam('sg_uf');
        $return = $this->_negocio->getMunicipioByUf($sg_uf);
        $arrReturn = array();
        if ($return) {
            $arrReturn = $return;
        } else {
            $arrReturn = array(
                'id_municipio' => null,
                'nu_codigoibge' => null,
                'sg_uf' => null,
                'st_nomemunicipio' => null
            );
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    /**
     * Action para pesquisar usuário
     *
     * @return json
     */
    public function verificaUsuarioAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->getAllParams();
        unset($params['module'], $params['controller'], $params['action']);
        $result = $this->_negocio->verificaUsuario($params);
        $arrReturn = array();
        if ($result->getType() == 'success') {
            foreach ($result->getMensagem() as $row) {
                if (isset($row['dt_nascimento'])) {
                    if ($row['dt_nascimento'] instanceof Zend_Date) {
                        $row['dt_nascimento'] = $row['dt_nascimento']->toString('d/MM/Y');
                    } elseif ($row['dt_nascimento'] instanceof DateTime) {
                        $row['dt_nascimento'] = $row['dt_nascimento']->format('d/m/Y');
                    }
                }
                $arrReturn[] = $row;
            }
            $this->getResponse()->setHttpResponseCode(200)->appendBody(json_encode($arrReturn));
        } else {
            $arrReturn = array(
                'type' => $result->getType(),
                'title' => $result->getTitle(),
                'text' => $result->getText()
            );
            $this->getResponse()->setHttpResponseCode(400)->appendBody(json_encode($arrReturn));
        }
    }

    /** Retorna os dados da descrição do coordenador.
     * @return json
     */

    public function getDescricaoCoordenadorAction()
    {
        $negocio = new \G2\Negocio\Curso();
        $data = $this->getRequest()->getParams();
        $result = $negocio->getDetalhesCoordenador($data['id_usuario'], $data['id_entidade']);
        $this->_helper->json($result);
    }

    /** Action para salvar a descrição do coordenador do curso
     * na tb_pessoa.
     * @return json
     */
    public function salvaDescricaoCoordenadorAction()
    {

        $negocio = new \G2\Negocio\Curso();
        $data = $this->getRequest()->getParams();
        $arrReturn = array();

        if ($this->getRequest()->isPost()) {
            $result = $negocio->salvarDescricaoCoordenador(
                $data['id'],
                $data['id_entidade'],
                $data['st_descricaocoordenador']
            );

            $arrReturn = array(
                'type' => $result->getType(),
                'text' => $result->getText(),
                'title' => $result->getTitle()
            );

            /* Evita exibir todos os detalhes da exceção para o usuário.
            * A mensagem de erro é muito grande e quebra completamente
            * o layout do pnotify. */

            if ($result->getType() === \Ead1_IMensageiro::TYPE_ERRO) {
                $arrReturn['text'] = 'Não foi possível salvar a descrição do Coordenador.';
            }
        }
        $this->_helper->json($arrReturn);
    }


    /**
     * Action para rr formulário de cadastro resumido
     *
     * @return json
     */
    public function salvaCadastroResumidoAction()
    {
        $data = $this->getRequest()->getParams(); //get data
        $arrReturn = array();
        $fileImg = null;

        if ($this->getRequest()->isPost()) { //if post
            if (isset($_FILES['st_urlavatar']['tmp_name'])) {
                $fileImg = $_FILES['st_urlavatar'];
            }
            $form = new CadastroRapidoPessoaForm(); //instance of form
            $form->getElement('id_municipio')->setRegisterInArrayValidator(false);
            $form->getElement('id_municipio_corresp')->setRegisterInArrayValidator(false);
            $form->getElement('id_municipionascimento')->setRegisterInArrayValidator(false);

            $data['id_registropessoa'] = (!isset($data['id_registropessoa']) ? 4 : $data['id_registropessoa']); //fixa o valor do id_registropessoa

            if ($form->isValid($data)) { //valid form
                $data['id_estadocivil'] = (!isset($data['id_estadocivil']) ? 1 : $data['id_estadocivil']); //fix data id_estadocivil
                $data['id_pais'] = (!isset($data['id_pais']) ? 22 : $data['id_pais']); //fix data id_pais
                $result = $this->_negocio->salvarPessoaCadastroResumido($data); //send to negocio and persist

                //Get Result and set array to return
                if ($result->getType() == 'success') { //if success
                    //set array return success
                    $arrReturn = array(
                        'type' => $result->getType(),
                        'title' => $result->getTitle(),
                        'text' => $result->getText(),
                        'data' => array(),
                        'enviaEmail' => isset($result->enviaEmail) ? $result->enviaEmail : false
                    );

                    //busca os dados do usuário
                    $arrReturn['data'] = $this->_negocio
                        ->getDadosCadastroRapidoUsuario($result->getFirstMensagem()->getId_usuario());

                    //Faz upload da imagem tirada pela webcam
                    if (!empty($data['str_avatar'])) {
                        $imagem = $this->_negocio
                            ->uploadAvatar(
                                $data['str_avatar'],
                                $result->getFirstMensagem()->getId_usuario(),
                                $arrReturn['data']['st_urlavatar']
                            );
                        if ($imagem['type'] == 'success') {
                            $this->_negocio->salvaAvatarPessoa(array(
                                'id_usuario' => $result->getFirstMensagem()->getId_usuario(),
                                'st_urlavatar' => "/upload/usuario/" . $imagem['file']
                            ));
                            $arrReturn['data']['st_urlavatar'] = "/upload/usuario/" . $imagem['file'];
                        }
                    }
                    //Faz upload da imagem direto do computador
                    if ($fileImg) {
                        $imagem = $this->_negocio
                            ->uploadAvatar(
                                $fileImg,
                                $result->getFirstMensagem()->getId_usuario(),
                                $arrReturn['data']['st_urlavatar']
                            );
                        if ($imagem['type'] == 'success') {
                            $this->_negocio->salvaAvatarPessoa(array(
                                'id_usuario' => $result->getFirstMensagem()->getId_usuario(),
                                'st_urlavatar' => "/upload/usuario/" . $imagem['file']
                            ));
                            $arrReturn['data']['st_urlavatar'] = "/upload/usuario/" . $imagem['file'];
                        }
                    }

                    //verifica se a imagem esta no diretorio
                    if (!empty($arrReturn['data']['st_urlavatar'])
                        && file_exists(
                            realpath(APPLICATION_PATH . "/../public" . $arrReturn['data']['st_urlavatar'])
                        )
                    ) {
                        $arrReturn['data']['st_urlavatar'] = $arrReturn['data']['st_urlavatar'] . '?_=' . uniqid();
                    } else {
                        $arrReturn['data']['st_urlavatar'] = '/upload/usuario/usuario_foto_generica.jpg';
                    }

                } else {
                    //set array return fail
                    $arrReturn = array(
                        'type' => $result->getType(),
                        'title' => $result->getTitle(),
                        'text' => $result->getText(),
                    );
                }
            } else { //invalid form
                $arrErros = array();
                $formMens = $form->getMessages(); //get message erro

                if ($formMens) { //if message
                    $i = 0;
                    foreach ($formMens as $erros) {
                        if ($erros) {
                            foreach ($erros as $mens) {
                                $arrErros[$i] = $mens;
                            }
                        }
                        $i++;
                    }
                }
                $arrReturn = array(
                    'type' => 'warning',
                    'title' => 'Atenção!',
                    'text' => implode('<br>', $arrErros),
                );
            }
        }
        $this->_helper->json($arrReturn); //return json
    }


    /**
     * Action para rr formulário de cadastro resumido
     *
     * @return json
     */
    public function salvaCadastroResumidoDadosBancariosAction()
    {
        $data = $this->getRequest()->getParams(); //get data
        $arrReturn = array();
        $fileImg = null;

        if ($this->getRequest()->isPost()) { //if post

            $form = new CadastroRapidoPessoaForm(); //instance of form

            $data['id_registropessoa'] = (!isset($data['id_registropessoa']) ? 4 : $data['id_registropessoa']); //fixa o valor do id_registropessoa

            $result = $this->_negocio->salvarPessoaCadastroResumidoDadosBancarios($data); //send to negocio and persist
            //Get Result and set array to return
            if ($result->getType() == 'success') { //if success
                //set array return success
                $arrReturn = array(
                    'type' => $result->getType(),
                    'title' => $result->getTitle(),
                    'text' => $result->getText(),
                    'data' => array(),
                    'enviaEmail' => isset($result->enviaEmail) ? $result->enviaEmail : false
                );

            } else {
                //set array return fail
                $arrReturn = array(
                    'type' => $result->getType(),
                    'title' => $result->getTitle(),
                    'text' => $result->getText(),
                );
            }
        }
        $this->_helper->json($arrReturn); //return json
    }

    public function retornarPessoaAction()
    {
        //Pega todos os parametros e tira os defaults do zend
        $params = $this->limpaGetAllParams($this->getAllParams());
        //pesquisa a pessoa
        $result = $this->_negocio->findOneBy('\G2\Entity\Pessoa', $params); //recupera dados de pessoa
        $arrReturn = array();
        if ($result) {

            $responsavelLegal = $this->_negocio->findOneBy('\G2\Entity\ResponsavelLegal', array(
                'id_usuario' => $params['id_usuario']
            ));

            //monta o array para retorno
            $arrReturn = array(
                'id_usuario' => $result->getId_usuario(),
                'st_sexo' => $result->getSt_sexo(),
                'st_nomeexibicao' => $result->getSt_nomeexibicao(),
                'dt_nascimento' => $result->getDt_nascimento(),
                'st_nomepai' => $result->getSt_nomepai(),
                'st_nomemae' => $result->getSt_nomemae(),
                'sg_uf' => $result->getSg_uf(),
                'dt_cadastro' => $result->getDt_cadastro(),
                'id_usuariocadastro' => $result->getId_usuariocadastro(),
                'st_passaporte' => $result->getSt_passaporte(),
                'bl_ativo' => $result->getBl_ativo(),
                'st_urlavatar' => $result->getSt_urlavatar(),
                'st_cidade' => $result->getSt_cidade(),
                'id_entidade' => $result->getId_entidade() ? $result->getId_entidade()->getId_entidade() : NULL,
                'id_pais' => $result->getId_pais() ? $result->getId_pais()->getId_pais() : NULL,
                'id_municipio' => $result->getId_municipio() ? $result->getId_municipio()->getId_municipio() : NULL,
                'id_situacao' => $result->getId_situacao() ? $result->getId_situacao()->getId_situacao() : NULL,
                'id_estadocivil' => $result->getId_estadocivil()
                    ? $result->getId_estadocivil()->getId_estadocivil() : NULL,
                'st_identificacao' => $result->getSt_identificacao(),
                'st_cpfResponsavelLegal' => $responsavelLegal ? $responsavelLegal->getSt_cpf() : null,
                'st_nomeCompletoResponsavelLegal' => $responsavelLegal ? $responsavelLegal->getSt_nomecompleto() : null,
                'nu_dddResponsavelLegal' => $responsavelLegal ? $responsavelLegal->getNu_ddd() : null,
                'nu_telefoneResponsavelLegal' => $responsavelLegal ? $responsavelLegal->getNu_telefone() : null,
            );
        }
        $this->_helper->json($arrReturn);
    }

    public function getTelefonePessoaAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->_negocio->retornaTelefonePessoa($params);
        if ($result) {
            foreach ($result as $i => $row) {
                $result[$i]['id'] = $row['id_telefone'];
            }
        }
        $this->_helper->json($result);
    }

    public function getEmailPessoaAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->_negocio->retornaEmailPessoa($params);
        if ($result) {
            foreach ($result as $i => $row) {
                $result[$i]['id'] = $row['id_email'];
            }
        }
        $this->_helper->json($result);
    }

    public function salvarUsuarioImportacaoNucleoAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->_negocio->importarPessoaEntidade($params);
        if (!$result) {
            $return = array(
                'type' => 'success',
                'title' => 'Sucesso!',
                'text' => 'Importação realizada com sucesso!'
            );
        } else {
            $return = $result;
        }
        $this->_helper->json($return);
    }


    public function importarUsuarioAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json_Decoder::decode($body);
        $result = $this->_negocio->importarPessoaEntidade($data);
        if (!$result) {
            $return = array(
                'type' => 'success',
                'title' => 'Sucesso!',
                'text' => 'Importação realizada com sucesso!'
            );
        } else {
            $return = $result;
        }
        $this->getResponse()->appendBody(json_encode($return));
    }

    /**
     * Envia dados de acesso para o aluno
     */
    public function enviarDadosAcessoAction()
    {
        try {
            $this->_helper->viewRenderer->setNoRender();
            $id = $this->_request->getParam('id');

            if (!$id) {
                throw new Exception("Impossível enviar dados de acesso para usuário. Id do usuário não encontrado.");
            }

            $result = $this->_negocio->enviaDadosAcesso($id);

            if (($result instanceof Ead1_Mensageiro) && $result->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Exception($result->getText());
            }

            $response = array(
                'title' => $result->getTitle(),
                'text' => $result->getText(),
                'type' => $result->getType()
            );

            $this->getResponse()->setHttpResponseCode(200)
                ->appendBody(json_encode($response));

        } catch (Exception $e) {
            $this->getResponse()->setHttpResponseCode(400)
                ->appendBody(json_encode(array(
                    'title' => 'Erro!',
                    'text' => $e->getMessage(),
                    'type' => 'warning'
                )));
        }
    }

    public function getVwPerfilPessoaAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->_negocio->retornaVwPerfilPessoa($params);
        $this->_helper->json($result);
    }

    public function getVwPesquisarPessoaByIdAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        if ($this->getRequest()->getParam('id')) {
            $entity = $this->_negocio->retornaVwPesquisarPessoaById($this->getRequest()->getParam('id'));
            if ($entity) {
                $toArray = $this->_negocio->toArrayEntity($entity);
                $this->getResponse()->setHttpResponseCode(200)->appendBody(json_encode($toArray));
//                $this->_helper->json($toArray);
            } else {
                $this->getResponse()->setHttpResponseCode(400)->appendBody("Dados do usuário não encontrado.");
            }
        } else {
            throw new Zend_Exception(
                'Parametro ID não foi encontrado',
                Zend_Controller_Plugin_ErrorHandler::EXCEPTION_OTHER
            );
        }
    }

    public function pessoaJuridicaAction()
    {
        $id = $this->getParam('id');

        $form = new EntidadesDadosEnderecoForm();
        $formDadosConta = new EntidadesDadosFinanceiroForm();
        $formConfigs = new ConfiguracaoEntidadeForm();

        $this->view->id_entidade = NULL;
        if ($id) {
            $result = $this->_negocioEntidade->findEntidade($id);
            $this->view->id_entidade = $result->getId_entidade();
        }

        $this->view->formDadosConta = $formDadosConta;
        $this->view->form = $form;
        $this->view->formConfiguracoes = $formConfigs;
    }

    public function pessoaJuridicaJsAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        echo $this->view->render($this->_getParam('controller') . '/pessoa-juridica.js');
    }

    public function salvarPessoaJuridicaAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $compartilhaDados = new CompartilhaDadosBO();

        $params = $this->getAllParams();

        $form = new EntidadesDadosBasicosForm();
        $result = $this->_negocioEntidade
            ->salvarEntidadePessoaJuridica(
                $params['dados_entidade'],
                $params['dados_endereco'],
                $params['bl_relacionarentidade']
            );

        $dadosCompartilhaDados = new CompartilhaDadosTO();
        $dadosCompartilhaDados->setId_entidadeorigem($params['dados_entidade']['id_entidade']);
        $dadosCompartilhaDados->setId_entidadedestino($params['dados_entidade']['id_entidade']);
        $dadosCompartilhaDados->setId_tipodados('2');
        $insertCompartilhaDados = $compartilhaDados->cadastrarCompartilhaDados($dadosCompartilhaDados);

        $this->getResponse()->appendBody(json_encode($result));
    }

    public function processarContaEntidadePessoaJuridicaAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        $params = $this->getAllParams();

        $result = $this->_negocioContaEntidade->salvarContaEntidade($params['dados_conta']);

        if ($result->getTipo() == 1 && $params['dados_entidade_financeiro'] != null) {
            $resultEntidadeFinanceiro = $this->_negocioEntidade
                ->processarEntidadeFinanceiro($params['dados_entidade_financeiro']);
            $resultM = $result->getMensagem();
            $resultM[] = $resultEntidadeFinanceiro;
            $result->setMensagem($resultM);
        }

        $this->getResponse()->appendBody(json_encode($result));
    }

    public function processarCartaoPessoaJuridicaAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        $params = $this->getAllParams();

        $result = $this->_negocioEntidade->processarCartaoEntidade(
            $params['id_entidade'],
            $params['id_entidadefinanceiro'],
            $params['dados_cartao']
        );

        $this->getResponse()->appendBody(json_encode($result));
    }

    /**
     * Efetua o processamento dos dados do boleto.
     */
    public function processarBoletoPessoaJuridicaAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        $params = $this->limpaGetAllParams($this->getAllParams());

        //Validando no se existem os dados básicos do boleto necessários para o save.

        if (
            array_key_exists('st_codchave', $params) && $params['st_codchave'] &&
            array_key_exists('st_codsistema', $params) && $params['st_codsistema']
        ) {

            $result = $this->_negocioEntidade->processarBoleto($params);

            if ($result->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                $this->json($result->getMensagem());
            } else {
                $this->getResponse()->setHttpResponseCode(401);
                $this->json($result->toArray());
            }

        } else {
            $this->getResponse()->setHttpResponseCode(401);
            $this->json(array("Verifique os dados do boleto", \Ead1_IMensageiro::ERRO));
        }

    }

    public function processarConfiguracoesEntidadeAction()
    {
        $configuracaoNegocio = new \G2\Negocio\ConfiguracaoEntidade();

        $this->_helper->viewRenderer->setNoRender();
        $params = $this->getAllParams();

        $result = $configuracaoNegocio->processarConfiguracaoEntidade($params['dados_config_entidade']);

        if ($result->getTipo() == \Ead1_IMensageiro::SUCESSO) {
            $this->json($result->getMensagem());
        } else {
            $this->getResponse()->setHttpResponseCode(401);
            $this->json($result->toArray());
        }
    }

    public function processarConfiguracoesCssEntidadeAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $id_entidade = $this->getParam('id_entidade');

        $retorno = array();
        if (isset($_FILES['arquivo_css_portal'])) {
            $arquivo = $_FILES['arquivo_css_portal'];
            $resultPortalCss = $this->_negocioEntidade->salvarLayoutPortalCss($arquivo, $id_entidade);

            if ($resultPortalCss == false) {
                if (!empty($retorno))
                    $retorno['message'][] = 'Erro ao salvar css do portal';
                else
                    $retorno = array('retorno' => false, 'message' => array('Erro ao salvar css do portal'));
            }
        }
        if (empty($retorno))
            $retorno = array('retorno' => true);

        $this->getResponse()->appendBody(json_encode($retorno));
    }

    public function processarConfiguracoesLayoutEntidadeAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $id_entidade = $this->getParam('id_entidade');

        /*
         * Criando as pastas do portal recursivo e com chmod 777
         * */
        $uploadPathPortal = realpath(APPLICATION_PATH . "/../public/portaldoaluno/layout");
        if (!is_dir($uploadPathPortal . '/' . $id_entidade . '/imagens')) {
            $oldmask = umask(0);
            mkdir($uploadPathPortal . '/' . $id_entidade . '/imagens', 0777, true);
            umask($oldmask);
        }

        /*
         * Criando as pastas da loja recursivo e com chmod 777
         * */
        $uploadPathLojaFavicon = realpath(APPLICATION_PATH . "/../public/loja/layout/padrao/img/layout/");
        if (!is_dir($uploadPathLojaFavicon . '/favicons')) {

            $oldmask = umask(0);
            mkdir($uploadPathLojaFavicon . '/favicons', 0777, true);
            umask($oldmask);
        }

        $uploadPathPortalEntidadeImagem = realpath(
            APPLICATION_PATH . "/../public/portaldoaluno/layout/" . $id_entidade . "/imagens"
        );

        /*
         * Salvando os dados para a imagem que irá para o G2S e para o portal (topo)
         * */
        $arquivos = false;
        $retorno = array(
            'retorno' => true,
            'message' => array()
        );


        //salva a imagem do aplicativo
        if (isset($_FILES['imagem_aplicativo'])) {
            $arquivos = true;
            $resultAplicativo = $this->_negocioEntidade
                ->salvarImagemAplicativo($_FILES['imagem_aplicativo'], $id_entidade);

            if (!$resultAplicativo) {
                $retorno['message'][] = 'Erro ao salvar imagem em "Imagem Aplicativo"';
            }
        }

        if (isset($_FILES['imagem_g2s_portal'])) {
            $arquivos = true;

            $arquivo = $_FILES['imagem_g2s_portal'];
            $resultPortalG2S = $this->_negocioEntidade
                ->salvarLayoutG2SPortal($arquivo, $id_entidade, $uploadPathPortalEntidadeImagem);

            if (!$resultPortalG2S)
                $retorno['message'][] = 'Erro ao salvar imagem em "Logomarca da Certificadora"';
        }

        if (isset($_FILES['imagem_g2s_portal_entidade'])) {
            $arquivos = true;

            $arquivo = $_FILES['imagem_g2s_portal_entidade'];
            $resultPortalG2S = $this->_negocioEntidade
                ->salvarLayoutG2SPortalEntidade($arquivo, $id_entidade, $uploadPathPortalEntidadeImagem);

            if (!$resultPortalG2S)
                $retorno['message'][] = 'Erro ao salvar imagem em "Logomarca da Entidade"';
        }

        if (isset($_FILES['imagem_portal_loja'])) {
            $arquivos = true;

            $arquivo = $_FILES['imagem_portal_loja'];
            $resultPortalLoja = $this->_negocioEntidade
                ->salvarLayoutPortalLoja($arquivo, $id_entidade, $uploadPathPortalEntidadeImagem);

            if (!$resultPortalLoja)
                $retorno['message'][] = 'Erro ao salvar imagem em "Imagem da Logo Secundária"';
        }

        if (isset($_FILES['imagem_favicon_loja'])) {
            $arquivos = true;

            $arquivo = $_FILES['imagem_favicon_loja'];
            $resultPortalCss = $this->_negocioEntidade->salvarFaveIcon($arquivo, $id_entidade);

            if (!$resultPortalCss)
                $retorno['message'][] = 'Erro ao salvar imagem em "Imagem Favicon"';
        }

        if (!$arquivos)
            $retorno['message'][] = 'Nenhum arquivo selecionado, por favor, tente novamente.';

        if ($retorno['message'])
            $retorno['retorno'] = false;

        $this->_helper->json($retorno);
    }

    public function getConfiguracoesCssAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $id_entidade = $this->getParam('id_entidade');

        $retorno = $this->_negocioEntidade->verificarCssLayout($id_entidade);

        $this->getResponse()->appendBody(json_encode($retorno));
    }

    public function getConfiguracoesLayoutAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $id_entidade = $this->getParam('id_entidade');
        $uploadPathPortalEntidadeImagem = realpath(
            APPLICATION_PATH . "/../public/portaldoaluno/layout/" . $id_entidade . "/imagens"
        );

        $retorno = $this->_negocioEntidade->verificarLayoutEntidade($id_entidade, $uploadPathPortalEntidadeImagem);

        $this->getResponse()->appendBody(json_encode($retorno));
    }

    public function salvarCatracaAction()
    {
        $negocio = new \G2\Negocio\Catraca();
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->getAllParams();
        $retorno = $negocio->salvaCatraca($params);

        $this->getResponse()->appendBody(json_encode($retorno));
    }

    public function getCatracaByEntidadeAction()
    {
        $negocio = new \G2\Negocio\Catraca();
        $this->_helper->viewRenderer->setNoRender(true);
        $retorno = $negocio->getCatracaByEntidade();
        $situacao = new \G2\Entity\Situacao();
        $array = array();
        foreach ($retorno as $key => $ret) {
            $array[$key]['id'] = $ret->getId_catraca();
            $array[$key]['st_codigocatraca'] = $ret->getSt_codigocatraca();
            $sit = $situacao->getReference($ret->getId_situacao());
            $array[$key]['st_situacaocatraca'] = $sit->getSt_situacao();
            $array[$key]['id_situacao'] = $ret->getId_situacao();
        }

        $this->getResponse()->appendBody(json_encode($array));
    }

    public function deleteCatracaAction()
    {

    }

    public function salvarArrayIdentificacaoAction()
    {
        $params = $this->getParam('usuarios');
        $result = $this->_negocio->salvarArrayIdentificacao($params);
        $this->_helper->json($result->toArrayAll());
    }

    public function getImgAvatarUsuarioAction()
    {
        $this->view->img = $this->getParam('img');
    }

    public function salvarCancelamentoAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->_negocio->salvaCancelamento($params);
        $this->_helper->json($this->_negocio->toArrayEntity($result));
    }

    public function retornaCancelamentoAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->_negocio->retornaCancelamento($params);
        $this->_helper->json($this->_negocio->toArrayEntity($result));
    }

    public function processaDadosImportadosAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        try {
            $request = $this->getRequest();
            if ($request->isPost()) {
                $arrFinal = array();
                $_arrFinal = array();
                $data = $request->getParams();
                if (isset($data['dadosImportacao']) && !empty($data['dadosImportacao'])) {
                    foreach ($data['dadosImportacao'] as $d) {
                        $d['id_registropessoa'] = (!isset($d['id_registropessoa']) ? 4 : $d['id_registropessoa']);
                        $d['id_estadocivil'] = (!isset($d['id_estadocivil']) ? 1 : $d['id_estadocivil']);
                        $d['id_pais'] = 22;
                        $d['id_nacionalidade'] = 22;
                        $d['id_tipoendereco'] = 1;

                        $d['id_tipotelefone'] = 1;
                        $d['nu_ddi'] = $d['nu_ddi_residencial'];
                        $d['nu_ddd'] = $d['nu_ddd_residencial'];
                        $d['nu_telefone'] = $d['nu_telefone_residencial'];

                        $d['id_tipotelefone2'] = 3;
                        $d['nu_ddi2'] = $d['nu_ddi_celular'];
                        $d['nu_ddd2'] = $d['nu_ddd_celular'];
                        $d['nu_telefone2'] = $d['nu_telefone_celular'];

                        $existeCpf = $d['bl_existe'];
                        $arrFinal['nome'] = $d['st_nomecompleto'];

                        if ($existeCpf == 0) {
                            $result = $this->_negocio->salvarPessoaCadastroResumido($d);
                            if ($result->getTipo() == 1) {
                                $arrFinal['cadastro']['type'] = 1;

                                $obj = $result->getFirstMensagem();
                                $id_usuario = $obj->getId_usuario();
                                $arrFinal['cadastro']['message'] = $result->getText();
                            } else {
                                $arrFinal['cadastro']['type'] = 0;
                                $arrFinal['cadastro']['message'] = $result->getText();
                                $arrFinal['matricula']['type'] = 0;
                                $arrFinal['matricula']['message'] = 'Não foi possível matricular o aluno';
                            }
                            // checa se o usuário já tem cadastro e se está integrado a entidade em questão
                        } elseif ($existeCpf == 1
                            && !$this->_negocio->checaSeUsuarioEstaIncluidoNaEntidade($d['id_usuario'])
                        ) {
                            $integracao = $this->_negocio->incluirAlunoNaEntidade($d['id_usuario']);
                            if ($integracao->getTipo() == Ead1_IMensageiro::SUCESSO) {
                                $id_usuario = $d['id_usuario'];
                                $arrFinal['cadastro']['type'] = 0;
                                $arrFinal['cadastro']['message'] = 'Usuário já existente no sistema, porém foi integrado a entidade.';
                            } else {
                                $arrFinal['cadastro']['type'] = 0;
                                $arrFinal['cadastro']['message'] = 'Não foi possível integrar o usuario à entidade.';
                            }
                        } else {
                            $id_usuario = $d['id_usuario'];
                            $arrFinal['cadastro']['type'] = 0;
                            $arrFinal['cadastro']['message'] = 'Usuário já existe no sistema';
                        }

                        if (!isset($id_usuario)) {
                            $arrFinal['cadastro']['type'] = 0;
                            $arrFinal['cadastro']['message'] = "Id usuário não encontrado.";
                            $_arrFinal[] = $arrFinal;
                            continue;
                        }

                        $to = new MatriculaTO();
                        $to->setId_projetopedagogico($data['dadosProjeto']);
                        $to->setId_usuario($id_usuario);
                        $to->setId_turma($data['dadosTurma']);

                        // Instancia um objeto da classe Matricula e chama sua função para conferir se o usuário já está matriculado.
                        $matricula = new \G2\Negocio\Matricula();
                        $usuario_matriculado = $matricula
                            ->checaSeUsuarioEstaMatriculado($id_usuario, $data['dadosProjeto'], $data['dadosTurma']);

                        // Se o usuário não estiver matriculado, o sistema realiza a matrícula.
                        if (!$usuario_matriculado) {
                            $bo = new MatriculaBO();
                            $mensageiro = $bo->matricularDireto($to, true, false, false, true);
                        }

                        if (isset($mensageiro) && $mensageiro->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                            $arrFinal['matricula']['type'] = 1;
                            $arrFinal['matricula']['message'] = 'Matrícula feita com sucesso';
                        } else {
                            $arrFinal['matricula']['type'] = 0;
                            $arrFinal['matricula']['message'] = "Usuário já matriculado para este curso.";
                        }
                        $_arrFinal[] = $arrFinal;
                    }

                    $this->getResponse()->appendBody(Zend_Json::encode($_arrFinal))->setHttpResponseCode(200);
                    return true;

                }
            } else {
                $this->getResponse()->appendBody(
                    Zend_Json::encode(array(
                            'mensagem' => 'Erro dos dados na requisição',
                            'texto' => 'Dados incompletos na requisição',
                            'usuario' => '',
                            'tipo' => 'error',
                        )
                    ))->setHttpResponseCode(400);
                return false;
            }


        } catch (Exception $e) {
            throw new Exception($e);
        }

        return false;
    }

    public function visualizarCssAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $this->view->dados = $params;
    }

    public function gerarCssAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $resultado = $this->_negocio->gerarCss($params);
        if ($resultado) {
            $this->_helper->json(array('retorno' => true, 'message' => array('CSS gerado com sucesso.')));
        } else {
            $this->_helper->json(array(
                'retorno' => false,
                'message' => array('Erro ao gerar arquivo de css, contate o administrador!.')));
        }
    }

    /**
     * Action para pesquisar Conta do usuario
     *
     * @return json
     */
    public function getContaPessoaAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $id_usuario = $this->getParam('id_usuario');

        $result = $this->_negocio->getContaPessoa($id_usuario);

        $this->getResponse()->appendBody(json_encode($result));
    }

    public function getPessoaSecretariadoAction() {

        $st_nomeexibicao = $this->getParam('st_nomeexibicao');
        $id_entidade = $this->getParam('id_entidade');
        $result = $this->_negocio->retornaPessoaSecretariado($st_nomeexibicao, $id_entidade);
        $this->json($result);
    }

}

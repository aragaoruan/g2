<?php

/**
 * Classe de controller MVC para controlar cadastros de avaliação conjunto
 * @author Débora Castro
 * @since 21/10/2014
 * @package application
 * @subpackage controller
 */
class Default_AvaliacaoConjuntoController extends Ead1_ControllerRestrita
{

    /**
     * @var \G2\Negocio\AvaliacaoConjunto
     */
    private $_negocio;

    public function init ()
    {
        parent::init();
        $this->_negocio = new G2\Negocio\AvaliacaoConjunto();
    }

    public function indexAction ()
    {
        $id = $this->_getParam('id');

        if ($this->_getParam('id')!='') {

            $resultado = $this->_negocio->findAvaliacaoConjunto((int)$id);

            $array = array(
                'id_avaliacaoconjunto' => $resultado->getId_avaliacaoconjunto(),
                'st_avaliacaoconjunto' => $resultado->getSt_avaliacaoconjunto(),
                'id_tipoprova' => $resultado->getId_tipoprova(),
                'id_situacao' => $resultado->getId_situacao(),
                'id_tipocalculoavaliacao' => $resultado->getId_tipocalculoavaliacao()
            );
            $this->view->conjuntoavaliacao = json_encode($array);

        } else {
            $avaliacaoConjunto = new \G2\Entity\AvaliacaoConjunto();
            $arrResult = array(
                'id_avaliacaoconjunto' => null
            );
            $this->view->conjuntoavaliacao = json_encode($arrResult);
        }

//        echo $this->view->headScript()->appendFile('/avaliacao-conjunto/js');
    }

    public function jsAction ()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
    }

    public function retornaSituacaoAvaliacaoConjuntoAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        $where['st_tabela'] = 'tb_avaliacaoconjunto';
        $result = $this->_negocio->findBySituacao($where);
        $arrReturn = array();
        if ($result) {
            foreach ($result as $row) {
                $arrReturn[] = array(
                    'id_situacao' => $row->getId_situacao(),
                    'st_situacao' => $row->getSt_situacao()
                );
            }
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }


    public function retornaTipoProvaAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        $result = $this->_negocio->findAllTipoProva();
        $arrReturn = array();
        if ($result) {
            foreach ($result as $row) {
                $arrReturn[] = array(
                    'id_tipoprova' => $row->getId_tipoprova(),
                    'st_tipoprova' => $row->getSt_tipoprova()
                );
            }
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    public function retornaTipoCalculoAvaliacaoAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        $result = $this->_negocio->findAllTipoCalculoAvaliacao();
        $arrReturn = array();
        if ($result) {
            foreach ($result as $row) {
                $arrReturn[] = array(
                    'id_tipocalculoavaliacao' => $row->getId_tipocalculoavaliacao(),
                    'st_tipocalculoavaliacao' => $row->getSt_tipocalculoavaliacao()
                );
            }
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    public function retornaTipoAvaliacaoAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        $result = $this->_negocio->findAllTipoAvaliacao();
        $arrReturn = array();
        if ($result) {
            foreach ($result as $row) {
                $arrReturn[] = array(
                    'id_tipoavaliacao' => $row->getId_tipoavaliacao(),
                    'st_tipoavaliacao' => $row->getSt_tipoavaliacao()
                );
            }
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    public function retornaPesquisaAvaliacoesAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        $where['id_entidade'] = $this->_negocio->sessao->id_entidade;
        $idAvaRecupera = '';

        $tp_avaliacao = $this->getParam('id_tipoavaliacao');
        $bl = $this->getParam('bl_recuperacao');

        if(isset($tp_avaliacao)){
            $where['id_tipoavaliacao'] = $tp_avaliacao;
        }

        if(isset($bl)){
            $where['bl_recuperacao'] = $bl;
        }


        $result = $this->_negocio->findByVwPesquisaAvaliacao($where);

        $array = array();
        foreach ($result as $key => $ret) {
            $array[$key] = $this->_negocio->toArrayEntity($ret);
        }
        $this->_helper->json($array);
    }

    public function retornaTipoDisciplinaAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        $result = $this->_negocio->findAllTipoDisciplina();
        $array = array();
        foreach ($result as $key => $ret) {
            $array[$key] = $this->_negocio->toArrayEntity($ret);
        }
        $this->_helper->json($array);
    }

   public function retornaAvaliacaoConjuntoRelacaoAction() {
        $this->_helper->viewRenderer->setNoRender(true);

       $where = array();
       $where['id_avaliacaoconjunto'] = $this->getParam('id_avaliacaoconjunto');
       $where['id_entidade'] = $this->_negocio->sessao->id_entidade;

       $result = $this->_negocio->findByAvaliacaoConjuntoRelacao($where);
       $arrayACR = array();
       foreach ($result as $key => $ret) {
           $arrayACR[$key] = $this->_negocio->toArrayEntity($ret);
       }
       $this->_helper->json($arrayACR);
    }

    public function retornaAvaliacaoDisciplinaAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        $where['id_avaliacaoconjunto'] = $this->_getParam('id_avaliacaoconjunto');
//        $where['id_entidade'] = $this->_negocio->sessao->id_entidade;

        $result = $this->_negocio->findByAvaliacaoConjuntoDisciplina($where);
        $arrayACR = array();
        foreach ($result as $key => $ret) {
            $arrayACR[$key] = $this->_negocio->toArrayEntity($ret);
        }
        $this->_helper->json($arrayACR);
    }

    public function salvarAvaliacaoConjuntoAction(){
        if ($this->getRequest()->isPost()){
            $post = $this->getRequest()->getPost();

            $result = $this->_negocio->salvarAvaliacaoConjunto($post['avaliacaoconjunto']);
            $this->_helper->json($result);

        }
    }

    public function salvarAvaliacaoConjuntoRelacaoAction(){
        if ($this->getRequest()->isPost()){
            $post = $this->getRequest()->getPost();

            $result = $this->_negocio->salvarAvaliacaoConjuntoRelacao($post);
            $this->_helper->json($result);

        }
    }

        public function salvarAvaliacaoConjuntoDisciplinaAction(){
        if ($this->getRequest()->isPost()){
            $post = $this->getRequest()->getPost();

            $result = $this->_negocio->salvarAvaliacaoConjuntoDisciplina($post);
            $this->_helper->json($result);

        }
    }



}

<?php

/**
 * Controller para PreparacaoEntidade
 * @author Vinicius Avelino Alcantara <vinicius.alcantara@unyleya.com.br>
 * @since 2015-09-01
 * @package application
 * @subpackage controller
 */


class Default_PreparacaoEntidadeController extends Ead1_Controller
{
    private $negocio;

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('preparacao-entidade/index.js');
    }

    public function init()
    {
        $this->negocio = new \G2\Negocio\Negocio();
    }

    public function indexAction()
    {
    }

}
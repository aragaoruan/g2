<?php

/**
 * Classe de controller MVC para controlar Produtos
 * @author Elcio Guimarães <elcioguimaraes@gmail.com>
 * 
 * @package application
 * @subpackage controller
 */
class Default_ProdutoController extends Ead1_ControllerRestrita {

    public function init() {
        parent::init();
        parent::setupDoctrine();
    }

    public function jsAction(){ 
        $this->_helper->viewRenderer->setNoRender(true);
        echo   $this->view->render('produto/index.js');
    }
    
    public function indexAction() {
        
//        $id_produto = $this->_getParam('id', null);
        $this->view->id = $this->_getParam('id');
//        if($id_produto){
//
//            $bo = new ProdutoBO();
//
//            $pTO = new VwProdutoTO();
//            $pTO->setid_produto($id_produto);
//            $pTO->fetch(true, true, true);
//
//            $pTO->nu_valorparcela = number_format($pTO->getnu_valorparcela(), 2, '.', '');
//            $pTO->nu_valorentrada = number_format($pTO->getnu_valorentrada(), 2, '.', '');
//            $pTO->nu_parcelas = (int)$pTO->getNu_parcelas();
//
//            $arrayVw = $pTO->toBackboneArray();
//            $arrayVw['dt_iniciopontosprom'] = isset($arrayVw['dt_iniciopontosprom']) ? substr($arrayVw['dt_iniciopontosprom'], 0, 10) : null;
//            $arrayVw['dt_fimpontosprom'] 	= isset($arrayVw['dt_fimpontosprom']) ? substr($arrayVw['dt_fimpontosprom'], 0, 10) : null;
//
//            $this->view->planoPagamento = json_encode($arrayVw);
//
//            $to = new ProdutoImagemTO();
//            $to->setId_produto($id_produto);
//            $produtoImagem = $bo->retornarProdutoImagem($to);
//            if($produtoImagem->getTipo()==\Ead1_IMensageiro::SUCESSO){
//                $this->view->produtoImagem = json_encode($produtoImagem->getMensagem());
//            }
//        }
    }
    
    public function uploadAction(){
        
        try {
            
            $bo = new ProdutoBO();

            $pi = new ProdutoImagemTO();
            $pi->setBl_ativo(true);
            $pi->setId_produto($this->_getParam('id_produto', null));
            $ar = new ArquivoTO();
            $ar->setSt_caminhodiretorio('/upload/produto');
            $ar->setAr_arquivo($_FILES['id_produtoimagem']);
            $ar->setSt_extensaoarquivo(substr($_FILES['id_produtoimagem']['name'], (strrpos ($_FILES['id_produtoimagem']['name'],'.')+1)));
            
            $retorno =  $bo->uploadImagemProduto($pi, $ar);
        } catch (Exception $e) {
           $retorno = new \Ead1_Mensageiro('Erro ao fazer o Upload - '. $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        die(json_encode($retorno));

    }
    
    public function excluirImagemAction(){
        
        $to = new ProdutoImagemTO();
        $to->setId_upload($this->_getParam('id_upload', $to));
        
        $bo = new ProdutoBO();
        $retorno =  $bo->deletarProdutoImagem($to);
        
        die(json_encode($retorno));
        
    }
    
    public function pesquisarAction(){
    
    	try {
    
    		$to = new VwProdutoTO();
    		$to->montaToDinamico($this->limpaGetAllParams($this->_getAllParams()));
    		$to->setSt_produto(utf8_decode($to->getSt_produto()));
    		$ro = new ProdutoRO();
    		$mretorno = $ro->retornarVwProduto($to);
    		if($mretorno->getTipo()==Ead1_IMensageiro::SUCESSO){
    
    			$array = array();
    			foreach($mretorno->getMensagem() as $to){
    				$array[] = $to->toArray();
    			}
    
    			$retorno = new \Ead1_Mensageiro($array, \Ead1_IMensageiro::SUCESSO);
    			$retorno->setText(count($array) . " Produtos encontrado(s)");
    		} else {
    			$retorno = $mretorno;
    		}
    
    
    	} catch (Exception $e) {
    		$retorno = new \Ead1_Mensageiro('Erro ao Pesquisar - '. $e->getMessage(), \Ead1_IMensageiro::ERRO);
    	}
    
    	die(json_encode($retorno));
    
    
    }
    
    /*
     * Retorna a VwProduto
     * 
     * @param $array_params
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     */
    public function findVwProdutoAction() {
        $where = $this->_getParam('params');
        $negocio = new G2\Negocio\Produto();
        $entity = new G2\Entity\VwProduto();
        $data = array();

        $result = $negocio->findVwProdutoByArray($where);

        foreach ($result as $entity) {

            //preenche atributo id para utilizacao do backbone
            $data['id'] = $entity->getId_produto();
            $data[] = $negocio->toArrayEntity($entity);
        }

        $this->_helper->json($data);
    }

    /**
     * Pesquisa Dados de produto
     * @author Kayo Silva <kayo.silva@unyleya.com.br>@abstract
     */
    public function pesquisarProdutoAction ()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $negocio = new \G2\Negocio\Produto();
        $arrReturn = array();
        $result = $negocio->retornarProduto($params);
        if ($result) {
            foreach ($result as $row) {
                $arrReturn[] = array(
                    'id' => $row->getId_produto(),
                    'id_produto' => $row->getId_produto(),
                    'st_produto' => $row->getSt_produto(),
                    'id_tipoproduto' => $row->getId_tipoproduto()->getId_tipoproduto(),
                    'st_tipoproduto' => $row->getId_tipoproduto()->getSt_tipoproduto(),
                    'id_situacao' => $row->getId_situacao()->getId_situacao(),
                    'id_usuariocadastro' => $row->getId_usuariocadastro()->getId_usuario(),
                    'id_entidade' => $row->getId_entidade(),
                    'bl_ativo' => $row->getBl_ativo(),
                    'bl_todasformas' => $row->getBl_todasformas(),
                    'bl_todascampanhas' => $row->getBl_todascampanhas(),
                    'nu_gratuito' => $row->getNu_gratuito(),
                    'id_produtoimagempadrao' => $row->getId_produtoimagempadrao(),
                    'bl_unico' => $row->getBl_unico(),
                    'st_tipoproduto' => $row->getSt_tipoproduto(),
                    'id_produtovalor' => $row->getId_produtovalor(),
                    'nu_valor' => $row->getNu_valor(),
                    'nu_valormensal' => $row->getNu_valormensal(),
                    'id_tipoprodutovalor' => $row->getId_tipoprodutovalor(),
                    'st_tipoprodutovalor' => $row->getSt_tipoprodutovalor(),
                    'st_situacao' => $row->getSt_situacao(),
                    'id_planopagamento' => $row->getId_planopagamento(),
                    'nu_parcelas' => $row->getNu_parcelas(),
                    'nu_valorentrada' => $row->getNu_valorentrada(),
                    'nu_valorparcela' => $row->getNu_valorparcela(),
                    'st_nomeentidade' => $row->getSt_nomeentidade(),
                    'st_descricao' => $row->getSt_descricao(),
                    'st_observacoes' => $row->getSt_observacoes(),
                    'bl_destaque' => $row->getBl_destaque(),
                    'dt_cadastro' => $row->getDt_cadastro(),
                    'st_subtitulo' => $row->getSt_subtitulo(),
                    'st_slug' => $row->getSt_slug(),
                    'dt_atualizado' => $row->getDt_atualizado(),
                    'dt_iniciopontosprom' => $row->getDt_iniciopontosprom(),
                    'dt_fimpontosprom' => $row->getDt_fimpontosprom(),
                    'nu_pontos' => $row->getNu_pontos(),
                    'nu_pontospromocional' => $row->getNu_pontospromocional(),
                    'nu_estoque' => $row->getNu_estoque(),
                    'st_informacoesadicionais' => $row->getSt_informacoesadicionais(),
                    'st_estruturacurricular' => $row->getSt_estruturacurricular(),
                    'id_turma' => $row->getId_turma() ? $row->getId_turma()->getId_turma() : null,
                    'st_turma' => $row->getId_turma() ? $row->getId_turma()->getSt_turma() : null
                );
            }
        }
        $this->_helper->json($arrReturn);
    }

    public function pesquisarProdutoCompartilhadoAction () {

        $params = $this->limpaGetAllParams($this->getAllParams());
        $negocio = new \G2\Negocio\Produto();

        $result = $negocio->retornaVwProdutoCompartilhado($params);

        $this->_helper->json($result);
    }

    /**
     * Retorna todos os produtos da entidade
     * @author Helder Silva <helder.silva@unyleya.com.br>@abstract
     */

    public function findProdutosEntityAction(){
        $negocio = new \G2\Negocio\Produto();
        $resultado = $negocio->findAllProduto();
        $this->_helper->json($resultado->getMensagem());
    }
    
}

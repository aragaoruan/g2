<?php

/**
 * Controller para Mensagem do tipo sistema
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @package default
 * @subpackage controller
 * @since 2014-05-07
 */
use G2\Negocio\MensagemPadrao;

class Default_SistemaEntidadeMensagemController extends Ead1_ControllerRestrita
{

    private $negocio;

    public function init ()
    {
        parent::init();
        $this->negocio = new MensagemPadrao();
    }

    public function jsAction ()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('sistema-entidade-mensagem/index.js');
    }

    public function indexAction ()
    {
       
    }

}
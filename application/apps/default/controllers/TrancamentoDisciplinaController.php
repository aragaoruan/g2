<?php

/**
 * Controller default - Trancamento de disciplinas
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @since  17-05-2017
 * @package application
 * @subpackage controller
 */
class Default_TrancamentoDisciplinaController extends Ead1_ControllerRestrita
{

    /**@var \G2\Negocio\TrancamentoDisciplina $negocio */
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\TrancamentoDisciplina();
        $this->negocioMatricula = new \G2\Negocio\Matricula();
    }

    public function indexAction()
    {
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('trancamento-disciplina/index.js');
    }

    public function retornaDisciplinasTrancamentoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $data = $this->limpaGetAllParams($this->getAllParams());
        $whereNull = array('id_alocacao' => 'IS NOT NULL') ;
        $resultado = $this->negocio->findVwGradeNotaSearch(array(), $data, null, null, null, $whereNull);
        $arrReturn = array();
        if ($resultado) {
            foreach ($resultado as $key => $row) {
                $arrReturn[$key]                    = $this->negocio->toArrayEntity($row);
                $arrReturn[$key]['dt_inicio']       = $this->negocio->converterData($row->getDt_inicio(),'d/m/Y' );
                $arrReturn[$key]['dt_encerramento'] = $this->negocio->converterData($row->getDt_encerramento(),'d/m/Y' );
                $arrReturn[$key]['st_notaead']      = (int)$row->getSt_notaead();
                $arrReturn[$key]['bl_destaque']     = $this->negocio->marcaDisciplinasTrancamento($row);
            }
        }
        $this->_helper->json($arrReturn);
    }

    public function trancarAction(){
        $this->_helper->viewRenderer->setNoRender(true);
        $data = $this->limpaGetAllParams($this->getAllParams());
        $resultado = $this->negocio->trancarDisciplinas($data);
        $this->_helper->json($resultado);
    }

    public function verificaRenovacaoAction(){
        $this->_helper->viewRenderer->setNoRender(true);
        $id_matricula = $this->getParam('id_matricula', false);
        $this->_helper->json($this->negocio->verificaProcessoRenovacao($id_matricula));

    }

}
<?php

/**
 * Classe de controller MVC para controlar cadastros de disciplina
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2013-17-12
 * @package application
 * @subpackage controller
 */
class Default_DisciplinaController extends Ead1_ControllerRestrita
{

    /**
     * @var G2\Negocio\Disciplina $_negocio
     */
    private $_negocio;

    public function init()
    {
        parent::init();
        $this->_negocio = new G2\Negocio\Disciplina();
    }

    public function indexAction()
    {
        $id = $this->_getParam('id');
        $form = new DisciplinaForm();
        $nivelEnsino = new \G2\Negocio\NivelEnsino();
        $idPerfil = null;
        $idPerfilProfessor = null;

        $entidade = $this->_negocio->findOneBy('\G2\Entity\Entidade',
            array('id_entidade' => $this->_negocio->sessao->id_entidade));

        if ($entidade && $entidade instanceof \G2\Entity\Entidade) {
            $this->view->esquemaConfiguracaoEntidade =
                $entidade->getId_esquemaconfiguracao()->getId_esquemaconfiguracao();
        }

        $this->view->autores = $this->_negocio->retornarDadosCoordenadorDisciplina(array(
            'id_perfilpedagogico' => 4,
            'id_situacaoperfil' => \G2\Constante\Situacao::TB_USUARIOPERFILENTIDADE_ATIVO
        ));
        $this->view->professores = $this->_negocio->retornarDadosCoordenadorDisciplina(array(
            'id_perfilpedagogico' => 1,
            'id_situacaoperfil' => \G2\Constante\Situacao::TB_USUARIOPERFILENTIDADE_ATIVO
        ));
        $nivelEnsinoResult = $nivelEnsino->findAll();
        $perfil = $this->_negocio->retornaPerfilDisciplinaByEntidadeSessao(array(
            'id_perfilpedagogico' => 4,
            'id_situacao' => \G2\Constante\Situacao::TB_PERFIL_ATIVO
        ));

        $perfilNegocio = new \G2\Negocio\Perfil();
        $perfilProfessor = $perfilNegocio->retornaPerfil(array(
            'id_perfilpedagogico' => 1,
            'id_situacao' => \G2\Constante\Situacao::TB_PERFIL_ATIVO,
            'bl_ativo' => true,
            'id_entidade' => $perfilNegocio->sessao->id_entidade
        ));
        if ($perfil) {
            $idPerfil = $perfil->getId_perfil();
        }

        if (count($perfilProfessor) && $perfilProfessor[0] instanceof \G2\Entity\Perfil) {
            $idPerfilProfessor = $perfilProfessor[0]->getId_perfil();
        }

        $bo = new EntidadeBO();

        $resultEntidades = $bo->retornaEntidadeRecursivo();
        $entidadesArray = array();
        if ($resultEntidades->getTipo() == Ead1_IMensageiro::SUCESSO) {
            foreach ($resultEntidades->getMensagem() as $key => $row) {
                $entidadesArray[] = $row->toBackboneArray();
            }
        }
        $entidades = new Ead1_Mensageiro($entidadesArray, Ead1_IMensageiro::SUCESSO);
        if ($id) {
            $result = $this->_negocio->findDisciplina($id);
            $arrResult = array(
                'id_disciplina' => $result->getId_disciplina(),
                'st_disciplina' => $result->getSt_disciplina(),
                'st_descricao' => $result->getSt_descricao(),
                'nu_identificador' => $result->getNu_identificador(),
                'nu_cargahoraria' => $result->getNu_cargahoraria(),
                'nu_creditos' => $result->getNu_creditos(),
                'nu_codigoparceiro' => $result->getNu_codigoparceiro(),
                'bl_compartilhargrupo' => $result->getBl_compartilhargrupo() ? 1 : 0,
                'id_situacao' => $result->getId_situacao()->getId_situacao(),
                'st_tituloexibicao' => $result->getSt_tituloexibicao(),
                'id_tipodisciplina' => $result->getId_tipodisciplina(),
                'st_ementacertificado' => $result->getSt_ementacertificado(),
                'bl_provamontada' => $result->getBl_provamontada() ? 1 : 0,
                'bl_disciplinasemestre' => $result->getBl_disciplinasemestre() ? 1 : 0,
                'nu_repeticao' => $result->getNu_repeticao(),
                'sg_uf' => $result->getSg_uf() ? $result->getSg_uf()->getSg_uf() : null,
                'st_apelido' => $result->getSt_apelido(),
                'id_grupodisciplina' => $result->getId_grupodisciplina() ? $result->getId_grupodisciplina()->getId_grupodisciplina() : null
            );

            $this->view->disciplina = json_encode($arrResult);
        }
        $this->view->nivelEnsino = $nivelEnsinoResult;
        $this->view->idPerfilProfessor = $idPerfilProfessor;
        $this->view->idPerfil = $idPerfil;
        $this->view->entidades = $entidades;
        $this->view->form = $form;
        $this->view->idDisciplina = $id;
        echo $this->view->headScript()->appendFile('/disciplina/js');
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
    }

    public function getSerieNivelEnsinoAction()
    {
        $params = $this->getAllParams();
        unset($params['module'], $params['controller'], $params['action']);
        $this->_helper->viewRenderer->setNoRender(true);
        $nivelEnsino = new \G2\Negocio\NivelEnsino();

        $result = $nivelEnsino->findSerieNivelEnsino($params);
        $arrReturn = array();
        if ($result) {
            foreach ($result as $row) {
                $arrReturn[] = $row->_toArray();
            }
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    public function getArvoreAreaConhecimentoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->getAllParams();
        $areaConhecimento = new G2\Negocio\AreaConhecimento();
        unset($params['module'], $params['controller'], $params['action']);
        $result = $areaConhecimento->retornaArvoreAreaConhecimento($params);
        $arrReturn = array();
        if ($result->getType() == 'success') {
            foreach ($result->getMensagem() as $row) {
                $dtCadastro = new Zend_Date($row['to']->getDt_cadastro());
                $arrReturn[] = array(
                    'id' => $row['to']->getId_areaconhecimento(),
                    'id_areaconhecimento' => $row['to']->getId_areaconhecimento(),
                    'id_situacao' => $row['to']->getId_situacao(),
                    'st_descricao' => $row['to']->getSt_descricao(),
                    'bl_ativo' => $row['to']->getBl_ativo(),
                    'st_areaconhecimento' => $row['to']->getSt_areaconhecimento(),
                    'id_entidade' => $row['to']->getId_entidade(),
                    'dt_cadastro' => $dtCadastro->toString('dd/M/Y'),
                    'id_usuariocadastro' => $row['to']->getId_usuariocadastro(),
                    'id_areaconhecimentopai' => $row['to']->getId_areaconhecimentopai(),
                    'id_tipoareaconhecimento' => $row['to']->getId_tipoareaconhecimento(),
                    'st_tituloexibicao' => $row['to']->getSt_tituloexibicao(),
                    'bl_extras' => $row['to']->getBl_extras(),
                );
            }
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    public function getDisciplinaSerieNivelEnsinoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $nivelEnsinoNegocio = new \G2\Negocio\NivelEnsino();

        $params = $this->getAllParams();
        unset($params['module'], $params['controller'], $params['action']);
        $result = $this->_negocio->findDisciplinaSerieNivel($params);
        $arrReturn = array(); //Array para retorno
        $arrKeys = array(); //Array para armazenar as chaves
        if ($result->getType() == 'success') {
            foreach ($result->getMensagem() as $i => $row) {
                $serie = $nivelEnsinoNegocio->findSerie($row->getId_serie()); //recupera as series
                if (!array_key_exists($row->getId_nivelensino(), $arrKeys)) {//verifica se o array já foi setado
                    $arrKeys[$row->getId_nivelensino()] = $i; //incrementa a posição i do loop atual no array das chaves
                    $nivelEnsino = $nivelEnsinoNegocio->findNivelEnsino($row->getId_nivelensino()); //recupera o nivel de ensino
                    //cria o array
                    $arrReturn[$arrKeys[$row->getId_nivelensino()]] = array(
                        'id_serie' => $row->getId_serie(),
                        'id_disciplina' => $row->getId_disciplina(),
                        'id_nivelensino' => $row->getId_nivelensino(),
                        'st_nivelensino' => $nivelEnsino->getSt_nivelensino()
                    );
                }
                //cria o array no mesmo indice
                $arrReturn[$arrKeys[$row->getId_nivelensino()]]['series'][] = array(
                    'id_serie' => $serie->getId_serie(),
                    'st_serie' => $serie->getSt_serie()
                );
            }
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    public function salvaAreaJoinDisciplinaAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
        $result = $this->_negocio->salvaArrAreaJoinDisciplina($data);
        if ($result->getType() == 'success') {
            $this->getResponse()->appendBody(json_encode($data));
            $this->getResponse()->setHttpResponseCode(201);
        } else {
            $this->getResponse()->appendBody(json_encode($result->getText()));
            $this->getResponse()->setHttpResponseCode(400);
        }
    }

    public function removeVinculoDisciplinaNivelEnsinoAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $request = $this->getRequest();

        if ($request->isPost()) {
            $data = $request->getPost();
            $result = $this->_negocio->deletaDisciplinaSerieNivelEnsino($data);
            if ($result->getType() == 'success') {
                $this->getResponse()->setHttpResponseCode(204)->appendBody('Registro removido');
            } else {
                $this->getResponse()->setHttpResponseCode(400);
                $this->getResponse()->setBody($result->getText());
            }
        }
    }

    public function findVwDisciplinasPrrAction()
    {
        $data = array();
        $entity = new \G2\Entity\VwDisciplinasPrr();
        $result = $this->_negocio->findVwDisciplinasPrr($this->_getParam('params'));

        foreach ($result as $key => $entity) {

            $data[$key]['id'] = $entity->getId_disciplina();
            $data[$key]['id_disciplina'] = $entity->getId_disciplina();
            $data[$key]['id_matricula'] = $entity->getId_matricula();
            $data[$key]['st_tituloexibicaodisciplina'] = ($entity->getSt_tituloexibicaodisciplina());
            $data[$key]['st_disciplina'] = $entity->getSt_disciplina();
            $data[$key]['st_nota'] = $entity->getSt_nota();
            $data[$key]['nu_valor'] = $entity->getNu_valor();
            $data[$key]['id_vendaproduto'] = $entity->getId_vendaproduto();
            $data[$key]['dt_entrega'] = $entity->getDt_entrega();
            $data[$key]['id_produto'] = $entity->getId_produto();
            $data[$key]['id_venda'] = $entity->getId_venda();
            $data[$key]['id_ocorrencia'] = $entity->getId_ocorrencia();
            $data[$key]['bl_cursandoprr'] = $entity->getBl_cursandoprr();
            $data[$key]['dt_cadastrosala'] = $entity->getDt_cadastrosala()->format('d-m-Y');
            $data[$key]['dt_encerramentosala'] = $entity->getDt_encerramentosala();
            $data[$key]['id_saladeaula'] = $entity->getId_saladeaula();
            $data[$key]['st_projetopedagogico'] = $entity->getSt_projetopedagogico();
            $data[$key]['st_saladeaulaprr'] = $entity->getSt_saladeaulaprr();
            $data[$key]['id_usuario'] = $entity->getId_usuario();
            $data[$key]['id_evolucao'] = $entity->getId_evolucao();
            $data[$key]['nu_valorbrutovenda'] = number_format($entity->getNu_valorbrutovenda(), 2, ',', '.');
        }

        $this->_helper->json($data);
    }

    public function getVwDisciplinaIntegracaoMoodleAction()
    {
        $params = $this->getAllParams();
        unset($params['module'], $params['controller'], $params['action']);
        $disciplinaTO = new \DisciplinaTO();
        $disciplinaTO->setId_disciplina($params['id_disciplina']);
        $ro = new \DisciplinaRO();
        $mensageiro = $ro->retornaVwDisciplinaIntegracaoMoodle($disciplinaTO);

        $arrReturn = array(); //Array para retorno
        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            foreach ($mensageiro->getMensagem() as $i => $row) {
                $arrReturn[] = $row->toBackboneArray();
            }
        }
        $this->_helper->json($arrReturn);
    }

    public function removeVinculoDisciplinaIntegracaoAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $request = $this->getRequest();

        if ($request->isPost()) {
            $data = $request->getPost();
            $bo = new DisciplinaBO();
            $to = new DisciplinaIntegracaoTO();
            $to->setId_disciplinaintegracao($data['id_disciplinaintegracao']);
            $result = $bo->excluirDisciplinaIntegracao($to);
            if ($result->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $this->getResponse()->setHttpResponseCode(204)->appendBody('Registro removido');
            } else {
                $this->getResponse()->setHttpResponseCode(400);
                $this->getResponse()->setBody($result->getMensagem());
            }
        }
    }

    public function retornarDisciplinasEntidadeRecursivaAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->_negocio->retornarDisciplinasRecursivaIMP($params);
        $this->_helper->json($result);
    }

    public function salvarConteudoAction()
    {
        try {
            if ($this->getRequest()->getRawBody()) {
                $decode = Zend_Json::decode($this->getRequest()->getRawBody());
                $this->getRequest()->setPost($decode);
            }

            $negocio = new \G2\Negocio\Disciplina();
            $result = $negocio->salvarConteudoDisciplina($this->getRequest()->getPost(), $_FILES);

            if ($result instanceof \G2\Entity\DisciplinaConteudo) {
                $entity = $negocio->toArrayEntity($result);
                $entity['id_formatoarquivo'] = $result->getId_formatoarquivo()->getId_formatoarquivo();
                $entity['id_disciplina'] = $result->getId_disciplina()->getId_disciplina();
                $entity['id_usuariocadastro'] = $result->getId_usuariocadastro()->getId_usuario();

                $this->_helper->json($entity);
            }
        } catch (Exception $e) {
            $this->_helper->viewRenderer->setNoRender();
            $this->getResponse()->setHttpResponseCode(400)
                ->setBody($e->getMessage());
        }
    }

    public function retornaDisciplinaProjetoPedagogicoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $negocio = new \G2\Negocio\VwModuloDisciplina();
        $params = $this->limpaGetAllParams($this->getRequest()->getParams());
        $array = array();

        foreach ($negocio->findByVwDisciplina($params) as $key => $value) {
            $array[$key] = $negocio->toArrayEntity($value);
            $arrayDisciplina[$key]["id_disciplina"] = $array[$key]["id_disciplina"];
            $arrayDisciplina[$key]["st_disciplina"] = $array[$key]["st_disciplina"];
        }
        $this->_helper->json($arrayDisciplina);

    }

}

<?php

/**
 * Created by PhpStorm.
 * User: helder.silva
 * Date: 15/08/14
 * Time: 15:58
 */
class Default_ContasDeEmailController extends Ead1_ControllerRestrita
{


    public function indexAction()
    {

    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
    }

    public function retornarEntidadeEmailAction()
    {
        $dados = new \G2\Negocio\EmailConfig();
        $dados->findByEntidade();
        $result = array();
        foreach ($dados->findByEntidade() as $dad) {
            $result[] = $dados->toArrayEntity($dad);
        }
        $this->_helper->json($result);
    }

    public function verificarEmailAction()
    {
        $dados = $this->getAllParams();
        $arrReturn = array(
            'status' => true,
            'mensagem' => array()
        );
        if ($dados['id_tipoconexaoemailsaida'] == 1) {
            $tipoconexaoemailsaida = 'ssl';
        } elseif ($dados['id_tipoconexaoemailsaida'] == 2) {
            $tipoconexaoemailsaida = 'tls';
        }
        $smtp = $dados['st_smtp'];
        $conta = $dados['st_conta'];
        $senha = $dados['st_senha'];
        $portaSmtp = $dados['nu_portasaida'];
        $portaPop = $dados['nu_portaentrada'];
        $dominio = explode('@', $dados['st_conta']);


        //valida o email
        $bo = new Ead1_BO();
        $resultEmail = $bo->validaEmailAvancado($dados['st_conta']);
        if (!$resultEmail) {
            $arrReturn['mensagem'][] = 'E-mail da conta inválido.';
        }

        try {
            if ($dados['st_pop']) {
                $pop = new Zend_Mail_Storage_Pop3(array(
                    'host' => $dados['st_pop'],
                    'port' => $portaPop,
                    'user' => $conta,
                    'password' => $senha
                ));
            } elseif ($dados['st_imap']) {
                $mail = new Zend_Mail_Storage_Imap(array(
                    'host' => $dados['st_imap'],
                    'port' => $portaPop,
                    'user' => $conta,
                    'password' => $senha
                ));
            }

            $resultPop = true;
        } catch (Exception $e) {
            $arrReturn['mensagem'][] = "Configurações de POP/IMAP inválidas. (" . $e->getMessage() . ")";
            $resultPop = false;
        }

        try {
            $config = array(
                'auth' => 'login',
                'username' => $conta,
                'password' => $senha,
                'ssl' => $tipoconexaoemailsaida,
                'port' => $portaSmtp
            );

            $smtp = new Zend_Mail_Transport_Smtp($smtp, $config);

            $resultSmtp = true;
        } catch (Exception $e) {
            $arrReturn['mensagem'][] = "Configurações de SMTP inválidas. (" . $e->getMessage() . ")";
            $resultSmtp = false;
        }

        if ($resultPop && $resultSmtp && $resultEmail) {
            $this->_helper->json($arrReturn);
        } else {
            $arrReturn['status'] = false;
            $this->_helper->json($arrReturn);
        }
    }


}
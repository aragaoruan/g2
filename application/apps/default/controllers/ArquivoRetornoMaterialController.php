<?php

/**
 * Classe de controller MVC para controlar Arquivo Retorno de Material
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @package application
 * @subpackage controller
 */
class Default_ArquivoRetornoMaterialController extends Ead1_ControllerRestrita
{

    private $negocio;

    public function init()
    {
        parent::init();
        parent::setupDoctrine();
        $this->negocio = new \G2\Negocio\ArquivoRetornoMaterial();
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('/arquivo-retorno-material/index.js');
    }

    public function indexAction()
    {
        echo $this->view->headScript()->appendFile('/arquivo-retorno-material/js');
    }

    public function retornaArquivoRetornoMaterialEntidadeAction(){
        $this->_helper->viewRenderer->setNoRender(true);
        $results = $this->negocio->findByArquivoRetornoMaterial($this->getAllParams());
        $this->_helper->json($results);


    }

    public function retornaArquivoRetornoPacoteAction(){
        $this->_helper->viewRenderer->setNoRender(true);
        $results = $this->negocio->findByArquivoRetornoPacote($this->getAllParams());
        $this->_helper->json($results);
    }

    public function uploadAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        if (isset($_FILES['arquivo']['name'])) {
            $arquivo = $_FILES['arquivo'];

            // Validar extensao do arquivo
            $extensions = array('xls', 'xlsx');
            $fileExtension = pathinfo($arquivo['name'], PATHINFO_EXTENSION);

            if(!in_array($fileExtension, $extensions)) {
                return false;
            }

            $mensageiro = $this->negocio->uploadArquivo($arquivo);
            $this->_helper->json($mensageiro);
        }
    }



}

<?php

/**
 * Classe de controller MVC para controlar Tipos de Projeto do Contrato
 * @author Elcio Guimarães <elcioguimaraes@gmail.com>
 * 
 * @package application
 * @subpackage controller
 */
class Default_ProjetoContratoDuracaoTipoController extends Ead1_ControllerRestrita {
    
    public function init() {
        parent::init();
        parent::setupDoctrine();
    }

    public function jsAction(){ 
        $this->_helper->viewRenderer->setNoRender(true);
        echo   $this->view->render('projeto-contrato-duracao-tipo/index.js');
    }
    
    public function indexAction() {
        
    }
    
}


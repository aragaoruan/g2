<?php

/**
 * Classe de controller MVC para Consulta de pagamentos
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class Default_ConsultaPagamentoController extends Ead1_ControllerRestrita {

    private $_negocio;
    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('consulta-pagamento/index.js');
    }

    public function init()
    {
        parent::init();
        parent::setupDoctrine();
        $this->_negocio = new \G2\Negocio\ConsultaPagamento();
    }

    public function indexAction()
    {
    }

    public function retornaConsultaPagamentoAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $params = array();
        $tipo_liberacao = $this->getParam('tipo_liberacao');
        $id_categoriasala = $this->getParam('id_categoriasala');
        $id_tipodisciplina = $this->getParam('id_tipodisciplina');
        $id_professor = $this->getParam('id_professor');

        if (isset($id_professor) && $id_professor != null) {
            $params['id_professor'] = $id_professor;
        }
        $params['id_tipodisciplina'] = $id_tipodisciplina;
        $params['id_categoriasala'] = $id_categoriasala;

        switch ($tipo_liberacao) {
            case 'financeiro':
                $return = $this->_negocio->retornarPagamentosConsulta($params, \G2\Negocio\ConsultaPagamento::FINANCEIRO);
                break;
            case 'pago':
                $return = $this->_negocio->retornarPagamentosConsulta($params, \G2\Negocio\ConsultaPagamento::PAGOS);
                break;
            default:
                $return = '';
                break;
        }


        if ($return) {
            foreach($return as $key => $re){
                //Zend_Debug::dump($this->_negocio->toJsonEntity($re));die;
                $arReturn[$key] = $this->_negocio->toArrayEntity($re);
            }
        } else {
            $arReturn = array();
        }
        $this->_helper->json($arReturn);
        //$this->getResponse()->appendBody(json_encode($arReturn));
    }


    public function retornaProfessorCoordenadorAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->_negocio->retornarProfessorCoordenador($params);
        $this->getResponse()->appendBody(json_encode($result));
    }
}

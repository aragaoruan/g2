<?php

/**
 * Controller Relatorio Comercial Ecommerce
 * @author Neemiuas Santos <neemias.santos@unyleya.com.br>
 * @since 2015-04-16
 * @package application
 * @subpackage controller
 */
//use G2\Negocio\Pessoa;

class Default_RelatorioComercialEcommerceController extends Ead1_ControllerRestrita
{

    /**
     * @var \G2\Negocio\VwEcommerce
     */
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\VwEcommerce();
    }

    public function indexAction()
    {
        $js = $this->view->render('relatorio-comercial-ecommerce/index.js');
        echo $this->view->headScript()->appendScript($js);

    }

    public function pesquisaRelatorioAction()
    {

        try {
            $params = $this->limpaGetAllParams($this->getAllParams());
            $this->_helper->json($this->negocio->retornaResultadoPesquisa($params));
        } catch(\Exception $e){
            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json($e->getMessage());
        }


    }

    public function gerarXlsAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocio->retornaResultadoPesquisa($params);
        $this->view->result = $result;
    }
}
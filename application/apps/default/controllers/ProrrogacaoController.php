<?php
/**
 * Classe de controller Prorrogação
 * @author Neemias Santos <neemias.santos@gmail.com>
 * Date: 29-04-2015
 * Time: 09:20
 */

class Default_ProrrogacaoController extends Ead1_ControllerRestrita {

    public function init() {
        parent::init();
        parent::setupDoctrine();
    }

    public function jsAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('prorrogacao/index.js');
    }

    public function indexAction() {


    }

} 
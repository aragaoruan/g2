<?php

/**
 * Description of AvaliacaoController
 *
 * @author Rafael Bruno(RBD) <rafael.oliveira@unyleya.com.br>
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */
class Default_AvaliacaoController extends Ead1_ControllerRestrita
{

    /**
     * @var \G2\Negocio\Avaliacao
     */
    protected $_negocio;

    public function init()
    {
        parent::init();
        $this->_negocio = new G2\Negocio\Avaliacao();
    }


    /**
     * Adicionado funcoes para a Funcionalidade no Menu
     */
    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('avaliacao/index.js');
    }

    public function indexAction()
    {
    }


    public function getTipoAvaliacaoAction()
    {
        $to = new TipoAvaliacaoTO();

        $bo = new AvaliacaoBO();
        $mensageiro = $bo->retornarTipoAvaliacao($to);

        $this->_helper->json($mensageiro);
    }

    public function getOrganizacaoProjetoAction()
    {
        $to = new EntidadeTO();
        $to->setBl_acessasistema(false);
        $to->setBl_ativo(true);

        $bo = new ProjetoPedagogicoBO();
        $mensageiro = $bo->retornarEntidadesProjetos($to);

        $this->_helper->json($mensageiro);
    }


    /**
     * Action para funcionalidade gerenciaNotas
     * @author Rafael Bruno(RBD) <rafael.oliveira@unyleya.com.br>
     */
    public function gerenciaNotasAction()
    {
        $this->_helper->layout()->disableLayout();

        echo $this->view->headScript()
            ->appendScript($this->view->render('/avaliacao/gerencia-notas.js'));
        echo $this->view->headScript()
            ->appendScript('/js/jquery.form.js');
    }

    /**
     * Action para funcionalidade Gerenciar Notas
     * @author Rafael Bruno(RBD) <rafael.oliveira@unyleya.com.br>
     */
    public function salvarAvaliacaoAlunoAction()
    {
        try {
            if ($this->getRequest()->isPost()) {
                $post = $this->getRequest()->getPost();
                //verifica se no post NÃO veio a posição correta OU se ela esta vazia, assim gera uma exception
                if (!isset($post['avaliacaoaluno']) || empty($post['avaliacaoaluno'])) {
                    throw new Exception("Parametros esperados não encontrado.");
                }
                $result = $this->_negocio->cadastrarAvaliacaoAluno($post['avaliacaoaluno'], $_FILES);
                $entityRetorno = [];
                if ($result) {
                    $entityRetorno = $this->_negocio->findOneBy('\G2\Entity\VwAvaliacaoAluno',
                        array('id_avaliacaoaluno' => $result->getId_avaliacaoaluno()));
                    if ($entityRetorno) {
                        $entityRetorno = $this->_negocio->toArrayEntity($entityRetorno);
                    }
                }
                $this->_helper->json($entityRetorno);
            }
        } catch (Exception $e) {
            $this->getResponse()->setHttpResponseCode(500);
            $this->_helper->json($e->getMessage());
        }
    }

    /**
     * Action para funcionalidade Gerenciar Notas
     * @author Rafael Bruno(RBD) <rafael.oliveira@unyleya.com.br>
     */
    public function editarAvaliacaoAlunoAction()
    {
        $post = $this->getRequest()->getPost();
        $result = $this->_negocio->salvarAvaliacaoAluno($post);

        $where = array(
            'id_matricula' => $result->getId_matricula(),
            'id_avaliacaoconjuntoreferencia' => $result->getId_avaliacaoconjuntoreferencia(),
            'id_avaliacao' => $result->getId_avaliacao()
        );

        $vwAvaliacaoAluno = $this->_negocio->findVwAvaliacaoAluno($where);
        $toArray = $this->_negocio->toArrayEntity($vwAvaliacaoAluno[0]);
        $this->_helper->json($toArray);
    }

    public function verificarStatusNotaAction()
    {
        $id_matriculadisciplina = $this->getParam('id_matriculadisciplina');
        $st_nota = $this->getParam('st_nota');

        $matriculadisciplina = $this->_negocio->find('\G2\Entity\MatriculaDisciplina', $id_matriculadisciplina);
        $matricula = $this->_negocio->find('\G2\Entity\Matricula', $matriculadisciplina->getId_matricula());

        $bl_satisfatorio = $this->_negocio->verificarStatusNota($matriculadisciplina, $st_nota);

        $response = array(
            'id_matricula' => (int)$matricula->getId_matricula(),
            'id_situacaoagendamento' => (int)($matricula->getId_situacaoagendamento() ? $matricula->getId_situacaoagendamento()->getId_situacao() : null),
            'bl_satisfatorio' => $bl_satisfatorio
        );

        $this->_helper->json($response);
    }

}

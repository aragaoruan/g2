<?php

/**
 * Controller para Folha de Numeros Apoio
 * @author Helder Fernandes <helder.silva@unyleya.com.br>
 * @since 2015-02-13
 * @package application
 * @subpackage controller
 */
class Default_FolhaDeNumerosApoioController extends Ead1_ControllerRestrita
{
    private $negocio;


    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('folha-de-numeros-apoio/index.js');
    }

    public function init()
    {
        parent::init();
//        $this->negocio = new \G2\Negocio\Negocio();
    }

    public function indexAction()
    {

    }

    public function retornaCategoriasAction()
    {
        $this->negocio = new G2\Negocio\FolhaDeNumerosApoio();
        $result = $this->negocio->retornaCategorias();
        $data = array();
        foreach ($result as $categoria) {
            $toArray = $this->negocio->toArrayEntity($categoria);
            array_push($data, $toArray);
        }
        $this->_helper->json($data);
    }

    /**
     * Retorna a propria entidade e sua filhas
     */
    public function retornaEntidadeAction()
    {
        $this->_negocio = new G2\Negocio\Assunto();
        $where = array(
            'nu_entidadepai' => $this->_negocio->sessao->id_entidade
        );
        $data = array();

        $result = $this->_negocio->findVwEntidadeRecursivaId($where);

        foreach ($result as $entity) {
            $toArray = $this->_negocio->toArrayEntity($entity);
            array_push($data, $toArray);
        }
        $this->_helper->json($data);
    }

    /**
     * EXECUTA TODA A MONTAGEM DO JSON PARA RETORNO, DESDE A BUSCA NO BANCO
     * ATE A MONTAGEM DO ARRAY
     */
    public function gerarRelatorioAction()
    {
        $post = $this->limpaGetAllParams($this->getAllParams());
        $this->negocio = new G2\Negocio\FolhaDeNumerosApoio();
        if (!$post['id_entidade']) {
            $post['id_entidadecadastro'] = $this->negocio->sessao->id_entidade;
            unset($post['id_entidade']);
        }
        if (!$post['id_categoria']) {
            unset($post['id_categoria']);
        }
//        if (!$post['id_evolucao']) {
        unset($post['id_evolucao']);
//        }
        $result = $this->negocio->retornaRelatorio($post);
//        Zend_Debug::dump($result);exit;

        $array = array();
        foreach ($result as $value) {
            //SE NÃO EXISTE TAL ENTIDADE NO ARRAY, CRIA A ENTIDADE, A CATEGORIA DO ELEMENTO E A TURMA
            if (!array_key_exists($value->getId_entidade(), $array)) {
                $array[$value->getId_entidade()] = array(
                    'id_entidade' => $value->getId_entidade(),
                    'st_entidade' => $value->getSt_entidade(),
                    'categoria' => array(
                        $value->getId_categoria() => array(
                            'id_categoria' => $value->getId_categoria(),
                            'st_categoria' => $value->getSt_categoria(),
                            'turma' => array(
                                0 => array(
                                    'dt_inicio' => (new DateTime($value->getDt_inicio()))->format('d/m/Y'),
                                    'id_turma' => $value->getId_turma(),
                                    'id_entidade' => $value->getId_entidade(),
                                    'id_evolucao' => $value->getId_evolucao(),
                                    'id_categoria' => $value->getId_categoria(),
                                    'nu_matriculas' => $value->getNu_matriculas(),
                                    'st_cargahoraria' => $value->getSt_cargahoraria(),
                                    'st_turma' => $value->getSt_turma(),
                                    'st_categoria' => $value->getSt_categoria(),
                                    'st_entidade' => $value->getSt_entidade(),
                                    'st_evolucao' => $value->getSt_evolucao(),
                                    'st_turno' => $value->getSt_turno(),
                                    'nu_freepass' => $value->getNu_freepass(),
                                    'nu_garantiaduo' => $value->getNu_garantiaduo()
                                )
                            )

                        )

                    )
                );
            } else {
                //CASO EXISTA A ENTIDADE, VERIFICA SE TEM A CATEGORIA, CASO NÃO TENHA, CRIA UMA CATEGORIA INSERINDO
                //A TURMA DO ELEMENTO
                if (!array_key_exists($value->getId_categoria(), $array[$value->getId_entidade()]['categoria'])) {
                    $array[$value->getId_entidade()]['categoria'][$value->getId_categoria()] = array(
                        'id_categoria' => $value->getId_categoria(),
                        'st_categoria' => $value->getSt_categoria(),
                        'turma' => array(
                            0 => array(
                                'dt_inicio' => (new DateTime($value->getDt_inicio()))->format('d/m/Y'),
                                'id_turma' => $value->getId_turma(),
                                'id_entidade' => $value->getId_entidade(),
                                'id_evolucao' => $value->getId_evolucao(),
                                'id_categoria' => $value->getId_categoria(),
                                'nu_matriculas' => $value->getNu_matriculas(),
                                'st_cargahoraria' => $value->getSt_cargahoraria(),
                                'st_turma' => $value->getSt_turma(),
                                'st_categoria' => $value->getSt_categoria(),
                                'st_entidade' => $value->getSt_entidade(),
                                'st_evolucao' => $value->getSt_evolucao(),
                                'st_turno' => $value->getSt_turno(),
                                'nu_freepass' => $value->getNu_freepass(),
                                'nu_garantiaduo' => $value->getNu_garantiaduo()
                            )
                        ));

                } else {
                    //CASO HAJA ENTIDADE E CATEGORIA, APENAS INSERE EM SEU LUGAR CORRESPONDENTE O ELEMENTO.
                    array_push($array[$value->getId_entidade()]['categoria'][$value->getId_categoria()]['turma'], array(
                        'dt_inicio' => (new DateTime($value->getDt_inicio()))->format('d/m/Y'),
                        'id_turma' => $value->getId_turma(),
                        'id_entidade' => $value->getId_entidade(),
                        'id_evolucao' => $value->getId_evolucao(),
                        'id_categoria' => $value->getId_categoria(),
                        'nu_matriculas' => $value->getNu_matriculas(),
                        'st_cargahoraria' => $value->getSt_cargahoraria(),
                        'st_turma' => $value->getSt_turma(),
                        'st_categoria' => $value->getSt_categoria(),
                        'st_entidade' => $value->getSt_entidade(),
                        'st_evolucao' => $value->getSt_evolucao(),
                        'st_turno' => $value->getSt_turno(),
                        'nu_freepass' => $value->getNu_freepass(),
                        'nu_garantiaduo' => $value->getNu_garantiaduo()
                    ));
                }
            }
        }

        //ZERA O INDEX DO ARRAY PARA RETORNAR O JSON
        $arrCoisa = [];
        foreach ($array as $arr) {
            $arrCoisa[] = $arr;
        }

        $this->_helper->json($arrCoisa);
    }
}

<?php

class Default_HorarioDeAulaController extends Ead1_ControllerRestrita
{

    /**
     * @var \G2\Negocio\HorarioAula $negocio
     */
    private $negocio;

    public function init ()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\HorarioAula();
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('horario-de-aula/index.js');
    }

    public function indexAction()
    {
    }

    public function turnosAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        $negocio = new \G2\Negocio\Turno();
        $mensageiro = $negocio->findAll();

        echo json_encode($mensageiro);
    }

    public function horariosPontoSoftAction() {

        try {
            $negocio    = new \G2\Negocio\IntegracaoExterna(Ead1_Sessao::getObjetoSessaoGeral()->getid_entidade());
            $mensageiro = $negocio->retornarHorariosAcessos();
            if($mensageiro->getTipo()==Ead1_IMensageiro::SUCESSO){
                $this->_helper->json($mensageiro->getMensagem());
            } else {
                throw new \Exception($mensageiro->getFirstMensagem());
            }

        } catch(\Exception $e){
            $this->getResponse()->setHttpResponseCode(400);
            $mensageiro = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
            $this->_helper->json($mensageiro->toArray());
        }


    }

    public function horariosPorDataAction() {
        $dt = $this->getParam('dt');

        $results = $this->negocio->findHorariosDisponiveisPorData($dt);

        $this->_helper->json($results);
    }
    
    public function retornaTipoAulaAction(){
        $tipoAula = new \G2\Negocio\TipoAula();
        $result = $tipoAula->findTipoAula(array('bl_ativo'=>true));
        $retorno = array();
        if($result){
            foreach ($result as $value){
                array_push($retorno, $tipoAula->toArrayEntity($value));
            }
        }

        $this->_helper->json($retorno);
    }

}


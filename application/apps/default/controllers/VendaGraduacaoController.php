<?php

/**
 * Class VendaGraduacaoController
 * Controller para Venda de Graduação
 * @author helder.silva <helder.silva@unyleya.com.br>
 * @since 2015-12-15
 * @package application
 * @subpackage controller
 */
class Default_VendaGraduacaoController extends Ead1_ControllerRestrita
{
    /**
     * @var \G2\Negocio\VendaGraduacao
     */
    private $negocio;

    /**
     * @var \G2\Negocio\Ocorrencia
     */
    private $negocioOcorrencia;

    /**
     * @var \G2\Negocio\Recebimento
     */
    private $negocioRecebimento;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\VendaGraduacao();
        $this->negocioOcorrencia = new \G2\Negocio\Ocorrencia();
        $this->negocioRecebimento = new \G2\Negocio\Recebimento();
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
    }

    public function indexAction()
    {
        echo $this->view->headLink()->appendStylesheet('/css/auto-complete-pessoa.css');
        $id = $this->getParam('id');
        $venda = null;
        if ($id) {
            $result = $this->negocio->find('\G2\Entity\Venda', $id);
            if ($result) {
                //usa o mesmo metodo da Negocio/VendaGraduaca para que se necessario mudança faça apenas em 1 lugar
                $venda = $this->negocio->retornaVenda($result);
            }
        }

        $this->view->form = new CadastroRapidoPessoaForm();
        $this->view->vendagraduacao = json_encode($venda);
    }

    public function retornaInformacoesValoresAction()
    {
        $response = null;

        try {
            $params = $this->limpaGetAllParams($this->getAllParams());
            $response = $this->negocio->retornaInformacoesValores($params);

            if (!empty($response['error'])) {
                throw new \Exception($response['error']);
            }
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(500);

            $response = array(
                'type' => 'error',
                'title' => 'Erro',
                'text' => $e->getMessage()
            );
        }

        $this->_helper->json($response);
    }

    public function salvarVendaAction()
    {
        $response = null;

        try {
            $params = $this->limpaGetAllParams($this->getAllParams());
            $objetoTratado = $this->negocio->trataParametros($params);

            $ng_venda = new \G2\Negocio\Venda();
            $mensageiro = $ng_venda->procedimentoSalvarVenda($objetoTratado);

            if ($mensageiro->getType() !== \Ead1_IMensageiro::TYPE_SUCESSO) {
                throw new \Exception('Erro ao salvar venda');
            }

            $venda = $this->negocio->retornaVenda($mensageiro->getFirstMensagem());

            $response = array(
                'type' => 'success',
                'title' => 'Sucesso',
                'text' => 'Venda salva com sucesso.',
                'venda' => $venda
            );
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(500);

            $response = array(
                'type' => 'error',
                'title' => 'Erro',
                'text' => $e->getMessage()
            );
        }

        $this->_helper->json($response);
    }

    public function getSalasByTurmaAction()
    {
        $results = array();

        $id_produto = $this->getParam('id_produto');
        $id_usuario = $this->getParam('id_usuario');

        $params = $this->limpaGetAllParams($this->getAllParams());
        unset($params['id_produto'], $params['id_usuario']);

        if ($id_produto && $id_usuario) {
            $results = $this->negocio->findSalasDisponiveisOfertaTurma($id_produto, $id_usuario, $params);
        }

        $this->_helper->json($results);
    }

    public function getSalasPadraoAction()
    {
        $results = array();

        $dt_abertura = $this->getParam('dt_abertura');
        $id_usuario = $this->getParam('id_usuario');
        $id_projetopedagogico = $this->getParam('id_projetopedagogico');
        $limit = $this->getParam('limit');

        if ($dt_abertura && $id_usuario && $id_projetopedagogico) {
            $results = $this->negocio->findSalasPadraoPorData($dt_abertura, $id_usuario, $id_projetopedagogico, $limit);
        }

        $this->_helper->json($results);
    }

    public function calculaValoresAction()
    {
        $disciplinas = $this->getParam('id_disciplina');
        $params = $this->limpaGetAllParams($this->getAllParams());
        unset($params['id_disciplina']);

        $mensageiro = $this->negocio->calculaValoresContrato($disciplinas, $params);

        if ($mensageiro->getTipo() == \Ead1_IMensageiro::SUCESSO) {
            $mensageiro = $mensageiro->getFirstMensagem();
        } else {
            $this->getResponse()->setHttpResponseCode(400);
        }

        $this->_helper->json($mensageiro);
    }

    public function retornaSubAssuntoAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocioOcorrencia->findSubAssuntoCo($params);
        $arrRetorno = [];
        foreach ($result as $value) {
            array_push($arrRetorno, $this->negocio->toArrayEntity($value));
        };
        $this->_helper->json($arrRetorno);
    }

    public function retornaAtendenteSubassuntoAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocioOcorrencia->findByAtendenteCo($params);
        $this->_helper->json($result);
    }

    public function retornaOcorrenciaAproveitamentoAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
    }

    public function confirmarMatriculaAction()
    {
        $id_venda = $this->getParam('id_venda');

        if ($id_venda) {
            $mensageiro = $this->negocioRecebimento->processoAtivacaoAutomatica($id_venda);
        } else {
            $mensageiro = new \Ead1_Mensageiro("Parâmetro da venda não encontrado.", \Ead1_IMensageiro::ERRO);
        }

        $this->_helper->json($mensageiro);
    }

    public function calculaCampanhasAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $this->_helper->json($this->negocio->calcularCampanhas($params));
    }

}

<?php

/**
 * Controller Relatorio Calculo Pagamento Materia
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 * @since 2016-01-26
 * @package application
 */
class Default_RelatorioCalculoPagamentoMateriaController extends Ead1_ControllerRestrita
{

    public function init()
    {
        $this->negocio = new \G2\Negocio\VwComercialGeral();
    }

    public function indexAction(){
        $js = $this->view->render('relatorio-calculo-pagamento-materia/index.js');
        echo $this->view->headScript()->appendScript($js);
    }

    public function pesquisaDadosAction(){
        $params = $this->limpaGetAllParams($this->getRequest()->getParams());
        $this->_helper->json($this->negocio->retornaDados($params));
    }

    public function gerarXlsAction(){
        $params = $this->limpaGetAllParams($this->getRequest()->getParams());
        $this->view->result = $this->negocio->retornaDados($params);
    }
}
<?php

/**
 * Classe de controller MVC para controlar a configuração do primeiro acesso
 * @author rafael.rocha <rafael.rocha@unyleya.com.br>
 * 
 * @package application
 * @subpackage controller
 */
class Default_PrimeiroAcessoController extends Ead1_ControllerRestrita {

    private $negocio;

    public function indexAction() {
        $this->_helper->viewRenderer->setNoRender(false);
    }

    public function jsAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('primeiro-acesso/index.js');
    }

    public function preBoasVindasAction() {
        $this->negocio = new \G2\Negocio\PrimeiroAcesso();
        $etapa = $this->negocio->findPABoasVindasByEntidadeSessao();
        $this->view->textoBoasVindas = $etapa ? $etapa->getId_textosistema()->getSt_texto() : '';
    }

    public function preDadosContatoAction() {
        $this->negocio = new \G2\Negocio\PrimeiroAcesso();
        $etapa = $this->negocio->findPADadosContatoByEntidadeSessao();
        $this->view->textoMensagem = $etapa ? $etapa->getId_textomensagem()->getSt_texto() : '';
        $this->view->textoInstrucao = $etapa ? $etapa->getId_textoinstrucao()->getSt_texto() : '';
    }

    public function preDadosCadastraisAction() {
        $this->negocio = new \G2\Negocio\PrimeiroAcesso();
        $etapa = $this->negocio->findPADadosCadastraisByEntidadeSessao();
        $this->view->textoMensagem = $etapa ? $etapa->getId_textomensagem()->getSt_texto() : '';
        $this->view->textoInstrucao = $etapa ? $etapa->getId_textoinstrucao()->getSt_texto() : '';
    }

    public function preAceitacaoContratoAction() {
        $primeiroAcessoBO = new PrimeiroAcessoBO();
        $texto = $primeiroAcessoBO->retornarTextoSistemaContrato()->getSt_texto();
        $this->view->texto = $texto ? $texto : '';
    }

    public function preMensagemInformativaAction() {
        $this->negocio = new \G2\Negocio\PrimeiroAcesso();
        $etapa = $this->negocio->findPAMensagemInformativaByEntidadeSessao();
        $this->view->texto = $etapa ? $etapa->getId_textomensagem()->getSt_texto() : '';
    }

    public function findAssuntosPaiAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->negocio = new \G2\Negocio\PrimeiroAcesso();
        echo $this->negocio->findAssuntosJSON();
    }

    public function findSubAssuntosAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->negocio = new \G2\Negocio\PrimeiroAcesso();
        echo $this->negocio->findSubAssuntosJSON($this->getParam('id_pai'));
    }

    public function findCategoriasOcorrenciaAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->negocio = new \G2\Negocio\PrimeiroAcesso();
        echo $this->negocio->findCategoriaOcorrenciaJSON();
    }

}

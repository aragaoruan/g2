<?php

/**
 * Classe controller para operacoes relacionadas com assuntos do sistemas 
 * de ocorrencias
 *
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class Default_AssuntoController extends Ead1_ControllerRestrita {

    private $_negocio;

    public function indexAction()
    {
        ;
    }

    /**
     * Action inicial, para funcionalidade assunto/compartilhar
     */
    public function compartilharAction()
    {
        echo $this->view->headScript()->appendFile("/modelo-backbone/VwEntidadeRecursivaId");
        echo $this->view->headScript()->appendFile('/modelo-backbone/VwAssuntoEntidade');
        echo $this->view->headScript()->appendFile('/modelo-backbone/TipoOcorrencia');
        echo $this->view->headScript()->appendFile('/assunto/compartilhar-js');
    }

    public function compartilharJsAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        echo $this->view->render('assunto/compartilhar.js');
    }

    /**
     * Action responsavel por retornar dados da VwEntidadeRecursivaId
     * @params request
     * @return \Zend_Json::encoder
     */
    public function findVwEntidadeRecursivaIdAction()
    {
        $this->_negocio = new G2\Negocio\Assunto();
        $where = array(
            'nu_entidadepai' => $this->_negocio->sessao->id_entidade
        );
        $data = array();

        $result = $this->_negocio->findVwEntidadeRecursivaId($where);

        foreach ($result as $entity) {
            $toArray = $this->_negocio->toArrayEntity($entity);
            array_push($data, $toArray);
        }

        $this->_helper->json($data);
    }

    /**
     * Action responsavel por retornar dados da VwAssuntoEntidade
     * @params request
     * @return \Zend_Json::encoder
     */
    public function findVwAssuntoEntidadeAction()
    {
        $this->_negocio = new G2\Negocio\Assunto();
        $where = array(
            'id_entidadecadastro' => $this->_negocio->sessao->id_entidade,
            'id_entidade' => $this->getRequest()->getParam('id_entidadecadastro')
        );
        $data = array();

        $result = $this->_negocio->findVwAssuntoEntidade($where);

        foreach ($result as $entity) {
            $toArray = $this->_negocio->toArrayEntity($entity);
            array_push($data, $toArray);
        }

        $this->_helper->json($data);
    }

    /**
     * Action responsavel por receber request de dados de compartilhamentos
     * de assuntos feitos pelo usuario, na funcionalidade.
     * @params request
     * @return \Zend_Json::encoder result success ou error
     */
    public function atualizarCompartilhamentoAssuntoAction()
    {
        $this->_negocio = new G2\Negocio\Assunto();

        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();

            if ($this->_negocio->atualizarCompartilhamentoAssuntos($post)) {
                $this->_helper->json(array('result' => TRUE));
            }else{
                $this->_helper->json(array('result' => FALSE));
            }
        }
    }
    
    /**
     * Action responsavel por retornar tipos de ocorrencias apartir de classe
     * estatica que contem constantes de tipos de ocorrencias.
     * @return \Zend_Json::encoder
     */
    public function retornaTipoOcorrenciaConstanteAction(){
        $constante =  \G2\Constante\TipoOcorrencia::getArrayTipoOcorrencia();
        $data = array();
        
        foreach($constante as $key => $value){
            $entity = array('id_tipoocorrencia' => $key, 'st_tipoocorrencia' => $value);
            array_push($data, $entity);
        }
        
        $this->_helper->json($data);
    }

}

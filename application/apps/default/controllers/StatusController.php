<?php

/**
 * Controller para gerenciar os status do sistema
 * @author Helder Silva <helder.silva@unyleya.com.br    >
 * @since 2015-11-18
 * @package application
 * @subpackage controller
 */
class Default_StatusController extends Zend_Controller_Action
{

    private $negocio;
    private $_config;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Negocio();
        $front = Zend_Controller_Front::getInstance();
        $this->_config = $front->getParam('bootstrap');


    }

    public function bancoAction()
    {
        echo $this->negocio->dataBaseStatus();
        die;
    }

    public function webserviceAction()
    {
        $client = new Zend_Http_Client($this->_config->getOption('st_url_ws'), array('keepalive' => true));
        try {
            $status = $client->request(Zend_Http_Client::GET)->getStatus();
            if($status == 200){
                echo true;
            }else{
                echo false;
            }
        } catch (\Exception $e) {
            echo $e->getCode();
        }
        die;
    }

    public function portalAction()
    {
        $client = new Zend_Http_Client($this->_config->getOption('st_url_portal'), array('keepalive' => true));
        try {
            $status = $client->request(Zend_Http_Client::GET)->getStatus();
            if($status == 200){
                echo true;
            }else{
                echo false;
            }
        } catch (\Exception $e) {
            echo $e->getCode();
        }
        die;
    }

    public function g2sAction()
    {
        $client = new Zend_Http_Client($this->_config->getOption('st_url_g2s'), array('keepalive' => true));
        try {
            $status = $client->request(Zend_Http_Client::GET)->getStatus();
            if($status == 200){
                echo true;
            }else{
                echo false;
            }
        } catch (\Exception $e) {
            echo $e->getCode();
        }
        die;
    }

    public function lojaAction()
    {
        $client = new Zend_Http_Client($this->_config->getOption('st_url_g2s'), array('keepalive' => true));
        try {
            $status = $client->request(Zend_Http_Client::GET)->getStatus();
            if($status == 200){
                echo true;
            }else{
                echo false;
            }
        } catch (\Exception $e) {
            echo $e->getCode();
        }
        die;
    }

}
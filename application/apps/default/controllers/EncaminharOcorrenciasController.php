<?php
/**
 * Created by PhpStorm.
 * User: Alex Alexandre <alex.alexandre@unyleya.com.br>
 * Date: 16/05/18
 * Time: 14:25
 */

class Default_EncaminharOcorrenciasController extends Ead1_ControllerRestrita
{

    /**
     * @var \G2\Negocio\Ocorrencia
     */
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Ocorrencia();
    }

    public function indexAction()
    {

    }

    public function retornarFuncaoAction()
    {
        $funcoes = $this->negocio->findFuncaoCo();

        $arrRetorno = [];
        $arrFinal = array();
        foreach ($funcoes as $v) {
            $arrRetorno['id_funcao'] = $v['id_funcao'] . '-' . $v['id_nucleoco'];
            $arrRetorno['st_funcao'] = $v['st_funcao'] . ' - ' . $v['st_nucleoco'];
            array_push($arrFinal, $arrRetorno);
        }

        $this->_helper->json($arrFinal);
    }

    public function retornarEvolucaoAction()
    {
        $evolucao = new \G2\Negocio\Evolucao();
        $retorno = $evolucao->retornarEvolucao(array('st_tabela' => 'tb_ocorrencia'));
        $this->_helper->json($this->negocio->toArrayEntity($retorno));
    }

    public function retornarAtendenteAction()
    {
        $idNucleo = $this->retornarIdNucleoCo($this->getParam('id_funcao'));
        $this->_helper->json($this->negocio->findByAtendenteCo(array('id_nucleoco' => $idNucleo)));
    }

    public function retornarAssuntoAction()
    {
        $idNucleo = $this->retornarIdNucleoCo($this->getParam('id_funcao'));
        $this->_helper->json($this->negocio->findAssuntoCo(array('id_nucleoco' => $idNucleo)));
    }

    /**
     * Função responsável por retornar os atendentes vinculados a um subassunto,
     * usado na combo de atendente para encaminhar
     */
    public function retornarAtendenteEncaminharAction()
    {
        $this->_helper->json($this->negocio->findAtendenteCoEncaminhar($this->limpaGetAllParams($this->getAllParams())));
    }

    /**
     * Função responsável por encaminhar ocorrências em lote para outro Atendente
     * GII-9400
     *
     * @throws Exception
     */
    public function encaminharOcorrenciasEmLoteAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $params = $this->limpaGetAllParams($this->getAllParams());
        $idOcorrencias = $this->getParam('id_ocorrencia');

        if (count($this->getParam('id_ocorrencia')) > 1) {

            foreach ($this->getParam('id_ocorrencia') as $value) {
                $result = $this->negocio->encaminharOcorrenciasEmLote($params, (int)$value);
            }

            $this->_helper->json($result);
        }

        $this->_helper->json($this->negocio->encaminharOcorrenciasEmLote($params, (int)$idOcorrencias[0]));
    }

    /**
     * Função responsável por receber o parametro que vem do select id_funcao.
     * Foi necessário criar este método para formatar e recuperar apenas o id_nucleoco
     * Pois o value do select vem no formato 'id_funcao-id_nucleoco'.  Exemplo: 8-19
     *  GII-9400
     *
     * @param $string
     * @return mixed
     */
    public function retornarIdNucleoCo($string)
    {
        $idNucleo = explode('-', $string);
        return $idNucleo[1];
    }

}

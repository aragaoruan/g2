<?php

/**
 * Classe de controller para Contatos do Site
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2013-05-11
 */
use G2\Negocio\Situacao,
    G2\Negocio\Prevenda;

class Default_ContatosSiteController extends Ead1_ControllerRestrita
{

    /**
     * Prevenda Negocio
     * @var G2\Negocio\Prevenda
     */
    private $_negocio;

    public function init ()
    {
        parent::init();
        $this->_negocio = new Prevenda();
    }

    public function indexAction ()
    {
        $situacao = new Situacao();
        $atendente = new \G2\Negocio\VwAtendente();
        $this->view->arrSituacao = $situacao->retornaSituacaoFindBy(array('st_tabela' => 'tb_prevenda'));
        $this->view->atendentes = $atendente->findByAtendentePorEntidade();
    }

    /**
     * Action para js do backbone
     */
    public function jsAction ()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('contatos-site/index.js');
    }

    /**
     * Salva mensagens como lida
     * @param Ajax Post $params Parametros post
     * @return Json Return json array
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     * @update Helder Silva <helder.silva@unyleya.com.br>
     * @date 2013-11-08
     */
    public function saveReadMensagesAction ()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->limpaGetAllParams($this->getAllParams());
        if ($params) {

            $return = $this->_negocio->salvarByIdPrevendaProduto(
                $params['modelos'], \G2\Constante\Situacao::TB_PREVENDA_DISTRIBUIDA,
                $this->getParam('id_atendente', null)
            );
            if ($return) {
                $this->_helper->json($return);
            }
        } else {
            $this->_helper->json(array(
                'type' => 'error',
                'title' => 'Erro ao Tentar Atualizar Registros!',
                'text' => 'Houve um erro ao tentar atualizar registros, Parâmetros necessários não localizados.'
            ));
        }
    }

    /**
     * Salva mensagens como descartadas
     * @param Ajax Post $params Parametros post
     * @return Json Return json array
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     * @update Helder Silva <helder.silva@unyleya.com.br>
     * @date 2013-11-12
     */
    public function saveDiscardedMessagesAction ()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->limpaGetAllParams($this->getAllParams());
        if ($params) {
            $return = $this->_negocio->salvarByIdPrevendaProduto(
                $params['modelos'],
                (!empty($params['id_situacao']) ? $params['id_situacao'] :
                    \G2\Constante\Situacao::TB_PREVENDA_DESCARTADA),
                null,
                $params['st_descarte']
            );
            if ($return) {
                $this->_helper->json($return);
            }
        } else {
            $this->_helper->json(array(
                'type' => 'error',
                'title' => 'Erro ao Tentar Atualizar Registros!',
                'text' => 'Houve um erro ao tentar atualizar registros, Parâmetros necessários não localizados.'
            ));
        }
    }

    public function readInteracoesPrevendaAction()
    {
        $this->view->interacoes = $this->_negocio->retornarTramitesPreVenda($this->getParam('id_prevenda'));
    }

    /**
     * Gerar Xls para exportar para excel
     * @author Helder Fernandes <helder.silva@unyleya.com.br>
     * @date 2015-02-23
     */

    public function gerarXlsAction()
    {
        $arquivo = "relatorio_pre_matriculas_" . date('d-m-Y') . ".xls";

        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: application/x-msexcel');

        header("Content-Disposition: attachment; filename=" . $arquivo);


        $params['id_situacao'] = $this->getParam('id_situacao');
        $params['dt_inicio'] = $this->getParam('dt_inicio');
        $params['dt_fim'] = $this->getParam('dt_fim');
        $params['id_usuarioatendimento'] = $this->getParam('id_atendente');
        $params['dt_ultimainteracao'] = $this->getParam('dt_interacao');
        $params['st_nomecompleto'] = $this->getParam('st_nomecompleto', null);
        $params['busca'] = $this->getParam('busca', null);

        $return = $this->_negocio->returnPreVendas($params);

        $this->view->dados = $return;
    }
}

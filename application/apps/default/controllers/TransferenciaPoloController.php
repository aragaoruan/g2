<?php

class Default_TransferenciaPoloController extends Ead1_Controller
{
    /**
     * @var \G2\Negocio\Transferencia $negocio
     */
    private $negocio;

    public function init()
    {
        $this->negocio = new \G2\Negocio\Transferencia();
    }

    public function indexAction()
    {
    }

    public function salvarTransferenciaAction()
    {
        try {
            $data = $this->limpaGetAllParams($this->getAllParams());
            $this->negocio->salvarTransferenciaPolo($data);
            $response = array(
                'type' => 'success',
                'title' => 'Sucesso',
                'text' => 'A transferência foi realizada com sucesso.'
            );
        } catch (\Exception $e) {
            $response = array(
                'type' => 'error',
                'title' => 'Erro',
                'text' => $e->getMessage()
            );
            $this->getResponse()->setHttpResponseCode(500);
        }

        $this->_helper->json($response);
    }

}

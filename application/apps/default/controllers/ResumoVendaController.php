<?php

class Default_ResumoVendaController extends Ead1_ControllerRestrita
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {

    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
    }

    public function retornaRelatorioAction()
    {
        $this->negocio = new G2\Negocio\RelResumoVenda();

        $this->_helper->json($this->negocio->executaRelatorio($this->limpaGetAllParams($this->getAllParams())));
    }

    public function gerarXlsAction()
    {
        $this->negocio = new G2\Negocio\RelResumoVenda();
        $this->view->dados = $this->negocio->executaRelatorio($this->limpaGetAllParams($this->getAllParams()));

    }

}
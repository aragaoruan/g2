<?php

/**
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @date 2017-03-21
 */
class Default_TransferenciaTurmaController extends Ead1_Controller
{
    /**
     * @var \G2\Negocio\Transferencia $negocio
     */
    private $negocio;

    public function init()
    {
        $this->negocio = new \G2\Negocio\Transferencia();
    }

    public function indexAction()
    {
    }

    public function salvarTransferenciaTurmaAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $data = $this->limpaGetAllParams($this->getAllParams());

        $negocioMat = new \G2\Negocio\Matricula();

        $params = array('id_matricula' => $data['id_matricula']);
        $notasAgrupadas = $negocioMat->retornaVwGradeNotaAgrupada($params);

        try {
            $response = $this->negocio->transferirAlunoTurma($data);
            if (!($response instanceof Ead1_Mensageiro)) {
                throw new Exception('As notas e historico não foram agrupados na turma');
            }
            if ($response->getTipo()!== Ead1_IMensageiro::SUCESSO) {
                throw new Exception($response->getText());
            }

            $this->view->notasAgrupadas = $notasAgrupadas;
            $html = $this->view->render('matricula/historico-matricula-grade-nota.phtml');
            $negocioMat->salvarHistoricoMatriculaGradeNota($params, $html);
            $this->_helper->json($response);

        } catch (\Exception $e) {

            return $this->getResponse()->setHttpResponseCode(500)
                ->appendBody($e->getMessage());
        }

    }

}

<?php

/**
 * Classe de controller MVC para exibir a Ficha de Matrícula
 * @author Elcio Guimarães <elcioguimaraes@gmail.com>
 * @date
 * @package application
 * @subpackage controller
 */
class Default_FichaMatriculaController extends Ead1_ControllerRestrita {


    public function init() {
        parent::init();
        parent::setupDoctrine();
    }

    public function indexAction() {

        try {

            $id_venda = $this->getParam('id_venda', false);
            if($id_venda==false){
                die("Venda não informada.");
            }

            $pe = new \G2\Negocio\Pessoa();
            $v  = $pe->find('G2\Entity\Venda', $id_venda);

            $cvto = new VwClienteVendaTO();
            $cvto->setId_venda($v->getIdvenda());
            $cvto->setId_entidade($v->getIdEntidade());

            $e = new EntidadeTO();
            $e->setId_entidade($cvto->getId_entidade());
            $e->fetch(true, true, true);
            $this->view->en = $e->toArray();

            $bo = new FinanceiroBO();
            $cv = $bo->retornaVendasCliente($cvto);
            if($cv->getTipo()==\Ead1_IMensageiro::SUCESSO){
                $cvarr = $cv->getFirstMensagem();
                if($cvarr==false){
                    die("Dados não encontrados.");
                }

                $us = $pe->retornaDadosPessoa(array('id_usuario'=>$cvarr['id_usuario'], 'id_entidade'=>$cvarr['id_entidade']));

                if($us) {
                    $this->view->pe = $us[0];
                }
                $dtcadastro = new \Zend_Date($cvarr['dt_cadastro']);
                $cvarr['dt_cadastro'] = $dtcadastro->toString("dd/MM/yyyy");
                $dtconfirmacao = $v->getDtConfirmacao()->format('d/m/Y');//$pe->converterData($v->getDtConfirmacao(), 'dd/MM/yyyy');

                $this->view->cv = array_merge($cvarr, array(

                    'st_origemvenda'=> ($v->getIdorigemvenda() ? $v->getIdorigemvenda()->getSt_origemvenda() : 'Não informado.'),
                    'id_atendente'=> ($v->getid_atendente() ? $v->getid_atendente()->getid_usuario() : '000'),
                    'st_atendente'=> ($v->getid_atendente() ? $v->getid_atendente()->getSt_nomecompleto() : 'Não informado.'),
                    'dt_confirmacao' => $dtconfirmacao,

                ));
                $artramites = null;
                $dt = new \TramiteDAO();
                $tramr  =  $dt->retornarTramiteSP(\CategoriaTramiteTO::VENDA,array($id_venda) );
                if($tramr){
                    foreach($tramr as $tramite){
                        $temp = new \SpTramiteTO($tramite);
                        $artramites[] = $temp->toBackboneArray();
                    }
                }
                $this->view->in = $artramites;

            } else {
                die($cv->getFirstMensagem());
            }

        } catch(\Exception $e){
            die($e->getMessage());
        }

    }
    

}

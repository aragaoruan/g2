<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TipoOcorrenciaController
 *
 * @author rafael.bruno
 */
class Default_TipoOcorrenciaController extends Ead1_ControllerRestrita {
    
    public function indexAction(){
        $negocioTipoOcorrencia = new G2\Negocio\Ocorrencia();
        $negocioSituacao = new G2\Negocio\Situacao();

        $this->view->comboTipoOcorrencia = $negocioTipoOcorrencia->findAll('G2\Entity\TipoOcorrencia');
        $this->view->comboSituacao = $negocioSituacao->findBy('G2\Entity\Situacao', array('st_tabela' => 'tb_categoriaocorrencia'));
    }
    
    public function jsAction(){
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render($this->_getParam('controller').'/index.js');
    }
    
}
<?php

/**
 * Description of FolhaDeNumerosTurmaXMesController
 *
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class Default_FolhaDeNumerosTurmaXMesController extends Ead1_ControllerRestrita
{
    private $ProjetoNegocio;
    private $TurmaNegocio;
    private $FolhaNegocio;

    public function init()
    {
        parent::init();
        $this->ProjetoNegocio = new \G2\Negocio\ProjetoPedagogico();
        $this->TurmaNegocio = new \G2\Negocio\Turma();
        $this->FolhaNegocio = new \G2\Negocio\FolhaDeNumerosTurmaXMes();
    }

    public function indexAction()
    {

    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
    }

    /**
     * Retorna todas as holdings onde a entidade esta presente.
     */
    public function retornaHoldingAction(){
        $params = $this->limpaGetAllParams($this->getAllParams());
        $retorno = $this->FolhaNegocio->findHolding($params);
        $arrRetorno = array();
        foreach($retorno as $key=>$value){
            $arrRetorno[$key]['id_holding'] = $value->getId_holding();
            $arrRetorno[$key]['st_holding'] = $value->getSt_holding();
        }
        $this->_helper->json($arrRetorno);
    }

    /**
     * Retorna todas as entidades que estão presentes na holding selecionada
     */
    public function retornaEntidadeHoldingAction(){
        $params = $this->limpaGetAllParams($this->getAllParams());
        $retorno = $this->FolhaNegocio->findEntidadeHolding($params);
        $arrRetorno = array();
        foreach($retorno as $key=>$value){
            $arrRetorno[$key]['id_entidade'] = $value->getId_entidade();
            $arrRetorno[$key]['st_nomeentidade'] = $value->getSt_nomeentidade();
        }
        $this->_helper->json($arrRetorno);
    }

    /**
     * Retorna todos os projetos pedagógicos da entidade selecionada
     */
    public function getProjetosPedagogicosAction(){
        $params = $this->limpaGetAllParams($this->getAllParams());
        $retorno = $this->FolhaNegocio->findVwProjetoEntidade($params);
        $this->_helper->json($retorno);
    }

    /**
     * Retorna todas as Turmas do Projeto Pedagogico selecionado
     */
    public function getTurmaProjetoPedagogicoAction(){
        $params = $this->limpaGetAllParams($this->getAllParams());
        $retorno = $this->FolhaNegocio->findVwTurmaProjetoPedagogico($params);
        $arrRetorno = array();
        foreach($retorno as $key=>$value){
            $arrRetorno[$key]['id_turma'] = $value->getId_turma();
            $arrRetorno[$key]['st_turma'] = $value->getSt_turma();
        }
        $this->_helper->json($arrRetorno);
    }

    /**
     * Executa a consulta passando todos os filtros
     */
    public function getRelatorioAction(){
        $params = $this->limpaGetAllParams($this->getAllParams());
        $retorno = $this->FolhaNegocio->getRelatorio($params);


        $chave = array();

        foreach($retorno as $result){
            if(array_key_exists($result['id_projetopedagogico'], $chave)){
                array_push($chave[$result['id_projetopedagogico']]['vendas'], $result);
            }else{
                $chave[$result['id_projetopedagogico']]= array(
                    'id_projetopedagogico' => $result['id_projetopedagogico'],
                    'st_projetopedagogico' => $result['st_projetopedagogico'],
                    'vendas' => array(
                        0 =>$result
                    )
                );
            }
        }
        $arrCoisa = [];
        foreach ($chave as $arr) {
            $arrCoisa[] = $arr;
        }

        /**
         * Esse trecho executa a verificação de tipo da requisição, se for do tipo xls ou html
         * é enviado para a view getRelatorio para ser impressa em arquivo ou html, se não ele
         * retorna para o index.js para ser impresso na tela.
         */
        if(array_key_exists('tipo',$params) && ($params['tipo'] == 'xls' || $params['tipo'] == 'html')){
            $this->view->dados = $chave;
            $this->view->tipo = $params['tipo'];
        }else{
            $this->_helper->json($arrCoisa);
        }

    }



}
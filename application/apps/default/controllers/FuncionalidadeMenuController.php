<?php

/**
 * Controller para edicao de Funcionalidade (Item de Menu)
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2015-03-19
 * @package application
 * @subpackage controller
 */


class Default_FuncionalidadeMenuController extends Ead1_RestRestrita
{
    private $negocio;

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('funcionalidade-menu/index.js');
    }

    public function init()
    {
        $this->negocio = new \G2\Negocio\Funcionalidade();
    }

    public function indexAction()
    {
    }

    public function getFuncionalidadesBreadcrumbAction()
    {
        $params = $this->getAllParams();
        $orderBy = $this->getParam('order_by') ? array($this->getParam('order_by') => $this->getParam('sort') ? $this->getParam('sort') : 'ASC') : null;
        unset($params['module'], $params['controller'], $params['action'], $params['order_by'], $params['sort']);

        $mensageiro = $this->negocio->findBy('\G2\Entity\Funcionalidade', $params, $orderBy);
        $results = array();
        foreach ($mensageiro as $entity) {
            if ($entity->getId_funcionalidadepai()) {
                $pai = $this->negocio->findFuncionalidade($entity->getId_funcionalidadepai()->getId_funcionalidade());
                $entity->setSt_funcionalidade($pai->getSt_funcionalidade() . ' » ' . $entity->getSt_funcionalidade());
            }
            $results[$entity->getSt_funcionalidade()] = $this->negocio->toArrayEntity($entity);
        }

        ksort($results);
        $this->_helper->json(array_values($results));
    }

}
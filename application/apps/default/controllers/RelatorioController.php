<?php

use G2\Negocio\Entidade;

/**
 * Controller de geração de relatórios
 * @author Arthur Cláudio
 * @package application
 * @subpackage controller
 */
class Default_RelatorioController extends Ead1_ControllerRestrita
{

    /**
     * @var \G2\Negocio\Negocio
     */
    private $negocio;

    /**
     * @var \RelatorioBO
     */
    private $relatorioBo;

    public function init ()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Negocio();
        $this->relatorioBo = new RelatorioBO();
    }

    /**
     * Gera um pdf a partir dos dados enviados
     *
     */
    public function disciplinasXlsAction ()
    {
        $xls = new Ead1_GeradorXLS();
        $sessao = new Zend_Session_Namespace('geral');
        $loginBO = new LoginBO();
        $mensageiro = $loginBO->retornaDadosUsuario($sessao->id_usuario, $sessao->id_entidade, $sessao->id_perfil);
        if ($mensageiro->getCodigo() == Ead1_IMensageiro::AVISO) {
            echo 'Erro ao gerar o documento. Tente novamente por favor';
            die();
        }

        $usuarioTO = $mensageiro->getMensagem();
        $usuarioTO = $usuarioTO[0];

        $id_turma = $this->getRequest()->getParam('id_turma');
        $id_projeto = $this->getRequest()->getParam('id_projeto');
        $id_user = $sessao->id_entidade;

        $xlsObj = new VwConferenciaTurmasTO();
        $bo = new TurmaBO();
        $xlsObj->set_idEntidade($id_user);
        $xlsObj->set_idTurma($id_turma);
        $xlsObj->set_idProjetopedagogico($id_projeto);
        $retornaBO = $bo->retornarConferenciaTurmas($xlsObj);

        $obj = $retornaBO->mensagem;

        $this->view->disciplinas = $obj;
    }


    public function gerarXlsEncFinAction(){

        $this->ajax();
        $dados = $this->getAllParams();
        unset($dados['controller'], $dados['module'], $dados['action']);

        $liberacao                  = null;
        $id_categoriasala           = null;
        $id_tipodisciplina          = null;
        $id_professordisciplina     = null;
        $bl_recusado                = null;

        foreach($dados['parametros'] as $chave => $valor){
            $$chave = $valor;
        }

        $salabo   = new SalaDeAulaBO();
        $arReturn = json_encode($salabo->retornarEncerramento($liberacao, $id_categoriasala, $id_tipodisciplina, $id_professordisciplina, $bl_recusado));
        $dados['converterUtf8'] = $dados['converterUtf8'] == 'true' ? true : false;

        $bo = new RelatorioBO();
        $bo->gerarXls($dados['cabecalho'], $arReturn, false, false, $dados['converterUtf8'], false);

    }


    public function gerarXlsAction ()
    {
        $this->ajax();

        $converterUtf = $this->getRequest()->getParam('converterUtf8');
        $converterEnconding = $this->getRequest()->getParam('converterEnconding', true);

        $xls = new Ead1_GeradorXLS();
        $sessao = new Zend_Session_Namespace('geral');
        $loginBO = new LoginBO();
        $mensageiro = $loginBO->retornaDadosUsuario($sessao->id_usuario, $sessao->id_entidade, $sessao->id_perfil);
        if ($mensageiro->getCodigo() == Ead1_IMensageiro::AVISO) {
            echo 'Erro ao gerar o documento. Tente novamente por favor';
            exit;
        }

        $usuarioTO = $mensageiro->getMensagem();
        $usuarioTO = $usuarioTO[0];

        $dadosCabecalho = $this->getRequest()->getParam('cabecalho');
        $dadosCorpo = $this->getRequest()->getParam('dados');
        $dadosCorpo = htmlentities($dadosCorpo, ENT_NOQUOTES);
        $dadosCabecalho = htmlentities($dadosCabecalho, ENT_NOQUOTES);

        if ($converterUtf == true) {
            $dadosCabecalho = utf8_encode($dadosCabecalho);
            $dadosCorpo = utf8_encode($dadosCorpo);
        }

        $cabecalho = json_decode($dadosCabecalho, true);
        $dados = json_decode($dadosCorpo, true);

        $arXlsHeaderTO = array();
        foreach ($cabecalho as $index => $colunaCabecalho) {
            $xlsHeaderTO = new XLSHeaderTO();
            $xlsHeaderTO->setSt_header($colunaCabecalho);
            $xlsHeaderTO->setSt_par($index);
            $arXlsHeaderTO[] = $xlsHeaderTO;
        }

        $arTO = Ead1_TO_Dinamico::encapsularTo($dados, new ParametroRelatorioTO(), false, false, $converterUtf);

        $x = new XLSConfigurationTO();
        $titulo = $this->getRequest()->getParam('titulo');
        $filename = $this->getRequest()->getParam('filename');
        // $x->setTitle((!empty($titulo) ? $titulo : 'Relat&oacute;rio de Pesquisa '));
        // $x->setFooter('Gerado por: ' . $usuarioTO->st_nomecompleto . ' - ' . $usuarioTO->st_cpf . ' Data: ' . date('d/m/Y H:i:s'));
        $x->setFilename((!empty($filename) ? $filename : 'relatorio'));

        if ($this->getParam('type') == 'html') {
            $entidade = $this->negocio->find('\G2\Entity\Entidade', $this->negocio->sessao->id_entidade);

            foreach ($cabecalho as $name => &$label) {
                $label = array(
                    'name' => $name,
                    'label' => $label
                );
            }

            //Seta os parametros para gerar a tabela na view
            $this->view->nomeEntidade = $entidade->getSt_nomeentidade();
            $this->view->imgEntidade = $entidade->getSt_urlimglogo();
            $this->view->nomeRelatorio = $titulo;
            $this->view->cabecalhoDadosRelatorio = $cabecalho;
            $this->view->nomeUsuario = $this->relatorioBo->sessao->nomeUsuario;
            $this->view->qtdRegistros = count($dados);
            $this->view->dataGeracao = date("d/m/Y à\s H:i:s");
            $this->view->dadosRelatorio = $dados;

            echo $this->view->render('relatorio/impressao.phtml');
        } else {
            $xls->geraXLS($arXlsHeaderTO, $arTO, $x, $converterEnconding);
        }
    }

    public function gerarPdfAction ()
    {
        $this->ajax();

        $sessao = new Zend_Session_Namespace('geral');
        $loginBO = new LoginBO();
        $mensageiro = $loginBO->retornaDadosUsuario($sessao->id_usuario, $sessao->id_entidade, $sessao->id_perfil);

        if ($mensageiro->getCodigo == Ead1_IMensageiro::AVISO) {
            echo 'Erro ao gerar o documento. Tente novamente por favor';
            exit;
        }

        $usuarioTO = $mensageiro->getMensagem();
        $usuarioTO = $usuarioTO[0];

        $xls = new Ead1_GeradorXLS();

        $cabecalho = json_decode($this->getRequest()->getParam('cabecalho'), true);
        $dados = json_decode($this->getRequest()->getParam('dados'), true);

        ksort($dados);
        ksort($cabecalho);

        $arXlsHeaderTO = array();
        foreach ($cabecalho as $index => $colunaCabecalho) {
            $xlsHeaderTO = new XLSHeaderTO();
            $xlsHeaderTO->setSt_header($colunaCabecalho);
            $xlsHeaderTO->setSt_par($index);
            $arXlsHeaderTO[] = $xlsHeaderTO;
        }
        $arTO = Ead1_TO_Dinamico::encapsularTo($dados, new ParametroRelatorioTO());

        $titulo = $this->getRequest()->getParam('titulo');
        $filename = $this->getRequest()->getParam('filename');

        $x = new XLSConfigurationTO();
        $x->setTitle((!empty($titulo) ? $titulo : 'Relatório de Pesquisa '));
        $x->setFooter('Gerado por: ' . $usuarioTO->st_nomecompleto . ' - ' . $usuarioTO->st_cpf . ' Data: ' . date('d/m/Y H:i:s'));
        $x->setFilename((!empty($filename) ? $filename : 'relatorio'));

        $x->setHtml($xls->geraHTML($arXlsHeaderTO, $arTO, $x));
        $html = $xls->montaHTMLCabecalho($x);


        $ead1PDF = new Ead1_Pdf();
        $ead1PDF->carregarHTML($html);
        $ead1PDF->configuraPapel($x->getTipoPapel(), $x->getOrientacao());
        $ead1PDF->gerarPdf($x->getFilename() . '.pdf');
    }

    public function gerarLivroRegistroAction ()
    {
        try {
            $this->ajax();

            $mensagem = array();
            $lreTO = new LivroRegistroEntidadeTO();
            $lreTO->setId_livroregistroentidade($this->_getParam('id_livroregistroentidade'));
            $livroRegistroBO = new LivroRegistroBO();
            $mensageiro = $livroRegistroBO->retornarLivroRegistroEntidade($lreTO);
            if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $mensagem = $mensageiro->getMensagem();
                $lreTO = $mensagem[0];
            } else {
                throw new Zend_Exception('Livro não encontrado!');
            }

            $registroTO = new LivroRegistroTO();
            $registroTO->setId_livroregistroentidade($lreTO->getId_livroregistroentidade());
            $registroTO->setNu_livro($this->getRequest()->getParam('nu_livro'));
            $matriculaBO = new MatriculaBO();
            $where = 'nu_folha between ' . $this->getRequest()->getParam('nu_folhainicial') . ' and ' . $this->getRequest()->getParam('nu_folhafinal');
            $mensageiro = $matriculaBO->retornarLivroRegistro($registroTO, $where);
            if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $mensagem = $mensageiro->getMensagem();
                if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception($mensagem[0]);
                }
            } else {
                throw new Zend_Exception('Nenhum registro encontrado para esse livro!');
            }

            $htmlFrente = '';
            $htmlVerso = '';

            $textosBO = new TextosBO();

            $nu_registro = 0;

            $responsavelLegalBO = new ResponsavelLegalBO();
            $mensagemResponsaveis = $responsavelLegalBO->retornarEntidadeResponsavelLegal(new VwResponsavelLegalTO());
            if ($mensagemResponsaveis->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $arrResponsaveis = $mensagemResponsaveis->getMensagem();
                foreach ($arrResponsaveis as $responsavel) {
                    if ($responsavel->getId_tipoentidaderesponsavel() == ResponsavelLegalBO::DIRETOR && $responsavel->getBl_padrao()) {
                        $this->view->nomeresponsavel = $responsavel->getSt_nomeexibicao();
                        $this->view->registroresponsavel = $responsavel->getSt_registro();
                        $this->view->cargoresponsavel = $responsavel->getSt_tipoentidaderesponsavel();
                    }
                }
            } else {
                $mensagemExcessao = $mensagemResponsaveis->getMensagem();
                throw new Zend_Exception($mensagemExcessao[0]);
            }

            $htmlLivro = $this->view->render("relatorio/registro-livro-cabecalho.html");
            $primeiraPagina = true;
            foreach ($mensagem as $registro) {
                $vwgdTO = new VwGerarDeclaracaoTO();
                $vwgdTO->setId_matricula($registro->getId_matricula());

                $mensageiro = $textosBO->retornarVwGerarDeclaracao($vwgdTO);
                $mensagem = $mensageiro->getMensagem();
                $vwgdTO = $mensagem[0];
                $dt_conluintealuno = new Zend_Date($vwgdTO->getDt_concluintealuno());
                $dt_nascimentoaluno = new Zend_Date($vwgdTO->getDt_nascimentoaluno());
                $dt_dataexpedicaoaluno = new Zend_Date($vwgdTO->getDt_dataexpedicaoaluno());
                $this->view->nu_registro = $registro->getNu_registro();
                $this->view->st_nivelensino = $vwgdTO->getSt_nivelensino();
                $this->view->id_matricula = $vwgdTO->getId_matricula();
                $this->view->dt_conclusao = $dt_conluintealuno->toString('dd/MM/yyyy');
                $this->view->st_nomealuno = $vwgdTO->getSt_nomecompletoaluno();
                $this->view->dt_nascimentoaluno = $dt_nascimentoaluno->toString('dd/MM/yyyy');
                $this->view->st_rgaluno = $vwgdTO->getSt_rgaluno();
                $this->view->st_orgaoexpeditor = $vwgdTO->getSt_orgaoexpeditor();
                $this->view->dt_dataexpedicaoaluno = $dt_dataexpedicaoaluno->toString('dd/MM/yyyy');
                $this->view->st_ufaluno = $vwgdTO->getSt_ufaluno();
                $this->view->st_municipioaluno = $vwgdTO->getSt_municipioaluno();
                $date = new Zend_Date(null, Zend_Date::ISO_8601);
                $this->view->dt_hoje = $date->toString('dd/MM/yyyy');

                $htmlFrente .= $this->view->render("relatorio/registro-livro-registro-frente.phtml");
                $htmlVerso .= $this->view->render("relatorio/registro-livro-registro-verso.phtml");
                $nu_registro++;
                if ($nu_registro == $lreTO->getNu_registros()) {
                    if (!$primeiraPagina) {
                        $htmlLivro .= '<div style="page-break-before: always;"></div>';
                    }
                    $primeiraPagina = false;
                    $htmlLivro .= $htmlFrente . '<div style="page-break-before: always;"></div>' . $htmlVerso;
                    $htmlFrente = '';
                    $htmlVerso = '';
                    $nu_registro = 0;
                }
            }
            if (!$primeiraPagina) {
                $htmlLivro .= '<div style="page-break-before: always;"></div>';
            }
            $primeiraPagina = false;
            $htmlLivro .= $htmlFrente . '<div style="page-break-before: always;"></div>' . $htmlVerso;
            $htmlLivro .= "</body> </html>";

            echo $htmlLivro;

//			$ead1PDF	= new Ead1_Pdf();
//			$ead1PDF->carregarHTML( $htmlLivro );
//			//$ead1PDF->configuraPapel( $x->getTipoPapel(),$x->getOrientacao() );
//			$ead1PDF->gerarPdf( $lreTO->getSt_livroregistroentidade().'.pdf' );
        } catch (Zend_Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Método para view de geração de relatório no modo impressão ou xls
     */
    public function impressaoAction ()
    {
        //Isntancia Entidade Negocio
        $entidadeNegocio = new Entidade();

        //Recuperar os parametros
        $params = $this->_request->getParams();
        $id_funcionalidade = $params['id_funcionalidade'];
        $qtdPorPagina = $params['qntPorPagina'];
        $xls = (isset($params['xls']) ? $params['xls'] : false);
        if($xls)
            $qtdPorPagina = 999999;

        //Unset nos index do array descnecessarios
        unset($params['qntPorPagina'], $params['id_funcionalidade'], $params['controller'], $params['action'], $params['module'], $params['xls']);

        //Busca os dados da entidade
        $entidade = $entidadeNegocio->findEntidade($this->relatorioBo->sessao->id_entidade);
        $ordenacao = ($this->_getParam('ordenacao') == "null" ? null : $this->_getParam('ordenacao'));

        //Busca as informações do relatorio e os dados
        $dadosRelatorio = $this->relatorioBo->retornarDadosRelatorio(null, $params, null, $qtdPorPagina, true, $id_funcionalidade, $ordenacao, $xls);

        /**
         * @history AC-26692
         * @update 02/02/2015
         * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
         * @description Ordenar os dados de acordo com o primeiro campo (index)
         */
        // Fazer a ordenacao (simular order by) pelo primeiro campo
        $resultsRelatorio = $dadosRelatorio['dados'];
        if ($resultsRelatorio) {
            // Descobrir qual o primeiro campo
            $orderIndexes = array_keys(current($resultsRelatorio));
            $orderBy = array_shift($orderIndexes);
            foreach ($orderIndexes as $key => $value) {
                if (substr($value, 0, 3) == 'st_') {
                    $orderBy = $value;
                    break;
                }
            }

            // Atribuir o valor do campo descoberto ao indice de cada item no array
            $dadosRelatorio['dados'] = array();
            foreach ($resultsRelatorio as $key => $result) {
                // Removendo acentos
                $index = strtolower(preg_replace('/[`^~\'"]/', null, iconv('UTF-8', 'ASCII//TRANSLIT', $result[$orderBy])));
                $dadosRelatorio['dados'][$index . $key] = $result;
            }

            // Finalmente ordernar pelo indice definido acima
            ksort($dadosRelatorio['dados']);
            // Entao, reindexar o array (para indices numericos)
            $dadosRelatorio['dados'] = array_values($dadosRelatorio['dados']);
        }

        /**
         * SUBSTITUI CARACTERES INVÁLIDOS
         */
        // Transformar os dados em uma string pra fazer o replace
        $dadosRelatorio['dados'] = json_encode($dadosRelatorio['dados']);
        $replace_from_to = array(
            '\u201c' => '"',
            '\u201d' => '"',
        );
        foreach ($replace_from_to as $char => $to) {
            $dadosRelatorio['dados'] = str_replace($char, addslashes($to), $dadosRelatorio['dados']);
        }
        // Apos o replace, transformar os dados novamente em um array
        $dadosRelatorio['dados'] = json_decode($dadosRelatorio['dados'], true);

        //Se for xls
        if ($xls) {
            //Instancia o gerador do xls
            $xls = new Ead1_GeradorXLS();
            $arrParametroTO = Ead1_TO_Dinamico::encapsularTo($dadosRelatorio['dados'], new ParametroRelatorioTO());
            $arXlsHeaderTO = array();
            foreach ($dadosRelatorio['grid'] as $colunaCabecalho) {
                $xlsHeaderTO = new XLSHeaderTO();
                $xlsHeaderTO->setSt_header(mb_convert_encoding($colunaCabecalho['label'],'ISO-8859-1','auto'));
                $xlsHeaderTO->setSt_par($colunaCabecalho['name']);
                $arXlsHeaderTO[] = $xlsHeaderTO;
            }
            $x = new XLSConfigurationTO();
            $x->setTitle(mb_convert_encoding($dadosRelatorio['relatorio']->getSt_relatorio(),'ISO-8859-1','auto'));
//            $x->setFooter('Gerado por: ' . strtoupper($this->relatorioBo->sessao->nomeUsuario) . ' Data: ' . date('d/m/Y H:i:s'));
            $x->setFooter('');
            $x->setFilename('relatorio');
            $x->setTitleFontSize('11pt');
            $x->setBodyFontSize('11pt');
            $x->setHeaderFontSize('11pt');
            $x->setFooterFontSize('11pt');
            $x->setStripedBgColor1(false);
            $x->setStripedBgColor2(false);
            $xls->geraXLS($arXlsHeaderTO, $arrParametroTO, $x,false );
            exit;
        } else {

            //Seta os parametros para gerar a tabela na view
            $dadosPaginacao = $dadosRelatorio['paginacao'];
            $this->view->nomeEntidade = $entidade->getSt_nomeentidade();
            $this->view->imgEntidade = $entidade->getSt_urlimglogo();
            $this->view->nomeRelatorio = $dadosRelatorio["relatorio"]->getSt_relatorio();
            $this->view->cabecalhoDadosRelatorio = $dadosRelatorio["grid"];
            $this->view->nomeUsuario = $this->relatorioBo->sessao->nomeUsuario;
            $this->view->qtdRegistros = $dadosPaginacao['qtdRegistrosTotal'];
            $this->view->dataGeracao = date("d/m/Y à\s H:i:s");
            $this->view->dadosRelatorio = $dadosRelatorio['dados'];
            $this->view->arrFiltros = $this->relatorioBo->trataArrayFiltroParaExibirDados($dadosRelatorio['filtros'], $params);
        }
    }

    /**
     * Relatorio Action, monta a tela de relatório com filtros
     */
    public function masterRelatorioAction ()
    {
        $relatorioBo = new RelatorioBO();

        // Buscar os parametros
        $idFuncionalidade = $this->getParam('funcionalidade');
        $params['id_funcionalidade'] = $idFuncionalidade;
        $params['bl_filtro'] = true;

        // Recuperar os dados da BO
        $return = $relatorioBo->retornaCamposRelatorios($params);
        $this->view->fields = array();

        if ($return) {
            $return['filtros'] = $return;
            $return['filtros']['id_funcionalidade'] = $idFuncionalidade;

            $this->view->form = new RelatorioForm($return['filtros']);

            foreach ($this->view->form->displayGroup as $element) {
                $this->view->fields[] = $element;
            }
        } else {
            $this->view->erro = 'Houve um erro ao montar o relatório.';
        }
    }

    /**
     * Action para recuperar informações do relatorio dinamico
     */
    public function gerarRelatorioAction ()
    {
        $relatorioBo = new RelatorioBO();
        //Recupera o request
        $request = $this->limpaGetAllParams($this->getRequest()->getParams());
        //Verifica se é um post
        //if ($request->isPost()) {
            //Recupera os parametros do post
            $params = $request;
            $id_funcionalidade = $params['id_funcionalidade'];
            $limit = (int)$this->getParam('per_page');
            $page = (int)$this->getParam('page');
            $orderBy = $this->getParam('sort_by');
            unset($params['qntPorPagina'], $params['btnPesquisar'], $params['id_funcionalidade']);

            if ($orderBy) {
                $sort = $this->getParam('order');
                if ($sort) {
                    $orderBy .= ' ' . $sort;
                }
            }

            //Envia para a BO
            $return = $relatorioBo->retornarDadosRelatorio(null, $params, $page, $limit, true, $id_funcionalidade, $orderBy);

            /**
             * @history AC-26692
             * @update 02/02/2015
             * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
             * @description Ordenar os dados de acordo com o primeiro campo (index)
             */
            // Fazer a ordenacao (simular order by) pelo primeiro campo
            // Somente se o parametro de ORDER BY não tiver sido enviado
            $resultsRelatorio = $return['dados'];
            if ($resultsRelatorio && !$orderBy) {
                // Descobrir qual o primeiro campo do tipo String (st_)
                $orderIndexes = array_keys(current($resultsRelatorio));
                $orderBy = array_shift($orderIndexes);
                foreach ($orderIndexes as $key => $value) {
                    if (substr($value, 0, 3) == 'st_') {
                        $orderBy = $value;
                        break;
                    }
                }

                // Atribuir o valor do campo descoberto ao indice de cada item no array
                $return['dados'] = array();
                foreach ($resultsRelatorio as $key => $result) {
                    // Removendo acentos
                    $index = strtolower(preg_replace('/[`^~\'"]/', null, iconv('UTF-8', 'ASCII//TRANSLIT', $result[$orderBy])));
                    $return['dados'][$index . $key] = $result;
                }

                // Finalmente ordernar pelo indice definido acima
                ksort($return['dados']);
                // Entao, reindexar o array (para indices numericos)
                $return['dados'] = array_values($return['dados']);
            }

            //Trata o retorno
            $paginacao = $return['paginacao'];
            $limit =(int)$paginacao['qtdRegistrosPorPagina'];
            $returnArr['offset'] = (int)($page-1) * $limit;
            $returnArr['limit'] = $limit;
            $returnArr['page'] = $paginacao['pagina'];
            $returnArr['total'] = (int)$paginacao['qtdRegistrosTotal'];
            $returnArr['columns'] = $return['grid'];
            $returnArr['rows'] = $return['dados'];
            //Retorna o json
            $this->_helper->json($returnArr);
        //}
    }


    /**
     * Recupera dos dados de um select dinamico e retorna json
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     */
    public function getDadosSelectDinamicoAction ()
    {
        $this->_helper->viewRenderer->setNoRender();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $params = $request->getPost();
            $return = $this->relatorioBo->getDadosSelectDinamico($params['idCampoRelatorio'], $params['valueParentField']);
            $this->_helper->json($return);
        }
    }

    public function gerarXlsCursoAction ()
    {


        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $vw = new VwEncerramentoSalasCursoTO();
        $sc = new SecretariaBO();

        $cabecalho = $this->getParam('cabecalho');
        $tipo_liberacao = $this->getParam('tipo_liberacao');
        $id_categoriasala = $this->getParam('id_categoriasala');
        $id_tipodisciplina = $this->getParam('id_tipodisciplina');
        $id_professor = $this->getParam('id_professor');

        $vw->setId_entidade($_SESSION['geral']['id_entidade']);

        $filtros = array(
            'idUsuarioProfessor' => $id_professor,
            'tipoEncerramento' => $tipo_liberacao,
            'idTipoDisciplina' => $id_tipodisciplina
        );

        $return = $sc->retornarVwEncerramentoSalasCurso_Financeiro($vw, $filtros);

        $arReturn = $return->getMensagem();
        $arTipo = $return->getTipo();

        if ($arTipo == 1) {
            foreach ($arReturn as $k => $vw) {
                if ($vw->getDt_encerramentocoordenador()) {
                    $vw->setDt_encerramentocoordenador($vw->getDt_encerramentocoordenador()->toString('dd/MM/Y'));
                } else {
                    $vw->setDt_encerramentocoordenador('');
                }

                if ($vw->getDt_encerramentofinanceiro()) {
                    $vw->setDt_encerramentofinanceiro($vw->getDt_encerramentofinanceiro()->toString('dd/MM/Y'));
                } else {
                    $vw->setDt_encerramentofinanceiro('');
                }

                if ($vw->getDt_encerramentopedagogico()) {
                    $vw->setDt_encerramentopedagogico($vw->getDt_encerramentopedagogico()->toString('dd/MM/Y'));
                } else {
                    $vw->setDt_encerramentopedagogico('');
                }

                if ($vw->getDt_encerramentoprofessor()) {
                    $vw->setDt_encerramentoprofessor($vw->getDt_encerramentoProfessor()->toString('dd/MM/Y'));
                } else {
                    $vw->setDt_encerramentoprofessor('');
                }

                $arReturn[$k] = $vw->toArray();
            }
            //        $returnMensagemJson = Zend_Json::encode($arReturn);
            //Instancia o gerador do xls
            $xls = new Ead1_GeradorXLS();
            $arrParametroTO = Ead1_TO_Dinamico::encapsularTo($arReturn, new ParametroRelatorioTO());
            $arXlsHeaderTO = array();
            $arXlsHeaderTO = array();
            $dadosCabecalho = json_decode($cabecalho, true);
            foreach ($dadosCabecalho as $index => $colunaCabecalho) {
                $xlsHeaderTO = new XLSHeaderTO();
                $xlsHeaderTO->setSt_header($colunaCabecalho);
                $xlsHeaderTO->setSt_par($index);
                $arXlsHeaderTO[] = $xlsHeaderTO;
            }
            $x = new XLSConfigurationTO();
            $x->setTitle('Pagamento de Professor por Curso');
            $x->setFooter('Gerado por: ' . $this->relatorioBo->sessao->nomeUsuario . ' Data: ' . date('d/m/Y H:i:s'));
            $x->setFilename('relatorio');
            $xls->geraXLS($arXlsHeaderTO, $arrParametroTO, $x, false);
            exit;
        }
        return false;
    }

    public function downloadCensoAction()
    {
        $censoNegocio = new \G2\Negocio\Censo();

        $this->view->entidades = $censoNegocio->getNomesPolos();
        //$this->view->produtos = $censoNegocio->getNomesCursos();
        $this->view->anosMatricula = $censoNegocio->getAnosComMatricula();
        $this->view->anosCenso = $censoNegocio->getAnosCensoPopulados();
    }

    public function contagemPesquisaCensoAction()
    {
        $censoNegocio = new \G2\Negocio\Censo();
        $this->_helper->json(
            $censoNegocio->contagemDadosPesquisa($this->limpaGetAllParams($this->getAllParams()))
        );
    }

    public function downloadArquivoCensoAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        $censoNegocio = new \G2\Negocio\Censo();
        return $censoNegocio->gerarArquivoAluno($this->_request->getParams());
    }

}

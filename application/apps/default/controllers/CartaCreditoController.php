<?php
/**
 * Classe Controller para Carta de Credito
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */

class Default_CartaCreditoController extends Ead1_ControllerRestrita{

    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\CartaCredito();
    }

    public function indexAction(){

    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
    }

    /**
     * Retorna as cartas referentes ao usuário
     * @return mixed
     */
    public function retornaCartasUsuarioAction(){
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocio->retornaAllCartasCredito($params);
        $arrResult = array();
        foreach($result as $res){
            $obj = $this->negocio->toArrayEntity($res);
            $obj['dt_cadastro'] = date ("d/m/Y",strtotime($obj['dt_cadastro']));
            $obj['dt_validade'] = date ("d/m/Y",strtotime($obj['dt_validade']['date']));
            $obj['nu_valordisponivel'] = number_format($obj['nu_valordisponivel'],2,'.','');
            $obj['nu_valororiginal'] = number_format($obj['nu_valororiginal'],2,'.','');
            $obj['nu_valorutilizado'] = number_format($obj['nu_valorutilizado'],2,'.','');
            $obj['modificada'] = ($obj['nu_valordisponivel'] != $obj['nu_valororiginal'] ? 'disabled' : '');
            $obj['todaUtilizada'] = $obj['nu_valordisponivel'] == '0.00' ? 'disabled' : '';
            array_push($arrResult, $obj);
        }
        return $this->_helper->json($arrResult);
    }

    /**
     * retorna as holdings compartilhadas pela entidade da carta baseada nos parâmetros.
     * @return mixed
     */
    public function retornaEntidadeCompartilhadaCartaAction(){
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocio->retornaEntidadesHoldingsParameters($params);
        return $this->_helper->json($result);
    }

    /**
     * Salva a carta de crédito.
     * @return mixed
     */
    public function saveAction(){
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocio->saveCartaCredito($params);
        return $this->_helper->json($result);
    }
}
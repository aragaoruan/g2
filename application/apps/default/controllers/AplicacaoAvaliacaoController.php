<?php

use G2\Negocio\AplicacaoAvaliacao;

/*
 * Cadastrar uma Aplicacao para Avaliacao (controlador default) - (Local de prova para agendamento)
 * @autor: Débora Castro <debora.castro@unyleya.com.br>
 * 03-12-2013
 */


class Default_AplicacaoAvaliacaoController extends Ead1_ControllerRestrita
{

    /**
     * @var \G2\Negocio\AplicacaoAvaliacao
     */
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\AplicacaoAvaliacao();
    }

    public function indexAction()
    {
        $this->_helper->viewRenderer->setNoRender(false);
        $id = $this->getParam('id');
        //$form = new AplicacaoAvaliacaoForm(array('id_avaliacaoaplicacao' => $id));

        //$this->view->form = $form;
        //$this->view->id_avaliacaoaplicacao = $id;

        $this->view->editModel = array();

        if ($id) {
            $entity = $this->negocio->find('G2\Entity\VwAvaliacaoAplicacao', $id);
            if ($entity instanceof G2\Entity\VwAvaliacaoAplicacao) {

                $dt_alteracaolimite = $this->negocio->converterData($entity->getDt_alteracaolimite(), 'd/m/Y');
                $dt_antecedenciaminima = $this->negocio->converterData($entity->getDt_antecedenciaminima(), 'd/m/Y');
                $dt_aplicacao = $this->negocio->converterData($entity->getDt_aplicacao(), 'd/m/Y');

                $entity = $this->negocio->toArrayEntity($entity);

                $entity['dt_alteracaolimite'] = $dt_alteracaolimite;
                $entity['dt_antecedenciaminima'] = $dt_antecedenciaminima;
                $entity['dt_aplicacao'] = $dt_aplicacao;

                $this->view->editModel = $entity;
            }
        }

    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
    }

    public function retornaAvaliacoesAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $result = $this->negocio->findAvaliacao(null, array(
            'st_entidadeavaliacao' => 'ASC',
            'st_avaliacao' => 'ASC'
        ));
        $arrReturn = array();
        if ($result) {
            foreach ($result as $row) {
                $arrReturn[] = array(
                    'id_avaliacao'     => $row->getId_avaliacao(),
                    'st_avaliacao'     => $row->getSt_avaliacao(),
                    'st_tipoavaliacao' => $row->getSt_tipoavaliacao(),
                    'st_situacao'      => $row->getSt_situacao(),
                    'st_entidadeavaliacao' => $row->getSt_entidadeavaliacao()
                );
            }
        }

        $this->_helper->json($arrReturn);
//        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    public function buscaEnderecoAplicadorAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $params['id_aplicadorprova'] = $this->getParam('idParam');
        $result = $this->negocio->findEnderecoAplicador($params);
        $arrReturn = array();
        if ($result) {
            foreach ($result as $row) {
                $arrReturn[$row->getId_endereco()] = array(
                    'id_value' => $row->getId_endereco(),
                    'st_text'  => $row->getSt_endereco()
                );
            }
        }

        $this->getResponse()->appendBody(json_encode(array_values($arrReturn)));
    }

    public function filtraAplicadorAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $params = null;
        if ($this->getParam('sg_uf')) {
            $params['sg_uf'] = $this->getParam('sg_uf');
        }
        if ($this->getParam('id_municipio')) {
            $params['id_municipio'] = $this->getParam('id_municipio');
        }
        $result = $this->negocio->findEnderecoAplicador($params);
        $arrReturn = array();
        if ($result) {
            foreach ($result as $row) {
                $arrReturn[$row->getId_aplicadorprova()] = array(
                    'id_value' => $row->getId_aplicadorprova(),
                    'st_text'  => $row->getSt_aplicadorprova()
                );
            }
        }

        $this->getResponse()->appendBody(json_encode(array_values($arrReturn)));
    }

    public function retornaAvaliacoesUpdateAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $param['id_avaliacaoaplicacao'] = $this->getParam('id');
        $result = $this->negocio->findByAvaliacoes($param);
        $arrReturn = array();
        if ($result) {
            foreach ($result as $row) {
                $arrReturn[] = array(
                    'id_avaliacao'     => $row->getId_avaliacao(),
                    'st_avaliacao'     => $row->getSt_avaliacao(),
                    'st_tipoavaliacao' => $row->getSt_tipoavaliacao(),
                    'st_situacao'      => $row->getSt_situacao()
                );
            }
        }

        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    public function retornaAplicadorEntidadeAplicacaoAction()
    {
        $negocio = new \G2\Negocio\AplicacaoAvaliacao();
        $array = array();

        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);

        $return = $negocio->retornaLocalProva($params);
        foreach ($return as $key => $value) {
            $return[$key]['id'] = $value['id_aplicadorprova'];
        }

        return $this->_helper->json($return);
    }

    public function getAction()
    {
        $this->getResponse()
            ->appendBody("From getAction() returning the requested article");
    }

    public function postAction()
    {
        $this->getResponse()
            ->appendBody("From postAction() creating the requested article");
    }

    public function putAction()
    {
        $this->getResponse()
            ->appendBody("From putAction() updating the requested article");
    }

}

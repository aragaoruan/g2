<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 27/03/2015
 * Time: 13:59
 */
class Default_ItemDeMaterialPresencialController extends Ead1_ControllerRestrita
{

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\ItemDeMaterialPresencial();
    }

    public function indexAction()
    {


    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
    }

    public function entregaAction()
    {
        $controller = $this->getRequest()->getControllerName();
        $var = $this->view->render($controller . '/entrega.js');
        echo $this->view->headScript()->appendScript($var);
    }

    public function retornaTurmaAction()
    {
        $post = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocio->retornaTurmaEntidade($post);
        $arrResult = array();
        foreach ($result as $key => $value) {
            $arrResult[$key]['id_turma'] = $value->getId_turma();
            $arrResult[$key]['st_turma'] = $value->getId_turma() . ' - ' . $value->getSt_turma();
        }
        $this->_helper->json($arrResult);
    }

    public function retornaDisciplinaAction()
    {
        $post = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocio->retornaDisciplinaTurma($post);
        $array = array();
        foreach ($result as $key => $value) {
            $array[$key]['id_disciplina'] = $value['id_disciplina'];
            $array[$key]['st_disciplina'] = $value['id_disciplina'] . ' - ' . $value['st_disciplina'];
        }


        $this->_helper->json($array);
    }

    public function retornaProfessorAction()
    {


        $post = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocio->retornaProfessorDisciplina($post);
        $arrResult = array();
        foreach ($result as $key => $value) {
            $arrResult[$key]['id_usuario'] = $value->getId_usuario();
            $arrResult[$key]['st_nomecompleto'] = $value->getSt_nomecompleto();
        }
        $this->_helper->json($arrResult);
    }

    public function retornaEncontroAction()
    {
        $post = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocio->retornaEncontroDisciplina($post);
        $arrResult = array();
        foreach ($result as $key => $value) {
            $arrResult[$key]['id_encontro'] = $value->getId_itemgradehoraria();
            $arrResult[$key]['nu_encontro'] = $value->getNu_encontro();
        }
        $this->_helper->json($arrResult);
    }

    public function retornaTipoDeMaterialAction()
    {
        $post = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocio->retornaTipoDeMaterial($post);
        $arrResult = array();
        foreach ($result as $key => $value) {
            $arrResult[$key]['id_tipodematerial'] = $value->getId_tipodematerial();
            $arrResult[$key]['st_tipodematerial'] = $value->getSt_tipodematerial();
        }
        $this->_helper->json($arrResult);
    }

    public function saveAction()
    {
        $post = $this->limpaGetAllParams($this->getAllParams());

        $result = $this->negocio->salvaMaterial($post);
        $arrResult = array();
        $arrResult['id_unidade'] = $result->getId_entidade()->getId_entidade();
        $arrResult['id_turma'] = $result->getId_turma()->getId_turma();
        $arrResult['id_disciplina'] = $result->getId_disciplina()->getId_disciplina();
        $arrResult['id_professor'] = $result->getId_professor()->getId_usuario();
        $arrResult['nu_encontro'] = $result->getNu_encontro();
        $arrResult['st_titulo'] = $result->getSt_nomematerial();
        $arrResult['id_tipo'] = $result->getId_tipomaterial()->getId_tipodematerial();
        $arrResult['id_situacao'] = $result->getId_situacao()->getId_situacao();
        $arrResult['nu_valor'] = $result->getNu_valor();
        $arrResult['nu_paginas'] = $result->getNu_paginas();
        $arrResult['nu_qtdestoque'] = $result->getNu_qtdestoque();
        $arrResult['id'] = $result->getId_material();
        $this->_helper->json($arrResult);
    }

    public function retornaPesquisaAction()
    {
        $post = $this->limpaGetAllParams($this->getAllParams());
        unset($post['page'], $post['per_page']);
        $resultado = $this->negocio->retornaPesquisaItemDeMaterialPresencial($post);
        $arrResult = array();

        foreach ($resultado as $key => $result) {
            $arrResult[$key]['id_itemdematerial'] = $result->getId_itemdematerial();
            $arrResult[$key]['st_itemdematerial'] = $result->getSt_itemdematerial();

            $arrResult[$key]['id_unidade'] = $result->getId_entidade();
            $arrResult[$key]['st_unidade'] = $result->getSt_entidade();

            $arrResult[$key]['id_disciplina'] = $result->getId_disciplina();
            $arrResult[$key]['st_disciplina'] = $result->getSt_disciplina();

            $arrResult[$key]['nu_encontro'] = $result->getNu_encontro();

            $arrResult[$key]['id_professor'] = $result->getId_professor();
            $arrResult[$key]['st_professor'] = $result->getSt_professor();

            $arrResult[$key]['id_tipo'] = $result->getId_tipodematerial();
            $arrResult[$key]['st_tipo'] = $result->getSt_tipodematerial();

            $arrResult[$key]['id_situacao'] = $result->getId_situacao();
            $arrResult[$key]['st_situacao'] = $result->getSt_situacao();

            $arrResult[$key]['nu_valor'] = $result->getNu_valor();

            $arrResult[$key]['nu_paginas'] = $result->getNu_qtdepaginas();

            $arrResult[$key]['id_upload'] = $result->getId_upload();
            $arrResult[$key]['st_upload'] = $result->getId_upload() != NULL ? $this->negocio->retornaNomeUpload($result->getId_upload()) : null;

            $arrResult[$key]['bl_portal'] = $result->getBl_portal();

            $arrResult[$key]['id_turma'] = $result->getId_turma();
            $arrResult[$key]['st_turma'] = $result->getSt_turma();

            $arrResult[$key]['nu_qtdestoque'] = $result->getNu_qtdestoque();
        }
//        Zend_Debug::dump($arrResult);exit;
        $this->_helper->json($arrResult);
    }

    public function gerarBarcodeAction()
    {
        $post = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocio->retornaDadosCodigoBarras($post);
        $arrResult = array();
        $arrResult['id'] = $result->getId_itemdematerial();
        $this->view->dados = $arrResult;
    }

    public function barCodeAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        if ($this->getParam('text')) {
            echo $this->view->barCode($this->getParam('text'), $this->getParam('barHeight'));
        } else {
            echo 'Parametro text não encontrado.';
        }
    }

    public function retornaMatriculasAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocio->retornaMatriculas($params);
        $arrResult = array();
        foreach ($result as $key => $result) {
            $arrResult[$key]['id_matricula'] = $result->getId_matricula();
            $arrResult[$key]['st_matricula'] = $result->getId_matricula() . ' - ' . $result->getSt_projetopedagogico();
        }
        $this->_helper->json($arrResult);
    }

    public function entregaRapidaAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocio->executaEntregaRapida($params);
        $arrayResult = array();
        if ($result == 0) {
            $arrayResult['title'] = 'Alerta';
            $arrayResult['type'] = 'alert';
            $arrayResult['mensagem'] = 'Material já entregue para o usuário. ';
            $arrayResult['tipo'] = 'entregue';
        } elseif ($result == 1) {
            $arrayResult['title'] = 'Sucesso';
            $arrayResult['type'] = 'success';
            $arrayResult['mensagem'] = 'Material entregue com sucesso. ';
            $arrayResult['tipo'] = 'success';
        } elseif ($result == 2) {
            $arrayResult['title'] = 'Alerta';
            $arrayResult['type'] = 'alert';
            $arrayResult['mensagem'] = 'Item de material não encontrado. ';
            $arrayResult['tipo'] = 'success';
        } else {
            $this->_helper->json($result);
        }
        $this->_helper->json($arrayResult);
    }

    public function retornaItensEntregaAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocio->retornaItemMaterialEntrega($params);
        $this->_helper->json($result);
    }

    public function entregaTableAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocio->executaEntrega($params);
        $arrResult = array();
        $arrResult['id_matricula'] = $result->getId_matricula();
        $arrResult['id_usuariocadastro'] = $result->getId_usuariocadastro();
        $arrResult['id_situacao'] = $result->getId_situacao();
        $arrResult['id_itemdematerial'] = $result->getId_itemdematerial();
        $arrResult['dt_entrega'] = $result->getDt_entrega();
        $arrResult['bl_ativo'] = $result->getBl_ativo();
        $arrResult['id_matriculadisciplina'] = $result->getId_matriculadisciplina();
        $arrResult['id_entregamaterial'] = $result->getId_entregamaterial();
        $this->_helper->json($arrResult);

    }

    public function devolverTableAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocio->devolverEntrega($params);
        $arrResult = array();
        $arrResult['id_matricula'] = $result->getId_matricula();
        $arrResult['id_usuariocadastro'] = $result->getId_usuariocadastro();
        $arrResult['id_situacao'] = $result->getId_situacao();
        $arrResult['id_itemdematerial'] = $result->getId_itemdematerial();
        $arrResult['dt_entrega'] = $result->getDt_entrega();
        $arrResult['bl_ativo'] = $result->getBl_ativo();
        $arrResult['id_matriculadisciplina'] = $result->getId_matriculadisciplina();
        $arrResult['id_entregamaterial'] = $result->getId_entregamaterial();
        $this->_helper->json($arrResult);
    }

    public function retornaTramiteAction(){
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocio->retornaTramite($params);
        $this->_helper->json($result);
    }
}
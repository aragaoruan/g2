<?php

/**
 * Description of AvaliacaoAlunoController
 *
 * @author rafael.bruno
 */
class Default_AvaliacaoAlunoController extends Ead1_ControllerRestrita {

    public function indexAction(){
        
        $secretariaNegocio = new \G2\Negocio\Secretaria();
        
        $this->view->projetoPedagogico = $secretariaNegocio
                ->retornaVwProjetoEntidade(array('id_entidade'=>$secretariaNegocio->sessao->id_entidade));
        $this->view->situacao = array(0 => 'Pendente', 1 => 'Entregue');
    }
    
    public function jsAction(){
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('avaliacao-aluno/defesa-tcc.js');
    }
    
    public function findVwAvaliacaoAlunoAction(){
        $negocio = new G2\Negocio\Avaliacao();
        $where = $this->_getParam('params');

        foreach ($negocio->findVwAvaliacaoAluno($where) as $entity) {
            $entity->setDt_encerramentosala($entity->getDt_encerramentosala()->format('d-m-Y'));
            $data[] = $negocio->toArrayEntity($entity);
        }

        $this->_helper->json($data);
    }

    public function downloadTccAction(){
        try{
            set_time_limit(0);
            $id_upload = $this->_getParam('id_upload', null);
            if (is_null($id_upload) || empty($id_upload)){
                throw new Exception('Código da interação não informado');
            }

            $upload = new UploadTO();
            $upload->setId_upload($id_upload);
            $upload->fetch(true, true, true);

            $caminho = 'upload'.DIRECTORY_SEPARATOR.'avaliacao'.DIRECTORY_SEPARATOR.$upload->getSt_upload();

            // Verifica se o arquivo não existe
            if (!file_exists($caminho)) {
                die('Arquivo não encontrado');
            }

            $arquivo = $caminho;
            if(isset($arquivo) && file_exists($arquivo)){ // faz o teste se a variavel não esta vazia e se o arquivo realmente existe
                $tipo = '';
                switch(strtolower(substr(strrchr(basename($arquivo),"."),1))){ // verifica a extensão do arquivo para pegar o tipo
                    case "pdf": $tipo="application/pdf"; break;
                    case "exe": $tipo="application/octet-stream"; break;
                    case "zip": $tipo="application/zip"; break;
                    case "doc": $tipo="application/msword"; break;
                    case "xls": $tipo="application/vnd.ms-excel"; break;
                    case "ppt": $tipo="application/vnd.ms-powerpoint"; break;
                    case "gif": $tipo="image/gif"; break;
                    case "png": $tipo="image/png"; break;
                    case "jpg": $tipo="image/jpg"; break;
                    case "mp3": $tipo="audio/mpeg"; break;
                    case "php": // deixar vazio por seurança
                    case "htm": // deixar vazio por seurança
                    case "html": // deixar vazio por seurança
                }
                header("Content-Type: ".$tipo);
                header("Content-Length: ".filesize($arquivo));
                header("Content-Disposition: attachment; filename=".basename($arquivo));
                readfile($arquivo); // lê o arquivo
                exit; // aborta pós-ações

            }
        }catch (Exception $e){
            die($e->getMessage());
        }
    }
    
    public function findVwAvaliacaoAlunoDefesaTccAction(){
        $avaliacaoNegocio = new \G2\Negocio\AvaliacaoAluno();
        $array = array();
        $result = $avaliacaoNegocio->findVwAvaliacaoAlunoDefesaTcc($this->getRequest()->getPost());
        
        foreach($result as $value){
            $toArray = $avaliacaoNegocio->toArrayEntity($value);
            
            if($value->getDt_defesa()){
                $toArray['dt_defesa'] = $value->getDt_defesa()->format('d/m/Y');
            }
            
            array_push($array, $toArray);
        }
        
        $this->_helper->json($array);
    }
    
    public function salvarDefesaTccAction(){
        $avaliacaoNegocio = new \G2\Negocio\AvaliacaoAluno();
        $result = array();
        if($this->getRequest()->isPost()){
            $result = $avaliacaoNegocio->salvarDefesaTcc($this->getRequest()->getParam('data'));
        }
        $this->_helper->json($result);
    }
    
}
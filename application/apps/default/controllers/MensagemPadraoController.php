<?php

/**
 * Controller para Mensagem Padrão
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @package default
 * @subpackage controller
 * @since 2013-0-12
 */
use G2\Negocio\MensagemPadrao;

class Default_MensagemPadraoController extends Ead1_ControllerRestrita
{

    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new MensagemPadrao();
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
//        echo $this->view->render('mensagem-padrao/index.js');
//        echo $this->view->render('sms-entidade-mensagem/index.js');
//        echo $this->view->render('sistema-entidade-mensagem/index.js');
    }

    public function indexAction()
    {
        $this->view->smsEntidade = $this->view->render('sms-entidade-mensagem/index.phtml');
        $this->view->sistemaEntidade = $this->view->render('sistema-entidade-mensagem/index.phtml');
    }

    /**
     * Recebe parametro via get do id_textoexibicao e retorna Json de G2\Entity\TextoCategoria
     * @param GET idTextoExibicao
     * @return array
     */
    public function getTextoCategoriaByExibicaoAction ()
    {
        $idTextoExibicao = $this->getParam('idTextoExibicao');
        $result = $this->negocio->find('G2\Entity\TextoExibicao', array('id_textoexibicao' => $idTextoExibicao));
        $arrReturn = array();
        if ($result) {
            foreach ($result->getTextoCategoria() as $row) {
                $arrReturn[] = array(
                    'id' => $row->getId_textocategoria(),
                    'text' => $row->getSt_textocategoria(),
                );
            }
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    /**
     * Recupera contas de envio por entidade sessão
     * @return json
     * @param void
     */
    public function getContaDeEnvioAction()
    {
        $result = $this->negocio->getContaEnvio();
        $arrRetorno = array();
        if ($result) {
            foreach ($result as $row) {
                $arrRetorno[] = array(
                    'id' => $row->getId_emailconfig(),
                    'st_conta' => $row->getSt_conta()
                );
            }
        }
        $this->_helper->json($arrRetorno);
    }

}

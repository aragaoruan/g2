<?php
/**
 * Classe para geração de códigos fonte
 * 
 * @author edermariano
 *
 * @package application
 * @subpackage controller
 */
class Default_GeradorCodigoController extends Ead1_Controller{
	
	public function ormAction(){
		$arArquivosOrm = scandir(APPLICATION_PATH . '/../application/models/orm');
    	foreach($arArquivosOrm as $arquivo){
    		if(substr($arquivo, 0, 1) != '.'){
    			$arOrms[substr($arquivo, 0, -4)] = 'tb_' . strtolower(substr($arquivo, 0, -7));
    		}
    	}
    	$tbUsuario = new UsuarioORM();
    	$tabelas = $tbUsuario->getAdapter()->listTables();
    	foreach($tabelas as $tabela){
    		$to = array_search($tabela, $arOrms);
    		if(!$to){
    			echo "<fieldset><legend>$tabela</legend>";
    			echo 'Eh necessario criar o ORM para tabela: ' . $tabela;
    			echo "<hr /><a href=\"http://" . $_SERVER["HTTP_HOST"] . "/gestor2/public/geradorcodigo/arquivoorm/orm/$tabela\" target='_blank'>Arquivo</a>";
    			echo "</fieldset>";
    		}
    	}
    	exit;
		
	}
	
	public function toAction(){
		$arArquivosTo = scandir(APPLICATION_PATH . '/../application/models/to');
    	foreach($arArquivosTo as $arquivo){
    		if(substr($arquivo, 0, 1) != '.'){
    			$arTos[substr($arquivo, 0, -4)] = 'tb_' . strtolower(substr($arquivo, 0, -6));
    		}
    	}
    	
    	
    	$tbUsuario = new UsuarioORM();
    	$tabelas = $tbUsuario->getAdapter()->listTables();
    	foreach($tabelas as $tabela){
    		$to = array_search($tabela, $arTos);
    		if($to){
    			$objTo = new $to();
    			$arAtributosTo = array_keys(get_object_vars($objTo));
    			$arAtributosTabela = $tbUsuario->getAdapter()->describeTable($tabela);
    			$arAtributosTabela = array_keys($arAtributosTabela);
    			
    			$diferencaDeAtributosTO = array_diff($arAtributosTabela, $arAtributosTo);
    			$diferencaDeAtributosTabela = array_diff($arAtributosTo, $arAtributosTabela);    			
    			if(!empty($diferencaDeAtributosTO) || !empty($diferencaDeAtributosTabela)){
	    			echo "<fieldset><legend>RELACIONAMENTOS</legend>";
	    			if(!empty($diferencaDeAtributosTO)){
	    				echo "<fieldset><legend>$to -> $tabela</legend>";
		    			echo "O TO <span style='color: red;'>$to</span> nao possui o(s) atributo(s): <span style='color: red;'>" . implode(', ', $diferencaDeAtributosTO);
		    			echo "</span><br /><!--<a href=\"http://" . $_SERVER["HTTP_HOST"] . "/gestor2/public/geradorcodigo/arquivoto/to/$to\" target='_blank'>Arquivo $to;</a>-->";
		    			echo '<hr />';
		    			Zend_Debug::dump($arAtributosTo, 'Atributos do TO: ' . $to);
		    			echo '<hr />';
		    			Zend_Debug::dump($arAtributosTabela, 'Atributos da Tabela: ' . $tabela);
	    				echo "</fieldset>";
	    			}
	    			
	    			if(!empty($diferencaDeAtributosTabela)){
	    				echo "<fieldset><legend>$tabela -> $to</legend>";
		    			echo "O TO <span style='color: red;'>$to</span> possui o(s) atributo(s): <span style='color: red;'>" . implode(', ', $diferencaDeAtributosTabela);
		    			echo "</span> que nao existem na tabela $tabela<br /><!--<a href=\"http://" . $_SERVER["HTTP_HOST"] . "/gestor2/public/geradorcodigo/arquivoto/to/$to\" target='_blank'>Arquivo $to;</a>-->";
		    			echo '<hr />';
		    			Zend_Debug::dump($arAtributosTo, 'Atributos do TO: ' . $to);
		    			echo '<hr />';
		    			Zend_Debug::dump($arAtributosTabela, 'Atributos da Tabela: ' . $tabela);
	    				echo "</fieldset>";
	    			}
    			echo "</fieldset>";
    			}
    		}else{
    			echo 'É necessário criar o TO para tabela: ' . $tabela;
    		}
    	}
    	exit;
	}
	
	public function arquivotoAction(){
		$to = $this->_getParam('to');
		$ar = file(APPLICATION_PATH . '/../application/models/to/' . $to . '.php');
		foreach($ar as $linhas){
			echo str_replace(' ', '&nbsp;', $linhas) . "<br />";
		}
		//Zend_Debug::dump($ar, 'Arquivo: ');
		exit;
	}
	
	public function arquivoormAction(){
		$to = $this->_getParam('orm');
		$tbUsuario = new UsuarioORM();
		$dados = $tbUsuario->getDefaultAdapter()->describeTable($to);
		$orm = ucfirst(substr($to, 3)) . "ORM";
		echo "<form action='{$this->getFrontController()->getBaseUrl()}/geradorcodigo/criararquivo' method='post'>";
		$str = "class $orm extends Ead1_ORM{<br />";
		$str .= 'public $_name = \'' .  $to . "';<br />";
		
		foreach($dados as $campo => $dado){
			if($dado["PRIMARY"] == true){
				$arPks[] = $campo;
			}
		}
		
		$str .= 'public $_primary = array(\'' .  implode("', '", $arPks) . "');<br />}";
		echo $str;
		$str64 = base64_encode($str);
		echo "<input type='hidden' name='conteudoarquivo' value='$str64' />";
		echo "<br /> <input type='submit' name='enviar' value='Criar' />";
		echo "</form>";
		exit;
	}
	
	public function criararquivoAction(){
		Zend_Debug::dump($this->_getAllParams());
		Zend_Debug::dump($this->_getParam("arquivo"));
		Zend_Debug::dump(explode('<br />', base64_decode($this->_getParam("conteudoarquivo"))));
		
		if($arquivo = fopen('teste.php', 'w')){
			foreach(explode('<br />', base64_decode($this->_getParam("conteudoarquivo"))) as $linha){
				fwrite($arquivo, $linha);
			}
			fclose($arquivo);
		}else{
			echo 'ERRO';
		}
		exit;
		
	}
	
	/**
	 * Gerador de TO
	 */
	public function gerartoAction(){
		header ("Content-type: application/x-msexcel");
		header ("Content-Disposition: attachment; filename=to.txt");
		$orm = new VwPessoaORM();
		$to_name = substr(get_class($orm), 0, -3) . 'TO';
		$describeTable = $orm->info();
		$cols = $describeTable['cols'];
		
		echo "<?php \n";
		echo "class $to_name extends Ead1_TO_Dinamico {\n";
		//Criando Variaveis
		foreach($cols as $col){
			echo "\n\tpublic $".$col.";";
		}
		
		echo "\n";
		
		//Criando getters
		foreach($cols as $col){
			echo "\n\t/**";
			echo "\n\t* @return the $".$col;
			echo "\n\t*/";
			echo "\n\tpublic function get".ucwords($col)."(){ \n";
			echo "\t\treturn \$this->".$col.";";
			echo "\n\t}\n";
		}
		
		echo "\n";
		
		//Criando Setters
		foreach($cols as $col){
			echo "\n\t/**";
			echo "\n\t* @param field_type $".$col;
			echo "\n\t*/";
			echo "\n\tpublic function set".ucwords($col)."(\$value){ \n";
			echo "\t\t\$this->".$col." = \$value;";
			echo "\n\t}\n";
		}
		
		echo "}";
		
		exit;
	}
}
<?php

class Default_IntegracaoFabricaDeProvasController extends Ead1_ControllerRestrita
{
    /**
     * @var \G2\Negocio\Maximize $negocio
     */
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Maximize();
    }

    public function indexAction()
    {

    }

    public function listarProvasAction()
    {
        $st_prova = $this->getParam('st_prova');
        $result = $this->negocio->listarProvas($st_prova);
        if ($result->getTipo() === \Ead1_IMensageiro::ERRO) {
            $this->getResponse()->setHttpResponseCode(500);
        }
        $this->_helper->json($result);
    }

}

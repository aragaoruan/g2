<?php

/**
 * Created by PhpStorm.
 * User: kayo.silva
 * Date: 19/10/2015
 * Time: 09:24
 */
class Default_PerguntasFrequentesController extends Ead1_ControllerRestrita
{
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\PerguntasFrequentes();
    }


    public function indexAction()
    {

        if ($this->getParam('id')) {
            $this->view->esquema = $this->negocio->findEsquema($this->getParam('id'), true);
        }


    }


    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        echo $this->view->render($this->_getParam('controller') . '/index.js');
    }

    public function salvarArrayCategoriasAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json_Decoder::decode($body);

        $result = $this->negocio->salvarArrayCategorias($data);
        if ($result) {
            $this->getResponse()->setHttpResponseCode(201)->appendBody(json_encode($result));
        } else {
            $this->getResponse()->setHttpResponseCode(400)->setBody("Erro ao salvar categorias.");
        }
    }

    public function salvarArrayPerguntasAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json_Decoder::decode($body);
        $result = $this->negocio->salvarArrayPerguntas($data);
        if ($result) {
            $this->getResponse()->setHttpResponseCode(201)->appendBody(json_encode($result));
        } else {
            $this->getResponse()->setHttpResponseCode(400)->setBody("Erro ao salvar categorias.");
        }
    }
}
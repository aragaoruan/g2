<?php

/**
 * Controller para SMS Entidade Mensagem
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @package default
 * @subpackage controller
 * @since 2013-09-23
 */
use G2\Negocio\MensagemPadrao;

class Default_SmsEntidadeMensagemController extends Ead1_ControllerRestrita
{

    private $negocio;

    public function init ()
    {
        parent::init();
        $this->negocio = new MensagemPadrao();
    }

    public function jsAction ()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('sms-entidade-mensagem/index.js');
    }

    public function indexAction ()
    {
       
    }

}
<?php

use G2\Negocio\TextoSistema;

/**
 * TextosController
 *
 * @author
 * @version
 */
class Default_TextosController extends Zend_Rest_Controller
{

    /**
     * @var TextoSistema
     */
    private $negocio;

    public function init()
    {
        $this->_helper->viewRenderer->setNoRender(true);

//        $this->doctrineContainer = Zend_Registry::get('doctrine');
//        $this->em = $this->doctrineContainer->getEntityManager();

        $this->negocio = new TextoSistema();
    }

    public function indexAction()
    {
        $this->_helper->viewRenderer->setNoRender(false);

        $id = $this->getParam('id');
        $form = new TextosSistemaForm(array('id' => $id));
        $this->view->form = $form;
    }

    /**
     * Retorna json com textos do sistema
     */
    public function getTextoSistemaOptionsAction()
    {
        $id_textoexibicao = $this->getParam('idTextoExibicao');
        $result = $this->negocio->findByEntidadeSessao(array('id_textoexibicao' => $id_textoexibicao));
        $arrReturn = array();
        if ($result) {
            foreach ($result as $row) {
                $arrReturn[] = array(
                    'id' => $row->getId_textosistema(),
                    'id_textosistema' => $row->getId_textosistema(),
                    'st_textosistema' => $row->getSt_textosistema(),
//                    'id_textocategoria' => $row->getId_textocategoria()->getId_textocategoria()
                );
            }
        }

        $this->getResponse()
            ->appendBody(json_encode($arrReturn));
    }

    /**
     * Renderiza o javascript para REST
     */
    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render($this->_getParam('controller') . '/index.js');
    }

    /**
     * Recebe parametro via get do id_textoexibicao e retorna Json de G2\Entity\TextoCategoria
     * @param GET idTextoExibicao
     * @return array
     */
    public function getTextoCategoriaByExibicaoAction()
    {
        $idTextoExibicao = $this->getParam('idTextoExibicao');
        $result = $this->negocio->find('G2\Entity\TextoExibicao', array('id_textoexibicao' => $idTextoExibicao));
        $arrReturn = array();
        if ($result) {
            foreach ($result->getTextoCategoria() as $row) {
                $arrReturn[] = array(
                    'id' => $row->getId_textocategoria(),
                    'text' => $row->getSt_textocategoria(),
                );
            }
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    /**
     * Recebe parametro via get id_textocategoria e retorna json de G2\Entity\TextoVariaveis
     * @param void
     * @return array
     */
    public function getVariaveisByCategoriaAction()
    {
        $idTextoCategoria = $this->getParam('idTextoCategoria');
        $result = $this->negocio
            ->findBy('G2\Entity\TextoVariaveis', array(
                'id_textocategoria' => $idTextoCategoria,
                'bl_exibicao' => 1
            ), array('st_textovariaveis' => 'ASC'));
        $arrReturn = array();
        if ($result) {
            foreach ($result as $row) {
                $arrReturn[] = array(
                    'id' => $row->getId_textovariaveis(),
                    'text' => $row->getSt_textovariaveis(),
                );
            }
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }


    /**
     * Não usamos mais o de cima no cadastro de texto sistema
     * @throws Exception
     */
    public function getVariaveisAction()
    {

        try {
            $data = $this->getAllParams();
            unset($data['controller'], $data['action'], $data['module']);

            if (!$data || !isset($data['id_textocategoria']) || !$data['id_textocategoria']) {
                $this->_helper->json(array());
            }

            if (isset($data['st_textovariaveis']) && !$data['st_textovariaveis'])
                unset($data['st_textovariaveis']);

            //adicionando o parametro para ele pesquisar só pelos textos de sistemas marcados para exibição
            $data = array_merge($data, array('bl_exibicao' => 1));

            $tv = new \G2\Entity\TextoVariaveis();
            $result = $tv->findByCustom($data, array('st_textovariaveis' => 'ASC'));
            $arrReturn = array();
            if ($result) {
                foreach ($result as $row) {
                    $arrReturn[] = array(
                        'id' => $row->getId_textovariaveis(),
                        'text' => $row->getSt_textovariaveis(),
                    );
                }
            }
            $this->_helper->json($arrReturn);
        } catch (\Exception $e) {
            $this->_helper->json(array(0 => $e->getMessage()));
        }

    }

    public function getAction()
    {
        $this->getResponse()
            ->appendBody("From getAction() returning the requested article");
    }

    public function postAction()
    {
        $this->getResponse()
            ->appendBody("From postAction() creating the requested article");
    }

    public function putAction()
    {
        $this->getResponse()
            ->appendBody("From putAction() updating the requested article");
    }

    public function deleteAction()
    {
        $id = $this->getParam('id');
        $this->getResponse()
            ->appendBody("From deleteAction() deleting the requested article" . $id);
    }

    public function headAction()
    {

    }

    public function reciboAction()
    {

        try {

            ini_set('display_errors', 0);
            $bo = new TextoSistemaBO();

            $textoSistemaTO = new TextoSistemaTO();
            $textoSistemaTO->setId_textosistema((int)$this->_getParam('id_textosistema'));
            $textoSistemaTO->fetch(true, true, true);

            if (!$textoSistemaTO->getSt_textosistema()) {
                throw new Exception('Texto do sistema não encontrado.');
            }

            switch ($this->_getParam('type')) {
                case TextoSistemaBO::HTML:

                    $params = (array)$this->_getParam('arrParams');
                    $params = array_filter($params);

                    $variaveis = array();

                    if (array_key_exists('dt_inicio', $params)) {
                        $variaveis['dt_inicio'] = $params['dt_inicio'];
                    }

                    if (array_key_exists('dt_termino', $params)) {
                        $variaveis['dt_termino'] = $params['dt_termino'];
                    }

                    $params['variaveis'] = $variaveis;

                    $textoSistemaTOConsulta = new TextoSistemaTO();
                    $textoSistemaTOConsulta->setId_textosistema($textoSistemaTO->getId_textosistema());
                    $textoSistemaTOConsulta->setId_entidade($textoSistemaTO->getId_entidade());

                    $mensageiro = $bo->gerarHTML($textoSistemaTOConsulta, $params, true)->subtractMensageiro();

                    echo $mensageiro->getFirstMensagem()->getSt_texto();
                    break;
                case TextoSistemaBO::PDF:
                    $bo->gerarPDF($textoSistemaTO, $this->_getParam('arrParams'), $this->_getParam('filename'),
                        null, true)->subtractMensageiro();
                    break;
                default:
                    throw new Zend_Validate_Exception("Tipo de Geração não Suportado. ("
                        . strtoupper($this->_getParam('file')) . ")");
                    break;
            }
        } catch (Zend_Exception $e) {
            $this->_helper->viewRenderer->setNoRender(false);
            $erro = '<div class="container cabecalho prepend-top erro span-24" >';
            $erro .= '<p>Erro: ' . $e->getMessage() . '</p>';
            $erro .= '</div>';
            $this->view->cabecalho = $erro;
        }
    }

    /**
     * Action que gera o Texto do Sistema
     */
    public function geracaoAction()
    {
        try {
            //$this->ajax();
            $bo = new TextoSistemaBO();
            $this->_validaParametrosGeracaoTextoSistema();
            $textoSistemaTO = new TextoSistemaTO();
            $textoSistemaTO->setId_textosistema((int)$this->_getParam('id_textosistema'));
            switch ($this->_getParam('type')) {
                case TextoSistemaBO::HTML:
                    $mensageiro = $bo->gerarHTML($textoSistemaTO, $this->_getParam('arrParams'))->subtractMensageiro();

                    if ($this->getParam('draft')) {
                        echo '<img src="/img/background/draft.jpg" 
                    style="position: fixed; top: 50%; left: 50%; margin: -495px 0 0 -600px; z-index: -1;" />';
                    }

                    echo $mensageiro->getFirstMensagem()->getSt_texto();

                    break;
                case TextoSistemaBO::PDF:
                    ini_set('display_errors', 0);
                    $bo->gerarPDF(
                        $textoSistemaTO,
                        $this->_getParam('arrParams'),
                        $this->_getParam('filename')
                    )->subtractMensageiro();
                    break;
                default:
                    throw new Zend_Validate_Exception("Tipo de Geração não Suportado. ("
                        . strtoupper($this->_getParam('file')) . ")");
                    break;
            }
        } catch (Zend_Exception $e) {
            $this->_helper->viewRenderer->setNoRender(false);
            $erro = '<div class="container cabecalho prepend-top erro span-24" >';
            $erro .= '<p>Erro: ' . $e->getMessage() . '</p>';
            $erro .= '</div>';
            $this->view->cabecalho = $erro;
        }
    }

    public function recebimentoAction()
    {
        try {
            $entidadeTO = new EntidadeTO();
            $entidadeTO->setId_entidade($entidadeTO->getSessao()->id_entidade);
            $entidadeTO->fetch(true, true, true);

            $relatorioBO = new RelatorioBO();
            $postExibicao = $relatorioBO
                ->trataArrayExibicao(
                    $this->_getParam('colunas'), ($this->_getParam('xls') ? $this->_getParam('xls') : false));
            $exibicao = (empty($postExibicao) ? null : $postExibicao);

            $vwResumoFinanceiro = new VwResumoFinanceiroTO();
            $vwResumoFinanceiro->setId_venda($this->_getParam('id_venda'));
            $vwResumoFinanceiro->setId_entidade($entidadeTO->getId_entidade());
            $vendaDAO = new VendaDAO();
            $dadosGrid = $vendaDAO->retornarResumoFinanceiroVenda($vwResumoFinanceiro)->toArray();

            if ($this->_getParam('xls')) {
                $xls = new Ead1_GeradorXLS();
                $arrParametroTO = Ead1_TO_Dinamico::encapsularTo($dadosGrid, new ParametroRelatorioTO());
                $arrParametroTOtratado = array();
                foreach ($arrParametroTO as $parametroRelatorioTO) {
                    $parametroRelatorioTO->dt_vencimento = ($parametroRelatorioTO->dt_vencimento
                        ? $parametroRelatorioTO->dt_vencimento->toString('dd/MM/YYYY') : 'Sem data');
                    $parametroRelatorioTO->dt_quitado = ($parametroRelatorioTO->dt_quitado
                        ? $parametroRelatorioTO->dt_quitado->toString('dd/MM/YYYY') : 'Sem data');
                    $arrParametroTOtratado[] = $parametroRelatorioTO;
                }

                $arrXLSHeaderTO = Ead1_TO_Dinamico::encapsularTo($exibicao, new XLSHeaderTO());

                $x = new XLSConfigurationTO();
                $x->setTitle('Grade de Lançamentos');
                $x->setFooter('Gerado por: ' . $entidadeTO->getSessao()->nomeUsuario . ' Data: ' . date('d/m/Y H:i:s'));
                $x->setFilename('lacamento');
                $xls->geraXLS($arrXLSHeaderTO, $arrParametroTOtratado, $x);
                exit;
            }
        } catch (Zend_Exception $e) {
            echo $e->getMessage();
            exit;
        }
    }

    /**
     * Método que valida os Parametros de Geração de Texto Sistema
     */
    private function _validaParametrosGeracaoTextoSistema()
    {
        if (!$this->hasParam('arrParams')
            || !$this->hasParam('id_textosistema')
            || !$this->hasParam('type')
            || !$this->hasParam('filename')) {
            throw new Zend_Validate_Exception("Faltam de Argumentos de Paramêtros!");
        }
    }

    public function vendaLancamentosAtrasadosAction()
    {
        try {
            $entidadeTO = new EntidadeTO();
            $entidadeTO->setId_entidade($entidadeTO->getSessao()->id_entidade);
            $entidadeTO->fetch(true, true, true);

            $colunas = array();
            $colunas[0] = "id_lancamento,Cod Lanc";
            $colunas[1] = "bl_entrada,Tipo";
            $colunas[2] = "st_meiopagamento,Meio Pagto";
            $colunas[3] = "nu_valor,Valor";
            $colunas[4] = "dt_vencimento,Vencimento";
            $colunas[5] = "dt_quitado,Quitacao";
            $colunas[6] = "nu_quitado,Valor Quitado";
            $colunas[7] = "nu_valoratualizado,Valor Atualizado";
            $colunas[8] = "st_situacao,Situacao";

            $relatorioBO = new RelatorioBO();
            $postExibicao = $relatorioBO->trataArrayExibicao($colunas,
                ($this->_getParam('xls') ? $this->_getParam('xls') : false));
            $exibicao = (empty($postExibicao) ? null : $postExibicao);


            $vwResumoFinanceiro = new VwResumoFinanceiroTO();
            $vwResumoFinanceiro->setId_venda($this->_getParam('id_venda'));
            $vwResumoFinanceiro->setId_entidade($entidadeTO->getId_entidade());

            $vendaDAO = new VendaDAO();
            $dadosGrid = $vendaDAO->retornarResumoFinanceiroVenda($vwResumoFinanceiro)->toArray();

            $xls = new Ead1_GeradorXLS();

            $retorno = Ead1_TO_Dinamico::encapsularTo($vendaDAO
                ->retornarResumoFinanceiroVenda($vwResumoFinanceiro), new VwResumoFinanceiroTO());

            $dadosXLS = array();

            foreach ($retorno as &$vwTO) {
                $bo = new TextosBO();
                $vwTO->setNu_quitado($bo->formataValor($vwTO->getNu_quitado()));
                $vwTO->setNu_valor($bo->formataValor($vwTO->getNu_valor()));
                $vwTO->setBl_entrada($vwTO->getBl_entrada() ? 'Entrada' : 'Parcela');
                $vwTO->setDt_quitado($vwTO->getDt_quitado() ?
                    $vwTO->getDt_quitado()->toString('dd/MM/YYYY') : 'Sem data');
                $vwTO->setDt_vencimento($vwTO->getDt_vencimento() ?
                    $vwTO->getDt_vencimento()->toString('dd/MM/YYYY') : 'Sem data');
            }

            $arrXLSHeaderTO = Ead1_TO_Dinamico::encapsularTo($exibicao, new XLSHeaderTO());

            $x = new XLSConfigurationTO();

            $x->setTitle('Resumo financeiro venda: ' . $retorno[0]->getId_venda());
            $x->setFooter('Gerado por: ' . $entidadeTO->getSessao()->nomeUsuario . ' Data: ' . date('d/m/Y H:i:s'));
            $x->setFilename('LancamentosAtrasados_Venda' . $retorno[0]->getId_venda());
            //$xls->geraXLS($arrXLSHeaderTO, $arrParametroTOtratado, $x);
            $xls->geraXLS($arrXLSHeaderTO, $retorno, $x);
            exit;
        } catch (Zend_Exception $e) {
            echo $e->getMessage();
            exit;
        }
    }

    public function retornarTextosMensagemCobrancaAction()
    {
        $result = $this->negocio->findBy('G2\Entity\TextoSistema', array(
            'id_entidade' => Ead1_Sessao::getSessaoGeral()->id_entidade,
            'id_textocategoria' => \G2\Constante\TextoCategoria::VENDA
        ));

        $arrReturn = array();

        foreach ($result as $row) {
            $arrReturn[] = $this->negocio->toArrayEntity($row);
        }

        header('ContentType: text/json');
        echo json_encode($arrReturn);
    }

    public function retornarTextoSistemaByMensagemPadraoAction()
    {
        $id_mensagempadrao = $this->getParam('id_mensagempadrao');
        $result = $this->negocio
            ->retornarTextoSistemaByMensagemPadrao($id_mensagempadrao);
        if ($result) {
            $result = $this->negocio->toArrayEntity($result);
        }

        $this->_helper->json($result);
    }

    public function getDisciplinasAptoAction()
    {
        $arr_return = array();

        $id_matricula = $this->getParam('id_matricula');

        if ($id_matricula) {
            $arr_return = $this->negocio->findDisciplinasAptoAgendamento($id_matricula);
        }

        $this->_helper->json($arr_return);
    }

    public function getAgendamentosChamadasAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $results = $this->negocio->findAvaliacoesDisciplinasAgendamento($params);
        $this->_helper->json($results);
    }

    public function comprovanteAgendamentoAction()
    {
        try {
            $id_usuario = $this->getParam('id_usuario');
            $id_avaliacaoagendamento = $this->getParam('id_avaliacaoagendamento');

            if (!$id_usuario) {
                throw new \Exception("Informe o Usuário.");
            }

            if (!$id_avaliacaoagendamento) {
                throw new \Exception("Informe o Agendamento.");
            }

            $negocio = new \G2\Negocio\GerenciaProva();
            $result = $negocio->findByVwAvaliacaoAgendamento(array(
                'id_usuario' => $id_usuario,
                'id_avaliacaoagendamento' => $id_avaliacaoagendamento
            ));

            if ($result && $result[0] instanceof \G2\Entity\VwAvaliacaoAgendamento) {
                $this->view->texto = utf8_decode($negocio
                    ->retornaTextoMensagemAgendamento($negocio->toArrayEntity($result[0])));
            } else {
                throw new Exception('Agendamento não encontrado.');
            }

            // Renderizar a view ja utilizada no portal antigo
            $this->view->addScriptPath(APPLICATION_PATH . "/apps/portal/views/scripts/");
            echo $this->view->render('avaliacao/imprimir-agendamento.phtml');
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function visualizarAction()
    {

        try {

            ini_set('display_errors', 0);
            $bo = new TextoSistemaBO();

            $securityKey = Zend_Registry::get('config')->security->key;
            $paramsDecifrado = json_decode(\util\Cipher::decrypt($this->_getParam('key'), $securityKey));

            $textoSistemaTO = new TextoSistemaTO();
            $textoSistemaTO->setId_textosistema((int)$this->_getParam('id_textosistema'));
            $textoSistemaTO->fetch(true, true, true);

            if (!$textoSistemaTO->getSt_textosistema()) {
                throw new Exception('Texto do sistema não encontrado.');
            }

            switch ($this->_getParam('type')) {
                case TextoSistemaBO::HTML:

                    $params = (array)$paramsDecifrado;
                    $params = array_filter($params);

                    $variaveis = array();

                    if (array_key_exists('id_venda', $params)) {
                        $variaveis['id_venda'] = $params['id_venda'];
                    }

                    if (array_key_exists('id_acordo', $params)) {
                        $variaveis['id_acordo'] = $params['id_acordo'];
                    }

                    $params['variaveis'] = $variaveis;

                    $textoSistemaTOConsulta = new TextoSistemaTO();
                    $textoSistemaTOConsulta->setId_textosistema($textoSistemaTO->getId_textosistema());

                    $mensageiro = $bo->gerarHTML($textoSistemaTOConsulta, $params, true)->subtractMensageiro();

                    echo $mensageiro->getFirstMensagem()->getSt_texto();
                    break;
                case TextoSistemaBO::PDF:
                    $bo->gerarPDF($textoSistemaTO, $this->_getParam('arrParams'), $this->_getParam('filename'))
                        ->subtractMensageiro();
                    break;
                default:
                    throw new Zend_Validate_Exception("Tipo de Geração não Suportado. ("
                        . strtoupper($this->_getParam('file')) . ")");
                    break;
            }
        } catch (Zend_Exception $e) {
            $this->_helper->viewRenderer->setNoRender(false);
            $erro = '<div class="container cabecalho prepend-top erro span-24" >';
            $erro .= '<p>Erro: ' . $e->getMessage() . '</p>';
            $erro .= '</div>';
            $this->view->cabecalho = $erro;
        }
    }


}

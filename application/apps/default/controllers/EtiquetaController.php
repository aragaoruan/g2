<?php

/**
 * Class Default_EtiquetaController
 * @author João Marcos Bizarro Lopes <joao.lopes@unyleya.com.br>
 * @since 2017-08-25
 * @package application
 * @subpackage controller
 */
class Default_EtiquetaController extends Ead1_ControllerRestrita
{

    private $negocio;

    /**
     * Instancia a Negocio de Etiqueta
     */
    public function init()
    {
        parent::init();
        $this->negocio = new G2\Negocio\Etiqueta();
    }

    public function indexAction()
    {
    }

    /**
     * Método responsável por gerar e baixar o XLS de etiquetas
     */
    public function gerarXlsAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = [];
        foreach ($params as $param) {
            $data = json_decode($param);
            array_push($result, $data);
        }

        $this->view->result = $result;
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $controller = $this->getRequest()->getControllerName();
        $jsFile = $this->view->render($controller . '/index.js');
        echo $jsFile;
    }

    /**
     * Método responsável por retornar as declarações de acordo com
     * os parâmetros
     * @return mixed
     */
    public function getDeclaracaoAction()
    {
        $result = $this->negocio->findDeclaracao(
            $this->limpaGetAllParams($this->getAllParams())
        );

        return $this->_helper->json(
            $this->negocio->toArrayEntity(
                $result
            )
        );
    }

    /**
     * Método responsável por retornar as certificações de acordo com
     * os parâmetros
     * @return mixed
     */
    public function getCertificacaoAction()
    {
        $result = $this->negocio->findCertificacao(
            $this->limpaGetAllParams($this->getAllParams())
        );

        return $this->_helper->json(
            $this->negocio->toArrayEntity(
                $result
            )
        );
    }

    public function getPesquisaSemParametroAction()
    {
        $result = $this->negocio->findSemParametro(
            $this->limpaGetAllParams($this->getAllParams())
        );

        return $this->_helper->json(
            $this->negocio->toArrayEntity(
                $result
            )
        );
    }
}

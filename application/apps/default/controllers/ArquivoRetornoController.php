<?php

/**
 * Classe de controller MVC para controlar BaixarBoleto
 * @author Rafael Rocha <rafael.rocha.mg@gmail.com>
 *
 * @package application
 * @subpackage controller
 */
class Default_ArquivoRetornoController extends Ead1_ControllerRestrita
{

    private $negocio;

    public function init()
    {
        parent::init();
        parent::setupDoctrine();
        $this->negocio = new \G2\Negocio\RegraPagamento();
        $this->_helper->getHelper('viewRenderer')->setNoRender();
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('/arquivo-retorno/index.js');
    }

    public function indexAction()
    {
        echo $this->view->headScript()->appendFile('/arquivo-retorno/js');
    }

    public function uploadAction() {
        set_time_limit(0);
        ini_set('max_execution_time', 0); //0=NOLIMIT
        ini_set('default_socket_timeout', 60000);

        $loginBO = new LoginBO();
        $arquitoTO = new ArquivoTO();
        $bo = new \ArquivoRetornoBO();

        $arquitoTO->setAr_arquivo($_FILES['arquivo']);
        $arquitoTO->setSt_caminhodiretorio('boleto'.DIRECTORY_SEPARATOR.'arquivo_retorno');

        $name = explode('.', $_FILES['arquivo']['name']);
        $arquitoTO->setSt_nomearquivo($name[0]);
        $arquitoTO->setSt_extensaoarquivo($name[count($name)-1]);

        $mensageiro = $loginBO->uploadBasico($arquitoTO);
        if($mensageiro->getTipo() == \Ead1_IMensageiro::ERRO){
            $this->_helper->json($mensageiro);
        }

        $mensageiroArp = $bo->processarArquivoRetorno($mensageiro->getMensagem(1));
        if($mensageiroArp->getTipo() == \Ead1_IMensageiro::ERRO){
            $this->_helper->json($mensageiroArp);
        }

        echo json_encode(new Ead1_Mensageiro($mensageiroArp->getMensagem(1), Ead1_IMensageiro::SUCESSO));
    }

    public function retornarRegraProfessorProjetoAction()
    {

        $vws = $this->negocio->findByProfessorProjeto();

        $retorno = array();

        foreach ($vws as $vw) {
            $retorno[] = $this->negocio->toArrayEntity($vw);
        }
        echo json_encode($retorno);
    }

    public function gerarXlsArquivoRetornoAction(){

        $this->bo = new \ArquivoRetornoBO();

        /** @var  $mensageiro \Ead1_Mensageiro */
        $mensageiro = $this->bo->retornoLancamentos(array('id' => $this->_getParam('id-arquivo-retorno', null), 'bl_ativo' => '0', 'st_carteira' => '109'));
        $this->view->boletosInativos = $this->preparaDadosXLS($mensageiro->getMensagem());

        /** @var  $mensageiro \Ead1_Mensageiro */
        $mensageiro = $this->bo->retornoLancamentos(array('id' => $this->_getParam('id-arquivo-retorno', null), 'bl_quitado' => '1', 'st_carteira' => '109', 'nu_maior' => '0'));
        $this->view->boletosQuitados = $this->preparaDadosXLS($mensageiro->getMensagem());

        /** @var  $mensageiro \Ead1_Mensageiro */
        $mensageiro = $this->bo->retornoLancamentos(array('id' => $this->_getParam('id-arquivo-retorno', null), 'id_lancamento' => 'is null', 'st_carteira' => '109'));
        $this->view->boletosNaoLocalizados = $this->preparaDadosXLS($mensageiro->getMensagem());

        /** @var  $mensageiro \Ead1_Mensageiro */
        $mensageiro = $this->bo->retornoLancamentos(array('id' => $this->_getParam('id-arquivo-retorno', null),'id_lancamento' => 'is not null', 'st_carteira' => '109', 'nu_maior' => 1));
        $this->view->pagamentoMenorOriginal = $this->preparaDadosXLS($mensageiro->getMensagem());

        /** @var  $mensageiro \Ead1_Mensageiro */
        $mensageiro = $this->bo->retornoLancamentos(array('id' => $this->_getParam('id-arquivo-retorno', null), 'st_carteira' => '175'));
        $this->view->boletosG1 = $this->preparaDadosXLS($mensageiro->getMensagem());

        $arquivoRetorno = new ArquivoRetornoTO();
        $arquivoRetorno->setId_arquivoretorno($this->_getParam('id-arquivo-retorno', null));

        $mensageiro = $this->bo->listarArquivoRetorno($arquivoRetorno);
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.$mensageiro->getFirstMensagem()->getSt_arquivoretorno().'.xls"');

        echo $this->view->render('/arquivo-retorno/xls.phtml');

    }

    private  function preparaDadosXLS($arrVwRetornoTo){
        $valorTotal = 0;
        /** @var $to VwRetornoLancamentoTO */
        foreach($arrVwRetornoTo as &$to){
            $valorTotal = $valorTotal+$to['nu_valor'];
        }
        /** @var $to VwRetornoLancamentoTO */
        foreach($arrVwRetornoTo as &$to){
            $to['bl_quitado'] =  $to['bl_quitado'] ? 'SIM' : 'N&Atilde;O';
            if (!is_null($to['dt_quitado'])){
                $dt_quitado = new \DateTime($to['dt_quitado']);
                $to['dt_quitado'] = $dt_quitado->format('d/m/Y');
            }
            if (!is_null($to['dt_vencimento'])){
                $dt_quitado = new \DateTime($to['dt_vencimento']);
                $to['dt_vencimento'] = $dt_quitado->format('d/m/Y');
            }
            if($to['nu_valorretorno']){
                $to['nu_valorretorno'] = number_format($to['nu_valorretorno'], 2, ',', '.');
            }
            if($to['nu_valor']){
                $to['nu_valor'] = number_format($to['nu_valor'], 2, ',', '.');
            }
            if($to['nu_valornominal']){
                $to['nu_valornominal'] = number_format($to['nu_valornominal'], 2, ',', '.');
            }
                $to['nu_valortotal'] = number_format($valorTotal, 2, ',', '.');
        }

        return $arrVwRetornoTo;
    }

    public function retornaArquivoRetornoAction(){
        $arquivoRetorno = new ArquivoRetornoTO();
        $arquivoRetorno->setId_arquivoretorno($this->_getParam('idAquivoRetorno', null));
        $arquivoRetornoBO = new ArquivoRetornoBO();
        $this->_helper->json($arquivoRetornoBO->listarArquivoRetorno($arquivoRetorno));
    }

    public function quitarLancamentosArquivoRetornoAction(){

        set_time_limit(0);
        ini_set('max_execution_time', 0); //0=NOLIMIT
        ini_set('default_socket_timeout', 60000);

        $arquitoTO = new ArquivoTO();
        $bo = new \ArquivoRetornoBO();

        $id_arquivoretorno = $this->getParam('id_arquivoretorno');

        $vwRetornoLancamentoTO = new VwRetornoLancamentoTO();
        $vwRetornoLancamentoTO->setId_arquivoretorno($id_arquivoretorno);
        $vwRetornoLancamentoTO->setSt_carteira(109);
        $vwRetornoLancamentoTO->setBl_ativo(1);
        $vwRetornoLancamentoTO->setBl_quitadoretorno(0);

        // daqui em diante está sendo feito em doctrine.
        $mensageiroArl = $bo->retornarVwRetornoLancamento($vwRetornoLancamentoTO);
        if($mensageiroArl->getTipo() == \Ead1_IMensageiro::ERRO){
            $this->_helper->json($mensageiroArl);
        }

        $mensageiroQlar = $bo->quitarLancamentosArquivoRetorno($mensageiroArl->getMensagem());

        if($mensageiroQlar->getTipo() == \Ead1_IMensageiro::ERRO){
            $this->_helper->json($mensageiroQlar);
        }
        $arquivoRetorno = new ArquivoRetornoTO();
        $arquivoRetorno->setId_arquivoretorno($id_arquivoretorno);

        $this->_helper->json($bo->listarArquivoRetorno($arquivoRetorno));
    }
}

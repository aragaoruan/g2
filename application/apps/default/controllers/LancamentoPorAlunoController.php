<?php

use G2\Negocio\AvaliacaoAluno;

/**
 * Controller para lançar nota de provas presenciais por aluno (final e recuperação)
 * @author Débora Castro <debora.castro@unyleya.com.br>
 * @since  05-02-2014
 * @package application
 * @subpackage controller
 */
class Default_LancamentoPorALunoController extends Ead1_ControllerRestrita
{

    /**
     * @var \G2\Negocio\AvaliacaoAluno $negocio
     */
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new AvaliacaoAluno();
    }

    public function indexAction()
    {
        $formPesquisa = new PesquisaLancamentoPorAlunoForm();
        $this->view->form = $formPesquisa;
    }

    public function jsAction()
    {
//        $this->_helper->viewRenderer->setNoRender(true);
//        echo $this->view->render('lancamento-por-aluno/index.js');
        $this->_helper->viewRenderer->setNoRender(true);
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
    }

    public function pesquisaAlunoLancamentoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $result = array();
        $return = null;
        $params = $this->getRequest()->getParams();
        if ($params) {
            $return = $this->negocio->pesquisaAlunoLancamento($params);
            if ($return->getTipo() == Ead1_IMensageiro::SUCESSO) {
                foreach ($return->getMensagem() as $value) {
                    $result[] = array(
                        'st_nomecompleto'         => $value['st_nomecompleto'],
                        'st_areaconhecimento'     => $value['st_areaconhecimento'],
                        'dt_aplicacao'            => ($value['dt_aplicacao'] ? date_format($value['dt_aplicacao'], 'd/m/Y') : 'Sem Data'),
                        'st_aplicadorprova'       => $value['st_aplicadorprova'],
                        'id_aplicadorprova'       => $value['id_aplicadorprova'],
                        'st_projetopedagogico'    => $value['st_projetopedagogico'],
                        'id_projetopedagogico'    => $value['id_projetopedagogico'],
                        'id_avaliacaoagendamento' => $value['id_avaliacaoagendamento'],
                        'st_situacao'             => $value['st_situacao'],
                        'st_evolucao'             => $value['st_evolucao'],
                        'id_matricula'            => $value['id_matricula'],
                        'nu_valor'                => $value['nu_valor']
                    );
                }
                $return->setMensagem($result);
            }
        }
        $this->getResponse()->appendBody(json_encode($return));
    }

    public function findAlunoAvaliacaoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $result = array();
        $params = $this->getRequest()->getParams();
        unset($params['controller'], $params['action'], $params['module']);

        $retorno = $this->negocio->findByAlunoAvaliacao($params);
        if (is_array($retorno)) {
            foreach ($retorno as $value) {
                $result[] = array(
                    'id_avaliacaoaluno' => $value->getId_avaliacaoaluno(),
                    'id_avaliacao' => $value->getId_avaliacao(),
                    'id_avaliacaoagendamento' => $value->getId_avaliacaoagendamento(),
                    'id_matricula' => $value->getId_matricula(),
                    'id_disciplina' => $value->getId_disciplina(),
                    'id_avaliacaorecupera' => $value->getId_avaliacaorecupera(),
                    'st_disciplina' => ($value->getSt_tituloexibicaodisciplina() ? $value->getSt_tituloexibicaodisciplina() : ''),
                    'st_nota' => $value->getSt_nota() === '' || is_null($value->getSt_nota()) ? '' : (int)$value->getSt_nota(),
                    'st_usuariocadavaliacao' => ($value->getSt_usuariocadavaliacao() ? $value->getSt_usuariocadavaliacao() : ''),
                    'st_evolucaodisciplina' => ($value->getSt_evolucaodisciplina() ? $value->getSt_evolucaodisciplina() : ''),
                    'dt_avaliacao' => ($value->getDt_avaliacao() ? date_format($value->getDt_avaliacao(), 'd/m/Y') : 'Sem Data'),
                    'dt_cadastroavaliacao' => ($value->getDt_cadastroavaliacao() ? date_format($value->getDt_cadastroavaliacao(), 'd/m/Y') : 'Sem Data'),
                    'bl_provaglobal' => ($value->getBl_provaglobal()),
                    'id_avaliacaoconjuntoreferencia' => ($value->getId_avaliacaoconjuntoreferencia()),
                    'bl_agendamento' => ($value->getBl_agendamento()),
                    'id_provasrealizadas' => $value->getId_provasrealizadas(),
                    'nu_notamax' => $value->getNu_notamax());
            }
        }
        $this->_helper->json($result);
    }

    public function findHistoricoAlunoAvaliacaoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $result = array();

        $parametros = $this->getParam('params');
        $retorno = $this->negocio->findByHistoricoAlunoAvaliacao($parametros);
        if (is_array($retorno)) {
            foreach ($retorno as $value) {
                $result[] = array(
                    'id_avaliacaoaluno' => $value->getId_avaliacaoaluno(),
                    'id_avaliacao' => $value->getId_avaliacao(),
                    'id_disciplina' => $value->getId_disciplina(),
                    'st_disciplina' => ($value->getSt_tituloexibicaodisciplina() ? $value->getSt_tituloexibicaodisciplina() : ''),
                    'st_nota' => ($value->getSt_nota() ? $value->getSt_nota() : ''),
                    'st_usuariocadavaliacao' => ($value->getSt_usuariocadavaliacao() ? $value->getSt_usuariocadavaliacao() : ''),
                    'dt_avaliacao' => ($value->getDt_avaliacao() ? date_format($value->getDt_avaliacao(), 'd/m/Y') : 'Sem Data'),
                    'dt_cadastroavaliacao' => ($value->getDt_cadastroavaliacao() ? date_format($value->getDt_cadastroavaliacao(), 'd/m/Y') : 'Sem Data'),
                    'bl_provaglobal' => ($value->getBl_provaglobal()),
                    'id_avaliacaoconjuntoreferencia' => ($value->getId_avaliacaoconjuntoreferencia()));
            }
        }
//        Zend_Debug::dump($result);
        $this->_helper->json(array_reverse($result));
    }

    public function lancarNotaFinalAction()
    {
        $arrRetorno = array();
        $params['id_matricula'] = $this->getParam('id_matricula');
        $params['st_nota'] = $this->getParam('st_nota');
        $params['id_avaliacao'] = $this->getParam('id_avaliacao');
        $params['id_avaliacaoagendamento'] = $this->getParam('id_avaliacaoagendamento');
        if ($this->getParam('id_avaliacaoconjuntoreferencia')) {
            $params['id_avaliacaoconjuntoreferencia'] = $this->getParam('id_avaliacaoconjuntoreferencia');
        }

        $result = $this->negocio->salvarNotaAvaliacaoPresencial($params);
        $resultEmail = null;
        if ($result->type == Ead1_IMensageiro::TYPE_SUCESSO){
            $resultEmail = $this->negocio->notificarAluno($params['id_matricula']);
        }

        $arrRetorno['result'] = $result;
        $arrRetorno['resultEmail'] = $resultEmail;


        $this->_helper->json($arrRetorno);
    }

    public function retornaProvasAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $result = $this->negocio->findByProvasCadastradas(array('id_matricula' => $this->_getParam('id_matricula')));
        $arrReturn = array();
        if ($result) {
            foreach ($result as $row) {
                $arrReturn[] = array(
                    'id_avaliacao' => $row->getId_avaliacao(),
                    'st_avaliacao' => $row->getSt_avaliacao(),
                    'id_avaliacaorecupera' => $row->getId_tipodeavaliacao(),
                    'id_matricula' => $row->getId_matricula(),
                    'bl_provaglobal' => $row->getBl_provaglobal()
                );
            }
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    public function retornaMatriculaNotaAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $result = $this->negocio->findVwMatriculaNota($this->_getParam('params'));
        $arrReturn = array();
        if ($result) {
            foreach ($result as $row) {
                $arrReturn[] = array(
                    'id_matriculadisciplina' => $row->getId_matriculadisciplina(),
                    'id_avaliacao' => $row->getId_avaliacaofinal(),
                    'st_disciplina' => $row->getSt_disciplinafinal(),
                    'st_notaead' => (int)($row->getSt_notaead()),
                    'st_notafinal' => (int)$row->getSt_notafinal(),
                    'nu_notatotal' => $row->getNu_notatotal()
                );
            }
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    public function getAction()
    {
        $this->getResponse()
            ->appendBody("From getAction() returning the requested article");
    }

    public function postAction()
    {
        $this->getResponse()
            ->appendBody("From postAction() creating the requested article");
    }

    public function putAction()
    {
        $this->getResponse()
            ->appendBody("From putAction() updating the requested article");
    }

}

<?php

/**
 * Classe de controller Prorrogação
 * @author Neemias Santos <neemias.santos@gmail.com>
 * Date: 21/07/15
 * Time: 15:10
 */
class Default_ProrrogacaoSalaController extends Ead1_ControllerRestrita
{

    private $negocio;
    public function init()
    {
        parent::init();
        parent::setupDoctrine();
        $this->negocio = new \G2\Negocio\Matricula();
    }

    public function indexAction()
    {

    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('prorrogacao-sala/index.js');
    }

    public function retornaDisciplinaAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $negocio = new \G2\Negocio\VwModuloDisciplina();
        $params = $this->limpaGetAllParams($this->getRequest()->getParams());
        $array = array();

        foreach ($negocio->findByVwDisciplina($params) as $key => $value) {
            $array[$key] = $negocio->toArrayEntity($value);
        }

        $this->_helper->json($array);
    }

    public function retornaSalaAulaAction(){
        $this->_helper->viewRenderer->setNoRender(true);
        $negocio = new \G2\Negocio\SalaDeAula();
        $params = $this->limpaGetAllParams($this->getRequest()->getParams());

        $result = $negocio->retornaSalaDeAulaProrrogaSalaAula($params);

        $this->_helper->json($result);
    }

    public function retornaMatriculaParamsAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        $params = $this->limpaGetAllParams($this->getRequest()->getParams());

        $result = $this->negocio->retornaVwMatriculaAlocacao($params);

        $this->_helper->Json($result);

    }

    public function prorrogaAlunosSalaAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        $params = $this->limpaGetAllParams($this->getRequest()->getParams());

        $result = $this->negocio->alteraDataTerminoSala($params);

        $this->_helper->Json($result);

    }
}
<?php

/**
 * Class MotivoController
 * Controller para Motivo
 * @author helder.silva <helder.silva@unyleya.com.br>
 * @since 2015-01-09
 * @package application
 * @subpackage controller
 */
class Default_MotivoController extends Ead1_ControllerRestrita
{
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Motivo();
    }

    public function indexAction()
    {
        $id = $this->getParam('id');
        $return = array();
        if ($id) {
            $result = $this->negocio->find('\G2\Entity\Motivo', $id);
            if ($result) {
                $return = $this->negocio->toArrayEntity($result);
                $return['id_situacao'] = $result->getId_situacao() ? $result->getId_situacao()->getId_situacao() : NULL;
                $return['id_entidadecadastro'] = $result->getId_entidadecadastro() ? $result->getId_entidadecadastro()->getId_entidade() : NULL;
                $return['id_usuariocadastro'] = $result->getId_usuariocadastro() ? $result->getId_usuariocadastro()->getId_usuario() : NULL;
                $return['id'] = $result->getId_motivo();
            }
        }
        $this->view->motivo = json_encode($return);
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
    }

    public function saveAction()
    {
        $parametros = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocio->salvarMotivo($parametros);
        $resultado = $this->negocio->toArrayEntity($result);
        $resultado['id_situacao'] = $result->getId_situacao() ? $result->getId_situacao()->getId_situacao() : NULL;
        $resultado['id_entidadecadastro'] = $result->getId_entidadecadastro() ? $result->getId_entidadecadastro()->getId_entidade() : NULL;
        $resultado['id_usuariocadastro'] = $result->getId_usuariocadastro() ? $result->getId_usuariocadastro()->getId_usuario() : NULL;
        $resultado['id'] = $result->getId_motivo();
        $this->_helper->json($resultado);
    }
}
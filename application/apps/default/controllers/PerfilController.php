<?php

/**
 * Controller para Perfil
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2014-11-27
 * @package application
 * @subpackage controller
 */


class Default_PerfilController extends Ead1_ControllerRestrita
{
    private $negocio;

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('perfil/index.js');
    }

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Negocio();
    }

    public function indexAction()
    {
    }

//    public function getPerfilPedagogicoAction() {
//        $this->_helper->viewRenderer->setNoRender(true);
//
//        $params = $this->getAllParams();
//        unset($params['module'], $params['controller'], $params['action'], $params['order_by'], $params['sort']);
//
//        if (!isset($params['id_entidade']) || !$params['id_entidade'])
//            $params['id_entidade'] = $this->negocio->sessao->id_entidade;
//
//        $mensageiro = $this->negocio->findBy('\G2\Entity\Perfil', $params);
//
//        $results = array();
//        foreach ($mensageiro as $entity) {
//            if ($entity->getId_perfilpedagogico()) {
//                $idPP = $entity->getId_perfilpedagogico()->getId_perfilpedagogico();
//                $stPP = $entity->getId_perfilpedagogico()->getSt_perfilpedagogico();
//                $results[$stPP . $idPP] = array(
//                    'id_perfilpedagogico' => $idPP,
//                    'st_perfilpedagogico' => $stPP
//                );
//            }
//        }
//        ksort($results);
//        $this->_helper->json(array_values($results));
//    }

    public function getPerfilEntidadeAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        $idPerfil = $this->getParam('id_perfil');

        if ($idPerfil) {
            $to = new PerfilEntidadeTO();
            $to->setId_perfil($idPerfil);

            $bo = new PerfilBO();
            $mensageiro = $bo->retornarPerfilEntidadeCompleto($to);

            // Necessario criar funcao dentro da funcao para poder usar o USORT
            // porque o USORT nao usa o $this
            function _sortPerfilEntidade($a, $b) {
                if ($a->getSt_nomeentidade() == $b->getSt_nomeentidade()) {
                    return 0;
                }
                return ($a->getSt_nomeentidade() < $b->getSt_nomeentidade()) ? -1 : 1;
            }

            // ORDENAR
            if (isset($mensageiro->type) && $mensageiro->type == 'success' && $mensageiro->mensagem) {
                usort($mensageiro->mensagem, '_sortPerfilEntidade');
            }

            $this->_helper->json($mensageiro);
        }
    }

    public function getArvoreSelectedAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        $idPerfil = $this->getParam('id_perfil');

        if ($idPerfil) {
            $to = new PerfilEntidadeTO();
            $to->setId_perfil($idPerfil);

            $bo = new PerfilBO();
            $mensageiro = $bo->retornarPerfilEntidade($to);

            $this->_helper->json($mensageiro);
        }
    }

    public function getPessoasDisponiveisPerfilAction(){
        $this->_helper->viewRenderer->setNoRender(true);

        $idPefil = $this->getParam('id_perfil');
        $stNomeCompleto = $this->getParam('st_nomecompleto');

        if ($idPefil && $stNomeCompleto) {
            $to = new VwUsuarioPerfilEntidadeTO();
            $to->setSt_nomecompleto($stNomeCompleto);
            $to->setId_perfil($idPefil);

            $bo = new PerfilBO();
            $mensageiro = $bo->retornaPessoasDisponiveisPerfil($to);

            $this->_helper->json($mensageiro);
        }
    }

    public function getFuncionalidadeSelectedAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        $idPefil = $this->getParam('id_perfil');

        if ($idPefil) {
            $to = new PerfilFuncionalidadeTO();
            $to->setId_perfil($idPefil);
            $to->setBl_ativo(true);

            $bo = new PerfilBO();
            $mensageiro = $bo->retornaFuncionalidadePerfil($to);

            $this->_helper->json($mensageiro);
        }
    }

    public function getPermissaoPerfilAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        $idPefil = $this->getParam('id_perfil');

        if ($idPefil) {
            $to = new PerfilTO();
            $to->setId_perfil($idPefil);
            $to->setBl_ativo(true);
            $to->setBl_padrao(false);

            $bo = new PerfilBO();
            $mensageiro = $bo->retornaPermissoesPerfil($to);

            if ($mensageiro->getMensagem() && @$mensageiro->getType() == 'success') {
                foreach ($mensageiro->getMensagem() as $key => $item) {
                    $mensageiro->mensagem[$key]['breadcrumb'] = $item['breadcrumb'];
                }
            }

            $this->_helper->json($mensageiro);
        }
    }

    public function getArvoreEntidadeFuncionalidadeAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        $idSistema = $this->getParam('id_sistema');

        if ($idSistema) {
            $to = new VwEntidadeFuncionalidadeTO();
            $to->setId_sistema($idSistema);
            $to->setBl_obrigatorio(true);
            $to->setBl_funcionalidadeativo(true);
            $to->setBl_entidadefuncionalidadeativo(true);
            $to->setBl_visivel(true);

            $bo = new PerfilBO();
            $results = $bo->retornaArvoreEntidadeFuncionalidade($to);

            if (isset($results->mensagem)) {
                $html = $this->_htmlArvoteItems($results->mensagem);
                echo $html;
            }
        }
    }

    public function _htmlArvoteItems($items) {
        $html = '';

        foreach ($items as $item) {
            $html .= "<li id='" . $item['to']->id_funcionalidade . "' >";
            $html .= "<a>{$item['label']}</a>";
//            $html .= '<i class="icon icon-check select-all"></i>';
            if ($item['children']) {
                $html .= '<ul>';
                $html .= $this->_htmlArvoteItems($item['children']);
                $html .= '</ul>';
            }
            $html .= '</li>';
        }

        return $html;
    }

    /**
     * Action que retona um ou mais perfis buscando por parte do nome do perfil usando
     * like, alem de outros parametros correspondentes aos atributos da vw_perfilentidaderelacao,
     * este metodo usa o Repository de Perfil com metodo especifico para esta consulta.
     *
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     */
    public function retornaVwPerfilEntidadeRelacaoParteAction(){
        $array = array();

        $perfilNegocio = new \G2\Negocio\Perfil();
        $params = $this->getRequest()->getParams();

        //retirando parametros nao necessarios para consulta
        unset($params['module'], $params['action'], $params['controller']);

        $result = $perfilNegocio->retornaVwPerfilEntidadeRelacaoParteNomePerfil($params);

        foreach ($result as $key => $value){
            $array[] = $perfilNegocio->toArrayEntity($value);
        }

        $this->_helper->json($array);
    }

}

<?php
/**
 * User: wellington.gomes
 * Date: 17/04/2018
 * Time: 17:42
 */


class Default_RelatorioLogAlunoController extends Ead1_ControllerRestrita
{
    private $negocio;
    private $relatorio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\SalaDeAula();
        $this->relatorio = new \G2\Negocio\Relatorio();
    }

    public function indexAction()
    {

    }

    public function salasAction()
    {
        $id_matricula = $this->_getParam('id_matricula');
        $retorno = $this->negocio->retornaSalaPorMatricula($id_matricula);
        return $this->_helper->json($retorno);
    }

    public function logsAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $retorno = $this->relatorio->relatorioLogAluno($params);
        return $this->_helper->json($retorno);
    }

    public function geraxlsAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $retorno = $this->relatorio->relatorioLogAluno($params);

        if (!$retorno) {
            return $this->_helper->json('Dados não encontrados.');
        }
        $this->view->dados_aluno = [
            "aluno" => $retorno[0]['st_nomecompleto'],
            'cpf' => \G2\Utils\Helper::mask($retorno[0]['st_cpf'], "###.###.###-##"),
            'curso' => $retorno[0]['st_projetopedagogico'],
            'email' => $retorno[0]['st_email'],
            'entidade' => $retorno[0]['st_nomeentidade']
        ];
        $this->view->result = $retorno;
    }
}

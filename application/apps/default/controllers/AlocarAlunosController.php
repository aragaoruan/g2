<?php


/**
 * Controller para alocar alunos
 * @author Débora Castro <debora.castro@unyleya.com.br>
 * @since  09-04-2014
 * @package application
 * @subpackage controller
 */
class Default_AlocarAlunosController extends Ead1_ControllerRestrita
{

    /**
     * @var \G2\Negocio\Alocacao $negocio
     */
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Alocacao();
    }

    public function indexAction()
    {
//        $formPesquisa = new PesquisaLancamentoPorAlunoForm();
//        $this->view->form = $formPesquisa;
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('alocar-alunos/index.js');
    }

    public function retornaModulosAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $result = $this->negocio->findByModulos($this->_getParam('id_projeto'));
        $arrReturn = array();
        if ($result) {
            foreach ($result as $row) {
                $arrReturn[] = array(
                    'id_modulo' => $row->getId_modulo(),
                    'st_modulo' => $row->getSt_modulo()
                );
            }
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    public function retornaSalaAlocacaoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $params['id_matricula'] = $this->_getParam('id_matricula');
        $params['id_disciplina'] = $this->_getParam('id_disciplina');
        $params['id_categoriasala'] = $this->_getParam('id_categoriasala');

        $result = $this->negocio->findByVwSalaDisciplinaAlocacao($params, $this->_getParam('bl_encerradas', false));
        $arrReturn = array();
        if ($result) {
            foreach ($result as $row) {
                $arrReturn[] = array(
                    'id_saladeaula' => $row->getId_saladeaula(),
                    'st_saladeaula' => $row->getSt_saladeaula(),
                    'id_matriculadisciplina' => $row->getId_matriculadisciplina(),
                    'dt_abertura' => ($row->getDt_abertura() ? date_format($row->getDt_abertura(), 'd/m/Y') : 'Sem Data')
                );
            }
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    public function retornaDadosAlocacaoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $params['id_matricula'] = $this->_getParam('id_matricula');
        $params['id_projetopedagogico'] = $this->_getParam('id_projetopedagogico');
        $result = $this->negocio->findByVwAlocacaoAlunoSala($params);

        $disciplinaConcedida = \G2\Constante\Situacao::TB_MATRICULADISCIPLINA_CONCEDIDA;

        $arrReturn = array();
        if ($result) {
            foreach ($result as $row) {
                $arrReturn[] = array(
                    'id_saladeaulanormal'            => $row->getId_saladeaulanormal(),
                    'st_saladeaulanormal'            => ($row->getId_situacaomatriculadisciplina() == $disciplinaConcedida ? 'Disciplina Isenta' :
                                                                    ($row->getSt_saladeaulanormal() ? $row->getSt_saladeaulanormal().' - ' : 'Sem Alocação - ')),
                    'id_alocacaonormal'              => $row->getId_alocacaonormal(),
                    'id_disciplina'                  => $row->getId_disciplina(),
                    'st_disciplina'                  => $row->getSt_disciplina(),
                    'id_tipodisciplina'              => $row->getId_tipodisciplina(),
                    'id_matricula'                   => $row->getId_matricula(),
                    'id_alocacaoprr'                 => $row->getId_alocacaoprr(),
                    'id_saladeaulaprr'               => $row->getId_saladeaulaprr(),
                    'st_saladeaulaprr'               => ($row->getId_situacaomatriculadisciplina() == $disciplinaConcedida || $row->getId_situacaomatriculadisciplina() == $disciplinaConcedida ?
                                                                    'Disciplina Isenta': ($row->getSt_saladeaulaprr() ? $row->getSt_saladeaulaprr().' - ' : 'Sem Alocação - ')),
                    'id_matriculadisciplinanor'      => $row->getId_matriculadisciplinanor(),
                    'id_matriculadisciplinaprr'      => $row->getId_matriculadisciplinaprr(),
                    'dt_aberturaprr'                 => ($row->getId_situacaomatriculadisciplina() == $disciplinaConcedida || $row->getId_situacaomatriculadisciplina() == $disciplinaConcedida ?
                                                                    '' : ($row->getDt_aberturaprr() ? date_format($row->getDt_aberturaprr(), 'd/m/Y') : ' Sem Data')),
                    'dt_aberturanormal'              => ($row->getId_situacaomatriculadisciplina() == $disciplinaConcedida || $row->getId_situacaomatriculadisciplina()== $disciplinaConcedida ?
                                                                    '' : ($row->getDt_aberturanormal() ? date_format($row->getDt_aberturanormal(), 'd/m/Y') : ' Sem Data')),
                    'dt_inicioalocacaonormal'        => ($row->getDt_inicioalocacaonormal() ? date_format($row->getDt_inicioalocacaonormal(), 'd/m/Y') : date("d/m/y")),
                    'dt_inicioalocacaoprr'           => ($row->getDt_inicioalocacaoprr() ? date_format($row->getDt_inicioalocacaoprr(), 'd/m/Y') : date("d/m/y")),
                    'nu_diasextensaonormal'          => $row->getNu_diasextensaonormal(),
                    'nu_diasalunonormal'             => $row->getNu_diasalunonormal(),
                    'nu_diasextensaoprr'             => $row->getNu_diasextensaoprr(),
                    'nu_diasalunoprr'                => $row->getNu_diasalunoprr(),
                    'nu_diasacessoatualp'            => $row->getNu_diasacessoatualp(),
                    'nu_diasacessoatualn'            => $row->getNu_diasacessoatualn(),
                    'id_tiposalanormal'              => $row->getId_tiposalanormal(),
                    'id_tiposalaprr'                 => $row->getId_tiposalaprr(),
                    'id_situacaomatriculadisciplina' => $row->getId_situacaomatriculadisciplina(),
                    'id_situacaoproibidaparaalocacao'=> $disciplinaConcedida ,
                    'id_evolucaomatriculadisciplina' => $row->getId_evolucaomatriculadisciplina()
                );
            }
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    public function validarAlocacaoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $params['id_saladeaula'] = $this->_getParam('id_saladeaula');
        $params['id_matriculadisciplina'] = $this->_getParam('id_matriculadisciplina');
        $params['id_categoriasala'] = $this->_getParam('id_categoriasala');
        $params['id_tipodisciplina'] = $this->_getParam('id_tipodisciplina');

        $result = $this->negocio->validarAlocacao($params);

        $this->_helper->json($result);
    }

    public function salvarOpcoesSalaAction()
    {

        $result = $this->negocio->salvarOpcoesSala($this->getAllParams());
//        Zend_Debug::dump($result);die;
        $this->_helper->json($result);
    }

    public function salvarAlocacaoAction()
    {
        $result = $this->negocio->salvarAlocacao($this->getAllParams());
        $msg = "";

        if ($result->getType() === \Ead1_IMensageiro::TYPE_SUCESSO
            && $this->getParam("id_categoriasala") == SalaDeAulaTO::SALA_PRR) {

            $msg = $this->negocio->notificarEnvolvidos($this->limpaGetAllParams($this->getAllParams()));
        }

        $retorno = array(
            'alocacao' => $result,
            'email' => $msg
        );

        $this->_helper->json($retorno);
    }


    public function sincronizarAutoAlocarAction()
    {

        $params['id_matricula'] = $this->_getParam('id_matricula');
        $params['id_projetopedagogico'] = $this->_getParam('id_projetopedagogico');

        $result = $this->negocio->sincronizarAutoAlocar($params);
//        Zend_Debug::dump($result);die;
        $this->_helper->json($result);
    }

    public function reintegrarAlocacaoAction()
    {

        $params['id_saladeaula'] = $this->_getParam('id_saladeaula');
        $params['id_matriculadisciplina'] = $this->_getParam('id_matriculadisciplina');
        $params['id_alocacao'] = $this->_getParam('id_alocacao');

        $result = $this->negocio->reintegrarAlocacao($params);
//        Zend_Debug::dump($result);die;
        $this->_helper->json($result);
    }


    public function desalocarAlunoAction()
    {
        $result = $this->negocio->desalocarAluno($this->getAllParams());
        $this->_helper->json($result);
    }

    public function getAction()
    {
        $this->getResponse()
            ->appendBody("From getAction() returning the requested article");
    }

    public function postAction()
    {
        $this->getResponse()
            ->appendBody("From postAction() creating the requested article");
    }

    public function putAction()
    {
        $this->getResponse()
            ->appendBody("From putAction() updating the requested article");
    }

}

<?php

/**
 * Controller Relatorio Controle Turma Grade Horaria
 * @author Neemiuas Santos <neemias.santos@unyleya.com.br>
 * @since 2015-12-17
 * @package application
 * @subpackage controller
 */
class Default_RelatorioControleTurmaGradeHorariaController extends Ead1_ControllerRestrita
{
    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $js = $this->view->render('relatorio-controle-turma-grade-horaria/index.js');
        echo $this->view->headScript()->appendScript($js);
    }

    public function pesquisaDadosAction(){
        $params = $this->limpaGetAllParams($this->getRequest()->getParams());

        $negocioGradeHoraria = new \G2\Negocio\GradeHoraria();

        $dados = $negocioGradeHoraria->relatorioControleTurmaGradeHoraria($params);

        $this->view->dados = $dados;

    }


}
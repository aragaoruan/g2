<?php

/**
 * Classe de controller MVC para controlar Periodo Letivo
 * @author Felipe Pastor <felipepastorff@gmail.com>
 *
 * @package application
 * @subpackage controller
 */
class Default_PeriodoLetivoController extends Ead1_ControllerRestrita
{

    public function init()
    {
        parent::init();
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('periodo-letivo/index.js');
    }

    public function indexAction()
    {

    }
}

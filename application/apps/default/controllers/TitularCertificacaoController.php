<?php

/**
 * Controller para TitularCertificacao
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2015-02-05
 * @package application
 * @subpackage controller
 */


class Default_TitularCertificacaoController extends Ead1_Controller
{

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('titular-certificacao/index.js');
    }

    public function init()
    {
    }

    public function indexAction()
    {
    }

    public function getProjetoByAreaAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        $negocio = new \G2\Negocio\ProjetoPedagogico();

        $mensageiro = $negocio->findByAreaConhecimentoEntidade($this->getParam('id_areaconhecimento'));
        $results = array();
        foreach ($mensageiro as $entity) {
            $results[] = $negocio->toArrayEntity($entity);
        }

        if (!$results) {
            $results = new \Ead1_Mensageiro();
            $results->setMensageiro('Nenhum projeto pedagógico encontrado', \Ead1_IMensageiro::AVISO, null);
        }

        $this->_helper->json($results);
    }


    public function getTitulacaoAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        $negocio = new \G2\Negocio\ProjetoPedagogico();
        $mensageiro = $negocio->findAll('G2\Entity\Titulacao');
        $results = array();
        foreach ($mensageiro as $entity) {
            $results[] = $negocio->toArrayEntity($entity);
        }
        $this->_helper->json($results);
    }

} 
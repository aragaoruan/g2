<?php

/**
 * Classe controller para Pagamento de Colaborador
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-04-30
 * @package default
 * @subpackage controllers
 */
class Default_PagamentoColaboradorController extends Ead1_ControllerRestrita
{

    /**
     * @var \G2\Negocio\PagamentoColaborador
     */
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\PagamentoColaborador();
    }

    public function indexAction()
    {
        echo $this->view->headScript()->appendFile('/pagamento-colaborador/js');
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
    }

    public function retornarDisciplinasAction()
    {
        $idUsuario = $this->getParam('id_usuario');
        $result = $this->negocio->retornaDisciplinasColaborador($idUsuario);
        $this->_helper->json($result);
    }

    public function creditarAdiantamentoAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json_Decoder::decode($body);
        $result = $this->negocio->salvarAdiantamento($data);
        if ($result) {
            $this->getResponse()->setHttpResponseCode(201)->appendBody(json_encode($result));
        } else {
            $this->getResponse()->setHttpResponseCode(400)->appendBody('Erro ao tentar salvar.');
        }
    }

    public function detalharAction()
    {
        $id = $this->getParam('id');
        $id_funcao = $this->getParam('perfil');
        $result = $this->negocio->retornarUsuariosComissionados(array('id_usuario' => $id, 'id_funcao' => $id_funcao), true);
        $arrReturn = array();
        if ($result) {
            foreach ($result as $i => $row) {
                $arrReturn = $row;
                $arrReturn['id'] = $row['id_usuario'];
            }
        }
        $this->view->modelColaborador = $arrReturn;
        $this->view->anos = $this->retornarAnosSelect();
        echo $this->view->headScript()->appendFile('/pagamento-colaborador/detalhar-js');
    }

    public function retornaExtratoAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = array();
        if ($params['id_perfil'] == 2) {//coordenador de projeto
            $result = $this->negocio->retornarExtratoDetalhadoCoordenadorProjeto($params);
        } elseif ($params['id_perfil'] == 4) {//coordenador de disciplina
            $result = $this->negocio->retornarExtratoDetalhadoCoordenadorDisciplina($params);
        }

        $this->_helper->json($result);
    }

    /**
     * Retorna array com anos abaixo do atual
     * @return array
     */
    private function retornarAnosSelect()
    {
        $anoAtual = intval(date("Y"));
        $anoIni = $anoAtual - 10;
        $arrAnos = array();
        for ($i = $anoAtual; $i >= $anoIni; $i--) {
            $arrAnos[] = $i;
        }
        return $arrAnos;
    }

    public function detalharJsAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        echo $this->view->render($this->getRequest()->getControllerName() . '/detalhar.js');
    }

    public function salvarPagamentoAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $decodeRawBody = Zend_Json::decode($this->getRequest()->getRawBody());
        $result = $this->negocio->salvarArrayLancamentosPagamentoColaborador($decodeRawBody);
        if ($result) {
            $this->getResponse()->appendBody(json_encode($result));
            $this->getResponse()->setHttpResponseCode(201);
        } else {
            $this->getResponse()->appendBody("Erro ao tentar salvar o pagamento.");
            $this->getResponse()->setHttpResponseCode(400);
        }
    }

    public function gerarXlsAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        //Recupera os parametros
        $params = $this->limpaGetAllParams($this->getAllParams());
        $pessoaNegocio = new \G2\Negocio\Pessoa();
        $sessao = new Zend_Session_Namespace('geral'); //pega os dados da sessão
        $loginBO = new LoginBO(); //instancia a bo de login
        $xls = new Ead1_GeradorXLS(); //Instancia o XLS

        //recupera os dados do usuario logado
        $usuario = $loginBO->retornaDadosUsuario($sessao->id_usuario, $sessao->id_entidade, $sessao->id_perfil);
        $usuarioTO = $usuario->getFirstMensagem(); //Pega a primeira mensagem do mensageiro
        $pessoa = $pessoaNegocio->retornaPessoaById($params['id']);


        //Monta o array do cabeçalho
        $cabecalhos = array(
            'dt_vencimento' => 'Data',
            'st_projetopedagogico' => 'Projeto Pedagogico',
            'st_disciplina' => 'Disciplina',
            'st_tipo' => 'Tipo (R$)',
            'nu_valor' => 'Saldo (R$)'
        );

        $arXlsHeaderTO = array();
        foreach ($cabecalhos as $key => $value) {
            $xlsHeaderTO = new XLSHeaderTO();
            $xlsHeaderTO->setSt_header($value);
            $xlsHeaderTO->setSt_par($key);
            $arXlsHeaderTO[] = $xlsHeaderTO;
        }
        //Busca os dados
        $result = $this->negocio->retornarExtratoDetalhadoColaborador($params);

        //Verifica se retornou resultado
        if ($result) {
            $arrParametroTO = Ead1_TO_Dinamico::encapsularTo($result, new ParametroRelatorioTO(), false, false, true);
            $x = new XLSConfigurationTO();
            $x->setTitle("Extrato Colaborador - " . $pessoa->getSt_nomeexibicao());
            $x->setFooter('Gerado por: ' . $usuarioTO->getSt_nomecompleto() . ' Data: ' . date('d/m/Y H:i:s'));
            $x->setFilename('extrato-colaborador');
            $xls->geraXLS($arXlsHeaderTO, $arrParametroTO, $x);
        }
    }

    public function retornaAutorizacoesAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocio->retornaUsuariosAutorizacao($params);
        return $this->_helper->json($result);
    }

    public function autorizarPagamentoAction()
    {
        $decodeRawBody = Zend_Json::decode($this->getRequest()->getRawBody());
        $result = $this->negocio->efetuaAutorizacaoPagamento($decodeRawBody);
        $this->_helper->json($result->toArrayAll());
    }

    public function retornarPagamentosEfetuadosExtratoAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocio->retornaListaPagamentosEfetuados($params);
        $this->_helper->json($result);
    }

    public function retornarPagamentosEfetuadosAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocio->retornarLancamentosAutorizados($params);
        $this->_helper->json($result);
    }

}

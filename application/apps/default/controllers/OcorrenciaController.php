<?php

/**
 * Class ControllerMVC Ocorrencias
 *
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class Default_OcorrenciaController extends Ead1_ControllerRestrita
{

    /**
     * @var \G2\Negocio\Ocorrencia
     */
    private $_negocio;

    public function init()
    {
        parent::init();
        $this->_negocio = new \G2\Negocio\Ocorrencia();
    }

    /**
     * Action inicial para carrgamento de conteudo PHP da tela inicial.
     */
    public function indexAction()
    {
        $formPesquisa = new PesquisaOcorrenciaForm();
        $formOcorrencia = new OcorrenciaForm();
        $config = Ead1_Ambiente::geral();

        /**
         * Historia AC-26068
         * Desabilitando a opcao ENCERRAMENTO_REALIZADO_ATENDENTE, de acordo
         * com necessidade do cliente para uso do sistema
         */
        $formOcorrencia->getElement('id_evolucao')
            ->setAttribs(array('disable' => array(\G2\Constante\Evolucao::ENCERRAMENTO_REALIZADO_ATENDENTE)))
            ->removeDecorator('Label');

        $this->view->id_usuario = $this->_negocio->sessao->id_usuario;
        $this->view->id_entidade = $this->_negocio->sessao->id_entidade;
        $this->view->st_nomeusuario = $this->_negocio->sessao->nomeUsuario;
        $this->view->formOcorrencia = $formOcorrencia;
        $this->view->formPesquisa = $formPesquisa;

        $this->view->st_url_g2 = $config->st_url_gestor2;
    }

    /**
     * Carrega o script para funcionalide de ocorrencia
     */
    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('ocorrencia/index.js');
    }

    /**
     * Lista assuntos filtrando por entidade, usuario autenticado e id_assuntocopai
     */
    public function findSubAssuntoAction()
    {
        $params = array(
            'id_assuntocopai' => $this->getParam('id_assuntocopai'),
            'id_entidade' => $this->getParam('id_entidade')
        );

        $response = array(array('id' => '', 'id_assuntoco' => '', 'st_assuntoco' => '- Selecione Subassunto -'));

        $results = $this->_negocio->findSubAssuntoCo($params);

        if (is_array($results)) {
            foreach ($results as $value) {
                $response[] = array(
                    'id' => $value->getId_assuntoco(),
                    'id_assuntoco' => $value->getId_assuntoco(),
                    'id_assuntocopai' => $value->getId_assuntocopai(),
                    'id_entidade' => $value->getId_entidade(),
                    'id_situacao' => $value->getId_situacao(),
                    'id_tipoocorrencia' => $value->getId_tipoocorrencia(),
                    'bl_ativo' => $value->getBl_ativo(),
                    'st_assuntoco' => $value->getSt_assuntoco(),
                    'bl_notificaresponsavel' => $value->getBl_notificaresponsavel(),
                    'bl_notificaatendente' => $value->getBl_notificaatendente(),
                    'bl_autodistribuicao' => $value->getBl_autodistribuicao(),
                    'bl_abertura' => $value->getBl_abertura(),
                    'id_textosistema' => $value->getId_textosistema()
                );
            }
        }

        $this->_helper->json($response);
    }

    /**
     * Action responsavel por retornar assuntos de ocorrencias.
     */
    public function findAssuntoAction()
    {
        $params = $this->getParam('params');

        $response = array(array("id_assuntoco" => "", "st_assuntoco" => '- Selecione assunto -'));

        $results = $this->_negocio->findAssuntoCo($params);

        if (is_array($results)) {
            $response = array_merge($response, $results);
        }

        $this->_helper->json($response);
    }

    /**
     * Metodo que recebe um id_ocorrencia e busca informações para impressao
     * de uma ocorrencia.
     */
    public function imprimirOcorrenciaAction()
    {

        if ($this->_getParam('numero') && $this->_negocio->sessao->id_usuario) {

            $bo = new CAtencaoBO();
            $ocorrenciaTO = new VwOcorrenciaTO();
            $ocorrenciaTO->setId_ocorrencia($this->_getParam('numero'));
            $ocorrencia = $bo->retornaVwOcorrenciaPorId($ocorrenciaTO);

            $vwOcorrenciaTO = new VwOcorrenciaTO();
            $vwOcorrenciaTO->setId_ocorrencia($this->_getParam('numero'));
            $interacoes = $bo->retornaInteracoesOcorrencia($vwOcorrenciaTO);

            $entidadeTO = new EntidadeTO();
            $entidadeTO->setId_entidade($ocorrencia->getId_entidade());
            $entidade = new EntidadeBO();
            $entidade->retornaEntidade($entidadeTO);
            $dataEntidade = $entidade->mensageiro->getMensagem();

            $this->view->entidade = $dataEntidade[0];
            $this->view->interacoes = $interacoes->getMensagem();
            $this->view->ocorrencia = $ocorrencia;
        } else {
            $this->view->mensagem = 'Número da ocorrência invalido!';
        }
    }

    /**
     * Metodo responsavel por carregar as funções de um atendente
     */
    public function findNucleoPessoaCoAction()
    {
        $i = 0;
        $arrayData = array();
        $params = $this->limpaGetAllParams($this->getAllParams());

        foreach ($this->_negocio->findFuncaoCo($params) as $value) {
            $value['id'] = $i++;
            $arrayData[] = $value;
        }

        $this->_helper->json($arrayData);
    }

    /**
     * Metodo responsavel por carregar atendentes por função
     */
    public function findAtendenteAction()
    {
        $arrayData = $this->_negocio->findByAtendenteCo($this->getParam('params'));
        $this->_helper->json($arrayData);
    }

    /**
     * Metodo responsavel por carregar os tramites de uma determinada ocorrencia
     */
    public function findInteracaoAction()
    {
        $result = array();
        $params = $this->getRequest()->getParam('params');

        /** @var \G2\Entity\VwOcorrenciaInteracao[] $arrayEntitys */
        $arrayEntitys = $this->_negocio->retornaVwOcorrenciaInteracao($params);

        if ($arrayEntitys) {
            foreach ($arrayEntitys as $entity) {
                /*
                 * Substuindo caracters vindos de banco de dados correspondentes no html a tag <br/>
                 * para formatar valor do campo na visualizacao do usuario.
                 */
                $entity->setSt_tramite(nl2br($entity->getSt_tramite()));
                $result[] = $this->_negocio->toArrayEntity($entity);
            }
        }

        $this->_helper->json($result);
    }

    /**
     * Action responsavel por salvar nova interacao com anexo
     */
    public function novaInteracaoAction()
    {
        try {
            $post = $this->getRequest()->getPost();

            if (array_key_exists('graduacao', $post) && $post['graduacao']) {
                $post['ocorrencia'] = Zend_Json::decode($post['ocorrencia']);
                $arquivo = array_key_exists('arquivoInteracao', $_FILES) ? $_FILES['arquivoInteracao'] : null;
            } else {
                //Verificando se um arquivo foi enviano na interacao
                $arquivo = array_key_exists('arquivo', $_FILES) ? $_FILES['arquivo'] : null;

            }

            //Verificando se a opcao visivel ao interessado esta marcada
            $blVisivel = array_key_exists('bl_visivel', $post) && $post['bl_visivel'] == 1 ? TRUE : FALSE;

            //Preenchendo notificacao da interacao
            $blNotificacao = $blVisivel;

            $result = $this->_negocio->gerarNovaInteracao($post, $arquivo, $blVisivel, $blNotificacao);

            $this->_helper->json($result);
        } catch (Exception $e) {
            $this->_helper->viewRenderer->setNoRender();
            $this->getResponse()->setHttpResponseCode(400)
                ->appendBody(json_encode(array(
                    'title' => 'Erro!',
                    'text' => $e->getMessage(),
                    'type' => 'error'
                )));
        }
    }

    /**
     * Action responsavel por receber parametros de uma ocorrencia
     */
    public function addResponsavelOcorrenciaAction()
    {
        $json = $this->getRequest()->getRawBody();
        $data = Zend_Json_Decoder::decode($json);
        $entity = $this->_negocio->addResponsavelOcorrencia($data);

        if ($entity) {
            $this->_helper->json($this->_negocio->toArrayEntity($entity));
        }
    }

    /**
     * Action responsavel por receber parametros e retornar uma lista de atendentes
     */
    public function findAtendenteEncaminharAction()
    {
        $arrayData = $this->_negocio->findAtendenteCoEncaminhar($this->_getParam('params'));
        $this->_helper->json($arrayData);
    }

    /**
     * Action responsavel por retornar lista de ocorrencias pendentes de um atendente
     */
    public function findOcorrenciasPendentesAction()
    {
        $this->_negocio = new \G2\Negocio\Ocorrencia();
        $data = array();

        $result = $this->_negocio->findVwOcorrenciaAguardando(
            array(
                'id_entidade' => $this->_negocio->sessao->id_entidade,
                'id_usuarioresponsavel' => $this->_negocio->sessao->id_usuario,
            ));

        foreach ($result as $entity) {
            array_push($data, $this->_negocio->toArrayEntity($entity));
        }

        $this->_helper->json($data);
    }

    /**
     * Action responsavel por gravar ciencia nas ocorrencias pendentes
     */
    public function confirmarCienciaOcorrenciaPendenteAction()
    {
        $this->_negocio = new \G2\Negocio\Ocorrencia();
        $result = array();

        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();
            $result = $this->_negocio->confirmarCienciaOcorrenciaPendente($post['data']);
        }

        return $this->_helper->json($result);
    }

    public function findVwOcorrenciaAction()
    {
        $this->_negocio = new \G2\Negocio\Ocorrencia();
        $data = array();
        $where = array();

        $where['id_usuariointeressado'] = $this->getRequest()->getParam('id_usuariointeressado');
        $where['id_nucleoco'] = $this->getRequest()->getParam('id_nucleoco');
        $where['id_entidade'] = $this->getParam('id_entidade') ?: $this->_negocio->sessao->id_entidade;

        $result = $this->_negocio->findByVwOcorrenciasPessoa($where);
        foreach ($result as $entity) {
            $array = $this->_negocio->toArrayEntity($entity);
            $array['id'] = $entity->getId_ocorrencia();
            array_push($data, $array);
        }

        $this->_helper->json($data);
    }

    /**
     * Metodo responsavel por receber requisicao para criacao de uma copia
     * de uma ocorrencia.
     *
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     */
    public function gerarCopiaOcorrenciaAction()
    {
        $json = $this->getRequest()->getRawBody();
        $data = Zend_Json_Decoder::decode($json);
        $form = new OcorrenciaForm();

        if ($form->isValid($data)) {
            $negocio = new \G2\Negocio\Ocorrencia();
            $entity = $negocio->gerarCopiaOcorrencia($data);
            $toArray = $negocio->toArrayEntity($entity);
            $this->_helper->json($toArray);
        } else {
            var_dump($form->getErrors());
        }
    }

    /**
     * Método que separa assuntos de uma ocorrência
     * Cria uma ocorrência com id_ocorrenciaoriginal da ocorrencia original que será distribuída para um novo atendente
     */
    public function salvarDuplicidadeAction()
    {
        $json = $this->getRequest()->getRawBody();
        $data = Zend_Json_Decoder::decode($json);
        $negocio = new \G2\Negocio\Ocorrencia();
        $return = $negocio->gerarDuplicidadeOcorrencia($data);
        $this->_helper->json($return);
    }

    /**
     * Busca Ocorrênicas relacionadas (origem) de outra ocorrencia
     */
    public function findOcorrenciasRelacionadasAction()
    {
        $this->_negocio = new \G2\Negocio\Ocorrencia();
        $data = array();
        $where = array();

        $id_ocorrencia = $this->getRequest()->getParam('id_ocorrencia');

        $result = $this->_negocio->findOcorrenciasRelacionadas($id_ocorrencia);
        if ($result):
            foreach ($result as $entity) {
                $array = $this->_negocio->toArrayEntity($entity);
                $array['id'] = $entity->getId_ocorrencia();
                array_push($data, $array);
            }
        endif;

        $this->_helper->json($data);
    }

    public function getServicosAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $negocio = new \G2\Negocio\Ocorrencia();
        $result = $negocio->getServicos($params);
        $this->_helper->json($result);
    }

    public function salvarCodigoRastreioAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $negocio = new \G2\Negocio\Ocorrencia();
        $result = $negocio->salvarCodigoRastreio($params);
        $this->_helper->json($result);
    }

    public function salvarEvolucaoServicoAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $negocio = new \G2\Negocio\Ocorrencia();
        $result = $negocio->salvarEvolucaoServico($params);
        $this->_helper->json($result);
    }

    public function retornaOcorrenciaAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $negocio = new \G2\Negocio\Ocorrencia();
        $result = $negocio->retornaOcorrencia($params, true);
        $this->_helper->json($result);
    }

    /**
     * Metodo responsável por alterar a situação de uma ocorrência
     *
     * @author Rafael Leite <rafael.leite@unyleya.com.br>
     */
    public function alterarSituacaoOcorrenciaAction()
    {
        try {
            $params = $this->limpaGetAllParams($this->getAllParams());
            $negocio = new \G2\Negocio\Ocorrencia();
            $result = $negocio->alterarSituacaoOcorrecia($params);
            $this->_helper->json($result);
        } catch (Exception $e) {
            $this->_helper->viewRenderer->setNoRender();
            $this->getResponse()->setHttpResponseCode(400)
                ->appendBody($e->getMessage());
        }
    }

    public function getFuncoesByEntidadeAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        try {
            $id_entidade = $this->getParam('id_entidade');

            $results = $this->_negocio->findFuncaoCo(array('id_entidade' => $id_entidade));

            $this->_helper->json($results ?: array());
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(500)->appendBody($e->getMessage());
        }
    }

    public function atualizarOcorrenciaVisivelAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->_negocio->atualizarOcorrenciaVisivel($params);
        $this->_helper->json($result);
    }

    public function gerarXlsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $params = $this->limpaGetAllParams($this->getAllParams());

        $grid = $this->_negocio->findByOcorrenciaPessoa($params, 0, 0, true);

        header('Content-Transfer-Encoding: binary');
        header("Content-Type: application/octet-stream");
        header("Content-Transfer-Encoding: binary");
        header('Expires: '.gmdate('D, d M Y H:i:s').' GMT');
        header('Content-Disposition: attachment; filename="ocorrencias_' . date('Y_m_d_h_i_s')  . '.xls"');
        header('Pragma: no-cache');

        $this->view->ocorrencias = $grid;
        echo $this->view->render('/ocorrencia/xls.phtml');
    }
}

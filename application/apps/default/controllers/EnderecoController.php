<?php

/**
 * Controller do Endereco
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 * @since 2014-08-20
 * @package application
 * @subpackage controller
 */
use G2\Negocio\Endereco;

class Default_EnderecoController extends Ead1_ControllerRestrita
{

    private $_negocio;

    public function init()
    {
        parent::init();
        $this->_negocio = new Endereco();
    }

    public function getEnderecosAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        try {
            $id_entidade = $this->getParam('id');
            $one = $this->getParam('unico');

            if ($id_entidade) {
                $result = $this->_negocio->retornaEnderecos($id_entidade, $one);
                $this->getResponse()->appendBody(json_encode($result));
            }
            return false;
        } catch (Exception $e) {
            throw $e;
        }
    }
}
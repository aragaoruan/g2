<?php

/**
 * Class ControllerMVC Ocorrencias do aluno
 *
 * @author Paulo Silva <paulo.silva@unyleya.com.br>
 */
class Default_MinhasOcorrenciasController extends Ead1_ControllerRestrita {

    private $_negocio;

    public function init()
    {
        parent::init();
        $this->_negocio = new G2\Negocio\Ocorrencia();
    }

    /**
     * Action inicial para carrgamento de conteudo PHP da tela inicial.
     */
    public function indexAction()
    {
    }

    /**
     * Carrega o script para funcionalide de ocorrencia
     * @return javascript
     */
    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('minhas-ocorrencias/index.js');
    }

    public function listaPessoasAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $negocio = new \G2\Negocio\Ocorrencia();

        $data = $this->getRequest()->getQuery();

        if (isset($data['str']))
            $data = $data['str'];
        else
            $data = false;

        $results = $negocio->getPerfilNaoAlunos($data);

        $arrReturn = array();
        foreach ($results as $result) {
            $arrReturn[] = array(
                'id_usuario' => $result->getId_usuario(),
                'st_nomecompleto' => $result->getSt_nomecompleto()
            );
        }

        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    public function listaAcompanhadoresAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $negocio = new \G2\Negocio\Negocio();
        $id = (int) $this->getParam('id');

        $results = $negocio->findBy('\G2\Entity\OcorrenciaAcompanhante', array(
            'id_ocorrencia' => $id
        ));

        $arrReturn = array();
        foreach ($results as $result) {
            $perfil = $negocio->findOneBy('\G2\Entity\VwUsuarioPerfilEntidade', array(
                'id_usuario' => $result->getId_usuario()
            ));

            $arrReturn[] = array(
                'id_usuario' => $perfil->getId_usuario(),
                'st_nomecompleto' => $perfil->getSt_nomecompleto()
            );
        }

        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    public function atualizaAcompanhadoresAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $data = $this->getRequest()->getQuery();

        if (isset($data['id_ocorrencia'])) {
            $negocio = new \G2\Negocio\Ocorrencia();
            $negocio->salvaAcompanhadores($data['id_ocorrencia'], (array) @$data['acompanhadores']);
        }
    }

}

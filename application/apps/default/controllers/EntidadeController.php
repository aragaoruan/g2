<?php

/**
 * Controller do Entidade
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 * @since 2014-08-20
 * @package application
 * @subpackage controller
 */
use G2\Negocio\ConfiguracaoEntidade;
use G2\Negocio\Entidade;
use G2\Negocio\EntidadeIntegracao;

class Default_EntidadeController extends Ead1_ControllerRestrita
{

    /**
     * @var \G2\Negocio\ConfiguracaoEntidade
     */
    private $_negocioConfiguracaoEntidade;

    /**
     * @var \G2\Negocio\Entidade
     */
    private $_negocioEntidade;

    /**
     * @var \G2\Negocio\EntidadeIntegracao
     */
    private $_negocioEntidadeIntegracao;

    public function init()
    {
        parent::init();
        $this->_negocioEntidade = new \G2\Negocio\Entidade();
        $this->_negocioConfiguracaoEntidade = new \G2\Negocio\ConfiguracaoEntidade();
        $this->_negocioEntidadeIntegracao = new \G2\Negocio\EntidadeIntegracao();
    }

    public function getEntidadeAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        try {
            $id_entidade = $this->getParam('id');
            $bl_relacao = $this->getParam('bl_relacao');

            if ($id_entidade) {
                $result = $this->_negocioEntidade->findEntidade($id_entidade, true);

                if (isset($bl_relacao) && $bl_relacao == 1) {
                    $bl_relacao = $this->_negocioEntidade->findEntidadeRelacao($id_entidade);

                    if (sizeof($bl_relacao) > 0)
                        $result['bl_relacao'] = 1;
                }
                $this->getResponse()->appendBody(json_encode($result));
            }
            return false;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getConfiguracaoEntidadeAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        try {
            $id_entidade = $this->getParam('id');
            if ($id_entidade) {
                $result = $this->_negocioConfiguracaoEntidade->findConfiguracaoEntidade($id_entidade);
                $this->getResponse()->appendBody(json_encode($result));
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getEntidadeIntegracaoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        try {
            $id_entidade = $this->getParam('id_entidade');
            $id_sistema = $this->getParam('id_sistema');

            if ($id_entidade) {
                $result = $this->_negocioEntidadeIntegracao->retornaEntidadeIntegracao($id_entidade, $id_sistema);
                $this->getResponse()->appendBody(json_encode($result));
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Action para retornar dados do autocomplete
     */
    public function retornaAutocompleteEntidadeAction()
    {
        $str = $this->getParam('str');
        $result = $this->_negocioEntidade->retornaEntidadeAutocomplete($str);
        $this->_helper->json($result);
    }

    public function retornarEntidadeRecursivaIdAction()
    {
        $idEntidade = $this->getParam('id_entidade');
        $result = $this->_negocioEntidade->retornarVwEntidadeRecursaId($idEntidade);
        $this->_helper->json($result);
    }

}

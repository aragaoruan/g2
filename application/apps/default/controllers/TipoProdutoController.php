<?php

/**
 * Classe de controller MVC para controlar Tipos de Produto
 * @author Elcio Guimarães <elcioguimaraes@gmail.com>
 * 
 * @package application
 * @subpackage controller
 */
class Default_TipoProdutoController extends Ead1_ControllerRestrita {
    public function init() {
        parent::init();
        parent::setupDoctrine();
    }

    public function jsAction(){ 
        $this->_helper->viewRenderer->setNoRender(true);
        echo   $this->view->render('tipo-produto/index.js');
    }
    
    public function indexAction() {
        
    }
}

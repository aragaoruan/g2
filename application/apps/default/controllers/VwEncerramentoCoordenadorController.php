<?php

/**
 * Classe de controller MVC para controlar os dados da Vw_EncerramentoCoordenador
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 *
 * @package application
 * @subpackage controller
 */
class Default_VwEncerramentoCoordenadorController extends Ead1_ControllerRestrita
{
    private $_bo;

    public function init()
    {
        parent::init();
        parent::setupDoctrine();
        $this->_bo = new SecretariaBO();
    }

    public function indexAction()
    {

    }

    public function retornarEncerramentoAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $vw = new VwEncerramentoCoordenadorTO();

        $tipo_liberacao = $this->getParam('tipo_liberacao');
        $id_categoriasala = $this->getParam('id_categoriasala');
        $id_tipodisciplina = $this->getParam('id_tipodisciplina');
        $id_professor = $this->getParam('id_professor');
        $bl_recusa = $this->getParam('bl_recusado', false);

        //validar o tipo de disciplina
        if($id_tipodisciplina != TipoDisciplinaTO::TCC){
            $bl_recusa = null;
        }

        if (isset($id_professor) && $id_professor != null) {
            $vw->setId_usuarioprofessor($id_professor);
        }
        $vw->setId_tipodisciplina($id_tipodisciplina);

        switch ($tipo_liberacao) {
            case 'financeiro':
                $vw->setId_categoriasala($id_categoriasala);
                $return = $this->_bo->retornarVwEncerramentoCoordenador_Financeiro($vw, $bl_recusa);
                break;
            case 'coordenador':
                $return = $this->_bo->retornarVwEncerramentoCoordenador_Coordenador($vw, $bl_recusa);
                break;
            case 'pedagogico':
                $vw->setId_categoriasala($id_categoriasala);
                $return = $this->_bo->retornarVwEncerramentoCoordenador_Pedagogico($vw, $bl_recusa);
                break;
            default:
                $return = '';
                break;
        }

        if ($return->getTipo() == \Ead1_IMensageiro::SUCESSO) {
            $arReturn = $return->getMensagem();
        } else {
            $arReturn = array();
        }
        
        $this->getResponse()->appendBody(Zend_Json::encode($arReturn));
    }

    public function retornarFinanceiroAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $bo = new SecretariaBO();
        $negocio = new G2\Negocio\Secretaria();

        $dql = 'SELECT vw FROM G2\Entity\VwEncerramentoCoordenador vw WHERE vw.dt_encerramentopedagogico IS NOT NULL AND vw.dt_encerramentofinanceiro IS NULL AND vw.id_entidade = '
            . $this->getParam('id_entidade', Ead1_Sessao::getSessaoGeral()->id_entidade);

        $vws = $this->em->createQuery($dql)->execute();

        foreach ($vws as $key => $vw) {

            if ($vw->getDt_encerramentocoordenador()) {
                $dt_encerramentoCoordenador = new DateTime($vw->getDt_encerramentocoordenador());
                $vw->setDt_encerramentocoordenador($dt_encerramentoCoordenador->format('d/m/Y'));
            } else {
                $vw->setDt_encerramentocoordenador('');
            }

            if ($vw->getDt_encerramentofinanceiro()) {
                $dt_encerramentoFinanceiro = new DateTime($vw->getDt_encerramentofinanceiro());
                $vw->setDt_encerramentofinanceiro($dt_encerramentoFinanceiro->format('d/m/Y'));
            } else {
                $vw->setDt_encerramentofinanceiro('');
            }

            if ($vw->getDt_encerramentopedagogico()) {
                $dt_encerramentoPedagogico = new DateTime($vw->getDt_encerramentopedagogico());
                $vw->setDt_encerramentopedagogico($dt_encerramentoPedagogico->format('d/m/Y'));
            } else {
                $vw->setDt_encerramentopedagogico('');
            }

            if ($vw->getDt_encerramentoprofessor()) {
                $dt_encerramentoProfessor = new DateTime($vw->getDt_encerramentoprofessor());
                $vw->setDt_encerramentoprofessor($dt_encerramentoProfessor->format('d/m/Y'));
            } else {
                $vw->setDt_encerramentoprofessor('');
            }

            $vws[$key] = $negocio->toArrayEntity($vw);
        }

        echo json_encode($vws);
    }

}

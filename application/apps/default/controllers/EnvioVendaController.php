<?php

/**
 * Classe de controller MVC para controlar os envios das vendas para o fluxus
 * @author Rafael Rocha <rafael.rocha.mg@gmail.com>
 * 
 * @package application
 * @subpackage controller
 */
class Default_EnvioVendaController extends Ead1_ControllerRestrita {

    private $negocio;

    public function init() {
        parent::init();
        parent::setupDoctrine();
        $this->negocio = new \G2\Negocio\RegraPagamento();
        $this->_helper->getHelper('viewRenderer')->setNoRender();
    }

    public function jsAction() {
        echo $this->view->render('envio-venda/index.js');
    }

    public function indexAction() {
        echo $this->view->render('envio-venda/index.phtml');
    }

    public function enviarAction() {
        $this->getResponse()->setHeader('Content-Type', 'application/json');
        $id_venda = $this->_getParam('id_venda', null);

        $vendaNegocio = new G2\Negocio\Venda();
        //Procura pela venda enviada no parâmetro
        $venda = $vendaNegocio->find('G2\Entity\Venda', $id_venda);

        //se a venda existir faz o procedimento
        if ($venda && $venda->getIdVenda()) {
            $negocio = new \G2\Negocio\TotvsVwIntegraGestorFluxus();
            $mensageiro = $negocio->integrar((!empty($id_venda) ? array('campoalfaop1' => $id_venda . 'G2U') : array()));
            if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $mensageiro->type = 'success';
                $mensageiro->text = 'Venda ' . $id_venda . ' enviada com sucesso.';
                $mensageiro->title = 'Sucesso';
            }
            $this->getResponse()->appendBody(json_encode($mensageiro->toArray()));
            $this->getResponse()->setHttpResponseCode(200);

            // se a venda não existir apenas devolve um erro 404
        } else {
            $this->getResponse()->appendBody(json_encode(array(
                'type' => 'error',
                'text' => 'Venda ' . $id_venda . ' não localizada.',
                'title' => 'Erro!'
            )));
            $this->getResponse()->setHttpResponseCode(404);
        }
    }

}

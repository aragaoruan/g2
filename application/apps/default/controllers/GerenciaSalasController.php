<?php

use G2\Negocio\GerenciaSalas;

/*
 * @autor: Débora Castro <debora.castro@unyleya.com.br>
 * Gerencia Salas (controlador default)
 * 15/10/2013
 */

class Default_GerenciaSalasController extends Ead1_ControllerRestrita
{
    /** @var  \G2\Negocio\GerenciaSalas */
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new GerenciaSalas();
    }

    public function indexAction()
    {
        $this->_helper->viewRenderer->setNoRender(false);
        $id = $this->getParam('id_saladeaula');
        $form = new GerenciaSalasForm(array('id_saladeaula' => $id));
        $this->view->form = $form;
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('gerencia-salas/index.js');
    }

    public function getAction()
    {
        $this->getResponse()
            ->appendBody("From getAction() returning the requested article");
    }

    public function postAction()
    {
        $this->getResponse()
            ->appendBody("From postAction() creating the requested article");
    }

    public function putAction()
    {
        $this->getResponse()
            ->appendBody("From putAction() updating the requested article");
    }

    public function getAreaConhecimentoDisciplinaAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $idEntidade = $this->getParam('id_entidade');
        $result = $this->negocio->findByAreaDisciplina($idEntidade);
        $arrReturn = array();
        if ($result) {
            foreach ($result as $row) {
                $arrReturn[] = array(
                    'id' => $row->getId(),
                    'text' => $row->getSt_areaconhecimento(),
                );
            }
        }
        array_unshift($arrReturn, '');
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    public function retornoPesquisaDisciplinaAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $resultado = $this->negocio->findByPesquisaDisciplinaModal($this->getAllParams());
        $arrReturn = array();
        if ($resultado) {
            foreach ($resultado as $row) {
                $arrReturn[] = array(
                    'id_disciplina' => $row->getId_disciplina(),
                    'st_disciplina' => $row->getSt_disciplina(),
                    'st_areaconhecimento' => $row->getSt_areaconhecimento(),
                    'st_nivelensino' => $row->getSt_nivelensino(),
                    'st_serie' => $row->getSt_serie(),
                );
            }
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    public function sincronizarSalasDeAulaAction()
    {

        $this->_helper->viewRenderer->setNoRender();
        try {
            $parametros = $this->getAllParams();

            $result = $this->negocio->sincronizarSalas($parametros);
            $this->getResponse()->appendBody(($result->toJson()));
        } catch (Exception $e) {
            $this->getResponse()->setHttpResponseCode(400)
                ->setBody($e->getMessage());
        }
    }

    public function cadastrarSalaAction()
    {

    }

    public function retornaTurmaProjetoEntidadeAction()
    {

        $parametros = $this->limpaGetAllParams($this->getAllParams());
        $pp = new \G2\Negocio\ProjetoPedagogico();
        $objetos = $this->negocio->findBy('\G2\Entity\VwTurmaProjetoPedagogico', $parametros);
        $retorno = array();
        if ($objetos) {
            foreach ($objetos as $objeto) {
                $elemento = $this->negocio->toArrayEntity($objeto);
                $elemento['turmas'] = array();
                array_push($retorno, $elemento);
            }
        }
        $this->_helper->json($retorno);
    }
}
<?php

/**
 * Classe de controller MVC para controlar Enc. Financeiro
 * @author Rafael Rocha <rafael.rocha.mg@gmail.com>
 * 
 * @package application
 * @subpackage controller
 */
class Default_EncerramentoCoordenadorController extends Ead1_ControllerRestrita {

    private $_bo;
    private $_secretariaBO;

    public function init() {
        parent::init();
        parent::setupDoctrine();
        $this->_bo = new MatriculaBO();
        $this->_secretariaBO = new SecretariaBO();
    }

    public function jsAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('encerramento-coordenador/index.js');
    }

    public function indexAction() {
        
    }
}

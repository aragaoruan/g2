<?php

use G2\Negocio\GerenciaProva;

/**
 * Controller para criar agendamento de provas finais e de avaliação
 * @author Débora Castro <debora.castro@unyleya.com.br>
 * @since  19-12-2013
 * @package application
 * @subpackage controller
 */
class Default_GerenciaProvaController extends Ead1_ControllerRestrita
{

    /**
     * @var \G2\Negocio\GerenciaProva
     */
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\GerenciaProva();
    }

    public function indexAction()
    {
        $this->view->id_usuario = $this->negocio->sessao->id_usuario;
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
    }

    public function retornaProvasAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $result = $this->negocio->findProvasCadastradas();
        if ($result) {
            foreach ($result as $key => $row) {
                $result[$key]['id_avaliacaorecupera'] = $row['id_avaliacaorecupera'] != null ? $result[$key]['id_avaliacaorecupera'] : 0;
            }
        }
        $arrReturn = array();
        /*if ($result) {
            foreach ($result as $row) {
                Zend_Debug::dump($row);die;
                $arrReturn[] = array(
                    'id_avaliacao' => $row->getId_avaliacao(),
                    'st_avaliacao' => $row->getSt_avaliacao(),
                    'id_avaliacaorecupera' => $row->getId_avaliacaorecupera() != null ? $row->getId_avaliacaorecupera() : 0,
                    'st_avaliacaorecupera' => $row->getSt_avaliacaorecupera(),
                    'id_tipoavaliacao' => $row->getId_tipoavaliacao()
                );
            }
        }*/
        $this->getResponse()->appendBody(json_encode($result));
    }

    public function pesquisaAlunosAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->getRequest()->getParams();

        $result = $this->negocio->pesquisaAlunos($params);

        $this->_helper->json($result->getMensagem());
    }

    public function buscaAvaliacoesAction()
    {
        $params = array(
            'sg_uf' => $this->getParam('sg_uf'),
            'id_municipio' => $this->getParam('id_municipio'),
            'id_avaliacao' => $this->getParam('id_avaliacao')
        );

        if ($params['id_avaliacao']) {
            $result = $this->negocio->findVwAplicacoesPorProva($params);
        } else {
            unset($params['id_avaliacao']);
            $result = $this->negocio->findVwAplicacoes($params);
        }

        $arrReturn = array();

        if ($result) {
            foreach ($result as $row) {
                $arrReturn[] = array(
                    'id_avaliacaoaplicacao' => $row->getId_avaliacaoaplicacao(),
                    'st_aplicadorprova' => $row->getSt_aplicadorprova(),
                    'st_endereco' => $row->getSt_endereco(),
                    'st_cidade' => $row->getSt_cidade(),
                    'sg_uf' => $row->getSg_uf(),
                    'dt_aplicacao' => $this->negocio->converterData($row->getDt_aplicacao(), 'd/m/Y'),
                    'hr_inicioprova' => $this->negocio->converterData($row->getHr_inicioprova(), 'H:i'),
                    'hr_fimprova' => $this->negocio->converterData($row->getHr_fimprova(), 'H:i'),
                    'nu_vagasrestantes' => $row->getNu_vagasrestantes()
                );
            }
        }

        $this->_helper->json($arrReturn);
    }

    public function buscaDisciplinasAction()
    {
        $negocio = new \G2\Negocio\GerenciaProva();
        $this->_helper->viewRenderer->setNoRender(true);

        $params['id_matricula'] = $this->getParam('id_matricula');
        $params['id_avaliacao'] = $this->getParam('id_avaliacao');

        $result = $negocio->findByVwDisciplinasAgendamento($params);
//        Zend_Debug::dump($result);die;
        $arrReturn = array();
        if ($result) {
            foreach ($result as $row) {
                $arrReturn[] = array(
                    'id_disciplina'                  => $row->getId_disciplina(),
                    'st_disciplina'                  => $row->getSt_disciplina(),
                    'id_avaliacaoconjuntoreferencia' => $row->getId_avaliacaoconjuntoreferencia(),
                    'id_avaliacao'                   => $row->getId_avaliacao(),
                    'bl_status'                      => $row->getBl_status(),
                    'st_status'                      => $row->getSt_status()
                );
            }
        }

        $this->_helper->json(array_reverse($arrReturn));
//        $this->getResponse()->appendBody(json_encode(array_reverse($arrReturn)));
    }

    public function listaDisciplinasAction()
    {
        $negocio = new \G2\Negocio\GerenciaProva();
        $this->_helper->viewRenderer->setNoRender(true);

        $params['id_avaliacaoagendamento'] = $this->getParam('id_avaliacaoagendamento');
        $params['bl_agendado'] = 1;

        $result = $negocio->findByVwDisciplinasAgendamento($params);
        $arrReturn = array();
        if ($result) {
            foreach ($result as $row) {
                $arrReturn[] = array(
                    'st_disciplina'  => $row->getSt_disciplina(),
                    'bl_provaglobal' => $row->getBl_provaglobal()
                );
            }
        }
        $this->getResponse()->appendBody(json_encode(array_reverse($arrReturn)));
    }

    /**
     * Retorna apenas os municípios que possuem um polo para aplicação de prova disponível
     */
    public function retornaMunicipioAplicacaoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $params = null;
        if ($this->getParam('sg_uf') != '') {
            $params['sg_uf'] = $this->getParam('sg_uf');
        }

        $result = $this->negocio->findByVwMunicipioAplicacao($params);
        $arrReturn = array();
        if (is_array($result)) {
            foreach ($result as $value) {
                $arrReturn[] = array(
                    'id_municipio'     => $value->getId_municipio(),
                    'sg_uf'            => $value->getSg_uf(),
                    'st_nomemunicipio' => $value->getSt_nomemunicipio()
                );
            }
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    public function listaPresencaAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $result = array();
        $params = $this->getRequest()->getParams();

        unset($params['module']);
        unset($params['action']);
        unset($params['controller']);

        $result = $this->negocio->findOneByPresencaAgendamento($params);

        $this->getResponse()->appendBody(json_encode($result));

    }

    public function getDisciplinasAptoAction()
    {
        $arr_return = array();

        $id_matricula = $this->getParam('id_matricula');

        if ($id_matricula) {
            $arr_return = $this->negocio->findDisciplinasAptoAgendamento($id_matricula);
        }

        $this->_helper->json($arr_return);
    }

    # @deprecated
    public function getAgendamentosChamadasAction() {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $results = $this->negocio->findAvaliacoesDisciplinasAgendamento($params);
        $this->_helper->json($results);
    }

    public function getProvaAgendadasAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $results = $this->negocio->provaAgendada($params);
        $this->_helper->json($results);
    }
}
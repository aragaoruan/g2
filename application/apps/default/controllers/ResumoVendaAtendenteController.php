<?php

class Default_ResumoVendaAtendenteController extends Ead1_ControllerRestrita
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {

    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
    }

    public function retornaRelatorioAction()
    {
        $this->negocio = new G2\Negocio\RelResumoVenda();

        $this->_helper->json($this->negocio->executaRelatorioIndividualAtendente($this->limpaGetAllParams($this->getAllParams())));
    }

    public function gerarXlsAction()
    {
        $this->negocio = new G2\Negocio\RelResumoVenda();
        $this->view->dados = $this->negocio->executaRelatorioIndividualAtendente($this->limpaGetAllParams($this->getAllParams()));
        $this->view->dadosResumo = $this->negocio->executaRelatorioIndividualResumoAtendente($this->limpaGetAllParams($this->getAllParams()));

    }

    public function retornaAtendentesAction(){

        $atendenteNegocio = new \G2\Negocio\VwAtendente();
        $atendentes = $atendenteNegocio->entitySerialize($atendenteNegocio->findBy('G2\Entity\VwAtendente', array('id_entidade' => $atendenteNegocio->sessao->id_entidade)));

        $this->_helper->json($atendentes);
    }

    public function retornaRelatorioResumoAction(){
        $this->negocio = new G2\Negocio\RelResumoVenda();
        $this->_helper->json($this->negocio->executaRelatorioIndividualResumoAtendente($this->limpaGetAllParams($this->getAllParams())));
    }
}
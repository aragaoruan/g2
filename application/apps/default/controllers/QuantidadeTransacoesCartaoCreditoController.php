<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Resende
 * Date: 12/03/15
 * Time: 11:00
 */

class Default_QuantidadeTransacoesCartaoCreditoController extends Ead1_ControllerRestrita
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {

    }
//retornarVwEntidadeRecursaId
    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
    }

    /**
     * Retorna a propria entidade e sua filhas
     */
    public function retornaEntidadeRecursivaAction()
    {
        $_negocio = new G2\Negocio\Entidade();

        $this->_helper->json($_negocio->retornarVwEntidadeRecursaId());
    }

    public function retornaRelatorioAction()
    {
        $this->negocio = new G2\Negocio\QuantidadeTransacoesCartaoCredito();

        $this->_helper->json($this->negocio->executaRelatorio($this->limpaGetAllParams($this->getAllParams())));
    }

    public function gerarXlsAction()
    {
        $this->negocio = new G2\Negocio\QuantidadeTransacoesCartaoCredito();
        $this->view->dados = $this->negocio->executaRelatorio($this->limpaGetAllParams($this->getAllParams()));

    }
} 
<?php

/**
 * Controller para Funcionalidade
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2014-11-03
 * @package application
 * @subpackage controller
 */


class Default_FuncionalidadeController extends Ead1_ControllerRestrita
{
    private $negocio;

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('funcionalidade/index.js');
    }

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Negocio();
    }

    public function indexAction()
    {
    }

    public function getEntidadeFuncionalidadeAction() {
        $bo = new PesquisarBO();
        $to = new VwEntidadeClasseTO();
        $results = $bo->pesquisarEntidadeFilha($to);

        $this->_helper->json($results);
    }

    public function getArvoreVisualizacaoAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        $idEntidade = $this->getParam('id_entidade');

        $bo = new PerfilBO();
        $to = new VwEntidadeFuncionalidadeTO();

        $to->setId_entidade($idEntidade);
        $results = $bo->retornaArvoreEntidadeFuncionalidade($to);

        if (isset($results->mensagem)) {
            $html = $this->_htmlArvoteItems($results->mensagem);
            echo $html;
        }
    }

    public function getArvoreAtivacaoAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        $idEntidade = $this->getParam('id_entidade');

        $bo = new PerfilBO();
        $to = new VwEntidadeFuncionalidadeTO();

        $to->setId_entidade($idEntidade);
        $results = $bo->retornaArvoreEntidadeFuncionalidadeSuperUsuario($to);

        if (isset($results->mensagem)) {
            $html = $this->_htmlArvoteItems($results->mensagem);
            echo $html;
        }
    }

    public function _htmlArvoteItems($items) {
        $html = '';

        foreach ($items as $item) {
            $attributes = array(
                "data-id='{$item['to']->id_funcionalidade}'",
                "data-id-situacao='{$item['to']->id_situacao}'",
                "data-bl-ativo='{$item['to']->bl_ativo}'",
            );

            $html .= "<li " . implode(' ', $attributes) . " >";
            $html .= "<a>{$item['label']}</a>";
            $html .= '<i class="icon icon-check select-all"></i>';
            if ($item['children']) {
                $html .= '<ul>';
                $html .= $this->_htmlArvoteItems($item['children']);
                $html .= '</ul>';
            }
            $html .= '</li>';
        }

        return $html;
    }


    public function getArvoreVisualizacaoSelecionadosAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        $idEntidade = $this->getParam('id_entidade');

        $bo = new EntidadeBO();
        $to = new EntidadeFuncionalidadeTO();

        $to->setId_entidade($idEntidade);
        $to->setBl_ativo(true);
        $to->setBl_visivel(true);
        $results = $bo->retornarEntidadeFuncionalidade($to);

        $json = array();
        if ($results->mensagem) {
            foreach($results->mensagem as $result) {
                $json[] = $result->id_funcionalidade;
            }

            $this->_helper->json($json);
        }
    }

    public function getArvoreAtivacaoSelecionadosAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        $idEntidade = $this->getParam('id_entidade');

        $bo = new EntidadeBO();
        $to = new EntidadeFuncionalidadeTO();

        $to->setId_entidade($idEntidade);
        $to->setBl_ativo(true);
        $results = $bo->retornarTodasEntidadeFuncionalidades($to);

        $json = array();
        if ($results->mensagem) {
            foreach($results->mensagem as $result) {
                if (isset($result->id_funcionalidade)) {
                    $json[] = $result->id_funcionalidade;
                }
            }

            $this->_helper->json($json);
        }
    }


}

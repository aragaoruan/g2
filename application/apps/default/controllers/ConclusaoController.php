<?php

/**
 * Created by PhpStorm.
 * User: UNYLEYA
 * Date: 04/11/14
 * Time: 10:25
 */
class Default_ConclusaoController extends Ead1_ControllerRestrita
{

    /**
     * @var \G2\Negocio\Conclusao $negocio
     */
    public $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Conclusao();
    }

    public function indexAction()
    {
        $id = $this->_getParam('id');

    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('conclusao/index.js');
    }

    public function retornaEntidadesFilhaAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $result = $this->negocio->findAllVwEntidadeClasse();

        $this->_helper->json($result);
    }

    public function retornaProjetoEntidadeAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $where['id_entidade'] = $this->getParam('id_entidade');
        $data = array();
        $result = $this->negocio->findByVwProjetoEntidade($where);
        foreach ($result as $row) {
            $data[] = $this->negocio->toArrayEntity($row);
        }
        $this->_helper->json($data);
    }

    public function retornaAlunosConcluintesAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $results = $this->negocio->pesquisaAlunosConcluintes($this->getAllParams());

        // Gerar XLS dos resultados
        if ($this->getParam('xls')) {
            // Preparar campos que não estão no formato de saída correto
            $results = array_map(function ($item) {
                $item['dt_concluinte'] = substr($item['dt_concluinte'], 0, 10);
                $item['st_documentacao'] = $item['bl_documentacao'] === null ? 'Não Analisado' : ($item['bl_documentacao'] ? 'Sim' : 'Não');
                $item['st_academico'] = $item['bl_academico'] === null ? 'Não Analisado' : ($item['bl_academico'] ? 'Sim' : 'Não');
                $item['st_dtenviocodigorastreamento'] = $item['st_dtenvioaluno'];

                // Adicionar Código de Rastreamento na string de "Envio ao Aluno"
                if ($item['st_codigoacompanhamento']) {
                    $item['st_dtenviocodigorastreamento'] .= ' - ' . $item['st_codigoacompanhamento'];
                }

                return $item;
            }, $results);

            $cabecalho = array(
                'id_matricula'                 => 'Matrícula',
                'st_nomecompleto'              => "Nome",
                'st_evolucao'                  => 'Evolução',
                'st_projetopedagogico'         => "Projeto Pedagógico",
                'st_cpf'                       => "CPF",
                'dt_concluinte'                => 'Data Concluinte',
                'st_documentacao'              => 'Documentação',
                'st_academico'                 => 'Acadêmico',
                'st_dtcadastrocertificacao'    => 'Certificado Gerado',
                'st_dtenviocertificado'        => 'Envio Assinatura',
                'st_dtretornocertificadora'    => 'Retorno Assinatura',
                'st_dtenviocodigorastreamento' => 'Envio ao Aluno'
            );

            $ng_relatorio = new \G2\Negocio\Relatorio();
            $ng_relatorio->export($results, $cabecalho, 'xls', 'Conclusão');
        }

        if ($this->getParam('xls_etiquetas')) {
            return $this->negocio->gerarXlsEtiquetas($results);
        } else {
            $this->_helper->json($results);
        }
    }

    public function retornaStatusDocumentacaoAlunoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $result = $this->negocio->pesquisaStatusDocumentacaoAluno($this->getAllParams());
        $this->_helper->json($result);
    }

    public function retornaTextosCertificacaoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $negocioTexto = new \G2\Negocio\TextoSistema();
        $result = $negocioTexto->findVwTextoSistema(array('id_textocategoria' => 4));
        $this->_helper->json($result->getMensagem());
    }

    public function retornaDocumentacaoAlunoSecretariaAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $result = $this->negocio->pesquisaDocumentacaoAlunoSecretaria($this->getAllParams());


        $dados = array();

        If ($result) {
            sort($result);
            foreach ($result as $index => $row) {
                $dados['st_documentos'] = $row->getSt_documentos();
                $dados['st_situacao'] = $row->getSt_situacao();
            }
        }
        $this->_helper->json($result);
    }


    public function retornaDocumentacaoAlunoFinanceiroAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $result = $this->negocio->pesquisaDocumentacaoAlunoFinanceiro($this->getAllParams());

        $dados = array();
        If ($result) {
            sort($result);
            foreach ($result as $row) {
                if (is_array($row)) {
                    foreach ($row as $value) {
                        if ($value['label'] == 'Em Atraso') {
                            foreach ($value['dados'] as $v) {
                                $x = array();
                                $data_vencimento = $v->getDt_vencimento() != null
                                    ? ($v->getDt_vencimento() instanceof Zend_Date ? $v->getDt_vencimento()
                                        : new Zend_Date($v->getDt_vencimento())) : null;
                                $x['nu_valor'] = money_format('%.2n', $v->getNu_valor());
                                $x['dt_vencimento'] = $data_vencimento instanceof Zend_Date
                                    ? $data_vencimento->toString("dd/MM/Y") : ' -- ';
                                $x['st_meiopagamento'] = $v->getSt_meiopagamento();
                                array_push($dados, $x);
                            }
                        }
                    }
                }

            }
        }
        $this->_helper->json($dados);
    }

    public function retornaDocumentacaoAlunoPedagogicoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $result = $this->negocio->pesquisaDocumentacaoAlunoPedagogico($this->getAllParams());

        $dados = array();

        If ($result) {
            sort($result);
            foreach ($result as $row) {

                $dados['st_disciplina'] = $row->getSt_disciplina();
                $dados['st_evolucao'] = $row->getSt_evolucao();
            }
        }
        $this->_helper->json($result);
    }

    public function certificarAlunoAction()
    {

        $result = $this->negocio->certificarAluno($this->getAllParams());
        $this->_helper->json($result);
    }

    public function retornaCertificadoImpressoAction() {
        $id_matricula = $this->getParam('id_matricula');
        $result = $this->negocio->retornaCertificadoImpresso($id_matricula);
        $this->_helper->json($result);
    }

    public function salvarImpressaoCertificadoAction() {
        $params = $this->getAllParams();
        $result = $this->negocio->salvarImpressaoCertificado($params);
        $this->_helper->json($result);
    }

    /**
     * Metodo responsavel por receber lista de matriculas e repassar
     * para o metodo que grava processos de certificacoes
     * @return ArrayList G2\Entity\VwMatricula
     * @author Rafael Bruno <rafael.oliveira@unyleya.com.br>
     */
    public function salvarListProcessoMatriculaCertificacaoAction()
    {
        $post = $this->getRequest()->getPost();
        $return = array();

        $result = $this->negocio->salvarListProcessoMatriculaCertificacao($post['data']);

        foreach ($result as $key => $value) {
            $return[$key] = $this->negocio->toArrayEntity($value);
            $return[$key]['id'] = $value->getId_matricula();
            $return[$key]['dt_cadastro'] = $value->getDt_cadastro()->format('d/m/Y');
            $return[$key]['dt_retornocertificadora'] = $value->getDt_retornocertificadora()->format('d/m/Y');
            $return[$key]['dt_enviocertificado'] = $value->getDt_enviocertificado()->format('d/m/Y');
            $return[$key]['id_entidade'] = $value->getId_entidade()->getId_entidade();
            $return[$key]['id_usuarioenvioaluno'] = $value->getId_usuarioenvioaluno()->getId_usuario();
            $return[$key]['id_usuarioenviocertificado'] = $value->getId_usuarioenviocertificado()->getId_usuario();
            $return[$key]['id_usuarioretornocertificadora'] = $value->getId_usuarioretornocertificadora()->getId_usuario();
        }

        $this->_helper->json($return);
    }

    public function retornaDisciplinasComplementaresAction()
    {

        $result = $this->negocio->findByVwMatricula(array(
            'id_usuario' => $this->getParam('id_usuario'),
            'bl_disciplinacomplementar' => true
        ));

        $toArray = array();
        foreach ($result as &$value) {
            $toArray[] = $this->negocio->toArrayEntity($value);
        }

        $this->_helper->json($toArray);
    }

    /**
     * Action responsavel por receber collecao(json) de matriculas para fazer vinculacao
     * de matriculas
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     * @throws Zend_Exception
     * @return json G2\Entity\VwMatricula
     */
    public function vincularDisciplinasComplementaresAction()
    {
        $this->negocio = new \G2\Negocio\Conclusao();
        $decodeRawBody = Zend_Json::decode($this->getRequest()->getRawBody());

        $return = $this->negocio->vincularListMatriculaComplementar($decodeRawBody);

        $array = array();
        foreach ($return as $key => $entity) {
            $array[] = $this->negocio->toArrayEntity($entity);
        }

        $this->_helper->json($array);
    }


    public function salvarEdicaoDataConclusaoAction()
    {
        $data = $this->getAllParams();
        $ng_matricula = new \G2\Negocio\Matricula();
        $ng_matricula->beginTransaction();
        try {
            $tra = new \G2\Negocio\Tramite();
            $tramite = $tra->salvarTramiteDataConclusao($data['id_matricula'], $data['dt_concluinte'], \TramiteMatriculaTO::MANUAL);
            if ($tramite) {
                $mensageiro = $ng_matricula->salvarMatricula($data);
            } else {
                throw new \Exception("Não foi possível gerar o tramite da matrícula!");
            }
            $ng_matricula->commit();
        } catch (\Exception $e) {
            $ng_matricula->rollback();
            $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        $this->_helper->json($mensageiro);
    }

}
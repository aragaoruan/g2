<?php
/**
 * Classe Controller para Holding
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */

class Default_HoldingController extends Ead1_ControllerRestrita{

    private $negocio;

    public function init()
    {
        $this->negocio = new \G2\Negocio\GradeHoraria();
    }

    public function indexAction(){
        $id = $this->_getParam('id');
        if($id){
            $negocio = new \G2\Negocio\Holding();
            $result = $negocio->findByHoldingId($id);
            $this->view->dados = $result->getFirstMensagem();
        }
    }

    public function salvarHoldingAction()
    {
        $dados = $this->limpaGetAllParams($this->getRequest()->getParams());
        $negocio = new \G2\Negocio\Holding();
        $result = $negocio->salvarHolding($dados);

        return $this->_helper->json($result);
    }

    public function retornarEntidadesMesmaHoldingAction()
    {
        $negocio = new G2\Negocio\Holding();
        $return = $negocio->retornarEntidadesHolding();
        foreach($return as $dados)
        {
            $retorno[] = $negocio->toArrayEntity($dados);
        }
        $this->_helper->json($retorno);
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
    }

}
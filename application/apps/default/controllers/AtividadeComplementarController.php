<?php


/**
 * Class Default_AtividadeComplementarController
 */
class Default_AtividadeComplementarController extends Ead1_ControllerRestrita
{
    /** @var  \G2\Negocio\AtividadeComplementar */
    private $negocio;

    public function init()
    {
        $this->negocio = new \G2\Negocio\AtividadeComplementar();
        parent::init();
    }

    public function indexAction()
    {

    }

    public function tipoAtividadeAction()
    {

    }

    public function retornarAtividadesAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        try {

            $params = $this->limpaGetAllParams($this->getAllParams());

            if (!isset($params["id_entidade"])
                || (isset($params["id_entidade"]) && empty($params["id_entidade"]))
            ) {
                $params["id_entidade"] = $this->negocio->getId_entidade();
            }

            $result = $this->negocio->getRepository(\G2\Entity\AtividadeComplementar::class)
                ->retornarPesquisaAtividade($params);

            if (!$result) {
                return $this->getResponse()->setHttpResponseCode(404)
                    ->appendBody("Nenhum resultado encontrado.");
            }

            return $this->getResponse()->appendBody(json_encode($result));
        } catch (Exception $e) {
            return $this->getResponse()->setHttpResponseCode(500)
                ->appendBody($e->getMessage());
        }
    }

    public function salvarAvaliacaoAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        try {

            if ($this->getRequest()->isPost()) {
                $body = $this->getRequest()->getRawBody();
                $data = (Array)json_decode($body);

                $result = $this->negocio->salvarAvaliacaoAtividade($data);

                if ($result instanceof \G2\Entity\AtividadeComplementar) {
                    return $this->getResponse()->setHttpResponseCode(201)
                        ->appendBody(json_encode($result->toBackboneArray()));
                }
            }
        } catch (Exception $e) {
            return $this->getResponse()->setHttpResponseCode(500)
                ->appendBody($e->getMessage());
        }
    }
}

<?php

/**
 * Description of SalaAulaController
 *
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 * @update Debora Castro <debora.castro@unyleya.com.br>
 * @update Neemias Santos <neemias.santos@unyleya.com.br>
 * @update Denise Xavier - 24-01-2017
 * @update Ilson Nóbrega <ilson.nobrega@unyleya.com.br>
 */
class Default_SalaDeAulaController extends Ead1_ControllerRestrita
{

    /**
     * @var \G2\Negocio\GerenciaSalas $negocio
     */
    private $negocio;
    /**
     * @var G2\Negocio\Disciplina $_negocio
     */
    private $_negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\GerenciaSalas();
        $this->_negocio = new G2\Negocio\Disciplina();
    }

    public function indexAction()
    {
        $rota = $this->getRequest()->getParams();
        $session = $this->negocio->sessao;
        $this->view->professores = $this->_negocio->retornarDadosCoordenadorDisciplina(array(
            'id_perfilpedagogico' => 1,
            'id_situacaoperfil' => 15
        ));

        $id = $this->getParam('id');
        $form = new SalaDeAulaForm(array('id_saladeaula' => $id));
        $this->view->form = $form;
        $this->view->entidade = $session->id_entidade;
    }

    public function salaDeAulaPersonalizadoAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $session = $this->negocio->sessao;
        $this->view->professores = $this->_negocio->retornarDadosCoordenadorDisciplina(array(
            'id_perfilpedagogico' => 1,
            'id_situacaoperfil' => 15
        ));

        $id = $this->getParam('id');
        $form = new SalaDeAulaForm(array('id_saladeaula' => $id));
        $this->view->form = $form;
        $this->view->entidade = $session->id_entidade;
        $this->view->personalizado = true;
        echo $this->view->render('/sala-de-aula/index.phtml');
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
    }

    public function retornaProjetoPedagogicoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $params = $this->getAllParams();
        unset($params['module'], $params['controller'], $params['action']);

        if (isset($params['st_projetopedagogico'])) {
            if (!isset($params['search']))
                $params['search'] = array();

            $params['search']['st_projetopedagogico'] = $params['st_projetopedagogico'];
            unset($params['st_projetopedagogico']);
        }

        $results = $this->negocio->findByProjetoPedagogico($params);

        $json = array();
        if ($results) {
            foreach ($results as $result) {
                $json[] = array(
                    'id' => $result->getId_projetopedagogico(),
                    'id_projetopedagogico' => $result->getId_projetopedagogico(),
                    'st_projetopedagogico' => $result->getSt_projetopedagogico()
                );
            }
        }

        $this->_helper->json($json);
    }

    public function retornaDadosSalaEditAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $resultado = $this->negocio->findBySalaDeAula(array('id_saladeaula' => $this->getParam('id_saladeaula')));
        $disciplina = $this->negocio->findOneBy('\G2\Entity\DisciplinaSalaDeAula', array('id_saladeaula' => (int)$this->getParam('id_saladeaula')));
        $stDisciplina = null;

        if ($disciplina instanceof \G2\Entity\DisciplinaSalaDeAula) {
            $stDisciplina = $this->negocio->find('\G2\Entity\Disciplina', (int)$disciplina->getId_disciplina());
        }

        $arrReturn = array();

        if (is_array($resultado) && $resultado) {
            $row = array_shift($resultado);
            $arrReturn = array(
                'id_saladeaula' => $row->getId_saladeaula(),
                'st_saladeaula' => $row->getSt_saladeaula(),
                'nu_maxalunos' => $row->getNu_maxalunos(),
                'dt_inicioinscricao' => $row->getDt_inicioinscricao() ? date_format($row->getDt_inicioinscricao(), 'd/m/Y') : '',
                'dt_fiminscricao' => $row->getDt_fiminscricao() ? date_format($row->getDt_fiminscricao(), 'd/m/Y') : '',
                'dt_abertura' => $row->getDt_abertura() ? $row->getDt_abertura()->format('d/m/Y') : '',
                'dt_encerramento' => $row->getDt_encerramento() ? date_format($row->getDt_encerramento(), 'd/m/Y') : '',
                'nu_diasaluno' => $row->getNu_diasaluno(),
                'bl_semdiasaluno' => $row->getBl_semdiasaluno(),
                'nu_diasencerramento' => $row->getNu_diasencerramento(),
                'bl_semencerramento' => $row->getBl_semencerramento(),
                'bl_todasentidades' => $row->getBl_todasentidades(),
                'id_categoriasala' => $row->getId_categoriasala()->getId_categoriasala(),
                'id_disciplina' => $stDisciplina ? $stDisciplina->getId_disciplina() : '',
                'st_disciplina' => $stDisciplina ? $stDisciplina->getSt_disciplina() : '',
                'nu_diasextensao' => $row->getNu_diasextensao(),
                'id_situacao' => $row->getId_situacao()->getId_situacao(),
                'bl_ativo' => $row->getBl_ativa(),
                'id_modalidadesaladeaula' => $row->getId_modalidadesaladeaula() ? $row->getId_modalidadesaladeaula()->getId_modalidadesaladeaula() : null,
                'dt_comextensao' => $row->getDt_encerramento() && $row->getNu_diasextensao() ? (date('d/m/Y', strtotime("+" . (int)$row->getNu_diasextensao() . " days", strtotime(date_format($row->getDt_encerramento(), 'd-m-Y'))))) : '',
                'dt_cancelamento' => $row->getDt_cancelamento() ? $row->getDt_cancelamento()->format('d/m/Y H:i:s') : null,
                'id_usuariocancelamento' => $row->getId_usuariocancelamento() ? $row->getId_usuariocancelamento()->getId_usuario() : null,
                'st_nomeusuariocancelamento' => $row->getId_usuariocancelamento() ? $row->getId_usuariocancelamento()->getSt_nomecompleto() : null,
                'bl_ofertaexcepcional' => $row->getBl_ofertaexcepcional(),
                'bl_vincularturma' => $row->getBl_vincularturma(),
                'id_entidadeintegracao' => $row->getId_entidadeintegracao() ? $row->getId_entidadeintegracao()->getId_entidadeintegracao() : null,
                'array_coordenadores' => $this->negocio->retornaCoordenadoresProjetoSala($row->getId_saladeaula())
            );
        }

        $this->_helper->json($arrReturn);
    }

    public function salvarSalaDadosBasicosAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->getAllParams();
        $result = $this->negocio->salvarSalaDeAulaDadosBasicos($params);

        $this->getResponse()->appendBody(json_encode($result));
    }

    public function salvarSalaPermissoesAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->getAllParams();

        $result = $this->negocio->salvarSalaDeAulaPermissoes($params);

        $this->getResponse()->appendBody(json_encode($result));
    }

    public function salvarProdutoValorPrrAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->getAllParams();

        $result = $this->negocio->salvarDadosProdutoPrr($params);

        $this->getResponse()->appendBody(json_encode($result));
    }

    public function salvarSalaIntegracaoAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $params = $this->getAllParams();


        $result = $this->negocio->salvarIntegracaoSalaDeAula($params);

        $this->getResponse()->appendBody(json_encode($result));
    }

    public function salvarSalaReIntegracaoAction()
    {

        $this->_helper->viewRenderer->setNoRender(true);

        $id_saladeaula = $this->_getParam('idSalaIntegrar', false);
        $id_sistema = $this->_getParam('id_sistemaintegrado', false);

        $result = $this->negocio->reintegrarSalaDeAula($id_saladeaula, $id_sistema);

        $this->getResponse()->appendBody(json_encode($result));

    }

    public function recarregaArvoreSalaAction()
    {
        $result = array();
        $this->_helper->viewRenderer->setNoRender(true);
        $retorno = $this->negocio->findBy('G2\Entity\SalaDeAulaEntidade', array('id_saladeaula' => $this->_getParam('id_saladeaula')));
        if (!empty($retorno)) {
            foreach ($retorno as $entidade) {
                $result[] = $entidade->getId_entidade();
            }
        }
        echo Zend_Json_Encoder::encode($result);
    }

    public function retornaProjetoSalaAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $parametros = $this->limpaGetAllParams($this->getAllParams());

        $salaNegocio = new \G2\Negocio\SalaDeAula();
        $retorno = $salaNegocio->retornaProjetoSala($parametros);

        $this->getResponse()->appendBody(json_encode($retorno));
    }

    public function retornaProdutoPrrAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $arrReturn = array();

        $bo = new \ProdutoBO();
        $to = new \ProdutoPrrTO();
        $to->setId_saladeaula($this->getParam('id_saladeaula'));
        $to->setId_entidade($this->negocio->sessao->id_entidade);
        $to->fetch(true, true, true);
        $idProduto = $to->getId_produto();
        if (!empty($idProduto)) {
            $toValor = new \ProdutoValorTO();
            $toValor->setId_produto($to->getId_produto());

            $retorno = $bo->retornarProdutoValor($toValor);

            if ($retorno->getTipo() == Ead1_IMensageiro::SUCESSO) {
                foreach ($retorno->getMensagem() as $row) {
                    $arrReturn[] = array(
                        'id_produto' => $row->getId_produto(),
                        'id_produtovalor' => $row->getId_produtovalor(),
                        'nu_valor' => number_format($row->getNu_valor(), 2, '.', ''),
                        'dt_inicio' => $row->getDt_inicio() ? $row->getDt_inicio()->toString('dd/MM/yyyy') : '',
                        'dt_termino' => $row->getDt_termino() ? $row->getDt_termino()->toString('dd/MM/yyyy') : ''
                    );
                }
            }
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    public function editarSalaDadosBasicosAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->getAllParams();

        $result = $this->negocio->editarSalaDeAulaDadosBasicos($params);

        $this->getResponse()->appendBody(json_encode($result));
    }

    public function editarSalaPermissoesAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->getAllParams();

        $result = $this->negocio->editarSalaDeAulaPermissoes($params);

        $this->getResponse()->appendBody(json_encode($result));
    }

    public function editarProdutoValorPrrAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->getAllParams();

        $result = $this->negocio->editarProdutoValorPrr($params);

        $this->getResponse()->appendBody(json_encode($result));
    }

    public function retornaDadosIntegracaoSalaAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $arrReturn = array();

        $to = new \SalaDeAulaIntegracaoTO();
        $to->setId_saladeaula($this->getParam('id_saladeaula'));
        $to->fetch(false, true, true);

        $idCodCurso = $to->getId_saladeaulaintegracao();
        if (!empty($idCodCurso)) {

            $arrReturn[] = array(
                'st_codsistemacurso' => $to->getSt_codsistemacurso(),
                'id_saladeaula' => $to->getId_saladeaula(),
                'id_saladeaulaintegracao' => $to->getId_saladeaulaintegracao(),
                'id_sistema' => $to->getId_sistema(),
                'id_disciplinaintegracao' => $to->getId_disciplinaintegracao(),
            );
        } else {
            $arrReturn[] = array(
                'st_codsistemacurso' => '',
                'id_saladeaula' => '',
                'id_saladeaulaintegracao' => '',
                'id_sistema' => 'Sem Integração',
                'id_disciplinaintegracao' => '',
            );
        }


        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    public function reintegrarProfessoresAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $salaBO = new \SalaDeAulaBO();

        $salaTO = new \SalaDeAulaTO();
        $salaTO->setId_saladeaula($this->getParam('id_saladeaula'));
        $salaTO->setId_entidade($this->negocio->sessao->id_entidade);

        $result = $salaBO->reintegrarProfessoresSalaDeAula($salaTO);

        $this->getResponse()->appendBody(json_encode($result));
    }

    public function reintegrarAlunosSalaAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $salaBO = new \SalaDeAulaBO();

        $salaTO = new \SalaDeAulaTO();
        $salaTO->setId_saladeaula($this->getParam('id_saladeaula'));
        $salaTO->setId_entidade($this->negocio->sessao->id_entidade);


        $result = $salaBO->reintegrarAlocacaoAlunoSalaDeAula($salaTO);

        $this->getResponse()->appendBody(json_encode($result));
    }


    public function desvincularProfessorMoodleAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $params = $this->getAllParams();

        $result = $this->negocio->desvincularProfessorSalaMoodle($params);

        $this->getResponse()->appendBody(json_encode($result));
    }

    /**
     * Action que recebe como parametro dados de uma sala de aula para
     * efetuar um cancelamento.
     *
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     */
    public function cancelarSalaDeAulaAction()
    {
        $data = \Zend_Json::decode($this->getRequest()->getRawBody());
        $salaDeAulaNegocio = new \G2\Negocio\SalaDeAula();

        $entity = $salaDeAulaNegocio->cancelarSalaDeAula($data);

        $arrReturn = array(
            'id_saladeaula' => $entity->getId_saladeaula(),
            'dt_cadastro' => $entity->getDt_cadastro(),
            'st_saladeaula' => $entity->getSt_saladeaula(),
            'bl_ativa' => $entity->getBl_ativa(),
            'id_usuariocadastro' => $entity->getId_usuariocadastro(),
            'dt_inicioinscricao' => $entity->getDt_inicioinscricao() ? $entity->getDt_inicioinscricao()->format('d-m-Y') : '',
            'dt_fiminscricao' => $entity->getDt_fiminscricao() ? $entity->getDt_fiminscricao()->format('d-m-Y') : '',
            'dt_abertura' => $entity->getDt_abertura() ? $entity->getDt_abertura()->format('d-m-Y') : '',
            'dt_encerramento' => $entity->getDt_encerramento() ? $entity->getDt_encerramento()->format('d-m-Y') : '',
            'id_entidade' => $entity->getId_entidade(),
            'bl_usardoperiodoletivo' => $entity->getBl_usardoperiodoletivo(),
            'nu_maxalunos' => $entity->getNu_maxalunos(),
            'nu_diasencerramento' => $entity->getNu_diasencerramento(),
            'bl_semencerramento' => $entity->getBl_semencerramento(),
            'bl_todasentidades' => $entity->getBl_todasentidades(),
            'nu_diasaluno' => $entity->getNu_diasaluno(),
            'bl_semdiasaluno' => $entity->getBl_semdiasaluno(),
            'nu_diasextensao' => $entity->getNu_diasextensao(),
            'st_pontosnegativos' => $entity->getSt_pontosnegativos(),
            'st_pontospositivos' => $entity->getSt_pontospositivos(),
            'st_pontosmelhorar' => $entity->getSt_pontosmelhorar(),
            'st_pontosresgate' => $entity->getSt_pontosresgate(),
            'st_conclusoesfinais' => $entity->getSt_conclusoesfinais(),
            'st_justificativaacima' => $entity->getSt_justificativaacima(),
            'st_justificativaabaixo' => $entity->getSt_justificativaabaixo(),
            'dt_atualiza' => $entity->getDt_atualiza(),
            'id_modalidadesaladeaula' => $entity->getId_modalidadesaladeaula() ? $entity->getId_modalidadesaladeaula()->getId_modalidadesaladeaula() : NULL,
            'id_tiposaladeaula' => $entity->getId_tiposaladeaula() ? $entity->getId_tiposaladeaula()->getId_tiposaladeaula() : NULL,
            'id_periodoletivo' => $entity->getId_periodoletivo() ? $entity->getId_periodoletivo()->getId_periodoletivo() : NULL,
            'id_situacao' => $entity->getId_situacao() ? $entity->getId_situacao()->getId_situacao() : NULL,
            'id_categoriasala' => $entity->getId_categoriasala() ? $entity->getId_categoriasala()->getId_categoriasala() : NULL,
            'id_modalidadesala' => $entity->getId_modalidadesaladeaula()
        );

        $this->_helper->json($arrReturn);
    }

    public function dividirSalaAction()
    {
    }

    public function retornaVwSalaDeAulaQuantitativosSalasNaoIniciadasAction()
    {
        $array = array();

        $limit = $this->getParam('limit', NULL);
        $order = array('st_saladeaula' => 'ASC');
        $stSalaDeAulaLike = $this->getParam('q', NULL);

        $params = $this->getAllParams();
        unset($params['module'], $params['action'], $params['controller'], $params['limit'], $params['q']);

        $result = $this->negocio->retornaVwSalaDeAulaQuantitativosSalasNaoIniciadas($params, $stSalaDeAulaLike, $order, $limit);

        foreach ($result as $key => $value) {
            $array[] = $this->negocio->toArrayEntity($value);
        }

        $this->_helper->json($array);
    }

    public function salvarDivisaoSalaAction()
    {
        $params = $this->getAllParams();

        unset(
            $params['module'],
            $params['action'],
            $params['controller']
        );

        $negocio = new \G2\Negocio\GerenciaSalas();
        $result = $negocio->salvarDivisaoSala($params);
        $this->_helper->json($result);
    }

    public function returnTurmaSalaByProjetoAction()
    {

        $parametros = $this->limpaGetAllParams($this->getAllParams());
        $pp = new \G2\Negocio\ProjetoPedagogico();
        $retorno = $pp->getRepository('\G2\Entity\VwTurmaProjetoPedagogico')->returnTurmaSalaByProjeto($parametros);
        $this->_helper->json($retorno);

    }

    public function verificaAreaProjetoSalaAction()
    {
        $parametros = $this->limpaGetAllParams($this->getAllParams());
        $aps = new \G2\Negocio\Negocio();
        $resultado = $aps->findBy('\G2\Entity\AreaProjetoSala', $parametros);
        if ($resultado) {
            foreach ($resultado as $key => $value) {
                $array[] = $aps->toArrayEntity($value);
            }
            $this->_helper->json($array);
        } else {
            $this->_helper->json(array());
        }
    }
}

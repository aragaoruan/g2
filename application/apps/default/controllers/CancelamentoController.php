<?php

/**
 * Class CancelamentoController
 * Controller para Cancelamento
 * @author helder.silva <helder.silva@unyleya.com.br>
 * @since 2015-01-09
 * @package application
 * @subpackage controller
 */
class Default_CancelamentoController extends Ead1_ControllerRestrita
{
    /**
     * @var \G2\Negocio\Cancelamento
     */
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Cancelamento();
    }

    public function indexAction()
    {

    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
    }

    public function retornaMatriculasAction()
    {
        $post = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocio->retornaMatriculasUsuario($post);
        $arrReturn = array();

        if ($result) {
            foreach ($result as $key => $row) {
                $arrReturn[$key] = $this->negocio->toArrayEntity($row);
                $arrReturn[$key]['st_labelselect'] = $row->getId_matricula() . ' - ' . $row->getSt_projetopedagogico() . ' - ' . $row->getSt_evolucao();
            };
        }

        return $this->_helper->json($arrReturn);
    }

    public function salvarCancelamentoAction()
    {
        try {
            if ($this->getRequest()->isPost()) {
                $post = $this->getRequest()->getPost();
                $result = $this->negocio->salvarCancelamento($post);
                $this->_helper->json($this->negocio->toArrayEntity($result));
            }
        } catch (Exception $e) {
            $this->_helper->viewRenderer->setNoRender();
            return $this->getResponse()->setHttpResponseCode(500)
                ->appendBody($e->getMessage());
        }
    }

    public function salvarCalculosAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        try {
            if ($this->getRequest()->isPost()) {

                $body = $this->getRequest()->getRawBody();
                $post = (Array)json_decode($body);
                if (!$post) {
                    $post = $this->getRequest()->getPost();
                    if (!$post) {
                        throw new Exception("Nenhum dado para salvar.");
                    }
                }

                $arrReturn = $this->negocio->salvarCalculos($post);
                $retorno = [
                    'type' => \Ead1_IMensageiro::SUCESSO,
                    'tipo' => 'success',
                    'title' => 'Sucesso',
                    'mensagem' => 'Calculo cadastrado com sucesso!',
                    'objeto' => $this->negocio->toArrayEntity($arrReturn)
                ];
                $this->_helper->json($retorno);
            }

        } catch (Exception $e) {
            return $this->getResponse()->setHttpResponseCode(500)
                ->appendBody($e->getMessage());
        }
    }

    public function finalizarCartaAction()
    {
        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();
            $result = $this->negocio->finalizarCarta($post);
            $this->_helper->json($result);
        }
    }

    public function retornaLogAction()
    {
        $valores = array();
        $resultado = array();
        $posts = $this->limpaGetAllParams($this->getAllParams());
        foreach ($posts['id_matricula'] as $post) {
            $parametro = array("id_matricula" => $post);
            $result = $this->negocio->retornaLog($parametro);
            array_push($valores, $result);

        }
        foreach ($valores as $valore) {
            foreach ($valore as $va) {
                array_push($resultado, $va);
            }
        }
        $this->_helper->json($resultado);
    }

    public function retornaFiltroLogAction()
    {
        $post = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocio->retornaFiltroLog($post);
        $this->_helper->json($result);
    }

    public function retornaTextoCartaCreditoAction()
    {
        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();
            $result = $this->negocio->retornaTextoCartaCredito($post);
            if ($result) {
                $arrReturn = $this->negocio->toArrayEntity($result[0]);
                $mensageiro = new Ead1_Mensageiro($arrReturn, Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new Ead1_Mensageiro('Nenhum texto sistema cadastrado.', Ead1_IMensageiro::AVISO);
            }
            $this->_helper->json($mensageiro->toArrayAll());
        }
    }

    public function atualizaMateriaisAction()
    {
        $post = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocio->atualizaMateriais($post);
        $this->_helper->json($result);
    }

    public function atualizaCargaUtilizadaAction()
    {
        $post = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocio->atualizaCargaUtilizada($post);
        $this->_helper->json($result);
    }

    public function requerimentoCancelamentoAction()
    {
        $params = $this->limpaGetAllParams($this->getRequest()->getParams());

        if ($params) {

            $matriculaNegocio = new \G2\Negocio\Matricula();
            $entidadeNegocio = new \G2\Negocio\Entidade();
            $projetoPedagogicoNegocio = new \G2\Negocio\ProjetoPedagogico();
            $pessoaNegocio = new \G2\Negocio\Pessoa();

            $cancelamento = $this->negocio->retornaDadosCancelamento($params['id_matricula']);
            $matricula = $matriculaNegocio->retornaVwMatricula(array('id_matricula' => $cancelamento['id_matricula']), false);
            $entidade = $entidadeNegocio->findEntidade($matricula->getId_entidadematricula());
            $projetoArea = $projetoPedagogicoNegocio->findOneByVwProjetoArea(array('id_projetopedagogico' => $matricula->getId_projetopedagogico()));
            $contaPessoa = $pessoaNegocio->getContaPessoa($matricula->getId_usuario(), false);

            $this->view->entidade = $entidade;
            $this->view->matricula = $matricula;
            $this->view->cancelamento = $cancelamento;
            $this->view->projetoArea = $projetoArea;
            $this->view->contaPessoa = $contaPessoa;
        }
    }

    /**
     * Funcao salva
     * @return Zend_Controller_Response_Abstract
     * @throws Zend_Controller_Response_Exception
     */
    public function salvarJustificaticaCalculoCancelamentoAction()
    {

        try {
            if ($this->getRequest()->isPost()) {
                $post = $this->getRequest()->getPost();
                $result = $this->negocio->salvarJustificativaCalculoCancelamentoPos($post);
                if ($result == false) {
                    $result = new Ead1_Mensageiro('Cancelamento não encontrado.', Ead1_IMensageiro::ERRO);
                    $this->_helper->json($result);
                } else {
                    $this->_helper->json($result);
                }
            }
        } catch (Exception $e) {
            return $this->getResponse()->setHttpResponseCode(500)
                ->appendBody($e->getMessage());
        }
    }
}

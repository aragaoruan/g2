<?php

/**
 * Classe de controller MVC para controlar cadastros de produtos concursos
 * @author Helder Fernandes Silva <helder.silva@unyleya.com.br>
 * @since 2014-10-02
 * @package application
 * @subpackage controller
 */
Class Default_ConcursoProdutoController extends Ead1_ControllerRestrita
{
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\ConcursoProduto();
    }

    public function indexAction()
    {

    }
    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
    }

    public function findConcursoProdutoAction(){
        $id_concurso = $this->_getParam('id', null);
        $negocio = new \G2\Negocio\ConcursoProduto();
        $produto = new \G2\Negocio\Produto();
        $resultado = $negocio->findConcursoProduto($id_concurso);
        $array = array();
        foreach ($resultado as $key => $ret) {
            $objeto = $produto->find('\G2\Entity\Produto', $ret->getId_produto());
            $array[$key]['id_produto'] = $objeto->getId_produto();
            $array[$key]['st_produto'] = $objeto->getSt_produto();
            $array[$key]['id_concurso'] = $id_concurso;
        }
        $this->_helper->json($array);
    }

    public function deleteConcursoProdutoAction(){
        $params['id_concurso'] = $this->getParam('conc', null);
        $params['id_produto'] = $this->getParam('prod', null);
        $negocio = new \G2\Negocio\ConcursoProduto();
        $resultado = $negocio->deleteConcursoProduto($params);
        $this->_helper->json($resultado);
    }
 

}
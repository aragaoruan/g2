<?php

/**
 * Classe de controller MVC para área de conhecimento
 * @package application
 * @subpackage controller
 */
class Default_AreaConhecimentoController extends Ead1_ControllerRestrita {

    public function indexAction() {
        $this->_helper->viewRenderer->setNoRender(false);
        if ($this->getParam('id')) {
            $id = $this->getParam('id');
            $form = new AreaConhecimentoForm(array('id' => $id));
        } else {
            $form = new AreaConhecimentoForm();
        }
        $this->view->form = $form;
    }

    public function jsAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('area-conhecimento/index.js');
    }

    public function getAction() {
        $this->getResponse()
                ->appendBody("From getAction() returning the requested article");
    }

    public function postAction() {
        $this->getResponse()
                ->appendBody("From postAction() creating the requested article");
    }

    public function putAction() {
        $this->getResponse()
                ->appendBody("From putAction() updating the requested article");
    }

    public function recarregaArvoreAction() {
        $result = array();
        $this->_helper->viewRenderer->setNoRender(true);
        $negocioAreaEntidade = new G2\Negocio\AreaEntidade();
        $retorno = $negocioAreaEntidade->findBy('G2\Entity\AreaEntidade', array('id_areaconhecimento' => $this->_getParam('id_areaconhecimento')));
        // Zend_Debug::dump($retorno,__CLASS__.'('.__LINE__.')');die;
        if (!empty($retorno)) {
            foreach ($retorno as $entidade) {
                $result[] = /* array('id_entidade' => */$entidade->getId_entidade();
            }
        }
        echo Zend_Json_Encoder::encode($result);
    }

    /**
     * Busca somente as áreas que possuem algum projeto vinculado
     */
    public function getAreasComProjetoAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        $negocio = new \G2\Negocio\AreaConhecimento();
        echo Zend_Json_Encoder::encode($negocio->findAreaComProjeto());
    }

}

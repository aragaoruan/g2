<?php

/**
 * Controller Relatorio Folha de Pagamento
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 * @since 2016-01-04
 * @package application
 * @subpackage controller
 */
class Default_RelatorioFolhaPagamentoController extends Ead1_ControllerRestrita
{
    public function init()
    {
        $this->negocioDisciplina = new G2\Negocio\Disciplina();
    }
    public function indexAction()
    {
        $js = $this->view->render('relatorio-folha-pagamento/index.js');
        echo $this->view->headScript()->appendScript($js);

        $this->view->professores = $this->negocioDisciplina->retornarDadosCoordenadorDisciplina(array(
            'id_perfilpedagogico' => G2\Constante\Professor::PERFILPEDAGOGICOTUTOR,
            'id_situacaoperfil' => \G2\Constante\Situacao::TB_USUARIOPERFILENTIDADE_ATIVO
        ));

    }

    public function pesquisaDadosAction(){
        $params = $this->limpaGetAllParams($this->getRequest()->getParams());

        $negocioProfessor = new \G2\Negocio\Professores();

        $dados = $negocioProfessor->retornaProfessorAulasMinistradas($params);

        $this->view->dados = $dados;

    }

    public function gerarXlsAction()
    {
        $params = $this->limpaGetAllParams($this->getRequest()->getParams());
        $negocioProfessor = new \G2\Negocio\Professores();
        $dados = $negocioProfessor->retornaProfessorAulasMinistradas($params);
        $this->view->dados = $dados;
    }
}

<?php

/**
 * Class CategoriaController
 * Controller para Categoria
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2014-11-03
 * @package application
 * @subpackage controller
 */
class Default_CategoriaController extends Ead1_ControllerRestrita
{
    private $negocio;

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('categoria/index.js');
    }

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Categoria();
    }

    public function indexAction()
    {
    }

    public function retornaCategoriaEntidadeAction()
    {
        $params = $this->getRequest()->getParams();

        unset($params['module'], $params['action'], $params['controller']);
        $result = $this->negocio->retornarCategoriaEntidadeParams($params);

        $this->_helper->json($result);

    }

    public function recarregaArvoreAction()
    {

        $results = $this->negocio->findBy('\G2\Entity\CategoriaEntidade', array(
//            'id_entidade' => $this->negocio->sessao->id_entidade,
            'id_categoria' => $this->getParam('id_categoria')
        ));

        $response = array();
        if (!empty($results)) {
            foreach ($results as $result) {
                $response[] = $result->getId_entidade();
            }
        }
        $this->_helper->json($response);
    }

} 
<?php

class Default_ImportacaoDeDadosController extends Ead1_ControllerRestrita
{

    private $_negocio;
    private $negocio;

    public function init()
    {
        parent::init();
        $this->_negocio = new \G2\Negocio\Pessoa();
        $this->negocio = new \G2\Negocio\ImportacaoDeDados();
    }

    public function indexAction()
    {

    }

    public function indexJsAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        echo $this->view->render($this->_getParam('controller') . '/index.js');
    }

    public function processarArquivoAction()
    {
        try {
            $this->_helper->viewRenderer->setNoRender(true);

            $inputFileName = $_FILES['st_arquivoimportacao']['tmp_name'];

            $return = $this->negocio->verificarArquivoEnviado($inputFileName);
            $code = $return['retorno'];
            unset($return['retorno']);

            $this->getResponse()
                ->appendBody(Zend_Json::encode($return))
                ->setHttpResponseCode($code ? 200 : 500);

        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}
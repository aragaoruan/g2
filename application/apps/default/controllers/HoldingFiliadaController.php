<?php

/**
 * Classe Controller para Holding Filiada
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */
class Default_HoldingFiliadaController extends Ead1_ControllerRestrita
{

    private $negocio;

    public function init()
    {
        $this->form = new GradeHorariaForm();
        $this->negocio = new \G2\Negocio\GradeHoraria();
    }

    public function indexAction()
    {
        $id = $this->_getParam('id');
        if ($id) {
            $negocio = new \G2\Negocio\Holding();
            $result = $negocio->retornarHoldingFiliada();
            $this->view->dados = $result->getFirstMensagem();
        }
    }


    public function buscaHoldingFiliadaAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $negocio = new \G2\Negocio\Holding();
        $result = $negocio->retornarHoldingFiliada();
        return $this->_helper->json($result);
    }

    public function salvaHoldingFiliadaAction()
    {

        $dados = $this->limpaGetAllParams($this->getRequest()->getParams());
        $negocio = new \G2\Negocio\Holding();
        $result = $negocio->salvarHoldingFiliada($dados);

        return $this->_helper->json($result);
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
    }

    public function desvincularAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $id = intval($this->getParam('id'));

        $negocio = new \G2\Negocio\Holding();
        $result = $negocio->desvincularHoldingFiliada($id);
        if (!$result->getId_holdingfiliada()) {
            $this->getResponse()->setHttpResponseCode(204)->appendBody('Registro removido.');
        } else {
            $this->getResponse()->setHttpResponseCode(400)->appendBody('Erro ao remover registro.');
        }

    }

    public function getPerfisEntidadesHoldingFiliadasAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $params = $this->getAllParams();
        $negocio = new \G2\Negocio\Holding();
        $result = $negocio->retornarPerfisUsuarioEntidadesMesmaHolding($params);
        return $this->_helper->json($result);
    }

}

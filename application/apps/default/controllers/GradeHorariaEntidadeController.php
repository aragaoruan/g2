<?php

/**
 * Classe Controller para Grade Horaria por Entidade
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */
class Default_GradeHorariaEntidadeController extends Ead1_ControllerRestrita
{
    private $negocio;
    private $negocioGrade;

    public function init()
    {
        $this->negocio = new \G2\Negocio\GradeHoraria();
        $this->negocioGrade = new G2\Negocio\Disciplina();
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        echo $this->view->render($this->_getParam('controller') . '/index.js');
    }

    public function indexAction()
    {
        $this->view->professores = $this->negocioGrade->retornarDadosCoordenadorDisciplina(array(
            'id_perfilpedagogico' => 1,
            'id_situacaoperfil' => \G2\Constante\Situacao::TB_USUARIOPERFILENTIDADE_ATIVO
        ));
    }

}
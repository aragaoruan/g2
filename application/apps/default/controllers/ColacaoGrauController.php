<?php

use G2\Negocio\ColacaoGrau;

/**
 * Controller para Colação de Grau
 * @author Rafael Leite <rafael.leite@unyleya.com.br>
 * @since  22-06-2017
 * @package application
 * @subpackage controller
 */
class Default_ColacaoGrauController extends Ead1_ControllerRestrita
{

    /**
     * @var \G2\Negocio\ColacaoGrau
     */
    private $negocio;
    private $relatorio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\ColacaoGrau();
        $this->relatorio = new \G2\Negocio\Relatorio();
    }

    public function indexAction()
    {
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('colacao-grau/index.js');
    }

    public function retornaAlunoColacaoAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $params = $this->limpaGetAllParams($this->getAllParams());


        unset($params['dt_colacaolote'], $params['bl_presencalote']);

        $resultado = $this->negocio->pesquisaAlunoColacao($params);

        $arrReturn = [];
        if ($resultado) {
            $arrReturn = (new \G2\Transformer\ColacaoGrau\AlunoColacaoTransformer ($resultado))
                ->getTransformer();
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    public function nominataAction()
    {
        $arrEntidade = $this->negocio->find(\G2\Entity\Entidade::class, $this->negocio->getId_entidade());
        $this->view->dadosEntidade = $this->negocio->toArrayEntity($arrEntidade);
    }

    public function listaPresencaAction()
    {
        $arrEntidade = $this->negocio->find(\G2\Entity\Entidade::class, $this->negocio->getId_entidade());
        $this->view->dadosEntidade = $this->negocio->toArrayEntity($arrEntidade);
    }


    public function retornoPesquisaAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $resultado = $this->negocio->findByPesquisaProjetoModal($this->getAllParams());
        $arrReturn = array();
        $arrPP = array();
        if ($resultado) {
            foreach ($resultado as $row) {

                if (in_array($row->getId_projetopedagogico(), $arrPP)) {
                    continue;
                }

                $arrReturn[] = array(
                    'id_projetopedagogico' => $row->getId_projetopedagogico(),
                    'st_projetopedagogico' => $row->getSt_projetopedagogico(),
                    'st_areaconhecimento' => $row->getSt_areaconhecimento(),
                    'st_nivelensino' => $row->getSt_nivelensino(),
                    'st_nomeentidade' => $row->getSt_nomeentidade(),
                    'st_situacao' => $row->getSt_situacao(),
                    'bl_turma' => ($row->getBl_turma() ? 1 : 0)
                );

                $arrPP[] = $row->getId_projetopedagogico();
            }
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    public function salvarColacaoGrauAction()
    {
        try {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);

            $dados = $this->getAllParams();
            $return = $this->negocio->salvarColacaoGrau($dados);
            if ($return->getType() != Ead1_IMensageiro::TYPE_SUCESSO) {
                throw new Exception($return->getText());
            }
            return $this->_helper->json($return);
        } catch (Exception $e) {
            return $this->getResponse()
                ->setHttpResponseCode(500)
                ->appendBody($e->getMessage());
        }
    }


    public function checkDataColacaoAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        try {
            //verifica se o parametro não foi passado
            if (!$this->getParam('id_matricula')) {
                throw new Exception("Informe o ID da matrícula");
            }

            //busca os dados de colação ativo vinculado com a matricula
            $matColacao = new \G2\Entity\MatriculaColacao();
            $matColacao->findOneBy(array(
                'id_matricula' => $this->getParam('id_matricula'),
                'bl_ativo' => true
            ));

            //verifica se achou resultado e se tem data de colação
            if ($matColacao && $matColacao->getDt_colacao()) {
                return $this->getResponse()->appendBody(json_encode(true));
            }

            return $this->getResponse()->appendBody(json_encode(false));

        } catch (Exception $e) {
            return $this->getResponse()
                ->setHttpResponseCode(500)
                ->appendBody($e->getMessage());
        }
    }

    public function getAction()
    {
        $this->getResponse()
            ->appendBody("From getAction() returning the requested article");
    }

    public function postAction()
    {
        $this->getResponse()
            ->appendBody("From postAction() creating the requested article");
    }

    public function putAction()
    {
        $this->getResponse()
            ->appendBody("From putAction() updating the requested article");
    }

}

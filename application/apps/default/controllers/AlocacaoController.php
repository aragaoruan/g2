<?php

use G2\Entity\EncerramentoSala;
use G2\Entity\VwAlocacao;
use G2\Events\TccDevolvido;
use G2\Negocio\Alocacao;
use G2\Negocio\Tramite;

class Default_AlocacaoController extends \Ead1_ControllerRestrita
{
    private $negocio;
    private $entity;

    public function init()
    {
        parent::init();
        $this->negocio = new Alocacao();
        $this->entity = new VwAlocacao();
    }

    public function devolverTccAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        //cria na sessão o parametro de liberacao do perfil de encerramento
        Zend_Session::namespaceUnset('liberacao');
        $sessao = new \Zend_Session_Namespace('liberacao');
        $sessao->id_funcionalidade = $this->getParam('liberacao');

        if ($this->negocio->atualizaSituacaoTcc(
            $this->getParametrosDeBusca(),
            \G2\Constante\Situacao::TB_ALOCACAO_DEVOLVIDO_AO_TUTOR))
        {
            $eventParams = $this->getRequest()->getParams();
            $eventParams['id_alocacao'] = $this->getParametrosDeBusca()['id_alocacao'];
            $tccDevolvido = new TccDevolvido(new EncerramentoSala(), new Tramite());
            $tccDevolvido->dispatch($eventParams);
            return $this->responseSuccess('TCC devolvido ao tutor com sucesso!');
        }

        return $this->responseNotModified('A situação do TCC não foi alterada!');
    }

    private function getParametrosDeBusca()
    {
        $preparaArray = $this->prepareArrayFromRequest(array('id_saladeaula', 'id_usuario'));
        return [
            'id_alocacao' => $this->getIdAlocacaoBy($preparaArray),
        ];
    }

    private function getIdAlocacaoBy($search)
    {
        $search['bl_ativo'] = 1;
        return $this->negocio->findOneBy($this->entity, $search)->getId_alocacao();
    }
}

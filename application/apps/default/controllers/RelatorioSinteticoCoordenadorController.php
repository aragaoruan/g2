<?php

/**
 * Controller Relatorio Sintético do Coordenador
 * @author Arthur Luiz Lara Quites <arthur.quites@unyleya.com.br>
 * @since 17-04-2018
 * @package application
 * @subpackage controller
 */
class Default_RelatorioSinteticoCoordenadorController extends Ead1_ControllerRestrita
{
    private $negocio;
    private $sessao;
    public function init()
    {
        $this->negocio = new G2\Negocio\Negocio();
        $this->sessao = new Zend_Session_Namespace('geral');
    }

    public function indexAction()
    {
        $js = $this->view->render('relatorio-sintetico-coordenador/index.js');
        echo $this->view->headScript()->appendScript($js);
    }

    public function retornaCoordenadoresDeProjetoPedagogicoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $projetoPedagogico = new \G2\Negocio\ProjetoPedagogico();
        try {
            $coordenadores = $projetoPedagogico->retornaCoordenadoresDeProjetoPedagogico($this->sessao->id_entidade);
        }catch (Exception $e){
            return false;
        }
        $this->_helper->json($coordenadores);
    }

    public function verificacoordenadordeprojetoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->getAllParams();
        $resultado = $this->negocio->findOneBy('G2\Entity\Perfil', array('id_perfil' => $params['id_perfil']));
        $this->_helper->json($this->negocio->toArrayEntity($resultado));
    }

    public function retornaRelatorioSinteticoCoordenadorProjetosPedagogicosAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $negocio = new \G2\Negocio\Relatorio();
        $params = $this->getAllParams();
        try{
            $resultado = $negocio->relatorioSinteticoCoordenadorProjetosPedagogicos($params);
        }catch (Exception $e){
            $resultado = new Ead1_Mensageiro('Houve um erro na hora de recuperar o relatório', Ead1_IMensageiro::ERRO);
        }
        return $this->_helper->json($resultado);

    }

    public function recuperarProjetosPedagogicosCoordenadorAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $projetopedagogico = new \G2\Negocio\ProjetoPedagogico();
        $id_coordenador = $this->getParam('id_coordenador');

        try{
            return $this->_helper->json($projetopedagogico->retornarProjetoPedagogicoCoordenador($id_coordenador, $this->sessao->id_entidade));
        }catch (Exception $e){
            return $this->_helper->json($e->getMessage());
        }
    }

    public function gerarXlsAction()
    {
//        print_r($this->getParam('xls'));exit;
        $this->view->dados = $this->getParam('xls');
    }
}

<?php

/**
 * Controller para Entity
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2014-12-15
 * @package application
 * @subpackage controller
 */


class Default_NucleoCoController extends Ead1_Controller
{
    /**
     * @var \G2\Negocio\Negocio $negocio
     */
    private $negocio;

    /**
     * @var \G2\Negocio\AssuntoCo $assunto
     */
    private $assunto;

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('nucleo-co/index.js');
    }

    public function init()
    {
        $this->negocio = new \G2\Negocio\Negocio();
        $this->assunto = new \G2\Negocio\AssuntoCo();
    }

    public function indexAction()
    {
    }

    public function getTipoOcorrenciaAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $to = new TipoOcorrenciaTO();

        $bo = new AssuntoCoBO();
        $mensageiro = $bo->retornaTipoOcorrencia($to);

        $this->_helper->json($mensageiro);
    }

    public function getPessoaInstituicaoAction()
    {
        $data = $this->getAllParams();

        $to = new VwEntidadeCompartilhaDadosTO();
        foreach ($data as $key => $value) {
            if (array_key_exists($key, $to)) {
                $to->{$key} = $value;
            }
        }

        $bo = new EntidadeBO();
        $mensageiro = $bo->retornarVwEntidadeCompartilhaDados($to);
        $this->_helper->json($mensageiro);
    }

    public function getPessoaAutocompleteAction()
    {
        $data = $this->getAllParams();

        $to = new VwPessoaTO();
        foreach ($data as $key => $value) {
            if (array_key_exists($key, $to)) {
                $to->{$key} = $value;
            }
        }

        $bo = new NucleoCoBO();
        $mensageiro = $bo->retornaPessoas($to);
        $this->_helper->json($mensageiro);
    }

    public function getHoldingsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        try {
            $id_entidade = $this->getParam('id_entidade');

            /** @var \G2\Repository\Holding $rep */
            $rep = $this->negocio->getRepository('\G2\Entity\Holding');
            $results = $rep->getHoldingsByEntidade($id_entidade);

            $this->_helper->json($results ?: array());
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(500)->appendBody($e->getMessage());
        }
    }

}

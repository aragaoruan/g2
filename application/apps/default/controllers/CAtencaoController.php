<?php

/**
 * Description of CAtencaoController
 *
 * @author rafael.bruno
 */
class Default_CAtencaoController extends Ead1_ControllerRestrita {

    public function indexAction(){
        $this->_redirect('/c-atencao/imprimir-ocorrencia');
    }
    
    public function imprimirOcorrenciaAction() {

        if ($this->_getParam('numero')) {

            $bo = new CAtencaoBO();
            $ocorrenciaTO = new VwOcorrenciaTO();
            $ocorrenciaTO->setId_ocorrencia($this->_getParam('numero'));
            $ocorrencia = $bo->retornaVwOcorrenciaPorId($ocorrenciaTO);

            $vwOcorrenciaTO = new VwOcorrenciaTO();
            $vwOcorrenciaTO->setId_ocorrencia($this->_getParam('numero'));
            $interacoes = $bo->retornaInteracoesOcorrencia($vwOcorrenciaTO);

            $entidadeTO = new EntidadeTO();
            $entidadeTO->setId_entidade($ocorrencia->getId_entidade());
            $entidade = new EntidadeBO();
            $entidade->retornaEntidade($entidadeTO);
            $dataEntidade = $entidade->mensageiro->getMensagem();

            $this->view->entidade = $dataEntidade[0];
            $this->view->interacoes = $interacoes->getMensagem();
            $this->view->ocorrencia = $ocorrencia;
        }else{
            $this->view->mensagem = 'Número da ocorrência invalido!';
        }
    }

}
<?php

/**
 * Classe de controller MVC para controlar o Projeto Pedagógico
 * @author Elcio Guimarães <elcioguimaraes@gmail.com>
 *
 * @package application
 * @subpackage controller
 */
class Default_ProjetoPedagogicoController extends Ead1_ControllerRestrita
{

    /**
     * @var \G2\Negocio\ProjetoPedagogico
     */
    private $negocio;

    public function init()
    {
        parent::init();
        parent::setupDoctrine();
        $this->negocio = new \G2\Negocio\ProjetoPedagogico();
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('projeto-pedagogico/index.js');
    }

    public function indexAction()
    {

        $this->view->id = $this->_getParam('id', null);
    }

    public function atualizaPonderacaoCalculadaDisciplinaAction()
    {
        $idProjeto = $this->getParam('id_projetopedagogico');
        if ($idProjeto) {
            $negocio = new G2\Negocio\ProjetoPedagogico();
            $result = $negocio->atualizaPonderacaoCalculada($idProjeto);
            if ($result) {
                $return = array();
                foreach ($result as $ent){
                    $return[] = $negocio->toArrayEntity($ent);
                }
                $this->_helper->json(array(
                    'type' => 'success',
                    'title' => 'Sucesso!',
                    'text' => 'Ponderação Calculada salva com sucesso.',
                    'obj' => $return
                ));
            } else {
                $this->_helper->json(array(
                    'type' => 'error',
                    'title' => 'Erro!',
                    'text' => 'Erro ao tentar atualizar Ponderação Calculada.'
                ));
            }
        } else {
            $this->_helper->json(array(
                'type' => 'error',
                'title' => 'Erro!',
                'text' => 'Erro ao tentar atualizar Ponderação Calculada, Id do Projeto Pedagogico não informado.'
            ));
        }
    }

    /**
     * Retorna os projetos pedagógicos filtrados por área
     */
    public function getByAreaAction()
    {

        $negocio = new \G2\Negocio\ProjetoPedagogico();
        $projetos = $negocio->retornarProjetosRegraPagamento($this->getParam('id_areaconhecimento'));

        if (count($projetos) > 0) {
            $this->getResponse()->setHttpResponseCode(200);
            $this->_helper->json($projetos);
        } else {
            $this->getResponse()->setHttpResponseCode(404);
            $this->_helper->json(array());
        }
    }

    public function projetoTurmaEntidadeRecursivaAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $negocio = new \G2\Negocio\ProjetoPedagogico();
        $mensageiro = $negocio->retornaProjetoPedagogicoEntidadeRecursiva();
        if ($mensageiro->getType() == Ead1_IMensageiro::TYPE_SUCESSO) {
            $this->getResponse()->appendBody(json_encode($mensageiro->getMensagem()));
            $this->getResponse()->setHttpResponseCode(200);
        } else {
            $this->getResponse()->appendBody(json_encode($mensageiro->getText()));
            $this->getResponse()->setHttpResponseCode(400);
        }
    }

    public function retornarEntidadeComTurmaByProjetoAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $idProjeto = $this->getParam('id_projeto');
        $negocio = new \G2\Negocio\ProjetoPedagogico();
        $mensageiro = $negocio->retornaEntidadeComTurmaByProjeto($idProjeto);
        if ($mensageiro->getType() == Ead1_IMensageiro::TYPE_SUCESSO) {
            $this->getResponse()->appendBody(json_encode($mensageiro->getMensagem()));
            $this->getResponse()->setHttpResponseCode(200);
        } else {
            $this->getResponse()->appendBody(json_encode($mensageiro->getText()));
            $this->getResponse()->setHttpResponseCode(400);
        }
    }

    public function retornarTurmaProjetoUnidadeAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $params = $this->limpaGetAllParams($this->getAllParams());

        $idProjeto = $this->getParam('id_projeto');
        $idEntidade = $this->getParam('id_entidade');
        $idTurno = $this->getParam('id_turno');

        $negocio = new \G2\Negocio\ProjetoPedagogico();
        $mensageiro = $negocio->retornarTurmaByProjetoUnidade($idProjeto, $idEntidade, $idTurno);

        if ($mensageiro->getType() == Ead1_IMensageiro::TYPE_SUCESSO) {
            $this->getResponse()->appendBody(json_encode($mensageiro->getMensagem()));
            $this->getResponse()->setHttpResponseCode(200);
        } else {
            $this->getResponse()->appendBody(json_encode($mensageiro->getText()));
            $this->getResponse()->setHttpResponseCode(400);
        }

    }


    public function retornarDisciplinaProjetoUnidadeAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $idProjeto = $this->getParam('id_projeto');
        $idEntidade = $this->getParam('id_entidade');
        $idTurma = $this->getParam('id_turma');

        $negocio = new \G2\Negocio\ProjetoPedagogico();
        $mensageiro = $negocio->retornarDisciplinasProjetoUnidade($idProjeto, $idEntidade, $idTurma);

        if ($mensageiro->getType() == Ead1_IMensageiro::TYPE_SUCESSO) {
            $this->getResponse()->appendBody(json_encode($mensageiro->getMensagem()));
            $this->getResponse()->setHttpResponseCode(200);
        } else {
            $this->getResponse()->appendBody(json_encode($mensageiro->getText()));
            $this->getResponse()->setHttpResponseCode(400);
        }

    }

    public function retornarTurnoByProjetoAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $negocio = new \G2\Negocio\ProjetoPedagogico();
        $mensageiro = $negocio->retornarTurnoByProjeto($this->getParam('id_projeto'));
        if ($mensageiro->getType() == Ead1_IMensageiro::TYPE_SUCESSO) {
            $this->getResponse()->appendBody(json_encode($mensageiro->getMensagem()));
            $this->getResponse()->setHttpResponseCode(200);
        } else {
            $this->getResponse()->appendBody(json_encode($mensageiro->getText()));
            $this->getResponse()->setHttpResponseCode(400);
        }

    }

    /*
     * Retorna projetos pedagogicos por entidade
     */
    public function projetoPedagogicoEntidadeAction()
    {
        $params = $this->limpaGetAllParams($this->getRequest()->getParams());

        $result = $this->negocio->findVwProjetoEntidade($params);

        $this->_helper->json($result);
    }

    /**
     * Retorna projetos pedagogicos incomuns de uma ou mais salas de aula
     */
    public function retornaProjetosEmComumEntreSalasAction()
    {

        $result = array();

        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);

        $negocio = new \G2\Negocio\ProjetoPedagogico();
        $result = $negocio->retornaProjetosEmComumEntreSalas($params);

        $this->_helper->json($result);

    }

    /**
     * Salvar somente o id_anexoementa
     */
    public function salvarAnexoEmentaAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $id_projetopedagogico = $this->getParam('id_projetopedagogico');

        if ($id_projetopedagogico && isset($_FILES['arquivo']['name'])) {
            $arquivo = $_FILES['arquivo'];

            $mensageiro = $this->negocio->salvarAnexoEmenta($id_projetopedagogico, $arquivo);
            $this->_helper->json($mensageiro);
        }
    }

    /*
     * Retorna projetos pedagogicos por entidade
     */
    public function retornaPonderacaoCalculadaAction()
    {
        $params = $this->limpaGetAllParams($this->getRequest()->getParams());

        $result = $this->negocio->findVwModuloDisciplina($params);

        $this->_helper->json($result);
    }

    /*
     * Retorna os Projetos Pedagogicos para entidade para combobox Projeto Pedagogico
     */
    public function retornaProjetosPedagogicosEntidadeAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $negocio = new \G2\Negocio\ProjetoPedagogico();
        $return = $negocio->findProjetoEntidade(['bl_ativo' => true]);

        $result = array();
        $order = array();
        foreach ($return->getMensagem() as $key => $value) {
            if( $value->getId_projetopedagogico()->getId_situacao()->getId_situacao() == \G2\Constante\Situacao::TB_PROJETOPEDAGOGICO_ATIVO){
                $result[$key]['id_projetopedagogico'] = $value->getId_projetopedagogico()->getId_projetopedagogico();
                $result[$key]['st_projetopedagogico'] = $value->getId_projetopedagogico()->getSt_projetopedagogico();
                $order[$key] = $value->getId_projetopedagogico()->getSt_projetopedagogico();
            }
        }
        array_values($result);
        array_multisort($order, SORT_ASC, $result);
        echo json_encode($result);

    }
}

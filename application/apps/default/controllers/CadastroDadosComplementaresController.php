<?php
/**
 * Created by PhpStorm.
 * User: Unyleya06
 * Date: 20/10/2015
 * Time: 10:29
 */

class Default_CadastroDadosComplementaresController extends Ead1_Controller {

    public function indexAction(){

        $form = new CadastroRapidoPessoaForm();
        $form->getElement('sg_uf')->setAttrib('required',true);
        $form->getElement('sg_uf')->setAttrib('name','sg_ufinstituicao');
        $form->getElement('sg_uf')->setAttrib('id','sg_ufinstituicao');
        $this->view->form = $form;

    }

    public function jsAction(){
        $this->_helper->viewRenderer->setNoRender(true);

        echo $this->view->render('cadastro-dados-complementares/index.js');
    }


}
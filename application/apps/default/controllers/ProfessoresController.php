<?php

/**
 * Controller para Professores - funcionalidade de cadastro em lote
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @since 2015-05-12
 * @package application
 * @subpackage controller
 */


class Default_ProfessoresController extends Ead1_Controller
{

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('professores/index.js');
    }

    public function init()
    {
    }

    public function indexAction()
    {
    }

    public function getProfessoresAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        $negocio = new \G2\Negocio\Disciplina();

        $results = $negocio->retornarDadosCoordenadorDisciplina(array(
                                'id_perfilpedagogico' => 1,
                                'id_situacaoperfil' => 15
                            ));

        if (!$results) {
            $results = new \Ead1_Mensageiro();
            $results->setMensageiro('Nenhum professor encontrado', \Ead1_IMensageiro::AVISO, null);
        }

        $this->_helper->json($results);
    }


    public function getDisciplinasAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        $negocio = new \G2\Negocio\GerenciaSalas();
        $mensageiro = $negocio->findByPesquisaDisciplinaModal(array(
                'id_entidade' => $negocio->sessao->id_entidade
        ));
        $results = array();
        if(is_array($mensageiro)):
            foreach ($mensageiro as $entity) {
                $results[] = $negocio->toArrayEntity($entity);
            }
        endif;
        $this->_helper->json($results);
    }


    public function pesquisaProfessorAction(){
        $this->_helper->viewRenderer->setNoRender(true);
        $negocio = new \G2\Negocio\Professores();
        $mensageiro = $negocio->retornaPesquisaProfessores($this->limpaGetAllParams($this->getAllParams()));
        $results = array();
        foreach ($mensageiro as $entity) {
            $results[] = $negocio->toArrayEntity($entity);
        }
        $this->_helper->json($results);
    }

    public function gerarXlsPesquisaProfessorAction(){
        $negocio = new \G2\Negocio\Professores();
        $mensageiro = $negocio->retornaPesquisaProfessores($this->limpaGetAllParams($this->getAllParams()));
        return $negocio->gerarXlsPesquisaProfessores($mensageiro);
    }

    public function addProfessorLoteAction(){
        $this->_helper->viewRenderer->setNoRender(true);
        $negocio = new \G2\Negocio\Professores();
        $mensageiro = $negocio->addProfessores($this->limpaGetAllParams($this->getAllParams()));
        $this->_helper->json($mensageiro);
    }

    public function removeProfessorLoteAction(){
        $this->_helper->viewRenderer->setNoRender(true);
        $negocio = new \G2\Negocio\Professores();
        $mensageiro = $negocio->removeProfessores($this->limpaGetAllParams($this->getAllParams()));
        $this->_helper->json($mensageiro);
    }

}

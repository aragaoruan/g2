<?php
/**
 * GatewayController
 * 
 * @author eder.mariano
 * 
 * @package application
 * @subpackage controller
 * @version 1
 */
class Default_GatewayController extends Ead1_Controller {

	/**
	 * Método para habilitar o amf para comunicação com Flex
	 */
	public function amfAction() {
		$this->getHelper('viewRenderer')->setNoRender();
		$amf = new Zend_Amf_Server();
		$amf->setProduction(false);
		$amf->addDirectory(APPLICATION_PATH . '/../application/ro');
		echo $amf->handle();
	}
	
}
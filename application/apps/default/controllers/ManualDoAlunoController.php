<?php

class Default_ManualDoAlunoController extends Ead1_ControllerRestrita
{

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('manual-do-aluno/index.js');
    }

    public function indexAction()
    {

    }


    public function uploadAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        if (isset($_FILES['arquivo']['name'])) {
            $arquivo = $_FILES['arquivo'];

            // Validar extensao do arquivo
            $extensions = array('pdf', 'doc', 'docx', 'jpg', 'jpeg', 'png');
            $fileExtension = pathinfo($arquivo['name'], PATHINFO_EXTENSION);

            if(!in_array($fileExtension, $extensions)) {
                return false;
            }

            $negocio = new \G2\Negocio\ConfiguracaoEntidade();
            $mensageiro = $negocio->salvarManualDoAluno($arquivo);

            $this->_helper->json($mensageiro);
        }
    }

}


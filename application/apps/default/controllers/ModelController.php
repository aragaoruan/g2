<?php
class Default_ModelController extends Ead1_Controller{
	
	public function indexAction(){
		
	}
	
	
	
	public function gerarAction(){
		$params = $this->_request->getParams();
		$desc = Zend_Db_Table_Abstract::getDefaultAdapter()->describeTable($params['tabela']);
		$primary = array();
		foreach ($desc as $col){
			if($col["PRIMARY"]){
				$primary[] = "'".$col["COLUMN_NAME"]."'";
			}
		}
		if(empty($primary)){
			$keys = array_keys($desc);
			$primary[] = "'".$keys[0]."'";
		}
		
		$this->view->tabela = $params['tabela'];
		
		$htmlORM = "";
		$htmlORM .= "class ".($params['class_name'] ? $params['class_name']."ORM" : "ClassNameORM")." extends Ead1_ORM{\n";
		$htmlORM .= "\tpublic \$_name = '".$params['tabela']."';\n";
		$htmlORM .= "\tpublic \$_primary = array(".implode(", ", $primary).");\n";
		$htmlORM .= "}";
		$this->view->orm = $htmlORM;
		
		$classNameTO = ($params['class_name'] ? $params['class_name']."TO" : "ClassNameTO");
		
		$htmlTO = "";
		$htmlTO .= "class ".$classNameTO." extends Ead1_TO_Dinamico{\n\n";
		foreach($desc as $col){
			$htmlTO .= "\tpublic $".$col["COLUMN_NAME"].";\n";
		}
		$htmlTO .= "\n";
		foreach($desc as $col){
			$htmlTO .= "\n\t/**";
			$htmlTO .= "\n\t * @return the $".$col["COLUMN_NAME"];
			$htmlTO .= "\n\t */";
			$htmlTO .= "\n\tpublic function get".ucwords($col["COLUMN_NAME"])."(){ \n";
			$htmlTO .= "\t\treturn \$this->".$col["COLUMN_NAME"].";";
			$htmlTO .= "\n\t}\n";
		}
		$htmlTO .= "\n";
		foreach($desc as $col){
			$htmlTO .=  "\n\t/**";
			$htmlTO .=  "\n\t * @param field_type $".$col["COLUMN_NAME"];
			$htmlTO .=  "\n\t */";
			$htmlTO .=  "\n\tpublic function set".ucwords($col["COLUMN_NAME"])."($".$col["COLUMN_NAME"]."){ \n";
			$htmlTO .=  "\t\t\$this->".$col["COLUMN_NAME"]." = $".$col["COLUMN_NAME"].";";
			$htmlTO .=  "\n\t}\n";
		}
		$htmlTO .= "\n";
		$htmlTO .= "}";
		$this->view->to = $htmlTO;
		
		
		$htmlTOFlex = "";
		$htmlTOFlex .= "/****************************************************************************************** \n";
		$htmlTOFlex .= "* Ead1 - ".date("Y")." \n";
		$htmlTOFlex .= "* \n";
		$htmlTOFlex .= "* @name ".$classNameTO." \n";
		$htmlTOFlex .= "* # Encoding UTF-8 \n";
		$htmlTOFlex .= "* \n";
		$htmlTOFlex .= "* Classe que encapsula dados da tabela ".$params['tabela']." \n";
		$htmlTOFlex .= "* \n";
		$htmlTOFlex .= "* -------------------------------------------------------------------------------------------- \n";
		$htmlTOFlex .= "* \n";
		$htmlTOFlex .= "* @author Eduardo Romão - ejushiro@gmail.com \n";
		$htmlTOFlex .= "* \n";
		$htmlTOFlex .= "******************************************************************************************/ \n";
		$htmlTOFlex .= "\n";
		$htmlTOFlex .= "package br.com.ead1.gestor2.models.TO{";
		$htmlTOFlex .= "\n \n";
		$htmlTOFlex .= "\t [RemoteClass(alias = '".$classNameTO."')]";
		$htmlTOFlex .= "\n \n";
		$htmlTOFlex .= "\t [Bindable]";
		$htmlTOFlex .= "\n";
		$htmlTOFlex .= "\t public class ".$classNameTO." extends MasterTO {";
		foreach($desc as $col){
			$htmlTOFlex .= "\n \n";
			$type = '';
			switch($col['DATA_TYPE']){
				case 'varchar':
					$type = 'String';
					break;
				case 'bit':
					$type = 'Boolean';
					break;
				case 'int':
					$type = 'int';
					break;
				case 'datetime2':
					$type = 'Date';
					break;
				case 'text':
					$type = 'String';
					break;
				case 'date':
					$type = 'Date';
					break;
				case 'numeric':
					$type = 'Number';
					break;
			}
			$htmlTOFlex .= "\t \t private var _".$col["COLUMN_NAME"].":".$type.";";
			$htmlTOFlex .= "\n \n";
			$htmlTOFlex .= "\t \t public function set ".$col["COLUMN_NAME"]."(value:".$type."):void {";
			$htmlTOFlex .= "\n";
			$htmlTOFlex .= "\t \t \t _".$col["COLUMN_NAME"]." = value;";
			$htmlTOFlex .= "\n";
			$htmlTOFlex .= "\t \t }";
			$htmlTOFlex .= "\n \n";
			$htmlTOFlex .= "\t \t public function get ".$col["COLUMN_NAME"]."():".$type." {";
			$htmlTOFlex .= "\n";
			$htmlTOFlex .= "\t \t \t return _".$col["COLUMN_NAME"].";";
			$htmlTOFlex .= "\n";
			$htmlTOFlex .= "\t \t }";
		}
		$htmlTOFlex .= "\n \n";
		$htmlTOFlex .= "\t }";
		$htmlTOFlex .= "\n";
		$htmlTOFlex .= "}";
		$this->view->toFlex = $htmlTOFlex;
	}
	
	
	
}
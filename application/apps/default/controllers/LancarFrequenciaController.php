<?php

use G2\Negocio\GerenciaProva;

/**
 * Controller para lançar frequência de alunos que realizaram avaliações presenciais. (provas finais e de recuperação)
 * @author Débora Castro <debora.castro@unyleya.com.br>
 * @since  22/01/2014
 * @package application
 * @subpackage controller
 */
class Default_LancarFrequenciaController extends Ead1_ControllerRestrita
{

    /**
     * @var \G2\Negocio\GerenciaProva $negocio
     */
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\GerenciaProva();
    }

    public function indexAction()
    {
        echo $this->view->headScript()->appendFile('/lancar-frequencia/js');

    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('lancar-frequencia/index.js');
    }

    public function pesquisaAlunosFrequenciaAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $params = array('id_entidade' => $this->negocio->sessao->id_entidade);

        if ($this->getParam('st_nomecompleto')) {
            $params['st_nomecompleto'] = $this->getParam('st_nomecompleto');
        }

        if ($this->getParam('id_aplicadorprova')) {
            $params['id_aplicadorprova'] = $this->getParam('id_aplicadorprova');
        }
        if ($this->getParam('dt_aplicacao')) {
            $dataEntrada = $this->negocio->converteDataBanco($this->getParam('dt_aplicacao'));
            $params['dt_aplicacao'] = $dataEntrada;
        }
        if ($params) {
            $params['id_situacao'] = array(
                \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_AGENDADO,
                \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_REAGENDADO
            );

            $result = $this->negocio->findByVwAvaliacaoAgendamento($params);

            $configuracao = $this->negocio->findOneBy('\G2\Entity\VwEntidadeEsquemaConfiguracao', array(
                'id_entidade'         => $this->negocio->sessao->id_entidade,
                'id_itemconfiguracao' => \G2\Constante\ItemConfiguracao::LINHA_DE_NEGOCIO
            ));

            $st_disciplina = null;
            if($configuracao instanceof \G2\Entity\VwEntidadeEsquemaConfiguracao && $configuracao->getSt_valor() == \G2\Constante\LinhaDeNegocio::POS_GRADUACAO){
                $st_disciplina = 'Prova Global';
            }

            $arrReturn = array();
            if ($result) {
                foreach ($result as $row) {
                    $arrReturn[] = array(
                        'st_exibe' => $row->getSt_nomecompleto(),
                        'dt_aplicacao' => ($row->getDt_aplicacao() ? date_format($row->getDt_aplicacao(), 'd/m/Y') : 'Sem Data'),
                        'id_avaliacaoagendamento' => $row->getId_avaliacaoagendamento(),
                        'st_aplicadorprova' => $row->getSt_aplicadorprova(),
                        'id_situacao' => $row->getId_situacao(),
                        'st_projetopedagogico' => $row->getSt_projetopedagogico(),
                        'st_disciplina' => ($st_disciplina == null) ? $row->getSt_disciplina() : $st_disciplina,
                        'st_horarioaula' => $row->getSt_horarioaula(),
                        'st_projetopedagogico' => $row->getSt_projetopedagogico(),
                        'st_avaliacao' => ($row->getId_tipodeavaliacao() == 0 ? $row->getSt_avaliacao() : $row->getSt_avaliacao() . ' ' . $row->getId_tipodeavaliacao()),
                        'id_tipodeavaliacao' => $row->getId_tipodeavaliacao(),
                        'bl_ativo' => $row->getBl_ativo(),
                        'nu_presenca' => $row->getNu_presenca(),
                        'st_usuariolancamento' => $row->getSt_usuariolancamento(),
                        'id_entidade' => $row->getId_entidade()
                    );
                }
                if ($this->getParam('print')) {
                    $this->view->dados = $arrReturn;
                    $this->imprimirFrequenciaAction();
                } else {
                    $this->_helper->json($arrReturn);
                }
            }
        }
    }

    public function pesquisaAgendamentoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $params['id_avaliacaoagendamento'] = $this->getParam('id_avaliacaoagendamento');
        $result = $this->negocio->findByAvaliacaoAgendamento($params);

        $this->getResponse()->appendBody(json_encode($result));
    }

    public function imprimirFrequenciaAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        if (!$this->view->dados) {
            $params = array();

            if ($this->getParam('id_aplicadorprova')) {
                $params['id_aplicadorprova'] = $this->getParam('id_aplicadorprova');
            }

            if ($this->getParam('dt_aplicacao')) {
                $data = str_replace('-', '/', $this->getParam('dt_aplicacao'));
                $dataEntrada = $this->negocio->converteDataBanco($data);
                $params['dt_aplicacao'] = $dataEntrada;
            }

            $configuracao = $this->negocio->findOneBy('\G2\Entity\VwEntidadeEsquemaConfiguracao', array(
                'id_entidade'         => $this->negocio->sessao->id_entidade,
                'id_itemconfiguracao' => \G2\Constante\ItemConfiguracao::LINHA_DE_NEGOCIO
            ));

            $st_disciplina = null;
            if($configuracao instanceof \G2\Entity\VwEntidadeEsquemaConfiguracao && $configuracao->getSt_valor() == \G2\Constante\LinhaDeNegocio::GRADUACAO){
                $st_disciplina = 'Prova Global';
            }

            if ($params) {
                $result = $this->negocio->findByVwAvaliacaoAgendamento($params);
                $arrReturn = array();
                if ($result) {
                    foreach ($result as $row) {
                        $arrReturn[] = array(
                            'st_exibe' => $row->getSt_nomecompleto(),
                            'dt_aplicacao' => ($row->getDt_aplicacao() ? date_format($row->getDt_aplicacao(), 'd/m/Y') : 'Sem Data'),
                            'id_avaliacaoagendamento' => $row->getId_avaliacaoagendamento(),
                            'st_aplicadorprova' => $row->getSt_aplicadorprova(),
                            'id_situacao' => $row->getId_situacao(),
                            'st_projetopedagogico' => $row->getSt_projetopedagogico(),
                            'st_disciplina' => ($st_disciplina == null) ? $row->getSt_disciplina() : $st_disciplina,
                            'st_horarioaula' => $row->getSt_horarioaula(),
                            'st_avaliacao' => ($row->getId_tipodeavaliacao() == 0 ? $row->getSt_avaliacao() : $row->getSt_avaliacao() . ' ' . $row->getId_tipodeavaliacao()),
                            'id_tipodeavaliacao' => $row->getId_tipodeavaliacao(),
                            'bl_ativo' => $row->getBl_ativo(),
                            'nu_presenca' => $row->getNu_presenca(),
                            'st_usuariolancamento' => $row->getSt_usuariolancamento()

                        );
                    }
                }
                $this->view->dados = $arrReturn;
            }

        }

        echo $this->view->render('lancar-frequencia/imprimir-frequencia.phtml');
    }


    public function retornaTramitesAction(){
        $tramites = $this->negocio->retornaHistoricoLancamentoFrequencia($this->getParam('id_avaliacaoagendamento'));
        if (is_array($tramites)) {
            $this->_helper->json($tramites);
        }
    }

}

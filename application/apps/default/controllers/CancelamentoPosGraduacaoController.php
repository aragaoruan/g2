<?php

class Default_CancelamentoPosGraduacaoController extends Ead1_ControllerRestrita
{

    /** @var  $negocio \G2\Negocio\CancelamentoPosGraduacao */
    private $negocio;

    /**
     * @var G2\Negocio\Matricula
     */
    private $matricula;
    /**
     * @var G2\Negocio\Cancelamento
     */
    private $cancelamento;

    public function init()
    {
        $this->negocio = new \G2\Negocio\CancelamentoPosGraduacao();
        parent::init();
        $this->matricula = new G2\Negocio\Matricula();
        $this->cancelamento = new G2\Negocio\Cancelamento();
    }

    public function indexAction()
    {

    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
    }


    public function retornarContratoMatriculaUsuarioAction()
    {
        $idUsuario = $this->getParam('id_usuario');
        $result = $this->negocio->retornarContratoMatriculaUsuario($idUsuario);
        $this->_helper->json($result);
    }

    public function retornarProcessosDeCancelamentoUsuarioAction()
    {
        $id_usuario = $this->getParam('id_usuario');
        $result = $this->negocio->retornarProcessosDeCancelamentoUsuario($id_usuario);
        $this->_helper->json($result);
    }

    public function retornarMateriaAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        try {
            $idMatricula = $this->getParam('id_matricula');

            if (!$idMatricula) {
                throw new Exception("Id da matrícula não informado");
            }

            $result = $this->matricula->retornarMaterias($idMatricula);

            if (!$result) {
                return $this->getResponse()->setHttpResponseCode(404)
                    ->appendBody("Nenhum resultado encontrado.");
            }

            $this->_helper->json($result);
        } catch (Exception $e) {
            return $this->getResponse()->setHttpResponseCode(500)
                ->appendBody($e->getMessage());
        }
    }


    public function retornarCalculoTotalPagoAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $matriculas = $this->getParam("matriculas");

        try {
            if (!$matriculas) {
                throw new Exception("Informe o id da matrícula.");
            }

            $result = $this->matricula->getRepository(\G2\Entity\VwVendaLancamento::class)
                ->getTotalLancamentosPagosPorMatricula($matriculas);
            if (!$result) {
                return $this->getResponse()->setHttpResponseCode(404)
                    ->appendBody("Nenhum resultado encontrado.");
            }
            $this->_helper->json($result);
        } catch (Exception $e) {
            return $this->getResponse()->setHttpResponseCode(500)
                ->appendBody($e->getMessage());
        }

    }

    public function retornarMotivosCancelamentoAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $id_linhadenegocio = $this->getParam('id_linhadenegocio');

        try {

            $result = $this->negocio->retornarMotivosCancelamento($id_linhadenegocio);

            if (!$result) {
                return $this->getResponse()->setHttpResponseCode(404)
                    ->appendBody("Nenhum resultado encontrado.");
            }

            $this->_helper->json($result);

        } catch (Exception $e) {
            return $this->getResponse()->setHttpResponseCode(500)
                ->appendBody($e->getMessage());
        }
    }

    public function pdfAction()
    {

        $idMatricula = $this->getParam('id_cancelamento');
        $dadosmatricula = array();
        $results = $this->negocio->retornarMatriculasPorCancelamento($idMatricula);
        foreach ($results as $result) {
            array_push($dadosmatricula, $result['id_matricula']);
        }
        $this->view->entidade = $this->matricula->sessao->id_entidade;
        $this->view->dadosUsuario = $this->matricula->toArrayEntity($this->matricula->consultaVwMatricula(
                array("id_matricula"=> $results[0]['id_matricula'])));

        $this->view->dadosMatricula = $this->matricula->retornarMaterias($dadosmatricula);
        $this->view->dadosCancelamento = $this->cancelamento->toArrayEntity($this->cancelamento
            ->retornarCancelamento(array("id_cancelamento" => $this->view->dadosUsuario[0]['id_cancelamento'])));

        $this->view->valorPago = $this->matricula->getRepository(\G2\Entity\VwVendaLancamento::class)
            ->getTotalLancamentosPagosPorMatricula($dadosmatricula);

        $this->view->dadosMotivoCancelamento = $this->cancelamento
            ->retornarMotivoCancelamento(array("id_cancelamento" => $this->view->dadosUsuario[0]['id_cancelamento']));

        $this->view->dadosRessarcimento = false;
        $dadosRessarcimento = $this->cancelamento
            ->retornaResacimentoCancelamento($this->view->dadosUsuario[0]['id_cancelamento']);
        if ($dadosRessarcimento) {
            $this->view->dadosRessarcimento = $this->cancelamento->toArrayEntity($dadosRessarcimento);
        }

        $ead1PDF = new Ead1_Pdf();
        $ead1PDF->carregarHTML(utf8_encode($this->view->render('cancelamento-pos-graduacao/pdf.phtml')))
            ->configuraPapel('a4', 'portrait');
        $ead1PDF->gerarPdf('Processo de Cancelamento' . ' ' . date('d_m_Y_H_i_s') . '.pdf');
        exit;
    }

    public function atualizaMatriculaDisciplinaAction()
    {

        $this->_helper->viewRenderer->setNoRender();
        $disciplinas = $this->getParam('disciplinas');
        try {
            if (!$disciplinas) {
                throw new Exception("Informe Disciplinas.");
            }
            $result = $this->matricula->atualizarMatriculaDisciplinaCancelamento($disciplinas);

            $retorno = [
                'type' => \Ead1_IMensageiro::SUCESSO,
                'tipo' => 'success',
                'title' => 'Sucesso',
                'mensagem' => 'Calculo cadastrado com sucesso!',
                'objeto' => $result
            ];
            $this->_helper->json($retorno);

        } catch (Exception $e) {
            return $this->getResponse()->setHttpResponseCode(500)
                ->appendBody($e->getMessage());
        }
    }


    public function retornarMotivosProcessoAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $id_cancelamento = $this->getParam('id_cancelamento');

        try {

            $result = $this->negocio->retornarMotivosProcesso($id_cancelamento);

            if (!$result) {
                return $this->getResponse()->setHttpResponseCode(404)
                    ->appendBody("Nenhum resultado encontrado.");
            }

            $this->_helper->json($result);

        } catch (Exception $e) {
            return $this->getResponse()->setHttpResponseCode(500)
                ->appendBody($e->getMessage());
        }

    }

    public function salvarMotivosProcessoAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        try {
            if ($this->getRequest()->isPost()) {
                $params = $this->getRequest()->getPost();
                $result = $this->negocio->salvarMotivosPorProcesso($params);
                return $this->getResponse()->setHttpResponseCode(201)
                    ->appendBody(json_encode($result));
            }
        } catch (\Exception $e) {
            return $this->getResponse()->setHttpResponseCode(500)
                ->appendBody($e->getMessage());
        }

    }

    public function retornaMatriculaAction()
    {
        try {
            $idCancelamento = $this->getParam('id_cancelamento');

            if (!$idCancelamento) {
                throw new Exception("Informe Disciplinas.");
            }

            $result = $this->matricula->retornaMatriculaCancelamento($idCancelamento);
            $this->_helper->json($result);
        } catch (Exception $e) {
            return $this->getResponse()->setHttpResponseCode(500)
                ->appendBody($e->getMessage());
        }
    }

    public function retornarMatriculasPorCancelamentoAction()
    {
        try {
            $this->_helper->viewRenderer->setNoRender();

            $id_cancelamento = $this->getParam('id_cancelamento');
            $result = $this->negocio->retornarMatriculasPorCancelamento($id_cancelamento);
            $this->_helper->json($result);

        } catch (\Exception $e) {
            return $this->getResponse()->setHttpResponseCode(500)
                ->appendBody($e->getMessage());
        }

    }

    public function salvarCancelamentoMatriculasAction()
    {
        $id_matriculas = $this->getParam('id_matriculas');
        $id_cancelamento = $this->getParam('id_cancelamento');

        $result = $this->negocio->salvarCancelamentoMatriculas($id_matriculas, $id_cancelamento);

        $this->_helper->json($result);

    }

    public function salvarLancamentosAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $data = $this->limpaGetAllParams($this->getAllParams());

        if (array_key_exists("lancamentosExcluidos", $data) &&
            array_key_exists("lancamentos", $data)) {

            //Realiza o cancelamento das parcelas e gera o acordo para salvar nas novas parcelas;
            $acordo = $this->negocio->cancelarLancamentosCancelamento(
                $data["lancamentosExcluidos"], $data["id_venda"], \Ead1\Financeiro\Fluxus\Integracao::ACORDO,
                count($data["lancamentos"]));

            if (count($data["id_matriculaorigem"]) > 1) {
                foreach ($data["id_matriculaorigem"] as $value) {
                    $retorno = $this->negocio->salvarLancamentos($data, $value, $acordo['retorno']['IDACORDO']);
                }
                $this->negocio->atualizarBlConcluir($data['cancelamento']);
                $this->negocio->sincronizarFluxo($data["id_venda"]);
                $this->_helper->json($retorno);
            }

            $retorno = $this->negocio->salvarLancamentos(
                $data, $data["id_matriculaorigem"][0], $acordo['retorno']['IDACORDO']
            );

        } elseif (array_key_exists("lancamentosExcluidos", $data) &&
            !array_key_exists("lancamentos", $data)) {

            $retorno = $this->negocio->cancelarLancamentosCancelamento(
                $data["lancamentosExcluidos"], $data["id_venda"], 2
            );

        } else {

            if (count($data["id_matriculaorigem"]) > 1) {
                foreach ($data["id_matriculaorigem"] as $value) {
                    $retorno = $this->negocio->salvarLancamentos($data, $value);
                }
                $this->negocio->atualizarBlConcluir($data['cancelamento']);
                $this->negocio->sincronizarFluxo($data["id_venda"]);
                $this->_helper->json($retorno);
            }

            $retorno = $this->negocio->salvarLancamentos(
                $data, $data["id_matriculaorigem"][0]
            );
        }

        //atualizar lancamentos excluidos com bl_cancelamento true
        if (array_key_exists("lancamentosExcluidos", $data)) {
            $this->negocio->atualizandoLancamentoCancelamento($data["lancamentosExcluidos"]);
        }
        $this->negocio->atualizarBlConcluir($data['cancelamento']);
        $this->negocio->sincronizarFluxo($data["id_venda"]);

        $this->_helper->json($retorno);
    }

    public function retornaLancamentosAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $data = $this->limpaGetAllParams($this->getAllParams());
        $retorno = $this->negocio->retornaLancamentos($data);
        $this->_helper->json($retorno);
    }

    /**
     * Função responsável por retornar o TID da transação.
     */
    public function retornaTidAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $retorno = $this->negocio->retornaTID($this->getParam("id_venda"));
        $this->_helper->json($retorno);
    }

    /**
     * Função responsável por salvar as interações na tabela tb_logcancelamento
     * Funcionamento: para usar esta  função é necessário passar como paramentro
     * um array com 3 parametros. São eles,
     * o id_matricula, id_cancelamento e a mensagem que desaja salvar.
     *
     * @throws Exception
     * @throws Zend_Exception
     */
    public function salvarInteracoesAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $negocioCancelamento = new \G2\Negocio\Cancelamento();
        $data = $this->limpaGetAllParams($this->getAllParams());
        $matricula = $this->negocio
            ->findOneBy(\G2\Entity\Matricula::class,
                array("id_matricula" => $data["params"]["matricula"])
            );
        $cancelamento = $this->negocio
            ->findOneBy(\G2\Entity\Cancelamento::class,
                array("id_cancelamento" => $data["params"]["cancelamento"])
            );
        $negocioCancelamento
            ->salvarLogCancelamento(
                $matricula, $cancelamento, $data["params"]["mensagem"]
            );
    }

    /**
     * Função responsável por alterar o processo de cancelamento, quando o aluno desistir
     *
     * @throws Zend_Exception
     */
    public function desistirCancelamentoAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        try {
            $data = $this->limpaGetAllParams($this->getAllParams());
            $cancelamentoNegocio = new \G2\Negocio\Cancelamento();

            if (is_array($data["id_matricula"])) {
                foreach ($data["id_matricula"] as $matricula) {
                    $this->negocio->atualizarDataDesistiu($data["id_cancelamento"]);
                    $mensageiro = $cancelamentoNegocio->alterarEvolucaoCancelamento(
                        $data["id_cancelamento"],
                        \G2\Constante\Evolucao::TB_MOTIVOCANCELANENTO_DESISTIU,
                        $matricula
                    );

                    if ($mensageiro->getType() !== Ead1_IMensageiro::TYPE_SUCESSO) {
                        throw new Exception($mensageiro->getText());
                    }
                }

                $this->_helper->json([
                    "type" => "success",
                    "title" => "Sucesso!",
                    "mensagem" => "Dados alterados com sucesso!"
                ]);
            }
        } catch (Exception $e) {
            return $this->getResponse()->setHttpResponseCode(500)
                ->appendBody($e->getMessage());
        }

    }

    /**
     * Função responsável por finalizar o processo de cancelamento
     *
     * @throws Zend_Exception
     */
    public function finalizarCancelamentoAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $data = $this->limpaGetAllParams($this->getAllParams());

        if (count($data["id_matricula"]) > 1) {
            foreach ($data["id_matricula"] as $value) {
                $retorno = $this->negocio->alterarEvolucoesCancelamento(
                    $data["id_cancelamento"],
                    $value,
                    \G2\Constante\Evolucao::TB_MATRICULA_CANCELADO,
                    \G2\Constante\Evolucao::TB_MOTIVOCANCELANENTO_FINALIZADO);
            }


            $this->_helper->json($retorno);
        }

        $result = $this->negocio->alterarEvolucoesCancelamento(
            $data["id_cancelamento"],
            $data["id_matricula"][0],
            \G2\Constante\Evolucao::TB_MATRICULA_CANCELADO,
            \G2\Constante\Evolucao::TB_MOTIVOCANCELANENTO_FINALIZADO);

        $this->_helper->json($result);
    }

    public function cancelarLancamentosCancelamentoAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $data = $this->limpaGetAllParams($this->getAllParams());
        $retorno = $this->negocio->cancelarLancamentosCancelamento($data);
        $this->_helper->json($retorno);
    }

    public function atualizarDataReferenciaAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $data = $this->limpaGetAllParams($this->getAllParams());
        $this->negocio->atualizarDataReferencia($data);
    }

}

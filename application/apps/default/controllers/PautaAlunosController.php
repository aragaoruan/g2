<?php
use G2\Negocio\PautaAlunos;

/**
 * Controller para relatorio de Pauta de aluno
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @since  16-10-2015
 * @package application
 * @subpackage controller
 */
class Default_PautaAlunosController extends Ead1_ControllerRestrita
{

    /**
     * @var \G2\Negocio\PautaAlunos
     */
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\PautaAlunos();
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('pauta-alunos/index.js');
    }

    public function indexAction()
    {

    }

    public function findPeriodoLetivoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $results = $this->negocio->findComboPeriodoLetivo();
        $return = [];
        if ($results) {
            foreach ($results as $key => $result) {
                $return[$key] = $this->negocio->toArrayEntity($result);
            }
        }

        $this->_helper->json($return);
    }

    public function findDisciplinaByPeriodoLetivoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $results = $this->negocio->findDisciplinaByPeriodoLetivo($this->getParam('id_periodoletivo'));
        $this->_helper->json($results);
    }

    public function findCursoByDisciplinaAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $results = $this->negocio->findCursoByDisciplina($this->getParam('id_disciplina'));
        $this->_helper->json($results);
    }

    public function pesquisaAlunosAction()
    {
        $this->view->id_entidade = $this->negocio->sessao->id_entidade;
        $this->view->st_nomeentidade = $this->getParam('st_nomeentidade', null);
        $this->view->dados = $this->negocio->pesquisaAlunosPauta($this->getAllParams());

    }

    public function gerarPdfAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        try {
            if (!$this->negocio->validaDadosPdf($this->getAllParams())) {
                $this->view->pdf = true;
                $this->view->st_nomeentidade = $this->getParam('st_nomeentidade', null);
                $this->view->id_entidade = $this->negocio->sessao->id_entidade;
                $this->view->dataPdf = $this->getParam('data');
                $this->view->dados = $this->negocio->pesquisaAlunosPauta($this->getAllParams());

                $arrayTipo = [];
                if ($this->getParam('bl_avaliacao') === 'true') {
                    $arrayTipo[] = utf8_encode('AVALIA��O');
                }
                if ($this->getParam('bl_presencial') === 'true') {
                    $arrayTipo[] = 'ENCONTRO PRESENCIAL';
                }
                $this->view->tipo = $arrayTipo;
                $ead1PDF = new Ead1_Pdf();
                $ead1PDF->carregarHTML(utf8_encode($this->view->render('pauta-alunos/pdf.phtml')))->configuraPapel('a4', 'portrait');
                $ead1PDF->gerarPdf('Pauta Alunos' . ' ' . date('d_m_Y_H_i_s') . '.pdf');

            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }


}
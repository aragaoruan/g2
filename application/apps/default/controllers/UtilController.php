<?php

/**
 * Classe para utilitários como busca de cidades por exemplo
 * @author Elcio Mauro Guimarães
 *
 * @package application
 * @subpackage controller
 */
class Default_UtilController extends Ead1_ControllerRestrita
{

    /**
     * @var \G2\Negocio\VwEntidadeRecursiva
     */
    private $negocio;

    /**
     *
     */
    public function init()
    {
        parent::init();
        $this->setupDoctrine();
        $this->negocio = new \G2\Negocio\VwEntidadeRecursiva();
    }

    /**
     *
     */
    public function retornaMunicipiosJsonAction()
    {
        $muncipios = array();

        try {

            $sg_uf = $this->_getParam('sg_uf', false);
            if (!$sg_uf) {
                $sg_uf = $this->_getParam('entrega_sg_uf', false);
            }
            if (!$sg_uf) {
                throw new Zend_Exception("Informe a UF");
            }

            $to = new MunicipioTO();
            $to->setSg_uf($sg_uf);

//            $mensageiro = Ead1_Util::retornarMunicipios($to);
            $ead1 = new Ead1_Util();
            $mensageiro = $ead1->retornarMunicipios($to);
//            Zend_Debug::dump($mensageiro);die;

            if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
                foreach ($mensageiro->getMensagem() as $muTO) {
                    $muncipios[] = array($muTO->getId_municipio(), $muTO->getSt_nomemunicipio());
                }
            }
        } catch (Exception $e) {
            $muncipios = false;
        }

        die($this->_helper->json(new Ead1_Mensageiro($muncipios, Ead1_IMensageiro::SUCESSO)));
    }

    /**
     * Monta as arvores da entidade buscando pela entidade da sessão
     */
    public function arvoreEntidadeAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $entidadeRecursiva = new \G2\Negocio\VwEntidadeRecursiva();
        $result = $entidadeRecursiva->findByCadeiaEntidades(array('cadeiaentidades' => $entidadeRecursiva->sessao->id_entidade));

        if ($result instanceof \Ead1_Mensageiro) {
            die('');
        }

        $retorno = '';
//        if (!empty($result)) {
        if ($result) {
            foreach ($result as $key => $linha) {
                $retorno = '<li id="' . $linha['to']->getId_entidade() . '">';
                $retorno .= '<a href="#" id="' . $linha['to']->getId_entidade() . '">' . $linha['to']->getSt_nomeentidade() . '</a>';
                $retorno .= '<ul>';
                $retorno .= $this->retornaFilha($linha);
                $retorno .= '<ul>';
                $retorno .= '</li>';
            }
        }
        echo $retorno;
    }

    /**
     * Retorna entidades filhas recursivamente
     * @param $eTO
     * @return null|string
     */
    private function retornaFilha($eTO)
    {
        $string = null;
        if (is_array($eTO) && !empty($eTO)) {
            foreach ($eTO as $index => $linhas) {
                if ($index == 'to') {
                    $toAux = $linhas;
                }
                if ($index == 'children') {
                    if ($linhas != null) {
                        foreach ($linhas as $linha) {
                            if (($linha['to']->getId_entidadepai() == $toAux->getId_entidade())) {
                                $string .= '<li id="' . $linha['to']->getId_entidade() . '">
                                <a href="#" id="' . $linha['to']->getId_entidade() . '">' . $linha['to']->getSt_nomeentidade() . '</a>';
                                if ($linha['children'] != null) {
                                    $string .= '<ul>';
                                    $string .= $this->retornaFilha($linha);
                                    $string .= '</ul>';
                                }
                                $string .= '</li>';
                            }
                        }
                    }
                }
            }
        }
        return (is_null($string) ? null : $string);
    }

    /**
     *
     */
    public function getAreaProjetoPedagogicoAction()
    {
        $result = $this->negocio->findBy('G2\Entity\VwProjetoEntidade', array('id_entidade' => $this->negocio->sessao->id_entidade), array('st_projetopedagogico' => 'ASC'));
        $this->view->projetos = $result;
    }

    /**
     * Componente de auto complete de pessoa
     */
    public function autoCompletePessoaAction()
    {
        // $this->_helper->layout->enableLayout();
    }

    /*
     * Método para chamada de modal referente a pesquisa de disciplina
     * (atualmente utilizada no Gerencia Salas)
     * @autor: Débora Castro
     * */

    /**
     *
     */
    public function modalPesquisaDisciplinaAction()
    {
        $form2 = new PesquisaDisciplinaForm();
        $this->view->form2 = $form2;
    }

    /*
     * Método que filtra os aplicadores de prova por UF e/ou Município
     */

    /**
     *
     */
    public function filtraAplicadorAction()
    {
        $negocio = new \G2\Negocio\AplicacaoAvaliacao();
        $this->_helper->viewRenderer->setNoRender(true);
        $params = array();
        if ($this->getParam('sg_uf')) {
            $params['sg_uf'] = $this->getParam('sg_uf');
        }
        if ($this->getParam('id_municipio')) {
            $params['id_municipio'] = $this->getParam('id_municipio');
        }
        if ($this->hasParam('bl_ativo')) {
            $params['bl_ativo'] = $this->getParam('bl_ativo');
        }
        $result = $negocio->findEnderecoAplicador($params);
        $arrReturn = array();
        if ($result) {
            foreach ($result as $row) {
                $arrReturn[] = array(
                    'id_value' => $row->getId_aplicadorprova(),
                    'st_text'  => $row->getSt_aplicadorprova()
                );
            }
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    /*
     * Método para carregar combos de UF
     */

    /**
     *
     */
    public function buscaUfAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $bo = new PesquisarBO();
        $uf = $bo->retornarSelectUf();
        $arrReturn = array();
        if ($uf) {
            foreach ($uf as $value) {
                $arrReturn[] = array(
                    'id_value' => $value['valor'],
                    'st_text'  => $value['label']
                );
            }
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    /**
     * Método para carregar combos de municípios com aplicações ativas
     * (agendamento de prova)
     */
    public function retornaMunicipioAplicacaoAction()
    {
        $negocio = new \G2\Negocio\GerenciaProva();
        $this->_helper->viewRenderer->setNoRender(true);

        $params = null;
        if ($this->getParam('idParam')) {
            $params['sg_uf'] = $this->getParam('idParam');
        }
        $result = $negocio->findByVwMunicipioAplicacao($params);
        $arrReturn = array();
        if ($result) {
            foreach ($result as $value) {
                $arrReturn[] = array(
                    'id_value' => $value->getId_municipio(),
                    'st_text'  => $value->getSt_nomemunicipio()
                );
            }
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    /**
     * Metodo responsavel por verificar a permissao do usuario logado
     * em uma determinada funcionalidade, esta informacao e buscada
     * na tabela tb_perfilpermissaofuncionalidade
     *
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     * @param array request
     */
    public function perfilPermissaoFuncionalidadeAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $this->negocio = new \G2\Negocio\Negocio();
        $result = array();

        $where = array(
            'id_perfil'         => $this->negocio->sessao->id_perfil,
            'id_funcionalidade' => $this->getRequest()->getParam('id_funcionalidade'),
            'id_permissao'      => $this->getRequest()->getParam('id_permissao'));

        $entity = $this->negocio->find('\G2\Entity\PerfilPermissaoFuncionalidade', $where);

        if ($entity) {
            array_push($result, $this->negocio->toArrayEntity($entity));
        }

        $this->_helper->json($result);
    }

    /*
    * Método para carregar combos de UF (entity)
    */

    /**
     *
     */
    public function retornaUfAction()
    {
        $negocio = new \G2\Negocio\Negocio();
        $arrReturn = array();
        $result = $negocio->findAll('G2\Entity\Uf');
        if ($result) {
            foreach ($result as $row) {
                $arrReturn[] = array(
                    'sg_uf' => $row->getSg_uf(),
                    'st_uf' => $row->getSt_uf()
                );
            }
        }

        $this->_helper->json($arrReturn);
    }

    public function retornaMunicipioAction()
    {
        $response = array();

        $sg_uf = $this->getParam('sg_uf');

        if ($sg_uf) {
            $negocio = new \G2\Negocio\Negocio();
            $results = $negocio->findBy('G2\Entity\Municipio', array('sg_uf' => $sg_uf));
            if ($results) {
                foreach ($results as $result) {
                    $response[] = array(
                        'id_municipio' => $result->getId_municipio(),
                        'st_nomemunicipio' => $result->getSt_nomemunicipio()
                    );
                }
            }
        }

        $this->_helper->json($response);
    }

    /**
     *
     */
    public function retornaMunicipioAgendamentoAction()
    {
        $negocio = new \G2\Negocio\GerenciaProva();
        $this->_helper->viewRenderer->setNoRender(true);

        $result = $negocio->findByVwMunicipioAplicacao(array('sg_uf' => $this->getParam('sg_uf')));
        $arrReturn = array();
        if ($result) {
            foreach ($result as $value) {
                $arrReturn[] = array(
                    'id_municipio' => $value->getId_municipio(),
                    'st_municipio' => $value->getSt_nomemunicipio()
                );
            }
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    /*
    * Método que filtra os aplicadores de prova por UF e/ou Município
    */

    /**
     *
     */
    public function retornaAplicadorAgendamentoAction()
    {
        $negocio = new \G2\Negocio\AplicacaoAvaliacao();
        $this->_helper->viewRenderer->setNoRender(true);
        $params = array('bl_ativo' => true);

        if ($this->getParam('sg_uf')) {
            $params['sg_uf'] = $this->getParam('sg_uf');
        }
        if ($this->getParam('id_municipio')) {
            $params['id_municipio'] = $this->getParam('id_municipio');
        }
        $result = $negocio->findEnderecoAplicador($params);
        $arrReturn = array();
        if ($result) {
            foreach ($result as $row) {
                $arrReturn[] = array(
                    'id_aplicadorprova' => $row->getId_aplicadorprova(),
                    'st_aplicadorprova' => $row->getSt_aplicadorprova(),
                    'bl_ativo'          => $row->getBl_ativo()
                );
            }
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }


    /**
     * @description Função responsável por gerar entidades, com opção de ser minificado ou não.
     * @return bool
     */
    public function gerarJsEntidadesAction()
    {
        $devMode = $this->getParam('dev');

        if ($this->getRequest()->getModuleName() == 'default') {
            $dirEntity = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . "library" . DIRECTORY_SEPARATOR . "G2" . DIRECTORY_SEPARATOR . "Entity";
            $dirBuildJS = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . "public" . DIRECTORY_SEPARATOR . "gestor2" . DIRECTORY_SEPARATOR . "build" . DIRECTORY_SEPARATOR . "js";

            if ($handle = opendir($dirEntity)) {
                $arquivoGeralJS = $dirBuildJS . DIRECTORY_SEPARATOR . 'all_entities.js';

                $handle = opendir($dirEntity);

                $this->view->dev = false;
                $content = '';
                if ($devMode == 1) {
                    $this->view->dev = true;
                    $content = "/*\n"
                        . "============================================\n"
                        . " ARQUIVO GERADO AUTOMATICAMENTE, NÃO EDITAR\n"
                        . " RESPONSÁVEL POR AGRUPAR TODAS AS MODELOS\n"
                        . " cache gerado em: " . date("d/m/Y H:i:s") . "\n"
                        . "============================================\n"
                        . "*/\n\n";
                }

                while (false !== ($file = readdir($handle))) {
                    if ($file != "." && $file != ".." && !is_dir($dirEntity . DIRECTORY_SEPARATOR . $file)) {
                        $baseName = basename($dirEntity . DIRECTORY_SEPARATOR . $file, ".php");

                        $repoEntity = "G2\\Entity\\" . $baseName;
                        //Recupera os atributos da entity
                        $metas = $this->em->getClassMetadata($repoEntity);

                        if ($devMode == 1) {
                            $content .= "var " . $baseName . " = Backbone.Model.extend({\n"
                                . "defaults: {\n";
                        } else {
                            $content .= "var " . $baseName . " = Backbone.Model.extend({defaults: {";
                        }

                        if ($metas->reflFields) {
                            $endArray = array_keys($metas->reflFields);
                            foreach ($metas->reflFields as $key => $attr) {
                                $comma = ',';

                                if ($key === end($endArray))
                                    $comma = '';

                                if ($devMode == 1) {
                                    $content .= "\t" . $key . ": ''" . $comma . "\n";
                                } else {
                                    $content .= $key . ": ''" . $comma . "";
                                }
                            }
                        }

                        if ($devMode == 1) {
                            $content .= "},\n";
                            $content .= "url: function() {\n";
                            $content .= "\treturn this.id ? '/api/" . $this->camelToDashed($baseName) . "/' + this.id : '/api/" . $this->camelToDashed($baseName) . "';\n";
                            $content .= "}\n";
                            $content .= "});\n";
                        } else {
                            $content .= "},url: function() {return this.id ? '/api/" . $this->camelToDashed($baseName) . "/' + this.id : '/api/" . $this->camelToDashed($baseName) . "';}});";
                        }
                    }
                }

                if (file_put_contents($arquivoGeralJS, $content)) {
                    closedir($handle);
                    $this->view->retorno = true;
                } else {
                    $this->view->retorno = false;
                }
            }
        }
        return true;
    }

    /**
     * @param $string
     * @return string
     */
    public function camelToDashed($string)
    {
        return strtolower(preg_replace('/([a-zA-Z])(?=[A-Z])/', '$1-', $string));
    }


    public function atualizarMensagemSessaoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->getRequest()->getParams();
        $negocio_mensagem = new \G2\Negocio\MensagemUsuario();
        $retorno = $negocio_mensagem->retonarMensagensUsuario(array('id_evolucaodestinatario' => 42));
        $nu_mensagem = sizeof($retorno);
        $resultado = array('nu_mensagens' => $nu_mensagem);
        $this->getResponse()->appendBody(json_encode($resultado));

//        Zend_Debug::dump($nu_mensagem);
//        Zend_Debug::dump($retorno);die;
    }


    public function retornaPaisAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $negocio = new \G2\Negocio\AplicacaoAvaliacao();

        $mensageiro = $negocio->findAll('\G2\Entity\Pais');
        $results = array();
        foreach ($mensageiro as $entity) {
            $results[] = $negocio->toArrayEntity($entity);
        }

        if (!$results) {
            $results = new \Ead1_Mensageiro();
            $results->setMensageiro('Nenhum pais encontrado', \Ead1_IMensageiro::AVISO, null);
        }

        $this->_helper->json($results);
    }


    /**
     * @deprecated
     */
    public function tipoSelecaoAction()
    {
        $ts = new \G2\Entity\TipoSelecao();

//        array $array, $order = null, $limit = null, $offset = 0, $returnEntity = true

        $result = $ts->findBy(array(), array('st_tiposelecao' => 'ASC'), null, 0, false);
        $this->_helper->json($result);
    }

    public function getBancoAction()
    {
        $ngBanco = new \G2\Negocio\Banco();
        $response = array();
        $results = $ngBanco->findByBanco();
        if ($results) {
            foreach ($results as $item) {
                $response[] = array(
                    'id_banco' => $item->getId_banco(),
                    'st_nomebanco' => $item->getSt_banco() . " - " . $item->getSt_nomebanco()
                );
            }
        }

        return $this->_helper->json($response);
    }

}

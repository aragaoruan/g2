<?php

/**
 * Classe de controller MVC para controlar RegraPagamento
 * @author Rafael Rocha <rafael.rocha.mg@gmail.com>
 * 
 * @package application
 * @subpackage controller
 */
class Default_RegraPagamentoController extends Ead1_ControllerRestrita {

    private $negocio;

    public function init() {
        parent::init();
        parent::setupDoctrine();
        $this->negocio = new \G2\Negocio\RegraPagamento();
        $this->_helper->getHelper('viewRenderer')->setNoRender();
    }

    public function jsAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('regra-pagamento/index.js');
    }

    public function indexAction() {
        echo $this->view->headScript()->appendFile('/regra-pagamento/js');
    }

    public function retornarRegraEntidadeAction() {

        $vws = $this->negocio->findByEntidade($this->getRequest()->getQuery());

        $retorno = array();

        foreach ($vws as $vw) {
            $retorno[] = $this->negocio->toArrayEntity($vw);
        }
        echo json_encode($retorno);
    }

    public function retornarRegraAreaConhecimentoAction() {

        $vws = $this->negocio->findByAreaConhecimento($this->getRequest()->getQuery());

        $retorno = array();

        foreach ($vws as $vw) {
            $retorno[] = $this->negocio->toArrayEntity($vw);
        }
        echo json_encode($retorno);
    }

    public function retornarRegraProjetoPedagogicoAction() {
        $vws = $this->negocio->findByProjetoPedagogico($this->getRequest()->getQuery());

        $retorno = array();

        foreach ($vws as $vw) {
            $retorno[] = $this->negocio->toArrayEntity($vw);
        }
        echo json_encode($retorno);
    }

    public function retornarRegraProfessorAction() {

        $vws = $this->negocio->findByProfessor($this->getRequest()->getQuery());

        $retorno = array();

        foreach ($vws as $vw) {
            $retorno[] = $this->negocio->toArrayEntity($vw);
        }
        echo json_encode($retorno);
    }

    public function retornarRegraProfessorProjetoAction() {

        $vws = $this->negocio->findByProfessorProjeto($this->getRequest()->getQuery());

        $retorno = array();

        foreach ($vws as $vw) {
            $retorno[] = $this->negocio->toArrayEntity($vw);
        }
        echo json_encode($retorno);
    }

}

<?php

/**
 * Class FormaPagamentoController
 * Controller para Forma de Pagamento
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-08-21
 * @package application
 * @subpackage controller
 */

use G2\Negocio\FormaPagamento;

class Default_FormaPagamentoController extends Ead1_ControllerRestrita
{

    /**
     * @var G2\Negocio\FormaPagamento
     */
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new FormaPagamento();
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('forma-pagamento/index.js');
    }

    public function indexAction()
    {
    }

    public function getProdutosAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        $bo = new ProdutoBO();
        $to = new VwProdutoTO();

        $to->setId_entidade($this->negocio->sessao->id_entidade);

        $mensageiro = $bo->retornarVwProduto($to);

        $this->getResponse()->appendBody(json_encode($mensageiro->mensagem));
    }

    public function getAplicacaoAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        $negocio = new \G2\Negocio\Negocio();
        $results = $negocio->findBy('G2\Entity\FormaPagamentoAplicacao', array());

        $mensageiro = array();
        foreach($results as $key => $result) {
            $mensageiro[] = $negocio->toArrayEntity($result);
        }

        array_unshift($mensageiro, array(
            'id_formapagamentoaplicacao' => '',
            'st_formapagamentoaplicacao' => '[Selecione]'
        ));

        $this->getResponse()->appendBody(json_encode($mensageiro));
    }

    public function getParcelaAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        $id = $this->getParam('id_formapagamento');


        $negocio = new \G2\Negocio\Negocio();
        $results = $negocio->findBy('G2\Entity\FormaPagamentoParcela', array(
            'id_formapagamento' => $id
        ));

        $mensageiro = array();
        foreach ($results as $result) {
            $indexes = $negocio->toArrayEntity($result);
            $indexes['id_meiopagamento'] = array(
                'id_meiopagamento' => $result->getId_meiopagamento()->getId_meiopagamento(),
                'st_meiopagamento' => $result->getId_meiopagamento()->getSt_meiopagamento()
            );
            $mensageiro[] = $indexes;
        }

        $this->getResponse()->appendBody(json_encode($mensageiro));
    }

    public function retornarFormaPagamentoPorAplicacaoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $result = $this->negocio->retornarFormaPagamentoPorAplicacao();
        $return = array();
        if ($result->getType() == Ead1_IMensageiro::TYPE_SUCESSO) {
            $return = $result->getMensagem();
        }
        unset($result);
        $this->json($return);

    }
}
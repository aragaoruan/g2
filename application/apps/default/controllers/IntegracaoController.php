<?php

/**
 * Classe de controller MVC para controlar a Integração
 * @author Ilson Nóbrega
 *
 * @package default
 * @subpackage controller
 */
class Default_IntegracaoController extends Ead1_ControllerRestrita
{
    /** @var  G2\Negocio\IntegracaoNegocio */
    private $negocio;

    public function init()
    {
        parent::init();
        parent::setupDoctrine();
        $this->negocio = new G2\Negocio\IntegracaoNegocio();
    }

    public function indexAction()
    {

    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
    }

    public function sincronizarMoodleAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        $return = $this->negocio->acaoSincronizar($this->getParam('id_entidadeintegracao'));
        $this->getResponse()->appendBody(($return));
    }

    public function sincronizarAlunosMoodleAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $return = $this->negocio->acaoSincronizarAlunos();
        $this->getResponse()->appendBody(($return));
    }

    public function dadosMoodleAction()
    {
        $buscarMoodle = $this->negocio->buscaDadosMoodle();
        $this->_helper->json($buscarMoodle);
    }

    public function salvarDadosMoodleAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $dadosMoodle = Zend_Json::decode($this->getRequest()->getRawBody());
        $salvarMoodle = $this->negocio->salvarDadosMoodle($dadosMoodle);
        $this->getResponse()->appendBody(json_encode($salvarMoodle));

    }

    public function dadosPagamentoTotvsAction()
    {
        $buscarTotvs = $this->negocio->buscaEntidadeIntegracaoTotvs();
        $this->_helper->json($buscarTotvs);
    }

    public function salvarMeioPagamentoIntegracaoAction()
    {
        $dadosFinanceiros = Zend_Json::decode($this->getParam('dados_financeiros'));
        $meioPagamento = Zend_Json::decode($this->getParam('meio_pagamento'));
        $salvarTotvs = $this->negocio->salvarDadosCompletosTotvs($dadosFinanceiros, $meioPagamento);
        $this->_helper->json($salvarTotvs);
    }

    public function integrarSalasAction()
    {
        $this->ajax();
        $x = 0;
        $to = new EntidadeTO();
        $bo = new IntegracaoBO();
        $bo->debug = $this->_getParam('bl_debug', false);
        $retorno = $bo->integrarSalas($to->getSessao()->id_entidade, \G2\Constante\Sistema::BLACKBOARD);
        echo "<fieldset><legend>Integrando as Salas de Aula</legend>";
        foreach ($retorno->getMensagem() as $mensagem) {
            echo (++$x) . ' - ' . $mensagem . '<br>';
        }
        echo "</fieldset>";

    }


    public function verificarSalasAction()
    {
        $this->ajax();
        $x = 0;
        $to = new EntidadeTO();
        $bo = new IntegracaoBO();
        $bo->debug = $this->_getParam('bl_debug', false);
        $retorno = $bo->verificarSalas($to->getSessao()->id_entidade, 15);
        echo "<fieldset><legend>Verificando as Salas de Aula</legend>";
        foreach ($retorno->getMensagem() as $mensagem) {
            echo (++$x) . ' - ' . $mensagem . '<br>';
        }
        echo "</fieldset>";

    }

    public function integrarAlunosAction()
    {


        $this->ajax();
        $x = 0;
        $to = new EntidadeTO();
        $bo = new IntegracaoBO();
        $bo->debug = $this->_getParam('bl_debug', false);
        $retorno = $bo->integrarAlunos($to->getSessao()->id_entidade, 15);
        $qtd = 0;
        if ($retorno->getTipo() == \Ead1_IMensageiro::SUCESSO) {
            $qtd = count($retorno->getMensagem());
        }
        echo "<fieldset><legend>Integrando " . $qtd . " Aluno(s) Pendente(s), limite de 20. Para reintegrar, vá até o cadastro da Sala de Aula.</legend>";
        foreach ($retorno->getMensagem() as $mensagem) {
            echo (++$x) . ' - ' . $mensagem . '<br>';
        }
        echo "</fieldset>";


    }

    public function integrarObservadorAction()
    {

        $this->ajax();
        $x = 0;
        $to = new EntidadeTO();
        $bo = new IntegracaoBO();
        $bo->debug = $this->_getParam('bl_debug', false);
        $retorno = $bo->integrarObservador($to->getSessao()->id_entidade, 15);
        echo "<fieldset><legend>Integrando " . count($retorno->getMensagem()) . " Observador(es).</legend>";
        foreach ($retorno->getMensagem() as $mensagem) {
            echo (++$x) . ' - ' . $mensagem . '<br>';
        }
        echo "</fieldset>";


    }

    public function integrarProfessoresAction()
    {

        $this->ajax();
        $x = 0;
        $to = new EntidadeTO();
        $bo = new IntegracaoBO();
        $bo->debug = $this->_getParam('bl_debug', false);
        $retorno = $bo->integrarProfessores($to->getSessao()->id_entidade, 15);
        echo "<fieldset><legend>Integrando " . count($retorno->getMensagem()) . " Professor(es) Pendente(s), limite de 20. Para reintegrar, vá até o cadastro da Sala de Aula.</legend>";
        foreach ($retorno->getMensagem() as $mensagem) {
            echo (++$x) . ' - ' . $mensagem . '<br>';
        }
        echo "</fieldset>";


    }

    public function integrarEntidadeAction()
    {
        $this->ajax();
        try {
            $bo = new IntegracaoBO();
            $id_entidade = $this->getParam('id_entidade', false);
            $id_sistema = $this->getParam('id_sistema', false);
            $retorno = $bo->integrarEntidade($id_entidade, $id_sistema);
            if ($retorno->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                echo $retorno->getFirstMensagem();
            } else {
                $this->getResponse()->setHttpResponseCode(400);
                echo $retorno->getFirstMensagem();
            }
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            echo $e->getMessage();
        }

    }
}

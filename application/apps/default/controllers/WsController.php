<?php

/**
 * Classe de WebService onde aplicações irão consumir o serviço que esta classe provê
 * @author edermariano
 *
 */
class Default_WsController extends Ead1_Controller
{

    public function init()
    {

        parent::init();

        ini_set('allow_url_fopen', 1);
        ini_set('display_errors', 0);

        $id = $this->_getParam('id', false);

        /*bloco necessario para tratar o json enviado pela request body->raw
            O bloco seta os parametros que são acessiveis depois recuperando a variável superglobal $_REQUEST['parans']
        */
        if(empty($id)) {
            $postData = file_get_contents('php://input');
            $json = json_decode($postData);

            if(!empty($json)){
                $this->setParam('id', $json->id);
                $this->setParam('chave', $json->chave);
                $this->setParam('method', $json->method);
                $id = $this->_getParam('id');
                $_REQUEST['method'] = $json->method;
                $_REQUEST['params'] =  $json;
            }
        }

        if ($id) {
            $bo = new WebServiceBO();
            if (!$bo->autenticar($this->_getParam('chave'), $this->_getParam('id'))) {

                die('<?xml version="1.0" encoding="UTF-8"?>
                                                <' . $this->_getParam('action') . ' generator="zend" version="1.0">
						<' . $this->_getParam('method') . '>
						<mensagem>
						<key_0>Acesso Negado! ' . $bo->codigo . '</key_0>
						</mensagem>
						<tipo>0</tipo>
						<codigo>Exception</codigo>
						<status>success</status>
						</' . $this->_getParam('method') . '></' . $this->_getParam('action') . '>');
            }
        } else {
            die('<?xml version="1.0" encoding="UTF-8"?>
					<' . $this->_getParam('action') . ' generator="zend" version="1.0">
					<' . $this->_getParam('method') . '>
					<mensagem>
					<key_0>Acesso Negado! Id não informado!</key_0>
					</mensagem>
					<dynMensagem></dynMensagem>
					<tipo>0</tipo>
					<codigo>Exception</codigo>
					<logado></logado>
					<su></su>
					<status>success</status>
					</' . $this->_getParam('method') . '></' . $this->_getParam('action') . '>');
        }
        $this->ajax();
    }

    /**
     * Método do Server do Rest
     *
     */
    public function cursoAction()
    {

        $rest = new Zend_Rest_Server();
        $rest->setClass('CursoWS');
        $rest->handle();
    }

    /**
     * Método do Server do Rest
     *
     */
    public function acessoafiliadoAction()
    {

        $rest = new Zend_Rest_Server();
        $rest->setClass('AcessoAfiliadoWS');
        $rest->handle();
    }

    /**
     * Método do Server do Rest
     *
     */
    public function pessoaAction()
    {
        $rest = new Zend_Rest_Server();
        $rest->setClass('PessoaWS');
        $rest->handle();
    }

    /**
     * Método do Server do Rest
     *
     */
    public function vendaAction()
    {
        $rest = new Zend_Rest_Server();
        $rest->setClass('VendaWS');
        $rest->handle();
    }

    /**
     * Método do Server do Rest Entidade
     *
     */
    public function entidadeAction()
    {
        $rest = new Zend_Rest_Server();
        $rest->setClass('EntidadeWS');
        $rest->handle();
    }

    /**
     * @author ?
     */
    public function entregaDocumentoAction()
    {
        $rest = new Zend_Rest_Server();
        $rest->setClass('EntregaDocumentoWS');
        $rest->handle();
    }

    /**
     * Método do Server do Rest
     *
     */
    public function utilAction()
    {

        $rest = new Zend_Rest_Server();
        $rest->setClass('UtilWS');
        $rest->handle();
    }

    /**
     * Método do Server do Produto
     *
     */
    public function produtoAction()
    {

        $rest = new Zend_Rest_Server();
        $rest->setClass('ProdutoWS');
        $rest->handle();
    }

    /**
     * Método do server de disciplinas
     */
    public function disciplinasAction()
    {
        $rest = new Zend_Rest_Server();
        $rest->setClass('DisciplinasWS');
        $rest->handle();
    }

    /**
     * Método do server de Concursos
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     * @since 2014-01-20
     */
    public function concursosAction()
    {
        $rest = new Zend_Rest_Server();
        $rest->setClass('ConcursosWS');
        $rest->handle();
    }

    /**
     * Método do server de Combo
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     * @since 2014-07-02
     */
    public function comboAction()
    {
        $rest = new Zend_Rest_Server();
        $rest->setClass('ComboWS');
        $rest->handle();
    }

    /**
     * Método do server de Tags
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     * @since 2014-01-16
     */
    public function tagsAction()
    {
        $rest = new Zend_Rest_Server();
        $rest->setClass('TagsWS');
        $rest->handle();
    }

    /**
     * Método do server para Carreiras
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     * @since 2014-01-17
     */
    public function carreirasAction()
    {
        $rest = new Zend_Rest_Server();
        $rest->setClass('CarreirasWS');
        $rest->handle();
    }

    /**
     * Método do server para Categorias
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     * @since 2014-01-20
     */
    public function categoriasAction()
    {
        $rest = new Zend_Rest_Server();
        $rest->setClass('CategoriasWS');
        $rest->handle();
    }

    /**
     * Método para serviço de Professor
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     * @since 2014-04-02
     */
    public function professorAction()
    {
        $rest = new Zend_Rest_Server();
        $rest->setClass('ProfessorWS');
        $rest->handle();
    }

    /**
     * Método para serviço de Cupom
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     * @since 2014-03-21
     */
    public function cupomAction()
    {
        $rest = new Zend_Rest_Server();
        $rest->setClass('CupomWS');
        $rest->handle();
    }

    /**
     * Metodo para Serviço de Matricula
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     * @since 2014-06-05
     */
    public function matriculaAction()
    {
        $rest = new Zend_Rest_Server();
        $rest->setClass("MatriculaWS");
        $rest->handle();

    }

    /**
     * Metodo para Serviço do Bot
     * @author Arthur Luiz Lara Quites
     * @since 26/12/2017
     */
    public function botAction()
    {
        $rest = new Zend_Rest_Server();
        $rest->setClass("BotWS");
        $rest->handle();
    }


    /**
     * Método para Serviço de Grade Horaria
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     * @since 2015-02-20
     */
    public function gradehorariaAction()
    {
        $rest = new Zend_Rest_Server();
        $rest->setClass("GradeHorariaWS");
        $rest->handle();
    }

    /**
     * Metodo para Serviço de Campanha Premio
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     * @since 2015-07-08
     */
    public function campanhapremioAction()
    {
        $rest = new Zend_Rest_Server();
        $rest->setClass("CampanhaPremioWS");
        $rest->handle();

    }

    /**
     * Metodo para Serviço de Polos
     * @author Arthur Luiz Lara Quites
     * @since 26/12/2017
     */
    public function polosAction()
    {
        $rest = new Zend_Rest_Server();
        $rest->setClass("PolosWS");
        $rest->handle();

    }

    /**
     * Método do Server do Rest
     *
     */
    public function salaAction()
    {
        $rest = new Zend_Rest_Server();
        $rest->setClass('SalaWS');
        $rest->handle();
    }

    /**
     * Método do Server do Rest
     */
    public function restAction()
    {

        if ($this->_hasParam('id')) {
            $bo = new WebServiceBO();

            if ($bo->autenticar($this->_getParam('chave'), $this->_getParam('id'))) {
                $rest = new Zend_Rest_Server();
                $rest->setClass("WebServiceBO");
                $rest->handle();
            } else {
                echo "Acesso Negado! " . $bo->codigo;
            }
        } else {
            echo "Acesso Negado! Id não encontrado!";
        }
        exit;
    }

    /**
     * Método do Server do Soap
     */
    public function soapAction()
    {
        $options["soapVersion"] = "SOAP_1_1";
        $options["actor"] = $_SERVER["SERVER_NAME"];
        $classes[] = "WebServiceBO";
        $options["classmap"] = $classes;
        $options["encoding"] = "UTF-8";
        $options["uri"] = $_SERVER["SERVER_NAME"];

        $soap = new Zend_Soap_Server(null, $options);
        $soap->setClass("WebServiceBO");
        $soap->setObject(new WebServiceBO());
        $soap->handle();
        exit;
    }

    public function wsdlAction()
    {

        echo $wsdl->toXML();
        exit;
    }

    /**
     * Método para gerar md5
     */
    public function md5Action()
    {
        Zend_Debug::dump(md5(1 . "webServiceGestor"));
        exit;
    }
}

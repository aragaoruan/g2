<?php

/**
 * Controller para Mensagem Cobranca
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 * @package default
 * @subpackage controller
 */
use G2\Negocio\MensagemCobranca;

class Default_MensagemCobrancaController extends Ead1_ControllerRestrita
{

    private $negocio;

    public function init ()
    {
        parent::init();
        $this->negocio = new MensagemCobranca();
    }

    public function jsAction ()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('mensagem-cobranca/index.js');
    }

    public function indexAction ()
    {
        $this->view->smsEntidade = $this->view->render('sms-entidade-mensagem/index.phtml');
    }

    /**
     * Recupera contas de envio por entidade sessão
     * @return json 
     * @param void
     */
    public function getContaDeEnvioAction ()
    {
        $result = $this->negocio->getContaEnvio();
        $arrRetorno = array();
        if ($result) {
            foreach ($result as $row) {
                $arrRetorno[] = array(
                    'id' => $row->getId_emailconfig(),
                    'st_conta' => $row->getSt_conta()
                );
            }
        }
        $this->_helper->json($arrRetorno);
    }

}
<?php

class Default_RelatorioLancamentoVendaProdutoController extends Ead1_Controller
{
    /**
     * @var \G2\Negocio\VwLancamentoVendaProduto
     */
    private $negocio;

    public function init()
    {
        $this->negocio = new \G2\Negocio\VwLancamentoVendaProduto();
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('relatorio-lancamento-venda-produto/atendente.js');
    }

    public function indexAction()
    {
    }

    public function atendenteAction()
    {
    }

    public function retornaTipoProdutoAction()
    {
        $negocio = new \G2\Negocio\Negocio();
        $results = $negocio->findAll('\G2\Entity\TipoProduto');
        foreach ($results as $result) {
            $arrResult[] = array(
                'id_tipoproduto' => $result->getId_tipoproduto(),
                'st_tipoproduto' => $result->getSt_tipoproduto()
            );
        }

        $this->_helper->json($arrResult);
    }

    public function retornaEntidadesAtivasAction()
    {
        $negocio = new G2\Negocio\Entidade();
        $resultado = $negocio->retornarVwEntidadeRecursaId();
        $this->_helper->json($resultado);
    }

    public function retornaAtendentesEntidadeAction()
    {

        $idEntidade = $this->getParam('id_entidade');

        if (isset($idEntidade)) {
            $listaAtendentes = $this->negocio->findBy('\G2\Entity\VwAtendente',
                array('id_entidade' => $idEntidade),
                array('st_nomecompleto' => 'ASC'));
        } else {
            $listaAtendentes = $this->negocio->findBy('\G2\Entity\VwAtendente',
                array('st_nomecompleto' => 'ASC'));
        }

        $arrResult = [];

        if ($listaAtendentes) {
            foreach ($listaAtendentes as $atendente) {
                $arrResult[] = array(
                    'id_usuario' => $atendente->getId_usuario(),
                    'st_nomecompleto' => $atendente->getSt_nomecompleto()
                );
            }
        }

        $this->_helper->json($arrResult);
    }

    public function pesquisarAction()
    {
        $params = $this->getAllParams();
        $titulo = 'Lançamentos da Venda Produto';

        if (isset($params['id_tiporelatoriolvp'])
            && $params['id_tiporelatoriolvp'] ==
            G2\Constante\RelatorioLancamentoVendaProduto::RELATORIO_LANCAMENTO_VENDA_PRODUTO_POR_ATENDENTE
        ) {
            $params['id_atendente'] = $this->negocio->sessao->id_usuario;
            $titulo .= ' Por Atendente';
        }

        $paramsClean = $this->limpaGetAllParams($params);

        $results = $this->negocio->lancamentoVendaProduto($paramsClean);

        // Caso o request seja para impressão ou geração de XLS.
        if ($this->getParam('type')) {
            $cabecalho = array(
                'st_nomeentidade' => "Entidade",
                'st_atendente' => "Atendente",
                'st_aluno' => "Aluno",
                'st_cpf' => "CPF",
                'st_rg' => "RG",
                'st_email' => "E-mail",
                'st_ddd' => "DDD",
                'st_telefone' => "Telefone",
                'st_cep' => "CEP",
                'st_endereco' => "Endereço",
                'st_bairro' => "Bairro",
                'st_complemento' => "Complemento",
                'nu_numero' => "Número",
                'st_cidade' => "Cidade",
                'sg_uf' => "UF",
                'id_venda' => "ID Venda",
                'st_meiopagamento' => "Meio de pagamento",
                'st_valorliquido' => "Valor Liquido",
                'st_valorbruto' => "Valor Bruto",
                'id_aluno' => "ID Usuário",
                'dt_confirmacao' => "Data Confirmação da Venda",
                'st_produto' => "Nome do Produto",
                'st_idproduto' => "ID do Produto",
                'dt_vencimento' => "Data Vencimento",
                'dt_baixa' => "Data Baixa",
                'st_valorlancamento' => "Valor do Lançamento",
                'st_valorlancamentopago' => "Valor Pago do Lançamento",
                'dt_cadastro' => "Data de Criação",
                'st_valorproduto' => "Valor do Produto",
                'id_lancamento' => "ID Lancamento",
                'st_ordem' => "Número Lancamento",
            );

            $this->negocio->export($results, $cabecalho, $this->getParam('type'),
                $titulo);
        }
        $this->_helper->json($results);
    }

}

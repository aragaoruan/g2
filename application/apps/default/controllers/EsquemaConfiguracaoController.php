<?php


class Default_EsquemaConfiguracaoController extends Ead1_ControllerRestrita
{
    /** @var \G2\Negocio\EsquemaConfiguracao $negocio */
    public $negocio;

    public function init()
    {
        parent::init();

        $this->negocio = new \G2\Negocio\EsquemaConfiguracao();
    }

    public function indexAction()
    {
        $id = $this->_getParam('id');
        if ($id) {
            $negocio = new \G2\Negocio\EsquemaConfiguracao();
            $result = $negocio->findByEsquemaId($id);
            // $nu_validadecartacredito = $negocio->retornaValidadeCredito();
            $this->view->nu_validadecartacredito = 0;
            $this->view->dados = $result->getMensagem();
        }

        $itemRenovacao = $this->negocio
            ->retornarItemConfiguracaoPorId(\G2\Constante\ItemConfiguracao::RENOVACAO_AUTOMATICA_A_PARTIR);

        if ($itemRenovacao) {
            $this->view->itemRenovacao = $itemRenovacao->toArray();
        }
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $controller = $this->getRequest()->getControllerName();
        echo $this->view->render($controller . '/index.js');
    }

    public function salvarEsquemaAction()
    {
        $dados = $this->limpaGetAllParams($this->getRequest()->getParams());
        $negocio = new \G2\Negocio\EsquemaConfiguracao();
        $result = $negocio->salvarEsquemaConfiguracao($dados);

        return $this->_helper->json($result);
    }

    public function retornarEsquemaConfiguracaoAction()
    {
        $id = $this->_getParam('id');
        if ($id) {
            $negocio = new \G2\Negocio\EsquemaConfiguracao();
            $result = $negocio->findByEsquemaId($id);

            return $this->_helper->json($result);
        }

        return false;

    }

    public function retornarEsquemaValoresAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $id_itemconfiguracao = $this->getRequest()->getParam('id_itemconfiguracao', null);

        if ($id_itemconfiguracao) {
            $result = $this->negocio->retornarEsquemaValores(array('id_itemconfiguracao' => $id_itemconfiguracao));
            $result = $result->getMensagem();

            if ($result === 'Nenhum registro encontrado!')
                return false;

            return $this->_helper->json($result);
        }

        return false;

    }

    public function retornarHistoricoParametroRenovacaoAction()
    {
        //desabilita a visualização da view do zend
        $this->_helper->viewRenderer->setNoRender();
        try {
            //define o id da configuração que veio via parametro
            $id_esquemaconfiguracao = $this->getParam('id_esquemaconfiguracao');
            //verifica se o parametro tem valor
            if (!$id_esquemaconfiguracao) {
                throw  new Exception(\G2\Constante\MensagemSistema::ID_ESQUEMA_VAZIO);
            }
            //chama a negocio de log e o método
            $negocioLog = new \G2\Negocio\Log();
            $mensageiro = $negocioLog->retornarLogParametroRenovacaByEsquemaConfiguracao($id_esquemaconfiguracao);

            //verifica se veio sucesso do método
            if ($mensageiro->getType() != Ead1_IMensageiro::TYPE_SUCESSO) {
                throw new Exception($mensageiro->getText());
            }

            //response
            $this->getResponse()->appendBody(json_encode($mensageiro->getMensagem()));

        } catch (Exception $e) {
            $this->getResponse()->setHttpResponseCode(400)
                ->appendBody($e->getMessage());
        }

    }
}

<?php

/**
 * Classe Controller para Grade Horaria
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Default_GradeHorariaController extends Ead1_ControllerRestrita
{

    /**
     * @var GradeHorariaForm $form
     */
    private $form;

    /**
     * @var \G2\Negocio\GradeHoraria $negocio
     */
    private $negocio;

    public function init()
    {
        $this->form = new GradeHorariaForm();
        $this->negocio = new \G2\Negocio\GradeHoraria();
    }

    public function indexAction()
    {
        $id = $this->getParam('id');

        if ($id) {
            $result = $this->negocio->findGradeHoraria($id);
            $data = array(
                'id' => $result->getId_gradehoraria(),
                'id_gradehoraria' => $result->getId_gradehoraria(),
                'st_nomegradehoraria' => $result->getSt_nomegradehoraria(),
                'id_situacao' => $result->getId_situacao() ? $result->getId_situacao()->getId_situacao() : NULL,
                'id_entidade' => $result->getId_entidade() ? $result->getId_entidade()->getId_entidade() : NULL,
                'dt_iniciogradehoraria' => $result->getDt_iniciogradehoraria()->format('d/m/Y'),
                'dt_fimgradehoraria' => $result->getDt_fimgradehoraria()->format('d/m/Y'),
                'dt_cadastro' => $result->getDt_cadastro(),
                'bl_ativo' => $result->getBl_ativo()
            );
            $this->view->grade = $data;
        }

        $perfilNegocio = new \G2\Negocio\Perfil();
        $perfilProfessor = $perfilNegocio->retornarPerfilProfessor();
        $idPerfilProfessor = null;
        if ($perfilProfessor instanceof \G2\Entity\Perfil) {
            $idPerfilProfessor = $perfilProfessor->getId_perfil();
        }

        $this->view->idPerfilProfessor = $idPerfilProfessor;
        $this->view->form = $this->form;

    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        echo $this->view->render($this->_getParam('controller') . '/index.js');
    }

    public function salvarItensGradeETurmaAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        if ($this->_request->isPost()) {
            //pega os dados que forma postados
            $params = $this->_request->getPost();
            //envia para a negocio
            $result = $this->negocio->procedimentoSalvarItemGradeHorariaComTurma($params);

            //pega o retorno
            if ($result->getType() == Ead1_IMensageiro::TYPE_SUCESSO) {
                $this->getResponse()->setHttpResponseCode(201)->setBody(json_encode(array(
                    'type' => $result->getType(),
                    'title' => $result->getTitle(),
                    'text' => $result->getText()
                )));
            } elseif ($result->getType() == Ead1_IMensageiro::TYPE_AVISO) {
                $this->getResponse()->setHttpResponseCode(412)->setBody(json_encode(array(
                    'type' => $result->getType(),
                    'title' => $result->getTitle(),
                    'text' => $result->getText()
                )));
            } else {
                $this->getResponse()->setHttpResponseCode(400)->setBody(json_encode(array(
                    'type' => 'error',
                    'title' => "Erro!",
                    'text' => "Erro ao salvar grade."
                )));
            }
        }
    }

    public function getListaItensGradeAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocio->retornaListaItensGrade($params);
        $this->_helper->json($result);
    }

    public function agendaProfessorAction()
    {
        $this->_helper->layout->disableLayout();
        $params = $this->limpaGetAllParams($this->getAllParams());

        $return = $this->negocio->retornaDadosAgendaProfessor($params);
        //monta as variaveis para va view
        $this->view->professor = $return['professor'];
        $this->view->gradeHoraria = $return['gradeHoraria'];
        $this->view->diasSemana = $return['diasSemana'];
        $this->view->turnos = $return['turnos'];
        $this->view->params = $return['params'];
        $this->view->entidade = $return['entidade'];

    }

    public function printAction()
    {
        $this->_helper->layout->disableLayout();

        $idGrade = $this->getParam('id_gradehoraria');
        $gradeHoraria = $this->negocio->findGradeHoraria($idGrade);

        if ($gradeHoraria) {
            $itensGrade = $this->negocio->retornaListaItensGrade(array('id_gradehoraria' => $gradeHoraria->getId_gradehoraria()));

            $this->view->gradeHoraria = $gradeHoraria;
            $this->view->itensGrade = $itensGrade;
        } else {
            $this->view->mensagem = 'Grade Horaria não encontrada.';
        }

    }

    public function printComboAction()
    {
        $this->_helper->layout->disableLayout();

        $data = $this->limpaGetAllParams($this->getRequest()->getParams());
        $arrayGradeHoraria = explode(',', $data['id_gradehoraria']);

        $gradeHoraria = array();
        foreach ($arrayGradeHoraria as $value) {
            $gradeHoraria[] = $this->negocio->findGradeHoraria($value, true);
        }

        if (isset($gradeHoraria) && !empty($gradeHoraria)) {

            foreach ($gradeHoraria as &$grade) {
                $grade['itensGrade'] = $this->negocio->retornaListaItensGrade(array('id_gradehoraria' => $grade['id_gradehoraria']));
            }

            $this->view->gradeHoraria = $gradeHoraria;

        } else {
            $this->view->mensagem = 'Grade Horaria não encontrada.';
        }

    }


    public function professoresTirinhaAction()
    {
        $arrayParams = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->negocio->retornarProfessoresItensGradeByGrade($arrayParams);
        $this->_helper->json($result);
    }

    public function enviarAgendaProfessorEmailAction()
    {
        try {
            $this->_helper->viewRenderer->setNoRender();
            $params = $this->limpaGetAllParams($this->getAllParams());

            $return = $this->negocio->retornaDadosAgendaProfessor($params);
            //monta as variaveis para va view
            $this->view->professor = $return['professor'];
            $this->view->gradeHoraria = $return['gradeHoraria'];
            $this->view->diasSemana = $return['diasSemana'];
            $this->view->turnos = $return['turnos'];
            $this->view->params = $return['params'];
            $this->view->entidade = $return['entidade'];


            $htmlEmail = $this->getTextoEmail($params['id_gradehoraria'], $params['id_professor'], $return['professor']['st_nomeexibicao'], $return['entidade']['id_entidade']);
            $this->view->textoAuxiliarEmail = $htmlEmail;

            $view = $this->view->render('grade-horaria/agenda-professor.phtml');
            $result = $this->negocio->enviaEmailGradeProfessor($view, $return['professor'], true);
        } catch (Exception $e) {
            $result = new Ead1_Mensageiro('Erro ao tentar enviar grade para professor. ' . $e->getMessage());
        }
        $this->_helper->json($result->toArrayAll());
    }

    public function enviarEmailsProfessoresEmCadeiaAction()
    {
        try {
            $mensageiro = new Ead1_Mensageiro();
            $this->_helper->viewRenderer->setNoRender();
            $params = $this->limpaGetAllParams($this->getAllParams());
            if (!isset($params['id_gradehoraria']) || empty($params['id_gradehoraria'])) {
                throw new Exception("Grade Horaria não informada.");
            } else {
                $id_grade = $params['id_gradehoraria'];
            }

            if (!isset($params['professores']) || !is_array($params['professores'])) {
                throw new Exception("Professores não informados.");
            } else {
                $professores = $params['professores'];
            }
            $sucesso = true;
            foreach ($professores as $professor) {
                $return = $this->negocio->retornaDadosAgendaProfessor(array(
                    'id_professor' => $professor,
                    'id_gradehoraria' => $id_grade
                ));
                //monta as variaveis para va view
                $this->view->professor = $return['professor'];
                $this->view->gradeHoraria = $return['gradeHoraria'];
                $this->view->diasSemana = $return['diasSemana'];
                $this->view->turnos = $return['turnos'];
                $this->view->params = $return['params'];
                $this->view->entidade = $return['entidade'];


                $htmlEmail = $this->getTextoEmail($params['id_gradehoraria'], $professor, $return['professor']['st_nomeexibicao'], $return['entidade']['id_entidade']);
                $this->view->textoAuxiliarEmail = $htmlEmail;

                $view = $this->view->render('grade-horaria/agenda-professor.phtml');
                $result = $this->negocio->enviaEmailGradeProfessor($view, $return['professor'], false);

                if ($result->getType() != Ead1_IMensageiro::TYPE_SUCESSO) {
                    $sucesso = false;
                    throw new Exception('Erro ao enviar email para professor especifico: id_professor => ' . $professor);
                }

            }
            if ($sucesso) {
                $mensageiro->addMensagem('E-mails enviados com sucesso!', Ead1_IMensageiro::SUCESSO);
            }
        } catch (Exception $e) {
            $mensageiro->addMensagem('Erro ao tentar enviar grade para professores. ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        $this->_helper->json($mensageiro->toArrayAll());
    }

    public function imprimirTodasTirinhasAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        if (!isset($params['id_gradehoraria']) || empty($params['id_gradehoraria'])) {
            throw new Exception("Grade Horaria não informada.");
        } else {
            $id_grade = $params['id_gradehoraria'];
        }
        if (!isset($params['professores']) || empty($params['professores'])) {
            throw new Exception("Professores não informados.");
        } else {
            $professores = explode(',', $params['professores']);
        }

        $arrReturn = array();

        foreach ($professores as $value) {
            $return = $this->negocio->retornaDadosAgendaProfessor(array(
                'id_professor' => $value,
                'id_gradehoraria' => $id_grade
            ));

            //monta as variaveis para va view
            $array = array(
                'professor' => $return['professor'],
                'gradeHoraria' => $return['gradeHoraria'],
                'diasSemana' => $return['diasSemana'],
                'turnos' => $return['turnos'],
                'params' => $return['params'],
                'entidade' => $return['entidade']
            );
            array_push($arrReturn, $array);
        }

        $this->view->dado = $arrReturn;
        $this->view->id_grade = $id_grade;

    }

    public function imprimirTodasTirinhasComboAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());

        if (!isset($params['data']) || empty($params['data'])) {
            throw new Exception("Professores não informados.");
        } else {
            $professores = explode(',', $params['data']);
        }

        $arrReturn = array();

        foreach ($professores as $value) {
            $explode = explode('-', $value);
            $id_gradehoraria = $explode[0];
            $id_professor = $explode[1];
            $id_unidade = $explode[2];
            $return = $this->negocio->retornaDadosAgendaProfessor(array(
                'id_professor' => $id_professor,
                'id_gradehoraria' => $id_gradehoraria,
                'id_entidade' => $id_unidade
            ));
            //monta as variaveis para va view
            $array = array(
                'professor' => $return['professor'],
                'gradeHoraria' => $return['gradeHoraria'],
                'diasSemana' => $return['diasSemana'],
                'turnos' => $return['turnos'],
                'params' => $return['params'],
                'entidade' => $return['entidade']
            );
            array_push($arrReturn, $array);
        }
        $this->view->dado = $arrReturn;
    }

    private function getTextoEmail($id_grade, $id_professor, $st_nomeprofessor, $id_entidade)
    {
        $url = Ead1_Ambiente::geral()->st_url_gestor2 . "/grade-horaria/agenda-professor/pdf/1/id_gradehoraria/{$id_grade}/id_professor/{$id_professor}/id_entidade/{$id_entidade}";


        $html = "<p>Prezado(a) Professor(a) {$st_nomeprofessor}</p>";
        $html .= "<p>Segue a sua grade horária dos próximos 7 dias.</p>";
        $html .= "<p>Muitas vezes o seu serviço de email vai apresentar incompatibilidade e trazer a grade em um formato não muito agradável.</p>";
        $html .= "<p>Se esse é o seu caso, por favor clique no link . <a href='{$url}'>{$url}</a> para ver a sua grade em um formato mais agradável.</p>";
        return $html;

    }

    public function imprimiGradeHorariaComboAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $data = $this->limpaGetAllParams($this->getRequest()->getParams());

        $result = $this->negocio->retornarImprimiGradeHorariaCombo($data);

        $this->getResponse()->setHttpResponseCode(201)->appendBody(json_encode($result));
    }

} 
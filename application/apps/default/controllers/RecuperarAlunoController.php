<?php

/**
 * Controller Relatorio Alunos a Recuperar
 * @author Rafael Leite <rafael.leite@unyleya.com.br>
 * @since 2017-02-03
 * @package application
 * @subpackage controller
 *
 *
 * @update 31/10/2017
 * @author Alex Alexandre <alex.alexandre@unyleya.com.br>
 */

class Default_RecuperarAlunoController extends Ead1_Controller
{
    /**
     * @var \G2\Negocio\VwAlunosRecuperar
     */
    private $negocio;

    /**
     * @var \G2\Negocio\Relatorio
     */
    private $relatorio;

    /**
     * @var \G2\Negocio\Ocorrencia
     */
    private $ocorrencia;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\VwAlunosRecuperar();
        $this->ocorrencia = new G2\Negocio\Ocorrencia();
        $this->relatorio = new \G2\Negocio\Relatorio();
    }

    public function indexAction()
    {
        $indexJs = $this->view->render('recuperar-aluno/index.js');
        echo $this->view->headScript()->appendScript($indexJs);
    }

    public function pesquisarAction()
    {
        try {

            $params = $this->getAllParams();
            unset($params['module'], $params['controller'], $params['action'], $params['order'],
                $params['sort'], $params['nu_registros']);

            $results = $this->negocio->retornaResultadoPesquisa($params);

            if ($this->getParam('type')) {
                $cabecalho = array(
                    'st_nomecompleto'           => "Nome",
                    'st_cpf'                    => "CPF",
                    'st_email'                  => "E-mail",
                    'st_ddd'                    => "DDD Principal",
                    'st_telefone'               => "Telefone Principal",
                    'st_dddalternativo'         => "DDD Secundário",
                    'st_telefonealternativo'    => "Telefone Secundário",
                    'st_areaconhecimento'       => "Área",
                    'st_projetopedagogico'      => "Projeto Pedagógico",
                    'st_evolucao'               => "Evolução",
                    'dt_inicioturma'            => "Data de Abertura da Turma",
                    'st_disciplina'             => "Nome Disciplina",
                    'dt_aberturasala'           => "Data de Abertura Sala",
                    'dt_encerramentosala'       => "Data Encerramento Sala",
                    'st_nota'                   => "Nota da Prova",
                    'st_notaead'                => "Nota EAD",
                    'st_notafinal'              => "Nota Final"
                );

                $this->relatorio->export($results, $cabecalho, $this->getParam('type'), 'Alunos a Recuperar');
            }

            $this->_helper->json($results);
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json($e->getMessage());
        }
    }

    /**
     * Função responsável por chamar a camada de negocio
     * para buscar os atendentes e num de ocorrencias
     * @author Alex Alexandre <alex.alexandre@unyleya.com.br>
     */
    public function getAtendenteAction()
    {
        $results = $this->ocorrencia->getAtendente($this->getParam("tipoServico"));
        $this->_helper->json($results);
    }

    /**
     * Função responsável por receber os parametros,
     * fazer um primeiro tratamento para liberar para
     * a proxima camada
     */
    public function criarOcorrenciasAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $qtdAlunos = count($params["idUsuario"]);

        for ($i = 0; $i < $qtdAlunos; $i++) {
            $ocorrencia = $this->ocorrencia->tratarDadosNovaOcorrencia(
                $params["idUsuario"][$i],
                $params["idSalaDeAula"][$i],
                $params["atendenteID"],
                $params["tipoOcorrencia"]);
        }

        $this->_helper->json($ocorrencia);
    }

}

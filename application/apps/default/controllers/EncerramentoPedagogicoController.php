<?php

/**
 * Classe de controller MVC para controlar Enc. Financeiro
 * @author Rafael Rocha <rafael.rocha.mg@gmail.com>
 *
 * @package application
 * @subpackage controller
 */
class Default_EncerramentoPedagogicoController extends Ead1_ControllerRestrita
{

    private $_bo;
    private $_secretariaBO;
    private $_negocio;

    public function init()
    {
        parent::init();
        parent::setupDoctrine();
        $this->_negocio = new \G2\Negocio\Secretaria();
    }

    public function jsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        echo $this->view->render('encerramento-pedagogico/index.js');
    }

    public function indexAction()
    {

    }

    public function recusaPagamentoAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);

        $dados = $this->getAllParams();
        $retorno = $this->_negocio->salvarRecusaPagamento($dados);

        echo Zend_Json_Encoder::encode($retorno);
    }

    public function liberaPagamentoAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);

        $dados = $this->getAllParams();
        $retorno = $this->_negocio->liberaPagamento($dados);

        echo Zend_Json_Encoder::encode($retorno);
    }
}

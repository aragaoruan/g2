<?php

/**
 * Class Zend_View_Helper_ItemGradeHoraria Helper para retornar item da grade
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 10/12/2014
 */
class Zend_View_Helper_ItemGradeHoraria extends Zend_View_Helper_Abstract
{

    public function itemGradeHoraria(array $params)
    {
        $gradeHorariaNegocio = new \G2\Negocio\GradeHoraria();
        return $gradeHorariaNegocio->retornaListaItensGrade($params);
    }
} 
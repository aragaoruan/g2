<?php

/**
 * Class Zend_View_Helper_BarCode Helper para gerar código de barras
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-09-01
 */
class Zend_View_Helper_BarCode extends Zend_View_Helper_Abstract
{
    public function barCode($text, $barHeight = 40, $drawText = false, $backgroundColor = '#E6E7E8')
    {
        $options = array(
            'text' => $text,
            'barHeight' => $barHeight,
            'drawText' => $drawText,
            'backgroundColor' => $backgroundColor
        );
        $barCode = new Zend_Barcode_Object_Code128($options);
        $renderer = new Zend_Barcode_Renderer_Image();
        $renderer->setBarcode($barCode);
        return $renderer->render();
    }
}
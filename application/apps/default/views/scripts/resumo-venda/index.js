/**
 * ******************************************************
 * ******                  VARIAVEIS              *******
 * ******************************************************
 */
var entidadeSelect;
var viewRelatorio;

var ModelRelatorio = Backbone.Model.extend({
    defaults: {
        valor_dinheiro: '',
        valor_cheque: '' ,
        valor_cartao: '' ,
        valor_transferencia: '' ,
        valor_carta: '' ,
        valor_bolsa: '' ,
        valor_deposito: '' ,
        valor_empenho: '' ,
        valor_boleto: '' ,
        entidade: '' ,
        total_vendas: '' ,
        valor_recebido: ''
    }
});


/**
 * ******************************************************
 * ******               COLLECTION                *******
 * ******************************************************
 */

var EntidadeCollection = Backbone.Collection.extend({
    url: '/quantidade-transacoes-cartao-credito/retorna-entidade-recursiva',
    model: VwEntidadeRecursiva,
    initialize: function () {
        //this.fetch()
    }
});

var RelatorioCollection = Backbone.Collection.extend({
    model: ModelRelatorio
});

/**
 * ******************************************************
 * ******                  LAYOUT                 *******
 * ******************************************************
 */

var FolhaLayout = Marionette.LayoutView.extend({
    template: '#resumo_venda_layout',
    tagname: 'div',
    className: 'containder-fluid',
    regions: {
        relatorio: '#tabela-relatorio',
        itensRelatorio: '#tabelaDadosRelatorio'
    },
    ui: {
        'idEntidade': '#id_entidade',
        'dtInicio': "#dt_filtro_inicio",
        'dtFim': "#dt_filtro_fim"
    },
    initialize: function () {
        thisLayout = this;
    },
    onShow: function () {
        this.renderizaSelectEntidade();
        loaded()
    },
    renderizaSelectEntidade: function () {
        loading();
        var that = this;
        var collectionEntidade = new EntidadeCollection();
        collectionEntidade.fetch({
            success: function () {
                var view = new SelectView({
                    el: that.ui.idEntidade,                 // Elemento da DOM
                    collection: collectionEntidade,         // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_nomeentidade',     // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_entidade',         // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: null                // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();
                loaded();
            }
        });

    },
    gerarRelatorio: function () {
        if (this.validaFiltros()) {
            var that = this;
            loading();

            var collectionResultRelatorio = Backbone.Collection.extend({
                model: ModelRelatorio
            });
            var collectionFetch = new collectionResultRelatorio();
            collectionFetch.url = '/resumo-venda/retorna-relatorio';
            collectionFetch.fetch({
                data: {
                    'id_entidade': that.ui.idEntidade.val(),
                    'dt_fim': that.ui.dtFim.val(),
                    'dt_inicio': that.ui.dtInicio.val()
                },
                type: 'POST',
                success: function () {
                    if(collectionFetch.models==""){
                        $.pnotify({
                            title: 'Aviso!',
                            text: 'Nenhum dado encontrado para gerar relatório.',
                            type: 'warning'
                        });

                        //$('#btn_gerar_xls').prop('disabled',true);
                        $('#container-table').hide('fast');

                        loaded();

                    }else{

                        //$('#btn_gerar_xls').prop('disabled',false);

                        var CollectionTable = Backbone.Collection.extend({
                            model: ModelRelatorio
                        });

                        var itemView = Marionette.ItemView.extend({
                            tagName: 'tr',
                            template: '#item-relatorio',
                            formatNumber: function(field){
                                var n = 2; //n: length of decimal
                                var x = 3; //x: length of sections
                                var s = '.';
                                var c = ','

                                var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
                                    num = parseFloat(this.model.get(field)).toFixed(Math.max(0, ~~n));
                                var value =  (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
                                console.log((value))
                                return (value == 'NaN') ? '0,00' : value;
                            },
                            onBeforeRender: function () {

                                this.model.set({ valor_dinheiro: this.formatNumber('valor_dinheiro') });
                                this.model.set({ valor_cheque: this.formatNumber('valor_cheque') });
                                this.model.set({ valor_cartao: this.formatNumber('valor_cartao') });
                                this.model.set({ valor_transferencia: this.formatNumber('valor_transferencia') });
                                this.model.set({ valor_carta: this.formatNumber('valor_carta') });
                                this.model.set({ valor_bolsa: this.formatNumber('valor_bolsa') });
                                this.model.set({ valor_deposito: this.formatNumber('valor_deposito') });
                                this.model.set({ valor_empenho: this.formatNumber('valor_empenho') });
                                this.model.set({ valor_boleto: this.formatNumber('valor_boleto') });
                                //this.model.set({ total_vendas: this.formatNumber('total_vendas') });
                                this.model.set({ valor_recebido: this.formatNumber('valor_recebido') });

                            }
                        });

                        var quantidadeCartaoView = Marionette.CompositeView.extend({
                            childView: itemView,
                            template: "#script_tabela_relatorio",
                            childViewContainer: 'tbody',
                            initialize: function () {

                            },
                            onShow: function () {
                                return this;
                            }
                        });

                        viewRelatorio = new quantidadeCartaoView({
                            el: that.$el.find('#container-table'),
                            collection: collectionFetch
                        });

                        $('#container-table').show();
                        viewRelatorio.render();
                        loaded()
                    }

                }
            });
        }

    },
    validaFiltros: function () {
        flag = true;

        var dtInicio = $("#dt_filtro_inicio").val().split("/");
        var dtFim = $("#dt_filtro_fim").val().split("/");

        var date1 = new Date(dtInicio[2] + "/" + dtInicio[1] + "/" + dtInicio[0]);
        var date2 = new Date(dtFim[2] + "/" + dtFim[1] + "/" + dtFim[0]);

        var diferenca = Math.abs(date1 - date2); //diferença em milésimos e positivo
        var dia = 1000*60*60*24; // milésimos de segundo correspondente a um dia
        var total = Math.round(diferenca/dia); //valor total de dias arredondado

        if(total > 365){
            $.pnotify({
                title: 'Aviso!',
                text: 'O período entre a data de Matrícula deve ser de no máximo 365 dias.',
                type: 'warning'
            });
            flag = false;
        }

        return flag;
    },
    gerarXLS: function(){
        that = this;
        if(this.validaFiltros()){
            var data = {
                'id_entidade': that.ui.idEntidade.val(),
                'dt_fim': that.ui.dtFim.val(),
                'dt_inicio': that.ui.dtInicio.val()
            }
            var url = '/resumo-venda/gerar-xls/?tipo=xls&id_entidade='+that.ui.idEntidade.val()+'&dt_fim='+that.ui.dtFim.val()+'&dt_inicio='+that.ui.dtInicio.val();
            window.open(url,'_blank');
        }
    },
    events: {
        'click #btn_gerar_relatorio': 'gerarRelatorio',
        'click #btn_gerar_xls': 'gerarXLS'
    }
})

/**
 * ******************************************************
 * ******                 ITEMVIEW                *******
 * ******************************************************
 */
var tabelaRelatorioView = Marionette.ItemView.extend({
    template: '#item-relatorio',
    tagName: 'tr',
    ui: {},
    onRender: function () {
    }
});

var relatorioView = Marionette.ItemView.extend({
    template: '#script_tabela_relatorio',
    ui: {}

});

/**
 * ******************************************************
 * ******               COLLECTIONVIEW            *******
 * ******************************************************
 */
var RelatorioCollectionView = Marionette.CollectionView.extend({
    template: '#tabela-relatorio',
    el: '#tabelaDadosRelatorio',
    childView: tabelaRelatorioView
});


/**
 * ******************************************************
 * ******              COMPOSITEVIEW              *******
 * ******************************************************
 */
var layoutFolha = new FolhaLayout();
G2S.show(layoutFolha);




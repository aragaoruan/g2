/**
 * Created by Débora Castro on 04/11/14.
 * <debora.castro@unyleya.com.br>
 */


$.ajaxSetup({'async': false});

/*******************************************************************************
 *  Views
 ******************************************************************************/

/***********************
 *   imports
 **********************/
$.getScript('js/backbone/views/Conclusao/PesquisaAlunosConclusaoView.js');

/***********************
 *   ConclusaoView
 **********************/

var ConclusaoView = Backbone.Marionette.LayoutView.extend({
    template: '#layout-regions',
    events: {
    },
    regions: {
        'pesquisaRegion': '#container-pesquisa',
        'listaAlunosRegion': '#container-lista-alunos'
    },
    renderPesquisaAlunosConclusaoView: function() {
        this.pesquisaRegion.show(new PesquisaAlunosConclusaoView());
    },
    onShow: function() {
        this.renderPesquisaAlunosConclusaoView();
    }
});


var view = new ConclusaoView();
G2S.layout.content.show(view);

$.ajaxSetup({'async': true});
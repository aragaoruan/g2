/**
 * Reescreve o modelo de Livro Coleção e adiciona atributo is_editing aos defaults e também cria o metodo toggleEdit
 * @type Backbone Model Reescreve Modelo de Livro Coleção @exp;LivroColecao@call;extend
 */
var LivroColecaoModel = LivroColecao.extend({
    initialize: function () {
        this.defaults['is_editing'] = false;
        this.defaults['bl_ativo'] = true;
        return this;
    },
    toggleEdit: function () {
        this.is_editing = !this.is_editing;
    }
});
/**
 * Reescreve o modelo de VwUsuarioPerfilEntidadeReferencia adicionando atributo is_editing no default e criando metodo toggleEdit
 * @type Backbone Model @exp;VwUsuarioPerfilEntidadeReferencia@call;extend
 */
var VwUsuarioPerfilEntidadeReferenciaModel = VwUsuarioPerfilEntidadeReferencia.extend({
    initialize: function () {
        this.defaults['is_editing'] = true;
        return this;
    },
    toggleEdit: function () {
        this.is_editing = !this.is_editing;
    }
});

/**
 * @type Backbone Collection para VwUsuarioPerfilEntidadeReferencia @exp;Backbone@pro;Collection@call;extend
 */
var VwUsuarioPerfilEntidadeReferenciaCollection = Backbone.Collection.extend({
    url: 'api/vw-usuario-perfil-entidade-referencia',
    model: VwUsuarioPerfilEntidadeReferenciaModel
});

/**
 * @type Backbone Collection LivroEntidade @exp;Backbone@pro;Collection@call;extend
 */
var LivroEntidadeCollection = Backbone.Collection.extend({
    url: '/api/livro-entidade/',
    model: LivroEntidade
});

/**
 * @type Backbone Collection LivroCollection @exp;Backbone @pro;Collection @call;extend
 */
var LivroCollection = Backbone.Collection.extend({
    url: 'api/livro',
    model: Livro
});

/**
 * @type Backbone Collection para Colecao Livro @exp;Bakcbone@pro;Collection@call;extend
 */
var LivroColecaoCollection = Backbone.Collection.extend({
    url: 'api/livro-colecao',
    model: LivroColecaoModel
});


/**
 * @type Backbone View para select de Coleções @exp;Backbone@pro;View@call;extend
 */
var SelectColecaoLivroView = Backbone.View.extend({
    tagName: 'option',
    initialize: function () {
        _.bindAll(this, 'render');
    },
    render: function () {
        $(this.el).attr('value', this.model.get('id_livrocolecao')).html(this.model.get('st_livrocolecao'));
        return this;
    }
});

/**
 * @type Backbone View para ColecaoLivro @exp;Backbone@pro;View@call;extend
 */
var ColecaoLivroView = Backbone.View.extend({
    initialize: function () {
        this.render = _.bind(this.render, this);
        this.model.bind('change', this.render);
    },
    bindings: {
        'input[name=st_livrocolecao]': 'st_livrocolecao'
    },
    render: function () {
        var variables = this.model.toJSON();
        var tr;
        if (this.model.is_editing) {
            tr = _.template($("#livroColecaoTemplateEdit").html(), variables);
        } else {
            tr = _.template($("#livroColecaoTemplateList").html(), variables);
        }
        this.el.innerHTML = tr;
        if (!this.model.id) {
            $(this.el).find('.delete-link').hide();
        }
        return this;
    },
    toggleEdit: function () {
        this.oldSettings = this.model.toJSON();
        this.model.toggleEdit();
        this.render();
    },
    resetEdit: function () {
        this.model.set(this.oldSettings);
        this.toggleEdit();
        if (!this.model.id) {
            this.remove();
        }
    },
    saveChanges: function () {
        that = this;
        this.model.set({st_livrocolecao: $(this.el).find('input[name=st_livrocolecao]').val()});
        this.model.save(null, {
            success: function (model, response) {
                that.toggleEdit();
                that.render();
                that.populeSelectColecao();
                $.pnotify({title: 'Sucesso!', text: 'Operação realizada com sucesso!', type: 'success'});
            },
            error: function (model, response) {
                $.pnotify({title: 'Erro!', text: 'Houve um problema ao executar esta operação, tente novamente', type: 'error'});
            }
        });
    },
    delete: function () {
        that = this;
        bootbox.confirm("Tem certeza de que deseja apagar o registro?", function (result) {
            if (result) {
                that.model.destroy({});
                that.remove();
                $.pnotify({title: 'Registro Removido', text: 'O registro foi removido do banco de dados com sucesso!', type: 'success'});
                that.populeSelectColecao();
            }
        });
    },
    populeSelectColecao: function () {
        var livroColecao = new LivroColecaoCollection();
        livroColecao.fetch({
            success: function () {
                renderer = new CollectionView({
                    collection: livroColecao,
                    childViewConstructor: SelectColecaoLivroView,
                    childViewTagName: 'option',
                    el: $('#id_livrocolecao')
                }).render();
                $('#id_livrocolecao').prepend('<option selected>Selecione</option>');
            }
        });
    },
    events: {
        'click .edit-link': 'toggleEdit',
        'click .save-link': 'saveChanges',
        'click .delete-link': 'delete',
        'click .cancel-link': 'resetEdit'
    }
});

/**
 * @type View Backbone para gerenciar ações de autores @exp;Backbone@pro;View@call;extend
 */
var AutoresView = Backbone.View.extend({
    initialize: function () {
        this.render = _.bind(this.render, this);
        this.model.bind('change', this.render);
    },
    render: function () {
        var variables = this.model.toJSON();
        var tr;
        if (this.model.is_editing) {
            if (this.oldSettings) {
                tr = _.template($("#livroAutorEdit").html(), variables);
            } else {
                tr = _.template($("#livroAutorAdd").html(), variables);
            }
        } else {
            tr = _.template($("#livroAutorList").html(), variables);
        }
        this.el.innerHTML = tr;
        if (this.model.get('bl_autor')) {
            this.$el.find('span.label').removeClass('label-important').addClass('label-success').html('Será exibido');
        }


        if (this.model.get('bl_autor') && this.model.id) {
            $(this.el).find("input[name='bl_autor']").attr('checked', 'checked');
        }
        if (!this.model.id) {
            $(this.el).find('.salva-autor').hide();
        }
        return this;
    },
    checkBlAutor: function () {
        var checkInput = $(this.el).find('input[name="bl_autor"]').is(':checked');
        if (checkInput) {
            $(this.el).find('select[name="id_usuario"]').attr('required', 'required');
            $(this.el).find("span.label").removeClass('label-important')
                .addClass('label-success').html('Será exibido');
        } else {
            $(this.el).find('select[name="id_usuario"]').removeAttr('required');
            $(this.el).find("span.label").removeClass('label-success')
                .addClass('label-important').html('Não será exibido');
        }
    },
    removeAutor: function () {
        that = this;
        bootbox.confirm("Tem certeza de que deseja remover o registro?", function (result) {
            if (result) {
                if (that.model.id) {
                    that.model.destroy({});
                }
                that.remove();
                bootbox.hideAll();
                $.pnotify({title: 'Registro Removido', text: 'O registro foi removido com sucesso!', type: 'success'});
                that.populeSelectColecao();
            }
        });
    },
    resetEdit: function () {
        this.model.set(this.oldSettings);
        this.toggleEdit();
        if (!this.model.id) {
            this.remove();
        }
    },
    toggleEdit: function () {
        this.oldSettings = this.model.toJSON();
        this.model.toggleEdit();
        this.render();
    },
    saveChanges: function () {
        that = this;
        var blAutor = $(this.el).find('input[name="bl_autor"]').is(':checked');
        this.model.set({
            bl_autor: blAutor
        });
        this.model.save(null, {
            success: function (model, response) {
                that.toggleEdit();
                that.render();
                $.pnotify({title: 'Sucesso!', text: 'Alteração realizada com sucesso!', type: 'success'});
            },
            error: function (model, response) {
                $.pnotify({title: 'Erro!', text: 'Houve um problema ao executar esta operação, tente novamente', type: 'error'});
            }
        });
    },
    events: {
        'click input[name="bl_autor"]': 'checkBlAutor',
        'click .remove-autor': 'removeAutor',
        'click .edit-link': 'toggleEdit',
        'click .cancel-link': 'resetEdit',
        'click .salva-autor': 'saveChanges'
    }
});


/**
 * @type Backbone View para Cadastro de livro @exp;Backbone @pro;View @call;extend
 */
var LivroView = Backbone.View.extend({
    initialize: function () {
        this.render = _.bind(this.render, this);
        this.model.bind('change', this.render);
    },
    render: function () {
        var variables = this.model.toJSON();
        var elemTemplate;
        elemTemplate = _.template($("#templateLivro").html(), variables);
        this.el.innerHTML = elemTemplate;
        return this;
    },
    showModalColecao: function () {
        loading();
        $("#modalColecao").modal();
        var livroColecao = new LivroColecaoCollection();
        livroColecao.fetch({
            success: function () {
                new CollectionView({
                    collection: livroColecao,
                    childViewConstructor: ColecaoLivroView,
                    childViewTagName: 'tr',
                    el: $('#tabela-colecao tbody')
                }).render();
                loaded();
            }
        });
    },
    validaNum: function (event) {
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || (event.keyCode == 65 && event.ctrlKey === true) || (event.keyCode >= 35 && event.keyCode <= 39)) {
            return;
        } else {
            /* Testar acionamento de outras teclas */
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
            }
        }
    },
    events: {
        'click a#showColecao': 'showModalColecao',
        'keydown .numeric': 'validaNum'
    }
});


/**
 * Cria novo modelo e elemento na lista de autores
 * @returns {void}
 */
function novoAutor() {
    var autorRender = new CollectionView({
        collection: new VwUsuarioPerfilEntidadeReferenciaCollection,
        childViewConstructor: AutoresView,
        childViewTagName: 'tr',
        el: $('#tableAutores tbody')
    });
    autorRender._rendered = true;
    autorModel = new VwUsuarioPerfilEntidadeReferenciaModel()
    autorModel.toggleEdit();
    autorRender.add(autorModel);
}

/**
 * Cria novo modelo e adiciona nova linha na lista de Coleções
 * @returns {void}
 */
function novaColecao() {
    renderColecoes = new CollectionView({
        collection: new LivroColecaoCollection,
        childViewConstructor: ColecaoLivroView,
        childViewTagName: 'tr',
        el: $('#tabela-colecao tbody')
    });
    renderColecoes._rendered = true;
    var colecaoLivroModel = new LivroColecaoModel;
    colecaoLivroModel.toggleEdit();
    renderColecoes.add(colecaoLivroModel);
}

/**
 * Salva vinculo de Professores com Livro
 * @returns {void}
 */
function salvaAutorLivro() {
    var idLivro = parseInt($('#id').val());
    var idPerfil = parseInt($('#id_perfil').val());
    $("#tableAutores tbody tr").each(function (i, child) {
        if ($(child).find('select[name="id_usuario"]').val() && idLivro) {
            var modeloUsuario = new UsuarioPerfilEntidadeReferencia();
            var bl_autor = $(child).find('input[name="bl_autor"]').is(':checked');
            modeloUsuario.set({
                id_perfilreferencia: $(child).find('input[name="id_perfilreferencia"]').val(),
                id_usuario: parseInt($(child).find('select[name="id_usuario"]').val()),
                id_perfil: idPerfil,
                id_livro: idLivro,
                bl_ativo: true,
                bl_autor: bl_autor
            });
            modeloUsuario.save(null, {
                success: function (model, response) {
                    $(child).find('input[name="id_perfilreferencia"]').val(response.id_perfilreferencia)
                    $.pnotify({title: 'Sucesso!', text: 'Professor/Autor vinculado com sucesso!', type: 'success'});
                },
                error: function (model, response) {
                    $.pnotify({title: 'Erro!', text: 'Houve um problema ao vincular Professor/Autor, tente novamente', type: 'error'});
                    if (response.responseText) {
                        $.pnotify({title: 'Atenção!', text: response.responseText, type: 'warning'});
                    }
                }
            });
        }
    });
}
/**
 * Salva vinculo de Livros com Entidades
 * @returns {void}
 */
function salvaOrganizacoes() {
    var idLivro = $('#id').val();
    var arrEntidade = new Array();
    $("#entidades").find("li.jstree-checked").each(function (i, entidade) {
        arrEntidade[i] = {
            id_entidade: parseInt(entidade.id)
        }
    });
    $.ajax({
        url: '/livro/salva-livro-entidade',
        dataType: 'json',
        type: 'post',
        data: {
            'data': arrEntidade,
            'id_livro': parseInt(idLivro)
        },
        success: function (data) {
            if (data) {
                $.pnotify(data);
            }
        }
    });
}
/**
 * Instancia Coleção do backbone para requisitar entidades referentes ao livro e marca como selecionado
 * @returns {void}
 */
function populaEntidades() {
    var idLivro = $('#id').val();
    var collectionLivroEntidade = new LivroEntidadeCollection();
    collectionLivroEntidade.url += 'id_livro/' + idLivro;
    collectionLivroEntidade.fetch({
        success: function (colecao) {
            $.each(colecao.models, function (i, model) {
                $("#entidades").find('li#' + model.get('id_entidade'))
                    .removeClass('jstree-unchecked').addClass('jstree-checked');
            });
        }
    });
}
/**
 * Verifica os campos do formulário e valida os campos
 * @param {Object} form
 * @returns {Boolean}
 */
function validaForm(form) {
    var retorno = true;
    //Percorre os campos do formulário com atributo required
    $(form).find('*[required="required"]').each(function (i, field) {
        var label = $(field).prev('label').html();//Procura a Label do campo e atribui a variavel
        var valor = $(field).val();//Pega o valor do campo
        //Verifica se o não tem valor
        if (!valor) {
            //Mostra mensagem de campo obrigatório
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Campo <b>' + label + '</b> é de preenchimento obrigatório.'
            });
            return false; //atribui o valor false a variavel de retorno
        }
    });

    var arrEntidade = new Array();
    $("#entidades").find("li.jstree-checked").each(function (i, entidade) {
        arrEntidade[i] = {
            id_entidade: parseInt(entidade.id)
        }
    });
    if(arrEntidade.length){
        retorno = true;
    }else{
        $.pnotify({
            type: 'warning',
            title: 'Atenção!',
            text: 'Selecione pelomenos uma organização.'
        });
        return false;
    }

    //Percorre a tabela dos autores e verifica se tem pelo menos um autor selecionado
//    $("#tableAutores tbody tr").each(function(i, child) {
//        var idUsuario = $(child).find('select[name="id_usuario"]').val();
//        if (i === 0) {
//            if (typeof (idUsuario) !== 'undefined') {
//                //Mostra mensagem de campo obrigatório
//                if (!idUsuario) {
//                    $.pnotify({
//                        type: 'warning',
//                        title: 'Atenção!',
//                        text: 'Pelo menos um Professor/Autor deve ser vinculado ao Livro.'
//                    });
//                    retorno = false; //atribui o valor false a variavel de retorno
//                }
//            }
//        }
//    });
//    if (typeof ($("#tableAutores tbody tr").html()) == 'undefined') {
//        retorno = false;
//        $.pnotify({
//            type: 'warning',
//            title: 'Atenção!',
//            text: 'Pelo menos um Professor/Autor deve ser vinculado ao Livro.'
//        });
//    }
    return retorno;
}


/*
 * Renderiza bibliotecas JS depois do carregamento da DOM
 */
$(function () {
    $("#nu_peso").spinner({
        step: 1,
        min: 0,
        max: 9999999999
    });
    $("#nu_pagina").spinner({
        step: 1,
        min: 20,
        max: 99999
    });

    $('*[data-toggle]').tooltip();

    $("#entidades").jstree({
        "html_data": {
            "ajax": {
                "url": "util/arvore-entidade",
                "data": function (n) {
                    if (n.attr)
                        return {id: n.attr ? n.attr("id") : null};
                },
                success: function () {
                    populaEntidades();
                }
            }
        },
        "plugins": ["themes", "html_data", "checkbox", "sort", "ui"]
    }).bind();


    /**
     * Rendereiza o template do cadastro do livro
     * @type CollectionView
     */
    var renderTemplateLivro = new CollectionView({
        collection: new LivroCollection,
        childViewConstructor: LivroView,
        childViewTagName: 'div',
        el: $('#containerLivro')
    });
    renderTemplateLivro._rendered = true;
    /**
     * Instancia Model de livro
     * @type Livro modelLivro
     */
    var modelLivro = new Livro();
    renderTemplateLivro.add(modelLivro);


    /**
     * @type VwUsuarioPerfilEntidadeReferenciaCollection Collection Backbone
     */
    var collectionAutor = new VwUsuarioPerfilEntidadeReferenciaCollection();
    if ($('#id').val()) {
        collectionAutor.url += '/id_livro/' + $('#id').val() + '/id_perfil/' + $('#id_perfil').val();
        collectionAutor.fetch({
            success: function () {
                new CollectionView({
                    collection: collectionAutor,
                    childViewConstructor: AutoresView,
                    childViewTagName: 'tr',
                    el: $('#tableAutores tbody')
                }).render();
            }
        });
    } else {
        novoAutor();
    }


    $('#formLivro').submit(function () {
        if (validaForm(this)) {
            var livro = new Livro();
            livro.set({
                id_livro: $('#id').val(),
                id_situacao: $('#id_situacao').val(),
                id_tipolivro: $('#id_tipolivro').val(),
                id_livrocolecao: $('#id_livrocolecao').val(),
                st_livro: $('#st_livro').val(),
                st_isbn: $('#st_isbn').val(),
                st_codigocontrole: $('#st_codigocontrole').val(),
                st_edicao: $('#st_edicao').val(),
                nu_anolancamento: $('#nu_anolancamento').val(),
                nu_pagina: $('#nu_pagina').val(),
                nu_peso: $('#nu_peso').val(),
                bl_ativo: true
            });
            livro.save(null, {
                success: function (model, response) {
                    $("#id").val(response.id);
                    salvaAutorLivro();//Chama função para salvar autor
                    salvaOrganizacoes();//Salva vinculo com entidades
                    $.pnotify({title: 'Sucesso!', text: 'Livro salvo com sucesso!', type: 'success'});
                },
                error: function (model, response) {
                    $.pnotify({title: 'Erro!', text: 'Houve um problema ao salvar dados do Livro, tente novamente', type: 'error'});
                    if (response.responseText) {
                        $.pnotify({title: 'Atenção!', text: response.responseText, type: 'warning'});
                    }
                }
            });
        }
        return false;
    });

loaded();
});
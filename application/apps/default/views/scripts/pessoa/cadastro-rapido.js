var thatViewFormulario = '';
var sessionCEP = 'st_cep';

var CadastroModel = Backbone.Model.extend({
    defaults: {
        id: '',
        st_cpf: '',
        id_pais: '',
        st_nomecompleto: '',
        st_sexo: '',
        st_nomemae: '',
        st_nomepai: '',
        st_rg: '',
        st_orgaoexpeditor: '',
        dt_dataexpedicao: '',
        id_informacaoacademicapessoa: '',
        id_nivelensino: '',
        st_curso: '',
        dt_nascimento: '',
        st_nomeinstituicao: '',
        st_login: '',
        st_senha: '',
        id_registropessoa: '',
        st_urlavatar: '',
        id_endereco: '',
        id_paisendereco: '',
        sg_uf: '',
        st_cep: '',
        id_municipio: '',
        id_tipoendereco: '',
        st_endereco: '',
        st_bairro: '',
        st_cidade: '',
        st_complemento: '',
        nu_numero: '',
        id_email: '',
        st_email: '',
        id_telefone: '',
        id_tipotelefone: '',
        nu_ddi: '',
        nu_ddd: '',
        nu_telefone: '',
        id_telefone2: '',
        id_tipotelefone2: '',
        nu_ddi2: '',
        nu_ddd2: '',
        nu_telefone2: '',
        st_identificacao: '',
        id_titulacao: '',
        id_endereco_corresp: '',
        id_pais_corresp: '',
        st_cep_corresp: '',
        sg_uf_corresp: '',
        id_municipio_corresp: '',
        st_endereco_corresp: '',
        id_tipoendereco_corresp: '',
        st_bairro_corresp: '',
        nu_numero_corresp: '',
        st_complemento_corresp: '',
        st_passaporte: '',
        st_cpfResponsavelLegal: '',
        st_nomeResponsavelLegal: '',
        nu_dddResponsavelLegal: '',
        nu_telefoneResponsavelLegal: '',
        st_cnpj: '',
        st_agencia: '',
        st_conta: '',
        st_certificadoreservista: '',
        st_tituloeleitor: '',
        id_municipionascimento: '',
        sg_ufnascimento: '',
        id_categoriaservicomilitar:'',
        st_secaoeleitoral:'',
        st_zonaeleitoral:'',
        st_municipioeleitoral:'',
        dt_expedicaocertificadoreservista:'',
        st_reparticaoexpedidora:''
    }
});

var UsuarioCollection = Backbone.Collection.extend({
    url: '/pessoa/verifica-usuario/',
    model: Usuario
});
var PessoaCollection = Backbone.Collection.extend({
    url: '/api/pessoa',
    model: Pessoa
});
var PessoaEnderecoCollection = Backbone.Collection.extend({
    url: '/pessoa/get-endereco-principal-pessoa',
    model: PessoaEndereco
});
var MunicipioCollection = Backbone.Collection.extend({
    url: '/pessoa/get-municipio-by-uf',
    model: Municipio
});
var PessoaTelefoneCollection = Backbone.Collection.extend({
    url: '/pessoa/get-telefone-pessoa',
    model: VwPessoaTelefone
});
var PessoaEmailCollection = Backbone.Collection.extend({
    url: '/pessoa/get-email-pessoa',
    model: VwPessoaEmail
});
var DocumentoIdentidadeCollection = Backbone.Collection.extend({
    url: '/api/documento-identidade',
    model: DocumentoIdentidade
});

var InformacaoAcademicaPessoaCollection = Backbone.Collection.extend({
    url: '/api/informacao-academica-pessoa',
    model: InformacaoAcademicaPessoa
});

/**
 * @type View para setar os dados de endereço no formulário
 * @exp;Backbone
 * @pro;View
 * @call;extend
 */
var ViewSetDadosEndereco = Backbone.View.extend({
    tagName: "",
    className: "",
    initialize: function () {
        this.render = _.bind(this.render, this);
        this.model.bind('change', this.render);
    },
    render: function () {
        var sufix = sessionCEP.replace('st_cep', '');

        $("#sg_uf" + sufix).val(this.model.get('sg_uf')).trigger('change');
        $("#st_cep" + sufix).val(this.model.get('st_cep'));
        $("#st_endereco" + sufix).val(this.model.get('st_endereco'));
        $("#st_cidade" + sufix).val(this.model.get('st_cidade'));
        $("#nu_numero" + sufix).val(this.model.get('nu_numero'));
        $("#st_bairro" + sufix).val(this.model.get('st_bairro'));
        $("#st_complemento" + sufix).val(this.model.get('st_complemento'));
        $("#id_municipio" + sufix).val(this.model.get('id_municipio'));
        $("#id_municipiohidden" + sufix).val(this.model.get('id_municipio'));
        $("#id_endereco" + sufix).val(this.model.get('id_endereco'));

        if (!sessionCEP) {
            $("#sg_uf_corresp").val(this.model.get('sg_uf_corresp')).trigger('change');
            $("#st_cep_corresp").val(this.model.get('st_cep_corresp'));
            $("#st_endereco_corresp").val(this.model.get('st_endereco_corresp'));
            $("#st_cidade_corresp").val(this.model.get('st_cidade_corresp'));
            $("#nu_numero_corresp").val(this.model.get('nu_numero_corresp'));
            $("#st_bairro_corresp").val(this.model.get('st_bairro_corresp'));
            $("#st_complemento_corresp").val(this.model.get('st_complemento_corresp'));
            $("#id_municipio_corresp").val(this.model.get('id_municipio_corresp'));
            $("#id_municipiohidden_corresp").val(this.model.get('id_municipio_corresp'));
        }
        return this;
    }
});
/**
 *
 * @type View to Set Data for telefone
 * @exp;Backbone
 * @pro;View
 * @call;extend
 */
var ViewSetDadosTelefone = Backbone.View.extend({
    tagName: '',
    className: '',
    initialize: function () {
        this.render = _.bind(this.render, this);
        this.model.bind('change', this.render);
    },


    render: function () {
        if (this.model.get('bl_padrao')) {
            $("#id_telefone").val(this.model.get('id'));
            $("#id_tipotelefone").val(this.model.get('id_tipotelefone'));
            $("#nu_ddi").val(this.model.get('nu_ddi'));
            $("#nu_ddd").val(this.model.get('nu_ddd'));
            $("#nu_telefone").val(this.model.get('nu_telefone'));
        } else {
            $("#id_telefone2").val(this.model.get('id'));
            $("#id_tipotelefone2").val(this.model.get('id_tipotelefone'));
            $("#nu_ddi2").val(this.model.get('nu_ddi'));
            $("#nu_ddd2").val(this.model.get('nu_ddd'));
            $("#nu_telefone2").val(this.model.get('nu_telefone'));
        }
        return this;
    }
});
/**
 * @type View to Set data for email
 * @exp;Backbone
 * @pro;View
 * @call;extend
 */
var ViewSetDadosEmail = Backbone.View.extend({
    tagName: '',
    className: '',
    initialize: function () {
        this.render = _.bind(this.render, this);
        this.model.bind('change', this.render);
    },
    render: function () {
        if (this.model.get('bl_padrao')) {
//            $("#id_email").val(this.model.get('id'));
            $("#st_email").val(this.model.get('st_email'));
        }
        return this;
    }
});
/**
 * @type View to Select Municipio
 * @exp;Backbone
 * @pro;View
 * @call;extend
 */

var ViewSetMunicipioSelect = Backbone.View.extend({
    tagName: 'option',
    initialize: function () {
        this.render = _.bind(this.render, this);
        this.model.bind('change', this.render);
    },
    render: function () {
        if (this.model.get('id_municipio')) {
            $(this.el).attr('value', this.model.get('id_municipio')).text(this.model.get('st_nomemunicipio'));
            if (this.model.collection.ref_municipio.val() == this.model.get('id_municipio')) {
                $(this.el).attr('selected', 'selected');
            }
        } else {
            $(this.el).attr('value', '').text("Selecione o UF");
        }
        return this;
    }
});

var ViewRespLegal = Marionette.ItemView.extend({
    template: '#template-responsavel-legal',
});

/**
 * @type View For motal webcam @exp;Backbone@pro;View@call;extend
 */
var ViewModalWebCam = Backbone.View.extend({
    click: 0,
    initialize: function () {
        this.$el.modal('show');
    },
    render: function () {
        this.el.innerHTML = _.template($("#modalCamTemplate").html(), {});
        webcamComponente.main({
            element: this.$el.find('#webcam')
        });
        return this;
    },
    capturaImagem: function (e) {
        var elemClick = e.currentTarget;
        this.click++;
        if (this.click > 1) {
            $(elemClick).html('<i class="icon-picture icon-white"></i> Capturar');
            $("#webcam").show();
            $("#canvas").hide();
            this.click = 0;
        } else {
            $(elemClick).text('Capturar Nova');
            webcam.capture();
        }
    },
    escreverNovaImagem: function () {
        var str_avatar = $("#str_avatar").val();
        if (str_avatar) {
            thatViewFormulario.$el.find('#frame-img').empty().hide();
            thatViewFormulario.$el.find('#img-captura-container').show().html('<img style="height:150px; width:200px;" src="' + str_avatar + '" class="img-polaroid">');
            thatViewFormulario.pegarAvatarUsuario();
        }
    },
    events: {
        'click #btnCaptureImg': 'capturaImagem',
        'click .fechar-modal-cam': 'escreverNovaImagem'
    }
});
/**
 * @type View to render model User
 * @exp;Backbone
 * @pro;View
 * @call;extend
 */
var UserView = Backbone.View.extend({
    tagName: "",
    className: "",
    initialize: function () {
        this.render = _.bind(this.render, this);
//        this.model.bind('change', this.render);
    },
    render: function () {
        var variables = this.model.toJSON();
        if (this.model.get('id_entidade') == G2S.loggedUser.get('id_entidade') || this.model.get('id_entidade') == null) {
            this.model.set('id_entidade', G2S.loggedUser.get('id_entidade'));
            this.getDadosUserCurrentEntidade();
            return this;
        }
        var elemTemplate = _.template($("#modalUserTemplate").html(), variables);
        this.el.innerHTML = elemTemplate; //renderiza o template
        $("#modalUser").modal('show');
        return this;
    },
    getDadosUserCurrentEntidade: function () {
        $("#id").val(this.model.get('id'));
        $("#st_nomecompleto").val(this.model.get('st_nomecompleto'));
        $("#st_login").val(this.model.get('st_login'));
        $("#dt_nascimento").val(this.model.get('dt_nascimento'));
//        $('#btnChangePass').show();
//        $("#fiels-senha").hide();
//        $('#btnChangePass').attr('title', 'Alterar Senha');
//        $('#btnChangePass').html("<i class='icon-edit'></i> Alterar Senha");
        this.getDadosPessoa();
        this.getDadosIdentidade();
        this.getDadosEscolaridade();
        this.getDadosEndereco();
        this.getDadosTelefonePessoa();
        this.getDadosEmailPessoa();
    },
    setDataUserImport: function () {
        that = this;
        $("#id").val(this.model.get('id'));
        $("#st_nomecompleto").val(this.model.get('st_nomecompleto'));
        $("#st_login").val(this.model.get('st_login'));
        $("#dt_nascimento").val(this.model.get('dt_nascimento'));
        this.getDadosPessoa();
        this.getDadosIdentidade();
        this.getDadosEscolaridade();
        this.getDadosEndereco();
        this.getDadosTelefonePessoa();
        this.getDadosEmailPessoa();
        this.model.url = '/pessoa/importar-usuario';
        //salva a model
        this.model.save(null, {
            //resposta da model
            success: function (model, response) {
                $.pnotify({title: response.title, text: response.text, type: response.type}); //mensagem de retorno
            },
            error: function (model, response) {
                $.pnotify({
                    title: 'Erro ao atualizar o registro',
                    text: 'Houve um problema ao importar o registro, tente novamente',
                    type: 'error'
                });
            }
        });
        $("#modalUser").modal('hide');
    },
    getDadosIdentidade: function () {
        var identidadeCollection = new DocumentoIdentidadeCollection();
        identidadeCollection.url += '/id_usuario/' + this.model.get('id')
        identidadeCollection.fetch({
            success: function (collection) {
                if (collection.length) {
                    collection.each(function (model, i) {
                        $("#st_rg").val(model.get('st_rg'));
                        $("#st_orgaoexpeditor").val(model.get('st_orgaoexpeditor'));
                        $("#dt_dataexpedicao").val(model.get('dt_dataexpedicao'));
                    });
                } else {
                    $("#st_rg").val('');
                    $("#st_orgaoexpeditor").val('');
                    $("#dt_dataexpedicao").val('');
                }
            }
        });
    },
    getDadosEscolaridade: function () {
        var informacaoAcademicaPessoaCollection = new InformacaoAcademicaPessoaCollection();
        informacaoAcademicaPessoaCollection.url += '/id_usuario/' + this.model.get('id');
        informacaoAcademicaPessoaCollection.fetch({
            success: function (collection) {
                if (collection.length) {
                    collection.each(function (model, i) {
                        $("#id_nivelensino").val(model.get('id_nivelensino'));
                        $("#st_curso").val(model.get('st_curso'));
                        $("#st_nomeinstituicao").val(model.get('st_nomeinstituicao'));
                        $("#id_informacaoacademicapessoa").val(model.get('id_informacaoacademicapessoa'));
                    });
                } else {
                    $("#id_nivelensino").val('');
                    $("#st_curso").val('');
                    $("#st_nomeinstituicao").val('');
                    $("#id_informacaoacademicapessoa").val('');
                }
            }
        });
    },
    getDadosPessoa: function () {
        var pessoa = new Pessoa();
        pessoa.url = '/pessoa/retornar-pessoa/id_usuario/' + this.model.get('id') + '/id_entidade/' + this.model.get('id_entidade');
        pessoa.fetch({
            success: function (model) {
                if (model) {
                    $("#st_nomemae").val(model.get('st_nomemae'));
                    $("#st_nomepai").val(model.get('st_nomepai'));
                    $("#id_nacionalidade").val(model.get('id_pais'));

                    if (model.get('st_sexo') == 'M') {
                        $("#check_masculino").attr('checked', 'checked');
                    } else {
                        $("#check_feminino").attr('checked', 'checked');
                    }
                }
            }
        });
    },
    getDadosEndereco: function () {
        sessionCEP = '';
        var pessoaEndereco = new PessoaEnderecoCollection;
        pessoaEndereco.url += '/id_usuario/' + this.model.get('id') + '/id_entidade/' + this.model.get('id_entidade');
        pessoaEndereco.fetch({
            success: function () {
                collectionRender = new CollectionView({
                    collection: pessoaEndereco,
                    childViewConstructor: ViewSetDadosEndereco,
                    childViewTagName: 'div',
                    el: $('#modalUser')
                });
                collectionRender.render();
            }
        });
    },
    getDadosTelefonePessoa: function () {
        var pessoaTel = new PessoaTelefoneCollection;
        pessoaTel.url += '/id_usuario/' + this.model.get('id') + '/id_entidade/' + this.model.get('id_entidade');
        pessoaTel.fetch({
            success: function () {
                collectionRender = new CollectionView({
                    collection: pessoaTel,
                    childViewConstructor: ViewSetDadosTelefone,
                    childViewTagName: 'div',
                    el: $('#modalUser')
                });
                collectionRender.render();
            }
        });
    },
    getDadosEmailPessoa: function () {
        var pessoaEmail = new PessoaEmailCollection;
        pessoaEmail.url += '/id_usuario/' + this.model.get('id') + '/id_entidade/' + this.model.get('id_entidade');
        pessoaEmail.fetch({
            success: function () {
                collectionRender = new CollectionView({
                    collection: pessoaEmail,
                    childViewConstructor: ViewSetDadosEmail,
                    childViewTagName: 'div',
                    el: $('#modalUser')
                });
                collectionRender.render();
            }
        });
    },
    events: {
        'click #btnImportUser': 'setDataUserImport',
        'click #selecionaSexo': 'habilitaCertificadoReservista'
    }
});
/**
 * Recupera dados pessoa by CPF
 * @params {void}
 * @returns {Boolean}
 */
function getPessoaByCpf() {
    var valid = true;
    var cpf = $("#st_cpf").val();
    if (!cpf) {
        $.pnotify({
            title: 'Impossível Pesquisar!',
            text: 'Impossível pesquisar por CPF. Preencha o campo de CPF corretamente.',
            type: 'warning'
        });
        valid = false;
    } else {
        loading();
        var userCollection = new UsuarioCollection();
        userCollection.url += 'st_cpf/' + cpf;
        userCollection.fetch({
            success: function () {
                collectionRender = new CollectionView({
                    collection: userCollection,
                    childViewConstructor: UserView,
                    childViewTagName: 'div',
                    el: $('#modalUser')
                });
                collectionRender.render();
            },
            complete: function () {
                if (userCollection.models[0] && userCollection.models[0].get('dt_nascimento')) {
                    var dateParts1 = (userCollection.models[0].get('dt_nascimento')).split('/');
                    if (calculaIdade(dateParts1[1], dateParts1[0], dateParts1[2]) < 18) {
                        var view = new ViewRespLegal({
                            el: $("#responsave-legal"),
                            model: userCollection.models[0]
                        });
                        view.render();
                        $('#responsave-legal').removeAttr('hidden');
                    }
                }
                loaded();
            },
            error: function (collection, response) {
                loaded();
                var mensagemJSON = JSON.parse(response.responseText);
                $.pnotify(mensagemJSON);
                valid = false;
            }
        });
        return valid;
    }
}


/**
 * Return endereço by CEP
 * @param {void}
 * @return {boolean}
 */
function getEnderecoByCep(e) {
    var elem = $(e.target).closest('fieldset').find('.st_cep_btn');
    var cep = elem.val();
    sessionCEP = elem.attr('name');

    if (!cep) {
        $.pnotify({
            title: 'Impossível Pesquisar!',
            text: 'Impossível pesquisar por CEP. Preencha o campo de CEP corretamente',
            type: 'warning'
        });
        return false;
    } else {
        loading();
        var pessoaEndereco = new PessoaEnderecoCollection;
        pessoaEndereco.url = '/pessoa/get-endereco-by-cep/st_cep/' + cep + "?primeiro=1";
        pessoaEndereco.fetch({
            success: function (response) {
                if (response.length) {
                    //seta o id_endereco para não salvar outro endereço sempre que buscar pelo cep
                    pessoaEndereco.at(0).set('id_endereco', modelFormulario.get('id_endereco'));
                    collectionRender = new CollectionView({
                        collection: pessoaEndereco,
                        childViewConstructor: ViewSetDadosEndereco,
                        childViewTagName: 'div',
                        el: $('#modalUser')
                    });
                    collectionRender.render();
                    loaded();
                } else {
                    $.pnotify({
                        'title': 'CEP Inválido!',
                        'text': 'O CEP digitado não foi encontrado. Tente novamente!',
                        'type': 'error'
                    });
                    loaded();
                }
            }
        });
        return true;
    }
}

/**
 * Get municipios by uf on event change
 * @param {object} element
 * @returns {void}
 */
function getMunicipiosByUf(ufEl, muniEl) {
    loading();
    var sg_uf = $(ufEl).val();
    var municipioCollection = new MunicipioCollection;
    municipioCollection.ref_municipio = $(muniEl).closest('fieldset').find('.ref_municipio');
    municipioCollection.url += '/sg_uf/' + sg_uf;
    municipioCollection.fetch({
        success: function () {
            loading();
            collectionRender = new CollectionView({
                collection: municipioCollection,
                childViewConstructor: ViewSetMunicipioSelect,
                childViewTagName: 'option',
                el: $(muniEl)
            });
            collectionRender.render();
        },
        complete: loaded
    });
}

/**
 * Toggle fields required secundary tel
 * @returns {void}
 */
function toggleRequiredSecundaryTel() {
    var id_tipotelefone = $("#id_tipotelefone2");
    var nu_ddi = $("#nu_ddi2");
    var nu_ddd = $("#nu_ddd2");
    var nu_telefone = $("#nu_telefone2");
    var concat = id_tipotelefone.val()
        + nu_ddi.val()
        + nu_ddd.val()
        + nu_telefone.val();
    if (concat !== '') {
        id_tipotelefone.attr('required', 'required');
        nu_ddi.attr('required', 'required');
        nu_ddd.attr('required', 'required');
        nu_telefone.attr('required', 'required');
    } else {
        id_tipotelefone.removeAttr('required');
        nu_ddi.removeAttr('required');
        nu_ddd.removeAttr('required');
        nu_telefone.removeAttr('required');
    }
}
//
function ativarProximaAba() {
    var abaAtiva = $('#tabCadastroPessoaJuridica > li.active');
    abaAtiva.next('li').removeClass('disabled').find('a').trigger('click');
}

var ViewFormulario = Backbone.View.extend({
    template: $('#template-formulario').html(),
    initialize: function (config) {
        thatViewFormulario = this;
        //verifica se o cadastro é originado da tela de negociação
        this.negociacao = config.negociacao ? config.negociacao : false;
    },
    render: function () {

        var that = this;

        /* Verifica se há permissão no perfil para cadastrar a descrição do coordenador.
         * Caso tenha, habilita a aba de descrição, obtém dados previamente
         * cadastrados e exibe-o no campo. */

        VerificarPerfilPermissao(36, 769, true).done(function(response){
            if (response.length){
                that.getDescricaoCoordenador();
                that.$el.find('#abadescricao').parent().removeClass('hidden');
            }
        });

        if (!this.model.get('st_urlavatar'))
            this.model.set('st_urlavatar', '/upload/usuario/usuario_foto_generica.jpg');

        VerificarPerfilPermissao(35, 8, true).done(function (response) {
            if (response.length == 1) {
                that.$el.find('#divTitulacao').removeClass('hide');
            } else {
                that.$el.find('#divTitulacao').addClass('hide', true);
            }
        });


        this.el.innerHTML = _.template(this.template, this.model.toJSON());

        if (this.model.id) {
            //seta o valor do input de sexo do usuário
            if (this.model.get('st_sexo') == 'M') {
                this.$el.find("#check_masculino").attr('checked', 'checked');
            } else {
                $("#check_feminino").attr('checked', 'checked');
                $("#st_certificadoreservista").prop('disabled', true).val('');
            }

            this.$el.find('#id_titulacao').val(this.model.get('id_titulacao'));
            this.$el.find('#id_nivelensino').val(this.model.get('id_nivelensino'));
            this.$el.find('#pais-endereco').find('#id_pais').val(this.model.get('id_paisendereco'));
            this.$el.find('#sg_uf').val(this.model.get('sg_uf'));
            this.$el.find('#sg_uf_corresp').val(this.model.get('sg_uf_corresp'));
            this.$el.find("#id_municipiohidden").val(this.model.get('id_municipio'));
            this.$el.find("#id_municipiohidden_corresp").val(this.model.get('id_municipio_corresp'));
            this.$el.find("#pais-user").find("#id_nacionalidade").val(this.model.get('id_pais'));
            this.$el.find('#id_tipotelefone').val(this.model.get('id_tipotelefone'));
            this.$el.find('#id_tipotelefone2').val(this.model.get('id_tipotelefone2'));
            this.$el.find('#sg_ufnascimento').val(this.model.get('sg_ufnascimento'));
            this.$el.find('#id_municipionascimentohidden').val(this.model.get('id_municipionascimento'));
            this.$el.find('#btnEnviarDados').show();
            this.$el.find('#btnListPerfilPessoa').show();
            this.$el.find('#id_categoriaservicomilitar').val(this.model.get('id_categoriaservicomilitar'));
            this.$el.find("#id_tiposanguineo").val(this.model.get("id_tiposanguineo"));
            this.$el.find("#sg_fatorrh").val(this.model.get("sg_fatorrh"));

            getMunicipiosByUf(this.$el.find('#sg_uf'), this.$el.find('#id_municipio'));
            getMunicipiosByUf(this.$el.find('#sg_uf_corresp'), this.$el.find('#id_municipio_corresp'));
            getMunicipiosByUf(this.$el.find('#sg_ufnascimento'), this.$el.find('#id_municipionascimento'));
            this.$el.find('#libancario').removeClass("disabled");
            this.$el.find('#ababancario').css("pointer-events", "auto");
            this.getDadosBancarios();

            //Remove o atributo required do campo senha quando for uma edição
            //Atualizado: atributo nao eh mais required porque o sistema agora pode gerar a senha caso o ususario nao a digite(TEC-397)
            //this.$el.find("#st_senha").removeAttr('required');
        } else {

            this.btnChangePass();
            this.$el.find('#btnChangePass').hide();
        }

        this.$el.find('#str_avatar').val('');

        this.$el.find("#st_cnpj").mask('99.999.999/9999-99');
        ;
        this.$el.find("#st_cpf").mask('999.999.999-99');
        this.$el.find("#st_cep,#st_cep_corresp").mask('99.999-999');

        //this.$el.find('#id_banco').select2();

        // Definir campos senhas como TEXTO para
        // navegadores que fazem autocomplete da senha,
        // mesmo quando  tenha usado o atributo autocomplete="off"
        this.$el.find("#st_senha, #st_confirmarsenha").attr('type', 'text');
        return this;
    },
    onShow: function () {

        var dateParts1 = ($('#dt_nascimento').val()).split('/');
        if (calculaIdade(dateParts1[1], dateParts1[0], dateParts1[2]) < 18) {
            var view = new ViewRespLegal({
                el: this.$el.find("#responsave-legal"),
                model: this.model
            })
            view.render();
        }

        this.$el.find('#id_banco').select2();

        return this;
    },
    getPessoaByCpf: getPessoaByCpf,
    btnChangePass: function () {
        var btn = this.$el.find('#btnChangePass');
        this.$el.find("#fiels-senha").toggle('fast', function () {
            var text = btn.attr('title');
            if (text === 'Cancelar') {
                btn.attr('title', 'Alterar Senha');
                btn.html("<i class='icon-edit'></i> Alterar Senha");
            } else {
                btn.attr('title', 'Cancelar');
                btn.html("<i class='icon-remove'></i> Cancelar");
            }
        });
    },
    getDadosCep: function (e) {
        getEnderecoByCep(e);
    },
    getMunicipiosByUf: function (e) {
        var elem = $(e.target);
        var mEl = '#' + elem.attr("id").replace('sg_uf', 'id_municipio');
        getMunicipiosByUf(elem, mEl);
    },
    toogleFieldsRequired: function () {
        toggleRequiredSecundaryTel();
    },
    showCamModal: function () {
        var camView = new ViewModalWebCam({
            el: this.$el.find("#modalCam")
        });
        camView.render();
    },
    verificaCamposSenha: function () {
        var st_senha = this.$el.find("#st_senha").val();
        var st_confirmasenha = this.$el.find("#st_confirmarsenha").val();
        var st_senhapadrao = this.$el.find('#senhapadrao').val();
        var title = 'Erro Preenchimento!';
        var text = 'Campo de Senha e Confirmar Senha devem ser iguais.';
        var type = 'alert';
        if (st_senha == " " && st_confirmasenha == " ") {
            st_senha = st_senhapadrao;
            $.pnotify({
                title: "Senha do Sistema",
                text: "Senha Criada pelo sistema!",
                type: type
            });
            return true;
        } else if (st_senha && st_confirmasenha) {
            if (st_senha !== st_confirmasenha) {
                $.pnotify({
                    title: title,
                    text: text,
                    type: type
                });
                return false;
            }
        } else {
            if (st_senha || st_confirmasenha) {
                $.pnotify({
                    title: title,
                    text: text,
                    type: type
                });
                return false;
            }
        }
        return true;
    },
    varificaCampoEmail: function () {
        var st_email = this.$el.find('#st_email').val();
        var st_confirmaremail = this.$el.find('#st_confirmaremail').val();
        var retorno = true;
        if (this.model.id && this.model.get('st_email') != st_email) {
            if (st_email != st_confirmaremail) {
                $.pnotify({
                    title: 'Erro Preenchimento!',
                    text: 'Campo de E-mail e Confirmar E-mail devem ser iguais.',
                    type: 'alert'
                });
                retorno = false;
            }
        }
        return retorno;
    },
    validaForm: function () {
        var valid = true;
        $('#st_login').val($('#st_login').val().toLowerCase().removeAccents().removerCharEspecial());
        //percorre os elementos procurando os que estiverem o atributo required
        $.each(this.$el.find('#formCadastroRapido').find('*[required="required"]'), function (i, elem) {
            var value = $(elem).val();
            if (!value) {
                $(elem).css('border', '1px solid red');
                valid = false;
                $.pnotify({
                    title: 'Atenção!',
                    text: 'Preencha o formulário corretamente! O campo ' + $(elem).attr('title') + ' é de preenchimento obrigatório.',
                    type: 'alert'
                });
            }
        });

        //verifica se os campos de senha estão corretos
        if (!this.verificaCamposSenha()) {
            valid = false;
        }

        if (!this.varificaCampoEmail()) {
            valid = false;
        }

        if (!this.validaCodigoCarteirinha()) {
            valid = false;
        }

        if (!this.validaContaBancaria()) {
            valid = false;
        }

        if (!this.validaDocumentoIdentidade()) {
            valid = false;
        }

        if (!this.validaEscolaridade()) {
            valid = false;
        }
        if (!this.validaEndereco()) {
            valid = false;
        }

        if (!this.validaTelefone()) {
            valid = false;
        }

        return valid;

    },

    getDescricaoCoordenador: function() {
        var that = this;
        var DescricaoCoordenadorModel = new Pessoa();
        DescricaoCoordenadorModel.url = '/pessoa/get-descricao-coordenador';
        DescricaoCoordenadorModel.fetch({
            data: $.param({
                'id_usuario': that.model.get('id'),
                'id_entidade': G2S.loggedUser.get('id_entidade')
            }),
            success: function() {
                that.$el.find('#st_descricaocoordenador').html(DescricaoCoordenadorModel.get('st_descricaocoordenador'));
            },
            error: function(){
                $.pnotify({
                    type: 'error',
                    title: 'Erro!',
                    text: 'Não foi possível obter os dados da descrição do coordenador.'
                })
            }
        });
    },

    getDadosBancarios: function () {
        var that = this;
        var DadosBancarios = new PessoaCollection();
        DadosBancarios.url = '/pessoa/get-conta-pessoa/id_usuario/' + this.model.get('id');
        DadosBancarios.fetch({
            success: function (model) {
                if (DadosBancarios) {
                    var model = DadosBancarios;
                    that.$el.find('#id_banco').val(model.get('st_banco')).trigger("change");
                    that.$el.find('#st_agencia').val(model.get('st_agencia'));
                    that.$el.find('#st_digitoagencia').val(model.get('st_digitoagencia'));
                    that.$el.find('#st_conta').val(model.get('st_conta'));
                    that.$el.find('#st_digitoconta').val(model.get('st_digitoconta'));
                    that.$el.find('#st_cnpj').val(model.get('nu_cnpj'));

                    if (model.get('id_tipopessoa') == '1') {
                        $("#id_tipoContaPessoaFisica").attr('checked', 'checked');
                    } else if (model.get('id_tipopessoa') == '2') {
                        $("#id_tipoContaPessoaJuridica").attr('checked', 'checked');
                    }

                    if (model.get('st_cnpj') != "undefined") {
                        $("#st_cnpj").removeAttr('disabled');
                    }

                }
            }
        });
    },
    submitForm: function (e) {
        var that = this;
        var dataForm = this.$el.find("#formCadastroRapido").serialize(); // atribui a variavel o serializa do formulario
        if (this.validaForm()) {
            //submit
            this.$el.find('#formCadastroRapido').ajaxSubmit({
                url: '/pessoa/salva-cadastro-resumido',
                type: 'post',
                dataType: 'json',
                beforeSend: loading,
                complete: loaded,
                success: function (data) {
                    if (data.type == 'success') {
                        thatViewFormulario.model.set(data.data);
                        thatViewFormulario.render();
                        var dateParts1 = (data.data.dt_nascimento).split('/');
                        if (calculaIdade(dateParts1[1], dateParts1[0], dateParts1[2]) < 18) {
                            var view = new ViewRespLegal({
                                el: $("#responsave-legal"),
                                model: thatViewFormulario.model
                            })
                            view.render();
                            $('#responsave-legal').removeAttr('hidden');
                        }

                        //verifica se o cadastro é originado da tela de negociação
                        if (that.negociacao) {
                            telaVendas.ocultarFormulario();
                        } else {
                            ativarProximaAba();
                        }

                    }
                    $.pnotify(data);

                    // Avisa caso o email de alteracao de dados nao tiver sido enviado
                    if (data.enviaEmail && data.enviaEmail.type != 'success') {
                        $.pnotify({
                            type: 'warning',
                            title: 'Aviso',
                            text: 'Não foi possível enviar o e-mail de alteração de dados para o usuário.'
                        });
                    }

                }
            });
        }
        return false;
    },
    submitFormDadosBancarios: function (e) {
        var dataForm = this.$el.find("#formCadastroRapidoDadosBancarios").serialize(); // atribui a variavel o serializa do formulario
        if (this.validaForm()) {
            //submit
            this.$el.find('#formCadastroRapidoDadosBancarios').ajaxSubmit({
                url: '/pessoa/salva-cadastro-resumido-dados-bancarios',
                type: 'post',
                dataType: 'json',
                beforeSend: loading,
                complete: loaded,
                success: function (data) {
                    $.pnotify(data);
                }
            });
        }

        return false;
    },
    submitFormDescricao: function() {
        if (this.validaForm()) {
            var $formCadastroRapidoDescricao = this.$el.find('#formCadastroRapidoDescricao');
            //Seta o id_entidade no input hidden '#id_entidade' do template.
            $formCadastroRapidoDescricao.find('#id_entidade').val(G2S.loggedUser.get('id_entidade'));
            $formCadastroRapidoDescricao
                .ajaxSubmit({
                    url: '/pessoa/salva-descricao-coordenador',
                    type: 'post',
                    dataType: 'json',
                    beforeSend: loading,
                    complete: loaded,
                    success: function (data) {
                        $.pnotify(data);
                    }
                })
        }
        return false;
    },
    carregaImagemAvatar: function (imagem) {
        thatViewFormulario.$el.find('#img-captura-container').empty().hide();
        thatViewFormulario.$el.find('#frame-img').show().attr('src', '/pessoa/get-img-avatar-usuario/?img=' + imagem);
    },
    validaCpf: function (e) {
        if (this.disabledKeys(e)) {
            var cpf = $(e.currentTarget).val();
            var cpfClean = cpf.replace(/[^0-9]/g, "");
            if (cpfClean.length === 11) {
                if (!validaCPF(cpf)) {
                    $.pnotify({
                        title: 'CPF inválido!',
                        text: 'CPF inválido! Digite o CPF corretamente.',
                        type: 'warning'
                    });
                    $(e.currentTarget).addClass('invalid-input');
                    return false;
                } else {
                    if (e.currentTarget.id != 'st_cpfResponsavelLegal')
                        this.getPessoaByCpf();
                    $(e.currentTarget).removeClass('invalid-input');
                    return true;
                }
            }
        }
    },
    validaCnpj: function (e) {
        var cnpj = $(e.currentTarget).val();
        var cnpjClean = cnpj.replace(/[^0-9]/g, "");
        if (cnpjClean.length === 14) {
            if (!validaCnpj(cnpj)) {
                $.pnotify({
                    title: 'CNPJ inválido!',
                    text: 'Digite novamente o Cnpj corretamente.',
                    type: 'warning'
                });
                $(e.currentTarget).addClass('invalid-input');
                return false;
            }
        }
    },
    verificaTipoPessoa: function (e) {
        var id_tipopessoa = this.$el.find("#id_tipoContaPessoaJuridica").is(':checked');
        if (id_tipopessoa) {
            this.$el.find("#st_cnpj").removeAttr('disabled');
        } else {
            this.$el.find("#st_cnpj").attr('disabled', true);
        }
    },
    disabledKeys: function (key) {
        //numeros teclas código 48-57 e 96-105 e 86
        if ((key.keyCode >= 48 && key.keyCode <= 57) || (key.keyCode >= 96 && key.keyCode <= 105) || key.keyCode == 86 || key.keyCode == 8 || key.keyCode == 46) {
            return true;
        }
        return false;
    },
    enviarDadosAcesso: function () {
        var that = this;
        //verifica se o id do usuario foi setado
        if (!this.model.id) {
            $.pnotify({
                title: 'Usuário Indefinido!',
                text: 'Impossível enviar dados de acesso para usuário. Id do usuário não encontrado.',
                type: 'warning'
            });
            this.$el.find('#btnEnviarDados').hide();
            return false;
        } else {
            $.ajax({
                dataType: 'json',
                type: 'get',
                async: false,
                url: '/pessoa/enviar-dados-acesso',
                data: {
                    id: that.model.id
                },
                beforeSend: function () {
                    loading();
                },
                success: function (data) {
                    $.pnotify(data);
                },
                complete: function () {
                    loaded();
                },
                error: function (response, data) {
                    var responseJson = JSON.parse(response.responseText);
                    $.pnotify(responseJson);
                }
            });
        }
    },
    listarPerfil: function () {
        loading();
        $('#div-perfil-usuario').html('');

        var columns = [
            {
                name: "st_nomeentidade",
                label: "Entidade",
                editable: false,
                cell: "string"
            },
            {
                name: "st_nomeperfil",
                label: "Perfil",
                editable: false,
                cell: "string"
            },
            {
                name: "dt_inicio",
                label: "Data de Início",
                editable: false,
                cell: "string"
            },
            {
                name: "st_notatcc",
                label: "Data de Término",
                editable: false,
                cell: "string"
            },
            {
                name: "st_situacaoperfil",
                label: "Situação Perfil",
                editable: false,
                cell: "string"
            },
            {
                name: "dt_cadastro",
                label: "Data Criação",
                editable: false,
                cell: "string"
            }
//                ,
//                {
//                    name: "st_responsavel",
//                    label: "Responsável Atribuição",
//                    editable: false,
//                    cell: "string"
//                },
//                {
//                    name: "st_sistema",
//                    label: "Sistema",
//                    editable: false,
//                    cell: "string"
//                }
        ];

        var VwListarPerfilUsuario = Backbone.Model.extend({});
        var DataGrid = Backbone.PageableCollection.extend({
            model: VwListarPerfilUsuario,
            url: "/",
            state: {
                pageSize: 15
            },
            mode: "client"
        });
        // recupara a url e os parametros passados
        var url = '/pessoa/get-vw-perfil-pessoa';
        var dataResponse;
        $.ajax({
            dataType: 'json',
            type: 'get',
            url: url,
            data: {id_usuario: this.model.get('id')},
            beforeSend: function () {
                //loading();
            },
            success: function (data) {
                dataResponse = data;
            },
            complete: function () {

                var dataGrid = new DataGrid(dataResponse);

                $("#div-perfil-usuario").html(''); //limpa o texto de carregando do container da grid
                //verifica se o json retornou algum resultado
                if (dataGrid.length > 0) {
                    //monta a grid
                    var pageableGrid = new Backgrid.Grid({
                        columns: columns,
                        collection: dataGrid,
                        className: 'backgrid backgrid-selectall table table-bordered table-hover table_imcube table-striped'
                    });
                    //renderiza o grid
                    $("#div-perfil-usuario").append(pageableGrid.render().$el);
                    //configura o paginator
                    var paginator = new Backgrid.Extension.Paginator({
                        collection: dataGrid
                    });
                    //renderiza o paginator
                    $("#div-perfil-usuario").append(paginator.render().$el);


                } else {
                    $("#div-perfil-usuario").html('Nenhum perfil encontrado para o usuário nessa instituição!');
                }
                $('#modal-perfil-usuario').modal('show');
                loaded();
            }
        });
    },
    passwordTypeToggle: function () {
        var checked = this.$el.find('#passwordType').prop('checked');
        if (checked) {
            this.$el.find("#st_senha, #st_confirmarsenha").attr('type', 'password');
        } else {
            this.$el.find("#st_senha, #st_confirmarsenha").attr('type', 'text');
        }

    },
    pegarAvatarUsuario: function () {
        $.pnotify({
            type: 'warning',
            title: 'Atenção!',
            text: 'Clique em Salvar para confirmar o envio da imagem.'
        });
        $('body,html').animate({
            scrollTop: this.$el.find('button[type="submit"]').offset().top
        }, 800);
    },
    validaContaBancaria: function () {
        var valid = true;
        var st_cnpj = this.$el.find("#st_cnpj").val();
        var id_banco = this.$el.find("#id_banco").val();
        var st_conta = this.$el.find("#st_conta").val();
        var st_agencia = this.$el.find("#st_agencia").val();
        var st_digitoconta = this.$el.find("#st_digitoconta").val();
        var st_digitoagencia = this.$el.find("#st_digitoagencia").val();
        var id_tipoContaPessoaFisica = this.$el.find("#id_tipoContaPessoaFisica").is(':checked');
        var id_tipoContaPessoaJuridica = this.$el.find("#id_tipoContaPessoaJuridica").is(':checked');

        if (st_cnpj && !id_tipoContaPessoaJuridica) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Selecione o Tipo de Conta Jurídica!'
            });
            valid = false;
        }
        if (id_tipoContaPessoaJuridica && !st_cnpj) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Digite o Cnpj!'
            });
            valid = false;
        }

        if (id_tipoContaPessoaFisica && !id_banco) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Selecione o banco'
            });
            valid = false;
        } else if (id_tipoContaPessoaFisica && !st_agencia) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Preencha os dados da agência'
            });
            valid = false;
        } else if (id_tipoContaPessoaFisica && !st_conta) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Preencha os dados da Conta'
            });
            valid = false;
        }

        if (id_tipoContaPessoaJuridica && !id_banco) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Selecione o banco'
            });
            valid = false;
        } else if (id_tipoContaPessoaJuridica && !st_agencia) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Preencha os dados da agência'
            });
            valid = false;
        } else if (id_tipoContaPessoaJuridica && !st_conta) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Preencha os dados da Conta'
            });
            valid = false;
        }

        return valid;
    },
    validaCodigoCarteirinha: function () {
        var valid = true;
        var st_codigo = this.$el.find("#st_identificacao").val();

        if (st_codigo) {
            if (!validaNumero(st_codigo)) {
                $.pnotify({
                    type: 'warning',
                    title: 'Atenção!',
                    text: 'O Cód. de Identificação deve ser somente números.'
                });
                valid = false;
            }

            if (st_codigo.length > 12) {
                $.pnotify({
                    type: 'warning',
                    title: 'Atenção!',
                    text: 'O Cód. de Identificação deve conter somente 12 caracteres.'
                });
                valid = false;
            }
        }
        return valid;
    },
    validaDocumentoIdentidade: function () {
        var valid = true;
        var st_rg = this.$el.find("#st_rg").val();
        var st_orgaoexpeditor = this.$el.find("#st_orgaoexpeditor").val();
        var dt_dataexpedicao = this.$el.find("#dt_dataexpedicao").val();

        if (!st_rg || !st_orgaoexpeditor || !dt_dataexpedicao) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Para cadastrar os dados do Documento de Identidade é necessário cadastrar o Nº do Documento, Orgão de Expedição e Data de Emissão!'
            });
            valid = false;
        }

        return valid;
    },
    validaEscolaridade: function () {
        var valid = true;
        var id_nivelensino = this.$el.find("#id_nivelensino").val();
        var st_curso = this.$el.find("#st_curso").val();
        var st_nomeinstituicao = this.$el.find("#st_nomeinstituicao").val();
        var id_titulacao = this.$el.find("#id_titulacao").val();

        if (!id_nivelensino && (st_curso || st_nomeinstituicao || id_titulacao)) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Para cadastrar os dados de escolaridade é necessário cadastrar o Grau de Instrução!'
            });
            valid = false;
        }

        return valid;
    },
    validaLogin: function (e) {
        $('#st_login').val($('#st_login').val().toLowerCase().removeAccents().removerCharEspecial());
    },
    validaEndereco: function (e)
    {
        var valid = true;
        var st_bairro = this.$el.find("#st_bairro").val();
        var st_bairroc = this.$el.find("#st_bairro_corresp").val();
        var st_endereco = this.$el.find("#st_endereco").val();
        var st_endereco_c = this.$el.find("#st_endereco_corresp").val();
        var nu_numero = this.$el.find("#nu_numero").val();
        var nu_numero_corresp = this.$el.find("#nu_numero_corresp").val();
        if(st_bairro.length > 100 || st_bairroc.length > 100)
        {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'O campo Bairro deverá ter no máximo 100 caracteres!'
            });
            valid = false;
        }
        if(st_endereco.length>100 || st_endereco_c.length > 100)
        {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'O campo Endereço deverá ter no máximo 100 caracteres!'
            });
            valid = false;
        }
        if(nu_numero.length>8 || nu_numero_corresp.length>100)
        {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'O campo Número deverá ter no máximo 8 caracteres!'
            });
            valid = false;
        }
        return valid;
    },
    events: {
        'click #getPessoaByCpf': 'getPessoaByCpf',
        'click #btnChangePass': 'btnChangePass',
        'click .btnCep': 'getDadosCep',
        'change #sg_uf': 'getMunicipiosByUf',
        'change #sg_uf_corresp': 'getMunicipiosByUf',
        'change #sg_ufnascimento': 'getMunicipiosByUf',
        'change #id_tipotelefone2,#nu_ddi2,#nu_ddd2,#nu_telefone2': 'toogleFieldsRequired',
        'click #btnModalCam': 'showCamModal',
        'submit #formCadastroRapido': 'submitForm',
        'submit #formCadastroRapidoDadosBancarios': 'submitFormDadosBancarios',
        'submit #formCadastroRapidoDescricao' : 'submitFormDescricao',
        'keyup #st_cpf': 'validaCpf',
        'blur #st_cnpj': 'validaCnpj',
        'keydown #st_cpf': 'disabledKeys',
        'click #btnEnviarDados': 'enviarDadosAcesso',
        'click #btnListPerfilPessoa': 'listarPerfil',
        'change #passwordType': 'passwordTypeToggle',
        'change #str_avatar': 'pegarAvatarUsuario',
        'change input#st_urlavatar': 'pegarAvatarUsuario',
        'change input[name="st_identificacao"]': 'validaCodigoCarteirinha',
        'keyup input[name="st_login"]': 'validaLogin',
        'click .copia-dados-endereco': 'copiaDadosEndereco',
        'change input[name="dt_nascimento"]': 'verificaIdade',
        'change input[name="id_tipopessoa"]': 'verificaTipoPessoa',
        'keyup #st_cpfResponsavelLegal': 'verificaCpf',
        'keydown #st_cpfResponsavelLegal': 'somenteNumeros',
        'keydown #nu_dddResponsavelLegal': 'somenteNumeros',
        'keydown #nu_telResponsavelLegal': 'somenteNumeros',
        'keydown #nu_ddi': 'somenteNumeros',
        'keydown #nu_ddd': 'somenteNumeros',
        'keydown #nu_telefone': 'somenteNumeros',
        'keydown #nu_ddi2': 'somenteNumeros',
        'keydown #nu_ddd2': 'somenteNumeros',
        'keydown #nu_telefone2': 'somenteNumeros',
        "keydown #st_certificadoreservista": "somenteNumeros",
        "keydown #st_tituloeleitor": "somenteNumeros",
        "keydown #st_zonaeleitoral": "somenteNumeros",
        "keydown #st_secaoeleitoral": "somenteNumeros",
        'blur #nu_ddi': 'defineTamanhoDDD',
        'blur #nu_ddi2': 'defineTamanhoDDD2',
        'click #selecionaSexo': 'habilitaCertificadoReservista',
        'change #id_tipotelefone': 'defineTamanhoTelefone',
        'change #id_tipotelefone2': 'defineTamanhoTelefone2'
    },
    habilitaCertificadoReservista: function () {
        if ($('#check_feminino').is(':checked')) {
            $("#st_certificadoreservista").prop('disabled', true);
            $("#dt_expedicaocertificadoreservista").prop('disabled', true);
            $("#id_categoriaservicomilitar").prop('disabled', true);
            $("#st_reparticaoexpedidora").prop('disabled', true);
        } else {
            $("#st_certificadoreservista").prop('disabled', false);
            $("#dt_expedicaocertificadoreservista").prop('disabled', false);
            $("#id_categoriaservicomilitar").prop('disabled', false);
            $("#st_reparticaoexpedidora").prop('disabled', false);
        }
    },
    verificaCpf: function (e) {
        this.validaCpf(e);
    },
    copiaDadosEndereco: function (e) {
        e.preventDefault();
        $("#id_municipiohidden_corresp").val($('#id_municipiohidden').val());
        $("#sg_uf_corresp").val($('#sg_uf').val()).trigger('change');
        $("#st_cep_corresp").val($('#st_cep').val());
        $("#st_endereco_corresp").val($('#st_endereco').val());
        $("#st_cidade_corresp").val($('#st_cidade').val());
        $("#st_bairro_corresp").val($('#st_bairro').val());
        $("#nu_numero_corresp").val($('#nu_numero').val());
        $("#st_complemento_corresp").val($('#st_complemento').val());
        $("#id_municipio_corresp").val($('#id_municipio').val());
    },
    verificaIdade: function (e) {
        var dateParts1 = (e.currentTarget.value).split('/');
        var view = new ViewRespLegal({
            el: this.$el.find("#responsave-legal"),
            model: this.model ? this.model : null
        })
        if (calculaIdade(dateParts1[1], dateParts1[0], dateParts1[2]) < 18) {
            $('#responsave-legal').removeAttr('hidden');
            view.render();
        } else {
            $('#st_cpfResponsavelLegal').removeAttr('required');
            $('#st_nomeResponsavelLegal').removeAttr('required');
            $('#nu_telResponsavelLegal').removeAttr('required');
            $('#responsave-legal').attr('hidden', true);
        }
    },
    somenteNumeros: function (e) {

        //teclas adicionais permitidas (tab,delete,backspace,setas direita e esquerda)
        keyCodesPermitidos = new Array(8, 9, 37, 39, 46);

        //Verifica se a tecla digitada é permitida
        if (keyCodesPermitidos.indexOf(e.keyCode) === -1 && !validaNumero(e.key)) {
            return false;
        }
        return true;
    },
    validaTelefone: function (e) {
        var valid = true;
        var tipotelefone = this.$el.find("#id_tipotelefone").val();
        var tipotelefone2 = this.$el.find("#id_tipotelefone2").val();
        var nu_ddi = this.$el.find("#nu_ddi").val();
        var nu_ddd = this.$el.find("#nu_ddd").val();
        var nu_telefone = this.$el.find("#nu_telefone").val();
        var nu_ddi2 = this.$el.find("#nu_ddi2").val();
        var nu_ddd2 = this.$el.find("#nu_ddd2").val();
        var nu_telefone2 = this.$el.find("#nu_telefone2").val();
        if(nu_telefone.length <8 || nu_telefone.length >9 || nu_ddd.length >3 || nu_ddi.length >3 || nu_ddi.length < 2)
        {
            $.pnotify({
                type: 'warning',
                title: 'Prezado Usuário,',
                text: 'Preencha corretamente os números de telefone, DDD e/ou DDI!'
            });
            valid = false;
        }
        var reDigits = /^[2-9][0-9]{7,8}\b/;
        if(!reDigits.test(nu_telefone) || nu_telefone2 && !reDigits.test(nu_telefone2))
        {
            $.pnotify({
                type: 'warning',
                title: 'Prezado Usuário,',
                text: 'Preencha o número de telefone com um valor válido!'
            });
            valid = false;
        }
        if(tipotelefone == 1 && nu_telefone.length != 8 || tipotelefone == 2 && nu_telefone.length != 8 || tipotelefone2 == 1 && nu_telefone2.length != 8 || tipotelefone2 == 2 && nu_telefone2.length != 8)
        {
            $.pnotify({
                type: 'warning',
                title: 'Prezado Usuário,',
                text: 'Telefones residenciais e comerciais devem ter apenas 8 dígitos!'
            });
            valid = false;
        }
        if(nu_ddi2 && nu_ddd2 && nu_telefone2)
        {
            if(nu_telefone2.length <8 || nu_telefone2.length >9 || nu_ddd2.length>3 || nu_ddd2.length<2 || nu_ddi2.length >3 || nu_ddi2.length < 2)
            {
                $.pnotify({
                    type: 'warning',
                    title: 'Prezado Usuário,',
                    text: 'Preencha corretamente os números de telefone, DDD e/ou DDI ou esvazie o campo secundário!'
                });
                valid = false;
            }
        }
        if(nu_ddi == 55 && nu_ddd.length>=3 || nu_ddi2 == 55 && nu_ddd2.length>=3 )
        {
            $.pnotify({
                type: 'warning',
                title: 'Prezado Usuário,',
                text: 'Preencha corretamente o DDD, pois no Brasil o mesmo contém apenas 2 dígitos!'
            });
            valid = false;
        }
        return valid;
    },
    defineTamanhoDDD: function ()
    {
        var nu_ddi = this.$el.find("#nu_ddi").val();
        if(nu_ddi.length==2 && nu_ddi == 55) {
            this.$el.find('#nu_ddd').attr('maxlength', 2);
        }else
        {
            this.$el.find('#nu_ddd').attr('maxlength', 3);
        }
    },
    defineTamanhoDDD2: function ()
    {
        var nu_ddi2 = this.$el.find("#nu_ddi2").val();
        if(nu_ddi2.length==2 && nu_ddi2 == 55) {
            this.$el.find('#nu_ddd2').attr('maxlength', 2);
        }else {
            this.$el.find('#nu_ddd2').attr('maxlength', 3);
        }
    },
    defineTamanhoTelefone: function ()
    {
        var tipotelefone = this.$el.find("#id_tipotelefone").val();
        if(tipotelefone==1 || tipotelefone==2) {
            this.$el.find('#nu_telefone').attr('maxlength', 8);
        }else
        {
            this.$el.find('#nu_telefone').attr('maxlength', 9);
        }
    },
    defineTamanhoTelefone2: function ()
    {
        var tipotelefone2 = this.$el.find("#id_tipotelefone2").val();
        if(tipotelefone2==1 || tipotelefone2==2) {
            this.$el.find('#nu_telefone2').attr('maxlength', 8);
        }else {
            this.$el.find('#nu_telefone2').attr('maxlength', 9);
        }
    }
});


var modelFormulario = new CadastroModel(DadosFormulario);
var viewFormulario = new ViewFormulario({model: modelFormulario});

$(function () {
    loaded();
//Set Mask Input
    $("#st_cpf").mask('999.999.999-99');
    $("#st_cpfResponsavel").mask('999.999.999-99');
    $("#st_cep,#st_cep_corresp").mask('99.999-999');

    //$("#modalCam").on('hidden', function () {
    //    var str_avatar = $("#str_avatar").val();
    //    if (str_avatar) {
    //        $("#img_avatar div.image").html('<img style="height:150px; width:200px;" src="' + str_avatar + '" class="img-polaroid">');
    //    }
    //});
    //$("#st_urlavatar").on('change', function () {
    //    var imagem = $(this).val();
    //    img = new Image();
    //    img.onLoad = function () {
    //        $("#img_avatar").drawImage(img, 200, 150);
    //    }
    //});
});

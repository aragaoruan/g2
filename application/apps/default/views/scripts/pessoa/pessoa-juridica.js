/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * *                             IMPORTS                               * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */

$.ajaxSetup({async: false});
var comboSituacao = [];
$.get('/api/situacao/?st_tabela=tb_itemativacaoentidade', function (response) {
    comboSituacao = response;
});
$.ajaxSetup({async: true});

/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * *                             VARÍAVEIS                             * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */

var that,
    thatEndereco,
    thatBoleto,
    thatLayout,
    thatCartao,
    thatConta,
    thatCartaoAdicional,
    thatConfiguracoes,
    thatConfiguracoesLayout,
    thatModal,
    thatEntidadeIntegracaoBoleto;

var EsquemaConfiguracao = Backbone.Model.extend({
    defaults: {
        id_esquemaconfiguracao: '',
        st_esquemaconfiguracao: '',
        id_usuariocadastro: '',
        dt_cadastro: '',
        id_usuarioatualizacao: '',
        dt_atualizacao: ''
    },
    idAttribute: 'id_esquemaconfiguracao',
    url: function () {
        return this.id ? '/api/esquema-configuracao/' + this.id : '/api/esquema-configuracao';
    }
});

var RecebedorModel = Backbone.Model.extend({
    defaults: {
        id_recebedor: '',
        st_apikey: ''
    },
    idAttribute: 'id_recebedor'
});


/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * *                        COLLECTIONS                                * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */

/*
 * TODO: Retirar todas as chamadas JQuery e utilizar Marionette
 * */

var EnderecoCollection = Backbone.Collection.extend({
    model: Endereco
});

var ContaEntidadeCollection = Backbone.Collection.extend({
    model: VwContaEntidadeFinanceiro
});

var CartaoConfigCollection = Backbone.Collection.extend({
    model: BoletoConfigPessoaJuridica
});

var MunicipioCollection = Backbone.Collection.extend({
    url: '/pessoa/get-municipio-by-uf',
    model: Municipio
});

var CatracaCollection = Backbone.Collection.extend({
    url: '/api/catraca',
    model: Catraca,
    initialize: function () {
    }
});

var SituacaoCollection = Backbone.Collection.extend({
    url: '/api/situacao/?st_tabela=tb_catraca',
    model: Situacao,
    initialize: function () {
        this.fetch()
    }
});

var SituacaoEntidadeCollection = Backbone.Collection.extend({
    url: '/api/situacao/?st_tabela=tb_entidade',
    model: Situacao,
    initialize: function () {
    }
});

/**
 * Collection para recebedor
 */
var RecebedorCollection = Backbone.Collection.extend({
    model: RecebedorModel,
    url: function () {
        return '/api/recebedor/';
    },
    data: {
        id_recebedor: null,
        st_recebedor: null,
        id_recebedorexterno: null
    }
});

/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * *                        VIEWS                                      * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 **/

var ShowModal = Marionette.ItemView.extend({
    template: '#template-modal',
    tagName: 'div',
    model: new EntidadeRelacaoModel(),
    ui: {
        bl_dadosfinanceiroconta: '#bl_dadosfinanceiroconta',
        bl_dadosfinanceiroboleto: '#bl_dadosfinanceiroboleto',
        bl_dadosfinanceirocartao: '#bl_dadosfinanceirocartao'
    },
    initialize: function () {
        thatModal = this;
    },
    onRender: function () {
        return this;
    }
});

var indexSelectSecretariado = 0;
var timerInputSecretariado,
    valueInputSecretariado;
var intervaloDeTempo = 500;
var secretariadoTemp = {
    id_secretariado: '',
    st_nomecompleto: ''
};

var ShowFormEntidadeView = Marionette.ItemView.extend({
    template: "#dados-basicos-entidade-form",
    tagName: "div",
    model: new EntidadeRelacaoModel(),
    className: "show-form-dados-entidade",
    ui: {
        id_entidade: "#id_entidade",
        st_situacao_entidade: "#id_situacao_entidade",
        cnpj: "#cnpj",
        inscricao: "#inscricao",
        razao_social: "#razao_social",
        nome_fantasia: "#nome_fantasia",
        site: "#site",
        bl_relacionarentidade: "#bl_relacionarentidade",
        st_wschave: "#st_wschave",
        id_entidade_php: "#id_entidade_php",
        st_siglaentidade: "#st_siglaentidade",
        st_urlportal: "#st_urlportal",
        st_urlnovoportal: "#st_urlnovoportal",
        st_apelido: "#st_apelido",
        st_urllogoutportal: "#st_urllogoutportal",
        id_usuariosecretariado: '#select-secretariado',
        inputSecretariado: "#input-secretariado",
        ulSecretariado: "#ul-secretariado",
        rowSecretariado: "#row-secretariado"
    },
    initialize: function () {

        this.ui.id_entidade_php = $('#id_entidade_php');

        if (id_entidade !== '') {
            this.ui.id_entidade_php.val(id_entidade);

            $('#tabCadastroPessoaJuridica li').removeClass('disabled');

            this.model.url = '/entidade/get-entidade?id=' + this.ui.id_entidade_php.val() + '&bl_relacao=1';
            this.model.fetch({async: false});
        }

        that = this;
    },
    events: {
        "blur #cnpj": '_validarCNPJ',
        "keyup #inscricao": '_validarInscricao',
        "keyup #razao_social": '_validarRazao',
        "keyup #nome_fantasia": '_validarFantasia',
        "keyup #site": '_validarSite',
        "keyup #st_urlportal": "_validarUrlPortal",
        "keyup #st_urllogoutportal": "_validarUrlLayoutPortal",
        "input @ui.inputSecretariado": "pesquisaSecretariado",
        "keyup @ui.inputSecretariado": "alternarSecretariado",
        "mouseover #ul-secretariado > li": "addIndicadorSelecaoListaSecretariado",
        "mouseout #ul-secretariado > li": "removeIndicadorSelecaoListaSecretariado",
        "click #ul-secretariado > li": "selecionaItemListaSecretariado",
        "focus @ui.inputSecretariado" : "limpaInputSecretariado"
    },
    limpaInputSecretariado: function () {
        secretariadoTemp.id_secretariado = that.model.get('id_usuariosecretariado');
        secretariadoTemp.st_nomecompleto = this.ui.inputSecretariado.val();

        this.ui.inputSecretariado.val("");
        that.model.set('id_usuariosecretariado', "");
    },
    selecionaItemListaSecretariado : function (e) {
        this.ui.inputSecretariado.val($(e.currentTarget).html());
        that.model.set('id_usuariosecretariado', $(e.currentTarget).data('id_usuario'));
        this.limpaListaSecretariado();
    },
    removeIndicadorSelecaoListaSecretariado: function (e) {
        $(e.currentTarget).removeClass('secretariadoSelected');
    },
    addIndicadorSelecaoListaSecretariado: function (e) {
        $("#ul-secretariado li").first().removeClass("secretariadoSelected");
        $(e.currentTarget).addClass('secretariadoSelected');
    },
    pesquisaSecretariado: function (e) {
        clearTimeout(timerInputSecretariado);

        var value = this.ui.inputSecretariado.val().replace(/[%|_]/g, '').trim();

        if (!value || value.length < 3 || value === valueInputSecretariado) {
            return;
        }

        var self = this;
        this.limpaListaSecretariado();

        this.ui.ulSecretariado.append("<li index=" + indexSelectSecretariado + " data-val=null >" + "Selecione..." + "</li>").removeClass("hidden");

        timerInputSecretariado = setTimeout(function () {
            valueInputSecretariado = value;

            $.ajax({
                url: '/pessoa/get-pessoa-secretariado',
                data: {
                    st_nomeexibicao: value,
                    id_entidade: G2S.loggedUser.get('id_entidade')
                },
                success: function (response) {
                    if (response.length === 0) {
                        self.limpaListaSecretariado();
                        self.ui.ulSecretariado.removeClass("hidden");
                        self.ui.ulSecretariado.append("<li index=" + indexSelectSecretariado + " data-val=null >" + "Nenhuma pessoa encontrada " + "</li>");
                        return false;
                    }

                    indexSelectSecretariado++;
                    for (var i = 0; i < response.length; i++) {
                        self.ui.ulSecretariado.append("<li index=" + indexSelectSecretariado + " data-id_usuario=" + response[i].id_usuario + ">"
                            + response[i].st_nomeexibicao + "</li>");
                    }

                    var marginBottomSize = (self.ui.ulSecretariado.height() + 25);
                    self.ui.rowSecretariado.css("margin-bottom", marginBottomSize + "px");
                }
            });

        }, intervaloDeTempo);

        $("#ul-secretariado li").first().addClass("secretariadoSelected");
    },
    alternarSecretariado: function (e) {
        var key = e.which,
            $elem = $('.secretariadoSelected');

        if (key === 38) {
            if ($elem.prev().length) {
                $elem.removeClass("secretariadoSelected");
                $elem.prev().addClass("secretariadoSelected");
                return true;
            }
        }

        if (key === 40) {
            if ($elem.next().length) {
                $elem.removeClass("secretariadoSelected");
                $elem.next().addClass("secretariadoSelected");
                return true;
            }
        }

        if (key === 13) {
            this.ui.inputSecretariado.val($elem.html());
            that.model.set('id_usuariosecretariado', $elem.data('id_usuario'));
            this.ui.ulSecretariado.addClass("hidden");
            this.ui.rowSecretariado.removeClass("lista-secretariado-expandida");
            this.limpaListaSecretariado();
            return true;
        }

        return false;
    },
    limpaListaSecretariado: function () {
        this.ui.ulSecretariado.addClass("hidden");
        this.ui.rowSecretariado.css("margin-bottom", "0");
        this.ui.ulSecretariado.empty();
        indexSelectSecretariado = 0;
    },
    carregaInputSecretario: function () {
        var self = this;
        if (that.model.get('id_usuariosecretariado')) {
            $.ajax({
                async: true,
                url: '/api/usuario',
                data: {
                    id: that.model.get('id_usuariosecretariado')
                },
                success: function (response) {
                    self.ui.inputSecretariado.val(response.st_nomecompleto);
                }
            })
        }
    },
    _validarUrlLayoutPortal: function () {
        if (this.ui.st_urllogoutportal.val().length >= 100) {
            $.pnotify({
                type: 'warning',
                text: 'O número máximo de caractere foi atingido',
                title: 'Aviso'
            });
        }
    },
    _validarUrlPortal: function () {
        if (this.ui.st_urlportal.val().length >= 100) {
            $.pnotify({
                type: 'warning',
                text: 'O número máximo de caractere foi atingido',
                title: 'Aviso'
            });
        }
    },
    _validarSite: function () {
        if (this.ui.site.val().length >= 100) {
            $.pnotify({
                type: 'warning',
                text: 'O número máximo de caractere foi atingido',
                title: 'Aviso'
            });
        }
    },
    _validarCNPJ: function () {
        if (!validaCNPJ(this.ui.cnpj.val().replace(/\D/g, ''))) {
            $.pnotify({
                type: 'warning',
                text: 'Por Favor Informe o CNPJ Correto',
                title: 'CNPJ Inválido'
            });
        }
    },
    _validarFantasia: function () {
        if (this.ui.nome_fantasia.val().length >= 60) {
            $.pnotify({
                type: 'warning',
                text: 'O número máximo de caractere foi atingido',
                title: 'Aviso'
            });
        }
    },
    _validarInscricao: function () {
        if (this.ui.inscricao.val().length >= 20) {
            $.pnotify({
                type: 'warning',
                text: 'O número máximo de caractere foi atingido',
                title: 'Aviso'
            });
        }
    },
    _validarRazao: function () {
        if (this.ui.razao_social.val().length >= 60) {
            $.pnotify({
                type: 'warning',
                text: 'O número máximo de caractere foi atingido',
                title: 'Aviso'
            });
        }
    },
    onRender: function () {
        thatConfiguracoesLayout.model.set('str_urlimglogoentidade', this.model.get('str_urlimglogoentidade') ? this.model.get('str_urlimglogoentidade') : '');
        that.ui.cnpj.mask('?99.999.999/9999-99', {reverse: true});
        this.renderizaSituacaoEntidade();
        this.carregaInputSecretario();
        return this;
    },
    renderizaSituacaoEntidade: function () {
        var that = this;

        var collectionSituacaoEntidade = new SituacaoEntidadeCollection();
        collectionSituacaoEntidade.fetch({
            success: function () {
                var view = new SelectView({
                    el: that.ui.st_situacao_entidade,        // Elemento da DOM
                    collection: collectionSituacaoEntidade,      // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_situacao', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_situacao', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: that.model.get('id_situacao').id_situacao       // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();
            },
        });

    }
});

var FormEntidadeEnderecoView = Marionette.ItemView.extend({
    template: '#dados-endereco-entidade-form',
    tagName: 'div',
    className: 'form-endereco-entidade',
    model: new Endereco(),
    ui: {
        id_endereco: '#id_endereco',
        id_entidadeendereco: '#id_entidadeendereco',
        pais: '#id_pais',
        cep: '#st_cep',
        sg_uf: '#sg_uf',
        id_municipio: '#id_municipio',
        cidade: '#st_cidade',
        tipo_endereco: '#id_tipoendereco',
        endereco: '#st_endereco',
        numero: '#nu_numero',
        complemento: '#st_complemento',
        bairro: '#st_bairro',
        sg_uf_selecionado: '#sg_uf_selecionado',
        id_tipoendereco_selecionado: '#id_tipoendereco_selecionado'
    },
    modelEvents: {
        "change": "modelChanged"
    },
    modelChanged: function () {
        return this;
    },
    events: {
        'change #sg_uf': 'atualizarSelectsEndereco',
        'change #id_tipoendereco': 'atualizarSelectsEndereco',
        'click #btn_get_cep': 'getCep',
        'blur #st_cep': 'validarCep',
        "keyup #st_cidade": "_validarCidade",
        "keyup #st_complemento": "_validarComplemento",
        "keyup #st_bairro": "_validarBairro"
    },
    _validarComplemento: function () {
        if (this.ui.complemento.val().length >= 100) {
            $.pnotify({
                type: 'warning',
                text: 'O número máximo de caractere foi atingido',
                title: 'Aviso'
            });
        }
    },
    _validarBairro: function () {
        if (this.ui.bairro.val().length >= 100) {
            $.pnotify({
                type: 'warning',
                text: 'O número máximo de caractere foi atingido',
                title: 'Aviso'
            });
        }
    },
    _validarCidade: function () {
        if (this.ui.cidade.val().length >= 100) {
            $.pnotify({
                type: 'warning',
                text: 'O número máximo de caractere foi atingido',
                title: 'Aviso'
            });
        }
    },
    validarCep: function () {
        $.ajax({
            url: '/pessoa/get-endereco-by-cep/primeiro/1/st_cep/' + this.ui.cep.val().replace(/\D/g, ''),
            type: 'POST',
            dataType: 'JSON',
            success: function (response) {
                if (response == false || response == 'false') {
                    $.pnotify({
                        type: 'warning',
                        text: 'Por Favor Informe o CEP Correto',
                        title: 'CEP Inválido.'
                    });
                } else {
                    thatEndereco.model.set(response);
                    setaFormEndereco(thatEndereco.model)
                }
            },
            error: function () {
                $.pnotify({title: 'Erro', type: 'error', text: 'Erro no sistema'});
            },
            complete: function () {
                loaded()
            },
            beforeSend: function () {
                loading()
            }
        });
    },
    getCep: function () {
        $('#dados-endereco-entidade-wrapper input[type=text].limpar-no-cep, #dados-endereco-entidade-wrapper select.limpar-no-cep').val('');
        getEnderecoByCep();
    },
    atualizarSelectsEndereco: function (e) {
        $(e.currentTarget).parents('.pai').find('.item-selecionado').text(!isNaN($(e.currentTarget).val()) ? $('option:selected', $(e.currentTarget)).text() : $(e.currentTarget).val());
        if (isNaN($(e.currentTarget).val()))
            getMunicipiosByUf($('option:selected', e.currentTarget).val());
    },
    initialize: function () {
        if (that.ui.id_entidade_php.val() != '' && this.model.get('id_endereco') == '') {
            this.model.url = '/default/endereco/get-enderecos?id=' + that.ui.id_entidade_php.val() + '&unico=1';
            this.model.fetch({async: false});
        }

        thatEndereco = this;
    },
    onShow: function () {
        if (thatEndereco.ui.sg_uf_selecionado.length > 0) {
            thatEndereco.ui.sg_uf.val(thatEndereco.ui.sg_uf_selecionado.data('value')).trigger('change');
        }
        return this;
    },
    onRender: function () {
        //Ao editar um cadastro, popula o combo de Tipo de Endereço  com o valor correto.
        this.ui.tipo_endereco.val(this.model.get('id_tipoendereco'));
        this.ui.cep.mask('?99.999-999', {placeholder: "__.___-__"});
    }
});

var EntidadeFinanceiroView = Marionette.ItemView.extend({
    tagName: 'div',
    template: function () {
        var tplEntidadeFinanceiro = '';
        $.get('/js/backbone/templates/entidade-financeiro/tpl-entidade-financeiro.html?_=' + $.now(), '', function (data) {
            tplEntidadeFinanceiro = _.template(data);
        });
        return tplEntidadeFinanceiro;
    },
    events: {
        'submit form': 'actionSave'
    },
    initialize: function () {
        $.ajaxSetup({'async': false});
        if (form_dados.model.get('id_entidade')) {
            var url = '/api/entidade-financeiro?id_entidade=' + form_dados.model.get('id_entidade');//G2S.loggedUser.get('id_entidade');
            this.model = new EntidadeFinanceiro();
            this.model.url = url;
            this.model.fetch();
        } else {
            this.model = new EntidadeFinanceiro();
        }

        return this;
    },
    onShow: function () {
        this.renderSelectTextoChequesDevolvidos();
        this.renderSelectTextoRecibo();
        this.renderSelectTextoReciboConsolidado();
        this.renderSelectImpostoRenda();

        if (VerificarPerfilPermissao(47, 286).length) {
            travarTelaFinanceiros(true);
        }

        return this;
    },
    renderSelectImpostoRenda: function () {
        $.ajaxSetup({'async': false});
        var that = this;
        var collection = new VwTextoSistemaCollection();
        collection.url = '/api/vw-texto-sistema';
        var data = {
            id_entidade: form_dados.model.get('id_entidade'),
            id_textocategoria: 6 // TextoCategoria::VENDA
        };

        var selected = that.model.get('id_textoimpostorenda') ? that.model.get('id_textoimpostorenda').id_textosistema : null;
        that.$el.find('#id_textoimpostorenda').html('');
        that.$el.find('#id_textoimpostorenda').prepend('<option value="" selected>Selecione</option>');
        collection.fetch({
            data: data,
            success: function () {
                var view = new SelectView({
                    el: that.$el.find('#id_textoimpostorenda'),
                    collection: collection,
                    childViewOptions: {
                        optionLabel: 'st_textosistema',
                        optionValue: 'id_textosistema',
                        optionSelected: selected
                    }
                });
                view.render();

            }
        });
    },
    renderSelectTextoRecibo: function () {
        $.ajaxSetup({'async': false});
        var that = this;
        var collection = new VwTextoSistemaCollection();
        collection.url = '/api/vw-texto-sistema';
        var data = {
            id_entidade: form_dados.model.get('id_entidade'),//(G2S.loggedUser.get('id_entidade')),
            id_textocategoria: 6 // TextoCategoria::VENDA
        };

        var selected = that.model.get('id_textosistemarecibo') ? that.model.get('id_textosistemarecibo').id_textosistema : null;
        that.$el.find('#id_textosistemarecibo').html('');
        that.$el.find('#id_textosistemarecibo').prepend('<option value="" selected>Selecione</option>');
        collection.fetch({
            data: data,
            success: function () {
                var view = new SelectView({
                    el: that.$el.find('#id_textosistemarecibo'),
                    collection: collection,
                    childViewOptions: {
                        optionLabel: 'st_textosistema',
                        optionValue: 'id_textosistema',
                        optionSelected: selected
                    }
                });
                view.render();

            }
        });
    },
    renderSelectTextoReciboConsolidado: function () {
        var that = this;
        var collection = new VwTextoSistemaCollection();
        collection.url = '/api/vw-texto-sistema';
        var data = {
            id_entidade: form_dados.model.get('id_entidade'),//(G2S.loggedUser.get('id_entidade')),
            id_textocategoria: 6 // TextoCategoria::VENDA
        };

        var selected = that.model.get('id_reciboconsolidado') ? that.model.get('id_reciboconsolidado').id_textosistema : null;
        that.$el.find('#id_reciboconsolidado').html('');
        that.$el.find('#id_reciboconsolidado').prepend('<option value="" selected>Selecione</option>');

        collection.fetch({
            data: data,
            success: function () {
                var view = new SelectView({
                    el: that.$el.find('#id_reciboconsolidado'),
                    collection: collection,
                    childViewOptions: {
                        optionLabel: 'st_textosistema',
                        optionValue: 'id_textosistema',
                        optionSelected: selected
                    }
                });
                view.render();
            }
        });
    },
    renderSelectTextoChequesDevolvidos: function () {
        var that = this;
        var collection = new VwTextoSistemaCollection();

        collection.url = '/api/vw-texto-sistema';
        var data = {
            id_entidade: form_dados.model.get('id_entidade'),//(G2S.loggedUser.get('id_entidade')),
            id_textocategoria: 6 // TextoCategoria::VENDA
        };

        var selected = that.model.get('id_textochequesdevolvidos') ? that.model.get('id_textochequesdevolvidos').id_textosistema : null;
        that.$el.find('#id_textochequesdevolvidos').html('');
        that.$el.find('#id_textochequesdevolvidos').prepend('<option value="" selected>Selecione</option>');
        collection.fetch({
            data: data,
            success: function () {
                var view = new SelectView({
                    el: that.$el.find('#id_textochequesdevolvidos'),
                    collection: collection,
                    childViewOptions: {
                        optionLabel: 'st_textosistema',
                        optionValue: 'id_textosistema',
                        optionSelected: selected
                    }
                });
                view.render();
            }
        });
    },
    actionSave: function (e) {
        e.preventDefault();

        var that = this;

        var values = {
            id_textosistemarecibo: $('#id_textosistemarecibo').val(),
            id_reciboconsolidado: $('#id_reciboconsolidado').val(),
            id_textochequesdevolvidos: $('#id_textochequesdevolvidos').val(),
            id_entidadefinanceiro: this.model.get('id_entidadefinanceiro') ? this.model.get('id_entidadefinanceiro') : null,
            id_entidade: form_dados.model.get('id_entidade'),
            id_textoimpostorenda: $('#id_textoimpostorenda').val(),
        };

        loading();
        this.model.save(values, {
            complete: function (response) {
                response = $.parseJSON(response.responseText);
                $.pnotify(response);
                loaded();
                that.initialize();
            }
        });
    }
});

var MunicipioOptionView = Marionette.ItemView.extend({
    tagName: 'option',
    template: _.template("<%=st_nomemunicipio%>"),
    onRender: function () {
        this.$el.attr('value', this.model.get('id_municipio'));

        if (thatEndereco.ui.id_municipio.val() == this.model.get('id_municipio'))
            this.$el.attr('selected', 'selected');
    }
});

var MunicipioSelectView = Marionette.CollectionView.extend({
    childView: MunicipioOptionView,
    tagName: 'select',
    className: 'select-assunto span12',
    collection: new MunicipioCollection(),
    onRender: function () {
        this.$el.attr('id', 'id_municipio');
        if (thatEndereco.model.get('id_municipio')) {
            thatEndereco.ui.id_municipio.val(thatEndereco.model.get('id_municipio'));
            $('#id_municipio').val(thatEndereco.model.get('id_municipio'));
        }
    },
    onShow: function () {
        if (thatEndereco.model.get('id_municipio')) {
            thatEndereco.ui.id_municipio.val(thatEndereco.model.get('id_municipio'));
            $('#id_municipio').val(thatEndereco.model.get('id_municipio'));
        }
    }
});

var CartaoView = Marionette.ItemView.extend({
    template: '#dados-cartao-entidade-form',
    tagName: 'div',
    className: 'form-cartao-entidade',
    model: new BoletoConfigPessoaJuridica(),
    events: {
        'click #delete_cartao': 'removeCartaoModel',
        'change #id_cartaobandeira': 'alterarCartaoModel',
    },
    ui: {
        id_cartaobandeira: '#id_cartaobandeira'
    },
    alterarCartaoModel: function (target) {
        this.model.set($(target.currentTarget).attr('id'), $(target.currentTarget).val());
    }
});

var FormEntidadeCartaoView = Marionette.CompositeView.extend({
    template: '#dados-cartao-wrapper-pai',
    tagName: 'div',
    className: 'row-fluid',
    childView: CartaoView,
    childViewContainer: "#lista-cartoes",
    ui: {
        id_cartaobandeira: '#id_cartaobandeira',
        add_cartao: '#add_cartao',
        id_sistema: '#id_sistema',
    },
    events: {
        'click #add_cartao': 'addCartaoModel',
        'click #delete_cartao': 'removeCartaoModel'
    },
    initialize: function () {
        if (that.ui.id_entidade_php.val() != '' && that.ui.id_entidade_php.val() != undefined) {
            cartaoConfigCollection.url = '/api/cartao?id_entidade=' + that.ui.id_entidade_php.val();
            cartaoConfigCollection.fetch({async: false});
        }
        thatCartao = this;
    },
    onShow: function () {
        if (VerificarPerfilPermissao(47, 286).length) {
            travarTelaCartao(true);
        }
    },
    addCartaoModel: function (e) {

        $.ajaxSetup({async: false});

        if (this.ui.id_cartaobandeira.val() == '') {
            $.pnotify({
                title: 'Erro',
                type: 'error',
                text: 'É necessário preencher o campo Bandeira.'
            });
        }

        if (this.ui.id_sistema.val() == '') {
            $.pnotify({
                title: 'Erro',
                type: 'error',
                text: 'É necessário preencher o campo Sistema.'
            });
        }


        var data = {
            id_entidade: $('#id_entidade_php').val(),
            id_cartaobandeira: thatCartao.ui.id_cartaobandeira.val(),
            id_sistema: thatCartao.ui.id_sistema.val()
        };
        var cartaoCheck = new CartaoConfigCollection();
        cartaoCheck.url = '/api/cartao';
        cartaoCheck.fetch({async: false, data: data});

        var inserir = true;
        cartaoConfigCollection.each(function (cartao) {
            if (thatCartao.ui.id_cartaobandeira.val() == cartao.get('id_cartaobandeira') && thatCartao.ui.id_sistema.val() == cartao.get('id_sistema')) {
                $.pnotify({
                    title: 'Atenção',
                    type: 'warning',
                    text: 'Já existe uma Bandeira com este Sistema adicionado.'
                });

                inserir = false;
            }
        }, this);

        if (!inserir) {
            return false;
        }

        if (cartaoCheck.models.length) {
            $.pnotify({
                title: 'Atenção',
                type: 'warning',
                text: 'Já existe uma Bandeira com este Sistema adicionado.'
            });

            return false;
        }

        if (!$(e.currentTarget).hasClass('disabled') && (!this.ui.id_cartaobandeira.attr('disabled') && this.ui.id_cartaobandeira.val() != '') && (!this.ui.id_sistema.attr('disabled') && this.ui.id_sistema.val() != '')) {
            cartaoConfigCollection.add(new BoletoConfigPessoaJuridica({
                id_cartaobandeira: thatCartao.ui.id_cartaobandeira.val(),
                st_cartaobandeira: $('option:selected', thatCartao.ui.id_cartaobandeira).text(),
                id_contaentidade: $('#id_contaentidadeconta').val(),
                st_sistema: $('option:selected', thatCartao.ui.id_sistema).text(),
                id_sistema: thatCartao.ui.id_sistema.val(),
            }));

            thatCartao.ui.id_cartaobandeira.val('');
            thatCartao.ui.id_sistema.val('');
        }

        $.ajaxSetup({async: true});
    }
});

var ContaView = Marionette.ItemView.extend({
    template: '#dados-conta-form',
    tagName: 'div',
    className: 'form-conta-entidade',
    model: new VwContaEntidadeFinanceiro(),
    ui: {
        nu_diaspagamentoentrada: '#nu_diaspagamentoentrada',
        st_banco: '#st_banco',
        st_agencia: '#st_agencia',
        st_digitoagencia: '#st_digitoagencia',
        st_conta: '#st_conta',
        st_digitoconta: '#st_digitoconta',
        id_tipodeconta: '#id_tipodeconta',
        id_contaentidade: '#id_contaentidade',
        id_entidadefinanceiro: '#id_entidadefinanceiro',
        id_tipodeconta_selecionado: '#id_tipodeconta_selecionado',
        st_banco_selecionado: '#st_banco_selecionado'
    },
    events: {
        'click #salvar_conta': 'salvarConta'
    },
    initialize: function () {
        if (that.ui.id_entidade_php.val() != '') {
            this.model.url = '/conta/get-conta-entidade-financeiro?id=' + that.ui.id_entidade_php.val();
            this.model.fetch({async: false});

            id_contaentidade = this.model.get('id_contaentidade');
        }
        thatConta = this;
    },
    onShow: function () {
        if (VerificarPerfilPermissao(47, 286).length) {
            travarTelaConta(true);
            $('.salvar-conta').attr('disabled', true);
            $('.form-conta-entidade .wrapper_actions').hide();
        }
    },
    onRender: function () {
        $('#id_contaentidade_php').val(this.ui.id_contaentidade.val());
    },
    salvarConta: function (e) {
        if (!$(e.currentTarget).hasClass('editar')) {

            thatConta.model.set('id_entidade', that.ui.id_entidade.val());
            thatConta.model.set('id_tipodeconta', thatConta.ui.id_tipodeconta.val() == '' ? thatConta.ui.id_tipodeconta_selecionado.data('value') : thatConta.ui.id_tipodeconta.val());
            thatConta.model.set('st_banco', thatConta.ui.st_banco.val() == '' ? thatConta.ui.st_banco_selecionado.data('value') : thatConta.ui.st_banco.val());
            thatConta.model.set('st_agencia', thatConta.ui.st_agencia.val());
            thatConta.model.set('st_digitoagencia', thatConta.ui.st_digitoagencia.val());
            thatConta.model.set('st_digitoconta', thatConta.ui.st_digitoconta.val());
            thatConta.model.set('st_conta', thatConta.ui.st_conta.val());
            thatConta.model.set('id_contaentidade', thatConta.ui.id_contaentidade.val());
            thatConta.model.unset('st_nomebanco');
            thatConta.model.unset('st_tipodeconta');
            thatConta.model.unset('id_entidadefinanceiro');
            thatConta.model.unset('nu_diaspagamentoentrada');
            thatConta.model.unset('bl_automatricula');
            thatConta.model.unset('nu_multacomcarta');
            thatConta.model.unset('nu_multasemcarta');

            var dados_entidade_financeiro = {
                id_entidadefinanceiro: thatConta.ui.id_entidadefinanceiro.val(),
                nu_diaspagamentoentrada: thatConta.ui.nu_diaspagamentoentrada.val(),
                id_entidade: that.ui.id_entidade.val()
            };

            $.ajax({
                url: '/pessoa/processar-conta-entidade-pessoa-juridica',
                type: 'POST',
                data: {
                    dados_conta: thatConta.model.toJSON(),
                    dados_entidade_financeiro: dados_entidade_financeiro
                },
                dataType: 'JSON',
                success: function (response) {
                    if (response.tipo == 1) {
                        var contaentidade = response.mensagem[0];
                        var entidadefinanceiro = response.mensagem[1];

                        thatConta.ui.id_entidadefinanceiro.val(entidadefinanceiro.id_entidadefinanceiro);

                        if (contaentidade.id_contaentidade.id_contaentidade != undefined) {
                            $('#id_contaentidade').val(contaentidade.id_contaentidade.id_contaentidade);
                            id_contaentidade = contaentidade.id_contaentidade.id_contaentidade;
                        } else {
                            $('#id_contaentidade').val(contaentidade.id_contaentidade);
                            id_contaentidade = contaentidade.id_contaentidade;
                        }

                        $.pnotify({
                            title: 'Sucesso',
                            type: 'success',
                            text: 'Dados da conta da entidade salvas com sucesso.'
                        });

                        travarTelaConta(false);
                    } else {
                        $.pnotify({title: 'Alerta', type: 'warning', text: 'Erro ao cadastrar a conta da entidade.'});
                    }
                },
                error: function () {
                    $.pnotify({title: 'Erro', type: 'error', text: 'Erro ao salvar as contas da entidade.'});
                },
                complete: function () {
                    loaded();
                },
                beforeSend: function () {
                    loading();
                }
            });
        } else {
            travarTelaConta(true);
            $(e.currentTarget).removeClass('btn-inverse editar').addClass('btn-success').children().removeClass('icon-pencil').addClass('icon-ok');
        }
    }
});

var FormBoletoView = Marionette.ItemView.extend({
    template: '#dados-boleto-entidade-form',
    tagName: 'div',
    className: 'form-boleto-entidade',
    model: new EntidadeIntegracao(),
    ui: {
        st_apikey: '#st_apikey',
        st_keycript: '#st_keycript',
        id_recebedor: '#id_recebedor'
    },
    events: {
        "click #salvar_boleto": "eventSalvarDadosBoleto",
    },
    initialize: function () {
        //Busca a entidade integracao dessa entidade com sistema 30 (pagar.me)
        if (that.ui.id_entidade_php.val() != '') {
            this.model.url = '/entidade/get-entidade-integracao?id_entidade=' + that.ui.id_entidade_php.val() + '&id_sistema=' + SISTEMAS.PAGARME;
            this.model.fetch();
        }
    },
    onShow: function () {
        if (VerificarPerfilPermissao(47, 286).length) {
            travarTelaPlataformaPagamento(true);
        }
    },
    onRender: function () {
        return this;
    },
    populaSelectRecebedor: function () {
        new SelectView({
            el: this.ui.id_recebedor,
            collection: recebedorCollection,
            childViewOptions: {
                optionLabel: 'st_recebedor',
                optionValue: 'id_recebedor',
                optionSelected: this.model.get('id_recebedor') ? this.model.get('id_recebedor').id_recebedor : null
            },
            clearView: true
        }).render();
    },
    eventSalvarDadosBoleto: function (e) {
        var thatM = this;
        if (!$(e.currentTarget).hasClass('disabled')) {
            if (this.validaDadosBoleto()) {
                this.model.set('st_codchave', this.ui.st_apikey.val());
                this.model.set('st_codsistema', this.ui.st_keycript.val());
                this.model.set('id_recebedor', this.ui.id_recebedor.val());
                this.model.set('id_entidade', $('#id_entidade_php').val());

                $.ajax({
                    url: '/pessoa/processar-boleto-pessoa-juridica',
                    type: 'POST',
                    data: this.model.toJSON(),
                    dataType: 'JSON',
                    success: function (response) {
                        if (response != 'false') {
                            thatM.model.set(response);
                            $.pnotify({
                                title: 'Sucesso',
                                type: 'success',
                                text: 'Dados do boleto cadastrados com sucesso.'
                            });
                        } else {
                            $.pnotify({
                                title: 'Alerta',
                                type: 'warning',
                                text: 'Erro ao cadastrar os dados do boleta da entidade.'
                            });
                        }
                    },
                    error: function () {
                        $.pnotify({title: 'Erro', type: 'error', text: 'Erro no sistema'});
                        loaded();
                    },
                    complete: function () {
                        $(e.currentTarget).removeClass('disabled');
                        loaded();
                    },
                    beforeSend: function () {
                        $(e.currentTarget).addClass('disabled');
                        loading();
                    }
                });
            }
        }

    },
    validaDadosBoleto: function () {
        var flag = true;

        if (!this.ui.st_apikey.val()) {
            flag = false;
            $('#divStApiKey').addClass('control-group error');
        } else {
            $('#divStApiKey').removeClass('control-group error');
        }

        if (!this.ui.st_keycript.val()) {
            flag = false;
            $('#divStKeyCript').addClass('control-group error');
        } else {
            $('#divStKeyCript').removeClass('control-group error');
        }

        if (!flag) {
            $.pnotify({title: 'Erro', type: 'error', text: 'Verifique os dados do boleto.'});
        }

        return flag;

    }
});

var CartaoDadosAdicionaisView = Marionette.ItemView.extend({
    template: '#dados-cartao-adicional-form',
    tagName: 'div',
    className: 'form-cartao-adicional',
    model: new EntidadeIntegracao(),
    ui: {
        id_entidadeintegracao: '#id_entidadeintegracao',
        st_codchave: '#st_codchave',
        st_caminho: '#st_caminho'
    },
    initialize: function () {
        if (that.ui.id_entidade_php.val() != '') {
            this.model.url = '/entidade/get-entidade-integracao?id_entidade=' + that.ui.id_entidade_php.val() + '&id_sistema=7';
            this.model.fetch({async: false});
        }

        thatCartaoAdicional = this;
    }
});

var EnderecoOptionView = Marionette.ItemView.extend({
    tagName: 'option',
    template: _.template('<%=st_endereco%> - <%=st_complemento%> - <%=st_cep%> - <%=st_bairro%> - <%=st_cidade%>'),
    onRender: function () {
        this.$el.attr('value', this.model.get('id_entidadeendereco'));
    }
});

var EnderecoSelectView = Marionette.CollectionView.extend({
    childView: EnderecoOptionView,
    tagName: 'select',
    className: 'select-endereco span12',
    onRender: function () {
        this.$el.attr('id', 'id_entidadeendereco');
        this.$el.attr('disabled', 'disabled');
    }
});

var ContaEntidadeOptionView = Marionette.ItemView.extend({
    tagName: 'option',
    template: _.template('<%=st_tipodeconta%> | <%=st_banco%> | <%=st_agencia%>-<%=st_digitoagencia%> | <%=st_conta%>-<%=st_digitoconta%>'),
    onRender: function () {
        this.$el.attr('value', this.model.get('id_contaentidade'));
    }
});

var ContaEntidadeSelectView = Marionette.CollectionView.extend({
    childView: ContaEntidadeOptionView,
    tagName: 'select',
    className: 'select-conta span12',
    onRender: function () {
        this.$el.attr('id', 'id_contaentidadeconta');
        this.$el.attr('disabled', 'disabled');
    }
});

var ConfiguracoesView = Marionette.ItemView.extend({
    template: "#dados-configuracoes-entidade",
    tagName: "div",
    className: "form-configuracoes-entidade",
    model: new ConfiguracaoEntidade(),
    ui: {
        "id_configuracaoentidade": "#id_configuracaoentidade",
        "bl_acessoapp": "#bl_acessoapp",
        "bl_minhasprovas": "#bl_minhasprovas",
        "bl_msgagendamentoportal": "#bl_msgagendamentoportal",
        "st_urlportal": "#st_urlportal",
        "st_corentidade": "#corentidade",
        "id_esquemaconfiguracao": "#id_esquemaconfiguracao",
        "id_esquemapergunta": "#id_esquemapergunta",
        "st_codgoogleanalytics": "#st_codgoogleanalytics",
        "st_googletagmanager": "#st_googletagmanager"
    },
    initialize: function () {
        if (that.ui.id_entidade_php.val() != '') {
            this.model.url = '/entidade/get-configuracao-entidade?id=' + that.ui.id_entidade_php.val();
            this.model.fetch({async: false});
        }
        thatConfiguracoes = this;
    },
    onRender: function () {
        this.carregaColpick();

    },
    onShow: function () {
        this.carregaColpick();
        this.populaSelectEsquemaPergunta();
    },
    populaSelectEsquemaPergunta: function () {
        var EsquemaPerguntaCollection = Backbone.Collection.extend({
            model: EsquemaPergunta,
            url: '/api/esquema-pergunta'
        });

        var esquemaCollection = new EsquemaPerguntaCollection();
        esquemaCollection.fetch({
            beforeSend: function () {
                loading();
            },
            complete: function () {
                loaded();
            },
            success: function () {
                var view = new SelectView({
                    el: thatConfiguracoes.ui.id_esquemapergunta,
                    collection: esquemaCollection,
                    childViewOptions: {
                        optionLabel: 'st_esquemapergunta',
                        optionValue: 'id_esquemapergunta',
                        optionSelected: that.model.get('id_esquemapergunta') ? that.model.get('id_esquemapergunta').id_esquemapergunta : null
                    }
                });
                view.render();
            }
        });


    },
    carregaColpick: function () {
        thatConfiguracoes.ui.st_corentidade.css('background-color', '#' + thatConfiguracoes.model.attributes.st_corentidade);

        varbackground = 'rgba(0,0,0,0)';
        if (thatConfiguracoes.model.attributes.st_corentidade) {
            varbackground = '#' + thatConfiguracoes.model.attributes.st_corentidade
        }

        $('#corentidade').colpick({
            colorScheme: 'dark',
            layout: 'rgbhex',
            color: varbackground,
            onSubmit: function (hsb, hex, rgb, el) {
                $(el).css('background-color', '#' + hex);
                $(el).colpickHide();
            }
        });
    }
});

var ConfiguracoesLayoutView = Marionette.ItemView.extend({
    template: '#dados-configuracoes-layout-entidade',
    tagName: 'div',
    model: new ConfigLayoutEntidade(),
    className: 'form-configuracoes-layout-entidade',
    ui: {
        imagem_g2s_portal: '#imagem_g2s_portal',
        imagem_portal_loja: '#imagem_portal_loja',
        form_layout_config: '#form_layout_entidade',
        arquivo_css_loja: '#css_loja',
        imagem_favicon_loja: '#imagem_favicon_loja'

    },
    events: {
        "submit #form_layout_entidade": "eventSalvarDadosConfiguracoesLayout"
    },
    initialize: function () {
        thatConfiguracoesLayout = this;
        if (that.ui.id_entidade_php.val() != '') {
            this.model.url = '/pessoa/get-configuracoes-layout?id_entidade=' + id_entidade;
            this.model.fetch({async: false});
        }
    },
    onShow: function () {
        if (id_contaentidade != '') {
            travarTelaConta(false);
        }
    },
    eventSalvarDadosConfiguracoesLayout: function (e) {
        var form = e.currentTarget;

        $(form).ajaxSubmit({
            url: '/pessoa/processar-configuracoes-layout-entidade',
            type: 'post',
            data: {
                id_entidade: id_entidade == '' ? that.ui.id_entidade.val() : id_entidade
            },
            dataType: 'json',
            success: function (response) {
                if (response.retorno != false) {
                    $.pnotify({
                        title: 'Sucesso',
                        type: 'success',
                        text: 'Parabéns, dados do layout cadastrados com sucesso.'
                    });
                    thatConfiguracoesLayout.initialize();
                    thatConfiguracoesLayout.render();
                } else {
                    $.pnotify({
                        title: 'Alerta',
                        type: 'warning',
                        text: 'Erro ao cadastrar os dados do layout da entidade.'
                    });
                }
            },
            error: function () {
                $.pnotify({
                    title: 'Erro',
                    type: 'error',
                    text: 'Erro no sistema, falha ao fazer o upload das imagens/CSS'
                });
            },
            complete: function () {
                loaded();
            },
            beforeSend: function () {
                loading();
            }
        });
    }
});


var ConfiguracoesCssView = Marionette.ItemView.extend({
    template: '#dados-configuracoes-css-entidade',
    tagName: 'div',
    model: new ConfigLayoutEntidade(),
    className: 'dados-configuracoes-css-entidade',
    ui: {
        arquivo_css_portal: '#arquivo_css_portal'
    },
    events: {
        "submit #form_css_entidade": "eventSalvarDadosCssLayout",
        "click #btn-gerar-css": "eventGerarCss",
        "click #btn-visualizar-css": "eventVisualizarCss"
    },
    initialize: function () {
        thatConfiguracoesCss = this;
        if (that.ui.id_entidade_php.val() != '') {
            this.model.url = '/pessoa/get-configuracoes-css?id_entidade=' + id_entidade;
            this.model.fetch({async: false});
        }
    },
    onShow: function () {
        if (id_contaentidade != '') {
            travarTelaConta(false);
        }
        this.carregaColpick();
    },
    onRender: function () {
        this.carregaColpick();
    },
    eventSalvarDadosCssLayout: function (e) {
        var form = e.currentTarget;

        $(form).ajaxSubmit({
            url: '/pessoa/processar-configuracoes-css-entidade',
            type: 'post',
            data: {
                id_entidade: id_entidade == '' ? that.ui.id_entidade.val() : id_entidade
            },
            dataType: 'json',
            success: function (response) {
                if (response.retorno != false) {
                    $.pnotify({
                        title: 'Sucesso',
                        type: 'success',
                        text: 'Parabéns, dados do layout cadastrados com sucesso.'
                    });
                    thatConfiguracoesCss.initialize();
                    thatConfiguracoesCss.render();
                } else {
                    $.pnotify({
                        title: 'Alerta',
                        type: 'warning',
                        text: 'Erro ao cadastrar os dados do layout da entidade.'
                    });
                }
            },
            error: function () {
                $.pnotify({
                    title: 'Erro',
                    type: 'error',
                    text: 'Erro no sistema, falha ao fazer o upload das imagens/CSS'
                });
            },
            complete: function () {
                loaded();
            },
            beforeSend: function () {
                loading();
            }
        });
    },
    eventGerarCss: function () {
        var rgbPrimario = this.converteCorHex($('#corprimaria').css('background-color'));
        var rgbSecundario = this.converteCorHex($('#corsecundaria').css('background-color'));
        var rgbTerciario = this.converteCorHex($('#corterciaria').css('background-color'));
        bootbox.confirm("Esta ação irá alterar as cores da loja desta entidade. Deseja realmente efetuar esta ação?", function (result) {
            if (result) {
                $.ajax({
                    url: '/pessoa/gerar-css?id_entidade=' + id_entidade + "&rgbPrimario=" + rgbPrimario + "&rgbSecundario=" + rgbSecundario + "&rgbTerciario=" + rgbTerciario,
                    type: 'POST',
                    /*data: {
                     id_entidade: id_entidade,
                     rgbPrimario: rgbPrimario,
                     rgbSecundario: rgbSecundario,
                     rgbTerciario: rgbTerciario
                     },*/
                    dataType: 'JSON',
                    success: function (response) {
                        if (response['retorno']) {
                            $.pnotify({
                                title: 'Sucesso',
                                type: 'success',
                                text: 'Configuração de css salva com sucesso.'
                            });
                        }
                    },
                    error: function () {
                        $.pnotify({
                            title: 'Erro',
                            type: 'warning',
                            text: 'Erro ao salvar css, não existe arquivo de configuração salvo para esta entidade.'
                        });
                    },
                    complete: function () {
                        loaded()
                    },
                    beforeSend: function () {
                        loading()
                    }
                });
            }
        });
    },
    eventVisualizarCss: function () {
        var rgbPrimario = this.converteCorHex($('#corprimaria').css('background-color'));
        var rgbSecundario = this.converteCorHex($('#corsecundaria').css('background-color'));
        var rgbTerciario = this.converteCorHex($('#corterciaria').css('background-color'));
        var url = '/pessoa/visualizar-css/?id_entidade=' + id_entidade + '&rgbPrimario=' + rgbPrimario + '&rgbSecundario=' + rgbSecundario + '&rgbTerciario=' + rgbTerciario;
        window.open(url, '_blank');

    },
    converteCorHex: function (rgb) {
        rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);

        function hex(x) {
            return ("0" + parseInt(x).toString(16)).slice(-2);
        }

        return hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
    },
    carregaColpick: function () {
        var corpri = thatConfiguracoesCss.model.attributes.st_corprimaria;
        var corsec = thatConfiguracoesCss.model.attributes.st_corsecundaria;
        var corterc = thatConfiguracoesCss.model.attributes.st_corterciaria;

        $corprimaria = $('#corprimaria');
        $corsecundaria = $('#corsecundaria');
        $corterciaria = $('#corterciaria');

        $corprimaria.css('background-color', '#' + corpri);
        $corsecundaria.css('background-color', '#' + corsec);
        $corterciaria.css('background-color', '#' + corterc);

        $corprimaria.colpick({
            colorScheme: 'dark',
            layout: 'rgbhex',
            color: '#' + corpri,
            onSubmit: function (hsb, hex, rgb, el) {
                $(el).css('background-color', '#' + hex);
                $(el).colpickHide();
            }
        });

        $corsecundaria.colpick({
            colorScheme: 'dark',
            layout: 'rgbhex',
            color: '#' + corsec,
            onSubmit: function (hsb, hex, rgb, el) {
                $(el).css('background-color', '#' + hex);
                $(el).colpickHide();
            }
        });

        $corterciaria.colpick({
            colorScheme: 'dark',
            layout: 'rgbhex',
            color: '#' + corterc,
            onSubmit: function (hsb, hex, rgb, el) {
                $(el).css('background-color', '#' + hex);
                $(el).colpickHide();
            }
        })
    }
});

var PessoaJuridicaDadosEntidadeLayout = Marionette.LayoutView.extend({
    template: '#template-dados-entidade',
    tagName: 'div',
    className: 'container-fluid',
    regions: {
        modal: '#modalImportacaoDados',
        dados: '#dados-basicos-entidade-wrapper',
        endereco: '#dados-endereco-entidade-wrapper',
        endereco_paises: '#paises-wrapper',
        municipios: '#municipios-wrapper',
        dados_conta: '#dados-conta-wrapper',
        dados_entidade_financeiro: '#dados-entidade-financeiro-wrapper',
        dados_boleto: '#dados-boleto-wrapper',
        dados_cartao: '#dados-cartao-wrapper',
        dados_configuracao: '#dados-configuracao-wrapper',
        dados_configuracao_layout: '#dados-configuracao-layout-wrapper',
        dados_css_layout: '#dados-configuracao-css-wrapper',
        dados_infraestrutura: '#dados-infraestrutura-wrapper',
        dados_infraestrurura_cadastrada: '#dados-infraestrutura-cadastrada-wrapper',
        situacao: '#combo_situacao',
        esquema_configuracao: '#combo_esquemaconfiguracao',
        dados_preparacao: '#preparacao-content',
        dados_cancelamento: '#dados-cancelamento-wrapper'
    },
    events: {
        "click #tabCadastroPessoaJuridica li": "eventTrocarAba",
        "click #btn-salvar-pessoa-juridica": "eventSalvarDadosBasicos",
        "click #btn-salvar-dados-configuracoes": "eventSalvarDadosConfiguracoes",
        "click #salvar_cartao": "eventSalvarDadosFinanceiros"
    },
    onShow: function () {
        $.ajaxSetup({'async': false});
        thatLayout = this;
        if (!VerificarPerfilPermissao(31, 286).length && !VerificarPerfilPermissao(47, 286).length) {
            $('#tab-financeiro-cabecalho').remove();
        }

        $.ajaxSetup({'async': true});
    },
    eventTrocarAba: function (e) {
        if ($(e.currentTarget).hasClass('disabled')) {
            return false;
        }
        return this;
    },
    validaNomeFantasia: function () {
        var nome_fantasia = this.$el.find("#nome_fantasia").val();
        if (nome_fantasia.length > 60) {
            $.pnotify({
                title: 'Atenção',
                text: 'O campo Nome Fantasia deverá ter no máximo 60 caracteres!',
                type: 'warning'
            });
            return false;
        }
        return true;
    },
    validaCamposObrigatorios: function () {

        //Controla o pNotify
        var exibeMensagem = 0;

        //Itera sobre os elementos da tela a serem validados.
        $("#id_situacao_entidade, " +
            "#cnpj, " +
            "#razao_social, " +
            "#inscricao, " +
            "#st_cep, " +
            "#st_cidade, " +
            "#nu_numero, " +
            "#st_bairro, " +
            "#id_tipoendereco, " +
            "#sg_uf, " +
            "#st_endereco, " +
            "#nome_fantasia")
            .each(function () {

                //Remove quaisquer classes de erro que podem estar ativas num input.
                $(this).closest('div').removeClass('error');

                /* Se não houver valor no input a ser validado,
                 adiciona a classe de error na div superior e
                 habilita a exibição do pNotify.
                 (bootstrap control-group)
                 */
                if ($(this).val() == '') {
                    exibeMensagem = 1;
                    $(this).closest('div').addClass('error');
                }

                //Ao focar no input, remove a classe de erro.
                $(this).focus(function () {
                    $(this).closest('div').removeClass('error');
                });

                /* Ao perder o foco, checa se o input tem algum valor.
                 Se não tiver, adiciona a classe error novamente,
                 tornando-o vermelho.
                 * */
                $(this).focusout(function () {
                    if ($(this).val() == '') {
                        $(this).closest('div').addClass('error');
                    }
                });

            });

        if (exibeMensagem) {
            $.pnotify({
                title: 'Erro',
                text: 'Preencha os campos obrigatórios.',
                type: 'error'
            });
            return false;
        }

        return true;
    },
    eventSalvarDadosBasicos: function (e) {
        if (this.validaCamposObrigatorios() && this.validaNomeFantasia()) {
            if (!$(e.currentTarget).hasClass('disabled')) {

                // Salvando os dados básico da entidade

                that.model.set('id_entidade', that.ui.id_entidade.val());
                that.model.set('st_cnpj', that.ui.cnpj.val().replace(/\D/g, ''));
                that.model.set('nu_inscricaoestadual', that.ui.inscricao.val());
                that.model.set('st_razaosocial', that.ui.razao_social.val());
                that.model.set('st_nomeentidade', that.ui.nome_fantasia.val());
                that.model.set('st_siglaentidade', that.ui.st_siglaentidade.val());
                that.model.set('st_urlsite', that.ui.site.val());
                that.model.set('st_urlportal', that.ui.st_urlportal.val());
                that.model.set('st_urlnovoportal', that.ui.st_urlnovoportal.val());
                that.model.set('st_apelido', that.ui.st_apelido.val());
                that.model.set('st_urllogoutportal', that.ui.st_urllogoutportal.val());
                that.model.set('id_situacao', that.ui.st_situacao_entidade.val());
                //O usuário secretariado já é setado no momento em que a pessoa faz a seleção no input.

                that.model.set('id_esquemaconfiguracao', thatConfiguracoes.ui.id_esquemaconfiguracao.val() !== undefined
                    ? thatConfiguracoes.ui.id_esquemaconfiguracao.val() : null);
                that.model.set('id_esquemapergunta', thatConfiguracoes.ui.id_esquemapergunta.val() !== undefined
                    ? thatConfiguracoes.ui.id_esquemapergunta.val() : null);

                that.model.unset('sistemas');
                that.model.unset('projetoPedagogico');
                that.model.unset('st_wschave');
                that.model.unset('id_usuariocadastro');
                that.model.unset('id_entidadecadastro');
                that.model.unset('st_conversao');
                that.model.unset('dt_cadastro');
                that.model.unset('bl_relacao');

                // Salvando os dados de endereço da entidade

                var optionTipoEndereco = $('#id_tipoendereco option:selected');

                thatEndereco.model.set('id_endereco', thatEndereco.ui.id_endereco.val());
                thatEndereco.model.set('id_pais', thatEndereco.ui.pais.val());
                thatEndereco.model.set('st_cep', thatEndereco.ui.cep.val());
                thatEndereco.model.set('sg_uf', thatEndereco.ui.sg_uf.val() === '' ?
                    thatEndereco.ui.sg_uf_selecionado.data('value') : thatEndereco.ui.sg_uf.val());
                thatEndereco.model.set('st_cidade', thatEndereco.ui.cidade.val());
                thatEndereco.model.set('id_tipoendereco', thatEndereco.ui.tipo_endereco.val() === '' ?
                    thatEndereco.ui.id_tipoendereco_selecionado.data('value') : thatEndereco.ui.tipo_endereco.val());
                thatEndereco.model.set('st_endereco', thatEndereco.ui.endereco.val());
                thatEndereco.model.set('nu_numero', thatEndereco.ui.numero.val());
                thatEndereco.model.set('st_complemento', thatEndereco.ui.complemento.val());
                thatEndereco.model.set('st_bairro', thatEndereco.ui.bairro.val());
                thatEndereco.model.set('st_tipoendereco', optionTipoEndereco.val() !== '' ?
                    optionTipoEndereco.text() : '');
                thatEndereco.model.set('id_municipio', $('#id_municipio').val());
                thatEndereco.model.set('bl_ativo', 1);

                thatEndereco.model.unset('id_entidade');
                thatEndereco.model.unset('id_entidadeendereco');
                thatEndereco.model.unset('st_categoriaendereco');
                thatEndereco.model.unset('st_nomemunicipio');
                thatEndereco.model.unset('st_nomepais');
                thatEndereco.model.unset('st_uf');
                thatEndereco.model.unset('st_estadoprovincia');
                thatEndereco.model.unset('st_categoriaendereco');
                thatEndereco.model.unset('id_categoriaendereco');
                thatEndereco.model.unset('id_usuario');

                $.ajax({
                    url: '/pessoa/salvar-pessoa-juridica',
                    type: 'GET',
                    data: {
                        dados_entidade: that.model.toJSON(),
                        dados_endereco: thatEndereco.model.toJSON(),
                        bl_relacionarentidade: that.ui.bl_relacionarentidade.is(':checked') ? that.ui.bl_relacionarentidade.val() : 0
                    },
                    dataType: 'JSON',
                    success: function (response) {
                        if (+response.tipo === 1) {
                            that.ui.id_entidade.val(response.retorno.entidade.id_entidade);
                            that.ui.st_wschave.val(response.retorno.entidade.st_wschave);
                            thatEndereco.ui.id_endereco.val(response.retorno.endereco.id_endereco);


                            $.pnotify({
                                title: 'Sucesso',
                                type: 'success',
                                text: 'Parabéns, dados básicos da entidade cadastrados com sucesso.'
                            });

                            if (that.ui.bl_relacionarentidade.is(':checked'))
                                that.ui.bl_relacionarentidade.attr('disabled', 'disabled');

                            ativarProximaAba();
                        } else {
                            $.pnotify({
                                title: 'Alerta',
                                type: 'alert',
                                text: 'Desculpe, tivemos erros no cadastro, por favor revise os dados inseridos.'
                            });
                        }
                    },
                    error: function () {
                        $.pnotify({title: 'Erro', type: 'error', text: 'Erro no sistema, por favor entre em contato.'});
                    },
                    complete: function () {
                        $(e.currentTarget).removeClass('disabled');
                        loaded();
                    },
                    beforeSend: function () {
                        $(e.currentTarget).addClass('disabled');
                        loading();
                    }
                });
            }
        }
    },
    eventSalvarDadosFinanceiros: function (e) {
        if (!$(e.currentTarget).hasClass('disabled')) {
            var dadosCartao = [];

            _(thatCartao.collection.models).each(function (model) {

                if (model.get('id_contaentidade') === '') {
                    model.set('id_contaentidade', $('#id_contaentidade').val());
                }
                dadosCartao.push(model.toJSON());
            });

            $.ajax({
                url: '/pessoa/processar-cartao-pessoa-juridica',
                type: 'POST',
                data: {
                    id_entidade: that.ui.id_entidade.val(),
                    id_entidadefinanceiro: thatConta.ui.id_entidadefinanceiro.val(),
                    dados_cartao: dadosCartao
                },
                dataType: 'JSON',
                success: function (response) {
                    if (response != 'false') {
                        cartaoConfigCollection.url = '/api/cartao?id_entidade=' + that.ui.id_entidade.val();
                        cartaoConfigCollection.fetch({async: false});

                        thatCartao.collection = cartaoConfigCollection;

                        var form_cartao = new FormEntidadeCartaoView({
                            collection: cartaoConfigCollection
                        });

                        viewDadosEntidade.dados_cartao.show(form_cartao);

                        travarTelaConta(false);

                        $.pnotify({
                            title: 'Sucesso',
                            type: 'success',
                            text: 'Parabéns, dados financeiro cadastrados com sucesso.'
                        });

                        ativarProximaAba();

                    } else {
                        $.pnotify({
                            title: 'Alerta',
                            type: 'warning',
                            text: 'Erro ao cadastrar os dados financeiro da entidade.'
                        });
                    }
                },
                error: function () {
                    $.pnotify({title: 'Erro', type: 'error', text: 'Erro no sistema'});
                },
                complete: function () {
                    $(e.currentTarget).removeClass('disabled');
                    loaded();
                },
                beforeSend: function () {
                    $(e.currentTarget).addClass('disabled');
                    loading();
                }
            });
        }
    },
    /**
     * Converte a cor retornada pelo colpick em hexadecimal
     * @param rgb
     * @returns {*}
     */
    converteCorHex: function (rgb) {
        rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);

        function hex(x) {
            return ("0" + parseInt(x).toString(16)).slice(-2);
        }

        return hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
    },
    eventSalvarDadosConfiguracoes: function (e) {

        if (thatConfiguracoes.ui.st_corentidade.val().length > 7) {
            $.pnotify({
                title: 'Erro',
                type: 'error',
                text: 'A cor da entidade deve ter menos que seis caracteres!'
            });
            $('#divCorEntidade').addClass('control-group error');
            return false;
        } else {
            $('#divCorEntidade').removeClass('control-group error');
        }

        if (!$(e.currentTarget).hasClass('disabled')) {

            var st_corentidade = null;
            if (thatConfiguracoes.ui.st_corentidade.css("background-color") !== "transparent"
                && thatConfiguracoes.ui.st_corentidade.css("background-color") !== "rgba(0, 0, 0, 0)") {
                st_corentidade = this.converteCorHex(thatConfiguracoes.ui.st_corentidade.css("background-color"));
            }
            var bl_msgagendamentoportal, bl_acessoapp, bl_minhasprovas = 0;
            if (thatConfiguracoes.ui.bl_msgagendamentoportal.is(":checked")) {
                bl_msgagendamentoportal = 1;
            }

            if (thatConfiguracoes.ui.bl_acessoapp.is(":checked")) {
                bl_acessoapp = 1;
            }

            if (thatConfiguracoes.ui.bl_minhasprovas.is(":checked")) {
                bl_minhasprovas = 1;
            }

            thatConfiguracoes.model.set("id_configuracaoentidade", thatConfiguracoes.ui.id_configuracaoentidade.val());
            thatConfiguracoes.model.set("id_entidade", that.ui.id_entidade.val());
            thatConfiguracoes.model.set("bl_acessoapp", bl_acessoapp);
            thatConfiguracoes.model.set("bl_minhasprovas", bl_minhasprovas);
            thatConfiguracoes.model.set("bl_msgagendamentoportal", bl_msgagendamentoportal);
            thatConfiguracoes.model.set("st_corentidade", st_corentidade);
            thatConfiguracoes.model.set("st_codgoogleanalytics", thatConfiguracoes.ui.st_codgoogleanalytics.val());
            thatConfiguracoes.model.set("st_googletagmanager", thatConfiguracoes.ui.st_googletagmanager.val());

            $.ajax({
                url: '/pessoa/processar-configuracoes-entidade',
                type: 'POST',
                data: {
                    dados_config_entidade: thatConfiguracoes.model.toJSON()
                },
                dataType: 'JSON',
                success: function (response) {
                    thatConfiguracoes.ui.id_configuracaoentidade.val(response.id_configuracaoentidade);
                    $.pnotify({
                        title: 'Sucesso',
                        type: 'success',
                        text: 'Parabéns, configuração da entidade salva com sucesso.'
                    });
                    $('#btn-salvar-dados-configuracoes').removeClass('disabled');
                    thatLayout.eventSalvarDadosBasicos(e);
                },
                error: function (response) {
                    $.pnotify(JSON.parse(response.responseText));
                },
                complete: function () {
                    $(e.currentTarget).removeClass('disabled');
                    loaded();
                },
                beforeSend: function () {
                    $(e.currentTarget).addClass('disabled');
                    loading();
                }
            });
        } else {
            $.pnotify({
                title: 'Erro',
                type: 'error',
                text: 'Essa funcionalidade está desabilitada pra esta entidade.'
            });
        }
    }
});

loading();
var viewDadosEntidade = new PessoaJuridicaDadosEntidadeLayout();
G2S.show(viewDadosEntidade);

var form_dados = new ShowFormEntidadeView();
var form_endereco = new FormEntidadeEnderecoView();

var form_entidade_financeiroView = new EntidadeFinanceiroView();
var form_conta = new ContaView();
var form_boleto = new FormBoletoView();
var cartaoConfigCollection = new CartaoConfigCollection();
var form_cartao = new FormEntidadeCartaoView({collection: cartaoConfigCollection});


var form_configuracao = new ConfiguracoesView();
var form_configuracao_layout = new ConfiguracoesLayoutView();
var form_configuracao_css = new ConfiguracoesCssView();

viewDadosEntidade.dados.show(form_dados);
viewDadosEntidade.endereco.show(form_endereco);
viewDadosEntidade.dados_conta.show(form_conta);
viewDadosEntidade.dados_entidade_financeiro.show(form_entidade_financeiroView);
viewDadosEntidade.dados_boleto.show(form_boleto);
viewDadosEntidade.dados_cartao.show(form_cartao);
viewDadosEntidade.dados_configuracao.show(form_configuracao);
viewDadosEntidade.dados_configuracao_layout.show(form_configuracao_layout);
viewDadosEntidade.dados_css_layout.show(form_configuracao_css);


ComboboxView({
    el: thatConfiguracoes.ui.id_esquemaconfiguracao,
    model: EsquemaConfiguracao,
    url: '/api/esquema-configuracao',
    optionLabel: 'st_esquemaconfiguracao',
    emptyOption: 'Selecione',
    optionSelectedId: (that.model.get('id_esquemaconfiguracao') ? that.model.get('id_esquemaconfiguracao')['id_esquemaconfiguracao'] : null)
}, function (el, composite) {
});

loaded();

function ativarProximaAba() {
    var abaAtiva = $('#tabCadastroPessoaJuridica > li.active');
    abaAtiva.next('li').removeClass('disabled').find('a').trigger('click');
}

function travarTodasTelas(flagTravar) {
    if (flagTravar) {
        travarTelaConta(true);
        travarTelaFinanceiros(true);
        travarTelaPlataformaPagamento(true);
    } else {
        travarTelaConta(false);
        travarTelaFinanceiros(false);
        travarTelaPlataformaPagamento(false);
    }
}

function travarTelaFinanceiros(flagTravar) {
    var select_form_entidade_financeiro = $('#form-entidade-financeiro select'),
        btn_salvar = $('#btn-salvar-entidade-financeiro'),
        wrapper_actions = $('#form-entidade-financeiro .wrapper_actions');

    if (flagTravar) {
        select_form_entidade_financeiro.attr('readonly', 'readonly');
        btn_salvar.attr('disabled', true);
        wrapper_actions.hide();
    }
}

function travarTelaPlataformaPagamento(flagTravar) {
    var input_form_boleto_entidade = $('.form-boleto-entidade input'),
        wrapper_actions = $('.form-boleto-entidade .wrapper_actions'),
        btn_salvar = $('#salvar_boleto');

    if (flagTravar) {
        input_form_boleto_entidade.attr('disabled', true);
        btn_salvar.attr('disabled', true);
        wrapper_actions.hide();
    }
}

function travarTelaCartao(flagTravar) {
    var select_bandeira_cartao = $('#id_cartaobandeira'),
        select_id_sistema = $('#id_sistema'),
        btn_add_cartao = $('#add_cartao'),
        btn_salvar = $('#salvar_cartao'),
        wrapper_actions = $('#dados-cartao-wrapper .wrapper_actions');

    if (flagTravar) {
        select_bandeira_cartao.attr('readonly', 'readonly');
        select_id_sistema.attr('readonly', 'readonly');
        btn_add_cartao.attr('disabled', true);
        btn_add_cartao.hide();
        btn_salvar.attr('disabled', true);
        wrapper_actions.hide();
    }
}

function travarTelaConta(travar) {
    var input_forms_conta = $('.form-conta-entidade input[type=text], .form-conta-entidade select, .form-conta-entidade input[type=number]');
    var checkbox = $('.form-conta-entidade input[type=checkbox]');
    var input_forms_boleto = $('#dados-boleto-wrapper input');
    var select_forms_boleto = $('#dados-boleto-wrapper select');
    var btn_cartao = $('#dados-cartao-wrapper a, #salvar_boleto, #salvar_cartao');
    var input_cartao = $('#dados-cartao-wrapper input');
    var select_cartao = $('#dados-cartao-wrapper select');
    var inputs_entidade_integracao = $('#dados-adicionais-cartao-wrapper input');

    if (travar == true) {
        input_forms_boleto.attr('readonly', 'readonly');
        select_forms_boleto.attr('disabled', 'disabled');
        btn_cartao.addClass('disabled');
        input_cartao.attr('readonly', 'readonly');
        select_cartao.attr('disabled', 'disabled');
        inputs_entidade_integracao.attr('readonly', 'readonly');
        input_forms_conta.removeAttr('readonly');
        checkbox.removeAttr('disabled');
    } else {
        input_forms_boleto.removeAttr('readonly');
        select_forms_boleto.removeAttr('disabled');
        btn_cartao.removeClass('disabled');
        input_cartao.removeAttr('readonly');
        select_cartao.removeAttr('disabled');
        inputs_entidade_integracao.removeAttr('readonly');
        input_forms_conta.attr('readonly', 'readonly');
        checkbox.attr('disabled');

        $('#salvar_conta').html('<i class="icon-ok icon-white"></i> Editar dados da conta')
            .addClass('btn-inverse editar').removeClass('btn-success').children().addClass('icon-pencil').removeClass('icon-ok');
    }
}

function setaFormEndereco(model) {
    /*A linha comentada abaixo recarregava de forma inválida
     * o combo de países na tela.*/
    thatEndereco.ui.sg_uf.val(model.get('sg_uf'));
    thatEndereco.ui.cidade.val(model.get('st_cidade'));
    thatEndereco.ui.tipo_endereco.val(model.get('id_tipoendereco'));
    thatEndereco.ui.endereco.val(model.get('st_endereco'));
    thatEndereco.ui.numero.val(model.get('nu_numero'));
    thatEndereco.ui.complemento.val(model.get('st_complemento'));
    thatEndereco.ui.bairro.val(model.get('st_bairro'));

    thatEndereco.ui.sg_uf.trigger('change');

    //Remove a classe error dos inputs de endereço, caso algum deles não tenha sido preenchido anteriormente.
    $('#sg_uf, #st_cidade, #st_endereco, #st_bairro, #nu_numero').each(function () {
        $(this).closest('div').removeClass('error');
    });
}

/**
 * Return endereço by CEP
 * @param {void}
 * @return {boolean}
 */
function getEnderecoByCep() {
    var cep = $("#st_cep").val().replace(/\D/g, '');
    if (!cep) {
        $.pnotify({
            title: 'Impossível Pesquisar!',
            text: 'Impossível pesquisar por CEP. Preencha o campo de CEP corretamente',
            type: 'warning'
        });
        return false;
    } else {
        thatEndereco.model.url = '/pessoa/get-endereco-by-cep/primeiro/1/st_cep/' + cep;
        thatEndereco.model.fetch({
            async: false,
            success: function (model) {
                setaFormEndereco(model);
                loaded();
            },
            beforeSend: function () {
                loading();
            }
        });
        return true;
    }
}

/**
 * Get municipios by uf on event change
 * @param {object} element
 * @returns {void}
 */
function getMunicipiosByUf(target) {
    loading();

    var sg_uf = target == null ? $('#sg_uf').val() : target;
    var municipioCollection = new MunicipioCollection;
    municipioCollection.url += '/sg_uf/' + sg_uf;
    municipioCollection.fetch({
        async: false,
        success: function () {
            loaded();
        }
    });

    var selectMunicipio = new MunicipioSelectView({
        collection: municipioCollection
    });
    thatLayout.municipios.show(selectMunicipio);

}

//$.ajaxSetup({async: false});
var viewModalDuplicidade = null;

//Variaveis uteis para reutilizacao de codigo
var fetchExec = false;

//Variavel que contem parametros necessario para pesquisa e abertura de ocorrencias
var dataForm = {};

var searchPageableCollection = Backbone.PageableCollection.extend({
    url: '/api/ocorrencia/',
    model: VwOcorrenciasPessoa,
    state: {
        pageSize: 10
    },
    //mode: "client"
    parseState: function (response, queryParams, state, options) {
        return {totalRecords: response.total};
    },
    parseRecords: function (response, options) {
        return response.rows || response;
    },
    sync: function (method, model, options) {
        options.beforeSend = loading;
        options.complete = loaded;
        return Backbone.sync(method, model, options);
    }
});

//Collection de vw_assuntos
var VwSubAssuntosCollection = Backbone.Collection.extend({
    model: VwAssuntos,
    url: '/ocorrencia/find-sub-assunto/'
});
var assuntoCollection = new VwSubAssuntosCollection();
var subAssuntoCollection = new VwSubAssuntosCollection();
var subAssuntoCollectionCopia = new VwSubAssuntosCollection();

//Collection para vw_nucleopessoaco
var VwNucleoPessoaCoCollection = Backbone.Collection.extend({
    model: VwNucleoPessoaCo,
    url: "ocorrencia/get-funcoes-by-entidade"
});
var NucleoPessoaCo = new VwNucleoPessoaCoCollection();

//Collection para vw_nucleopessoaco
var AtendenteCollection = Backbone.Collection.extend({
    model: VwNucleoPessoaCo,
    url: '/ocorrencia/find-nucleo-pessoa-co'
});

var AtendenceCoCopia = new AtendenteCollection();
var AtendenteCoEncaminhar = new AtendenteCollection();

//Collection para vw_ocorrenciainteracao
var VwOcorrenciaInteracaoCollection = Backbone.Collection.extend({
    model: VwOcorrenciaInteracao,
    url: '/ocorrencia/find-interacao'
});
var OcorrenciaInteracao = new VwOcorrenciaInteracaoCollection();

//Collection para controller tramite
var TramiteCollection = Backbone.Collection.extend({
    model: Tramite,
    url: '/api/tramite/'
});
var Tramites = new TramiteCollection();

//View para combo de atendentes
AtendenteView = Backbone.View.extend({
    tagName: 'option',
    initialize: function () {
        _.bindAll(this, 'render');
    },
    render: function () {
        $(this.el).attr('value', this.model.get('id_usuario')).text(this.model.get('st_nomecompleto'));
        return this;
    }
});

//View para combo de atendentes do painel encaminhar
AtendenteViewEncaminhar = Backbone.View.extend({
    tagName: 'option',
    initialize: function () {
        _.bindAll(this, 'render');
    },
    render: function () {

        if (!$('#optgroup-atendente').length && this.model.get('id_funcao') == 8) {
            $(this.el).attr('id', 'optgroup-atendente');
            $(this.el).attr('label', 'Atendente');
            $(this.el).append('<option value="' + this.model.get('id_usuario') + '">' + this.model.get('st_nomecompleto') + '</option>');
        }
        if (!$('#optgroup-atendente-setor').length && this.model.get('id_funcao') == 10) {
            $(this.el).attr('id', 'optgroup-atendente-setor');
            $(this.el).attr('label', 'Atendente de Setor');
            $(this.el).append('<option value="' + this.model.get('id_usuario') + '">' + this.model.get('st_nomecompleto') + '</option>');
        }

        if (this.model.get('id_funcao') == 8) {
            $('#optgroup-atendente').append('<option value="' + this.model.get('id_usuario') + '">' + this.model.get('st_nomecompleto') + '</option>');
        }
        if (this.model.get('id_funcao') == 10) {
            $('#optgroup-atendente-setor').append('<option value="' + this.model.get('id_usuario') + '">' + this.model.get('st_nomecompleto') + '</option>');
        }

        if (!$(this.el).attr('id')) {
            this.el = '';
        }

        return this;
    }
});

//View para combo de atendentes do painel encaminhar, copia
AtendenteViewEncaminharCopia = Backbone.View.extend({
    tagName: 'option',
    initialize: function () {
        _.bindAll(this, 'render');
    },
    render: function () {

        if (!$('#optgroup-atendente-copia').length && this.model.get('id_funcao') == 8) {
            $(this.el).attr('id', 'optgroup-atendente-copia');
            $(this.el).attr('label', 'Atendente');
            $(this.el).append('<option value="' + this.model.get('id_usuario') + '">' + this.model.get('st_nomecompleto') + '</option>');
        }
        if (!$('#optgroup-atendente-setor-copia').length && this.model.get('id_funcao') == 10) {
            $(this.el).attr('id', 'optgroup-atendente-setor-copia');
            $(this.el).attr('label', 'Atendente de Setor');
            $(this.el).append('<option value="' + this.model.get('id_usuario') + '">' + this.model.get('st_nomecompleto') + '</option>');
        }

        if (this.model.get('id_funcao') == 8) {
            $('#optgroup-atendente-copia').append('<option value="' + this.model.get('id_usuario') + '">' + this.model.get('st_nomecompleto') + '</option>');
        }
        if (this.model.get('id_funcao') == 10) {
            $('#optgroup-atendente-setor-copia').append('<option value="' + this.model.get('id_usuario') + '">' + this.model.get('st_nomecompleto') + '</option>');
        }

        if (!$(this.el).attr('id')) {
            this.el = '';
        }

        return this;
    }
});

//View para combo de assunto e subassunto
AssuntoCoView = Backbone.View.extend({
    tagName: 'option',
    initialize: function () {
        _.bindAll(this, 'render');
    },
    render: function () {
        $(this.el).attr('value', this.model.get('id_assuntoco')).text(this.model.get('st_assuntoco'));

        if (that.is_editing) {
            if (that.model.get('id_assuntocopai') == this.model.get('id_assuntoco')) {
                $(this.el).attr('selected', 'selected');
            }
        }

        return this;
    }
});

/**
 * View geral da tela de ocorrencia
 */
MyView = Backbone.View.extend({
    model: new VwOcorrenciasPessoa(),
    modelFuncao: new VwNucleoPessoaCo(),
    dataForm: [],
    el: '#tela-ocorrencias',
    tagName: "div",
    className: "div-class-name",
    is_editing: false,
    msg_success: '',
    msg_error: '',
    msg_confirm: '',
    initialize: function () {
        this.render();
        /*linha comentada por causar duplicidade na pesquisa e ser chamada antes de carregar o combo de funcoes
         * buscando dados de forma incorreta passando id_nucleoco undefined. O formulario (zend) já tras os dados
         * de acordo com o primeiro núcleo selecionado*/
        //this.eventChangeAtendenteAssunto();
        this.model.bind('change', this.render, this);
    },
    render: function () {

        var variables = {};
        var tr;
        var dataFixar = {};
        //Verifica se esta sendo carregado uma ocorrencia especifica
        if (G2S.editModel && Number(G2S.editModel.id_ocorrencia) !== 0 && window.location.hash !== '#/ocorrencia') {
            this.loadOcorrenciaById(G2S.editModel, true);
            this.is_editing = true;
        }

        //alterna entre template de listagem e de editar

        if (this.is_editing) {
            variables = this.model.toJSON();
            variables.st_cpf = this.mascaraCpf(variables.st_cpf);
            variables.visibleBtnFixar = false;
            variables.enableBtnFixar = false;
            variables.visibleBtnAcessarComoAluno = false;
            variables.visibleBtnTransferenciaCurso = false;

            //Verificando se o interessado e um aluno
            if (this.model.get("id_matricula")) {

                //Habilita para visualizacao o botao acessar como aluno
                variables.visibleBtnAcessarComoAluno = true;

                //Pega os dados existentes no fixar caso esteja preenchido
                if (localStorage.getItem('pessoaAutocomplete')) {
                    dataFixar = $.parseJSON(localStorage.getItem('pessoaAutocomplete'));
                }

                //Verifica se ja nao estao fixados os dados do interessado da ocorrencia
                if (dataFixar.id_usuario != this.model.get('id_usuariointeressado')) {
                    //Habilita o botao fixar
                    variables.enableBtnFixar = true;
                }

                //Torna visivel o botao verificar por se tratar de um aluno
                variables.visibleBtnFixar = true;

                //Verifica permissao do botão de transferencia de curso
                if ((this.model.get('id_situacao') == SITUACAO.TB_OCORRENCIA.EM_ANDAMENTO) &&
                    VerificarPerfilPermissao(PERMISSAO.ADICIONAR, FUNCIONALIDADE.TRANSFERENCIA_CURSO).length) {
                    variables.visibleBtnTransferenciaCurso = true;
                }
            }

            tr = _.template($('#ocorrencia-template-edit').html(), variables);
            this.$el.empty().append(tr);

            //Utilizando operador de negacao so para trabalhar, chato de encontrar ocorrencias de PRR
            this.model.get('bl_resgate') ? $(this.el).find('.nav.nav-tabs').show() : null;

            $('#id_evolucao').val(this.model.get('id_evolucao'));

            this.verificaPermissoes(parseInt(this.modelFuncao.get('id_funcao')));
            this.botoesExibicao();
            this.eventLoadInteracoes();
            this.addResponsavelOcorrencia();
            this.eventLoadOcorrenciasRelacionadas();
            this.gerarUrlAcessarComoAluno();
        } else {
            tr = _.template($('#ocorrencia-template-list').html(), variables);
            this.$el.empty().append(tr);
            $('.timepicker').timepicker({
                showMeridian: false,
                defaultTime: false
            });
        }

        this.eventLoadFuncao();

        return this;
    },
    gerarUrlAcessarComoAluno: function () {
        var that = this;
        if (this.model.get("id_matricula")) {
            $.ajax({
                url: '/api/vw-matricula',
                type: 'GET',
                dataType: 'json',
                async: false,
                data: {
                    id: this.model.get('id_matricula'),
                    id_usuario: this.model.get('id_usuario'),
                    id_entidade: this.model.get('id_entidadematricula')
                },
                success: function (modelVwMatricula) {
                    that.model.set("st_urlacesso", modelVwMatricula.st_urlacesso);
                    $("#btn-acessar-como-aluno").attr("href", modelVwMatricula.st_urlacesso);
                },
                error: function () {
                    $.pnotify({title: 'Erro', text: MensagensG2.MSG_OCORRENCIA_37, type: 'danger'});
                }
            });
        }
    },
    toggleEdit: function () {
        this.is_editing = !this.is_editing;
    },
    loadOcorrenciaById: function (model, forceFind) {
        this.toggleEdit();
        var that = this;
        var search = new searchPageableCollection();

        dataForm.id_ocorrencia = model.id_ocorrencia;
        dataForm.id_situacao = model.id_situacao;
        dataForm.id_evolucao = model.id_evolucao;
        dataForm.id_assuntoco = model.id_assuntoco;
        dataForm.id_nucleoco = model.id_nucleoco;

        if (forceFind) {
            dataForm.ignorar_entidade = true;
        }

        loading();
        search.queryParams = {
            paramsSearch: dataForm
        };
        search.fetch({
            async: false,
            success: function () {
                if (search.length) {
                    that.model = search.get(model.id_ocorrencia);
                    dataForm.id_nucleoco = that.model.get('id_nucleoco');
                    that.modelFuncao = new VwNucleoPessoaCo({
                        id_funcao: that.model.get('id_funcao'),
                        id_nucleoco: that.model.get('id_nucleoco')
                    });
                }
            },
            complete: function () {
                if (!search.length) {
                    $.pnotify({title: 'Aviso', text: MensagensG2.MSG_OCORRENCIA_03, type: 'warning'});
                    app_router.navigate('ocorrencia/0', {trigger: true});
                }
                loaded();
            }
        });
    },
    eventSave: function () {
        var oco = new Ocorrencia();
        var that = this;

        oco.set({
            id: this.model.get('id'),
            id_ocorrencia: this.model.get('id_ocorrencia'),
            dt_atendimento: null,
            dt_cadastro: this.model.get('dt_cadastroocorrencia'),
            id_matricula: this.model.get('id_matricula'),
            id_atendente: this.model.get('id_atendente'),
            st_atendente: this.model.get('st_atendente'),
            id_evolucao: $('#id_evolucao').val(),
            id_situacao: this.model.get('id_situacao'),
            id_usuariointeressado: this.model.get('id_usuariointeressado'),
            id_categoriaocorrencia: this.model.get('id_categoriaocorrencia'),
            id_assuntoco: this.model.get('id_assuntoco'),
            id_saladeaula: this.model.get('id_saladeaula'),
            id_ocorrenciaoriginal: this.model.get('id_ocorrenciaoriginal'),
            id_entidade: this.model.get('id_entidade'),
            id_usuariocadastro: this.model.get('id_atendente'),
            id_motivoocorrencia: null,
            st_titulo: this.model.get('st_titulo'),
            st_cpf: this.model.get('st_cpf'),
            st_ocorrencia: this.model.get('st_ocorrencia'),
            id_tipoocorrencia: this.model.get('id_tipoocorrencia'),
            st_cadastroocorrencia: this.model.get('st_cadastroocorrencia'),
            st_assuntocopai: this.model.get('st_assuntocopai'),
            st_assuntoco: this.model.get('st_assuntoco'),
            st_situacao: this.model.get('st_situacao'),
            st_nomeinteressado: this.model.get('st_nomeinteressado'),
            st_emailinteressado: this.model.get('st_emailinteressado '),
            st_telefoneinteressado: this.model.get('st_telefoneinteressado'),
            st_projetopedagogico: this.model.get('st_projetopedagogico '),
            st_saladeaula: this.model.get('st_saladeaula '),
            st_tituloexibicao: this.model.get('st_tituloexibicao'),
            st_coordenadortitular: this.model.get('st_coordenadortitular'),
            id_funcao: this.model.get('id_funcao'),
            st_evolucao: $('#id_evolucao option:selected').text(),
            tipo_update: 1
        });

        loading();
        oco.save(null, {
            success: function (response) {
                that.model = response;
                that.model.set('id_evolucao', $('#id_evolucao').val());
                that.model.set('st_evolucao', $('#id_evolucao option:selected').text());

                $.pnotify({title: 'Sucesso', text: MensagensG2.MSG_SAVE_SUCCESS, type: 'success'});
                $.pnotify({title: 'Sucesso', text: MensagensG2.MSG_OCORRENCIA_27, type: 'success'});
                loaded();
            },
            error: function () {
                $.pnotify({title: 'Erro', text: MensagensG2.MSG_SAVE_ERROR, type: 'error'});
            },
            complete: function () {
                that.render();
                loaded();
            }
        });
    },
    eventBack: function (e) {
        $.ajaxSetup({async: true});
        G2S.editModel = null;
        this.is_editing = false;
        app_router.navigate('ocorrencia/0', {trigger: true});
        this.eventLoadFuncao();
        this.eventLoadAssunto(false);
        this.eventLoadAtendente();
        this.popularForm();

        if (dataForm.id_funcao) {
            $('#id_funcao').val(
                dataForm.id_funcao + '-' + dataForm.id_nucleoco
            );
        }
        if (dataForm.id_atendente) {
            $('#id_atendente').val(dataForm.id_atendente);
        }
        if (dataForm.id_situacao) {
            $('#id_situacao').val(dataForm.id_situacao);
        }

        this.eventPesquisar(e);
        $.ajaxSetup({async: false});

    },
    eventPesquisar: function (e) {
        e.preventDefault();

        this.serializeForm();

        var search = new searchPageableCollection();
        var id_nucleoco = dataForm.id_nucleoco;

        if (!NucleoPessoaCo.length) {
            $.pnotify({title: 'Alerta', text: sprintf(MensagensG2.MSG_CAMPO_OBRIGATORIO, 'função'), type: 'warning'});
            $('#id_funcao').focus();
            return false;
        }

        var params = dataForm;

        var elem = $(this.el);
        params.hr_inicio = elem.find('[name=hr_inicio]').val();
        params.hr_fim = elem.find('[name=hr_fim]').val();

        search.queryParams.paramsSearch = params;

        //Valida se campos de datas foram preenchidos
        if (params.dt_cadastroocorrencia_inicio || params.dt_cadastroocorrencia_fim) {
            if (!params.dt_cadastroocorrencia_inicio || !params.dt_cadastroocorrencia_fim) {
                $.pnotify({title: 'Alerta', text: MensagensG2.MSG_OCORRENCIA_02, type: 'warning'});
                return false;
            }
        }

        //Valida se a opcao situacao foi selecionada pelo usuario
        if (parseInt(params.id_situacao) === 0) {
            $.pnotify({
                type: 'warning',
                title: 'Alerta',
                text: sprintf(MensagensG2.MSG_CAMPO_OBRIGATORIO, 'Situação')
            });
            return false;
        } else if (parseInt(params.id_situacao) == 98 && !params.st_nomeinteressado && !params.st_emailinteressado && !params.id_ocorrencia && !params.id_atendente && !params.dt_cadastroocorrencia_inicio && !params.id_assuntocopai) {
            // 98 = Situacao Em Andamento
            $.pnotify({
                type: 'warning',
                title: 'Alerta',
                text: 'Preencha o nome, o e-mail, o código, a data, o atendente ou o assunto da ocorrência antes de realizar a pesquisa.'
            });
            return false;
        } else if (params.id_situacao === '' && !params.st_nomeinteressado && !params.st_emailinteressado && !params.id_ocorrencia) {
            $.pnotify({
                type: 'warning',
                title: 'Alerta',
                text: 'Preencha o nome, o e-mail, o código da ocorrência ou altere a situação antes de realizar a pesquisa.'
            });
            return false;
        }

        //Valida os campos de hora - devem aparecer somente se a data estiver prenchida
        if (params.dt_cadastroocorrencia_inicio || params.dt_cadastroocorrencia_fim) {
            if (params.hr_inicio && params.hr_fim && (params.dt_cadastroocorrencia_inicio == params.dt_cadastroocorrencia_fim)) {
                var hr_in = parseInt(params.hr_inicio.split(':').join(''));
                var hr_fn = parseInt(params.hr_fim.split(':').join(''));
                if (hr_in > hr_fn) {
                    $.pnotify({
                        title: 'Alerta',
                        text: 'O horário de início não pode ser maior que o horário de término.',
                        type: 'warning'
                    });
                    elem.find('[name=hr_inicio]').focus();
                    return false;
                }
            }
        } else {
            if (params.hr_inicio || params.hr_fim) {
                $.pnotify({
                    title: 'Alerta',
                    text: 'Para pesquisar com horários é necessário selecionar uma data.',
                    type: 'warning'
                });
                return false;
            }
        }

        var HtmlPrintButtonCell = Backgrid.Cell.extend({
            render: function () {
                var disabled = 'disabled="disabled"';

                if (this.model.get('id_situacao') != 97) {
                    disabled = '';
                }

                this.$el.html(_.template("<a " + disabled + " class='btn btn-small btn-print-ocorrencia-inline' target='_blank' href='/ocorrencia/imprimir-ocorrencia/numero/" +
                    this.model.get('id_ocorrencia') + "'><i class='icon-print'></i></a>"));
                this.$el.addClass('btn-imprimir-td');
                return this;
            },
            events: {
                "click .btn-print-ocorrencia-inline": function (e) {
                    window.open('ocorrencia/imprimir-ocorrencia/numero/' + this.model.get('id_ocorrencia'), '_blank');
                    return false;
                }
            }
        });

        var columns = [
            {name: 'id_ocorrencia', label: '', cell: HtmlPrintButtonCell, editable: false},
            {
                name: 'id_ocorrencia',
                label: 'ID',
                cell: Backgrid.IntegerCell.extend({orderSeparator: '.'}),
                editable: false
            },
            {name: "st_entidadematricula", label: "Entidade de Matrícula", cell: "string", editable: false},
            {name: 'st_assuntocopai', label: 'Assunto', cell: 'string', editable: false},
            {name: 'st_assuntoco', label: 'Subassunto', cell: 'string', editable: false},
            {name: 'st_nomeinteressado', label: 'Nome Interessado', cell: 'string', editable: false},
            {name: 'st_titulo', label: 'Título', cell: 'string', editable: false},
            {name: 'st_atendente', label: 'Atendente', cell: 'string', editable: false},
            {name: 'st_evolucao', label: 'Evolução', cell: 'string', editable: false},
            {name: 'st_situacao', label: 'Situação', cell: 'string', editable: false},
            {name: 'st_cadastroocorrencia', label: 'Data Criação', cell: 'string', editable: false},
            {name: 'st_ultimotramite', label: 'Última Interação', cell: 'string', editable: false},
            {name: 'nu_horasatraso', label: 'Horas Atraso', cell: 'string', editable: false},
            {name: 'nu_diasaberto', label: 'Dias Aberto', cell: 'string', editable: false}
        ];

        var RowGrid = Backgrid.Row.extend({
            initialize: function (options) {
                RowGrid.__super__.initialize.apply(this, arguments);
                this.$el.css('color', '#' + view.getContrast50(this.model.get('st_cor')));
                this.$el.css('cursor', 'pointer');
                this.$el.attr('bgcolor', this.model.get('st_cor'));
            },
            abrirOcorrencia: function () {
                var that = this;
                //modifica a situação para "em andamento" se ela estiver como "Pendente"
                // e se o usuário da sessão for o atendente responsável
                if (+that.model.get('id_situacao') === SITUACAO.TB_OCORRENCIA.PENDENTE
                    && +that.model.get('id_atendente') === +G2S.loggedUser.get('id_usuario')
                ) {
                    $.ajax({
                        url: '/ocorrencia/alterar-situacao-ocorrencia',
                        type: 'POST',
                        dataType: 'json',
                        async: false,
                        data: {
                            id_ocorrencia: that.model.get('id'),
                            id_situacao: SITUACAO.TB_OCORRENCIA.EM_ANDAMENTO
                        },
                        success: function () {
                            that.model.set({
                                id_situacao: SITUACAO.TB_OCORRENCIA.EM_ANDAMENTO,
                                st_situacao: 'Em Andamento'
                            });
                            $.pnotify({title: 'Sucesso', text: MensagensG2.MSG_OCORRENCIA_35, type: 'success'});
                        },
                        error: function () {
                            $.pnotify({title: 'Erro', text: MensagensG2.MSG_OCORRENCIA_36, type: 'danger'});
                        }
                    });
                }

                var funcao = view.$el.find('#id_funcao').val().split('-');

                var id_nucleoco = funcao[1];
                var id_funcao = funcao[0];
                //var id_nucleoco = $('#id_funcao option:selected').attr('rel');
                //var id_funcao = $('#id_funcao').val();


                view.modelFuncao = NucleoPessoaCo.where({
                    id_funcao: parseInt(id_funcao),
                    id_nucleoco: parseInt(id_nucleoco)
                })[0];
                view.toggleEdit();
                view.model = that.model;
                app_router.navigate('ocorrencia/' + that.model.get('id'), {trigger: true});
            },
            events: {
                "click td[class!='btn-imprimir-td']": "abrirOcorrencia"
            }
        });

        var pagebleGrid = new Backgrid.Grid({
            className: 'backgrid table table-bordered',
            columns: columns,
            collection: search,
            row: RowGrid
        });

        var paginator = new Backgrid.Extension.Paginator({
            collection: search
        });

        var elemento = $('#data-ocorrencia');

        elemento.empty();
        paginator.$el.empty();

        $('#gerar-xls').addClass('hide');

        // Verifica se ha alguma requisicao em andamento para abortar e manter somente a utilma.
        if (fetchExec)
            fetchExec.abort();

        // Realiza a pesquisa
        fetchExec = search.fetch({
            //async: false,
            timeout: 55555,
            ifModified: true,
            success: function () {
                elemento.append(pagebleGrid.render().$el);
                elemento.append(paginator.render().$el);
                if (search.length) {
                    $('#gerar-xls').removeClass('hide');
                }
            }
        }).always(function (response) {
            if (response.status == 500) {
                $.pnotify({
                    type: 'error',
                    title: 'Erro!',
                    text: response.responseText
                });
            } else {
                if (!search.length) {
                    $.pnotify({
                        type: 'warning',
                        title: 'Aviso',
                        text: MensagensG2.MSG_OCORRENCIA_03
                    });
                }
            }
            //Preenchendo valor na tela de demonstracao de quantidade de resultado
            $('#qnt').empty().append(sprintf(MensagensG2.MSG_OCORRENCIA_32, search.state.totalRecords))
                .closest('div').find('br').remove();
            loaded();
        });

        /*
         * voltando o valor de id_nucleo da variavel que contem dados de pesquisa
         * do formulario após ser removido por nao entrar nos parametros
         * da consulta
         */
        dataForm.id_nucleoco = id_nucleoco;
    },
    eventLoadSubAssunto: function (reset) {
        that = this;

        var rendererSubAssunto = new CollectionView({
            collection: subAssuntoCollection,
            childViewConstructor: AssuntoCoView,
            childViewTagName: 'option',
            el: $('#id_assuntoco')
        });

        if ($('#id_assuntocopai').val()) {
            if (!Boolean(reset) && subAssuntoCollection.length) {
                rendererSubAssunto.render();
                //  $(rendererSubAssunto.el).prepend("<option value=''>Todos</option>");
            } else {
                loading();
                subAssuntoCollection.fetch({
                    async: false,
                    url: "/ocorrencia/find-sub-assunto",
                    data: {
                        id_assuntocopai: $('#id_assuntocopai').val(),
                        id_entidade: $("#id_entidade").val()
                        || that.model.get("id_entidadematricula")
                        || that.model.get("id_entidade")
                    },
                    success: function () {
                        rendererSubAssunto.render();
                        loaded();
                        //   $(rendererSubAssunto.el).prepend("<option value=''>Todos</option>");
                    },
                    error: function () {
                        loaded();
                    },
                    complete: function () {
                        if (that.is_editing) {
                            that.eventLoadAtendenteEncaminhar();
                        }
                    }
                });
            }
        } else {
            //  $(rendererSubAssunto.el).html("<option value=''>Todos</option>");
        }
    },
    eventLoadSubAssuntoCopia: function () {
        that = this;

        var rendererSubAssuntoCopia = new CollectionView({
            collection: subAssuntoCollectionCopia,
            childViewConstructor: AssuntoCoView,
            childViewTagName: 'option',
            el: $('#id_assuntoco_copia')
        });

        loading();
        subAssuntoCollectionCopia.fetch({
            url: "/ocorrencia/find-sub-assunto",
            data: {
                id_assuntocopai: $('#id_assuntocopai_copia').val(),
                id_entidade: $("#id_entidade").val()
                || that.model.get("id_entidadematricula")
                || that.model.get("id_entidade")
            },
            success: function () {
                rendererSubAssuntoCopia.render();
                loaded();
            },
            error: function () {
                loaded();
                $.pnotify({title: 'Erro', text: MensagensG2.MSG_OCORRENCIA_05, type: 'error'});
            },
            complete: function () {
                that.eventLoadAtendenteCopia();
            }
        });
    },
    eventLoadAssunto: function (reset) {
        that = this;

        var rendererAssunto = new CollectionView({
            collection: assuntoCollection,
            childViewConstructor: AssuntoCoView,
            childViewTagName: 'option',
            el: $('#id_assuntocopai')
        });

        $(rendererAssunto.el).empty();

        if (!Boolean(reset) && assuntoCollection.length) {
            rendererAssunto.render();
            //  $(rendererAssunto.el).prepend("<option value=''>Todos</option>");

            /**
             * Verificando se não foi selecionada nenhuma opcao anteriormente
             * para ser marcada a opcao todos na volta a tela principal de
             * ocorrencias
             */
            if (!dataForm.id_assuntocopai && !that.is_editing) {
                $(rendererAssunto.el).val(null);
            }

            /*
             * Usando timeout para evitar erros de tentativa de carregamento
             * sem antes os sistema ter aplicado as regras necessaria para o
             * combo id_assuntocopai
             */
            setTimeout(function () {
                that.eventLoadSubAssunto(true);
            }, 1000);
        } else {
            //loading();
            assuntoCollection.fetch({
                async: false,
                url: "/ocorrencia/find-assunto",
                data: {
                    params: {
                        id_nucleoco: dataForm.id_nucleoco,
                        id_entidade: $("#id_entidade").val()
                        || that.model.get("id_entidadematricula")
                        || that.model.get("id_entidade")
                    }
                },
                success: function () {
                    rendererAssunto.render();
                    // $(rendererAssunto.el).prepend("<option value=''>Todos</option>");

                    loaded();
                },
                error: function () {
                    loaded();
                    $.pnotify({title: 'Erro', text: MensagensG2.MSG_OCORRENCIA_04, type: 'error'});
                },
                complete: function () {
                    if (parseInt(that.model.get('id_funcao')) === 10) {
                        $(rendererAssunto.el).val(that.model.get('id_assuntocopai'));
                    } else {
                        $(rendererAssunto.el).val(null);
                        that.eventLoadSubAssunto(true);
                    }
                }
            });
        }
    },
    eventLoadAssuntoCopia: function () {

        var rendererAssuntoCopia = new CollectionView({
            collection: assuntoCollection,
            childViewConstructor: AssuntoCoView,
            childViewTagName: 'option',
            el: $('#id_assuntocopai_copia')
        });

        if (assuntoCollection.length) {
            rendererAssuntoCopia.render();
            this.eventLoadSubAssuntoCopia();
        } else {
            assuntoCollection.fetch({
                async: false,
                url: "/ocorrencia/find-assunto",
                data: {
                    params: {
                        id_nucleoco: dataForm.id_nucleoco,
                        id_entidade: $("#id_entidade").val()
                        || that.model.get("id_entidadematricula")
                        || that.model.get("id_entidade")
                    }
                },
                success: function () {
                    rendererAssuntoCopia.render();
                    loaded();
                },
                error: function () {
                    loaded();
                    $.pnotify({title: 'Erro', text: MensagensG2.MSG_OCORRENCIA_04, type: 'error'});
                }
            });
        }
    },
    eventLoadAtendente: function (reset) {
        /**
         * Mantendo o atendente da pesquisa
         */
        var id_atendente_pesquisado = $('#id_atendente').val();

        if (parseInt(dataForm.id_funcao) === 8) {
            $('#id_atendente').removeAttr('disabled');
        } else if (parseInt(dataForm.id_funcao) !== 10) {
            $('#id_atendente').removeAttr('disabled');
        }

        //loading();
        var AtendenteCo = new AtendenteCollection();
        var rendererAtendente = new CollectionView({
            collection: AtendenteCo,
            childViewConstructor: AtendenteView,
            childViewTagName: 'option',
            el: $('#id_atendente')
        });

        var $atendente = $(rendererAtendente.el);

        $atendente.empty();

        $.ajaxSetup({async: false});
        AtendenteCo.fetch({
            url: "/ocorrencia/find-atendente",
            data: {
                params: {
                    id_nucleoco: dataForm.id_nucleoco,
                    id_entidade: $("#id_entidade").val()
                    || that.model.get("id_entidadematricula")
                    || that.model.get("id_entidade")
                }
            },
            success: function () {
                loaded();
                rendererAtendente.render();
                $atendente.prepend("<option value=''>Todos</option>");
            },
            error: function () {
                loaded();
                $.pnotify({title: 'Erro', text: MensagensG2.MSG_OCORRENCIA_07, type: 'error'});
            }
        });

        if (id_atendente_pesquisado) {
            $('#id_atendente').val(id_atendente_pesquisado);
        } else {
            $('#id_atendente').val(null);
        }

        $.ajaxSetup({async: true});

        if (+dataForm.id_funcao === 10) {
            $('#id_atendente').prop('disabled', true).val($('#id_usuario').val());
        } else if (+dataForm.id_funcao === 8 || +dataForm.id_funcao === 9) {
            if (!$atendente.find("option[value='nd']").length) {
                $atendente.prepend("<option value='nd'>Não distribuído</option>").prop('disabled', false);
            }
            $atendente.val(dataForm.id_atendente || "nd");
        } else {
            $atendente.prop('disabled', false);
        }

    },
    eventLoadAtendenteCopia: function () {

        var rendererAtendenteCoCopia = new CollectionView({
            collection: AtendenceCoCopia,
            childViewConstructor: AtendenteViewEncaminharCopia,
            childViewTagName: 'optgroup',
            el: $('#id_atendente_copia')
        });

        if ($('#id_assuntoco_copia').val()) {

            loading();
            AtendenceCoCopia.url = "/ocorrencia/find-atendente-encaminhar";

            AtendenceCoCopia.fetch({
                data: {
                    params: {
                        id_assuntoco: $("#id_assuntoco_copia").val(),
                        id_assuntocopai: $("#id_assuntocopai_copia").val(),
                        id_entidade: $("#id_entidade").val() || this.model.get("id_entidadematricula") || this.model.get("id_entidade")
                    }
                },
                success: function () {
                    rendererAtendenteCoCopia.render();
                    loaded();
                },
                error: function () {
                    loaded();
                    $.pnotify({title: 'Erro', text: MensagensG2.MSG_OCORRENCIA_20, type: 'error'});
                }
            });
        } else {
            rendererAtendenteCoCopia.$el.html(null);
        }
    },
    eventLoadAtendenteEncaminhar: function () {

        var rendererAtendenteCoEncaminhar = new CollectionView({
            collection: AtendenteCoEncaminhar,
            childViewConstructor: AtendenteViewEncaminhar,
            childViewTagName: 'optgroup',
            el: $('#id_atendente_encaminhar')
        });

        if ($('#id_assuntoco').val()) {
            AtendenteCoEncaminhar.url = "/ocorrencia/find-atendente-encaminhar";

            if ($('#id_assuntoco').val()) {
                loading();
                AtendenteCoEncaminhar.fetch({
                    data: {
                        params: {
                            id_assuntoco: $("#id_assuntoco").val(),
                            id_assuntocopai: $("#id_assuntocopai").val(),
                            id_entidade: $("#id_entidade").val() || this.model.get("id_entidadematricula") || this.model.get("id_entidade")
                        }
                    },
                    success: function () {
                        rendererAtendenteCoEncaminhar.render();
                        loaded();
                    },
                    error: function () {
                        loaded();
                        $.pnotify({title: 'Erro', text: MensagensG2.MSG_OCORRENCIA_20, type: 'error'});
                    }
                });
            }
        } else {
            rendererAtendenteCoEncaminhar.$el.html(null);
        }
    },
    eventChangeAtendenteAssunto: function () {
        var that = this;
        var $input = $("#id_funcao");

        dataForm.id_funcao = null;
        dataForm.id_nucleoco = null;

        /*
         * Atualizando variavel que mantem dados do formulario de pesquisa ao
         * alterar funcao do atendente
         */
        if ($input.val()) {
            var funcao = $input.val().split("-");

            dataForm.id_funcao = funcao[0];
            dataForm.id_nucleoco = funcao[1];
            dataForm.servico = funcao[2];
        }

        if (dataForm.id_funcao == 8) {
            dataForm.id_atendente = 'nd';
        } else {
            dataForm.id_atendente = null;
        }

        if (!that.is_editing) {
            that.eventLoadAssunto(true);
            that.eventLoadAtendente();
        }
    },
    toggleForm: function () {
        $('.filtro-ocorrencia').toggleClass('hide', !NucleoPessoaCo.length);
        $('#data-ocorrencia').toggleClass('hide', !NucleoPessoaCo.length);
    },
    eventLoadFuncao: function (reset) {
        loaded();
        that = this;

        if (+G2S.loggedUser.get("id_linhadenegocio") === LINHA_DE_NEGOCIO.GRADUACAO) {
            $(".label-entidade").removeClass('hide');
        }

        if (reset) {
            NucleoPessoaCo.reset();
        }

        var SelectView = Backbone.View.extend({
            tagName: 'option',
            initialize: function () {
                _.bindAll(this, 'render');
            },
            render: function () {
                $(this.el)
                    .attr('value', this.model.get('id_funcao') + '-' + this.model.get('id_nucleoco'))
                    .text(this.model.get('st_funcao') + ' - ' + this.model.get('st_nucleoco'));
                //$(this.el).attr('rel', this.model.get('id_nucleoco'));
                return this;
            }
        });

        var renderer = new CollectionView({
            collection: NucleoPessoaCo,
            childViewConstructor: SelectView,
            childViewTagName: 'option',
            el: $('#id_funcao')
        });

        //Verificando se a model de funcoes ja se encontra preenchida
        if (NucleoPessoaCo.length) {
            renderer.render();
            that.toggleForm();
            that.eventLoadAtendente(true);
            that.eventLoadAssunto(true);
        } else {
            NucleoPessoaCo.fetch({
                async: false,
                data: {
                    id_entidade: $("#id_entidade").val()
                    || that.model.get("id_entidadematricula")
                    || that.model.get("id_entidade")
                },
                success: function () {
                    renderer.render();

                    if (!NucleoPessoaCo.length) {
                        $.pnotify({title: 'Alerta', text: MensagensG2.MSG_OCORRENCIA_08, type: 'warning'});
                        renderer.el.focus();
                    }
                },
                complete: function () {
                    that.toggleForm();

                    var val = renderer.$el.val() || "";

                    var funcao = val.split("-");
                    dataForm.id_funcao = funcao[0];
                    dataForm.id_nucleoco = funcao[1];

                    that.eventLoadAtendente(true);
                    that.eventLoadAssunto(true);

                    if (val) {
                        $atendente = $('#id_atendente');

                        if (+dataForm.id_funcao === 8 || +dataForm.id_funcao === 9) {
                            if (!$atendente.find("option[value='nd']").length) {
                                $atendente.prepend("<option value='nd'>Não distribuído</option>");
                            }
                            $atendente.val('nd');
                        }
                    }

                    loaded();
                }
            });
        }
        loaded();
    },
    eventEncerrarOcorrencia: function () {
        this.msg_confirm = sprintf(MensagensG2.MSG_OCORRENCIA_09, 'encerrar');
        this.msg_success = sprintf(MensagensG2.MSG_OCORRENCIA_10, 'encerrada');
        this.msg_error = sprintf(MensagensG2.MSG_OCORRENCIA_11, 'encerrar');
        this.updateOcorrencia(2);
    },
    eventDevolverOcorrencia: function () {

        this.msg_confirm = sprintf(MensagensG2.MSG_OCORRENCIA_09, 'devolver');
        this.msg_success = sprintf(MensagensG2.MSG_OCORRENCIA_10, 'devolvida');
        this.msg_error = sprintf(MensagensG2.MSG_OCORRENCIA_11, 'devolver');
        this.updateOcorrencia(3);
    },
    eventReabrirOcorrencia: function () {
        this.msg_confirm = sprintf(MensagensG2.MSG_OCORRENCIA_09, 'reabrir');
        this.msg_success = sprintf(MensagensG2.MSG_OCORRENCIA_10, 'reaberta');
        this.msg_error = sprintf(MensagensG2.MSG_OCORRENCIA_11, 'reabrir');
        this.updateOcorrencia(4);
    },
    eventSolicitarEncerramento: function () {
        this.msg_confirm = sprintf(MensagensG2.MSG_OCORRENCIA_09, 'solicitar encerramento');
        this.msg_success = MensagensG2.MSG_OCORRENCIA_21;
        this.msg_error = MensagensG2.MSG_OCORRENCIA_22;
        this.updateOcorrencia(5);
    },
    eventEncaminharGerarCopia: function () {
        bootbox.confirm(sprintf(MensagensG2.MSG_OCORRENCIA_09, 'encaminhar'),
            function (result) {
                if (result) {
                    //disparando evento de encaminhar ocorrencia
                    this.encaminharOcorrencia(false);
                    //verificando e disparando acao de gerar copia de ocorrencia
                    if ($('#checkbox-copiaencaminhar').is(':checked')) {
                        this.gerarCopiaOcorrencia(false);
                    }
                    view.render();
                }
            }.bind(this));
    },
    eventGerarNovaInteracao: function () {
        that = this;

        if (tinymce.editors["txt-interacao"].getContent() === '') {
            $.pnotify({
                title: 'Alerta',
                text: sprintf(MensagensG2.MSG_CAMPO_OBRIGATORIO, MensagensG2.MSG_OCORRENCIA_26),
                type: 'danger'
            });
            return false;
        }
        if (!$('.check-bl-visivel').is(':checked')) {
            $.pnotify({
                title: 'Alerta',
                text: sprintf(MensagensG2.MSG_CAMPO_OBRIGATORIO, MensagensG2.MSG_OCORRENCIA_25),
                type: 'danger'
            });
            return false;
        }

        if ($('.check-bl-visivel:checked').val() == 1) {
            bootbox.confirm(MensagensG2.MSG_OCORRENCIA_23, function (result) {
                if (result) {
                    that.functionGerarNovaInteracao(that);
                }
            });
        } else {
            this.functionGerarNovaInteracao(this);
        }
    },
    functionGerarNovaInteracao: function (that) {
        var btn = this.$el.find('#btn-gerar-interacao');

        if (btn.prop('disabled'))
            return false;

        btn.prop('disabled', true);

        loading();
        $('#form-novainteracao').ajaxSubmit({
            url: '/ocorrencia/nova-interacao',
            type: 'post',
            dataType: 'json',
            data: {
                ocorrencia: that.model.toJSON(),
                st_tramite: tinymce.editors["txt-interacao"].getContent()
            },
            uploadProgress: function (event, position, total, percentComplete) {
                $('.box-bar-progress').show();
                $('#upload-progress').css({width: percentComplete + '%'});
            },
            success: function (data) {
                if (data.tipo == 1) {
                    // loaded();
                    $.pnotify({title: data.title, text: data.text, type: 'success'});

                    view.render();
                } else {
                    // loaded();
                    $.pnotify({title: data.title, text: data.text, type: 'warning'});
                }
            },
            complete: function () {
                loaded();
                btn.prop('disabled', false);
            },
            error: function (response) {
                $.pnotify(response.responseJSON);
            }
        });
    },
    eventLoadInteracoes: function () {

        var InteracoesCollection = Backbone.PageableCollection.extend({
            url: '/ocorrencia/find-interacao/?params[id_ocorrencia]=' + this.model.get('id_ocorrencia'),
            model: VwOcorrenciaInteracao,
            mode: 'client',
            state: {
                pageSize: 999
            }
        });

        Interacoes = new InteracoesCollection();

        var columns = [
            {name: 'id_tramite', label: 'ID', editable: false, cell: 'string'},
            {name: 'st_nomecompleto', label: 'Pessoa', editable: false, cell: 'string'},
            {name: 'st_cadastro', label: 'Cadastro', editable: false, cell: 'string'}
        ];

        var pageableGrid = new Backgrid.Grid({
            className: 'backgrid table table-bordered',
            columns: columns,
            collection: Interacoes
        });

        var HtmlTramiteCell = Backgrid.Cell.extend({
            className: 'span7',
            render: function () {
                var elTemp = _.template('<div class="content">' + this.model.get('st_tramite') + '</div>');
                this.$el.html(elTemp);
                return this;
            }
        });

        var UploadCell = Backgrid.Cell.extend({
            template: _.template('-'),
            render: function () {
                if (this.model.get('st_upload')) {
                    this.template = _.template("<a class='btn-link btn' target='_blank' href='/upload/tramite/" + this.model.get('st_upload') + "'>Abrir</a>");
                }
                this.$el.html(this.template);
                return this;
            }
        });

        var cell = Backgrid.Cell.extend({
            render: function () {
                var variables = this.model.toJSON();
                var checkedSim = '';
                var checkedNao = '';

                Boolean(this.model.get('bl_visivel')) ? checkedSim = 'checked' : checkedNao = 'checked';

                var elTemp = _.template(
                    '<div class="form-inline">\n\
                        <label class="radio"> Sim<input type="radio" ' + checkedSim + ' name="radio-interacao-visivel-<%=id_tramite%>" class="btn-interacao" data-visivel="true"></label>\n\
                            <label class="radio"> Não<input type="radio" ' + checkedNao + ' name="radio-interacao-visivel-<%=id_tramite%>" class="btn-interacao" data-visivel="false"></label>\n\
                        </div>', variables);
                this.$el.html(elTemp);

                return this;
            },
            eventConfirmSetVisibilidade: function (element) {
                that = this;
                var visivel = Boolean($(element.target).data('visivel'));

                //Checando se a opcao escolhida ja se encontra marcada
                if (visivel === Boolean(this.model.get('bl_visivel'))) {
                    $.pnotify({title: 'Alerta', text: MensagensG2.MSG_OPTION_TRUE_CHECKED, type: 'warning'});
                    return false;
                }

                /**
                 * Checando se a opcao escolhida e (SIM), caso seja (NAO)
                 * executa diretamente, se for (SIM) e feita a pergunta para
                 * que o usuario faca a confirmacao
                 */
                if (visivel) {
                    bootbox.confirm(MensagensG2.MSG_OCORRENCIA_23, function (result) {
                        if (result) {
                            that.functionSetVisibilidadeInteracao(visivel, that);
                        }
                    });
                } else {
                    this.functionSetVisibilidadeInteracao(visivel, that);
                }
            },
            functionSetVisibilidadeInteracao: function (visivel) {
                loading();

                $.ajax({
                    url: '/ocorrencia/atualizar-ocorrencia-visivel',
                    data: {
                        id_tramite: that.model.get('id_tramite'),
                        bl_visivel: visivel
                    },
                    type: 'post',
                    dataType: 'json',
                    success: function (response) {
                        $.pnotify({title: 'Sucesso', text: MensagensG2.MSG_OCORRENCIA_12, type: 'success'});
                    },
                    error: function () {
                        $.pnotify({title: 'Erro', text: MensagensG2.MSG_OCORRENCIA_13, type: 'danger'});
                        loaded();
                    }
                });

                //Criando mensagem para registro de tramite em alteracao de visibilidade
                var st_visivel = visivel ? MensagensG2.MSG_OCORRENCIA_30 : MensagensG2.MSG_OCORRENCIA_31;
                var st_tramite = sprintf(MensagensG2.MSG_OCORRENCIA_29,
                    that.model.get('id_tramite'), that.model.get('st_cadastro'),
                    $('#st_nomeusuario').val(), st_visivel);

                //Gera um tramite na ocorrencia ao alterar visualizacao
                $.ajax({
                    url: '/ocorrencia/nova-interacao',
                    type: 'POST',
                    dataType: 'json',
                    data: {ocorrencia: view.model.toJSON(), st_tramite: st_tramite, bl_visivel: false},
                    success: function () {
                        $.pnotify({title: 'Sucesso', text: MensagensG2.MSG_OCORRENCIA_27, type: 'success'});
                        view.eventLoadInteracoes();
                        loaded();
                    },
                    error: function () {
                        $.pnotify({title: 'Erro', text: MensagensG2.MSG_OCORRENCIA_28, type: 'danger'});
                        loaded();
                    }
                });

            },
            events: {
                'focus .btn-interacao': 'eventConfirmSetVisibilidade'
            }
        });

        pageableGrid.insertColumn([
            {name: "st_tramite", label: 'Trâmite', editable: false, cell: HtmlTramiteCell},
            {name: "url_upload", label: 'Anexo', editable: false, cell: UploadCell},
            {name: "btn", label: 'Visível', editable: false, cell: cell}

        ]);

        $('#painel-interacoes').empty().append(pageableGrid.render().$el);

        Interacoes.fetch({reset: true});

    },
    eventLoadOcorrenciasRelacionadas: function () {
        var OcorrenciasRelacionadasCollection = Backbone.PageableCollection.extend({
            url: '/ocorrencia/find-ocorrencias-relacionadas/?id_ocorrencia=' + this.model.get('id_ocorrencia'),
            model: VwOcorrencia,
            mode: 'client',
            state: {
                pageSize: 999
            }
        });

        var ocorrenciasRelacionadas = new OcorrenciasRelacionadasCollection();

        var columnsOR = [
            {name: 'id_ocorrencia', label: '#', editable: false, cell: 'string'},
            {name: 'st_titulo', label: 'Título', editable: false, cell: 'string'},
            {name: 'st_assuntoco', label: 'SubAssunto', editable: false, cell: 'string'}
        ];

        var gridOcorrenciasRelacionadas = new Backgrid.Grid({
            className: 'backgrid table table-bordered grid-relacionadas',
            columns: columnsOR,
            collection: ocorrenciasRelacionadas
        });

        $('#ocorrencias-relacionadas').empty().append(gridOcorrenciasRelacionadas.render().$el);
        ocorrenciasRelacionadas.fetch({
            reset: true,
            complete: function () {
                if (Number(ocorrenciasRelacionadas.length)) {
                    $('#container-ocorrencias-relacionadas').removeClass('hide');
                }
            }
        });
    },
    addResponsavelOcorrencia: function () {
        var OcorrenciaResponsavelModel = new OcorrenciaResponsavel();
        that = this;

        if (!this.model.get('id_atendente') && parseInt(this.model.get('id_situacao')) === 97 && dataForm.id_funcao == 8) {

            OcorrenciaResponsavelModel.url = '/ocorrencia/add-responsavel-ocorrencia';
            OcorrenciaResponsavelModel.set({
                id_ocorrencia: this.model.get('id_ocorrencia')
            });

            OcorrenciaResponsavelModel.save(null, {
                success: function () {

                    $.pnotify({
                        title: 'Atribuição de ocorrência',
                        text: sprintf(MensagensG2.MSG_OCORRENCIA_14, OcorrenciaResponsavelModel.get('st_atendente')),
                        type: 'success'
                    });

                    //Setando dados de resultado da atribuição
                    that.model.set({
                        id_atendente: OcorrenciaResponsavelModel.get('id_usuario'),
                        st_atendente: OcorrenciaResponsavelModel.get('st_atendente'),
                        id_evolucao: OcorrenciaResponsavelModel.get('id_evolucao'),
                        id_situacao: OcorrenciaResponsavelModel.get('id_situacao'),
                        st_situacao: OcorrenciaResponsavelModel.get('st_situacao')
                    });

                    view.render();
                },
                error: function () {
                    $.pnotify({title: 'Erro', text: MensagensG2.MSG_OCORRENCIA_33, type: 'danger'});
                }
            });
        }
    },
    eventLoadAbaPrr: function () {
        $('#prr').load('/prr/index');
    },
    encaminharOcorrencia: function (renderizer) {

        if (!this.$el.find('#id_assuntoco').val()) {
            $.pnotify({
                title: 'Alerta',
                text: sprintf(MensagensG2.MSG_CAMPO_OBRIGATORIO, 'SubAssunto'),
                type: 'warning'
            });
            this.$el.find('#id_assuntoco').focus();
            return false;
        }

        if (!this.$el.find('#id_atendente_encaminhar').val()) {
            $.pnotify({
                title: 'Alerta',
                text: sprintf(MensagensG2.MSG_CAMPO_OBRIGATORIO, 'Atendente'),
                type: 'warning'
            });
            this.$el.find('#id_atendente_encaminhar').focus();
            return false;
        }

        //Tipo update 6 encaminhamento
        this.model.save({
            id_assuntocopai: $('#id_assuntocopai').val(),
            id_assuntoco: $('#id_assuntoco').val(),
            id_atendente: $('#id_atendente_encaminhar').val(),
            st_atendente: $('#id_atendente option:selected').text(),
            tipo_update: 6
        }, {
            url: "/api/ocorrencia",
            async: false,
            beforeSend: loading,
            complete: loaded,
            success: function (model) {
                if (Boolean(renderizer)) {
                    view.render();
                }
                view.model = model;
                $.pnotify({title: 'Sucesso', text: MensagensG2.MSG_OCORRENCIA_16, type: 'success'});
            },
            error: function () {
                $.pnotify({title: 'Erro', text: MensagensG2.MSG_OCORRENCIA_17, type: 'error'});
            }
        });
    },
    gerarCopiaOcorrencia: function (renderizer) {

        if (!$('#id_assuntoco_copia').val()) {
            $.pnotify({
                title: 'Alerta',
                text: sprintf(MensagensG2.MSG_CAMPO_OBRIGATORIO, 'SubAssunto cópia'),
                type: 'warning'
            });
            $('#id_assuntoco_copia').focus();
            return false;
        }

        if (!$('#id_atendente_copia').val()) {
            $.pnotify({
                title: 'Alerta',
                text: sprintf(MensagensG2.MSG_CAMPO_OBRIGATORIO, 'Atendente cópia'),
                type: 'warning'
            });
            $('#id_atendente_copia').focus();
            return false;
        }

        that.model.set({
            id: null,
            id_assuntocopai: $('#id_assuntocopai_copia').val(),
            id_assuntoco: $('#id_assuntoco_copia').val(),
            id_atendente: $('#id_atendente_copia').val(),
            st_atendente: $('#id_atendente_copia option:selected').text()
        });

        that.model.url = '/ocorrencia/gerar-copia-ocorrencia';
        that.model.save(null, {
            async: false,
            success: function (response) {
                if (Boolean(renderizer)) {
                    view.model = response;
                    view.render();
                }
                $.pnotify({title: 'Sucesso', text: MensagensG2.MSG_OCORRENCIA_18, type: 'success'});
            },
            error: function (response) {
                $.pnotify({title: 'Erro', text: MensagensG2.MSG_OCORRENCIA_19, type: 'error'});
            }
        });
    },
    updateOcorrencia: function (tipo_update) {
        that = this;

        bootbox.confirm(this.msg_confirm, function (result) {
            if (result) {

                loading();
                //Set attributo necessario para indicar diferentes tipos de alteracao
                that.model.attributes.tipo_update = tipo_update;
                that.model.url = '/api/ocorrencia/'
                that.model.save(null, {
                    success: function (response) {
                        $.pnotify({title: 'Sucesso', text: that.msg_success, type: 'success'});
                    },
                    error: function () {
                        loaded();
                        $.pnotify({title: 'Erro', text: that.msg_error, type: 'error'});
                    },
                    complete: function () {
                        view.render();
                        loaded();
                    }
                });
            }
        });
    },
    verificaPermissoes: function (id_funcao) {


        if (this.model.get('id_ocorrenciaoriginal') !== null) {

            VerificarPerfilPermissao(PERMISSAO.VINCULAR_OCORRENCIAS, FUNCIONALIDADE.OCORRENCIA, true)
                .done(function (response) {
                    this.$el.find("#btn-vincular-ocorrencia").hide().attr('disabled', 'disabled');
                    if (!view.model.get('id_ocorrenciaoriginal') && response.length) {
                        this.$el.find("#btn-vincular-ocorrencia").show().removeAttr('disabled');
                    }
                }.bind(this));
        }

        VerificarPerfilPermissao(PERMISSAO.GERAR_VENDA, FUNCIONALIDADE.OCORRENCIA, true)
            .done(function (response) {
                if (response.length) {
                    $.ajax({
                        url: '/venda/verificar-venda-ocorrencia',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            id_ocorrencia: view.model.get('id_ocorrencia'),
                        },
                        success: function (data) {
                            if (data.id_ocorrencia) {
                                this.$el.find('#btn-gerar-venda').hide().attr('disabled', 'disabled');
                            } else {
                                this.$el.find('#btn-gerar-venda').show().removeAttr('disabled');
                            }
                        }.bind(this),
                        error: function () {
                            this.$el.find('#btn-gerar-venda').hide().attr('disabled', 'disabled');
                        }.bind(this)
                    });
                } else {
                    this.$el.find('#btn-gerar-venda').hide().attr('disabled', 'disabled');
                }
            }.bind(this));

        switch (id_funcao) {
            case 8: //Atendente
                this.$el.find('#btn-save').show().removeAttr('disabled');
                this.$el.find('#btn-form-encaminhar').show().removeAttr('disabled');
                this.$el.find('#btn-encerrar').show().removeAttr('disabled');
                this.$el.find('#btn-form-novainteracao').show().removeAttr('disabled');
                this.$el.find('#btn-informar-duplicidade').show().removeAttr('disabled');
                break;

            case 9: //Responsável
                this.$el.find('#btn-save').show().removeAttr('disabled');
                this.$el.find('#btn-form-encaminhar').show().removeAttr('disabled');
                this.$el.find('#btn-form-novainteracao').show().removeAttr('disabled');
                this.$el.find('#btn-encerrar').show().removeAttr('disabled');
                break;

            case 10: //Atendente de setor
                this.$el.find('#btn-encerrar').show().removeAttr('disabled');

                if (parseInt(this.model.get('id_atendente')) === parseInt(G2S.loggedUser.get("id_usuario"))) {
                    this.$el.find('#btn-devolver-dist').show().removeAttr('disabled');
                    this.$el.find('#btn-form-novainteracao').show().removeAttr('disabled');
                    this.$el.find('#btn-save').show().removeAttr('disabled');
                    this.$el.find('#btn-form-encaminhar').show().removeAttr('disabled');
                    this.$el.find('#btn-informar-duplicidade').show().removeAttr('disabled');
                } else {
                    this.$el.find('#btn-save').hide().attr('disabled', 'disabled');
                    this.$el.find('#btn-devolver-dist').hide().attr('disabled', 'disabled');
                    this.$el.find('#btn-form-novainteracao').hide().attr('disabled', 'disabled');
                    this.$el.find('#btn-form-encaminhar').hide().attr('disabled', 'disabled');
                    this.$el.find('#btn-informar-duplicidade').hide().attr('disabled', 'disabled');
                }
                break;
        }
    },
    botoesExibicao: function () {
        this.verificaPermissaoCancelamento();
        var id_tipoocorrencia = parseInt(this.model.get('id_tipoocorrencia'));

        if (this.model.get('id_situacao') === 99) {
            $('#btn-encerrar').hide().attr('disabled', 'disabled');
            $('#btn-save').hide().attr('disabled', 'disabled');
            $('#btn-form-encaminhar').hide().attr('disabled', 'disabled');
            $('#btn-devolver-dist').hide().attr('disabled', 'disabled');
            $('#btn-form-novainteracao').hide().attr('disabled', 'disabled');
            $('#btn-reabrir').show().removeAttr('disabled');
        }

        if (id_tipoocorrencia === 3 && this.model.get('id_situacao') !== 99) {
            $('#btn-solicitar-enc').show().removeAttr('disabled');
        }
    },
    serializeForm: function () {
        var $input = $('#id_funcao');
        var funcao = $input.val().split('-');

        var funcaoNucleo = $input.find("option:selected").text().toUpperCase();
        var tipoFuncao = funcaoNucleo.indexOf("SAT") != -1 || funcaoNucleo.indexOf("TUTOR") != -1 ? "SAT" : "SAA";

        var idEntidade = $("#id_entidade").val()
            || that.model.get("id_entidadematricula")
            || that.model.get("id_entidade");

        dataForm = {
            id_ocorrencia: $('#id_ocorrencia').val(),
            st_nomeinteressado: $('#st_nomeinteressado').val(),
            st_emailinteressado: $('#st_emailinteressado').val(),
            id_funcao: funcao[0],
            id_assuntocopai: $('#id_assuntocopai').val(),
            id_evolucao: $('#id_evolucao').val(),
            dt_cadastroocorrencia_inicio: $('#dt_cadastroocorrencia_inicio').val(),
            dt_cadastroocorrencia_fim: $('#dt_cadastroocorrencia_fim').val(),
            id_atendente: $('#id_atendente').val(),
            id_assuntoco: $('#id_assuntoco').val(),
            id_situacao: $('#id_situacao').val(),
            id_entidade: idEntidade,
            id_nucleoco: funcao[1]
        };

        if (tipoFuncao === "SAA") {
            dataForm.id_entidadematricula = idEntidade;
        }
    },
    popularForm: function () {
        $.each(dataForm, function (index, value) {
            //se existir valor ou se o campo for o 'id_situacao', atualiza o campo na volta do formulário de pesquisa
            if (value || index === 'id_situacao' || index === 'id_atendente') {
                $('#' + index).val(value);
            }
        });
    },
    getContrast50: function (hexcolor) {
        if (hexcolor) {
            return (parseInt(hexcolor, 16) > 0xffffff / 2) ? '000000' : 'ffffff';
        } else {
            return '000000';
        }
    },
    mascaraCpf: function (str_cpf) {
        if (str_cpf.length && str_cpf.length >= 11) {
            var value = str_cpf.split('');
            value[2] += '.';
            value[5] += '.';
            value[8] += '-';
            return value.join('');
        } else {
            return str_cpf;
        }
    },
    eventAdaptarTamanhoCaixaTexto: function () {
        /*
         * Este evento adapta a caixa de texto ao tamanho do conteudo aplicado
         * pelo usuario para a mensagem do tramite
         */
        var alturaAtual = $('#txt-interacao').height();
        var dv = $('<div/>').html(nl2br($('#txt-interacao').val())).css('display', 'none').appendTo($('#text-input'));

        if (dv.height() !== alturaAtual) {
            $('#txt-interacao').animate({height: (dv.height() + 150)}, 300);
        } else {
            $('#txt-interacao').animate({height: (dv.height())}, 300);
        }

        dv.remove();
    },
    eventLoadOcorrenciasPessoa: function () {
        that = this;
        var VwOcorrenciasCollectionExtend = Backbone.PageableCollection.extend({
            url: "/ocorrencia/find-vw-ocorrencia?" + $.param({
                id_nucleoco: this.model.get("id_nucleoco"),
                id_usuariointeressado: this.model.get("id_usuariointeressado"),
                id_entidade: $("#id_entidade").val()
                || that.model.get("id_entidadematricula")
                || that.model.get("id_entidade")
            }),
            model: VwOcorrenciasPessoa,
            mode: 'client'
        });

        VwOcorrenciasCollection = new VwOcorrenciasCollectionExtend();

        var HtmlOpenButtonCell = Backgrid.Cell.extend({
            render: function () {
                var href = "/#/ocorrencia/" + this.model.get('id_ocorrencia');

                this.$el.html('<a href="' + href + '" target="_blank" class="btn btn-custom btn-abre-ocorrencia-relacionada" \n\
                            data-toggle="tooltip" title="Abre uma ocorrência em outra aba do navegador">Abrir</a>');
                return this;
            }
        });

        var HtmlPrintButtonCell = Backgrid.Cell.extend({
            render: function () {
                var disabled = 'disabled="disabled"';

                if (this.model.get('id_situacao') != 97) {
                    disabled = '';
                }

                this.$el.html(_.template("<button " + disabled + " class='btn btn-small btn-print-ocorrencia-inline'><i class='icon-print'></i></button>"));
                this.$el.addClass('btn-imprimir-td');
                return this;
            },
            events: {
                "click .btn-print-ocorrencia-inline": function (e) {
                    if (this.model.get('id_situacao') != 97) {
                        window.open('ocorrencia/imprimir-ocorrencia/numero/' + this.model.get('id_ocorrencia'), '_blank');
                        return false;
                    } else {
                        return false;
                    }
                }
            }
        });

        var columns = [
            {name: 'id_ocorrencia', label: '', editable: false, cell: HtmlPrintButtonCell},
            {name: 'id_ocorrencia', label: 'ID', editable: false, cell: 'string'},
            {name: 'st_assuntoco', label: 'Assunto', editable: false, cell: 'string'},
            {name: 'st_assuntocopai', label: 'SubAssunto', editable: false, cell: 'string'},
            {name: 'st_titulo', label: 'Titulo', editable: false, cell: 'string'},
            {name: 'st_evolucao', label: 'Evolução', editable: false, cell: 'string'},
            {name: 'nu_diasaberto', label: 'Dias Aberto', editable: false, cell: 'string'},
            {name: 'id_ocorrencia', label: '', editable: false, cell: HtmlOpenButtonCell}
        ];

        var pageableGrid = new Backgrid.Grid({
            className: 'backgrid table table-bordered',
            columns: columns,
            collection: VwOcorrenciasCollection
        });

        $('#div-ocorrencias-relacionadas').empty().append(pageableGrid.render().$el);

        VwOcorrenciasCollection.fetch({
            reset: true,
            before: loading(),
            complete: function () {
                loaded();
            }
        });
    },
    eventFixarPessoa: function (e) {
        $.ajax({
            dataType: "json",
            url: '/api/pessoa/get',
            data: {
                id_usuario: this.model.get('id_usuariointeressado')
            },
            success: function (response) {
                // Como busca pelo id_usuario, só retorna um elemento no array.
                response = response[0];
                //Preenchendo id utilizado no alocar
                response.id = response.id_usuario;
                localStorage.setItem('pessoaAutocomplete', JSON.stringify(response));

                //Desabilita o botao para que o usuario nao clique mais do que o necessario
                $(e.currentTarget).attr('disabled', true);

                $.pnotify({
                    title: 'Sucesso',
                    text: sprintf(MensagensG2.MSG_OCORRENCIA_34, response.st_nomecompleto),
                    type: 'success'
                });
            }
        });
    },
    eventInformarDuplicidade: function (e) {
        var that = this;
        if (viewModalDuplicidade) {
            viewModalDuplicidade.undelegateEvents();
        }
        var modelDuplicidadeModel = new DuplicidadeModel();
        modelDuplicidadeModel.set({
            'id_ocorrencia': that.model.get('id_ocorrencia'),
            'st_ocorrencia': that.model.get('st_ocorrencia')
        });
        viewModalDuplicidade = new ModalDuplicidadeView({
            model: modelDuplicidadeModel,
            el: that.$el.find("#container-modal-duplicidade")
        });
        viewModalDuplicidade.render();

    },
    events: {
        "click #btn-form-encaminhar": function (el) {
            $('#form-encaminhar').slideToggle();

            /* Verificando se formulario ja nao se encontra aberto para que seja
             * feita ou descartada a requisicao das informacoes */
            var isActive = $(el.currentTarget).hasClass('active');

            if (!isActive) {
                this.eventLoadAssunto();
                this.eventLoadSubAssunto();
                this.eventLoadAtendenteEncaminhar();
            }
        },
        "click #checkbox-copiaencaminhar": function (e) {

            $('#form-copiaencaminhar').slideToggle();

            //Verificando se o checkbox para copia esta marcado como verdadeiro
            if ($(e.target).is(':checked')) {

                //Removendo atributo disabled do formulario de copia
                $('#form-copiaencaminhar select').removeAttr('disabled');

                //Preenchendo combos para copia
                this.eventLoadAssuntoCopia(false);
                this.eventLoadAtendenteCopia();
            } else {
                $('#form-copiaencaminhar select').attr('disabled', 'disabled');
            }
        },
        "click #btn-form-novainteracao": function () {
            $('#form-novainteracao').slideToggle();

            if (tinymce.editors["txt-interacao"]) {
                tinymce.remove("#txt-interacao");
            }

            //Inicializa componente TinyMCE
            tinymce.init({
                selector: "#txt-interacao",
                menubar: false,
                plugins: "paste",
                theme_advanced_buttons3_add: "pastetext,pasteword,selectall",
                paste_auto_cleanup_on_paste: true,
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
            });

        },
        "click #btn-selecione-me": function () {
            $('#id_atendente').val($('#id_usuario').val());
        },
        "change #id_assuntoco": function () {
            if (this.is_editing) {
                this.eventLoadAtendenteEncaminhar();
            }
        },
        "click #btn-editar-evolucao": function () {
            this.$el.find('#label-cmb-id-evolucao').slideToggle();
        },
        "click #btn-save": "eventSave",
        "click #btn-back": "eventBack",
        "submit #form-search-filter": "eventPesquisar",
        "change #id_assuntocopai": "eventLoadSubAssunto",
        "click .btn-interacao": "eventSetVisibilidadeInteracao",
        "change #id_funcao": "eventChangeAtendenteAssunto",
        "click #btn-encerrar": "eventEncerrarOcorrencia",
        "click #btn-devolver-dist": "eventDevolverOcorrencia",
        "click #btn-reabrir": "eventReabrirOcorrencia",
        "click #btn-solicitar-enc": "eventSolicitarEncerramento",
        "change #id_assuntocopai_copia": "eventLoadSubAssuntoCopia",
        "click #btn-encaminhar": "eventEncaminharGerarCopia",
        "click #btn-gerar-interacao": "eventGerarNovaInteracao",
        "change #id_assuntoco_copia": "eventLoadAtendenteCopia",
        "click #tab-prr": "eventLoadAbaPrr",
        "keyup #txt-interacao": "eventAdaptarTamanhoCaixaTexto",
        "click #btn-mais-ocorrencia-usuario": "eventLoadOcorrenciasPessoa",
        "click #btn-fixar-pessoa": "eventFixarPessoa",
        'click #acompanhadores-toggle': 'acompanhadoresToggle',
        'click #btn-informar-duplicidade': 'eventInformarDuplicidade',
        'click #servicos-toggle': 'servicosToggle',
        'click #btn-transferencia-curso': "eventFixarPessoa",
        "click #btn-cancelamento": "abrirCancelamento",
        'click #btn-vincular-ocorrencia': 'vincularOcorrencia',
        'click #btn-gerar-venda': 'gerarVenda',
        'click #btn-imprimir-xls': 'gerarXls',
        "change #id_entidade": function () {
            this.eventLoadFuncao(true);
        }
    },
    verificaPermissaoCancelamento: function () {

        //extende a model para reescrever o parse
        var NucleoModel = NucleoCo.extend({
            parse: function (response) {
                if ((typeof response.mensagem !== "undefined")
                    && (typeof response.mensagem[0] !== "undefined")
                    && response.mensagem[0] !== false) {
                    return response.mensagem[0];
                }
            }
        });

        /*faz o fetch do nucleo passando os dados para verificar se o assunto e nucleo da ocorrencia permitem iniciar o
         processo de cancelamento*/
        var id_assuntoco = this.model.get('id_assuntoco');

        if (Array.isArray(id_assuntoco)) {
            id_assuntoco = this.model.get('id_assuntoco')[0];
        }


        (new NucleoModel()).fetch({
            beforeSend: loading,
            complete: loaded,
            data: {
                "id_nucleoco": this.model.get("id_nucleoco"),
                "id_assuntocancelamento": id_assuntoco
            },
            success: function (model) {
                if (model.get("id_assuntocancelamento")
                    && parseInt(G2S.loggedUser.get("id_linhadenegocio")) === LINHA_DE_NEGOCIO.POS_GRADUACAO) {
                    this.$el.find("#btn-cancelamento").show();
                }
            }.bind(this)
        });
    },
    abrirCancelamento: function (e) {

        //busca os dados da funcionalidade
        (new Funcionalidade())
            .set("id", FUNCIONALIDADE.CANCELAMENTO_POS)
            .fetch({
                beforeSend: function () {
                    loading();

                }.bind(this),
                complete: loaded,
                success: function (model) {
                    VerificarPerfilPermissao (PERMISSAO.ADICIONAR , FUNCIONALIDADE.CANCELAMENTO_POS, true)
                        .done(function (response) {
                            if (response.length) {
                                this.eventFixarPessoa(e);//fixa a pessoa no storage para pegar no autocomplete
                                localStorage.setItem("ocorrencia_cancelamento", JSON.stringify(this.model.toJSON()));
                                //redireciona para a url da funcionalidade
                                G2S.router.navigate("#" + model.get("st_urlcaminho"), {trigger: false});
                            } else {
                                $.pnotify({
                                    type: 'warning',
                                    title: 'Atenção!',
                                    text: 'Perfil não Possui permissão para iniciar Processo de Cancelamento.'
                                });
                            }

                        }.bind(this));

                }.bind(this)
            });
    },
    acompanhadoresEditing: false,
    acompanhadoresToggle: function () {
        if (!this.acompanhadoresEditing) {
            var AcompanhadoresIni = new Acompanhadores();
            $('#acompanhadores-show').html(AcompanhadoresIni.render().$el);

            $('#acompanhadores-added').attr("data-idocorrencia", that.model.get('id'));
            var ListaAcompanhadoresCollection = Backbone.Collection.extend({
                model: AcompanhadoresModel,
                url: '/minhas-ocorrencias/lista-acompanhadores/id/' + this.model.get('id')
            });

            var Initialize = new ListaAcompanhadoresCollection();
            Initialize.fetch({
                success: function () {
                    var renderer = new CollectionView({
                        collection: Initialize,
                        childViewConstructor: AcompanhadorAddedItemView,
                        childViewTagName: 'span',
                        el: $('#acompanhadores-added')
                    });
                    renderer.render().$el;

                }
            });
        } else {
            $('#acompanhadores-show').html('');
        }

        this.acompanhadoresEditing = !this.acompanhadoresEditing;
    },
    servicosToggle: function () {
        var container = $('#container-servicos');
        if (!container.length) {
            var modelo = new ServicosModel();

            modelo.url = 'ocorrencia/get-servicos/id_ocorrencia/' + this.model.get('id_ocorrencia');
            modelo.fetch({
                complete: function () {
                    var compositeServicos = new ServicosCompositeView({
                        el: '#servicos-show',
                        model: modelo,
                        collection: new ServicosCollection(modelo.get('st_produto'))
                    });
                    compositeServicos.render();
                }
            });
        } else {
            container.toggle();
        }
    },
    vincularOcorrencia: function () {

        var modal = new ModalOcorrenciaVinculada({
            model: this.model,
            el: that.$el.find("#modalOcorrencia")
        });

        modal.render();
    },

    gerarVenda: function () {
        $.ajax({
            url: '/venda/salvar-venda-ocorrencia',
            type: 'POST',
            dataType: 'json',
            data: {
                id_ocorrencia: this.model.get('id_ocorrencia'),
                id_usuariointeressado: this.model.get('id_usuariointeressado'),
            },
            success: function (data) {
                if (data.id_venda) {
                    this.$el.find('#btn-gerar-venda').hide().attr('disabled', 'disabled');
                    window.open("#CadastrarVenda/editar/" + data.id_venda, "_blank");
                    view.render();

                }
            }.bind(this),
            error: function () {
                $.pnotify({title: 'Erro', text: 'Erro ao atualizar Evolução do Serviço', type: 'danger'});

            }
        });

    },

    gerarXls: function (e) {
        e.preventDefault();

        this.serializeForm();

        var campos = [];
        for (var campo in dataForm) {
            if (null != dataForm[campo] || "" != $.trim(dataForm[campo])) {
                campos.push(campo + '=' + dataForm[campo]);
            }
        }

        var url = '/ocorrencia/gerar-xls/?' + campos.join('&');

        $("#download-ocorrencias").attr("action", url);
        $("#download-ocorrencias").trigger('submit');

        $("#download-ocorrencias").attr("action", "#");

        return false;
    }
});

var ModalOcorrenciaVinculada = Marionette.ItemView.extend({
    template: "#modal-ocorrencia-vinculada",
    tagName: "div",
    className: "modal hide fade",
    ui: {
        tipoOcorrencia: "#tipo-ocorrencia-holder",
        nucleoco: "#nucleo-holder",
        assuntoco: "#assunto-holder",
        subassuntoco: "#sub-assunto-holder",
        tituloOcorrencia: "#st_tituloocorrencia",
        txt_ocorrencia: "#txt-ocorrencia",
        gravarOcorrenciaVinculada: '#gravar-ocorrencia-vinculada',
        form: '#form-nova-ocorrencia',
    },
    events: {
        "change @ui.assuntoco": "subAssuntoCo",
        "click @ui.gravarOcorrenciaVinculada": "gravarNovaOcorrencia",
    },
    onRender: function () {
        this.$el.modal("show");
        this.tipoOcorrencia();
        this.assuntoCo();
        return this;
    },

    validarForm: function () {

        var error = false;

        if (this.ui.txt_ocorrencia.val() === "") {
            error = 'Campo texto é obrigatorio';
        }

        if (this.ui.tituloOcorrencia.val() === "") {
            error = 'Campo titulo ocorrência é obrigatorio';
        }

        if (this.ui.assuntoco.val() === "") {
            error = 'Campo assunto é obrigatorio';
        }

        if (this.ui.tipoOcorrencia.val() === "") {
            error = 'Campo tipo ocorrencia é obrigatorio';
        }

        if (error) {
            $.pnotify({
                title: 'Atenção',
                text: error,
                type: 'warning'
            });
            return false;
        }
        return true;
    },
    gravarNovaOcorrencia: function () {
        var oc = new Ocorrencia();
        if (this.validarForm()) {
            oc.set({
                "st_titulo": this.ui.tituloOcorrencia.val(),
                "st_ocorrencia": this.ui.txt_ocorrencia.val(),
                "id_ocorrenciaoriginal": this.model.get("id_ocorrencia"),
                "id_evolucao": EVOLUCAO.TB_OCORRENCIA.AGUARDANDO_ATENDIMENTO,
                "id_situacao": SITUACAO.TB_OCORRENCIA.PENDENTE,
                "id_motivoocorrencia": SITUACAO.TB_OCORRENCIA.MOTIVO_OCORRENCIA,
                "id_assuntoco": this.ui.assuntoco.val(),
                "id_assuntocopai": this.ui.subassuntoco.val(),
                "id_categoriaocorrencia": this.ui.tipoOcorrencia.val(),
                "id_matricula": this.model.get("id_matricula"),
                "ocorrencia_vinculada": true
            });

            loading();
            this.ui.form.ajaxSubmit({
                url: '/api/ocorrencia',
                type: 'post',
                dataType: 'json',
                data: {ocorrencia: oc.toJSON()},
                uploadProgress: function (event, position, total, percentComplete) {
                    $('.box-bar-progress').show();
                    $('#upload-progress-ocorrencia').css({width: percentComplete + '%'});
                },
                success: function (data) {
                    $.pnotify({title: data.title, text: "Ocorrência Criada com Sucesso!", type: 'success'});
                    loaded();
                },
                error: function (data) {
                    $.pnotify({
                        title: 'Atenção',
                        text: 'Uma ocorrência com o mesmo assunto foi criada recentemente.',
                        type: 'warning'
                    });
                    loaded();
                }
            });
            this.$el.modal("hide");
            view.render();
        } else {
            return false;
        }


    },

    subAssuntoCo: function () {
        var SubassuntoCoCollection = Backbone.Collection.extend({
            model: AssuntoCo,
            url: "/ocorrencia/find-sub-assunto"
        });

        var collection = new SubassuntoCoCollection();
        collection.fetch({
            data: {
                id_assuntocopai: this.ui.assuntoco.val(),
                id_entidade: $("#id_entidade").val()
                || that.model.get("id_entidadematricula")
                || that.model.get("id_entidade")
            },
            success: function () {
                var view = new SelectView({
                    el: this.ui.subassuntoco,
                    collection: collection,
                    childViewOptions: {
                        optionLabel: "st_assuntoco",
                        optionValue: "id_assuntoco",
                        optionSelected: null
                    }
                });
                view.render();
            }.bind(this)

        });
        loaded();
    },

    assuntoCo: function () {

        var AssuntoCoCollection = Backbone.Collection.extend({
            model: AssuntoCo,
            url: "/ocorrencia/find-assunto"
        });
        loading();
        var collection = new AssuntoCoCollection();
        collection.fetch({
            data: {
                params: {
                    id_nucleoco: this.model.get('id_nucleoco'),
                    id_entidade: $("#id_entidade").val()
                    || that.model.get("id_entidadematricula")
                    || that.model.get("id_entidade")
                }
            },
            success: function () {
                var view = new SelectView({
                    el: this.ui.assuntoco,
                    collection: collection,
                    childViewOptions: {
                        optionLabel: "st_assuntoco",
                        optionValue: "id_assuntoco",
                        optionSelected: null
                    }
                });
                view.render();
            }.bind(this)

        });
        loaded();
    },
    tipoOcorrencia: function () {
        loading();
        var TipoOcorrenciaCollection = Backbone.Collection.extend({
            model: TipoOcorrencia,
            url: '/api/tipo-ocorrencia'
        });

        var collection = new TipoOcorrenciaCollection();
        collection.fetch({
            data: {
                id_entidadecadastro: $("#id_entidade").val()
                || that.model.get("id_entidadematricula")
                || that.model.get("id_entidade")
            },
            success: function () {
                var view = new SelectView({
                    el: this.ui.tipoOcorrencia,
                    collection: collection,
                    childViewOptions: {
                        optionLabel: "st_categoriaocorrencia",
                        optionValue: "id_tipoocorrencia",
                        optionSelected: null
                    }
                });
                view.render();
            }.bind(this)

        });
        loaded();
    }
});

//Chamada da view geral de ocorrencias
var view = new MyView();

/*
 * Registrando rotas
 */
var AppRouter = Backbone.Router.extend({
    routes: {
        "ocorrencia/:id": "ocorrencia",
        "": ""
    },
    "ocorrencia": function (id) {
        if (view.is_editing && id != 0) {
            view.render();
        } else {
            view.is_editing = false;
            view.render();
        }
    },
    "": function () {
        view.is_editing = false;
        view.render();
    }
});

var app_router = new AppRouter();

$('#txt-interacao').bind('paste', function (e) {
    view.eventAdaptarTamanhoCaixaTexto();
});

$('#btn-mais-ocorrencia-usuario').popover({trigger: 'hover'});
$('.btn-tooltip').tooltip({trigger: 'hover'});

/**
 * Servicos
 */

var ServicosModel = Backbone.Model.extend({
    defaults: {
        'st_codigorastreio': '',
        'st_produto': [],
        'totalProdutos': ''
    }
});

var ProdutoServicoModel = Backbone.Model.extend({
    defaults: {
        'id_produto': '',
        'st_produto': '',
        'nu_valorproduto': '',
        'id_textosistema': '',
        'id_matricula': '',
        'nu_gratuito': ''
    }
})

var ServicosCollection = Backbone.Collection.extend({
    model: ProdutoServicoModel,
    url: '/ocorrencia/get-servicos/'
});

var ServicosRowView = Marionette.ItemView.extend({
    tagName: "tr",
    template: "#row-servicos",
    ui: {
        'select_evolucao_servico': '#id_situacao_servico_combo'
    },
    events: {
        'change #id_situacao_servico_combo': 'changeSelectService'
    },
    onRender: function () {
        this.verificaSituacao();
    },
    verificaSituacao: function () {
        var that = this;
        if (this.model.get('id_situacao') == 80) {
            that.ui.select_evolucao_servico.prop('disabled', true)
        }
    },
    changeSelectService: function () {
        var that = this;
        if (this.ui.select_evolucao_servico.val() == 80) {
            bootbox.confirm('Deseja realmente alterar a evolução do produto para entregue?', function (result) {
                if (result) {
                    that.ui.select_evolucao_servico.prop('disabled', true);
                    $.ajax({
                        url: '/ocorrencia/salvar-evolucao-servico',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            id_produto: that.model.get('id_produto'),
                            id_ocorrencia: that.model.get('id_ocorrencia'),
                            id_situacao: 80
                        },
                        success: function () {
                            $.pnotify({
                                title: 'Sucesso',
                                text: 'Atualização da Evolução do Serviço efetuada com sucesso!',
                                type: 'success'
                            });
                            view.eventLoadInteracoes();
                            loaded();
                        },
                        error: function () {
                            $.pnotify({title: 'Erro', text: 'Erro ao atualizar Evolução do Serviço', type: 'danger'});
                            loaded();
                        }
                    });
                }
            });
        } else {
            $.ajax({
                url: '/ocorrencia/salvar-evolucao-servico',
                type: 'POST',
                dataType: 'json',
                data: {
                    id_produto: this.model.get('id_produto'),
                    id_ocorrencia: this.model.get('id_ocorrencia'),
                    id_situacao: this.ui.select_evolucao_servico.val()
                },
                success: function () {
                    $.pnotify({
                        title: 'Sucesso',
                        text: 'Atualização da Evolução do Serviço efetuada com sucesso!',
                        type: 'success'
                    });
                    view.eventLoadInteracoes();
                    loaded();
                },
                error: function () {
                    $.pnotify({title: 'Erro', text: 'Erro ao atualizar Evolução do Serviço', type: 'danger'});
                    loaded();
                }
            });
        }
    }
})

var NoServiceView = Backbone.Marionette.ItemView.extend({
    tagName: 'tr',
    template: "#no-service"
});

var ServicosCompositeView = Marionette.CompositeView.extend({
    childView: ServicosRowView,
    childViewContainer: "tbody",
    template: "#table-servicos",
    emptyView: NoServiceView,
    events: {
        'click #btn_salvarRastreio': 'salvarRastreio'
    },
    ui: {
        'st_rastreio': '#st_codigorastreio'
    },
    salvarRastreio: function () {
        $.ajax({
            url: '/ocorrencia/salvar-codigo-rastreio',
            type: 'POST',
            dataType: 'json',
            data: {
                st_codigorastreio: this.ui.st_rastreio.val(),
                id_ocorrencia: view.model.get('id_ocorrencia')
            },
            success: function () {
                $.pnotify({title: 'Sucesso', text: 'Código de Rastreio cadastrado com sucesso!', type: 'success'});
                view.eventLoadInteracoes();
                loaded();
            },
            error: function () {
                $.pnotify({title: 'Erro', text: 'Erro ao cadastrar Código de Rastreio', type: 'danger'});
                loaded();
            }
        });
    }
});

/**
 * Acompanhadores
 */
var Acompanhadores = Marionette.ItemView.extend({
    tagName: 'div',
    template: '#acompanhadores-template',
    initialize: function () {
        _.bind(this, 'render');
    },
    events: {
        'keyup #acompanhadores-search': 'pesquisaPessoas'
    },
    pesquisaPessoas: function (e) {
        var elem = $(e.target);
        var value = elem.val().trim();

        if (value.length) {
            AcompanhadoresIni = new AcompanhadoresCompositeView();
            AcompanhadoresIni.collection.reset();
            AcompanhadoresIni.collection.url = '/minhas-ocorrencias/lista-pessoas/?str=' + value;
            AcompanhadoresIni.collection.fetch({
                success: function (model, response) {
                    loaded();
                }
            });

            $('#acompanhadores-list').html(AcompanhadoresIni.render().$el);
        } else {
            $('#acompanhadores-list').html('');
        }
    }
});

var AcompanhadoresModel = Backbone.Model.extend({
    idAttribute: 'id_usuario',
    defaults: {
        'id_usuario': null,
        'st_nomecompleto': ''
    }
});

var AcompanhadoresCollection = Backbone.Collection.extend({
    model: AcompanhadoresModel,
    url: '/minhas-ocorrencias/lista-pessoas/'
});

// Esta view é referente a cada acompnhador adicionado
var AcompanhadorAddedItemView = Marionette.ItemView.extend({
    tagName: 'span',
    model: new AcompanhadoresModel(),
    template: _.template(
        '<div class="label label-info" style="margin: 0 10px 10px 0;">' +
        '<%= st_nomecompleto %> ' +
        '<input type="hidden" name="acompanhadores" value="<%= id_usuario %>" />' +
        '<button class="btn btn-small" type="button" title="Remover">' +
        '<i class="icon-trash"></i>' +
        '</button>' +
        '</div>'
    ),
    events: {
        'click .btn': 'remove'
    },
    remove: function () {
        this.$el.remove();

        atualizaAcompanhadores();

    }
});

function atualizaAcompanhadores() {
    if ($('#acompanhadores-added').attr('data-idocorrencia')) {
        var id = $('#acompanhadores-added').attr('data-idocorrencia');
        var url = '/minhas-ocorrencias/atualiza-acompanhadores?id_ocorrencia=' + id;

        $('[name="acompanhadores"]').each(function () {
            url += ('&acompanhadores[]=' + $(this).val());
        });

        $.get(url);
    }
}

var AcompanhadoresView = Marionette.ItemView.extend({
    tagName: 'tr',
    initialize: function () {
        _.bind(this, 'render');
    },
    template: _.template(
        '<td><%= st_nomecompleto %></td>' +
        '<td width="100px">' +
        '<button id="acompanhadores-add" class="btn btn-small" type="button">' +
        '<i class="icon-plus"></i> ' +
        'Adicionar' +
        '</button>' +
        '</td>'
    ),
    events: {
        'click #acompanhadores-add': 'add'
    },
    add: function () {
        var values = {
            'id_usuario': this.model.get('id_usuario'),
            'st_nomecompleto': this.model.get('st_nomecompleto')
        };

        var ItemViewIni = new AcompanhadorAddedItemView();
        ItemViewIni.model.set(values);
        $('#acompanhadores-added').append(ItemViewIni.render().$el);

        atualizaAcompanhadores();

        $('#acompanhadores-search').val('');
        $('#acompanhadores-list').html('');
    }
});

var AcompanhadoresCompositeView = Marionette.CompositeView.extend({
    template: '#acompanhadores-list-template',
    collection: new AcompanhadoresCollection(),
    childView: AcompanhadoresView,
    childViewContainer: 'tbody'
});


var DuplicidadeModel = Backbone.Model.extend({
    defaults: {
        id_ocorrencia: '',
        st_ocorrencia: '',
        st_naotratada: '',
        st_tratada: ''
    },
    idAttribute: 'id_ocorencia'
});

var ModalDuplicidadeView = Marionette.CompositeView.extend({
    template: "#template-modal-duplicidade",
    ui: {
        st_naotratada: 'input[name="st_naotratada"]',
        st_tratada: 'input[name="st_tratada"]'
    },
    onRender: function () {
        this.$el.find('#modal-duplicidade').modal('show');
        return this;
    },
    validaDuplicidade: function () {
        var valid = true;
        var that = this;
        var modelEntidadeIntegracao = new ConfiguracaoEntidade();
        modelEntidadeIntegracao.url = '/entidade/get-configuracao-entidade/id/' + G2S.loggedUser.get('id_entidade');
        modelEntidadeIntegracao.fetch({
            beforeSend: function () {
                loading();
            },
            complete: function () {
                loaded();
            },
            success: function () {
                var id_assuntoduplicidade = modelEntidadeIntegracao.get('id_assuntoduplicidade');
                if (typeof id_assuntoduplicidade != 'object' || !Number(id_assuntoduplicidade.id_assuntoco)) {
                    $.pnotify({
                        type: 'warning',
                        title: 'Atenção!',
                        text: 'Essa entidade não tem assunto para duplicidade cadastrado. Verifique com o setor responsável.'
                    });
                    valid = false;
                }
            }

        });
        var st_tratada = that.ui.st_tratada.val();
        var st_naotratada = that.ui.st_naotratada.val();
        if (!st_tratada) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Informe o assunto a ser tratado na ocorrência.'
            });
            valid = false;
            that.ui.st_tratada.focus;
        }

        if (!st_naotratada) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Informe o assunto a NÃO ser tratado na ocorrência.'
            });
            valid = false;
        }

        return valid;
    },
    salvarDuplicidade: function (e) {
        e.preventDefault();
        var that = this;
        if (this.validaDuplicidade()) {
            var modelSave = new DuplicidadeModel();
            modelSave.set({
                'st_tratada': that.ui.st_tratada.val(),
                'st_naotratada': that.ui.st_naotratada.val(),
                'id_ocorrencia': that.model.get('id_ocorrencia'),
                'st_ocorrencia': that.model.get('st_ocorrencia')
            });

            modelSave.url = '/ocorrencia/salvar-duplicidade';
            modelSave.save(null, {
                beforeSend: function () {
                    loading();
                },
                complete: function () {
                    loaded();
                },
                success: function (model, response) {
                    $.pnotify({
                        title: response.title,
                        text: response.text + ' Ocorrência criada para duplicidade: ' + response.codigo,
                        type: response.type
                    });
                    view.render();
                    that.$el.find("#modal-duplicidade").modal('hide');
                    loaded();
                },
                error: function (model, response) {
                    $.pnotify({
                        title: 'Erro!',
                        text: 'Houve um erro ao tentar salvar a duplicidade.',
                        type: 'error'
                    });
                }
            });
        }

    },
    events: {
        'submit form#form-duplicidade': 'salvarDuplicidade'
    }
});

/******************************************************************************
 *              Grade Horaria Entidade
 *              Neemias Santos <neemias.santos@unyleya.com.br>
 *              08/09/2015
 ******************************************************************************/

/**
 ******************************************************************************************
 ******************************************************************************************
 ******************************** GLOBAIS *************************************************
 ******************************************************************************************
 ******************************************************************************************
 */

var itensCollection;

/**
 * Model criada para select de professores
 * @type Model @exp;Backbone@pro;Model@call;extend
 */
var SelectProfessorModel = Backbone.Model.extend({});

/**
 * @type Collection para Select de Professores @exp;Backbone@pro;Collection@call;extend
 */
var SelectProfessorCollection = Backbone.Collection.extend({
    model: SelectProfessorModel
});

/**
 ******************************************************************************************
 ******************************************************************************************
 *************************** LAYOUT FUNCIONALIDADE ****************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var TelaGradeLayout = Marionette.LayoutView.extend({
    template: '#template-layout-grade-tirinhas',
    tagName: 'div',
    regions: {
        formgrade: '#regiao-form-grade',
        datagrade: '#regiao-grade-data',
    },
    initialize: function () {

    },
    onShow: function () {
        this.renderizaFormGrade();
        $('#container-DataGrid').hide();
        return this;
    },
    renderizaFormGrade: function () {
        var gradeView = new FormGradeCompositeView({});
        this.formgrade.show(gradeView);
    }
});

/**
 ******************************************************************************************
 ******************************************************************************************
 ************************************** COMPOSITES ****************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var FormGradeCompositeView = Marionette.CompositeView.extend({
    initialize: function () {

    },
    template: '#template-form-grade-tirinhas',
    tagName: 'fieldset',
    ui: {
        'id_professor': '#id_professor',
        'dt_inicial': '#dt_inicial',
        'dt_termino': '#dt_termino'
    },
    events: {
        'click #btnPesquisar': 'pesquisaDados',
        'click #btnImprimir': 'imprimir'
    },
    onShow: function () {
        this.populaProfessor();
    },
    populaProfessor: function () {
        that = this;
        var collectionSelect = new SelectProfessorCollection();
        if (typeof (Professores) != 'undefined') {
            collectionSelect = new SelectProfessorCollection(Professores);
        }
        var view = new SelectView({
            el: that.$el.find("select[name='id_professor']"),        // Elemento da DOM
            collection: collectionSelect,      // Instancia da collection a ser utilizada
            childViewOptions: {
                optionLabel: 'st_nomecompleto', // Propriedade da Model que será utilizada como label do select
                optionValue: 'id_usuario', // Propriedade da Model que será utilizada como id do option do select
                optionSelected: null       // ID da option que receberá o atributo "selected"
            }
        });
        view.render();
        this.ui.id_professor.select2();
    },
    pesquisaDados: function () {
        var that = this;
        if (that.ui.dt_inicial.val() && !that.ui.dt_termino.val() || that.ui.dt_termino.val() && !that.ui.dt_inicial.val()) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Preencha os dois campos de Períodos.'
            });
            return false;
        } else {

            loading();

            var columns = [{
                name: "bl_imprimir",
                cell: "select-row",
                className: 'bl_imprimir',
                editable: false,
                headerCell: "select-all"
            }, {
                name: "st_nomeentidade",
                label: "Entidade",
                editable: false,
                cell: 'string'
            }, {
                name: "periodo",
                label: "Período",
                editable: false,
                cell: 'string'
            }, {
                name: "st_nomecompleto",
                label: "Nome do Professor",
                editable: false,
                cell: 'string'
            }, {
                name: "id_gradehoraria",
                label: "Grade",
                editable: false,
                cell: 'string'
            }];

            var PageableLog = Backbone.PageableCollection.extend({
                model: VwMatricula,
                url: '/grade-horaria/professores-tirinha',
                state: {
                    pageSize: 30
                },
                mode: "client",
            });
            var data = {
                'dt_inicial': that.ui.dt_inicial.val(),
                'dt_termino': that.ui.dt_termino.val(),
                'id_professor': that.ui.id_professor.val()
            };

            pageableLog = new PageableLog();
            itensCollection = new PageableLog();

            var RowGrid = Backgrid.Row.extend({
                initialize: function (options) {
                    RowGrid.__super__.initialize.apply(this, arguments);
                },
                events: {
                    "change input[type='checkbox']": 'selected'
                },
                selected: function () {
                    check = this.$el.find('input[type="checkbox"]').is(":checked");

                    if (check) {
                        this.model.set('attr_check', 1);
                        itensCollection.add(this.model);
                    } else {
                        this.model.set('attr_check', 0);
                        itensCollection.remove(this.model);
                    }
                }
            });

            var pageableGridLog = new Backgrid.Grid({
                className: 'backgrid table table-bordered',
                columns: columns,
                collection: pageableLog,
                emptyText: "Nenhum registro encontrado",
                row: RowGrid
            });

            $('#container-DataGrid').show();
            var $elemento = $('#template-data-grade');
            $elemento.empty();
            $elemento.append(pageableGridLog.render().$el);

            var paginator = new Backgrid.Extension.Paginator({
                collection: pageableLog
            });

            $elemento.append(paginator.render().el);
            $('.container-DataGrid').show();
            pageableLog.fetch({
                data: data,
                reset: true
            });
        }
        loaded();
    },
    imprimir: function () {
        if (itensCollection.length == 0) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Selecione os professores para imprimir.'
            });
            return false;
        } else {
            var arrDados = [];
            itensCollection.each(function (model, i) {
                if (model.get('attr_check')) {
                    arrDados[i] = model.get('id_gradehoraria') + '-' + model.get('id_usuario') + '-' + model.get('id_unidade');
                }
            });

            window.open("/grade-horaria/imprimir-todas-tirinhas-combo/data/" + arrDados, '_blank');
        }
    }
});

/*********************************************
 ************** INICIALIZAÇÃO ****************
 *********************************************/

var layoutGrade = new TelaGradeLayout();
G2S.show(layoutGrade);

/**
 * INSTÂNCIAS
 */
var layoutInit,
    formInit,
    financeiroInit,
    botoesInit;

/**
 * Inicializações (para o Codacy)
 */
var LayoutView,
    InfoMatriculaView,
    FormView,
    FinanceiroView,
    BotoesView,
    LancamentoCollection,
    LancamentoItem,
    LancamentoComposite;

/**
 * Permissão
 */
var blTransferirInadimplente;

/**
 * CLASSES
 */
var VendaGraduacao = Venda.extend({
    defaults: {
        // Origem
        id_venda: null,
        id_contratoregra: null,
        st_contratoregra: null,
        id_campanhacomercial: null,
        st_campanhacomercial: null,
        id_campanhapontualidade: null,
        st_campanhapontualidade: null,
        // Destino
        st_nomeentidadedestino: null,
        st_turmadestino: null,
        id_contratoregradestino: null,
        st_contratoregradestino: null,
        id_campanhacomercialdestino: null,
        st_campanhacomercialdestino: null,
        id_campanhapontualidadedestino: null,
        st_campanhapontualidadedestino: null
    },
    idAttribute: "id_venda",
    url: function () {
        return "/api/venda/" + (this.id || "");
    }
});

/**
 * MODELS
 */
var modelMatricula = new VwMatricula(),
    modelVenda = new VendaGraduacao(),
    modelPolo = new Entidade(),
    modelTurma = new VwTurma();

/**
 * COLLECTION
 */
var colLancVencidos,
    colLancAtuais;

LayoutView = Marionette.LayoutView.extend({
    el: "#layout-region",
    regions: {
        autocomplete: "#autocomplete-region",
        form: "#form-region",
        financeiro: "#financeiro-region",
        botoes: "#botoes-region"
    },
    initialize: function () {
        layoutInit = this;
        var that = this;

        componenteAutoComplete.main({
            model: VwPesquisarPessoa,
            element: that.autocomplete.$el,
            urlApi: "/api/pessoa/id_evolucao/" + EVOLUCAO.TB_MATRICULA.CURSANDO
        }, function () {
            that.model = new Backbone.Model(JSON.parse(localStorage.getItem("pessoaAutocomplete")));
            that.mostrarForm();
        }, function () {
            that.esconderForm();
        });
    },
    mostrarForm: function () {
        formInit = new FormView();
        this.form.show(formInit);
    },
    esconderForm: function () {
        this.form.empty();
        this.financeiro.empty();
        this.botoes.empty();
    }
});

InfoMatriculaView = Marionette.ItemView.extend({
    template: "#matricula-template",
    model: modelMatricula
});

FormView = Marionette.LayoutView.extend({
    template: "#form-template",
    model: new Backbone.Model(),
    regions: {
        infoMatricula: "#matricula-region"
    },
    ui: {
        labelMatricula: "#label-matricula",
        inputMatricula: "#id_matricula",
        labelPolo: "#label-polo",
        searchPolo: "#st_nomeentidade",
        inputPolo: "#id_entidade",
        labelTurma: "#label-turma",
        searchTurma: "#st_tituloexibicao",
        inputTurma: "#id_turma"
    },
    events: {
        "change @ui.inputMatricula": "carregarPolos",
        "change @ui.inputMatricula,@ui.inputPolo": "carregarTurmas",
        "change @ui.inputMatricula,@ui.inputPolo,@ui.inputTurma": "carregarFinanceiro"
    },
    onRender: function () {
        this.buscarPermissoes();
        this.carregarMatriculas();
    },
    buscarPermissoes: function () {
        VerificarPerfilPermissao(PERMISSAO.TRANSFERIR_ALUNO_INADIMPLENTE, FUNCIONALIDADE.TRANSFERENCIA_POLO, true)
            .done(function (response) {
                if (response.length) {
                    blTransferirInadimplente = true;
                }
            });
    },
    carregarMatriculas: function () {
        var that = this;
        var idUsuario = layoutInit.model.get("id_usuario");

        if (!idUsuario) {
            this.ui.inputMatricula.val("");
            return false;
        }

        this.ui.inputMatricula.html("<option value=''>Carregando...</option>");

        ComboboxView({
            el: that.ui.inputMatricula,
            url: "/api/vw-matricula"
                + "/id_evolucao/" + EVOLUCAO.TB_MATRICULA.CURSANDO
                + "/id_usuario/" + layoutInit.model.get("id_usuario")
                + "/id_entidadematricula/" + G2S.loggedUser.get("id_entidade"),
            optionLabelConstruct: function (model) {
                return model.get("id_matricula") + " - " + model.get("st_projetopedagogico") + " - " + model.get("st_evolucao");
            },
            model: Backbone.Model.extend({
                defaults: {
                    id_matricula: null,
                    st_matricula: null
                },
                idAttribute: "id_matricula"
            }),
            onChange: function (model) {
                if (model) {
                    modelMatricula.clear().set(model.toJSON());
                }
                that.infoMatricula.show(new InfoMatriculaView());
                that.ui.labelMatricula.addClass("span7");
            }
        });
    },
    carregarPolos: function () {
        var that = this;
        var idMatricula = this.ui.inputMatricula.val();

        if (!idMatricula) {
            this.ui.inputPolo.val("");
            this.ui.labelPolo.addClass("hide");
            return false;
        }

        this.ui.inputPolo.html("<option value=''>Carregando...</option>");
        this.ui.labelPolo.removeClass("hide");

        this.ui.searchPolo.focus();

        // Buscar dados da campanha comercial de origem
        this.buscarDadosVenda().done(function () {
            ComboboxView({
                el: that.ui.inputPolo,
                url: "/holding/retornar-entidades-mesma-holding",
                optionLabel: "st_nomeentidadeparceira",
                model: Backbone.Model.extend({
                    defaults: {
                        id_entidadeparceira: null,
                        st_nomeentidadeparceira: null
                    },
                    idAttribute: "id_entidadeparceira"
                }),
                onChange: function (model) {
                    if (model) {
                        modelPolo.clear().set(model.toJSON());
                    }
                },
                parse: function (response) {
                    // Não deverá mostrar o mesmo polo (entidade) da matrícula de origem
                    return !response ? [] : response.filter(function (item) {
                        return +item.id_entidadeparceira !== +modelMatricula.get("id_entidadematricula");
                    });
                }
            });
        });
    },
    carregarTurmas: function () {
        var that = this;
        var idEntidade = this.ui.inputPolo.val();

        // Verificar se o projeto está compartilhado com o polo de destino
        if (idEntidade) {
            $.ajax({
                url: "/api/projeto-entidade/",
                async: false,
                data: {
                    id_projetopedagogico: modelMatricula.get("id_projetopedagogico"),
                    id_entidade: idEntidade
                },
                success: function (response) {
                    if (!response || !response.length) {
                        idEntidade = null;
                    }
                }
            });

            if (!idEntidade) {
                $.pnotify({
                    type: "warning",
                    title: "Aviso",
                    text: "O curso de origem não está compartilhado com a entidade de destino"
                });
            }
        }

        if (!idEntidade) {
            this.ui.inputTurma.val("");
            this.ui.labelTurma.addClass("hide");
            return false;
        }

        this.ui.inputTurma.html("<option value=''>Carregando...</option>");
        this.ui.labelTurma.removeClass("hide");

        this.ui.searchTurma.focus();

        var data = {
            id_situacao: SITUACAO.TB_TURMA.ATIVA,
            id_projetopedagogico: modelMatricula.get("id_projetopedagogico"),
            id_entidade: modelPolo.get("id_entidadeparceira")
        };

        ComboboxView({
            el: that.ui.inputTurma,
            url: "/transfere-projeto/get-vw-turmas-disponives/?" + $.param(data),
            optionLabelConstruct: function (model) {
                return model.get("id_turma") + " - " + model.get("st_tituloexibicao");
            },
            model: Backbone.Model.extend({
                defaults: {
                    id_turma: null,
                    st_turma: null
                },
                idAttribute: "id_turma"
            }),
            onChange: function (model) {
                if (model) {
                    modelTurma.clear().set(model.toJSON());
                }
            }
        });
    },
    carregarFinanceiro: function () {
        var idTurma = this.ui.inputTurma.val();

        if (!idTurma) {
            layoutInit.financeiro.empty();
            layoutInit.botoes.empty();
            return false;
        }

        financeiroInit = new FinanceiroView();
        botoesInit = new BotoesView();

        layoutInit.financeiro.show(financeiroInit);
        layoutInit.botoes.show(botoesInit);
    },
    buscarDadosVenda: function () {
        return modelVenda
            .set("id_venda", modelMatricula.get("id_venda"))
            .fetch({
                beforeSend: loading,
                complete: loaded
            });
    }
});

FinanceiroView = Marionette.LayoutView.extend({
    template: "#financeiro-template",
    model: modelVenda,
    regions: {
        lancamentosOrigem: "#lancamentos-origem",
        lancamentosDestino: "#lancamentos-destino"
    },
    ui: {
        inputContratoRegra: "#id_contratoregradestino",
        inputCampComercial: "#id_campanhacomercialdestino",
        inputCampPontualidade: "#id_campanhapontualidadedestino",
        btnAtualizar: "#btn-atualizar"
    },
    events: {
        "change select": "bindValue",
        "click @ui.btnAtualizar": "carregarLancamentos"
    },
    bindValue: function (e) {
        var $el = $(e.target);

        // ID
        this.model.set($el.attr("name"), $el.val());

        // String
        this.model.set($el.attr("name").replace("id_", "st_"), $el.find(":selected").text());
    },
    onRender: function () {
        this.carregarContratoRegras();
        this.carregarCampanhas();

        colLancVencidos = new LancamentoCollection();
        colLancAtuais = new LancamentoCollection();

        this.lancamentosOrigem.show(new LancamentoComposite({
            collection: colLancVencidos,
            type: "origem"
        }));

        this.lancamentosDestino.show(new LancamentoComposite({
            collection: colLancAtuais,
            type: "destino"
        }));

        this.carregarLancamentos();
    },
    carregarContratoRegras: function () {
        ComboboxView({
            el: this.ui.inputContratoRegra,
            url: "/api/contrato-regra/?" + $.param({
                id_entidade: +modelPolo.get("id_entidadeparceira")
            }),
            optionLabel: "st_contratoregra",
            emptyOption: "[Selecione]",
            model: Backbone.Model.extend({
                defaults: {
                    id_contratoregra: null,
                    st_contratoregra: null
                },
                idAttribute: "id_contratoregra"
            })
        });
    },
    carregarCampanhas: function () {
        // Campanha Comercial
        ComboboxView({
            el: this.ui.inputCampComercial,
            url: "/api/campanha-comercial/?" + $.param({
                id_tipocampanha: 2,
                id_tipodesconto: 2,
                bl_ativo: 1,
                vigencia: true,
                id_entidade: +modelPolo.get("id_entidadeparceira")
            }),
            optionLabel: "st_campanhacomercial",
            emptyOption: "[Selecione]",
            model: Backbone.Model.extend({
                defaults: {
                    id_campanhacomercial: null,
                    st_campanhacomercial: null
                },
                idAttribute: "id_campanhacomercial"
            })
        });

        // Campanha de Pontualidade
        ComboboxView({
            el: this.ui.inputCampPontualidade,
            url: "/api/campanha-comercial/?" + $.param({
                id_tipocampanha: 3,
                id_tipodesconto: 2,
                bl_ativo: 1,
                vigencia: true,
                id_entidade: +modelPolo.get("id_entidadeparceira")
            }),
            optionLabel: "st_campanhacomercial",
            emptyOption: "[Selecione]",
            model: Backbone.Model.extend({
                defaults: {
                    id_campanhacomercial: null,
                    st_campanhacomercial: null
                },
                idAttribute: "id_campanhacomercial"
            })
        });
    },
    carregarLancamentos: function () {
        var today = (new Date()).toISOString().split("-").join("");
        var collection = new LancamentoCollection();

        colLancAtuais.reset();
        colLancVencidos.reset();

        collection.fetch({
            data: {
                id_venda: modelMatricula.get("id_venda")
            },
            beforeSend: loading,
            complete: loaded,
            success: function () {
                collection.each(function (model) {
                    var vencimento = model.get("dt_vencimento").substr(0, 10).split("/").reverse().join("");

                    if (vencimento >= today) {
                        model.set("type", "destino");
                        colLancAtuais.add(model);
                    } else {
                        model.set("type", "origem");
                        colLancVencidos.add(model);
                    }
                });
            }
        });
    }
});

BotoesView = Marionette.ItemView.extend({
    template: "#botoes-template",
    model: modelVenda,
    ui: {
        btnContinue: "#btn-continue",
        btnConfirm: "#btn-confirm",
        btnCancel: "#btn-cancel",
        modalResumo: "#modal-resumo"
    },
    events: {
        "click @ui.btnCancel": "cancelarTransferencia",
        "click @ui.btnConfirm": "salvarTransferencia"
    },
    modelEvents: {
        "change": "render"
    },
    onBeforeRender: function () {
        this.model.set({
            st_nomeentidadedestino: modelPolo.get("st_nomeentidadeparceira"),
            st_turmadestino: modelTurma.get("st_turma")
        });
    },
    verificarErros: function (data) {
        var erros = [];

        if (!data.id_contratoregradestino) {
            erros.push("É necessário informar a regra de contrato.");
        }

        if (!blTransferirInadimplente) {
            colLancVencidos.each(function (model) {
                if (!model.get("bl_quitado")) {
                    erros.push("É necessário que o aluno esteja adimplente, verifique as parcelas vencidas.");
                }
            });
        }

        return erros;
    },
    cancelarTransferencia: function () {
        bootbox.confirm({
            message: "Cancelar a transferência fará com que todas as alterações sejam perdidas.",
            buttons: {
                confirm: {
                    label: "Cancelar Transferência",
                    className: "btn-primary"
                },
                cancel: {
                    label: "Não Cancelar"
                }
            },
            callback: function (response) {
                if (response) {
                    layoutInit.esconderForm();
                    layoutInit.mostrarForm();
                }
            }
        });
    },
    salvarTransferencia: function (e) {
        e.preventDefault();

        var that = this;

        var data = {
            id_venda: modelVenda.get("id_venda"),
            id_vendaproduto: modelVenda.get("id_vendaproduto"),
            id_matricula: modelMatricula.get("id_matricula"),
            id_entidadedestino: +modelPolo.get("id_entidadeparceira"),
            id_turmadestino: +modelTurma.get("id_turma"),
            id_contratoregradestino: modelVenda.get("id_contratoregradestino"),
            id_campanhacomercialdestino: modelVenda.get("id_campanhacomercialdestino"),
            id_campanhapontualidadedestino: modelVenda.get("id_campanhapontualidadedestino"),
            lancamentos: colLancAtuais.toJSON()
        };

        var erros = this.verificarErros(data);

        if (erros.length) {
            $.pnotify({
                type: "warning",
                title: "Aviso",
                text: erros.shift()
            });
            return false;
        }

        loading();

        $.post("/transferencia-polo/salvar-transferencia", data)
            .done(function (response) {
                $.pnotify(response);
                that.ui.modalResumo.modal("hide");
                layoutInit.esconderForm();
                layoutInit.mostrarForm();
            })
            .fail(function (err) {
                $.pnotify(err.responseJSON);
            })
            .always(loaded)
        ;
    }
});

// LANCAMNETOS

LancamentoCollection = Backbone.Collection.extend({
    model: VwVendaLancamento,
    url: "/api/vw-venda-lancamento"
});

LancamentoItem = Marionette.ItemView.extend({
    tagName: "tr",
    template: "#lancamento-item",
    onShow: function () {
        if (this.model.get("type") === "destino") {
            var index = this.$el.prevAll("tr").length + 1;
            this.model.set("index", index);
            this.$el.find("td:first").html(index);
        }
    }
});

LancamentoComposite = Marionette.CompositeView.extend({
    model: new Backbone.Model(),
    template: "#lancamento-composite",
    childView: LancamentoItem,
    childViewContainer: "tbody",
    initialize: function (options) {
        this.model.set("type", options.type);
    }
});

// Main render
layoutInit = new LayoutView();
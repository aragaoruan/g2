/******************************************************************************
 *              Grade Horaria Entidade
 *              Neemias Santos <neemias.santos@unyleya.com.br>
 *              08/09/2015
 ******************************************************************************/

/**
 ******************************************************************************************
 ******************************************************************************************
 ******************************** GLOBAIS *************************************************
 ******************************************************************************************
 ******************************************************************************************
 */

var itensCollection;

/**
 * @type Collection para Select de Entidade @exp;Backbone@pro;Collection@call;extend
 */
var EntidadeRecursivaCollection = Backbone.Collection.extend({
    url: '/entidade/retornar-entidade-recursiva-id',
    model: VwEntidadeRecursiva,
});

/**
 ******************************************************************************************
 ******************************************************************************************
 *************************** LAYOUT FUNCIONALIDADE ****************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var TelaGradeLayout = Marionette.LayoutView.extend({
    template: '#template-layout-grade-entidade',
    tagName: 'div',
    regions: {
        formgrade: '#regiao-form-grade',
        datagrade: '#regiao-grade-data',
    },
    initialize: function () {

    },
    onShow: function () {
        this.renderizaFormGrade();
        $('#container-DataGrid').hide();
        return this;
    },
    renderizaFormGrade: function () {
        var gradeView = new FormGradeCompositeView({});
        this.formgrade.show(gradeView);
    }
});

/**
 ******************************************************************************************
 ******************************************************************************************
 ************************************** COMPOSITES ****************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var FormGradeCompositeView = Marionette.CompositeView.extend({
    initialize: function () {

    },
    template: '#template-form-grade-entidade',
    tagName: 'fieldset',
    ui: {
        'id_entidade': '#id_entidade',
        'dt_iniciogradehoraria': '#dt_iniciogradehoraria',
        'dt_fimgradehoraria': '#dt_fimgradehoraria'
    },
    events: {
        'click #btnPesquisar': 'pesquisaDados',
        'click #btnImprimir': 'imprimir'
    },
    onShow: function () {
        this.populaSelectEntidade();
        return this;
    },
    populaSelectEntidade: function () {
        var that = this;
        var collectionEntidadeRecursiva = new EntidadeRecursivaCollection();
        collectionEntidadeRecursiva.fetch({
            beforeSend: function () {
                loading();
            },
            success: function () {
                var view = new SelectView({
                    el: that.ui.id_entidade, // Elemento da DOM
                    collection: collectionEntidadeRecursiva, // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_nomeentidade', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_entidade', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: null      // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();
            }
        })
    },
    pesquisaDados: function () {
        var that = this;
        if (that.ui.dt_iniciogradehoraria.val() == '' || that.ui.dt_fimgradehoraria.val() == '' ) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Preencha os dois campos de Períodos.'
            });
            return false;
        } else {

            loading();

            var columns = [{
                name: "bl_imprimir",
                cell: "select-row",
                className: 'bl_imprimir',
                editable: false,
                headerCell: "select-all"
            }, {
                name: "st_unidades",
                label: "Unidades",
                editable: false,
                cell: 'string'
            }, {
                name: "st_nomegradehoraria",
                label: "Nome da Grade",
                editable: false,
                cell: 'string'
            }, {
                name: "st_periodo",
                label: "Período",
                editable: false,
                cell: 'string'
            }];

            var PageableLog = Backbone.PageableCollection.extend({
                model: GradeHoraria,
                url: '/default/grade-horaria/imprimi-grade-horaria-combo',
                state: {
                    pageSize: 30
                },
                mode: "client",
            });
            var data = {
                'dt_iniciogradehoraria': that.ui.dt_iniciogradehoraria.val(),
                'dt_fimgradehoraria': that.ui.dt_fimgradehoraria.val(),
                'id_unidade': that.ui.id_entidade.val(),
            };

            pageableLog = new PageableLog();
            itensCollection = new PageableLog();

            var RowGrid = Backgrid.Row.extend({
                initialize: function (options) {
                    RowGrid.__super__.initialize.apply(this, arguments);
                },
                events: {
                    "change input[type='checkbox']": 'selected'
                },
                selected: function () {
                    if (typeof  itensCollection.findWhere({id_gradehoraria: this.model.get('id_gradehoraria')}) === 'undefined') {
                        itensCollection.add(this.model);
                    } else {
                        itensCollection.remove(this.model);
                    }
                },
            });

            var pageableGridLog = new Backgrid.Grid({
                className: 'backgrid table table-bordered',
                columns: columns,
                collection: pageableLog,
                emptyText: "Nenhum registro encontrado",
                row: RowGrid
            });

            $('#container-DataGrid').show();
            var $elemento = $('#template-data-grade');
            $elemento.empty();
            $elemento.append(pageableGridLog.render().$el);

            var paginator = new Backgrid.Extension.Paginator({
                collection: pageableLog
            });

            $elemento.append(paginator.render().el);
            $('.container-DataGrid').show();
            pageableLog.fetch({
                data: data,
                reset: true
            });
        }
        loaded();
    },
    imprimir: function () {
        if (itensCollection.length == 0) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Selecione os professores para imprimir.'
            });
            return false;
        } else {
            var arrDados = [];
            itensCollection.each(function (model, i) {
                arrDados[i] = model.get('id_gradehoraria');
            });

            window.open("/relatorio-grade-projeto-entidade/retorno-relatorio-print/id_gradehoraria/" + arrDados, '_blank');
        }
    }
});

/*********************************************
 ************** INICIALIZAÇÃO ****************
 *********************************************/

var layoutGrade = new TelaGradeLayout();
G2S.show(layoutGrade);
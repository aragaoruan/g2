/**
 * Models
 */

var ModelRelatorio = Backbone.Model.extend({
    id_projetopedagogico: '',
    st_projetopedagogico: '',
    vendas: []
});

/**
 * Collections
 */

var collectionEntidade = new EntidadeCollection();

var HoldingCollection = Backbone.Collection.extend({
    url: '/folha-de-numeros-turma-x-mes/retorna-holding/',
    model: Holding,
    initialize: function () {
        //this.fetch()
    }
});

var EntidadeCollection = Backbone.Collection.extend({
    url: '/folha-de-numeros-turma-x-mes/retorna-entidade-holding',
    model: VwEntidadeRecursiva,
    initialize: function () {
        //this.fetch()
    }
});

var TurmaCollection = Backbone.Collection.extend({
    url: '/folha-de-numeros/retorna-turma/id/',
    model: Turma,
    initialize: function () {
        //this.fetch()
    }
});

var ProjetoPedagogicoCollection = Backbone.Collection.extend({
    url: '/folha-de-numeros-turma-x-mes/get-projetos-pedagogicos/',
    model: VwProjetoEntidade,
    initialize: function () {
        //this.fetch()
    }
});

/**LayoutView**/
var FolhaDePagamentoLayoutView = Marionette.LayoutView.extend({
    template: '#folha_layout',
    regions: {},
    ui: {
        'id_holding': '#id_holding',
        'id_entidade': '#id_entidade',
        'id_projetopedagogico': '#id_projeto',
        'id_turma_produto': '#id_turma-produto',
        'dt_mes_filtro': '#dt_mes_filtro',
        'dt_ano_filtro': '#dt_ano_filtro',
        'form': 'form',
        'btn_imprimir_relatorio': '#btn_imprimir_relatorio',
        'btn_gerar_xls': '#btn_gerar_xls'
    },
    events: {
        'change #id_holding': 'renderizaSelectEntidade',
        'change #id_entidade': 'renderizaSelectProjetoPedagogico',
        'change #id_projeto': 'renderizaSelectTurmaProduto',
        'change #dt_mes_filtro': function () {
            this.desabilitarBotoes();
        },
        'change #dt_mano_filtro': function () {
            this.desabilitarBotoes();
        },
        'submit form': 'submitRelatorio',
        'click #btn_gerar_xls': 'gerarXLS',
        'click #btn_imprimir_relatorio': 'imprimirRelatorio'
    },
    onRender: function () {
        this.renderizaSelectHolding();

    },
    desabilitarBotoes: function () {
        this.ui.btn_gerar_xls.prop('disabled', true);
        this.ui.btn_imprimir_relatorio.prop('disabled', true);
    },
    habilitarBotoes: function () {
        this.ui.btn_gerar_xls.prop('disabled', false);
        this.ui.btn_imprimir_relatorio.prop('disabled', false);
    },
    gerarXLS: function () {
        that = this;

        var allEntity = '';
        if (that.ui.id_entidade.val() == 0) {

            collectionEntidade.each(function (model, i) {
                if (collectionEntidade.length - 1 != i) {
                    allEntity += model.get('id_entidade') + ','
                } else {
                    allEntity += model.get('id_entidade')
                }
            });
        }

        var url = '/folha-de-numeros-turma-x-mes/get-relatorio/?tipo=xls&' +
            'id_entidade=' + (that.ui.id_entidade.val() != 0 ? that.ui.id_entidade.val() : allEntity) +
            '&id_projeto=' + that.ui.id_projetopedagogico.val() +
            '&id_turma-projeto=' + that.ui.id_turma_produto.val() +
            '&dt_mes_filtro=' + that.ui.dt_mes_filtro.val() +
            '&dt_ano_filtro=' + that.ui.dt_ano_filtro.val();

        window.open(url, '_blank');
    },
    imprimirRelatorio: function () {
        that = this;
        var allEntity = '';
        if (that.ui.id_entidade.val() == 0) {

            collectionEntidade.each(function (model, i) {
                if (collectionEntidade.length - 1 != i) {
                    allEntity += model.get('id_entidade') + ','
                } else {
                    allEntity += model.get('id_entidade')
                }
            });
        }


        var url = '/folha-de-numeros-turma-x-mes/get-relatorio/?tipo=html&' +
            'id_entidade=' + (that.ui.id_entidade.val() != 0 ? that.ui.id_entidade.val() : allEntity) + '' +
            '&id_projeto=' + that.ui.id_projetopedagogico.val() +
            '&id_turma-projeto=' + that.ui.id_turma_produto.val() +
            '&dt_mes_filtro=' + that.ui.dt_mes_filtro.val() +
            '&dt_ano_filtro=' + that.ui.dt_ano_filtro.val()
        window.open(url, '_blank');


    },
    validaForm: function () {
        var flag = true;
        if (!this.ui.id_entidade.val()) {
            $('#div-combo-entidade').addClass('control-group error');
            flag = false;
        } else {
            $('#div-combo-entidade').removeClass('control-group error');
            flag = true;
        }
        ;
        if (!this.ui.id_projetopedagogico.val()) {
            $('#div-combo-projeto a').css({border: '1px solid #b94a48'});
            flag = false;
        } else {
            $('#div-combo-projeto a').css({border: ''});
            flag = true;
        }
        ;
        if (!flag) {
            $.pnotify({
                title: 'Aviso!',
                text: 'Verifique os campos obrigatórios em vermelho.',
                type: 'warning'
            });
        }
        return flag;

    },
    submitRelatorio: function (e) {
        var that = this;
        e.preventDefault();

        if (this.validaForm()) {

            var dados = {
                'id_entidade': this.ui.id_entidade.val(),
                'id_projeto': this.ui.id_projetopedagogico.val(),
                'id_turma-produto': this.ui.id_turma_produto.val(),
                'dt_mes_filtro': this.ui.dt_mes_filtro.val(),
                'dt_ano_filtro': this.ui.dt_ano_filtro.val()
            }

            var collectionResultRelatorio = Backbone.Collection.extend({
                model: ModelRelatorio
            });

            //concatena as entidades quando é selecionado todos no combo de entidades.
            if (dados['id_entidade'] == 0) {
                var allEntity = '';
                collectionEntidade.each(function (model, i) {
                    if (collectionEntidade.length - 1 != i) {
                        allEntity += model.get('id_entidade') + ','
                    } else {
                        allEntity += model.get('id_entidade')
                    }
                });
                dados['id_entidade'] = allEntity;
            }

            var collectionFetch = new collectionResultRelatorio();
            collectionFetch.url = '/folha-de-numeros-turma-x-mes/get-relatorio';
            collectionFetch.fetch({
                data: dados,
                type: 'POST',
                beforeSend: function () {
                    loading();
                },
                success: function () {
                    if (collectionFetch.models == "") {
                        $.pnotify({
                            title: 'Aviso!',
                            text: 'Nenhum dado encontrado para gerar relatório.',
                            type: 'warning'
                        });

                        $('#btn_imprimir_relatorio').prop('disabled', true);
                        $('#btn_gerar_xls').prop('disabled', true);

                        $('#container-table').hide('fast');

                        loaded();

                    } else {

                        $('#btn_imprimir_relatorio').prop('disabled', false);
                        $('#btn_gerar_xls').prop('disabled', false);

                        var CollectionTable = Backbone.Collection.extend({
                            model: ModelRelatorio
                        });
                        var vendaView = Marionette.ItemView.extend({
                            tagName: 'tr',
                            template: '#item-relatorio'
                        });
                        var projetoVenda = Marionette.CompositeView.extend({
                            childView: vendaView,
                            template: "#script_tabela_relatorio",
                            childViewContainer: 'tbody',
                            initialize: function () {
                                var collection = new CollectionTable(this.model.get('vendas'));
                                this.collection = collection;
                                this.calculaValores();

                            },
                            onShow: function () {
                                debug(this.model);
                                return this;
                            },
                            calculaValores: function () {
                                var totalValor = 0, totalMatriculas = 0;

                                this.collection.each(function (model, i) {
                                    totalMatriculas++;
                                    totalValor += parseFloat(model.get('nu_valorliquido'));
                                    model.set('nu_valorliquido', formatValueToShow(parseFloat(model.get('nu_valorliquido')).toFixed(2)));
                                });


                                this.model.set('totalMatriculas', totalMatriculas);
                                this.model.set('totalValor', formatValueToShow(totalValor.toFixed(2)));
                            }
                        });
                        var projetoView = Marionette.CompositeView.extend({
                            childView: projetoVenda,
                            template: _.template('<div class="corpo"></div>'),
                            childViewContainer: '.corpo'
                        });
                        viewRelatorio = new projetoView({
                            el: that.$el.find('#container-table'),
                            collection: collectionFetch
                        });
                        $('#container-table').show();
                        viewRelatorio.render();
                        loaded();
                        formatValueToShow
                    }
                    ;

                },
                complete: function () {
                    loaded();
                    that.habilitarBotoes();
                }
            })
        }
    },
    renderizaSelectHolding: function () {
        loading();
        var that = this;
        var collectionHolding = new HoldingCollection();
        collectionHolding.url += 'id_entidade/' + G2S.loggedUser.get('id_entidade');
        collectionHolding.fetch({
            success: function () {
                var view = new SelectView({
                    el: that.ui.id_holding,        // Elemento da DOM
                    collection: collectionHolding,      // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_holding', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_holding', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: null       // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();
                loaded();
            },
            complete: function () {

            }
        });
    },
    renderizaSelectEntidade: function () {
        loading();
        this.desabilitarBotoes();
        var that = this;
        var valor = this.ui.id_holding.val() ? this.ui.id_holding.val() : null;
        if (valor) {

            that.ui.id_entidade.val("");
            that.ui.id_entidade.prop('disabled', false);
            that.ui.id_entidade.empty();
            that.ui.id_entidade.prepend($('<option selected value="">Selecione</option>'));

            collectionEntidade = new EntidadeCollection();
            collectionEntidade.url += '/id_holding/' + this.ui.id_holding.val();
            collectionEntidade.fetch({
                success: function () {
                    var view = new SelectView({
                        el: that.ui.id_entidade,        // Elemento da DOM
                        collection: collectionEntidade,      // Instancia da collection a ser utilizada
                        childViewOptions: {
                            optionLabel: 'st_nomeentidade', // Propriedade da Model que será utilizada como label do select
                            optionValue: 'id_entidade', // Propriedade da Model que será utilizada como id do option do select
                            optionSelected: null       // ID da option que receberá o atributo "selected"
                        }
                    });
                    view.render();
                    loaded();
                },
                complete: function () {
                    that.ui.id_entidade.prepend($('<option value="0">Todos</option>'))
                }
            });
        } else {
            that.ui.id_entidade.val("");
            that.ui.id_entidade.prop('disabled', true);
            that.ui.id_entidade.prepend($('<option selected value="">Selecione</option>'));
            that.ui.id_projetopedagogico.val("");
            that.ui.id_projetopedagogico.select2('destroy');
            that.ui.id_projetopedagogico.prop('disabled', true);
            that.ui.id_projetopedagogico.prepend($('<option selected value="">Selecione</option>'));
            that.ui.id_turma_produto.val("");
            that.ui.id_turma_produto.select2('destroy');
            that.ui.id_turma_produto.prop('disabled', true);
            that.ui.id_turma_produto.prepend($('<option selected value="">Selecione</option>'));
            loaded();
        }

    },
    renderizaSelectProjetoPedagogico: function () {
        loading();
        this.desabilitarBotoes();
        var that = this;
        var valor = this.ui.id_entidade.val() ? this.ui.id_entidade.val() : null;
        var entidadeTodas = '';
        if (valor) {


            if (valor == 0) {
                collectionEntidade.each(function (model, i) {
                    if (collectionEntidade.length - 1 != i) {
                        entidadeTodas += model.get('id_entidade') + ','
                    } else {
                        entidadeTodas += model.get('id_entidade')
                    }
                })
            }

            that.ui.id_projetopedagogico.val("");
            that.ui.id_projetopedagogico.prop('disabled', false);
            that.ui.id_projetopedagogico.empty();
            that.ui.id_projetopedagogico.select2('destroy');
            that.ui.id_projetopedagogico.prepend($('<option selected value="">Selecione</option>'));

            that.ui.id_turma_produto.val("");
            that.ui.id_turma_produto.empty();
            that.ui.id_turma_produto.select2('destroy');
            that.ui.id_turma_produto.prepend($('<option selected value="" >Selecione</option>'));
            that.ui.id_turma_produto.prop('disabled', true);

            var collectionProjetoPedagogico = new ProjetoPedagogicoCollection();
            collectionProjetoPedagogico.url += (that.ui.id_entidade.val() != 0) ? 'id_entidade/' + that.ui.id_entidade.val() : 'id_entidade/' + entidadeTodas;
            collectionProjetoPedagogico.fetch({
                success: function () {
                    collectionProjetoPedagogico.each(function (model, i) {
                        model.set('st_projetopedagogico', (model.get('st_projetopedagogico') + ' (' + model.get('st_nomeentidade') + ')'))
                    });

                    var view = new SelectView({
                        el: that.ui.id_projetopedagogico,        // Elemento da DOM
                        collection: collectionProjetoPedagogico,      // Instancia da collection a ser utilizada
                        childViewOptions: {
                            optionLabel: 'st_projetopedagogico', // Propriedade da Model que será utilizada como label do select
                            optionValue: 'id_projetopedagogico', // Propriedade da Model que será utilizada como id do option do select
                            optionSelected: null       // ID da option que receberá o atributo "selected"
                        }
                    });
                    view.render();
                    loaded();
                },
                complete: function () {
                    that.ui.id_projetopedagogico.select2();
                    that.ui.id_projetopedagogico.prepend($('<option value="0">Todos</option>'))
                }
            });
        } else {
            that.ui.id_projetopedagogico.val("");
            that.ui.id_projetopedagogico.select2('destroy');
            that.ui.id_projetopedagogico.prop('disabled', true);
            that.ui.id_projetopedagogico.prepend($('<option selected value="">Selecione</option>'));
            that.ui.id_turma_produto.val("");
            that.ui.id_turma_produto.select2('destroy');
            that.ui.id_turma_produto.prop('disabled', true);
            that.ui.id_turma_produto.prepend($('<option selected value="">Selecione</option>'));
            loaded();
        }

    },
    renderizaSelectTurmaProduto: function () {
        var that = this;
        this.desabilitarBotoes();
        var valor = this.ui.id_projetopedagogico.val() ? this.ui.id_projetopedagogico.val() : null;
        if (valor && valor != 0 && valor != 'SELECIONE') {
            that.ui.id_turma_produto.val("");
            that.ui.id_turma_produto.prop('disabled', false);
            that.ui.id_turma_produto.empty();
            that.ui.id_turma_produto.select2('destroy');
            that.ui.id_turma_produto.prepend($('<option selected disabled>Selecione</option>'));
            collect = new TurmaCollection();
            collect.url = '/folha-de-numeros-turma-x-mes/get-turma-projeto-pedagogico/id_projetopedagogico/' + valor + '/id_entidadecadastro/'+that.ui.id_entidade.val();
            collect.fetch({
                beforeSend: function () {
                    loading();
                    that.ui.id_turma_produto.empty();
                    that.ui.id_turma_produto.prepend("<option value=''>Selecione</option>");
                },
                success: function () {
                    var view = new SelectView({
                        el: that.ui.id_turma_produto,        // Elemento da DOM
                        collection: collect,      // Instancia da collection a ser utilizada
                        childViewOptions: {
                            optionLabel: 'st_turma', // Propriedade da Model que será utilizada como label do select
                            optionValue: 'id_turma', // Propriedade da Model que será utilizada como id do option do select
                            optionSelected: null       // ID da option que receberá o atributo "selected"
                        }
                    });
                    view.render();

                },
                complete: function () {
                    that.ui.id_turma_produto.prepend($('<option value="">Todos</option>'))
                    that.ui.id_turma_produto.select2();
                    loaded();
                }
            });
        } else {
            that.ui.id_turma_produto.val("");
            that.ui.id_turma_produto.select2('destroy');
            that.ui.id_turma_produto.prepend($('<option selected value="" >Selecione</option>'));
            that.ui.id_turma_produto.prop('disabled', true);
            loaded();
        }
    },
});

var layoutView = new FolhaDePagamentoLayoutView();
G2S.show(layoutView);
/**
 * Funcionalidade Adicionar Contrado do Aluno
 * @author Elcio Guimarães <elcioguimaraes@gmail.com>
 * @since 06/08/2015
 */

function addHandler(obj, evnt, handler) {
    if (obj.addEventListener) {
        obj.addEventListener(evnt.replace(/^on/, ''), handler, false);
    } else {
        if (obj[evnt]) {
            var origHandler = obj[evnt];
            obj[evnt] = function (evt) {
                origHandler(evt);
                handler(evt);
            };
        } else {
            obj[evnt] = function (evt) {
                handler(evt);
            };
        }
    }
}
addHandler(window, 'onerror', function (error, url, num) {
    //alert('ERRO - Contate o suporte. \n'+ error.message + ' \n ' + error.filename + '(' +error.lineno+ ')');
    console.log('ERRO: ' + error.message);
    console.log('------ ' + error.filename + '(' + error.lineno + ')');
    return true;
});
var VwMatriculaCollectionExtend = Backbone.Collection.extend({
    url: '/api/vw-matricula/',
    model: VwMatricula
});

var MinhaPastaCollection = Backbone.Collection.extend({
    url: '/api/minha-pasta',
    model: MinhaPasta
});

var VwMinhaPastaCollection = Backbone.Collection.extend({
    url: '/api/vw-minha-pasta',
    model: VwMinhaPasta
});

var VwMatriculaModel = VwMatricula.extend({idAttribute: 'id_matricula'});

/**
 ******************************************************************************************
 ******************************************************************************************
 *************************** SELECT MATRICULAS  *******************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var collectionMatricula = new VwMatriculaCollectionExtend();

var RowView = Marionette.ItemView.extend({
    tagName: "tr",
    template: "#row-template",
    events: {
        'click .actionDelete': 'actionDelete'
    },
    actionDelete: function () {
        var that = this;

        bootbox.confirm("Tem certeza de que deseja apagar o registro?", function (result) {
            if (result) {
                that.model.destroy({
                    wait: true,
                    success: function (model, response) {
                        $.pnotify({
                            type: 'success',
                            title: 'Sucesso',
                            text: response.mensagem
                        });
                        that.remove();
                        loaded();
                    },
                    beforeSend: function () {
                        loading();
                    },
                    complete: function () {
                        selectMatriculaCompositeView.mostraHistorico();
                    },
                    error: function (model, response) {
                        $.pnotify({
                            type: 'error',
                            title: 'Erro',
                            text: response.responseJSON.mensagem
                        });
                    }
                });
            }
        });
    }
});

var HistoricoRowView = Marionette.ItemView.extend({
    tagName: "tr",
    template: "#row-template-historico"
});

var SelectMatriculaCompositeView = Marionette.CompositeView.extend({
    template: '#template-adicionar-contrato',
    childView: RowView,
    collection: new MinhaPastaCollection(),
    childViewContainer: 'tbody',
    className: 'row-fluid',
    modelVwMatricula: null,
    ui: {
        matricula: '#id_matricula',
        contrato: '#regiao-adicionar-contrato',
        uploadcontrato: '#id_uploadcontrato',
        btnAddContrato: '#btn-add-contrato'
    },
    events: {
        'change #id_matricula': 'exibeDadosMatricula',
        'change #id_uploadcontrato': 'ativaEnviarContrato',
        'click #btn-add-contrato': 'enviarContrato',
        'click .actionDelete': 'actionDelete'
    },
    initialize: function () {
        selectMatriculaCompositeView = this;
    },
    onShow: function () {
        var that = this;
        var pessoa = componenteAutoComplete.getFixModel();
        if (pessoa != null) {
            collectionMatricula.url = '/matricula/retorna-matricula-contrato/id_usuario/' + pessoa.id;
            collectionMatricula.fetch({
                success: function () {
                    var ComboView = new SelectView({
                        el: that.ui.matricula,        // Elemento da DOM
                        collection: collectionMatricula,      // Instancia da collection a ser utilizada
                        childViewOptions: {
                            optionLabel: 'st_labelselect', // Propriedade da Model que será utilizada como label do select
                            optionValue: 'id_matricula', // Propriedade da Model que será utilizada como id do option do select
                            optionSelected: null       // ID da option que receberá o atributo "selected"
                        }
                    });
                    ComboView.render();
                },
                error: function () {
                    $.pnotify({
                        title: 'Erro',
                        text: 'Ocorreu um erro ao recuperar os dados da Matrícula.',
                        type: 'error'
                    });
                }
            });
        }
    },
    exibeDadosMatricula: function (e) {
        this.mostraArquivoSelecionado();
    },
    mostraArquivoSelecionado: function () {
        var that = this;
        loading();

        this.modelVwMatricula = null;

        if (that.ui.matricula.val()) {
            this.collection.fetch({
                reset: true,
                data: {
                    id_matricula: that.ui.matricula.val(),
                    id_situacao: situacaoAtiva,
                },
                complete: function () {
                    that.mostraHistorico()
                }
            });

            var model = new VwMatriculaModel();
            model.set({id_matricula: that.ui.matricula.val()});
            model.fetch({
                success: function (model, response, options) {
                    that.modelVwMatricula = model;
                }
            });
        }
    },
    mostraHistorico: function () {

        (new HistoricoCompositeView({
            el: this.$el.find('#container-historico'),
        }).render());

        loaded();
    },
    ativaEnviarContrato: function () {
        if (this.ui.uploadcontrato.val()) {
            this.ui.btnAddContrato.attr('disabled', false);
        } else {
            this.ui.btnAddContrato.attr('disabled', true);
        }
    },
    enviarContrato: function () {

        var that = this;

        // SALVAR ARQUIVO ANEXO EMENTA
        var id_uploadcontrato = $('#id_uploadcontrato');
        if (id_uploadcontrato.val() && that.ui.matricula.val()) {
            var arquivos = id_uploadcontrato[0].files;
            var formData = new FormData();

            for (var i = 0; i < arquivos.length; i++) {
                var file = arquivos[i];
                if (file.size >= 10000000) {
                    $.pnotify({
                        title: 'Aviso',
                        text: 'Só é permitido o envio de arquivo igual ou inferior a 10mb.',
                        type: 'warning'
                    });
                    loaded();
                    return false;
                }
                formData.append('arquivo', file, file.name);
            }

            loading();
            $.ajax({
                url: '/venda/upload-contrato?id_venda=' + that.modelVwMatricula.get('id_venda') + '&id_matricula=' + that.ui.matricula.val(),
                type: 'post',
                data: formData,
                cache: false,
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function (response) {
                    if (response.type == 'success') {
                        that.mostraArquivoSelecionado();
                        $.pnotify(response);
                    } else {
                        this.error(response);
                    }
                    that.ui.btnAddContrato.attr('disabled', true);
                },
                error: function (response) {
                    $.pnotify(response);
                },
                complete: function () {
                    id_uploadcontrato.val('');
                    loaded();
                }
            });
        } else {
            if (!id_uploadcontrato.val()) {
                $.pnotify({title: 'Aviso', text: 'Informe o Arquivo!.', type: 'warning'});
            }
            if (!that.modelVwMatricula) {
                $.pnotify({title: 'Aviso', text: 'Escolha a Matrícula!', type: 'warning'});
            }
        }


    }
});

var HistoricoCompositeView = Marionette.CompositeView.extend({
    template: '#template-data-historico',
    childView: HistoricoRowView,
    childViewContainer: 'tbody',
    className: 'row-fluid',
    ui: {
        matricula: '#id_matricula'
    },
    onRender: function () {
        this.collection = new VwMinhaPastaCollection();
        this.collection.fetch({
            data: {
                id_matricula: selectMatriculaCompositeView.ui.matricula.val()
            }
        });
    }
});


/**
 ******************************************************************************************
 ******************************************************************************************
 *************************** LAYOUT FUNCIONALIDADE ****************************************
 ******************************************************************************************
 ******************************************************************************************
 */
//var SelectView = null;
var TelaGradeLayout = Marionette.LayoutView.extend({
    template: '#template-layout-adicionar-documento',
    tagName: 'div',
    className: 'container-fluid',
    regions: {
        formPesquisa: '#regiao-pesquisa-matricula',
        dados: '#regiao-dados'
    },
    onShow: function () {
        componenteAutoComplete.main({
                model: VwPesquisarPessoa,
                element: this.$el.find('#regiao-pesquisa')
            }, function () {
                var SelectView = new SelectMatriculaCompositeView();
                telaGrade.formPesquisa.show(SelectView);
            }, function () {
                telaGrade.formPesquisa.destroy();
            }
        );
        loaded();
    }
});

var telaGrade = new TelaGradeLayout();
G2S.show(telaGrade);
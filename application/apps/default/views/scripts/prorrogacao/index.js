/**
 * JavaScript Neemias Santos
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 * @since 2015-04-29
 */

/**
 ******************************************************************************************
 ******************************************************************************************
 *************************** COLLECTION  **************************************************
 ******************************************************************************************
 ******************************************************************************************
 */

var pageableLog = '';
var EntidadeRecursivaCollection = Backbone.Collection.extend({
    url: '/entidade/retornar-entidade-recursiva-id',
    model: VwEntidadeRecursiva,
    initialize: function () {
        //this.fetch()
    }
});

var ProjetoPedagocigoCollection = Backbone.Collection.extend({
    url: '/default/projeto-pedagogico',
    model: VwProjetoPedagogico,
    initialize: function () {
        //this.fetch()
    }
});

var MatriculaCollection = Backbone.Collection.extend({
    url: '/default/matricula/retorna-matricula-params',
    model: VwMatricula,
    initialize: function () {
        //this.fetch()
    }
});

/**
 ******************************************************************************************
 ******************************************************************************************
 *************************** LAYOUT FUNCIONALIDADE ****************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var TelaProrrogacaoLayout = Marionette.LayoutView.extend({
    template: '#template-layout-prorrogacao',
    tagName: 'div',
    className: 'container-fluid',
    ui: {
        'id_projetopedagogico': 'select[name="id_projetopedagogico"]',
        'id_entidadematriz': 'select[name="id_entidadematriz"]',
        'dt_inicio': 'input[name="dt_inicio"]',
        'nu_dias': 'input[name="nu_dias"]',
        'dt_termino': 'input[name="dt_termino"]',
        'btnPesquisar': '#btnPesquisar',
        'btnProrrogar': '#btnProrrogar'
    },
    events: {
        'change select[name="id_entidadematriz"]': 'populaSelectProjeto',
        'click #btnPesquisar': 'pesquisaDados',
        "click #btnProrrogar": "prorrogaCollection",
        'click .select-all-header-cell input[type="checkbox"]': 'desmarcaTodos'
    },
    regions: {
        formgrade: '#regiao-prorrogacao'
    },
    onShow: function () {
        this.populaSelectEntidade();
        $('#container-DataGrid').hide();
        loaded();
        return this;
    },
    populaSelectEntidade: function () {
        var that = this;
        var collectionEntidadeRecursiva = new EntidadeRecursivaCollection();
        collectionEntidadeRecursiva.fetch({
            beforeSend: function () {
                loading();
            },
            success: function () {
                var view = new SelectView({
                    el: that.ui.id_entidadematriz, // Elemento da DOM
                    collection: collectionEntidadeRecursiva, // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_nomeentidade', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_entidade', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: null      // ID da option que receberá o atributo "selected"
                    }
                });

                view.render();
            }
        })
    },
    populaSelectProjeto: function () {
        var that = this;
        var collectionProjetoPedagogico = new ProjetoPedagocigoCollection();
        collectionProjetoPedagogico.url += '/projeto-pedagogico-entidade/id_entidadecadastro/' + that.ui.id_entidadematriz.val() + '/id_situacao/7/bl_ativo/1';
        collectionProjetoPedagogico.fetch({
            beforeSend: function () {
                loading();
            },
            success: function () {
                that.ui.id_projetopedagogico.select2('destroy');
                that.ui.id_projetopedagogico.empty();
                that.ui.id_projetopedagogico.prepend('<option value="" selected="selected">Escolha uma opção</option>');
                var view = new SelectView({
                    el: that.ui.id_projetopedagogico, // Elemento da DOM
                    collection: collectionProjetoPedagogico, // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_projetopedagogico', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_projetopedagogico', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: null      // ID da option que receberá o atributo "selected"
                    }
                });

                view.render();
                that.ui.id_projetopedagogico.select2();
                loaded();
            }
        })
    },
    pesquisaDados: function () {
        var that = this;
        var id_projetopedagogico = that.ui.id_projetopedagogico.val();
        var id_entidadematriz = that.ui.id_entidadematriz.val();
        var dt_inicio = that.ui.dt_inicio.val();
        var dt_termino = that.ui.dt_termino.val();

        if (!id_projetopedagogico || !id_entidadematriz || !dt_inicio || !dt_termino) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção',
                text: 'Preencha os campos solicitados!'
            });
        }

        loading();

        var HtmlCheckeBoxCell = Backgrid.Cell.extend({
            render: function () {
                var check = "checked";
                if (!this.model.get('bl_prorroga')) {
                    check = "";
                }

                var elTemp = _.template('<input type="checkbox" ' + check + ' name="bl_prorroga">');
                this.$el.html(elTemp);
                return this;
            },
            events: {
                'change input[name="bl_prorroga"]': 'onChange'
            },
            onChange: function () {
                //verificar se p checkbox esta marcado e seta na modelo true ou false
                var checked = this.$el.find('input[name="bl_prorroga"]').is(":checked");
                this.model.set('bl_prorroga', checked);
            }
        });

        var columns = [{
            name: "bl_prorroga",
            cell: HtmlCheckeBoxCell,
            editable: false,
            headerCell: "select-all"
        }, {
            name: "st_nomecompleto",
            label: "Nome do Aluno",
            editable: false,
            cell: 'string'
        }, {
            name: "st_entidadematriz",
            label: "Entidade",
            editable: false,
            cell: 'string'
        }, {
            name: "dt_cadastro",
            label: "Data da Matrícula",
            editable: false,
            cell: 'string'
        }, {
            name: "dt_terminomatricula",
            label: "Data de Término",
            editable: false,
            cell: 'string'
        }];

        var PageableLog = Backbone.PageableCollection.extend({
            model: VwMatricula,
            url: '/default/matricula/retorna-matricula-params',
            state: {
                pageSize: 30
            },
            mode: "client"
        });
        var data = {
            id_projetopedagogico: that.ui.id_projetopedagogico.val(),
            id_entidadematriz: that.ui.id_entidadematriz.val(),
            dt_inicio: that.ui.dt_inicio.val(),
            dt_termino: that.ui.dt_termino.val(),
            id_situacao: '50',
            termino_matricula:'is not null'
        };

        pageableLog = new PageableLog();

        var RowGrid = Backgrid.Row.extend({
            initialize: function (options) {
                RowGrid.__super__.initialize.apply(this, arguments);
            }
        });
        var pageableGridLog = new Backgrid.Grid({
            className: 'backgrid table table-bordered',
            columns: columns,
            collection: pageableLog,
            emptyText: "Nenhum registro encontrado",
            row: RowGrid
        });

        $('#container-DataGrid').show();
        var $elemento = $('#template-data-grade');
        $elemento.empty();
        $elemento.append(pageableGridLog.render().$el);

        var paginator = new Backgrid.Extension.Paginator({
            collection: pageableLog
        });

        $elemento.append(paginator.render().el);
        $('.container-DataGrid').show();
        pageableLog.fetch({
            data: data,
            reset: true
        });

        $elemento.find("input[type=checkbox]").attr("checked", true);
        $(".select-all-header-cell input[type='checkbox'] ").prop('checked', true);

        loaded();
    },
    prorrogaCollection: function () {
        var that = this;

        parametros = {
            'collection': pageableLog.toJSON(),
            'nu_dias': that.ui.nu_dias.val()
        };

        $.ajax({
            dataType: 'json',
            type: 'post',
            url: '/matricula/prorroga-alunos-grupo',
            data: parametros,
            beforeSend: function () {
                loading();
            },
            success: function (data) {
                $.pnotify({
                    title: data['title'],
                    text: data['mensagem'],
                    type: 'success'
                });
            },
            complete: function () {
                loaded();
            }
        })

    },
    desmarcaTodos: function () {

        if ($(".select-all-header-cell input[type='checkbox']").is(":checked") == true) {
            $("input[type='checkbox']").prop('checked', false);
            $(".select-all-header-cell input[type='checkbox'] ").prop('checked', false);
        } else {
            $(".select-all-header-cell input[type='checkbox'] ").prop('checked', true);
            $("input[type='checkbox']").prop('checked', true);
        }

        var checked = $(".select-all-header-cell input[type='checkbox']").is(":checked");
        var checkedValue = (checked) ? false : true;

        _.each(pageableLog.models, function(val, key){
            val.set('bl_prorroga', checkedValue);
        });
        $("input[type='checkbox']").prop('checked', checkedValue);
    }
});


/**
 ******************************************************************************************
 ******************************************************************************************
 ******************************** SHOW LAYOUT *********************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var telaGrade = new TelaProrrogacaoLayout();
G2S.show(telaGrade);

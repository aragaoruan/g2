/**
 * Created with JetBrains PhpStorm.
 * User: Débora Castro
 * Date: 07/10/13
 * Time: 12:40
 * To change this template use File | Settings | File Templates.
 */

var GerenciaSalasCollection = Backbone.Collection.extend({
    model: SalaDeAula,
    url: "/api/gerencia-salas"
});

//model e view responsáveis pelo carregamento dinâmico do combo select
var SelectDisciplinaModel = Backbone.Model.extend({});
var SelectDisciplinaCollection = Backbone.Collection.extend({
    model: SelectDisciplinaModel,
    url: ''
});
var SelectDisciplinaView = Backbone.View.extend({
    tagName: 'option',
    initialize: function () {
        _.bindAll(this, 'render');
    },
    render: function () {
        $(this.el).attr('value', this.model.get('id')).text(this.model.get('text'));
        return this;
    }
});
var ModelTurma = Backbone.Model.extend({
    defaults: {
        checked: false
    },
    idAttribute: 'id_turma'
});
/**
 * Collection que traz os tipos de sala
 */
var turmasCollection = Backbone.Collection.extend({
    model: ModelTurma
});


$(function () {

    //ação do select Area conhecimento
    $('#id_entidade').off().on('change', function () {
        loading();
        var pesquisaAreaCollection = new SelectDisciplinaCollection();
        pesquisaAreaCollection.url = '/gerencia-salas/get-area-conhecimento-disciplina/id_entidade/' + $(this).val();
        pesquisaAreaCollection.fetch({
            success: function () {
                loaded();
                renderer = new CollectionView({
                    collection: pesquisaAreaCollection,
                    childViewConstructor: SelectDisciplinaView,
                    childViewTagName: 'option',
                    el: $('#id_areaconhecimento')
                });
                renderer.render();
            }
        });
    });

    //executa a pesquisa na modal disciplina
    $("#formPesquisaDisciplina").off().on('submit', function () {
        $('#tabela-retorno-pesquisa-disciplina').html('<tr><td colspan="4"></td></tr>');
        var parametros = $('#formPesquisaDisciplina').serialize();
        loading();
        $.ajax({
            url: 'gerencia-salas/retorno-pesquisa-disciplina',
            type: 'post',
            dataType: 'json',
            data: parametros,
            success: function (data) {
                for (var i = 0; i < data.length; i++) {
                    this.pesquisad += '<tr>';
                    this.pesquisad += '<td><input type="radio" name="idDisciplina" id="idDisciplina" value="' + data[i].id_disciplina + '" /></td>';
                    this.pesquisad += '<td><label id="nomeDisciplina' + data[i].id_disciplina + '">' + data[i].st_disciplina + '</label></td>';
                    this.pesquisad += '<td>' + data[i].st_areaconhecimento + '</td>';
                    this.pesquisad += '<td>' + data[i].st_nivelensino + '</td>';
                    this.pesquisad += '<td>' + data[i].st_serie + '</td>';
                    this.pesquisad += '<tr/>';
                }
                $('#tabela-retorno-pesquisa-disciplina').html(this.pesquisad);
                loaded();
            }, error: function () {
                this.pesquisad = '<tr><td colspan="5">Não há registros para essa pesquisa.</td></tr>';
                $('#tabela-retorno-pesquisa-disciplina').html(this.pesquisad);
                loaded();
            }
        });
        return false;
    });

    //popula o select do moodle
    ComponenteSelectMoodle
        .render({
            el: $("#id_moodle"),
            data: {
                id_entidade: $("#id_entidade").val(),
                id_sistema: $("#id_sistema").val()
            }
        });
});


//método de salvar (com opção de integrar ou não)
$("#formGerenciaSala").off().on('submit', function (e) {
    /* Verifica se as datas são válidas e se não existem.
     salas com o mesmo título. */
    if (!validaDatasAoSalvar() || !nomesSalasRepetidos()) {
        return false;
    } else {
        var checked_ids = [];
        $("#entidades").jstree("get_checked", null, true).each
        (function () {
            checked_ids.push(this.id);
        }); //buscando valores da árvore

        $('#btnSalvar').prop('disabled', true);
        var gerenciaSalas = new SalaDeAula();
        gerenciaSalas.url = '/api/gerencia-salas';
        var paramObj = formToObject('#formGerenciaSala');
        paramObj.entidades = checked_ids;
        paramObj.nu_linhas = $('#nu_linhas').attr('value');

        gerenciaSalas.set(paramObj);
        loading();
        gerenciaSalas.save(null, {
            //resposta da model
            success: function (response) {
                loaded();
                $('#nu_linhas').attr({value: 0}); //controle de quantas linhas foram adicionadas (tratado na camada negogcio)
                $('#btnSalvar').prop('disabled', false); //função desabilitar o botão de salvar, para evitar reenvio de requisição antes do término do processo anterior
                $.pnotify({title: response.changed.title, text: response.changed.text, type: response.changed.type});//mensagem de retorno
                if (response.changed.type == 'success') { //verifica o tipo de retorno para chamada do método de sincronização
                    integracaoSalas(response.changed.mensagem);
                }
            },
            error: function (response) {
                loaded();
                $.pnotify({title: response.changed.title, text: response.changed.text, type: response.changed.type});//mensagem de retorno
            },
            complete: function (response) {
                loaded();
                $('#btnSalvar').prop('disabled', false); //função desabilitar o botão de salvar, para evitar reenvio de requisição antes do término do processo anterior
            }
        });
        return false;
    }

});

/*
 * Método que busca o retorno das salas salvas e monta formulario para escolha 
 * de quais serão sincronizadas
 */
function integracaoSalas(dados) {
    $('#formGerenciaSala').slideToggle();
    $('#sincronizacao-moodle').show();

    var grid_sincronizacao = '';

    $(jQuery.parseJSON(JSON.stringify(dados))).each(function () {
        var ID = this.id_saladeaula;
        var Nome = this.st_saladeaula;

        var id_disciplinaintegracao = this.id_disciplinaintegracao;
        grid_sincronizacao += '<tr>';
        grid_sincronizacao += '<td>' +
            '<input type="hidden" id="id_disciplinaintegracao_' + ID + '" name="id_disciplinaintegracao[]" value="' + id_disciplinaintegracao + '">' +
            '<input type="checkbox" id="id_saladeaula_sincronizar_' + ID + '" name="id_saladeaula_sincronizar[]" class="checkall" value="' + ID + '" />';
        grid_sincronizacao += '</td>';
        grid_sincronizacao += '<td>' + Nome + '</td></tr>';
    });
    $('#conteudo-sincronizacao-moodle tbody').html(grid_sincronizacao);
}

function sincronizarSalas() {
    var parametros = $('#formSincronizarSalas').serialize();
    loading();
    $.ajax({
        url: 'gerencia-salas/sincronizar-salas-de-aula',
        type: 'post',
        dataType: 'json',
        data: parametros,
        success: function (response) {
            loaded();
            $.limparFormulario('#formGerenciaSala'); //limpa formulário principal
            $('.respContent').html(''); //limpa linhas duplicadas
            $('#lbDisciplina').text(''); //limpa o campo disciplina (linha sala de aula)
            $('#lbProjetos').text(''); //limpa o campo de projeto (linha projetos)
            $('#formGerenciaSala').slideToggle();
            $('#sincronizacao-moodle').hide();
            $.pnotify({title: response.title, text: response.text, type: response.type});//mensagem de retorno
        }, error: function (response) {
            loaded();
            $.pnotify({
                title: response.responseText.title,
                text: response.responseText.text,
                type: response.responseText.type
            });//mensagem de retorno
        }
    });
    return false;
}

function naoSincronizar() {
    $('#sincronizacao-moodle').slideToggle();
    $('.respContent').html('');
    $('#lbDisciplina').text(''); //limpa o campo disciplina (linha sala de aula)
    $('#lbProjetos').text(''); //limpa o campo de projeto (linha projetos)
    $('#formGerenciaSala').slideToggle();
}

$(function () {
    $("#entidades").jstree({
        "html_data": {
            "ajax": {
                "url": "util/arvore-entidade",
                "data": function (n) {
                    return {id: n.attr ? n.attr("id") : 0};
                }
            }
        },
        "plugins": ["themes", "html_data", "checkbox", "sort", "ui"]

    }).bind('loaded.jstree', function (event, data) {
        //opcao de marcar todas as entidades
        $("#entidades").jstree('btn_check_all');
    });
});
$.fn.enumerar = function () {
    var num = 1;
    this.each(function () {
        $(this).children('.numeracao').text(num);
        num++;
    });
    return this;
};

$(document).ready(function () {
    //função para clonar linhas de sala de aula (multiplica e altera os ids)
    $("#btnAddResp").click(function (e) {
        e.preventDefault();

        var htmlBox = $("#resp").html();
        var $newResp = $("<div />");
        var ultNum = Number($(".respContent").children(':last-child').children('.numeracao').text()) + 1;
        $('#nu_linhas').attr({value: ultNum});
        $newResp.html(htmlBox);
        $newResp.attr({id: ultNum});
        $newResp.attr({class: 'lnSala linerepeat'});
        $newResp.children('.numeracao').text(ultNum);
        $newResp.children('#div_controle').children('#divexterna').children('#numerador').attr({name: 'linhadisciplina' + ultNum});
        $newResp.children('#div_controle').children('#divexterna').children('#numerador').children('.divButtons').children('.btDisciplina').attr({id: ultNum});
        $newResp.children('#div_controle').children('#divexterna').children('#numerador').children('.divButtons').children('#id_disciplina').attr({
            id: 'id_disciplina' + ultNum,
            value: $('#id_disciplina').val()
        });

        $newResp.children('#div_controle').children('#divexterna').children('#numerador').children('.divButtons').children('#lbDisciplina').attr({id: 'lbDisciplina' + ultNum});
        $newResp.children('#div_controle').children('#divexterna').children('#numerador1').attr({name: 'linhaprojeto' + ultNum});
        $newResp.children('#div_controle').children('#divexterna').children('#numerador1').children('.divButtons').children('.btProjeto').attr({id: ultNum});
        $newResp.children('#div_controle').children('#divexterna').children('#numerador1').children('.divButtons').children('#projetosSalvos').attr({id: 'projetosSalvos' + ultNum});
        $newResp.children('#div_controle').children('#divexterna').children('#numerador1').children('#coordenadores').attr({id: 'coordenadores' + ultNum});

        $newResp.children('#div_controle').children('#divexterna').children('#numerador2').children('.divButtons').children('.btTurma').attr({id: ultNum});
        $newResp.children('#div_controle').children('#divexterna').children('#numerador2').children('.divButtons').children('#turmasSalvas').attr({id: 'turmasSalvas' + ultNum});
        $newResp.children('#div_controle').children('#divexterna').children('#numerador2').attr({name: 'linhaturma' + ultNum});

        $newResp.find('.btnListarProjetos').attr({
            id: ultNum
        });

        $newResp.find('.btnListarTurmas').attr({
            id: ultNum
        });

        $newResp.find('#st_saladeaula').attr({
            name: 'st_saladeaula' + ultNum,
            value: $('#st_saladeaula').val()
        });

        $newResp.find('#dt_inicioinscricao').attr({
            name: 'dt_inicioinscricao' + ultNum,
            value: $('#dt_inicioinscricao').val()
        });

        $newResp.find('#dt_fiminscricao').attr({
            name: 'dt_fiminscricao' + ultNum,
            value: $('#dt_fiminscricao').val()
        });

        $newResp.find('#dt_abertura').attr({
            name: 'dt_abertura' + ultNum,
            value: $('#dt_abertura').val()
        });

        $newResp.find('#dt_encerramento').attr({
            name: 'dt_encerramento' + ultNum,
            value: $('#dt_encerramento').val()
        });

        $newResp.find("#btn_dt_inicioinscricao").attr({name: 'btn_dt_inicioinscricao' + ultNum});
        $newResp.find('#btn_dt_fiminscricao').attr({name: 'btn_dt_fiminscricao' + ultNum});
        $newResp.find('#btn_dt_abertura').attr({name: 'btn_dt_abertura' + ultNum});
        $newResp.find('#btn_dt_encerramento').attr({name: 'btn_dt_encerramento' + ultNum});
        $newResp.find('#id_avaliacaoconjunto').attr({name: 'id_avaliacaoconjunto' + ultNum});
        $newResp.find('#id_avaliacaoconjunto').val($('#id_avaliacaoconjunto option:selected').val());
        $newResp.find('#id_usuario').attr({name: 'id_usuario' + ultNum});
        $newResp.find('#id_usuario').val($('#id_usuario option:selected').val());
        $newResp.find('#id_disciplinaintegracao').attr({name: 'id_disciplinaintegracao' + ultNum});
        $newResp.find('#id_disciplinaintegracao').val($('#id_disciplinaintegracao option:selected').val());


        if ($(".respContent").html() == '') {

            $newResp.find('.numeracao').text(1);
            $newResp.find('#numerador').attr({name: 'linhadisciplina' + 1});
            $newResp.find('.btDisciplina').attr({name: 'linhadisciplina' + 1});
            $newResp.find('.btDisciplina').attr({id: 1});
            $newResp.find('#numerador1').attr({name: 'linhaprojeto' + 1});
            $newResp.find('.btProjeto').attr({name: 'linhaprojeto' + 1});
            $newResp.find('.btProjeto').attr({id: 1});
            $newResp.find('#numerador2').attr({name: 'linhaturma' + 1});
            $newResp.find('.btTurma').attr({name: 'linhaturma' + 1});
            $newResp.find('.btTurma').attr({id: 1});

        }

        $(".respContent").append($newResp);
        $('#qtd_salas').text(parseInt($('.respContent .lnSala').length) + 1);

        /* Ao adicionar uma nova sala,
         * exibe "Salas" corretamente no plural. */
        $("#texto_qtd_salas").html('Salas&nbsp;');
    });

    $('#resp .remover').click(function () {
        $('#resp input,select').val(null);
        $('#lbDisciplina').text('');
    });

    $(".respContent").on('click', '.remover', function (e) {
        e.preventDefault();
        var $box = $(this).closest('.lnSala');
        $(this).closest('.lnSala').remove();
        $('#qtd_salas').text(parseInt($('.respContent .lnSala').length) + 1);
        $box.children().enumerar();

        /*Caso a quantidade de salas seja 1,
         * exibe "Sala" corretamente, no singular. */
        if ($('#qtd_salas').text() == '1') {
            $("#texto_qtd_salas").html('Sala&nbsp');
        }
    });


    $('#inputBuscaProjetos').on('keyup', function () {
        var value = $(this).val().trim();
        var children = $('#tabela-projetos-pedagogicos tr');

        if (value) {
            children.each(function () {
                var child = $(this);
                if (!child.data('label'))
                    child.attr('data-label', child.find('label').text().removeAccents().toLowerCase().trim());
            });
            children.hide().filter('[data-label*="' + value.removeAccents().toLowerCase() + '"]').show();
        }
        else
            children.show();

    });

    validaAnosCampos();
});

function fechaModal() {
    $.limparFormulario('#pesquisaDisciplina');
    $('#pesquisaDisciplina').modal('hide');
}

function setIntegracao(id) {
    $('#bl_integracao').attr({'value': id});
}

function fechar(div) {
    $(div).modal('hide');
}

$('#bl_semencerramento').change(function () {
    if ($('#bl_semencerramento').is(':checked')) {
        $('#nu_diasencerramento').val('');
        $('#nu_diasencerramento').attr('readonly', true);
    } else {
        $('#nu_diasencerramento').removeAttr('readonly');
    }
});

$('#bl_semdiasaluno').change(function () {
    if ($('#bl_semdiasaluno').is(':checked')) {
        $('#nu_diasaluno').val('');
        $('#nu_diasaluno').attr('readonly', true);
    } else {
        $('#nu_diasaluno').removeAttr('readonly');
    }
});

function buscaDisciplina(id) { //seta o id e o nome da disciplina escolhida na modal disciplina dentro do formulário principal
    var id_disc = $('#idDisciplina:checked').val();
    var id_entidadeintegracao = $('#id_moodle option:selected').val();
    var nome_disc = $('#nomeDisciplina' + id_disc).text();
    $("[name='id_avaliacaoconjunto" + id + "']").html('');
    $("[name='id_disciplinaintegracao" + id + "']").html('');

    if (id == '0') {
        $('#lbDisciplina').show();
        $('#lbDisciplina').text(nome_disc);
        $('#id_disciplina').attr({value: id_disc});

        //Carrega o combo da primeira sala sendo cadastrada
        carregaComboAvaliacaoConjunto(id_disc, '');
        $('#id_avaliacaoconjunto').prepend('<option value="">Selecione</option>');
        $('#id_avaliacaoconjunto').attr('required', true);

        //Carrega combo da sala de referencia
        carregaComboSalaReferencia(id_disc, '',id_entidadeintegracao);
        $('#id_disciplinaintegracao').prepend('<option value="">Selecione</option>');
        // $('#id_disciplinaintegracao').attr('required',true);

    } else {
        $('#lbDisciplina' + id).show();
        $('#lbDisciplina' + id).text(nome_disc);
        $('#id_disciplina' + id).attr({value: id_disc});

        //Carrega o combo das demais salas por id definido no evento (btnAddResp) que
        //estao sendo cadastradas
        carregaComboAvaliacaoConjunto(id_disc, id);
        carregaComboSalaReferencia(id_disc, '',id_entidadeintegracao);
    }
    $.limparFormulario('#pesquisaDisciplina');
    $('#pesquisaDisciplina').modal('hide');
}

function marcarModalD(id) {
    if($('#id_moodle option:selected').val()){
        $('.salvaD').attr({'id': id});
        $("#pesquisaDisciplina").modal('show');
        $('#tabela-retorno-pesquisa-disciplina').html('<tr><td colspan="4"></td></tr>');
    }else{
        $.pnotify({
            title: 'Aviso',
            text: 'Selecione um moodle.',
            type: 'warning'
        });
        $('#id_moodle').focus();
    }
}

function marcaModal(id) {
    var array_marcados = ($('#projetosSalvos' + id).val()).split(',');
    $('#tabela-projetos-pedagogicos input:checkbox:checked').each(function (index, currentObject) {
        currentObject.checked = false;
    });
    if ($('#projetosSalvos' + $(this).attr('id')) !== undefined) {
        $('#tabela-projetos-pedagogicos input:checkbox').each(function (index, currentObject) {
            if (Number($.inArray(currentObject.id, array_marcados)) >= 0) {
                currentObject.checked = true;
            }
        });
    }
    $('.salvaPP').attr({'id': id});
}

var capitalize = function (input) {
    return input.toUpperCase();
};

//salva o id a partir da modal Projetos.
$('.salvaPP').click(function () {
    var checked_ids = [];
    $('#tabela-projetos-pedagogicos input:checkbox:checked').each(function (index, currentObject) {
        checked_ids.push(currentObject.id);
    });
    var idIndex = '#' + $(this).attr('id');

    if (checked_ids.length > 0) {
        var msg = 'Listar 1 Projeto';
        if (checked_ids.length > 1) {
            msg = 'Listar ' + checked_ids.length + ' Projetos';
        }

        if ($(this).attr('id') == '') {
            $('#projetosSalvos').attr({value: Array(checked_ids)});
            /* .first() necessário para setar a mensagem no primeiro
             * botão com a classe .btnListarProjetos, visto que o
             * id da primeira sala é uma string vazia.*/
            $('.btnListarProjetos').first().html(msg).show();
        } else {
            $('#projetosSalvos' + $(this).attr('id')).attr({value: Array(checked_ids)});
            $(idIndex + ' .btnListarProjetos').html(msg).show();
        }

        $('#pesquisaProjetos').modal('hide');
    } else {
        if ($(this).attr('id') == '') {
            $('.btnListarProjetos').first().hide();
        } else {
            $(idIndex + '.btnListarProjetos').hide();
        }
        $('#pesquisaProjetos').modal('hide');
    }
});
//método que possibilita o check all na modal de projetos
$('#marcaTodosPP').click(function () {

    $('input.checkall:visible').prop('checked', $(this).prop('checked'));

});

$('#marcaTodosI').click(function () {
    if (this.checked === true) {
        $(":checkbox[class*=checkall]").each(function () {
            this.checked = true;
        });
    } else {
        $(":checkbox[class*=checkall]").each(function () {
            this.checked = false;
        });
    }
});

//funções básicas
$.limparFormulario = function (area) {
    $(area).find('input[type="text"],input[type="radio"],input[type="number"],input[type="checkbox"],textarea,select').val('');
    $(area + ' input').val('');
};

$('#btnAddResp , .remover').tooltip();

function validarDatasInicioFim(name) {

    var datInicio = 'dt_inicioinscricao' + name.substring(15, 16);
    var dataInicio = ($(":input[name*=" + datInicio + "]").val()).split("/");
    var dataFim = ($(":input[name*=" + name + "]").val()).split("/");
    var dataInformadaI = new Date(dataInicio[2], dataInicio[1] - 1, dataInicio[0]);
    var dataInformadaE = new Date(dataFim[2], dataFim[1] - 1, dataFim[0]);
    if (dataInformadaI > dataInformadaE) {
        $.pnotify({
            title: 'Aviso',
            text: 'Data Final não pode ser MENOR que a de Início!',
            type: 'warning'
        });
        $(":input[name*=" + name + "]").val('');
    }
}

function validarDatasAberturaEncerra(name) {
    var datInicio = 'dt_abertura' + name.substring(15, 16);
    var dataInicio = ($(":input[name*=" + datInicio + "]").val()).split("/");
    var dataFim = ($(":input[name*=" + name + "]").val()).split("/");
    var dataInformadaI = new Date(dataInicio[2], dataInicio[1] - 1, dataInicio[0]);
    var dataInformadaE = new Date(dataFim[2], dataFim[1] - 1, dataFim[0]);

    if (dataInformadaI > dataInformadaE) {
        $.pnotify({
            title: 'Aviso',
            text: 'Data de Encerramento não pode ser MENOR que a de Abertura!',
            type: 'warning'
        });
        $(":input[name*=" + name + "]").val('');
    }
}


function calendario(name) {
    var name = name.substring(4);
    $(":input[name=" + name + "]").datepicker({
        format: 'dd/mm/yyyy',
        language: "pt-BR",
        autoclose: true,
        todayHighlight: true,
        clearBtn: true
    });
    $(":input[name=" + name + "]").datepicker('show');
}


function carregaComboAvaliacaoConjunto(id_disciplina, id) {
    id_avaliacaoconjunto = '';

    var VwAvaliacaoConjuntoDisciplinaCollection = Backbone.Collection.extend({
        model: VwAvaliacaoConjuntoDisciplina
    });

    var collection = new VwAvaliacaoConjuntoDisciplinaCollection();
    collection.url = '/api/vw-avaliacao-conjunto-disciplina?id_disciplina=' + id_disciplina;
    collection.fetch();

    $('select[name="id_avaliacaoconjunto' + id + '"]').html('');
    var view = new SelectView({
        el: $("[name='id_avaliacaoconjunto" + id + "']"),
        collection: collection,
        childViewOptions: {
            optionLabel: 'st_avaliacaoconjunto',
            optionValue: 'id_avaliacaoconjunto',
            optionSelected: id_avaliacaoconjunto
        }
    });

    view.render();
}

function carregaComboSalaReferencia(id_disciplina, id, id_entidadeintegracao) {
    console.log(id_entidadeintegracao);

    id_disciplinaintegracao = '';
    var DisciplinaIntegracao = Backbone.Model.extend({});
    var DisciplinaIntegracaoCollection = Backbone.Collection.extend({
        model: DisciplinaIntegracao
    });

    var collection = new DisciplinaIntegracaoCollection();
    collection.url = '/api/disciplina-integracao?id_disciplina=' + id_disciplina + '&id_sistema=' + SISTEMAS.MOODLE + '&id_entidadeintegracao=' + id_entidadeintegracao;
    collection.fetch();

    $('select[name="id_disciplinaintegracao' + id + '"]').html('');
    var view = new SelectView({
        el: $("[name='id_disciplinaintegracao" + id + "']"),
        collection: collection,
        childViewOptions: {
            optionLabel: 'st_salareferencia',
            optionValue: 'id_disciplinaintegracao',
            optionSelected: id_disciplinaintegracao
        }
    });

    view.render();
}


/* VALIDAÇÃO DO ANO:
 *
 *  O jQueryArray idInput armazena os IDs dos inputs
 *  Início / Fim Inscrição e Abertura / Encerramento.
 *  A função valida ano cria um pNotify baseado no ano
 *  inserido no input.
 * */

function validaAnosCampos() {

    var $idInput = $("#dt_inicioinscricao, #dt_fiminscricao, #dt_abertura, #dt_encerramento");

    $(document).on('blur', '#dt_inicioinscricao, #dt_fiminscricao, #dt_abertura, #dt_encerramento', function () {
        var input = $(this);
        var date = new Date(input.val());

        if (!validaAno(date)) {
            //Adiciona borda vermelha, indicando campo inválido.
            input.parent().addClass('error');
        } else {
            //Remove borda vermelha, indicando campo válido.
            input.parent().removeClass('error');
        }
    });

    function validaAno(data) {
        if (data.getFullYear() < 1999) {
            $.pnotify({
                title: "Aviso:",
                text: "O ano deve ser maior que 1999!"
            });
            return false;
        } else {
            return true;
        }
    }
}

function validaDatasAoSalvar() {

    /* Contabiliza a quantidade de elementos com a classe error.
     * Baseado nesse valor, a mensagem de erro adequada será exibida
     * (com ou sem plural).
     */

    var count = $('#linha_sala_componentes').find('.error').length;

    if (count == 1) {
        $.pnotify({
                title: 'Data inválida!',
                text: 'Por favor, corrija a data inválida.',
                type: 'error'
            }
        );
        return false;
    } else if (count > 1) {
        $.pnotify({
                title: 'Datas inválidas!',
                text: 'Por favor, corrija as datas inválidas.',
                type: 'error'
            }
        );
        return false;
    } else {
        return true;
    }
}

function listaProjetosModal(id) {

    /* Necessário para setar os valores corretos
     * nos inputs que vão "alimentar" a lista de
     * projetos pedagógicos. */
    marcaModal(id);

    /* Localiza as strings dos inputs que estão com status checked e as
     adiciona na variável string.
     */

    var strings = [];
    $('#pesquisaProjetos').find('tbody input:checked').each(function (index, currentObject) {
        strings.push(currentObject.getAttribute("data-coordenador"));
    });

    // Seta a string na div vazia do modal que lista os projetos.
    $('#areaListaProjetosPedagogicos').html(strings.join("<br/>"));
}

function listarTurmasModal(id) {

    var strings = [];
    var turmas = ($('#turmasSalvas' + id)[0].getAttribute("turmas")).split(',');
    _.each(turmas, function(item){
        strings.push(item);

    });

    // Seta a string na div vazia do modal que lista os projetos.
    $('#turmasSelecionadas').html(strings.join("<br/>"));
}


// Verifica se existem salas com o mesmo título.
function nomesSalasRepetidos() {

    var arrayTitulos = [];

    $('[id=st_saladeaula]').each(function () {
        arrayTitulos.push($(this).val().toLowerCase());
    });

    for (var i = 0; i < arrayTitulos.length - 1; i++) {
        for (var j = i + 1; j < arrayTitulos.length; j++) {
            if (arrayTitulos[i] == arrayTitulos[j]) {
                $.pnotify({
                    'title': 'Impossível salvar sala',
                    'text': 'Existe mais de uma sala com o nome <strong>' + arrayTitulos[i] + '</strong>. Altere-o e tente novamente!',
                    'type': 'error'
                });
                return false;
            }
        }
    }
    return true;
}

loaded();

/**
 * Desabilita o requerido do campo se for diferente de "Graduação"
 */
if (G2S.loggedUser.get('id_linhadenegocio') != 2) {
    $("#id_periodoletivo").attr('required', false);
}

/**
 * Ao selecionar o periodo letivo, buscar informacoes das datas
 */
$('#id_periodoletivo').on('change', function () {
    var elem = $(this);
    var val = elem.val();
    var dates = $('.dateg');
    var calds = $('.btn_calendario');

    if (2 == G2S.loggedUser.get('id_linhadenegocio')) {
        // Se a linha de negocio da entidade for GRADUACAO

        // pointer-events impede que abra o calendario quando o input tiver focus
        dates.prop('readonly', true).css('pointer-events', 'none');
        calds.prop('disabled', true);

        if (val) {
            loading();
            $.getJSON('/api/periodo-letivo/' + val, function (response) {
                var date_indexes = ['dt_inicioinscricao', 'dt_fiminscricao', 'dt_abertura', 'dt_encerramento'];

                for (var i in date_indexes) {

                    var index = date_indexes[i];
                    var input = $('[name^="' + index + '"]');

                    if (response[index]) {
                        var dt_br = response[index].substr(0, 10).split('-').reverse().join('/');
                        input.val(dt_br);
                    } else {
                        input.val('');
                    }
                }

                loaded();
            });
        } else {
            dates.val('');
        }
    } else {
        dates.prop('readonly', false).css('pointer-events', 'auto');
        calds.prop('disabled', false);
    }
});
$('#bl_todasentidades').on('click', function (e) {
    var elem = $(e.target);

    if (elem.is(':checked')) {
        $("#entidades").jstree("check_all");
        $('#entidades').slideUp();
    } else {
        $("#entidades").jstree("uncheck_all");
        $('#entidades').slideDown();
    }
});

/**
 * Item view de projetos
 */
var itemViewTurmaItem = Marionette.ItemView.extend({
    template: '#item-turma',
    tagName: 'tr',
    ui: {
    },
    events: {},
    initialize: function () {
    },
    onRender: function () {
    }
});


/**
 * Composite filha, para cada módulo vai existir uma composite dessa
 */
var compositeTurma = Marionette.CompositeView.extend({
    template: '#view-cabecalho-turmas',
    el: $('#listaTurmasDisponiveis'),
    tagName: 'div',
    // emptyView:'#empty-salas',
    childView: itemViewTurmaItem,
    childViewContainer: 'tbody',
    ui: {
        checkboxAllTurmas: '#checkboxAllTurmas'
    },
    events: {
        'change @ui.checkboxAllTurmas': 'checkAll'
    },
    initialize: function () {
    },
    onRender: function () {
        return this;
    },
    onShow: function () {
        if(this.options.all_checked){
            this.ui.checkboxAllTurmas.attr({checked: true});
        }
    },
    checkAll: function (el) {
        if (el.currentTarget.checked) {
            $('.checkboxTurma').prop('checked', true);
            _.each(this.collection.models, function (model) {
                model.set('checked', true);
            });
        }else{
            $('.checkboxTurma').prop('checked', false);
            _.each(this.collection.models, function (model) {
                model.set('checked', false);
            })
        }
    }
});

//salva o id a partir da modal Projetos.
$('.salvaTurmas').click(function () {
    var checked_ids = [];
    var checked_names = [];
    $('#tableTurma input:checkbox:checked').each(function (index, currentObject) {
        checked_ids.push(currentObject.id);
        checked_names.push(currentObject.getAttribute("st_turma"));
    });
    var idIndex = '#' + $(this).attr('id');

    if (checked_ids.length > 0) {
        var msg = 'Listar 1 Turma';
        if (checked_ids.length > 1) {
            msg = 'Listar ' + checked_ids.length + ' Turmas';
        }

        if ($(this).attr('id') == '') {
            $('#turmasSalvas').attr({value: Array(checked_ids), turmas: Array(checked_names)});
            /* .first() necessário para setar a mensagem no primeiro
             * botão com a classe .btnListarProjetos, visto que o
             * id da primeira sala é uma string vazia.*/
            $('.btnListarTurmas').first().html(msg).show();
        } else {
            $('#turmasSalvas' + $(this).attr('id')).attr({value: Array(checked_ids), turmas: Array(checked_names)});
            $(idIndex + ' .btnListarTurmas').html(msg).show();
        }

        $('#pesquisaTurmas').modal('hide');
    } else {
        if ($(this).attr('id') == '') {
            $('.btnListarTurmas').first().hide();
        } else {
            $(idIndex + '.btnListarTurmas').hide();
        }
        $('#turmasSalvas'+ $(this).attr('id')).attr({value: Array(), turmas: Array()});
        $('#pesquisaTurmas').modal('hide');
    }
});


$('.input-search-turmas').keyup(function (e) {
    var valorDigitado = e.currentTarget.value;
    var uper = valorDigitado.toUpperCase();
    var tableRows = $(".pesquisa-turma tbody>tr");

    if (valorDigitado.length > 0) {
        tableRows.hide().find("td:contains('" + uper + "')").parent("tr").show();
    } else {
        tableRows.show();
    }
});

function vinculaTurmasModal(id) {

    /* Localiza as strings dos inputs que estão com status checked e as
     adiciona na variável string.
     */

    var strings = [];
    var id_projetos = [];
    var array_marcados = [];
    var all_checked = false;

    var projetosSelecionados = $('#pesquisaProjetos').find('tbody input:checked');

    if (projetosSelecionados.length) {
        $('#pesquisaProjetos').find('tbody input:checked').each(function (index, currentObject) {
            strings.push(currentObject.getAttribute("data-coordenador"));
            id_projetos.push(currentObject.getAttribute("value"));
        });


    } else {
        strings.push('<h6>Nenhum Projeto Pedagogico Selecionado</h6>');
    }


    // Seta a string na div vazia do modal que lista os projetos.
    $('#areaListaProjetosPedagogicosToTurma').html(strings.join("<br/>"));

    var checked_ids = [];
    $("#entidades").jstree("get_checked", null, true).each
    (function () {
        checked_ids.push(this.id);
    }); //buscando valores da árvore

    //verifica se tem unidade e projeto pedagogico selecionados para selecionar as turmas
    if (!checked_ids.length) {
        $('#listaTurmasDisponiveis').html('<h6>Selecione uma entidade em organização para carregar as turmas.</h6>');
        $('.salvaTurmas').addClass('hide');
    } else if (!id_projetos.length) {
        $('#listaTurmasDisponiveis').html('<h6>Selecione um projeto pedagogico para carregar as turmas.</h6>');
        $('.salvaTurmas').addClass('hide');
    } else {

        var tCollection = new turmasCollection();
        tCollection.url = '/gerencia-salas/retorna-turma-projeto-entidade';
        tCollection.fetch({
            async:false,
            data: {
                id_entidadecadastro: checked_ids,
                id_projetopedagogico: id_projetos,
                bl_ativo: true
            },
            beforeSend: loading(),
            success: function () {
                array_marcados = ($('#turmasSalvas' + id).val()).split(',');
                if(array_marcados.length){
                    _.each(tCollection.models, function(model){
                        model.set({'checked': false});
                        if(array_marcados.indexOf(model.get('id_turma')+'') > -1){
                            model.set({'checked': true});
                        }
                    });
                }
                if(array_marcados.length == tCollection.models.length){
                    all_checked = true;
                }
            },
            complete: function(){
                loaded();
            }

        });

        var compTurma = new compositeTurma({collection: tCollection, id_item: id, all_checked: all_checked});
        compTurma.render();
        $('.salvaTurmas').attr({id: id});
        $('.salvaTurmas').removeClass('hide');
    }

}

function habilitaVincularTurma() {
    var bl_vincularturma = $('#bl_vincularturma')[0].checked;
    if(!bl_vincularturma){
        $('.divTurma').addClass('hide');
        $('.btnListarTurmas').addClass('hide');
        $('#labelTurma').addClass('hide');
    }else{
        $('.divTurma').removeClass('hide');
        $('.btnListarTurmas').removeClass('hide');
        $('#labelTurma').removeClass('hide');
    }
}
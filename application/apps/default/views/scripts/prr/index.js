/* 
 * Comecando a trabalhar na funcionalidade de PRR
 */

/**
 * Collection VwAvaliacaoAluno
 * @exp;Backbone
 * @pro;Collection
 * @call;extend
 */

/*
 * Variavel criada para armazenar em escopo global as salas selecionadas
 * para fechar uma venda de PRR
 */
var cartSalaDisciplina = [];

var VwDisciplinasPrrCollectionPageable = Backbone.PageableCollection.extend({
    url: "/disciplina/find-vw-disciplinas-prr",
    model: VwDisciplinasPrr,
    state: {
        pageSize: 10
    },
    mode: 'client'
});
var VwDisciplinasPrrCollection = new VwDisciplinasPrrCollectionPageable();

//Set constante para id_evolucao da venda
VwDisciplinasPrr.ID_EVOLUCAOVENDACONFIRMADA = 10;

var VwResumoFinanceiroCollectionPageable = Backbone.PageableCollection.extend({
    url: "/venda/find-vw-resumo-financeiro",
    model: VwResumoFinanceiro,
    state: {
        pageSize: 100
    },
    mode: 'client',
    valorParcelasRegistradas: function () {
        var nuValor = 0;

        this.each(function (i, o) {
            nuValor += parseFloat(i.get('nu_valor'));
        });

        return nuValor;
    }
});
var VwResumoFinanceiroCollection = new VwResumoFinanceiroCollectionPageable();

var VwSalaDisciplinaPrrCollectionExtend = Backbone.Collection.extend({
    model: VwSalaDisciplinaPrr,
    url: '/sala-de-aula/retorna-vw-sala-disciplina-prr'
});
var VwSalaDisciplinaPrrCollection = new VwSalaDisciplinaPrrCollectionExtend();

var VendaCollectionExtend = Backbone.Collection.extend({
    model: Venda,
    url: '/api/venda'
});
VendaCollection = new VendaCollectionExtend();

var VendaProdutoCollectionExtend = Backbone.Collection.extend({
    model: VendaProduto,
    url: '/api/venda-produto'
});
VendaProdutoCollection = new VendaProdutoCollectionExtend();

var MeioPagamentoCollectionExtend = Backbone.Collection.extend({
    model: MeioPagamento,
    url: '/prr/retorna-meios-pagamento-prr'
});
MeioPagamentoCollection = new MeioPagamentoCollectionExtend();


//View para combo de atendentes
MeioPagamentoView = Backbone.View.extend({
    tagName: 'option',
    initialize: function () {
        _.bindAll(this, 'render');
    },
    render: function () {
        $(this.el).attr('value', this.model.get('id_meiopagamento')).text(this.model.get('st_meiopagamento'));
        return this;
    }
});

PrrViewExtend = Backbone.View.extend({
    model: VwDisciplinasPrr,
    tagName: 'div',
    className: 'div-class-name',
    selectedSalaModel: {},
    cartProdutoPrr: [],
    valorAcumulado: 0,
    valorDisponivelParcelar: 0,
    currentDisciplinaAddCart: 0,
    dataOcorrencia: {},
    el: '#tela-prr',
    initialize: function () {
        this.model.bind('change', this.render());
    },
    render: function () {
        var template = null;

        template = _.template($('#div-content-1').html());
        this.$el.empty().append(template);

        if (!view.model.get('id_matricula')) {
            $.pnotify({title: 'Erro', text: MensagensG2.MSG_PRR_09, type: 'danger'});
        } else {
            this.loadAvaliacoesAluno(view.model.get('id_matricula'), view.model.get('id_ocorrencia'));

            //Recebe a varivel Backbone.View.Extend criada para a aba de ocorrencia
            this.dataOcorrencia = view;
        }

        $('.datepicker').mask('99/99/9999');

        //Ocultando aba de pagamentos enquanto sistema sistema faz verificacao da existencia de registro de venda.
        $('#tab-pagamento').hide();

        return this;
    },
    loadAvaliacoesAluno: function (id_matricula, id_ocorrencia) {

        var url = VwSalaDisciplinaPrrCollection.url;

        var RowGrid = Backgrid.Row.extend({
            initialize: function () {
                RowGrid.__super__.initialize.apply(this, arguments);
                this.$el.css('cursor', 'pointer');
            },
            events: {
                "click": "loadModalSalasPrr"
            },
            loadModalSalasPrr: function () {
                /*
                 * Verificando e mostrando a mensagem ao usuario caso o a evolucao
                 * da venda da disciplina ja estiver com codigo 10 - Confirmada
                 */
                if (this.model.get('id_evolucao') == VwDisciplinasPrr.ID_EVOLUCAOVENDACONFIRMADA) {
                    $.pnotify({title: 'Alerta', text: MensagensG2.MSG_PRR_10, type: 'warning'});
                    return false;
                }

                PrrView.model = this.model;
                that = this;
                var currentModelId = null;

                var ModalSalaDisciplinaAlocacaoView = Backbone.View.extend({
                    tagName: 'tr',
                    className: 'table table-bordered table-condensed table-datagrid',
                    initialize: function () {
                        this.render = _.bind(this.render, this);
                        this.$el.css('cursor', 'pointer');
                    },
                    render: function () {
                        that.selectedSalaModel = {};

                        var variables = this.model.toJSON();

                        this.el.innerHTML = _.template($('#sala-disciplina-alocacao-modal-template').html(), variables);

                        if (parseInt($('#sala-' + this.model.get('id_disciplina')).attr('rel')) === this.model.get('id_saladeaula')) {
                            this.$el.find('.checked-radio-sala').attr('checked', true);
                            currentModelId = this.model.id;
                        } else if (this.model.get('id_vendaproduto')) {
                            this.$el.find('.checked-radio-sala').attr('disabled', true);
                            currentModelId = this.model.id;
                        }

                        return this;
                    },
                    events: {
                        'click input:radio': 'addProdutoVenda'
                    },
                    addProdutoVenda: function () {
                        var id_disciplina = that.model.id;
                        var id_sala = this.model.id;
                        var salaModel = this.model.attributes;

                        if (!this.model.get('id_vendaproduto') && id_disciplina != PrrView.currentDisciplinaAddCart) {

                            if (!cartSalaDisciplina.length) {

                                //Exibindo div da aba disciplina com dados de venda e botao de acao para salvar salas escolhidas
                                $('.div-dados-venda').show();

                                //Add sala de aula ao array de escolhas do usuario
                                cartSalaDisciplina.push({
                                    id_sala: this.model.id,
                                    id_disciplina: that.model.id,
                                    salaModel: salaModel
                                });

                                //Add texto ao label de sala selecionada da lista de disciplinas
                                $('#sala-' + that.model.get('id_disciplina')).text(this.model.get('st_saladeaula')).attr('rel', this.model.id)
                                    .append('<button class="btn btn-mini btn-link" id="clear"><i class="icon-trash icon-white"></i></button>');
                            } else {
                                $.each(cartSalaDisciplina, function (index, value) {
                                    if (id_disciplina == value.id_disciplina && id_sala == value.id_sala) {

                                        //Add sala de aula ao array de escolhas do usuario
                                        cartSalaDisciplina[index] = {
                                            id_sala: id_sala,
                                            id_disciplina: id_disciplina,
                                            salaModel: salaModel
                                        };
                                    } else {
                                        cartSalaDisciplina.push({
                                            id_sala: id_sala,
                                            id_disciplina: that.model.id,
                                            salaModel: salaModel
                                        });
                                    }
                                });

                                //Add texto ao label de sala selecionada da lista de disciplinas
                                $('#sala-' + that.model.get('id_disciplina')).text(this.model.get('st_saladeaula')).attr('rel', this.model.id)
                                    .append('<button class="btn btn-mini btn-link" id="clear"><i class="icon-trash icon-white"></i></button>');
                            }

                            //Armazenando model corrente
                            PrrView.currentDisciplinaAddCart = id_disciplina;
                            currentModelId = id_sala;
                        } else {
                            this.$el.find('.checked-radio-sala').attr('disabled', true).attr('checked', false);
                            $.pnotify({title: 'Alerta', text: MensagensG2.MSG_PRR_08, type: 'warning'});
                        }
                    }
                });

                loading();
                VwSalaDisciplinaPrrCollection.url = url + '?'
                + '&params[id_ocorrencia]=' + id_ocorrencia
                + '&params[id_disciplina]=' + that.model.get('id_disciplina');

                VwSalaDisciplinaPrrCollection.fetch({
                    success: function () {
                        var SalaDisciplinaAlocacaoCollectionView = new CollectionView({
                            collection: VwSalaDisciplinaPrrCollection,
                            childViewConstructor: ModalSalaDisciplinaAlocacaoView,
                            childViewTagName: 'tr',
                            el: $('#modal-salas-prr #tbody')
                        });

                        SalaDisciplinaAlocacaoCollectionView.render();

                        loaded();
                    },
                    error: function () {
                        loaded();
                        $.pnotify({title: 'Erro', text: MensagensG2.MSG_ERROR, type: 'warning'});
                    },
                    complete: function () {
                        if (!VwSalaDisciplinaPrrCollection.length) {
                            $.pnotify({title: 'Informação', text: MensagensG2.MSG_PRR_01, type: 'warning'});
                        }
                    }
                });

                $('#modal-salas-prr').modal('show');
            }
        });

        var columns = [
            {name: 'id_disciplina', label: 'Codigo', editable: false, cell: 'string'},
            {name: 'st_tituloexibicaodisciplina', label: 'Disciplina', editable: false, cell: 'string'},
            {name: 'dt_cadastrosala', label: 'Início', editable: false, cell: 'string'},
            {name: 'dt_encerramentosala', label: 'Término', editable: false, cell: 'string'}
        ];

        VwDisciplinasPrrCollection.url = "/disciplina/find-vw-disciplinas-prr?params[id_matricula]=" + id_matricula + '&params[id_ocorrencia]=' + id_ocorrencia;
        var pageableGrid = new Backgrid.Grid({
            className: 'backgrid table table-bordered',
            columns: columns,
            collection: VwDisciplinasPrrCollection,
            row: RowGrid
        });

        var salaSelecionadaCell = Backgrid.Cell.extend({
            render: function () {
                var elTemp = _.template('<span id="sala-' + this.model.get('id_disciplina')
                + '" class="label sala-selecionada">' + (this.model.get('id_vendaproduto') === '' ? '--' : this.model.get('st_saladeaulaprr')) + '</span>');

                this.$el.attr('data-cel', this.model.get('id_disciplina'));
                this.$el.html(elTemp);

                if (this.model.get('id_vendaproduto') && this.model.get('id_evolucao') < VwDisciplinasPrr.ID_EVOLUCAOVENDACONFIRMADA) {
                    this.$el.find('span').append(' <button class="btn btn-mini btn-link" id="clear"><i class="icon-trash icon-white"></i></button>');
                }

                return this;
            },
            events: {
                'click #clear': function () {
                    this.deleteVendaProduto();
                    return false;
                }
            },
            deleteVendaProduto: function () {

                that = this;

                bootbox.confirm(MensagensG2.MSG_PRR_04, function (result) {

                    if (result) {

                        $('#sala-' + that.model.get('id_disciplina')).removeAttr('rel');
                        $('#sala-' + that.model.get('id_disciplina')).remove();

                        that.$el.append('<span id="sala-' + that.model.get('id_disciplina') + '" class="label sala-selecionada">--</span>');

                        if (that.model.get('id_vendaproduto')) {
                            var vendaProduto = new VendaProduto();

                            vendaProduto.set({id: that.model.get('id_vendaproduto')});
                            vendaProduto.destroy({
                                success: function (response) {
                                    $.pnotify({
                                        title: 'Sucesso',
                                        text: sprintf(MensagensG2.MSG_PRR_06, ''),
                                        type: 'success'
                                    });
                                    that.model.set('id_vendaproduto', null);
                                },
                                error: function () {
                                    $.pnotify({title: 'Erro', text: MensagensG2.MSG_PRR_05, type: 'danger'});
                                },
                                complete: function () {
                                    //Atualizando collection apos delecao de registro
                                    PrrView.render();
                                }
                            });

                        } else {
                            PrrView.currentDisciplinaAddCart = 0;

                            $.each(cartSalaDisciplina, function (index, value) {
                                if (parseInt(value.id_disciplina) === parseInt(that.model.id)) {
                                    cartSalaDisciplina.splice(index);
                                }
                            });
                        }
                    }
                });
            }
        });

        var notaDisciplinaCell = Backgrid.Cell.extend({
            render: function () {
                var content = '';

                if (this.model.get('st_nota')) {
                    content = parseFloat(this.model.get('st_nota')).toFixed(0);
                } else {
                    content = 'S/N';
                }

                var elTemp = _.template(content);
                this.$el.html(elTemp);

                return this;
            }
        });

        pageableGrid.insertColumn([
            {name: 'st_nota', label: 'Nota', editable: false, cell: notaDisciplinaCell},
            {name: 'st_nota', label: 'Sala Selecionada', editable: false, cell: salaSelecionadaCell}
        ]);

        var paginator = new Backgrid.Extension.Paginator({
            collection: VwDisciplinasPrrCollection
        });

        $('#grid-disciplinas-prr').empty().append(pageableGrid.render().$el);
        $('#grid-disciplinas-prr').append(paginator.render().$el);

        loading();
        VwDisciplinasPrrCollection.fetch({
            reset: true,
            success: function (response) {
                if (!response.length) {
                    $.pnotify({title: 'Aviso', text: MensagensG2.MSG_PRR_01, type: 'warning'});
                }
                loaded();
            },
            complete: function () {

                if (VwDisciplinasPrrCollection.models[0].attributes) {
                    //Apresentando aba de pagamento caso haja uma venda para a mesma
                    $('#tab-pagamento').show();
                }
                loaded();
            }
        });

    },
    addPrrCart: function () {

        if (!$('.checked-radio-sala').is(':checked')) {
            $.pnotify({title: 'Aviso', text: MensagensG2.MSG_PRR_02, type: 'warning'});
            $('#table-grid-salas td').addClass('alert alert-danger');
            return false;
        }

        //Validando se o campo data foi preenchido corretamente
        if (!$('#dt-entrega').val()) {
            $.pnotify({title: 'Aviso', text: MensagensG2.MSG_PRR_07, type: 'warning'});
            $('#dt-entrega').focus();
            return false;
        }

        if (cartSalaDisciplina.length) {
            var currentSalaDisciplinaCart = 0;
            var soma = 0;


            $('#modal-salas-prr').modal('hide');

            $.each(cartSalaDisciplina, function (index, value) {
                var result = value.salaModel;

                if (result && result.nu_valor > 0 && currentSalaDisciplinaCart.id_sala != result.id_saladeaula) {

                    //Somatorio para obter valor liquido e bruto
                    soma = soma + result.nu_valor;

                } else {
                    soma = 0;
                }

                currentSalaDisciplinaCart = value;
            });

            this.valorAcumulado = soma;
            $('.div-dados-venda #vl-bruto').val(this.valorAcumulado);
        } else {
            $.pnotify({title: 'Alerta', type: 'warning', text: MensagensG2.MSG_PRR_02});
        }
    },
    salvarVendaPrr: function () {
        var venda = new Venda();
        var vendaProduto = new VendaProduto();
        var vwSalaDisciplinaPrr = null;
        var produtos = [];

        if (cartSalaDisciplina.length) {
            $.each(cartSalaDisciplina, function (index, value) {
                vwSalaDisciplinaPrr = value.salaModel;

                vendaProduto.set({
                    id_produto: vwSalaDisciplinaPrr.id_produto,
                    nu_valorbruto: vwSalaDisciplinaPrr.nu_valor,
                    nu_valorliquido: vwSalaDisciplinaPrr.nu_valor,
                    dt_entrega: vwSalaDisciplinaPrr.dt_entrega,
                    id_matricula: vwSalaDisciplinaPrr.id_matricula
                });

                produtos.push(vendaProduto.clone().toJSON());
            });

            venda.set({
                id: this.model.get('id_venda'),
                id_usuario: this.model.get('id_usuario'),
                id_entidade: this.model.get('id_entidade'),
                nu_valorliquido: (this.valorAcumulado),
                nu_valorbruto: (this.valorAcumulado),
                id_venda: this.model.get('id_venda'),
                id_ocorrencia: this.model.get('id_ocorrencia')
            });

            loading();
            $.ajax({
                url: '/venda/salvar-venda-prr',
                type: 'POST',
                data: {venda: venda.toJSON(), produtos: produtos},
                dataType: 'JSON',
                success: function (response) {
                    $.pnotify({title: 'Sucesso', type: 'success', text: MensagensG2.MSG_PRR_03});
                },
                error: function () {
                    $.pnotify({
                        title: 'Alerta',
                        type: 'warning',
                        text: sprintf(MensagensG2.MSG_ERROR_CUSTOM, 'salvar as salas de PRR')
                    });
                },
                complete: function () {
                    //Atualizando tela apos cadastro de novos produtos
                    PrrView.render();
                    loaded();
                }
            });
        }
    },
    loadAbaPagamento: function () {
        var VwDisciplinasPrrModel = VwDisciplinasPrrCollection.models[0];

        /*
         * Carregando permissoes necessarias de acordo com a funcionalidade
         * 599 -> AbaPrr, e permissao 7 -> Abonar
         */
        if (this.functionVerificaPerfilPermissao(599, 7).length) {
            $('#btn-abonar-venda-prr').show();
        }

        //Carregando o combo meios de pagamento
        this.eventLoadMeiosPagamento();

        //Setando o campo valor total da venda
        $('#nu_valorbrutovenda').val(VwDisciplinasPrrModel.get('nu_valorbrutovenda'));

        //Verificando a situacao de venda ja confirmada
        if (VwDisciplinasPrrModel.get('id_evolucao') >= VwDisciplinasPrr.ID_EVOLUCAOVENDACONFIRMADA) {
            $('#btn-confirmar-venda-prr').attr('disabled', true);
            $('#btn-salvar-prr').attr('disabled', true);

            /*
             * LEIA REGRA: mesmo que a evolucao esteja na situacao 10, o botao abonar
             * so podera ser inativado se a evolucao da venda for igual a (10) e o
             * valor da venda igual a (0)
             */
            if (parseFloat(VwDisciplinasPrrModel.get('nu_valorbrutovenda')).toFixed(2) <= 0) {
                $('#btn-abonar-venda-prr').attr('disabled', true);
            }
        }

        var RowGrid = Backgrid.Row.extend({
            initialize: function () {
                RowGrid.__super__.initialize.apply(this, arguments);
                this.$el.css('cursor', 'pointer');

                if (this.model.get('bl_entrada')) {
                    this.$el.addClass('alert alert-success');
                }

                /*
                 * Verificando se a model atual contem um meio de pagamento 
                 * cartao ou boleto para ativar os botoes de envio de cobranca
                 */
                switch (this.model.get('id_meiopagamento')) {
                    case 1:
                        $('#btn-enviar-cobranca-cartao-prr').show();
                    case 2:
                        $('#btn-enviar-cobranca-boleto-prr').show();
                }

                /**
                 * LEIA REGRA: boleto quitado, venda nao pode ser mais abonada.
                 * Verifica se a entrada e um lancamento do tipo boleto e se o
                 * mesmo ja se encontra quitado.
                 */
                if (this.model.get('bl_quitado') && this.model.get('id_meiopagamento') === 2) {
                    $('#btn-abonar-venda-prr').attr('disabled', true);
                }

                $('#id_meiopagamento').val(this.model.get('id_meiopagamento')).attr('disabled', true);
            }
        });

        //Formatando conteudo da celula para apresentar valor com R$
        var nuValorCell = Backgrid.Cell.extend({
            render: function () {

                var elTemp = _.template('R$ ' + this.model.get('nu_valor').replace('.', ','));
                this.$el.html(elTemp);

                return this;
            }
        });

        var columns = [
            {name: 'nu_parcela', label: 'Número da Parcela', editable: false, cell: 'string'},
            {name: 'nu_valor', label: 'Valor', editable: false, cell: nuValorCell},
            {name: 'st_meiopagamento', label: 'Meio de Pagamento', editable: false, cell: 'string'},
            {name: 'dt_vencimento', label: 'Vencimento', editable: false, cell: 'string'},
            {name: 'dt_quitado', label: 'Data Pagamento', editable: false, cell: 'string'},
            {name: 'st_situacao', label: 'Situação', editable: false, cell: 'string'}
        ];

        VwResumoFinanceiroCollection.url = '/venda/find-vw-resumo-financeiro?params[id_venda]=' + VwDisciplinasPrrModel.get('id_venda');
        var pageableGrid = new Backgrid.Grid({
            className: 'backgrid table table-bordered',
            columns: columns,
            collection: VwResumoFinanceiroCollection,
            row: RowGrid
        });

        var parcelaCell = Backgrid.Cell.extend({
            render: function () {
                var htmlBtnDescatarParcela = '';
                var htmlBtnQuitarBoleto = '';

                if (VwDisciplinasPrrCollection.models[0].get('id_evolucao') < VwDisciplinasPrr.ID_EVOLUCAOVENDACONFIRMADA) {
                    htmlBtnDescatarParcela = '<button class="btn btn-small btn-danger btn-descartar-parcela-prr" title="Descartar parcela"><i class="icon-trash icon-white"></i> Excluir</button>';
                } else {
                    if (this.model.get('bl_entrada') && this.model.get('id_meiopagamento') === 2 && !this.model.get('bl_quitado')) {
                        htmlBtnQuitarBoleto = '<button class="btn btn-small btn-inverse btn-quitar-entrada-boleto-prr" title="Quitar boleto de entrada"><i class="icon-ok icon-white"></i> Quitar</button>';
                    }
                }

                var elTemp = _.template(
                    '<center><div class="btn-group">' + htmlBtnDescatarParcela + htmlBtnQuitarBoleto + '</div></center>');
                this.$el.html(elTemp);
                return this;
            },
            events: {
                "click .btn-descartar-parcela-prr": "deleteParcelaPrr",
                "click .btn-quitar-entrada-boleto-prr": "quitarEntradaBoletoPrr"
            },
            deleteParcelaPrr: function () {
                /**
                 * Este metodo obtem o valor da parcela, add novamente ao montante
                 * de valor disponivel para parcelamento e entao o destroy da collection
                 */
                that = this;

                bootbox.confirm(MensagensG2.MSG_PRR_14, function (result) {

                    if (result) {
                        loading();
                        var nuValor = parseFloat(that.model.get('nu_valor'));
                        PrrView.valorDisponivelParcelar += nuValor;

                        $('#nu_valordisponivelparcelar').val(PrrView.valorDisponivelParcelar);

                        if (!that.model.get('id')) {
                            that.model.destroy();
                            loaded();
                        } else {
                            $.ajax({
                                url: '/venda/cancelar-lancamento',
                                type: 'post',
                                data: {params: [that.model.toJSON()]},
                                success: function () {
                                    that.model.destroy();
                                    that.render();
                                    loaded();
                                }
                            });
                        }

                        /**
                         * Verificando se a parcela apagada e a entrada, para caso seja
                         * reabilitar o combo de id_meiopagamento, para seleção e novas
                         * inclusões de lancamentos.
                         */
                        if (VwResumoFinanceiroCollection.isEmpty()) {
                            $('#id_meiopagamento').removeAttr('disabled');
                        }
                    }
                });
            },
            quitarEntradaBoletoPrr: function () {
                that = this;
                bootbox.confirm(MensagensG2.MSG_PRR_32, function (result) {

                    if (result) {
                        var lancamento = new Lancamento();

                        $.each(that.model.attributes, function (index, value) {
                            if ($.inArray(index, lancamento.attributes)) {
                                lancamento.set(index, value);
                            }
                        });

                        loading();
                        $.ajax({
                            url: '/venda/quitar-boleto',
                            data: {params: lancamento.toJSON()},
                            success: function (data) {
                                $.pnotify({title: 'Sucesso', text: MensagensG2.MSG_PRR_33, type: 'success'});
                                loaded();
                            },
                            error: function () {
                                $.pnotify({
                                    title: 'Danger',
                                    text: sprintf(MensagensG2.MSG_ERROR_CUSTOM, 'quitar boleto'),
                                    type: 'danger'
                                });
                            },
                            complete: function () {
                                PrrView.loadAbaPagamento();
                            }
                        });
                    }
                });
            }
        });

        pageableGrid.insertColumn([
            {name: 'actions', label: 'Opções', editable: false, cell: parcelaCell}
        ]);

        $('#grid-lancamentos-prr').empty().append(pageableGrid.render().$el);

        loading();
        VwResumoFinanceiroCollection.fetch({
            reset: true,
            complete: function () {
                //Definindo valor ainda disponivel para parcelamento
                var parcelasRegistradas = VwResumoFinanceiroCollection.valorParcelasRegistradas();

                var nuValorBruto = VwDisciplinasPrrModel.get('nu_valorbrutovenda').replace('.', '').replace(',', '.');
                PrrView.valorDisponivelParcelar = (nuValorBruto - parcelasRegistradas);

                //Setar texto na tela para o usuario
                $('#nu_valordisponivelparcelar').val(parseFloat(PrrView.valorDisponivelParcelar).toFixed(2));
                loaded();
            }
        });
    },
    addParcela: function () {
        var VwDisciplinasPrrModel = VwDisciplinasPrrCollection.models[0];

        if (this.valorDisponivelParcelar <= 0) {
            $.pnotify({title: 'Alerta', text: MensagensG2.MSG_PRR_18, type: 'warning'});
            return false;

        } else if (!$('#nu_parcela').val()) {
            $.pnotify({title: 'Alerta', text: MensagensG2.MSG_PRR_19, type: 'warning'});
            $('#nu_parcela').attr('required', true).focus();
            return false;

        } else if (!$('#id_meiopagamento').val()) {
            $.pnotify({title: 'Alerta', text: MensagensG2.MSG_PRR_20, type: 'warning'});
            $('#id_meiopagamento').attr('required', true).focus();
            return false;

        } else if (!$('#nu_valor').val()) {
            $.pnotify({title: 'Alerta', text: MensagensG2.MSG_PRR_21, type: 'warning'});
            $('#nu_valor').attr('required', true).focus();
            return false;

        } else if (!$('#dt_vencimento').val()) {
            $.pnotify({title: 'Alerta', text: MensagensG2.MSG_PRR_22, type: 'warning'});
            $('#nu_valor').attr('required', true).focus();
            return false;
        }

        for (var i = 0; i < $('#nu_parcela').val(); i++) {

            /*
             * Caso seja inserido varias parcelas de uma vez, um metodo define automaticamente
             * o valor da proxima parcela para 30 dias após a ultima parcela inserida
             */
            if (i !== 0) {
                $('#dt_vencimento').val(this.functionProximaDataVencimento($('#dt_vencimento').val()));
            }

            /**
             * Setando valores na model que serao salvos para cada parcela
             */
            var Model = new VwResumoFinanceiro();
            Model.set('nu_parcela', (VwResumoFinanceiroCollection.length + 1));
            Model.set('nu_valor', $('#nu_valor').val());
            Model.set('id_meiopagamento', $('#id_meiopagamento :selected').val());
            Model.set('st_meiopagamento', $('#id_meiopagamento :selected').text());
            Model.set('dt_vencimento', $('#dt_vencimento').val());
            Model.set('id_venda', VwDisciplinasPrrModel.get('id_venda'));
            Model.set('id_usuario', VwDisciplinasPrrModel.get('id_usuario'));

            if (parseInt(Model.get('nu_parcela')) === 1) {
                Model.set('bl_entrada', true);
            }

            var nuValor = (Model.get('nu_valor').replace('.', '').replace(',', '.'));

            var boolCriarParcela = (this.valorDisponivelParcelar - nuValor) <= 0 ? false : true;

            if (Boolean(boolCriarParcela)) {

                //Subtraindo novo valor de parcela adicionado do valor disponivel
                this.valorDisponivelParcelar -= nuValor;

                //Setando visualizacao de valor disponivel para usuario
                $('#nu_valordisponivelparcelar').val(parseFloat(this.valorDisponivelParcelar).toFixed(2));

                VwResumoFinanceiroCollection.add(Model);
            } else {
                /**
                 * Esta condicao captura o que restou do valor e cria uma parcela com este ultimo valor
                 * alem de dar um alerta ao usuario informando o que ocorreu
                 */
                Model.set('nu_valor', parseFloat(this.valorDisponivelParcelar).toFixed(2).replace('.', ','));
                this.valorDisponivelParcelar = 0.0;

                VwResumoFinanceiroCollection.add(Model);

                $('#nu_valordisponivelparcelar').val(this.valorDisponivelParcelar);
                $.pnotify({
                    title: 'Alerta',
                    text: sprintf(MensagensG2.MSG_PRR_23, (VwResumoFinanceiroCollection.length)),
                    type: 'danger'
                });
                break;
            }
        }
    },
    functionProximaDataVencimento: function (value) {

        var split = value.split('/');
        var newDate = split[2] + '/' + split[1] + '/' + split[0];

        var data = new Date(newDate);
        data.setMonth(data.getMonth() + 1);

        var d = data.getDate();
        var m = data.getMonth() + 1;
        var y = data.getFullYear();

        return ((d >= 10) ? d : "0" + d) + '/' + ((m >= 10) ? m : "0" + m) + '/' + y;
    },
    functionSuporValorParcela: function () {
        //Metodo magico que ao ser inserido a quantidade de parcelas

        var parcelado = this.valorDisponivelParcelar / parseInt($('#nu_parcela').val());
        $('#nu_valor').val(parseFloat(parcelado).toFixed(2).replace('.', ','));
    },
    eventLoadMeiosPagamento: function () {
        var rendererMeioPagamento = new CollectionView({
            collection: MeioPagamentoCollection,
            childViewConstructor: MeioPagamentoView,
            childViewTagName: 'option',
            el: $('#id_meiopagamento')
        });

        loading();
        MeioPagamentoCollection.url = '/prr/retorna-meios-pagamento-prr';
        MeioPagamentoCollection.fetch({
            success: function () {
                rendererMeioPagamento.render();
                $(rendererMeioPagamento.el).prepend("<option value=''>--</option>");
                loaded();
            },
            error: function () {
                loaded();
                $.pnotify({title: 'Erro', text: MensagensG2.MSG_PRR_24, type: 'danger'});
            }
        });
    },
    save: function () {
        bootbox.confirm(MensagensG2.MSG_PRR_25, function (result) {
            if (result) {
                var request = [];

                $.each(VwResumoFinanceiroCollection.models, function (index, value) {
                    if (value.get('id')) {
                        value.set('nu_valor', value.get('nu_valor').replace('.', ','));
                    }
                    request.push(value.attributes);
                });

                if (!request.length) {
                    $.pnotify({title: 'Alerta', text: MensagensG2.MSG_PRR_26, type: 'warning'});
                    return false;
                }

                loading();
                $.ajax({
                    url: '/venda/salvar-lancamentos-prr',
                    type: 'post',
                    dataType: 'json',
                    data: {params: request},
                    success: function (response) {
                        PrrView.loadAbaPagamento();
                        $.pnotify({title: 'Sucesso', text: 'Sucesso', type: 'success'});
                    },
                    error: function () {
                        $.pnotify({title: 'Erro', text: MensagensG2.MSG_PRR_27, type: 'danger'});
                    },
                    complete: function () {
                        loaded();
                    }
                });
            }
        });
    },
    eventConfirmarVendaPrr: function () {
        that = this;

        if (VwDisciplinasPrrCollection.models[0].get('id_evolucao') == VwDisciplinasPrr.ID_EVOLUCAOVENDACONFIRMADA) {
            $.pnotify({title: 'Alerta', text: MensagensG2.MSG_PRR_13, type: 'warning'});
            return false;
        }

        bootbox.confirm(MensagensG2.MSG_PRR_11, function (result) {
            if (result) {
                loading();
                var venda = new Venda();
                venda.set('id_venda', VwDisciplinasPrrCollection.models[0].get('id_venda'));
                venda.set('id', VwDisciplinasPrrCollection.models[0].get('id_venda'));
                venda.set('id_evolucao', VwDisciplinasPrr.ID_EVOLUCAOVENDACONFIRMADA); //Set evolucao confirmada
                venda.save(null, {
                    success: function () {
                        //Setando todas as evolucoes da model venda da collection VwDisciplinasprr para nova evolucao
                        $.each(VwDisciplinasPrrCollection.models, function (index) {
                            VwDisciplinasPrrCollection.models[index].set('id_evolucao', venda.get('id_evolucao'));
                        });
                        $.pnotify({title: 'Sucesso', text: MensagensG2.MSG_PRR_12, type: 'success'});
                        loaded();
                    },
                    error: function () {
                        $.pnotify({
                            title: 'Alerta',
                            text: sprintf(MensagensG2.MSG_ERROR_CUSTOM, 'concluír a venda'),
                            type: 'error'
                        });
                        loaded();
                    },
                    complete: function () {
                        PrrView.loadAbaPagamento();
                    }
                });
            }
        });
    },
    eventJustificarAbonarVendaPrr: function () {

        bootbox.dialog({
            message: '<textarea class="span5" id="txt-tramite" required="on" rows="2"></textarea>',
            title: "Deseja abonar o aluno de todos os custos dessa venda? \n\Motivo:",
            buttons: {
                success: {
                    label: "Sim",
                    className: "btn-danger",
                    callback: function () {

                        //Validando o formulario de justificativa
                        if (!$('#txt-tramite').val()) {
                            $('.modal-body').prepend('<div class="alert alert-danger"><i class="icon-alert"></i> Favor preencher a justificativa para esta ação</div>');
                            $('#txt-tramite').focus();
                            return false;
                        }

                        var venda = new Venda();
                        venda.set('id_venda', VwDisciplinasPrrCollection.models[0].get('id_venda'));
                        venda.set('id', VwDisciplinasPrrCollection.models[0].get('id_venda'));
                        venda.set('id_evolucao', VwDisciplinasPrr.ID_EVOLUCAOVENDACONFIRMADA); //Set evolucao confirmada
                        venda.set('nu_valorliquido', 0.0); //Zerando o valor da venda
                        venda.set('nu_valorbruto', 0.0); //Zerando o valor da venda

                        var lancamentos = [];

                        $.each(VwResumoFinanceiroCollection.models, function (index, value) {
                            var lancamento = new Lancamento();
                            lancamento.set('id_lancamento', value.get('id_lancamento'));
                            lancamento.set('id', value.get('id_lancamento'));
                            lancamentos.push(lancamento.toJSON());
                        });

                        loading();
                        $.ajax({
                            async: false,
                            url: '/venda/abonar-venda-prr',
                            type: 'POST',
                            data: {
                                params: {
                                    venda: venda.toJSON(),
                                    lancamentos: lancamentos,
                                    tramite: {
                                        st_tramite: sprintf(MensagensG2.MSG_PRR_28, $('#txt-tramite').val()),
                                        ocorrencia: PrrView.dataOcorrencia.model.toJSON()
                                    }
                                }
                            },
                            success: function () {
                                $.each(VwDisciplinasPrrCollection.models, function (index) {
                                    VwDisciplinasPrrCollection.models[index].set('id_evolucao', venda.get('id_evolucao'));
                                    VwDisciplinasPrrCollection.models[index].set('nu_valorbrutovenda', venda.get('nu_valorbruto'));
                                });
                                $.pnotify({title: 'Sucesso', text: MensagensG2.MSG_PRR_29, type: 'success'});
                                $.pnotify({title: 'Sucesso', text: MensagensG2.MSG_PRR_30, type: 'success'});

                                loaded();
                            },
                            error: function () {
                                $.pnotify({title: 'Alerta', text: MensagensG2.MSG_PRR_31, type: 'error'});
                                loaded();
                            },
                            complete: function () {
                                PrrView.loadAbaPagamento();
                            }
                        });
                    }
                },
                close: {
                    label: "Cancelar",
                    className: "btn",
                    callback: function () {

                    }
                }
            }
        });
    },
    eventEnviarCobranca: function (id_meiopagamento) {
        that = this;

        bootbox.confirm(MensagensG2.MSG_PRR_15, function (result) {
            if (result) {
                loading();

                var vwResumoFinanceiro = new VwResumoFinanceiro();
                vwResumoFinanceiro.set('id_venda', VwDisciplinasPrrCollection.models[0].get('id_venda'));
                vwResumoFinanceiro.set('id_meiopagamento', id_meiopagamento);

                $.ajax({
                    url: '/prr/enviar-mensagem-cobranca',
                    type: 'post',
                    data: {params: vwResumoFinanceiro.toJSON()},
                    success: function (response) {
                        loaded();
                        if (parseInt(response.tipo) === 1) {
                            $.pnotify({title: 'Sucesso', text: MensagensG2.MSG_PRR_16, type: 'success'});
                        } else {
                            $.pnotify({title: 'Alerta', text: MensagensG2.MSG_PRR_17, type: 'warning'});
                        }
                    },
                    complete: function () {
                        PrrView.loadAbaPagamento();
                    }
                });
            }
        });
    },
    functionVerificaPerfilPermissao: function (id_funcionalidade, id_permissao) {
        var PerfilPermissaoFuncionalidadeExtend = Backbone.Collection.extend({
            model: PerfilPermissaoFuncionalidade,
            url: '/util/perfil-permissao-funcionalidade'
        });
        var PerfilPermissaoFuncionalidadeCollection = new PerfilPermissaoFuncionalidadeExtend();


        PerfilPermissaoFuncionalidadeCollection.url = '/util/perfil-permissao-funcionalidade/id_permissao/' + id_permissao + '/id_funcionalidade/' + id_funcionalidade;
        PerfilPermissaoFuncionalidadeCollection.fetch({
            async: false
        });

        return PerfilPermissaoFuncionalidadeCollection;
    },
    events: {
        "click #btn-selecionar-sala": 'addPrrCart',
        "click #btn-concluir-venda": 'salvarVendaPrr',
        "click #tab-pagamento": 'loadAbaPagamento',
        "click #btn-add-parcela": 'addParcela',
        "keyup #nu_parcela": 'functionSuporValorParcela',
        "click #btn-salvar-prr": 'save',
        "click #btn-confirmar-venda-prr": 'eventConfirmarVendaPrr',
        "click #btn-abonar-venda-prr": 'eventJustificarAbonarVendaPrr',
        "click #btn-enviar-cobranca-boleto-prr": function () {
            this.eventEnviarCobranca(2);
        },
        "click #btn-enviar-cobranca-cartao-prr": function () {
            this.eventEnviarCobranca(1);
        },
    }

});

$(document).ready(function () {
    PrrView = new PrrViewExtend();

//    $("#nu_parcelas").spinner({
//        step: 1,
//        min: 0,
//        max: 99
//    });

    $("#nu_parcela").numeric(",");

    $("#nu_valor").numeric(",");
    $('#nu_valor').maskMoney({showSymbol: true, decimal: ",", thousands: "."});

});
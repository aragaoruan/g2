/**
 * View geral para a funcionalidade de cadastro de manual do aluno
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */

/***********************
 *   ManualDoAlunoModel
 **********************/

var ManualDoAlunoModel = Backbone.Model.extend({
    url: '/api/manual-do-aluno/',
    defaults: {
        id_upload: '',
        st_upload: ''
    }
});


/***********************
 *   ManualDoAlunoView
 **********************/

var ManualDoAlunoView = Marionette.ItemView.extend({
    template: _.template($('#manual-do-aluno-enviado').html()),
    model: new ManualDoAlunoModel(),
    tagName: 'form',
    className: 'row-fluid',
    render: function () {
        var that = this;

        var variables = that.model.fetch({
            success: function (model, response) {
                that.el.innerHTML = that.template(response);
                loaded();
            }
        });
    },
    events: {
        'change #manual-do-aluno-arquivo': 'toggleSalvar',
        'click #manual-do-aluno-salvar': 'salvar'
    },
    toggleSalvar: function () {
        var arquivo = $('#manual-do-aluno-arquivo');
        if (arquivo.val() != '')
            $('.form-actions').show();
        else
            $('.form-actions').hide();
    },
    salvar: function (e) {
        e.preventDefault();

        var that = this;

        var arquivos = $('#manual-do-aluno-arquivo')[0].files;
        var formData = new FormData();

        for (var i = 0; i < arquivos.length; i++) {
            var file = arquivos[i];

            // Validar extensao do arquivo
            var extensions = ['.pdf', '.doc', '.docx', '.jpg', '.jpeg', '.png'];
            var extensaoValida = (new RegExp('(' + extensions.join('|').replace(/\./g, '\\.') + ')$')).test(file.name);

            if (extensaoValida)
                formData.append('arquivo', file, file.name);
            else {
                $.pnotify({title: 'Aviso', text: 'Esta extensão de arquivo não é permitida.', type: 'warning'});
                return false;
            }
        }

        var template = _.template($('#manual-do-aluno-carregando').html());
        that.el.innerHTML = template();

        var progressBar = $('#manual-do-aluno-progress');

        $.ajax({
            url: 'manual-do-aluno/upload',
            type: 'post',
            data: formData,
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false,
            progressUpload: function (evt) {
                if (evt.lengthComputable) {
                    var value = parseInt((evt.loaded / evt.total * 100));
                    progressBar.find('.bar').css('width', value + '%');
                }
            },
            success: function (response, textStatus, jqXHR) {
                if (response.type == 'success')
                    $.pnotify({title: 'Concluído', text: 'O novo arquivo foi salvo!', type: 'success'});
                else
                    this.error();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $.pnotify({title: 'Erro', text: 'Houve um erro ao salvar o arquivo.', type: 'error'});
            },
            complete: function () {
                that.render();
            }
        });
    }
});


var ManualDoAluno = new ManualDoAlunoView();
G2S.layout.content.show(ManualDoAluno);


/***********************
 *   Configurar progresso
 *   do upload AJAX
 **********************/
(function addXhrProgressEvent($) {
    var originalXhr = $.ajaxSettings.xhr;
    $.ajaxSetup({
        progress: function () {
        },
        progressUpload: function () {
        },
        xhr: function () {
            var req = originalXhr(), that = this;
            if (req.upload) {
                if (typeof req.upload.addEventListener == "function") {
                    req.upload.addEventListener("progress", function (evt) {
                        that.progressUpload(evt);
                    }, false);
                }
            }
            return req;
        }
    });
})(jQuery);
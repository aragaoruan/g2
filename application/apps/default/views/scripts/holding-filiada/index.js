/**
 * CADASTRO DE Holding Filiada
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */

/*********************************************
 ************** COLLECTIONS ******************
 *********************************************/
var ColletionSituacao = Backbone.Collection.extend({
    model: Situacao,
    url: '/api/situacao/?st_tabela=tb_holding',
    initialize: function () {
        this.fetch({async: false});
    }
});

var CheckBoxCellEmitirRelatorio = Backgrid.Cell.extend({
    render: function () {
        var check = "";
        var disable = "";
        if (this.model.get('bl_emiterelatorioholding')) {
            check = "checked='checked'";
        }
        if(!this.model.get('id_holdingfiliada'))
        {
            disable = "disabled";
            check = "";
        }
        var temp = _.template('<input type="checkbox" ' + check + disable +' class="bl_emiterelatorioholding" name="bl_emiterelatorioholding">');
        this.$el.html(temp);
        return this;
    },
    events: {
        'change input[name="bl_emiterelatorioholding"]': 'onChange'
    },
    onChange: function () {
        var that = this;

        var params = {
            id_holding: that.model.get('id_holding'),
            id_holdingfiliada: that.model.get('id_holdingfiliada'),
            bl_emiterelatorio: this.$el.find('input[name="bl_emiterelatorioholding"]').is(':checked')
        };
        $.ajax({
            dataType: 'json',
            type: 'get',
            url: '/holding-filiada/salva-holding-filiada',
            data: params,
            beforeSend: function () {
                loading();
            },
            success: function (data) {
                layoutHoldingFiliada.renderGrid();
                $.pnotify({
                    type: data.type,
                    title: data.title,
                    text: data.text
                });
            },
            complete: function () {
                loaded();
            },
            error: function (response) {
                var textResponse = JSON.parse(response.responseText);
                $.pnotify(textResponse);
            }
        });

    }
});

var HtmlCheckBoxCell = Backgrid.Cell.extend({
    render: function () {
        if (this.model.get('bl_ativo')) {
            var check = "checked='checked'";
        }

        var elTemp = _.template('<input type="checkbox" ' + check + ' class="id_holdingfiliada" name="id_holdingfiliada">');
        this.$el.html(elTemp);
        return this;
    },
    events: {
        'change input[name="id_holdingfiliada"]': 'onChange'
    },
    onChange: function () {
        var that = this;

        /*if (!that.$el.find('input[name="id_holdingfiliada"]').is(':checked')) {
            bootbox.confirm("Tem certeza de que deseja remover o vinculo com esta Holding?", function (result) {
                if (result) {
                    that.model.url = '/holding-filiada/desvincular/id/' + that.model.id;
                    that.model.destroy({
                        beforeSend: function () {
                            loading();
                        },
                        complete: function () {
                            loaded();
                        },
                        success: function () {
                            $.pnotify({
                                title: 'Registro Removido',
                                text: 'O registro foi desvinculado com sucesso!',
                                type: 'success'
                            });
                            that.remove();
                        }
                    });

                }
            });
        }*/


        var params = {
            id_holding: that.model.get('id_holding'),
            id_holdingfiliada: that.model.get('id_holdingfiliada'),
            bl_emiterelatorio: that.model.get('bl_emiterelatorioholding')
        };
        $.ajax({
            dataType: 'json',
            type: 'get',
            url: '/holding-filiada/salva-holding-filiada',
            data: params,
            beforeSend: function () {
                loading();
            },
            success: function (data) {
                layoutHoldingFiliada.renderGrid();
                $.pnotify({
                    type: data.type,
                    title: data.title,
                    text: data.text
                });
            },
            complete: function () {
                loaded();
            },
            error: function (response) {
                var textResponse = JSON.parse(response.responseText);
                $.pnotify(textResponse);
            }
        });

    }
});

var RowGrid = Backgrid.Row.extend({
    initialize: function (options) {
        RowGrid.__super__.initialize.apply(this, arguments);
    }
});

var PageableLog = Backbone.PageableCollection.extend({
    model: Holding,
    url: '/holding-filiada/busca-holding-filiada',
    state: {
        pageSize: 30
    },
    mode: "client"
});

var columns = [{
    name: "id_holding",
    label: "Holdings Habilitadas",
    cell: HtmlCheckBoxCell,
    className: 'span1',
    editable: false,
    headerCell: "select-all"
}, {
    name: "st_holding",
    label: "Nome da Holding",
    editable: false,
    cell: 'string'
},
    {
        name: "bl_emiterelatorioholding",
        label: "Emite Relatório",
        cell: CheckBoxCellEmitirRelatorio,
        className: 'span1',
        editable: false
    }];

/*********************************************
 ************** LAYOUT ***********************
 *********************************************/
var LayoutHoldingFiliada = Marionette.LayoutView.extend({
    template: '#template-dados-filiada',
    tagName: 'div',
    className: 'container-fluid',
    events: {
        "click #btn-salvar": "salvarDados"
    },
    onShow: function () {
        this.renderGrid();
    },
    renderGrid: function () {
        var pageableLog = new PageableLog();

        var pageableGridLog = new Backgrid.Grid({
            className: 'backgrid table table-bordered',
            columns: columns,
            collection: pageableLog,
            emptyText: "Nenhum registro encontrado",
            row: RowGrid
        });


        this.$el.find('#container-DataGrid').show();

        var $elemento = this.$el.find('#template-data-grade');
        $elemento.empty();
        $elemento.append(pageableGridLog.render().$el);

        var paginator = new Backgrid.Extension.Paginator({
            collection: pageableLog
        });

        $elemento.append(paginator.render().el);
        //$('.container-DataGrid').show();
        pageableLog.fetch({});

        $(".select-all-header-cell input[type='checkbox'] ").hide();
        $(".select-all-header-cell").html('Holdings Habilitadas');

        loaded();


    },
    salvarDados: function () {
        that = this;
        if ($('#id_situacao').val() != '') {


            parametros = {
                'id_situacao': $('#id_situacao').val(),
                'id_holding': $('.id_holding').val(),
                'st_holding': $('#st_holding').val()
            };

            $.ajax({
                type: 'post',
                url: '/holding/salvar-holding',
                data: parametros,
                beforeSend: function () {
                    loading();
                },
                success: function (data) {
                    $.pnotify({
                        title: data['title'],
                        text: data['mensagem'],
                        type: 'success'
                    });
                },
                complete: function () {
                    loaded();
                }
            })
        } else {
            $.pnotify({
                title: 'Campo Obrigatório!',
                text: 'Selecione um status',
                type: 'error'
            });
        }
    }
});

/*********************************************
 ************** INICIALIZAÇÃO ****************
 *********************************************/
var layoutHoldingFiliada = new LayoutHoldingFiliada();
G2S.show(layoutHoldingFiliada);
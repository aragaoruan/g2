var campanhaPontualidadeCollectionObj,
    campanhaComercialCollectionObj,
    collectionResponsavelFinanceiro,
    aproveitamentoSolicitacaoModal,
    viewBotoes,
    modalAproveitamento,
    aproveitamentoInteracoes;

// Instancias globais para ItemView de cada secao
var viewCursosInformacoes,
    viewContratoInformacoes,
    viewMensalidadeInformacoes,
    viewAtendenteInformacoes,
    viewPagamentoInformacoes;


//Elementos da secao de Lancamentos
var COMBO_MEIO_ENTRADA = 1,
    COMBO_MEIO_PARCELA = 2,
    INPUT_VENCIMENTO_ENTRADA = 3,
    INPUT_VENCIMENTO_PARCELA = 4,
    COMBO_RESPONSAVEL = 5;

// Meios de Pagamento
var PAG_CARTAO = 1,
    PAG_BOLETO = 2;

/**********************************************************************************************
 * ******************************      MODELOS     ****************************************
 **********************************************************************************************/

/**
 * Modelo principal, utilizada para armazenar algumas informações.
 * @type {Backbone.Model}
 */
var informacoesModel = Backbone.Model.extend({
    defaults: {
        id_produto: "",
        id_usuario: "",
        id_vendaproduto: ""
    },
    url: "/venda-graduacao/retorna-informacoes-valores"
});

/**
 * Model responsável por armazenar as informações do contrato
 */
var contratoModel = Backbone.Model.extend({
    defaults: {
        'nu_creditos': '',
        'nu_valorcredito': '',
        'nu_valorcreditomostrar': '',
        'nu_valorcontrato': '',
        'nu_valorcontratomostrar': '',
        'nu_desconto': '',
        'nu_descontomostrar': '',
        'nu_valornegociacao': '',
        'nu_valornegociacaomostrar': '',
        'id_campanhacomercial': ''
    }
});

/**
 * Model responsável por armazenar as informações de mensalidade.
 */
var mensalidadeModel = Backbone.Model.extend({
    defaults: {
        'nu_valorentrada': '',
        'nu_valorentradamostrar': '',
        'nu_valordescontoentrada': '',
        'nu_valordescontoentradamostrar': '',
        'nu_valormensalidade': '',
        'nu_valormensalidademostrar': '',
        'id_campanhapontualidade': '',
        'nu_valormensalidadepontualidade': '',
        'nu_valormensalidadepontualidademostrar': ''
    }
});

/**
 * Modelo secundária, utilizada para armazenar as informações de atendente e regra de contrato.
 * Utilizada devido a ser informações que não terão impacto direto nas informações da tela, não sendo
 * necessário recarregar toda a tela no eventoModel.
 */
var secondModel = Backbone.Model.extend({
    defaults: {
        'id_atendente': '',
        'id_contratoregra': '',
        'id_turma': '',
        'id_tiposelecao': ''
    }
});

/**
 * Model responsável por armazenar as informações de pagamento.
 */
var PagamentoModel = Backbone.Model.extend({
    defaults: {
        "id_meiopagamentoentrada": "",
        "dt_vencimentoentrada": "",
        "id_meiopagamentoparcelas": "",
        "nu_vencimentoparcelas": "",
        "nu_mesesintervalovencimento": "",
        "id_responsavelfinanceiro": "",
        "bl_quitadaentrada": ""
    }
});

/**
 * Model responsável por armazenar as informações da solicitação de aproveitamento.
 * @type (Backbone.Model)
 */
var AproveitamentoModel = Backbone.Model.extend({
    defaults: {
        id_ocorrencia: '',
        st_ocorrencia: '',
        st_nomeinteressado: ''
    }
});

/**
 * Módel Responsaveis Financeiro
 * @type {Backbone.Model}
 */
var ResponsavelFinanceiroModel = Backbone.Model.extend({
    defaults: {
        id: '',
        st_cpf: '',
        st_nomecompleto: '',
        st_cnpj: '',
        st_tiporesponsavel: 'Fisica'
    },
    idAttribute: 'id_usuario'
});

//Collection de vw_assuntos
var VwSubAssuntosCollection = Backbone.Collection.extend({
    model: VwAssuntoCo,
    url: '/ocorrencia/find-sub-assunto/'
});

var VwAssuntoNucleoPessoaCollection = Backbone.Collection.extend({
    model: VwAssuntoNucleuPessoa
});

var VwOcorrenciasPessoaCollection = Backbone.Collection.extend({
    model: VwOcorrenciasPessoa
});

var VwOcorrenciaInteracaoCollection = Backbone.Collection.extend({
    model: VwOcorrenciaInteracao,
    url: '/ocorrencia/find-interacao'
});

/**
 * Instancia a model de venda e adiciona um atributo fake
 * @type {Backbone.Model}
 */
var VendaModel = Venda.extend({
    initialize: function () {
        return this;
    },
    idAttribute: 'id_venda',
    url: function () {
        return '/venda-graduacao/salvar-venda/' + (this.id || '');
    }
});

/**
 * Instâncias das modelos.
 * @type {Backbone.Model}
 */
var vendaModel = new VendaModel();
var mainModel = new informacoesModel();
var secModel = new secondModel();
var conModel = new contratoModel();
var mensModel = new mensalidadeModel();
var pagamentoModel = new PagamentoModel();
var aproveitamentoModel = new AproveitamentoModel();


/**********************************************************************************************
 * ******************************      COLLECTIONS     ****************************************
 **********************************************************************************************/

/**
 * Collection de curso
 */
var cursoCollection = Backbone.Collection.extend({
    model: ProdutoEntidade
});

/**
 * Collection de turma
 */
var turmaCollection = Backbone.Collection.extend({
    model: VwTurmasProduto
});

/**
 * Collection que traz os processos de seleção
 */
var processoSeletivoCollection = Backbone.Collection.extend({
    model: TipoSelecao
});

/**
 * Collection de atendentes
 */
var NucleoFuncionariotmCollection = Backbone.Collection.extend({
    model: NucleoFuncionarioTm,
    url: '/venda/retornar-funcionario-nucleo'
});

/**
 * Collection para Regra de Contrato
 */
var RegraContratoCollection = Backbone.Collection.extend({
    model: ContratoRegra,
    url: '/api/contrato-regra'
});

/**
 * Collection para campanha de pontualidade
 */
var CampanhaPontualidadeCollection = Backbone.Collection.extend({
    model: CampanhaComercial,
    url: function () {
        return '/api/campanha-comercial/?' + $.param(_.defaults(this.data));
    },
    data: {
        id_tipocampanha: null,
        id_tipodesconto: null
    }
});

/**
 * Collection meio de pagamento
 */
var MeioPagamentoCollection = Backbone.Collection.extend({
    model: MeioPagamento
});

/**
 * Collection para Responsavel Financeiro
 */
var ResponsavelFinanceiroCollection = Backbone.Collection.extend({
    model: ResponsavelFinanceiroModel
});

// Instancia a collection responsavel financeiros
collectionResponsavelFinanceiro = new ResponsavelFinanceiroCollection();


/**
 * Collection Vw_vendalancamento
 * @type {Backbone.Collection}
 */
var VwVendaLancamentoCollection = Backbone.Collection.extend({
    model: VwVendaLancamento,
    url: '/api/vw-venda-lancamento'
});


var vwVendaLancamentoCollection = new VwVendaLancamentoCollection();


/**
 * Collection da GRADE de SALAS
 */
var GridCollection = Backbone.Collection.extend({
    url: '/venda-graduacao/get-salas-by-turma/',
    parse: function (response) {
        this.groups = response.grupos || [];
        this.ofertas = response.ofertas;
        return response.salas || response || [];
    }
});

/**
 * Collection de Assunto da solicitação de aproveitamento
 */
var AssuntoCollection = Backbone.Collection.extend({
    model: AssuntoCo
});

/**
 * Collection de Subassunto da solicitação de aproveitamento
 */
var SubAssuntoCollection = Backbone.Collection.extend({
    model: AssuntoCo
});

var VwNucleoAssuntoCoCollection = Backbone.Collection.extend({
    model: VwNucleoAssuntoCo
});

/**
 *
 */

var ModelSalaDeAula = Backbone.Model.extend({
    idAttribute: 'id_saladeaula'
});

var CollectionSalaDeAula = Backbone.Collection.extend({
    model: ModelSalaDeAula
});

var collectionDisciplinasSelecionadas = new CollectionSalaDeAula();
var collectionGridPadrao = new GridCollection();
var collectionGridExcepcional = new GridCollection();
var collectionGridEstendidas = new GridCollection();

/**********************************************************************************************
 * ******************************        LAYOUTS       ****************************************
 **********************************************************************************************/

/**
 * Layout que vai conter as regiões as demais regiões como botões, informações, atendentes...
 */
var LayoutInformacoes = Backbone.Marionette.LayoutView.extend({
    template: '#layout-informacoes',
    tagName: 'div',
    regions: {
        regiao1: '#wrapper-informacoes-1',
        regiao2: '#wrapper-informacoes-2',
        regiao3: '#wrapper-informacoes-3',
        regiao4: '#wrapper-informacoes-4',
        regiao5: '#wrapper-informacoes-5',
        regiaoGrade: '#wrapper-informacoes-grade'
    },
    events: {},
    mostraTelaInformacoes: function () {
        var layouInformacoes = telaVendas.informacoesRegion.currentView;

        viewCursosInformacoes.$el.show();
        viewContratoInformacoes.$el.show();
        viewMensalidadeInformacoes.$el.show();
        viewAtendenteInformacoes.$el.show();
        viewPagamentoInformacoes.$el.show();
        viewBotoes.$el.show();

        layouInformacoes.regiaoGrade.currentView.$el.hide();
    },
    mostraTelaGrid: function () {
        var layouInformacoes = telaVendas.informacoesRegion.currentView;

        viewCursosInformacoes.$el.hide();
        viewContratoInformacoes.$el.hide();
        viewMensalidadeInformacoes.$el.hide();
        viewAtendenteInformacoes.$el.hide();
        viewPagamentoInformacoes.$el.hide();
        viewBotoes.$el.hide();

        var model = conModel;
        model.set(mensModel.toJSON());
        model.url = '/venda-graduacao/calcula-valores/';

        layouInformacoes.regiaoGrade.show(new ViewGradeDisciplina({
            model: model,
            previousConModel: conModel.clone(),
            previousMensModel: mensModel.clone()
        }));

        var ContratoViewExtend = ViewContratoInformacoes.extend({
            model: model,
            onRender: function () {
                this.populaSelectCampanhaComercial();

                // Esconde o botao "Alterar/Visualizar Grade"
                this.ui.btn_grade.hide();
            }
        });
        var contratoView = new ContratoViewExtend();
        layouInformacoes.regiaoGrade.currentView.gridContratoRegion.show(contratoView);


        var MensalidadeViewExtend = ViewMensalidadeInformacoes.extend({
            model: model,
            modelEvents: {
                change: 'render'
            },
            initialize: function () {
                // Remove o initialize do pai, pq nao precisa do combobox
            },
            onRender: function () {
                this.ui.selectCampanhaPontualidade.hide();

                var percent = mensModel.get('nu_porcentagemdescontopontualidade') || 0;
                this.ui.selectCampanhaPontualidade.after('<b>' + (percent ? percent + '%' : 'Nenhuma') + '</b>');
            }
        });
        var mensalidadeView = new MensalidadeViewExtend();
        layouInformacoes.regiaoGrade.currentView.gridMensalidadeRegion.show(mensalidadeView);
    }
});

/**
 * Layout que vai gerenciar o layout total da tela, contendo as regiões do autocomplete e da negociação
 */
var LayoutVendaGraduacao = Backbone.Marionette.LayoutView.extend({
    template: '#layout-venda-graduacao',
    tagName: 'div',
    autoComplete: componenteAutoComplete,
    regions: {
        adicionarPessoaRegion: '#container-adicionar-pessoa',
        informacoesRegion: '#wrapper-informacoes',
        lancamentosRegion: '#wrapper-lancamentos',
        botoesRegion: '#wrapper-botoes',
        modal: '#box-modal-rf',
        modalSolicitacao: '#box-modal-solicitacao'
    },
    events: {
        'click @ui.btnAdicionarPessoa': 'adicionarPessoa',
        'click @ui.btnOcultarForm': 'ocultarFormulario'
    },
    ui: {
        autoComplete: '#autoComplete',
        contentBoxVendas: '.content-box-vendas',
        btnAdicionarPessoa: '#btn-adicionar-pessoa',
        btnOcultarForm: '#btn-ocultar-form'
    },
    initialize: function () {
        this.autoComplete.clearFixModel();
    },
    onShow: function () {
        var that = this;
        loading();
        var autocomplete = this.autoComplete;

        //Verifica se o G2S.editModel esta preenchido, isso significa que estou editando ou já salvei a venda
        if (G2S.editModel) {
            //Seta os dados da venda
            vendaModel.set(G2S.editModel);

            mainModel.set({
                id_usuario: vendaModel.get('id_usuario'),
                id_produto: vendaModel.get('id_produto')

            });
            conModel.set({
                nu_creditos: vendaModel.get('nu_creditos'),
                id_campanhacomercial: vendaModel.get('id_campanhacomercial')
            });
            mensModel.set({
                id_campanhapontualidade: vendaModel.get('id_campanhapontualidade')
            });
            secModel.set({
                id_atendente: vendaModel.get('id_atendente'),
                id_contratoregra: vendaModel.get('id_contratoregra'),
                id_vendedor: vendaModel.get('id_vendedor'),
                id_nucleotelemarketing: vendaModel.get('id_nucleotelemarketing'),
                id_turma: vendaModel.get('id_turma'),
                dt_ingresso: vendaModel.get('dt_ingresso'),
                id_tiposelecao: vendaModel.get('id_tiposelecao')
            });

            pagamentoModel.set({
                "id_meiopagamentoentrada": vendaModel.get("id_meiopagamentoentrada"),
                "dt_vencimentoentrada": vendaModel.get("dt_vencimentoentrada"),
                "id_meiopagamentoparcelas": vendaModel.get("id_meiopagamentoparcelas"),
                "nu_vencimentoparcelas": vendaModel.get("nu_vencimentoparcelas"),
                "nu_mesesintervalovencimento": vendaModel.get("nu_mesesintervalovencimento"),
                "id_responsavelfinanceiro": vendaModel.get("id_responsavelfinanceirograd"),
                "bl_quitadaentrada": vendaModel.get("bl_quitadaentrada")
            });
            that.buscaDados();//busca os dados complementares

        } else {
            // G2S.editModel não encontrado
            //Chama o componente de autocomplete
            autocomplete.main({
                model: VwPesquisarPessoa,
                element: this.ui.autoComplete
            }, function () {
                pessoaFixada = autocomplete.getFixModel();

                aproveitamentoModel.set({
                    id_usuariointeressado: pessoaFixada.id_usuario,
                    st_nomeinteressado: pessoaFixada.st_nomecompleto,
                    st_emailinteressado: pessoaFixada.st_email,
                    st_telefoneinteressado: pessoaFixada.st_telefone
                });

                vendaModel.set({id_usuario: pessoaFixada.id});
                that.setResponsavel(autocomplete.getFixModel());
                abreNegociacao();

                //Oculta o botão de incluir nova pessoa
                that.$el.find('#btn-adicionar-pessoa').css({
                    'display': 'none'
                });
            }, function () {
                fechaNegociacao();
            });

            if (!autocomplete.getFixModel()) {
                that.ui.contentBoxVendas.hide('fast');
            }
            //Exibe o botão de incluir nova pessoa caso não esteja editando
            that.ui.btnAdicionarPessoa.css({
                'display': 'inline'
            });
        }
        loaded();
        return this;
    },
    ocultarFormulario: function () {
        //this.adicionarPessoaRegion.destroy();
        var adicionaPessoa = $('#container-adicionar-pessoa');

        adicionaPessoa.empty().css({
            'display': 'none'
        });

        $('#btn-ocultar-form').css({
            'display': 'none'
        });
    },
    adicionarPessoa: function () {
        $('#container-adicionar-pessoa').css({
            'display': 'inline',
            'border': '1px solid #767676',
            'margin-top': '20px',
            'margin-right': '0px'
        });

        $('#btn-ocultar-form').css({
            'display': 'inline'
        });

        var viewFormulario = new ViewFormulario({
            model: modelFormulario,
            el: '#container-adicionar-pessoa'
        });
        viewFormulario.render();
    },
    buscaDados: function () {
        var that = this;
        var autocomplete = this.autoComplete;

        //Limpa a div do autocomplete para não escrever o componente 2 vezes
        that.ui.autoComplete.empty();

        //Busca a pessoa
        var vwPessoa = new VwPesquisarPessoa();
        vwPessoa.url = '/pessoa/get-vw-pesquisar-pessoa-by-id/id/' + vendaModel.get('id_usuario');
        vwPessoa.fetch({
            beforeSend: loading,
            complete: function () {
                loaded();
            },
            error: function (model, response) {
                loaded();
                $.pnotify({
                    type: 'error',
                    title: 'Erro!',
                    text: 'Houve um erro ao consultar dados da pessoa. ' + response.responseText
                });
                fechaNegociacao();
            },
            success: function () {
                //Transforma o resultado do fetch em objeto Json
                var pessoaFix = vwPessoa.toJSON();
                pessoaFix.id = pessoaFix.id_usuario; //Cria um novo atributo no objeto
                autocomplete.clearFixModel();
                autocomplete.modelRender = pessoaFix; //seta no modelRenderer do autocomplete
                autocomplete.setFixModel();//e define o registro fixado
                pessoaFixada = autocomplete.getFixModel();//recupera os dados da pessoa fixada
                that.setResponsavel(autocomplete.getFixModel());
                //Cria o AutoComplete
                autocomplete.main({
                    model: VwPesquisarPessoa,
                    element: that.ui.autoComplete
                }, abreNegociacao, fechaNegociacao);
                $("#btnClearAutocomplete").attr('disabled', 'disabled');
                $("#btn-adicionar-pessoa").attr('disabled', 'disabled');
                /* $("#btn-aproveitamento").removeAttr('disabled');*/
                viewBotoes.ui.btn_aproveitamento.removeAttr('disabled');
                //AutoComplete, recupera dados para setar pessoa como responsavel

                //Seta o nome do usuário na model de aproveitamento
                aproveitamentoModel.set({
                    id_usuariointeressado: pessoaFix.id_usuario,
                    st_nomeinteressado: pessoaFix.st_nomecompleto,
                    st_emailinteressado: pessoaFix.st_email,
                    st_telefoneinteressado: pessoaFix.st_telefone
                })

            }
        });
    },
    setResponsavel: function (pessoa) {
        //Instancia a model de responsavel financeiro e seta os valores
        var modeloResponsavel = new ResponsavelFinanceiroModel({
            id: pessoa.id,
            st_cpf: pessoa.st_cpf,
            st_nomecompleto: pessoa.st_nomecompleto
        });
        //atribui a model na collection de responsaveis financeiros
        collectionResponsavelFinanceiro.add(modeloResponsavel);
    }
});

/**********************************************************************************************
 * ******************************      ITEMSVIEW     ****************************************
 **********************************************************************************************/

/**
 * View que vai conter os botões e suas ações.
 */
var ViewBotoes = Marionette.ItemView.extend({
    template: '#template-botoes',
    tagName: 'fieldset',
    onRender: function () {
        var that = this;

        var id_venda = vendaModel.get('id_venda');
        var id_situacao = vendaModel.get('id_situacao');
        var id_evolucao = vendaModel.get('id_evolucao');

        // Habilitar o botao "Solicitar Aproveitamento" caso possua id_venda
        if (id_venda && pessoaFixada) {
            this.ui.btn_aproveitamento.prop('disabled', false);
        }

        // Habilitar o botao "Ficha de Matrícula" caso a Evolução = Confirmada
        if (id_venda && +id_evolucao === EVOLUCAO.TB_VENDA.CONFIRMADA) {
            this.ui.btn_ficha.prop('disabled', false);
        }

        // Habilitar o botao "Confirmar Recebimento" caso a entrada já tenha sido quitada.
        if ((vendaModel.get('bl_quitadaentrada') || vendaModel.get('dt_prevquitado'))
            && +id_situacao !== SITUACAO.TB_VENDA.ATIVO && +id_evolucao !== EVOLUCAO.TB_VENDA.CONFIRMADA) {
            this.ui.btn_confirmar.prop('disabled', false);
        }

        // Habilitar o botao "Concluir Negociacao" caso
        // Situacao = "Pendente" e Evolucao = "Aguardando Negociacao"
        if (id_venda
            && +id_situacao === SITUACAO.TB_VENDA.PENDENTE
            && +id_evolucao === EVOLUCAO.TB_VENDA.AGUARDANDO_NEGOCIACAO) {
            this.ui.btn_concluir.prop('disabled', false);
        }

        // Habilitar o botao "Salvar" caso
        // Evolucao != "Aguardando Recebimento"
        if (this.podeSalvar()) {
            this.ui.btn_salvar.prop('disabled', false);
        }

        // Caso a venda ja tenha sido confirmada, desabilitar os campos que não serão possíveis editar.
        if (this.vendaConfirmada()) {
            // Turma
            viewCursosInformacoes.ui.selectTurma.prop('disabled', true);
            // Processo Seletivo
            viewCursosInformacoes.ui.selectProcessoSelecao.prop('disabled', true);
            // Campanha Comercial
            viewContratoInformacoes.ui.selectCampanhaComercial.prop('disabled', true);
            // Campanha Pontualidade
            viewMensalidadeInformacoes.ui.selectCampanhaPontualidade.prop('disabled', true);
            // Atendente
            viewAtendenteInformacoes.ui.selectAtendente.prop('disabled', true);
            // Regra Contrato
            viewAtendenteInformacoes.ui.selectContratoRegra.prop('disabled', true);
            // Vencimento das parcelas
            viewPagamentoInformacoes.ui.vencimentoParcela.prop('disabled', true);
            // Intervalo vencimento da segunda parcela
            viewPagamentoInformacoes.ui.intervaloVencimento.prop('disabled', true);
            // Responsavel Financeiro
            viewPagamentoInformacoes.ui.selectResponsavelFinanceiro.prop('disabled', true);
        }

        // Verifica permissao para o Contrato e caso a Evolução = Confirmada
        var nu_viascontrato = vendaModel.get('nu_viascontrato') || 0;
        if (id_venda && +id_evolucao === EVOLUCAO.TB_VENDA.CONFIRMADA) {
            VerificarPerfilPermissao(28, 627, true).done(function (response) {
                if ((response && response.length) || !nu_viascontrato) {
                    that.ui.btn_contrato.prop('disabled', false);
                }
            });
        }

        // Se ja tiver Solicitação de Aproveitamento, alterar o texto do botao
        if (vendaModel.get('id_ocorrenciaaproveitamento')) {
            this.ui.btn_aproveitamento.text('Aguardando Aprovação do Coordenador');
        }
    },
    ui: {
        btn_salvar: '#btn-salvar',
        btn_confirmar: '#btn-confirmar',
        btn_aproveitamento: '#btn-aproveitamento',
        btn_concluir: '#btn-concluir',
        btn_ficha: '#btn-ficha',
        btn_contrato: '#btn-contrato'
    },
    events: {
        'click @ui.btn_aproveitamento': 'showModalSolicitarAproveitamento',
        'click @ui.btn_salvar': 'save',
        'click @ui.btn_confirmar': 'confirmarRecebimento',
        'click @ui.btn_concluir': 'concluirNegociacao',
        'click @ui.btn_ficha': 'abrirFicha',
        'click @ui.btn_contrato': 'abrirContrato'
    },
    /**
     * Este metodo salva toda a venda, incluindo os lancamentos e as disciplinas da grade
     */
    save: function () {
        if (viewBotoes.podeSalvar()) {
            return this.salvarVenda({
                lancamentos: vwVendaLancamentoCollection.toJSON(),
                disciplinas: collectionDisciplinasSelecionadas.toJSON()
            });
        } else {
            $.pnotify({
                type: 'warning',
                title: 'Aviso',
                text: 'Não é possível alterar a venda após a Negociação ter sido concluída.'
            });
            return false;
        }
    },
    /**
     * Este metodo salva a venda, permitindo escolher os parametros
     */
    salvarVenda: function (data) {
        if (validaVenda()) {
            //Preenche a modelo vendaModel com as informações para serem salvas
            vendaModel.set({
                dt_ingresso: secModel.get('dt_ingresso'),
                id_produto: mainModel.get('id_produto'),
                id_campanhacomercial: conModel.get('id_campanhacomercial') ? conModel.get('id_campanhacomercial') : '',
                nu_parcelas: vwVendaLancamentoCollection.length,
                nu_creditos: conModel.get('nu_creditos'),
                nu_descontovalor: conModel.get('nu_desconto'),
                nu_valorbruto: conModel.get('nu_valorcontrato'),
                nu_valorliquido: conModel.get('nu_valornegociacao'),
                id_campanhapontualidade: mensModel.get('id_campanhapontualidade') ? mensModel.get('id_campanhapontualidade') : '',
                id_turma: secModel.get('id_turma'),
                id_tiposelecao: secModel.get('id_tiposelecao'),
                id_atendente: secModel.get('id_atendente'),
                id_contratoregra: secModel.get('id_contratoregra'),
                id_vendedor: secModel.get('id_vendedor'),
                id_nucleotelemarketing: secModel.get('id_nucleotelemarketing')
            });

            // Tratar o parametro data
            data = $.extend({
                pessoa: pessoaFixada,
                venda: vendaModel.toJSON()
            }, data);

            return $.ajax({
                dataType: 'json',
                type: 'post',
                url: '/venda-graduacao/salvar-venda',
                data: data,
                beforeSend: loading,
                success: function (data) {
                    $.pnotify({
                        type: data.type,
                        title: data.title,
                        text: data.text
                    });

                    vendaModel.set(data.venda);

                    G2S.router.navigate('#' + G2S.funcionalidadeAtual.get('classeFlexBotao') + '/editar/' + vendaModel.get('id_venda'), {trigger: false});

                    var model = vwVendaLancamentoCollection.findWhere({bl_quitado: false, bl_entrada: true});
                    if (model) {
                        model.set('bl_botaomostrar', 1);
                    }
                    viewCursosInformacoes.completaLancamentos();
                },
                complete: function () {
                    loaded();
                    viewBotoes.render();
                }
            })
        }

        return false;
    },
    showModalSolicitarAproveitamento: function () {
        modalAproveitamento = new LayoutAproveitamentoSolicitacaoView();
        telaVendas.modalSolicitacao.show(modalAproveitamento);
    },
    concluirNegociacao: function () {
        if (viewBotoes.podeSalvar() && validaVenda()) {
            // Salvar Situação = "Pendente" e Evolução = "Aguardando Recebimento"
            vendaModel.set({
                id_situacao: SITUACAO.TB_VENDA.PENDENTE,
                id_evolucao: EVOLUCAO.TB_VENDA.AGUARDANDO_RECEBIMENTO
            });
            this.salvarVenda();
        } else {
            $.pnotify({
                type: 'warning',
                title: 'Aviso',
                text: 'Não é possível Concluir a Negociação mais de uma vez.'
            });
            return false;
        }
    },
    confirmarRecebimento: function () {

        var error = null;

        if (this.vendaConfirmada()) {
            error = 'Não é possível Confirmar o recebimento mais de uma vez.';
        } else if (!vendaModel.get('bl_quitadaentrada') && !vendaModel.get('dt_prevquitado')) {
            error = 'A entrada deve ser quitada parar confirmar o recebimento.';
        }

        if (error) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção',
                text: error
            });

            return false;
        }
        if (validaVenda()) {
            $.ajax({
                dataType: 'json',
                type: 'post',
                url: '/venda-graduacao/confirmar-matricula',
                data: {
                    id_venda: vendaModel.get('id_venda')
                },
                beforeSend: loading,
                success: function (response) {
                    if (response && response.type && response.type == 'success') {
                        vendaModel.set('id_evolucao', EVOLUCAO.TB_VENDA.CONFIRMADA);
                    }
                },
                complete: function (response) {
                    $.pnotify(response.responseJSON);
                    viewBotoes.render();
                    loaded();
                }
            })
        }


    },
    // Função útil para poder habilitar/desabilitar campos
    podeSalvar: function () {
        var id_situacao = vendaModel.get('id_situacao');
        var id_evolucao = vendaModel.get('id_evolucao');

        return (!id_situacao || +id_situacao === SITUACAO.TB_VENDA.PENDENTE)
            && (!id_evolucao || +id_evolucao === EVOLUCAO.TB_VENDA.AGUARDANDO_NEGOCIACAO);
    },
    // Funcao para verificar se a venda ja foi confirmada
    vendaConfirmada: function () {
        return +vendaModel.get('id_evolucao') === EVOLUCAO.TB_VENDA.CONFIRMADA;
    },
    abrirFicha: function () {
        abrirPopup('/ficha-matricula/?id_venda=' + vendaModel.get('id_venda'));
    },
    abrirContrato: function () {
        var id_venda = vendaModel.get('id_venda');
        var id_situacao = vendaModel.get('id_situacao');
        var id_evolucao = vendaModel.get('id_evolucao');

        // Evolução = Confirmada
        if (id_venda && (+id_evolucao === EVOLUCAO.TB_VENDA.CONFIRMADA || this.printContrato)) {

            var fileName = "Contrato - " + pessoaFixada.st_nomecompleto;
            var id_textositema = 141;
            var nu_viascontrato = vendaModel.get('nu_viascontrato') || 0;

            var id_contrato,
                modelContrato;

            var VwGerarContratoCollection = Backbone.Collection.extend({
                model: VwGerarContrato,
                url: '/api/vw-gerar-contrato/'
            });

            // Se for 2a via ou mais, pedir Justificativa
            if (nu_viascontrato) {
                var model = this.model;
                var viewModalJustificativa = new ViewModalJustificativa({
                    model: model
                });
                telaVendas.modal.show(viewModalJustificativa);
            } else {
                $.ajax({
                    dataType: 'json',
                    type: 'post',
                    url: '/venda/contabiliza-via-contrato',
                    data: {
                        id_venda: id_venda,
                        nu_viascontrato: nu_viascontrato
                    },
                    beforeSend: loading,
                    success: function (data) {
                        vendaModel.set('nu_viascontrato', data.nu_viascontrato);

                        $.pnotify({
                            title: 'Sucesso',
                            text: 'Contrato gerado e contabilizado com sucesso!',
                            type: 'success'
                        });

                        var contratoCollection = new VwGerarContratoCollection();
                        contratoCollection.fetch({
                            data: {
                                id_venda: id_venda
                            },
                            beforeSend: loading,
                            success: function () {
                                if (contratoCollection.length) {
                                    modelContrato = contratoCollection.at(0);
                                }
                            },
                            complete: function () {
                                loaded();

                                if (modelContrato) {
                                    id_contrato = modelContrato.get('id_contrato');
                                    id_textositema = modelContrato.get('id_contratomodelo') || id_textositema;
                                    abrirPopup('/textos/geracao/?filename=' + fileName + '&id_textosistema=' + id_textositema + '&arrParams[id_contrato]=' + id_contrato + '&type=html');
                                } else {
                                    $.pnotify({
                                        title: 'Atenção',
                                        text: 'Contrato não encontrato, verifique as regras de contrato.',
                                        type: 'warning'
                                    });
                                }
                            }
                        });
                    },
                    error: function () {
                        $.pnotify({
                            title: 'Erro',
                            text: 'Erro ao tentar contabilizar via do contrato!',
                            type: 'error'
                        });
                    },
                    complete: function () {
                        viewBotoes.render();
                        loaded();
                    }
                });
            }
        } else {
            $.pnotify({
                title: 'Atenção',
                text: 'Contrato indisponível no momento, verifique se a venda esta definida e a evolução da mesma é "Confirmada".',
                type: 'warning'
            });
        }
    }
});

/**
 * View Que vai conter as informações de curso, turma e processo seletivo, e suas ações.
 */
var ViewCursosInformacoes = Marionette.ItemView.extend({
    template: '#template-cursos-informacoes',
    tagName: 'fieldset',
    model: mainModel,
    onRender: function () {
        this.populaDataIngresso();
        this.populaSelectCurso();
        this.populaProcessoSeletivo();
        if (vendaModel.get('id_venda')) {
            this.ui.selectCurso.attr('disabled', 'disabled');
        }
    },
    ui: {
        selectTurma: 'select[name="id_turma"]',
        selectCurso: 'select[name="id_curso"]',
        selectProcessoSelecao: 'select[name="id_processoseletivo"]',
        digitDataIngresso: 'input[name="dt_ingresso"]',
        divTurma: '#divTurma',
        divProcessoSeletivo: '#divProcessoSeletivo'
    },
    events: {
        'change @ui.selectCurso': 'disparaSelectCurso',
        'change @ui.selectTurma': 'disparaSelectTurma',
        'change @ui.digitDataIngresso': 'disparaDataIngresso',
        'change @ui.selectProcessoSelecao': 'disparaSelectPSelecao'
    },
    disparaSelectCurso: function () {
        mainModel.set({id_produto: this.ui.selectCurso.val()});
        this.populaSelectTurma();
        this.buscaInformacoes();
    },
    disparaSelectTurma: function () {
        secModel.set({
            id_turma: this.ui.selectTurma.val()
        });

        if (this.ui.selectTurma.val()) {
            this.buscaInformacoes();
        }
    },
    disparaDataIngresso: function () {
        secModel.set({dt_ingresso: this.ui.digitDataIngresso.val()});
    },
    disparaSelectPSelecao: function () {
        secModel.set({
            id_tiposelecao: this.ui.selectProcessoSelecao.val()
        });
    },
    populaProcessoSeletivo: function () {
        var that = this;
        var selecaoCollection = new processoSeletivoCollection;
        selecaoCollection.url = '/api/tipo-selecao/';
        selecaoCollection.fetch({
            beforeSend: loading,
            complete: function () {
                loaded();
            },
            error: function () {
                $.pnotify({
                    type: 'warning',
                    title: 'Erro!',
                    text: 'Erro ao carregar processos seletivos.'
                });
            },
            success: function () {
                new SelectView({
                    el: that.ui.selectProcessoSelecao,
                    collection: selecaoCollection,
                    childViewOptions: {
                        optionLabel: 'st_tiposelecao',
                        optionValue: 'id_tiposelecao',
                        optionSelected: secModel.get('id_tiposelecao') ? secModel.get('id_tiposelecao') : null
                    }
                }).render();
            }
        });
    },
    populaSelectTurma: function () {
        var that = this;
        var id_produto = this.ui.selectCurso.val();
        var turmaColection = new turmaCollection;

        if (id_produto) {
            turmaColection.url = '/turma/get-turmas-by-produto/id_produto/' + id_produto;
            turmaColection.fetch({
                data: {
                    id_turma: vendaModel.get('id_turma')
                },
                error: function () {
                    $.pnotify({
                        type: 'warning',
                        title: 'Erro!',
                        text: 'Erro ao carregar turmas.'
                    });
                },
                success: function () {
                    new SelectView({
                        el: that.ui.selectTurma,
                        collection: turmaColection,
                        childViewOptions: {
                            optionLabel: 'st_turma',
                            optionValue: 'id_turma',
                            optionSelected: secModel.get('id_turma') ? secModel.get('id_turma') : null
                        },
                        clearView: true
                    }).render();
                }
            });
        }
    },
    populaDataIngresso: function () {
        if (secModel.get('dt_ingresso')) {
            var dtIngresso = new Date(secModel.get('dt_ingresso'));
            this.ui.digitDataIngresso.val(dateToPtBr(dtIngresso));
        }
    },
    populaSelectCurso: function () {
        var that = this;
        var cursoColection = new cursoCollection();

        cursoColection.fetch({
            url: "/produto/pesquisar-produto-compartilhado",
            data: {
                id_situacao: SITUACAO.TB_PRODUTO.ATIVO,
                id_tipoproduto: TIPO_PRODUTO.PROJETO_PEDAGOGICO,
                id_selected: mainModel.get("id_produto")
            },
            beforeSend: loading,
            complete: function () {
                if (mainModel.get("id_produto")) {
                    that.buscaInformacoes();
                }

                that.populaSelectTurma();
                loaded();
            },
            error: function () {
                $.pnotify({
                    type: 'warning',
                    title: 'Erro!',
                    text: 'Erro ao carregar cursos.'
                });
            },
            success: function () {
                new SelectView({
                    el: that.ui.selectCurso,
                    collection: cursoColection,
                    childViewOptions: {
                        optionLabel: 'st_produto',
                        optionValue: 'id_produto',
                        optionSelected: mainModel.get('id_produto') ? mainModel.get('id_produto') : null
                    }
                }).render();

                if (!mainModel.get('id_produto')) {
                    that.ui.selectCurso.removeAttr("disabled");
                }
            }
        });
    },
    buscaInformacoes: function () {
        var that = this;
        $.ajax({
            dataType: 'json',
            //type: 'post',
            url: '/venda-graduacao/retorna-informacoes-valores/',
            data: {
                id_venda: vendaModel.get('id_venda'),
                id_produto: this.ui.selectCurso.val() ? this.ui.selectCurso.val() : mainModel.get('id_produto'),
                id_usuario: vendaModel.get('id_usuario'),
                id_turma: secModel.get('id_turma'),
                id_campanhacomercial: vendaModel.get('id_campanhacomercial') ? vendaModel.get('id_campanhacomercial') : null,
                id_campanhapontualidade: vendaModel.get('id_campanhapontualidade') ? vendaModel.get('id_campanhapontualidade') : null
            },
            beforeSend: loading,
            success: function (data) {
                conModel.set(data);
                mensModel.set(data);

                collectionDisciplinasSelecionadas.set(data.disciplinas);

                if (!vendaModel.get('dt_ultimaoferta') && data.dt_ultimaoferta) {
                    vendaModel.set('dt_ultimaoferta', data.dt_ultimaoferta);
                }

                that.completaLancamentos();
            },
            error: function (response) {
                that.ui.selectCurso.val("");

                $.pnotify(getMensageiro(response) || {
                    type: "error",
                    title: "Erro",
                    text: "Houve um erro ao realizar o procedimento."
                });
            },
            complete: loaded
        })
    },
    completaLancamentos: function () {
        var that = this;
        if (vendaModel.get('id_venda') == '') { // não tem id_venda, não existe lancamento
            that.geraLancamentoGraducao();
        } else {
            that.retornaLancamentos();
        }
    },
    geraLancamentoGraducao: function () {
        geraLancamentosGraducao();
    },
    retornaLancamentos: function () {
        //Faz o fetch nos lançamentos da venda
        vwVendaLancamentoCollection.fetch({
            data: {
                id_venda: vendaModel.get('id_venda')
            },
            async: false,
            success: function () {
                //procura o primeiro que não foi quitado, e marca o botao como habilitado (apenas para entrada)
                var model = vwVendaLancamentoCollection.findWhere({bl_quitado: false, bl_entrada: true});
                if (model) {
                    if (model.get('dt_prevquitado')) {
                        model.set('bl_quitado', true);
                    } else {
                        model.set('bl_botaomostrar', 1);
                    }
                }
            }
        });
    }
});

/**
 * View que vai conter as informações da venda, como dados financeiros, campanha comercial, e acesso a grade de materias.
 */
var ViewContratoInformacoes = Marionette.ItemView.extend({
    template: '#template-contrato-informacoes',
    tagName: 'fieldset',
    model: conModel,
    conModel: conModel,
    mensModel: mensModel,
    modelEvents: {
        'change': function () {
            this.render();
        }
    },
    initialize: function () {
        campanhaComercialCollectionObj = new CampanhaPontualidadeCollection();
        campanhaComercialCollectionObj.fetch({
            data: {
                id_tipocampanha: 2,
                id_tipodesconto: 2,
                id_campanhaselecionada: conModel.get('id_campanhacomercial'),
                vigencia: true
            }
        });
    },
    onRender: function () {

        if (viewBotoes.podeSalvar()) {
            this.ui.btn_grade.prop('disabled', false);
        }

        this.populaSelectCampanhaComercial();
    },
    ui: {
        selectCampanhaComercial: 'select[name="id_campanhacomercial"]',
        btn_grade: '#btn_grade'
    },
    events: {
        'change @ui.selectCampanhaComercial': 'calcularCampanhas',
        'click @ui.btn_grade': 'renderizaGrade'
    },
    calcularCampanhas: function () {
        $.ajax({
            dataType: 'json',
            type: 'post',
            url: '/venda-graduacao/calcula-campanhas',
            data: {
                id_venda: vendaModel.get('id_venda'),
                id_campanhacomercial: this.ui.selectCampanhaComercial.val(),
                id_campanhapontualidade: viewMensalidadeInformacoes.ui.selectCampanhaPontualidade.val(),
                nu_valorcontrato: conModel.get('nu_valorcontrato')
            },
            beforeSend: loading,
            success: function (data) {
                conModel.set(data);
                mensModel.set(data);

                // Gerar lançamentos da graduação se valor negociado for maior que zero
                if (data.nu_valornegociacao > 0) {
                    habilitaCamposPagamento();
                    geraLancamentosGraducao();
                } else {
                    //apaga as parcelas que foram geradas e não foram quitadas, desabilita os campos de pagamento
                    vwVendaLancamentoCollection.each(function (model) {
                        if (model.get('bl_quitado') !== true) {
                            vwVendaLancamentoCollection.remove(model);
                        }
                    });
                    desabilitaCamposPagamento();
                }
            },
            complete: loaded
        })
    },
    populaSelectCampanhaComercial: function () {
        new SelectView({
            el: this.ui.selectCampanhaComercial,
            collection: campanhaComercialCollectionObj,
            childViewOptions: {
                optionLabel: 'st_campanhacomercial',
                optionValue: 'id_campanhacomercial',
                optionSelected: conModel.get('id_campanhacomercial') || null
            },
            clearView: true
        }).render();

        if (viewBotoes.vendaConfirmada()) {
            this.ui.selectCampanhaComercial.prop('disabled', true);
        }
    },
    renderizaGrade: function () {
        var id_produto = viewCursosInformacoes.ui.selectCurso.val();
        var id_turma = viewCursosInformacoes.ui.selectTurma.val();
        var id_usuario = vendaModel.get('id_usuario');

        var error;
        if (!id_produto) {
            error = 'Selecione um Curso para poder visualizar a grade.';
        } else if (!id_turma) {
            error = 'Selecione uma Turma para poder visualizar a grade.';
        } else if (!id_usuario) {
            error = 'Selecione um Usuário para poder visualizar a grade.';
        } else {
            telaVendas.informacoesRegion.currentView.mostraTelaGrid();
            return true;
        }

        if (error) {
            $.pnotify({
                type: 'warning',
                title: 'Aviso',
                text: error
            });
        }
    }
});

/**
 * View que vai conter as informações como valor de entrada, mensalidade e de pontualidade, tanto como as ações
 * de campanha pontualidade.10073153
 */
var ViewMensalidadeInformacoes = Marionette.ItemView.extend({
    template: '#template-mensalidade-informacoes',
    tagName: 'fieldset',
    model: mensModel,
    modelEvents: {
        'change': function () {
            this.render();
        }
    },
    initialize: function () {
        campanhaPontualidadeCollectionObj = new CampanhaPontualidadeCollection();
        campanhaPontualidadeCollectionObj.fetch({
            data: {
                id_tipocampanha: 3,
                id_tipodesconto: 2,
                id_campanhaselecionada: mensModel.get('id_campanhapontualidade'),
                vigencia: true
            }
        });
    },
    onRender: function () {
        this.populaSelectCampanhaPontualidade();
    },
    ui: {
        selectCampanhaPontualidade: 'select[name="id_campanha_pontualidade"]'
    },
    events: {
        'change @ui.selectCampanhaPontualidade': 'calcularCampanhas'
    },
    calcularCampanhas: function () {
        viewContratoInformacoes.calcularCampanhas();
    },
    populaSelectCampanhaPontualidade: function () {
        new SelectView({
            el: this.ui.selectCampanhaPontualidade,
            collection: campanhaPontualidadeCollectionObj,
            childViewOptions: {
                optionLabel: 'st_campanhacomercial',
                optionValue: 'id_campanhacomercial',
                optionSelected: mensModel.get('id_campanhapontualidade') ? mensModel.get('id_campanhapontualidade') : null
            },
            clearView: true
        }).render();

        if (viewBotoes.vendaConfirmada()) {
            this.ui.selectCampanhaPontualidade.prop('disabled', true);
        }
    }
});

/**
 * View que vai conter as informações de atendente e regra de contrato, tanto como suas ações.
 */
var ViewAtendenteInformacoes = Marionette.ItemView.extend({
    template: '#template-atendente-informacoes',
    tagName: 'fieldset',
    model: mainModel,
    initialize: function () {
        this.permissao = VerificarPerfilPermissao(33, 760, false);
    },
    onBeforeRender: function () {
        this.model.set('st_afiliado', vendaModel.get('st_afiliado'));
    },
    onRender: function () {
        var that = this;

        this.populaSelectAtendente();
        this.populaSelectContratoRegra();
        this.populaSelectVendedor();
        this.populaSelectNucleo();

        // Habilitar os combos se a venda não estiver pendente
        if (viewBotoes.podeSalvar()) {
            // Verificar permissão de alterar o Núcleo de Telemarketing
            VerificarPerfilPermissao(40, 760, true).done(function (response) {
                if (response && response.length) {
                    that.ui.selectNucleo.prop('disabled', false);
                }
            });

            // Verificar permissão de alterar o Vendedor
            VerificarPerfilPermissao(41, 760, true).done(function (response) {
                if (response && response.length) {
                    that.ui.selectVendedor.prop('disabled', false);
                }
            });
        }
    },
    ui: {
        selectAtendente: 'select[name="id_atendente"]',
        selectContratoRegra: 'select[name="id_regra_contrato"]',
        selectVendedor: 'select[name="id_vendedor"]',
        selectNucleo: 'select[name="id_nucleotelemarketing"]',
        divAtendente: '#divAtendente',
        divRegraContrato: '#divRegraContrato'
    },
    events: {
        'change select': 'updateModel'
    },
    updateModel: function () {
        secModel.set({
            id_atendente: this.ui.selectAtendente.val(),
            id_contratoregra: this.ui.selectContratoRegra.val(),
            id_vendedor: this.ui.selectVendedor.val(),
            id_nucleotelemarketing: this.ui.selectNucleo.val()
        })
    },
    populaSelectContratoRegra: function () {
        var that = this;
        var contratoRegraCol = new RegraContratoCollection;
        contratoRegraCol.fetch({
            beforeSend: loading,
            complete: function () {
                loaded();
            },
            error: function () {
                $.pnotify({
                    type: 'warning',
                    title: 'Erro!',
                    text: 'Erro ao carregar processos seletivos.'
                });
            },
            success: function () {
                new SelectView({
                    el: that.ui.selectContratoRegra,
                    collection: contratoRegraCol,
                    childViewOptions: {
                        optionLabel: 'st_contratoregra',
                        optionValue: 'id_contratoregra',
                        optionSelected: secModel.get('id_contratoregra') ? secModel.get('id_contratoregra') : null
                    }
                }).render();
            }
        });

    },
    populaSelectAtendente: function () {
        var that = this;
        var atendenteCol = new NucleoFuncionariotmCollection;
        atendenteCol.fetch({
            beforeSend: loading,
            success: function () {
                new SelectView({
                    el: that.ui.selectAtendente,
                    collection: atendenteCol,
                    childViewOptions: {
                        optionLabel: 'st_nomecompleto',
                        optionValue: 'id_usuario',
                        optionSelected: secModel.get('id_atendente') ? secModel.get('id_atendente') : G2S.loggedUser.get('id_usuario')
                    }
                }).render();
            },
            complete: function () {
                loaded();
            }
        });

        this.permissaoSelectAtendente(this.permissao);

    },
    populaSelectVendedor: function () {
        ComboboxView({
            el: this.ui.selectVendedor,
            url: '/api/vendedor?bl_ativo=1&id_entidade=' + G2S.loggedUser.get('id_entidade'),
            optionLabel: 'st_vendedor',
            optionSelectedId: vendaModel.get('id_vendedor'),
            emptyOption: '[Selecione]',
            model: Backbone.Model.extend({
                idAttribute: 'id_vendedor',
                defaults: {
                    id_vendedor: null,
                    st_vendedor: null
                }
            })
        });
    },
    populaSelectNucleo: function () {
        ComboboxView({
            el: this.ui.selectNucleo,
            url: '/api/nucleo-telemarketing?bl_ativo=1&id_entidade=' + G2S.loggedUser.get('id_entidade'),
            optionLabel: 'st_nucleotelemarketing',
            optionSelectedId: vendaModel.get('id_nucleotelemarketing'),
            emptyOption: '[Selecione]',
            model: Backbone.Model.extend({
                idAttribute: 'id_nucleotelemarketing',
                defaults: {
                    id_nucleotelemarketing: null,
                    st_nucleotelemarketing: null
                }
            })
        });
    },
    permissaoSelectAtendente: function (permissao) {
        if (permissao.length) {
            this.ui.selectAtendente.prop('disabled', false);
        } else {
            this.ui.selectAtendente.prop('disabled', true);
        }
    }
});

/**
 * View para lista de lançamentos graduação sem resultado
 * @type {ItemView}
 */
var LancamentosGraduacaoEmptyView = Marionette.ItemView.extend({
    template: "#template-empty-lancamento-graduacao",
    tagName: 'tr'
});


/**
 * ItemView para Itens de lançamentos da graduação
 * @type {ItemView}
 */
var ViewItemLamcamentoGraduacao = Marionette.ItemView.extend({
    template: "#template-item-lancamento-graduacao",
    tagName: 'tr',
    ui: {
        'btn_opcoes_lancamento_gra': 'button[name="btn-opcoes-lancamento-gra"]'
    },
    modelEvents: {
        'change': 'render'
    },
    initialize: function () {
        this.model.set('bl_botaomostrar', 0);
    },
    onBeforeRender: function () {
        var nu_valor = parseFloat(this.model.get('nu_valor'));
        this.model.set('nu_valormostrar', formatValueToShow(nu_valor.toFixed(2)));

        var nuOrdem = this.model.collection.models.indexOf(this.model) + 1;
        if (nuOrdem <= 0) {
            nuOrdem = 1;
        }
        this.model.set('nu_ordem', nuOrdem);
    },
    events: {
        'click @ui.btn_opcoes_lancamento_gra': 'quitarLancamento'
    },
    quitarLancamento: function () {
        var that = this;
        if (!that.ui.btn_opcoes_lancamento_gra.is(':disabled') && (this.model.get('bl_entrada') && !this.model.get('bl_quitado'))) {
            bootbox.confirm("Confirmar recebimento para este lançamento?", function (result) {
                if (result) {
                    that.ui.btn_opcoes_lancamento_gra.attr('disabled', 'disabled');
                    $.ajax({
                        dataType: 'json',
                        type: 'post',
                        url: '/recebimento/pagar-boleto',
                        data: {
                            id_lancamento: that.model.get('id_lancamento')
                        },
                        beforeSend: loading,
                        success: function (data) {
                            if (data.type == 'success') {
                                that.model.set('bl_quitado', true);
                                that.model.set('bl_botaomostrar', 0);
                                vendaModel.set('bl_quitadaentrada', true);

                                pagamentoModel.set({
                                    bl_quitadaentrada: true,
                                    id_meiopagamentoentrada: that.model.get('id_meiopagamento')
                                });

                                viewPagamentoInformacoes.habilitaCampos();
                            }
                            $.pnotify({
                                title: data.title,
                                text: 'Lançamento quitado com sucesso',
                                type: data.type
                            });
                        },
                        complete: function () {
                            viewBotoes.render();
                            loaded();
                        },
                        error: function (data) {
                            $.pnotify({
                                title: data.title,
                                text: data.text,
                                type: 'error'
                            });
                        }
                    });
                }
            });
        }
    }
});


/**
 * Composite de informações de pagamento
 */
var ViewPagamentoInformacoes = Marionette.CompositeView.extend({
    template: '#template-pagamento-informacoes',
    tagName: 'fieldset',
    childViewContainer: '#lista-lancamentos-venda-graduacao',
    childView: ViewItemLamcamentoGraduacao,
    emptyView: LancamentosGraduacaoEmptyView,
    model: pagamentoModel,
    collection: vwVendaLancamentoCollection,
    onBeforeRender: function () {

    },
    onRender: function () {
        this.populaResponsavelFinanceiro();
        this.habilitaCampos();
    },
    ui: {
        selectPagamentoEntrada: 'select[name="id_meiopagamentoentrada"]',
        vencimentoEntrada: 'input[name="dt_vencimentoentrada"]',
        selectPagamentoParcelas: 'select[name="id_meiopagamentoparcelas"]',
        vencimentoParcela: 'select[name="nu_vencimentoparcelas"]',
        selectResponsavelFinanceiro: 'select[name="id_responsavelfinanceirograd"]',
        intervaloVencimento: '#nu_mesesintervalovencimento',
        buttonProcurarResponsavel: '#btn-procurar-responsavel-financeiro',

        divMeioEntrada: '#divMeioEntrada',
        divMeioParcelas: '#divMeioParcelas',
        divVencimentoEntrada: '#divVencimentoEntrada',
        divVencimentoParcelas: '#divVencimentoParcelas',
        divRespFinanceiroGrad: '#divRespFinanceiroGrad'

    },
    events: {
        'click @ui.buttonProcurarResponsavel': 'showModalResponsavelFinanceiro',
        'change @ui.selectPagamentoEntrada': 'changeMeioPagEntrada',
        'change @ui.selectPagamentoParcelas': 'changeMeioPagParcela',
        'change @ui.vencimentoEntrada': 'refreshCollection',
        'change @ui.vencimentoParcela': 'refreshCollection',
        'change @ui.intervaloVencimento': 'refreshCollection',
        'change @ui.selectResponsavelFinanceiro': 'refreshCollection'

    },
    habilitaCampos: function () {
        if (vendaModel.get('id_venda')) {
            // Desabilitar alteracoes na Entrada caso ela ja tiver sido quitada
            var desabilitar_entrada = false;
            if (pagamentoModel.get('bl_quitadaentrada') || vendaModel.get('dt_prevquitado')) {
                desabilitar_entrada = true;
            }
            this.ui.selectPagamentoEntrada.prop('disabled', desabilitar_entrada);
            this.ui.vencimentoEntrada.prop('disabled', desabilitar_entrada);

            // Desabilitar o Meio de Pagamento das Parcelas caso o pagamento da Entrada for Boleto OU alguma parcela ja tenha sido quitada
            var desabilitar_pagamento_parcelas = pagamentoModel.get('id_meiopagamentoentrada') == PAG_BOLETO || pagamentoModel.get('bl_quitadaparcela');
            this.ui.selectPagamentoParcelas.prop('disabled', desabilitar_pagamento_parcelas);

            // Desabilitar o Vencimento das Parcelas caso o meio de pagamento das Parcelas for Cartao OU alguma parcela ja tenha sido quitada
            var desabilitar_vencimento_parcelas = pagamentoModel.get('id_meiopagamentoparcelas') == PAG_CARTAO || pagamentoModel.get('bl_quitadaparcela');
            this.ui.vencimentoParcela.prop('disabled', desabilitar_vencimento_parcelas);
            this.ui.intervaloVencimento.prop('disabled', desabilitar_vencimento_parcelas);

            if (pagamentoModel.get('id_meiopagamentoparcelas') == PAG_CARTAO) {
                this.ui.vencimentoParcela.val('');
                this.ui.intervaloVencimento.val(1);
            }
        }
    },
    changeMeioPagEntrada: function () {
        var value = this.ui.selectPagamentoEntrada.val();

        //Caso a entrada seja no boleto, as parcelas só podem ser pagas no boleto
        if (value == PAG_BOLETO) {
            this.ui.selectPagamentoParcelas.val(value).prop('disabled', true).trigger('change');
            //this.ui.vencimentoParcela.prop('disabled', false);
        } else {
            this.ui.selectPagamentoParcelas.prop('disabled', false).trigger('change');
            //this.ui.vencimentoParcela.prop('disabled', true);
        }

        geraLancamentosGraducao(COMBO_MEIO_ENTRADA);
    },
    changeMeioPagParcela: function () {
        var value = this.ui.selectPagamentoParcelas.val();

        //Cartão
        if (value == PAG_CARTAO) {
            this.ui.vencimentoParcela.val('').prop('disabled', true);
            this.ui.intervaloVencimento.val(1).prop('disabled', true);
        } else {
            this.ui.vencimentoParcela.prop('disabled', false);
            this.ui.intervaloVencimento.val(1).prop('disabled', false);
        }

        geraLancamentosGraducao(COMBO_MEIO_PARCELA);
    },
    populaResponsavelFinanceiro: function () {
        var that = this;
        var responsavel = new SelectView({
            el: that.ui.selectResponsavelFinanceiro,
            collection: collectionResponsavelFinanceiro,
            childViewOptions: {
                optionLabel: 'st_nomecompleto',
                optionValue: 'id',
                optionSelected: pagamentoModel.get('id_responsavelfinanceiro') ? pagamentoModel.get('id_responsavelfinanceiro') : null
            }
        });
        responsavel.render();
    },
    showModalResponsavelFinanceiro: function () {
        var modalPesquisa = new PesquisaResponsavelItemView();
        telaVendas.modal.show(modalPesquisa);
    },
    refreshCollection: function (e) {
        var ref = $(e.currentTarget).attr('id');

        if (ref === 'nu_vencimentoparcelas' || ref === 'nu_mesesintervalovencimento') {
            geraLancamentosGraducao(INPUT_VENCIMENTO_PARCELA)
        } else if (ref === 'dt_vencimentoentrada') {
            geraLancamentosGraducao(INPUT_VENCIMENTO_ENTRADA);
        } else {
            geraLancamentosGraducao(COMBO_RESPONSAVEL);
        }
    }
});

/**
 * LayoutView para Solicitação de Aproveitamento
 */
var LayoutAproveitamentoSolicitacaoView = Marionette.LayoutView.extend({
    template: '#layout-solicitacao',
    tagName: 'div',
    className: 'modal hide fade modal-large modalSolicitacaoOcorrencia',
    regions: {
        info_gerais: '#wrapper-info-gerais',
        interacoes: '#wrapper-interacoes'
    },
    onShow: function () {
        var that = this;
        this.$el.modal('show');

        aproveitamentoSolicitacaoModal = new AproveitamentoSolicitacaoItemView();
        this.info_gerais.show(aproveitamentoSolicitacaoModal);
        if (vendaModel.get('id_ocorrenciaaproveitamento')) {
            aproveitamentoInteracoes = new AproveitamentoIteracoesCompositeView({collection: new VwOcorrenciaInteracaoCollection});
            that.interacoes.show(aproveitamentoInteracoes);
        }
    },
    ui: {
        btn_gerar: '#btn_gerar',
        btn_voltarcancelar: '#btn_voltarcancelar',
        form_aproveitamento: '#form_aproveitamento'
    },
    events: {
        'click @ui.btn_gerar': 'gerarOcorrencia',
        'change @ui.id_subassuntoco': 'alterarSubassunto'
    },
    gerarOcorrencia: function () {
        var that = this;

        var ocorrencia = {
            'st_ocorrencia': tinyMCE.get('st_ocorrencia').getContent(),
            'st_titulo': 'Aproveitamento de Disciplina',
            'id_venda': vendaModel.get('id_venda'),
            'id_assuntoco': aproveitamentoModel.get('id_assuntoco'),
            'id_atendente': aproveitamentoSolicitacaoModal.ui.id_atendente_solicitacao.val(),
            'id_assuntocopai': vendaModel.get('id_assuntoisencao'),
            'id_categoriaocorrencia': 1,
            'id_evolucao': EVOLUCAO.TB_OCORRENCIA.AGUARDANDO_ATENDIMENTO,
            'id_situacao': SITUACAO.TB_OCORRENCIA.PENDENTE,
            'id_usuariointeressado': aproveitamentoModel.get('id_usuariointeressado'),
            'st_nomeinteressado': aproveitamentoModel.get('st_nomeinteressado'),
            'st_emailinteressado': aproveitamentoModel.get('st_email'),
            'st_telefoneinteressado': aproveitamentoModel.get('st_telefone')
        };

        if (!aproveitamentoSolicitacaoModal.ui.id_subassuntoco.val()) {
            $.pnotify({
                type: 'error',
                title: 'Erro',
                text: 'Por favor, selecione um subassunto para solicitação.'
            });
            return false;
        }

        if (!aproveitamentoSolicitacaoModal.ui.id_atendente_solicitacao.val()) {
            $.pnotify({
                type: 'error',
                title: 'Erro',
                text: 'Por favor, selecione um atendente para solicitação.'
            });
            return false;
        }

        if (tinyMCE.EditorManager.get('st_ocorrencia').getBody().textContent == '') {
            $.pnotify({
                type: 'error',
                title: 'Erro',
                text: 'Por favor, inclua uma descrição para essa ocorrência.'
            });
            return false;
        }

        if (!validaVenda()) {
            // return false;
        }

        this.ui.form_aproveitamento.ajaxSubmit({
            dataType: 'json',
            type: 'post',
            url: '/api/ocorrencia',
            data: {
                graduacao: true,
                ocorrencia: JSON.stringify(ocorrencia)
            },
            beforeSend: loading,
            success: function (data) {

                if (!data || !data.id_ocorrencia) {
                    $.pnotify({
                        type: 'error',
                        title: 'Erro',
                        text: 'Houve um erro ao gerar a ocorrência.'
                    });
                    return false;
                }

                $.pnotify({
                    type: 'success',
                    title: 'Sucesso',
                    text: 'Sucesso ao salvar solicitação.'
                });
                aproveitamentoModel.set(data);
                vendaModel.set({
                    'id_ocorrenciaaproveitamento': aproveitamentoModel.get('id_ocorrencia')
                });
                viewBotoes.salvarVenda();
                aproveitamentoSolicitacaoModal.desabilitarCampos();
                aproveitamentoInteracoes = new AproveitamentoIteracoesCompositeView({collection: new VwOcorrenciaInteracaoCollection});
                that.interacoes.show(aproveitamentoInteracoes);
            },
            complete: loaded
        })
    }
});

var InteracoesItemView = Marionette.ItemView.extend({
    template: '#template-item-tramite-aproveitamento',
    tagName: 'tr',
    model: VwOcorrenciaInteracao,
    ui: {},
    events: {},
    initialize: function () {

    },
    onShow: function () {
        return this;
    }
});

var AproveitamentoIteracoesCompositeView = Marionette.CompositeView.extend({
    template: '#template-modal-interacao-ocorrencia',
    tagName: 'div',
    childView: InteracoesItemView,
    childViewContainer: "tbody",
    ui: {
        formNovaInteracao: '#form-nova-interacao',
        btn_gerarNovaInteracao: '#btn_gerarNovaInteracao',
        st_novotramite: '#st_novotramite',
        arquivoInteracao: '#arquivoInteracao',
        btn_interacao: '.btn_interacao'
    },
    events: {
        'click @ui.btn_interacao': 'toggleNovaInteracao',
        'click @ui.btn_gerarNovaInteracao': 'gerarNovaInteracao'
    },
    initialize: function () {
    },
    onShow: function () {
        return this;
    },
    onRender: function () {
        this.fetchInteracoes();
    },
    fetchInteracoes: function () {
        this.collection.fetch({
            data: {
                params: {
                    id_ocorrencia: vendaModel.get('id_ocorrenciaaproveitamento')
                }
            },
            beforeSend: loading,
            complete: function () {
                loaded();
            }
        });
    },
    toggleNovaInteracao: function () {
        this.ui.btn_interacao.toggleClass('hide');
        this.ui.formNovaInteracao.toggle();
    },
    gerarNovaInteracao: function () {
        var that = this;
        if (this.validaNovaInteracao()) {
            var ocorrencia = {
                id_ocorrencia: vendaModel.get('id_ocorrenciaaproveitamento'),
                bl_visivel: 1,
                id_evolucao: aproveitamentoModel.get('id_evolucao'),
                id_usuariointeressado: aproveitamentoModel.get('id_usuariointeressado'),
                id_entidade: G2S.loggedUser.get('id_entidade')
            };

            this.ui.formNovaInteracao.ajaxSubmit({
                dataType: 'json',
                type: 'post',
                url: '/ocorrencia/nova-interacao',
                data: {
                    graduacao: true,
                    ocorrencia: JSON.stringify(ocorrencia),
                    st_tramite: this.ui.st_novotramite.val()
                },
                beforeSend: loading,
                success: function () {
                    $.pnotify({
                        type: 'success',
                        title: 'Sucesso',
                        text: 'Sucesso ao salvar nova interação.'
                    });
                    that.ui.st_novotramite.val('');
                    that.ui.arquivoInteracao.val('');
                    that.toggleNovaInteracao();

                    that.fetchInteracoes();
                },
                complete: function () {
                    loaded();
                },
                error: function () {
                    $.pnotify({
                        type: 'error',
                        title: 'Erro',
                        text: 'Erro ao cadastrar trâmite.'
                    });
                }
            })
        }
    },
    validaNovaInteracao: function () {
        var flag = true;
        if (!this.ui.st_novotramite.val()) {
            flag = false;
            $.pnotify({
                title: 'Atenção!',
                text: 'Digite a descrição do trâmite.',
                type: 'warning'
            });
            $('#divStNovoTramite').addClass('control-group error');
        } else {
            $('#divStNovoTramite').removeClass('control-group error');
        }
        return flag;
    }
});


/**
 * ItemView para Solicitação de Aproveitamento
 */
var AproveitamentoSolicitacaoItemView = Marionette.ItemView.extend({
    template: '#template-modal-solicitacao-aproveitamento',
    tagName: 'div',
    model: aproveitamentoModel,
    ui: {
        st_ocorrencia: 'textarea[name="st_ocorrencia"]',
        id_subassuntoco: '#id_subassuntoco',
        id_atendente_solicitacao: '#id_atendente_solicitacao',
        arquivo: '#arquivo'
    },
    events: {
        'change @ui.id_subassuntoco': 'alterarSubassunto'
    },
    initialize: function () {
        aproveitamentoModel.set({
            'id_assuntocopai': vendaModel.get('id_assuntoisencao'),
            'st_assuntocopai': vendaModel.get('st_assuntoisencao'),
            'id_categoriaocorrencia': 1,
            'id_evolucao': EVOLUCAO.TB_OCORRENCIA.AGUARDANDO_ATENDIMENTO,
            'id_situacao': SITUACAO.TB_OCORRENCIA.PENDENTE
        })
    },
    onShow: function () {
        var that = this;

        loadTinyMCE('st_ocorrencia');

        if (vendaModel.get('id_ocorrenciaaproveitamento')) {
            aproveitamentoModel.url = '/api/ocorrencia/id/' + vendaModel.get('id_ocorrenciaaproveitamento');
            aproveitamentoModel.fetch({
                complete: function () {
                    that.populaSelectSubAssunto();
                    tinyMCE.get('st_ocorrencia').setContent(aproveitamentoModel.get('st_ocorrencia'));
                }
            })
        } else {
            this.populaSelectSubAssunto();
        }

        return this;
    },
    alterarSubassunto: function () {
        aproveitamentoModel.set({
            'id_assuntoco': this.ui.id_subassuntoco.val()
        });
        this.populaSelectAtendente();
    },
    populaSelectAtendente: function () {
        var that = this;
        var atendenteCollection = new VwNucleoAssuntoCoCollection();
        atendenteCollection.url = '/venda-graduacao/retorna-atendente-subassunto/id_assuntocopai/' + aproveitamentoModel.get('id_assuntocopai') + '/id_assuntoco/' + this.ui.id_subassuntoco.val();
        atendenteCollection.fetch({
            beforeSend: loading,
            complete: function () {
                if (aproveitamentoModel.get('id_atendente_solicitacao'))
                    that.desabilitarCampos();
                loaded();
            },
            error: function () {
                $.pnotify({
                    type: 'warning',
                    title: 'Erro!',
                    text: 'Erro ao carregar assunto.'
                });
            },
            success: function () {
                new SelectView({
                    el: that.ui.id_atendente_solicitacao,
                    collection: atendenteCollection,
                    childViewOptions: {
                        optionLabel: 'st_nomecompleto',
                        optionValue: 'id_usuario',
                        optionSelected: aproveitamentoModel.get('id_atendente_solicitacao') ? aproveitamentoModel.get('id_atendente_solicitacao') : null
                    },
                    clearView: true
                }).render();
            }
        });
    },
    populaSelectSubAssunto: function () {
        var that = this;
        var assuntoCollection = new VwSubAssuntosCollection();
        assuntoCollection.url = '/venda-graduacao/retorna-sub-assunto/id_assuntocopai/' + aproveitamentoModel.get('id_assuntocopai');
        assuntoCollection.fetch({
            beforeSend: loading,
            complete: function () {
                if (aproveitamentoModel.get('id_assuntoco'))
                    that.populaSelectAtendente();
                loaded();
            },
            error: function () {
                $.pnotify({
                    type: 'warning',
                    title: 'Erro!',
                    text: 'Erro ao carregar assunto.'
                });
            },
            success: function () {
                new SelectView({
                    el: that.ui.id_subassuntoco,
                    collection: assuntoCollection,
                    childViewOptions: {
                        optionLabel: 'st_assuntoco',
                        optionValue: 'id_assuntoco',
                        optionSelected: aproveitamentoModel.get('id_assuntoco') ? aproveitamentoModel.get('id_assuntoco') : null
                    }
                }).render();

                that.ui.id_subassuntoco.trigger('change');
            }
        });
    },
    desabilitarCampos: function () {
        this.ui.id_subassuntoco.attr('disabled', 'disabled');
        this.ui.id_atendente_solicitacao.attr('disabled', 'disabled');
        this.ui.st_ocorrencia.attr('disabled', 'disabled');
        this.ui.arquivo.attr('disabled', 'disabled');
        modalAproveitamento.ui.btn_gerar.hide();
        modalAproveitamento.ui.btn_voltarcancelar.text('Voltar');
        tinyMCE.get('st_ocorrencia').getBody().setAttribute('contenteditable', false);
        viewBotoes.ui.btn_aproveitamento.text('Aguardando Aprovação do Coordenador');
    }
});

/**
 * Item para Pesquisa de responsável
 * @type {CompositeView}
 */
var PesquisaResponsavelItemView = Marionette.ItemView.extend({
    template: '#template-modal-busca-responsavel-financeiro',
    tagName: 'div',
    componente: componenteAutoComplete,
    ui: {
        tipoBuscaFisica: "input#tipo_pessoa_fisica",
        tipoBuscaJuridica: "input#tipo_pessoa_juridica",
        txtSearch: 'input#search-backbone',
        containerACResponsavelFinanceiro: '#container-autocomplete-responsavel-financeiro'
    },
    onShow: function () {
        this.$el.parent().modal('show');
        this.selecionaTipoPesquisa();

        return this;
    },
    renderizaAutoComplete: function (modelo, url, elementTemplateListaComponente) {
        var that = this;
        this.componente.main({
            model: modelo,
            element: that.ui.containerACResponsavelFinanceiro,
            localStorageName: 'responsavelAutocomplete',
            elementTemplateComponente: '#template-autocomplete-responsavel-financeiro',
            elementTemplateListaComponente: elementTemplateListaComponente,
            containerItens: '#autocomplete-responsavel-itens',
            tagNameItem: 'li',
            urlApi: url
        });
    },
    selecionaTipoPesquisa: function () {
        var that = this;

        that.ui.containerACResponsavelFinanceiro.empty();
        if (this.ui.tipoBuscaFisica.is(':checked')) {
            that.ui.containerACResponsavelFinanceiro.find('input#search-backbone').attr('placeholder', 'Pesquisar Pessoa Física');
            this.renderizaAutoComplete(VwPesquisarPessoa, '/api/pessoa', '#template-autocomplete-item-responsavel-financeiro');
        } else if (this.ui.tipoBuscaJuridica.is(':checked')) {
            that.ui.containerACResponsavelFinanceiro.find('input#search-backbone').attr('placeholder', 'Pesquisar Pessoa Jurídica');
            this.renderizaAutoComplete(Entidade, '/entidade/retorna-autocomplete-entidade', '#template-autocomplete-item-responsavel-financeiro-juridico');
        }
    },
    addPessoa: function (e) {
        $(e.currentTarget).parent().parent().remove();
        var pessoa = this.componente.modelRender;
        var modeloResponsavel;

        if (this.ui.tipoBuscaFisica.is(':checked')) {
            modeloResponsavel = new ResponsavelFinanceiroModel({
                id: pessoa.id,
                st_nomecompleto: pessoa.st_nomecompleto,
                st_cpf: pessoa.st_cpf
            });
        } else {
            modeloResponsavel = new ResponsavelFinanceiroModel({
                id: pessoa.id,
                st_nomecompleto: pessoa.st_nomeentidade,
                st_cnpj: pessoa.st_cnpj,
                st_tiporesponsavel: 'Juridica'
            });
        }
        collectionResponsavelFinanceiro.add(modeloResponsavel);
    },
    events: {
        'click input[name="tipo_pessoa"]': 'selecionaTipoPesquisa',
        'click .item-autocomplete a.btn-success': 'addPessoa'
    }
});


/**
 * View da Grade de Disciplinas
 */
var ViewGradeDisciplina = Marionette.LayoutView.extend({
    template: '#template-grade',
    regions: {
        containerGrid: '#grid-container',
        containerGridExcepcional: '#grid-container-excepcional',
        containerGridEstendidas: '#grid-container-estendidas',
        gridContratoRegion: '#grid-contrato',
        gridMensalidadeRegion: '#grid-mensalidade'
    },
    ui: {
        disciplinasSelecionadas: '#disciplinas_selec',
        btnSave: '.btnSave',
        btnCancel: '.btnCancel'
    },
    previousConModel: null,
    previousMensModel: null,
    initialize: function (options) {
        this.previousConModel = options.previousConModel || new Backbone.Model();
        this.previousMensModel = options.previousMensModel || new Backbone.Model();

        this.model.set({
            st_nomecompleto: pessoaFixada.st_nomecompleto,
            st_curso: viewCursosInformacoes.ui.selectCurso.find('option:selected').text(),
            st_turma: viewCursosInformacoes.ui.selectTurma.find('option:selected').text(),
            nu_disciplinasselecionadas: collectionDisciplinasSelecionadas.length
        })
    },
    onRender: function () {
        var that = this;

        var id_produto = viewCursosInformacoes.ui.selectCurso.val();
        var id_turma = viewCursosInformacoes.ui.selectTurma.val();
        var id_usuario = vendaModel.get('id_usuario');

        if (!viewBotoes.podeSalvar()) {
            this.ui.btnSave.hide();
            this.ui.btnCancel.html('<i class="icon icon-arrow-left"></i> Voltar para a Venda');
        }

        // Armazenar os DEFERREDS do jQuery (fetchs), e executar uma ação após todos estiverem concluídos
        var deferreds = [];

        loading();

        deferreds.push(
            collectionGridPadrao.fetch({
                data: {
                    id_turma: id_turma,
                    id_produto: id_produto,
                    id_usuario: id_usuario,
                    bl_ofertaexcepcional: false,
                    id_tipooferta: 1 // Padrão
                },
                error: function () {
                    that.gridError(that.containerGrid);
                }
            })
        );

        deferreds.push(
            collectionGridExcepcional.fetch({
                data: {
                    id_turma: id_turma,
                    id_produto: id_produto,
                    id_usuario: id_usuario,
                    bl_ofertaexcepcional: true,
                    id_tipooferta: 1 // Padrão
                },
                error: function () {
                    that.gridError(that.containerGridExcepcional);
                }
            })
        );

        deferreds.push(
            collectionGridEstendidas.fetch({
                data: {
                    id_turma: id_turma,
                    id_produto: id_produto,
                    id_usuario: id_usuario,
                    id_tipooferta: 2 // Estendida
                },
                error: function () {
                    that.gridError(that.containerGridEstendidas);
                }
            })
        );

        // Quando concluir o fetch das collections
        $.when.apply($, deferreds).always(function () {
            var gridPadrao = new GridCollectionView({
                groups: collectionGridPadrao.groups,
                collection: collectionGridPadrao,
                childViewOptions: {
                    gridModel: that.model,
                    collectionSelected: collectionDisciplinasSelecionadas,
                    collectionsToCheck: [collectionGridPadrao, collectionGridExcepcional, collectionGridEstendidas],
                    selectedCountElem: that.ui.disciplinasSelecionadas,
                    fetchData: {
                        id_venda: vendaModel.get('id_venda'),
                        id_produto: viewCursosInformacoes.ui.selectCurso.val(),
                        id_campanhacomercial: viewContratoInformacoes.ui.selectCampanhaComercial.val(),
                        id_campanhapontualidade: viewMensalidadeInformacoes.ui.selectCampanhaPontualidade.val()
                    }
                },
                onShow: function () {
                    if (!viewBotoes.podeSalvar()) {
                        this.$el.find('input').prop('disabled', true);
                    }
                }
            });

            that.containerGrid.show(gridPadrao);

            var gridExcepcional = new GridCollectionView({
                model: that.model,
                groups: collectionGridExcepcional.groups,
                collection: collectionGridExcepcional,
                childViewOptions: {
                    gridModel: that.model,
                    collectionSelected: collectionDisciplinasSelecionadas,
                    collectionsToCheck: [collectionGridPadrao, collectionGridExcepcional, collectionGridEstendidas],
                    selectedCountElem: that.ui.disciplinasSelecionadas,
                    fetchData: {
                        id_venda: vendaModel.get('id_venda'),
                        id_produto: viewCursosInformacoes.ui.selectCurso.val(),
                        id_campanhacomercial: viewContratoInformacoes.ui.selectCampanhaComercial.val(),
                        id_campanhapontualidade: viewMensalidadeInformacoes.ui.selectCampanhaPontualidade.val()
                    }
                },
                onShow: function () {
                    if (!viewBotoes.podeSalvar()) {
                        this.$el.find('input').prop('disabled', true);
                    }
                }
            });

            that.containerGridExcepcional.show(gridExcepcional);

            var gridEstendida = new GridCollectionView({
                model: that.model,
                groups: collectionGridEstendidas.groups,
                collection: collectionGridEstendidas,
                childViewOptions: {
                    gridModel: that.model,
                    collectionSelected: collectionDisciplinasSelecionadas,
                    collectionsToCheck: [collectionGridPadrao, collectionGridEstendidas, collectionGridEstendidas],
                    selectedCountElem: that.ui.disciplinasSelecionadas,
                    fetchData: {
                        id_venda: vendaModel.get('id_venda'),
                        id_produto: viewCursosInformacoes.ui.selectCurso.val(),
                        id_campanhacomercial: viewContratoInformacoes.ui.selectCampanhaComercial.val(),
                        id_campanhapontualidade: viewMensalidadeInformacoes.ui.selectCampanhaPontualidade.val()
                    }
                },
                onShow: function () {
                    if (!viewBotoes.podeSalvar()) {
                        this.$el.find('input').prop('disabled', true);
                    }
                }
            });

            that.containerGridEstendidas.show(gridEstendida);

            loaded();
        });
    },
    events: {
        'click @ui.btnSave': 'actionSave',
        'click @ui.btnCancel': 'actionCancel'
    },
    actionSave: function () {
        if (!viewBotoes.podeSalvar()) {
            $.pnotify({
                type: 'warning',
                title: 'Aviso',
                text: 'Não é possível alterar a Grade após a Negociação ter sido concluída.'
            });
            return false;
        }

        var nu_mindisciplinas = conModel.get('nu_mindisciplinas');
        var nu_maxdisciplinas = conModel.get('nu_maxdisciplinas');
        var nu_padraoselecionadas = collectionDisciplinasSelecionadas.filter(function (item) {
            return !item.get('bl_obrigatorio');
        }).length;

        if (nu_padraoselecionadas.length < nu_mindisciplinas) {
            // Valida se possui o numero minimo de disciplinas selecionadas
            $.pnotify({
                type: 'warning',
                title: 'Atenção',
                text: 'É necessário selecionar no mínimo ' + nu_mindisciplinas + ' disciplinas'
            });
            return false;
        } else if (nu_padraoselecionadas > nu_maxdisciplinas) {
            // Valida se possui o numero de disciplinas selecionadas nao excedo o maximo
            $.pnotify({
                type: 'warning',
                title: 'Atenção',
                text: 'O número de disciplinas selecionadas excedeu ao máximo permitido'
            });
            return false;
        } else {
            // Atualizar a data da última oferta na venda
            var ultima_oferta = collectionGridPadrao.ofertas = collectionGridPadrao.ofertas[collectionGridPadrao.ofertas.length - 1];
            vendaModel.set('dt_ultimaoferta', ultima_oferta.dt_encerramento);

            // Serialize
            var values = this.model.toJSON();

            // Atualiza o conModel global
            conModel.set(values);

            // Atualizar informacoes da mensalidade
            mensModel.set(values);

            telaVendas.informacoesRegion.currentView.mostraTelaInformacoes();
            geraLancamentosGraducao();

            return true;
        }
    },
    actionCancel: function () {
        collectionDisciplinasSelecionadas.set(collectionDisciplinasSelecionadas.previousModels);
        telaVendas.informacoesRegion.currentView.mostraTelaInformacoes();

        // Restaura os models
        conModel.set(this.previousConModel.toJSON());
        mensModel.set(this.previousMensModel.toJSON());
    },
    gridError: function (region) {
        $.pnotify({
            type: 'error',
            text: 'Não foi possível carregar a grade.'
        });
        $(region.el).find('.alert').html('Nenhuma oferta disponível.');
    }
});


var ViewModalJustificativa = Marionette.ItemView.extend({
    template: '#template-modal-justifica-contrato',
    ui: {
        st_justificativacontrato: '#st_justificativacontrato'
    },
    onShow: function () {
        this.$el.parent().modal('show');
        return this;
    },
    destroyModel: function () {
        this.model.clear();
    },
    salvarJustificativa: function () {
        var that = this;

        var id_venda = vendaModel.get('id_venda');
        var fileName = "Contrato - " + pessoaFixada.st_nomecompleto;
        var id_textositema = 141;

        var id_contrato,
            modelContrato;

        if (!this.ui.st_justificativacontrato.val().trim()) {
            $('#divJustificativa').addClass('control-group error');
            $('#divAvisoJustificativa').removeClass('hidden');
            return false;
        } else {
            $('#divJustificativa').removeClass('control-group error');
            $('#divAvisoJustificativa').addClass('hidden');
        }

        $.ajax({
            dataType: 'json',
            type: 'post',
            url: '/venda/salva-justificativa',
            async: false,
            data: {
                id_venda: id_venda,
                st_justificativa: this.ui.st_justificativacontrato.val(),
                nu_viascontrato: vendaModel.get('nu_viascontrato')
            },
            beforeSend: loading,
            success: function (data) {
                vendaModel.set('nu_viascontrato', data.nu_viascontrato);

                $.pnotify({
                    title: 'Sucesso',
                    text: 'Justificativa gerada e contrato contabilizado com sucesso!',
                    type: 'success'
                });

                var VwGerarContratoCollection = Backbone.Collection.extend({
                    model: VwGerarContrato,
                    url: '/api/vw-gerar-contrato/'
                });

                var contratoCollection = new VwGerarContratoCollection();
                contratoCollection.fetch({
                    data: {
                        id_venda: id_venda
                    },
                    beforeSend: loading,
                    success: function () {
                        if (contratoCollection.length) {
                            modelContrato = contratoCollection.at(0);
                        }
                    },
                    complete: function () {
                        loaded();
                        if (modelContrato) {
                            id_contrato = modelContrato.get('id_contrato');
                            id_textositema = modelContrato.get('id_contratomodelo') || id_textositema;
                            abrirPopup('/textos/geracao/?filename=' + fileName + '&id_textosistema=' + id_textositema + '&arrParams[id_contrato]=' + id_contrato + '&type=html');
                        } else {
                            $.pnotify({
                                title: 'Atenção',
                                text: 'Contrato não encontrato, verifique as regras de contrato.',
                                type: 'warning'
                            });
                        }

                        that.$el.closest('.modal').modal('hide');
                    }
                });
            },
            error: function () {
                $.pnotify({
                    title: 'Erro',
                    text: 'Erro ao tentar criar a justificativa de via do contrato!',
                    type: 'error'
                });
            },
            complete: loaded
        });
    },
    events: {
        'click #btnSaveJustificativa': 'salvarJustificativa'
    }
});


/**
 * Abre a tela de negociação caso um usuário seja selecionado no autocomplete,
 * ou venha selecionado para edição da venda.
 */
function abreNegociacao() {

    $('.content-box-vendas').show('fast');

    var layouInformacoes = new LayoutInformacoes({});
    viewCursosInformacoes = new ViewCursosInformacoes();
    viewContratoInformacoes = new ViewContratoInformacoes();
    viewMensalidadeInformacoes = new ViewMensalidadeInformacoes();
    viewAtendenteInformacoes = new ViewAtendenteInformacoes();
    viewPagamentoInformacoes = new ViewPagamentoInformacoes();
    viewBotoes = new ViewBotoes();

    telaVendas.informacoesRegion.show(layouInformacoes);

    layouInformacoes.regiao1.show(viewCursosInformacoes);
    layouInformacoes.regiao2.show(viewContratoInformacoes);
    layouInformacoes.regiao3.show(viewMensalidadeInformacoes);
    layouInformacoes.regiao4.show(viewAtendenteInformacoes);
    layouInformacoes.regiao5.show(viewPagamentoInformacoes);

    telaVendas.botoesRegion.show(viewBotoes);
}

/**
 * Fecha a tela de negociação.
 */
function fechaNegociacao() {
    telaVendas.informacoesRegion.destroy();
    collectionResponsavelFinanceiro.remove(collectionResponsavelFinanceiro.models);
    $('.content-box-vendas').hide('fast');
}

/**
 * Método onde serão validados os campos obrigatórios no formulário.
 * @returns {boolean}
 */
function validaVenda() {
    var flag = true;

    if (!viewCursosInformacoes.ui.selectCurso.val()) {
        flag = false;
        $.pnotify({
            title: 'Atenção!',
            text: 'Selecione um Curso.',
            type: 'warning'
        });

        $('#divCurso').addClass('control-group error');
    } else {
        $('#divCurso').removeClass('control-group error');
    }

    if (!viewCursosInformacoes.ui.selectTurma.val()) {
        flag = false;
        $.pnotify({
            title: 'Atenção!',
            text: 'Selecione uma Turma.',
            type: 'warning'
        });
        viewCursosInformacoes.ui.divTurma.addClass('control-group error');
    } else {
        viewCursosInformacoes.ui.divTurma.removeClass('control-group error');
    }

    // validação para data de ingresso
    // if (viewCursosInformacoes.ui.digitDataIngresso.val() == '') {
    //     flag = false;
    //     $.pnotify({
    //         title: 'Atenção!',
    //         text: 'Preencha a Data de Ingresso.',
    //         type: 'warning'
    //     });
    //     viewCursosInformacoes.ui.digitDataIngresso.addClass('control-group error');
    // } else {
    //     viewCursosInformacoes.ui.digitDataIngresso.removeClass('control-group error');
    // }

    if (!viewCursosInformacoes.ui.selectProcessoSelecao.val()) {
        flag = false;
        $.pnotify({
            title: 'Atenção!',
            text: 'Selecione um Processo Seletivo.',
            type: 'warning'
        });
        viewCursosInformacoes.ui.divProcessoSeletivo.addClass('control-group error');
    } else {
        viewCursosInformacoes.ui.divProcessoSeletivo.removeClass('control-group error');
    }

    if (collectionDisciplinasSelecionadas.length < conModel.get('nu_mindisciplinas')) {
        // Valida se possui o numero minimo de disciplinas selecionadas
        flag = false;
        $.pnotify({
            type: 'warning',
            title: 'Atenção',
            text: 'É necessário que a grade possua no mínimo ' + conModel.get('nu_mindisciplinas') + ' disciplinas.'
        });
    }

    if (!viewAtendenteInformacoes.ui.selectAtendente.val()) {
        flag = false;
        $.pnotify({
            title: 'Atenção!',
            text: 'Selecione um atendente.',
            type: 'warning'
        });
        viewAtendenteInformacoes.ui.divAtendente.addClass('control-group error');
    } else {
        viewAtendenteInformacoes.ui.divAtendente.removeClass('control-group error');
    }

    if (!viewAtendenteInformacoes.ui.selectContratoRegra.val()) {
        flag = false;
        $.pnotify({
            title: 'Atenção!',
            text: 'Selecione uma Regra de Contrato.',
            type: 'warning'
        });
        viewAtendenteInformacoes.ui.divRegraContrato.addClass('control-group error');
    } else {
        viewAtendenteInformacoes.ui.divRegraContrato.removeClass('control-group error');
    }


    //Valida preenchimento dos meios de pagamento
    if (!viewPagamentoInformacoes.ui.selectPagamentoEntrada.val() && conModel.get('nu_valornegociacao') > 0) {
        flag = false;
        $.pnotify({
            title: 'Atenção!',
            text: 'Selecione um meio de pagamento para entrada.',
            type: 'warning'
        });
        viewPagamentoInformacoes.ui.divMeioEntrada.addClass('control-group error');
    } else {
        viewPagamentoInformacoes.ui.divMeioEntrada.removeClass('control-group error');
    }

    if (!viewPagamentoInformacoes.ui.selectPagamentoParcelas.val() && conModel.get('nu_valornegociacao') > 0) {
        flag = false;
        $.pnotify({
            title: 'Atenção!',
            text: 'Selecione um meio de pagamento para as parcelas.',
            type: 'warning'
        });
        viewPagamentoInformacoes.ui.divMeioParcelas.addClass('control-group error');
    } else {
        viewPagamentoInformacoes.ui.divMeioParcelas.removeClass('control-group error');
    }

    if (!viewPagamentoInformacoes.ui.vencimentoEntrada.val() && conModel.get('nu_valornegociacao') > 0) {
        flag = false;
        $.pnotify({
            title: 'Atenção!',
            text: 'Selecione uma data para o vencimento da entrada.',
            type: 'warning'
        });
        viewPagamentoInformacoes.ui.divVencimentoEntrada.addClass('control-group error');
    } else {
        viewPagamentoInformacoes.ui.divVencimentoEntrada.removeClass('control-group error');
    }

    //Caso não seja a entrada Cartao e Parcela Cartão, é obrigatorio escolher a data de vencimento das parcelas
    if (viewPagamentoInformacoes.ui.selectPagamentoEntrada.val() != 1
        && viewPagamentoInformacoes.ui.selectPagamentoParcelas.val() != 1
        && conModel.get('nu_valornegociacao') > 0) {
        if (!viewPagamentoInformacoes.ui.vencimentoParcela.val()) {
            flag = false;
            $.pnotify({
                title: 'Atenção!',
                text: 'Selecione um dia para o vencimento das parcelas.',
                type: 'warning'
            });
            viewPagamentoInformacoes.ui.divVencimentoParcelas.addClass('control-group error');
        } else {
            viewPagamentoInformacoes.ui.divVencimentoParcelas.removeClass('control-group error');
        }
    }

    //valida responsavel financeiro
    if (!viewPagamentoInformacoes.ui.selectResponsavelFinanceiro.val() && conModel.get('nu_valornegociacao') > 0) {
        flag = false;
        $.pnotify({
            title: 'Atenção!',
            text: 'Selecione um responsável financeiro para essa venda.',
            type: 'warning'
        });
        viewPagamentoInformacoes.ui.divRespFinanceiroGrad.addClass('control-group error');
    } else {
        viewPagamentoInformacoes.ui.divRespFinanceiroGrad.removeClass('control-group error');
    }

    return flag;
}


function geraLancamentosGraducao(elem) {
    //Se a venda não tiver sido salva, pode alterar sem regras os lancamentos
    if (!vendaModel.get('id_venda')) {
        vwVendaLancamentoCollection.reset();
        if (collectionDisciplinasSelecionadas.length) { //calcula as parcelas apenas se tiver disciplinas selecionadas
            geraLancamentoEntrada();
            geraLancamentoParcelas();
        }
    } else {
        alteraLancamentos(elem);
    }
}

function alteraLancamentos(elem) {
    //Elementos de alterações especificas (data e valor das parcelas)
    if (elem) {
        var elemento = parseInt(elem);
        switch (elemento) {
            case COMBO_MEIO_ENTRADA: {
                vwVendaLancamentoCollection.each(function (model) {
                    if (model.get('bl_entrada') && !model.get('bl_quitada')) {
                        model.set({
                            'id_meiopagamento': viewPagamentoInformacoes.ui.selectPagamentoEntrada.val(),
                            'st_meiopagamento': !viewPagamentoInformacoes.ui.selectPagamentoEntrada.val() ? ' - ' : viewPagamentoInformacoes.ui.selectPagamentoEntrada.find("option:selected").text()
                        });
                    }
                });
                break;
            }
            case INPUT_VENCIMENTO_ENTRADA: {
                vwVendaLancamentoCollection.each(function (model) {
                    if (!model.get('bl_quitada')) {
                        if (model.get('bl_entrada')) {
                            model.set('dt_vencimento', viewPagamentoInformacoes.ui.vencimentoEntrada.val());
                        }
                        // Recalcular as Datas das Parcelas
                        alteraLancamentos(INPUT_VENCIMENTO_PARCELA);
                    }
                });
                break;
            }
            case COMBO_MEIO_PARCELA: {
                vwVendaLancamentoCollection.each(function (model) {
                    if (!model.get('bl_entrada') && !model.get('bl_quitada')) {
                        model.set({
                            'id_meiopagamento': viewPagamentoInformacoes.ui.selectPagamentoParcelas.val(),
                            'st_meiopagamento': !viewPagamentoInformacoes.ui.selectPagamentoParcelas.val() ? ' - ' : viewPagamentoInformacoes.ui.selectPagamentoParcelas.find("option:selected").text()
                        });

                        //Se for Cartao, Datas das Parcelas
                        alteraLancamentos(INPUT_VENCIMENTO_PARCELA);
                    }
                });
                break;
            }
            case INPUT_VENCIMENTO_PARCELA: {
                vwVendaLancamentoCollection.each(function (model, key) {
                    var incremento = key;
                    var dt_vencimento = viewPagamentoInformacoes.ui.vencimentoEntrada.val();
                    var dia_vencimento = viewPagamentoInformacoes.ui.vencimentoParcela.val();

                    if (!model.get('bl_entrada') && !model.get('bl_quitada')) {
                        if (dt_vencimento) {

                            if (!dia_vencimento) {
                                var dia_temp = viewPagamentoInformacoes.ui.vencimentoParcela.data('selected');
                                if (dia_temp) {
                                    dia_vencimento = parseInt(dia_temp);
                                }
                            }

                            var nu_intervalo = (parseInt(viewPagamentoInformacoes.ui.intervaloVencimento.val()) || 1) - 1;

                            // Mudar apenas o dia do vencimento da parcela
                            if (dia_vencimento && viewPagamentoInformacoes.ui.selectPagamentoParcelas.val() == PAG_BOLETO) {
                                dt_vencimento = dia_vencimento + dt_vencimento.substr(2, 10);
                                dt_vencimento = proximaDataVencimentoComAnoBissexto(dt_vencimento, 0, true);
                            }

                            dt_vencimento = (proximaDataVencimentoComAnoBissexto(dt_vencimento, incremento + nu_intervalo)).split('-').reverse().join('/');
                        }

                        model.set('dt_vencimento', dt_vencimento);
                    }
                });
                break;
            }
            case COMBO_RESPONSAVEL: {
                var responsavel = retornaResponsavelFinanceiroModel();

                vwVendaLancamentoCollection.each(function (model) {
                    model.set({
                        id_usuariolancamento: responsavel.id_usuariolancamento,
                        id_entidadelancamento: responsavel.id_entidadelancamento
                    })
                });
                break;
            }
            default :
                break;
        }
    } else {
        // @history GII-9383
        // Quando o valor for ZERO, gerar apenas uma parcela com 1 centavo e já PAGA
        if (parseInt(conModel.get('nu_valornegociacao')) <= 0) {
            var first = vwVendaLancamentoCollection.at(0);

            first.set({
                nu_valor: 0.01,
                bl_quitado: true,
                dt_quitado: (new Date()).toISOString().substr(0, 10)
            });

            vwVendaLancamentoCollection.set([first]);
        } else if (vwVendaLancamentoCollection.length < 6) {
            var quitados = vwVendaLancamentoCollection.models.filter(function(model) {
                return model.get("bl_quitado") && parseInt(model.get("nu_valor")) > 0;
            });

            viewPagamentoInformacoes.ui.selectPagamentoEntrada.trigger("change");

            vwVendaLancamentoCollection.set(quitados);
            geraLancamentoEntrada();
            geraLancamentoParcelas();
        }

        //verifica se existe algum lancamento que não foi quitado
        var modelNaoQuitado = vwVendaLancamentoCollection.findWhere({bl_quitado: false});
        //se existir lancamentos nao quitado, verifica quantos são
        if (modelNaoQuitado) {
            var arrayNaoQuitado = vwVendaLancamentoCollection.where({bl_quitado: false});
            // se o length dos não quitados for igual ao length da collection de lancamentos, pode-se apenas setar o valor da mensalidade
            // nos lancamentos pois nenhum foi quitado
            if (arrayNaoQuitado.length == vwVendaLancamentoCollection.length) {
                vwVendaLancamentoCollection.each(function (model) {
                    model.set('nu_valor', mensModel.get('nu_valormensalidade'));
                });
            } else {
                // se não, verifica qual valor já foi pago
                var valorQuitado = mensModel.get('nu_valordescontoentrada');
                vwVendaLancamentoCollection.each(function (model) {
                    if (model.get('bl_quitado') == true) {
                        valorQuitado += formatValueToReal(model.get('nu_valor'));
                    }
                });

                var valorAPagar = formatValueToReal(conModel.get('nu_valornegociacao')) - valorQuitado;
                var valorParcelas = valorAPagar / parseInt(arrayNaoQuitado.length);

                //seta nos que nao foram pagos o valor
                vwVendaLancamentoCollection.each(function (model) {
                    if (model.get('bl_quitado') == false) {
                        model.set('nu_valor', valorParcelas);
                    }
                });
            }
        }
    }

}

function geraLancamentoEntrada() {
    var responsavel = retornaResponsavelFinanceiroModel();

    var vwVendaLanEntrada = vwVendaLancamentoCollection.findWhere({bl_entrada: true});

    // Adicionar uma entrada
    if (!vwVendaLanEntrada) {
        vwVendaLanEntrada = new VwVendaLancamento();
    }

    vwVendaLanEntrada.set({
        'bl_entrada': true,
        'nu_valor': mensModel.get('nu_valorentrada'),
        'nu_parcelas': 1,
        'bl_quitado': false,
        'bl_ativo': 1,
        'id_tipolancamento': 1,
        'id_entidadelancamento': responsavel.id_entidadelancamento,
        'id_usuariolancamento': responsavel.id_usuariolancamento,
        'id_meiopagamento': viewPagamentoInformacoes.ui.selectPagamentoEntrada.val(),
        'st_meiopagamento': viewPagamentoInformacoes.ui.selectPagamentoEntrada.find("option:selected").text() || " - ",
        'id_venda': vendaModel.get('id_venda')
    });

    if (viewPagamentoInformacoes.ui.vencimentoEntrada.val()) {
        vwVendaLanEntrada.set('dt_vencimento', viewPagamentoInformacoes.ui.vencimentoEntrada.val());
    }

    vwVendaLancamentoCollection.add(vwVendaLanEntrada);
}

function geraLancamentoParcelas() {
    var responsavel = retornaResponsavelFinanceiroModel();

    var nuParcelas = 6 - vwVendaLancamentoCollection.length;

    var meioPagamento = viewPagamentoInformacoes.ui.selectPagamentoParcelas.val() || viewPagamentoInformacoes.ui.selectPagamentoEntrada.val();

    // Adiciona 5 lancamentos como parcelas
    for (var i = 1; i <= nuParcelas; i++) {
        var vwVendaLanParcela = new VwVendaLancamento();

        vwVendaLanParcela.set({
            bl_entrada: false,
            'nu_valor': mensModel.get('nu_valormensalidade'),
            'nu_parcelas': i + 1,
            'bl_quitado': false,
            'bl_ativo': 1,
            'id_tipolancamento': 1,
            'id_entidadelancamento': responsavel.id_entidadelancamento,
            'id_usuariolancamento': responsavel.id_usuariolancamento,
            'id_meiopagamento': meioPagamento,
            'st_meiopagamento': viewPagamentoInformacoes.ui.selectPagamentoParcelas.find("option[value='" + meioPagamento + "']").text(),
            'id_venda': vendaModel.get('id_venda')
        });

        var dt_vencimento = viewPagamentoInformacoes.ui.vencimentoEntrada.val();

        // Se as parcelas forem no boleto
        if (viewPagamentoInformacoes.ui.selectPagamentoParcelas.val() == PAG_BOLETO) {
            var diaVencParcela = viewPagamentoInformacoes.ui.vencimentoParcela.val();

            //verifica se foi setado o dia de vencimento das parcelas
            if (diaVencParcela) {
                var nu_intervalo = (parseInt(viewPagamentoInformacoes.ui.intervaloVencimento.val()) || 1) - 1;
                var mes = parseInt(i) + parseInt(nu_intervalo);

                dt_vencimento = diaVencParcela + dt_vencimento.substr(2, 10);
                dt_vencimento = (proximaDataVencimentoComAnoBissexto(dt_vencimento, mes)).split('-').reverse().join('/');

                vwVendaLanParcela.set('dt_vencimento', dt_vencimento);
            }
        }

        //Parcelas com cartão de crédito não escolhe a data
        if (viewPagamentoInformacoes.ui.selectPagamentoParcelas.val() == PAG_CARTAO) {
            dt_vencimento = (proximaDataVencimentoComAnoBissexto(dt_vencimento, i)).split('-').reverse().join('/');
            vwVendaLanParcela.set('dt_vencimento', dt_vencimento);
        }

        vwVendaLancamentoCollection.add(vwVendaLanParcela);
    }
}

function desabilitaCamposPagamento() {
    //Se a venda não tiver sido salva, pode alterar sem regras os lancamentos
    $('#id_campanha_pontualidade').attr('disabled', 'disabled');
    $('#id_meiopagamentoentrada').attr('disabled', 'disabled');
    $('#dt_vencimentoentrada').attr('disabled', 'disabled');
    $('#id_meiopagamentoparcelas').attr('disabled', 'disabled');
    $('#nu_vencimentoparcelas').attr('disabled', 'disabled');
    $('#nu_mesesintervalovencimento').attr('disabled', 'disabled');
    $('#id_responsavelfinanceirograd').attr('disabled', 'disabled');
    $('#btn-procurar-responsavel-financeiro').attr('disabled', 'disabled');
}

function habilitaCamposPagamento() {
    //Se a venda não tiver sido salva, pode alterar sem regras os lancamentos
    $('#id_campanha_pontualidade').removeAttr('disabled');
    $('#id_meiopagamentoentrada').removeAttr('disabled');
    $('#dt_vencimentoentrada').removeAttr('disabled');
    $('#id_meiopagamentoparcelas').removeAttr('disabled');
    $('#nu_vencimentoparcelas').removeAttr('disabled');
    $('#nu_mesesintervalovencimento').removeAttr('disabled');
    $('#id_responsavelfinanceirograd').removeAttr('disabled');
    $('#btn-procurar-responsavel-financeiro').removeAttr('disabled');
}

function retornaResponsavelFinanceiroModel() {
    var id_responsavelfinanceiro = $('#id_responsavelfinanceirograd').val();
    var responsavel = collectionResponsavelFinanceiro.where({id: parseInt(id_responsavelfinanceiro)});
    var id_usuariolancamento = null;
    var id_entidadelancamento = null;

    if (responsavel[0]) {
        if (responsavel[0].get('st_tiporesponsavel') == 'Fisica') {
            id_usuariolancamento = id_responsavelfinanceiro;
        } else {
            id_entidadelancamento = id_responsavelfinanceiro;
        }

    }
    return {'id_usuariolancamento': id_usuariolancamento, 'id_entidadelancamento': id_entidadelancamento};
}

/**
 ******************************************************************************************
 ******************************************************************************************
 ********* SHOW LAYOUT ********************************************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var telaVendas = new LayoutVendaGraduacao();
G2S.show(telaVendas);
var boolCheckAll = false;
var search;
var layoutEncaminharOcorrencia;

/**
 * Collections para o div do filtro das ocorrencias
 */

var Funcao = Backbone.Collection.extend({
    model: Backbone.Model,
    url: '/encaminhar-ocorrencias/retornar-funcao'
});

var EvolucaoCollection = Backbone.Collection.extend({
    model: Evolucao,
    url: '/encaminhar-ocorrencias/retornar-evolucao'
});

var Atendente = Backbone.Collection.extend({
    model: Backbone.Model,
    url: '/encaminhar-ocorrencias/retornar-atendente/'
});

var Assunto = Backbone.Collection.extend({
    model: Backbone.Model,
    url: '/encaminhar-ocorrencias/retornar-assunto/'
});

var SubAssunto = Backbone.Collection.extend({
    model: VwAssuntos,
    url: '/ocorrencia/find-sub-assunto/'
});

var SearchPageableCollection = Backbone.PageableCollection.extend({
    url: '/api/ocorrencia/',
    model: VwOcorrenciasPessoa,
    state: {
        pageSize: 200
    },
    mode: "client",
    parseState: function (response, queryParams, state, options) {
        return {totalRecords: response.total};
    },
    parseRecords: function (response, options) {
        _.each(response, function (model, index) {
            model.index = index;
        });
        return response;
    },
    sync: function (method, model, options) {
        options.beforeSend = loading;
        options.complete = loaded;
        return Backbone.sync(method, model, options);
    }
});

/**
 * Collections para o div do encaminhar ocorrencias
 */

var AssuntoEncaminhar = Backbone.Collection.extend({
    model: Backbone.Model,
    url: '/encaminhar-ocorrencias/retornar-assunto/'
});

var SubAssuntoEncaminhar = Backbone.Collection.extend({
    model: VwAssuntos,
    url: '/ocorrencia/find-sub-assunto/'
});

var AtendenteEncaminhar = Backbone.Collection.extend({
    model: Backbone.Model,
    url: '/encaminhar-ocorrencias/retornar-atendente-encaminhar/'
});

/**
 * ItemView da div encaminhar ocorrencias
 */
var ItemViewencaminharocorrencias = Marionette.ItemView.extend({
    template: "#itemView-encaminhar-ocorrencias",
    ui: {
        idAtendenteEncaminhar: '#id_atendente_encaminhar',
        idAssuntoEncaminhar: '#id_assunto_encaminhar',
        idSubassuntoEncaminhar: '#id_subassunto_encaminhar',
        idBtnEncaminhar: '#id_btn_encaminhar'
    },
    events: {
        'change @ui.idAssuntoEncaminhar': 'carregarSubAssuntoEncaminhar',
        'change @ui.idSubassuntoEncaminhar': 'carregarAtendenteEncaminhar',
        'click @ui.idBtnEncaminhar': 'encaminharOcorrencias'
    },
    initialize: function () {

    },
    onShow: function () {
        this.carregarAssuntoEncaminhar();
    },
    carregarAssuntoEncaminhar: function () {
        var assuntoEncaminhar = new AssuntoEncaminhar();
        assuntoEncaminhar.fetch({
            async: false,
            data: {id_funcao: $('#id_funcao').val()},
            beforeSend: function () {
                loading();
            }.bind(this),
            complete: loaded,
            success: function (collection) {
                $('#id_assunto_encaminhar').empty();
                var viewSelectAssuntoEncaminhar = new SelectView({
                    el: "#id_assunto_encaminhar",
                    collection: collection,
                    childViewOptions: {
                        optionLabel: 'st_assuntoco',
                        optionValue: 'id_assuntoco',
                        optionSelected: ''
                    }
                });
                this.ui.idAssuntoEncaminhar.html('<option value="">Selecione o Assunto</option>');
                viewSelectAssuntoEncaminhar.render();
                this.carregarSubAssuntoEncaminhar();
            }.bind(this)
        });
    },
    carregarSubAssuntoEncaminhar: function () {
        var subassuntoEncaminhar = new SubAssuntoEncaminhar();
        subassuntoEncaminhar.fetch({
            async: false,
            data: {id_assuntocopai: this.ui.idAssuntoEncaminhar.val()},
            beforeSend: function () {
                loading();
            }.bind(this),
            complete: loaded,
            success: function (collection) {
                this.ui.idSubassuntoEncaminhar.empty();
                var viewSelectSubAssuntoEncaminhar = new SelectView({
                    el: "#id_subassunto_encaminhar",
                    collection: collection,
                    childViewOptions: {
                        optionLabel: 'st_assuntoco',
                        optionValue: 'id_assuntoco',
                        optionSelected: ''
                    }
                });

                viewSelectSubAssuntoEncaminhar.render();
            }.bind(this)
        });
    },
    carregarAtendenteEncaminhar: function () {
        var atendenteEncaminhar = new AtendenteEncaminhar();
        atendenteEncaminhar.fetch({
            data: {id_assuntocopai: this.ui.idAssuntoEncaminhar.val(), id_assuntoco: this.ui.idSubassuntoEncaminhar.val()},
            beforeSend: function () {
                loading();
            }.bind(this),
            complete: loaded,
            success: function (collection) {
                this.ui.idAtendenteEncaminhar.empty();
                var viewSelectAtendenteEncaminhar = new SelectView({
                    el: "#id_atendente_encaminhar",
                    collection: collection,
                    childViewOptions: {
                        optionLabel: 'st_nomecompleto',
                        optionValue: 'id_usuario',
                        optionSelected: ''
                    }
                });

                viewSelectAtendenteEncaminhar.render();
            }.bind(this)
        });
    },
    encaminharOcorrencias: function () {
        var ocorrencias = search.where({attr_check: true});
        var idOcorrencias = new Array();

        $.each(ocorrencias, function (key, value) {
            idOcorrencias.push(value.get("id_ocorrencia"));
        });

        if (this.validarParametrosEncaminharOcorrencia(idOcorrencias)) {
            $.ajax({
                url: '/encaminhar-ocorrencias/encaminhar-ocorrencias-em-lote/',
                method: 'POST',
                data: {
                    id_assuntoco: this.ui.idSubassuntoEncaminhar.val(),
                    id_usuario: this.ui.idAtendenteEncaminhar.val(),
                    id_ocorrencia: idOcorrencias
                },
                beforeSend: function () {
                    loading();
                }.bind(this),
                complete: loaded,
                success: function (retorno) {
                    $.pnotify({
                        type: 'success',
                        title: 'Sucesso!',
                        text: retorno.text
                    });

                    layoutEncaminharOcorrencia.pesquisarOcorrencias();
                },
                error: function (erro) {
                    $.pnotify({
                        type: 'error',
                        title: 'Erro!',
                        text: erro.statusText
                    });
                }
            });
        }

    },
    validarParametrosEncaminharOcorrencia: function (idOcorrencias) {
        var erroEncaminhar = null;

        if (idOcorrencias.length === 0) {
            erroEncaminhar = 'Selecione uma ocorrência.';
        } else if (!this.ui.idAssuntoEncaminhar.val() || this.ui.idAssuntoEncaminhar.val() === "") {
            this.ui.idAssuntoEncaminhar.focus();
            erroEncaminhar = 'Selecione um Assunto.';
        } else if (!this.ui.idSubassuntoEncaminhar.val() || this.ui.idSubassuntoEncaminhar.val() === "") {
            this.ui.idSubassuntoEncaminhar.focus();
            erroEncaminhar = 'Selecione um Subsssunto.';
        } else if (!this.ui.idAtendenteEncaminhar.val() || this.ui.idAtendenteEncaminhar.val() === "") {
            this.ui.idAtendenteEncaminhar.focus();
            erroEncaminhar = 'Selecione um Atendente.';
        }

        if (erroEncaminhar) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: erroEncaminhar
            });
            return false;
        }

        return true;
    }

});

/**
 *  Layout principal
 */
var LayoutEncaminharOcorrencias = Marionette.LayoutView.extend({
    template: '#template-filtro-ocorrencias',
    regions: {
        regiaoEncaminharOcorrencias: "#regiao-encaminhar-ocorrencias"
    },
    ui: {
        idFuncao: '#id_funcao',
        idOcorrencia: '#id_ocorrencia',
        idAtendente: '#id_atendente',
        idAssunto: '#id_assunto',
        idSubAssunto: '#id_subassunto',
        idEvolucao: '#id_evolucao',
        idSituacao: '#id_situacao',
        idBtnPesquisar: '#id_btn-pesquisar'
    },
    events: {
        'change @ui.idFuncao': function () {
            this.carregarAtendente();
            this.carregarAssunto();
        },
        'change @ui.idAssunto': 'carregarSubAssunto',
        'click @ui.idBtnPesquisar': 'pesquisarOcorrencias'
    },
    onShow: function () {
        this.carregarFuncao();
        this.carregarEvolucao();
    },
    carregarFuncao: function () {
        var funcao = new Funcao();
        funcao.fetch({
            beforeSend: function () {
                loading();
            }.bind(this),
            complete: loaded,
            success: function (collection) {
                var viewSelectFuncao = new SelectView({
                    el: "#id_funcao",
                    collection: collection,
                    childViewOptions: {
                        optionLabel: 'st_funcao',
                        optionValue: 'id_funcao',
                        optionSelected: 'id_funcao'
                    }
                });

                viewSelectFuncao.render();
            }.bind(this)
        });
    },
    carregarEvolucao: function () {
        var evolucao = new EvolucaoCollection();
        evolucao.fetch({
            beforeSend: function () {
                loading();
            }.bind(this),
            complete: loaded,
            success: function (collection) {
                var viewSelectFuncao = new SelectView({
                    el: "#id_evolucao",
                    collection: collection,
                    childViewOptions: {
                        optionLabel: 'st_evolucao',
                        optionValue: 'idEvolucao',
                        optionSelected: ''
                    }
                });

                viewSelectFuncao.render();
            }.bind(this)
        });
    },
    carregarAtendente: function () {
        var atendente = new Atendente();
        atendente.fetch({
            data: {id_funcao: this.ui.idFuncao.val()},
            beforeSend: function () {
                loading();
            }.bind(this),
            complete: loaded,
            success: function (collection) {
                this.ui.idAtendente.empty();
                var viewSelectAtendente = new SelectView({
                    el: "#id_atendente",
                    collection: collection,
                    childViewOptions: {
                        optionLabel: 'st_nomecompleto',
                        optionValue: 'id_usuario',
                        optionSelected: ''
                    }
                });
                this.ui.idAtendente.html('<option value="">Todos</option>');
                viewSelectAtendente.render();
            }.bind(this)
        });
    },
    carregarAssunto: function () {
        var assunto = new Assunto();
        assunto.fetch({
            data: {id_funcao: this.ui.idFuncao.val()},
            beforeSend: function () {
                loading();
            }.bind(this),
            complete: loaded,
            success: function (collection) {
                this.ui.idAssunto.empty();
                var viewSelectAssunto = new SelectView({
                    el: "#id_assunto",
                    collection: collection,
                    childViewOptions: {
                        optionLabel: 'st_assuntoco',
                        optionValue: 'id_assuntoco',
                        optionSelected: ''
                    }
                });
                this.ui.idAssunto.html('<option value="">Selecione o Assunto</option>');
                viewSelectAssunto.render();
            }.bind(this)
        });
    },
    carregarSubAssunto: function () {
        var subassunto = new SubAssunto();
        subassunto.fetch({
            data: {id_assuntocopai: this.ui.idAssunto.val()},
            beforeSend: function () {
                loading();
            }.bind(this),
            complete: loaded,
            success: function (collection) {
                this.ui.idSubAssunto.empty();
                var viewSelectSubAssunto = new SelectView({
                    el: "#id_subassunto",
                    collection: collection,
                    childViewOptions: {
                        optionLabel: 'st_assuntoco',
                        optionValue: 'id_assuntoco',
                        optionSelected: ''
                    }
                });

                viewSelectSubAssunto.render();
            }.bind(this)
        });
    },
    pesquisarOcorrencias: function () {

        if (this.validarParametros()) {


            search = new SearchPageableCollection();

            var params = {
                id_ocorrencia: this.ui.idOcorrencia.val(),
                id_assuntocopai: this.ui.idAssunto.val(),
                id_evolucao: this.ui.idEvolucao.val(),
                id_atendente: this.ui.idAtendente.val(),
                id_assuntoco: this.ui.idSubAssunto.val(),
                id_situacao: this.ui.idSituacao.val(),
                id_entidade: G2S.loggedUser.get("id_entidade"),
                id_nucleoco: this.ui.idFuncao.val().split('-')[1],
                id_funcao: this.ui.idFuncao.val().split('-')[0]
            };
            search.queryParams.paramsSearch = params;

            var CheckedCell = Backgrid.Cell.extend({
                className: "select-row-cell",
                render: function () {
                    var checked = this.model.get("attr_check") ? "checked='checked'" : '';
                    this.$el.html(_.template('<input type="checkbox" name="model[' + this.model.get("index") + ']" ' + checked + ' tabindex="-1" class="select-header-cell">'));
                    return this;
                }
            });

            var columns = [
                {name: '', label: 'Todos', editable: false, cell: CheckedCell, headerCell: 'select-all'},
                {
                    name: 'id_ocorrencia',
                    label: 'ID',
                    cell: Backgrid.IntegerCell.extend({orderSeparator: '.'}),
                    editable: false
                },
                {name: "st_entidadematricula", label: "Entidade de Matrícula", cell: "string", editable: false},
                {name: 'st_assuntocopai', label: 'Assunto', cell: 'string', editable: false},
                {name: 'st_assuntoco', label: 'Subassunto', cell: 'string', editable: false},
                {name: 'st_nomeinteressado', label: 'Nome Interessado', cell: 'string', editable: false},
                {name: 'st_titulo', label: 'Título', cell: 'string', editable: false},
                {name: 'st_atendente', label: 'Atendente', cell: 'string', editable: false},
                {name: 'st_evolucao', label: 'Evolução', cell: 'string', editable: false},
                {name: 'st_situacao', label: 'Situação', cell: 'string', editable: false},
                {name: 'st_cadastroocorrencia', label: 'Data Criação', cell: 'string', editable: false},
                {name: 'st_ultimotramite', label: 'Última Interação', cell: 'string', editable: false},
                {name: 'nu_horasatraso', label: 'Horas Atraso', cell: 'string', editable: false},
                {name: 'nu_diasaberto', label: 'Dias Aberto', cell: 'string', editable: false}
            ];

            var RowGrid = Backgrid.Row.extend({
                initialize: function (options) {
                    RowGrid.__super__.initialize.apply(this, arguments);
                },
                events: {
                    "click .select-header-cell": function () {
                        if (this.$el.find("input[name='model[" + this.model.get("index") + "]']").is(":checked")) {
                            $("input[name='model[" + this.model.get("index") + "]']").attr("checked", true);
                            this.model.set("attr_check", true);
                            search.add(this.model);
                        } else {
                            $("input[name='model[" + this.model.get("index") + "]']").attr("checked", false);
                            this.model.set("attr_check", false);
                            search.add(this.model);
                        }
                    }
                }
            });

            var pagebleGrid = new Backgrid.Grid({
                className: 'backgrid table table-bordered',
                columns: columns,
                collection: search,
                row: RowGrid,
                events: {
                    "click .select-all-header-cell": function () {
                        loading();
                        if (!boolCheckAll) {
                            boolCheckAll = true;

                            this.collection.each(function (model) {
                                if (!model.get("attr_check")) {
                                    model.set("attr_check", true);
                                    search.add(model);
                                }
                            });
                        } else {
                            boolCheckAll = false;

                            this.collection.each(function (model) {
                                if (model.get("attr_check")) {
                                    model.set("attr_check", false);
                                    search.add(model);
                                }
                            });
                        }
                        pagebleGrid.render();
                        $(".select-all-header-cell input").attr("checked", boolCheckAll);
                        loaded();
                    }
                }
            });

            var paginator = new Backgrid.Extension.Paginator({
                collection: search
            });

            var elemento = $('#data-ocorrencia');

            elemento.empty();
            paginator.$el.empty();

            // Realiza a pesquisa
            search.fetch({
                //async: false,
                timeout: 55555,
                ifModified: true,
                success: function () {
                    elemento.append(pagebleGrid.render().$el);
                    elemento.append(paginator.render().$el);

                    layoutEncaminharOcorrencia.getRegion('regiaoEncaminharOcorrencias').show(new ItemViewencaminharocorrencias());
                }
            }).always(function (response) {
                if (response.status === 500) {
                    $.pnotify({
                        type: 'error',
                        title: 'Erro!',
                        text: response.responseText
                    });
                } else {
                    if (!search.length) {
                        $.pnotify({
                            type: 'warning',
                            title: 'Aviso',
                            text: MensagensG2.MSG_OCORRENCIA_03
                        });
                    }
                }
                //Preenchendo valor na tela de demonstracao de quantidade de resultado
                $('#qnt').empty().append(sprintf(MensagensG2.MSG_OCORRENCIA_32, search.state.totalRecords))
                    .closest('div').find('br').remove();
                loaded();
            });
        }
    },
    validarParametros: function () {

        if (!this.ui.idAssunto.val()) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção',
                text: 'Escolha um assunto.'
            });
            return false;
        }

        return true;
    }
});

layoutEncaminharOcorrencia = new LayoutEncaminharOcorrencias();
G2S.show(layoutEncaminharOcorrencia);
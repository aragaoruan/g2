/**
 * Funcionalidade Relatorio Provas por Polo
 *
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */

/**
 * Modelo criada neste ponto para uso da utils que retorna lista de polos
 * por entidade, e tem a definicao de dados diferenciada.
 */
var ComboPoloModel = Backbone.Model.extend({
    defaults: {
        st_text: '',
        id_value: ''
    }
});

/**
 * Collection Polos
 */
var PoloCollectionExtend = Backbone.Collection.extend({
    url: '/util/filtra-aplicador',
    model: ComboPoloModel
});

/**
 * CompositeView para funcionalidade
 */
var RelatorioProvasPoloCompositeView = Marionette.CompositeView.extend({
    template: '#tpl-relatorio-provas-polo',
    ui: {
        'qntPorPagina': '#qntPorPagina',
        'comboPolo': '.combo-polo',
        'formPesquisaProvas': '#formRelatorio',
        'inputDataProvaInicio': '.input-dt-prova-inicio',
        'inputDataProvaTermino': '.input-dt-prova-termino'
    },
    onRender: function(){

        var that = this;

        var poloCollection = new PoloCollectionExtend();
        poloCollection.fetch({
            data: {
                bl_ativo: 1
            }
        });

        (new SelectView({
            el: that.ui.comboPolo,
            collection: poloCollection,
            childViewOptions: {
                optionLabel: 'st_text',
                optionValue: 'id_value',
                optionSelected: null
            }
        })).render();

    },
    events: {
        'submit #formRelatorio': 'renderGrid'
    },
    renderGrid: function(e){

        e.preventDefault();
        var that = this;

        var PageableCollectionExtend = Backbone.PageableCollection.extend({
            model: VwProvasPorPolo,
            url: '/provas-polo/pesquisa-provas-polo',
            mode: 'client',
            state: {
                pageSize: 30
            }
        });

        var pageableCollection = new PageableCollectionExtend();

        var HtmlButton = Backgrid.Cell.extend({
            template: _.template(" <div class='span12'>" +
            "<button <%=nu_totalagendamentos ? '':'disabled' %> class='btn btn-small btn-inverse xls align-left'><i class='icon icon-white icon-resize-small'></i> Gerar XLS</button> " +
            "<button <%=nu_totalagendamentos ? '':'disabled' %> class='btn btn-small btn-custom relatorio align-left'><i class='icon icon-white icon-pencil'></i> Relatório de alunos</button> " +
            "</div> <div class='sapan12'><button <%=nu_totalagendamentos ? '':'disabled' %> class='btn btn-small btn-success lista align-left'><i class='icon icon-white icon-book'></i> Lista de Presença</button> " +
             "<button class='btn btn-small btn-warning agendamentos align-left'><i class='icon icon-white icon-list'></i> Agendamentos</button> </div>"),
            events: {
                "click .xls": "geraXls",
                "click .relatorio": "relatorioAlunos",
                "click .lista": "listaPresenca",
                "click .agendamentos" : "listaAgendamentos"
            },
            geraXls: function(e) {

                try
                {
                    that = this;
                    var i = 0;
                    for (var key in that.model.attributes) {
                        if (i == 0) {
                            that.model.id = that.model.attributes[key];
                        }
                        i++;
                    }
                    var url = '/provas-polo/gerar-xls/?id_avaliacaoaplicacao=' + that.model.get('id_avaliacaoaplicacao');
                    window.open(url, '_blank');
                    return false;
                }
                catch(err)
                {
                    return false;
                }
            },
            relatorioAlunos: function(e) {
                e.preventDefault();
                var url = '/provas-polo/gerar-lista';
                window.open(url + '/id_avaliacaoaplicacao/' + this.model.get('id_avaliacaoaplicacao'), '_blank');
            },
            listaPresenca: function(e) {
                try
                {
                    that = this;
                    var i = 0;
                    for (var key in that.model.attributes) {
                        if (i == 0) {
                            that.model.id = that.model.attributes[key];
                        }
                        i++;
                    }
                    var url = '/provas-polo/gerar-pdf/?id_avaliacaoaplicacao=' +
                        that.model.get('id_avaliacaoaplicacao') + '&id_entidade=' + that.model.get('id_entidade');
                    window.open(url, '_blank');
                    return false;
                }
                catch(err)
                {
                    return false;
                }
            },
            listaAgendamentos: function(){
                try
                {
                    /** Abre XLS com a listagem de presença dos alunos que realizaram prova nessa aplicacao**/
                    var that = this;
                    var url = '/provas-polo/gerar-xls-presenca/?id_avaliacaoaplicacao=' + that.model.get('id_avaliacaoaplicacao');
                    window.open(url, '_blank');
                    return false;
                }
                catch(err)
                {
                    return false;
                }
            },
            render: function() {
                this.$el.html(this.template(this.model.toJSON()));
                this.delegateEvents();
                return this;
            }
        });

        var columns = [
            {name: 'st_aplicadorprova', label: 'Aplicador', editable: false, cell: 'string'},
            {name: 'dt_aplicacao', label: 'Data Prova', editable: false, cell: 'string'},
            {name: 'hr_inicio', label: 'Hora Início', editable: false, cell: 'string'},
            {name: 'hr_fim', label: 'Hora Término', editable: false, cell: 'string'},
            {name: 'nu_maxaplicacao', label: 'Vagas Existentes', editable: false, cell: 'string'},
            {name: 'nu_totalagendamentos', label: 'Total Agendamentos', editable: false, cell: 'string'},
            {name: 'nu_agendamentoprovafinal', label: 'Agendamento Prova Final', editable: false, cell: 'string'},
            {name: 'nu_agendamentorecuperacao', label: 'Agendamento Recuperação', editable: false, cell: 'string'},
            {name: 'nu_vagasrestantes', label: 'Vagas Restantes', editable: false, cell: 'string'},
            {name: 'options', label: 'Ações', cell: HtmlButton, editable: false}
        ];

        var grid = new Backgrid.Grid({
            className: 'table table-bordered table-condensed backgrid',
            columns: columns,
            collection: pageableCollection,
            emptyText: 'Nenhum registro foi encontrado com os filtros informados!'
        });

        var renderGrid = $('#container-DataGrid');
        renderGrid.empty().append(grid.render().el);

        var paginator = new Backgrid.Extension.Paginator({
            collection: pageableCollection
        });

        renderGrid.append(paginator.render().el);

        pageableCollection.fetch({
            beforeSend: function(){
                loading();
            },
            reset: true,
            data: {
                id_aplicadorprova: that.ui.comboPolo.val(),
                dt_inicio: that.ui.inputDataProvaInicio.val(),
                dt_termino: that.ui.inputDataProvaTermino.val()
            },
            complete: function(){
                loaded();
            }
        });

    }
});

G2S.show(new RelatorioProvasPoloCompositeView());
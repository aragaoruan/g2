// CONFIGURACAO DE PERMISSOES
var PermissaoFuncionalidade = {
    // vw_perfilpermissoesfuncionalidade
    id_funcionalidade: 651, //Codigo da funcionalidade
    permissoes: {
        adicionar: 1, deletar: 2, atualizar: 3 //Codigos das permissoes da tabela permissao
    }
};

var permissaoRemover = VerificarPerfilPermissao(PermissaoFuncionalidade.permissoes.deletar, PermissaoFuncionalidade.id_funcionalidade);

var TitulacaoModel = Backbone.Model.extend({
    idAttribute: 'id_titulacao',
    defaults: {
        id_titulacao: null,
        st_titulacao: ''
    }
});

var TitulacaoCollectionExtend = Backbone.Collection.extend({
    url: '/default/titular-certificacao/get-titulacao/',
    model: TitulacaoModel
});
var TitulacaoCollection = new TitulacaoCollectionExtend();
TitulacaoCollection.fetch();

var SearchView = Marionette.ItemView.extend({
    template: '#search-view',
    tagName: 'div',
    usuario: {},
    onShow: function() {
        this.$el.find('input').first().focus();
    },
    onRender: function() {
        var that = this;
        var elem = this.$el;

        // Montar SELECT "Área"
        var AreaModel = Backbone.Model.extend({
            idAttribute: 'id_areaconhecimento',
            defaults: {
                id_areaconhecimento: null,
                st_areaconhecimento: ''
            }
        });
        ComboboxView({
            el: elem.find('[name="id_areaconhecimento"]'),
            model: AreaModel,
            url: '/api/area-conhecimento/',
            optionLabel: 'st_areaconhecimento',
            emptyOption: '[Selecione]',
            parse: function(response) {
                if (!response.length) {
                    $.pnotify({
                        title: 'Aviso',
                        type: 'warning',
                        text: 'Nenhuma área encontrada'
                    });
                }
                return response;
            }
        });
    },
    events: {
        'submit form': 'actionSearch',
        'click .actionDisciplina': 'actionDisciplina',
        'change [name="id_areaconhecimento"]': 'refreshProjeto',
        // Eventos autocomplete
        'keyup [name="autocomplete"]': 'autocompleteSearch',
        'keydown [name="autocomplete"]': 'autocompletePrevent',
        'change [name="id_usuario"]': 'autocompleteChange',
        'blur [name="autocomplete"]': 'autocompleteHide'
    },
    actionSearch: function(e) {
        e.preventDefault();

        var that = this;
        var elem = this.$el;

        var id_usuario = Number(elem.find('[name="id_usuario"]').val());
        var id_areaconhecimento = Number(elem.find('[name="id_areaconhecimento"]').val());
        var id_projetopedagogico = Number(elem.find('[name="id_projetopedagogico"]').val());

        if (!id_usuario && !id_areaconhecimento && !id_projetopedagogico) {
            $.pnotify({
                type: 'warning',
                title: 'Aviso',
                text: 'Selecione um filtro para realizar a pesquisa.'
            });
            return false;
        }

        var ResultsIni = new ResultsComposite();
        G2S.layout.disciplina.reset();
        G2S.layout.results.reset();
        G2S.layout.results.show(ResultsIni);
    },
    actionDisciplina: function() {
        var that = this;

        // Verifica se selecionou o projeto para buscar as disciplinas
        if (!Number($('[name="id_projetopedagogico"]').val())) {
            $.pnotify({
                type: 'warning',
                title: 'Aviso',
                text: 'Selecione um projeto pedagógico para buscar disciplinas.'
            });
            return false;
        }

        //Verifica usuario para trazer titulação default do mesmo no combo
        if(!Number(this.$el.find('[name="id_usuario"]').val())){
            $.pnotify({
                type: 'warning',
                title: 'Aviso',
                text: 'Selecione um usuário para adicioná-lo como titular.'
            });
            return false;
        }

        // Faz o fetch do usuario pra pegar a Especializacao
        $.ajaxSetup({ async: false });
        $.getJSON('/api/usuario/' + this.$el.find('[name="id_usuario"]').val(), function(response) {
            that.usuario = response;
        });
        $.ajaxSetup({ async: true });

        G2S.layout.results.reset();
        G2S.layout.disciplina.reset();
        G2S.layout.disciplina.show(new DisciplinaComposite());
    },
    refreshProjeto: function() {
        var elem = this.$el;

        // Montar SELECT "Projeto Pedagógico"
        var ProjetoModel = Backbone.Model.extend({
            idAttribute: 'id_projetopedagogico',
            defaults: {
                id_projetopedagogico: null,
                st_projetopedagogico: ''
            }
        });
        ComboboxView({
            el: elem.find('[name="id_projetopedagogico"]'),
            model: ProjetoModel,
            url: '/titular-certificacao/get-projeto-by-area/?id_areaconhecimento=' + elem.find('[name="id_areaconhecimento"]').val(),
            optionLabel: 'st_projetopedagogico',
            emptyOption: '[Selecione]'
        });
    },
    // AUTOCOMPLETE
    autocompleteRequest: false,
    autocompleteStr: '',
    autocompleteSearch: function(e) {
        var elem = $(e.target).closest('.autocomplete');
        var combobox = elem.find('select');
        var input = elem.find('input');
        var value = input.val().trim();

        /**
         * KeyCode 48 até 90 = 0 (zero) até Z (letra Z)
         * KeyCode 8 = backspace (apagar)
         * KeyCode 46 = delete (del)
         */
        if (input.val() == '') {
            combobox.html('').hide();
        } else if (((e.keyCode >= 48 && e.keyCode <= 90) || (e.keyCode == 8 || e.keyCode == 46)) && value != this.autocompleteStr && value.length >= 7) {
            // Se as teclas forem alfanumericas realizar a busca

            // Abortar/Cancelar busca do autocomplete anterior
            if (this.autocompleteRequest)
                this.autocompleteRequest.abort();

            elem.find('img').removeClass('hide');

            /**
             * Parametro id_perfilpedagogico = 9
             * Define o perfil "Titular de Certificação"
             */
            var url = '/api/vw-usuario-perfil-pedagogico?id_perfilpedagogico=9&search[st_nomecompleto]=' + value;
            this.autocompleteRequest = $.get(url, function(response) {
                elem.find('img').addClass('hide');
                combobox.children().not(':selected').remove();
                if (response) {
                    combobox.show();
                    for (var i in response) {
                        var obj = response[i];

                        if (!combobox.find('[value="' + obj.id_usuario + '"]').length)
                            combobox.append('<option value="' + obj.id_usuario + '" data-id-perfil="' + obj.id_perfil + '">' + obj.st_nomecompleto + '</option>');
                    }
                }

            });
        }

        this.autocompleteStr = value;
    },
    autocompleteChange: function(e) {
        var elem = $(e.target).closest('.autocomplete');
        var combobox = elem.find('select');
        var input = elem.find('input');

        var selected = combobox.find('option:selected');

        if (this.autocompleteRequest)
            this.autocompleteRequest.abort();
        elem.find('img').addClass('hide');
        input.val(selected.text()).focus();
        combobox.children().not(':selected').remove();
        combobox.hide();
    },
    autocompletePrevent: function(e) {
        var elem = $(e.target).closest('.autocomplete');
        var combobox = elem.find('select');
        var input = elem.find('input');
        var value = input.val().trim();

        /**
         * KeyCode 8 = backspace (apagar)
         * KeyCode 46 = delete (del)
         * KeyCode 38/40 = seta pra cima/baixo
         * KeyCode 9 = TAB
         */
        if ((e.keyCode == 8 || e.keyCode == 46) && ((combobox.find('option:selected').length && combobox.find('option:selected').val()) || value == '')) {
            // Se usar tecla BACKSPACE ou DELETE
            // e tiver um item selecionado ou o input for VAZIO,
            // desmarcar este item, e limpar o input
            input.val('');
            combobox.html('').hide();
        } else if ((e.keyCode == 38 || e.keyCode == 40 || e.keyCode == 9) && combobox.is(':visible')) {
            e.preventDefault();
            // Se usar setas do teclado par cima/baixo e o <select> estiver visivel, navegar pelas opções do mesmo
            var focused = combobox.find('option:selected');

            if (e.keyCode == 38) {
                focused = focused.prev();
                if (!focused.length)
                    focused = combobox.find('option:last');
            } else {
                focused = focused.next();
                if (!focused.length)
                    focused = combobox.find('option:first');
            }

            combobox.find('option').prop('selected', false);
            focused.prop('selected', true);
            input.val(focused.text());
        } else if (e.keyCode == 13 && combobox.is(':visible')) {
            // Se apertar a tecla enter, selecionar o item
            combobox.trigger('change');
            e.preventDefault();
        }
    },
    autocompleteHide: function(e) {
        var elem = $(e.target).closest('.autocomplete');
        var combobox = elem.find('select');
        var input = elem.find('input');

        setTimeout(function() {
            input.val(combobox.find('option:selected').text());
            combobox.hide();
        }, 200);
    }
});



G2S.layout.addRegions({
    search: '#search-content',
    results: '#results-content',
    disciplina: '#disciplina-content'
});
var SearchIni = new SearchView();
G2S.layout.search.show(SearchIni);
loaded();




/**
 * LISTAR DISCIPLINAS
 */
// MODEL
var DisciplinaModel = Backbone.Model.extend({
    idAttribute: 'id_disciplina',
    defaults: {
        id_disciplina: 0,
        st_disciplina: '',
        dt_inicio: null,
        dt_fim: null,

        id_usuario: 0,
        id_perfil: 0,
        id_areaconhecimento: 0,
        id_projetopedagogico: 0,
        bl_titular: 1,
        id_titulacao: 0,
        st_titulacao: '',
        actionSave: false
    },
    url: function() {
        return '/api/titular-certificacao/';
    }
});

// ITEM VIEW
var DisciplinaView = Marionette.ItemView.extend({
    model: new DisciplinaModel(),
    template: '#disciplina-view',
    tagName: 'tr',
    className: 'result-item',
    events: {
        'change [name="id_disciplina"]': 'toggleEdit',
        'blur input[type="text"]': 'updateAttributes',
        'change input[type="text"]': 'updateAttributes',
        'change select': 'updateAttributes',
        'change [name="id_titulacao"]': 'updateAttributes'
    },
    onRender: function (){
        var elem = this.$el;

        /*
         *  verificando se o usuario carregado para adicionar ja conte id_titulacao na tb_usuario
         *  caso tenha esta titulacao dele sera setada no combo de titulacao ao escolher a disciplina
         *  para adicionar ao periodo.
         */
        var idTitulacao = SearchIni.usuario.id_titulacao ?
            SearchIni.usuario.id_titulacao.id_titulacao : false;

        ComboboxView({
            el: elem.find('[name="id_titulacao"]'),
            model: TitulacaoModel,
            optionLabel: 'st_titulacao',
            emptyOption: '[Selecione]',
            collection: TitulacaoCollection.toJSON(),
            optionSelectedId: (idTitulacao)
        });
    },
    toggleEdit: function(e) {
        var elem = this.$el;
        var checked = $(e.target).is(':checked');

        this.model.set('actionSave', checked);

        if (checked) {
            elem.find('[name="dt_inicio"], [name="dt_fim"], [name="id_titulacao"]').show();
        } else {
            elem.find('[name="dt_inicio"], [name="dt_fim"], [name="id_titulacao"]').hide();
        }
    },
    updateAttributes: function(e) {
        var elem = this.$el;
        var input = $(e.target);

        this.model.set(input.attr('name'), input.val());

        // Validar se data final é maior que inicial
        var dt_inicio = Number((new String(this.model.get('dt_inicio'))).substr(0, 10).split('/').reverse().join(''));
        var dt_fim = Number((new String(this.model.get('dt_fim'))).substr(0, 10).split('/').reverse().join(''));
        if (dt_fim && dt_fim < dt_inicio) {
            $.pnotify({
                type: 'warning',
                title: 'Aviso',
                text: 'A Data de Término não pode ser anterior à Data de Início.'
            });
            elem.find('input:last').val('');
        }
    }
});

var DisciplinaEmpty = Marionette.ItemView.extend({
    template: _.template('<td colspan="5">Nenhuma disciplina encontrada.</td>'),
    tagName: 'tr'
});

// COLLECTION
var DisciplinaCollection = Backbone.Collection.extend({
    model: DisciplinaModel,
    url: '/api/vw-disciplina-perfil-pedagogico-entidade/'
});

// COMPOSITE VIEW
var DisciplinaComposite = Marionette.CompositeView.extend({
    template: '#disciplina-template',
    collection: new DisciplinaCollection(),
    childView: DisciplinaView,
    emptyView: DisciplinaEmpty,
    childViewContainer: 'tbody',
    initialize: function() {
        loading();
        this.collection.url = '/api/vw-disciplina-perfil-pedagogico-entidade/?id_perfilpedagogico=9&id_projetopedagogico=' + $('[name="id_projetopedagogico"]').val();
        this.collection.fetch().done(loaded);
    },
    events: {
        'submit form': 'actionSave',
        'click .actionCancel': 'actionCancel',
        'keyup [name="disciplina_search"]': 'filterDisciplinas'
    },
    actionSave: function(e) {
        e.preventDefault();

        var elem = this.$el;

        var id_usuario = Number($('[name="id_usuario"]').val());
        var id_areaconhecimento = Number($('[name="id_areaconhecimento"]').val());
        var id_projetopedagogico = Number($('[name="id_projetopedagogico"]').val());

        if (!id_usuario || !id_areaconhecimento || !id_projetopedagogico) {
            $.pnotify({
                type: 'error',
                title: 'Erro',
                text: 'Para salvar as disciplinas, é necessário informar o Titular, a Área e o Projeto Pedagógico'
            });
            return false;
        }

        loading();
        $.ajaxSetup({ async: false });

        var successCount = 0;
        var errorCount = 0;

        var disciplinas = this.collection.models;

        // Salvar cada disciplina
        for (var i in disciplinas) {
            var obj = disciplinas[i];
            var view = elem.find('tbody tr').eq(i);

            if (obj.get('actionSave')) {
                obj.set({
                    id_usuario: id_usuario,
                    id_areaconhecimento: id_areaconhecimento,
                    id_projetopedagogico: id_projetopedagogico,
                    id_titulacao: Number(view.find('[name="id_titulacao"]').val()),
                    id_perfil: Number($('[name="id_usuario"] option:selected').data('id-perfil'))
                });
                obj.save(null, {
                    success: function(model, response) {
                        view.removeClass('error').addClass('success')
                            .find('.disciplina-error').css('color', 'green').html('Sucesso: ' + response.text).end()
                            .find('input[type="checkbox"]').prop('checked', false).trigger('change');
                        successCount++;
                    },
                    error: function(model, response) {
                        response = $.parseJSON(response.responseText);
                        view.removeClass('success').addClass('error')
                            .find('.disciplina-error').css('color', 'red').html("Erro: " + response.text);
                        errorCount++;
                    }
                });
            }
        }

        loaded();

        if (errorCount && successCount) {
            $.pnotify({
                type: 'warning',
                title: 'Aviso',
                text: 'Ação concluída. Verifique os registros que não puderam ser salvos.'
            });
            loaded();
            return false;
        } else if (errorCount) {
            $.pnotify({
                type: 'error',
                title: 'Erro',
                text: 'As disciplinas não puderam ser referenciadas.'
            });
            loaded();
            return false;
        } else {
            $.pnotify({
                type: 'success',
                title: 'Concluído',
                text: 'As disciplinas foram referenciadas com sucesso.'
            });
        }

        $.ajaxSetup({ async: true });
        loaded();

        var ResultsIni = new ResultsComposite();
        G2S.layout.disciplina.reset();
        G2S.layout.results.reset();
        G2S.layout.results.show(ResultsIni);

    },
    actionCancel: function() {
        G2S.layout.disciplina.reset();
    },
    filterDisciplinas: function(e) {
        var elem = this.$el;
        var input = $(e.target);
        var value = input.val();

        var items = elem.find('.result-item');
        items.each(function() {
            var user = $(this);
            if (!user.attr('data-label')) {
                user.attr('data-label', user.find('td:first + td').text().removeAccents().trim().toLowerCase());
            }
        });

        if (value.trim() === '') {
            items.show();
        } else {
            var toShow = items.filter('[data-label^="' + value.removeAccents().trim().toLowerCase() + '"]');
            items.not(toShow).hide();
            toShow.show();
        }
    }
});


/**
 * LISTAR RESULTADOS DA BUSCA
 */
var ResultsModel = Backbone.Model.extend({
    idAttribute: 'id_perfilreferencia',
    defaults: {
        id_perfilreferencia: 0,
        id_usuario: {
            id_usuario: 0,
            st_nomecompleto: ''
        },
        id_areaconhecimento: {
            id_areaconhecimento: 0,
            st_areaconhecimento: ''
        },
        id_projetopedagogico: {
            id_projetopedagogico: 0,
            st_projetopedagogico: ''
        },
        dt_inicio: '',
        dt_fim: '',
        st_situacao: '',
        id_titulacao: {
            id_titulacao: 0,
            st_titulacao: ''
        },
        bl_ativo: 0
    },
    url: function() {
        return '/api/titular-certificacao/' + (this.id || '');
    }
});
var ResultsView = Marionette.ItemView.extend({
    model: new ResultsModel(),
    template: '#results-view',
    tagName: 'tr',
    onBeforeRender: function() {
        var dt_now = Number((new Date()).toISOString().substr(0, 10).split('-').join(''));
        var dt_fim = Number((new String(this.model.get('dt_fim'))).substr(0, 10).split('/').reverse().join(''));

        if (this.model.get('dt_fim') && dt_fim <= dt_now)
            this.model.set('st_situacao', 'Expirado');
        else
            this.model.set('st_situacao', 'Habilitado');
    },
    onRender: function() {
        var elem = this.$el;

        if (this.model.get('bl_ativo') == 0)
            elem.addClass('error');
        else if (this.model.get('st_situacao') == 'Expirado')
            elem.addClass('warning');
        else
            elem.removeClass('warning error');

        // Se nao tiver permissoes para remover o registro, remover os botoes de remover.
        if (!permissaoRemover.length) {
            this.$el.find('.actionRemove').remove();
        }
    },
    events: {
        'click .actionEdit': 'actionEdit',
        'click .actionSave': 'actionSave',
        'click .actionRemove' : 'actionRemove'
    },
    isEditing: false,
    actionEdit: function() {
        this.template = this.isEditing ? '#results-view' : '#results-edit';
        this.render();
        this.isEditing = !this.isEditing;
    },
    actionSave: function() {
        var that = this;
        var elem = this.$el;

        this.model.set({
            dt_fim: elem.find('[name="dt_fim"]').val() || null
        });

        // Validar se data final é maior que inicial
        var dt_inicio = Number((new String(this.model.get('dt_inicio'))).substr(0, 10).split('/').reverse().join(''));
        var dt_fim = Number((new String(this.model.get('dt_fim'))).substr(0, 10).split('/').reverse().join(''));
        if (dt_fim && dt_fim < dt_inicio) {
            $.pnotify({
                type: 'error',
                title: 'Erro',
                text: 'A Data de Término não pode ser anterior à Data de Início.'
            });
            elem.find('input:last').val('');
            return false;
        }

        loading();
        this.model.save(null, {
            success: function(model, response) {
                that.actionEdit();
            },
            complete: function(response) {
                response = $.parseJSON(response.responseText);
                $.pnotify(response);
                loaded();
            }
        });
    },
    actionRemove: function() {
        var that = this;
        bootbox.confirm("Deseja realmente desativar este registro?", function(response) {
            if (response) {
                loading();
                that.model.save(null, {
                    method: 'delete',
                    silent: true,
                    wait: true,
                    success: function(model, response) {
                        //var dt_fim = (new String(response.mensagem.dt_fim)).substr(0, 10).split('-').reverse().join('/');
                        //that.model.set('dt_fim', dt_fim);
                        //that.model.set('bl_ativo', 0);
                        //that.render();
                        that.remove();
                    },
                    complete: function(response) {
                        response = $.parseJSON(response.responseText);
                        $.pnotify(response);
                        loaded();
                    }
                });
            }
        });
    }
});

// COLLECTION
var ResultsCollection = Backbone.Collection.extend({
    model: ResultsModel,
    url: '/api/titular-certificacao/'
});

// EMPTY ITEM VIEW
var ResultsEmpty = Marionette.ItemView.extend({
    template: _.template('<td colspan="9">Nenhum registro encontrado.</td>'),
    tagName: 'tr'
});

// COMPOSITE VIEW
var ResultsComposite = Marionette.CompositeView.extend({
    template: '#results-template',
    collection: new ResultsCollection(),
    childView: ResultsView,
    emptyView: ResultsEmpty,
    childViewContainer: 'tbody',
    initialize: function() {
        loading();

        var id_usuario = Number($('[name="id_usuario"]').val());
        var id_areaconhecimento = Number($('[name="id_areaconhecimento"]').val());
        var id_projetopedagogico = Number($('[name="id_projetopedagogico"]').val());

        var data = ['bl_titular=1', 'bl_ativo=1'];
        if (id_usuario)
            data.push('id_usuario=' + id_usuario);
        if (id_areaconhecimento)
            data.push('id_areaconhecimento=' + id_areaconhecimento);
        if (id_projetopedagogico)
            data.push('id_projetopedagogico=' + id_projetopedagogico);

        this.collection.url = '/api/titular-certificacao/?' + data.join('&');
        this.collection.fetch({reset: true}).done(loaded);
    }
});


/**
 * Prototipo (str.removeAccents())
 * usado para remover acentos de strings
 */
//Prototipos para remover acentos de string
var Latinise = {};
Latinise.latin_map = {"Á": "A", "Ă": "A", "Ắ": "A", "Ặ": "A", "Ằ": "A", "Ẳ": "A", "Ẵ": "A", "Ǎ": "A", "Â": "A", "Ấ": "A", "Ậ": "A", "Ầ": "A", "Ẩ": "A", "Ẫ": "A", "Ä": "A", "Ǟ": "A", "Ȧ": "A", "Ǡ": "A", "Ạ": "A", "Ȁ": "A", "À": "A", "Ả": "A", "Ȃ": "A", "Ā": "A", "Ą": "A", "Å": "A", "Ǻ": "A", "Ḁ": "A", "Ⱥ": "A", "Ã": "A", "Ꜳ": "AA", "Æ": "AE", "Ǽ": "AE", "Ǣ": "AE", "Ꜵ": "AO", "Ꜷ": "AU", "Ꜹ": "AV", "Ꜻ": "AV", "Ꜽ": "AY", "Ḃ": "B", "Ḅ": "B", "Ɓ": "B", "Ḇ": "B", "Ƀ": "B", "Ƃ": "B", "Ć": "C", "Č": "C", "Ç": "C", "Ḉ": "C", "Ĉ": "C", "Ċ": "C", "Ƈ": "C", "Ȼ": "C", "Ď": "D", "Ḑ": "D", "Ḓ": "D", "Ḋ": "D", "Ḍ": "D", "Ɗ": "D", "Ḏ": "D", "ǲ": "D", "ǅ": "D", "Đ": "D", "Ƌ": "D", "Ǳ": "DZ", "Ǆ": "DZ", "É": "E", "Ĕ": "E", "Ě": "E", "Ȩ": "E", "Ḝ": "E", "Ê": "E", "Ế": "E", "Ệ": "E", "Ề": "E", "Ể": "E", "Ễ": "E", "Ḙ": "E", "Ë": "E", "Ė": "E", "Ẹ": "E", "Ȅ": "E", "È": "E", "Ẻ": "E", "Ȇ": "E", "Ē": "E", "Ḗ": "E", "Ḕ": "E", "Ę": "E", "Ɇ": "E", "Ẽ": "E", "Ḛ": "E", "Ꝫ": "ET", "Ḟ": "F", "Ƒ": "F", "Ǵ": "G", "Ğ": "G", "Ǧ": "G", "Ģ": "G", "Ĝ": "G", "Ġ": "G", "Ɠ": "G", "Ḡ": "G", "Ǥ": "G", "Ḫ": "H", "Ȟ": "H", "Ḩ": "H", "Ĥ": "H", "Ⱨ": "H", "Ḧ": "H", "Ḣ": "H", "Ḥ": "H", "Ħ": "H", "Í": "I", "Ĭ": "I", "Ǐ": "I", "Î": "I", "Ï": "I", "Ḯ": "I", "İ": "I", "Ị": "I", "Ȉ": "I", "Ì": "I", "Ỉ": "I", "Ȋ": "I", "Ī": "I", "Į": "I", "Ɨ": "I", "Ĩ": "I", "Ḭ": "I", "Ꝺ": "D", "Ꝼ": "F", "Ᵹ": "G", "Ꞃ": "R", "Ꞅ": "S", "Ꞇ": "T", "Ꝭ": "IS", "Ĵ": "J", "Ɉ": "J", "Ḱ": "K", "Ǩ": "K", "Ķ": "K", "Ⱪ": "K", "Ꝃ": "K", "Ḳ": "K", "Ƙ": "K", "Ḵ": "K", "Ꝁ": "K", "Ꝅ": "K", "Ĺ": "L", "Ƚ": "L", "Ľ": "L", "Ļ": "L", "Ḽ": "L", "Ḷ": "L", "Ḹ": "L", "Ⱡ": "L", "Ꝉ": "L", "Ḻ": "L", "Ŀ": "L", "Ɫ": "L", "ǈ": "L", "Ł": "L", "Ǉ": "LJ", "Ḿ": "M", "Ṁ": "M", "Ṃ": "M", "Ɱ": "M", "Ń": "N", "Ň": "N", "Ņ": "N", "Ṋ": "N", "Ṅ": "N", "Ṇ": "N", "Ǹ": "N", "Ɲ": "N", "Ṉ": "N", "Ƞ": "N", "ǋ": "N", "Ñ": "N", "Ǌ": "NJ", "Ó": "O", "Ŏ": "O", "Ǒ": "O", "Ô": "O", "Ố": "O", "Ộ": "O", "Ồ": "O", "Ổ": "O", "Ỗ": "O", "Ö": "O", "Ȫ": "O", "Ȯ": "O", "Ȱ": "O", "Ọ": "O", "Ő": "O", "Ȍ": "O", "Ò": "O", "Ỏ": "O", "Ơ": "O", "Ớ": "O", "Ợ": "O", "Ờ": "O", "Ở": "O", "Ỡ": "O", "Ȏ": "O", "Ꝋ": "O", "Ꝍ": "O", "Ō": "O", "Ṓ": "O", "Ṑ": "O", "Ɵ": "O", "Ǫ": "O", "Ǭ": "O", "Ø": "O", "Ǿ": "O", "Õ": "O", "Ṍ": "O", "Ṏ": "O", "Ȭ": "O", "Ƣ": "OI", "Ꝏ": "OO", "Ɛ": "E", "Ɔ": "O", "Ȣ": "OU", "Ṕ": "P", "Ṗ": "P", "Ꝓ": "P", "Ƥ": "P", "Ꝕ": "P", "Ᵽ": "P", "Ꝑ": "P", "Ꝙ": "Q", "Ꝗ": "Q", "Ŕ": "R", "Ř": "R", "Ŗ": "R", "Ṙ": "R", "Ṛ": "R", "Ṝ": "R", "Ȑ": "R", "Ȓ": "R", "Ṟ": "R", "Ɍ": "R", "Ɽ": "R", "Ꜿ": "C", "Ǝ": "E", "Ś": "S", "Ṥ": "S", "Š": "S", "Ṧ": "S", "Ş": "S", "Ŝ": "S", "Ș": "S", "Ṡ": "S", "Ṣ": "S", "Ṩ": "S", "Ť": "T", "Ţ": "T", "Ṱ": "T", "Ț": "T", "Ⱦ": "T", "Ṫ": "T", "Ṭ": "T", "Ƭ": "T", "Ṯ": "T", "Ʈ": "T", "Ŧ": "T", "Ɐ": "A", "Ꞁ": "L", "Ɯ": "M", "Ʌ": "V", "Ꜩ": "TZ", "Ú": "U", "Ŭ": "U", "Ǔ": "U", "Û": "U", "Ṷ": "U", "Ü": "U", "Ǘ": "U", "Ǚ": "U", "Ǜ": "U", "Ǖ": "U", "Ṳ": "U", "Ụ": "U", "Ű": "U", "Ȕ": "U", "Ù": "U", "Ủ": "U", "Ư": "U", "Ứ": "U", "Ự": "U", "Ừ": "U", "Ử": "U", "Ữ": "U", "Ȗ": "U", "Ū": "U", "Ṻ": "U", "Ų": "U", "Ů": "U", "Ũ": "U", "Ṹ": "U", "Ṵ": "U", "Ꝟ": "V", "Ṿ": "V", "Ʋ": "V", "Ṽ": "V", "Ꝡ": "VY", "Ẃ": "W", "Ŵ": "W", "Ẅ": "W", "Ẇ": "W", "Ẉ": "W", "Ẁ": "W", "Ⱳ": "W", "Ẍ": "X", "Ẋ": "X", "Ý": "Y", "Ŷ": "Y", "Ÿ": "Y", "Ẏ": "Y", "Ỵ": "Y", "Ỳ": "Y", "Ƴ": "Y", "Ỷ": "Y", "Ỿ": "Y", "Ȳ": "Y", "Ɏ": "Y", "Ỹ": "Y", "Ź": "Z", "Ž": "Z", "Ẑ": "Z", "Ⱬ": "Z", "Ż": "Z", "Ẓ": "Z", "Ȥ": "Z", "Ẕ": "Z", "Ƶ": "Z", "Ĳ": "IJ", "Œ": "OE", "ᴀ": "A", "ᴁ": "AE", "ʙ": "B", "ᴃ": "B", "ᴄ": "C", "ᴅ": "D", "ᴇ": "E", "ꜰ": "F", "ɢ": "G", "ʛ": "G", "ʜ": "H", "ɪ": "I", "ʁ": "R", "ᴊ": "J", "ᴋ": "K", "ʟ": "L", "ᴌ": "L", "ᴍ": "M", "ɴ": "N", "ᴏ": "O", "ɶ": "OE", "ᴐ": "O", "ᴕ": "OU", "ᴘ": "P", "ʀ": "R", "ᴎ": "N", "ᴙ": "R", "ꜱ": "S", "ᴛ": "T", "ⱻ": "E", "ᴚ": "R", "ᴜ": "U", "ᴠ": "V", "ᴡ": "W", "ʏ": "Y", "ᴢ": "Z", "á": "a", "ă": "a", "ắ": "a", "ặ": "a", "ằ": "a", "ẳ": "a", "ẵ": "a", "ǎ": "a", "â": "a", "ấ": "a", "ậ": "a", "ầ": "a", "ẩ": "a", "ẫ": "a", "ä": "a", "ǟ": "a", "ȧ": "a", "ǡ": "a", "ạ": "a", "ȁ": "a", "à": "a", "ả": "a", "ȃ": "a", "ā": "a", "ą": "a", "ᶏ": "a", "ẚ": "a", "å": "a", "ǻ": "a", "ḁ": "a", "ⱥ": "a", "ã": "a", "ꜳ": "aa", "æ": "ae", "ǽ": "ae", "ǣ": "ae", "ꜵ": "ao", "ꜷ": "au", "ꜹ": "av", "ꜻ": "av", "ꜽ": "ay", "ḃ": "b", "ḅ": "b", "ɓ": "b", "ḇ": "b", "ᵬ": "b", "ᶀ": "b", "ƀ": "b", "ƃ": "b", "ɵ": "o", "ć": "c", "č": "c", "ç": "c", "ḉ": "c", "ĉ": "c", "ɕ": "c", "ċ": "c", "ƈ": "c", "ȼ": "c", "ď": "d", "ḑ": "d", "ḓ": "d", "ȡ": "d", "ḋ": "d", "ḍ": "d", "ɗ": "d", "ᶑ": "d", "ḏ": "d", "ᵭ": "d", "ᶁ": "d", "đ": "d", "ɖ": "d", "ƌ": "d", "ı": "i", "ȷ": "j", "ɟ": "j", "ʄ": "j", "ǳ": "dz", "ǆ": "dz", "é": "e", "ĕ": "e", "ě": "e", "ȩ": "e", "ḝ": "e", "ê": "e", "ế": "e", "ệ": "e", "ề": "e", "ể": "e", "ễ": "e", "ḙ": "e", "ë": "e", "ė": "e", "ẹ": "e", "ȅ": "e", "è": "e", "ẻ": "e", "ȇ": "e", "ē": "e", "ḗ": "e", "ḕ": "e", "ⱸ": "e", "ę": "e", "ᶒ": "e", "ɇ": "e", "ẽ": "e", "ḛ": "e", "ꝫ": "et", "ḟ": "f", "ƒ": "f", "ᵮ": "f", "ᶂ": "f", "ǵ": "g", "ğ": "g", "ǧ": "g", "ģ": "g", "ĝ": "g", "ġ": "g", "ɠ": "g", "ḡ": "g", "ᶃ": "g", "ǥ": "g", "ḫ": "h", "ȟ": "h", "ḩ": "h", "ĥ": "h", "ⱨ": "h", "ḧ": "h", "ḣ": "h", "ḥ": "h", "ɦ": "h", "ẖ": "h", "ħ": "h", "ƕ": "hv", "í": "i", "ĭ": "i", "ǐ": "i", "î": "i", "ï": "i", "ḯ": "i", "ị": "i", "ȉ": "i", "ì": "i", "ỉ": "i", "ȋ": "i", "ī": "i", "į": "i", "ᶖ": "i", "ɨ": "i", "ĩ": "i", "ḭ": "i", "ꝺ": "d", "ꝼ": "f", "ᵹ": "g", "ꞃ": "r", "ꞅ": "s", "ꞇ": "t", "ꝭ": "is", "ǰ": "j", "ĵ": "j", "ʝ": "j", "ɉ": "j", "ḱ": "k", "ǩ": "k", "ķ": "k", "ⱪ": "k", "ꝃ": "k", "ḳ": "k", "ƙ": "k", "ḵ": "k", "ᶄ": "k", "ꝁ": "k", "ꝅ": "k", "ĺ": "l", "ƚ": "l", "ɬ": "l", "ľ": "l", "ļ": "l", "ḽ": "l", "ȴ": "l", "ḷ": "l", "ḹ": "l", "ⱡ": "l", "ꝉ": "l", "ḻ": "l", "ŀ": "l", "ɫ": "l", "ᶅ": "l", "ɭ": "l", "ł": "l", "ǉ": "lj", "ſ": "s", "ẜ": "s", "ẛ": "s", "ẝ": "s", "ḿ": "m", "ṁ": "m", "ṃ": "m", "ɱ": "m", "ᵯ": "m", "ᶆ": "m", "ń": "n", "ň": "n", "ņ": "n", "ṋ": "n", "ȵ": "n", "ṅ": "n", "ṇ": "n", "ǹ": "n", "ɲ": "n", "ṉ": "n", "ƞ": "n", "ᵰ": "n", "ᶇ": "n", "ɳ": "n", "ñ": "n", "ǌ": "nj", "ó": "o", "ŏ": "o", "ǒ": "o", "ô": "o", "ố": "o", "ộ": "o", "ồ": "o", "ổ": "o", "ỗ": "o", "ö": "o", "ȫ": "o", "ȯ": "o", "ȱ": "o", "ọ": "o", "ő": "o", "ȍ": "o", "ò": "o", "ỏ": "o", "ơ": "o", "ớ": "o", "ợ": "o", "ờ": "o", "ở": "o", "ỡ": "o", "ȏ": "o", "ꝋ": "o", "ꝍ": "o", "ⱺ": "o", "ō": "o", "ṓ": "o", "ṑ": "o", "ǫ": "o", "ǭ": "o", "ø": "o", "ǿ": "o", "õ": "o", "ṍ": "o", "ṏ": "o", "ȭ": "o", "ƣ": "oi", "ꝏ": "oo", "ɛ": "e", "ᶓ": "e", "ɔ": "o", "ᶗ": "o", "ȣ": "ou", "ṕ": "p", "ṗ": "p", "ꝓ": "p", "ƥ": "p", "ᵱ": "p", "ᶈ": "p", "ꝕ": "p", "ᵽ": "p", "ꝑ": "p", "ꝙ": "q", "ʠ": "q", "ɋ": "q", "ꝗ": "q", "ŕ": "r", "ř": "r", "ŗ": "r", "ṙ": "r", "ṛ": "r", "ṝ": "r", "ȑ": "r", "ɾ": "r", "ᵳ": "r", "ȓ": "r", "ṟ": "r", "ɼ": "r", "ᵲ": "r", "ᶉ": "r", "ɍ": "r", "ɽ": "r", "ↄ": "c", "ꜿ": "c", "ɘ": "e", "ɿ": "r", "ś": "s", "ṥ": "s", "š": "s", "ṧ": "s", "ş": "s", "ŝ": "s", "ș": "s", "ṡ": "s", "ṣ": "s", "ṩ": "s", "ʂ": "s", "ᵴ": "s", "ᶊ": "s", "ȿ": "s", "ɡ": "g", "ᴑ": "o", "ᴓ": "o", "ᴝ": "u", "ť": "t", "ţ": "t", "ṱ": "t", "ț": "t", "ȶ": "t", "ẗ": "t", "ⱦ": "t", "ṫ": "t", "ṭ": "t", "ƭ": "t", "ṯ": "t", "ᵵ": "t", "ƫ": "t", "ʈ": "t", "ŧ": "t", "ᵺ": "th", "ɐ": "a", "ᴂ": "ae", "ǝ": "e", "ᵷ": "g", "ɥ": "h", "ʮ": "h", "ʯ": "h", "ᴉ": "i", "ʞ": "k", "ꞁ": "l", "ɯ": "m", "ɰ": "m", "ᴔ": "oe", "ɹ": "r", "ɻ": "r", "ɺ": "r", "ⱹ": "r", "ʇ": "t", "ʌ": "v", "ʍ": "w", "ʎ": "y", "ꜩ": "tz", "ú": "u", "ŭ": "u", "ǔ": "u", "û": "u", "ṷ": "u", "ü": "u", "ǘ": "u", "ǚ": "u", "ǜ": "u", "ǖ": "u", "ṳ": "u", "ụ": "u", "ű": "u", "ȕ": "u", "ù": "u", "ủ": "u", "ư": "u", "ứ": "u", "ự": "u", "ừ": "u", "ử": "u", "ữ": "u", "ȗ": "u", "ū": "u", "ṻ": "u", "ų": "u", "ᶙ": "u", "ů": "u", "ũ": "u", "ṹ": "u", "ṵ": "u", "ᵫ": "ue", "ꝸ": "um", "ⱴ": "v", "ꝟ": "v", "ṿ": "v", "ʋ": "v", "ᶌ": "v", "ⱱ": "v", "ṽ": "v", "ꝡ": "vy", "ẃ": "w", "ŵ": "w", "ẅ": "w", "ẇ": "w", "ẉ": "w", "ẁ": "w", "ⱳ": "w", "ẘ": "w", "ẍ": "x", "ẋ": "x", "ᶍ": "x", "ý": "y", "ŷ": "y", "ÿ": "y", "ẏ": "y", "ỵ": "y", "ỳ": "y", "ƴ": "y", "ỷ": "y", "ỿ": "y", "ȳ": "y", "ẙ": "y", "ɏ": "y", "ỹ": "y", "ź": "z", "ž": "z", "ẑ": "z", "ʑ": "z", "ⱬ": "z", "ż": "z", "ẓ": "z", "ȥ": "z", "ẕ": "z", "ᵶ": "z", "ᶎ": "z", "ʐ": "z", "ƶ": "z", "ɀ": "z", "ﬀ": "ff", "ﬃ": "ffi", "ﬄ": "ffl", "ﬁ": "fi", "ﬂ": "fl", "ĳ": "ij", "œ": "oe", "ﬆ": "st", "ₐ": "a", "ₑ": "e", "ᵢ": "i", "ⱼ": "j", "ₒ": "o", "ᵣ": "r", "ᵤ": "u", "ᵥ": "v", "ₓ": "x"};
String.prototype.removeAccents = function () {
    return this.replace(/[^A-Za-z0-9\[\] ]/g, function (a) {
        return Latinise.latin_map[a] || a;
    });
};

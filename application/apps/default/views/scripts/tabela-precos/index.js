/**
 * View geral para a funcionalidade de cadastro de tabela de precos
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */

/***********************
 *   Tabela de Preços
 **********************/

var TabelaPrecosModel = Backbone.Model.extend({
    url: '/api/tabela-precos/',
    defaults: {
        id_entidade: '',
        id_upload: '',
        st_upload: ''
    },
    initialize: function () {
        this.fetch({async: false});
    }
});

/***********************
 *   TabelaPrecosView
 **********************/

var TabelaPrecosView = Marionette.ItemView.extend({
    template: '#tabela-precos-template',
    model: new TabelaPrecosModel(),
    tagName: 'form',
    className: 'row-fluid',
    events: {
        'change #tabela-precos-arquivo': 'toggleSalvar',
        'click #btnSalvar': 'salvar'
    },
    ui: {
        'btnSalvar': '#btnSalvar'
    },
    toggleSalvar: function () {

    },
    salvar: function (e) {

        var nameFile = '';
        var sizeFile = '';
        var data = new FormData();
        jQuery.each($('#tabela-precos-arquivo')[0].files, function (i, file) {
            data.append('file-' + i, file);
        })


        var that = this;
        //submit
        $.ajax({
            url: '/tabela-precos/upload',
            type: 'post',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                loading();
            },
            success: function (data) {
                that.model.fetch({
                    success: function () {
                        that.render();
                    }
                });
                loaded();
                if (data.error) {
                    jQuery.each(data.error, function (index, value) {
                        $.pnotify({
                            title: value.title,
                            text: value.text,
                            type: value.type
                        });
                    });
                } else {
                    $.pnotify({
                        title: 'Sucesso',
                        text: 'Arquivo salvo com sucesso!',
                        type: 'success'
                    });
                }
            }
        });
    }
});

var TabelaPrecos = new TabelaPrecosView();
G2S.layout.content.show(TabelaPrecos);


/***********************
 *   Configurar progresso
 *   do upload AJAX
 **********************/
(function addXhrProgressEvent($) {
    var originalXhr = $.ajaxSettings.xhr;
    $.ajaxSetup({
        progress: function () {
        },
        progressUpload: function () {
        },
        xhr: function () {
            var req = originalXhr(), that = this;
            if (req.upload) {
                if (typeof req.upload.addEventListener == "function") {
                    req.upload.addEventListener("progress", function (evt) {
                        that.progressUpload(evt);
                    }, false);
                }
            }
            return req;
        }
    });
})(jQuery);
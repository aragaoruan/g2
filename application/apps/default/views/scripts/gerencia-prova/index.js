/**
 * User: Débora Castro <debora.castro@unyleya.com.br>
 * @update em 27/05/2015 - Débora Castro
 * Refatoraçao em 02/12/2014
 * Date: 19/12/2013
 */


/************************************
 * Variaveis Globais
 *******************************************************************/
var mpesquisa;
var validacao = false;


/************************************
 * Models
 *******************************************************************/
var AlunosAgendamentoModel = Backbone.Model.extend({
    defaults: {
        st_nomecompleto: '',
        id_usuario: null,
        id_matricula: null,
        st_projetopedagogico: '',
        id_projetopedagogico: null
    }
});

var AvaliacaoAgendamentoModel = AvaliacaoAgendamento.extend({
    idAttribute: 'id_avaliacaoagendamento',
    url: function() {
        return '/api/avaliacao-agendamento/' + (this.id || '');
    }
});

var modelAluno = new AlunosAgendamentoModel();
var modelAvaliacaoAgendamento = new AvaliacaoAgendamentoModel();

var viewAgendamento;
var viewAluno;
var viewSituacao;
var viewHistorico;
var viewAvaliacoes;

var ModelA = Backbone.Model.extend({
    defaults: {
        id_avaliacaoagendamento: '',
        dt_agendamento: '',
        dt_aplicacao: '',
        id_matricula: '',
        id_usuario: '',
        id_situacao: '',
        id_avaliacaoaplicacao: '',
        id_primeirachamada: '',
        id_ultimachamada: '',
        id_evolucao: '',
        bl_provaglobal: '',
        id_mensagens: '',
        id_provasrealizadas: '',
        id_entidade: '',
        id_horarioaula: '',
        id_avaliacao: '',
        id_projetopedagogico: '',
        id_tipodeavaliacao: '',
        id_situacaopresenca: '',
        id_chamada: '',
        bl_ativo: '',
        st_nomecompleto: '',
        st_avaliacao: '',
        st_situacao: '',
        st_nomeentidade: '',
        st_horarioaula: '',
        st_mensagens: '',
        st_projetopedagogico: '',
        st_codsistema: '',
        st_endereco: '',
        st_aplicadorprova: '',
        st_cidade: '',
        st_situacaopresenca: '',
        nu_primeirapresenca: '',
        nu_ultimapresenca: '',
        id_aplicadorprova: '',
        nu_presenca: '',
        id_usuariolancamento: '',
        st_usuariolancamento: '',
        bl_temnotafinal: '',
        st_email: '',
        nu_telefone: '',
        nu_ddd: '',
        nu_ddi: '',
        hr_inicio: '',
        hr_fim: '',
        sg_uf: '',
        st_complemento: '',
        nu_numero: '',
        st_bairro: '',
        st_telefoneaplicador: '',
        st_avaliacaorecupera: '',
        hr_inicioprova: '',
        hr_fimprova: ''
    }
});

var ModelHistorico = VwHistoricoAgendamento.extend();

var ModelDisciplinasAgendamento = VwDisciplinasAgendamento.extend();

//inicialização objeto model.
var modelGlobalAgendamento = new ModelA();

/************************************
 * Collections
 *******************************************************************/
var VwAlunosAgendamentoCollection = Backbone.Collection.extend({
    model: AlunosAgendamentoModel,
    url: '/default/gerencia-prova/pesquisa-alunos'
});

var VwAvaliacaoAplicacaoCollection = Backbone.Collection.extend({
    model: ModelA,
    url: '/default/gerencia-prova/busca-avaliacoes'
});

var VwHistoricoAgendamentoCollection = Backbone.Collection.extend({
    model: ModelHistorico,
    url: '/api/vw-historico-agendamento'
});

var VwDisciplinasAgendamentoCollection = Backbone.Collection.extend({
    model: ModelDisciplinasAgendamento,
    url: '/default/gerencia-prova/busca-disciplinas'
});

var AgendamentosCollection = Backbone.Collection.extend({
    //url: 'api/vw-avaliacao-agendamento/',
    //url: 'api/vw-avaliacao-aluno/',
    url: '/gerencia-prova/get-agendamentos-chamadas/',
    parse: function(response) {
        this.colunas_chamadas = response.colunas_chamadas || 0;
        return response.avaliacoes || response || [];
    },
    data: {}
});

//inicialização dos objetos collection.
var collectionListaHistorico = new VwHistoricoAgendamentoCollection();
var colecaoDisciplinas = new VwDisciplinasAgendamentoCollection();
var colecaoAgendamentos = new AgendamentosCollection();


/**
 * Pesquisa Alunos - Arquivos Gerais
 *******************************************************************/

var listaAlunosItemView = Backbone.Marionette.ItemView.extend({
    model: new AlunosAgendamentoModel(),
    template: _.template('<td class="exibeagendamento" style="cursor: pointer;"><b><%= st_nomecompleto %> - (Mat. <%= id_matricula %>)</b><br /><%=st_projetopedagogico%></td>'),
    tagName: 'tr',
    events: {
        'click .exibeagendamento': 'listaDadosAgendamentoAluno'
    },
    listaDadosAgendamentoAluno: function () {
        aluno = this.model.get('id_matricula');
        modelAluno.set(this.model.toJSON());

        viewAgendamento = new AgendamentoView();
        telaGerenciaProva.dadosAgendamento.reset().show(viewAgendamento);
    }
});

var listaAlunosItemViewEmpty = Backbone.Marionette.ItemView.extend({
    template: _.template('<td>Nenhum Aluno Encontrado.</td>'),
    tagName: 'tr'
});

var listaAlunosAgendamentoView = Backbone.Marionette.CompositeView.extend({
    template: _.template('<table id="tableListaAlunos" ' +
        'class=" table table-bordered table-striped backgrid">' +
        '<thead><th>Alunos</th></thead><tbody></tbody></table>'),
    collection: new VwAlunosAgendamentoCollection(),
    childView: listaAlunosItemView,
    emptyView: listaAlunosItemViewEmpty,
    childViewContainer: 'tbody',
    onRender: function () {
        loading();
        this.collection.fetch({
            data: mpesquisa,
            complete: function () {
                $('#lista-alunos-agendamento').show();
                loaded();
            }
        });
    }
});

/************************************
 * Composite e Item das Disciplinas do Agendamento
 *******************************************************************/

var DisciplinasView = Backbone.Marionette.ItemView.extend({
    template: '#disciplinas-view',
    tagName: 'tr'
});

var DisciplinasComposite = Backbone.Marionette.CompositeView.extend({
    template: '#disciplinas-template',
    collection: colecaoDisciplinas,
    childView: DisciplinasView,
    emptyView: Marionette.ItemView.extend({
        tagName: 'tr',
        template: _.template('<td colspan="3">Nenhuma disciplina disponível</td>')
    }),
    childViewContainer: 'tbody'
});

/************************************
 * Composite e Item do historico
 *******************************************************************/
var HistoricoItemView = Backbone.Marionette.ItemView.extend({
    model: new ModelHistorico(),
    template: '#historico-view',
    tagName: 'tr'
});
var HistoricoEmptyItemView = Backbone.Marionette.ItemView.extend({
    template: _.template(' <td colspan="5">Sem Histórico</td>'),
    tagName: 'tr'
});
var ListarHistoricoView = Marionette.CompositeView.extend({
    template: '#tela-historico-agendamento',
    ui: {
        id_situacaohistorico: '#id_situacaohistorico',
        id_anos: '#id_anos'
    },
    collection: collectionListaHistorico,
    childView: HistoricoItemView,
    emptyView: HistoricoEmptyItemView,
    childViewContainer: 'form#form-lista-historico table#tabela-retorno-pesquisa-historico tbody',
    onRender: function () {
        this.collection.fetch({
            reset: true,
            data: {
                id_matricula: modelAluno.get('id_matricula')
            }
        });
        this.listaAnos();
    },
    listaAnos: function () {
        var data = new Date();
        var ano = data.getFullYear();
        var num_anos = ano - 10;
        var options = '<option value="">--</option>';
        for (var i = num_anos; i < ano + 1; i++) {
            options += '<option value="' + i + '">' + i + '</option>';
        }
        this.ui.id_anos.html(options).show();
    },
    events: {
        'click #btnPesquisaHistorico': 'pesquisaHistorico'
    },
    pesquisaHistorico: function () {
        var that = this;

        this.collection.fetch({
            data: {
                id_matricula: modelAluno.get('id_matricula'),
                id_situacao: that.ui.id_situacaohistorico.val(),
                id_ano: that.ui.id_anos.val()
            }
        });
    }
});

/************************************
 * Composite e Item dos locais de prova
 *******************************************************************/
//composite e itens responsaveis pela listagem dos locais de prova (agendar e reagendar)
var PolosView = Backbone.Marionette.ItemView.extend({
    model: modelGlobalAgendamento,
    tagName: 'tr',
    template: '#tr-lista-locais-prova'
});

var PolosEmptyView = Backbone.Marionette.ItemView.extend({
    model: modelGlobalAgendamento,
    template: _.template('<tr><td colspan="7">Não foram encontrados locais de prova disponíveis</td></tr>')
});

var PolosComposite = Marionette.CompositeView.extend({
    template: '#polos-template',
    collection: new VwAvaliacaoAplicacaoCollection(),
    childView: PolosView,
    emptyView: PolosEmptyView,
    childViewContainer: 'tbody'
});

/************************************
 * Telas de eventos do Agendamento - Arquivos Gerais
 *******************************************************************/


/************************************
 * Layout
 *******************************************************************/
var LayoutGerenciaProva = Marionette.LayoutView.extend({
    template: '#layout-gerencia-prova',
    tagName: 'div',
    regions: {
        regiaoListaDeAlunos: '#lista-alunos-agendamento',
        regiaoAbasAgendamento: '#abas-agendamento',
        regiaoDadosAbasAgendamento: '#dados-abas-agendamento',
        regiaoHistorico: '#dados-historico-agendamento',
        dadosAgendamento: '#dados-agendamento'
    },
    ui: {
        st_nomecompleto: '#st_nomecompleto',
        st_email: '#st_email',
        st_cpf: '#st_cpf',
        sg_uf: '#sg_uf',
        aplicadorprova: '#id_aplicadorprova',
        dt_aplicacao: '#dt_aplicacao',
        dadosAgendamento: '#dados-agendamento'
    },
    events: {
        'submit #formPesquisaAlunosAgendamento': 'pesquisarAlunos',
        'change #sg_uf': 'renderAplicadoresSelect',
        'change #id_aplicadorprova': 'renderDataAplicacaoSelect'
    },
    onShow: function () {
        this.renderUfSelect();
    },
    validate: function () {
        var values = Backbone.Syphon.serialize(this);

        var error = false;

        var validAluno = (values.st_nomecompleto || values.st_email || values.st_cpf);
        var validAplicacao = (values.sg_uf && values.id_aplicadorprova && values.dt_aplicacao);

        if (validAluno || validAplicacao) {
            return true;
        }

        if (!validAluno && !validAplicacao) {
            error = 'Preencha um campo dos dados do aluno ou todos da aplicação';
        } else if (!validAplicacao) {
            error = 'Preencha todos os dados em relação à aplicação'
        }

        if (error) {
            $.pnotify({
                'type': 'warning',
                'title': 'Campos Obrigatórios',
                'text': error
            });
            return false;
        }

        return true;
    },
    pesquisarAlunos: function (e) {
        e.preventDefault();

        if (this.validate()) {

            mpesquisa = Backbone.Syphon.serialize(this);
            //$(document).find('#dados-agendamento').hide();
            //$(document).find('#dados-historico-agendamento').hide();


            this.dadosAgendamento.reset();
            this.regiaoListaDeAlunos.reset().show(new listaAlunosAgendamentoView());
            //this.regiaoAbasAgendamento.reset().show(new listaAvaliacoesAgendamentoView({collection: colecaoAvaliacoes}));
            //this.regiaoDadosAbasAgendamento.reset().show(new listaDetalhesAbaAgendamentoView({collection: colecaoAvaliacoes}));
        }
    },
    renderUfSelect: function () {
        var that = this;
        ComboboxView({
            el: that.ui.sg_uf,
            url: '/default/util/retorna-uf/',
            optionLabel: 'st_uf',
            optionSelectedId: '',
            emptyOption: '[Todos]',
            model: Backbone.Model.extend({
                idAttribute: 'sg_uf'
            })
        });
    },
    renderAplicadoresSelect: function () {
        var elem = this.$el;
        this.ui.aplicadorprova.html('<options>Carregando...</options>');

        this.ui.dt_aplicacao.html('<options>[Selecione]</options>');
        if (this.ui.sg_uf.val() != '') {
            var AplicadorModel = Backbone.Model.extend({
                idAttribute: 'id_aplicadorprova',
                defaults: {
                    id_aplicadorprova: null,
                    st_aplicadorprova: ''
                }
            });
            ComboboxView({
                el: elem.find('[name="id_aplicadorprova"]'),
                model: AplicadorModel,
                url: '/api/vw-aplicador/index/bl_ativo/1/sg_uf/' + this.ui.sg_uf.val(),
                optionLabel: 'st_aplicadorprova',
                emptyOption: '[Selecione]'
            });
        } else {
            this.ui.aplicadorprova.html('<options>[Selecione]</options>');
        }

    },
    renderDataAplicacaoSelect: function () {
        var elem = this.$el;
        var DataAplicacaoModel = Backbone.Model.extend({
            idAttribute: 'dt_aplicacao',
            defaults: {
                dt_aplicacao: ''
            }
        });
        ComboboxView({
            el: elem.find('[name="dt_aplicacao"]'),
            model: DataAplicacaoModel,
            url: '/api/vw-aplicador/index/id_aplicadorprova/' + this.ui.aplicadorprova.val() + '/sodata/true/id_entidade/' + G2S.loggedUser.get('id_entidade'),
            optionLabel: 'dt_aplicacao',
            emptyOption: '[Selecione]'
        });
    }
});


/*********************************************************
 * *******************************************************
 * NOVO AGENDAMENTO GRADUACAO ****************************
 * *******************************************************
 * *******************************************************
 */

//var AvaliacaoCollection = Backbone.Collection.extend({
//    url: '/api/vw-avaliacao-conjunto-disciplina/'
//});

var AvaliacaoView = Marionette.ItemView.extend({
    template: '#avaliacao-view',
    tagName: 'tr',
    ui: {
        radio: 'input[type="radio"]'
    },
    events: {
        'change @ui.radio': 'carregarBotoes'
    },
    onBeforeRender: function() {
        var agendamentos = this.model.get('agendamentos');

        // Pegar o ultimo agendamento para mostrar as informacoes na TABELA
        var ultimo_agendamento = agendamentos[agendamentos.length - 1];
        this.model.set('ultimo_agendamento', ultimo_agendamento);

        /**
         * Fazer com que as colunas fiquem em quantidades iguais, para o registro q tem menos Chamadas
         */
        var length = agendamentos.length;
        var chamadas = this.model.collection.colunas_chamadas;

        for (var i = 0; i < chamadas - length; i++) {
            agendamentos.push({
                dt_agendamento: '',
                st_cidade: '',
                nu_presenca: ''
            });
        }

        this.model.set('agendamentos', agendamentos);

        // CONFIGURAR A COR DO LABEL
        var color = '#fff';

        if (ultimo_agendamento.nu_presenca !== null) {
            // SE TIVER PRESENCA, ENTAO A PROVA JA FOI REALIZADA
            color = 'rgb(255,123,0)';
        } else {
            switch (ultimo_agendamento.id_situacao) {
                case SITUACAO.TB_AVALIACAOAGENDAMENTO.LIBERADO:
                    color = 'rgb(0,102,255)';
                    break;
                case SITUACAO.TB_AVALIACAOAGENDAMENTO.ABONADO:
                    color = 'rgb(0,102,255)';
                    break;
                case SITUACAO.TB_AVALIACAOAGENDAMENTO.AGENDADO:
                    color = 'rgb(51,153,0)';
                    break;
                case SITUACAO.TB_AVALIACAOAGENDAMENTO.REAGENDADO:
                    color = 'rgb(51,153,0)';
                    break;
                default:
                    color = 'rgb(0,51,255)';
                    break;
            }
        }

        this.model.set('color', color);
    },
    onRender: function() {
        //var that = this;

        //modelPresenca.fetch({
        //    data: {
        //        id_matricula: modelAluno.get('id_matricula'),
        //        id_avaliacao: that.model.get('id_avaliacao')
        //    }
        //});
    },
    carregarBotoes: function() {
        if (this.model.get('ultimo_agendamento').nu_presenca === null) {
            var values = this.model.toJSON();
            modelAvaliacaoAgendamento.set(values.ultimo_agendamento);
            modelAvaliacaoAgendamento.set(values);

            var botoesView = new BotoesView({model: modelAvaliacaoAgendamento});
            viewAgendamento.botoes.show(botoesView);
        } else {
            viewAgendamento.botoes.reset();
        }
    }
});

var AvaliacaoComposite = Marionette.CompositeView.extend({
    template: '#avaliacao-template',
    //collection: new AvaliacaoCollection(),
    collection: colecaoAgendamentos,
    childView: AvaliacaoView,
    emptyView: Marionette.ItemView.extend({
        tagName: 'tr',
        template: _.template('<td colspan="3">Nenhuma avaliação disponível</td>')
    }),
    childViewContainer: 'tbody',
    collectionEvents: {
        onFetch: 'labelsChamadas'
    },
    labelsChamadas: function () {
        for (var i = 0; i < this.collection.colunas_chamadas; i++) {
            this.$el.find('.label-chamadas').append('<th colspan="3" class="text-center">' + (i + 1) + 'ª Chamada</th>');
            this.$el.find('.label-head').append('<th class="text-center">Data</th><th class="text-center">Aplicação</th><th class="text-center">Presença</th>');
        }
    }
});

var AlunoView = Marionette.ItemView.extend({
    model: modelAluno,
    template: '#aluno-template',
    onRender: function () {
        //debug(this.model.toJSON());
    }
});

var BotoesView = Marionette.LayoutView.extend({
    template: '#botoes-template',
    regions: {
        form: '#container-form'
    },
    ui: {
        // BOXES
        boxBotoes: '#box-botoes',
        // BUTTONS
        btnVoltar: '.btn-voltar',
        btnLiberar: '.btn-liberar',
        btnAbonar: '.btn-abonar',
        btnAgendar: '.btn-agendar',
        btnCancelar: '.btn-cancelar',
        btnReagendar: '.btn-reagendar',
        // FORMS
        form: 'form'
    },
    onRender: function() {
        //debug(this.model.get('id_situacao'));
    },
    events: {
        // BUTTON ACTIONS
        'click @ui.btnVoltar': 'actionVoltar',
        'click @ui.btnLiberar': 'actionLiberar',
        'click @ui.btnAbonar': 'actionAbonar',
        'click @ui.btnAgendar': 'actionAgendar',
        'click @ui.btnReagendar': 'actionReagendar',
        'click @ui.btnCancelar': 'actionCancelar',

        // FORM SUBMITS
        'submit @ui.form': 'actionSalvar'
    },
    actionVoltar: function() {
        this.form.reset();
        this.ui.boxBotoes.show();
    },
    actionLiberar: function() {
        this.ui.boxBotoes.hide();
        var viewForm = new FormViewLiberar({model: this.model});
        this.form.show(viewForm);

        //this.renderTelaAcoes('#form-liberar');
        //if (ModelSave.attributes.id_tipodeavaliacao > 0) {
        //    $(document).find('#div-tabela-disciplinas').hide();
        //    $(document).find('#provaglobal').show();
        //} else {
        //    $(document).find('#bl_provaglobal').prop('disabled', true).show();
        //
        //}
    },
    actionAbonar: function() {
        this.ui.boxBotoes.hide();
        var viewForm = new FormViewLiberar({model: this.model});
        this.form.show(viewForm);
        viewForm.ui.id_situacao.val(130);
        viewForm.ui.header.html('Abonar Agendamento');
    },
    actionAgendar: function() {
        this.ui.boxBotoes.hide();
        var viewForm = new FormViewAgendar({model: this.model});
        this.form.show(viewForm);
    },
    actionReagendar: function() {
        this.ui.boxBotoes.hide();
        var viewForm = new FormViewAgendar({model: this.model});
        this.form.show(viewForm);
        viewForm.ui.id_situacao.val(69);
        viewForm.ui.header.html('Reagendar Avaliação');
    },
    actionCancelar: function() {
        this.ui.boxBotoes.hide();
        var viewForm = new FormViewLiberar({model: this.model});
        this.form.show(viewForm);
        viewForm.ui.boxProvaGlobal.remove();
        viewForm.ui.id_situacao.val(70);
        viewForm.ui.header.html('Cancelar Agendamento');
    },
    actionSalvar: function(e) {
        e.preventDefault ? e.preventDefault(): '';

        var that = this;

        var formView = this.form.currentView;
        var data = formView.serialize ? formView.serialize() : {};

        if (!data) {
            return false;
        }

        var el_provaglobal = this.$el.find('#bl_provaglobal');

        var values = Backbone.Syphon.serialize(formView);
        values.id_matricula = modelAluno.get('id_matricula');
        values.id_usuario = modelAluno.get('id_usuario');
        values.bl_provaglobal = el_provaglobal.length ? this.$el.find('#bl_provaglobal').is(':checked') : this.model.get('bl_provaglobal');

        // VALIDACAO / VALIDATE
        if (!values.st_justificativa) {
            $.pnotify({
                type: 'warning',
                title: 'Aviso',
                text: 'Por favor insira a justificativa.'
            });
            return false;
        }

        data = $.extend(values, data);

        this.model.save(data, {
            beforeSend: loading,
            complete: function(response) {
                $.pnotify(response.responseJSON || {
                        type: 'error',
                        title: 'Erro',
                        text: 'Houve um erro ao realizar o procedimento'
                    });

                // So recarrega a view caso o Mensageiro for SUCESSO!
                if (response && response.responseJSON && 1 == response.responseJSON.tipo) {
                    viewSituacao.loadAvaliacoes(that.model.get('id_avaliacao'));
                    viewHistorico.collection.fetch({
                        reset: true,
                        data: {
                            id_matricula: modelAluno.get('id_matricula')
                        }
                    });
                    that.$el.remove();
                }

                loaded();
            }
        });
    }
});



// FORMS DOS BOTOES
var FormViewLiberar = Marionette.LayoutView.extend({
    template: '#form-liberar',
    regions: {
        disciplinas: '#container-disciplinas'
    },
    ui: {
        header: '.header',
        boxProvaGlobal: '#provaglobal',
        id_situacao: '#id_situacao',
        bl_provaglobal: '#bl_provaglobal',
        st_justificativa: '#st_justificativa'
    },
    events: {
        'change @ui.bl_provaglobal': 'carregarDisciplinas'
    },
    onRender: function() {
        if (G2S.loggedUser.get('bl_provapordisciplina')) {
            this.ui.boxProvaGlobal.addClass('hide').find('input').prop('checked', false);
        } else {
            this.ui.boxProvaGlobal.removeClass('hide');
            // Se for RECUPERACAO, permitir marcar / desmarcar prova global
            if (this.model.get('id_tipodeavaliacao') || this.model.get('id_avaliacao') == 114) {
                this.ui.boxProvaGlobal.find('input').prop('disabled', false);
            }
        }
    },
    carregarDisciplinas: function() {
        var that = this;

        if (this.ui.bl_provaglobal.is(':checked')) {
            // Se for prova global, esconder a selecao de disciplinas
            this.disciplinas.reset();
        } else {
            var viewDisciplinas = new DisciplinasComposite();

            colecaoDisciplinas.fetch({
                data: {
                    id_matricula: modelAluno.get('id_matricula'),
                    id_avaliacao: that.model.get('id_avaliacao')
                },
                beforeSend: loading,
                complete: loaded
            });

            this.disciplinas.show(viewDisciplinas);
        }
    },
    serialize: function () {
        var data = {};

        if (!G2S.loggedUser.get('bl_provapordisciplina') && !this.ui.bl_provaglobal.is(':checked')) {
            var disciplinas = [];

            this.disciplinas.currentView.$el.find('input[type="checkbox"]:checked').each(function() {
                disciplinas.push($(this).val());
            });

            if (!disciplinas.length) {
                $.pnotify({
                    type: 'warning',
                    title: 'Aviso',
                    text: 'Por favor selecione ao menos uma disciplina.'
                });
                return false;
            }

            data.id_avaliacaoconjuntoreferencia = disciplinas;
        }

        return data;
    }
});

var FormViewAgendar = Marionette.LayoutView.extend({
    template: '#form-agendar',
    regions: {
        polos: '#container-polos'
    },
    ui: {
        header: '.header',
        boxJustificativa: '#div_justificativa',
        btnPesquisar: '.btn-pesquisar',
        id_situacao: '#id_situacao',
        sg_uf: '#sg_uf',
        id_municipio: '#id_municipio',
        st_justificativa: '#st_justificativa',
        inputs_polos: 'input[type="radio"][name="id_avaliacaoaplicacao"]'
    },
    onRender: function(){
        ComboboxView({
            el: this.ui.sg_uf,
            url: '/default/util/retorna-uf/',
            optionLabel: 'st_uf',
            emptyOption: '[Selecione]',
            model: Backbone.Model.extend({
                idAttribute: 'sg_uf'
            })
        });
    },
    events: {
        'change @ui.sg_uf': 'carregarMunicipios',
        'change @ui.id_municipio': 'pesquisaReset',
        'click @ui.btnPesquisar': 'actionPesquisar',
        'change @ui.inputs_polos': 'carregarJustificativa'
    },
    carregarMunicipios: function() {
        this.pesquisaReset();

        loading();
        ComboboxView({
            el: this.ui.id_municipio,
            // url: '/api/municipio/?sg_uf=' + this.ui.sg_uf.val(),
            url: '/default/util/retorna-municipio-agendamento?sg_uf=' + this.ui.sg_uf.val(),
            // optionLabel: 'st_nomemunicipio',
            optionLabel: 'st_municipio',
            emptyOption: '[Selecione]',
            model: Backbone.Model.extend({
                idAttribute: 'id_municipio'
            })
        }, function(el, composite) {
            if (!composite.collection.length) {
                $.pnotify({
                    type: 'warning',
                    title: 'Aviso',
                    text: 'Não foram encontrados municípios com aplicador disponível'
                })
            }
        });
    },
    pesquisaReset: function() {
        this.ui.boxJustificativa.addClass('hide');
        this.polos.reset();
    },
    actionPesquisar: function() {
        var that = this;

        this.pesquisaReset();

        var viewPolos = new PolosComposite();
        var id_avaliacao = viewAvaliacoes.$el.find('input:checked').val();

        viewPolos.collection.fetch({
            beforeSend: loading,
            data: {
                sg_uf: that.ui.sg_uf.val(),
                id_municipio: that.ui.id_municipio.val(),
                id_avaliacao: id_avaliacao
            },
            success: function () {
                that.polos.show(viewPolos);
                //$(document).find('#fieldLocalProva').removeClass('hide');
                //$(document).find('#fieldPesquisaPolo').addClass('hide');
                //$(document).find('#div_justificativa').removeClass('hide');
                //$(document).find('#fieldLocalProva').html(new PolosComposite({collection: colletionLocais}).render().$el);
            },
            complete: loaded
        });
    },
    carregarJustificativa: function() {
        this.ui.boxJustificativa.removeClass('hide');
        this.ui.st_justificativa.val('').focus();
    }
});

var SituacaoView = Marionette.LayoutView.extend({
    template: '#situacao-template',
    regions: {
        avaliacao: '#container-avaliacao'
    },
    ui: {
        id_disciplina: '#id_disciplina'
    },
    events: {
        'change @ui.id_disciplina': 'loadAvaliacoes'
    },
    onRender: function() {
        var that = this;

        if (G2S.loggedUser.get('bl_provapordisciplina')) {
            // Combo Disciplinas
            ComboboxView({
                el: this.ui.id_disciplina,
                url: '/default/gerencia-prova/get-disciplinas-apto?id_matricula=' + modelAluno.get('id_matricula'),
                optionLabel: 'st_disciplina',
                model: Backbone.Model.extend({
                    idAttribute: 'id_disciplina'
                })
            }, function (el) {
                el.find('option:first').prop('selected', true);
                that.loadAvaliacoes();
            });
        } else {
            this.ui.id_disciplina.closest('label').hide();
            that.loadAvaliacoes();
        }
    },
    loadAvaliacoes: function(id_avaliacao) {
        viewAgendamento.botoes.reset();

        var data = {
            id_matricula: modelAluno.get('id_matricula')
        };

        if (G2S.loggedUser.get('bl_provapordisciplina')) {
            data.id_disciplina = this.ui.id_disciplina.val();
            data.id_tipoavaliacao = [1,2,4];
        } else {
            data.id_tipoavaliacao = 4;
        }

        viewAvaliacoes = new AvaliacaoComposite();

        this.avaliacao.show(viewAvaliacoes);
        colecaoAgendamentos.reset();
        colecaoAgendamentos.fetch({
            beforeSend: loading,
            data: data,
            success: function() {
                viewAvaliacoes.labelsChamadas();
                if (id_avaliacao) {
                    viewAvaliacoes.$el.find('input[type="radio"][value="' + id_avaliacao + '"]').prop('checked', true).trigger('change');
                }
            },
            complete: loaded
        });
    }
});



var AgendamentoView = Marionette.LayoutView.extend({
    template: '#agendamento-template',
    regions: {
        aluno: '#container-aluno',
        situacao: '#container-situacao',
        botoes: '#container-botoes',
        historico: '#container-historico'
    },
    onRender: function() {
        viewAluno = new AlunoView();
        this.aluno.show(viewAluno);

        viewSituacao = new SituacaoView();
        this.situacao.show(viewSituacao);

        viewHistorico = new ListarHistoricoView();
        this.historico.show(viewHistorico);
    }
});




/*********************************************
 ************** INICIALIZAÇÃO ****************
 *********************************************/
var telaGerenciaProva = new LayoutGerenciaProva();
G2S.layout.content.show(telaGerenciaProva);
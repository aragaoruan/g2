/**
 * CADASTRO DE ASSUNTO PRA A CENTRAL DE ATENÇÃO
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
$.getScript('js/backbone/views/Util/SelectView.js');

/**
 * Model generica para backgrid
 * @type Model
 * @exp;Backbone
 * @pro;Model
 * @call;extend
 */
PesquisaModelo = Backbone.Model.extend({
    defaults: {
        id_assuntoco: '',
        id_assuntocopai: '',
        st_assuntoco: '',
        st_subassuntoco: '',
        st_tipoocorrencia: '',
        st_situacao: '',
        id_situacao: '',
        id_tipoocorrencia: ''
    },
    url: function () {
        return this.id ? '/default/retornar-vw-pesquisa-assuntoco/' + this.id : '/default/retornar-vw-pesquisa-assuntoco/';
    }
});

/**
 * Model básica para combo texto sistema
 * @type {*}
 */
TextoModel = Backbone.Model.extend({
    defaults: {
        id_textosistema: '',
        st_textosistema: ''
    }
});

/**
 * Backgrid pageable
 * @exp;Backbone
 * @pro;PageableCollection
 * @call;extend
 */
PesquisaCollection = Backbone.PageableCollection.extend({
    model: PesquisaModelo,
    url: "/assunto-co/retornar-vw-pesquisa-assuntoco",
    state: {
        pageSize: 15
    },
    mode: "client" // define que a paginação será no lado client
});

/**
 * @type Backgrid Cell
 * @exp;Backgrid
 * @pro;Cell
 * @call;extend
 */
AbreRegistro = Backgrid.Cell.extend({
    template: _.template("<div class='span12' id='acoes'><a class='btn btn-small btn-custom subassunto'  id='<%=id%>'><i class='icon icon-white icon-plus'></i>Sub Assunto</a>&nbsp <a class='btn btn-small btn-custom editar' id='<%=id%>'><i class='icon icon-white icon-pencil'> </i> Editar</a></div>"),
    events: {
        "click .subassunto": "subAssunto",
        "click .editar": "editar"
    },
    subAssunto: function (e) {
        var modelAssuntoCo = new AssuntoCo();
        modelAssuntoCo.set('id_assuntocopai', this.model.get('id'));
        modelAssuntoCo.set('st_assuntocopai', this.model.get('st_assuntoco'));
        G2S.editModel = modelAssuntoCo;

        var assuntoView = new itemAssuntoco({
            model: modelAssuntoCo
        });

        G2S.show(assuntoView);

    },
    editar: function (e) {
        var modelAssuntoCo = new AssuntoCo();
        modelAssuntoCo.idAttribute = 'id_assuntoco';
        modelAssuntoCo.id = this.model.get('id');
        modelAssuntoCo.fetch({
            success: function () {
                G2S.editModel = modelAssuntoCo;
                var assuntoView = new itemAssuntoco({
                    model: modelAssuntoCo
                });
                G2S.show(assuntoView);
            }
        });
    },
    showBtnSubAssunto: function () {
        var that = this;
        if (that.model.get('id_assuntocopai') == '') {
            return true;
        } else {
            return false;
        }
    },
    render: function () {
        this.$el.css('width', '260px');
        this.$el.html(this.template({"id": this.model.id}));
        this.delegateEvents();
        //hide btn .delete
        if (!this.showBtnSubAssunto()) {
            this.$el.find('.subassunto').hide();
        }
        return this;
    }
});

var columnsGrid = [
    {
        name: "id_assuntoco",
        label: "ID",
        editable: false,
        cell: "string"
    },
    {
        name: "st_assuntoco",
        label: "Assunto",
        editable: false,
        cell: "string"
    },
    {
        name: "st_subassuntoco",
        label: "Sub Assunto",
        editable: false,
        cell: "string"
    },
    {
        name: "st_situacao",
        label: "Situação",
        editable: false,
        cell: "string"
    },
    {
        name: "st_tipoocorrencia",
        label: "Tipo Interessado",
        editable: false,
        cell: "string"
    }
];

/**
 * Collection TipoOcorrencia
 * @type {*}
 */
var TipoOcorrenciaCollectionExtend = Backbone.Collection.extend({
    url: '/assunto-co/tipo-ocorrencia',
    model: TipoOcorrencia
});

/**
 * COMBO PARA TIPO INTERESSADO
 */
var tipoOcorrenciaCollection = new TipoOcorrenciaCollectionExtend();
tipoOcorrenciaCollection.fetch();

var ComboTipoOcorrenciaView = Marionette.ItemView.extend({
    tagName: 'option',
    template: _.template('<%=st_tipoocorrencia%>'),
    onRender: function () {
        this.$el.attr('value', this.model.get('id_tipoocorrencia'));
    }
});

var ComboTipoOcorrenciaCollection = Marionette.CollectionView.extend({
    tagName: 'select',
    childView: ComboTipoOcorrenciaView,
    labelName: 'Tipo interessado: '
});

/**
 * Collection TextoSistema
 * @type {*}
 */
var TextoSistemaCollectionExtend = Backbone.Collection.extend({
    url: '/assunto-co/texto-sistema/',
    model: TextoModel
});

var TextoCollection = new TextoSistemaCollectionExtend();
TextoCollection.url += 'id_textocategoria/8';
TextoCollection.fetch();


var ComboTextoView = Marionette.ItemView.extend({
    tagName: 'option',
    template: _.template('<%=st_textosistema%>'),
    onRender: function () {
        this.$el.attr('value', this.model.get('id_textosistema'));
    }
});

/**
 * ASSUNTO GRID VIEW
 * @type {*}
 */
AssuntoPesquisaView = Marionette.ItemView.extend({
    template: function () {
        return $('#template-assunto-co').html();
    },
    ui: {
        containerTable: '#container-table',
        idTipointeressadoSelect: '#id-tipo-interessado-select',
        formComboTipoInteressado: '#form-combo-tipo-ocorrencia'
    },
    initialize: function () {
        var thatPesquisa = this;
        return thatPesquisa;
    },
    onShow: function () {
        this.loadComboTipoInteressado();
        this.loadGridAssunto();
    },
    onRender: function () {
        return this;
    },
    addNewAction: function () {
        var model = new AssuntoCo();
        var assuntoView = new itemAssuntoco({
            model: model
        });
        G2S.show(assuntoView);

    },
    loadComboTipoInteressado: function () {
        loading();
        this.ui.formComboTipoInteressado.show();
        this.ui.idTipointeressadoSelect.html('<option value="">--</option>');

        (new ComboTipoOcorrenciaCollection({
            collection: tipoOcorrenciaCollection,
            el: this.ui.idTipointeressadoSelect
        })).render();

        loaded();
    },
    loadGridAssunto: function (id) {
        var that = this;
        G2S.dataGridCollection = new PesquisaCollection();//instancia a collection
        G2S.dataGridCollection.queryParams = {
            id_entidadecadastro: G2S.loggedUser.get("id_entidade"),
            id_tipoocorrencia: (typeof id === "undefined" ? "" : id)
        };

        //faz o fetch
        G2S.dataGridCollection.fetch({
            async: false,
            beforeSend: function () {
                loading();
                that.$el.find("#container-table").empty();
            },
            success: function (collection) {
                if (!collection.length) {//collection vazia mostra mensagem de nada encontrado
                    $.pnotify({
                        title: "Sem resultado!",
                        text: "Sem resultado para exibição!",
                        type: "warning"
                    });
                } else {

                    var pageableGrid = new Backgrid.Grid({
                        columns: columnsGrid,
                        collection: collection,
                        className: 'backgrid backgrid-selectall table table-bordered table-hover table-striped'
                    });

                    that.$el.find("#container-table").append(pageableGrid.render().$el);
                    var paginator = new Backgrid.Extension.Paginator({
                        collection: collection
                    });
                    that.$el.find("#container-table").append(paginator.render().$el);

                    /**
                     * Filtros para grid de assuntos
                     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
                     */
                    var optionsFilter = [];
                    _.each(collection.fullCollection.toJSON(), function (val) {
                        if (val.st_assuntoco) {
                            optionsFilter.push({id_assuntoco: val.id_assuntoco, st_assuntoco: val.st_assuntoco});
                        }
                    });

                    var optionsSort = _.sortBy(optionsFilter, ['st_assuntoco']);

                    var selectFilter = new Backgrid.Extension.SelectFilter({
                        className: "backgrid-filter form-control",
                        collection: collection,
                        field: ["id_assuntocopai"],
                        selectOptions: _.union([{label: "Todos", value: null}],
                            _.map(optionsSort, function (o) {
                                return {label: o.st_assuntoco, value: o.id_assuntoco};
                            }))
                    });

                    $(selectFilter.el).css({float: "right", 'margin-right': '10px'});
                    $filter = selectFilter.render().$el;
                    that.$el.find("#container-table").prepend($filter);

                    var filter = new Backgrid.Extension.ClientSideFilter({
                        collection: collection.fullCollection,
                        fields: ['st_subassuntoco']
                    });
                    that.$el.find("#container-table").prepend(filter.render().$el);
                    $(filter.el).css({float: "right"});

                    pageableGrid.insertColumn([{
                        name: 'options',
                        label: 'Opções',
                        sortable: false,
                        editable: false,
                        cell: AbreRegistro
                    }]);
                }
            },
            error: function () {
                $.pnotify({
                    title: "Erro!",
                    text: "Ocorreu um erro ao pesquisar assuntos!",
                    type: "error"
                });

            },
            complete: function () {
                loaded();
            }
        });

        return false;
    },
    changeComboTipo: function (e) {
        var id = $(e.currentTarget).val();
        if (id) {
            this.loadGridAssunto(id);
        }
    },
    events: {
        'click #btnAddNew': 'addNewAction',
        'change #id-tipo-interessado-select': 'changeComboTipo'
    }
});


/***********************
 * Cadastro de assuntoco
 */
var itemAssuntoco = Marionette.ItemView.extend({
    template: '#assunto',
    ui: {
        idTextoSistemaSelect: '#selectTextoSistema',
        formComboTexto: '#form-texto-sistema'
    },
    initialize: function () {
        G2S.editModel = this.model;

    },
    onShow: function () {
        if (this.model.get('id_assuntoco') || this.model.get('id_assuntocopai')) {
            this.ui.idTextoSistemaSelect.html('<option value="0">--</option>');
            var viewSelectTextos = new SelectView({
                el: this.ui.idTextoSistemaSelect,
                collection: TextoCollection,
                childViewOptions: {
                    optionLabel: 'st_textosistema',
                    optionValue: 'id_textosistema',
                    optionSelected: this.model.get('id_textosistema') ? this.model.get('id_textosistema') : 0
                }
            });
            viewSelectTextos.render();
        }


        this.carregaEntidade();
        return this;
    },
    onRender: function () {
        this.$el.find('.ck_assunto').popover();
        this.carregaCategorias();
    },
    carregaCategorias: function () {
        var elem = this.$el;
        var model = this.model;
        //Categoria
        var ModelCategoria = Backbone.Model.extend({
            idAttribute: 'id_nucleofinalidade',
            defaults: {
                'id_nucleofinalidade': null,
                'st_nucleofinalidade': ''
            }
        });

        $.getJSON('/api/nucleo-finalidade', function (response) {
            if (response.type == 'success') {
                var arrayFinalidade = response.mensagem;
                ComboboxView({
                    el: elem.find('[name="categoriaassunto"]'),
                    model: ModelCategoria,
                    collection: arrayFinalidade,
                    emptyOption: '[Selecione]',
                    optionLabel: 'st_nucleofinalidade',
                    optionSelectedId: model.get('id_assuntocategoria')
                });
            }
        });
    },
    salvaTransaction: function () {

        var checked_ids = [];

        $("#entidades").jstree("get_checked", null, true).each
        (function () {
            checked_ids.push(this.id);
        });

        if ($("#st_assuntoco").val() == '' || $('#selectTipoInteressado option:selected').val() == '' || (!checked_ids.length)) {
            bootbox.dialog({
                message: "Marque pelo menos uma entidade",
                title: "Aviso",
                buttons: {
                    danger: {
                        label: "OK!",
                        className: "btn-danger"
                    }
                }
            });
        } else {
            var modelo = new AssuntoCo();
            modelo.set({
                'id_assuntoco': G2S.editModel.get('id_assuntoco') ? G2S.editModel.get('id_assuntoco') : '',
                'id_situacao': $("#selectSituacao option:selected").val(),
                'id_assuntocopai': G2S.editModel.get('id_assuntocopai'),
                'id_entidadecadastro': G2S.loggedUser.get('id_entidade'),
                'id_usuariocadastro': G2S.loggedUser.get('id_usuario'),
                'id_tipoocorrencia': $('#selectTipoInteressado option:selected').val(),
                'st_assuntoco': $("#st_assuntoco").val(),
                'bl_autodistribuicao': $("#bl_autodistribuicao").is(":checked") ? 1 : 0,
                'bl_abertura': $("#bl_abertura").is(":checked") ? 1 : 0,
                'bl_ativo': 1,
                'id_textosistema': $('#selectTextoSistema option:selected').val(),
                'bl_cancelamento': $("#bl_cancelamento").is(":checked") ? 1 : 0,
                'bl_trancamento': $("#bl_trancamento").is(":checked") ? 1 : 0,
                'bl_interno': $("#bl_interno").is(":checked") ? 1 : 0,
                'bl_recuperacao_resgate': $("#bl_recuperacao_resgate").is(":checked") ? 1 : 0,
                'id_assuntocategoria': $("#selectCategoriaAssunto option:selected").val(),
            });

            loading();

            $.ajax({
                url: "assunto-co/salvar",
                type: 'POST',
                dataType: 'json',
                data: {
                    dados: modelo.toJSON(),
                    id_entidade: checked_ids
                },
                beforeSend: function () {
                    loading();
                },
                success: function (data) {
                    modelo.set('id_assuntoco', data.codigo);
                    G2S.editModel = modelo;
                    if (data.title === 'Aviso') {
                        $.pnotify({title: 'Atenção', text: data.mensagem, type: 'wanrning'});
                    } else {
                        $.pnotify({title: 'Cadastro de Assunto', text: data.mensagem, type: 'success'});
                    }

                    loading();
                },
                complete: function () {
                    loaded();
                },
                error: function (data) {
                    $.pnotify({
                        title: 'Erro!',
                        text: 'Ocorreu um erro ao cadastrar o assunto. Tente novamente! ' + data.responseJSON.text,
                        type: 'error'
                    });
                }
            });
        }
    },
    carregaEntidade: function () {
        loaded();
        $("#entidades").jstree({
            "html_data": {
                "ajax": {
                    "url": "util/arvore-entidade",
                    "data": function (n) {
                        return {id: n.attr ? n.attr("id") : 0};
                    }
                }
            },
            "plugins": ["themes", "html_data", "checkbox", "sort", "ui"]
        }).bind("loaded.jstree", function () {
            if (G2S.editModel.get('id_assuntoco') != undefined) {
                $.ajax({
                    url: G2S.editModel ? "assunto-co/recarrega-arvore/id_assuntoco/"
                        + G2S.editModel.get('id_assuntoco') : null,
                    type: 'POST',
                    dataType: 'json',
                    success: function (data) {
                        $("#entidades").jstree("get_unchecked", null, true).each(function () {
                            for (var i = 0; i < data.length; i++) {
                                if (data[i] == this.id) {
                                    $.jstree._reference("#entidades").check_node('li#' + this.id);
                                }
                            }
                        });
                    }
                });
            }
        });
    },
    events: {
        'submit form#form-assuntoco': 'salvaTransaction',
        'click #btn-voltar': 'voltar',
        'change #bl_recuperacao_resgate': 'recuperacao_resgate',
        'change #bl_interno': 'blInterno',
        'change #selectTipoInteressado': 'selectTipoInteressado',
        'change #bl_cancelamento': 'verificarcancelamentoNucleo',
    },
    voltar: function () {
        var pesquisa = new AssuntoPesquisaView();
        G2S.layout.content.show(pesquisa);

    },

    selectTipoInteressado : function () {
        var e = document.getElementById("selectTipoInteressado");
        var itemSelecionado = e.options[e.selectedIndex].value;
        if (parseInt(itemSelecionado) !== 1  ) {
            document.getElementById("bl_recuperacao_resgate").checked = false;
            document.getElementById("bl_recuperacao_resgate").disabled = true;
        } else {
            document.getElementById("bl_recuperacao_resgate").disabled = false;
        }
    },
    recuperacao_resgate:function () {
        if ( $("#bl_recuperacao_resgate").is(":checked") ){
                document.getElementById("bl_interno").checked= true;

           }

    },
    blInterno:function () {
        if ( $("#bl_interno").is(":checked") ){

            document.getElementById("bl_interno").checked= true;

        }else {
            document.getElementById("bl_recuperacao_resgate").checked = false;
        }

    },

    verificarcancelamentoNucleo:function () {

        var id_assuntoco = null;

        if (Array.isArray(this.model.get('id_assuntoco'))) {
            id_assuntoco = this.model.get('id_assuntoco')[0];
        } else {
            id_assuntoco = this.model.get('id_assuntoco');
        }

        $.ajax({
            url: "/api/nucleo-co",
            type: 'GET',
            data: {
                'id_assuntocancelamento': id_assuntoco
            },
            dataType: 'json',
            success: function (data) {
                if(data.mensagem[0]) {
                    $.pnotify({
                        title: 'Atenção',
                        type: 'warning',
                        text: 'Subassunto vinculado ao assunto selecionado no núcleo.Vá ao núcleo e altere o assunto Cancelamento selecionado.'
                    });
                    document.getElementById("bl_cancelamento").checked= true;
                }
            }
        });

    },

});


//Iniciando a tela
var pesquisa = new AssuntoPesquisaView();
G2S.layout.content.show(pesquisa);


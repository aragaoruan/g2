// Include funcao ComboBoxView
$.ajax({
    url: '/js/comboboxview.js',
    dataType: "script",
    async: false
});

$.ajaxSetup({async: false});
$.getScript('js/backbone/views/Util/SelectView.js');
/* * * *  * * * * * *  * * * * * *  * * * * * *  * * * * * *  * *
 * COLLECTIONS
 * * * *  * * * * * *  * * * * * *  * * * * * *  * * * * * *  * */
var TipoOcorrenciaCollectionExtend = Backbone.Collection.extend({
    model: TipoOcorrencia,
    url: '/api/tipo-ocorrencia'
});

var TipoOcorrenciaCollection = new TipoOcorrenciaCollectionExtend();
TipoOcorrenciaCollection.fetch({ data: { id_tipoocorrencia: 1} });

var VwAssuntosCollectionExtend = Backbone.Collection.extend({
    model: VwAssuntos,
    url: '/ocorrencia-aluno/find-assunto/id_tipoocorrencia/1'
});

var VwAssuntosCollection = new VwAssuntosCollectionExtend();
VwAssuntosCollection.fetch();

var VwSubAssuntosCollectionExtend = Backbone.Collection.extend({
    model: VwAssuntos,
    url: '/ocorrencia/find-sub-assunto/'
});

var OcorrenciaAlunoCompositeView = Marionette.CompositeView.extend({
    template: '#template-ocorrencia-aluno',
    className: 'row-fluid',
    childView: Marionette.ItemView.extend({
        template: false
    }),
    ui: {
        containerTable: '#container-nova-ocorrencia-aluno',
        itemAutoComplete: '.item-autocomplete',
        tipoOcorrencia: "#tipo-ocorrencia-holder",
        assunto: "#assunto-holder",
        subassunto: "#sub-assunto-holder",
        formnova: "#form-nova-ocorrencia",
        nuMatricula: '#id_matricula'
    },
    onShow: function () {
        var that = this;
        that.ui.containerTable.hide();
        this.carregaCombos();


        if (localStorage.getItem('pessoaAutocomplete')) {
            var pessoa = JSON.parse(localStorage.getItem('pessoaAutocomplete'));
            if (pessoa != null) {
                this.newOcorrenciaAluno(pessoa);
            }
        }
        componenteAutoComplete.main({
            model: VwPesquisarPessoa,
            element: this.$el.find('#autoComplete')
        }, function () {

        }, function () {
            that.ui.containerTable.hide();
            that.ui.formnova.trigger('reset');
        });

    },
    newOcorrenciaAluno: function (pessoa) {
        loading();
        this.ui.containerTable.show();

        var pessoa = JSON.parse(localStorage.getItem('pessoaAutocomplete'));
        if (pessoa) {
            // Montar SELECT "Nº. Matrícula";
            var Model = Backbone.Model.extend({
                idAttribute: 'id_matricula',
                defaults: {
                    id_matricula: 0,
                    st_projetopedagogico: '',
                    st_evolucao: ''
                }
            });
            ComboboxView({
                el: this.ui.nuMatricula,
                model: Model,
                url: '/api/vw-matricula/id_usuario/' + pessoa.id_usuario,
                emptyOption: '[Selecione]',
                optionLabel: 'id_matricula',
                optionLabelConstruct: function (model) {
                    return model.get('id_matricula') + ' - ' + model.get('st_projetopedagogico') + ' - ' + model.get('st_evolucao');
                }
            });
        }

        loaded();
    },
    carregaCombos: function () {
        //combo tipo ocorrência
        var viewSelectTipoOcorrencia = new SelectView({
            el: this.ui.tipoOcorrencia,
            collection: TipoOcorrenciaCollection,
            childViewOptions: {
                optionLabel: 'st_categoriaocorrencia',
                optionValue: 'id_tipoocorrencia',
                optionSelected: ''
            }
        });
        viewSelectTipoOcorrencia.render();

        //combo assunto
        var viewSelectAssuntos = new SelectView({
            el: this.ui.assunto,
            collection: VwAssuntosCollection,
            childViewOptions: {
                optionLabel: 'st_assuntoco',
                optionValue: 'id_assuntoco',
                optionSelected: ''
            }
        });
        viewSelectAssuntos.render();

        //Inicializa componente TinyMCE
        tinymce.init({
            selector: "#txt-ocorrencia",
            plugins: "paste",
            theme_advanced_buttons3_add: "pastetext,pasteword,selectall",
            paste_auto_cleanup_on_paste: true,
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        });
    },
    gravarNovaOcorrencia: function () {
        var that = this;
        var pessoa = JSON.parse(localStorage.getItem('pessoaAutocomplete'));

        // Validacao dos cmapos obrigatorios
        if (pessoa == null || tinymce.editors["txt-ocorrencia"].getContent() == '' || $('#sub-assunto-holder').val() == '' || $('#assunto-holder').val() == '' || $('#id_matricula').val() == '') {
            $.pnotify({
                type: 'warning',
                title: 'Atenção',
                text: 'Preencha os campos obrigatórios antes de prosseguir'
            });
            return false;
        }
        var oc = new Ocorrencia();
        oc.set({
            st_titulo: $('#st_tituloocorrencia').val(),
            st_ocorrencia: tinymce.editors["txt-ocorrencia"].getContent(),
            id_assuntoco: $('#sub-assunto-holder').val(),
            id_assuntocopai: $('#assunto-holder').val(),
            id_categoriaocorrencia: $('#tipo-ocorrencia-holder').val(),
            id_usuariointeressado: pessoa.id_usuario,
            id_entidade: G2S.loggedUser.get('id_entidade'),
            id_usuariocadastro: G2S.loggedUser.get('id_usuario'),
            id_evolucao: 28,
            id_situacao: 97,
            // UPDATE AC-26658 - 12/01/2015
            id_matricula: Number($('#id_matricula').val())
        });

        loading();
        $('#form-nova-ocorrencia').ajaxSubmit({
            url: '/api/ocorrencia-aluno',
            type: 'post',
            dataType: 'json',
            data: {ocorrencia: oc.toJSON()},
            uploadProgress: function (event, position, total, percentComplete) {
                $('.box-bar-progress').show();
                $('#upload-progress-ocorrencia').css({width: percentComplete + '%'});
            },
            success: function (data) {

                $.pnotify({
                    title: data.title,
                    text: "Ocorrência <a style='color:#000' href='/#ocorrencia/" + data.id_ocorrencia + "'>" + data.id_ocorrencia + "</a> criada com Sucesso!",
                    type: 'success'
                });
                if (data.id_ocorrencia) {
                    that.ui.formnova.trigger('reset');
                } else {
                    $.pnotify({title: data.title, text: data.mensagem, type: 'warning'});
                }
                loaded();
            },
            error: function (data) {
                loaded();
                $.pnotify({
                    title: 'Erro',
                    text: data.responseJSON.msg,
                    type: 'error'
                });
            }
        });

    },
    cancelarOcorrencia: function () {
        var that = this;
        if (tinymce.editors["txt-ocorrencia"].getContent() !== "") {
            bootbox.confirm("Deseja cancelar a criação dessa ocorrência? O Conteúdo já digitado será perdido.", function (result) {
                if (result) {
                    that.ui.formnova.trigger('reset');
                }
            });
        } else {
            that.ui.formnova.trigger('reset');
        }
    },
    events: {
        'click .item-autocomplete': function () {
            var pessoa = JSON.parse(localStorage.getItem('pessoaAutocomplete'));
            this.newOcorrenciaAluno(pessoa);
        },
        "change #assunto-holder": function () {
            loading();
            var VwSubAssuntosCollection = new VwSubAssuntosCollectionExtend();
            VwSubAssuntosCollection.url += 'id_assuntocopai/' + $('#assunto-holder').val();
            VwSubAssuntosCollection.fetch();

            this.ui.subassunto.html('');
            var viewSelectSubAssuntos = new SelectView({
                el: this.ui.subassunto,
                collection: VwSubAssuntosCollection,
                childViewOptions: {
                    optionLabel: 'st_assuntoco',
                    optionValue: 'id_assuntoco',
                    optionSelected: ''
                }
            });

            viewSelectSubAssuntos.render();
            loaded();

        },
        "click #btn-gerar-ocorrencia": "gravarNovaOcorrencia",
        "click #btn-cancelar-ocorrencia": "cancelarOcorrencia"
    },
    initialize: function() {

    }
});

$(function () {
    G2S.layout.content.show(new OcorrenciaAlunoCompositeView());
});
var layoutLancarAproveitamento,
    viewMatricula,
    lancamentoPaiComposite,
    collectionOcorrenciasSelec,
    collectionMatriculas,
    matricula,
    collectionOcorrencias,
    permissaoEditar,
    collectionDisciplinaOrigem;

//MODELOS DEFINIDAS

var ModelDados = Backbone.Model.extend({
    defaults: {
        id_usuario: '',
        st_nomecompleto: '',
        st_nomemae: '',
        st_email: '',
        st_cpf: ''
    }
});

var ModeloModulos = Backbone.Model.extend({
    attributes: {
        id: '',
        nome: '',
        disciplinas: []
    }
});

var modelDados = new ModelDados();

/**
 * COLLECTIONS
 */
var MatriculaCollection = Backbone.Collection.extend({
    model: VwMatricula,
    url: '/api/matricula'
});

var OcorrenciaCollection = Backbone.Collection.extend({
    model: Ocorrencia,
    url: '/lancar-aproveitamento/retorna-ocorrencia-by-matricula'
});

var CollectionModulos = Backbone.Collection.extend({
    collection: ModeloModulos,
    url: '/lancar-aproveitamento/retorna-modulos-disciplinas-aproveitamento'
});


/**
 * Collection definida para receber cada modelo de disciplina alterada
 * @type {Backbone.Collection}
 */
var collectionDisciplinasSelecionadas = new Backbone.Collection();


/**
 * LAYOUT PRINCIPAL
 */
var LayoutLancarAproveitamento = Marionette.LayoutView.extend({
    template: '#layout-lancar-aproveitamento',
    tagName: 'div',
    autoComplete: componenteAutoComplete,
    regions: {
        matriculaRegion: '#container-seleciona-matricula',
        ocorrenciaRegion: '#container-manipula-ocorrencia',
        disciplinaOrigemRegion: '#container-disciplinas-origem',
        aproveitamentoRegion: '#wrapper-lancar-aproveitamento',
        botoesRegion: '#wrapper-botoes'
    },
    ui: {},
    events: {
        'click #btn-adicionar-pessoa': 'adicionarPessoa',
        'click #btn-ocultar-form': 'ocultarFormulario'
    },
    initialize: function () {
        this.autoComplete.clearFixModel();

        VerificarPerfilPermissao(43, 744, true).done(function (response) {
            permissaoEditar = !!response.length;
        });
    },
    onShow: function () {
        var that = this;
        loading();
        var autocomplete = this.autoComplete;

        // G2S.editModel não encontrado
        //Chama o componente de autocomplete
        autocomplete.main({
            model: VwPesquisarPessoa,
            element: $("#autoComplete")
        }, function () {
            pessoaFixada = autocomplete.getFixModel();
            modelDados.set({
                id_usuario: pessoaFixada.id_usuario,
                st_nomecompleto: pessoaFixada.st_nomecompleto,
                st_nomemae: pessoaFixada.st_nomemae,
                st_email: pessoaFixada.st_email,
                st_cpf: pessoaFixada.st_cpf
            });
            abreNegociacao();

            //Oculta o botão de incluir nova pessoa
            that.$el.find('#btn-adicionar-pessoa').css({
                'display': 'none'
            });
        }, function () {
            fechaNegociacao();
        });

        if (!autocomplete.getFixModel()) {
            $('.content-box-vendas').hide('fast');
        }

        loaded();
        return this;
    },
    ocultarFormulario: function () {
        //this.adicionarPessoaRegion.destroy();
        var adicionaPessoa = $('#container-adicionar-pessoa');

        adicionaPessoa.empty().css({
            'display': 'none'
        });

        $("#btn-ocultar-form").css({
            'display': 'none'
        });
    }
});

var ViewMatricula = Marionette.ItemView.extend({
    template: '#view-informacoes-aproveitamento',
    tagName: 'div',
    className: 'well well-large clearfix',
    model: modelDados,
    ui: {
        selectMatricula: '#id_matricula'
    },
    events: {
        'change @ui.selectMatricula': 'executaMatricula'
    },
    onRender: function () {
        this.populaSelectMatricula();
        return this;
    },
    populaSelectMatricula: function () {
        var that = this;

        collectionMatriculas = new MatriculaCollection();
        //Buscando na controller de cancelamento porque ja retorna no formato necessário.
        collectionMatriculas.url = '/cancelamento/retorna-matriculas/id/' + modelDados.get('id_usuario');
        collectionMatriculas.fetch({
            beforeSend: loading,
            success: function () {
                new SelectView({
                    el: that.ui.selectMatricula,
                    collection: collectionMatriculas,
                    childViewOptions: {
                        optionLabel: 'st_labelselect',
                        optionValue: 'id_matricula',
                        optionSelected: null
                    }
                }).render();
            },
            error: function () {
                $.pnotify({
                    type: 'warning',
                    title: 'Erro!',
                    text: 'Erro ao carregar matrículas.'
                });
            },
            complete: loaded
        });
    },
    executaMatricula: function () {
        var id_matricula = this.ui.selectMatricula.val();

        if (!id_matricula) {
            telaAproveitamento.ocorrenciaRegion.reset();
            telaAproveitamento.aproveitamentoRegion.reset();
            telaAproveitamento.botoesRegion.reset();
            telaAproveitamento.disciplinaOrigemRegion.reset();
            return;
        }

        matricula = collectionMatriculas.where({
            id_matricula: parseInt(id_matricula)
        });

        lancamentoPaiComposite = new LancamentosPaiComposite({
            collection: new CollectionModulos
        });

        //ocorrenciaComposite = new OcorrenciaComposite;
        telaAproveitamento.ocorrenciaRegion.show(new OcorrenciaComposite);
        telaAproveitamento.aproveitamentoRegion.show(lancamentoPaiComposite);
        telaAproveitamento.botoesRegion.show(new ViewBotoes);
        telaAproveitamento.disciplinaOrigemRegion.show(new DisciplinaOrigemComposite);
    }
});

var OcorrenciaComposite = Marionette.ItemView.extend({
    template: '#view-manipula-ocorrencia',
    tagName: 'div',
    ui: {
        ocorrenciasRelacionadasComponente: '#ocorrenciasRelacionadas'
    },
    events: {},
    onShow: function () {
        var that = this;
        collectionOcorrencias = new OcorrenciaCollection();
        collectionOcorrenciasSelec = new OcorrenciaCollection();
        collectionOcorrenciasSelec.url = '/lancar-aproveitamento/retorna-ocorrencias-relacionadas-aproveitamento';

        var myItem = new SideBySideView({
            collections: {
                left: collectionOcorrencias,
                right: collectionOcorrenciasSelec
            },
            label: 'st_tituloOcorrencia',
            label_search: 'id_ocorrencia',
            idAttributeGlobal: 'id_ocorrencia',
            fetch: false,
            addAll: true,
            removeAll: false,
            globalSearch: true,
            confirmRemove: true
        });
        that.ui.ocorrenciasRelacionadasComponente.html(myItem.render().$el);


        $.when(collectionOcorrencias.fetch({
            async: true,
            data: {id_matricula: matricula[0].get('id_matricula')}
        }), collectionOcorrenciasSelec.fetch({
            async: true,
            data: {
                id_aproveitamento: matricula[0].get('id_aproveitamento') ? matricula[0].get('id_aproveitamento') : null,
                bl_ativo: true
            }
        })).done(function () {
            collectionOcorrencias.remove(collectionOcorrenciasSelec.toJSON());
        });
    }
});

/**
 * Disciplinas de Origem selecionadas
 */
var MatriculaDisciplinaOrigemModel = Backbone.Model.extend({
    idAttribute: 'id_discmatriculadiscorigem',
    defaults: {
        id_discmatriculadiscorigem: null,
        id_disciplinaorigem: null,
        st_disciplina: null
    }
});

var MatriculaDisciplinaOrigemView = Marionette.ItemView.extend({
    model: new MatriculaDisciplinaOrigemModel(),
    template: "#item-disciplinas-origem",
    ui: {
        inputDisciplina: 'select',
        btnDelete: '.link-delete'
    },
    events: {
        "change @ui.inputDisciplina": "setValue",
        "click @ui.btnDelete": "actionRemove"
    },
    onRender: function () {
        var that = this;

        // Se já tiver o ID, então desabilitar o campo
        this.ui.inputDisciplina.prop('disabled', this.model.get('id_discmatriculadiscorigem'));

        if (!this.model.get('id_disciplinaorigem')) {
            this.ui.btnDelete.prop('disabled', false);
        }

        (new SelectView({
            el: that.ui.inputDisciplina,
            collection: collectionDisciplinaOrigem,
            childViewOptions: {
                optionLabel: 'st_disciplina',
                optionValue: 'id_disciplinaorigem',
                optionSelected: that.model.get('id_disciplinaorigem')
            },
            clearView: true
        })).render();
    },
    setValue: function () {
        var id_disciplinaorigem = parseInt(this.ui.inputDisciplina.val());
        var st_disciplina = this.ui.inputDisciplina.find('option:selected').text();

        var $container = this.$el.closest('.container-disciplinas-origem');

        var $exists = $container.find('select').not(this.ui.inputDisciplina).filter(function () {
            return parseInt($(this).val()) === id_disciplinaorigem;
        });

        if ($exists.length) {
            $.pnotify({
                type: 'warning',
                title: 'Aviso',
                text: 'Esta disciplina de origem já foi adicionada.'
            });

            this.ui.inputDisciplina.val('');
        }

        this.model.set({
            id_disciplinaorigem: id_disciplinaorigem,
            st_disciplina: st_disciplina
        });
    },
    actionRemove: function () {
        this.model.collection.remove(this.model);
    }
});

/**
 * Item view de disciplina
 */
var DisciplinaItem = Marionette.CompositeView.extend({
    template: '#item-disciplinas',
    tagName: 'tr',
    childView: MatriculaDisciplinaOrigemView,
    childViewContainer: '.container-disciplinas-origem',
    ui: {
        chkbox_disciplina: '.chkbox_disciplina',
        sg_uf: 'select',
        disciplinaOrigem: '.input_disciplina',
        instituicao: '.input_instituicao',
        carga_horaria_aproveitamento: '.input_carga_horaria_aproveitamento',

        inputsDisciplinaOrigem: '.input_disciplinaorigem',
        btnAdd: '.link-add'
    },
    collectionEvents: {
        add: 'syncDisciplinasOrigem',
        remove: 'syncDisciplinasOrigem'
    },
    events: {
        'change @ui.chkbox_disciplina': 'changeChkBox',
        'change @ui.inputsDisciplinaOrigem': 'syncDisciplinasOrigem',
        'click @ui.btnAdd': 'actionAddDisciplinaOrigem'
    },
    initialize: function () {
        // Collection para carregar as disciplinas de Origem
        this.collection = new Backbone.Collection(this.model.get('disciplinas_origem'));
    },
    onRender: function () {
        this.model.set('checked', this.collection.length ? 1 : 0);
    },
    changeChkBox: function () {
        var $state = Number(this.ui.chkbox_disciplina.prop('checked'));

        this.model.set('checked', $state);
        collectionDisciplinasSelecionadas.add(this.model);

        if ($state) {
            this.habilitaCampos();
        } else {
            if (!this.model.get('id_aproveitamento')) {
                collectionDisciplinasSelecionadas.remove(this.model);
            }

            this.desabilitaCampos();
            this.$el.find('div').removeClass('control-group error');

            this.collection.reset();
        }
    },
    habilitaCampos: function () {
        if (!this.collection.length) {
            this.collection.add(new MatriculaDisciplinaOrigemModel());
        }

        this.bindUIElements();

        this.ui.inputsDisciplinaOrigem.prop('disabled', false);
        this.ui.btnAdd.prop('disabled', false);
    },
    desabilitaCampos: function () {
        this.bindUIElements();

        this.ui.inputsDisciplinaOrigem.prop('disabled', true);
        this.ui.btnAdd.prop('disabled', true);
    },
    actionAddDisciplinaOrigem: function () {
        model = new MatriculaDisciplinaOrigemModel();
        this.collection.add(model);
        this.$el.find("select:last").focus();
    },
    syncDisciplinasOrigem: function () {
        this.model.set('disciplinas_origem', this.collection.toJSON());
        collectionDisciplinasSelecionadas.add(this.model);
    }
});

/**
 * Composite filha, para cada módulo vai existir uma composite dessa
 */
var LancamentosComposite = Marionette.CompositeView.extend({
    template: '#view-modulos-aproveitamento',
    tagName: 'div',
    childView: DisciplinaItem,
    childViewContainer: 'tbody',
    initialize: function () {
        this.collection = new Backbone.Collection(this.model.get('disciplinas'));
    }
});

/**
 * Composite pai, vai conter as várias composites referentes aos modulos
 */
var LancamentosPaiComposite = Marionette.CompositeView.extend({
    template: '#template-lancamentos-pai',
    tagName: 'div',
    childView: LancamentosComposite,
    childViewContainer: '#container-filhos',
    initialize: function () {
        this.fetchModulos();
    },
    fetchModulos: function () {
        this.collection.fetch({
            data: {
                data: {
                    id_matricula: matricula[0].get('id_matricula')
                }
            },
            beforeSend: loading,
            complete: loaded
        });
    }
});

/**
 * View que vai conter os botões e suas ações.
 */
var ViewBotoes = Marionette.ItemView.extend({
    template: '#template-botoes-aproveitamento',
    tagName: 'fieldset',
    ui: {
        btn_salvar: '#btn_salvar',
        btn_editar: '#btn_editar',
        btn_cancelar: '#btn_cancelar'
    },
    events: {
        'click @ui.btn_salvar': 'save',
        'click @ui.btn_editar': 'editar',
        'click @ui.btn_cancelar': 'cancelar'
    },
    onRender: function () {
        if (permissaoEditar) {
            this.ui.btn_editar.removeClass('hide');
        }
    },
    validaAproveitamento: function () {
        var is_valid = true;
        var $el_error = null;

        _.each(collectionDisciplinasSelecionadas.models, function (model) {
            if (model.get('checked')) {
                var $div = $("#divDisciplinaOrigem_" + model.get('id_disciplina'));

                var has_value = $div.find("select").filter(function () {
                    var $el = $(this);
                    var $parent = $el.closest('div');

                    if ($el.val()) {
                        $parent.removeClass('control-group error');
                        return true;
                    }

                    if (!$el_error) {
                        $el_error = $el;
                    }

                    $parent.addClass('control-group error');
                    return false;
                }).length;

                var has_empty = $div.find("select").filter(function () {
                    return !$(this).val();
                }).length;

                if (!has_value || has_empty) {
                    is_valid = false;
                }
            }
        });

        if (!is_valid) {
            if ($el_error) {
                $el_error.focus();
            }

            $.pnotify({
                title: 'Atenção',
                text: 'Verifique os campos em destaque.',
                type: 'warning'
            });
        }

        if (!collectionOcorrenciasSelec.length) {
            is_valid = false;
            $.pnotify({
                title: 'Atenção',
                text: 'Deve haver pelo menos uma ocorrência relacionada a este aproveitamento.',
                type: 'warning'
            });
        }

        return is_valid;
    },
    save: function () {
        var that = this;

        if (this.validaAproveitamento()) {

            $.ajax({
                dataType: 'json',
                type: 'post',
                url: '/lancar-aproveitamento/save',
                async: false,
                data: {
                    ocorrencias: collectionOcorrenciasSelec.toJSON(),
                    disciplinas: collectionDisciplinasSelecionadas.toJSON(),
                    id_matricula: matricula[0].get('id_matricula'),
                    id_aproveitamento: matricula[0].get('id_aproveitamento') ? matricula[0].get('id_aproveitamento') : null

                },
                beforeSend: function () {
                    loading();
                },
                success: function (data) {
                    $.pnotify({
                        title: data.title,
                        text: data.mensagem,
                        type: data.type
                    });

                    lancamentoPaiComposite = new LancamentosPaiComposite({
                        collection: new CollectionModulos
                    });
                    telaAproveitamento.aproveitamentoRegion.show(lancamentoPaiComposite);

                    _.each(collectionDisciplinasSelecionadas.models, function (model) {
                        $('#chk_' + model.get('id_disciplina')).prop('disabled', true);
                        $("#divDisciplinaOrigem_" + model.get('id_disciplina') + " *:input").prop('disabled', true)
                    });

                    _.each(collectionOcorrenciasSelec.models, function (model) {
                        model.set({editable: false});
                        $("tr[data-label='" + model.get('st_tituloOcorrencia').trim().removeAccents().toLowerCase() + "']").find('button').attr('disabled', true);
                    });
                    if (permissaoEditar) {
                        that.ui.btn_editar.removeClass('hide');
                        that.ui.btn_cancelar.addClass('hide');
                    }
                },
                complete: loaded,
                error: function () {
                    $.pnotify({
                        title: 'Erro!',
                        text: 'Erro ao atualizar as informações.',
                        type: 'error'
                    });
                }
            });
        }
    },
    editar: function () {

        $('html, body').animate({
            scrollTop: $('#divEquivalencia').offset().top
        });

        $('.chkbox_disciplina:disabled, .divDisciplinaOrigem :input:disabled').prop('disabled', false);

        // _.each(collectionDisciplinasSelecionadas.models, function (model) {
        //     $('#chk_' + model.get('id_disciplina')).prop('disabled', false);
        //     $("#divDisciplinaOrigem_" + model.get('id_disciplina') + " *:input").prop('disabled', false);
        // });

        if (permissaoEditar) {
            this.ui.btn_editar.addClass('hide');
            this.ui.btn_cancelar.removeClass('hide');
        }
    },
    cancelar: function () {
        lancamentoPaiComposite = new LancamentosPaiComposite({
            collection: new CollectionModulos
        });
        telaAproveitamento.aproveitamentoRegion.show(lancamentoPaiComposite);

        // _.each(collectionDisciplinasSelecionadas.models, function (model) {
        //     $('#chk_' + model.get('id_disciplina')).prop('disabled', true);
        //     $("#divDisciplinaOrigem_" + model.get('id_disciplina') + " *:input").prop('disabled', true)
        // });

        $('.chkbox_disciplina:disabled, .divDisciplinaOrigem :input:disabled').prop('disabled', true);

        if (permissaoEditar) {
            this.ui.btn_editar.removeClass('hide');
            this.ui.btn_cancelar.addClass('hide');
        }
    }
});

/**
 * Abre a tela de negociação caso um usuário seja selecionado no autocomplete,
 * ou venha selecionado para edição da venda.
 */
function abreNegociacao() {
    $('.content-box-vendas').show('fast');
    viewMatricula = new ViewMatricula();
    telaAproveitamento.matriculaRegion.show(viewMatricula);
}

/**
 * Fecha a tela de negociação.
 */
function fechaNegociacao() {
    telaAproveitamento.matriculaRegion.empty();
    telaAproveitamento.ocorrenciaRegion.empty();
    telaAproveitamento.aproveitamentoRegion.empty();
    telaAproveitamento.disciplinaOrigemRegion.empty();
    $('.content-box-vendas').hide('fast');
}

layoutLancarAproveitamento = new LayoutLancarAproveitamento();
var telaAproveitamento = layoutLancarAproveitamento;
G2S.show(telaAproveitamento);
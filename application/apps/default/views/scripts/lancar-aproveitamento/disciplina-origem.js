var DisciplinaOrigemModel = Backbone.Model.extend({
    idAttribute: "id_disciplinaorigem",
    isEditing: false,
    toggleEdit: function (value) {
        this.isEditing = (typeof value !== "undefined") ? value : !this.isEditing;
    },
    url: function () {
        return "/api/disciplina-origem/" + (this.id || "")
    },
    defaults: {
        bl_ativo: true,
        nu_cargahoraria: null,
        sg_uf: null,
        st_disciplina: null,
        st_instituicao: null
    },
    validation: {
        st_disciplina: {
            required: true,
            msg: "O nome da disciplina é obrigatório."
        },
        nu_cargahoraria: {
            required: true,
            pattern: "number",
            msg: "O campo carga horária é obrigatório."
        },
        st_instituicao: {
            required: true,
            msg: "O nome da instituição é obrigatório."
        },
        sg_uf: {
            required: true,
            msg: "O campo UF é obrigatório."
        }
    }
});

var DisciplinaOrigemCollection = Backbone.Collection.extend({
    url: "/api/disciplina-origem",
    model: DisciplinaOrigemModel
});

var DisciplinaOrigemLoading = Marionette.ItemView.extend({
    tagName: "tr",
    template: _.template("<td colspan='5'><div class='alert alert-info no-margin'>Buscando dados...</div></td>")
});

var DisciplinaOrigemEmpty = Marionette.ItemView.extend({
    tagName: "tr",
    template: _.template("<td colspan='5'>Nenhuma disciplina cadastrada.</td>")
});

var DisciplinaOrigemView = Marionette.ItemView.extend({
    tagName: "tr",
    template: "#template-item-disciplina-origem-list",
    msgError: false,
    ui: {
        inputDisciplina: "input[name=st_disciplina]",
        inputCargaHoraria: "input[name=nu_cargahoraria]",
        inputInstituicao: "input[name=st_instituicao]",
        inputUf: "select[name=sg_uf]",
        btnEdit: ".edit-link",
        btnDelete: ".delete-link",
        btnCancel: ".cancel-link",
        btnSave: ".save-link"
    },
    events: {
        "click @ui.btnEdit": "actionEdit",
        "click @ui.btnDelete": "actionDelete",
        "click @ui.btnCancel": "actionCancel",
        "click @ui.btnSave": "actionSave"
    },
    initialize: function () {
        var that = this;

        Backbone.Validation.bind(this, {
            invalid: function (view, attr, error) {
                // Condição usada para mostrar somente uma mensagem de erro de cada vez do Backbone Validation (evitar spam na tela)
                if (!that.msgError) {
                    that.msgError = true;

                    $.pnotify({
                        type: "warning",
                        title: "Aviso",
                        text: error
                    });

                    that.$el.find(":input[name=" + attr + "]").focus();
                }
            }
        });
    },
    onBeforeRender: function () {
        this.template = this.model.isEditing ? "#template-item-disciplina-origem-add-edit" : "#template-item-disciplina-origem-list";
    },
    onRender: function () {
        if (!this.model.get("bl_ativo")) {
            this.model.collection.remove(this.model);
            return;
        }

        this.ui.inputUf.val(this.model.get("sg_uf"));
    },
    actionEdit: function () {
        this.model.toggleEdit(true);
        this.render();
    },
    actionDelete: function () {
        var that = this;

        bootbox.confirm("Deseja remover o registro?", function (response) {
            if (response) {
                that.model.set("bl_ativo", false);
                that.actionSave();
            }
        });
    },
    actionCancel: function () {
        this.model.toggleEdit(false);

        if (!this.model.id) {
            this.model.collection.remove(this.model);
        } else {
            this.render();
        }
    },
    actionSave: function () {
        var data = Backbone.Syphon.serialize(this);
        data.id_aluno = modelDados.get("id_usuario");

        // Usado para mostrar somente uma mensagem de erro de cada vez do Backbone Validation (evitar spam na tela)
        this.msgError = false;

        var message = "Os dados foram salvos corretamente.";

        this.model.save(data, {
            beforeSend: loading,
            success: _.bind(function () {
                this.model.toggleEdit(false);
                this.render();
                lancamentoPaiComposite.render();

                // Se estiver removido o registro, alterar a mensagem de retorno
                if (!this.model.get('bl_ativo')) {
                    message = "O registro foi removido com sucesso.";
                }

                $.pnotify({
                    type: "success",
                    title: "Sucesso",
                    text: message
                });
            }, this),
            error: function (model, response) {
                var message = "Não foi possível salvar os dados.";

                if (response && response.responseJSON && response.responseJSON.text) {
                    message = response.responseJSON.text;
                }

                $.pnotify({
                    type: "error",
                    title: "Erro",
                    text: message
                });
            },
            complete: loaded
        });
    }
});

var DisciplinaOrigemComposite = Marionette.CompositeView.extend({
    template: "#template-disciplina-origem",
    collection: new DisciplinaOrigemCollection(),
    childView: DisciplinaOrigemView,
    emptyView: DisciplinaOrigemLoading,
    childViewContainer: "tbody",
    ui: {
        btnAddDisc: "#btn-add-disc-origem"
    },
    events: {
        "click @ui.btnAddDisc": "addDisciplina"
    },
    collectionEvents: {
        remove: 'onFetch'
    },
    initialize: function () {
        var that = this;

        collectionDisciplinaOrigem = this.collection;

        this.collection.fetch({
            data: {
                id_aluno: modelDados.get("id_usuario"),
                bl_ativo: true
            },
            beforeSend: loading,
            complete: function () {
                loaded();
                that.onFetch();
            }
        })
    },
    onFetch: function () {
        if (this.collection.length === 0) {
            this.emptyView = DisciplinaOrigemEmpty;
            this.render();
        }
    },
    addDisciplina: function () {
        // Verificar se já tem um model vazio
        var model = this.collection.find(function (model) {
            return !model.id;
        });

        if (!model) {
            model = new this.collection.model;
        }

        model.toggleEdit(true);

        this.collection.add(model);
        this.$el.find("tbody tr:last input:first").focus();
    }
});
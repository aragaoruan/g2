/****************************************************
 **************   MODEL HISTORICO  ******************
 ****************************************************/
var HistoricoModel = Backbone.Model.extend({
    idAttribute: "id_log"
});
/****************************************************
 **************   COLLECTION HISTORICO  *************
 ****************************************************/
var HistoricoCollection = Backbone.Collection.extend({
    url: "/log/historico-atividade-complementar",
    model: HistoricoModel
});


var AtividadesComplementarCollection = Backbone.Collection.extend({
    model: AtividadeComplementar
});

var ProjetosCollection = Backbone.Collection.extend({
    model: VwProjetoEntidade,
    url: "/api/vw-projeto-entidade"
});

var SituacaoCollection = Backbone.Collection.extend({
    model: Situacao,
    url: "/api/situacao"
});

var TipoAtividadeCollection = Backbone.Collection.extend({
    model: TipoAtividadeComplementar,
    url: "/api/tipo-atividade-complementar"
});

var FormPesquisaAtividade = Marionette.ItemView.extend({
    template: "#template-formulario",
    ui: {
        "cursoSelect": "#curso",
        "statusSelect": "#status",
        "tipoAtividadeSelect": "#tipo_atividade",
        "nomeInput": "#nome",
        "cpfInput": "#cpf",
        "btnPesquisar": "#btnPesquisar",
        "formPesquisaAtividade": "#formPesquisaAtividade"
    },
    events: {
        "submit @ui.formPesquisaAtividade": "pesquisarAtividade",
        "change @ui.cursoSelect, @ui.statusSelect": "habilitarPesquisa"
    },
    onShow: function () {
        this.habilitarPesquisa();

        //carrega os dados dos selects
        this.carregarProjetos();
        this.carregarSituacao();
        this.carregarTipoAtividade();
    },
    habilitarPesquisa: function () {
        if (this.ui.cursoSelect.val() && this.ui.statusSelect.val()) {
            this.ui.btnPesquisar.removeAttr("disabled");
            return true;
        }

        this.ui.btnPesquisar.attr("disabled", "disabled");
        return false;
    },
    carregarTipoAtividade: function () {
        var tipoAtividade = new TipoAtividadeCollection();
        tipoAtividade.fetch({
            beforeSend: loading,
            complete: loaded,
            success: function (collection) {
                (new SelectView({
                    el: this.ui.tipoAtividadeSelect,
                    collection: collection,
                    sort: "st_tipoatividadecomplementar",
                    childViewOptions: {
                        optionLabel: "st_tipoatividadecomplementar",
                        optionValue: "id_tipoatividadecomplementar",
                        optionSelected: null
                    }
                })).render();

            }.bind(this)
        });
    },
    carregarSituacao: function () {
        (new SituacaoCollection())
            .fetch({
                data: {
                    "st_tabela": "tb_atividadecomplementar"
                },
                beforeSend: loading,
                complete: loaded,
                success: _.bind(function (collection) {
                    (new SelectView({
                        el: this.ui.statusSelect,
                        collection: collection,
                        sort: "st_situacao",
                        childViewOptions: {
                            optionLabel: "st_situacao",
                            optionValue: "id_situacao",
                            optionSelected: null
                        }
                    })).render();

                }, this)
            });
    },
    carregarProjetos: function () {
        var projetos = new ProjetosCollection();
        projetos.fetch({
            data: {
                "id_entidade": G2S.loggedUser.get("id_entidade"),
                "bl_ativo": true
            },
            beforeSend: loading,
            complete: loaded,
            success: function (collection) {
                if (!collection.length) {
                    $.pnotify({
                        "title": "Atenção!",
                        "type": "warning",
                        "text": "Não foi encontrado nenhum curso."
                    });
                }
                (new SelectView({
                    el: this.ui.cursoSelect,
                    collection: collection,
                    childViewOptions: {
                        optionLabel: "st_projetopedagogico",
                        optionValue: "id_projetopedagogico",
                        optionSelected: null
                    }
                })).render();


            }.bind(this)
        });
    },
    pesquisarAtividade: function () {
        if (!this.habilitarPesquisa()) {
            $.pnotify({
                "title": "Atenção!",
                "type": "warning",
                "text": "Verifique os campos obrigatórios."
            });
            return;
        }

        // Busca os valores no forumario e faz o serialize.
        var values = Backbone.Syphon.serialize(this);

        (new AtividadesComplementarCollection())
            .fetch({
                url: "/atividade-complementar/retornar-atividades",
                data: values,
                beforeSend: loading,
                complete: loaded,
                error: function (collection, response) {
                    if (parseInt(response.status) === 404) {
                        $.pnotify({
                            title: "Atenção!",
                            type: "warning",
                            text: response.responseText
                        });
                        telaAtividades.getRegion("tabelaAtividade").reset();
                        return;
                    }

                    $.pnotify({
                        title: "Erro!",
                        type: "error",
                        text: "Erro ao consultar dados. " + response.responseText
                    });
                },
                success: _.bind(function (collection) {
                    var atividadeView = new AtividadesComplementaresView({
                        collection: collection
                    });
                    telaAtividades.getRegion("tabelaAtividade").show(atividadeView);

                }, this)
            });
        return false;

    }
});

var EmptyView = Marionette.ItemView.extend({
    template: "#template-lista-vazia",
    tagName: "tr",
    onRender: function () {
        if (typeof this._parent.colspan !== "undefined") {
            this.$el.find("td").attr("colspan", this._parent.colspan);
        }
    }
});

var AtividadeItemView = Marionette.ItemView.extend({
    template: "#template-row-atividade",
    tagName: "tr",
    ui: {
        "btnAvaliar": ".btn-avaliar",
        "btnEditar": ".btn-editar",
        "btnHistorico": ".btn-historico",
        "btnResumo": ".btn-resumo"
    },
    events: {
        "click @ui.btnResumo": "mostrarResumo",
        "click @ui.btnHistorico": "carregarHistorico",
        "click @ui.btnAvaliar, @ui.btnEditar": "editarAtividade"
    },
    initialize: function () {
        //formata o cpf
        var cpf = this.model.get("st_cpf")
            .replace(/\D/g, '')
            .replace(/^(\d{3})(\d{3})(\d{3})(\d{2})/, "$1.$2.$3-$4");
        this.model.set("st_cpf", cpf);
        return this;
    },
    mostrarResumo: function () {
        this._parent.$el.find("#modalResumo").find(".modal-body")
            .html(nl2br(this.model.get("st_resumoatividade")));
        this._parent.$el.find("#modalResumo").modal("show");
    },
    editarAtividade: function () {
        var modal = new ModalEditarAtividade({
            model: this.model
        });
        telaAtividades.getRegion("modalAtividade").show(modal);
    },
    carregarHistorico: function () {

        (new HistoricoCollection())
            .fetch({
                data: {
                    "id_atividadecomplementar": this.model.get("id_atividadecomplementar")
                },
                error: function (collection, response) {
                    if (parseInt(response.status) === 404) {
                        $.pnotify({
                            title: "Atenção!",
                            type: "warning",
                            text: response.responseText
                        });
                        return;
                    }

                    $.pnotify({
                        title: "Erro!",
                        type: "error",
                        text: "Erro ao consultar dados. " + response.responseText
                    });
                },
                beforeSend: loading,
                complete: loaded,
                success: _.bind(function (collection) {
                    var historico = new HistoricoCompositeView({
                        collection: collection
                    });
                    telaAtividades.getRegion("modalAtividade").show(historico);
                }, this)
            });
    }
});

var AtividadesComplementaresView = Marionette.CompositeView.extend({
    template: "#template-tabela-atividades",
    emptyView: EmptyView,
    childView: AtividadeItemView,
    colspan: 11,
    childViewContainer: "tbody"
});

var HistoricoItemView = Marionette.ItemView.extend({
    template: '#template-historico-lista',
    tagName: 'tr'
});

var HistoricoCompositeView = Marionette.CompositeView.extend({
    template: "#tempalte-modal-historico",
    tagName: "div",
    className: "modal hide fade",
    childView: HistoricoItemView,
    emptyView: EmptyView,
    childViewContainer: "tbody",
    colspan: 3,
    onShow: function () {
        this.$el.modal("show");
        return this;
    }
});


var ModalEditarAtividade = Marionette.ItemView.extend({
    template: "#tempalte-modal-atividade",
    tagName: "div",
    className: "modal hide fade",
    ui: {
        formEditarAtividade: "#formEditarAtividade",
        stTituloAtividade: "#st_tituloatividade",
        idSituacao: "input[name='id_situacao']",
        nuHorasConvalidada: "#nu_horasconvalidada",
        stObservacaoAvaliador: "#st_observacaoavaliador",
        contadorResumo: "#contador-resumo",
        btnSalvarDados: "#btnSalvarDados"
    },
    events: {
        "keyup @ui.stObservacaoAvaliador": "contarCaracteres",
        "change @ui.idSituacao": "toggleCampoHoras",
        "keyup @ui.nuHorasConvalidada": "validaHoras",
        "change @ui.stTituloAtividade, @ui.idSituacao, @ui.nuHorasConvalidada": "validarSalvar",
        "submit @ui.formEditarAtividade": "salvarDados"
    },
    onRender: function () {
        this.$el.modal("show");
        this.carregarSituacoes();

        if (parseInt(this.model.get("id_situacao")) !== 200) {
            this.ui.nuHorasConvalidada.attr("disabled", "disabled");
        }

        this.verificarTotalHoras();
    },
    verificarTotalHoras: function () {

        //clona a coleção da tabela com todas as atividades
        var collectionAtividades = telaAtividades.getRegion("tabelaAtividade").currentView.collection.clone();
        return collectionAtividades.fetch({
            url: "/atividade-complementar/retornar-atividades",
            data: {
                "id_matricula": this.model.get("id_matricula"),
                "id_situacao": SITUACAO.TB_ATIVIDADECOMPLEMENTAR.DEFERIDO
            },
            beforeSend: loading,
            complete: loaded,
            success: function () {
                //inicia a variavel para incrementar o somatório das horas
                var horasAtividades = 0;

                //percorre o array de model somando as horas
                collectionAtividades.each(function (model) {
                    var currentEachAtividade = parseInt(model.get("id_atividadecomplementar"));
                    var currentAtividade = parseInt(this.model.get("id_atividadecomplementar"));
                    if (currentEachAtividade !== currentAtividade) {
                        horasAtividades += parseInt(model.get("nu_horasconvalidada"));
                    }
                }.bind(this));

                this.horasAtividadesPostadas = horasAtividades;

            }.bind(this)
        });
    },
    validaHoras: function () {
        var valid = true,
            text = "",
            horas = parseInt(this.ui.nuHorasConvalidada.val()),
            totalHorasAtividades = this.horasAtividadesPostadas + horas,
            horasProjeto = parseInt(this.model.get("nu_horasatividades"));

        if (parseInt(this.$el.find("input[name='id_situacao']:checked").val()) === 200 && !horas) {
            valid = false;
        }

        if (horas < 0) {
            text += "Não é permitido inserir horas negativas.<br>";
            this.ui.nuHorasConvalidada.focus().val("");
            valid = false;
        }

        if (totalHorasAtividades > horasProjeto) {
            //pega o total de horas da atividade + as horas postada inicialmente na atividade que esta sendo editada
            text += "A quantidade de horas inserida ultrapassa o valor máximo permitido." +
                " <br>Total de horas permitido " + horasProjeto +
                "<br>Total de horas postada "
                + (totalHorasAtividades + parseInt(this.model.get("nu_horasconvalidada") || 0));

            this.ui.nuHorasConvalidada.focus().val("");
            valid = false;
        }

        if (!valid) {
            $.pnotify({"title": "Atenção!", "type": "warning", "text": text});
        }

        return valid;

    },
    salvarDados: function (e) {
        e.preventDefault();

        if (!this.validarSalvar()) {
            $.pnotify({
                "title": "Atenção!",
                "type": "warning",
                "text": "Verifique os campos obrigatórios."
            });
            return false;
        }

        var situacao = parseInt(this.$el.find("input[name='id_situacao']:checked").val());
        this.model.set({
            "id_situacao": situacao,
            "st_tituloatividade": this.ui.stTituloAtividade.val(),
            "nu_horasconvalidada": situacao === 200 ? this.ui.nuHorasConvalidada.val() : 0,
            "st_observacaoavaliador": this.ui.stObservacaoAvaliador.val()
        }).save(null, {
            url: "atividade-complementar/salvar-avaliacao",
            beforeSend: loading,
            complete: loaded,
            error: function (collection, response) {
                $.pnotify({
                    title: "Erro!",
                    type: "error",
                    text: "Erro ao tentar salvar dados. " + response.responseText
                });
            },
            success: _.bind(function () {
                $.pnotify({
                    title: "Sucesso!",
                    type: "success",
                    text: "Atividade Avaliada com Sucesso!"
                });
                this.$el.modal("hide");
                telaAtividades.getRegion("formAtividade").currentView.pesquisarAtividade();
            }, this)

        });
    },
    validarSalvar: function () {
        var habilita = false;

        var situacao = this.$el.find("input[name='id_situacao']:checked").val();

        if (this.ui.stTituloAtividade.val().trim()
            && situacao
            && this.validaHoras()) {
            habilita = true;
        }
        if (habilita) {
            this.ui.btnSalvarDados.removeAttr("disabled");
            return habilita;
        }

        this.ui.btnSalvarDados.attr("disabled", "disabled");
        return habilita;

    },
    toggleCampoHoras: function () {
        if (parseInt(this.$el.find("input[name='id_situacao']:checked").val()) === 200) {
            this.ui.nuHorasConvalidada.removeAttr("disabled");
            return true;
        }

        this.ui.nuHorasConvalidada.attr("disabled", "disabled").val("");
        return false;
    },

    carregarSituacoes: function () {
        (new SituacaoCollection())
            .fetch({
                data: {
                    "st_tabela": "tb_atividadecomplementar"
                },
                beforeSend: loading,
                complete: loaded,
                success: _.bind(function (collection) {
                    var modelEmAnalise = collection.findWhere({"id_situacao": 202});
                    collection.remove(modelEmAnalise);
                    var situacoes = "";
                    var situacaoSelecionada = parseInt(this.model.get("id_situacao"));
                    _.each(collection.toJSON(), function (model) {
                        model.checked = (parseInt(model.id_situacao) === situacaoSelecionada);
                        situacoes += _.template($("#template-radio-button").html(), model);
                    });
                    this.$el.find("#container-situacoes").html(situacoes);
                    this.validarSalvar();

                }, this)
            });
    },
    contarCaracteres: function () {
        var currentLength = this.ui.stObservacaoAvaliador.val().length;
        var maxLength = parseInt(this.ui.stObservacaoAvaliador.attr("maxlength"));
        this.ui.contadorResumo.html((maxLength - currentLength) + " / " + maxLength);
    }
});


var TelaAtividadesLayout = Marionette.LayoutView.extend({
    template: "#template-regioes",
    tagName: "div",
    regions: {
        formAtividade: "#container-form",
        tabelaAtividade: "#container-result",
        modalAtividade: "#regiao-modal"
    },
    onShow: function () {
        var formulario = new FormPesquisaAtividade();
        this.getRegion("formAtividade").show(formulario);
    }
});

var telaAtividades = new TelaAtividadesLayout();
G2S.show(telaAtividades);

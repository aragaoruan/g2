var TipoAtividadeModel = TipoAtividadeComplementar.extend({
    isEditing: false,
    toggleEdit: function () {
        this.isEditing = !this.isEditing;
    },
    validation: {
        "st_tipoatividadecomplementar": {
            required: true,
            msg: "Nome do Tipo de Atividade Complementar é obrigatório."
        }
    }
});

var TipoAtividadeCollection = Backbone.Collection.extend({
    url: "/api/tipo-atividade-complementar",
    model: TipoAtividadeModel
});

var EmptyView = Marionette.ItemView.extend({
    template: "#template-lista-vazia",
    tagName: "tr"
});
var TipoAtividadeItemView = Marionette.ItemView.extend({
    template: "#template-item-listagem",
    tagName: "tr",
    ui: {
        "btnSalvar": ".save",
        "btnCancelar": ".cancel",
        "btnEditar": ".edit",
        "fielTitulo": "input[name='st_tipoatividade']"
    },
    events: {
        "click @ui.btnCancelar": "cancelarAcao",
        "click @ui.btnEditar": "editarRegistro",
        "click @ui.btnSalvar": "salvarRegistro"
    },
    initialize: function () {
        this.toggleTemplate();
        Backbone.Validation.bind(this, {
            invalid: function (view, attr, error) {
                $.pnotify({
                    type: "warning",
                    title: "Aviso",
                    text: error
                });
            }
        });

        if (this.model.get("id_tipoatividadecomplementar")) {
            this.model.id = this.model.get("id_tipoatividadecomplementar");
        }

        return this;
    },
    toggleTemplate: function () {
        this.template = "#template-item-listagem";
        //verifica se estou editando o registro (ou adicionando) e altera o template
        if (this.model.isEditing) {
            this.template = "#template-item-form";
        }
    },
    editarRegistro: function () {
        this.oldSettings = this.model.toJSON();
        this.model.toggleEdit();
        this.toggleTemplate();
        this.render();
    },
    cancelarAcao: function () {
        if (!this.model.id) {
            this.model.destroy();
            return;
        }
        this.model.set(this.oldSettings);
        this.editarRegistro();
    },
    salvarRegistro: function () {


        this.model.set("st_tipoatividadecomplementar", this.ui.fielTitulo.val());
        this.model.save(null, {
            beforeSend: loading,
            complete: loaded,
            error: function (model, response) {
                $.pnotify({
                    title: "Erro!",
                    type: "error",
                    text: "Erro ao tentar salvar dados. " + response.responseText
                });
            },
            success: _.bind(function () {
                $.pnotify({
                    title: "Sucesso!",
                    type: "success",
                    text: "Dados salvos com sucesso!"
                });
                this.model.id = this.model.get("id_tipoatividadecomplementar");
                this.editarRegistro();
            }, this)
        });

    }
});

var TipoAtividadeView = Marionette.CompositeView.extend({
    template: "#template-tipo-atividade",
    emptyView: EmptyView,
    childView: TipoAtividadeItemView,
    childViewContainer: "tbody",
    ui: {
        "btnAdd": "#btn-add"
    },
    events: {
        "click @ui.btnAdd": "addTipoAtividade"
    },
    addTipoAtividade: function () {
        var model = new TipoAtividadeModel();
        model.toggleEdit();
        this.collection.add(model);
    }
});

var collection = new TipoAtividadeCollection();
collection.fetch({
    beforeSend: loading,
    complete: loaded,
    error: function (collection, response) {
        $.pnotify({
            "type": "error",
            "title": "Erro!",
            "text": response.responseText
        });
    },
    success: function (collection) {
        G2S.show(new TipoAtividadeView({
            collection: collection
        }));
    }
});

/**
 * Created by helder.silva on 02/10/2014.
 */
/**
 * ******************************************************
 * ******                  VARIAVEIS                 *******
 * ******************************************************
 */

var modelo, thatConcurso;

/**
 * ******************************************************
 * ******                  LAYOUT                 *******
 * ******************************************************
 */

var ConcursoLayout = Marionette.LayoutView.extend({
    template: '#concurso_layout',
    tagName: 'div',
    className: 'containder-fluid',
    regions: {
        concurso: '#dados_concurso',
        situacao: '#combo_situacao',
        datas: '#datas_concurso',
        relaciona_produtos: '#relaciona_produtos',
        item_produtos: '#tabelaProdutos',
        item_selecionados: '#tabelaProdutosSelecionados'
    },
    events: {
        "click #btn_salvar": 'salvaTransaction',
        'keyup .input-search': 'pesquisaProdutos',
    },
    initialize: function () {
        thisLayout = this;

    },
    salvaTransaction: function () {

        if (thatConcurso.ui.st_concurso.val() == "") {
            $.pnotify({title: 'Erro', class: 'warning', text: 'Campo Nome Vazio!', type: 'danger'});
            return false;
        }

        modelo = new Concurso();
        if (G2S.editModel) {
            modelo.set('id_concurso', G2S.editModel.id_concurso);
            modelo.set('st_concurso', thatConcurso.ui.st_concurso.val());
            modelo.set('st_orgao', thatConcurso.ui.st_orgao.val());
            modelo.set('nu_vagas', thatConcurso.ui.nu_vagas.val());
            modelo.set('st_banca', thatConcurso.ui.st_banca.val());
            modelo.set('id_situacao', $('#combo_situacao option:selected').val());
            modelo.set('dt_inicioinscricao', thatConcurso.ui.dt_inicioinscricao.val());
            modelo.set('dt_fiminscricao', thatConcurso.ui.dt_fiminscricao.val());
            modelo.set('dt_prova', thatConcurso.ui.dt_prova.val());
        } else {
            modelo.set('st_concurso', thatConcurso.ui.st_concurso.val());
            modelo.set('st_orgao', thatConcurso.ui.st_orgao.val());
            modelo.set('nu_vagas', thatConcurso.ui.nu_vagas.val());
            modelo.set('st_banca', thatConcurso.ui.st_banca.val());
            modelo.set('id_situacao', $('#combo_situacao option:selected').val());
            modelo.set('dt_inicioinscricao', thatConcurso.ui.dt_inicioinscricao.val());
            modelo.set('dt_fiminscricao', thatConcurso.ui.dt_fiminscricao.val());
            modelo.set('dt_prova', thatConcurso.ui.dt_prova.val());
        }

        var arraySelecionados = [];

        _.each(collectionSelecionados.models, function (model) {
            arraySelecionados.push(model.toJSON());
        });


        $("#form-concurso").ajaxForm({
            url: 'concurso/salvar-dados-concurso',
            type: 'POST',
            dataType: 'json',
            data: {
                dados_concurso: modelo.toJSON(),
                produtos: arraySelecionados
            },
            beforeSend: function () {
                loading();
            },
            success: function (data) {
                modelo.set(data);
                window.location.href = "/#CadastrarConcurso/editar/" + modelo.get('id_concurso');
                $.pnotify({title: 'Registro de Concurso', text: 'Cadastro realizado com sucesso', type: 'success'});
                location.reload();
                loading();
            }, 
            complete: function () {
                loaded();
            },
            error: function (data) {
                $.pnotify({title: 'Erro', text: 'Operação não realizada!', type: 'danger'});
            }
        }).submit();
    },
    pesquisaProdutos: function (e) {
        var valorDigitado = e.currentTarget.value;
        var uper = valorDigitado.toUpperCase();
        var tableRows = $(".pesquisa-produtos tbody>tr");

        if (valorDigitado.length > 0) {
            tableRows.hide().find("td:contains('" + uper + "')").parent("tr").show();
        } else {
            tableRows.show();
        }
    }



});

/**
 * ******************************************************
 * ******              COLLECTION                 *******
 * ******************************************************
 */

var SituacaoCollection = Backbone.Collection.extend({
    url: '/api/situacao/?st_tabela=tb_concurso',
    model: Situacao,
    initialize: function () {
        this.fetch()
    }
});

var ProdutoCollection = Backbone.Collection.extend({
    model: Produto,
    async: false,
    url: '/produto/find-produtos-entity'
});

var ProdutoSelecionadoCollection = Backbone.Collection.extend({
    model: Produto,
    async: false,
    url: function () {
        return G2S.editModel ? '/concurso-produto/find-concurso-produto/id/' + G2S.editModel.id_concurso : '/concurso-produto/find-concurso-produto'
    },
    initialize: function () {
        this.fetch()
    }
});

var collection;
var collectionSelecionados = new ProdutoSelecionadoCollection();
var collectionProduto = new ProdutoCollection();

/**
 * ******************************************************
 * ******               ITEMVIEW                  *******
 * ******************************************************
 */

var ProdutosItemView = Marionette.ItemView.extend({
    template: '#item-produtos',
    tagName: 'tr',
    onRender: function () {
        return this;
    },
    addModel: function () {
        collectionSelecionados.add(this.model.clone());
        this.remove();
        this.model.destroy({})
    },
    events: {
        'click #btn-add': 'addModel'
    }
});
var ProdutosSelecionadosItemView = Marionette.ItemView.extend({
    template: '#item-selecionados',
    tagName: 'tr',
    collection: collectionSelecionados,
    removeModel: function () {
        that = this;
        if (that.model.get('id_concurso')) {
            bootbox.confirm("Este projeto será removido do registro! Deseja continuar?", function (result) {
                if (result) {
                    collectionProduto.add(that.model.clone());
                    collectionSelecionados.remove(that.model);
                    that.model.id = that.model.get('id_produto');
                    that.model.url = 'concurso-produto/delete-concurso-produto/prod/' + that.model.id + '/conc/' + G2S.editModel.id_concurso;
                    that.remove();
                    that.model.destroy({});
                    $.pnotify({title: 'Projeto removido', type: 'success'});
                }
            });
        } else {
            collectionProduto.add(that.model.clone());
            that.remove();
            that.model.destroy({});
            collectionProduto.comparator = 'id';
            collectionProduto.sort();
        }
    },
    onRender: function () {
        var q = collectionProduto.findWhere({id_produto: this.model.get('id_produto')});
        collectionProduto.remove(q);
        return this;
    },
    events: {
        'click #btn-remove': 'removeModel'
    }
});
var SituacaoOptionView = Marionette.ItemView.extend({
    tagName: 'option',
    template: _.template('<%=st_situacao%>'),
    onRender: function () {
        this.$el.attr('value', this.model.get('id_situacao'));
        if (G2S.editModel) {
            if (this.model.get('id_situacao') == G2S.editModel.id_situacao) {
                this.$el.attr('selected', 'selected');
            }
        }
    }
});
var modeloDados = new Concurso();
var DadosConcursoView = Marionette.ItemView.extend({
    template: '#item-concurso',
    model: modeloDados,
    onRender: function () {

    },
    initialize: function () {
        thatConcurso = this;
        if (G2S.editModel) {
            loading();
            this.model.set(G2S.editModel);
            debug(G2S.editModel);
            loaded();
        }
    },
    ui: {
        st_concurso: '#st_concurso',
        st_orgao: '#st_orgao',
        nu_vagas: '#nu_vagas',
        st_banca: '#st_banca',
        id_situacao: '#combo_situacao',
        dt_inicioinscricao: '#dataInicioInscricao',
        dt_fiminscricao: '#dataFimInscricao',
        dt_prova: '#dataProva'
    }
});
var RelacionarProdutosView = Marionette.ItemView.extend({
    template: '#item-relaciona-produtos',
    ui: {},
    onRender: function () {
    }
});


/**
 * ******************************************************
 * ******               COLLECTIONVIEW            *******
 * ******************************************************
 */

var ProdutosSelecionadosCollectionView = Marionette.CollectionView.extend({
    template: '#relaciona_produtos',
    childView: ProdutosSelecionadosItemView,
    onRender: function () {
    }
});

var ProdutosCollectionView = Marionette.CollectionView.extend({
    template: '#relaciona_produtos',
    el: '#tabelaProdutos',
    childView: ProdutosItemView,
    collection: collectionProduto,
    onRender: function () {
        debug('aqui');
        this.collection.fetch();
        new ProdutosSelecionadosCollectionView({
            collection: collectionSelecionados,
            el: "#tabelaProdutosSelecionados"
        }).render();
    }
});

var SituacaoSelectView = Marionette.CollectionView.extend({
    childView: SituacaoOptionView,
    collection: new SituacaoCollection(),
    tagName: 'select',
    className: 'span12',
    initialize: function () {
        this.render();
    }
});

var layoutConcurso = new ConcursoLayout();
G2S.show(layoutConcurso);
layoutConcurso.concurso.show(new DadosConcursoView);
layoutConcurso.situacao.show(new SituacaoSelectView);
layoutConcurso.relaciona_produtos.show(new RelacionarProdutosView);
layoutConcurso.item_produtos.show(new ProdutosCollectionView);
layoutConcurso.item_selecionados.show(new ProdutosSelecionadosCollectionView);
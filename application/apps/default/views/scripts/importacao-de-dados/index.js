/*
 *
 *
 * */
var thatImportacao,
    dadosimportados,
    importacaoLayout;

var DadosImportados = Backbone.Model.extend({
    defaults: {}
});

var ProjetosComboModel = Backbone.Model.extend({
    url: function () {
        return '/api/matricula-institucional/'
    },
    defaults: {
        'id_projeto': null,
        'id_usuario': null,
        'id_turma': null
    }
});

var TurmaModel = Backbone.Model.extend({});

var TurmaCollection = Backbone.Collection.extend({
    model: TurmaModel,
    url: '/transfere-projeto/get-vw-turmas-disponives/',
    initialize: function () {
        //this.fetch({async: false});
    }
});

var ProjetosModel = Backbone.Model.extend({});

var ProjetosCollection = Backbone.Collection.extend({
    model: ProjetosModel,
    url: '/matricula-institucional/projetos-pedagogicos/',
    initialize: function () {
        this.fetch({async: false});
    }
});

/*
 * Layout da funcionalidade
 * Descrição das regiões e campos que podem ser utilizados.
 * */
var ImportacaoLayout = Marionette.LayoutView.extend({
    template: '#template-layout-importacao',
    tagName: 'div',
    className: 'container-fluid',
    regions: {
        carregar_arquivo: '#wrapper-carregar-arquivo',
        dados_carregados: '#wrapper-dados-arquivo',
        matricula_institucional: '#wrapper-matricula-institucional',
        combo_projeto: '#wrapper-projeto',
        combo_turma: '#wrapper-turma',
        dados_relatorio: '#wrapper-dados-relatorio'
    },
    ui: {
        matricula_institucional: '#wrapper-matricula-institucional',
        combo_turma: '#wrapper-turma',
        combo_projeto: '#wrapper-projeto',
        actions: '#wrapper_actions'
    },
    events: {
        'change #wrapper-projeto select': 'changeProjeto',
        'change #wrapper-turma select': 'changeTurma',
        'click #btn-salvar-dados': 'salvarDados'
    },
    salvarDados: function () {
        $.ajax({
            url: '/pessoa/processa-dados-importados',
            type: 'post',
            dataType: 'JSON',
            data: {
                dadosImportacao: dadosimportados.collection.toJSON(),
                dadosTurma: importacaoLayout.ui.combo_turma.find('select').val(),
                dadosProjeto: importacaoLayout.ui.combo_projeto.find('select').val()
            },
            beforeSend: function () {
                importacaoLayout.dados_relatorio.reset();
                loading();
            },
            complete: function () {
                loaded();
                importacaoLayout.ui.actions.find('button').attr('disabled', 'disabled');
            },
            success: function (response) {
                //console.log(response);
                importacaoLayout.dados_relatorio.show(new RelatorioView({
                    collection: new Backbone.Collection(response)
                }));
                $.pnotify({
                    title: 'Atenção!',
                    type: 'success',
                    text: 'Dados processados com sucesso, por favor verifique o relatório gerado.'
                });
            },
            error: function () {
                $.pnotify({
                    title: 'Erro no sistema',
                    type: 'error',
                    text: 'Por favor, entre em contato com o administrador.'
                });
            }
        });

        return false;
    },
    changeProjeto: function (e) {
        var turmaCollection = new TurmaCollection();
        turmaCollection.url = '/transfere-projeto/get-vw-turmas-disponives/id_projetopedagogico/' + $(e.currentTarget).val();
        turmaCollection.fetch({async: false});

        importacaoLayout.combo_turma.show(new TurmaSelectView({
            collection: turmaCollection
        }));
    },
    changeTurma: function (e) {
        if ($(e.currentTarget).val() != '') {
            this.ui.actions.find('button').removeAttr('disabled');
        } else {
            this.ui.actions.find('button').attr('disabled', 'disabled');
        }
    }
});

/*
 * Item view para o render da primeira parte da aplicação
 * */
var ImportacaoArquivoView = Marionette.ItemView.extend({
    template: '#template-carregar-arquivo',
    tagName: 'div',
    className: 'well',
    ui: {
        carregar_arquivo: '#st_arquivoimportacao',
        btn_enviar: '#btn_enviararquivo',
        bar: '.bar',
        percent: '.percent',
        status: '#status',
        form: '#form-carregar-arquivo',
        progress: '#progress',
        error_alert: '#error-alert'
    },
    events: {
        'submit #form-carregar-arquivo': 'submitArquivo'
    },
    submitArquivo: function (e) {
        if (thatImportacao.ui.carregar_arquivo.val() == '') {
            thatImportacao.ui.error_alert.html('<strong>OPS!</strong> Por favor, adicione um arquivo para importação.').show();
            return false;
        }

        $(e.currentTarget).ajaxSubmit({
            url: '/importacao-de-dados/processar-arquivo',
            type: 'post',
            dataType: 'json',
            beforeSend: function () {
                var percentVal = '0%';
                thatImportacao.ui.status.empty();
                thatImportacao.ui.bar.width(percentVal);
                thatImportacao.ui.percent.html(percentVal);

                /*
                 * Resetando todos os layouts do arquivo para baixo.
                 * */
                importacaoLayout.dados_relatorio.reset();
                importacaoLayout.dados_carregados.reset();
                importacaoLayout.ui.matricula_institucional.addClass('hide');
                importacaoLayout.combo_turma.reset();

                // :(
                importacaoLayout.ui.combo_turma.html("<select class='select-item-turma'><option value='#'>Selecione uma turma</option></select>");

                importacaoLayout.ui.actions.find('button').attr('disabled', 'disabled');
            },
            uploadProgress: function (event, position, total, percentComplete) {
                var percentVal = percentComplete + '%';
                thatImportacao.ui.bar.width(percentVal);
                thatImportacao.ui.percent.html(percentVal);
            },
            success: function (xhr) {
                dadosimportados = new ShowImportacaoView({
                    collection: new Backbone.Collection(xhr)
                });
                importacaoLayout.dados_carregados.show(dadosimportados);
                importacaoLayout.combo_projeto.show(new ProjetoSelectView());

                importacaoLayout.ui.matricula_institucional.removeClass('hide');
            },
            complete: function () {
                thatImportacao.ui.error_alert.hide();
            },
            error: function (xhr) {
                importacaoLayout.dados_carregados.show(
                    new ShowImportacaoErrorView({
                        collection: new Backbone.Collection($.parseJSON(xhr.responseText))
                    })
                );
            }
        });

        return false;
    },
    initialize: function () {
        thatImportacao = this;
    }
});

var PlanilhaView = Marionette.ItemView.extend({
    template: '#template-dados-unicos-importados',
    tagName: 'tr',
    model: new DadosImportados(),
    onRender: function () {
        if (this.model.get('bl_existe') == 1) {
            $(this.$el).addClass('error')
        }
    }
});

var ShowImportacaoView = Marionette.CompositeView.extend({
    template: '#template-dados-importados',
    tagName: 'div',
    className: 'well',
    childView: PlanilhaView,
    childViewContainer: "#lista-dados",
    ui: {
        feedback: '#feedback'
    },
    initialize: function () {
        console.log('iniciado show importacao');
    },
    onShow: function () {
        this.ui.feedback.append('<div class="alert alert-success"><strong>Parabéns!</strong> Todos os dados obrigatórios estão preenchidos.</div>');

        this.ui.feedback.append('<div class="alert alert-error no"><strong>Atenção!</strong> Todos os usuários sinalizados com a cor <strong>VERMELHO</strong> não serão importados pois já existem no ' +
            'sistema ou estão com dados incorretos .</div>');
        var that = this;

        this.collection.find(function (model) {
            console.log(model);
            if (model.get('bl_cpf')) {
                var st_nomecompleto = model.get('st_nomecompleto');
                that.ui.feedback.append('<div class="alert alert-error"><strong>Atenção!</strong> O CPF do usuário(a) <strong>' + st_nomecompleto + '</strong> não é valido.</div>');
            }
            if (model.get('bl_sexo')) {
                var st_nomecompleto = model.get('st_nomecompleto');
                that.ui.feedback.append('<div class="alert alert-error"><strong>Atenção!</strong> O campo SEXO do usuário(a) <strong>' + st_nomecompleto + '</strong> não foi preenchido.</div>');
            }
            if (model.get('bl_email')) {
                var st_nomecompleto = model.get('st_nomecompleto');
                that.ui.feedback.append('<div class="alert alert-error"><strong>Atenção!</strong> O campo EMAIL do usuário(a) <strong>' + st_nomecompleto + '</strong> não é valido.</div>');
            }
        });
    }
});

var PlanilhaErrorView = Marionette.ItemView.extend({
    template: '#template-dados-unicos-importados-error',
    tagName: 'tr',
    model: new DadosImportados()
});

var ShowImportacaoErrorView = Marionette.CompositeView.extend({
    template: '#template-dados-importados-error',
    tagName: 'div',
    className: 'well',
    childView: PlanilhaErrorView,
    childViewContainer: "#lista-dados-error",
    ui: {},
    events: {},
    initialize: function () {
        console.log('iniciado show importacao');
    }
});

var ProjetoOptionView = Marionette.ItemView.extend({
    tagName: 'option',
    className: 'item-projeto',
    template: _.template('<%=st_projetopedagogico%>'),
    onRender: function () {
        this.$el.attr('value', this.model.get('id_projetopedagogico'));
    }
});

var ProjetoSelectView = Marionette.CollectionView.extend({
    childView: ProjetoOptionView,
    collection: new ProjetosCollection(),
    tagName: 'select',
    className: 'select-item-projeto',
    onRender: function () {
        this.$el.prepend('<option value="" selected="selected">Escolha um projeto</option>');
    }
});

var TurmaOptionView = Marionette.ItemView.extend({
    tagName: 'option',
    template: _.template('<%=st_turma%>'),
    onRender: function () {
        this.$el.attr('value', this.model.get('id_turma'));
    }
});

var TurmaSelectView = Marionette.CollectionView.extend({
    childView: TurmaOptionView,
    tagName: 'select',
    className: 'select-item-turma',
    onRender: function () {
        this.$el.prepend('<option value="" selected="selected">Selecione uma turma</option>');
    }
});

var RelatorioDadosView = Marionette.ItemView.extend({
    template: '#template-dados-unicos-relatorio',
    tagName: 'tr',
    model: new DadosImportados()
});

var RelatorioView = Marionette.CompositeView.extend({
    template: '#template-dados-relatorio',
    tagName: 'div',
    className: 'well',
    childView: RelatorioDadosView,
    childViewContainer: "#lista-dados-relatorio"
});

importacaoLayout = new ImportacaoLayout();
G2S.show(importacaoLayout);

importacaoLayout.carregar_arquivo.show(new ImportacaoArquivoView());


// View do Select PROJETOS
var ProjetosComboModel = Backbone.Model.extend({
    url: function() {
        return '/api/matricula-institucional/'
    },
    defaults: {
        'id_projeto': null,
        'id_usuario': null,
        'id_turma': null
    }
});
var ProjetosComboView = Marionette.ItemView.extend({
    model: new ProjetosComboModel(),
    el: $('#projetos-combo'),
    events: {
        'click .save': 'addMatricula',
        'change #id_projeto' : 'carregaTurma'
    },
    carregaTurma: function(e){
        ComboboxView({
            el: '#id_turma',
            url: '/transfere-projeto/get-vw-turmas-disponives/id_projetopedagogico/' + $(e.target).val(),
            optionLabel: 'st_turma',
            emptyOption: '[Selecione]',
            model: Backbone.Model.extend({
                idAttribute: 'id_turma',
                defaults: {
                    id_turma: null,
                    st_turma: ''
                }
            })
        });
    },
    pessoa: componenteAutoComplete.getFixModel(),
    addMatricula: function() {
        var pessoa = componenteAutoComplete.getFixModel();
        var values = {
            id_projeto: parseInt( $('#id_projeto').val() ),
            id_usuario: parseInt( pessoa.id_usuario ),
            id_turma: parseInt($('#id_turma').val())
        };

        /**
         * Validação
         */
        var msgValidacao = false;
        if (!values.id_projeto || values.id_projeto == '') {
            // Verificar se selecionou um projeto
            msgValidacao = 'É necessário selecionar um projeto pedagógico.';
        } else {
            // Verificar se já nao esta matriculado
            var projetos = $('.id_projetopedagogico');
            projetos.each(function() {
                if ($(this).text() == values.id_projeto) {
                    msgValidacao = 'Esta pessoa já está matriculada neste projeto pedagógico.';
                    return false;
                }
            });
        }

        if (msgValidacao) {
            $.pnotify({
                title: 'Erro',
                text: msgValidacao,
                type: 'error'
            });
            return false;
        }


        loading();
        this.model.set(values);
        this.model.save(null, {
            success: function (model, response) {
                $('.list').trigger('click');
                $('#id_projeto option:selected').prop('selected', false).trigger('change');

                $.pnotify({
                    title: 'Concluído',
                    text: 'A matrícula foi efetuada com sucesso!',
                    type: 'success'
                });
            },
            error: function (model, response) {
                var response = jQuery.parseJSON(response.responseText);

                var message = 'Houve um erro ao tentar efetuar matrícula, tente novamente.';
                if(response.erro)
                    message = response.erro.join('<br/>');

                $.pnotify({
                    title: 'Erro',
                    text: message,
                    type: 'error'
                });
            },
            complete: function () {
                loaded();
            }
        });
    }
});


// INICIALIZA A TELA
new ProjetosComboView();


/**
 * Carregar combobox de "Projeto Pedagogico"
 */
ComboboxView({
    el: '#id_projeto',
    url: '/matricula-institucional/projetos-pedagogicos/',
    optionLabel: 'st_projetopedagogico',
    emptyOption: '[Selecione]',
    model: Backbone.Model.extend({
        idAttribute: 'id_projetopedagogico',
        defaults: {
            id_projetopedagogico: null,
            st_projetopedagogico: ''
        }
    })
});

/**
 * Funcoes ao selecionar uma pessoa
 */

// Listas matriculas feitas para a pessoa selecionada
var MatriculasModel = Backbone.Model.extend({});
var MatriculasCollection = Backbone.Collection.extend({
    model: MatriculasModel,
    url: function () {
        var pessoa = componenteAutoComplete.getFixPessoa();
        return '/api/vw-matricula/id_usuario/' + pessoa.id_usuario;
    }
});

/***********************
 *   View para cada Row
 **********************/
var MatriculasView = Marionette.ItemView.extend({
    template: '#matricula-item',
    tagName: 'tr'
});

/***********************
 *   CompositeView
 **********************/
var MatriculasCompositeView = Marionette.CompositeView.extend({
    template: '#matricula-list',
    collection: new MatriculasCollection(),
    childView: MatriculasView,
    childViewContainer: 'tbody',
    initialize: function () {
        loading();
        this.collection.fetch({
            success: function (model, response) {
                loaded();
            }
        });
    },
    events: {
        'click .list': 'initialize'
    }
});

G2S.layout.addRegions({
    matriculas: '#matriculas'
});



/**
 * Carregando o AutoComplete de Pessoa
 */
$.getScript('/modelo-backbone/VwPesquisarPessoa', function() {
    componenteAutoComplete.main({
            model: VwPesquisarPessoa,
            element: $("#autoComplete")
        },
        function() {
            $('#projetos-combo').show();

            // Mostrar matriculas
            var MatriculasIni = new MatriculasCompositeView();
            G2S.layout.matriculas.show(MatriculasIni);
        },
        function() {
            $('#matriculas').html('');
            $('#projetos-combo').hide();
        });
});



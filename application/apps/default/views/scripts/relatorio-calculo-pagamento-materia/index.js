/******************************************************************************
 *              Relatório Relatorio Calculo Pagamento Materia
 *              Neemias Santos  <neemias.santos@unyleya.com.br>
 *              2011-01-26
 * ****************************************************************************
 */
var ModelTotalizador = Backbone.Model.extend();
/**
 ******************************************************************************************
 ******************************************************************************************
 *************************** collection  ***************************************************
 ******************************************************************************************
 ******************************************************************************************
 */

/**
 * @type Collection para Entidade @exp;Backbone@pro;Collection@call;extend
 */
var EntidadeCollection = Backbone.Collection.extend({
    url: '/entidade/retornar-entidade-recursiva-id',
    model: VwEntidadeRecursivaId
});


/**
 * @type Collection para VwEvolucao @exp;Backbone@pro;Collection@call;extend
 */
var VwTurmaDisciplinaCollection = Backbone.Collection.extend({
    model: VwTurmaDisciplina,
    url: '/turma/retornar-vw-turma'
});

var VwComercialGeralCollection = Backbone.Collection.extend({
    model: VwComercialGeral
});

/**
 ******************************************************************************************
 ******************************************************************************************
 *************************** LAYOUT FUNCIONALIDADE ****************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var TelaGradeLayout = Marionette.LayoutView.extend({
    template: '#template-calculo-pagamento-materia',
    tagName: 'div',
    regions: {
        tabelaGrade: "#regiao-grade-data",
        formgrade: '#regiao-grade'
    },
    onShow: function () {
        loading();
        this.renderizaFormGrade();
        loaded();
        return this;
    },
    renderizaFormGrade: function () {
        var gradeView = new FormGradeCompositeView({});
        this.formgrade.show(gradeView);
    }
});


/**
 ******************************************************************************************
 ******************************************************************************************
 *************************** FORM DA GRADE ****************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var FormGradeCompositeView = Marionette.ItemView.extend({
    initialize: function () {

    },
    template: '#template-form-calculo-pagamento-materia',
    tagName: 'fieldset',
    ui: {
        'id_entidade': 'select[name="id_entidade"]',
        'id_turma': 'select[name="id_turma"]',
        'btnSalvar': '#btnPesquisar',
        'btnGeralExcel': '#btn_gerar_relatorio'
    },
    events: {
        'change #id_entidade': 'populaTurmaEntidade',
        'click #btnPesquisar': 'pesquisarDados',
        'click #btn_gerar_relatorio': 'btnGeralExcel'
    },
    onShow: function () {
        $('#container-DataGrid').hide();
        this.populaSelectEntidade();
    },
    populaSelectEntidade: function () {
        var that = this;
        var collectionEntidade = new EntidadeCollection();
        collectionEntidade.fetch({
            success: function () {
                var view = new SelectView({
                    el: that.$el.find("select[name='id_entidade']"),        // Elemento da DOM
                    collection: collectionEntidade,      // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_nomeentidade', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_entidade', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: null       // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();
            }
        });
    },
    populaTurmaEntidade: function () {
        var that = this;
        if (!that.ui.id_entidade.val()) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Selecione a Entidade para poder buscar as Turmas.'
            });
            return false;
        }
        loading();
        that.ui.id_turma.select2('destroy');
        that.ui.id_turma.empty();
        that.ui.id_turma.prepend('<option value="" selected="selected">Escolha uma turma</option>');
        var collectionTurma = new VwTurmaDisciplinaCollection();
        collectionTurma.url += '/bl_ativo/1/id_entidadecadastro/' + that.ui.id_entidade.val();
        collectionTurma.fetch({
            success: function () {
                var view = new SelectView({
                    el: that.ui.id_turma,        // Elemento da DOM
                    collection: collectionTurma,      // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_turma', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_turma', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: null       // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();
                that.ui.id_turma.select2();
                loaded();
            }
        });
    },
    pesquisarDados: function () {
        var that = this;
        if (!that.ui.id_entidade.val() && !that.ui.id_turma.val()) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Selecione a Entidade.'
            });
            return false;
        }


        var parametros = {
            'id_entidade': that.ui.id_entidade.val(),
            'id_turma': that.ui.id_turma.val()
        };

        $.ajax({
            beforeSend: function () {
                loading();
            },
            type: 'post',
            data: parametros,
            url: '/default/relatorio-calculo-pagamento-materia/pesquisa-dados',
            success: function (response) {
                /*
                 Nesse ponto, verificamos se existe alunos, se houver ele divide os dados e carrega os dados recebidos
                 em uma collection alunos, e na modulo de totalizador, e chama os itens views carregando os dados na tabela
                 */
                if (response.alunos) {
                    var collectionLinhas = new VwComercialGeralCollection(response.alunos);


                    var modelTotalizador = new ModelTotalizador();
                    modelTotalizador.set({
                        'valorTotal': formatValueToShow(response.valorTotal.toFixed(2)),
                        'valorTotalTurma': formatValueToShow(response.valorTotalTurma.toFixed(2)),
                        'valorTotalCancelados': formatValueToShow(response.valorTotalCancelados.toFixed(2)),
                        'valorTotalCursando': formatValueToShow(response.valorTotalCursando.toFixed(2)),
                        'valorTotalCartaDeCredito': formatValueToShow(response.valorTotalCartaDeCredito.toFixed(2))
                    });


                    var view = new TableView({
                        collection: collectionLinhas,
                        model: modelTotalizador
                    });

                    telaGrade.tabelaGrade.show(view);
                }
            },
            complete: function () {
                loaded();
            },
            error: function (error) {

            }
        });

    },
    btnGeralExcel: function (response) {
        var that = this;
        if (!that.ui.id_entidade.val() && !that.ui.id_turma.val()) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Selecione a entidade e a turma'
            });
            return false;
        }
        this.$el.find('form').attr('target', '_blank').attr('action', '/default/relatorio-calculo-pagamento-materia/gerar-xls').submit();
        this.$el.find('form').removeAttr('target');
    }
});

var RowView = Marionette.ItemView.extend({
    tagName: "tr",
    template: "#row-template"
});

var TableView = Marionette.CompositeView.extend({
    childView: RowView,
    childViewContainer: "tbody",
    template: "#table-template"
});

/**
 ******************************************************************************************
 ******************************************************************************************
 ******************************** SHOW LAYOUT *********************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var telaGrade = new TelaGradeLayout();
G2S.show(telaGrade);
/******************************************************************************
 *              Relatorio Controle Turma Grade Horaria
 *              Neemias Santos  <neemias.santos@unyleya.com.br>
 *              2015-12-17
 * ****************************************************************************
 */

/**
 * @type Collection para Entidade @exp;Backbone@pro;Collection@call;extend
 */
var EntidadeCollection = Backbone.Collection.extend({
    url: '/entidade/retornar-entidade-recursiva-id',
    model: VwEntidadeRecursivaId
});

/**
 * @type Collection para Entidade @exp;Backbone@pro;Collection@call;extend
 */
var ProjetoPedagocigoCollection = Backbone.Collection.extend({
    url: '/api/projeto-pedagogico',
    model: VwProjetoPedagogico
});

/**
 * @type Collection para Entidade @exp;Backbone@pro;Collection@call;extend
 */
var TurmaCollection = Backbone.Collection.extend({
    url: '/default/turma/retornar-vw-turma',
    model: VwTurma
});


/**
 ******************************************************************************************
 ******************************************************************************************
 *************************** LAYOUT FUNCIONALIDADE ****************************************
 ******************************************************************************************
 ******************************************************************************************
 */

var TelaGradeLayout = Marionette.LayoutView.extend({
    template: '#template-layout-grade',
    tagName: 'div',
    regions: {
        tabelaGrade: "#regiao-grade-data",
        formgrade: '#regiao-grade'
    },
    onShow: function () {
        this.renderizaFormGrade();
        return this;
    },
    renderizaFormGrade: function () {
        var gradeView = new FormGradeCompositeView({});
        this.formgrade.show(gradeView);
    }
});

/**
 ******************************************************************************************
 ******************************************************************************************
 *********************************** FORM DA GRADE ****************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var FormGradeCompositeView = Marionette.CompositeView.extend({
    template: '#template-form-grade',
    tagName: 'fieldset',
    ui: {
        'id_projetoPedagogico': 'select[name="id_projetoPedagogico"]',
        'id_unidade': 'select[name="id_unidade"]',
        'id_turma': 'select[name="id_turma"]',
        'btnPesquisar': '#btnPesquisar',
    },
    events: {
        'change #id_unidade': 'populaSelectTurma',
        'click #btnPesquisar': 'pesquisarDados'
    },
    onShow: function () {
        this.populaSelectProjetoPedagogico();
        this.populaSelectUnidade();
    },
    populaSelectUnidade: function () {
        var that = this;
        collectionEntidade = new EntidadeCollection();
        collectionEntidade.fetch({
            async: false,
            beforeSend: function () {
                loading();
            },
            success: function () {
                var view = new SelectView({
                    el:  that.ui.id_unidade,        // Elemento da DOM
                    collection: collectionEntidade,      // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_nomeentidade', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_entidade', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: null       // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();
            },
            complete: function () {
            }
        });
    },
    populaSelectProjetoPedagogico: function () {
        var that = this;

        collectionProjetoPedagogico = new ProjetoPedagocigoCollection();
        collectionProjetoPedagogico.fetch({
            async: false,
            beforeSend: function () {
                loading();
            },
            success: function () {
                var view = new SelectView({
                    el:  that.ui.id_projetoPedagogico,        // Elemento da DOM,        // Elemento da DOM
                    collection: collectionProjetoPedagogico,      // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_projetopedagogico', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_projetopedagogico', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: null       // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();
                that.ui.id_projetoPedagogico.select2();
            },
            complete: function () {
                var newOption = $('<option value="0" selected>Todas</option>');
                newOption.insertAfter('#id_projetoPedagogico option[value=""]');
                loaded();
            }
        });
    },
    populaSelectTurma: function () {
        var that = this;
        that.ui.id_turma.select2('destroy');
        that.ui.id_turma.empty();
        that.ui.id_turma.prepend('<option value="0" >Todas</option>');

        collectionTurma = new TurmaCollection();
        collectionTurma.url += (that.ui.id_unidade.val() != 0 ? '/id_entidadecadastro/' + that.ui.id_unidade.val() : '') + '/bl_ativo/1/id_situacao/83';
        collectionTurma.fetch({
            async: false,
            beforeSend: function () {
                loading();
            },
            success: function () {
                var view = new SelectView({
                    el:  that.ui.id_turma,        // Elemento da DOM
                    collection: collectionTurma,      // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_turma', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_turma', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: null       // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();
                that.ui.id_turma.select2();
                loaded()
            }

        });
    },
    pesquisarDados: function () {
        var that = this;
        if (!that.ui.id_unidade.val() && !that.ui.id_unidade.val()) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Selecione a Unidade.'
            });
            return false;
        }

        //monta as variaveis com os valores
        var id_unidade = (that.ui.id_unidade.val() != 0 ) ? '/id_unidade/' + that.ui.id_unidade.val() : '';
        var id_projetopedagogico = (that.ui.id_projetoPedagogico.val() != 0) ? '/id_projetopedagogico/' + that.ui.id_projetoPedagogico.val() : '';
        var id_turma = (that.ui.id_turma.val() != 0 ) ? '/id_turma/' + that.ui.id_turma.val() : '';

        //Concatena as variaveis
        var params = (id_unidade + id_projetopedagogico + id_turma);

        window.open('relatorio-controle-turma-grade-horaria/pesquisa-dados' + params, '_blank');
    }
});


/**
 ******************************************************************************************
 ******************************************************************************************
 ******************************** SHOW LAYOUT *********************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var telaGrade = new TelaGradeLayout();
G2S.show(telaGrade);

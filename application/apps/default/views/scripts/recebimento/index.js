/*******************************************************************************
 * Imports
 ******************************************************************************/
$.ajaxSetup({'async': false});

$.getScript('/js/backbone/views/Recebimento/RecebimentoView.js?_=' + new Date().getTime());

loading();

/*******************************************************************************
 *  Views
 ******************************************************************************/

var tpl = $('#container-recebimento').html();

/***********************
 *   RecebimentoView
 **********************/

var RecebimentoView = Backbone.Marionette.LayoutView.extend({
    template: _.template(tpl),
    vwClienteVenda: null,
    events: {
        'click #link-pesquisa': 'renderResumoView'
    },
    regions: {
        'pesquisaRegion': '#container-pesquisa',
        'tabsRegion': '#container-tabs'
    },
    renderPesquisaView: function () {
        this.pesquisaRegion.show(new PesquisaView());
    },
    onShow: function () {
        this.renderPesquisaView();
        loaded();
    }
});

$(function () {
    var view = new RecebimentoView();
    G2S.layout.content.show(view);

    $("#id_venda").bind("keydown", function (val) {
        //8 = Backspace; 9 = Tab; 37 = ArrowLeft; 39 = ArrowRight; 46 = Delete
        var keyCodesPermitidos  = [8, 9, 37, 39, 46];
        if (keyCodesPermitidos.indexOf(val.keyCode) === -1 && !validaNumero(val.key)) {
            $.pnotify({
                type: "warning",
                title: "Atenção!",
                text: "O Código da Venda deve receber somente números."
            });
            return false;
        }
    }); // DEFINE QUE O CAMPO SÓ RECEBERÁ NÚMEROS
});

$.ajaxSetup({'async': true});
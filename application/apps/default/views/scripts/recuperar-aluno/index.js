var viewLayout,
    viewGrid,
    viewPaginator,
    collectionPesquisa;

var boolCheckAll = false;

/****************************************************
 * Models
 ****************************************************/
var ModelArea = Backbone.Model.extend({
    idAttribute: 'id_areaconhecimento',
    defaults: {
        'id_areaconhecimento': null,
        'st_areaconhecimento': ''
    }
});

var ModelProjeto = Backbone.Model.extend({
    idAttribute: 'id_projetopedagogico',
    defaults: {
        'id_projetopedagogico': null,
        'st_projetopedagogico': ''
    }
});

var ModelDisciplina = Backbone.Model.extend({
    idAttribute: 'id_disicplina',
    defaults: {
        'id_disicplina': null,
        'st_disicplina': ''
    }
});


/**
 * MODEL E COLLECTION DA ATENDENTE
 */
var AtendenteModel = Backbone.Model.extend({
    idAttribute: "id_usuario",
    defaults: {
        "id_usuario": null,
        "st_nomecompleto": ""
    }
});

var AtendenteCollection = Backbone.Collection.extend({
    model: AtendenteModel,
    url: '/default/recuperar-aluno/get-atendente'
});


/****************************************************
 * Collections
 ****************************************************/

var AreaConhecimentoCollection = Backbone.Collection.extend({
    model: ModelArea,
    url: '/default/area-conhecimento/get-areas-com-projeto'
});

var ProjetoPedagogicoCollection = Backbone.Collection.extend({
    model: ModelProjeto
});
var DisciplinaCollection = Backbone.Collection.extend({
    model: ModelDisciplina
});

/****************************************************
 * Layout
 ****************************************************/

var LayoutView = Marionette.LayoutView.extend({
    template: '#layout-template-alunos-recuperar',
    regions: {
        pesquisa: '#pesquisa-container',
        paginacao: '#pesquisa-paginacao'
    },
    ui: {
        form: 'form',
        idAreaConhecimento: '#id_areaconhecimento',
        idProjetoPedagogico: '#id_projetopedagogico',
        idDisciplina: '#id_disciplina',
        dtInicioTurmaMin: '#dt_inicioturma_min',
        dtInicioTurmaMax: '#dt_inicioturma_max',
        idNotaEad: '#id_notaead',
        tipoServico: "#id_tipo_servico",
        tipoServicoOcorrencia: "#tipo_servico_ocorrencia",
        atendenteOcorrencia: "#atendente_ocorrencia",
        // PAGER
        nu_registros: '#nu_registros',
        //BUTTONS
        btnPrint: '.btnPrint',
        btnXls: '.btnXls',
        btnGerarOcorrencia: "#btnGerarOcorrencia",
        btnCriarOcorrenicia: "#btnCriarOcorrencia",
        // OTHER
        totalCount: '.total-box'
    },
    events: {
        'change @ui.idAreaConhecimento': function () {
            this.loadComboProjetoPedagogico($('#id_areaconhecimento').val());
        },
        'change @ui.idProjetoPedagogico': function () {
            this.loadComboDisciplina($('#id_projetopedagogico').val());
        },
        'submit form': 'actionSearch',
        'click @ui.btnPrint': 'actionExport',
        'click @ui.btnXls': 'actionExport',
        "click @ui.btnGerarOcorrencia": "abrirModalOcorrencia",
        "click @ui.btnCriarOcorrenicia": "criarOcorrencia",
        "change @ui.tipoServico": "regrasTipoServico"
    },
    onRender: function () {
        // Carregar combo Area de Conhecimento
        this.loadComboAreaConhecimento();
    },
    regrasTipoServico: function () { //Função que vai setar Nota EAD em 40, caso tipo de serviço for igual a "Recuperação"

        if (this.ui.tipoServico.val() === "recuperação") {
            this.ui.idNotaEad.prop("disabled", true);
            this.ui.idNotaEad.val(40);
        } else {
            this.ui.idNotaEad.prop("disabled", false);
            this.ui.idNotaEad.val(20);
        }

        if (this.ui.tipoServico.val() !== "todos") {

            this.ui.tipoServicoOcorrencia.val(this.ui.tipoServico.val());
            this.ui.tipoServicoOcorrencia.prop('disabled', true);
        } else {

            this.ui.tipoServicoOcorrencia.prop("disabled", false);
            this.ui.tipoServicoOcorrencia.val("");
        }

    },
    loadComboAreaConhecimento: function () {
        var that = this;

        // Carregar combo Area de Conhecimento
        var collectionArea = new AreaConhecimentoCollection();
        collectionArea.fetch();
        this.ui.idAreaConhecimento.html('<option value="">Selecione</option>');

        var AreaSelectView = new SelectView({
            el: that.$el.find('#id_areaconhecimento'),
            collection: collectionArea,
            childViewOptions: {
                optionLabel: 'st_areaconhecimento',
                optionValue: 'id_areaconhecimento'
            }
        });
        AreaSelectView.render();

    },
    loadComboProjetoPedagogico: function (id) {
        var that = this;

        // Carregar combo Projeto Pedagogico
        collectionProjetoPedagogico = new ProjetoPedagogicoCollection();
        collectionProjetoPedagogico.url = '/default/projeto-pedagogico/get-by-area/id_areaconhecimento/' + id;
        collectionProjetoPedagogico.fetch({
            async: false,
            beforeSend: function () {
                loading();
            },
            success: function () {
                var ProjetoSelectView = new SelectView({
                    el: that.ui.idProjetoPedagogico,
                    collection: collectionProjetoPedagogico,
                    childViewOptions: {
                        optionLabel: 'st_projetopedagogico',
                        optionValue: 'id_projetopedagogico',
                        optionSelected: null
                    },
                    clearView: true
                });
                ProjetoSelectView.render();
                that.ui.idProjetoPedagogico.select2();
            },
            complete: function () {
                loaded();
            }
        });
    },
    loadComboDisciplina: function (id) {
        var that = this;

        // Carregar combo Disciplinas
        collectionDisciplina = new DisciplinaCollection();
        collectionDisciplina.url = '/default/disciplina/retorna-disciplina-projeto-pedagogico/id_projetopedagogico/' + id;
        collectionDisciplina.fetch({
            async: false,
            beforeSend: function () {
                loading();
            },
            success: function () {
                var DisciplinaSelectView = new SelectView({
                    el: that.ui.idDisciplina,
                    collection: collectionDisciplina,
                    childViewOptions: {
                        optionLabel: 'st_disciplina',
                        optionValue: 'id_disciplina',
                        optionSelected: null
                    },
                    clearView: true
                });
                DisciplinaSelectView.render();
                that.ui.idDisciplina.select2();
            },
            complete: function () {
                loaded();
            }
        });
    },
    validate: function () {
        var that = this;

        if (!that.ui.idAreaConhecimento.val()) {
            $.pnotify({
                type: 'warning',
                title: 'Campos Obrigatórios',
                text: 'É necessário preencher a Área Conhecimento'
            });
            that.ui.idAreaConhecimento.focus();
            return false;
        } else if (that.ui.dtInicioTurmaMin.val() || that.ui.dtInicioTurmaMax.val()) {
            return that.validateDatas();
        }

        if (that.ui.dtInicioTurmaMin.val() || that.ui.dtInicioTurmaMax.val()) {
            return that.validateDatas();
        }

        if (!that.ui.tipoServico.val()) {
            $.pnotify({
                type: 'warning',
                title: 'Campos Obrigatórios',
                text: 'É necessário selecionar o tipo de serviço!'
            });
            that.ui.tipoServico.focus();
            return false;
        }
        return true;
    },
    validateDatas: function () {
        var that = this;

        var min = this.ui.dtInicioTurmaMin.val();
        var max = this.ui.dtInicioTurmaMax.val();

        if (min && max) {
            var diff = daysDiff(min, max);

            if (diff < 0) {
                $.pnotify({
                    type: 'warning',
                    title: 'Data Incorreta',
                    text: 'A segunda data deve ser maior que a primeira'
                });
                that.ui.dtInicioTurmaMax.val('').focus();
                return false;
            }

            return true;
        } else {
            if (!min) {
                $.pnotify({
                    type: 'warning',
                    title: 'Data Incorreta',
                    text: 'A data inicial está vazia!'
                });
                this.ui.dtInicioTurmaMin.val('').focus();
                return false;

            } else {
                $.pnotify({
                    type: 'warning',
                    title: 'Data Incorreta',
                    text: 'A data final está vazia!'
                });
                this.ui.dtInicioTurmaMax.val('').focus();
                return false;

            }
        }

    },
    actionSearch: function (e) {
        e && e.preventDefault ? e.preventDefault() : '';

        var that = this;

        if (!this.validate()) {
            return false;
        }

        var data = Backbone.Syphon.serialize(this);

        // Backgrid CLASSES
        var PesquisaModel = Backbone.Model.extend({});
        var PesquisaCollection = Backbone.PageableCollection.extend({
            model: PesquisaModel,
            url: '/default/recuperar-aluno/pesquisar/',
            mode: 'client', // Paginacao no lado do cliente (via JS)
            state: {
                pageSize: parseInt(data.nu_registros)
            },
            parse: function (collection) {
                _.each(collection, function (model, index) {
                    model.index = index;
                });
                return collection;
            }
        });

        var CheckedCell = Backgrid.Cell.extend({
            className: "select-row-cell",
            render: function () {
                var checked = this.model.get("attr_check") ? "checked='checked'" : '';
                this.$el.html(_.template('<input type="checkbox" name="model[' + this.model.get("index") + ']" ' + checked + ' tabindex="-1">'));
                return this;
            },
            events: {
                "click input": function (e) {
                    if (this.$el.find("input[name='model[" + this.model.get("index") + "]']").is(":checked")) {
                        $("input[name='model[" + this.model.get("index") + "]']").attr("checked", true);
                        this.model.set("attr_check", true);
                        collectionPesquisa.add(this.model);
                    } else {
                        $("input[name='model[" + this.model.get("index") + "]']").attr("checked", false);
                        this.model.set("attr_check", false);
                        collectionPesquisa.add(this.model);
                    }
                }
            }
        });

        // CONFIG GRID COLUMNS
        var columns = [
            {name: '', label: 'Todos', editable: false, cell: CheckedCell, headerCell: 'select-all'},
            {name: 'st_nomecompleto', label: 'Nome', editable: false, cell: 'string'},
            {name: "st_cpf", label: "CPF", editable: false, cell: "string"},
            {name: 'st_email', label: 'E-mail', editable: false, cell: 'string'},
            {name: 'st_ddd', label: 'DDD Principal', editable: false, cell: 'string'},
            {name: 'st_telefone', label: 'Telefone Principal', editable: false, cell: 'string'},
            {name: 'st_dddalternativo', label: 'DDD Secundário', editable: false, cell: 'string'},
            {name: 'st_telefonealternativo', label: 'Telefone Secundário', editable: false, cell: 'string'},
            {name: 'st_areaconhecimento', label: 'Área', editable: false, cell: 'string'},
            {name: 'st_projetopedagogico', label: 'Projeto Pedagógico', editable: false, cell: 'string'},
            {name: 'st_evolucao', label: 'Evolução', editable: false, cell: 'string'},
            {name: 'dt_inicioturma', label: 'Data de Abertura da Turma', editable: false, cell: 'string'},
            {name: 'dt_aberturasala', label: 'Data de Abertura Sala', editable: false, cell: 'string'},
            {name: 'dt_encerramentosala', label: 'Data Encerramento Sala', editable: false, cell: 'string'},
            {name: 'st_nota', label: 'Nota da Prova', editable: false, cell: 'string'},
            {name: 'st_notaead', label: 'Nota EAD', editable: false, cell: 'string'},
            {name: 'st_notafinal', label: 'Nota Final', editable: false, cell: 'string'},
            {name: 'tipo_servico', label: 'Tipo de Serviço', editable: false, cell: 'string'}
        ];

        collectionPesquisa = new PesquisaCollection();

        viewGrid = new Backgrid.Grid({
            className: "backgrid table table-striped table-bordered table-hover",
            columns: columns,
            collection: collectionPesquisa,
            events: {
                "click .select-all-header-cell input": function () {
                    loading();
                    if (!boolCheckAll) {
                        boolCheckAll = true;

                        this.collection.each(function (model) {
                            if (!model.get("attr_check")) {
                                model.set("attr_check", true);
                                collectionPesquisa.add(model);
                            }
                        });
                    } else {
                        boolCheckAll = false;

                        this.collection.each(function (model) {
                            if (model.get("attr_check")) {
                                model.set("attr_check", false);
                                collectionPesquisa.add(model);
                            }
                        });
                    }
                    viewGrid.render();
                    $(".select-all-header-cell input").attr("checked", boolCheckAll);
                    loaded();
                }
            }
        });

        viewPaginator = new Backgrid.Extension.Paginator({
            collection: collectionPesquisa
        });

        // Region Pesquisa
        this.pesquisa.show(viewGrid);
        this.paginacao.show(viewPaginator);

        this.fetched = collectionPesquisa.fetch({
            data: data,
            reset: true,
            beforeSend: loading,
            complete: function () {
                loaded();
            },
            success: function () {
                that.ui.totalCount.show().find('span').html(collectionPesquisa.fullCollection.length);
                that.ui.btnGerarOcorrencia.prop("disabled", false);
                if (!collectionPesquisa.length) {
                    that.ui.btnGerarOcorrencia.prop("disabled", true);
                    $.pnotify({
                        type: 'warning',
                        title: 'Aviso',
                        text: 'Nenhum registro encontrado.'
                    });
                }
            },
            error: function () {
                $.pnotify({
                    type: 'error',
                    title: 'Aviso',
                    text: 'Algo deu errado, por favor tente novamente.'
                });
            }
        });
    },
    actionExport: function (e) {
        e && e.preventDefault ? e.preventDefault() : '';

        if (!this.fetched) {
            this.actionSearch();
        }

        var data = Backbone.Syphon.serialize(this);

        var url = '/default/recuperar-aluno/pesquisar/?' + $.param(data);
        var win;

        if (e && e.currentTarget && $(e.currentTarget).hasClass('btnPrint')) {
            url += '&type=html';
            win = window.open(url, '_blank');
        } else {
            url += '&type=xls';
            win = window.open(url, '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=780, height=200, top=0, left=0');
        }

        if (!win) {
            $.pnotify({
                type: 'warning',
                title: 'Aviso',
                text: 'Seu navegador bloqueou o popup.'
            });
        }
    },
    abrirModalOcorrencia: function () {
        var that = this;

        var alunos = collectionPesquisa.where({attr_check: true});
        var tipoServico = new Array();

        $.each(alunos, function (key, value) {
            tipoServico.push(value.get("tipo_servico"));
        });

        var maisDeUm = this.verificaTipoServicoIgual(tipoServico);

        if (!maisDeUm) {

            $.pnotify({
                type: "error",
                title: "Erro",
                text: "Existe mais de um tipo de serviço para gerar ocorrência!"
            });

        } else {

            if (tipoServico.length === 1) {
                this.ui.tipoServicoOcorrencia.prop("disabled", false);
                this.ui.tipoServicoOcorrencia.val("");
            } else if (this.ui.tipoServico.val() !== "todos") {
                this.ui.tipoServicoOcorrencia.prop("disabled", true);
                this.ui.tipoServicoOcorrencia.val(this.ui.tipoServico.val());
            }

            var atendenteComboBox = new AtendenteCollection();

            atendenteComboBox.fetch({
                data: {tipoServico: this.ui.tipoServico.val()},
                beforeSend: function () {
                    loading();
                },
                complete: function () {
                    loaded();
                },
                success: function () {

                    if (!atendenteComboBox.length) {
                        $("#btnCriarOcorrencia").attr("disabled", true);
                        $.pnotify({
                            type: "warning",
                            title: "Atenção",
                            text: "Não existe atendentes vinculados a este serviço!"
                        });
                    }

                    var AtendenteSelect = new SelectView({
                        el: that.ui.atendenteOcorrencia,
                        collection: atendenteComboBox,
                        childViewOptions: {
                            optionLabel: "st_nomecompleto",
                            optionValue: "id_usuario",
                            optionSelected: null
                        },
                        clearView: true

                    });

                    /**
                     * Só abre a modal quando carregar o combobox
                     * com os atendentes.
                     */
                    that.$el.find("#modalOcorrencia").modal("show");

                    AtendenteSelect.render();
                },
                error: function () {
                    $.pnotify({
                        type: "error",
                        title: "Erro",
                        text: "Não foi possível recuperar os atendentes, por favor tente novamente!"
                    });
                }
            });
        }
    },
    criarOcorrencia: function () {
        var that = this;
        var alunos = collectionPesquisa.where({attr_check: true});
        var atendenteID = this.ui.atendenteOcorrencia.val();
        var tipoOcorrencia = this.ui.tipoServicoOcorrencia.val();
        var idUsuario = new Array();
        var idSalaDeAula = new Array();

        $.each(alunos, function (key, value) {
            idUsuario.push(value.get("id_usuario"));
            idSalaDeAula.push(value.get("id_saladeaula"));
        });

        var validacao = this.validaCriarOcorrencia(idUsuario, idSalaDeAula, atendenteID);

        if (validacao) {
            $.ajax({
                url: "/default/recuperar-aluno/criar-ocorrencias",
                type: "POST",
                data: {
                    "idUsuario": idUsuario,
                    "idSalaDeAula": idSalaDeAula,
                    "atendenteID": atendenteID,
                    "tipoOcorrencia": tipoOcorrencia
                },
                beforeSend: function () {
                    loading();
                },
                success: function () {
                    that.$el.find("#modalOcorrencia").modal("hide"); //Fecha a modal automaticamente

                    $.pnotify({
                        type: "success",
                        title: "Sucesso",
                        text: "As ocorrências foram geradas com sucesso!"
                    });

                    loaded();

                    //Carrego a pesquisa novamente.
                    viewLayout.actionSearch();
                },
                error: function () {
                    loaded();
                    $.pnotify({
                        type: "error",
                        title: "Erro",
                        text: "Não foi possível gerar as ocorrências, por favor tente novamente!"
                    });
                }

            });
        }
    },
    /**
     *  Essa função é responsável por validar os campos
     *  Antes de enviar para o back-end
     */
    validaCriarOcorrencia: function (idUsuario, idSalaDeAula, atendenteID, tipoOcorrencia) {
        var that = this;
        if (idUsuario.length === 0 || idUsuario === null || idSalaDeAula.length === 0 || idSalaDeAula === null) {
            $.pnotify({
                type: "warning",
                title: "Aviso",
                text: "Por favor, selecione ao menos 1 aluno."
            });

            that.$el.find("#modalOcorrencia").modal("hide"); //Fecha a modal automaticamente
            return false;
        }

        if (atendenteID === "" || tipoOcorrencia === "") {
            $.pnotify({
                type: "warning",
                title: "Aviso",
                text: "Por favor, verifique se o tipo de ocorrência ou o atendente foram selecionados."
            });
            return false;
        }

        return true;
    },
    verificaTipoServicoIgual: function (array) {
        var filtrado = array.filter(function (elem, pos, arr) {
            return arr.indexOf(elem) === pos;
        });

        if (filtrado.length > 1) {
            return false;
        }
        return true;
    }
});


// Render layout template
viewLayout = new LayoutView();
G2S.layout.content.show(viewLayout);
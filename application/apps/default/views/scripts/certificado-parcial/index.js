var CONSTANTES = {
    CARACTERES_MIN: 11,
    ZERO: 0,
    TODOS: 'todos'
};


var CertificadoParcialCollection = Backbone.Collection.extend({
    model: CertificadoParcial,
    url: '/certificado-parcial/retorna-certificado-parcial'
});

var Entidade = Backbone.Collection.extend({
    model: VwEntidadeRelacao,
    url: '/certificado-parcial/retorna-polos'
});

//PESQUISA PARA A CRID HISTORICO
var HistoricoCertificado = Backbone.PageableCollection.extend({
    model: Backbone.Model,
    url: '/certificado-parcial/retorna-historico',
    mode: "client",
    state: {
        pageSize: 6
    }
});

//ROW GRID
var RowGrid = Backgrid.Row.extend({
    initialize: function (options) {
        RowGrid.__super__.initialize.apply(this, arguments);
    }
});

/**
 * Layout principal, carrega os <select> Certificado Parcial e Polo
 */
var LayoutCertificadoParcial = Marionette.LayoutView.extend({
    template: '#template-certificacao-parcial',
    regions: {
        modalRegiao: "#modalHistorico"
    },
    ui: {
        pesquisa: '#pesquisar',
        idCertificadoparcial: '#id_certificadoparcial',
        idEntidade: '#id_entidade',
        stCpf: '#st_cpf',
        stNomecompleto: '#st_nomecompleto',
        nuRegistros: '#nu_registros',
        status: '#status'
    },
    events: {
        'click @ui.pesquisa': 'perquisarGerarCerificado',
    },
    onShow: function () {
        this.carregarCertificadoParcial();
        this.carregarEntidades();
    },

    /**
     * Valida todos os campos caso os campos Nome do Aluno e CPF esteja nulo é obrigatorio
     * escolher um Certificado Parcial
     * @returns {boolean}
     */
    validarCampos: function () {
        if (
            this.ui.stNomecompleto.val().length === CONSTANTES.ZERO
            && this.ui.stCpf.val().length === CONSTANTES.ZERO
            && this.ui.idCertificadoparcial.val() === CONSTANTES.TODOS
        ) {
            return true;
        } else {
            return false;
        }
    },

    /**
     * Valida o campo Nome do se tem 11 ou mais caracteres, caso tenha ignora o <select>
     * de Certificado Parcial, verifica também se o campo de cpf tem valor caso tenha
     * a funcção retorna false para que a poder execultar a validação de CPF
     * @returns {boolean}
     */
    validarNomeAluno: function () {
        if (this.ui.stCpf.val().length > CONSTANTES.ZERO) {
            return false;
        }
        if (
            this.ui.idCertificadoparcial.val() === CONSTANTES.TODOS
            && this.ui.stNomecompleto.val().length < CONSTANTES.CARACTERES_MIN
        ) {
            return true;
        }
        return false;
    },

    /**
     * Valida o campo de CPF caso tenha os 11 caracteres ignora o <select> Certificado Parcial
     * se o CPF estiver sem valor abilita o <select> Certificado Parcial
     * @returns {boolean}
     */
    validaCpf: function () {
        if (this.ui.stCpf.val().length === CONSTANTES.ZERO) {
            return false;
        }
        if (
            this.ui.idCertificadoparcial.val() === CONSTANTES.TODOS
            && this.ui.stCpf.val().length < CONSTANTES.CARACTERES_MIN
        ) {
            return true;
        }
        return false;
    },

    /**
     * Validado todos os campos do formulario
     * @returns {boolean}
     */
    validate: function () {

        var error = null;

        if (this.validarCampos()) {

            error = 'Certificado parcial é obrigatório.';

        } else if (this.validarNomeAluno()) {

            error = 'Certificado parcial é obrigatório.';

        } else if (this.validaCpf()) {

            error = 'Cpf deve conter no minimo 11 caracteres.';
            this.ui.stCpf.val("");
        } else if (this.ui.status.val() === CONSTANTES.TODOS) {
            error = 'Selecione um Status Certificado Parcial.'
        }
        if (error) {
            $.pnotify({
                type: 'warning',
                title: 'Obrigatório',
                text: error
            });
            return false;
        }
        return true;
    },
    /**
     * Pesquisa Certificados parciais apartir dos parametros pessados no fomulario Certificado Parcial
     */
    perquisarGerarCerificado: function () {

        //Validando Formulario
        if (this.validate()) {

            //PESQUISA PARA A CRID CERTIFICADO PARCIAL
            var CertificadoParcialPageable = Backbone.PageableCollection.extend({
                model: VwAlunosaptoscertificadoparcial,
                url: '/certificado-parcial/retorna-alunos-aptos-certificado-parcial',
                mode: "client",
                state: {
                    pageSize: parseInt(this.ui.nuRegistros.val())
                }
            });

            var data = {
                id_entidade: this.ui.idEntidade.val() === CONSTANTES.TODOS
                    ? null :
                    this.ui.idEntidade.val(),
                id_certificadoparcial: this.ui.idCertificadoparcial.val() === CONSTANTES.TODOS
                    ? null
                    : this.ui.idCertificadoparcial.val(),
                st_nomecompleto: this.ui.stNomecompleto.val(),
                st_cpf: this.ui.stCpf.val(),
                status: this.ui.status.val()
            };

            var certificado = new CertificadoParcialPageable();
            certificado.fetch({
                data: data,
                beforeSend: function () {
                    loading();
                }.bind(this),
                complete: loaded,
                success: function (collection) {
                    var pageableGridLog = new Backgrid.Grid({
                        className: 'backgrid table table-bordered',
                        columns: this.getColumns(),
                        collection: collection,
                        emptyText: "Nenhum registro encontrado",
                        row: RowGrid
                    });

                    var paginator = new Backgrid.Extension.Paginator({
                        collection: collection
                    });
                    this.$el.find('#container-DataGrid').show();

                    var $elemento = this.$el.find('#template-data-grade');
                    $elemento.empty();
                    $elemento.append(pageableGridLog.render().$el);

                    $elemento.append(paginator.render().el);
                }.bind(this)
            });
        }

    },
    /**
     * Função para carregar <select> de Polos
     */
    carregarEntidades: function () {
        var entidade = new Entidade();
        entidade.fetch({
            beforeSend: function () {
                loading();
            }.bind(this),
            complete: loaded,
            success: function (collection) {
                var viewSelectEntidades = new SelectView({
                    el: "#id_entidade",
                    collection: collection,
                    childViewOptions: {
                        optionLabel: 'st_nomeentidade',
                        optionValue: 'id_entidade',
                        optionSelected: ''
                    }
                });

                viewSelectEntidades.render();
            }.bind(this)
        });
    },
    /**
     * Função para carregar o <select> de Certificado Parcial
     */
    carregarCertificadoParcial: function () {
        var certificadoParcial = new CertificadoParcialCollection();
        certificadoParcial.fetch({
            beforeSend: loading,
            complete: loaded,
            success: function (collection) {
                var viewSelectCertificadoParcial = new SelectView({
                    el: this.$el.find('#id_certificadoparcial'),
                    collection: collection,
                    childViewOptions: {
                        optionLabel: 'st_certificadoparcial',
                        optionValue: 'id_certificadoparcial',
                        optionSelected: ''
                    }
                });

                viewSelectCertificadoParcial.render();
            }.bind(this)
        });
    },

    /**
     * Retorna o bind de colunas da tabela (grid de Certiticado Parcial)
     */
    getColumns: function () {
        return [
            {
                name: 'st_nomeentidade',
                label: 'Polo',
                editable: false,
                cell: 'string'
            },
            {
                name: 'st_certificadoparcial',
                label: 'Certificado Parcial',
                editable: false,
                cell: 'string'
            },
            {
                name: 'st_nomecompleto',
                label: 'Nome do Aluno',
                editable: false,
                cell: 'string'
            },
            {
                name: 'st_cpf',
                label: 'CPF',
                editable: false,
                cell: 'string'
            },
            {
                name: 'dt_impressao',
                label: 'Data Impressão',
                editable: false,
                cell: 'string'
            },
            {
                name: '',
                label: 'Gerar Certificado Parcial',
                editable: false,
                cell: this.getGerarCertificadoParcial()
            },
            {
                name: '',
                label: 'Histórico',
                editable: false,
                cell: this.getHistorico()
            }
        ];
    },
    /**
     * Cell para criação dos botões e Gerar e Visualizar
     * @returns {*}
     */
    getGerarCertificadoParcial: function () {
        return Backgrid.Cell.extend({
            events: {
                "click .btn-gerar": "gerarCertificado",
                "click .btn-visualizar": "visualizarCertificado"
            },
            render: function () {
                this.$el.html(_.template(
                    "<div class='btn-toolbar'>" +
                    "<div class='btn-group'>" +
                    "<button type='button' class='btn  btn-success btn-gerar' title='Gerar'>" +
                    "<i class='icon-ok icon-white'></i> Gerar" +
                    "</button>" +
                    "</div>" +
                    "<div class='btn-group'>" +
                    "<button type='button' class='btn  btn-warning btn-visualizar' title='Visualizar'>" +
                    "<i class='icon-eye-open icon-white'></i> Visualizar" +
                    "</button>" +
                    "</div>" +
                    "</div>"
                ));
                this.delegateEvents();
                return this;
            },
            gerarCertificado: function () {

                var data = {
                    id_entidade: this.model.get('id_entidade')
                };
                $.ajax({
                    dataType: 'json',
                    type: 'post',
                    url: '/certificado-parcial/verificar-sigla',
                    data: data,
                    beforeSend: function () {
                        loading();
                    }.bind(this),
                    complete: loaded,
                    success: function (collection) {
                        if (collection.type === 'warning') {
                            $.pnotify({
                                title: collection.title,
                                text: collection.text,
                                type: collection.type
                            });
                        } else {
                            window.open("/certificado-parcial/visualizar-certificado?id_certificadoparcial="
                                + this.model.get('id_certificadoparcial')
                                + "&id_matricula=" + this.model.get('id_matricula')
                                + "&id_entidade=" + this.model.get('id_entidade')
                                + "&gerar=" + true);
                        }

                    }.bind(this)
                });
            },
            visualizarCertificado: function () {
                var data = {
                    id_entidade: this.model.get('id_entidade')
                };
                $.ajax({
                    dataType: 'json',
                    type: 'post',
                    url: '/certificado-parcial/verificar-sigla',
                    data: data,
                    beforeSend: function () {
                        loading();
                    }.bind(this),
                    complete: loaded,
                    success: function (collection) {
                        if (collection.type === 'warning') {
                            $.pnotify({
                                title: collection.title,
                                text: collection.text,
                                type: collection.type
                            });
                        } else {
                            window.open("/certificado-parcial/visualizar-certificado?id_certificadoparcial="
                                + this.model.get('id_certificadoparcial')
                                + "&id_matricula=" + this.model.get('id_matricula')
                                + "&id_entidade=" + this.model.get('id_entidade')
                                + "&visualizar=" + true);
                        }

                    }.bind(this),
                    error: function (response) {
                    }
                });

            }
        });
    },
    /**
     * Cell para criar botão para a modal do Historico
     * @returns {*}
     */
    getHistorico: function () {
        return Backgrid.Cell.extend({
            events: {
                "click .historico": "abrirModalHistorico"
            },
            render: function () {
                this.$el.html(_.template(
                    "<div class='btn-toolbar'>" +
                    "<div class='btn-group'>" +
                    "<a class='btn  btn-success historico' title='Histórico' href='javascript:void(0)'>" +
                    "<i class='icon icon-white icon-list-alt'></i>" +
                    "</a>" +
                    "</div>" +
                    "</div>"));
                this.delegateEvents();
                return this;
            },
            abrirModalHistorico: function () {
                var modal = new ModalHistorico({
                    model: this.model
                });

                layoutCertificadoParcial.getRegion("modalRegiao").show(modal);

            }
        });
    }

});

/**
 * Modal historico
 */
var ModalHistorico = Marionette.ItemView.extend({
    template: "#modal-historico",
    tagName: "div",
    className: "modal hide fade modal-dialog  modal-lg",
    initialize: function () {
        this.buscarHistorico();
    },
    onShow: function () {

        this.$el.modal("show");
        return this;
    },
    buscarHistorico: function () {

        var data = {
            id_certificadoparcial: this.model.get('id_certificadoparcial'),
            id_matricula: this.model.get('id_matricula')
        };

        var historicoCertificado = new HistoricoCertificado();
        historicoCertificado.fetch({
            data: data,
            beforeSend: function () {
                loading();
            }.bind(this),
            complete: loaded,
            success: function (collection) {
                var pageableGridLog = new Backgrid.Grid({
                    className: 'backgrid table table-bordered',
                    columns: this.getColumns(),
                    collection: collection,
                    emptyText: "Nenhum registro encontrado",
                    row: RowGrid
                });

                var paginator = new Backgrid.Extension.Paginator({
                    collection: collection
                });
                this.$el.find('#container-DataGridHistorico').show();

                var $elemento = this.$el.find('#template-data-gradeHistorico');
                $elemento.empty();
                $elemento.append(pageableGridLog.render().$el);

                $elemento.append(paginator.render().el);
            }.bind(this)
        });

    },
    getColumns: function () {
        return [
            {
                name: 'st_certificadoparcial',
                label: 'Nome do Certificado Parcial',
                editable: false,
                cell: 'string'
            },
            {
                name: 'dt_cadastro',
                label: 'Data de Impressão',
                editable: false,
                cell: 'string'
            },
            {
                name: 'st_usuariocadastro',
                label: 'Usuário',
                editable: false,
                cell: 'string'
            },
            {
                name: 'st_polo',
                label: 'Polo',
                editable: false,
                cell: 'string'
            }
        ];
    },
});

var layoutCertificadoParcial = new LayoutCertificadoParcial();

G2S.show(layoutCertificadoParcial);
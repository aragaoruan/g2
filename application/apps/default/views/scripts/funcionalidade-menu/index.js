// Esta sera a Collection dos comboboxes
var arrayFuncionalidade = null;

// MODEL
var FuncionalidadeModel = Backbone.Model.extend({
    idAttribute: 'id_funcionalidade',
    defaults: {
        id_funcionalidade: 0,
        st_funcionalidade: '',
        st_ajuda: '',
        id_funcionalidadepai: null,
        st_funcionalidadepai: ''
    },
    url: function() {
        return '/api/funcionalidade-menu/' + (this.id || '');
    },
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    }
});

// ITEM VIEW
var FuncionalidadeView = Marionette.ItemView.extend({
    model: new FuncionalidadeModel(),
    //template: '#funcionalidade-view',
    tagName: 'tr',
    ui: {
        st_funcionalidade: '[name="st_funcionalidade"]',
        st_ajuda: '[name="st_ajuda"]',
        id_funcionalidadepai: '[name="id_funcionalidadepai"]'
    },
    onBeforeRender: function() {
        if (this.model.is_editing)
            this.template = '#funcionalidade-edit';
        else
            this.template = '#funcionalidade-view'
    },
    onRender: function() {
        var that = this;

        if (this.model.is_editing) {
            ComboboxView({
                el: that.ui.id_funcionalidadepai,
                model: FuncionalidadeModel,
                url: '/funcionalidade-menu/get-funcionalidades-breadcrumb?id_situacao=123&id_tipofuncionalidade=2&order_by=st_funcionalidade',
                collection: arrayFuncionalidade,
                optionLabel: 'st_funcionalidade',
                optionSelectedId: that.model.get('id_funcionalidadepai') ? that.model.get('id_funcionalidadepai').id_funcionalidade : null,
                emptyOption: '[Selecione]'
            }, function(el, comp) {
                if (comp.collection && comp.collection.models) {
                    arrayFuncionalidade = comp.collection.models;
                }
            });
        } else if (this.model.get('id_funcionalidadepai')) {
            var value = Number(this.model.get('id_funcionalidadepai').id_funcionalidade);
            for (var i in arrayFuncionalidade) {
                var obj = arrayFuncionalidade[i];
                if (value == Number(obj.id_funcionalidade)) {
                    this.$el.find('.id_funcionalidadepai').html(obj.st_funcionalidade);
                    break;
                }
            }
        }
    },
    events: {
        'click .actionEdit': 'actionEdit',
        'click .actionSave': 'actionSave'
    },
    actionEdit: function() {
        this.model.toggleEdit();
        this.render();
    },
    actionSave: function() {
        var that = this;
        var elem = this.$el;

        // Novos valores
        var values = {
            st_funcionalidade: this.ui.st_funcionalidade.val(),
            st_ajuda: this.ui.st_ajuda.val(),
            id_funcionalidadepai: {
                id_funcionalidade: this.ui.id_funcionalidadepai.val()
            }
        };

        // Validar campo Nome da Funcionalidade
        if (!values.st_funcionalidade.trim()) {
            $.pnotify({
                type: 'warning',
                title: 'Campo Obrigatório',
                text: 'Por favor preencha o campo "Nome".'
            });
            return false;
        }

        // Salvar alteracoes
        loading();
        this.model.save(values, {
            success: function(model, response) {
                that.model.toggleEdit();
                that.model.set(response.mensagem);
                that.render();
            },
            complete: function(response) {
                response = $.parseJSON(response.responseText);
                $.pnotify(response);
                loaded();
            }
        });
    }
});

// COLLECTION
var FuncionalidadeCollection = Backbone.Collection.extend({
    model: FuncionalidadeModel,
    url: '/api/funcionalidade-menu/?id_situacao=123&order_by=st_funcionalidade&id_tipofuncionalidade=3'
});

// COMPOSITE VIEW
var FuncionalidadeComposite = Marionette.CompositeView.extend({
    template: '#funcionalidade-template',
    collection: new FuncionalidadeCollection(),
    childView: FuncionalidadeView,
    childViewContainer: 'tbody',
    initialize: function() {
        this.collection.fetch({ complete: loaded });
    },
    onRender: function() {
        this.verifyEmpty();
    },
    collectionEvents: {
        add: 'verifyEmpty',
        remove: 'verifyEmpty'
    },
    verifyEmpty: function() {
        var elem = this.$el;

        if (this.collection.length) {
            elem.find('.empty-view').remove();
        } else {
            var ViewEmpty = Marionette.ItemView.extend({
                tagName: 'tr',
                className: 'empty-view',
                template: _.template('<td colspan="' + elem.find('thead th').length + '">Nenhum registro encontrado.</td>')
            });
            elem.find('tbody').append((new ViewEmpty()).render().$el);
        }
    }
});

var FuncionalidadeIni = new FuncionalidadeComposite();
G2S.layout.content.show(FuncionalidadeIni);
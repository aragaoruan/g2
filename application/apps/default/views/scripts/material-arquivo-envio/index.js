/**
 * Created by Débora Castro on 04/03/2015.
 * <debora.castro@unyleya.com.br
 */
$.getScript('/js/backbone/models/modalbasica.js');

var modal = false;
var modalArquivo = false;
var PesquisaPacotes = false;

var template = {
    htmlBody: function () {
        var html = '<div class="row-fluid">' +
            '<label>Área:</label>' +
            '<select id="id_areagerapacote" name="id_area" class="span12"></select>' +
            '</div>' +
            '<div class="row-fluid">' +
            '<label>Curso:</label><select id="id_projetopedagogicopacote" class="span12" name="id_proejtopedagogico"></select>' +
            '</div>';

        return html;
    },
    htmlFooter: function () {
        var html1 = '<button id="btnAcaoCancelarGerar" name="btnAcaoCancelarGerar" class="btn btn-danger">Cancelar</button>' +
            '<button id="btnAcaoGerar" name="btnAcaoGerar" class="btn btn-success">Gerar</button>';
        return html1;
    },
    htmlArquivoBody: function () {
        var html2 = '<div class="row-fluid">' +
            '<label>Lote:</label>' +
            '<input  type="text" id="id_lotematerialModal" name="id_lotematerial" class="span3" value="" />' +
            '</div>' +
            '<div class="row-fluid">' +
            '<label>Data de Criação do Pacote:</label>' +
            '<input id="dt_criacaoinicioModal" class="span5 datepicker" value="" name="dt_criacaoinicio" />' +
            '<span>&nbsp;Até&nbsp;</span>' +
            '<input id="dt_criacaofimModal" class="span5 datepicker" value="" name="dt_criacaofim" />' +
            '</div>';

        return html2;
    },
    htmlArquivoFooter: function () {
        var html3 = '<button id="btnAcaoCancelarArquivo" name="btnAcaoCancelarArquivo" class="btn btn-danger">Cancelar</button>' +
            '<button id="btnAcaoGerarXLS" name="btnAcaoGerarXLS" class="btn btn-success">Gerar XLS</button>';
        return html3;
    }
}

/****************************************************
 * Models
 ****************************************************/
var ModelArea = Backbone.Model.extend({
    idAttribute: 'id_areaconhecimento',
    defaults: {
        'id_areaconhecimento': null,
        'st_areaconhecimento': ''
    }
});

var ModelProjeto = Backbone.Model.extend({
    idAttribute: 'id_projetopedagogico',
    defaults: {
        'id_projetopedagogico': null,
        'st_projetopedagogico': ''
    }
});

var ModelPesquisaPacote = Backbone.Model.extend({
    defaults: {
        'st_nomecompleto': '',
        'id_areaconhecimentopesquisa': null,
        'id_projetopedagogicopesquisa': null,
        'dt_envioinicio': '',
        'dt_enviofim': '',
        'dt_postageminicio': '',
        'dt_postagemfim': '',
        'id_situacaopesquisa': ''
    },
    url: '/default/material-arquivo-envio/pesquisar-pacote-material'
});


/****************************************************
 * Layout
 ****************************************************/
var LayoutMaterialEnvio = Marionette.LayoutView.extend({
    template: '#template-material-envio',
    tagName: 'div',
    className: 'container-fluid',
    regions: {
        regiaoBotoesPacote: '#topoBotoes',
        regiaoFormPesquisa: '#formPesquisa'
    }
});

/****************************************************
 * Collections
 ****************************************************/

var VwAreaConhecimentoCollection = Backbone.Collection.extend({
    model: ModelArea,
    url: '/default/area-conhecimento/get-areas-com-projeto'
});

var VwProjetoPedagogicoCollection = Backbone.Collection.extend({
    model: ModelProjeto
});

var PesquisaPacoteCollection = Backbone.Collection.extend({
    model: VwEntregaMaterial,
    url: '',
    urlRoot: '/default/material-arquivo-envio/pesquisar-pacote-material'
});

/****************************************************
 * Views
 ****************************************************/
var ViewDadosPesquisa = Marionette.ItemView.extend({
    template: _.template('<td><%=id_pacote%></td>' +
    '<td><%=id_lotematerial%></td>' +
    '<td><%=st_nomecompleto%></td>' +
    '<td><%=id_matricula%></td>' +
    '<td><%=st_projetopedagogico%></td>' +
    '<td><%=st_itemdematerial%></td>' +
    '<td><%=dt_criacaopacote%></td>' +
    '<td><%=dt_entrega%></td>' +
    '<td><%=st_situacaopacote%></td>'),
    tagName: 'tr'
});

var ViewDadosPesquisaEmpty = Marionette.ItemView.extend({
    tagName: 'tr',
    template: _.template('<td colspan="9"><center>Nenhum Aluno Encontrado!</center></td>')
});

var ViewDadosPesquisaComposite = Marionette.CompositeView.extend({
    template: '#templateGridPesquisaPacote',
    childView: ViewDadosPesquisa,
    childViewContainer: '#tbodyPesquisaPacotes',
    emptyView: ViewDadosPesquisaEmpty
});

var ViewTelaPesquisa = Marionette.ItemView.extend({
    template: '#tempFormPesquisa',
    model: new ModelPesquisaPacote(),
    ui: {
        itemComboAreaPesquisa: '#id_areaconhecimentopesquisa',
        itemComboProjetoPesquisa: '#id_projetopedagogicopesquisa',
        itemComboSituacaoPesquisa: '#id_situacaopesquisa',
        itemNomePesquisa: '#st_nomecompleto',
        itemDataCriacaoPesquisa: '#dt_criacaoinicio',
        itemDataCriacaoFimPesquisa: '#dt_criacaofim',
        itemDataPostagemInicio: '#dt_postageminicio',
        itemDataPostagemFim: '#dt_postagemfim',
        itemBtnPesquisarPacote: '#btnPesquisarPacotes',
        itemFormPesquisaPacote: '#formPesquisaPacote',
        itemDivNomeAluno: '#divNomeAluno',
        itemDivDtCriacao: '#divDataCriacao'
    },
    onRender: function () {
        this.carregaComboAreaPesquisa();

        $('.datepicker').datepicker({
            format:'dd/mm/yyyy',
            autoclose: true,
            forceparse:true});
    },
    events: {
        'change #id_areaconhecimentopesquisa': function () {
            this.carregaComboProjetoPesquisa(this.ui.itemComboAreaPesquisa.val());
        },
        'submit #formPesquisaPacote': 'pesquisaPacote'
    },
    carregaComboAreaPesquisa: function () {
        var collectionArea = new VwAreaConhecimentoCollection();
        collectionArea.fetch();
        this.ui.itemComboAreaPesquisa.html('<option value="">Todas</option>');

        var viewAreaPesquisa = new SelectView({
            el: this.$el.find('#id_areaconhecimentopesquisa'),
            collection: collectionArea,
            childViewOptions: {
                optionLabel: 'st_areaconhecimento',
                optionValue: 'id_areaconhecimento'
            }
        });
        viewAreaPesquisa.render();
    },
    carregaComboProjetoPesquisa: function (id) {

        if (id != '') {
            var collection1 = new VwProjetoPedagogicoCollection();
            collection1.url = '/default/projeto-pedagogico/get-by-area?id_areaconhecimento=' + id;
            collection1.fetch();

            this.ui.itemComboProjetoPesquisa.html('<option value="">Todos</option>');

            var view1 = new SelectView({
                el: this.$el.find('#id_projetopedagogicopesquisa'),
                collection: collection1,
                childViewOptions: {
                    optionLabel: 'st_projetopedagogico',
                    optionValue: 'id_projetopedagogico'
                }
            });
            view1.render();
        } else {
            this.ui.itemComboProjetoPesquisa.html('<option value=""></option>');
        }
    },
    validacoes: function () {
        if ((this.ui.itemNomePesquisa.val() == '') && (this.ui.itemDataCriacaoPesquisa.val() == '' || this.ui.itemDataCriacaoFimPesquisa.val() == '')) {
            $.pnotify({
                title: 'Aviso!',
                text: 'Por Favor informe o nome do Aluno ou um Intervalo para Data de Criação do Pacote.',
                type: 'warning'
            });
            this.ui.itemNomePesquisa.focus();
            this.ui.itemDivNomeAluno.addClass('control-group warning');
            this.ui.itemDivDtCriacao.addClass('control-group warning');
            return false;
        }
        this.ui.itemDivNomeAluno.removeClass('control-group warning');
        this.ui.itemDivDtCriacao.removeClass('control-group warning');

        if (this.ui.itemDataCriacaoPesquisa.val() != '' && this.ui.itemDataCriacaoFimPesquisa.val() != '') {
            comprarDataCriacao1 = compareDate(this.ui.itemDataCriacaoPesquisa.val(), this.ui.itemDataCriacaoFimPesquisa.val());

            if (comprarDataCriacao1 == -1) {
                $.pnotify({
                    title: 'Aviso!',
                    text: 'O período de DATA de Criação do Pacote é inválida: a data inicial tem que ser menor que a final!',
                    type: 'warning'
                });
                this.ui.itemDataCriacaoPesquisa.focus();
                this.ui.itemDivDtCriacao.addClass('control-group warning');
                return false;

            }
        }

        if (this.ui.itemDataPostagemInicio.val() != '' && this.ui.itemDataPostagemFim.val() != '') {
            comprarDataCriacao = compareDate(this.ui.itemDataPostagemInicio.val(), this.ui.itemDataPostagemFim.val());

            if (comprarDataCriacao == -1) {
                $.pnotify({
                    title: 'Aviso!',
                    text: 'O período de DATA de Postagem é inválida: a data inicial tem que ser menor que a final!',
                    type: 'warning'
                });
                this.ui.itemDataPostagemInicio.focus();
                return false;
            }

        }

        return true;
    },
    pesquisaPacote: function (e) {

        //var data = Backbone.Syphon.serialize(this);
        //this.model.set(data);
        //this.model.fetch();

        if (this.validacoes()) {
            this.ui.itemDivNomeAluno.removeClass('control-group warning');
            this.ui.itemDivDtCriacao.removeClass('control-group warning');

            if (PesquisaPacotes) {
                PesquisaPacotes.url = PesquisaPacotes.urlRoot + "?" + $('#formPesquisaPacote').serialize();
            } else {
                PesquisaPacotes = new PesquisaPacoteCollection({});
                PesquisaPacotes.url = PesquisaPacotes.urlRoot + "?" + $('#formPesquisaPacote').serialize();
            }

            loading();
            PesquisaPacotes.fetch({
                complete: function () {
                    loaded();
                }
            });

            compositePesquisa = new ViewDadosPesquisaComposite({
                el: '#fieldRetornoPesquisa',
                collection: PesquisaPacotes
            }).render();
        }

        return false;
    }
});

var ViewTopoBotoes = Marionette.ItemView.extend({
    template: '#tempTopoBotoes',
    events: {
        'click #btnGerarPacote': 'modalGerarPacote',
        'click #btnGerarArquivoEnvio': 'modalArquivoEnvio',
        'click #btnAcaoGerar': 'actionGerarPacote',
        'click #btnAcaoCancelarGerar': 'modalFecharModal',
        'click #btnAcaoCancelarArquivo': 'modalFecharModal',
        'change #id_areagerapacote': function () {
            this.carregaComboProjeto($('#modalGerarPacote #id_areagerapacote option:selected').val());
        },
        'click #btnAcaoGerarXLS': 'actionGerarArquivoEnvio'
    },
    modalGerarPacote: function () {

        dadosModal = new ModalBasicaModel({
            titulo_modal: 'Gerar Pacote',
            conteudo_modal_body: template.htmlBody(),
            conteudo_modal_footer: template.htmlFooter()
        });

        modal = new ModalBasicaView({
            el: '#modalGerarPacote',
            model: dadosModal
        });

        modal.renderModal({});

        $('#modalGerarPacote #modalBasica').modal('show');
        this.carregaComboArea();
    },
    modalArquivoEnvio: function () {
        dadosModalArquivo = new ModalBasicaModel({
            titulo_modal: 'Gerar Arquivo de Envio',
            conteudo_modal_body: template.htmlArquivoBody(),
            conteudo_modal_footer: template.htmlArquivoFooter()
        });

        modalArquivo = new ModalBasicaView({
            el: '#modalGerarArquivoEnvio',
            model: dadosModalArquivo
        });

        modalArquivo.renderModal({});

        $('#modalGerarArquivoEnvio #modalBasica').modal('show');
    },
    modalFecharModal: function () {
        $('#modalGerarPacote #modalBasica').modal('hide');
        $('#modalGerarArquivoEnvio #modalBasica').modal('hide');
    },
    carregaComboArea: function () {
        var collectionArea = new VwAreaConhecimentoCollection();
        collectionArea.fetch();
        $('#modalBasica #id_areagerapacote').html('<option value="">Todos</option>');

        var viewArea = new SelectView({
            el: $('#modalBasica #id_areagerapacote'),
            collection: collectionArea,
            childViewOptions: {
                optionLabel: 'st_areaconhecimento',
                optionValue: 'id_areaconhecimento'
            }
        });
        viewArea.render();
    },
    carregaComboProjeto: function (id) {

        var collection1 = new VwProjetoPedagogicoCollection();
        collection1.url = '/default/projeto-pedagogico/get-by-area?id_areaconhecimento=' + id;
        collection1.fetch();
        $('#modalBasica #id_projetopedagogicopacote').html('<option value="">Todos</option>');

        var view1 = new SelectView({
            el: $('#modalBasica #id_projetopedagogicopacote'),
            collection: collection1,
            childViewOptions: {
                optionLabel: 'st_projetopedagogico',
                optionValue: 'id_projetopedagogico'
            }
        });

        view1.render();
    },
    actionGerarPacote: function () {
        var that = this;
        this.modalFecharModal();

        var id_area = $('#id_areagerapacote option:selected').val();
        var id_projeto = $('#id_projetopedagogicopacote option:selected').val();

        loading();
        $.ajax({
            url: '/default/material-arquivo-envio/material-gerar-pacote',
            type: 'POST',
            data: {'id_areaconhecimento': id_area, 'id_projetopedagogico': id_projeto}
        }).done(function (data, textStatus, jqXHR) {
            loaded();

            $.pnotify({
                type: data.type,
                title: data.title,
                text: data.text
            });

            if (data.tipo == 1) {
                that.actionBoxArquivoEnvio(data.id);
            }
        });

    },
    actionBoxArquivoEnvio: function (id) {
        var metodo = this;
        bootbox.confirm('O lote ' + id + ' foi gerado com sucesso! Deseja gerar arquivo de envio?', function (result) {
            if (result) {
                metodo.modalArquivoEnvio();
                $('#modalGerarArquivoEnvio #modalBasica #id_lotematerialModal').val(id);
            }
        });
    },
    actionGerarArquivoEnvio: function () {
        this.modalFecharModal();

        var id_lotematerial = $('#modalGerarArquivoEnvio #id_lotematerialModal').val();
        var dt_inicio = $('#dt_criacaoinicioModal').val();
        var dt_fim = $('#dt_criacaofimModal').val();

        if (id_lotematerial == '' && (dt_inicio == '' || dt_fim == '')) {
            $.pnotify({
                type: 'warning',
                title: 'Aviso!',
                text: 'Preencha o campo LOTE ou indique um período de Criação de LOTE'
            });
        } else {
            loading();
            var url = '/default/material-arquivo-envio/material-gerar-arquivo-envio?id_lotematerial=' + id_lotematerial + "&dt_inicio=" + dt_inicio+ "&dt_fim=" + dt_fim ;
            window.open(url, '_blank');
            loaded();
        }
    }
});


/*********************************************
 ************** INICIALIZAÇÃO ****************
 *********************************************/

var telaMaterialEnvio = new LayoutMaterialEnvio();
G2S.show(telaMaterialEnvio);
telaMaterialEnvio.regiaoBotoesPacote.show(new ViewTopoBotoes());
telaMaterialEnvio.regiaoFormPesquisa.show(new ViewTelaPesquisa);


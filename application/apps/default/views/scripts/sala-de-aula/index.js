$.ajaxSetup({async: false});

categoriaPrr = 2;
var varIntegracaoView = false;
var vincularTurma = false;
var tCollection;

var Constantes = {
    PERMISSAO: 13, //Codigo de permissao para uma funcionalidade, provindo da tb_permissao
    FUNCIONALIDADE: 23, //Codigo da funcionalidade sala-de-aula, provindo da tb_funcionalidade
    SALADEAULA_SITUACAO_INATIVA: 74, //Codigo da situacao da sala de aula inativa
    SALADEAULA_SITUACAO_ATIVA: 8 //Codigo da situacao da sala de aula inativa
};

//$.getScript('js/backbone/views/SelectView.js');
//$.getScript('/js/backbone/models/SalaDeAula.js');

var VwUsuarioPerfilEntidadeReferenciaCollection = Backbone.Collection.extend({
    model: VwUsuarioPerfilEntidadeReferenciaModel,
    url: '/api/vw-usuario-perfil-entidade-referencia/'
});

var VwProfessoresPopuladaCollection = Backbone.Collection.extend({
    model: VwUsuarioPerfilEntidadeReferenciaModel
});

var VwProjetoAreaCollection = Backbone.Collection.extend({
    model: VwProjetoArea,
    url: '/api/vw-usuario-perfil-entidade-referencia/'
});

var SalaDeAulaCollection = Backbone.Collection.extend({
    model: SalaDeAula,
    url: '/default/sala-de-aula/retorna-dados-sala-edit/id_saladeaula/' + $('#idSalaDeAulaEdit').val()
});

var SalaDeAulaExistenteCollection = Backbone.Collection.extend({
    model: VwSaladeAulaIntegracao,
    url: '/vw-salade-aula-integracao/find-sala-by-entity-disc/id_disciplina/'
});

var AlocacaoCollectionExtend = Backbone.Collection.extend({
    model: Alocacao,
    url: '/api/alocacao/'
});

/**
 * Model de turma
 */
var ModelTurma = Backbone.Model.extend({
    defaults: {
        id_turmasala: null,
        checked: false,
        editando: false
    },
    idAttribute: 'id_turma'
});
/**
 * Collection que traz os tipos de turma
 */
var turmasCollection = Backbone.Collection.extend({
    model: ModelTurma
});

var SelectProfessorModel = Backbone.Model.extend({});
var SelectProfessorCollection = Backbone.Collection.extend({
    model: SelectProfessorModel
});
var SelectProfessorView = Backbone.View.extend({
    render: function () {
        this.$el.attr('value', this.model.get('id_usuario') + '#' + this.model.get('id_perfil')).text(this.model.get('st_nomecompleto'));
        return this;
    }
});
var ViewProfessor = Backbone.View.extend({
    className: 'row-fluid',
    initialize: function () {
        this.render = _.bind(this.render, this);
        this.model.bind('change', this.render);
    },
    render: function () {
        var variaveis = this.model.toJSON();
        this.el.innerHTML = _.template(this.toggleTemplate(), variaveis);
        if (this.model.is_editing) {
            this.populaSelectProfessor();
        }
        $('.dadosSala').tooltip();
        return this;
    },
    toggleTemplate: function () {
        var elementoHtml = '';
        if (this.model.is_editing) {
            elementoHtml += '<div class="span8 divProf" id="colocarTitular">'
                + '<select name="professor" class="id_usuarioprofessor">'
                + '<option value="">Selecione</option>'
                + '</select>'
                + '</div>';
            if ($('.temTitular').length == 0) {
                elementoHtml += '<div class="span2 checkProfessor">';
            } else {
                elementoHtml += '<div class="span2 checkProfessor hide">';
            }
            elementoHtml += '<label class="checkbox">Titular'
                + '<input type="checkbox" class="profTitular">'
                + '</label>'
                + '</div>'
                + '<div class="span2">'
                + '<a class="btn-danger btn delete-item pull-right btn-small" data-toggle="tooltip" title="Remover Item"><i class="icon-remove icon-white"></i></a>'
                + '</div>';
        } else {
            elementoHtml += '<div class="span8">';
            elementoHtml += '<%=st_nomecompleto%>'
                + '</div>'
                + '<div class="span2">';
            if (this.model.get('bl_titular')) {
                elementoHtml += '<label class="label label-success temTitular">Titular</label>';
            }
            elementoHtml += '</div>'
                + '<div class="span2">'
                + '<a class="btn-danger btn delete-item pull-right btn-small" data-toggle="tooltip" title="Remover Item"><i class="icon-remove icon-white"></i></a>';
            elementoHtml += '</div>';
        }
        return elementoHtml;
    },
    populaSelectProfessor: function () {
        that = this;
        var collectionSelect = new SelectProfessorCollection();
        if (typeof (Professores) != 'undefined') {
            collectionSelect = new SelectProfessorCollection(Professores);
        }
        var selectOptions = new CollectionView({
            collection: collectionSelect,
            childViewConstructor: SelectProfessorView,
            childViewTagName: 'option',
            el: that.$el.find("div.divProf").find('select[name="professor"]')
        });
        selectOptions.render();
    },
    removeProfessor: function () {
        that = this;
        var titular = that.model.get('bl_titular');
        if (that.model.id) {
            bootbox.confirm("Tem certeza de que deseja desvincular este registro?", function (result) {
                if (result) {
                    if (titular) {
                        $('.checkProfessor').each(function (index, currentObject) {
                            $(this).show();
                        });
                    }
                    that.removerProfessorMoodle(that.model.id);
                    that.model.destroy({});
                    that.remove();
                    bootbox.hideAll();
                    $.pnotify({
                        title: 'Registro Removido',
                        text: 'O registro foi desvinculado com sucesso!',
                        type: 'success'
                    });
                }
            });
        } else {
            that.remove();
        }
    },
    removerProfessorMoodle: function (idProf) {
        $.ajax({
            url: '/default/sala-de-aula/desvincular-professor-moodle',
            type: 'post',
            dataType: 'json',
            data: 'id_saladeaula=' + $('#idSalaDeAulaEdit').val() + '&id_perfilreferencia=' + idProf,
            beforeSend: function () {
                loading();
            },
            success: function (data) {
                $.pnotify({title: data.title, text: data.text, type: data.type});
            },
            error: function (data) {
                $.pnotify({title: data.title, text: data.text, type: data.type});
            },
            complete: function () {
                loaded();
            }
        });
    },
    events: {
        'click .delete-item': 'removeProfessor',
        'click .profTitular': 'selecionarTitular'
    },
    selecionarTitular: function (e) {
        var that = this;
        var objeto = that.$el.find("div.divProf").find('select[name="professor"]').find('option:selected');
        if (e.currentTarget.checked) {
            $('.profTitular').each(function (index, currentObject) {
                $(this).removeAttr('checked');
                $(this).attr('disabled', false);
            });
            e.currentTarget.checked = true;
        } else {
            $('.profTitular').each(function (index, currentObject) {
                $(this).removeAttr('checked');
                $(this).attr('disabled', false);
            });
        }
        $('#professor_titular').val(objeto[0].value);
    }
});
var listaProjetosView = Backbone.View.extend({
    model: VwProjetoArea,
    initialize: function () {
        this.render = _.bind(this.render, this);
    },
    render: function () {
        var variables = this.model.toJSON();
        var temp;
        temp = _.template($('#template-projetos-lista').html(), variables);
        this.$el.html(temp);
        loaded();
        return this;
    },
    events: {
        "click .addProjeto": "selecionaProjeto"
    },
    selecionaProjeto: function (e) {
        $(e.currentTarget).attr('disabled', true);
        var that = this;
        var model = new VwProjetoArea();
        model.set({
            id_projetopedagogico: that.model.get('id_projetopedagogico'),
            st_projetopedagogico: that.model.get('st_projetopedagogico')
        });
        collectionProjetosSa.add(model);
    }
});

var ProjetoSelecionadoItemView = Marionette.ItemView.extend({
    tagName: 'tr',
    getTemplate: function () {
        if (vincularTurma) {
            return '#template-projetos-selecionados-turma'
        } else {
            return '#template-projetos-selecionados'

        }
    },
    ui: {
        btn_vincular_turma: '#btn_vincular_turma',
        btn_visualizar_turma: '#btn_visualizar_turma',
        btn_salvar_turma: '#btn_salvar_turma'
    },
    events: {
        "click .remover": "removerProjetoSelecionado",
        "change #nu_diasacesso": "atualizaDiasAcesso",
        "click @ui.btn_vincular_turma": "carregarTurmas",
        "click @ui.btn_visualizar_turma": "visualizarTurmas",
        'click @ui.btn_salvar_turma': 'salvarTurma'
    },
    initialize: function (options) {
    },
    onRender: function () {
        if (this.model.attributes) {
            var objP = $('#id_projetopedagogico' + this.model.get('id_projetopedagogico'));
            objP.attr('disabled', true);
            objP.attr('checked', true);
        }
        return this;
    },
    removerProjetoSelecionado: function (e) {
        e.preventDefault();
        var that = this;
        bootbox.confirm("Tem certeza de que deseja desvincular este projeto da sala de aula?", function (result) {
            if (result) {
                var id_projeto = that.model.get('id_projetopedagogico');
                that.model.destroy({
                    success: function () {
                        var obj = $('#id_projetopedagogico' + id_projeto);
                        obj.removeAttr('checked').removeAttr('disabled');
                        that.$el.remove();
                        $.pnotify({
                            title: 'Sucesso!',
                            type: 'success',
                            text: 'Projeto desvinculado com sucesso da sala de aula.'
                        });

                        that.atualizaCoordenadoresIntegracao();
                        loading();
                    },
                    complete: function () {
                        loaded();
                    },
                    beforeSend: function () {
                        loading();
                    },
                    error: function () {
                        $.pnotify({
                            title: 'Erro!',
                            type: 'error',
                            text: 'Houve um erro ao tentar desvincular este projeto da sala de aula.'
                        });
                    }
                });

            }
        });
    },
    atualizaDiasAcesso: function (e) {
        this.model.set({nu_diasacesso: e.currentTarget.value});
    },
    /**
     * Atualiza a tela de integração da sala de aula para  exibir os coordenadores titulares
     * Deve ser chamada depois da ação de salvar / editar permissões
     */
    atualizaCoordenadoresIntegracao: function () {
        var id_saladeaula = $("#idSalaDeAulaEdit").val();
        if (id_saladeaula != '') {
            var dados = null;
            var url = '/default/sala-de-aula/retorna-dados-sala-edit/id_saladeaula/' + id_saladeaula;
            $.ajax({
                dataType: "json",
                url: url,
                async: false,
                success: function (resp) {
                    dados = resp;
                    listaPrincipalView.telaIntegracao(dados);
                }
            });

        }
    },
    carregarTurmas: function () {

        var layoutVinculaTurmas = new layoutBaseModal({modelo: this.model});
        layoutVinculaTurmas.render();

    },
    visualizarTurmas: function () {
        var visualizaTurmasModal = new layoutVisualizaModal({modelo: this.model});
        visualizaTurmasModal.render();
    },
    salvarTurma: function () {
        this.model.set({
            turmas: function () {
                return tCollection.findWhere({checked: true});
            }
        });
    }
});

var ProjetosSalecionadosCompositeView = Marionette.CompositeView.extend({
    childView: ProjetoSelecionadoItemView,
    childViewContainer: 'tbody',
    onRender: function () {
        return this;
    }
});


var integracaoView = Backbone.View.extend({
    el: $('#IntegracaoSala'),
    render: function () {
        loading();
        var variables = this.model;
        var temp;
        temp = _.template($('#template-integracao-sala').html(), variables);
        this.$el.html(temp);

        if ($('#idSalaDeAulaEdit').val() != '') {
            this.buscaDadosIntegracao();
            this.varificarSalaIntegrada();

            $('#reIntegrarSalaDeAula').attr('disabled', true);
            $('#btn-integrar-sala-existente').attr('disabled', true);
        }
        return this;
    },
    varificarSalaIntegrada: function () {
        $.ajax({
            url: 'vw-salade-aula-integracao/verifica-sala-integrada/id/' + ($('#idSalaDeAulaEdit').val()),
            type: 'post',
            dataType: 'json',
            success: function (data) {
                if (data.length) {
                    this.$el.find('#btn-integrar-sala-existente').attr('disabled', false);
                    this.$el.find('#integrarSalaDeAula').attr('disabled', true);
                    this.$el.find('#id_disciplinaintegracao').attr('disabled', true);
                    this.$el.find('#id_entidadeintegracao').attr('disabled', true);
                }
            }.bind(this), error: function (data) {
                $.pnotify({title: data.title, text: data.text, type: data.type});
            },
            complete: function () {
                loaded();
            }
        });
    },
    buscaDadosIntegracao: function () {
        $.ajax({
            url: 'sala-de-aula/retorna-dados-integracao-sala',
            type: 'post',
            dataType: 'json',
            data: 'id_saladeaula=' + $('#idSalaDeAulaEdit').val(),
            success: function (data) {
                if (data.length) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].st_codsistemacurso != '') {

                            this.$el.find("#id_sistemaintegrado").val(data[i].id_sistema);
                            this.$el.find("#codigoCurso").text(data[i].st_codsistemacurso);

                            this.$el.find('#integrarSalaDeAula').attr('disabled', true);
                            this.$el.find('#reIntegrarSalaDeAula').attr('disabled', false);


                            this.$el.find('#id_sistema').val(data[i].id_sistema);

                            if (data[i].id_disciplinaintegracao) {
                                this.id_disciplinaintegracao = data[i].id_disciplinaintegracao;
                                // this.$el.find('#id_disciplinaintegracao').val(data[i].id_disciplinaintegracao);
                            }
                        }
                    }
                }


            }.bind(this), error: function (data) {
                $.pnotify({title: data.title, text: data.text, type: data.type});
            },
            complete: function () {
                this.populaSelectMoodle();
                this.exibeBotoes();
                loaded();
                this.habilitaSelecaoSalaReferenciaJaIntegrada();
            }.bind(this)
        });
    },
    events: {
        "click #integrarSalaDeAula": "integrarSalaDeAula",
        "click #reIntegrarSalaDeAula": "reIntegrarSalaDeAula",
        "click #ReintegrarAlunos": "ReintegrarAlunos",
        "click #ReintegrarProfessores": "ReintegrarProfessores",
        "click #btn-integrar-sala-existente": "integrarSalaExistente",
        "click #btn-atualizar-sala-referencia": "atualizarSalaReferencia",
        "change #id_sistema": "exibeBotoes",
        "change #id_entidadeintegracao": "montaComboDisciplinaIntegracao"
    },
    validaCampoEntidadeIntegracao: function () {
        var $integrarSalaDeAula = this.$el.find("#integrarSalaDeAula");
        var $id_entidadeintegracao = this.$el.find("#id_entidadeintegracao");
        if (!$id_entidadeintegracao.val()) {
            $integrarSalaDeAula.attr("disabled", "disabled");
            $id_entidadeintegracao.focus();
            return false;
        }

        $integrarSalaDeAula.removeAttr("disabled");
        return true;

    },
    populaSelectMoodle: function () {
        // GII-9599 - Por Reinaldo: Condição valida se a sala já tem uma integração independente do status do Moodle, e mostra a integração da sala.
        if (this.model.id_entidadeintegracao) {
            ComponenteSelectMoodle.data = {
                "bl_ativo": true,
                "id_entidade": G2S.loggedUser.get("id_entidade"),
                "id_sistema": SISTEMAS.MOODLE
            };
        }

        ComponenteSelectMoodle
            .render({
                el: this.$el.find("#id_entidadeintegracao"),
                selected: this.model.id_entidadeintegracao
            }).success(function () {
            if (this.validaCampoEntidadeIntegracao()) {
                this.montaComboDisciplinaIntegracao();
            }
        }.bind(this));
    },
    montaComboDisciplinaIntegracao: function () {
        var $id_disciplinaintegracao = this.$el.find("[name='id_disciplinaintegracao']");

        if (!this.validaCampoEntidadeIntegracao()) {
            $.pnotify({
                "type": "warning",
                "title": "Atenção!",
                "text": "Selcione o Moodle para carregar as Salas de Referência."
            });
            $id_disciplinaintegracao.empty().html('<option value="">Selecione</option>')
            return;
        }

        var DisciplinaIntegracaoCollection = Backbone.Collection.extend({
            model: DisciplinaIntegracao,
            url: "/api/disciplina-integracao"
        });
        var collection = new DisciplinaIntegracaoCollection();
        collection.fetch({
            data: {
                id_disciplina: this.model.id_disciplina,
                id_sistema: SISTEMAS.MOODLE,
                id_entidadeintegracao: this.$el.find("#id_entidadeintegracao").val()
            },
            beforeSend: function () {
                loading();
                $id_disciplinaintegracao.empty().html('<option value="">Selecione</option>')
            }.bind(this),
            complete: loaded,
            success: function (collection) {
                if (!collection.length) {
                    $.pnotify({
                        "type": "warning",
                        "title": "Atenção!",
                        "text": "Nenhuma Salas de Referência encontrada."
                    });
                }

                (new SelectView({
                    el: $id_disciplinaintegracao,
                    collection: collection,
                    childViewOptions: {
                        optionLabel: 'st_salareferencia',
                        optionValue: 'id_disciplinaintegracao',
                        optionSelected: this.id_disciplinaintegracao
                    }
                })).render();

            }.bind(this)
        });
    },
    exibeBotoes: function () {
        var id_sistema = this.$el.find(".row-fluid #dadosIntegracaoSala #id_sistema").val();
        $('.div-moodle').addClass('hide');
        $('.div-bb').addClass('hide');

        if (id_sistema == SISTEMAS.MOODLE) {
            $('.div-moodle').removeClass('hide');
        } else if (id_sistema == SISTEMAS.BLACKBOARD) {
            $('.div-bb').removeClass('hide');
        }

    },
    integrarSalaExistente: function () {
        var collection = new SalaDeAulaExistenteCollection();
        collection.url += $('#id_disciplina').val();
        collection.fetch({async: false});

        (new ViewIntegraSalaExistente({
            collection: collection
        })).render();

        $('#IntegraSalaExistente').modal('show');
    },
    integrarSalaDeAula: function (e) {
        e.preventDefault();


        var param = $('#dadosIntegracaoSala').serializeArray();

        if (!this.$el.find("#id_entidadeintegracao")) {
            $.pnotify({
                "type": "warning",
                "title": "Atenção!",
                "text": "Selcione o moodle para integrar."
            });
            return;
        }


        listaPrincipalView.model.save({"id_entidadeintegracao": this.$el.find("#id_entidadeintegracao").val()},
            {
                beforeSend: loading,
                success: function () {
                    $.each(param, function (key, obj) {
                        if (obj.name == "id_entidadeintegracao") {
                            delete param[key];
                        }
                    });

                    $.pnotify({title: "Sucesso!", text: 'Moodle vinculado a sala com sucesso!', type: "success"});

                    $.ajax({
                        url: '/default/sala-de-aula/salvar-sala-integracao',
                        type: 'post',
                        dataType: 'json',
                        data: param,
                        beforeSend: loading,
                        success: function (data) {
                            if (data.type == 'success') {
                                this.$el.find('#id_sistema').attr('disabled', true);
                                this.$el.find('#integrarSalaDeAula').attr('disabled', true);
                                this.$el.find('#id_disciplinaintegracao').attr('disabled', true);
                                this.$el.find('#btn-integrar-sala-existente').attr('disabled', true);
                                this.$el.find('#codigoCurso').text(data.mensagem);
                                this.$el.find('.div-moodle').removeClass('hide');

                                $.pnotify({title: data.title, text: 'Sala integrada com sucesso!', type: data.type});
                            } else {
                                $.pnotify({title: data.title, text: data.text, type: data.type});
                                this.$el.find('#codigoCurso').text(data.mensagem);
                            }

                        }.bind(this), error: function (data) {
                            $.pnotify({title: data.title, text: data.text, type: data.type});
                        },
                        complete: loaded
                    });

                }.bind(this),
                error: function (model, response) {
                    loaded();
                    $.pnotify({
                        "text": response.responseText,
                        "type": "error",
                        "title": "Erro!"
                    });
                }
            }
        );

    },
    reIntegrarSalaDeAula: function () {

        var param = $('#dadosIntegracaoSala').serialize();
        loading();
        $.ajax({
            url: 'sala-de-aula/salvar-sala-re-integracao',
            type: 'post',
            dataType: 'json',
            data: param,
            success: function (data) {
                $('#integrarSalaDeAula').attr('disabled', true);
                $('#codigoCurso').text(data.mensagem);
                $.pnotify(data);
            }, error: function (data) {
                $.pnotify(data);
            },
            complete: function () {
                loaded();
            }
        });
    },
    ReintegrarProfessores: function () {
        if ($('#idSala').val() != '') {
            loading();
            $.ajax({
                url: 'sala-de-aula/reintegrar-professores',
                type: 'post',
                dataType: 'json',
                data: 'id_saladeaula=' + $('#idSala').val(),
                success: function (data) {
                    $.pnotify({title: data.title, text: data.mensagem, type: data.type});
                }, error: function (data) {
                    $.pnotify({title: data.title, text: data.text, type: data.type});
                },
                complete: function () {
                    loaded();
                }
            });
        }
    },
    ReintegrarAlunos: function () {
        if ($('#idSala').val() != '') {
            loading();
            $.ajax({
                url: 'sala-de-aula/reintegrar-alunos-sala',
                type: 'post',
                dataType: 'json',
                data: 'id_saladeaula=' + $('#idSala').val(),
                success: function (data) {
                    $.pnotify({title: data.title, text: data.mensagem, type: data.type});
                }, error: function (data) {
                    $.pnotify({title: data.title, text: data.text, type: data.type});
                },
                complete: function () {
                    loaded();
                }
            });
        }
    },
    habilitaSelecaoSalaReferenciaJaIntegrada: function () {
        var salaNaoIniciada = compareDate(dateToPtBr(new Date()), this.model.dt_abertura);

        // Verificação por !== 1 necessária devido à limitação do método compareDate().
        if (salaNaoIniciada !== 1 || this.salaProcessada() === false) {
            return false;
        }

        $('select[name="id_disciplinaintegracao"]').prop("disabled", false);
        $('#btn-atualizar-sala-referencia').prop("disabled", false);

    },
    atualizarSalaReferencia: function () {

        var id_disciplinaintegracao = $('select[id="id_disciplinaintegracao"]').val();
        var id_saladeaula = this.model.id_saladeaula;

        $.ajax({
            url: "sala-de-aula-integracao/atualiza-disciplina-integracao-sala",
            data: {
                'id_disciplinaintegracao': id_disciplinaintegracao,
                'id_saladeaula': id_saladeaula
            },
            beforeSend: function () {
                loading();
            },
            success: function () {
                $.pnotify({
                    'type': 'success',
                    'title': 'Sucesso',
                    'text': 'Sala de referência incluída.'
                });
            },
            complete: function () {
                loaded();
            }
        })

    },
    salaProcessada: function () {
        $.ajax({
            url: 'sala-de-aula-integracao/retrona-sala-de-aula-integracao',
            async: false,
            data: {
                id_saladeaula: this.model.id_saladeaula
            },
            success: function (response) {
                return response.bl_conteudo;
            },
            error: function (response) {

                if (response.status === 404) {
                    $.pnotify({
                        "title": "Atenção!",
                        "type": "warning",
                        "text": "Sala de aula ainda não integrada."
                    });
                    return;
                }

                $.pnotify({
                    "title": "Erro!",
                    "type": "error",
                    "text": response.responseText
                });

            }

        });
    }
});

var NoSalaIntegracaoExistente = Backbone.Marionette.ItemView.extend({
    tagName: 'tr',
    template: "#no-salaintegrada"
});

var ViewItemSalaIntegraExistente = Marionette.ItemView.extend({
    template: '#template-item-pesquisa-turma',
    tagName: 'tr',
    integrarSalaExistente: function () {
        loading();
        $.ajax({
            url: '/vw-salade-aula-integracao/integrar-sala-aula/',
            type: 'post',
            dataType: 'json',
            data: {
                'id_saladeaula': $('#idSalaDeAulaEdit').val(),
                'id_saladeaulaexistente': this.model.get('id_saladeaula'),
                'id_disciplina': $('#id_disciplina').val()
            },
            success: function (data) {
                $('#btn-integrar-sala-existente').attr('disabled', true);
                $('#integrarSalaDeAula').attr('disabled', true);
                $('#reIntegrarSalaDeAula').attr('disabled', false);
                $.pnotify({title: data.title, text: data.text, type: data.type});
                $('#codigoCurso').text(data.mensagem['st_codsistemacurso']);
                $('#IntegraSalaExistente').modal('hide');
            }, error: function (data) {
                $.pnotify({title: data.title, text: data.mensagem, type: data.type});
            },
            complete: function () {
                loaded();
            }
        });
    },
    events: {
        'click .btn-success': 'integrarSalaExistente'
    },
    initialize: function () {

    }
});

var ViewIntegraSalaExistente = Marionette.CompositeView.extend({
    el: $('#composite-content'),
    template: '#template-integra-sala-existente',
    childView: ViewItemSalaIntegraExistente,
    childViewContainer: 'tbody',
    tagName: 'div',
    className: 'row-fluid',
    emptyView: NoSalaIntegracaoExistente,
    ui: {
        st_saladeaula: 'input[name="st_saladeaula"]'
    },
    pesquisarSala: function (e) {
        var valorDigitado = e.currentTarget.value;
        var uper = valorDigitado.toUpperCase();
        var tableRows = $(".pesquisa-sala tbody>tr");

        if (valorDigitado.length > 0) {
            tableRows.hide().find("td:contains('" + uper + "')").parent("tr").show();
        } else {
            tableRows.show();
        }
    },
    events: {
        'keyup .input-search': 'pesquisarSala'
    },
    initialize: function () {

    },
    onRenderTemplate: function () {
    }
});

var collectionProjetosSa = new VwProjetoAreaCollection();

var PrincipalView = Backbone.View.extend({
    el: $('#SalaDeAula'),
    initialize: function () {
        this.telaDadosBasicos();
        /**
         * Desabilita o requerido do campo se for diferente de "Graduação"
         */
        if (G2S.loggedUser.get('id_linhadenegocio') != 2) {
            $("#id_periodoletivo").attr('required', false);
        }

        this.render();
    },
    render: function () {
        var that = this;
        if ($('#idSalaDeAulaEdit').val() == '') {
            $('#IntegracaoSala').html('<h4>Nenhuma sala cadastrada!</h4>');
            $('.div-acoes').show();
            this.verificaLinhaNegocio();
            loaded();
        } else {
            var collectionSalaEdit = new SalaDeAulaCollection();
            collectionSalaEdit.url = 'sala-de-aula/retorna-dados-sala-edit/id_saladeaula/' + $('#idSalaDeAulaEdit').val();
            loading();
            collectionSalaEdit.fetch({
                success: function () {
                    that.model = collectionSalaEdit.models[0];
                },
                complete: function () {
                    $('#id_situacao').val(collectionSalaEdit.models[0].attributes.id_situacao);

                    //Sendo preenchido pelo Zend_Form: $('#id_periodoletivo').val(collectionSalaEdit.models[0].attributes.id_periodoletivo);
                    //Sendo preenchido pelo Zend_Form: $('#id_categoriasala').val(collectionSalaEdit.models[0].attributes.id_categoriasala);

                    $('#id_modalidadesaladeaula').val(collectionSalaEdit.models[0].attributes.id_modalidadesaladeaula);
                    $('#nu_maxalunos').val(collectionSalaEdit.models[0].attributes.nu_maxalunos);
                    $('#dt_inicioinscricao').val(collectionSalaEdit.models[0].attributes.dt_inicioinscricao);
                    $('#dt_fiminscricao').val(collectionSalaEdit.models[0].attributes.dt_fiminscricao);
                    $('#dt_abertura').val(collectionSalaEdit.models[0].attributes.dt_abertura);
                    $('#dt_encerramento').val(collectionSalaEdit.models[0].attributes.dt_encerramento);
                    $('#nu_diasaluno').val(collectionSalaEdit.models[0].attributes.nu_diasaluno);
                    $('#nu_diasencerramento').val(collectionSalaEdit.models[0].attributes.nu_diasencerramento);

                    if (collectionSalaEdit.models[0].attributes.bl_ofertaexcepcional) {
                        $('#bl_ofertaexcepcional').attr('checked', true);
                    }
                    if (collectionSalaEdit.models[0].attributes.bl_vincularturma) {
                        $('#bl_vincularturma').attr('checked', true);
                        vincularTurma = true;
                    }
                    if (collectionSalaEdit.models[0].attributes.nu_diasextensao) {
                        $('#lbEncerramento').attr('data-content', 'Data de encerramento após extensão: ' + collectionSalaEdit.models[0].attributes.dt_comextensao + '.');
                    }
                    if (collectionSalaEdit.models[0].attributes.bl_semdiasaluno) {
                        $('#nu_diasaluno').attr('readonly', true);
                        $('#bl_semdiasaluno').attr('checked', true);
                    }
                    if (collectionSalaEdit.models[0].attributes.bl_semencerramento) {
                        $('#nu_diasencerramento').attr('readonly', true);
                        $('#bl_semencerramento').attr('checked', true);
                    }
                    $('#id_disciplina').val(collectionSalaEdit.models[0].attributes.id_disciplina);
                    $('#st_disciplina').text(collectionSalaEdit.models[0].attributes.st_disciplina);
//                    $('#id_categoriasala').attr('disabled', true);
                    if (collectionSalaEdit.models[0].attributes.bl_todasentidades) {
                        $('#salaDadosBasicos').append('<input type="hidden" id="todasEntidades" value="' + collectionSalaEdit.models[0].attributes.bl_todasentidades + '">');
                    } else {
                        $('#salaDadosBasicos').append('<input type="hidden" id="todasEntidades" value="' + 0 + '">');
                    }

                    if ($('#id_categoriasala').val() == categoriaPrr) {
                        $('#editarProdutoPrr').show();
                    }

                    that.renderTipoConjuntoSelectedEdit($('#idSalaDeAulaEdit').val(), collectionSalaEdit.models[0].get('id_disciplina'));
                    $('#salvarSalaDeAula').hide();
                    $('#editarSalaDeAula').show();
                    $('#mostrarPermissoesEdit').show();
                    that.telaIntegracao(collectionSalaEdit.models[0].attributes);

                    //Ocultando botao cancelar sala para reprocessamento baseado nas novas informacoes
                    //salvas para a sala de aula no (render).
                    $('#cancelarSala').hide();

                    //Verificando se a sala esta ativa para mostrar botoes de acoes
                    if (!parseInt(that.model.get('id_usuariocancelamento'))) {
                        $('.div-acoes').show();

                        //Verificando se a sala ainda nao foi iniciada
                        var salaAberta = compareDate(dateToPtBr(new Date()), collectionSalaEdit.models[0].get('dt_abertura'));

                        if (salaAberta == 1 && parseInt(that.model.get('id_situacao')) == Constantes.SALADEAULA_SITUACAO_ATIVA) {

                            //Verificando se o usuario tem permissao para ver a funcionalidade
                            if (VerificarPerfilPermissao(Constantes.PERMISSAO, Constantes.FUNCIONALIDADE).length) {
                                $('#cancelarSala').show();
                            }
                        }

                    } else {
                        $('.div-alert #mensagem').text('A Sala de Aula foi Cancelada dia ' + that.model.get('dt_cancelamento') + ' por ' + that.model.get('st_nomeusuariocancelamento'));
                        $('.div-alert').show();
                        $('.div-acoes').hide();
                    }

                    that.verificaLinhaNegocio();

                    loaded();
                }
            });

        }
    },
    events: {
        "submit #form-cadastrar-sala-de-aula": "salvarDadosBasicos",
        "click #salvarPermissoes": "salvarPermissoes",
        "click #ck_todasentidades": "todasEntidades",
        "click #salvarSalaProdutoPrr": "salvarSalaProdutoPrr",
        "click #marcaTodosI": "marcaTodos",
        "click #editarSalaDeAula": "editarSalaDeAula",
        "click #novaSalaDeAula": "novaSalaDeAula",
        "click #mostrarPermissoesEdit": "mostrarPermissoesEdit",
        "click #editarPermissoes": "editarPermissoes",
        "click #editarProdutoPrr": "mostrarProdutoPrr",
        "click #editarSalaProdutoPrr": "editarSalaProdutoPrr",
        "keyup .input-search": "pesquisaProjetos",
        'click #fieldSetProfessores .add-item': 'addItemProfessor',
        'click #id_avaliacaoconjuntoh': 'renderTipoConjuntoSelected',
        'click #bl_semdiasaluno': 'toggleCampoSemDiasAluno',
        'click #bl_semencerramento': 'toggleCampoSemEncerramento',
        'click #cancelarSala': 'cancelarSala',
        'click #salvarSalaDeAula': 'validaConjuntoAvalicao',
        // history GRA-38
        'change #id_periodoletivo': 'verificaLinhaNegocio'
    },
    verificaLinhaNegocio: function () {
        var elem = $('#id_periodoletivo');
        var val = elem.val();
        var dates = $('.dateg');

        if (2 == G2S.loggedUser.get('id_linhadenegocio')) {
            // Se a linha de negocio da entidade for GRADUACAO

            // pointer-events impede que abra o calendario quando o input tiver focus
            dates.prop('readonly', true).css('pointer-events', 'none');

            if (val) {
                loading();
                $.getJSON('/api/periodo-letivo/' + val, function (response) {
                    var date_indexes = ['dt_inicioinscricao', 'dt_fiminscricao', 'dt_abertura', 'dt_encerramento'];

                    for (var i in date_indexes) {

                        var index = date_indexes[i];
                        var input = $('#' + index);

                        if (response[index]) {
                            var dt_br = response[index].substr(0, 10).split('-').reverse().join('/');
                            input.val(dt_br);
                        } else {
                            input.val('');
                        }
                    }

                    loaded();
                });
            } else {
                dates.val('');
            }

        } else {
            dates.prop('readonly', false).css('pointer-events', 'auto');
        }
    },
    //Valida o combo Conjunto de Avalicação ao clicar no botão de envio do form.
    validaConjuntoAvalicao: function () {
        if ($('#id_avaliacaoconjunto').hasClass('hide')) {
            this.renderTipoConjuntoSelected();
            return false;
        }
    },
    renderTipoConjuntoSelectedEdit: function (id_saladeaula, id_disciplina) {
        id_avaliacaoconjunto = '';

        var VwAvaliacaoConjuntoCollection = Backbone.Collection.extend({
            model: VwAvaliacaoConjuntoReferencia
        });

        var VwAvaliacaoConjuntoDisciplinaCollection = Backbone.Collection.extend({
            model: VwAvaliacaoConjuntoDisciplina
        });

        var collection = new VwAvaliacaoConjuntoDisciplinaCollection();
        collection.url = '/api/vw-avaliacao-conjunto-disciplina?id_disciplina=' + id_disciplina;
        collection.fetch({
            success: function () {

                /**
                 * Verificando se existe uma opcao do combo de conjunto salva no banco
                 * para marcar automaticamente no carregamento da pagina em edicao
                 */
                if (id_saladeaula) {
                    var vwAvaliacaoConjunto = new VwAvaliacaoConjuntoCollection();
                    vwAvaliacaoConjunto.url = '/api/vw-avaliacao-conjunto-referencia?id_saladeaula=' + id_saladeaula;
                    vwAvaliacaoConjunto.fetch({
                        async: false,
                        complete: function () {
                            if (vwAvaliacaoConjunto.length) {
                                id_avaliacaoconjunto = vwAvaliacaoConjunto.models[0].get('id_avaliacaoconjunto');
                            }
                        }
                    });
                }

                $('select#id_avaliacaoconjunto').html('').attr('required', true);
                $('select#id_avaliacaoconjunto').append('<option value="">Selecione</center></option>');

                var view = new SelectView({
                    el: $('#form-cadastrar-sala-de-aula #id_avaliacaoconjunto'),
                    collection: collection,
                    childViewOptions: {
                        optionLabel: 'st_avaliacaoconjunto',
                        optionValue: 'id_avaliacaoconjunto',
                        optionSelected: id_avaliacaoconjunto
                    }
                });

                $('#id_avaliacaoconjunto').removeClass('hide');
                $('#id_avaliacaoconjuntoh').addClass('hide');
                view.render();
            },
            error: function () {
                $.pnotify({title: 'ATENÇÂO', text: 'Desculpe, houve um erro na requisição.', type: 'danger'});
            },
            beforeSend: function () {
                loading();
            },
            complete: function () {
                if (!collection.length) {
                    //Verificando e desabilitando campo caso a disciplina nao tenha conjunto avaliacao
                    $.pnotify({
                        title: 'ATENÇÃO',
                        text: 'Essa disciplina não possui conjunto de avaliação cadastrado!',
                        type: 'warning'
                    });
                }
                loaded();
            }
        });

    },
    renderTipoConjuntoSelected: function () {

        $('#id_avaliacaoconjuntoh').addClass('hide');
        $('#id_avaliacaoconjunto').removeClass('hide');
        $('#id_avaliacaoconjunto').removeAttr('disabled');

        var id_disciplina = '';
        var id_avaliacaoconjunto = '';
        if ($('#id_disciplina').val() != '') {
            id_disciplina = $('#id_disciplina').val();
        } else {
            $.pnotify({title: 'Atenção', text: 'Primeiro, selecione uma disciplina.', type: 'warning'});
            return false;
        }

        var VwAvaliacaoConjuntoDisciplinaCollection = Backbone.Collection.extend({
            model: VwAvaliacaoConjuntoDisciplina
        });

        var collection = new VwAvaliacaoConjuntoDisciplinaCollection();
        collection.url = '/api/vw-avaliacao-conjunto-disciplina?id_disciplina=' + id_disciplina;
        collection.fetch();
        $('select#id_avaliacaoconjunto').html('');
        var view = new SelectView({
            el: $('#form-cadastrar-sala-de-aula #id_avaliacaoconjunto'),
            collection: collection,
            childViewOptions: {
                optionLabel: 'st_avaliacaoconjunto',
                optionValue: 'id_avaliacaoconjunto',
                optionSelected: id_avaliacaoconjunto
            }
        });

        view.render();
    },
    pesquisaProjetos: function (e) {
        var valorDigitado = e.currentTarget.value;
        var uper = valorDigitado.toUpperCase();
        var tableRows = $(".pesquisa-projetos tbody>tr");
        if (valorDigitado.length > 0) {
            tableRows.hide().find("td:contains('" + uper + "')").parent("tr").show();
        } else {
            tableRows.show();
        }

    },
    telaDadosBasicos: function () {
        var temp;

        temp = _.template($('#template-sala-dados-basicos').html());
        this.$el.find("#form-cadastrar-sala-de-aula").append(temp);
        $('#abas-sala-de-aula a[href="#SalaDeAula"]').tab('show');
        $('.dadosSala').popover();
        return this;
    },
    editarSalaDeAula: function () {
        var that = this;

        var bl_semdiasaluno = $('#bl_semdiasaluno').is(':checked') ? 1 : 0;
        var bl_semencerramento = $('#bl_semencerramento').is(':checked') ? 1 : 0;
        var bl_ofertaexcepcional = $('#bl_ofertaexcepcional').is(':checked') ? 1 : 0;
        var bl_vincularturma = $('#bl_vincularturma').is(':checked') ? 1 : 0;

        //Validacao de campo obrigatorio
        if ($('#id_avaliacaoconjunto').val() == '' || $('#id_avaliacaoconjunto').is(':disabled')) {
            $.pnotify({title: 'ATENÇÃO', text: 'O campo Conjunto Avaliação é obrigatório', type: 'warning'});

            return false;
        }


        if (compareDate($('#dt_inicioinscricao').val(), $('#dt_abertura').val()) == -1) {
            $.pnotify({
                type: 'warning',
                title: 'ATENÇÃO',
                text: 'A data de Início da Inscrição deve ser menor que a data de Abertura'
            });
            $('#divInscricao').addClass('control-group error');
            $('#divAbertura').addClass('control-group error');
            return false;
        } else {
            $('#divInscricao').removeClass('control-group error');
            $('#divAbertura').removeClass('control-group error');
        }

        var parametros = $('#form-cadastrar-sala-de-aula').serialize();
        parametros += '&bl_semencerramento=' + bl_semencerramento + '&bl_semdiasaluno=' + bl_semdiasaluno + '&bl_ofertaexcepcional=' + bl_ofertaexcepcional + '&id_saladeaula=' + $('#idSalaDeAulaEdit').val() + '&bl_vincularturma=' + bl_vincularturma;
        loading();
        $.ajax({
            url: 'sala-de-aula/editar-sala-dados-basicos',
            type: 'post',
            dataType: 'json',
            data: parametros,
            success: function (data) {
                $.pnotify({title: data.title, text: data.text, type: data.type});
                $('#id_avaliacaoconjunto').removeClass('alert-error');
            }, error: function (data) {
                $.pnotify({title: data.title, text: data.text, type: data.type});
            },
            complete: function () {
                loaded();
                that.render();
                if ($('#mostrarPermissoesEdit')[0].disabled) {
                    that.carregaProjetos('edit');
                }
                vincularTurma = bl_vincularturma;
            }
        });
    },
    editarPermissoes: function () {
        var that = this;
        var checked_ids = [];
        var ids_projeto = [];
        var ids_professores = [];
        var bl_todasentidades = 0;
        $("#entidades").jstree("get_checked", null, true).each
        (function () {
            checked_ids.push(this.id);
        }); //buscando valores da árvore

        if ($('#ck_todasentidades').is(':checked')) {
            bl_todasentidades = 1;
        }

        if (!checked_ids.length && !bl_todasentidades) {
            $.pnotify({title: 'Atenção', text: 'Selecione uma organização!', type: 'warning'});
            return false;
        } else {
            $('#todasEntidades').val(bl_todasentidades);
            $('div#idProjetosSel table#template-projetos-selecionados').each(function (index, currentObject) {
                ids_projeto.push(currentObject.value);
            });
            $('.id_usuarioprofessor option:selected').each(function (index, currentObject) {
                ids_professores.push(currentObject.value);
            });
            loading();

            var projetos = [];

            _.each(collectionProjetosSa.models, function (model) {

                var turmas = [];
                if (typeof model.get('turmas') != 'undefined' && model.get('turmas').length) {
                    _.each(model.get('turmas'), function (elem) {
                        var x = {
                            id_turma: elem.get('id_turma')
                        };
                        turmas.push(x)
                    })
                }

                var x = {
                    id: model.get('id'),
                    id_projetopedagogico: model.get('id_projetopedagogico'),
                    st_projetopedagogico: model.get('st_projetopedagogico'),
                    nu_diasacesso: model.get('nu_diasacesso'),
                    id_areaconhecimento: model.get('id_areaconhecimento'),
                    st_areaconhecimento: model.get('st_areaconhecimento'),
                    st_tituloexibicao: model.get('st_tituloexibicao'),
                    tipo: model.get('tipo'),
                    id_entidade: model.get('id_entidade'),
                    turmas: turmas.length ? turmas : null
                };
                projetos.push(x);
            });

            var parametros = {
                projetos: projetos,
                bl_todasentidades: bl_todasentidades,
                id_entidade: checked_ids,
                idProfessores: ids_professores,
                id_saladeaula: $('#idSalaDeAulaEdit').val(),
                professor_titular: $('#professor_titular').val(),
                serialize: $('#salvarSalaPermissoes').serialize()
            };
            $.ajax({
                url: 'sala-de-aula/editar-sala-permissoes',
                type: 'post',
                dataType: 'json',
                data: parametros,
                success: function (data) {
                    $.pnotify({title: data.title, text: data.text, type: data.type});
                    that.telaPermissoesEdit();
                }, error: function (data) {
                    $.pnotify({title: data.title, text: data.text, type: data.type});
                },
                complete: function () {
                    loaded();
                }
            });
        }


    },
    editarSalaProdutoPrr: function () {
        var parametros = $('#editarSalaProdutoPrr').serialize();
        parametros += '&id_saladeaula=' + $('#idSaladeaula').val();
        loading();
        $.ajax({
            url: 'sala-de-aula/editar-produto-valor-prr',
            type: 'post',
            dataType: 'json',
            data: parametros,
            success: function (data) {
                $('#abas-sala-de-aula a[href="#IntegracaoSala"]').tab('show');
                $.pnotify({title: data.title, text: data.text, type: data.type});
            }, error: function (data) {
                $.pnotify({title: data.title, text: data.text, type: data.type});
            },
            complete: function () {
                loaded();
            }
        });
    },
    mostrarProdutoPrr: function () {
        var that = this;
        var temp1;
        var parametros = 'id_saladeaula=' + $('#idSalaDeAulaEdit').val();
        loading();
        $.ajax({
            url: 'sala-de-aula/retorna-produto-prr',
            type: 'post',
            dataType: 'json',
            data: parametros,
            success: function (data) {
                if (data == '' || data == undefined) {
                    that.telaProdutoPrr();
                    $('#salaProdutoPrr').show();
                } else {
                    var variables = new ProdutoValor(data);
                    var dados = variables.toJSON();
                    temp1 = _.template($('#template-sala-produto-prr-edit').html(), dados[0]);
                    that.$el.append(temp1);
                }
                $('#editarProdutoPrr').attr('disabled', true);
            }, error: function (data) {
                $.pnotify({title: data.title, text: data.text, type: data.type});
            },
            complete: function () {
                loaded();
            }
        });
        $('.dadosSala').popover();
        return that;
    },
    mostrarPermissoesEdit: function () {
        var that = this;
        that.telaPermissoesEdit();
        $('.dadosSala').tooltip();
        $('#mostrarPermissoesEdit').attr('disabled', true);
    },
    novaSalaDeAula: function () {
        $('#IntegracaoSala').html('<h4>Nenhuma sala cadastrada!</h4>');
        var that = this;
        that.model.destroy({});
        that.telaDadosBasicos();
    },
    carregaProjetos: function (editar) {
        var that = this;
        loading();
        var collectionProjetos = new VwProjetoAreaCollection();
        collectionProjetos.url = 'sala-de-aula/retorna-projeto-pedagogico';
//        loading();
        $('#lista-projetos-pedagogicos tbody').html('Carregando...');
        collectionProjetos.fetch({
            success: function () {
                var tabelaProjetoView = new CollectionView({
                    collection: collectionProjetos,
                    childViewTagName: 'tr',
                    childViewConstructor: listaProjetosView,
                    el: $('#lista-projetos-pedagogicos tbody')
                });
                tabelaProjetoView.render();
            },
            complete: function () {
                var viewSelecionados = new ProjetosSalecionadosCompositeView({
                    template: that.model.get('bl_vincularturma') ? '#template-table-projetos-selected-turma' : '#template-table-projetos-selected',
                    collection: collectionProjetosSa,
                    el: $('div#divProjetosSelecionados')
                });
                viewSelecionados.render();
                if (editar == 'edit') {
                    that.carregaProjetosSelecionadosEdit();
                }
//                loaded();
            }
        });
        loaded();
    },
    carregaProjetosSelecionadosEdit: function () {
        var that = this;

        collectionProjetosSa.url = 'sala-de-aula/retorna-projeto-sala/';
        collectionProjetosSa.fetch({
            data: {
                id_saladeaula: $('#idSalaDeAulaEdit').val()
            },
            success: function () {
                _.each(collectionProjetosSa.models, function (model) {
                    var arr = [];
                    _.each(model.get('turmas'), function (turma) {
                        arr.push(new ModelTurma(turma));
                    });
                    model.set({turmas: arr})
                });
            },
            complete: function () {
                loaded();
            },
            beforeSend: function () {
                loading();
            }
        });
    },
    carregaProfessores: function () {
        loading();
        var idSalaDeAulaE = $('#idSalaDeAulaEdit').val();
        var collectionProfessor = new VwUsuarioPerfilEntidadeReferenciaCollection();
        if (idSalaDeAulaE != '') {
            collectionProfessor.url += 'id_saladeaula/' + idSalaDeAulaE;
            collectionProfessor.fetch({
                success: function () {
                    renderer = new CollectionView({
                        collection: collectionProfessor,
                        childViewConstructor: ViewProfessor,
                        childViewTagName: 'div',
                        el: $('#containerProfessores')
                    }).render();
                }
            });
        } else {
            this.addItemProfessor();
        }
        $('.dadosSala').tooltip();
        loaded();
    },
    addItemProfessor: function () {
        var collectionProfessor = new VwUsuarioPerfilEntidadeReferenciaCollection();
        renderer = new CollectionView({
            collection: collectionProfessor,
            childViewConstructor: ViewProfessor,
            childViewTagName: 'div',
            el: $('#containerProfessores')
        });
        renderer._rendered = true;
        var model = new VwUsuarioPerfilEntidadeReferenciaModel();
        model.toggleEdit();
        renderer.add(model);
    },
    telaPermissoesEdit: function (dados) {
        $('#dadosPermissoes').remove();
        var temp1;
        temp1 = _.template($('#template-sala-permissoes').html(), dados);
        this.$el.append(temp1);
        this.carregaEntidade();
        this.carregaProjetos('edit');
        this.carregaProfessores();
        $('#editarPermissoes').show();
        $('#salvarPermissoes').hide();
        $('#idSaladeaula').val('');
        return this;
    },
    telaPermissoes: function () {
        var temp1;
        temp1 = _.template($('#template-sala-permissoes').html());
        this.$el.append(temp1);
        this.carregaEntidade();
        this.carregaProjetos('');
        this.carregaProfessores();
        return this;
    },
    telaProdutoPrr: function () {
        var temp1;
        temp1 = _.template($('#template-sala-produto-prr').html());
        this.$el.append(temp1);
        this.formatacao();
        return this;
    },
    salvarDadosBasicos: function () {
        var that = this;

        var bl_ofertaexcepcional = $('#bl_ofertaexcepcional').is(':checked') ? 1 : 0;
        var bl_semdiasaluno = $('#bl_semdiasaluno').is(':checked') ? 1 : 0;
        var bl_semencerramento = $('#bl_semencerramento').is(':checked') ? 1 : 0;
        var bl_vincularturma = $('#bl_vincularturma').is(':checked') ? 1 : 0;

        if (new Date($('#dt_inicioinscricao').val()) > new Date($('#dt_abertura').val())) {
            $.pnotify({
                title: 'ATENÇÃO',
                text: 'A data de Inscrição deve ser menor que a data de Abertura',
                type: 'warning'
            });
            $('#divInscricao').addClass('control-group error');
            $('#divAbertura').addClass('control-group error');
            return false;
        } else {
            $('#divInscricao').removeClass('control-group error');
            $('#divAbertura').removeClass('control-group error');
        }

        var parametros = $('#form-cadastrar-sala-de-aula').serialize();
        parametros += '&bl_semencerramento=' + bl_semencerramento + '&bl_semdiasaluno=' + bl_semdiasaluno + '&bl_ofertaexcepcional=' + bl_ofertaexcepcional + '&bl_vincularturma=' + bl_vincularturma;
        loading();
        $.ajax({
            url: 'sala-de-aula/salvar-sala-dados-basicos',
            type: 'post',
            dataType: 'json',
            data: parametros,
            success: function (data) {

                /* Verifica se o retorno informa a existência de uma sala de aula
                 idêntica. Caso positivo, exibe o alerta na tela. */

                if (data.existeSalaComMesmoNome) {
                    $.pnotify({
                        'title': data.title,
                        'text': data.text,
                        'type': data.type
                    })
                } else {
                    $('#salvarSalaDeAula').hide();
                    //that.telaPermissoes();
                    //that.telaIntegracao(data.mensagem);
                    $.pnotify({title: data.title, text: data.text, type: data.type});
                    $('#idSaladeaula').val(data.mensagem.id_saladeaula);
                    $('#idSalaDeAulaEdit').val(data.mensagem.id_saladeaula);
                    if (data.mensagem.id_categoriasala == categoriaPrr) {
                        that.telaProdutoPrr();
                    }
                }
            },
            error: function (data) {
                $.pnotify({title: data.title, text: data.text, type: data.type});
            },
            complete: function () {
                loaded();
                that.render();
            }
        });

        //Impede que a tela seja completamente recarregada.
        return false;
    },
    telaIntegracao: function (dados) {
        if (varIntegracaoView) {
            varIntegracaoView.model = dados;
            varIntegracaoView.render();
        } else {
            varIntegracaoView = new integracaoView({model: dados}).render();
        }
        //var view = new integracaoView({model: dados});
        //view.render();
    },
    formatacao: function () {
        $('#nu_valor').maskMoney({
            prefix: 'R$ ',
            allowNegative: true,
            thousands: '.',
            decimal: ',',
            affixesStay: false
        });
    },
    carregaEntidade: function () {
        var that = this;
        loading();
        var id_saladeaula = $("#idSalaDeAulaEdit").val();
        if (id_saladeaula != '') {
            var sala = null;
            var url = '/default/sala-de-aula/retorna-dados-sala-edit/id_saladeaula/' + id_saladeaula;
            $.ajax({
                dataType: "json",
                url: url,
                async: false,
                success: function (resp) {
                    sala = resp;
                    that.telaIntegracao(sala);
                }
            });

            if (sala != null && sala.bl_todasentidades == true) {
                $('#ck_todasentidades').attr('checked', true);
                $('#entidades').hide();
            } else {
                $('#ck_todasentidades').attr('checked', false);
                $('#entidades').show();
            }
        } else {
            $('#ck_todasentidades').attr('checked', false);
            $('#entidades').show();
        }

        $("#entidades").jstree({
            "html_data": {
                "ajax": {
                    "url": "/default/util/arvore-entidade",
                    "data": function (n) {
                        return {id: n.attr ? n.attr("id") : 0};
                    }
                }
            },
            "plugins": ["themes", "html_data", "checkbox", "sort", "ui"]
        }).bind("loaded.jstree", function (event, data) {
            if ($("#idSalaDeAulaEdit").val() != '') {
                $.ajax({
                    url: "sala-de-aula/recarrega-arvore-sala",
                    type: 'POST',
                    dataType: 'json',
                    data: 'id_saladeaula=' + $("#idSalaDeAulaEdit").val(),
                    success: function (data) {
                        $("#entidades").jstree("get_unchecked", null, true).each
                        (function () {
                            for (var i = 0; i < data.length; i++) {
                                if (data[i] == this.id) {
                                    $.jstree._reference("#entidades").check_node('li#' + this.id);
                                }
                            }
                        });
                    }
                });
            }
        });
        loaded();
    },
    salvarPermissoes: function () {
        var checked_ids = [];
        var ids_projeto = [];
        var ids_professores = [];
        var bl_todasentidades = 0;
        $("#entidades").jstree("get_checked", null, true).each
        (function () {
            checked_ids.push(this.id);
        }); //buscando valores da árvore

        if ($('#ck_todasentidades').is(':checked')) {
            bl_todasentidades = 1;
        }

        $('div#idProjetosSel table#lista-projetos-pedagogicos input:checkbox:checked').each(function (index, currentObject) {
            ids_projeto.push(currentObject.value);
        });
        $('.id_usuarioprofessor option:selected').each(function (index, currentObject) {
            ids_professores.push(currentObject.value);
        });
        var parametros = {
            projetos: collectionProjetosSa.toJSON(),
            bl_todasentidades: bl_todasentidades,
            id_entidade: checked_ids,
            idProfessores: ids_professores,
            id_saladeaula: $('#idSalaDeAulaEdit').val(),
            professor_titular: $('#professor_titular').val(),
            serialize: $('#salvarSalaPermissoes').serialize()
        };
        $.ajax({
            url: '/default/sala-de-aula/salvar-sala-permissoes',
            type: 'post',
            dataType: 'json',
            data: parametros,
            success: function (data) {
                $('#salaProdutoPrr').show();
                $('#salvarPermissoes').prop('disabled', true);

                $.pnotify({title: data.title, text: data.text, type: data.type});
            }, error: function (data) {
                $.pnotify({title: data.title, text: data.text, type: data.type});
            },
            complete: function () {
                $('#salvarPermissoes').prop('disabled', false);

                loaded();
            },
            beforeSend: function () {
                loading();
            }
        });
    },
    todasEntidades: function (e) {
        $('#entidades').slideToggle();
    },
    salvarSalaProdutoPrr: function () {
        var parametros = $('#cadastroSalaProdutoPrr').serialize();
        parametros += '&id_saladeaula=' + $('#idSaladeaula').val();

        $.ajax({
            url: '/default/sala-de-aula/salvar-produto-valor-prr',
            type: 'post',
            dataType: 'json',
            data: parametros,
            success: function (data) {
                $('#salvarSalaProdutoPrr').hide();
                $('#abas-sala-de-aula a[href="#IntegracaoSala"]').tab('show');
                $.pnotify({title: data.title, text: data.text, type: data.type});
            }, error: function (data) {
                $.pnotify({title: data.title, text: data.text, type: data.type});
            },
            complete: function () {
                loaded();
            },
            beforeSend: function () {
                loading();
            }
        });
    },
    marcaTodos: function (e) {
        if (e.target.checked === true) {
            $(":checkbox[class*=checkall]").each(function () {
                this.checked = true;
                this.disabled = true;
                $('#lista-projetos-pedagogicos-selecionados tbody').html('<tr><td colspan="2">Todos os projetos foram selecionados!</td></tr>');
            });
        } else {
            $(":checkbox[class*=checkall]").each(function () {
                this.checked = false;
                this.disabled = false;
                $('#lista-projetos-pedagogicos-selecionados tbody').html('');
            });
        }
    },
    toggleCampoSemDiasAluno: function (e) {
        if ($(e.currentTarget).is(':checked')) {
            $('#nu_diasaluno').attr('readonly', true);
        } else {
            $('#nu_diasaluno').removeAttr('readonly');
        }
    },
    toggleCampoSemEncerramento: function (e) {
        if ($(e.currentTarget).is(':checked')) {
            $('#nu_diasencerramento').attr('readonly', true);
        } else {
            $('#nu_diasencerramento').removeAttr('readonly');
        }
    },
    cancelarSala: function () {

        var AlocacaoCollection = new AlocacaoCollectionExtend();

        AlocacaoCollection.url = '/api/alocacao/index/id_saladeaula/' + this.model.get('id_saladeaula') + '/bl_ativo/1';

        var that = this;

        AlocacaoCollection.fetch({
            beforeSend: function () {
                loading();
            },
            complete: function () {
                loaded();
            },
            success: function () {
                bootbox.confirm('<h5>A Sala de Aula contém <strong>' + AlocacaoCollection.length + '</strong> alunos alocados. Ao cancelar a Sala de Aula todos os alunos serão desalocados. Deseja cancelar a Sala de Aula?</h5>', function (result) {
                    if (result) {
                        that.model.url = '/default/sala-de-aula/cancelar-sala-de-aula';
                        that.model.save(null, {
                            beforeSend: loading,
                            complete: function () {
                                that.render();
                                $.pnotify({title: 'Sucesso', text: 'Sala desativada com sucesso', type: 'success'});
                                loaded();
                            }
                        });
                    }
                });
            }
        });

    },
});

/**
 * Item view de projetos
 */
var itemViewTurmaItem = Marionette.ItemView.extend({
    template: '#item-turma',
    tagName: 'tr',
    ui: {
        chkTurma: '#chkTurma'
    },
    events: {
        'change @ui.chkTurma': 'changeChkBoxTurma'
    },
    initialize: function () {
    },
    onRender: function () {
    },
    changeChkBoxTurma: function (el) {
        this.model.set({checked: el.currentTarget.checked});
    }
});

var layoutVisualizaModal = Backbone.Marionette.LayoutView.extend({
    template: '#base-modal',
    el: '#box-modal',
    regions: {
        regiao1: '#regiao1'
    },
    onShow: function () {
    },
    onRender: function () {
        var compModal = new compositeVisualizaTurmaModal({modelo: this.options.modelo});
        this.regiao1.show(compModal);
    }
});


/**
 * Item view para visualizacao das turmas vinculadas
 * @type {ItemView}
 */
var ViewItemVisualizaTurma = Marionette.ItemView.extend({
    template: _.template('<%=st_turma%>'),
    tagName: 'p'
});

/**
 * Item view para visualizacao das turmas vinculadas
 * @type {ItemView}
 */
var ViewItemVisualizaSemTurma = Marionette.ItemView.extend({
    template: '#empty-nenhuma-turma',
});


/**
 * CollectionView para visualizacao de turmas vinculadas
 * @type {CollectionView}
 */
var VisualizaTurmasCollectionView = Marionette.CollectionView.extend({
    tagName: 'div',
    childView: ViewItemVisualizaTurma,
    emptyView: ViewItemVisualizaSemTurma
});

/**
 * Composite para visualizacao de turmas vinculadas
 */
var compositeVisualizaTurmaModal = Backbone.Marionette.CompositeView.extend({
    template: '#visualizaTurmas',
    tagName: 'div',
    className: 'modal hide fade modal-large',
    ui: {
        fechar: '#btn_fechar'
    },
    events: {
        'click @ui.fechar': 'fechar'
    },
    onShow: function () {
        var that = this;
        tCollection = new turmasCollection();
        tCollection.add(that.options.modelo.get('turmas'));

        var vTCollection = new VisualizaTurmasCollectionView({
            collection: tCollection,
            el: '#listaTurmasDisponiveis'
        });
        vTCollection.render();
        this.$el.modal('show');
    },
    fechar: function () {
        this.$el.modal('hide');
        tCollection = new turmasCollection();
    }
});

/**
 *
 * Composites, Layouts e ItensView para Vinculo de turma
 *
 */

var layoutBaseModal = Backbone.Marionette.LayoutView.extend({
    template: '#base-modal',
    el: '#box-modal',
    regions: {
        regiao1: '#regiao1'
    },
    onShow: function () {
    },
    onRender: function () {
        var compModal = new compositeModal({modelo: this.options.modelo});
        this.regiao1.show(compModal);
    }
});

/**
 * Composite para vinculo de turma
 */
var compositeModal = Backbone.Marionette.CompositeView.extend({
    template: '#pesquisaTurmas',
    tagName: 'div',
    className: 'modal hide fade modal-large',
    ui: {
        salvarTurma: '#btn_salvar_turma',
        fechar: '#btn_fechar'
    },
    events: {
        'click @ui.salvarTurma': 'salvarTurma',
        'click @ui.fechar': 'fechar',
        'keyup .input-search-turmas': 'pesquisarTurma'
    },
    pesquisarTurma: function (e) {
        var valorDigitado = e.currentTarget.value;
        var uper = valorDigitado.toUpperCase();
        var tableRows = $(".pesquisa-turma tbody>tr");

        if (valorDigitado.length > 0) {
            tableRows.hide().find("td:contains('" + uper + "')").parent("tr").show();
        } else {
            tableRows.show();
        }
    },
    onShow: function () {
        var that = this;
        var checked_ids = [];
        var novoPj = null;


        $.ajax({
            url: '/sala-de-aula/verifica-area-projeto-sala',
            type: 'post',
            dataType: 'json',
            async: false,
            data: {
                id_projetopedagogico: that.options.modelo.get('id_projetopedagogico'),
                id_saladeaula: $('#idSalaDeAulaEdit').val(),
            },
            beforeSend: function () {
                loading();
                $("#entidades").jstree("get_checked", null, true).each
                (function () {
                    checked_ids.push(this.id);
                });
            },
            success: function (data) {
                if (data.length) {
                    novoPj = false;
                } else {
                    novoPj = true;
                }
            }, error: function () {
            },
            complete: function () {
                loaded();
            }
        });
        var parametros = {};
        tCollection = new turmasCollection();
        if (novoPj) {
            tCollection.url = '/gerencia-salas/retorna-turma-projeto-entidade';
            parametros = {
                id_entidadecadastro: checked_ids,
                id_projetopedagogico: that.options.modelo.get('id_projetopedagogico'),
                bl_ativo: true
            }
        } else {
            tCollection.url = '/sala-de-aula/return-turma-sala-by-projeto';
            parametros = {
                id_entidadecadastro: checked_ids,
                id_projetopedagogico: that.options.modelo.get('id_projetopedagogico'),
                id_saladeaula: $('#idSalaDeAulaEdit').val(),
                bl_ativo: true
            }
        }

        tCollection.fetch({
            async: false,
            data: parametros,
            beforeSend: loading(),
            success: function () {
                _.each(tCollection.models, function (model) {
                    _.each(that.options.modelo.get('turmas'), function (elem) {
                        if (model.get('id_turma') == elem.get('id_turma')) {
                            model.set({'checked': true});
                        }
                    })
                });
                tCollection.comparator = function (turma) {
                    return turma.get('id_turma');
                };
                tCollection.sort();
            },
            complete: function () {
                var compTurma = new compositeTurma({collection: tCollection, el: '#listaTurmasDisponiveis'});
                compTurma.render();
                that.$el.modal('show');
                loaded();
            }
        });
    },
    salvarTurma: function () {
        this.options.modelo.set({
            turmas: _.filter(tCollection.models, function (model) {
                return model.get('checked') == true
            })
        });
        this.$el.modal('hide');
        tCollection = new turmasCollection({});
    },
    fechar: function () {
        this.$el.modal('hide');
        tCollection = new turmasCollection({});
    }
});

/**
 * Item view para visualizacao das turmas vinculadas
 * @type {ItemView}
 */
var ViewItemCarregaTurmaSemTurma = Marionette.ItemView.extend({
    template: '#nenhuma-turma-encontrada',
});

/**
 * Composite para renderizar a tabela dentro da modal
 */
var compositeTurma = Backbone.Marionette.CompositeView.extend({
    template: '#view-cabecalho-turmas',
    tagName: 'div',
    childView: itemViewTurmaItem,
    emptyView: ViewItemCarregaTurmaSemTurma,
    childViewContainer: 'tbody',
    ui: {
        checkboxAllTurmas: '#checkboxAllTurmas',
    },
    events: {
        'change @ui.checkboxAllTurmas': 'checkAll'
    },
    initialize: function () {
    },
    onRender: function () {
        return this;
    },
    onShow: function () {
        if (this.options.all_checked) {
            this.ui.checkboxAllTurmas.attr({checked: true});
        }
    },
    checkAll: function (el) {
        if (el.currentTarget.checked) {
            $('.checkboxTurma').prop('checked', true);
            _.each(this.collection.models, function (model) {
                model.set('checked', true);
            });
        } else {
            $('.checkboxTurma').prop('checked', false);
            _.each(this.collection.models, function (model) {
                model.set('checked', false);
            })
        }
    },
    salvarTurma: function () {
    }

});

function buscaDisciplina() { //seta o id e o nome da disciplina escolhida na modal disciplina dentro do formulário principal

    var id_disc = $('[name="idDisciplina"]:checked').val();
    var nome_disc = $('#nomeDisciplina' + id_disc).text();
//        $('#lbDisciplina').show();
    $('#st_disciplina').text(nome_disc);
    $('#id_disciplina').attr({value: id_disc});
    $('select#id_avaliacaoconjunto').html('');
    $('#pesquisaDisciplina').modal('hide');

    loading();
    setTimeout(function () {
        listaPrincipalView.renderTipoConjuntoSelectedEdit(null, id_disc);
        return false;
        loaded();
    }, 700);

    $('#id_avaliacaoconjunto').removeAttr('disabled');
    $('#id_avaliacaoconjuntoh').removeClass('hide');
    $('#id_avaliacaoconjunto').addClass('hide');
}

$("#formPesquisaDisciplina").submit(function () {
    $('#tabela-retorno-pesquisa-disciplina').html('<tr><td colspan="4"></td></tr>');
    var parametros = $('#formPesquisaDisciplina').serialize();
    loading();
    $.ajax({
        url: '/default/gerencia-salas/retorno-pesquisa-disciplina',
        type: 'post',
        dataType: 'json',
        data: parametros,
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                var label = 'idDisciplina' + i;
                this.pesquisad += '<tr>';
                this.pesquisad += '<td><input type="radio" name="idDisciplina" id="' + label + '" value="' + data[i].id_disciplina + '" /></td>';
                this.pesquisad += '<td><label id="nomeDisciplina' + data[i].id_disciplina + '" for="' + label + '">' + data[i].st_disciplina + '</label></td>';
                this.pesquisad += '<td>' + data[i].st_areaconhecimento + '</td>';
                this.pesquisad += '<td>' + data[i].st_nivelensino + '</td>';
                this.pesquisad += '<td>' + data[i].st_serie + '</td>';
                this.pesquisad += '<tr/>';
            }
            $('#tabela-retorno-pesquisa-disciplina').html(this.pesquisad);
            loaded();
        }, error: function () {
            this.pesquisad = '<tr><td colspan="5">Não há registros para essa pesquisa.</td></tr>';
            $('#tabela-retorno-pesquisa-disciplina').html(this.pesquisad);
            loaded();
        }
    });
    return false;
});

listaPrincipalView = new PrincipalView();
//listaPrincipalView.render();
//G2S.layout.content.show(new PrincipalView());

$.ajaxSetup({async: true});

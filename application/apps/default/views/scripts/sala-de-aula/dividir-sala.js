var fetch = [];

var AppLayoutView = Backbone.Marionette.LayoutView.extend({
    template: '#layout-view-template',
    el: '#container-layout-view',
    regions: {
        region1: '#region1'
    },
    onRender: function () {
        this.getRegion('region1').show(new SalaOrigemCompositeView({collection: new ProjetosPedagogicosEmComumCollectionExtend()}));
    }
});

var ProjetosEmComumModel = Backbone.Model.extend({
    attributes: {
        id_projetopedagogico: '',
        st_projetopedagogico: '',
        nu_alocados: ''
    }
});

var VwSalaDeAulaQuantitativosCollectionExtend = Backbone.Collection.extend({
    model: VwSalaDeAulaQuantitativos,
    url: '/sala-de-aula/retorna-vw-sala-de-aula-quantitativos-salas-nao-iniciadas'
});

var ProjetosPedagogicosEmComumCollectionExtend = Backbone.Collection.extend({
    model: ProjetosEmComumModel,
    url: '/projeto-pedagogico/retorna-projetos-em-comum-entre-salas'
});

var ProjetosPedagogicosEmComumItemView = Marionette.ItemView.extend({
    template: '#tpl-projetos-pedagogicos-em-comum',
    tagName: 'tr',
    ui: {
        'checkbox': 'input'
    },
    events: {
        'change input': function () {
            if (this.ui.checkbox.is(':checked')) {
                this.model.updated = true;
            } else {
                this.model.updated = false;
            }
        }
    }
});

var SelectItemView = Marionette.ItemView.extend({
    tagName: 'option',
    template: _.template(''),
    onRender: function () {
        this.$el.attr('value', this.model.get('id_saladeaula'));
        this.$el.text(this.model.get('st_saladeaula') + ' - (' + this.model.get('nu_alunos') + ') Alunos');
    }
});

var SelectCollectionViewExtend = Marionette.CollectionView.extend({
    childView: SelectItemView
});

var EmptyView = Marionette.ItemView.extend({
    tagName: 'tr',
    className: 'alert alert-info center',
    template: _.template('<td colspan="3" style="text-align: center;">Selecione a [Sala de Origem] e a [Sala de Destino] para carregar os projetos</td>')
});

var SalaOrigemCompositeView = Marionette.CompositeView.extend({
    template: '#composite-region-1',
    className: 'row-fluid',
    childView: ProjetosPedagogicosEmComumItemView,
    childViewContainer: 'tbody',
    emptyView: EmptyView,
    collectionClone: [],
    collectionSalaOrigem: [],
    collectionSalaDestino: [],
    salaOrigemSelected: {},
    salaDestinoSelected: {},
    ui: {
        comboSalaOrigem: '.combo-sala-origem',
        comboSalaDestino: '#combo-sala-destino',
        comboFormaSelecaoAlunos: '#combo-forma-selecao-alunos',
        formaSelecaoAlunosAleatoria: '#forma-selecao-alunos-aleatoria',
        formaSelecaoAlunosProjetoPedagogico: '#forma-selecao-alunos-projeto-pedagogico',
        inputQuantidadeAlunos: '#input-quantidade-alunos',
        inputFiltroProjetoPedagogico: '#input-filtro-projeto-pedagogico',
        formDividirSala: '#form-dividir-sala',
        state2: '.state2'
    },
    onShow: function () {
        this.renderDataComboSalaOrigem();
    },
    events: {
        'change .combo-sala-origem': 'appendDataComboSalaDestino',
        'change #combo-sala-destino': 'appendLoadDataGridProjetos',
        'change #combo-forma-selecao-alunos': 'alternateTemplateFormaSelecaoAlunos',
        'keyup #input-filtro-projeto-pedagogico': 'filtrarPerfis',
        'submit #form-dividir-sala': 'salvarDivisaoSalas'
    },
    alternateTemplateFormaSelecaoAlunos: function () {
        if (this.ui.comboFormaSelecaoAlunos.val() == 1) {

            this.renderFormaSelecaoAlunosProjetoPedagogico(false);

            this.ui.formaSelecaoAlunosAleatoria.hide();
            this.ui.formaSelecaoAlunosProjetoPedagogico.show();
        } else {
            this.ui.formaSelecaoAlunosAleatoria.show();
            this.ui.formaSelecaoAlunosProjetoPedagogico.hide();
        }
    },
    appendLoadDataGridProjetos: function () {
        this.controleAcoesUsuario();

        this.salaDestinoSelected = this.collectionSalaDestino.findWhere({'id_saladeaula': parseInt(this.ui.comboSalaDestino.val())});
        
        this.renderFormaSelecaoAlunosProjetoPedagogico(true);
    },
    controleAcoesUsuario: function(){
        if (this.ui.comboSalaDestino.val()) {
            this.ui.state2.find('input,select').removeAttr('disabled');
            this.ui.formDividirSala.find('button').removeAttr('disabled');
        } else {
            this.ui.state2.find('input,select').attr('disabled', true);
            this.ui.formDividirSala.find('button').attr('disabled', true);
        }
    },
    renderDataComboSalaOrigem: function () {
        that = this;

        this.collectionSalaOrigem = new VwSalaDeAulaQuantitativosCollectionExtend();

        this.ui.comboSalaOrigem.select2({
            width: '100%',
            placeholder: 'Selecione e/ou busque mais salas',
            cloneCollection: [],
            allowClear: true,
            query: function (query) {

                if (query.term.length >= 3 || query.term.length == '' && !that.collectionSalaOrigem.length) {

                    if (fetch.readyState) {
                        fetch.abort();
                    }

                    fetch = that.collectionSalaOrigem.fetch({
                        url: '/sala-de-aula/retorna-vw-sala-de-aula-quantitativos-salas-nao-iniciadas',
                        data: {
                            q: query.term,
                            id_entidade: G2S.loggedUser.get('id_entidade'),
                            bl_ativa: 1,
                            limit: 50
                        },
                        complete: function () {
                            var data = [];

                            that.collectionSalaOrigem.each(function (model) {
                                data.push({
                                    text: model.get('st_saladeaula') + ' - (' + model.get('nu_alunos') + ') Alunos',
                                    id: model.get('id_saladeaula')
                                });
                            });

                            query.callback({results: data});
                        }
                    });
                } else {
                    var data = [];

                    that.collectionSalaOrigem.each(function (model) {
                        data.push({
                            text: model.get('st_saladeaula') + ' - (' + model.get('nu_alunos') + ') Alunos',
                            id: model.get('id_saladeaula')
                        });
                    });

                    query.callback({results: data});
                }
            }
        });

    },
    appendDataComboSalaDestino: function () {
        var that = this;

        this.salaOrigemSelected = this.collectionSalaOrigem.findWhere({'id_saladeaula': parseInt(this.ui.comboSalaOrigem.val())});
        
        //resetando dados da grid de projetos e verificando situacao para acoes
        this.collection.reset();
        this.controleAcoesUsuario();
        
        //reset dados combo sala de destino
        this.collectionSalaDestino = new VwSalaDeAulaQuantitativosCollectionExtend();
        that.ui.comboSalaDestino.empty();
        that.ui.comboSalaDestino.append('<option value="">--Selecione--</option>');

        if (this.salaOrigemSelected) {

            this.ui.inputQuantidadeAlunos.attr('max', this.salaOrigemSelected.get('nu_alunos'));

            this.collectionSalaDestino.fetch({
                beforeSend: function () {
                    loading();
                },
                data: {
                    id_entidade: G2S.loggedUser.get('id_entidade'),
                    id_disciplina: that.salaOrigemSelected.get('id_disciplina'),
                    bl_ativa: 1
                },
                complete: function () {

                    that.ui.comboSalaDestino.empty();
                    that.ui.comboSalaDestino.append('<option value="">--Selecione--</option>');

                    (new SelectCollectionViewExtend({
                        el: that.ui.comboSalaDestino,
                        collection: that.collectionSalaDestino
                    })).render();

                    //Desmarcando para usuario nao selecionar a mesma sala no combo de destino
                    that.ui.comboSalaDestino.find('option').removeAttr('disabled');
                    that.ui.comboSalaDestino.find('option[value="' + that.salaOrigemSelected.get('id_saladeaula') + '"]').attr('disabled', true);

                    loaded();
                }
            });
        }

        this.ui.comboSalaDestino.select2({width: '100%'});
    },
    renderFormaSelecaoAlunosProjetoPedagogico: function (reset) {
        var that = this;

        if (reset && that.ui.comboSalaOrigem.val() && that.ui.comboSalaDestino.val()) {
            this.collection.fetch({
                data: {
                    ids_saladeaula: [that.ui.comboSalaOrigem.val(), that.ui.comboSalaDestino.val()],
                    id_entidade: G2S.loggedUser.get('id_entidade')
                },
                beforeSend: function () {
                    loading();
                },
                complete: function () {

                    //copiando collection para usar no filtro uma lista que contem os dados
                    //originais nao perdendo na hora que o filtro for aplicado.
                    that.collectionClone = that.collection.clone();

                    loaded();
                }
            });
        }
    },
    filtrarPerfis: function () {
        var inputVal = this.ui.inputFiltroProjetoPedagogico.val();

        if (inputVal.length >= 3 || inputVal.length == 0) {

            //Filtrando em colecao que se mantem intacta collectionClone
            var newCollection = this.collectionClone.filter(function (child, index, collection) {
                return child.get('st_projetopedagogico').toLowerCase().indexOf(inputVal) != -1;
            });

            //resetando a collecao de perfis somente com perfis encontrados nos filtros.
            this.collection.set(newCollection);
        }
    },
    salvarDivisaoSalas: function (e) {
        e.preventDefault();

        var that = this;

        if (this.ui.comboFormaSelecaoAlunos.val() == 1) {

            if (!that.ui.comboSalaOrigem.val() || !that.ui.comboSalaDestino.val()){
                $.pnotify({title: 'Alerta', text: MensagensG2.MSG_DIVIDIR_SALA_03, type: 'warning'});
                return false;
            }

            var data = [];

            this.collection.each(function (model, index) {
                if (model.updated) {
                    data.push(model.toJSON());
                }
            });

            if (data.length) {
                $.ajax({
                    url: '/sala-de-aula/salvar-divisao-sala',
                    data: {
                        tipoDivisao: 1,
                        projetos: data,
                        id_saladeaulaorigem: that.ui.comboSalaOrigem.val(),
                        id_saladeauladestino: that.ui.comboSalaDestino.val()
                    },
                    beforeSend: function(){
                        loading();
                    },
                    success: function (response) {

                        //calculando quantidade de alunos disponiveis em sala de aula apos
                        //operacao de divisao de sala
                        var sub = that.salaOrigemSelected.get('nu_alunos') - response.length;
                        var sum = parseInt(that.salaDestinoSelected.get('nu_alunos')) + parseInt(response.length);

                        //Atualizando na opcao escolhida no combo de sala de origem nova
                        //quantidade de alunos disponiveis na sala apos atualizacao
                        that.ui.comboSalaOrigem.select2('data', {
                            id: that.salaOrigemSelected.get('id_saladeaula'),
                            text: that.salaOrigemSelected.get('st_saladeaula') + ' - (' + sub + ') Alunos'
                        });
                        
                        //Atualizando na opcao escolhida no combo de sala de destino nova
                        //quantidade de alunos alocados na sala apos atualizacao
                        that.ui.comboSalaDestino.select2('data', {
                            id: that.salaDestinoSelected.get('id_saladeaula'),
                            text: that.salaDestinoSelected.get('st_saladeaula') + ' - (' + sum + ') Alunos'
                        });
                        
                        that.salaDestinoSelected.set('nu_alunos', sum);
                        that.salaOrigemSelected.set('nu_alunos', sub);

                        that.appendLoadDataGridProjetos();

                        $.pnotify({title: 'Sucesso', text: MensagensG2.MSG_DIVIDIR_SALA_02, type: 'success'});
                        loaded();
                    }
                });
            } else {
                $.pnotify({title: 'Alerta', text: MensagensG2.MSG_DIVIDIR_SALA_01, type: 'warning'});
            }

        } else {

            if (!that.ui.comboSalaOrigem.val() || !that.ui.comboSalaDestino.val()){
                $.pnotify({title: 'Alerta', text: MensagensG2.MSG_DIVIDIR_SALA_03, type: 'warning'});
                return false;
            }

            if (that.ui.inputQuantidadeAlunos.val()) {

                $.ajax({
                    url: '/sala-de-aula/salvar-divisao-sala',
                    data: {
                        tipoDivisao: 1,
                        qtd_alunos: that.ui.inputQuantidadeAlunos.val(),
                        id_saladeaulaorigem: that.ui.comboSalaOrigem.val(),
                        id_saladeauladestino: that.ui.comboSalaDestino.val()
                    },
                    beforeSend: function () {
                        loading();
                    },
                    success: function (response) {

                        //calculando quantidade de alunos disponiveis em sala de aula apos
                        //operacao de divisao de sala
                        var sub = that.salaOrigemSelected.get('nu_alunos') - that.ui.inputQuantidadeAlunos.val();
                        var sum = parseInt(that.salaDestinoSelected.get('nu_alunos')) + parseInt(that.ui.inputQuantidadeAlunos.val());

                        //Atualizando na opcao escolhida no combo de sala de origem nova
                        //quantidade de alunos disponiveis na sala apos atualizacao
                        that.ui.comboSalaOrigem.select2('data', {
                            id: that.salaOrigemSelected.get('id_saladeaula'),
                            text: that.salaOrigemSelected.get('st_saladeaula') + ' - (' + sub + ') Alunos'
                        });
                        
                        //Atualizando na opcao escolhida no combo de sala de destino nova
                        //quantidade de alunos alocados na sala apos atualizacao
                        that.ui.comboSalaDestino.select2('data', {
                            id: that.salaDestinoSelected.get('id_saladeaula'),
                            text: that.salaDestinoSelected.get('st_saladeaula') + ' - (' + sum + ') Alunos'
                        });

                        //Atualizando valor maximo a ser inserido no campo de quantidade de alunos
                        //apos executar operacao de divisao de salas.
                        that.ui.inputQuantidadeAlunos.attr('max', sub);

                        that.salaDestinoSelected.set('nu_alunos', sum);
                        that.salaOrigemSelected.set('nu_alunos', sub);
                        
                        that.appendLoadDataGridProjetos();

                        $.pnotify({title: 'Sucesso', text: MensagensG2.MSG_DIVIDIR_SALA_02, type: 'success'});

                        loaded();
                    }
                });
            } else {
                $.pnotify({title: 'Alerta', text: MensagensG2.MSG_DIVIDIR_SALA_04, type: 'warning'});
            }
        }
    }
});


//G2S.show(new AppLayoutView());
var layout = new AppLayoutView();
layout.render();
//layout.onRender();
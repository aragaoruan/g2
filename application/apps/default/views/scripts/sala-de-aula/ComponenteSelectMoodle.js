/**
 * Componente para renderizar os dados da entidade de integração no select
 *
 * @type {{
 * data: {bl_ativo: boolean, id_situacao: number, id_entidade: *, id_sistema: number},
 * $el: null,
 * optionSelected: null, success:
 * ComponenteSelectMoodle.success,
 * render: ComponenteSelectMoodle.render,
 * montaSelect: ComponenteSelectMoodle.montaSelect
 * }}
 */
var ComponenteSelectMoodle = {
    data: {
        "bl_ativo": true,
        "id_situacao": SITUACAO.TB_ENTIDADEINTEGRACAO.ATIVA,
        "id_entidade": G2S.loggedUser.get("id_entidade"),
        "id_sistema": SISTEMAS.MOODLE
    },
    $el: null,
    optionSelected: null,
    success: function (callback) {
        if (callback && typeof (callback) === "function") {
            callback();
        }
        return this;
    },
    render: function (options) {
        if (typeof options.el === "undefined") {
            throw "Elemento onde será renderizado os dados do select não definido.";
        }

        if (typeof options.data !== "undefined") {
            $.extend(this.data, options.data);
        }
        if (typeof options.selected !== "undefined") {
            this.optionSelected = options.selected;
        }

        this.$el = options.el;
        this.montaSelect();
        return this;
    },
    montaSelect: function () {
        var EntidadeIntegracaoCollection = Backbone.Collection.extend({
            model: EntidadeIntegracao,
            url: "/api/entidade-integracao"
        });
        (new EntidadeIntegracaoCollection())
            .fetch({
                data: this.data,
                beforeSend: function () {
                    loading();
                    this.$el.empty().html("<option value=''>Selecione</option>");
                }.bind(this),
                complete: loaded,
                error: function (collection, response) {
                    $.pnotify(response.responseJSON);
                }.bind(this),
                success: function (collection) {
                    //Seleciona automaticamente o valor do combo se tiver apenas um registro.
                    if (collection.length == 1 && !this.optionSelected) {
                        this.optionSelected = collection.at(0).get("id_entidadeintegracao");
                    }
                    (new SelectView({
                        el: this.$el,
                        collection: collection,
                        sort: "st_titulo",
                        childViewOptions: {
                            optionLabel: "st_titulo",
                            optionValue: "id_entidadeintegracao",
                            optionSelected: this.optionSelected
                        }
                    })).render();
                    this.success();
                }.bind(this)
            });
    }
};

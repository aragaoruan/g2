/**
 * Created by vinicius.alcantara on 12/05/16.
 */

/**
 ******************************************************************************************
 ******************************************************************************************
 ********* GLOBAIS ************************************************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var editable = true;
var collectionVendaCartasCredito;
var collectionProdutoVenda;
var collectionFormaVwMeioDivisao;
var collectionResponsavelFinanceiro;
var valoresModel;
var nu_valorbruto = 0;
var nu_valorliquido = 0;
var thatViewFormaPagamento;
var thatViewGridProdutos;
var pessoaFixada;
var atendentesCollection;
var contratoRegraVenda;
var thatAutoComplete;
var campanhaPontualidadeCollection;
var alteracoesdeVenda = [];
var descontop = 0;

/**
 ******************************************************************************************
 ******************************************************************************************
 ********* MODELS *************************************************************************
 ******************************************************************************************
 ******************************************************************************************
 */

var VwProdutoVendaModel = VwProdutoVenda.extend({
    idAttribute: 'id_vendaproduto'
});

/**
 * Instancia a model de venda e adiciona um atributo fake
 * @type {Venda}
 */
var VendaModel = Venda.extend({
    initialize: function () {
        this.defaults.st_atendente;
        return this;
    },
    idAttribute: 'id_venda'
});

/**
 * Model para representar a SpCampanhaVendaProduto
 * @type {Backbone.Model}
 */
var SpCampanhaVendaProduto = Backbone.Model.extend({
    defaults: {
        id_campanhacomercial: '',
        id_entidade: '',
        id_situacao: '',
        st_situacao: '',
        nu_disponibilidade: '',
        st_campanhacomercial: '',
        st_descricao: '',
        dt_inicio: '',
        dt_fim: '',
        bl_todasformas: '',
        bl_todosprodutos: '',
        id_tipocampanha: '',
        st_tipocampanha: '',
        id_produto: '',
        id_venda: '',
        nu_descontominimo: '',
        nu_descontomaximo: '',
        id_tipodesconto: '',
        nu_valordesconto: ''
    },
    idAttribute: 'id_campanhacomercial'
});

/**
 * Modelo para representar NucleoFuncionariotm
 * @type {Backbone.Model}
 */
var NucleoFuncionariotm = Backbone.Model.extend({
    defaults: {
        'id_nucleofuncionariotm': '',
        'id_funcao': '',
        'st_funcao': '',
        'id_usuario': '',
        'st_nomecompleto': '',
        'id_nucleotm': '',
        'dt_cadastro': '',
        'bl_ativo': '',
        'id_usuariocadastro': '',
        'id_situacao': '',
        'st_situacao': '',
        'st_nucleotm': ''
    },
    url: '/venda/retornar-funcionario-nucleo'
});


/**
 * Model para template de Valores
 * @type {Backbone.Model}
 */
var ValoresModel = Backbone.Model.extend({
    defaults: {
        nu_valorbruto: 0,
        nu_valorliquido: 0,
        nu_valordesconto: 0,
        nu_percentualdesconto: 0,
        nu_descontocarta: 0
    }
});

/**
 * Model Backbone local para formas de pagamento selecionada
 * @type {Backbone.Model}
 */
var FormaPagamentoSelectedModel = Backbone.Model.extend({
    defaults: {
        id_atendente: '',
        st_nomeatendente: '',
        id_contratoregra: '',
        st_contratoregra: '',
        id_campanhapontualidade: '',
        st_campanhapontualidade: ''
    }
});

/**
 * Módel para representar Responsaveis Finanaceiro
 * @type {Backbone.Model}
 */
var ResponsavelFinanceiroModel = Backbone.Model.extend({
    defaults: {
        id: '',
        st_cpf: '',
        st_nomecompleto: '',
        st_cnpj: '',
        st_tiporesponsavel: 'Fisica'
    },
    idAttribute: 'id_usuario'
});

/**
 * Model para layout de produto
 * @type {VwProduto}
 *
 */
var ProdutoVendaModel = VwProduto.extend();

/**
 ******************************************************************************************
 ******************************************************************************************
 ********* COLLECTIONS ********************************************************************
 ******************************************************************************************
 ******************************************************************************************
 */

/**
 * Collection para campanha de pontualidade
 */
var CampanhaPontualidadeCollection = Backbone.Collection.extend({
    model: CampanhaComercial,
    url: '/api/campanha-comercial/?',
    data: {
        id_tipocampanha: null
    }
});

var VendaCartaCreditoCollection = Backbone.Collection.extend({
    model: VendaCartaCredito,
    url: '/api/venda-carta-credito'
});

/**
 * Collection para Regra de Contrato
 * @type @exp;Backbone@pro;Collection@call;extend
 */
var ContratoRegraCollection = Backbone.Collection.extend({
    model: ContratoRegra,
    url: '/api/contrato-regra'
});

/**
 * Collection para Atendente
 * @type @exp;Backbone@pro;Collection@call;extend
 */
var NucleoFuncionariotmCollection = Backbone.Collection.extend({
    model: NucleoFuncionariotm,
    url: '/venda/retornar-funcionario-nucleo'
});

/**
 * Collection para Responsavel Financeiro
 * @type @exp;Backbone@pro;Collection@call;extend
 */
var ResponsavelFinanceiroCollection = Backbone.Collection.extend({
    model: ResponsavelFinanceiroModel
});

/**
 * Colelction Produto venda
 * @type @exp;Backbone@pro;Collection@call;extend
 */
var VwProdutoVendaCollection = Backbone.Collection.extend({
    model: VwProdutoVendaModel,
    url: '/api/vw-produto-venda'
});

/**
 * Collection para SP
 * @type {Backbone.Collection}
 */
var SpCampanhaVendaProdutoCollection = Backbone.Collection.extend({
    model: SpCampanhaVendaProduto,
    url: '/venda/retorna-sp-campanha-venda-produto'
});

/**
 * Collection para Layout de produto
 * @type {Backbone.Collection}
 */
var ProdutoVendaCollection = Backbone.Collection.extend({
    model: ProdutoVendaModel
});

/**
 *Collection para VwProduto
 * @type {Backbone.Collection}
 */
var VwProdutoCollection = Backbone.Collection.extend({
    model: VwProduto,
    url: 'produto/pesquisar-produto'
});


/**
 * Collection para vw_formameiodivisao
 * @type {Backbone.Collection}
 */
var VwFormaMeioDivisaoCollection = Backbone.Collection.extend({
    model: VwFormaMeioDivisao.extend({
        idAttribute: 'id_meiopagamento'
    }),
    url: '/api/vw-forma-meio-divisao'
});

/**
 * Collection para SpTramite
 * @type @exp;Backbone@pro;Collection@call;extend
 */
var SpTramiteCollection = Backbone.Collection.extend({
    model: SpTramite,
    url: '/api/sp-tramite'
});

/**
 ******************************************************************************************
 ******************************************************************************************
 ********* INSTANCIAS PARA COLLECTIONS QUE BUSCA COISAS QUE SÃO COMUNS ********************
 ******************************************************************************************
 ******************************************************************************************
 */


//Instancia a collection para os atendentes e faz o fetch
atendentesCollection = new NucleoFuncionariotmCollection();
atendentesCollection.fetch();


// Instancia a collection e faz o fetch

//Instancia da collection de spCampanhaVendaProduto
var spCampanhaVendaProdutoCollection = new SpCampanhaVendaProdutoCollection();

//Intancia a model de venda
var vendaModel = new VendaModel();

// Instancia a collection para responsavel financeiros
collectionResponsavelFinanceiro = new ResponsavelFinanceiroCollection();

// Instancia a collection para layout de produto venda
collectionProdutoVenda = new VwProdutoVendaCollection();// new ProdutoVendaCollection();

collectionVendaCartasCredito = new VendaCartaCreditoCollection();

// Instancia a collection para a regra de contrato
var contratoRegraModel = new ContratoRegra();

// Model para Layout de valores
valoresModel = new ValoresModel();

// Instancia a collection de forma e meio divisao
collectionFormaVwMeioDivisao = new VwFormaMeioDivisaoCollection();

campanhaPontualidadeCollection = new CampanhaPontualidadeCollection();

/**
 * Instancia a modelo temporaria da forma de pagamento selecionada
 * @type FormaPagamentoSelectedModel|FormaPagamentoSelectedModel
 */
var modelFormaPagamentoSelected = new FormaPagamentoSelectedModel();

/**
 * Instancia a collection de contrato regra e faz o fetch
 * @type ContratoRegraCollection
 */
var contratoRegraCollection = new ContratoRegraCollection();
contratoRegraCollection.fetch();


/**
 ******************************************************************************************
 ******************************************************************************************
 ********* LAYOUT FUNCIONALIDADE **********************************************************
 ******************************************************************************************
 ******************************************************************************************
 */

/**
 * LAYOUT
 * @type {Marionette.Layout}
 */
var LayoutAlterarVenda = Marionette.LayoutView.extend({
    template: '#layout-alterar-venda',
    tagName: 'div',
    className: 'container-fluid',
    autoComplete: componenteAutoComplete,
    printContrato: false,
    regions: {
        produtos: '#box-produtos',
        tramites: '#box-tramite',
        vendas: '#box-venda',
        valores: '#box-valores',
        adicionarPessoaRegion: '#container-adicionar-pessoa'
    },
    initialize: function () {
        this.autoComplete.clearFixModel();
    },
    onShow: function () {
        //Cria uma variavel no ecopo local para armazenar o this
        var that = this;
        loading();
        //armazena o autocomplete
        var autocomplete = this.autoComplete;
        thatAutoComplete = this.autoComplete;

        //Verifica se o G2S.editModel esta preenchido, isso significa que estou editando ou já salvei a venda
        if (G2S.editModel) {
            //Seta os dados da venda
            vendaModel.set(G2S.editModel);
            that.buscaDados();//busca os dados complementares
        } else {// G2S.editModel não encontrado
            //Chama o componente de autocomplete
            autocomplete.main({
                model: VwPesquisarPessoa,
                element: $("#autoComplete")
            }, function () {
                that.setResponsavel(autocomplete.getFixModel());
                pessoaFixada = autocomplete.getFixModel();
                abreNegociacao();
                //Oculta o botão de incluir nova pessoa
                that.$el.find('#btn-adicionar-pessoa').css({
                    'display': 'none'
                });
            }, function () {
                fechaNegociacao();
            });

            if (!autocomplete.getFixModel()) {
                $('.content-box-vendas').hide('fast');
            }
            //Exibe o botão de incluir nova pessoa caso não esteja editando
            that.$el.find('#btn-adicionar-pessoa').css({
                'display': 'inline'
            });
        }
        loaded();
        return this;
    },
    buscaDados: function () {
        var that = this;
        var autocomplete = this.autoComplete;

        //Limpa a div do autocomplete para não escrever o componente 2 vezes
        $("#autoComplete").empty();

        //Busca a pessoa
        var vwPessoa = new VwPesquisarPessoa();
        vwPessoa.url = '/pessoa/get-vw-pesquisar-pessoa-by-id/id/' + vendaModel.get('id_usuario');
        vwPessoa.fetch({
            beforeSend: function () {
                loading();
            },
            complete: function () {
                loaded();
            },
            error: function (model, response) {
                loaded();
                $.pnotify({
                    type: 'error',
                    title: 'Erro!',
                    text: 'Houve um erro ao consultar dados da pessoa. ' + response.responseText
                });
                fechaNegociacao();
            },
            success: function (model) {
                //Transforma o resultado do fetch em objeto Json
                var pessoaFix = vwPessoa.toJSON();
                pessoaFix.id = pessoaFix.id_usuario; //Cria um novo atributo no objeto

                //Seta as urls das collections que tenho que carregar os dados
                collectionProdutoVenda.url = '/api/vw-produto-venda/id_venda/' + vendaModel.get('id_venda');

                //Fetch na collection de ProdutoVenda
                collectionProdutoVenda.fetch({
                    async: false,
                    complete: function () {

                        //Instancia a model de vwVendaUsuario
                        var vendaUsuario = new VwVendaUsuario();
                        vendaUsuario.id = vendaModel.get('id_venda');//Atribui o id_venda como id de vendaUsuario

                        //Faz o fetch de vendaUsuario
                        vendaUsuario.fetch({
                            async: false,
                            success: function () {
                                //Limpa o auto complete
                                autocomplete.clearFixModel();
                                autocomplete.modelRender = pessoaFix; //seta no modelRenderer do autocomplete
                                autocomplete.setFixModel();//e define o registro fixado
                                pessoaFixada = autocomplete.getFixModel();//recupera os dados da pessoa fixada

                                //Cria o AutoComplete
                                autocomplete.main({
                                    model: VwPesquisarPessoa,
                                    element: $("#autoComplete")
                                }, abreNegociacao);
                                $("#btnClearAutocomplete").attr('disabled', 'disabled');
                                $("#btn-adicionar-pessoa").attr('disabled', 'disabled');
                                //AutoComplete, recupera dados para setar pessoa como responsavel

                                that.setResponsavel(pessoaFixada);
                                that.showTramite();
                            },
                            complete: function () {
                                //Seta os valores do modelo no modelo que definine a forma de pagamento selecionada

                                calculaValorBruto();
                                modelFormaPagamentoSelected.set({
                                    id_evolucao: vendaUsuario.get('id_evolucao'),
                                    st_evolucao: vendaUsuario.get('st_evolucao'),
                                    id_situacao: vendaUsuario.get('id_situacao'),
                                    st_situacao: vendaUsuario.get('st_situacao'),
                                    id_atendente: vendaUsuario.get('id_atendente')
                                });
                            }
                        });

                    }
                });

            }
        });

    },
    showTramite: function () {
        var spTramiteCollection = new SpTramiteCollection();
        if (vendaModel.get('id_venda')) {
            spTramiteCollection.url += '?id_categoriarramite=3&chave[]=' + vendaModel.get('id_venda');
            spTramiteCollection.fetch({
                beforeSend: function () {
                    loading();
                },
                complete: function () {
                    loaded();
                },
                success: function () {
                    var tramiteView = new TramitesCompositeView({
                        collection: spTramiteCollection
                    });
                    telaAlterarVendas.tramites.show(tramiteView);
                }
            });
        }
    },
    setResponsavel: function (pessoa) {
        //Instancia a model de responsavel financeiro e seta os valores
        var modeloResponsavel = new ResponsavelFinanceiroModel({
            id: pessoa.id,
            st_cpf: pessoa.st_cpf,
            st_nomecompleto: pessoa.st_nomecompleto
        });
        //atribui a model na collection de responsaveis financeiros
        collectionResponsavelFinanceiro.add(modeloResponsavel);
    },
    verificaContratoRegra: function () {
        var id_contratoregra = modelFormaPagamentoSelected.get('id_contratoregra');
        if (id_contratoregra) {
            var contratoRegraModel = contratoRegraCollection.findWhere({id_contratoregra: parseInt(id_contratoregra)});
            if ((typeof  contratoRegraModel != 'undefined') && contratoRegraModel.get('id_tiporegracontrato') && contratoRegraModel.get('id_tiporegracontrato').id_tiporegracontrato == 2) {
                if (collectionProdutoVenda.length > 1) {
                    $.pnotify({
                        type: 'warning',
                        title: 'Atençao!',
                        text: 'Para contratos do tipo Free Pass, somente um produto é aceito.'
                    });
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        } else {
            $.pnotify({
                type: 'warning',
                title: 'Atençao!',
                text: 'Atenção, nenhuma regra de contrato foi selecionada. Favor entrar em contato com o administrador.'
            });
            return true;
        }
    },
    validaDados: function () {
        var id_atendente = modelFormaPagamentoSelected.get('id_atendente');
        var valid = true;
        if (id_atendente == null) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Por favor, escolha uma atendente e tente de novo!'
            });
            valid = false
        }

        return valid;
    },
    alterarVenda: function () {
        if (this.validaDados() && this.verificaContratoRegra()) {
            var params = {
                'id_venda': vendaModel.get('id_venda'),
                'id_campanhapontualidade': modelFormaPagamentoSelected.get('id_campanhapontualidade') ? modelFormaPagamentoSelected.get('id_campanhapontualidade') : null,
                'id_atendente': modelFormaPagamentoSelected.get('id_atendente'),
                'id_contratoregra': modelFormaPagamentoSelected.get('id_contratoregra'),
                'id_campanhacomercial': collectionProdutoVenda.models[0].get('id_campanhacomercial'),
                'vendaProduto': collectionProdutoVenda.toJSON(),
                'alteracoesVenda': alteracoesdeVenda,
                'venda': vendaModel.toJSON()
            };
            var that = this;
            $.ajax({
                type: 'post',
                url: '/alterar-venda/alterar-venda',
                data: params,
                beforeSend: function () {
                    loading();
                },
                success: function (data) {
                    $.pnotify({
                        type: data.type,
                        title: data.title,
                        text: data.text
                    });
                    alteracoesdeVenda = [];

                    _.each(collectionProdutoVenda.models, function (model, i) {
                        model.set('id_campanhacomercialoriginal', model.get('id_campanhacomercial'));
                    });

                    that.showTramite();
                },
                complete: function () {
                    loaded();
                },
                error: function (data) {
                    $.pnotify({
                        type: 'error',
                        title: 'Erro!',
                        text: data.text
                    });
                }
            });
        } else {

            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Preencha corretamente os campos obrigatórios e tente de novo!'
            });
        }
    },
    adicionarPessoa: function () {
        $("#container-adicionar-pessoa").css({
            'display': 'inline',
            'border': '1px solid #767676',
            'margin-top': '20px',
            'margin-right': '0px'
        });
        $("#btn-ocultar-form").css({
            'display': 'inline'
        });

        //$('#container-adicionar-pessoa').html(viewFormulario.template);

        var viewFormulario = new ViewFormulario({
            model: modelFormulario,
            el: '#container-adicionar-pessoa'
        });
        viewFormulario.render();
    },
    ocultarFormulario: function () {
        $('#container-adicionar-pessoa').empty();
        $("#container-adicionar-pessoa").css({
            'display': 'none'
        });
        $("#btn-ocultar-form").css({
            'display': 'none'
        });
    },
    events: {
        'click #btn-salvar': 'alterarVenda',
        'click #btn-adicionar-pessoa': 'adicionarPessoa',
        'click #btn-ocultar-form': 'ocultarFormulario'
    }
});

function calculaValorBruto() {
    collectionVendaCartasCredito.url = '/api/venda-carta-credito?id_venda=' + vendaModel.get('id_venda');
    //percorre a collection de produtos selecionados para venda e soma os valores dos produtos definindo o valor bruto
    var nu_somavalor = 0;
    var nu_somadesconto = 0;
    collectionProdutoVenda.each(function (produto) {
        var nu_valor = parseFloat(produto.get('nu_valorbruto'));
        var nu_valorvenda = parseFloat(produto.get('nu_valorliquido'));

        if (nu_valorvenda != nu_valor) {
            nu_somadesconto += nu_valor - nu_valorvenda;
        }
        nu_somavalor += nu_valor;

    });
    //pega o valor da carta
    var nu_valordescontocarta = 0;
    collectionVendaCartasCredito.each(function (carta) {
        nu_valordescontocarta += parseFloat(carta.get('nu_valorutilizado'));
    });

    //calcula a porcentagem de desconto
    var nu_porcentagemdesconto = (nu_somadesconto && nu_somavalor) ? (nu_somadesconto / nu_somavalor) * 100 : 0;
    //calcula o valor liquido
    var nu_valorliquido = nu_somavalor - nu_somadesconto - nu_valordescontocarta;

    //seta os valores no modelo
    valoresModel.set({
        'nu_valorbruto': nu_somavalor.toFixed(2),
        'nu_valordesconto': nu_somadesconto.toFixed(2),
        'nu_percentualdesconto': nu_porcentagemdesconto.toFixed(2),
        'nu_valorliquido': nu_valorliquido.toFixed(2),
        'nu_descontocarta': nu_valordescontocarta.toFixed(2)
    });
    //Abre e configura view de valores
    var viewValores = new ValoresItemView({
        model: valoresModel
    });
    telaAlterarVendas.valores.show(viewValores);//show view de valores
}

/**
 * ItemView para Select de campanha
 * @type {ItemView}
 */
var ItemCampanhaProdutoView = Marionette.ItemView.extend({
    template: _.template('<%=st_campanhacomercial%>'),
    tagName: 'option',
    onRender: function () {
        this.$el.attr('value', this.model.get('id_campanhacomercial'));
        return this;
    }
});

/**
 * CollectionView para Select de Campanha
 * @type {CollectionView}
 */
var CampanhaProdutoView = Marionette.CollectionView.extend({
    tagName: 'select',
    childView: ItemCampanhaProdutoView,
    onRender: function () {
        return this;
    }
});


/**
 ******************************************************************************************
 ******************************************************************************************
 ********* PRODUTOS ***********************************************************************
 ******************************************************************************************
 ******************************************************************************************
 */

/**
 * ItemView para Composite de Lista de produtos
 * @type {ItemView}
 */
var ViewItemProdutos = Marionette.ItemView.extend({
    template: '#template-row-produto',
    tagName: 'tr',
    modelEvents: {
        'change:id_campanhacomercial': function () {
            var that = this;
            if (!this.model.get('id_campanhacomercial')) {
                this.model.set('id_campanhacomercial', that.ui.idCampanha.val());
                this.calculaValorPorcentagem();
            }
        }
    },
    ui: {
        idCampanha: 'select[name="campanha"]',
        idTurma: '[name="id_turma"]',
        porcentagemDesconto: 'input[name="desconto_percent"]'
    },
    selectDescontosOptions: new Array(),
    nu_descontomaximo: null,
    id_tipodesconto: null,
    initialize: function () {
        var nu_valor = parseFloat(this.model.get('nu_valorliquido'));
        this.model.set('nu_valorliquido', nu_valor.toFixed(2));

        var valorDesconto = this.model.get('nu_descontovalor');
        this.model.set('nu_descontovalor', formatValueToShow(valorDesconto));

        var porcentagemDesconto = this.model.get('nu_descontoporcentagem') ? parseFloat(this.model.get('nu_descontoporcentagem')) : 0;
        this.model.set('nu_descontoporcentagem', formatValueToShow(porcentagemDesconto.toFixed(2)));
        return this;
    },
    onRender: function () {
        var that = this;
        var elem = this.$el;
        if (parseFloat(this.model.get("nu_descontoporcentagem")) == 100) {
            this.ui.idCampanha.attr('disabled', 'disabled');
        }
        // Montar SELECT "Turma"
        var Model = Backbone.Model.extend({
            idAttribute: 'id_turma',
            defaults: {
                st_turma: ''
            }
        });

        var idTurmaElem = elem.find(this.ui.idTurma);
        ComboboxView({
            el: idTurmaElem,
            model: Model,
            //url: '/turma/get-turmas-by-produto/?id_produto=' + (that.model.get('id_produto')),
            url: '/turma/retorna-turma-projeto?id=' + that.model.get('id_turma'),
            optionLabel: 'st_turma'
        });

        if (this.model.get('id_campanhacomercial')) {
            this.ui.idCampanha.val(this.model.get('id_campanhacomercial'));
            this.model.set('id_campanhacomercialoriginal', this.model.get('id_campanhacomercial'));
            this.toogleDescontos(this.model.get('id_campanhacomercial'));
        }
        return this;
    },
    calculaValorPorcentagem: function () {
        //recupera os valores
        var nu_valor = formatValueToReal(this.model.get('nu_valorbruto'));//valor do produto

        //valor % desconto
        var nu_porcentagemdesconto = this.ui.porcentagemDesconto.val() ? formatValueToReal(this.ui.porcentagemDesconto.val()) : 0;

        if (nu_porcentagemdesconto > parseFloat(this.nu_descontomaximo)) {
            $.pnotify({
                title: 'Atenção!',
                text: 'O valor de desconto para este produto excede o valor máximo permitido.',
                type: 'warning'
            });
            this.ui.porcentagemDesconto.val(this.model.get('nu_descontoporcentagem'));
            return false;
        }


        //Campanha
        var id_campanha = this.ui.idCampanha.val();

        //calcula o desconto
        var valordesconto = nu_valor * (nu_porcentagemdesconto / 100);
        var nu_valorvenda = nu_valor - valordesconto;

        //Seta os valores na model
        this.model.set('nu_valorliquido', nu_valorvenda.toFixed(2));//seta o desconto no model
        this.model.set('nu_descontoporcentagem', nu_porcentagemdesconto.toFixed(2));
        this.model.set('nu_descontovalor', valordesconto.toFixed(2));

        this.render();//renderiza novamente

        //monta o select de campanha
        this.montaSelectCampanha(id_campanha);


        //escreve os valores nos inputs
        this.ui.porcentagemDesconto.removeAttr('disabled').val(formatValueToShow(nu_porcentagemdesconto.toFixed(2)));
        calculaValorBruto();

    },
    montaSelectCampanha: function (id_campanha) {
        //monta a string com os options do select
        var optStr = '';
        $.each(this.selectDescontosOptions, function (i, obj) {
            optStr += '<option value="' + obj.value + '">' + obj.text + '</option>';
        });
        //monta o select
        this.ui.idCampanha.empty().html(optStr).val(id_campanha);
    },
    toogleDescontos: function (campanha) {
        var that = this;
        var id_campanha = typeof (campanha) != 'object' && campanha ? campanha : this.ui.idCampanha.val();
        if (id_campanha && parseFloat(this.model.get("nu_descontoporcentagem")) != 100) {
            //pesquisa na sp
            var descontos = spCampanhaVendaProdutoCollection.findWhere({id_campanhacomercial: parseInt(id_campanha)});
            if (descontos) {
                this.nu_descontomaximo = descontos.get('nu_descontaximo');
                this.id_tipodesconto = descontos.get('id_tipodesconto');
            }
            if (id_campanha != vendaModel.get('id_campanha'))
                if (id_campanha != that.model.get('id_campanha')) {
                    var campanhaalteracao = true;
                    _.each(alteracoesdeVenda, function (elem, index) {
                        if (descontos && elem['id_vendaproduto'] == that.model.get('id_vendaproduto')) {
                            elem['id_campanhacomercial'] = descontos.get('id_campanhacomercial');
                            campanhaalteracao = false;
                            if (elem['id_campanhacomercial'] != that.model.get('id_campanhacomercialoriginal')) {
                                elem['st_texto'] = 'Alterando Campanha Comercial para: ' + descontos.get('st_campanhacomercial');
                            }
                        }
                    });

                    if (campanhaalteracao && descontos) {
                        var array = ({
                            id_vendaproduto: that.model.get('id_vendaproduto'),
                            id_venda: vendaModel.get('id_venda'),
                            st_texto: 'Alterando Campanha Comercial para: ' + descontos.get('st_campanhacomercial'),
                            id_campanhacomercial: descontos.get('id_campanhacomercial'),
                            tipo: 2
                        });
                    }
                }
            if (array)
                alteracoesdeVenda.push(array);
            this.model.set('id_campanhacomercial', id_campanha);
        } else {
            this.ui.idCampanha.attr('disabled', 'disabled');
            //$("select[name='id_campanhapontualidade']").attr('disabled', 'disabled');
            $.pnotify({
                title: 'Atenção!',
                text: 'O aluno possui bolsa de 100% de desconto, a informação da Campanha e Campanha Pontualidade não poderão ser alteradas',
                type: 'warning'
            });
        }

        var that = this;
        this.ui.idCampanha.find('option').each(function (i, obj) {
            that.selectDescontosOptions[i] = {'value': obj.value, 'text': obj.text};
        });
    },
    events: {
        'change select[name="campanha"]': 'toogleDescontos'
    },
    updateTurma: function (e) {
        var value = $(e.target).val();
        this.model.set('id_turma', (value || null));
    }
});

/**
 * EmptyView para Composite de produtos
 * @type {ItemView}
 */
var EmptyView = Marionette.ItemView.extend({
    template: '#template-grid-vazia',
    tagName: 'tr'
});


/**
 * CompositeView para renderizar produtos da venda
 * @type {CompositeView}
 */
var ViewGridProdutos = Marionette.CompositeView.extend({
    template: '#template-grid-produtos',
    tagName: 'fieldset',
    className: 'view-produtos',
    childViewContainer: '#lista-produtos-venda',
    childView: ViewItemProdutos,
    emptyView: EmptyView,
    collectionEvents: {
        'add': function (model) {
            this.toggleContratoRegra();
        },
        'remove': function () {
            this.toggleContratoRegra();
        }
    },
    toggleContratoRegra: function () {
        var produtoProjeto = this.collection.findWhere({id_tipoproduto: 1});
        if (produtoProjeto) {
            $('select[name="id_contratoregra"]').removeAttr('disabled');
        } else {
            $('select[name="id_contratoregra"]').attr('disabled', 'disabled');
        }
    },
    initialize: function () {
        thatViewGridProdutos = this;
    }
});

/**
 ******************************************************************************************
 ******************************************************************************************
 ********* VENDA / FORMA PAGAMENTO ********************************************************
 ******************************************************************************************
 ******************************************************************************************
 */

/**
 * Item view para renderizar template do container de venda e valores
 * @type {ItemView}
 */
var ViewDetalhesVenda = Marionette.ItemView.extend({
    template: "#template-detalhes-venda",
    tagName: "div",
    className: "row-fluid",
    ui: {
        'idAtendente': 'select[name="id_usuario"]',
        'idContratoRegra': 'select[name="id_contratoregra"]',
        'idCampanhaPontualidade': 'select[name="id_campanhapontualidade"]'
    },
    initialize: function () {
        thatViewFormaPagamento = this;
    },
    viewFormPgtoSelected: null,
    onShow: function () {
        this.populaSelectAtendente();
        this.populaSelectContratoRegra();
        this.getSpCampanhaVendaProduto();
        this.populaSelectCampanhaPontualidade();
        return this;
    },
    populaSelectCampanhaPontualidade: function () {
        var that = this;
        campanhaPontualidadeCollection = new CampanhaPontualidadeCollection();
        campanhaPontualidadeCollection.fetch({
            data: {
                id_tipocampanha: 3,
                id_campanhaselecionada: vendaModel.get('id_campanhapontualidade')
            },
            beforeSend: loading,
            complete: loaded,
            success: function (collection) {
                var optionSelected = null;
                var defaultModel = collection.findWhere({bl_aplicardesconto: true});

                // Selecionar a campanha padrão somente se não tiver o id_venda
                if (vendaModel.get('id_venda')) {
                    optionSelected = vendaModel.get('id_campanhapontualidade');
                } else {
                    optionSelected = defaultModel ? defaultModel.get('id_campanhacomercial') : null;
                }

                that.ui.idCampanhaPontualidade.empty();
                that.ui.idCampanhaPontualidade.html('<option value="" >Selecione</option>');
                if (descontop == 100) {
                    that.ui.idCampanhaPontualidade.attr('disabled', 'disabled');
                }
                var view = new SelectView({
                    el: that.ui.idCampanhaPontualidade,
                    collection: collection,
                    childViewOptions: {
                        optionLabel: 'st_campanhacomercial',
                        optionValue: 'id_campanhacomercial',
                        optionSelected: optionSelected
                    }
                });
                view.render();
                that.selecionaCampanhaPontualidade();
                if (descontop == 100) {
                    that.ui.idCampanhaPontualidade.attr('disabled', 'disabled');
                }
            }
        });

    },
    populaSelectContratoRegra: function () {
        var that = this;
        // instancia a modelo
        //altera a url do modelo
        contratoRegraModel.url = '/venda/retornar-contrato-regra-venda/id_venda/' + vendaModel.get('id_venda');
        //faz o fetch
        contratoRegraModel.fetch({
            success: function () {
                contratoRegraVenda = contratoRegraModel;
                //no sucesso seta os valores na model de forma de pagamento
                modelFormaPagamentoSelected.set({
                    id_contratoregra: contratoRegraModel.get('id_contratoregra'),
                    st_contratoregra: contratoRegraModel.get('st_contratoregra')
                });

                var view = new SelectView({
                    el: that.$el.find('select[name="id_contratoregra"]'),
                    collection: contratoRegraCollection,
                    childViewOptions: {
                        optionLabel: 'st_contratoregra',
                        optionValue: 'id_contratoregra',
                        optionSelected: modelFormaPagamentoSelected.get('id_contratoregra') ? modelFormaPagamentoSelected.get('id_contratoregra') : null
                    }
                });
                view.render();
            }
        });
    },
    populaSelectAtendente: function () {
        var that = this;
        atendentesCollection.fetch({
            success: function () {
                var atendente = new SelectView({
                    el: that.$el.find('select[name="id_usuario"]'),
                    collection: atendentesCollection,
                    childViewOptions: {
                        optionLabel: 'st_nomecompleto',
                        optionValue: 'id_usuario',
                        optionSelected: G2S.editModel ? G2S.editModel.id_atendente : G2S.loggedUser.get('id_usuario')
                    }
                });
                atendente.render();

                $("select[name='id_usuario']").trigger('change');
                that.ui.idAtendente.val(G2S.editModel ? G2S.editModel.id_atendente : G2S.loggedUser.get('id_usuario'));

                if (modelFormaPagamentoSelected.get('id_atendente')) {
                    that.selecionaAtendente();
                }
            },
            complete: function () {

            }
        });
    },
    getFormaMeioDivisao: function () {
        collectionFormaVwMeioDivisao.url = '/api/vw-forma-meio-divisao/id_formapagamento/' + this.ui.idFormaPagamento.val();
        collectionFormaVwMeioDivisao.fetch();
    },
    getSpCampanhaVendaProduto: function () {
        if (collectionProdutoVenda.length) {
            var that = this;
            collectionProdutoVenda.each(function (model, i) {
                var params = {
                    bl_todasformas: 1,
                    bl_todosprodutos: 1,
                    id_tipocampanha: 2,
                    // É necessário enviar null como string
                    id_formapagamento: "null",
                    id_produto: model.get('id_produto'),
                    id_venda: model.get('id_venda'),
                    id_campanhaselecionada: model.get('id_campanhacomercial')
                };

                var spCampanha = new SpCampanhaVendaProdutoCollection();

                spCampanha.fetch({
                    url: "/venda/retorna-sp-campanha-venda-produto",
                    data: params,
                    reset: true,
                    beforeSend: function () {
                        $("#campanha" + model.get('id_produto')).html('<option>Carregando...</option>');
                    },
                    success: function () {
                        $("#campanha" + model.get('id_produto')).empty();
                        $("#campanha" + model.get('id_produto')).html('<option value="">Selecione</option>');
                        if (spCampanha.length) {
                            var campanhaSelect = new CampanhaProdutoView({
                                collection: spCampanha,
                                el: $("#campanha" + model.get('id_produto'))
                            });
                            descontop = parseFloat(model.get("nu_descontoporcentagem"));
                            campanhaSelect.render();
                            if (model.get("id_campanhacomercial")) {
                                $("#campanha" + model.get('id_produto')).val(model.get("id_campanhacomercial"));
                            } else {
                                $("#campanha" + model.get('id_produto')).val('');
                            }
                        } else {
                            model.set('nu_descontovalor', 0);
                            model.set('nu_descontoporcentagem', 0);
                            model.set('id_campanhacomercial', null);
                        }
                    },
                    complete: function () {
                        spCampanhaVendaProdutoCollection.add(spCampanha.toJSON());
                    }
                });
            });
        }
    },
    selecionaAtendente: function () {
        var atendente = this.ui.idAtendente.val();
        if (atendente) {
            var resultAtendente = atendentesCollection.findWhere({'id_usuario': parseInt(atendente)});
            if (resultAtendente) {
                modelFormaPagamentoSelected.set({
                    id_atendente: resultAtendente.get('id_usuario'),
                    st_nomeatendente: resultAtendente.get('st_nomecompleto')
                });
                if (vendaModel.get('id_venda')) {

                    if (vendaModel.get('id_atendente') != resultAtendente.get('id_usuario')) {
                        var atendentealteracao = true;
                        for (var i = 0; i < alteracoesdeVenda.length; i++) {
                            if ("id_atendente" in alteracoesdeVenda[i]) {
                                alteracoesdeVenda[i]['st_texto'] = 'Alterando atendente para: ' + resultAtendente.get('st_nomecompleto');
                                alteracoesdeVenda[i]['id_atendente'] = resultAtendente.get('id_usuario');
                                atendentealteracao = false;
                            }
                        }
                        if (atendentealteracao) {
                            var array = ({
                                id_venda: vendaModel.get('id_venda'),
                                st_texto: 'Alterando atendente para: ' + resultAtendente.get('st_nomecompleto'),
                                id_atendente: resultAtendente.get('id_usuario'),
                                tipo: 20
                            });
                            alteracoesdeVenda.push(array);
                        }
                    }
                }
            }
        } else {
            var userModel = new Usuario();
            userModel.id = modelFormaPagamentoSelected.get('id_atendente');
            userModel.fetch({
                beforeSend: function () {
                    loading();
                },
                complete: function () {
                    loaded();
                },
                success: function () {
                    modelFormaPagamentoSelected.set({
                        id_atendente: userModel.get('id_usuario'),
                        st_nomeatendente: userModel.get('st_nomecompleto')
                    });
                }
            });
        }
    },
    selecionaCampanhaPontualidade: function () {

        var campanha = this.ui.idCampanhaPontualidade.val();

        if (campanha) {
            var resCampanha = campanhaPontualidadeCollection.findWhere({id_campanhacomercial: parseInt(campanha)});
            if (resCampanha) {
                modelFormaPagamentoSelected.set({
                    id_campanhapontualidade: resCampanha.get('id_campanhacomercial'),
                    st_campanhapontualidade: resCampanha.get('st_campanhacomercial')
                });
            }
        } else {
            modelFormaPagamentoSelected.set({
                id_campanhapontualidade: null,
                st_campanhapontualidade: null
            });
        }

        if (vendaModel.get('id_campanhapontualidade') != modelFormaPagamentoSelected.get('id_campanhapontualidade')) {

            var campanhapontualidadealteracao = true;

            for (var i = 0; i < alteracoesdeVenda.length; i++) {
                if ("id_campanhapontualidade" in alteracoesdeVenda[i]) {

                    alteracoesdeVenda[i]['st_texto'] = modelFormaPagamentoSelected.get('st_campanhapontualidade') === null
                        ? 'Removendo campanha de pontualidade.'
                        : 'Alterando campanha de pontualidade para: ' + modelFormaPagamentoSelected.get('st_campanhapontualidade');
                    alteracoesdeVenda[i]['id_campanhapontualidade'] = modelFormaPagamentoSelected.get('id_campanhapontualidade');
                    campanhapontualidadealteracao = false;
                }
            }

            var array = [];

            if (campanhapontualidadealteracao) {
                if (modelFormaPagamentoSelected.get('id_campanhapontualidade') === null) {
                    array = {
                        id_venda: vendaModel.get('id_venda'),
                        st_texto: 'Removendo campanha de pontualidade.',
                        id_campanhapontualidade: null,
                        tipo: 2 //Tipo do trâmite
                    }
                } else {
                    array = {
                        id_venda: vendaModel.get('id_venda'),
                        st_texto: 'Alterando campanha de pontualidade para: ' + resCampanha.get('st_campanhacomercial') + '.',
                        id_campanhapontualidade: resCampanha.get('id_campanhacomercial'),
                        tipo: 2 //Tipo do trâmite
                    }
                }
                alteracoesdeVenda.push(array);
            }
        }
    },
    selecionaContratoRegra: function () {//renderiza o contrato
        var id_contratoregra = this.ui.idContratoRegra.val();
        if (id_contratoregra) {

            var contratoRegraModel = contratoRegraCollection.findWhere({id_contratoregra: parseInt(id_contratoregra)});
            if (id_contratoregra != modelFormaPagamentoSelected.get('id_contratoregra')) {
                var contratoregraalteracao = true;
                for (var i = 0; i < alteracoesdeVenda.length; i++) {
                    if ("id_contratoregra" in alteracoesdeVenda[i]) {
                        alteracoesdeVenda[i]['st_texto'] = 'Alterando Regra de Contrato para: ' + contratoRegraModel.get('st_contratoregra');
                        alteracoesdeVenda[i]['id_contratoregra'] = contratoRegraModel.get('id_contratoregra');
                        contratoregraalteracao = false;
                    }
                }
                if (contratoregraalteracao) {
                    var array = ({
                        id_venda: vendaModel.get('id_venda'),
                        st_texto: 'Alterando Regra de Contrato para: ' + contratoRegraModel.get('st_contratoregra'),
                        id_contratoregra: contratoRegraModel.get('id_contratoregra'),
                        tipo: 2
                    });
                    alteracoesdeVenda.push(array);
                }
            }
            modelFormaPagamentoSelected.set({
                id_contratoregra: contratoRegraModel.get('id_contratoregra'),
                st_contratoregra: contratoRegraModel.get('st_contratoregra')
            });
        }
    },

    events: {
        'change select[name="id_usuario"]': 'selecionaAtendente',
        'change select[name="id_contratoregra"]': 'selecionaContratoRegra',
        'change select[name="id_campanhapontualidade"]': 'selecionaCampanhaPontualidade'
    }
});

/**
 ******************************************************************************************
 ******************************************************************************************
 ********* VALORES ************************************************************************
 ******************************************************************************************
 ******************************************************************************************
 */
/**
 * ItemView para rederizar Valores no layout
 * @type {ItemView}
 */
var ValoresItemView = Marionette.ItemView.extend({
    template: '#template-valores',
    tagName: 'fieldset'
});


/**
 ******************************************************************************************
 ******************************************************************************************
 ********* TRAMITES ***********************************************************************
 ******************************************************************************************
 ******************************************************************************************
 */

/**
 * ItemView para tramites
 * @type @exp;Marionette@pro;ItemView@call;extend
 */
var ViewItemTramite = Marionette.ItemView.extend({
    template: '#template-item-tramite',
    tagName: 'tr',
    onRender: function () {
        return this;
    }
});

/**
 * ItemView para tramite vazia
 * @type @exp;Marionette@pro;ItemView@call;extend
 */
var TramiteEmptyView = Marionette.ItemView.extend({
    template: '#template-lista-vazia-tramite',
    tagName: 'tr'
});

/**
 * CollectionView para tramite
 * @type @exp;Marionette@pro;CompositeView@call;extend
 */
var TramitesCompositeView = Marionette.CompositeView.extend({
    template: '#template-tramites',
    tagName: 'fieldset',
    className: 'view-tramites',
    childViewContainer: '#lista-tramites',
    childView: ViewItemTramite,
    emptyView: TramiteEmptyView,
    ui: {
        'btnCancelar': '#btn-cancelar-tramite',
        'btnAdd': '#btn-add-tramite',
        'btnSalvar': '#btn-salvar-tramite',
        'txtTramite': '#st_tramite',
        'boxForm': '#container-form-add-tramite'
    },
    onShow: function () {
        //verifica se o id_venda esta definido para habilitar ou desabilitar o botão
        if (!vendaModel.get('id_venda')) {
            this.ui.btnAdd.attr('disabled', 'disabled');
        } else {
            this.ui.btnAdd.removeAttr('disabled');
        }
    },
    cresceInput: function () {
        //aumenta a altura do textarea
        this.ui.txtTramite.css({
            height: '80px'
        });
    },
    diminuiInput: function () {
        //diminui quando não tiver valor
        if (!this.ui.txtTramite.val()) {
            this.ui.txtTramite.css({
                height: '30px'
            });
        }
    },
    addTramite: function () {
        //mostra o box onde esta o formulario para adicionar tramite quando o id_venda existir
        if (vendaModel.get('id_venda')) {
            this.ui.boxForm.show();
            this.ui.btnAdd.attr('disabled', 'disabled');
            this.diminuiInput();
        }
    },
    cancelaTramite: function () {
        //cancela o tramite
        this.ui.txtTramite.val('');
        this.ui.boxForm.hide();
        this.ui.btnAdd.removeAttr('disabled');
    },
    salvarTramite: function () {
        var that = this;//escopo
        //varifica se existe id_venda
        if (vendaModel.get('id_venda')) {
            var model = new SpTramite();//instancia a model
            //seta os valores
            model.set({
                'st_tramite': that.ui.txtTramite.val(),
                'id_categoriatramite': 3,
                'id_campo': vendaModel.get('id_venda')
            });

            //salva os dados
            model.save(null, {
                beforeSend: function () {
                    loading();
                },
                complete: function () {
                    loaded();
                },
                success: function (model, response) {
                    //on success faz o fetch novamente dos dados do tramite
                    that.collection.fetch({
                        reset: true,
                        success: function () {
                            //mostra mensagem de sucesso
                            $.pnotify({title: 'Sucesso!', text: 'Tramite salvo com sucesso!', type: 'success'});
                            that.cancelaTramite();//esconde o formulario
                        }
                    });
                },
                error: function (model, response) {
                    $.pnotify({title: 'Erro!', text: 'Houve um erro ao tentar salvar tramite.', type: 'error'});
                }
            });
        } else {//se o id_venda não estiver definido montra o aviso
            $.pnotify({
                title: 'Aviso!',
                text: 'Venda não foi definida, para criar uma interação/tramite, salve a venda.',
                type: 'warning'
            });
        }
        return false;
    },
    events: {
        'click #btn-add-tramite': 'addTramite',
        'focus #st_tramite': 'cresceInput',
        'blur #st_tramite': 'diminuiInput',
        'click #btn-cancelar-tramite': 'cancelaTramite',
        'submit #form-tramite': 'salvarTramite'
    }
});


function abreNegociacao(id) {

    //função que busca uma negociação do banco e cria as views correspondentes
    $('.content-box-vendas').show('fast');

    //abre box de venda
    var viewDetalhesVenda = new ViewDetalhesVenda({
        //model
    });

    //abre e configura o box de produtos
    var viewGridProdutos = new ViewGridProdutos({
        collection: collectionProdutoVenda
    });

    //Abre e configura view de valores
    valoresModel = new ValoresModel();
    var viewValores = new ValoresItemView({model: valoresModel});

    //show views
    telaAlterarVendas.produtos.show(viewGridProdutos);
    telaAlterarVendas.vendas.show(viewDetalhesVenda);
    telaAlterarVendas.valores.show(viewValores);

}

function fechaNegociacao(id) {
    // ao limpar o componente de pesquisa de pessoas, precisamos fechar as views

    //collection
    collectionProdutoVenda.remove(collectionProdutoVenda.models);
    collectionVwVendaLancamento.remove(collectionVwVendaLancamento.models);
    collectionResponsavelFinanceiro.remove(collectionResponsavelFinanceiro.models)
    collectionFormaVwMeioDivisao.remove(collectionFormaVwMeioDivisao.models)
    contratoRegraVenda = null;
    modelFormaPagamentoSelected = new FormaPagamentoSelectedModel();
    valoresModel.clear();
    lancamentoVendaModel.destroy();
    vendaModel.clear();
    // que estão abertas no layout
    telaAlterarVendas.produtos.destroy();
    telaAlterarVendas.tramites.destroy();
    telaAlterarVendas.vendas.destroy();
    telaAlterarVendas.lancamentos.destroy();
    telaAlterarVendas.valores.destroy();
    telaAlterarVendas.modal.destroy();
    telaAlterarVendas.adicionarPessoaRegion.destroy();

    editable = true;
    $('.content-box-vendas').hide('fast');
}

/**
 ******************************************************************************************
 ******************************************************************************************
 ********* SHOW LAYOUT ********************************************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var telaAlterarVendas = new LayoutAlterarVenda();
G2S.show(telaAlterarVendas);
var thatModalCarteirinha;
/**
 * COLLECTION E MODEL PARA CARTEIRINHA
 */
var AlunosCarteirinhaModel = Backbone.Model.extend({
    defaults: {
        id_usuario: '',
        st_nomecompleto: '',
        st_identificacao: '',
        bl_print: true
    }
});
var AlunosCarteirinhaCollection = Backbone.Collection.extend({
    model: AlunosCarteirinhaModel,
    url: '/turma/get-alunos-turma-carteirinha'
});

/**
 * MARIONETTES ELEMENTOS PARA CARTEIRINHA
 */
var ItemViewCarteirinha = Marionette.ItemView.extend({
    template: '#template-item-carteirinha',
    tagName: 'tr',
    events: {
        'change input[name="bl_print"]': 'togglePrint',
        'change input[name="st_identificacao"]': 'setSt_identificacao'
    },
    ui: {
        bl_print: 'input[name="bl_print"]',
        st_identificacao: 'input[name="st_identificacao"]'
    },
    validaCodigoCarteirinha: function () {
        var valid = true;
        var st_codigo = this.ui.st_identificacao.val();
        if (!st_codigo) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Informe o Cód. de Identificação.'
            });
            valid = false;
        }

        if (!validaNumero(st_codigo)) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'O Cód. de Identificação deve ser somente números.'
            });
            valid = false;
        }

        if (st_codigo.length > 12) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'O Cód. de Identificação deve conter somente 12 caracteres.'
            });
            valid = false;
        }

        return valid;
    },
    setSt_identificacao: function () {
        if (this.validaCodigoCarteirinha()) {
            this.model.set('st_identificacao', this.ui.st_identificacao.val());
        } else {
            this.model.set('st_identificacao', null);
        }
    },
    togglePrint: function () {
        this.model.set('bl_print', this.ui.bl_print.is(':checked'));
    },
    onRender: function () {
    },
    modelEvents: {
        "change:bl_print": function () {
            //thatModalCarteirinha.verificaTodosCheck();
            thatModalCarteirinha.toggleBtnImprimir();
        },
        "change:st_identificacao": function () {
            thatModalCarteirinha.toggleBtnSalvar();
        }
    }
});
var EmptyViewCarteirinha = Marionette.ItemView.extend({
    template: '#template-itemcarteirinha-empty',
    tagName: 'tr'
});
var CompositeViewCarteirinha = Marionette.CompositeView.extend({
    template: '#template-modal-carteirinha',
    tagName: 'div',
    childView: ItemViewCarteirinha,
    emptyView: EmptyViewCarteirinha,
    childViewContainer: 'tbody',
    initialize: function () {
        thatModalCarteirinha = this;
    },
    events: {
        'click #btn-salvar-carteirinhas': 'salvarIdentificacoes',
        'click #btn-imprimir': 'imprimirCarteirinhas'
    },
    ui: {
        checkall: 'input[name="checkall"]',
        btnPrint: '#btn-imprimir',
        btnSalvar: '#btn-salvar-carteirinhas'
    },
    onRender: function () {
        this.$el.find('#modal-carteirinhas').modal('show');
        this.toggleBtnImprimir();
        return this;
    },
    validaModelsPrint: function () {
        var valid = true;
        var collectionWhere = this.collection.where({bl_print: true});
        if (!collectionWhere.length) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Selecione pelo menos um aluno para imprimir!'
            });
            valid = false;
        }
        return valid;
    },
    imprimirCarteirinhas: function () {
        if (this.validaModelsPrint()) {
            var collectionPrint = this.collection.where({bl_print: true});
            var arrPrint = [];
            $.each(collectionPrint, function (i, model) {
                arrPrint[i] = {
                    'id_usuario': model.get('id_usuario'),
                    'id_turma': G2S.editModel.id_turma
                };
            });
            var url = '/carteirinha/imprimir-lote/?usuarios=' + JSON.stringify(arrPrint);
            window.open(url, 'janela', 'width=350,height=430, top=100, left=100, scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no');
        }
    },
    salvarIdentificacoes: function () {
        var that = this;
        var arrObj = [];
        this.collection.each(function (model, i) {
            if (model.get('st_identificacao') && model.get('bl_print')) {
                arrObj[i] = {
                    id_usuario: model.get('id_usuario'),
                    st_identificacao: model.get('st_identificacao')
                };
            }
        });
        if (arrObj) {
            $.ajax({
                dataType: 'json',
                type: 'post',
                url: '/pessoa/salvar-array-identificacao',
                data: {
                    usuarios: arrObj
                },
                beforeSend: function () {
                    loading();
                },
                success: function (data) {
                    $.pnotify({
                        type: data.type,
                        title: data.title,
                        text: data.text
                    });
                },
                complete: function () {
                    loaded();
                    that.$el.find('#modal-carteirinhas').modal('hide');
                },
                error: function (data) {
                    $.pnotify({
                        type: 'error',
                        title: 'Erro!',
                        text: 'Erro ao tentar salvar códigos dos usuários.'
                    });
                }
            });
        }
    },
    toggleBtnImprimir: function () {
        var collectionWhere = this.collection.where({bl_print: true});
        if (collectionWhere.length) {
            this.ui.btnPrint.prop('disabled', false);
        } else {
            this.ui.btnPrint.prop('disabled', true);
        }
    },
    toggleBtnSalvar: function () {
        var salvar = false;
        this.collection.each(function (model, i) {
            if (model.get('st_identificacao')) {
                salvar = true;
            }
        });
        if (salvar) {
            this.ui.btnSalvar.prop('disabled', false);
        } else {
            this.ui.btnSalvar.prop('disabled', true);
        }
    }
    //verificaTodosCheck: function () {
    //    var checkAll = true;
    //    this.collection.each(function (model, i) {
    //        if (!model.get('bl_print')) {
    //            checkAll = false;
    //        }
    //    });
    //    if (checkAll) {
    //        this.ui.checkall.prop('checked', true);
    //    } else {
    //        this.ui.checkall.prop('checked', false);
    //    }
    //},
    //toggleCheckAll: function () {
    //    var that = this;
    //    $.each(this.children._views, function (i, view) {
    //        if (that.ui.checkall.is(':checked')) {
    //            view.ui.bl_print.prop('checked', true);
    //        } else {
    //            view.ui.bl_print.prop('checked', false);
    //        }
    //    });
    //}
});


//item view
var HorarioItemView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: '#item-horario',
    events: {
        'click #ck_selectedHorario': 'selectedHorario'
    },
    selectedHorario: function () {
        if (this.model.get('ck_selectedHorario') == true) {
            this.model.set({'ck_selectedHorario': false});
        } else {
            this.model.set({'ck_selectedHorario': true});
        }
    }
});


var NoHorarioView = Backbone.Marionette.ItemView.extend({
    tagName: 'tr',
    template: "#no-horario"
});
//composite view
var HorarioCompositeView = Marionette.CompositeView.extend({
    template: '#tabelaHorario',
    childView: HorarioItemView,
    childViewContainer: 'tbody',
    emptyView: NoHorarioView
});

var ProjetosPegagogicosCollection = Backbone.Collection.extend({
    model: ProjetoPedagogico,
    async: false,
    url: '/sala-de-aula/retorna-projeto-pedagogico/'
});

var ProjetosSelecionadosCollection = Backbone.Collection.extend({
    model: ProjetoPedagogico,
    //async: false,
    url: function () {
        return G2S.editModel ? '/turma/retorna-turma-projeto/id/' + G2S.editModel.id_turma : '/turma/retorna-turma-projeto/'
    }
});
ProjetosSelecionadosCollection.comparator = function (model) {
    return model.get('st_projetopedagogico');
};

// collection do backbone com os objetos
var horarioCollection = Backbone.Collection.extend({
    model: VwHorarioAula,
    url: function () {
        return G2S.editModel ? '/turma/retorna-horarios/id/' + G2S.editModel.id_turma : '/turma/retorna-horarios'
    },
    initialize: function () {
        this.fetch();
    }
});

var collection;

var collectionSelecionados = new ProjetosSelecionadosCollection();
var collectionProjetos = new ProjetosPegagogicosCollection();

var ProjetosPedagogicosItemView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: '#item-projetos',
    onRender: function () {
        return this;
    },
    addModel: function () {
        collectionSelecionados.add(this.model.clone());

        this.remove();
        this.model.destroy({});
    },
    events: {
        'click #btn-add': 'addModel'
    }
});

var ProjetosSelecionadosItemView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: '#item-selecionados',
    collection: collectionSelecionados,
    removeModel: function () {
        var that = this;

        if (that.model.get('id_turma')) {
            bootbox.confirm("Este projeto será removido do registro! Deseja continuar?", function (result) {
                if (result) {
                    collectionProjetos.add(that.model.clone());
                    that.remove();
                    that.model.url = '/turma/deleta-projeto/id/' + that.model.id + '/proj/' + G2S.editModel.id_turma;
                    that.model.destroy({});
                    $.pnotify({title: 'Projeto removido', type: 'success'});
                }
            });
        } else {
            collectionProjetos.add(that.model.clone());
            that.remove();
            that.model.destroy();
            collectionProjetos.comparator = 'st_projetopedagogico';
            collectionProjetos.sort();
        }
    },
    onRender: function () {
        var that = this;
        collectionProjetos.remove(that.model);
        return this;
    },
    events: {
        'click #btn-remove': 'removeModel'
    }
});

var ProjetosCollectionSelecionadosView = Marionette.CollectionView.extend({
    template: '#turma',
    childView: ProjetosSelecionadosItemView,
    onRender: function () {
        this.collection.fetch();
    }
});


var teste;

var itemTurma = Marionette.LayoutView.extend({
    template: '#turma',
    regions: {
        'projetoPedagogico': '#projeto-pedagogico-view'
    },
    ui: {
        dataInicioInscricao: '#dataInicioInscricao',
        dataFimInscricao: '#dataFimInscricao',
        dataAbertura: '#dataAbertura',
        dataEncerramento: '#dataEncerramento',
        comboEvolucao: '#combo_evolucao',
        nu_maxFreePass: '#maxFreePass'
    },
    initialize: function () {
        btn_enviarEmail = this;
    },
    onShow: function () {
        this.populaSelectEvolucao();
        var myItem = new SideBySideView({
            collections: {
                left: collectionProjetos,
                right: collectionSelecionados
            },
            label: 'st_projetopedagogico',
            fetch: false,
            addAll: true,
            globalSearch: true,
            confirmRemove: true
        });
        this.projetoPedagogico.show(myItem);

        // Quando terminar o fetch de todos os projetos, e dos projetos selecionados,
        // remover os projetos selecionados que estao na lista de todos os projetos
        $.when(collectionProjetos.fetch({async: true}), collectionSelecionados.fetch({async: true})).done(function () {
            collectionProjetos.remove(collectionSelecionados.toJSON());
        });


        collection = new horarioCollection();

        var horarioView = new HorarioCompositeView({
            collection: collection,
            el: '#coisa'
        });
        horarioView.render();


        var btnModel = new ButtonModel({
            caption: "Enviar e-mail de matricula para alunos",
            url: '/turma/envia-email/idturma/' + G2S.editModel.id_turma,
            color: "success",
            class_btn: "teste"
        });

        if (G2S.editModel.id_turma) {
            G2S.botoesAuxiliares.add(btnModel);
            this.$el.find('#btn-modal-carteirinha').removeAttr('disabled');
        }

        this.carregaEntidade();
        return this;
    },
    populaSelectEvolucao: function () {
        var Collection = Backbone.Collection.extend({
            url: '/api/evolucao/st_tabela/tb_turma',
            model: Evolucao
        });
        var view;
        var that = this;
        var collectionEvolucao = new Collection();
        collectionEvolucao.fetch({
            beforeSend: function () {
                loading()
            },
            success: function () {
                view = new SelectView({
                    el: that.$el.find("select[name='combo_evolucao']"),      // Elemento da DOM
                    collection: collectionEvolucao,      // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_evolucao', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_evolucao', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: G2S.editModel ? G2S.editModel.id_evolucao : null       // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();
            },

            complete: function () {

                if (G2S.editModel) {
                    if (G2S.editModel.id_evolucao != 46) {
                        $('#maxFreePass').attr('disabled', true)
                    } else {
                        VerificarPerfilPermissao(20, 409, true).done(function (response) {
                            if (response.length == 1) {
                                $('#maxFreePass').removeAttr('disabled')
                            }
                        });
                    }
                }
                loaded();
            }
        });
    },
    salvaTransaction: function () {
        var that = this;

        var inputTitulo = $("#inputTitulo").val();
        var inputTituloExibicao = $("#inputTituloExibicao").val();

        //validação de campo
        if (inputTitulo.length > 255) {
            bootbox.alert("O limite de caracteres foi atingido, por favor altere o nome do Título");
            return false;
        }
        if (inputTituloExibicao.length > 255) {
            bootbox.alert("O limite de caracteres foi atingido, por favor altere o nome do Título de Exibição");
            return false;
        }

        if (this.verificaData() && this.validaProjetoSelecionado()) {
            var modelo = new Turma();
            var turmaTurno = new TurmaTurno();
            if (G2S.editModel) {
                modelo.set({
                    'id_turma': G2S.editModel.id_turma,
                    'st_turma': $("#inputTitulo").val(),
                    'st_tituloexibicao': $("#inputTituloExibicao").val(),
                    'nu_maxalunos': $("#maxAlunos").val(),
                    'dt_inicioinscricao': $("#dataInicioInscricao").val(),
                    'dt_fiminscricao': $("#dataFimInscricao").val(),
                    'dt_inicio': $("#dataAbertura").val(),
                    'dt_fim': $("#dataEncerramento").val(),
                    'st_codigo': $("#st_codigo").val(),
                    'id_situacao': $("#selectSituacao option:selected").val(),
                    'id_evolucao': $("#combo_evolucao option:selected").val(),
                    'nu_maxfreepass': that.ui.comboEvolucao.val() == 46 ? that.ui.nu_maxFreePass.val() : 0
                });
                turmaTurno.set({
                    'id_turma': G2S.editModel.id_turma,
                    'id_turno': $("#selectTurno option:selected").val()
                });
            } else {
                modelo.set({
                    'st_turma': $("#inputTitulo").val(),
                    'st_codigo': $("#st_codigo").val(),
                    'st_tituloexibicao': $("#inputTituloExibicao").val(),
                    'nu_maxalunos': $("#maxAlunos").val(),
                    'dt_inicioinscricao': $("#dataInicioInscricao").val(),
                    'dt_fiminscricao': $("#dataFimInscricao").val(),
                    'dt_inicio': $("#dataAbertura").val(),
                    'dt_fim': $("#dataEncerramento").val(),
                    'id_situacao': $("#selectSituacao option:selected").val(),
                    'id_evolucao': $("#combo_evolucao option:selected").val(),
                    'nu_maxfreepass': that.ui.comboEvolucao.val() == 46 ? that.ui.nu_maxFreePass.val() : 0
                });
                turmaTurno.set({
                    'id_turno': $("#selectTurno option:selected").val()
                });
            }

            var array = [];

            _.each(collectionSelecionados.models, function (model) {
                array.push(model.attributes.id_projetopedagogico);
            });

            var checked_ids_horario = [];

            _.each(collection.models, function (model) {
                checked_ids_horario.push(model.toJSON());
            });

            var checked_ids = [];

            this.$el.find("#entidades").jstree("get_checked", null, true)
                .each(function () {
                    checked_ids.push(this.id);
                }); //buscando valores da árvore

            if (this.$el.find("#bl_todasentidades").is(":checked")) {
                var bl_todasentidades = 1;
                this.$el.find("#entidades").jstree("get_unchecked", null, false)
                    .each(function () {
                        checked_ids.push(this.id);
                    }); //buscando valores da árvore

            } else {
                var bl_todasentidades = 0;
            }

            $.ajax({
                url: "turma/envia-dados-tabela",
                type: 'POST',
                dataType: 'json',
                data: {
                    id_projetopedagogico: array,
                    modeloTurma: modelo.toJSON(),
                    id_entidade: checked_ids,
                    modeloTurno: turmaTurno.toJSON(),
                    horario: checked_ids_horario
                },
                beforeSend: function () {
                    loading();
                    $('#btn-salvar').attr('disabled', true);
                },
                success: function (data) {
                    modelo.set(data);

                    if (modelo.get('st_codigo'))
                        $('#st_codigo').attr('readonly', true);


                    if (modelo.get('saldoFreePass'))
                        $('#saldoFreePass').val(modelo.get('saldoFreePass'));


                    window.location.href = "/#CadastrarTurma/editar/" + modelo.get('id_turma');
                    $.pnotify({title: 'Registro de Turma', text: 'Cadastro realizado com sucesso', type: 'success'});
                    loading();
                },
                complete: function () {
                    loaded();
                    $('#btn-salvar').attr('disabled', false);
                },
                error: function (data) {
                    var mensageiro = $.parseJSON(data.responseText);
                    $.pnotify(mensageiro);
                }
            });
        }
    },
    carregaEntidade: function () {
        var that = this;

        that.$el.find("#bl_todasentidades").attr("checked", false);
        that.$el.find("#entidades").show();

        loaded();
        that.$el.find("#entidades").jstree({
            "html_data": {
                "ajax": {
                    "url": "util/arvore-entidade",
                    "data": function (n) {
                        return {id: n.attr ? n.attr("id") : 0};
                    }
                }
            },
            "plugins": ["themes", "html_data", "checkbox", "sort", "ui"]
        }).bind("loaded.jstree", function () {
            if (parseInt(that.$el.find("#id").val()) !== 0) {
                $.ajax({
                    url: G2S.editModel ? "turma/recarrega-arvore/idturma/" + G2S.editModel.id_turma : null,
                    type: "POST",
                    dataType: "json",
                    success: function (data) {
                        var entidadesArvore = [];
                        that.$el.find("#entidades")
                            .jstree("get_unchecked", null, true)
                            .each(function () {
                                for (var i = 0; i < data.length; i++) {
                                    if (parseInt(data[i]) === parseInt(this.id)) {
                                        $.jstree._reference("#entidades").check_node('li#' + this.id);
                                    }
                                }
                                entidadesArvore.push(this.id);
                            });

                        if (data.length === entidadesArvore.length) {
                            that.$el.find("#bl_todasentidades").prop("checked", true);
                            that.$el.find("#entidades").hide();
                        }
                    }
                });
            }
        });
    },
    todasEntidades: function () {
        this.$el.find("#entidades").slideToggle();
    },
    events: {
        'click #btn-addall': 'addAll',
        'click #btn-removeall': 'removeAll',
        'click #bl_todasentidades': 'todasEntidades',
        'submit form#form-turma': 'salvaTransaction',
        'change #dataFimInscricao': 'verificaData',
        'change #dataInicioInscricao': 'verificaData',
        'change #dataAbertura': 'verificaData',
        'change #dataEncerramento': 'verificaData',
        'change #combo_evolucao': 'verificaEvolucao',
        'click #btnbtnModel': 'enviaEmail',
        'click #btn-modal-carteirinha': 'abrirModalCarteirinha',
        'click #btn-finalizar': 'finalizarTurma'
    },
    verificaEvolucao: function () {
        var that = this;

        if (that.ui.comboEvolucao.val() != 46) {
            $('#maxFreePass').attr('disabled', true)
        } else {
            VerificarPerfilPermissao(20, 409, true).done(function (response) {
                if (response.length == 1) {
                    $('#maxFreePass').removeAttr('disabled')
                }
            })
        }
    },
    enviaEmail: function () {
        var that = this;
        var url = '/turma/envia-email/idturma/' + G2S.editModel.id_turma;
        $.get(url, function (response) {
            bootbox.dialog({
                message: response.mensagem[0],
                title: response.title,
                buttons: {
                    success: {
                        label: "OK!",
                        className: "btn-success"
                    }
                }
            });

        });

    },
    finalizarTurma: function () {

        var that = this;

        if (that.model.get('id_turma')) {
            bootbox.confirm("Deseja realmente finalizar esta Turma?", function (result) {
                if (result) {

                    switch (that.model.get('id_evolucao')) {

                        case 54:
                            bootbox.alert("Você não pode finalizar uma Turma cancelada.");
                            break;
                        case 53:
                            bootbox.alert("Você não pode finalizar uma Turma já finalizada.");
                            break;
                        default:
                            //jhjkhkjhkhjhkjhkj


                            $.ajax({
                                url: "turma/finalizar",
                                type: 'POST',
                                dataType: 'json',
                                data: {
                                    id_turma: that.model.get('id_turma')
                                },
                                beforeSend: function () {
                                    loading();
                                    $('#btn-salvar').attr('disabled', true);
                                    $('#btn-finalizar').attr('disabled', true);
                                },
                                success: function (data) {

                                    if (data.tipo == 1) {
                                        that.model.set({
                                            id_evolucao: data.mensagem.id_evolucao,
                                            dt_fim: data.mensagem.dt_fim
                                        });
                                        $("#combo_evolucao").val(data.mensagem.id_evolucao);
                                        $("#dataEncerramento").val(data.mensagem.dt_fim.substring(0, 10));
                                        $.pnotify({
                                            title: 'Sucesso',
                                            text: "Turma finalizada com sucesso.",
                                            type: 'success'
                                        });
                                    } else {
                                        $.pnotify({title: 'Erro', text: data.mensagem, type: 'danger'});
                                    }
                                },
                                complete: function () {
                                    loaded();
                                    $('#btn-salvar').attr('disabled', false);
                                    $('#btn-finalizar').attr('disabled', false);
                                },
                                error: function (data) {
                                    $.pnotify({title: 'Erro', text: 'Operação não realizada!', type: 'danger'});
                                }
                            });

                            break;


                    }


                }
            });
        } else {
            bootbox.alert("Antes de finalizar uma Turma, você precisa salvá-la.");
        }
    },
    validaProjetoSelecionado: function () {
        var valido = true;
        if (!collectionSelecionados.length) {
            $.pnotify({
                type: "warning",
                title: "Atenção!",
                text: "Projeto Pedagogico não vinculado, selecione ao menos um projeto para salvar a turma."
            });
            valido = false;
        }

        return valido;

    },
    verificaData: function (e) {
        if ($('#dataInicioInscricao').val() != '' || $('#dataFimInscricao').val() != '') {
            mensagem = {};
            if (compareDate($('#dataInicioInscricao').val(), $('#dataFimInscricao').val()) == -1) {
                this.ui.dataInicioInscricao.parent().addClass('error');
                this.ui.dataFimInscricao.parent().addClass('error');
                mensagem.title = 'Verifique as Datas!';
                mensagem.text = 'A data de inicio da inscrição deve ser menor ou igual a data de fim da inscrição!';
                mensagem.type = 'warning';
            } else if (compareDate($('#dataAbertura').val(), $('#dataEncerramento').val()) == -1) {
                this.ui.dataAbertura.parent().addClass('error');
                this.ui.dataEncerramento.parent().addClass('error');
                mensagem.title = 'Verifique as Datas!';
                mensagem.text = 'A data de abertura da turma deve ser menor ou igual a data de encerramento da turma!';
                mensagem.type = 'warning';
            } else if (compareDate($('#dataInicioInscricao').val(), $('#dataAbertura').val()) == -1) {
                this.ui.dataInicioInscricao.parent().addClass('error');
                this.ui.dataAbertura.parent().addClass('error');
                mensagem.title = 'Verifique as Datas!';
                mensagem.text = 'A data de inicio da inscrição deve ser menor ou igual a data de abertura da turma!';
                mensagem.type = 'warning';
            } else {
                this.ui.dataAbertura.parent().removeClass('error');
                this.ui.dataInicioInscricao.parent().removeClass('error');
                this.ui.dataEncerramento.parent().removeClass('error');
                this.ui.dataFimInscricao.parent().removeClass('error');
            }
            if (!e) {
                if (mensagem.type) {
                    $.pnotify(mensagem);
                    return false;
                }
            }

            return true;
        } else {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Selecione as datas de inicio e fim da incrição na turma.'
            });
            return false;
        }

    },
    abrirModalCarteirinha: function () {
        var that = this;
        var collectionCarteirinha = new AlunosCarteirinhaCollection();
        collectionCarteirinha.url += '/id_turma/' + that.model.get('id_turma');
        collectionCarteirinha.fetch({
            beforeSend: function () {
                loading();
            },
            complete: function () {
                loaded();
            },
            error: function () {
                $.pnotify({
                    type: 'error',
                    title: 'Erro!',
                    text: 'Erro ao consultar dados.'
                });
            },
            success: function () {
                var carteirinhaView = new CompositeViewCarteirinha({
                    el: that.$el.find('#container-modal'),
                    collection: collectionCarteirinha
                });
                carteirinhaView.render();
            }
        });
    }
});

var model = new Turma();

if (G2S.editModel != null) {
    model.set(G2S.editModel);
//to do: Refatorar a chamada do ajax!
}

var turmaView = new itemTurma({
    model: model
});

G2S.show(turmaView);

loaded();

$('.teste').on('click', function (e) {
    e.preventDefault();
    btn_enviarEmail.enviaEmail();
});

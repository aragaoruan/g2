/**
 * View geral para a funcionalidade de horario de aula
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */

var HorariosPontoSoftModel = Backbone.Model.extend({
    defaults: {
        cod_horario_acesso: '',
        descr_horario_acesso: '',
        vetor_horarios: '',
        padrao_hora_extra: '',
        limite_horario_movel: ''
    }
});
var HorarioPontoSoftCollection = Backbone.Collection.extend({
    model: HorariosPontoSoftModel,
    url: '/horario-de-aula/horarios-ponto-soft/'
});

/***********************
 *   Model Horario de Aula
 **********************/
var HorarioDeAulaModel = Backbone.Model.extend({
    url: function () {
        return 'api/horario-de-aula/' + (this.id || '');
    },
    idAttribute: 'id_horarioaula',
    defaults: {
        id_horarioaula: null,
        id_codhorarioacesso: null,
        st_codhorarioacesso: null,
        st_horarioaula: '',
        hr_inicio: '',
        hr_fim: '',
        id_tipoaula:{
            "id_tipoaula":null,
            "st_tipoaula":null
        },
        id_turno: {
            "id_turno": null,
            "st_turno": ''
        },
        id_entidade: {
            "id_entidade": null,
            "st_nomeentidade": ''
        },
        diasemana: {
            bl_segunda: 0,
            bl_terca: 0,
            bl_quarta: 0,
            bl_quinta: 0,
            bl_sexta: 0,
            bl_sabado: 0,
            bl_domingo: 0
        }
    }
});


/***********************
 *   Collection
 **********************/
var HorarioDeAulaCollection = Backbone.Collection.extend({
    model: HorarioDeAulaModel,
    url: function () {
        return '/api/horario-de-aula/';
    }
});



/***********************
 *   View para cada Row
 **********************/
var HorarioDeAulaView = Marionette.ItemView.extend({
    template: '#horario-de-aula-item',
    tagName: 'tr',
    events: {
        'click .edit': 'editar',
        'click .cancel': 'cancel',
        'click .save': 'save',
        'click .delete': 'delete'
    },
    is_editing: false,
    //render: function(){
    //    var that = this;
    //    var HorarioCollection = new HorarioPontoSoftCollection();
    //        HorarioCollection.fetch({
    //            success: function(){
    //                var HorarioPontoSoft = HorarioCollection.find(function(model) { return model.get('cod_horario_acesso') == that.model.get('id_codhorarioacesso'); });
    //                console.log(HorarioPontoSoft);
    //            },
    //            error: function (a, response) {
    //                console.log(response);
    //            }
    //    });
    //},
    toggleEdit: function () {
        if (this.is_editing)
            this.template = '#horario-de-aula-item';
        else
            this.template = '#horario-de-aula-edit';

        this.render();
        this.is_editing = !this.is_editing;
    },
    editar: function () {
        this.toggleEdit();

        getTurnos(this.el, this.model.get('id_turno').id_turno);
        getHorariosPontoSoft(this.el, this.model.get('id_codhorarioacesso'));
        getTipoAula(this.el, this.model.get('id_tipoaula').id_tipoaula);
        $('.timepicker').timepicker({
            showMeridian: false
        });
    },
    cancel: function () {
        this.toggleEdit();
    },
    save: function () {
        var that = this;
        var elem = $(this.el);

        var values = {
            st_horarioaula: elem.find('[name=st_horarioaula]').val(),
            id_codhorarioacesso: elem.find('[name=id_codhorarioacesso]').val(),
            st_codhorarioacesso: elem.find('[name=id_codhorarioacesso] option:selected').text(),
            hr_inicio: elem.find('[name=hr_inicio]').val(),
            hr_fim: elem.find('[name=hr_fim]').val(),
            id_turno: {
                id_turno: elem.find('[name=id_turno]').val(),
                st_turno: elem.find('[name=id_turno] option:selected').text()
            },
            id_tipoaula:{
                id_tipoaula:elem.find('[name=id_tipoaula]').val(),
                st_tipoaula:elem.find('[name=id_tipoaula] option:selected').text()
            },
            diasemana: {
                bl_segunda: elem.find('[name=bl_segunda]').prop('checked'),
                bl_terca: elem.find('[name=bl_terca]').prop('checked'),
                bl_quarta: elem.find('[name=bl_quarta]').prop('checked'),
                bl_quinta: elem.find('[name=bl_quinta]').prop('checked'),
                bl_sexta: elem.find('[name=bl_sexta]').prop('checked'),
                bl_sabado: elem.find('[name=bl_sabado]').prop('checked'),
                bl_domingo: elem.find('[name=bl_domingo]').prop('checked')
            }
        };

        // Validar os campos
        // Verificar se preencheu a descricao em Horario
        if (!values.st_horarioaula || values.st_horarioaula == '') {
            $.pnotify({
                title: 'Erro',
                text: 'O campo horário não pode ficar vazio.',
                type: 'error'
            });
            elem.find('[name=st_horarioaula]').focus();
            return false;
        }
        // Verificar se hora inicial é maior que a final
        var ini = parseInt(values.hr_inicio.split(':').join(''));
        var fim = parseInt(values.hr_fim.split(':').join(''));
        if (ini > fim) {
            $.pnotify({
                title: 'Erro',
                text: 'O horário de início não pode ser maior que o horário de término.',
                type: 'error'
            });
            elem.find('[name=hr_inicio]').focus();
            return false;
        }
        // Verificar se selecionou o turno
        if (!values.id_turno.id_turno || values.id_turno.id_turno == '') {
            $.pnotify({
                title: 'Erro',
                text: 'É necessário selecionar um turno.',
                type: 'error'
            });
            elem.find('[name=id_turno]').focus();
            return false;
        }

        // Verificar se selecionou o tipo de aula
        if (!values.id_tipoaula.id_tipoaula || values.id_tipoaula.id_tipoaula== '') {
            $.pnotify({
                title: 'Erro',
                text: 'É necessário selecionar um um tipo de aula.',
                type: 'error'
            });
            elem.find('[name=id_tipoaula]').focus();
            return false;
        }

        loading();
        this.model.set(values);

        this.model.save(null, {
            success: function (model, response) {
                $.pnotify({title: 'Concluído', text: 'O registro foi atualizado com sucesso!', type: 'success'});

                if (response.id_horarioaula) {
                    // Se estiver adicionando um novo (POST), é necessário
                    // redefinir o id_horarioaula e id_entidade que estão nulos
                    values.id_horarioaula = response.id_horarioaula;
                    values.id_entidade = response.id_entidade;
                    that.model.set(values);
                    that.render();
                }

                that.toggleEdit();
            },
            error: function (model, response) {
                $.pnotify({title: 'Erro', text: 'Houve um erro ao tentar atualizar o registro, tente novamente.', type: 'error'});
            },
            complete: function () {
                loaded();
            }
        });
    },
    delete: function () {
        var that = this;
        bootbox.confirm("Tem certeza de que deseja apagar o registro?", function (result) {
            if (result) {
                that.model.destroy({
                    success: function (model, response) {
                        $.pnotify({title: 'Concluído', text: 'O registro foi removido.', type: 'success'});
                        that.remove();
                    },
                    error: function (model, response) {
                        $.pnotify({title: 'Erro', text: 'Houve um erro ao remover o registro, tente novamente.', type: 'error'});
                    }
                });
            }
        });
    }
});


/***********************
 *   CompositeView
 **********************/
var HorarioDeAulaCompositeView = Marionette.CompositeView.extend({
    template: '#horario-de-aula-list',
    collection: new HorarioDeAulaCollection(),
    childView: HorarioDeAulaView,
    childViewContainer: 'tbody',
    events: {
        'click .add': function() {
            var view = new HorarioDeAulaView({model: new HorarioDeAulaModel()});
            var element = this.$el.find(this.childViewContainer);
            element.append(view.render().el);
            view.editar();
        }
    },
    initialize: function () {
        this.collection.fetch({
            success: function (model, response) {
                loaded();
            }
        });
    }
});


var HorarioDeAula = new HorarioDeAulaCompositeView();
G2S.layout.addRegions({
    horarioDeAula: '#horario-de-aula'
});
G2S.layout.horarioDeAula.show(HorarioDeAula);


/**
 * Modo editar:
 * COMBO BOX: Turnos
 */
function getTurnos(element, selectedId) {
    var TurnoModel = Backbone.Model.extend({
        defaults: {
            id_turno: '',
            st_turno: ''
        }
    });
    var TurnoCollection = Backbone.Collection.extend({
        model: TurnoModel,
        url: '/horario-de-aula/turnos/'
    });
    var TurnoView = Marionette.ItemView.extend({
        tagName: "option",
        initialize: function () {
            _.bind(this, 'render');
        },
        render: function () {
            var element = $(this.el);

            element.attr('value', this.model.get('id_turno'))
                .html(this.model.get('st_turno'));

            if (selectedId == this.model.get('id_turno'))
                element.attr('selected', 'selected');

            return this;
        }
    });

    var TurnoCollectionIni = new TurnoCollection();
    TurnoCollectionIni.fetch({
        success: function () {
            renderer = new CollectionView({
                collection: TurnoCollectionIni,
                childViewConstructor: TurnoView,
                childViewTagName: 'option',
                el: $(element).find('[name=id_turno]')
            });
            renderer.render();
        }
    });
}

/**
 * Modo editar:
 * COMBO BOX: Horários PontoSoft
 */
function getHorariosPontoSoft(element, selectedId) {


    var HorarioView = Marionette.ItemView.extend({
        tagName: "option",
        initialize: function () {
            _.bind(this, 'render');
        },
        render: function () {
            var element = $(this.el);

            element.attr('value', this.model.get('cod_horario_acesso'))
                .html(this.model.get('cod_horario_acesso') + ' - ' + this.model.get('descr_horario_acesso'));

            if (selectedId == this.model.get('cod_horario_acesso'))
                element.attr('selected', 'selected');

            return this;
        }
    });

    var HorarioCollectionIni = new HorarioPontoSoftCollection();
        HorarioCollectionIni.fetch({
            success: function () {
                renderer = new CollectionView({
                    collection: HorarioCollectionIni,
                    childViewConstructor: HorarioView,
                    childViewTagName: 'option',
                    el: $(element).find('[name=id_codhorarioacesso]')
                });
                renderer.render();
            },
            error: function (a, response) {
                $(element).find('[name=id_codhorarioacesso]').remove();
                $.pnotify({
                    title: 'Aviso',
                    text: 'Não foram enconrados os Horários da Integração com a PontoSoft. Caso esta entidade possua Integração com as Catracas da PontoSoft, favor procurar o suporte antes de prosseguir.',
                    type: 'error'
                });
            }
        });
}


/**
 * Modo editar:
 * COMBO BOX: TipoAula
 */
function getTipoAula(element, selectedId) {
    var TipoAulaModel = Backbone.Model.extend({
        defaults: {
            id_tipoaula: '',
            st_tipoaula: ''
        }
    });
    var TipoAulaCollection = Backbone.Collection.extend({
        model: TipoAulaModel,
        url: '/horario-de-aula/retorna-tipo-aula/'
    });
    var TipoAulaView = Marionette.ItemView.extend({
        tagName: "option",
        initialize: function () {
            _.bind(this, 'render');
        },
        render: function () {
            var element = $(this.el);

            element.attr('value', this.model.get('id_tipoaula'))
                .html(this.model.get('st_tipoaula'));

            if (selectedId == this.model.get('id_tipoaula'))
                element.attr('selected', 'selected');

            return this;
        }
    });

    var TipoAulaCollectionIni = new TipoAulaCollection();
    TipoAulaCollectionIni.fetch({
        success: function () {
            renderer = new CollectionView({
                collection: TipoAulaCollectionIni,
                childViewConstructor: TipoAulaView,
                childViewTagName: 'option',
                el: $(element).find('[name=id_tipoaula]')
            });
            renderer.render();
        }
    });
}
/**
 * VARIAVEIS GLOBAIS
 */

var arrayArea,
    arrayNivel,
    arrayEntidade;

var layoutInit,
    formInit,
    modalProjetoInit,
    salasInit,
    salasOrigemInit,
    salasDestinoInit,
    modalGradeInit,
    modalSimulacaoInit,
    modalLancamentosInit,
    financeiroInit,
    ressarcimentoInit,
    negociacaoFinanceiraInit,
    botoesGeraisInit;

var collectionMeioPagamento,
    collectionCampanha,
    collectionAtendente,
    collectionResponsavelFinanceiro,
    collectionVwVendaLancamento,
    collectionModalLancamento,
    collectionVendaAditivo,
    collectionAutoComplete;

var modelMatricula,
    modelVendaAditivo;

var thatLancamento,
    thatAutoComplete,
    thatCalculoValorTransferencia,
    thatIdVenda;

var thatNuValorAPagar = 0;

/**
 * ARRAYS
 */

/**
 * Array com tipos de lançamentos baseados na tb_lancamentovenda na coluna bl_entrada
 */
var TiposLancamentos = [
    {
        id: 1,
        name: 'Entrada',
        bl_entrada: 1
    },
    {
        id: 2,
        name: 'Parcela',
        bl_entrada: 0
    }
];


/**
 * MODELS
 */

var ModelMeioPagamento = Backbone.Model.extend({
    idAttribute: 'id_meiopagamento',
    defaults: {
        'id_meiopagamento': null,
        'st_meiopagamento': ''
    }
});

var ModelCampanha = Backbone.Model.extend({
    idAttribute: 'id_campanhacomercial',
    defaults: {
        'id_campanhacomercial': null,
        'st_campanhacomercial': '',
        'id_tipodesconto': '',
        'nu_valordesconto': ''
    }
});

var ModelAtendente = Backbone.Model.extend({
    idAttribute: 'id_atendente',
    defaults: {
        'id_atendente': null,
        'st_atendente': ''
    }
});

var ModelResponsavelFinanceiro = Backbone.Model.extend({
    idAttribute: 'id_usuario',
    defaults: {
        'id': '',
        'st_cpf': '',
        'st_nomecompleto': '',
        'st_cnpj': '',
        'st_tiporesponsavel': 'Fisica'
    }
});


/**
 * COLLECTION
 */
var ModalProjetoCollection = Backbone.Collection.extend({
    model: ProjetoPedagogico,
    url: '/transfere-projeto/retorno-pesquisa-projeto/'
});

var SalasOrigemCollection = Backbone.Collection.extend({
    url: '/transfere-projeto/retornar-disciplinas-origem/',
    model: Backbone.Model.extend({
        defaults: {
            id_disciplina_prr: null,
            st_disciplina_prr: null,
            st_saladeaula_prr: null,
            dt_abertura_prr: null,
            dt_encerramento_prr: null,
            nu_notafinal_prr: null,
            st_notatcc_prr: null,
            st_notaead_prr: null
        }
    })
});

var SalasDestinoCollection = SalasOrigemCollection.extend({
    url: '/transfere-projeto/retornar-disciplinas-destino/'
});

var BancoCollection = Backbone.Collection.extend({
    model: Banco,
    url: '/default/util/get-banco'
});

var MeioPagamentoCollection = Backbone.Collection.extend({
    model: ModelMeioPagamento,
    url: '/default/transfere-projeto/get-meio-pagamento'
});

var CampanhaCollection = Backbone.Collection.extend({
    model: ModelCampanha,
    url: '/default/transfere-projeto/get-campanha'
});

var AtendenteCollection = Backbone.Collection.extend({
    model: ModelAtendente,
    url: '/default/transfere-projeto/get-atendente'
});

var ResponsavelFinanceiroCollection = Backbone.Collection.extend({
    model: ModelResponsavelFinanceiro
});

var VwVendaLancamentoCollection = Backbone.Collection.extend({
    model: VwVendaLancamento,
    url: '/api/vw-venda-lancamento'
});

var VendaAditivoCollection = Backbone.Collection.extend({
    model: VendaAditivo,
    url: '/api/vendaaditivo'
});

var ModalLancamentosCollection = Backbone.Collection.extend({
    model: VwVendaLancamento,
    url: '/api/vw-venda-lancamento/'
});

var TipoLancamentoCollection = new Backbone.Collection(TiposLancamentos);

/**
 * INSTÂNCIAS DE COLLECTIONS
 */
collectionMeioPagamento = new MeioPagamentoCollection();
collectionCampanha = new CampanhaCollection();
collectionAtendente = new AtendenteCollection();
collectionResponsavelFinanceiro = new ResponsavelFinanceiroCollection();
collectionVwVendaLancamento = new VwVendaLancamentoCollection();
collectionModalLancamento = new ModalLancamentosCollection();
collectionVendaAditivo = new VendaAditivoCollection();

/**
 * INSTÂNCIAS DE MODEL
 */
modelMatricula = new VwMatricula();
modelVendaAditivo = new VendaAditivo();


/**
 * LAYOUT
 */

/**
 * LayoutView do Layout Geral
 * @type {LayoutView}
 */
var LayoutView = Marionette.LayoutView.extend({
    el: '#layout-region',
    autoCompleteComponente: componenteAutoComplete,
    regions: {
        autocomplete: '#autocomplete-region',
        formRegion: '#form-region',
        salasRegion: '#salas-region',
        financeiroRegion: '#financeiro-region',
        botoesGeraisRegion: '#botoes-gerais-region'
    },
    initialize: function () {
        layoutInit = this;

        loading();

        //armazena o autoCompleteComponente
        collectionAutoComplete = this.autoCompleteComponente;

        collectionAutoComplete.main({
            model: VwPesquisarPessoa,
            element: layoutInit.autocomplete.$el
        }, function () {
            layoutInit.model = new Backbone.Model(collectionAutoComplete.getFixModel());
            layoutInit.mostrarForm();
        }, function () {
            layoutInit.esconderForm();
        });

        loaded();
        return this;
    },
    mostrarForm: function () {
        formInit = new FormView();
        this.formRegion.show(formInit);

        thatAutoComplete = collectionAutoComplete.getFixModel();
        this.setResponsavel(thatAutoComplete);
    },
    esconderForm: function () {
        this.formRegion.empty();
        this.salasRegion.empty();
        this.financeiroRegion.empty();
        this.botoesGeraisRegion.empty();
    },
    setResponsavel: function (dataThatAutoComplete) {
        //Instancia a model de responsavel financeiro e seta os valores
        if (dataThatAutoComplete) {
            collectionResponsavelFinanceiro.reset();
            var modeloResponsavel = new ModelResponsavelFinanceiro({
                id: layoutInit.model.get('id'),
                st_cpf: layoutInit.model.get('st_cpf'),
                st_nomecompleto: layoutInit.model.get('st_nomecompleto'),
                id_usuario: layoutInit.model.get('id_usuario')
            });
            //atribui a model na collection de responsaveis financeiros
            collectionResponsavelFinanceiro.add(modeloResponsavel);
        }
    },
    limpaFormularios: function () {
        layoutInit.salasRegion.empty();
        layoutInit.financeiroRegion.empty();
        layoutInit.botoesGeraisRegion.empty();
    }

});

/**
 * ItemView da Informações das Matrículas
 * @type {ItemView}
 */
var InfoMatriculaView = Marionette.ItemView.extend({
    template: '#matricula-view',
    model: modelMatricula
});

/**
 * LayoutView do Form
 * @type {LayoutView}
 */
var FormView = Marionette.LayoutView.extend({
    template: '#form-view',
    model: new Backbone.Model(),
    regions: {
        infoMatriculaRegion: '#matricula-region',
        modalProjetoRegion: '#modal-projeto-region'
    },
    ui: {
        labelMatricula: '#label-matricula',
        inputMatricula: '#id_matricula',
        labelProjeto: '#label-projeto',
        nomeProjeto: '#nome_projeto',
        inputProjeto: '#id_projetopedagogico',
        btnProjeto: '#btn-projeto',
        labelTurma: '#label-turma',
        inputTurma: '#id_turma'
    },
    events: {
        'change @ui.inputMatricula': 'mostrarFormProjeto',
        'change @ui.inputTurma': 'carregarSalas',
        'click @ui.btnProjeto': 'abrirModalProjeto'
    },
    onRender: function () {
        this.carregarMatriculas();
    },
    carregarMatriculas: function () {
        var that = this;

        var id_usuario = layoutInit.model.get('id_usuario');

        if (!id_usuario) {
            this.ui.inputMatricula.html('<option value="">[Selecione um Usuário]</option>');
            return false;
        }

        this.ui.inputMatricula.html('<option value="">Carregando...</option>');

        ComboboxView({
            el: that.ui.inputMatricula,
            url: '/api/vw-matricula/id_evolucao/' + EVOLUCAO.TB_MATRICULA.CURSANDO + '/id_usuario/' + layoutInit.model.get('id_usuario'),
            optionLabelConstruct: function (model) {
                return model.get('id_matricula') + ' - ' + model.get('st_projetopedagogico') + ' - ' + model.get('st_evolucao');
            },
            model: Backbone.Model.extend({
                idAttribute: 'id_matricula',
                defaults: {
                    id_matricula: null,
                    st_matricula: null
                }
            }),
            onChange: function (model) {
                thatIdVenda = model.get('id_venda');
                modelMatricula.clear().set(model.toJSON());
                that.infoMatriculaRegion.show(new InfoMatriculaView());
            }
        });
    },
    mostrarFormProjeto: function () {
        this.ui.labelProjeto.removeClass('hide');
    },
    abrirModalProjeto: function () {
        modalProjetoInit = new ModalProjetoComposite();
        this.modalProjetoRegion.show(modalProjetoInit);
    },
    carregarTurmas: function () {
        var that = this;
        var id_matricula = this.ui.inputMatricula.val();

        if (!id_matricula) {
            this.ui.inputTurma.html('<option value="">[Selecione uma Matrícula]</option>');
            this.ui.labelTurma.addClass('hide');
            return false;
        }

        this.ui.inputTurma.html('<option value="">Carregando...</option>');
        this.ui.labelTurma.removeClass('hide');

        ComboboxView({
            el: that.ui.inputTurma,
            url: '/transfere-projeto/get-vw-turmas-disponives/id_situacao/' + SITUACAO.TB_TURMA.ATIVA + '/id_projetopedagogico/' + formInit.ui.inputProjeto.val(),
            optionLabelConstruct: function (model) {
                return model.get('id_turma') + ' - ' + model.get('st_tituloexibicao');
            },
            model: Backbone.Model.extend({
                idAttribute: 'id_turma',
                defaults: {
                    id_turma: null,
                    st_turma: null
                }
            })
        });
    },
    carregarSalas: function () {
        salasInit = new SalasLayout();
        layoutInit.salasRegion.show(salasInit);
    }
});

/**
 * CompositeView da Modal de Projetos
 * @type {CompositeView}
 */
var ModalProjetoComposite = Marionette.CompositeView.extend({
    template: '#modal-projeto-composite',
    tagName: 'div',
    className: 'modal fade',
    collection: new ModalProjetoCollection(),
    childViewContainer: 'tbody',
    childView: Marionette.ItemView.extend({
        template: '#modal-projeto-view',
        tagName: 'tr'
    }),
    emptyView: Marionette.ItemView.extend({
        tagName: 'tr',
        template: _.template('<td colspan="6">Nenhum projeto encontrado.</td>')
    }),
    ui: {
        form: 'form',
        inputNomeProjeto: '#st_projetopedagogico',
        inputArea: '#id_areaconhecimento',
        inputNivel: '#id_nivelensino',
        inputEntidade: '#id_entidade',
        // MODAL
        header: '.modal-header',
        body: '.modal-body',
        footer: '.modal-footer'
    },
    events: {
        'change input[type="radio"]': 'mostrarBotoes',
        'click #btn-selecionar-projeto': 'selecionarProjeto',
        'submit @ui.form': 'pesquisarProjeto'
    },
    onRender: function () {
        this.carregarAreas();
        this.carregarNiveis();
        this.carregarEntidades();
    },
    onShow: function () {
        this.$el.modal('show');
        this.ui.inputNomeProjeto.focus();
        this.ui.footer.addClass('hide');
    },
    mostrarBotoes: function () {
        this.ui.footer.removeClass('hide');
    },
    selecionarProjeto: function () {
        var $input = this.$el.find('input[name="id_projetopedagogico"]:checked');
        var id_projetopedagogico = $input.val();
        var st_projetopedagogico = $input.closest('tr').find('.st_projetopedagogico').text();

        if (!id_projetopedagogico) {
            $.pnotify({
                type: 'warning',
                title: 'Aviso',
                text: 'Selecione um Projeto Pedagógico'
            });
            return false;
        }

        formInit.ui.btnProjeto.removeClass('btn-info').addClass('btn-danger').html('Trocar Curso <i class="icon-white icon-remove"></i>');

        formInit.ui.inputProjeto.val(id_projetopedagogico);
        formInit.ui.nomeProjeto.html(st_projetopedagogico);

        layoutInit.limpaFormularios();

        formInit.carregarTurmas();

        this.$el.modal('hide');
    },
    pesquisarProjeto: function (e) {
        e.preventDefault();

        var data = Backbone.Syphon.serialize(this, {
            include: ['st_projetopedagogico', 'id_areaconhecimento', 'id_nivelensino', 'id_entidade']
        });

        this.collection.fetch({
            beforeSend: loading,
            complete: loaded,
            data: data,
            type: 'post'
        });
    },
    carregarAreas: function () {
        ComboboxView({
            el: this.ui.inputArea,
            url: '/transfere-projeto/get-areas/',
            collection: arrayArea,
            optionLabel: 'st_areaconhecimento',
            emptyOption: '[Selecione]',
            model: Backbone.Model.extend({
                idAttribute: 'id_areaconhecimento',
                defaults: {
                    id_areaconhecimento: null,
                    st_areaconhecimento: null
                }
            })
        });
    },
    carregarNiveis: function () {
        ComboboxView({
            el: this.ui.inputNivel,
            url: '/transfere-projeto/get-niveis/',
            collection: arrayNivel,
            optionLabel: 'st_nivelensino',
            emptyOption: '[Selecione]',
            model: Backbone.Model.extend({
                idAttribute: 'id_nivelensino',
                defaults: {
                    id_nivelensino: null,
                    st_nivelensino: null
                }
            })
        });
    },
    carregarEntidades: function () {
        ComboboxView({
            el: this.ui.inputEntidade,
            url: '/transfere-projeto/get-entidades/',
            collection: arrayEntidade,
            optionLabel: 'st_nomeentidade',
            model: Backbone.Model.extend({
                idAttribute: 'id_entidade',
                defaults: {
                    id_entidade: null,
                    st_nomeentidade: null
                }
            })
        }, function (el, composite) {
            composite.$el.find('option:first-child').prop('selected', true);
        });
    }
});


/**
 ** SALAS
 */

/**
 * LayoutView das Salas
 * @type {LayoutView}
 */
var SalasLayout = Marionette.LayoutView.extend({
    template: '#salas-template',
    regions: {
        origem: '#origem-region',
        destino: '#destino-region',
        modalSimulacao: '#modal-simulacao-region'
    },
    ui: {
        divOrigemDestino: '#div-origem-destino',
        divContador: '#div-contador',
        contador: '.contador',
        id_disciplina: '.id_disciplina',
        inputTramite: '#st_tramite',
        divBotoesDisciplinas: '#div_botoes_disciplinas'
    },
    events: {
        'click #btn-limpar': 'limparSalas',
        'change .id_disciplina': 'atualizaContador',
        'click #btn-simular': 'simularTransferencia',
        'click #btn-prosseguir': 'prosseguirTransferencia',
        'click .id_disciplina': 'verificaSalaDisponivel',
        'click #btn-salvar-modal': 'efetivaTransferencia',
        'click .btn-toogle-disciplinas': 'toogleFunction'
    },
    onRender: function () {
        salasOrigemInit = new SalasOrigemComposite();
        salasDestinoInit = new SalasDestinoComposite();
        this.origem.show(salasOrigemInit);
        this.destino.show(salasDestinoInit);
    },
    limparSalas: function () {
        this.remove();
        formInit.ui.inputTurma.val('');
    },
    retornarDadosCalcular: function () {
        var data = {
            id_matriculaorigem: modelMatricula.get('id_matricula'),
            id_projetodestino: formInit.ui.inputProjeto.val(),
            id_turmadestino: formInit.ui.inputTurma.val(),
            disciplinas_calcular: [],
            disciplinas_aproveitar: [],
            dataInicioSala: []
        };

        // Buscar disciplinas selecionadas
        salasOrigemInit.collection.each(function (model) {
            if (model.get('selected')) {
                data.disciplinas_aproveitar.push(model.get('id_disciplina'));
            }
            if (model.get('bl_calcular')) {
                data.disciplinas_calcular.push(model.get('id_disciplina'));
            }
        });

        // Buscar data de inicio da sala de aula
        salasOrigemInit.collection.each(function (model) {

            if (model.get('dt_abertura')) {
                data.dataInicioSala.push(converteStringDataBRToUSA(model.get('dt_abertura')));
            }
        });

        return data;
    },
    simularTransferencia: function () {

        loading();

        var that = this;

        var data = that.retornarDadosCalcular();

        $.ajax({
            type: 'post',
            url: '/transfere-projeto/calcular-valor-transferencia/',
            data: data,
            async: true,
            beforeSend: function () {
            },
            success: function (retorno) {
                if (retorno.tipo === 1) {
                    that.abrirModalSimulacao(retorno.mensagem);
                } else {
                    $.pnotify({
                        type: 'error',
                        title: 'Erro',
                        text: 'A simulação da transferência de curso não foi concluída. ' + (retorno.text)
                    });
                }
            },
            error: function () {
                $.pnotify({
                    type: 'error',
                    title: 'Erro',
                    text: 'A simulação da transferência de curso não foi concluída. '
                });
                loaded();
            },
            complete: function () {
                loaded();
            }
        });
    },
    prosseguirTransferencia: function () {
        // Validar se informou o motivo da transferência
        if (!this.ui.inputTramite.val().trim()) {
            $.pnotify({
                type: 'warning',
                title: 'Aviso',
                text: 'Informe o motivo da transferência'
            });
            this.ui.inputTramite.focus();
            return false;
        }
        financeiroInit = new NegociacaoLayout();
        layoutInit.financeiroRegion.show(financeiroInit);

    },
    abrirModalSimulacao: function (retorno) {
        modalSimulacaoInit = new ModalSimulacaoComposite({
            model: new Backbone.Model(retorno)
        });

        this.modalSimulacao.show(modalSimulacaoInit);
    },
    atualizaContador: function () {
        var that = this;

        var nu_selecionadas = salasOrigemInit.collection.filter(function (item) {
            return item.get('selected');
        }).length;

        if (nu_selecionadas) {
            var plural = nu_selecionadas > 1 ? 's' : '';
            that.ui.contador.html('<label><b>' + nu_selecionadas + ' disciplina' + plural + ' selecionada' + plural + ' para aproveitar.</b></label>');
        } else {
            that.ui.contador.empty();
        }
    },
    verificaSalaDisponivel: function () {
        var data = {
            id_projetopedagogico: formInit.ui.inputProjeto.val(),
            id_turma: formInit.ui.inputTurma.val(),
            id_disciplina: []
        };

        salasOrigemInit.collection.each(function (model) {
            if (model.get('selected')) {
                data.id_disciplina.push(model.get('id_disciplina'));
            }
        });
        //se não tiver disciplinas para aproveitamento, retorna true
        if (!data.id_disciplina.length) {
            return true;
        } else {
            var requisicao = $.ajax({
                type: 'post',
                url: '/transfere-projeto/valida-sala-transferencia/',
                data: data,
                async: false,
                beforeSend: loading,
                success: function (mensageiro) {
                    if (mensageiro.tipo !== 1) {
                        $.pnotify({
                            type: mensageiro.type,
                            title: mensageiro.title,
                            text: mensageiro.text
                        });
                    }

                },
                error: function (erro) {
                    $.pnotify({
                        type: 'error',
                        title: 'Erro',
                        text: erro.text
                    })
                },
                complete: function () {
                    loaded();

                }
            });
            if (requisicao.responseJSON.tipo !== 1) {
                return false;
            } else {
                return true;
            }
        }
    },
    toogleFunction: function () {
        this.ui.divOrigemDestino.toggle();
        this.ui.divContador.toggle();
        this.ui.divBotoesDisciplinas.toggle();
    }
});

/**
 * CompositeView das Salas de Origem
 * @type {CompositeView}
 */
var SalasOrigemComposite = Marionette.CompositeView.extend({
    template: '#salas-origem-template',
    collection: new SalasOrigemCollection(),
    childViewContainer: 'tbody',
    childView: Marionette.ItemView.extend({
        template: '#salas-origem-view',
        tagName: 'tr',
        events: {
            'change input.id_disciplina': 'setModel',
            'change input.nu_cargahoraria': 'setNu_cargahoraria'
        },
        setModel: function (e) {
            var $input = $(e.target);
            this.model.set('selected', $input.prop('checked'));
        },
        setNu_cargahoraria: function (e) {
            var $input = $(e.target);
            this.model.set('bl_calcular', $input.prop('checked'));

            //pega a data atual no formato Pt_br
            var dataAtual = new Date();
            var dia = dataAtual.getDate();
            if (dia.toString().length == 1) dia = "0" + dia;
            var mes = dataAtual.getMonth() + 1;
            if (mes.toString().length == 1) mes = "0" + mes;
            var ano = dataAtual.getFullYear();
            var dataAtualPtBr = dia + "/" + mes + "/" + ano;

            //pega a data inicial da sala
            var dataInicioSala = this.model.get('dt_abertura');

            if (dataInicioSala) {
                //calcula a diferença entre as duas datas
                var dateDiff = diferencaEntreDatas(dataInicioSala, dataAtualPtBr);

                //se a data for menor que 7 dias mostra a mensagem
                if (dateDiff <= 7) {//7 dias
                    if ($(e.target).is(':checked') === true) {
                        bootbox.confirm("A sala de aula dessa disciplina tem menos de 7 (sete) dias que se inicou, deseja continuar com a seleção?", function (result) {
                            if (result === false) {
                                $input.attr('checked', false);
                            }
                        });
                    }
                }
            }
        }
    }),
    emptyView: Marionette.ItemView.extend({
        template: _.template('<td colspan="11">Nenhum registro encontrado.</td>'),
        tagName: 'tr'
    }),
    initialize: function () {
        this.collection.fetch({
            data: {
                id_projetopedagogico: modelMatricula.get('id_projetopedagogico'),
                id_projetotransfere: formInit.ui.inputProjeto.val(),
                id_matricula: modelMatricula.get('id_matricula'),
                id_turma: formInit.ui.inputTurma.val(),
                bl_ordenar: true
            },
            beforeSend: loading,
            complete: loaded
        });
    }
});

/**
 * CompositeView das Salas de Destino
 * @type {CompositeView}
 */
var SalasDestinoComposite = Marionette.CompositeView.extend({
    template: '#salas-destino-template',
    collection: new SalasDestinoCollection(),
    childViewContainer: 'tbody',
    childView: Marionette.ItemView.extend({
        template: '#salas-destino-view',
        tagName: 'tr'
    }),
    emptyView: Marionette.ItemView.extend({
        template: _.template('<td colspan="3">Nenhum registro encontrado.</td>'),
        tagName: 'tr'
    }),
    initialize: function () {
        this.collection.fetch({
            data: {
                id_projetopedagogico: modelMatricula.get('id_projetopedagogico'),
                id_projetotransfere: formInit.ui.inputProjeto.val(),
                id_matricula: modelMatricula.get('id_matricula'),
                id_turma: formInit.ui.inputTurma.val()
            },
            beforeSend: loading,
            complete: loaded
        });
    }
});

/**
 *  CompositeView da Modal de informações para imprimir a simulação
 * @type {CompositeView}
 */
var ModalSimulacaoComposite = Marionette.CompositeView.extend({
    template: '#modal-simulacao-template',
    tagName: 'div',
    className: 'modal fade',
    ui: {
        tipo_ressarcimento: '#tipo_ressarcimento',
        div_restituicao: '#div_restituicao',
        div_estorno: '#div_estorno',
        id_banco: '#id_banco',
        nu_agencia: '#nu_agencia',
        nu_conta: '#nu_conta',
        nu_operacao: '#nu_operacao',
        st_observacao: '#st_observacao'
    },
    events: {
        'click #btn-imprimir-simulacao': 'imprimirSimulacao'
    },
    onShow: function () {
        this.$el.modal('show');

        var id_tiporessarcimento = this.model.get("st_tid");
        var nu_valorapagar = this.model.get("nu_valorapagar");

        if (parseFloat(nu_valorapagar) < 0) {
            this.ui.tipo_ressarcimento.removeClass('hide');
            if (id_tiporessarcimento) {
                this.ui.div_restituicao.addClass('hide');
                this.ui.div_estorno.removeClass('hide');
            } else {
                this.ui.div_estorno.addClass('hide');
                this.ui.div_restituicao.removeClass('hide');
            }
        } else if (parseFloat(nu_valorapagar) >= 0) {
            this.ui.tipo_ressarcimento.addClass('hide');
            this.ui.div_restituicao.addClass('hide');
            this.ui.div_estorno.addClass('hide');
        }
    },
    onRender: function () {
        // Carregar combo banco
        var id_tiporessarcimento = this.model.get("st_tid");
        var nu_valorapagar = this.model.get("nu_valorapagar");
        if (parseFloat(nu_valorapagar) < 0 && !id_tiporessarcimento) {
            this.loadComboBanco();
        }
    },
    loadComboBanco: function () {
        var that = this;

        // Carregar combo Banco
        var collectionBanco = new BancoCollection();
        collectionBanco.fetch();
        this.ui.id_banco.html('<option value="">Selecione</option>');

        var BancoSelectView = new SelectView({
            el: that.$el.find('#id_banco'),
            collection: collectionBanco,
            childViewOptions: {
                optionLabel: 'st_nomebanco',
                optionValue: 'id_banco'
            }
        });
        BancoSelectView.render();
    },
    imprimirSimulacao: function () {
        var arrDados = this.model.toJSON();
        arrDados.id_projetotransfere = formInit.ui.inputProjeto.val();
        arrDados.id_turma = formInit.ui.inputTurma.val();
        arrDados.nu_banco = '';
        arrDados.nu_agencia = '';
        arrDados.nu_conta = '';
        arrDados.nu_operacao = '';

        if (arrDados.st_tid == null) {
            if (modalSimulacaoInit.ui.id_banco.val()) arrDados.id_banco = modalSimulacaoInit.ui.id_banco.val();
            if (modalSimulacaoInit.ui.nu_agencia.val()) arrDados.nu_agencia = modalSimulacaoInit.ui.nu_agencia.val();
            if (modalSimulacaoInit.ui.nu_conta.val()) arrDados.nu_conta = modalSimulacaoInit.ui.nu_conta.val();
            if (modalSimulacaoInit.ui.nu_operacao.val()) arrDados.nu_operacao = modalSimulacaoInit.ui.nu_operacao.val();
        }
        arrDados.st_observacao = (modalSimulacaoInit.ui.st_observacao.val()) ? modalSimulacaoInit.ui.st_observacao.val() : '';

        this.$el.modal('hide');
        window.open('/transfere-projeto/imprimir-simulacao/data/' + JSON.stringify(arrDados), '_blank');
    }
});


/**
 ** RESSARCIMENTO
 */

/**
 * ItemView para lista de lançamentos vazio
 * @type {ItemView}
 */
var RessarcimentoItem = Marionette.ItemView.extend({
    template: '#ressarcimento-composite',
    ui: {
        nu_valorapagar: "#nu_valorapagar",
        st_observacao: "#st_observacao",
        div_estorno: '#div_estorno',
        div_restituicao: '#div_restituicao',
        id_banco: "#id_banco"
    },
    onRender: function () {
        salasInit.toogleFunction();
        if (this.model.get('st_tid')) {
            this.ui.div_estorno.removeClass('hide');
            this.ui.div_restituicao.addClass('hide');
        } else {
            this.ui.div_restituicao.removeClass('hide');
            this.ui.div_estorno.addClass('hide');
            this.loadComboBanco();
        }
    },
    loadComboBanco: function () {
        var that = this;

        // Carregar combo Banco
        var collectionBanco = new BancoCollection();
        collectionBanco.fetch();
        this.ui.id_banco.html('<option value="">Selecione</option>');

        var BancoSelectView = new SelectView({
            el: that.$el.find('#id_banco'),
            collection: collectionBanco,
            childViewOptions: {
                optionLabel: 'st_nomebanco',
                optionValue: 'id_banco'
            }
        });
        BancoSelectView.render();
    }
});


/**
 ** NEGOCIAÇÃO FINANCEIRA
 */
var NegociacaoLayout = Marionette.LayoutView.extend({
    template: '#negociacao-template',
    regions: {
        negociacaoRegion: '#negociacao-region',
        ressarcimentoRegion: '#ressarcimento-region'
    },
    onRender: function () {
        loading();
        var that = this;
        var data = salasInit.retornarDadosCalcular();

        $.ajax({
            type: 'post',
            url: '/transfere-projeto/calcular-valor-transferencia/',
            data: data,
            async: true,
            success: function (retorno) {
                if (retorno.tipo === 1) {
                    var calculoValorTransferencia = retorno.mensagem;

                    thatCalculoValorTransferencia = calculoValorTransferencia;
                    thatNuValorAPagar = calculoValorTransferencia.nu_valorapagar;

                    if (parseFloat(thatNuValorAPagar) < 0) {
                        ressarcimentoInit = new RessarcimentoItem({
                            model: new Backbone.Model(calculoValorTransferencia)
                        });
                        that.ressarcimentoRegion
                            .show(ressarcimentoInit);
                    } else {
                        collectionVwVendaLancamento = new VwVendaLancamentoCollection();
                        negociacaoFinanceiraInit = new NegociacaoFinanceiraComposite({
                            model: new Backbone.Model(calculoValorTransferencia),
                            collection: collectionVwVendaLancamento
                        });

                        that.negociacaoRegion.show(negociacaoFinanceiraInit);
                    }
                    salasInit.ui.divBotoesDisciplinas.addClass('hide');

                    botoesGeraisInit = new BotoesGeraisLayout();
                    layoutInit.botoesGeraisRegion.show(botoesGeraisInit);
                } else {
                    $.pnotify({
                        type: 'error',
                        title: 'Erro',
                        text: 'A simulação da transferência de curso não foi concluída. ' + (retorno.text)
                    });
                }

            },
            error: function () {
                $.pnotify({
                    type: 'error',
                    title: 'Erro',
                    text: 'O calculo do novo curso não foi concluído.'
                })
            },
            complete: function () {
                loaded();
            }
        });

    },
    limparNegociacao: function () {
        this.remove();
        formInit.ui.inputTurma.val('');
    },


});


/**
 * ItemView para lista de lançamentos vazio
 * @type {ItemView}
 */
var LancamentosEmptyView = Marionette.ItemView.extend({
    template: "#template-empty-lancamento",
    tagName: 'tr'
});

/**
 * ItemView para Itens de Lançamentos
 * @type {ItemView}
 */
var LancamentoItemView = Marionette.ItemView.extend({
    template: "#template-item-lancamento",
    tagName: 'tr',
    ui: {
        'dt_vencimento': 'input[name="dt_vencimento"]',
        'nu_valor': 'input[name="nu_valor"]',
        'id_responsavel': 'select[name="id_responsavel"]',
        'btn_datepicker': 'a[name="btn_datepicker"]',
        'btn_confirmar': '.btn-confirmar',
        'td_pagamento': '.td-pagamento',
        'btn_remover': '#btn_remover_lancamento_unico',
        'chk_um_lancamento': '.chk_um_lancamento'
    },
    modelEvents: {
        'change:bl_quitado': function () {
            this.render();
        }
    },

    onBeforeRender: function () {
        var nuOrdem = this.model.collection.models.indexOf(this.model) + 1;
        if (nuOrdem <= 0) {
            nuOrdem = 1;
        }
        this.model.set('nu_ordem', nuOrdem);
    },
    initialize: function () {
        var tipo = 2;
        if (this.model.get('bl_entrada')) {
            tipo = 1
        }

        var tipoLancamento = TipoLancamentoCollection.where({id: tipo});
        if (typeof (tipoLancamento[0]) != 'undefined') {
            this.model.set('st_tipolancamento', tipoLancamento[0].get('name'));
        }

        var ordem = this.model.get('nu_ordem') ? parseInt(this.model.get('nu_ordem')) : 1;
        this.model.set('nu_ordem', ordem);

        return this;
    },
    onRender: function () {

        if (this.model.get('st_urlpagamento')) {
            this.ui.td_pagamento.append($('<a href="' + this.model.get('st_urlpagamento') + '" target="_blank" class="btn btn-default" >' + this.model.get('st_meiopagamento') + '</a>'));
        }

        var valor = formatValueToShow(this.model.get('nu_valor'));
        this.ui.nu_valor.maskMoney({
            precision: 2,
            decimal: ",",
            thousands: "."
        }).val(valor).attr('placeholder', this.model.get('nu_valor'));

        // Se o primeiro item da collection com o mesmo numero do cartao for este mesmo itemview, mostrar botao remover.
        // Os comentarios abaixo são devido a mudança na tela, para remover os botões de excluir lançamentos
        var mesmoCartao = this.model.collection.findWhere({nu_cartao: this.model.get('nu_cartao')});
        if (mesmoCartao == this.model || this.model.get('id_meiopagamento') != 1) {
            this.ui.chk_um_lancamento.removeClass('hide');
        }
        else {
            this.ui.chk_um_lancamento.addClass('hide');
        }

        return this;
    },
    removeLancamento: function () {

        var that = this;
        var id_meiopagamento = that.model.get('id_meiopagamento');

        /* Mensagem a ser utilizada no Bootbox.
         Caso cartão, informa sobre remoção múltipla dos lançamentos.
         */

        var msg = "Tem certeza que deseja remover este lançamento?";
        if (id_meiopagamento == 1)
            msg = "Tem certeza que deseja remover todos os lançamentos deste cartão?";

        bootbox.confirm(msg, function (result) {
            if (result) {

                var collection = that.model.collection;

                if (id_meiopagamento == 1) {

                    var nuCartaoAtual = Number(that.model.get('nu_cartao'));
                    var toRemove = [];

                    /* Ao remover uma serie de lançamentos (num mesmo cartao),
                     atualiza os models e as views, decrementando o número
                     do cartao.
                     */

                    collection.each(function (model) {
                        var nuCartao = Number(model.get('nu_cartao'));
                        if (nuCartao == nuCartaoAtual) {
                            model.set('bl_ativo', false);

                            // Lancamentos que terao o bl_ativo setado como false no banco
                            if (model.get('id_lancamento'))
                                lancamentosParaRemover.push(model.get('id_lancamento'));

                            // Lancamentos que serao removidos da collection se nao tiver id
                            toRemove.push(model);

                        } else if (nuCartao > nuCartaoAtual) {
                            model.set('nu_cartao', nuCartao - 1);
                        }
                    });

                    var firstModel = collection.findWhere({bl_ativo: true});
                    if (firstModel)
                        firstModel.set('bl_entrada', 1);
                    collection.remove(toRemove);

                } else {
                    that.model.set('bl_ativo', false);
                    if (that.model.get('id_lancamento'))
                        lancamentosParaRemover.push(that.model.get('id_lancamento'));
                    collection.remove(that.model);
                }

                $.pnotify({title: 'Sucesso!', text: 'Lançamentos removidos com sucesso!', type: 'success'});
            }
        });

    },
    changeValorLancamento: function () {
        var that = this;
        var valor = this.$el.find('input[name="nu_valor"]').val();
        this.model.set('nu_valor', valor);

        thatLancamento.calculaTotais();
    },
    changeDtVencimento: function () {
        var dt_vencimento = this.$el.find('input[name="dt_vencimento"]').val();
        this.model.set('dt_vencimento', dt_vencimento);
    },
    marcaLancamentosParaExcluir: function () {
        var that = this;
        //verifico se o check box esta marcado, se sim, marca a modelo para ser excluida
        if (this.ui.chk_um_lancamento.is(':checked')) {
            this.model.set({excluirLancamento: true});
            //se houver na modelo o atributo nu_cartao, quer dizer que este lançamento faz parte de um cartão
            if (this.model.get('nu_cartao')) {
                //vai marcar todas as modelos que fazem parte deste cartão
                _.each(this.model.collection.models, function (model, i) {
                    if (model.get('nu_cartao') == that.model.get('nu_cartao')) {
                        model.set({excluirLancamento: true});
                    }
                })
            }
        } else {
            //caso não esteja marcado o checkbox, remove o atributo da modelo e se fizer parte de um cartão
            //remove de todas os lançamentos deste cartão
            this.model.unset('excluirLancamento');
            if (this.model.get('nu_cartao')) {
                _.each(this.model.collection.models, function (model, i) {
                    if (model.get('nu_cartao') == that.model.get('nu_cartao')) {
                        model.unset('excluirLancamento');
                    }
                })
            }
        }
    },
    events: {
        'click .btn-danger': 'removeLancamento',
        'blur input[name="nu_valor"]': 'changeValorLancamento',
        'change input[name="dt_vencimento"]': 'changeDtVencimento',
        'change .chk_um_lancamento': 'marcaLancamentosParaExcluir'
    }
});

/**
 * CompositeView da Negociação Financeira
 * @type {CompositeView}
 */
var NegociacaoFinanceiraComposite = Marionette.CompositeView.extend({
    template: '#negociacao-financeira-composite',
    className: 'view-lancamentos',
    childViewContainer: '#lista-lancamentos-venda',
    childView: LancamentoItemView,
    emptyView: LancamentosEmptyView,
    valorFinal: 0,
    valorVenda: 0,
    totalEntrada: 0,
    totalParcela: 0,
    totalLancamento: 0,
    valorRestante: 0,
    idTipoDesconto: 0,
    valorDesconto: 0,
    collectionEvents: {
        remove: function () {
            this.render();
            this.calculaTotais();
            this.ui.nuTotal.val(this.valorRestante);
        }
    },
    modelEvents: {
        'change': 'setValorVenda'
    },
    setValorVenda: function () {
        this.valorVenda = this.model.get('nu_valorapagar');
        this.valorRestante = this.model.get('nu_valorapagar');
        this.render();
        this.calculaTotais();
    },
    ui: {
        nu_valorapagar: "#nu_valorapagar",
        st_observacao: "#st_observacao",
        id_meiopagamento: "#id_meiopagamento",
        id_campanhacomercial: "#id_campanhacomercial",
        tipo_desconto: "#tipo_desconto",
        nu_desconto: "#nu_desconto",
        tipoLancamento: "#tipoLancamento",
        nu_quantidade: "#nu_quantidade",
        id_atendente: "#id_atendente",
        id_responsavel: "#id_responsavel",
        nu_valor: '#nu_valorparcela',
        nuTotal: '#nu_total',
        nuPrazoParcela: '#nu_prazoparcela',
        dtPrimeiraParcela: '#dt_primeiraparcela',
        nuValorEntrada: '#nu_totalentrada',
        nuValorParcela: '#nu_totalparcela',
        nuValorRestante: '#nu_valorrestante',
        nuTotalLancamento: '#nu_totallancamento',
        tipoPrazo: '#tipo_prazoparcela',
        btnDtPicker: '#btn_datepicker',
        btnAddLancamento: '#btn-add-lancamento',
        btnProcurarResponsavel: '#btn-procurar-responsavel',
        chk_all_lancamentos: '#chk_all_lancamentos',
        btn_excluir_lancamentos: '#btn_excluir_lancamentos',
        divMeioPagamento: '#divMeioPagamento',
        divDataPrimeiraParcela: '#divDataPrimeiraParcela',
        divQtdLancamentos: '#divQtdLancamentos',
        divNuValorParcela: '#divNuValorParcela',
        divAtendente: '#divAtendente',
        divNuValorRestante: '#divNuValorRestante',
        divNuTotalLancamento: '#divNuTotalLancamento',
        tooltipCalculos: '#tooltipCalculos'
    },
    initialize: function () {
        thatLancamento = this;
        this.valorFinal = formatValueToReal(this.model.get('nu_valorapagar'));
        return this;
    },
    onShow: function () {
        return this;
    },
    onRender: function () {

        var nu_valorapagar = this.model.get("nu_valorapagar");

        salasInit.toogleFunction();

        this.ui.nu_valorapagar.val(nu_valorapagar);
        this.ui.nu_valor.val(nu_valorapagar);
        this.ui.nuTotal.val(nu_valorapagar);
        this.populaSelectMeioPagamento();
        this.populaSelectCampanha();
        this.populaSelectAtendente();
        this.populaSelectResponsavel();

        this.ui.tooltipCalculos.popover({
            content: function () {
                return _.template($("#popover-calculos-composite").html());
            }
        });

        //se o valor do aditivo for igual a 0 desabilita os campos do lançamento
        if (parseFloat(nu_valorapagar) == 0) {
            this.ui.id_meiopagamento.attr('disabled', 'disabled');
            this.ui.id_campanhacomercial.attr('disabled', 'disabled');
            this.ui.nu_quantidade.attr('disabled', 'disabled');
            this.ui.btnDtPicker.attr('disabled', 'disabled');
            this.ui.dtPrimeiraParcela.removeAttr('style');
            this.ui.id_atendente.attr('disabled', 'disabled');
            this.ui.btnAddLancamento.attr('disabled', 'disabled');
            this.ui.st_observacao.attr('disabled', 'disabled');
            this.ui.btn_excluir_lancamentos.attr('disabled', 'disabled');
            this.ui.chk_all_lancamentos.attr('disabled', 'disabled');
        }

        return this;
    },
    selecionaTodosLancamentos: function () {
        if (this.ui.chk_all_lancamentos.is(':checked')) {
            $('.chk_um_lancamento').prop('checked', true);
            _.each(this.collection.models, function (model, i) {
                model.set({excluirLancamento: true});
            });
        } else {
            $('.chk_um_lancamento').prop('checked', false);
            _.each(this.collection.models, function (model, i) {
                model.unset('excluirLancamento');
            });
        }
    },
    populaSelectMeioPagamento: function () {
        var that = this;

        // Carregar combo Meio de Pagamento
        collectionMeioPagamento.fetch();
        this.ui.id_meiopagamento.html('<option value="">Selecione</option>');

        var MeioPagamentoSelectView = new SelectView({
            el: that.$el.find('#id_meiopagamento'),
            collection: collectionMeioPagamento,
            childViewOptions: {
                optionLabel: 'st_meiopagamento',
                optionValue: 'id_meiopagamento'
            }
        });
        MeioPagamentoSelectView.render();
    },
    populaSelectCampanha: function () {
        var that = this;

        // Carregar combo Campanha
        collectionCampanha.fetch();
        this.ui.id_campanhacomercial.html('<option value="">Selecione</option>');

        var CampanhaSelectView = new SelectView({
            el: that.$el.find('#id_campanhacomercial'),
            collection: collectionCampanha,
            childViewOptions: {
                optionLabel: 'st_campanhacomercial',
                optionValue: 'id_campanhacomercial'
            }
        });
        CampanhaSelectView.render();
    },
    populaSelectAtendente: function () {
        var that = this;

        // Carregar combo Atendente
        collectionAtendente.fetch();
        this.ui.id_atendente.html('<option value="">Selecione</option>');

        var AtendenteSelectView = new SelectView({
            el: that.$el.find('#id_atendente'),
            collection: collectionAtendente,
            childViewOptions: {
                optionLabel: 'st_atendente',
                optionValue: 'id_atendente'
            }
        });
        AtendenteSelectView.render();
    },
    selecionaMeioPagamento: function () {

        var idMeioPagamento = this.ui.id_meiopagamento.val();

        if (idMeioPagamento == MEIO_PAGAMENTO.CARTAO_DEBITO) {
            this.ui.nu_quantidade.val(1).attr('disabled', 'disabled');
        } else {
            this.ui.nu_quantidade.val(1).removeAttr('disabled');
        }
    },
    selecionaCampanhaComercial: function () {
        var nu_desconto = 0;

        var valorapagar = formatValueToReal(this.model.get('nu_valorapagar'));

        var idCampanhaComercial = this.ui.id_campanhacomercial.val();

        if (idCampanhaComercial) {
            collectionCampanha.find(function (item) {
                if (item.get('id_campanhacomercial') == idCampanhaComercial) {
                    nu_desconto = item.get('nu_valordesconto');
                    idTipoDesconto = item.get('id_tipodesconto');
                }
            });
        }

        this.ui.nu_desconto.val(nu_desconto ? nu_desconto : 0);

        if (idTipoDesconto == TIPO_DESCONTO.VALOR) {
            this.ui.tipo_desconto.html('R$');
            this.valorDesconto = parseFloat(nu_desconto);
            this.valorFinal = valorapagar - (this.valorDesconto ? this.valorDesconto : 0 );

        } else if (idTipoDesconto == TIPO_DESCONTO.PORCENTAGEM) {
            this.ui.tipo_desconto.html('%');
            this.valorDesconto = (valorapagar * (parseFloat(nu_desconto) / 100));
            this.valorFinal = valorapagar - this.valorDesconto;
        }

        this.calculaParcelas();

        this.ui.nuTotal.val(number_format(this.valorFinal, 2, ',', '.'));

    },
    populaSelectResponsavel: function () {
        var that = this;
        var responsavel = new SelectView({
            el: that.ui.id_responsavel,
            collection: collectionResponsavelFinanceiro,
            childViewOptions: {
                optionLabel: 'st_nomecompleto',
                optionValue: 'id',
                optionSelected: null
            }
        });
        responsavel.render();
    },
    calculaParcelas: function () {
        //pega o valor da venda
        var valorVenda = this.valorFinal;

        //quantidade de parcelas
        var qtdParcela = this.ui.nu_quantidade.val() ? parseInt(this.ui.nu_quantidade.val()) : 1;

        /*removendo a classe de erro caso o valor seja 0,
         já que o sistema vai colocar o valor 1 após a validação*/

        this.ui.divQtdLancamentos.removeClass('control-group error');

        //Valor total
        var valorParcela = 0;

        if (!this.verificaValorVenda()) {
            return false;
        }

        if (qtdParcela) {
            //calcula
            valorParcela = valorVenda / qtdParcela;

            //valida se a quantidade de parcela é maior que 1 e se o valor é menor que 100,00
            if (qtdParcela > 1 && valorParcela < 100) {
                this.ui.divNuValorParcela.addClass('control-group error');
                $.pnotify({
                    title: 'Atenção!',
                    text: 'O valor da parcela não pode ser menor que R$ 100,00.',
                    type: 'warning'
                });
            } else {
                this.ui.divNuValorParcela.removeClass('control-group error');
            }
            //coloca o valor no input
            this.ui.nu_valor.val(formatValueToShow(valorParcela.toFixed(2)));
            this.ui.divQtdLancamentos.removeClass('control-group error');
            return valorParcela;//retorna o valor
        } else {
            this.ui.divQtdLancamentos.addClass('control-group error');
            $.pnotify({
                title: 'Atenção!',
                text: 'O número de parcelas não pode ser igual a 0 (zero).',
                type: 'warning'
            });
            return false;
        }

    },
    verificaValorVenda: function () {
        if (this.model.get('nu_valorapagar')) {
            return true;
        } else {
            $.pnotify({title: 'Atenção!', text: 'Valor da venda não definido.', type: 'warning'});
            return false;
        }
    },
    adicionaLancamento: function () {

        var nu_valorapagar = this.ui.nu_valorapagar.val();
        var id_meiopagamento = this.ui.id_meiopagamento.val();
        var st_meiopagamento = this.ui.id_meiopagamento.find('option:selected').text();
        var id_campanhacomercial = this.ui.id_campanhacomercial.val();
        var nu_valordesconto = number_format(this.valorDesconto, 2, ',', '.');
        var id_tipolancamento = this.ui.tipoLancamento.val();
        var nu_parcelas = parseInt(this.ui.nu_quantidade.val());
        var id_atendente = this.ui.id_atendente.val();
        var nu_total = this.ui.nuTotal.val();
        var st_observacao = this.ui.st_observacao.val();
        var st_nomecompleto = this.ui.id_responsavel.find('option:selected').text();
        var id_usuariolancamento = this.ui.id_responsavel.val();
        var dt_vencimento = this.ui.dtPrimeiraParcela.val();
        var nuPrazoParcela = 1;//prazo pré-fixado na regra de negócio
        var tipoPrazo = 'meses';//prazo pré-fixado na regra de negócio
        var nu_valor = formatValueToReal(this.ui.nu_valor.val());
        nu_valor = parseFloat(nu_valor);
        nu_valor = nu_valor.toFixed(2);

        //limite máximo de lançamentos é 24
        var nu_maximoparcela = 24;
        if ((this.collection.length + nu_parcelas) > nu_maximoparcela) {
            $.pnotify({
                title: 'Atenção!',
                text: 'O número máximo de lançamentos para esta negociação é ' + nu_maximoparcela + '.',
                type: 'warning'
            });
            return false;
        }

        //valida campos obrigatórios
        if (this.validaSave(id_meiopagamento, dt_vencimento, id_atendente)) {

            var dt_object = new Date(dt_vencimento.split('/').reverse());

            /* nu_cartao armazena o maior número do cartão */
            var nu_cartao = 0;
            if (id_meiopagamento == 1) {
                $.each(this.collection.models, function (index, obj) {
                    var value = Number(obj.get('nu_cartao'));
                    if (value > nu_cartao)
                        nu_cartao = value;
                });
                nu_cartao++;
            } else {
                nu_cartao = null;
            }

            for (var i = 0; i < nu_parcelas; i++) {

                //pega a quantidade de elementos na collection
                var nu_ordem = this.collection.length;

                //REAJUSTA A DATA DE VENCIMENTO
                dt_vencimento = dt_object.toISOString().substr(0, 10).split('-').reverse().join('/');

                //a regra informa que sempre sera "Parcela"
                var st_tipolancamento = 'Parcela';

                //instancia a VwVendaLancamento para cada lançamento
                var modelVendaLancamento = new VwVendaLancamento();

                //seta os valores no modelo
                modelVendaLancamento.set({
                    id_meiopagamento: id_meiopagamento,
                    st_meiopagamento: st_meiopagamento,
                    bl_entrada: false,
                    nu_parcelas: nu_parcelas,//quantidade de parcelas
                    nu_valor: nu_valor,//valor da parcela
                    dt_vencimento: dt_vencimento,
                    nu_ordem: ++nu_ordem,
                    st_nomecompleto: st_nomecompleto,
                    st_tipolancamento: st_tipolancamento,
                    id_tipolancamento: id_tipolancamento,
                    nu_cartao: nu_cartao,
                    bl_ativo: 1,
                    id_usuariocadastro: id_atendente
                });

                var responsavel = collectionResponsavelFinanceiro.where({id: parseInt(id_usuariolancamento)});
                if (responsavel[0]) {
                    if (responsavel[0].get('st_tiporesponsavel') == 'Fisica') {
                        modelVendaLancamento.set('id_usuariolancamento', id_usuariolancamento);
                    } else {
                        modelVendaLancamento.set('id_entidadelancamento', id_usuariolancamento);
                    }
                }

                //adiciona a collection pai todos os lançamento
                this.collection.add(modelVendaLancamento);

                // Calcular quantos dias sera incrementado em cada parcela
                //Incrementa os dias na data de vencimento, para a proxima parcela
                switch (tipoPrazo) {
                    case 'dias':
                        dt_object.setDate(dt_object.getDate() + nuPrazoParcela);
                        break;
                    case 'meses':
                        dt_object.setMonth(dt_object.getMonth() + nuPrazoParcela);
                        break;
                    default:
                        break;
                }
            }

            //formata a modelo de VendaAditivo antes de setar os valores
            modelVendaAditivo.clear();

            //seta os valores para tb_vendaAditivo
            modelVendaAditivo.set({
                dt_cadastro: dateToPtBr(new Date()),
                nu_valorliquido: formatValueToReal(thatCalculoValorTransferencia.nu_valorcursoorigem),
                dt_primeiraparcela: dt_vencimento,
                id_atendente: id_atendente,
                id_campanhacomercial: id_campanhacomercial,
                id_meiopagamento: id_meiopagamento,
                id_usuario: thatAutoComplete.id_usuario,
                id_venda: thatCalculoValorTransferencia.id_venda,
                nu_desconto: formatValueToReal(nu_valordesconto),
                nu_quantidadeparcelas: nu_parcelas,
                nu_valorapagar: formatValueToReal(nu_valorapagar),
                nu_valortransferencia: formatValueToReal(nu_total),
                nu_valorproporcional: formatValueToReal(thatCalculoValorTransferencia.nu_valorproporcional),
                st_observacao: st_observacao
            });

            this.calculaTotais();

        } else {
            return false;
        }
    },
    calculaTotais: function () {
        //cria as variaveis
        //pega o valor da venda
        var that = this;
        var valorVenda = parseFloat(that.valorFinal.toFixed(2));
        var totalLancamento = 0;
        var totalEntrada = 0;
        var totalParcela = 0;
        var valorRestante = 0;

        //percorre a collection
        if (this.collection.length) {
            this.collection.each(function (model, i) {
                //atribui o valor vindo da model na variavel

                var valor = formatValueToReal(model.get('nu_valor'));

                //incrementa em total de lancamentos
                totalLancamento += valor;

                //verifica se o registro atual é uma entrada e vai incrementando os respectivos lancamentos
                if (model.get('bl_entrada')) {
                    totalEntrada += valor;
                } else {
                    totalParcela += valor;
                }
            });
        }

        totalLancamento = parseFloat(totalLancamento.toFixed(2));
        //pega o total de lancamentos subtrai do total da venda para ter o restante
        valorRestante = valorVenda - totalLancamento;

        //verifica se o valor da venda é maior que o dos lancamentos
        if (valorVenda != totalLancamento) {
            this.ui.id_meiopagamento.val('').removeAttr('disabled');
            this.ui.id_campanhacomercial.val('').removeAttr('disabled');
            this.ui.nu_desconto.val('').attr('disabled', 'disabled');
            this.ui.nu_quantidade.val(1).removeAttr('disabled');
            this.ui.nu_valor.val('').attr('disabled', 'disabled');
            this.ui.dtPrimeiraParcela.val('').attr('disabled', 'disabled');
            this.ui.id_atendente.val('').removeAttr('disabled');
            this.ui.id_responsavel.val('').attr('disabled', 'disabled');
            this.populaSelectResponsavel();
            this.ui.btnDtPicker.removeAttr('disabled');
            this.ui.btnAddLancamento.removeAttr('disabled');
            this.ui.divNuTotalLancamento.removeClass('success').addClass('error');

        } else if (valorVenda == totalLancamento.toFixed(2)) {
            this.ui.nu_valorapagar.val('').attr('disabled', 'disabled');
            this.ui.id_meiopagamento.val('').attr('disabled', 'disabled');
            this.ui.id_campanhacomercial.val('').attr('disabled', 'disabled');
            this.ui.nu_desconto.val('').attr('disabled', 'disabled');
            this.ui.nu_quantidade.val('').attr('disabled', 'disabled');
            this.ui.nu_valor.val('').attr('disabled', 'disabled');
            this.ui.nuTotal.val('').attr('disabled', 'disabled');
            this.ui.dtPrimeiraParcela.val('').attr('disabled', 'disabled');
            this.ui.dtPrimeiraParcela.attr('style', 'background-color: #eeeeee;');
            this.ui.id_atendente.val('').attr('disabled', 'disabled');
            this.ui.id_responsavel.val('').attr('disabled', 'disabled');
            this.ui.btnDtPicker.attr('disabled', 'disabled');
            this.ui.btnAddLancamento.attr('disabled', 'disabled');
            this.ui.divNuTotalLancamento.removeClass('error').addClass('success');
        } else {
            this.ui.divNuTotalLancamento.removeClass('error').removeClass('success');
        }

        if (parseFloat((valorRestante < 0 ? valorRestante * -1 : valorRestante)).toFixed(2) != 0) {
            this.ui.divNuValorRestante.removeClass('success').addClass('error');
        } else {
            this.ui.divNuValorRestante.removeClass('error').addClass('success');
        }


        that.totalLancamento = formatValueToShow(totalLancamento.toFixed(2));
        that.totalEntrada = formatValueToShow(totalEntrada.toFixed(2));
        that.totalParcela = formatValueToShow(totalParcela.toFixed(2));
        that.valorRestante = formatValueToShow(valorRestante.toFixed(2));


        //seta os valores nos inputs
        this.ui.nuValorRestante.val(that.valorRestante);
        this.ui.nuValorEntrada.val(that.totalEntrada);
        this.ui.nuValorParcela.val(that.totalParcela);
        this.ui.nuTotalLancamento.val(that.totalLancamento);

    },
    excluirLancamentosSelecionados: function () {
        var that = this;
        var modelsExcluir = this.collection.where({excluirLancamento: true});
        if (modelsExcluir.length) {
            bootbox.confirm("Deseja remover os lançamentos selecionados?", function (result) {
                if (result) {

                    that.collection.remove(modelsExcluir);
                    $.pnotify({title: 'Sucesso!', text: 'Lançamento(s) removido(s) com sucesso!', type: 'success'});
                }
            });
        } else {
            $.pnotify({title: 'Alerta!', text: 'Nenhum lançamento selecionado!', type: 'alert'});
        }

    },
    validaSave: function (id_meiopagamento, dtPrimeiraParcela, id_atendente) {

        var flag_meio = true;
        var flag_date = true;

        //Valida o meio de pagamento
        if (!id_meiopagamento) {
            this.ui.divMeioPagamento.addClass('control-group error');
            flag_meio = false;
        } else {
            this.ui.divMeioPagamento.removeClass('control-group error');
        }


        //valida se a data da primeira parcela foi preenchida
        if (!dtPrimeiraParcela) {
            this.ui.divDataPrimeiraParcela.addClass('control-group error');
            flag_date = false;
        } else {
            var dtAtual = dateToPtBr(new Date());

            //valida a dt de vencimento para a primeira parcela caso boleto
            if (diferencaEntreDatas(dtPrimeiraParcela, dtAtual) == 0 && id_meiopagamento == 2) {//2 == boleto

                this.ui.divDataPrimeiraParcela.addClass('control-group error');

                $.pnotify({
                    title: 'Atenção!',
                    text: 'A data para a primeira parcela do boleto não pode ser a data atual.',
                    type: 'warning'
                });

                return false;
            }

            this.ui.divDataPrimeiraParcela.removeClass('control-group error');
        }

        var flag_atendente = true;
        //Valida o atendente
        if (!id_atendente) {
            this.ui.divAtendente.addClass('control-group error');
            flag_atendente = false;
        } else {
            this.ui.divAtendente.removeClass('control-group error');
        }


        var flag_valor_menor = true;
        //verifica se o valor da parcela é menor que R$100,00
        if (parseInt(this.ui.nu_quantidade.val()) > 1 && formatValueToReal(this.ui.nu_valor.val()) < 100) {
            this.ui.divNuValorParcela.addClass('control-group error');
            $.pnotify({
                title: 'Atenção!',
                text: 'O valor da parcela não pode ser menor que R$ 100,00.',
                type: 'warning'
            });
            flag_valor_menor = false;
        } else {
            this.ui.divNuValorParcela.removeClass('control-group error');
        }

        if (!flag_valor_menor || !flag_atendente || !flag_meio || !flag_date) {
            $.pnotify({
                title: 'Atenção!',
                text: 'Verifique os campos obrigatórios.',
                type: 'warning'
            });
            return false;
        }

        return true;
    },
    events: {
        'change #id_meiopagamento': 'selecionaMeioPagamento',
        'change #id_campanhacomercial': 'selecionaCampanhaComercial',
        'change input#nu_quantidade': 'calculaParcelas',
        'blur input#nu_quantidade': 'calculaParcelas',
        'click #btn-add-lancamento': 'adicionaLancamento',
        'click input#chk_all_lancamentos': 'selecionaTodosLancamentos',
        'click #btn_excluir_lancamentos': 'excluirLancamentosSelecionados'
    }
});


/**
 * BOTOES GERAIS
 */

/**
 * CompositeView da Botões Gerais
 * @type {LayoutView}
 */
var BotoesGeraisLayout = Marionette.LayoutView.extend({
    template: '#botoes-gerais-layout',
    regions: {
        modalGrade: '#modal-grade-region',
        modalLancamentos: '#modal-lancamentos-region'
    },
    ui: {
        btn_efetivar_transferencia: '#btn-efetivar-transferencia',
        btn_modal_lancamentos: '#btn-modal-lancamentos'
    },
    events: {
        'click #btn-efetivar-transferencia': 'abrirModalSalvar',
        'click #btn-modal-lancamentos': 'abrirModalLancamentos'
    },
    abrirModalSalvar: function () {

        //verifica se as disciplinas selecionadas para aproveitamento, tem sala disponivel
        if (!salasInit.verificaSalaDisponivel()) {
            return false;
        }

        var salas_aproveitar = salasOrigemInit.collection.filter(function (item) {
            return item.get('selected');
        });

        var salas_manter = salasDestinoInit.collection.filter(function (item) {
            return !salas_aproveitar.find(function (aproveitar) {
                return aproveitar.get('id_disciplina') == item.get('id_disciplina');
            });
        });

        var salas = salas_aproveitar.concat(salas_manter);

        modalGradeInit = new ModalGradeComposite({
            collection: new Backbone.Collection(salas)
        });

        this.modalGrade.show(modalGradeInit);
    },
    abrirModalLancamentos: function () {

        var that = this;
        collectionModalLancamento.fetch({
            data: {
                id_venda: thatIdVenda
            },
            beforeSend: loading,
            success: function (collection) {
                //Percorre a collection e formata o valor e as datas
                collection.each(function (model, i) {
                    /*
                     * Trata a data retornada da model, de um objeto para string PT-BR.
                     *  * Não usei nenhuma função porque nenhuma funcionou nesse caso.
                     */
                    var dtQuitado = (model.get('dt_quitado'));
                    var dtQuitadoPtBr = '';
                    if (dtQuitado) {
                        var dtQuitadoDate = dtQuitado.date;
                        var dtQuitadoSoDate = dtQuitadoDate.split(' ');
                        var dtQuitadoDateSplit = dtQuitadoSoDate[0].split('-');
                        dtQuitadoPtBr = dtQuitadoDateSplit[2] + "/" + dtQuitadoDateSplit[1] + "/" + dtQuitadoDateSplit[0];
                    }

                    // Trata o valor quitado retornado da model
                    var nuQuitado = model.get('nu_quitado') ? number_format(model.get('nu_quitado'), 2, ',', '.') : '';

                    // Trata o valor do lançamento retornado da model
                    var nuValor = model.get('nu_valor') ? number_format(model.get('nu_valor'), 2, ',', '.') : '';

                    model.set('nu_valorliquido', nuValor);
                    model.set('dt_quitado', dtQuitadoPtBr);
                    model.set('nu_quitado', nuQuitado)
                });
                modalLancamentosInit = new ModalLancamentosComposite({
                    collection: collectionModalLancamento
                });
                that.modalLancamentos.show(modalLancamentosInit);
                loaded();
            }
        });

    }
});

/**
 * CompositeView da Modal Grade Confirmação
 * @type {CompositeView}
 */
var ModalGradeComposite = Marionette.CompositeView.extend({
    model: new Backbone.Model,
    template: '#modal-grade-template',
    tagName: 'div',
    className: 'modal fade',
    childViewContainer: 'tbody',
    childView: Marionette.ItemView.extend({
        template: '#modal-grade-view',
        tagName: 'tr',
        onRender: function () {
            if (this.model.get('selected')) {
                this.$el.addClass('success text-success');
            }
        }
    }),
    events: {
        'click #btn-salvar-transferencia': 'efetivaTransferencia'
    },
    onShow: function () {
        this.$el.modal('show');
    },
    onBeforeRender: function () {
        var contador = this.collection.filter(function (item) {
            return item.get('selected');
        });
        this.model.set('nu_contador', contador.length);

        //ordena a lista pela dta de abertura
        this.collection.models.sort(function (a, b) {
            var dateA = converteStringData(a.attributes.dt_abertura, 1);
            var dateB = converteStringData(b.attributes.dt_abertura, 1);
            if (dateA.valueOf() < dateB.valueOf())
                return -1;
            if (dateA.valueOf() > dateB.valueOf())
                return 1;
            return 0;
        })
    },
    validaDados: function () {
        var valid = true;

        return valid;
    },
    verificaLancamentos: function () {
        var flag = true;

        //verifica se tem lançamento
        if (!collectionVwVendaLancamento.length && parseFloat(thatNuValorAPagar) > 0) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Informe os lançamentos!'
            });
            flag = false;
        } else if (collectionVwVendaLancamento.length && parseFloat(thatNuValorAPagar) > 0) {
            collectionVwVendaLancamento.each(function (model, i) {
                if (model.get('nu_valor') == 0) {
                    $.pnotify({
                        type: 'warning',
                        title: 'Atençao!',
                        text: 'Remova os lançamentos com valor zero.'
                    });
                    flag = false;
                }
            });
        }
        return flag;
    },
    efetivaTransferencia: function () {
        if (this.validaDados()) {

            //Monta um objeto com os parametros
            var data = {
                id_matriculaorigem: modelMatricula.get('id_matricula'),
                id_projetodestino: formInit.ui.inputProjeto.val(),
                id_turma: formInit.ui.inputTurma.val(),
                st_tramite: salasInit.ui.inputTramite.val().trim(),
                disciplinas_aproveitar: [],
                disciplinas_calcular: [],
                pessoa: thatAutoComplete,
                lancamentos: [],
                venda_aditivo: null
            };

            if (parseFloat(thatNuValorAPagar) < 0) {

                //formata a modelo de VendaAditivo antes de setar os valores
                modelVendaAditivo.clear();

                //seta os valores para tb_vendaAditivo
                modelVendaAditivo.set({
                    nu_valorapagar: parseFloat(thatNuValorAPagar),
                    st_observacao: ressarcimentoInit.ui.st_observacao.val(),
                    nu_valorproporcional: formatValueToReal(thatCalculoValorTransferencia.nu_valorproporcional),
                });

                //Monta um objeto com os parametros
                data.venda_aditivo = modelVendaAditivo.toJSON();

            } else if (parseFloat(thatNuValorAPagar) > 0) {
                if (this.verificaLancamentos()) {

                    //Percorre a collection e formata o valor
                    collectionVwVendaLancamento.each(function (model, i) {
                        model.set('nu_valor', formatValueToReal(model.get('nu_valor')));
                    });

                    data.lancamentos = collectionVwVendaLancamento.toJSON();
                    data.venda_aditivo = modelVendaAditivo.toJSON();

                    // Buscar disciplinas selecionadas
                    salasOrigemInit.collection.each(function (model) {
                        if (model.get('selected')) {
                            data.disciplinas_aproveitar.push(model.get('id_disciplina'));
                        }
                        if (model.get('bl_calcular')) {
                            data.disciplinas_calcular.push(model.get('id_disciplina'));
                        }
                    });

                } else {
                    return false;
                }
            } else {// se o valor for igual a 0

                // Buscar disciplinas selecionadas
                salasOrigemInit.collection.each(function (model) {
                    if (model.get('selected')) {
                        data.disciplinas_aproveitar.push(model.get('id_disciplina'));
                    }
                    if (model.get('bl_calcular')) {
                        data.disciplinas_calcular.push(model.get('id_disciplina'));
                    }
                });

                //formata a modelo de VendaAditivo antes de setar os valores
                modelVendaAditivo.clear();

                //seta os valores para tb_vendaAditivo
                modelVendaAditivo.set({
                    dt_cadastro: dateToPtBr(new Date()),
                    nu_valorliquido: formatValueToReal(thatCalculoValorTransferencia.nu_valorcursoorigem),
                    id_usuario: thatAutoComplete.id_usuario,
                    nu_valorapagar: parseFloat(thatNuValorAPagar),
                    st_observacao: '',
                    nu_valorproporcional: formatValueToReal(thatCalculoValorTransferencia.nu_valorproporcional),
                });
                //Monta um objeto com os parametros
                data.venda_aditivo = modelVendaAditivo.toJSON();

            }

            $.ajax({
                dataType: 'json',
                type: 'post',
                url: '/transfere-projeto/salvar-transferencia',
                data: data,
                beforeSend: function () {
                    loading();
                },
                success: function () {
                    $.pnotify({
                        type: 'success',
                        title: 'Sucesso',
                        text: 'A transferência de curso foi concluída.'
                    });

                    layoutInit.limpaFormularios();
                    layoutInit.mostrarForm();
                },
                error: function () {
                    $.pnotify({
                        type: 'error',
                        title: 'Erro',
                        text: 'A transferência de curso não foi concluída.'
                    })
                },
                complete: function () {
                    loaded();
                }
            });

        } else {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Verifique os campos obrigatórios!'
            });
        }
        return false;
    }
});

/**
 * CompositeView da Modal Grade Confirmação
 * @type {CompositeView}
 */
var ModalLancamentosComposite = Marionette.CompositeView.extend({
    template: '#modal-lancamentos-composite',
    tagName: 'div',
    className: 'modal fade',
    collection: collectionModalLancamento,
    childViewContainer: 'tbody',
    childView: Marionette.ItemView.extend({
        template: '#modal-lancamentos-view',
        tagName: 'tr'
    }),
    emptyView: Marionette.ItemView.extend({
        tagName: 'tr',
        template: _.template('<td colspan="7">Nenhum lancamento encontrado.</td>')
    }),
    initialize: function () {

    },
    onShow: function () {
        this.$el.modal('show');
    }
});


layoutInit = new LayoutView();
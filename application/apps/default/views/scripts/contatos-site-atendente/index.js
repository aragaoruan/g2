/**
 * Arquivos Js para funcionalidade Contatos Site
 * @author Elcio Guimarães <elcioguimaraes@gmail.com>
 * @since 2017-11-07
 */

var Prevenda = Backbone.Model.extend({});
/**
 * Collection to Contatos Site
 * @type Backbone Collection
 * @exp;Backbone
 * @pro;Collection
 * @call;extend
 */
var pageableContatos = new Backbone.Collection.extend({
    model: Prevenda
});

var pageableGrid = null;

/**
 * View to render template Filter
 * @type Backbone View
 * @exp;Backbone
 * @pro;View
 * @call;extend
 */
var FiltroView = Backbone.View.extend({
    tagname: "div",
    className: "span12",
    initialize: function () {
        this.stopListening();
        _.bindAll(this, "render");
        this.render = _.bind(this.render, this);
    },
    render: function () {
        var elTemplate = _.template($("#template-filtro-contatos-site-atendente").html(), {});
        this.el.innerHTML = elTemplate;
        setDatePickerMax();
        return this;
    },
    submitForm: function () {
        //Submit form filter
        //Render grid result
        this.renderGrid({
            elementGrid: $("#containerGrid")
        });
        $("#gerarXls").prop("disabled", false);
        return this;
    },
    renderGrid: function (options) {
        loading();
        $(options.elementGrid).html($("#template-grid-contatos-site-atendente").html());
        $("#tblGrid").empty();

        var params = "dt_inicio=" + $("#dt_inicio").val() +
            "&dt_fim=" + $("#dt_fim").val() +
            "&id_situacao=" + $("#id_situacao option:selected").val() +
            "&busca=atendente" +
            "&st_nomecompleto=" + $("#st_nomecompleto").val() +
            "&id_atendente=" + G2S.loggedUser.get("id_usuario");

        var PageableContatos = Backbone.PageableCollection.extend({
            model: Prevenda,
            url: "api/contatos-site?" + params,
            state: {
                pageSize: 20
            },
            mode: "client" // page entirely on the client side
        });

        var RowGrid = Backgrid.Row.extend({
            initialize: function () {
                RowGrid.__super__.initialize.apply(this, arguments);
            },
            habilitarBotoes: function () {
                if (!pageableGrid.getSelectedModels().length) {
                    $("button.btn-success, button.btn-danger").attr("disabled", "disabled");
                } else {
                    if (this.model.get("id_situacao") === 125) {
                        $("button.btn-danger").removeAttr("disabled");
                    } else if (this.model.get("id_situacao") === 126) {
                        $("button.btn-success").removeAttr("disabled");
                    } else {
                        $("button.btn-success, button.btn-danger").removeAttr("disabled");
                    }
                }
            },
            events: {
                "change input[type=checkbox]": "habilitarBotoes"
            }
        });

        pageableContatos = new PageableContatos();

        pageableGrid = new Backgrid.Grid({
            row: RowGrid,
            columns: [{
                name: "id_prevendaproduto",
                label: "ID",
                editable: false,
                cell: "string"
            }, {
                name: "st_nomecompleto",
                label: "Nome",
                editable: false,
                cell: "string"
            }, {
                name: "st_email",
                label: "E-mail",
                editable: false,
                cell: "string"
            }, {
                name: "nu_telefoneresidencial",
                label: "Telefone Residencial",
                editable: false,
                cell: "string"
            }, {
                name: "nu_telefonecelular",
                label: "Telefone Celular",
                editable: false,
                cell: "string"
            }, {
                name: "dt_cadastro",
                label: "Data Abertura",
                editable: false,
                cell: "string"
            }, {
                name: "dt_ultimainteracao",
                label: "Data da Última Interação",
                editable: false,
                cell: "string"
            }, {
                name: "st_produto",
                label: "Curso",
                editable: false,
                cell: "string"
            }, {
                name: "st_situacao",
                label: "Situação",
                editable: false,
                cell: "string"
            }, {
                name: "",
                label: "Registrar Atendimento",
                editable: false,
                cell: Backgrid.Cell.extend({
                    className: "custom_class",
                    render: function () {
                        var idVendaProduto = this.model.get("id_prevendaproduto");
                        var stNomeCompleto = this.model.get("st_nomecompleto");
                        var stProduto = this.model.get("st_produto");
                        var idSituacao = this.model.get("id_situacao");
                        this.$el.html(
                            "<button id='btn-interacao' data-id='" + idVendaProduto + "' data-id_situacao='208'" +
                            " class='btn btn-small btn-default btn-salva-e-muda-interacao' href='#'" +
                            " data-st_nomecompleto='" + stNomeCompleto + "' data-st_produto='" + stProduto + "'" +
                            " data-id_situacaocorrente='" + idSituacao + "'>" +
                            "Registrar" +
                            "</button>");
                        return this;
                    }
                }),
            }, {
                name: "",
                label: "Detalhar Interação",
                editable: false,
                cell: Backgrid.Cell.extend({
                    className: "custom_class",
                    render: function () {
                        var idPreVenda = this.model.get("id_prevenda");
                        this.$el.html(
                            "<button data-id='" + idPreVenda + "' " +
                            " class='btn btn-small btn-primary btn-visualizar' href='#'>" +
                            "Visualizar" +
                            "</button>");
                        return this;
                    }
                }),
            }, {
                name: "",
                label: "Ações",
                editable: false,
                cell: Backgrid.Cell.extend({
                    className: "custom_class",
                    render: function () {
                        var idPreVendaProduto = this.model.get("id_prevendaproduto");
                        var stNomeCompleto = this.model.get("st_nomecompleto");
                        var stProduto = this.model.get("st_produto");
                        this.$el.html(
                            "<div style='width: 170px'>" +
                            "<button data-id='" + idPreVendaProduto + "' data-id_situacao='126'" +
                            " data-st_nomecompleto='" + stNomeCompleto + "' data-st_produto='" + stProduto + "'" +
                            " class='btn btn-small btn-danger btn-salva-e-muda-interacao' href='#'> " +
                            "Descartar" +
                            "</button>&nbsp;&nbsp;" +
                            "<button data-id='" + idPreVendaProduto + "' data-id_situacao='125'" +
                            " data-st_nomecompleto='" + stNomeCompleto + "' data-st_produto='" + stProduto + "' " +
                            " class='btn btn-small btn-success btn-salva-e-muda-interacao' href='#'> " +
                            "Finalizar" +
                            "</button>" +
                            "</div>"
                        );

                        return this;
                    }
                }),
            }],
            collection: pageableContatos,
            className: "backgrid backgrid-selectall table table-striped table-bordered table-hover"
        });

        // Render the grid and attach the root to your HTML document
        $("#tblGrid").append(pageableGrid.render().el);

        // Initialize the paginator
        var paginator = new Backgrid.Extension.Paginator({
            collection: pageableContatos
        });

        // Render the paginator
        $("#tblGrid").after(paginator.render().el);

        // Fetch some countries from the url
        pageableContatos.fetch({
            reset: true,
            success: function () {
                loaded();
            }
        });
        return this;
    },
    salvarInteracao: function (event) {
        var self = this;

        var title = "Informe o motivo do descarte";
        var button = "Descartar";

        switch ($(event.currentTarget).data("id_situacao")) {

            case 125:
                title = "Informe a interação para finalizar";
                button = "Salvar";
                break;
            case 208:
                title = "Informe a interação para o Atendimento em negociação";
                button = "Salvar";
                break;
            default:

        }

        bootbox.dialog({
            title: title,
            closeButton: true,
            message: "<div align='left'>" +
            "<p><strong>Nome do Aluno:</strong>" + $(event.currentTarget).data("st_nomecompleto") + "</p>" +
            "<p><strong>Nome do Curso:</strong>" + $(event.currentTarget).data("st_produto") + "</p>" +
            "<textarea id='tarea' rows='10' style='margin: 0px 0px 10px; width: 97%; height: 149px;' >" +
            "</textarea>" +
            "</div>",
            buttons: {
                cancel: {
                    label: "Cancelar",
                    className: "btn-warning"
                },
                success: {
                    label: button,
                    className: "btn-danger",
                    callback: function () {
                        var result = document.getElementById("tarea").value;

                        if (!result) {
                            $.pnotify({
                                title: "Aviso",
                                text: title,
                                type: "warning"
                            });
                            return;
                        }

                        var id_situacao = $(event.currentTarget).data("id_situacao");

                        if (id_situacao === 208 &&
                            $(event.currentTarget).data("id_situacaocorrente") !== 209) {
                            id_situacao = $(event.currentTarget).data("id_situacaocorrente");
                        }

                        $.ajax({
                            dataType: "json",
                            type: "post",
                            url: "/contatos-site/save-discarded-messages",
                            data: {
                                "st_descarte": result,
                                "modelos": [$(event.currentTarget).data("id")],
                                "id_situacao": id_situacao
                            },
                            beforeSend: function () {
                                loading();
                            },
                            success: function (data) {
                                //Render grid result
                                self.renderGrid({
                                    elementGrid: $("#containerGrid")
                                });
                                loaded();
                                $.pnotify({title: data.title, text: data.text, type: data.type});
                            }
                        });
                    }
                }
            }
        });

        return false;
    },
    visualizarInteracoes: function (event) {
        var idPrevenda = $(event.currentTarget).data("id");

        $.ajax({
            dataType: "html",
            type: "post",
            url: "/contatos-site/read-interacoes-prevenda",
            data: {
                "id_prevenda": idPrevenda,
            },
            beforeSend: function () {
                loading();
            },
            success: function (data) {
                //Render grid result
                loaded();
                bootbox.dialog({
                    size: "large",
                    title: "Interações",
                    closeButton: true,
                    message: data ? data : "<div align='center'>Nenhuma interação encontrada</div>",
                });
            }
        });

        return false;
    },
    events: {
        "click .btn-custom": "submitForm",
        "click .btn-visualizar": "visualizarInteracoes",
        "click .btn-salva-e-muda-interacao": "salvarInteracao",
    }
});

//Render View
new FiltroView({el: $("#containerAll")}).render();


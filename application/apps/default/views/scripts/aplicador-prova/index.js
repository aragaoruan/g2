/**
 * User: Denise Xavier
 * Date: 21/05/2015
 * Aplicador Prova @https://docs.google.com/document/d/1PlQUoqvUoIFd-kcndwtpBUApb4f6hH6a3Sr0bn5uGjI/edit
 */

var VwPessoaEndereco = Backbone.Model.extend({
    defaults: {
        id_usuario: '',
        id_entidade: '',
        bl_padrao: '',
        st_categoriaendereco: '',
        id_tipoendereco: '',
        st_tipoendereco: '',
        id_endereco: '',
        st_endereco: '',
        st_complemento: '',
        nu_numero: '',
        st_bairro: '',
        st_cidade: '',
        st_estadoprovincia: '',
        st_cep: '',
        sg_uf: '',
        id_categoriaendereco: '',
        id_municipio: '',
        st_nomemunicipio: '',
        st_uf: '',
        id_pais: '',
        st_nomepais: '',
        bl_ativo: ''
    }
});

var AplicadorProvaCollection = Backbone.Collection.extend({
    model: AplicadorProva,
    url: "/api/aplicador-prova"
});

var PessoaEnderecoCollection = Backbone.Collection.extend({
    url: '/pessoa/get-endereco-principal-pessoa',
    model: PessoaEndereco
});

var ViewSetDadosEndereco = Backbone.View.extend({
    tagName: "",
    className: "",
    initialize: function () {
        this.render = _.bind(this.render, this);
        this.model.bind('change', this.render);
    },
    render: function () {

        $.ajaxSetup({ async: false });
        $("#sg_uf").val(this.model.get('sg_uf')).trigger('change');
        $.ajaxSetup({ async: true });
        $("#st_cep").val(this.model.get('st_cep'));
        $("#st_endereco").val(this.model.get('st_endereco'));
        $("#st_cidade").val(this.model.get('st_cidade'));
        $("#nu_numero").val(this.model.get('nu_numero'));
        $("#st_bairro").val(this.model.get('st_bairro'));
        $("#st_complemento").val(this.model.get('st_complemento'));
        $("#id_municipio").val(this.model.get('id_municipio'));

        return this;
    }
});


var EnderecoAplicadorView = Marionette.ItemView.extend({
    template: '#template-endereco-aplicador',
    tagName: 'div',
    ui: {
        select_pais :'#id_pais',
        select_uf: '#sg_uf',
        select_municipio: '#id_municipio'
    },
    onRender: function () {
        var that = this;
        that.carregaPais();
    },
    onShow: function() {
    },
    events: {
        'click .btnCep' : 'buscaEnderecoCep',
        'change [name="sg_uf"]': 'changeUf'
    },
    changeUf: function(e){
        var that = this;
        var sg_uf =  e.currentTarget.value;
        if(sg_uf != ''){
            that.carregaMunicipio(sg_uf);
        }
    },
    carregaPais: function(){
        var that = this;
        var PaisModel = Backbone.Model.extend({
            idAttribute: 'id_pais',
            defaults: {
                id_pais: null,
                st_nomepais: ''
            }
        });
        ComboboxView({
            el: that.ui.select_pais,
            model: PaisModel,
            url: '/default/util/retorna-pais',
            optionLabel: 'st_nomepais',
            optionSelectedId: (that.model.get('id_pais') != null ? that.model.get('id_pais') : 22)
        }, function(el, composite) {
            that.carregaUf(that.model.get('id_pais'));
        });
    },
    carregaUf: function(id){
        var that = this;
        var UfModel = Backbone.Model.extend({
            idAttribute: 'sg_uf',
            defaults: {
                sg_uf: null,
                st_uf: ''
            }
        });
        ComboboxView({
            el: that.ui.select_uf,
            model: UfModel,
            url: '/default/util/retorna-uf',
            optionLabel: 'sg_uf',
            optionSelectedId: (that.model.get('sg_uf'))
        }, function(el, composite) {
            that.carregaMunicipio(that.model.get('sg_uf'));
        });

    },
    carregaMunicipio: function(sg_uf){
        var that = this;
        var MunicipoModel = Backbone.Model.extend({
            idAttribute: 'id_municipio',
            defaults: {
                id_municipio: null,
                st_nomemunicipio: ''
            }
        });
        ComboboxView({
            el: that.ui.select_municipio,
            model: MunicipoModel,
            url: '/default/pessoa/get-municipio-by-uf/sg_uf/'+sg_uf,
            optionLabel: 'st_nomemunicipio',
            optionSelectedId: (that.model.get('id_municipio'))
        });
    },
    buscaEnderecoCep: function (e) {
        var elem = $(e.target).closest('fieldset').find('.st_cep_btn');
        var cep = elem.val();
        var sessionCEP = elem.attr('name');

        if (!cep) {
            $.pnotify({
                title: 'Impossível Pesquisar!',
                text: 'Impossível pesquisar por CEP. Preencha o campo de CEP corretamente',
                type: 'warning'
            });
            return false;
        } else {
            loading();
            var pessoaEndereco = new PessoaEnderecoCollection;
            pessoaEndereco.url = '/pessoa/get-endereco-by-cep/st_cep/' + cep + "?primeiro=1";
            pessoaEndereco.fetch({
                success: function () {
                    collectionRender = new CollectionView({
                        collection: pessoaEndereco,
                        childViewConstructor: ViewSetDadosEndereco,
                        childViewTagName: 'div',
                        el: $('#modalUser')
                    });
                    collectionRender.render();
                },
                complete: function (response) {
                    loaded();
                    if (!response || (response.responseText && response.responseText == "false")) {
                        $.pnotify({
                            'title': 'CEP Inválido!',
                            'text': 'O CEP digitado não foi encontrado. Tente novamente!',
                            'type': 'error'
                        });
                    }
                }
            });
            return true;
        }
    }
});


var AplicadorView = Marionette.ItemView.extend({
    template: '#aplicador-view',
    tagName: 'div',
    model: AplicadorProva,
    ui: {
        arvore_entidades :'#entidades',
        div_usuario: '#div_usuario',
        div_entidade: '#div_entidade',
        div_endereco: '#content-endereco',
        id_aplicador: '#id'

    },
    onShow: function() {
    },
    onRender: function() {
        var that = this;
        that.carregaEntidades();
        that.carregaEndereco();
        if(this.model.get('id')){
            if(this.model.get('id_usuarioaplicador') != null){
                that.ui.div_usuario.removeClass('hide');
                that.ui.div_endereco.removeClass('hide');
            }else if(this.model.get('id_entidadeaplicador') != null){
                that.ui.div_entidade.removeClass('hide');
            }else{
                that.ui.div_usuario.addClass('hide');
                that.ui.div_entidade.addClass('hide');
            }
        }

        var PessoaCollection = Backbone.Collection.extend({
            model: VwPesquisarPessoa.extend({
                label: function () {
                    return _.template('<a>' +
                        '<div class="span12"> Nome: <%= st_nomecompleto %>' +
                        '</br><b>CPF:</b> <%=st_cpf%></br>' +
                        '     <b>E-mail:</b> <%=st_email%></br>' +
                        '</div></a>', this.attributes);
                }
            }),
            url: "/api/pessoa/get",
            cache: false,
            parse: function (request) {
                var result = JSON.parse(request);
                return result;
            }
        });

        var acUser = new AutoCompleteView({
            input: that.$el.find('input[name=search]'),
            model: new PessoaCollection(),
            onSelect: function (model) {
                $("#selected").show().find("p").html(model.label());
                this.input.val(model.get('st_nomecompleto'));
                that.$el.find('#id_usuarioaplicador').attr({'value': model.get('id')});
            },
            queryParameter: 'str',
            minKeywordLength: 7,
            wait: 500,
            highlight: '',
            className: 'autocomplete'
        }).render();
        acUser.$el.removeAttr('style').addClass('row-fluid');

        var EntidadeCollection = Backbone.Collection.extend({
            model: Entidade.extend({
                label: function () {
                    return _.template('<a>' +
                        '<div class="span12">Razão Social: <%= st_razaosocial %>' +
                        '</br><b>CNPJ: </b> <%=st_cnpj%></br>' +
                        '</div></a>', this.attributes);
                }
            }),
            url: "/api/entidade/get",
            cache: false,
            parse: function (request) {
                var result = JSON.parse(request);
                return result;
            }
        });

        var acEntidade = new AutoCompleteView({
            input: that.$el.find('input[name=searchEntidade]'),
            model: new EntidadeCollection(),
            onSelect: function (model) {
                $("#selected").show().find("p").html(model.label());
                this.input.val(model.get('st_razaosocial'));
                that.$el.find('#id_entidadeaplicador').attr({'value': model.get('id')});
            },
            queryParameter: 'str',
            minKeywordLength: 7,
            wait: 500,
            highlight: '',
            className: 'autocomplete'
        }).render();
        acEntidade.$el.removeAttr('style').addClass('row-fluid');

    },
    events: {
        'change [name="id_tipopessoa"]': 'changeTipoPessoa',
        'click #btnSalvar': 'salvarAplicador',
        // Eventos autocomplete
        'keypress [name="search"]': 'autoComplete',
        'keypress [name="searchEntidade"]': 'autoCompleteEntidade'
    },
    autoComplete: function(e){
        var elem = $(e.target).closest('#search');
        if (elem.val().length >= 3) {
        }
    },
    autoCompleteEntidade: function(e){
        var elem = $(e.target).closest('#searchEntidade');
        if (elem.val().length >= 3) {

        }
    },
    carregaEntidades: function(){
        var that = this;
        that.ui.arvore_entidades.jstree({
            checkbox : {
                two_state : false,
            },
            "html_data": {
                "ajax": {
                    "url": "util/arvore-entidade",
                    "data": function(n) {
                        return {id: n.attr ? n.attr("id") : 0};
                    }
                }
            },
            "plugins": ["themes", "html_data", "checkbox", "sort", "ui"]
        }).bind("loaded.jstree", function(event, data) {
            if (that.model.get('id')) {
                $.ajax({
                    url: "aplicador-prova/recarrega-arvore",
                    type: 'POST',
                    dataType: 'json',
                    data: 'id_aplicadorprova=' + that.model.get('id'),
                    success: function(data) {
                        that.ui.arvore_entidades.jstree("get_unchecked", null, true).each
                        (function() {
                            for (var i = 0; i < data.length; i++) {
                                if (data[i] == this.id && !$('li#'+this.id).hasClass('jstree-closed')) {
                                    $.jstree._reference("#entidades").check_node('li#' + this.id);
                                }
                            }
                        });
                    }
                });
            }
        });
    },
    carregaEndereco: function(){
        var that = this;
        var modelEndereco = new VwPessoaEndereco();
        if(that.model.get('id_tipopessoa') == 1){
            modelEndereco.url = '/aplicador-prova/retorna-endereco/id_usuarioaplicador/'+that.model.get('id_usuarioaplicador') ;
        }else{
            modelEndereco.url = '/aplicador-prova/retorna-endereco/id_usuarioaplicador' ;
        }
        modelEndereco.fetch({
            success: function (model, response) {
                modelEndereco.set(response);
                var item = new EnderecoAplicadorView({
                    model: modelEndereco
                });
                that.ui.div_endereco.html(item.render().$el);
            }
        });
        //var item = new EnderecoAplicadorView();

    },

    changeTipoPessoa: function(e){
        var that = this;
        that.ui.div_usuario.addClass('hide');
        that.ui.div_entidade.addClass('hide');
        that.ui.div_endereco.addClass('hide');
        if(Number(e.currentTarget.value) == 1){
            that.ui.div_usuario.removeClass('hide');
            that.ui.div_endereco.removeClass('hide');
        }else if(Number(e.currentTarget.value) == 2){
            that.ui.div_entidade.removeClass('hide');
        }
    },
    salvarAplicador: function() {
        var that = this;
        $('#btnSalvar').text('Aguarde...');
        $('#btnSalvar').prop('disabled', true);
        var checked_ids = [];
        $('#entidades').find('.jstree-checked, .jstree-undetermined').each(function() {
            checked_ids.push( $(this).attr('id') );
        });

        var localProva = new AplicadorProva();
        localProva.url = '/api/aplicador-prova';
        var paramObj = formToObject('#formLocalProva');

        if(Number(checked_ids.length) == 0){
            $.pnotify({title: 'Aviso', text:'Selecione pelo menos uma entidade.', type: 'warning'});
            $('#btnSalvar').text('Salvar');
            $('#btnSalvar').prop('disabled', false);
            return false;
        }
        paramObj.entidades = checked_ids;


        var endereco = new VwPessoaEndereco();
        if(paramObj.id_tipopessoa == 1){
            var objEnd = formToObject('#formEnderecoAplicador');
            endereco.set(objEnd);
            // valida se o campo é obrigatorio no caso de endereço de pessoa física
            if(endereco.get('id_pais') == '' ||  endereco.get('st_cep') == '' ||
                endereco.get('id_tipoendereco') == '' || endereco.get('st_endereco') == '' ||
                endereco.get('nu_numero') == '' || endereco.get('st_bairro') == '' || endereco.get('sg_uf') == ''
                || endereco.get('id_municipio') == '' ){
                $.pnotify({title: 'Aviso', text:'Os campos com * são obrigatórios. Preencha-os antes de prosseguir', type: 'warning'});
                $('#btnSalvar').text('Salvar');
                $('#btnSalvar').prop('disabled', false);
                return false;
            }

            paramObj.endereco = objEnd;

        }
        localProva.set(paramObj);
        if (!localProva.id)
            localProva.id = null
        loading();
        localProva.save( null,{
            success: function(response) {
                var retorno = response.changed;
                //colocar aqui o retorno no id_endereco e no id_aplicador (id)
                if(retorno.tipo == 1){
                    that.ui.id_aplicador.val(retorno.codigo.id);
                    that.ui.div_endereco.find('#id_endereco').val(retorno.codigo.id_endereco);
                }

                loaded();
                $.pnotify({title: retorno.title, text: retorno.text, type: retorno.type});//mensagem de retorno
            },
            error: function(response) {
                var retorno = response.changed;
                $.pnotify({title: retorno.title, text: retorno.text, type: 'error'});
                loaded();
            },
            complete: function(response) {
                $('#btnSalvar').text('Salvar');
                $('#btnSalvar').prop('disabled', false);
            }
        });
        return false;

    }
});

var url = window.location.href;
var idParam = Number(url.split('/').pop());
if (!isNaN(idParam)) {
    var model = new AplicadorProva();
    model.set('id', idParam);
    model.fetch({
        success: function (model, response) {
            model.set(response);
            appViewIni = new AplicadorView({
                model: model
            });

            G2S.layout.content.show(appViewIni);
        }
    });
} else {
    appViewIni = new AplicadorView({
        model: new VwAplicadorProva()
    });
    G2S.layout.content.show(appViewIni);
}
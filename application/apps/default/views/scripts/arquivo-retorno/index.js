/*******************************************************************************
 * Imports
 ******************************************************************************/
$.ajaxSetup({'async': false});
$.getScript('/js/backbone/views/ArquivoRetorno/ArquivoRetornoView.js?_=' + new Date().getTime());
$.getScript('/js/backbone/views/ArquivoRetorno/ArquivoRetornoGridView.js?_=' + new Date().getTime());
$.getScript('/js/bootstrap-filestyle.js?_=' + new Date().getTime());

/*******************************************************************************
 *  Views
 ******************************************************************************/

$(function() {
    var url = window.location.hash;
    var piecesUrl = url.split("/");

    if(piecesUrl.length == 3){
        var view = new ArquivoRetornoGridView();
        G2S.layout.content.show(view);
        $('#div-grid-arquivo-retorno').show();
    }else{
        var view = new ArquivoRetornoView();
        G2S.layout.content.show(view);
        $(":file").filestyle({buttonText: "Buscar", iconName: "icon-search", buttonName: 'btn-primary'});
        $('#div-form-arquivo-retorno').show();
    }
});

/***********************
 *   Configurar progresso
 *   do upload AJAX
 **********************/
(function addXhrProgressEvent($) {
    var originalXhr = $.ajaxSettings.xhr;
    $.ajaxSetup({
        progress: function () {
        },
        progressUpload: function () {
        },
        xhr: function () {
            var req = originalXhr(), that = this;
            if (req.upload) {
                if (typeof req.upload.addEventListener == "function") {
                    req.upload.addEventListener("progress", function (evt) {
                        //$(":file").filestyle({buttonText: "Buscar", iconName: "icon-search", buttonName: 'btn-primary'});
                        //this.progressUpload(evt);
                    }, false);
                }
            }
            return req;
        }
    });
})(jQuery);

$.ajaxSetup({'async': true});
loaded();
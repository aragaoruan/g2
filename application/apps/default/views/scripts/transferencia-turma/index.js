/**
 * MODELS
 */
var modelMatricula = new VwMatricula(),
    modelTurma = new VwTurma();

/**
 * VIEWS
 */
var layoutInit,
    formInit,
    salasOrigemInit,
    salasDestinoInit,
    modalGradeInit;

var LayoutView = Marionette.LayoutView.extend({
    el: '#layout-region',
    regions: {
        autocomplete: '#autocomplete-region',
        form: '#form-region',
        salas: '#salas-region'
    },
    initialize: function () {
        layoutInit = this;
        var that = this;

        componenteAutoComplete.main({
            model: VwPesquisarPessoa,
            element: that.autocomplete.$el
        }, function () {
            that.model = new Backbone.Model(JSON.parse(localStorage.getItem('pessoaAutocomplete')));
            that.mostrarForm();
        }, function () {
            that.esconderForm();
        });
    },
    mostrarForm: function () {
        formInit = new FormView();
        this.form.show(formInit);
    },
    esconderForm: function () {
        this.form.empty();
        this.salas.empty();
    }
});


var InfoMatriculaView = Marionette.ItemView.extend({
    template: '#matricula-template',
    model: modelMatricula
});


var FormView = Marionette.LayoutView.extend({
    template: '#form-template',
    model: new Backbone.Model(),
    regions: {
        infoMatricula: '#matricula-region'
    },
    ui: {
        labelMatricula: '#label-matricula',
        inputMatricula: '#id_matricula',
        labelTurma: '#label-turma',
        inputTurma: '#id_turma'
    },
    events: {
        'change @ui.inputMatricula': 'carregarTurmas'
    },
    onRender: function () {
        this.carregarMatriculas();
    },
    carregarMatriculas: function () {
        var that = this;
        var id_usuario = layoutInit.model.get('id_usuario');

        if (!id_usuario) {
            this.ui.inputMatricula.html('<option value="">[Selecione um Usuário]</option>');
            return false;
        }

        this.ui.inputMatricula.html('<option value="">Carregando...</option>');

        ComboboxView({
            el: that.ui.inputMatricula,
            url: '/api/vw-matricula/id_evolucao/' + EVOLUCAO.TB_MATRICULA.CURSANDO + '/id_usuario/' + layoutInit.model.get('id_usuario'),
            optionLabelConstruct: function (model) {
                return model.get('id_matricula') + ' - ' + model.get('st_projetopedagogico') + ' - ' + model.get('st_evolucao');
            },
            model: Backbone.Model.extend({
                idAttribute: 'id_matricula',
                defaults: {
                    id_matricula: null,
                    st_matricula: null
                }
            }),
            onChange: function (model) {
                modelMatricula.clear().set(model.toJSON());
                that.infoMatricula.show(new InfoMatriculaView());
            }
        });
    },
    carregarTurmas: function () {
        var that = this;
        var id_matricula = this.ui.inputMatricula.val();

        if (!id_matricula) {
            this.ui.inputTurma.html('<option value="">[Selecione uma Matrícula]</option>');
            this.ui.labelTurma.addClass('hide');
            return false;
        }

        this.ui.inputTurma.html('<option value="">Carregando...</option>');
        this.ui.labelTurma.removeClass('hide');

        ComboboxView({
            el: that.ui.inputTurma,
            url: '/transfere-projeto/get-vw-turmas-disponives/id_situacao/' + SITUACAO.TB_TURMA.ATIVA + '/id_projetopedagogico/' + modelMatricula.get('id_projetopedagogico'),
            optionLabelConstruct: function (model) {
                return model.get('id_turma') + ' - ' + model.get('st_tituloexibicao');
            },
            model: Backbone.Model.extend({
                idAttribute: 'id_turma',
                defaults: {
                    id_turma: null,
                    st_turma: null
                }
            }),
            onChange: function (model) {
                modelTurma.clear().set(model.toJSON());
                layoutInit.salas.show(new SalasLayout());
            }
        });
    }
});

/**
 * Salas
 */
var SalasLayout = Marionette.LayoutView.extend({
    template: '#salas-template',
    regions: {
        origem: '#origem-region',
        destino: '#destino-region',
        modalGrade: '#modal-grade-region'
    },
    ui : {
      'contador' : '.contador',
      'id_disciplina' :'.id_disciplina'
    },
    events: {
        'click #btn-limpar': 'limparSalas',
        'click #btn-salvar': 'abrirModalSalvar',
        'change .id_disciplina' : 'atualizaContador'
    },
    onRender: function () {
        salasOrigemInit = new SalasOrigemComposite();
        salasDestinoInit = new SalasDestinoComposite();
        this.origem.show(salasOrigemInit);
        this.destino.show(salasDestinoInit);
    },
    limparSalas: function () {
        this.remove();
        formInit.ui.inputTurma.val('');
    },
    abrirModalSalvar: function () {
        /*var salas_realocar = salasOrigemInit.collection.filter(function (item) {
            item.set('bl_manter', false);
            return !item.get('selected');
        });

        var salas_manter = salasDestinoInit.collection.filter(function (item) {
            item.set('bl_manter', true);
            return salasOrigemInit.collection.find(function (origem) {
                return origem.get('selected') && origem.get('id_disciplina') == item.get('id_disciplina');
            });
        });
*/

        var salas_manter = salasOrigemInit.collection.filter(function (item) {
            item.set('bl_manter', true);
            return item.get('selected');
        });

        var salas_realocar = salasDestinoInit.collection.filter(function (item) {
            item.set('bl_manter', false);
            return salasOrigemInit.collection.find(function (origem) {
                return !origem.get('selected') && origem.get('id_disciplina') == item.get('id_disciplina');
            });
        });



        var salas = salas_realocar.concat(salas_manter);

        modalGradeInit = new ModalGradeComposite({
            collection: new Backbone.Collection(salas)
        });

        this.modalGrade.show(modalGradeInit);
    },
    atualizaContador: function () {

        var disciplinas_selecionadas = salasOrigemInit.collection.filter(function (item) {
            return item.get('selected');
        });
        var that = this;
        if(disciplinas_selecionadas.length){
            that.ui.contador.empty().append('<label><b>'+disciplinas_selecionadas.length+' disciplina(s) selecionada(s)</b></label>');
        }else{
            that.ui.contador.empty();
        }
    }
});


/**
 * Salas de Origem
 */

var SalasOrigemCollection = Backbone.Collection.extend({
    model: VwSalaDisciplinaParaTransferencia,
    url: '/transfere-projeto/retornar-disciplinas-origem/'
});

var SalasOrigemComposite = Marionette.CompositeView.extend({
    template: '#salas-origem-template',
    collection: new SalasOrigemCollection(),
    childViewContainer: 'tbody',
    childView: Marionette.ItemView.extend({
        template: '#salas-origem-view',
        tagName: 'tr',
        events: {
            'change input': 'setModel'
        },
        setModel: function (e) {
            var $input = $(e.target);
            this.model.set('selected', $input.prop('checked'));
        }
    }),
    emptyView: Marionette.ItemView.extend({
        template: _.template('<td colspan="11">Nenhum registro encontrado.</td>'),
        tagName: 'tr'
    }),
    initialize: function () {
        this.collection.fetch({
            data: {
                id_projetopedagogico: modelMatricula.get('id_projetopedagogico'),
                id_projetotransfere: modelMatricula.get('id_projetopedagogico'),
                id_matricula: modelMatricula.get('id_matricula'),
                id_turma: formInit.ui.inputTurma.val(),
                bl_ordenar: true
            },
            beforeSend: loading,
            complete: loaded
        });
    }
});

/**
 * Salas de Destino
 */
var SalasDestinoCollection = Backbone.Collection.extend({
    model: VwSalaDisciplinaParaTransferencia,
    url: '/transfere-projeto/retornar-disciplinas-destino/'
});

var SalasDestinoComposite = Marionette.CompositeView.extend({
    template: '#salas-destino-template',
    collection: new SalasDestinoCollection(),
    childViewContainer: 'tbody',
    childView: Marionette.ItemView.extend({
        template: '#salas-destino-view',
        tagName: 'tr'
    }),
    emptyView: Marionette.ItemView.extend({
        template: _.template('<td colspan="3">Nenhum registro encontrado.</td>'),
        tagName: 'tr'
    }),
    initialize: function () {
        this.collection.fetch({
            data: {
                id_projetopedagogico: modelMatricula.get('id_projetopedagogico'),
                id_projetotransfere: modelMatricula.get('id_projetopedagogico'),
                id_matricula: modelMatricula.get('id_matricula'),
                id_turma: formInit.ui.inputTurma.val()
            },
            beforeSend: loading,
            complete: loaded
        });
    }
});

/**
 * Modal Grade Confirmação
 */
var ModalGradeComposite = Marionette.CompositeView.extend({
    template: '#modal-grade-template',
    tagName: 'div',
    className: 'modal fade',
    childViewContainer: 'tbody',
    childView: Marionette.ItemView.extend({
        template: '#modal-grade-view',
        tagName: 'tr',
        onRender: function () {
            if (this.model.get('bl_manter')) {
                this.$el.addClass('success text-success');
            }
        }
    }),
    events: {
        'click #btn-salvar-modal': 'salvarSalas'
    },
    onShow: function () {
        this.$el.modal('show');
    },
    model: new Backbone.Model ,
    onBeforeRender: function () {
        var contador = this.collection.filter(function (item) {
            return item.get('bl_manter');
        });
        this.model.set('nu_contador', contador.length);
    },
    salvarSalas: function () {
        var data = Backbone.Syphon.serialize(layoutInit);
        data.id_disciplina = [];

        salasOrigemInit.collection.each(function (model) {
            if (!model.get('selected')) {
                data.id_disciplina.push(model.get('id_disciplina'));
            }
        });

        $.ajax({
            type: 'post',
            url: '/transferencia-turma/salvar-transferencia-turma/',
            data: data,
            beforeSend: loading,
            success: function () {
                $.pnotify({
                    type: 'success',
                    title: 'Sucesso',
                    text: 'A transferência de turma foi concluída.'
                })
            },
            error: function () {
                $.pnotify({
                    type: 'error',
                    title: 'Erro',
                    text: 'A transferência de turma não foi concluída.'
                })
            },
            complete: function () {
                loaded();
                layoutInit.salas.reset();
                layoutInit.mostrarForm();
            }
        });
    }
});


layoutInit = new LayoutView();
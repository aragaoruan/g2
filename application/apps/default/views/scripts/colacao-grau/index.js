/*******************************
 * VARIAVEIS GLOBAIS
 *******************************/
var mainLayoutInit,
    formInit,
    dadosInit,
    modalHistoricoInit;

var collectionProjetoPedagogico,
    collectionHistorico,
    collectionVwMatriculaDiplomacao;

var modelMatriculaColacao;

var permissaoEditar = false;

/*******************************
 * COLLECTIONS
 *******************************/

var ProjetoPedagogicoCollection = Backbone.Collection.extend({
    url: "default/projeto-pedagogico/retorna-projetos-pedagogicos-entidade/",
    model: VwProjetoEntidade
});

var VwMatriculaDiplomacaoCollection = Backbone.Collection.extend({
    url: "default/colacao-grau/retorna-aluno-colacao/",
    model: VwMatriculaDiplomacao
});
var HistoricoCollection = Backbone.Collection.extend({
    model: Tramite
});


/*******************************
 * INSTÂNCIAS DE MODEL
 *******************************/
modelMatriculaColacao = new MatriculaColacao();


/*******************************
 * LAYOUT
 *******************************/

var ModalHistoricoEmptyView = Marionette.ItemView.extend({
    template: _.template("<td class='warning' colspan='3'> " +
        "<label class='alert alert-block'>Nenhuma informação encontrada.</label></td>"),
    tagName: "tr"
});

var ModalHistoricoItemView = Marionette.ItemView.extend({
    template: "#modal-historico-item-view",
    tagName: "tr"
});

var ModalHistoricoCompositeView = Marionette.CompositeView.extend({
    template: "#modal-historico-template",
    tagName: "div",
    className: "modal fade",
    childViewContainer: "tbody",
    emptyView: ModalHistoricoEmptyView,
    childView: ModalHistoricoItemView,
    events: {
        "click #btnFecharModalHistorico": "fecharModalHistorico"
    },
    onShow: function () {
        this.$el.modal("show");
    },
    fecharModalHistorico: function () {
        this.$el.modal("hide");
    }
});

var DadosEmptyView = Marionette.ItemView.extend({
    template: _.template("<td class='warning' colspan='9'> " +
        "<label class='alert alert-block'>Nenhuma informação encontrada.</label></td>"),
    tagName: "tr"
});

var DadosItemView = Marionette.ItemView.extend({
    template: "#dados-item-view",
    tagName: "tr",
    bl_presencaModel: null,
    preencimento_lote: false,
    modelEvents: {
        "change:dt_colacao": function () {
            this.preencimento_lote = true;
            this.render();
        },
        "change:bl_presenca": function () {
            this.preencimento_lote = true;
            this.ui.bl_presenca.val(this.model.get("bl_presenca"));
            this.render();
        }
    },
    ui: {
        dt_colacao: "#dt_colacao",
        bl_presenca: "#bl_presenca",
        sem_bl_presenca: "#sem_bl_presenca",
        btnEditarItem: "#btnEditarItem"
    },
    events: {
        "click #btnHistoricoItem": "historicoItem",
        "click #btnEditarItem": "editarItem",
        "change #dt_colacao": function () {

            var dt_colacao = this.ui.dt_colacao.val();

            this.model.set("bl_salvarpresenca", true);

            //mostra a combo com os valores do bl_presenca
            this.ui.sem_bl_presenca.hide();
            this.ui.bl_presenca.show();
            this.ui.bl_presenca.val(this.bl_presencaModel);

            //salva a data de colação na model e coloca a flag para salvar no banco
            this.model.set("dt_colacao", this.ui.dt_colacao.val());
            this.model.set("bl_salvardata", true);

            var compareDatas = daysDiff(getTodayStringDate(), dt_colacao);

            //valida se a data da colação é uma data valida e se ela é menor ou igual a data atual
            if (validaDataBR(dt_colacao) && compareDatas <= 0) {
                //desabilita o bl_presenca
                this.ui.bl_presenca.removeAttr("disabled");
            } else {
                //habilita o bl_presenca e seta como null (Selecione)
                this.ui.bl_presenca.attr("disabled", "disabled");
                this.ui.bl_presenca.val("null");
            }
        },
        "change #bl_presenca": function () {
            //set o valor do bl_presenca na variavel "bl_presencaModel"
            this.bl_presencaModel = this.ui.bl_presenca.val();

            //salva a presenca/ausencia na model e coloca a flag para salvar no banco
            this.model.set("bl_presenca", this.bl_presencaModel);
            this.model.set("bl_salvarpresenca", true);
        }
    },
    initialize: function () {
        //Seta as variáveis bl_bancocolacao e bl_bancopresenca se os respectivos campos já tiverem dados vindo do banco
        (this.model.get("dt_colacao")) ? this.model.set("bl_bancocolacao", true) : this.model.set("bl_bancocolacao", false);
        (this.model.get("bl_presenca") === true) ? this.model.set("bl_bancopresenca", true) : this.model.set("bl_bancopresenca", false);


        return this;
    },
    onRender: function () {
        //se o usuario tiver permissão mostra o botão de edição
        if (permissaoEditar === true) {
            this.ui.btnEditarItem.show();
            if (!this.model.get('bl_documentacao')) {
                this.ui.btnEditarItem.attr('disabled', 'disabled');
            }
        } else {
            this.ui.btnEditarItem.hide();
        }

        this.bl_presencaModel = (this.model.get("bl_presenca") === true) ? 1 : ((this.model.get("bl_presenca") === false) ? 0 : "null");

        //bloqueia os campos ao renderizar a grid de dados
        this.bloqueiaCampos();
    },
    bloqueiaCampos: function () {

        if (this.preencimento_lote == false) {
            //lança false para o flag dos campos da grid
            this.model.set("bl_salvardata", false);
            this.model.set("bl_salvarpresenca", false);

            //habilita select "bl_presenca" só se tiver data de colacao
            if (this.model.get("dt_colacao")) {
                this.ui.bl_presenca.show();
                this.ui.sem_bl_presenca.hide();

                if (this.model.get("bl_presenca") === "") {
                    this.ui.bl_presenca.removeAttr("disabled");
                } else if (this.model.get("bl_presenca") === 0) {
                    this.ui.dt_colacao.removeAttr("disabled");
                    this.ui.bl_presenca.attr("disabled", "disabled");
                } else if (this.model.get("bl_presenca") === 1) {
                    this.ui.dt_colacao.attr("disabled", "disabled");
                    this.ui.bl_presenca.attr("disabled", "disabled");
                }

                //seta bl_presenca
                this.ui.bl_presenca.val(this.bl_presencaModel);

            } else {
                this.ui.sem_bl_presenca.show();
                this.ui.bl_presenca.hide();
            }

            var compareDatas = daysDiff(getTodayStringDate(), this.ui.dt_colacao.val());

            //desabilita campo da data da colação se bl_presenca for diferente de "Ausente"
            //  ou se a diferença da data de colacao para a data atual passar a 7 dias
            if (this.model.get("dt_colacao") && (this.model.get("bl_presenca") !== false || compareDatas < -7))
                this.ui.dt_colacao.attr("disabled", "disabled");
        } else {
            var that = this;

            that.ui.bl_presenca.show();
            that.ui.sem_bl_presenca.hide();

            var compareDatas = daysDiff(getTodayStringDate(), this.model.get("dt_colacao"));

            if (this.model.get("dt_colacao") && compareDatas <= 0) {
                that.ui.bl_presenca.removeAttr("disabled");
            } else {
                this.model.set("bl_presenca", "null");
                that.ui.bl_presenca.val("null");
                that.ui.bl_presenca.attr("disabled", "disabled");
            }

        }

        //verifica se a documentação não esta ok e desabilita o campo
        if (!this.model.get('bl_documentacao')) {
            this.ui.dt_colacao.attr('disabled', 'disabled');
        }

    },
    historicoItem: function () {
        var that = this;
        collectionHistorico = new HistoricoCollection();
        collectionHistorico.url = "/matricula/retornar-tramites/?id_matricula=" + that.model.get("id_matricula") + "&id_tipotramite=" + TRAMITE.MATRICULA_COLACAO_GRAU,
            collectionHistorico.fetch({
                async: false,
                beforeSend: function () {
                    loading();
                },
                success: function () {
                    console.log(collectionHistorico);
                    //abre modal do historico do item da colacao
                    modalHistoricoInit = new ModalHistoricoCompositeView({
                        collection: collectionHistorico
                    });
                    mainLayoutInit.modalHistoricoRegiao.show(modalHistoricoInit);
                },
                complete: function () {
                    loaded();
                }

            });

    },
    editarItem: function () {
        if (this.model.get('bl_documentacao')) {
            this.ui.dt_colacao.removeAttr("disabled");
            this.ui.bl_presenca.removeAttr("disabled");
        }
    }
});

var DadosCompositeView = Marionette.CompositeView.extend({
    template: "#dados-template",
    childViewContainer: "tbody",
    emptyView: DadosEmptyView,
    childView: DadosItemView,
    events: {
        "click #btnSalvar": "salvar"
    },
    salvar: function () {

        var data = [];

        //Array com os dados que  serão salvos (bl_salvardata = 1 || bl_salvarpresenca)
        this.collection.each(function (vwModel) {
            if (vwModel.get("bl_salvardata") || vwModel.get("bl_salvarpresenca")) {
                data.push(vwModel.toJSON());
            }
        });

        //se não tiver nada para alterar mostra a mensagem
        if (!data.length) {
            $.pnotify({
                title: "Alerta",
                text: MensagensG2.MSG_COLACAO_01,
                type: "warning"
            });
            return false;
        }

        $.ajax({
            url: "/colacao-grau/salvar-colacao-grau",
            type: "post",
            data: {data: data},
            dataType: "JSON",
            beforeSend: loading(),
            success: function () {

                collectionVwMatriculaDiplomacao.each(function (model) {
                    if (model.get("bl_salvardata") == true) {
                        model.set("bl_bancocolacao", true);
                        model.set("bl_salvardata", false);
                    }
                    if (model.get("bl_salvarcolacao") == true) {
                        model.set("bl_bancocolacao", true);
                        model.set("bl_salvarcolacao", false);
                    }
                });

                $.pnotify({
                    title: "Sucesso",
                    text: sprintf(MensagensG2.MSG_COLACAO_03),
                    type: "success"
                });
            },
            complete: loaded(),
            error: function () {
                $.pnotify({
                    title: "Erro",
                    text: sprintf(MensagensG2.MSG_COLACAO_02),
                    type: "danger"
                });
            }
        });

    }
});

var FormCompositeView = Marionette.CompositeView.extend({
    template: "#form-template",
    ui: {
        st_nomecompleto: '#st_nomecompleto',
        id_projetopedagogico: '#id_projetopedagogico',
        dt_conclusao_min: '#dt_conclusao_min',
        dt_conclusao_max: '#dt_conclusao_max',
        dt_colacao_min: '#dt_colacao_min',
        dt_colacao_max: '#dt_colacao_max',
        btn_gerarnominata: '#btnGerarNominata',
        btn_gerarlistapresenca: '#btnGerarListaPresenca',
        btn_preencherlote: '#btnPreencherLote',
        div_preencherlote: '#divPreencherLote',
        btn_pesquisar: '#btnPesquisar',
        dt_colacaolote: '#dt_colacaolote',
        bl_presencalote: '#bl_presencalote',
        id_situacaodocumento: "#id_situacaodocumento",
    },
    events: {
        'click @ui.btn_gerarnominata': 'gerarNominata',
        'click @ui.btn_gerarlistapresenca': 'gerarListaPresenca',
        'click @ui.btn_preencherlote': 'preencherLote',
        'change @ui.dt_colacaolote': function () {
            var that = this;

            //percorre a collection para ir setando a data de colação em todas
            collectionVwMatriculaDiplomacao.each(function (model) {
                //verifica os parametros e se a documentação esta ok
                if (model.get("bl_bancocolacao") == false && model.get('bl_documentacao')) {
                    model.set("dt_colacao", that.ui.dt_colacaolote.val());
                    model.set("bl_salvardata", true);
                }
            });

            var compareDatas = daysDiff(getTodayStringDate(), that.ui.dt_colacaolote.val());

            if (validaDataBR(this.ui.dt_colacaolote.val()) && compareDatas <= 0) {
                that.ui.bl_presencalote.val("null");
                that.ui.bl_presencalote.removeAttr("disabled");
            } else {
                that.ui.bl_presencalote.val("null");
                that.ui.bl_presencalote.attr("disabled", "disabled");
            }
        },
        "change @ui.bl_presencalote": function () {
            var that = this;
            //percorre a collection para ir setando a bl presença
            collectionVwMatriculaDiplomacao.each(function (model) {
                //verifica os parametros e se a documentação esta ok
                if (model.get("bl_bancopresenca") == false && model.get('bl_documentacao')) {
                    model.set("bl_presenca", that.ui.bl_presencalote.val());
                    model.set("bl_salvarpresenca", true);
                }
            });

        },
        'click @ui.btn_pesquisar': 'pesquisarAlunosColacao'
    },
    onShow: function () {
        //verifica permissão para botão editar
        this.verificaPermissaoBotaoEditar();

        this.loadComboProjetoPedagogico();
        this.ui.btn_gerarnominata.hide();
        this.ui.btn_gerarlistapresenca.hide();
        this.ui.btn_preencherlote.hide();
        this.ui.div_preencherlote.hide();
    },
    loadComboProjetoPedagogico: function () {
        // Carregar combo Projeto Pedagogico
        collectionProjetoPedagogico = new ProjetoPedagogicoCollection();
        collectionProjetoPedagogico.fetch({
            async: false,
            beforeSend: function () {
                loading();
            },
            success: _.bind(function () {
                var ProjetoSelectView = new SelectView({
                    el: this.ui.id_projetopedagogico,
                    collection: collectionProjetoPedagogico,
                    childViewOptions: {
                        optionLabel: "st_projetopedagogico",
                        optionValue: "id_projetopedagogico",
                        optionSelected: ""
                    },
                    clearView: true
                });
                ProjetoSelectView.render();
                this.ui.id_projetopedagogico.select2();
            }, this),
            complete: function () {
                loaded();
            }
        });
    },
    pesquisarAlunosColacao: function () {
        var that = this;
        var dataSerialize = Backbone.Syphon.serialize(this);
        collectionVwMatriculaDiplomacao = new VwMatriculaDiplomacaoCollection();
        collectionVwMatriculaDiplomacao.fetch({
            data: dataSerialize,
            type: "POST",
            beforeSend: loading,
            complete: loaded,
            success: _.bind(function () {
                localStorage.setItem("colacaoStorage", JSON.stringify(collectionVwMatriculaDiplomacao.toJSON()));

                dadosInit = new DadosCompositeView({
                    collection: collectionVwMatriculaDiplomacao
                });

                mainLayoutInit.dadosRegion.show(dadosInit);

                this.ui.btn_gerarnominata.show();
                this.ui.btn_gerarlistapresenca.show();

                //se tiver só um resultado não aparece o preencher em lote
                if (collectionVwMatriculaDiplomacao.length > 1) {
                    this.ui.btn_preencherlote.show();
                } else {
                    this.ui.btn_preencherlote.hide();
                }
            }, this)
        });
    },
    verificaPermissaoBotaoEditar: function () {
        VerificarPerfilPermissao(46, 792, true).done(function (response) {
            if (response.length == 1) {
                permissaoEditar = true;
            }
        });
    },
    gerarNominata: function () {
        window.open("default/colacao-grau/nominata/", "_blank");
    },
    gerarListaPresenca: function () {
        window.open("default/colacao-grau/lista-presenca/", "_blank");
    },
    preencherLote: function () {
        this.ui.div_preencherlote.show();
    }
});

var MainLayoutView = Marionette.LayoutView.extend({
    el: "#main-layout-region",
    regions: {
        formRegion: "#form-region",
        dadosRegion: "#dados-region",
        modalHistoricoRegiao: "#modal-historico-region"
    },
    initialize: function () {
        mainLayoutInit = this;
        this.showView();
    },
    showView: function () {
        formInit = new FormCompositeView({
            model: this.model
        });
        this.formRegion.show(formInit);
    },
    hideView: function () {
        this.dadosRegion.empty();
    }
});

mainLayoutInit = new MainLayoutView();
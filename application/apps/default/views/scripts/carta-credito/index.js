/**
 *              Collections
 */

var SituacaoCollection = Backbone.Collection.extend({
    url: '/api/situacao/?st_tabela=tb_cartacredito',
    model: Situacao
});

var EntidadeCollection = Backbone.Collection.extend({
    url: '/carta-credito/retorna-entidade-compartilhada-carta/',
    model: Entidade
});

/**
 *                 Models
 */

var modelCarta = Backbone.Model.extend({
    defaults: {
        dt_validade: '',
        nu_valordisponivel: '0,00',
        id_entidade: '',
        id_situacao: '',
        modificada: ''
    }
});

/*********************************************
 ************** LAYOUT ******************
 *********************************************/

var LayoutCartaCredito = Marionette.LayoutView.extend({
    template: '#template-carta-principal',
    tagName: 'div',
    className: 'container-fluid',
    regions: {
        regiao1: '#wrapper-body',
        regiao2: '#wrapper-modal'
    },
    ui: {
        regiao1: '#wrapper-body',
        regiao2: '#wrapper-modal',
        autocomplete: '#autoComplete'
    },
    events: {},
    initialize: function () {

    },
    onShow: function () {
        thatLayout = this;

        //Chama o componente de autocomplete
        componenteAutoComplete.main({
            model: VwPesquisarPessoa,
            element: this.ui.autocomplete
        }, function () {
            pessoaDados = componenteAutoComplete.getFixModel();
            thatLayout.abreDados();
        }, function () {
            pessoaDados = '';
            thatLayout.fechaDados();
        });

        return this;

    },
    abreDados: function () {
        this.ui.regiao1.show();
        telaPrincipal.regiao1.show(new ViewBody());
    },
    fechaDados: function () {
        this.ui.regiao1.hide('fast');
    }
});

var ViewBody = Marionette.ItemView.extend({
    template: '#bodyPrincipal',
    ui: {
        btnAdd: '#btnAdd',
        wrapperTable: '#wrapper-table'
    },
    events: {
        'click #btnAdd': 'adicionarCarta'
    },
    onShow: function () {
        this.renderizaTabela();
    },
    renderizaTabela: function () {
        var HtmlOpcoesCell = Backgrid.Cell.extend({
            render: function () {
                var elTemp = _.template(
                    '<div class="content span12">' +
                    '<button type="button" class="btn" id="btnEditar" ' + this.model.get('todaUtilizada') + '><i class="icon-edit"></i></button>' +
                    '<button type="button" class="btn btn-danger" id="btnExcluir"><i class="icon-trash"></i></button>' +
                    '</div>');
                this.$el.html(elTemp);
                return this;
            }
        });

        var columns = [{
            name: "id_cartacredito",
            label: "Id",
            editable: false,
            cell: "string"
        }, {
            name: "dt_cadastro",
            label: "Data de Cadastro",
            cell: "string",
            editable: false
        }, {
            name: "nu_valororiginal",
            label: "Valor da Carta",
            cell: "string",
            editable: false
        }, {
            name: "nu_valorutilizado",
            label: "Valor Utilizado",
            cell: "string",
            editable: false
        }, {
            name: "nu_valordisponivel",
            label: "Valor Restante",
            cell: "string",
            editable: false
        }, {
            name: "st_situacao",
            label: "Situação",
            cell: "string",
            editable: false
        }, {
            name: "dt_validade",
            label: "Data de Validade",
            cell: "string",
            editable: false
        }, {
            name: "st_nomeentidade",
            label: "Entidade",
            cell: "string",
            editable: false
        }, {
            name: 'opcoes',
            label: "Opções",
            cell: HtmlOpcoesCell,
            editable: false
        }];

        var RowGrid = Backgrid.Row.extend({
            initialize: function (options) {
                RowGrid.__super__.initialize.apply(this, arguments);
                this.$el.css('cursor', 'pointer');
            }, editar: function () {
                this.model.set({'operacao': 'Editar carta de crédito'});
                modalCarta = new ModalCartaItemView({model: this.model});
                telaPrincipal.regiao2.show(modalCarta);
            }, excluir: function () {
                var that = this;
                modelo = new CartaCredito();
                modelo.set({
                    'id': this.model.get('id_cartacredito')
                });
                bootbox.confirm("Deseja realmente excluir este registro?", function (result) {
                    if (result) {
                        modelo.destroy({
                            success: function () {
                                $.pnotify({
                                    title: "Sucesso!",
                                    text: "Carta de crédito removida com sucesso!",
                                    type: "success"
                                });
                                that.$el.remove();
                            }
                        })
                    }
                })
            },
            events: {
                "click #btnEditar": "editar",
                "click #btnExcluir": "excluir"
            }
        });

        var PageableTab = Backbone.PageableCollection.extend({
            model: VwCartaCredito,
            url: 'carta-credito/retorna-cartas-usuario/id_usuario/' + pessoaDados.id_usuario + '/bl_ativo/1',
            state: {
                pageSize: 10
            },
            mode: "client"
        });
        var pageableTab = new PageableTab();
        var pageableGridTable = new Backgrid.Grid({
            className: 'backgrid table table-bordered',
            columns: columns,
            collection: pageableTab,
            row: RowGrid,
            emptyText: "Nenhum registro encontrado"
        });

        var $elemento = this.ui.wrapperTable;
        $elemento.empty();
        $elemento.append(pageableGridTable.render().$el);

        var paginator = new Backgrid.Extension.Paginator({
            collection: pageableTab
        });

        $elemento.append(paginator.render().el);

        pageableTab.fetch({reset: true, beforeSend: loading(), complete: loaded()});
    },
    adicionarCarta: function () {
        var mdCarta = new modelCarta;
        mdCarta.set({
            'operacao': 'Adicionar nova carta de crédito',
            'id_usuario': pessoaDados.id_usuario
        });
        var modalCarta = new ModalCartaItemView({model: mdCarta});
        telaPrincipal.regiao2.show(modalCarta);
    }
});


/**
 *          MODAL UTILIZADA PARA ALTERAR UMA CARTA DE CRÉDITO
 */


var ModalCartaItemView = Marionette.LayoutView.extend({
    template: '#modalCarta',
    tagName: 'div',
    className: 'modal hide fade modal-large',
    ui: {
        id_entidade: '#id_entidade',
        id_situacao: '#id_situacao',
        dt_validade: '#dt_validade',
        nu_valor: '#nu_valor',
        btnSalvar: '#btnSalvar'
    },
    events: {
        'click #btnSalvar': 'save'
    },
    beforeRender: function () {
        if (this.model.get('nu_valordisponivel') == this.model.get('nu_valordisponivel')) {
            this.model.set({modificada: disabled});
        }
    },
    onRender: function () {
        loading();
        this.renderizaSituacao();
        this.renderizaEntidade();
    },
    onShow: function () {
        this.$el.modal('show');
        this.ui.nu_valor.maskMoney({precision: 2, thousands: '', decimal: ",", allowNegative: false});
        this.ui.nu_valor.trigger('click');
        return this;
    },
    renderizaSituacao: function () {
        var that = this;
        var collectionSituacao = new SituacaoCollection();
        collectionSituacao.fetch({
            success: function () {
                var view = new SelectView({
                    el: that.$el.find("select[name='id_situacao']"),        // Elemento da DOM
                    collection: collectionSituacao,      // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_situacao', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_situacao', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: that.model.get('id_situacao') ? that.model.get('id_situacao') : 142     // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();
                loaded();
            }
        });
    },
    renderizaEntidade: function () {
        var that = this;
        var collectionEntidade = new EntidadeCollection();
        collectionEntidade.url += '?bl_cartacompartilhada=1&id_entidade=' + (that.model.get('id_entidade') ? that.model.get('id_entidade') : pessoaDados.id_entidade);
        collectionEntidade.fetch({
            success: function () {
                var view = new SelectView({
                    el: that.$el.find("select[name='id_entidade']"),        // Elemento da DOM
                    collection: collectionEntidade,      // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_nomeentidadeparceira', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_entidadeparceira', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: that.model.get('id_entidade')       // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();
                loaded();
            },
            complete: function () {
                that.ui.id_entidade.select2();
            }
        });
    },
    save: function () {
        var that = this;

        if (this.validaForm()) {

            var data = this.model.toJSON();
            data.id_entidade = this.ui.id_entidade.val();
            data.id_situacao = this.ui.id_situacao.val();
            data.dt_validade = this.ui.dt_validade.val();
            data.nu_valororiginal = this.ui.nu_valor.val();

            $.ajax({
                url: '/carta-credito/save',
                dataType: 'json',
                data: data,
                type: 'post',
                beforeSend: function () {
                    loading();
                },
                success: function (data) {

                    $.pnotify(data.notify);

                    that.model.set({id_cartacredito: data.objeto.id_cartacredito});
                    that.model.set({id_entidade: data.objeto.id_entidade.id_entidade});
                    that.model.set({id_situacao: data.objeto.id_situacao.id_situacao});
                    that.model.set({dt_validade: converteStringDataUSAToBR(data.objeto.dt_validade)});
                    that.model.set({nu_valororiginal: data.objeto.nu_valororiginal});
                    that.model.set({nu_valordisponivel: data.objeto.nu_valororiginal});
                    that.model.set({st_nomeentidade: data.objeto.st_nomeentidade});

                    telaPrincipal.regiao1.show(new ViewBody());

                    loaded();
                },
                complete: function () {
                    loaded();
                },
                error: function (data) {
                    $.pnotify(data);
                }
            })
        } else {
            _.each(arr, function(valor){
                $.pnotify(valor);
            })
        }
    },
    validaForm: function () {
        var flag = true;

        arr = [];

        if (this.ui.id_entidade.val() == "") {
            $("#divIdEntidade").find($('.select2-selection--single')).css('border', '1px solid #a94442');
            arr.push({title: 'Atenção!', text: 'Selecione uma entidade!', type: 'warning'});
            flag = false;
        } else {
            $("#divIdEntidade").find($('.select2-selection--single')).css('border', '1px solid #aaa');
        }

        if (!this.ui.dt_validade.val()) {
            $('#divValidade').addClass('control-group error');
            arr.push({title: 'Atenção!', text: 'Selecione uma data de validade!', type: 'warning'});
            flag = false;
        } else {
            $('#divValidade').removeClass('control-group error');
        }

        if (!this.ui.id_situacao.val()) {
            $('#divIdSituacao').addClass('control-group error');
            arr.push({title: 'Atenção!', text: 'Selecione uma situação!', type: 'warning'});
            flag = false;
        } else {
            $('#divIdSituacao').removeClass('control-group error');
        }

        if (!this.ui.nu_valor.val() || this.ui.nu_valor.val() == "0,00") {
            $('#divValor').addClass('control-group error');
            arr.push({title: 'Atenção!', text: 'Insira um valor para a carta de crédito!', type: 'warning'});
            flag = false;
        } else {
            $('#divValor').removeClass('control-group error');
        }

        return flag;
    }
});

/*********************************************
 ************** INICIALIZAÇÃO ****************
 *********************************************/

var telaPrincipal = new LayoutCartaCredito();
G2S.show(telaPrincipal);
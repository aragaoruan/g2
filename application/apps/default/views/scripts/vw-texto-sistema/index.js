var VwTextoSistema = Backbone.Model.extend({
    defaults: {
        id_textosistema: '',
        id_usuario: '',
        id_entidade: '',
        id_textoexibicao: '',
        id_textocategoria: '',
        id_situacao: '',
        st_textosistema: '',
        st_texto: '',
        dt_inicio: '',
        dt_fim: '',
        dt_cadastro: '',
        id_orientacaotexto: '',
        st_textoexibicao: '',
        st_textocategoria: '',
        st_situacao: '',
        st_descricaosituacao: '',
        st_orientacaotexto: ''        
    },
    idAttribute: 'id_textosistema',
    toString: function(){
        return this.get("st_textosistema");
    }, 
    url: function() {
        return this.id ? '/api/vw-texto-sistema/' + this.id : '/api/vw-texto-sistema';
    },
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    }
});

var VwTextosSistema = Backbone.Collection.extend({
    model: VwTextoSistema,
    url: function(){ 
        return "/api/vw-texto-sistema/?"   + $.param(_.defaults(this.data));
    },
    parse: function(data) {
        return data.mensagem;
    },
    data: {
              id_textocategoria: null,
              id_textoexibicao: null
    }
});
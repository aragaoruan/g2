
/**
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 * Funcionalidade Defesa TCC
 */

$.ajaxSetup({sync: false});

var VwAvaliacaoAlunoCollectionExtend = Backbone.Collection.extend({
    model: VwAvaliacaoAluno
});


var AvaliacoesItemView = Marionette.ItemView.extend({
    template: _.template('<td><%=id_matricula%></td><td><%=st_projetopedagogico%></td>\n\
                          <td><%=st_nomecompleto%></td><td><%=st_situacao%></td>\n\
                          <td><input class="datepicker input-mini" type="text" value="<%=dt_defesa%>"></td>'),
    tagName: 'tr',
    events: {
        "change input.datepicker": function(e) {
            this.model.set('dt_defesa', $(e.currentTarget).val());
        }
    }
});


var DefesaTccCompositeView = Marionette.CompositeView.extend({
    template: '#template-defesa-tcc',
    childView: AvaliacoesItemView,
    childViewContainer: 'tbody',
    pesquisarAvaliacoes: function() {
        loading();

        var params = $('#form-search-filter').serialize();
        var that = this;
        that.collection.url = '/avaliacao-aluno/find-vw-avaliacao-aluno-defesa-tcc';
        that.collection.fetch({
            type: 'post',
            data: params,
            complete: function() {
                if (!that.collection.length) {
                    $.pnotify({title: 'Alerta', text: MensagensG2.MSG_DEFESA_TCC_04, type: 'warning'});
                }
                loaded();
            }
        });
        return false;
    },
    salvarDefesaTcc: function() {
        loading();
        var data = [];
        
        $.each(this.collection.models, function(index, model) {
            if (model.get('dt_defesa').length) {
                data.push(model.clone().toJSON());
            }
        });

        if (data.length) {
            $.ajax({
                url: '/avaliacao-aluno/salvar-defesa-tcc',
                type: 'post',
                dataType: 'json',
                data: {data: data},
                success: function(response) {
                    //recarregando conteudo
                    $.pnotify({title: 'Alerta', text: MensagensG2.MSG_DEFESA_TCC_01, type: 'success'});
                },
                complete: function() {
                    DefesaTCC.pesquisarAvaliacoes();
                    loaded();
                },
                error: function(response) {
                    $.pnotify({title: 'Alerta', text: MensagensG2.MSG_DEFESA_TCC_03, type: 'error'});
                }
            });
        } else {
            $.pnotify({title: 'Alerta', text: MensagensG2.MSG_DEFESA_TCC_02, type: 'warning'});
        }
    },
    events: {
        "submit #form-search-filter": "pesquisarAvaliacoes",
        "click #btn-salvar-defesa-tcc": "salvarDefesaTcc"
    }
});

//instancia a view
var DefesaTCC = new DefesaTccCompositeView({
    collection: new VwAvaliacaoAlunoCollectionExtend()
});
//renderiza
G2S.layout.content.show(DefesaTCC);

                                                            
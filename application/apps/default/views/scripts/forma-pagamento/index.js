loading();

/**
 * Template Global
 */
var GlobalModel = Backbone.Model.extend({
    idAttribute: 'id_formapagamento',
    defaults: {
        id_formapagamento: null,
        st_formapagamento: '',
        st_descricao : '',
        id_situacao: {
            id_situacao: null
        },
        id_formapagamentoaplicacao: null,
        nu_entradavalormin: 0.00,
        nu_entradavalormax: 0.00,
        nu_maxparcelas: null,
        nu_valorminparcela: 0.00,
        nu_valormin: 0.00,
        nu_valormax: 0.00,
        nu_jurosmin: 0.00,
        nu_jurosmax: 0.00,
        nu_multa: 0.00,
        nu_juros: 0.00,
        bl_todosprodutos: 0,
        meios_pagamentos: [],
        produtos: [],
        parcelas: []
    },
    url: function () {
        return 'api/forma-pagamento/' + (this.id || '');
    }
});
var GlobalView = Marionette.ItemView.extend({
    tagName: 'div',
    template: '#global-template',
    model: new GlobalModel(),
    onRender: function() {
        getSituacao(this.model.get('id_situacao').id_situacao);
        getAplicacao(this.model.get('id_formapagamentoaplicacao'));
        getMeioPagamentoCheckbox(this.model.get('meios_pagamentos'));

        this.$el.find('#parcelamento tbody').html(getParcela(this.model.get('parcelas')));

        this.$el.find('[name="st_formapagamento"]').focus();

        this.$el
            .find('[name="nu_entradavalormin"], [name="nu_entradavalormax"], [name="nu_valorminparcela"], [name="nu_valormin"], [name="nu_valormax"], [name="nu_jurosmin"], [name="nu_jurosmax"], [name="nu_multa"], [name="nu_juros"]')
            .maskMoney({precision: 2, decimal: ".", thousands: ""});

        showLists();

        loaded();
    },
    events: {
        'keyup .input-search': 'inputFilter',
        'submit #form' : 'save',
        'click .btn-cancel': 'new',
        'change [name="bl_todosprodutos"]': 'toggleProdutos'
    },
    toggleProdutos: function(e) {
        var checked = $(e.target).prop('checked');
        if (checked) {
            $('.produtos-toggle').hide();
            $('#produtos-selecionados-list .btn-remove').trigger('click');
        } else {
            $('.produtos-toggle').show();
        }
    },
    inputFilter: function(e) {
        var str = $(e.target).val().removeAccents().toLowerCase().trim();
        var options = $(e.target).closest('.search-box').find('table:first tbody tr');

        if (str.length) {
            options.each(function() {
                var col = $(this).find('td').first();

                if (!col.attr('data-label'))
                    col.attr('data-label', col.text().removeAccents().toLowerCase().trim());
            });

            options.hide()
                .find('td[data-label^="' + str + '"]')
                .closest('tr').show();
        } else {
            options.show();
        }
    },
    new: function() {
        this.model.set(this.model.defaults);
        this.render();
    },
    edit: function(id) {
        var that = this;

        this.model.set('id_formapagamento', id);
        this.model.fetch({
            success: function(model, response) {
                that.model.set(response);
                that.render();
            }
        });
    },
    save: function(e) {
        e.preventDefault();

        var that = this;
        var elem = $(e.target);

        var values = {
            st_formapagamento : elem.find('[name="st_formapagamento"]').val(),
            st_descricao : elem.find('[name="st_descricao"]').val(),
            id_situacao: {
                id_situacao: Number(elem.find('[name="id_situacao"]').val())
            },
            id_formapagamentoaplicacao: Number(elem.find('[name="id_formapagamentoaplicacao"]').val()),
            nu_entradavalormin: Number(elem.find('[name="nu_entradavalormin"]').val()),
            nu_entradavalormax: Number(elem.find('[name="nu_entradavalormax"]').val()),
            nu_maxparcelas: Number(elem.find('[name="nu_maxparcelas"]').val()) || 0,
            nu_valorminparcela: Number(elem.find('[name="nu_valorminparcela"]').val()),
            nu_valormin: Number(elem.find('[name="nu_valormin"]').val()),
            nu_valormax: Number(elem.find('[name="nu_valormax"]').val()),
            nu_jurosmin: Number(elem.find('[name="nu_jurosmin"]').val()),
            nu_jurosmax: Number(elem.find('[name="nu_jurosmax"]').val()),
            nu_multa: Number(elem.find('[name="nu_multa"]').val()),
            nu_juros: Number(elem.find('[name="nu_juros"]').val()),
            bl_todosprodutos: elem.find('[name="bl_todosprodutos"]').is(':checked') ? 1 : 0,
            meios_pagamentos: [],
            produtos: [],
            parcelas: []
        };

        // Validar campos obrigatórios
        var errorMsg = null;
        var errorElem = null;

        if (values.st_formapagamento.trim() === '') {
            errorMsg = 'Preencha o título';
            errorElem = $('[name="st_formapagamento"]');
        }
        else if (values.st_descricao.trim() === '') {
            errorMsg = 'Preencha a descrição';
            errorElem = $('[name="st_descricao"]');
        }
        else if (values.id_situacao.id_situacao === 0) {
            errorMsg = 'Informe a situação';
            errorElem = $('[name="id_situacao"]');
        }
        else if (values.id_formapagamentoaplicacao === 0) {
            errorMsg = 'Informe a aplicação';
            errorElem = $('[name="id_formapagamentoaplicacao"]');
        }
        else if (values.nu_entradavalormin > values.nu_entradavalormax) {
            errorMsg = 'O Valor de Entrada máximo deve ser maior que o mínimo';
            errorElem = $('[name="nu_entradavalormin"]');
        }
        else if (values.nu_maxparcelas < 0) {
            errorMsg = 'O número de parcelas não pode ser menor que zero';
            errorElem = $('[name="nu_maxparcelas"]');
        }
        else if (values.nu_valormin > values.nu_valormax) {
            errorMsg = 'A faixa de valor máxima deve ser maior que a mínima';
            errorElem = $('[name="nu_valormin"]');
        }
        else if (values.nu_jurosmin > values.nu_jurosmax) {
            errorMsg = 'O juros máximo devem ser maiores que o mínimo';
            errorElem = $('[name="nu_jurosmin"]');
        }


        if (errorMsg) {
            $.pnotify({
                title: 'Erro',
                text: errorMsg,
                type: 'error'
            });
            errorElem.focus();
            return false;
        }


        // Pegar ids dos meios de pagamentos (Entrada)
        elem.find('[name="meios_pagamentos[]"]:checked').each(function () {
            values.meios_pagamentos.push($(this).val());
        });

        // Pegar ids dos produtos selecionados
        var produtos = ProdutosSelecionadosIni.collection.models;
        if (produtos) {
            for (index in produtos) {
                values.produtos.push( produtos[index].id )
            }
        }


        // Salvar parcelmentos que nao foram clicados no botao salvar
        $('#parcelamento .actionSave').each(function() {
            $(this).trigger('click');
        });
        // Pegar dados dos parcelamentos adicionados
        var parcelamentos = ParcelasIni.collection.models;
        if (parcelamentos) {
            for (index in parcelamentos) {
                values.parcelas.push(parcelamentos[index].attributes)
            }
        }

        loading();
        this.model.set(values);

        this.model.save(null, {
            success: function (model, response) {
                $.pnotify({
                    title: 'Concluído',
                    text: 'O registro foi salvo com sucesso!',
                    type: 'success'
                });

                window.location.hash = 'pesquisa/' + G2S.funcionalidadeAtual.get('classeFlexBotao');
            },
            error: function () {
                $.pnotify({
                    title: 'Erro',
                    text: 'Houve um erro ao tentar salvar o registro, tente novamente.',
                    type: 'error'
                });
            },
            complete: function () {
                loaded();
            }
        });;
    }
});



// ComboBoxes
// Funcoes que trazem os conteudos
// dos campos <select>

function getSituacao(selectedId, fn) {
    var Model = Backbone.Model.extend({
        idAttribute: 'id_situacao',
        defaults: {
            'id_situacao': null,
            'st_situacao': '[Selecione]',
            'st_tabela': '',
            'st_campo': '',
            'st_descricaosituacao': ''
        }
    });

    ComboboxView({
        el: '#id_situacao',
        model: Model,
        url: '/api/situacao?st_tabela=tb_formapagamento',
        optionLabel: 'st_situacao',
        optionSelectedId: selectedId
    }, fn);
}

function getAplicacao(selectedId, fn) {
    var Model = Backbone.Model.extend({
        idAttribute: 'id_formapagamentoaplicacao',
        defaults: {
            id_formapagamentoaplicacao: null,
            st_id_formapagamentoaplicacao: ''
        }
    });

    ComboboxView({
        el: '#id_formapagamentoaplicacao',
        model: Model,
        url: '/forma-pagamento/get-aplicacao/',
        optionLabel: 'st_formapagamentoaplicacao',
        optionSelectedId: selectedId
    }, fn);
}

function getMeioPagamento(el, selectedId, fn) {
    var Model = Backbone.Model.extend({
        idAttribute: 'id_meiopagamento',
        defaults: {
            id_meiopagamento: null,
            st_meiopagamento: '',
            st_descricao: ''
        }
    });

    ComboboxView({
        el: el,
        model: Model,
        url: '/api/meio-pagamento/',
        optionLabel: 'st_meiopagamento',
        optionSelectedId: selectedId
    }, fn);
}

function getMeioPagamentoCheckbox(selectedIds) {
    var Model = Backbone.Model.extend({
        idAttribute: 'id_meiopagamento',
        defaults: {
            id_meiopagamento: null,
            st_meiopagamento: '',
            st_descricao: ''
        }
    });

    var Collection = Backbone.Collection.extend({
        model: Model,
        url: '/api/meio-pagamento/'
    });

    var ItemView = Marionette.ItemView.extend({
        tagName: "label",
        className: 'checkbox inline',
        initialize: function () {
            _.bind(this, 'render');
        },
        render: function () {
            var elem = $(this.el);

            elem
                .attr('title', this.model.get('st_descricao'))
                .append('<input type="checkbox" name="meios_pagamentos[]" />')
                .css({
                    margin: '0 20px 0 0',
                    minWidth: '120px'
                });

            elem.find('input[type="checkbox"]')
                .attr('value', this.model.id)
                .after(this.model.get('st_meiopagamento'));

            for (var index in selectedIds) {
                if (Number(selectedIds[index]) == Number(this.model.id)) {
                    elem.find('input[type="checkbox"]').prop('checked', 'checked');
                    break;
                }
            }

            return this;
        }
    });

    var Initialize = new Collection();
    Initialize.fetch({
        success: function () {
            var renderer = new CollectionView({
                collection: Initialize,
                childViewConstructor: ItemView,
                childViewTagName: 'label',
                el: $('#meios_pagamentos')
            });
            renderer.render().$el;
        }
    });
}


function getParcela(parcelas) {


    return 'ASD';
}


/**
 * Funcao que monta os campos
 * <select> com as opcoes
 */
function ComboboxView(params, fn) {

    /**
     * @param params.el
     * @param params.model
     * @param params.url
     * @param params.optionLabel
     * @param params.optionSelectedId
     * @param fn
     */

    if (typeof params !== "object") {
        console.log("ComboboxView ERRO: \n" +
            "O parametro deve ser um OBJETO, com os índices: \n" +
            " - el: '#element' // seletor da tag select \n" +
            " - model: BackboneModel // instancia BackboneModel, que contenha os atributos 'url' e 'idAttribute'" +
            " - optionLabel: 'field' // campo do BackboneModel que sera usado na descricao da tag option" +
            " - [opcional] optionSelectedId: 'value' // valor que sera comparado com os options para determinar o selecionado");
        return false;
    }

    var Collection = Backbone.Collection.extend({
        model: params.model,
        url: params.url
    });

    var ItemView = Marionette.ItemView.extend({
        tagName: "option",
        render: function () {
            var elem = $(this.el);

            elem
                .attr('value', this.model.id)
                .html(this.model.get(params.optionLabel));

            if (params.optionSelectedId == this.model.id)
                elem.attr('selected', 'selected');

            return this;
        }
    });

    var Initialize = new Collection();
    Initialize.fetch({
        success: function () {
            var renderer = new CollectionView({
                collection: Initialize,
                childViewConstructor: ItemView,
                childViewTagName: 'option',
                el: $(params.el)
            });
            renderer.render().$el;

            if (typeof fn === 'function') {
                fn.call();
            }
        }
    });
}








/**
 * Produtos
 */
var ProdutosModel = Backbone.Model.extend({
    idAttribute: 'id_produto',
    defaults: {
        id_produto: null,
        st_produto: '',
        id_entidade: null,
        id_tipoproduto: null,
        st_tipoproduto: '',
        nu_valor: null,
        nu_valormensal: null,
        id_tipoprodutovalor: null,
        st_tipoprodutovalor: '',
        id_situacao: null,
        st_situacao: '',
        id_produtovalor: null,
        id_usuariocadastro: null,
        bl_ativo: null,
        bl_todasformas: null,
        bl_todascampanhas: null,
        nu_gratuito: null,
        id_produtoimagempadrao: null,
        bl_unico: null,
        id_planopagamento: null,
        nu_parcelas: null,
        nu_valorentrada: null,
        nu_valorparcela: null,
        st_descricao: null,
        st_observacoes: null,
        st_informacoesadicionais: null,
        st_estruturacurricular: null,
        st_subtitulo: null,
        st_slug: null,
        bl_destaque: null,
        dt_cadastro: {},
        dt_atualizado: {},
        dt_iniciopontosprom: {},
        dt_fimpontosprom: {},
        nu_pontos: null,
        nu_pontospromocional: null,
        nu_estoque: null,
        id_produtovalorvenda: null,
        nu_valorvenda: null,
        id_modelovenda: null,
        nu_quantidade: null,
        bl_todasentidades: null,
        st_nomeentidade: '',
        st_modelovenda: '',
        nu_valorpromocional: null
    }
});

var ProdutosCollection = Backbone.Collection.extend({
    model: ProdutosModel,
    url: '/forma-pagamento/get-produtos/'
});

var ProdutosView = Marionette.ItemView.extend({
    template: '#produtos-all-item',
    tagName: 'tr',
    onRender: function() {
        this.$el.attr('data-id', this.model.id);
    },
    events: {
        'click .btn-add': 'addModel'
    },
    addModel: function() {
        ProdutosSelecionadosIni.collection.add(this.model.clone());
        ProdutosIni.collection.remove(this.model);
        // this.model.destroy()
    }
});

var ProdutosSelecionadosView = Marionette.ItemView.extend({
    template: '#produtos-selecionados-item',
    tagName: 'tr',
    events: {
        'click .btn-remove': 'removeModel'
    },
    removeModel: function() {
        ProdutosIni.collection.add(this.model.clone());
        ProdutosSelecionadosIni.collection.remove(this.model);
        // this.model.destroy();
    }
});

var ProdutosCompositeView = Marionette.CompositeView.extend({
    template: '#produtos-all-template',
    collection: new ProdutosCollection(),
    childView: ProdutosView,
    childViewContainer: 'tbody'
});

var ProdutosSelecionadosCompositeView = Marionette.CompositeView.extend({
    template: '#produtos-selecionados-template',
    collection: new ProdutosCollection(),
    childView: ProdutosSelecionadosView,
    childViewContainer: 'tbody'
});

var ProdutosIni = new ProdutosCompositeView();
var ProdutosSelecionadosIni = new ProdutosSelecionadosCompositeView();


/**
 * Parcelas
 */

var ParcelasModel = Backbone.Model.extend({
    idAttribute: 'id_formapagamentoparcela',
    defaults: {
        id_formapagamentoparcela: null,
        id_formapagamento: null,
        nu_parcelaquantidademin: 0,
        nu_parcelaquantidademax: 0,
        nu_juros: 0.00,
        nu_valormin: 0.00,
        id_meiopagamento: {
            id_meiopagamento: null,
            st_meiopagamento: ''
        }
    }
});

var ParcelasCollection = Backbone.Collection.extend({
    model: ParcelasModel,
    url: '/forma-pagamento/get-parcela/'
});

var ParcelasView = Marionette.ItemView.extend({
    template: '#parcelamento-item',
    tagName: 'tr',
    model: new ParcelasModel(),
    onRender: function() {
        this.$el.attr('data-id', this.model.id);
    },
    events: {
        'click .actionRemove': 'actionRemove',
        'click .actionEdit': 'actionEdit',
        'click .actionSave': 'actionSave'
    },
    isEditing: false,
    actionRemove: function() {
        ParcelasIni.collection.remove(this.model);
    },
    actionEdit: function() {
        if (this.isEditing) {

            if (!this.model.get('id_meiopagamento').id_meiopagamento)
                this.actionRemove();

            this.template = '#parcelamento-item';
            this.render();
        } else {
            this.template = '#parcelamento-item-edit';
            this.render();
            getMeioPagamento(this.$el.find('#id_meiopagamento'), this.model.get('id_meiopagamento').id_meiopagamento);
        }

        this.$el
            .find('[name="nu_valormin"], [name="nu_juros"]')
            .maskMoney({precision: 2, decimal: ".", thousands: ""});

        this.isEditing = !this.isEditing;
    },
    actionSave: function() {
        var elem = this.$el;

        var values = {
            id_formapagamento: GlobalViewIni.model.id,
            nu_parcelaquantidademin: Number(elem.find('[name="nu_parcelaquantidademin"]').val()),
            nu_parcelaquantidademax: Number(elem.find('[name="nu_parcelaquantidademax"]').val()),
            nu_juros: Number(elem.find('[name="nu_juros"]').val()),
            nu_valormin: Number(elem.find('[name="nu_valormin"]').val()),
            id_meiopagamento: {
                id_meiopagamento: elem.find('[name="id_meiopagamento"]').val(),
                st_meiopagamento: elem.find('[name="id_meiopagamento"] option:selected').text()
            }
        };

        this.model.set(values);
        this.actionEdit();
    }
});

var ParcelasCompositeView = Marionette.CompositeView.extend({
    template: '#parcelamento-template',
    collection: new ParcelasCollection(),
    childView: ParcelasView,
    childViewContainer: 'tbody',
    onRender: function() {
        this.verifyEmpty();
    },
    events: {
        'click .actionAdd': 'actionAdd'
    },
    actionAdd: function() {
        var ItemView = new ParcelasView();
        ParcelasIni.collection.add(ItemView.model.clone());
        ParcelasIni.$el.find('tr:last .actionEdit').trigger('click');
    },
    collectionEvents: {
        'add': 'verifyEmpty',
        'remove': 'verifyEmpty'
    },
    verifyEmpty: function() {
        var elem = this.$el;

        if (this.collection.length) {
            elem
                .find('.empty-view').remove();
        } else {
            var ViewEmpty = Marionette.ItemView.extend({
                tagName: 'tr',
                className: 'empty-view',
                template: _.template('<td colspan="5">Nenhum registro encontrado.</td>')
            });

            elem
                .find('tbody').append((new ViewEmpty()).render().$el);
        }
    }
});

var ParcelasIni = new ParcelasCompositeView();




function showLists() {
    ProdutosIni.collection.fetch({
        success: function() {
            ProdutosSelecionadosIni.collection.reset();

            $('#produtos-all-list').html(ProdutosIni.render().$el);
            $('#produtos-selecionados-list').html(ProdutosSelecionadosIni.render().$el);

            // Ao editar, adiciona os Produtos que já contem
            var selecionados = GlobalViewIni.model.get('produtos');
            var produtos = ProdutosIni.collection.models;

            if (selecionados.length) {
                for (index in selecionados) {
                    $('#produtos-all-list [data-id="' + selecionados[index] + '"] .btn-add').trigger('click');
                }
            }
        }
    });

    ParcelasIni.collection.url = '/forma-pagamento/get-parcela/?id_formapagamento=' + GlobalViewIni.model.id;
    ParcelasIni.collection.fetch({
        success: function() {
            $('#parcelamento').html(ParcelasIni.render().$el);
        }
    });
}


var GlobalViewIni = new GlobalView();

// Verificar se possui id no url
var url = window.location.href;
var idParam = Number(url.split('/').pop());
if (!isNaN(idParam)) {
    var model = new GlobalModel();
    model.set('id_formapagamento', idParam);
    model.fetch({
        success: function(model, response) {
            model.set(response);

            GlobalViewIni = new GlobalView({
                model: model
            });

            G2S.layout.content.show(GlobalViewIni);
        }
    });
} else {
    G2S.layout.content.show(GlobalViewIni);
}





/**
 * Prototipo (str.removeAccents())
 * usado para remover acentos de strings
 */
//Prototipos para remover acentos de string
var Latinise = {};
Latinise.latin_map = {"Á": "A", "Ă": "A", "Ắ": "A", "Ặ": "A", "Ằ": "A", "Ẳ": "A", "Ẵ": "A", "Ǎ": "A", "Â": "A", "Ấ": "A", "Ậ": "A", "Ầ": "A", "Ẩ": "A", "Ẫ": "A", "Ä": "A", "Ǟ": "A", "Ȧ": "A", "Ǡ": "A", "Ạ": "A", "Ȁ": "A", "À": "A", "Ả": "A", "Ȃ": "A", "Ā": "A", "Ą": "A", "Å": "A", "Ǻ": "A", "Ḁ": "A", "Ⱥ": "A", "Ã": "A", "Ꜳ": "AA", "Æ": "AE", "Ǽ": "AE", "Ǣ": "AE", "Ꜵ": "AO", "Ꜷ": "AU", "Ꜹ": "AV", "Ꜻ": "AV", "Ꜽ": "AY", "Ḃ": "B", "Ḅ": "B", "Ɓ": "B", "Ḇ": "B", "Ƀ": "B", "Ƃ": "B", "Ć": "C", "Č": "C", "Ç": "C", "Ḉ": "C", "Ĉ": "C", "Ċ": "C", "Ƈ": "C", "Ȼ": "C", "Ď": "D", "Ḑ": "D", "Ḓ": "D", "Ḋ": "D", "Ḍ": "D", "Ɗ": "D", "Ḏ": "D", "ǲ": "D", "ǅ": "D", "Đ": "D", "Ƌ": "D", "Ǳ": "DZ", "Ǆ": "DZ", "É": "E", "Ĕ": "E", "Ě": "E", "Ȩ": "E", "Ḝ": "E", "Ê": "E", "Ế": "E", "Ệ": "E", "Ề": "E", "Ể": "E", "Ễ": "E", "Ḙ": "E", "Ë": "E", "Ė": "E", "Ẹ": "E", "Ȅ": "E", "È": "E", "Ẻ": "E", "Ȇ": "E", "Ē": "E", "Ḗ": "E", "Ḕ": "E", "Ę": "E", "Ɇ": "E", "Ẽ": "E", "Ḛ": "E", "Ꝫ": "ET", "Ḟ": "F", "Ƒ": "F", "Ǵ": "G", "Ğ": "G", "Ǧ": "G", "Ģ": "G", "Ĝ": "G", "Ġ": "G", "Ɠ": "G", "Ḡ": "G", "Ǥ": "G", "Ḫ": "H", "Ȟ": "H", "Ḩ": "H", "Ĥ": "H", "Ⱨ": "H", "Ḧ": "H", "Ḣ": "H", "Ḥ": "H", "Ħ": "H", "Í": "I", "Ĭ": "I", "Ǐ": "I", "Î": "I", "Ï": "I", "Ḯ": "I", "İ": "I", "Ị": "I", "Ȉ": "I", "Ì": "I", "Ỉ": "I", "Ȋ": "I", "Ī": "I", "Į": "I", "Ɨ": "I", "Ĩ": "I", "Ḭ": "I", "Ꝺ": "D", "Ꝼ": "F", "Ᵹ": "G", "Ꞃ": "R", "Ꞅ": "S", "Ꞇ": "T", "Ꝭ": "IS", "Ĵ": "J", "Ɉ": "J", "Ḱ": "K", "Ǩ": "K", "Ķ": "K", "Ⱪ": "K", "Ꝃ": "K", "Ḳ": "K", "Ƙ": "K", "Ḵ": "K", "Ꝁ": "K", "Ꝅ": "K", "Ĺ": "L", "Ƚ": "L", "Ľ": "L", "Ļ": "L", "Ḽ": "L", "Ḷ": "L", "Ḹ": "L", "Ⱡ": "L", "Ꝉ": "L", "Ḻ": "L", "Ŀ": "L", "Ɫ": "L", "ǈ": "L", "Ł": "L", "Ǉ": "LJ", "Ḿ": "M", "Ṁ": "M", "Ṃ": "M", "Ɱ": "M", "Ń": "N", "Ň": "N", "Ņ": "N", "Ṋ": "N", "Ṅ": "N", "Ṇ": "N", "Ǹ": "N", "Ɲ": "N", "Ṉ": "N", "Ƞ": "N", "ǋ": "N", "Ñ": "N", "Ǌ": "NJ", "Ó": "O", "Ŏ": "O", "Ǒ": "O", "Ô": "O", "Ố": "O", "Ộ": "O", "Ồ": "O", "Ổ": "O", "Ỗ": "O", "Ö": "O", "Ȫ": "O", "Ȯ": "O", "Ȱ": "O", "Ọ": "O", "Ő": "O", "Ȍ": "O", "Ò": "O", "Ỏ": "O", "Ơ": "O", "Ớ": "O", "Ợ": "O", "Ờ": "O", "Ở": "O", "Ỡ": "O", "Ȏ": "O", "Ꝋ": "O", "Ꝍ": "O", "Ō": "O", "Ṓ": "O", "Ṑ": "O", "Ɵ": "O", "Ǫ": "O", "Ǭ": "O", "Ø": "O", "Ǿ": "O", "Õ": "O", "Ṍ": "O", "Ṏ": "O", "Ȭ": "O", "Ƣ": "OI", "Ꝏ": "OO", "Ɛ": "E", "Ɔ": "O", "Ȣ": "OU", "Ṕ": "P", "Ṗ": "P", "Ꝓ": "P", "Ƥ": "P", "Ꝕ": "P", "Ᵽ": "P", "Ꝑ": "P", "Ꝙ": "Q", "Ꝗ": "Q", "Ŕ": "R", "Ř": "R", "Ŗ": "R", "Ṙ": "R", "Ṛ": "R", "Ṝ": "R", "Ȑ": "R", "Ȓ": "R", "Ṟ": "R", "Ɍ": "R", "Ɽ": "R", "Ꜿ": "C", "Ǝ": "E", "Ś": "S", "Ṥ": "S", "Š": "S", "Ṧ": "S", "Ş": "S", "Ŝ": "S", "Ș": "S", "Ṡ": "S", "Ṣ": "S", "Ṩ": "S", "Ť": "T", "Ţ": "T", "Ṱ": "T", "Ț": "T", "Ⱦ": "T", "Ṫ": "T", "Ṭ": "T", "Ƭ": "T", "Ṯ": "T", "Ʈ": "T", "Ŧ": "T", "Ɐ": "A", "Ꞁ": "L", "Ɯ": "M", "Ʌ": "V", "Ꜩ": "TZ", "Ú": "U", "Ŭ": "U", "Ǔ": "U", "Û": "U", "Ṷ": "U", "Ü": "U", "Ǘ": "U", "Ǚ": "U", "Ǜ": "U", "Ǖ": "U", "Ṳ": "U", "Ụ": "U", "Ű": "U", "Ȕ": "U", "Ù": "U", "Ủ": "U", "Ư": "U", "Ứ": "U", "Ự": "U", "Ừ": "U", "Ử": "U", "Ữ": "U", "Ȗ": "U", "Ū": "U", "Ṻ": "U", "Ų": "U", "Ů": "U", "Ũ": "U", "Ṹ": "U", "Ṵ": "U", "Ꝟ": "V", "Ṿ": "V", "Ʋ": "V", "Ṽ": "V", "Ꝡ": "VY", "Ẃ": "W", "Ŵ": "W", "Ẅ": "W", "Ẇ": "W", "Ẉ": "W", "Ẁ": "W", "Ⱳ": "W", "Ẍ": "X", "Ẋ": "X", "Ý": "Y", "Ŷ": "Y", "Ÿ": "Y", "Ẏ": "Y", "Ỵ": "Y", "Ỳ": "Y", "Ƴ": "Y", "Ỷ": "Y", "Ỿ": "Y", "Ȳ": "Y", "Ɏ": "Y", "Ỹ": "Y", "Ź": "Z", "Ž": "Z", "Ẑ": "Z", "Ⱬ": "Z", "Ż": "Z", "Ẓ": "Z", "Ȥ": "Z", "Ẕ": "Z", "Ƶ": "Z", "Ĳ": "IJ", "Œ": "OE", "ᴀ": "A", "ᴁ": "AE", "ʙ": "B", "ᴃ": "B", "ᴄ": "C", "ᴅ": "D", "ᴇ": "E", "ꜰ": "F", "ɢ": "G", "ʛ": "G", "ʜ": "H", "ɪ": "I", "ʁ": "R", "ᴊ": "J", "ᴋ": "K", "ʟ": "L", "ᴌ": "L", "ᴍ": "M", "ɴ": "N", "ᴏ": "O", "ɶ": "OE", "ᴐ": "O", "ᴕ": "OU", "ᴘ": "P", "ʀ": "R", "ᴎ": "N", "ᴙ": "R", "ꜱ": "S", "ᴛ": "T", "ⱻ": "E", "ᴚ": "R", "ᴜ": "U", "ᴠ": "V", "ᴡ": "W", "ʏ": "Y", "ᴢ": "Z", "á": "a", "ă": "a", "ắ": "a", "ặ": "a", "ằ": "a", "ẳ": "a", "ẵ": "a", "ǎ": "a", "â": "a", "ấ": "a", "ậ": "a", "ầ": "a", "ẩ": "a", "ẫ": "a", "ä": "a", "ǟ": "a", "ȧ": "a", "ǡ": "a", "ạ": "a", "ȁ": "a", "à": "a", "ả": "a", "ȃ": "a", "ā": "a", "ą": "a", "ᶏ": "a", "ẚ": "a", "å": "a", "ǻ": "a", "ḁ": "a", "ⱥ": "a", "ã": "a", "ꜳ": "aa", "æ": "ae", "ǽ": "ae", "ǣ": "ae", "ꜵ": "ao", "ꜷ": "au", "ꜹ": "av", "ꜻ": "av", "ꜽ": "ay", "ḃ": "b", "ḅ": "b", "ɓ": "b", "ḇ": "b", "ᵬ": "b", "ᶀ": "b", "ƀ": "b", "ƃ": "b", "ɵ": "o", "ć": "c", "č": "c", "ç": "c", "ḉ": "c", "ĉ": "c", "ɕ": "c", "ċ": "c", "ƈ": "c", "ȼ": "c", "ď": "d", "ḑ": "d", "ḓ": "d", "ȡ": "d", "ḋ": "d", "ḍ": "d", "ɗ": "d", "ᶑ": "d", "ḏ": "d", "ᵭ": "d", "ᶁ": "d", "đ": "d", "ɖ": "d", "ƌ": "d", "ı": "i", "ȷ": "j", "ɟ": "j", "ʄ": "j", "ǳ": "dz", "ǆ": "dz", "é": "e", "ĕ": "e", "ě": "e", "ȩ": "e", "ḝ": "e", "ê": "e", "ế": "e", "ệ": "e", "ề": "e", "ể": "e", "ễ": "e", "ḙ": "e", "ë": "e", "ė": "e", "ẹ": "e", "ȅ": "e", "è": "e", "ẻ": "e", "ȇ": "e", "ē": "e", "ḗ": "e", "ḕ": "e", "ⱸ": "e", "ę": "e", "ᶒ": "e", "ɇ": "e", "ẽ": "e", "ḛ": "e", "ꝫ": "et", "ḟ": "f", "ƒ": "f", "ᵮ": "f", "ᶂ": "f", "ǵ": "g", "ğ": "g", "ǧ": "g", "ģ": "g", "ĝ": "g", "ġ": "g", "ɠ": "g", "ḡ": "g", "ᶃ": "g", "ǥ": "g", "ḫ": "h", "ȟ": "h", "ḩ": "h", "ĥ": "h", "ⱨ": "h", "ḧ": "h", "ḣ": "h", "ḥ": "h", "ɦ": "h", "ẖ": "h", "ħ": "h", "ƕ": "hv", "í": "i", "ĭ": "i", "ǐ": "i", "î": "i", "ï": "i", "ḯ": "i", "ị": "i", "ȉ": "i", "ì": "i", "ỉ": "i", "ȋ": "i", "ī": "i", "į": "i", "ᶖ": "i", "ɨ": "i", "ĩ": "i", "ḭ": "i", "ꝺ": "d", "ꝼ": "f", "ᵹ": "g", "ꞃ": "r", "ꞅ": "s", "ꞇ": "t", "ꝭ": "is", "ǰ": "j", "ĵ": "j", "ʝ": "j", "ɉ": "j", "ḱ": "k", "ǩ": "k", "ķ": "k", "ⱪ": "k", "ꝃ": "k", "ḳ": "k", "ƙ": "k", "ḵ": "k", "ᶄ": "k", "ꝁ": "k", "ꝅ": "k", "ĺ": "l", "ƚ": "l", "ɬ": "l", "ľ": "l", "ļ": "l", "ḽ": "l", "ȴ": "l", "ḷ": "l", "ḹ": "l", "ⱡ": "l", "ꝉ": "l", "ḻ": "l", "ŀ": "l", "ɫ": "l", "ᶅ": "l", "ɭ": "l", "ł": "l", "ǉ": "lj", "ſ": "s", "ẜ": "s", "ẛ": "s", "ẝ": "s", "ḿ": "m", "ṁ": "m", "ṃ": "m", "ɱ": "m", "ᵯ": "m", "ᶆ": "m", "ń": "n", "ň": "n", "ņ": "n", "ṋ": "n", "ȵ": "n", "ṅ": "n", "ṇ": "n", "ǹ": "n", "ɲ": "n", "ṉ": "n", "ƞ": "n", "ᵰ": "n", "ᶇ": "n", "ɳ": "n", "ñ": "n", "ǌ": "nj", "ó": "o", "ŏ": "o", "ǒ": "o", "ô": "o", "ố": "o", "ộ": "o", "ồ": "o", "ổ": "o", "ỗ": "o", "ö": "o", "ȫ": "o", "ȯ": "o", "ȱ": "o", "ọ": "o", "ő": "o", "ȍ": "o", "ò": "o", "ỏ": "o", "ơ": "o", "ớ": "o", "ợ": "o", "ờ": "o", "ở": "o", "ỡ": "o", "ȏ": "o", "ꝋ": "o", "ꝍ": "o", "ⱺ": "o", "ō": "o", "ṓ": "o", "ṑ": "o", "ǫ": "o", "ǭ": "o", "ø": "o", "ǿ": "o", "õ": "o", "ṍ": "o", "ṏ": "o", "ȭ": "o", "ƣ": "oi", "ꝏ": "oo", "ɛ": "e", "ᶓ": "e", "ɔ": "o", "ᶗ": "o", "ȣ": "ou", "ṕ": "p", "ṗ": "p", "ꝓ": "p", "ƥ": "p", "ᵱ": "p", "ᶈ": "p", "ꝕ": "p", "ᵽ": "p", "ꝑ": "p", "ꝙ": "q", "ʠ": "q", "ɋ": "q", "ꝗ": "q", "ŕ": "r", "ř": "r", "ŗ": "r", "ṙ": "r", "ṛ": "r", "ṝ": "r", "ȑ": "r", "ɾ": "r", "ᵳ": "r", "ȓ": "r", "ṟ": "r", "ɼ": "r", "ᵲ": "r", "ᶉ": "r", "ɍ": "r", "ɽ": "r", "ↄ": "c", "ꜿ": "c", "ɘ": "e", "ɿ": "r", "ś": "s", "ṥ": "s", "š": "s", "ṧ": "s", "ş": "s", "ŝ": "s", "ș": "s", "ṡ": "s", "ṣ": "s", "ṩ": "s", "ʂ": "s", "ᵴ": "s", "ᶊ": "s", "ȿ": "s", "ɡ": "g", "ᴑ": "o", "ᴓ": "o", "ᴝ": "u", "ť": "t", "ţ": "t", "ṱ": "t", "ț": "t", "ȶ": "t", "ẗ": "t", "ⱦ": "t", "ṫ": "t", "ṭ": "t", "ƭ": "t", "ṯ": "t", "ᵵ": "t", "ƫ": "t", "ʈ": "t", "ŧ": "t", "ᵺ": "th", "ɐ": "a", "ᴂ": "ae", "ǝ": "e", "ᵷ": "g", "ɥ": "h", "ʮ": "h", "ʯ": "h", "ᴉ": "i", "ʞ": "k", "ꞁ": "l", "ɯ": "m", "ɰ": "m", "ᴔ": "oe", "ɹ": "r", "ɻ": "r", "ɺ": "r", "ⱹ": "r", "ʇ": "t", "ʌ": "v", "ʍ": "w", "ʎ": "y", "ꜩ": "tz", "ú": "u", "ŭ": "u", "ǔ": "u", "û": "u", "ṷ": "u", "ü": "u", "ǘ": "u", "ǚ": "u", "ǜ": "u", "ǖ": "u", "ṳ": "u", "ụ": "u", "ű": "u", "ȕ": "u", "ù": "u", "ủ": "u", "ư": "u", "ứ": "u", "ự": "u", "ừ": "u", "ử": "u", "ữ": "u", "ȗ": "u", "ū": "u", "ṻ": "u", "ų": "u", "ᶙ": "u", "ů": "u", "ũ": "u", "ṹ": "u", "ṵ": "u", "ᵫ": "ue", "ꝸ": "um", "ⱴ": "v", "ꝟ": "v", "ṿ": "v", "ʋ": "v", "ᶌ": "v", "ⱱ": "v", "ṽ": "v", "ꝡ": "vy", "ẃ": "w", "ŵ": "w", "ẅ": "w", "ẇ": "w", "ẉ": "w", "ẁ": "w", "ⱳ": "w", "ẘ": "w", "ẍ": "x", "ẋ": "x", "ᶍ": "x", "ý": "y", "ŷ": "y", "ÿ": "y", "ẏ": "y", "ỵ": "y", "ỳ": "y", "ƴ": "y", "ỷ": "y", "ỿ": "y", "ȳ": "y", "ẙ": "y", "ɏ": "y", "ỹ": "y", "ź": "z", "ž": "z", "ẑ": "z", "ʑ": "z", "ⱬ": "z", "ż": "z", "ẓ": "z", "ȥ": "z", "ẕ": "z", "ᵶ": "z", "ᶎ": "z", "ʐ": "z", "ƶ": "z", "ɀ": "z", "ﬀ": "ff", "ﬃ": "ffi", "ﬄ": "ffl", "ﬁ": "fi", "ﬂ": "fl", "ĳ": "ij", "œ": "oe", "ﬆ": "st", "ₐ": "a", "ₑ": "e", "ᵢ": "i", "ⱼ": "j", "ₒ": "o", "ᵣ": "r", "ᵤ": "u", "ᵥ": "v", "ₓ": "x"};
String.prototype.removeAccents = function () {
    return this.replace(/[^A-Za-z0-9\[\] ]/g, function (a) {
        return Latinise.latin_map[a] || a;
    });
};
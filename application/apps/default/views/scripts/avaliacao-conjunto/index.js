/**
 * User: Débora Castro <debora.castro@unyleya.com.br>
 * Date: 21/10/2014
 */
/*******************************************************************************
 * Imports
 ******************************************************************************/



/*******************************************************************************
 *  Views
 ******************************************************************************/

$(function () {

    if (avaliacaoconjunto != null) {
        var model = new AvaliacaoConjunto(avaliacaoconjunto);
    } else {
        var model = new AvaliacaoConjunto();
    }

    var view = new AvaliacaoConjuntoView({model: model});
    G2S.layout.content.show(view);
});
loaded();

$.ajaxSetup({'async': true});


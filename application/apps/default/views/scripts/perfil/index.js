var comboResultSituacao = null;
var comboSistema = null;
var comboPerfilPedagogico = null;

var ResultsModel = Backbone.Model.extend({
    idAttribute: 'id_perfil',
    defaults: {
        id_perfil: null,
        st_nomeperfil: '',
        id_situacao: null,
        id_entidade: null,
        id_sistema: null,
        id_perfilpedagogico: null,
        id_entidadecadastro: null,
        id_entidadeclasse: null,
        bl_padrao: false,
        bl_ativo: true,
        // Strings usados somente para as Views, nao para salvar.
        st_nomeperfilentidade: '',
        st_situacao: '',
        st_nomentidade: '',
        st_sistema: '',
        st_entidadeclasse: '',
        st_perfilpedagogico: '',
        // Array de organizacoes
        organizacao_selected: [],
        funcionalidade_selected: [],
        permissao_selected: [],
        // Usado como referencia ao duplicar, pra buscar parmissoes
        id_duplicado: null
    },
    url: function () {
        return '/api/perfil/' + (this.id || '');
    }
});
var ResultsView = Marionette.ItemView.extend({
    model: new ResultsModel(),
    template: '#results-view',
    tagName: 'tr',
    className: 'result-item',
    onRender: function () {
        // Defini o id_duplicado para referencia dos JSTREE e Permissoes
        var idPerfil = this.model.get('id_perfil');
        if (idPerfil)
            this.model.set('id_duplicado', idPerfil);
    },
    events: {
        'click .openUsers': 'openUsers',
        'click .openSettings': 'openSettings',
        'click .actionClone': 'actionClone',
        'click .actionDelete': 'actionDelete'
    },
    openUsers: function () {
        loading();
        G2S.layout.content.show(new UsuarioCompositeView(this.model.toJSON()));
    },
    openSettings: function () {
        var View = new EditView({
            model: this.model
        });
        G2S.layout.content.show(View);
    },
    actionClone: function () {
        var NewModel = this.model.clone();
        var values = {
            id_perfil: null,
            st_nomeperfil: NewModel.get('st_nomeperfil') + ' - Copia'
        };
        NewModel.set(values);

        this.model.collection.add(NewModel);
        this.$el.closest('tbody').find('.result-item:last .openSettings').trigger('click');
    },
    actionDelete: function () {
        var that = this;

        loading();
        $.ajax({
            url: '/api/usuario-perfil-entidade/?id_perfil=' + that.model.get('id_perfil'),
            success: function (response) {
                loaded();
                var mensagem = response.length ? 'Existem usuários cadastrados. Deseja realmente excluir?' : 'Deseja realmente excluir este perfil?';
                bootbox.confirm(mensagem, function (response) {
                    if (response) {
                        that.model.destroy();
                    }
                });
            }
        });

    }
});

var ResultsEmpty = Marionette.ItemView.extend({
    tagName: 'tr',
    className: 'empty-view',
    template: _.template('<td colspan="5">Nenhum registro encontrado.</td>')
});

var ResultsCollection = Backbone.Collection.extend({
    model: ResultsModel,
    url: '/api/perfil/',
    parse: function (response) {
        return (response.type ? response.mensagem : response);
    }
});

var ResultsCompositeView = Marionette.CompositeView.extend({
    template: '#results-template',
    collection: new ResultsCollection(),
    childView: ResultsView,
    emptyView: ResultsEmpty,
    childViewContainer: 'tbody',
    onRender: function () {
        this.collection.fetch({
            reset: true,
            complete: loaded
        });
    },
    events: {
        'click .actionAdd': 'actionAdd',
        'keyup [name="perfil_search"]': 'filterProfiles'
    },
    actionAdd: function () {
        loading();

        var View = new EditView();
        G2S.layout.content.show(View);
    },
    filterProfiles: function (e) {
        var elem = this.$el;
        var input = $(e.target);
        var value = input.val();

        var items = elem.find('.result-item');
        items.each(function () {
            var user = $(this);
            if (!user.attr('data-label')) {
                user.attr('data-label', user.find('td:first').text().removeAccents().trim().toLowerCase());
            }
        });

        if (value.trim() === '') {
            items.show();
        } else {
            var toShow = items.filter('[data-label^="' + value.removeAccents().trim().toLowerCase() + '"]');
            items.not(toShow).hide();
            toShow.show();
        }

    }
});

var EditView = Marionette.ItemView.extend({
    model: new ResultsModel(),
    template: '#edit-view',
    tagName: 'div',
    onRender: function () {
        var that = this;
        var elem = this.$el;

        // Montar SELECT "Situação"
        ComboboxView({
            el: elem.find('#id_situacao'),
            url: '/api/situacao/?st_tabela=tb_perfil',
            collection: comboResultSituacao,
            optionLabel: 'st_situacao',
            optionSelectedId: that.model.get('id_situacao'),
            emptyOption: '[Selecione]',
            model: Backbone.Model.extend({
                idAttribute: 'id_situacao',
                defaults: {
                    id_situacao: null,
                    st_situacao: ''
                }
            })
        }, function (el, composite) {
            if (!comboResultSituacao && composite.collection && composite.collection.models)
                comboResultSituacao = composite.collection.models;
        });

        // Montar SELECT "Sistema"
        ComboboxView({
            el: elem.find('#id_sistema'),
            url: '/api/sistema?bl_ativo=1',
            collection: comboSistema,
            optionLabel: 'st_sistema',
            optionSelectedId: that.model.get('id_sistema'),
            emptyOption: '[Selecione]',
            model: Backbone.Model.extend({
                idAttribute: 'id_sistema',
                defaults: {
                    id_sistema: null,
                    st_sistema: '',
                    bl_ativo: false
                }
            })
        }, function (el, composite) {
            if (!comboSistema && composite.collection && composite.collection.models)
                comboSistema = composite.collection.models;
            that.loadPermissions();
        });

        // Montar SELECT "Perfil Pedagógco"
        ComboboxView({
            el: elem.find('#id_perfilpedagogico'),
            url: '/api/perfil-pedagogico?order_by=st_perfilpedagogico',
            collection: comboPerfilPedagogico,
            optionLabel: 'st_perfilpedagogico',
            optionSelectedId: that.model.get('id_perfilpedagogico'),
            emptyOption: '[Selecione]',
            model: Backbone.Model.extend({
                idAttribute: 'id_perfilpedagogico',
                defaults: {
                    id_perfilpedagogico: null,
                    st_perfilpedagogico: ''
                }
            })
        }, function (el, composite) {
            if (!comboPerfilPedagogico && composite.collection && composite.collection.models)
                comboPerfilPedagogico = composite.collection.models;
        });


        // Árvore Entidades / Organizações
        var jstreeElem = elem.find('#organizacao-content');
        jstreeElem.jstree({
            html_data: {
                ajax: {
                    url: 'util/arvore-entidade'
                }
            },
            plugins: ['themes', 'html_data', 'checkbox', 'sort', 'ui']
        }).on('loaded.jstree', function () {

            jstreeElem.jstree('btn_check_all');

            var idPerfil = that.model.get('id_duplicado');
            if (idPerfil) {
                $.getJSON('/perfil/get-arvore-selected/?id_perfil=' + idPerfil, function (response) {
                    response = response.mensagem;

                    if (response.length) {
                        for (var i in response) {
                            var idEntidade = response[i].id_entidade;
                            jstreeElem.jstree('check_node', 'li#' + idEntidade);
                        }
                    }
                });
            }
        });


        // Monta tree das permissoes
        var PermissaoModel = Backbone.Model.extend({});
        var PermissaoView = Marionette.ItemView.extend({
            model: new PermissaoModel(),
            template: '#permissao-view',
            tagName: 'div',
            className: 'row-fluid'
        });
        var PermissaoCollection = Backbone.Collection.extend({
            model: PermissaoModel,
            url: '/perfil/get-permissao-perfil/?id_perfil=' + that.model.get('id_duplicado'),
            parse: function (data) {
                if (data.tipo && data.tipo != 1) {
                    $.pnotify({'type': data.tipo, 'text': data.mensagem});
                }
                return data.mensagem || data;
            }
        });

        var PermissaoCompositeView = Marionette.CompositeView.extend({
            template: '#permissao-template',
            collection: new PermissaoCollection(),
            childView: PermissaoView,
            childViewContainer: '.permissao-collection',
            onRender: function () {
            }
        });

        var PermissaoIni = new PermissaoCompositeView();
        PermissaoIni.collection.fetch({
            success: function () {
                elem.find('#permissao-content').html(PermissaoIni.render().$el.html());
            }
        });

        loaded();
    },
    events: {
        'submit form': 'actionSave',
        'click .actionBack': 'actionBack',
        'change [name="id_sistema"]': 'loadPermissions',
        'click [name="permissao[]"]': 'checkPermissionsRules'
    },
    actionBack: function () {
        this.model.set(this.model.defaults);
        G2S.layout.content.show(new ResultsCompositeView());
    },
    actionSave: function (e) {
        e.preventDefault();

        var that = this;
        var elem = this.$el;

        var values = {
            st_nomeperfil: elem.find('[name="st_nomeperfil"]').val(),
            id_situacao: Number(elem.find('[name="id_situacao"]').val()),
            // st_nomeperfilentidade: elem.find('[name="st_nomeperfilentidade"]').val(),
            id_sistema: Number(elem.find('[name="id_sistema"]').val()),
            id_perfilpedagogico: Number(elem.find('[name="id_perfilpedagogico"]').val()),
            organizacao_selected: [],
            funcionalidade_selected: [],
            permissao_selected: []
        };

        // Validacao dos campos
        var errorMsg = null;
        if (values.st_nomeperfil === '')
            errorMsg = 'Insira o nome do perfil.';
        else if (values.id_situacao === '')
            errorMsg = 'Informe a situação do perfil.';
        else if (values.id_sistema === '')
            errorMsg = 'Informe o sistema.';

        if (errorMsg) {
            $.pnotify({
                type: 'error',
                title: 'Campos Obrigatórios',
                text: errorMsg
            });
            return false;
        }

        // Buscar Organizacoes no JSTREE
        elem.find('#organizacao-content').jstree('get_checked', null, true).each(function () {
            values.organizacao_selected.push(this.id);
        });

        // Buscar Funcionalidades no JSTREE aba PERMISSOES
        elem.find('#funcionalidade-content').jstree('get_checked', null, true).each(function () {
            values.funcionalidade_selected.push(this.id);
        });

        // Buscar Permissoes marcadas aba PERMISSOES
        elem.find('#permissao-content input:checked').each(function () {
            var input = $(this);
            values.permissao_selected.push({
                id_funcionalidade: input.attr('data-id-funcionalidade'),
                id_permissao: input.attr('data-id-permissao')
            });
        });

        loading();

        this.model.save(values, {
            success: function (model, response) {
                $.pnotify({
                    type: response.type,
                    title: response.title,
                    text: response.text
                });
                that.actionBack();
            },
            error: function (model, response) {
                $.pnotify({
                    type: response.responseJSON.type,
                    title: response.responseJSON.title,
                    text: response.responseJSON.text
                });
            },
            complete: function () {
                loaded();
            }
        });

    },
    loadPermissions: function () {
        var that = this;
        var elem = this.$el;

        var jstreeElem = elem.find('#funcionalidade-content');
        jstreeElem.jstree({
            html_data: {
                ajax: {
                    url: '/perfil/get-arvore-entidade-funcionalidade?id_sistema=' + elem.find('[name="id_sistema"]').val()
                }
            },
            plugins: ['themes', 'html_data', 'checkbox', 'sort', 'ui']
        }).bind('loaded.jstree', function () {

            jstreeElem.jstree('btn_check_all');

            var idPerfil = that.model.get('id_duplicado');
            if (idPerfil) {
                $.getJSON('/perfil/get-funcionalidade-selected/?id_perfil=' + idPerfil, function (response) {
                    response = response.mensagem;

                    if (response.length) {
                        for (var i in response) {
                            var idFuncionalidade = response[i].id_funcionalidade;
                            jstreeElem.jstree('check_node', 'li#' + idFuncionalidade);
                        }
                    }
                });
            }
        });

    },

    checkPermissionsRules: function (event) {
        // Aba Financeiro -> Permissão: 31, Funcionalidade: 286
        // Aba Financeiro Visualizar -> Permissão: 47, Funcionalidade: 286
        var input = $(event.target),
            idPermissao = input.attr('data-id-permissao'),
            idFuncionalidade = input.attr('data-id-funcionalidade'),
            isChecked = input.is(':checked');

        if (idFuncionalidade == 286 && (idPermissao == 31 || idPermissao == 47)) {
            var elem = this.$el,
                permissaoParam;
            idPermissao == 31 ? permissaoParam = 47 : permissaoParam = 31
            if (isChecked) {
                var permissaoExcludente = elem.find("input[data-id-permissao='" + permissaoParam + "'][data-id-funcionalidade='" + idFuncionalidade + "']");
                var actualStatus = permissaoExcludente.is(':checked');
                permissaoExcludente.attr('checked', false);

                if (actualStatus) {
                    $.pnotify({
                        title: 'Alerta',
                        type: 'warning',
                        text: 'Apenas uma permissão pode ser marcada.'
                    });
                }
            }
        }
    }
});


//USUARIOS

var comboUsuarioSituacao = null;
var comboUsuarioPerfilEntidade = null;

var UsuarioModel = Backbone.Model.extend({
    idAttribute: 'id_usuario',
    defaults: {
        id_usuario: null,
        id_situacao: null,
        id_entidade: [],
        id_perfil: null,
        bl_ativo: true,
        dt_inicio: null,
        dt_termino: null,
        dt_cadastro: null,
        st_nomecompleto: null,
        st_situacaoperfil: null,
        st_nomeentidade: null,
        entidades_remover: []
    },
    url: function () {
        return '/api/usuario-perfil-entidade/' + (this.id || '');
    }
});

var UsuarioView = Marionette.ItemView.extend({
    model: new UsuarioModel(),
    template: '#usuario-view',
    tagName: 'tr',
    className: 'result-item',
    isEditing: false,
    onRender: function () {
        var that = this;
        var elem = this.$el;

        if (this.isEditing) {
            // Buscar COLLECTION de UsuarioPerfilEntidade
            if (!comboUsuarioPerfilEntidade) {
                $.ajaxSetup({async: false});
                $.get('/perfil/get-perfil-entidade?id_perfil=' + that.model.get('id_perfil'), function (response) {
                    comboUsuarioPerfilEntidade = response.mensagem;
                });
                $.ajaxSetup({async: true});
            }

            // Montar SELECT "Situação"
            ComboboxView({
                el: elem.find('[name="id_situacao"]'),
                url: '/api/situacao/?st_tabela=tb_usuarioperfilentidade',
                collection: comboUsuarioSituacao,
                optionLabel: 'st_situacao',
                optionSelectedId: that.model.get('id_situacao'),
                emptyOption: '[Selecione]',
                model: Backbone.Model.extend({
                    idAttribute: 'id_situacao',
                    defaults: {
                        id_situacao: null,
                        st_situacao: ''
                    }
                })
            }, function (el, composite) {
                if (!comboUsuarioSituacao && composite.collection && composite.collection.models)
                    comboUsuarioSituacao = composite.collection.models;
            });


            // Montar SELECT "Unidade"
            var unidadesIdInt = that.model.get('id_entidade');
            unidadesIdInt = unidadesIdInt.map(Number);

            var UnidadeModel = Backbone.Model.extend({
                idAttribute: 'id_entidade',
                defaults: {
                    id_entidade: null,
                    id_perfil: null,
                    st_nomeperfil: '',
                    st_nomeentidade: ''
                }
            });
            var UnidadeView = Marionette.ItemView.extend({
                template: '#unidade-view',
                model: new UnidadeModel(),
                tagName: 'label',
                className: 'checkbox',
                onRender: function () {
                    var elem = this.$el;

                    if (unidadesIdInt.indexOf(this.model.id) != -1) {
                        elem.find('input').attr('checked', 'checked');
                    }
                }
            });
            var UnidadeCollection = Backbone.Collection.extend({
                model: UnidadeModel,
                url: '/perfil/get-perfil-entidade?id_perfil=' + that.model.get('id_perfil'),
                parse: function (data) {
                    if (data.tipo && data.tipo != 1) {
                        $.pnotify({'type': data.tipo, 'text': data.mensagem});
                    }
                    return data.mensagem || data;
                }
            });
            var UnidadeCompositeView = Marionette.CompositeView.extend({
                template: _.template(''),
                collection: new UnidadeCollection(comboUsuarioPerfilEntidade),
                childView: UnidadeView
            });

            var UnidadeIni = new UnidadeCompositeView();
            elem.find('.list-entidade').html(UnidadeIni.render().$el.html());
        }
    },
    events: {
        'click .actionEdit': 'actionEdit',
        'click .actionSave': 'actionSave',
        'click .actionDelete': 'actionDelete',
        'change .select_id_usuario': 'changeIdUsuario',
        'keyup [name="st_nomecompleto"]': 'searchUser',
        'blur [name="st_nomecompleto"]': 'verifyIdUsuario'
    },
    changeIdUsuario: function (e) {
        var elem = this.$el;
        var input = $(e.target);
        var value = Number(input.val());
        var option = input.find('option[value="' + value + '"]');

        var usuarioExiste = this.model.collection.findWhere({
            id_usuario: value
        });
        if (usuarioExiste && value != this.model.get('id_usuario')) {
            $.pnotify({
                type: 'error',
                title: 'Erro',
                text: 'Este usuário já encontra-se cadastrado'
            });
            elem.find('.select_id_usuario').html('');
            return;
        }

        elem.find('[name="st_nomecompleto"]').val(option.html());
        elem.find('[name="id_usuario"]').val(value);
        input.hide();
    },
    verifyIdUsuario: function (e) {
        var elem = this.$el;

        setTimeout(function () {
            var input = $(e.target);
            var combo = elem.find('.select_id_usuario');

            if (combo.val() === null || combo.val() === '') {
                input.val('');
                combo.hide();
            }
        }, 100);
    },
    actionEdit: function () {
        if (this.isEditing) {
            if (this.model.id) {
                this.template = '#usuario-view';
            } else {
                this.actionDelete();
                return;
            }
        } else {
            this.template = '#usuario-edit';
        }

        this.isEditing = !this.isEditing;
        this.render();
    },
    actionSave: function () {
        var that = this;
        var elem = this.$el;

        var st_nomeentidade = [];
        elem.find('[name="id_entidade[]"]:checked').each(function () {
            st_nomeentidade.push($(this).closest('label').text().trim());
        });
        var values = {
            id_situacao: elem.find('[name="id_situacao"]').val(),
            id_usuario: elem.find('[name="id_usuario"]').val(),
            id_entidade: [],
            //dt_inicio: elem.find('[name="dt_inicio"]').val(),
            //dt_termino: elem.find('[name="dt_termino"]').val(),
            // PARA MOSTRAR NA VIEW
            st_nomecompleto: elem.find('[name="st_nomecompleto"]').val(),
            st_situacaoperfil: elem.find('[name="id_situacao"] option:selected').html(),
            st_nomeentidade: st_nomeentidade.join(', ')
        };

        // Pegar Unidades selecionadas
        elem.find('[name="id_entidade[]"]:checked').each(function () {
            values.id_entidade.push($(this).val());
        });

        // Validacao dos campos
        var errorMsg = null;
        if (values.id_usuario === '' || values.id_usuario === null)
            errorMsg = 'Selecione um usuário.';
        //else if (values.dt_inicio === '' || values.dt_inicio === null)
        //    errorMsg = 'Informe a data de início.';
        else if (values.id_situacao === '' || values.id_situacao === null)
            errorMsg = 'Informe a situação do usuário.';
        else if (values.id_entidade.length == 0)
            errorMsg = 'Selecione uma unidade.';

        if (errorMsg) {
            $.pnotify({
                type: 'error',
                title: 'Campos Obrigatórios',
                text: errorMsg
            });
            return false;
        }

        loading();

        this.model.save(values, {
            success: function (model, response) {
                loaded();
                if (values.id_entidade === '' || values.id_entidade === null) {
                    that.model.collection.remove(that.model);
                } else {
                    that.actionEdit();
                }
                ;
                $.pnotify({
                    text: response.text,
                    type: response.type,
                    icon: true
                });
            },
            error: function (model, response) {
                loaded();
                $.pnotify({
                 text: response.responseJSON.text,
                 type: 'error',
                 icon: false
                });
            }
        });
    },
    actionDelete: function () {
        var that = this;
        var elem = this.$el;

        bootbox.confirm('Deseja realmente remover este usuário?', function (response) {
            if (response) {
                if (that.model.id) {

                    var values = {
                        id_entidade: []
                    };

                    loading();

                    that.model.save(values, {
                        success: function (model, response) {
                            loaded();
                            $.pnotify({
                                type: response.type,
                                title: response.title,
                                text: 'O usuário foi removido.'
                            });
                        }
                    });
                }

                that.model.collection.remove(that.model);
            }
        });
    },
    isSearching: null,
    searchStr: null,
    searchUser: function (e) {
        var that = this;
        var elem = this.$el;

        var input = $(e.target);
        var value = input.val().trim();
        var ComboElement = elem.find('.select_id_usuario');

        /**
         * KeyCode 40 = Arrow Down
         * Para descer no COMBOBOX
         */
        if (e.keyCode == 40) {
            ComboElement.focus();
        }


        /**
         * KeyCode 48 até 90 = 0 (zero) até Z (letra Z)
         * KeyCode 8 = backspace (apagar)
         */
        if (((e.keyCode >= 48 && e.keyCode <= 90) || e.keyCode == 8) && value != this.searchStr && value.length >= 7) {
            if (this.isSearching !== null)
                this.isSearching.abort();

            // Limpa o campo com o id do usario
            elem.find('[name="id_usuario"]').val('');

            this.isSearching = $.ajax({
                url: '/perfil/get-pessoas-disponiveis-perfil/',
                data: {id_perfil: that.model.get('id_perfil'), st_nomecompleto: value},
                success: function (response) {
                    if (response.type == 'success') {
                        response = response.mensagem;

                        ComboElement.show();
                        var SelectModel = Backbone.Model.extend({
                            idAttribute: 'id_usuario',
                            defaults: {
                                id_usuario: null,
                                st_nomecompleto: null
                            }
                        });

                        ComboboxView({
                            el: ComboElement,
                            model: SelectModel,
                            collection: response,
                            optionLabel: 'st_nomecompleto'
                        });
                    } else {
                        ComboElement.hide();
                    }
                }
            });
            this.searchStr = value;
        }
    }
});

var UsuarioEmpty = Marionette.ItemView.extend({
    tagName: 'tr',
    template: _.template('<td colspan="4">Nenhum registro encontrado.</td>')
});

var UsuarioCollection = Backbone.Collection.extend({
    model: UsuarioModel,
    url: '/api/usuario-perfil-entidade/',
    parse: function (data) {
        if (data.tipo && data.tipo != 1) {
            $.pnotify({'type': data.tipo, 'text': data.mensagem});
        }
        return data.mensagem || data;
    }
});

var UsuarioCompositeView = Marionette.CompositeView.extend({
    template: '#usuario-template',
    collection: new UsuarioCollection(),
    childView: UsuarioView,
    emptyView: UsuarioEmpty,
    childViewContainer: 'tbody',
    initialize: function (params) {
        this.st_nomeperfil = params.st_nomeperfil || '';
        this.collection.url = '/api/usuario-perfil-entidade/?id_perfil=' + params.id_perfil;
        this.id_perfil = params.id_perfil || null;
    },
    onRender: function () {
        this.$el.find('legend > span').html(this.st_nomeperfil);
        this.collection.fetch({
            reset: true,
            success: function () {
                loaded();
            }
        });
        comboUsuarioSituacao = null;
        comboUsuarioPerfilEntidade = null;
    },
    events: {
        'click .actionAdd': 'actionAdd',
        'click .actionBack': 'actionBack',
        'keyup [name="usuario_search"]': 'filterUsers'
    },
    actionAdd: function () {
        var Model = new UsuarioModel();
        Model.set('id_perfil', this.id_perfil);
        this.collection.add(Model);

        // HIGHLIGHT ITEM NA TABELA
        var ViewAdded = this.$el.find('.result-item:last');

        if (ViewAdded.length) {
            ViewAdded.addClass('warning').trigger('hover').css('outline', '#657991 solid 3px');

            ViewAdded.find('.actionEdit').trigger('click');
            $('html, body').animate({'scrollTop': ViewAdded.offset().top}, function () {
                ViewAdded.find('input:first').focus();
            });

            setTimeout(function () {
                ViewAdded.removeClass('warning').css('outline', 'none');
            }, 4000);
        }
    },
    filterUsers: function (e) {
        var elem = this.$el;
        var input = $(e.target);
        var value = input.val();

        var usuarios = elem.find('.result-item');
        usuarios.each(function () {
            var user = $(this);
            if (!user.attr('data-label')) {
                user.attr('data-label', user.find('td:first').text().removeAccents().trim().toLowerCase());
            }
        });

        if (value.trim() === '') {
            usuarios.show();
        } else {
            var toShow = usuarios.filter('[data-label^="' + value.removeAccents().trim().toLowerCase() + '"]');
            usuarios.not(toShow).hide();
            toShow.show();
        }

    },
    actionBack: function () {
        G2S.layout.content.show(new ResultsCompositeView());
    }
});


loading();
G2S.layout.content.show(new ResultsCompositeView());

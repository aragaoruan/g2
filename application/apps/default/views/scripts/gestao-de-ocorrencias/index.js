var LayoutGestaoOcorrencias = Marionette.LayoutView.extend({
    template: '#gestOcorrTemplate',
    ui: {
        //INPUTS
        //nomeGestOcorr: '#nomeGestOcorr',
        //emailGestOcorr: '#emailGestOcorr',
        //codigoGesOcorr: '#codigoGestOcorr',
        id_nucleo: '#id_nucleoco',
        id_usuarioresponsavel: '#id_usuarioresponsavel',
        id_assuntoco: '#id_assuntoco',
        id_subassunto: '#id_subassunto',
        id_evolucao: '#id_evolucao',
        id_situacao: '#id_situacao',
        hr_inicio: '#hr_inicio',
        hr_fim: '#hr_fim',
        dt_inicio: '#dt_inicio',
        dt_fim: '#dt_fim'
    },
    regions: {
        pesquisaGestOcorr: '#pesquisaGestOcorr'
    },
    events: {
        'change @ui.id_nucleo' : 'carregarAtendenteEAssunto',
        'change @ui.id_assuntoco': 'carregarSubAssunto',
        'change @ui.id_subassunto': 'preparaAssunto',
        'submit form': 'actionSearch',
        'click #xlsAnalitico': 'geraXLSAnalitico',
        'click #xlsSintetico': 'gerarXLSSintetico'
    },
    geraXLSAnalitico: function(){

        var dados = [];

        /* fullCcollection necessário para iterar sobre TODOS os elementos da
        Backbone.PageableCollection, e não só os que estão sendo exibidos na
        página.
        */
        gestaoDeOcorrenciasCollection.fullCollection.each(function(model) {
            dados.push({
                id_ocorrencia: model.get('id_ocorrencia'),
                st_assuntocopai : model.get('st_assuntocopai'),
                st_assuntoco: model.get('st_assuntoco'),
                st_nomeinteressado: model.get('st_nomeinteressado'),
                st_projetopedagogico: model.get('st_projetopedagogico'),
                st_nomeresponsavel: model.get('st_nomeresponsavel'),
                st_evolucao: model.get('st_evolucao'),
                st_situacao: model.get('st_situacao'),
                dt_cadastro: model.get('dt_cadastro'),
                st_ultimotramite: model.get('st_ultimotramite')});
        });
        var cabecalho = {
                id_ocorrencia: 'ID',
                st_assuntocopai: 'Assunto',
                st_assuntoco: 'Subassunto',
                st_nomeinteressado: 'Nome Interessado',
                st_projetopedagogico: 'Curso',
                st_nomeresponsavel: 'Nome Responsável',
                st_evolucao: 'Evolução',
                st_situacao: 'Situação',
                dt_cadastro: 'Criação',
                st_ultimotramite: 'Última Interação'
        };

        $.ajax({
            url: "/relatorio/gerar-xls",
            dataType: 'html',
            type: 'post',
            data: {
                'dados': JSON.stringify(dados),
                'cabecalho': JSON.stringify(cabecalho),
                'converterUtf8': true
            },
            success: function (resp) {
                var win = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(resp), "", "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=780, height=200, top=0, left=0");
            },
            complete: function (resp) {

            }
        })
    },
    gerarXLSSintetico: function(){

        var dados = [];
        var pendentes = 0;
        var emAndamento = 0;
        var encerradas = 0;
        var aguardandoEncerramento = 0;
        var totalOcorrencias = 0;

        var cabecalho = { totalOcorrencias: 'Total De Ocorrências' };
        var projetos = {};

        /* fullCcollection necessário para iterar sobre TODOS os elementos da
         Backbone.PageableCollection, e não só os que estão sendo exibidos na
         página.
         */

        gestaoDeOcorrenciasCollection.fullCollection.each(function(model){

            var projetoIndex = (model.get('st_projetopedagogico') + "").toString().removeAccents().trim();

            if(!projetoIndex) {
                projetoIndex = 'sem_curso';
            }

            if (typeof projetos[projetoIndex]  == 'undefined'){
                projetos[projetoIndex] = 0;
            }

            projetos[projetoIndex]++;

            cabecalho[projetoIndex] = model.get('st_projetopedagogico') || '- SEM CURSO -';

            switch(model.get('id_situacao')){
                case 97:
                    pendentes++;
                    break;
                case 98:
                    emAndamento++;
                    break;
                case 99:
                    encerradas++;
                    break;
                default:
                    aguardandoEncerramento++;
                    break;
            }
            totalOcorrencias++;
        });

        cabecalho.pendentes = 'Pendentes';
        cabecalho.emAndamento = 'Em Andamento';
        cabecalho.encerradas = 'Encerradas';
        cabecalho.aguardandoEncerramento = 'Aguardando Encerramento';

        dados[0] = $.extend({}, projetos, {
            totalOcorrencias: totalOcorrencias,
            pendentes: pendentes,
            emAndamento: emAndamento,
            encerradas: encerradas,
            aguardandoEncerramento: aguardandoEncerramento
        });

        $.ajax({
            url: "/relatorio/gerar-xls",
            dataType: 'html',
            type: 'post',
            data: {
                'dados': JSON.stringify(dados),
                'cabecalho': JSON.stringify(cabecalho),
                'converterUtf8': true
            },
            success: function (resp) {
                var win = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(resp), "", "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=780, height=200, top=0, left=0");
            },
            complete: function (resp) {

            }
        })
    },
    carregarAtendenteEAssunto: function(){
        var that = this;

        ComboboxView({
            el: that.ui.id_usuarioresponsavel,
            url: '/ocorrencia/find-atendente?params[id_nucleoco]=' + that.ui.id_nucleo.val(),
            optionLabel: 'st_nomecompleto',
            emptyOption: '- Selecione Atendente -',
            model: Backbone.Model.extend({
                idAttribute: 'id_usuario'
            })
        });
        ComboboxView({
            el: that.ui.id_assuntoco,
            url: '/default/ocorrencia-aluno/find-assunto/id_tipoocorrencia',
            optionLabel: 'st_assuntoco',
            //emptyOption: '[Selecione]',
            model: Backbone.Model.extend({
                idAttribute: 'id_assuntoco'
            })
        });
    },
    preparaAssunto: function(e) {
        var elem = $(e.currentTarget);
        var value = elem.val().toString().trim();
        if (value)
            this.ui.id_subassunto.attr('name', 'id_assuntoco');
        else
            this.ui.id_subassunto.attr('name', 'id_subassuntoco');
    },
    actionSearch: function(e){

        var that = this;
        e.preventDefault();

        if (this.validaIntervaloMeses()) {
            /* Limpa uma possível grid, mensagem de alerta
             * ou paginator previamente existente.
             */
            $('.gestaoDeOcorrResult').remove();
            $('.backgrid-paginator').remove();

            var values = Backbone.Syphon.serialize(this);
            values.id_entidade = G2S.loggedUser.get('id_entidade');

            // Concatena os valores de hora e data e faz a conversão de acordo com o padrão SQL.
            values.dt_inicio = values.dt_inicio.split('/').reverse().join('-') + ' ' + values.hr_inicio ;
            values.dt_fim = values.dt_fim.split('/').reverse().join('-') + ' ' + values.hr_fim;

            //Removendo attrs não existentes na VW.
            delete values.id_subassunto;
            delete values.hr_inicio;
            delete values.hr_fim;

            //Fetch da Collection
            gestaoDeOcorrenciasCollection.fetch({

                beforeSend: function() {
                    loading();
                },

                data: values,

                //Caso o fetch seja bem sucedido:
                success: function(response){
                    //Se houverem models na collection, monta a grid.
                    if (gestaoDeOcorrenciasCollection.length != 0) {
                        var gestaoDeOcorrenciasGrid = new Backgrid.Grid({
                            columns: [
                                {name: 'id_ocorrencia', label: 'ID', editable: false, cell: 'string'},
                                {name: 'st_assuntocopai', label: 'Assunto', editable: false, cell: 'string'},
                                {name: 'st_assuntoco', label: 'Subassunto', editable: false, cell: 'string'},
                                {name: 'st_nomeinteressado', label: 'Nome Interessado', editable: false, cell: 'string'},
                                {name: 'st_projetopedagogico', label: 'Curso', editable: false, cell: 'string'},
                                {name: 'st_nomeresponsavel', label: 'Nome Responsável', editable: false, cell: 'string'},
                                {name: 'st_evolucao', label: 'Evolução', editable: false, cell: 'string'},
                                {name: 'st_situacao', label: 'Situação', editable: false, cell: 'string'},
                                {name: 'st_cadastro', label: 'Criação', editable: false, cell: 'string'},
                                {name: 'st_ultimotramite', label: 'Última Interação', editable: false, cell: 'string'}
                            ],
                            collection: gestaoDeOcorrenciasCollection,
                            className: 'backgrid table table-bordered gestaoDeOcorrResult'
                        });

                        var gestaoDeOcorrenciasGridPaginator = new Backgrid.Extension.Paginator({
                            collection: gestaoDeOcorrenciasCollection
                        });

                        //Renderiza a grid e o paginator após a tela de pesquisa.
                        $('#pesquisaGestOcorr').append(gestaoDeOcorrenciasGrid.render().el).append(gestaoDeOcorrenciasGridPaginator.render().el);

                        //Habilita os botões de geração de XLS.
                        $('#xlsSintetico, #xlsAnalitico').removeAttr('disabled');
                    } else {
                        /*Caso não haja models (ou seja, resultados para a pesquisa)
                         * renderiza a mensagem de alerta após a tela de pesquisa. */
                        $('#pesquisaGestOcorr').append('<div class="gestaoDeOcorrResult alert alert-info"><h5>Não há resultados para essa pesquisa.</h5></div>');
                        //Desabilita os botões XLS.
                        $('#xlsSintetico, #xlsAnalitico').prop('disabled', true);
                    }
                    loaded();
                }
            });
        }
    },
    carregarSubAssunto: function(){
        var that = this;
        ComboboxView({
            el: this.ui.id_subassunto,
            url: '/ocorrencia/find-sub-assunto/id_assuntocopai/' + that.ui.id_assuntoco.val(),
            optionLabel: 'st_assuntoco',
            model: Backbone.Model.extend({
                idAttribute: 'id_assuntoco'
            })
        }, function(elem) {
            elem.trigger('change');
        });
    },
    onRender: function() {
        this.loadComboboxes();
    },
    onShow: function(){
        this.$el.find('.timepicker').timepicker({
            defaultTime: false,
            showMeridian: false
        });
    },
    loadComboboxes: function(){
        var that = this;

        ComboboxView({
            el: that.ui.id_evolucao,
            url: '/api/evolucao/st_tabela/tb_ocorrencia',
            optionLabel: 'st_evolucao',
            emptyOption: '- Selecione Evolução -',
            model: Backbone.Model.extend({
                idAttribute: 'id_evolucao'
            })
        });

        ComboboxView({
            el: that.ui.id_situacao,
            url: '/api/situacao/?st_tabela=tb_ocorrencia',
            emptyOption: '- Selecione Situação -',
            optionLabel: 'st_situacao',
            model: Backbone.Model.extend({
                idAttribute: 'id_situacao'
            })
        });

        ComboboxView({
            el: that.ui.id_nucleo,
            url: '/ocorrencia/find-nucleo-pessoa-co',
            emptyOption: '- Selecione o Núcleo -',
            optionLabel: 'st_nucleoco',
            optionLabelConstruct: function(model){
                return model.get('st_funcao') + ' - ' + model.get('st_nucleoco')
            },
            model: Backbone.Model.extend({
                idAttribute: ['id_funcao', 'id_nucleoco']
            }),
            childViewOptions: {
                onRender: function() {
                    this.$el.attr('value', this.model.get('id_nucleoco'));
                    this.$el.attr('rel', this.model.get('id_funcao'));
                }
            }
        });
    },
    validaIntervaloMeses: function(){

        //Armazena as strings digitadas nos inputs "criada entre / e".
        var dt_inicio = this.ui.dt_inicio.val();
        var dt_fim = this.ui.dt_fim.val();

        /*Utilizando .substr, adequa a string que está no formato DD/MM/YYYY para o formato MM/DD/YYYY
        * e cria objetos de data utilizando as novas strings.
         */
        var inicioFomat = new Date(dt_inicio.substr(3,2) + '/' + dt_inicio.substr(0,2) + '/' + dt_inicio.substr(6,4));
        var fimFormat = new Date(dt_fim.substr(3,2) + '/' + dt_fim.substr(0,2) + '/' + dt_fim.substr(6,4));

        //Calcula a diferença de tempo.
        var umDia = 1000 * 3600 * 24;
        var difDeTempo = Math.abs(fimFormat.getTime() - inicioFomat.getTime());
        var difDeDias = Math.ceil(difDeTempo / umDia);

        /*Verifica se data de criação final é menor que a data inicial.
        Caso positivo, retorna false, impedindo o envio do form.*/
        if (fimFormat.getDate() < inicioFomat.getDate()) {
            $.pnotify({
                title: 'Erro',
                text: 'A data máxima de criação não pode ser menor que a data de início.',
                type: 'error'
            });
            return false;
        }

        /*Verifica se o intervalo de dias entre as datas é maior que 30.
         Caso positivo, retorna false, impedindo o envio do form.*/
        if(difDeDias > 30){
            $.pnotify({
                title: 'Erro:',
                text: 'Só é possível fazer consultas com intervalos de no máximo 30 dias. Sua consulta tem um intervalo de ' + difDeDias + ' dias.',
                type: 'error'
            });
            return false;
        }

        //Caso as datas cumpram os requisitos acima, retorna true - permitindo o envio do form.
        return true;
    }
});

var GestaoDeOcorrenciasModel = Backbone.Model.extend({});
var GestaoDeOcorrenciasCollection = Backbone.PageableCollection.extend({
    model: GestaoDeOcorrenciasModel,
    url: 'api/vw-gestao-de-ocorrencias',
    state: {
        pageSize: 10
    },
    mode: 'client'
});

var gestaoDeOcorrenciasCollection = new GestaoDeOcorrenciasCollection();
var layoutGestaoOcorrencias = new LayoutGestaoOcorrencias();
G2S.layout.content.show(layoutGestaoOcorrencias);
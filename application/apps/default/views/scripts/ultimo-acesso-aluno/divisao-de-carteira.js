var ModelRelatorio = Backbone.Model.extend({});

var RelatorioCollection = Backbone.PageableCollection.extend({
    model: ModelRelatorio,
    url: '/ultimo-acesso-aluno/retorna-relatorio',
    state: {
        pageSize: 15
    },
    mode: "client"
});

var EntidadeCollection = Backbone.Collection.extend({
    url: '/quantidade-transacoes-cartao-credito/retorna-entidade-recursiva',
    model: VwEntidadeRecursiva,
    initialize: function () {
        //this.fetch()
    }
});

var LayoutRelatorio = Marionette.LayoutView.extend({
    template: '#template-layout-relatorio',
    tagname: 'div',
    className: 'containder-fluid',
    regions: {
        formulario: '#container-formulario',
        tabela: '#container-tabela'
    },
    onShow: function () {
        var formulario = new FormularioView();
        this.formulario.show(formulario);

        return this;
    }
});


var FormularioView = Marionette.ItemView.extend({
    template: "#template-formulario-relatorio",
    ui: {
        idEntidade: 'select#id_entidade',
        dt_inicio: 'input#dt_filtro_inicio',
        dt_fim: 'input#dt_filtro_fim'
    },
    onShow: function () {
        this.populaSelectEntidade();
        return this;
    },
    populaSelectEntidade: function () {
        var that = this;
        var collectionEntidade = new EntidadeCollection();
        collectionEntidade.fetch({
            beforeSend: function () {
                loading();
            },
            complete: function () {
                loaded();
            },
            success: function () {
                var view = new SelectView({
                    el: that.ui.idEntidade,                 // Elemento da DOM
                    collection: collectionEntidade,         // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_nomeentidade',     // Propriedade da Model que ser� utilizada como label do select
                        optionValue: 'id_entidade',         // Propriedade da Model que ser� utilizada como id do option do select
                        optionSelected: null                // ID da option que receber� o atributo "selected"
                    }
                });
                view.render();
            }
        });

    },
    getColumnsGrid: function () {
        var columns = [{
            name: "id_contrato",
            label: "Contrato",
            cell: "string",
            editable:false
        }, {
            name: "st_nomecompleto",
            label: "Nome do Aluno",
            cell: "string",
            editable:false
        }, {
            name: "st_email",
            label: "E-mail",
            cell: "string",
            editable:false
        }, {
            name: "st_nomeentidade",
            label: "Entidade",
            cell: "string",
            editable:false
        }, {
            name: "st_evolucao",
            label: "Evolução",
            cell: "string",
            editable:false
        }, {
            name: "id_matricula",
            label: "Matrícula",
            cell: "string",
            editable:false
        }, {
            name: "st_projetopedagogico",
            label: "Projeto Pedagógico",
            cell: "string",
            editable:false
        }, {
            name: "nu_disciplina",
            label: "Quantidade de Disciplinas",
            cell: "string",
            editable:false
        }, {
            name: "dt_cadastro",
            label: "Último acesso",
            cell: "string",
            editable:false
        }, {
            name: "nu_disciplinascomnota",
            label: "N Disc. Completas (Nota satisfatória)",
            cell: "string",
            editable:false
        }, {
            name: "nu_disciplinasemnota",
            label: "N Disc. Completas (Nota não satisfatória)",
            cell: "string",
            editable:false
        }, {
            name: "nu_disciplinasnaoencerradas",
            label: "N Disc. não encerradas",
            cell: "string",
            editable:false
        },{
            name: "st_situacaoagendamento",
            label: "Situação do Agendamento",
            cell: "string",
            editable:false
        }];
        return columns;
    },
    enviarFormulario: function (e) {
        e.preventDefault();
        var that = this;
        var collectionRelatorio = new RelatorioCollection();
        collectionRelatorio.url = '/ultimo-acesso-aluno/retorna-relatorio';
        collectionRelatorio.fetch({
            data: {
                'id_entidade': that.ui.idEntidade.val(),
                'dt_fim': that.ui.dt_fim.val(),
                'dt_inicio': that.ui.dt_inicio.val(),
                'detalhado': 1
            },
            type: 'POST',
            complete: function () {
                loaded();
            },
            beforeSend: function () {
                loading();
                layoutRelatorio.$el.find('#container-tabela').empty();
            },
            success: function () {
                var grid = new Backgrid.Grid({
                    columns: that.getColumnsGrid(),
                    collection: collectionRelatorio,
                    className: 'backgrid backgrid-selectall table table-striped table-bordered table-hover'
                });

                var paginator = new Backgrid.Extension.Paginator({
                    collection: collectionRelatorio
                });
                layoutRelatorio.$el.find('#container-tabela').append(grid.render().$el);
                layoutRelatorio.$el.find('#container-tabela').append(paginator.render().$el);
            }
        });

    },
    gararXls: function () {
        var that = this;
        var url = '/ultimo-acesso-aluno/gerar-xls/?detalhado=1&tipo=xls&id_entidade=' + that.ui.idEntidade.val() + '&dt_fim=' + that.ui.dt_fim.val() + '&dt_inicio=' + that.ui.dt_inicio.val();
        window.open(url, '_blank');
    },
    events: {
        'submit #formRelatorio': 'enviarFormulario',
        'click button#btn_gerar_xls': 'gararXls'
    }
});

var layoutRelatorio = new LayoutRelatorio();
G2S.show(layoutRelatorio);
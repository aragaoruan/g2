/**
 *Array de Objetoso com indice e nome dos meses
 * @type {array}
 */
var Meses = [
    {
        id: 1,
        text: "Janeiro"
    },
    {
        id: 2,
        text: "Fevereiro"
    },
    {
        id: 3,
        text: "Março"
    },
    {
        id: 4,
        text: "Abril"
    },
    {
        id: 5,
        text: "Maio"
    },
    {
        id: 6,
        text: "Junho"
    },
    {
        id: 7,
        text: "Julho"
    },
    {
        id: 8,
        text: "Agosto"
    },
    {
        id: 9,
        text: "Setembro"
    },
    {
        id: 10,
        text: "Outubro"
    },
    {
        id: 11,
        text: "Novembro"
    },
    {
        id: 12,
        text: "Dezembro"
    }
];
/**
 * Função para retornar o nome da label
 * @param float valor
 * @returns {string}
 */
function retornaLabel(valor) {
    //verifica o valor e retorna as labels de acordo com os valores
    if (valor > 0) {
        return 'label-success';
    } else if (valor < 0) {
        return 'label-important';
    } else {
        return '';
    }
}

/**
 ******************************************************
 ************************ MODELS **********************
 ******************************************************
 */
var ModeloExtratoDisciplinas = Backbone.Model.extend({
    defaults: {
        id_disciplina: '',
        st_disciplina: '',
        detalhes: [],
        projetos: [],
        labelSubtotalVenda: '',
        labelAdiantamentoVenda: '',
        labelSaldoVenda: '',
        labelTotalVenda: '',
        labelSubtotalComissao: '',
        labelAdiantamentoComissao: '',
        labelSaldoComissao: '',
        labelTotalComissao: '',
        nu_saldovenda: "0,00",
        nu_saldocomissao: "0,00",
        nu_subtotalvenda: "0,00",
        nu_subtotalcomissao: "0,00",
        nu_adiantamentovenda: "0,00",
        nu_adiantamentocomissao: "0,00",
        nu_totalvenda: "0,00",
        nu_totalcomissao: "0,00"
    },
    bl_pagar: false
});


var PagamentosModel = Backbone.Model.extend({});

var ModelProjetosExtrato = Backbone.Model.extend({});

var AnoModel = Backbone.Model.extend({});
var MesModel = Backbone.Model.extend({});

/**
 ******************************************************
 ******************** COLLECTIONS *********************
 ******************************************************
 */

var PagamentosCollection = Backbone.Collection.extend({
    model: PagamentosModel,
    url: function () {
        return '/pagamento-colaborador/retornar-pagamentos-efetuados-extrato/?' + $.param(_.defaults(this.data));
    }
});
var ProjetosExtratoCollection = Backbone.Collection.extend({
    model: ModelProjetosExtrato
});

var MesCollection = Backbone.Collection.extend({
    model: MesModel
});

var AnoCollection = Backbone.Collection.extend({
    model: AnoModel
});

var CollectionExtratoDisciplina = Backbone.Collection.extend({
    model: ModeloExtratoDisciplinas,
    url: function () {
        return '/pagamento-colaborador/retorna-extrato/?' + $.param(_.defaults(this.data));
    },
    total: 0
});


/**
 ******************************************************
 **************** VARIAVEIS GLOBAIS *******************
 ******************************************************
 */

var thatAbas;
var thatBusca;
var modelUsuario = new VwUsuariosComissionados();
var collectionExtrato = new CollectionExtratoDisciplina();
var collectionPagamentos = new PagamentosCollection();

/**
 ******************************************************
 ********************** LAYOUT ************************
 ******************************************************
 */
var LayoutDetalhar = Marionette.LayoutView.extend({
    template: "#template-regioes",
    className: 'row-fluid',
    regions: {
        usuario: "#usuario-regiao",
        busca: "#formulario-regiao",
        abas: "#abas-regiao"
    },
    onShow: function () {
        this.showBtnVoltar();
        this.rederizaUsuario();
        this.renderizaBusca();
        //this.renderizaAbas();
        return this;
    },
    showBtnVoltar: function () {
        var btnModel = new ButtonModel({
            caption: "Voltar",
            url: "pagamento-colaborador",
            color: "inverse"
        });
        var collection = new ButtonCollection(btnModel);
        G2S.breadcrumb.extra.show(new AuxButtons({collection: collection}));

    },
    renderizaAbas: function () {
        var abasView = new AbasLayout();
        this.abas.show(abasView);
    },
    renderizaBusca: function () {
        var buscaItemView = new BuscaItemView();
        this.busca.show(buscaItemView);
    },
    rederizaUsuario: function () {
        modelUsuario = new VwUsuariosComissionados();
        modelUsuario.set(ModelColaborador);

        var usuarioItemView = new UsuarioItemView({
            model: modelUsuario
        });
        this.usuario.show(usuarioItemView);
    }
});


/**
 ******************************************************
 ************************ ABAS ************************
 ******************************************************
 */
var AbasLayout = Marionette.LayoutView.extend({
    template: "#template-abas",
    regions: {
        abaExtrato: '#tab1',
        abaPagamentos: '#tab2'
    },
    initialize: function () {
        thatAbas = this;
    },
    onShow: function () {
        //this.renderizaAbaExtrato();
        return this;
    },
    renderizaAbaExtrato: function () {
        if (modelUsuario.get('id_funcao') == 4) {
            var extratoView = new AbaExtratoDisciplinasCompositeView({
                collection: collectionExtrato
            });
        } else if (modelUsuario.get('id_funcao') == 2) {
            var extratoView = new ProjetosDetalheCompositeView({
                collection: collectionExtrato
            });
        }
        this.abaExtrato.show(extratoView);
    },
    renderizaAbaPagamento: function () {
        var pagamentoView = new AbaPagamentosCompositeView({
            collection: collectionPagamentos
        });
        this.abaPagamentos.show(pagamentoView);
    }
});

/**
 ******************************************************
 ********************** USUÁRIO ***********************
 ******************************************************
 */
var UsuarioItemView = Marionette.ItemView.extend({
    template: "#template-usuario"
});


/**
 ******************************************************
 ********************** BUSCA ************************
 ******************************************************
 */
var BuscaItemView = Marionette.ItemView.extend({
    template: "#template-formulario",
    ui: {
        selectMes: '#mes',
        selectAno: '#ano'
    },
    events: {
        'submit #form-filtro': 'pesquisarDados'
    },
    anoAtual: null,
    mesAtual: null,
    setAnoAtual: function () {
        var dtAtual = new Date();
        this.anoAtual = dtAtual.getFullYear();
    },
    setMesAtual: function () {
        var dtAtual = new Date();
        this.mesAtual = dtAtual.getMonth();
    },
    initialize: function () {
        thatBusca = this;
        this.setAnoAtual();
        this.setMesAtual();
        return this;
    },
    onShow: function () {
        this.populaSelectAno();
        this.populaSelectMês();
    },
    populaSelectAno: function () {

        //instancia a collection do ano
        var anoCollection = new AnoCollection();
        //faz um loop de 5 laços e decrementa os anos
        for (var i = 5; i >= 0; i--) {
            var ano = this.anoAtual - i;//pega o ano atual e decrementa o valor do laço
            anoCollection.add(new AnoModel({id: ano, nome: ano}));//seta no modelo
        }

        //instancia o selectview
        var view = new SelectView({
            sort: false,
            el: this.ui.selectAno,
            collection: anoCollection,
            childViewOptions: {
                optionLabel: 'nome',
                optionValue: 'id',
                optionSelected: this.anoAtual
            }
        });
        view.render();

    },
    populaSelectMês: function () {
        //seta a quantidade de laços, é 11 pois o javascript começa os meses do 0 (zero) e termina no 11 (onze)
        var lacos = 11;
        var mesCollection = new MesCollection();//instancia da collection do mês
        //faz o loop para pegar os meses no array
        for (var i = 0; i <= lacos; i++) {
            var id = Meses[i]['id'];
            var text = Meses[i]['text'];
            //seta os meses na modelo
            mesCollection.add(new MesModel({id: id, nome: text}));
        }

        //monta o select view
        var view = new SelectView({
            sort: false,
            el: this.ui.selectMes,
            collection: mesCollection,
            childViewOptions: {
                optionLabel: 'nome',
                optionValue: 'id',
                optionSelected: this.mesAtual
            }
        });
        view.render();
    },
    pesquisarDados: function (e) {
        e.preventDefault();
        this.pesquisarExtrato();
        this.pesquisarPagamentos();

    },
    pesquisarPagamentos: function () {

        collectionPagamentos = new PagamentosCollection();
        collectionPagamentos.data = {
            ano: this.ui.selectAno.val(),
            mes: this.ui.selectMes.val(),
            id_usuario: modelUsuario.get('id_usuario'),
            id_perfil: modelUsuario.get('id_funcao')
        }

        collectionPagamentos.fetch({
            beforeSend: function () {
                loading();
            },
            complete: function () {
                loaded();
            },
            error: function () {
                $.pnotify({
                    type: 'error',
                    title: 'Erro!',
                    text: 'Houve um erro inesperado ao consultar dados para aba de Pagamentos'
                })
            },
            success: function () {
                thatAbas.renderizaAbaPagamento();
            }
        });

    },
    pesquisarExtrato: function () {
        collectionExtrato = new CollectionExtratoDisciplina();
        collectionExtrato.data = {
            ano: this.ui.selectAno.val(),
            mes: this.ui.selectMes.val(),
            id_usuario: modelUsuario.get('id_usuario'),
            id_perfil: modelUsuario.get('id_funcao')
        }
        collectionExtrato.fetch({
            beforeSend: function () {
                loading();
                layoutDetalhar.renderizaAbas();
            },
            complete: function () {
                loaded();
            },
            error: function () {
                $.pnotify({
                    type: 'error',
                    title: 'Erro!',
                    text: 'Houve um erro inesperado ao consultar dados para aba de Extrato.'
                })
            },
            success: function () {
                thatAbas.renderizaAbaExtrato();
            }
        });
    }
});


/**
 ******************************************************
 ************* VIEW PARA SEM RESULTADOS ***************
 ******************************************************
 */
var EmptyViewExtrato = Marionette.ItemView.extend({
    tagName: 'div',
    className: 'row-fluid',
    template: _.template("<div class='alert'>Sem Resultado</div>")
});

/**
 ******************************************************
 ******** VIEW PARA SEM RESULTADOS NA TABELA **********
 ******************************************************
 */
var EmptyViewProjetosExtrato = Marionette.ItemView.extend({
    tagName: 'tr',
    template: _.template("<td colspan='3'><div class='alert'>Sem Resultado</div></td>")
});

/**
 ******************************************************
 ******* VIEW PARA DADOS DAS TABELAS / PROJETOS *******
 ******************************************************
 */
var ProjetosDisciplinaItemView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: '#template-linha-extrato',
    initialize: function () {
        //recupera os valores da modelo e força eles como float
        var valorVenda = parseFloat(this.model.get('nu_valorvenda') ? this.model.get('nu_valorvenda') : 0);
        var valorComissao = parseFloat(this.model.get('nu_valor') ? this.model.get('nu_valor') : 0);

        //formata os valores para view
        this.model.set('nu_valorvenda', formatValueToShow(valorVenda.toFixed(2)));
        this.model.set('nu_valor', formatValueToShow(valorComissao.toFixed(2)));
        return this;
    }
});

/**
 ******************************************************
 ***** VIEW PARA TELA DE COORDENADOR DE PROJETO *******
 ******************************************************
 */
var ProjetosDetalheCompositeView = Marionette.CompositeView.extend({
    template: "#template-projetos-lista",
    childViewContainer: "tbody",
    emptyView: EmptyViewProjetosExtrato,
    childView: ProjetosDisciplinaItemView,
    model: new ModeloExtratoDisciplinas,
    initialize: function () {
        this.calculaTotais();
        return this;
    },
    calculaTotais: function () {
        //calcula os totais, formata os valores e define as labels
        var that = this;

        //inicia duas variaveis para o total com valor 0
        var totalVenda = 0;
        var totalComissao = 0;
        var subTotal = 0;

        //percorre a collection
        this.collection.each(function (model) {
            //recupera os valores da modelo e força eles como float
            var valorVenda = parseFloat(model.get('nu_valorvenda') ? model.get('nu_valorvenda') : 0);
            var valorComissao = parseFloat(model.get('nu_valor') ? model.get('nu_valor') : 0);

            totalVenda += valorVenda;
            totalComissao += valorComissao;

            if (model.get('st_tipo') === 'Sub Total') {
                subTotal += valorComissao;
            }

            //verifica qual o tipo e toma uma ação para cada respectivo
            switch (model.get('st_tipo')) {
                case 'Saldo Anterior':
                    that.model.set('labelSaldoComissao', retornaLabel(valorComissao));
                    that.model.set('nu_saldocomissao', formatValueToShow(valorComissao.toFixed(2)));
                    break;
                case 'Sub Total':
                    that.model.set('labelSubtotalComissao', retornaLabel(subTotal));
                    that.model.set('nu_subtotalcomissao', formatValueToShow(subTotal.toFixed(2)));
                    break;
                case 'Adiantamento':
                    that.model.set('labelAdiantamentoComissao', retornaLabel(valorComissao));
                    that.model.set('nu_adiantamentocomissao', formatValueToShow(valorComissao.toFixed(2)));
                    break;
            }

        });

        //seta os totais na model e as labels
        that.model.set({
            nu_totalcomissao: formatValueToShow(totalComissao.toFixed(2)),
            labelTotalComissao: retornaLabel(totalComissao)
        });
    }
});


/**
 ******************************************************
 *********** VIEW PARA DADOS DAS TABELAS **************
 ******************************************************
 */
var DisciplinasDetalheCompositeView = Marionette.CompositeView.extend({
    template: "#template-disciplinas-detalhe",
    childViewContainer: "tbody",
    emptyView: EmptyViewProjetosExtrato,
    childView: ProjetosDisciplinaItemView,
    initialize: function () {
        //instancia uma nova collection e atribui a collection da composite
        this.collection = new ProjetosExtratoCollection();
        this.collection.add(this.model.get('projetos'));//atribui o array de objetos de projetos na collection
        this.calculaTotais();//calcula os totais e seta os valores das labels
        return this;
    },
    calculaTotais: function () {
        //calcula os totais, formata os valores e define as labels
        var that = this;

        //inicia duas variaveis para o total com valor 0
        var totalVenda = 0;
        var totalComissao = 0;
        //percorre o objeto de detalhes
        $.each(this.model.get('detalhes'), function (i, obj) {

            //inicia as variaveis dos loop's, verificando se os valores existem caso não seta como zero
            var saldoVenda = obj.nu_valorvenda ? parseFloat(obj.nu_valorvenda) : 0;
            var saldoComissao = obj.nu_valor ? parseFloat(obj.nu_valor) : 0;

            //calcula os totais
            totalVenda += saldoVenda;
            totalComissao += saldoComissao;

            //formata os valores para exibir na view
            saldoVenda = formatValueToShow(saldoVenda.toFixed(2));
            saldoComissao = formatValueToShow(saldoComissao.toFixed(2));

            //verifica qual o tipo e toma uma ação para cada respectivo
            switch (obj.st_tipo) {
                case 'Saldo Anterior':
                    that.model.set('labelSaldoComissao', retornaLabel(obj.nu_valor));
                    that.model.set('labelSaldoVenda', retornaLabel(obj.nu_valorvenda));
                    that.model.set('nu_saldovenda', saldoVenda);
                    that.model.set('nu_saldocomissao', saldoComissao);
                    break;
                case 'Sub Total':
                    that.model.set('labelSubtotalComissao', retornaLabel(obj.nu_valor));
                    that.model.set('labelSubtotalVenda', retornaLabel(obj.nu_valorvenda));
                    that.model.set('nu_subtotalvenda', saldoVenda);
                    that.model.set('nu_subtotalcomissao', saldoComissao);
                    break;
                case 'Adiantamento':
                    that.model.set('labelAdiantamentoComissao', retornaLabel(obj.nu_valor));
                    that.model.set('labelAdiantamentoVenda', retornaLabel(obj.nu_valorvenda));
                    that.model.set('nu_adiantamentovenda', saldoVenda);
                    that.model.set('nu_adiantamentocomissao', saldoComissao);
                    break;
            }

        });

        //seta os totais na model e as labels
        that.model.set({
            nu_totalcomissao: formatValueToShow(totalComissao.toFixed(2)),
            nu_totalvenda: formatValueToShow(totalVenda.toFixed(2)),
            labelTotalComissao: retornaLabel(totalComissao),
            labelTotalVenda: retornaLabel(totalVenda)
        });
    }
});

/**
 ******************************************************
 ********* VIEW PARA TABELAS DAS DISCIPLINAS **********
 ******************************************************
 */
var AbaExtratoDisciplinasCompositeView = Marionette.CompositeView.extend({
    template: "#aba-extrato-detalhar",
    childViewContainer: "fieldset",
    collection: collectionExtrato,
    emptyView: EmptyViewExtrato,
    childView: DisciplinasDetalheCompositeView,
    ui: {
        'containerTotalReceber': '#total-receber'
    },
    onRender: function () {
        this.calculaTotalReceber();
        return this;
    },
    calculaTotalReceber: function () {
        //inicia a variavel para o calculo
        var total = 0;
        //percorre a collection
        this.collection.each(function (model) {
            //formata o valor para real somando na variavel total
            total += formatValueToReal(model.get('nu_totalcomissao'));
        });

        //cria uma variavel label vazia
        var label = '';
        //verifica se o total é maior que zero e define a label como sucesso
        if (total > 0) {
            label = "label-success";
        } else if (total < 0) {//senão seta como important
            label = "label-important";
        }

        //pega o nome do mês do select da busca
        var mes = thatBusca.ui.selectMes.find('option:selected').text();
        this.ui.containerTotalReceber.find('span.mes b').html(mes);//escreve o nome do mes no elemento do template
        //escreve o valor calculado no elemento do template
        this.ui.containerTotalReceber.find('span.label').addClass(label).html("R$ " + formatValueToShow(total.toFixed(2)));
    }
});

/**
 ******************************************************
 ****** VIEW PARA ITENS DA TABELA DE PAGAMENTO ********
 ******************************************************
 */
var PagamentoItemView = Marionette.ItemView.extend({
    template: "#template-linha-pagamento",
    tagName: 'tr',
    initialize: function () {
        //formata o valor para exibição setando o valor na modelo
        var nu_valor = parseInt(this.model.get('nu_valor'));
        this.model.set('nu_valor', formatValueToShow(nu_valor.toFixed(2)));
        return this;
    }
});

/**
 ******************************************************
 ****** VIEW RENDERIZAR TABELA DE PAGAMENTOS ********
 ******************************************************
 */
var AbaPagamentosCompositeView = Marionette.CompositeView.extend({
    collection: collectionPagamentos,
    template: "#aba-pagamento-detalhar",
    childViewContainer: "tbody",
    emptyView: EmptyViewProjetosExtrato,
    childView: PagamentoItemView,
    ui: {
        contentMes: '#nome-mes'
    },
    onShow: function () {
        //pega o nome do mes e insere numa região definida pelo ui
        var mes = thatBusca.ui.selectMes.find('option:selected').text();
        this.ui.contentMes.html(mes);
        return this;
    }
});

/**
 ******************************************************
 *************** INICIA A RENDERIZAÇÃO ****************
 ******************************************************
 */
var layoutDetalhar = new LayoutDetalhar();
G2S.show(layoutDetalhar);
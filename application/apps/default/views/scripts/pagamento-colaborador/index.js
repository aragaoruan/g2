/**
 * Função para gerar Select
 * @param el
 * @param collection
 * @param optionLabel
 * @param optionValue
 * @param optionSelected
 */
function geraSelect(el, collection, optionLabel, optionValue, optionSelected) {

    var view = new SelectView({
        el: el,
        collection: collection,
        childViewOptions: {
            optionLabel: optionLabel,
            optionValue: optionValue,
            optionSelected: optionSelected
        }
    });
    view.render();
}

/**
 ***************** VARIAVEIS PARA O ESCOPO *****************
 */


/**
 * Modelo Backbone para select de disciplinas
 * @type Backbone Model
 */
var SelectDisciplinaModel = Backbone.Model.extend({
    defaults: {
        id: 0,
        text: 'Selecione'
    }
});

var AdiantamentoModel = Backbone.Model.extend({
    defaults: {
        id_usuario: '',
        nu_valor: 0,
        id_disciplina: '',
        id_funcao: ''
    },
    url: 'pagamento-colaborador/creditar-adiantamento'
});

var AutorizacaoModel = Backbone.Model.extend({
    defaults: {
        id_usuario: '',
        st_cpf: '',
        nu_valor: 0,
        nu_valorMostrar: 0,
        st_nomeexibicao: '',
        st_funcao: '',
        id_funcao: ''
    }
});

var PagamentoModel = Backbone.Model.extend({});

/**
 * Collection Backbone para select de disciplinas
 * @type Backbone Collection
 */
var SelectDisciplinaCollection = Backbone.Collection.extend({
    model: SelectDisciplinaModel,
    url: '/pagamento-colaborador/retornar-disciplinas'
});

var AutorizacaoCollection = Backbone.Collection.extend({
    model: AutorizacaoModel,
    url: 'pagamento-colaborador/retorna-autorizacoes',
    url: function () {
        return 'pagamento-colaborador/retorna-autorizacoes/?' + $.param(_.defaults(this.data));
    },
    data: {
        st_cpf: null,
        st_nomecompleto: null
    }
});

var PagamentosCollection = Backbone.Collection.extend({
    model: PagamentoModel,
    url: function () {
        return 'pagamento-colaborador/retornar-pagamentos-efetuados/?' + $.param(_.defaults(this.data));
    },
    data: {
        st_cpf: null,
        st_nomecompleto: null
    }
});


/**
 * Reescreve o modelo adicionando algumas funcoes
 * @type BackboneModel @exp;VwUsuariosComissionados@call;extend
 */
var VwUsuariosComissionadosModel = VwUsuariosComissionados.extend({
    bl_pagar: false
});

/**
 * Collection para Modelo de Usuarios Comissionados
 * @type @exp;Backbone@pro;Collection@call;extend
 */
var ComissoesCollection = Backbone.Collection.extend({
    model: VwUsuariosComissionadosModel,
    url: function () {
        return '/api/vw-usuarios-comissionados/?' + $.param(_.defaults(this.data));
    },
    data: {
        st_cpf: null,
        st_nomecompleto: null
    }
});

/**
 * Collection Backbone para VwComissaoReceber
 * @type Backbone Collection
 */
var VwComissaoReceberCollection = Backbone.Collection.extend({
    model: VwComissaoReceber,
    url: '/api/vw-comissao-receber/'
});


/**
 ***************** INSTANCIAS DAS COLLECTIONS *****************
 */
var collectionListaUsuarios = new ComissoesCollection();
var collectionAutorizacao = new AutorizacaoCollection();
var collectionPagamentos = new PagamentosCollection();
var telaAbas;

/**
 * Layout para separar as regiões da tela
 */
var LayoutTelaPagamento = Marionette.LayoutView.extend({
    template: "#template-layout",
    regions: {
        busca: "#formulario-regiao",
        abas: "#abas-regiao",
        modal: "#modal-regiao"
    },
    onShow: function () {
        this.renderizaTelaPesquisa();
        this.renderizaAbas();
        return this;
    },
    renderizaTelaPesquisa: function () {
        var telaPesquisa = new FormularioItemView();
        this.busca.show(telaPesquisa);
    },
    renderizaAbas: function () {
        telaAbas = new AbasLayoutView();
        this.abas.show(telaAbas);
    }
});

/**
 * Layout para as abas
 */
var AbasLayoutView = Marionette.LayoutView.extend({
    template: "#container-abas",
    regions: {
        abaListaColaboradores: '#tab1',
        abaAutorizacaoPagamento: '#tab2',
        abaPagamento: '#tab3'
    },
    events: {},
    onShow: function () {

        this.renderizaAbaListaColaboradores();
        this.renderizaAbaListaAutorizacoes();
        this.renderizaAbaPagamentos();

        return this;
    },
    renderizaAbaListaColaboradores: function () {
        var that = this;
        //collectionListaUsuarios.fetch({
        //    beforeSend: function () {
        //        loading();
        //    },
        //    complete: function () {
        //        loaded();
        //    },
        //    success: function () {
        //        var abaListaColaboradores = new AbaListaColaboradores({
        //            collection: collectionListaUsuarios
        //        });
        //        that.abaListaColaboradores.show(abaListaColaboradores);
        //    }
        //});
        var abaListaColaboradores = new AbaListaColaboradores({
            collection: collectionListaUsuarios
        });
        that.abaListaColaboradores.show(abaListaColaboradores);
    },
    renderizaAbaListaAutorizacoes: function () {
        var that = this;
        VerificarPerfilPermissao(26, 614, true).done(function (response) {
            if (response.length == 1) {
                $('#li_autorizacao').show();
                collectionAutorizacao.fetch({
                    beforeSend: function () {
                        loading();
                    },
                    complete: function () {
                        loaded();
                    },
                    success: function () {
                        var abaListaAutorizacoes = new AbaListaAutorizacao({
                            collection: collectionAutorizacao
                        });
                        that.abaAutorizacaoPagamento.show(abaListaAutorizacoes);
                    }
                });
            }
        })
    },
    renderizaAbaPagamentos: function () {
        var that = this;
        var abaPagamento = new CompositeViewAbaPagamento({
            collection: collectionPagamentos
        });

        VerificarPerfilPermissao(25, 614, true).done(function (response) {
            if (response.length == 1) {
                $('#li_pagamento').show();
                that.abaPagamento.show(abaPagamento);
            }
        })
    }
});


var ItemViewModalAdiantamento = Marionette.ItemView.extend({
    template: "#template-modal-adiantar",
    className: "modal hide fade",
    tagName: 'div',
    ui: {
        nu_valor: "input[name='nu_valor']",
        id_disciplina: "select[name='id_disciplina']"
    },
    events: {
        'click #btn-salvar-modal': 'adiantarCredito'
    },
    onShow: function () {
        this.$el.modal({keyboard: false});
        this.ui.nu_valor.val("").maskMoney({precision: 2, decimal: ",", thousands: "."});

        this.populaSelectDisciplina();
        return this;
    },
    populaSelectDisciplina: function () {
        var that = this;
        var disciplinas = new SelectDisciplinaCollection();
        disciplinas.url += "?id_usuario=" + that.model.get('id_usuario');
        disciplinas.fetch({
            beforeSend: function () {
                loading();
                that.ui.id_disciplina.empty().html("<option value=''>Selecione</option>");
            },
            success: function (collection) {
                geraSelect(that.ui.id_disciplina, collection, 'text', 'id', null);
            },
            complete: function () {
                loaded();
            }
        });
    },
    validaAdiatamento: function () {
        var valid = true;

        if (!this.ui.id_disciplina.val()) {
            valid = false;
            $.pnotify({
                title: 'Atenção!',
                text: 'Selecione a Disciplina para fazer o Adiantamento.',
                type: 'warning'
            });
        }

        if (!this.ui.nu_valor.val()) {
            valid = false;
            $.pnotify({
                title: 'Atenção!',
                text: 'Informe o valor para fazer o Adiantamento.',
                type: 'warning'
            });
        }

        return valid;

    },
    adiantarCredito: function (e) {
        e.preventDefault();
        var that = this;
        var id_disciplina = this.ui.id_disciplina.val();
        var nu_valor = this.ui.nu_valor.val();

        if (this.validaAdiatamento()) {

            var funcao = 2;
            if (this.model.get('st_perfilpedagogico') == 'Coordenador de Disciplina') {
                funcao = 4;
            }


            var modelo = new AdiantamentoModel();
            modelo.set({
                id_disciplina: parseInt(id_disciplina),
                nu_valor: nu_valor,
                id_usuario: this.model.get('id_usuario'),
                id_funcao: funcao
            });

            modelo.save(null, {
                beforeSend: function () {
                    loading();
                },
                success: function (model, response) {
                    $.pnotify({
                        title: 'Registro Atualizado',
                        text: 'O adiantamento foi creditado com sucesso!',
                        type: 'success'
                    });
                },
                error: function (model, response) {
                    $.pnotify({
                        title: 'Erro ao atualizar o registro',
                        text: 'Houve um problema ao Creditar os valores para <b>' + that.model.get('st_nomecompleto') + '</b>, tente novamente',
                        type: 'error'
                    });
                },
                complete: function () {
                    that.$el.modal('hide');
                    loaded();
                }
            });

        }
    }
});


var ItemViewPrincipal = Marionette.ItemView.extend({
    tagName: 'tr',
    template: '#item-colaborador',
    events: {
        'click .btn-adiantamento': 'adiantar',
        'click .btn-detalhar': 'detalhar'
    },
    onShow: function () {
        VerificarPerfilPermissao(24, 614, true).done(function (response) {
            if (response.length == 1) {
                $('.btn-adiantamento').removeAttr('disabled');
            } else {
                $('.btn-adiantamento').attr('disabled', true);
            }
        })
    },
    detalhar: function () {
        G2S.router.navigate("pagamento-colaborador/detalhar/" + this.model.get('id_usuario') + '/perfil/' + this.model.get('id_funcao'), {trigger: true});
    },
    adiantar: function () {
        var modalView = new ItemViewModalAdiantamento({
            model: this.model
        });
        layoutTelaPagamento.modal.show(modalView);
    }
});

var ItemViewPrincipalAutorizacao = Marionette.ItemView.extend({
    tagName: 'tr',
    template: '#item-autorizacao',
    events: {
        'change input': function (e) {
            if ($(e.currentTarget).is(':checked')) {
                this.model.ischecked = true;
            }
            else {
                this.model.ischecked = false
            }
        }
    },
    onShow: function () {
        if (this.model.get('nu_valor') <= 0 || !this.model.get('nu_valor')) {
            this.$el.find('input').prop('disabled', true);
        }
    }
});


var ItemViewEmptyListaColaboradores = Marionette.ItemView.extend({
    tagName: 'tr',
    template: _.template("<td colspan='4'><div class='alert'>Sem Resultado</div></td>")
});

var ItemViewEmptyListaAutorizacoes = Marionette.ItemView.extend({
    tagName: 'tr',
    template: _.template("<td colspan='4'><div class='alert'>Sem Resultado</div></td>")
});

var FormularioItemView = Marionette.ItemView.extend({
    template: "#formulario-busca",
    ui: {
        st_cpf: "input[name='st_cpf']",
        st_nomecompleto: "input[name='st_nomecompleto']",
        formularioPesquisa: "form#formPesquisa"
    },
    onShow: function () {
        this.ui.st_cpf.mask('999.999.999-99');
        return this;
    },
    validaFormulario: function () {
        var text = "";
        $.each(this.ui.formularioPesquisa.serializeArray(), function (i, obj) {
            text += obj.value;
        });

        if (text.length) {
            return true;
        } else {
            $.pnotify({
                type: 'warning',
                text: "Informe pelo menos um parametro para pesquisa.",
                title: 'Atenção!'
            });
            return false;
        }
    },
    pesquisaDados: function (e) {
        e.preventDefault();
        this.pesquisaDadosAbaColaboradores();
        this.pesquisaDadosAbaAutorizacao();
        this.pesquisaDadosAbaPagamento();
    },
    pesquisaDadosAbaColaboradores: function () {
        collectionListaUsuarios.data = {
            st_cpf: this.ui.st_cpf.val(),
            st_nomecompleto: this.ui.st_nomecompleto.val()
        }

        collectionListaUsuarios.fetch({
            beforeSend: function () {
                loading();
            },
            success: function () {
            },
            complete: function () {
                loaded();
            }
        });
    },
    pesquisaDadosAbaAutorizacao: function () {
        collectionAutorizacao.data = {
            st_cpf: this.ui.st_cpf.val(),
            st_nomecompleto: this.ui.st_nomecompleto.val()
        }
        collectionAutorizacao.fetch({
            beforeSend: function () {
                loading();
            },
            success: function () {
            },
            complete: function () {
                loaded();
            }
        });
    },
    pesquisaDadosAbaPagamento: function () {
        collectionPagamentos.data = {
            st_cpf: this.ui.st_cpf.val(),
            st_nomecompleto: this.ui.st_nomecompleto.val()
        }
        collectionPagamentos.fetch({
            beforeSend: function () {
                loading();
            },
            success: function () {
            },
            complete: function () {
                loaded();
            }
        });
    },

    events: {
        'submit #formPesquisa': 'pesquisaDados'
    }
});

var ItemViewPagamento = Marionette.ItemView.extend({
    template: "#item-pagamento",
    tagName: 'tr',
    initialize: function () {
        var nu_valor = parseFloat(this.model.get('nu_valortotal') ? this.model.get('nu_valortotal') : 0);
        this.model.set('nu_valortotal', formatValueToShow(nu_valor.toFixed(2)));
        return this;
    },
    ui: {
        'dt_pagamento': 'input[name="dt_pagamento"]'
    },
    events: {
        'change @ui.dt_pagamento': 'setDtPagamento'
    },
    setDtPagamento: function () {
        var dt_pagamento = this.ui.dt_pagamento.val();
        if (dt_pagamento.length) {
            this.model.set({
                'dt_quitado': dt_pagamento,
                'bl_quitado': true
            });
            this.model.edited = true;
        } else {
            this.model.set({
                'dt_quitado': null,
                'bl_quitado': false
            });
            this.model.edited = false;
        }
    }
});

var CompositeViewAbaPagamento = Marionette.CompositeView.extend({
    template: "#template-aba-pagamentos",
    childView: ItemViewPagamento,
    childViewContainer: "tbody",
    emptyView: ItemViewEmptyListaColaboradores,
    collection: collectionPagamentos,
    ui: {
        'btnPagar': '#btn-registrar-pagamento'
    },
    collectionEvents: {
        'change': 'verificaItensPagar'
    },
    onShow: function () {
        this.ui.btnPagar.attr('disabled', 'disabled');
        return this;
    },
    events: {
        'click @ui.btnPagar': 'pagarLancamentos'
    },
    verificaItensPagar: function () {
        var collectionPagar = this.collection.where({bl_quitado: true});
        if (collectionPagar.length) {
            this.ui.btnPagar.removeAttr('disabled');
        } else {
            this.ui.btnPagar.attr('disabled', 'disabled');
        }
    },
    pagarLancamentos: function () {
        var objPagar = this.collection.where({bl_quitado: true});

        if (!objPagar.length) {
            $.pnotify({
                title: "Atenção!",
                type: "warning",
                text: "Não é possível confirmar pagamento, informe ao menos uma data."
            });
        } else {

            bootbox.confirm("Confirmar o pagamento dos registros informados?", function (result) {
                if (result) {
                    var collectionPagar = new PagamentosCollection();
                    _.each(objPagar, function (model) {
                        collectionPagar.add(model);
                    });

                    Backbone.sync('create', collectionPagar, {
                            url: '/pagamento-colaborador/salvar-pagamento',
                            beforeSend: function () {
                                loading();
                            },
                            success: function (response) {
                                $.pnotify({
                                    title: 'Sucesso!',
                                    text: 'Pagamentos registrados com sucesso.',
                                    type: 'success'
                                });
                                telaAbas.renderizaAbaPagamentos();
                            },
                            error: function () {
                                $.pnotify({title: 'Erro!', text: 'Erro ao confirmar pagamentos.', type: 'error'});
                            },
                            complete: function () {
                                loaded();
                            }
                        }
                    )


                }

            })
        }


    }
});

var AbaListaColaboradores = Marionette.CompositeView.extend({
    template: "#template-aba-lista-colaboradores",
    childView: ItemViewPrincipal,
    childViewContainer: "tbody",
    emptyView: ItemViewEmptyListaColaboradores,
    collection: collectionListaUsuarios,
    onShow: function () {

    }
});

var AbaListaAutorizacao = Marionette.CompositeView.extend({
    template: "#template-aba-lista-autorizacao",
    childView: ItemViewPrincipalAutorizacao,
    childViewContainer: "tbody",
    emptyView: ItemViewEmptyListaAutorizacoes,
    collection: collectionAutorizacao,
    collectionClone: {},
    events: {
        'keyup #nu_valorcorte': 'efetuaCorteValor',
        'focusout #nu_valorcorte': 'efetuaCorteValor',
        'change #check-all-autorizacao': 'marcarTodos',
        'click #btn-autorizar-pagamento': 'autorizarPagamento',
        'change input': function () {
            this.atualizaTotal();
        }
    },
    ui: {
        valorCorte: '#nu_valorcorte',
        valorTotal: '#nu_valortotal',
        checkAll: '#check-all-autorizacao'
    },
    onRender: function () {
        this.collectionClone = this.collection.clone();
    },
    onShow: function () {
        this.atualizaTotal();
        this.ui.valorCorte.maskMoney({precision: 2, decimal: ",", thousands: ".", allowNegative: false});
        this.ui.valorTotal.maskMoney({precision: 2, decimal: ",", thousands: "."});
    },
    efetuaCorteValor: function () {
        that = this;
        modelo = this.collectionClone.clone();
        var arr = [];
        if (this.ui.valorCorte.val() != "" && this.ui.valorCorte.val() != '0,00') {
            for (var i = 0; i < modelo.length; i++) {
                if (parseFloat(formatValueToReal(modelo.models[i].get('nu_valor'))) < parseFloat(formatValueToReal(that.ui.valorCorte.val()))) {
                    arr.push(modelo.models[i]);
                    modelo.models[i].deleted = true;
                } else {
                    modelo.models[i].false = false;
                }
            }
        }

        modelo.remove(arr);

        this.collection.set(modelo.models);
        this.atualizaTotal();
    },
    atualizaTotal: function () {
        var total = 0;
        _.each(this.collection.models, function (model) {
            if (parseFloat(formatValueToReal(model.get('nu_valor'))) > 0 && model.ischecked) {
                total += parseFloat(formatValueToReal(model.get('nu_valor')))
            }
        });
        this.ui.valorTotal.val(formatValueToShow(total.toFixed(2)));
    },
    marcarTodos: function () {
        var that = this;
        _.each(this.collection.models, function (model) {
            if (that.ui.checkAll.is(':checked')) {
                if (model.get('nu_valor') > 0) {
                    model.ischecked = true;
                    $('#chk_autorizar_' + model.get('id_usuario') + '_' + model.get('id_funcao')).prop('checked', true)
                }
            } else {
                $('#chk_autorizar_' + model.get('id_usuario') + '_' + model.get('id_funcao')).prop('checked', false)
                model.ischecked = false;
            }
        });
        this.atualizaTotal();
    },
    autorizarPagamento: function () {
        var that = this;
        var collectionEnviar = new AutorizacaoCollection();
        _.each(this.collection.models, function (model) {
            if (model.ischecked) {
                collectionEnviar.add(model);
            }
        })
        bootbox.confirm("Confirmar autorização de pagamento?", function (result) {
            if (result) {
                if (collectionEnviar.length) {
                    Backbone.sync('create', collectionEnviar, {
                            url: '/pagamento-colaborador/autorizar-pagamento',
                            beforeSend: function () {
                                loading();
                            },
                            success: function (response) {
                                $.pnotify({
                                    title: 'Sucesso',
                                    text: 'Autorização de Pagamento efetuada com sucesso.',
                                    type: 'success'
                                });
                                that.collection.fetch({
                                    beforeSend: function () {
                                        loading();
                                    },
                                    success: function () {
                                        that.collectionClone.reset();
                                        that.collectionClone = that.collection.clone();
                                        that.atualizaTotal();
                                        telaAbas.renderizaAbaPagamentos();
                                    },
                                    complete: function () {

                                    }
                                });
                            },
                            error: function () {
                                $.pnotify({title: 'Erro', text: 'Erro ao autorizar pagamentos.', type: 'error'});
                            },
                            complete: function () {
                                loaded();
                            }
                        }
                    )
                } else {
                    $.pnotify({
                        title: 'Alerta',
                        text: 'Nenhum pagamento selecionado para ser autorizado.',
                        type: 'alert'
                    });
                }
            }
        })
    }


});

var layoutTelaPagamento = new LayoutTelaPagamento();
G2S.show(layoutTelaPagamento);
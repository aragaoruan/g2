/*
 * 
 * Modelo para guardar os Tipos
 */
var ProjetoContratoDuracaoTipo = Backbone.Model.extend({
    defaults: {
        
        id_projetocontratoduracaotipo: null,
        st_projetocontratoduracaotipo: 'Informe o Tipo',
        st_descricao: 'Informe a descrição'
        
    },
    idAttribute: 'id_projetocontratoduracaotipo',
    url: function() {
        return this.id ? '/api/projeto-contrato-duracao-tipo/' + this.id : '/api/projeto-contrato-duracao-tipo';
    },
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function(){
        return this.get("st_projetocontratoduracaotipo");
    },        
    schema: {
        st_projetocontratoduracaotipo: { type: 'Text', title: 'Tipo' },
        st_descricao: { type: 'TextArea', title: 'Descrição' }
    }
});


/*
 *  Coleção dos locais de aula retornados na requisição 
 *  padrão /api/projeto-contrato-duracao-tipo. 
 *  Alterando o parse para tratar o resultado do Mensageiro
 */
var ProjetosContratoDuracaoTipo = Backbone.Collection.extend({
    model: ProjetoContratoDuracaoTipo,
    url: "/api/projeto-contrato-duracao-tipo",
    parse: function(data) {
        return data.mensagem;
    }
});
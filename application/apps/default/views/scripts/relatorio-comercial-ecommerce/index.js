/******************************************************************************
 *              Relatório Comercial Ecommerce
 *              Neemias Santos  <neemias.santos@unyleya.com.br>
 *              2015-04-16
 * ****************************************************************************
 */
$.ajaxSetup({async: false});

/**
 ******************************************************************************************
 ******************************************************************************************
 *************************** collection  ***************************************************
 ******************************************************************************************
 ******************************************************************************************
 */

/**
 * @type Collection para Entidade @exp;Backbone@pro;Collection@call;extend
 */
var EntidadeCollection = Backbone.Collection.extend({
    url: '/entidade/retornar-entidade-recursiva-id',
    model: VwEntidadeRecursivaId,
    initialize: function () {
        //this.fetch()
    }
});

/**
 * @type Collection para Categoria @exp;Backbone@pro;Collection@call;extend
 */
var CategoriaCollection = Backbone.Collection.extend({
    model: Categoria,
    url: '/categoria/retorna-categoria-entidade',
    initialize: function () {
        //this.fetch()
    }
});

/**
 * @type Collection para Produto @exp;Backbone@pro;Collection@call;extend
 */
var VwCategoriaProdutoCollection = Backbone.Collection.extend({
    model: VwCategoriaProduto,
    url: 'api/vw-categoria-produto'
});

/**
 * @type Collection para MeioPagamento @exp;Backbone@pro;Collection@call;extend
 */
var MeioPagamentoCollection = Backbone.Collection.extend({
    model: MeioPagamento,
    url: '/api/meio-pagamento'
});

/**
 * @type Collection para Evolução @exp;Backbone@pro;Collection@call;extend
 */
var EvolucaoCollection = Backbone.Collection.extend({
    model: Evolucao,
    url: '/api/evolucao'
});

/**
 * @type Collection para Evolução @exp;Backbone@pro;Collection@call;extend
 */
var UfCollection = Backbone.Collection.extend({
    model: Uf
});

/**
 * @type Collection para VwEvolucao @exp;Backbone@pro;Collection@call;extend
 */
var VwEcommerceCollection = Backbone.Collection.extend({
    model: VwEcommerce,
    url: '/api/evolucao'
});

/**
 ******************************************************************************************
 ******************************************************************************************
 *************************** LAYOUT FUNCIONALIDADE ****************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var TelaGradeLayout = Marionette.LayoutView.extend({
    template: '#template-layout-grade',
    tagName: 'div',
    regions: {
        tabelaGrade: "#regiao-grade-data",
        formgrade: '#regiao-grade'
    },
    onShow: function () {
        this.renderizaFormGrade();
        loaded();
        return this;
    },
    renderizaFormGrade: function () {
        var gradeView = new FormGradeCompositeView({});
        this.formgrade.show(gradeView);
    }
});


/**
 ******************************************************************************************
 ******************************************************************************************
 *************************** FORM DA GRADE ****************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var FormGradeCompositeView = Marionette.CompositeView.extend({
    initialize: function () {

    },
    template: '#template-form-grade',
    tagName: 'fieldset',
    ui: {
        'id_entidade': 'select[name="id_entidade"]',
        'id_categoria': 'select[name="id_categoria"]',
        'id_produto': 'select[name="id_produto"]',
        'id_meiopagamento': 'select[name="id_meiopagamento"]',
        'id_evolucao': 'select[name="id_evolucao"]',
        'sg_uf': 'select[name="sg_uf"]',
        'dt_inicial_comprar': 'input[name="dt_inicial_comprar"]',
        'dt_termino_comprar': 'input[name="dt_termino_comprar"]',
        'dt_inicial_confirmacao': 'input[name="dt_inicial_confirmacao"]',
        'dt_termino_confirmacao': 'input[name="dt_termino_confirmacao"]',
        'btnSalvar': '#btnPesquisar',
        'btnGeralExcel': '#btn_gerar_relatorio'
    },
    events: {
        'change #id_entidade': 'populaCategoriaEntidade',
        'change #id_categoria': 'populaSelectProduto',
        'click #btnPesquisar': 'pesquisarDados',
        'click #btn_gerar_relatorio': 'btnGeralExcel'
    },
    onShow: function () {
        $('#container-DataGrid').hide();
        this.populaSelectEntidade();
        this.populaSelectMeioPagamento();
        this.populaSelectEvolucao();
        this.popularSelectUf();
    },
    populaSelectEntidade: function () {
        var that = this;
        that.ui.id_produto.select2('destroy');
        that.ui.id_produto.empty();
        that.ui.id_produto.prepend('<option value="" selected="selected">Escolha uma opção</option>');
        var collectionEntidade = new EntidadeCollection();
        collectionEntidade.fetch({
            success: function () {
                var view = new SelectView({
                    el: that.$el.find("select[name='id_entidade']"),        // Elemento da DOM
                    collection: collectionEntidade,      // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_nomeentidade', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_entidade', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: null       // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();
            },
            complete: function () {
                var newOption = $('<option value="0" selected>Todas</option>');
                newOption.insertAfter('#id_entidade option[value=""]');
                that.populaCategoriaEntidade();
            }
        });
    },
    populaCategoriaEntidade: function () {
        var that = this;
        if (!that.ui.id_entidade.val()) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Selecione a Entidade para poder buscar as Categorias.'
            });
            return false;
        }
        loading();
        that.ui.id_categoria.empty();
        that.ui.id_produto.select2('destroy');
        that.ui.id_produto.empty();
        that.ui.id_produto.prepend('<option value="" selected="selected">Escolha uma opção</option>');
        var collectionCategoria = new CategoriaCollection();
        collectionCategoria.url += '/id_entidade/' + that.ui.id_entidade.val() + '/todos/' + (that.ui.id_entidade.val() != 0 ? false : true);
        collectionCategoria.fetch({
            success: function () {
                var view = new SelectView({
                    el: that.ui.id_categoria,        // Elemento da DOM
                    collection: collectionCategoria,      // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_categoria', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_categoria', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: null       // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();

                that.ui.id_categoria.removeAttr('disabled');
                that.ui.id_categoria.prepend('<option value="" selected="selected">Escolha uma opção</option>');
                loaded();
            }
        });
    },
    populaSelectProduto: function () {
        var that = this;
        if (!that.ui.id_categoria.val()) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Selecione a Entidade para poder buscar as Categorias.'
            });
            return false;
        }
        loading();
        that.ui.id_produto.select2('destroy');
        that.ui.id_produto.empty();
        that.ui.id_produto.prepend('<option value="" selected="selected">Escolha uma opção</option>');
        var collectionVwCategoriaProduto = new VwCategoriaProdutoCollection();
        collectionVwCategoriaProduto.url += '/id_categoria/' + that.ui.id_categoria.val() + '/id_entidadecadastro/' + (that.ui.id_entidade.val() != 0 ? that.ui.id_entidade.val() : G2S.loggedUser.get('id_entidade'));
        collectionVwCategoriaProduto.fetch({
            success: function () {
                var view = new SelectView({
                    el: that.ui.id_produto,        // Elemento da DOM
                    collection: collectionVwCategoriaProduto,      // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_produto', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_produto', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: null       // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();

                that.ui.id_produto.select2();
                that.ui.id_produto.removeAttr('disabled');
                loaded();
            }
        });
    },
    populaSelectMeioPagamento: function () {
        var that = this;

        var collectionMeioPagamento = new MeioPagamentoCollection();
        var collectionMeioPagamento = new MeioPagamentoCollection();
        collectionMeioPagamento.url += '/index' +
        '/order/st_meiopagamento/type/asc';
        collectionMeioPagamento.fetch({
            success: function () {
                var view = new SelectView({
                    el: that.ui.id_meiopagamento,        // Elemento da DOM
                    collection: collectionMeioPagamento,      // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_meiopagamento', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_meiopagamento', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: null       // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();
            }
        });
    },
    populaSelectEvolucao: function () {
        var that = this;

        var collectionEvolucao = new EvolucaoCollection();
        collectionEvolucao.url += '/st_tabela/tb_venda';
        collectionEvolucao.fetch({
            success: function () {
                var view = new SelectView({
                    el: that.ui.id_evolucao,        // Elemento da DOM
                    collection: collectionEvolucao,      // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_evolucao', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_evolucao', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: null       // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();
            }
        });
    },
    popularSelectUf: function () {
        var that = this;

        var collectionUf = new UfCollection();
        collectionUf.url = '/default/util/retorna-uf/';
        collectionUf.fetch({
            success: function () {
                var view = new SelectView({
                    el: that.ui.sg_uf,        // Elemento da DOM
                    collection: collectionUf,      // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_uf', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'sg_uf', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: null       // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();
            }
        });
    },
    pesquisarDados: function () {
        var that = this;
        if (!that.ui.id_entidade.val() && !that.ui.id_categoria.val()) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Selecione a Entidade.'
            });
            return false;
        }

        if (that.ui.dt_inicial_comprar.val() && !that.ui.dt_termino_comprar.val() || that.ui.dt_termino_comprar.val() && !that.ui.dt_inicial_comprar.val()) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Preencha os dois campos de Periodo da compra.'
            });
            return false;
        }

        if (that.ui.dt_inicial_confirmacao.val() && !that.ui.dt_termino_confirmacao.val() || that.ui.dt_termino_confirmacao.val() && !that.ui.dt_inicial_confirmacao.val()) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Preencha os dois campos de Periodo da Confirmação.'
            });
            return false;
        }

        loading();
        var columns = [
            {
                name: "st_nomeentidade",
                label: "Entidade",
                editable: false,
                cell: 'string'
            }, {
                name: "st_nomecompleto",
                label: "Nome do Aluno",
                editable: false,
                cell: 'string'
            }, {
                name: "st_produto",
                label: "Produto",
                editable: false,
                cell: 'string'
            }, {
                name: "nu_valortabela",
                label: "Valor Tabela do Produto",
                editable: false,
                cell: 'string'
            }, {
                name: "nu_valorcupom",
                label: "Valor Cupom",
                editable: false,
                cell: 'string'
            }, {
                name: "nu_valornegociado",
                label: "Valor Negociado do Produto",
                editable: false,
                cell: 'string'
            }, {
                name: "st_campanhacomercial",
                label: "Campanha",
                editable: false,
                cell: 'string'
            }, {
                name: "st_codigocupom",
                label: "Código Cupom",
                editable: false,
                cell: 'string'
            }, {
                name: "st_nomeatendente",
                label: "Atendente",
                editable: false,
                cell: 'string'
            }, {
                name: "id_venda",
                label: "Cod Venda",
                editable: false,
                cell: 'string'
            }, {
                name: "st_evolucao",
                label: "Evolução da Compra",
                editable: false,
                cell: 'string'
            },{
                name: "st_status",
                label: "Status Compra",
                editable: false,
                cell: 'string'
            }, {
                name: "st_meiopagamento",
                label: "Meio de Pagamento",
                editable: false,
                cell: 'string'
            }, {
                name: "nu_parcelas",
                label: "Parcelas",
                editable: false,
                cell: 'string'
            }, {
                name: "dt_cadastroshow",
                label: "Data da Compra",
                editable: false,
                cell: 'string'
            }, {
                name: "dt_confirmacaoshow",
                label: "Data da Confirmação",
                editable: false,
                cell: 'string'
            }, {
                name: "st_categorias",
                label: "Categoria do Produto",
                editable: false,
                cell: 'string'
            }, {
                name: "st_cpf",
                label: "CPF",
                editable: false,
                cell: 'string'
            }, {
                name: "dt_nascimento",
                label: "Data de Nascimento",
                editable: false,
                cell: 'string'
            }, {
                name: "st_email",
                label: "E-mail",
                editable: false,
                cell: 'string'
            }, {
                name: "st_telefone",
                label: "Telefone",
                editable: false,
                cell: 'string'
            }, {
                name: "st_endereco",
                label: "Endereço",
                editable: false,
                cell: 'string'
            }, {
                name: "st_bairro",
                label: "Bairro",
                editable: false,
                cell: 'string'
            }, {
                name: "st_cep",
                label: "CEP",
                editable: false,
                cell: 'string'
            }, {
                name: "sg_uf",
                label: "UF",
                editable: false,
                cell: 'string'
            }, {
                name: "st_cidade",
                label: "Cidade",
                editable: false,
                cell: 'string'
            }, {
                name: "st_areaconhecimento",
                label: "Área de Interesse",
                editable: false,
                cell: 'string'
            }
        ];

        var PageableLog = Backbone.PageableCollection.extend({
            model: VwEcommerce,
            url: '/relatorio-comercial-ecommerce/pesquisa-relatorio/',
            state: {
                pageSize: 10
            },
            mode: "client"
        });
        var data = {
            id_entidade: that.ui.id_entidade.val(),
            id_categoria: that.ui.id_categoria.val(),
            id_produto: that.ui.id_produto.val(),
            id_evolucao: that.ui.id_evolucao.val(),
            id_meiopagamento: that.ui.id_meiopagamento.val(),
            sg_uf: that.ui.sg_uf.val(),
            dt_inicial_comprar: that.ui.dt_inicial_comprar.val(),
            dt_termino_comprar: that.ui.dt_termino_comprar.val(),
            dt_inicial_confirmacao: that.ui.dt_inicial_confirmacao.val(),
            dt_termino_confirmacao: that.ui.dt_termino_confirmacao.val()
        };

        var pageableLog = new PageableLog();

        var pageableGridLog = new Backgrid.Grid({
            className: 'backgrid table table-bordered',
            columns: columns,
            collection: pageableLog,
            emptyText: "Nenhum registro encontrado"
        });

        $('#container-DataGrid').show();
        var $elemento = $('#template-data-grade');
        $elemento.empty();
        //$elemento.append(pageableGridLog.render().$el);
        var paginator = new Backgrid.Extension.Paginator({
            collection: pageableLog
        });
        //$elemento.append(paginator.render().el);

        pageableLog.fetch({
            data: data,
            reset: true,
            beforeSend: function () {
                loading();
            },
            success: function () {
                telaGrade.$el.find("#regiao-grade-data").empty();
                telaGrade.$el.find("#regiao-grade-data").append(pageableGridLog.render().$el);
                telaGrade.$el.find("#regiao-grade-data").append(paginator.render().el);
            },
            error: function (collection, response) {
                $.pnotify({
                    type: 'warning',
                    title: 'Atenção!',
                    text: response.responseText
                });
            },
            complete: function(){
                loaded();
            }

        });
        //loaded();
    },
    btnGeralExcel: function (response) {
        var that = this;
        if (!that.ui.id_entidade.val() && !that.ui.id_categoria.val()) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Selecione a Entidade.'
            });
            return false;
        }

        if (!(that.ui.dt_inicial_comprar.val() && that.ui.dt_termino_comprar.val()) && !(that.ui.dt_termino_comprar.val() && !that.ui.dt_inicial_comprar.val())) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Voce precisa informar ao menos um Periodo para pesquisa!'
            });
            return false;
        }

        loading();
        this.$el.find('form').attr('target', '_blank').attr('action', 'relatorio-comercial-ecommerce/gerar-xls').submit();
        this.$el.find('form').removeAttr('target');
        loaded();
    }
});

/**
 ******************************************************************************************
 ******************************************************************************************
 ******************************** SHOW LAYOUT *********************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var telaGrade = new TelaGradeLayout();
G2S.show(telaGrade);
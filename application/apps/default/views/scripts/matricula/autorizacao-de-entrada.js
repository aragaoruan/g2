/**
 ******************************************************************************************
 ******************************************************************************************
 *************************** LAYOUT FUNCIONALIDADE ****************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var TelaAutorizacoesLayout = Marionette.LayoutView.extend({
    template: '#template-layout',
    tagName: 'div',
    regions: {
        containerAutorizacao: '#container-autorizacao',
    },
    onShow: function () {
        this.renderizaAutoComplete();
        return this;
    },
    renderizaAutoComplete: function () {
        var that = this;
        componenteAutoComplete.main({
            model: VwPesquisarPessoa,
            element: this.$el.find("#container-autocomplete")
        }, function () {
            that.renderizaTelaAutorizacao();
        }, function () {
            that.getRegion('containerAutorizacao').empty();
        });
    },
    renderizaTelaAutorizacao: function () {
        var that = this;
        var vwEmissoesAutorizacaoEntrada = new VwEmissoesAutorizacaoEntrada();
        var usuario = componenteAutoComplete.getFixModel();
        vwEmissoesAutorizacaoEntrada.url = '/api/vw-emissoes-autorizacao-entrada/' + usuario.id_usuario;
        vwEmissoesAutorizacaoEntrada.fetch({
            beforeSend: function () {
                loading();
            },
            complete: function () {
                loaded();
            },
            success: function () {
                var telaAutorizacao = new TelaAutorizacaoView({
                    model: vwEmissoesAutorizacaoEntrada
                });
                that.containerAutorizacao.show(telaAutorizacao);
            }
        });
    }
});


var TelaAutorizacaoView = Marionette.ItemView.extend({
    template: "#template-autorizacoes",
    textoSistema: null,
    matricula: null,
    maxEmissao: 3,
    initialize: function () {
        this.model.set('nu_maxemissoes', this.maxEmissao);
        return this;
    },
    events: {
        'click #btn-emitir': 'emitirAutorizacao'
    },
    ui: {
        btnEmitir: '#btn-emitir'
    },
    onShow: function () {
        this.getTextoSistema();
        this.verificaQuantidadeImpressoes();
        this.retornaMatriculaAluno();
        return this;
    },
    verificaQuantidadeImpressoes: function () {
        if (parseInt(this.model.get('nu_totalemissao')) == this.maxEmissao) {
            this.$el.find('span.label-success').removeClass('label-success');
            this.ui.btnEmitir.hide();
            this.$el.find('span.label').popover({
                title: 'Atenção',
                content: 'Não é possível emitir autorizações. Tentativas esgotadas.',
                placement: 'bottom',
                trigger: 'hover'
            });
        } else {
            this.$el.find('span.label').addClass('label-success');
            this.ui.btnEmitir.show();
        }
    },
    getTextoSistema: function () {
        var that = this;
        var vwSistemaEntidadeMensagem = new VwSistemaEntidadeMensagem();
        vwSistemaEntidadeMensagem.url = '/textos/retornar-texto-sistema-by-mensagem-padrao/?id_mensagempadrao=32';
        vwSistemaEntidadeMensagem.fetch({
            beforeSend: function () {
                loading();
            },
            complete: function () {
                loaded();
            },
            success: function () {
                that.textoSistema = vwSistemaEntidadeMensagem;
            }
        });
    },
    retornaMatriculaAluno: function () {
        var that = this;
        var vwMatricula = new VwMatricula();
        vwMatricula.url = "/matricula/retornar-matricula-by-usuario?id_usuario=" + this.model.get('id_usuario') + "&id_entidadematricula=" + this.model.get('id_entidade') + "&id_entidadematriz=" + this.model.get('id_entidade');
        vwMatricula.fetch({
            beforeSend: function () {
                loading();
            },
            complete: function () {
                loaded();
            },
            success: function () {
                that.matricula = vwMatricula;
            }
        });
    },
    emitirAutorizacao: function () {
        var that = this;
        if (that.validaParametros()) {
            bootbox.confirm("Deseja realmente imprimir uma autorização de entrada para o aluno?", function (result) {
                if (result) {//true
                    var autorizacaoEntrada = new AutorizacaoEntrada();
                    autorizacaoEntrada.set({
                        id_usuario: that.model.get('id_usuario'),
                        id_textosistema: that.textoSistema.get('id_textosistema')
                    });
                    autorizacaoEntrada.save(null, {
                        beforeSend: function () {
                            loading();
                        },
                        complete: function () {
                            loaded();
                        },
                        error: function (model, response) {
                            var textResponse = response.responseText;
                            $.pnotify({
                                title: 'Erro!',
                                text: "Houve um erro ao tentar imprimir autorização. ",
                                type: 'error'
                            });
                        },
                        success: function () {
                            telaAutorizacoes.renderizaTelaAutorizacao();
                            that.abrirPopup();
                        }
                    });
                }
            });
        }
    },
    validaParametros: function () {
        var valid = true;
        if (!this.matricula.get('id_matricula')) {
            $.pnotify({
                title: 'Atenção!',
                text: "Nenhuma matricula localizadad para este usuário.",
                type: 'warning'
            });
            valid = false;
        }
        if (!this.textoSistema.get('id_textosistema')) {
            $.pnotify({
                title: 'Atenção!',
                text: "Nenhuma texto sistema configurado para essa ação.",
                type: 'warning'
            });
            valid = false;
        }

        return valid;
    },
    abrirPopup: function () {

        var parametros = '?id_usuario=' + this.model.get('id_usuario');
        parametros += '&id_textosistema=' + this.textoSistema.get('id_textosistema');
        parametros += '&arrParams[id_matricula]=' + this.matricula.get('id_matricula');
        parametros += '&filename=autorizacao-entrada&type=html';
        var url = '/textos/geracao/' + parametros;
        window.open(url, 'janela', 'width=800, height=600, top=100, left=100, scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no');
    }
});



/**
 ******************************************************************************************
 ******************************************************************************************
 ******************************** SHOW LAYOUT *********************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var telaAutorizacoes = new TelaAutorizacoesLayout();
G2S.show(telaAutorizacoes);

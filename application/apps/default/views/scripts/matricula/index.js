var viewModal = null;
var collectionEsquemaConfiguracao = retornarEsquemaConfiguracao(22, false);
var linhaNegocio = null;
var entidade_propria = true;
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * *                             MODELS                                * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */

var ModeloModulos = Backbone.Model.extend({
    attributes: {
        id_modulo: '',
        st_modulos: '',
        disciplinas: []
    }
});

var TipoTramiteModel = Backbone.Model.extend({
    attributes: {
        st_tipotramite: '',
        id_tipotramite: ''
    }
});

var CarteirinhaModel = Backbone.Model.extend({
    defaults: {
        id_usuario: '',
        st_nomecompleto: '',
        st_cpf: '',
        dt_nascimento: '',
        id_turma: '',
        st_turma: '',
        st_codidentificacao: '',
        id_matricula: '',
        id_contrato: '',
        id_contratoregra: '',
        id_modelocarteirinha: '',
        st_modelocarteirinha: '',
        id_entidade: '',
        st_entidade: '',
        dt_validade: ''
    },
    idAttribute: 'id_usuario'
});

var vendaModel = new (Backbone.Model.extend({
    idAttribute: "id_venda",
    url: function() {
        return "/api/venda/" + (this.id || "");
    }
}));

var listMatriculasview = false;

/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * *                        COLLECTIONS                                * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
var VwMatriculasCollection = Backbone.Collection.extend({
    model: VwMatricula
});

var VwAvaliacaoAlunoCollection = Backbone.Collection.extend({
    model: VwAvaliacaoAluno
});

var VwContratoCarteirinhaCollection = Backbone.Collection.extend({
    model: VwContratoCarteirinha,
    url: '/api/vw-contrato-carteirinha'
});

//collection modulos normais
var CollectionModulos = Backbone.Collection.extend({
    collection: ModeloModulos,
    url: '/matricula/retornar-vw-grade-nota/'
});

//collection Tipo Tramite
var TipoTramiteCollection = Backbone.Collection.extend({
    model: TipoTramiteModel
});


//PERMISSOES DE EVOLUCAO DE MATRICULA
var PermissaoFuncionalidadeCollection = Backbone.Collection.extend({
    url: '/default/matricula/permissoes-alterar-evolucao'
});

/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * *                             VIEWS                                 * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
var ListaMatriculasView = Backbone.View.extend({
    //tagName: 'option',
    el: $('#container-lista-matriculas'),
    model: new VwMatricula(),
    carteirinhaCollection: null,
    matriculaView: null,
    initialize: function () {
        this.render();
    },
    render: function () {
        var pessoa = this.getPessoa();
        if (pessoa.id) {
            var that = this;

            $.get('/js/backbone/templates/matricula/form-matricula-lista.html', {"_": $.now()}, function (template) {
                that.template = _.template(template);
                $(that.el).append(that.template(pessoa));
            });
            this.popularLista(pessoa.id);
        }
        return this;
    },
    events: {
        'click #btn-modal-carteirinha': 'abreModalCarteirinha',
        "change #lista-matriculas": "selecionarOpcao"
    },
    selecionarOpcao: function () {
        loading();
        var that = this;

        if (that.matriculaView != null) {
            that.matriculaView.destroyMe();
        }

        // Resetar o model de Venda
        vendaModel.clear().set(vendaModel.defaults);

        $.get('/matricula/retornar-matriculas-by-params/id_matricula/' + $('#lista-matriculas').val(), function (data) {
            that.matriculaView = new MatriculaView(data[0]);
            vendaModel.set("id_venda", data[0].id_venda);

            $('#container-matricula').html(that.matriculaView.render().el);
            $('#container-matricula').show();

        });
        loaded();
    },

    /**
     * Get LocalStorage pessoaAutocomplete
     * @returns {@exp;localStorage@call;getItem}
     */
    getPessoa: function () {
        return componenteAutoComplete.getFixModel();
    },
    popularLista: function (id) {

        var collectionMatricula = new VwMatriculasCollection();
        collectionMatricula.url = '/matricula/retornar-matriculas-by-params/id_usuario/' + id;
        collectionMatricula.fetch({
                success: function (dados) {
                    if (dados.length) {
                        var comboView = new CollectionView({
                            collection: collectionMatricula,
                            childViewTagName: 'option',
                            childViewConstructor: OptionMatriculaView,
                            el: $('#lista-matriculas')
                        });
                        comboView.render();
                    } else {
                        $.pnotify({
                            type: 'warning',
                            title: 'Atenção!',
                            text: 'Nenhum registro encontrado.'
                        });
                    }
                },
                error: function (dado, response) {
                    $.pnotify(response.responseJSON);
                }
            }
        );
    },
    abreModalCarteirinha: function () {
        var that = this;
        var modelCarteirinhaModel = new CarteirinhaModel();
        var modeloCarteirinha = this.carteirinhaCollection.at(0);
        var pessoa = this.getPessoa();
        var matricula = this.matriculaView.model;
        var dt_validade = "";
        if (matricula.get('dt_terminoturma')) {
            dt_validade = matricula.get('dt_terminoturma').split(" ");
            dt_validade = dt_validade[0].split('/');
            dt_validade = dt_validade[2] + "-" + dt_validade[1] + "-" + dt_validade[0];
        }

        var modelTurma = new VwTurmaTotalCargaHoraria();
        modelTurma.url = '/turma/retornar-turma-maior-carga-horaria/id_projetopedagogico/' + matricula.get('id_projetopedagogico') + '/id_turma/' + matricula.get('id_turma');
        modelTurma.fetch({
            beforeSend: function () {
                loading();
            },
            complete: loaded,
            success: function () {
                modelCarteirinhaModel.set({
                    id_usuario: pessoa.id_usuario,
                    st_nomecompleto: pessoa.st_nomecompleto,
                    st_cpf: pessoa.st_cpf,
                    dt_nascimento: pessoa.dt_nascimento ? pessoa.dt_nascimento.date : null,
                    st_codidentificacao: pessoa.st_identificacao,
                    id_turma: modelTurma.get("id_turma"),
                    st_turma: modelTurma.get("st_turma"),
                    id_matricula: matricula.get("id_matricula"),
                    id_contrato: matricula.get("id_contrato"),
                    id_contratoregra: modeloCarteirinha.get("id_contratoregra"),
                    id_modelocarteirinha: modeloCarteirinha.get("id_modelocarteirinha"),
                    st_modelocarteirinha: modeloCarteirinha.get("st_modelocarteirinha"),
                    id_entidade: matricula.get("id_entidadematricula"),
                    st_entidade: matricula.get("st_entidadematricula"),
                    dt_validade: dt_validade
                });

                if (viewModal) {
                    viewModal.undelegateEvents();
                }

                viewModal = new ModalCarteirinhaView({
                    model: modelCarteirinhaModel,
                    el: that.$el.find("#container-modal-carteirinha")
                });
                viewModal.render();
            },
            error: function (model, response) {

                $.pnotify({
                    type: "warning",
                    title: "Alerta!",
                    text: response.responseText
                });
            }
        });


    },
    getCarteirinha: function () {
        var that = this;
        if (!entidade_propria) {
            that.$el.find('#btn-modal-carteirinha').hide();
        } else {
            var collection = new VwContratoCarteirinhaCollection();

            collection.fetch({
                data: {
                    id_contrato: that.matriculaView.model.get('id_contrato')
                },
                beforeSend: loading,
                success: function () {
                    if (collection.length) {
                        that.$el.find('#btn-modal-carteirinha').show();
                    } else {
                        that.$el.find('#btn-modal-carteirinha').hide();
                    }
                    that.carteirinhaCollection = collection;
                },
                complete: loaded
            });
        }

    }
});

var ModalCarteirinhaView = Marionette.CompositeView.extend({
    template: "#template-modal-carteirinha",
    ui: {
        stCodIdentificacao: 'input[name="st_codidentificacao"]',
        boxCodIdentificacao: '.box-codidentificacao'
    },
    onRender: function () {
        this.$el.find('#modal-carteirinha').modal('show');

        var carteirinha = listMatriculasview.carteirinhaCollection.at(0);

        // Esconder o campo de Cód de Identificação quando modelo de carteirinha for 3, graduação.
        if (carteirinha && +carteirinha.get('id_modelocarteirinha') === MODELO_CARTEIRINHA.GRADUACAO) {
            this.ui.boxCodIdentificacao.hide();
        } else {
            this.ui.boxCodIdentificacao.show();
            this.buscaCodIdentificacao();
        }

        return this;
    },
    buscaCodIdentificacao: function () {
        var that = this;
        var modelBuscaPessoa = new VwPesquisarPessoa();
        modelBuscaPessoa.url = '/api/pessoa/id_usuario/' + this.model.get('id_usuario');
        modelBuscaPessoa.fetch({
            beforeSend: loading,
            complete: loaded,
            success: function () {
                that.model.set('st_codidentificacao', modelBuscaPessoa.get('st_identificacao'));
                that.ui.stCodIdentificacao.val(modelBuscaPessoa.get('st_identificacao'));
            }

        });
    },
    validaCodigoCarteirinha: function () {
        var valid = true;
        var st_codigo = this.ui.stCodIdentificacao.val();

        if (st_codigo == '') {
            return true;
        }

        if (!st_codigo) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Informe o Cód. de Identificação.'
            });
            valid = false;
        }

        if (!validaNumero(st_codigo)) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'O Cód. de Identificação deve ser somente números.'
            });
            valid = false;
        }

        if (st_codigo.length > 12) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'O Cód. de Identificação deve conter somente 12 caracteres.'
            });
            valid = false;
        }

        return valid;
    },
    getTemplateCarteirinha: function (e) {
        e.preventDefault();
        var that = this;
        that.abrePopup();
        that.$el.find("#modal-carteirinha").modal('hide');
    },
    abrePopup: function () {
        var strParams = '?';
        var params = {
            id_usuario: this.model.get('id_usuario'),
            id_turma: this.model.get('id_turma'),
            id_modelocarteirinha: this.model.get('id_modelocarteirinha'),
            id_entidade: this.model.get('id_entidade'),
            id_matricula: this.model.get('id_matricula')
        };

        $.each(params, function (key, value) {
            strParams += key + "=" + value + "&";
        });
        var url = "/carteirinha/monta-carteirinha/" + strParams;
        window.open(url, 'janela', 'width=350,height=430, top=100, left=100, scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no');
    },
    salvarCodIdentificacao: function (e) {
        e.preventDefault();
        var that = this;
        if (this.validaCodigoCarteirinha()) {
            this.model.set('st_codidentificacao', this.ui.stCodIdentificacao.val());

            if (this.model.hasChanged('st_codidentificacao')) {
                var modelPessoa = new Pessoa();
                modelPessoa.set({
                    id_usuario: this.model.get('id_usuario'),
                    st_identificacao: this.model.get('st_codidentificacao')
                });

                modelPessoa.id = this.model.get('id_usuario');

                modelPessoa.save(null, {
                    beforeSend: function () {
                        loading();
                    },
                    complete: function () {
                        loaded();
                    },
                    success: function (model, response) {
                        $.pnotify({
                            title: 'Sucesso!',
                            text: 'Código de identificação salvo com sucesso!',
                            type: 'success'
                        });
                    },
                    error: function (model, response) {
                        $.pnotify({
                            title: 'Erro!',
                            text: 'Houve um erro ao tentar salvar Código de identificação.',
                            type: 'error'
                        });
                    }
                });
            }
        }
    },
    events: {
        'submit form#form-carteirinha': 'getTemplateCarteirinha',
        'click #btn-salvar-cod-identificacao': 'salvarCodIdentificacao'
    }
});


var OptionMatriculaView = Backbone.View.extend({
    template: _.template('<%=id_matricula%>'),
    render: function () {
        this.$el.attr('value', this.model.id);
        this.$el.text(this.model.get('id_matricula') + ' - ' + this.model.get('st_projetopedagogico') + ' - ' +
            this.model.get('st_evolucao') + ' - (' + this.model.get('st_entidadematricula') + ")");
        return this;
    }
});

var MatriculaView = Backbone.View.extend({
    model: new VwMatricula(),
    template: '',
    vwMatriculasCollection: new VwMatriculasCollection(),
    permissaoContrato: true,
    initialize: function (data) {
        this.model = new VwMatricula(data);
        var permissao = VerificarPerfilPermissao(29, 275, false);
        if (permissao.length == 1) {
            permissao = true;
        } else {
            permissao = false;
        }
        this.permissaoContrato = permissao;
    },
    destroyMe: function () {
        //COMPLETELY UNBIND THE VIEW
        this.undelegateEvents();
        $(this.el).removeData().unbind();
        //Remove view from DOM
        this.el.remove();
        Backbone.View.prototype.remove.call(this);
    },
    ui: {
        "selectTipoTramite": "#selectTipoTramite"
    },
    events: {
        "click #filtrar": "renderLogs",
        "click #btn-bloquear-matricula": "bloquearMatricula",
        "click #btn-trancar-matricula": "trancarMatricula",
        "click #btn-cancelar-matricula": "cancelarMatricula",
        "click #btn-decurso-prazo-matricula": "decursoPrazoMatricula",
        "click #btn-transferir-entidade": "transferirMatricula",
        "click #btn-anular-matricula": "anularMatricula",
        "click #btn-liberar-acesso-matricula": "liberarAcessoMatricula",
        "click #btn-alterar-matricula": "alterarMatricula",
        "click #btn-sincronizar-parcelas": "sincronizarParcelas",
        "click #link-tab": "renderGradeCurricular",
        "click #link-avaliacao-agendamento": "renderAvaliacaoAgendamento",
        "click #link-tab2": "renderTramites",
        "click #link-tab5": "renderParcelas",
        "click #link-tab6": "renderHistoricoMensagem",
        "click #btn-novo-tramite": "mostrarFormNovoTramite",
        "click #btn-cancelar-tramite": "esconderFormNovoTramite",
        "click #btn-salvar-tramite": "salvarTramite",
        "click #link-tabtcc": "renderTcc",
        'click #link-tab7': 'renderizaTabDataTermino',
        'click #link-tab8': 'renderizarTabMaterial',
        'click #link-tab9': 'renderizarOcorrencia',
        'click a#btn-contrato': 'abrirContrato',
        'click .btn-historico-impressao': 'imprimirGrade',
        'change select[name="selectTipoTramite"]': 'renderTramites'
    },

    render: function () {

        var that = this;
        if (that.model.get('dt_aceitecontrato') === null ||
            that.model.get('dt_aceitecontrato') === '') {
            that.model.set('dt_aceitecontrato', ' - ');
        } else {
            that.model.set('dt_aceitecontrato', formatarDataToPtBR(that.model.get('dt_aceitecontrato')));
        }

        $.get('/js/backbone/templates/matricula/form-matricula.html', {"_": $.now()}, function (template) {
            that.template = _.template(template);
            that.model.set("dt_concluinte", that.model.get("dt_concluinte").substr(0, 10));
            $(that.el).html(that.template(that.model.toJSON()));
            var dt_fim = new Date();
            var dt_inicio = new Date();
            dt_inicio.setDate(dt_inicio.getDate() - 30);

            $(that.el).find('#dt_fim').val(dt_fim.toISOString().substr(0, 10).split('-').reverse().join('/'));
            $(that.el).find('#dt_inicio').val(dt_inicio.toISOString().substr(0, 10).split('-').reverse().join('/'));

            //verifica se é permitido imprimir o contrato
            if (!that.permissaoContrato) {
                that.$el.find('a#btn-contrato').hide();
            } else {
                that.$el.find('a#btn-contrato').show();
            }

            entidade_propria = true;
            if (G2S.loggedUser.get('id_entidade') != that.model.get('id_entidadematricula')) {
                entidade_propria = false;
            }

            if (!entidade_propria) {
                that.$el.find('.evolMatricula').hide();
                that.$el.find('.novo_tramite').hide();

            } else {
                that.$el.find('.evolMatricula').show();
                that.$el.find('.novo_tramite').show();
            }

            listMatriculasview.getCarteirinha();

        }).done(function () {
            that.renderGradeCurricular();
        });
        that.renderStatusFinanceiro();
        that.carregaPermissoesEvolucaoMatricula();


        return this;
    },
    abrirContrato: function (e) {
        e.preventDefault();

        //verifica se o usuário  tem permissão para imprimir o contrato
        if (this.permissaoContrato) {
            if (this.model.get('id_venda') && this.model.get('id_contrato')) {
                var that = this;
                var pessoa = this.getPessoa();
                var modelContrato;

                var VwGerarContratoCollection = Backbone.Collection.extend({
                    model: VwGerarContrato
                });

                var contratoCollection = new VwGerarContratoCollection();
                contratoCollection.url = '/api/vw-gerar-contrato/id_venda/' + that.model.get('id_venda');
                contratoCollection.fetch({
                    beforeSend: function () {
                        loading();
                    },
                    success: function () {
                        if (contratoCollection.length)
                            modelContrato = contratoCollection.at(0);
                    },
                    complete: function () {
                        loaded();
                        var url = '/textos/geracao/?filename=Contrato - ' + pessoa.st_nomecompleto + '&id_textosistema=' + modelContrato.get('id_contratomodelo') + '&arrParams[id_contrato]=' + that.model.get('id_contrato') + '&type=html';
                        window.open(url, 'janela', 'width=800, height=600, top=100, left=100, scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no');

                    }
                });

            } else {
                $.pnotify({
                    title: 'Atenção!',
                    text: 'Contrato indisponível no momento, verifique a situação da matricula e/ou a Data de Aceite.',
                    type: 'warning'
                });
            }
        }
    },
    imprimirGrade: function (e) {
        e.preventDefault();

        var id_matricula = this.model.get('id_matricula');
        var id_historicogradenota = jQuery(e.currentTarget).attr('data-value');
        var url = '/matricula/imprimir-historico-grade-nota/?id_matricula=' + id_matricula + '&id_historicogradenota=' + id_historicogradenota;

        window.open(url, 'janela', 'width=800, height=600, top=100, left=100, scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no');
        return false;
    },
    carregaPermissoesEvolucaoMatricula: function () {
        var permissoesElems = {
            14: '#btn-bloquear-matricula',
            15: '#btn-trancar-matricula',
            16: '#btn-cancelar-matricula',
            17: '#btn-anular-matricula',
            18: '#btn-transferir-entidade',
            19: '#btn-liberar-acesso-matricula',
            37: '#btn-decurso-prazo-matricula'
        };

        var permissoesCollection = new PermissaoFuncionalidadeCollection();

        permissoesCollection.fetch().done(function () {
            if (permissoesCollection.length) {
                // Ocultar a mensagem de nenhuma ação permitida.
                $('.msg-evolucao').addClass('hide');

                // Mostrar o botao de cada permissao
                permissoesCollection.each(function (model) {
                    $(permissoesElems[model.get('id_permissao')]).removeClass('hide');
                });
            }
        });

    },
    /**
     * Get LocalStorage pessoaAutocomplete
     * @returns {@exp;localStorage@call;getItem}
     */
    getPessoa: function () {
        return JSON.parse(localStorage.getItem('pessoaAutocomplete'));
    },
    renderStatusFinanceiro: function () {
        var that = this;

        this.fetchResponsavelFinanceiro().done(function (data) {
            var situacaoElem = $('.situacao-financeira').removeClass('inadimplente adimplente');

            // SE NAO POSSUI RESPONSAVEL FINANCEIRO, QUER DIZER QUE NAO POSSUI LANCAMENTOS
            if (data && data.type && data.type != 'success') {
                situacaoElem
                    .addClass('inadimplente')
                    .html('SEM LANÇAMENTOS');
            } else {
                // SE POSSUIR RESPONSAVEL, VERIFICA SE POSSUI LANCAMENTOS
                $.ajaxSetup({async: false});
                var possuiLancamento = false;
                $.each(data.text, function (index, obj) {
                    var url = '/default/matricula/retornar-vw-resumo-financeiro?bl_original=1&id_matricula=' + that.model.get('id_matricula') + '&id_usuariolancamento=' + obj.id_usuariolancamento;
                    $.get(url, function (msgResumoFin) {
                        if (msgResumoFin && msgResumoFin.type && msgResumoFin.type == 'success')
                            possuiLancamento = true;
                    })
                });
                $.ajaxSetup({async: true});

                if (!possuiLancamento) {
                    //SE NAO POSSUI LANCAMENTOS, DEFINE  O STATUS COMO "SEM LANCAMENTOS"
                    situacaoElem
                        .addClass('inadimplente')
                        .html('SEM LANÇAMENTOS');
                } else {
                    // SE POSSUIR, VERIFICA SE HA ALGUM ATRASADO;
                    if (that.model.get('id_venda')) {
                        $.get('/matricula/retornar-lancamentos-atrasados?id_venda=' + that.model.get('id_venda'), function (data) {
                            data = $.parseJSON(data);
                            if (data.nu_atrasados > 0) {
                                situacaoElem
                                    .html('INADIMPLENTE')
                                    .addClass('inadimplente')
                                    .append('<br/><small>Há ' + data.nu_diasatraso + ' dias.</small>');
                            } else {
                                situacaoElem
                                    .addClass('adimplente')
                                    .html('ADIMPLENTE');
                            }
                        });
                    }
                }
            }
        });
    },
    renderGradeCurricular: function () {

        /**
         * Este método foi refatorado e convertido para Marionette.
         * @author: Helder Silva <helder.silva@unyleya.com.br>
         * @since: 06/04/2016
         */

        //limpa a div antes de renderizar algo.
        $('#container-grade-curricular').empty().html('');

        //preenche a variável global linhaNegocio.
        linhaNegocio = collectionEsquemaConfiguracao.at(0);

        //Cria a composite de salas normais
        var MPC = new ModulosPaiComposite({
            collection: new CollectionModulos,
            el: '#container-grade-curricular'
        });

        MPC.render();
    },
    renderTramites: function () {
        loading();

        var that = this;

        //adiciona os forms de novo tramite e filtro dos tramites
        $.get('/js/backbone/templates/matricula/form-novo-tramite.html', {"_": $.now()}, function (template) {
            $("#container-tramites").html(template);

            if (!entidade_propria) {
                that.$el.find('.novo_tramite').hide();
            } else {
                that.$el.find('.novo_tramite').show();
            }

        });


        var SpTramite = Backbone.Model.extend({});
        var DataGrid = Backbone.PageableCollection.extend({
            model: SpTramite,
            url: "/",
            state: {
                pageSize: 15
            },
            mode: "client" // page entirely on the client side
        });

        //recupera o valor
        var filtro = $(that.el).find('#selectTipoTramite').val();

        if (filtro) {
            var url = '/matricula/retornar-tramites/?id_matricula=' + $('#lista-matriculas').val() + '&id_tipotramite=' + filtro;
        } else {
            var url = '/matricula/retornar-tramites/?id_matricula=' + $('#lista-matriculas').val();
        }

        // recupara a url e os parametros passados
        var dataResponse;
        $.ajax({
            dataType: 'json',
            type: 'get',
            url: url,
            success: function (data) {
                dataResponse = data;
                that.populaSelectTipoTramite();
            },
            complete: function () {

                var HtmlArquivoCell = Backgrid.Cell.extend({
                    className: 'span2',
                    render: function () {
                        var url = window.location.href.replace("/#/matricula", "");
                        if (this.model.get('st_upload') == null) {
                            var elTemp = _.template('<div class="content"> -- </div>');
                        } else {
                            var elTemp = _.template('<div class="content"> <a target="_blank" class="btn btn-info " ' +
                                'href="' + url + '/upload/tramite/' + this.model.get('st_upload') + '"> Baixar Arquivo </a> </div>');

                        }
                        this.$el.html(elTemp);
                        return this;
                    }
                });
                var HtmlTramiteCell = Backgrid.Cell.extend({
                    className: 'span7',
                    render: function () {
                        var html = this.model.get('st_tramite');

                        if (this.model.get('id_tramitematricula')) {
                            html = '<a href="#" class="btn-historico" data-toggle="modal" data-target="#historico-';
                            html += this.model.get('id_tramitematricula') + '">' + this.model.get('st_tramite') + '</a>';
                            html += '<div class="modal hide fade" id="historico-' + this.model.get('id_tramitematricula');
                            html += '" tabindex="-1" role="dialog" aria-labelledby="Mensagem" aria-hidden="false">';
                            html += '<div class="modal-header">';
                            html +=     '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
                            html += '<h4 class="modal-title" id="myModalLabel">Grade de Notas</h4>';
                            html += '</div>';
                            html += '<div class="modal-body">';
                            html += this.model.get('st_htmlgrade');
                            html += '</div>';
                            html += '<div class="modal-footer">';
                            html += '<button type="button" class="btn btn-primary btn-historico-impressao" data-value="';
                            html += this.model.get('id_historicogradenota') + '"><i class="icon icon-print icon-white"></i> Imprimir</button>';
                            html += '<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>';
                            html += '</div></div>';
                            html += '<style>' + '@media(min-width: 900px) {.modal{width: 1080px;margin-left: -560px' + '}<style>';
                        }

                        var elTemp = _.template('<div class="content">' + html + '</div>');

                        this.$el.html(elTemp);
                        return this;
                    }
                });
                var btAcessarTermoCell = Backgrid.Cell.extend({
                    className: 'span1',
                    render: function () {
                        var elTemp = _.template('Sem url para acesso');
                        if (this.model.get('st_url')) {
                            var elTemp = _.template('<a href="' + this.model.get('st_url') + '" target="_blank" class="btn btn-info">Acessar</a>');
                        }
                        this.$el.html(elTemp);
                        return this;
                    }
                });
                var dataGrid = new DataGrid(dataResponse);
                var columns = [{
                    name: "st_tramite",
                    label: "Texto",
                    editable: false,
                    cell: HtmlTramiteCell
                },
                    {
                        name: "st_tipotramite",
                        label: "Tipo",
                        editable: false,
                        cell: "string"
                    },
                    {
                        name: "dt_cadastro",
                        label: "Data",
                        editable: false,
                        cell: "string"
                    },
                    {
                        name: "st_nomecompleto",
                        label: "Usuário",
                        editable: false,
                        cell: "string"
                    },
                    {
                        name: "st_upload",
                        label: "Arquivo",
                        editable: false,
                        cell: HtmlArquivoCell
                    },
                    {
                        name: "st_url",
                        label: "Termo",
                        editable: false,
                        cell: btAcessarTermoCell
                    }

                    // TABELA

                ];

                //limpa o texto de carregando do container da grid
                //verifica se o json retornou algum resultado
                if (dataGrid.length > 0) {
                    //monta a grid
                    var pageableGrid = new Backgrid.Grid({
                        columns: columns,
                        collection: dataGrid,
                        className: 'backgrid backgrid-selectall table table-bordered table-hover table_imcube'
                    });
                    //renderiza o grid
                    $("#container-tramites").append(pageableGrid.render().$el);
                    //configura o paginator
                    var paginator = new Backgrid.Extension.Paginator({
                        collection: dataGrid
                    });
                    //renderiza o paginator
                    $("#container-tramites").append(paginator.render().$el);

                    dataGrid.fetch({reset: true});
                } else {
                    $("#container-tramites").append('Sem resultado para exibição!');
                }

                if (filtro) {
                    $("#selectTipoTramite").val(filtro);
                }

                loaded();
            }
        });
    },
    populaSelectTipoTramite: function () {
        var that = this;

        collectionTipoTramite = new TipoTramiteCollection();
        collectionTipoTramite.url = '/api/tipo-tramite/id_categoriatramite/1';//1 = matricula
        collectionTipoTramite.fetch({
            async: false,
            beforeSend: function () {
            },
            success: function () {
                var view = new SelectView({
                    el: that.ui.selectTipoTramite,        // Elemento da DOM,        // Elemento da DOM
                    collection: collectionTipoTramite,      // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_tipotramite', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_tipotramite', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: that.model.get('id_tipotramite')       // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();
            },
            complete: function () {
            }
        });
    },
    mostrarFormNovoTramite: function () {
        $('#st_tramite').empty();
        $('form#form-novo-tramite input#id_matricula').val($('#lista-matriculas').val())
        $('#container-novo-tramite .well').removeClass('hidden');
        return false;
    },
    esconderFormNovoTramite: function () {
        $('#st_tramite').val('');
        $('#container-novo-tramite .well').addClass('hidden');
        return false;
    },
    salvarTramite: function () {

        if ($('#st_tramite').val() !== '' && $('#st_tramite').val() !== 'undefined') {
            var retorno = '';

            if ($("input#anexo")[0].files[0] != undefined) {

                var nomeArquivo = $("input#anexo")[0].files[0].name;

                var _validFileExtensions = /(\.jpg|\.pdf|\.doc|\.docx|\.xls|\.xlsx)$/i;
                if (!_validFileExtensions.exec(nomeArquivo)) {
                    $("#erro-anexo").show()
                        .html("Formato inválido do arquivo. Os formatos permitidos são: PDF, JPG, DOC e XLS.");
                    return false;
                }

                if ($("input#anexo")[0].files[0].size > 10485760) {
                    $("#erro-anexo").show()
                        .html("O tamanho do anexo excede os 10mb permitido pelo sistema.");
                    return false;
                }
                $("#erro-anexo").hide();
            }
            that = this;

            $('#form-novo-tramite').ajaxSubmit({
                dataType: 'json',
                type: 'post',
                url: '/matricula/salvar-tramite-matricula',
                beforeSend: function () {
                    loading();
                },
                success: function (data) {
                    retorno = data;
                },
                complete: function () {
                    loaded();
                    $.pnotify({title: retorno.title, text: retorno.text, type: retorno.type});
                    $('#st_tramite').val('');
                    $('#container-novo-tramite .well').addClass('hidden');
                    that.renderTramites();
                }
            });


        } else {
            $.pnotify({title: 'Erro ao validar formulário', text: 'Preencha o texto do trâmite.', type: 'warning'});
        }
        return false;
    },
    renderTcc: function () {
        $.ajaxSetup({'async': false});
        this.templateTcc = function () {
        };
        this.vwAluno = new VwAvaliacaoAluno();
        this.vwAluno.url = '/matricula/buscar-tcc/id_matricula/' + $('#lista-matriculas').val();
        this.vwAluno.fetch();

        var that = this;

        $.get('/js/backbone/templates/matricula/form-tcc.html', {"_": $.now()}, function (template) {
            that.templateTcc = _.template(template);
            $('#container-tcc').html(that.templateTcc(that.vwAluno.toJSON()));

            if (!entidade_propria) {
                $('.baixar_anexo').hide();
            }
        });

        $('#container-tcc').show();
    },
    renderLogs: function () {
        var dt_inicio = new Date($('#dt_inicio').val().split('/').reverse().join('-'));
        var dt_fim = new Date($('#dt_fim').val().split('/').reverse().join('-'));

        var inicio_min = new Date($('#dt_fim').val().split('/').reverse().join('-'));
        inicio_min.setDate(inicio_min.getDate() - 30);

        var nu_inicio = dt_inicio.toISOString().substr(0, 10).split('-').join('');
        var nu_min = inicio_min.toISOString().substr(0, 10).split('-').join('');

        if (nu_inicio < nu_min) {
            $.pnotify({
                type: 'warning',
                title: 'Aviso',
                text: 'O intervalo de data não pode ultrapassar 1 mês'
            });
            return false;
        }
        loading();
        var VwLogMatricula = Backbone.Model.extend({});
        var DataGrid = Backbone.PageableCollection.extend({
            model: VwLogMatricula,
            url: "/",
            state: {
                pageSize: 15
            },
            mode: "client" // page entirely on the client side
        });
        // recupara a url e os parametros passados
        var url = '/matricula/retornar-vw-log-matricula';
        var dataResponse;
        $.ajax({
            dataType: 'json',
            type: 'get',
            url: url,
            data: 'id_matricula=' + $('#lista-matriculas').val() + '&dt_inicio=' + $('#dt_inicio').val() + '&dt_fim=' + $('#dt_fim').val(),
            success: function (data) {
                dataResponse = data;
            },
            complete: function () {
                var dataGrid = new DataGrid(dataResponse);
                var columns = [{
                    name: "st_saladeaula",
                    label: "Sala de Aula",
                    editable: false,
                    cell: "string"
                },
                    {
                        name: "id_funcionalidade",
                        label: "ID Func.",
                        editable: false,
                        cell: "string"
                    },
                    {
                        name: "st_funcionalidade",
                        label: "Funcionalidade",
                        editable: false,
                        cell: "string"
                    },
                    {
                        name: "st_nomeperfil",
                        label: "Perfil",
                        editable: false,
                        cell: "string"
                    },
                    {
                        name: "dt_cadastro",
                        label: "Data",
                        editable: false,
                        cell: "string"
                    }
                ];
                $("#container-grid-logs").html(''); //limpa o texto de carregando do container da grid
                //verifica se o json retornou algum resultado
                if (dataGrid.length > 0) {
                    //monta a grid
                    var pageableGrid = new Backgrid.Grid({
                        columns: columns,
                        collection: dataGrid,
                        className: 'backgrid backgrid-selectall table table-bordered table-hover table_imcube'
                    });
                    //renderiza o grid
                    $("#container-grid-logs").append(pageableGrid.render().$el);
                    //configura o paginator
                    var paginator = new Backgrid.Extension.Paginator({
                        collection: dataGrid
                    });
                    //renderiza o paginator
                    $("#container-grid-logs").append(paginator.render().$el);
                    dataGrid.fetch({reset: true});
                } else {
                    $("#container-grid-logs").html('Sem resultado para exibição!');
                }
                loaded();
            }
        });
    },
    fetchResponsavelFinanceiro: function () {
        var that = this;
        return $.ajax({
            dataType: 'json',
            type: 'get',
            url: '/default/matricula/retornar-responsavel-financeiro',
            data: 'id_matricula=' + $('#lista-matriculas').val(),
            success: function () {
            },
            complete: function () {
                loaded();
            }
        });
    },
    renderParcelas: function () {
        var that = this;

        loading();

        if (!vendaModel.get("loaded")) {
            vendaModel.fetch({
                async: false
            });

            vendaModel.set("loaded", true);
        }

        $("#container-financeiro").html('');

        this.fetchResponsavelFinanceiro().done(function (data) {
            $.ajaxSetup({'async': false});
            if (data.type === 'success') {
                $("#container-financeiro").append('<fieldset><legend><h5>Parcelas Atuais</h5></legend></fieldset>');

                for (var i in data.text) {
                    that.gerarGridParcelas(data.text[i].id_usuariolancamento, data.text[i].st_usuariolancamento, 0);
                }

                $("#container-financeiro").append('<fieldset><legend><h5>Parcelas Originais</h5></legend></fieldset>');

                for (var i in data.text) {
                    that.gerarGridParcelas(data.text[i].id_usuariolancamento, data.text[i].st_usuariolancamento, 1);
                }
            } else {
                $("#container-financeiro").append(data.text);
                $.pnotify(data);
            }
            $.ajaxSetup({'async': true});
        })
    },
    renderHistoricoMensagem: function () {
        loading();
        $("#container-historico-mensagem").html('');
        var that = this;
        var dataResponse;
        $.ajax({
            dataType: 'json',
            type: 'get',
            url: '/default/matricula/historico-mensagem',
            data: 'id_matricula=' + $('#lista-matriculas').val(),
            success: function (data) {
                dataResponse = data;
            },
            complete: function (data) {
                $.ajaxSetup({'async': false});
                var VwEnvioMensagem = Backbone.Model.extend({});
                var DataGrid = Backbone.PageableCollection.extend({
                    model: VwEnvioMensagem,
                    url: "/",
                    state: {
                        pageSize: 200
                    },
                    mode: "client"
                });

                var dataGrid = new DataGrid(dataResponse.mensagem);

                var columns = [{
                    name: "st_mensagem",
                    label: "Título da Mensagem",
                    editable: false,
                    cell: "string"
                },
                    {
                        name: "st_evolucao",
                        label: "Status Da Mensagem",
                        editable: false,
                        cell: "string"
                    },
                    {
                        name: "st_tipoenvio",
                        label: "Tipo de Envio",
                        editable: false,
                        cell: "string"
                    },
                    {
                        name: "st_evolucaodestinatario",
                        label: "Status da Visualização",
                        editable: false,
                        cell: "string"
                    },
                    {
                        name: "dt_envio",
                        label: "Data de Envio",
                        editable: false,
                        cell: "string"
                    },

                    //adicionando funcionalidade de ver mensagem completa num modal
                    {
                        name: "st_texto",
                        label: "Mensagem",
                        editable: false,
                        cell: Backgrid.Cell.extend({
                            render: function () {
                                this.$el.html('<button class="btn btn-custom" type="button"  ><i class="icon icon-white icon-envelope"></i></button>');
                                var html = $(
                                    '<button class="btn btn-custom" type="button" data-toggle="modal" data-target="#mensagem-' +
                                    this.model.get('id_mensagem') +
                                    '" ><i class="icon icon-white icon-envelope"></i></button>' +
                                    '<div class="modal hide fade" id="mensagem-' + this.model.get('id_mensagem') + '" tabindex="-1" role="dialog" aria-labelledby="Mensagem" aria-hidden="false">' +
                                    '<div class="modal-header">' +
                                    '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                                    '<h4 class="modal-title" id="myModalLabel">Mensagem:</h4>' +
                                    '</div>' +
                                    '<div class="modal-body">' +
                                    this.model.get('st_texto') +//pega o texto da mensagem
                                    '</div>' +
                                    '<div class="modal-footer">' +
                                    '<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>' +
                                    //'</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '<style>' +
                                    '@media(min-width: 900px) {.modal{width: 860px;margin-left: -420px' +
                                    '}<style>'
                                );
                                this.$el.html(html);
                                return this;
                            }
                        })
                    }
                ];


                var RowGrid = Backgrid.Row.extend({
                    initialize: function (options) {
                        RowGrid.__super__.initialize.apply(this, arguments);
                    }
                });

                var pageableGrid = new Backgrid.Grid({
                    row: RowGrid,
                    columns: columns,
                    collection: dataGrid,
                    className: 'backgrid backgrid-selectall table table-bordered table-hover table_imcube'
                });


                $("#container-historico-mensagem").append(pageableGrid.render().$el);

                $.ajaxSetup({'async': true});

                loaded();
            }
        });
    },
    gerarGridParcelas: function (id_usuariolancamento, st_usuariolancamento, bl_original) {
        var VwResumoFinanceiro = Backbone.Model.extend({});
        var DataGrid = Backbone.PageableCollection.extend({
            model: VwResumoFinanceiro,
            url: "/",
            state: {
                pageSize: 12
            },
            mode: "client", // page entirely on the client side
            valorParcelasAPagar: function () {
                var valorAPagar = 0;
                this.fullCollection.each(function (i, o) {
                    if (!i.get('bl_quitado')) {
                        valorAPagar += parseFloat(i.get('nu_valor'));
                    }
                });

                return valorAPagar;
            },
            valorParcelasEmAtraso: function () {
                var valorAtrasados = 0;
                this.fullCollection.each(function (i, o) {
                    if (!i.get('bl_quitado')) {
                        var dt_vencimento = moment(i.get('dt_vencimento'), "DD/MM/YYYY");

                        var hoje = moment();
                        var diferenca = hoje.diff(dt_vencimento, 'days');
                        if (diferenca > 0) {
                            valorAtrasados += parseFloat(i.get('nu_valor'));
                        }
                    }
                });
                return valorAtrasados;
            },
            st_situacaopedido: function () {
                var model = this.fullCollection.models[0]
                return model.get('st_situacaopedido');
            }
        });

        // recupara a url e os parametros passados
        var url = '/default/matricula/retornar-vw-resumo-financeiro';
        var dataResponse;
        $.ajax({
            dataType: 'json',
            type: 'get',
            url: url,
            data: 'id_matricula=' + $('#lista-matriculas').val() + '&id_usuariolancamento=' + id_usuariolancamento + '&bl_original=' + bl_original,
            success: function (data) {
                dataResponse = data;
            },
            complete: function () {
                if (dataResponse.type === 'success') {
                    var dataGrid = new DataGrid(dataResponse.mensagem);

                    //Colunas comuns a grid de parcelas originais e atuais
                    var columns = [{
                        name: "nu_ordemparcela",
                        label: "Nº Parcelas",
                        editable: false,
                        cell: "string"
                    },
                        {
                            name: "st_meiopagamento",
                            label: "Meio de Pagamento",
                            editable: false,
                            cell: "string"
                        },
                        {
                            name: "st_codtransacaooperadora",
                            label: "TID",
                            editable: false,
                            cell: "string"
                        },
                        {
                            name: "nu_valor",
                            label: "Valor",
                            editable: false,
                            cell: "string"
                        },
                        {
                            name: "dt_vencimento",
                            label: "Vencimento",
                            editable: false,
                            cell: "string"
                        },
                        {
                            name: "st_nossonumero",
                            label: "Nosso Número",
                            editable: false,
                            cell: "string"
                        },
                        {
                            name: "nu_quitado",
                            label: "Valor baixado",
                            editable: false,
                            cell: "string"
                        },
                        {
                            name: "dt_quitado",
                            label: "Data da Baixa",
                            editable: false,
                            cell: "string"
                        },
                        {
                            name: "st_autorizacao",
                            label: "Nº Autorização",
                            editable: false,
                            cell: "string"
                        },
                        {
                            name: "st_coddocumento",
                            label: "Nº Doc",
                            editable: false,
                            cell: "string"
                        },
                        {
                            name: "st_ultimosdigitos",
                            label: "4 Últimos dígitos do Cartão",
                            editable: false,
                            cell: "string"
                        },
                        {
                            name: "dt_venda",
                            label: "Data da Venda",
                            editable: false,
                            cell: "string"
                        }
                    ];

                    if (bl_original) {
                        //Adiciona as colunas específicas da grid de parcelas originais
                        columns.push({
                            name: "st_situacao",
                            label: "Situação",
                            editable: false,
                            cell: "string"
                        });
                    } else {
                        //Adiciona as colunas da grid de parcelas atuais
                        columns.push({
                            name: "st_link",
                            label: "Recibo",
                            editable: false,
                            cell: Backgrid.UriCell.extend({
                                render: function () {
                                    if (this.model.get('bl_quitado')) {
                                        this.$el.html('<a href="' + this.model.get('st_link') + '" target="_blank"><img src="/img/ico/blue-document-pdf.png" alt="' + this.model.get('id_lancamento') + '"/></a>');
                                    }
                                    return this;
                                }
                            })
                        });

                        columns.push({
                            name: "nu_cheque",
                            label: "Nº Cheque",
                            editable: false,
                            cell: "string"
                        });

                        columns.push({
                            name: "st_situacaolancamento",
                            label: "Situação",
                            editable: false,
                            cell: "string"
                        });

                        columns.push({
                            name: "dt_atualizado",
                            label: "Data de Atualização das Parcelas",
                            editable: false,
                            cell: "string"
                        });
                    }

                    var valorAPagar = 0;
                    var valorAtrasados = 0;

                    // Suppose you want to highlight the entire row when an editable field is focused
                    var RowGrid = Backgrid.Row.extend({
                        initialize: function (options) {
                            RowGrid.__super__.initialize.apply(this, arguments);
                        }
                    });

                    //verifica se o json retornou algum resultado
                    if (dataGrid.length > 0) {
                        //monta a grid
                        var pageableGrid = new Backgrid.Grid({
                            row: RowGrid,
                            columns: columns,
                            collection: dataGrid,
                            className: 'backgrid backgrid-selectall table table-bordered table-hover table_imcube'
                        });

                        var contentHtml = "" +
                            "<div class='titulo-grid-parcelas row-fluid'>" +
                            "   <div class='span3'>" + st_usuariolancamento + "</div>";

                        if (!bl_original) {
                            if (vendaModel.get("id_campanhacomercial")) {
                                contentHtml += "<div class='span3'>Campanha Comercial: " + vendaModel.get("st_campanhacomercial") + "</div>";
                            }

                            if (vendaModel.get("id_campanhapontualidade")) {
                                contentHtml += "<div class='span3'>Campanha Pontualidade: " + vendaModel.get("st_campanhapontualidade") + "</div>";
                            }

                            if (vendaModel.get("nu_creditos")) {
                                contentHtml += "<div class='span3'>Total de Créditos Adquiridos: " + vendaModel.get("nu_creditos") + "</div>";
                            }

                            contentHtml += "" +
                                '<div class="span4 pull-right text-right">' +
                                '   <button id="btn-sincronizar-parcelas" class="btn btn-info" title="Sincronizar Parcelas" type="button" ' +
                                '       style="display: inline-block; margin-top: 10px; margin-right: 10px;">' +
                                '       <i class="icon-spin icon-refresh icon-white"></i> ' +
                                '       Sincronizar Parcelas' +
                                '   </button>' +
                                '</div>';
                        }

                        contentHtml += "</div>";

                        $("#container-financeiro").append(contentHtml);

                        if (!bl_original) {
                            $("#container-financeiro")
                                .append('<input type="hidden" id="id_venda" name="id_venda" value="' + dataResponse.mensagem[0].get('id_venda') + '"/>');
                        }

                        $("#container-financeiro").append(pageableGrid.render().$el);

                        if (!bl_original) {

                            var html = '<table class="backgrid backgrid-selectall table table-bordered table-hover table_imcube"><thead>' +
                                '<tr><th>Valores a pagar: R$ ' + dataGrid.valorParcelasAPagar().toFixed(2) + '</th></tr>' +
                                '<tr><th>Valores vencidos: R$ ' + dataGrid.valorParcelasEmAtraso().toFixed(2) + '</th></tr>';

                            var situacaoPedido = dataGrid.st_situacaopedido();
                            if (situacaoPedido != undefined) {
                                html = html + '<tr><th>Situação do Pedido: ' + situacaoPedido + '</th></tr>';
                            }
                            html = html + '</thead></table>';
                            $(pageableGrid.render().$el).after(html);

                        }

                        //configura o paginator
                        var paginator = new Backgrid.Extension.Paginator({
                            collection: dataGrid
                        });
                        //renderiza o paginator
                        $("#container-financeiro").append(paginator.render().$el);
                        dataGrid.fetch({reset: true});
                        $("#container-financeiro").append('</div></div>');
                    } else {
                        $("#container-financeiro").append('Sem resultado para exibição!');
                    }
                } else {
                    $("#container-financeiro").html('');
                    $("#container-financeiro").append(dataResponse.text);
                    $.pnotify(dataResponse);
                }
            }
        });
    },
    sincronizarParcelas: function () {
        loading();
        var url = '/default/Robo/sincronizar-dados-fluxus-gestor-w-s';
        var dataResponse;
        var self = this
        $.ajax({
            dataType: 'json',
            type: 'get',
            url: url,
            data: 'id_venda=' + $("#id_venda").val(),
            beforeSend: function () {
                $('.icon-spin').addClass('icon-refresh-animate');
                $('#btn-sincronizar-parcelas').attr('disabled', 'true');
            },
            success: function (data) {
                dataResponse = data;
            },
            complete: function () {
                $.pnotify({type: dataResponse.type, text: dataResponse.message});
                if (dataResponse.type == 'success') {
                    self.renderParcelas();
                }

                loaded();
                $('.icon-spin').removeClass('icon-refresh-animate');
                $('#btn-sincronizar-parcelas').removeAttr('disabled');
            }
        });
    },
    bloquearMatricula: function () {
        this.mostrarCamposAlteracao('bloquear');
    },
    trancarMatricula: function () {
        this.mostrarCamposAlteracao('trancar');
    },
    cancelarMatricula: function () {
        this.mostrarCamposAlteracao('cancelar');
    },
    transferirMatricula: function () {
        this.mostrarCamposAlteracao('transferir');
    },
    anularMatricula: function () {
        this.mostrarCamposAlteracao('anular');
    },
    liberarAcessoMatricula: function () {
        this.mostrarCamposAlteracao('liberar');
    },
    decursoPrazoMatricula: function () {
        this.mostrarCamposAlteracao('prazo');
    },
    mostrarCamposAlteracao: function (st_acao) {
        $('#st_acao').val(st_acao);
        $('#st_motivo').val('');
        if (st_acao == 'trancar') {
            $('#datas-trancar').removeClass('hidden');
        } else {
            $('#datas-trancar').addClass('hidden');
        }
        $('#container-alterar-matricula').removeClass('hidden');
    },
    alterarMatricula: function () {
        var that = this;
        if ($('#st_motivo').val() !== '' && $('#st_motivo').val() !== 'undefined') {
            var retorno = '';
            var dados = 'id_matricula=' + $('#lista-matriculas').val();
            dados += '&st_acao=' + $('#st_acao').val();
            dados += '&st_motivo=' + $('#st_motivo').val();
            if ($('#st_acao').val() == 'trancar') {
                if (($('#dt_abertura_tramite').val() == '') || ($('#dt_encerramento_tramite').val() == '')) {
                    $.pnotify({
                        title: 'Erro ao validar formulário',
                        text: 'Para trancar uma matrícula é necessário selecionar um intervalo de datas.',
                        type: 'warning'
                    });
                    return false;
                }
            }

            dados += '&dt_alteracao[dt_abertura]=' + $('#dt_abertura_tramite').val();
            dados += '&dt_alteracao[dt_encerramento]=' + $('#dt_encerramento_tramite').val();
            $.ajax({
                dataType: 'json',
                type: 'get',
                url: '/default/matricula/alterar',
                data: dados,
                beforeSend: function () {
                    loading();
                },
                success: function (data) {
                    retorno = data;
                },
                complete: function () {
                    loaded();
                    $.pnotify({title: retorno.title, text: retorno.text, type: retorno.type});
                    that.recarregarTela(that.model.get('id_matricula'));
                }
            });
        } else {
            $.pnotify({title: 'Erro ao validar formulário', text: 'Preencha o motivo', type: 'warning'});
        }
    },
    recarregarTela: function (id_matricula) {
        $('#container-lista-matriculas').html('');
        $('#container-lista-matriculas').show();

        if (listMatriculasview == false) {
            listMatriculasview = new ListaMatriculasView();
        } else {
            listMatriculasview.render();

            if (listMatriculasview.matriculaView != null) {
                listMatriculasview.matriculaView.destroyMe();
            }
            $.get('/api/vw-matricula/' + id_matricula, function (data) {
                listMatriculasview.matriculaView = new MatriculaView(data);
                $('#lista-matriculas option[value=' + id_matricula + ']').attr('selected', 'selected');
                $('#container-matricula').html(listMatriculasview.matriculaView.render().el);
                $('#container-matricula').show();
            });

        }
    },
    renderizaTabDataTermino: function () {
        var that = this;
        var matriculaModel = new Matricula();
        //percorre os atributos
        $.each(matriculaModel.defaults, function (key, val) {
            //verifica se no model da view existe o atributo com valor e seta na model que foi instanciada acima
            if (that.model.get(key))
                matriculaModel.set(key, that.model.get(key));

            if (key == 'dt_termino')
                matriculaModel.set('dt_termino', that.model.get('dt_terminomatricula'));


        });
        //renderiza a view da aba
        var viewAba = new AbaDataTerminoItemView({
            el: that.$el.find('#tab7'),
            model: matriculaModel
        });
        viewAba.render();

    },
    renderizarTabMaterial: function () {

        var Collection = Backbone.Collection.extend({
            model: VwEntregaMaterial,
            parse: function (response) {

                var current = null;
                var array = [];
                _.each(response, function (val, key) {
                    if (val.id_pacote == current) {
                        var pos = (key - 1);
                        if (array[pos]) {
                            array[pos].disciplinas.push(val.st_disciplina);
                            array[pos].itens.push(val.st_disciplina);
                        }
                    } else {
                        array.push({
                            id_pacote: val.id_pacote,
                            dt_entrega: val.dt_entrega,
                            st_codrastreamento: val.st_codrastreamento,
                            disciplinas: [val.st_disciplina],
                            itens: [val.st_itemdematerial]
                        });
                    }
                    current = val.id_pacote;

                });

                return array;
            }
        });

        (new CompositeViewMaterial({
            el: '#container-grid-material',
            model: this.model,
            collection: new Collection()
        })).render();
    },

    renderizarOcorrencia: function () {
        (new OcorrenciaCompositeView({
            el: '#container-grid-ocorrencia'
            , model: this.model
            , collection: new OcorrenciaCollection()
        })).render();
    },

    renderAvaliacaoAgendamento: function () {
        (new AvaliacaoAgendamentoCompositeView({
            el: '#container-grid-avaliacao-agendamento'
            , model: this.model
            , collection: new AvaliacaoAgendamentoCollection()
        })).render();
    }
});

/**
 *
 * ******************* REFATORAÇÃO DA TAB GRADE *******************************
 *
 * Item view de disciplina
 */
var DisciplinaItem = Marionette.ItemView.extend({
    template: '#item-disciplinas',
    tagName: 'tr',
    ui: {
        btnHistoricoAlocacao: '#btnHistoricoAlocacao',
        btnProcessaNota: '#btnProcessaNota',
        evolucaoDisciplina: '#evolucaoDisciplina',
        evolucaoNota: '#evolucaoNota',
        disciplina: '#disciplina',
        notafinal: '#notafinal',
        notaAtividade: '#notaatividade',
        notaead: '#notaead',
        stnotaead: '#st_notaead',
        stnotaeadprr: '#st_notaead_prr',
        encerramento: '#encerramento',
        dtencerramento: '#dt_encerramento',
        dtencerramentoprr: '#dt_encerramento_prr',
        notarecuperacao: '#notarecuperacao',
        tcc: '#tcc',
        historicoapr: '#historicoapr',
        historicoTracamento: '#historico-tracamento',
        nunotafinal: '#nu_notafinal',
        btnLogMoodle: '#btnLogMoodle',
        btnLogMoodlePrr: '#btnLogMoodlePrr'
    },
    events: {
        'click @ui.btnHistoricoAlocacao': 'historicoAlocacao',
        'click @ui.btnProcessaNota': 'processaNota',
        'click @ui.historicoapr': 'historicoAproveitamento',
        'click @ui.historicoTracamento': 'historicoTracamento',
        'click @ui.btnLogMoodle': 'LogMoodle',
        'click @ui.btnLogMoodlePrr': 'btnLogMoodlePrr',
    },
    onRender: function () {
        this.executaVerificacoes();
        return this;
    },
    executaVerificacoes: function () {
        var that = this;

        //verifica se vai ou não habilitar o botão de processa nota
        if (!that.model.get('id_avaliacaoconjuntoreferencia')) {
            that.ui.btnProcessaNota.hide();
        } else {
            this.ui.btnProcessaNota.popover();
        }

        //habilita o popover do historico de alocação
        this.ui.btnHistoricoAlocacao.popover();

        //habilita o popover do Log Moodle
        this.ui.btnLogMoodle.popover({html: true});

        /**
         * Executa diversas verificações, e insere o template da celula de acordo com cada uma.
         */
        if (this.model.get("bl_complementar")) {
            disciplina = this.model.get("st_disciplina") + ' <span class="label label-important" title="Discipina complementar">Disciplina complementar</span>';
        } else {
            disciplina = this.model.get("st_disciplina");
        }
        this.ui.disciplina.empty().append(disciplina);

        if (this.model.get("id_tiponotafinal") == 2) {
            notafinal = '<span class="text-warning" title="Nota de aproveitamento">' + this.model.get("st_notafinal") + ' </span>';
        } else {
            notafinal = this.model.get("st_notafinal");
        }
        this.ui.notafinal.empty().append(notafinal);

        if (this.model.get("id_tiponotaead") == 2) {
            ead = '<span class="text-warning" title="Nota de aproveitamento">' + this.model.get("st_notaead") + ' </span>';
        } else {
            ead = this.model.get("st_notaead");
        }
        if (this.model.get("st_notaead_prr") == '') {
            this.ui.notaead.empty().append(ead);
        }

        if (this.model.get("id_tiponotaatividade") == 2) {
            notaatividade = '<span class="text-warning" title="Nota de aproveitamento">' + this.model.get("st_notaatividade") + ' </span>';
        } else {
            notaatividade = this.model.get("st_notaatividade");
        }
        this.ui.notaAtividade.empty().append(notaatividade);

        if (this.model.get("id_aproveitamento") != null) {
            evolucaoDisciplina = "<span> <a id='historicoapr' data-placement='top' data-original-title='Histórico Aproveitamento' data-trigger='hover' data-toggle='popover'>" + this.model.get("st_statusdisciplina") + " </a></span>";
        } else if (this.model.get('id_evolucao') == EVOLUCAO.TB_MATRICULA_DISCIPLINA.TRANCADA) {
            evolucaoDisciplina = "<span> <a id='historico-tracamento' data-placement='top' data-original-title='Histórico de trancamento de disciplina' data-trigger='hover' data-toggle='popover'>" + this.model.get("st_statusdisciplina") + " </a></span>";
        }
        else {
            evolucaoDisciplina = this.model.get("st_statusdisciplina");
        }

        this.ui.evolucaoDisciplina.empty().append(evolucaoDisciplina);

        if (this.model.get('st_status') === 'Aproveitamento de Disciplina') {
            template = "<a class='btn btn-link' style='color: rgb(95, 95, 95);text-align: left; font-size: 12px; padding: 8px 0px;'>" + that.model.get('st_status') + "</a>"
            that.ui.evolucaoNota.empty().append(template);
        }

        dt_string = '';
        if (this.model.get("dt_encerramento") != null) {
            if (that.model.get("dt_encerramento") != that.model.get("dt_encerramentoextensao")) {
                dt_string = "<div class='div-dt-extensao'><span class='dt_encerramentoextensao'>" + that.model.get("dt_encerramentoextensao") + "</span> <i class='icon-blue icon-info-sign avisopo' data-placement='right' data-trigger='hover' data-content='Data de encerramento após extensão. Data original: " + that.model.get("dt_encerramento") + "' data-toggle='popover'></i> </div>";
            } else {
                dt_string = this.model.get("dt_encerramento");
            }
        }
        this.ui.dtencerramento.empty().append(dt_string);
        this.ui.dtencerramento.find('.avisopo').popover();
        this.delegateEvents();
        var tcc = false;
        if (this.model.get("st_notatcc_prr") == '') {
            if (this.model.get("id_tiponotatcc") == 2) {
                tcc = '<span class="text-warning" title="Nota de aproveitamento">' + this.model.get("st_notatcc") + ' </span>';
            } else if (this.model.get("id_tipodisciplina") == 2 && (this.model.get('dt_defesatcc') === null)) {
                dt_string = "-";
            } else {
                tcc = this.model.get("st_notatcc");
            }
            if (tcc) {
                this.ui.tcc.empty().append(tcc);
            }
        } else if (this.model.get("st_notatcc_prr") >= 0) {
            this.ui.notaead.empty().append(this.model.get("st_notaead"));
        }


        /**
         * Verifica o status da evolução da nota e adiciona classes com cores ao templete da celula.
         */
        if (this.model.get('st_status') === 'Aprovado') {
            that.ui.evolucaoNota.addClass('aprovado');
        } else if (this.model.get('st_status') === 'Satisfatório') {
            that.ui.evolucaoNota.addClass('satisfatorio');
        } else if (this.model.get('st_status') === 'Insatisfatório') {
            that.ui.evolucaoNota.addClass('insatisfatorio');
        }

        /**
         * Verifica a linha de negócio e apresenta as colunas de acordo com a necessidade.
         */
        if (linhaNegocio.get('st_valor') == LINHA_DE_NEGOCIO.GRADUACAO) {
            that.ui.evolucaoNota.hide();
        } else {
            that.ui.notaAtividade.hide();
            that.ui.notarecuperacao.hide();
            that.ui.evolucaoDisciplina.hide();
        }

        /**
         * Habilita o cursor do mouse como do tipo pointer!
         */
        this.$el.css('cursor', 'pointer');

        if (!entidade_propria) {
            this.ui.btnProcessaNota.hide();
            this.ui.btnHistoricoAlocacao.hide();
        }

        return this;
    },
    historicoAlocacao: function (e) {

        /**
         * Cria a modal com o histórico de alocação da disciplina.
         */

        $('#container-modal-historico').empty();
        var customModal = $(
            '<div class="modal hide fade" id="modal-historico-alocacao" tabindex="-1" role="dialog" aria-labelledby="Histórico de Alocação" aria-hidden="false">' +
            '<div class="modal-dialog">' +
            '<div class="modal-content">' +
            '<div class="modal-header">' +
            '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
            '<h4 class="modal-title" id="myModalLabel">Histórico de alocações:</h4>' +
            '</div>' +
            '<div class="modal-body">' +
            '<div id="container-grid-historico-alocacao" class="control-group">' +
            '</div>' +
            '<div class="modal-footer">' +
            '<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>'
        );

        $('#container-modal-historico').append(customModal);

        $('.modal .hide').show();
        $("#modal-historico-alocacao").modal('show');

        //inicio da busca dos históricos da alocação
        var modelHistorico = Backbone.Model.extend({});
        var DataGridHistorico = Backbone.PageableCollection.extend({
            model: modelHistorico,
            url: "/",
            state: {pageSize: 15},
            mode: "client"
        });

        var urlHistorico = '/matricula/retorna-historico-alocacao';
        var dataResponseHistorico;
        $.ajax({
            dataType: 'json',
            type: 'get',
            url: urlHistorico,
            data: {id_matriculadisciplina: this.model.get('id_matriculadisciplina')},
            beforeSend: function () {
            },
            success: function (data) {
                dataResponseHistorico = data;
            },
            complete: function () {
                var dataGridHistórico = new DataGridHistorico(dataResponseHistorico);
                var columnsHistorico = [{
                    name: "st_saladeaula",
                    label: "Sala de Aula",
                    editable: false,
                    cell: "string"
                },
                    {
                        name: "dt_cadasto",
                        label: "Data de Cadastro",
                        editable: false,
                        cell: "string"
                    },
                    {
                        name: "dt_abertura",
                        label: "Abertura da Sala",
                        editable: false,
                        cell: "string"
                    },
                    {
                        name: "dt_encerramento",
                        label: "Encerramento da Sala",
                        editable: false,
                        cell: "string"
                    },
                    {
                        name: "dt_inicio",
                        label: "Início da Alocação",
                        editable: false,
                        cell: "string"
                    },
                    {
                        name: "st_nomecompleto",
                        label: "Usuário de cadastro",
                        editable: false,
                        cell: "string"
                    },
                    {
                        name: "st_situacao", label: "Situação", editable: false, cell: "string"
                    }
                ];
                $("#container-grid-historico-alocacao").html('');
                if (dataGridHistórico.length > 0) {
                    var pageableGridHistorico = new Backgrid.Grid({
                        columns: columnsHistorico,
                        collection: dataGridHistórico,
                        className: 'backgrid backgrid-selectall table table-bordered table-hover'
                    });
                    $("#container-grid-historico-alocacao").append(pageableGridHistorico.render().$el);
                    var paginatorHistorico = new Backgrid.Extension.Paginator({
                        collection: dataGridHistórico
                    });


                    $("#container-grid-historico-alocacao").append(paginatorHistorico.render().$el);


                } else {
                    $("#container-grid-historico-alocacao").html('Aluno não possui alocações nessa disciplina!');
                }
                loaded();
            }
        });
        //fim da busca dos históricos da alocação


    },
    processaNota: function () {
        /**
         * Processa a nota de uma disciplina.
         * @type {DisciplinaItem}
         */
        var that = this;
        $.ajax({
            dataType: 'json',
            type: 'post',
            url: '/matricula/processa-nota-disciplina/',
            data: {
                id_matricula: this.model.get('id_matricula'),
                id_matriculadisciplina: this.model.get('id_matriculadisciplina'),
                id_avaliacaoconjuntoreferencia: this.model.get('id_avaliacaoconjuntoreferencia')
            },
            beforeSend: loading,
            success: function (response) {

                //atualiza em tempo de execução os campos da grid ao clicar em "processar nota"
                $.pnotify(response);

                if (response.st_notafinal) {
                    that.ui.notafinal.empty().append(response.st_notafinal);
                }
                ;

                if (response.st_notaAtividade) {
                    that.ui.notaAtividade.empty().append(response.st_notaAtividade);
                }
                ;

                if (response.temPrr == false) {
                    if (response.st_notaead) {
                        that.ui.notaead.empty().append(response.st_notaead);
                    }
                    ;
                } else {

                    if (response.st_notaead) {
                        that.ui.stnotaead.empty().append(response.st_notaead);
                    }
                    ;

                    if (response.st_notaead_prr) {
                        that.ui.stnotaeadprr.empty().append(response.st_notaead_prr);
                    }
                    ;
                }

                if (response.st_notarecuperacao) {
                    that.ui.notarecuperacao.empty().append(response.st_notarecuperacao);
                }
                ;

                if (response.st_notatcc) {
                    that.ui.tcc.empty().append(response.st_notatcc);
                }
                ;

                if (response.nu_notafinal) {
                    that.ui.nunotafinal.empty().append(response.nu_notafinal);
                }
                ;

                if (response.st_status) {
                    that.ui.evolucaoNota.empty().append(response.st_status);
                    that.ui.evolucaoNota.removeClass().addClass(response.st_status.toLowerCase().removeAccents().trim());
                }
                ;
                loaded();
            },
            complete: loaded
        });
    },

    historicoAproveitamento: function (e) {
        /**
         * Cria a modal com o historico de aproveitamento.
         */
        $('#container-modal-aproveitamento').empty();
        var modalAproveitamento = $(
            '<div class="modal hide fade modalAproveitamento" id="modal-historico-aproveitamento" tabindex="-1" role="dialog" aria-labelledby="Histórico de Aproveitamento" aria-hidden="false">' +
            '<div class="modal-dialog">' +
            '<div class="modal-content">' +
            '<div class="modal-header">' +
            '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
            '<h4 class="modal-title">Histórico de Aproveitamento</h4>' +
            '</div>' +
            '<div class="modal-body">' +
            '<div id="container-grid-historico-aproveitamento" class="control-group">' +
            '</div>' +
            '<div class="modal-footer">' +
            '<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>'
        );

        $('#container-modal-aproveitamento').append(modalAproveitamento);

        $('.modalAproveitamento').show();
        $("#modal-historico-aproveitamento").modal('show');
        var modelHistoricoAproveitamento = Backbone.Model.extend({});
        var DataGridHistoricoAproveitamento = Backbone.PageableCollection.extend({
            model: modelHistoricoAproveitamento,
            state: {pageSize: 15},
            mode: "client"
        });

        var dataGridAproveitamento = new DataGridHistoricoAproveitamento(this.model);
        var columnsAproveitamento = [
            {
                editable: false,
                name: "st_disciplinaoriginal",
                label: "Disciplinas de Origem",
                cell: Backgrid.Cell.extend({
                    render: function () {
                        this.$el.html(this.model.get('st_disciplinaoriginal').split('|').join('<br/>'));
                        return this;
                    }
                })
            },
            {
                editable: false,
                name: "dt_cadastroaproveitamento",
                label: "Data",
                cell: "string"
            },
            {
                editable: false,
                name: "st_nomeusuarioaproveitamento",
                label: "Responsável",
                cell: "string"
            }
        ];

        $("#container-grid-historico-aproveitamento").html('');
        if (dataGridAproveitamento.length > 0) {
            var pageableGridAproveitamento = new Backgrid.Grid({
                columns: columnsAproveitamento,
                collection: dataGridAproveitamento,
                className: 'backgrid backgrid-selectall table table-bordered table-hover'
            });
            $("#container-grid-historico-aproveitamento").append(pageableGridAproveitamento.render().$el);
        } else {
            $("#container-grid-historico-aproveitamento").html('Nenhuma disciplina marcada como aproveitamento.');
        }


    },
    /**  Lista historico do trancamento de disciplina   **/
    historicoTracamento: function () {

        var container = $('#container-modal-trancamento');
        container.empty();
        var modalTrancamento = $('#template-historico-trancamento').html();
        container.append(modalTrancamento);

        var containerGrid = $('#container-grid-historico-trancamento');
        containerGrid.empty();

        $('.modal .hide').show();
        $("#modal-historico-trancamento").modal('show');

        //inicio da busca dos históricos da alocação
        var modelHistorico = Backbone.Model.extend({});
        var dataGridTrancamento = Backbone.PageableCollection.extend({
            model: modelHistorico,
            state: {pageSize: 15},
            mode: "client"
        });

        var urlRequest = '/matricula/retorna-historico-alocacao';
        var dataResponse;
        $.ajax({
            dataType: 'json',
            type: 'get',
            url: urlRequest,
            data: {
                id_matriculadisciplina: this.model.get('id_matriculadisciplina')
                , bl_trancamento: 1
            },
            success: function (data) {
                dataResponse = data;
            },
            complete: function () {
                var gridTrancamento = new dataGridTrancamento(dataResponse);
                var columnsTrancamento = [
                    {name: "st_disciplina", label: "Disciplina", cell: "string"},
                    {name: "dt_trancamento", label: "Data do trancamento", cell: "string"},
                    {name: "st_usuariotrancamento", label: "Responsável", cell: "string"}
                ];

                if (gridTrancamento.length > 0) {
                    var dadosGrid = new Backgrid.Grid({
                        columns: columnsTrancamento,
                        collection: gridTrancamento,
                        className: 'backgrid backgrid-selectall table table-bordered table-hover'
                    });
                    containerGrid.append(dadosGrid.render().$el);
                } else {
                    containerGrid.html('Nenhum histórico de trancamento para exibição');
                }
                loaded();
            }
        });
        //fim do histórico de trancamento na alocação
    },

    btnLogMoodlePrr: function () {
        var data = {
            id_saladeaula: this.model.get('id_saladeaula_prr'),
            id_matricula: this.model.get('id_matricula')
        };
        $.ajax({
            dataType: 'json',
            type: 'post',
            url: '/matricula/autenticar-log-moodle',
            data: data,
            beforeSend: function () {

            },
            success: function (data) {
                if (data.tipo == 1) {
                    window.open(data.mensagem, '_blank');
                } else {
                    $.pnotify({
                        type: 'warning',
                        title: 'Atenção!',
                        text: data.text
                    });
                }
            },
            error: function (data) {
                $.pnotify({
                    type: 'warning',
                    title: 'Atenção!',
                    text: data.mensagem
                });
            }
        });
    },
    LogMoodle: function () {
        var data = {
            id_saladeaula: this.model.get('id_saladeaula'),
            id_matricula: this.model.get('id_matricula')
        };
        $.ajax({
            dataType: 'json',
            type: 'post',
            url: '/matricula/autenticar-log-moodle',
            data: data,
            beforeSend: function () {

            },
            success: function (data) {
                if (data.tipo == 1) {
                    window.open(data.mensagem, '_blank');
                } else {
                    $.pnotify({
                        type: 'warning',
                        title: 'Atenção!',
                        text: data.text
                    });
                }
            },
            error: function () {
                $.pnotify({
                    type: 'warning',
                    title: 'Atenção!',
                    text: 'Usuário atualmente não existe no Moodle.'
                });
            }
        });
    }
});

/**
 * Composite filha, para cada módulo vai existir uma composite desta.
 */
var ModulosComposite = Marionette.CompositeView.extend({
    template: '#view-modulos-disciplinas',
    tagName: 'div',
    childView: DisciplinaItem,
    childViewContainer: 'tbody',

    ui: {
        columnProvaPresencial: '#columnProvaPresencial',
        columnNotaAtividadePresencial: '#columnNotaAtividadePresencial',
        columnNotaRecuperacao: '#columnNotaRecuperacao',
        columnEvolucaoDisciplina: '#columnEvolucaoDisciplina',
        columnEvolucaoNota: '#columnEvolucaoNota'
    },
    initialize: function () {
        // ordena a collection por data de abertura
        var ordenado = _.sortBy(this.model.get('disciplinas'), function (num) {
            if (num.dt_abertura && num.dt_abertura != '-') {
                return formatarData(num.dt_abertura).getTime();
            } else {
                if ((num.dt_abertura_prr)) {
                    return formatarData(num.dt_abertura_prr).getTime();
                } else {
                    return null;
                }
            }
        });

        this.collection = new Backbone.Collection(ordenado);
    },
    onRender: function () {

        if (linhaNegocio.get('st_valor') == LINHA_DE_NEGOCIO.GRADUACAO) {
            this.ui.columnEvolucaoNota.hide();
        } else {
            this.ui.columnNotaAtividadePresencial.hide();
            this.ui.columnNotaRecuperacao.hide();
            this.ui.columnEvolucaoDisciplina.hide();
        }

    }
});

/**
 * Composite pai, vai conter as várias composites referentes aos modulos.
 */
var ModulosPaiComposite = Marionette.CompositeView.extend({
    template: '#template-modulos-pai',
    tagName: 'div',
    childView: ModulosComposite,
    childViewContainer: '#container-filhos',
    ui: {
        'containerVisualizarHistorico': '#container-visualizar-historico',
        'btnVisualizarHistorico': '#btn-visualizar-historico',
        'containerModalHistoricoGrade': '#container-modal-historico-grade'
    },
    events: {
        'click @ui.btnVisualizarHistorico': 'abreModalHistorico'
    },
    initialize: function () {
        this.fetchModulos();
    },
    onRender: function () {
        this.verificaPermissaoBotaoHistorico();
    },
    fetchModulos: function () {
        this.collection.composite = this;
        this.collection.fetch({
            data: {
                id_matricula: $('#lista-matriculas').val()
            },
            beforeSend: loading,
            complete: loaded
        });
    },
    abreModalHistorico: function (e) {
        e.preventDefault();
        var modalHistorico = new ModalHistoricoGrade();
        modalHistorico.render();
        this.ui.containerModalHistoricoGrade.html(modalHistorico.$el);
        modalHistorico.$el.modal("show");
    },
    verificaPermissaoBotaoHistorico: function () {
        var self = this;
        VerificarPerfilPermissao(PERMISSAO.EXIBIR_BOTAO_DE_HISTORICO, FUNCIONALIDADE.MATRICULAS, true).done(function (response) {
            if (+response.length) {
                self.ui.containerVisualizarHistorico.removeClass('hide');
            }
        });
    }
});

var CollectionGradeNotaPorStatusDisciplina = Backbone.Collection.extend({
    model: Backbone.Model,
    url: 'matricula/retornar-vw-grade-nota-por-status-disciplina'
});

var HistoricoGradeItemView = Marionette.ItemView.extend({
    template: '#itemview-historico-grade',
    tagName: 'tr',
    onBeforeRender: function () {
        this.ajusteDados()
    },
    ajusteDados: function (model) {
        this.processaDatas();
        this.model.set("nu_notafinal", this.processaNotaFinal());
        if (this.model.get("st_notaead")) {
            this.model.set("st_notaead", +this.model.get("st_notaead"));
        }
        if ((this.model.get("id_evolucao")
            && +G2S.loggedUser.get('id_linhadenegocio') === +LINHA_DE_NEGOCIO.GRADUACAO)
        && +this.model.get("id_evolucao") === +EVOLUCAO.TB_MATRICULA_DISCIPLINA.INSATISFATORIO) {
            this.model.set("st_status", 'Reprovado');
        }
    },
    processaNotaFinal() {
        var idDisciplinaPrr = this.model.get("id_disciplina_prr");
        var stNotaEadPrr = this.model.get("st_notaead_prr");
        var stNotaEad = this.model.get("st_notaead");
        var idTipoDisciplina = this.model.get("id_tipodisciplina");
        var dtDefesaTcc = this.model.get("dt_defesatcc");

        if (idDisciplinaPrr != "" && (stNotaEadPrr > stNotaEad)) {
            if (idTipoDisciplina == 2 && dtDefesaTcc === null) {
                return "-";
            } else {
                return this.model.get("nu_notafinal_prr");
            }
        } else {
            if (idTipoDisciplina == 2 && dtDefesaTcc === null) {
                return "-";
            } else {
                return this.model.get("nu_notafinal");
            }
        }
    },
    processaDatas() {
        var self = this;
        var campos = ["dt_abertura", "dt_encerramento"];

        campos.forEach(function (campo) {

            var dt;
            if (self.model.get(campo)) {
                dt = self.model.get(campo);
                if (dt.date) {
                    dt = dateToPtBr(new Date(dt.date));
                }
            }

            self.model.set(campo, dt || "-");
        });
    }
});

var ModalHistoricoGrade = Marionette.CompositeView.extend({
    template: '#compositeview-modal-historico-grade',
    className: 'modal hide fade',
    childView: HistoricoGradeItemView,
    childViewContainer: 'tbody',
    collection: new CollectionGradeNotaPorStatusDisciplina(),
    emptyView: Marionette.ItemView.extend({
        template: _.template('<p>Carregando...</p>')
    }),
    initialize: function () {
        loading();
        var self = this;
        this.collection.fetch({
            data: {
                "id_matricula": $('#lista-matriculas').val()
            },
            complete: loaded
        });
    },
    onBeforeRender: function () {

        /* Alterando o tamanho da modal do BS2. Cálculo:
        * Width: X px
        * Margin-left: 1/2X px - 30px */

        this.$el.css({
            'width': '900px',
            'margin-left': '-420px'
        });
    }
});

/**
 *******************************************************************************
 *******************************************************************************
 **                Outras funcionalidades
 *******************************************************************************
 *******************************************************************************
 */
var renderizaAutoComplete = function (model) {
    componenteAutoComplete.main({
        model: model,
        element: $("#autoComplete"),
        entidadesMesmaHolding: true
    }, function () {
        $('#container-lista-matriculas').html('');
        $('#container-lista-matriculas').show();
        if (listMatriculasview == false) {
            listMatriculasview = new ListaMatriculasView();
        } else {
            listMatriculasview.render();
        }
    }, function () {
        if (listMatriculasview != false && listMatriculasview.matriculaView != null) {
            listMatriculasview.matriculaView.destroyMe();
        }
        $('#container-lista-matriculas, #container-matricula').html('');
        $('#container-lista-matriculas, #container-matricula').hide();
    });
}

var pessoa = JSON.parse(localStorage.getItem('pessoaAutocomplete'));

// $.getScript('/modelo-backbone/VwPesquisarPessoa', function () {

//verifica se existe dados na variável "pessoa" e se existe nela existe a variável "entidades_matriculadas"
if (pessoa && typeof pessoa.entidades_matriculadas === "undefined") {
    var Model = VwPesquisarPessoa.extend({
        parse: function (response) {
            if (typeof response[0] !== "undefined") {
                return response[0];
            }
        }
    });
    //faz novamente a pesquisa e seta os valores na modelo "Model"
    (new Model).fetch({
        data: {
            id: pessoa.id_usuario,
            all: 1
        },
        url: '/api/pessoa',
        success: function (model) {
            componenteAutoComplete.clearFixModel();
            componenteAutoComplete.modelRender = model.toJSON();
            componenteAutoComplete.setFixModel();
            renderizaAutoComplete(Model);
        }
    });
} else {
    renderizaAutoComplete(VwPesquisarPessoa)
}

// });

$.ajaxSetup({async: true});

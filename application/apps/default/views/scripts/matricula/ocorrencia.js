var OcorrenciaModel = Backbone.Model.extend({
    defaults: {
        id_ocorrencia: null,
        st_assuntocopai: null,
        st_assuntoco: null,
        st_titulo: null,
        st_evolucao: null,
        st_situacao: null,
        dt_cadastro: null
    },
    idAttribute: 'id_ocorrencia'
});

var OcorrenciaCollection = Backbone.Collection.extend({
     model: OcorrenciaModel
});

var OcorrenciaEmptyView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: _.template('<td colspan="5"><center>Nenhuma informação para ser visualizada</center></td>')
});

var OcorrenciaItemView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: '#tpl-ocorrencia-item-view'
});

var OcorrenciaCompositeView = Marionette.CompositeView.extend({
    template: '#tpl-ocorrencia-composite-view',
    childViewContainer: 'tbody',
    emptyView: OcorrenciaEmptyView,
    childView: OcorrenciaItemView,
    onRender: function () {
        this.collection.fetch({
            url: '/api/vw-ocorrencia/index',
            data: {
                id_entidade: G2S.loggedUser.get('id_entidade'),
                id_matricula: this.model.get('id_matricula'),
            }
        });
    }
});
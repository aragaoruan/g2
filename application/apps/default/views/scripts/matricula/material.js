var EmptyView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: _.template('<td colspan="5"><center>Nenhuma informação para ser visualizada</center></td>')
});

var MaterialItemView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: '#tpl-item-view-material'
});

var TB_PACOTE = {
    A_ENVIAR: 155,
    ENVIADO: 156
};

var CompositeViewMaterial = Marionette.CompositeView.extend({
    template: '#tpl-composite-view-material',
    childViewContainer: 'tbody',
    emptyView: EmptyView,
    childView: MaterialItemView,
    onRender: function () {

        this.collection.fetch({
            url: '/api/vw-entrega-material/index',
            data: {
                id_entidade: G2S.loggedUser.get('id_entidade'),
                id_matricula: this.model.get('id_matricula'),
                id_situacaopacote: TB_PACOTE.ENVIADO
            }
        });
    }
});
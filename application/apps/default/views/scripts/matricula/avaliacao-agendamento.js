var AvaliacaoAgendamentoModel = Backbone.Model.extend({
    defaults: {
        st_avaliacao: null,
        st_usuariolancamento: null,
        dt_agendamento: null,
        dt_aplicacao: null,
        st_aplicadorprova: null,
        st_disciplina: null,
        statusProva: null,
        statusPresenca: null,
    }
});

var AvaliacaoAgendamentoCollection = Backbone.Collection.extend({
    url: '/gerencia-prova/get-prova-agendadas/',
    model: AvaliacaoAgendamentoModel,
    parse: function (response) {

        if (! response.length) {
            var resNotFound = new AvaliacaoAgendamentoEmptyView();
            $('tbody', this.$el).html(resNotFound.render().el);
            return;
        }

        this.trigger("fetch", this);

        return response;
    }
});

var AvaliacaoAgendamentoLoadingEmptyView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: _.template('<td colspan="8"><center>Por favor, aguarde enquanto as informações são recuperadas.</center></td>')
});

var AvaliacaoAgendamentoEmptyView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: _.template('<td colspan="8"><center>Nenhuma informação para ser visualizada</center></td>')
});

var AvaliacaoAgendamentoItemView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: '#tpl-avaliacao-agendamento-item-view'
});

var AvaliacaoAgendamentoCompositeView = Marionette.CompositeView.extend({
    template: '#tpl-avaliacao-agendamento-composite-view',
    childViewContainer: 'tbody',
    emptyView: AvaliacaoAgendamentoEmptyView,
    childView: AvaliacaoAgendamentoItemView,

    initialize: function () {
        this.collection.on('request', function () {
            var vwLoader = new AvaliacaoAgendamentoLoadingEmptyView();
            $('tbody', this.$el).html(vwLoader.render().el);
        }, this);

        this.collection.on('fetch', function () {
            $('tbody', this.$el).empty();
        }, this);
    },

    onRender: function () {
        this.collection.fetch({ data: {
                id_matricula: this.model.get('id_matricula'),
                id_tipoavaliacao: TIPO_AVALIACAO.AVALIACAO_PRESENCIAL
            },
        });
    }
});

var AbaDataTerminoItemView = Marionette.ItemView.extend({
    template: "#template-aba-data-termino",
    initialize: function () {
        this.calculaDiferencaEntreDias();
        this.model.edited = false;
    },
    ui: {
        nu_dias: 'input[name="nu_dias"]',
        dt_dias: 'input[name="dt_dias"]',
        btnAtualizar: '#btn-atualizar',
        btnSalvar: '#btn-salvar-data',
        minimoDate: '#minimoDate',
        btnDatePicker: '#btn-calendar-dt_dias'
    },
    events: {
        'change input[name="nu_dias"]': 'calculaDataFutura',
        'keyup input[name="nu_dias"]': 'calculaDataFutura',
        'change input[name="dt_dias"]': 'calculaNumeroDias',
        'click #btn-atualizar': 'atualizaDataTermino',
        'click #btn-salvar-data': 'salvarDtTermino',
        'click #btn-calendar-dt_dias': function () {
            this.ui.dt_dias.focus();
        }
    },
    calculaDiferencaEntreDias: function () {
        //calcula a diferença entre a data inicio da matricula e a data termino
        var diferencaDias = 0;
        if (this.model.get('dt_inicio') && this.model.get('dt_termino')) {
            diferencaDias = diferencaEntreDatas(this.model.get('dt_inicio'), this.model.get('dt_termino'));
        }
        this.model.set('diferenca', diferencaDias);
    },
    toggleBotaoAtualizar: function () {
        //habilita ou desabilita o botão de atualizar data
        if (this.ui.dt_dias.val() && this.ui.nu_dias.val()) {
            this.ui.btnAtualizar.removeAttr('disabled');
        } else {
            this.ui.btnAtualizar.attr('disabled', true);
        }
    },
    calculaNumeroDias: function () {
        //calcula o numero de dias prorrogado baseado na data selecionada
        var data = this.ui.dt_dias.val();
        if (data) {
            var diferencaDias = 0;//inicia a diferenca como zero
            //verifica se a data foi passada e se a data de termino também esta setada
            if (data && this.model.get('dt_termino')) {
                //calcula a diferença entra as datas
                diferencaDias = diferencaEntreDatas(this.model.get('dt_termino'), data);
                //verifica se diferença é diferente de zero
                if (diferencaDias) {
                    this.ui.nu_dias.val(diferencaDias);
                } else {//mostra o warning se a diferença for igual zero
                    $.pnotify({
                        type: 'warning',
                        title: 'Atenção!',
                        text: 'Você não pode prorrogar a data de término para a data selecionada. ' + diferencaDias + ' dia(s).'
                    });
                    this.ui.nu_dias.val(null);
                }
            }
        }
        //chama a função para habilitar ou desabilitar os campos
        this.toggleBotaoAtualizar();
    },
    calculaDataFutura: function () {
        //calula a data baseada no numero de dias informado
        var nu_dias = this.ui.nu_dias.val();
        if (nu_dias) {
            //pega a data de termino da matricula
            var dataInicio = this.model.get('dt_termino').split(' ');
            if (dataInicio[0]) {
                //calula qual é a proxima data baseado na data de termino e numero de dias informado
                var proximaData = proximaDataVencimento(dataInicio[0], nu_dias, 'd');
                //pega a data retornada pela função e trata ela para exibição.
                var dt_proximaData = (proximaData.getDate() < 10 ? "0" + proximaData.getDate() : proximaData.getDate()) + "/" + (proximaData.getMonth() < 9 ? "0" + (parseInt(proximaData.getMonth()) + 1 ) : (parseInt(proximaData.getMonth()) + 1 )) + "/" + proximaData.getFullYear();
                this.ui.dt_dias.val(dt_proximaData)
            }
        } else {//se não for informado o numero de dias zera o campo de data
            this.ui.dt_dias.val(null);
        }
        //chama a função que habilita ou desabilita o botão de atualizar
        this.toggleBotaoAtualizar();
    },
    validaNovaDataTermino: function () {
        //valida as datas antes de atualizar

        //atribui as datas as variaveis removendo as barras
        var novaData = this.ui.dt_dias.val();
        var dt_termino = this.model.get('dt_termino');
        var valid = true;//cria uma variavel com valor padrao true para validar os dados

        //verifica se o valor veio preenchido
        if (!novaData) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Informe a nova data para término.'
            });
            valid = false;//se entrou no if é porque o valor não esta preenchido então quer dizer que não é valido
        }

        //valida se a data nova é maior que data já definida
        if (!diferencaEntreDatas(dt_termino, novaData)) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'A nova data de término não pode ser menor ou igual a data de término já definida.'
            });
            valid = false;
        }

        return valid;

    },
    atualizaDataTermino: function () {
        //atualiza a data de termino na modelo
        var dt_terminoNova = this.ui.dt_dias.val();
        if (this.validaNovaDataTermino()) {//valida os dados
            //seta o valor na model
            this.model.set('dt_termino', dt_terminoNova);
            this.model.edited = true;//altera o atributo da model dizendo que foi editada e pode ser salva
            this.calculaDiferencaEntreDias();//calcula a diferença entre a data inicio da matricula e a data final com a nova data
            this.render();//renderiza novamente a view
        }
    },
    salvarDtTermino: function () {
        //salva a nova data
        var that = this;//variavel para escopo
        //pergunta se é mesmo para salvar
        bootbox.confirm("Deseja realmente atlerar a data de término da matrícula?", function (result) {
            if (result) {//true
                //salva a model
                that.model.url = '/matricula/alterar-data-termino';
                that.model.save(null, {
                    beforeSend: function () {
                        loading();
                    },
                    complete: function () {
                        loaded();
                    },
                    error: function (model, response) {
                        var textResponse = response.responseText;
                        $.pnotify({
                            title: 'Erro!',
                            text: "Erro ao tentar salvar data de término da matrícula. " + textResponse,
                            type: 'error'
                        });
                    },
                    success: function (model, response) {
                        $.pnotify({
                            title: 'Sucesso!',
                            text: 'Dados salvo com sucesso!',
                            type: 'success'
                        });
                        that.model.edited = false;
                        that.render();
                    }
                });
            }
        });
    },
    onRender: function () {
        if (this.model.edited) {
            this.ui.btnSalvar.removeAttr('disabled');
        } else {
            this.ui.btnSalvar.attr('disabled', 'disabled');
        }

        if (this.model.get('dt_inicio')) {
            var dtinicio = this.model.get('dt_inicio').split(' ');
            this.ui.minimoDate.val(dtinicio[0]);
        }

        if (!entidade_propria) {
            $('.mudar_termino input').prop('disabled', true);
            $('.mudar_termino button').prop('disabled', true);
        } else {
            $('.mudar_termino input').prop('disabled', false);
            $('.mudar_termino button').prop('disabled', false);
        }

        return this;
    }
});
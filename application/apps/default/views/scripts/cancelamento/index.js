/*********************************************
 **************    MODELS   ******************
 *********************************************/

var CancelamentoModel = Backbone.Model.extend({
    defaults: {
        id: '',
        id_matricula: '',
        id_evolucao: '',
        st_evolucao: '',
        st_observacao: '',
        dt_solicitacao: '',
        nu_diasAcesso: '',
        motivos: [],
        //valorBruto: '',
        valorNegociado: '',
        valorDesconto: '',
        valorCargaHoraria: ''
    }
});

/*********************************************
 ************** COLLECTIONS ******************
 *********************************************/

var CollectionMotivo = Backbone.Collection.extend({
    model: Motivo,
    url: '/api/motivo'
});

var CollectionCancelamento = Backbone.Collection.extend({
    model: CancelamentoModel
});

var CollectionLog = Backbone.Collection.extend({
    model: LogCancelamento
});

/*********************************************
 ************** VARIAVEIS ********************
 *********************************************/
var pessoaDadosCancelamento;
var collectionMotivos;
var collectionCancel;
var valorMaterial;
var valorUtilizadoMaterial;
var valorNegociacao;
var valorHoraAula;
var cargaHoraria;
var cargaHorariaCursada;
var valorTotal;
var valorMultaCarta;
var valorMultaDevolucao;
var valorPercentualMulta;
var valorPercentualMultaCarta;
var valorPercentualMultaDevolucao;
var valorTotalUtilizado;
var valorTotalDevolucao;
var valorTotalCarta;
var valorDiferenca;
var valorPago;
var valorPagoCarta;
//var valorBruto;
var id_matricula;
var id_cancelamento;

/*********************************************
 ************** LAYOUT ******************
 *********************************************/

var LayoutCancelamento = Marionette.LayoutView.extend({
    template: '#template-dados-cancelamento',
    tagName: 'div',
    className: 'container-fluid',
    regions: {
        modal: '#modalDadosGeral',
        matricula: '#dados-cancelamento-matricula-wrapper',
        evolucao: '#dados-cancelamento-evolucao-wrapper',
        datas: '#dados-cancelamento-data-wrapper',
        motivo: '#dados-cancelamento-motivo-wrapper',
        observacao: '#dados-cancelamento-observacao-wrapper',
        salvardados: '#dados-cancelamento-botoes-wrapper',
        valorInicial: '#calculo-valor-inicial-wrapper',
        valorRecisao: '#calculo-valor-recisao-wrapper',
        observacaoValores: '#calculo-observacao-wrapper',
        salvarValores: '#calculo-botoes-wrapper',
        filtroLog: '#filtro-log-wrapper',
        tabelaLog: '#tabela-log-wrapper'
    },
    events: {
        modalDados: '#modalDadosGeral',
        "click #tabDadosCancelamento li": "eventTrocarAba",
        "click #btn-salvar-dados": "salvarDadosGerais",
        "change #id_matricula": "carregaDados",
        "focusout #valorMaterial": "calculaMateriais",
        "focusout #valorUtilizadoMaterial": "calculaMateriais",
        "focusout #cargaHorariaCursada": "calculaRecisao",
        "focusout #percentualMultaCarta": "calculaRecisao",
        "focusout #percentualMultaDevolucao": "calculaRecisao",
        "click #btn-salvar-valores": "salvarCalculos",
        "click #btn-finalizar-carta": "finalizarCancelamento",
        "click #btn-filtrar-log": "filtrarLog",
        "keydown #cargaHorariaCursada": "somenteNumeros",
        "click #btn-gerar-carta": "imprimirCarta",
        "click #btn-atualizar-materiais": "atualizaMateriais",
        "click #btn-atualizar-carga-cursada": "atualizaCargaHorariaUtilizada"
    },
    initialize: function () {
    },
    onShow: function () {
        thatLayout = this;

        if (!componenteAutoComplete.getFixModel()) {
            $('#content-dados').hide('fast');
            $('#content-log').hide('fast');
        }

        //Chama o componente de autocomplete
        componenteAutoComplete.main({
            model: VwPesquisarPessoa,
            element: $("#autoComplete")
        }, function () {
            pessoaDadosCancelamento = componenteAutoComplete.getFixModel();
            abreDadosCancelamento();
        }, function () {
            pessoaDadosCancelamento = '';
            fechaDadosCancelamento();
        });

        this.formataCampos();


        return this;

    },
    eventTrocarAba: function (e) {
        if ($(e.currentTarget).hasClass('disabled')) {
            return false;
        }
        return this;
    },
    atualizaMateriais: function () {

        var parametros = {
            'id_matricula': $('#id_matricula option:selected').val()
        }
        $.ajax({
            type: 'post',
            url: '/cancelamento/atualiza-materiais',
            data: parametros,
            beforeSend: function () {
                loading();
            },
            success: function (data) {


                $.pnotify({
                    title: 'Sucesso!',
                    text: 'Materiais atualizados com sucesso.',
                    type: 'success'
                });

                if (data.nu_valortotal && data.nu_valortotalutilizado) {
                    valorMaterial = data.nu_valortotal;
                    valorUtilizadoMaterial = data.nu_valortotalutilizado;
                    $('#valorMaterial').val(formatValueToShow(data.nu_valortotal));
                    $('#valorUtilizadoMaterial').val(formatValueToShow(data.nu_valortotalutilizado));
                    $('#valorMaterial').trigger("focusout");
                    $('#valorUtilizadoMaterial').trigger("focusout");
                }


            },
            complete: function () {
                loaded();
            }
        })
    },
    atualizaCargaHorariaUtilizada: function () {
        that = this;
        var parametros = {
            'id_matricula': $('#id_matricula option:selected').val()
        }
        $.ajax({
            type: 'post',
            url: '/cancelamento/atualiza-carga-utilizada',
            data: parametros,
            beforeSend: function () {
                loading();
            },
            success: function (data) {


                if (data.nu_cargahorariaofertada) {
                    $.pnotify({
                        title: 'Sucesso!',
                        text: 'Carga Horaria Utilizada atualizada com sucesso.',
                        type: 'success'
                    });
                    cargaHorariaCursada = data.nu_cargahorariaofertada;
                    $('#cargaHorariaCursada').val(data.nu_cargahorariaofertada);
                    $('#cargaHorariaCursada').trigger("focusout");
                } else {
                    $.pnotify({
                        title: 'Alerta!',
                        text: 'Problema ao efetuar o calculo de atualização da carga horária utilizada, favor inserir os valores manualmente.',
                        type: 'alert'
                    });
                }


            },
            complete: function () {
                loaded();
            }
        })
    },
    salvarDadosGerais: function () {
        thatSave = this;
        if (this.validaCampos()) {

            var checked_ids_motivo = [];

            //coleta os motivos com checkbox marcados
            _.each(collectionMotivos.models, function (model) {
                if ($('#id_motivo-' + model.get('id_motivo'))[0].checked) {
                    checked_ids_motivo.push(model.toJSON());
                }
            });

            //parametros enviados para salver os dados gerais de cancelamento
            var parametros = {
                'matricula': $('#id_matricula option:selected').val(),
                'evolucao': $('#id_evolucao option:selected').val(),
                'dt_solicitacao': $('#dt_solicitacao').val(),
                'nu_diasAcesso': $('#nu_acesso').val(),
                'motivos': checked_ids_motivo,
                'observacao': $('#st_observacao').val()
            }
            $.ajax({
                dataType: 'json',
                type: 'post',
                url: '/cancelamento/salvar-cancelamento',
                data: parametros,
                beforeSend: function () {
                    loading();
                },
                success: function (data) {

                    $.pnotify({
                        title: 'Sucesso!',
                        text: 'Dados gravados com sucesso.',
                        type: 'success'
                    });
                    $('#id_cancelamento').val(data['id_cancelamento']);
                    thatSave.carregaDados();

                },
                complete: function () {
                    loaded();
                }
            })
        }
        //ativarProximaAba();
    },

    /**
     * Efetua a validação de alguns dos campos de input
     * @returns {boolean|*}
     */
    validaCampos: function () {
        valid = true;

        /**
         * Verifica se algum elemento do select de matricula está selecionado
         */
        if ($('#id_matricula option:selected').val() == '') {
            $.pnotify({
                title: 'Atenção!',
                text: 'Selecione uma matricula.',
                type: 'warning'
            });
            $('#divMatricula').addClass('control-group error');
            valid = false;
        } else {
            $('#divMatricula').removeClass('control-group error');
        }

        /**
         * Verifica se algum elemento do select de evolução está selecionado
         */
        if ($('#id_evolucao option:selected').val() == '') {
            $.pnotify({
                title: 'Atenção!',
                text: 'Selecione uma evolução.',
                type: 'warning'
            });
            $('#divEvolucao').addClass('control-group error');
            valid = false;
        } else {
            $('#divEvolucao').removeClass('control-group error');
        }

        /**
         * Verifica se a data não está vazia ou se a data é menor ou igual a data atual
         */
        if ($('#dt_solicitacao').val() == "") {
            $.pnotify({
                title: 'Atenção!',
                text: 'Selecione a data de solicitação',
                type: 'warning'
            });
            $('#divDataSolicitacao').addClass('control-group error');
            valid = false;
        } else {
            $('#divDataSolicitacao').removeClass('control-group error');
        }

        /**
         * Verifica se o numero de dias de acesso está validado
         */
        if ($('#nu_acesso').val() == "") {
            $.pnotify({
                title: 'Atenção!',
                text: 'Selecione o número de dias de acesso.',
                type: 'warning'
            });
            $('#divNuAcesso').addClass('control-group error');
            valid = false;
        } else {
            $('#divNuAcesso').removeClass('control-group error');
        }

        if ($('#nu_acesso').val() < 0) {
            $.pnotify({
                title: 'Atenção!',
                text: 'O número de dias de acesso deve ser positivo.',
                type: 'warning'
            });
            $('#divNuAcesso').addClass('control-group error');
            valid = false;
        }


        return valid;
    },
    /**
     * Método carregaDados, efetua o carregamento dos dados para o formulário, efetuando
     * as contas financeiras inicias.
     */
    carregaDados: function () {
        collectionCancel = new CollectionCancelamento();
        collectionCancel.url = '/api/cancelamento/' + $('#id_matricula option:selected').val();
        collectionCancel.fetch({
            beforeSend: function () {
                loading();
            },
            success: function () {

                //marca todos os checkbox de motivo como false.
                _.each(collectionMotivos.models, function (model) {
                    $('#id_motivo-' + model.get('id_motivo')).prop('checked', false);
                });

                //marca os checkbox retornados na collection como true.
                _.each(collectionCancel.models[0].get('motivos'), function (model) {
                    $('#id_motivo-' + model.id_motivo.id_motivo).prop('checked', true);
                });

                //preenche a variavel id_cancelamento caso exista um cancelamento salvo nos dados gerais
                if (collectionMotivos.length && collectionMotivos.models['0'].get('id') != '') {
                    id_cancelamento = collectionCancel.models[0].get('id');
                    $('#id_cancelamento').val(id_cancelamento);
                }

                //preenche o select de carta gerada
                if (collectionCancel.models[0].get('bl_cartaGerada')) {
                    $('#selectGerarCarta').val(1);
                } else {
                    $('#selectGerarCarta').val(0);
                }

                //preenche os dados gerais com os dados da collection
                $('#id_evolucao').val(collectionCancel.models[0].get('id_evolucao') ? collectionCancel.models[0].get('id_evolucao') : 49);
                $('#dt_solicitacao').val(collectionCancel.models[0].get('dt_solicitacao'));
                $('#nu_acesso').val(collectionCancel.models[0].get('nu_diasAcesso'));
                $('#st_observacao').val(collectionCancel.models[0].get('st_observacao'));
                $('#evolucaoAtual').text(collectionCancel.models[0].get('st_evolucao'));

                //trata o campo valor material
                if (collectionCancel.models[0].get('valorMaterial')) {
                    $('#valorMaterial').val(formatValueToShow(collectionCancel.models[0].get('valorMaterial')));
                } else {
                    $('#valorMaterial').val("0,00");
                }

                //trata o campo de valor utilizado do material
                if (collectionCancel.models[0].get('valorUtilizadoMaterial')) {
                    $('#valorUtilizadoMaterial').val(formatValueToShow(collectionCancel.models[0].get('valorUtilizadoMaterial')));
                } else {
                    $('#valorUtilizadoMaterial').val("0,00");
                }

                //trata os outros campos financeiros caso não exista carga horaria cursada
                if (collectionCancel.models[0].get('valorCargaHorariaCursada') != null) {
                    $('#cargaHorariaCursada').val(collectionCancel.models[0].get('valorCargaHorariaCursada'));
                } else {
                    $('#cargaHorariaCursada').val(0);
                    $('#valorTotal').text("R$ 0,00");
                    $('#valorMulta').text("R$ 0,00");
                    $('#totalUtilizado').text("R$ 0,00");
                    $('#totalDevolucao').text("R$ 0,00");

                }

                //trata o campo de observação dos valores
                if (collectionCancel.models[0].get('st_observacaocalculo') != null) {
                    $('#st_observacaoValores').text(collectionCancel.models[0].get('st_observacaocalculo'));
                } else {
                    $('#st_observacaoValores').text("");
                }


                habilitaCamposCancelamento();

                //insere o id da matricula para buscar o cancelamento
                $("#requerimento-cancelamento").attr("href", "/cancelamento/requerimento-cancelamento/id_matricula/" + collectionCancel.models[0].get('id_matricula'));

                if (collectionCancel.at(0).get('bl_cancelamentofinalizado') && collectionCancel.models[0].get('bl_cartaGerada') == false) {
                    $('#btn-gerar-carta').addClass('hide');
                    $('#requerimento-cancelamento').removeClass('hide');
                } else if (collectionCancel.models[0].get('bl_cancelamentofinalizado')) {
                    $('#btn-gerar-carta').removeClass('hide');
                    $('#requerimento-cancelamento').addClass('hide');
                }

                //caso haja um id de cancelamento
                if ($('#id_cancelamento').val() != '') {
                    //caso haja carga horaria e valor negociado

                    //trata o campo de multa
                    if (!collectionCancel.models[0].get('bl_cancelamentofinalizado')) {
                        if (!collectionCancel.models[0].get('valormultaCarta')) {
                            $('#btn-finalizar-carta').attr('disabled', true);
                            $('#btn-gerar-carta').addClass('hide');
                        } else {
                            $('#btn-finalizar-carta').removeAttr('disabled');
                            $('#requerimento-cancelamento').addClass('hide');
                        }

                        if (VerificarPerfilPermissao(12, 656, false).length == 1) {
                            $('#percentualMultaDevolucao').removeAttr('disabled');
                            $('#percentualMultaCarta').removeAttr('disabled');
                            $('#valorMaterial').removeAttr('disabled');
                            $('#valorUtilizadoMaterial').removeAttr('disabled');
                        }

                        if (collectionCancel.models[0].get('multaPorcentagemCartaCancelamento')) {
                            $('#percentualMultaCarta').val(formatValueToShow(collectionCancel.models[0].get('multaPorcentagemCartaCancelamento')));
                            valorPercentualMultaCarta = collectionCancel.models[0].get('multaPorcentagemCartaCancelamento');
                        } else {
                            if (collectionCancel.models[0].get('multaPorcentagemCarta')) {
                                $('#percentualMultaCarta').val(formatValueToShow(collectionCancel.models[0].get('multaPorcentagemCarta')));
                                valorPercentualMultaCarta = collectionCancel.models[0].get('multaPorcentagemCarta');
                            } else {
                                $.pnotify({
                                    title: 'Aviso!',
                                    text: 'Esta entidade não tem multa de cancelamento cadastrada.',
                                    type: 'Erro'
                                });
                            }
                        }
                        if (collectionCancel.models[0].get('multaPorcentagemDevolucaoCancelamento')) {
                            $('#percentualMultaDevolucao').val(formatValueToShow(collectionCancel.models[0].get('multaPorcentagemDevolucaoCancelamento')));
                            valorPercentualMultaDevolucao = collectionCancel.models[0].get('multaPorcentagemDevolucaoCancelamento');
                        } else {
                            if (collectionCancel.models[0].get('multaPorcentagemCarta')) {
                                $('#percentualMultaDevolucao').val(formatValueToShow(collectionCancel.models[0].get('multaPorcentagemDevolucao')));
                                valorPercentualMultaDevolucao = collectionCancel.models[0].get('multaPorcentagemDevolucao');
                            } else {
                                $.pnotify({
                                    title: 'Aviso!',
                                    text: 'Esta entidade não tem multa de cancelamento cadastrada.',
                                    type: 'Erro'
                                });
                            }
                        }
                    } else {
                        if (VerificarPerfilPermissao(1, 656, false).length == 1) {
                            $('#percentualMultaDevolucao').attr('disabled', true);
                            $('#percentualMultaCarta').attr('disabled', true);
                        }

                        if (collectionCancel.models[0].get('multaPorcentagemCartaCancelamento')) {
                            $('#percentualMultaCarta').val(formatValueToShow(collectionCancel.models[0].get('multaPorcentagemCartaCancelamento')));
                            valorPercentualMultaCarta = collectionCancel.models[0].get('multaPorcentagemCartaCancelamento')
                        }
                        if (collectionCancel.models[0].get('multaPorcentagemDevolucaoCancelamento')) {
                            $('#percentualMultaDevolucao').val(formatValueToShow(collectionCancel.models[0].get('multaPorcentagemDevolucaoCancelamento')));
                            valorPercentualMultaDevolucao = collectionCancel.models[0].get('multaPorcentagemDevolucaoCancelamento')
                        }
                    }
                    if (collectionCancel.models[0].get('cargaHoraria') != null && collectionCancel.models[0].get('valorNegociado') != null) {

                        //efetua todos os calculos e preenchimentos dos campos de calculo


                        id_matricula = collectionCancel.models[0].get('id_matricula');
                        valorNegociacao = formatValueToReal(collectionCancel.models[0].get('valorNegociado'));
                        cargaHoraria = formatValueToReal(collectionCancel.models[0].get('cargaHoraria'));
                        valorHoraAula = valorNegociacao / cargaHoraria;
                        valorDiferenca = collectionCancel.models[0].get('valorDesconto');
                        valorMultaCarta = (valorNegociacao / 100) * valorPercentualMultaCarta;
                        valorMultaDevolucao = (valorNegociacao / 100) * valorPercentualMultaDevolucao;
                        valorTotalCarta = valorNegociacao - valorTotal - valorMultaCarta - valorUtilizadoMaterial;
                        valorTotalDevolucao = valorNegociacao - valorTotal - valorMultaDevolucao - valorUtilizadoMaterial;
                        valorPago = formatValueToReal(collectionCancel.models[0].get('valorPago'));
                        valorPagoCarta = formatValueToReal(collectionCancel.models[0].get('valorPagoCarta'));


                        $('#id_matriculaValores').text(id_matricula);
                        //$('#valorBruto').text('R$ ' + formatValueToShow(valorBruto.toFixed(2)));
                        $('#valorNegociado').text('R$ ' + formatValueToShow(valorNegociacao.toFixed(2)));
                        $('#cargaHoraria').text(cargaHoraria + ' horas');
                        $('#valorHoraAula').text('R$ ' + formatValueToShow(valorHoraAula.toFixed(2)));
                        $('#valorDiferenca').text('R$ ' + formatValueToShow(valorDiferenca.toFixed(2)));
                        $('#valorTotal').text('R$ ' + formatValueToShow(valorDiferenca.toFixed(2)));
                        $('#totalDevolucao').text('R$ ' + formatValueToShow(valorTotalDevolucao.toFixed(2)));
                        $('#totalCarta').text('R$ ' + formatValueToShow(valorTotalCarta.toFixed(2)));
                        $('#valorPago').text('R$ ' + formatValueToShow(valorPago.toFixed(2)));
                        $('#valorPagoCarta').text('R$ ' + formatValueToShow(valorPagoCarta.toFixed(2)));

                        thatLayout.calculaMateriais();
                        thatLayout.calculaRecisao();

                        //Se o produto não tiver um valor liquido, ou seja, com um desconto de 100%, desabilita
                        //os campos de edição dos valores de calculo
                        if (valorNegociacao == 0) {
                            $('#valorMaterial').prop('disabled', 'true');
                            $('#valorUtilizadoMaterial').prop('disabled', 'true');
                            $('#cargaHorariaCursada').prop('disabled', 'true');
                            $('#selectGerarCarta').prop('disabled', 'true');
                            $('#btn-gerar-carta').removeClass('hide');
                        }

                        //verifica se o cancelamento ja foi finalizado, se sim desabilita todos os campos de input
                        if (collectionCancel.models[0].get('bl_cancelamentofinalizado')) {
                            desabilitaCamposCancelamento();
                            //habilita somente o botão de gerar carta
                            if (collectionCancel.models[0].get('bl_cartaGerada') == true) {
                                $('#btn-gerar-carta').removeClass('hide');
                                $('#requerimento-cancelamento').addClass('hide');
                            }
                        } else {
                            $('#btn-gerar-carta').addClass('hide');
                        }

                        //valida os campos novamente.
                        thatLayout.validaCampos();

                        //caso haja um cancelamento, a aba de Calculo é habilitada
                        habilitarProximaAba();


                    } else {
                        //caso não haja carga horária cadastrada no produto, não é abilitada a aba de Calculo
                        $.pnotify({
                            title: 'Aviso!',
                            text: 'Este produto não possui carga horária cadastrada ou valor negociado para calculo de cancelamento.',
                            type: 'Erro'
                        });
                        desabilitarAbas();
                    }
                } else {
                    //se não houver um cancelamento apenas desabilita a aba de calculo.
                    desabilitarAbas();
                }
                //Após o carregamento habilita a aba de Log
                $('#abaLog').removeClass('disabled');
            },
            complete: function () {
                loaded();
            }
        });

        renderizaLog(1);

    },

    /**
     * Efetua a formatação de campos de input
     */
    formataCampos: function () {
        $('#valorMaterial').maskMoney({precision: 2, decimal: ",", thousands: "."});
        $('#valorUtilizadoMaterial').maskMoney({precision: 2, decimal: ",", thousands: "."});
        $('#percentualMultaDevolucao').maskMoney({precision: 2, decimal: ",", allowNegative: false});
        $('#percentualMultaCarta').maskMoney({precision: 2, decimal: ",", allowNegative: false});
    },

    /**
     * Calcula os valores dos materiais, atualizando os valores do formulário
     * @returns {boolean}
     */
    calculaMateriais: function () {
        //preenche a variavel de valor do material e do material utilizado
        valorMaterial = ($('#valorMaterial').val() != '' && $('#valorMaterial').val() != null) ? formatValueToReal($('#valorMaterial').val()) : 0;
        valorUtilizadoMaterial = ($('#valorUtilizadoMaterial').val() != '' && $('#valorUtilizadoMaterial').val() != null) ? formatValueToReal($('#valorUtilizadoMaterial').val()) : 0;

        //preenche o valor desembolsado no formulário com o valor inicial da negociação
        $('#valorDemenbolsado').text('R$ ' + valorNegociacao.toFixed(2));

        //verifica se os valores de materias estão corretos
        if (valorUtilizadoMaterial > valorMaterial) {
            $('#divCalculoMateriais').addClass('control-group error');
            $.pnotify({
                title: 'Aviso!',
                text: 'O valor do material utilizado deve ser menor ou igual ao valor do material.',
                type: 'warning'
            });
            return false;
        } else if (valorMaterial > valorNegociacao) {
            $('#divCalculoMateriais').addClass('control-group error');
            $.pnotify({
                title: 'Aviso!',
                text: 'O valor do material deve ser menor que o valor da negociação.',
                type: 'warning'
            });
            return false;
        } else {
            $('#divCalculoMateriais').removeClass('control-group error');
            if (cargaHoraria != null) {
                valorTotalCarta = valorNegociacao - valorTotalUtilizado - valorMultaCarta - valorUtilizadoMaterial;
                valorTotalDevolucao = valorNegociacao - valorTotalUtilizado - valorMultaDevolucao - valorUtilizadoMaterial;
                $('#totalDevolucao').text('R$ ' + formatValueToShow(valorTotalDevolucao.toFixed(2)));
            }
            //apartir dos valores atualizados calcula a recisão do contrato
            this.calculaRecisao();
        }
    },
    /**
     * Efetua o calculo de recisão
     * @returns {boolean}
     */
    calculaRecisao: function () {
        //se houver valor de hora aula
        if (valorHoraAula != null) {
            cargaHorariaCursada = formatValueToReal($('#cargaHorariaCursada').val());
            if (cargaHorariaCursada > cargaHoraria || cargaHorariaCursada < 0) {
                $('#divCargaHoraria').addClass('control-group error');
                $.pnotify({
                    title: 'Aviso!',
                    text: 'A carga horária cursada deve estar entre 0 e ' + cargaHoraria,
                    type: 'warning'
                });
                return false;
            } else {
                $('#divCargaHoraria').removeClass('control-group error');
            }

            valorTotal = cargaHorariaCursada * valorHoraAula;
            $('#valorTotal').text('R$ ' + formatValueToShow(valorTotal.toFixed(2)));

            //se houver uma multa preenchida para carta
            if (valorPercentualMultaCarta) {

                //verifica se o valor da multa é positivo
                if (valorPercentualMultaCarta >= 0 && formatValueToReal(collectionCancel.models[0].get('multaPorcentagemCarta')) >= formatValueToReal($('#percentualMultaCarta').val())) {
                    //calcula os valores com base no percentual de multa
                    $('#divMultaCarta').removeClass('control-group error');
                    valorMultaCarta = (valorNegociacao / 100) * formatValueToReal($('#percentualMultaCarta').val());
                    $('#valorMultaCarta').text('R$ ' + formatValueToShow(valorMultaCarta.toFixed(2)));
                    valorTotalUtilizado = valorTotalCarta + valorMultaCarta;
                    $('#totalUtilizado').text('R$ ' + formatValueToShow(valorTotalUtilizado.toFixed(2)));
                    valorTotalCarta = valorNegociacao - valorTotal - valorMultaCarta - valorUtilizadoMaterial;
                    if (valorTotalCarta >= 0) {
                        $('#totalCarta').removeClass("label label-important");
                        $('#totalCarta').addClass("label label-success");
                    } else {
                        $('#totalCarta').removeClass("label label-success");
                        $('#totalCarta').addClass("label label-important");
                    }
                    $('#totalCarta').empty();
                    $('#totalCarta').append('R$' + formatValueToShow(valorTotalCarta.toFixed(2)));


                } else {
                    $('#divMultaCarta').addClass('control-group error');
                    $.pnotify({
                        title: 'Aviso!',
                        text: 'O percentual da multa deve estar entre 0 e ' + collectionCancel.models[0].get('multaPorcentagemCarta') + '%.',
                        type: 'warning'
                    });
                    return false;
                }


            } else {
                //se não houver um valor de multa, atualiza os outros campos.
                $('#valorMultaCarta').text('R$ 0.00');
            }
            if (valorPercentualMultaDevolucao) {

                //verifica se o valor da multa é positivo
                if (valorPercentualMultaDevolucao >= 0 && formatValueToReal(collectionCancel.models[0].get('multaPorcentagemDevolucao')) >= formatValueToReal($('#percentualMultaDevolucao').val())) {
                    //calcula os valores com base no percentual de multa
                    $('#divMultaDevolucao').removeClass('control-group error');
                    valorMultaDevolucao = (valorNegociacao / 100) * formatValueToReal($('#percentualMultaDevolucao').val());
                    $('#valorMultaDevolucao').text('R$ ' + formatValueToShow(valorMultaDevolucao.toFixed(2)));
                    valorTotalUtilizado = valorTotalDevolucao + valorMultaDevolucao;
                    $('#totalUtilizado').text('R$ ' + formatValueToShow(valorTotalUtilizado.toFixed(2)));
                    valorTotalDevolucao = valorNegociacao - valorTotal - valorMultaDevolucao - valorUtilizadoMaterial;
                    if (valorTotalDevolucao >= 0) {
                        $('#totalDevolucao').removeClass("label label-important");
                        $('#totalDevolucao').addClass("label label-success");
                    } else {
                        $('#totalDevolucao').removeClass("label label-success");
                        $('#totalDevolucao').addClass("label label-important");
                    }
                    $('#totalDevolucao').empty();
                    $('#totalDevolucao').append('R$' + formatValueToShow(valorTotalDevolucao.toFixed(2)));


                } else {
                    $('#divMultaDevolucao').addClass('control-group error');
                    $.pnotify({
                        title: 'Aviso!',
                        text: 'O percentual da multa deve estar entre 0 e ' + collectionCancel.models[0].get('multaPorcentagemDevolucao') + '%.',
                        type: 'warning'
                    });
                    return false;
                }


            } else {
                //se não houver um valor de multa, atualiza os outros campos.
                $('#valorMultaDevolucao').text('R$ 0.00');
                $('#valorMultaCarta').text('R$ 0.00');
            }

        } else {
            //caso o produto não tenha carga horaria, emite o erro.
            $.pnotify({
                title: 'Aviso!',
                text: 'Este produto não tem carga horária!',
                type: 'error'
            });
        }
    },
    validaRescisao: function () {
        var flag = true;

        if (valorUtilizadoMaterial > valorMaterial) {
            $('#divCalculoMateriais').addClass('control-group error');
            $.pnotify({
                title: 'Aviso!',
                text: 'O valor do material utilizado deve ser menor ou igual ao valor do material.',
                type: 'warning'
            });
            flag = false;
        }
        ;

        if (valorMaterial > valorNegociacao) {
            $('#divCalculoMateriais').addClass('control-group error');
            $.pnotify({
                title: 'Aviso!',
                text: 'O valor do material deve ser menor que o valor da negociação.',
                type: 'warning'
            });
            flag = false;
        }
        ;

        if (cargaHorariaCursada > cargaHoraria || cargaHorariaCursada < 0) {
            $('#divCargaHoraria').addClass('control-group error');
            $.pnotify({
                title: 'Aviso!',
                text: 'A carga horária cursada deve estar entre 0 e ' + cargaHoraria,
                type: 'warning'
            });
            flag = false;
        }
        ;

        if ($('#cargaHorariaCursada').val() == "") {
            $('#divCargaHoraria').addClass('control-group error');
            $.pnotify({
                title: 'Aviso!',
                text: 'A carga horária cursada deve estar preenchida!',
                type: 'warning'
            });
            flag = false;
        }


        return flag;
    },
    salvarCalculos: function () {

        if (this.validaRescisao()) {
            that = this;
            var checked_ids_motivo = [];

            //coleta os motivos com checkbox marcados
            _.each(collectionMotivos.models, function (model) {
                if ($('#id_motivo-' + model.get('id_motivo'))[0].checked) {
                    checked_ids_motivo.push(model.toJSON());
                }
            });

            parametros = {
                'id_cancelamento': collectionCancel.models[0].get('id'),
                //'nu_valorBruto': valorBruto,
                'nu_valorNegociado': valorNegociacao,
                'nu_valorDiferenca': valorDiferenca,
                'cargaHoraria': cargaHoraria,
                'valorHoraAula': valorHoraAula,
                'valorMaterial': valorMaterial,
                'valorUtilizadoMaterial': valorUtilizadoMaterial,
                'cargaHorariaCursada': cargaHorariaCursada,
                'valorTotal': valorTotal,
                'valorMultaCarta': valorMultaCarta,
                'valorMultaPorcentagemCarta': formatValueToReal($('#percentualMultaCarta').val()),
                'valorMultaDevolucao': valorMultaDevolucao,
                'valorMultaPorcentagemDevolucao': formatValueToReal($('#percentualMultaDevolucao').val()),
                'totalUtilizado': valorTotalUtilizado,
                'valorDevolucao': valorTotalDevolucao,
                'valorCarta': valorTotalCarta,
                'st_observacao': $('#st_observacaoValores').val(),
                'selectCarta': $('#selectGerarCarta').val(),
                'motivos': checked_ids_motivo
            };


            $.ajax({
                dataType: 'json',
                type: 'post',
                url: '/cancelamento/salvar-calculos',
                data: parametros,
                beforeSend: function () {
                    loading();
                },
                success: function (data) {
                    $.pnotify({
                        title: data['title'],
                        text: data['mensagem'],
                        type: data['tipo']
                    });
                    that.carregaDados();
                    $('#btn-finalizar-carta').removeAttr('disabled');
                    $('#btn-gerar-carta').addClass('hide');
                    $('#requerimento-cancelamento').addClass('hide');
                },
                complete: function () {
                    loaded();
                }
            })
        }


    },
    finalizarCancelamento: function () {
        var that = this;
        if (this.validaRescisao()) {
            if (valorTotalCarta < 0 && $('#selectGerarCarta option:selected').val() == "1") {
                $.pnotify({
                    title: 'Aviso',
                    text: 'Não é possível gerar carta de crédito com valor negativo!',
                    type: 'warning'
                });
            } else {
                bootbox.confirm("Tem certeza que deseja finalizar o processo de cancelamento?", function (result) {
                    if (result) {
                        var checked_ids_motivo = [];

                        //coleta os motivos com checkbox marcados
                        _.each(collectionMotivos.models, function (model) {
                            if ($('#id_motivo-' + model.get('id_motivo'))[0].checked) {
                                checked_ids_motivo.push(model.toJSON());
                            }
                        });

                        parametros = {
                            'nu_valordevolucao': valorTotalDevolucao,
                            'nu_valorcarta': valorTotalCarta,
                            'id_cancelamento': id_cancelamento,
                            'id_situacao': 142,
                            'id_usuarioCadastro': componenteAutoComplete.getFixModel().id_usuario,
                            'gerarCarta': $('#selectGerarCarta').val(),
                            'motivos': checked_ids_motivo
                        };

                        $.ajax({
                            dataType: 'json',
                            type: 'post',
                            url: '/cancelamento/finalizar-carta',
                            data: parametros,
                            beforeSend: function () {
                                loading();
                            },
                            success: function (data) {
                                $.pnotify({
                                    title: 'Sucesso',
                                    text: 'Cancelamento finalizado com sucesso!',
                                    type: 'success'
                                });
                                $('#id_evolucao').val(51);
                                $('evolucaoAtual').text("Finalizado.")
                                desabilitaCamposCancelamento();
                                renderizaLog(1);
                                if ($('#selectGerarCarta').val() == 0) {
                                    $('#btn-gerar-carta').addClass('hide');
                                    $('#requerimento-cancelamento').removeClass('hide');
                                    console.log(data);
                                    window.open('/cancelamento/requerimento-cancelamento/id_matricula/' + data.id_matricula);
                                } else {
                                    that.imprimirCarta();
                                }
                            },
                            complete: function () {
                                loaded();
                            }
                        });
                    }
                });
            }
        }
    },
    filtrarLog: function (e) {

        if ($('#dt_inicio').val() > $('#dt_fim').val()) {
            $.pnotify({
                title: 'Aviso',
                text: 'Data de inicio deve ser menor ou igual a data de fim.',
                type: 'warning'
            });
            $('#divDataFiltro').addClass('control-group error');
            return false;
        } else {
            $('#divDataFiltro').removeClass('control-group error');
            renderizaLog(2);
        }
    },
    somenteNumeros: function (e) {
        //teclas adicionais permitidas (tab,delete,backspace,setas direita e esquerda)
        keyCodesPermitidos = new Array(8, 9, 37, 39, 46);
        //numeros e 0 a 9 do teclado alfanumerico
        for (x = 48; x <= 57; x++) {
            keyCodesPermitidos.push(x);
        }

        //numeros e 0 a 9 do teclado numerico
        for (x = 96; x <= 105; x++) {
            keyCodesPermitidos.push(x);
        }

        //Pega a tecla digitada
        keyCode = e.which;
        //Verifica se a tecla digitada é permitida
        if ($.inArray(keyCode, keyCodesPermitidos) != -1) {
            return true;
        }
        return false;
    },
    imprimirCarta: function () {

        parametros = {
            'id_mensagempadrao': 30
        }

        $.ajax({
            dataType: 'json',
            type: 'post',
            url: '/cancelamento/retorna-texto-carta-credito',
            data: parametros,
            beforeSend: function () {
                loading();
            },
            success: function (data) {
                if (data.type == 'success') {
                    window.open('/textos/geracao/?id_textosistema=' + data['mensagem'].id_textosistema + '&arrParams[id_matricula]=' + id_matricula + '&type=html&filename=Declaracao');
                } else {
                    $.pnotify({
                        title: 'Aviso',
                        text: 'Não existe texto de sistema cadastrado!',
                        type: 'warning'
                    });
                }
            },
            error: function () {

            },
            complete: function () {
                loaded();

            }

        })
    }

});

function habilitaCamposCancelamento() {
    $('#nu_acesso').removeAttr('disabled');
    $('#id_evolucao').removeAttr('disabled');
    $('#dt_solicitacao').removeAttr('disabled');
    //$('#valorMaterial').removeAttr('disabled');
    //$('#valorUtilizadoMaterial').removeAttr('disabled');
    $('#cargaHorariaCursada').removeAttr('disabled');
    $('#st_observacaoValores').removeAttr('disabled');
    $('#multa').removeAttr('disabled');
    $('#selectGerarCarta').removeAttr('disabled');
    $('#btn-salvar-valores').removeAttr('disabled');
    $('#btn-finalizar-carta').removeAttr('disabled');
    $('#btn-atualizar-materiais').removeAttr('disabled');
    $('#btn-atualizar-carga-cursada').removeAttr('disabled');


    _.each(collectionMotivos.models, function (model) {
        $('#id_motivo-' + model.get('id_motivo')).removeAttr('disabled');
    });
};

function desabilitaCamposCancelamento() {
    $('#dt_solicitacao').attr('disabled', true);
    $('#id_evolucao').attr('disabled', true);
    $('#nu_acesso').attr('disabled', true);
    $('#valorMaterial').attr('disabled', true);
    $('#valorUtilizadoMaterial').attr('disabled', true);
    $('#cargaHorariaCursada').attr('disabled', true);
    $('#st_observacaoValores').attr('disabled', true);
    $('#multa').attr('disabled', true);
    $('#selectGerarCarta').attr('disabled', true);
    $('#btn-salvar-valores').attr('disabled', true);
    $('#btn-finalizar-carta').attr('disabled', true);
    $('#percentualMultaCarta').attr('disabled', true);
    $('#percentualMultaDevolucao').attr('disabled', true);
    $('#btn-atualizar-materiais').attr('disabled', true);
    $('#btn-atualizar-carga-cursada').attr('disabled', true);

    _.each(collectionMotivos.models, function (model) {
        $('#id_motivo-' + model.get('id_motivo')).attr('disabled', true);
    });
};

function abreDadosCancelamento() {
    telaCancelamento.matricula.show(new ViewMatricula());
    telaCancelamento.evolucao.show(new ViewEvolucao());
    telaCancelamento.datas.show(new ViewDatas());
    telaCancelamento.motivo.show(new ViewMotivo());
    telaCancelamento.observacao.show(new ViewObservacao());
    telaCancelamento.salvardados.show(new ViewBotaoSalvar());
    telaCancelamento.salvarValores.show(new ViewBotaoSalvarValores());
    telaCancelamento.observacaoValores.show(new ViewObservacaoValores());
    telaCancelamento.valorInicial.show(new ViewCalculoValorInicial());
    telaCancelamento.valorRecisao.show(new ViewCalculoValorRecisao());
    telaCancelamento.filtroLog.show(new ViewFiltroLog());
    desabilitarAbas();
    $('#content-dados').show();
    $('#content-log').show();
};

function fechaDadosCancelamento() {
    telaCancelamento.matricula.destroy();
    telaCancelamento.evolucao.destroy();
    telaCancelamento.datas.destroy();
    telaCancelamento.motivo.destroy();
    telaCancelamento.observacao.destroy();
    telaCancelamento.salvardados.destroy();
    desabilitarAbas();
    $('#content-dados').hide('fast');
    $('#content-log').hide('fast');
    $('#abaLog').addClass('disabled');
};

function habilitarProximaAba() {
    var abaAtiva = $('#tabDadosCancelamento > li.active');
    abaAtiva.next('li').removeClass('disabled');
};

function desabilitarAbas() {
    var primeiraAba = $('#tabDadosCancelamento > li.active');
    primeiraAba.next('li').addClass('disabled');
};

function renderizaLog(tipo) {

    var HtmlMotivoCell = Backgrid.Cell.extend({
        className: 'span7',
        render: function () {
            var elTemp = _.template('<div class="content">' + this.model.get('st_motivocancelamento') + '</div>');
            this.$el.html(elTemp);
            return this;
        }
    });

    var columns = [{
        name: "id_matricula",
        label: "Matrícula",
        editable: false,
        cell: Backgrid.IntegerCell.extend({
            orderSeparator: ''
        })
    }, {
        name: "st_situacao",
        label: "Situação",
        cell: "string",
        editable: false
    }, {
        name: "st_evolucao",
        label: "Evolução",
        cell: "string",
        editable: false
    }, {
        name: "st_evolucaocancelamento",
        label: "Evolucao cancelamento",
        cell: "string",
        editable: false
    }, {
        name: "dt_registro",
        label: "Data de registro",
        cell: "string",
        editable: false
    }, {
        name: "st_motivocancelamento",
        label: "Motivo",
        cell: HtmlMotivoCell,
        editable: false
    }, {
        name: "nu_materialutilizado",
        label: "Valor material utilizado",
        cell: "string",
        editable: false
    }, {
        name: "nu_cargahorariacursada",
        label: "Carga horaria cursada",
        cell: "string",
        editable: false
    }, {
        name: "nu_percentualmulta",
        label: "Valor percentual de multa",
        cell: "string",
        editable: false
    }, {
        name: "nu_valordevolucao",
        label: "Valor a ser devolvido",
        cell: "string",
        editable: false
    }, {
        name: "bl_cartacredito",
        label: "Carta gerada",
        cell: "string",
        editable: false
    }, {
        name: "st_perfil",
        label: "Perfil",
        cell: "string",
        editable: false
    }];

    if (tipo == 1) {
        var PageableLog = Backbone.PageableCollection.extend({
            model: LogCancelamento,
            url: '/cancelamento/retorna-log/id_matricula/' + $('#id_matricula option:selected').val(),
            state: {
                pageSize: 10
            },
            mode: "client"
        });
    } else if (tipo == 2) {
        var PageableLog = Backbone.PageableCollection.extend({
            model: LogCancelamento,
            url: '/cancelamento/retorna-filtro-log/',
            state: {
                pageSize: 10
            },
            mode: "client"
        });
    }


    var pageableLog = new PageableLog();


    var pageableGridLog = new Backgrid.Grid({
        className: 'backgrid table table-bordered',
        columns: columns,
        collection: pageableLog,
        emptyText: "Nenhum registro encontrado"


    });

    var $elemento = $('#tabela-log-wrapper');
    $elemento.empty();
    $elemento.append(pageableGridLog.render().$el);

    var paginator = new Backgrid.Extension.Paginator({
        collection: pageableLog
    });

    $elemento.append(paginator.render().el);

    if (tipo == 1) {
        pageableLog.fetch({reset: true});
    } else if (tipo == 2) {
        datas = {
            'dt_inicio': $('#dt_inicio').val(),
            'dt_fim': $('#dt_fim').val(),
            'id_matricula': $('#id_matricula').val()
        }
        pageableLog.fetch({data: datas, reset: true});
    }
}


/*********************************************
 **************   ITEMVIEW  ******************
 *********************************************/

var ViewMatricula = Marionette.ItemView.extend({
    template: '#dados-cancelamento-matricula',
    tagName: 'div',
    onRender: function () {
        this.populaSelectMatricula();
        return this;
    },
    populaSelectMatricula: function () {
        var Collection = Backbone.Collection.extend({
            model: VwMatricula
        });
        var view;
        var that = this;
        var collectionMatriculas = new Collection();
        collectionMatriculas.url = '/cancelamento/retorna-matriculas/id/' + pessoaDadosCancelamento['id'];
        collectionMatriculas.fetch({
            beforeSend: function () {
                loading()
            },
            success: function () {
                view = new SelectView({
                    el: that.$el.find("select[name='id_matricula']"),      // Elemento da DOM
                    collection: collectionMatriculas,      // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_labelselect', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_matricula', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: null       // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();
            },

            complete: function () {
                loaded()
            }
        });
    }

});

var ViewEvolucao = Marionette.ItemView.extend({
    template: '#dados-cancelamento-evolucao',
    tagName: 'div',
    onRender: function () {
        this.populaSelectEvolucao();
        return this;
    },
    populaSelectEvolucao: function () {
        var Collection = Backbone.Collection.extend({
            model: Evolucao
        });
        var that = this;
        var collectionEvolucao = new Collection();
        collectionEvolucao.url = '/api/evolucao/st_tabela/tb_motivocancelamento';
        collectionEvolucao.fetch({
            success: function () {
                var view = new SelectView({
                    el: that.$el.find("select[name='id_evolucao']"),      // Elemento da DOM
                    collection: collectionEvolucao,      // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_evolucao', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_evolucao', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: 49       // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();
            }
        });


    }
});

var ViewDatas = Marionette.ItemView.extend({
    template: '#dados-cancelamento-datas',
    tagName: 'div',
    onShow: function () {
        $('#dt_solicitacao').datepicker({
            language: 'pt-BR',
            format: 'dd/mm/yyyy',
            endDate: new Date()
        });
    }
});

var ViewObservacao = Marionette.ItemView.extend({
    template: '#dados-cancelamento-observacao',
    tagName: 'div'
});

var ViewObservacaoValores = Marionette.ItemView.extend({
    template: '#calculo-observacao',
    tagName: 'div'
});

var ViewBotaoSalvar = Marionette.ItemView.extend({
    template: '#dados-cancelamento-botao',
    tagName: 'div'
});

var ViewBotaoSalvarValores = Marionette.ItemView.extend({
    template: '#valores-botao',
    tagName: 'div'
});

var ViewCalculoValorInicial = Marionette.ItemView.extend({
    template: '#calculo-valor-inicial',
    tagName: 'div'
})

var ViewCalculoValorRecisao = Marionette.ItemView.extend({
    template: '#calculo-valor-recisao',
    tagName: 'div'
});

var ViewItemMotivo = Marionette.ItemView.extend({
    template: _.template('<input type="checkbox" name="id_motivo" id="id_motivo-<%=id_motivo%>" value="<%=id_motivo%>"> <%=st_motivo%>'),
    tagName: 'label',
    className: 'checkbox'
});

var ViewFiltroLog = Marionette.ItemView.extend({
    template: '#filtro-log',
    tagName: 'div'
});

/*********************************************
 ************** COMPOSITEVIEW ****************
 *********************************************/

var ViewMotivo = Marionette.CompositeView.extend({
    initialize: function () {
        var that = this;
        collectionMotivos = new CollectionMotivo();
        collectionMotivos.fetch({

            success: function () {
                that.collection = collectionMotivos;
                that.render();
            }

        });
    },
    template: _.template("<div id='container-itens'></div>"),
    childView: ViewItemMotivo,
    childViewContainer: '#container-itens',
    tagName: 'div',
    className: 'row-fluid'
});

/*********************************************
 ************** INICIALIZAÇÃO ****************
 *********************************************/

var telaCancelamento = new LayoutCancelamento();
G2S.show(telaCancelamento);


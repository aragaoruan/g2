/**
 * IMPRESSÃO DE CARTEIRINHA
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */

/*********************************************
 ************** VARIAVEIS ********************
 *********************************************/
var pageableGridLog;

/*********************************************
 **************   ITEMVIEW  ******************
 *********************************************/
var ViewDados = Marionette.ItemView.extend({
    template: '#template-dados',
    tagName: 'div',
    ui: {
        st_urlavatar: '#st_urlavatar',
        dt_matricula_inicial: '#dt_matricula_inicial',
        dt_matricula_fim: '#dt_matricula_fim',
        btnPrint: '#btn-imprimir',
        btnGeralXls: '#btn-gerar-xls'

    },
    events: {
        "click #btn-pesquisar": "pesquisaAlunos",
        "click #btn-salvar": "salvarDados",
        'click #btn-imprimir': 'imprimirCarteirinhas',
        "click #btn-gerar-xls": 'gerarXls'
    },
    onShow: function () {
    },
    pesquisaAlunos: function () {
        loading();
        var that = this;
        if (!that.ui.dt_matricula_inicial.val() && !that.ui.dt_matricula_fim.val()) {
            $.pnotify({
                title: 'Campo Obrigatório!',
                text: 'Escolha o período de datas de inicio e fim de matricula!',
                type: 'error'
            });
        }

        loading();

        var st_avatar = Backgrid.Cell.extend({
            render: function () {
                var result = this.model.get('st_urlavatar') ? 'Sim' : 'Não';
                this.$el.html(result);
                return this;
            }
        });

        var cellIndentificacao = Backgrid.Cell.extend({
            render: function () {
                if (this.model.get('st_urlavatar')) {
                    this.$el.html(_.template('<input type="text" value="<%= st_identificacao %>" class="identificacao" name="identificacao" maxlength="12" placeholder="Identificação" >', this.model.toJSON()));
                    return this;
                } else {
                    this.$el.html(_.template(''));
                    return this;
                }
            }
        });

        var columns = [
            {
                // enable the select-all extension
                name: "bl_imprimir",
                cell: 'select-row',
                editable: false,
                headerCell: "select-all"
            }, {
                name: "st_nomecompleto",
                label: "Nome do Aluno",
                editable: false,
                cell: 'string'
            }, {
                name: "st_identificacao",
                label: "Identificação",
                editable: false,
                cell: cellIndentificacao
            }, {
                name: "st_codigo",
                label: "Código da Turma",
                editable: false,
                cell: 'string'
            }, {
                name: "st_urlavatar",
                label: "Possui Foto",
                editable: false,
                cell: st_avatar
            }];

        var PageableLog = Backbone.PageableCollection.extend({
            model: Matricula,
            url: '/turma/get-alunos-turma-carteirinha-combo/',
            state: {
                pageSize: 30
            },
            mode: "client"
        });

        var data = {
            dt_matricula_inicial: that.ui.dt_matricula_inicial.val(),
            dt_matricula_fim: that.ui.dt_matricula_fim.val(),
            st_urlavatar: that.ui.st_urlavatar.val()
        };

        var RowGrid = Backgrid.Row.extend({
            initialize: function (options) {
                RowGrid.__super__.initialize.apply(this, arguments);
                this.$el.css('cursor', 'pointer');
            },
            onKeyUp: function (e) {
                var inputValor = $(e.currentTarget);
                this.model.set('st_identificacao', inputValor.val());
                inputValor.val(this.model.get('st_identificacao'));
            },
            somenteNumeros: function (e) {
                //teclas adicionais permitidas (tab,delete,backspace,setas direita e esquerda)
                keyCodesPermitidos = new Array(8, 9, 37, 39, 46);
                //numeros e 0 a 9 do teclado alfanumerico
                for (x = 48; x <= 57; x++) {
                    keyCodesPermitidos.push(x);
                }

                //numeros e 0 a 9 do teclado numerico
                for (x = 96; x <= 105; x++) {
                    keyCodesPermitidos.push(x);
                }
                //Pega a tecla digitada
                keyCode = e.which;
                //Verifica se a tecla digitada é permitida
                if ($.inArray(keyCode, keyCodesPermitidos) != -1) {
                    return true;
                }
                return false;
            },
            events: {
                'change .identificacao': 'onKeyUp',
                'keydown .identificacao': 'somenteNumeros'
            }
        });

        var pageableLog = new PageableLog();

        pageableGridLog = new Backgrid.Grid({
            className: 'backgrid table table-bordered',
            columns: columns,
            collection: pageableLog,
            row: RowGrid,
            emptyText: "Nenhum registro encontrado"
        });

        var paginator = new Backgrid.Extension.Paginator({
            collection: pageableLog
        });

        pageableLog.fetch({
            data: data,
            reset: true,
            beforeSend: function () {
                loading();
            },
            success: function (response) {
                that.$el.find("#regiao-grade-data").empty();
                that.$el.find("#regiao-grade-data").append(pageableGridLog.render().$el);
                that.$el.find("#regiao-grade-data").append(paginator.render().$el);
                /**
                 * Marca todos os checkbox
                 * De acordo com backgrid
                 */
                pageableGridLog.collection.each(function (model, i) {
                    model.trigger("backgrid:select", model, true);
                });
                $('#container-DataGrid').show();

            },
            error: function (collection, response) {
                $.pnotify({
                    type: 'warning',
                    title: 'Atenção!',
                    text: response.responseText
                });
            },
            complete: function () {
                loaded();
            }

        });
    },
    salvarDados: function () {
        that = this;

        var selectedModels = pageableGridLog.getSelectedModels();
        var collectionEnviar = new Backbone.Collection;
        collectionEnviar.add(selectedModels);

        var validacao = true;
        if (collectionEnviar.length) {
            collectionEnviar.each(function (model, i) {
                if (!model.get('st_identificacao')) {
                    validacao = false;
                }
            });
        } else {
            $.pnotify({
                type: 'error',
                title: 'Campo Obrigatório',
                text: 'Selecione os alunos.'
            });
            return false;
        }

        if (!validacao) {
            $.pnotify({
                type: 'error',
                title: 'Campo Obrigatório',
                text: 'Existe alunos com o campo IDENTIFICAÇÃO em branco, favor verifique'
            });
            return false;
        }

        if (validacao) {
            $.ajax({
                dataType: 'json',
                type: 'post',
                url: '/pessoa/salvar-array-identificacao',
                data: {
                    usuarios: collectionEnviar.toJSON()
                },
                beforeSend: function () {
                    loading();
                },
                success: function (data) {
                    $.pnotify({
                        type: data.type,
                        title: data.title,
                        text: data.text
                    });
                    if (data.type) {
                        that.pesquisaAlunos();
                    }
                },
                complete: function () {
                    loaded();
                },
                error: function (data) {
                    console.log(data);
                    $.pnotify({
                        type: 'error',
                        title: 'Erro!',
                        text: 'Erro ao tentar salvar códigos dos usuários.'
                    });
                }
            });
        }
    },
    imprimirCarteirinhas: function () {
        var selectedModels = pageableGridLog.getSelectedModels();
        var collectionEnviar = new Backbone.Collection;
        collectionEnviar.add(selectedModels);

        var validacao = true;
        var arrPrint = [];
        if (collectionEnviar.length) {
            collectionEnviar.each(function (model, i) {
                arrPrint[i] = {
                    'id_usuario': model.get('id_usuario'),
                    'id_turma': model.get('id_turma')
                };
            });
        } else {
            $.pnotify({
                type: 'error',
                title: 'Campo Obrigatório',
                text: 'Selecione os alunos.'
            });
            return false;
        }

        if (!validacao) {
            $.pnotify({
                type: 'error',
                title: 'Campo Obrigatório',
                text: 'Existe alunos com o campo IDENTIFICAÇÃO em branco, favor verifique'
            });
            return false;
        }

        var url = '/carteirinha/imprimir-lote/?usuarios=' + JSON.stringify(arrPrint);
        window.open(url, 'janela', 'width=350,height=430, top=100, left=100, scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no');

    },
    gerarXls: function () {
        var selectedModels = pageableGridLog.getSelectedModels();
        var collectionEnviar = new Backbone.Collection;
        collectionEnviar.add(selectedModels);

        var validacao = true;
        var arrPrint = [];
        if (collectionEnviar.length) {
            collectionEnviar.each(function (model, i) {
                arrPrint[i] = {
                    'id_usuario': model.get('id_usuario'),
                    'st_identificacao': model.get('st_identificacao'),
                    'st_nomecompleto': model.get('st_nomecompleto'),
                    'st_codigo': model.get('st_codigo'),
                    'st_urlavatar': model.get('st_urlavatar')
                }
            });
        } else {
            $.pnotify({
                type: 'error',
                title: 'Campo Obrigatório',
                text: 'Selecione os alunos.'
            });
            return false;
        }

        if (validacao) {
            var url = '/impressao-carteirinha/gerar-xls/?usuarios=' + JSON.stringify(arrPrint);
            window.open(url);
        }
    }
});


/*********************************************
 ************** LAYOUT ***********************
 *********************************************/
var ImpressaoCarteirinhaLayout = Marionette.LayoutView.extend({
    template: '#template-dados-carteirinha',
    tagName: 'div',
    className: 'container-fluid',
    regions: {
        dados: '#wrapper-dados'
    },
    onShow: function () {
        this.dados.show(new ViewDados());
        $('#container-DataGrid').hide();
    }
});

/*********************************************
 ************** INICIALIZAÇÃO ****************
 *********************************************/

var layoutImpressaoCarteirinha = new ImpressaoCarteirinhaLayout();
G2S.show(layoutImpressaoCarteirinha);
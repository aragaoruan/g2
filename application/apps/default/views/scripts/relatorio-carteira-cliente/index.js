var LayoutRelatorio = Marionette.LayoutView.extend({
    template: '#template-layout-relatorio',
    tagname: 'div',
    className: 'containder-fluid',
    regions: {
        formulario: '#container-formulario',
        tabela: '#container-tabela'
    },
    onShow: function () {
        var formulario = new FormularioView();
        this.formulario.show(formulario);

        return this;
    }
});


var FormularioView = Marionette.ItemView.extend({
    template: "#template-formulario-relatorio",
    ui: {
        dt_inicio: 'input#dt_filtro_inicio',
        dt_fim: 'input#dt_filtro_fim'
    },
    onShow: function () {
        var that = this;
        return this;
    },
    events: {
        'click #btnPesquisar': 'enviarFormulario',
        'click button#botaoImpressaoXLS': 'gararXls',
        'click button#botaoImpressaoNormal' : 'gerarImpressao'
    },
    enviarFormulario: function(){
        var that = this;
        var data = { 'dt_inicio' : that.ui.dt_inicio.val(), 'dt_fim' : that.ui.dt_fim.val() };
        $.ajax({
            dataType: 'html',
            type: 'post',
            url: '/relatorio-carteira-cliente/retorna-relatorio',
            data: data,
            beforeSend: function () {
                loading();
            },
            success: function (html) {
                $('#container-tabela').html('');
                $('#container-tabela').html(html);
            },
            complete: function () {
                loaded();
            },
            error: function (data) {
                $.pnotify({
                    type: 'error',
                    title: 'Erro!',
                    text: 'Erro ao tentar salvar códigos dos usuários.'
                });
            }
        });
    },
    gararXls: function () {
        var that = this;
        var url = '/relatorio-carteira-cliente/gerar-xls/?dt_fim=' + that.ui.dt_fim.val() + '&dt_inicio=' + that.ui.dt_inicio.val();
        window.open(url, '_blank');
    },
    gerarImpressao: function () {
        var that = this;
        var url = '/relatorio-carteira-cliente/impressao/?dt_fim=' + that.ui.dt_fim.val() + '&dt_inicio=' + that.ui.dt_inicio.val();
        window.open(url, '_blank');
    }
});

var layoutRelatorio = new LayoutRelatorio();
G2S.show(layoutRelatorio);
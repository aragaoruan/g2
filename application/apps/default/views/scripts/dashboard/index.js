// Variavel para Array de Situacoes
var comboSituacao = null;

// MODEL
var DashboardModel = Backbone.Model.extend({
    idAttribute: 'id_dashboard',
    defaults: {
        id_dashboard: null,
        st_dashboard: '',
        st_descricao: '',
        st_origem: '',
        st_tipochart: '',
        st_xtipo: '',
        st_xtitulo: '',
        st_ytipo: '',
        st_ytitulo: '',
        st_campolabel: '',
        id_funcionalidade: {
            id_funcionalidade: null,
            st_funcionalidade: ''
        },
        id_situacao: {
            id_situacao: null,
            st_situacao: ''
        },
        bl_ativo: 1,
        // RELACOES TB
        tb_campodashboard: null
    },
    url: function() {
        return '/api/dashboard/' + (this.id || '');
    }
});

// ITEM VIEW
var DashboardView = Marionette.ItemView.extend({
    model: new DashboardModel(),
    tagName: 'tr',
    template: '#dashboard-view',
    events: {
        'click .actionEdit': 'actionEdit',
        'click .actionRemove': 'actionRemove'
    },
    actionEdit: function(e) {
        e.preventDefault();

        var that = this;

        var Model = new DashboardEdit({ model: that.model });
        G2S.layout.dashboard.show(Model);
    },
    actionRemove: function(e) {
        e.preventDefault();

        var that = this;

        bootbox.confirm('Deseja realmente remover este registro?', function(response) {
            if (response) {
                loading();
                if (that.model.get('id_dashboard')) {
                    that.model.destroy({
                        silent: true,
                        success: function() {
                            that.remove();
                        },
                        complete: function(response) {
                            response = $.parseJSON(response.responseText);
                            $.pnotify(response);
                            loaded();
                        }
                    });
                }
            }
        });
    }
});

// EMPTY ITEMVIEW
var DashboardEmpty = Marionette.ItemView.extend({
    tagName: 'tr',
    template: _.template('<td colspan="5">Nenhum registro encontrado.</td>')
});

// DASHBOARD EDIT
var DashboardEdit = Marionette.ItemView.extend({
    model: new DashboardModel(),
    tagName: 'div',
    template: '#dashboard-edit',
    ui: {
        st_dashboard: '[name="st_dashboard"]',
        st_descricao: '[name="st_descricao"]',
        st_origem: '[name="st_origem"]',
        st_tipochart: '[name="st_tipochart"]',
        st_xtipo: '[name="st_xtipo"]',
        st_xtitulo: '[name="st_xtitulo"]',
        st_ytipo: '[name="st_ytipo"]',
        st_ytitulo: '[name="st_ytitulo"]',
        st_campolabel: '[name="st_campolabel"]',
        id_funcionalidade: '[name="id_funcionalidade"]',
        id_situacao: '[name="id_situacao"]',
        tb_campodashboard: '.campo-dashboard',
        chart_preview: '.dashboard-preview'
    },
    onRender: function() {
        var that = this;

        // Selecionar opcao nos comboboxes
        this.ui.st_tipochart.val(this.model.get('st_tipochart'));
        this.ui.st_xtipo.val(this.model.get('st_xtipo'));
        this.ui.st_ytipo.val(this.model.get('st_ytipo'));

        // Montar SELECT "Situação"
        ComboboxView({
            el: that.ui.id_situacao,
            url: '/api/situacao/?st_tabela=tb_dashboard',
            collection: comboSituacao,
            optionLabel: 'st_situacao',
            optionSelectedId: that.model.get('id_situacao').id_situacao,
            emptyOption: '[Selecione]',
            model: Backbone.Model.extend({
                idAttribute: 'id_situacao',
                defaults: {
                    id_situacao: null,
                    st_situacao: ''
                }
            })
        }, function (el, composite) {
            if (!comboSituacao && composite.collection && composite.collection.models)
                comboSituacao = composite.collection.models;
        });


        // Listar campos cadastrados
        this.FieldIni = new FieldComposite({ id_dashboard: that.model.get('id_dashboard') });
        this.ui.tb_campodashboard.html(this.FieldIni.render().$el);
    },
    onShow: function() {
        this.ui.st_dashboard.focus();
    },
    events: {
        'click .actionEdit': 'actionEdit',
        'submit form': 'actionSave',
        'click .actionPreview': 'actionPreview'
    },
    actionEdit: function(e) {
        e.preventDefault();
        G2S.layout.dashboard.show(new DashboardComposite());
    },
    getValidatedValues: function() {
        var values = {
            st_dashboard: this.ui.st_dashboard.val(),
            st_descricao: this.ui.st_descricao.val(),
            st_origem: this.ui.st_origem.val(),
            st_tipochart: this.ui.st_tipochart.val(),
            st_xtipo: this.ui.st_xtipo.val(),
            st_xtitulo: this.ui.st_xtitulo.val(),
            st_ytipo: this.ui.st_ytipo.val(),
            st_ytitulo: this.ui.st_ytitulo.val(),
            st_campolabel: this.ui.st_campolabel.val(),
            id_funcionalidade: {
                id_funcionalidade: this.ui.id_funcionalidade.val()
            },
            id_situacao: {
                id_situacao: this.ui.id_situacao.val(),
                st_situacao: this.ui.id_situacao.find('option:selected').text()
            },
            tb_campodashboard: this.FieldIni.collection.toJSON()
        };

        // Validacao dos campos
        var errorMsg = false;
        var errorInp = false;
        if (!values.st_dashboard.trim()) {
            errorMsg = 'O campo Título é obrigatório.';
            errorInp = this.ui.st_dashboard;
        }
        else if (!values.st_origem.trim()) {
            errorMsg = 'O campo Origem é obrigatório.';
            errorInp = this.ui.st_origem;
        }
        else if (!values.st_tipochart.trim()) {
            errorMsg = 'O campo Tipo de Chart é obrigatório.';
            errorInp = this.ui.st_tipochart;
        }
        else if (!values.id_situacao.id_situacao) {
            errorMsg = 'O campo Situação é obrigatório.';
            errorInp = this.ui.id_situacao;
        }
        else if (!values.tb_campodashboard.length) {
            errorMsg = 'É necessário adicionar pelo menos um campo.';
            errorInp = $('.actionAddField');
        }

        if (errorMsg) {
            $.pnotify({ type: 'warning', title: 'Campo Obrigatório', text: errorMsg });
            errorInp.focus();
            return false;
        }

        return values;
    },
    actionSave: function(e) {
        e.preventDefault();

        var that = this;

        var values = this.getValidatedValues();
        if (!values)
            return false;

        // Salvar registro
        loading();
        this.model.save(values, {
            success: function(model, response) {
                // Definir o ID caso obtenha sucesso
                if (response.type && response.type == 'success')
                    that.model.set('id_dashboard', response.codigo);
            },
            complete: function(response) {
                response = $.parseJSON(response.responseText);
                $.pnotify(response);
                loaded();
            }
        });
    },
    actionPreview: function() {
        var that = this;

        var values = this.getValidatedValues();
        if (!values)
            return false;

        loading();

        $.ajax({
            url: '/dashboard/get-json-preview',
            data: values,
            success: function(response) {
                response.tooltip = {
                    formatter: function() {
                        return this.series.name + ': <b>' + this.y + '</b>';
                    }
                };
                that.ui.chart_preview.highcharts(response);
                that.ui.chart_preview.closest('.modal').modal('show');
                $(window).trigger('resize');
            },
            error: function() {
                $.pnotify({
                    type: 'error',
                    title: 'Erro',
                    text: 'Não foi possível gerar a visualização. Verifique os campos e tente novamente.'
                })
            },
            complete: loaded
        });
    }
});

// COLLECTION
var DashboardCollection = Backbone.Collection.extend({
    model: DashboardModel,
    url: '/api/dashboard/?bl_ativo=1&order_by=st_dashboard'
});

// COMPOSITE VIEW
var DashboardComposite = Marionette.CompositeView.extend({
    template: '#dashboard-template',
    collection: new DashboardCollection(),
    childView: DashboardView,
    emptyView: DashboardEmpty,
    childViewContainer: 'tbody',
    initialize: function() {
        this.collection.fetch({ complete: loaded });
    },
    events: {
        'click .actionAdd': 'actionAdd'
    },
    actionAdd: function(e) {
        e.preventDefault();

        var Model = new DashboardEdit();
        Model.model.set(Model.model.defaults);
        G2S.layout.dashboard.show(Model);
    }
});

G2S.layout.addRegions({ dashboard : '#dashboard-content' });
G2S.layout.dashboard.show(new DashboardComposite());


/**
 * DADOS PARA CAMPOS AO EDITAR DASHOARD
 */
// MODEL
var FieldModel = Backbone.Model.extend({
    idAttribute: 'id_campodashboard',
    defaults: {
        id_campodashboard: null,
        st_campodashboard: '',
        st_titulocampo: '',
        id_dashboard: null
    },
    //url: function() {
    //    return '/api/campo-dashboard/' + (this.id || '');
    //},
    is_editing: false,
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    }
});

// ITEMVIEW
var FieldView = Marionette.ItemView.extend({
    model: new FieldModel(),
    tagName: 'tr',
    ui: {
        st_campodashboard: '[name="st_campodashboard"]',
        st_titulocampo: '[name="st_titulocampo"]'
    },
    onBeforeRender: function() {
        this.template = this.model.is_editing ? '#field-edit' : '#field-view'
    },
    events: {
        'click .actionEdit': 'actionEdit',
        'click .actionSave': 'actionSave',
        'click .actionRemove': 'actionRemove'
    },
    actionEdit: function(e) {
        e.preventDefault();

        if (!this.model.get('id_campodashboard') && !this.model.get('st_campodashboard').trim() && !this.model.get('st_titulocampo').trim())
            this.model.destroy();
        else {
            this.model.toggleEdit();
            this.render();
            this.$el.find('input:first').focus();
        }
    },
    actionSave: function(e) {
        e.preventDefault();

        var values = {
            st_campodashboard: this.ui.st_campodashboard.val(),
            st_titulocampo: this.ui.st_titulocampo.val()
        };

        var errorMsg = false;
        if (!values.st_campodashboard.trim()) {
            errorMsg = 'O Nome do Campo é obrigatório.';
            this.ui.st_campodashboard.focus();
        }
        else if (!values.st_titulocampo.trim()) {
            errorMsg = 'O Título do Campo é obrigatório.';
            this.ui.st_titulocampo.focus();
        }

        if (errorMsg) {
            $.pnotify({ type: 'warning', title: 'Campo Obrigatório', text: errorMsg });
            return false;
        }

        this.model.set(values);

        this.model.toggleEdit();
        this.render();
    },
    actionRemove: function(e) {
        e.preventDefault();

        var that = this;

        bootbox.confirm('Deseja realmente remover este registro?', function(response) {
            if (response) {
                that.model.set('id_campodashboard', null);
                that.model.destroy();
            }
        });
    }
});

// EMPTY ITEMVIEW
var FieldEmpty = Marionette.ItemView.extend({
    tagName: 'tr',
    template: _.template('<td colspan="3">Nenhum registro encontrado.</td>')
});

// COLLECTION
var FieldCollection = Backbone.Collection.extend({
    model: FieldModel,
    url: '/api/campo-dashboard/',
    urlRoot: '/api/campo-dashboard/'
});

// COMPOSITE VIEW
var FieldComposite = Marionette.CompositeView.extend({
    template: '#field-template',
    collection: new FieldCollection(),
    childView: FieldView,
    emptyView: FieldEmpty,
    childViewContainer: 'tbody',
    initialize: function(options) {
        this.id_dashboard = options.id_dashboard || null;
    },
    onRender: function() {
        if (this.id_dashboard) {
            this.collection.url = this.collection.urlRoot + '?id_dashboard=' + this.id_dashboard;
            this.collection.fetch({complete: loaded});
        } else {
            this.collection.reset();
        }
    },
    events: {
        'click .actionAddField': 'actionAddField'
    },
    actionAddField: function(e) {
        e.preventDefault();

        var Model = new FieldModel();
        Model.set(Model.defaults);
        Model.is_editing = true;
        this.collection.add(Model);
        this.$el.find('tr:last input:first').focus();
    },
    addCollection: function(model) {
        model.set('id_dashboard', this.id_dashboard);
    }
});
/**
 * Leitura de arquivo retorno para envio de material
 * @date 25-08-2015
 * @author Denise Xavier  <denise.xavier@unyleya.com.br>
 */

var PesquisaArquivoRetorno = false;
var TelaAdicionar = false;
var TelaListar = false;

/****************************************************
 * Models
 ****************************************************/
var ModelSituacao = Backbone.Model.extend({
    idAttribute: 'id_situacao',
    defaults: {
        'id_situacao': null,
        'st_situacao': ''
    }
});



var ModelPesquisaArquivo = Backbone.Model.extend({
    defaults: {
        'st_link': '',
        'id_arquivoretornomaterial': null,
        'nu_itens': null,
        'dt_cadastro': '',
        'st_arquivoretornomaterial': '',
        'id_situacao': '',
        'st_situacao': ''
    },
    url: '/default/arquivo-retorno-material/pesquisar-arquivo-retorno-material'
});


var ModelArquivoRetornoMaterial = Backbone.Model.extend({
    defaults: {
        'id_arquivoretornomaterial': '',
        'id_entidade': '',
        'st_arquivoretornomaterial': '',
        'dt_cadastro': '',
        'id_usuariocadastro': '',
        'id_situacao': '',
        'bl_ativo': '',
        'id_upload': ''
    },
    url: '/default/arquivo-retorno-material/upload'
});

var ModelArquivoPacote = Backbone.Model.extend({
    defaults:{
        'id_pacote' :'',
        'id_itemdematerial': '',
        'st_codrastreamento': '',
        'dt_postagem':'',
        'id_situacaopostagem' :''
    },
    url: '/default/arquivo-retorno-material/pesquisar-arquivo-retorno-listar'
});

/****************************************************
 * Layout
 ****************************************************/
var LayoutArquivoRetorno = Marionette.LayoutView.extend({
    template: '#layout-arquivo-retorno',
    tagName: 'div',
    className: 'container-fluid',
    regions: {
        regiaoDetalhe: '#containerDetalhes',
        regiaoFind: '#formFind',
        regiaoAdd: '#formAdd'
    }
});


/****************************************************
 * Collections
 ****************************************************/

var SituacaoCollection = Backbone.Collection.extend({
    model: ModelSituacao,
    url: '/api/situacao/index/st_tabela/tb_arquivoretornomaterial'
});


var PesquisaCollection = Backbone.Collection.extend({
    model: ModelPesquisaArquivo,
    url: '',
    urlRoot: '/default/arquivo-retorno-material/retorna-arquivo-retorno-material-entidade'
});

var ArquivoPacoteCollection = Backbone.Collection.extend({
    model: ModelArquivoPacote,
    url: '',
    urlRoot: '/default/arquivo-retorno-material/retorna-arquivo-retorno-pacote'
});

/****************************************************
 * Views
 ****************************************************/

var ViewDadosPesquisaEmpty = Marionette.ItemView.extend({
    tagName: 'tr',
    template: _.template('<td colspan="9"><div class="alert alert-warning">Nenhum arquivo para exibição</div>/td>')
});

var ViewTelaPesquisa = Marionette.ItemView.extend({
    template: '#template-find-file-return',
    model: new ModelPesquisaArquivo(),
    ui: {
        itemStArquivo: '#st_arquivoretornomaterial',
        itemIdSituacao: '#id_situacao',
        itemdtInicio: '#dt_cadastroinicio',
        itemdtTermino: '#dt_cadastrofim'
    },
    onRender: function () {
        this.carregaCombo();
    },
    events: {
        'submit #form-pesquisa-arquivo': 'pesquisaArquivoRetorno',
        'click #btn-add-arquivo-retorno': 'adicionarArquivoRetorno'
    },
    carregaCombo: function () {
        var collectionSit = new SituacaoCollection();
        collectionSit.fetch();
        this.ui.itemIdSituacao.html('<option value="">Todas</option>');

        var viewSituacao = new SelectView({
            el: this.$el.find('#id_situacao'),
            collection: collectionSit,
            childViewOptions: {
                optionLabel: 'st_situacao',
                optionValue: 'id_situacao'
            }
        });
        viewSituacao.render();
    },
    validacoes: function () {
        var retorno = true;
        if ((this.ui.itemStArquivo.val() == '') && (this.ui.itemdtInicio.val() == '' || this.ui.itemdtTermino.val() == '')) {
            $.pnotify({
                title: 'Aviso!',
                text: 'Por favor informe o nome do arquivo ou um intervalo de datas para prosseguir.',
                type: 'warning'
            });
            this.ui.itemStArquivo.focus();
            retorno =  false;
        }

        if (this.ui.itemdtInicio.val() != '' && this.ui.itemdtTermino.val() != '') {
            var compararDataMenor = compareDate(this.ui.itemdtInicio.val(), this.ui.itemdtTermino.val());
            if (compararDataMenor == -1) {
                $.pnotify({
                    title: 'Aviso!',
                    text: 'Período inválido, a data inicial deve que ser menor que a final.',
                    type: 'warning'
                });
                this.ui.itemdtInicio.focus();
                retorno =  false;

            }
        }
        return retorno;
    },
    pesquisaArquivoRetorno: function (e) {
        e.preventDefault();

        if (this.validacoes()) {

            if (PesquisaArquivoRetorno) {
                PesquisaArquivoRetorno.url = PesquisaArquivoRetorno.urlRoot + "?" + $('#form-pesquisa-arquivo').serialize();
            } else {
                PesquisaArquivoRetorno = new PesquisaCollection({});
                PesquisaArquivoRetorno.url = PesquisaArquivoRetorno.urlRoot + "?" + $('#form-pesquisa-arquivo').serialize();
            }

            loading();
            var containerGrid = $(layoutArquivoRetorno.regions.regiaoFind).find('#conteudo-pesquisa');
            var cellOption = Backgrid.Cell.extend({
                template: _.template("<div class='text-center'><a href='http://<%=st_link%>' class='btn btn-small btn-inverse' target='blank'><i class='icon icon-white icon-arrow-down' title='Download'></i></a> " +
                "<a class='btn btn-small btn-success edit'><i class='icon icon-white icon-file' title='Visualizar'></i> </a></div>"),
                events: {
                    "click .edit": "abreArquivos"
                },
                abreArquivos: function (e) {
                    var containerGridPacote = $(layoutArquivoRetorno.regions.regiaoFind).find('#conteudo-lista-detalhes');
                        var columnsPacote = [
                            {name: "id_pacote", label: "Código Pacote", editable: false, cell: "string" },
                            {name: "id_itemdematerial", label: "Item Material", editable: false, cell: "string" },
                            {name: "st_codrastreamento", label: "Código Rastreamento", editable: false, cell: "string" },
                            {name: "st_resultado", label: "Resultado", editable: false, cell: "string" }
                        ];
                    var listaArquivoPacote = new ArquivoPacoteCollection();
                    listaArquivoPacote.url = listaArquivoPacote.urlRoot + '/id_arquivoretornomaterial/'+this.model.get('id_arquivoretornomaterial');
                        var grid = new Backgrid.Grid({
                            className: 'backgrid table table-bordered table-hover arquivo-retorno-pacote',
                            columns: columnsPacote,
                            collection: listaArquivoPacote
                        });
                    listaArquivoPacote.fetch({
                            complete: function (collection) {
                                $(layoutArquivoRetorno.regions.regiaoFind).find('#conteudo-pesquisa').addClass('hide');
                                $(layoutArquivoRetorno.regions.regiaoFind).find('#conteudo-pesquisa').hide();
                                containerGridPacote.show().empty().append(grid.render().$el);
                                loaded();
                            },
                            beforeSend: function () {
                                loading();
                            }
                        });
                },
                render: function () {
                    this.$el.html(this.template({"st_link": this.model.get('st_link')}));
                    this.delegateEvents();
                    return this;
                }
            });

            var columns = [
                {name: "st_arquivoretornomaterial", label: "Arquivo", editable: false, cell: "string" },
                {name: "dt_cadastro", label: "Data Cadastro", editable: false, cell: "string" },
                {name: "nu_itens", label: "Itens do arquivo", editable: false, cell: "string" },
                {name: "st_situacao", label: "Situação", editable: false, cell: "string" },
                {name: "options", label: "Opções", sortable: false, editable: false, cell: cellOption}
            ];
            var grid = new Backgrid.Grid({
                className: 'backgrid table table-bordered table-hover arquivo-retorno-material',
                columns: columns,
                collection: PesquisaArquivoRetorno
            });

            PesquisaArquivoRetorno.fetch({
                complete: function (collection) {
                    //renderiza o grid
                    $(layoutArquivoRetorno.regions.regiaoFind).find('#conteudo-lista-detalhes').hide();
                    containerGrid.show().empty().append(grid.render().$el);
                    loaded();
                },
                beforeSend: function () {
                    loading();
                }
            });
        }
    },
    adicionarArquivoRetorno: function(){
        $(layoutArquivoRetorno.regiaoFind.el).hide();
        if(TelaAdicionar){
            $(layoutArquivoRetorno.regiaoAdd.el).show();
        }else{
            TelaAdicionar = new ViewTelaAdicionarArquivo();
            layoutArquivoRetorno.regiaoAdd.show(new ViewTelaAdicionarArquivo());
        }

    }
});



var ViewTelaAdicionarArquivo = Marionette.ItemView.extend({
    template: '#template-add-file',
    model: new ModelArquivoRetornoMaterial(),
    onRender: function () {
        $('#bt-modelo-arquivo').css('cursor', 'pointer');
    },
    events: {
        'click #bnt-salvar-arquivo': 'uploadArquivoRetornoMaterial',
        'click #btn-cancelar-envio': 'cancelarEnvioArquivoRetorno',
        'click #bt-modelo-arquivo' : 'toogleModelo'
    },
    toogleModelo: function(){
        $('#arquivo-retorno-material-modelo').toggle();
    },
    uploadArquivoRetornoMaterial: function (e) {
        e.preventDefault();
        var that = this;

        var arquivos = $('#arquivo-retorno-material')[0].files;
        var formData = new FormData();

        for (var i = 0; i < arquivos.length; i++) {
            var file = arquivos[i];

            // Validar extensao do arquivo
            var extensions = ['.xls', '.xlsx'];
            var extensaoValida = (new RegExp('(' + extensions.join('|').replace(/\./g, '\\.') + ')$')).test(file.name);

            if (extensaoValida)
                formData.append('arquivo', file, file.name);
            else {
                $.pnotify({title: 'Aviso', text: 'Esta extensão de arquivo não é permitida.', type: 'warning'});
                return false;
            }
        }

        var template = _.template($('#arquiv-retorno-material-carregando').html());
        that.el.innerHTML = template();

        var progressBar = $('#arquivo-retorno-material-progress');

        $.ajax({
            url: 'arquivo-retorno-material/upload',
            type: 'post',
            data: formData,
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false,
            progressUpload: function (evt) {
                if (evt.lengthComputable) {
                    var value = parseInt((evt.loaded / evt.total * 100));
                    progressBar.find('.bar').css('width', value + '%');
                }
            },
            success: function (response, textStatus, jqXHR) {
                if (response.type == 'success'){
                    $.pnotify({title: 'Concluído', text: response.mensagem, type: 'success'});
                    $(layoutArquivoRetorno.regiaoAdd.el).hide();
                    $(layoutArquivoRetorno.regiaoFind.el).find('#form-pesquisa-arquivo').submit();
                    $(layoutArquivoRetorno.regiaoFind.el).show();
                }else
                    this.error();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                that.render();
                $.pnotify({title: 'Erro', text: 'Houve um erro ao salvar o arquivo.', type: 'error'});
            },
            complete: function () {
                that.render();
            }
        });

    },
    cancelarEnvioArquivoRetorno: function(){
        $(layoutArquivoRetorno.regiaoAdd.el).hide();
        $(layoutArquivoRetorno.regiaoFind.el).show();
    }
});


/*********************************************
 ************** INICIALIZAÇÃO ****************
 *********************************************/

var layoutArquivoRetorno = new LayoutArquivoRetorno();
G2S.show(layoutArquivoRetorno);
layoutArquivoRetorno.regiaoFind.show(new ViewTelaPesquisa());


/**
 * Conjunto de funcoes javascript, marionette, backbone para funcionalidade de
 * gerenciamento de notificacoes.
 *
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */

var NOTIFICACAO = {
    LimiteAlunoExcedidoSala: 1,
    ContratoSemMatriculaVinculada: 2
};

var LayoutViewExtend = Marionette.LayoutView.extend({
    template: '#layout-view-template',
    regions: {
        limiteAlunosExcedidosRegion: '#limite-alunos-excedidos-region',
        notificaContratoSemMatriculaRegion: '#venda-sem-matricula-gerada-region'
        //vendaSemMatriculaGeradaRegion: '#venda-sem-matricula-gerada-region'
    },
    onShow: function () {

        this.getRegion('limiteAlunosExcedidosRegion').show(new LimiteAlunoExcedidoCompositeViewExtend({
            collection: new VwPerfisEntidadeNotificacaoCollectionExtend()
        }));
        this.getRegion('notificaContratoSemMatriculaRegion').show(new NotificacaoContratoSemMatriculaCompositeViewExtend({
            collection: new VwPerfisEntidadeNotificacaoCollectionExtend()
        }));
    }
});

var VwPerfisEntidadeNotificacaoCollectionExtend = Backbone.Collection.extend({
    model: VwPerfisEntidadeNotificacao,
    url: '/organizacao/retorna-vw-perfis-entidade-notificacao',
    save: function (options) {
        return Backbone.sync('create', this, options);
    }
});

var LimiteAlunosExcedidosItemView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: '#tpl-item-view-limite-alunos-excedidos',
    ui: {
        check: 'input'
    },
    events: {
        'click input': function () {
            this.model.change = !this.model.change;

            if (this.ui.check.is(':checked')) {
                this.model.set('id_notificacaoentidadeperfil', 0);
            }
        }
    }
});

var LimiteAlunosExcedidosEmptyView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: _.template('<td colspan="5"><center>Nenhuma informação para ser visualizada</center></td>')
});

var LimiteAlunoExcedidoCompositeViewExtend = Marionette.CompositeView.extend({
    template: '#limite-alunos-excedidos-composite-view',
    childViewContainer: 'tbody',
    childView: LimiteAlunosExcedidosItemView,
    emptyView: LimiteAlunosExcedidosEmptyView,
    model: new NotificacaoEntidade(),
    collectionClone: [],
    ui: {
        inputFiltroPerfil: '.input-filtro-perfil',
        inputLimitPercentualAlunosSala: '.input-limit-percentual-alunos-sala',
        comboEnviarEmailLimitAlunoSala: '.combo-enviar-email-limit-aluno-sala'
    },
    initialize: function () {
        var that = this;

        this.collection.fetch({
            async: false,
            data: {
                id_entidade: G2S.loggedUser.get('id_entidade'),
                id_notificacao: NOTIFICACAO.LimiteAlunoExcedidoSala
            },
            complete: function () {

                if (that.collection.length) {
                    that.model.set({
                        'id_notificacaoentidade': that.collection.at(0).get('id_notificacaoentidade'),
                        'bl_enviaparaemail': that.collection.at(0).get('bl_enviaparaemail'),
                        'nu_percentuallimitealunosala': that.collection.at(0).get('nu_percentuallimitealunosala'),
                        'id_entidade': that.collection.at(0).get('id_entidade'),
                        'id_perfil': that.collection.at(0).get('id_perfil')
                    });

                    //Setando o tipo de notificacao para 1 - Limite de alunos excedidos
                    that.model.set('id_notificacao', NOTIFICACAO.LimiteAlunoExcedidoSala);
                    that.model.changed = null;
                } else {
                    //Caso a entidade nao tenha um vinculo com notificacao registrada na tb_notificacaoentidade
                    that.collection.fetch({
                        data: {
                            id_notificacao: NOTIFICACAO.LimiteAlunoExcedidoSala
                       }
                    });
                }
            }
        });
        return this;
    },
    onRender: function () {
        //copiando collection para usar no filtro uma lista que contem os dados
        //originais nao perdendo na hora que o filtro for aplicado.
        this.collectionClone = this.collection.clone();

        //evitando uso de caracteres de texto em campo numerico.
        this.ui.inputLimitPercentualAlunosSala.numeric();
    },
    events: {
        'keyup .input-filtro-perfil': 'filtrarPerfis',
        'click #btn-salvar-notificacao-limit-aluno-sala': 'salvarNotificacao',
        'change .combo-enviar-email-limit-aluno-sala': 'setEnviarEmail',
        'change .input-limit-percentual-alunos-sala': 'setInputPercentualLimitAlunoSala',
        'click #btn-cancelar-notificacao-limit-aluno-sala': 'cancelar'
    },
    filtrarPerfis: function () {
        var inputVal = this.ui.inputFiltroPerfil.val();

        if (inputVal.length >= 3 || inputVal.length == 0) {

            //Filtrando em colecao que se mantem intacta collectionClone
            var newCollection = this.collectionClone.filter(function (child, index, collection) {
                return child.get('st_nomeperfil').toLowerCase().indexOf(inputVal) != -1;
            });

            //resetando a collecao de perfis somente com perfis encontrados nos filtros.
            this.collection.set(newCollection);
        }
    },
    salvarNotificacao: function (e) {
        e.preventDefault();
        var that = this;

        if(!this.ui.comboEnviarEmailLimitAlunoSala.val()){
            $.pnotify({title: 'Aviso', text: 'Escolha uma opção no campo "Enviar para o Email"', type: 'warning'});
            this.ui.comboEnviarEmailLimitAlunoSala.focus();
            return false;
        }

        //Preparando os dados para serem salvos no sync de uma collection inteira.
        var dataSave = {
            perfis: [],
            bl_enviaparaemail: this.model.get('bl_enviaparaemail'),
            id_entidade: this.model.get('id_entidade'),
            id_notificacao: NOTIFICACAO.LimiteAlunoExcedidoSala,
            id_notificacaoentidade: this.model.get('id_notificacaoentidade'),
            id_notificacaoentidadeperfil: this.model.get('id_notificacaoentidadeperfil'),
            id_perfil: this.model.get('id_perfil'),
            nu_percentuallimitealunosala: this.model.get('nu_percentuallimitealunosala')
        };

        this.collection.each(function (model) {
            if (model.change) {
                dataSave.perfis.push(model);
            }
        });

        if (dataSave.perfis.length || this.model.changed) {

            //Setando collection somente com dados alterados para serem salvos
            var collection = new VwPerfisEntidadeNotificacaoCollectionExtend(dataSave);
            collection.url = '/organizacao/salvar-dados-notificacao-entidade';
            collection.save({
                complete: function (response) {
                    that.collection.each(function (model) {
                        model.change = false;
                        model.changed = null;
                    });

                    $.pnotify({title: 'Sucesso', text: 'Dados salvos com sucesso', type: 'success'});
                }
            });

        } else {
            $.pnotify({title: 'Aviso', text: 'Nenhuma informação a ser salva', type: 'warning'});
        }

    },
    cancelar: function(){
        $('#aColapseOne').trigger('click');
    },
    setEnviarEmail: function () {
        this.model.set('bl_enviaparaemail', this.ui.comboEnviarEmailLimitAlunoSala.val());
    },
    setInputPercentualLimitAlunoSala: function (e) {
        this.model.set('nu_percentuallimitealunosala', this.ui.inputLimitPercentualAlunosSala.val());
    }
});

var NotificacaoContratoSemMatriculaCompositeViewExtend = Marionette.CompositeView.extend({
    template: '#venda-sem-matricula-geradas-composite-view',
    childViewContainer: 'tbody',
    childView: LimiteAlunosExcedidosItemView,
    emptyView: LimiteAlunosExcedidosEmptyView,
    model: new NotificacaoEntidade(),
    collectionClone: [],
    ui: {
        inputFiltroPerfil: '.input-filtro-perfil',
        comboEnviarEmailContratoSemMatricula: '.combo-enviar-email-contrato-sem-matricula'
    },
    initialize: function () {
        var that = this;

        this.collection.fetch({
            async: false,
            data: {
                id_entidade: G2S.loggedUser.get('id_entidade'),
                id_notificacao: NOTIFICACAO.ContratoSemMatriculaVinculada
            },
            complete: function () {

                if (that.collection.length) {
                    that.model.set({
                        'id_notificacaoentidade': that.collection.at(0).get('id_notificacaoentidade'),
                        'bl_enviaparaemail': that.collection.at(0).get('bl_enviaparaemail'),
                        'id_entidade': that.collection.at(0).get('id_entidade'),
                        'id_perfil': that.collection.at(0).get('id_perfil')
                    });

                    //Setando o tipo de notificacao para 2 - ContratoSemMatriculaVinculada
                    that.model.set('id_notificacao', NOTIFICACAO.ContratoSemMatriculaVinculada);
                    that.model.changed = null;
                } else {
                    //Caso a entidade nao tenha um vinculo com notificacao registrada na tb_notificacaoentidade
                    that.collection.fetch({
                        data: {
                            id_notificacao: NOTIFICACAO.ContratoSemMatriculaVinculada
                       }
                    });
                }
            }
        });
        return this;
    },
    onRender: function () {
        //copiando collection para usar no filtro uma lista que contem os dados
        //originais nao perdendo na hora que o filtro for aplicado.
        this.collectionClone = this.collection.clone();
    },
    events: {
        'keyup .input-filtro-perfil': 'filtrarPerfis',
        'click #btn-salvar-contrato-sem-matricula': 'salvarNotificacao',
        'change .combo-enviar-email-contrato-sem-matricula': 'setEnviarEmail',
        'change .input-limit-percentual-alunos-sala': 'setInputPercentualLimitAlunoSala',
        'click #btn-cancelar-contrato-sem-matricula': 'cancelar'
    },
    filtrarPerfis: function () {
        var inputVal = this.ui.inputFiltroPerfil.val();

        if (inputVal.length >= 3 || inputVal.length == 0) {

            //Filtrando em colecao que se mantem intacta collectionClone
            var newCollection = this.collectionClone.filter(function (child, index, collection) {
                return child.get('st_nomeperfil').toLowerCase().indexOf(inputVal) != -1;
            });

            //resetando a collecao de perfis somente com perfis encontrados nos filtros.
            this.collection.set(newCollection);
        }
    },
    salvarNotificacao: function (e) {
        e.preventDefault();
        var that = this;

        if(!this.ui.comboEnviarEmailContratoSemMatricula.val()){
            $.pnotify({title: 'Aviso', text: 'Escolha uma opção no campo "Enviar para o Email"', type: 'warning'});
            this.ui.comboEnviarEmailContratoSemMatricula.focus();
            return false;
        }

        //Preparando os dados para serem salvos no sync de uma collection inteira.
        var dataSave = {
            perfis: [],
            bl_enviaparaemail: this.model.get('bl_enviaparaemail'),
            id_entidade: this.model.get('id_entidade'),
            id_notificacao: NOTIFICACAO.ContratoSemMatriculaVinculada,
            id_notificacaoentidade: this.model.get('id_notificacaoentidade'),
            id_notificacaoentidadeperfil: this.model.get('id_notificacaoentidadeperfil'),
            id_perfil: this.model.get('id_perfil')
        };

        this.collection.each(function (model) {
            if (model.change) {
                dataSave.perfis.push(model);
            }
        });

        if (dataSave.perfis.length || this.model.changed) {

            //Setando collection somente com dados alterados para serem salvos
            var collection = new VwPerfisEntidadeNotificacaoCollectionExtend(dataSave);
            collection.url = '/organizacao/salvar-dados-notificacao-entidade';
            collection.save({
                complete: function (response) {
                    that.collection.each(function (model) {
                        model.change = false;
                        model.changed = null;
                    });

                    $.pnotify({title: 'Sucesso', text: 'Dados salvos com sucesso', type: 'success'});
                }
            });

        } else {
            $.pnotify({title: 'Aviso', text: 'Nenhuma informação a ser salva', type: 'warning'});
        }

    },
    cancelar: function(){
        $('#aColapseOne').trigger('click');
    },
    setEnviarEmail: function () {
        this.model.set('bl_enviaparaemail', this.ui.comboEnviarEmailContratoSemMatricula.val());
    }
});

G2S.show(new LayoutViewExtend());


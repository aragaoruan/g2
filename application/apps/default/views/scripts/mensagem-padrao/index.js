/**
 * @author Ilson Nóbrega <ilson.nobrega@unyleya.com.br>
 * @date 07-10-2014
 */

var that;

/*********************************************************************************
 * RESPONSÁVEL POR DEFINIR AS MENSAGENS DO SISTEMA NO GERAL
 *********************************************************************************/

var MensagemPadraoModel = MensagemPadrao.extend({
    defaults: {
        st_textosistema: 'Texto Sistema',
        st_conta: 'Conta de Envio'
    }
});

var SistemaEntidadeMensagemModel = SistemaEntidadeMensagem.extend({
    defaults: {
        st_textosistema: 'Texto Sistema'
    }
});

var SmsEntidadeMensagemModel = SmsEntidadeMensagem.extend({
    defaults: {
        st_textosistema: 'Texto Sistema'
    }
});

var SmsEntidadeMensagemCollection = Backbone.Collection.extend({
    url: '/api/sms-entidade-mensagem',
    model: SmsEntidadeMensagemModel
});

var MensagemPadraoCollection = Backbone.Collection.extend({
    model: MensagemPadraoModel,
    url: "/api/mensagem-padrao"
});

var SistemaEntidadeMensagemCollection = Backbone.Collection.extend({
    url: '/api/sistema-entidade-mensagem',
    model: SistemaEntidadeMensagemModel
});

var MensagemPadraoLayout = Marionette.LayoutView.extend({
    template: '#template-tabs',
    tagName: 'div',
    className: 'container-fluid',
    regions: {
        mensagensEmail: '#mensagens-email',
        mensagensSms: '#mensagens-sms',
        mensagensSistema: '#mensagens-sistema'
    },
    events: {}
});

/*********************************************************************************
 * TEXTO SISTEMA
 *********************************************************************************/

var TextoSistema = Backbone.Model.extend({});

var TextoSistemaCollection = Backbone.Collection.extend({
    model: TextoSistema,
    url: '/textos/get-texto-sistema-options/'
});

var TextoSistemaOptionView = Marionette.ItemView.extend({
    tagName: 'option',
    template: _.template('<%=st_textosistema%>'),
    onRender: function () {
        this.$el.attr('value', this.model.get('id'));
    }
});

var TextoSelectView = Marionette.CollectionView.extend({
    childView: TextoSistemaOptionView,
    tagName: 'select',
    initialize: function () {
        this.render();
    }
});

/*********************************************************************************
 * CONTA ENVIO
 *********************************************************************************/

var ContaEnvio = Backbone.Model.extend({});

var ContaEnvioCollection = Backbone.Collection.extend({
    model: ContaEnvio,
    url: '/mensagem-padrao/get-conta-de-envio',
    initialize: function () {
        this.fetch();
    }
});

var ContaEvioOptionView = Marionette.ItemView.extend({
    tagName: 'option',
    template: _.template('<%=st_conta%>'),
    onRender: function () {
        this.$el.attr('value', this.model.get('id'));
    }
});

var ContaEnvioSelectView = Marionette.CollectionView.extend({
    childView: ContaEvioOptionView,
    collection: new ContaEnvioCollection(),
    tagName: 'select',
    initialize: function () {
        this.render();
    }
});

/*********************************************************************************
 * EMAIL
 *********************************************************************************/
var MensagemEmailItemView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: '#mensagem-email-item-view',
    ui: {
        slTextoSistema: '#slTextoSistema',
        slContaEnvio: '#slContaEnvio'
    },
    events: {
        'click #btnedit': function () {
            this.template = '#mensagem-email-item-view-edit';
            this.render();

            var textoSistemaCollection = new TextoSistemaCollection();
            textoSistemaCollection.url += 'idTextoExibicao/2';
            textoSistemaCollection.fetch({async: false});

            (new TextoSelectView({
                el: this.ui.slTextoSistema,
                collection: textoSistemaCollection
            }).render().$el.val(
                this.model.get('id_textosistema')
            ));

            (new ContaEnvioSelectView({
                el: this.ui.slContaEnvio
            }).render().$el.val(
                this.model.get('id_emailconfig')
            ));
        },
        'click #btncancelar': function () {
            this.template = '#mensagem-email-item-view';
            this.render();
        },
        'click #btnsalvar': function () {
            loading();

            var renderizaDeVolta = this;

            var idTextoSistema = this.ui.slTextoSistema.val();
            var idContaEnvio = this.ui.slContaEnvio.val();
            var textoSistema = this.ui.slTextoSistema.find('option:selected').text();
            var contaEnvio = this.ui.slContaEnvio.find('option:selected').text();

            this.model.set('id_textosistema', idTextoSistema);
            this.model.set('id_emailconfig', idContaEnvio);
            this.model.set('st_textosistema', textoSistema);
            this.model.set('st_conta', contaEnvio);
            this.model.save(null, {
                success: function () {
                    renderizaDeVolta.template = '#mensagem-email-item-view';
                    renderizaDeVolta.render();
                    loaded();
                    $.pnotify({
                        title: 'Mensagens - Sucesso',
                        type: 'success',
                        text: "Os dados foram salvos com sucesso!"
                    });
                },
                error: function () {
                    renderizaDeVolta.template = '#mensagem-email-item-view';
                    renderizaDeVolta.render();
                    loaded();
                    $.pnotify({title: 'Mensagens - Erro', type: 'error', text: "Ocorreu um erro ao salvar os dados."});
                }
            });

        }
    }
});

var MensagemEmailEmptyView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: '#mensagem-email-empty-view'
});


var MensagemEmailCompositeView = Marionette.CompositeView.extend({
    template: '#mensagem-email-view',
    childView: MensagemEmailItemView,
    emptyView: MensagemEmailEmptyView,
    childViewContainer: 'tbody',
    ui: {
        //Ui items
    }
});

/*********************************************************************************
 * SMS
 *********************************************************************************/
var MensagemSmsEmptyView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: '#mensagem-sms-empty-view'
});

var MensagemSmsItemView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: '#mensagem-sms-item-view',
    ui: {
        slTextoSistema: '#slTextoSistema',
        slContaEnvio: '#slContaEnvio'
    },
    events: {
        'click #btnedit': function () {
            this.template = '#mensagem-sms-item-view-edit';
            this.render();

            var textoSistemaCollection = new TextoSistemaCollection();
            textoSistemaCollection.url += 'idTextoExibicao/4';
            textoSistemaCollection.fetch({async: false});

            (new TextoSelectView({
                el: this.ui.slTextoSistema,
                collection: textoSistemaCollection
            }).render().$el.val(
                this.model.get('id_textosistema')
            ));
        },
        'click #btncancelar': function () {
            this.template = '#mensagem-sms-item-view';
            this.render();
        },
        'click #btnsalvar': function () {
            loading();

            var renderizaSms = this;

            var idTextoSistema = this.ui.slTextoSistema.val();
            var textoSistema = this.ui.slTextoSistema.find('option:selected').text();

            this.model.set('id_textosistema', idTextoSistema);
            this.model.set('st_textosistema', textoSistema);

            this.model.save(null, {
                success: function () {
                    renderizaSms.template = '#mensagem-sms-item-view';
                    renderizaSms.render();
                    loaded();
                    $.pnotify({
                        title: 'Mensagens - Sucesso',
                        type: 'success',
                        text: "Os dados foram salvos com sucesso!"
                    });
                },
                error: function () {
                    renderizaSms.template = '#mensagem-sms-item-view';
                    renderizaSms.render();
                    loaded();
                    $.pnotify({title: 'Mensagens - Erro', type: 'error', text: "Ocorreu um erro ao salvar os dados."});
                }
            });

        }
    }
});

var MensagemSmsCompositeView = Marionette.CompositeView.extend({
    template: '#mensagem-sms-view',
    childView: MensagemSmsItemView,
    emptyView: MensagemSmsEmptyView,
    childViewContainer: 'tbody',
    ui: {
        //Ui items
    }
});

/*********************************************************************************
 *  SISTEMA
 *********************************************************************************/
var MensagemSistemaEmptyView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: '#mensagem-sistema-empty-view'
});

var MensagemSistemaItemView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: '#mensagem-sistema-item-view',
    ui: {
        slTextoSistema: '#slTextoSistema',
        slContaEnvio: '#slContaEnvio'
    },
    events: {
        'click #btnedit': function () {
            this.template = '#mensagem-sistema-item-view-edit';
            this.render();

            var textoSistemaCollection = new TextoSistemaCollection();
            textoSistemaCollection.url += 'idTextoExibicao/3';
            textoSistemaCollection.fetch({async: false});

            (new TextoSelectView({
                el: this.ui.slTextoSistema,
                collection: textoSistemaCollection
            }).render().$el.val(
                this.model.get('id_textosistema')
            ));
        },
        'click #btncancelar': function () {
            this.template = '#mensagem-sistema-item-view';
            this.render();
        },
        'click #btnsalvar': function () {
            loading();

            var renderizaSms = this;

            var idTextoSistema = this.ui.slTextoSistema.val();
            var textoSistema = this.ui.slTextoSistema.find('option:selected').text();

            this.model.set('id_textosistema', idTextoSistema);
            this.model.set('st_textosistema', textoSistema);

            this.model.save(null, {
                success: function () {
                    renderizaSms.template = '#mensagem-sistema-item-view';
                    renderizaSms.render();
                    loaded();
                    $.pnotify({
                        title: 'Mensagens - Sucesso',
                        type: 'success',
                        text: "Os dados foram salvos com sucesso!"
                    });
                },
                error: function () {
                    $.pnotify({title: 'Mensagens - Erro', type: 'error', text: "Ocorreu um erro ao salvar os dados."});
                }
            });

        }
    }
});

var MensagemSistemaCompositeView = Marionette.CompositeView.extend({
    template: '#mensagem-sistema-view',
    childView: MensagemSistemaItemView,
    emptyView: MensagemSistemaEmptyView,
    childViewContainer: 'tbody',
    ui: {
        //Ui items
    }
});


/*********************************************************************************
 * INSTANCIAS
 *********************************************************************************/

//Layout
var mensagemPadrao = new MensagemPadraoLayout();

//Mensagem de Email
var mensagemPadraoCollection = new MensagemPadraoCollection();
mensagemPadraoCollection.fetch();

var mensagemEmailCompositeView = new MensagemEmailCompositeView({
    collection: mensagemPadraoCollection
});

//Mensagem de SMS

var smsEntidadeMensagemCollection = new SmsEntidadeMensagemCollection();
smsEntidadeMensagemCollection.fetch();

var mensagemSmsCompositeView = new MensagemSmsCompositeView({
    collection: smsEntidadeMensagemCollection
});

//Mensagens do Sistema
sistemaEntidadeColletion = new SistemaEntidadeMensagemCollection();
sistemaEntidadeColletion.fetch();

var mensagemSistemaCompositeView = new MensagemSistemaCompositeView({
    collection: sistemaEntidadeColletion
});

G2S.show(mensagemPadrao);

//"Show" das Regions
mensagemPadrao.mensagensEmail.show(mensagemEmailCompositeView);
mensagemPadrao.mensagensSms.show(mensagemSmsCompositeView);
mensagemPadrao.mensagensSistema.show(mensagemSistemaCompositeView);

loaded();
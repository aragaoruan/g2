// MODEL
var HomeDashboardModel = Backbone.Model.extend({
    idAttribute: 'id_dashboard',
    url: function() {
        return '/dashboard/get-json' + (this.id ? '?id_dashboard=' + this.id : '');
    }
});

// ITEM VIEW
var HomeDashboardView = Marionette.ItemView.extend({
    model: new HomeDashboardModel(),
    template: '#home-dashboard-view',
    tagName: 'div',
    className: 'span4',
    ui: {
        charts: '.home-dashboard-charts'
    },
    onRender: function() {
        var that = this;

        var noMargin = (this.model.collection.indexOf(this.model)) % 3;
        if (noMargin == 0)
            this.$el.css('margin-left', '0');

        that.model.fetch({
            success: function() {
                that.model.attributes.tooltip = {
                    formatter: function() {
                        return this.series.name + ': <b>' + this.y + '</b>';
                    }
                };

                that.ui.charts
                    .css('height', '300px')
                    .highcharts(that.model.attributes);
            }
        });
    },
    onShow: function() {
        $(window).trigger('resize');
    }
});

// COLLECTION
var HomeDashboardCollection = Backbone.Collection.extend({
    model: HomeDashboardModel,
    url: '/dashboard/get-by-session?bl_ativo=1&id_situacao=165',
    parse: function(response) {
        return (response.type ? response.mensagem : response);
    }
});

// COMPOSITE VIEW
var HomeDashboardComposite = Marionette.CompositeView.extend({
    template: '#home-dashboard-template',
    collection: new HomeDashboardCollection(),
    childView: HomeDashboardView,
    childViewContainer: '.home-dashboard-content',
    initialize: function() {
        this.collection.fetch({ complete: loaded });
    }
});


G2S.layout.content.show(new HomeDashboardComposite());
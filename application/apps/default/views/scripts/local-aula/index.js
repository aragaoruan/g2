/*
 * 
 * Modelo para guardar os locais de aula
 */
var LocalAula = Backbone.Model.extend({
    defaults: {
        st_localaula: 'Nome do local de aula',
        id_catraca: '',
        st_codigocatraca: '',
        nu_maxalunos: '',
        id_situacao: 60,
        id_entidade: '',
        dt_cadastro: '1970-01-01 00:00',
        is_editing: false
    },
    url: function () {
        return this.id ? '/api/local-aula/' + this.id : '/api/local-aula';
    },
    toggleEdit: function () {
        this.is_editing = !this.is_editing;
    }
});
/*
 *
 * Modelo que pega as catracas
 */
var CatracaCollection = Backbone.Collection.extend({
    url: '/api/catraca',
    model: Catraca,
    initialize: function () {
        //this.fetch()
    }
});

/*
 *  Coleção dos locais de aula retornados na requisição 
 *  padrão /api/local-aula. 
 */
var LocaisAula = Backbone.Collection.extend({
    model: LocalAula,
    url: "/api/local-aula"
});

var locaisaula = new LocaisAula;
var that;


LocalAulaView = Backbone.View.extend({
    initialize: function (options) {
        this.render = _.bind(this.render, this);
        this.model.bind('change', this.render);
    },
    tagName: "tr",
    className: "localaula",
    bindings: {
        'input[name=st_localaula]': 'st_localaula'
    },
    render: function () {
        var variables = {
            st_localaula: this.model.get('st_localaula'),
            st_codigocatraca: this.model.get('st_codigocatraca'),
            id_catraca: this.model.get('id_catraca'),
            nu_maxalunos: this.model.get('nu_maxalunos'),
            id_entidade: this.model.get('id_entidade'),
            id: this.model.id
        };
        var tr;
        if (this.model.is_editing) {
            tr = _.template($("#local-aula-template-edit").html(), variables);
            this.populaSelectCatraca();
        } else {
            tr = _.template($("#local-aula-template-list").html(), variables);
        }
        this.el.innerHTML = tr;
        if (!this.model.id) {
            $(this.el).find('.delete-link').hide();
        }
        return this;
    },
    populaSelectCatraca: function () {
        var that = this;
        var collection = new CatracaCollection();
        collection.url += '?bl_ativo=1&id_situacao=138';
        collection.fetch({
            beforeSend: function () {
                loading();
            },
            complete: function () {
                //that.$el.find('select[name="id_catraca_list"]').prepend('<option value="">Selecione</option>');
                loaded();
            },
            success: function (result) {
                var view = new SelectView({
                    el: that.$el.find('select[name="id_catraca_list"]'),
                    collection: collection,
                    childViewOptions: {
                        optionLabel: 'st_codigocatraca',
                        optionValue: 'id',
                        optionSelected: that.model.get('id_catraca')
                    }
                });
                view.render();
            }
        })
    },
    delete: function () {
        that = this;
        bootbox.confirm("Tem certeza de que deseja apagar o registro?", function (result) {
            if (result) {
                that.model.destroy({});
                that.remove();
                $.pnotify({
                    title: 'Registro Removido',
                    text: 'O registro foi removido do banco de dados com sucesso!',
                    type: 'success'
                });
            }
        });
    },
    saveChanges: function () {
        that = this;
        if(this.verificaMaxAlunos()){
            this.model.set({
                'st_localaula': $(this.el).find('input[name=st_localaula]').val(),
                'st_codigocatraca': $(this.el).find('select[name=id_catraca_list]').find('option:selected').text(),
                'id_catraca': $(this.el).find('select[name=id_catraca_list]').val(),
                'nu_maxalunos': $(this.el).find('input[name=nu_maxalunos]').val()
            });
            this.model.save(null, {
                success: function (model, response) {
                    that.toggleEdit();
                    that.render();
                    $.pnotify({
                        title: 'Registro Atualizado',
                        text: 'O registro foi atualizado com sucesso!',
                        type: 'success'
                    });
                },
                error: function (model, response) {
                    $.pnotify({
                        title: 'Erro ao atualizar o registro',
                        text: 'Houve um problema ao atualizar o registro, tente novamente',
                        type: 'error'
                    });
                }
            });
        }
    },
    toggleEdit: function () {
        this.oldSettings = this.model.toJSON();
        this.model.toggleEdit();
        this.render();
    },
    resetEdit: function () {
        this.model.set(this.oldSettings);
        this.toggleEdit();
        if (!this.model.id) {
            this.remove();
        }
    },
    verificaMaxAlunos: function(){
        if($('#nu_maxalunos').val()>9999 || $('#nu_maxalunos').val() <= 0 ){
            $.pnotify({
                title: 'Erro ao editar registro',
                text: 'Verifique o número máximo de alunos! Somente números positivos de 1 até 9999.',
                type: 'error'
            });
            return false;
        }
        return true;
    },

    somenteNumeros: function (e) {
        //teclas adicionais permitidas (tab,delete,backspace,setas direita e esquerda)
        keyCodesPermitidos = new Array(8, 9, 37, 39, 46);
        //numeros e 0 a 9 do teclado alfanumerico
        for (x = 48; x <= 57; x++) {
            keyCodesPermitidos.push(x);
        }

        //numeros e 0 a 9 do teclado numerico
        for (x = 96; x <= 105; x++) {
            keyCodesPermitidos.push(x);
        }

        //Pega a tecla digitada
        keyCode = e.which;
        //Verifica se a tecla digitada é permitida
        if ($.inArray(keyCode, keyCodesPermitidos) != -1) {
            return true;
        }
        return false;
    },

    events: {
        'click .edit-link': 'toggleEdit',
        'click .save-link': 'saveChanges',
        'click .delete-link': 'delete',
        'click .cancel-link': 'resetEdit',
        'keydown #nu_maxalunos': 'somenteNumeros'
    }
});

var LocaisDeAulaView = Backbone.View.extend({
    initialize: function (options) {
        _(this).bindAll('add', 'remove');

        if (!options.childViewConstructor)
            throw "no child view constructor provided";
        if (!options.childViewTagName)
            throw "no child view tag name provided";

        this._childViewConstructor = options.childViewConstructor;
        this._childViewTagName = options.childViewTagName;

        this._childViews = [];

        this.collection.each(this.add);

        this.collection.bind('add', this.add);
        this.collection.bind('remove', this.remove);
    },
    add: function (model) {
        var childView = new this._childViewConstructor({
            tagName: this._childViewTagName,
            model: model
        });

        this._childViews.push(childView);

        if (this._rendered) {
            $(this.el).append(childView.render().el);
        }
    },
    remove: function (model) {
        var viewToRemove = _(this._childViews).select(function (cv) {
            return cv.model === model;
        })[0];
        this._childViews = _(this._childViews).without(viewToRemove);
        if (this._rendered)
            $(viewToRemove.el).remove();
    },
    render: function () {
        var that = this;
        this._rendered = true;

        $(this.el).empty();

        _(this._childViews).each(function (childView) {
            $(that.el).append(childView.render().el);
        });

        return this;
    }
});

function novoLocalDeAula() {
    var nLa = new LocalAula();
    nLa.toggleEdit();
    listaDeLocaisView.add(nLa);
}

locaisaula.fetch({
    success: function () {

        listaDeLocaisView = new LocaisDeAulaView({
            collection: locaisaula,
            childViewConstructor: LocalAulaView,
            childViewTagName: 'tr',
            el: $('#tabela-local-aula')
        });
        loaded();
        listaDeLocaisView.render();
    }
});
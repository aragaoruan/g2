/**
 * Verifica se o usuario tem permissão para acessar algumas regiões específicas
 * @type {boolean}
 */
var permissao = false;
VerificarPerfilPermissao(55, 816, true).done(function (response) {
    if (response.length === 1) {
        permissao = true;
    }
});

var calculoView,
    st_tid,
    thatNuValorAPagar = 0;

/**
 * Collection generica para encapsular os dados
 */
var CursosCollection = Backbone.Collection.extend({
    model: Backbone.Model,
    url: "/cancelamento-pos-graduacao/retornar-materia"
});

var MatriculasCollection = Backbone.Collection.extend({
    model: Backbone.Model,
    url: "cancelamento-pos-graduacao/retornar-matriculas-por-cancelamento"
});

var thatMatricula = [];

var responseError = function (collection, response) {
    var type = "error",
        title = "Erro!";

    if (response.status === 404) {
        type = "warning";
        title = "Atenção!";
    }
    $.pnotify({
        "type": type,
        "title": title,
        "text": response.responseText
    });
};


var ModalAbaCalculo = Marionette.CompositeView.extend({
    template: "#modal-calculo",
    tagName: "div",
    className: "modal hide fade",
    initialize: function (options) {
        this.parentView = options.parent;
        return this;
    },
    ui: {
        justificativaCalculo: "#txt-calculo-justificativa",
        gravarJustificativaCalculo: "#gravar-justificativa-calculo",
        form: "#form-justificativa-calculo",
        btnCancelar: ".btn-danger",
        numCaracteres: "#num-caracteres",
        numDigitados: "#num-digitados"
    },
    events: {
        "click @ui.gravarJustificativaCalculo": "gravarJustificativaCalculo",
        "click @ui.btnCancelar": "cancelar",
        "keyup @ui.justificativaCalculo": "contarCaracteres"
    },
    onRender: function () {
        this.$el.modal("show");
    },
    contarCaracteres: function () {
        var limite = 255;
        var caracteresDigitados = this.ui.justificativaCalculo.val().length;
        var caracteresRestantes = limite - caracteresDigitados;

        if (caracteresDigitados >= limite) {
            this.ui.justificativaCalculo.val(this.ui.justificativaCalculo.val().substr(0, limite));
        }
        this.ui.numCaracteres.text(caracteresRestantes);
        this.ui.numDigitados.text(caracteresDigitados);
    },
    cancelar: function () {
        this.model.set("bl_cancelamento", !this.model.get("bl_cancelamento"));
        // this.parentView.render();
        this.parentView._parent.calculaValorDisponibilizado(true);
    },
    gravarJustificativaCalculo: function () {

        if (this.ui.justificativaCalculo.val() === "") {
            $("#erro-justificativa").show().html("Campo Justificativa Obrigatorio.");

            $.pnotify({
                "type": 'danger',
                "title": 'Atenção',
                "text": 'Campo Justificativa Obrigatorio.'
            });
            return false;
        }
        if (this.ui.justificativaCalculo.val().length < 15) {
            $("#erro-justificativa").show().html("Minimo 15 caracteres");

            $.pnotify({
                "type": 'danger',
                "title": 'Atenção',
                "text": 'Minimo 15 caracteres.'
            });
            return false;
        }
        this.ui.form.ajaxSubmit({
            url: "cancelamento/salvar-justificatica-calculo-cancelamento",
            type: "POST",
            dataType: "json",
            data: {
                "id_matricula": this.model.get("id_matricula"),
                "bl_cancelamento": this.model.get("bl_cancelamento"),
                "id_disciplina": this.model.get("id_disciplina"),
                "st_disciplina": this.model.get("st_disciplina")
            },
            beforeSend: loading,
            complete: loaded, success: function (collection) {
                if (collection.tipo === 0) {
                    $.pnotify({
                        "type": collection.type,
                        "title": collection.title,
                        "text": collection.text
                    });
                } else {
                    $.pnotify({
                        "type": "success",
                        "title": "Sucesso",
                        "text": "Justificativa Salva com Sucesso!"
                    });
                }
                this.parentView._parent.calculaValorDisponibilizado(true);
                this.$el.modal("hide");
            }.bind(this),
            error: function (collection, response) {
                $.pnotify({
                    "type": 'error',
                    "title": 'Erro',
                    "text": response.responseText
                });

            }
        });
    }

});

/**
 * Item view das linhas das disciplinas
 */
var DisciplinasItemView = Marionette.ItemView.extend({
    template: "#template-row-disciplina",
    tagName: "tr",
    initialize: function () {
        if (this.model.get("bl_cancelamento") === null) {
            this.model.set("bl_cancelamento", true);
        }
        return this;
    },
    ui: {
        "checkCancelamento": "input[type='checkbox']"
    },
    events: {
        "change @ui.checkCancelamento": "toggleCancelamento"
    },
    modelEvents: {
        "change bl_cancelamento": function () {

        }
    },
    toggleCancelamento: function () {
        this.model.set("bl_cancelamento", this.ui.checkCancelamento.is(":checked"));
        this.modalCalculo();
    },
    modalCalculo: function () {
        calculoView.getRegion("modalRegion").show(new ModalAbaCalculo({
            model: this.model,
            parent: this
        }));
    }
});


/**
 * Composite view que renderiza cada curso
 */
var CursosComposite = Marionette.CompositeView.extend({
    template: "#template-cursos",
    childView: DisciplinasItemView,
    childViewContainer: ".table-disciplinas tbody",
    ui: {
        tooltipCalculosDisponibilizado: "#tooltipCalculoDisponibilizado"
    },
    initialize: function () {
        this.collection = new Backbone.Collection(this.model.get("disciplinas"));
        this.calculaValorDisponibilizado();

        calculoView.getRegion("regiaoValoresRescisorios")
            .show(new ValoresRescisoriosItemView({
                model: this.model,
                cursosComposite: this,
            }));

        return this;
    },
    onShow: function () {

        this.ui.tooltipCalculosDisponibilizado.popover({
            content: function () {
                return _.template($("#popover-calculos-disponibilizada").html());
            }
        });

    },
    calculaValorDisponibilizado: function (render) {
        var chDisponibilizada = 0;
        //percorre as models
        this.collection.each(function (model) {
            //verifica se o valor é true ou ainda é vazio
            if (model.get("bl_cancelamento") === true || model.get("bl_cancelamento") === null) {
                chDisponibilizada += parseInt(model.get("nu_cargahoraria"));
            }
        });
        //seta a nova carga horaria
        this.model.set("nu_chdisponibilizada", chDisponibilizada);

        //calcula o valor valor disponibilizado
        var valorDisponibilizado = (parseInt(this.model.get("nu_valortotalcurso"))
            / parseInt(this.model.get("nu_cargahorariaprojeto")) * parseInt(this.model.get("nu_chdisponibilizada")));

        //seta na model o valor
        this.model.set("nu_valordisponibilizado", valorDisponibilizado);

        //verifica se o parametro render foi passado para renderizar novamente a view
        if (typeof render !== "undefined" && render === true) {
            this.render();
            calculoView.renderizaCalculos();
        }
    }
});

/**
 * Composite view do array de curso
 */
var CursosDisciplinasComposite = Marionette.CompositeView.extend({
    template: "#template-cursos-disciplinas",
    childView: CursosComposite,
    childViewContainer: "#container-cursos",
    onShow: function () {
        return this;
    }
});


var CalculosItemView = Marionette.ItemView.extend({
    template: "#template-calculos-totais",
    cssClass: "alert",
    ui: {
        tooltipValorDevido: "#tooltipValorDevido"
    },
    onBeforeRender: function () {
        this.calcularValores();
    },
    onShow: function () {

        this.ui.tooltipValorDevido.popover({
            content: function () {
                return _.template($("#popover-valor-devido").html());
            }
        });

        this.alteraCorBarra();

        if (permissao) {

            var regiaoLancamento = this._parentLayoutView().getRegion("regiaoLancamentos");
            var regiaoRessarcimento = this._parentLayoutView().getRegion("regiaoRessarcimento");

            var negociacaoFinanceiraComposite = new NegociacaoFinanceiraComposite({
                model: this.model,
                collection: collectionVwVendaLancamento
            });

            var ressarcimentoItem = new RessarcimentoItemCancelamento({
                model: this.model,
                collection: collectionVwVendaLancamento
            });

            if (parseFloat(this.model.get("nu_valorapagar")) < 0) {
                regiaoLancamento.$el.hide();
                regiaoRessarcimento.show(ressarcimentoItem);
                regiaoRessarcimento.$el.show();

            } else {
                regiaoRessarcimento.$el.hide();
                regiaoLancamento.show(negociacaoFinanceiraComposite);
                regiaoLancamento.$el.show();
            }
        }

    },
    calcularValores: function () {
        var viewCursos = this._parentLayoutView().regiaoCursos.currentView;
        if (permissao) {
            var regiaoValoresRescisorios = this._parentLayoutView().regiaoValoresRescisorios.currentView.model;
        }
        var nuTotalCursos = 0;
        var nuTotalDisponibilizado = 0;
        var nuChProjeto = 0;
        var nuChDisponibilizada = 0;
        // var nuSaldoDevido = 0;

        if (viewCursos.collection.length) {
            viewCursos.collection.each(function (model) {
                nuTotalCursos += model.get("nu_valortotalcurso");
                nuTotalDisponibilizado += model.get("nu_valordisponibilizado");
                nuChProjeto += model.get("nu_cargahorariaprojeto");
                nuChDisponibilizada += model.get("nu_chdisponibilizada");
                //TODO Ver se é esse o calculo para o saldo devido
                //Saldo devido por curso
                // var saldoDevidoPorCurso = model.get("nu_valordisponibilizado") - parseInt(this.model.get("nu_valor"));
                // nuSaldoDevido += saldoDevido;
            }.bind(this));
        }

        this.model.set({
            "nu_totalcursos": nuTotalCursos.toFixed(2),
            "nu_totaldisponibilizado": nuTotalDisponibilizado.toFixed(2),
            "nu_valorapagar": (nuTotalDisponibilizado - parseInt(this.model.get("nu_valor"))).toFixed(2),
            "nu_totalchprojeto": nuChProjeto,
            "nu_totalchdisponibilizada": nuChDisponibilizada,
            // "id_venda": regiaoValoresRescisorios.get("id_venda")
        });

        if (permissao) {
            this.model.set("id_venda", regiaoValoresRescisorios.get("id_venda"));
        }
        thatNuValorAPagar = (nuTotalDisponibilizado - parseInt(this.model.get("nu_valor"))).toFixed(2);

        /**
         * Essa função é necessaria para usar na hora do ressarcimento
         */
        this.retornaTID(this.model);

    },
    alteraCorBarra: function () {
        var cssClass = "alert-info";
        if (this.model.get("nu_valorapagar") < 0) {
            cssClass = "alert-error";
        }
        this.$el.addClass(cssClass);
    },
    retornaTID: function (model) {
        $.ajax({
            url: "/cancelamento-pos-graduacao/retorna-tid",
            method: "GET",
            data: {"id_venda": this.model.get("id_venda")},
            success: function (retorno) {
                model.set("st_tid", retorno);
            },
            error: function () {
                $.pnotify({
                    "type": "error",
                    "title": "Erro",
                    "text": "Não foi possível fazer a busca ao TID"
                });
            }
        });
    }
});


var ValoresRescisoriosItemView = Marionette.ItemView.extend({
    template: "#template-valores-rescisorios",
    ui: {
        dataCancelamento: "#data-pedido-cancelamento",
        dataReferencia: "#data-referencia",
        restituicaoTotal: "#restituicao-total"
    },
    events: {
        "click @ui.restituicaoTotal": "restituicaoTotal",
        "change @ui.dataCancelamento": "atualizarData"
    },
    initialize: function (options) {
        this.cursosComposite = options.cursosComposite
    },
    onShow: function () {

        //Função responsável por atualizar a data de referencia na tb_cancelamento
        this.atualizarDataReferencia();

        return this;
    },
    atualizarData: function () {
        this.model.set('dt_cancelamento', this.ui.dataCancelamento.val());
    },
    restituicaoTotal: function () {
        // this.interacaoRestituicaoTotal();
        var selected = this.ui.restituicaoTotal.is(':checked');
        _.each(this.cursosComposite._parent.children._views, function (model) {
            _.each(model.children._views, function (view) {
                view.model.set("bl_cancelamento", !selected);
                view.$el.find("input").removeAttr("checked");
            });
        });
        if (selected) {

            //desabilita todos os inputs do form de lançamentos
            $("#form-lancamentos :input").prop("disabled", true);

            //desabilita os campos, do dados de ressarcimento
            $("#nu_valorapagar").prop("disabled", true);
            $("#st_observacao").prop("disabled", true);

            this.ui.dataReferencia.val(this.model.get("dt_matricula"));

        } else {

            $("#form-lancamentos :input").prop("disabled", false);
            $("#nu_valorapagar").prop("disabled", false);
            $("#st_observacao").prop("disabled", false);

            this.ui.dataReferencia.val(this.model.get("dt_referencia"));

        }
        //Função responsável por atualizar a data de referencia na tb_cancelamento
        this.atualizarDataReferencia();
        //Função responsabel para atualizar a div dce calculo
        this.cursosComposite.calculaValorDisponibilizado(true);
        this.interacaoRestituicaoTotal();
        if (selected) {
            $("input[type='checkbox']").prop("disabled", true);
            this.ui.restituicaoTotal.prop("checked", selected);
        } else {
            $("input[type='checkbox']").prop("disabled", false);
            $("input[type='checkbox']").prop("checked", true);
            this.ui.restituicaoTotal.prop("checked", selected);
        }
        //Não desabilita o checkbox do restituição total
        this.ui.restituicaoTotal.prop("disabled", false);

    },
    atualizarDataReferencia: function () {
        $.post("cancelamento-pos-graduacao/atualizar-data-referencia", {
            'id_cancelamento': cancelamentoAtualModel.id,
            'dtReferencia': this.ui.dataReferencia.val()
        }, function (retorno) {
        })
    },
    interacaoRestituicaoTotal: function () {
        var selected = this.ui.restituicaoTotal.is(':checked');
        var msg = "";
        if (selected) {
            msg = "Restituição total ao aluno";
        } else {
            msg = "Desmarcado a restituição ao aluno";
        }
        var paramConcluirProcessoInteracoes = {
            matricula: this.model.attributes.id_matricula,
            cancelamento: cancelamentoAtualModel.id,
            mensagem: msg
        };
        $.post("cancelamento-pos-graduacao/salvar-interacoes", {params: paramConcluirProcessoInteracoes},
            function (retorno) {
            });

    }
});

var ViewCalculo = Marionette.LayoutView.extend({
    template: "#view-calculo",
    tagName: "div",
    className: "row-fluid",
    regions: {
        regiaoCursos: "#regiao-cursos-disciplinas",
        regiaoCalculoTotais: "#regiao-calculos-totais",
        modalRegion: "#modalCalculo",
        regiaoLancamentos: "#regiao-lancamentos",
        regiaoRessarcimento: "#regiao-ressarcimento",
        regiaoValoresRescisorios: "#regiao-valores-rescisorios"
    },
    ui: {
        btnSalvar: "#btn-salvar-calculo",
        btnDesistir: "#btn-desistir",
        btnFinalizar: "#btn-finalizar",
        btnConcluir: "#btn-concluir",
        btnExportarPdf: "#btn-exportar-pdf",
    },
    events: {
        "click @ui.btnSalvar": "salvarCalculo",
        "click @ui.btnConcluir": "concluirCancelamento",
        "click @ui.btnDesistir": "desistirCancelamento",
        "click @ui.btnFinalizar": "finalizarCancelamentoPos",
        "click @ui.btnExportarPdf": "exportarPdf"
    },
    initialize: function (options) {

        return this;

    },
    onShow: function () {
        this.idMatricula = [];
        calculoView = this;
        var matriculas = new MatriculasCollection();
        matriculas.fetch({
            "data": {
                "id_cancelamento": cancelamentoAtualModel.id
            },
            beforeSend: loading,
            complete: loaded,
            error: responseError,
            success: function (model) {
                matriculas.each(function (model) {
                    this.idMatricula.push(model.get('id_matricula'));
                }.bind(this));
                this.renderizaDisciplinas();
            }.bind(this)
        });
    },
    modelTotais: function () {
        var modelTotais = new Backbone.Model({"nu_valor": null});
        modelTotais.fetch({
            "url": "cancelamento-pos-graduacao/retornar-calculo-total-pago",
            "data": {
                "matriculas": this.idMatricula
            },
            beforeSend: loading,
            complete: function () {
                loaded();
                this.modelCalculos = modelTotais;
                this.renderizaCalculos();
            }.bind(this),
            error: responseError
        });

    },
    exportarPdf: function () {
        this.salvarCalculo();
        window.open("/cancelamento-pos-graduacao/pdf?id_cancelamento=" + cancelamentoAtualModel.id);
    },
    renderizaCalculos: function () {
        this.getRegion("regiaoCalculoTotais")
            .show(new CalculosItemView({
                model: this.modelCalculos
            }));
    },
    renderizaDisciplinas: function () {

        var collectionCursos = new CursosCollection();
        collectionCursos.fetch({
            async:false,
            "data": {
                "id_matricula": this.idMatricula
            },
            beforeSend: loading,
            complete: loaded,
            success: function (collection) {
                this.getRegion("regiaoCursos")
                    .show(new CursosDisciplinasComposite({
                        collection: collection
                    }));
                this.modelTotais();
                this.desbloquearBotoes();
            }.bind(this),
            error: responseError
        });
    },
    desbloquearBotoes: function () {
        this.ui.btnDesistir.removeAttr("disabled");

        if (permissao) {
            this.ui.btnConcluir.removeAttr("disabled");
        }

        this.ui.btnExportarPdf.removeAttr("disabled");
        this.ui.btnSalvar.removeAttr("disabled");

    },
    salvarCalculo: function () {

        var cancelamento = new Cancelamento();
        var calculos = this.getRegion("regiaoCalculoTotais").currentView.model;

        if (permissao) {
            var valores = this.getRegion("regiaoValoresRescisorios").currentView.model;
            if (parseFloat(calculos.get("nu_valorapagar")) < 0) {
                var regiaoRessarcimento = this.getRegion("regiaoRessarcimento").currentView.model;
                cancelamento.set("regiaoRessarcimento", regiaoRessarcimento.toJSON()); // Parametro para salvar Dados bancarios.)
            }

            cancelamento.set("dt_cancelamento", valores.get('dt_cancelamento')); //Data do pedido do cancelamento

        }

        var nuValorHoraAula = calculos.get("nu_totalcursos") / calculos.get("nu_totalchprojeto");

        if (permissao) {
            if (parseFloat(calculos.get("nu_valorapagar")) < 0) {
                if (!this.verificarDadosResarcimento())
                    return false;
            }

            if (valores.get('dt_cancelamento') === null || valores.get('dt_cancelamento') === '') {
                $.pnotify({
                    "type": "warning",
                    "title": "Atenção",
                    "text": "Adicione a data do pedido do cancelamento !"
                });
                return false;
            }
        }

        cancelamento.save({
            "id_cancelamento": cancelamentoAtualModel.id ,
            "cargaHoraria": calculos.get("nu_totalchprojeto"),//somatório de carga horaria dos cursos
            "valorHoraAula": nuValorHoraAula.toFixed(2),//soma dos cursos dividito pela soma das cargas horaria
            "nu_valorNegociado": calculos.get("nu_totalcursos"),// somatorio dos valores dos cursos
            "cargaHorariaCursada": calculos.get("nu_totalchdisponibilizada"),//somatório de carga horaria disponibilizada
            "totalUtilizado": calculos.get("nu_totaldisponibilizado"),// somatorio dos valores disponibilizados
            "nu_valorDiferenca": calculos.get("nu_valorapagar"),// saldo devido
        }, {
            url: "cancelamento/salvar-calculos",
            beforeSend: loading,
            complete: function () {

                var disciplinas = [];
                _.each(
                    this.getRegion("regiaoCursos").currentView.children._views,
                    function (view) {
                        disciplina = {
                            'id_matricula': view.model.get('id_matricula'),
                            'diciplinas': view.collection.toJSON(),
                        }
                        disciplinas.push(disciplina);
                    }
                );

                $.ajax({
                    url: 'cancelamento-pos-graduacao/atualiza-matricula-disciplina',
                    type: 'POST',
                    data: {'disciplinas': disciplinas},
                    dataType: 'json',
                    success: function (model) {
                        $.pnotify({
                            "type": model.tipo,
                            "title": model.title,
                            "text": model.mensagem
                        });
                        loaded();
                    },
                    error: function () {
                    },
                    complete: function () {
                    }
                });
            }.bind(this),
            error: responseError
        });
    },
    verificarDadosResarcimento: function () {
        var regiaoRessarcimento = this.getRegion("regiaoRessarcimento").currentView.model;

        if (regiaoRessarcimento.get('st_tid') === null) {
            if (typeof regiaoRessarcimento.get('id_banco') === "undefined") {
                $.pnotify({
                    "type": "warning",
                    "title": "Atenção",
                    "text": "Selecione um banco!"
                });
                return false;
            }

            if (typeof regiaoRessarcimento.get('nu_agencia') === "undefined") {
                $.pnotify({
                    "type": "warning",
                    "title": "Atenção",
                    "text": "Adicione agência!"
                });
                return false;
            }

            if (typeof regiaoRessarcimento.get('nu_conta') === "undefined") {
                $.pnotify({
                    "type": "warning",
                    "title": "Atenção",
                    "text": "Adicione o N° da conta!"
                });
                return false;
            }
        }
        return true;
    },
    concluirCancelamento: function () {

        var regiaoValoresRescisorios = this.getRegion("regiaoValoresRescisorios").currentView.model;
        var idMatricula = this.idMatricula;
        modelVendaAditivo.set("id_matriculaorigem", idMatricula);

        //Monta um objeto com os parametros
        var data = {
            id_matriculaorigem: idMatricula,
            lancamentos: [],
            venda_aditivo: null,
            id_venda: regiaoValoresRescisorios.get("id_venda"),
            lancamentosExcluidos: lancamentosExcluidos,
            cancelamento: cancelamentoAtualModel.id
        };

        if (parseFloat(thatNuValorAPagar) < 0) {

            //formata a modelo de VendaAditivo antes de setar os valores
            modelVendaAditivo.clear();

            //seta os valores para tb_vendaAditivo
            modelVendaAditivo.set({
                nu_valorapagar: parseFloat(thatNuValorAPagar),
                st_observacao: $("#st_observacao").val()
            });

            //Monta um objeto com os parametros
            data.venda_aditivo = modelVendaAditivo.toJSON();
        } else if (parseFloat(thatNuValorAPagar) > 0) {

            //lançamentos
            var lancamentos = calculoView.getRegion("regiaoLancamentos").currentView.collection;
            data.lancamentos = lancamentos.toJSON();
            data.venda_aditivo = modelVendaAditivo.toJSON();

        }

        var paramConcluirProcessoInteracoes = {
            matricula: this.idMatricula,
            cancelamento: cancelamentoAtualModel.id,
            mensagem: this.montarMensagemConcluirProcesso()
        };

        $.ajax({
            url: "cancelamento-pos-graduacao/salvar-lancamentos",
            method: "POST",
            data: data,
            beforeSend: loading,
            complete: loaded,
            success: function (retorno) {
                $.pnotify({
                    "type": retorno.type,
                    "title": retorno.title,
                    "text": retorno.mensagem
                });

                $.post("cancelamento-pos-graduacao/salvar-interacoes", {params: paramConcluirProcessoInteracoes},
                    function (retorno) {
                    });
                $("#btn-finalizar").attr("disabled", false);
                $("#btn-concluir").attr("disabled", true);
                $("#btn-salvar-calculo").attr("disabled", true);
                $("#form-div-lancamento").hide();
            },
            error: function (erro) {
                $.pnotify({
                    "type": erro.type,
                    "title": erro.title,
                    "text": erro.mensagem
                });
            },

        });


    },
    desistirCancelamento: function () {
        var id_matricula = this.idMatricula;

        var paramDesistirProcessoInteracoes = {
            matricula: id_matricula,
            cancelamento: cancelamentoAtualModel.id,
            mensagem: this.montarMensagemDesistirProcesso()
        };

        bootbox.confirm("Tem certeza que deseja desistir do processo de cancelamento?", function (result) {
            if (result) {
                $.ajax({
                    url: "cancelamento-pos-graduacao/desistir-cancelamento",
                    method: "POST",
                    data: {
                        "id_matricula": id_matricula,
                        "id_cancelamento": cancelamentoAtualModel.id
                    },
                    beforeSend: loading,
                    complete: loaded,
                    success: function (retorno) {
                        $.pnotify({
                            "type": retorno.type,
                            "title": retorno.title,
                            "text": retorno.mensagem
                        });

                        $.post("cancelamento-pos-graduacao/salvar-interacoes", {params: paramDesistirProcessoInteracoes},
                            function (retorno) {
                            });
                    },
                    error: function (erro) {
                        $.pnotify({
                            "type": erro.tipo,
                            "title": erro.title,
                            "text": erro.mensagem
                        });
                    },

                });
            }
        });
    },
    finalizarCancelamentoPos: function () {
        var id_matricula = this.idMatricula;

        var paramFinalizarProcessoInteracoes = {
            matricula: id_matricula,
            cancelamento: cancelamentoAtualModel.id,
            mensagem: this.montarMensagemFinalizarProcesso()
        };

        bootbox.confirm("Tem certeza que deseja finalizar o processo de cancelamento?", function (result) {
            if (result) {
                $.ajax({
                    url: "cancelamento-pos-graduacao/finalizar-cancelamento",
                    method: "POST",
                    data: {
                        "id_matricula": id_matricula,
                        "id_cancelamento": cancelamentoAtualModel.id
                    },
                    beforeSend: loading,
                    complete: loaded,
                    success: function (retorno) {
                        $.pnotify({
                            "type": retorno.type,
                            "title": retorno.title,
                            "text": retorno.mensagem
                        });

                        $.post("cancelamento-pos-graduacao/salvar-interacoes", {params: paramFinalizarProcessoInteracoes},
                            function (retorno) {
                            });
                        $("#form-div-lancamento").hide();
                        $("#btn-finalizar").attr("disabled", true);
                        $("#btn-concluir").attr("disabled", true);
                        $("#btn-exportar-pdf").attr("disabled", true);
                        $("#btn-salvar-calculo").attr("disabled", true);
                    },
                    error: function (erro) {
                        $.pnotify({
                            "type": erro.tipo,
                            "title": erro.title,
                            "text": erro.mensagem
                        });
                    },

                });
            }
        });
    },
    //Essa função monta uma mensagem que irá salvar no logcancelamento. Para aparecer na aba interações.
    montarMensagemConcluirProcesso: function () {
        var mensagem = "";
        mensagem += "O processo foi concluído pelo atendente: " + G2S.loggedUser.get("nomeUsuario");
        mensagem += "\n no dia: " + dateToPtBr(new Date());

        return mensagem;
    },
    //Essa função monta uma mensagem que irá salvar no logcancelamento. Para aparecer na aba interações.
    montarMensagemDesistirProcesso: function () {
        var mensagem = "";
        mensagem += "A evolução do processo foi alterada para \"desistiu\" pelo atendente: " + G2S.loggedUser.get("nomeUsuario");
        mensagem += "\n no dia: " + dateToPtBr(new Date());

        return mensagem;
    },//Essa função monta uma mensagem que irá salvar no logcancelamento. Para aparecer na aba interações.
    montarMensagemFinalizarProcesso: function () {
        var mensagem = "";
        mensagem += "A evolução do processo foi alterada para \"finalizado\" pelo atendente: " + G2S.loggedUser.get("nomeUsuario");
        mensagem += "\n no dia: " + dateToPtBr(new Date());

        return mensagem;
    }
});
var REGIAO = {
    GERAL: 1,
    CALCULO: 2,
    INTERACOES: 3
};
var collectionMotivos = null;
var NovoCancelamentoModel = Backbone.Model.extend({
    url: "/api/cancelamento",
    idAttribute: "id_cancelamento",
    defaults: {
        "bl_cancelamentofinalizado": "",
        "bl_cartagerada": "",
        "dt_cadastro": "",
        "dt_finalizacao": "",
        "dt_solicitacao": "",
        "id_evolucao": "",
        "id_ocorrencia": "",
        "id_usuario": "",
        "id_usuariocadastro": G2S.loggedUser.get("id_usuario"),
        "nu_cargahoraria": "",
        "nu_cargahorariacursada": "",
        "nu_diascancelamento": "",
        "nu_multaporcentagemcarta": "",
        "nu_multaporcentagemdevolucao": "",
        "nu_totalutilizado": "",
        "nu_valorbruto": "",
        "nu_valorcarta": "",
        "nu_valordevolucao": "",
        "nu_valordiferenca": "",
        "nu_valorhoraaula": "",
        "nu_valormaterial": "",
        "nu_valormultacarta": "",
        "nu_valormultadevolucao": "",
        "nu_valornegociado": "",
        "nu_valortotal": "",
        "nu_valorutilizadomaterial": "",
        "st_observacao": "",
        "st_observacaocalculo": "",
        "bl_concluir": "",
        "bl_finalizar": ""
    }
});

var cancelamentoAtualModel = new NovoCancelamentoModel();

var matriculasCancelamentoAtual = [];

var objOcorrenciaCancelamento = JSON.parse(localStorage.getItem("ocorrencia_cancelamento"));

var LayoutPrincipal = Marionette.LayoutView.extend({
    template: "#template-layout-principal",
    tagName: "div",
    className: "container-fluid tela-cancelamento-pos",
    regions: {
        regiaoGeral: "#regiao-geral",
        regiaoCalculo: "#regiao-calculo",
        regiaoInteracoes: "#regiao-interacoes"
    },
    ui: {
        "aba": "ul.nav-tabs li a"
    },
    events: {
        "click @ui.aba": function (e) {
            var regicao = this.$el.find(e.currentTarget).data("regiao");
            if (regicao !== REGIAO.GERAL && !this.$el.find(e.currentTarget).parent().hasClass("active")) {
                this.renderizaRegiao(regicao);
            }
        },
        "click #btn-render-calculo": function () {
            if ($("#textarea-observacao").length > 255) {
                $.pnotify({
                    title: "Erro",
                    type: "error",
                    text: "As observações devem ter no máximo 255 caracteres."
                });
                return false;
                // } else {
                // cancelamentoAtualModel.set("st_observacao", $("#textarea-observacao").val());
            }

            if (!$("#form-cancelamento input:checked").length) {
                $.pnotify({
                    title: "Erro",
                    type: "error",
                    text: "É obrigatório selecionar um ou mais motivos."
                });
                return false;
            }

            this.salvarCancelamento();

        },
        "click #btn-cancelar": function () {
            bootbox.confirm("Deseja realmente cancelar o processo?", function (response) {
                if (response) {
                    if (viewGeral.regiaoDadosCancelamento.hasView()) {
                        viewGeral.regiaoDadosCancelamento.empty();
                    }
                    $("#btnClearAutocomplete").click();
                    $("#lista-processos-cancelamento-combobox").find("option:selected").prop("selected", false);
                }
            });
        }
    },
    onShow: function () {
        componenteAutoComplete.main({
            model: VwPesquisarPessoa,
            element: $("#autoComplete")
        }, function () {
            //OnFix
            this.renderizaRegiao(REGIAO.GERAL);
        }.bind(this), function () {
            //OnClear
            this.resetarTodasAsRegioes();
        }.bind(this));

    },
    verificaOcorrenciaCancelamentoStorage: function () {
        if (!objOcorrenciaCancelamento) {
            return false;
        }
        return true;
    },
    resetarTodasAsRegioes: function () {
        this.regiaoGeral.reset();
        this.regiaoCalculo.reset();
        this.regiaoInteracoes.reset();

        $("#tab-geral").addClass("active");
        $("#tab-calculo").removeClass("active").addClass("disabled bs2-disabled-tab");
        $("#tab-interacoes").removeClass("active").addClass("disabled bs2-disabled-tab");
        $("#tab-content-regiao-calculo").removeClass("active");
        $("#tab-content-regiao-interacoes").removeClass("active");
        return true;
    },
    renderizaRegiao: function (regiao) {
        loading();
        switch (regiao) {
            case REGIAO.GERAL:
                viewGeral = new ViewGeral();
                this.regiaoGeral.show(viewGeral);
                $("#tab-content-regiao-geral").addClass("active");
                break;

            case REGIAO.CALCULO:
                this.regiaoCalculo.show(new ViewCalculo());
                $("#tab-interacoes").removeClass("disabled bs2-disabled-tab");
                $("#tab-geral").removeClass("active");
                $("#tab-interacoes").removeClass("active");
                $("#tab-calculo").removeClass("disabled bs2-disabled-tab").addClass("active");
                $("#tab-content-regiao-geral").removeClass("active");
                $("#tab-content-regiao-interacoes").removeClass("active");
                $("#tab-content-regiao-calculo").addClass("active");
                break;

            case REGIAO.INTERACOES:
                this.regiaoInteracoes.show(new ViewInteracoes());
                $("#tab-geral").removeClass("active");
                $("#tab-calculo").removeClass("active");
                $("#tab-interacoes").removeClass("disabled bs2-disabled-tab");
                $("#tab-content-regiao-geral").removeClass("active");
                $("#tab-content-regiao-calculo").removeClass("active");
                $("#tab-content-regiao-interacoes").addClass("active");
                break;

            default:
                return 0;
        }

        loaded();

    },
    salvarCancelamento: function () {
        cancelamentoAtualModel.save({
            id_usuario: componenteAutoComplete.getFixModel().id_usuario
        }, {
            beforeSend: loading,
            complete: loaded,
            success: function () {
                this.salvarCancelamentoNaMatricula();
            }.bind(this)
        });
    },
    salvarMotivosNoProcesso: function () {


        var motivos = [];
        collectionMotivos.forEach(function (model) {
            var changed = model.changedAttributes();
            if (changed) {
                motivos.push({
                    "id_motivo": model.get("id_motivo"),
                    "bl_ativo": model.get("checked") ? 1 : 0
                });
            }
        });

        if (!motivos.length) {
            loading();
            this.renderizaRegiao(REGIAO.CALCULO);
            loaded();
            return false;
        }
        $.ajax({
            url: "/cancelamento-pos-graduacao/salvar-motivos-processo/",
            data: {
                "id_cancelamento": cancelamentoAtualModel.id,
                "motivos": motivos
            },
            beforeSend: loading,
            complete: loaded,
            method: "POST",
            success: function () {
                this.renderizaRegiao(REGIAO.CALCULO);
            }.bind(this)
        });

    },
    salvarCancelamentoNaMatricula: function () {
        var matriculas = [];
        _.each(matriculasCancelamentoAtual, function (matricula) {
            matriculas.push(matricula.id_matricula);
        });
        if (cancelamentoAtualModel.id) {
            $.ajax({
                url: "/cancelamento-pos-graduacao/salvar-cancelamento-matriculas/",
                data: {
                    id_matriculas: matriculas,
                    id_cancelamento: cancelamentoAtualModel.id
                },
                beforeSend: loading,
                complete: loaded,
                success: function () {
                    this.salvarLogObservacao(matriculas);
                    this.salvarMotivosNoProcesso();

                }.bind(this)
            });
        }
    },
    salvarLogObservacao: function () {
        loading();
        var matriculasObservacao = [];
        _.each(matriculasCancelamentoAtual, function (matricula) {
            matriculasObservacao.push(matricula.id_matricula);
        });

        var msg = "Observação: " + cancelamentoAtualModel._previousAttributes.st_observacao;
        var paramFinalizarProcessoInteracoes = {
            matricula: matriculasObservacao[0],
            cancelamento: cancelamentoAtualModel.id,
            mensagem: msg
        };

        if (cancelamentoAtualModel.get("change_observacao")) {
            $.post("cancelamento-pos-graduacao/salvar-interacoes", {params: paramFinalizarProcessoInteracoes},
                function (retorno) {
                    loaded();
                }.bind(this));
        }
    }

});

// Inicialização da Tela
var layoutPrincipal = new LayoutPrincipal();
G2S.show(layoutPrincipal);
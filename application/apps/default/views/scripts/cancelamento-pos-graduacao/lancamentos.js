/**
 * Array com tipos de lançamentos baseados na tb_lancamentovenda na coluna bl_entrada
 */
var TiposLancamentos = [
    {
        id: 1,
        name: 'Entrada',
        bl_entrada: 1
    },
    {
        id: 2,
        name: 'Parcela',
        bl_entrada: 0
    }
];

/**
 * Essa variavel é declarada com scopo global, pois irei usar ela em outra função, quando for concluir os processos.
 * @type {Array}
 */
var lancamentosExcluidos = [];

/**
 * VARIAVEIS GLOBAIS
 */

var ressarcimentoInit;

var collectionMeioPagamento,
    collectionCampanha,
    collectionAtendente,
    collectionResponsavelFinanceiro,
    collectionVwVendaLancamento;


/**
 * MODELS
 */
var ModelMeioPagamento = Backbone.Model.extend({
    idAttribute: 'id_meiopagamento',
    defaults: {
        'id_meiopagamento': null,
        'st_meiopagamento': ''
    }
});

var ModelCampanha = Backbone.Model.extend({
    idAttribute: 'id_campanhacomercial',
    defaults: {
        'id_campanhacomercial': null,
        'st_campanhacomercial': '',
        'id_tipodesconto': '',
        'nu_valordesconto': ''
    }
});

var ModelAtendente = Backbone.Model.extend({
    idAttribute: 'id_atendente',
    defaults: {
        'id_atendente': null,
        'st_atendente': ''
    }
});

var ModelResponsavelFinanceiro = Backbone.Model.extend({
    idAttribute: 'id_usuario',
    defaults: {
        'id': '',
        'st_cpf': '',
        'st_nomecompleto': '',
        'st_cnpj': '',
        'st_tiporesponsavel': 'Fisica'
    }
});

/**
 * COLLECTION
 */
var MeioPagamentoCollection = Backbone.Collection.extend({
    model: ModelMeioPagamento,
    url: '/default/transfere-projeto/get-meio-pagamento'
});

var CampanhaCollection = Backbone.Collection.extend({
    model: ModelCampanha,
    url: '/default/transfere-projeto/get-campanha'
});

var AtendenteCollection = Backbone.Collection.extend({
    model: ModelAtendente,
    url: '/default/transfere-projeto/get-atendente'
});

var ResponsavelFinanceiroCollection = Backbone.Collection.extend({
    model: ModelResponsavelFinanceiro
});

var VwVendaLancamentoCollection = Backbone.Collection.extend({
    model: VwVendaLancamento,
    url: '/api/vw-venda-lancamento'
});

var TipoLancamentoCollection = new Backbone.Collection(TiposLancamentos);

var BancoCollection = Backbone.Collection.extend({
    model: Banco,
    url: '/default/util/get-banco'
});

/**
 * INSTÂNCIAS DE COLLECTIONS
 */
collectionMeioPagamento = new MeioPagamentoCollection();
collectionCampanha = new CampanhaCollection();
collectionAtendente = new AtendenteCollection();
collectionResponsavelFinanceiro = new ResponsavelFinanceiroCollection();
collectionVwVendaLancamento = new VwVendaLancamentoCollection();

/**
 * INSTÂNCIAS DE MODEL
 */
modelVendaAditivo = new VendaAditivo();

/**
 ** RESSARCIMENTO
 */

/**
 * ItemView para lista de lançamentos vazio
 * @type {ItemView}
 */
var RessarcimentoItemCancelamento = Marionette.ItemView.extend({
    template: '#ressarcimento-composite',
    ui: {
        nu_valorapagar: "#nu_valorapagar",
        st_observacao: "#st_observacao",
        div_estorno: '#div_estorno',
        div_restituicao: '#div_restituicao',
        id_banco: "#id_banco",
        nu_agencia: "#nu_agencia",
        nu_conta: "#nu_conta",
        nu_operacao: "#nu_operacao",
        st_observacao: "#st_observacao",
        st_tid: "#st_tid"
    },
    events: {
        "change @ui.id_banco": "idBanco",
        "change @ui.nu_agencia": "nuAgencia",
        "change @ui.nu_conta": "nuConta",
        "change @ui.nu_operacao": "nuOperacao",
        "change @ui.st_observacao": "stObservacao"
    },
    onRender: function () {
        return this;
    },
    onShow: function () {
        if (this.model.get('st_tid')) {
            this.ui.div_estorno.removeClass('hide');
            this.ui.div_restituicao.addClass('hide');
        } else {
            this.ui.div_restituicao.removeClass('hide');
            this.ui.div_estorno.addClass('hide');
            this.loadComboBanco();
        }
    },
    idBanco: function () {
        this.model.set("id_banco", this.ui.id_banco.val());
    },
    nuAgencia: function () {
        this.model.set("nu_agencia", this.ui.nu_agencia.val());
    },
    nuOperacao: function () {
        this.model.set("nu_operacao", this.ui.nu_operacao.val());
    },
    nuConta: function () {
        this.model.set("nu_conta", this.ui.nu_conta.val());
    },
    stObservacao: function () {
        this.model.set("st_observacao", this.ui.st_observacao.val());
    },
    loadComboBanco: function () {
        var that = this;

        // Carregar combo Banco
        var collectionBanco = new BancoCollection();
        collectionBanco.fetch();
        this.ui.id_banco.html('<option value="">Selecione</option>');

        var BancoSelectView = new SelectView({
            el: that.$el.find('#id_banco'),
            collection: collectionBanco,
            childViewOptions: {
                optionLabel: 'st_nomebanco',
                optionValue: 'id_banco'
            }
        });
        BancoSelectView.render();
    }
});

/**
 * ItemView para lista de lançamentos vazio
 * @type {ItemView}
 */
var LancamentosEmptyView = Marionette.ItemView.extend({
    template: "#template-empty-lancamento",
    tagName: 'tr'
});

/**
 * ItemView para Itens de Lançamentos
 * @type {ItemView}
 */
var LancamentoItemView = Marionette.ItemView.extend({
    template: "#template-item-lancamento",
    tagName: 'tr',
    ui: {
        'dt_vencimento': 'input[name="dt_vencimento"]',
        'nu_valor': 'input[name="nu_valor"]',
        'id_responsavel': 'select[name="id_responsavel"]',
        'btn_datepicker': 'a[name="btn_datepicker"]',
        'btn_confirmar': '.btn-confirmar',
        'td_pagamento': '.td-pagamento',
        'btn_remover': '#btn_remover_lancamento_unico',
        'chk_um_lancamento': '.chk_um_lancamento'
    },
    modelEvents: {
        'change:bl_quitado': function () {
            this.render();
        }
    },

    initialize: function () {
        var tipo = 2;
        if (this.model.get('bl_entrada')) {
            tipo = 1
        }

        var tipoLancamento = TipoLancamentoCollection.where({id: tipo});
        if (typeof (tipoLancamento[0]) !== 'undefined') {
            this.model.set('st_tipolancamento', tipoLancamento[0].get('name'));
        }

        return this;
    },
    onRender: function () {

        if (this.model.get('st_urlpagamento')) {
            this.ui.td_pagamento.append($('<a href="' + this.model.get('st_urlpagamento') + '" target="_blank" class="btn btn-default" >' + this.model.get('st_meiopagamento') + '</a>'));
        }

        var valor = formatValueToShow(this.model.get('nu_valor'));
        this.ui.nu_valor.maskMoney({
            precision: 2,
            decimal: ",",
            thousands: "."
        }).val(valor).attr('placeholder', this.model.get('nu_valor'));

        // Se o primeiro item da collection com o mesmo numero do cartao for este mesmo itemview, mostrar botao remover.
        // Os comentarios abaixo são devido a mudança na tela, para remover os botões de excluir lançamentos
        var mesmoCartao = this.model.collection.findWhere({nu_cartao: this.model.get('nu_cartao')});
        if (mesmoCartao === this.model || this.model.get('id_meiopagamento') !== 1) {
            this.ui.chk_um_lancamento.removeClass('hide');
        }
        else {
            this.ui.chk_um_lancamento.addClass('hide');
        }

        return this;
    },
    removeLancamento: function () {

        var that = this;
        var id_meiopagamento = that.model.get('id_meiopagamento');

        /* Mensagem a ser utilizada no Bootbox.
         Caso cartão, informa sobre remoção múltipla dos lançamentos.
         */

        var msg = "Tem certeza que deseja remover este lançamento?";
        if (id_meiopagamento === 1)
            msg = "Tem certeza que deseja remover todos os lançamentos deste cartão?";

        bootbox.confirm(msg, function (result) {
            if (result) {

                var collection = that.model.collection;

                if (id_meiopagamento === 1) {

                    var nuCartaoAtual = Number(that.model.get('nu_cartao'));
                    var toRemove = [];

                    /* Ao remover uma serie de lançamentos (num mesmo cartao),
                     atualiza os models e as views, decrementando o número
                     do cartao.
                     */

                    collection.each(function (model) {
                        var nuCartao = Number(model.get('nu_cartao'));
                        if (nuCartao === nuCartaoAtual) {
                            model.set('bl_ativo', false);

                            // Lancamentos que terao o bl_ativo setado como false no banco
                            if (model.get('id_lancamento'))
                                lancamentosParaRemover.push(model.get('id_lancamento'));

                            // Lancamentos que serao removidos da collection se nao tiver id
                            toRemove.push(model);

                        } else if (nuCartao > nuCartaoAtual) {
                            model.set('nu_cartao', nuCartao - 1);
                        }
                    });

                    var firstModel = collection.findWhere({bl_ativo: true});
                    if (firstModel)
                        firstModel.set('bl_entrada', 1);
                    collection.remove(toRemove);

                } else {
                    that.model.set('bl_ativo', false);
                    if (that.model.get('id_lancamento'))
                        lancamentosParaRemover.push(that.model.get('id_lancamento'));
                    collection.remove(that.model);
                }

                $.pnotify({title: 'Sucesso!', text: 'Lançamentos removidos com sucesso!', type: 'success'});
            }
        });

    },
    changeValorLancamento: function () {
        var that = this;
        var valor = this.$el.find('input[name="nu_valor"]').val();
        this.model.set('nu_valor', valor);

        thatLancamento.calculaTotais();
    },
    changeDtVencimento: function () {
        var dt_vencimento = this.$el.find('input[name="dt_vencimento"]').val();
        this.model.set('dt_vencimento', dt_vencimento);
    },
    marcaLancamentosParaExcluir: function () {
        var that = this;
        //verifico se o check box esta marcado, se sim, marca a modelo para ser excluida
        if (this.ui.chk_um_lancamento.is(':checked')) {
            this.model.set({excluirLancamento: true});
            //se houver na modelo o atributo nu_cartao, quer dizer que este lançamento faz parte de um cartão
            if (this.model.get('nu_cartao')) {
                //vai marcar todas as modelos que fazem parte deste cartão
                _.each(this.model.collection.models, function (model, i) {
                    if (model.get('nu_cartao') === that.model.get('nu_cartao')) {
                        model.set({excluirLancamento: true});
                    }
                })
            }
        } else {
            //caso não esteja marcado o checkbox, remove o atributo da modelo e se fizer parte de um cartão
            //remove de todas os lançamentos deste cartão
            this.model.unset('excluirLancamento');
            if (this.model.get('nu_cartao')) {
                _.each(this.model.collection.models, function (model, i) {
                    if (model.get('nu_cartao') === that.model.get('nu_cartao')) {
                        model.unset('excluirLancamento');
                    }
                })
            }
        }
    },
    events: {
        'click .btn-danger': 'removeLancamento',
        'blur input[name="nu_valor"]': 'changeValorLancamento',
        'change input[name="dt_vencimento"]': 'changeDtVencimento',
        'change .chk_um_lancamento': 'marcaLancamentosParaExcluir'
    }
});

/**
 * CompositeView da Negociação Financeira
 * @type {CompositeView}
 */
var NegociacaoFinanceiraComposite = Marionette.CompositeView.extend({
    template: '#negociacao-financeira-composite',
    className: 'view-lancamentos',
    childViewContainer: '#lista-lancamentos-venda',
    childView: LancamentoItemView,
    emptyView: LancamentosEmptyView,
    valorFinal: 0,
    valorVenda: 0,
    totalEntrada: 0,
    totalParcela: 0,
    totalLancamento: 0,
    valorRestante: 0,
    idTipoDesconto: 0,
    valorDesconto: 0,
    collectionEvents: {
        remove: function () {
            this.render();
            this.calculaTotais();
            this.ui.nuTotal.val(this.valorRestante);
        }
    },
    modelEvents: {
        'change': 'setValorVenda'
    },
    setValorVenda: function () {
        this.valorVenda = this.model.get('nu_valorapagar');
        this.valorRestante = this.model.get('nu_valorapagar');
        this.render();
        this.calculaTotais();
    },
    ui: {
        nu_valorapagar: "#nu_valorapagar",
        st_observacao: "#st_observacao",
        id_meiopagamento: "#id_meiopagamento",
        id_campanhacomercial: "#id_campanhacomercial",
        tipo_desconto: "#tipo_desconto",
        nu_desconto: "#nu_desconto",
        tipoLancamento: "#tipoLancamento",
        nu_quantidade: "#nu_quantidade",
        id_atendente: "#id_atendente",
        id_responsavel: "#id_responsavel",
        nu_valor: '#nu_valorparcela',
        nuTotal: '#nu_total',
        nuPrazoParcela: '#nu_prazoparcela',
        dtPrimeiraParcela: '#dt_primeiraparcela',
        nuValorEntrada: '#nu_totalentrada',
        nuValorParcela: '#nu_totalparcela',
        nuValorRestante: '#nu_valorrestante',
        nuTotalLancamento: '#nu_totallancamento',
        tipoPrazo: '#tipo_prazoparcela',
        btnDtPicker: '#btn_datepicker',
        btnAddLancamento: '#btn-add-lancamento',
        btnProcurarResponsavel: '#btn-procurar-responsavel',
        chk_all_lancamentos: '#chk_all_lancamentos',
        btn_excluir_lancamentos: '#btn_excluir_lancamentos',
        divMeioPagamento: '#divMeioPagamento',
        divDataPrimeiraParcela: '#divDataPrimeiraParcela',
        divQtdLancamentos: '#divQtdLancamentos',
        divNuValorParcela: '#divNuValorParcela',
        divAtendente: '#divAtendente',
        divNuValorRestante: '#divNuValorRestante',
        divNuTotalLancamento: '#divNuTotalLancamento',
        tooltipCalculos: '#tooltipCalculos',
        btnConcluir: '#btn-concluir',
        btnFinalizar: '#btn-btn-finalizar',
    },
    initialize: function () {
        thatLancamento = this;
        this.valorFinal = formatValueToReal(this.model.get('nu_valorapagar'));

        collectionVwVendaLancamento.fetch({
            data: {
                "id_venda": this.model.get("id_venda"),
                "bl_quitado": "FALSE",
                "bl_cancelamento": "FALSE"
            }
        });

        return this;
    },
    onShow: function () {
        return this;
    },
    onRender: function () {
        var nu_valorapagar = this.model.get("nu_valorapagar");

        this.ui.nu_valorapagar.val(nu_valorapagar);
        this.ui.nu_valor.val(nu_valorapagar);
        this.ui.nuTotal.val(nu_valorapagar);
        this.populaSelectMeioPagamento();
        this.populaSelectCampanha();
        this.populaSelectAtendente();

        this.setResponsavel(componenteAutoComplete.getFixModel());

        this.populaSelectResponsavel();

        this.ui.tooltipCalculos.popover({
            content: function () {
                return _.template($("#popover-calculos-composite").html());
            }
        });

        //se o valor do aditivo for igual a 0 desabilita os campos do lançamento
        if (parseFloat(nu_valorapagar) === 0) {
            this.ui.id_meiopagamento.attr('disabled', 'disabled');
            this.ui.id_campanhacomercial.attr('disabled', 'disabled');
            this.ui.nu_quantidade.attr('disabled', 'disabled');
            this.ui.btnDtPicker.attr('disabled', 'disabled');
            this.ui.dtPrimeiraParcela.removeAttr('style');
            this.ui.id_atendente.attr('disabled', 'disabled');
            this.ui.btnAddLancamento.attr('disabled', 'disabled');
            this.ui.st_observacao.attr('disabled', 'disabled');
            this.ui.btn_excluir_lancamentos.attr('disabled', 'disabled');
            this.ui.chk_all_lancamentos.attr('disabled', 'disabled');
        }

        return this;
    },
    selecionaTodosLancamentos: function () {
        if (this.ui.chk_all_lancamentos.is(':checked')) {
            $('.chk_um_lancamento').prop('checked', true);
            _.each(this.collection.models, function (model, i) {
                model.set({excluirLancamento: true});
            });
        } else {
            $('.chk_um_lancamento').prop('checked', false);
            _.each(this.collection.models, function (model, i) {
                model.unset('excluirLancamento');
            });
        }
    },
    populaSelectMeioPagamento: function () {
        var that = this;

        // Carregar combo Meio de Pagamento
        collectionMeioPagamento.fetch();
        this.ui.id_meiopagamento.html('<option value="">Selecione</option>');

        var MeioPagamentoSelectView = new SelectView({
            el: that.$el.find('#id_meiopagamento'),
            collection: collectionMeioPagamento,
            childViewOptions: {
                optionLabel: 'st_meiopagamento',
                optionValue: 'id_meiopagamento'
            }
        });
        MeioPagamentoSelectView.render();
    },
    populaSelectCampanha: function () {
        var that = this;

        // Carregar combo Campanha
        collectionCampanha.fetch();
        this.ui.id_campanhacomercial.html('<option value="">Selecione</option>');

        var CampanhaSelectView = new SelectView({
            el: that.$el.find('#id_campanhacomercial'),
            collection: collectionCampanha,
            childViewOptions: {
                optionLabel: 'st_campanhacomercial',
                optionValue: 'id_campanhacomercial'
            }
        });
        CampanhaSelectView.render();
    },
    setResponsavel: function (dataThatAutoComplete) {

        //Instancia a model de responsavel financeiro e seta os valores
        if (dataThatAutoComplete) {
            collectionResponsavelFinanceiro.reset();
            var modeloResponsavel = new ModelResponsavelFinanceiro({
                id: dataThatAutoComplete.id_usuario,
                st_cpf: dataThatAutoComplete.st_cpf,
                st_nomecompleto: dataThatAutoComplete.st_nomecompleto,
                id_usuario: dataThatAutoComplete.id_usuario
            });
            //atribui a model na collection de responsaveis financeiros
            collectionResponsavelFinanceiro.add(modeloResponsavel);
        }
    },
    populaSelectAtendente: function () {
        var that = this;

        // Carregar combo Atendente
        collectionAtendente.fetch();
        this.ui.id_atendente.html('<option value="">Selecione</option>');

        var AtendenteSelectView = new SelectView({
            el: that.$el.find('#id_atendente'),
            collection: collectionAtendente,
            childViewOptions: {
                optionLabel: 'st_atendente',
                optionValue: 'id_atendente'
            }
        });
        AtendenteSelectView.render();
    },
    selecionaMeioPagamento: function () {

        var idMeioPagamento = this.ui.id_meiopagamento.val();

        if (idMeioPagamento === MEIO_PAGAMENTO.CARTAO_DEBITO) {
            this.ui.nu_quantidade.val(1).attr('disabled', 'disabled');
        } else {
            this.ui.nu_quantidade.val(1).removeAttr('disabled');
        }
    },
    selecionaCampanhaComercial: function () {
        var nu_desconto = 0;

        var valorapagar = formatValueToReal(this.model.get('nu_valorapagar'));

        var idCampanhaComercial = this.ui.id_campanhacomercial.val();

        if (idCampanhaComercial) {
            collectionCampanha.find(function (item) {
                if (item.get('id_campanhacomercial') === idCampanhaComercial) {
                    nu_desconto = item.get('nu_valordesconto');
                    idTipoDesconto = item.get('id_tipodesconto');
                }
            });
        }

        this.ui.nu_desconto.val(nu_desconto ? nu_desconto : 0);

        if (idTipoDesconto === TIPO_DESCONTO.VALOR) {
            this.ui.tipo_desconto.html('R$');
            this.valorDesconto = parseFloat(nu_desconto);
            this.valorFinal = valorapagar - (this.valorDesconto ? this.valorDesconto : 0);

        } else if (idTipoDesconto === TIPO_DESCONTO.PORCENTAGEM) {
            this.ui.tipo_desconto.html('%');
            this.valorDesconto = (valorapagar * (parseFloat(nu_desconto) / 100));
            this.valorFinal = valorapagar - this.valorDesconto;
        }

        this.calculaParcelas();

        this.ui.nuTotal.val(number_format(this.valorFinal, 2, ',', '.'));

    },
    populaSelectResponsavel: function () {
        var that = this;
        var responsavel = new SelectView({
            el: that.ui.id_responsavel,
            collection: collectionResponsavelFinanceiro,
            childViewOptions: {
                optionLabel: 'st_nomecompleto',
                optionValue: 'id',
                optionSelected: null
            }
        });
        responsavel.render();
    },
    calculaParcelas: function () {
        //pega o valor da venda
        var valorVenda = this.valorFinal;

        //quantidade de parcelas
        var qtdParcela = this.ui.nu_quantidade.val() ? parseInt(this.ui.nu_quantidade.val()) : 1;

        /*removendo a classe de erro caso o valor seja 0,
         já que o sistema vai colocar o valor 1 após a validação*/

        this.ui.divQtdLancamentos.removeClass('control-group error');

        //Valor total
        var valorParcela = 0;

        if (!this.verificaValorVenda()) {
            return false;
        }

        if (qtdParcela) {
            //calcula
            valorParcela = valorVenda / qtdParcela;
            if (valorParcela < 100) {
                this.ui.divNuValorParcela.addClass('control-group error');
                $.pnotify({
                    title: 'Atenção!',
                    text: 'O valor da parcela não pode ser menor que R$ 100,00.',
                    type: 'warning'
                });
            } else {
                this.ui.divNuValorParcela.removeClass('control-group error');
            }
            //coloca o valor no input
            this.ui.nu_valor.val(formatValueToShow(valorParcela.toFixed(2)));
            this.ui.divQtdLancamentos.removeClass('control-group error');
            return valorParcela;//retorna o valor
        } else {
            this.ui.divQtdLancamentos.addClass('control-group error');
            $.pnotify({
                title: 'Atenção!',
                text: 'O número de parcelas não pode ser igual a 0 (zero).',
                type: 'warning'
            });
            return false;
        }

    },
    verificaValorVenda: function () {
        if (this.model.get('nu_valorapagar')) {
            return true;
        } else {
            $.pnotify({title: 'Atenção!', text: 'Valor da venda não definido.', type: 'warning'});
            return false;
        }
    },
    adicionaLancamento: function () {

        var nu_valorapagar = this.ui.nu_valorapagar.val();
        var id_meiopagamento = this.ui.id_meiopagamento.val();
        var st_meiopagamento = this.ui.id_meiopagamento.find('option:selected').text();
        var id_campanhacomercial = this.ui.id_campanhacomercial.val();
        var nu_valordesconto = number_format(this.valorDesconto, 2, ',', '.');
        var id_tipolancamento = this.ui.tipoLancamento.val();
        var nu_parcelas = parseInt(this.ui.nu_quantidade.val());
        var id_atendente = this.ui.id_atendente.val();
        var nu_total = this.ui.nuTotal.val();
        var st_observacao = this.ui.st_observacao.val();
        var st_nomecompleto = this.ui.id_responsavel.find('option:selected').text();
        var id_usuariolancamento = this.ui.id_responsavel.val();
        var dt_vencimento = this.ui.dtPrimeiraParcela.val();
        var nuPrazoParcela = 1;//prazo pré-fixado na regra de negócio
        var tipoPrazo = 'meses';//prazo pré-fixado na regra de negócio
        var nu_valor = formatValueToReal(this.ui.nu_valor.val());
        nu_valor = parseFloat(nu_valor);
        nu_valor = nu_valor.toFixed(2);

        //valida campos obrigatórios
        if (this.validaSave(id_meiopagamento, dt_vencimento, id_atendente)) {

            var dt_object = new Date(dt_vencimento.split('/').reverse());

            /* nu_cartao armazena o maior número do cartão */
            var nu_cartao = 0;
            if (id_meiopagamento === 1) {
                $.each(this.collection.models, function (index, obj) {
                    var value = Number(obj.get('nu_cartao'));
                    if (value > nu_cartao)
                        nu_cartao = value;
                });
                nu_cartao++;
            } else {
                nu_cartao = null;
            }

            for (var i = 0; i < nu_parcelas; i++) {

                //pega a quantidade de elementos na collection
                var nu_ordem = this.collection.length;

                //REAJUSTA A DATA DE VENCIMENTO
                dt_vencimento = dt_object.toISOString().substr(0, 10).split('-').reverse().join('/');

                //a regra informa que sempre sera "Parcela"
                var st_tipolancamento = 'Parcela';

                //instancia a VwVendaLancamento para cada lançamento
                var modelVendaLancamento = new VwVendaLancamento();

                //seta os valores no modelo
                modelVendaLancamento.set({
                    id_meiopagamento: id_meiopagamento,
                    st_meiopagamento: st_meiopagamento,
                    bl_entrada: false,
                    nu_parcelas: nu_parcelas,//quantidade de parcelas
                    nu_valor: nu_valor,//valor da parcela
                    dt_vencimento: dt_vencimento,
                    nu_ordem: ++nu_ordem,
                    st_nomecompleto: st_nomecompleto,
                    st_tipolancamento: st_tipolancamento,
                    id_tipolancamento: id_tipolancamento,
                    nu_cartao: nu_cartao,
                    bl_ativo: 1,
                    id_usuariocadastro: id_atendente
                });

                var responsavel = collectionResponsavelFinanceiro.where({id: parseInt(id_usuariolancamento)});
                if (responsavel[0]) {
                    if (responsavel[0].get('st_tiporesponsavel') === 'Fisica') {
                        modelVendaLancamento.set('id_usuariolancamento', id_usuariolancamento);
                    } else {
                        modelVendaLancamento.set('id_entidadelancamento', id_usuariolancamento);
                    }
                }

                //adiciona a collection pai todos os lançamento
                this.collection.add(modelVendaLancamento);

                // Calcular quantos dias sera incrementado em cada parcela
                //Incrementa os dias na data de vencimento, para a proxima parcela
                switch (tipoPrazo) {
                    case 'dias':
                        dt_object.setDate(dt_object.getDate() + nuPrazoParcela);
                        break;
                    case 'meses':
                        dt_object.setMonth(dt_object.getMonth() + nuPrazoParcela);
                        break;
                    default:
                        break;
                }
            }

            //formata a modelo de VendaAditivo antes de setar os valores
            modelVendaAditivo.clear();

            //seta os valores para tb_vendaAditivo
            modelVendaAditivo.set({
                dt_cadastro: dateToPtBr(new Date()),
                nu_valorliquido: formatValueToReal(this.model.get("nu_valor")),
                dt_primeiraparcela: dt_vencimento,
                id_atendente: id_atendente,
                id_campanhacomercial: id_campanhacomercial,
                id_meiopagamento: id_meiopagamento,
                id_usuario: componenteAutoComplete.getFixModel().id_usuario,
                id_venda: this.model.get("id_venda"),
                nu_desconto: formatValueToReal(nu_valordesconto),
                nu_quantidadeparcelas: nu_parcelas,
                nu_valorapagar: formatValueToReal(nu_valorapagar),
                nu_valortransferencia: formatValueToReal(nu_total),
                st_observacao: st_observacao
            });

            this.calculaTotais();

        } else {
            return false;
        }
    },
    calculaTotais: function () {
        //cria as variaveis
        //pega o valor da venda
        var that = this;
        var valorVenda = parseFloat(that.valorFinal.toFixed(2));
        var totalLancamento = 0;
        var totalEntrada = 0;
        var totalParcela = 0;
        var valorRestante = 0;

        //percorre a collection
        if (this.collection.length) {
            this.collection.each(function (model, i) {
                //atribui o valor vindo da model na variavel

                var valor = formatValueToReal(model.get('nu_valor'));

                //incrementa em total de lancamentos
                totalLancamento += valor;

                //verifica se o registro atual é uma entrada e vai incrementando os respectivos lancamentos
                if (model.get('bl_entrada')) {
                    totalEntrada += valor;
                } else {
                    totalParcela += valor;
                }
            });
        }

        totalLancamento = parseFloat(totalLancamento.toFixed(2));
        //pega o total de lancamentos subtrai do total da venda para ter o restante
        valorRestante = valorVenda - totalLancamento;

        //verifica se o valor da venda é maior que o dos lancamentos
        if (valorVenda !== totalLancamento) {
            this.ui.id_meiopagamento.val('').removeAttr('disabled');
            this.ui.id_campanhacomercial.val('').removeAttr('disabled');
            this.ui.nu_desconto.val('').attr('disabled', 'disabled');
            this.ui.nu_quantidade.val(1).removeAttr('disabled');
            this.ui.nu_valor.val('').attr('disabled', 'disabled');
            this.ui.dtPrimeiraParcela.val('').attr('disabled', 'disabled');
            this.ui.id_atendente.val('').removeAttr('disabled');
            this.ui.id_responsavel.val('').attr('disabled', 'disabled');
            this.populaSelectResponsavel();
            this.ui.btnDtPicker.removeAttr('disabled');
            this.ui.btnAddLancamento.removeAttr('disabled');
            this.ui.divNuTotalLancamento.removeClass('success').addClass('error');

        } else if (valorVenda === totalLancamento.toFixed(2)) {
            this.ui.nu_valorapagar.val('').attr('disabled', 'disabled');
            this.ui.id_meiopagamento.val('').attr('disabled', 'disabled');
            this.ui.id_campanhacomercial.val('').attr('disabled', 'disabled');
            this.ui.nu_desconto.val('').attr('disabled', 'disabled');
            this.ui.nu_quantidade.val('').attr('disabled', 'disabled');
            this.ui.nu_valor.val('').attr('disabled', 'disabled');
            this.ui.nuTotal.val('').attr('disabled', 'disabled');
            this.ui.dtPrimeiraParcela.val('').attr('disabled', 'disabled');
            this.ui.dtPrimeiraParcela.attr('style', 'background-color: #eeeeee;');
            this.ui.id_atendente.val('').attr('disabled', 'disabled');
            this.ui.id_responsavel.val('').attr('disabled', 'disabled');
            this.ui.btnDtPicker.attr('disabled', 'disabled');
            this.ui.btnAddLancamento.attr('disabled', 'disabled');
            this.ui.divNuTotalLancamento.removeClass('error').addClass('success');
        } else {
            this.ui.divNuTotalLancamento.removeClass('error').removeClass('success');
        }

        if (cancelamentoAtualModel.get('bl_finalizar') === true) {
            $("#form-div-lancamento").hide();
            $("#btn-finalizar").attr("disabled", true);
            $("#btn-concluir").attr("disabled", true);
            $("#btn-exportar-pdf").attr("disabled", true);
            $("#btn-salvar-calculo").attr("disabled", true);

        }
        else if (cancelamentoAtualModel.get('bl_concluir') === true) {
            $("#form-div-lancamento").hide();
            $("#btn-concluir").attr("disabled", true);
            $("#btn-exportar-pdf").attr("disabled", true);
            $("#btn-salvar-calculo").attr("disabled", true);
            $("#btn-finalizar").attr("disabled", false);

        } else if ((valorRestante < 0 ? valorRestante * -1 : valorRestante) !== 0) {
            this.ui.divNuValorRestante.removeClass('success').addClass('error');
            $("#btn-concluir").attr("disabled", true);
        } else {
            this.ui.divNuValorRestante.removeClass('error').addClass('success');
            if (permissao) {
                $("#btn-concluir").attr("disabled", false);
            }
        }


        that.totalLancamento = formatValueToShow(totalLancamento.toFixed(2));
        that.totalEntrada = formatValueToShow(totalEntrada.toFixed(2));
        that.totalParcela = formatValueToShow(totalParcela.toFixed(2));
        that.valorRestante = formatValueToShow(valorRestante.toFixed(2));


        //seta os valores nos inputs
        this.ui.nuValorRestante.val(that.valorRestante);
        this.ui.nuValorEntrada.val(that.totalEntrada);
        this.ui.nuValorParcela.val(that.totalParcela);
        this.ui.nuTotalLancamento.val(that.totalLancamento);

    },
    excluirLancamentosSelecionados: function () {
        var that = this;
        var modelsExcluir = this.collection.where({excluirLancamento: true});
        var params = [];
        if (modelsExcluir.length) {
            modelsExcluir.forEach(function (model, i) {
                params[i] = model.toJSON();
            });

            bootbox.confirm("Deseja remover os lançamentos selecionados?", function (result) {
                if (result) {
                    lancamentosExcluidos = params;
                    that.collection.remove(modelsExcluir);
                    $.pnotify({title: 'Sucesso!', text: 'Lançamento(s) removido(s) com sucesso!', type: 'success'});
                }
            });
        } else {
            $.pnotify({title: 'Alerta!', text: 'Nenhum lançamento selecionado!', type: 'alert'});
        }

    },
    validaSave: function (id_meiopagamento, dtPrimeiraParcela, id_atendente) {

        var flag_meio = true;
        var flag_date = true;

        //Valida o meio de pagamento
        if (!id_meiopagamento) {
            this.ui.divMeioPagamento.addClass('control-group error');
            flag_meio = false;
        } else {
            this.ui.divMeioPagamento.removeClass('control-group error');
        }


        //valida se a data da primeira parcela foi preenchida
        if (!dtPrimeiraParcela) {
            this.ui.divDataPrimeiraParcela.addClass('control-group error');
            flag_date = false;
        } else {
            var dtAtual = dateToPtBr(new Date());

            //valida a dt de vencimento para a primeira parcela caso boleto
            if (diferencaEntreDatas(dtPrimeiraParcela, dtAtual) === 0 && id_meiopagamento === 2) {//2 == boleto

                this.ui.divDataPrimeiraParcela.addClass('control-group error');

                $.pnotify({
                    title: 'Atenção!',
                    text: 'A data para a primeira parcela do boleto não pode ser a data atual.',
                    type: 'warning'
                });

                return false;
            }

            this.ui.divDataPrimeiraParcela.removeClass('control-group error');
        }

        var flag_atendente = true;
        //Valida o atendente
        if (!id_atendente) {
            this.ui.divAtendente.addClass('control-group error');
            flag_atendente = false;
        } else {
            this.ui.divAtendente.removeClass('control-group error');
        }


        var flag_valor_menor = true;
        //verifica se o valor da parcela é menor que R$100,00
        if (formatValueToReal(this.ui.nu_valor.val()) < 100) {
            this.ui.divNuValorParcela.addClass('control-group error');
            $.pnotify({
                title: 'Atenção!',
                text: 'O valor da parcela não pode ser menor que R$ 100,00.',
                type: 'warning'
            });
            flag_valor_menor = false;
        } else {
            this.ui.divNuValorParcela.removeClass('control-group error');
        }

        if (!flag_valor_menor || !flag_atendente || !flag_meio || !flag_date) {
            $.pnotify({
                title: 'Atenção!',
                text: 'Verifique os campos obrigatórios.',
                type: 'warning'
            });
            return false;
        }

        return true;
    },
    events: {
        'change #id_meiopagamento': 'selecionaMeioPagamento',
        'change #id_campanhacomercial': 'selecionaCampanhaComercial',
        'change input#nu_quantidade': 'calculaParcelas',
        'blur input#nu_quantidade': 'calculaParcelas',
        'click #btn-add-lancamento': 'adicionaLancamento',
        'click input#chk_all_lancamentos': 'selecionaTodosLancamentos',
        'click #btn_excluir_lancamentos': 'excluirLancamentosSelecionados'
    }
});
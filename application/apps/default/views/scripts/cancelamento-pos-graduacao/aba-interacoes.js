var InteracaoCollection = Backbone.Collection.extend({
    model: LogCancelamento,
    url: "/cancelamento/retorna-log"
});

var MatriculasCollection = Backbone.Collection.extend({
    model: Backbone.Model,
    url: "cancelamento-pos-graduacao/retornar-matriculas-por-cancelamento"
});
var InteracaoItemView = Marionette.ItemView.extend({
    template: "#template-row-interacao",
    tagName: "tr"
});

var InteracaoComposite = Marionette.CompositeView.extend({
    template: "#template-imteracao",
    tagName: "div",
    className: "well",
    childView: InteracaoItemView,
    childViewContainer: ".table-interacoes tbody"
});

var ViewInteracoes = Marionette.LayoutView.extend({
    template: "#view-interacoes",
    tagName: "div",
    className: "well",
    regions: {
        regiaoInteracao: "#regiao-interacao"
    },
    onShow: function () {
        var matriculas = new MatriculasCollection();
        this.idMatricula = [];
        matriculas.fetch({
            "data": {
                "id_cancelamento": cancelamentoAtualModel.get('id_cancelamento')
            },
            async: false,
            beforeSend: loading,
            complete: loaded,
            error: responseError,
            success: function (model) {
                matriculas.each(function (model) {
                    this.idMatricula.push(model.get('id_matricula'));
                }.bind(this));
                var interacaoConllection = new InteracaoCollection;
                interacaoConllection.fetch({
                    data: {"id_matricula": this.idMatricula},
                    beforeSend: loading,
                    complete: loaded,
                    success: function () {
                        this.getRegion("regiaoInteracao").show(new InteracaoComposite({collection: interacaoConllection}));
                        loaded();
                    }.bind(this),
                    error: function () {
                        loaded();
                        $.pnotify({
                            "type": "warning",
                            "title": "Atenção!",
                            "text": "Por favor tente novamente."
                        });
                    }
                });

            }.bind(this)
        });


    }
});



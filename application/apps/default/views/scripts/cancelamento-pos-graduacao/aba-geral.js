//ItemViews
var ItemViewProcessosCancelamento = Marionette.ItemView.extend({
    template: "#processos-cancelamento-itemview-template"
});
var stEvolucao = "";
var ProcessosCancelamentoItemView = Marionette.ItemView.extend({
    template: "#layout-lista-processos-cancelamento",
    ui: {
        comboboxListaProcessosCancelamento: "#lista-processos-cancelamento-combobox",
        btnNovoProcessoCancelamento: "#btn-novo-processo-cancelamento"
    },
    events: {
        "click #btn-novo-processo-cancelamento": "renderizaMatriculasUsuario",
        "change #lista-processos-cancelamento-combobox": "carregaModel"
    },
    onRender: function () {
        ComboboxView({
            el: this.ui.comboboxListaProcessosCancelamento,
            url: "/cancelamento-pos-graduacao/retornar-processos-de-cancelamento-usuario/?id_usuario="
            + componenteAutoComplete.getFixModel().id,
            optionLabelConstruct: function (model) {
                switch (model.get("id_evolucao").id_evolucao) {
                    case 49:
                        stEvolucao = "Pedido de Cancelamento";
                        break;
                    case 50:
                        stEvolucao = "Processo de Cancelamento";
                        break;
                    case 51:
                        stEvolucao = "Finalizado";
                        break;
                    case 52:
                        stEvolucao = "Desistiu";
                        break;
                }

                var mensagem = "Cancelamento - " + model.get("id_cancelamento");

                if (model.get("id_ocorrencia")) {
                    mensagem += " | Ocorrência - " + model.get("id_ocorrencia").id_ocorrencia;
                }
                return mensagem;
            },
            model: Backbone.Model.extend({
                idAttribute: "id_cancelamento",
                defaults: {}
            }),
            // emptyOption: "Nenhum processo de cancelamento aberto para esse usuário."
        });

    },
    renderizaMatriculasUsuario: function () {

        // Remove a seleção do ítem no combobox.
        this.ui.comboboxListaProcessosCancelamento.find("option:selected").prop("selected", false);

        $options = $("#lista-processos-cancelamento-combobox option");

        $options.each(function (index, model) {
            if (!$(model).val()) {
                $(model).remove();
            }
        });

        this.ui.btnNovoProcessoCancelamento.hide();
        viewAlunoListaProcessos.regiaoMatriculaCancelamento.show(new LayoutListaContratosMatriculas());

        if (viewGeral.regiaoDadosCancelamento.hasView()) {
            viewGeral.regiaoDadosCancelamento.empty();
        }
    },
    carregaModel: function () {

        var id_cancelamento = this.ui.comboboxListaProcessosCancelamento.find("option:selected").val();

        if (!id_cancelamento) {
            cancelamentoAtualModel = new CancelamentoModel();
        } else {
            cancelamentoAtualModel.set("idAttribute", id_cancelamento);
        }


        cancelamentoAtualModel.fetch({
            async: false,
            data: {
                id_cancelamento: cancelamentoAtualModel.get("idAttribute")
            },
            beforeSend: loading,
            complete: loaded,
            success: function () {
                viewGeral.regiaoDadosCancelamento.show(new LayoutViewDadosCancelamento());
                $("#tab-calculo").removeClass("disabled bs2-disabled-tab");
                $("#tab-interacoes").removeClass("disabled bs2-disabled-tab");
            }
        });

        this.renderizaMatriculasAssociadasCancelamento();
    },
    renderizaMatriculasAssociadasCancelamento: function () {
        $.ajax({
            url: "cancelamento-pos-graduacao/retornar-matriculas-por-cancelamento",
            data: {
                id_cancelamento: cancelamentoAtualModel.get("idAttribute")
            },
            success: function (response) {
                matriculasCancelamentoAtual = response;
                if (matriculasCancelamentoAtual) {
                    $("#cancelamento-lista-cursos").empty();
                    $("#cancelamento-situacao-evolucao").empty();
                    _.each(response, function (matricula) {
                        if (parseInt(matricula.id_evolucao) === EVOLUCAO.TB_MATRICULA.CURSANDO
                            || parseInt(matricula.id_evolucao) === EVOLUCAO.TB_MATRICULA.BLOQUEADO) {
                            $("#cancelamento-lista-cursos")
                                .append("<div><p>" + matricula.st_projetopedagogico + "</p></div>");
                            $("#cancelamento-situacao-evolucao").append("<div><p>" + stEvolucao + "</p></div>");
                        }
                    });
                }
            }
        });
    }
});

//Collections
var CollectionListaContratoDisciplinas = Backbone.Collection.extend({
    model: Backbone.Model,
    url: "/cancelamento-pos-graduacao/retornar-contrato-matricula-usuario"
});

var CollectionListaProcessosCancelamento = Backbone.Collection.extend({
    model: Backbone.Model,
    url: "/cancelamento-pos-graduacao/retornar-processos-de-cancelamento-usuario"
});

var CollectionMotivosCancelamento = Backbone.Collection.extend({
    model: Backbone.Model,
    url: "cancelamento-pos-graduacao/retornar-motivos-cancelamento"
});

//LayoutViews
var ViewGeral = Marionette.LayoutView.extend({
    template: "#view-geral",
    tagName: "div",
    regions: {
        regiaoAlunoListaProcessos: "#regiao-aluno-lista-processos",
        regiaoDadosCancelamento: "#regiao-dados-cancelamento"
    },
    onShow: function () {
        viewAlunoListaProcessos = new LayoutViewAlunoListaProcessos();
        this.regiaoAlunoListaProcessos.show(viewAlunoListaProcessos);
    }
});

var LayoutViewAlunoListaProcessos = Marionette.LayoutView.extend({
    template: "#view-aluno-lista-processos",
    tagName: "div",
    className: "well",
    regions: {
        regiaoMatriculaCancelamento: "#regiao-matricula-cancelamento",
        regiaoProcessosCancealmento: "#regiao-processos-cancelamento"
    },
    ui: {
        labelNomeAluno: "#nome-aluno"
    },
    initialize: function () {
    },
    onShow: function () {
        this.ui.labelNomeAluno.text(componenteAutoComplete.getFixModel().st_nomecompleto);
        this.regiaoProcessosCancealmento.show(new ProcessosCancelamentoItemView());
    }
});

var LayoutViewDadosCancelamento = Marionette.LayoutView.extend({
    template: "#template-dados-cancelamento",
    className: "well",
    ui: {
        listaCursos: "#cancelamento-lista-cursos",
        listaEvolucoes: "#cancelamento-situacao-evolucao",
        textAreaObservacao: "#textarea-observacao"
    },
    collection: new CollectionMotivosCancelamento(),
    events: {
        "change #textarea-observacao": function () {

            var $textoObservacao = this.ui.textAreaObservacao.val();

            if ($textoObservacao.length > 255) {
                $.pnotify({
                    title: "Erro",
                    type: "error",
                    text: "As observações devem ter no máximo 255 caracteres."
                });
                return false;
            }
            cancelamentoAtualModel.set("st_observacao", this.ui.textAreaObservacao.val());
            cancelamentoAtualModel.set("change_observacao", true);
        },
        "change input[type='checkbox']": function (e) {
            var checked = $(e.currentTarget).is(":checked");
            var id = $(e.currentTarget).val();
            var model = this.collection.findWhere({id_motivo: parseInt(id)});
            if (model) {
                model.set("checked", checked);
            }
        },
    },
    initialize: function () {
        collectionMotivos = this.collection;
    },
    onShow: function () {
        this.carregarInformacoesTela();
    },
    carregarInformacoesTela: function () {
        var that = this;
        this.collection.fetch({
            data: {
                id_linhadenegocio: LINHA_DE_NEGOCIO.POS_GRADUACAO
            },
            success: function () {

                that.adicionarMotivosNoForm();

                if (cancelamentoAtualModel.get("idAttribute")) {
                    that.verificaEMarcaMotivosExistentes();
                }
            }
        });

        //Faz o display dos cursos e respectivas evoluções.
        _.each(matriculasCancelamentoAtual, function (matricula) {
            that.ui.listaCursos.append("<div><p>" + matricula.st_projetopedagogico + "</p></div>");
            that.ui.listaEvolucoes.append("<div><p>Pedido de de Cancelamento </p></div>");
        });

        if (cancelamentoAtualModel.get("st_observacao")) {
            this.ui.textAreaObservacao.html(cancelamentoAtualModel.get("st_observacao"));
        }
    },
    adicionarMotivosNoForm: function () {

        var $formCancelamentoLegend = $("#form-cancelamento").find("legend");
        this.collection.each(function (model) {
            var checkboxElemString = "<label class='checkbox'>" +
                "<input type='checkbox' name='motivos[]' value='" + model.get("id_motivo") + "'>" +
                model.get("st_motivo")
                + "</label>";

            $(checkboxElemString).insertAfter($formCancelamentoLegend);
        });
    },
    verificaEMarcaMotivosExistentes: function () {
        $.ajax({
            url: "/cancelamento-pos-graduacao/retornar-motivos-processo?id_cancelamento=" + cancelamentoAtualModel.get("idAttribute"),
            success: function (response) {

                $checkboxes = $("#form-cancelamento input");
                $checkboxes.each(function () {
                    var that = $(this);
                    _.each(response, function (value, key) {
                        if (that.val() == value.id_motivo.id_motivo) {
                            that.prop("checked", true);
                        }
                    })
                });
            }
        });
    }
});

var LayoutListaContratosMatriculas = Marionette.LayoutView.extend({
    template: "#layout-lista-contrato-matriculas",
    ui: {
        form: "#lista-matriculas-cancelamento-form",
        btnAdicionarMatriculasProcesso: "#btn-adicionar-matriculas-processo"
    },
    events: {
        "click #btn-adicionar-matriculas-processo": "adicionarMatriculasNovoProcesso",
        "change input[type='checkbox']": function () {
            var that = this;
            var $el = $(event.target);
            _.each(that.retornaArrayMatriculasSelecionadas(), function (obj) {
                if (obj.id_contrato !== $el.data("id-contrato")) {
                    $("#lista-matriculas-cancelamento-form input:checkbox").prop("checked", false);
                    $el.prop("checked", true);
                }
            });
        }
    },
    collection: new CollectionListaContratoDisciplinas(),
    onRender: function () {
        var that = this;
        var objCollection = "";

        // Busca e popula tela com as matrículas disponíveis para cancelamento.
        this.collection.fetch({
            data: {
                id_usuario: componenteAutoComplete.getFixModel().id
            },
            success: function (model, response) {

                if (!response.length) {
                    that.ui.form.append("Nenhuma matrícula disponível para um novo processo de cancelamento.");
                    $("#regiao-matricula-cancelamento h5").hide();
                    return false;
                }

                objCollection = that.collection.toJSON();

                objCollection = _.groupBy(objCollection, function (model) {
                    return model.id_venda;
                });
                // Adiciona ítens dinamicamente no form.
                $.each(objCollection, function (index, model) {
                    that.ui.form.append("<h6> Contrato Nº - " + index + "</h6>");
                    $.each(model, function (key, value) {
                        var html = "<label class='checkbox'>";
                        html += "<input type='checkbox' value='" + value.id_matricula + "' ";
                        html += "data-id-contrato='" + value.id_contrato + "' ";
                        html += "data-st-projetopedagogico='" + value.st_projetopedagogico + "' ";
                        html += "data-st-evolucao='" + value.st_evolucao + "'>";
                        html += "</input>" + value.st_projetopedagogico + " - " + value.st_evolucao + "</label>";
                        that.ui.form.append(html);
                    });
                });
            }
        });
    },
    retornaArrayMatriculasSelecionadas: function () {

        var arrayMatriculasSelecionadas = [];
        $checkboxes = $("#lista-matriculas-cancelamento-form input:checkbox:checked");

        if (!$checkboxes.length) {
            $.pnotify({
                title: "Erro",
                type: "error",
                text: "Para adicionar um novo processo de cancelamento, é necessário selecionar pelo menos uma matrícula."
            });

            return false;
        }

        $checkboxes.each(function () {
            arrayMatriculasSelecionadas.push({
                id_matricula: $(this).val(),
                id_contrato: $(this).data("id-contrato"),
                st_projetopedagogico: $(this).data("st-projetopedagogico"),
                st_evolucao: $(this).data("st-evolucao")
            });
        });


        return arrayMatriculasSelecionadas;

    },
    adicionarMatriculasNovoProcesso: function () {
        matriculasCancelamentoAtual = this.retornaArrayMatriculasSelecionadas();

        if (!matriculasCancelamentoAtual) {
            return false;
        }

        viewAlunoListaProcessos.regiaoMatriculaCancelamento.empty();

        if ($("#lista-processos-cancelamento-combobox").find("option").length == 1) {
            $("#lista-processos-cancelamento-combobox").empty();
        }

        $("#lista-processos-cancelamento-combobox").append("<option selected value=''>Novo processo de cancelamento</option>");
        $("#btn-novo-processo-cancelamento").show();
        viewGeral.regiaoDadosCancelamento.show(new LayoutViewDadosCancelamento());

        cancelamentoAtualModel = new NovoCancelamentoModel();

        if (objOcorrenciaCancelamento) {
            cancelamentoAtualModel.set("id_ocorrencia", objOcorrenciaCancelamento.id_ocorrencia);
            cancelamentoAtualModel.set("dt_solicitacao", objOcorrenciaCancelamento.dt_cadastroocorrencia);
        }

    }
});

var viewGeral = new ViewGeral();
var viewAlunoListaProcessos = new LayoutViewAlunoListaProcessos();
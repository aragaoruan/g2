var ModelResultado = Backbone.Model.extend({
    attributes: {
        total: 0,
        enem: 0,
        simplificada: 0,
        remanescentes: 0
    },
    url: '/relatorio/contagem-pesquisa-censo'
});

var ResultadoView = Marionette.ItemView.extend({
    template: '#layout-resultado',
});

var LayoutView = Marionette.ItemView.extend({
    template: '#layout-template',
    ui: {
        form: '#form-censo',
        polo: '#polo',
        produto: '#produto',
        anoInicio: '#anoInicio',
        anoFim: '#anoFim',
        anoReferencia: '#anoReferencia',
        botaoPesquisar: '#btnPesquisar',
        botaoGerarArquivo: '#btnGerarArquivo',
        pesquisaResultados: '#pesquisaResultados'
    },
    events: {
        'click @ui.botaoPesquisar': 'pesquisar',
        'click @ui.botaoGerarArquivo': 'gerarArquivo',
        'change @ui.polo': 'carregaCursos',
        'change @ui.produto': 'escondeResumo',
        'change @ui.anoInicio': 'escondeResumo',
        'change @ui.anoFim': 'escondeResumo',
        'change @ui.anoReferencia': 'escondeResumo',
    },
    onShow: function () {
        this.carregaPolos();
        this.carregaCursos();

        this.$el.find("#pesquisaResultados").hide();
        this.$el.find("#btnGerarArquivo").hide();
    },
    escondeResumo: function () {
        this.$el.find("#pesquisaResultados").hide();
        this.$el.find("#btnGerarArquivo").hide();
    },
    pesquisar: function () {
        var data = this.ui.form.serialize();
        var modelResultado = new ModelResultado;

        modelResultado.fetch({
            data: data,
            beforeSend: loading,
            success: function(){
                (new ResultadoView({
                    model: modelResultado,
                    el: this.ui.pesquisaResultados
                })).render();

                if (modelResultado.get('total')) {
                    this.$el.find("#btnGerarArquivo").show();
                }

                this.$el.find("#pesquisaResultados").show();
            }.bind(this),
            complete: loaded
        });

    },
    gerarArquivo: function () {
        window.open('/relatorio/download-arquivo-censo' +
            '/polo/' + this.ui.polo.val() +
            '/produto/' + this.ui.produto.val() +
            '/anoInicio/' + this.ui.anoInicio.val() +
            '/anoFim/' + this.ui.anoFim.val() +
            '/anoReferencia/' + this.ui.anoReferencia.val()
        );
    },
    carregaCursos: function () {
        this.escondeResumo();

        this.ui.produto.empty();
        this.ui.produto.html('<option value="0">[TODOS]</option>');

        var idPolo = +this.ui.polo.val() || G2S.loggedUser.get('id_entidade');

        /**
         *Collection para VwProduto
         * @type {Backbone.Collection}
         */
        var VwProdutoCollection = Backbone.Collection.extend({
            model: VwProduto,
            url: '/api/vw-projeto-entidade/?id_entidade=' + idPolo + '&id_situacao=7&bl_ativo=1'
        });

        new VwProdutoCollection().fetch({
            data: {
                idEntidade: idPolo,
                idSituacao: SITUACAO.TB_PRODUTO.ATIVO,
            }, success: function (collection) {
                var view = new SelectView({
                    el: this.ui.produto,
                    collection: collection,
                    childViewOptions: {
                        optionLabel: 'st_projetopedagogico',
                        optionValue: 'id_projetopedagogico'
                    }
                });
                view.render();
            }.bind(this)
        });
    },

    carregaPolos: function () {
        this.ui.polo.empty();
        this.ui.polo.html('<option value="0">[TODOS]</option>');

        /**
         *Collection para VwProduto
         * @type {Backbone.Collection}
         */
        var modelEntidade = Backbone.Model.extend();

        var arrModel = [];
        _.each(polosCenso,function(model){
            arrModel.push(new modelEntidade(model));
        });

        var poloCollection = Backbone.Collection.extend({
            model: modelEntidade
        });

        var collectionPolo = new poloCollection(arrModel);

        var view = new SelectView({
            el: this.ui.polo,
            collection: collectionPolo,
            childViewOptions: {
                optionLabel: 'st_nomeentidade',
                optionValue: 'id_entidade'
            }
        });
        view.render();
    }
});

// Render layout template
G2S.layout.content.show(new LayoutView());
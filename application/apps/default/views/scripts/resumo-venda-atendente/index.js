/**
 * ******************************************************
 * ******                  VARIAVEIS              *******
 * ******************************************************
 */
var entidadeSelect;
var viewRelatorio;

var ModelRelatorio = Backbone.Model.extend({
    defaults: {
        st_nomealuno: '',
        st_nomeatendente: '',
        id_venda: '',
        st_produto: '',
        nu_valorliquido: '',
        nu_valordinheiro: '',
        nu_valorcheque: '',
        nu_valorcartao: '',
        nu_valortransferencia: '',
        nu_valorcarta: '',
        nu_valorbolsa: '',
        nu_valorpermuta: '',
        nu_valordeposito: '',
        nu_valorempenho: '',
        nu_valorboleto: '',
        nu_valorestorno: '',
        st_nomeentidade: '',
        dt_cadastro: '',
        nu_parcelas: ''
    }
});

var ModelResumoRelatorio = Backbone.Model.extend({
    defaults: {
        nu_totalvendas: '',
        nu_totalrecebido: '',
        nu_dinheiro: '',
        nu_cheque: '',
        nu_cartao: '',
        nu_transferencia: '',
        nu_carta: '',
        nu_bolsa: '',
        nu_permuta: '',
        nu_deposito: '',
        nu_empenho: '',
        nu_boleto: ''
    }
});


/**
 * ******************************************************
 * ******               COLLECTION                *******
 * ******************************************************
 */

var EntidadeCollection = Backbone.Collection.extend({
    url: '/quantidade-transacoes-cartao-credito/retorna-entidade-recursiva',
    model: VwEntidadeRecursiva,
    initialize: function () {
        //this.fetch()
    }
});

var VwAtendenteModel = Backbone.Model.extend({
    defaults: {
        dt_cadastro : '',
        id_nucleofuncionariotm: '',
        id_funcao: '',
        id_usuario: '',
        id_nucleotm: '',
        id_usuariocadastro: '',
        id_situacao: '',
        id_entidade: '',
        bl_ativo: '',
        st_nucleotm: '',
        st_funcao: '',
        st_situacao: '',
        st_nomecompleto: '',
        st_cpf: '',
        id_entidadematriz: ''
    }
});

var RelatorioCollection = Backbone.Collection.extend({
    model: ModelRelatorio
});

/**
 * ******************************************************
 * ******                  LAYOUT                 *******
 * ******************************************************
 */

var RelLayout = Marionette.LayoutView.extend({
    template: '#resumo_venda_atendente_layout',
    tagname: 'div',
    className: 'containder-fluid',
    regions: {
        relatorio: '#tabela-relatorio',
        itensRelatorio: '#tabelaDadosRelatorio',
        itensRelatorioSomatorio: '#tabelaDadosRelatorioSomatorio'
    },
    ui: {
        'idEntidade': '#id_entidade',
        'dtInicio': "#dt_filtro_inicio",
        'dtFim': "#dt_filtro_fim",
        "idAtendente" : '#id_atendente'
    },
    initialize: function () {
        thisLayout = this;
    },
    onShow: function () {
        this.renderizaSelectEntidade();
        this.renderizaSelectAtendente();
        loaded()
    },
    renderizaSelectEntidade: function () {
        loading();
        var that = this;
        var collectionEntidade = new EntidadeCollection();
        collectionEntidade.fetch({
            success: function () {
                var view = new SelectView({
                    el: that.ui.idEntidade,                 // Elemento da DOM
                    collection: collectionEntidade,         // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_nomeentidade',     // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_entidade',         // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: null                // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();
                loaded();
            }
        });

    },
    renderizaSelectAtendente: function(){
        $('#id_atendente').empty();
        $('#id_atendente').append(new Option(G2S.loggedUser.get('nomeUsuario'), 0));
    },
    gerarRelatorio: function () {
        if (this.validaFiltros()) {
            var that = this;
            loading();

            var collectionResultRelatorio = Backbone.Collection.extend({
                model: ModelRelatorio
            });

            var collectionFetch = new collectionResultRelatorio();
            collectionFetch.url = '/resumo-venda-atendente/retorna-relatorio';
            collectionFetch.fetch({
                data: {
                    'id_entidade': that.ui.idEntidade.val(),
                    'dt_fim': that.ui.dtFim.val(),
                    'dt_inicio': that.ui.dtInicio.val(),
                    'id_atendente': that.ui.idAtendente.val()
                },
                type: 'POST',
                success: function () {
                    if(collectionFetch.models==""){
                        $.pnotify({
                            title: 'Aviso!',
                            text: 'Nenhum dado encontrado para gerar relatório.',
                            type: 'warning'
                        });

                        $('#container-table').hide('fast');
                        loaded();

                    }else{

                        var CollectionTable = Backbone.Collection.extend({ model: ModelRelatorio });
                        var itemView = Marionette.ItemView.extend({
                            tagName: 'tr',
                            template: '#item-relatorio',
                            formatNumber: function(field){
                                var n = 2; //n: length of decimal
                                var x = 3; //x: length of sections
                                var s = '.';
                                var c = ','

                                var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
                                    num = parseFloat(this.model.get(field)).toFixed(Math.max(0, ~~n));
                                var value =  (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
                                return (value == 'NaN') ? '0,00' : value;
                            },
                            onBeforeRender: function () {
                                this.model.set({ nu_valorliquido: this.formatNumber('nu_valorliquido') });
                                this.model.set({ nu_valordinheiro: this.formatNumber('nu_valordinheiro') });
                                this.model.set({ nu_valorcheque: this.formatNumber('nu_valorcheque') });
                                this.model.set({ nu_valorcartao: this.formatNumber('nu_valorcartao') });
                                this.model.set({ nu_valortransferencia: this.formatNumber('nu_valortransferencia') });
                                this.model.set({ nu_valorcarta: this.formatNumber('nu_valorcarta') });
                                this.model.set({ nu_valorbolsa: this.formatNumber('nu_valorbolsa') });
                                this.model.set({ nu_valorpermuta: this.formatNumber('nu_valorpermuta') });
                                this.model.set({ nu_valordeposito: this.formatNumber('nu_valordeposito') });
                                this.model.set({ nu_valorempenho: this.formatNumber('nu_valorempenho') });
                                this.model.set({ nu_valorboleto: this.formatNumber('nu_valorboleto') });
                                this.model.set({ nu_valorestorno: this.formatNumber('nu_valorestorno') });

                                var d = this.model.get('dt_cadastro').split(" ")[0].split("-")
                                this.model.set({ dt_cadastro: d[2]+'/'+d[1]+"/"+d[0] });
                            }
                        });

                        var RelAtendenteView = Marionette.CompositeView.extend({
                            childView: itemView,
                            template: "#script_tabela_relatorio",
                            childViewContainer: 'tbody',
                            initialize: function () {

                            },
                            onShow: function () {
                                return this;
                            }
                        });

                        viewRelatorio = new RelAtendenteView({
                            el: that.$el.find('#container-table'),
                            collection: collectionFetch
                        });

                        $('#container-table').show();
                        viewRelatorio.render();
                        loaded()
                    }
                }
            });

            var collectionAtendenteResultRelatorio = Backbone.Collection.extend({
                model: ModelResumoRelatorio
            });

            var collectionAtendenteFetch = new collectionAtendenteResultRelatorio();
            collectionAtendenteFetch.url = '/resumo-venda-atendente/retorna-relatorio-resumo';
            collectionAtendenteFetch.fetch({
                data: {
                    'id_entidade': that.ui.idEntidade.val(),
                    'dt_fim': that.ui.dtFim.val(),
                    'dt_inicio': that.ui.dtInicio.val(),
                    'id_atendente': that.ui.idAtendente.val()
                },
                type: 'POST',
                success: function () {
                    if(collectionAtendenteFetch.models==""){
                        $.pnotify({
                            title: 'Aviso!',
                            text: 'Nenhum dado encontrado para gerar relatório.',
                            type: 'warning'
                        });

                        $('#container-table').hide('fast');
                        loaded();

                    }else{
                        var CollectionAtendenteTable = Backbone.Collection.extend({ model: ModelResumoRelatorio });
                        var itemViewAtendente = Marionette.ItemView.extend({
                            tagName: 'tr',
                            template: '#item-relatorio-somatorio',
                            formatNumber: function(field){
                                var n = 2; //n: length of decimal
                                var x = 3; //x: length of sections
                                var s = '.';
                                var c = ','

                                var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
                                    num = parseFloat(this.model.get(field)).toFixed(Math.max(0, ~~n));
                                var value =  (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
                                return (value == 'NaN') ? '0,00' : value;
                            },
                            onBeforeRender: function () {
                                this.model.set({ nu_totalrecebido: this.formatNumber('nu_totalrecebido') });
                                this.model.set({ nu_dinheiro: this.formatNumber('nu_dinheiro') });
                                this.model.set({ nu_cheque: this.formatNumber('nu_cheque') });
                                this.model.set({ nu_cartao: this.formatNumber('nu_cartao') });
                                this.model.set({ nu_transferencia: this.formatNumber('nu_transferencia') });
                                this.model.set({ nu_carta: this.formatNumber('nu_carta') });
                                this.model.set({ nu_bolsa: this.formatNumber('nu_bolsa') });
                                this.model.set({ nu_permuta: this.formatNumber('nu_permuta') });
                                this.model.set({ nu_deposito: this.formatNumber('nu_deposito') });
                                this.model.set({ nu_empenho: this.formatNumber('nu_empenho') });
                                this.model.set({ nu_boleto: this.formatNumber('nu_boleto') });
                            }
                        });

                        var RelAtendenteView = Marionette.CompositeView.extend({
                            childView: itemViewAtendente,
                            template: "#script_tabela_relatorio_somatorio",
                            childViewContainer: 'tbody',
                            initialize: function () {

                            },
                            onShow: function () {
                                return this;
                            }
                        });

                        viewRelatorioAtendente = new RelAtendenteView({
                            el: that.$el.find('#container-table-somatorio'),
                            collection: collectionAtendenteFetch
                        });

                        $('#container-table-somatorio').show();
                        viewRelatorioAtendente.render();
                        loaded()
                    }
                }
            });
        }

    },
    validaFiltros: function () {
        flag = true;

        if ($("#dt_filtro_inicio").val() && $("#dt_filtro_fim").val()){
            var dtInicio = $("#dt_filtro_inicio").val().split("/");
            var dtFim = $("#dt_filtro_fim").val().split("/");

            var date1 = new Date(dtInicio[2] + "/" + dtInicio[1] + "/" + dtInicio[0]);
            var date2 = new Date(dtFim[2] + "/" + dtFim[1] + "/" + dtFim[0]);

            var diferenca = Math.abs(date1 - date2); //diferença em milésimos e positivo
            var dia = 1000*60*60*24; // milésimos de segundo correspondente a um dia
            var total = Math.round(diferenca/dia); //valor total de dias arredondado

            if(total > 365){
                $.pnotify({
                    title: 'Aviso!',
                    text: 'O período entre a data da Venda deve ser de no máximo 365 dias.',
                    type: 'warning'
                });
                flag = false;
            }
        }else{
            $.pnotify({
                title: 'Aviso!',
                text: 'É necessário informar um período para a data da Venda.',
                type: 'warning'
            });
            flag = false;
        }

        return flag;
    },
    gerarXLS: function(){
        that = this;
        if(this.validaFiltros()){
            var data = {
                'id_entidade': that.ui.idEntidade.val(),
                'dt_fim': that.ui.dtFim.val(),
                'dt_inicio': that.ui.dtInicio.val()
            }
            var url = '/resumo-venda-atendente/gerar-xls/?tipo=xls&id_entidade='+that.ui.idEntidade.val()+'&dt_fim='+that.ui.dtFim.val()+'&dt_inicio='+that.ui.dtInicio.val();
            window.open(url,'_blank');
        }
    },
    events: {
        'click #btn_gerar_relatorio': 'gerarRelatorio',
        'click #btn_gerar_xls': 'gerarXLS'
    }
})

/**
 * ******************************************************
 * ******                 ITEMVIEW                *******
 * ******************************************************
 */
var tabelaRelatorioView = Marionette.ItemView.extend({
    template: '#item-relatorio',
    tagName: 'tr',
    ui: {},
    onRender: function () {
    }
});

var relatorioView = Marionette.ItemView.extend({
    template: '#script_tabela_relatorio',
    ui: {}

});

/**
 * ******************************************************
 * ******               COLLECTIONVIEW            *******
 * ******************************************************
 */
var RelatorioCollectionView = Marionette.CollectionView.extend({
    template: '#tabela-relatorio',
    el: '#tabelaDadosRelatorio',
    childView: tabelaRelatorioView
});


/**
 * ******************************************************
 * ******              COMPOSITEVIEW              *******
 * ******************************************************
 */
var RelAtendente = new RelLayout();
G2S.show(RelAtendente);




/******************************************************************************
 *              Relatório de Consulta de Disciplina
 *              Rafael Leite  <rafael.leite@unyleya.com.br>
 *              2016-09-27
 * ****************************************************************************
 */
$.ajaxSetup({async: false});

/**
 ******************************************************************************************
 *************************** COLLECTION  ***************************************************
 ******************************************************************************************
 */

/**
 * @type Collection para ProjetoPedagogico @exp;Backbone@pro;Collection@call;extend
 */

var ProjetoPedagogicoCollection = Backbone.Collection.extend({
    url: '/projeto-pedagogico/retorna-projetos-pedagogicos-entidade/',
    model: VwProjetoEntidade,
    initialize: function () {
        //this.fetch()
    }
});

/**
 * @type Collection para Disciplina @exp;Backbone@pro;Collection@call;extend
 */
var DisciplinaCollection = Backbone.Collection.extend({
    url: '/disciplina/retorna-disciplina-projeto-pedagogico/',
    model: VwModuloDisciplina,
    initialize: function () {
        //this.fetch()
    }
});

/**
 * @type Collection para FormatoArquivo @exp;Backbone@pro;Collection@call;extend
 */
var FormatoArquivoCollection = Backbone.Collection.extend({
    url: 'api/formato-arquivo/',
    model: FormatoArquivo,
    initialize: function () {
        //this.fetch()
    }
});


/**
 ******************************************************************************************
 *************************** LAYOUT ****************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var TelaGradeLayout = Marionette.LayoutView.extend({
    template: '#template-layout-grade',
    tagName: 'div',
    regions: {
        tabelaGrade: "#regiao-grade-data",
        formgrade: '#regiao-grade',
    },
    onShow: function () {
        this.renderizaFormGrade();
        loaded();
        return this;
    },
    renderizaFormGrade: function () {
        var gradeView = new FormGradeCompositeView({});
        this.formgrade.show(gradeView);
    }
});


/**
 ******************************************************************************************
 ******************************************************************************************
 *************************** FORM DA GRADE ****************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var FormGradeCompositeView = Marionette.CompositeView.extend({
    initialize: function () {

    },
    template: '#template-form-grade',
    tagName: 'fieldset',
    ui: {
        'id_projetopedagogico': 'select[name="id_projetopedagogico"]',
        'id_disciplina': 'select[name="id_disciplina"]',
        'id_formatoarquivo': 'select[name="id_formatoarquivo"]',
        'dt_inicial_cadastro_conteudo': 'input[name="dt_inicial_cadastro_conteudo"]',
        'dt_termino_cadastro_conteudo': 'input[name="dt_termino_cadastro_conteudo"]',
        'btnSalvar': '#btnPesquisar',
    },
    events: {
        'change #id_projetopedagogico': 'populaSelectDisciplina',
        'click #btnPesquisar': 'pesquisarDados',
        'click #btn_gerar_relatorio': 'btnGeralExcel'
    },
    onShow: function () {
        $('#container-DataGrid').hide();
        this.populaSelectProjetoPedagogico();
        this.populaSelectFormaroArquivo();
    },
    populaSelectProjetoPedagogico: function () {
        var that = this;
        var collectionProjetoPedagogico = new ProjetoPedagogicoCollection();
        collectionProjetoPedagogico.fetch({
            success: function () {
                var view = new SelectView({
                    el: that.$el.find("select[name='id_projetopedagogico']"),        // Elemento da DOM
                    collection: collectionProjetoPedagogico,      // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_projetopedagogico', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_projetopedagogico', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: null       // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();
            }
        });
    },
    populaSelectDisciplina: function () {
        var that = this;
        that.$el.find("select[name='id_disciplina']").empty();
        that.$el.find("select[name='id_disciplina']").prepend('<option value="" selected="selected">Selecione</option>');
        if (!that.ui.id_projetopedagogico.val()) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Selecione o Projeto Pedagógico para poder buscar as Disciplinas.'
            });
            return false;
        }
        loading();
        var collectionDisciplina = new DisciplinaCollection();
        collectionDisciplina.url += 'id_projetopedagogico/'+that.ui.id_projetopedagogico.val();
        collectionDisciplina.fetch({
            success: function () {
                var view = new SelectView({
                    el: that.$el.find("select[name='id_disciplina']"),        // Elemento da DOM
                    collection: collectionDisciplina,      // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_disciplina', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_disciplina', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: null       // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();

                that.ui.id_disciplina.removeAttr('disabled');
                loaded();
            }
        });
    },
    populaSelectFormaroArquivo: function () {
        var that = this;

        var collectionFormatoArquivo = new FormatoArquivoCollection();
        collectionFormatoArquivo.fetch({
            success: function () {
                var view = new SelectView({
                    el: that.$el.find("select[name='id_formatoarquivo']"),         // Elemento da DOM
                    collection: collectionFormatoArquivo,      // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_formatoarquivo', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_formatoarquivo', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: null       // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();
            }
        });
    },
    pesquisarDados: function () {

        that = this;
        if (!that.ui.id_projetopedagogico.val()) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Selecione um Projeto Pedagógico.'
            });
            return false;
        }
        loading();
        var HtmlBotoesCell = Backgrid.Cell.extend({
            className: 'span2',
            render: function () {
                var arquivoconteudo = '';
                if(this.model.get('st_formatoarquivo') == "ZIP"){
                    arquivoconteudo = this.model.get('id_disciplinaconteudo')+"/home.html";
                }else{
                    arquivoconteudo = this.model.get('id_disciplinaconteudo')+"."+this.model.get('st_formatoarquivo').toLowerCase();
                }
                var elTemp = _.template( "<a href='/disciplina_conteudo/"+this.model.get('id_disciplina')+"/"+arquivoconteudo+"' class='btn btn-small btn-success' target='_blank'><i class='icon icon-white icon-eye-open'></i> Visualizar</a>");
                this.$el.html(elTemp);
                return this;
            }
        });

        var TurmaCell = Backgrid.Cell.extend({
            render: function(){
                var elTemp = _.each(collectionPesquisa)
            }
        });

        var columns = [
            {
                name: "id_projetopedagogico",
                label: "ID Projeto Pedagógico",
                editable: false,
                cell: 'string'
            }, {
                name: "st_projetopedagogico",
                label: "Projeto Pedagógico",
                editable: false,
                cell: 'string'
            }, {
                name: "id_disciplina",
                label: "ID Disciplina",
                editable: false,
                cell: 'string'
            }, {
                name: "st_tituloexibicao",
                label: "Título de Exibição",
                editable: false,
                cell: 'string'
            }, {
                name: "st_disciplina",
                label: "Título da Disciplina",
                editable: false,
                cell: 'string'
            }, {
                name: "nu_cargahoraria",
                label: "Carga Horária",
                editable: false,
                cell: 'string'
            }, {
                name: "st_formatoarquivo",
                label: "Extensão de Arquivo",
                editable: false,
                cell: 'string'
            }, {
                name: "st_descricaoconteudo",
                label: "Descrição do Conteúdo",
                editable: false,
                cell: 'string'
            }, {
                name: "dt_cadastro",
                label: "Data de Cadastro",
                editable: false,
                cell: 'string'
            }, {
                name: "id_acao",
                label: "Ação",
                editable: false,
                cell: HtmlBotoesCell
            }
        ];

        var RowGrid = Backgrid.Row.extend({
            initialize: function (options) {
                RowGrid.__super__.initialize.apply(this, arguments);
                this.$el.css('cursor', 'pointer');
            },
            /**
             * Mostra o arquivo do com o conteúdo numa nova aba
             */
        });

        var PageableLog = Backbone.PageableCollection.extend({
            url: '/relatorio-consulta-disciplina/pesquisa-relatorio/',
            model: VwConsultaDisciplina,
            state: {
                pageSize: 10
            },
            mode: "client"
        });
        var data = {
            id_projetopedagogico: that.ui.id_projetopedagogico.val(),
            id_disciplina: that.ui.id_disciplina.val(),
            id_formatoarquivo: that.ui.id_formatoarquivo.val(),
            dt_inicial_cadastro_conteudo: that.ui.dt_inicial_cadastro_conteudo.val(),
            dt_termino_cadastro_conteudo: that.ui.dt_termino_cadastro_conteudo.val()
        };

        var pageableLog = new PageableLog();

        var pageableGridLog = new Backgrid.Grid({
            className: 'backgrid table table-bordered',
            columns: columns,
            collection: pageableLog,
            row: RowGrid,
            emptyText: "Nenhum registro encontrado"
        });

        $('#container-DataGrid').show();
        var $elemento = $('#template-data-grade');
        $elemento.empty();
        //$elemento.append(pageableGridLog.render().$el);
        var paginator = new Backgrid.Extension.Paginator({
            collection: pageableLog
        });
        //$elemento.append(paginator.render().el);

        pageableLog.fetch({
            data: data,
            reset: true,
            beforeSend: function () {
                loading();
            },
            success: function () {
                telaGrade.$el.find("#regiao-grade-data").empty();
                telaGrade.$el.find("#regiao-grade-data").append(pageableGridLog.render().$el);
                telaGrade.$el.find("#regiao-grade-data").append(paginator.render().el);
            },
            error: function (collection, response) {
                $.pnotify({
                    type: 'warning',
                    title: 'Atenção!',
                    text: response.responseText
                });
            },
            complete: function(){
                loaded();
            }

        });
        //loaded();
    },
    btnGeralExcel: function (response) {
        var that = this;
        if (!that.ui.id_projetopedagogico.val()) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Selecione um Projeto pedagógico.'
            });
            return false;
        }
        
        loading();
        this.$el.find('form').attr('target', '_blank').attr('action', 'relatorio-consulta-disciplina/gerar-xls').submit();
        this.$el.find('form').removeAttr('target');
        loaded();
    }
});


/**
 ******************************************************************************************
 ******************************** SHOW LAYOUT *********************************************
 ******************************************************************************************
 */

var telaGrade = new TelaGradeLayout();
G2S.show(telaGrade);
var CollectionEsquemaConfiguracao = retornarEsquemaConfiguracao(25, false);

var collectionSemestres = [
    {'nu_semestre': 1},
    {'nu_semestre': 2},
    {'nu_semestre': 3},
    {'nu_semestre': 4},
    {'nu_semestre': 5},
    {'nu_semestre': 6},
    {'nu_semestre': 7},
    {'nu_semestre': 8},
    {'nu_semestre': 9},
    {'nu_semestre': 10}
];

var collectionSemestresObrigatoriosOrdinais = [
    {'nu_semestre': 'Não', 'id_semestre': 0},
    {'nu_semestre': '1º', 'id_semestre': 1},
    {'nu_semestre': '2º', 'id_semestre': 2},
    {'nu_semestre': '3º', 'id_semestre': 3},
    {'nu_semestre': '4º', 'id_semestre': 4},
    {'nu_semestre': '5º', 'id_semestre': 5},
    {'nu_semestre': '6º', 'id_semestre': 6},
    {'nu_semestre': '7º', 'id_semestre': 7},
    {'nu_semestre': '8º', 'id_semestre': 8},
    {'nu_semestre': '9º', 'id_semestre': 9},
    {'nu_semestre': '10º', 'id_semestre': 10}
];

var collectionSemestresDisponiveisOrdinais = [
    {'nu_semestre': 'Sim', 'id_semestre': 0},
    {'nu_semestre': '1º', 'id_semestre': 1},
    {'nu_semestre': '2º', 'id_semestre': 2},
    {'nu_semestre': '3º', 'id_semestre': 3},
    {'nu_semestre': '4º', 'id_semestre': 4},
    {'nu_semestre': '5º', 'id_semestre': 5},
    {'nu_semestre': '6º', 'id_semestre': 6},
    {'nu_semestre': '7º', 'id_semestre': 7},
    {'nu_semestre': '8º', 'id_semestre': 8},
    {'nu_semestre': '9º', 'id_semestre': 9},
    {'nu_semestre': '10º', 'id_semestre': 10}
];

var ModuloDisciplinaModel = VwModuloDisciplinaModel.extend({
    idAttribute: 'id_modulodisciplina',
    url: function () {
        return '/api/modulo-disciplina/' + (this.id || '');
    }
});

var CollectionDisciplinasPonderacao = Backbone.Collection.extend({
    model: ModuloDisciplinaModel,
    url: '/projeto-pedagogico/retorna-ponderacao-calculada'
});

/** Collection para pesquisa de disciplinas*/
var VwDisciplinaSerieNivelCollection = Backbone.Collection.extend({
    model: ModuloDisciplinaModel.extend({idAttribute: 'id_disciplina'}),
    urlRoot: '/api/vw-disciplina-serie-nivel/',
    async: false
});

// MODEL
var ParametroTccModel = Backbone.Model.extend({
    idAttribute: 'id_parametrotcc',
    defaults: {
        id_parametrotcc: null,
        st_parametrotcc: '',
        nu_peso: null,
        bl_ativo: 1,
        id_projetopedagogico: null
    },
    url: function () {
        return this.id ? '/api/parametro-tcc/' + this.id : '/api/parametro-tcc';
    },
    is_editing: false,
    toggleEdit: function () {
        this.is_editing = !this.is_editing;
    }
});

//MODEL
var ProjetoPedagogicoExtend = ProjetoPedagogico.extend({
    idAttribute: 'id_projetopedagogico',
    url: function () {
        return this.id ? '/api/projeto-pedagogico/' + this.id : '/api/projeto-pedagogico';
    },
    toggleEdit: function () {
        this.is_editing = !this.is_editing;
    },
    toString: function () {
        return this.get("st_projetopedagogico");
    }
});

var permissaoPossuiProva = false;

/****************************************************************
 *
 * ****************  CONFIGURACOES SERIE ********************
 *
 ****************************************************************/
var SerieView,
    SerieEmpty,
    SerieCollection,
    SerieComposite;
var disciplinaComposite;
var ponderacaoComposite;

// ITEM VIEW
SerieView = Marionette.ItemView.extend({
    template: _.template('<input type="checkbox" name="id_serie" value="<%= id_serie %>" /> <%= st_serie %>'),
    tagName: 'label',
    className: 'checkbox',
    events: {
        'change input': function (e) {
            this.model.set('checked', $(e.target).prop('checked'));
        }
    }
});

// EMPTY ITEM VIEW
SerieEmpty = Marionette.ItemView.extend({
    template: _.template('Nenhuma série encontrada.'),
    tagName: 'div'
});

// COLLECTION
SerieCollection = Backbone.Collection.extend({
    url: '/api/vw-serie-nivel-ensino'
});

// COMPOSITE VIEW
SerieComposite = Marionette.CompositeView.extend({
    template: _.template('<div></div>'),
    collection: new SerieCollection(),
    childView: SerieView,
    emptyView: SerieEmpty,
    childViewContainer: 'div',
    onRender: function () {
        this.collection.fetch({
            data: {
                id_nivelensino: projetoLayout.ui.id_nivelensino.val(),
            },
            complete: function () {
                this.getChecked();
            }.bind(this)
        });

    },
    getChecked: function () {
        var that = this;
        var id_projetopedagogico = projetoLayout.model.get('id_projetopedagogico');

        if (id_projetopedagogico && this.collection.models) {
            $.ajax({
                url: '/api/projeto-pedagogico-serie-nivel-ensino/?id=' + id_projetopedagogico,
                dataType: 'json',
                success: function (response) {
                    for (var i in response.id_serie) {
                        var id_serie = response.id_serie[i];
                        that.$el.find('input:checkbox[value="' + id_serie + '"]').prop('checked', true).trigger('change');
                    }
                }
            });
        }
    },
    actionSave: function () {
        var models = this.collection.where({checked: true});
        var id_serie = [];
        if (models) {
            for (var i in models) {
                id_serie.push(models[i].get('id_serie'));
            }
        }
        if (id_serie.length && projetoLayout.ui.id_nivelensino.val()) {
            return $.ajax({
                url: '/api/projeto-pedagogico-serie-nivel-ensino/',
                type: 'put',
                dataType: 'json',
                contentType: "application/json",
                data: JSON.stringify({
                    id_serie: id_serie,
                    id_nivelensino: projetoLayout.ui.id_nivelensino.val(),
                    id_projetopedagogico: projetoLayout.model.get('id_projetopedagogico')
                })
            });
        } else {
            $.pnotify({
                title: "Escolaridade - Erro",
                text: "Cadastre corretamente a escolaridade e o nível de ensino.",
                type: 'error'
            });
        }

    }
});

/****************************************************************
 *
 * ************  CONFIGURACOES ORGANIZAÇÕES ****************
 *
 ****************************************************************/
var OrganizacaoComposite;

//COMPOSITE
OrganizacaoComposite = Marionette.CompositeView.extend({
    template: '#projeto-organizacao-template',
    ui: {
        //JS TREE
        organizacao: '#organizacao-content',
        bl_todasentidades: '[name="bl_todasentidades"]'
    },
    onRender: function () {
        var that = this;

        this.ui.bl_todasentidades.prop('checked', projetoLayout.model.get('bl_todasentidades')).trigger('change');

        // ARVORE DE ENTIDADES
        this.ui.organizacao
            .jstree({
                html_data: {
                    ajax: {
                        url: 'util/arvore-entidade'
                    }
                },
                plugins: ['themes', 'html_data', 'checkbox', 'sort', 'ui']
            })
            .on('loaded.jstree', function () {
                that.ui.organizacao.jstree('btn_check_all');
                var id_projeto = projetoLayout.model.get('id_projetopedagogico');
                if (id_projeto) {
                    $.getJSON('/api/projeto-entidade/?id_projeto=' + id_projeto, function (response) {
                        if (response) {
                            for (var i in response) {
                                that.ui.organizacao.jstree('check_node', 'li#' + response[i].id_entidade["id_entidade"]);
                            }
                        }
                    });
                }
            });
    },
    events: {
        'change [name="bl_todasentidades"]': 'toggleOrganizacao'
    },
    toggleOrganizacao: function (e) {
        // Mostra / esconde arvore de entidades
        this.ui.organizacao.toggle(!$(e.target).prop('checked'));
    },
    actionSave: function () {
        var that = this;
        var id_entidade = [];
        that.ui.organizacao.jstree('get_checked', null, true).each(function () {
            id_entidade.push(this.id);
        });
        return $.ajax({
            url: '/api/projeto-entidade/',
            type: 'post',
            dataType: 'json',
            contentType: "application/json",
            data: JSON.stringify({
                id_entidade: id_entidade,
                id_projeto: projetoLayout.model.get('id_projetopedagogico'),
                id_projetoentidade: null
            })
        });
    }
});

/****************************************************************
 *
 * ************  CONFIGURACOES AREA CONHECIMENTO ****************
 *
 ****************************************************************/
var AreaView,
    AreaEmpty,
    AreaCollection,
    AreaCapesComposite,
    AreaMecItemView;

// ITEM VIEW
AreaView = Marionette.ItemView.extend({
    template: _.template('<input type="radio" name="id_areaconhecimento" value="<%= id_areaconhecimento %>" /> <%= st_areaconhecimento %>'),
    tagName: 'label',
    className: 'checkbox',
    setAreaConhecimento: function () {
        this.model.collection.each(function (model, i) {
            model.set('checked', false);
        });
        this.model.set('checked', true);
    },
    events: {
        'change input': 'setAreaConhecimento'
    }
});

// EMPTY ITEM VIEW
AreaEmpty = Marionette.ItemView.extend({
    template: _.template('Nenhuma área encontrada.'),
    tagName: 'div'
});

// COLLECTION
AreaCollection = Backbone.Collection.extend({
    url: '/api/area-conhecimento/?bl_ativo=true&backbone=true'
});

// COMPOSITE VIEW
AreaCapesComposite = Marionette.CompositeView.extend({
    template: "#template-area-conhecimento-capes", // _.template('<div></div>'),
    collection: new AreaCollection(),
    childView: AreaView,
    emptyView: AreaEmpty,
    childViewContainer: "#area-capes-composite-child-container",
    ui: {
        selectOrigemAreaConhecimento: "#origem-areaconhecimento"
    },
    initialize: function () {
        var self = this;
        this.collection.fetch({
            data: {
                'id_origemareaconhecimento': ORIGEM_AREA_CONHECIMENTO.CAPES
            }
        });
    },
    onRender: function () {
        this.getChecked();
    },
    getChecked: function () {
        var that = this;
        var id_projetopedagogico = projetoLayout.model.get('id_projetopedagogico');

        if (id_projetopedagogico && this.collection.models) {
            $.ajax({
                url: '/api/area-projeto-pedagogico/',
                dataType: 'json',
                data: {id: id_projetopedagogico},
                success: function (response) {
                    var id_areaconhecimento = response.mensagem.id_areaconhecimento[0];
                    that.$el.find('input:radio[value="' + id_areaconhecimento + '"]').prop('checked', true).trigger('change');
                }
            });
        }
    },
    actionSave: function () {
        var models = this.collection.where({checked: true});
        var id_areaconhecimento = [];
        if (models) {
            for (var i in models) {
                id_areaconhecimento.push(models[i].get('id_areaconhecimento'));
            }
        }

        if (id_areaconhecimento.length) {
            return $.ajax({
                url: '/api/area-projeto-pedagogico/',
                type: 'put',
                dataType: 'json',
                contentType: "application/json",
                data: JSON.stringify({
                    id_areaconhecimento: id_areaconhecimento,
                    id_projetopedagogico: projetoLayout.model.get('id_projetopedagogico')
                })
            });
        } else {
            $.pnotify({
                title: "Área conhecimento - Erro",
                text: "Escolha uma área para relacionar o projeto ",
                type: 'error'
            });
        }

    },
    validate: function () {
        var models = this.collection.where({checked: true});
        var id_areaconhecimento = [];
        if (models) {
            for (var i in models) {
                id_areaconhecimento.push(models[i].get('id_areaconhecimento'));
            }
        }
        if (id_areaconhecimento.length) {
            return true;
        } else {
            $.pnotify({
                title: "Área conhecimento - Erro",
                text: "Escolha uma área para relacionar ao projeto ",
                type: 'error'
            });
            return false;
        }

    }
});

AreaMecItemView = Marionette.ItemView.extend({
    template: "#template-area-conhecimento-mec",
    ui: {
        selectAreaConhecimentoMEC: "#select-area-conhecimento-mec",
        mensagemAreaNaoCadastrada: "#msg-area-nao-cadastrada-mec",
        mensagemCarregando: "#msg-carregando"
    },
    onRender: function () {
        var self = this;
        ComboboxView({
            el: this.ui.selectAreaConhecimentoMEC,
            model: Backbone.Model.extend({
                idAttribute: "id_areaconhecimento"
            }),
            url: "/api/area-conhecimento/?" + $.param({
                bl_ativo: true,
                backbone: true,
                id_origemareaconhecimento: 2
            }),
            optionLabel: "st_areaconhecimento",
            emptyOption: "[Nenhuma]",
            optionSelectedId: projetoLayout.model.get("id_areaconhecimentomec")
        }, function(el) {
            // Exibe o select e esconde a mensagem de área não cadastrada e a msg de carregando.
            self.ui.mensagemCarregando.addClass("hidden");
            if (el[0].length > 1) {
                el.removeClass("hidden");
                return true;
            }
            self.ui.mensagemAreaNaoCadastrada.removeClass('hidden');
            return false;
        });
    }
});


/****************************************************************
 *
 * **************  COMPOSITE PARA COORDENADORES *****************
 *
 ****************************************************************/

var UsuarioCoordenadorCollectionExtend = Backbone.Collection.extend({
    url: '/api/vw-usuario-perfil-entidade/?id_perfilpedagogico=2&id_situacaoperfil=15',
    model: VwUsuarioPerfilEntidade
});
var UsuarioCoordenadorCollection = new UsuarioCoordenadorCollectionExtend();
UsuarioCoordenadorCollection.fetch();

var ModelUsuarioPerfilEntidadeReferencia = UsuarioPerfilEntidadeReferencia.extend({
    idAttribute: 'id_perfilreferencia',
    url: function () {
        return 'api/usuario-perfil-entidade-referencia/' + (this.id || '');
    }
});


var CoordenadorView = Marionette.ItemView.extend({
    template: '#template-line-coordenador',
    model: new ModelUsuarioPerfilEntidadeReferencia(),
    ui: {
        id_usuario: '#id_usuario'
    },
    initialize: function () {
    },
    onRender: function () {
        var that = this;
        //Combo professores
        ComboboxView({
            el: that.ui.id_usuario,
            collection: UsuarioCoordenadorCollection.toJSON(),
            url: '/api/vw-usuario-perfil-entidade/?id_perfilpedagogico=2&id_situacaoperfil=15',
            optionLabel: 'st_nomecompleto',
            optionSelectedId: that.model.get('id_usuario'),
            emptyOption: '[Selecione]',
            model: Backbone.Model.extend({
                idAttribute: 'id_usuario',
            })
        }, function (el, composite) {
            if (!that.collectionComboProfessores)
                that.collectionComboProfessores = composite.collection;

            var first_model = composite.collection.findWhere({id_usuario: that.model.get('id_usuario')});
            if (first_model) {
                that.$el.find('#id_perfil').val(first_model.get('id_perfil')).trigger('change');
            }
        })
    },
    events: {
        'click .actionDel': 'actionDel',
        'change #id_usuario': 'changeUser',
        'change': 'actionChange',
        'change input[name="bl_titular"]': 'actionClearTitulares'
    },
    changeUser: function (e) {
        var that = this;
        var first_model = null;
        this.collectionComboProfessores.each(function (model) {
            if (+model.get('id_usuario') === +that.ui.id_usuario.val()) {
                first_model = model;
            }
        });
        if (first_model) {
            that.$el.find('#id_perfil').val(first_model.get('id_perfil')).trigger('change');
        }
    },
    actionDel: function () {
        var that = this;
        bootbox.confirm("Deseja realmente excluir este coordenador?", function (result) {
            if (result) {
                loading();
                if (that.model.get('id_perfilreferencia') && projetoLayout.model.get('id_projetopedagogico')) {
                    that.model.destroy({
                        success: function (model, response) {
                            $.pnotify({
                                title: "Coordenador - Sucesso",
                                text: "Coordenador excluído com sucesso!",
                                type: 'success'
                            });
                            loaded();
                        },
                        error: function (model, response) {
                            $.pnotify({
                                title: "Coordenador - Erro",
                                text: "Ocorreu um erro na requisição",
                                type: 'error'
                            });
                            loaded();
                        }
                    });
                } else {
                    that.model.collection.remove(that.model);
                    loaded();
                }

            }
        });
    },
    actionChange: function () {
        var values = Backbone.Syphon.serialize(this);
        if (values.bl_titular == 'on') values.bl_titular = 1;
        values.bl_ativo = 1;
        values.id_entidade = G2S.loggedUser.get('id_entidade');
        this.model.is_valid = true;
        if (!values.id_usuario) {
            this.model.is_valid = false;
        }
        this.model.set(values);
    },
    actionClearTitulares: function () {
        if (!this.model.get('bl_titular')) {
            //Retira titulares marcados anteriormente
            this.model.collection.each(function (idx) {
                idx.set('bl_titular', false);
            });
        }
    }
});

var CoordenadorCollection = Backbone.Collection.extend({
    url: '/api/vw-usuario-perfil-entidade-referencia/',
    model: ModelUsuarioPerfilEntidadeReferencia
});

var CoordenadorComposite = Marionette.CompositeView.extend({
    template: '#template-content-coordenadores',
    childView: CoordenadorView,
    childViewContainer: '#body-table-coordenadores',
    collection: new CoordenadorCollection(),
    events: {
        'click .actionAdd': 'actionAdd'
    },
    actionAdd: function () {
        var modelNew = new ModelUsuarioPerfilEntidadeReferencia();
        modelNew.set(modelNew.defaults);
        modelNew.set({
            'id_perfilreferencia': null,
            bl_titular: 0
        });

        this.collection.add(modelNew);
    },
    onRender: function () {
        if (projetoLayout.model.get('id_projetopedagogico')) {
            this.collection.fetch({
                data: {
                    id_projetopedagogico: projetoLayout.model.get('id_projetopedagogico'),
                    id_perfilpedagogico: 2
                }
            });
        }
    },
    saveCoordenadores: function () {
        var that = this;
        var deferreds = [];
        if (that.calculaValores()) {
            $.ajaxSetup({async: false});
            this.collection.each(function (model) {
                model.set('id_projetopedagogico', projetoLayout.model.get('id_projetopedagogico'));
                if (model.is_valid) {
                    deferreds.push(model.save(null, {
                        success: function (model, response) {
                        },
                        error: function (model, response) {
                            $.pnotify({
                                type: 'error',
                                title: 'Erro',
                                text: response.responseJSON.mensagem
                            })
                        }
                    }));
                }
            });
            $.ajaxSetup({async: true});
            $.pnotify({
                title: "Coordenador - Sucesso",
                text: "Coordenadore(s) salvos com sucesso!",
                type: 'success'
            });
        }
        return $.when.apply(null, deferreds);

    },
    calculaValores: function () {

        var valorTotal = 0.00;
        var valorAtual = 0.00;
        this.collection.each(function (model) {
            valorAtual = parseFloat(model.get('nu_porcentagem').toString(), 10).toFixed(2);
            valorTotal = Number(valorTotal) + Number(valorAtual);
        });

        if (valorTotal > 100) {
            $.pnotify({
                title: "Coordenador - Erro",
                text: "A soma da porcentagem não pode ser maior que 100. " + valorTotal + " foi o valor total informado!",
                type: 'error'
            });
            return false;
        }
        return true;
    }
});

var IntegracaoFabricaDeProvasView = new Marionette.ItemView.extend({
    tagName: 'tr',
    template: '#prova-integrada-fabrica-de-provas'
});


/*************************************************
 *
 * Segunda aba - módulos e disciplinas
 *
 ************************************************/
var DisciplinaView = Marionette.ItemView.extend({
    template: '#template-line-disciplina-projeto',
    tagName: 'tr',
    ui: {
        nu_obrigatorioalocacao: '#nu_obrigatorioalocacao',
        nu_disponivelapartirdo: '#nu_disponivelapartirdo',
        justGrad: '.justGrad',
        noGrad: '.hidden-graducao'
    },
    initialize: function () {
    },
    onRender: function () {
        var that = this;
        if (+G2S.loggedUser.get('id_linhadenegocio') === LINHA_DE_NEGOCIO.GRADUACAO) {
            this.ui.noGrad.hide();
            ComboboxView({
                el: that.ui.nu_obrigatorioalocacao,
                collection: collectionSemestresObrigatoriosOrdinais,
                optionLabel: 'nu_semestre',
                optionSelectedId: that.model.get('nu_obrigatorioalocacao'),
                //emptyOption: 'Não',
                model: Backbone.Model.extend({
                    idAttribute: 'id_semestre'
                })
            });

            ComboboxView({
                el: that.ui.nu_disponivelapartirdo,
                collection: collectionSemestresDisponiveisOrdinais,
                optionLabel: 'nu_semestre',
                optionSelectedId: that.model.get('nu_disponivelapartirdo'),
                //emptyOption: 'Não',
                model: Backbone.Model.extend({
                    idAttribute: 'id_semestre'
                })
            });
        } else {
            that.ui.justGrad.hide();
        }
    },
    events: {
        'change': 'actionChange',
        'change @ui.nu_obrigatorioalocacao': 'changeObrigatorioAlocacao',
        'change @ui.nu_disponivelapartirdo': 'changeObrigatorioAlocacao'
    },
    actionChange: function () {
        var values = Backbone.Syphon.serialize(this);
        if (values.bl_obrigatoria == 'on') values.bl_obrigatoria = 1;
        this.model.set(values);
    },
    changeObrigatorioAlocacao: function () {
        if (this.ui.nu_disponivelapartirdo.val()) {
            if (parseInt(this.ui.nu_obrigatorioalocacao.val()) && (parseInt(this.ui.nu_obrigatorioalocacao.val()) < parseInt(this.ui.nu_disponivelapartirdo.val()))) {
                this.ui.nu_obrigatorioalocacao.val(0);
                $.pnotify({
                    title: 'Aviso',
                    text: 'O valor do campo "Obrigatório para alocação" não deve ser menor que o valor do campo "Disponível a partir de".',
                    type: 'warning'
                });
            }
        }
    }


});

var DisciplinaPonderacaoView = Marionette.ItemView.extend({
    template: '#template-line-disciplina-ponderacao',
    onRender: function () {
        return this;
    },
    events: {
        'change': 'actionChange'
    },
    actionChange: function () {
        var values = Backbone.Syphon.serialize(this);
        this.model.set(values);
    }
});


var ModalSideBySideView = Marionette.ItemView.extend({
    template: '#template-modal-projeto',
    onRender: function () {
        var that = this;
        var collection = new VwDisciplinaSerieNivelCollection();
        collection.url = collection.urlRoot + '?' + $.param({
            id_modulo: that.model.get('id_modulo')
        });

        var myItem = new SideBySideView({
            collections: {
                left: collection,
                right: that.collection
            },
            label: 'st_disciplina',
            label_search: 'id_disciplina',
            fetch: true,
            addAll: true,
            globalSearch: true,
            idAttributeGlobal: 'id_disciplina'
        });

        this.$el.find('.modal-body').html(myItem.render().$el);
    },
    events: {
        'click #salvar-disciplinas-modal': 'actionSave'
    }
});

var DisciplinaCollection = Backbone.Collection.extend({
    url: 'api/vw-modulo-disciplina/',
    model: ModuloDisciplinaModel
});

var DisciplinaComposite = Marionette.CompositeView.extend({
    template: '#template-accordion-modulo',
    childView: DisciplinaView,
    childViewContainer: '#body-table-disciplina-projeto',
    ui: {
        contentModal: '#content-modal',
    },
    initialize: function () {
        this.collection = new DisciplinaCollection();
        disciplinaComposite = this;
        $('.popLabel').popover();
        return this;
    },
    events: {
        'click .addDisciplina': 'addDisciplina',
        'hide .modal': 'salvarDisciplinas',
        'change': 'actionChange',
        'click .actionSaveDisciplina': 'salvarDisciplinas',
        'click .delDisciplina': 'excluirDisciplinas',
        'click .removeModulo': 'removeModulo'
    },
    collectionEvents: {
        add: function (model) {
            model.idAttribute = 'id_modulodisciplina';
            model.id = model.get(model.idAttribute);
        },
        remove: function (model) {
            if (model.id) {
                model.destroy().done(function (response) {
                    $.pnotify({
                        title: "Disciplina",
                        text: "Disciplina removida com sucesso!",
                        type: 'success'
                    });
                });

                //removendo da collection de ponderação
                var toRemovePon = [];
                toRemovePon.push(model);
                collectionDisciplinaPonderacaoIni.remove(toRemovePon);
                modulosIni.atualizaPonderacaoCalculada(false, true);
            }
        }
    },
    actionChange: function () {
        var values = Backbone.Syphon.serialize(this);
        this.model.set(values);
    },
    addDisciplina: function () {
        var that = this;

        var modal = new ModalSideBySideView({
            model: that.model,
            collection: that.collection
        });
        that.ui.contentModal.html(modal.render().$el);
        this.$el.find('.modal').modal('show');
    },
    salvarDisciplinas: function () {
        var that = this;
        that.model.set({
            st_tituloexibicao: that.model.get('st_modulo'),
            bl_ativo: 1,
            st_descricao: that.model.get('st_modulo'),
            id_projetopedagogico: projetoLayout.model.get('id_projetopedagogico'),
            id_situacao: 33
        });
        $.ajaxSetup({async: false});

        that.model.save(null, {
            success: function () {
                if (that.model.get('id_modulo')) {
                    that.collection.each(function (modelDisciplina) {
                        modelDisciplina.set('id_modulo', that.model.get('id_modulo'));
                        modelDisciplina.set('id_nivelensino ', null);
                        modelDisciplina.save(null, {});
                    });

                    modulosIni.calculaPondAplicada(false);
                    that.calculaTotalCargaHoraria();
                    modulosIni.atualizaPonderacaoCalculada(false, true);
                    that.render();
                }
            }
        });

        $.ajaxSetup({async: true});
        $.pnotify({
            title: "Módulo e disciplina",
            text: "Módulo e disciplina(s) salvos com sucesso!",
            type: 'success'
        });


    },
    removeModulo: function () {
        var that = this;
        bootbox.confirm("<b>Deseja excluir esse módulo?</b> Todas as disciplinas relacionadas a ele serão excluídas!", function (response) {
            if (response) {
                if (that.model.get('id_modulo')) {
                    that.model.destroy({
                        success: function (dataReturn) {
                            $.pnotify({
                                type: 'success',
                                title: 'Registro Removido',
                                text: 'O módulo foi removido com sucesso.'
                            });
                        },
                        error: function () {
                            $.pnotify({
                                type: 'error',
                                title: 'Erro',
                                text: 'Não é possivel apagar esse módulo.'
                            });
                        },
                        complete: loaded
                    });
                } else {
                    that.remove();
                }
                //remove da collection de ponderação e calcula novamente a ponderação aplicada
                var toRemovePon = [];
                that.collection.each(function (modelDisciplina) {
                    toRemovePon.push(modelDisciplina);
                });

                collectionDisciplinaPonderacaoIni.remove(toRemovePon);
                modulosIni.calculaPondAplicada();
            }
        });
    },
    excluirDisciplinas: function () {
        var that = this;
        var nu_excluir = 0;
        var toRemove = [];
        $.ajaxSetup({async: false});
        that.collection.each(function (modelDisciplina) {
            if (modelDisciplina.get('id_modulodisciplinaDel') == true) {
                nu_excluir += 1;
                toRemove.push(modelDisciplina);
            }
        });

        if (nu_excluir > 0) {
            bootbox.confirm("Deseja realmente excluir " + nu_excluir + " disciplina" + (nu_excluir > 1 ? "s" : "") + " ?", function (result) {
                if (result) {
                    loading();
                    that.collection.remove(toRemove);
                    that.calculaTotalCargaHoraria();
                }
            });
        } else {
            bootbox.alert("<strong>Por favor, selecione uma disciplina antes de excluir.</strong>");
        }

        $.ajaxSetup({async: true});
    },
    calculaTotalCargaHoraria: function () {
        var totalCargaHoraria = 0;
        var that = this;
        that.collection.each(function (modelDisciplina) {
            var valor = modelDisciplina.get('nu_cargahoraria') ? modelDisciplina.get('nu_cargahoraria').toString().replace(/,/g, '.') : 0;
            totalCargaHoraria += parseFloat(valor);

        });
        this.$el.find(".totalCargaHoraria").html("<b>" + totalCargaHoraria + " h </b>");
    },
    onRender: function () {
        var that = this;
        loading();
        if (this.model.get('id_modulo')) {
            this.collection.fetch({
                data: {
                    id_projetopedagogico: projetoLayout.model.get('id_projetopedagogico'),
                    'id_modulo': this.model.get('id_modulo')
                },
                success: function (collection) {
                    modulosIni.contador += 1;
                    collectionDisciplinaPonderacaoIni.remove(collection.models);
                    collectionDisciplinaPonderacaoIni.add(collection.models);
                },
                complete: function () {
                    that.calculaTotalCargaHoraria();
                    modulosIni.calculaPondAplicada();
                    loaded();
                }
            });
        } else {
            this.collection.reset();
            loaded();
        }
    }
});

var ModelModulo = Modulo.extend({
    idAttribute: 'id_modulo',
    url: function () {
        return 'api/modulo/' + (this.id || '');
    }
});

var ModuloCollection = Backbone.Collection.extend({
    url: '/api/modulo/',
    model: ModelModulo
});

var ModuloComposite = Marionette.CompositeView.extend({
    template: '#template-projeto-disciplina',
    collection: new ModuloCollection(),
    childView: DisciplinaComposite,
    childViewContainer: '#content-accordion-modulo',
    ui: {
        contentPonderacao: '#content-disciplina-ponderacao',
    },
    contador: 0,
    onShow: function () {
        var that = this;
        collectionDisciplinaPonderacaoIni.reset();

        that.collection.fetch({
            data: {
                id_projetopedagogico: projetoLayout.model.get('id_projetopedagogico'),
                bl_ativo: 1,
                id_situacao: 33
            },
            complete: function () {
                // that.calculaPondAplicada();
            }
        });
    },
    events: {
        'click #btn-adicionar-modulo': 'addModulo',
        'click .actionCorrigirMatricula': 'corrigirMatriculas',
        'click .actionSavePonderacao': 'salvarPonderacao',
    },
    corrigirMatriculas: function () {

        if (!projetoLayout.model.get('id_projetopedagogico')) {
            $.pnotify({
                title: "Módulo Disciplina - Erro",
                text: "É preciso salvar o Projeto Pedagógico antes de corrigir as Matrículas.",
                type: 'error'
            });
            return false;
        }

        loading();

        var id_projetopedagogico = projetoLayout.model.get('id_projetopedagogico');

        $.post("/matricula/corrigir-matriculas", {id_projetopedagogico: id_projetopedagogico}, function (data) {
            $.pnotify(data);
            loaded();
        }, "json");

    },
    addModulo: function () {
        var modelNew = new Modulo();
        var numModulos = this.collection.models.length;
        modelNew.set('st_modulo', 'Módulo ' + (Number(numModulos) + 1));
        modelNew.idAttribute = 'id_modulo';
        modelNew.set('id_modulo', null);
        this.collection.add(modelNew);
    },
    calculaPondAplicada: function (bl_msg) {
        var totalPonAplicada = 0;
        var totalPonCalculada = 0;
        var that = this;
        collectionDisciplinaPonderacaoIni.each(function (modelDisciplina) {
            var valor = modelDisciplina.get('nu_ponderacaoaplicada') ? modelDisciplina.get('nu_ponderacaoaplicada').toString().replace(/,/g, '.') : modelDisciplina.get('nu_ponderacaocalculada').toString().replace(/,/g, '.');
            totalPonAplicada += parseFloat(valor);

            var valorCal = modelDisciplina.get('nu_ponderacaocalculada') ? modelDisciplina.get('nu_ponderacaocalculada').toString().replace(/,/g, '.') : 0;
            totalPonCalculada += parseFloat(valorCal);
        });
        //ponderacaoComposite.render();
        this.$el.find(".totalPonderacaoCalculada").html(totalPonCalculada.toFixed(2) + "%");
        if (totalPonAplicada > 100) {
            $.pnotify({
                title: "Atenção!",
                text: "O valor total de Pond. Aplicada não pode ultrapassar 100%!",
                type: "warning"
            });
            return false;
        } else {
            this.$el.find(".totalPonderacaoAplicada").val(totalPonAplicada);
            return true;
        }
    },
    atualizaPonderacaoCalculada: function (bl_msg, resetCollection) {
        var that = this;
        var id = projetoLayout.model.get('id_projetopedagogico');
        if (id) {
            $.ajax({
                type: 'post',
                url: '/projeto-pedagogico/atualiza-ponderacao-calculada-disciplina',
                data: {id_projetopedagogico: id},
                dataType: 'json',
                beforeSend: function () {
                    loading();
                },
                complete: function () {
                    loaded();
                },
                success: function (data) {
                    if (bl_msg != undefined)
                        $.pnotify(data);
                }
            });

            //Limpa a collection para buscar os dados atualizados
            if (resetCollection) {

            }
        }
    },
    salvarPonderacao: function () {
        var that = this;
        loading();
        collectionDisciplinaPonderacaoIni.each(function (modelDisciplina) {
            modelDisciplina.set('nu_ponderacaoaplicada', modelDisciplina.get('nu_ponderacaoaplicada').toString());
            modelDisciplina.save();
        });

        if (that.calculaPondAplicada()) {
            that.atualizaPonderacaoCalculada(null, false);
            that.render();
            $.pnotify({
                title: "Sucesso!",
                text: "Ponderação salva com sucesso",
                type: "success"
            });
        }
        loaded();
    },
    onRender: function () {
        ponderacaoComposite = new DisciplinaPonderacaoComposite({
            collection: collectionDisciplinaPonderacaoIni
        });

        this.ui.contentPonderacao.html(ponderacaoComposite.render().$el);
    }
});


/****************************************************
 * PARAMETROS DE TCC
 * *************************************************/
var ParametroTccView = Marionette.ItemView.extend({
    model: new ParametroTccModel(),
    tagName: 'tr',
    onBeforeRender: function () {
        this.template = this.model.is_editing ? '#template-tcc-line-edit' : '#template-tcc-line-list'
    },
    events: {
        'click .edit': 'actionEdit',
        'click .save': 'actionSave',
        'click .delete': 'actionRemove',
        'click .cancel': 'resetEdit',
        'change': 'actionChange'
    },
    actionEdit: function () {
        this.model.toggleEdit();
        this.render();
    },
    actionRemove: function () {
        var that = this;
        bootbox.confirm("Deseja realmente apagar este registro?", function (response) {
            if (response) {
                that.model.destroy({
                    success: function (dataReturn) {
                        //that.remove();
                        $.pnotify({
                            type: 'success',
                            title: 'Registro Removido',
                            text: 'O registro foi removido.'
                        });
                    },
                    error: function () {
                        $.pnotify({
                            type: 'error',
                            title: 'Erro',
                            text: 'Não é possivel apagar esse parâmetro de tcc.'
                        });
                    },
                    complete: loaded
                });
            }
        });
    },
    actionSave: function () {
        var that = this;
        // Validacao dos campos
        if (!this.validate())
            return false;

        loading();
        this.model.save(null, {
            success: function (model, response) {
                if (response.type == 'success')
                    that.model.set(response.mensagem);
                that.actionEdit(); // Alterna para o modo de exibicao
            },
            complete: function (response) {
                response = $.parseJSON(response.responseText);
                $.pnotify(response);
                loaded();
            }
        });
    },
    validate: function () {
        var errorMsg = null;
        if (!this.model.get('st_parametrotcc'))
            errorMsg = 'Insira o nome do parâmetro a ser salvo.';
        else if (!this.model.get('nu_peso'))
            errorMsg = 'Insira o peso do parâmetro';

        if (errorMsg) {
            $.pnotify({
                type: 'warning',
                title: 'Campos Obrigatórios',
                text: errorMsg
            });
            return false;
        }

        return true;
    },
    resetEdit: function () {
        this.model.set(this.oldSettings);
        this.actionEdit();
        if (!this.model.id)
            this.remove();
    },
    actionChange: function () {
        var values = Backbone.Syphon.serialize(this);
        values.id_projetopedagogico = projetoLayout.model.get('id_projetopedagogico');
        this.model.set(values);
    }
});

// COLLECTION
var ParametroTccCollection = Backbone.Collection.extend({
    model: ParametroTccModel,
    url: '/api/parametro-tcc'
});

var ParametroTccComposite = Marionette.CompositeView.extend({
    template: '#template-tcc',
    collection: new ParametroTccCollection(),
    childView: ParametroTccView,
    emptyView: Marionette.ItemView.extend({
        tagName: 'tr',
        template: _.template('<td colspan="3">Nenhum parâmetro tcc adicionado.</td>')
    }),
    childViewContainer: 'tbody',
    onRender: function () {
        if (projetoLayout.model.get('id_projetopedagogico')) {
            this.collection.fetch({
                reset: true,
                data: {
                    id_projetopedagogico: projetoLayout.model.get('id_projetopedagogico'),
                    bl_ativo: 1
                }
            });
        }
    },
    events: {
        'click .actionAdd': 'actionAdd'
    },
    actionAdd: function () {
        var newModel = new ParametroTccModel();
        newModel.toggleEdit();
        this.collection.add(newModel);
    }
});

/*************************************************
 *
 * LAYOUT VIEW PRINCIPAL e PRIMEIRA ABA - Dados Básicos
 *
 ************************************************/
var ProjetoLayout = Marionette.LayoutView.extend({
    template: '#projeto-pedagogico-template',
    model: new ProjetoPedagogicoExtend(),
    regions: {
        form_projetodisciplina: '#form-projeto-disciplina',
        form_projetotextos: '#form-projeto-textos',
        form_projetoavaliacao: '#form-projeto-avaliacao',
        form_projetoparamtcc: '#form-projeto-param-tcc',
        form_integracaofabricadeprovas: '#form-integracao-fabrica-de-provas',
        form_projetoatividade: '#form-projeto-atividade',
        id_entidade: '#id_entidade',
        id_serie: '#id_serie',
        id_areaconhecimento: '#id_areaconhecimento',
        id_areaconhecimentomec: '#id_areaconhecimentomec',
        coordenadores: '#container-coordenadores'
    },
    ui: {
        //COMBOBOXES
        id_nivelensino: '#id_nivelensino',
        id_contratoregra: '#id_contratoregra',
        id_situacao: '#id_situacao',
        organizacao: '#organizacao-content',
        st_reconhecimento: '#st_reconhecimento',
        contadorRegressivo: '#contadorRegressivo',
        bl_todasentidades: '[name="bl_todasentidades"]',
        nu_semestre: '#nu_semestre',
        div_nu_semestre: '#div_nu_semestre',
        st_grauacademico: '#id_grauacademico'
    },
    collections: {
        id_serienivel: {},
        id_areaconhecimento: {},
        id_areaconhecimentomec: {},
        // ARRAYS PARA COMBOBOEX
        id_nivelensino: null,
        id_contratoregra: null,
        id_situacao: null,
        nu_semestre: null,
        st_grauacademico: null
    },
    initialize: function () {
        this.model.set('id_projetopedagogico', Number(window.location.href.split('/').pop()) || null);
        this.model.set('bl_enviaremail', 1);
    },
    onRender: function () {
        loading();
        this.loadComboboxes();

        areaCapesComposite = new AreaCapesComposite();
        this.collections.id_areaconhecimento = areaCapesComposite;
        this.id_areaconhecimento.show(this.collections.id_areaconhecimento);

        areaMecComposite = new AreaMecItemView();
        this.id_areaconhecimentomec.show(areaMecComposite);

        this.coordenadores.show(coordenadoresIni);
        this.id_entidade.show(projetoentidade);
        this.verificaPermissaoBotoes();
        this.verificaObrigatoriedadeGrauAcademico();
        loaded();
    },
    onShow: function () {
        loadTinyMCE('st_objetivo,st_estruturacurricular,st_conteudoprogramatico,st_metodologiaavaliacao,st_certificacao,st_horario,st_publicoalvo,st_mercadotrabalho,st_boasvindascurso');
        this.ui.st_reconhecimento.trigger('keyup');
    },
    verificaPermissaoBotoes: function () {
        VerificarPerfilPermissao(38, 19, true).done(function (response) {
            var that = this;
            //Desabilita div do campo "Possui prova montada" se o usuario não tiver permissão
            if (response.length === 1) {
                $('#bl_possuiprova').removeAttr('disabled');
            } else {
                $('#bl_possuiprova').attr('disabled', true);
            }
        });
    },
    verificaObrigatoriedadeGrauAcademico: function () {
        // adiciona obrigatoriedade caso a linha de negócio seja 2(GRADUAÇÃO)
        if (+G2S.loggedUser.get('id_linhadenegocio') === LINHA_DE_NEGOCIO.GRADUACAO) {
            this.ui.st_grauacademico.attr('required', 'required');
        }
    },
    loadComboboxes: function () {
        var that = this;

        //Combo situação
        ComboboxView({
            el: that.ui.id_situacao,
            collection: that.collections.id_situacao,
            url: '/api/situacao/?st_tabela=tb_projetopedagogico',
            optionLabel: 'st_situacao',
            optionSelectedId: that.model.get('id_situacao'),
            emptyOption: '[Selecione]',
            model: Backbone.Model.extend({
                idAttribute: 'id_situacao'
            })
        }, function (el, composite) {
            if (!that.collections.id_situacao)
                that.collections.id_situacao = composite;
        });

        //Regra do contrato
        ComboboxView({
            el: that.ui.id_contratoregra,
            collection: that.collections.id_contratoregra,
            url: '/api/contrato-regra/',
            optionLabel: 'st_contratoregra',
            optionSelectedId: that.model.get('id_contratoregra'),
            emptyOption: '[Selecione]',
            model: Backbone.Model.extend({
                idAttribute: 'id_contratoregra'
            })
        }, function (el, composite) {
            if (!that.collections.id_contratoregra)
                that.collections.id_contratoregra = composite;
        });

        var id_nivelensinoselected;
        if (that.model.get('id_projetopedagogico')) {
            $.ajax({
                url: '/api/projeto-pedagogico-serie-nivel-ensino/',
                dataType: 'json',
                async: false,
                data: {id: that.model.get('id_projetopedagogico')},
                success: function (response) {
                    id_nivelensinoselected = response.id_nivelensino;
                }
            });
        }
        //Nivel Ensino
        ComboboxView({
            el: that.ui.id_nivelensino,
            collection: that.collections.id_nivelensino,
            url: '/api/nivel-ensino/',
            optionLabel: 'st_nivelensino',
            optionSelectedId: id_nivelensinoselected,
            emptyOption: '[Selecione]',
            model: Backbone.Model.extend({
                idAttribute: 'id_nivelensino'
            })
        }, function (el, composite) {
            if (!that.collections.id_nivelensino)
                that.collections.id_nivelensino = composite;
            // Renderiza a série de acordo com o nível caso o mesmo esteja selecionado
            if (el.val()) {
                that.renderSerie();
            }
        });
        //Numero Semestre
        if (+G2S.loggedUser.get('id_linhadenegocio') === LINHA_DE_NEGOCIO.GRADUACAO) {
            ComboboxView({
                el: that.ui.nu_semestre,
                collection: collectionSemestres,
                optionLabel: 'nu_semestre',
                optionSelectedId: that.model.get('nu_semestre'),
                emptyOption: '[Selecione]',
                model: Backbone.Model.extend({
                    idAttribute: 'nu_semestre'
                })
            })
        } else {
            that.ui.div_nu_semestre.empty();
        }

        // grau academico
        ComboboxView({
            el: that.ui.st_grauacademico,
            collection: that.collections.st_grauacademico,
            url: '/api/grau-academico/',
            optionLabel: 'st_grauacademico',
            optionSelectedId: that.model.get('id_grauacademico'),
            emptyOption: '[Selecione]',
            model: Backbone.Model.extend({
                idAttribute: 'id_grauacademico'
            })
        }, function (el, composite) {
            if (!that.collections.st_grauacademico)
                that.collections.st_grauacademico = composite;
        });
    },
    events: {
        'click .actionBack': 'actionBack',
        'submit form#frm-pp-basicos': 'saveBasicos',
        'change #id_nivelensino': 'renderSerie',
        'change #id_anexoementa': 'validaExtensao',
        'click #tab-projeto-disciplina': 'renderAba2',
        'click #tab-projeto-texto': 'renderAba3',
        'click #tab-projeto-avaliacao': 'renderAba4',
        'keyup @ui.st_reconhecimento': 'contadorReconhecimento'
    },
    contadorReconhecimento: function (e) {
        e.preventDefault();
        var limite = 1000;
        var caracteresDigitados = this.ui.st_reconhecimento.val().length;
        var caracteresRestantes = limite - caracteresDigitados;
        this.ui.contadorRegressivo.text(caracteresRestantes);
    },
    actionBack: function (e) {
        e.preventDefault();
        G2S.router.navigate('#/pesquisa/CadastrarProjetoPedagogico', {trigger: true});
    },
    saveBasicos: function (e) {
        e.preventDefault();

        var that = this;
        var values = Backbone.Syphon.serialize(this);
        that.model.set(values);

        // Salvar o Projeto Pedagogico (tb_projetopedagogico)
        if (that.collections.id_areaconhecimento.validate()) {
            // Se salvar corretamente, salva suas relacoes
            this.model.save(null, {
                beforeSend: loading,
                success: function (model, response) {
                    that.model.set(response);

                    $.ajaxSetup({async: false});

                    //Salvar escolaridade (Nivel e série)
                    that.collections.id_serienivel.actionSave();

                    //Salvar Area conhecimento
                    that.collections.id_areaconhecimento.actionSave();

                    //salvar coordenadores
                    coordenadoresIni.saveCoordenadores().done(function () {
                    });

                    //Salvar Organizações
                    projetoentidade.actionSave();

                    // SALVAR ARQUIVO ANEXO EMENTA
                    var input_anexo = $('#id_anexoementa');
                    if (input_anexo.val()) {
                        var arquivos = input_anexo[0].files;
                        var formData = new FormData();

                        for (var i = 0; i < arquivos.length; i++) {
                            var file = arquivos[i];
                            formData.append('arquivo', file, file.name);
                        }
                        $.ajax({
                            url: 'projeto-pedagogico/salvar-anexo-ementa?id_projetopedagogico=' + response.id_projetopedagogico,
                            type: 'post',
                            data: formData,
                            cache: false,
                            dataType: 'json',
                            processData: false,
                            contentType: false,
                            success: function (response) {
                                if (response.type == 'success') {
                                    that.model.set('id_anexoementa', response.codigo);
                                    that.mostraArquivoSelecionado();
                                    $.pnotify({
                                        title: 'Concluído',
                                        text: 'O novo arquivo de ementa foi salvo!',
                                        type: 'success'
                                    });
                                } else {
                                    this.error();
                                }
                            },
                            error: function () {
                                $.pnotify({title: 'Erro', text: 'Houve um erro ao salvar o arquivo.', type: 'error'});
                            },
                            complete: function () {
                                input_anexo.val('');
                            }
                        });
                    }

                    $.ajaxSetup({async: true});

                    $.pnotify({
                        type: 'success',
                        title: 'Sucesso',
                        text: 'Projeto pedagogico salvo com sucesso'
                    })
                },
                error: function (model, response) {
                    $.pnotify(response.responseJSON);
                },
                complete: loaded
            });
        }

    },
    mostraArquivoSelecionado: function () {
        $.getJSON('/api/upload/' + this.model.get('id_anexoementa'), function (response) {
            if (response && response.id_upload) {
                $('.ementa-label')
                    .attr('href', '/upload/' + response.st_upload)
                    .html(response.st_upload);
            } else {
                $('.ementa-label')
                    .removeAttr('href')
                    .html('Nenhum');
            }
        });
    },
    validaExtensao: function (e) {
        var input = $(e.target);
        var extensions = ['.pdf', '.doc', '.docx'];
        var extensaoValida = (new RegExp('(' + extensions.join('|').replace(/\./g, '\\.') + ')$')).test(input.val());

        if (!extensaoValida) {
            input.val('');
            $.pnotify({title: 'Aviso', text: 'Esta extensão de arquivo não é permitida.', type: 'warning'});
            return false;
        }

        return true;
    },
    validate: function () {
        var msg = [];

        this.$el.find('[required]').each(function () {
            var $this = $(this);
            if (!$this.val()) {
                msg.push($this.closest('label').text().trim());
            }
        });

        if (msg) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção',
                text: 'Preencha os campos ' + msg.join(', ') + ' antes de continuar.'
            });
            return false;
        }

        return true;
    },
    renderSerie: function () {
        var value = Number(this.ui.id_nivelensino.val());
        SerieCollection = Backbone.Collection.extend({
            url: '/api/vw-serie-nivel-ensino/?id_nivelensino=' + value
        });
        this.collections.id_serienivel = new SerieComposite();
        this.id_serie.show(this.collections.id_serienivel);


    },
    renderAba2: function () {
        var that = this;
        if (that.model.get('id_projetopedagogico')) {
            this.form_projetodisciplina.show(modulosIni);
        } else {
            $.pnotify({
                type: 'warning',
                title: 'Atenção',
                text: 'Salve a primeira aba antes de continuar.'
            });

        }
    },
    renderAba3: function () {
        var that = this;
        if (that.model.get('id_projetopedagogico')) {
            this.form_projetotextos.show(textosIni);
        } else {
            $.pnotify({
                type: 'warning',
                title: 'Atenção',
                text: 'Salve a primeira aba antes de continuar.'
            });

        }
    },
    renderAba4: function () {
        var that = this;
        if (that.model.get('id_projetopedagogico')) {
            this.form_projetoavaliacao.show(avaliacaoIni);
            this.form_projetoparamtcc.show(parametroTccIni);

            // Mostrar dados de atividades complementares se a linha de negócio for "Graduação"
            if (+G2S.loggedUser.get('id_linhadenegocio') === LINHA_DE_NEGOCIO.GRADUACAO) {
                this.form_projetoatividade.show(atividadeIni);
            }
            this.form_integracaofabricadeprovas.show(integracaoFabricaDeProvasViewIni);
        } else {
            $.pnotify({
                type: 'warning',
                title: 'Atenção',
                text: 'Salve a primeira aba antes de continuar.'
            });

        }
    }
});


// INICIALIZA A MONTAGEM DA TELA

var projetoLayout = new ProjetoLayout();
var coordenadoresIni = new CoordenadorComposite();
var modulosIni = new ModuloComposite();
var parametroTccIni = new ParametroTccComposite();
var collectionDisciplinaPonderacaoIni = new CollectionDisciplinasPonderacao();


var DisciplinaPonderacaoComposite = Marionette.CompositeView.extend({
    template: '#template-disciplina-ponderacao',
    childView: DisciplinaPonderacaoView,
    childViewContainer: '#content-lines-ponderacao'
});


/** Terceira ABA - TEXTOS */
var TextosView = Marionette.ItemView.extend({
    template: '#template-textos',
    model: projetoLayout.model,
    ui: {
        'imagem_curso': '#imagem_curso',
        'st_descricao': '#st_descricao'
    },
    onShow: function () {
        loadTinyMCE('st_objetivo,st_estruturacurricular,st_conteudoprogramatico,st_metodologiaavaliacao,st_certificacao,st_horario,st_publicoalvo,st_mercadotrabalho,st_descricao,st_boasvindascurso');
    },
    events: {
        'click .saveTextos': 'actionSave'
    },
    actionSave: function (e) {
        var that = this;
        e.preventDefault();

        //pega o formulario e envia, junto com a imagem
        this.$el.find("#form-textos-projeto-pedagogico").ajaxSubmit({
            url: this.model.url(),
            type: this.model.id ? 'PUT' : 'POST',
            dataType: 'json',
            beforeSend: loading,
            complete: loaded,
            success: function (response) {
                $('#id_imagem').attr('href', response.st_imagem);
                that.model.set(response);
                $.pnotify({
                    type: 'success',
                    title: 'Sucesso',
                    text: 'Textos salvos com sucesso.'
                });
                //limpa o campo
                that.ui.imagem_curso.val('');
            },
            error: function (response) {
                var mensageiro = $.parseJSON(response.responseText);
                $.pnotify({
                    type: 'error',
                    title: mensageiro.mensagem.title ? mensageiro.mensagem.title : mensageiro.title,
                    text: mensageiro.mensagem.text ? mensageiro.mensagem.text : mensageiro.text
                });
            }
        });
    }
});
var textosIni = new TextosView();


/** Quarta ABA - AVALIAÇÃO E PARAMETROS DE TCC */
var AvaliacaoView = Marionette.ItemView.extend({
    template: '#template-avaliacao',
    model: projetoLayout.model,
    config: CollectionEsquemaConfiguracao.at(0),
    onBeforeRender: function () {
        if (parseInt(this.config.get('st_valor')) == 1) {
            this.model.set('bl_mediavariavel', 1);
        }
    },
    initialize: function () {
        this.model.set('bl_mediavariavel', 0);
    },
    events: {
        'click .saveAvaliacao': 'actionSave',
        'change': 'actionChange'
    },
    actionChange: function () {
        var values = Backbone.Syphon.serialize(this);
        this.model.set(values);
    },
    actionSave: function () {
        var that = this;
        if ((parseInt(this.config.get('st_valor')) == 1) && (!parseInt(this.model.get('nu_percentualaprovacaovariavel')))) {
            $.pnotify({
                type: 'warning',
                title: 'Aviso',
                text: 'O percentual de aprovação média váriavel é obrigatório e não foi passado. Verifique o campo.'
            });
            return false;
        } else {
            this.model.save(null, {
                beforeSend: loading,
                success: function (model, response) {
                    that.model.set(response);
                    $.pnotify({
                        type: 'success',
                        title: 'Sucesso',
                        text: 'Avalição salva com sucesso.'
                    });
                },
                error: function () {
                    $.pnotify({
                        type: 'error',
                        title: 'Erro',
                        text: 'Erro ao salvar a avaliação. Verifique os campos.'
                    });
                },
                complete: function () {
                    loaded();
                }
            });
        }

    }
});

var AtividadeView = Marionette.ItemView.extend({
    template: '#template-atividade',
    model: projetoLayout.model,
    ui: {
        btnSalvar: '#salvarAtividade'
    },
    events: {
        'click @ui.btnSalvar': 'actionSave'
    },
    actionSave: function () {
        var that = this;

        var values = Backbone.Syphon.serialize(this);

        this.model.save(values, {
            beforeSend: loading,
            success: function (model, response) {
                that.model.set(response);
                $.pnotify({
                    type: 'success',
                    title: 'Sucesso',
                    text: 'Dados salvos com sucesso!'
                });
            },
            error: function () {
                $.pnotify({
                    type: 'error',
                    title: 'Erro',
                    text: 'Erro ao salvar os dados. Verifique os campos.'
                });
            },
            complete: loaded
        });
    }
});

var avaliacaoIni = new AvaliacaoView();
var projetoentidade = new OrganizacaoComposite();
var atividadeIni = new AtividadeView();
var areaCapesComposite = new AreaCapesComposite();
var areaMecComposite = new AreaMecItemView();

/* Campo integração de prova com a Fábrica de Provas só aparece
 na tela caso a linha de negócio da entidade seja Pós Graduação. */

if (+G2S.loggedUser.get('id_linhadenegocio') === LINHA_DE_NEGOCIO.POS_GRADUACAO) {

    var integracaoFabricaDeProvasView = Marionette.LayoutView.extend({
        model: projetoLayout.model,
        ui: {
            nomeProvaVinculada: '#prova-vinculada',
            btnDesvincular: '#btn-desvincular',
            btnVincular: '#btn-vincular'
        },
        regions: {
            modal: '#modal-container'
        },
        onRender: function () {

            this.verificaProvaVinculada();
        },
        template: '#template-integracao-fabrica-de-provas',
        events: {
            "click #btn-vincular": "abreModal",
            "click #btn-desvincular": "desvincularProva"
        },
        abreModal: function () {
            var modalComposite = new ModalIntegracaoFabricaProvasComposite();
            this.modal.show(modalComposite);
            modalComposite.$el.modal("show");
        },
        verificaProvaVinculada: function () {
            if (this.model.get("st_provaintegracao")) {
                this.ui.nomeProvaVinculada.text(this.model.get("st_provaintegracao"));
                this.ui.btnDesvincular.removeClass("hide");
                this.ui.btnVincular.text("Alterar");
            }
        },
        desvincularProva: function () {
            var that = this;
            bootbox.confirm('Deseja realmente desvincular essa prova?', function (response) {
                if (response) {
                    // Backbone.save() não permite setar e salvar valores como null.
                    that.model.save({
                        st_provaintegracao: "",
                        st_codprovaintegracao: ""
                    }).always(function () {
                        that.render();
                    });
                }
            });
        }
    });

    var IntegracaoFabricaDeProvasCollection = Backbone.Collection.extend({
        model: Backbone.Model,
        url: 'integracao-fabrica-de-provas/listar-provas',
        parse: function (response) {
            return response.mensagem || response;
        }
    });

    var IntegracaoFabricaDeProvasItem = Marionette.ItemView.extend({
        template: '#template-linha-provas',
        tagName: 'tr'
    });

    var ModalIntegracaoFabricaProvasComposite = Marionette.CompositeView.extend({
        model: projetoLayout.model,
        template: '#modal-integracao-fabrica-de-provas',
        className: 'modal hide fade',
        collection: new IntegracaoFabricaDeProvasCollection(),
        childView: IntegracaoFabricaDeProvasItem,
        childViewContainer: 'tbody',
        emptyView: Marionette.ItemView.extend({
            tagName: 'tr',
            template: _.template('<td colspan="2">Nenhuma prova encontrada.</td>')
        }),
        ui: {
            tabela: 'table',
            form: 'form',
            btnSalvarProvaModal: '#btn-salvar-prova-modal'
        },
        events: {
            'submit @ui.form': 'actionSearch',
            'click @ui.btnSalvarProvaModal': 'vincularProva'
        },
        actionSearch: function (e) {
            var that = this;
            e && e.preventDefault ? e.preventDefault() : null;
            this.ui.tabela.removeClass('hide');

            this.collection.fetch({
                data: Backbone.Syphon.serialize(this),
                beforeSend: loading,
                complete: function (model, response) {
                    loaded();
                    that.collection.length
                        ? that.ui.btnSalvarProvaModal.removeClass("hide")
                        : that.ui.btnSalvarProvaModal.addClass("hide");
                }
            });
        },
        vincularProva: function () {
            var that = this;
            var $el_cod = this.$el.find("input[type=radio]:checked");
            var $el_titulo = $el_cod.closest('tr').find('input[type=hidden]');

            if (!$el_cod.length) {
                $.pnotify({
                    'type': "alert",
                    "title": "Alerta",
                    "text": "Selecione uma prova para vinculação."
                });

                return false;
            }

            this.model.save({
                "st_codprovaintegracao": $el_cod.val(),
                "st_provaintegracao": $el_titulo.val()
            }, {
                success: function (model, response) {
                    $.pnotify({
                        'title': 'Sucesso',
                        'text': 'Prova vinculada com sucesso.',
                        'type': 'success'
                    });
                },
                error: function (model, response) {
                    $.pnotify({
                        'title': 'Erro',
                        'texto': 'Não foi possível vincular a prova.',
                        'type': 'error'
                    });
                },
                complete: function (model, response) {
                    that.$el.modal('hide');
                    integracaoFabricaDeProvasViewIni.render();
                }
            });
        }
    });

    var integracaoFabricaDeProvasViewIni = new integracaoFabricaDeProvasView();

}
/**
 * Iniciando a funcionalidade
 */
if (projetoLayout.model.get('id_projetopedagogico')) {
    projetoLayout.model.fetch({
        complete: function () {
            G2S.layout.content.show(projetoLayout);
        }
    });
} else {
    G2S.layout.content.show(projetoLayout);
}

/**
 * Regioes
 */
G2S.layout.addRegions({
    turma: '#turma-content',
    projeto: '#projeto-content',
    disciplina: '#disciplina-content'
});


/**
 * Turma
 */
var TurmaModel = Backbone.Model.extend({
    idAttribute: 'id_turma',
    defaults: {
        id_turma: null,
        id_usuariocadastro: null,
        id_situacao: null,
        id_entidadecadastro: null,
        st_turma: '',
        st_tituloexibicao: '',
        nu_maxalunos: null,
        dt_inicioinscricao: null,
        dt_fiminscricao: null,
        dt_inicio: null,
        dt_fim: null,
        dt_cadastro: null,
        bl_ativo: false,
        id_evolucao: null
    }
});

var TurmaCollection = Backbone.Collection.extend({
    model: TurmaModel,
    url: '/api/turma/',
    parse: function (data) {
        if (data.tipo && data.tipo != 1) {
            $.pnotify({'type': data.tipo, 'text': data.mensagem});
        }
        return data.mensagem || data;
    }
});

var TurmaView = Marionette.ItemView.extend({
    model: new TurmaModel(),
    template: '#turma-view',
    tagName: 'tr',
    attributes: {
        style: 'cursor: pointer;'
    },
    events: {
        'click': 'showProjetos'
    },
    showProjetos: function (e) {
        var that = this;
        var elem = $(e.target).closest('tr');

        if (elem.closest('.info').length)
            return;

        elem.closest('tbody').find('tr.info').removeClass('info');
        elem.addClass('info');

        loading();

        ProjetoIni.collection.reset();
        DisciplinaIni.collection.reset();

        ProjetoIni.collection.url = '/conferencia-turma/get-projeto-pedagogico/id_turma/' + that.model.id,
            ProjetoIni.collection.fetch({
                success: function () {
                    G2S.layout.projeto.show(ProjetoIni);
                    loaded();
                }
            });
    }
});

var TurmaCompositeView = Marionette.CompositeView.extend({
    template: '#turma-template',
    collection: new TurmaCollection(),
    childView: TurmaView,
    childViewContainer: "tbody"
});


/**
 * Projeto
 */
var ProjetoModel = Backbone.Model.extend({
    idAttribute: "id_projetopedagogico",
    defaults: {
        id_turma: null,
        st_turma: "",
        id_projetopedagogico: null,
        st_projetopedagogico: "",
        id_entidade: null,
        bl_confirmada: false
    }
});

var ProjetoCollection = Backbone.Collection.extend({
    model: ProjetoModel,
    url: "/conferencia-turma/get-projeto-pedagogico/",
    parse: function (data) {
        if (data.tipo && data.tipo != 1) {
            $.pnotify({'type': data.tipo, 'text': data.mensagem});
        }
        return data.mensagem || data;
    }
});

var ProjetoView = Marionette.ItemView.extend({
    model: new ProjetoModel(),
    template: '#projeto-view',
    tagName: 'tr',
    attributes: {
        style: 'cursor: pointer;'
    },
    events: {
        'click': 'showDisciplinas'
    },
    showDisciplinas: function (e) {
        var that = this;
        var elem = $(e.target);

        if (elem.closest('.actionXls').length || elem.closest('.info').length)
            return;

        elem.closest('tbody').find('tr.info').removeClass('info');
        elem.closest('tr').addClass('info');

        loading();

        DisciplinaIni.collection.reset();

        DisciplinaIni.collection.url = '/conferencia-turma/get-disciplina-projeto/id_turma/' + that.model.get('id_turma') + '/id_projetopedagogico/' + this.model.id,
            DisciplinaIni.collection.fetch({
                success: function () {
                    G2S.layout.disciplina.show(DisciplinaIni);
                    loaded();
                }
            });
    }
});

var ProjetoCompositeView = Marionette.CompositeView.extend({
    template: '#projeto-template',
    collection: new ProjetoCollection(),
    childView: ProjetoView,
    childViewContainer: 'tbody'
});


/**
 * Disciplina
 */
var DisciplinaModel = Backbone.Model.extend({
    idAttribute: 'id_disciplina',
    defaults: {
        id_turma: null,
        id_projetopedagogico: null,
        id_entidade: null,
        id_disciplina: null,
        st_disciplina: '',
        st_saladeaula: '',
        st_projetopedagogico: '',
        dt_inicioinscricao: null,
        dt_fiminscricao: null,
        dt_abertura: null,
        dt_encerramento: null,
        st_professor: '',
        st_sistema: '',
        st_coordenadorpp: '',
        st_codsistema: '',
        st_salareferencia: '',
        st_processada: '',
        st_nomemoodle: ''
    }
});

var DisciplinaCollection = Backbone.Collection.extend({
    model: DisciplinaModel,
    url: '/conferencia-turma/get-disciplina-projeto/',
    parse: function (data) {
        if (data.tipo && data.tipo != 1) {
            $.pnotify({'type': data.tipo, 'text': data.mensagem});
        }
        return data.mensagem || data;
    }
});

var DisciplinaView = Marionette.ItemView.extend({
    model: new DisciplinaModel(),
    template: '#disciplina-view',
    tagName: 'div',
    className: 'row-fluid'
});

var DisciplinaCompositeView = Marionette.CompositeView.extend({
    template: '#disciplina-template',
    collection: new DisciplinaCollection(),
    childView: DisciplinaView,
    childViewContainer: '#discilina-body'
});


// RENDER OS HTMLS
var TurmaIni = new TurmaCompositeView();
TurmaIni.collection.fetch({
    success: function () {
        G2S.layout.turma.show(TurmaIni);
        loaded();
    }
});

var ProjetoIni = new ProjetoCompositeView();
G2S.layout.projeto.show(ProjetoIni);

var DisciplinaIni = new DisciplinaCompositeView();
G2S.layout.disciplina.show(DisciplinaIni);

// Ajustar altura das listas
$(window).on('load resize', function () {
    var maxHeight = $(window).height() - $('#top').outerHeight() - $('#menu').outerHeight() - $('#breadcrumb').outerHeight() - 80;
    $('.list-panel').css('maxHeight', maxHeight);
}).trigger('load');
/**
 * Arquivos Js para funcionalidade Contatos Site
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2013-11-05
 */

var Prevenda = Backbone.Model.extend({});
/**
 * Collection to Contatos Site
 * @type Backbone Collection
 * @exp;Backbone
 * @pro;Collection
 * @call;extend
 */
var pageableContatos = new Backbone.Collection.extend({
    model: Prevenda
});

var pageableGrid = null;

/**
 * View to render template Filter
 * @type Backbone View
 * @exp;Backbone
 * @pro;View
 * @call;extend
 */
var FiltroView = Backbone.View.extend({
    tagname: "div",
    className: "span12",
        initialize: function () {
            this.stopListening();
            _.bindAll(this, "render");
            this.render = _.bind(this.render, this);
        },
        render: function () {
            var elTemplate = _.template($("#template-filtro-contatos-site").html(), {});
            this.el.innerHTML = elTemplate;
            setDatePickerMax();
            return this;
        },
    submitForm: function () {
            //Render grid result
            this.renderGrid({
                elementGrid: $("#containerGrid")
            });
        $("#gerarXls").prop("disabled", false);
            return this;
        },
        renderGrid: function (options) {
            loading();
            $(options.elementGrid).html($("#template-grid-contatos-site").html());
            $("#tblGrid").empty();

            var params = "dt_inicio=" + $("#dt_inicio").val() +
                "&dt_fim=" + $("#dt_fim").val() +
                "&id_situacao=" + $("#id_situacao option:selected").val() +
                "&id_atendente=" + $("#id_atendente option:selected").val() +
                "&dt_interacao=" + $("#dt_interacao").val();

            var PageableContatos = Backbone.PageableCollection.extend({
                model: Prevenda,
                url: "api/contatos-site?" + params,
                state: {
                    pageSize: 20
                },
                mode: "client" // page entirely on the client side
            });

            var RowGrid = Backgrid.Row.extend({
                initialize: function () {
                    RowGrid.__super__.initialize.apply(this, arguments);
                    this.$el.css("cursor", "pointer");
                },
                habilitarBotoes: function () {
                    if (!pageableGrid.getSelectedModels().length) {
                        $("button.btn-distruibir, button.btn-descartar").attr("disabled", "disabled");
                    } else {
                        $("button.btn-distruibir, button.btn-descartar").removeAttr("disabled");
                    }
                },
                events: {
                    "change input[type=checkbox]": "habilitarBotoes"
                }
            });

            pageableContatos = new PageableContatos();

            pageableGrid = new Backgrid.Grid({
                row: RowGrid,
                columns: [{
                    name: "id_prevendaproduto",
                    label: "ID",
                    editable: false,
                    cell: "string"
                }, {
                    name: "st_nomecompleto",
                    label: "Nome",
                    editable: false,
                    cell: "string"
                }, {
                    name: "st_email",
                    label: "E-mail",
                    editable: false,
                    cell: "string"
                }, {
                    name: "nu_telefoneresidencial",
                    label: "Telefone Residencial",
                    editable: false,
                    cell: "string"
                }, {
                    name: "nu_telefonecelular",
                    label: "Telefone Celular",
                    editable: false,
                    cell: "string"
                }, {
                    name: "dt_cadastro",
                    label: "Data Abertura",
                    editable: false,
                    cell: "string"
                }, {
                    name: "dt_ultimainteracao",
                    label: "Data da Última Interação",
                    editable: false,
                    cell: "string"
                }, {
                    name: "st_atendente",
                    label: "Atendente",
                    editable: false,
                    cell: "string"
                }, {
                    name: "st_produto",
                    label: "Curso",
                    editable: false,
                    cell: "string"
                }, {
                    name: "st_situacao",
                    label: "Situação",
                    editable: false,
                    cell: "string"
                }, {
                    name: "",
                    label: "Interações",
                    editable: false,
                    cell: Backgrid.Cell.extend({
                        className: "custom_class",
                        render: function () {
                            var idVenda = this.model.get("id_prevenda");
                            this.$el.html(
                                "<button id='btn-interacao' data-id='" + idVenda + "' " +
                                " class='btn btn-default btn-interacao'>" +
                                "Visualizar" +
                                "</button>"
                            );
                            return this;
                        }
                    }),
                }, {
                    name: "",
                    label: "Ações",
                    cell: "select-row",
                    editable: false,
                    headerCell: "select-all"
                }],
                collection: pageableContatos,
                className: "backgrid backgrid-selectall table table-striped table-bordered table-hover"
            });

            // Render the grid and attach the root to your HTML document
            $("#tblGrid").append(pageableGrid.render().el);

            // Initialize the paginator
            var paginator = new Backgrid.Extension.Paginator({
                collection: pageableContatos
            });

            // Render the paginator
            $("#tblGrid").after(paginator.render().el);

            // Fetch some countries from the url
            pageableContatos.fetch({
                reset: true,
                success: function () {
                    loaded();
                },
                beforeSend: function () {
                    loading();
                },
                afterSend: function () {
                    loading();
                }
            });
            return this;
        },
        saveReadMensages: function () {
            var self = this;
            //Salva registros marcados como lido

            var modelosSelecionadas = pageableGrid.getSelectedModels();
            var collectionEnviar = new Backbone.Collection();
            collectionEnviar.add(modelosSelecionadas);

            if (modelosSelecionadas.length === 0) {
                $.pnotify({
                    title: "Aviso",
                    text: "Por favor, selecione as prés para serem distribuidas!",
                    type: "warning"
                });
                return;
            }

            if (!$("#id_atendente_distribuir option:selected").val()) {
                $.pnotify({
                    title: "Aviso",
                    text: "Por favor, selecione um atendente!",
                    type: "warning"
                });
                return;
            }

            bootbox.confirm("Confirma a distribuição das pré-matrículas para o atendente selecionado?",
                function (result) {
                    /*função a ser executada caso positivo*/
                    if (result) {
                        $.ajax({
                            dataType: "json",
                            type: "post",
                            url: "/contatos-site/save-read-mensages",
                            data: {
                                "modelos": collectionEnviar.toJSON(),
                                "id_atendente": $("#id_atendente_distribuir option:selected").val()
                            },
                            beforeSend: function () {
                                loading();
                            },
                            success: function (data) {
                                loaded();
                                //Render grid result
                                self.renderGrid({
                                    elementGrid: $("#containerGrid")
                                });

                                $.pnotify({title: data.title, text: data.text, type: data.type});
                            }
                        });
                    }
                }
            );

            return false;
        },
        saveDiscardedMessages: function () {
            var self = this;
            //Salva registros marcados como lido
            var modelosSelecionadas = pageableGrid.getSelectedModels();
            var collectionEnviar = new Backbone.Collection();
            collectionEnviar.add(modelosSelecionadas);

            if (modelosSelecionadas.length === 0) {
                $.pnotify({
                    title: "Aviso",
                    text: "Por favor, selecione as prés a serem descartadas!",
                    type: "warning"
                });
                return;
            }

            bootbox.dialog({
                title: "Digite o motivo do descarte:",
                closeButton: true,
                message: "<div align='center'>" +
                "<textarea cols='10' id='tarea' rows='10' " +
                " style='margin: 0px 0px 10px; width: 493px; height: 149px;'></textarea> " +
                "</div> ",
                buttons: {
                    cancel: {
                        label: "Cancelar",
                        className: "btn-warning"
                    },
                    success: {
                        label: "Descartar",
                        className: "btn-danger",
                        callback: function () {
                            var result = document.getElementById("tarea").value;

                            if (!result) {
                                $.pnotify({
                                    title: "Aviso",
                                    text: "Por favor, digite o motivo do descarte!",
                                    type: "warning"
                                });
                                return;
                            }

                            $.ajax({
                                dataType: "json",
                                type: "post",
                                url: "/contatos-site/save-discarded-messages",
                                data: {
                                    "modelos": collectionEnviar.toJSON(),
                                    "st_descarte": result
                                },
                                beforeSend: function () {
                                    loading();
                                },
                                success: function (data) {
                                    //Render grid result
                                    self.renderGrid({
                                        elementGrid: $("#containerGrid")
                                    });
                                    loaded();
                                    $.pnotify({title: data.title, text: data.text, type: data.type});
                                }
                            });
                        }
                    }
                }
            });

            return false;
        },
        gerarXls: function () {
            var params = "dt_inicio=" + $("#dt_inicio").val() +
                "&dt_fim=" + $("#dt_fim").val() +
                "&id_situacao=" + $("#id_situacao option:selected").val() +
                "&id_atendente=" + $("#id_atendente option:selected").val() +
                "&dt_interacao=" + $("#dt_interacao").val();

            var url = "/contatos-site/gerar-xls/?" + params;
            window.open(url, "_blank");
        },
        desabilitaXls: function () {
            $("#gerarXls").prop("disabled", true);
        },
    visualizarInteracoes: function (event) {
        var idPrevenda = $(event.currentTarget).data("id");

        $.ajax({
            dataType: "html",
            type: "post",
            url: "/contatos-site/read-interacoes-prevenda",
            data: {
                "id_prevenda": idPrevenda,
            },
            beforeSend: function () {
                loading();
            },
            success: function (data) {
                //Render grid result
                loaded();
                bootbox.dialog({
                    size: "small",
                    title: "Interações",
                    closeButton: true,
                    message: data ? data : "<div align='center'>Nenhuma interação encontrada</div>",
                });
            }
        });

        return false;
    },
    resetForm: function () {
        $("#formFiltro")[0].reset();
    },
        events: {
            "click .btn-pesquisar": "submitForm",
            "click .btn-limpar-campos": "resetForm",
            "click .btn-distruibir": "saveReadMensages",
            "click .btn-descartar": "saveDiscardedMessages",
            "click #gerarXls": "gerarXls",
            "change #id_situacao": "desabilitaXls",
            "change #dt_inicio": "desabilitaXls",
            "change #dt_fim": "desabilitaXls",
            "click .btn-interacao": "visualizarInteracoes"
        }
});

//Render View
new FiltroView({el: $("#containerAll")}).render();


/**
 * Created by Denise Xavier
 * @data 2015-05-12
 */
var request = {};
var comboReady = false;
var grid;

var ProfessorModel = Backbone.Model.extend({
    idAttribute: 'id_usuario',
    defaults: {
        id_usuario: null,
        st_nomecompleto: '',
        id_perfil: null
    }
});

var collectionUsuarioExtend = Backbone.Collection.extend({
    model: ProfessorModel,
    url: '/default/professores/get-professores'
});
var collectionUsuario = false;
if (!collectionUsuario) {
    $.ajaxSetup({async: false});
    collectionUsuario = new collectionUsuarioExtend();
    collectionUsuario.fetch();
    $.ajaxSetup({async: true});
}

var VwProfessorSalaDeAulaLoteCollection = Backbone.PageableCollection.extend({
    url: '/professores/pesquisa-professor',
    model: VwProfessorSalaDeAulaLote,
    mode: "client"
});
var colecctionProf = new VwProfessorSalaDeAulaLoteCollection();

var SearchView = Marionette.ItemView.extend({
    template: '#search-view',
    tagName: 'div',
    onShow: function () {
        this.$el.find('input').first().focus();
        var elem = this.$el;

        var DisciplinaModel = Backbone.Model.extend({
            idAttribute: 'id_disciplina',
            defaults: {
                id_disciplina: null,
                st_disciplina: ''
            }
        });
        loading();

        ComboboxView({
                el: elem.find('[name="id_usuario"]'),
                model: ProfessorModel,
                //url: '/default/professores/get-professores',
                collection: collectionUsuario.toJSON(),
                optionLabel: 'st_nomecompleto',
                emptyOption: '[Selecione]',
                parse: function (response) {
                    if (!response.length) {
                        $.pnotify({
                            title: 'Aviso',
                            type: 'warning',
                            text: 'Nenhum professor encontrado'
                        });
                    }
                    return response;
                }
            }, function (elem, composite) {
                if (comboReady)
                    loaded();
                comboReady = true;
                elem.select2();
            }
        );


        ComboboxView({
            el: elem.find('[name="id_disciplina"]'),
            model: DisciplinaModel,
            url: '/default/professores/get-disciplinas',
            optionLabel: 'st_disciplina',
            emptyOption: '[Selecione]',
            parse: function (response) {
                if (!response.length) {
                    $.pnotify({
                        title: 'Aviso',
                        type: 'warning',
                        text: 'Nenhuma disciplina encontrado'
                    });
                }

                return response;
            }
        }, function (elem, composite) {
            if (comboReady)
                loaded();
            comboReady = true;
            elem.select2();
        });
    },
    onRender: function () {
        var that = this;
    },
    events: {
        'submit form': 'actionSearch',
        'click .actionAdd': 'actionAdd',
        'click .actionRemove': 'actionRemove',
        'click .add-professor': 'saveAddProfessor',
        'click .add-professor-remove': 'saveAddProfessor',
        'click .close-modal-remove': 'actionCloseModal',
        'click .btn-gerar-xls': 'actionGerarXlsResultados'
    },
    actionSearch: function (e) {
        e.preventDefault();

        var that = this;
        var elem = this.$el;

        request.st_saladeaula = elem.find('[name="st_saladeaula"]').val();
        request.id_usuario = Number(elem.find('[name="id_usuario"]').val());
        request.id_disciplina = Number(elem.find('[name="id_disciplina"]').val());
        request.dt_aberturainicio = (elem.find('[name="dt_aberturainicio"]').val());
        request.dt_aberturafim = (elem.find('[name="dt_aberturafim"]').val());
        request.dt_encerramentoinicio = (elem.find('[name="dt_encerramentoinicio"]').val());
        request.dt_encerramentofim = (elem.find('[name="dt_encerramentofim"]').val());
        request.bl_professores_indefinidos = (elem.find('[name="bl_professores_indefinidos"]').is(':checked'));


        if (request.st_saladeaula == '' && !request.id_usuario && !request.id_disciplina && request.dt_aberturainicio == '' && request.dt_aberturafim == ''
            && request.dt_encerramentoinicio == '' && request.dt_encerramentofim == '') {
            $.pnotify({
                type: 'warning',
                title: 'Aviso',
                text: 'Selecione um filtro para realizar a pesquisa.'
            });
            return false;
        }
        if (request.dt_encerramentoinicio != '' || request.dt_encerramentofim != '') {
            if (request.dt_encerramentoinicio == '' || request.dt_encerramentofim == '') {
                $.pnotify({type: 'warning', title: 'Aviso', text: 'Selecione um intervalo de datas de encerramento.'});
                return false;
            }
            var dataencerramento = compareDate(request.dt_encerramentoinicio, request.dt_encerramentofim);
            if (dataencerramento == -1) {
                $.pnotify({
                    type: 'warning',
                    title: 'Aviso',
                    text: 'A data de início não deve ser maior que a data de término.'
                });
                return false;
            }

        }

        if (request.dt_aberturainicio != '' || request.dt_aberturafim != '') {
            if (request.dt_aberturainicio == '' || request.dt_aberturafim == '') {
                $.pnotify({type: 'warning', title: 'Aviso', text: 'Selecione um intervalo de datas abertura.'});
                return false;
            }
            var dataabertura = compareDate(request.dt_aberturainicio, request.dt_aberturafim);
            if (dataabertura == -1) {
                $.pnotify({
                    type: 'warning',
                    title: 'Aviso',
                    text: 'A data de início não deve ser maior que a data de término.'
                });
                return false;
            }

        }

        that.renderGrid();
    },
    renderGrid: function () {
        var that = this;
        var elem = this.$el;
        var containerGrid = elem.find('[id="content-result-sub"]').empty();
        var containerAction = elem.find('[id="content-result"]');
        var columns = null;
        columns = [
            {name: "", cell: "select-row", headerCell: "select-all"},
            {name: "st_saladeaula", label: "Sala de Aula", editable: false, cell: "string"},
            {name: "st_disciplina", label: "Disciplina", editable: false, cell: "string"},
            {name: "st_nomecompleto", label: "Professor", editable: false, cell: "string"},
            {name: "st_titular", label: "Titular", editable: false, cell: "string"},
            {name: "st_dtabertura", label: "Abertura", editable: false, cell: "string"},
            {name: "st_dtencerramento", label: "Encerramento", editable: false, cell: "string"}

        ];

        grid = new Backgrid.Grid({
            className: 'backgrid table table-bordered projeto-origem',
            columns: columns,
            collection: colecctionProf
        });

        colecctionProf.fetch({
            data: request,
            complete: function (collection, response) {
                containerAction.show();
                containerGrid.empty().append(grid.render().el);
                containerGrid.show();
                loaded();
            },
            beforeSend: function () {
                loading();
            }
        });

    },
    actionGerarXlsResultados: function () {
        var url = '/default/professores/gerar-xls-pesquisa-professor?' + this.serialize(request);

        var win = window.open(url, "_blank");

        if (!win) {
            $.pnotify({
                type: 'warning',
                title: 'Aviso',
                text: 'Seu navegador não permitiu abrir o popup.'
            });
        }
    },
    serialize: function (obj) {
        var str = [];
        for (var p in obj)
            if (obj.hasOwnProperty(p)) {
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            }
        return str.join("&");
    },
    actionAdd: function () {
        var elem = this.$el;
        var selectedModels = grid.getSelectedModels();
        if (selectedModels.length > 0) {
            var collectionUsuarioAdd = collectionUsuario;
            $.each(collectionUsuarioAdd.models, function (index, model) {
                model.set('id_usuario', model.get('id_usuario') + '#' + model.get('id_perfil'));
            });

            ComboboxView({
                el: elem.find('[name="id_usuario_add"]'),
                model: ProfessorModel,
                collection: collectionUsuarioAdd.toJSON(),
                optionLabel: 'st_nomecompleto',
                emptyOption: '[Selecione]'
            });
            elem.find('[id="modalAddProfessor"]').modal('show');
        } else {
            $.pnotify({
                type: 'warning',
                title: 'Aviso',
                text: 'Selecione uma ou mais salas para prosseguir.'
            });
            return false;
        }


    },
    saveAddProfessor: function () {
        var that = this;
        var elem = this.$el;
        var id_usuario = elem.find('[name="id_usuario_add"]').val();
        if ((id_usuario) == '') {
            $.pnotify({
                type: 'warning',
                title: 'Aviso',
                text: 'Selecione um professor.'
            });
            return false;
        }
        //Salva os professores selecionados
        var selectedModels = grid.getSelectedModels();
        var salasDeAula = [];
        $.each(selectedModels, function (index, value) {
            salasDeAula.push(value.attributes.id_saladeaula);
        });
        $.ajax({
            dataType: 'json',
            type: 'post',
            url: '/default/professores/add-professor-lote',
            data: {
                'salas': salasDeAula,
                'id_usuario': id_usuario,
                'bl_titular': elem.find('[name="bl_titular_add"]').val()
            },
            beforeSend: function () {
                loading();
            },
            success: function (response) {
                $.pnotify({title: response.retorno, text: response.mensagem, type: response.type});
                that.renderGrid();
                elem.find('[id="modalAddProfessor"]').modal('hide');
            },
            error: function (response) {
                $.pnotify({title: response.retorno, text: response.mensagem, type: response.type});
            },
            complete: function () {
                loaded();
            }
        });

    },
    actionRemove: function () {
        var that = this;
        var selectedModels = grid.getSelectedModels();
        if (selectedModels.length > 0) {
            var perfilRef = [];
            var arraySemProfessor = [];
            $.each(selectedModels, function (index, value) {
                perfilRef.push(value.attributes.id_perfilreferencia);
                if (!Number(value.attributes.id_usuario)) {
                    arraySemProfessor.push(value.attributes.id_saladeaula);
                }
            });
            var mensagem;
            if (arraySemProfessor.length > 0) {
                mensagem = 'Tem certeza que deseja remover os professores das salas selecionadas? <br> Você selecionou ' + arraySemProfessor.length + ' salas que nao possuem professores.';
            } else {
                mensagem = 'Tem certeza que deseja remover os professores das salas selecionadas?';
            }
            bootbox.confirm(mensagem, function (result) {
                if (result) {
                    $.ajax({
                        dataType: 'json',
                        type: 'post',
                        url: '/default/professores/remove-professor-lote',
                        data: {'perfilRef': perfilRef},
                        beforeSend: function () {
                            loading();
                        },
                        success: function (response) {
                            $.pnotify({title: response.retorno, text: response.mensagem, type: response.type});
                            that.actionAddBeforeRemove();
                        },
                        error: function (response) {
                            $.pnotify({title: response.retorno, text: response.mensagem, type: response.type});
                        },
                        complete: function () {
                            loaded();

                        }
                    });
                }
            });
        } else {
            $.pnotify({title: 'Atenção', text: 'Selecione registros para serem removidos', type: 'warning'});
        }
    },
    actionAddBeforeRemove: function () {
        var elem = this.$el;
        elem.find('[id="modalAlunoConclusaoLabel"]').text('Professor(es) removido(s) com sucesso! Deseja adicionar um novo professor a essas salas de aula?');
        elem.find('.add-professor').addClass('hide');
        elem.find('.cancel').addClass('hide');
        elem.find('.close-modal-remove').removeClass('hide');
        elem.find('.add-professor-remove').removeClass('hide');
        var collectionUsuarioAdd = collectionUsuario;
        $.each(collectionUsuarioAdd.models, function (index, model) {
            model.set('id_usuario', model.get('id_usuario') + '#' + model.get('id_perfil'));
        });

        ComboboxView({
            el: elem.find('[name="id_usuario_add"]'),
            model: ProfessorModel,
            collection: collectionUsuarioAdd.toJSON(),
            optionLabel: 'st_nomecompleto',
            emptyOption: '[Selecione]'
        });
        elem.find('[id="modalAddProfessor"]').modal('show');
    },
    actionCloseModal: function () {
        var that = this;
        var elem = this.$el;
        elem.find('[id="modalAlunoConclusaoLabel"]').text('Selecione o professor que deseja adicionar as salas selecionadas');
        elem.find('.add-professor').removeClass('hide');
        elem.find('.cancel').removeClass('hide');
        elem.find('.close-modal-remove').addClass('hide');
        elem.find('.add-professor-remove').addClass('hide');
        elem.find('[id="modalAddProfessor"]').modal('hide');
        that.renderGrid();
    },
});


G2S.layout.addRegions({
    search: '#search-content',
    modal: '#modal-content'
});
var SearchIni = new SearchView();
G2S.layout.search.show(SearchIni);

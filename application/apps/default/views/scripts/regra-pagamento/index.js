/*******************************************************************************
 * Imports
 ******************************************************************************/
$.ajaxSetup({'async': false});
$.getScript('/js/backbone/views/RegraPagamento/RegraPagamentoView.js');

/*******************************************************************************
 *  Views
 ******************************************************************************/

$(function() {
    var view = new RegraPagamentoView();
    G2S.layout.content.show(view);
});

$.ajaxSetup({'async': true});
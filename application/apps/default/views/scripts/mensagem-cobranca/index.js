/*******************************************************************************
 * Imports
 ******************************************************************************/
$.ajaxSetup({'async': false});
$.getScript('/js/backbone/models/mensagem-cobranca.js');
$.getScript('/js/backbone/models/texto-sistema.js');

/*******************************************************************************
 *  Views
 ******************************************************************************/
var MensagemCobrancaView = Backbone.Marionette.ItemView.extend({
    template: _.template($('#tpl').html()),
    events: {
        'click #btn-add': 'showForm'
    },
    showForm: function() {
        var formView = new MensagemCobrancaFormView({model: new MensagemCobranca()});
        $('#form-container').html(formView.render().$el);
        formView.renderTextoSistemaSelect();
    }
});

var MensagemCobrancaFormView = Backbone.Marionette.ItemView.extend({
    template: _.template($('#tpl-form').html()),
    initialize: function() {
        this.model = new MensagemCobranca();
    },
    events: {
        'click #btn-save': 'save'
    },
    validate: function() {
        var message = '';

        if (!this.model.get('nu_diasatraso')) {
            message += 'Dias para envio, ';
        }

        if (!this.model.get('id_textosistema')) {
            message += 'Texto da mensagem, ';
        }

        if (message) {
            message = 'O(s) campo(s) ' + message + 'precisa(m) ser preenchido(s)';
            $.pnotify({
                'title': 'Erro!',
                'text': message,
                'type': 'error'
            });

            return false;
        }

        return true;
    },
    save: function() {
        //this.model.set($('#form-mensagem-cobranca').serialize());
        this.model.set({'nu_diasatraso': $('#nu_diasatraso').val()});
        this.model.set({'id_textosistema': $('#id_textosistema option:selected').val()});
        this.model.set({'bl_antecedencia': $('#bl_antecedencia').val()});
        this.model.set({'bl_ativo': $('#bl_ativo').is(':checked')});

        var that = this;
        if (this.validate()) {
            this.model.save(this.model.toJSON(), {
                success: function(model, response) {
                    $.pnotify({'title': 'Sucesso', 'type': 'success', 'text': 'Mensagem de Cobrança salvo com sucesso.'});
                    that.renderListagem();
                    that.remove();
                },
                error: function(model, response) {
                    $.pnotify({'title': 'Erro', 'type': 'error', 'text': 'Erro ao salvar Mensagem de Cobrança.'});
                }
            });

        }
    },
    onRender: function() {
        this.renderTextoSistemaSelect();
    },
    renderTextoSistemaSelect: function() {
        var selected = '';

        if (this.model.get('id_textosistema')) {
            selected = this.model.get('id_textosistema');
        }

        var TextoSistemaCollection = Backbone.Collection.extend({
            model: TextoSistema,
            url: 'textos/retornar-textos-mensagem-cobranca'
        });

        var collection = new TextoSistemaCollection();
        collection.fetch();

        var view = new SelectView({
            el: $('#id_textosistema'),
            collection: collection,
            childViewOptions: {
                optionLabel: 'st_textosistema',
                optionValue: 'id_textosistema',
                optionSelected: selected
            }
        });

        view.render();
    },
    renderListagem: function() {
        //Renderiza a listagem
        var tableView = new TableView();
        tableView.render();
        $("#list-container").html(tableView.render().$el);
    }
});

var RowView = Backbone.Marionette.ItemView.extend({
    tagName: "tr",
    template: '#tpl-row',
    events: {
        'click .btn-edit': 'showForm',
        'click .btn-delete': 'delete'
    },
    showForm: function() {
        var formView = new MensagemCobrancaFormView();
        formView.model = this.model;
        $('#form-container').html(formView.render().$el);
        formView.renderTextoSistemaSelect();
    },
    delete: function() {
        var that = this;
        this.model.destroy({
            wait: true,
            success: function(model, response) {
                that.remove();
            },
            error: function(model, response) {
                $.pnotify({'title': 'Erro', 'text': 'Erro ao remover Mensagem de Cobrança', 'type': 'error'});
            }
        });
    },
    onRender: function() {
    }
});

var TableView = Marionette.CompositeView.extend({
    template: '#tpl-table',
    childView: RowView,
    childViewContainer: "#row-container",
    initialize: function() {
        var Collection = Backbone.Collection.extend({
            model: MensagemCobranca,
            url: function() {
                return 'api/mensagem-cobranca';
            }
        });
        this.collection = new Collection();
        this.collection.fetch();
    }
});

var SelectView = Backbone.Marionette.CollectionView.extend({
    initialize: function() {
        this.childView = Marionette.ItemView.extend({
            tagName: 'option',
            initialize: function(options) {
                this.optionSelected = options.optionSelected;
                this.optionValue = options.optionValue ? options.optionValue : 'id';
                this.template = _.template('<%= ' + options.optionLabel + ' %>');
            },
            onRender: function() {
                this.$el.attr('value', this.model.get(this.optionValue));
                if (this.optionSelected == this.model.get(this.optionValue)) {
                    this.$el.attr('selected', 'selected');
                }
            }
        });
    }

});

//Renderiza a view
var view = new MensagemCobrancaView();
$("#container").html(view.render().$el);

//Renderiza a listagem
var tableView = new TableView();
tableView.render();
$("#list-container").append(tableView.render().$el);

$.ajaxSetup({'async': true});
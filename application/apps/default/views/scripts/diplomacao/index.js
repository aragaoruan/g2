/**
 * Permissões
 */
var PermissaoEditar = {
    dtEnvioPolo: false,
    dtEntregueAluno: false,
    dadosDiploma: false
};

const FUNC_DIPLOMACAO = 787;
const FUNC_EDIT_DT_ENVIO_POLO = 790;
const FUNC_EDIT_DT_ENTREGUE_ALUNO = 791;

/**
 * VIEWS
 */
var mainInit,
    researchInit,
    resultsInit,
    modalGerarInit;

/**
 * COLLETION
 */
var AproveitamentoCollection = Backbone.Collection.extend({
    url: 'api/aproveitamento',
    model: Backbone.Model.extend({
        idAttribute: 'id_aproveitamento',
        defaults: {
            id_aproveitamento: null,
            id_matricula: null,
            bl_ativo: null
        }
    })
});

var MainLayoutView = Marionette.LayoutView.extend({
    el: '#main-region',
    regions: {
        research: '#form-research-region'
    },
    initialize: function () {
        mainInit = this;
        researchInit = new FormResearch();
        this.research.show(researchInit);
    }
});


var FormResearch = Marionette.LayoutView.extend({
    template: '#template-research',
    model: new Backbone.Model(),
    regions: {
        resultsRegion: '#results-region',
        modalGerar: '#modal-region'
    },
    ui: {
        // FORM
        form: 'form',
        // INPUTS
        inputDtColocaoInicio: '#dt_colacao_inicio',
        inputDtColocaoTermino: '#dt_colacao_termino',
        inputEvolucao: '#id_evolucaodiplomacao',
        inputProjeto: '#id_projetopedagogico',
        // DIVS
        divHide: '.divHide',
        divAptoadiplomar: '#div_aptoadiplomar',
        divDiplomagerado: '#div_diplomagerado',
        divEnviadoassinatura: '#div_enviadoassinatura',
        divRetornadoassinatura: '#div_retornadoassinatura',
        divEnviadosecretaria: '#div_enviadosecretaria',
        divEnviadoregistro: '#div_enviadoregistro',
        divRetornoregistro: '#div_retornoregistro',
        divEnviopolo: '#div_enviopolo',
        divEntreguealuno: '#div_entreguealuno',
        divHistoricogerado: '#div_historicogerado',
        divEquivalenciagerado: '#div_equivalenciagerado',
        // BUTTONS
        btnLimpar: '#btnLimpar',
        btnPesquisar: '#btnPesquisar'
    },
    events: {
        'submit @ui.form': 'pesquisarAlunosDiplomacao',
        'reset @ui.form': 'limparResultados'
    },
    onRender: function () {
        this.loadCombobox();
        if (VerificarPerfilPermissao(PERMISSAO.DIPLOMACAO_EDITAR_DATA_ENVIO_POLO, FUNC_EDIT_DT_ENVIO_POLO).length) {
            PermissaoEditar.dtEnvioPolo = true;
        }
        if (VerificarPerfilPermissao(PERMISSAO.DIPLOMACAO_EDITAR_DATA_ENTREGUE_ALUNO, FUNC_EDIT_DT_ENTREGUE_ALUNO).length) {
            PermissaoEditar.dtEntregueAluno = true;
        }
        // Verificar permissão de editar Dados do Diploma
        if (VerificarPerfilPermissao(PERMISSAO.DIPLOMACAO_EDITAR_REGISTRO_DIPLOMA, FUNC_DIPLOMACAO).length) {
            PermissaoEditar.dadosDiploma = true;
        }
    },
    loadCombobox: function () {
        var that = this;
        //Evolucao
        ComboboxView({
            el: that.ui.inputEvolucao,
            url: '/api/evolucao/st_tabela/tb_matricula/st_campo/id_evolucaodiplomacao',
            optionLabel: 'st_evolucao',
            emptyOption: 'Todas',
            model: Evolucao.extend({
                idAttribute: 'id_evolucao'
            }),
            onChange: function (modelEv) {
                //funcao para exibir campos de datas de acordo com a evolucao selecionada
                that.ui.divHide.addClass('hide');
                if (modelEv)
                    that.showComboDate(modelEv.get('id_evolucao'));
            }
        });

        //Projeto Pedagogico
        ComboboxView({
            el: that.ui.inputProjeto,
            url: '/conclusao/retorna-projeto-entidade/id_entidade/' + G2S.loggedUser.get('id_entidade'),
            optionLabel: 'st_projetopedagogico',
            emptyOption: 'Todos',
            model: ProjetoPedagogico.extend({
                idAttribute: 'id_projetopedagogico'
            })
        });
    },
    showComboDate: function (id_evolucao) {
        switch (id_evolucao) {
            case EVOLUCAO.TB_MATRICULA.DIPLOMACAO.APTO_A_DIPLOMAR:
                this.ui.divAptoadiplomar.removeClass('hide');
                break;

            case EVOLUCAO.TB_MATRICULA.DIPLOMACAO.DIPLOMA_GERADO:
                this.ui.divDiplomagerado.removeClass('hide');
                break;

            case EVOLUCAO.TB_MATRICULA.DIPLOMACAO.ENTREGUE_ALUNO:
                this.ui.divEntreguealuno.removeClass('hide');
                break;

            case EVOLUCAO.TB_MATRICULA.DIPLOMACAO.ENVIADO_ASSINATURA:
                this.ui.divEnviadoassinatura.removeClass('hide');
                break;

            case EVOLUCAO.TB_MATRICULA.DIPLOMACAO.ENVIADO_POLO:
                this.ui.divEnviopolo.removeClass('hide');
                break;

            case EVOLUCAO.TB_MATRICULA.DIPLOMACAO.ENVIADO_REGISTRO:
                this.ui.divEnviadoregistro.removeClass('hide');
                break;

            case EVOLUCAO.TB_MATRICULA.DIPLOMACAO.ENVIADO_SECRETARIA:
                this.ui.divEnviadosecretaria.removeClass('hide');
                break;

            case EVOLUCAO.TB_MATRICULA.DIPLOMACAO.HISTORICO_GERADO:
                this.ui.divHistoricogerado.removeClass('hide');
                break;

            case EVOLUCAO.TB_MATRICULA.DIPLOMACAO.EQUIVALENCIA_GERADO:
                this.ui.divEquivalenciagerado.removeClass('hide');
                break;

            case EVOLUCAO.TB_MATRICULA.DIPLOMACAO.RETORNADO_ASSINATURA:
                this.ui.divRetornadoassinatura.removeClass('hide');
                break;

            case EVOLUCAO.TB_MATRICULA.DIPLOMACAO.RETORNADO_REGISTRO:
                this.ui.divRetornoregistro.removeClass('hide');
                break;

            default:
                this.ui.divHide.addClass('hide');
                break;
        }
    },
    pesquisarAlunosDiplomacao: function (e) {
        e.preventDefault();
        var params = Backbone.Syphon.serialize(this);

        resultsInit = new ResultsComposite(params);
        this.resultsRegion.show(resultsInit);
    },
    limparResultados: function () {
        this.resultsRegion.reset();
    }
});

/**************
 * Resultados *
 ***************/
var ResultsModel = VwMatriculaDiplomacao.extend({
    // idAttribute: ['id_matricula', 'id_projetopedagogico'],
    // url: '/diplomacao/clonar-diplomacao',
    url: '/api/matricula-diplomacao/',
    idAttribute: 'id_matriculadiplomacao',
    bl_alterarenvioassinatura: false,
    bl_alterarretornossinatura: false,
    bl_alterarenviosecretaria: false,
    bl_alterarenvioregistro: false,
    bl_alterarretornoregistro: false
});

var ResultsCollection = Backbone.Collection.extend({
    url: 'diplomacao/retorna-alunos-diplomacao/',
    model: ResultsModel
});

var ResultsView = Marionette.ItemView.extend({
    template: '#results-view',
    tagName: 'tr',
    ui: {
        btnVisualizar: '#btnVisualizar',
        btnGerar: '#btnGerar',
        inputsRegistroDiploma: '.registroDiploma',
        dtEnviopolo: '.dt_enviopolo',
        dtEntregueAluno: '.dt_entreguealuno'
    },
    events: {
        'change input[type=checkbox]': 'setModelChange',
        'changeDate @ui.dtEnviopolo': 'setDate',
        'changeDate @ui.dtEntregueAluno': 'setDate',
        'input @ui.inputsRegistroDiploma': 'setRegistroDiploma',
        'changeDate @ui.inputsRegistroDiploma': 'setRegistroDiploma',
        'click @ui.btnVisualizar': function () {
            this.abrirModalGerar('visualizar')
        },
        'click @ui.btnGerar': function () {
            this.abrirModalGerar('gerar')
        }
    },
    initialize: function () {
        this.model.set('id', this.model.get('id_matricula') + '' + this.model.get('id_projetopedagogico'));
        this.modelBkp = this.model.toJSON();
    },
    onBeforeRender: function () {
        //adiciona permissões de edição
        this.model.set({
            'permEditDataEnvioPolo': PermissaoEditar.dtEnvioPolo,
            'permEditDataEntregueAluno': PermissaoEditar.dtEntregueAluno,
            'permEditDadosDiploma': PermissaoEditar.dadosDiploma,
            'bl_alteracaocomperfil': 0
        })
    },
    setRegistroDiploma: function (e) {
        var $input = $(e.target);
        var attr = $input.attr('name');

        var old_val = this.modelBkp[attr];
        var val = ($input.val() + "").trim() || null;

        // Se preencher um campo, os outros são obrigatórios)
        if (val) {
            this.ui.inputsRegistroDiploma.prop('required', true);
        }
        // Se o item já possuía valor salvo no banco, não permite deixar sem valor
        else if (old_val) {
            $input.prop('required', true);
        }
        // Se nenhum outro item possuir valor, então não é obrigatório
        else {
            var preenchidos = this.ui.inputsRegistroDiploma.filter(function () {
                return ($(this).val() + "").trim();
            });

            if (!preenchidos.length) {
                this.ui.inputsRegistroDiploma.prop('required', false);
            }
        }

        this.model.set('bl_alterar', true);
        this.model.set(attr, val);
    },
    setModelChange: function (e) {
        e.preventDefault();
        var $input = $(e.target);
        this.model.set('bl_alterar', $input.prop('checked'));

        if ($input.prop('checked')) {
            $input.next().text(getTodayStringDate());
            this.setDataInput($input.prop('class').substr(3));

        } else {
            $input.next().empty();
        }
    },
    setDate: function (e) {
        e.preventDefault();
        var $input = $(e.target);
        if ($input.data('datepicker')) {
            var $string = $input.data('id');
            this.model.set('bl_alterar', true);
            this.setDataInput($string);
        }
    },
    setDataInput: function ($rule) {
        switch ($rule) {
            case 'envioassinatura':
                this.model.set('id_usuarioenviadoassinatura', G2S.loggedUser.get('id_usuario'));
                this.model.set('id_evolucaodiplomacao', EVOLUCAO.TB_MATRICULA.DIPLOMACAO.ENVIADO_ASSINATURA);
                this.model.set('dt_enviadoassinatura', dateEnString());
                break;

            case 'retornoassinatura':
                this.model.set('id_usuarioretornadoassinatura', G2S.loggedUser.get('id_usuario'));
                this.model.set('id_evolucaodiplomacao', EVOLUCAO.TB_MATRICULA.DIPLOMACAO.RETORNADO_ASSINATURA);
                this.model.set('dt_retornadoassinatura', dateEnString());
                break;


            case 'enviosecretaria':
                this.model.set('id_usuarioenviadosecretaria', G2S.loggedUser.get('id_usuario'));
                this.model.set('id_evolucaodiplomacao', EVOLUCAO.TB_MATRICULA.DIPLOMACAO.ENVIADO_SECRETARIA);
                this.model.set('dt_enviadosecretaria', dateEnString());
                break;

            case 'envioregistro':
                this.model.set('id_usuarioenviadoregistro', G2S.loggedUser.get('id_usuario'));
                this.model.set('id_evolucaodiplomacao', EVOLUCAO.TB_MATRICULA.DIPLOMACAO.ENVIADO_REGISTRO);
                this.model.set('dt_enviadoregistro', dateEnString());
                break;

            case 'retornoregistro':
                this.model.set('id_usuarioretornoregistro', G2S.loggedUser.get('id_usuario'));
                this.model.set('id_evolucaodiplomacao', EVOLUCAO.TB_MATRICULA.DIPLOMACAO.RETORNADO_REGISTRO);
                this.model.set('dt_retornoregistro', dateEnString());
                break;

            //input datepicker
            case 'enviopolo':
                if (this.ui.dtEnviopolo.val()) {
                    if (parseInt(daysDiff(this.ui.dtEnviopolo.val(), this.model.get('dt_retornoregistro'))) <= 0 &&
                        ( !this.ui.dtEntregueAluno.val() || (parseInt(daysDiff(this.ui.dtEntregueAluno.val(), this.ui.dtEnviopolo.val())) <= 0))
                    ) {
                        if (this.model.get('dt_enviopolo') && (this.model.get('id_evolucaodiplomacao') !== EVOLUCAO.TB_MATRICULA.DIPLOMACAO.RETORNADO_REGISTRO))
                            this.model.set('bl_alteracaocomperfil', 1);
                        this.model.set('dt_enviopolo', this.ui.dtEnviopolo.val());
                        this.model.set('id_evolucaodiplomacao', EVOLUCAO.TB_MATRICULA.DIPLOMACAO.ENVIADO_POLO);
                        this.model.set('id_usuarioenviopolo', G2S.loggedUser.get('id_usuario'));
                    }
                    else {
                        this.ui.dtEnviopolo.val('');
                        this.model.set('bl_alterar', false);
                        $.pnotify({title: 'Alerta', text: MensagensG2.MSG_DIPLOMACAO_07, type: 'warning'});
                    }
                } else {
                    this.model.set('bl_alterar', false);
                    $.pnotify({title: 'Alerta', text: MensagensG2.MSG_DIPLOMACAO_05, type: 'warning'});
                }
                break;

            case 'entreguealuno':
                if (this.ui.dtEntregueAluno.val()) {
                    if (parseInt(daysDiff(this.ui.dtEntregueAluno.val(), this.model.get('dt_enviopolo'))) <= 0) {
                        if (this.model.get('dt_entreguealuno') && (this.model.get('id_evolucaodiplomacao') !== EVOLUCAO.TB_MATRICULA.DIPLOMACAO.ENVIADO_POLO))
                            this.model.set('bl_alteracaocomperfil', 1);
                        this.model.set('dt_entreguealuno', this.ui.dtEntregueAluno.val());
                        this.model.set('id_evolucaodiplomacao', EVOLUCAO.TB_MATRICULA.DIPLOMACAO.ENTREGUE_ALUNO);
                        this.model.set('id_usuarioentreguealuno', G2S.loggedUser.get('id_usuario'));
                    } else {
                        this.ui.dtEntregueAluno.val('');
                        this.model.set('bl_alterar', false);
                        $.pnotify({title: 'Alerta', text: MensagensG2.MSG_DIPLOMACAO_08, type: 'warning'});
                    }
                } else {
                    this.model.set('bl_alterar', false);
                    $.pnotify({title: 'Alerta', text: MensagensG2.MSG_DIPLOMACAO_06, type: 'warning'});
                }
                break;

            default:
                $.pnotify({title: 'Alerta', text: "Não implementado", type: 'warning'});
                break;
        }
    },
    abrirModalGerar: function ($type_action) {
        // Verificar se possui algum aproveitamento
        var aproveitamentoCollection = new AproveitamentoCollection();
        aproveitamentoCollection.fetch({
            data: {
                id_matricula: this.model.get('id_matricula'),
                bl_ativo: true
            },
            beforeSend: loading,
            complete: _.bind(function () {
                loaded();

                this.model.set({
                    st_actiontype: $type_action,
                    bl_gerarhistorico: false,
                    bl_gerardiploma: false,
                    bl_gerarequivalencia: false,
                    bl_draft: false
                });

                modalGerarInit = new ModalGerarItem({
                    model: this.model
                });

                researchInit.modalGerar.show(modalGerarInit);

                if (aproveitamentoCollection.length) {
                    modalGerarInit.ui.gerarEquivalencia.show();
                } else {
                    modalGerarInit.ui.gerarEquivalencia.hide();
                }
            }, this)
        });
    }
});

var ResultsComposite = Marionette.CompositeView.extend({
    template: '#results-composite',
    collection: new ResultsCollection(),
    childViewContainer: 'tbody',
    childView: ResultsView,
    emptyView: Marionette.ItemView.extend({
        template: _.template('<td colspan="18">Nenhum registro encontrado.</td>'),
        tagName: 'tr'
    }),
    ui: {
        checkAllEnvioAssinatura: '#ck-all-envio-assinatura',
        checkAllRetornoAssinatura: '#ck-all-retorno-assinatura',
        checkAllEnvioSecretaria: '#ck-all-envio-secretaria',
        checkAllEnvioRegistro: '#ck-all-envio-registro',
        checkAllRetornoRegistro: '#ck-all-retorno-registro',
        btnSalvarEtapa: '#btnSalvarFaseDiplomacao',
        btnGerarXLS: '#btnGerarXLS',
        ckAll: '.ckAll'
    },
    initialize: function (options) {
        this.params = options || {};
        this.pesquisarAction();
    },
    pesquisarAction: function () {
        this.collection.reset();
        this.collection.fetch({
            data: this.params,
            beforeSend: loading,
            complete: _.bind(function () {
                loaded();
                this.setChangesRules();
                this.habilitaFuncoes();
                this.formatarCPFs();
            }, this)
        });
    },
    events: {
        'change @ui.ckAll': 'marcaTodos',
        'click @ui.btnSalvarEtapa': 'salvarEtapa',
        'click @ui.btnGerarXLS': 'gerarXLS'
    },
    setChangesRules: function () {
        this.collection.each(function (item) {
            if (item.get('dt_diplomagerado') && item.get('dt_historicogerado') && !item.get('dt_enviadoassinatura')) {
                item.set('bl_alterarenvioassinatura', true);
            }

            if (item.get('id_evolucaodiplomacao') === EVOLUCAO.TB_MATRICULA.DIPLOMACAO.ENVIADO_ASSINATURA) {
                item.set('bl_alterarretornossinatura', true);
            }

            if (item.get('id_evolucaodiplomacao') === EVOLUCAO.TB_MATRICULA.DIPLOMACAO.RETORNADO_ASSINATURA) {
                item.set('bl_alterarenviosecretaria', true);
            }

            if (item.get('id_evolucaodiplomacao') === EVOLUCAO.TB_MATRICULA.DIPLOMACAO.ENVIADO_SECRETARIA) {
                item.set('bl_alterarenvioregistro', true);
            }

            if (item.get('id_evolucaodiplomacao') === EVOLUCAO.TB_MATRICULA.DIPLOMACAO.ENVIADO_REGISTRO) {
                item.set('bl_alterarretornoregistro', true);
            }
        });
    },
    habilitaFuncoes: function () {
        if (this.collection.length) {
            //Verifica se deve habilitar checkbox Checkall para colunas
            var blCheckAllEnvioAssinatura = this.collection.filter(function (item) {
                return item.get('dt_diplomagerado') && item.get('dt_historicogerado') && !item.get('dt_enviadoassinatura');
            }).length;

            if (blCheckAllEnvioAssinatura)
                this.ui.checkAllEnvioAssinatura.show();

            var blCheckAllRetornoAssinatura = this.collection.filter(function (item) {
                return item.get('id_evolucaodiplomacao') === EVOLUCAO.TB_MATRICULA.DIPLOMACAO.ENVIADO_ASSINATURA
                    || item.get('dt_enviadoassinatura');
            }).length;

            if (blCheckAllRetornoAssinatura)
                this.ui.checkAllRetornoAssinatura.show();

            var blCheckAllEnvioSecretaria = this.collection.filter(function (item) {
                return item.get('id_evolucaodiplomacao') === EVOLUCAO.TB_MATRICULA.DIPLOMACAO.RETORNADO_ASSINATURA
                    || item.get('dt_retornadoassinatura');
            }).length;

            if (blCheckAllEnvioSecretaria)
                this.ui.checkAllEnvioSecretaria.show();

            var blCheckAllEnvioRegistro = this.collection.filter(function (item) {
                return item.get('id_evolucaodiplomacao') === EVOLUCAO.TB_MATRICULA.DIPLOMACAO.ENVIADO_SECRETARIA
                    || item.get('dt_enviadosecretaria');
            }).length;

            if (blCheckAllEnvioRegistro)
                this.ui.checkAllEnvioRegistro.show();

            var blCheckAllRetornoRegistro = this.collection.filter(function (item) {
                return item.get('id_evolucaodiplomacao') === EVOLUCAO.TB_MATRICULA.DIPLOMACAO.ENVIADO_REGISTRO
                    || item.get('dt_enviadoregistro');
            }).length;

            if (blCheckAllRetornoRegistro) {
                this.ui.checkAllRetornoRegistro.show();
            }

            //Botão é mostrado apenas se existe algum registro para ser salvo
            if (blCheckAllEnvioAssinatura || blCheckAllRetornoAssinatura || blCheckAllEnvioSecretaria || blCheckAllEnvioRegistro || blCheckAllRetornoRegistro) {
                this.ui.btnSalvarEtapa.show();
            }
        }
    },
    marcaTodos: function (e) {
        e.preventDefault();
        var $input = $(e.target);
        var $string = $input.data('id');

        var $results = this.$el.find('.ck-' + $string).not(':disabled');

        if ($results.length) {
            loading();
            if ($input.prop('checked')) {
                $results.prop('checked', true);
                $results.nextAll('.td-' + $string + ' span').text(getTodayStringDate());
                this.setModelRules($string);
            } else {
                $results.prop('checked', false);
                $results.nextAll('.td-' + $string + ' span').empty();
                this.cleanModelRules($string);
            }
            loaded();
        }
    },
    setModelRules: function ($rule) {
        switch ($rule) {
            case 'envioassinatura':
                this.collection.each(function (item) {
                    if (item.get('dt_diplomagerado') && item.get('dt_historicogerado') && !item.get('dt_enviadoassinatura') && item.get('bl_alterarenvioassinatura')) {
                        item.set('id_usuarioenviadoassinatura', G2S.loggedUser.get('id_usuario'));
                        item.set('id_evolucaodiplomacao', EVOLUCAO.TB_MATRICULA.DIPLOMACAO.ENVIADO_ASSINATURA);
                        item.set('dt_enviadoassinatura', dateEnString());
                        item.set('bl_alterar', true);
                    }
                });
                break;

            case 'retornoassinatura':
                this.collection.each(function (item) {
                    if (item.get('id_evolucaodiplomacao') === EVOLUCAO.TB_MATRICULA.DIPLOMACAO.ENVIADO_ASSINATURA && item.get('bl_alterarretornossinatura')) {
                        item.set('id_usuarioretornadoassinatura', G2S.loggedUser.get('id_usuario'));
                        item.set('id_evolucaodiplomacao', EVOLUCAO.TB_MATRICULA.DIPLOMACAO.RETORNADO_ASSINATURA);
                        item.set('dt_retornadoassinatura', dateEnString());
                        item.set('bl_alterar', true);
                    }
                });
                break;


            case 'enviosecretaria':
                this.collection.each(function (item) {
                    if (item.get('id_evolucaodiplomacao') === EVOLUCAO.TB_MATRICULA.DIPLOMACAO.RETORNADO_ASSINATURA && item.get('bl_alterarenviosecretaria')) {
                        item.set('id_usuarioenviadosecretaria', G2S.loggedUser.get('id_usuario'));
                        item.set('id_evolucaodiplomacao', EVOLUCAO.TB_MATRICULA.DIPLOMACAO.ENVIADO_SECRETARIA);
                        item.set('dt_enviadosecretaria', dateEnString());
                        item.set('bl_alterar', true);
                    }
                });
                break;

            case 'envioregistro':
                this.collection.each(function (item) {
                    if (item.get('id_evolucaodiplomacao') === EVOLUCAO.TB_MATRICULA.DIPLOMACAO.ENVIADO_SECRETARIA && item.get('bl_alterarenvioregistro')) {
                        item.set('id_usuarioenviadoregistro', G2S.loggedUser.get('id_usuario'));
                        item.set('id_evolucaodiplomacao', EVOLUCAO.TB_MATRICULA.DIPLOMACAO.ENVIADO_REGISTRO);
                        item.set('dt_enviadoregistro', dateEnString());
                        item.set('bl_alterar', true);
                    }
                });
                break;

            case 'retornoregistro':
                this.collection.each(function (item) {
                    if (item.get('id_evolucaodiplomacao') === EVOLUCAO.TB_MATRICULA.DIPLOMACAO.ENVIADO_REGISTRO && item.get('bl_alterarretornoregistro')) {
                        item.set('id_usuarioretornoregistro', G2S.loggedUser.get('id_usuario'));
                        item.set('id_evolucaodiplomacao', EVOLUCAO.TB_MATRICULA.DIPLOMACAO.RETORNADO_REGISTRO);
                        item.set('dt_retornoregistro', dateEnString());
                        item.set('bl_alterar', true);
                    }
                });
                break;
            default:
                break;
        }
    },
    cleanModelRules: function ($rule) {
        switch ($rule) {
            case 'envioassinatura':
                this.collection.each(function (item) {
                    if (item.get('bl_alterarenvioassinatura')) {
                        item.set('id_usuarioenviadoassinatura', null);
                        item.set('id_evolucaodiplomacao', EVOLUCAO.TB_MATRICULA.DIPLOMACAO.APTO_A_DIPLOMAR);
                        item.set('dt_enviadoassinatura', null);
                        item.set('bl_alterar', false);
                    }
                });
                break;

            case 'retornoassinatura':
                this.collection.each(function (item) {
                    if (item.get('bl_alterarretornossinatura')) {
                        item.set('id_usuarioretornadoassinatura', null);
                        item.set('id_evolucaodiplomacao', EVOLUCAO.TB_MATRICULA.DIPLOMACAO.ENVIADO_ASSINATURA);
                        item.set('dt_retornadoassinatura', null);
                        item.set('bl_alterar', false);
                    }
                });
                break;


            case 'enviosecretaria':
                this.collection.each(function (item) {
                    if (item.get('bl_alterarenviosecretaria')) {
                        item.set('id_usuarioenviadosecretaria', null);
                        item.set('id_evolucaodiplomacao', EVOLUCAO.TB_MATRICULA.DIPLOMACAO.RETORNADO_ASSINATURA);
                        item.set('dt_enviadosecretaria', null);
                        item.set('bl_alterar', false);
                    }
                });
                break;

            case 'envioregistro':
                this.collection.each(function (item) {
                    if (item.get('bl_alterarenvioregistro')) {
                        item.set('id_usuarioenviadoregistro', null);
                        item.set('id_evolucaodiplomacao', EVOLUCAO.TB_MATRICULA.DIPLOMACAO.ENVIADO_SECRETARIA);
                        item.set('dt_enviadoregistro', null);
                        item.set('bl_alterar', false);
                    }
                });
                break;

            case 'retornoregistro':
                this.collection.each(function (item) {
                    if (item.get('bl_alterarretornoregistro')) {
                        item.set('id_usuarioretornoregistro', null);
                        item.set('id_evolucaodiplomacao', EVOLUCAO.TB_MATRICULA.DIPLOMACAO.ENVIADO_REGISTRO);
                        item.set('dt_retornoregistro', null);
                        item.set('bl_alterar', false);
                    }
                });
                break;
            default:
                break;
        }
    },
    isValid: function () {
        var $input = this.$el.find('input:required:invalid').first();

        if ($input.length) {
            $input.focus();
            $.pnotify({
                type: 'warning',
                title: 'Aviso',
                text: 'O campo deve ser preenchido.'
            });

            return false;
        }

        return true;
    },
    salvarEtapa: function () {
        var data = [];

        // Validar campos obrigatorios
        if (!this.isValid()) {
            return false;
        }

        //Array com os dados que a serem salvar (bl_alterar = 1)
        this.collection.each(function (vwModel) {
            if (vwModel.get('bl_alterar') && vwModel.get('id_matriculadiplomacao')) {
                data.push(vwModel.toJSON());
            }
        });

        if (!data.length) {
            $.pnotify({title: 'Alerta', text: MensagensG2.MSG_DIPLOMACAO_01, type: 'warning'});
            return false;
        }

        return $.ajax({
            url: '/diplomacao/salvar-etapa',
            type: 'post',
            data: {data: data},
            dataType: 'JSON',
            beforeSend: loading(),
            success: _.bind(function () {
                /*_.each(data, function (model) {
                 that.collection.each(function (vwModel) {
                 if (vwModel.get('id_matriculadiplomacao') === model.id_matriculadiplomacao) {
                 if (model.id_evolucaodiplomacao !== EVOLUCAO.TB_MATRICULA.DIPLOMACAO.ENVIADO_POLO
                 && model.id_evolucaodiplomacao !== EVOLUCAO.TB_MATRICULA.DIPLOMACAO.ENTREGUE_ALUNO)
                 that.collection.remove(vwModel);
                 }
                 });

                 });*/
                resultsInit.pesquisarAction();
            }, this),
            complete: _.bind(function (response) {
                if (response.responseJSON.type === 'success') {
                    this.ui.ckAll.removeAttr('checked');
                }

                $.pnotify(response.responseJSON);
                loaded();
            }, this),
            error: function () {
                $.pnotify({
                    title: 'Erro',
                    text: sprintf(MensagensG2.MSG_DIPLOMACAO_02),
                    type: 'danger'
                });
                loaded();
            }
        });
    },
    gerarXLS: function () {
        // tratando HTML da tabela para gerar XLS
        var html = $('div[id="tableAlunoData"]').clone();
        html.find('.visualizacao').remove();
        html.find('.ck-envioassinatura').remove();

        // substituindo cabeçalho na coluna
        html.find('#registroColumn')
            .replaceWith('<th>N. Registro do Diploma</th>' +
                '<th>Livro Registro do Diploma</th>' +
                '<th>Folha Registro do Diploma</th>'
            );

        html.find('.registroValues').each(function () {
            var registroAtual = $(this);

            // reiniciando valores das variáveis
            var registroValues = [];
            var countValues = 0;

            $(this).find('input').each(function () {
                registroValues[countValues] = $(this).val();
                countValues++;
            });
            // substituindo valores
            registroAtual.replaceWith(
                '<td>\n' +
                '    <label> ' + registroValues[0] + ' </label>\n' +
                '</td>\n' +
                '<td>\n' +
                '    <label> ' + registroValues[1] + ' </label>\n' +
                '</td>\n' +
                '<td>\n' +
                '    <label> ' + registroValues[2] + ' </label>\n' +
                '</td>'
            );
        });

        // substitui inputs por labels para evitar inputs dentro do XLS
        html.find('input').each(function () {
            $(this).replaceWith('<label>' + $(this).val() + '</label>');
        });

        // pegando somente o HTML(desfazendo objeto)
        html = html.html();

        // gerando "fake link" para poder setar o nome do arquivo
        var downloadLink = document.createElement("a");
        var blob = new Blob(["\ufeff", html]);
        var url = URL.createObjectURL(blob);
        downloadLink.href = url;

        // gerando data
        var actualDate = new Date();
        var strDate = actualDate.getDate() + '-' + (actualDate.getMonth() + 1) + '-' + actualDate.getFullYear();

        downloadLink.download = "diplomacao_" + strDate + ".xls";

        // fazendo o download do arquivo
        document.body.appendChild(downloadLink);
        downloadLink.click();
        document.body.removeChild(downloadLink);
    },
    formatarCPFs: function () {
        $(".cpf").mask("999.999.999-99");
    }
});


/**
 * Modal Gerar
 */
var ModalGerarItem = Marionette.ItemView.extend({
    template: '#modal-template',
    tagName: 'div',
    className: 'modal fade modal-lg',
    ui: {
        gerarEquivalencia: '#gerarEquivalencia',
        inputCedula: 'input[name="st_cedula"]',
        inputMotivo: 'textarea[name="st_motivo"]',
        inputObservacao: 'textarea[name="st_observacao"]',
        btnConfirm: '#btn-confirm',
        btn2Via: '#btn-2via'
    },
    events: {
        'change input[type=checkbox]': 'setModel',
        'click @ui.btnConfirm': 'validarCedula',
        'click @ui.btn2Via': 'validarCedula'
    },
    onShow: function () {
        this.$el.modal('show');
    },
    model: new Backbone.Model,
    setModel: function (e) {
        e.preventDefault();
        var $input = $(e.target);
        this.model.set('bl_gerar' + $input.data('doc'), $input.prop('checked'));

        var bl_botoesgerar = this.model.get('bl_gerardiploma') || this.model.get('bl_gerarhistorico') || this.model.get('bl_gerarequivalencia');
        var bl_desabilitardiploma = this.model.get('st_actiontype') !== 'gerar' || !this.model.get('bl_gerardiploma');
        var bl_desabilitarhistorico = this.model.get('st_actiontype') !== 'gerar' || !this.model.get('bl_gerarhistorico');

        this.ui.btnConfirm.toggleClass('hide', bl_desabilitardiploma && !bl_botoesgerar);
        this.ui.btn2Via.toggleClass('hide', bl_desabilitardiploma && !bl_botoesgerar);

        this.ui.inputCedula.prop('disabled', bl_desabilitardiploma);
        this.ui.inputMotivo.prop('disabled', bl_desabilitardiploma);

        this.ui.inputObservacao.prop('disabled', bl_desabilitarhistorico);

        // if (bl_desabilitar) {
        // this.ui.inputCedula.val('');
        // this.ui.inputMotivo.val('');
        // }
    },
    validarCedula: function (e) {
        // Verificar se o botão acionado é o 2ª Via
        this.model.set('bl_gerarsegundavia', Number($(e.target).is(this.ui.btn2Via)));

        // Quando o usuário selecionar a opção diploma o campo 'Número da Cédula' é de preenchimento obrigatório;
        if (!this.model.get('bl_gerardiploma') || this.model.get('st_actiontype') !== 'gerar') {
            this.gerarArquivos();
            return true;
        }

        var st_cedula = this.ui.inputCedula.val().trim();

        if (!st_cedula) {
            $.pnotify({
                type: 'warning',
                title: 'Aviso',
                text: 'Por favor informe o número da cédula.'
            });
            this.ui.inputCedula.focus();
            return false;
        }

        // Verificar se a cédula não está sendo usada
        // if (this.model.get('bl_gerarsegundavia')) {
        loading();

        $.getJSON('/api/matricula-diplomacao', {
            st_cedula: st_cedula
        }, _.bind(function (response) {
            loaded();

            if (response.length) {
                $.pnotify({
                    type: 'warning',
                    title: 'Aviso',
                    text: 'Este número já foi cadastrado. Por favor verifique o número ou tente cadastrar outro número.'
                });

                return false;
            }

            this.gerarArquivos();
        }, this));
    },
    validarGeracaoHistorico: function () {
        // GII-8420
        if (this.model.get('bl_gerarhistorico')) {
            var error = null;

            if (!this.model.get('id_tiposelecao')) {
                error = 'A forma de ingresso do aluno não está cadastrada no sistema';
            } else if (!this.model.get('dt_ingresso')) {
                error = 'A data de ingresso do aluno não está cadastrada no sistema';
            } else if (!this.model.get('st_reconhecimento')) {
                error = 'O reconhecimento do curso do aluno não está cadastrado no sistema';
            }

            if (error) {
                $.pnotify({
                    type: 'warning',
                    title: 'Aviso',
                    text: error
                });

                return false;
            }
        }

        return true;
    },
    gerarArquivos: function () {
        var bl_gerarsegundavia = this.model.get('bl_gerarsegundavia');
        var $action_type = this.model.get('st_actiontype');

        if (!this.validarGeracaoHistorico()) {
            return false;
        }

        var $request = null;

        switch ($action_type) {
            case 'gerar':
                if (this.model.get('bl_gerarhistorico')) {
                    this.model.set('bl_alterar', true);

                    if (!this.model.get('dt_historicogerado') || bl_gerarsegundavia) {
                        this.model.set('id_evolucaodiplomacao', this.model.get('dt_historicogerado') && bl_gerarsegundavia ? EVOLUCAO.TB_MATRICULA.DIPLOMACAO.SEGUNDA_VIA_HISTORICO_GERADO : EVOLUCAO.TB_MATRICULA.DIPLOMACAO.HISTORICO_GERADO);
                        this.model.set('id_usuariohistoricogerado', G2S.loggedUser.get('id_usuario'));
                        this.model.set('dt_historicogerado', dateEnString());
                    }

                    // Sempre salvar a observação
                    this.model.set('st_observacao', this.ui.inputObservacao.val().trim());
                    $request = resultsInit.salvarEtapa();
                }

                if (this.model.get('bl_gerarequivalencia') && (!this.model.get('dt_equivalenciagerado') || bl_gerarsegundavia)) {
                    this.model.set('bl_alterar', true);
                    this.model.set('id_usuarioequivalenciagerado', G2S.loggedUser.get('id_usuario'));
                    this.model.set('id_evolucaodiplomacao', this.model.get('dt_equivalenciagerado') && bl_gerarsegundavia ? EVOLUCAO.TB_MATRICULA.DIPLOMACAO.SEGUNDA_VIA_EQUIVALENCIA_GERADO : EVOLUCAO.TB_MATRICULA.DIPLOMACAO.EQUIVALENCIA_GERADO);
                    this.model.set('dt_equivalenciagerado', dateEnString());
                    $request = resultsInit.salvarEtapa();
                }

                if (this.model.get('bl_gerardiploma')) {
                    this.model.set('bl_alterar', true);

                    if (!bl_gerarsegundavia) {
                        this.model.set('id_usuariodiplomagerado', G2S.loggedUser.get('id_usuario'));
                        this.model.set('id_evolucaodiplomacao', EVOLUCAO.TB_MATRICULA.DIPLOMACAO.DIPLOMA_GERADO);
                        this.model.set('dt_diplomagerado', dateEnString());
                        this.model.set('st_cedula', this.ui.inputCedula.val().trim());
                        this.model.set('st_motivo', this.ui.inputMotivo.val().trim());
                    } else {
                        this.model.set('id_evolucaodiplomacao', EVOLUCAO.TB_MATRICULA.DIPLOMACAO.SEGUNDA_VIA_DIPLOMA_GERADO);

                        // Criar um novo registro para a SEGUNDA VIA
                        var model = new ResultsModel();

                        model.save({
                            // Definir que é uma alteração para salvar a etapa mais tarde
                            bl_alterar: true,

                            // Dados que deverão ser iguais ao da primeira via
                            id_matricula: this.model.get('id_matricula'),
                            // st_diplomanumero: this.model.get('st_diplomanumero'),
                            // st_diplomalivro: this.model.get('st_diplomalivro'),
                            // st_diplomafolha: this.model.get('st_diplomafolha'),
                            st_observacao: this.ui.inputObservacao.val().trim(),

                            // Referência da diplomação de origem
                            id_diplomacaoorigem: this.model.get('id_matriculadiplomacao'),

                            // Manter dados de APTO A DIPLOMAR
                            dt_aptodiplomar: this.model.get('dt_aptodiplomar'),
                            id_usuarioaptodiplomar: this.model.get('id_usuarioaptodiplomar'),

                            // Criar novos dados do DIPLOMA GERADO
                            dt_diplomagerado: dateEnString(),
                            id_usuariodiplomagerado: G2S.loggedUser.get('id_usuario'),

                            // Manter dados do HISTÓRICO GERADO
                            dt_historicogerado: this.model.get('dt_historicogerado'),
                            id_usuariohistoricogerado: this.model.get('id_usuariohistoricogerado'),

                            // Manter dados do quadro de EQUIVALÊNCIA GERADO
                            dt_equivalenciagerado: this.model.get('dt_equivalenciagerado'),
                            id_usuarioequivalenciagerado: this.model.get('id_usuarioequivalenciagerado'),

                            // Adicionar novas informações
                            st_cedula: this.ui.inputCedula.val().trim(),
                            st_motivo: this.ui.inputMotivo.val().trim(),

                            // Abaixo as variaveis que serão definidas por padrão no PHP, devem ser enviadas como NULL
                            bl_ativo: null,
                            dt_cadastro: null,
                            id_entidade: null,
                            id_usuariocadastro: null
                        }, {
                            beforeSend: loading,
                            complete: loaded
                        });
                    }

                    $request = resultsInit.salvarEtapa();
                }

                this.model.set('bl_draft', false);
                break;

            case 'visualizar':
                this.model.set('bl_draft', true);
                break;

            default:
                break;
        }

        if (!this.model.get('bl_gerarhistorico') && !this.model.get('bl_gerardiploma') && !this.model.get('bl_gerarequivalencia')) {
            $.pnotify({title: 'Alerta', text: MensagensG2.MSG_DIPLOMACAO_03, type: 'warning'});
            return false;
        } else if ($request) {
            $request.done(_.bind(function () {
                this.abrirPopupsGeracao();
            }, this));
        } else {
            this.abrirPopupsGeracao();
        }
    },
    abrirPopupsGeracao: function () {
        this.$el.modal('hide');

        if (this.model.get('bl_gerarhistorico')) {
            // var $winHistorico = window.open('/textos/geracao/?filename=Emissão Historico&id_textosistema=' + TEXTO_SISTEMA_FIXO.HISTORICO_ESCOLAR_DEFINITIVO + '&arrParams[id_matricula]=' + this.model.get('id_matricula') + '&type=html' + (this.model.get('bl_draft') ? '&draft=true' : ''), '_blank');
            var $winHistorico = window.open('/diplomacao/gerar-documento/?documento=historico&id_matricula=' + this.model.get('id_matricula') + (this.model.get('bl_draft') ? '&draft=true' : ''), '_blank');
            if (!$winHistorico) {
                $.pnotify({title: 'Alerta', text: MensagensG2.MSG_DIPLOMACAO_04, type: 'warning'});
            }
        }

        if (this.model.get('bl_gerardiploma')) {
            // var $winDiploma = window.open('/textos/geracao/?filename=Emissão Diploma&id_textosistema=' + TEXTO_SISTEMA_FIXO.DIPLOMA_GRADUACAO + '&arrParams[id_matricula]=' + this.model.get('id_matricula') + '&type=html' + (this.model.get('bl_draft') ? '&draft=true' : ''), '_blank');
            var $winDiploma = window.open('/diplomacao/gerar-documento/?documento=diploma&id_matricula=' + this.model.get('id_matricula') + (this.model.get('bl_draft') ? '&draft=true' : ''), '_blank');
            if (!$winDiploma) {
                $.pnotify({title: 'Alerta', text: MensagensG2.MSG_DIPLOMACAO_04, type: 'warning'});
            }
        }

        if (this.model.get('bl_gerarequivalencia')) {
            // var $winEquivalencia = window.open('/textos/geracao/?filename=Emissão Quadro de Equivalência&id_textosistema=' + TEXTO_SISTEMA_FIXO.QUADRO_EQUIVALENCIA + '&arrParams[id_matricula]=' + this.model.get('id_matricula') + '&type=html' + (this.model.get('bl_draft') ? '&draft=true' : ''), '_blank');
            var $winEquivalencia = window.open('/diplomacao/gerar-documento/?documento=equivalencia&id_matricula=' + this.model.get('id_matricula') + (this.model.get('bl_draft') ? '&draft=true' : ''), '_blank');
            if (!$winEquivalencia) {
                $.pnotify({title: 'Alerta', text: MensagensG2.MSG_DIPLOMACAO_04, type: 'warning'});
            }
        }
    }
});


maintInit = new MainLayoutView();
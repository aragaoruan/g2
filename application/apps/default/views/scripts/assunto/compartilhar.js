/* 
 * BackboneJs para funcionalidade de compartilhamento de assuntos
 * 
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */

$.ajaxSetup({async: false});
var modelSelected = {};
var boolCheckAll = false;

var EntidadesPageableCollectionExtend = Backbone.PageableCollection.extend({
    url: '/assunto/find-vw-entidade-recursiva-id',
    model: VwEntidadeRecursivaId,
    state: {
        pageSize: 15
    },
    mode: 'client'
});

var AssuntosPageableCollectionExtend = Backbone.PageableCollection.extend({
    url: '/assunto/find-vw-assunto-entidade',
    model: VwAssuntoEntidade,
    state: {
        pageSize: 500
    },
    mode: 'client'
});

var VwAssuntoEntidadeCollectionExtend = Backbone.Collection.extend({
    model: VwAssuntoEntidade
});
var AssuntosCollection = new VwAssuntoEntidadeCollectionExtend();

var TipoOcorrenciaCollectionExtend = Backbone.Collection.extend({
    model: TipoOcorrencia,
    url: '/assunto/retorna-tipo-ocorrencia-constante'
});

var CompartilharAssuntoItemView = Marionette.ItemView.extend({
    template: '#template-compartilhamentos-listar',
    tagName: 'div',
    onRender: function() {

        var RowGrid = Backgrid.Row.extend({
            initialize: function(options) {
                RowGrid.__super__.initialize.apply(this, arguments);
                this.$el.css('cursor', 'pointer');
            },
            abreCompartilhamentos: function() {
                modelSelected = this.model;
                rota.navigate('/assunto/compartilhar/' + this.model.get('id_entidade'), {trigger: true});
            },
            events: {
                "click": "abreCompartilhamentos"
            }
        });

        var collection = new EntidadesPageableCollectionExtend();

        var columns = [
            {name: 'id_entidade', label: 'ID Entidade', cell: 'string', editable: false},
            {name: 'st_nomeentidade', label: 'Nome Entidade', cell: 'string', editable: false}
        ];

        var pagebleGrid = new Backgrid.Grid({
            className: 'backgrid table table-bordered',
            columns: columns,
            collection: collection,
            row: RowGrid
        });

        var paginator = new Backgrid.Extension.Paginator({
            collection: collection
        });

        loading();
        collection.fetch({
            complete: loaded
        });

        this.$el.find('#painel-compartilhamentos-entidades').empty().append(pagebleGrid.render().$el);
        this.$el.find('#painel-compartilhamentos-entidades').append(paginator.render().$el);
    }
});

var CompartilharAssuntoEditarItemView = Marionette.ItemView.extend({
    template: _.template('<div class="row-fluid">' +
            '<div class="span12">' +
            '<div class="span5"><button id="btn-voltar" class="btn btn-inverse"><i class="icon-step-backward icon-white"></i> Voltar</button>' +
            '&nbsp;<button id="btn-save-compartilhamento" class="btn btn-success">Salvar Compartilhamentos</button>' +
            '&nbsp;<button class="btn popovers" id="btn-reset-grid" data-title="Dica" data-content="Restaura informações da tela com as informações que estão gravadas no banco de dados">' +
            '<i class="icon-refresh"></i></button></div><div id="div-filter-grid-keywords" class="span3"></div>' +
            '<div class="span4"><select data-title="Dica" data-content="Informe caso necessário o filtro do tipo de assunto."' +
            'id="id-tipo-ocorrencia" class="span11 popovers"></select></div>' +
            '</div>' +
            '<div style="overflow: auto; max-height:500px;" class="row-fluid" id="painel-compartilhamentos-assuntos"><!--Conteudo preenchido pelo backgrid--></div>' +
            '</div>'),
    tagName: 'div',
    arrayAssuntosAlterados: [],
    onRender: function() {

        this.$el.find('.popovers').popover({'show': true, trigger: 'hover', placement: 'bottom'});

        var ComboTipoOcorrenciaView = Marionette.ItemView.extend({
            tagName: 'option',
            template: _.template('<%=st_tipoocorrencia%>'),
            onRender: function() {
                this.$el.attr('value', this.model.get('id_tipoocorrencia'));
            }
        });

        var ComboTipoOcorrenciaCollection = Marionette.CollectionView.extend({
            tagName: 'select',
            childView: ComboTipoOcorrenciaView,
            labelName: 'Tipo Ocorrencia: '
        });

        var TipoOcorrenciaCollection = new TipoOcorrenciaCollectionExtend();
        TipoOcorrenciaCollection.fetch();

        this.$el.find('#id-tipo-ocorrencia').empty();

        (new ComboTipoOcorrenciaCollection({
            collection: TipoOcorrenciaCollection,
            el: this.$el.find('#id-tipo-ocorrencia')
        })).render();

        var CheckedCell = Backgrid.Cell.extend({
            className: 'select-row-cell',
            render: function() {
                var checked = this.model.get('attr_check') ? 'checked="checked"' : '';
                this.$el.html(_.template('<input type="checkbox" ' + checked + ' tabindex="-1">'));
                return this;
            }
        });

        var columns = [
            {name: '', cell: CheckedCell, headerCell: "select-all", editable: false},
            {name: 'id_assunto', label: 'ID', cell: 'string', editable: false},
            {name: 'st_assuntopai', label: 'Assunto Pai', cell: 'string', editable: false},
            {name: 'st_subassunto', label: 'Assunto', cell: 'string', editable: false}
        ];

        var collectionAssuntos = new AssuntosPageableCollectionExtend();

        var RowGrid = Backgrid.Row.extend({
            initialize: function(options) {
                RowGrid.__super__.initialize.apply(this, arguments);

                if (this.model.get('id_assuntoentidadeco')) {
                    this.model.set('attr_check', true);
                } else {
                    this.model.set('attr_check', false);
                }
            },
            events: {
                "click input": "check"
            },
            check: function(e) {
                var checked = $(e.target).is(':checked');

                if (checked) {
                    this.model.set('attr_check', true);
                    AssuntosCollection.add(this.model);
                } else {
                    this.model.set('attr_check', false);
                    AssuntosCollection.add(this.model);
                }
            }
        });

        var pagebleGrid = new Backgrid.Grid({
            className: 'backgrid table table-bordered',
            columns: columns,
            collection: collectionAssuntos,
            row: RowGrid,
            events: {
                'click .select-all-header-cell input': function() {
                    loading();
                    if (!boolCheckAll) {
                        boolCheckAll = true;

                        this.collection.each(function(model) {
                            if (!model.get('attr_check')) {
                                model.set('attr_check', true);
                                AssuntosCollection.add(model);
                            }
                        });
                    } else {
                        boolCheckAll = false;

                        this.collection.each(function(model) {
                            if (model.get('attr_check')) {
                                model.set('attr_check', false);
                                AssuntosCollection.add(model);
                            }
                        });
                    }
                    pagebleGrid.render();
                    $('.select-all-header-cell input').attr('checked', boolCheckAll);
                    loaded();
                }
            }
        });

        loading();
        collectionAssuntos.url += '/id_entidadecadastro/' + modelSelected.get('id_entidade');
        collectionAssuntos.fetch({
            async: false,
            complete: function() {
                loaded();
            }
        });

        //Este filtro backgrid, age de forma oculta, condicionado
        //ao parametro do combo tipo ocorrencia
        var filter = new Backgrid.Extension.ClientSideFilter({
            collection: collectionAssuntos.fullCollection,
            placeholder: 'Procurar',
            className: 'pull-right hide',
            id: 'filter-id-tipo-assunto',
            fields: ['id_tipoocorrencia']
        });

        var filter2 = new Backgrid.Extension.ClientSideFilter({
            collection: collectionAssuntos,
            placeholder: 'Procurar',
            className: 'pull-right',
            id: 'filter2'
        });

        this.$el.find('#painel-compartilhamentos-assuntos').empty().append(pagebleGrid.render().$el);
        this.$el.find('#painel-compartilhamentos-assuntos').prepend(filter.render().$el);
        this.$el.find('#div-filter-grid-keywords').empty().prepend(filter2.render().$el);
    },
    eventChangeComboTipoOcorrencia: function() {
        this.$el.find('#filter-id-tipo-assunto input').val(this.$el.find('#id-tipo-ocorrencia').val());
        this.$el.find('#filter-id-tipo-assunto input').trigger('keyup');
    },
    events: {
        "click #btn-voltar": function() {
            rota.navigate('/assunto/compartilhar', {trigger: true});
        },
        "change #id-tipo-ocorrencia": "eventChangeComboTipoOcorrencia",
        'click #btn-save-compartilhamento': function() {
            that = this;
            var arrVwassuntoEntidade = [];

            //Acumulando conteudo em array, para ser postado colecao de dados a ser salvo
            AssuntosCollection.each(function(model) {
                arrVwassuntoEntidade.push(model.clone().toJSON());
            });

            if (AssuntosCollection.length) {
                $.ajax({
                    url: '/assunto/atualizar-compartilhamento-assunto',
                    data: {arrassuntos: arrVwassuntoEntidade, entidade: modelSelected.toJSON()},
                    type: 'POST',
                    dataType: 'json',
                    async: false,
                    beforeSend: function() {
                        loading();
                    },
                    success: function(response) {
                        if (response.result) {
                            $.pnotify({title: 'Aviso', text: 'Salvamento de vínculos realizados com sucesso!', type: 'success'});
                        } else {
                            $.pnotify({title: 'Aviso', text: 'Não foi possivel salvar os vínculos!', type: 'warning'});
                        }
                    },
                    complete: function() {
                        AssuntosCollection.reset();
                        loaded();
                    },
                    error: function(response) {
                        $.pnotify({title: 'Aviso', text: 'Erro ao executar a operação!', type: 'danger'});
                    }
                });

            } else {
                $.pnotify({title: 'Aviso', text: 'Não há nenhuma alteração para ser salva', type: 'warning'});
            }

            $.when(this.onRender()).then(function() {
                that.eventChangeComboTipoOcorrencia();
            });
            AssuntosCollection.reset();
        },
        'click #btn-reset-grid': function() {
            AssuntosCollection.reset();
            that = this;
            $.when(this.onRender()).then(function() {
                that.eventChangeComboTipoOcorrencia();
            });
        }
    }
});

//Criando rotas internas para funcionalidade
var AppRouter = Backbone.Router.extend({
    routes: {
        "assunto/compartilhar/:id": "assuntos"
    },
    "assuntos": function(id) {
        G2S.layout.content.destroy();
        G2S.layout.content.show(new CompartilharAssuntoEditarItemView());

        $('#filter-id-tipo-assunto input').val(1).trigger('keyup');
    }
});
var rota = new AppRouter();

//Fazendo chamada da tela inicial da funcionalidade
G2S.layout.content.show(new CompartilharAssuntoItemView());


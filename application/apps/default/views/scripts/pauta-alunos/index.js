var PesquisaView = Marionette.ItemView.extend({
        template: '#pesquisa-view',
        ui: {
            'contentList': '#content-lista',
            'id_periodoletivo': '[name="id_periodoletivo"]',
            'id_disciplina': '[name="id_disciplina"]',
            'id_projetopedagogico': '[name="id_projetopedagogico"]'
        },
        tagName: 'div',
        onShow: function () {
            this.$el.find('input').first().focus();
        },
        onRender: function () {
            var that = this;
            //Select Periodo Letivo
            var PeriodoLetivoModel = Backbone.Model.extend({
                idAttribute: 'id_periodoletivo',
                defaults: {
                    id_periodoletivo: null,
                    st_periodoletivo: ''
                }
            });
            ComboboxView({
                el: that.ui.id_periodoletivo,
                model: PeriodoLetivoModel,
                url: '/pauta-alunos/find-periodo-letivo',
                optionLabel: 'st_periodoletivo',
                emptyOption: '[Selecione]',
                parse: function (response) {
                    if (!response.length) {
                        $.pnotify({
                            title: 'Aviso',
                            type: 'warning',
                            text: 'Nenhum período letivo encontrado'
                        });
                    }
                    return response;
                }
            });

        },
        events: {
            'submit form': 'actionSearch',
            'change [name="id_periodoletivo"]': 'refreshDisciplina',
            'change [name="id_disciplina"]': 'refreshProjeto',
            'click #gerar-pdf': 'gerarPdf'
        },
        refreshDisciplina: function () {
            var that = this;
            if (that.ui.id_periodoletivo.val() != '') {
                // Montar SELECT Disciplina
                var DisciplinaModel = Backbone.Model.extend({
                    idAttribute: 'id_disciplina',
                    defaults: {
                        id_disciplina: null,
                        st_disciplina: ''
                    }
                });
                ComboboxView({
                    el: that.ui.id_disciplina,
                    model: DisciplinaModel,
                    url: '/pauta-alunos/find-disciplina-by-periodo-letivo/?id_periodoletivo=' + that.ui.id_periodoletivo.val(),
                    optionLabel: 'st_disciplina',
                    emptyOption: '[Selecione]'
                });
            }
        },
        refreshProjeto: function () {
           var that = this;
            if (that.ui.id_disciplina.val() != '') {
                // Montar SELECT Projeto Pedagógico
                var ProjetoModel = Backbone.Model.extend({
                    idAttribute: 'id_projetopedagogico',
                    defaults: {
                        id_projetopedagogico: null,
                        st_projetopedagogico: ''
                    }
                });
                loading();
                ComboboxView({
                    el: that.ui.id_projetopedagogico,
                    model: ProjetoModel,
                    url: '/pauta-alunos/find-curso-by-disciplina/?id_disciplina=' + that.ui.id_disciplina.val(),
                    optionLabel: 'st_projetopedagogico',
                    emptyOption: '[Todos]'
                });
                loaded();
            }
        },
        actionSearch: function (e) {
            e.preventDefault();
            var that = this;

            var id_periodoletivo = Number(that.ui.id_periodoletivo.val());
            var id_disciplina = Number(that.ui.id_disciplina.val());
            var id_projetopedagogico = Number(that.ui.id_projetopedagogico.val());

            if (!id_periodoletivo && !id_disciplina) {
                $.pnotify({
                    type: 'warning',
                    title: 'Aviso',
                    text: 'Selecione Periodo Letivo e Disciplina para continuar.'
                });
                return false;
            } else {
                $.ajax({
                        url: 'pauta-alunos/pesquisa-alunos',
                        data: {
                            'id_disciplina': id_disciplina
                            , 'id_projetopedagogico': id_projetopedagogico
                            , 'st_nomeentidade': G2S.loggedUser.get('st_nomeentidade')
                        },
                        success: function (data) {
                            that.ui.contentList.empty().html(data);
                        },
                        dataType: 'html'
                    }
                )
                ;
            }
        },
        gerarPdf: function () {
            var that = this;
            var bl_avaliacao = this.$el.find('[name="bl_avaliacao"]').is(':checked');
            var bl_presencial = this.$el.find('[name="bl_presencial"]').is(':checked');
            var dataPdf = this.$el.find('[name="data_pdf"]').val();
            if (dataPdf == '') {
                $.pnotify({
                    type: 'warning',
                    title: 'Aviso',
                    text: 'Selecione uma data para a geração do PDF.'
                });
                return false;
            } else if (!bl_avaliacao && !bl_presencial) {
                $.pnotify({
                    type: 'warning',
                    title: 'Aviso',
                    text: 'Selecione pelo menos um tipo de lista.'
                });
                return false;
            } else {
                try {

                    var id_periodoletivo = Number(that.ui.id_periodoletivo.val());
                    var id_disciplina = Number(that.ui.id_disciplina.val());
                    var id_projetopedagogico = Number(that.ui.id_projetopedagogico.val());

                    var url = '/pauta-alunos/gerar-pdf/?id_disciplina=' + id_disciplina + '&id_projetopedagogico=' + id_projetopedagogico
                        + '&data=' + dataPdf + '&bl_avaliacao=' + bl_avaliacao + '&bl_presencial=' + bl_presencial + '&st_nomeentidade=' + G2S.loggedUser.get('st_nomeentidade');
                    window.open(url, '_blank');
                    return false;
                }
                catch (err) {
                    return false;
                }
            }
        }
    })
    ;

G2S.layout.addRegions({
    search: '#search-content'
});

var PesquisaIni = new PesquisaView();
G2S.layout.search.show(PesquisaIni);
loaded();
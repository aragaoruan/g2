/**
 * Função para gerar Select
 * @param el
 * @param collection
 * @param optionLabel
 * @param optionValue
 * @param optionSelected
 */
function geraSelect(el, collection, optionLabel, optionValue, optionSelected) {

    var view = new SelectView({
        el: el,
        collection: collection,
        childViewOptions: {
            optionLabel: optionLabel,
            optionValue: optionValue,
            optionSelected: optionSelected
        }
    });
    view.render();
}

//Model fixa para retornar unidades
var UnidadeModel = Backbone.Model.extend({
    defaults: {
        id_entidade: '',
        st_nomeentidade: ''
    },
    url: '/entidade/retornar-entidade-recursiva-id'
});

var UnidadesCollection = Backbone.Collection.extend({
    model: UnidadeModel,
    url: '/entidade/retornar-entidade-recursiva-id'
});

var VwTurmaProjetoPedagogicoCollection = Backbone.Collection.extend({
    model: VwTurmaProjetoPedagogico,
    url: '/relatorio-grade-projeto-entidade/retornar-projetos/',
    parse: function (response) {
        $.each(response, function (i, obj) {
            response[i].st_projetopedagogico = obj.id_projetopedagogico + ' - ' + obj.st_projetopedagogico;
        });

        return response;
    }
});

/**
 ******************************************************************************************
 ******************************************************************************************
 *************************** LAYOUT FUNCIONALIDADE ****************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var TelaRelatorioLayout = Marionette.LayoutView.extend({
    template: '#template-layout-relatorio',
    tagName: 'div',
    className: 'container-fluid',
    onShow: function () {
        this.populaSelectEntidade();
        loaded();
        return this;
    },
    ui: {
        'idUnidade': '#id_entidade',
        'idProjeto': '#id_projetopedagogico',
        'formPesquisa': '#form-consulta'
    },
    events: {
        'change #id_entidade': 'populaSelectProjeto',
        'submit #form-consulta': 'pesquisarDados'
    },
    populaSelectEntidade: function () {
        var that = this;
        var collectionUnidades = new UnidadesCollection();
        collectionUnidades.fetch({
            beforeSend: function () {
                loading();
            },
            complete: function () {
                loaded();
            },
            success: function () {
                geraSelect(that.ui.idUnidade, collectionUnidades, 'st_nomeentidade', 'id_entidade', null);
            }
        });
    },
    populaSelectProjeto: function () {
        var that = this;
        var turmaProjetoCollection = new VwTurmaProjetoPedagogicoCollection();
        turmaProjetoCollection.url += '?id_entidade=' + that.ui.idUnidade.val();
        turmaProjetoCollection.fetch({
            beforeSend: function () {
                that.ui.idProjeto.empty().html('<option value="">Selecione</option>');
                loading();
            },
            complete: function () {
                loaded();
            },
            success: function () {
                geraSelect(that.ui.idProjeto, turmaProjetoCollection, 'st_projetopedagogico', 'id_projetopedagogico', null);
            }
        });
    },
    pesquisarDados: function (e) {
        this.ui.formPesquisa.attr('action', '/relatorio-grade-projeto-entidade/gerar-xls-relatorio');
        this.ui.formPesquisa.attr('target', '_blank');
        this.ui.formPesquisa.submit();
    }
});

var layoutRelatorio = new TelaRelatorioLayout();
G2S.show(layoutRelatorio);
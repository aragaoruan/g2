var SituacaoCollection = Backbone.Collection.extend({
    model: Situacao,
    url: '/api/situacao'
});
var EvolucaoCollection = Backbone.Collection.extend({
    model: Evolucao,
    url: '/api/evolucao'
});
var ModelVenda = Backbone.Model.extend({
    defaults: {
        id_usuario: '',
        st_nomecompleto: '',
        st_cpf: '',
        id_venda: '',
        id_entidade: '',
        id_situacao: '',
        st_situacao: '',
        id_evolucao: '',
        st_evolucao: '',
        id_vendaproduto: '',
        id_produto: '',
        st_produto: ''
    },
    url: function () {
        return this.id ? '/api/venda-assinatura/' + this.id : '/api/venda-assinatura';
    }
});
var CollectionGrid = Backbone.Collection.extend({
    model: ModelVenda
});
var SituacaoItemView = Marionette.ItemView.extend({
    tagName: 'option',
    template: _.template("<%=st_situacao%>"),
    initialize: function () {
        this.$el.attr('value', this.model.get('id_situacao'));
        return this;
    }
});
var EvolucaoItemView = Marionette.ItemView.extend({
    tagName: 'option',
    template: _.template("<%=st_evolucao%>"),
    initialize: function () {
        this.$el.attr('value', this.model.get('id_evolucao'));
        return this;
    }
});
var SituacaoCollectionView = Marionette.CollectionView.extend({
    childView: SituacaoItemView
});
var EvolucaoCollectionView = Marionette.CollectionView.extend({
    childView: EvolucaoItemView
});
var ItemViewPrincipal = Marionette.ItemView.extend({
    tagName: 'tr',
    template: '#template-row-item',
    onRender: function () {
        //desabilita botão de cancelar para registros não confirmados
        if (this.model.get('id_evolucao') != 10) {
            this.$el.find('td').find('button').attr('disabled', 'disabled').attr('title', 'Cancelada');
        }
    },
    cancelarVenda: function () {
        var that = this;
        bootbox.confirm("Tem certeza de que deseja cancelar esta venda?", function (result) {
            if (result) {
                that.model.set({
                    id_evolucao: 8,
                    st_evolucao: 'Cancelada'
                });
                that.model.save(null, {
                    beforeSend: function () {
                        loading();
                    },
                    success: function (model, response) {
                        $.pnotify({title: 'Venda Cancelada', text: 'Venda cancelada com sucesso!', type: 'success'});
                    },
                    error: function (model, response) {
                        $.pnotify({title: 'Erro ao Cancelar', text: 'Erro ao tentar cancelar a venda!', type: 'error'});
                    },
                    complete: function () {
                        that.render();
                        loaded();
                    }
                });
            }
        });
    },
    events: {
        'click .btn-cancelar': 'cancelarVenda'
    }
});
var PrincipalView = Marionette.CompositeView.extend({
    template: '#template-tela-principal',
    childView: ItemViewPrincipal,
    childViewContainer: 'tbody',
    onShow: function () {
        loaded();
        if (!this.collection.length) {
            this.$el.find('table').hide();
        }
        this.populaSituacao();
        this.populaEvolucao();
        return this;
    },
    populaSituacao: function () {
        var that = this;
        var collectionSituacao = new SituacaoCollection();
        collectionSituacao.url += '?st_tabela=tb_venda';
        collectionSituacao.fetch({
            beforeSend: function () {
                that.$el.find('#id_situacao').html('<option>Carregando...</option>');
            },
            success: function () {
                that.$el.find('#id_situacao').empty();
                var view = new SituacaoCollectionView({
                    el: '#id_situacao',
                    collection: collectionSituacao
                });
                view.render();
                that.$el.find('#id_situacao').prepend('<option value="" selected>Todos</option>');
            }
        });
    },
    populaEvolucao: function () {
        var that = this;
        var collectionEvolucao = new EvolucaoCollection();
        collectionEvolucao.url += '/st_tabela/tb_venda';
        collectionEvolucao.fetch({
            beforeSend: function () {
                that.$el.find('#id_evolucao').html('<option>Carregando...</option>');
            },
            success: function (collection) {
                that.$el.find('#id_evolucao').empty();
                var collectionEvolucaoRender = new EvolucaoCollection();
                collectionEvolucao.each(function (model, i) {
                    if (model.get('id_evolucao') == 8 || model.get('id_evolucao') == 10) {
                        collectionEvolucaoRender.add(model);
                    }
                });
                var view = new EvolucaoCollectionView({
                    el: '#id_evolucao',
                    collection: collectionEvolucaoRender
                });
                view.render();
                that.$el.find('#id_evolucao').prepend('<option value="" selected>Todos</option>');
            }
        });
    },
    pesquisar: function () {
        var that = this;
        var st_nomecompleto = null;
        var st_cpf = null;
        var id_situacao = null;
        var id_evolucao = null;
        var st_buscar = this.$el.find('#st_buscar').val();
        if (st_buscar) {
            if (this.$el.find('#st_nomecompleto').is(':checked')) {
                st_nomecompleto = st_buscar;
            }
            if (this.$el.find('#st_cpf').is(':checked')) {
                st_cpf = st_buscar;
            }
        }
        if (this.$el.find('#id_situacao').val())
            id_situacao = this.$el.find('#id_situacao').val();
        if (this.$el.find('#id_evolucao').val())
            id_evolucao = this.$el.find('#id_evolucao').val();
        var params = {
            st_nomecompleto: st_nomecompleto,
            st_cpf: st_cpf,
            id_situacao: id_situacao,
            id_evolucao: id_evolucao
        };
        var strParams = '';
        $.each(params, function (key, value) {
            if (value)
                strParams += key + '/' + value + '/';
        });
        that.collection.url = '/api/venda-assinatura/' + strParams;
        that.collection.fetch({
            beforeSend: function () {
                loading();
            },
            complete: function () {
                if (!that.collection.length) {
                    $.pnotify({
                        title: "Sem resultado!",
                        text: "Sem resultado para exibição!",
                        type: "warning"
                    });
                    that.$el.find('table').hide();
                } else {
                    that.$el.find('table').show();
                }
            },
            success: function () {
                loaded();
            }
        });
        return false;
    },
    events: {
        'submit #form-pesquisa': 'pesquisar'
    }
});
$(function () {
    var collection = new CollectionGrid();
    G2S.layout.content.show(new PrincipalView({collection: collection}));
    $('*[data-toggle]').tooltip();
});




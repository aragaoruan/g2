/**
 * Created by paulo.silva on 31/07/14.
 */

/**
 ******************************************************************************************
 ******************************************************************************************
 ********* GLOBAIS ************************************************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var editable = true;
var tipoProduto;
var formaPagamento;
var collectionVendaCartasCredito;
var collectionProdutoVenda;
var collectionVwVendaLancamento;
var collectionFormaVwMeioDivisao;
var collectionResponsavelFinanceiro;
var valoresModel;
var lancamentoVendaModel;
var nu_valorbruto = 0;
var nu_valorliquido = 0;
var thatLancamento;
var thatViewFormaPagamento;
var thatViewListaProdutos;
var pessoaFixada;
var atendentesCollection;
var contratoRegraVenda;
var thatAutoComplete;
var thatCartaCredito;
var lancamentosParaRemover = [];
var campanhaPontualidadeCollection;
var permissaoContrato = false;
var permissaoConfirmarRecebimento = false;
var permissaoAlterarDtVencimentoBoleto = false;
var alteracoesdeVenda = [];
var viewFormulario;
/**
 * Array com tipos de lançamentos baseados na tb_lancamentovenda na coluna bl_entrada
 * @type {*[]}
 */
var TiposLancamentos = [
    {
        id: 1,
        name: 'Entrada',
        bl_entrada: 1
    },
    {
        id: 2,
        name: 'Parcela',
        bl_entrada: 0
    }
];

/**
 ******************************************************************************************
 ******************************************************************************************
 ********* MODELS *************************************************************************
 ******************************************************************************************
 ******************************************************************************************
 */

var VwProdutoVendaModel = VwProdutoVenda.extend({
    idAttribute: 'id_vendaproduto'
});

/**
 * Instancia a model de venda e adiciona um atributo fake
 * @type {Venda}
 */
var VendaModel = Venda.extend({
    initialize: function () {
        this.defaults.st_atendente;
        return this;
    },
    idAttribute: 'id_venda'
});

/**
 * Model para representar a SpCampanhaVendaProduto
 * @type {Backbone.Model}
 */
var SpCampanhaVendaProduto = Backbone.Model.extend({
    defaults: {
        id_campanhacomercial: '',
        id_entidade: '',
        id_situacao: '',
        st_situacao: '',
        nu_disponibilidade: '',
        st_campanhacomercial: '',
        st_descricao: '',
        dt_inicio: '',
        dt_fim: '',
        bl_todasformas: '',
        bl_todosprodutos: '',
        id_tipocampanha: '',
        st_tipocampanha: '',
        id_formapagamento: '',
        id_produto: '',
        id_venda: '',
        nu_descontominimo: '',
        nu_descontomaximo: '',
        id_tipodesconto: '',
        nu_valordesconto: ''
    },
    idAttribute: 'id_campanhacomercial'
});

/**
 * Modelo para representar NucleoFuncionariotm
 * @type {Backbone.Model}
 */
var NucleoFuncionariotm = Backbone.Model.extend({
    defaults: {
        'id_nucleofuncionariotm': '',
        'id_funcao': '',
        'st_funcao': '',
        'id_usuario': '',
        'st_nomecompleto': '',
        'id_nucleotm': '',
        'dt_cadastro': '',
        'bl_ativo': '',
        'id_usuariocadastro': '',
        'id_situacao': '',
        'st_situacao': '',
        'st_nucleotm': ''
    },
    url: '/venda/retornar-funcionario-nucleo'
});

/**
 * Model implementando novo atributo baseado na model de Lancamento
 * @type {Lancamento}
 */
var LancamentoVendaModel = Lancamento.extend({
    initialize: function () {
        this.defaults.bl_entrada = 0;
    }
});

/**
 * Model para template de Valores
 * @type {Backbone.Model}
 */
var ValoresModel = Backbone.Model.extend({
    defaults: {
        nu_valorbruto: 0,
        nu_valorliquido: 0,
        nu_valordesconto: 0,
        nu_percentualdesconto: 0,
        nu_descontocarta: 0
    }
});

/**
 * reescrevendo a url da model e mudando o atributo de id
 * @type {FormaPagamento}
 */
var FormaPagamentoModel = FormaPagamento.extend({
    url: function () {
        return this.id ? '/forma-pagamento/retornar-forma-pagamento-por-aplicacao/' + this.id : '/forma-pagamento/retornar-forma-pagamento-por-aplicacao/';
    },
    idAttribute: 'id_formapagamento'
});

/**
 * Model Backbone local para formas de pagamento selecionada
 * @type {Backbone.Model}
 */
var FormaPagamentoSelectedModel = Backbone.Model.extend({
    defaults: {
        id_formapagamento: '',
        st_formapagamento: '',
        id_evolucao: '',
        st_evolucao: '',
        id_situacao: '',
        st_situacao: '',
        nu_maxparcelas: '',
        id_atendente: '',
        st_nomeatendente: '',
        id_contratoregra: '',
        st_contratoregra: '',
        id_campanhapontualidade: '',
        st_campanhapontualidade: '',
        id_cupom: '',
        st_cupom: ''
    }
});

/**
 * Model Backbone local para tipo de lançamento
 * @type {Backbone.Model}
 */
var TipoLancamentoModel = Backbone.Model.extend({
    defaults: {
        id: '',
        name: '',
        bl_entrada: ''
    }
});

/**
 * Módel para representar Responsaveis Finanaceiro
 * @type {Backbone.Model}
 */
var ResponsavelFinanceiroModel = Backbone.Model.extend({
    defaults: {
        id: '',
        st_cpf: '',
        st_nomecompleto: '',
        st_cnpj: '',
        st_tiporesponsavel: 'Fisica'
    },
    idAttribute: 'id_usuario'
});

/**
 * Model para layout de produto
 * @type {VwProduto}
 *
 */
var ProdutoVendaModel = VwProduto.extend();

var HistoricoCartaCreditoModel = Backbone.Model.extend({//modelo de dados necessários para o histórico da carta de crédito
    defaults: {
        st_produto: '',
        dt_utilizacao: '',
        nu_valorutilizado: '',
        nu_valordisponivel: ''
    }
});

/**
 ******************************************************************************************
 ******************************************************************************************
 ********* COLLECTIONS ********************************************************************
 ******************************************************************************************
 ******************************************************************************************
 */

/**
 * Collection para campanha de pontualidade
 */
var CampanhaPontualidadeCollection = Backbone.Collection.extend({
    model: CampanhaComercial,
    url: function () {
        return '/api/campanha-comercial/?' + $.param(_.defaults(this.data));
    },
    data: {
        id_tipocampanha: null
    }
});

/**
 * Collection para Regra de Contrato
 * @type @exp;Backbone@pro;Collection@call;extend
 */
var ContratoRegraCollection = Backbone.Collection.extend({
    model: ContratoRegra,
    url: '/api/contrato-regra'
});

var VwEntidadeCartaoCollection = Backbone.Collection.extend({
    model: VwEntidadeCartao,
    url: '/api/vw-entidade-cartao'
});

/**
 * Collection para Atendente
 * @type @exp;Backbone@pro;Collection@call;extend
 */
var NucleoFuncionariotmCollection = Backbone.Collection.extend({
    model: NucleoFuncionariotm,
    url: '/venda/retornar-funcionario-nucleo'
});

/**
 * Collection para Responsavel Financeiro
 * @type @exp;Backbone@pro;Collection@call;extend
 */
var ResponsavelFinanceiroCollection = Backbone.Collection.extend({
    model: ResponsavelFinanceiroModel
});

/**
 * Colelction Produto venda
 * @type @exp;Backbone@pro;Collection@call;extend
 */
var VwProdutoVendaCollection = Backbone.Collection.extend({
    model: VwProdutoVendaModel,
    url: '/api/vw-produto-venda'
});

/**
 * Collection para SP
 * @type {Backbone.Collection}
 */
var SpCampanhaVendaProdutoCollection = Backbone.Collection.extend({
    model: SpCampanhaVendaProduto,
    url: '/venda/retorna-sp-campanha-venda-produto'
});

/**
 * Collection para FormaDiaVencimento
 * @type {Backbone.Collection}
 */
var FormaDiaVencimentoCollection = Backbone.Collection.extend({
    model: FormaDiaVencimento,
    url: '/api/forma-dia-vencimento'
});

/**
 * Collection para Layout de produto
 * @type {Backbone.Collection}
 */
var ProdutoVendaCollection = Backbone.Collection.extend({
    model: ProdutoVendaModel
});

var FormaPagamentoCollection = Backbone.Collection.extend({
    model: FormaPagamentoModel,
    url: '/forma-pagamento/retornar-forma-pagamento-por-aplicacao/'
});

/**
 * Collection para Tipo Produto
 * @type {Backbone.Collection}
 */
var TipoProdutoCollection = Backbone.Collection.extend({
    model: TipoProduto,
    url: '/api/tipo-produto',
    parse: function (response) {
        //Reescreve o parse pois a requisição ao api esta retornando o mensageiro
        if (response.type == 'success') {
            return response.mensagem;
        }
    }
});

/**
 *Collection para VwProduto
 * @type {Backbone.Collection}
 */
var VwProdutoCollection = Backbone.Collection.extend({
    model: VwProduto,
    url: 'produto/pesquisar-produto'
});

/**
 * Collection temporaria para tipo de lançamento
 * @type {Backbone.Collection}
 */
var TipoLancamentoCollection = new Backbone.Collection(TiposLancamentos);

/**
 * Collection para vw_vendalancamento
 * @type {Backbone.Collection}
 */
var VwVendaLancamentoCollection = Backbone.Collection.extend({
    model: VwVendaLancamento,
    url: '/api/vw-venda-lancamento'
});

/**
 * Collection para vw_formameiodivisao
 * @type {Backbone.Collection}
 */
var VwFormaMeioDivisaoCollection = Backbone.Collection.extend({
    model: VwFormaMeioDivisao.extend({
        idAttribute: 'id_meiopagamento'
    }),
    url: '/api/vw-forma-meio-divisao'
});

/**
 * Collection para SpTramite
 * @type @exp;Backbone@pro;Collection@call;extend
 */
var SpTramiteCollection = Backbone.Collection.extend({
    model: SpTramite,
    url: '/api/sp-tramite'
});

var VwCartaCreditoCollection = Backbone.Collection.extend({
    model: VwCartaCredito,
    url: '/api/vw-carta-credito'
});

var VendaCartaCreditoCollection = Backbone.Collection.extend({
    model: VendaCartaCredito,
    url: '/api/venda-carta-credito'
});

var HistoricoCartaCreditoCollection = Backbone.Collection.extend({ //collection que contém a url da função que retornará o histórico da carta de crédito
    model: HistoricoCartaCreditoModel,
    url: '/venda/retornar-historico-credito/'
});

/**
 ******************************************************************************************
 ******************************************************************************************
 ********* INSTANCIAS PARA COLLECTIONS QUE BUSCA COISAS QUE SÃO COMUNS ********************
 ******************************************************************************************
 ******************************************************************************************
 */


//Instancia a collection para os atendentes e faz o fetch
atendentesCollection = new NucleoFuncionariotmCollection();
atendentesCollection.fetch();

// Instancia a collection e faz o fetch
tipoProduto = new TipoProdutoCollection();
tipoProduto.fetch();

// Instancia a collection e faz o fetch
formaPagamento = new FormaPagamentoCollection();

//Instancia da collection de spCampanhaVendaProduto
var spCampanhaVendaProdutoCollection = new SpCampanhaVendaProdutoCollection();

//Intancia a model de venda
var vendaModel = new VendaModel();

// Instancia a collection para responsavel financeiros
collectionResponsavelFinanceiro = new ResponsavelFinanceiroCollection();

// Instancia a collection para layout de produto venda
collectionProdutoVenda = new VwProdutoVendaCollection();// new ProdutoVendaCollection();


// Instancia a collection para a vw de lançamentos
collectionVwVendaLancamento = new VwVendaLancamentoCollection();


// Model para Layout de valores
valoresModel = new ValoresModel();

// Instancia a model de Lancamentovendas
lancamentoVendaModel = new LancamentoVendaModel();

// Instancia a collection de forma e meio divisao
collectionFormaVwMeioDivisao = new VwFormaMeioDivisaoCollection();

campanhaPontualidadeCollection = new CampanhaPontualidadeCollection();

/**
 * Instancia a modelo temporaria da forma de pagamento selecionada
 * @type FormaPagamentoSelectedModel|FormaPagamentoSelectedModel
 */
var modelFormaPagamentoSelected = new FormaPagamentoSelectedModel();

/**
 * Instancia a collection de contrato regra e faz o fetch
 * @type ContratoRegraCollection
 */
var contratoRegraCollection = new ContratoRegraCollection();
contratoRegraCollection.fetch();

collectionVendaCartasCredito = new VendaCartaCreditoCollection();


function verificaIdVenda() {
    if (vendaModel && !vendaModel.get('id_venda')) {
        $.pnotify({
            "title": "Erro!",
            "type": "error",
            "text": "Id da venda não encontrado, tente novamente!"
        })
        return false;
    }
    return true;
}

/**
 ******************************************************************************************
 ******************************************************************************************
 ********* LAYOUT FUNCIONALIDADE **********************************************************
 ******************************************************************************************
 ******************************************************************************************
 */

/**
 * LAYOUT
 * @type {Marionette.Layout}
 */
var LayoutVenda = Marionette.LayoutView.extend({
    template: '#layout-venda',
    tagName: 'div',
    className: 'container-fluid',
    autoComplete: componenteAutoComplete,
    printContrato: false,
    regions: {
        produtos: '#box-produtos',
        tramites: '#box-tramite',
        vendas: '#box-venda',
        lancamentos: '#box-lancamentos',
        modal: '#box-modal',
        valores: '#box-valores',
        adicionarPessoaRegion: '#container-adicionar-pessoa'
    },
    ui: {},
    initialize: function () {
        this.autoComplete.clearFixModel();
        VerificarPerfilPermissao(28, 627, true).done(function (response) {
            if (response.length == 1) {
                permissaoContrato = true;
            } else {
                permissaoContrato = false;
            }
        });

        VerificarPerfilPermissao(39, 627, true).done(function (response) {
            if (response.length == 1) {
                permissaoConfirmarRecebimento = true;
            } else {
                permissaoConfirmarRecebimento = false;
            }
        });

        var response = VerificarPerfilPermissao(51, 627);

        if (response.length) {
            permissaoAlterarDtVencimentoBoleto = true;
        } else {
            permissaoAlterarDtVencimentoBoleto = false;
        }

    },
    onShow: function () {
        //Cria uma variavel no ecopo local para armazenar o this
        var that = this;
        loading();
        //armazena o autocomplete
        var autocomplete = this.autoComplete;
        thatAutoComplete = this.autoComplete;

        //Verifica se o G2S.editModel esta preenchido, isso significa que estou editando ou já salvei a venda
        if (G2S.editModel) {
            //Seta os dados da venda
            vendaModel.set(G2S.editModel);
            lancamentosParaRemover = [];
            that.buscaDados();//busca os dados complementares

        } else {// G2S.editModel não encontrado
            //Chama o componente de autocomplete
            autocomplete.main({
                model: VwPesquisarPessoa,
                element: $("#autoComplete"),
                elementTemplateListaComponente: "#template-componente-list-autocomplete-venda",
            }, function () {
                that.setResponsavel(autocomplete.getFixModel());
                pessoaFixada = autocomplete.getFixModel();
                abreNegociacao();
                //Oculta o botão de incluir nova pessoa
                that.$el.find('#btn-adicionar-pessoa').css({
                    'display': 'none'
                });
            }, function () {
                fechaNegociacao();
            });

            if (!autocomplete.getFixModel()) {
                $('.content-box-vendas').hide('fast');
            }
            //Exibe o botão de incluir nova pessoa caso não esteja editando
            that.$el.find('#btn-adicionar-pessoa').css({
                'display': 'inline'
            });
        }
        loaded();
        return this;
    },
    habilitaBotoes: function () {

        if (vendaModel.get('id_venda') && vendaModel.get('id_evolucao') == 7) {
            //se a venda tiver sido salva e a evolução for Aguardando Negociação
            this.$el.find('#btn-salvar').removeAttr('disabled');
            $("select[name='id_formapagamento']").removeAttr('disabled');
            $("select[name='id_contratoregra']").removeAttr('disabled');
            $("input[name='bl_usarcarta']").removeAttr('disabled');

            //Foi comentada essa permissão de editar atendente, porque um ... pediu pra fazer e depois pediu para não ter mais

            //VerificarPerfilPermissao(23, 627, true).done(function (response) {
            //    if (response.length) {
            //        $("select[name='id_usuario']").removeAttr('disabled');
            //    }
            //});

            //se tiver lançamento ou se o desconto for 100%
            if (collectionVwVendaLancamento.length > 0 || valoresModel.get('nu_percentualdesconto') == '100.00' || valoresModel.get('nu_valorliquido') == '0.00') {
                this.$el.find('#btn-concluir').removeAttr('disabled');
            }
            editable = true;

            $('#btn_excluir_lancamentos').removeAttr('disabled');
            $('#chk_all_lancamentos').removeAttr('disabled');

        } else if (vendaModel.get('id_evolucao') == 9) {
            //se a evolucao for Aguardando Recebimento
            this.$el.find('#btn-concluir').attr('disabled', 'disabled');
            this.$el.find('#btn-salvar').attr('disabled', 'disabled');

            $('#btn_excluir_lancamentos').attr('disabled', 'disabled');
            $('#chk_all_lancamentos').attr('disabled', 'disabled');
            $('#meio_lancamento').attr('disabled', 'disabled');
            $('#tipo_lancamento').attr('disabled', 'disabled');
            $('#nu_valorparcela').attr('disabled', 'disabled');
            $('#id_responsavel').attr('disabled', 'disabled');
            $('#btn-procurar-responsavel').attr('disabled', 'disabled');
            $('#btn-add-lancamento').attr('disabled', 'disabled');

            //se a evolução for aguardando recebimento e o desconto for de 100% ele habilita o botão de confirmar
            if (valoresModel.get('nu_percentualdesconto') == '100.00' || valoresModel.get('nu_valorliquido') == '0.00') {
                this.$el.find('#btn-confirmar').removeAttr('disabled');
            }
            editable = false;
        } else if (vendaModel.get('id_evolucao') == 10) {
            //Se a evolução da venda estiver como confirmada desabilita todos os botões
            this.$el.find('#btn-concluir').attr('disabled', 'disabled');
            this.$el.find('#btn-salvar').attr('disabled', 'disabled');
            this.$el.find('#btn-confirmar').attr('disabled', 'disabled');
            this.$el.find('#btn-ficha').removeAttr('disabled');
            if (permissaoContrato || vendaModel.get('nu_viascontrato') < 1 || vendaModel.get('nu_viascontrato') == '') {
                $('#btn-contrato').removeAttr('disabled')
            } else {
                $('#btn-contrato').attr('disabled', 'disabled');
            }
            loaded();

            $('#btn_excluir_lancamentos').attr('disabled', 'disabled');
            $('#chk_all_lancamentos').attr('disabled', 'disabled');
            $('#meio_lancamento').attr('disabled', 'disabled');
            $('#tipo_lancamento').attr('disabled', 'disabled');
            $('#nu_valorparcela').attr('disabled', 'disabled');
            $('#id_responsavel').attr('disabled', 'disabled');
            $('#btn-procurar-responsavel').attr('disabled', 'disabled');
            $('#btn-add-lancamento').attr('disabled', 'disabled');

            editable = false;
        }
        if (permissaoContrato || vendaModel.get('nu_viascontrato') < 1 || vendaModel.get('nu_viascontrato') == '') {
            this.printContrato = this.verificaImpressaoContrato();
        }

    },
    verificaImpressaoContrato: function () {
        var configs = retornarEsquemaConfiguracao(1, false);
        var print = false;
        //verifica se a venda já foi salva e se tem configuração
        if (vendaModel.get('id_venda') && configs.length) {
            var config = configs.at(0);//atribui os valores da configuraçao
            //verifica se o valor da configura e verdadeiro
            if (parseFloat(config.get('st_valor'))) {
                if (vendaModel.get('id_evolucao') == 10) {
                    //remove o atributo
                    this.$el.find('#btn-contrato').removeAttr('disabled');
                    print = true;//seta como true, pode imprimir
                }
            } else if (config.get('st_valor') == "") {//verifica se o valor é vazio
                print = true;
            } else {
                //verifica se a evolução da venda e confirmada (pode imprimir)
                if (vendaModel.get('id_evolucao') == 10) {
                    this.$el.find('#btn-contrato').removeAttr('disabled');
                    print = true;
                } else {
                    print = false;
                    this.$el.find('#btn-contrato').attr('disabled', 'disabled');
                }
            }
        }
        return print;
    },
    buscaDados: function () {
        var that = this;
        var autocomplete = this.autoComplete;

        //Limpa a div do autocomplete para não escrever o componente 2 vezes
        $("#autoComplete").empty();

        //Busca a pessoa
        var vwPessoa = new VwPesquisarPessoa();
        vwPessoa.url = '/pessoa/get-vw-pesquisar-pessoa-by-id/id/' + vendaModel.get('id_usuario');
        vwPessoa.fetch({
            beforeSend: function () {
                loading();
            },
            complete: function () {
                loaded();
            },
            error: function (model, response) {
                loaded();
                $.pnotify({
                    type: 'error',
                    title: 'Erro!',
                    text: 'Houve um erro ao consultar dados da pessoa. ' + response.responseText
                });
                fechaNegociacao();
            },
            success: function (model) {
                //Transforma o resultado do fetch em objeto Json
                var pessoaFix = vwPessoa.toJSON();
                pessoaFix.id = pessoaFix.id_usuario; //Cria um novo atributo no objeto
                if (verificaIdVenda()) {
                    //Seta as urls das collections que tenho que carregar os dados
                    collectionProdutoVenda.url = '/api/vw-produto-venda/id_venda/' + vendaModel.get('id_venda');
                    collectionVwVendaLancamento.url = '/api/vw-venda-lancamento/id_venda/' + vendaModel.get('id_venda');

                    //Fetch na collection de ProdutoVenda
                    collectionProdutoVenda.fetch({
                        async: false,
                        complete: function () {

                            //Instancia a model de vwVendaUsuario
                            var vendaUsuario = new VwVendaUsuario();
                            vendaUsuario.id = vendaModel.get('id_venda');//Atribui o id_venda como id de vendaUsuario

                            //Faz o fetch de vendaUsuario
                            vendaUsuario.fetch({
                                async: false,
                                complete: function () {
                                    //Seta os valores do modelo no modelo que definine a forma de pagamento selecionada
                                    modelFormaPagamentoSelected.set({
                                        id_evolucao: vendaUsuario.get('id_evolucao'),
                                        st_evolucao: vendaUsuario.get('st_evolucao'),
                                        id_situacao: vendaUsuario.get('id_situacao'),
                                        st_situacao: vendaUsuario.get('st_situacao'),
                                        id_formapagamento: vendaUsuario.get('id_formapagamento'),
                                        st_formapagamento: vendaUsuario.get('st_formapagamento'),
                                        id_atendente: vendaUsuario.get('id_atendente')
                                    });


                                    //Faz o fetch nos lançamentos da venda
                                    collectionVwVendaLancamento.fetch({
                                        async: false,
                                        success: function () {
                                            //Limpa o auto complete
                                            autocomplete.clearFixModel();
                                            autocomplete.modelRender = pessoaFix; //seta no modelRenderer do autocomplete
                                            autocomplete.setFixModel();//e define o registro fixado
                                            pessoaFixada = autocomplete.getFixModel();//recupera os dados da pessoa fixada

                                            //Cria o AutoComplete
                                            autocomplete.main({
                                                model: VwPesquisarPessoa,
                                                element: $("#autoComplete"),
                                                elementTemplateListaComponente: "#template-componente-list-autocomplete-venda",
                                            }, abreNegociacao, fechaNegociacao);
                                            $("#btnClearAutocomplete").attr('disabled', 'disabled');
                                            $("#btn-adicionar-pessoa").attr('disabled', 'disabled');
                                            //AutoComplete, recupera dados para setar pessoa como responsavel
                                            that.setResponsavel(pessoaFixada);
                                        },
                                        complete: function () {
                                            that.habilitaBotoes();
                                            that.buscarDadosContratoRegra();
                                            that.buscarDadosCampanhaPontualidade();
                                            that.buscarCupom();
                                            that.buscaDadosCartaCredito();
                                            calculaValorBruto();
                                        }
                                    });

                                }
                            });

                        }
                    });
                }

            }
        });

    },
    buscaDadosCartaCredito: function () {
        collectionVendaCartasCredito.url = '/api/venda-carta-credito?id_venda=' + vendaModel.get('id_venda');
        collectionVendaCartasCredito.fetch({
            beforeSend: function () {
                loading();
            },
            complete: function () {
                loaded();
            },
            success: function () {
                if (collectionVendaCartasCredito.length) {
                    thatViewFormaPagamento.$el.find('input[name="bl_usarcarta"]').attr('checked', 'checked');
                    thatViewFormaPagamento.mostrarTabelaCartaCredito();
                }
            }
        });
    },
    buscarCupom: function () {
        modelFormaPagamentoSelected.set({
            id_cupom: vendaModel.get('id_cupom'),
            st_cupom: vendaModel.get('st_cupom') ? vendaModel.get('st_cupom') : 'Nenhum cupom utilizado!'
        });
    },
    buscarDadosCampanhaPontualidade: function () {
        var campanhaComercial = new CampanhaComercial();
        campanhaComercial.url = '/api/campanha-comercial/' + vendaModel.get('id_campanhapontualidade');
        campanhaComercial.fetch({
            beforeSend: function () {
                loading();
            },
            complete: function () {
                loaded();
            },
            success: function () {
                modelFormaPagamentoSelected.set({
                    id_campanhapontualidade: campanhaComercial.get('id'),
                    st_campanhapontualidade: campanhaComercial.get('st_campanhacomercial')
                });
            }
        });
    },
    buscarDadosContratoRegra: function () {//retorna dados do contrato regra
        var contratoRegraModel = new ContratoRegra();//instancia a modelo
        //altera a url do modelo
        contratoRegraModel.url = '/venda/retornar-contrato-regra-venda/id_venda/' + vendaModel.get('id_venda');
        //faz o fetch
        contratoRegraModel.fetch({
            success: function () {
                contratoRegraVenda = contratoRegraModel;
                //no sucesso seta os valores na model de forma de pagamento
                modelFormaPagamentoSelected.set({
                    id_contratoregra: contratoRegraModel.get('id_contratoregra'),
                    st_contratoregra: contratoRegraModel.get('st_contratoregra')
                });
            }
        });
    },
    setResponsavel: function (pessoa) {
        //Instancia a model de responsavel financeiro e seta os valores
        var modeloResponsavel = new ResponsavelFinanceiroModel({
            id: pessoa.id,
            st_cpf: pessoa.st_cpf,
            st_nomecompleto: pessoa.st_nomecompleto
        });
        //atribui a model na collection de responsaveis financeiros
        collectionResponsavelFinanceiro.add(modeloResponsavel);
    },
    verificaVendaXLancamentos: function () {
        //Verificas se o valor da venda fecha com o valor dos lançamentos

        //Variavel para retornar no final
        var valid = false;

        //Recupera o valor da venda
        var valorVenda = formatValueToReal(vendaModel.get('nu_valorliquido'));

        //Cria uma variavel para incrementar o calculo dos lançamentos
        var valorLancamentos = 0;

        //Percorre a collection e calcula o total dos lancamentos
        collectionVwVendaLancamento.each(function (model, i) {
            valorLancamentos += formatValueToReal(model.get('nu_valor'));
        });

        //Verifica se o valor da venda é igual o dos lançamentos ou se tem 100% de desconto!
        var valorRestante = formatValueToReal(thatLancamento.valorRestante);
        if (valorVenda == valorLancamentos.toFixed(2) || valoresModel.get('nu_percentualdesconto') == '100.00' || valorRestante == 0) {
            valid = true;
        }
        return valid;
    },
    verificaLancamentos: function () {
        var flag = true;

        //verifica se tem lançamento
        if (!collectionVwVendaLancamento.length && valoresModel.get('nu_valorliquido') != '0.00') {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Informe os lançamentos!'
            });
            flag = false;
        } else {
            collectionVwVendaLancamento.each(function (model, i) {
                if (model.get('nu_valor') == 0) {
                    $.pnotify({
                        type: 'warning',
                        title: 'Atençao!',
                        text: 'Remova os lançamentos com valor zero.'
                    });
                    flag = false;
                }
            });
        }
        return flag;

    },
    verificaContratoRegra: function () {

        /*verifica se existem produtos do tipo taxa, se houver somente deste tipo não vai validar a regra de contrato, do
         contrario vai validar.*/

        var colProdVenda = collectionProdutoVenda.where({'id_tipoproduto': 2});
        if (colProdVenda) {
            if (colProdVenda.length != collectionProdutoVenda.length) {
                var id_contratoregra = modelFormaPagamentoSelected.get('id_contratoregra');
                if (id_contratoregra) {
                    var contratoRegraModel = contratoRegraCollection.findWhere({id_contratoregra: parseInt(id_contratoregra)});
                    if ((typeof  contratoRegraModel != 'undefined') && contratoRegraModel.get('id_tiporegracontrato') && contratoRegraModel.get('id_tiporegracontrato').id_tiporegracontrato == 2) {
                        if (collectionProdutoVenda.length > 1) {
                            $.pnotify({
                                type: 'warning',
                                title: 'Atençao!',
                                text: 'Para contratos do tipo Free Pass, somente um produto é aceito.'
                            });
                            return false;
                        } else {
                            return true;
                        }
                    } else {
                        return true;
                    }
                } else {
                    $.pnotify({
                        type: 'warning',
                        title: 'Atençao!',
                        text: 'Atenção, nenhuma regra de contrato foi selecionada. Favor entrar em contato com o administrador.'
                    });
                    return true;
                }
            }
        }
        return true;
    },
    validaDados: function () {
        var elem = this.$el;

        var valid = true;

        if (!editable) {
            $.pnotify({
                type: 'error',
                title: 'Ops!',
                text: 'A evolução atual desta venda não permite alterações em nenhum campo!'
            });
            valid = false;
        }

        // Verificar se adicionou algum produto
        if (!collectionProdutoVenda.length) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Nenhum produto selecionado!'
            });
            valid = false;
        }

        // Verificar se prenecheu nos produtos adicionados,
        // o campo Turma (os obrigatorios, sao somente alguns)
        var requiredTurmas = elem.find('#lista-produtos-venda select[name="id_turma"]:invalid');
        if (requiredTurmas.length) {
            requiredTurmas.first().focus();
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Este produto precisa de uma Turma.'
            });
            valid = false;
        }

        //Valida a forma de pagamento
        if (parseFloat(valoresModel.get('nu_valorliquido')) != 0 && !modelFormaPagamentoSelected.get('id_formapagamento')) {
            $('#divPlanoPagamento').addClass('control-group error');
            valid = false;
        } else {
            $('#divPlanoPagamento').removeClass('control-group error');
        }


        var colProdVenda = collectionProdutoVenda.where({'id_tipoproduto': 2});
        if (colProdVenda) {
            if (colProdVenda.length != collectionProdutoVenda.length) {
                if (!modelFormaPagamentoSelected.get('id_contratoregra')) {
                    $.pnotify({
                        type: 'warning',
                        title: 'Atenção!',
                        text: 'Regra de Contrato não informada!'
                    });
                    valid = false;
                }
            }
        } else {
            ///verifica se a evolução da venda é Aguardando Recebimento e se não foi passado o contrato regra
            if (!modelFormaPagamentoSelected.get('id_contratoregra')) {
                $.pnotify({
                    type: 'warning',
                    title: 'Atenção!',
                    text: 'Regra de Contrato não informada!'
                });
                valid = false;
            }
        }

        //Verifia se tem um atendente selecionado
        if (!modelFormaPagamentoSelected.get('id_atendente')) {
            $('#divAtendente').addClass('control-group error');
            valid = false;
        } else {
            $('#divAtendente').removeClass('control-group error');
        }

        //verifica se a evolução da venda é igual a 9 (Aguardando Recebimento) e se o valor liquido da venda é diferente do total de lançamentos
        if (vendaModel.get('id_evolucao') == 9 && parseFloat(valoresModel.get('nu_valorliquido')) != formatValueToReal(lancamentoVendaModel.get('nu_valor'))) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'O valor total para os lançamentos é maior que o Valor Líquido da venda.'
            });
            valid = false;
        }

        //verifica se o valor do desconto das cartas é maior que o da venda
        //if (parseFloat(valoresModel.get('nu_descontocarta')) > parseFloat(valoresModel.get('nu_valorliquido'))) {
        //    $.pnotify({
        //        type: 'warning',
        //        title: 'Atenção!',
        //        text: 'Os descontos aplicados com as Cartas de Crédito são maiores do que o vvalor Líquido da Venda.'
        //    });
        //    valid = false;
        //}

        return valid;
    },
    salvarVenda: function () {
        if (this.validaDados() && this.verificaContratoRegra()) {
            if (this.verificaLancamentos()) {
                //Seta os dados na model de venda
                vendaModel.set({
                    'nu_valorbruto': formatValueToShow(valoresModel.get('nu_valorbruto')),
                    'nu_valorliquido': formatValueToShow(valoresModel.get('nu_valorliquido')),
                    'id_usuario': pessoaFixada.id,
                    'nu_valorcartacredito': valoresModel.get('nu_descontocarta'),
                    'id_campanhapontualidade': modelFormaPagamentoSelected.get('id_campanhapontualidade') ? modelFormaPagamentoSelected.get('id_campanhapontualidade') : null,
                    'id_atendente': modelFormaPagamentoSelected.get('id_atendente')
                });

                //Percorre a collection e formata o valor
                collectionVwVendaLancamento.each(function (model, i) {
                    model.set('nu_valor', formatValueToReal(model.get('nu_valor')));
                });

                //Seta os valores para
                collectionVendaCartasCredito.each(function (model, i) {
                    model.set('nu_valorutilizado', formatValueToReal(model.get('nu_valorutilizado')));
                });

                //Monta um objeto com os parametros
                var params = {
                    'pessoa': pessoaFixada,
                    'produtos': collectionProdutoVenda.toJSON(),
                    'forma_pagamento': modelFormaPagamentoSelected.toJSON(),
                    'lancamentos': collectionVwVendaLancamento.toJSON(),
                    'venda': vendaModel.toJSON(),
                    'carta_credito': collectionVendaCartasCredito.toJSON(),
                    'lancamentos_remover': lancamentosParaRemover,
                    'alteracoesVenda': alteracoesdeVenda,
                    'iniciandoVenda': vendaModel.get('id_venda') == '' ? true : false
                };

                console.log(vendaModel);
                
                var that = this;
                $.ajax({
                    dataType: 'json',
                    type: 'post',
                    url: '/venda/salvar-venda',
                    data: params,
                    beforeSend: function () {
                        loading();
                    },
                    success: function (data) {
                        if (data.type == 'success') {
                            G2S.editModel = data.text.venda;
                            vendaModel.set(G2S.editModel);

                            var produtos = [];
                            if (data.text.produto_venda) {
                                $.each(data.text.produto_venda, function (i, obj) {
                                    var produto = new VwProdutoVenda();
                                    produto.set(obj);
                                    produtos[i] = produto;
                                });
                                collectionProdutoVenda.reset();
                                collectionProdutoVenda.set(produtos);
                            }

                            if (data.text.evolucao) {
                                var evolucao = data.text.evolucao;
                                modelFormaPagamentoSelected.set(evolucao);
                                that.buscarDadosContratoRegra();
                                that.buscarDadosCampanhaPontualidade();
                            }

                            if (data.text.carta_credito) {
                                that.buscaDadosCartaCredito();
                            }
                            alteracoesdeVenda = [];
                            $.pnotify({title: data.title, text: "Dados salvos com sucesso!", type: data.type});
                        } else {
                            $.pnotify({title: data.title, text: data.text, type: data.type});
                        }
                    },
                    complete: function () {
                        if (verificaIdVenda()) {
                            collectionVwVendaLancamento.url = '/api/vw-venda-lancamento/id_venda/' + vendaModel.get('id_venda');
                            collectionVwVendaLancamento.fetch({
                                success: function () {
                                    //var viewLancamentos = new ViewListaLancamentos({
                                    //    model: lancamentoVendaModel,
                                    //    collection: collectionVwVendaLancamento
                                    //});
                                    //telaVendas.lancamentos.show(viewLancamentos);
                                }
                            });
                        }

                        //O RBD me salvou falando pra fazer dessa forma, já que o retorno do metodo de salvar não estava
                        //funcionando nem com ajuda de reza brava.

                        $.ajax({
                            dataType: 'json',
                            type: 'post',
                            url: '/venda/retorna-venda',
                            data: G2S.editModel,
                            beforeSend: function () {
                                loading();
                            },
                            success: function (data) {
                                if (data.id_evolucao) {
                                    var evolucao = data.id_evolucao;
                                    G2S.editModel = data;
                                    vendaModel.set(G2S.editModel);
                                    modelFormaPagamentoSelected.set(evolucao);
                                    that.buscarDadosContratoRegra();
                                    that.buscarDadosCampanhaPontualidade();
                                    that.buscarCupom();
                                }
                            }, complete: function () {
                                calculaValorBruto();
                                that.habilitaBotoes();
                                thatViewFormaPagamento.selecionaFormaPagamento();
                                desabilitaCampos();
                                loaded();
                            },
                            error: function (response) {
                                $.pnotify({
                                    type: 'error',
                                    title: 'Erro!',
                                    text: 'Houve um erro ao consultar dados da venda! [' + response.responseText + ']. Recarregue a página e tente novamente.'
                                });

                            }
                        });

                        alteracoesdeVenda = [];

                    },
                    error: function (data) {
                        $.pnotify({title: data.title, text: data.text, type: data.type});
                    }
                });
            } else {
                return false;
            }
        } else {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Verifique os campos obrigatórios!'
            });
        }
        return false;
    },
    concluirNegociacao: function () {
        var that = this;

        if (vendaModel.get('id_evolucao') != 7) {
            $.pnotify({
                type: 'error',
                title: 'Ops!',
                text: 'A evolução atual desta venda não permite concluir negociação!'
            });
            return false;
        }
        if (!that.verificaVagaMatricula()) {
            bootbox.confirm("A turma selecionada não possuí vaga do tipo Free Pass disponível, deseja realmente concluir esta venda?", function (result) {
                if (result) {
                    //Verifica se o os valores da venda e dos laçamentos fecham

                    //seta o id_evolucao da venda como 9 :: Aguardando Recebimento
                    vendaModel.set('id_evolucao', 9);
                    that.salvarVenda();
                }
            });
        } else {
            if (that.verificaVendaXLancamentos()) {
                //chama o confirm
                bootbox.confirm("Deseja realmente concluir esta venda?", function (result) {
                    if (result) {
                        //Verifica se o os valores da venda e dos laçamentos fecham

                        //seta o id_evolucao da venda como 9 :: Aguardando Recebimento
                        vendaModel.set('id_evolucao', 9);
                        that.salvarVenda();
                    }
                });
            } else {
                $.pnotify({
                    title: 'Atenção',
                    text: 'A venda não pode ser concluída, verifique os valores.',
                    type: 'warning'
                });
            }
        }

    },
    verificaVagaMatricula: function () {
        var id_contratoregra = modelFormaPagamentoSelected.get('id_contratoregra');
        var contratoRegraModel = contratoRegraCollection.findWhere({id_contratoregra: parseInt(id_contratoregra)});
        //if (typeof contratoRegraModel != 'undefined') {
        if ((typeof  contratoRegraModel != 'undefined') && contratoRegraModel.get('id_tiporegracontrato') && contratoRegraModel.get('id_tiporegracontrato').id_tiporegracontrato) {
            if (contratoRegraModel.get('id_tiporegracontrato').id_tiporegracontrato == 2) {
                ret = true;
                $.ajax({
                    dataType: 'json',
                    type: 'post',
                    url: '/venda/verifica-vagas-freepass',
                    async: false,
                    data: {
                        id_turma: collectionProdutoVenda.models[0].get('id_turma')
                    },
                    beforeSend: function () {
                        loading();
                    },
                    success: function (data) {
                        if (data.type == 'success') {
                            ret = true;
                        } else {
                            ret = false;
                        }
                    },
                    complete: function () {
                        loaded();
                    },
                    error: function () {
                        $.pnotify({
                            title: 'Erro!',
                            text: 'Erro ao verificar alocação de matrículas Free Pass.',
                            type: 'error'
                        });
                    }
                });
                return ret;
            } else {
                return true;
            }
        } else {
            return true;
        }
        //} else {
        //    $.pnotify({
        //        title: 'Atenção!',
        //        text: 'Regra de Contrato da venda não encontrado.',
        //        type: 'warning'
        //    });
        //    return false;
        //}
    },
    confirmarRecebimento: function () {
        var that = this;

        if (vendaModel.get('id_evolucao') != 9) {
            $.pnotify({
                type: 'error',
                title: 'Ops!',
                text: 'A evolução atual desta venda não permite confirmar recebimento!'
            });
            return false;
        }

        /* Só permite confirmar o recebimento se não houverem entradas pendentes.
         * Faz essa verificação pela quantidade de botões de recebimento habilitados
         * dentro da tabela #lista-lancamentos-venda.
         */

        if (($('#lista-lancamentos-venda .btn-success').length) == 0) {
            if (that.verificaVagaMatricula()) {
                if (that.verificaVendaXLancamentos()) {
                    $.ajax({
                        dataType: 'json',
                        type: 'post',
                        url: '/recebimento/processo-ativacao-automatica',
                        data: {
                            id_venda: vendaModel.get('id_venda')
                        },
                        beforeSend: function () {
                            loading();
                        },
                        success: function (data) {
                            if (data.type == 'success') {
                                that.$el.find('#btn-confirmar').attr('disabled', 'disabled');
                                if (permissaoContrato || vendaModel.get('nu_viascontrato') <= 1) {
                                    $('#btn-contrato').removeAttr('disabled');
                                } else {
                                    $('#btn-contrato').attr('disabled', 'disabled');
                                }
                                that.$el.find('#btn-ficha').removeAttr('disabled');

                                modelFormaPagamentoSelected.set({
                                    'id_evolucao': 10,
                                    'id_situacao': 39,
                                    'st_evolucao': "Confirmada",
                                    'st_situacao': "Ativo"
                                });
                                vendaModel.set({
                                    'id_evolucao': 10,
                                    'id_situacao': 39
                                });
                                desabilitaCampos();
                            }
                            $.pnotify({
                                title: data.title,
                                text: data.text,
                                type: data.type
                            });
                            var VwGerarContratoCollection = Backbone.Collection.extend({
                                model: VwGerarContrato
                            });

                            var modelContrato;
                            var contratoCollection = new VwGerarContratoCollection();
                            contratoCollection.url = '/api/vw-gerar-contrato/id_venda/' + vendaModel.get('id_venda');
                            contratoCollection.fetch({
                                beforeSend: function () {
                                    loading();
                                },
                                success: function () {
                                    if (contratoCollection.length)
                                        modelContrato = contratoCollection.at(0);
                                },
                                complete: function () {
                                    if (modelContrato) {
                                        $.ajax({
                                            dataType: 'json',
                                            type: 'post',
                                            url: '/venda/gerar-espelho',
                                            data: {
                                                filename: modelContrato.get('id_contrato'),
                                                id_venda: vendaModel.get('id_venda'),
                                                id_textosistema: contratoRegraVenda.get('id_contratomodelo'),
                                                type: 'pdf',
                                                id_contrato: modelContrato.get('id_contrato'),
                                                confirmacao: true
                                            },
                                            beforeSend: function () {
                                            },
                                            success: function (data) {
                                                $.pnotify({
                                                    title: data.title,
                                                    text: data.text,
                                                    type: data.type
                                                });
                                            },
                                            error: function (data) {
                                                $.pnotify({
                                                    title: 'Erro',
                                                    text: 'Erro ao tentar gerar espelho do contrato!',
                                                    type: 'error'
                                                });
                                            }
                                        })
                                    }
                                }

                            });
                        },
                        complete: function () {
                            loaded();
                        },
                        error: function () {
                            $.pnotify({
                                title: 'Erro 1065',
                                text: 'Erro ao tentar salvar as confirmações de recebimento.',
                                type: 'error'
                            });
                        }
                    });
                } else {
                    $.pnotify({
                        title: 'Atenção',
                        text: 'A venda não pode ser concluída, verifique os valores.',
                        type: 'warning'
                    });
                }
            } else {
                $.pnotify({
                    title: 'Atenção',
                    text: 'Não foi possível matricular o aluno nesta turma porque não há mais vagas para alunos do tipo Free Pass.',
                    type: 'warning'
                });
            }
        } else {
            $.pnotify({
                title: 'Erro',
                text: 'É necessário quitar todas as entradas antes de confirmar o recebimento / matrícula.',
                type: 'error'
            });
        }
    },
    abrirFicha: function (e) {
        if (vendaModel.get('id_venda') && vendaModel.get('id_evolucao') == 10) {
            this.abrirPopUp('/ficha-matricula/?id_venda=' + vendaModel.get('id_venda'));
        } else {
            $.pnotify({
                title: 'Atenção',
                text: 'Ficha de Matrícula indisponível no momento, verifique se a venda esta definida e a evolução da mesma é "Confirmada".',
                type: 'warning'
            });
        }
    },
    verificaEspelho: function ($parametro) {
        return $.ajax({
            dataType: 'json',
            type: 'post',
            url: '/venda/verifica-espelho',
            async: false,
            data: {
                id_venda: $parametro
            },
            beforeSend: function () {
            },
            success: function (data) {
                return data.existe;
            },
            error: function (data) {
                $.pnotify({
                    title: 'Erro',
                    text: 'Erro ao tentar gerar espelho do contrato!',
                    type: 'error'
                });
            }
        });
    },
    abrirContrato: function () {
        if (vendaModel.get('id_venda') && (vendaModel.get('id_evolucao') == 10 || this.printContrato)) {

            /**
             * Até o proximo comentário a baixo deve ser retirado e o trecho a baixo descomentado para que volte a
             * funcionar a geração de segunda via e justificativa
             * @type {LayoutVenda}
             */

            var that = this;
            var nomePessoa = pessoaFixada.st_nomecompleto;
            var fileName = "Contrato - " + nomePessoa;
            var id_contrato = null;
            var modelContrato;
            var id_textositema = 141;
            var id_venda = vendaModel.get('id_venda');
            var VwGerarContratoCollection = Backbone.Collection.extend({
                model: VwGerarContrato
            });
            if (vendaModel.get('nu_viascontrato') && vendaModel.get('nu_viascontrato') >= 1) {
                var model = this.model;
                var viewModalJustificativa = new ViewModalJustificativa({
                    model: model
                });
                telaVendas.modal.show(viewModalJustificativa);
            } else {
                $.ajax({
                    dataType: 'json',
                    type: 'post',
                    url: '/venda/contabiliza-via-contrato',
                    async: false,
                    data: {
                        id_venda: id_venda,
                        nu_viascontrato: vendaModel.get('nu_viascontrato') ? vendaModel.get('nu_viascontrato') : 0
                    },
                    beforeSend: function () {
                    },
                    success: function (data) {

                        if (data.nu_viascontrato >= 1 && !permissaoContrato) {
                            $('#btn-contrato').attr('disabled', 'disabled');
                        } else {
                            $('#btn-contrato').removeAttr('disabled', 'disabled');
                        }
                        $.pnotify({
                            title: 'Sucesso',
                            text: 'Contrato gerado e contabilizado com sucesso!',
                            type: 'success'
                        });

                        var contratoCollection = new VwGerarContratoCollection();
                        contratoCollection.url = '/api/vw-gerar-contrato/id_venda/' + vendaModel.get('id_venda');
                        contratoCollection.fetch({
                            beforeSend: function () {
                                loading();
                            },
                            success: function () {
                                if (contratoCollection.length)
                                    modelContrato = contratoCollection.at(0);
                            },
                            complete: function () {
                                loaded();
                                if (contratoRegraVenda && contratoRegraVenda.get('id_contratomodelo')) {
                                    id_contrato = modelContrato.get('id_contrato');
                                    id_textositema = contratoRegraVenda.get('id_contratomodelo');
                                    that.abrirPopUp('/textos/geracao/?filename=' + fileName + '&id_textosistema=' + id_textositema + '&arrParams[id_contrato]=' + id_contrato + '&type=html');
                                } else if (modelContrato) {
                                    id_contrato = modelContrato.get('id_contrato');
                                    id_textositema = modelContrato.get('id_contratomodelo') ? modelContrato.get('id_contratomodelo') : id_textositema;
                                    that.abrirPopUp('/textos/geracao/?filename=' + fileName + '&id_textosistema=' + id_textositema + '&arrParams[id_contrato]=' + id_contrato + '&type=html');
                                } else {
                                    $.pnotify({
                                        title: 'Atenção',
                                        text: 'Contrato não encontrato, verifique as regras de contrato.',
                                        type: 'warning'
                                    });
                                }
                            }
                        });
                        vendaModel.set('nu_viascontrato', data.nu_viascontrato);
                    },
                    error: function (data) {
                        $.pnotify({
                            title: 'Erro',
                            text: 'Erro ao tentar contabilizar via do contrato!',
                            type: 'error'
                        });
                    }

                });
            }


            /**
             * Trecho de código abaixo foi comentado devido a necessidade de configuração dos contratos em produção.
             * Também junto está comentado a justificativa de segunda via do contrato.
             */

            //var that = this;
            //var id_venda = vendaModel.get('id_venda');
            //var verificaEspelho = this.verificaEspelho(id_venda);
            //if (verificaEspelho.responseJSON.existe) {
            //    if (vendaModel.get('nu_viascontrato') && vendaModel.get('nu_viascontrato') >= 1) {
            //        var model = this.model;
            //        var viewModalJustificativa = new ViewModalJustificativa({
            //            model: model
            //        });
            //        telaVendas.modal.show(viewModalJustificativa);
            //    } else {
            //        $.ajax({
            //            dataType: 'json',
            //            type: 'post',
            //            url: '/venda/contabiliza-via-contrato',
            //            async: false,
            //            data: {
            //                id_venda: id_venda,
            //                nu_viascontrato: vendaModel.get('nu_viascontrato') ? vendaModel.get('nu_viascontrato') : 0
            //            },
            //            beforeSend: function () {
            //            },
            //            success: function (data) {
            //                if (data.nu_viascontrato >= 1 && !permissaoContrato) {
            //                    $('#btn-contrato').attr('disabled', 'disabled');
            //                } else {
            //                    $('#btn-contrato').removeAttr('disabled', 'disabled');
            //                }
            //                $.pnotify({
            //                    title: 'Sucesso',
            //                    text: 'Contrato gerado e contabilizado com sucesso!',
            //                    type: 'success'
            //                });
            //                vendaModel.set('nu_viascontrato', data.nu_viascontrato);
            //            },
            //            error: function (data) {
            //                $.pnotify({
            //                    title: 'Erro',
            //                    text: 'Erro ao tentar contabilizar via do contrato!',
            //                    type: 'error'
            //                });
            //            }
            //
            //        });
            //        window.open('/upload/minhapasta/' + id_venda + '/ContratoNegociacao' + id_venda + '.pdf');
            //    }
            //} else {
            //    var nomePessoa = pessoaFixada.st_nomecompleto;
            //    var id_contrato = null;
            //    var modelContrato;
            //    var id_textositema = 141;
            //    var VwGerarContratoCollection = Backbone.Collection.extend({
            //        model: VwGerarContrato
            //    });
            //    if (vendaModel.get('nu_viascontrato') && vendaModel.get('nu_viascontrato') >= 1) {
            //        var model = this.model;
            //        var viewModalJustificativa = new ViewModalJustificativa({
            //            model: model
            //        });
            //        telaVendas.modal.show(viewModalJustificativa);
            //    } else {
            //        $.ajax({
            //            dataType: 'json',
            //            type: 'post',
            //            url: '/venda/contabiliza-via-contrato',
            //            async: false,
            //            data: {
            //                id_venda: id_venda,
            //                nu_viascontrato: vendaModel.get('nu_viascontrato') ? vendaModel.get('nu_viascontrato') : 0
            //            },
            //            beforeSend: function () {
            //            },
            //            success: function (data) {
            //                if (data.nu_viascontrato >= 1 && !permissaoContrato) {
            //                    $('#btn-contrato').attr('disabled', 'disabled');
            //                } else {
            //                    $('#btn-contrato').removeAttr('disabled', 'disabled');
            //                }
            //                $.pnotify({
            //                    title: 'Sucesso',
            //                    text: 'Contrato gerado e contabilizado com sucesso!',
            //                    type: 'success'
            //                });
            //                vendaModel.set('nu_viascontrato', data.nu_viascontrato);
            //            },
            //            error: function (data) {
            //                $.pnotify({
            //                    title: 'Erro',
            //                    text: 'Erro ao tentar contabilizar via do contrato!',
            //                    type: 'error'
            //                });
            //            }
            //        });
            //    }
            //    ;
            //    var contratoCollection = new VwGerarContratoCollection();
            //    contratoCollection.url = '/api/vw-gerar-contrato/id_venda/' + vendaModel.get('id_venda');
            //    contratoCollection.fetch({
            //        beforeSend: function () {
            //            loading();
            //        },
            //        success: function () {
            //            if (contratoCollection.length)
            //                modelContrato = contratoCollection.at(0);
            //        },
            //        complete: function () {
            //            loaded();
            //            if (contratoRegraVenda && contratoRegraVenda.get('id_contratomodelo')) {
            //                id_contrato = modelContrato.get('id_contrato');
            //                id_textositema = contratoRegraVenda.get('id_contratomodelo');
            //                that.abrirPopUp('/venda/gerar-espelho/?filename=' + id_contrato + '&id_venda=' + id_venda + '&id_textosistema=' + id_textositema + '&id_contrato=' + id_contrato + '&type=pdf');
            //            } else if (modelContrato) {
            //                id_contrato = modelContrato.get('id_contrato');
            //                id_textositema = modelContrato.get('id_contratomodelo') ? modelContrato.get('id_contratomodelo') : id_textositema;
            //                that.abrirPopUp('/textos/gerar-espelho/?filename=' + id_contrato + '&id_venda=' + id_venda + '&id_textosistema=' + id_textositema + '&id_contrato=' + id_contrato + '&type=pdf');
            //            } else {
            //                $.pnotify({
            //                    title: 'Atenção',
            //                    text: 'Contrato não encontrato, verifique as regras de contrato.',
            //                    type: 'warning'
            //                });
            //            }
            //        }
            //    });
            //}
        } else {
            $.pnotify({
                title: 'Atenção',
                text: 'Contrato indisponível no momento, verifique se a venda esta definida e a evolução da mesma é "Confirmada".',
                type: 'warning'
            });
        }
    },
    abrirPopUp: function (url) {
        window.open(url, 'janela', 'width=800, height=600, top=100, left=100, scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no');
    },
    toggleAbas: function (e) {
        e.preventDefault();
        var elem = $(e.currentTarget);
        var aba = elem.attr('href');
        if (!elem.parent('li').is('.active')) {
            elem.parent('li').parent('ul').find('li').removeClass('active');
            elem.parent('li').addClass('active');
            this.$el.find('.aba-container').hide();
            this.$el.find(aba).show();

            if (aba == '#box-tramite') {
                var spTramiteCollection = new SpTramiteCollection();
                if (vendaModel.get('id_venda')) {
                    spTramiteCollection.url += '?id_categoriarramite=3&chave[]=' + vendaModel.get('id_venda');
                    spTramiteCollection.fetch({
                        beforeSend: function () {
                            loading();
                        },
                        complete: function () {
                            loaded();
                        },
                        success: function () {
                            var tramiteView = new TramitesCompositeView({
                                collection: spTramiteCollection
                            });
                            telaVendas.tramites.show(tramiteView);
                        }
                    });
                } else {
                    var tramiteView = new TramitesCompositeView({
                        collection: spTramiteCollection
                    });
                    telaVendas.tramites.show(tramiteView);
                }
            }

        }

    },
    adicionarPessoa: function () {
        $("#container-adicionar-pessoa").css({
            'display': 'inline',
            'border': '1px solid #767676',
            'margin-top': '20px',
            'margin-right': '0px'
        });
        $("#btn-ocultar-form").css({
            'display': 'inline'
        });

        //$('#container-adicionar-pessoa').html(viewFormulario.template);

        viewFormulario = new ViewFormulario({
            model: modelFormulario,
            el: '#container-adicionar-pessoa',
            negociacao: true
        });
        viewFormulario.render();
        //this.adicionarPessoaRegion.show(viewFormulario.render());
    },
    ocultarFormulario: function () {
        //this.adicionarPessoaRegion.destroy();
        $('#container-adicionar-pessoa').empty();
        $("#container-adicionar-pessoa").css({
            'display': 'none'
        });
        $("#btn-ocultar-form").css({
            'display': 'none'
        });

        if (viewFormulario.model.get('id')) {
            $('#search-backbone').val(viewFormulario.model.get('st_cpf')).trigger('keyup');
        }

    },
    events: {
        'click #btn-concluir': 'concluirNegociacao',
        'click #btn-salvar': 'salvarVenda',
        'click #btn-confirmar': 'confirmarRecebimento',
        'click #btn-ficha': 'abrirFicha',
        'click #btn-contrato': 'abrirContrato',
        'click ul.nav-tabs li a': 'toggleAbas',
        'click #btn-adicionar-pessoa': 'adicionarPessoa',
        'click #btn-ocultar-form': 'ocultarFormulario'
    }
});

/**
 * View para modal de justificativa de segunda via de contrato
 */
var ViewModalJustificativa = Marionette.ItemView.extend({
    template: '#template-modal-justifica-contrato',
    ui: {
        justificativa: '#st_justificativacontrato'
    },
    onShow: function () {
        this.$el.parent().modal('show');
        return this;
    },
    destroyModel: function () {
        this.model.clear();
    },
    salvarJustificativa: function (e) {
        var that = this;

//Apagar quando for colocar quando for habilitar contrato em pdf
        var nomePessoa = pessoaFixada.st_nomecompleto;
        var fileName = "Contrato - " + nomePessoa;
        var id_contrato = null;
        var modelContrato;
        var id_textositema = 141;
        var id_venda = vendaModel.get('id_venda');
//apagar até aqui quando for habilitar o pdf

        if (this.ui.justificativa.val() == '') {
            $('#divJustificativa').addClass('control-group error');
            $('#divAvisoJustificativa').removeClass('hidden');
            return false;
        } else {
            $('#divJustificativa').removeClass('control-group error');
            $('#divAvisoJustificativa').addClass('hidden');
        }

//apagar daqui até o proximo comentário quando for habilitar o contrato pdf

        $.ajax({
            dataType: 'json',
            type: 'post',
            url: '/venda/salva-justificativa',
            async: false,
            data: {
                id_venda: vendaModel.get('id_venda'),
                st_justificativa: this.ui.justificativa.val(),
                nu_viascontrato: vendaModel.get('nu_viascontrato')
            },
            beforeSend: function () {
                loading();
            },
            success: function (data) {
                $.pnotify({
                    title: 'Sucesso',
                    text: 'Justificativa gerada e contrato contabilizado com sucesso!',
                    type: 'success'
                });

                var VwGerarContratoCollection = Backbone.Collection.extend({
                    model: VwGerarContrato
                });


                var modelContrato;
                var contratoCollection = new VwGerarContratoCollection();
                contratoCollection.url = '/api/vw-gerar-contrato/id_venda/' + vendaModel.get('id_venda');
                contratoCollection.fetch({
                    beforeSend: function () {
                        loading();
                    },
                    success: function () {
                        if (contratoCollection.length)
                            modelContrato = contratoCollection.at(0);
                    },
                    complete: function () {
                        loaded();
                        if (contratoRegraVenda && contratoRegraVenda.get('id_contratomodelo')) {
                            id_contrato = modelContrato.get('id_contrato');
                            id_textositema = contratoRegraVenda.get('id_contratomodelo');
                            var url = '/textos/geracao/?filename=' + fileName + '&id_textosistema=' + id_textositema + '&arrParams[id_contrato]=' + id_contrato + '&type=html';
                            window.open(url, 'janela', 'width=800, height=600, top=100, left=100, scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no');
                        } else if (modelContrato) {
                            id_contrato = modelContrato.get('id_contrato');
                            id_textositema = modelContrato.get('id_contratomodelo') ? modelContrato.get('id_contratomodelo') : id_textositema;
                            var url = '/textos/geracao/?filename=' + fileName + '&id_textosistema=' + id_textositema + '&arrParams[id_contrato]=' + id_contrato + '&type=html';
                            window.open(url, 'janela', 'width=800, height=600, top=100, left=100, scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no');
                        } else {
                            $.pnotify({
                                title: 'Atenção',
                                text: 'Contrato não encontrato, verifique as regras de contrato.',
                                type: 'warning'
                            });
                        }
                    }
                });

//apagar até aqui

                //window.open('/upload/minhapasta/' + data.id_venda + '/ContratoNegociacao' + data.id_venda + '.pdf');
                vendaModel.set('nu_viascontrato', data.nu_viascontrato);
                that.$el.parent().modal('hide');
            },
            error: function (data) {
                $.pnotify({
                    title: 'Erro',
                    text: 'Erro ao tentar criar a justificativa de via do contrato!',
                    type: 'error'
                });
            },
            complete: function () {
                loaded();
            }
        });
    },
    events: {
        //'click .btn-cancelar': 'destroyModel',
        'click #btnSaveJustificativa': 'salvarJustificativa'
    }
});

function desabilitaCampos() {
    if (vendaModel.get('id_evolucao') == 10 || vendaModel.get('id_evolucao') == 9) {

        //desabilita campos de carta de credito
        if (thatCartaCredito) {
            $.each(thatCartaCredito.children._views, function (i, view) {
                $.each(view.ui, function (i, elem) {
                    elem.attr('disabled', 'disabled');
                });
            });
        }

        //Desabilita os campos da view de forma de pagamento
        $.each(thatViewFormaPagamento.ui, function (i, elem) {
            elem.attr('disabled', 'disabled');
        });

        //Desabilita os elementos da lista de produtos
        thatViewListaProdutos.$el.find("#btn-pesquisar-produto").attr('disabled', 'disabled');
        $.each(thatViewListaProdutos.children._views, function (i, view) {
            $.each(view.ui, function (i, elem) {
                elem.attr('disabled', 'disabled');
            });
        });
        //verifica se existe lançamentos
        if (thatLancamento.collection.length) {
            $.each(thatLancamento.ui, function (i, elem) {
                if (i != 'btn_recarregar_lancamentos')
                    elem.attr('disabled', 'disabled');
            });
            $.each(thatLancamento.children._views, function (i, view) {
                var model = view.model;
                $.each(view.ui, function (i, elem) {
                    if (i == 'btn_confirmar') {
                        if (model.get('bl_entrada') && !model.get('bl_quitado') && permissaoConfirmarRecebimento) {
                            elem.removeAttr('disabled').addClass('btn-success');
                        } else {
                            elem.attr('disabled', 'disabled').removeClass('btn-success');
                        }
                    } else {
                        if (
                            i != 'btn_enviar_boleto' &&
                            i != 'btn_gerar_boleto' &&
                            i != 'btn_visualizar_boleto' &&
                            i != 'btn_recarregar_lancamentos'
                        )
                            elem.attr('disabled', 'disabled');
                    }
                });
            });
            thatLancamento.verificcaLancamentosHabilitaBotaoConfirmacarRecebimento();
        }
    }
}

function abreNegociacao(id) {
    //função que busca uma negociação do banco e cria as views correspondentes
    $('.content-box-vendas').show('fast');


    //abre e configura o box de produtos
    var viewListaProdutos = new ViewListaProdutos({
        collection: collectionProdutoVenda
    });

    //abre box de venda
    var viewFormasPagamento = new ViewFormasPagamento({
        //model
    });

    //abre e configura o box de lancamentos
    var viewLancamentos = new ViewListaLancamentos({
        model: lancamentoVendaModel,
        collection: collectionVwVendaLancamento
    });

    //Abre e configura view de valores
    valoresModel = new ValoresModel();
    var viewValores = new ValoresItemView({model: valoresModel});

    //show views
    telaVendas.produtos.show(viewListaProdutos);
    telaVendas.lancamentos.show(viewLancamentos);
    telaVendas.vendas.show(viewFormasPagamento);
    telaVendas.valores.show(viewValores);

    desabilitaCampos();

}

function fechaNegociacao(id) {
    // ao limpar o componente de pesquisa de pessoas, precisamos fechar as views

    //collection
    collectionProdutoVenda.remove(collectionProdutoVenda.models);
    collectionVwVendaLancamento.remove(collectionVwVendaLancamento.models);
    collectionResponsavelFinanceiro.remove(collectionResponsavelFinanceiro.models);
    collectionFormaVwMeioDivisao.remove(collectionFormaVwMeioDivisao.models);
    contratoRegraVenda = null;
    modelFormaPagamentoSelected = new FormaPagamentoSelectedModel();
    valoresModel.clear();
    lancamentoVendaModel.destroy();
    vendaModel.clear();
    // que estão abertas no layout
    telaVendas.produtos.destroy();
    telaVendas.tramites.destroy();
    telaVendas.vendas.destroy();
    telaVendas.lancamentos.destroy();
    telaVendas.valores.destroy();
    telaVendas.modal.destroy();
    telaVendas.adicionarPessoaRegion.destroy();

    editable = true;
    $('.content-box-vendas').hide('fast');
}

function calculaValorBruto() {
    //percorre a collection de produtos selecionados para venda e soma os valores dos produtos definindo o valor bruto
    var nu_somavalor = 0;
    var nu_somadesconto = 0;
    collectionProdutoVenda.each(function (produto) {
        var nu_valor = parseFloat(produto.get('nu_valorbruto'));
        var nu_valorvenda = parseFloat(produto.get('nu_valorliquido'));

        if (nu_valorvenda != nu_valor) {
            nu_somadesconto += nu_valor - nu_valorvenda;
        }
        nu_somavalor += nu_valor;

    });

    //pega o valor da carta
    var nu_valordescontocarta = 0;
    collectionVendaCartasCredito.each(function (carta) {
        nu_valordescontocarta += parseFloat(carta.get('nu_valorutilizado'));
    });
    //calcula a porcentagem de desconto
    var nu_porcentagemdesconto = (nu_somadesconto && nu_somavalor) ? (nu_somadesconto / nu_somavalor) * 100 : 0;
    //calcula o valor liquido
    var nu_valorliquido = nu_somavalor - nu_somadesconto - nu_valordescontocarta;

    //seta o valor liquido na model para lancamento
    lancamentoVendaModel.set('nu_valor', formatValueToShow(nu_valorliquido.toFixed(2)));

    //seta os valores no modelo
    valoresModel.set({
        'nu_valorbruto': nu_somavalor.toFixed(2),
        'nu_valordesconto': nu_somadesconto.toFixed(2),
        'nu_percentualdesconto': nu_porcentagemdesconto.toFixed(2),
        'nu_valorliquido': nu_valorliquido.toFixed(2),
        'nu_descontocarta': nu_valordescontocarta.toFixed(2)
    });
    //Abre e configura view de valores
    var viewValores = new ValoresItemView({
        model: valoresModel
    });
    thatLancamento.setValorVenda();
    //thatLancamento.calculaTotais();
    telaVendas.valores.show(viewValores);//show view de valores
}

/**
 ******************************************************************************************
 ******************************************************************************************
 ********* ELEMENTOS DE SELECT ************************************************************
 ******************************************************************************************
 ******************************************************************************************
 */


/**
 * ItemView para meio de pagamento
 * @type {ItemView}
 */
var MeioPagamentoItemView = Marionette.ItemView.extend({
    tagName: 'option',
    template: _.template("<%=st_meiopagamento%>"),
    initialize: function () {
        this.$el.attr('value', this.model.get('id_meiopagamento'));
        return this;
    }
});


/**
 * Item view para select
 * @type {ItemView}
 */
var ViewItemSelectTipoLancamento = Marionette.ItemView.extend({
    template: _.template('<%=name%>'),
    tagName: 'option',
    initialize: function () {
        this.$el.attr('value', this.model.id);
        return this;
    }
});


/**
 * CollectionView para Select de tipo de lançamento
 * @type {CollectionView}
 */
var TipoLancamentoView = Marionette.CollectionView.extend({
    tagName: 'select',
    childView: ViewItemSelectTipoLancamento,
    collectionEvents: {
        'add': function () {
            this.render();
        }
    }
});

/**
 * ItemView para Select de campanha
 * @type {ItemView}
 */
var ItemCampanhaProdutoView = Marionette.ItemView.extend({
    template: _.template('<%=st_campanhacomercial%>'),
    tagName: 'option',
    onRender: function () {
        this.$el.attr('value', this.model.get('id_campanhacomercial'));
        return this;
    }
});

/**
 * CollectionView para Select de Campanha
 * @type {CollectionView}
 */
var CampanhaProdutoView = Marionette.CollectionView.extend({
    tagName: 'select',
    childView: ItemCampanhaProdutoView,
    onRender: function () {
        return this;
    }
});

/**
 * CollectionView para meios de pagamento
 * @type {*}
 */
var MeioPagamentoCollectionView = Marionette.CollectionView.extend({
    tagName: 'select',
    childView: MeioPagamentoItemView,
    onRender: function () {
        this.$el.prepend('<option value="" selected>Selecione</option>');
        return this;
    }
});

/**
 ******************************************************************************************
 ******************************************************************************************
 ********* PRODUTOS ***********************************************************************
 ******************************************************************************************
 ******************************************************************************************
 */

/**
 * ItemView para Composite de Lista de produtos
 * @type {ItemView}
 */
var ViewItemProdutos = Marionette.ItemView.extend({
    template: '#template-item-produto',
    tagName: 'tr',
    modelEvents: {
        'change:id_campanhacomercial': function () {
            if (!this.model.get('id_campanhacomercial')) {
                this.calculaValorPorcentagem();
                this.ui.porcentagemDesconto.attr('disabled', 'disabled');
                this.ui.idCampanha.attr('disabled', 'disabled');
            } else {
                this.ui.idCampanha.removeAttr('disabled');
            }
        }
    },
    ui: {
        idMatricula: 'select[name="matricula"]',
        idCampanha: 'select[name="campanha"]',
        porcentagemDesconto: 'input[name="desconto_percent"]',
        valorDesconto: 'input[name="desconto_valor"]',
        btnRemove: '#btn_remover_produto',
        idTurma: '[name="id_turma"]',
        id_tiposelecao: 'select[name="id_tiposelecao"]'
    },
    selectDescontosOptions: [],
    nu_descontomaximo: null,
    id_tipodesconto: null,
    initialize: function () {
        var nu_valor = parseFloat(this.model.get('nu_valorliquido'));
        this.model.set('nu_valorliquido', nu_valor.toFixed(2));

        var valorDesconto = this.model.get('nu_descontovalor');
        this.model.set('nu_descontovalor', formatValueToShow(valorDesconto));

        var porcentagemDesconto = this.model.get('nu_descontoporcentagem') ? parseFloat(this.model.get('nu_descontoporcentagem')) : 0;
        this.model.set('nu_descontoporcentagem', formatValueToShow(porcentagemDesconto.toFixed(2)));
        return this;
    },
    onRender: function () {
        var that = this;
        var elem = this.$el;

        // Montar SELECT "Turma"
        var Model = Backbone.Model.extend({
            idAttribute: 'id_turma',
            defaults: {
                id_turma: null,
                st_turma: ''
            }
        });
        var idTurmaElem = elem.find(this.ui.idTurma);

        ComboboxView({
            el: idTurmaElem,
            model: Model,
            url: '/turma/get-turmas-by-produto/?id_produto=' + (that.model.get('id_produto')) + '&id_situacao=' + SITUACAO['TB_TURMA']['ATIVA'],
            optionLabel: 'st_turma',
            optionSelectedId: that.model.get('id_turma'),
            emptyOption: '[Selecione]'
        });

        // Verificar se possui turma, e nao permitir editar caso ja tenha
        if (this.model.get('id_turma'))
            idTurmaElem.attr('disabled', 'disabled');
        else
            idTurmaElem.removeAttr('disabled');


        // Verificar se o tipo de produto e um projeto pedagogico
        if (Number(this.model.get('id_tipoproduto')) == 1) {
            // Verificar se o campo Turma e obrigatorio
            $.getJSON('/api/projeto-pedagogico/' + this.model.get('id_produto'), function (response) {
                if (Number(response.bl_turma))
                    idTurmaElem.attr('required', 'required');
                else
                    idTurmaElem.removeAttr('required');
            });
        }


        if (this.model.get('id_campanhacomercial')) {
            this.ui.idCampanha.val(this.model.get('id_campanhacomercial'));
            this.toogleDescontos(this.model.get('id_campanhacomercial'));
        }
        this.desabilitaCampos();
        this.ui.porcentagemDesconto.maskMoney({precision: 2, decimal: ",", thousands: "."});
        this.ui.valorDesconto.maskMoney({precision: 2, decimal: ",", thousands: "."});


        // Montar SELECT "id_tiposelecao"
        var Model = Backbone.Model.extend({
            idAttribute: 'id_tiposelecao',
            defaults: {
                id_tiposelecao: null,
                st_tiposelecao: ''
            }
        });
        var id_tiposelecao = elem.find(this.ui.id_tiposelecao);
        ComboboxView({
            el: id_tiposelecao,
            model: Model,
            url: '/api/tipo-selecao',
            optionLabel: 'st_tiposelecao',
            optionSelectedId: that.model.get('id_tiposelecao'),
            emptyOption: '[Selecione]'
        });

        if (that.model.get('id_tipoproduto') != 1) {
            elem.find('.sel-tiposelecao').hide();
            elem.find('.col-tiposelecao').html('Nao se aplica');
        } else {
            elem.find('.sel-tiposelecao').show();
        }

        if (that.model.get('id_tipoproduto') == 2) {
            elem.find('.sel-turma').hide();
            elem.find('.col-turma').html('Nao se aplica');
        } else {
            elem.find('.sel-turma').show();
        }


        return this;
    },
    desabilitaCampos: function () {
        if (vendaModel.get('id_evolucao') == 10 || vendaModel.get('id_evolucao') == 9) {
            $.each(this.ui, function (i, elem) {
                elem.attr('disabled', 'disabled');
            });
        }
    },
    calculaValorVenda: function () {
        //recupera os valores
        var nu_valor = formatValueToReal(this.model.get('nu_valorbruto'));//valor do produto

        //valor $ desconto
        var nu_valordesconto = this.ui.valorDesconto.val() ? formatValueToReal(this.ui.valorDesconto.val()) : 0;

        //Campanha
        var id_campanha = this.ui.idCampanha.val();

        //calcula o desconto
        var nu_valorvenda = nu_valor - nu_valordesconto;


        if (nu_valordesconto > parseFloat(this.nu_descontomaximo)) {
            $.pnotify({
                title: 'Atenção!',
                text: 'O valor de desconto para este produto excede o valor máximo permitido.',
                type: 'warning'
            });
            this.ui.valorDesconto.val(this.model.get('nu_descontovalor'));
            this.ui.porcentagemDesconto.val(this.model.get('nu_descontoporcentagem'));
            return false;
        }

        if (nu_valordesconto > nu_valor) {
            $.pnotify({
                title: 'Atenção!',
                text: 'O valor de desconto é maior que o valor do produto.',
                type: 'warning'
            });
            this.ui.valorDesconto.val(this.model.get('nu_descontovalor'));
            this.ui.porcentagemDesconto.val(this.model.get('nu_descontoporcentagem'));
            return false;
        }


        //valor porcentagem do desconto
        var valorporcentagem = (nu_valordesconto / nu_valor) * 100;
        //seta o valor no modelo
        this.model.set('nu_valorliquido', nu_valorvenda.toFixed(2));//seta o desconto no model
        this.model.set('nu_descontoporcentagem', valorporcentagem.toFixed(2));
        this.model.set('nu_descontovalor', nu_valordesconto.toFixed(2));
        this.render();//renderiza novamente

        //monta a string com os options do select
        this.montaSelectCampanha(id_campanha);

        //escreve os valores nos inputs
        this.ui.porcentagemDesconto.val(formatValueToShow(valorporcentagem.toFixed(2)));
        this.ui.valorDesconto.removeAttr('disabled').val(formatValueToShow(nu_valordesconto.toFixed(2)));
        calculaValorBruto();
    },
    calculaValorPorcentagem: function () {
        //recupera os valores
        var nu_valor = formatValueToReal(this.model.get('nu_valorbruto'));//valor do produto

        //valor % desconto
        var nu_porcentagemdesconto = this.ui.porcentagemDesconto.val() ? formatValueToReal(this.ui.porcentagemDesconto.val()) : 0;

        if (nu_porcentagemdesconto > parseFloat(this.nu_descontomaximo)) {
            $.pnotify({
                title: 'Atenção!',
                text: 'O valor de desconto para este produto excede o valor máximo permitido.',
                type: 'warning'
            });
            this.ui.valorDesconto.val(this.model.get('nu_descontovalor'));
            this.ui.porcentagemDesconto.val(this.model.get('nu_descontoporcentagem'));
            return false;
        }


        //Campanha
        var id_campanha = this.ui.idCampanha.val();

        //calcula o desconto
        var valordesconto = nu_valor * (nu_porcentagemdesconto / 100);
        var nu_valorvenda = nu_valor - valordesconto;


        //valida se o valor do desconto é maior que o valor do produto
        //if (((parseFloat(this.nu_descontomaximo) / 100) * nu_valor ) > nu_valor) {
        //    $.pnotify({
        //        title: 'Atenção!',
        //        text: 'O valor de desconto é maior que o valor do produto.',
        //        type: 'warning'
        //    });
        //    this.ui.valorDesconto.val(this.model.get('nu_descontovalor'));
        //    this.ui.porcentagemDesconto.val(this.model.get('nu_descontoporcentagem'));
        //    return false;
        //}


        //Seta os valores na model
        this.model.set('nu_valorliquido', nu_valorvenda.toFixed(2));//seta o desconto no model
        this.model.set('nu_descontoporcentagem', nu_porcentagemdesconto.toFixed(2));
        this.model.set('nu_descontovalor', valordesconto.toFixed(2));

        this.render();//renderiza novamente

        //monta o select de campanha
        this.montaSelectCampanha(id_campanha);


        //escreve os valores nos inputs
        this.ui.porcentagemDesconto.removeAttr('disabled').val(formatValueToShow(nu_porcentagemdesconto.toFixed(2)));
        this.ui.valorDesconto.val(formatValueToShow(valordesconto.toFixed(2)));
        calculaValorBruto();

    },
    montaSelectCampanha: function (id_campanha) {
        //monta a string com os options do select
        var optStr = '';
        $.each(this.selectDescontosOptions, function (i, obj) {
            optStr += '<option value="' + obj.value + '">' + obj.text + '</option>';
        });
        //monta o select
        this.ui.idCampanha.empty().html(optStr).val(id_campanha);
    },
    removerProduto: function () {
        var that = this;
        bootbox.confirm("Tem certeza de que deseja remover este registro?", function (result) {
            if (result) {
                if (G2S.editModel && !that.model.get('novo')) {
                    var produto = that.model.get('st_produto');
                    $.ajax({
                        dataType: 'json',
                        type: 'post',
                        url: '/venda/remove-produto',
                        async: false,
                        data: {
                            id_vendaproduto: that.model.get('id_vendaproduto')
                        },
                        beforeSend: function () {
                            loading();
                        },
                        success: function (data) {
                            collectionProdutoVenda.remove(that.model);
                            that.model.destroy({});
                            that.remove();
                        },
                        complete: function () {
                            loaded();
                        },
                        error: function () {
                            $.pnotify({
                                title: 'Erro!',
                                text: 'Erro ao remover produto.',
                                type: 'error'
                            });
                        }
                    });
                } else {
                    //remove o modelo da collection
                    collectionProdutoVenda.remove(that.model);
                    that.model.destroy({});
                    that.remove();
                }
                $.pnotify({title: 'Sucesso!', text: 'Removido com sucesso!', type: 'success'});
                //calcula o valor bruto
                calculaValorBruto();
                if (vendaModel.get('id_venda')) {
                    var array = ({
                        id_venda: vendaModel.get('id_venda'),
                        st_texto: 'Removendo Produto: ' + produto,
                        tipo: 19
                    });
                    alteracoesdeVenda.push(array);
                }
            }
        });

    },
    toogleDescontos: function (campanha) {
        var id_campanha = typeof (campanha) != 'object' && campanha ? campanha : this.ui.idCampanha.val();
        if (id_campanha) {
            //pesquisa na sp
            var descontos = spCampanhaVendaProdutoCollection.findWhere({id_campanhacomercial: parseInt(id_campanha)});
            if (descontos) {
                this.nu_descontomaximo = descontos.get('nu_descontaximo');
                this.id_tipodesconto = descontos.get('id_tipodesconto');
            }

            if (this.id_tipodesconto) {
                if (this.id_tipodesconto == 1) {
                    this.ui.porcentagemDesconto.val('').attr('disabled', 'disabled');
                    this.ui.valorDesconto.removeAttr('disabled');
                } else if (this.id_tipodesconto == 2) {
                    this.ui.valorDesconto.val('').attr('disabled', 'disabled');
                    this.ui.porcentagemDesconto.removeAttr('disabled');
                } else {
                    this.ui.porcentagemDesconto.val('').attr('disabled', 'disabled');
                    this.ui.valorDesconto.val('').attr('disabled', 'disabled');
                }

            }


            this.model.set('id_campanhacomercial', id_campanha);
            //this.ui.porcentagemDesconto.removeAttr('disabled');
            //this.ui.valorDesconto.removeAttr('disabled');
        } else {
            this.ui.porcentagemDesconto.val('');
            this.ui.valorDesconto.val('');
            this.calculaValorVenda();
            this.ui.porcentagemDesconto.attr('disabled', 'disabled');
            this.ui.valorDesconto.attr('disabled', 'disabled');
        }

        var that = this;
        this.ui.idCampanha.find('option').each(function (i, obj) {
            that.selectDescontosOptions[i] = {'value': obj.value, 'text': obj.text};
        });
    },
    events: {
        'click button.btn-danger': 'removerProduto',
        'change select[name="campanha"]': 'toogleDescontos',
        'blur input[name="desconto_percent"]': 'calculaValorPorcentagem',
        'blur input[name="desconto_valor"]': 'calculaValorVenda',
        'change [name="id_turma"]': 'updateTurma',
        'change [name="id_tiposelecao"]': 'updateTiposelecao'
    },
    updateTurma: function (e) {
        var value = $(e.target).val();
        this.model.set('id_turma', (value || null));
    },
    updateTiposelecao: function (e) {
        var value = $(e.target).val();
        this.model.set('id_tiposelecao', (value || null));
    }
});

/**
 * EmptyView para Composite de produtos
 * @type {ItemView}
 */
var EmptyView = Marionette.ItemView.extend({
    template: '#template-lista-vazia',
    tagName: 'tr'
});


/**
 * CompositeView para renderizar produtos da venda
 * @type {CompositeView}
 */
var ViewListaProdutos = Marionette.CompositeView.extend({
    template: '#template-lista-produtos',
    tagName: 'fieldset',
    className: 'view-produtos',
    childViewContainer: '#lista-produtos-venda',
    childView: ViewItemProdutos,
    emptyView: EmptyView,
    collectionEvents: {
        'add': function (model) {
            this.toggleContratoRegra();
            if (this.collection.length == 1) {
                //habilita os campos de forma de pagamento e atendente somente quando adicionar o primeiro, porque depois ele não precisa mais habilitar
                $('select[name="id_formapagamento"], button#btn-salvar').removeAttr('disabled');
                $("input[name='bl_usarcarta']").removeAttr('disabled');
                $("select[name='id_campanhapontualidade']").removeAttr('disabled');
                $("select[name='id_usuario']").removeAttr('disabled');
            }
        },
        'remove': function () {
            this.toggleContratoRegra();
            if (this.collection.length == 0) {
                //habilita os campos de forma de pagamento e atendente somente quando adicionar o primeiro, porque depois ele não precisa mais habilitar
                $('select[name="id_formapagamento"], button#btn-salvar').attr('disabled', 'disabled');
                $("input[name='bl_usarcarta']").attr('disabled', 'disabled');
                $("select[name='id_campanhapontualidade']").attr('disabled', 'disabled');
                $("select[name='id_usuario']").attr('disabled', 'disabled');
            }
        }
    },
    toggleContratoRegra: function () {
        var produtoProjeto = this.collection.findWhere({id_tipoproduto: 1});
        if (produtoProjeto) {
            $('select[name="id_contratoregra"]').removeAttr('disabled');
        } else {
            $('select[name="id_contratoregra"]').attr('disabled', 'disabled');
        }
    },
    initialize: function () {
        thatViewListaProdutos = this;
    },
    renderizaModalPesquisa: function () {
        var collectionPesquisa = new VwProdutoCollection();
        var viewPesquisa = new ViewPesquisaProduto({
            collection: collectionPesquisa
        });
        telaVendas.modal.show(viewPesquisa);
    },
    events: {
        'click #btn-pesquisar-produto': 'renderizaModalPesquisa'
    }
});


/**
 ******************************************************************************************
 ******************************************************************************************
 ********* PESQUISA PRODUTOS **************************************************************
 ******************************************************************************************
 ******************************************************************************************
 */

/**
 * ItemView para Itens da pesquisa de produtos
 * @type {ItemView}
 */
var ViewItemPesquisaProduto = Marionette.ItemView.extend({
    template: '#template-item-pesquisa-produto',
    tagName: 'tr',
    addProduto: function () {
        var that = this;
        //adiciona o model na collection da lista de produtos
        var produto = new VwProdutoVenda();
        produto.set({
            bl_ativo: this.model.get('bl_ativo'),
            id_modelovenda: this.model.get('id_modelovenda'),
            id_produto: this.model.get('id_produto'),
            id_tipoproduto: this.model.get('id_tipoproduto'),
            id_tipoprodutovalor: this.model.get('id_tipoprodutovalor'),
            id_tiposelecao: this.model.get('id_tiposelecao'),
            st_tiposelecao: this.model.get('st_tiposelecao'),
            nu_valorbruto: this.model.get('nu_valor'),
            nu_valorliquido: this.model.get('nu_valor'),
            st_produto: this.model.get('st_produto'),
            st_tipoproduto: this.model.get('st_tipoproduto'),
            st_tipoprodutovalor: this.model.get('st_tipoprodutovalor'),
            id_turma: this.model.get('id_turma'),
            st_turma: this.model.get('st_turma'),
            novo: true
        });
        collectionProdutoVenda.add(produto);
        //calcula o valor bruto
        calculaValorBruto();
        if (vendaModel.get('id_venda')) {
            var array = ({
                id_venda: vendaModel.get('id_venda'),
                st_texto: 'Adicionando Produto: ' + that.model.get('st_produto'),
                tipo: 18
            });
            alteracoesdeVenda.push(array);
        }

    },
    events: {
        'click .btn-success': 'addProduto'
    }
});


/**
 * CompositeView para Pesquisa de produto
 * @type {CompositeView}
 */
var ViewPesquisaProduto = Marionette.CompositeView.extend({
    template: "#template-pesquisa-produto",
    childView: ViewItemPesquisaProduto,
    childViewContainer: 'tbody',
    ui: {
        st_produto: 'input[name="st_produto"]',
        id_tipo: 'select[name="tipo"]'
    },
    onShow: function () {
        //Popula Select de tipo Produto
        this.getTipoProduto();
        this.$el.parent().modal('show');
        return this;
    },
    getTipoProduto: function () {
        var tipoProdutoView = new SelectView({
            el: this.$el.find("select[name='tipo']"),
            collection: tipoProduto,
            childViewOptions: {
                optionLabel: 'st_tipoproduto',
                optionValue: 'id_tipoproduto',
                optionSelected: null
            }
        });
        tipoProdutoView.render();
    },
    pesquisarProduto: function () {
        var params = "";//Cria variavel para armazenar os parametros

        var st_produto = this.ui.st_produto.val();//atribui o st_produto a variavel
        var id_tipoproduto = this.ui.id_tipo.val();//atribui o id_tipoproduto a variavel
        var id_situacao = SITUACAO['TB_PRODUTO']['ATIVO']; // atribui o id_situacao a variavel

        if (st_produto || id_tipoproduto) { //verifica se veio pelo menos um parametro
            params += st_produto ? "/st_produto/" + st_produto : ""; //incrementa o paramentro a variavel
            params += id_tipoproduto ? "/id_tipoproduto/" + id_tipoproduto : "";
            params += id_situacao ? "/id_situacao/" + id_situacao : "";
            this.collection.url = '/produto/pesquisar-produto' + params; //incrementa parametros na variavel para fazer um get
            this.collection.fetch({
                beforeSend: function () {
                    loading();
                },
                success: function () {
                },
                error: function (collection, response) {
                    loaded();
                    $.pnotify({
                        title: 'Erro!',
                        text: 'Houve um erro ao pesquisar produtos, tente novamente! ',
                        type: 'error'
                    });
                },
                complete: function () {
                    loaded();
                }
            });
        } else {
            $.pnotify({title: 'Atenção!', text: 'Pelo menos um parâmetro deve ser passado.', type: 'warning'});
        }
        return false;
    },
    events: {
        'submit form#form-busca-produtos': 'pesquisarProduto'
    }
});


/**
 ******************************************************************************************
 ******************************************************************************************
 ********* VENDA / FORMA PAGAMENTO ********************************************************
 ******************************************************************************************
 ******************************************************************************************
 */


/**
 * ItemView para forma de pagamento selecionado no select
 * @type {ItemView}
 */
var ViewFormaPagamentoSelected = Marionette.ItemView.extend({
    template: '#template-forma-pagamento-selected',
    className: 'row-fluid',
    modelEvents: {
        //"change:id_campanhapontualidade": function () {
        //thatViewFormaPagamento.populaSelectCampanhaPontualidade();
        //this.render();
        //},
        "change": function () {
            this.render();
        }
    },
    onRender: function () {
        return this;
    }
});

/**
 * Item view para renderizar template do container de venda e valores
 * @type {ItemView}
 */
var ViewFormasPagamento = Marionette.ItemView.extend({
    template: "#template-forma-pagamento",
    tagName: "div",
    className: "row-fluid",
    ui: {
        'idFormaPagamento': 'select[name="id_formapagamento"]',
        'idAtendente': 'select[name="id_usuario"]',
        'idContratoRegra': 'select[name="id_contratoregra"]',
        'idCampanhaPontualidade': 'select[name="id_campanhapontualidade"]',
        'blCartaCredito': 'input[name="bl_usarcarta"]',
        'btnAdd': 'button#btnSelecionaFormaPagamento'
    },
    initialize: function () {
        thatViewFormaPagamento = this;
    },
    viewFormPgtoSelected: null,
    onShow: function () {
        //instancia a view do item seleciondo
        this.viewFormPgtoSelected = new ViewFormaPagamentoSelected({
            model: modelFormaPagamentoSelected,
            el: this.$el.find('#container-forma-pagamento-selected')
        });
        this.populaSelectFormaPagamento();
        this.populaSelectAtendente();
        this.populaSelectContratoRegra();
        this.populaSelectCampanhaPontualidade();
        return this;
    },
    populaSelectCampanhaPontualidade: function () {
        var that = this;
        campanhaPontualidadeCollection = new CampanhaPontualidadeCollection();
        campanhaPontualidadeCollection.data = {
            id_tipocampanha: 3,
            id_campanhaselecionada: vendaModel.get('id_campanhapontualidade')
        };
        campanhaPontualidadeCollection.fetch({
            beforeSend: function () {
                loading();
            },
            complete: function () {
                loaded();
            },
            success: function (collection) {
                var optionSelected = null;
                var defaultModel = collection.findWhere({bl_aplicardesconto: true});

                // if (vendaModel.get('id_venda')) {
                //     optionSelected = vendaModel.get('id_campanhapontualidade');
                // } else
                if (defaultModel) {
                    optionSelected = defaultModel.get('id_campanhacomercial');
                }

                that.ui.idCampanhaPontualidade.empty();
                that.ui.idCampanhaPontualidade.html('<option value="" >Selecione</option>');
                var view = new SelectView({
                    el: that.ui.idCampanhaPontualidade,
                    collection: collection,
                    childViewOptions: {
                        optionLabel: 'st_campanhacomercial',
                        optionValue: 'id_campanhacomercial',
                        optionSelected: optionSelected
                    }
                });
                view.render();
                that.selecionaCampanhaPontualidade();
            }
        });

    },
    populaSelectContratoRegra: function () {
        var view = new SelectView({
            el: this.$el.find('select[name="id_contratoregra"]'),
            collection: contratoRegraCollection,
            childViewOptions: {
                optionLabel: 'st_contratoregra',
                optionValue: 'id_contratoregra',
                optionSelected: modelFormaPagamentoSelected.get('id_contratoregra') ? modelFormaPagamentoSelected.get('id_contratoregra') : null
            }
        });
        view.render();
    },
    populaSelectAtendente: function () {
        var that = this;
        atendentesCollection.fetch({
            success: function () {
                var atendente = new SelectView({
                    el: that.$el.find('select[name="id_usuario"]'),
                    collection: atendentesCollection,
                    childViewOptions: {
                        optionLabel: 'st_nomecompleto',
                        optionValue: 'id_usuario',
                        optionSelected: G2S.editModel ? G2S.editModel.id_atendente : G2S.loggedUser.get('id_usuario')
                    }
                });
                atendente.render();

                $("select[name='id_usuario']").trigger('change');
                that.ui.idAtendente.val(G2S.editModel ? G2S.editModel.id_atendente : G2S.loggedUser.get('id_usuario'));

                if (modelFormaPagamentoSelected.get('id_atendente')) {
                    //VerificarPerfilPermissao(23, 627, true).done(function (response) {
                    //    if (response.length == 1) {
                    //        that.ui.idAtendente.removeAttr('disabled');
                    //    } else {
                    //        that.ui.idAtendente.attr('disabled', true);
                    //    }
                    //});
                    that.selecionaAtendente();
                }
            },
            complete: function () {

            }
        });
    },
    populaSelectFormaPagamento: function () {
        var that = this;

        formaPagamento.fetch({
            success: function () {
                var formaPgtoView = new SelectView({
                    el: that.$el.find('select[name="id_formapagamento"]'),
                    collection: formaPagamento,
                    childViewOptions: {
                        optionLabel: 'st_formapagamento',
                        optionValue: 'id_formapagamento',
                        optionSelected: vendaModel.get('id_formapagamento')
                    }
                });
                formaPgtoView.render();

                if (vendaModel.get('id_formapagamento')) {
//                    that.ui.idFormaPagamento.val(vendaModel.get('id_formapagamento'));
                    that.selecionaFormaPagamento();
                }
            }
        });


    },
    selecionaFormaPagamento: function () {
        if (!collectionProdutoVenda.length) {
            $.pnotify({title: 'Atenção!', text: 'Selecione os produtos para sua venda!', type: 'warning'});
            return false;
        }

        var idFormaPagamento = parseInt(this.ui.idFormaPagamento.val());//pega o valor do select e força ele para um inteiro

        var modelFormaPagemento = formaPagamento.findWhere({id_formapagamento: idFormaPagamento}); //busca o registro na collection
        //verifica se há alguma coisa na posição 0 da collection
        //if (typeof modelFormaPagemento != 'undefined') {
        if (idFormaPagamento && modelFormaPagemento) {
            loading();
            //Busca FormaDiaVencimento
            this.getFormaDiaVencimento();
            this.getFormaMeioDivisao();
            this.getSpCampanhaVendaProduto();

            //seta os valores para a forma de pagamento selecionada
            modelFormaPagamentoSelected.set({
                id_formapagamento: modelFormaPagemento.get('id_formapagamento'),
                st_formapagamento: modelFormaPagemento.get('st_formapagamento'),
                nu_maxparcelas: modelFormaPagemento.get('nu_maxparcelas')
            });


            $.each(thatLancamento.ui, function (i, elem) {
                if (i != 'chk_all_lancamentos' && i != 'btn_excluir_lancamentos')
                    elem.attr('disabled', 'disabled');
            });

            //Verifica se a quantidade máxima de parcelas é 0 e remove a o tipo "Parcela" do lançamento.
            if (modelFormaPagamentoSelected.get('nu_maxparcelas') == 0 || modelFormaPagamentoSelected.get('nu_maxparcelas') == 1) {
                var modeloParcela = TipoLancamentoCollection.findWhere({'id': 2});
                TipoLancamentoCollection.remove(modeloParcela);
            } else {
                var tipoLancamentoCollection = new Backbone.Collection(TiposLancamentos);
                TipoLancamentoCollection.set(tipoLancamentoCollection.models);
                thatLancamento.populaSelectTipoLancamento();
            }

            //Apos selecionar a forma de pagamento, habilitar o campo meio de pagamento
            thatLancamento.ui.meioLancamento.val('').removeAttr('disabled');

            //Habilita campo de tipo de lancamento.
            //thatLancamento.ui.tipoLancamento.val('').removeAttr('disabled');
            thatLancamento.ui.nuQuantidade.val(1).attr('max', modelFormaPagamentoSelected.get('nu_maxparcelas'));
            this.renderizaFormaPagamento();

            loaded();
        } else {
            $.each(thatLancamento.ui, function (i, elem) {
                elem.attr('disabled', 'disabled');
            });

            $.each(thatViewListaProdutos.children._views, function (i, view) {
                $.each(view.ui, function (i, elem) {
                    elem.val('').attr('disabled', 'disabled');
                    if (elem.attr('name') == 'campanha') {
                        elem.empty().html('<option value="">Selecione</option>');
                    }
                });
            });

            if (formatValueToReal(vendaModel.get('nu_valorliquido')) != 0 && !modelFormaPagamentoSelected.get('id_formapagamento')) {
                //se não retornou nada na collecion mostra mensagem de alerta
                $.pnotify({title: 'Atenção!', text: 'Selecione uma Plano de Pagamento!', type: 'warning'});
                this.ui.idFormaPagamento.focus();
            }
        }
    },
    renderizaFormaPagamento: function () {
        //renderiza
        this.viewFormPgtoSelected.render();
    },
    getFormaDiaVencimento: function () {
        var collectionFormaDiaVencimento = new FormaDiaVencimentoCollection();
        collectionFormaDiaVencimento.url += '/id_formapagamento/' + this.ui.idFormaPagamento.val();
        collectionFormaDiaVencimento.fetch();

    },
    getFormaMeioDivisao: function () {
        collectionFormaVwMeioDivisao.url = '/api/vw-forma-meio-divisao/id_formapagamento/' + this.ui.idFormaPagamento.val();
        collectionFormaVwMeioDivisao.fetch();
    },
    getSpCampanhaVendaProduto: function () {
        if (collectionProdutoVenda.length) {
            var that = this;
            collectionProdutoVenda.each(function (model, i) {
                var params = {
                    bl_todasformas: 1,
                    bl_todosprodutos: 1,
                    id_tipocampanha: 2,
                    id_formapagamento: that.ui.idFormaPagamento.val(),
                    id_produto: model.get('id_produto'),
                    id_campanhaselecionada: model.get('id_campanhacomercial')
                };

                var spCampanha = new SpCampanhaVendaProdutoCollection();

                spCampanha.fetch({
                    url: "/venda/retorna-sp-campanha-venda-produto",
                    data: params,
                    reset: true,
                    beforeSend: function () {
                        $("#campanha" + model.get('id_produto')).html('<option>Carregando...</option>');
                    },
                    success: function () {
                        $("#campanha" + model.get('id_produto')).empty();
                        $("#campanha" + model.get('id_produto')).html('<option value="" selected>Selecione</option>');

                        if (spCampanha.length) {
                            var campanhaSelect = new CampanhaProdutoView({
                                collection: spCampanha,
                                el: $("#campanha" + model.get('id_produto'))
                            });
                            campanhaSelect.render();
                            if (vendaModel.get('id_evolucao') == 10 || vendaModel.get('id_evolucao') == 9) {
                                $("#campanha" + model.get('id_produto')).attr('disabled', 'disabled').val(model.get('id_campanhacomercial'));
                            } else {
                                $("#campanha" + model.get('id_produto')).removeAttr('disabled').val(model.get('id_campanhacomercial'));
                            }
                        } else {
                            $("#campanha" + model.get('id_produto')).val('').attr('disabled', 'disabled');
                            $("#desconto_percent" + model.get('id_produto')).val('').attr('disabled', 'disabled');
                            $("#desconto_valor" + model.get('id_produto')).val('').attr('disabled', 'disabled');
                            model.set('nu_descontovalor', 0);
                            model.set('nu_descontoporcentagem', 0);
                            model.set('id_campanhacomercial', null);
                        }
                    },
                    complete: function () {
                        spCampanhaVendaProdutoCollection.add(spCampanha.toJSON());
                    }
                });
            });
        }
    },
    selecionaAtendente: function () {
        var atendente = this.ui.idAtendente.val();
        if (atendente) {
            var resultAtendente = atendentesCollection.findWhere({'id_usuario': parseInt(atendente)});
            if (resultAtendente) {
                modelFormaPagamentoSelected.set({
                    id_atendente: resultAtendente.get('id_usuario'),
                    st_nomeatendente: resultAtendente.get('st_nomecompleto')
                });
                this.renderizaFormaPagamento();

                if (vendaModel.get('id_venda')) {

                    if (vendaModel.get('id_venda')) {
                        var array = ({
                            id_venda: vendaModel.get('id_venda'),
                            st_texto: 'Alterando atendente para: ' + resultAtendente.get('st_nomecompleto'),
                            id_atendente: resultAtendente.get('id_usuario'),
                            tipo: 20
                        });
                        alteracoesdeVenda.push(array);
                    }
                }
            }
        } else {
            var userModel = new Usuario();
            userModel.id = modelFormaPagamentoSelected.get('id_atendente');
            userModel.fetch({
                beforeSend: function () {
                    loading();
                },
                complete: function () {
                    loaded();
                },
                success: function () {
                    modelFormaPagamentoSelected.set({
                        id_atendente: userModel.get('id_usuario'),
                        st_nomeatendente: userModel.get('st_nomecompleto')
                    });
                }
            });
        }
    },
    selecionaCampanhaPontualidade: function () {
        var campanha = this.ui.idCampanhaPontualidade.val();
        if (campanha) {
            var resCampanha = campanhaPontualidadeCollection.findWhere({id_campanhacomercial: parseInt(campanha)});
            if (resCampanha) {
                modelFormaPagamentoSelected.set({
                    id_campanhapontualidade: resCampanha.get('id_campanhacomercial'),
                    st_campanhapontualidade: resCampanha.get('st_campanhacomercial')
                });
            }
        } else {
            modelFormaPagamentoSelected.set({
                id_campanhapontualidade: null,
                st_campanhapontualidade: null
            });
        }
        this.renderizaFormaPagamento();
    },
    selecionaContratoRegra: function () {//renderiza o contato
        var id_contratoregra = this.ui.idContratoRegra.val();
        if (id_contratoregra) {

            var contratoRegraModel = contratoRegraCollection.findWhere({id_contratoregra: parseInt(id_contratoregra)});

            modelFormaPagamentoSelected.set({
                id_contratoregra: contratoRegraModel.get('id_contratoregra'),
                st_contratoregra: contratoRegraModel.get('st_contratoregra')
            });

            if (contratoRegraModel.get('id_tiporegracontrato') && contratoRegraModel.get('id_tiporegracontrato').id_tiporegracontrato == 2) {
                $.ajax({
                    dataType: 'json',
                    type: 'post',
                    url: '/venda/verifica-vagas-freepass',
                    async: false,
                    data: {
                        id_turma: collectionProdutoVenda.models[0].get('id_turma')
                    },
                    beforeSend: function () {
                        loading();
                    },
                    success: function (data) {
                        if (data.type == 'success') {
                            if (parseInt(data.mensagem.restanteFreePass) > 0) {
                                $.pnotify({
                                    title: 'Atenção',
                                    text: 'A turma possui ' + data.mensagem.restanteFreePass + ' vaga(s) Free-Pass.',
                                    type: 'success'
                                });
                            }
                        } else {
                            $.pnotify({
                                title: 'Atenção',
                                text: 'A turma já atingiu o limite de ' + data.mensagem.totalFreePass + ' vaga(s) Free-Pass.',
                                type: 'warning'
                            });
                        }
                    },
                    complete: function () {
                        loaded();
                    },
                    error: function () {
                        $.pnotify({
                            title: 'Erro!',
                            text: 'Erro ao verificar alocação de matrículas Free Pass.',
                            type: 'error'
                        });
                    }
                });
            }

            this.renderizaFormaPagamento();
        }
    },
    mostrarTabelaCartaCredito: function () {
        var that = this;
        var bl_usarcarta = this.ui.blCartaCredito.is(':checked');
        if (bl_usarcarta) {
            var collectionCartaTabela = new VwCartaCreditoCollection();
            collectionCartaTabela.url += '/?bl_ativo=1&id_usuario=' + pessoaFixada.id_usuario;
            collectionCartaTabela.fetch({
                beforeSend: function () {
                    loading();
                },
                complete: function () {
                    loaded();
                },
                success: function () {
                    var viewCartaCredito = new CartaCreditoCompositeView({
                        collection: collectionCartaTabela,
                        el: that.$el.find('#container-tabela-cartas')
                    });
                    viewCartaCredito.render();
                }
            });

        } else {
            collectionVendaCartasCredito.reset();
            this.$el.find('#container-tabela-cartas').empty();
        }
    },

    events: {
        //'click button#btnSelecionaFormaPagamento': 'selecionaFormaPagamento',
        'change select[name="id_usuario"]': 'selecionaAtendente',
        'change select[name="id_formapagamento"]': 'selecionaFormaPagamento',
        'change select[name="id_contratoregra"]': 'selecionaContratoRegra',
        'change select[name="id_campanhapontualidade"]': 'selecionaCampanhaPontualidade',
        'change input[name="bl_usarcarta"]': 'mostrarTabelaCartaCredito'
    }
});

var HistoricoItemView = Marionette.ItemView.extend({//instanciando item view que será o cabeçalho da tabela do modal histórico
    template: '#view-historico-carta-credito',
    tagName: 'tr'
});

var HistoricoCartaCreditoEmptyItemView = Marionette.ItemView.extend({//instanciando item view que será usada no caso da collection estar vazia
    template: '#template-empty-historico',
    tagName: 'tr'
});

var HistoricoCreditoCompositeView = Marionette.CompositeView.extend({//instanciando composite view que será o 'corpo' da tabela
    template: '#modal-tabela-historico-carta-credito',
    childView: HistoricoItemView,
    emptyView: HistoricoCartaCreditoEmptyItemView,
    tagName: 'div',
    className: 'row-fluid',
    childViewContainer: 'tbody'
});


/**
 ******************************************************************************************
 ******************************************************************************************
 ********* CARTA DE CREDITO ***************************************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var CartaCreditoItemView = Marionette.ItemView.extend({
    template: '#template-item-tabela-carta-credito',
    tagName: 'tr',
    ui: {
        bl_usar: 'input[name="bl_usar"]',
        nu_valorutilizar: 'input[name="nu_valorutilizar"]',
        abrirmodalhistorico: '#modal-historico'
    },
    events: {
        'change input[name="bl_usar"]': 'toggleUsar',
        'blur input[name="nu_valorutilizar"]': 'atualizaValorDescontoCarta',
        'click .detalhes': 'abrirHistoricoCartaCredito'
    },
    initialize: function () {
        var nu_valororiginal = parseFloat(this.model.get('nu_valororiginal'));
        var nu_valordisponivel = parseFloat(this.model.get('nu_valordisponivel'));
        this.model.set('nu_valororiginal', formatValueToShow(nu_valororiginal.toFixed(2)));
        this.model.set('nu_valordisponivel', formatValueToShow(nu_valordisponivel.toFixed(2)));
    },
    onRender: function () {
        this.ui.nu_valorutilizar.maskMoney({
            precision: 2,
            decimal: ",",
            thousands: "."
        }).val(this.model.get('nu_valordisponivel'));
        this.renderizaValoresUtilizadosCarta();
        if ((vendaModel.get('id_evolucao') && vendaModel.get('id_evolucao') != 7) || this.model.get('id_situacao') != 142) {
            this.desabilitaCampos();
        }
        return this;
    },
    desabilitaCampos: function () {
        $.each(this.ui, function (i, elem) {
            elem.attr('disabled', 'disabled');
        });
    },
    renderizaValoresUtilizadosCarta: function () {
        var cartaUsada = collectionVendaCartasCredito.findWhere({
            'id_cartacredito': this.model.get('id_cartacredito')
        });
        if (cartaUsada) {
            this.ui.bl_usar.attr('checked', 'checked');
            this.ui.nu_valorutilizar.removeAttr('disabled');
            var nu_valorutilizado = parseFloat(cartaUsada.get('nu_valorutilizado'));
            this.ui.nu_valorutilizar.val(formatValueToShow(nu_valorutilizado.toFixed(2)));
            calculaValorBruto();
            //this.model.set('nu_valorutilizado', formatValueToShow(nu_valorutilizado.toFixed(2)));
        }
    },
    toggleUsar: function () {
        var usar = this.ui.bl_usar.is(':checked');
        if (usar) {
            this.ui.nu_valorutilizar.removeAttr('disabled');
            this.addValorDescontoCarta();
        } else {
            this.ui.nu_valorutilizar.attr('disabled', 'disabled');
            this.removeValorCarta();
        }
    },
    atualizaValorDescontoCarta: function () {
        var valorCarta = formatValueToReal(this.ui.nu_valorutilizar.val());
        var that = this;

        var valorDisponivel = formatValueToReal(that.model.get('nu_valordisponivel'));
        if (valorDisponivel < valorCarta) {
            $.pnotify({
                type: 'warning',
                title: 'Atençao!',
                text: 'Valor informado é maior do que o valor dispinivel.'
            });
            that.ui.nu_valorutilizar.val(that.model.get('nu_valordisponivel')).focus();
            return false;
        }

        collectionVendaCartasCredito.each(function (model) {
            if (model.get('id_cartacredito') == that.model.get('id_cartacredito')) {
                model.set('nu_valorutilizado', valorCarta);
            }
        });
        calculaValorBruto();
    },
    removeValorCarta: function () {
        var resultModel = collectionVendaCartasCredito.findWhere({
            'id_cartacredito': this.model.get('id_cartacredito')
        });
        collectionVendaCartasCredito.remove(resultModel);
        calculaValorBruto();
    },
    addValorDescontoCarta: function () {
        var valorCarta = formatValueToReal(this.ui.nu_valorutilizar.val());
        var vendaCartaModel = new VendaCartaCredito();
        vendaCartaModel.set({
            'id_cartacredito': this.model.get('id_cartacredito'),
            'id_venda': vendaModel.get('id_venda'),
            nu_valorutilizado: valorCarta
        });
        collectionVendaCartasCredito.add(vendaCartaModel);
        calculaValorBruto();
    },
    abrirHistoricoCartaCredito: function () {// função que abre o histórico da carta de credito em um modal
        var id_cartacredito = this.model.get('id_cartacredito');//instanciando id da carta de crédito
        //procura o elemento na dom onde será rederizado os dados da view do modal
        var elemContainer = thatViewFormaPagamento.$el.find('#container-carta-credito').find('#modal-historico');
        //debug(elemContainer);
        var collectionhistorico = new HistoricoCartaCreditoCollection();//instanciando variável collection
        collectionhistorico.url += '?id_cartacredito=' + id_cartacredito;//adicionando a id da carta de crédito a url da collection
        collectionhistorico.fetch({
            success: function (retorno) {
                //if (retorno.length) {
                //cria a instancia da view passando a collection
                var viewHistorico = new HistoricoCreditoCompositeView({
                    collection: collectionhistorico
                });
                //pega o elemento onde será renderizado os dados
                elemContainer.empty()//limpa o conteudo
                    .append(viewHistorico.render().$el)//atribui o conteudo da view que foi renderizada
                    .modal('show');//da um show no modal (mostra modal)
                // } else {
                //    elemContainer.empty()
                //       .modal('show');
                //}
            }
        });
    }
});

var CartaCreditoEmptyItemView = Marionette.ItemView.extend({
    template: '#template-empty-carta-credito',
    tagName: 'tr'
});

var CartaCreditoCompositeView = Marionette.CompositeView.extend({
    template: '#template-tabela-carta-credito',
    childView: CartaCreditoItemView,
    emptyView: CartaCreditoEmptyItemView,
    tagName: 'div',
    className: 'row-fluid',
    childViewContainer: 'tbody',
    initialize: function () {
        thatCartaCredito = this;
    },
    onShow: function () {

        return this;
    }
});


/**
 ******************************************************************************************
 ******************************************************************************************
 ********* VALORES ************************************************************************
 ******************************************************************************************
 ******************************************************************************************
 */
/**
 * ItemView para rederizar Valores no layout
 * @type {ItemView}
 */
var ValoresItemView = Marionette.ItemView.extend({
    template: '#template-valores',
    tagName: 'fieldset'
});

/**
 ******************************************************************************************
 ******************************************************************************************
 ********* PESQUISA RESPONSÁVEL ***********************************************************
 ******************************************************************************************
 ******************************************************************************************
 */

/**
 * Item para Pesquisa de responsável
 * @type {CompositeView}
 */
var PesquisaResponvelItemView = Marionette.ItemView.extend({
    template: '#template-modal-busca-responsavel',
    tagName: 'div',
    componente: componenteAutoComplete,
    ui: {
        tipoBuscaFisica: "input#tipo_pessoa_fisica",
        tipoBuscaJuridica: "input#tipo_pessoa_juridica",
        txtSearch: 'input#search-backbone'
    },
    onShow: function () {
        //Popula Select de tipo Produto
        this.$el.parent().modal('show');
        this.selecionaTipoPesquisa();

        return this;
    },
    renderizaAutoComplete: function (modelo, url, elementTemplateListaComponente) {
        this.componente.main({
            model: modelo,
            element: this.$el.find('#container-autocomplete-responsavel'),
            localStorageName: 'responsavelAutocomplete',
            elementTemplateComponente: '#template-autocomplete-responsavel',
            elementTemplateListaComponente: elementTemplateListaComponente, //'#template-autocomplete-item-responsavel',
            containerItens: '#autocomplete-responsavel-itens',
            tagNameItem: 'li',
            urlApi: url
        });
    },
    selecionaTipoPesquisa: function () {
        this.$el.find('#container-autocomplete-responsavel').empty();
        if (this.ui.tipoBuscaFisica.is(':checked')) {
            this.$el.find('#container-autocomplete-responsavel').find('input#search-backbone').attr('placeholder', 'Pesquisar Pessoa Física');
            this.renderizaAutoComplete(VwPesquisarPessoa, '/api/pessoa', '#template-autocomplete-item-responsavel');
        } else if (this.ui.tipoBuscaJuridica.is(':checked')) {
            this.$el.find('#container-autocomplete-responsavel').find('input#search-backbone').attr('placeholder', 'Pesquisar Pessoa Jurídica');
            this.renderizaAutoComplete(Entidade, '/entidade/retorna-autocomplete-entidade', '#template-autocomplete-item-responsavel-juridico');
        }
    },
    addPessoa: function (e) {
        $(e.currentTarget).parent().parent().remove();
        var pessoa = this.componente.modelRender;

        if (this.ui.tipoBuscaFisica.is(':checked')) {
            var modeloResponsavel = new ResponsavelFinanceiroModel({
                id: pessoa.id,
                st_nomecompleto: pessoa.st_nomecompleto,
                st_cpf: pessoa.st_cpf
            });
        } else {
            var modeloResponsavel = new ResponsavelFinanceiroModel({
                id: pessoa.id,
                st_nomecompleto: pessoa.st_nomeentidade,
                st_cnpj: pessoa.st_cnpj,
                st_tiporesponsavel: 'Juridica'
            });
        }
        collectionResponsavelFinanceiro.add(modeloResponsavel);
    },
    events: {
        'click input[name="tipo_pessoa"]': 'selecionaTipoPesquisa',
        'click .item-autocomplete a.btn-success': 'addPessoa'
    }
});


/**
 ******************************************************************************************
 ******************************************************************************************
 ********* LANCAMENTOS ********************************************************************
 ******************************************************************************************
 ******************************************************************************************
 */


/**
 * View para Modal de pagamento com cartão
 */
var ViewModalCartao = Marionette.ItemView.extend({
    template: '#template-pagar-cartao',
    bandeiraSelected: null,
    lancamentosParcela: [],
    ui: {
        'btnSalvar': '.btn-success',
        'autorizacao': '#autorizacao',
        'documento': '#documento',
        'dt_prevquitado': '#dt_prevquitado',
        'digitos': '#digitos',
        'gridRecebimentoModal': '#grid-modal-content',
        'totalEntrada': '.total-entrada',
        'totalParcelas': '.total-parcelas',
        'totalTransacao': '.total-transacao'
    },
    onBeforeRender: function () {
        this.model.set({
            totalEntrada: 0,
            totalParcelas: 0
        });
    },
    onRender: function () {
        this.populaGridModal();
    },
    populaGridModal: function () {
        var that = this;

        // ITEM VIEW
        var TableView = Marionette.ItemView.extend({
            model: new VwVendaLancamento(),
            template: '#view-confimar-recebimento',
            tagName: 'tr'
        });

        // EMPTY ITEM VIEW
        var TableEmpty = Marionette.ItemView.extend({
            template: _.template('<td colspan="4">Nenhum registro encontrado.</td>'),
            tagName: 'tr'
        });

        var collectionUrlFiltro = new VwVendaLancamentoCollection();
        collectionUrlFiltro.url = '/api/vw-venda-lancamento/?id_venda=' + vendaModel.get('id_venda') + '&nu_cartao=' + (this.model.get('nu_cartao') ? this.model.get('nu_cartao') : 1);


        // COMPOSITE VIEW
        var GridModalComposite = Marionette.CompositeView.extend({
            template: '#grid-confirmar-recebimento',
            collection: collectionUrlFiltro,
            childView: TableView,
            emptyView: TableEmpty,
            childViewContainer: 'tbody',
            onRender: function () {
                this.collection.fetch({
                    reset: true,
                    success: function (collection) {
                        collection = collection.toJSON();
                        for (var i in collection) {
                            var obj = collection[i];
                            if (obj.bl_entrada) {
                                that.model.attributes.totalEntrada += parseFloat(obj.nu_valor);
                            } else {
                                that.model.attributes.totalParcelas += Number(obj.nu_valor);
                                that.lancamentosParcela.push(obj);
                            }
                            that.ui.totalEntrada.html(that.model.get('totalEntrada').toFixed(2));
                            that.ui.totalParcelas.html(that.model.get('totalParcelas').toFixed(2));
                            that.ui.totalTransacao.html((that.model.get('totalEntrada') + that.model.get('totalParcelas')).toFixed(2));
                        }
                    },
                    complete: loaded
                });
            }
        });


        //collectionUrlFiltro.fetch({
        //    beforeSend: function () {
        //        loading();
        //    },
        //    complete: function () {
        //        loaded();
        //    },
        //    success: function () {
        //        var grid = new GridModalComposite({
        //            collection: collectionUrlFiltro,
        //            el: that.ui.gridRecebimentoModal
        //        });
        //        grid.render();
        //    }
        //});


        this.ui.gridRecebimentoModal.html(new GridModalComposite().render().$el);
    },
    onShow: function () {
        this.$el.parent().modal('show');
        this.populaBandeiras();
        return this;
    },
    populaBandeiras: function () {
        var that = this;
        var BandeiraItemView = Marionette.ItemView.extend({
            template: '#template-bandeiras',
            tagName: 'label',
            className: 'radio inline',
            setBandeira: function (e) {
                var elem = $(e.currentTarget);
                if (elem.is(':checked')) {
                    that.bandeiraSelected = this.model;
                }
            },
            onRender: function () {
                this.$el.attr('title', this.model.get('st_cartaobandeira'));
                this.$el.find('img').attr('src', '/img/pagamento/icones/' + this.model.get('st_cartaobandeira').toLowerCase() + '.png');
            },
            events: {
                'click input[type="radio"]': 'setBandeira'
            }
        });

        var BandeirasCollectionView = Marionette.CollectionView.extend({
            childView: BandeiraItemView
        });


        var collectionBandeiras = new VwEntidadeCartaoCollection();
        collectionBandeiras.fetch({
            data: {id_sistema: 7},
            beforeSend: function () {
                loading();
            },
            complete: function () {
                loaded();
            },
            success: function (collection) {
                if (collection.length) {
                    var view = new BandeirasCollectionView({
                        collection: collectionBandeiras,
                        el: that.$el.find('#container-bandeiras')
                    });
                    view.render();
                    that.ui.btnSalvar.removeAttr('disabled');
                } else {
                    $.pnotify({
                        type: 'warning',
                        title: 'Atenção!',
                        text: 'Nenhuma bandeira de cartão encontrada.'
                    });
                    that.ui.btnSalvar.attr('disabled', 'disabled');
                }

            }
        });
    },
    destroyModel: function () {
        this.model.clear();
    },
    salvarPagamento: function (e) {
        e.preventDefault();


        if (this.validaForm()) {
            var autorizacao = this.ui.autorizacao.val();
            var st_coddocumento = this.ui.documento.val();
            var dt_prevquitado = this.ui.dt_prevquitado.val();
            var id_cartaoconfig = this.bandeiraSelected.get('id_cartaoconfig');
            var digitos = this.ui.digitos.val();
            this.model.set({
                'dt_prevquitado': dt_prevquitado,
                'st_coddocumento': st_coddocumento,
                'id_cartaoconfig': id_cartaoconfig,
                'bl_quitado': true,
                st_autorizacao: autorizacao,
                digitos: digitos
            });
            var lancamentos = [];
            lancamentos[0] = this.model.toJSON();

            var lancamentoCollection = collectionVwVendaLancamento.findWhere({id_lancamento: this.model.get('id_lancamento')});

            //seta os valores nos demais lançamentos do cartao
            $.each(this.lancamentosParcela, function (i, obj) {
                obj.st_coddocumento = st_coddocumento;
                obj.id_cartaoconfig = id_cartaoconfig;
                obj.bl_quitado = false;
                obj.st_autorizacao = autorizacao;
                obj.digitos = digitos;
            });

            var that = this;
            $.ajax({
                dataType: 'json',
                type: 'post',
                url: '/recebimento/pagar-cartao-debito',
                data: {
                    'lancamentos': JSON.stringify(lancamentos),
                    'parcelas': JSON.stringify(this.lancamentosParcela)
                },
                beforeSend: function () {
                    loading();
                },
                success: function (data) {
                    if (data.type == 'success') {
                        lancamentoCollection.set({
                            bl_quitado: true,
                            nu_quitado: that.model.get('nu_quitado')
                        });

                        $("#btn-confirmar").removeAttr('disabled');

                    }
                    $.pnotify({
                        title: data.title,
                        text: data.text,
                        type: data.type
                    });
                    that.$el.parent().modal('hide');
                },
                complete: function () {
                    loaded();
                },
                error: function () {
                    $.pnotify({
                        title: 'Erro! 2716',
                        text: 'Erro ao tentar salvar as confirmações de recebimento.',
                        type: 'error'
                    });
                }
            });

        }

    },
    validaForm: function () {
        var autorizacao = this.ui.autorizacao.val();
        var documento = this.ui.documento.val();
        var dt_prevquitado = this.ui.dt_prevquitado.val();
        var digitos = this.ui.digitos.val();

        var valid = true;
        if (!this.bandeiraSelected) {
            $.pnotify({
                title: 'Atenção!',
                text: 'Selecione a bandeira do cartão.',
                type: 'warning'
            });
            valid = false;
        }
        if (!autorizacao) {
            $.pnotify({
                title: 'Atenção!',
                text: 'Informe o Número da Autorização.',
                type: 'warning'
            });
            valid = false;
        }
        if (!documento) {
            $.pnotify({
                title: 'Atenção!',
                text: 'Informe o Número da Documento.',
                type: 'warning'
            });
            valid = false;
        }
        if (!dt_prevquitado) {
            $.pnotify({
                title: 'Atenção!',
                text: 'Informe a Data da Venda.',
                type: 'warning'
            });
            valid = false;
        }

        if (!digitos) {
            $.pnotify({
                title: 'Atenção!',
                text: 'Informe os 4 últimos dígitos do cartão.',
                type: 'warning'
            });
            valid = false;
        }

        return valid;

    },
    events: {
        'click .btn-cancelar': 'destroyModel',
        'submit form[name="formPagarCheque"]': 'salvarPagamento'
    }
});

/**
 * View para modal de pagamento em cheque
 */
var ViewModalCheque = Marionette.ItemView.extend({
    template: '#template-pagar-cheque',
    ui: {
        dt_quitado: 'input[name="dt_quitado"]',
        st_numcheque: 'input[name="st_numcheque"]'
    },
    onShow: function () {
        this.$el.parent().modal('show');
        return this;
    },
    destroyModel: function () {
        this.model.clear();
    },
    salvarPagamento: function (e) {
        e.preventDefault();
        var dt_quitado = this.ui.dt_quitado.val();
        var st_numcheque = this.ui.st_numcheque.val();

        if (st_numcheque == "") {
            $.pnotify({
                title: 'Atenção!',
                text: 'Informe o número do cheque.',
                type: 'warning'
            });
            return false;
        }
        if (dt_quitado == "") {
            $.pnotify({
                title: 'Atenção!',
                text: 'Informe a data do cheque.',
                type: 'warning'
            });
            return false;
        }

        this.model.set({
            dt_quitado: dt_quitado,
            st_numcheque: st_numcheque
        });

        var lancamentoCollection = collectionVwVendaLancamento.findWhere({id_lancamento: this.model.get('id_lancamento')});
        var that = this;
        $.ajax({
            dataType: 'json',
            type: 'post',
            url: '/recebimento/pagar-cheque',
            data: this.model.toJSON(),
            beforeSend: function () {
                loading();
            },
            success: function (data) {
                if (data.type == 'success') {
                    lancamentoCollection.set({
                        bl_quitado: true,
                        nu_quitado: that.model.get('nu_quitado')
                    });
                    $("#btn-confirmar").removeAttr('disabled');
                }
                $.pnotify({
                    title: data.title,
                    text: data.text,
                    type: data.type
                });
                that.$el.parent().modal('hide');
            },
            complete: function () {
                loaded();
            },
            error: function (response) {
                $.pnotify({
                    title: 'Erro! 2858',
                    text: 'Erro ao tentar salvar as confirmações de recebimento. ' + response.responseJSON.text,
                    type: 'error'
                });
            }
        });
    },
    events: {
        'click .btn-cancelar': 'destroyModel',
        'submit form[name="formPagarCheque"]': 'salvarPagamento'
    }
});


/**
 * View para lista de lançamentos vazio
 * @type {ItemView}
 */
var LancamentosEmptyView = Marionette.ItemView.extend({
    template: "#template-empty-lancamento",
    tagName: 'tr'
});


/**
 * ItemView para Itens de Lançamentos
 * @type {ItemView}
 */
var ViewItemLamcamento = Marionette.ItemView.extend({
    template: "#template-item-lancamento",
    tagName: 'tr',
    ui: {
        'dt_vencimento': 'input[name="dt_vencimento"]',
        'nu_valor': 'input[name="nu_valor"]',
        'id_responsavel': 'select[name="id_responsavel"]',
        'btn_datepicker': 'a[name="btn_datepicker"]',
        'btn_confirmar': '.btn-confirmar',
        'td_pagamento': '.td-pagamento',
        'btn_remover': '#btn_remover_lancamento_unico',
        'chk_um_lancamento': '.chk_um_lancamento',
        'btn_gerar_boleto': '#btn-gerar-boleto',
        'div_info_boleto_registrado': '#div-info-boleto-registrado',
        'btn_enviar_boleto': '#btn-enviar-boleto',
        'btn_visualizar_boleto': '#btn-visualizar-boleto',
    },
    modelEvents: {
        'change:bl_quitado': function () {
            this.render();
        }
    },

    onBeforeRender: function () {
        var nuOrdem = this.model.collection.models.indexOf(this.model) + 1;
        if (nuOrdem <= 0) {
            nuOrdem = 1;
        }
        this.model.set('nu_ordem', nuOrdem);
    },
    initialize: function () {
        var tipo = 2;
        if (this.model.get('bl_entrada')) {
            tipo = 1
        }
        //enviarBoleto
        var tipoLancamento = TipoLancamentoCollection.where({id: tipo});
        if (typeof (tipoLancamento[0]) != 'undefined') {
            this.model.set('st_tipolancamento', tipoLancamento[0].get('name'));
        }

        var ordem = this.model.get('nu_ordem') ? parseInt(this.model.get('nu_ordem')) : 1;
        this.model.set('nu_ordem', ordem);

        this.model.set('dt_baixa', null);

        if (this.model.get('dt_quitado')) {
            var dt_baixa = moment(this.model.get('dt_quitado').date, 'YYYY-MM-DD');
            this.model.set('dt_baixa', dt_baixa.format('DD/MM/YYYY'))
        }

        //var valor = formatValueToShow(this.model.get('nu_valor'));
        //this.model.set('nu_valor', valor);
        return this;
    },
    onRender: function () {

        if (this.model.get('st_urlpagamento') && this.model.get('id_meiopagamento') != MEIO_PAGAMENTO.BOLETO_BANCARIO) {
            this.ui.td_pagamento.append($('<a href="' + this.model.get('st_urlpagamento') + '" target="_blank" class="btn btn-default" >' + this.model.get('st_meiopagamento') + '</a>'));
        }

        var valor = formatValueToShow(this.model.get('nu_valor'));
        this.ui.nu_valor.maskMoney({
            precision: 2,
            decimal: ",",
            thousands: "."
        }).val(valor).attr('placeholder', this.model.get('nu_valor'));


        if (vendaModel.get('id_evolucao') != EVOLUCAO.TB_VENDA.CONFIRMADA && this.model.get('bl_quitado')) {
            $('#btn-confirmar').removeAttr('disabled');
        }
        //valida se o máximo de parcela é igual a 1 ou 0 e desabilita o campo de valor
        var nu_maxparcela = parseInt(modelFormaPagamentoSelected.get('nu_maxparcelas'));
        if (nu_maxparcela == 1 || nu_maxparcela == 0) {
            this.ui.nu_valor.attr('disabled', 'disabled');
        }
        this.desabilitaCampos();

        // Se o primeiro item da collection com o mesmo numero do cartao for este mesmo itemview, mostrar botao remover.
        // Os comentarios abaixo são devido a mudança na tela, para remover os botões de excluir lançamentos
        var mesmoCartao = this.model.collection.findWhere({nu_cartao: this.model.get('nu_cartao')});
        if (mesmoCartao == this.model || this.model.get('id_meiopagamento') != 1) {
            //this.ui.btn_remover.removeClass('hide');
            this.ui.chk_um_lancamento.removeClass('hide');
        }
        else {
            this.ui.chk_um_lancamento.addClass('hide');
            //this.ui.btn_remover.addClass('hide');
        }

        var now = moment();
        var dt_vencimento_ = moment(this.model.get('dt_vencimento'), 'DD/MM/YYYY');

        if (
            vendaModel.get('id_evolucao')
            && vendaModel.get('id_evolucao') != EVOLUCAO.TB_VENDA.AGUARDANDO_NEGOCIACAO
            && this.model.get('id_meiopagamento') == MEIO_PAGAMENTO.BOLETO_BANCARIO
            && !this.model.get('st_transacaoexterna')
            && !this.model.get('st_urlboleto')
            && !this.model.get('bl_quitado')
        ) {
            this.ui.btn_gerar_boleto.removeClass('hide');
            this.ui.btn_gerar_boleto.removeAttr('disabled');
            this.ui.btn_confirmar.addClass('hide');
        } else if (
            this.model.get('id_meiopagamento') == MEIO_PAGAMENTO.BOLETO_BANCARIO
            && this.model.get('st_transacaoexterna') // && this.model.get('st_transacaoexterna') != null
            && !this.model.get('st_urlboleto')  //&& !this.model.get('st_urlboleto') == null
        ) {
            this.ui.div_info_boleto_registrado.removeClass('hide');
            this.ui.btn_confirmar.addClass('hide');
            this.ui.btn_gerar_boleto.addClass('hide');
        } else if (this.model.get('id_meiopagamento') == MEIO_PAGAMENTO.BOLETO_BANCARIO) {
            this.ui.btn_confirmar.attr('disabled', 'disabled');
        }

        if (
            this.model.get('id_meiopagamento') == MEIO_PAGAMENTO.BOLETO_BANCARIO
            && this.model.get('st_transacaoexterna')
            && this.model.get('st_urlboleto')
            && !this.model.get('bl_quitado')
        ) {
            //aqui
            this.ui.btn_visualizar_boleto.removeClass('hide').removeAttr('disabled');
            this.ui.btn_enviar_boleto.removeClass('hide').removeAttr('disabled');
            this.ui.btn_confirmar.removeAttr('disabled');
        }

        this.ui.dt_vencimento.datepicker({
            minDate: 'today',
            format: 'dd/mm/yyyy',
            language: "pt-BR",
            autoclose: true,
            todayHighlight: true,
            clearBtn: true
        });

        this.ui.dt_vencimento.datepicker("option", "disabled", true);
        if (
            now.diff(dt_vencimento_, 'days') > 0
            && this.model.get('id_meiopagamento') == MEIO_PAGAMENTO.BOLETO_BANCARIO
            && permissaoAlterarDtVencimentoBoleto
        ) {
            this.ui.td_pagamento.html('');
            this.ui.btn_confirmar.addClass('hide');
            this.ui.btn_gerar_boleto.addClass('hide');
            this.ui.div_info_boleto_registrado.removeClass('hide');
            this.ui.div_info_boleto_registrado.html('Boleto expirado');
            this.ui.dt_vencimento.removeAttr('disabled');
            this.ui.dt_vencimento.siblings('.btn').removeAttr('disabled');//
        }

        return this;
    },
    desabilitaCampos: function () {
        var that = this;
        if (vendaModel.get('id_evolucao') == 10 || vendaModel.get('id_evolucao') == 9) {
            $.each(this.ui, function (i, elem) {
                if (i == 'btn_confirmar') {
                    if (that.model.get('bl_entrada') && !that.model.get('bl_quitado') && vendaModel.get('id_evolucao') == 9 && permissaoConfirmarRecebimento) {
                        elem.removeAttr('disabled').addClass('btn-success');
                    }

                    /* Desabilita o botão de recebimento caso exista o nu_quitado
                     * no model. Necessário para lançamentos de cartões, visto
                     * que nessa categoria o bl_quitado não é setado ao confirmar
                     * a quitação.*/

                    if (that.model.get('nu_quitado')) {
                        elem.attr('disabled', 'disabled').removeClass('btn-success');
                    }

                } else {
                    elem.attr('disabled', 'disabled');
                }
            });
        }
    },
    removeLancamento: function () {

        var that = this;
        var id_meiopagamento = that.model.get('id_meiopagamento');

        /* Mensagem a ser utilizada no Bootbox.
         Caso cartão, informa sobre remoção múltipla dos lançamentos.
         */

        var msg = "Tem certeza que deseja remover este lançamento?";
        if (id_meiopagamento == 1)
            msg = "Tem certeza que deseja remover todos os lançamentos deste cartão?";

        bootbox.confirm(msg, function (result) {
            if (result) {

                var collection = that.model.collection;

                if (id_meiopagamento == 1) {

                    var nuCartaoAtual = Number(that.model.get('nu_cartao'));
                    var toRemove = [];

                    /* Ao remover uma serie de lançamentos (num mesmo cartao),
                     atualiza os models e as views, decrementando o número
                     do cartao.
                     */

                    collection.each(function (model) {
                        var nuCartao = Number(model.get('nu_cartao'));
                        if (nuCartao == nuCartaoAtual) {
                            model.set('bl_ativo', false);

                            // Lancamentos que terao o bl_ativo setado como false no banco
                            if (model.get('id_lancamento'))
                                lancamentosParaRemover.push(model.get('id_lancamento'));

                            // Lancamentos que serao removidos da collection se nao tiver id
                            toRemove.push(model);

                        } else if (nuCartao > nuCartaoAtual) {
                            model.set('nu_cartao', nuCartao - 1);
                        }
                    });

                    var firstModel = collection.findWhere({bl_ativo: true});
                    if (firstModel)
                        firstModel.set('bl_entrada', 1);
                    collection.remove(toRemove);

                } else {
                    that.model.set('bl_ativo', false);
                    if (that.model.get('id_lancamento'))
                        lancamentosParaRemover.push(that.model.get('id_lancamento'));
                    collection.remove(that.model);
                }

                $.pnotify({title: 'Sucesso!', text: 'Lançamentos removidos com sucesso!', type: 'success'});
            }
        });

    },
    changeValorLancamento: function () {
        var that = this;
        var valor = this.$el.find('input[name="nu_valor"]').val();
        this.model.set('nu_valor', valor);
        if (vendaModel.get('id_venda')) {
            var array = ({
                id_venda: vendaModel.get('id_venda'),
                st_texto: 'Alterando valor da Parcela do lançamento de: ',
                st_meiopagamento: that.model.get('st_meiopagamento'),
                st_tipolancamento: that.model.get('st_tipolancamento'),
                nu_valor: valor,
                nu_cartao: that.model.get('nu_cartao') != 0 ? that.model.get('nu_cartao') : '',
                alteracaoParcela: true,
                tipo: 21
            });
            alteracoesdeVenda.push(array);
        }
        thatLancamento.calculaTotais();
    },
    changeDtVencimento: function (element) {


        var dt_vencimento = $(element.target).val();

        if (dt_vencimento && (this.model.get('dt_vencimento').substr(0, 10) == dt_vencimento.substr(0, 10))) {
            return false;
        }

        var now = moment();
        var dt_vencimento_ = moment(dt_vencimento, 'DD/MM/YYYY');

        if (now.diff(dt_vencimento_, 'days') > 0) {
            $.pnotify({
                title: 'Data de vencimento inválida',
                text: 'Data de vencimento não pode ser menor que a data corrente!',
                type: 'warning'
            });
            $(element.target).val(this.model.get('dt_vencimento'))
            return this;
        }

        this.model.set('dt_vencimento', dt_vencimento);

        if (vendaModel.get('id_evolucao') != 7 && vendaModel.get('id_venda')) {

            if (!permissaoAlterarDtVencimentoBoleto && this.model.get('id_meiopagamento') == 2) {
                $.pnotify({
                    title: 'Ops!',
                    text: 'Você não tem permissão para realizar essa ação!',
                    type: 'warning'
                });
                return this;
            }

            this.model.set('id_usuario',
                this.model.get('id_usuariocadastro') ?
                    this.model.get('id_usuariocadastro') :
                    this.model.get('id_usuariolancamento')
            );

            var _this = this;

            $.ajax({
                dataType: 'json',
                type: 'post',
                url: '/venda/salvar-lancamento',
                data: {params: this.model.toJSON()},
                beforeSend: function () {
                    loading();
                },
                success: function (data) {
                    if (verificaIdVenda()) {
                        collectionVwVendaLancamento.reset();

                        collectionVwVendaLancamento.url = '/api/vw-venda-lancamento/id_venda/' + vendaModel.get('id_venda');
                        //Faz o fetch nos lançamentos da venda
                        setTimeout(
                            collectionVwVendaLancamento.fetch({
                                async: false,
                                success: function () {

                                },
                                complete: function () {
                                    loaded();
                                }
                            }), 10000
                        );
                    }
                },
                complete: function () {
                    //loaded();
                },
                error: function (response) {
                    $.pnotify({
                        title: 'Erro! 3154',
                        text: response.responseJSON.text,
                        type: 'error'
                    });
                }
            });
        }

    },
    carregaModalConfirmarRecebimento: function () {
        if (!this.$el.find('.btn-confirmar').is(':disabled') && (this.model.get('bl_entrada') || !this.model.get('bl_quitado'))) {
            var id_meiopagamento = parseInt(this.model.get('id_meiopagamento'));
            switch (id_meiopagamento) {
                case 1://Cartão Crédito
                    this.quitarLancamentoCredito();
                    break;
                case 2://Boleto Bancário
                    this.quitarLancamentoDinheiro();
                    break;
                case 3://Dinheiro
                    this.quitarLancamentoDinheiro();
                    break;
                case 4://Cheque
                    this.quitarLancamentoCheque();
                    break;
                case 5://Empenho
                    this.quitarLancamentoDinheiro();
                    break;
                case 6://Depósito Bancário
                    this.quitarLancamentoDinheiro();
                    break;
                case 7://Cartão Débito
                    this.quitarLancamentoDebito();
                    break;
                case 8://Transferência
                    this.quitarLancamentoDinheiro();
                    break;
                case 13://Pagamento em Dinheiro
                    this.quitarLancamentoDinheiro();
                    break;
                default ://Qualquer outra forma de pagamento não implementado
                    $.pnotify({title: 'Atenção!', text: 'Não implementado!', type: 'warning'});
                    break;
            }
        }
    },
    quitarLancamentoCheque: function () {
        var model = new Lancamento();
        var data_hoje = new Date();
        data_hoje.setMonth(parseInt(data_hoje.getMonth()) + 1);
        var dt_quitado = data_hoje.getDate() + '/' + (data_hoje.getMonth() > 9 ? data_hoje.getMonth() : '0' + data_hoje.getMonth()) + '/' + data_hoje.getFullYear();
        model.set({
            'id': this.model.get('id_lancamento'),
            'id_lancamento': this.model.get('id_lancamento'),
            'nu_quitado': this.model.get('nu_valor'),
            'nu_valor': this.model.get('nu_valor'),
            'dt_quitado': dt_quitado
        });
        var viewModalCheque = new ViewModalCheque({
            model: model
        });
        telaVendas.modal.show(viewModalCheque);
    },
    quitarLancamentoCredito: function () {
        this.quitarLancamentoDebito();
    },
    quitarLancamentoDebito: function () {
        var model = new Lancamento();
        model.set({
            'id': this.model.get('id_lancamento'),
            'id_lancamento': this.model.get('id_lancamento'),
            'nu_quitado': this.model.get('nu_valor'),
            'nu_valor': this.model.get('nu_valor'),
            'dt_prevquitado': this.model.get('dt_vencimento'),
            'id_meiopagamento': this.model.get('id_meiopagamento'),
            'nu_cartao': this.model.get('nu_cartao')
        });
        var viewModalCartao = new ViewModalCartao({
            model: model
        });
        telaVendas.modal.show(viewModalCartao);

    },
    quitarLancamentoDinheiro: function () {
        var that = this;
        bootbox.confirm("Confirmar recebimento para este lançamento?", function (result) {
            if (result) {
                that.$el.find('td').find('.btn-confirmar').attr('disabled', 'disabled');
                $.ajax({
                    dataType: 'json',
                    type: 'post',
                    url: '/recebimento/pagar-dinheiro',
                    data: {
                        id_lancamento: that.model.get('id_lancamento')
                    },
                    beforeSend: function () {
                        loading();
                    },
                    success: function (data) {
                        if (data.type == 'success') {
                            that.model.set('bl_quitado', true);
                            $("#btn-confirmar").removeAttr('disabled');
                        }
                        $.pnotify({
                            title: data.title,
                            text: data.text,
                            type: data.type
                        });
                    },
                    complete: function () {
                        loaded();
                    },
                    error: function (response) {
                        $.pnotify({
                            title: 'Erro! 3154',
                            text: response.responseJSON.text,
                            type: 'error'
                        });
                    }
                });
            }
        });
    },
    marcaLancamentosParaExcluir: function () {
        var that = this;
        //verifico se o check box esta marcado, se sim, marca a modelo para ser excluida
        if (this.ui.chk_um_lancamento.is(':checked')) {
            this.model.set({excluirLancamento: true});
            //se houver na modelo o atributo nu_cartao, quer dizer que este lançamento faz parte de um cartão
            if (this.model.get('nu_cartao')) {
                //vai marcar todas as modelos que fazem parte deste cartão
                _.each(this.model.collection.models, function (model, i) {
                    if (model.get('nu_cartao') == that.model.get('nu_cartao')) {
                        model.set({excluirLancamento: true});
                    }
                })
            }
        } else {
            //caso não esteja marcado o checkbox, remove o atributo da modelo e se fizer parte de um cartão
            //remove de todas os lançamentos deste cartão
            this.model.unset('excluirLancamento');
            if (this.model.get('nu_cartao')) {
                _.each(this.model.collection.models, function (model, i) {
                    if (model.get('nu_cartao') == that.model.get('nu_cartao')) {
                        model.unset('excluirLancamento');
                    }
                })
            }
        }
    },
    gerarBoletoRegistrado: function () {
        var that = this;

        bootbox.confirm('Deseja realmente solicitar o boleto?', function (result) {
            if (result) {
                $.ajax({
                    dataType: 'json',
                    type: 'get',
                    url: '/Recebimento/gerar-boleto',
                    data: {'id_lancamento': that.model.get('id_lancamento'), 'funcao': 'getbarcode'},
                    beforeSend: function () {
                        loading();
                    },
                    success: function (data) {
                        if (data.st_urlboleto != null) {
                            setTimeout(
                                that.buscarDadosLancamentos()
                                , 10000
                            );
                        } else {

                            setTimeout(
                                that.buscarDadosLancamentos()
                                , 10000
                            );

                            $.pnotify({
                                title: 'Operação efetuada com sucesso',
                                text: 'Solicitação para geração de boleto enviada com sucesso!' +
                                '<br>Enviaremos o boleto ao e-mail quando estiver disponível.',
                                type: 'success'
                            });
                        }
                    },
                    complete: function () {
                        loaded();
                    },
                    error: function (response) {
                        $.pnotify({
                            title: 'Erro! 3154',
                            text: 'Não foi possível solicitar o boleto. Tente novamente',
                            type: 'error'
                        });
                    }
                });
            }
        });
    },
    buscarDadosLancamentos: function () {

        if (verificaIdVenda()) {
            collectionVwVendaLancamento.reset();

            collectionVwVendaLancamento.url = '/api/vw-venda-lancamento/id_venda/' + vendaModel.get('id_venda');
            //Faz o fetch nos lançamentos da venda

            collectionVwVendaLancamento.fetch({
                async: false,
                beforeSend: function () {
                    loading();
                },
                success: function () {
                    loaded();
                },
                complete: function () {
                    telaVendas.habilitaBotoes();
                    calculaValorBruto();
                    loaded();
                }
            });
        }
    },
    enviarBoletoRegistrado: function () {
        $.ajax({
            dataType: 'json',
            type: 'post',
            url: '/recebimento/enviar-boleto',
            data: {id_lancamento: this.model.get('id_lancamento')},
            beforeSend: function () {
                loading();
            },
            success: function (data) {
                $.pnotify({
                    title: data.title,
                    text: data.text,
                    type: data.type
                });
            },
            complete: function () {
                loaded();
            },
            error: function (response) {
                $.pnotify({
                    title: 'Erro! 3154',
                    text: response.responseJSON.text,
                    type: 'error'
                });
            }
        });
    },
    events: {
        'click #btn_excluir_lancamentos': 'removeLancamento',
        'blur input[name="nu_valor"]': 'changeValorLancamento',
        'change input[name="dt_vencimento"]': 'changeDtVencimento',
        'click .btn-confirmar': 'carregaModalConfirmarRecebimento',
        'change .chk_um_lancamento': 'marcaLancamentosParaExcluir',
        'click #btn-gerar-boleto': 'gerarBoletoRegistrado',
        'click #btn-enviar-boleto': 'enviarBoletoRegistrado',
        'click #btn_recarregar_lancamentos': 'buscarDadosLancamentos',
    }
});

/**
 * CompositeView para lista de lançamentos
 * @type {CompositeView}
 */
var ViewListaLancamentos = Marionette.CompositeView.extend({
    template: '#template-lista-lancamentos',
    tagName: 'fieldset',
    className: 'view-lancamentos',
    childViewContainer: '#lista-lancamentos-venda',
    childView: ViewItemLamcamento,
    emptyView: LancamentosEmptyView,
    valorVenda: 0,
    totalEntrada: 0,
    totalParcela: 0,
    totalLancamento: 0,
    valorRestante: 0,
    collectionEvents: {
        remove: function () {
            this.render();
            this.calculaTotais();
            //this.setValorVenda();
            this.ui.nuTotal.val(this.valorRestante);
            //this.ui.tipoLancamento.removeAttr('disabled');
        }
    },
    modelEvents: {
        'change': 'setValorVenda'
    },
    setValorVenda: function () {
        //thatLancamento.ui.nuQuantidade.val(1).attr('max', modelFormaPagamentoSelected.get('nu_maxparcelas'));
        this.valorVenda = this.model.get('nu_valor');
        this.valorRestante = this.model.get('nu_valor');
        this.render();
        this.adicionaMascaras();
        this.verificcaLancamentosHabilitaBotaoConfirmacarRecebimento();
        this.calculaTotais();
    },
    ui: {
        tipoLancamento: '#tipo_lancamento',
        meioLancamento: '#meio_lancamento',
        nuQuantidade: '#nu_quantidade',
        nuValor: '#nu_valorparcela',
        nuTotal: '#nu_total',
        nuPrazoParcela: '#nu_prazoparcela',
        dtPrimeiraParcela: '#dt_primeiraparcela',
        idResponsavel: '#id_responsavel',
        nuValorEntrada: '#nu_totalentrada',
        nuValorParcela: '#nu_totalparcela',
        nuValorRestante: '#nu_valorrestante',
        nuTotalLancamento: '#nu_totallancamento',
        tipoPrazo: '#tipo_prazoparcela',
        btnDtPicker: '#btn_datepicker',
        btnAddLancamento: '#btn-add-lancamento',
        btnProcurarResponsavel: '#btn-procurar-responsavel',
        chk_all_lancamentos: '#chk_all_lancamentos',
        btn_excluir_lancamentos: '#btn_excluir_lancamentos',
        btn_recarregar_lancamentos: '#btn_recarregar_lancamentos'
    },
    adicionaMascaras: function () {
        this.ui.nuValor.maskMoney({precision: 2, decimal: ",", thousands: "."}).attr('placeholder', '0,00');
        this.ui.nuTotal.maskMoney({precision: 2, decimal: ",", thousands: "."}).attr('placeholder', '0,00');
        this.ui.nuValorEntrada.maskMoney({precision: 2, decimal: ",", thousands: "."}).attr('placeholder', '0,00');
        this.ui.nuValorParcela.maskMoney({precision: 2, decimal: ",", thousands: "."}).attr('placeholder', '0,00');
        this.ui.nuValorRestante.maskMoney({precision: 2, decimal: ",", thousands: "."}).attr('placeholder', '0,00');
        this.ui.nuTotalLancamento.maskMoney({precision: 2, decimal: ",", thousands: "."}).attr('placeholder', '0,00');
    },
    initialize: function () {
        thatLancamento = this;
        return this;
    },
    onShow: function () {
        //this.adicionaMascaras();
        this.verificcaLancamentosHabilitaBotaoConfirmacarRecebimento();
        return this;
    },
    onRender: function () {
        var that = this;
        this.adicionaMascaras();
        this.populaSelectMeioPagamento();
        //this.populaSelectTipoLancamento();
        this.populaSelectResponsavel();
        this.habilitaCampos();
        //this.calculaTotais();
        return this;
    },
    selecionaTodosLancamentos: function () {
        if (this.ui.chk_all_lancamentos.is(':checked')) {
            $('.chk_um_lancamento').prop('checked', true);
            _.each(this.collection.models, function (model, i) {
                model.set({excluirLancamento: true});
            });
        } else {
            $('.chk_um_lancamento').prop('checked', false);
            _.each(this.collection.models, function (model, i) {
                model.unset('excluirLancamento');
            });
        }
    },
    verificcaLancamentosHabilitaBotaoConfirmacarRecebimento: function () {
        var temEntrada = false;
        this.collection.each(function (model, i) {
            temEntrada = model.get('bl_entrada');
        });

        //if (!temEntrada && (vendaModel.get('id_evolucao') == 9)) {
        //    $("#btn-confirmar").removeAttr('disabled');
        //}
    },
    populaSelectResponsavel: function () {

        var responsavel = new SelectView({
            el: this.ui.idResponsavel,
            collection: collectionResponsavelFinanceiro,
            childViewOptions: {
                optionLabel: 'st_nomecompleto',
                optionValue: 'id',
                optionSelected: null
            }
        });
        responsavel.render();
    },
    populaSelectTipoLancamento: function () {
        var that = this;

        if (!modelFormaPagamentoSelected.get('id_formapagamento')) {
            this.ui.tipoLancamento.html('<option value="">Selecione o Meio de Pagamento</option>');
        } else {
            //this.ui.tipoLancamento.html('<option value="">Selecione</option>');
            this.ui.tipoLancamento.html('').removeAttr('disabled');

            if (that.ui.meioLancamento.val() == 1) {
                // SE FOR CARTAO DE CREDITO, REMOVE O "ENTRADA" E DEIXA SOMENTE O "PARCELA"
                TipoLancamentoCollection.remove({'id': 1});
            } else {
                // SE NAO, REMOSTRA OS 2
                TipoLancamentoCollection = new Backbone.Collection(TiposLancamentos);
            }

            var tipoLancamento = new TipoLancamentoView({
                collection: TipoLancamentoCollection,
                el: this.ui.tipoLancamento
            });
            tipoLancamento.render();

            this.ui.tipoLancamento.find('option:first').prop('selected', true).end().trigger('change');
        }
    },
    habilitaCampos: function () {
        //valida se seleciona a forma de pagamento
        if (!modelFormaPagamentoSelected.get('id_formapagamento')) {
            $.pnotify({
                title: 'Atenção!',
                type: 'warning',
                text: 'Selecione a Plano de Pagamento.'
            });
        } else {
            //adiciona o atributo disabled nos ui
            $.each(this.ui, function (key, ui) {
                if (
                    key != 'chk_all_lancamentos' &&
                    key != 'btn_excluir_lancamentos' &&
                    key != 'meioLancamento' &&
                    key != 'tipoLancamento' &&
                    key != 'nuValorEntrada' &&
                    key != 'nuValorParcela' &&
                    key != 'nuValorRestante' &&
                    key != 'nuTotalLancamento' &&
                    key != 'btn_recarregar_lancamentos'
                )
                    ui.attr('disabled', 'disabled');
            });

            if (vendaModel.get('id_evolucao') == 10 || vendaModel.get('id_evolucao') == 9) {
                this.ui.btn_excluir_lancamentos.attr('disabled', 'disabled');
                this.ui.chk_all_lancamentos.attr('disabled', 'disabled');
            } else if (vendaModel.get('id_evolucao') == 7) {
                this.ui.btn_excluir_lancamentos.remove('disabled');
                this.ui.chk_all_lancamentos.remove('disabled');
            }


            var dtPrimeiraParcela = new Date();
            var d, m, y;
            d = parseInt(dtPrimeiraParcela.getDate());
            m = parseInt(dtPrimeiraParcela.getMonth());
            y = parseInt(dtPrimeiraParcela.getFullYear());
            dtPrimeiraParcela = ((d >= 10) ? d : "0" + d) + '/' + (((m + 1) >= 10) ? m + 1 : '0' + (m + 1)) + '/' + y;

            //se o tipo de lancamento for 2, remove o disabled de alguns campos
            if (this.ui.tipoLancamento.val() == 2) {
                $.each(this.ui, function (key, ui) {
                    if (key != 'nuTotal')
                        ui.removeAttr('disabled');
                });
                this.ui.tipoPrazo.val('meses');
                this.ui.nuPrazoParcela.val(1);
                this.ui.dtPrimeiraParcela.val(dtPrimeiraParcela);
                this.ui.idResponsavel.val(pessoaFixada.id);

            } else if (this.ui.tipoLancamento.val() == 1) {
                /*Se o tipo for 1, remove o disabled de outros campos
                 e seta valores padrão para os elementos da ui.
                 */
                this.ui.tipoPrazo.val('');
                this.ui.nuPrazoParcela.val('');
                this.ui.nuQuantidade.val(1);
                this.ui.dtPrimeiraParcela.val(dtPrimeiraParcela);
                this.ui.idResponsavel.val(pessoaFixada.id);
                this.ui.nuValor.removeAttr('disabled');
                this.ui.idResponsavel.removeAttr('disabled');
                this.ui.btnAddLancamento.removeAttr('disabled');
                this.ui.btnProcurarResponsavel.removeAttr('disabled');
            }
            this.calculaParcela();
            this.ui.btn_recarregar_lancamentos.removeAttr('disabled');
        }
    },
    populaSelectMeioPagamento: function () {
        this.ui.meioLancamento.empty();
        var view = new MeioPagamentoCollectionView({
            collection: collectionFormaVwMeioDivisao,
            el: this.ui.meioLancamento
        });
        view.render();
    },
    showModalResponsavel: function () {
        var modalPesquisa = new PesquisaResponvelItemView();
        telaVendas.modal.show(modalPesquisa);
    },
    calculaValorRestante: function () {

        //pega o valor da venda
        var valorVenda = this.valorRestante;
        var valorVenda = parseFloat(valorVenda.replace('.', '').replace(',', '.'));

        //pega o valor da parcela digitado
        var valorParcela = this.ui.nuValor.val();
        valorParcela = parseFloat(valorParcela.replace('.', '').replace(',', '.'));

        //quantidade de parcelas
        var qtdParcelas = parseInt(this.ui.nuQuantidade.val());

        //verifica se o valor da parcela é maior que o da venda
        if (valorParcela > valorVenda) {
            $.pnotify({title: 'Atenção!', text: 'Valor da parcela é maior que o valor da venda.', type: 'warning'});
            this.ui.nuValor.val('').focus();
            return false;
        } else {
            //var valorTotalLancemento = valorVenda - (valorParcela * qtdParcelas);
            var valorTotalLancemento = valorParcela * qtdParcelas;
            this.ui.nuTotal.val(formatValueToShow(valorTotalLancemento.toFixed(2)));
            return valorTotalLancemento;
        }

    },
    calculaParcela: function () {

        //pega o valor da venda
        var valorVenda = this.valorRestante;
        if (valorVenda)
            valorVenda = parseFloat(valorVenda.replace('.', '').replace(',', '.'));

        //quantidade de parcelas
        var qtdParcela = this.ui.nuQuantidade.val() ? parseInt(this.ui.nuQuantidade.val()) : 1;

        /*removendo a classe de erro caso o valor seja 0,
         já que o sistema vai colocar o valor 1 após a validação*/

        $('#divQtdLancamentos').removeClass('control-group error');

        //Valor total
        var valorParcela = 0;

        if (!this.verificaValorVenda()) {
            return false;
        }

        if (qtdParcela > modelFormaPagamentoSelected.get('nu_maxparcelas')) {
            $.pnotify({
                title: 'Atenção!',
                text: 'O número máximo de parcelas não pode ser maior que ' + modelFormaPagamentoSelected.get('nu_maxparcelas') + '.',
                type: 'warning'
            });
            return false;
        }

        if (qtdParcela) {
            //calcula
            valorParcela = valorVenda / qtdParcela;
            //coloca o valor no input
            this.ui.nuValor.val(formatValueToShow(valorParcela.toFixed(2)));
            $('#divQtdLancamentos').removeClass('control-group error');
            return valorParcela;//retorna o valor
        } else {
            $('#divQtdLancamentos').addClass('control-group error');
            $.pnotify({
                title: 'Atenção!',
                text: 'O número de parcelas não pode ser igual a 0 (zero).',
                type: 'warning'
            });
            return false;
        }
    },

    verificaValorVenda: function () {
        if (this.model.get('nu_valor')) {
            return true;
        } else {
            $.pnotify({title: 'Atenção!', text: 'Valor da venda não definido.', type: 'warning'});
            return false;
        }
    },

    adicionaLancamento: function () {
        var ui = this.ui;
        var nu_parcelas = parseInt(ui.nuQuantidade.val());
        var bl_entrada = ui.tipoLancamento.val() == 1 ? true : false;
        var nu_valor = formatValueToReal(ui.nuValor.val());
        nu_valor = parseFloat(nu_valor);
        nu_valor = nu_valor.toFixed(2);
        var dt_vencimento = ui.dtPrimeiraParcela.val() ? ui.dtPrimeiraParcela.val() : null;
        var id_tipolancamento = ui.tipoLancamento.val();
        var id_meiopagamento = ui.meioLancamento.val();
        var st_nomecompleto = ui.idResponsavel.find('option:selected').text();
        var id_usuariolancamento = ui.idResponsavel.val();
        var st_meiopagamento = ui.meioLancamento.find('option:selected').text();
        var id_formapagamento = modelFormaPagamentoSelected.get('id_formapagamento');
        var tipoPrazo = ui.tipoPrazo.val();
        var nuPrazoParcela = Number(ui.nuPrazoParcela.val());

        //Cria um objeto do tipo DATE;

        if (this.validaSave(nu_valor, id_tipolancamento, id_meiopagamento, id_usuariolancamento, dt_vencimento)) {
            var dt_object = new Date(dt_vencimento.split('/').reverse());
            //SE for PARCELA e nao for CARTÃO DE CRÉDITO
            if (id_tipolancamento == 2 && id_meiopagamento != 1) {
                //valida se foi passado o numero de parcelas e tipo prazo
                if (!nu_parcelas || !tipoPrazo) {
                    $.pnotify({
                        title: 'Atenção!',
                        text: 'Informe o Prazo entre as Parcelas.',
                        type: 'warning'
                    });
                    return false;
                }

                //valida a dt de vencimento apra a primeira parcela
                if (!dt_vencimento) {
                    $.pnotify({
                        title: 'Atenção!',
                        text: 'Informe a data para a primeira parcela.',
                        type: 'warning'
                    });
                    return false;
                }
            }

            if (nu_parcelas) {

                /* nu_cartao armazena o maior número do cartão */
                var nu_cartao = 0;
                if (id_meiopagamento == 1) {
                    $.each(this.collection.models, function (index, obj) {
                        var value = Number(obj.get('nu_cartao'));
                        if (value > nu_cartao)
                            nu_cartao = value;
                    });
                    nu_cartao++;
                } else {
                    nu_cartao = null;
                }


                for (var i = 0; i < nu_parcelas; i++) {

                    //pega a quantidade de elementos na collection
                    var nu_ordem = this.collection.length;

                    //SE FOR CARTAO, REVER VARIAVEIS
                    if (id_meiopagamento == 1) {
                        if (i == 0) {
                            // Se for o primeiro item da parcela, definir como entrada
                            id_tipolancamento = 1;
                            bl_entrada = true;
                        } else {
                            // Senao, definir como parcela
                            id_tipolancamento = 2;
                            bl_entrada = false;
                        }
                    }

                    //REAJUSTA A DATA DE VENCIMENTO
                    dt_vencimento = dt_object.toISOString().substr(0, 10).split('-').reverse().join('/');

                    //instancia o modelo
                    var vendaLancamentoModel = new VwVendaLancamento();

                    var st_tipolancamento = 'Parcela';
                    // SE FOR ENTRADA, DEFINIR A STRING COMO ENTRADA
                    if (id_tipolancamento == 1)
                        st_tipolancamento = 'Entrada';

                    //seta os valores no modelo
                    vendaLancamentoModel.set({
                        id_tipolancamento: id_tipolancamento,
                        id_meiopagamento: id_meiopagamento,
                        bl_entrada: bl_entrada,
                        nu_parcelas: nu_parcelas,
                        nu_valor: nu_valor,
                        dt_vencimento: dt_vencimento,
                        nu_ordem: ++nu_ordem,
                        st_nomecompleto: st_nomecompleto,
                        st_tipolancamento: st_tipolancamento,
                        st_meiopagamento: st_meiopagamento,
                        id_formapagamento: id_formapagamento,
                        nu_cartao: nu_cartao,
                        bl_ativo: 1
                    });

                    var responsavel = collectionResponsavelFinanceiro.where({id: parseInt(id_usuariolancamento)});
                    if (responsavel[0]) {
                        if (responsavel[0].get('st_tiporesponsavel') == 'Fisica') {
                            vendaLancamentoModel.set('id_usuariolancamento', id_usuariolancamento);
                        } else {
                            vendaLancamentoModel.set('id_entidadelancamento', id_usuariolancamento);
                        }

                    }
                    this.collection.add(vendaLancamentoModel);

                    // Calcular quantos dias sera incrementado em cada parcela
                    //Incrementa os dias na data de vencimento, para a proxima parcela
                    switch (tipoPrazo) {
                        case 'dias':
                            dt_object.setDate(dt_object.getDate() + nuPrazoParcela);
                            break;
                        case 'meses':
                            var increment = nuPrazoParcela;
                            dt_object.setMonth(dt_object.getMonth() + nuPrazoParcela);
                            break;
                        default:
                            break;
                    }
                }
            }
            this.calculaTotais();
            if (vendaModel.get('id_venda')) {
                var array = ({
                    id_venda: vendaModel.get('id_venda'),
                    st_texto: 'Adicionando lançamento de: ',
                    st_meiopagamento: st_meiopagamento,
                    nu_parcelas: nu_parcelas,
                    st_tipolancamento: st_tipolancamento,
                    nu_valor: nu_valor,
                    nu_cartao: nu_cartao,
                    alteracaoParcela: false,
                    tipo: 21
                });
                alteracoesdeVenda.push(array);
            }
        } else {
            return false;
        }
    },
    validaSave: function (nu_valor, id_tipolancamento, id_meiopagamento, id_usuariolancamento, dt_vencimento) {
        var flag = true;
        var maxParcela = parseInt(modelFormaPagamentoSelected.get('nu_maxparcelas'));
        //Valida se o numero máximo de parcelas é igual a 1 ou a 0 e se o valor do lançamento é diferente do valor da venda
        if ((maxParcela == 0 || maxParcela == 1) && parseFloat(nu_valor) != formatValueToReal(this.model.get('nu_valor'))) {
            $('#divValorLancamento').addClass('control-group error');
            $.pnotify({
                title: 'Atenção!',
                text: 'O valor do lançamento é menor do que o valor permitido.',
                type: 'warning'
            });
            flag = false;
        } else {
            $('#divValorLancamento').removeClass('control-group error');
        }

        //valida se informou o resposavel
        if (!id_usuariolancamento) {
            $('#divResponsavelLancamento').addClass('control-group error');
            flag = false;
        } else {
            $('#divResponsavelLancamento').removeClass('control-group error');
        }

        //valida se a data da primeira parcela foi preenchida
        if (!dt_vencimento) {
            $('#divDataPrimeiraParcela').addClass('control-group error');
            flag = false;
        } else {
            $('#divDataPrimeiraParcela').removeClass('control-group error');
        }

        //valida se informou o valor do lançamento
        if (!this.ui.nuValor.val()) {
            $('#divValorLancamento').addClass('control-group error');
            flag = false;
        } else {
            $('#divValorLancamento').removeClass('control-group error');
        }

        //Valida o meio de pagamento
        if (!id_meiopagamento) {
            $('#divMeioPagamento').addClass('control-group error');
            flag = false;
        } else {
            $('#divMeioPagamento').removeClass('control-group error');
        }


        //Valida o lançamento
        if (!id_tipolancamento) {
            $('#divTipoLancamento').addClass('control-group error');
            flag = false;
        } else {
            $('#divTipoLancamento').removeClass('control-group error');
        }

        if (!flag) {
            $.pnotify({
                title: 'Atenção!',
                text: 'Verifique os campos obrigatórios.',
                type: 'warning'
            });
        }
        return flag;
    },
    calculaTotais: function () {
        //cria as variaveis
        //pega o valor da venda
        var that = this;
        var valorVenda = that.valorVenda;
        valorVenda = valorVenda ? parseFloat(valorVenda.replace('.', '').replace(',', '.')) : 0;
        var totalLancamento = 0;
        var totalEntrada = 0;
        var totalParcela = 0;
        var valorRestante = 0;
        //percorre a collection
        if (this.collection.length) {
            this.collection.each(function (model, i) {
                //atribui o valor vindo da model na variavel

                var valor = formatValueToReal(model.get('nu_valor'));

                //incrementa em total de lancamentos
                totalLancamento += valor;

                //verifica se o registro atual é uma entrada e vai incrementando os respectivos lancamentos
                if (model.get('bl_entrada')) {
                    totalEntrada += valor;
                } else {
                    totalParcela += valor;
                }
            });
        }

        totalLancamento = parseFloat(totalLancamento.toFixed(2));
        //pega o total de lancamentos subtrai do total da venda para ter o restante
        valorRestante = valorVenda - totalLancamento;


        //verifica se o valor da venda é maior que o dos lancamentos
        if (valorVenda != totalLancamento) {
            this.ui.nuTotalLancamento.parent().parent().parent().removeClass('success').addClass('error');

            this.ui.meioLancamento.val('').removeAttr('disabled');
            this.ui.tipoLancamento.val('').attr('disabled', 'disabled');
            this.ui.nuQuantidade.val(1).attr('disabled', 'disabled');
            this.ui.nuValorParcela.val('').attr('disabled', 'disabled');
            this.ui.nuPrazoParcela.val('').attr('disabled', 'disabled');
            this.ui.tipoPrazo.val('').attr('disabled', 'disabled');
            this.ui.dtPrimeiraParcela.val('').attr('disabled', 'disabled');
            this.ui.idResponsavel.val('').attr('disabled', 'disabled');
            this.ui.nuValor.val('').attr('disabled', 'disabled');

        } else if (valorVenda == totalLancamento.toFixed(2)) {
            this.ui.nuTotalLancamento.parent().parent().parent().removeClass('error').addClass('success');
            this.ui.meioLancamento.val('').attr('disabled', 'disabled');
            this.ui.tipoLancamento.val('').attr('disabled', 'disabled');
            this.ui.nuQuantidade.val('').attr('disabled', 'disabled');
            this.ui.nuValor.val('').attr('disabled', 'disabled');
            this.ui.nuTotal.val(formatValueToShow(valorRestante.toFixed(2))).attr('disabled', 'disabled');
            this.ui.nuPrazoParcela.val('').attr('disabled', 'disabled');
            this.ui.tipoPrazo.val('').attr('disabled', 'disabled');
            this.ui.dtPrimeiraParcela.val('').attr('disabled', 'disabled');
            this.ui.idResponsavel.val('').attr('disabled', 'disabled');
            this.ui.btnDtPicker.attr('disabled', 'disabled');
            this.ui.btnProcurarResponsavel.attr('disabled', 'disabled');
            this.ui.btnAddLancamento.attr('disabled', 'disabled');
        } else {
            this.ui.nuTotalLancamento.parent().parent().parent()
                .removeClass('error')
                .removeClass('success');
        }

        if (totalLancamento < valorVenda) {
            //this.ui.tipoLancamento.val('').removeAttr('disabled');
        }

        if (valorRestante != 0) {
            this.ui.nuValorRestante.parent().parent().parent().removeClass('success').addClass('error');
        } else {
            this.ui.nuValorRestante.parent().parent().parent().removeClass('error').addClass('success');
        }


        that.totalLancamento = formatValueToShow(totalLancamento.toFixed(2));
        that.totalEntrada = formatValueToShow(totalEntrada.toFixed(2));
        that.totalParcela = formatValueToShow(totalParcela.toFixed(2));
        that.valorRestante = formatValueToShow(valorRestante.toFixed(2));


        //seta os valores nos inputs
        this.ui.nuValorRestante.val(that.valorRestante);
        this.ui.nuValorEntrada.val(that.totalEntrada);
        this.ui.nuValorParcela.val(that.totalParcela);
        this.ui.nuTotalLancamento.val(that.totalLancamento);
        this.ui.nuTotal.val(that.valorRestante);

    },
    excluirLancamentosSelecionados: function () {
        var that = this;
        var modelsExcluir = this.collection.where({excluirLancamento: true});
        if (modelsExcluir.length) {
            bootbox.confirm("Deseja remover os lançamentos selecionados?", function (result) {
                if (result) {
                    if (vendaModel.get('id_venda')) {
                        var array = null;
                        modelsExcluir.forEach(function (valor) {
                            array = ({
                                id_venda: vendaModel.get('id_venda'),
                                st_texto: 'Excluindo lançamento de: ',
                                st_meiopagamento: valor.get('st_meiopagamento'),
                                nu_parcelas: valor.get('nu_parcelas'),
                                st_tipolancamento: valor.get('st_tipolancamento'),
                                nu_valor: valor.get('nu_valor'),
                                nu_cartao: valor.get('nu_cartao') ? valor.get('nu_cartao') : '',
                                alteracaoParcela: false,
                                tipo: 21
                            });
                        });
                        alteracoesdeVenda.push(array);
                    }

                    that.collection.remove(modelsExcluir);
                    $.pnotify({title: 'Sucesso!', text: 'Lançamento(s) removido(s) com sucesso!', type: 'success'});
                }
            });
        } else {
            $.pnotify({title: 'Alerta!', text: 'Nenhum lançamento selecionado!', type: 'alert'});
        }

    },
    carregarDadosLancamento: function () {

        if (verificaIdVenda()) {
            collectionVwVendaLancamento.reset();

            collectionVwVendaLancamento.url = '/api/vw-venda-lancamento/id_venda/' + vendaModel.get('id_venda');
            loading();
            //Faz o fetch nos lançamentos da venda
            collectionVwVendaLancamento.fetch({
                async: false,
                beforeSend: function () {
                    loading();
                },
                success: function () {
                    $.pnotify({title: 'Sucesso!', text: 'Lançamento(s) atualizado(s) com sucesso!', type: 'success'});
                },
                complete: function () {
                    telaVendas.habilitaBotoes();
                    loaded();
                }
            });
        }
    },
    events: {
        'change select[name="tipo_lancamento"]': 'habilitaCampos',
        'click #btn-procurar-responsavel': 'showModalResponsavel',
        'change input#nu_quantidade': 'calculaParcela',
        'blur input#nu_valorparcela': 'calculaValorRestante',
        'click #btn-add-lancamento': 'adicionaLancamento',
        'change select[name="meio_lancamento"]': 'populaSelectTipoLancamento',
        'click input#chk_all_lancamentos': 'selecionaTodosLancamentos',
        'click #btn_excluir_lancamentos': 'excluirLancamentosSelecionados',
        'click #btn_recarregar_lancamentos': 'carregarDadosLancamento'
    }
});

/**
 ******************************************************************************************
 ******************************************************************************************
 ********* TRAMITES ***********************************************************************
 ******************************************************************************************
 ******************************************************************************************
 */

/**
 * ItemView para tramites
 * @type @exp;Marionette@pro;ItemView@call;extend
 */
var ViewItemTramite = Marionette.ItemView.extend({
    template: '#template-item-tramite',
    tagName: 'tr',
    onRender: function () {
        return this;
    }
});

/**
 * ItemView para tramite vazia
 * @type @exp;Marionette@pro;ItemView@call;extend
 */
var TramiteEmptyView = Marionette.ItemView.extend({
    template: '#template-lista-vazia-tramite',
    tagName: 'tr'
});

/**
 * CollectionView para tramite
 * @type @exp;Marionette@pro;CompositeView@call;extend
 */
var TramitesCompositeView = Marionette.CompositeView.extend({
    template: '#template-tramites',
    tagName: 'fieldset',
    className: 'view-tramites',
    childViewContainer: '#lista-tramites',
    childView: ViewItemTramite,
    emptyView: TramiteEmptyView,
    ui: {
        'btnCancelar': '#btn-cancelar-tramite',
        'btnAdd': '#btn-add-tramite',
        'btnSalvar': '#btn-salvar-tramite',
        'txtTramite': '#st_tramite',
        'boxForm': '#container-form-add-tramite'
    },
    onShow: function () {
        //verifica se o id_venda esta definido para habilitar ou desabilitar o botão
        if (!vendaModel.get('id_venda')) {
            this.ui.btnAdd.attr('disabled', 'disabled');
        } else {
            this.ui.btnAdd.removeAttr('disabled');
        }
    },
    cresceInput: function () {
        //aumenta a altura do textarea
        this.ui.txtTramite.css({
            height: '80px'
        });
    },
    diminuiInput: function () {
        //diminui quando não tiver valor
        if (!this.ui.txtTramite.val()) {
            this.ui.txtTramite.css({
                height: '30px'
            });
        }
    },
    addTramite: function () {
        //mostra o box onde esta o formulario para adicionar tramite quando o id_venda existir
        if (vendaModel.get('id_venda')) {
            this.ui.boxForm.show();
            this.ui.btnAdd.attr('disabled', 'disabled');
            this.diminuiInput();
        }
    },
    cancelaTramite: function () {
        //cancela o tramite
        this.ui.txtTramite.val('');
        this.ui.boxForm.hide();
        this.ui.btnAdd.removeAttr('disabled');
    },
    salvarTramite: function () {
        var that = this;//escopo
        //varifica se existe id_venda
        if (vendaModel.get('id_venda')) {
            var model = new SpTramite();//instancia a model
            //seta os valores
            model.set({
                'st_tramite': that.ui.txtTramite.val(),
                'id_categoriatramite': 3,
                'id_campo': vendaModel.get('id_venda')
            });

            //salva os dados
            model.save(null, {
                beforeSend: function () {
                    loading();
                },
                complete: function () {
                    loaded();
                },
                success: function (model, response) {
                    //on success faz o fetch novamente dos dados do tramite
                    that.collection.fetch({
                        reset: true,
                        success: function () {
                            //mostra mensagem de sucesso
                            $.pnotify({title: 'Sucesso!', text: 'Tramite salvo com sucesso!', type: 'success'});
                            that.cancelaTramite();//esconde o formulario
                        }
                    });
                },
                error: function (model, response) {
                    $.pnotify({title: 'Erro!', text: 'Houve um erro ao tentar salvar tramite.', type: 'error'});
                }
            });
        } else {//se o id_venda não estiver definido montra o aviso
            $.pnotify({
                title: 'Aviso!',
                text: 'Venda não foi definida, para criar uma interação/tramite, salve a venda.',
                type: 'warning'
            });
        }
        return false;
    },
    events: {
        'click #btn-add-tramite': 'addTramite',
        'focus #st_tramite': 'cresceInput',
        'blur #st_tramite': 'diminuiInput',
        'click #btn-cancelar-tramite': 'cancelaTramite',
        'submit #form-tramite': 'salvarTramite'
    }
});


/**
 ******************************************************************************************
 ******************************************************************************************
 ********* SHOW LAYOUT ********************************************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var telaVendas = new LayoutVenda();
G2S.show(telaVendas);
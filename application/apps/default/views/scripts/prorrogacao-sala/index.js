/**
 * JavaScript Neemias Santos
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 * Created 21/07/15.
 */

/**
 ******************************************************************************************
 ******************************************************************************************
 **************************************** COLLECTION **************************************
 ******************************************************************************************
 ******************************************************************************************
 */

var EntidadeRecursivaCollection = Backbone.Collection.extend({
    url: '/entidade/retornar-entidade-recursiva-id',
    model: VwEntidadeRecursiva
});

var ProjetoPedagocigoCollection = Backbone.Collection.extend({
    url: '/default/projeto-pedagogico',
    model: VwProjetoPedagogico
});

var DisciplinaCollection = Backbone.Collection.extend({
    url: '/default/prorrogacao-sala/retorna-disciplina',
    model: Disciplina
});

var SalaAulaCollection = Backbone.Collection.extend({
    url: '/default/prorrogacao-sala/retorna-sala-aula',
    model: SalaDeAula
});

var matriculaGlobal;

/**
 ******************************************************************************************
 ******************************************************************************************
 *************************** LAYOUT FUNCIONALIDADE ****************************************
 ******************************************************************************************
 ******************************************************************************************
 */

var TelaProrrogacaoSala = Marionette.LayoutView.extend({
    template: '#template-layout-prorrogacao-sala',
    tagName: 'div',
    className: 'container-fluid',
    ui: {
        'id_entidadematriz': 'select[name="id_entidadematriz"]',
        'id_projetopedagogico': 'select[name="id_projetopedagogico"]',
        'id_disciplina': 'select[name="id_disciplina"]',
        'id_saladeaula': 'select[name="id_saladeaula"]',
        'dt_inicio': 'input[name="dt_inicio"]',
        'nu_dias': 'input[name="nu_dias"]',
        'dt_termino': 'input[name="dt_termino"]',
        'btnPesquisar': '#btnPesquisar',
        'btnProrrogar': '#btnProrrogar'
    },
    events: {
        'change select[name="id_entidadematriz"]': 'populaSelectProjeto',
        'change select[name="id_projetopedagogico"]': 'populaSelectDisciplina',
        'change select[name="id_disciplina"]': 'popularSelectSalaAula',
        'click #btnPesquisar': 'pesquisaDados',
        'click #btnProrrogar': "prorrogaCollection",
        'click .select-all-header-cell input[type="checkbox"]': 'desmarcaTodos'
    },
    regions: {
        formgrade: '#regiao-prorrogacao-sala'
    },
    onShow: function () {
        this.populaSelectEntidade();
        $('#container-DataGrid').hide();
        loaded();
        return this;
    },
    populaSelectEntidade: function () {
        var that = this;
        var collectionEntidadeRecursiva = new EntidadeRecursivaCollection();
        collectionEntidadeRecursiva.fetch({
            beforeSend: function () {
                loading();
            },
            success: function () {
                var view = new SelectView({
                    el: that.ui.id_entidadematriz, // Elemento da DOM
                    collection: collectionEntidadeRecursiva, // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_nomeentidade', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_entidade', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: null      // ID da option que receberá o atributo "selected"
                    }
                });

                view.render();
            }
        })
    },
    populaSelectProjeto: function () {
        var that = this;
        var collectionProjetoPedagogico = new ProjetoPedagocigoCollection();
        collectionProjetoPedagogico.url += '/projeto-pedagogico-entidade/id_entidadecadastro/' + that.ui.id_entidadematriz.val() + '/id_situacao/7/bl_ativo/1';
        collectionProjetoPedagogico.fetch({
            beforeSend: function () {
                loading();
            },
            success: function () {
                that.ui.id_projetopedagogico.select2('destroy');
                that.ui.id_projetopedagogico.empty();
                that.ui.id_projetopedagogico.prepend('<option value="" selected="selected">Escolha uma opção</option>');
                var view = new SelectView({
                    el: that.ui.id_projetopedagogico, // Elemento da DOM
                    collection: collectionProjetoPedagogico, // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_projetopedagogico', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_projetopedagogico', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: null      // ID da option que receberá o atributo "selected"
                    }
                });

                view.render();
                that.ui.id_projetopedagogico.select2();
                loaded();
            }
        })
    },
    populaSelectDisciplina: function () {
        var that = this;
        var collectionDisciplina = new DisciplinaCollection();
        collectionDisciplina.url += '/id_projetopedagogico/' + that.ui.id_projetopedagogico.val();
        collectionDisciplina.fetch({
            beforeSend: function () {
                loading();
            },
            success: function () {
                that.ui.id_disciplina.select2('destroy');
                that.ui.id_disciplina.empty();
                that.ui.id_disciplina.prepend('<option value="" selected="selected">Escolha uma opção</option>');
                var view = new SelectView({
                    el: that.ui.id_disciplina, // Elemento da DOM
                    collection: collectionDisciplina, // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_disciplina', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_disciplina', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: null      // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();
                that.ui.id_disciplina.select2();
                loaded();
            }
        });

    },
    popularSelectSalaAula: function () {
        var that = this;
        var collectionSalaAula = new SalaAulaCollection();
        collectionSalaAula.url += '/id_entidade/' + that.ui.id_entidadematriz.val() + '/id_projetopedagogico/' + that.ui.id_projetopedagogico.val() + '/id_disciplina/' + that.ui.id_disciplina.val();
        collectionSalaAula.fetch({
            beforeSend: function () {
                loading();
            },
            success: function () {
                that.ui.id_saladeaula.select2('destroy');
                that.ui.id_saladeaula.empty();
                that.ui.id_saladeaula.prepend('<option value="" selected="selected">Escolha uma opção</option>');
                var view = new SelectView({
                    el: that.ui.id_saladeaula,
                    collection: collectionSalaAula,
                    childViewOptions: {
                        optionLabel: 'st_saladeaula',
                        optionValue: 'id_saladeaula',
                        optionSelected: null
                    }
                });
                view.render();
                that.ui.id_saladeaula.select2();
                loaded();
            }
        });

    },
    pesquisaDados: function () {
        var that = this;
        var id_entidadematriz = that.ui.id_entidadematriz.val();
        var id_projetopedagogico = that.ui.id_projetopedagogico.val();
        var id_disciplina = that.ui.id_disciplina.val();
        var id_saladeaula = that.ui.id_saladeaula.val();
        var dt_inicio = that.ui.dt_inicio.val();
        var dt_termino = that.ui.dt_termino.val();

        if (!id_projetopedagogico || !id_entidadematriz || !id_disciplina || !id_saladeaula || !dt_inicio || !dt_termino) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção',
                text: 'Preencha os campos solicitados!'
            });
            return false;
        }

        loading();

        var HtmlCheckeBoxCell = Backgrid.Cell.extend({
            render: function () {
                var check = "checked";
                if (this.model.get('bl_prorroga') == '') {
                    check = "";
                }

                var elTemp = _.template('<input type="checkbox" ' + check + '  name="bl_prorroga" class="bl_prorroga">');
                this.$el.html(elTemp);
                return this;
            },
            events: {
                'change input[name="bl_prorroga"]': 'onChange'
            },
            onChange: function () {
                //verificar se p checkbox esta marcado e seta na modelo true ou false
                var checked = this.$el.find('input[name="bl_prorroga"]').is(":checked");
                this.model.set('bl_prorroga', checked);
            }
        });

        var columns = [{
            name: "bl_prorroga",
            cell: HtmlCheckeBoxCell,
            editable: false,
            headerCell: "select-all"
        }, {
            name: "st_nomecompleto",
            label: "Nome do Aluno",
            editable: false,
            cell: 'string'
        }, {
            name: "st_nomeentidade",
            label: "Entidade",
            editable: false,
            cell: 'string'
        }, {
            name: "dt_encerramento",
            label: "Data de Término",
            editable: false,
            cell: 'string'
        }, {
            name: "dt_ultimoacesso",
            label: "Último dia de acesso",
            editable: false,
            cell: 'string'
        }];

        var PageableLog = Backbone.PageableCollection.extend({
            model: VwMatricula,
            url: '/default/prorrogacao-sala/retorna-matricula-params',
            state: {
                pageSize: 30
            },
            mode: "client"
        });
        var data = {
            id_projetopedagogico: that.ui.id_projetopedagogico.val(),
            id_entidadematriz: that.ui.id_entidadematriz.val(),
            id_disciplina: that.ui.id_disciplina.val(),
            id_saladeaula: that.ui.id_saladeaula.val(),
            dt_inicio: that.ui.dt_inicio.val(),
            dt_termino: that.ui.dt_termino.val()
        };

        pageableLog = new PageableLog();

        var RowGrid = Backgrid.Row.extend({
            initialize: function (options) {
                RowGrid.__super__.initialize.apply(this, arguments);
            },
            marcarDesmarcar: function () {
                var that = this;
                var elem = this.$el.find('td').find('input[type="checkbox"]');
                //altera o valor do campo para extender ou não os dias para aquela matricula
                that.model.set('bl_prorroga', elem.is(":checked"))
            },
            events: {
                'click input[type="checkbox"].bl_prorroga': "marcarDesmarcar"
            }
        });
        var pageableGridLog = new Backgrid.Grid({
            className: 'backgrid table table-bordered',
            columns: columns,
            collection: pageableLog,
            emptyText: "Nenhum registro encontrado",
            row: RowGrid
        });

        $('#container-DataGrid').show();
        var $elemento = $('#template-data-grade');
        $elemento.empty();
        $elemento.append(pageableGridLog.render().$el);

        var paginator = new Backgrid.Extension.Paginator({
            collection: pageableLog
        });

        $elemento.append(paginator.render().el);
        $('.container-DataGrid').show();
        pageableLog.fetch({
            data: data,
            reset: true
        });

        $elemento.find("input[type=checkbox]").attr("checked", true);
        $(".select-all-header-cell input[type='checkbox'] ").prop('checked', true);

        loaded();
    },
    prorrogaCollection: function () {
        var that = this;
        var nu_dias = that.ui.nu_dias.val();

        if (!nu_dias) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção',
                text: 'Preencha o campo DIAS com a quantidade de dias para estender!'
            });
            that.ui.nu_dias.focus();
            return false;
        }

        parametros = {
            'collection': pageableLog.toJSON(),
            'nu_dias': nu_dias
        };

        $.ajax({
            dataType: 'json',
            type: 'post',
            url: '/prorrogacao-sala/prorroga-alunos-sala',
            data: parametros,
            beforeSend: function () {
                loading();
            },
            success: function (data) {
                $.pnotify({
                    title: data['title'],
                    text: data['mensagem'],
                    type: 'success'
                });
                that.pesquisaDados();
            },
            complete: function () {
                loaded();
            }
        })

    },
    desmarcaTodos: function () {
        var elem = $(".select-all-header-cell input[type='checkbox']").is(":checked");

        _.each(pageableLog.models, function (val, key) {
            val.set('bl_prorroga', elem);
        });
    }
});

var telaGradeProrrogaSala = new TelaProrrogacaoSala();
G2S.show(telaGradeProrrogaSala);

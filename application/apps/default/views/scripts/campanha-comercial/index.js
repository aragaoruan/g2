var thatSalvar;
var thatFormaPagamentoMarionette;
var timer;

/**
 * Objeto com os html dos templates que são utilizados na funcionalidade
 * @type Object
 */
TemplatesCampanha = ({
    valorTipodesconto: function () {
        var html = '<label class="control-label">'
            + '<div class="input-prepend">'
            + '<span class="add-on">R$</span>'
            + '<input type="text" class="numeric desabilitactrlv span12" value="<%=nu_valordesconto%>" title="Valor do Desconto" name="nu_valordesconto" id="nu_valordesconto"/>'
            + '</div>'
            + '</label>';
        return html;
    },
    percentualTipoDesconto: function () {
        var html = '<label  class="control-label">'
            + '<div class="input-append">'
            + '<input type="text" class="numeric desabilitactrlv span12" value="<%=nu_valordesconto%>" title="Valor do Desconto" name="nu_valordesconto" id="nu_valordesconto"/>'
            + '<span class="add-on">%</span>'
            + '</div>'
            + '</label>';
        return html;
    },
    itemFormaPagamento: function () {
        var html = '<td>'
            + '<input type="hidden" name="id_formapagamento" value="<%=id_formapagamento%>">'
            + '<%=st_formapagamento%>'
            + '</td>'
            + '<td>'
            + '<center><a class="btn-danger btn" data-toggle="tooltip" title="Remover Item"><i class="icon-remove icon-white"></i></a></center>'
            + '</td>';
        return html;
    },
    itemCategoria: function () {
        var html = '<td>'
            + '<input type="hidden" name="id_categoria" value="<%=id_categoria%>"><%=st_categoria%>'
            + '</td>'
            + '<td>'
            + '<center><a class="btn-danger btn remove-item" data-toggle="tooltip" title="Remover Item"><i class="icon-remove icon-white"></i></a></center>'
            + '</td>';
        return html;
    },
    itemProdutoPesquisa: function () {
        var html = '<td><%=st_produto%></td>'
            + '<td><%=st_tipoproduto%></td>'
            + '<td><%=nu_valor%></td>'
            + '<td><a title="Adicionar Item" class="btn btn-success btn-small " data-toggle="tooltip"><i class="icon-ok icon-white"></i></a></td>';
        return html;
    },
    containerProdutos: function () {
        var html = ' <fieldset>'
            + '<legend>Produtos <label class="checkbox pull-right"><input type="checkbox" title="Todos os Produtos" value="1" id="bl_todosprodutos" name="bl_todosprodutos"> Todos</label></legend>'
            + '<div style="margin-bottom: 10px;" class="row-fluid">'
            + '<div class="span12">'
            + '<button type="button" class="btn pull-right pesquisar" data-toggle="tooltip" title="Localizar">Localizar</button>'
            + '</div>'
            + '</div>'
            + '<table class="table table-striped table-bordered table-hover backgrid backgrid-selectall">'
            + '<thead>'
            + '<tr>'
            + '<th>Produto</th>'
            + '<th>Tipo Produto</th>'
            + '<th></th>'
            + '</tr>'
            + '</thead>'
            + '<tbody>'
            + '</tbody>'
            + '</table>'
            + '</fieldset>';
        return html;
    },
    itemProdutos: function () {
        var html = '<td><input type="hidden" name="id_produto" value="<%=id_produto%>"><%=st_produto%></td>'
            + '<td><%=st_tipoproduto%></td>'
            + '<td><center><a class="btn-danger remove-item btn" data-toggle="tooltip" title="Remover Item"><i class="icon-remove icon-white"></i></a></center></td>';
        return html;
    },
    containerCategorias: function () {
        var html = ''
            + '<fieldset>'
            + '<legend>Categorias</legend>'
            + '<div class="row-fluid">'
            + '<div class="input-append span12">'
            //                        + '<div class="span9">'
            + '<select id="id_categoria" name="id_categoria" class="span8">'
            + '<option>Selecione</option>'
            + '</select>'
            //                        + '</div>'
            //                        + '<div class="span3">'
            + '<button type="button" class="btn additem" data-toggle="tooltip" title="Adicionar">Adicionar</button>'
            //                        + '</div>'
            + '</div>'
            + '</div>'
            + '<table class="table table-striped table-bordered table-hover backgrid backgrid-selectall">'
            + '<thead>'
            + '<tr>'
            + '<th>Categoria do Produto</th>'
            + '<th></th>'
            + '</tr>'
            + '</thead>'
            + '<tbody>'
            + '</tbody>'
            + '</table>'
            + '</fieldset>'
            + '';
        return html;
    },
    containerFormaPagamento: function () {
        var html = '<fieldset>'
            + '<legend>Forma de Pagamento <label class="checkbox pull-right">'
            + '<input type="checkbox" title="Todas as Formas de Pagamento" value="1" id="bl_todasformas" name="bl_todasformas"> Todas</label></legend>'
            + '<div class="row-fluid">'
            + '<div class="span9">'
            + '<select id="id_formapagamento" name="id_formapagamento" class="span12">'
            + '<option>Selecione</option>'
            + '</select>'
            + '</div>'
            + '<div class="span3">'
            + '<button type="button" class="btn additem" data-toggle="tooltip" title="Adicionar">Adicionar</button>'
            + '</div>'
            + '</div>'
            + '<table class="table table-striped table-bordered table-hover backgrid backgrid-selectall">'
            + '<thead>'
            + '<tr>'
            + '<th>Forma de Pagamento</th>'
            + '<th></th>'
            + '</tr>'
            + '</thead>'
            + '<tbody>'
            + '</tbody>'
            + '</table>'
            + '</fieldset>';
        return html;
    },
    containerPesquisaProduto: function () {
        var html = '<form name="form-pesquisa-produto" id="form-pesquisa-produto" class="form-inline" action="" method="post">'
            + ' <label>'
            + ' Nome: '
            + ' <input type="text" class="" name="st_produto" id="st_produto">'
            + ' </label>'
            + ' <label>'
            + ' Situação: '
            + ' <select name="id_situacao" id="id_situacao" class="input-small">'
            + ' <option value="" selected>Selecione</option>'
            + ' </select>'
            + ' </label>'
            + ' <button data-loading-text="Enviando..." type="submit" class="btn"><i class="icon-search"></i> Pesquisar</button>'
            + ' </form>'
            + ' <div style="height:300px; overflow-y:auto;">'
            + ' <table class="table table-striped table-bordered table-hover backgrid backgrid-selectall">'
            + ' <thead>'
            + ' <tr>'
            + ' <th>Produto</th>'
            + ' <th>Tipo Produto</th>'
            + ' <th>Valor</th>'
            + ' <th></th>'
            + ' </tr>'
            + '  </thead>'
            //                + '<tbody>'
            //                + '</tbody>'
            + '</table>'
            + '</div>';
        return html;
    }
});
/**
 * Templates para tab de cupom
 * @type Object
 */
TemplatesCupom = ({
    tabelaCupons: function () {
        var html = '<table class="table table-striped table-bordered table-hover backgrid backgrid-selectall">'
            + '<thead>'
            + '<tr>'
            + '<th>Código do Cupom</th>'
            + '<th>Desconto</th>'
            + '<th>Inicio</th>'
            + '<th>Término</th>'
            + '<th>Campanha</th>'
            + '<th>Ativo</th>'
            + '<th>Usar uma vez</th>'
            + '</tr>'
            + '</thead>'
            + '<tbody>'
            + '</tbody>'
            + '</table>';
        return html;
    },
    itemTabelaCupom: function () {
        var html = '<td><%=st_codigocupom%></td>'
            + '<td><%=nu_desconto%> - <%=st_tipodesconto%></td>'
            + '<td><%=dt_inicio%></td>'
            + '<td><%=dt_fim%></td>'
            + '<td><%=st_campanhacomercial%></td>'
            + '<td><span class="label <%=class_label%>"><%=ativacao%></span></td>'
            + '<td><span class="label <%=class_label2%>"><%=unico%></span></td>';
        return html;
    },
    itemTabelaCupomEdit: function () {
        var html = ' <td><%=st_codigocupom%></td>'
            + '<td><%=nu_desconto%> - <%=st_tipodesconto%></td>'
            + '<td><%=dt_inicio%></td>'
            + '<td><%=dt_fim%></td>'
            + '<td><%=st_campanhacomercial%></td>'
            + '<td>'
            + '<input type="checkbox" title="Ativar na criação" <%=ativacao%> value="1" id="bl_ativacao" name="bl_ativacao">'
            + '</td>'
            + '<td>'
            + '<input type="checkbox" title="Utilizar somente uma vez" <%=unico%> value="1" id="bl_unico" name="bl_unico">'
            + '</td>';
        return html;
    }
});

/**
 **************************** MODELS
 */
/**
 * @type Implementando complementos na modelo de Forma Pagamento @exp;FormaPagamento@call;extend
 */
var FormaPagamentoModel = FormaPagamento.extend({
    idAttribute: 'id_formapagamento'
});
/**
 * @type Implementando idAttribute no modelo @exp;Situacao@call;extend
 */
var SituacaoModel = Situacao.extend({
    idAttribute: 'id_situacao'
});

/**
 * @type Extendendo a campanha categoria model para atribuir mais uma atributo fantasma @exp;CampanhaCategorias@call;extend
 */
var CampanhaCategoriasModel = CampanhaCategorias.extend({
    initialize: function () {
        this.defaults['st_categoria'] = '';
    }
});

var VwCupomCampanhaModel = Cupom.extend({
    initialize: function () {
        this.defaults['st_campanhacomercial'] = '';
        this.defaults['st_tipodesconto'] = '';
    }
});

var CampanhaProdutoModel = CampanhaProduto.extend({
    idAttribute: "id_produto"
});

var CampanhaFormaDePagamentoModel = CampanhaFormaPagamento.extend({
    idAttribute: "id_formapagamento"
});

/**
 **************************** COLLECTIONS
 */
var VwCupomCampanhaCollection = Backbone.Collection.extend({
    model: VwCupomCampanhaModel,
    url: 'campanha-comercial/retornar-cupons'
});

var VwProdutoCollection = Backbone.Collection.extend({
    model: VwProduto,
    url: 'produto/pesquisar-produto'
});


var VwProdutoCompartilhadoCollection = Backbone.Collection.extend({
    model: VwProdutoCompartilhado,
    url: '/api/vw-produto-compartilhado'
});

var CampanhaFormaDePagamento = Backbone.Collection.extend({
    model: CampanhaFormaDePagamentoModel,
    url: '/forma-pagamento/retorna-forma-pagamento-por-campanha'
});

var PremioCampanhaCollection = Backbone.Collection.extend({
    model: PremioProduto
})

/**
 * @type Collection de Situacao @exp;Backbone@pro;Collection@call;extend
 */
var SituacaoCollection = Backbone.Collection.extend({
    model: SituacaoModel,
    url: function () {
        return "/api/situacao/?" + $.param(_.defaults(this.data));
    },
    data: {}
});

/**
 * @type Collection para CampanhaComercial @exp;Backbone@pro;Collection@call;extend
 */
var CampanhaCollection = Backbone.Collection.extend({
    model: CampanhaComercial,
    url: 'api/campanha-comercial'
});

/**
 * @type Collection para VwCampanhaFormaPagamento @exp;Backbone@pro;Collection@call;extend
 */
var VwCampanhaFormaPagamentoCollection = Backbone.Collection.extend({
    model: VwCampanhaFormaPagamento,
    url: 'api/vw-campanha-forma-pagamento'
});
/**
 * @type Collection VwCampanhaProduto @exp;Backbone@pro;Collection@call;extend
 */
var VwCampanhaProdutoCollection = Backbone.Collection.extend({
    model: VwCampanhaProduto,
    url: 'api/vw-campanha-produto'
});
/**
 * @type Collection para Categoria de Campanha @exp;Backbone@pro;Collection@call;extend
 */
var CampanhaCategoriasCollection = Backbone.Collection.extend({
    model: CampanhaCategoriasModel,
    url: 'api/campanha-categorias'
});
/**
 * @type Collection para Cupom @exp;Backbone@pro;Collection@call;extend
 */
var CupomCollection = Backbone.Collection.extend({
    model: Cupom,
    url: 'api/cupom'
});

/**
 * @type Collection para Forma de Pagamento @exp;Backbone@pro;Collection@call;extend
 */
var FormaPagamentoCollection = Backbone.Collection.extend({
    model: FormaPagamentoModel,
    url: 'api/forma-pagamento'
});

/**
 * @type Collection para Categoria @exp;Backbone@pro;Collection@call;extend
 */
var CategoriaCollection = Backbone.Collection.extend({
    model: Categoria,
    url: 'api/categoria'
});

/**
 * @type Collection para Campanha Desconto @exp;Backbone@pro;Collection@call;extend
 */
var CampanhaDescontoCollection = Backbone.Collection.extend({
    model: CampanhaDesconto,
    url: 'api/campanha-desconto'
});

/**
 **************************** VIEWS
 */


/**
 **************************** Forma de pagamento
 */

/**
 * @type View FormaPagamento @exp;Backbone@pro;View@call;extend
 */
var FormaPagamentoMarionetteView = Marionette.ItemView.extend({
    tagName: 'div',
    className: 'span4 well well-small',
    template: '#forma-pagamento-marionette-view',
    modeloFormaPgto: {},
    initialize: function () {
        thatFormaPagamentoMarionette = this;
    },
    onRender: function () {
        var that = this;
        this.populaSelectFormaPgto();
        var $elementoTabel = that.$el;
        if (G2S.editModel) {
            if (G2S.editModel.get('bl_todasformas')) {
                this.$el.find('input[name="bl_todasformas"]').attr('checked', 'checked');
                this.gerenciaFormasPgto();
            }


            var campanhaMeioPagamento = new VwCampanhaFormaPagamentoCollection();
            campanhaMeioPagamento.url += '/id_campanhacomercial/' + G2S.editModel.id;
            var fetchCampanha = campanhaMeioPagamento.fetch();

            if (fetchCampanha) {
                campanhaMeioPagamentoCollectionView = new CollectionView({
                    collection: campanhaMeioPagamento,
                    childViewConstructor: VwFormaPagamentoView,
                    childViewTagName: 'tr',
                    el: $elementoTabel.find('table tbody')
                });
                campanhaMeioPagamentoCollectionView.render();
            }
        }
        return this;
    },
    adicionarFormaPagamento: function () {
        var that = this;
        var formaPagamento = {};//monta um objeto vazio

        var elFomaPagamento = $(that.el).find('select#id_formapagamento'); //atribui a variavel o elemento da dom

        if (elFomaPagamento.val()) {//verifica se o valor é válido
            if (!that.modeloFormaPgto[elFomaPagamento.val()]) {
                //monta o objeto com o valor e texto selecionado
                formaPagamento = {
                    'id_formapagamento': elFomaPagamento.val(),
                    'st_formapagamento': elFomaPagamento.find("option:selected").text()
                };
                that.modeloFormaPgto[elFomaPagamento.val()] = formaPagamento;
                //adiciona os dados na collection
                var collectionFormaPgto = new VwCampanhaFormaPagamentoCollection(formaPagamento);
                var viewFormaPagamento = new CollectionView({
                    collection: collectionFormaPgto,
                    childViewConstructor: VwFormaPagamentoView,
                    childViewTagName: 'tr',
                    el: $(that.el).find('table tbody')
                });
                //adiciona os dados na model
                var formaPgto = new VwCampanhaFormaPagamento(formaPagamento);
                viewFormaPagamento._rendered = true;
                viewFormaPagamento.add(formaPgto);//adiciona o objeto
            } else {
                $.pnotify({
                    title: 'Atenção!',
                    text: 'Este item já foi adicionado a lista de Formas de Pagamento.',
                    type: 'warning'
                });
            }
            formaPgto.remove(); //observar o que isto faz
        }
    },
    populaSelectFormaPgto: function () {
        var that = this;
        var formaPgtoCollection = new FormaPagamentoCollection();
        formaPgtoCollection.fetch({
            success: function () {
                var formaPgto = new CollectionView({
                    collection: formaPgtoCollection,
                    childViewConstructor: SelectFormaPagamentoView,
                    childViewTagName: 'option',
                    el: $(that.el).find('select#id_formapagamento')
                });
                formaPgto.render();
            },
            complete: function () {
                //Escreve um elemento no inicio com o texto generico
                //$(that.el).find('select#id_formapagamento').prepend('<option value="0" selected>Selecione</option>');
            }
        });
    },
    adicionarTodasFormas: function () {
        var formaPagamento = new Array();//cria um novo array
        //precorre o objeto do select e recupera todos os valores
        $(this.el).find('select#id_formapagamento option[value!=""]').each(function (i, obj) {
            var valor = $(obj).val();
            var label = $(obj).text();
            if (valor && valor != 0) {
                //cria um array de objetos
                formaPagamento[i] = {
                    'id_formapagamento': valor,
                    'st_formapagamento': label
                };
            }
        });
        //chama a função para popular
        this.populaFormaPagamento(formaPagamento);
    },
    removerTodasFormas: function () {

        if (G2S.editModel) {
            var arrFormas = [];

            this.$el.find("table tbody tr").each(function (i, elemTr) {
                var id_formapagamento = $(elemTr).find('td input[name="id_formapagamento"]').val();
                arrFormas[i] = {
                    id_formapagamento: id_formapagamento,
                    id_campanhacomercial: G2S.editModel.id
                };
            });

            $.ajax({
                url: '/campanha-comercial/deletar-array-forma-pagamento',
                dataType: 'json',
                data: {
                    'data': arrFormas
                },
                type: 'post'
            });
        }

        this.$el.find('table tbody').empty();//limpa a tabela
    },
    gerenciaFormasPgto: function (obj) {
        //verifica se o checkbox esta checkado
        if (this.$el.find('input[name="bl_todasformas"]').is(':checked')) {
            this.$el.find('select#id_formapagamento').attr('disabled', 'disabled');
            this.$el.find('.additem').attr('disabled', 'disabled');
            this.removerTodasFormas();
        } else {
            this.$el.find('select#id_formapagamento').removeAttr('disabled');
            this.$el.find('.additem').removeAttr('disabled');
            this.adicionarTodasFormas();//adiciona todas as formas de pagamento
        }
    },
    populaFormaPagamento: function (collection) {
        that = this;
        var collectionFormaPgto = new VwCampanhaFormaPagamentoCollection(collection);//atribui o array de objeto no
        //monta o collectioView
        var viewFormaPagamento = new CollectionView({
            collection: collectionFormaPgto,
            childViewConstructor: VwFormaPagamentoView,
            childViewTagName: 'tr',
            el: $(that.el).find('table tbody')
        });
        viewFormaPagamento.render();
    },
    salvarFormaPagamento: function () {
        var arrFormas = [];
        if (G2S.editModel) {
            if ($("#container-forma-pagamento-marionette").is(":visible")) {
                this.$el.find("table tbody tr").each(function (i, elemTr) {
                    var id_formapagamento = $(elemTr).find('td input[name="id_formapagamento"]').val();
                    arrFormas[i] = {
                        id_formapagamento: id_formapagamento,
                        id_campanhacomercial: G2S.editModel.id
                    };
                });
                if (arrFormas.length) {
                    $.ajax({
                        url: '/campanha-comercial/salva-array-formas-pagamento',
                        dataType: 'json',
                        data: {
                            'data': arrFormas
                        },
                        type: 'post',
                        success: function (data) {
                            if (data.type == 'success') {
                                $.pnotify({title: 'Sucesso!', text: data.text, type: 'success'});
                            } else {
                                $.pnotify({title: 'Erro!', text: data.text, type: 'error'});
                            }
                        }
                    });
                }
            }
        }
    },
    events: {
        'click .additem': 'adicionarFormaPagamento',
        'click input[type="checkbox"][name="bl_todasformas"]': 'gerenciaFormasPgto'
    }
});


/**
 * @type View para itens de forma de pagamento @exp;Backbone@pro;View@call;extend
 */
var VwFormaPagamentoView = Backbone.View.extend({
    render: function () {
        var variaveis = this.model.toJSON();
        this.el.innerHTML = _.template(TemplatesCampanha.itemFormaPagamento(), variaveis);
        return this;
    },
    delete: function () {
        that = this;

        bootbox.confirm("Tem certeza de que deseja apagar o registro?", function (result) {
            if (result) {
                if (G2S.editModel) {
                    var idFormaPagamento = that.model.get('id_formapagamento');
                    that.model.set('id', idFormaPagamento);
                    that.model.url = '/api/vw-campanha-forma-pagamento/id_campanha/' + G2S.editModel.id + '/id_formapagamento/' + idFormaPagamento;
                    that.el.remove();
                    that.model.destroy({});

                    $.pnotify({title: 'Sucesso!', text: 'Forma de pagamento removida com sucesso!', type: 'success'});
                } else {
                    that.el.remove();
                }

            }
        });
    },
    events: {
        'click a.btn-danger': 'delete'
    }

});


/**
 * @type View que preenche os dados no select de forma de pagamento @exp;Backbone@pro;View@call;extend
 */
var SelectFormaPagamentoView = Backbone.View.extend({
    initialize: function () {
        this.model.bind('change', this.render);
    },
    render: function () {
        //escreve os valores no elemento atual
        this.$el.attr('value', this.model.id).text(this.model.get('st_formapagamento'));
        return this;
    }
});


/**
 **************************** Categoria da Campanha
 */

/**
 * @type View para CategoriaCampanha @exp;Backbone@pro;View@call;extend
 */
var CampanhaCategoriasView = Backbone.View.extend({
    modeloCampanha: {},
    initialize: function () {

    },
    render: function () {
        var elemetTemplate = _.template(TemplatesCampanha.containerCategorias(), {});
        this.el.innerHTML = elemetTemplate;
        this.populaSelectCategorias();
        if (G2S.editModel) {
            var elemTable = this.$el.find('fieldset').find("table").find("tbody");
            var campanhaCategorias = new CampanhaCategoriasCollection();
            campanhaCategorias.url += '/id_campanhacomercial/' + G2S.editModel.id;
            campanhaCategorias.fetch({
                success: function () {
                    new CollectionView({
                        collection: campanhaCategorias,
                        childViewConstructor: CategoriasView,
                        childViewTagName: 'tr',
                        el: elemTable
                    }).render();
                }
            });
        }
        return this;
    },
    populaSelectCategorias: function () {
        that = this;
        var categorias = new CategoriaCollection();
        categorias.fetch({
            success: function () {
                var categoria = new CollectionView({
                    collection: categorias,
                    childViewConstructor: SelectCategoriaView,
                    childViewTagName: 'option',
                    el: $('select#id_categoria')
                });
                categoria.render();
            },
            complete: function () {
                $('select#id_categoria').prepend('<option value="" selected>Selecione</option>');
            }
        });
    },
    adicionarItem: function () {
        that = this;
        var categorias = {};
        var elCategoria = $(that.el).find('select#id_categoria'); //atribui a variavel o elemento da dom

        if (elCategoria.val()) {
            if (!that.modeloCampanha[elCategoria.val()]) {
                categorias = {
                    'id_categoria': elCategoria.val(),
                    'st_categoria': elCategoria.find('option:selected').text()
                };
                that.modeloCampanha[elCategoria.val()] = categorias;
                var categoriaCollection = new CategoriaCollection();
                var categoriaModel = new Categoria(categorias);

                var viewCategorias = new CollectionView({
                    collection: categoriaCollection,
                    childViewConstructor: CategoriasView,
                    childViewTagName: 'tr',
                    el: $(that.el).find('table tbody')
                });
                //adiciona os dados na model
                viewCategorias._rendered = true;
                viewCategorias.add(categoriaModel);//adiciona o objeto
            } else {
                $.pnotify({
                    title: 'Atenção!',
                    text: 'Este item já foi adicionado a lista de Categorias.',
                    type: 'warning'
                });
            }
        }
    },
    events: {
        'click .additem': 'adicionarItem'
    }
});
/**
 * @type View para select de Categorias @exp;Backbone@pro;View@call;extend
 */
var SelectCategoriaView = Backbone.View.extend({
    initialize: function () {
        this.model.bind('change', this.render);
    },
    render: function () {
        this.$el.attr('value', this.model.get('id_categoria')).text(this.model.get('st_categoria'));
        return this;
    }
});
/**
 * Seleção de produtos para campanha premio
 */

var ProdutoPremioCollection = Backbone.Collection.extend({
    model: Produto
});

var ProdutosPremiosSelecionadosCollection = Backbone.Collection.extend({
    model: Produto
});

ProdutosPremiosSelecionadosCollection.comparator = function (model) {
    return model.get('st_produto');
};

ProdutoPremioCollection.comparator = function (model) {
    return model.get('st_produto');
};

var collectionProduto = new ProdutoPremioCollection();

var collectionSelecionados = new ProdutosPremiosSelecionadosCollection();

var ProdutoItemView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: '#produto-premio',
    collection: collectionProduto,
    initialize: function () {
        return this;
    },
    onRender: function () {
    },
    addModel: function () {
        var that = this;
        collectionSelecionados.add(that.model.clone());
        that.remove();
        that.model.url = '/api/produto',
            that.model.destroy({})
        collectionSelecionados.comparator = 'st_produto';
        collectionSelecionados.sort();
    },
    events: {
        'click #btn-add': 'addModel'
    }
});


var ProdutosSelecionadosItemView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: '#produto-premio-selecionados',
    initialize: function () {
        return this;
    },
    onRender: function () {
        var that = this;
        var q = collectionProduto.findWhere({id_produto: that.model.get('id_produto')});
        collectionProduto.remove(q);
        return this;
    },
    removeModel: function () {
        var that = this;
        collectionProduto.add(that.model.clone());
        this.remove();
        this.model.url = '/api/produto/';
        this.model.destroy({});
        collectionProduto.comparator = 'st_produto';
        collectionProduto.sort();
    },

    events: {
        'click #btn-remove': 'removeModel'
    }
});

var ProdutoCollectionSelecionadosView = Marionette.CollectionView.extend({
    template: '#main-template',
    childView: ProdutosSelecionadosItemView,
    initialize: function () {
        this.collection.fetch({
            url: G2S.editModel ? '/campanha-comercial/retorna-produto-premio-selecionado/id_campanhacomercial/' + G2S.editModel.id + '/id_premio/' + G2S.editModel.get('id_premio') : '',
            beforeSend: function () {
                loading();
            },

            complete: function () {
                loaded();
            }
        });
    },
    onRender: function () {

    }
});

var ProdutoCollectionView = Marionette.CollectionView.extend({
    template: '#main-template',
    childView: ProdutoItemView,
    initialize: function () {
    },
    onRender: function () {
    }
});


/**
 * @type View para lista de categorias vinculadas a campanha @exp;Backbone@pro;View@call;extend
 */
var CategoriasView = Backbone.View.extend({
    initialize: function () {
        this.model.bind('change', this.render);
    },
    render: function () {
        this.el.innerHTML = _.template(TemplatesCampanha.itemCategoria(), this.model.toJSON());
        return this;
    },
    removeItem: function () {
        this.model.url = '/api/campanha-categorias/' + this.model.get('id_campanhacategoris');

        that = this;
        bootbox.confirm("Tem certeza de que deseja apagar o registro?", function (result) {
            if (result) {
                that.model.destroy({
                    beforeSend: function () {
                        loading();
                    },
                    success: function (collection) {
                        loaded();
                        $.pnotify({
                            title: 'Sucesso!',
                            text: 'Categoria excluída com sucesso! ',
                            type: 'success'
                        });
                    },
                    error: function (collection, response) {
                        loaded();
                        $.pnotify({
                            title: 'Erro!',
                            text: 'Houve um erro ao deletar a categoria, tente novamente! ',
                            type: 'error'
                        });
                    },
                    complete: function () {
                        loaded();
                    }
                });
                that.remove();
            }
        });
    },
    events: {
        'click a.remove-item': 'removeItem'
    }
});

/**
 **************************** Modal de Pesquisa de Produto
 */

/**
 * @type View para Formulário de Pesquisa de Produto @exp;Backbone@pro;View@call;extend
 */
var FormPesquisaProdutoView = Marionette.CompositeView.extend({
    template: _.template(TemplatesCampanha.containerPesquisaProduto()),
    onRender: function () {
//        this.el.innerHTML = _.template(TemplatesCampanha.containerPesquisaProduto(), {});
        this.populaSelectSituacao();
//        return this;
    },
    populaSelectSituacao: function () {
        that = this;
        var situacao = new SituacaoCollection();//instancia a collection
        situacao.data = {st_tabela: 'tb_produto'};//seta os parametros para consulta
        //fetch
        situacao.fetch({
            success: function () {
                var formaPgto = new CollectionView({
                    collection: situacao,
                    childViewConstructor: SelectSituacaoView,
                    childViewTagName: 'option',
                    el: that.$el.find('select#id_situacao')
                });
                formaPgto.render();
            },
            complete: function () {
                //Escreve um elemento no inicio com o texto generico
                that.$el.find('select#id_situacao').prepend('<option value="" selected>Selecione</option>');
            }
        });
    },
    pesquisarProduto: function () {
        that = this;
        var params = "";//Cria variavel para armazenar os parametros
        var st_produto = that.$el.find('#st_produto').val();//atribui o st_produto a variavel
        var id_situacao = that.$el.find('#id_situacao').val();//atribui o id_situacao a variavel
        if (st_produto || id_situacao) { //verifica se veio pelo menos um parametro
            var produtoCollection = new VwProdutoCompartilhadoCollection(); //instancia a collection
            params += st_produto ? "/st_produto/" + st_produto : ""; //incrementa o paramentro a variavel
            params += id_situacao ? "/id_situacao/" + id_situacao : "";
            produtoCollection.url += params; //incrementa parametros na variavel para fazer um get
            produtoCollection.fetch({
                beforeSend: function () {
                    loading();
//                    that.$el.find("button[type='submit']").button('loading');
                    that.$el.find('div table tbody').empty();
                },
                success: function (collection) {
                    that.$el.find('div table tbody').empty();
                    var produto = new ProdutosPesquisaView({
                        collection: collection,
                        el: that.$el.find('div table')
                    });
                    produto.render();
                },
                error: function (collection, response) {
                    loaded();
//                    var resposta = JSON.parse(response.responseText);
                    $.pnotify({
                        title: 'Erro!',
                        text: 'Houve um erro ao pesquisar produtos, tente novamente! ',
                        type: 'error'
                    });
                },
                complete: function () {
                    loaded();
//                    that.$el.find("button").button('reset');
                }
            });
        } else {
            $.pnotify({title: 'Atenção!', text: 'Pelo menos um parâmetro deve ser passado.', type: 'warning'});
        }
        return false;
    },
    events: {
        'submit form#form-pesquisa-produto': 'pesquisarProduto'
    }
});

/**
 * @type View para preencher o select de situacao @exp;Backbone@pro;View@call;extend
 */
var SelectSituacaoView = Backbone.View.extend({
    initialize: function () {
        this.model.bind('change', this.render);
    },
    render: function () {
        this.$el.attr('value', this.model.id).text(this.model.get('st_situacao'));
        return this;
    }
});

var thatProdutoPesquisa;
/**
 * @type View para lista da pesquisa de produto @exp;Backbone@pro;View@call;extend
 */
var ProdutoPesquisaView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: _.template(TemplatesCampanha.itemProdutoPesquisa()),
    modelSet: {},
    initialize: function () {
        thatProdutoPesquisa = this;
    },
    onRender: function () {
//        var variaveis = this.model.toJSON();
    },
    adicionarProdutoLista: function () {
        var modelo = this.model.toJSON();
        var id = modelo.id_produto;
        if (!this.modelSet[id]) {
            $('#bl_todosprodutos').attr('checked', false);
            this.modelSet[id] = modelo;
            var produtoModel = new VwProduto(modelo);
            var produtoCollection = new VwProdutoCollection(modelo); //instancia a collection
            var produto = new CollectionView({
                collection: produtoCollection,
                childViewConstructor: ItemCapanhaProdutoView,
                childViewTagName: 'tr',
                el: $("#container-produtos").find('table tbody')
            });
            produto._rendered = true;
            produto.add(produtoModel);
        } else {
            $.pnotify({title: 'Atenção!', text: 'Este item já foi adicionado a lista de produtos.', type: 'warning'});
        }
    },
    events: {
        'click a.btn-success': 'adicionarProdutoLista'
    }
});

var ProdutosPesquisaView = Marionette.CollectionView.extend({
    tagName: 'tbody',
    childView: ProdutoPesquisaView
});

/**
 **************************** Lista de Produtos Campanha
 */

/**
 * @type View para CampanhaProduto @exp;Backbone@pro;View@call;extend
 */
var CampanhaProdutoView = Marionette.ItemView.extend({
    template: _.template(TemplatesCampanha.containerProdutos()),
    onRender: function () {
//        var variaveis = {};
//        var elementTemplate = _.template(TemplatesCampanha.containerProdutos(), variaveis);
//        this.el.innerHTML = elementTemplate;


        if (G2S.editModel && !G2S.editModel.get("bl_todosprodutos")) {
            var campanhaProdutoCollection = new VwCampanhaProdutoCollection();
            campanhaProdutoCollection.url += '/id_campanhacomercial/' + G2S.editModel.id;
            campanhaProdutoCollection.fetch({
                success: function () {
                    var campanhaView = new CollectionView({
                        collection: campanhaProdutoCollection,
                        childViewConstructor: ItemCapanhaProdutoView,
                        childViewTagName: 'tr',
                        el: $("#container-produtos").find('table tbody')
                    }).render();
                }
            });
        }
        return this;
    },
    showModal: function () {
        $("#modal-container").modal({show: true});
        $("#modal-container").find(".modal-header h3").html("Pesquisar Produto");
        var formView = new FormPesquisaProdutoView({
            el: $("#modal-container").find(".modal-body")
        });
        formView.render();
    },
    tooglePesquisa: function (e) {
        var checked = $(e.currentTarget).is(":checked");
        if (checked) {
            this.$el.find("button.pesquisar").attr('disabled', 'disabled');
            $("#container-produtos").find('table tbody').hide();
            $("#container-categorias").hide();
        } else {
            this.$el.find('button.pesquisar').removeAttr('disabled');
            $("#container-produtos").find('table tbody').show();
            $("#container-categorias").show();
        }
    },
    events: {
        'click button.pesquisar': 'showModal',
        'click input[type="checkbox"][name="bl_todosprodutos"]': 'tooglePesquisa'
    }
});

/**
 * @type View para renderizar itens do produto campanha @exp;Backbone@pro;View@call;extend
 */
var ItemCapanhaProdutoView = Backbone.View.extend({
    initialize: function () {
        this.model.bind('change', this.render);
    },
    render: function () {
        var variaveis = this.model.toJSON();
        this.el.innerHTML = _.template(TemplatesCampanha.itemProdutos(), variaveis);
        return this;
    },
    removeItem: function () {
        that = this;
        var model = new CampanhaProdutoModel();
        thatProdutoPesquisa;

        model.set({
            id_campanhacomercial: this.model.get('id_campanhacomercial'),
            id_produto: this.model.get('id_produto')
        });
        var id_produto = this.model.get('id_produto');
        model.url = '/api/campanha-produto/id_produto/' + this.model.get('id_produto') + '/id_campanhacomercial/' + this.model.get('id_campanhacomercial');
        bootbox.confirm("Tem certeza de que deseja apagar o registro?", function (result) {
            if (result) {
                if (that.model.get('id_campanhacomercial')) {
                    model.destroy({});
                }
                that.remove();
                //remove o produto do objeto que fica verificando se o produto já existe no modal de pesquisa de produto
                delete thatProdutoPesquisa.modelSet[id_produto];
            }
        });
    },
    events: {
        'click a.remove-item': 'removeItem'
    }
});

var PremioItemView = Marionette.ItemView.extend({
    model: Premio,
    url: function () {
        G2S.editModel ? '/campanha-comercial/retorna-premio/id_premio/' + G2S.editModel.get('id_premio') : ''
    }
})

var PremioComposite = Marionette.CompositeView.extend({
    template: '#premio-template',
    childView: PremioItemView,
    ui: {
        'bl_comprasAcima': '#bl_comprasAcima',
        'nu_comprasAcima': '#nu_comprasAcima',
        'bl_comprandoUnid': '#bl_comprandoUnid',
        'nu_comprandoUnid': '#nu_comprandoUnid',
        'bl_promocaoCumulativa': '#bl_promocaoCumulativa',
        'id_tipopremio': '#id_tipopremio',
        'select_campanha_cupom': '#select_campanha_cupom',
        'st_prefixoCupomPremio': '#st_prefixoCupomPremio',
        'tipo_desconto': '#tipo_desconto',
        'nu_desconto': '#nu_desconto'
    },
    initialize: function () {

    },
    onRender: function () {
        var that = this;
        this.formataCampos();
        this.renderProdutosPremio();
        this.populaSelectCampanha();
        if (collectionSelecionados.isEmpty()) {
            new ProdutoCollectionSelecionadosView({
                collection: collectionSelecionados,
                el: "#tabelaProdutosSelecionados"
            }).render();
        }
        new ProdutoCollectionView({
            collection: collectionProduto,
            el: '#tabelaProdutoCampanha'
        }).render();
        return this;
    },
    formataCampos: function () {
        this.ui.nu_comprasAcima.maskMoney({precision: 2, thousands: '', decimal: ",", allowNegative: false});
        this.ui.nu_comprandoUnid.maskMoney({precision: 0, thousands: '', allowNegative: false});
        this.ui.nu_desconto.maskMoney({precision: 2, thousands: '', decimal: ",", allowNegative: false});
    },
    selecionaTipoPremio: function () {
        if (this.ui.id_tipopremio.val() == 1) {
            $('#div_cupom').hide();
            $('#div_produtos').show();
        } else if ($('#id_tipopremio').val() == 2) {
            $('#div_produtos').hide();
            $('#div_cupom').show();
        }
    },
    renderProdutosPremio: function () {
        that = this;

        var produtoCampanha = new CampanhaProdutoView({
            el: that.$el.find('#container-produtos-premio')
        });
        produtoCampanha.render();
        this.selecionaTipoPremio();

    },
    populaSelectCampanha: function () {
        var Collection = Backbone.Collection.extend({
            model: VwMatricula
        });
        var view;
        var that = this;
        var collectionCampanha = new CampanhaCollection();
        collectionCampanha.url = '/campanha-comercial/retorna-cupons/entidade/' + G2S.loggedUser.get('id_entidade');
        collectionCampanha.fetch({
            beforeSend: function () {
                loading()
            },
            success: function () {
                view = new SelectView({
                    el: that.$el.find("select[name='select_campanha_cupom']"),      // Elemento da DOM
                    collection: collectionCampanha,      // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_campanhacomercial', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_campanhacomercial', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: that.model.get('id_cupomcampanha') ? that.model.get('id_cupomcampanha').id_campanhacomercial : null
                    }
                });
                view.render();
            },

            complete: function () {
                loaded();
                that.ui.select_campanha_cupom.select2({placeholder: "Selecione"});
            }
        });
    },
    verificaTipoDesconto: function () {
        this.ui.nu_desconto.val('');
        if (this.ui.tipo_desconto.val() != '') {
            this.ui.nu_desconto.removeAttr('disabled');
            if (this.ui.tipo_desconto.val() == 2) {
                this.ui.nu_desconto.maskMoney({
                    precision: 2,
                    decimal: ",",
                    thousands: ".",
                    allowNegative: false,
                    suffix: '%'
                });
            }
            else {
                this.ui.nu_desconto.maskMoney({
                    precision: 2,
                    decimal: ",",
                    thousands: ".",
                    allowNegative: false,
                    suffix: ''
                });
            }
        }
        else {
            this.ui.nu_desconto.attr('disabled');
        }
    },
    validaPremio: function () {

        if (this.ui.id_tipopremio.val() == 2 && !this.ui.select_campanha_cupom.val()) {
            $.pnotify({title: 'Atenção!', text: 'Selecione uma campanha!', type: 'warning'});
            $('#divSelectCampanhaPremio').addClass('control-group error');
            return false;
        } else {
            $('#divSelectCampanhaPremio').removeClass('control-group error');
        }

        if (this.ui.id_tipopremio.val() == 2 && this.ui.st_prefixoCupomPremio.val() == "") {
            $.pnotify({title: 'Atenção!', text: 'Insira um prefixo.', type: 'warning'});
            $('#divPrefixoCupom').addClass('control-group error');
            return false;
        } else {
            $('#divPrefixoCupom').removeClass('control-group error');
        }

        if (this.ui.id_tipopremio.val() == 2 && !this.ui.tipo_desconto.val()) {
            $.pnotify({title: 'Atenção!', text: 'Selecione um tipo e desconto!.', type: 'warning'});
            $('#divTipoDesconto').addClass('control-group error');
            return false;
        } else {
            $('#divTipoDesconto').removeClass('control-group error');
        }

        if (this.ui.id_tipopremio.val() == 2 && this.ui.tipo_desconto.val() == 2 && (formatValueToReal(this.ui.nu_desconto.val()) < 0 || formatValueToReal(this.ui.nu_desconto.val()) > 100 || this.ui.nu_desconto.val() == "")) {
            $.pnotify({title: 'Atenção!', text: 'O Valor do desconto deve ser entre 0 e 100%!', type: 'warning'});
            $('#divValorDescontoPremio').addClass('control-group error');
            return false;
        } else {
            $('#divValorDescontoPremio').removeClass('control-group error');
        }

        if (this.ui.id_tipopremio.val() == 2 && this.ui.tipo_desconto.val() == 1 && this.ui.nu_desconto.val() == "") {
            $.pnotify({title: 'Atenção!', text: 'Informe o valor do desconto!.', type: 'warning'});
            $('#divValorDescontoPremio').addClass('control-group error');
            return false;
        } else {
            $('#divValorDescontoPremio').removeClass('control-group error');
        }

        if (this.ui.id_tipopremio.val() == 1 && collectionSelecionados.length == 0) {
            $.pnotify({title: 'Atenção!', text: 'Selecione pelomenos um produto!.', type: 'warning'});
            return false;
        }


        return true;
    },
    salvarPremio: function () {
        var premio = [];
        if (!this.validaPremio()) {
            return false;
        } else {
            if (this.ui.id_tipopremio.val() == 1) {
                _.each(collectionSelecionados.models, function (model) {
                    premio.push(model.attributes.id_produto);
                });
            } else if (this.ui.id_tipopremio.val() == 2) {
                premio = {
                    'select_campanha_cupom': this.ui.select_campanha_cupom.val(),
                    'prefixo_cupom': this.ui.st_prefixoCupomPremio.val(),
                    'tipo_desconto': this.ui.tipo_desconto.val(),
                    'valor_desconto': formatValueToReal(this.ui.nu_desconto.val())
                }
            }
            var dados = {
                'id_campanhacomercial': G2S.editModel.id ? G2S.editModel.id : null,
                'id_premio': G2S.editModel.get('id_premio') ? G2S.editModel.get('id_premio') : null,
                'bl_comprasacima': this.ui.bl_comprasAcima.is(':checked'),
                'bl_comprasunidades': this.ui.bl_comprandoUnid.is(':checked'),
                'bl_promocaoCumulativa': this.ui.bl_promocaoCumulativa.is(':checked'),
                'nu_comprasacima': this.ui.nu_comprasAcima.val() ? formatValueToReal(this.ui.nu_comprasAcima.val()) : null,
                'nu_comprasunidades': this.ui.nu_comprandoUnid.val() ? formatValueToReal(this.ui.nu_comprandoUnid.val()) : null,
                'id_tipopremio': this.ui.id_tipopremio.val(),
                'premio': premio
            };

            $.ajax({
                url: '/campanha-comercial/salva-campanha-premio',
                dataType: 'json',
                data: {
                    data: dados
                },
                type: 'post',
                beforeSend: function () {
                    loading();
                },
                success: function (data) {
                    G2S.editModel.attributes.id_premio = data.id_premio;
                    $.pnotify({title: 'Sucesso!', text: 'Prêmio cadastrado com sucesso!', type: 'success'});
                },
                error: function (data) {
                    $.pnotify({title: 'Erro!', text: 'Erro ao cadastrar prêmio!', type: 'error'});
                    loaded();
                },
                complete: function () {
                    loaded();
                }
            });
        }

    },
    pesquisaProdutos: function (e) {
        var valorDigitado = e.currentTarget.value;
        if (valorDigitado.length >= 3) {
            clearTimeout(timer);
            timer = setTimeout((function () {
                collectionProduto.url = '/campanha-comercial/pesquisar-produto/st_produto/' + valorDigitado + '/id_entidade/' + G2S.loggedUser.get('id_entidade');
                collectionProduto.fetch({
                    beforeSend: function () {


                        loading();
                    },
                    complete: function () {
                        _.each(collectionSelecionados.models, function (model) {
                            var q = collectionProduto.findWhere({id_produto: model.get('id_produto')});
                            collectionProduto.remove(q);
                        });
                        loaded();
                    }

                });
            }), 500);
        } else {
            collectionProduto.reset();
            $('#tabelaProdutoCampanha').empty()
        }

    },
    events: {
        'change #id_tipopremio': 'selecionaTipoPremio',
        'change #tipo_desconto': 'verificaTipoDesconto',
        'click #btn_salvarPremio': 'salvarPremio',
        'keyup .input-search': 'pesquisaProdutos'
    }
});


/**
 **************************** Formulário de Campanha
 */

/**
 * @type View para CampanhaComercial @exp;Backbone@pro;View@call;extend
 */
var CampanhaView = Backbone.View.extend({
    botaoRelatorioModel: null,
    formaPagto: null,
    hasCampaign: false,
    initialize: function () {
        var that = this;
//        that.model.bind('change', this.render);
        that.botaoRelatorioModel = new ButtonModel({
            caption: "Gerar XLS de Cupons",
            url: "/campanha-comercial/xls-cupom/idCampanha/" + this.model.id,
            color: "inverse"
        });

    },
    cancelar: function () {
        window.location = '#pesquisa/CadastrarCampanha';
    },
    render: function () {

        var variaveis = this.model.toJSON();
        this.el.innerHTML = _.template($("#formulario-campanha-template").html(), variaveis);//renderiza o template do formulário no elemento
        //chama as funções pra renderizar os outros elementos no template
        this.renderTabCupom();
        this.renderFormaPagamento();
        this.renderCategoria();
        this.renderProdutos();

        if (this.model.toJSON()) {
            var dt_inicio = this.model.get('dt_inicio').split(" ");
            var dt_fim = this.model.get('dt_fim').split(" ");
            this.$el.find("#dt_inicio").val(dt_inicio[0]);
            this.$el.find("#dt_fim").val(dt_fim[0]);
            this.$el.find("#hora_ini").val(dt_inicio[1]);
            this.$el.find("#hora_fim").val(dt_fim[1]);
            var bl_disponibilidade = this.model.get('bl_disponibilidade') ? 1 : 0;
            //var bl_aplicardesconto = this.model.get('bl_aplicardesconto') ? 1 : 0;
            this.$el.find("#id_tipocampanha").val(this.model.get('id_tipocampanha'));
            this.$el.find('#id_finalidadecampanha').val(this.model.get('id_finalidadecampanha') ? this.model.get('id_finalidadecampanha') : 1);
            this.$el.find("#id_situacao").val(this.model.get('id_situacao'));
            this.$el.find("#id_categoriacampanha").val(this.model.get('id_categoriacampanha'));
            this.$el.find("#bl_disponibilidade").val(bl_disponibilidade);
            this.$el.find("#bl_aplicardesconto").prop('checked', this.model.get('bl_aplicardesconto'));
            this.$el.find("#id_tipodesconto").val(this.model.get('id_tipodesconto'));

            //verifica os boleanos para setar o valor nos inputs
            if (this.model.get("bl_todosprodutos")) {
                this.$el.find("#bl_todosprodutos").attr('checked', 'checked');
                this.$el.find('#container-categorias').hide();
            }

            this.toggleDesconto();
            this.tipoDesconto();
            this.renderTabPremio();
            this.toggleTipoCampanha();
            this.toggleFinalidade();
        }

        this.$el.find("#nu_diasprazo").spinner({
            step: 1,
            max: 9999,
            min: 1
        });

        this.trataTipoCampanha();
        return this;
    },
    renderTabPremio: function () {
        var that = this;
        if (G2S.editModel && this.model.get('id_categoriacampanha') == 4) {
            if (this.model.get('id_premio') != "") {
                var modelo = new Premio();
                modelo.url = '/campanha-comercial/retorna-premio/id_premio/' + G2S.editModel.get('id_premio');
                modelo.fetch({
                    success: function () {
                    },
                    complete: function () {
                        if (viewTabPremio) {
                            viewTabPremio.undelegateEvents();
                        }
                        viewTabPremio = new PremioComposite({
                            el: mainView.$el.find("#premio"),
                            model: modelo
                        });
                        viewTabPremio.render();
                        $('#li_premio').removeClass('hide');
                        if (modelo.get('bl_promocaocumulativa')) {
                            $('#bl_promocaoCumulativa').prop('checked', true);
                        } else {
                            $('#bl_promocaoCumulativa').prop('checked', false)
                        }
                    }
                });
            } else {
                if (viewTabPremio) {
                    viewTabPremio.undelegateEvents();
                }
                viewTabPremio = new PremioComposite({
                    el: mainView.$el.find("#premio"),
                    model: new Premio()
                });
                viewTabPremio.render();
                mainView.$el.find("#li_premio").removeClass('hide');
            }
        } else {
            $('#li_premio').addClass('hide')
        }

    },
    renderTabCupom: function () {
        var that = this;
        //Cria o botão de gerar relatório
        if (this.model.get('id_categoriacampanha') == 1) {
            G2S.botoesAuxiliares.add(that.botaoRelatorioModel);//Adiciona o botão de gerar relatorio
            $.ajax({
                url: '/campanha-comercial/tab-cupom',
                success: function (response) {
                    that.$el.parent().parent().find('#holder-container').html(response);
                    //Renderiza a view do formulário de campanha
                    that.$el.parent().parent().find(".nav-tabs").find('a[href="#cupom"]').parent('li').show();

                    var formCupomView = new FormCupomView({
                        el: that.$el.parent().find("#cupom"),
                        template: that.$el.parent().parent().find('#holder-container').find("#formulario-cupom-template").html()
                    });
                    formCupomView.render();
                }
            });
        } else {
            G2S.botoesAuxiliares.remove(that.botaoRelatorioModel);
            that.$el.parent().parent().find(".nav-tabs").find('a[href="#cupom"]').parent('li').hide();
        }
    },
    getViewFormaPagamento: function () {
        this.formaPagto = new FormaPagamentoMarionetteView({
            el: this.$el.find('#container-forma-pagamento-marionette')
        });
    },
    renderFormaPagamento: function () {

        var formaPagto = new FormaPagamentoMarionetteView({
            el: this.$el.find('#container-forma-pagamento-marionette')
        });

        if (this.model.get('id_categoriacampanha') == 2) {
            formaPagto.render();
        } else {
            this.$el.find('#container-tabs-vinculos').find('#container-forma-pagamento-marionette').remove();
            if (formaPagto) {
                formaPagto.remove();
            }
        }
    },
    renderCategoria: function () {
        var that = this;
        var categoriaCampanha = new CampanhaCategoriasView({
            el: that.$el.find('#container-categorias')
        });
        categoriaCampanha.render();
    },
    buscaCategorias: function () {
        var categorias = document.getElementById('container-categorias');
        return categorias.querySelectorAll('tbody tr');
    },
    buscaProdutos: function () {
        var produtos = document.getElementById('container-produtos');
        return produtos.querySelectorAll('tbody tr');
    },
    buscaTodosProdutos: function () {
        return $('#bl_todosprodutos').is(':checked');
    },
    renderProdutos: function () {
        that = this;
        var produtoCampanha = new CampanhaProdutoView({
            el: that.$el.find('#container-produtos')
        });
        produtoCampanha.render();
    },
    tipoDesconto: function () {
        //verifica qual tipo de desconto e renderiza o template correto
        var idDesconto = this.$el.find("#id_tipodesconto").val();//recupera o objeto
        var html = '';
        switch (idDesconto) {
            case '1':
                html = _.template(TemplatesCampanha.valorTipodesconto(), this.model.toJSON());
                break;
            case '2':
                html = _.template(TemplatesCampanha.percentualTipoDesconto(), this.model.toJSON());
                break;
        }

        this.$el.find("#containerInputsValor").show();
        this.$el.find("#containerInputsValor").html(html);
    },
    submitForm: function (e) {
        e.preventDefault();
        if (!this.trataTipoCampanha()) {
            return false;
        }
        var that = this;
        thatSalvar = this;
        var formEl = e.currentTarget;
        if (this.validaForm(formEl)) {
            var dt_inicio = $(formEl).find('#dt_inicio').val() ? $(formEl).find('#dt_inicio').val() + " " + $(formEl).find('#hora_ini').val() + ":00" : null;
            var dt_fim = $(formEl).find('#dt_fim').val() ? $(formEl).find('#dt_fim').val() + " " + $(formEl).find('#hora_fim').val() + ":00" : null;
            var input = document.getElementById('st_imagem');

            var formData = new FormData();

            formData.append('st_campanhacomercial', $(formEl).find('#st_campanhacomercial').val());
            formData.append('dt_fim', dt_fim);
            formData.append('dt_inicio', dt_inicio);
            formData.append('st_descricao', tinymce.activeEditor.getContent());
            formData.append('id_situacao', parseInt($(formEl).find('#id_situacao').val()));
            formData.append('id_tipodesconto', $(formEl).find('#id_tipodesconto').val() ? parseInt($(formEl).find('#id_tipodesconto').val()) : null);
            formData.append('id_tipocampanha', parseInt($(formEl).find('#id_tipocampanha').val()));
            formData.append('id_finalidadecampanha', $(formEl).find('#id_finalidadecampanha').val() ? $(formEl).find('#id_finalidadecampanha').val() : 1);
            formData.append('id_categoriacampanha', parseInt($(formEl).find('#id_categoriacampanha').val()));
            formData.append('bl_disponibilidade', $(formEl).find('#bl_disponibilidade').val() == 0 ? false : true);
            formData.append('bl_aplicardesconto', $(formEl).find('#bl_aplicardesconto').is(':checked'));
            formData.append('bl_todosprodutos', $(formEl).find('#bl_todosprodutos').is(':checked'));
            formData.append('bl_todasformas', $('#bl_todasformas').is(":checked"));
            formData.append('bl_ativo', true);
            formData.append('nu_disponibilidade', null);
            formData.append('nu_valordesconto', $(formEl).find('#nu_valordesconto').val() ? $(formEl).find('#nu_valordesconto').val() : null);
            formData.append('nu_diasprazo', $(formEl).find('#nu_diasprazo').val() ? $(formEl).find('#nu_diasprazo').val() : null);
            formData.append('bl_mobile', $(formEl).find('#bl_mobile').is(':checked') && $(formEl).find('#bl_mobile').is(":visible") ? $(formEl).find('#bl_mobile').val() : null);
            formData.append('bl_portaldoaluno', $(formEl).find('#bl_portaldoaluno').is(':checked') && $(formEl).find('#bl_portaldoaluno').is(":visible") ? $(formEl).find('#bl_portaldoaluno').val() : null);
            formData.append('id_campanhacomercial', $(formEl).find('#id_campanhacomercial').val());
            formData.append('st_link', $(formEl).find('#st_link').is(":visible") && $(formEl).find('#st_link').val() ? $(formEl).find('#st_link').val() : null);
            formData.append('st_imagem', input.files[0]);

            var exibicaoPortalApp = false;
            if (($(formEl).find('#bl_mobile').is(':checked') == true || $(formEl).find('#bl_portaldoaluno').is(':checked') == true)) {
                // $.pnotify({title: 'Atenção!', text: 'Selecione onde a promoção será exibida!', type: 'Warning'});
                exibicaoPortalApp = true;
                // return true;
            }

            if ($.trim($(formEl).find('#st_imagem').val()).length <= 0 &&
                $.trim($(formEl).find('#nome_imagem').val()) === "" &&
                exibicaoPortalApp) {
                $.pnotify({title: 'Atenção!', text: 'Anexe uma imagem!', type: 'Warning'});
                return true;
            }

            if ((exibicaoPortalApp && !this.validaLink($(formEl).find('#st_link').val(), exibicaoPortalApp)) ||
                (!exibicaoPortalApp && !this.validaLink($(formEl).find('#st_link').val(), exibicaoPortalApp))) {
                $.pnotify({title: 'Atençãoao!', text: 'Digite um link válido!', type: 'Warning'});
                return true;
            }

            if (this.buscaCategorias().length == 0 && (this.buscaProdutos().length == 0 && !this.buscaTodosProdutos()) && exibicaoPortalApp) {
                $.pnotify({title: 'Atenção!', text: 'Selecione uma categoria ou um produto!', type: 'Warning'});
                return true;
            }

            if (!this.verificaDescricao(tinymce.activeEditor.getContent())) {
                $.pnotify({title: 'Atenção!', text: 'Digite uma descrição válida!', type: 'Warning'});
                return true;
            }

            $.ajax({
                url: 'api/campanha-comercial',
                method: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    loading();
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    G2S.editModel = that.model;
                    G2S.editModel.id = response.id;
                    thatFormaPagamentoMarionette.salvarFormaPagamento();
                    that.salvaProdutos();
                    that.salvaCategorias();
                    that.renderTabCupom();
                    $('#id_campanhacomercial').val(response.id);
                    window.location = '#/pesquisa/CadastrarCampanha';
                    $.pnotify({title: 'Sucesso!', text: 'Campanha salva com sucesso!', type: 'success'});
                    setTimeout(function () {
                        loaded();
                    }, 100);
                },
                complete: function () {
                    that.renderTabPremio();
                },
                error: function (response) {
                    loaded();
                    var resposta = JSON.parse(response.responseText);
                    $.pnotify({
                        title: 'Erro!',
                        text: 'Houve um problema ao salvar Campanha, tente novamente! ',
                        type: 'error'
                    });
                    $.pnotify(resposta);
                }
            });
        }
        return false;
    },
    verificaDescricao: function (texto) {
        var div = $('<div>');
        div.append(texto);
        if (div.find('p').text() == "") {
            return false;
        } else {
            return true;
        }
    },
    validaLink: function (link, exibicaoMarcada) {
        if (link == "" && !exibicaoMarcada) {
            return true
        }
        if (!link.match(/[0-9a-z]\.[a-z]/) || link.match(/\s/)) {
            return false;
        } else {
            if ((link.match(/\//) && !link.match(/\/[a-z]/)) || link.match(/^\.[a-z]/) || !link.match(/^[0-9a-z]/)) {
                return false;
            } else {
                return true;
            }
        }
    },
    pegaNomeImagem: function () {
        $('#nome_imagem').val($('#st_imagem').val().replace(/C:\\fakepath\\/i, ''));
    },
    retornaCategoriasSelecionadas: function () {
        var arrCategorias = [];
        this.$el.find("#container-categorias table tbody tr").each(function (i, elemTr) {
            arrCategorias[i] = $(elemTr).find('td input[name="id_categoria"]').val();
        });
        return arrCategorias;
    },
    salvaCategorias: function () {
        var arrCategorias = [];
        if (G2S.editModel) {
            this.$el.find("#container-categorias table tbody tr").each(function (i, elemTr) {
                var id_categoria = $(elemTr).find('td input[name="id_categoria"]').val();
                arrCategorias[i] = {
                    id_categoria: id_categoria,
                    id_campanhacomercial: G2S.editModel.id
                };
            });

            if (arrCategorias.length) {
                $.ajax({
                    url: '/campanha-comercial/salva-campanha-categoria',
                    dataTupe: 'json',
                    data: {
                        data: arrCategorias
                    },
                    type: 'post',
                    success: function (data) {
                        if (data.type == 'success') {
                            $.pnotify({title: 'Sucesso!', text: data.text, type: 'success'});
                        } else {
                            $.pnotify({title: 'Erro!', text: data.text, type: 'error'});
                        }
                    }
                });
            }
        }

    },
    retornaProdutosSelecionados: function () {
        var arrProdutos = [];
        this.$el.find("#container-produtos table tbody tr").each(function (i, elemTr) {
            arrProdutos[i] = $(elemTr).find('td input[name="id_produto"]').val();
        });
        return arrProdutos;
    },
    salvaProdutos: function () {
        var arrProdutos = [];
        if (G2S.editModel) {
            if (this.$el.find("input#bl_todosprodutos").is(":checked")) {
                this.$el.find("#container-produtos table tbody").empty();
            }
            this.$el.find("#container-produtos table tbody tr").each(function (i, elemTr) {
                var id_produto = $(elemTr).find('td input[name="id_produto"]').val();
                arrProdutos[i] = {
                    id_produto: id_produto,
                    id_campanhacomercial: G2S.editModel.id
                };
            });
            if (arrProdutos.length) {
                $.ajax({
                    url: '/campanha-comercial/salva-array-produtos',
                    dataTupe: 'json',
                    data: {
                        data: arrProdutos
                    },
                    type: 'post',
                    success: function (data) {
                        if (data.type == 'success') {
                            $.pnotify({title: 'Sucesso!', text: data.text, type: 'success'});
                        } else {
                            $.pnotify({title: 'Erro!', text: data.text, type: 'error'});
                        }
                    }
                });
            }
        }
    },
    salvarFormasPagamento: function () {
        var arrFormas = [];
        //if (G2S.editModel) {$("#container-forma-pagamento-marionette").is(":visible")
        if (this.$el.find("#container-forma-pagamento-marionette").is(":visible")) {
            this.$el.find("#forma-pagamento-marionette-view table tbody tr").each(function (i, elemTr) {
                var id_formapagamento = $(elemTr).find('td input[name="id_formapagamento"]').val();
                arrFormas[i] = {
                    id_formapagamento: id_formapagamento,
                    id_campanhacomercial: G2S.editModel.id
                };
            });

            if (arrFormas.length) {
                $.ajax({
                    url: '/campanha-comercial/salva-array-formas-pagamento',
                    dataType: 'json',
                    data: {
                        'data': arrFormas
                    },
                    type: 'post',
                    success: function (data) {
                        if (data.type == 'success') {
                            $.pnotify({title: 'Sucesso!', text: data.text, type: 'success'});
                        } else {
                            $.pnotify({title: 'Erro!', text: data.text, type: 'error'});
                        }
                    }
                });
            }
        } else {
            return true;
        }

        //}
    },
    validaForm: function (form) {

        var valid = true;//inicia a variavel como true
        $(form).find('*[required="required"]').each(function (i, elem) {
            var valor = $(elem).val();//atribui o valor
            //varifica se não tem valor
            if (!valor) {
                valid = false; //seta a variavel como false
            }
        });

        if (this.hasCampaign && this.$el.find('input#bl_aplicardesconto').is(':checked')) {
            valid = false;
            $.pnotify({
                type: 'error',
                title: 'Atenção!',
                text: 'Você nao pode definir esta campanha como Pontualidade Padrão.'
            });
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Já existe uma Campanha de Pontualidade marcada como Padrão: ' + this.hasCampaign.st_campanha
            });
        }

        if(
            !$('#bl_todosprodutos').is(':checked')
            && ([3,4].indexOf(parseInt(this.$el.find("#id_finalidadecampanha").val())) > -1)
            && (!this.retornaProdutosSelecionados().length && !this.retornaCategoriasSelecionadas().length)
        ){
            valid = false;
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Você precisa informar algum Produto ou Categoria'
            });
        }

        return valid;
    },
    toggleDesconto: function () {
        var tipoDesconto = this.$el.find("#id_categoriacampanha").val();

        if (tipoDesconto == 2) {
            $('#container-tabs-vinculos').prepend("<div class='span4 well well-small' id='container-forma-pagamento-marionette'></div>");
            var formaPagto = new FormaPagamentoMarionetteView({
                el: this.$el.find('#container-forma-pagamento-marionette')
            });
            formaPagto.render();
            this.$el.find('#descontos-campanha').show();
            // this.$el.find('#divExibicao').show();
            // this.$el.find('#divLinkImagem').show();
            // this.$el.find('#st_link').prop('required', true);
            this.$el.find('.prazo').hide();

        } else if (tipoDesconto == 3) {
            this.$el.find('#descontos-campanha').hide();
            // this.$el.find('#divExibicao').hide();
            // this.$el.find('#divLinkImagem').hide();
            // this.$el.find('#st_link').prop('required', false);
            this.$el.find('.prazo').show();
            if (this.formaPagto) {
                this.formaPagto.remove();
            }
        }
        else {
            this.$el.find('#container-tabs-vinculos').find('#container-forma-pagamento-marionette').remove();
            if (this.formaPagto) {
                this.formaPagto.remove();
            }
            this.$el.find('.prazo').hide();
            $('#li_premio').addClass('hide')
            this.$el.find('#descontos-campanha').hide();
            // this.$el.find('#divExibicao').hide();
            // this.$el.find('#divLinkImagem').hide();
            // this.$el.find('#st_link').prop('required', false);
        }
        this.toggleTipoCampanha();
    },
    toggleTipoCampanha: function () {
        var tipo = this.$el.find('select#id_tipocampanha').val();
        if (parseInt(tipo) == 3) {
            this.$el.find('#container-pontualidade-padrao').show();
            this.$el.find('#container-forma-pagamento-marionette').hide();
            this.$el.find('#container-categorias').hide();
            this.$el.find('#container-produtos').hide();
        } else {
            this.$el.find('#container-pontualidade-padrao').hide();
            this.$el.find('#container-forma-pagamento-marionette').show();
            this.$el.find('#container-categorias').show();
            this.$el.find('#container-produtos').show();
        }

    },
    checkCampanhasPontualidade: function () {
        var that = this;
        if (this.$el.find('input#bl_aplicardesconto').is(":checked")) {
            $.ajax({
                url: 'campanha-comercial/verifica-campanhas-pontualidade',
                data: {
                    id_campanha: this.model.get('id')
                },
                dataType: 'json',
                beforeSend: function () {
                    loading()
                },
                complete: function () {
                    loaded();
                },
                success: function (data) {

                    if (typeof data.st_campanha != 'undefined') {
                        that.hasCampaign = data;
                        $.pnotify({
                            title: 'Atenção!',
                            text: "Já existe uma Campanha de Pontualidade marcada como Padrão: " + data.st_campanha,
                            type: 'warning'
                        });

                    } else {
                        that.hasCampaign = false;
                    }

                }
            });
        }
    },
    toggleFinalidade: function () {

        /* Regra: input de finalidade só deve ficar habilitado caso
        * categoria seja Promoção e tipo Venda. (GII-7636) */

        var categoria = this.$el.find('#id_categoriacampanha').val() == 2;
        var tipo = this.$el.find('#id_tipocampanha').val() == 2;

        var inputFinalidade = this.$el.find('#id_finalidadecampanha');

        if (categoria && tipo) {
            inputFinalidade.prop('disabled', false);
        } else {
            //Volta para finalidade venda.
            inputFinalidade.val(1);
            inputFinalidade.prop('disabled', true);
        }
    },
    trataTipoCampanha: function () {
        this.$el.find('#bl_mobile').prop('disabled', false);
        this.$el.find('#bl_portaldoaluno').prop('disabled', false);
        if (this.$el.find("#container-produtos table tbody tr").length > 0 && this.$el.find('#id_tipocampanha').val() == 3) {
            this.$el.find('#id_tipocampanha').val(2).change();
            $.pnotify({
                title: 'Atenção!',
                text: 'Exclua os produtos antes de cadastrar uma campanha do tipo pontualidade!',
                type: 'warning'
            });
            return false;
        }
        if (this.$el.find('#id_tipocampanha').val() == 3) {
            this.$el.find('#bl_mobile').prop('checked', false).prop('disabled', true);
            this.$el.find('#bl_portaldoaluno').prop('checked', false).prop('disabled', true);
            this.$el.find('#bl_todosprodutos').prop('checked', false);

        }
        return true;
    },
    events: {
        'change select#id_categoriacampanha': function () {
            this.toggleDesconto();
            this.toggleFinalidade();
        },
        'change select#id_tipocampanha': function () {
            this.trataTipoCampanha();
            this.toggleTipoCampanha();
            this.toggleFinalidade();
        },
        'change select#id_tipodesconto': 'tipoDesconto',
        'change input#bl_aplicardesconto': 'checkCampanhasPontualidade',
        'submit form#formCampanha': 'submitForm',
        'click #btnCancelar': 'cancelar',
        'change #st_imagem': 'pegaNomeImagem'
    }
});


/**
 **************************** Formulário de Cupom
 */

/**
 * @type View para Cupom @exp;Backbone@pro;View@call;extend
 */
var FormCupomView = Backbone.View.extend({
    initialize: function (options) {
        //initialize
        this.template = options.template;
    },
    render: function () {
        var variaveis = G2S.editModel ? G2S.editModel.toJSON() : {};
        this.el.innerHTML = _.template(this.template, variaveis);
        this.renderTableCupom();
        this.$el.find("#nu_cupons").spinner({
            step: 1,
            max: 9999,
            min: 1
        });
        this.$el.find(".ui-spinner-button").addClass('hide');
        return this;
    },
    renderTableCupom: function () {
        that = this;
        var cupons = new CupomTableView({
            el: $(that.el).find("#lista-cupom")
        });
        cupons.render();
    },
    salvaCupom: function () {
        that = this;
        var cupomModel = new Cupom();
        var qtdCupons = this.$el.find("#nu_cupons").val() ? this.$el.find("#nu_cupons").val() : 1;
        var dtInicio = that.$el.find("#dt_inicio").val() + " " + that.$el.find("#hora_ini").val() + ":00";
        var dtFim = that.$el.find("#dt_fim").val() + " " + that.$el.find("#hora_fim").val() + ":00";
        cupomModel.set({
            st_prefixo: $("#st_prefixo").val(),
            st_complemento: $("#st_complemento").val(),
            nu_desconto: $("#nu_desconto").val(),
            dt_inicio: dtInicio,
            dt_fim: dtFim,
            bl_ativacao: $("#bl_ativacao").is(":checked"),
            bl_unico: $("#bl_unico").is(":checked"),
            id_tipodesconto: parseInt(that.$el.find("#id_tipodesconto").val()),
            id_campanhacomercial: G2S.editModel.id,
            nu_quantidadecupons: qtdCupons
        });


        //valida campos
        if (cupomModel.get('id_tipodesconto') == 2) {
            if (cupomModel.get('nu_desconto') > 100) {
                $.pnotify({
                    title: 'Atenção!',
                    text: 'A porcentagem de desconto não pode exceder a 100%!',
                    type: 'warning'
                });
                return false;
            }
        }

//        if (qtdCupons) {
//            for (var i = 0; i < qtdCupons; i++) {
        cupomModel.save(null, {
            beforeSend: function () {
                loading();
            },
            success: function (model, response) { //                loaded();
                $.pnotify({title: 'Sucesso!', text: 'Cupom salvo com sucesso!', type: 'success'});
                that.render();
            },
            complete: function () {
                loaded();
            },
            error: function (model, response) {
                loaded();
                var response = JSON.parse(response.responseText);
                $.pnotify(response);
                $.pnotify({
                    title: 'Erro!',
                    text: 'Houve um problema ao salvar Cupom, tente novamente!',
                    type: 'error'
                });

            }
        });
//            }
//        }
        return false;
    },
    toggleComplemento: function (elem) {
        var value = $(elem.currentTarget).val();
        if (value == 1) {
            this.$el.find("#st_complemento").val("").removeAttr('required').attr('disabled', 'disabled');
            this.$el.find("#nu_cupons").removeAttr('disabled');
            this.$el.find(".ui-spinner-button").removeClass('hide');
        } else {
            this.$el.find(".ui-spinner-button").addClass('hide');
            this.$el.find("#st_complemento").attr('required', 'required').removeAttr('disabled');
            this.$el.find("#nu_cupons").attr('disabled', 'disabled');
        }
    },
    geraXls: function () {
        window.open('/campanha-comercial/xls-cupom/idCampanha/' + G2S.editModel.id, '_blank');
        return false;
    },
    events: {
        'submit form#formCupom': 'salvaCupom',
        'change select#st_tipocodigo': 'toggleComplemento',
        'click #btnGerarRelatorio': 'geraXls'
    }
});

var CupomModalRelatorio = Backbone.View.extend({
    initialize: function () {
        this.$el.modal({show: true, backdrop: 'static'});
        this.$el.find(".modal-header h3").html("Gerar Relatório");
    },
    render: function () {
        var tempEle = _.template('<p>Deseja gerar um relatório XLS com os Cupons vinculados a esta Campanha Comercial?<p><div class="modal-footer"><a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Não</a><a href="#" class="btn btn-primary">Sim,gerar relatório.</a></div>');
        this.$el.find('.modal-body').html(tempEle);
        return this;
    },
    gerarRelatorio: function () {
        window.open('/campanha-comercial/xls-cupom/idCampanha/' + G2S.editModel.id, '_blank');
        this.$el.modal('hide');
        return false;
    },
    events: {
        'click .btn-primary': 'gerarRelatorio'
    }
});


/**
 * @type View para lista de cupons @exp;Backbone@pro;View@call;extend
 */
var CupomTableView = Backbone.View.extend({
    render: function () {
        this.el.innerHTML = _.template(TemplatesCupom.tabelaCupons(), {});

        if (G2S.editModel) {
            this.populaCuponsLista();
        }
        return this;
    },
    populaCuponsLista: function () {
        var $elemento = this.$el;
        var collection = new VwCupomCampanhaCollection();
        collection.url += '/id_campanhacomercial/' + G2S.editModel.id;
        collection.fetch({
            beforeSend: function () {
                $elemento.find('table tbody').html('<tr class="load-cupons"><td colspan="7" style="text-align:center">Carregando...</td></tr>')
            },
            success: function () {
                new CollectionView({
                    collection: collection,
                    childViewConstructor: CuponsView,
                    childViewTagName: 'tr',
                    el: $elemento.find('table tbody')
                }).render();
            },
            complete: function () {
                $elemento.find('table tbody tr.load-cupons').remove();
            }
        })
    }
});
/**
 * @type View para cupom @exp;Backbone@pro;View@call;extend
 */
var CuponsView = Backbone.View.extend({
    initiliaze: function () {
        this.model.bind('change', this.render);
    },
    render: function () {
        var variaveis = this.model.toJSON();
        if (this.model.get('bl_ativacao')) {
            variaveis.ativacao = 'checked';
        } else {
            variaveis.ativacao = '';
        }
        if (this.model.get('bl_unico')) {
            variaveis.unico = 'checked';
        } else {
            variaveis.unico = '';
        }

        variaveis.dt_inicio = this.formataDataPtBr(this.model.get('dt_inicio'));
        variaveis.dt_fim = this.formataDataPtBr(this.model.get('dt_fim'));

        this.model.set('dt_inicio', variaveis.dt_inicio);
        this.model.set('dt_fim', variaveis.dt_fim);

        var elemento = _.template(TemplatesCupom.itemTabelaCupomEdit(), variaveis);
        this.el.innerHTML = elemento;
        return this;
    },
    formataDataPtBr: function (date) {
        var arrDateTime = date.split(" ");

        var arrDate = arrDateTime[0].split("-");
        var hora = arrDateTime[1];
        var data = arrDate[2] + "/" + arrDate[1] + "/" + arrDate[0];
        return data + " " + hora;
    },
    toggleBlAtivacao: function (e) {
        that = this;
        var $elem = $(e.currentTarget);
        that.model.set('bl_ativacao', $elem.is(':checked'));
        that.model.save(null, {
            beforeSend: function () {
                loading();
            },
            success: function (model, response) {
                $.pnotify({title: 'Sucesso!', text: 'Ativação alterada com sucesso!', type: 'success'});
                that.render();
                loaded();
            },
            error: function (model, response) {
                $.pnotify({
                    title: 'Erro!',
                    text: 'Houve um problema ao salvar Ativação Cupom, tente novamente!',
                    type: 'error'
                });
            }
        });
    },
    toggleBlUnico: function (e) {
        that = this;
        var $elem = $(e.currentTarget);
        that.model.set('bl_unico', $elem.is(':checked'));
        that.model.save(null, {
            beforeSend: function () {
                loading();
            },
            success: function (model, response) {
                $.pnotify({title: 'Sucesso!', text: 'Utilização do Cupom alterada com sucesso!', type: 'success'});
                that.render();
                loaded();
            },
            error: function (model, response) {
                $.pnotify({
                    title: 'Erro!',
                    text: 'Houve um problema ao salvar Utilização do Cupom, tente novamente!',
                    type: 'error'
                });
            }
        });
    },
    events: {
        'click input[name="bl_ativacao"]': 'toggleBlAtivacao',
        'click input[name="bl_unico"]': 'toggleBlUnico'
    }
});

var viewTabPremio = null;

var MainView = Marionette.ItemView.extend({
    template: "#main-template",
    onRender: function () {
        var that = this;
        var campanhaModel = new CampanhaComercial();
        if (CampanhaModel) {
            campanhaModel = new CampanhaComercial(CampanhaModel);
            G2S.editModel = campanhaModel;
        }
        var viewTabCampanha = new CampanhaView({
            model: campanhaModel,
            el: that.$el.find("#campanha")
        });

        viewTabCampanha.render();
        loaded();
    }
});
var mainView = new MainView();
G2S.layout.content.show(mainView);

$(function () {
    $('*[data-toggle]').tooltip();
});

loadTinyMCE('st_descricao', 10, true);
var SituacaoCollection = Backbone.Collection.extend({
    url: '/api/situacao/?st_tabela=tb_motivo',
    model: Situacao,
    initialize: function () {
        //this.fetch()
    }
});

var LayoutMotivo = Marionette.LayoutView.extend({
    template: '#template-dados-motivo',
    tagName: 'div',
    className: 'container-fluid',
    regions: {
        dadosMotivo: '#cadastro-motivo-wrapper',
        dadosArvore: '#vinculo-entidade-motivo-wrapper',
        botoes: '#botoes-wrapper'
    },
    events: {
        'click #btn-salvar': 'salvar'
    },
    ui: {
        'st_motivo': '#st_tituloMotivo',
        'id_situacao': '#id_situacao'
    },
    onShow: function () {
        var model = new Motivo();
        if (G2S.editModel != null) {
            model.set(G2S.editModel);
        }
        this.dadosMotivo.show(new ViewDadosMotivo({model: model}));
        this.dadosArvore.show(new ViewDadosArvore());
        this.botoes.show(new ViewBotoes());
    },
    salvar: function () {
        var that = this;

        var checked_ids = [];
        $("#entidades").jstree("get_checked", null, true).each
        (function () {
            checked_ids.push(this.id);
        });

        var unchecked_ids = [];
        $("#entidades").jstree("get_unchecked", null, true).each
        (function () {
            unchecked_ids.push(this.id);
        });

        if (G2S.editModel) {
            parametros = {
                'id_motivo': G2S.editModel.id,
                'st_motivo': $('#st_tituloMotivo').val(),
                'id_situacao': $('#id_situacao').val(),
                'entidades_checked': checked_ids,
                'entidades_unchecked': unchecked_ids
            }
        } else {
            parametros = {
                'st_motivo': $('#st_tituloMotivo').val(),
                'id_situacao': $('#id_situacao').val(),
                'entidades_checked': checked_ids,
                'entidades_unchecked': unchecked_ids
            }
        }

        $.ajax({
            url: 'motivo/save',
            type: 'POST',
            dataType: 'json',
            data: parametros,
            beforeSend: function () {
                loading();
            },
            success: function () {
                loaded();
                $.pnotify({title: 'Sucesso', text: 'Cadastro realizado com sucesso', type: 'success'});
            },
            complete: function () {
                loaded();
            },
            error: function (data) {
                $.pnotify({title: 'Erro', text: 'Operação não realizada!', type: 'danger'});
            }
        })
    }
});


var ViewDadosMotivo = Marionette.ItemView.extend({
    template: '#cadastro-dados-motivo',
    tagName: 'div',
    onShow: function () {
        this.populaSelectSituacao();
    },
    populaSelectSituacao: function () {
        loading();
        var that = this;
        var collectionSituacao = new SituacaoCollection();
        collectionSituacao.fetch({
            success: function () {
                var view = new SelectView({
                    el: that.$el.find("select[name='id_situacao']"),         // Elemento da DOM
                    collection: collectionSituacao,      // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_situacao', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_situacao', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: that.model.get('id_situacao')       // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();
                loaded();
            }
        });

    }
})

var ViewDadosArvore = Marionette.ItemView.extend({
    template: '#vinculo-entidade-tree',
    tagName: 'div',
    onShow: function () {
        this.carregaEntidade();
    },
    carregaEntidade: function () {
        loaded();
        $("#entidades").jstree({
            "html_data": {
                "ajax": {
                    "url": "util/arvore-entidade",
                    "data": function (n) {
                        return {id: n.attr ? n.attr("id") : 0};
                    }
                }
            },
            "plugins": ["themes", "html_data", "checkbox", "sort", "ui"]
        })
            .
            bind("loaded.jstree", function (event, data) {
                if (G2S.editModel.id) {
                    $.ajax({
                        url: G2S.editModel ? "/api/motivo-entidade/?id_motivo=" + G2S.editModel.id : null,
                        type: 'get',
                        dataType: 'json',
                        success: function (data) {
                            $.each(data, function (idx, item) {
                                if (item.bl_ativo == true) {
                                    $.jstree._reference("#entidades").check_node('li#' + item.id_entidade);
                                } else {
                                    $.jstree._reference("#entidades").uncheck_node('li#' + item.id_entidade);
                                }
                            })
                        }
                    });
                }
            });
    }
});

var ViewBotoes = Marionette.ItemView.extend({
    template: '#botoes',
    tagName: 'div'
});


G2S.show(new LayoutMotivo());
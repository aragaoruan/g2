/**
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @date 2015-05-14
 */
/**
 * Variaveis que faz cache das collections dos Comboboxes
 */
var comboArea,
    comboNivel;

var comboTipoLivro,
    comboColecaoLivro;

var produtoValorDelete = [];

var TextoSistemaCollection = Backbone.Collection.extend({
    url: "/api/produto-declaracao/",
    model: TextoSistema,
    initialize: function () {
    }
});

var FormaDisponibilizacaoCollection = Backbone.Collection.extend({
    url: "/api/forma-disponibilizacao/",
    model: FormaDisponibilizacao,
    initialize: function () {
    }
});

/**
 * CONFIGURACOES TELA DE EDICAO
 * Layout Base / Layout Principal da Tela
 */
// MODEL
var ProdutoModel = Backbone.Model.extend({
    defaults: {
        // OBRIGATORIOS
        id_produto: null,
        st_produto: "",
        bl_ativo: 1,
        bl_destaque: 0,
        bl_mostrarpreco: 0,
        bl_todascampanhas: 0,
        bl_todasentidades: 0,
        bl_todasformas: 0,
        bl_unico: 0,
        nu_gratuito: 0,
        // OPICIONAIS
        dt_fimpontosprom: null,
        dt_iniciopontosprom: null,
        nu_estoque: null,
        nu_pontos: null,
        nu_pontospromocional: null,
        st_descricao: null,
        st_observacoes: null,
        st_subtitulo: null
    },
    idAttribute: "id_produto",
    url: function () {
        return "/api/produto/" + (this.id || "");
    }
});

var ProdutoLayout = Marionette.LayoutView.extend({
    template: "#produto-template",
    model: new ProdutoModel(),
    regions: {
        //FORMS
        form_tipoproduto: "#form_tipoproduto",
        produtoValorRegion: "#produto-valor-container",
        id_produtovalor_promocional: "#id_produtovalor_promocional",
        id_entidade: "#id_entidade",
        //COLLECTIONS REGIONS
        id_categoria: "#id_categoria",
        id_areaconhecimento: "#id_areaconhecimento"
    },
    ui: {
        //TEXTAREAS:
        st_descricao: "textarea[name='st_descricao']",
        st_observacoes: "textarea[name='st_observacoes']",
        st_disciplina: "textarea[name='st_disciplina']",
        //COMBOBOXES
        id_tipoproduto: "#id_tipoproduto",
        id_modelovenda: "#id_modelovenda",
        id_situacao: "#id_situacao",
        btn_salvarbasico: "#salvarbasico",
        st_codigoavaliacao: "#st_codigoavaliacao"
    },
    collections: {
        id_categoria: {},
        id_areaconhecimento: {},
        // ARRAYS PARA COMBOBOEX
        id_tipoproduto: null,
        id_modelovenda: null,
        id_situacao: null,
        st_codigoavaliacao: null

    },
    initialize: function () {
        this.model.idAttribute = "id_produto";
        this.model.set("id_produto", Number(window.location.href.split("/").pop()) || "");
    },
    onRender: function () {
        var that = this;

        this.$el.find(".maskMoney").maskMoney({
            allowEmpty: true,
            allowZero: true,
            defaultZero: false,
            decimal: ".",
            thousands: ""
        });

        this.collections.id_categoria = new CategoriaComposite();
        this.collections.id_areaconhecimento = new AreaComposite();
        this.id_categoria.show(this.collections.id_categoria);
        this.id_areaconhecimento.show(this.collections.id_areaconhecimento);

        if (that.model.get("id_produto")) {
            // PRODUTO VALOR NORMAL
            // ProdutoValorStart.collection = new ProdutoValorCollection();
            ProdutoValorStart.collection.fetch({
                reset: true,
                data: {
                    id_produto: that.model.get("id_produto"),
                    id_tipoprodutovalor: TIPO_PRODUTO_VALOR.NORMAL
                },
                complete: function () {
                    // ProdutoValorStart.model.set("id_tipoprodutovalor", TIPO_PRODUTO_VALOR.NORMAL);
                    that.produtoValorRegion.show(ProdutoValorStart);
                }
            });

            // PRODUTO VALOR PROMOCIONAL
            ProdutoValorPromocionalStart.model = new ProdutoValorModel();
            ProdutoValorPromocionalStart.model.fetch({
                data: {
                    id_produto: that.model.get("id_produto"),
                    id_tipoprodutovalor: TIPO_PRODUTO_VALOR.PROMOCIONAL
                },
                complete: function () {
                    ProdutoValorPromocionalStart.model.set("id_tipoprodutovalor", TIPO_PRODUTO_VALOR.PROMOCIONAL);
                    that.id_produtovalor_promocional.show(ProdutoValorPromocionalStart);
                }
            });
        } else {
            that.produtoValorRegion.show(ProdutoValorStart);
            that.id_produtovalor_promocional.show(ProdutoValorPromocionalStart);
        }

        //JS TREE ENTIDADES / ORGANIZACOES
        this.id_entidade.show(ProdutoEntidadeStart);
        this.loadComboboxes();

        return this;
    },
    onShow: function () {
        loadTinyMCE("st_descricao,st_observacoes");
        return this;
    },
    loadComboboxes: function () {
        var that = this;

        //MODELO DE VENDA
        ComboboxView({
            el: that.ui.id_modelovenda,
            collection: that.collections.id_modelovenda,
            url: "/api/modelo-venda/",
            optionLabel: "st_modelovenda",
            optionSelectedId: that.model.get("id_modelovenda"),
            emptyOption: "[Selecione]",
            model: Backbone.Model.extend({
                idAttribute: "id_modelovenda"
            })
        }, function (el, composite) {
            if (!that.collections.id_modelovenda) {
                that.collections.id_modelovenda = composite;
            }

            that.verificaModeloVenda();
        });

        //SITUACAO
        ComboboxView({
            el: that.ui.id_situacao,
            collection: that.collections.id_situacao,
            url: "/api/situacao/?st_tabela=tb_produto",
            optionLabel: "st_situacao",
            optionSelectedId: that.model.get("id_situacao"),
            emptyOption: "[Selecione]",
            model: Backbone.Model.extend({
                idAttribute: "id_situacao"
            })
        }, function (el, composite) {
            if (!that.collections.id_situacao) {
                that.collections.id_situacao = composite;
            }
        });


        //TIPO DE PRODUTO
        ComboboxView({
            el: that.ui.id_tipoproduto,
            collection: that.collections.id_tipoproduto,
            url: "/api/tipo-produto/",
            optionLabel: "st_tipoproduto",
            optionSelectedId: that.model.get("id_tipoproduto"),
            emptyOption: "[Selecione]",
            model: Backbone.Model.extend({
                idAttribute: "id_tipoproduto"
            })
        }, function (el, composite) {
            if (!that.collections.id_tipoproduto) {
                that.collections.id_tipoproduto = composite;
            }

            // Renderizar o cadastro de acordo com o tipo
            if (typeof el.val() !== "undefined" && el.val()) {
                that.ui.id_tipoproduto.prop("disabled", true);
                that.renderTipoProduto();
            }
        });

    },
    events: {
        "click .actionBack": "actionBack",
        "click #salvarbasico": "actionSave",
        "click #valorpromocional": "actionSave",
        "change #id_tipoproduto": "renderTipoProduto",
        "change @ui.id_modelovenda": "verificaModeloVenda"
    },
    actionBack: function (e) {
        e.preventDefault();
        G2S.router.navigate("#/pesquisa/CadastrarProduto", {trigger: true});
    },
    actionSave: function (e) {
        e.preventDefault();

        this.ui.btn_salvarbasico.addClass("disabled");

        var that = this;
        var values = Backbone.Syphon.serialize(this);

        values.st_observacoes = tinymce.get("st_observacoes").getContent();
        values.st_descricao = tinymce.get("st_descricao").getContent();

        // INICIO VALIDACOES
        var id_tipoproduto = Number(that.model.get("id_tipoproduto"));
        var errorMsg = null;

        if (id_tipoproduto == TIPO_PRODUTO.PROJETO_PEDAGOGICO && !ProdutoProjetoStart.model.get("id_projetopedagogico")) {
            //SE FOR PROJETO PEDAGOGICO
            errorMsg = "Selecione um projeto pedagógico antes de continuar.";
        } else if (id_tipoproduto == TIPO_PRODUTO.LIVRO && !ProdutoLivroStart.model.get("id_livro")) {
            //SE FOR LIVRO
            errorMsg = "Selecione um livro antes de continuar.";
        } else if (id_tipoproduto == TIPO_PRODUTO.SIMULADO && !that.model.get("st_codigoavaliacao")) {
            //SE FOR SIMULADO
            errorMsg = "Digite o Código Avaliação antes de continuar.";
        }

        // Não pode possuir valor sem data de início
        if ($("#id_produtovalor input[name='dt_inicio']").filter(function () {
                return !$(this).val();
            }).length) {
            errorMsg = "É necessário informar a data de início de todos os valores.";
        }

        var id_modelovenda = +ProdutoStart.ui.id_modelovenda.val() || +ProdutoStart.model.get("id_modelovenda");

        // Não pode salvar Produto com Valor 0 na Graduação
        if (id_modelovenda === MODELO_VENDA.CREDITO) {
            this.$el.find("#produto-valor-container input[name='nu_valormensal']").each(function () {
                if (+$(this).val() <= 0) {
                    errorMsg = "O produto não deve possuir valores zero.";
                }
            });
        }

        if (errorMsg) {
            $.pnotify({
                type: "warning",
                title: "Seleção Obrigatória",
                text: errorMsg
            });

            this.ui.btn_salvarbasico.removeClass("disabled");

            return false;
        }
        // FIM VALIDACOES

        // Salvar o Produto (tb_produto)
        // Se salvar corretamente, salva suas relacoes
        this.model.save(values, {
            beforeSend: loading,
            success: function (model, response) {
                $.ajaxSetup({async: false});

                that.model.set(response);

                //Salvar entidades
                ProdutoEntidadeStart.actionSave();

                // Remover Valores solicitados
                if (produtoValorDelete.length) {
                    produtoValorDelete.forEach(function (model) {
                        model.destroy();
                    });
                }

                //Salvar produto valor (normal)
                ProdutoValorStart.collection.each(function (model) {
                    var child = ProdutoValorStart.children.findByModel(model);
                    child.model.set("id_tipoprodutovalor", TIPO_PRODUTO_VALOR.NORMAL);
                    child.actionSave();
                });

                //Salvar produto valor (promocional)
                ProdutoValorPromocionalStart.model.set("id_tipoprodutovalor", TIPO_PRODUTO_VALOR.PROMOCIONAL);
                ProdutoValorPromocionalStart.actionSave();

                //Salvar categorias selecionadas
                that.collections.id_categoria.actionSave();

                //Salvar areas selecionadas
                that.collections.id_areaconhecimento.actionSave();

                switch (that.model.get("id_tipoproduto")) {
                    case TIPO_PRODUTO.PROJETO_PEDAGOGICO:
                        //Salvar Projeto Pedagogico, se o Tipo de Produto = 1
                        ProdutoProjetoStart.actionSave();
                        break;
                    case TIPO_PRODUTO.DECLARACOES:
                        //Salvar Projeto Declaração, se o Tipo de Produto = 3
                        ProdutoDeclaracaoStart.actionSave();
                        break;
                    case TIPO_PRODUTO.LIVRO:
                        //Salvar Livro, se o Tipo de Produto = 6
                        ProdutoLivroStart.actionSave();
                        break;
                    case TIPO_PRODUTO.COMBO:
                        //Savar Combo, se o Tipo de Produto = 7
                        ProdutoComboStart.actionSave();
                        break;
                }

                $.ajaxSetup({async: true});

                that.ui.btn_salvarbasico.removeClass("disabled");

                $.pnotify({
                    type: "success",
                    title: "Sucesso",
                    text: "Os dados foram salvos com sucesso"
                });
            },
            error: function (model, response) {
                $.pnotify(response.responseJSON);
                that.ui.btn_salvarbasico.toggleClass("disabled");
            },
            complete: loaded
        });
    },
    validate: function () {
        var msg = [];

        this.$el.find("[required]").each(function () {
            var $this = $(this);
            if (!$this.val()) {
                msg.push($this.closest("label").text().trim());
            }
        });

        if (msg) {
            $.pnotify({
                type: "warning",
                title: "Atenção",
                text: "Preencha os campos " + msg.join(", ") + " antes de continuar."
            });
            return false;
        }

        return true;
    },
    renderTipoProduto: function () {
        var that = this;

        var value = Number(this.ui.id_tipoproduto.val());
        // Esconde/Mostra o Modelo de Venda
        if (value == TIPO_PRODUTO.PROJETO_PEDAGOGICO) {
            this.ui.id_modelovenda.val(this.model.get("id_modelovenda")).closest("label").show();
        } else {
            this.ui.id_modelovenda.val(1).closest("label").hide(); // 1 = Padrao
        }

        // Esconde/Mostra o Codigo Avaliação
        if (value == TIPO_PRODUTO.SIMULADO) {
            this.ui.st_codigoavaliacao.val(this.model.get("st_codigoavaliacao")).closest("label").show();
        } else {
            this.ui.st_codigoavaliacao.val(1).closest("label").hide(); // 1 = Padrao
        }

        // Renderiza o formular do tipo de produto
        that.form_tipoproduto.reset();
        switch (value) {
            case TIPO_PRODUTO.PROJETO_PEDAGOGICO: {
                ProdutoProjetoStart = new ProdutoProjetoView();
                // PROJETO PEDAGOGICO
                ProdutoProjetoStart.model.is_editing = false;
                if (this.model.get("id_produto")) {
                    ProdutoProjetoStart.model.fetch({
                        data: {
                            id_produto: that.model.get("id_produto")
                        },
                        complete: function () {
                            if (ProdutoProjetoStart.model.get("id_projetopedagogico")) {
                                ProdutoProjetoStart.model.is_editing = true;
                            }

                            that.form_tipoproduto.show(ProdutoProjetoStart);
                        }
                    });
                } else {
                    that.form_tipoproduto.show(ProdutoProjetoStart);
                }
                break;
            }
            case TIPO_PRODUTO.DECLARACOES: {
                ProdutoDeclaracaoStart = new ProdutoDeclaracaoView();

                // DECLARAÇÕES
                if (this.model.get("id_produto")) {
                    ProdutoDeclaracaoStart.model.toggleEdit();
                }

                ProdutoDeclaracaoStart.model.fetch({
                    data: {
                        id_produto: that.model.get("id_produto")
                    },
                    complete: function () {
                        that.form_tipoproduto.show(ProdutoDeclaracaoStart);
                    }
                });
                break;
            }
            case TIPO_PRODUTO.LIVRO: {
                ProdutoLivroStart = new ProdutoLivroView();

                // LIVRO
                if (this.model.get("id_produto")) {
                    ProdutoLivroStart.model.toggleEdit();
                }

                ProdutoLivroStart.model.fetch({
                    data: {
                        id_produto: that.model.get("id_produto")
                    },
                    complete: function () {
                        that.form_tipoproduto.show(ProdutoLivroStart);
                    }
                });
                break;
            }
            case TIPO_PRODUTO.COMBO: {
                ProdutoComboStart = new ProdutoComboComposite();
                // COMBO
                that.form_tipoproduto.show(ProdutoComboStart);
                break;
            }
            case TIPO_PRODUTO.TAXA: {
                // TAXA
                // Nao faz nada, apenas permite o cadastro
                break;
            }
            case TIPO_PRODUTO.SIMULADO: {
                // Simulados
                // Nao faz nada, apenas permite o cadastro
                break;
            }
            case TIPO_PRODUTO.VIDEO: {
                // Vídeos
                // Nao faz nada, apenas permite o cadastro
                break;
            }
            default: {
                that.form_tipoproduto.reset();
                $.pnotify({
                    type: "warning",
                    title: "Aviso",
                    text: "Este Tipo de Produto não foi implementado."
                });
                this.ui.id_tipoproduto.val("");
                break;
            }
        }
    },
    verificaModeloVenda: function () {
        if (this.produtoValorRegion.currentView && ProdutoValorStart.children) {
            ProdutoValorStart.collection.each(function (model) {
                var child = ProdutoValorStart.children.findByModel(model);
                child.alternaValorCredito();
            });
        }

        if (this.id_produtovalor_promocional.currentView) {
            this.id_produtovalor_promocional.currentView.alternaValorCredito();
        }
    }
});

/**
 * Configuracoes dos Valores
 */
var ProdutoValorModel = Backbone.Model.extend({
    defaults: {
        id_produtovalor: null,
        id_produto: null,
        nu_valor: null,
        nu_valormensal: null,
        dt_inicio: "",
        dt_termino: ""
    },
    idAttribute: "id_produtovalor",
    url: function () {
        return "/api/produto-valor/" + (this.id || "");
    }
});

var ProdutoValorView = Marionette.ItemView.extend({
    template: "#produto-valor-view",
    ui: {
        nu_valor: "[name='nu_valor']",
        nu_valormensal: "[name='nu_valormensal']",
        dt_inicio: "[name='dt_inicio']",
        dt_termino: "[name='dt_termino']",
        btnEdit: ".btnEdit",
        btnRemove: ".btnRemove"
    },
    events: {
        "changeDate @ui.dt_inicio": "onChangeDtInicio",
        "changeDate @ui.dt_termino": "onChangeDates",
        "click @ui.btnEdit": "actionEdit",
        "click @ui.btnRemove": "actionRemove"
    },
    onRender: function () {
        var that = this;

        this.$el.find(".maskMoney").maskMoney({
            allowEmpty: true,
            allowZero: true,
            defaultZero: false,
            decimal: ".",
            thousands: ""
        });

        VerificarPerfilPermissao(58, 214, true).done(function (response) {
            if (+response.length === 1) {
                if (that.model.collection) {
                    that.ui.btnRemove.toggleClass("hide", +that.model.collection.length === 1);
                    that.ui.btnEdit.toggleClass("hide", false);
                }
            }
        });

        this.alternaValorCredito();
    },
    onShow: function () {
        this.setDatepicker();
        this.desabilitarCampos();
    },
    setDatepicker: function () {
        var ultimoTermino = null;
        var $prev = this.$el.prev();

        if ($prev.length) {
            var $child = ProdutoValorStart.children.find(function (item) {
                return item.el === $prev.get(0);
            });

            var dtVal = $child.ui.dt_termino.val();

            if (dtVal) {
                var split = dtVal.split("/");
                ultimoTermino = new Date(split[2], split[1] - 1, +split[0] + 1);
            }
        }

        this.ui.dt_inicio.datepicker({
            format: "dd/mm/yyyy",
            language: "pt-BR",
            autoclose: true,
            todayHighlight: true,
            clearBtn: true,
            todayBtn: true,
            startDate: ultimoTermino,
            endDate: this.model.get("dt_termino")
        });

        this.ui.dt_termino.datepicker({
            format: "dd/mm/yyyy",
            language: "pt-BR",
            autoclose: true,
            todayHighlight: true,
            clearBtn: true,
            todayBtn: true,
            startDate: this.model.get("dt_inicio")
        });
    },
    desabilitarCampos: function () {
        // Se o valor já foi salvo, ele não pode ser alterado
        if (this.model.get("id_produtovalor")) {
            this.ui.dt_inicio.prop("disabled", true);
            this.ui.dt_termino.prop("disabled", this.ui.dt_termino.val());
            this.ui.nu_valor.prop("disabled", true);
            this.ui.nu_valormensal.prop("disabled", true);
        }
    },
    actionEdit: function () {
        this.ui.dt_inicio.prop("disabled", false);
        this.ui.dt_termino.prop("disabled", false);
        this.ui.nu_valor.prop("disabled", false);
        this.ui.nu_valormensal.prop("disabled", false);
    },
    actionRemove: function () {
        bootbox.confirm("Deseja remover este valor?", _.bind(function (response) {
            if (response) {
                // Adicionar a lista de DELETE para remover do BANCO ao salvar os dados da tela
                if (this.model.get("id_produtovalor")) {
                    produtoValorDelete.push(this.model);
                }

                this.model.collection.remove(this.model);
            }
        }, this));
    },
    actionSave: function () {
        var that = this;

        var values = Backbone.Syphon.serialize(this);
        values.id_produto = ProdutoStart.model.get("id_produto");

        if (values.dt_inicio) {
            return this.model.save(values, {
                success: function (model, response) {
                    that.model.set(response);
                    that.desabilitarCampos();
                }
            });
        }
    },
    onChangeDtInicio: function () {
        this.onChangeDates();

        var nuValor = this.ui.nu_valor.val();
        var dtInicio = this.ui.dt_inicio.val().replace(/[^\d\s]/gi, "");

        if (nuValor || dtInicio) {
            this.ui.nu_valor.attr("required", "required");
        } else {
            this.ui.nu_valor.removeAttr("required");
        }
    },
    onChangeDates: function () {
        var dtInicio = this.ui.dt_inicio.val();
        var dtTermino = this.ui.dt_termino.val();

        if (!dtInicio) {
            this.ui.dt_termino.prop("disabled", true).val("");
        } else {
            this.ui.dt_termino.prop("disabled", false);
        }

        setTimeout(_.bind(function () {
            this.ui.dt_termino.datepicker("setStartDate", dtInicio);
            this.ui.dt_inicio.datepicker("setEndDate", dtTermino);

            var $next = this.$el.next();

            if ($next.length) {
                var $child = ProdutoValorStart.children.find(function (item) {
                    return item.el === $next.get(0);
                });

                var newStartDate = null;

                if (dtTermino) {
                    var split = dtTermino.split("/");
                    newStartDate = new Date(split[2], split[1] - 1, +split[0] + 1);
                }

                $child.ui.dt_inicio.datepicker("setStartDate", newStartDate);

                var nuDtTermino = Number(dtTermino.split("/").reverse().join(""));
                var nuChildInicio = Number($child.ui.dt_inicio.val().split("/").reverse().join(""));

                if (nuChildInicio <= nuDtTermino) {
                    $child.ui.dt_inicio.val("").trigger("changeDate");
                    $child.ui.dt_termino.val("").trigger("changeDate");

                    this.$el.nextAll().find("input[name='dt_inicio'], input[name='dt_termino']").val("");
                }
            }
        }, this), 100);
    },
    alternaValorCredito: function () {
        var el_valor = this.ui.nu_valor;
        var el_valormensal = this.ui.nu_valormensal;

        var id_modelovenda = +ProdutoStart.ui.id_modelovenda.val() || +ProdutoStart.model.get("id_modelovenda");

        if (id_modelovenda === MODELO_VENDA.CREDITO) {
            el_valor.val("0.00").closest("label").addClass("hide");
            el_valormensal.closest("label").removeClass("offset1").css("marginLeft", "0").find("span").html("Valor por Crédito");
        } else {
            el_valor.closest("label").removeClass("hide");
            el_valormensal.closest("label").addClass("offset1").removeAttr("style").find("span").html("Valor Mensal");
        }
    }
});

var ProdutoValorCollection = Backbone.Collection.extend({
    model: ProdutoValorModel,
    url: "/api/produto-valor"
});

var ProdutoValorComposite = Marionette.CompositeView.extend({
    template: "#produto-valor-composite",
    collection: new ProdutoValorCollection(),
    childView: ProdutoValorView,
    childViewContainer: "#id_produtovalor",
    ui: {
        btnAdd: ".actionAdd",
        btnEdut: "btnEdit",
        btnRemove: ".btnRemove",
        inputsValorMensal: "[name='nu_valormensal']",
        inputsDtInicio: "[name='dt_inicio']",
        inputsDtTermino: "[name='dt_termino']"
    },
    events: {
        "click @ui.btnAdd": "actionAdd"
    },
    collectionEvents: {
        add: "toggleButtons",
        remove: "toggleButtons"
    },
    toggleButtons: function () {
        var that = this;
        VerificarPerfilPermissao(58, 214, true).done(function (response) {
            if (+response.length === 1) {
                that.bindUIElements();
                that.ui.btnRemove.toggleClass("hide", that.collection.length === 1);
                that.ui.btnEdit.toggleClass("hide", false);
            }
        });
    },
    actionAdd: function () {
        this.bindUIElements();

        var errors = [];

        // Não pode existir valores zero
        var id_modelovenda = +ProdutoStart.ui.id_modelovenda.val() || +ProdutoStart.model.get("id_modelovenda");

        if (id_modelovenda === MODELO_VENDA.CREDITO) {
            var sem_valor = this.ui.inputsValorMensal.filter(function () {
                return +$(this).val() <= 0;
            }).length;

            if (sem_valor) {
                errors.push("Para adicionar um novo valor é necessário que o último seja maior que zero.");
            }
        }

        // Não pode existir valores sem data de término
        var sem_termino = this.ui.inputsDtTermino.filter(function () {
            return !$(this).val();
        }).length;

        if (sem_termino) {
            errors.push("Para adicionar um novo valor é preciso determinar a data de término do último valor.");
        }

        if (errors.length) {
            $.pnotify({
                type: "warning",
                title: "Aviso",
                text: errors.shift()
            });

            return false;
        }

        this.collection.add(new ProdutoValorModel());
    }
});


/**
 * Configuracoes do Produto Entidade (Organizacao)
 */
var ProdutoEntidade = Marionette.ItemView.extend({
    template: "#produto-organizacao-template",
    ui: {
        //JS TREE
        organizacao: "#organizacao-content",
        bl_todasentidades: "[name='bl_todasentidades']"
    },
    onRender: function () {
        var that = this;

        this.ui.bl_todasentidades.prop("checked", ProdutoStart.model.get("bl_todasentidades")).trigger("change");

        // ARVORE DE ENTIDADES
        this.ui.organizacao
            .jstree({
                html_data: {
                    ajax: {
                        url: "util/arvore-entidade"
                    }
                },
                plugins: ["themes", "html_data", "checkbox", "sort", "ui"]
            })
            .on("loaded.jstree", function () {
                that.ui.organizacao.jstree("btn_check_all");

                var id_produto = ProdutoStart.model.get("id_produto");
                if (id_produto) {
                    $.getJSON("/api/produto-entidade/?id_produto=" + id_produto, function (response) {
                        if (response) {
                            for (var i in response) {
                                if (response[i].id_entidade) {
                                    that.ui.organizacao.jstree("check_node", "li#" + response[i].id_entidade);
                                }
                            }
                        }
                    });
                }
            });
    },
    events: {
        "change [name='bl_todasentidades']": "toggleOrganizacao"
    },
    toggleOrganizacao: function (e) {
        // Mostra / esconde arvore de entidades
        this.ui.organizacao.toggle(!$(e.target).prop("checked"));
    },
    actionSave: function () {
        var that = this;
        var id_entidade = [];
        that.ui.organizacao.jstree("get_checked", null, true).each(function () {
            id_entidade.push(this.id);
        });
        return $.ajax({
            url: "/api/produto-entidade/",
            type: "post",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({
                id_entidade: id_entidade,
                id_produto: ProdutoStart.model.get("id_produto"),
                id_produtoentidade: null
            })
        });
    }
});


/**
 * CONFIGURACOES COLLECTION CATEGORIA
 */
var CategoriaView,
    CategoriaEmpty,
    CategoriaCollection,
    CategoriaComposite;

// ITEM VIEW
CategoriaView = Marionette.ItemView.extend({
    template: _.template("<input type='checkbox' name='id_categoria' value='<%= id_categoria %>' /> <%= st_categoria %>"),
    tagName: "label",
    className: "checkbox",
    events: {
        "change input": function (e) {
            this.model.set("checked", $(e.target).prop("checked"));
        }
    }
});

// EMPTY ITEM VIEW
CategoriaEmpty = Marionette.ItemView.extend({
    template: _.template("Nenhuma categoria encontrada."),
    tagName: "div"
});

// COLLECTION
CategoriaCollection = Backbone.Collection.extend({
    //model: TableModel,
    url: "/api/categoria/?bl_ativo=true"
});

// COMPOSITE VIEW
CategoriaComposite = Marionette.CompositeView.extend({
    template: _.template("<div></div>"),
    collection: new CategoriaCollection(),
    childView: CategoriaView,
    emptyView: CategoriaEmpty,
    childViewContainer: "div",
    onRender: function () {
        var that = this;
        this.collection.fetch({
            reset: true,
            complete: function () {
                that.getChecked();
            }
        });
    },
    getChecked: function () {
        var that = this;
        var id_produto = ProdutoStart.model.get("id_produto");

        if (id_produto && this.collection.models) {
            $.ajax({
                url: "/api/vw-categoria-produto/?id_produto=" + id_produto,
                dataType: "json",
                success: function (response) {
                    for (var i in response) {
                        if (response[i].id_categoria) {
                            var id_categoria = response[i].id_categoria;
                            that.$el.find("input:checkbox[value='" + id_categoria + "']").prop("checked", true).trigger("change");
                        }
                    }
                }
            });
        }
    },
    actionSave: function () {
        var models = this.collection.where({checked: true});
        var id_categoria = [];
        if (models) {
            for (var i in models) {
                if (models[i].get("id_categoria")) {
                    id_categoria.push(models[i].get("id_categoria"));
                }
            }
        }
        return $.ajax({
            url: "/api/categoria-produto/",
            type: "post",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({
                id_categoria: id_categoria,
                id_produto: ProdutoStart.model.get("id_produto")
            })
        });
    }
});


/**
 * CONFIGURACOES COLLECTION AREA DE CONHECIMENTO
 */
var AreaView,
    AreaEmpty,
    AreaCollection,
    AreaComposite;

// ITEM VIEW
AreaView = Marionette.ItemView.extend({
    template: _.template("<input type='checkbox' name='id_areaconhecimento' value='<%= id_areaconhecimento %>' /> <%= st_areaconhecimento %>"),
    tagName: "label",
    className: "checkbox",
    events: {
        "change input": function (e) {
            this.model.set("checked", $(e.target).prop("checked"));
        }
    }
});

// EMPTY ITEM VIEW
AreaEmpty = Marionette.ItemView.extend({
    template: _.template("Nenhuma área encontrada."),
    tagName: "div"
});

// COLLECTION
AreaCollection = Backbone.Collection.extend({
    //model: TableModel,
    url: "/api/area-conhecimento/?bl_ativo=true&backbone=true"
});

// COMPOSITE VIEW
AreaComposite = Marionette.CompositeView.extend({
    template: _.template("<div></div>"),
    collection: new AreaCollection(),
    childView: AreaView,
    emptyView: AreaEmpty,
    childViewContainer: "div",
    onRender: function () {
        var that = this;
        this.collection.fetch({
            reset: true,
            complete: function () {
                that.getChecked();
            }
        });
    },
    getChecked: function () {
        var that = this;
        var id_produto = ProdutoStart.model.get("id_produto");

        if (id_produto && this.collection.models) {
            $.ajax({
                url: "/api/vw-produto-area/id_produto/" + id_produto,
                dataType: "json",
                success: function (response) {
                    for (var i in response) {
                        if (response[i].id_areaconhecimento) {
                            var id_areaconhecimento = response[i].id_areaconhecimento;
                            that.$el.find("input:checkbox[value='" + id_areaconhecimento + "']").prop("checked", true).trigger("change");
                        }
                    }
                }
            });
        }
    },
    actionSave: function () {
        var models = this.collection.where({checked: true});
        var id_areaconhecimento = [];
        if (models) {
            for (var i in models) {
                if (models[i].get("id_areaconhecimento")) {
                    id_areaconhecimento.push(models[i].get("id_areaconhecimento"));
                }
            }
        }
        return $.ajax({
            url: "/api/produto-area/",
            type: "post",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({
                id_areaconhecimento: id_areaconhecimento,
                id_produto: ProdutoStart.model.get("id_produto"),
                id_produtoarea: null
            })
        });
    }
});


/**
 * Configuracoes para quando selecionar o Tipo de Produto
 * @type {ProdutoProjetoView}
 */

/**
 * Configuracoes do Tipo de Produto = Projeto Pedagogico (1)
 */


/**
 * Collection para Regra de Contrato
 */
var RegraContratoCollection = Backbone.Collection.extend({
    model: ContratoRegra,
    url: "/api/contrato-regra"
});

var ProdutoProjetoModel = Backbone.Model.extend({
    idAttribute: "id_produtoprojetopedagogico",
    defaults: {
        id_produtoprojetopedagogico: null,
        id_produto: null,
        id_projetopedagogico: null,
        st_projetopedagogico: null,
        st_disciplina: null,
        nu_cargahoraria: null,
        bl_indeterminado: false,
        id_entidade: null,
        nu_tempoacesso: null,
        id_turma: null,
        id_contratoregra: null
    },
    url: function () {
        return "/api/produto-projeto-pedagogico/" + (this.id || "");
    },
    parse: function (response) {
        return response[0] || response;
    },
    toggleEdit: function () {
        this.is_editing = !this.is_editing;
    }
});

var ProdutoProjetoView = Marionette.ItemView.extend({
    model: new ProdutoProjetoModel(),
    template: "#produto-projeto-view",
    ui: {
        nu_tempoacesso: "[name='nu_tempoacesso']",
        id_turma: "[name='id_turma']",
        //COMBOBOXES PESQUISA
        id_areaconhecimento: "[name='id_areaconhecimento']",
        id_nivelensino: "[name='id_nivelensino']",
        st_disciplina: "#st_disciplina",
        id_regracontratoprojeto: "#id_regracontratoprojeto"
    },
    onBeforeRender: function () {
        if (this.model.is_editing) {
            this.template = "#produto-projeto-edit";
        } else {
            this.template = "#produto-projeto-view";
        }
    },
    onRender: function () {
        var that = this;

        if (that.model.get("id_projetopedagogico")) {
            // Busca dados do projeto pedagogico
            $.ajax({
                url: "/api/projeto-pedagogico/" + that.model.get("id_projetopedagogico"),
                success: function (response) {
                    if (response && response.id_projetopedagogico) {
                        that.model.set("st_projetopedagogico", response.st_projetopedagogico);
                        that.$el.find(".st_projetopedagogico").html(response.st_projetopedagogico);
                    }
                }
            });

            // Busca tabela de disciplinas caso nao possua
            if (!that.model.get("st_disciplina")) {
                $.getJSON("/api/vw-modulo-disciplina/?id_projetopedagogico=" + that.model.get("id_projetopedagogico"), function (response) {
                    if (response && response.length) {

                        var htmlDisciplinas = "<table><thead><tr><th>Disciplina</th><th>Professores</th></tr></thead><tbody>";

                        for (var i in response) {
                            htmlDisciplinas += "<tr><td>" + response[i].st_disciplina + "</td><td></td></tr>";
                        }

                        htmlDisciplinas += "</tbody></table>";
                        that.model.set("st_disciplina", htmlDisciplinas);
                        that.render().onShow();
                    }
                });
            }
        }

        if (this.model.is_editing) {
            this.renderEdit();
        } else {
            this.renderView();
        }
    },
    onShow: function () {
        loadTinyMCE("st_disciplina,st_descricao,st_observacoes");
        return this;
    },
    events: {
        "change [name='bl_indeterminado']": "toggleTempoAcesso",
        "click .actionSearchProjeto": "actionSearchProjeto",
        "click .actionRemoveProjeto": "actionRemoveProjeto"
    },
    toggleTempoAcesso: function (e) {
        this.ui.nu_tempoacesso.prop("disabled", $(e.target).prop("checked"));
        if (this.ui.nu_tempoacesso.is(":disabled")) {
            this.ui.nu_tempoacesso.val(0);
        }
    },
    renderView: function () {
        var that = this;

        this.populaSelectContratoRegra();

        //COMBOBOX AREA CONHECIMENTO
        ComboboxView({
            el: that.ui.id_areaconhecimento,
            url: "/api/area-conhecimento/",
            collection: comboArea,
            optionLabel: "st_areaconhecimento",
            emptyOption: "Todos",
            model: Backbone.Model.extend({
                defaults: {
                    id_areaconhecimento: null,
                    st_areaconhecimento: null
                },
                idAttribute: "id_areaconhecimento"
            })
        }, function (el, composite) {
            if (!comboArea) {
                comboArea = composite.collection.toJSON();
            }
        });

        //COMBOBOX NIVEL DE ENSINO
        ComboboxView({
            el: that.ui.id_nivelensino,
            url: "/api/nivel-ensino/",
            collection: comboNivel,
            optionLabel: "st_nivelensino",
            emptyOption: "Todos",
            model: Backbone.Model.extend({
                defaults: {
                    id_nivelensino: null,
                    st_nivelensino: null
                },
                idAttribute: "id_nivelensino"
            })
        }, function (el, composite) {
            if (!comboNivel) {
                comboNivel = composite.collection.toJSON();
            }
        });
    },
    renderEdit: function () {
        var that = this;

        this.populaSelectContratoRegra();

        //COMBOBOX TURMA
        ComboboxView({
            el: that.ui.id_turma,
            url: "/api/turmas-projeto/?id_projetopedagogico=" + that.model.get("id_projetopedagogico"),
            optionLabel: "st_turma",
            optionLabelConstruct: function (model) {
                return model.get("id_turma") + " - " + model.get("st_turma");
            },
            optionSelectedId: that.model.get("id_turma"),
            emptyOption: "[Selecione]",
            model: Backbone.Model.extend({
                defaults: {
                    id_turma: null,
                    st_turma: null
                },
                idAttribute: "id_turma"
            })
        });
    },
    actionSearchProjeto: function () {
        var that = this;
        var values = Backbone.Syphon.serialize(this);

        /**********************
         * CONFIGURACOES DA BUSCA
         **********************/
        var SearchModel,
            SearchView,
            SearchEmpty,
            SearchCollection,
            SearchComposite,
            SearchIni;

        // MODEL
        SearchModel = Backbone.Model.extend({
            defaults: {
                id_projetopedagogico: null,
                st_projetopedagogico: ""
            },
            idAttribute: "id_projetopedagogico"
        });

        // ITEM VIEW
        SearchView = Marionette.ItemView.extend({
            model: new SearchModel(),
            template: "#search-projeto-view",
            tagName: "tr",
            events: {
                "click .actionSelectProjeto": "actionSelectProjeto"
            },
            actionSelectProjeto: function () {
                that.model.set(this.model.toJSON());
                that.model.toggleEdit();
                that.render();
            }
        });

        // EMPTY ITEM VIEW
        SearchEmpty = Marionette.ItemView.extend({
            template: _.template("<td colspan='4'>Nenhum registro encontrado.</td>"),
            tagName: "tr"
        });

        // COLLECTION
        SearchCollection = Backbone.Collection.extend({
            model: SearchModel,
            url: "/vw-area-projeto-nivel-ensino/pesquisar/?" + $.param(values),
            parse: function (response) {
                return (response.tipo == 1 ? response.mensagem : []);
            }
        });

        // COMPOSITE VIEW
        SearchComposite = Marionette.CompositeView.extend({
            template: "#search-projeto-template",
            collection: new SearchCollection(),
            childView: SearchView,
            emptyView: SearchEmpty,
            childViewContainer: "tbody",
            onRender: function () {
                this.collection.fetch({
                    reset: true,
                    complete: loaded
                });
            },
            events: {
                "click .actionCloseSearch": "actionCloseSearch"
            },
            actionCloseSearch: function () {
                this.$el.html("");
            }
        });
        loading();
        this.$el.find("#search-tipo-produto").html(new SearchComposite().render().$el);
    },
    actionRemoveProjeto: function () {
        var that = this;

        $.each(this.model.attributes, function (key, model) {
            that.model.set(key, null);
        });

        this.model.toggleEdit();
        this.render();
    },
    actionSave: function () {
        var that = this;

        var values = Backbone.Syphon.serialize(this);
        values.id_produto = ProdutoStart.model.get("id_produto");
        values.id_contratoregra = this.ui.id_regracontratoprojeto.val() ? this.ui.id_regracontratoprojeto.val() : null;
        values.st_disciplina = tinymce.get("st_disciplina").getContent();

        if (this.model.get("id_projetopedagogico")) {
            return this.model.save(values, {
                success: function (model, response) {
                    that.model.set(response);
                },
                error: function (model, response) {
                    response = response.responseJSON;
                    $.pnotify({
                        type: "error",
                        title: "Projeto Pedagógico",
                        text: response.text
                    });
                }
            });
        }

        return false;
    },
    populaSelectContratoRegra: function () {
        var that = this;
        var contratoRegraCol = new RegraContratoCollection();
        contratoRegraCol.fetch({
            beforeSend: loading,
            complete: function () {
                loaded();
            },
            error: function () {
                $.pnotify({
                    type: "warning",
                    title: "Erro!",
                    text: "Erro ao carregar processos seletivos."
                });
            },
            success: function () {
                new SelectView({
                    el: that.ui.id_regracontratoprojeto,
                    collection: contratoRegraCol,
                    childViewOptions: {
                        optionLabel: "st_contratoregra",
                        optionValue: "id_contratoregra",
                        optionSelected: that.model.get("id_contratoregra") ? that.model.get("id_contratoregra") : null
                    }
                }).render();
            }
        });
    }
});


/**
 * Configuracoes do Tipo de Produto = Livro (6)
 */
var ProdutoLivroModel = Backbone.Model.extend({
    defaults: {
        id_produto: null,
        id_livro: null,
        id_entidade: null,
        st_livro: null
    },
    idAttribute: "id_livro",
    url: function () {
        return "/api/produto-livro/" + (this.id || "");
    },
    parse: function (response) {
        return response[0] || response;
    },
    toggleEdit: function () {
        this.is_editing = !this.is_editing;
    }
});

var ProdutoLivroView = Marionette.ItemView.extend({
    model: new ProdutoLivroModel(),
    template: "#produto-livro-view",
    ui: {
        //COMBOBOXES PESQUISA
        id_tipolivro: "[name='id_tipolivro']",
        id_livrocolecao: "[name='id_livrocolecao']"
    },
    onBeforeRender: function () {
        if (this.model.is_editing) {
            this.template = "#produto-livro-edit";
        } else {
            this.template = "#produto-livro-view";
        }
    },
    onRender: function () {
        if (this.model.is_editing) {
            this.renderEdit();
        } else {
            this.renderView();
        }
    },
    events: {
        "click .actionChangeLivro": "actionChangeLivro",
        "click .actionSearchLivro": "actionSearchLivro"
    },
    renderView: function () {
        var that = this;
        //COMBOBOX FORMATO LIVRO
        ComboboxView({
            el: that.ui.id_tipolivro,
            url: "/api/tipo-livro/",
            collection: comboTipoLivro,
            optionLabel: "st_tipolivro",
            emptyOption: "Todos",
            model: Backbone.Model.extend({
                idAttribute: "id_tipolivro",
                defaults: {
                    id_tipolivro: null,
                    st_tipolivro: null
                }
            })
        }, function (el, composite) {
            if (!comboTipoLivro) {
                comboTipoLivro = composite.collection.toJSON();
            }
        });

        //COMBOBOX COLECAO LIVRO
        ComboboxView({
            el: that.ui.id_livrocolecao,
            url: "/api/livro-colecao/",
            collection: comboColecaoLivro,
            optionLabel: "st_livrocolecao",
            emptyOption: "Todos",
            model: Backbone.Model.extend({
                defaults: {
                    id_livrocolecao: null,
                    st_livrocolecao: null
                },
                idAttribute: "id_livrocolecao"
            })
        }, function (el, composite) {
            if (!comboColecaoLivro) {
                comboColecaoLivro = composite.collection.toJSON();
            }
        });
    },
    renderEdit: function () {
    },
    actionChangeLivro: function () {
        this.model.set(this.model.defaults);
        this.model.toggleEdit();
        this.render();
    },
    actionSearchLivro: function () {
        var that = this;
        var values = Backbone.Syphon.serialize(this);

        /**********************
         * CONFIGURACOES DA BUSCA
         **********************/
        var SearchModel,
            SearchView,
            SearchEmpty,
            SearchCollection,
            SearchComposite,
            SearchIni;

        // MODEL
        SearchModel = Backbone.Model.extend({
            defaults: {
                id_livro: null,
                st_livro: ""
            },
            idAttribute: "id_livro"
        });

        // ITEM VIEW
        SearchView = Marionette.ItemView.extend({
            model: new SearchModel(),
            template: "#search-livro-view",
            tagName: "tr",
            events: {
                "click .actionSelectLivro": "actionSelectLivro"
            },
            actionSelectLivro: function () {
                that.model.set(this.model.toJSON());
                that.model.toggleEdit();
                that.render();
            }
        });

        // EMPTY ITEM VIEW
        SearchEmpty = Marionette.ItemView.extend({
            template: _.template("<td colspan='4'>Nenhum registro encontrado.</td>"),
            tagName: "tr"
        });

        // COLLECTION
        SearchCollection = Backbone.Collection.extend({
            model: SearchModel,
            url: "/livro/pesquisar?" + $.param(values),
            parse: function (response) {
                return (response.type ? response.mensagem : response);
            }
        });

        // COMPOSITE VIEW
        SearchComposite = Marionette.CompositeView.extend({
            template: "#search-livro-template",
            collection: new SearchCollection(),
            childView: SearchView,
            emptyView: SearchEmpty,
            childViewContainer: "tbody",
            onRender: function () {
                this.collection.fetch({
                    reset: true,
                    complete: loaded
                });
            },
            events: {
                "click .actionCloseSearch": "actionCloseSearch"
            },
            actionCloseSearch: function () {
                this.$el.html("");
            }
        });
        loading();
        this.$el.find("#search-tipo-produto").html(new SearchComposite().render().$el);
    },
    actionSave: function () {
        var that = this;

        var values = Backbone.Syphon.serialize(this);
        values.id_produto = ProdutoStart.model.get("id_produto");

        return this.model.save(values, {
            success: function (model, response) {
                that.model.set(response);
            }
        });
    }
});

/**
 * Configuracoes do Tipo de Produto = Declarações (3)
 */
var ProdutoDeclaracaoModel = Backbone.Model.extend({
    idAttribute: "id_textocategoria",
    model: TextoSistema,
    url: function () {
        return "/api/produto-declaracao/?" + (this.id || "");
    },
    parse: function (response) {
        return response[0] || response;
    },
    toggleEdit: function () {
        this.is_editing = !this.is_editing;
    }
});

var ProdutoDeclaracaoView = Marionette.ItemView.extend({
        model: new ProdutoDeclaracaoModel(),
        template: "#produto-declaracao-view",
        ui: {
            //COMBOBOXES PESQUISA
            id_textosistema: "[name='id_textosistema']",
            id_formadisponibilizacao: "[name='id_formadisponibilizacao']",
            id_arquivoexemplo: "[name='arquivo-exemplo']",
            produtDeclaracao: "#produtDeclaracao"
        },
        onBeforeRender: function () {
            if (this.model.is_editing) {
                this.template = "#produto-declaracao-edit";
            } else {
                this.template = "#produto-declaracao-view";
            }
        },
        onRender: function () {
            this.renderView();
        },
        events: {
            "click .actionChangeLivro": "actionChangeDeclaracao",
            "click .actionSearchLivro": "actionSearchDeclaracao"
        },
        renderView: function () {
            var that = this;

            that.ui.id_textosistema.prepend("<option value='' selected='selected'>Escolha uma opção</option>");
            var collectionDeclaracao = new TextoSistemaCollection();
            collectionDeclaracao.url += "id_textocategoria/3";
            collectionDeclaracao.fetch({
                success: function () {
                    var view = new SelectView({
                        el: that.ui.id_textosistema,
                        collection: collectionDeclaracao,
                        childViewOptions: {
                            optionLabel: "st_textosistema",
                            optionValue: "id_textosistema",
                            optionSelected: that.model.get("id_textosistema") ? that.model.get("id_textosistema") : null
                        }
                    });
                    view.render();
                }
            });

            that.ui.id_formadisponibilizacao.prepend("<option value='' selected='selected'>Escolha uma opção</option>");
            var collectionFormaDisponibilizacao = new FormaDisponibilizacaoCollection();
            collectionFormaDisponibilizacao.fetch({
                success: function () {
                    var view = new SelectView({
                        el: that.ui.id_formadisponibilizacao,
                        collection: collectionFormaDisponibilizacao,
                        childViewOptions: {
                            optionLabel: "st_formadisponibilizacao",
                            optionValue: "id_formadisponibilizacao",
                            optionSelected: that.model.get("id_formadisponibilizacao") ? that.model.get("id_formadisponibilizacao") : null
                        }
                    });
                    view.render();
                }
            });
        },
        actionSave: function () {
            var that = this;

            if (!that.ui.id_formadisponibilizacao.val()) {
                $.pnotify({
                    title: "Alerta",
                    text: "Selecione a Forma de Disponibilização",
                    type: "error"
                });
            }

            var params = {
                id_produtotextosistema: that.model.get("id_produtotextosistema") ? that.model.get("id_produtotextosistema") : null,
                id_produto: ProdutoStart.model.get("id_produto"),
                id_entidade: ProdutoStart.model.get("id_entidade"),
                id_formadisponibilizacao: that.ui.id_formadisponibilizacao.val(),
                id_textosistema: that.ui.id_textosistema.val()
            };


            this.ui.produtDeclaracao.ajaxSubmit({
                url: "/api/produto-declaracao",
                data: params,
                type: "post",
                dataType: "json"
            });

        }
    })
;


/**
 * Configuracoes do Tipo de Produto = Combo (7)
 */
// MODEL
var ProdutoComboModel = Backbone.Model.extend({
    idAttribute: "id_produtocombo",
    defaults: {
        id_produtocombo: null,
        id_produto: null,
        id_livro: null,
        id_entidade: null,
        st_produto: "",
        st_tipoproduto: "",
        nu_valor: null,
        nu_descontoporcentagem: null
    },
    url: function () {
        return "/api/produto-combo/" + (this.id || "");
    },
    parse: function (response) {
        return response[0] || response;
    },
    toggleEdit: function () {
        this.is_editing = !this.is_editing;
    }
});

// ITEM VIEW
var ProdutoComboView = Marionette.ItemView.extend({
    model: new ProdutoComboModel(),
    template: "#combo-view",
    tagName: "tr",
    ui: {
        nu_descontoporcentagem: "[name='nu_descontoporcentagem']",
        nu_valorfinal: ".nu_valorfinal"
    },
    onBeforeRender: function () {
        this.model.set("nu_valor", Number(this.model.get("nu_valor")).toFixed(2));
        this.model.set("nu_descontoporcentagem", Number(this.model.get("nu_descontoporcentagem")).toFixed(2));
    },
    onRender: function () {
        this.calculaValorFinal();
    },
    events: {
        "save": "actionSave",
        "keyup [name='nu_descontoporcentagem']": "calculaValorFinal",
        "change [name='nu_descontoporcentagem']": "calculaValorFinal",
        "click .actionRemoveComboProduto": "actionRemoveComboProduto"
    },
    calculaValorFinal: function () {
        var nu_valor = Number(this.model.get("nu_valor"));
        var nu_descontoporcentagem = Number(this.ui.nu_descontoporcentagem.val());
        var value = nu_valor - (nu_valor * nu_descontoporcentagem / 100);

        this.ui.nu_valorfinal.html(value.toFixed(2));
    },
    actionRemoveComboProduto: function () {
        var that = this;

        if (this.model.get("id_produtocombo")) {
            // Se tiver o id_produtocombo, remove do banco via REST
            bootbox.confirm("Deseja remover este produto do Combo?", function (response) {
                if (response) {
                    that.model.destroy({
                        success: function (model, response) {
                            if (!response.type || response.type != "success") {
                                this.error(model);
                            }
                        },
                        error: function (model) {
                            ProdutoComboStart.collection.add(model);
                        },
                        complete: function (response) {
                            $.pnotify(response.responseJSON);
                        }
                    });
                }
            });
        } else {
            // Senao, apenas remove da collection porque nao esta no banco
            this.model.collection.remove(this.model);
        }
    },
    actionSave: function () {
        var that = this;

        var values = Backbone.Syphon.serialize(this);

        values.id_produtoitem = this.model.get("id_produto");
        values.id_produto = ProdutoStart.model.get("id_produto");

        return this.model.save(values, {
            success: function (model, response) {
                that.model.set(response);
            }
        });
    }
});

// EMPTY ITEM VIEW
var ProdutoComboEmpty = Marionette.ItemView.extend({
    template: _.template("<td colspan='6'>Nenhum produto foi adicionado ao combo.</td>"),
    tagName: "tr"
});

// COLLECTION
var ProdutoComboCollection = Backbone.Collection.extend({
    model: ProdutoComboModel,
    url: "/api/vw-produto-combo/",
    parse: function (response) {
        return (response.type && response.type == "success" ? response.mensagem : []);
    }
});

// COMPOSITE VIEW
var ProdutoComboComposite = Marionette.CompositeView.extend({
    template: "#combo-template",
    collection: new ProdutoComboCollection(),
    childView: ProdutoComboView,
    emptyView: ProdutoComboEmpty,
    childViewContainer: ".produto-combo-list",
    ui: {
        modal: "#modal-combo",
        modal_body: ".produto-search-list"
    },
    onRender: function () {
        this.collection.fetch({
            reset: true,
            data: {
                id_combo: ProdutoStart.model.get("id_produto")
            },
            complete: loaded
        });
    },
    events: {
        "click .actionOpenModalCombo": "actionOpenModalCombo",
        "click .actionSearchProduto": "actionSearchProduto"
    },
    actionOpenModalCombo: function () {
        var that = this;

        this.ui.modal.modal("show");

        //TIPO DE PRODUTO
        ComboboxView({
            el: that.$el.find("[name='search_id_tipoproduto']"),
            collection: ProdutoStart.collections.id_tipoproduto.collection.toJSON(),
            url: "/api/tipo-produto/",
            optionLabel: "st_tipoproduto",
            emptyOption: "Todos",
            model: Backbone.Model.extend({
                idAttribute: "id_tipoproduto"
            })
        });
    },
    actionSearchProduto: function () {
        var that = this;

        var SearchComboView = Marionette.ItemView.extend({
            model: new ProdutoModel(),
            tagName: "tr",
            template: "#search-produto-view",
            onRender: function () {
            },
            events: {
                "click .actionAddComboProduto": "actionAddComboProduto"
            },
            actionAddComboProduto: function () {
                // Adiciona na collection dos produtos adicoinados
                that.collection.add(this.model.toJSON());
                // Remove da lista atual
                this.model.collection.remove(this.model);
            }
        });
        var SearchComboEmpty = Marionette.ItemView.extend({
            template: _.template("<td colspan='4'>Nenhum registro encontrado.</td>"),
            tagName: "tr"
        });
        var SearchComboCollection = Backbone.Collection.extend({
            model: ProdutoModel,
            url: "/produto/pesquisar/",
            parse: function (response) {
                return (response.type && response.type == "success" ? response.mensagem : []);
            }
        });
        var SearchComboComposite = Marionette.CompositeView.extend({
            template: "#search-produto-template",
            collection: new SearchComboCollection(),
            childView: SearchComboView,
            emptyView: SearchComboEmpty,
            childViewContainer: "tbody",
            onRender: function () {
                var data = {
                    st_produto: that.$el.find("[name='search_st_produto']").val(),
                    id_tipoproduto: that.$el.find("[name='search_id_tipoproduto']").val()
                };
                this.collection.fetch({
                    reset: true,
                    data: data,
                    complete: loaded
                });
            }
        });
        loading();
        this.ui.modal_body.html(new SearchComboComposite().render().$el);
    },
    actionSave: function () {
        this.$el.find("tr").trigger("save");
    }
});

//TIPOS DE PRODUTOS VIEWS
var ProdutoProjetoStart = new ProdutoProjetoView();
var ProdutoLivroStart = new ProdutoLivroView();
var ProdutoDeclaracaoStart = new ProdutoDeclaracaoView();
var ProdutoComboStart = new ProdutoComboComposite();

// INICIALIZA A MONTAGEM DA TELA
var ProdutoValorStart = new ProdutoValorComposite();

var ProdutoValorPromocionalStart = new ProdutoValorView({
    model: new ProdutoValorModel()
});

var ProdutoEntidadeStart = new ProdutoEntidade();
var ProdutoStart = new ProdutoLayout();

if (ProdutoStart.model.get("id_produto")) {
    ProdutoStart.model.fetch({
        beforeSend: loading,
        success: function () {
            G2S.layout.content.show(ProdutoStart);
        },
        complete: loaded
    });
} else {
    G2S.layout.content.show(ProdutoStart);
}
$(function () {
    $(".tooltipClass").tooltip();
});
/**
 * Impressão de Etiquetas com Endereço
 * É filtrada por duas entidades diferentes, uma para declaração e outra para certificação
 *@author João Marcos Bizarro Lopes <joao.lopes@unyleya.com.br>
 */

/**
 * Models de Declaração e Certificação
 */
var Declaracao = Backbone.Model.extend({
    defaults: {
        id_matricula: '',
        bl_imprimir: false,
        st_matricula: '',
        st_nomecompleto: '',
        st_cpf: '',
        st_servico: '',
        st_evolucao: '',
        st_endereco: '',
        st_numero: '',
        st_bairro: '',
        st_cidade: '',
        st_uf: '',
        st_cep: '',
        st_complemento: '',
        st_projetopedagogico: ''
    },
    idAttribute: 'id_matricula'
});
var Certificacao = Backbone.Model.extend({
    defaults: {
        id_matricula: '',
        bl_imprimir: false,
        st_matricula: '',
        st_nomecompleto: '',
        st_cpf: '',
        st_servico: '',
        st_evolucao: '',
        st_endereco: '',
        st_numero: '',
        st_bairro: '',
        st_cidade: '',
        st_uf: '',
        st_cep: '',
        st_complemento: '',
        st_projetopedagogico: ''
    },
    idAttribute: 'id_matricula'
});

var pesquisaSemParametro = Backbone.Model.extend({
    defaults: {
        id_matricula: '',
        bl_imprimir: false,
        st_matricula: '',
        st_nomecompleto: '',
        st_cpf: '',
        st_servico: '',
        st_evolucao: '',
        st_endereco: '',
        st_numero: '',
        st_bairro: '',
        st_cidade: '',
        st_uf: '',
        st_cep: '',
        st_complemento: '',
        st_projetopedagogico: ''
    },
    idAttribute: 'id_matricula'
});

/**
 * Variáveis globais
 */
var pageableGrid;
var declaracaoCollection;
var certificacaoCollection;

var ViewDados = Marionette.ItemView.extend({
    template: '#template-dados',
    tagName: 'div',
    ui: {
        btnPesquisar: '#btn-pesquisar',
        btnGerarXls: '#btn-gerar-xls',
        fieldPessoa: "input[name='field-pessoa']",
        fieldServico: "select[name='field-servico']",
        fieldPeriodoInicio: "input[name='field-periodo-inicio']",
        fieldPeriodoFim: "input[name='field-periodo-fim']",
        fieldSituacao: "input[name='field-situacao']",
        optionDeclaracao: "#field-servico option[value='1']",
        optionCertificacao: "#field-servico option[value='0']"
    },
    events: {
        'click @ui.btnPesquisar': 'pesquisa',
        'click @ui.btnGerarXls': 'gerarXls',
        'change .select-all-header-cell input': 'checkAll'
    },

    /**
     * Verifica as permissões e customiza a combo para o usuário
     */
    onShow: function () {
        if (VerificarPerfilPermissao(48, 795).length == 0) {
            // Remove Serviço de Declaração
            this.ui.optionDeclaracao.remove();
        }

        if (VerificarPerfilPermissao(49, 795).length == 0) {
            // Remove Serviço de Certificação
            this.ui.optionCertificacao.remove();
        }

        if (this.ui.fieldServico.children().length == 2) {
            $('#field-servico option[value="0"]').length ? this.ui.optionCertificacao.attr('selected', 'selected') : this.ui.optionDeclaracao.attr('selected', 'selected');
        }
    },

    /**
     * Verifica se a pesquisa é para Declaração ou Certificação
     */
    pesquisa: function () {
        loading();
        if (this.validateForm()) {
            if (parseInt(this.ui.fieldServico.val()) === 1) {
                this.pesquisaDeclaracao();
            } else if (parseInt(this.ui.fieldServico.val()) === 0) {
                this.pesquisaCertificacao();
            } else {
                this.pesquisaSemParametro();
            }
        } else {
            loaded();
        }
    },
    pesquisaDeclaracao: function () {
        var DeclaracaoCollection = this.newPageableCollection({model: Declaracao, url: '/etiqueta/get-declaracao'});
        declaracaoCollection = new DeclaracaoCollection();
        var RowGrid = Backgrid.Row.extend({
            initialize: function (options) {
                RowGrid.__super__.initialize.apply(this, arguments);
                this.$el.css('cursor', 'pointer');
            },
            render: function () {
                RowGrid.__super__.render.apply(this, arguments);
                if (this.model.get('bl_imprimir')) {
                    this.$el.find('.select-row-cell input').attr('checked', true);
                } else {
                    this.$el.find('.select-row-cell input').attr('checked', false);
                }
                return this;
            },
            events: {
                'change .select-row-cell input': 'checkForXls'
            },

            checkForXls: function () {
                this.model.get('bl_imprimir') ? this.model.set('bl_imprimir', false) : this.model.set('bl_imprimir', true);
            }
        });

        pageableGrid = new Backgrid.Grid({
            className: 'backgrid table table-bordered',
            columns: this.getColumns(),
            collection: declaracaoCollection,
            row: RowGrid,
            emptyText: 'Nenhum registro encontrado'
        });
        var paginator = new Backgrid.Extension.Paginator({
            collection: declaracaoCollection
        });
        declaracaoCollection.fetch({
            data: this.getParams(),
            reset: true,
            beforeSend: function () {
                loading();
            },
            success: _.bind(function (response) {
                this.$el.find("#regiao-grade-data").empty();
                this.$el.find("#regiao-grade-data").append(pageableGrid.render().$el);
                this.$el.find("#regiao-grade-data").append(paginator.render().$el);
                $('#container-DataGrid').show();
            }, this),
            error: function (collection, response) {
                $.pnotify({
                    type: 'warning',
                    title: 'Atenção!',
                    text: response.responseText
                });
            },
            complete: function () {
                loaded();
            }
        });
    },
    pesquisaSemParametro: function () {
        var DeclaracaoCollection = this.newPageableCollection({
            model: pesquisaSemParametro,
            url: '/etiqueta/get-pesquisa-sem-parametro'
        });
        declaracaoCollection = new DeclaracaoCollection();
        var RowGrid = Backgrid.Row.extend({
            initialize: function (options) {
                RowGrid.__super__.initialize.apply(this, arguments);
                this.$el.css('cursor', 'pointer');
            },
            render: function () {
                RowGrid.__super__.render.apply(this, arguments);
                if (this.model.get('bl_imprimir')) {
                    this.$el.find('.select-row-cell input').attr('checked', true);
                } else {
                    this.$el.find('.select-row-cell input').attr('checked', false);
                }
                return this;
            },
            events: {
                'change .select-row-cell input': 'checkForXls'
            },

            checkForXls: function () {
                this.model.get('bl_imprimir') ? this.model.set('bl_imprimir', false) : this.model.set('bl_imprimir', true);
            }
        });

        pageableGrid = new Backgrid.Grid({
            className: 'backgrid table table-bordered',
            columns: this.getColumns(),
            collection: declaracaoCollection,
            row: RowGrid,
            emptyText: 'Nenhum registro encontrado'
        });
        var paginator = new Backgrid.Extension.Paginator({
            collection: declaracaoCollection
        });
        declaracaoCollection.fetch({
            data: this.getParams(),
            reset: true,
            beforeSend: function () {
                loading();
            },
            success: _.bind(function (response) {
                this.$el.find("#regiao-grade-data").empty();
                this.$el.find("#regiao-grade-data").append(pageableGrid.render().$el);
                this.$el.find("#regiao-grade-data").append(paginator.render().$el);
                $('#container-DataGrid').show();
            }, this),
            error: function (collection, response) {
                $.pnotify({
                    type: 'warning',
                    title: 'Atenção!',
                    text: response.responseText
                });
            },
            complete: function () {
                loaded();
            }
        });
    },
    pesquisaCertificacao: function () {
        var CertificacaoCollection = this.newPageableCollection({
            model: Certificacao,
            url: '/etiqueta/get-certificacao'
        });
        certificacaoCollection = new CertificacaoCollection();
        var RowGrid = Backgrid.Row.extend({
            initialize: function (options) {
                RowGrid.__super__.initialize.apply(this, arguments);
                this.$el.css('cursor', 'pointer');
            },
            render: function () {
                RowGrid.__super__.render.apply(this, arguments);
                if (this.model.get('bl_imprimir')) {
                    this.$el.find('.select-row-cell input').attr('checked', true);
                } else {
                    this.$el.find('.select-row-cell input').attr('checked', false);
                }
                return this;
            },

            events: {
                'change .select-row-cell input': 'checkForXls'
            },

            checkForXls: function () {
                this.model.get('bl_imprimir') ? this.model.set('bl_imprimir', false) : this.model.set('bl_imprimir', true);
            }
        });

        pageableGrid = new Backgrid.Grid({
            className: 'backgrid table table-bordered',
            columns: this.getColumns(),
            collection: certificacaoCollection,
            row: RowGrid,
            emptyText: 'Nenhum registro encontrado'
        });
        var paginator = new Backgrid.Extension.Paginator({
            collection: certificacaoCollection
        });
        certificacaoCollection.fetch({
            data: this.getParams(),
            reset: true,
            beforeSend: function () {
                loading();
            },
            success: _.bind(function (response) {
                this.$el.find("#regiao-grade-data").empty();
                this.$el.find("#regiao-grade-data").append(pageableGrid.render().$el);
                this.$el.find("#regiao-grade-data").append(paginator.render().$el);
                $('#container-DataGrid').show();
            }, this),
            error: function (collection, response) {
                $.pnotify({
                    type: 'warning',
                    title: 'Atenção!',
                    text: response.responseText
                });
            },
            complete: function () {
                loaded();
            }
        });
    },

    /**
     * Retorna os parâmetros de pesquisa
     * @returns {{pessoa, servico, situacao: (*|jQuery), dtInicio, dtFim, idEntidade}}
     */
    getParams: function () {
        return {
            pessoa: this.ui.fieldPessoa.val(),
            servico: this.ui.fieldServico.val(),
            situacao: $("input[name='field-situacao']:checked").val(),
            dtInicio: this.ui.fieldPeriodoInicio.val(),
            dtFim: this.ui.fieldPeriodoFim.val(),
            idEntidade: G2S.loggedUser.attributes.id_entidade
        };
    },

    /**
     * Retorna o bind de colunas da tabela (grid)
     */
    getColumns: function () {
        return [
            {
                name: 'bl_imprimir',
                cell: 'select-row',
                editable: false,
                headerCell: 'select-all'
            },
            {
                name: 'id_matricula',
                label: 'Matrícula',
                editable: false,
                cell: 'string'
            },
            {
                name: 'st_projetopedagogico',
                label: 'Projeto Pedagógico',
                editable: false,
                cell: 'string'
            },
            {
                name: 'st_nomecompleto',
                label: 'Nome',
                editable: false,
                cell: 'string'
            },
            {
                name: 'st_cpf',
                label: 'CPF',
                editable: false,
                cell: 'string'
            },
            {
                name: 'st_servico',
                label: 'Serviço',
                editable: false,
                cell: 'string'
            },
            {
                name: 'st_evolucao',
                label: 'Evolução da Matricula',
                editable: false,
                cell: 'string'
            },
            {
                name: 'st_endereco',
                label: 'Endereço',
                editable: false,
                cell: 'string'
            }
        ];
    },

    /**
     * Retorna uma nova instância da PageableCollection
     * @param config
     */
    newPageableCollection: function (config) {
        if (!config.hasOwnProperty('model') || !config.hasOwnProperty('url')) {
            console.error('Não foi possível criar a PageableCollection. Parâmetros obrigatórios não foram passados.');
        }

        return Backbone.PageableCollection.extend({
            model: config.model,
            url: config.url,
            state: {
                pageSize: (config.hasOwnProperty('pageSize') ? config.pageSize : 20)
            },
            mode: (config.hasOwnProperty('mode') ? config.mode : 'client')
        });
    },

    /**
     * Valida o formulário
     * @returns {boolean}
     */
    validateForm: function () {
        if (this.ui.fieldPessoa.val().length >= 11) {
            return true;
        }
        if (!this.ui.fieldServico.val() || !$("input[name='field-situacao']:checked").val()) {
            $.pnotify({
                title: 'Campo Obrigatório!',
                text: 'Escolha um Serviço e uma Situação para realizar a pesquisa!',
                type: 'error'
            });
            return false;
        }

        if (this.ui.fieldPessoa.val().length > 45) {
            $.pnotify({
                title: 'Limite excedido!',
                text: 'O campo de pesquisar pessoa (Nome, E-mail ou CPF) deve possuir no máximo 45 caracteres',
                type: 'error'
            });
            return false;
        }

        if (!this.ui.fieldPeriodoInicio.val() && !this.ui.fieldPeriodoFim.val()) {
            this.ui.fieldPeriodoFim.val(todayBr());
            this.ui.fieldPeriodoInicio.val(oneYearAgo(this.ui.fieldPeriodoFim.val()));
        } else if (!this.ui.fieldPeriodoInicio.val() || !this.ui.fieldPeriodoFim.val()) {
            $.pnotify({
                title: 'Campo obrigatório!',
                text: 'Você esqueceu de preencher o Período Inicial ou o Período Final!',
                type: 'error'
            });
            return false;
        } else {
            if (daysDiff(this.ui.fieldPeriodoInicio.val(), this.ui.fieldPeriodoFim.val()) > 366) {
                $.pnotify({
                    title: 'Período Inválido!',
                    text: 'A pesquisa só é realizada num período máximo de 1 ano',
                    type: 'error'
                });
                return false;
            }
        }

        return true;
    },

    /**
     * SelectAll Paginated
     * @param event
     */
    checkAll: function (event) {
        event.preventDefault();

        var collection = this.getParams().servico == 1 ? declaracaoCollection : certificacaoCollection;

        if ($('.select-all-header-cell input').is(':checked')) {
            collection.fullCollection.models.forEach(function (item, index, arr) {
                item.attributes.bl_imprimir = true;
                $('.select-row-cell input').checked = true;
            });
            $.pnotify({
                title: 'Atenção!',
                text: 'Quando você clica em Marcar Todos, você está marcando todos os registros de todas as páginas',
                type: 'info'
            });
        } else {
            collection.fullCollection.models.forEach(function (item, index, arr) {
                item.attributes.bl_imprimir = false;
                $('.select-row-cell input').checked = false;
            });
            $.pnotify({
                title: 'Atenção!',
                text: 'Quando você clica em Desmarcar Todos, você está desmarcando todos os registros de todas as páginas',
                type: 'info'
            });
        }
    },

    /**
     * GerarXLS verifica cada model da fullCollection para incluir no XLS (getSelectedModels() não resolve)
     * @param event
     * @returns {boolean}
     */
    gerarXls: function (event) {
        event.preventDefault();

        var collectionEnviar = new Backbone.Collection;

        var collection = this.getParams().servico == 1 ? declaracaoCollection : certificacaoCollection;
        collection.fullCollection.models.forEach(function (item, index, arr) {
            if (item.get('bl_imprimir')) {
                collectionEnviar.add(item);
            }
        });

        var validacao = true;
        var arrPrint = [];
        if (collectionEnviar.length) {
            collectionEnviar.each(function (model, i) {
                arrPrint[i] = {
                    'st_nomecompleto': model.get('st_nomecompleto'),
                    'st_endereco': model.get('st_endereco'),
                    'st_numero': model.get('st_numero'),
                    'st_bairro': model.get('st_bairro'),
                    'st_cidade': model.get('st_cidade'),
                    'st_uf': model.get('st_uf'),
                    'st_cep': model.get('st_cep'),
                    'st_complemento': model.get('st_complemento')
                }
            });
        } else {
            $.pnotify({
                type: 'error',
                title: 'Campo Obrigatório',
                text: 'Selecione os alunos.'
            });
            return false;
        }

        $('#xls').empty();
        if (validacao) {
            arrPrint.forEach(function (item, index) {
                var input = document.createElement("input");
                input.type = "hidden";
                input.name = index;
                input.value = JSON.stringify(item);
                $('#xls').append(input);
            });
            $('#xls').submit();
        }
    }
});

/**
 * LayoutView, que instancia a ViewDados
 */
var EtiquetaLayout = Marionette.LayoutView.extend({
    template: '#template-dados-etiqueta',
    tagName: 'div',
    className: 'container-fluid',
    regions: {
        dados: '#wrapper-dados'
    },

    onShow: function () {
        this.dados.show(new ViewDados());
        $('#container-DataGrid').hide();
    }
});

var layoutEtiqueta = new EtiquetaLayout();
G2S.show(layoutEtiqueta);
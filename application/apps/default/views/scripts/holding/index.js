/**
 * CADASTRO DE Holding
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */

/*********************************************
 ************** COLLECTIONS ******************
 *********************************************/
var ColletionSituacao = Backbone.Collection.extend({
    model: Situacao,
    url: '/api/situacao/?st_tabela=tb_holding',
    initialize: function () {
        this.fetch({async: false});
    }
});

/*********************************************
 **************   ITEMVIEW  ******************
 *********************************************/

var ViewDados = Marionette.ItemView.extend({
    template: '#template-dados',
    tagName: 'div',
    ui: {
        id_situacao: '#id_situacao',
        st_holding: '.st_holding',
        id_holding: '.id_holding',
        bl_compartilharcarta: '#bl_compartilharcarta'
    },
    onShow: function () {
        this.populaSelectDados();
        this.carregaEntidade();
    },
    populaSelectDados: function () {

        var col = new ColletionSituacao();

        var view = new SelectView({
            el: this.ui.id_situacao, // Elemento da DOM
            collection: col, // Instancia da collection a ser utilizada
            childViewOptions: {
                optionLabel: 'st_situacao', // Propriedade da Model que será utilizada como label do select
                optionValue: 'id_situacao', // Propriedade da Model que será utilizada como id do option do select
                optionSelected: G2S.editModel ? G2S.editModel.id_situacao : null      // ID da option que receberá o atributo "selected"
            }
        });
        view.render();
    },
    carregaEntidade: function () {
        if (G2S.editModel) {
            $('.id_holding').val(G2S.editModel.id_holding);
            $('#st_holding').val(G2S.editModel.st_holding);
            $('#bl_compartilharcarta').val(G2S.editModel.bl_compartilharcarta);
        }
    }
});


/*********************************************
 ************** LAYOUT ***********************
 *********************************************/
var LayoutHolding = Marionette.LayoutView.extend({
    template: '#template-dados-holding',
    tagName: 'div',
    className: 'container-fluid',
    regions: {
        dados: '#wrapper-dados',
        select: '#id_situacao'
    },
    events: {
        "click #btn-salvar": "salvarDados"
    },
    initialize: function () {
        thatLayout = this;
    },
    onShow: function () {
        this.dados.show(new ViewDados());
    },
    salvarDados: function () {
        that = this;

        if (!this.validarNome() || !this.validarSituacao()) {
            return false;
        }

        var parametros = {
            'id_situacao': $('#id_situacao').val(),
            'id_holding': $('.id_holding').val(),
            'st_holding': $('#st_holding').val(),
            'bl_compartilharcarta': $('#bl_compartilharcarta').val() === "on" ? 1 : 0
        };

        $.ajax({
            type: 'post',
            url: '/holding/salvar-holding',
            data: parametros,
            beforeSend: function () {
                loading();
            },
            success: function (data) {
                $.pnotify({
                    title: data.title,
                    text: data.mensagem,
                    type: data.type
                });
            },
            complete: function () {
                loaded();
            }
        });
    },
    validarSituacao: function () {

        if ($('#id_situacao').val() !== '') {
            return true;
        }

        $.pnotify({
            title: 'Campo obrigatório!',
            text: 'Selecione a situação.',
            type: 'error'
        });

        return false;

    },
    validarNome: function() {

        if ($("#st_holding").val()) {
            return true;
        }

        $.pnotify({
            title: 'Campo obrigatório!',
            text: 'Insira o nome da holding.',
            type: 'error'
        });

        return false;
    }
});

/*********************************************
 ************** INICIALIZAÇÃO ****************
 *********************************************/

var layoutHolding = new LayoutHolding();
G2S.show(layoutHolding);
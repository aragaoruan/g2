/**
 * Definir regiões
 */
G2S.layout.addRegions({
    global: '#global-content',
    pessoaAssunto: '#pessoa-assunto-content'
});

/**
 * Importar scripts
 */
loading();
$.ajaxSetup({ async: false });
$.getScript('/js/backbone/views/NucleoCo/Collections.js');  // Fetch das collections necessarias
$.getScript('/js/backbone/views/NucleoCo/NucleoCo.js');     // Tela de edição de NucleoCo
$.getScript('/js/backbone/views/NucleoCo/NucleoList.js');   // Tela de listagem de NucleoCo

$.getScript('/js/backbone/views/NucleoCo/Evolucao.js');
$.getScript('/js/backbone/views/NucleoCo/AssuntoAdded.js');
$.getScript('/js/backbone/views/NucleoCo/AssuntoList.js');
$.getScript('/js/backbone/views/NucleoCo/PessoaList.js');
$.ajaxSetup({ async: true });

$.getScript('/js/minicolors.min.js');

loaded();

var NucleoListIni = new NucleoListComposite();
var NucleoCoIni = new NucleoView();

var EvolucaoIni = new EvolucaoComposite();
var AssuntoAddedIni = new AssuntoAddedComposite();
var AssuntoListIni = new AssuntoListComposite();
var PessoaListIni = new PessoaListComposite();

var ResponsavelIni = null;
var PessoaAssuntoIni = []; // Array de Composites
var PessoaAssuntoGlobalIni = null;

var EmailListIni = null;
var EmaillAddedIni = null;

G2S.layout.global.show(NucleoListIni);

/**
 * Layout da funcionalidade
 */
var ImportacaoSalaReferencia = Marionette.ItemView.extend({
    template: "#template-importacao-sala-referencia",
    tagName: "div",
    className: "container-fluid",
    ui: {
        moodleOrigem: "#select-moodle-origem",
        moodleDestino: "#select-moodle-destino",
        arquivoXLS: "#arquivoXLS",
        form: "#form-enviar-dados",
        btnSalvar: "#submitForm"
    },
    events: {
        "change @ui.moodleOrigem": "validaMoodleO",
        "change @ui.moodleDestino": "validaMoodleD",
        "submit #form-enviar-dados": "submitDados"
    },
    onShow: function () {
        this.populaSelectMoodle(this.ui.moodleOrigem);
    },
    populaSelectMoodle: function (elemento) {
        ComponenteSelectMoodle
            .render({
                el: elemento
            }).success(function () {
        }.bind(this));
    },
    validaMoodleO: function () {
        this.ui.moodleDestino.prop("disabled", false);
        this.populaSelectMoodle(this.ui.moodleDestino);
    },
    validaMoodleD: function () {
        var moodleO = this.ui.moodleOrigem.val();
        var moodleD = this.ui.moodleDestino.val();

        if (moodleD === moodleO) {
            this.ui.btnSalvar.prop("disabled", true);
            $.pnotify({
                type: "error",
                title: "OPS!",
                text: "Por favor, selecione um moodle de destino diferente do moodle de origem."
            });
        } else {
            this.ui.btnSalvar.prop("disabled", false);
        }

    },
    submitDados: function (e) {
        var arq = this.ui.arquivoXLS.val();
        if (arq === "") {
            $.pnotify({
                type: "error",
                title: "OPS!",
                text: "Por favor, adicione um arquivo para importação."
            });
        } else if (arq.substring((arq.length - 0), (arq.length - 4)) !== "xlsx" && arq.substring((arq.length - 0), (arq.length - 3)) !== "xls") {
            $.pnotify({
                type: "error",
                title: "OPS!",
                text: "Por favor, adicione um arquivo com as extensões '.xlsx'  ou '.xls'."
            });
        } else {
            this.ui.form.ajaxSubmit({
                url: "/importacao-sala-referencia/recebe-dados",
                type: "post",
                dataType: "json",
                beforeSend: function () {
                    loading();
                },
                success: function (xhr) {
                    $.pnotify({
                        type: "success",
                        title: "Sucesso!",
                        text: "Parabéns, a importação foi realizada com sucesso."
                    });
                    location.reload();
                },
                complete: function () {
                    loaded();
                },
                error: function (xhr) {
                    $.pnotify({
                        type: "error",
                        title: "OPS!",
                        text: "Não foi possível fazer a importação, por favor tente novamente."
                    });
                }
            });
        }
        e.preventDefault();
    }
});


G2S.show(new ImportacaoSalaReferencia());

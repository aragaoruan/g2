/**
 * Created with JetBrains PhpStorm.
 * User: Débora Castro
 * Date: 17/09/13
 * Time: 11:21
 */
var AreaConhecimentoModel = Backbone.Model.extend({
    defaults: {
        id_situacao: null,
        st_descricao: '',
        bl_ativo: true,
        st_areaconhecimento: '',
        id_entidade: null,
        id_usuariocadastro: null,
        id_areaconhecimentopai: 0,
        id_tipoareaconhecimento: null,
        st_tituloexibicao: '',
        is_editing: false,
        st_imagemarea: ''
    },
    fileAttribute: 'attachment',
    url: function () {
        //return this.id ? '/api/area-conhecimento/' + this.id : '/api/area-conhecimento';
        return '/api/area-conhecimento';
    },
    toggleEdit: function () {
        this.is_editing = !this.is_editing;
    }
});

var AreaConhecimentoCollection = Backbone.Collection.extend({
    model: AreaConhecimentoModel,
    url: "/api/area-conhecimento"
});

var AreaConhecimentoMView = Backbone.View.extend({
    tagName: "",
    className: "",
    initialize: function (options) {
        this.render = _.bind(this.render, this);
        this.model.bind('change', this.render);
    },
    render: function (options) {
        return this;
    },
    saveChanges: function (opt) {
//        that = this;
        console.log(opt);
        return false;
    }

});

var SelectModel = Backbone.Model.extend({});
var SelectCollection = Backbone.Collection.extend({
    model: SelectModel,
    url: ''
});
var SelectView = Backbone.View.extend({
    tagName: 'option',
    initialize: function () {
        _.bindAll(this, 'render');
    },
    render: function () {
        $(this.el).attr('value', this.model.get('id')).text(this.model.get('text'));
        return this;
    }
});


$(function () {


    $("#formAreaConhecimento").off().on('submit', function () {

        var checked_ids = [];
        $("#entidades").jstree("get_checked", null, true).each
        (function () {
            checked_ids.push(this.id);
        });
        var areaConhecimento = new AreaConhecimentoModel();

        var paramObj = {
            st_areaconhecimento: $("#st_areaconhecimento").val(),
            st_tituloexibicao: $("#st_tituloexibicao").val(),
            id_situacao: $("#id_situacao").val(),
            id_tipoareaconhecimento: $("#id_tipoareaconhecimento").val(),
            st_descricao: $("#st_descricao").val()
        };


        paramObj.entidades = checked_ids;


        if ($("#id").val() !== "") {
            //paramObj.id = $("#id").val();
            paramObj.id_areaconhecimento = $("#id").val();
        }
        areaConhecimento.set(paramObj);

        //verifica o arquivo
        if ($("#st_imagemarea")[0].files.length && typeof $("#st_imagemarea")[0].files[0] != 'undefined') {
            var objImg = $("#st_imagemarea")[0].files[0];

            if (objImg.type != 'image/jpeg' || objImg.type != 'image/png') {
                areaConhecimento.set('attachment', objImg);
            } else {
                $.pnotify({
                    title: 'Atenção!',
                    type: 'warning',
                    text: 'O tipo de arquivo selecionado não é suportado.<br>Selecione uma imagem com a extensão JPG ou PNG.'
                });
            }
        }
        areaConhecimento.save(null, {
            //resposta da model
            success: function (model, response) {
                $("#id").val(response.id);
                //this.id = response.id;
                if (response.st_imagemarea) {
                    $("#imagem-area").attr('src', response.st_imagemarea);
                }
                // this.toggleEdit();
                $.pnotify({title: response.title, text: response.text, type: response.type});//mensagem de retorno
            },
            error: function (model, response) {
                $.pnotify({title: response.title, text: response.text, type: 'error'});
            }
        });
        return false;
    });

    loaded();
    $("#entidades").jstree({
        "html_data": {
            "ajax": {
                "url": "util/arvore-entidade",
                "data": function (n) {
                    return {id: n.attr ? n.attr("id") : 0};
                }
            }
        },
        "plugins": ["themes", "html_data", "checkbox", "sort", "ui"]
    }).bind("loaded.jstree", function (event, data) {
        if ($("#id").val() != 0) {
            $.ajax({
                url: "area-conhecimento/recarrega-arvore",
                type: 'POST',
                dataType: 'json',
                data: 'id_areaconhecimento=' + $("#id").val(),
                success: function (data) {
                    $("#entidades").jstree("get_unchecked", null, true).each
                    (function () {
                        for (var i = 0; i < data.length; i++) {
                            if (data[i] == this.id) {
                                $.jstree._reference("#entidades").check_node('li#' + this.id);
                            }
                        }
                    });
                }
            });
        }
    });
});



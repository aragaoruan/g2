var viewRelatorioLog = false;

/* COLLECTIONS */
var VwMatriculasCollection = Backbone.Collection.extend({
    model: VwMatricula
});

var VwSalasColletion = Backbone.Collection.extend({
    model: Backbone.Model,
    url: '',
    defaults: {
        idSaladeaula: "",
        stSaladeaula: "",
        idMatricula: ""
    }
});
var VwLogAcesso = Backbone.Collection.extend({
    model: Backbone.Model,
    defaults: {
        idLogacesso: '',
        dtCadastro: '',
        idFuncionalidade: '',
        stFuncionalidade: '',
        idPerfil: '',
        stNomeperfil: '',
        idUsuario: '',
        stNomecompleto: '',
        idSaladeaula: '',
        stSaladeaula: '',
        idEntidade: '',
        dtCadastroFormatada: ''
    },
    parse: function (response) {
        var data = [];
        _.map(response, function (value) {
            var newDate = new Date(value.dt_cadastro);
            data.push(
                {
                    "st_saladeaula": value.st_saladeaula,
                    "id_funcionalidade": value.id_funcionalidade,
                    "st_funcionalidade": value.st_funcionalidade,
                    "st_nomeperfil": value.st_nomeperfil,
                    "dt_cadastro_formatada": newDate.toLocaleDateString('pt-BR') + ' ' + newDate.toLocaleTimeString('pt-BR')
                }
            );
        });
        return data;
    },
    url: ''
});

/* VAR */
/*TABELA RESULTADO LOGS*/
var columnsTableLogs = [
    {
        name: 'st_saladeaula', // JSON Field
        label: 'Sala de Aula', // Label que aparecera no Header da tabela
        editable: false,
        cell: 'string'
    },
    {
        name: 'id_funcionalidade', // JSON Field
        label: 'ID. Funcionalidade', // Label que aparecera no Header da tabela
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_funcionalidade', // JSON Field
        label: 'Funcionalidade', // Label que aparecera no Header da tabela
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_nomeperfil', // JSON Field
        label: 'Perfil', // Label que aparecera no Header da tabela
        editable: false,
        cell: 'string'
    },
    {
        name: 'dt_cadastro_formatada', // JSON Field
        label: 'Data da Ação', // Label que aparecera no Header da tabela
        editable: false,
        cell: 'string',
    },
];

/* VIEWS */
var OptionMatriculaView = Backbone.View.extend({
    template: _.template('<%=id_matricula%>'),
    render: function () {
        this.$el.attr('value', this.model.get('id_matricula'));
        this.$el.text(this.model.get('id_matricula') + ' - ' + this.model.get('st_projetopedagogico') + ' - ' +
            this.model.get('st_evolucao') + ' - (' + this.model.get('st_entidadematricula') + ")");
        return this;
    }
});

var OptionSalaDeAulaView = Backbone.View.extend({
    template: _.template('<%=id_saladeaula%>'),
    render: function () {
        this.$el.attr('value', this.model.get('id_saladeaula'));
        this.$el.text(this.model.get('st_saladeaula'));
        return this;
    }
});

var RelatorioLogView = Backbone.View.extend({
    el: $("#container-filtros"),
    relatorioLogView: null,
    ui: {
        autoComplete: "#autoComplete",
        containerFiltros: "#container-filtros",
        selectMatricula: "#select-matricula",
    },
    regions: {},
    initialize: function () {
        this.render();
    },
    render: function () {


    },
    events: {
        "change #select-matricula": "buscaSala",
        "click #btnBuscarLogs": "buscarLogs",
    },
    //METODOS
    toggleContainer: function () {
        $('#select-matricula').html('<option value="none">Seleciona a matrícula</option>');
        $("#container-filtros").toggle();
    },
    buscaMatricula: function () {

        var pessoa = this.getPessoa();
        var that = this;
        if (pessoa.id) {
            var id = pessoa.id;
            var collectionMatricula = new VwMatriculasCollection();
            collectionMatricula.url = '/matricula/retornar-matriculas-by-params/id_usuario/' + id;
            collectionMatricula.fetch({
                beforeSend: function () {
                    loading();
                },
                complete: function () {
                    loaded();
                },
                success: function (data) {
                    if (data.length) {
                        that.toggleContainer();

                        var comboViewMatricula = new SelectView({
                            el: "#select-matricula",
                            collection: collectionMatricula,
                            childViewOptions: {
                                optionLabel: 'st_projetopedagogico + " (" + st_evolucao+")"',
                                optionValue: 'id_matricula',
                                optionSelected: 'none'
                            }
                        });
                        comboViewMatricula.render();

                    } else {
                        $.pnotify({
                            type: 'warning',
                            title: 'Atenção!',
                            text: 'Nenhuma matrícula encontrada.'
                        });
                    }
                },
                error: function () {

                }
            });


        } else {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Erro ao buscar o aluno.'
            });
        }
    },
    buscaSala: function () {

        if ($("#select-matricula").val() === 'none') {
            $("#container-sala").hide();
            $('#dt_inicial').val('');
            $('#dt_final').val('');
            $('#select-sala').html('<option value="">Todas</option>');
            $("#table_logs").html('');
            $("#container-resultado").hide();

            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Seleciona uma matrícula!'
            });

            return false;
        }
        var collectionSalas = new VwSalasColletion();
        collectionSalas.url = '/relatorio-log-aluno/salas';
        collectionSalas.fetch({
            data: {
                'id_matricula': $("#select-matricula").val()
            },
            beforeSend: function () {
                loading();
            },
            complete: function () {
                loaded();
            },
            success: function (data) {
                if (data.length) {
                    $("#container-sala").show();
                    $('#select-sala').html('<option value="">Todas</option>');
                    var comboViewSala = new SelectView({
                        el: '#select-sala',
                        collection: collectionSalas,
                        childViewOptions: {
                            optionLabel: 'st_saladeaula',
                            optionValue: 'id_saladeaula',
                            optionSelected: ''
                        }
                    });
                    comboViewSala.render();
                } else {
                    $('#select-sala').html('<option value="all">Todas</option>');
                    $("#container-sala").hide();
                    $("#table_logs").html('');
                    $('#dt_inicial').val('');
                    $('#dt_final').val('');
                    $("#container-resultado").hide();
                    $.pnotify({
                        type: 'warning',
                        title: 'Atenção!',
                        text: 'Nenhuma sala encontrada'
                    });
                }

            }
        });

    },
    buscarLogs: function () {

        var idUsuario = this.getPessoa().id;
        var colletionLogs = new VwLogAcesso();
        colletionLogs.url = '/relatorio-log-aluno/logs';
        colletionLogs.fetch({
            data: {
                'id_usuario': idUsuario,
                'id_matricula': $('#select-matricula').val(),
                'id_saladeaula': $("#select-sala").val(),
                'dt_inicial': $("#dt_inicial").val(),
                'dt_final': $("#dt_final").val(),
            },
            beforeSend: function () {
                loading();
            },
            complete: function () {
                loaded();
            },
            success: function (data) {

                if (data.length) {
                    var idMatricula = $('#select-matricula').val();
                    var idSaladeaula = $("#select-sala").val();
                    var dtInicial = $("#dt_inicial").val();
                    var dtFinal = $("#dt_final").val();

                    var url = 'relatorio-log-aluno/geraXls?id_usuario=' + idUsuario + '&id_matricula=' + idMatricula
                        + '&id_saladeaula=' + idSaladeaula + '&dt_inicial=' + dtInicial + '&dt_final=' + dtFinal;
                    $('#botaoImpressaoXLS').attr('href',url);

                    $('#container-resultado').show();
                    tableViewLogs = new Backgrid.Grid({
                        el: "#table_logs",
                        className: 'backgrid table table-striped table-bordered table-hover',
                        columns: columnsTableLogs,
                        collection: colletionLogs,
                    });
                    tableViewLogs.render();
                } else {
                    $('#table_logs').html('');
                    $('#container-resultado').hide();

                    $.pnotify({
                        type: 'warning',
                        title: 'Atenção!',
                        text: 'Nenhum log encontrado!'
                    });
                }
            }, error: function () {}
        });
    },
    getPessoa: function () {
        return componenteAutoComplete.getFixModel();
    },
});

/* FORMULARIO AUTOCOMPLETE*/
var renderizaautoComplete = function () {

    componenteAutoComplete.clearFixModel();

    componenteAutoComplete.main({
        model: VwPesquisarPessoa,
        element: $("#autoComplete"),
        entidadesMesmaHolding: true
    }, function () {
        if (viewRelatorioLog === false) {
            viewRelatorioLog = new RelatorioLogView();
            viewRelatorioLog.buscaMatricula();
        } else {
            viewRelatorioLog.buscaMatricula();
        }
    }, function () {
        $('#select-matricula').html('<option value="none">Seleciona a matrícula</option>');
        $("#container-filtros").hide();
        $('#select-matricula').html('<option value="none">Seleciona a matrícula</option>');
        $('#select-sala').html('<option value="all">Todas</option>');
        $("#container-sala").hide();
        $('#dt_inicial').val('');
        $('#dt_final').val('');
        $("#table_logs").html('');
        $("#container-resultado").hide();
    });
};

renderizaautoComplete();
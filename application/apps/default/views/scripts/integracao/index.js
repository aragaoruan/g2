/**
 * Autor: Ilson Nóbrega <ilson.nobrega@unyleya.com.br>
 * Data: 18/08/2014
 * Formulário para fazer a integração entre G2 e softwares parceiros
 */

$.getScript('/js/backbone/models/modalbasica.js');
$.getScript('js/backbone/views/Util/modal_basica.js');

/**
 * Variáveis
 */
var that, opcao, thisTotvsFinanceiro, thisMoodleItemView;


var EntidadeIntegracao = Backbone.Model.extend({
    defaults: {
        'id_entidadeintegracao': '',
        'id_entidade': '',
        'id_usuariocadastro': '',
        'id_sistema': '',
        'st_codsistema': '',
        'dt_cadastro': '',
        'st_codchave': '',
        'st_codarea': '',
        'st_caminho': '',
        'nu_perfilsuporte': '',
        'nu_perfilprojeto': '',
        'nu_perfildisciplina': '',
        'nu_perfilobservador': '',
        'nu_perfilalunoobs': '',
        'nu_perfilalunoencerrado': '',
        'nu_contexto': '',
        'st_linkedserver': '',
        'st_codcpg': '',
        'bl_criarsalas': '',
        'bl_criarsala': '',
        'id_recebedor': '',
        'st_codchavewsuny': '',
        'bl_ativo': '',
        'id_situacao': '',
        'st_titulo': ''
    }
});

/**
 * Collections
 */
var IntegracaoCollection = Backbone.Collection.extend({
    model: EntidadeIntegracao,
    url: function () {
        return "/api/entidade-integracao/?" + $.param(_.defaults(this.data));
    },
    data: {}
});

var MeioPagamentoIntegracao = Backbone.Collection.extend({
    model: VwMeioPagamentoIntegracao,
    url: '/api/vw-meio-pagamento-integracao/id_sistema/3'
});

var MoodlesPorEntidadeCollection = Backbone.Collection.extend({
    model: Backbone.Model,
    url: '/default/integracao/dados-moodle'
});

// ItemViews

var FormIntegracaoView = Marionette.ItemView.extend({
    template: "#content-form-integracao",
    tagName: 'div',
    className: 'form-integracao',
    ui: {
        idSistema: '#id_sistema'
    },
    events: {
        'change #id_sistema': 'trocarSistema'
    },
    trocarSistema: function () {

        var that = this;

        switch (this.ui.idSistema.val()) {
            case '3':
                viewFormIntegracao.contentGeral.show(new DadosFinanceirosItemView());
                break;

            case '6':
                viewFormIntegracao.contentGeral.show(new CadastroMoodlesComposite());
                break;

            case '15':
                viewFormIntegracao.contentGeral.show(new BlackBoardItemView());
                break;
        }
    }
});

var BlackBoardItemView = Marionette.ItemView.extend({
    template: '#blackboard-item-view',
    tagName: 'div',
    initialize: function () {
        this.on('IntegrarEntidadeEvent', this.render, this);
    },
    integrarEntidade: function () {

        var that = this;

        $.get("/default/integracao/integrar-entidade/id_sistema/15/id_entidade/" + G2S.loggedUser.get('id_entidade'), {}, function (html) {
            that.$el.find('#integracao-saida-container').html(html);
            that.trigger("IntegrarEntidadeEvent");
            that.$el.find('#integracao-saida-container').html("Integração concluída! <br><br> Antes de começar a Sincronizar os dados, você precisa ter certeza de que os perfis foram criados no BlackBoard!");
        }).fail(function (response) {
            var msg = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ocorreu um erro na requisição!";
            if (typeof response.responseText != undefined) {
                msg = response.responseText;
                that.$el.find('#integracao-saida-container').html(response.responseText);
            }
            $.pnotify({title: 'Integração - Erro', type: 'error', text: msg});
        }).always(function () {
            loaded();
        });

    },
    reRender: function () {
        this.reRender();
    },
    integrarSalas: function () {
        this.requisicao("/default/integracao/integrar-salas");
    },
    verificarSalas: function () {
        this.requisicao("/default/integracao/verificar-salas");
    },
    integrarAlunos: function () {
        this.requisicao("/default/integracao/integrar-alunos");
    },
    integrarObservador: function () {
        this.requisicao("/default/integracao/integrar-observador");
    },
    integrarProfessores: function () {
        this.requisicao("/default/integracao/integrar-professores");
    },
    requisicao: function (url, callback) {
        var that = this;
        loading();

        $.get(url, {
            id_sistema: this.$el.find('#id_sistema').val()
        }, function (html) {
            that.$el.find('#integracao-saida-container').html(html);

            if (typeof callback != "undefined") {
                callback();
            }

        }).fail(function (response) {

            var msg = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ocorreu um erro na requisição!";
            if (typeof response.responseText != undefined) {
                msg = response.responseText;
                that.$el.find('#integracao-saida-container').html(response.responseText);
            }

            $.pnotify({title: 'Integração - Erro', type: 'error', text: msg});
        }).always(function () {
            loaded();
        });
    },
    events: {
        "click #btn-integrar-entidade": "integrarEntidade",
        "click #btn-integrar-sala": "integrarSalas",
        "click #btn-verificar-sala": "verificarSalas",
        "click #btn-integrar-alunos": "integrarAlunos",
        "click #btn-integrar-observador": "integrarObservador",
        "click #btn-integrar-professores": "integrarProfessores"
    },
    onRender: function (options, b, c) {
        var that = this;
        var veririca = new IntegracaoCollection();
        veririca.data = {id_entidade: G2S.loggedUser.get('id_entidade'), id_sistema: 15};
        veririca.fetch({
            success: function (collection, response) {
                that.$el.find('.nintegrado').hide();
            },
            error: function (collection, response) {
                that.$el.find('.integrado').hide();
            }
        });
    }

});

var MoodleItemView = Marionette.ItemView.extend({
    initialize: function (options) {
        options.model
            ? this.model = options.model
            : this.model = new EntidadeIntegracao();
    },
    tagName: 'div',
    template: '#templateMoodleItemView',
    events: {
        'click #salvarMoodle': 'salvarMoodle',
        'click #sincronizar': 'sincronizar',
        'click #sincronizarAlunos': 'sincronizarAlunos'
    },
    sincronizarAlunos: function (e) {
        that = this;
        loading();

        $(e.currentTarget).attr('disabled', true);

        $.ajax({
            url: '/default/integracao/sincronizar-alunos-moodle',
            type: 'post',
            dataType: 'html',
            data: '',
            success: function (response) {
                that.exibeRetornoIntegracao('Dados de Sincronização', response, '');
            },
            error: function () {
                $.pnotify({title: 'Erro', text: 'Ocorreu um erro ao tentar sincronizar Alunos.', type: 'error'});
            },
            complete: function () {
                loaded();
                $(e.currentTarget).attr('disabled', false);

            }
        });
    },
    sincronizar: function (e) {
        that = this;
        loading();

        $(e.currentTarget).attr('disabled', true);

        $.ajax({
            url: '/default/integracao/sincronizar-moodle',
            type: 'post',
            dataType: 'html',
            data: {'id_entidadeintegracao': this.model.get('id_entidadeintegracao')},
            success: function (response) {
                that.exibeRetornoIntegracao('Dados de Sincronização', response, '');
            },
            error: function () {
                $.pnotify({title: 'Erro', text: 'Ocorreu um erro ao tentar integrar.', type: 'error'});
            },
            complete: function () {
                loaded();
                $(e.currentTarget).attr('disabled', false);

            }
        });

    },
    exibeRetornoIntegracao: function (titulo, htmlBody, htmlFooter) {
        var dadosModal = new ModalBasicaModel({
            titulo_modal: titulo,
            conteudo_modal_body: htmlBody,
            conteudo_modal_footer: htmlFooter
        });
        this.$el.append('<div id="modalRetornoSincronizacao"></div>');
        var modal = new ModalBasicaView({
            el: $('#modalRetornoSincronizacao'),
            model: dadosModal
        });

        modal.renderModal();
        modal.modalShow();
        $('#modalBasica').modal('show');

    },
    salvarMoodle: function () {
        var param = {
            'id_entidadeintegracao': this.$el.find('#id_entidadeintegracao').val(),
            'st_codsistema': this.$el.find('#endereco').val(),
            'st_codchave': this.$el.find('#token').val(),
            'st_codarea': this.$el.find('#categoria').val(),
            'nu_contexto': this.$el.find('#contexto').val(),
            'nu_perfilsuporte': this.$el.find('#suporte').val(),
            'nu_perfilobservador': this.$el.find('#observador').val(),
            'nu_perfilalunoobs': this.$el.find('#alunoinstitucional').val(),
            'nu_perfilprojeto': this.$el.find('#coordenadorpedagogico').val(),
            'nu_perfildisciplina': this.$el.find('#coordenadordisciplina').val(),
            'nu_perfilalunoencerrado': this.$el.find('#alunoencerrado').val(),
            'st_nomeintegracao': this.$el.find('#st_nomeintegracao').val(),
            'st_codchavewsuny': this.$el.find('#st_codchavewsuny').val(),
            'st_titulo': this.$el.find('#st_titulo').val(),
            'id_situacao': this.$el.find('#id_situacao').val(),
            'bl_ativo': true
        };

        this.model.url = '/default/integracao/salvar-dados-moodle';
        var save = this.model.save(param);
        save.success(function () {
            $.pnotify({title: 'Sucesso', type: 'success', text: 'Parabéns, os dados foram cadastrados com sucesso!'});
            this.model.url = '/default/integracao/dados-moodle';
            this.model.fetch();
            viewFormIntegracao.contentGeral.show(new CadastroMoodlesComposite());
        }.bind(this));
        save.fail(function () {
            $.pnotify({
                title: 'Erro',
                type: 'error',
                text: 'Erro ao salvar os dados. Entre em contato com o administrador.'
            });
        });

        return this;
    },
    onRender: function () {
        if (this.model.get('id_situacao')) {
            this.$el.find('#id_situacao').val(this.model.get('id_situacao')).change();
        }
    }
});

var LinhaSelecaoMoodleItemView = Marionette.ItemView.extend({
    template: '#linha-selecao-moodle',
    tagName: 'tr',
    events: {
        'click #btnEditar': 'editarMoodle'
    },
    editarMoodle: function () {
        viewFormIntegracao.contentGeral.show(new MoodleItemView({model: this.model}));
    }
});

var LinhaMeioPagamentoItemView = Marionette.ItemView.extend({
    template: '#linha-meio-pagamento',
    collection: MeioPagamentoIntegracao,
    ui: {
        'codigoDoMeio':'#codigoDoMeio',
        'contaCaixa':'#contaCaixa'
    },
    events: {
        'change #codigoDoMeio': 'addCodigoDoMeio',
        'change #contaCaixa': 'addContaCaixa'
    },
    addCodigoDoMeio: function(){
        this.model.set('st_codsistema', this.ui.codigoDoMeio.val());
    },
    addContaCaixa: function(){
        this.model.set('st_codcontacaixa', this.ui.contaCaixa.val());
    }
});

var CadastroMoodlesComposite = Marionette.CompositeView.extend({
    tagName: 'div',
    template: '#tela-cadastro-moodles',
    childView: LinhaSelecaoMoodleItemView,
    childViewContainer: 'tbody',
    emptyView: Marionette.ItemView.extend({
        tagName: 'tr',
        template: _.template('<td colspan="4">Nenhum Moodle cadastrado nessa entidade.</td>')
    }),
    collection: new MoodlesPorEntidadeCollection(),
    events: {
        'click #cadastrar-moodle': 'cadastrarMoodle'
    },
    cadastrarMoodle: function () {
        viewFormIntegracao.contentGeral.show(new MoodleItemView());
    },
    onBeforeRender: function () {
        this.collection.fetch();
    },
    onRender: function () {
    }
});

var DadosFinanceirosItemView = Marionette.CompositeView.extend({
    tagName: 'div',
    template: '#dadosFinanceirosItemView',
    model: new EntidadeIntegracao(),
    childView: LinhaMeioPagamentoItemView,
    childViewContainer: '#lista-meio-pagamento',
    collection: new MeioPagamentoIntegracao(),
    emptyView: Marionette.ItemView.extend({
        tagName: 'tr',
        template: _.template('<p>Nenhum ítem</p>')
    }),
    ui: {
        'coligada': '#coligada',
        'centroDeCusto': '#centroDeCusto',
        'id_entidadeintegracao': '#id_entidadeintegracao'
    },
    setColigada: function () {
        this.model.set('st_codsistema', this.$el.find('input[name="coligada"]').val());
    },
    setCentroDeCusto: function () {
        this.model.set('st_codchave', this.$el.find('input[name="centroDeCusto"]').val());
    },
    events: {
        'change input[name="coligada"]': 'setColigada',
        'change input[name="centroDeCusto"]': 'setCentroDeCusto',
        'click #btnEnviar': 'enviar'
    },
    buscarDados: function () {
        this.model.fetch({
            url: "/default/integracao/dados-pagamento-totvs",
            beforeSend: loading,
            complete: loaded,
            success: function () {
                this.collection.fetch();
            }.bind(this)
        });
    },
    initialize: function () {
        thisTotvsFinanceiro = this;
        thisTotvsFinanceiro.buscarDados();
        thisTotvsFinanceiro.model.bind("change", this.render);
        return thisTotvsFinanceiro;
    },
    enviar: function () {


        var dadosFinanceiros = {
            'st_codsistema': thisTotvsFinanceiro.ui.coligada.val(),
            'st_codchave': thisTotvsFinanceiro.ui.centroDeCusto.val(),
            'id_entidadeintegracao': thisTotvsFinanceiro.ui.id_entidadeintegracao.val()
        };
        var valid = this.validacamposForm(dadosFinanceiros);

        if (valid) {
            console.log(this.collection.toJSON());
            $.ajax({
                url: '/default/integracao/salvar-meio-pagamento-integracao',
                type: 'POST',
                data: {
                    dados_financeiros: JSON.stringify(dadosFinanceiros),
                    meio_pagamento: JSON.stringify(this.collection.toJSON())
                },
                dataType: 'JSON',
                beforeSend: function () {
                    loading();
                },
                success: function (mensageiro) {
                    thisTotvsFinanceiro.ui.id_entidadeintegracao.val(mensageiro.mensagem.id_entidadeintegracao);
                    $.pnotify({
                        title: 'Sucesso',
                        type: 'success',
                        text: 'Parabéns, os dados foram cadastrados com sucesso!'
                    });
                    formIntegracao.trocarSistema();
                },
                error: function () {
                    $.pnotify({
                        title: 'Erro',
                        type: 'alert',
                        text: 'Erro no sistema, favor entrar em contato.'
                    });
                },
                complete: function () {
                    loaded();
                }
            });
            return false;
        }

    },
    validacamposForm: function (dadosFinanceiros) {
        if (dadosFinanceiros.st_codchave.length !== 10) {
            $.pnotify({
                title: 'Erro',
                type: 'alert',
                text: 'Centro de Custo tem que ter 10 caracteres.'
            });
            $('#centroDeCusto').focus();
            return false;
        }
        return true;
    }

});

var TotvsMeioPagamentoItemView = Marionette.ItemView.extend({
    template: '#meioDePagamentoItemView',
    setCodigoMeio: function () {
        this.model.set('st_codsistema', this.$el.find('input[name="codigoDoMeio"]').val());
    },
    setContaCaixa: function () {
        this.model.set('st_codcontacaixa', this.$el.find('input[name="contaCaixa"]').val());
    },
    onShow: function () {
        this.model.set('id_meiopagamentointegracao', this.$el.find('input[name="id_meiopagamentointegracao"]').val());
    },
    events: {
        'change input[name="codigoDoMeio"]': 'setCodigoMeio',
        'change input[name="contaCaixa"]': 'setContaCaixa'
    }
});

/**
 * Layout
 */
var FormIntegracaoLayout = Marionette.LayoutView.extend({
    template: '#form-integracao',
    tagName: 'div',
    className: 'container-fluid',
    regions: {
        contentGeral: '#content-geral',
        contentRegion: '#content-regiao',
        contentTotvs: '#content-totvs',
        contentTotvsMeioPagamento: '#content-meio-pagamento',
        contentTotvsDadosFinanceiros: '#content-form-financeiro',
        contentMoodle: '#content-moodle',
        contentBB: '#content-bb'
    },
    initialize: function () {
        loaded();
    }
});

var viewFormIntegracao = new FormIntegracaoLayout();

G2S.show(viewFormIntegracao);

var formIntegracao = new FormIntegracaoView();

viewFormIntegracao.contentRegion.show(formIntegracao);


var SmsEntidadeMensagemModel = Backbone.Model.extend({
    defaults: {
        st_mensagempadrao: '',
        st_textosistema: 'Texto Sistema',
        id_textosistema: '',
        is_editing: false
    },
    url: function() {
        return this.id ? '/api/sms-entidade-mensagem/' + this.id : '/api/sms-entidade-mensagem';
    },
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    }
});
var SmsEntidadeMensagemCollection = Backbone.Collection.extend({
    url: '/api/sms-entidade-mensagem',
    model: SmsEntidadeMensagemModel
});
var SmsEntidadeMensagemView = Backbone.View.extend({
    tagName: "tr",
    className: "",
    initialize: function(options) {
        this.render = _.bind(this.render, this);
        this.model.bind('change', this.render);
    },
    render: function(options) {
        var variables = this.model.toJSON();
        var tr;
        //alterana entre template de listagem e de editar
        if (this.model.is_editing) {
            tr = _.template($("#sms-entidade-template-edit").html(), variables);
        } else {
            tr = _.template($("#sms-entidade-template-list").html(), variables);
        }
        this.el.innerHTML = tr; //renderiza o template
        return this;
    },
    saveChanges: function() {
        that = this;
        this.model.set({
            id_textosistema: $(this.el).find('select[name=id_textosistema]').val(),
            st_textosistema: $(this.el).find('select[name=id_textosistema] option:selected').text(),
        });//seta o atributo
        //salva a model
        this.model.save(null, {
            //resposta da model
            success: function(model, response) {
                that.toggleEdit(); //alterna o modo de edição
                that.render();//renderiza
                $.pnotify({title: response.title, text: response.text, type: response.type});//mensagem de retorno
            },
            error: function(model, response) {
                $.pnotify({title: 'Erro ao atualizar o registro', text: 'Houve um problema ao atualizar o registro, tente novamente', type: 'error'});
            }
        });
    },
    toggleEdit: function() {
        this.oldSettings = this.model.toJSON();
        getTextoSistemaOptions(this.el, this.oldSettings.id_textosistema, 4);
        this.model.toggleEdit();
        this.render();
    },
    resetEdit: function() {
        this.model.set(this.oldSettings);
        this.toggleEdit();
        if (!this.model.id) {
            this.remove();
        }
    },
    events: {
        'click .edit-link': 'toggleEdit',
        'click .save-link': 'saveChanges',
        'click .cancel-link': 'resetEdit'
    }
});

var smsEntidadeCollecion = new SmsEntidadeMensagemCollection; //instancia a colecao
smsEntidadeCollecion.fetch({
    success: function() {
        render = new CollectionView({
            collection: smsEntidadeCollecion,
            childViewConstructor: SmsEntidadeMensagemView,
            childViewTagName: 'tr',
            el: $('#tableMensSms tbody')
        });
        render.render();
    }
});

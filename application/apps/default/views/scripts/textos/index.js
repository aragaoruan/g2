var TextosSistemaCollection = Backbone.Collection.extend({
    model: TextoSistema,
    url: "/api/textos"
});

// Categoria Texto
var SelectModel = Backbone.Model.extend({});
var SelectCollection = Backbone.Collection.extend({
    model: SelectModel,
    url: ''
});
var SelectView = Backbone.View.extend({
    tagName: 'option',
    initialize: function () {
        _.bindAll(this, 'render');
    },
    render: function () {
        $(this.el).attr('value', this.model.get('id')).text(this.model.get('text'));
        return this;
    }
});


$(function () {
    if($('#id').val()){
        if($('#id_textocategoria').val() == TEXTO_CATEGORIA.CABECALHO || $('#id_textocategoria').val() == TEXTO_CATEGORIA.RODAPE)
            $('.div-variaveis').hide();
        else if ($('#id_textocategoria').val() == TEXTO_CATEGORIA.DECLARACOES )
            $('.div-cabecalhoerodape ').removeClass('hide');
    }
    //ação do select Texto Exibição
    $('#id_textoexibicao').off().on('change', function () {
        if ($(this).val()) {
            var catTextoCollection = new SelectCollection();
            catTextoCollection.url = '/textos/get-texto-categoria-by-exibicao/idTextoExibicao/' + $(this).val();
            catTextoCollection.fetch({
                success: function () {
                    renderer = new CollectionView({
                        collection: catTextoCollection,
                        childViewConstructor: SelectView,
                        childViewTagName: 'option',
                        el: $('#id_textocategoria')
                    });
                    renderer.render();
                    $('#id_textocategoria').prepend('<option value="" selected>Selecione</option>');
                    $('#id_textocategoria').removeAttr('disabled');
                }
            });
        } else {
            $('#id_textocategoria').empty()
                .prepend('<option value="">Selecione a Exibição</option>').attr('disabled', 'disabled');
            $('#variaveis').empty()
                .prepend('<option value="">Selecione a Categoria do Texto</option>').attr('disabled', 'disabled');
            $('#st_textovariaveis').empty().attr('readonly', 'readonly');
        }
    });



    $('#id_textocategoria').off().on('change', function () {
        if ($(this).val()) {
            if($(this).val() == TEXTO_CATEGORIA.CABECALHO || $(this).val() == TEXTO_CATEGORIA.RODAPE){
                $('.div-variaveis').hide();
            }else{
                $('.div-variaveis').show();

                if($(this).val() == TEXTO_CATEGORIA.DECLARACOES){
                    $('.div-cabecalhoerodape').show();
                }else
                    $('.div-cabecalhoerodape').hide();

                var data = { id_textocategoria: $('#id_textocategoria').val(), st_textovariaveis: $('#st_textovariaveis').val() }

                var variaveisCollection = new SelectCollection();
                variaveisCollection.url = '/textos/get-variaveis?' + $.param(data);
                variaveisCollection.fetch({
                    success: function () {
                        renderer = new CollectionView({
                            collection: variaveisCollection,
                            childViewConstructor: SelectView,
                            childViewTagName: 'option',
                            el: $('#variaveis')
                        });
                        renderer.render();
                        $('#variaveis').removeAttr('disabled');
                        $('#st_textovariaveis').removeAttr('readonly');
                    }
                });
            }
        } else {
            $('#variaveis').empty()
                .prepend('<option value="">Selecione a Categoria do Texto</option>').attr('disabled', 'disabled');
        }
    });

    var digitando;

    var buscarTextoVariaveis = function(e) {

        var keyCode = e.keyCode || e.which;

        if(($(this).val() && ($(this).val().length >=3 || keyCode === 13)) || !$(this).val()){
            if(digitando){
                clearTimeout(digitando);
            }

            digitando = setTimeout(function(){
                var data = { id_textocategoria: $('#id_textocategoria').val(), st_textovariaveis: $('#st_textovariaveis').val() }

                var variaveisCollection = new SelectCollection();
                variaveisCollection.url = '/textos/get-variaveis?' + $.param(data);
                variaveisCollection.fetch({
                    success: function () {
                        renderer = new CollectionView({
                            collection: variaveisCollection,
                            childViewConstructor: SelectView,
                            childViewTagName: 'option',
                            el: $('#variaveis')
                        });
                        renderer.render();
                        $('#variaveis').removeAttr('disabled');
                    }
                });
            }, 1000);
        }

        if(keyCode === 13){
            e.preventDefault();
            return false;
        }

    }

    $('#st_textovariaveis').off().keypress(buscarTextoVariaveis);
    $('#st_textovariaveis').off().keyup(buscarTextoVariaveis);


    //insere o item clicado no multioptions no editor de texto
    $('#variaveis').off().on('click', function () {
        var variavel = $(this).find('option:selected').text();
        tinymce.activeEditor.execCommand('mceInsertContent', false, variavel);
    });
    //submete o formulário inteiro
    $("#btnSbmit").off().on('click', function () {
        var textoSistema = new TextoSistema();

        textoSistema.url = textoSistema.id ? '/api/textos/' + textoSistema.id : '/api/textos';

        textoSistema.set({
            id: ($("#id").val() ? $("#id").val() : null),
            st_textosistema: $("#st_textosistema").val(),
            dt_inicio: $("#dt_inicio").val(),
            dt_fim: $("#dt_fim").val(),
            id_orientacaotexto: $("#id_orientacaotexto").val(),
            st_texto: tinymce.get('editor').getContent(),
            id_sistuacao: 60,
            id_textocategoria: $("#id_textocategoria").val(),
            id_textoexibicao: $("#id_textoexibicao").val(),
            id_rodape: ($("#id_rodape").val() ? $("#id_rodape").val() : null),
            id_cabecalho: ($("#id_cabecalho").val() ? $("#id_cabecalho").val() : null)
        });
        textoSistema.save(null, {
            //resposta da model
            beforeSend: function () {
                loading();
            },
            success: function (model, response) {
                $("#id").val(response.id);
                $.pnotify({title: response.title, text: response.text, type: response.type});//mensagem de retorno
            },
            error: function (model, response) {
                $.pnotify({
                    title: 'Erro ao atualizar o registro',
                    text: 'Houve um problema ao atualizar o registro, tente novamente',
                    type: 'error'
                });
            },
            complete: function () {
                loaded();
            }
        });
        return false;
    });

    loadTinyMCE('editor');
    loaded();
});
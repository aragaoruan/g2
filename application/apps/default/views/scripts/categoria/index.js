var GlobalModel = Backbone.Model.extend({
    idAttribute: 'id_categoria',
    defaults: {
        id_categoria: null,
        st_nomeexibicao: '',
        st_categoria: '',
        id_usuariocadastro: null,
        id_entidadecadastro: null,
        id_uploadimagem: null,
        id_categoriapai: null,
        id_situacao: null,
        dt_cadastro: null,
        bl_ativo: 1,
        organizacoes: []
    },
    url: function () {
        return '/api/categoria/' + (this.id || '');
    }
});

var GlobalView = Marionette.ItemView.extend({
    template: '#global-template',
    tagName: 'div',
    model: new GlobalModel(),
    onRender: function () {
        var that = this;
        var elem = this.$el;

        loaded();

        // Carregar imagem, se possui
        if (this.model.id && this.model.get('id_uploadimagem')) {
            $.getJSON('/api/upload/' + this.model.get('id_uploadimagem'), function (response) {
                if (response.st_upload)
                    $('#imagem_uploaded').attr('src', '/upload/categoria/' + response.st_upload);
            });
        }


        // Listar Situação
        var Model = Backbone.Model.extend({
            idAttribute: 'id_situacao',
            defaults: {
                'id_situacao': null,
                'st_situacao': '[Selecione]',
                'st_tabela': '',
                'st_campo': '',
                'st_descricaosituacao': ''
            }
        });
        ComboboxView({
            el: '#id_situacao',
            model: Model,
            url: '/api/situacao?st_tabela=tb_categoria',
            optionLabel: 'st_situacao',
            optionSelectedId: that.model.get('id_situacao')
        });


        // Listar Categoria
        var Model = Backbone.Model.extend({
            idAttribute: 'id_categoria',
            defaults: {
                'id_categoria': null,
                'st_nomeexibicao': '[Selecione]'
            }
        });

        ComboboxView({
            el: '#id_categoriapai',
            model: Model,
            url: '/api/categoria',
            optionLabel: 'st_nomeexibicao',
            optionSelectedId: that.model.get('id_categoriapai'),
            emptyOption: true
        });

        this.carregaEntidade();
        return this;

    },
    carregaEntidade: function () {
        var that = this;
        var elem = this.$el;

        $(elem).find("#entidades").jstree({
            html_data: {
                ajax: {
                    url: 'util/arvore-entidade',
                    data: function (n) {
                        return {id: n.attr ? n.attr('id') : 0};
                    }
                }
            },
            "plugins": ['themes', 'html_data', 'checkbox', 'sort', 'ui']
        }).bind('loaded.jstree', function (event, data) {
            if ($("#id").val() != 0) {
                $.ajax({
                    url: 'categoria/recarrega-arvore/id_categoria/' + (that.model.id || ''),
                    type: 'POST',
                    dataType: 'json',
                    success: function (data) {
                        $("#entidades").jstree("get_unchecked", null, true).each
                        (function () {
                            for (var i = 0; i < data.length; i++) {
                                if (data[i] == this.id) {
                                    $.jstree._reference("#entidades").check_node('li#' + this.id);
                                }
                            }
                        });
                    }
                });
            }
        });
    },
    events: {
        'submit #form': 'actionSave',
        'click #bl_todasentidades': 'todasEntidades',
    },
    todasEntidades: function (e) {
        var elem = $(e.target);

        if (elem.is(':checked')) {
            $("#entidades").jstree("check_all");
            $('#entidades').slideUp();
        } else {
            $("#entidades").jstree("uncheck_all");
            $('#entidades').slideDown();
        }
    },
    actionSave: function (e) {
        e.preventDefault();

        var that = this;
        var elem = $(e.target);

        var values = {
            st_nomeexibicao: elem.find('[name="st_nomeexibicao"]').val(),
            st_categoria: elem.find('[name="st_categoria"]').val(),
            id_categoriapai: {
                id_categoria: Number(elem.find('[name="id_categoriapai"]').val())
            },
            id_situacao: {
                id_situacao: Number(elem.find('[name="id_situacao"]').val())
            },
            organizacoes: []
        };

        // Validar campos obrigatórios
        var errorMsg = null;
        var errorElem = null;

        if (values.st_categoria.trim() === '') {
            errorMsg = 'Insira a categoria';
            errorElem = $('[name="st_categoria"]');
        }
        else if (values.st_nomeexibicao.trim() === '') {
            errorMsg = 'Insira o nome de exibição';
            errorElem = $('[name="st_nomeexibicao"]');
        }
        else if (values.id_situacao.id_situacao === 0) {
            errorMsg = 'Informe a situação';
            errorElem = $('[name="id_situacao "]');
        }

        if (errorMsg) {
            $.pnotify({
                title: 'Erro',
                text: errorMsg,
                type: 'error'
            });
            errorElem.focus();
            return false;
        }


        var arquivos = $('[name="id_uploadimagem"]')[0].files;
        var formData = new FormData();

        for (var i = 0; i < arquivos.length; i++) {
            var file = arquivos[i];

            // Validar extensao do arquivo
            var extensions = ['.jpg', '.jpeg', '.png'];
            var extensaoValida = (new RegExp('(' + extensions.join('|').replace(/\./g, '\\.') + ')$')).test(file.name);

            if (extensaoValida)
                formData.append('arquivo', file, file.name);
            else {
                $.pnotify({
                    title: 'Aviso',
                    text: 'Esta extensão de arquivo não é permitida.',
                    type: 'warning'
                });
                return false;
            }
        }

        var progressBar = $('#upload-progress');

        $.ajax({
            url: 'api/upload?directory=upload/categoria',
            type: 'post',
            data: formData,
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false,
            beforeSend: function () {
                progressBar.show();
            },
            progressUpload: function (evt) {
                if (evt.lengthComputable) {
                    var value = parseInt((evt.loaded / evt.total * 100));
                    progressBar.find('.bar').css('width', value + '%');
                }
            },
            success: function (response, textStatus, jqXHR) {
                //if (response.id_upload) {
                //    return this.error();
                //}
                if (response.id_upload) {
                    values.id_uploadimagem = {
                        id_upload: response.id_upload
                    };
                }

                loading();
                that.model.set(values);

                // Salvar Organização
                $("#entidades").jstree('get_checked', null, true).each(function () {
                    //buscando valores da árvore
                    values.organizacoes.push(this.id);
                });


                that.model.save(null, {
                    success: function (model, response) {
                        $.pnotify({
                            title: 'Concluído',
                            text: 'O registro foi salvo com sucesso!',
                            type: 'success'
                        });
                        //that.model.set(response);
                        that.model.set(response);
                        that.render();
                    },
                    error: function () {
                        $.pnotify({
                            title: 'Erro',
                            text: 'Houve um erro ao tentar salvar o registro, tente novamente.',
                            type: 'error'
                        });
                    },
                    complete: function () {
                        loaded();
                    }
                });


            },
            error: function (jqXHR, textStatus, errorThrown) {
                $.pnotify({
                    title: 'Erro',
                    text: 'Houve um erro ao salvar o arquivo.',
                    type: 'error'
                });
                loaded();
                return false;
            }
        });


    }
});


// Render / Renderizar views
// Verificar se possui id no url
loading();
var GlobalIni = new GlobalView();

var url = window.location.href;
var idParam = Number(url.split('/').pop());
if (!isNaN(idParam)) {
    var model = new GlobalModel();
    model.id = idParam;
    model.fetch({
        success: function (model, response) {
            model.set(response);

            GlobalIni = new GlobalView({
                model: model
            });

            G2S.layout.content.show(GlobalIni);
        }
    });
} else {
    G2S.layout.content.show(GlobalIni);
}


/**
 * Funcao que monta os campos
 * <select> com as opcoes
 */
function ComboboxView(params, fn) {

    /**
     * @param params.el
     * @param params.model
     * @param params.url
     * @param params.optionLabel
     * @param params.optionSelectedId
     * @param fn
     */

    if (typeof params !== "object") {
        console.log("ComboboxView ERRO: \n" +
            "O parametro deve ser um OBJETO, com os índices: \n" +
            " - el: '#element' // seletor da tag select \n" +
            " - model: BackboneModel // instancia BackboneModel, que contenha os atributos 'url' e 'idAttribute'" +
            " - optionLabel: 'field' // campo do BackboneModel que sera usado na descricao da tag option" +
            " - [opcional] optionSelectedId: 'value' // valor que sera comparado com os options para determinar o selecionado");
        return false;
    }

    var Collection = Backbone.Collection.extend({
        model: params.model,
        url: params.url
    });

    var ItemView = Marionette.ItemView.extend({
        tagName: "option",
        render: function () {
            var elem = $(this.el);

            elem
                .attr('value', this.model.id)
                .html(this.model.get(params.optionLabel));

            if (params.optionSelectedId == this.model.id)
                elem.attr('selected', 'selected');

            return this;
        }
    });

    var Initialize = new Collection();
    Initialize.fetch({
        success: function () {
            var renderer = new CollectionView({
                collection: Initialize,
                childViewConstructor: ItemView,
                childViewTagName: 'option',
                el: $(params.el),
                onRender: function () {

                }
            });
            renderer.render();

            if (params.emptyOption) {
                $(params.el).prepend('<option value="">[Selecione]</option>');

                if (!$(params.el).find('[selected]').length)
                    $(params.el).find('option').first().attr('selected', 'selected');
            }

            if (typeof fn === 'function') {
                fn.call();
            }
        }
    });
}
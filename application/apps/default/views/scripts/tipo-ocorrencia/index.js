
var urlApi = '/api/tipo-ocorrencia/';
var idTableEdit = 'tipo-ocorrencia-template-edit';
var idTableList = 'tipo-ocorrencia-template-list';
var idTbodyList = 'data-tipo-ocorrencia';
var msgConfirmDelete = 'Tem certeza de que deseja apagar o registro?';

var TipoOcorrenciaModel = Backbone.Model.extend({
    defaults: {
        st_categoriaocorrencia: 'Tipo Ocorrencia',
        id_tipoocorrencia: '1',
        id_situacao: '95',
        st_situacao: 'Ativo',
        st_tipoocorrencia: 'Aluno',
        is_editing: false
    },
    url: function() {
        return this.id ? urlApi + this.id : urlApi;
    },
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    }
});
 
var TipoOcorrenciaCollection = Backbone.Collection.extend({
    model: TipoOcorrenciaModel,
    url: urlApi
});

TipoOcorrenciaView = Backbone.View.extend({
    tagName: "tr",
    className: "tr-class-name",
    initialize: function(options) {
        this.render = _.bind(this.render, this);
        this.model.bind('change', this.render);
    },
    render: function(options) {
        var variables = this.model.toJSON();
        var tr;
        //alterana entre template de listagem e de editar
        if (this.model.is_editing) {
            tr = _.template($('#' + idTableEdit).html(), variables);
        } else {
            tr = _.template($('#' + idTableList).html(), variables);
        }
        this.el.innerHTML = tr; //renderiza o template
        return this;
    },
    delete: function() {
        that = this;
        bootbox.confirm(msgConfirmDelete, function(result) {
            if (result) {
                that.model.destroy({
                    success: function(model, response) {
                        $.pnotify({title: response.title, text: response.text, type: response.type});
                        that.remove();
                    },
                    error: function(model, response) {
                        $.pnotify({title: response.title, text: response.text, type: response.type});
                    }
                });
            }
        });
    },
    saveChanges: function() {
        that = this;
        var st_categoriaocorrencia = $(this.el).find('input[name=st_categoriaocorrencia]').val();
        var id_situacao = $(this.el).find('select[name=id_situacao]').val();
        var st_situacao = $(this.el).find('select[name=id_situacao] option:selected').text();
        var id_tipoocorrencia = $(this.el).find('select[name=id_tipoocorrencia]').val();
        var st_tipoocorrencia = $(this.el).find('select[name=id_tipoocorrencia] option:selected').text();

        this.model.set({id_situacao: id_situacao, st_situacao:st_situacao, id_tipoocorrencia: id_tipoocorrencia, st_categoriaocorrencia: st_categoriaocorrencia, st_tipoocorrencia:st_tipoocorrencia});//seta o atributo
        //salva a model
        this.model.save(null, {
            //resposta da model
            success: function(model, response) {
                that.toggleEdit(); //alterna o modo de edição
                that.render();//renderiza
                $.pnotify({title: response.title, text: response.text, type: response.type});//mensagem de retorno
            },
            error: function(model, response) {
                $.pnotify({title: 'Erro ao atualizar o registro', text: 'Houve um problema ao atualizar o registro, tente novamente', type: 'error'});
            }
        });
    },
    toggleEdit: function() {
        this.oldSettings = this.model.toJSON();

        this.model.toggleEdit();
        this.render();
    },
    resetEdit: function() {
        this.model.set(this.oldSettings);
        this.toggleEdit();
        if (!this.model.id) {
            this.remove();
        }
    },
    events: {
        'click .edit-link': 'toggleEdit',
        'click .save-link': 'saveChanges',
        'click .delete-link': 'delete',
        'click .cancel-link': 'resetEdit'
    }
});

var tipoocorrencia = new TipoOcorrenciaCollection; //instancia a colecao
tipoocorrencia.fetch({
    success: function() {
        tiposOcorrencias = new CollectionView({
            collection: tipoocorrencia,
            childViewConstructor: TipoOcorrenciaView,
            childViewTagName: 'tr',
            el: $('#' + idTbodyList)
        });
        tiposOcorrencias.render();
    }
});

function novoTipoOcorrencia() {
    $('html, body').animate({scrollTop: 3000}, 1);
    var model = new TipoOcorrenciaModel();
    model.toggleEdit();
    tiposOcorrencias.add(model);
}
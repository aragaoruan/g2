/**
 * User: Débora Castro <debora.castro@unyleya.com.br>
 * Date: 04/12/13
 */
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * *                             MODELS                                * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
$.ajaxSetup({async: false});
$.getScript('/js/moment-with-langs.min.js');
$.getScript('/js/backbone/models/matricula.js');
$.getScript('/js/backbone/models/vw-matricula.js');
$.getScript('/js/backbone/models/modalbasica.js');
$.getScript('js/backbone/views/Util/modal_basica.js');

var telaAlocacao = false;
var modal = false;
var dadosModal = false;
var salaPermanente = false;
var stEvolucaoDisciplina = '';
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * *                        ESTATICOS                                  * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
var PermissaoFuncionalidade = {
    btnAlocarNormal: false,
    btnDesalocarNormal: false,
    btnAlocarPrr: false,
    btnIntegrar: false,
    btnAlocarSalasEncerradas: false,
    funcionalidade: 207, //Codigo da funcionalidade
    permissoes: {
          alocar: PERMISSAO.ALOCAR_ALUNOS_SALA_DE_AULA
        , integrar: PERMISSAO.INTEGRACAO_DE_ALUNOS
        , prr: PERMISSAO.PROGRAMA_DE_RECUPERACAO_DE_ALUNOS
        , salasEncerradas: PERMISSAO.ALOCAR_EM_SALAS_ENCERRADAS
    }
};

/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * *                        COLLECTIONS                                * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
var VwMatriculasCollection = Backbone.Collection.extend({
    model: VwMatricula
});

var VwAlocacaoAlunoSalaCollection = Backbone.Collection.extend({
    model: VwAlocacaoAlunoSala
});

var VwSalaDisciplinaAlocacaoCollection = Backbone.Collection.extend({
    model: VwSalaDisciplinaAlocacao
});

/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * *                             VIEWS                                 * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
var ListaMatriculasView = Backbone.View.extend({
    el: $('#container-alocar-lista'),
    model: new VwMatricula(),
    dadosAlocacaoView: null,
    initialize: function () {
        this.render();
    },
    render: function () {

        var pessoa = this.getPessoa();
        if (pessoa != null && pessoa.id) {
            var that = this;

            $.get('/js/backbone/templates/alocar/form-alocar-lista.html', {"_": $.now()}, function (template) {
                that.template = _.template(template);
                $(that.el).append(that.template(pessoa));
            });
            this.popularLista(pessoa.id);
        }
        $('#container-alocar-lista').show();
        return this;
    },
    events: {
        "change #lista-alocar-matriculas": "carregarDadosAlocacao",
        "click #autoalocar": "autoAlocar"
    },
    carregarDadosAlocacao: function () {

        var arr = $('#lista-alocar-matriculas').val().split('#');

        var modelMatriculaAtiva = collectionMatricula.findWhere({
            id_matricula: parseInt(arr[1]),
            id_projetopedagogico: parseInt(arr[0])
        });
        if (modelMatriculaAtiva) {
            stEvolucaoDisciplina = modelMatriculaAtiva.get("st_evolucao");
        }

        loading();
        $.ajaxSetup({async: false});
        if (VerificarPerfilPermissao(PermissaoFuncionalidade.permissoes.alocar, PermissaoFuncionalidade.funcionalidade).length) {
            PermissaoFuncionalidade.btnAlocarNormal = true;
            $('#th-alocar-normal').show();
        }

        if (VerificarPerfilPermissao(PermissaoFuncionalidade.permissoes.integrar, PermissaoFuncionalidade.funcionalidade).length) {
            PermissaoFuncionalidade.btnIntegrar = true;
            $('#th-reintegrar').show();
        }

        if (VerificarPerfilPermissao(PermissaoFuncionalidade.permissoes.prr, PermissaoFuncionalidade.funcionalidade).length) {
            PermissaoFuncionalidade.btnAlocarPrr = true;
            $('#th-alocar-prr').show();
        }

        //Verifica se o usuário tem permissão de ver o botão de listar as salas encerradas
        if (VerificarPerfilPermissao(PermissaoFuncionalidade.permissoes.salasEncerradas, PermissaoFuncionalidade.funcionalidade).length) {
            PermissaoFuncionalidade.btnAlocarSalasEncerradas = true;
        }


        $.ajaxSetup({async: true});


        if (arr == "") {
            $('#tabela-retorno-pesquisa-alocar tbody').html('');
            $('#container-alocar').hide();
        } else {
            if (this.dadosAlocacaoView != null) {
                this.dadosAlocacaoView.destroyMe();
            }

            var collectionDadosAlocar = new VwAlocacaoAlunoSalaCollection();
            collectionDadosAlocar.url = '/alocar-alunos/retorna-dados-alocacao?id_projetopedagogico=' + arr[0] + '&id_matricula=' + arr[1];
            collectionDadosAlocar.fetch({
                success: function () {
                    // Se for graduação, nao mostrar o botao auto alocar
                    if (G2S.loggedUser.get('id_linhadenegocio') == 2) {
                        $('#autoalocar').hide();
                    } else {
                        $('#autoalocar').show();

                        if (stEvolucaoDisciplina == "Concluinte"
                            || stEvolucaoDisciplina == "Certificado") {
                            $("#autoalocar").attr("disabled", true);
                        } else {
                            $("#autoalocar").attr("disabled", false);
                        }



                    }

                    var comboView = new CollectionView({
                        collection: collectionDadosAlocar,
                        childViewTagName: 'tr',
                        childViewConstructor: disciplinasView,
                        el: $('#tabela-retorno-pesquisa-alocar tbody')
                    });
                    comboView.render();
                },
                error: function () {
                    loaded();
                },
                complete: function () {
                    loaded();
                }
            });
            this.dadosAlocacaoView = new disciplinasView();
        }

    },
    getPessoa: function () {
        return JSON.parse(localStorage.getItem('pessoaAutocomplete'));
    },
    popularLista: function (id) {

        collectionMatricula = new VwMatriculasCollection();
        collectionMatricula.url = '/api/vw-matricula/id_usuario/' + id;
        collectionMatricula.fetch({
                success: function () {
                    var comboView = new CollectionView({
                        collection: collectionMatricula,
                        childViewTagName: 'option',
                        childViewConstructor: OptionMatriculaView,
                        el: $('#lista-alocar-matriculas')
                    });
                    comboView.render();
                },
                complete: function () {
                    $('#lista-alocar-matriculas').prepend('<option value="" selected="selected">Escolha uma opção</option>');
                }
            }
        );
    },
    autoAlocar: function () {
        // Somente se nao for GRADUACAO
        if (G2S.loggedUser.get('id_linhadenegocio') != 2) {
            var arr = $('#lista-alocar-matriculas').val().split('#');
            var that = this;

            $.ajax({
                url: 'alocar-alunos/sincronizar-auto-alocar',
                type: 'post',
                dataType: 'json',
                data: {id_matricula: arr[1], id_projetopedagogico: arr[0]},
                success: function (response) {
                    $.pnotify({title: response.title, text: response.text, type: response.type});
                },
                error: function (response) {
                    $.pnotify({title: response.title, text: response.text, type: response.type});
                },
                complete: function () {
                    $('#tabela-retorno-pesquisa-alocar tbody').html('');
                    $('#container-alocar').hide();
                    that.carregarDadosAlocacao();
                }
            });
        }
    }
});

var OptionMatriculaView = Backbone.View.extend({
    template: _.template('<%=id_projetopedagogico%>'),
    render: function () {
        this.$el.attr('value', this.model.get('id_projetopedagogico') + '#' + this.model.get('id_matricula'));
        this.$el.text(this.model.get('id_matricula') + ' - ' + this.model.get('st_projetopedagogico') + ' - ' + this.model.get('st_evolucao'));
        return this;
    }
});

var OptionSalaNormalView = Backbone.View.extend({
    template: _.template('<%=id_matriculadisciplina%>'),
    render: function () {
        this.$el.attr('value', this.model.get('id_saladeaula') + '#' + this.model.get('id_matriculadisciplina'));
        this.$el.text(this.model.get('dt_abertura') + ' - ' + this.model.get('st_saladeaula'));
        return this;
    }
});

var disciplinasView = Backbone.View.extend({
    model: VwAlocacaoAlunoSala,
    initialize: function () {
    },
    render: function () {
        var variables = this.model.attributes;
        variables.permissaoBtnAlocar = PermissaoFuncionalidade.btnAlocarNormal;
        variables.permissaoBtnReintegrar = PermissaoFuncionalidade.btnIntegrar;
        variables.permissaoBtnAlocarPrr = PermissaoFuncionalidade.btnAlocarPrr;
        variables.permissaoBtnListarEncerradas = PermissaoFuncionalidade.btnAlocarSalasEncerradas;
        var temp;
        temp = _.template($('#disciplina-alocar-aluno').html(), variables);
        $('#container-alocar').show();
        this.$el.html(temp);
        $('.informacao').tooltip();
        this.exibirBtOpcoes();
        return this;

    },
    events: {
        "click .normal": "comboSalaNormal",
        "click .salvarnormal": "validarAlocacoes",
        "click .cancelarnormal": "alterarOperacaoNormal",
        "click .desalocarnormal": "desalocarNormal",
        "click .prr": "comboSalaPrr",
        "click .salvarprr": "validarAlocacoes",
        "click .cancelarprr": "alterarOperacaoPrr",
        "click .reintegrar": "reintegrarAlocacao",
        "click .estendernormal": "estenderAlocacaoNormal",
        "click .estenderprr": "estenderAlocacaoPrr",
        "click .btExibeModal": "montarModalOpcoes",
        "click .listaencerradas": function () {
            loading();
            this.comboSalaNormal(true);
        }
    },
    exibirBtOpcoes: function () {
        if (this.model.get('id_tiposalanormal') == 1) {

            template = ' <a class="btn btExibeModal" href="#modalBasica" id="btExibeModaln-'+ this.model.get('id_disciplina') + '" '
                + 'data-toggle="modal" name="btExibeModaln">Opções</a>';
            this.$el.find('#botoes' + this.model.get('id_disciplina')).append(template);

        }
        if (this.model.get('id_alocacaoprr') != '') {
            templatep = ' <a class="btn btExibeModal" href="#modalBasica" id="btExibeModalp-'+ this.model.get('id_disciplina') + '" '
                            + 'data-toggle="modal" name="btExibeModalp">Opções</a>';
            this.$el.find('#botoesp' + this.model.get('id_disciplina')).append(templatep);
        }
        //Desabilita os botões de ALOCAR, DESALOCAR, OPÇÕES, SALVAR E CANCELAR,
        // nas colunas "Alocar Normal" e Alocar Prr se a ST_EVOLUCAO for 15 ou 16
        // se a matriculadisciplina estiver trancada
        if (stEvolucaoDisciplina == "Concluinte"
            || stEvolucaoDisciplina == "Certificado" ||  (this.model.get('id_evolucaomatriculadisciplina') == EVOLUCAO.TB_MATRICULA_DISCIPLINA.TRANCADA))  {


            this.$el.find(".normal").attr("disabled", true);
            this.$el.find(".salvarnormal").attr("disabled", true);
            this.$el.find(".cancelarnormal").attr("disabled", true);
            this.$el.find(".desalocarnormal").attr("disabled", true);

            this.$el.find(".prr").attr("disabled", true);
            this.$el.find(".salvarprr").attr("disabled", true);
            this.$el.find(".cancelarprr").attr("disabled", true);

            this.$el.find(".btExibeModal").attr("disabled", true);

            if(this.model.get('id_evolucaomatriculadisciplina') == EVOLUCAO.TB_MATRICULA_DISCIPLINA.TRANCADA){
                this.$el.find(".reintegrar").attr("disabled", true);
            }

            return false;

        } else {

            if(this.model.get('id_situacaomatriculadisciplina') == this.model.get('id_situacaoproibidaparaalocacao')){
                this.$el.find(".normal").attr("disabled", true);
                this.$el.find(".prr").attr("disabled", true);
                this.$el.find(".reintegrar").attr("disabled", true);
            }else{
                this.$el.find(".normal").attr("disabled", false);
                this.$el.find(".prr").attr("disabled", false);
            }

            this.$el.find(".salvarnormal").attr("disabled", false);
            this.$el.find(".cancelarnormal").attr("disabled", false);
            this.$el.find(".desalocarnormal").attr("disabled", false);

            this.$el.find(".salvarprr").attr("disabled", false);
            this.$el.find(".cancelarprr").attr("disabled", false);

            this.$el.find(".btExibeModal").attr("disabled", false);

            return true;

        }

    },
    montarModalOpcoes: function (e) {
        //Desabilita os botões de OPÇÕES nas colunas "Alocar Normal" e Alocar Prr se a ST_EVOLUCAO for 15 ou 16
        if (!(stEvolucaoDisciplina == "Concluinte"
            || stEvolucaoDisciplina == "Certificado")) {
            htmlBody = this.modalBody(e.currentTarget.name);
            htmlFooter = this.modalFooter(e.currentTarget.id, e.currentTarget.name);
            dadosModal = new ModalBasicaModel({
                titulo_modal: 'Opções',
                conteudo_modal_body: htmlBody,
                conteudo_modal_footer: htmlFooter
            });
            modal = new ModalBasicaView({
                el: $('#modalOpcoesAlocacao'),
                model: dadosModal
            });
            modal.renderModal();
        }
    },
    modalBody: function (name) {
        var html = '<div class="row-fluid">'
            + '<div class="span12"><div class="span6"><label>Início: ';
        if (name == 'btExibeModaln') {
            html += this.model.get('dt_inicioalocacaonormal')
                + '</label></div>'
                + '<div class="span6"><label>Dias de acesso da sala: '
                + this.model.get('nu_diasextensaonormal')
                + '</label></div>'
                + '<div class="row-fluid">'
                + '<div class="span12"><label>Dias restantes de acesso (com a extensão atual): '
                + this.model.get('nu_diasacessoatualn')
                + '</label></div>'
                + '</div>'
                + '<div class="row-fluid">'
                + '<div class="span12"><label>Dias de extensão do aluno: <input type="number" id="nu_diasaluno" name="nu_diasaluno" value="' +
                $('#nu_extensaodiassetadan' + this.model.get('id_disciplina')).val() +
                '" ></label></div>';
        } else {
            html += this.model.get('dt_inicioalocacaoprr') + '</label></div>'
                + '<div class="span6"><label>Dias de acesso da sala: ' + this.model.get('nu_diasextensaoprr')
                + '</label></div>'
                + '<div class="row-fluid">'
                + '<div class="span12"><label>Dias restantes de acesso (com a extensão atual): '
                + this.model.get('nu_diasacessoatualp') + '</label></div>'
                + '</div>'
                + '<div class="row-fluid">'
                + '<div class="span12"><label>Dias de extensão do aluno: <input type="number" id="nu_diasaluno" name="nu_diasaluno" value="'
                + $('#nu_extensaodiassetadap' + this.model.get('id_disciplina')).val()
                + '" ></label></div>';
        }
        html += '</div>'
            + '</div> '
            + '</div>';
        return html;
    },
    modalFooter: function (id, name) {
        var id_disciplina = id.split('-');

        var html = '<button type="reset" id="salvarOpcoesAlocacao" onclick="';

        if (name == 'btExibeModaln') {
            html += 'salvarOpcoes(' + id_disciplina[1] + ',' + $('#idAlocacaoNormal' + id_disciplina[1]).val() + ');" ' +
                'class="salvarOpcoes btn btn-success">Salvar</button>' +

                '<button type="reset" id="desalocaAluno" ' +
                'onclick="desalocarAluno(' + id_disciplina[1] + ',' + $('#idAlocacaoNormal' + id_disciplina[1]).val() + ');" ' +
                'class="desalocaAluno btn btn-warning">Desalocar</button>';
        } else {
            html += 'salvarOpcoes(' + id_disciplina[1] + ',' + $('#idAlocacaoPrr' + id_disciplina[1]).val() + ');" ' +
                'class="salvarOpcoes btn btn-success">Salvar</button>' +

                '<button type="reset" id="desalocaAluno" ' +
                'onclick="desalocarAluno(' + id_disciplina[1] + ',' + $('#idAlocacaoPrr' + id_disciplina[1]).val() + ');" ' +
                'class="desalocaAluno btn btn-warning">Desalocar</button>';

        }

        html += '<button type="reset" id="cancelarOpcoes" onclick="cancelaOpcoes();" class="cancelarOpcoes btn btn-danger">Cancelar</button>';

        return html;
    },
    comboSalaNormal: function (bl_encerradas) {
        var that = this;
        var arr = $('#lista-alocar-matriculas').val().split('#');


        var collectionSalaNormal = new VwSalaDisciplinaAlocacaoCollection();
        collectionSalaNormal.url = '/alocar-alunos/retorna-sala-alocacao?id_disciplina=' + that.model.get('id_disciplina') + '&id_matricula=' + arr[1] + '&id_categoriasala=1';
        if(bl_encerradas !== undefined && bl_encerradas === true)
            collectionSalaNormal.url += '&bl_encerradas=true';
        collectionSalaNormal.fetch({
                success: function () {

                    if(!(bl_encerradas === true))
                        that.alterarOperacaoNormal();

                    var comboView = new CollectionView({
                        collection: collectionSalaNormal,
                        childViewTagName: 'option',
                        childViewConstructor: OptionSalaNormalView,
                        el: $('#comboSt_saladeaulanormal' + that.model.get('id_disciplina'))
                    });
                    comboView.render();
                    $('#comboSt_saladeaulanormal' + that.model.get('id_disciplina') + ' option[value=' + that.model.get('id_saladeaulanormal') + '#' + that.model.get('id_matriculadisciplinanor') + ']').attr('selected', 'selected').addClass('text-warning');
                    loaded();
                }
            }
        );

    },
    validarAlocacoes: function (e) {
        var that = this;

        var $button = $(e.currentTarget);

        $button.prop("disabled", true);

        loading();

        if ($button.prop('name') === 'normal') {
            if ($('#comboSt_saladeaulanormal' + that.model.get('id_disciplina')).val()) {
                var array = $('#comboSt_saladeaulanormal' + that.model.get('id_disciplina')).val().split('#');
                var categoria = 1;
            } else {
                loaded();
                $.pnotify({title: 'Aviso!', text: ' Nenhuma Sala foi selecionada', type: 'erro'});
                return false;
            }
        } else {
            if ($('#st_saladeaulaprr' + that.model.get('id_disciplina')).val()) {
                var array = $('#st_saladeaulaprr' + that.model.get('id_disciplina')).val().split('#');
                var categoria = 2;
            } else {
                loaded();
                $.pnotify({title: 'Aviso!', text: ' Nenhuma Sala foi selecionada', type: 'erro'});
                return false;
            }

        }

        $.ajax({
            url: '/alocar-alunos/validar-alocacao',
            type: 'post',
            dataType: 'json',
            data: {
                id_saladeaula: array[0],
                id_matriculadisciplina: array[1],
                id_categoriasala: categoria,
                id_tipodisciplina: that.model.get('id_tipodisciplina')
            },
            success: function (response) {
                var bl_disabled = '';

                if (+response.tipo === 0) {
                    bl_disabled = 'hide';
                }

                bootbox.dialog({
                    message: response.text,
                    title: 'ATENÇÃO!',
                    buttons: {
                        success: {
                            label: "Confirmar",
                            className: "btn-success " + bl_disabled,
                            disabled: bl_disabled,
                            callback: function (evt) {
                                $(evt.currentTarget).add(e.currentTarget).prop("disabled", true).text("Aguarde...");

                                if (e.currentTarget.name == 'normal') {
                                    that.salvarAlocacao(1);
                                } else {
                                    that.salvarAlocacao(2);
                                }
                            }
                        },
                        danger: {
                            label: "Cancelar",
                            className: "btn-danger",
                            callback: function () {
                                close();
                            }
                        }
                    }
                });
            },
            error: function (response) {
                $.pnotify({title: response.title, text: response.text, type: response.type});
            },
            complete: function () {
                loaded();
                $button.prop("disabled", false);
            }
        });
    },
    salvarAlocacao: function (categoria) {
        var that = this;
        //Variavel que recupera o ID do projeto pedagogico;
        var arr = $('#lista-alocar-matriculas').val().split('#');

        if (categoria == 1) {
            var array = $('#comboSt_saladeaulanormal' + that.model.get('id_disciplina')).val().split('#');
            var dt_inicio = $('#dt_inicioalocacaon' + that.model.get('id_disciplina')).val();
        } else {
            var array = $('#st_saladeaulaprr' + that.model.get('id_disciplina')).val().split('#');
            var dt_inicio = $('#dt_inicioalocacaop' + that.model.get('id_disciplina')).val();
        }

        loading();

        $.ajax({
            url: 'alocar-alunos/salvar-alocacao',
            type: 'post',
            dataType: 'json',
            data: {
                id_saladeaula: array[0],
                id_matriculadisciplina: array[1],
                id_categoriasala: categoria,
                dt_inicio: dt_inicio,
                id_aluno: pessoa.id_usuario,
                id_prjPedagogico: arr[0]
            },
            success: function (response) {

                $.pnotify({
                    title: response["alocacao"].title,
                    text: response["alocacao"].text,
                    type: response["alocacao"].type
                });

                if (response["email"]) {
                    //Mensagem para aluno
                    $.pnotify({
                        title: response["email"]["aluno"].title,
                        text: response["email"]["aluno"].text,
                        type: response["email"]["aluno"].type
                    });

                    //Mensagem para professor
                    $.pnotify({
                        title: response["email"]["prof"].title,
                        text: response["email"]["prof"].text,
                        type: response["email"]["prof"].type
                    });

                    //Mensagem para coordenador
                    $.pnotify({
                        title: response["email"]["coord"].title,
                        text: response["email"]["coord"].text,
                        type: response["email"]["coord"].type
                    });
                }

            },
            error: function (response) {
                $.pnotify({
                    title: response["alocacao"].title,
                    text: response["alocacao"].text,
                    type: response["alocacao"].type
                });
            },
            complete: function () {
                loaded();
                that.recarregaDados();
            }
        });

    },
    desalocarNormal: function () {
        var that = this;
        desalocarAluno(that.model.get('id_disciplina'), $('#idAlocacaoNormal' + that.model.get('id_disciplina')).val());
    },
    comboSalaPrr: function () {
        var that = this;
        var arr = $('#lista-alocar-matriculas').val().split('#');
        var collectionSalaNormal = new VwSalaDisciplinaAlocacaoCollection();
        collectionSalaNormal.url = '/alocar-alunos/retorna-sala-alocacao?id_disciplina=' + that.model.get('id_disciplina') + '&id_matricula=' + arr[1] + '&id_categoriasala=2';
        collectionSalaNormal.fetch({
                success: function () {
                    that.alterarOperacaoPrr();
                    var comboView = new CollectionView({
                        collection: collectionSalaNormal,
                        childViewTagName: 'option',
                        childViewConstructor: OptionSalaNormalView,
                        el: $('#st_saladeaulaprr' + that.model.get('id_disciplina'))
                    });
                    comboView.render();
                    $('#st_saladeaulaprr' + that.model.get('id_disciplina') + ' option[value=' + that.model.get('id_saladeaulaprr') + '#' + that.model.get('id_matriculadisciplinaprr') + ']').attr('selected', 'selected').addClass('text-warning');

                }
            }
        );
    },
    reintegrarAlocacao: function () {

        that = this;
        if (that.model.get('id_matriculadisciplinaprr') == '' && that.model.get('id_matriculadisciplinanor') != '') {
            var matriculadisciplina = that.model.get('id_matriculadisciplinanor');
            var saladeaula = that.model.get('id_saladeaulanormal');
            var alocacao = that.model.get('id_alocacaonormal');
        } else {
            if (that.model.get('id_matriculadisciplinanor') != '') {
                var matriculadisciplina = [that.model.get('id_matriculadisciplinaprr'), that.model.get('id_matriculadisciplinanor')];
                var saladeaula = [that.model.get('id_saladeaulaprr'), that.model.get('id_saladeaulanormal')];
                var alocacao = [that.model.get('id_alocacaoprr'), that.model.get('id_alocacaonormal')];
            } else {
                if (that.model.get('id_matriculadisciplinaprr') != '') {
                    var matriculadisciplina = that.model.get('id_matriculadisciplinaprr');
                    var saladeaula = that.model.get('id_saladeaulaprr');
                    var alocacao = that.model.get('id_alocacaoprr');
                }
            }
        }

        loading();

        $.ajax({
            url: 'alocar-alunos/reintegrar-alocacao',
            type: 'post',
            dataType: 'json',
            data: {id_saladeaula: saladeaula, id_matriculadisciplina: matriculadisciplina, id_alocacao: alocacao},
            success: function (response) {
                $.pnotify({title: response.title, text: response.text, type: response.type});
            },
            error: function (response) {
                $.pnotify({title: response.title, text: response.text, type: response.type});
            },
            complete: function () {
                loaded();
            }
        });
    },
    recarregaDados: function () {
        loading();
        var arr = $('#lista-alocar-matriculas').val().split('#');
        var collectionDadosAlocar = new VwAlocacaoAlunoSalaCollection();
        collectionDadosAlocar.url = '/alocar-alunos/retorna-dados-alocacao?id_projetopedagogico=' + arr[0] + '&id_matricula=' + arr[1];
        collectionDadosAlocar.fetch({
            success: function () {
                var comboView = new CollectionView({
                    collection: collectionDadosAlocar,
                    childViewTagName: 'tr',
                    childViewConstructor: disciplinasView,
                    el: $('#tabela-retorno-pesquisa-alocar tbody')
                });
                comboView.render();
            },
            complete: function () {
                loaded();
            }
        });
    },
    destroyMe: function () {
        this.undelegateEvents();
        $(this.el).removeData().unbind();
        this.el.remove();
        Backbone.View.prototype.remove.call(this);
    },
    alterarOperacaoNormal: function () {
        $('#salaNormal' + this.model.get('id_disciplina')).fadeToggle();
        $('#n' + this.model.get('id_disciplina')).fadeToggle();
        $('#comboSt_saladeaulanormal' + this.model.get('id_disciplina')).fadeToggle();
        $('#ns' + this.model.get('id_disciplina')).fadeToggle();
        $('#nc' + this.model.get('id_disciplina')).fadeToggle();
        if (this.model.get('id_tiposalanormal') == 1) {
            $('#dataNormal' + this.model.get('id_disciplina')).fadeToggle();
        }
        $('#nse' + this.model.get('id_disciplina')).fadeToggle();
        $('#dn' + this.model.get('id_disciplina')).fadeToggle();

    },
    alterarOperacaoPrr: function () {
        $('#salaPrr' + this.model.get('id_disciplina')).fadeToggle();
        $('#p' + this.model.get('id_disciplina')).fadeToggle();
        $('#st_saladeaulaprr' + this.model.get('id_disciplina')).fadeToggle();
        $('#ps' + this.model.get('id_disciplina')).fadeToggle();
        $('#pc' + this.model.get('id_disciplina')).fadeToggle();
        $('#dataPrr' + this.model.get('id_disciplina')).fadeToggle();
        $('#btExibeModalp-' + this.model.get('id_disciplina')).fadeToggle();

    }
});

function desalocarAluno(id, id_alocacao) {
    $('#modalBasica').modal('hide');

    bootbox.confirm('Tem certeza de que deseja desalocar esse aluno nesta sala?', function (resultNormal) {
        if (resultNormal) {
            loading();
            $.ajax({
                url: 'alocar-alunos/desalocar-aluno',
                type: 'post',
                dataType: 'json',
                data: {id_alocacao: id_alocacao},
                success: function (response) {
                    $.pnotify({title: response.title, text: response.text, type: response.type});
                },
                error: function (response) {
                    $.pnotify({title: response.title, text: response.text, type: response.type});
                },
                complete: function () {
                    var arr = $('#lista-alocar-matriculas').val().split('#');
                    var collectionDadosAlocar = new VwAlocacaoAlunoSalaCollection();
                    collectionDadosAlocar.url = '/alocar-alunos/retorna-dados-alocacao?id_projetopedagogico=' + arr[0] + '&id_matricula=' + arr[1];
                    collectionDadosAlocar.fetch({
                        success: function () {
                            var comboView = new CollectionView({
                                collection: collectionDadosAlocar,
                                childViewTagName: 'tr',
                                childViewConstructor: disciplinasView,
                                el: $('#tabela-retorno-pesquisa-alocar tbody')
                            });
                            comboView.render();
                        },
                        complete: function () {
                            loaded();
                        }
                    });
                }
            });
        }
    });
}

function salvarOpcoes(id, id_alocacao) {
    var nu_diasaluno = $('#nu_diasaluno').val();

    loading();

    $.ajax({
        url: 'alocar-alunos/salvar-opcoes-sala',
        type: 'post',
        dataType: 'json',
        data: {id_alocacao: id_alocacao, nu_diasaluno: nu_diasaluno},
        success: function (response) {
            $('#modalBasica').modal('hide');
            $.pnotify({title: response.title, text: response.text, type: response.type});
        },
        error: function (response) {
            $.pnotify({title: response.title, text: response.text, type: response.type});
        },
        complete: function () {
            var arr = $('#lista-alocar-matriculas').val().split('#');
            var collectionDadosAlocar = new VwAlocacaoAlunoSalaCollection();
            collectionDadosAlocar.url = '/alocar-alunos/retorna-dados-alocacao?id_projetopedagogico=' + arr[0] + '&id_matricula=' + arr[1];
            collectionDadosAlocar.fetch({
                success: function () {
                    var comboView = new CollectionView({
                        collection: collectionDadosAlocar,
                        childViewTagName: 'tr',
                        childViewConstructor: disciplinasView,
                        el: $('#tabela-retorno-pesquisa-alocar tbody')
                    });
                    comboView.render();
                },
                complete: function () {
                    loaded();
                }
            });
        }
    });

}


function cancelaOpcoes() {
    $('#modalBasica').modal('hide');
}

/**
 *******************************************************************************
 *******************************************************************************
 **                Outras funcionalidades
 *******************************************************************************
 *******************************************************************************
 */

var listaMatriculasView;
var pessoa = JSON.parse(localStorage.getItem('pessoaAutocomplete'));
if (pessoa != null && pessoa.id) {
}


$.getScript('/modelo-backbone/VwPesquisarPessoa', function () {
    componenteAutoComplete.main({
        model: VwPesquisarPessoa,
        element: $("#autoComplete")
    }, function () {
        $('#container-alocar-lista').html('').hide();
        $('#container-alocar-lista').show();
        if (telaAlocacao == false) {
            telaAlocacao = new ListaMatriculasView();
        } else {
            telaAlocacao.render();
        }
    }, function () {
        $('#container-alocar-lista').html('');
        $('#container-alocar-lista, #container-alocar').hide();
    });
});
$.ajaxSetup({async: true});

/**
 * CADASTRO DE NÚCLEO
 * @author Vinícius Avelino Alcântara <vinicius.alcantara@unyleya.com.br>
 */


/*********************************************
 ************** COLLECTIONS ******************
 *********************************************/
var ColletionSituacao = Backbone.Collection.extend({
    model: Situacao,
    url: '/api/situacao/?st_tabela=tb_nucleotm',
    initialize: function () {
        this.fetch({async: false});
    }
});

var CollectionEntidadeHolding = Backbone.Collection.extend({
    model: VwEntidadesMesmaHolding,
    url: '/holding/retornar-entidades-mesma-holding',
    initialize: function () {
        this.fetch({async: false});
    }
});

/*********************************************
 ************** ITEMVIEW ***********************
 *********************************************/
var ItemNucleoAtendente = Marionette.ItemView.extend({
    template: '#template-dados',
    tagName: 'div',
    className: 'container-fluid',
    events: {
        "click #btn-salvar": "salvarDados"
    },
    ui: {
        id_situacao: '#id_situacao',
        id_nucleotm: '#id_nucleotm'
    },
    initialize: function () {
        thatLayout = this;

    },
    onShow: function () {
            this.populaSelectDados();
    },
    populaSelectDados: function () {
        var col = new ColletionSituacao();

        var view = new SelectView({
            el: this.ui.id_situacao, // Elemento da DOM
            collection: col, // Instancia da collection a ser utilizada
            childViewOptions: {
                optionLabel: 'st_situacao', // Propriedade da Model que será utilizada como label do select
                optionValue: 'id_situacao', // Propriedade da Model que será utilizada como id do option do select
                optionSelected: this.model.get('id_situacao').id_situacao
            }
        });

        var nucleo = new CollectionEntidadeHolding();

        var selectNucleo = new SelectView({
            el: this.ui.id_nucleotm, // Elemento da DOM
            collection: nucleo, // Instancia da collection a ser utilizada
            childViewOptions: {
                optionLabel: 'st_nomeentidadeparceira', // Propriedade da Model que será utilizada como label do select
                optionValue: 'id_entidadeparceira', // Propriedade da Model que será utilizada como id do option do select
                optionSelected: this.model.get('id_entidade')
            }
        });
        /*console.log(new MunicipioSelectView());
         layoutAntendente.select.show(new MunicipioSelectView());*/
        selectNucleo.render();
        view.render();
    },
    salvarDados: function () {
        that = this;
        if (this.ui.id_situacao.val() != '' && this.ui.id_nucleotm.val() != '') {
         var parametros = {
                'id_situacao': this.ui.id_situacao.val(),
                'id_entidade': this.ui.id_nucleotm.val()
            };

            loading();
            this.model.url = '/nucleo-atendentes/salvar-nucleo-atendente';
            this.model.save(parametros, {
                success: function (data) {
                    $.pnotify({
                        title: "Sucesso",
                        text: "Núcleo salvo!",
                        type: 'success'
                    });
                },
                error: function(data,response)
                {
                    data = $.parseJSON(response.responseText);
                    $.pnotify({
                        title: "Erro",
                        text: data,
                        type: 'erro'
                    });
                },
                complete: function () {
                    loaded();
                }
            });
            /*$.ajax({
                type: 'post',
                url: '/nucleo-atendentes/salvar-nucleo-atendente',
                data: parametros,
                beforeSend: function () {
                    loading();
                },
                success: function (data) {
                    $.pnotify({
                        title: data['title'],
                        text: data['mensagem'],
                        type: 'success'
                    });
                },
                complete: function () {
                    loaded();
                }
            })*/
        } else {
            $.pnotify({
                title: 'Campo Obrigatório!',
                text: 'Selecione uma situacao e/ou um Núcleo',
                type: 'error'
            });
        }
    }
});

/*********************************************
 ************** INICIALIZAÇÃO ****************
 *********************************************/

var itemNucleoAtendente = new ItemNucleoAtendente({
    model: new NucleoTm(G2S.editModel)
});
G2S.layout.content.show(itemNucleoAtendente);
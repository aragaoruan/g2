/******************************************************************************
 *              Funcionalidade Entrega de documentos
 *              Denise Xavier  <denise.xavier@unyleya.com.br>
 *              15/10/2014
 * ****************************************************************************
 */
$.ajaxSetup({async: false});

var VwEntregaDocumentoCollectionExtend = Backbone.Collection.extend({
    url: '/api/vw-entrega-documento/',
    model: VwEntregaDocumento
});

var SituacaoCollectionExtend = Backbone.Collection.extend({
    url: '/api/situacao/',
    model: Situacao
});

var VwMatriculaCollectionExtend = Backbone.Collection.extend({
    url: '/api/vw-matricula/',
    model: VwMatricula
});

var SituacoesCollection = new SituacaoCollectionExtend();
SituacoesCollection.url += 'index/st_tabela/tb_entregadocumentos';
SituacoesCollection.fetch();

var ComboSituacoesView = Marionette.ItemView.extend({
    tagName: 'option',
    template: _.template('<%=st_situacao%>'),
    onRender: function () {
        this.$el.attr('value', this.model.get('id_situacao'));
    }
});

var ComboSituacoesCollection = Marionette.CollectionView.extend({
    tagName: 'select',
    childView: ComboSituacoesView,
    labelName: ''
});

/**
 * Item View para aba de entrega de documentos
 * @type {*}
 */
var DocumentoItemViewExtend = Marionette.ItemView.extend({
    tagName: 'tr',
    template: '#template-grid-tbody',
    ui: {
        idSituacaoSelect: '#id-situacao-select'
    },
    initialize: function () {
    },

    onRender: function () {
        this.ui.idSituacaoSelect.html('<option value="">--</option>');

        var viewSelectSituacoes = new SelectView({
            el: this.ui.idSituacaoSelect,        // Elemento da DOM
            collection: SituacoesCollection,      // Instancia da collection a ser utilizada
            childViewOptions: {
                optionLabel: 'st_situacao', // Propriedade da Model que será utilizada como label do select
                optionValue: 'id_situacao', // Propriedade da Model que será utilizada como id do option do select
                optionSelected: this.model.get('id_situacao')       // ID da option que receberá o atributo "selected"
            }
        });

        viewSelectSituacoes.render();
    },
    salvaEntrega: function (id, label) {
        var that = this;

        bootbox.confirm(sprintf(MensagensG2.MSG_ENTREGA_DOCUMENTO_01, label), function (result) {
            if (result) {
                loading();
                that.model.set('id_situacao', id);
                $.ajax({
                    url: '/default/entrega-documentos/salvar',
                    data: that.model.toJSON(),
                    type: 'post',
                    dataType: 'json',
                    success: function (response) {
                        that.model.set('id_entregadocumentos', response.codigo);
                        $.pnotify({title: 'Alerta', text: MensagensG2.MSG_ENTREGA_DOCUMENTO_02, type: 'success'});
                    },
                    error: function () {
                        $.pnotify({title: 'Alerta', text: MensagensG2.MSG_ENTREGA_DOCUMENTO_03, type: 'danger'});
                        that.ui.idSituacaoSelect.val(that.model.get('id_situacao'));
                    }
                });
                loaded();
            } else {
                that.ui.idSituacaoSelect.val(that.model.get('id_situacao'));
            }
        });
    },
    events: {
        'change #id-situacao-select': function (e) {
            var that = this;
            var id = $(e.currentTarget).val();
            if (id) {
                var label = $(e.currentTarget).find(' :selected').text();
                this.salvaEntrega(id, label);
            } else {
                $.pnotify({title: 'Alerta', text: MensagensG2.MSG_ENTREGA_DOCUMENTO_04, type: 'danger'});
                that.ui.idSituacaoSelect.val(that.model.get('id_situacao'));
            }
        }

    }
});

var MatriculaDocumentoItemViewExtend = Marionette.ItemView.extend({
    tagName: 'tr',
    template: '#template-grid-tbody-revisao',
    ui: {
        idSelectRevisao: '#id-select-revisao',
        idSelectAcademico: '#id-select-academico',
        btnHistorico: '.historico',
        containerModal: '#container-modal-historico-documentacao',
        btnObservacaoEntrega: '#btn-observacao-entrega',
        txtAreaObservacaoEntrega: '#observacao-entrega'
    },
    events: {
        'click #btn-observacao-entrega': 'salvarObservacaoMatricula',
        'click .historico': 'openHistorico',
        'input #observacao-entrega': 'updateCharCount'
    },
    checkDataColacao: function () {
        //verifica se o aluno tem data de colação para bloquear ou liberar o campo de doc ok

        (new MatriculaColacao())
            .fetch({
                "url": "/colacao-grau/check-data-colacao",
                "data": {
                    "id_matricula": this.model.get("id_matricula")
                },
                "beforeSend": loading,
                "complete": loaded,
                "error": function (model, response) {
                    $.pnotify({
                        "title": "Erro!",
                        "type": "error",
                        "text": "Não foi possível verificar se o aluno possui Data de Colação. " +
                        "ERRO: " + response.responseText
                    });
                },
                success: _.bind(function (model, response) {
                    this.ui.idSelectRevisao.removeAttr("disabled")
                        .attr("title", "Alterar status da documentação.");
                    this.editStatusDocumentacao = true;
                    if (response && this.model.get("bl_documentacao") == "1") {
                        this.editStatusDocumentacao = false;
                        this.ui.idSelectRevisao.attr("disabled", "disabled")
                            .attr("title", "Não é possível alterar a documentação porque o aluno tem data de colação marcada.");
                    }
                }, this)
            });

    },
    onRender: function () {
        this.ui.btnHistorico.popover();

        var bl_documentacao,
            bl_academico;

        switch (this.model.get('bl_documentacao')) {
            case true:
            case '1':
            case 1:
                bl_documentacao = '1';
                break;
            case false:
            case '0':
            case 0:
                bl_documentacao = '0';
                break;
            default:
                bl_documentacao = '';
                break;
        }

        switch (this.model.get('bl_academico')) {
            case true:
            case '1':
            case 1:
                bl_academico = '1';
                break;
            case false:
            case '0':
            case 0:
                bl_academico = '0';
                break;
            default:
                bl_academico = '';
                break;
        }

        this.model.set({
            bl_documentacao: bl_documentacao,
            bl_academico: bl_academico
        });

        //verifica se o aluno tem data de colação
        this.checkDataColacao();
    },
    salvaDocumentacao: function () {
        var that = this,
            value = this.ui.idSelectRevisao.val() + "";

        that.model.set('bl_documentacao', value);

        $.ajax({
            url: '/default/entrega-documentos/salvar-revisao',
            data: that.model.toJSON(),
            type: 'post',
            dataType: 'json',
            async: true,
            beforeSend: loading,
            success: function (response) {
                $.pnotify({title: response.title, text: MensagensG2.MSG_ENTREGA_DOCUMENTO_02, type: 'success'});
            },
            error: function () {
                $.pnotify({title: 'Alerta', text: MensagensG2.MSG_ENTREGA_DOCUMENTO_03, type: 'danger'});
                that.ui.idSelectRevisao.val(that.model.get('bl_documentacao'));
            },
            complete: loaded
        });
    },
    salvaMarcacaoAcademico: function () {
        var that = this,
            value = this.ui.idSelectAcademico.val() + "";

        that.model.set('bl_academico', value);

        $.ajax({
            url: '/default/entrega-documentos/salvar-marcacao-academico',
            data: that.model.toJSON(),
            type: 'post',
            dataType: 'json',
            async: true,
            beforeSend: loading,
            success: function (response) {
                $.pnotify({title: response.title, text: MensagensG2.MSG_ENTREGA_DOCUMENTO_09, type: 'success'});
            },
            error: function () {
                $.pnotify({title: 'Alerta', text: MensagensG2.MSG_ENTREGA_DOCUMENTO_10, type: 'danger'});
                that.ui.idSelectAcademico.val(that.model.get('bl_academico'));
            },
            complete: loaded
        });
    },
    salvarObservacaoMatricula: function () {
        var that = this;

        var st_observacao = this.ui.txtAreaObservacaoEntrega.val().trim(),
            bl_documentacao = this.ui.idSelectRevisao.val(),
            bl_academico = this.ui.idSelectAcademico.val(),
            bl_salvar = true;

        // Só salvar se possuir alterações
        if (this.model.get('bl_documentacao') !== bl_documentacao) {
            // Se a revisão for pendente, é obrigatório preencher a observação
            if (bl_documentacao === '0' && !st_observacao) {
                bl_salvar = false;
            }
        }

        // Só salvar se possuir alterações
        if (this.model.get('bl_academico') !== bl_academico) {
            // Se a revisão for pendente, é obrigatório preencher a observação
            if (bl_academico === '0' && !st_observacao) {
                bl_salvar = false;
            }
        }

        if (!bl_salvar) {
            $.pnotify({
                type: 'warning',
                title: 'Aviso',
                text: 'O campo "Observação" deve ser preenchido quando a revisão for alterada para "Pendente".'
            });
            return false;
        }
        var render = false;
        // Só salvar se possuir alterações
        if (this.model.get('bl_documentacao') !== bl_documentacao) {

            if (!this.editStatusDocumentacao) {
                $.pnotify({
                    "title": "Atenção!",
                    "type": "warning",
                    "text": "Não é permitido alterar o status da documentação de OK para outro."
                });
                this.render();
                return false;
            }


            this.salvaDocumentacao();
            render = true;
        }

        // Só salvar se possuir alterações
        if (this.model.get('bl_academico') !== bl_academico) {
            this.salvaMarcacaoAcademico();
            render = true;
        }

        // Só salvar se possui o campo preenchido
        if (st_observacao) {
            $.ajax({
                url: '/default/entrega-documentos/salvar-observacao',
                data: {
                    id_matricula: this.model.get('id_matricula'),
                    st_observacaoentregadocumentos: st_observacao
                },
                type: 'post',
                dataType: 'json',
                beforeSend: loading,
                async: true,
                success: function (response) {
                    $.pnotify(response);
                    that.ui.txtAreaObservacaoEntrega.val('').trigger('input');
                },
                error: function (response) {
                    $.pnotify(response);
                },
                complete: loaded
            });
            render = true;
        }

        //decide se vai renderizar a grid novamente
        if (render)
            this.render();
    },
    openHistorico: function () {
        var that = this;
        that.ui.containerModal.empty();
        var customModal = $(
            '<div class="modal hide fade" id="modal-historico-documentacao" tabindex="-1" role="dialog" aria-labelledby="Histórico de Alteração" aria-hidden="false">' +
            '<div class="modal-dialog">' +
            '<div class="modal-content">' +
            '<div class="modal-header">' +
            '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
            '<h4 class="modal-title" id="myModalLabel">Histórico de alterações:</h4>' +
            '</div>' +
            '<div class="modal-body">' +
            '<div id="container-grid-historico-documentacao" class="control-group">' +
            '</div>' +
            '<div class="modal-footer">' +
            '<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>'
        );

        that.ui.containerModal.append(customModal);

        $('.modal .hide').show();
        $("#modal-historico-documentacao").modal('show');

        var modelTramites = Backbone.Model.extend({});
        var DataGridTramites = Backbone.PageableCollection.extend({
            model: modelTramites,
            url: "/",
            state: {pageSize: 15},
            mode: "client"
        });

        var urlTramites = '/entrega-documentos/retorna-tramites';
        var dataResponseTramites;
        $.ajax({
            dataType: 'json',
            type: 'get',
            url: urlTramites,
            data: {id_matricula: this.model.get('id_matricula')},
            beforeSend: function () {
            },
            success: function (data) {
                dataResponseTramites = data;
            },
            complete: function () {
                var dataGridTramite = new DataGridTramites(dataResponseTramites);
                var columnsTramite = [{
                    name: "dt_cadastro", label: "Data", editable: false, cell: "string"
                },
                    {
                        name: "st_nomecompleto", label: "Usuário cadastro", editable: false, cell: "string"
                    },
                    {
                        name: "st_tramite", label: "Alteração", editable: false, cell: "string"

                    }
                ];
                $("#container-grid-historico-documentacao").html('');
                if (dataGridTramite.length > 0) {
                    var pageableGridTramite = new Backgrid.Grid({
                        columns: columnsTramite,
                        collection: dataGridTramite,
                        className: 'backgrid backgrid-selectall table table-bordered table-hover'
                    });
                    $("#container-grid-historico-documentacao").append(pageableGridTramite.render().$el);
                    var paginatorTramites = new Backgrid.Extension.Paginator({
                        collection: dataGridTramite
                    });

                    $("#container-grid-historico-documentacao").append(paginatorTramites.render().$el);

                } else {
                    $("#container-grid-historico-documentacao").html('Nenhum trâmite cadastrado para a documentação dessa matrícula!');
                }
                loaded();
            }
        });
    },
    // Atualiza o contador de caracteres no campo de observação.
    updateCharCount: function (e) {
        $('#charCounter').html($(e.target).val().length);
    }
});


/**
 * EmptyView para Composite de produtos
 * @type {ItemView}
 */
var EmptyView = Marionette.ItemView.extend({
    template: '#template-lista-vazia',
    tagName: 'tr'
});

var DocumentosCompositeViewExtend = Marionette.CompositeView.extend({
    childView: DocumentoItemViewExtend,
    template: '#template-table-documento',
    childViewContainer: '#body-table-grid',
    emptyView: EmptyView,
    onRender: function () {
        $('.tooltipClass').tooltip();
    }
});

var MatriculasEntregaCompositeViewExtend = Marionette.CompositeView.extend({
    childView: MatriculaDocumentoItemViewExtend,
    template: '#template-table-revisao',
    childViewContainer: '#body-table-grid-revisao',
    emptyView: EmptyView,
    onRender: function () {
        $('.tooltipClass').tooltip();
    }
});

var EntregaDocumentosCompositeView = Marionette.CompositeView.extend({
    template: '#template-entrega-documentos',
    className: 'row-fluid',
    ui: {
        containerTable: '#container-table',
        itemAutoComplete: '.item-autocomplete',
        itemTab2: '#link-tab2',
        itemAutoCompleteRevisao: '#autoComplete-revisao',
        containerTableRevisao: '#container-table-revisao'
    },
    onShow: function () {
        that = this;

        componenteAutoComplete.main({
                model: VwPesquisarPessoa,
                element: this.$el.find('#autoComplete')
            },
            _.bind(function () {
                var pessoa = componenteAutoComplete.getFixModel();
                this.loadGridDocumentos(pessoa);
                this.loadGridMatricula(pessoa);
            }, this),
            _.bind(function () {
                this.ui.containerTable.empty();
                this.ui.containerTableRevisao.empty();
            }, this)
        );

    },
    loadGridDocumentos: function (pessoa) {
        this.ui.containerTable.show();
        var vwEntregaDocumentosCollection = new VwEntregaDocumentoCollectionExtend();
        vwEntregaDocumentosCollection.url += 'id_usuario/' + pessoa.id_usuario + '/id_entidade/' + G2S.loggedUser.get('id_entidade') + '/bl_entidadepai/1';
        vwEntregaDocumentosCollection.fetch({
            beforeSend: loading,
            complete: loaded,
            success: _.bind(function () {
                (new DocumentosCompositeViewExtend({
                    collection: vwEntregaDocumentosCollection,
                    el: this.ui.containerTable
                })).render();
            }, this)
        });
    },
    loadGridMatricula: function (pessoa) {
        this.ui.containerTableRevisao.show();
        var vwMatriculaDocumentosCollection = new VwMatriculaCollectionExtend();
        vwMatriculaDocumentosCollection.url = '/api/vw-matricula/id_usuario/' + pessoa.id_usuario + '/id_entidade/' + G2S.loggedUser.get('id_entidade');
        vwMatriculaDocumentosCollection.fetch({
            beforeSend: loading,
            complete: loaded,
            success: _.bind(function () {
                (new MatriculasEntregaCompositeViewExtend({
                    collection: vwMatriculaDocumentosCollection,
                    el: this.ui.containerTableRevisao
                })).render();
            }, this)
        });

    },
    events: {
        'click .item-autocomplete': function () {
            // var pessoa = JSON.parse(localStorage.getItem('pessoaAutocomplete'));
            // this.loadGridDocumentos(pessoa);
            // this.loadGridMatricula(pessoa);
        }
    }
});

$(function () {
    G2S.layout.content.show(new EntregaDocumentosCompositeView());
});
/**
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @date 2016-03-04
 * @description Codigo em Marionette
 */

/***********************************************************************
 * *********************************************************************
 * SECAO AVALIACOES ****************************************************
 * *********************************************************************
 ***********************************************************************/
var AvaliacoesView = Marionette.ItemView.extend({
    template: '#avaliacoes-view',
    tagName: 'tr',
    ui: {
        checkbox: 'input'
    },
    initialize: function () {
        this.model.set('checked', this.model.get('checked') || false);
    },
    modelEvents: {
        change: 'render'
    },
    events: {
        change: 'setValue'
    },
    setValue: function () {
        this.model.set('checked', this.ui.checkbox.is(':checked'));
    }
});

var AvaliacoesCollection = Backbone.Collection.extend({
    model: AvaliacaoAplicacao,
    url: '/aplicacao-avaliacao/retorna-avaliacoes/'
});

var collectionAvaliacoes = new AvaliacoesCollection();
var collectionAvaliacoesSelecionadas = new Backbone.Collection();

var AvaliacoesComposite = Marionette.CompositeView.extend({
    template: '#avaliacoes-template',
    collection: collectionAvaliacoes,
    childView: AvaliacoesView,
    childViewContainer: 'tbody',
    emptyView: Marionette.ItemView.extend({
        tagName: 'tr',
        template: _.template('<td colspan="4">Não foram encontradas Avaliações para esta Entidade.</td>')
    }),
    initialize: function () {
        this.collection.fetch({
            success: function () {
                // Marcar os selecionados caso a o Aplicador ja tenha sido salvo antes
                var id_avaliacaoaplicacao = layoutPrincipal.model.get('id_avaliacaoaplicacao');

                if (id_avaliacaoaplicacao) {
                    collectionAvaliacoesSelecionadas.fetch({
                        url: '/aplicacao-avaliacao/retorna-avaliacoes-update',
                        data: {
                            id: id_avaliacaoaplicacao
                        },
                        success: function () {
                            collectionAvaliacoesSelecionadas.each(function (model) {
                                var model_check = collectionAvaliacoes.findWhere({id_avaliacao: model.get('id_avaliacao')});
                                if (model_check) {
                                    model_check.set('checked', true);
                                }
                            });
                        }
                    })
                }

            }
        });
    }
});


/***********************************************************************
 * *********************************************************************
 * LAYOUT PRINCIPAL ****************************************************
 * *********************************************************************
 ***********************************************************************/

var LayoutPrincipal = Marionette.LayoutView.extend({
    model: G2S.editModel,
    template: '#template-layout-principal',
    regions: {
        grid_avaliacao: '#container-avaliacao'
    },
    ui: {
        form: 'form',

        input_uf: '#sg_uf',
        input_municipio: '#id_municipio',
        input_aplicador: '#id_aplicadorprova',
        input_endereco: '#id_endereco',
        input_horario: '#id_horarioaula',

        input_dt_aplicacao: '#dt_aplicacao',
        input_nu_maxaplicacao: '#nu_maxaplicacao',
        input_dt_antecedenciaminima: '#dt_antecedenciaminima',
        input_dt_alteracaolimite: '#dt_alteracaolimite'
    },
    events: {
        'submit @ui.form': 'actionSave',
        'change @ui.input_uf': 'carregarMunicipios',
        'change @ui.input_municipio': 'carregarAplicadores',
        'change @ui.input_aplicador': 'carregarEnderecos',
        'changeDate @ui.input_dt_aplicacao': 'carregarHorarios'
    },
    onRender: function () {
        this.carregarUfs();
        this.carregarHorarios();

        var grid_avaliacao = new AvaliacoesComposite();
        this.grid_avaliacao.show(grid_avaliacao);
    },
    carregarUfs: function () {
        var that = this;

        var sg_uf = this.model.get('sg_uf');

        ComboboxView({
            el: this.ui.input_uf,
            url: '/util/retorna-uf',
            optionLabel: 'st_uf',
            optionSelectedId: sg_uf || '',
            emptyOption: '--',
            model: Backbone.Model.extend({
                idAttribute: 'sg_uf'
            })
        }, function () {
            that.carregarMunicipios();
        });
    },
    carregarMunicipios: function () {
        var that = this;

        var sg_uf = this.ui.input_uf.val();

        if (sg_uf) {
            ComboboxView({
                el: this.ui.input_municipio,
                url: '/api/municipio?sg_uf=' + sg_uf,
                optionLabel: 'st_nomemunicipio',
                optionSelectedId: this.model.get('id_municipio') || '',
                emptyOption: '--',
                model: Backbone.Model.extend({
                    idAttribute: 'id_municipio'
                })
            }, function () {
                that.carregarAplicadores();
            });
        } else {
            this.ui.input_municipio.find('option').not(':first').remove();
            this.carregarAplicadores();
        }
    },
    carregarAplicadores: function () {
        var that = this;

        var sg_uf = this.ui.input_uf.val();
        var id_municipio = this.ui.input_municipio.val();

        if (id_municipio) {
            ComboboxView({
                el: this.ui.input_aplicador,
                url: '/aplicacao-avaliacao/filtra-aplicador?sg_uf=' + sg_uf + '&id_municipio=' + id_municipio,
                optionLabel: 'st_text',
                optionSelectedId: this.model.get('id_aplicadorprova') || '',
                emptyOption: '--',
                model: Backbone.Model.extend({
                    idAttribute: 'id_value'
                })
            }, function () {
                that.carregarEnderecos();
            });
        } else {
            this.ui.input_aplicador.find('option').not(':first').remove();
            this.carregarEnderecos();
        }
    },
    carregarEnderecos: function () {
        var id_aplicadorprova = this.ui.input_aplicador.val();

        if (id_aplicadorprova) {
            ComboboxView({
                el: this.ui.input_endereco,
                url: '/aplicacao-avaliacao/busca-endereco-aplicador?idParam=' + id_aplicadorprova,
                optionLabel: 'st_text',
                optionSelectedId: this.model.get('id_endereco') || '',
                emptyOption: '--',
                model: Backbone.Model.extend({
                    idAttribute: 'id_value'
                })
            });
        } else {
            this.ui.input_endereco.find('option').not(':first').remove();
        }
    },
    carregarHorarios: function () {
        var dt = this.ui.input_dt_aplicacao.val();

        if (dt) {
            ComboboxView({
                el: this.ui.input_horario,
                url: '/horario-de-aula/horarios-por-data/?dt=' + dt,
                optionLabel: 'st_horarioaula',
                optionSelectedId: this.model.get('id_horarioaula'),
                emptyOption: '--',
                model: Backbone.Model.extend({
                    idAttribute: 'id_horarioaula'
                })
            });
        }
    },
    actionSave: function (e) {
        var that = this;

        e.preventDefault ? e.preventDefault() : '';

        var values = Backbone.Syphon.serialize(this);

        // Buscar corretamente as avaliacoes
        values.id_avaliacao = [];
        collectionAvaliacoes.each(function (model) {
            if (model.get('checked')) {
                values.id_avaliacao.push(model.get('id_avaliacao'));
            }
        });

        if (!this.validate(values)) {
            return false;
        }

        that.model.set(values);

        $.ajax({
            type: 'post',
            url: '/api/aplicacao-avaliacao',
            data: JSON.stringify(this.model.toJSON()),
            dataType: 'json',
            contentType: 'application/json',
            beforeSend: loading,
            success: function (response) {
                values.id_avaliacaoaplicacao = response.id;
                that.model.set(values);
            },
            complete: function (response) {
                loaded();
                $.pnotify(
                    response.responseJSON || {
                        type: 'error',
                        title: 'Erro',
                        text: 'Houve um erro ao tentar salvar. Tente novamente.'
                    }
                );
            }
        });
    },
    validate: function (values) {
        var error = null;

        if (!values.id_avaliacao || !values.id_avaliacao.length) {
            error = 'Selecione pelo menos uma avaliação.';
        }

        if (error) {
            $.pnotify({
                type: 'warning',
                title: 'Obrigatório',
                text: error
            });
            return false;
        }

        return true;
    }
});

var layoutPrincipal = new LayoutPrincipal();
G2S.layout.content.show(layoutPrincipal);


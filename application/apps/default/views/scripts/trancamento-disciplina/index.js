/** VIEWS */
var layoutInit,
    formEnrollmentInit,
    subjectInit,
    modalWarningInit,
    modalConfirmInit;


var FormEnrollmentView = Marionette.ItemView.extend({
    template: '#enrollment-template',
    ui: {
        inputMatricula: '#id_matricula'
    },
    events: {
        'change @ui.inputMatricula': 'loadSubject'
    },
    onRender: function () {
        this.loadEnrollment();
    },
    loadEnrollment: function () {
        var that = this;
        var id_usuario = layoutInit.model.get('id_usuario');

        if (!id_usuario) {
            this.ui.inputMatricula.html('<option value="">[Selecione um usuário]</option>');
            return false;
        }

        this.ui.inputMatricula.html('<option value="">Carregando...</option>');

        ComboboxView({
            el: that.ui.inputMatricula,
            url: '/api/vw-matricula/id_evolucao/' + EVOLUCAO.TB_MATRICULA.CURSANDO + '/id_usuario/' + layoutInit.model.get('id_usuario'),
            optionLabelConstruct: function (model) {
                return model.get('id_matricula') + ' - ' + model.get('st_projetopedagogico') + ' - ' + model.get('st_evolucao');
            },
            model: Backbone.Model.extend({
                idAttribute: 'id_matricula',
                defaults: {
                    id_matricula: null,
                    st_matricula: null
                }
            })
        });
    },
    loadSubject: function () {
        subjectInit = new SubjectComposite();
        layoutInit.subjects.show(subjectInit);
    }
});


var VwGradeNotaCollection = Backbone.Collection.extend({
    model: VwGradeNota,
    url: '/trancamento-disciplina/retorna-disciplinas-trancamento/'
});

var rowSubjectItem = Marionette.ItemView.extend({
    template: '#subject-row-view',
    tagName: 'tr',
    model: new VwGradeNota(),
    events: {
        'change input': 'setModel'
    },
    setModel: function (e) {
        var $input = $(e.target);
        this.model.set('selected', $input.prop('checked'));
    }
});


var ModalConfirmComposite = Marionette.CompositeView.extend({
    template: '#modal-confirm-template',
    tagName: 'div',
    className: 'modal fade',
    childViewContainer: 'tbody',
    childView: Marionette.ItemView.extend({
        template: '#modal-confirm-view',
        tagName: 'tr'
    }),
    events: {
        'click #btn-salvar-trancamento': 'salvarTrancamento'
    },
    onShow: function () {
        this.$el.modal('show');
    },
    model: new Backbone.Model ,
    salvarTrancamento: function () {
        var data = Backbone.Syphon.serialize(layoutInit);
        data.id_alocacao = [];
        data.id_disciplina = [];

        subjectInit.collection.each(function (model) {
            if (model.get('selected')) {
                data.id_alocacao.push(model.get('id_alocacao'));
                data.id_disciplina.push(model.get('id_disciplina'));
            }
        });

        $.ajax({
            type: 'post',
            url: '/trancamento-disciplina/trancar/',
            data: data,
            beforeSend: loading,
            success: function (data) {
                $.pnotify({
                    type: data.type,
                    title: data.title,
                    text: data.text
                })
            },
            error: function (data) {
                $.pnotify({
                    type: data.type,
                    title: data.title,
                    text: data.text
                })
            },
            complete: function () {
                loaded();
                layoutInit.subjects.reset();
                layoutInit.showForm();
            }
        });
    }
});

var SubjectComposite = Marionette.CompositeView.extend({
    template: '#subject-grid-template',
    collection: new VwGradeNotaCollection(),
    childViewContainer: 'tbody',
    childView: rowSubjectItem,
    emptyView: Marionette.ItemView.extend({
        template: _.template('<td class="warning" colspan="8"> <label class="alert alert-block">Nenhuma disciplina disponível para trancamento.</label></td>'),
        tagName: 'tr'
    }),
    ui : {
        btnTrancar: '#btn-trancar',
        btnCancelar: '#btn-cancelar'
    },
    events: {
        'change .id_disciplina' : 'updateCount',
        'click @ui.btnCancelar' : function () {
            layoutInit.subjects.reset();
            layoutInit.showForm();
        },
        'click @ui.btnTrancar' : 'openModalConfirm'
    },
    initialize: function () {
        this.collection.fetch({
            data: {
                id_evolucao: EVOLUCAO.TB_MATRICULA_DISCIPLINA.CURSANDO,
                id_matricula: formEnrollmentInit.ui.inputMatricula.val()
            },
            beforeSend: loading,
            complete: loaded
        });
    },
    updateCount: function () {
        var disciplinas_selecionadas = this.collection.filter(function (item) {
            return item.get('selected');
        });

        /**
         * Quando o usuário selecionar a terceira disciplina a ser trancada, o sistema deve exibir uma mensagem:
         * "Não é recomendado trancar mais de 2 disciplinas" (O sistema exibirá a mensagem mas não impedirá que seja selecionado mais de duas disciplinas)
         */
        if(disciplinas_selecionadas.length == 3){
            modalWarningInit = new ModalWarningItem();
            layoutInit.modalwarning.show(modalWarningInit);
        }
    },
    openModalConfirm: function () {
            var disciplinas_selecionadas = this.collection.filter(function (item) {
                return item.get('selected');
            });

            if(disciplinas_selecionadas.length){
                if(this.verificaProcessoRenovacao()) {
                    modalConfirmInit = new ModalConfirmComposite({
                        collection: new Backbone.Collection(disciplinas_selecionadas)
                    });
                    layoutInit.modalconfirm.show(modalConfirmInit);
                }
            }else{
                $.pnotify({
                    type: 'warning',
                    title: 'Aviso',
                    text: 'Selecione pelo menos uma disciplina para prosseguir com o trancamento da disciplina'
                })
            }
    },
    verificaProcessoRenovacao: function () {
        var retorno = true;
        //verifica se o aluno está em processo de renovação automática, se ele estiver não pode add
        $.ajax({
            type: 'post',
            url: '/trancamento-disciplina/verifica-renovacao/',
            async: false,
            data: {
                    id_matricula: formEnrollmentInit.ui.inputMatricula.val()
            },
            beforeSend: loading,
            success: function (data) {
                if(data.type != 'success'){
                    $.pnotify({
                        type: data.type,
                        title: data.title,
                        text: data.text
                    });
                    retorno = false;
                }
            },
            error: function (data) {
                retorno = false;
                $.pnotify({
                    type: data.type,
                    title: data.title,
                    text: data.text
                })
            },
            complete: function () {
                loaded();
            }
        });
        return retorno
    }
});

var MainLayoutView = Marionette.LayoutView.extend({
    el: '#main-region',
    regions: {
        autocomplete: '#autocomplete-person-region',
        enrollment: '#enrollment-region',
        subjects: '#subject-region',
        modalwarning: '#modal-region',
        modalconfirm: '#modal-confirm-region'
    },
    initialize: function () {
        layoutInit = this;
        var that = this;

        componenteAutoComplete.main({
            model: VwPesquisarPessoa,
            element: that.autocomplete.$el
        }, function () {
            that.model = new Backbone.Model(componenteAutoComplete.getFixPessoa());
            that.showForm();
        }, function () {
            that.hideForm();
        });
    },
    showForm: function () {
        formEnrollmentInit = new FormEnrollmentView({
            model: this.model
        });
        this.enrollment.show(formEnrollmentInit);
    },
    hideForm: function () {
        this.enrollment.empty();
        this.subjects.empty();
    }
});


var ModalWarningItem = Marionette.ItemView.extend({
    template: '#modal-warning-template',
    tagName: 'div',
    className: 'modal fade',
    onShow: function () {
        this.$el.modal('show');
    },
    model: new Backbone.Model 
});


layoutInit = new MainLayoutView();
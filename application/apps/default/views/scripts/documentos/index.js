/**
 * Regioes usadas na funcionalidade
 */
G2S.layout.addRegions({
    filtros: '#filtros',
    resultado: '#resultado',
    projetosPedagogicos: '#projetos-pedagogicos'
});


/**
 * Model Resultado da Busca
 */
var DocumentosModel = Backbone.Model.extend({
    idAttribute: 'id_documentos',
    url: function () {
        return 'api/documentos/' + (this.id || '');
    },
    defaults: {
        id_documentos: null,
        bl_digitaliza: null,
        st_documentos: '',
        id_tipodocumento: {
            id_tipodocumento: null
        },
        bl_obrigatorio: null,
        nu_quantidade: null,
        id_situacao: {
            id_situacao: null
        },
        id_entidade: {
            id_entidade: null
        },
        bl_entidadepai : null,
        id_usuariocadastro: null,
        bl_ativo: null,
        id_documentosobrigatoriedade: {
            id_documentosobrigatoriedade: null
        },
        id_documentoscategoria: {
            id_documentoscategoria: null
        },
        projetos_pedagogicos: [],
        documentos_utilizacao: [],
        nu_ordenacao: null,
        isEditing: false
    }
});

/**
 * Collection Resultado da Busca
 */
var DocumentosCollection = Backbone.Collection.extend({
    model: DocumentosModel,
    url: function () {
        var url = '/api/documentos?';

        var values = {
            st_documentos: $('#st_documentos').val(),
            id_tipodocumento: $('#id_tipodocumento').val(),
            id_documentosobrigatoriedade: $('#id_documentosobrigatoriedade').val(),
            id_situacao: $('#id_situacao').val(),
            bl_digitaliza: $('#bl_digitaliza').val(),
            nu_quantidade: $('#nu_quantidade').val(),
            id_documentoscategoria: $('#id_documentoscategoria').val(),
            bl_entidadepai: $('#bl_entidadepai').val()
        };

        var value = '';
        for (var index in values) {
            value = values[index].toString();
            if (value.trim() !== '')
                url += index + '=' + value + '&';
        }

        url = url.substring(0, (url.length - 1));

        return url;
    }
});

/**
 * ItemView Resultado da Busca
 */
var DocumentosView = Marionette.ItemView.extend({
    template: '#documentos-item',
    tagName: 'tr',
    events: {
        'click .edit': 'edit',
        'click .delete': 'delete'
    },
    edit: function () {
        var values = {
            id_documentos: this.model.id,
            st_documentos: this.model.get('st_documentos'),
            bl_digitaliza: this.model.get('bl_digitaliza'),
            nu_quantidade: this.model.get('nu_quantidade'),
            id_tipodocumento: this.model.get('id_tipodocumento'),
            id_situacao: this.model.get('id_situacao'),
            id_entidade: this.model.get('id_entidade'),
            id_documentosobrigatoriedade: this.model.get('id_documentosobrigatoriedade'),
            id_documentoscategoria: this.model.get('id_documentoscategoria'),
            projetos_pedagogicos: this.model.get('projetos_pedagogicos'),
            documentos_utilizacao: this.model.get('documentos_utilizacao'),
            nu_ordenacao: this.model.get('nu_ordenacao')
        };

        PesquisaIni.model.set(values);
        PesquisaIni.setEditing();
    },
    delete: function () {
        var that = this;

        bootbox.confirm("Tem certeza de que deseja remover o registro?", function (result) {
            if (result) {
                that.model.destroy({
                    success: function () {
                        $.pnotify({
                            title: 'Concluído',
                            text: 'O registro foi removido.',
                            type: 'success'
                        });

                        that.remove();
                        PesquisaIni.render();
                    },
                    error: function () {
                        $.pnotify({
                            title: 'Erro',
                            text: 'Houve um erro ao remover o registro, tente novamente.',
                            type: 'error'
                        });
                    }
                });
            }
        });
    }
});

/**
 * CompositeView Resultado da Busca
 */
var DocumentosCompositView = Marionette.CompositeView.extend({
    template: '#documentos-list',
    collection: new DocumentosCollection(),
    childView: DocumentosView,
    childViewContainer: 'tbody',
    initialize: function () {
        this.collection.fetch({
            success: function () {
                loaded();
            }
        });
    }
});


// ComboBoxes
// Funcoes que trazem os conteudos
// dos campos <select>

function getTipoDocumento(selectedId, fn) {
    var Model = Backbone.Model.extend({
        idAttribute: 'id_tipodocumento',
        defaults: {
            id_tipodocumento: null,
            st_tipodocumento: '',
            bl_ativo: false
        }
    });

    ComboboxView({
        el: '#id_tipodocumento',
        model: Model,
        url: '/documentos/get-tipo-documento/',
        optionLabel: 'st_tipodocumento',
        optionSelectedId: selectedId
    }, fn);
}


function getDocumentosObrigatoriedade(selectedId) {
    var Model = Backbone.Model.extend({
        idAttribute: 'id_documentosobrigatoriedade',
        defaults: {
            id_documentosobrigatoriedade: null,
            st_documentosobrigatoriedade: '',
            bl_ativo: false
        }
    });

    ComboboxView({
        el: '#id_documentosobrigatoriedade',
        model: Model,
        url: '/documentos/get-obrigatoriedade/',
        optionLabel: 'st_documentosobrigatoriedade',
        optionSelectedId: selectedId
    });
}


function getDocumentosCategoria(selectedId) {
    var Model = Backbone.Model.extend({
        idAttribute: 'id_documentoscategoria',
        defaults: {
            id_documentoscategoria: null,
            st_documentoscategoria: '',
            bl_ativo: false
        }
    });

    ComboboxView({
        el: '#id_documentoscategoria',
        model: Model,
        url: '/documentos/get-categoria/',
        optionLabel: 'st_documentoscategoria',
        optionSelectedId: selectedId
    });
}


function getSituacao(selectedId) {
    var Model = Backbone.Model.extend({
        idAttribute: 'id_situacao',
        defaults: {
            id_documentoscategoria: null,
            st_documentoscategoria: '',
            bl_ativo: false
        }
    });

    ComboboxView({
        el: '#id_situacao',
        model: Model,
        url: '/documentos/get-situacao/',
        optionLabel: 'st_situacao',
        optionSelectedId: selectedId
    });
}

function getDocumentosUtilizacao(selectedIds) {
    var Model = Backbone.Model.extend({
        idAttribute: 'id_documentosutilizacao',
        defaults: {
            id_documentosutilizacao: null,
            st_documentosutilizacao: '',
            bl_ativo: false
        }
    });


    var Collection = Backbone.Collection.extend({
        model: Model,
        url: '/documentos/get-utilizacao/'
    });

    var ItemView = Marionette.ItemView.extend({
        tagName: "label",
        className: 'checkbox inline',
        initialize: function () {
            _.bind(this, 'render');
        },
        render: function () {
            var elem = $(this.el);

            elem.append('<input type="checkbox" name="documentos_utilizacao[]" />');

            elem.find('input[type="checkbox"]')
                .attr('value', this.model.id)
                .after(this.model.get('st_documentosutilizacao'));

            for (var index in selectedIds) {
                if (Number(selectedIds[index]) == Number(this.model.id)) {
                    elem.find('input[type="checkbox"]').prop('checked', 'checked');
                    break;
                }
            }

            return this;
        }
    });

    var Initialize = new Collection();
    Initialize.fetch({
        success: function () {
            var renderer = new CollectionView({
                collection: Initialize,
                childViewConstructor: ItemView,
                childViewTagName: 'label',
                el: $('#documentos_utilizacao')
            });
            renderer.render().$el;
        }
    });
}


/**
 * ItemView Caixa de Pesquisa
 */
var PesquisaView = Marionette.ItemView.extend({
    model: new DocumentosModel(),
    template: '#pesquisa-template',
    tagName: 'div',
    initialize: function () {
        loaded();
    },
    onRender: function () {
        getTipoDocumento(this.model.get('id_tipodocumento').id_tipodocumento, this.projetosPedagogicos);
        getDocumentosObrigatoriedade(this.model.get('id_documentosobrigatoriedade').id_documentosobrigatoriedade);
        getDocumentosCategoria(this.model.get('id_documentoscategoria').id_documentoscategoria);
        getSituacao(this.model.get('id_situacao').id_situacao);
        getDocumentosUtilizacao(this.model.get('documentos_utilizacao'));
    },
    events: {
        'click .search': 'search',
        'change input,select': 'unsearch',
        'click .cancel': 'cancel',
        'click .add': 'add',
        'click .save': 'save',
        'change #id_tipodocumento': 'projetosPedagogicos'
    },
    setEditing: function() {
        var elem = this.$el;

        this.model.set('isEditing', true);

        this.unsearch();
        this.render();

        elem.find('#st_documentos').focus();
    },
    search: function (e) {
        e.preventDefault();

        var DocumentosIni = new DocumentosCompositView();
        G2S.layout.resultado.show(DocumentosIni);
    },
    unsearch: function () {
        G2S.layout.resultado.reset();
    },
    cancel: function () {
        // Restaurar o padrao do model (limpar dados)
        this.model.set(this.model.defaults);
        this.render();
    },
    add: function () {
        this.model.set(this.model.defaults);
        this.setEditing();
    },
    save: function (e) {
        e.preventDefault();

        var that = this;
        var elem = this.$el;

        // Prepara o array dos DocumentosProjetosPedagogicos
        var projetos_pedagogicos = [];
        $('[name="projetos_pedagogicos"] option').each(function () {
            projetos_pedagogicos.push($(this).val());
        });

        // Prepara o array dos DocumentosUtilizacaoRelacao
        var documentos_utilizacao = [];
        elem.find('[name="documentos_utilizacao[]"]:checked').each(function () {
            documentos_utilizacao.push($(this).val());
        });

        var values = {
            st_documentos: elem.find('[name="st_documentos"]').val(),
            bl_digitaliza: elem.find('[name="bl_digitaliza"]').val(),
            nu_quantidade: elem.find('[name="nu_quantidade"]').val(),
            id_tipodocumento: {
                id_tipodocumento: elem.find('[name="id_tipodocumento"]').val()
            },
            id_situacao: {
                id_situacao: elem.find('[name="id_situacao"]').val()
            },
            id_entidade: that.model.get('id_entidade'),
            id_documentosobrigatoriedade: {
                id_documentosobrigatoriedade: elem.find('[name="id_documentosobrigatoriedade"]').val()
            },
            id_documentoscategoria: {
                id_documentoscategoria: elem.find('[name="id_documentoscategoria"]').val()
            },
            projetos_pedagogicos: projetos_pedagogicos,
            documentos_utilizacao: documentos_utilizacao,
            nu_ordenacao: elem.find('[name="nu_ordenacao"]').val()
        };


        // Validacao dos campos
        var errorMsg = null;

        if (values.st_documentos.trim() === '')
            errorMsg = 'Digite o título do documento.';
        else if (values.id_tipodocumento.id_tipodocumento.trim() === '')
            errorMsg = 'Selecione o tipo de documento.';
        else if (values.id_documentosobrigatoriedade.id_documentosobrigatoriedade.trim() === '')
            errorMsg = 'Selecione a obrigatoriedade do documento.';
        else if (values.nu_quantidade.trim() === '')
            errorMsg = 'Digite a quantidade.';
        else if (values.id_situacao.id_situacao.trim() === '')
            errorMsg = 'Informe a situação do documento.';
        else if (values.bl_digitaliza.trim() === '')
            errorMsg = 'Informe a digitalização do documento.';
        else if (values.id_documentoscategoria.id_documentoscategoria.trim() === '')
            errorMsg = 'Selecione a categoria do documento.';
        else if (values.nu_ordenacao.trim() === '')
            errorMsg = 'Digite a ordenação.';

        if (errorMsg) {
            $.pnotify({
                title: 'Erro',
                text: errorMsg,
                type: 'error'
            });

            return false;
        }

        loading();
        this.model.set(values);

        this.model.save(null, {
            success: function () {
                $.pnotify({
                    title: 'Concluído',
                    text: 'O registro foi atualizado com sucesso!',
                    type: 'success'
                });

                that.cancel();
            },
            error: function () {
                $.pnotify({
                    title: 'Erro',
                    text: 'Houve um erro ao tentar atualizar o registro, tente novamente.',
                    type: 'error'
                });
            },
            complete: function () {
                loaded();
            }
        });
    },
    projetosPedagogicos: function () {
        var elem = $('#id_tipodocumento');

        if (!PesquisaIni.model.get('isEditing') || Number(elem.val()) != 1) {
            G2S.layout.projetosPedagogicos.reset();
            return;
        }

        var ProjetoItemView = Marionette.ItemView.extend({
            template: '#projetos-pedagogicos-template',
            events: {
                'click option': 'toggleProjeto',
                'keyup #projetos-pesquisa-list': 'pesquisaList'
            },
            toggleProjeto: function (e) {
                var elem = $(e.target);
                var parent = elem.closest('select');

                if (parent.attr('id') == 'projetos-added')
                    $('#projetos-list').append(elem);
                else
                    $('#projetos-added').append(elem);
            },
            pesquisaList: function (e) {
                ComboboxFilter({
                    el: '#projetos-list',
                    input: $(e.target)
                });
            }
        });


        var ProjetoModel = Backbone.Model.extend({
            idAttribute: 'id_projetopedagogico',
            defaults: {
                id_projetopedagogico: null,
                st_projetopedagogico: ''
            }
        });

        ComboboxView({
            el: '#projetos-list',
            model: ProjetoModel,
            url: '/documentos/projetos-pedagogicos/',
            optionLabel: 'st_projetopedagogico'
        }, function () {
            var projetos = PesquisaIni.model.get('projetos_pedagogicos');
            for (var index in projetos) {
                $('#projetos-added').append(
                    $('#projetos-list option[value=' + projetos[index] + ']')
                );
            }
        });

        var ProjetoInitialize = new ProjetoItemView();
        G2S.layout.projetosPedagogicos.show(ProjetoInitialize);

    }
});

var PesquisaIni = new PesquisaView();
G2S.layout.filtros.show(PesquisaIni);

/**
 * Funcao usada para fazer a
 * busca dentro de um campo <select>
 */
function ComboboxFilter(params) {

    /**
     * @param params.el
     * @param params.input
     */

    if (typeof params !== "object") {
        console.log("ComboboxFilter ERRO: \n" +
            "O parametro deve ser um OBJETO, com os índices: \n" +
            " - el: '#element' // seletor da tag select \n" +
            " - input: '#element' // seletor do input text");
        return false;
    }

    var str = $(params.input).val().toLowerCase().removeAccents().trim();
    var comboBox = $(params.el);
    var options = comboBox.find('option');

    options.each(function () {
        var opt = $(this);
        if (!opt.attr('data-label'))
            opt.attr('data-label', $(this).text().toLowerCase().removeAccents().trim());
    });

    var noResultsOption =
        comboBox.find('.no-results').length ?
            comboBox.find('.no-results') :
            $('<option class="no-results" disabled="disabled" style="display: none;">Nenhum resultado encontrado</option>');
    comboBox.append(noResultsOption);

    var results = comboBox.find('option[data-label^="' + str + '"]').not(noResultsOption);

    if (str.length) {
        options.hide();
        if (results.length) {
            results.show();
            comboBox.find('option.no-results').hide();
        } else
            comboBox.find('option.no-results').show();
    } else {
        options.show();
        comboBox.find('option.no-results').hide();
    }
}

/**
 * Funcao que monta os campos
 * <select> com as opcoes
 */
function ComboboxView(params, fn) {

    /**
     * @param params.el
     * @param params.model
     * @param params.url
     * @param params.optionLabel
     * @param params.optionSelectedId
     * @param fn
     */

    if (typeof params !== "object") {
        console.log("ComboboxView ERRO: \n" +
            "O parametro deve ser um OBJETO, com os índices: \n" +
            " - el: '#element' // seletor da tag select \n" +
            " - model: BackboneModel // instancia BackboneModel, que contenha os atributos 'url' e 'idAttribute'" +
            " - optionLabel: 'field' // campo do BackboneModel que sera usado na descricao da tag option" +
            " - [opcional] optionSelectedId: 'value' // valor que sera comparado com os options para determinar o selecionado");
        return false;
    }

    var Collection = Backbone.Collection.extend({
        model: params.model,
        url: params.url
    });

    var ItemView = Marionette.ItemView.extend({
        tagName: "option",
        initialize: function () {
            _.bind(this, 'render');
        },
        render: function () {
            var elem = $(this.el);

            elem
                .attr('value', this.model.id)
                .html(this.model.get(params.optionLabel));

            if (params.optionSelectedId == this.model.id)
                elem.attr('selected', 'selected');

            return this;
        }
    });

    var Initialize = new Collection();
    Initialize.fetch({
        success: function () {
            var renderer = new CollectionView({
                collection: Initialize,
                childViewConstructor: ItemView,
                childViewTagName: 'option',
                el: $(params.el)
            });
            renderer.render().$el;

            if (typeof fn === 'function') {
                fn.call();
            }
        }
    });
}


/**
 * Prototipo (str.removeAccents())
 * usado para remover acentos de strings
 */
//Prototipos para remover acentos de string
var Latinise = {};
Latinise.latin_map = {"Á": "A", "Ă": "A", "Ắ": "A", "Ặ": "A", "Ằ": "A", "Ẳ": "A", "Ẵ": "A", "Ǎ": "A", "Â": "A", "Ấ": "A", "Ậ": "A", "Ầ": "A", "Ẩ": "A", "Ẫ": "A", "Ä": "A", "Ǟ": "A", "Ȧ": "A", "Ǡ": "A", "Ạ": "A", "Ȁ": "A", "À": "A", "Ả": "A", "Ȃ": "A", "Ā": "A", "Ą": "A", "Å": "A", "Ǻ": "A", "Ḁ": "A", "Ⱥ": "A", "Ã": "A", "Ꜳ": "AA", "Æ": "AE", "Ǽ": "AE", "Ǣ": "AE", "Ꜵ": "AO", "Ꜷ": "AU", "Ꜹ": "AV", "Ꜻ": "AV", "Ꜽ": "AY", "Ḃ": "B", "Ḅ": "B", "Ɓ": "B", "Ḇ": "B", "Ƀ": "B", "Ƃ": "B", "Ć": "C", "Č": "C", "Ç": "C", "Ḉ": "C", "Ĉ": "C", "Ċ": "C", "Ƈ": "C", "Ȼ": "C", "Ď": "D", "Ḑ": "D", "Ḓ": "D", "Ḋ": "D", "Ḍ": "D", "Ɗ": "D", "Ḏ": "D", "ǲ": "D", "ǅ": "D", "Đ": "D", "Ƌ": "D", "Ǳ": "DZ", "Ǆ": "DZ", "É": "E", "Ĕ": "E", "Ě": "E", "Ȩ": "E", "Ḝ": "E", "Ê": "E", "Ế": "E", "Ệ": "E", "Ề": "E", "Ể": "E", "Ễ": "E", "Ḙ": "E", "Ë": "E", "Ė": "E", "Ẹ": "E", "Ȅ": "E", "È": "E", "Ẻ": "E", "Ȇ": "E", "Ē": "E", "Ḗ": "E", "Ḕ": "E", "Ę": "E", "Ɇ": "E", "Ẽ": "E", "Ḛ": "E", "Ꝫ": "ET", "Ḟ": "F", "Ƒ": "F", "Ǵ": "G", "Ğ": "G", "Ǧ": "G", "Ģ": "G", "Ĝ": "G", "Ġ": "G", "Ɠ": "G", "Ḡ": "G", "Ǥ": "G", "Ḫ": "H", "Ȟ": "H", "Ḩ": "H", "Ĥ": "H", "Ⱨ": "H", "Ḧ": "H", "Ḣ": "H", "Ḥ": "H", "Ħ": "H", "Í": "I", "Ĭ": "I", "Ǐ": "I", "Î": "I", "Ï": "I", "Ḯ": "I", "İ": "I", "Ị": "I", "Ȉ": "I", "Ì": "I", "Ỉ": "I", "Ȋ": "I", "Ī": "I", "Į": "I", "Ɨ": "I", "Ĩ": "I", "Ḭ": "I", "Ꝺ": "D", "Ꝼ": "F", "Ᵹ": "G", "Ꞃ": "R", "Ꞅ": "S", "Ꞇ": "T", "Ꝭ": "IS", "Ĵ": "J", "Ɉ": "J", "Ḱ": "K", "Ǩ": "K", "Ķ": "K", "Ⱪ": "K", "Ꝃ": "K", "Ḳ": "K", "Ƙ": "K", "Ḵ": "K", "Ꝁ": "K", "Ꝅ": "K", "Ĺ": "L", "Ƚ": "L", "Ľ": "L", "Ļ": "L", "Ḽ": "L", "Ḷ": "L", "Ḹ": "L", "Ⱡ": "L", "Ꝉ": "L", "Ḻ": "L", "Ŀ": "L", "Ɫ": "L", "ǈ": "L", "Ł": "L", "Ǉ": "LJ", "Ḿ": "M", "Ṁ": "M", "Ṃ": "M", "Ɱ": "M", "Ń": "N", "Ň": "N", "Ņ": "N", "Ṋ": "N", "Ṅ": "N", "Ṇ": "N", "Ǹ": "N", "Ɲ": "N", "Ṉ": "N", "Ƞ": "N", "ǋ": "N", "Ñ": "N", "Ǌ": "NJ", "Ó": "O", "Ŏ": "O", "Ǒ": "O", "Ô": "O", "Ố": "O", "Ộ": "O", "Ồ": "O", "Ổ": "O", "Ỗ": "O", "Ö": "O", "Ȫ": "O", "Ȯ": "O", "Ȱ": "O", "Ọ": "O", "Ő": "O", "Ȍ": "O", "Ò": "O", "Ỏ": "O", "Ơ": "O", "Ớ": "O", "Ợ": "O", "Ờ": "O", "Ở": "O", "Ỡ": "O", "Ȏ": "O", "Ꝋ": "O", "Ꝍ": "O", "Ō": "O", "Ṓ": "O", "Ṑ": "O", "Ɵ": "O", "Ǫ": "O", "Ǭ": "O", "Ø": "O", "Ǿ": "O", "Õ": "O", "Ṍ": "O", "Ṏ": "O", "Ȭ": "O", "Ƣ": "OI", "Ꝏ": "OO", "Ɛ": "E", "Ɔ": "O", "Ȣ": "OU", "Ṕ": "P", "Ṗ": "P", "Ꝓ": "P", "Ƥ": "P", "Ꝕ": "P", "Ᵽ": "P", "Ꝑ": "P", "Ꝙ": "Q", "Ꝗ": "Q", "Ŕ": "R", "Ř": "R", "Ŗ": "R", "Ṙ": "R", "Ṛ": "R", "Ṝ": "R", "Ȑ": "R", "Ȓ": "R", "Ṟ": "R", "Ɍ": "R", "Ɽ": "R", "Ꜿ": "C", "Ǝ": "E", "Ś": "S", "Ṥ": "S", "Š": "S", "Ṧ": "S", "Ş": "S", "Ŝ": "S", "Ș": "S", "Ṡ": "S", "Ṣ": "S", "Ṩ": "S", "Ť": "T", "Ţ": "T", "Ṱ": "T", "Ț": "T", "Ⱦ": "T", "Ṫ": "T", "Ṭ": "T", "Ƭ": "T", "Ṯ": "T", "Ʈ": "T", "Ŧ": "T", "Ɐ": "A", "Ꞁ": "L", "Ɯ": "M", "Ʌ": "V", "Ꜩ": "TZ", "Ú": "U", "Ŭ": "U", "Ǔ": "U", "Û": "U", "Ṷ": "U", "Ü": "U", "Ǘ": "U", "Ǚ": "U", "Ǜ": "U", "Ǖ": "U", "Ṳ": "U", "Ụ": "U", "Ű": "U", "Ȕ": "U", "Ù": "U", "Ủ": "U", "Ư": "U", "Ứ": "U", "Ự": "U", "Ừ": "U", "Ử": "U", "Ữ": "U", "Ȗ": "U", "Ū": "U", "Ṻ": "U", "Ų": "U", "Ů": "U", "Ũ": "U", "Ṹ": "U", "Ṵ": "U", "Ꝟ": "V", "Ṿ": "V", "Ʋ": "V", "Ṽ": "V", "Ꝡ": "VY", "Ẃ": "W", "Ŵ": "W", "Ẅ": "W", "Ẇ": "W", "Ẉ": "W", "Ẁ": "W", "Ⱳ": "W", "Ẍ": "X", "Ẋ": "X", "Ý": "Y", "Ŷ": "Y", "Ÿ": "Y", "Ẏ": "Y", "Ỵ": "Y", "Ỳ": "Y", "Ƴ": "Y", "Ỷ": "Y", "Ỿ": "Y", "Ȳ": "Y", "Ɏ": "Y", "Ỹ": "Y", "Ź": "Z", "Ž": "Z", "Ẑ": "Z", "Ⱬ": "Z", "Ż": "Z", "Ẓ": "Z", "Ȥ": "Z", "Ẕ": "Z", "Ƶ": "Z", "Ĳ": "IJ", "Œ": "OE", "ᴀ": "A", "ᴁ": "AE", "ʙ": "B", "ᴃ": "B", "ᴄ": "C", "ᴅ": "D", "ᴇ": "E", "ꜰ": "F", "ɢ": "G", "ʛ": "G", "ʜ": "H", "ɪ": "I", "ʁ": "R", "ᴊ": "J", "ᴋ": "K", "ʟ": "L", "ᴌ": "L", "ᴍ": "M", "ɴ": "N", "ᴏ": "O", "ɶ": "OE", "ᴐ": "O", "ᴕ": "OU", "ᴘ": "P", "ʀ": "R", "ᴎ": "N", "ᴙ": "R", "ꜱ": "S", "ᴛ": "T", "ⱻ": "E", "ᴚ": "R", "ᴜ": "U", "ᴠ": "V", "ᴡ": "W", "ʏ": "Y", "ᴢ": "Z", "á": "a", "ă": "a", "ắ": "a", "ặ": "a", "ằ": "a", "ẳ": "a", "ẵ": "a", "ǎ": "a", "â": "a", "ấ": "a", "ậ": "a", "ầ": "a", "ẩ": "a", "ẫ": "a", "ä": "a", "ǟ": "a", "ȧ": "a", "ǡ": "a", "ạ": "a", "ȁ": "a", "à": "a", "ả": "a", "ȃ": "a", "ā": "a", "ą": "a", "ᶏ": "a", "ẚ": "a", "å": "a", "ǻ": "a", "ḁ": "a", "ⱥ": "a", "ã": "a", "ꜳ": "aa", "æ": "ae", "ǽ": "ae", "ǣ": "ae", "ꜵ": "ao", "ꜷ": "au", "ꜹ": "av", "ꜻ": "av", "ꜽ": "ay", "ḃ": "b", "ḅ": "b", "ɓ": "b", "ḇ": "b", "ᵬ": "b", "ᶀ": "b", "ƀ": "b", "ƃ": "b", "ɵ": "o", "ć": "c", "č": "c", "ç": "c", "ḉ": "c", "ĉ": "c", "ɕ": "c", "ċ": "c", "ƈ": "c", "ȼ": "c", "ď": "d", "ḑ": "d", "ḓ": "d", "ȡ": "d", "ḋ": "d", "ḍ": "d", "ɗ": "d", "ᶑ": "d", "ḏ": "d", "ᵭ": "d", "ᶁ": "d", "đ": "d", "ɖ": "d", "ƌ": "d", "ı": "i", "ȷ": "j", "ɟ": "j", "ʄ": "j", "ǳ": "dz", "ǆ": "dz", "é": "e", "ĕ": "e", "ě": "e", "ȩ": "e", "ḝ": "e", "ê": "e", "ế": "e", "ệ": "e", "ề": "e", "ể": "e", "ễ": "e", "ḙ": "e", "ë": "e", "ė": "e", "ẹ": "e", "ȅ": "e", "è": "e", "ẻ": "e", "ȇ": "e", "ē": "e", "ḗ": "e", "ḕ": "e", "ⱸ": "e", "ę": "e", "ᶒ": "e", "ɇ": "e", "ẽ": "e", "ḛ": "e", "ꝫ": "et", "ḟ": "f", "ƒ": "f", "ᵮ": "f", "ᶂ": "f", "ǵ": "g", "ğ": "g", "ǧ": "g", "ģ": "g", "ĝ": "g", "ġ": "g", "ɠ": "g", "ḡ": "g", "ᶃ": "g", "ǥ": "g", "ḫ": "h", "ȟ": "h", "ḩ": "h", "ĥ": "h", "ⱨ": "h", "ḧ": "h", "ḣ": "h", "ḥ": "h", "ɦ": "h", "ẖ": "h", "ħ": "h", "ƕ": "hv", "í": "i", "ĭ": "i", "ǐ": "i", "î": "i", "ï": "i", "ḯ": "i", "ị": "i", "ȉ": "i", "ì": "i", "ỉ": "i", "ȋ": "i", "ī": "i", "į": "i", "ᶖ": "i", "ɨ": "i", "ĩ": "i", "ḭ": "i", "ꝺ": "d", "ꝼ": "f", "ᵹ": "g", "ꞃ": "r", "ꞅ": "s", "ꞇ": "t", "ꝭ": "is", "ǰ": "j", "ĵ": "j", "ʝ": "j", "ɉ": "j", "ḱ": "k", "ǩ": "k", "ķ": "k", "ⱪ": "k", "ꝃ": "k", "ḳ": "k", "ƙ": "k", "ḵ": "k", "ᶄ": "k", "ꝁ": "k", "ꝅ": "k", "ĺ": "l", "ƚ": "l", "ɬ": "l", "ľ": "l", "ļ": "l", "ḽ": "l", "ȴ": "l", "ḷ": "l", "ḹ": "l", "ⱡ": "l", "ꝉ": "l", "ḻ": "l", "ŀ": "l", "ɫ": "l", "ᶅ": "l", "ɭ": "l", "ł": "l", "ǉ": "lj", "ſ": "s", "ẜ": "s", "ẛ": "s", "ẝ": "s", "ḿ": "m", "ṁ": "m", "ṃ": "m", "ɱ": "m", "ᵯ": "m", "ᶆ": "m", "ń": "n", "ň": "n", "ņ": "n", "ṋ": "n", "ȵ": "n", "ṅ": "n", "ṇ": "n", "ǹ": "n", "ɲ": "n", "ṉ": "n", "ƞ": "n", "ᵰ": "n", "ᶇ": "n", "ɳ": "n", "ñ": "n", "ǌ": "nj", "ó": "o", "ŏ": "o", "ǒ": "o", "ô": "o", "ố": "o", "ộ": "o", "ồ": "o", "ổ": "o", "ỗ": "o", "ö": "o", "ȫ": "o", "ȯ": "o", "ȱ": "o", "ọ": "o", "ő": "o", "ȍ": "o", "ò": "o", "ỏ": "o", "ơ": "o", "ớ": "o", "ợ": "o", "ờ": "o", "ở": "o", "ỡ": "o", "ȏ": "o", "ꝋ": "o", "ꝍ": "o", "ⱺ": "o", "ō": "o", "ṓ": "o", "ṑ": "o", "ǫ": "o", "ǭ": "o", "ø": "o", "ǿ": "o", "õ": "o", "ṍ": "o", "ṏ": "o", "ȭ": "o", "ƣ": "oi", "ꝏ": "oo", "ɛ": "e", "ᶓ": "e", "ɔ": "o", "ᶗ": "o", "ȣ": "ou", "ṕ": "p", "ṗ": "p", "ꝓ": "p", "ƥ": "p", "ᵱ": "p", "ᶈ": "p", "ꝕ": "p", "ᵽ": "p", "ꝑ": "p", "ꝙ": "q", "ʠ": "q", "ɋ": "q", "ꝗ": "q", "ŕ": "r", "ř": "r", "ŗ": "r", "ṙ": "r", "ṛ": "r", "ṝ": "r", "ȑ": "r", "ɾ": "r", "ᵳ": "r", "ȓ": "r", "ṟ": "r", "ɼ": "r", "ᵲ": "r", "ᶉ": "r", "ɍ": "r", "ɽ": "r", "ↄ": "c", "ꜿ": "c", "ɘ": "e", "ɿ": "r", "ś": "s", "ṥ": "s", "š": "s", "ṧ": "s", "ş": "s", "ŝ": "s", "ș": "s", "ṡ": "s", "ṣ": "s", "ṩ": "s", "ʂ": "s", "ᵴ": "s", "ᶊ": "s", "ȿ": "s", "ɡ": "g", "ᴑ": "o", "ᴓ": "o", "ᴝ": "u", "ť": "t", "ţ": "t", "ṱ": "t", "ț": "t", "ȶ": "t", "ẗ": "t", "ⱦ": "t", "ṫ": "t", "ṭ": "t", "ƭ": "t", "ṯ": "t", "ᵵ": "t", "ƫ": "t", "ʈ": "t", "ŧ": "t", "ᵺ": "th", "ɐ": "a", "ᴂ": "ae", "ǝ": "e", "ᵷ": "g", "ɥ": "h", "ʮ": "h", "ʯ": "h", "ᴉ": "i", "ʞ": "k", "ꞁ": "l", "ɯ": "m", "ɰ": "m", "ᴔ": "oe", "ɹ": "r", "ɻ": "r", "ɺ": "r", "ⱹ": "r", "ʇ": "t", "ʌ": "v", "ʍ": "w", "ʎ": "y", "ꜩ": "tz", "ú": "u", "ŭ": "u", "ǔ": "u", "û": "u", "ṷ": "u", "ü": "u", "ǘ": "u", "ǚ": "u", "ǜ": "u", "ǖ": "u", "ṳ": "u", "ụ": "u", "ű": "u", "ȕ": "u", "ù": "u", "ủ": "u", "ư": "u", "ứ": "u", "ự": "u", "ừ": "u", "ử": "u", "ữ": "u", "ȗ": "u", "ū": "u", "ṻ": "u", "ų": "u", "ᶙ": "u", "ů": "u", "ũ": "u", "ṹ": "u", "ṵ": "u", "ᵫ": "ue", "ꝸ": "um", "ⱴ": "v", "ꝟ": "v", "ṿ": "v", "ʋ": "v", "ᶌ": "v", "ⱱ": "v", "ṽ": "v", "ꝡ": "vy", "ẃ": "w", "ŵ": "w", "ẅ": "w", "ẇ": "w", "ẉ": "w", "ẁ": "w", "ⱳ": "w", "ẘ": "w", "ẍ": "x", "ẋ": "x", "ᶍ": "x", "ý": "y", "ŷ": "y", "ÿ": "y", "ẏ": "y", "ỵ": "y", "ỳ": "y", "ƴ": "y", "ỷ": "y", "ỿ": "y", "ȳ": "y", "ẙ": "y", "ɏ": "y", "ỹ": "y", "ź": "z", "ž": "z", "ẑ": "z", "ʑ": "z", "ⱬ": "z", "ż": "z", "ẓ": "z", "ȥ": "z", "ẕ": "z", "ᵶ": "z", "ᶎ": "z", "ʐ": "z", "ƶ": "z", "ɀ": "z", "ﬀ": "ff", "ﬃ": "ffi", "ﬄ": "ffl", "ﬁ": "fi", "ﬂ": "fl", "ĳ": "ij", "œ": "oe", "ﬆ": "st", "ₐ": "a", "ₑ": "e", "ᵢ": "i", "ⱼ": "j", "ₒ": "o", "ᵣ": "r", "ᵤ": "u", "ᵥ": "v", "ₓ": "x"};
String.prototype.removeAccents = function () {
    return this.replace(/[^A-Za-z0-9\[\] ]/g, function (a) {
        return Latinise.latin_map[a] || a;
    });
};

var ContasdeEmailCollection = Backbone.Collection.extend({
    model: EmailConfig,
    url: function () {
        return this.id ? '/api/email-config/' + this.id : '/api/email-config'
    }
});

var EmailItemView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: '#item-lista',
    onRender: function () {
        return this;
    },
    abrirModal: function () {
        var model = this.model;
        this.$el.find("#myModal").modal('show');
        new ModalItemView({
            model: model,
            el: '#myModal #container-modal'
        }).render();
    },
    deletarEmail: function () {
        that = this;
        if (that.model.id) {
            //bootbox.setLocale('pt-BR');
            bootbox.confirm("Tem certeza de que deseja deletar esta conta de e-mail?", function (result) {
                if (result) {
                    that.model.destroy({});
                    that.remove();
                    bootbox.hideAll();
                    $.pnotify({title: 'E-mail Removido', text: 'O e-mail foi deletado com sucesso!', type: 'success'});
                    var collection = new ContasdeEmailCollection();
                    collection.fetch();
                    var emailsView = new EmailsCollectionView({
                        collection: collection
                    });

                    G2S.show(emailsView);
                    loaded();
                }
            });
        }
    },
    events: {
        'click .btn-danger': 'deletarEmail',
        'click .btn-primary': 'abrirModal'
    }
});

var ModalItemView = Marionette.ItemView.extend({
    template: '#emailModal',
    onRender: function () {
        return this;
    },
    salvar: function (e) {
        e.preventDefault();
        var that = this;
        var data = {
            'st_conta': $("#inputEmail").val(),
            'st_usuario': $("#inputUser").val(),
            'st_senha': $("#inputPassword").val() ? $("#inputPassword").val() : this.model.get('st_senha'),
            'st_smtp': $("#inputSmtp").val(),
            'nu_portasaida': $("#inputPortaSaida").val(),
            'bl_autenticacaosegura': $("#chkautenticacao").prop('checked'),
            'id_tipoconexaoemailsaida': $("#selectTipoConexao option:selected").val(),
            'st_pop': $("#inputPop").val(),
            'st_imap': $("#inputImap").val(),
            'nu_portaentrada': $("#inputPortaEntrada").val(),
            'id_tipoconexaoemailentrada': $("#selectConexaoEntrada option:selected").val(),
            'bl_ativo': true
        };

        //if ($("#inputPassword").val() != 'Doctor Who?') {
        $.ajax({
            dataType: 'json',
            type: 'post',
            url: '/contas-de-email/verificar-email',
            data: data,
            beforeSend: function () {
                loading();
            },
            success: function (result) {
                if (result.status) {
                    that.salvarEmailConfiguracao(data);
                } else {

                    var stringMensagem = "";
                    $.each(result.mensagem, function (i, value) {
                        stringMensagem += value + "<br>";
                    });

                    $.pnotify({
                        title: 'Erro!',
                        text: stringMensagem,
                        type: 'danger'
                    });
                    return false;
                }
                return false;

            },
            complete: function () {
                loaded();
            },
            error: function (response) {
                var textResponse = JSON.parse(response.responseText);
                $.pnotify(textResponse);
            }
        });
        //} else {
        //    delete(data['st_senha']);
        //that.salvarEmailConfiguracao(data);
        //$.pnotify({title: 'Registro', text: 'Senha inalterada!', type: 'success'});
        //}
        //return false;
    },
    salvarEmailConfiguracao: function (data) {
        data.bl_erro = 0;
        this.model.save(data, {
            beforeSend: function () {
                loading();
            },
            success: function () {
                $('#myModal').modal('hide');
                that.render();
                $.pnotify({title: 'Registro', text: 'Salvo com sucesso!', type: 'success'});

            },
            complete: function () {
                var collection = new ContasdeEmailCollection();
                collection.fetch();
                var emailsView = new EmailsCollectionView({
                    collection: collection
                });

                G2S.show(emailsView);
                loaded();
            },
            error: function () {
                $.pnotify({title: 'Erro', text: 'Operação não realizada!', type: 'danger'});
                $('#myModal').modal('hide');
            }
        });
        that = this;
        return false;
    },
    events: {
        'submit form#form-email': 'salvar'
    }

});

var EmailsCollectionView = Marionette.CompositeView.extend({
    template: '#lista',
    childView: EmailItemView,
    childViewContainer: 'tbody',
    adicionar: function () {
        var model = new EmailConfig();
        this.$el.find("#myModal").modal('show');
        new ModalItemView({
            model: model,
            el: '#myModal #container-modal'
        }).render();
    },
    events: {
        'click #btn-adicionar': 'adicionar'
    }

});


var collection = new ContasdeEmailCollection();
collection.fetch();
var emailsView = new EmailsCollectionView({
    collection: collection
});

$("#btn").click(function () {
    $(this).hide();
});

G2S.show(emailsView);
loaded();
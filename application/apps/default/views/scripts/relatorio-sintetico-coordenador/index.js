/******************************************************************************
 *              Relatorio Sintetico Coordenador
 *              Arthur Luiz Lara Quites  <arthur.quites@unyleya.com.br>
 *              17-04-2018
 * ****************************************************************************
 */
var coordenador = false;
var dados = '';
var CoordenadorCollection = this.Backbone.Collection.extend({
    url: '/relatorio-sintetico-coordenador/retorna-coordenadores-de-projeto-pedagogico',
    model: Backbone.Model,
});

var RelatorioCollection = this.Backbone.PageableCollection.extend({
    url: '/relatorio-sintetico-coordenador/retorna-relatorio-sintetico-coordenador-projetos-pedagogicos',
    model: Backbone.Model,
    mode:'client',
    parseState: function (resp, queryParams, state, options) {
        dados = resp;
    },
    parseRecords: function (resp, options) {
        return resp;
    }
    // state: {
    //     pageSize: 10
    // },
});

var ProjetosPedagogicosColletion = this.Backbone.Collection.extend({
    url: '/relatorio-sintetico-coordenador/recuperar-projetos-pedagogicos-coordenador',
    model: Backbone.Model,
});

var TelaGradeLayout = Marionette.LayoutView.extend({
    template: '#template-relatorio',
    tagName: 'div',
    regions: {
        tabelaGrade: "#regiao-grade-data",
        formgrade: '#regiao-grade'
    },
    onShow: function () {
        this.renderizaFormGrade();
        return this;
    },
    renderizaFormGrade: function () {
        var gradeView = new LayoutRelatorio();
        this.formgrade.show(gradeView);
    }
});


var LayoutRelatorio = Marionette.LayoutView.extend({
    template: '#template-form-folha-pagamento',
    tagName: 'fieldset',
    ui: {
        'id_coordenador': 'select[name="id_coordenador"]',
        'projetosPedagogicos': 'select[name="projetosPedagogicos"]',
        'btnPesquisar': '#btnPesquisar',
        'btnGeralExcel': '#btnGerarExcel',
        'div_coordenador': '#div_coordenador',
        'div_carregando': '#div_carregando',
        'data_inicio': '#data_inicio',
        'data_termino': '#data_termino',
        'div_datas': '#div_datas',
        'div_projetospedagogicos': '#div_projetospedagogicos'
    },
    events: {
        'click #btnGeralExcel': 'gerarExcel',
        'click #btnPesquisar': 'pesquisarDados',
        'change select[name="id_coordenador"]': 'populaSelectProjetoPedagogico',

    },
    onShow: function () {
        var verifica = this.verificacoordenadordeprojeto();
        var that = this;
        verifica.done(function(data){
            that.ui.div_carregando.hide();
            if(data.id_perfilpedagogico && data.id_perfilpedagogico !== null && data.id_perfilpedagogico.id_perfilpedagogico == 2) {
                that.populaSelectProjetoPedagogico();
                coordenador = true;
            } else {
                that.ui.div_coordenador.show();
                that.populaSelectCoordenadores();
            }
        });
        return this;
    },
    gerarExcel: function () {
        var that = this;
        if(dados.length > 0) {
            var grid = new Backgrid.Grid({
                columns: that.getColumnsGrid(),
                collection: dados.fullCollection,
                className: 'backgrid backgrid-selectall table table-striped table-bordered table-hover table'
            });

            var file = new Blob(["\ufeff","<table>" + grid.render().$el[0].innerHTML + "<table/>"], {type: "application/vnd.ms-excel"});
            var url = URL.createObjectURL(file);
            var a = $("<a />", {
                href: url,
                download: "relatorio.xls"
            })
                .appendTo("body")
                .get(0)
                .click();
        }else{
            $.pnotify({title: 'Aviso', text: 'Não é possível gerar um XLS sem dados', type: 'warning'});
            return true;
        }
    },
    populaSelectCoordenadores: function () {
        var that = this;

        var collectionCoordenadores = new CoordenadorCollection();
        collectionCoordenadores.fetch({
            async: false,
            beforeSend: function () {
                that.ui.id_coordenador.empty();
                that.ui.id_coordenador.append('<option value="Todos">Todos</option>');
            },
            success: function (collection, response) {
                var view = new SelectView({
                    el: that.ui.id_coordenador,
                    collection: collection,
                    childViewOptions: {
                        optionLabel: 'st_nomecompleto',
                        optionValue: 'id_usuario',
                        // optionSelected: optionSelected
                    }
                });
                view.render();
            },
            complete: function () {
            }
        });
    },
    populaSelectProjetoPedagogico: function (){
        var that = this;
        if(isNaN(that.ui.id_coordenador.find('option:selected').val())){
            that.ui.projetosPedagogicos.empty();
            that.ui.projetosPedagogicos.append('<option value="">Todos</option>');
            return true;
        }
        var projetosPedagogicos = new ProjetosPedagogicosColletion();
        projetosPedagogicos.fetch({
            async: false,
            data: {
                'id_coordenador': that.ui.id_coordenador.find('option:selected').val() ? that.ui.id_coordenador.find('option:selected').val():  G2S.loggedUser.get('id_usuario')
            },
            type: 'POST',
            beforeSend: function () {
                that.ui.projetosPedagogicos.empty();
                that.ui.projetosPedagogicos.append('<option value="">Todos</option>');
            },
            success: function (collection, response) {
                var view = new SelectView({
                    el: that.ui.projetosPedagogicos,
                    collection: collection,
                    childViewOptions: {
                        optionLabel: 'st_projetopedagogico',
                        optionValue: 'id_projetopedagogico'
                    }
                });
                that.ui.div_projetospedagogicos.show();
                view.render();
            },
            complete: function () {
            }
        });
    },
    verificacoordenadordeprojeto: function(){
        var perfil = G2S.loggedUser.get('id_perfil');
        var params = {
            'id_perfil': perfil
        };
        return $.ajax({
            url : '/relatorio-sintetico-coordenador/verificacoordenadordeprojeto',
            type: 'POST',
            data: params
        });
    },
    pesquisarDados: function() {
        var that = this;
        if(that.ui.id_coordenador.is(':visible') && that.ui.id_coordenador.find('option:selected').val() == ''){
            $.pnotify({title: 'Aviso', text: 'Selecione o coordenador', type: 'warning'});
            return true;
        }

        if(!that.ui.data_inicio.val()){
            $.pnotify({title: 'Aviso', text: 'Informe a data de matrícula', type: 'warning'});
            return true;
        }
        var collectionRelatorio = new RelatorioCollection();

        collectionRelatorio.fetch({
            async: true,
            data: {
                'id_usuario': that.ui.id_coordenador.find('option:selected').val() ? that.ui.id_coordenador.find('option:selected').val():  G2S.loggedUser.get('id_usuario'),
                'id_projetopedagogico': that.ui.projetosPedagogicos.find('option:selected').val(),
                'data_inicio': telaGrade.$el.find('#data_inicio').val(),
                'data_termino': telaGrade.$el.find('#data_termino').val()
            },
            type: 'POST',
            beforeSend: function () {
                loading();
            },
            success: function (collection, response) {
                if(response.length > 0 && response.type !== 'error'){
                    dados = collection;
                    var grid = new Backgrid.Grid({
                        columns: that.getColumnsGrid(),
                        collection: collection,
                        className: 'backgrid backgrid-selectall table table-striped table-bordered table-hover table'
                    });
                    var paginator = new Backgrid.Extension.Paginator({
                        collection: collection
                    });
                    telaGrade.$el.find('#qntdReg').text("Foram encontrados "+dados.fullCollection.length+" registros");

                    telaGrade.$el.find('#regiao-grade-data').html(grid.render().$el);
                    telaGrade.$el.find('#regiao-grade-data').append(paginator.render().$el);
                }else if(response.type === 'error') {
                    $.pnotify({title: 'Erro', text: response.text, type: 'error'});
                }else{
                    telaGrade.$el.find('#regiao-grade-data').empty();
                    telaGrade.$el.find('#qntdReg').empty();
                    $.pnotify({title: 'Aviso', text: 'Não existem registros para o período informado.', type: 'warning'});
                }
            },complete:function(){
              loaded();
            },
            error: function (erro) {
                $.pnotify({title: 'Aviso', text: 'Não foi possível recuperar os dados', type: 'error'});
            }
        });
    },
    getColumnsGrid: function () {
        var columns = [];
        if(!coordenador){
            columns.push({
                name: "st_nomecompleto",
                label: "Nome do Coordenador",
                cell: "string",
                editable:false
            });
        }
        columns.push({
            name: "st_projetopedagogico",
            label: "Nome do Projeto Pedagógico",
            cell: "string",
            editable:false
        }, {
            name: "bloqueado",
            label: "Bloqueados",
            cell: "string",
            editable:false
        }, {
            name: "cancelado",
            label: "Cancelados",
            cell: "string",
            editable:false
        }, {
            name: "certificado",
            label: "Certificados",
            cell: "string",
            editable:false
        }, {
            name: "concluinte",
            label: "Concluintes",
            cell: "string",
            editable:false
        }, {
            name: "cursando",
            label: "Cursando",
            cell: "string",
            editable:false
        }, {
            name: "decurso_de_prazo",
            label: "Decurso de Prazo",
            cell: "string",
            editable:false
        }, {
            name: "trancado",
            label: "Trancados",
            cell: "string",
            editable:false
        },{
            name: "transferido",
            label: "Transferidos",
            cell: "string",
            editable:false
        });
        return columns;
    }
});

var telaGrade = new TelaGradeLayout();
G2S.show(telaGrade);
loaded();

// Include funcao ComboBoxView
$.ajax({
    url: '/js/comboboxview.js',
    dataType: "script",
    async: false
});

/**
 * Construir Tela
 */

var GlobalModel = Backbone.Model.extend({
    idAttribute: 'id_relatorio',
    defaults: {
        'id_relatorio': null,
        'st_relatorio': '',
        'st_origem': '',
        'bl_geral': false,
        'dt_cadastro': null,
        'st_descricao': '',
        'id_tipoorigemrelatorio': {
            'id_tipoorigemrelatorio': null
        },
        'id_funcionalidade': {
            'id_funcionalidade': null
        },
        'id_usuario': {
            'id_usuario': null
        },
        'id_entidade': {
            'id_entidade': null
        },
        campos: []
    },
    url: function () {
        return '/api/gerador-relatorio/' + (this.id || '');
    }
});

var GlobalView = Marionette.ItemView.extend({
    model: new GlobalModel(),
    tagName: 'div',
    template: '#global-template',
    onRender: function () {
        var that = this;
        var elem = this.$el;

        // Montar select "Tipo da Fonte de Dados"
        var Model = Backbone.Model.extend({
            idAttribute: 'id_tipoorigemrelatorio',
            defaults: {
                id_tipoorigemrelatorio: null,
                st_tipoorigemrelatorio: ''
            }
        });
        ComboboxView({
            el: elem.find('[name="id_tipoorigemrelatorio"]'),
            model: Model,
            url: '/api/tipo-origem-relatorio/',
            optionLabel: 'st_tipoorigemrelatorio',
            optionSelectedId: that.model.get('id_tipoorigemrelatorio').id_tipoorigemrelatorio,
            emptyOption: '[Selecione]'
        }, function (elem) {
            elem.trigger('change');
        });

        // Montar select "Fonte de Dados"
        var Model = Backbone.Model.extend({
            idAttribute: 'st_view',
            defaults: {
                st_view: ''
            }
        });
        ComboboxView({
            el: elem.find('select[data-name="st_origem"]'),
            model: Model,
            url: '/api/view/?schema_id=1',
            optionLabel: 'st_view',
            optionSelectedId: that.model.get('st_origem'),
            emptyOption: '[Selecione]'
        });

        CamposIni.collection.url = '/api/campo-relatorio/?id_relatorio=' + this.model.id;
        CamposIni.collection.fetch({
            success: function () {
                elem.find('#campos-content').html(CamposIni.render().$el);
            }
        });

        loaded();
    },
    events: {
        'submit form': 'actionSave',
        'change [name="id_tipoorigemrelatorio"]': 'actionFonte'
    },
    actionSave: function (e) {
        e.preventDefault();
loading();
        var that = this;
        var elem = this.$el;

        var values = {
            'st_relatorio': elem.find('[name="st_relatorio"]').val(),
            'st_origem': elem.find('[name="st_origem"]').val(),
            'bl_geral': elem.find('[name="bl_geral"]').is(':checked') ? true : false,
            'id_tipoorigemrelatorio': {
                'id_tipoorigemrelatorio': elem.find('[name="id_tipoorigemrelatorio"]').val()
            },
            'id_funcionalidade': {
                'id_funcionalidade': elem.find('[name="id_funcionalidade"]').val()
            },
            campos: []
        };


        // Validar campos obrigatórios
        var errorMsg = null;
        var errorElem = null;

        if (values.id_tipoorigemrelatorio.id_tipoorigemrelatorio.trim() === '') {
            errorMsg = 'Selecione o tipo da fonte de dads';
            errorElem = $('[name="id_tipoorigemrelatorio"]');
        }

        if (errorMsg) {
            $.pnotify({
                title: 'Erro',
                text: errorMsg,
                type: 'error'
            });
            errorElem.focus();
            return false;
        }

        var campos = $('#campos-content .campo-item');
        campos.each(function () {
            var item = $(this);

            camposValues = {
                st_camporelatorio: item.find('[name="st_camporelatorio"]').val(),
                st_titulocampo: item.find('[name="st_titulocampo"]').val(),
                bl_exibido: item.find('[name="bl_exibido"]').is(':checked') ? true : false,
                bl_filtro: item.find('[name="bl_filtro"]').is(':checked') ? true : false,
                nu_ordem: Number(item.find('[name="nu_ordem"]').val()),
                bl_obrigatorio: item.find('[name="bl_obrigatorio"]').val()
            };

            if (camposValues.bl_filtro) {
                // Sempre manter os índices atualizados com os registros da tabela tb_tipopropriedadecamporel
                camposValues.propriedade_campo_relatorio = {
                    1: item.find('[name="id_operacaorelatorio"]').val(),
                    2: item.find('[name="id_interface"]').val(),
                    3: item.find('[name="id_tipocampo"]').val(),
                    4: item.find('[name="st_default"]').val(),
                    5: item.find('[name="st_consulta"]').val(),
                    7: item.find('[name="st_campopai"]').val(),
                    9: item.find('[name="nu_rangeentredatas"]').val(),
                    10: item.find('[name="bl_customselect"]').val()
                };
            }

            values.campos.push(camposValues);

        });

        that.model.set(values);

        that.model.save(null, {
            success: function (model, response) {
                $.pnotify({
                    title: 'Concluído',
                    text: 'O registro foi salvo com sucesso!',
                    type: 'success'
                });
                if (response.codigo)
                    that.model.set('id_relatorio', response.codigo);
            },
            error: function () {
                $.pnotify({
                    title: 'Erro',
                    text: 'Houve um erro ao tentar salvar o registro, tente novamente.',
                    type: 'error'
                });
            },
            complete: function () {
                loaded();
            }
        })

    },
    actionFonte: function (e) {
        var elem = $(e.target);
        var value = elem.val();

        if (value == '1') {
            $('select[data-name="st_origem"]').attr('name', 'st_origem').show();
            $('input[data-name="st_origem"]').attr('name', '').hide();
        } else {
            $('select[data-name="st_origem"]').attr('name', '').hide();
            $('input[data-name="st_origem"]').attr('name', 'st_origem').show();
        }
    }
});


var TipoCampoList = false;
var InterfaceList = false;
var OperacaoRelatorioList = false;

$.ajaxSetup({async: false});
$.getJSON('/api/tipo-campo/', function (response) {
    TipoCampoList = response;
});
$.getJSON('/api/interface/', function (response) {
    InterfaceList = response;
});
$.getJSON('/api/operacao-relatorio/', function (response) {
    OperacaoRelatorioList = response;
});
$.ajaxSetup({async: true});


/**
 * Campos do Relatório
 */
var CamposModel = Backbone.Model.extend({
    idAttribute: 'id_camporelatorio',
    defaults: {
        id_camporelatorio: null,
        st_camporelatorio: '',
        st_titulocampo: '',
        bl_filtro: false,
        bl_exibido: false,
        nu_ordem: null,
        bl_obrigatorio: false,
        id_relatorio: null,
        propriedade_campo_relatorio: {
            1: null, // id_operacaorelatorio
            2: null, // id_interface
            3: null, // id_tipocampo
            4: null, // st_default
            5: null, // st_consulta
            7: null, // st_campopai
            9: null,  // nu_rangeentredatas
            10: null  // bl_customselect
        }
    }
});

var CamposView = Marionette.ItemView.extend({
    model: new CamposModel(),
    tagName: 'tr',
    className: 'campo-item',
    template: '#campos-item',
    ui: {
        idTipoCampo: '[name="id_tipocampo"]',
        nuRangeEntreDatas: '[name="nu_rangeentredatas"]',
        idInterface: '[name="id_interface"]',
        idOperacaoRelatorio: '[name="id_operacaorelatorio"]',
        blCustomSelect: '[name="bl_customselect"]'
    },
    events: {
        'click .campoOptions': 'campoOptions',
        'click .campoRemove': 'campoRemove',
        'click .campoRequired': 'campoRequired',
        'change [name="bl_filtro"]': 'toggleOptions',
        'change [name="id_tipocampo"]': 'toggleCampoRangeData',
        'change [name="id_operacaorelatorio"]': 'toggleCampoRangeData',
        'change [name="id_interface"]': 'toggleCustomSelect'
    },
    toggleCustomSelect: function () {
        if (this.ui.idInterface.val() == 2) {
            this.ui.blCustomSelect.removeAttr('disabled');
        } else {
            this.ui.blCustomSelect.attr('disabled', 'disabled');
        }
    },
    toggleCampoRangeData: function () {
        if ((this.ui.idTipoCampo.val() == 2 || this.ui.idTipoCampo.val() == 3) && this.ui.idOperacaoRelatorio.val() == 4) {
            this.ui.nuRangeEntreDatas.removeAttr('disabled');
        } else {
            this.ui.nuRangeEntreDatas.attr('disabled', 'disabled');
        }
    },
    onRender: function () {
        var that = this;
        var elem = this.$el.find('.campoOptionsList');
        /**
         * Montar Selects do botão OPCOES
         */
            // ID TIPO CAMPO
        var Model = Backbone.Model.extend({
                idAttribute: 'id_tipocampo',
                defaults: {
                    id_tipocampo: null,
                    st_tipocampo: ''
                }
            });
        ComboboxView({
            el: elem.find('[name="id_tipocampo"]'),
            model: Model,
            url: '/api/tipo-campo/',
            collection: TipoCampoList,
            optionLabel: 'st_tipocampo',
            optionSelectedId: that.model.get('propriedade_campo_relatorio')[3],
            emptyOption: '[Selecione]'
        });

        // ID INTERFACE
        Model = Backbone.Model.extend({
            idAttribute: 'id_interface',
            defaults: {
                id_interface: null,
                st_interface: ''
            }
        });
        ComboboxView({
            el: elem.find('[name="id_interface"]'),
            model: Model,
            url: '/api/interface/',
            collection: InterfaceList,
            optionLabel: 'st_interface',
            optionSelectedId: that.model.get('propriedade_campo_relatorio')[2],
            emptyOption: '[Selecione]'
        });

        // ID OPERACAO RELATORIO
        Model = Backbone.Model.extend({
            idAttribute: 'id_operacaorelatorio',
            defaults: {
                id_operacaorelatorio: null,
                st_operacaorelatorio: ''
            }
        });
        ComboboxView({
            el: elem.find('[name="id_operacaorelatorio"]'),
            model: Model,
            url: '/api/operacao-relatorio/',
            collection: OperacaoRelatorioList,
            optionLabel: 'st_operacaorelatorio',
            optionSelectedId: that.model.get('propriedade_campo_relatorio')[1],
            emptyOption: '[Selecione]'
        });
    },
    campoRequired: function (e) {
        var elem = $(e.target);
        var label = elem.closest('label');

        if (elem.is(':checked')) {
            label
                .addClass('btn-success')
                .attr('title', 'Campo ativo')
        } else {
            label
                .removeClass('btn-success')
                .attr('title', 'Campo inativo')
        }
    },
    campoOptions: function () {
        var elem = this.$el.find('.campoOptionsList');

        $('.campoOptionsList').not(elem).hide(200);

        elem.finish().toggle(300, function () {
            elem.find('input:first').focus();
        });
        this.toggleCampoRangeData();
        this.toggleCustomSelect();
    },
    campoRemove: function () {
        CamposIni.collection.remove(this.model);
    },
    toggleOptions: function (e) {
        var checked = $(e.target).prop('checked');
        var elem = this.$el.find('.campoOptions');
        elem.prop('disabled', !checked);
    }
});

var CamposCollection = Backbone.Collection.extend({
    model: CamposModel,
    url: '/api/campo-relatorio/'
});

var CamposCompositeView = Marionette.CompositeView.extend({
    template: '#campos-template',
    collection: new CamposCollection(),
    childView: CamposView,
    childViewContainer: 'tbody',
    onRender: function () {
        this.verifyEmpty();
    },
    events: {
        'click .camposAdd': 'camposAdd'
    },
    camposAdd: function () {
        var New = new CamposModel();
        CamposIni.collection.add(New);
        $('#campos-content tr:last input:first').focus();
    },
    collectionEvents: {
        'add': 'isNoEmpty',
        'remove': 'verifyEmpty'
    },
    verifyEmpty: function () {
        var elem = this.$el;

        var ViewEmpty = Marionette.ItemView.extend({
            tagName: 'tr',
            className: 'empty-view',
            template: _.template('<td colspan="5">Nenhum registro adicionado.</td>')
        });

        var items = elem.find('tbody tr').not('.empty-view');

        if (items.length <= 1)
            elem.find('tbody').append(new ViewEmpty().render().$el);
    },
    isNoEmpty: function () {
        var elem = this.$el;
        elem.find('.empty-view').remove();
    }
});


CamposIni = new CamposCompositeView();


/**
 * Inserir tudo no DOM
 */
// Render / Renderizar views
// Verificar se possui id no url
loading();
var GlobalIni = new GlobalView();

G2S.layout.addRegions({
    global: '#global-content'
});

var url = window.location.href;
var idParam = Number(url.split('/').pop());
if (!isNaN(idParam)) {
    var model = new GlobalModel();
    model.id = idParam;
    model.fetch({
        success: function (model, response) {
            model.set(response);

            GlobalIni = new GlobalView({
                model: model
            });

            G2S.layout.global.show(GlobalIni);
        }
    });
} else {
    G2S.layout.global.show(GlobalIni);
}
var ItensCollection = Backbone.Model.extend({
    defaults: {
        id_itemdematerial: '',
        st_itemdematerial: '',
        id_situacao: '',
        dt_cadastro: '',
        id_usuariocadastro: '',
        bl_portal: '',
        bl_ativo: '',
        id_entidade: '',
        nu_peso: '',
        id_unidade: '',
        st_unidade: '',
        id_turma: '',
        st_turma: '',
        id_disciplina: '',
        st_disciplina: '',
        nu_encontro: '',
        id_professor: '',
        st_professor: '',
        id_tipodematerial: '',
        st_tipodematerial: '',
        id_situacao: '',
        st_situacao: '',
        nu_valor: '',
        nu_qtdepaginas: '',
        id_upload: '',
        st_upload: ''
    }
})

var colecaoItens = Backbone.Collection.extend({
    model: ItensCollection
})

var colecaoTramite = Backbone.Model.extend({
    defaults: {
        id_tramite: '',
        st_tramite: '',
        dt_tramite: '',
        st_nomecompleto: ''
    }
})

var LayoutEntregaMaterial = Marionette.LayoutView.extend({
        template: '#entrega-de-material-presencial-layout',
        tagName: 'div',
        className: 'container-fluid',
        regions: {
            tabela: '#tabela-materiais-wrapper',
            modal: '#modal-itens-wrapper',
            tramite: '#tramite-wrapper'
        },
        events: {
            'change #st_item': 'carregaItemInput',
            'change #id_matricula': 'pesquisarItem'
        },
        ui: {
            'id_item': '#st_item',
            'id_matricula': '#id_matricula'
        },
        initialize: function () {
            thatLayout = this;

        },
        onShow: function () {


            if (!componenteAutoComplete.getFixModel()) {
                $('#content-dados').hide('fast');
                desabilitarAbas();
            }
            ;

            //Chama o componente de autocomplete
            componenteAutoComplete.main({
                model: VwPesquisarPessoa,
                element: $("#autoComplete")
            }, function () {
                thatLayout.abreLayoutGeral();
                thatLayout.pesquisarItem();
                thatLayout.renderizaLog();
                habilitarProximaAba();
            }, function () {
                thatLayout.fechaLayoutGeral();
                desabilitarAbas();
            });

            return this;
        },

        fechaLayoutGeral: function () {
            thatLayout.tabela.destroy();
            $('#content-dados').hide('fast');
            desabilitarAbas();
        },
        abreLayoutGeral: function () {
            thatLayout.populaSelectMatricula();
            $('#content-dados').show();
            habilitarProximaAba();
            this.ui.id_item.focus();
        },

        populaSelectMatricula: function () {
            var Collection = Backbone.Collection.extend({
                model: VwMatricula
            });
            var view;
            var that = this;
            var collectionMatriculas = new Collection();
            collectionMatriculas.url = 'item-de-material-presencial/retorna-matriculas/id/' + componenteAutoComplete.getFixModel()['id'];
            collectionMatriculas.fetch({
                beforeSend: function () {
                    loading()
                },
                success: function () {
                    view = new SelectView({
                        el: that.$el.find("select[name='id_matricula']"),      // Elemento da DOM
                        collection: collectionMatriculas,      // Instancia da collection a ser utilizada
                        childViewOptions: {
                            optionLabel: 'st_matricula', // Propriedade da Model que será utilizada como label do select
                            optionValue: 'id_matricula', // Propriedade da Model que será utilizada como id do option do select
                            optionSelected: null       // ID da option que receberá o atributo "selected"
                        }
                    });
                    view.render();
                },

                complete: function () {
                    loaded()
                }
            });
        },
        pesquisarItem: function () {
            that = this;
            var HtmlBotoesCell = Backgrid.Cell.extend({
                className: 'span2 acoes',
                render: function () {
                    var elTemp = null;

                    if (this.model.get('st_situacao') == 'Entregue') {
                        var elTemp = _.template('<div class="content" style="text-align: center">' +
                            '<button id="btn_devolver" class="btn btn-small btn-danger"><i class="icon icon-white icon-arrow-left"></i> Devolver</button>' +
                            '</div>'
                        )
                    } else {
                        var elTemp = _.template('<div class="content" style="text-align: center">' +
                            '<button id="btn_entregar" class="btn btn-small btn-success"><i class="icon icon-white icon-ok"></i> Entregar</button>' + " " +
                            '</div>'
                        );
                    }

                    this.$el.html(elTemp);
                    return this;
                }
            });

            var HtmlLinkMaterialCell = Backgrid.Cell.extend({
                className: 'span2',
                render: function () {
                    if (this.model.get('st_upload')) {
                        var elTemp = _.template(
                            "<a href='/upload/materiaispresenciais/" + this.model.get('st_upload') + "'><i class='icon-circle-arrow-down'></i>  " + this.model.get('st_upload') + '</a>'
                        );
                        this.$el.html(elTemp);
                    }
                    return this;
                }
            });

            var columns = [{
                name: "id_itemdematerial",
                label: "ID",
                editable: false,
                cell: "string"
            }, {
                name: "st_itemdematerial",
                label: 'Nome',
                editable: false,
                cell: "string"
            }, {
                name: "st_disciplina",
                label: "Disciplina",
                editable: false,
                cell: "string"
            }, {
                name: "st_turma",
                label: "Turma",
                editable: false,
                cell: "string"
            }, {
                name: "nu_valor",
                label: "Valor",
                editable: false,
                cell: "string"
            }, {
                name: "st_tipodematerial",
                label: "Tipo",
                editable: false,
                cell: "string"
            }, {
                name: "nu_encontro",
                label: "Encontro",
                editable: false,
                cell: "string"
            }, {
                name: "st_upload",
                label: "Arquivo",
                editable: false,
                cell: HtmlLinkMaterialCell
            }, {
                name: "acao",
                label: "Ações",
                cell: HtmlBotoesCell,
                editable: false
            }];

            var PageableItens = Backbone.PageableCollection.extend({
                model: ItensCollection,
                url: '/item-de-material-presencial/retorna-itens-entrega/id_usuario/' + componenteAutoComplete.getFixModel()['id'] + '/id_entidade/' + G2S.loggedUser.get('id_entidade') + '/id_matricula/' + that.ui.id_matricula.val(),
                state: {
                    pageSize: 10
                },
                mode: "client"
            });

            var RowGrid = Backgrid.Row.extend({
                initialize: function (options) {
                    RowGrid.__super__.initialize.apply(this, arguments);
                    this.$el.css('cursor', 'pointer');
                },
                entregar: function (e) {
                    var that = this;

                    var parametros = {
                        'id_itemdematerial': this.model.get('id_itemdematerial'),
                        'id_usuariocadastro': G2S.loggedUser.get('id_usuario'),
                        'id_disciplina': this.model.get('id_disciplina'),
                        'id_matricula': this.model.get('id_matricula'),
                        'id_matriculadisciplina': this.model.get('id_matriculadisciplina'),
                        'id_entregamaterial': this.model.get('id_entregamaterial') ? this.model.get('id_entregamaterial') : null
                    }

                    $.ajax({
                        dataType: 'json',
                        type: 'post',
                        url: '/item-de-material-presencial/entrega-table',
                        data: parametros,
                        beforeSend: function () {
                            loading();
                        },
                        success: function (data) {
                            if (data) {
                                thatLayout.renderizaLog();
                                $.pnotify({
                                    title: 'Sucesso!',
                                    text: 'Material entregue para o aluno ' + componenteAutoComplete.getFixModel().st_nomecompleto + '.',
                                    type: 'success'
                                });
                                that.model.set({'id_entregamaterial': data.id_entregamaterial});
                                that.$el.find('.acoes').html('<div class="content" style="text-align: center">' +
                                '<button id="btn_devolver" class="btn btn-small btn-danger"><i class="icon icon-white icon-arrow-left"></i> Devolver</button>' +
                                '</div>');
                            }
                        },
                        error: function (data) {
                            $.pnotify({
                                title: 'Erro!',
                                text: 'Material não entregue, entre em contato com o administrador do sistema!',
                                type: 'warning'
                            });
                        },
                        complete: function () {
                            loaded();
                        }
                    })

                },
                /**
                 * Devolver o material do aluno
                 */
                devolver: function () {
                    var that = this;

                    var parametros = {'id_entregamaterial': this.model.get('id_entregamaterial')};
                    bootbox.confirm("Tem certeza de que deseja finalizar esta matrícula?", function (result) {
                        if (result) {
                            $.ajax({
                                dataType: 'json',
                                type: 'post',
                                url: '/item-de-material-presencial/devolver-table',
                                data: parametros,
                                beforeSend: function () {
                                    loading();
                                },
                                success: function (data) {
                                    if (data) {
                                        thatLayout.renderizaLog();
                                        $.pnotify({
                                            title: 'Sucesso!',
                                            text: 'Material devolvido com sucesso ',
                                            type: 'success'
                                        });
                                        that.$el.find('.acoes').html('<div class="content" style="text-align: center">' +
                                        '<button id="btn_entregar" class="btn btn-small btn-success"><i class="icon icon-white icon-ok"></i> Entregar</button>' + " " +
                                        '</div>');
                                    }
                                },
                                error: function (data) {
                                    $.pnotify({
                                        title: 'Erro!',
                                        text: 'Material não entregue, entre em contato com o administrador do sistema!',
                                        type: 'warning'
                                    });
                                },
                                complete: function () {
                                    loaded();
                                }
                            })
                        }
                    })
                },
                events: {
                    "click #btn_entregar": "entregar",
                    "click #btn_devolver": "devolver"
                }
            });

            var collectionPesquisa = new PageableItens()

            var pageableGridItens = new Backgrid.Grid({
                className: 'backgrid table table-bordered',
                columns: columns,
                collection: collectionPesquisa,
                row: RowGrid,
                emptyText: "Nenhum registro encontrado"
            });

            var $elemento = $('#tabela-materiais-wrapper');
            $elemento.empty();
            $elemento.append(pageableGridItens.render().el);

            var paginator = new Backgrid.Extension.Paginator({
                collection: collectionPesquisa
            });

            $elemento.append(paginator.render().el);

            collectionPesquisa.fetch();
        },

        /**
         * efetua a entrega rapida do item.
         * @param e
         */
        carregaItemInput: function (e) {
            var that = this;
            parametros = {
                'id_itemdematerial': this.ui.id_item.val(),
                'id_usuario': componenteAutoComplete.getFixModel().id,
                'id_entidade': G2S.loggedUser.get('id_entidade')
            }

            $.ajax({
                dataType: 'json',
                type: 'post',
                url: '/item-de-material-presencial/entrega-rapida',
                data: parametros,
                beforeSend: function () {
                    loading();
                },
                success: function (data) {
                    if (data) {

                        if (data['tipo'] == 'success') {
                            $.pnotify({
                                title: data['title'],
                                text: data['mensagem'],
                                type: data['type']
                            });
                            if (data['type'] == 'success') {
                                thatLayout.pesquisarItem();
                                thatLayout.renderizaLog();
                            }
                            that.ui.id_item.val('');
                        } else if(data['tipo'] == 'entregue'){
                                bootbox.alert("Material já entregue para este usuário!");
                        }else {

                            var ViewSelecionaItem = Marionette.CompositeView.extend({
                                template: "#template-itens-modal",
                                childView: ViewItem,
                                childViewContainer: 'tbody',
                                onShow: function () {
                                    this.$el.parent().modal('show');
                                    return this;
                                }
                            });
                            loaded();
                            collectionItens = new colecaoItens(data);
                            var viewItens = new ViewSelecionaItem({
                                collection: collectionItens
                            });
                            thatLayout.modal.show(viewItens);
                        }
                    }
                },
                error: function (data) {
                    $.pnotify({
                        title: 'Erro!',
                        text: 'Material não entregue, entre em contato com o administrador do sistema!',
                        type: 'warning'
                    });
                },
                complete: function () {
                    loaded();
                }
            })
        },
        renderizaLog: function () {

            var span2 = Backgrid.Cell.extend({
                className: 'span2'
            });

            var columns = [{
                name: "id_tramite",
                label: "ID Trâmite",
                editable: false,
                cell: span2
            }, {
                name: "id_itemdematerial",
                label: "ID Item Material",
                cell: span2,
                editable: false
            }, {
                name: "st_tramite",
                label: "Texto",
                cell: "string",
                editable: false
            }, {
                name: "dt_tramite",
                label: "Data",
                cell: span2,
                editable: false
            }, {
                name: "st_nomecompleto",
                label: "Pessoa",
                cell: "string",
                editable: false
            }];

            var PageableLog = Backbone.PageableCollection.extend({
                model: colecaoTramite,
                url: '/item-de-material-presencial/retorna-tramite/id_usuario/' + componenteAutoComplete.getFixModel()['id'] + '/id_entidade/' + G2S.loggedUser.get('id_entidade'),
                state: {
                    pageSize: 10
                }
                ,
                mode: "client"
            });


            var pageableLog = new PageableLog();


            var pageableGridLog = new Backgrid.Grid({
                className: 'backgrid table table-bordered',
                columns: columns,
                collection: pageableLog,
                emptyText: "Nenhum registro encontrado"


            });

            var $elemento = $('#tramite-wrapper');
            $elemento.empty();
            $elemento.append(pageableGridLog.render().$el);

            var paginator = new Backgrid.Extension.Paginator({
                collection: pageableLog
            });

            $elemento.append(paginator.render().el);


            pageableLog.fetch({reset: true});


        }
    })
    ;

function habilitarProximaAba() {
    var abaAtiva = $('#tabDadosEntrega > li.active');
    abaAtiva.next('li').removeClass('hide');
};

function desabilitarAbas() {
    var primeiraAba = $('#tabDadosEntrega > li.active');
    primeiraAba.next('li').addClass('hide');
};

var ViewItem = Marionette.ItemView.extend({
    template: '#template-item',
    tagName: 'tr',
    events: {
        'click #entregar-modal': 'entregarItemModal'
    },
    entregarItemModal: function () {
        var that = this;
        var parametros = {
            'id_itemdematerial': this.model.get('id_itemdematerial'),
            'id_usuariocadastro': G2S.loggedUser.get('id_usuario'),
            'id_disciplina': this.model.get('id_disciplina'),
            'id_matricula': this.model.get('id_matricula'),
            'id_matriculadisciplina': this.model.get('id_matriculadisciplina'),
            'id_entregamaterial': this.model.get('id_entregamaterial') ? this.model.get('id_entregamaterial') : null
        }
        $.ajax({
            dataType: 'json',
            type: 'post',
            url: '/item-de-material-presencial/entrega-table',
            data: parametros,
            beforeSend: function () {
                loading();
            },
            success: function (data) {
                if (data) {
                    $.pnotify({
                        title: 'Sucesso!',
                        text: 'Material entregue para o aluno ' + componenteAutoComplete.getFixModel().st_nomecompleto + '.',
                        type: 'success'
                    });
                    that.model.set({'id_entregamaterial': data.id_entregamaterial});
                    that.$el.find('.acao-modal').html('<button id="entregar-modal" title="Item Entregue" class="btn btn-info btn-small acao-modal " disabled data-toggle="tooltip">' +
                    '<i class="icon-arrow-right icon-white"></i>' +
                    '</button>');
                    thatLayout.pesquisarItem();
                }
            },
            error: function () {
                $.pnotify({
                    title: 'Erro!',
                    text: 'Material não entregue, entre em contato com o administrador do sistema!',
                    type: 'warning'
                });
            },
            complete: function () {
                loaded();
            }
        })
    }
});


var ModalItemView = Marionette.CompositeView.extend({
    template: '#itensModal',
    onRender: function () {
        return this;
    },
    events: {}

});

G2S.show(new LayoutEntregaMaterial());
/**
 * Created by User on 27/03/2015.
 */
/*********************************************
 ************** COLLECTION ******************
 *********************************************/


modelo = new ItemDeMaterial();
modeloPesquisa = null;
thatLayout = '';

/**
 * VARIAVEIS
 */


var EntidadeCollection = Backbone.Collection.extend({
    url: '/entidade/retornar-entidade-recursiva-id',
    model: VwEntidadeRecursivaId,
    initialize: function () {
        //this.fetch()
    }
});

var SituacaoCollection = Backbone.Collection.extend({
    url: '/api/situacao/?st_tabela=tb_itemdematerial',
    model: Situacao,
    initialize: function () {
        //this.fetch()
    }
});

var TurmaCollection = Backbone.Collection.extend({
    model: Turma
});

var DisciplinaCollection = Backbone.Collection.extend({
    model: Disciplina
})

var ProfessorCollection = Backbone.Collection.extend({
    model: VwProfessorDisciplina
})

var EncontroCollection = Backbone.Collection.extend({
    model: ItemGradeHoraria
})

var TipoCollection = Backbone.Collection.extend({
    model: TipodeMaterial
})

var PesquisaCollection = Backbone.Model.extend({
    defaults: {
        id_itemdematerial: '',
        st_itemdematerial: '',
        id_unidade: '',
        st_unidade: '',
        id_turma: '',
        st_turma: '',
        id_disciplina: '',
        st_disciplina: '',
        nu_encontro: '',
        id_professor: '',
        st_professor: '',
        id_tipo: '',
        st_tipo: '',
        id_situacao: '',
        st_situacao: '',
        nu_valor: '',
        nu_paginas: '',
        id_upload: '',
        nu_qtdestoque:''
    }
})
/*********************************************
 ************** LAYOUT ******************
 *********************************************/

var LayoutItemDeMaterialPresencial = Marionette.LayoutView.extend({
    template: '#item-de-material-presencial-layout',
    teagName: 'div',
    className: 'container-fluid',
    regions: {
        unidade: '#unidade-wrapper',
        turma: '#turma-wrapper',
        disciplina: '#disciplina-wrapper',
        professor: '#professor-wrapper',
        encontro: '#encontro-wrapper',
        titulo: '#titulo-wrapper',
        tipo: '#tipo-wrapper',
        situacao: '#situacao-wrapper',
        valor: '#valor-wrapper',
        paginas: '#paginas-wrapper',
        arquivo: '#arquivo-wrapper',
        itens: '#itens-wrapper'
    },

    initialize: function () {
        thatLayout = this;
        this.carregaUnidade();
        this.carregaComboSituacao();
    },
    onShow: function () {
        thatLayout = this;
        this.formatacampos();
    },
    events: {
        'click #btn_adicionar': 'salvarItem',
        'click #btn_cancelar': 'cancelarEdicao',
        'click #btn_salvar': 'salvarItem',
        'keydown #nu_paginas': 'somenteNumeros',
        'keydown #nu_qtdestoque': 'somenteNumeros',
        'click #btn_limpar': 'limparFormulario',
        'click #btn_pesquisar': 'pesquisarItem',
        'change #id_unidade': 'carregaTurma',
        'change #id_turma': 'carregaDisciplina',
        'change #id_disciplina': 'carregaProfessor',
        'change #id_professor': 'changeProfessor',
        'change #id_tipo': 'changeTipo',
        'change #id_encontro': 'changeEncontro',
        'change #id_situacao': 'changeSituacao',
        'keyup #st_titulo': 'changeTitulo'
    },
    ui: {
        'id_unidade': '#id_unidade',
        'id_turma': '#id_turma',
        'id_disciplina': '#id_disciplina',
        'id_professor': '#id_professor',
        'id_encontro': '#id_encontro',
        'st_titulo': '#st_titulo',
        'id_tipomaterial': '#id_tipo',
        'id_situacao': '#id_situacao',
        'nu_valor': '#nu_valor',
        'nu_paginas': '#nu_paginas',
        'arquivo': '#arquivo-item-material',
        'bl_portal': '#bl_portal',
        'form': '#form-arquivo',
        'st_upload': '#st_upload',
        'nu_qtdestoque': '#nu_qtdestoque'
    },
    /**
     * Envia a modelo com os dados do formulário, juntamente com o arquivo de material
     * @param e
     */
    salvarItem: function (e) {
        that = this;
        e.preventDefault();
        if (this.valida()) {
            fileObject = that.ui.arquivo[0].files[0];
            modelo.fileAttribute = 'attachment';
            modelo.url = '/api/item-de-material-presencial';
            modelo.set({
                'id_entidade': that.ui.id_unidade.val(),
                'id_turma': that.ui.id_turma.val(),
                'id_disciplina': that.ui.id_disciplina.val(),
                'id_professor': that.ui.id_professor.val(),
                'nu_encontro': that.ui.id_encontro.val(),
                'id_tipomaterial': that.ui.id_tipomaterial.val(),
                'st_nomematerial': that.ui.st_titulo.val(),
                'id_situacao': that.ui.id_situacao.val(),
                'nu_valor': that.ui.nu_valor.val() ? formatValueToReal(that.ui.nu_valor.val()) : 0.00,
                'nu_paginas': that.ui.nu_paginas.val() ? that.ui.nu_paginas.val() : 0,
                'id_usuariocadastro': G2S.loggedUser.get('id_usuario'),
                'attachment': fileObject ? fileObject : null,
                'bl_ativo': true,
                'bl_portal': that.ui.bl_portal.is(':checked'),
                'nu_qtdestoque': that.ui.nu_qtdestoque.val()
            });
            modelo.save(null, {
                beforeSend: function(){
                    loading();
                },
                success: function () {
                    //verifica se foi salvo um arquivo, se sim mostra o link para download e visualização do material.
                    if (modelo.get('st_upload') != null) {
                        thatLayout.ui.st_upload.prop('href', '/upload/materiaispresenciais/' + modelo.get('st_upload'))
                        $('#divUpload').show();
                    } else {
                        $('#divUpload').hide();
                    }
                    thatLayout.pesquisarItem(e);
                    $.pnotify({
                        title: 'Sucesso',
                        text: 'Cadastro efetuado com sucesso!',
                        type: 'success'
                    });

                    thatLayout.limparFormulario();

                    $('#btn_salvar').hide();
                    $('#btn_cancelar').hide();
                    $('#btn_pesquisar').show();
                    $('#btn_limpar').show();
                    $('#btn_adicionar').show();

                },
                error: function () {
                    $.pnotify({
                        title: 'Erro',
                        text: 'Error ao cadastrar material!',
                        type: 'error'
                    });
                },
                complete: function(){
                    loaded();
                }
            });

        }
    },
    /**
     * Executa a pesquisa de acordo com os parametros passados no formulário
     * @param e
     */
    pesquisarItem: function (e) {
        that = this;
        e.preventDefault();
        this.itens.show(new ViewItens());
        var HtmlBotoesCell = Backgrid.Cell.extend({
            className: 'span5',
            render: function () {
                var elTemp = _.template('<div class="content" style="text-align: center;"> ' +
                    '<button id="btn_gerarcodigo" class="btn btn-small btn-inverse btn-custom "><i class="icon icon-white icon-barcode"> </i>Gerar Código</button> ' +
                    '<button id="btn_editar" class="btn btn-small btn-custom"><i class="icon icon-white icon-pencil"></i> Editar</button> ' +
                    '<button id="btn_remover" class="btn btn-small btn-danger"><i class="icon icon-white icon-trash"></i> Remover</button>' +
                    '</div>'
                );
                this.$el.html(elTemp);
                return this;
            }
        });

        var TurmaCell = Backgrid.Cell.extend({
            render: function(){
                var elTemp = _.each(collectionPesquisa)
            }
        })

        var columns = [{
            name: "id_itemdematerial",
            label: "ID",
            editable: false,
            cell: "string"
        }, {
            name: "st_itemdematerial",
            label: 'Nome',
            editable: false,
            cell: "string"
        }, {
            name: "nu_qtdestoque",
            label: "Estoque",
            editable: false,
            cell: "string"
        }, {
            name: "st_unidade",
            label: "Unidade",
            editable: false,
            cell: "string"
        }, {
            name: "turma",
            label: "Turma",
            editable: false,
            cell: Backgrid.Cell.extend({
                render: function () {
                    var turmas = '<ul>';
                    _.each(this.model.get('turma'), function(val,index){
                        turmas += '<li>'+val.st_turma+'</li>';
                    })
                    turmas +='</ul>';
                    this.$el.html(turmas);
                    return this;
                }
            })
        }, {
            name: "st_disciplina",
            label: "Disciplina",
            editable: false,
            cell: "string"
        }, {
            name: "nu_encontro",
            label: "Encontro",
            editable: false,
            cell: "string"
        }, {
            name: "acao",
            label: "Ações",
            cell: HtmlBotoesCell,
            editable: false
        }];

        parametros = {
            'id_entidade': that.ui.id_unidade.val() ? that.ui.id_unidade.val() : null,
            'id_turma': that.ui.id_turma.val() ? that.ui.id_turma.val() : null,
            'id_disciplina': that.ui.id_disciplina.val() ? that.ui.id_disciplina.val() : null,
            'id_professor': that.ui.id_professor.val() ? that.ui.id_professor.val() : null,
            'nu_encontro': that.ui.id_encontro.val() ? that.ui.id_encontro.val() : null,
            'id_tipodematerial': that.ui.id_tipomaterial.val() ? that.ui.id_tipomaterial.val() : null,
            'st_itemdematerial': that.ui.st_titulo.val() != '' ? that.ui.st_titulo.val() : null,
            'id_situacao': that.ui.id_situacao.val() ? that.ui.id_situacao.val() : null,
            'nu_valor': that.ui.nu_valor.val() != '' ? formatValueToReal(that.ui.nu_valor.val()) : null,
            'nu_qtdepaginas': that.ui.nu_paginas.val() != '' ? that.ui.nu_paginas.val() : null,
            'bl_portal': that.ui.bl_portal.is(':checked'),
            'bl_ativo': true
        }

        var PageableItens = Backbone.PageableCollection.extend({
            model: PesquisaCollection,
            url: '/item-de-material-presencial/retorna-pesquisa',
            state: {
                pageSize: 10
            },
            mode: "client",
            parse: function(response){

                var current = null;
                var array = [];
                var pos = null;
                _.each(response, function(val, key){
                    if (val.id_itemdematerial == current) {
                        if (array[pos]) {
                            array[pos].turma.push({id_turma: val.id_turma, st_turma: val.st_turma});
                        }
                    } else {
                        if(pos == null){
                            pos = 0;
                        }else{
                            pos++;
                        }
                        array.push({
                            id_itemdematerial:val.id_itemdematerial,
                            st_itemdematerial:val.st_itemdematerial,
                            id_unidade:val.id_unidade,
                            st_unidade:val.st_unidade,
                            turma:[{id_turma: val.id_turma, st_turma: val.st_turma}],
                            id_disciplina: val.id_disciplina,
                            st_disciplina: val.st_disciplina,
                            nu_encontro: val.nu_encontro,
                            id_professor: val.id_professor,
                            st_professor: val.st_professor,
                            id_tipo: val.id_tipo,
                            st_tipo: val.st_tipo,
                            id_situacao: val.id_situacao,
                            st_situacao: val.st_situacao,
                            nu_valor: val.nu_valor,
                            nu_paginas: val.nu_paginas,
                            id_upload: val.id_upload,
                            st_upload: val.st_upload,
                            bl_portal: val.bl_portal,
                            nu_qtdestoque: val.nu_qtdestoque

                        });
                        current = val.id_itemdematerial;
                    }
                });
                return array;
            }
        });

        var RowGrid = Backgrid.Row.extend({
            initialize: function (options) {
                RowGrid.__super__.initialize.apply(this, arguments);
                this.$el.css('cursor', 'pointer');
            },
            editar: function () {
                /**
                 *metodo para carregar a modelo de dados da pesquisa para o formulário, seta o id do material, os dados
                 * dos formulários e dispara um evento para preenchimento dos combos.
                 */

                $('#btn_salvar').show();
                $('#btn_cancelar').show();
                $('#btn_pesquisar').hide();
                $('#btn_limpar').hide();
                $('#btn_adicionar').hide();


                var that = this;
                thatLayout.limparFormulario();
                modeloPesquisa = null;//zero a modelo, para garantir.
                modeloPesquisa = this.model;//seto a modeloPesquisa com os dados da row
                modelo.set({
                    'id_material': this.model.get('id_itemdematerial')//envio para a modelo o id do material
                });
                /**
                 * preencho todos os campos
                 */

                var array = [];

                _.each(modeloPesquisa.get('turma'), function(val, index){
                    array.push(val.id_turma);
                })

                modeloPesquisa.set({id_turma: array});

                thatLayout.ui.id_unidade.val(modeloPesquisa.get('id_unidade'));
                thatLayout.ui.id_situacao.val(modeloPesquisa.get('id_situacao'));
                thatLayout.ui.nu_valor.val(formatValueToShow(modeloPesquisa.get('nu_valor')));
                thatLayout.ui.nu_paginas.val(modeloPesquisa.get('nu_paginas'));
                thatLayout.ui.st_titulo.val(modeloPesquisa.get('st_itemdematerial'));
                thatLayout.ui.id_encontro.val(modeloPesquisa.get('nu_encontro'));
                thatLayout.ui.bl_portal.prop('checked', modeloPesquisa.get('bl_portal'));
                thatLayout.ui.nu_qtdestoque.val(modeloPesquisa.get('nu_qtdestoque'));
                thatLayout.ui.id_unidade.trigger('change');
                /**
                 * verifica se tem arquivo, se sim habilita o link
                 */
                if (modeloPesquisa.get('st_upload') != null) {
                    thatLayout.ui.st_upload.prop('href', '/upload/materiaispresenciais/' + modeloPesquisa.get('st_upload'))
                    $('#divUpload').show();
                } else {
                    $('#divUpload').hide();
                }

            },
            /**
             * Remove um registro logicamente
             */
            remover: function () {
                var that = this;
                modelo = new ItemDeMaterialPresencial();
                modelo.set({
                    'id': this.model.get('id_itemdematerial')
                });
                bootbox.confirm("Este material será removido do registro! Deseja continuar?", function (result) {
                    if (result) {
                        modelo.destroy({
                            success: function () {
                                $.pnotify({
                                    title: "Sucesso!",
                                    text: "Registro removido com sucesso!",
                                    type: "success"
                                });

                                that.$el.remove();
                            }
                        })
                        thatLayout.limparFormulario();
                    }
                })
            },
            /**
             * Gera um codigo de barras com o id do material
             */
            gerarcodigo: function () {
                var url = '/item-de-material-presencial/gerar-Barcode/?id=' + this.model.get('id_itemdematerial');
                window.open(url, '_blank');
            },
            events: {
                "click #btn_editar": "editar",
                "click #btn_remover": "remover",
                "click #btn_gerarcodigo": "gerarcodigo"
            }
        });

        var collectionPesquisa = new PageableItens()

        var pageableGridItens = new Backgrid.Grid({
            className: 'backgrid table table-bordered',
            columns: columns,
            collection: collectionPesquisa,
            row: RowGrid,
            emptyText: "Nenhum registro encontrado"
        });

        var $elemento = $('#tabela-itens-wrapper');
        $elemento.empty();
        $elemento.append(pageableGridItens.render().el);

        var paginator = new Backgrid.Extension.Paginator({
            collection: collectionPesquisa
        });

        $elemento.append(paginator.render().el);

        collectionPesquisa.fetch({data: parametros});
    },
    cancelarEdicao: function(){
      var that = this;
        that.limparFormulario();

        $('#btn_salvar').hide();
        $('#btn_cancelar').hide();
        $('#btn_pesquisar').show();
        $('#btn_limpar').show();
        $('#btn_adicionar').show();

    },
    limparFormulario: function () {
        that = this;
        modelo = new ItemDeMaterialPresencial();
        modeloPesquisa = null;
        that.ui.id_turma.select2('destroy');
        that.ui.id_turma.removeAttr("multiple");
        that.ui.id_unidade.prop("selectedIndex", '');
        that.ui.id_turma.prop("selectedIndex", '');
        that.ui.id_disciplina.prop("selectedIndex", '');
        that.ui.id_professor.prop("selectedIndex", '');
        that.ui.id_encontro.val('');
        //that.ui.id_encontro.prop("selectedIndex", '');
        that.ui.id_tipomaterial.prop("selectedIndex", '');
        that.ui.id_situacao.prop("selectedIndex", '');
        that.ui.bl_portal.prop("checked", false);
        that.ui.nu_valor.val('');
        that.ui.nu_paginas.val('');
        that.ui.st_titulo.val('');
        that.ui.arquivo.val('');
        that.ui.nu_qtdestoque.val('');
        document.getElementById('id_professor').disabled = true;
        document.getElementById('id_turma').disabled = true;
        document.getElementById('id_disciplina').disabled = true;
        //document.getElementById('id_encontro').disabled = true;
        document.getElementById('id_tipo').disabled = true;
        if ($('#id_unidade'))$('#divUnidade').removeClass('control-group error');
        if ($('#id_turma'))$('#divTurma').removeClass('control-group error');
        if ($('#id_disciplina'))$('#divDisciplina').removeClass('control-group error');
        if ($('#id_professor'))$('#divProfessor').removeClass('control-group error');
        //if ($('#id_encontro'))$('#divEncontro').removeClass('control-group error');
        if ($('#id_titulo'))$('#divTitulo').removeClass('control-group error');
        if ($('#id_tipo'))$('#divTipo').removeClass('control-group error');
        if ($('#id_situacao'))$('#divSituacao').removeClass('control-group error');
        $('#divUpload').hide();


    },
    /**
     * Valida os campos obrigatórios do formulário
     * @returns {boolean|*}
     */
    valida: function () {
        flag = true;

        if (!$('#id_unidade').val()) {
            flag = false;
            $('#divUnidade').addClass('control-group error');
        } else {
            $('#divUnidade').removeClass('control-group error');
        }
        if (!$('#id_turma').val()) {
            flag = false;
            $('#divTurma').addClass('control-group error');
        } else {
            $('#divTurma').removeClass('control-group error');
        }
        if (!$('#id_disciplina').val()) {
            flag = false;
            $('#divDisciplina').addClass('control-group error');
        } else {
            $('#divDisciplina').removeClass('control-group error');
        }
        if (!$('#id_professor').val()) {
            flag = false;
            $('#divProfessor').addClass('control-group error');
        } else {
            $('#divProfessor').removeClass('control-group error');
        }
        //if (!$('#id_encontro').val()) {
        if ($('#id_encontro').val()=='' || $('#id_encontro').val()<1 || $('#id_encontro').val()>25) {
            flag = false;
            $('#divEncontro').addClass('control-group error');
        } else {
            $('#divEncontro').removeClass('control-group error');
        }
        if (!$('#st_titulo').val()) {
            flag = false;
            $('#divTitulo').addClass('control-group error');
        } else {
            $('#divTitulo').removeClass('control-group error');
        }
        if (!$('#id_tipo').val()) {
            flag = false;
            $('#divTipo').addClass('control-group error');
        } else {
            $('#divTipo').removeClass('control-group error');
        }
        if (!$('#id_situacao').val()) {
            flag = false;
            $('#divSituacao').addClass('control-group error');
        } else {
            $('#divSituacao').removeClass('control-group error');
        }

        if (!flag) {
            $.pnotify({
                title: 'Alerta',
                text: 'Verifique os campos em vermelho, eles são obrigatórios!',
                type: 'danger'
            });
        }

        return flag;
    },
    /**
     * Garante que somente numeros serão digitados nos campos necessários
     * @param e
     * @returns {boolean}
     */
    somenteNumeros: function (e) {
        //teclas adicionais permitidas (tab,delete,backspace,setas direita e esquerda)
        keyCodesPermitidos = new Array(8, 9, 37, 39, 46);
        //numeros e 0 a 9 do teclado alfanumerico
        for (x = 48; x <= 57; x++) {
            keyCodesPermitidos.push(x);
        }

        //numeros e 0 a 9 do teclado numerico
        for (x = 96; x <= 105; x++) {
            keyCodesPermitidos.push(x);
        }

        //Pega a tecla digitada
        keyCode = e.which;
        //Verifica se a tecla digitada é permitida
        if ($.inArray(keyCode, keyCodesPermitidos) != -1) {
            return true;
        }
        return false;
    },
    formatacampos: function () {
        $('#nu_valor').maskMoney({precision: 2, decimal: ",", thousands: "."});
    },
    carregaUnidade: function () {
        loading();
        that = this;
        var collectionEntidade = new EntidadeCollection();
        collectionEntidade.fetch({
            success: function () {
                var view = new SelectView({
                    el: that.$el.find("select[name='id_unidade']"),        // Elemento da DOM
                    collection: collectionEntidade,      // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_nomeentidade', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_entidade', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: null       // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();
                loaded();
            }
        });

    },
    /**
     * carrega o campo de turma
     */
    carregaTurma: function () {
        that = this;

        if (!that.ui.id_unidade.val()) {
            /**
             * Apaga os valores e desabilita todos os campos caso nenhuma unidade seja selecionada
             */
            that.ui.id_turma.empty();
            that.ui.id_turma.removeAttr("multiple");
            that.ui.id_turma.select2('destroy');
            that.ui.id_turma.prepend($('<option selected disabled>Selecione</option>'));

            document.getElementById('id_turma').disabled = true;
            that.ui.id_disciplina.empty();
            that.ui.id_disciplina.prepend($('<option selected disabled>Selecione</option>'));

            document.getElementById('id_disciplina').disabled = true;
            that.ui.id_professor.empty();
            that.ui.id_professor.prepend($('<option selected disabled>Selecione</option>'));

            //that.ui.id_encontro.empty();
            //that.ui.id_encontro.prepend($('<option selected disabled>Selecione</option>'));
            //document.getElementById('id_encontro').disabled = true;

            document.getElementById('id_professor').disabled = true;
        } else {
            /**
             * Carrega o combo de turma de acordo com a unidade
             * @type {TurmaCollection}
             */
            var CollectionTurma = new TurmaCollection();
            CollectionTurma.url = '/item-de-material-presencial/retorna-turma/id/' + that.ui.id_unidade.val();
            CollectionTurma.fetch({
                beforeSend: function () {
                    loading();
                    $('#divUnidade').removeClass('control-group error');
                },
                success: function () {
                    //Apago e desabilito os campos sequentes a turma para que sejam recarregados após uma seleção de turma
                    that.ui.id_turma.empty();
                    that.ui.id_turma.prepend($('<option selected disabled>Selecione</option>'));
                    document.getElementById('id_turma').disabled = true;

                    that.ui.id_disciplina.empty();
                    that.ui.id_disciplina.prepend($('<option selected disabled>Selecione</option>'));
                    document.getElementById('id_disciplina').disabled = true;

                    that.ui.id_professor.empty();
                    that.ui.id_professor.prepend($('<option selected disabled>Selecione</option>'));
                    document.getElementById('id_professor').disabled = true;

                    //that.ui.id_encontro.empty();
                    //that.ui.id_encontro.prepend($('<option selected disabled>Selecione</option>'));
                    //document.getElementById('id_encontro').disabled = true;


                    if (!CollectionTurma.isEmpty()) {
                        that.ui.id_turma.removeAttr('disabled');
                        that.ui.id_turma.attr("multiple", true);
                    }
                    var view = new SelectView({
                        el: that.ui.id_turma,         // Elemento da DOM
                        collection: CollectionTurma,      // Instancia da collection a ser utilizada
                        childViewOptions: {
                            optionLabel: 'st_turma', // Propriedade da Model que será utilizada como label do select
                            optionValue: 'id_turma', // Propriedade da Model que será utilizada como id do option do select
                            optionSelected: null//that.model.get('id_situacao')       // ID da option que receberá o atributo "selected"
                        }
                    });
                    view.render();
                },
                complete: function () {
                    loaded();
                    /**
                     * Verifica se é pesquisa, se sim posiciona o select na posição da pesquisa
                     */
                    if (modeloPesquisa) {
                        var turma = modeloPesquisa.get('id_turma');
                        that.ui.id_turma.select2({placeholder: "Selecione"});
                        that.ui.id_turma.attr("multiple", true);
                        that.ui.id_turma.select2("val",turma);
                        that.carregaDisciplina(turma);
                        //modeloPesquisa.set({'id_turma': ''});
                    }
                    ;
                    that.ui.id_turma.select2({placeholder: "Selecione"});
                    that.ui.id_turma.attr("multiple", true);
                    that.carregaTipoMateriais();
                }
            });
        }


    },
    carregaDisciplina: function () {
        that = this;
        if (!that.ui.id_turma.val()) {
            /**
             * Apaga os valores e desabilita todos os campos caso nenhuma turma seja selecionada
             */

            that.ui.id_disciplina.empty();
            that.ui.id_disciplina.prepend($('<option selected disabled>Selecione</option>'))
            document.getElementById('id_disciplina').disabled = true;

            that.ui.id_professor.empty();
            that.ui.id_professor.prepend($('<option selected disabled>Selecione</option>'));
            document.getElementById('id_professor').disabled = true;

            //that.ui.id_encontro.empty();
            //that.ui.id_encontro.prepend($('<option selected disabled>Selecione</option>'));
            //document.getElementById('id_encontro').disabled = true;
        } else {
            if ($('#id_turma'))$('#divTurma').removeClass('control-group error');
            var Collection = new DisciplinaCollection();
            Collection.url = '/item-de-material-presencial/retorna-disciplina/';
            Collection.fetch({
                data: {id: that.ui.id_turma.val()},
                beforeSend: function () {
                    loading()
                },
                success: function () {
                    that.ui.id_disciplina.empty();
                    that.ui.id_disciplina.prepend($('<option selected value="" >Selecione</option>'));
                    document.getElementById('id_disciplina').disabled = true;

                    that.ui.id_professor.empty();
                    that.ui.id_professor.prepend($('<option selected value="">Selecione</option>'));
                    document.getElementById('id_professor').disabled = true;

                    if (!Collection.isEmpty()) {
                        $('#id_disciplina').removeAttr('disabled')
                    }

                    var view = new SelectView({
                        el: that.ui.id_disciplina,         // Elemento da DOM
                        collection: Collection,      // Instancia da collection a ser utilizada
                        childViewOptions: {
                            optionLabel: 'st_disciplina', // Propriedade da Model que será utilizada como label do select
                            optionValue: 'id_disciplina', // Propriedade da Model que será utilizada como id do option do select
                            optionSelected: null//that.model.get('id_situacao')       // ID da option que receberá o atributo "selected"
                        }
                    });
                    view.render();
                },
                complete: function () {
                    loaded();
                    if (modeloPesquisa && !Collection.isEmpty()) {
                        that.ui.id_disciplina.val(modeloPesquisa.get('id_disciplina'))
                        that.carregaProfessor(modeloPesquisa.get('id_disciplina'));
                        //modeloPesquisa.set({'id_disciplina': ''});
                    }
                    ;
                }
            });
        }
    },
    carregaProfessor: function () {
        that = this;
        if ($('#id_disciplina'))$('#divDisciplina').removeClass('control-group error');
        var Collection = new ProfessorCollection();
        Collection.url = '/item-de-material-presencial/retorna-professor/id_disciplina/' + that.ui.id_disciplina.val();
        Collection.fetch({
            beforeSend: function () {
                loading()
            },
            success: function () {
                that.ui.id_professor.empty();
                that.ui.id_professor.prepend($('<option selected value="">Selecione</option>'));
                document.getElementById('id_professor').disabled = true;

                if (!Collection.isEmpty()) {
                    that.ui.id_professor.removeAttr('disabled')
                }
                var view = new SelectView({
                    el: that.ui.id_professor,         // Elemento da DOM
                    collection: Collection,      // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_nomecompleto', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_usuario', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: null//that.model.get('id_situacao')       // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();
            },
            complete: function () {
                loaded();
                that.carregaEncontro(that.ui.id_disciplina.val());
                if (modeloPesquisa && !Collection.isEmpty()) {
                    that.ui.id_professor.val(modeloPesquisa.get('id_professor'));
                    //modeloPesquisa.set({'id_professor': ''});
                }
                ;
            }
        });
    },
    carregaEncontro: function () {
        that = this;
        if ($('#id_encontro'))$('#divEncontro').removeClass('control-group error');
        var Collection = new EncontroCollection();
        Collection.url = '/item-de-material-presencial/retorna-encontro/id_disciplina/' + that.ui.id_disciplina.val() + '/id_entidade/' + that.ui.id_unidade.val();
        Collection.fetch({
            beforeSend: function () {
                loading()
            },
            success: function () {
                //that.ui.id_encontro.empty();
                //that.ui.id_encontro.prepend($('<option selected value="">Selecione</option>'));
                //document.getElementById('id_encontro').disabled = true;

                //if (!Collection.isEmpty()) {
                    //that.ui.id_encontro.removeAttr('disabled')
                //}
                //var view = new SelectView({
                //    el: that.ui.id_encontro,         // Elemento da DOM
                //    collection: Collection,      // Instancia da collection a ser utilizada
                //    childViewOptions: {
                //        optionLabel: 'nu_encontro', // Propriedade da Model que será utilizada como label do select
                //        optionValue: 'nu_encontro', // Propriedade da Model que será utilizada como id do option do select
                //        optionSelected: null//that.model.get('id_situacao')       // ID da option que receberá o atributo "selected"
                //    }
                //});
                //view.render();
            },
            complete: function () {
                loaded();
                if (modeloPesquisa && !Collection.isEmpty()) {
                    that.ui.id_encontro.val(modeloPesquisa.get('nu_encontro'));
                    //modeloPesquisa.set({'id_encontro': ''});
                }
                ;
            }
        });
    },
    carregaTipoMateriais: function () {

        loading();
        var that = this;
        var collectionTipo = new TipoCollection();
        collectionTipo.url = '/item-de-material-presencial/retorna-tipo-de-material/id/' + that.ui.id_unidade.val();
        collectionTipo.fetch({
            success: function () {


                that.ui.id_tipomaterial.empty();
                that.ui.id_tipomaterial.prepend($('<option selected value="">Selecione</option>'));
                document.getElementById('id_tipo').disabled = true;

                if (!collectionTipo.isEmpty()) {
                    that.ui.id_tipomaterial.removeAttr('disabled')
                }
                var view = new SelectView({
                    el: that.ui.id_tipomaterial,         // Elemento da DOM
                    collection: collectionTipo,      // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_tipodematerial', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_tipodematerial', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: null//that.model.get('id_situacao')       // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();
                loaded();
            },
            complete: function () {
                if (modeloPesquisa) {
                    that.ui.id_tipomaterial.val(modeloPesquisa.get('id_tipo'));
                    //modeloPesquisa.set({'id_tipo': ''});
                }
                ;
            }
        });

    },
    carregaComboSituacao: function () {
        loading();
        var that = this;
        var collectionSituacao = new SituacaoCollection();
        collectionSituacao.fetch({
            success: function () {
                var view = new SelectView({
                    el: that.$el.find("select[name='id_situacao']"),         // Elemento da DOM
                    collection: collectionSituacao,      // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_situacao', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_situacao', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: null//that.model.get('id_situacao')       // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();
            },
            complete: function () {
                loaded();

            }
        });

    },
    /**
     * Retira os alertas de campos vazios.
     */
    changeProfessor: function () {
        if ($('#id_professor'))$('#divProfessor').removeClass('control-group error');
    },
    changeSituacao: function () {
        $('#divSituacao').removeClass('control-group error');
    },
    changeTitulo: function () {
        if ($('#st_titulo').val().length >= 0) {
            $('#divTitulo').removeClass('control-group error');
        } else {
            $('#divTitulo').addClass('control-group error');
        }
    },
    changeEncontro: function () {
        if ($('#id_encontro'))$('#divEncontro').removeClass('control-group error');
    },
    changeTipo: function () {
        if ($('#id_tipo'))$('#divTipo').removeClass('control-group error');
    }

})

var ViewItens = Marionette.CompositeView.extend({
    template: '#itens-item-material-template',
    tagName: 'div'
})

G2S.show(new LayoutItemDeMaterialPresencial());
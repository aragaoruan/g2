// Include funcao ComboBoxView
var UnidadeModel = Backbone.Model.extend({
    defaults: {
        id_entidade: null,
        id_situacao: null,
        st_cnpj: '',
        nu_inscricaoanual: 0,
        st_nomeentidade: '',
        st_razaosocial: '',
        st_urlimglogo: '',
        bl_ativo: false,
        bl_acessasistema: false,
        id_configuracaoentidade: null,
        id_entidadeclasse: null,
        id_entidadepai: null,
        st_entidadeclasse: '',
        nu_cnpj: 0,
        nu_inscricaoestadual: 0
    },
    idAttribute: "id_entidade"
});

var loadedCount;

function verificarLoaded() {
    if (loadedCount >= 1) {
        loaded();
    }
}

ComboboxView({
    el: $('[name="unidade"]'),
    model: UnidadeModel,
    url: "/funcionalidade/get-entidade-funcionalidade",
    optionLabel: "st_nomeentidade"
}, function (elem) {
    $('#st_nomeentidade').focus();

    elem.on('change', function () {
        loadedCount = 0;

        loading();

        var value = $(this).val();

        if (!value) {
            $('.actions').hide();
            $('.visualizacao-body, .ativacao-body')
                .html('')
                .closest('.well')
                .hide();
            return;
        } else {
            $('.actions').show();
            $('.visualizacao-body, .ativacao-body, .actions')
                .closest('.well')
                .show();
        }

        // JSTREE VISUALIZAÇÃO
        $('.visualizacao-body').jstree({
            html_data: {
                ajax: {
                    url: '/funcionalidade/get-arvore-visualizacao',
                    data: function () {
                        return {id_entidade: value};
                    }
                }
            },
            'plugins': ['themes', 'html_data', 'checkbox', 'sort', 'ui']
        }).bind('loaded.jstree', function (event, data) {
            var jsTreeElem = $(this);
            var value = $('[name="unidade"]').val();

            $.get('/funcionalidade/get-arvore-visualizacao-selecionados?id_entidade=' + value, function (response) {

                if (response) {
                    for (var i in response) {
                        $.jstree._reference('.visualizacao-body').check_node('li[data-id="' + response[i] + '"]');
                        //jsTreeElem.jstree('check_node', 'li[data-id="' + response[i] + '"]');
                    }
                    jsTreeElem.jstree('close_all', -1);
                }

            });
        });

        // JSTREE ATIVAÇÃO
        $('.ativacao-body').jstree({
            html_data: {
                ajax: {
                    url: '/funcionalidade/get-arvore-ativacao',
                    data: function () {
                        return {id_entidade: value};
                    }
                }
            },
            'plugins': ['themes', 'html_data', 'checkbox', 'sort', 'ui']
        }).bind('loaded.jstree', function (event, data) {
            var jsTreeElem = $(this);
            var value = $('[name="unidade"]').val();

            $.get('/funcionalidade/get-arvore-ativacao-selecionados?id_entidade=' + value, function (response) {
                loadedCount++;

                if (response) {
                    for (var i in response) {
                        $.jstree._reference('.ativacao-body').check_node('li[data-id="' + response[i] + '"]');
                        //jsTreeElem.jstree('check_node', 'li[data-id="' + response[i] + '"]');
                    }
                    jsTreeElem.jstree('close_all', -1);
                }

                verificarLoaded();
            });
        });
    });
});

// RENDERIZAR TUDO
var GlobalModel = Backbone.Model.extend({
    defaults: {
        visualizacao: [],
        ativacao: []
    },
    url: '/api/funcionalidade/'
});

var GlobalView = Marionette.ItemView.extend({
    model: new GlobalModel(),
    el: $('#global-content'),
    events: {
        'submit form': 'actionSave',
        'click .select-all': 'jsTreeSelectChildren'
    },
    jsTreeSelectChildren: function (e) {
        e.preventDefault();

        var that = this;
        var elem = $(e.target).closest('li');

        var reference = elem.closest('.ativacao-body').length ? '.ativacao-body' : '.visualizacao-body';
        var isChecked = $.jstree._reference(reference).is_checked(elem);

        if (isChecked) {
            $.jstree._reference(reference).uncheck_node(elem);
            elem.find('li').each(function () {
                $.jstree._reference(reference).uncheck_node($(this));
            });
        } else {
            $.jstree._reference(reference).check_node(elem);
            elem.find('li').each(function () {
                $.jstree._reference(reference).check_node($(this));
            });
        }
    },
    actionSave: function (e) {
        e.preventDefault();

        var that = this;
        var id_entidade = $('[name="unidade"]').val();

        // Buscar items VISUALIZAÇÃO
        var visualizacao = [];
        $('.visualizacao-body').jstree("get_checked", null, true).each(function () {
            var elem = $(this);
            var id = elem.attr('data-id');

            if (id && id !== '') {
                visualizacao.push({
                    id_funcionalidade: id,
                    id_situacao: elem.attr('data-id-situacao'),
                    bl_ativo: elem.attr('data-bl-ativo'),
                    id_entidade: id_entidade
                });
            }
        });

        // Buscar items ATIVAÇÃO
        var ativacao = [];
        $('.ativacao-body').jstree("get_checked", null, true).each(function () {
            var elem = $(this);
            var id = elem.attr('data-id');

            if (id && id !== '') {
                ativacao.push({
                    id_funcionalidade: id,
                    id_situacao: elem.attr('data-id-situacao'),
                    bl_ativo: elem.attr('data-bl-ativo'),
                    id_entidade: id_entidade
                });
            }
        });

        var values = {
            visualizacao: visualizacao,
            ativacao: ativacao
        };

        loading();

        that.model.save(values, {
            success: function () {
                $('[name="unidade"]').val('').trigger('change');
            },
            complete: function (response) {
                response = $.parseJSON(response.responseText);

                loaded();
                $.pnotify({
                    'type': response.type,
                    'title': response.title,
                    'text': response.text
                });
            }
        });

    }
});

var GlobalIni = new GlobalView();

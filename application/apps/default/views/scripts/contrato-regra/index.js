var GlobalModel = Backbone.Model.extend({
    idAttribute: 'id_contratoregra',
    defaults: {
        id_contratoregra: null,
        st_contratoregra: '',
        nu_contratoduracao: 0,
        id_projetocontratoduracaotipo: {
            id_projetocontratoduracaotipo: null
        },
        bl_mesesmensalidade: 0,
        nu_mesesmensalidade: 0,
        nu_tempoextensao: 0,
        bl_renovarcontrato: 1,
        nu_mesesmulta: 0,
        nu_contratomultavalor: 0.00,
        bl_proporcaomes: 0,
        id_contratomodelo: null,
        id_extensaomodelo: null,
        id_aditivotransferencia: null,
        id_entidade: null,
        id_usuario: null,
        dt_cadastro: null,
        id_modelocarteirinha: {
            id_modelocarteirinha: null
        },
        bl_ativo: 1,
        id_tiporegracontrato: null
    },
    url: function () {
        return '/api/contrato-regra/' + (this.id || '');
    }
});

var GlobalView = Marionette.ItemView.extend({
    template: '#global-template',
    tagName: 'div',
    model: new GlobalModel(),
    ui: {
        bl_mesesmensalidade: '[name="bl_mesesmensalidade"]',
        bl_proporcaomes: '[name="bl_proporcaomes"]',
        bl_renovarcontrato: '[name="bl_renovarcontrato"]',
        id_contratomodelo: '[name="id_contratomodelo"]',
        id_extensaomodelo: '[name="id_extensaomodelo"]',
        id_aditivotransferencia: '[name="id_aditivotransferencia"]',
        id_modelocarteirinha: '[name="id_modelocarteirinha"]',
        id_projetocontratoduracaotipo: '[name="id_projetocontratoduracaotipo"]',
        id_tiporegracontrato: '[name="id_tiporegracontrato"]',
        nu_contratoduracao: '[name="nu_contratoduracao"]',
        nu_contratomultavalor: '[name="nu_contratomultavalor"]',
        nu_mesesmensalidade: '[name="nu_mesesmensalidade"]',
        nu_mesesmulta: '[name="id_formapagamentoaplicacao"]',
        nu_tempoextensao: '[name="nu_tempoextensao"]',
        st_contratoregra: '[name="st_contratoregra"]'
    },
    onBeforeRender: function() {
        this.model.set('bl_mesesmensalidade', this.model.get('nu_mesesmensalidade') ? true : false);
    },
    onRender: function () {
        var that = this;

        loaded();

        // ComboBox Modelo Carteirinha
        ComboboxView({
            el: that.ui.id_modelocarteirinha,
            url: '/api/modelo-carteirinha-por-entidade',
            optionLabel: 'st_modelocarteirinha',
            optionSelectedId: that.model.get('id_modelocarteirinha') ? that.model.get('id_modelocarteirinha').id_modelocarteirinha : null,
            emptyOption: '[Selecione]',
            model: Backbone.Model.extend({
                idAttribute: 'id_modelocarteirinha',
                defaults: {
                    'id_modelocarteirinha': '',
                    'st_modelocarteirinha': ''
                }
            })
        });

        // ComboBox Tipo de Duração
        ComboboxView({
            el: that.ui.id_projetocontratoduracaotipo,
            url: '/api/projeto-contrato-duracao-tipo',
            optionLabel: 'st_projetocontratoduracaotipo',
            optionSelectedId: that.model.get('id_projetocontratoduracaotipo') ? that.model.get('id_projetocontratoduracaotipo').id_projetocontratoduracaotipo : null,
            emptyOption: '[Selecione]',
            model: Backbone.Model.extend({
                idAttribute: 'id_projetocontratoduracaotipo',
                defaults: {
                    'id_projetocontratoduracaotipo': '',
                    'st_projetocontratoduracaotipo': ''
                }
            })
        });

        // ComboBox Tipo de Regra de Contrato
        ComboboxView({
            el: that.ui.id_tiporegracontrato,
            url: '/api/tipo-regra-contrato',
            optionLabel: 'st_tiporegracontrato',
            optionSelectedId: that.model.get('id_tiporegracontrato') ? that.model.get('id_tiporegracontrato').id_tiporegracontrato : null,
            emptyOption: '[Selecione]',
            model: Backbone.Model.extend({
                idAttribute: 'id_tiporegracontrato',
                defaults: {
                    'id_tiporegracontrato': '',
                    'st_tiporegracontrato': ''
                }
            })
        });

        // Montar ComboBox Textos
        var TextoModel = Backbone.Model.extend({
            idAttribute: 'id_textosistema',
            defaults: {
                id_textosistema: '',
                st_textosistema: ''
            }
        });
        var Collection = Backbone.Collection.extend({
            model: TextoModel,
            url: '/api/vw-texto-sistema?id_textocategoria=2',
            parse: function (response) {
                return response.mensagem || response || [];
            }
        });

        var collectionTextos = new Collection();
        collectionTextos.fetch({
            success: function () {
                // Campo id_contratomodelo
                ComboboxView({
                    el: that.ui.id_contratomodelo,
                    collection: collectionTextos.toJSON(),
                    optionLabel: 'st_textosistema',
                    optionSelectedId: that.model.get('id_contratomodelo'),
                    emptyOption: '[Selecione]',
                    model: TextoModel
                });

                // Campo id_extensaomodelo
                ComboboxView({
                    el: that.ui.id_extensaomodelo,
                    collection: collectionTextos.toJSON(),
                    optionLabel: 'st_textosistema',
                    optionSelectedId: that.model.get('id_contratomodelo'),
                    emptyOption: '[Selecione]',
                    model: TextoModel
                });

                // Campo id_aditivotransferencia
                ComboboxView({
                    el: that.ui.id_aditivotransferencia,
                    collection: collectionTextos.toJSON(),
                    optionLabel: 'st_textosistema',
                    optionSelectedId: that.model.get('id_aditivotransferencia') ? that.model.get('id_aditivotransferencia').id_textosistema : null,
                    emptyOption: '[Selecione]',
                    model: TextoModel
                });
            }
        });

    },
    events: {
        'click @ui.bl_mesesmensalidade': 'actionMensalidades',
        'submit #form': 'actionSave'
    },
    actionMensalidades: function () {
        var checked = this.ui.bl_mesesmensalidade.prop('checked');
        var input = this.ui.nu_mesesmensalidade;

        input.prop('disabled', !checked);

        if (!checked) {
            input.val('0');
        }
    },
    actionSave: function (e) {
        e.preventDefault();

        var that = this;

        var values = Backbone.Syphon.serialize(this);

        // Validar campos obrigatórios
        var errorMsg = null;
        var errorElem = null;

        if (!values.st_contratoregra.trim()) {
            errorMsg = 'Preencha o título';
            errorElem = that.ui.st_contratoregra;
        }
        else if (!values.id_projetocontratoduracaotipo) {
            errorMsg = 'Informe o tipo de duração do contrato';
            errorElem = that.ui.id_projetocontratoduracaotipo;
        }
        else if (!values.id_tiporegracontrato) {
            errorMsg = 'Selecione um tipo de regra de contrato';
            errorElem = that.ui.id_tiporegracontrato;
        }
        else if (!values.id_contratomodelo) {
            errorMsg = 'Selecione o texto do contrato';
            errorElem = that.ui.id_contratomodelo;
        }
        else if (!values.id_extensaomodelo) {
            errorMsg = 'Selecione o texto do aditivo de renovação';
            errorElem = that.ui.id_extensaomodelo;
        }
        else if (!values.id_aditivotransferencia) {
            errorMsg = 'Selecione o texto do aditivo de transferência';
            errorElem = that.ui.id_aditivotransferencia;
        }

        if (errorMsg) {
            $.pnotify({
                title: 'Erro',
                text: errorMsg,
                type: 'error'
            });
            errorElem.focus();
            return false;
        }

        this.model.save(values, {
            beforeSend: loading,
            success: function () {
                $.pnotify({
                    title: 'Concluído',
                    text: 'O registro foi salvo com sucesso!',
                    type: 'success'
                });

                that.model.set(that.model.defaults);
                that.render();
            },
            error: function () {
                $.pnotify({
                    title: 'Erro',
                    text: 'Houve um erro ao tentar salvar o registro, tente novamente.',
                    type: 'error'
                });
            },
            complete: loaded
        });

    }
});


// Render / Renderizar views
// Verificar se possui id no url
loading();
var GlobalIni = new GlobalView();

var url = window.location.href;
var idParam = Number(url.split('/').pop());
if (!isNaN(idParam)) {
    var model = new GlobalModel();
    model.id = idParam;
    model.fetch({
        success: function (model, response) {
            model.set(response);

            GlobalIni = new GlobalView({
                model: model
            });

            G2S.layout.content.show(GlobalIni);
        }
    });
} else {
    G2S.layout.content.show(GlobalIni);
}

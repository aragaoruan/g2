var TipoOfertaModel = Backbone.Model.extend({
    idAttribute: 'id_tipooferta',
    defaults: {
        id_tipooferta: '',
        st_tipooferta: ''
    },
    url: function () {
        return '/api/tipo-oferta';
    }
});

var PeriodoLetivo = Backbone.Model.extend({
    idAttribute: 'id_periodoletivo',
    defaults: {
        id_periodoletivo: null,
        st_periodoletivo: '',
        dt_inicioinscricao: '',
        dt_fiminscricao: '',
        dt_abertura: '',
        dt_encerramento: '',
        id_tipooferta: '',
        st_tipooferta: '',
        id_entidade: '',
        id_usuariocadastro: '',
        id_editing: false
    },
    url: function () {
        return this.id ? '/api/periodo-letivo/' + this.id : '/api/periodo-letivo';
    }
});

var TipoOfertaCollection = Backbone.Collection.extend({
    model: TipoOfertaModel,
    url: '/api/tipo-oferta'
});

var PeriodoLetivoCollection = Backbone.Collection.extend({
    url: '/api/periodo-letivo',
    model: PeriodoLetivo,
    initialize: function () {
        return this.fetch();
    }
});

var PeriodoLetivoLayout = Marionette.LayoutView.extend({
    template: '#periodo-letivo-layout',
    tagName: 'div',
    className: 'container-fluid',
    regions: {
        adicionarNovoPeriodo: '#adicionar-novo-periodo-region',
        listarPeriodo: '#listar-periodo-region'
    },
    onShow: function () {

    }
});

var AdicionarPeriodoItemView = Marionette.ItemView.extend({
    template: '#adicionar-novo-periodo-template',
    tagName: 'div',
    model: new PeriodoLetivo(),
    className: 'well',
    ui: {
        formAdicionarPeriodo: '#form-adicionar-periodo',
        selectTipoOferta: 'select[name="id_tipooferta"]',
        st_periodoletivo: '#st_periodoletivo',
        dt_abertura: '#dt_abertura',
        dt_encerramento: '#dt_encerramento',
        dt_inicioinscricao: '#dt_inicioinscricao',
        dt_fiminscricao: '#dt_fiminscricao'
    },
    events: {
        "submit @ui.formAdicionarPeriodo": 'adicionarPeriodoEvent'
    },
    onRender: function (){
        this.populaSelectTipoOferta();
    },
    populaSelectTipoOferta: function () {
        var that = this;
        var tipoOfertaCol = new TipoOfertaCollection;
        tipoOfertaCol.fetch({
            beforeSend: loading,
            success: function () {
                new SelectView({
                    el: that.ui.selectTipoOferta,
                    collection: tipoOfertaCol,
                    childViewOptions: {
                        optionLabel: 'st_tipooferta',
                        optionValue: 'id_tipooferta'
                    },
                    sort:false
                }).render();
            },
            complete: function () {
                loaded();
            }
        });

    },
    adicionarPeriodoEvent: function (e) {
        e.preventDefault();

        if (this.validate()) {
            this.model.clear().set({
                st_periodoletivo: this.ui.st_periodoletivo.val(),
                id_tipooferta: this.ui.selectTipoOferta.val(),
                dt_abertura: this.ui.dt_abertura.val(),
                dt_encerramento: this.ui.dt_encerramento.val(),
                dt_inicioinscricao: this.ui.dt_inicioinscricao.val(),
                dt_fiminscricao: this.ui.dt_fiminscricao.val()
            }).save(null, {
                beforeSend: function () {
                    loading()
                },
                complete: function () {
                    loaded();
                },
                success: function (model, response) {
                    e.currentTarget.reset();
                    collectionListaPeriodo.unshift(model);

                    $.pnotify({
                        type: 'success',
                        title: 'Sucesso',
                        text: 'Oferta de Sala salva com sucesso.'
                    });

                },
                error: function (model, response) {
                    $.pnotify({
                        type: 'error',
                        title: 'Erro',
                        text: response.responseText
                    });
                }
            });
        }

    },
    validate: function() {
        var nu_inicioinscricao = Number(this.ui.dt_inicioinscricao.val().split('/').reverse().join(''));
        var nu_abertura = Number(this.ui.dt_abertura.val().split('/').reverse().join(''));

        // Verificar se data de inscrição é menor que a data de abertura
        if (nu_inicioinscricao > nu_abertura) {
            this.ui.dt_inicioinscricao.focus();
            $.pnotify({
                type: 'warning',
                title: 'ATENÇÃO',
                text: 'A data de Início da Inscrição deve ser menor que a data de Abertura'
            });
            return false;
        }

        return true;
    }
});

var PeriodoItemView = Marionette.ItemView.extend({
    tagName: 'tr',
    className: '',
    model: new PeriodoLetivo(),
    initialize: function () {
        this.model.set('id_editing', false);
    },
    ui: {
        btnExcluirItem: "#btn-excluir-item-periodo",
        btnEditItem: "#btn-editar-item-periodo",
        btnEditConfirmItem: "#btn-confirmar-edicao",
        st_periodoletivo_edit: '#st_periodoletivo_edit',
        selectTipoOferta_edit: 'select[name="id_tipooferta_edit"]',
        dt_abertura_edit: '#dt_abertura_edit',
        dt_encerramento_edit: '#dt_encerramento_edit',
        dt_inicioinscricao_edit: '#dt_inicioinscricao_edit',
        dt_fiminscricao_edit: '#dt_fiminscricao_edit'
    },
    events: {
        "click @ui.btnExcluirItem": 'excluirItemEvent',
        "click @ui.btnEditItem": 'editarItemEvent',
        "click @ui.btnEditConfirmItem": 'confirmEditarItemEvent'

    },
    populaSelectTipoOfertaEdit: function () {
        var that = this;
        var tipoOfertaEditCol = new TipoOfertaCollection;
        tipoOfertaEditCol.fetch({
            beforeSend: loading,
            success: function () {
                new SelectView({
                    el: that.ui.selectTipoOferta_edit,
                    collection: tipoOfertaEditCol,
                    childViewOptions: {
                        optionLabel: 'st_tipooferta',
                        optionValue: 'id_tipooferta',
                        optionSelected: that.model.get('id_tipooferta') ? that.model.get('id_tipooferta') : ''
                    },
                    sort:false
                }).render();
            },
            complete: function () {
                loaded();
            }
        });

    },
    getTemplate: function () {
        if (this.model.get("id_editing") == false) {
            return "#periodo-letivo-item";
        } else {
            return "#periodo-letivo-item-editando";
        }
    },
    excluirItemEvent: function () {
        var that = this;
        bootbox.confirm('Deseja realmente remover esta Oferta de Sala?', function (response) {
            if (response) {
                that.model.destroy({
                    wait: true,
                    success: function (model, response) {
                        $.pnotify({
                            type: 'success',
                            title: 'Sucesso',
                            text: response
                        });
                    },
                    beforeSend: function () {
                        loading();
                    },
                    complete: function () {
                        loaded();
                    },
                    error: function (model, response) {
                        $.pnotify({
                            type: 'error',
                            title: 'Erro',
                            text: response.responseText
                        });
                    }
                })
            }
        });
    },
    editarItemEvent: function () {
        this.model.set('id_editing', true);
        this.populaSelectTipoOfertaEdit();
        this.render();
    },
    confirmEditarItemEvent: function () {
        var self = this;

        if (this.validate()) {
            this.model.set({
                st_periodoletivo: this.ui.st_periodoletivo_edit.val(),
                id_tipooferta: this.ui.selectTipoOferta_edit.val(),
                dt_abertura: this.ui.dt_abertura_edit.val(),
                dt_encerramento: this.ui.dt_encerramento_edit.val(),
                dt_inicioinscricao: this.ui.dt_inicioinscricao_edit.val(),
                dt_fiminscricao: this.ui.dt_fiminscricao_edit.val(),
                id_editing: false
            }).save(null, {
                beforeSend: function () {
                    loading()
                },
                complete: function () {
                    loaded();
                },
                success: function (model, response) {
                    $.pnotify({
                        type: 'success',
                        title: 'Sucesso',
                        text: 'Oferta de Sala atualizada com sucesso.'
                    });
                    self.render();
                },
                error: function (model, response) {
                    $.pnotify({
                        type: 'error',
                        title: 'Erro',
                        text: response.responseText
                    });
                }
            });
        }

    },
    validate: function() {
        var nu_inicioinscricao = Number(this.ui.dt_inicioinscricao_edit.val().split('/').reverse().join(''));
        var nu_abertura = Number(this.ui.dt_abertura_edit.val().split('/').reverse().join(''));

        // Verificar se data de inscrição é menor que a data de abertura
        if (nu_inicioinscricao > nu_abertura) {
            this.ui.dt_inicioinscricao_edit.focus();
            $.pnotify({
                type: 'warning',
                title: 'ATENÇÃO',
                text: 'A data de Início da Inscrição deve ser menor que a data de Abertura'
            });
            return false;
        }

        return true;
    }
});

var PeriodoEmptyView = Marionette.ItemView.extend({
    template: '#sem-itens',
    tagName: 'tr'
});

var ListarPeriodoCompositeView = Marionette.CompositeView.extend({
    template: '#listar-periodo-template',
    tagName: 'div',
    className: 'well',
    childView: PeriodoItemView,
    childViewContainer: "#periodo-item-container",
    emptyView: PeriodoEmptyView
});

var layoutPeriodo = new PeriodoLetivoLayout();
G2S.show(layoutPeriodo);

layoutPeriodo.adicionarNovoPeriodo.show(new AdicionarPeriodoItemView());

var collectionListaPeriodo = new PeriodoLetivoCollection();
layoutPeriodo.listarPeriodo.show(new ListarPeriodoCompositeView({
    collection: collectionListaPeriodo
}));
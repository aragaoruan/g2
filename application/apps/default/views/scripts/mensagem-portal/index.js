/***********************
 * Cadastro de mensagens para portal
 */
$.getScript("js/utils/functions.js", function () {
});

var itemMensagem = Marionette.ItemView.extend({
    template: '#template-mensagem',
    dados: '',
    model: new Backbone.Model({
        st_mensagem: '',
        dt_enviar: '',
        dt_saida: '',
        st_texto: '',
        id_situacao: '',
        bl_importante: ''
    }),

    initialize: function () {
        this.carregaDadosRegistro();

    },
    onShow: function () {
        this.carregaAreas();
        loadTinyMCE('editor', 10, true);
        setTimeout(function () {
            disableTinyMCe(id_mensagem);
        }, 300)

        return this;

    },
    onRender: function () {
    },
    carregaAreas: function () {
        loading();
        var elem = this.$el;
        var AreaModel = Backbone.Model.extend({
            idAttribute: 'id_areaconhecimento',
            defaults: {
                id_areaconhecimento: null,
                st_areaconhecimento: ''
            }
        });
        ComboboxView({
            el: elem.find('[name="id_areaconhecimento"]'),
            model: AreaModel,
            url: '/api/area-conhecimento/',
            optionLabel: 'st_areaconhecimento',
            emptyOption: '[Selecione]',
            optionSelectedId: this.dados.id_areaconhecimento != null ? this.dados.id_areaconhecimento : null,
            parse: function (response) {
                if (!response.length) {
                    $.pnotify({
                        title: 'Aviso',
                        type: 'warning',
                        text: 'Nenhuma área encontrada'
                    });
                }
                return response;
            }
        });

        if (this.dados.id_areaconhecimento != null) {
            this.loadProjeto();
        }
        loaded();
    },
    carregaDadosRegistro: function () {
        var dados;
        var elem = this.$el;
        var modelMensagem = new VwPesquisaMensagemPortal();
        if (id_mensagem != null) {
            modelMensagem.url = '/api/vw-mensagem-portal/' + id_mensagem;
            modelMensagem.fetch({async: false})
                .then(function (e) {
                    dados = e;
                })
            this.dados = dados;
            this.model.set(dados)
        }
    },
    loadProjeto: function () {
        loading();
        var elem = this.$el;
        var ProjetoModel = Backbone.Model.extend({
            idAttribute: 'id_projetopedagogico',
            defaults: {
                id_projetopedagogico: null,
                st_projetopedagogico: ''
            }
        });
        var projeto = ComboboxView({
            el: elem.find('[name="id_projetopedagogico"]'),
            model: ProjetoModel,
            url: '/titular-certificacao/get-projeto-by-area/?id_areaconhecimento=' + (this.dados.id_areaconhecimento != null ? this.dados.id_areaconhecimento : elem.find('[name="id_areaconhecimento"]').val()),
            optionLabel: 'st_projetopedagogico',
            emptyOption: '[Selecione]',
            optionSelectedId: this.dados.id_projetopedagogico != null ? this.dados.id_projetopedagogico : null,
        });
        if (this.dados.id_projetopedagogico != null) {
            this.loadTurma();
        }
        loaded();
    },
    loadTurma: function () {
        loading();
        var elem = this.$el;

        var TurmaModel = Backbone.Model.extend({
            idAttribute: 'id_turma',
            defaults: {
                id_turma: null,
                st_turma: ''
            }
        });
        var turma = ComboboxView({
            el: elem.find('[name="id_turma"]'),
            model: TurmaModel,
            url: '/default/mensagem-portal/get-turma-by-projeto/?id_projetopedagogico=' + (this.dados.id_projetopedagogico != null ? this.dados.id_projetopedagogico : elem.find('[name="id_projetopedagogico"]').val()),
            optionLabel: 'st_turma',
            emptyOption: '[Selecione]',
            optionSelectedId: this.dados.id_turma != null ? this.dados.id_turma : null
        });
        loaded();
    },
    salvaMensagem: function () {
        if (!id_mensagem && !verificaEntradaTinymce(tinymce.activeEditor.getContent())) {
            $.pnotify({title: 'Erro', text: 'O texto da mensagem é obrigatório', type: 'warning'});
            return false;
        }

        if ($('#st_mensagem').val() == '') {
            $.pnotify({title: 'Erro', text: 'O título da mensagem é obrigatório', type: 'warning'});
            return false;
        }

        if ($('#dt_enviar').val() == '') {
            $.pnotify({title: 'Erro', text: 'A data para envio é obrigatório', type: 'warning'});
            return false;
        }

        var dataEnviar = ($('#dt_enviar').val()).split("/");
        var dataTermino = ($('#dt_saida').val()).split("/");
        var dataHoje = new Date();
        dataEnviar = new Date(dataEnviar[2], dataEnviar[1] - 1, dataEnviar[0]);
        dataTermino = dataTermino.length > 0 ? new Date(dataTermino[2], dataTermino[1] - 1, dataTermino[0]) : null;
        dataHoje = new Date(dataHoje.getFullYear(), dataHoje.getMonth(), dataHoje.getDate());
        if (dataEnviar < dataHoje && !$('#dt_enviar').is((':disabled'))) {
            $.pnotify({title: 'Erro', text: 'A data para envio não deve ser menor que a data atual', type: 'warning'});
            return false;
        }

        if (dataEnviar > dataTermino && !dataTermino) {
            $.pnotify({
                title: 'Erro',
                text: 'A data para envio não deve ser maior que a data de término',
                type: 'warning'
            });
            return false;
        } else if (!dataTermino) {
            dataTermino = null;
        }

        loading();

        var dados = $('#formMensagem').serializeArray();
        for (var index = 0; index < dados.length; ++index) {
            if (dados[index].name == "dt_saida" && !dataTermino) {
                dados[index].value = null;
            }

            if (dados[index].name == "st_texto") {
                dados[index].value = tinymce.get('editor').getContent();
                break;
            }
        }

        $.ajax({
            url: "default/mensagem-portal/salvar-mensagem-portal",
            type: 'POST',
            dataType: 'json',
            data: dados,
            beforeSend: function () {
                loading();
            },
            success: function (data) {
                // $('#id_mensagem').val(data.obj);
                // G2S.editModel.set('id_mensagem', data.obj);
                window.location = '#/pesquisa/CadastrarMensagemPortal';
                $.pnotify({title: 'Cadastro de Mensagem Portal', text: data.text, type: 'success'});
                loaded();
            },
            complete: function () {
                loaded();
            },
            error: function (data) {
                $.pnotify({
                    title: 'Erro',
                    text: 'Ocorreu um erro ao cadastrar a mensagem: ' + data.text,
                    type: 'danger'
                });
            }
        });
    },

    events: {
        'click #btn_enviarmensagem': 'salvaMensagem',
        'change [name="id_areaconhecimento"]': 'loadProjeto',
        'change [name="id_projetopedagogico"]': 'loadTurma',
    }
});

$(document).ready(function () {
    $('.anchor').bind('click', function () {
        $('#editor').attr('value', $(this).closest('item').find('texto').html());
        $('.anchor').bind('click', function () {
        });
    });
});


function htmlspecialchars_decode(string, quote_style) {
    var optTemp = 0,
        i = 0,
        noquotes = false;
    if (typeof quote_style === 'undefined') {
        quote_style = 2;
    }
    string = string.toString().replace(/&lt;/g, '<').replace(/&gt;/g, '>');
    var OPTS = {
        'ENT_NOQUOTES': 0,
        'ENT_HTML_QUOTE_SINGLE': 1,
        'ENT_HTML_QUOTE_DOUBLE': 2,
        'ENT_COMPAT': 2,
        'ENT_QUOTES': 3,
        'ENT_IGNORE': 4
    };
    if (quote_style === 0) {
        noquotes = true;
    }
    if (typeof quote_style !== 'number') { // Allow for a single string or an array of string flags
        quote_style = [].concat(quote_style);
        for (i = 0; i < quote_style.length; i++) {
            // Resolve string input to bitwise e.g. 'PATHINFO_EXTENSION' becomes 4
            if (OPTS[quote_style[i]] === 0) {
                noquotes = true;
            } else if (OPTS[quote_style[i]]) {
                optTemp = optTemp | OPTS[quote_style[i]];
            }
        }
        quote_style = optTemp;
    }
    if (quote_style & OPTS.ENT_HTML_QUOTE_SINGLE) {
        string = string.replace(/&#0*39;/g, "'"); // PHP doesn't currently escape if more than one 0, but it should
        // string = string.replace(/&apos;|&#x0*27;/g, "'"); // This would also be useful here, but not a part of PHP
    }
    if (!noquotes) {
        string = string.replace(/&quot;/g, '"');
    }
    // Put this in last place to avoid escape being double-decoded
    string = string.replace(/&amp;/g, '&');

    return string;
}


var mensagemView = new itemMensagem();
G2S.layout.content.show(mensagemView);
loaded();

function disableTinyMCe(id_mensagem) {
    if (id_mensagem) {
        tinymce.EditorManager.execCommand('mceToggleEditor', true, 'editor');
        var texto = $('#editor').val();
        $('#editor').hide();
        $('#div_editor').html(texto).css('border', '1px solid #d8d8d8');
        $('#div_editor').css('background-color', 'white')
            .css('min-height', '200px')
            .css('padding', '5px');
    } else {

    }
}


$.ajaxSetup({async: true});
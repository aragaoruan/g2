/**
 * CADASTRO DE ATENDENTES
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */

/*********************************************
 ************** COLLECTIONS ******************
 *********************************************/
var ColletionSituacao = Backbone.Collection.extend({
    model: Situacao,
    url: '/api/situacao/?st_tabela=tb_nucleofuncionariotm',
    initialize: function () {
        this.fetch({async: false});
    }
});

/*********************************************
 ************** VARIAVEIS ********************
 *********************************************/
var pessoaDados;

/*********************************************
 **************   ITEMVIEW  ******************
 *********************************************/

var ViewDados = Marionette.ItemView.extend({
    template: '#template-dados',
    tagName: 'div',
    ui: {
        id_situacao: '#id_situacao',
        id_nucleofuncionariotm: '.id_nucleofuncionariotm'
    },
    onShow: function () {
        this.populaSelectDados();
        this.carregaEntidade();
        this.ui.id_nucleofuncionariotm.val(G2S.editModel.id_nucleofuncionariotm);
    },
    populaSelectDados: function () {
        var col = new ColletionSituacao();

        var view = new SelectView({
            el: this.ui.id_situacao, // Elemento da DOM
            collection: col, // Instancia da collection a ser utilizada
            childViewOptions: {
                optionLabel: 'st_situacao', // Propriedade da Model que será utilizada como label do select
                optionValue: 'id_situacao', // Propriedade da Model que será utilizada como id do option do select
                optionSelected: G2S.editModel ? G2S.editModel.id_situacao : null      // ID da option que receberá o atributo "selected"
            }
        });
        /*console.log(new MunicipioSelectView());
         layoutAntendente.select.show(new MunicipioSelectView());*/
        view.render();
    },
    carregaEntidade: function () {
        loaded();
        if (G2S.editModel && G2S.editModel.id_nucleotm) {
            var urljstree = "atendente/pesquisa-nucleo-usuario?id_nucleotm=" + G2S.editModel.id_nucleotm;
            var habiliado = false;
        } else {
            var urljstree = "atendente/pesquisa-nucleo";
            var habiliado = true;
        }
        $("#entidades").jstree({
            "html_data": {
                "ajax": {
                    "url": urljstree,
                    "data": function (n) {
                        return {id: n.attr ? n.attr("id") : 0};
                    }
                }
            },
            "plugins": ["themes", "html_data", "checkbox", "ui"], "checkbox": {"two_state": true}
        }).bind("loaded.jstree", function (event, data) {
            if (G2S.editModel && G2S.editModel.id_nucleotm) {
                $.jstree._reference(".id_nucleotm").hide_checkboxes();
            }
        });
    }
})


/*********************************************
 ************** LAYOUT ***********************
 *********************************************/
var LayoutAntendente = Marionette.LayoutView.extend({
    template: '#template-dados-atendente',
    tagName: 'div',
    className: 'container-fluid',
    regions: {
        dados: '#wrapper-dados',
        select: '#id_situacao'
    },
    events: {
        "click #btn-salvar": "salvarDados"
    },
    initialize: function () {
        thatLayout = this;
    },
    onShow: function () {
        if (G2S.editModel) {
            var that = this;
            //Limpa a div do autocomplete para não escrever o componente 2 vezes
            that.$el.find('#autoComplete').empty();
            //Busca a pessoa
            var vwPessoa = new VwPesquisarPessoa();
            vwPessoa.url = '/pessoa/get-vw-pesquisar-pessoa-by-id/id/' + G2S.editModel.id_usuario;
            vwPessoa.fetch({
                beforeSend: function () {
                    loading();
                },
                complete: function () {
                    loaded();
                },
                error: function (model, response) {
                    loaded();
                    $.pnotify({
                        type: 'error',
                        title: 'Erro!',
                        text: 'Houve um erro ao consultar dados da pessoa. ' + response.responseText
                    });
                },
                success: function (model) {
                    //Transforma o resultado do fetch em objeto Json
                    var pessoaFix = vwPessoa.toJSON();
                    pessoaFix.id = pessoaFix.id_usuario; //Cria um novo atributo no objeto

                    //Limpa o auto complete
                    componenteAutoComplete.clearFixModel();
                    componenteAutoComplete.modelRender = pessoaFix; //seta no modelRenderer do autocomplete
                    componenteAutoComplete.setFixModel();//e define o registro fixado

                    //Cria o AutoComplete
                    componenteAutoComplete.main({
                        model: VwPesquisarPessoa,
                        element: that.$el.find('#autoComplete')
                    }, function () {
                        pessoaDados = componenteAutoComplete.getFixModel();
                    }, function () {

                    });
                }
            });

        } else {
            //Chama o componente de autocomplete
            componenteAutoComplete.main({
                model: VwPesquisarPessoa,
                element: this.$el.find('#autoComplete')
            }, function () {
                pessoaDados = componenteAutoComplete.getFixModel();
            }, function () {
            });
        }

        this.dados.show(new ViewDados());
    },
    salvarDados: function () {
        that = this;
        if ($('#id_situacao').val() != '') {
            var checked_ids = [];
            $("#entidades").jstree("get_checked", null, true).each
            (function () {
                checked_ids.push(this.id);
            });

            parametros = {
                'id_situacao': $('#id_situacao').val(),
                'id_nucleofuncionariotm': $('.id_nucleofuncionariotm').val(),
                'ids_nucleotm': checked_ids,
                'usuario': pessoaDados
            };

            $.ajax({
                type: 'post',
                url: '/atendente/salvar-atendente',
                data: parametros,
                beforeSend: function () {
                    loading();
                },
                success: function (data) {
                    $.pnotify({
                        title: data['title'],
                        text: data['mensagem'],
                        type: 'success'
                    });
                },
                complete: function () {
                    loaded();
                }
            })
        } else {
            $.pnotify({
                title: 'Campo Obrigatório!',
                text: 'Selecione um status',
                type: 'error'
            });
        }
    }
});

/*********************************************
 ************** INICIALIZAÇÃO ****************
 *********************************************/

var layoutAntendente = new LayoutAntendente();
G2S.show(layoutAntendente);
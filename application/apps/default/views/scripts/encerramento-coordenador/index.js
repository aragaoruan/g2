/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * *                             VARÍAVEIS                             * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */

var that;
var dataRequest = {};
var professor = $('#id_professordisciplina');

/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * *                             MODELS                                * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */


/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * *                        COLLECTIONS                                * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
var TipoDisciplinaCollection = Backbone.Collection.extend({
    model: TipoDisciplina
});

var CategoriaSalaCollection = Backbone.Collection.extend({
    model: CategoriaSala
});

var ProfessorCollection = Backbone.Collection.extend({
    model: VwProfessorDisciplina
});

var EncerramentoSalaCollection = Backbone.PageableCollection.extend({
    url: '/vw-encerramento-coordenador/retornar-encerramento',
    model: VwEncerramentoSalas,
    state: {
        pageSize: 10
    },
    mode: "client",
    save: function () {
        var encerramento = [];
        _(this.models).each(function (model) {
            if (model.get('dt_encerramentocoordenador') != '')
                encerramento.push(model.toJSON());
        });

        if (encerramento.length > 0) {
            $.ajax({
                url: '/vw-encerramento-salas/salvar-encerramento',
                type: 'POST',
                data: {
                    encerramento: encerramento,
                    tipo_liberacao: 'coordenador'
                },
                dataType: 'JSON',
                success: function (response) {
                    $.pnotify({title: 'Sucesso', type: 'success', text: MensagensG2.MSG_ENCERRAMENTO_SUCESSO});
                    that.renderEncerramentoView();
                },
                error: function (response) {
                    $.pnotify({title: 'Alerta', type: 'warning', text: sprintf(MensagensG2.MSG_ERROR_CUSTOM, 'Erro ao salvar as datas de encerramento')});
                },
                complete: function (response) {
                    loaded();
                },
                beforeSend: function () {
                    loading();
                }
            });
        } else {
            $.pnotify({title: 'Aviso', type: 'warning', text: 'Não ha registros escolhios para serem encerrados'});
        }
    }
});

var collection = new EncerramentoSalaCollection();

/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * *                             VIEWS                                 * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
var FormPesquisaView = Backbone.View.extend({
    //tagName: 'option',
    el: '#form-pesquisa-encerramento',
    collection: collection,
    initialize: function () {
        this.render();
    },
    render: function () {
        that = this;
        this.popularTipoDisciplina();
        this.popularCategoriaSala();
        this.popularProfessor();
        loaded();
//        this.encerramentoView = new EncerramentoView();
    },
    events: {
        "change #id_tipodisciplina": "changeTipoDisciplina",
//        "change #id_categoriasala": "renderEncerramentoView",
//        "change #id_professordisciplina": "renderEncerramentoViewProfessor",
//        "change #liberacao": 'renderLiberadoView',
        "click .salvarEncerramento": "salvarEncerramento",
        "click .exportarXls": "exportarXls",
        "click .pesquisaEncerramento" : "renderEncerramentoView"
    },
    salvarEncerramento: function (e) {
        this.collection.save();
    },
    encerramentoView: null,
    changeTipoDisciplina: function(e){
        this.popularProfessor();
        if ($(e.currentTarget).val() == 2) {
            $('.recusa').addClass('span2').show();
            $('.acao.final').removeClass('span4').addClass('span2');
        } else {
            $('.recusa').removeClass('span2').hide();
            $('.acao.final').addClass('span4').removeClass('span2');
        }
    },
    popularTipoDisciplina: function () {
        var tipoCollection = new TipoDisciplinaCollection();
        tipoCollection.url = '/api/tipo-disciplina';
        tipoCollection.fetch({
                success: function () {
                    var comboView = new CollectionView({
                        collection: tipoCollection,
                        childViewTagName: 'option',
                        childViewConstructor: OptionTipoDisciplinaView,
                        el: $('#id_tipodisciplina')
                    });
                    comboView.render();
                    $(comboView.el).prepend("<option value='' selected>Selecione</option>");
                }}
        );
    },
    popularProfessor: function () {
        var tipoCollection = new ProfessorCollection();
        tipoCollection.url = '/api/vw-professor-disciplina';
        tipoCollection.fetch({
                data: $.param({
                    tipo_disciplina: $('#id_tipodisciplina :selected ').val()
                }),
                success: function () {
                    var comboView = new CollectionView({
                        collection: tipoCollection,
                        childViewTagName: 'option',
                        childViewConstructor: OptionProfessorDisciplinaView,
                        el: $('#id_professordisciplina')
                    });
                    comboView.render();
                    $(comboView.el).prepend("<option value='' selected>Todos</option>");
                }}
        );
    },
    popularCategoriaSala: function () {
        var categoriaSalaCollection = new CategoriaSalaCollection();
        categoriaSalaCollection.url = '/api/categoria-sala';
        categoriaSalaCollection.fetch({success: function () {
                var comboView = new CollectionView({
                    collection: categoriaSalaCollection,
                    childViewTagName: 'option',
                    childViewConstructor: OptionCategoriaSalaView,
                    el: $('#id_categoriasala')
                });
                comboView.render();
            }}
        );
    },
    renderEncerramentoViewProfessor: function () {
        if (this.encerramentoView) {
            this.encerramentoView.destroyMe();
        }

        dataRequest.id_professor = professor.val();

        this.encerramentoView = new EncerramentoView();
    },
    renderEncerramentoView: function () {
        if (this.encerramentoView) {
            this.encerramentoView.destroyMe();
        }
        if ($('#id_tipodisciplina').val() == '') {
            $.pnotify({title: 'Erro', type: 'error', text: 'Escolha o tipo de disciplina antes de prosseguir!'});
            return false;
        }
        if(professor.val() != ''){
            dataRequest.id_professor = professor.val();
        }
        this.encerramentoView = new EncerramentoView();
    },
    renderLiberadoView: function (e) {
        this.encerramentoView = new EncerramentoView();
        if ($(e.currentTarget).val() == 'financeiro') {

        }
    },

    exportarXls: function (e) {
        var dados = [];
        var tipo_disciplina = $('#id_tipodisciplina').val();
        var cabecalho = {};

        _(this.collection.fullCollection.models).each(function (model, d) {
            dados.push(model.toJSON());
        });

        if (tipo_disciplina == 2) {
            cabecalho = {
                id_encerramentosala: 'ID',
                st_saladeaula: 'Sala de Aula',
                st_professor: 'Professor',
                st_projetopedagogico: 'Curso',
                st_nota: 'Nota',
                st_tituloavaliacao: 'Título da Monografia',
                st_aluno: 'Aluno',
                dt_encerramentoprofessor: 'Coordenador'
            };

            if($('#bl_recusado').val() == 1){
                cabecalho.st_motivorecusa = 'Motivo Recusa';
            }
        } else {
            cabecalho = {
                id_encerramentosala: 'ID',
                st_saladeaula: 'Sala de Aula',
                st_professor: 'Professor',
                dt_encerramentoprofessor: 'Coordenador',
                st_disciplina: 'Disciplina',
                st_areaconhecimento: 'Área Conhecimento',
                nu_alunos: 'Alunos a encerrar'
            };
        }

        $.ajax({
            url: "/relatorio/gerar-xls",
            dataType: 'html',
            type: 'post',
            data: {
                'dados': JSON.stringify(dados),
                'cabecalho': JSON.stringify(cabecalho),
                'converterUtf8': true
            },
            success: function (resp) {
                var win = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(resp), "", "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=780, height=200, top=0, left=0");
                setTimeout(function () {
                    win.destroy();
                }, 4000)
            },
            complete: function (resp) {

            }
        })
    }
});

var EncerramentoView = Backbone.View.extend({
    initialize: function () {
        this.render();
    },
    render: function () {
        this.renderGrid();
        return this;
    },
    destroyMe: function () {
        //COMPLETELY UNBIND THE VIEW
        this.undelegateEvents();
        $(this.el).removeData().unbind();
        //Remove view from DOM
        this.el.remove();
        Backbone.View.prototype.remove.call(this);
    },
    renderGrid: function () {
        var containerGrid = $('#container-grid');
        var colecao = that.collection;
        var tipo_liberacao = $('#liberacao').val();
        var tipo_disciplina = $('#id_tipodisciplina').val();
        var categoria_sala = $('#id_categoriasala').val();
        var recusa = '';
        if ($('#id_tipodisciplina').val() == 2) {
            recusa = $('#bl_recusado').val();
        }


        /*
         * Colunas do datagrid
         * */
        var cellSalaDeAula = Backgrid.Cell.extend({
            events: {
                "click .st_saladeaula": "abrirDetalhamento"
            },
            render: function () {
                this.$el.html(_.template('<a href="javascript:void(0)" class="st_saladeaula">'+this.model.get('st_saladeaula')+'</a>'));
                this.delegateEvents();
                return this;
            },
            abrirDetalhamento: function () {
                $.ajax({
                    url: '/vw-encerramento-salas/retornar-dados-detalhamento-sala',
                    type: 'GET',
                    data: {
                        id_encerramentosala: this.model.get('id_saladeaula')
                    },
                    dataType: 'HTML',
                    success: function (response) {
                        var modal_detalhamento = $('#modal-detalhamento');
                        modal_detalhamento.html(response);
                        modal_detalhamento.modal('show');
                    },
                    error: function (response) {

                    },
                    complete: function () {
                        loaded();
                    },
                    beforeSend: function () {
                        loading();
                    }
                })
            }
        });

         var cellDate = Backgrid.Cell.extend({
            events: {
//                "focus .encerramento-date": "habilitarData",
                "change .encerramento-date": "resetData"
            },
            resetData: function (e) {
                $(e.currentTarget).parents('tr').toggleClass('hover');
                $(e.currentTarget).parents('tbody').toggleClass('hover');

                var thatReset = this;

                /**
                 * Hotfix AC-26666-problema-tela-repasse-g2
                 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
                 * Alteracao para determinar por acao do usuario qual registro
                 * deseja encerrar ou nao
                 */
                if ($(e.currentTarget).is(':checked')) {
                    thatReset.model.set({
                        dt_encerramentocoordenador: 1
                    });
                } else {
                    thatReset.model.set({
                        dt_encerramentocoordenador: ''
                    });
                }
            },
            habilitarData: function (e) {
                containerGrid.find('table tr').removeClass('hover');
                $(e.currentTarget).parents('tr').addClass('hover');
                $(e.currentTarget).parents('tbody').addClass('hover');
                e.preventDefault();
            },
            render: function () {
                var data = '';
                //var select = '';
                //if (this.model.get('st_encerramentocoordenador') != "") {
                //    data = this.model.get('st_encerramentocoordenador');
                //    select = 'checked';
                //
                //    /**
                //     * Hotfix AC-26666-problema-tela-repasse-g2
                //     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
                //     * Alteracao para determinar no carregamento a marcacao automatica
                //     * dos registros para encerramento
                //     */
                //    this.model.set('dt_encerramentocoordenador', 1);
                //}
                this.$el.html(_.template("<input class='encerramento-date' type='checkbox' value='" + data + "'/>"));
                this.$el.addClass('editor');
                this.delegateEvents();
                return this;
            }
        });

        var cellTcc = Backgrid.Cell.extend({
            events: {
                "click .interacoesTCC": "abrirRelatorio"
            },
            render: function () {
                var interacao, versao_final;

                interacao = '<a class="btn interacoesTCC" title="Visualizar interações" href="javascript:void(0)"><i class="icon-tags"></i></a>';

                if (this.model.get('st_upload') != '') {
                    versao_final = '<a class="btn" target="_blank" title="Versão final do tcc" href="/upload/avaliacao/' + this.model.get('st_upload') + '"><i class="icon-file"></i></a>';
                } else {
                    versao_final = '';
                }

                this.$el.html(_.template("<div class='btn-toolbar'><div class='btn-group'>" + interacao + versao_final + "</div></div>"));
                this.delegateEvents();
                return this;
            },
            abrirRelatorio: function () {
                var that = this;
                $.ajax({
                    dataType: 'json',
                    type: 'post',
                    url: '/vw-encerramento-salas/retornar-link-interacoes',
                    data: { to: 'VwEncerramentoCoordenadorTO'
                        ,obj: that.model.toJSON()},
                    beforeSend: function () {
                        loading();
                    },
                    success: function (dataResponse) {
                        if(dataResponse.tipo == 0){
                            $.pnotify({title: dataResponse.retorno, text: dataResponse.mensagem, type: dataResponse.type});
                        }else{
                            var win = window.open(dataResponse.mensagem, '_blank');
                            if(win){
                                win.focus();
                            }else{
                                $.pnotify({title: 'Aviso', text: 'Desbloqueie o pop-up do navegador!', type: 'warning'});
                                loaded();
                            }
                        }
                    },
                    complete: function () {
                        loaded();
                    }
                });
            }
        });

        var cellTextoRecusa = Backgrid.Cell.extend({
            events: {
                "click" : function(){
                   this.render(1);
                }
            },
            render: function (completo) {
                    var interacaoCompleto, textoParticionado;

                    interacaoCompleto = this.model.get('st_motivorecusa') != null ? this.model.get('st_motivorecusa') : '';
                    textoParticionado = interacaoCompleto.substr(0,45) + (interacaoCompleto.length > 45 ? ' (...)' : '');;

                    if(completo != undefined && completo == 1){
                        this.$el.html(_.template("<div>"+interacaoCompleto+"</div>"));
                    }else{
                        this.$el.html(_.template("<div>"+textoParticionado+"</div>"));
                    }
                    this.$el.addClass('motivo-recusa');
                    this.delegateEvents();
                    return this;
            }
        });


        //coluna do relatorio do moodle
        var cellRelatorio = Backgrid.Cell.extend({
            events: {
                "click .relatorio": "abrirRelatorioMoodle"
            },
            render: function () {
                this.$el.html(_.template("<div class='btn-toolbar'><div class='btn-group'> <a class='btn  btn-success relatorio' title='Relatório' href='javascript:void(0)'>" +
                "<i class='icon icon-white icon-list-alt'></i></a></div></div>"));
                this.delegateEvents();
                return this;
            },
            abrirRelatorioMoodle: function () {
                var that = this;
                $.ajax({
                    dataType: 'json',
                    type: 'post',
                    url: '/vw-encerramento-salas/retornar-link-relatorio',
                    data: { to: 'VwEncerramentoCoordenadorTO'
                        ,obj: that.model.toJSON()},
                    beforeSend: function () {
                        loading();
                    },
                    success: function (dataResponse) {
                        if(dataResponse.tipo == 0){
                            $.pnotify({title: dataResponse.retorno, text: dataResponse.mensagem, type: dataResponse.type});
                        }else{
                            var win = window.open(dataResponse.mensagem, '_blank');
                            if(win){
                                win.focus();
                            }else{
                                $.pnotify({title: 'Aviso', text: 'Desbloqueie o pop-up do navegador!', type: 'warning'});
                                loaded();
                            }
                        }
                    },
                    complete: function () {
                        loaded();
                    }
                });
            }
        });

        //fim coluna relatorio moodle

        var columns = null;
        if (tipo_liberacao == 'coordenador') {
            columns = [
                {
                    name: "id_encerramentosala",
                    label: "ID",
                    editable: false,
                    cell: "string"
                },
                {
                    name: "st_saladeaula",
                    label: "Sala de Aula",
                    editable: false,
                    cell: cellSalaDeAula
                },
                {
                    name: "nu_cargahoraria",
                    label: "Carga Horária",
                    editable: false,
                    cell: "string"
                },
                {
                    name: "st_disciplina",
                    label: "Disciplina",
                    editable: false,
                    cell: "string"
                },
                {
                    name: "st_professor",
                    label: "Professor",
                    editable: false,
                    cell: "string"
                },
                {
                    name: "relatorio",
                    label: "Relatório",
                    editable: false,
                    cell: cellRelatorio
                },
                {
                    name: "nu_alunos",
                    label: "Alunos",
                    editable: false,
                    cell: "string"
                },
                {
                    name: "st_encerramentoprofessor",
                    label: "Coordenador",
                    editable: false,
                    cell: 'string'
                },
                {
                    name: "st_encerramentocoordenador",
                    label: "Pedagógico",
                    editable: false,
                    cell: cellDate
                },
                {
                    name: "st_encerramentopedagogico",
                    label: "Financeiro",
                    editable: false,
                    cell: "string"
                },
                {
                    name: "st_encerramentofinanceiro",
                    label: "Pagamento",
                    editable: false,
                    cell: "string"
                }
            ];
        } else {
            columns = [
                {
                    name: "id_encerramentosala",
                    label: "ID",
                    editable: false,
                    cell: "string"
                },
                {
                    name: "st_saladeaula",
                    label: "Sala de Aula",
                    editable: false,
                    cell: cellSalaDeAula
                },
                {
                    name: "nu_cargahoraria",
                    label: "Carga Horária",
                    editable: false,
                    cell: "string"
                },
                {
                    name: "st_disciplina",
                    label: "Disciplina",
                    editable: false,
                    cell: "string"
                },
                {
                    name: "st_professor",
                    label: "Professor",
                    editable: false,
                    cell: "string"
                },
                {
                    name: "relatorio",
                    label: "Relatório",
                    editable: false,
                    cell: cellRelatorio
                },
                {
                    name: "nu_alunos",
                    label: "Alunos",
                    editable: false,
                    cell: "string"
                },
                {
                    name: "st_encerramentoprofessor",
                    label: "Coordenador",
                    editable: false,
                    cell: "string"
                },
                {
                    name: "st_encerramentocoordenador",
                    label: "Pedagógico",
                    editable: false,
                    cell: "string"
                },
                {
                    name: "st_encerramentopedagogico",
                    label: "Financeiro",
                    editable: false,
                    cell: "string"
                },
                {
                    name: "st_encerramentofinanceiro",
                    label: "Pagamento",
                    editable: false,
                    cell: "string"
                }
            ];
        }

        var acao = $('.acao');
        var acaoFinal = $('.acao.final');
        if (tipo_disciplina == 2) {
            var new_columns = [];

            for (var i = 0; i < columns.length; i++) {
                if (columns[i].name == 'nu_alunos') {
                    this.removeColumns(columns, i);
                }

                if (columns[i].name == 'nu_cargahoraria') {
                    this.removeColumns(columns, i);
                }

                if (columns[i].name == 'st_disciplina') {
                    this.removeColumns(columns, i);
                }

                if (columns[i].name == 'relatorio') {
                    this.removeColumns(columns, i);
                }

                if (columns[i].name == 'st_professor') {
                    new_columns.push(columns[i]);
                    new_columns.push(
                        {
                            name: "st_projetopedagogico",
                            label: "Curso",
                            editable: false,
                            cell: "string"
                        },
                        {
                            name: "st_nota",
                            label: "Nota",
                            editable: false,
                            cell: "string"
                        },
                        {
                            name: "st_tituloavaliacao",
                            label: "Título da Monografia",
                            editable: false,
                            cell: "string"
                        },
                        {
                            name: '',
                            label: "TCC",
                            editable: false,
                            cell: cellTcc
                        },
                        {
                            name: "st_aluno",
                            label: "Aluno",
                            editable: false,
                            cell: "string"
                        }
                    )
                } else {
                    new_columns.push(columns[i]);
                }
            }
            if(recusa == 1){
                new_columns.push({
                    name: "st_motivorecusa",
                    label: "Motivo Recusa",
                    editable: false,
                    cell: cellTextoRecusa
                });
            }
            columns = new_columns;
        }

        columns = this.devolverTcc(columns, {tipoLiberacao: tipo_liberacao, tipoDisciplina: tipo_disciplina});

        // recupera os parametros passados
        dataRequest.id_tipodisciplina = tipo_disciplina;
        dataRequest.id_categoriasala = categoria_sala;
        dataRequest.tipo_liberacao = tipo_liberacao;
        dataRequest.id_professor = professor.val();
        dataRequest.bl_recusado = recusa;

        var grid = new Backgrid.Grid({
            className: 'backgrid table table-bordered table-hover encerramento',
            columns: columns,
            collection: colecao
        });

        var paginacaoPrincipal = new Backgrid.Extension.Paginator({
            collection: colecao
        });

        colecao.fetch({
            data: dataRequest,
            complete: function (collection, response) {
                containerGrid.empty().append(grid.render().el).append(paginacaoPrincipal.render().el);
                loaded();
            },
            beforeSend: function () {
                loading();
            }
        });
    },
    devolverTcc: function (columns, types) {
        if (VerificarPerfilPermissao(52, 450).length == 1 && this.validateTypes(types))
            columns.push(this.colunaDevolverTcc());
        return columns
    },
    validateTypes: function (types) {
        return types.tipoDisciplina == TIPO_DISCIPLINA.TCC && (types.tipoLiberacao == 'pedagogico' || types.tipoLiberacao == 'coordenador');
    },
    colunaDevolverTcc: function () {
        return {
            name: '',
            label: "Devolver Tcc",
            editable: false,
            cell: this.cellDevolverTcc()
        }
    },
    cellDevolverTcc: function () {
        return Backgrid.Cell.extend({
            template: _.template("<div class='btn-toolbar devolver-tcc-btns' style='text-align: center;'>" +
                "<a class='btn btn-primary devolve-tcc' title='Devolver TCC para o Tutor'>" +
                "<i class='icon icon-white icon-remove-circle'></i>" +
                "</a>" +
                "</div>"),
            events: {
                "click .devolve-tcc": "devolveTcc",
                "hidden #modal-devolver-tcc": "destroy"
            },
            devolveTcc: function () {
                $('#devolver-tcc-modal').empty();
                var customModal = _.template($('#template-devolver-tcc').html());

                $('#devolver-tcc-modal').append(customModal);
                $('.modal .hide').show();
                $('.modal-recusa-pagamentol').modal();
                $("#modal-devolver-tcc").modal('show');
                $("#modal-devolver-tcc").find('#id_encerramentosala').val(this.model.get('id_encerramentosala'));
                $("#modal-devolver-tcc").find('#id_saladeaula').val(this.model.get('id_saladeaula'));
                $("#modal-devolver-tcc").find('#id_usuario').val(this.model.get('id_aluno'));
                $("#modal-devolver-tcc").find('#id_matricula').val(this.model.get('id_matricula'));
                $(".btn-devolver-tcc").attr("id", "btn-devolver-tcc" + this.model.get('id_encerramentosala'));
                $("#modal-devolver-tcc").find('#motivo-devolucao').val('');
                var tr = this.el;
                $('#btn-devolver-tcc' + this.model.get('id_encerramentosala')).click(function () {
                    if ($('#motivo-devolucao').val() == '') {
                        $.pnotify({title: 'Aviso', text: 'O campo Motivo da Devolução é obrigatório!', type: 'error'});
                        return false;
                    }
                    var dadosForm = $('#form-devolver-tcc').serialize();
                    var dadosFormConcat = dadosForm.concat("&liberacao=coordenador");

                    $.ajax({
                        dataType: 'json',
                        type: 'post',
                        url: 'alocacao/devolver-tcc',
                        data: dadosFormConcat,
                        success: function (data) {
                            $.pnotify({title: data.retorno, text: data.mensagem, type: data.type});
                            if (data.type == 'success') {
                                $('#modal-devolver-tcc').modal('hide');
                                $(tr).parent('tr').remove();
                                $(tr).parent('tr').prevObject.remove();
                            }
                        }
                    });
                });
            },
            render: function () {
                this.$el.html(this.template());
                this.delegateEvents();
                return this;
            },
            destroy: function (e) {
                $("modal-devolver-tcc").remove();
            }
        });
    },
    removeColumns: function (columns, index) {
        columns.splice(index, 1);
    }
});

var OptionProfessorDisciplinaView = Backbone.View.extend({
    template: _.template('<%=id_usuario%>'),
    render: function () {
        this.$el.attr('value', this.model.id_usuario ? this.model.id_usuario : this.model.get('id_usuario'));
        this.$el.text(this.model.get('st_nomecompleto'));
        return this;
    }
});

var OptionTipoDisciplinaView = Backbone.View.extend({
    template: _.template('<%=id_tipodisciplina%>'),
    render: function () {
        this.$el.attr('value', this.model.id_tipodisciplina ? this.model.id_tipodisciplina : this.model.get('id_tipodisciplina'));
        this.$el.text(this.model.get('st_tipodisciplina'));
        return this;
    }
});

var OptionCategoriaSalaView = Backbone.View.extend({
    template: _.template('<%=id_categoriasala%>'),
    render: function () {
        this.$el.attr('value', this.model.id_categoriasala ? this.model.id_categoriasala : this.model.get('id_categoriasala'));
        this.$el.text(this.model.get('st_categoriasala'));
        return this;
    }
});

/**
 *******************************************************************************
 *******************************************************************************
 **                Outras funcionalidades
 *******************************************************************************
 *******************************************************************************
 */

var formPesquisaView = new FormPesquisaView();

$.ajaxSetup({async: true});

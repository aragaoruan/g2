// Variavel cache para "Situacao"
var comboSituacao = null;

// MODEL
var TipoMaterialModel = Backbone.Model.extend({
    idAttribute: 'id_tipodematerial',
    defaults: {
        id_tipodematerial: null,
        st_tipodematerial: '',
        id_situacao: {
            id_situacao: null,
            st_situacao: ''
        },
        id_entidade: {
            id_entidade: null
        }
    },
    url: function() {
        return '/api/tipo-material/' + (this.id || '');
    },
    is_editing: false,
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    }
});

// ITEM VIEW
var TipoMaterialView = Marionette.ItemView.extend({
    model: new TipoMaterialModel(),
    tagName: 'tr',
    ui: {
        id_situacao: '[name="id_situacao[id_situacao]"]'
    },
    onBeforeRender: function() {
        this.template = this.model.is_editing ? '#tipo-material-template-edit' : '#tipo-material-template-list'
    },
    onRender: function() {
        var that = this;
        var elem = this.$el;

        if (this.model.is_editing) {
            ComboboxView({
                el: that.ui.id_situacao,
                url: '/api/situacao/?st_tabela=tb_tipodematerial',
                collection: comboSituacao,
                optionLabel: 'st_situacao',
                optionSelectedId: that.model.get('id_situacao').id_situacao,
                emptyOption: '[Selecione]',
                model: Backbone.Model.extend({
                    idAttribute: 'id_situacao',
                    defaults: {
                        id_situacao: 0,
                        st_situacao: ''
                    }
                })
            }, function(el, composite) {
                if (!comboSituacao && composite.collection.toJSON())
                    comboSituacao = composite.collection.toJSON();
            });
        }
    },
    events: {
        'click .actionEdit': 'actionEdit',
        'click .actionSave': 'actionSave',
        'click .actionRemove': 'actionRemove',
        'click .cancel-link': 'resetEdit'
    },
    actionEdit: function() {
        this.model.toggleEdit();
        this.render();
    },
    actionRemove: function() {
        var that = this;
        bootbox.confirm("Deseja realmente remover este registro?", function(response) {
            if (response) {
                that.model.destroy({
                    silent: true,
                    success: function() {
                        that.remove();
                        $.pnotify({
                            type: 'success',
                            title: 'Registro Removido',
                            text: 'O registro foi removido.'
                        });
                    },
                    error: function() {
                        $.pnotify({
                            type: 'error',
                            title: 'Erro',
                            text: 'Não é possivel apagar esse tipo de material.'
                        });
                    },
                    complete: loaded
                });
            }
        });
    },
    actionSave: function() {
        var that = this;

        var values = Backbone.Syphon.serialize(this);

        this.model.set(values);

        // Validacao dos campos
        if (!this.validate())
            return false;

        loading();
        this.model.save(null, {
            success: function(model, response) {
                if (response.type == 'success')
                    that.model.set(response.mensagem);
                that.actionEdit(); // Alterna para o modo de exibicao
            },
            complete: function(response) {
                response = $.parseJSON(response.responseText);
                $.pnotify(response);
                loaded();
            }
        });
    },
    validate: function() {
        var errorMsg = null;
        if (!this.model.get('st_tipodematerial'))
            errorMsg = 'Insira o nome do tipo de material.';
        else if (!this.model.get('id_situacao').id_situacao)
            errorMsg = 'Selecione uma situação.';

        if (errorMsg) {
            $.pnotify({
                type: 'warning',
                title: 'Campos Obrigatórios',
                text: errorMsg
            });
            return false;
        }

        return true;
    },
    resetEdit: function() {
        this.model.set(this.oldSettings);
        this.actionEdit();

        if (!this.model.id)
            this.remove();
    }
});

// EMPTY ITEM VIEW
var TipoMaterialEmpty = Marionette.ItemView.extend({
    template: _.template('<td colaspan="3">Nenhum registro encontrado.</td>'),
    tagName: 'tr'
});

// COLLECTION
var TipoMaterialCollection = Backbone.Collection.extend({
    model: TipoMaterialModel,
    url: '/api/tipo-material'
});

// COMPOSITE VIEW
var TipoMaterialComposite = Marionette.CompositeView.extend({
    template: '#tipo-material-template',
    collection: new TipoMaterialCollection(),
    childView: TipoMaterialView,
    emptyView: TipoMaterialEmpty,
    childViewContainer: 'tbody',
    initialize: function() {
        this.collection.fetch({ complete: loaded });
    },
    onRender: function() {
        this.verifyEmpty();
    },
    events: {
        'click .actionAdd': 'actionAdd'
    },
    actionAdd: function() {
        var newModel = new TipoMaterialModel();
        newModel.toggleEdit();
        this.collection.add(newModel);
    },
    collectionEvents: {
        add: 'verifyEmpty',
        remove: 'verifyEmpty'
    },
    verifyEmpty: function() {
        var elem = this.$el;

        if (this.collection.length) {
            elem.find('.empty-view').remove();
        } else {
            var ViewEmpty = Marionette.ItemView.extend({
                tagName: 'tr',
                className: 'empty-view',
                template: _.template('<td colspan="3">Nenhum tipo de material adicionado.</td>')
            });
            elem.find('tbody').append((new ViewEmpty()).render().$el);
        }
    }
});

// RENDER
G2S.layout.addRegions({
    table: '#tipo-material-content'
});
var TipoMaterialIni = new TipoMaterialComposite();
G2S.layout.table.show(TipoMaterialIni);

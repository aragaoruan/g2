/**
 * Created by Unyleya06 on 19/10/2015.
 */
$.ajaxSetup({async: false});

var dadosComplementaresExibicaoModel = Backbone.Model.extend({
    url: 'api/vw-pessoa-dados-complementares/',
    idAttribute: 'id_usuario',
    defaults: {
        id_usuario: '',
        id_entidade: '',
        dt_nascimento: '',
        id_pais: '',
        st_sexo: '',
        st_nomemae: '',
        st_nomepai: '',
        st_tituloeleitor: '',
        st_certificadoreservista: '',
        st_curso: '',
        st_nomeinstituicao: '',
        st_nomecompleto: '',
        st_cpf: '',
        id_titulacao: '',
        st_rg: '',
        st_orgaoexpeditor: '',
        dt_dataexpedicao: '',
        st_nivelensino: ''
    }
});

var pessoaDadosComplementaresModel = Backbone.Model.extend({
    url: 'api/pessoa-dados-complementares/',
    idAttribute: 'id_pessoadadoscomplementares',
    defaults: {
        id_pessoadadoscomplementares: null,
        id_usuario: '',
        id_tipoescola: {
            id_tipoescola: '',
            st_tipoescola: ''
        },
        dt_anoconclusao: '',
        st_nomeinstituicao: '',
        id_formaingresso: null,
        id_etnia: '',
        id_pessoacomdeficiencia: '',
        st_descricaodeficiencia: '',
        st_curso: '',
        sg_ufinstituicao: '',
        id_municipioinstituicao: ''
    }
});

var viewDadosPrevios = Marionette.ItemView.extend({
    template: '#template-conteudo',
    model: new dadosComplementaresExibicaoModel()
});

var MunicipioCollection = Backbone.Collection.extend({
    url: '/pessoa/get-municipio-by-uf',
    model: Municipio
});

var viewFormDadosComplementares = Marionette.ItemView.extend({
    template: '#template-form',
    model: new pessoaDadosComplementaresModel(),
    ui: {
        uf: '#sg_ufinstituicao',
        municipio: '#id_municipioinstituicao',
        id_formaingresso: '[name="id_formaingresso"]',
        id_etnia: '[name="id_etnia"]'
    },
    events: {
        'change @ui.uf': 'preencheMunicipio'
    },
    onRender: function () {
        var that = this;

        // Combobox Etnia
        ComboboxView({
            el: that.ui.id_etnia,
            url: '/api/etnia/',
            optionLabel: 'st_etnia',
            optionSelectedId: that.model.get('id_etnia') || '',
            emptyOption: 'Selecione',
            model: Backbone.Model.extend({
                idAttribute: 'id_etnia'
            })
        });
    },
    onShow: function () {
        if (this.model.get('sg_ufinstituicao') != undefined && this.model.get('sg_ufinstituicao')) {
            this.$el.find('#sg_ufinstituicao').val(this.model.get('sg_ufinstituicao').sg_uf);
            this.preencheMunicipio(this.model.get('sg_ufinstituicao').sg_uf);
        }
    },
    preencheMunicipio: function (uf) {
        /**
         * Get municipios by uf on event change
         * @param {object} element
         * @returns {void}
         */

        var that = this;
        loading();
        var municipioCollection = new MunicipioCollection;
        municipioCollection.url += '/sg_uf/' + this.ui.uf.val();
        municipioCollection.fetch({
            beforeSend: function () {
                that.ui.municipio.empty();
            },
            success: function () {
                new SelectView({
                    el: that.ui.municipio,
                    collection: municipioCollection,
                    childViewOptions: {
                        optionLabel: 'st_nomemunicipio',
                        optionValue: 'id_municipio',
                        optionSelected: that.model.get('id_municipioinstituicao') ? that.model.get('id_municipioinstituicao').id_municipio : null
                    }
                }).render();
            },
            complete: function () {
                loaded();
            }
        });
    }
});

var DadosComplementaresLayoutView = Marionette.LayoutView.extend({
    template: '#template-layout',
    regions: {
        autoComplete: '#autoComplete',
        dadosPreviosExibicao: '#dadosPrevios',
        conteudoForm: '#conteudoForm',
        deficienciasRegion: '#container-deficiencia'
    },
    events: {
        'submit #dadosComplementaresForm': 'submitForm',
        'change [name="id_pessoacomdeficiencia"]': 'deficienciaOnOff'
    },
    initialize: function () {
        thatLayout = this;
    },
    onShow: function () {
        componenteAutoComplete.main({
            model: VwPesquisarPessoa,
            element: $('#autoComplete')
        }, function () {
            //Quando fixa a pessoa
            thatLayout.renderizaForm();
        }, function () {
            thatLayout.limpaForm();
            //Quando limpa a pessoa.
        });
        return this;
    },
    renderizaForm: function () {
        var that = this;

        var viewDados = new viewDadosPrevios();

        viewDados.model.fetch({
            url: viewDados.model.url + componenteAutoComplete.getFixModel().id_usuario + '?id_entidade=' + G2S.loggedUser.get('id_entidade')
        }).success(function () {
            // Formata as strings de data para o formato correto.
            if (viewDados.model.get('dt_dataexpedicao')) {
                viewDados.model.set({
                    dt_dataexpedicao: formatarDataParaString(new Date(viewDados.model.get('dt_dataexpedicao').substr(0, 10)))
                });
            }

            if (viewDados.model.get('dt_nascimento')) {
                viewDados.model.set({
                    dt_nascimento: formatarDataParaString(new Date(viewDados.model.get('dt_nascimento')))
                });
            }

            /* Caso não haja cadastro prévido da data de expedição do documento/nascimento,
             * seta a propriedade no model como uma string vazia. Necessário para evitar erro de renderização da tela. */

            if (viewDados.model.get('dt_dataexpedicao') == null) {
                viewDados.model.set({
                    dt_dataexpedicao: ''
                });

            } else if (viewDados.model.get('dt_nascimento') == null) {
                viewDados.model.set({
                    'dt_nascimento': ''
                })
            }

        }).fail(function () {
            $.pnotify({
                type: 'error',
                title: 'Erro!',
                text: 'Usuário não encontrado.'
            });
        }).always(function () {
            that.dadosPreviosExibicao.show(viewDados);
        });

        var viewForm = new viewFormDadosComplementares();

        //Workaround para evitar que o template seja renderizado sempre com os valores do primeiro usuário consultado.
        viewForm.model = new pessoaDadosComplementaresModel();
        viewForm.model.fetch({
            url: viewForm.model.url + '?id_usuario=' + componenteAutoComplete.getFixModel().id_usuario
        }).success(function () {
            //Workaround para evitar que o template seja renderizado sempre com os valores do primeiro usuário consultado.
            that.conteudoForm.reset().show(viewForm);

            if (viewForm.model.get('id_pessoacomdeficiencia') == 1) {
                that.deficienciaOnOff();
            }

        }).always(function () {
            that.conteudoForm.show(viewForm);
            that.autoSelect(viewForm.model.get('id_etnia'));
        });
    },
    limpaForm: function () {
        this.dadosPreviosExibicao.reset();
        this.conteudoForm.reset();
    },
    submitForm: function (e) {
        e.preventDefault();

        var that = this;
        var model = this.conteudoForm.currentView.model;
        var values = Backbone.Syphon.serialize(this.conteudoForm.currentView);
        var deficienciasView = this.deficienciasRegion.currentView;

        values.deficiencias = [];

        // Definir corretamente as deficiencias para salvar
        if (deficienciasView) {
            values.deficiencias = deficienciasView.collection.toJSON().filter(function (item) {
                return item.bl_possuideficiencia;
            });
        }

        model.set(values);
        model.set({
            //Adiciona o id_usuario na model.
            id_usuario: this.dadosPreviosExibicao.currentView.model.get('id_usuario')
        });

        loading();
        model.save()
            .done(function (message) {
                if (message.type == 'success') {
                    $.pnotify({
                        'type': 'success',
                        'title': 'Sucesso!',
                        'text': 'Registro salvo com sucesso.'
                    });

                    if (deficienciasView) {
                        deficienciasView.initialize();
                    }
                } else if (message.type == 'error') {
                    $.pnotify({
                        'type': 'error',
                        'title': 'Erro!',
                        'text': 'Não foi possível salvar o registro.'
                    });
                }
            })
            .always(loaded);
    },
    //Desativa (ou ativa) o campo de especificação de deficiência.
    deficienciaOnOff: function () {
        if ($('[name="id_pessoacomdeficiencia"]:checked').val() == 1) {
            $('.deficiencia-content').removeClass('hide');
            $('[name="st_descricaodeficiencia"]').prop('disabled', false).prop('required', true);
            this.deficienciasRegion.show(new DeficienciaComposite());
        } else {
            $('.deficiencia-content').addClass('hide');
            $('[name="st_descricaodeficiencia"]').prop('disabled', true).prop('required', false).val('');
            this.deficienciasRegion.reset();
        }
    },
    //Preenche automaticamente os selects da tela, baseando nos dados da model.
    autoSelect: function (valEtnia) {
        $('[name="id_etnia"]').val(valEtnia);

    }
});

/**
 * Configuracões das opções de deficiência
 */

var DeficienciaCollection = Backbone.Collection.extend({
    model: Deficiencia,
    url: '/api/vw-usuario-deficiencia'
});

var DeficienciaItem = Marionette.ItemView.extend({
    template: _.template('<label class="checkbox inline"><input type="checkbox" name="deficiencias[]" value="<%= id_deficiencia %>" <%= bl_possuideficiencia ? "checked" : "" %>/><%= st_deficiencia + (st_descricao ? " (" + st_descricao + ")" : "") %></label>'),
    ui: {
        input: "input"
    },
    events: {
        "change @ui.input": "setModel"
    },
    setModel: function () {
        this.model.set('bl_possuideficiencia', this.ui.input.is(':checked'));
        this.render();
    }
});

var DeficienciaComposite = Marionette.CompositeView.extend({
    template: _.template('<div></div>'),
    collection: new DeficienciaCollection(),
    childView: DeficienciaItem,
    ui: {
        inputs: "input[type='checkbox']"
    },
    events: {
        "click @ui.inputs": "checkGroups"
    },
    initialize: function () {
        this.collection.fetch({
            beforeSend: loading,
            complete: loaded,
            data: {
                "id_usuario": componenteAutoComplete.getFixModel().id_usuario
            }
        });
    },
    onRender: function () {
        this.checkGroups();
    },
    checkGroups: function () {
        var that = this;

        this.bindUIElements();
        this.ui.inputs.prop('disabled', false);

        var groups = [
            [1, 2, 6],
            [3, 4, 6],
            [1, 3, 6]
        ];

        this.ui.inputs.filter(':checked').each(function () {
            // Pegar o ID (id_deficiencia)
            var id = parseInt($(this).val());

            // Buscar grupos que possuem o id do selecionado
            groups
                .filter(function (group) {
                    return group.indexOf(id) !== -1;
                })
                .forEach(function (group) {
                    group.forEach(function (id_deficiencia) {
                        that.ui.inputs.filter("[value='" + id_deficiencia + "']").not(':checked').prop('disabled', true);
                    });
                });
        })
    }
});

G2S.layout.content.show(new DadosComplementaresLayoutView());
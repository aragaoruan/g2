$(document).ready(function () {
    $('#data-lancamento').show();
    $('#result-lancamento').hide();

});

//Variavel utilizada para manter o valor da nota maxima que pode ser incluida.
var nuNotaMax = null;

var GlobalModel = Backbone.Model.extend({
    // idAttribute: 'id_matricula',
    idAttribute: 'id_avaliacaoaluno',
    defaults: {
        id_avaliacao: '',
        st_str: '',
        sg_uf: '',
        id_municipio: '',
        id_aplicadorprova: '',
        dt_aplicacao: '',
        st_nomecompleto: '',
        id_projetopedagogico: '',
        st_projetopedagogico: '',
        st_areaconhecimento: '',
        id_areaconhecimento: '',
        id_situacao: '',
        st_situacao: '',
        st_evolucao: '',
        nu_valor: '',
        id_disciplina: '',
        st_disciplina: '',
        st_nota: '',
        dt_cadastroavaliacao: '',
        dt_avaliacao: '',
        st_usuariocadavaliacao: '',
        st_evolucaodisciplina: '',
        st_notaead: '',
        st_notafinal: '',
        nu_notatotal: '',
        id_matricula: '',
        st_avaliacao: '',
        id_avaliacaoagendamento: '',
        st_aplicadorprova: '',
        id_avaliacaoconjuntoreferencia: '',
        id_avaliacaorecupera: '',
        bl_provaglobal: '',
        bl_agendamento: '',
        id_provasrealizadas: '',
        nu_notamax: '',
        id_avaliacaoaluno: ''
    }
});


var VwAvaliacaoAlunoColecao = Backbone.Collection.extend({
    model: GlobalModel.extend({
        idAttribute: 'id_disciplina'
    }),
    url: '/default/lancamento-por-aluno/find-aluno-avaliacao'
});
var VwAvaliacaoAlunoCollection = new VwAvaliacaoAlunoColecao();

var VwHistoricoAvaliacaoAlunoColecao = Backbone.Collection.extend({
    model: GlobalModel,
    url: '/default/lancamento-por-aluno/find-historico-aluno-avaliacao'
});
var VwHistoricoAvaliacaoAlunoCollection = new VwHistoricoAvaliacaoAlunoColecao();

var VwMatricualNotaColecao = Backbone.Collection.extend({
    model: GlobalModel.extend({
        idAttribute: 'id_matriculadisciplina'
    }),
    url: '/default/lancamento-por-aluno/retorna-matricula-nota'
});
var VwMatriculaNotaCollection = new VwMatricualNotaColecao();

var VwAvaliacaoAgendamentoColecao = Backbone.Collection.extend({
    model: GlobalModel.extend({
        idAttribute: 'id_avaliacao'
    }),
    url: '/default/lancamento-por-aluno/retorna-provas'
});

var VwAvaliacaoAgendamentoCollection = new VwAvaliacaoAgendamentoColecao();


var disciplinasView = Backbone.View.extend({
    initialize: function () {
        this.render = _.bind(this.render, this);
        this.$el.css('cursor', 'pointer');
    },
    render: function () {
        var variables = this.model.toJSON();
        var temp;

        if (this.model.get('id_avaliacaorecupera') > 0 && this.model.get('bl_provaglobal') == 0) {
            temp = _.template($('#disciplina-lancamento-prova-recuperacao').html(), variables);
        } else {
            temp = _.template($('#disciplina-lancamento-prova-final-e-global').html(), variables);
        }
        this.$el.html(temp);
        return this;
    },
    events: {
        'click #btnSalvarNotaRecuperacao': 'salvarNotaRecuperacao'
    },
    salvarNotaRecuperacao: function () {
        lancaNotaRecuperacao(this.model.get('id_avaliacaoconjuntoreferencia'));
    }
});

var historicoView = Backbone.View.extend({
    initialize: function () {
        this.render = _.bind(this.render, this);
        this.$el.css('cursor', 'pointer');
    },
    render: function () {
        var variables = this.model.toJSON();
        var temp;
        temp = _.template($('#historico-lancamento-notas').html(), variables);
        this.$el.html(temp);
        return this;
    },
    events: {}
});

var notasView = Backbone.View.extend({
    initialize: function () {
        this.render = _.bind(this.render, this);
        this.$el.css('cursor', 'pointer');
    },
    render: function () {
        var variables = this.model.toJSON();
        var temp = _.template($('#disciplina-lancamento-notas').html(), variables);
        this.$el.html(temp);
        return this;
    },
    events: {}
});

var abasListaView = Backbone.View.extend({
    initialize: function () {
        this.render = _.bind(this.render, this);
        this.carregaDados(VwAvaliacaoAgendamentoCollection.models[0].attributes.id_avaliacao, VwAvaliacaoAgendamentoCollection.models[0].attributes.id_matricula);
    },
    render: function () {
        var variables = this.model.toJSON();
        var temp = _.template('<a id="prova<%=id_avaliacao%>" class="aba" name="prova_final" data-toggle="tab"><%=st_avaliacao%></a>', variables);
        this.$el.html(temp);
        return this;
    },
    carregaDados: function (id_avaliacao, id_matricula) {
        var that = this;
        $('#tabela-retorno-pesquisa-disciplinas tbody').html('<td colspan="6">Carregando...</td>');

        var possui_recuperacao = VwAvaliacaoAgendamentoCollection.findWhere({id_avaliacaorecupera: 1});

        if (this.model.get('bl_provaglobal')) {
            $('#labelGlobal').show();
        } else {
            $('#labelGlobal').hide();
        }

        $('#lancamentoFinalGlobal').show();
        if (that.model.get('id_avaliacaorecupera')) {
            $('#lancamentoFinalGlobal').show();
        } else if (possui_recuperacao) {
            $('#lancamentoFinalGlobal').hide();
        }

        $('#ul_menu')
            .find('li').removeClass('active')
            .end()
            .find('a[id="prova' + id_avaliacao + '"]').closest('li').addClass('active');

        VwAvaliacaoAlunoCollection.fetch({
            data: {
                id_matricula: id_matricula,
                id_avaliacao: id_avaliacao
            },
            success: function () {
                var disciplinaListaView = new CollectionView({
                    collection: VwAvaliacaoAlunoCollection,
                    childViewConstructor: disciplinasView,
                    childViewTagName: 'tr',
                    el: $('#tabela-retorno-pesquisa-disciplinas tbody')
                });
                disciplinaListaView.render();
            },
            error: function () {
                $.pnotify({title: 'Erro', text: 'Erro ao tentar carregar as informações!', type: 'error'});
            },
            complete: function () {
                that.carregaHistorico(id_avaliacao, id_matricula);
                loaded();
            }
        });

    },
    carregaHistorico: function (id_avaliacao, id_matricula) {
        $('#tabela-retorno-pesquisa-historico-nota tbody').html('<td colspan="5">Carregando...</td>');

        VwHistoricoAvaliacaoAlunoCollection.fetch({
            data: {
                params: {
                    id_matricula: id_matricula,
                    id_avaliacao: id_avaliacao
                }
            },
            success: function () {
                var historicoListaView = new CollectionView({
                    collection: VwHistoricoAvaliacaoAlunoCollection,
                    childViewConstructor: historicoView,
                    childViewTagName: 'tr',
                    el: $('#tabela-retorno-pesquisa-historico-nota tbody')
                });
                historicoListaView.render();
            },
            error: function () {
                $.pnotify({title: 'Erro', text: 'Erro ao tentar carregar as informações!', type: 'error'});
            },
            complete: function () {
                loaded();
            }
        });
    },
    events: {
        "click": function () {
            this.carregaDados(this.model.get('id_avaliacao'), this.model.get('id_matricula'));
            this.carregaHistorico(this.model.get('id_avaliacao'), this.model.get('id_matricula'));
            controlaAbas(0);
            return false;
        }
    }
});


$(function() {
    var url_aplicador = '/default/aplicacao-avaliacao/retorna-aplicador-entidade-aplicacao';
    var url_municipio = '/default/util/retorna-municipio-aplicacao';

    $('#sg_uf').change(function() {
        //busca método da utilController e aguarda um retorno do mensageiro em json
        if ($(this).val()) {
            var params = {
                idParam: $("select#sg_uf option:selected").val(),
                ajax: true
            };
            comboDinamico(url_municipio, 'id_municipio', params, 'id_value', 'st_text');
            $('#id_aplicadorprova').html('<option value="">Todos</option>').show();
        } else {
            var params = {
                ajax: true
            };
            $('#id_municipio').html('<option value="">Todos</option>').show();
            $('#id_aplicadorprova').html('<option value="">Todos</option>').show();
        }
    });

    $('#id_municipio').change(function(){
        if ($(this).val()){
            var params1 = {
                sg_uf: $("select#sg_uf option:selected").val(),
                id_entidade: G2S.loggedUser.get('id_entidade')
            };
            comboDinamico(url_aplicador, 'id_aplicadorprova', params1, 'id_aplicadorprova', 'st_aplicadorprova');
        } else {
            $('#id_aplicadorprova').html('<option value="">Todos</option>').show();
        }
    })
});

$("#formPesquisaLancamentoPorAluno").off().on('submit', function () {
    $('#data-lancamento').show();
    $('#result-lancamento').hide();

    var RowGrid = Backgrid.Row.extend({
        abreLancamento: function (e) {
            that = this;
            var i = 0;
            for (var key in that.model.attributes) {
                if (i == 0) {
                    that.model.id = that.model.attributes[key];
                }
                i++;
            }
        },
        events: {
            "click": "abreLancamento"
        },
        render: function () {
            this.delegateEvents();
            return this;
        }
    });

    var containerGrid = $("#data-lancamento").hide();
    var params = $("#formPesquisaLancamentoPorAluno").serialize();


    // Validacao dos campos da busca
    if (!$('#st_str').val() && (!$('#sg_uf').val() || !$('#id_municipio').val() || !$('#id_aplicadorprova').val() || !$('#dt_aplicacao').val())) {
        $.pnotify({
            type: 'warning',
            title: 'Pesquisa',
            text: 'É necessário inserir o Nome / E-mail / CPF ou preencher UF, Município, Local e Data'
        });
        return false;
    }

    var alunos = Backbone.Model.extend({
        idAttribute: 'id_matricula'
    });
    var DataGrid = Backbone.PageableCollection.extend({
        model: alunos,
        url: "/",
        state: {
            pageSize: 15
        },
        mode: "client"
    });
    var url = '/default/lancamento-por-aluno/pesquisa-aluno-lancamento';
    containerGrid.show();
    var dataResponse;
    var dataComplete;
    $.ajax({
        dataType: 'json',
        type: 'post',
        url: url,
        data: params,
        beforeSend: function () {
            loading();
        },
        success: function (data) {
            dataResponse = data.mensagem;
            dataComplete = data;
        },
        complete: function () {
            loaded();

            containerGrid.html('');
            if (dataComplete.tipo && dataComplete.tipo == 1) {
                var dataGrid = new DataGrid(dataResponse);
                var columns = [
                    {
                        name: 'st_exibe',
                        label: 'Alunos', editable: false,
                        cell: Backgrid.Cell.extend({
                            render: function () {
                                var exibir = this.model.get("st_nomecompleto");
                                this.$el.css('cursor', 'pointer');
                                this.$el.html(exibir);
                                this.delegateEvents();
                                return this;
                            }
                        })
                    },
                    {name: 'st_areaconhecimento', label: 'Área', cell: 'string', editable: false},
                    {name: 'st_projetopedagogico', label: 'Curso', cell: 'string', editable: false},
                    {name: 'st_situacao', label: 'Situação', cell: 'string', editable: false},
                    {name: 'st_evolucao', label: 'Evolução', cell: 'string', editable: false},
                    {name: 'st_aplicadorprova', label: 'Local de Prova', cell: 'string', editable: false},
                    {name: 'dt_aplicacao', label: 'Data da Prova', cell: 'string', editable: false},
                    {
                        name: 'editar',
                        label: 'Ações',
                        editable: false,
                        cell: Backgrid.Cell.extend({
                            render: function () {
                                this.$el.html('<button class="btn btn-custom btn-small"><i class="icon-white icon-pencil"></i> Lançar notas</button>');
                                return this;
                            },
                            events: {
                                "click": "abreLancamento"
                            },
                            abreLancamento: function () {
                                this.dadosAluno(this.model);
                                this.montaAbas();
                            },
                            dadosAluno: function (data) {
                                $('#data-lancamento').hide();
                                $('#result-lancamento').show();
                                var that = this;
                                $('#idMatricula').attr({'value': this.model.get('id_matricula')});
                                nuNotaMax = this.model.get('nu_valor');
                                var variables = data.toJSON();
                                var temp = _.template($('#dados-aluno-nota').html(), variables);
                                that.el = $('#dados-aluno');
                                that.el.html(temp);
                                return this;
                            },
                            montaAbas: function () {
                                var mat = this.model.get('id_matricula');

                                var $menu = $('#ul_menu');

                                VwAvaliacaoAgendamentoCollection.fetch({
                                    data: {
                                            id_matricula: mat
                                    },
                                    success: function () {
                                        var abasView = new CollectionView({
                                            collection: VwAvaliacaoAgendamentoCollection,
                                            childViewConstructor: abasListaView,
                                            childViewTagName: 'li',
                                            el: $menu
                                        });
                                        abasView.render();
                                        $menu
                                            .append('<li><a id="listaNota" class="aba" name="lista_nota" onclick="abaNotaFinais(' + mat + ');" style="cursor: pointer;">NOTAS FINAIS</a></li>')
                                            .find('li:first').addClass('active');

                                        $menu.find('li:first').trigger('click');

                                    },
                                    error: function () {
                                        $.pnotify({title: 'Erro', text: 'Erro ao tentar montar abas!', type: 'error'});
                                    },
                                    complete: loaded
                                });
                            }
                        })
                    },
                ];
                containerGrid.html('');
                //verifica se o json retornou algum resultado
                if (dataGrid.length > 0) {
                    //monta a grid
                    var pageableGrid = new Backgrid.Grid({
                        columns: columns,
                        collection: dataGrid,
                        className: 'backgrid backgrid-selectall table table-striped table-bordered table-hover'
                    });
                    //renderiza o grid
                    containerGrid.append(pageableGrid.render().$el);
                    //configura o paginator
                    var paginator = new Backgrid.Extension.Paginator({
                        collection: dataGrid
                    });
                    //renderiza o paginator
                    containerGrid.append(paginator.render().$el);
                    dataGrid.fetch({reset: true});
                } else {
                    containerGrid.html('');
                }
            } else {
                containerGrid.html(dataComplete.text);
                $.pnotify({title: dataComplete.title, text: dataComplete.text, type: dataComplete.type});
            }
        }
    });
});

function lancarFinal() {
    var nota = $('#stNota').val();
    var avaliacao = VwAvaliacaoAlunoCollection.models[0].attributes.id_avaliacao;
    var matricula = VwAvaliacaoAlunoCollection.models[0].attributes.id_matricula;
    var ida = VwAvaliacaoAlunoCollection.models[0].attributes.id_avaliacaoagendamento;

    /**
     * Regra de validação, a nota deve ser maior igual a zero e menor que a nota maxima configurada por entidade
     */
    if (nota == '' || parseInt(nota) > nuNotaMax || parseInt(nota) < 0) {
        $.pnotify({title: 'Aviso!', text: 'Favor informar a nota entre 0 e ' + nuNotaMax, type: 'warning'});//mensagem de retorno
    } else {
        if (VwAvaliacaoAlunoCollection.models[0].attributes.nu_notamax < nota) {
            $.pnotify({title: 'Aviso!', text: 'A nota lançanda não pode ser maior que a máxima permitida', type: 'warning'});

        } else {
            loading();
            $.ajax({
                url: '/default/lancamento-por-aluno/lancar-nota-final',
                type: 'post',
                dataType: 'json',
                data: {id_matricula: matricula, st_nota: nota, id_avaliacao: avaliacao, id_avaliacaoagendamento: ida},
                success: function (response) {
                    loaded();
                    $.pnotify({title: response['result'].title, text: response['result'].text, type: response['result'].type});//mensagem de retorno
                    $.pnotify({title: response['resultEmail'].title, text: response['resultEmail'].text, type: response['resultEmail'].type});//mensagem de retorno
                },
                error: function (response) {
                    loaded();
                    $.pnotify({title: response.title, text: response.text, type: response.type});//mensagem de retorno
                },
                complete: function () {
                    VwAvaliacaoAlunoCollection.fetch({
                        data: {
                            id_matricula: matricula,
                            id_avaliacao: avaliacao
                        },
                        success: function () {
                            var disciplinaListaView = new CollectionView({
                                collection: VwAvaliacaoAlunoCollection,
                                childViewConstructor: disciplinasView,
                                childViewTagName: 'tr',
                                el: $('#tabela-retorno-pesquisa-disciplinas tbody')
                            });
                            disciplinaListaView.render();
                        },
                        error: function () {
                            $.pnotify({title: 'Erro', text: 'Erro ao tentar carregar as informações!', type: 'error'});
                        },
                        complete: function () {
                            loaded();
                            $('#tabela-retorno-pesquisa-disciplinas tbody').html('<td colspan="6">Carregando...</td>');

                            VwAvaliacaoAlunoCollection.fetch({
                                data: {
                                    id_matricula: matricula,
                                    id_avaliacao: avaliacao
                                },
                                success: function () {
                                    var disciplinaListaView = new CollectionView({
                                        collection: VwAvaliacaoAlunoCollection,
                                        childViewConstructor: disciplinasView,
                                        childViewTagName: 'tr',
                                        el: $('#tabela-retorno-pesquisa-disciplinas tbody')
                                    });
                                    disciplinaListaView.render();
                                },
                                error: function () {
                                    $.pnotify({title: 'Erro', text: 'Erro ao tentar carregar as informações!', type: 'error'});
                                },
                                complete: function () {
                                    carregaHistorico(matricula, avaliacao);
                                    loaded();
                                }
                            });
                        }
                    });
                }
            });
        }
    }
}

function lancaNotaRecuperacao(idref) {
    var nota = $('#idNota' + idref).val();
    var avaliacao = VwAvaliacaoAlunoCollection.models[0].attributes.id_avaliacao;
    var matricula = VwAvaliacaoAlunoCollection.models[0].attributes.id_matricula;
    var ida = VwAvaliacaoAlunoCollection.models[0].attributes.id_avaliacaoagendamento;

    if (nota == '') {
        $.pnotify({title: 'Aviso!', text: 'Favor informar a Nota!', type: 'warning'});//mensagem de retorno                  
    } else {
        if (VwAvaliacaoAlunoCollection.models[0].attributes.nu_notamax < nota) {
            $.pnotify({title: 'Aviso!', text: 'A nota lançanda não pode ser maior que a máxima permitida', type: 'warning'});

        } else {
            loading();
            $.ajax({
                url: '/default/lancamento-por-aluno/lancar-nota-final',
                type: 'post',
                dataType: 'json',
                data: {id_matricula: matricula, st_nota: nota, id_avaliacao: avaliacao, id_avaliacaoagendamento: ida, id_avaliacaoconjuntoreferencia: idref}, success: function (response) {
                    $.pnotify({title: response.title, text: response.text, type: response.type});//mensagem de retorno          
                },
                error: function (response) {
                    $.pnotify({title: response.title, text: response.text, type: response.type});//mensagem de retorno
                },
                complete: function () {
                    loaded();
                    $('#tabela-retorno-pesquisa-disciplinas tbody').html('<td colspan="6">Carregando...</td>');

                    VwAvaliacaoAlunoCollection.fetch({
                        data: {
                            id_matricula: matricula,
                            id_avaliacao: avaliacao
                        },
                        success: function () {
                            var disciplinaListaView = new CollectionView({
                                collection: VwAvaliacaoAlunoCollection,
                                childViewConstructor: disciplinasView,
                                childViewTagName: 'tr',
                                el: $('#tabela-retorno-pesquisa-disciplinas tbody')
                            });
                            disciplinaListaView.render();
                        },
                        error: function () {
                            $.pnotify({title: 'Erro', text: 'Erro ao tentar carregar as informações!', type: 'error'});
                        },
                        complete: function () {
                            carregaHistorico(matricula, avaliacao);
                            loaded();
                        }
                    });
                }
            });
        }
    }
}

function voltarPesquisa() {
    $('#result-lancamento').hide();
    $('#data-lancamento').show();
}

function abaNotaFinais(id_matricula) {
    $('#tabela-retorno-pesquisa-notas tbody').html('<td colspan="4">Carregando...</td>');
    $('#tabela-retorno-pesquisa-disciplinas tbody').html('<td colspan="6">Carregando...</td>');

    $('#labelGlobal').hide();
    $('#lancamentoFinalGlobal').hide();

    $('#ul_menu')
        .find('li').removeClass('active')
        .end()
        .find('a[id="listaNota"]').closest('li').addClass('active');

    VwMatriculaNotaCollection.fetch({
        data: {
            params: {
                id_matricula: id_matricula
            }
        },
        success: function () {
            var notasListaView = new CollectionView({
                collection: VwMatriculaNotaCollection,
                childViewConstructor: notasView,
                childViewTagName: 'tr',
                el: $('#tabela-retorno-pesquisa-notas tbody')
            });
            notasListaView.render();
        },
        error: function () {
            $.pnotify({title: 'Erro', text: 'Erro ao tentar carregar as informações!', type: 'error'});
        },
        complete: function () {
            loaded();
            controlaAbas(1);
        }
    });
}

function controlaAbas(lista) {
    $.each($('.aba'), function () {
        if (lista == 1) {
            $('#tabela-retorno-pesquisa-disciplinas').hide();
            $('#tabela-retorno-pesquisa-notas').show();
            //$('#lancamentoFinalGlobal').hide();
        } else {
            $('#tabela-retorno-pesquisa-disciplinas').show();
            $('#tabela-retorno-pesquisa-notas').hide();
            //$('#lancamentoFinalGlobal').show();

        }
    });
}

function fecharModal() {
    $('#pesquisaHistorico').modal('hide');
}

function carregaHistorico(id_matricula, id_avaliacao) {
    $('#tabela-retorno-pesquisa-historico-nota tbody').html('<td colspan="5">Carregando...</td>');

    VwHistoricoAvaliacaoAlunoCollection.fetch({
        data: {
            params: {
                id_matricula: id_matricula,
                id_avaliacao: id_avaliacao
            }
        },
        success: function () {
            var historicoListaView = new CollectionView({
                collection: VwHistoricoAvaliacaoAlunoCollection,
                childViewConstructor: historicoView,
                childViewTagName: 'tr',
                el: $('#tabela-retorno-pesquisa-historico-nota tbody')
            });
            historicoListaView.render();
        },
        error: function () {
            $.pnotify({title: 'Erro', text: 'Erro ao tentar carregar as informações!', type: 'error'});
        },
        complete: function () {
            loaded();
        }
    });
}

function comboDinamico(url, idCampo, params, id, value) {
    $.getJSON(url, params, function (dados) {
        var options = '<option value="">--</option>';
        for (var i = 0; i < dados.length; i++) {
            options += '<option value="' + dados[i][id] + '">' + dados[i][value] + '</option>';
        }
        $('#' + idCampo).html(options).show();
    });
}
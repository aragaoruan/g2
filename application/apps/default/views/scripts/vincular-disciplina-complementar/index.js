var viewInstanciada = false;

var matriculas = false;
var idEvolucao = false;
/* MODEL*/
var MatriculasCollection = Backbone.Collection.extend({
    model: Backbone.Model.extend({
        defaults: {
            id_matricula: '',
            st_projetopedagogico: ''
        }
    })
});
var DisciplinasCollection = Backbone.Collection.extend({
    model: Backbone.Model.extend({
        defaults: {
            id_matricula: '',
            st_projetopedagogico: ''
        }
    })
});

var viewGeral;

var DisciplinasVinculadasCollection = Backbone.Collection.extend({
    model: Backbone.Model.extend({
        defaults: {
            id_matricula: '',
            st_projetopedagogico: '',
            dt_matriculavinculada: ''
        }
    })
});

var HistoricoCollection = Backbone.Collection.extend({
    model: Backbone.Model.extend({
        defaults: {
            st_historicovincularmatricula: '',
            dt_historicovincularmatricula: '',
            st_usuario: ''
        }
    })
});

(function () {
    var HtmlCell = Backgrid.HtmlCell = Backgrid.Cell.extend({

        /** @property */
        className: 'html-cell',

        initialize: function () {
            Backgrid.Cell.prototype.initialize.apply(this, arguments);
        },

        render: function () {
            this.$el.empty();
            var rawValue = this.model.get(this.column.get('name'));
            var formattedValue = this.formatter.fromRaw(rawValue, this.model);
            this.$el.append(formattedValue);
            this.delegateEvents();
            return this;
        }
    });
}).call(this);

/*TABELA RESULTADO VINCULAR*/
var columnsTableVincular = [
    {
        name: 'id_matricula', // JSON Field
        label: "",
        editable: false,
        cell: 'html',
        formatter: {
            fromRaw: function (value, model) {
                if (idEvolucao === EVOLUCAO.TB_MATRICULA.CURSANDO) {
                    return "<input type='checkbox'  name='id_disciplinacomplementar[]' value='" + value + "'>";
                } else {
                    return "";
                } 
                
            }
        }
    },
    {
        name: 'st_projetopedagogico', // JSON Field
        label: 'Vincular disciplina complementar na matrícula',
        editable: false,
        cell: 'string'
    }
];
/*TABELA RESULTADO VINCULAR*/
var columnsTableModulos = [

    {
        name: 'st_projetopedagogico', // JSON Field
        label: 'Disciplina',
        editable: false,
        cell: 'string'
    },
    {
        name: 'dt_matriculavinculada', // JSON Field
        label: 'Data da Vinculação',
        editable: false,
        cell: 'string'
    },
    {
        name: 'id_matricula', // JSON Field
        label: "Ação",
        editable: false,
        cell: 'html',
        formatter: {
            fromRaw: function (value) {

                if (idEvolucao === EVOLUCAO.TB_MATRICULA.CURSANDO) {
                    return "<button class='btn btn-danger btnDesvincularMatricula' " +
                        "onclick='desvincularMatricula(this)' type='button' data-id=" + value + ">" +
                        "<i class='icon-trash icon-white'></i> Desvincular</button>";
                } else {
                    return "";
                }

            }
        }
    }
];
/*TABELA HISTORICO*/
var columnsTableHistorico = [
    {
        name: 'st_historicovincularmatricula', // JSON Field
        label: "Módulo Complementar",
        editable: false,
        cell: 'string'
    },
    {
        name: 'dt_historicovincularmatricula', // JSON Field
        label: 'Data',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_usuario', // JSON Field
        label: 'Usuário',
        editable: false,
        cell: 'string'
    }
];

var viewVincularDisciplinaComplementar = Marionette.LayoutView.extend({
    el: $("#container-filtros"),
    relatorioLogView: null,

    initialize: function () {
    },
    render: function () {
        this.buscarMatricula();
        return this;
    },
    ui: {
        selectMatricula: "#select-matricula",
        btnVincularMatriculas: "#btnVincularMatriculas",
        btnDesvincularMatricula: ".btnDesvincularMatricula"
    },
    events: {
        "change @ui.selectMatricula": "getModulosAdicionados",
        "click @ui.btnVincularMatriculas": "vincularMatricula",
        "click @ui.btnDesvincularMatricula": "desvincularMatricula"
    },
    getPessoa: function () {
        return componenteAutoComplete.getFixModel();
    },
    getModulosAdicionados: function () {
        var that = this;

        var idMatricula = parseInt($('#select-matricula').val());

        matriculas.each(
            function (data) {
                if (data.attributes.id_matricula === idMatricula) {
                    idEvolucao = data.attributes.id_evolucao;
                }

            }
        );

        if (idEvolucao !== EVOLUCAO.TB_MATRICULA.CURSANDO) {
            this.$el.find('#btnVincularMatriculas').hide().attr('disabled', 'disabled');
        } else {
            this.$el.find('#btnVincularMatriculas').show().removeAttr('disabled');
        }

        //Aba Historico
        that.getHistorico();
        if (idMatricula === 'none') {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Selecione uma matrícula.'
            });
            $("#container-modulos").addClass('hide');
            $("#table_disciplinas").addClass('hide');
            return false;
        }

        var disciplinasVinculadasCollection = new DisciplinasVinculadasCollection();
        disciplinasVinculadasCollection.url = '/vincular-disciplina-complementar/retornar-matricula-complementar-vinculada?id_matricula=' + idMatricula;
        disciplinasVinculadasCollection.fetch({
            success: function () {
                that.getMatriculasComplementares();
                $("#container-modulos").removeClass('hide');
                $("#table_disciplinas").removeClass('hide');
                var tableModulos = new Backgrid.Grid({
                    el: "#container-modulos table",
                    className: 'table table-bordered table-hover backgrid',
                    columns: columnsTableModulos,
                    collection: disciplinasVinculadasCollection,
                    emptyText: "Nenhuma disciplina complementar vinculada para a matrícula selecionada!"
                });
                tableModulos.render();

            }
        });
    },
    vincularMatricula: function () {
        var that = this;
        var idMatricula = $('#select-matricula').val();
        var idDisciplinacomplementar = [];

        $("#table_disciplinas input[type='checkbox']:checked").each(function () {
            idDisciplinacomplementar.push($(this).val());
        });

        if (!idDisciplinacomplementar.length) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Selecione uma disciplina complementar.'
            });
            return false;
        }
        var data = {'id_matricula': idMatricula, 'id_disciplinacomplementar': idDisciplinacomplementar};

        $.post('/vincular-disciplina-complementar/vincular-matriculas', data).success(function (data) {
            $.pnotify({
                type: 'success',
                title: 'Atenção!',
                text: data.mensagem
            });

            that.getModulosAdicionados();
            //that.getMatriculasComplementares();
            return true;
        }).fail(function (xhr, data) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: data
            });
            return false;
        });


    },
    getMatriculasComplementares: function () {
        var pessoa = this.getPessoa();
        var idUsuario = pessoa.id;
        var disciplinasCollection = new DisciplinasCollection();
        disciplinasCollection.url = '/vincular-disciplina-complementar/retorna-matricula-complementar?id_usuario=' + idUsuario;
        disciplinasCollection.fetch({
            reset: true,
            beforeSend: function () {
                loading();
            },
            complete: function () {
                loaded();
            },
            success: function (data) {
                //Grid Disciplinas complementares
                var tableVincular = new Backgrid.Grid({
                    el: "#table_disciplinas table",
                    className: 'table table-bordered table-hover backgrid',
                    columns: columnsTableVincular,
                    collection: disciplinasCollection,
                    emptyText: "Sem disciplina complementar para vincular!"
                });
                tableVincular.render();
                if (data.length) {
                    $("#btnVincularMatriculas").removeClass('hide');
                } else {
                    if (!$("#btnVincularMatriculas").hasClass('hide')) {
                        $("#btnVincularMatriculas").addClass('hide');
                    }
                }
                return true;
            }

        });
    },
    getHistorico: function () {
        //table_historico
        var idMatricula = $('#select-matricula').val();

        var historicoCollection = new HistoricoCollection();
        historicoCollection.url = '/vincular-disciplina-complementar/retornar-historico?id_matricula=' + idMatricula;
        historicoCollection.fetch({
            success: function () {
                $('#container_historico').removeClass('hide');

                var tableHistorico = new Backgrid.Grid({
                    el: "#table_historico table",
                    className: 'table table-bordered table-hover backgrid',
                    columns: columnsTableHistorico,
                    collection: historicoCollection,
                    emptyText: "-"
                });
                tableHistorico.render();
            }
        });
    },
    buscarMatricula: function () {
        var that = this;
        var pessoa = this.getPessoa();
        var idUsuario = pessoa.id;
        var matriculasCollection = new MatriculasCollection();
        matriculasCollection.url = '/vincular-disciplina-complementar/retornar-matriculas?id_usuario=' + idUsuario;
        matriculasCollection.fetch({
            reset: true,
            beforeSend: function () {
                loading();
            },
            complete: function () {
                loaded();
            },
            success: function (data) {
                if (data.length) {
                    that.getMatriculasComplementares();
                    $("#container-filtros").removeClass('hide');
                    //Como Matriculas
                    var viewSelectMatriculas = new SelectView({
                        el: "#select-matricula",
                        collection: matriculasCollection,
                        childViewOptions: {
                            optionLabel: 'st_projetopedagogico',
                            optionValue: 'id_matricula',
                            optionSelected: ''
                        }
                    });
                    matriculas = data;
                    viewSelectMatriculas.render();
                } else {
                    $.pnotify({
                        type: 'warning',
                        title: 'Atenção!',
                        text: 'Nenhuma matrícula (cursando) encontrada.'
                    });
                    return false;
                }
            }
        });
    }
});

//Desvincular Matricula
function desvincularMatricula(id) {
    var view = viewGeral;
    $('#confirmaDesvincularMatricula').modal().one('click', '#btnConfirmaDesvincular', function () {

        var idMatricula = $('#select-matricula').val();
        var idDisciplinacomplementar = $(id).attr('data-id');
        var data = {'id_matricula': idMatricula, 'id_disciplinacomplementar': idDisciplinacomplementar};

        $.post('/vincular-disciplina-complementar/desvincular-matricula', data).done(function (data) {

            $.pnotify({
                type: 'success',
                title: 'Atenção!',
                text: data.mensagem
            });
            view.getModulosAdicionados();

            $('#confirmaDesvincularMatricula').modal('hide');
            return true;
        }).fail(function (xhr, data) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: data
            });
            return false;
        });


    });
}


/* FORMULARIO AUTOCOMPLETE*/
var renderizaautoComplete = function () {

    //componenteAutoComplete.clearFixModel();

    componenteAutoComplete.main({
        model: VwPesquisarPessoa,
        element: $("#autoComplete"),
        entidadesMesmaHolding: true
    }, function () {
       if(!viewInstanciada){
           viewGeral = new viewVincularDisciplinaComplementar();
           viewInstanciada = true;
       }
        viewGeral.render();
    }, function () {
        $('#select-matricula').html('<option value="none">Seleciona a matrícula</option>');
        $("#container-filtros").addClass('hide');
        $("#btnVincularMatriculas").addClass('hide');
        $("#container-modulos").addClass('hide');
        $("#table_disciplinas").addClass('hide');
        $("#container_historico").addClass('hide');
    });
};

renderizaautoComplete();
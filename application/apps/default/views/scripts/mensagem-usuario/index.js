var naoLidas = [], clonedCollection = [], collectionMensagem;

var ModelGlobalMensagemAluno = Backbone.Model.extend({
    defaults: {
        'id_mensagem': null,
        'id_evolucao': null,
        'st_mensagem': '',
        'bl_importante': '',
        'dt_enviar': '',
        'dt_cadastromsg': '',
        'st_texto': '',
        'st_nomeentidade': '',
        'id_usuario': null,
        'id_entidade': null,
        'id_situacao': null,
        'id_enviomensagem': null,
        'id_enviodestinatario': null
    },
    url: '/api/mensagem-usuario/'
});

/****************************************************
 * Collections
 ****************************************************/

var MensagensAlunoCollection = Backbone.Collection.extend({
    url: '/api/mensagem-usuario/',
    model: ModelGlobalMensagemAluno
});

var ListaMensagens = Marionette.ItemView.extend({
    template: '#templateRowMensagem',
    tagName: 'tr',
    initialize: function () {
        this.$el.attr('id', this.model.get('id_mensagem'));
    },
    ui: {
        "starImportante": '.importante i',
        "itemMensagem": '.abrirMensagem'
    },
    events: {
        'click @ui.starImportante': 'setImportante',
        'click @ui.itemMensagem': 'abrirMensagem'
    },
    setImportante: function (e) {
        $(e.currentTarget).toggleClass('icon-star-empty').toggleClass('icon-star');

        this.model.save({
            bl_importante: this.model.get('bl_importante') ? false : true,
            id_evolucao: this.model.get('id_evolucaodestinatario'),
            atualizar_mensagem: true
        }, {
            beforeSend: function () {
                loading();
            },
            success: function () {
                loaded();
            }
        });

    },
    abrirMensagem: function (e) {
        layout.containerNaoLidas.empty();

        var id_mensagem = this.model.get('id_mensagem');
        var mensagem = collectionMensagem.where({id_mensagem: id_mensagem});

        layout.container.show(new ItemMensagem({
            model: mensagem[0]
        }));

        mensagem[0].save({id_evolucao: 43},
            {
                beforeSend: function () {
                    loading();
                },
                success: function () {
                    loaded();
                }
            });
    }
});

var EmptyView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: _.template('<td colspan="4">Sem mensagens</td>')
});

var MensagemComposite = Marionette.CompositeView.extend({
    template: '#templateContainerMensagem',
    childView: ListaMensagens,
    childViewContainer: 'tbody',
    emptyView: EmptyView
});

var ItemMensagem = Marionette.ItemView.extend({
    template: '#templateItemMensagem',
    tagName: 'div',
    className: 'well well-small'
});

var PessoaJuridicaDadosEntidadeLayout = Marionette.LayoutView.extend({
    template: '#templateMensagem',
    tagName: 'div',
    className: 'container-fluid',
    ui: {
        "mensagem": '.mensagem-link a'
    },
    events: {
        'click @ui.mensagem': 'loadMensagem'
    },
    regions: {
        menu: '#menuMensagem',
        container: '#containerMensagem',
        containerNaoLidas: '#containerMensagemNaoLidas'
    },
    onShow: function () {
        this.loadMensagem();
    },
    loadMensagem: function (e) {
        var tipo = 0, importante = 0, obj = null;

        if (e != undefined) {
            e.preventDefault();
            obj = $(e.currentTarget);
            tipo = obj.data('tipo');
            importante = obj.data('importante');
            this.ui.mensagem.removeClass('active');
            obj.addClass('active');
        }

        collectionMensagem = new MensagensAlunoCollection();

        if (tipo != 0)
            collectionMensagem.url += '?id_evolucaodestinatario=' + tipo;

        if (importante != 0)
            collectionMensagem.url += '?bl_importante=' + importante;

        var that = this;
        collectionMensagem.fetch({
            beforeSend: function () {
                loading();
            },
            complete: function () {
                loaded();
            },
            success: function () {
                clonedCollection = [];
                naoLidas = [];
                collectionMensagem.each(function (model) {
                    if (model.get('id_evolucaodestinatario') == 42) {
                        naoLidas.push(model);
                    } else {
                        clonedCollection.push(model);
                    }
                });

                if (tipo != 42) {
                    that.container.show(new MensagemComposite({collection: new Backbone.Collection(clonedCollection)}));
                } else {
                    that.container.empty();
                }

                that.containerNaoLidas.show(new MensagemComposite({
                    template: '#templateContainerMensagemNaoLida',
                    collection: new Backbone.Collection(naoLidas),
                    emptyView: Marionette.ItemView.extend({
                        tagName: 'tr',
                        template: _.template('<td colspan="4"><strong>Parabéns! Todas as suas mensagens foram lidas :)</strong></td>')
                    })
                }));
            }


        });
    }
});

/*********************************************
 ************** INICIALIZAÇÃO ****************
 *********************************************/

var layout = new PessoaJuridicaDadosEntidadeLayout();

G2S.layout.content.show(layout);
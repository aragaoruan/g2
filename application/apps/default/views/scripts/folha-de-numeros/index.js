/**
 * Created by helder.silva on 05/01/2015.
 */

/**
 * ******************************************************
 * ******                  VARIAVEIS              *******
 * ******************************************************
 */
var entidadeSelect;
var viewRelatorio;
var RelatorioQuantitativo = Backbone.Model.extend({
    defaults: {
        dt_atual: '',
        dt_inicio: '',
        id_entidade: '',
        id_evolucao: '',
        id_produto: '',
        id_turma: '',
        nu_cargahoraria: '',
        st_projetopedagogico: '',
        id_projetopedagogico: '',
        nu_dia1: '',
        nu_dia2: '',
        nu_dia3: '',
        nu_dia4: '',
        nu_dia5: '',
        nu_dia6: '',
        st_evolucao: '',
        st_nomeentidade: '',
        st_produto: '',
        st_turma: ''
    }
});


var ModelRelatorio = Backbone.Model.extend({
    id_projeto: '',
    st_projeto: '',
    produtos: [],
    dia_1:'',
    dia_2:'',
    dia_3:'',
    dia_4:'',
    dia_5:'',
    dia_6:'',
    total1: 0,
    total2: 0,
    total3: 0,
    total4: 0,
    total5: 0,
    total6: 0
});
/**
 * ******************************************************
 * ******               COLLECTION                *******
 * ******************************************************
 */

//var EvolucaoCollection = Backbone.Collection.extend({
//    url: '/api/evolucao/?st_tabela=tb_turma',
//    model: Evolucao,
//    initialize: function () {
//        //this.fetch()
//    }
//});

var EntidadeCollection = Backbone.Collection.extend({
    url: '/folha-de-numeros/retorna-entidade',
    model: VwEntidadeRecursiva,
    initialize: function () {
        //this.fetch()
    }
});

var TurmaCollection = Backbone.Collection.extend({
    url: '/folha-de-numeros/retorna-turma/id/' + entidadeSelect,
    model: Turma,
    initialize: function () {
        //this.fetch()
    }
});

var RelatorioCollection = Backbone.Collection.extend({
    model: RelatorioQuantitativo
});
/**
 * ******************************************************
 * ******                  LAYOUT                 *******
 * ******************************************************
 */


var FolhaLayout = Marionette.LayoutView.extend({
    template: '#folha_layout',
    tagname: 'div',
    className: 'containder-fluid',
    regions: {
        relatorio: '#tabela-relatorio',
        itensRelatorio: '#tabelaDadosRelatorio'
    },
    ui: {
        'idEntidade': '#id_entidade',
        'idTurma': '#id_turma',
        'idEvolucao': "#id_evolucao",
        'dtInicio': "#dt_filtro"
    },
    initialize: function () {
        thisLayout = this;
    },
    onShow: function () {
        this.renderizaSelectEntidade();
        //this.renderizaSelectEvolucao();
        loaded()
    },
    //renderizaSelectEvolucao: function () {
    //    var that = this;
    //    var collection = new EvolucaoCollection();
    //    collection.fetch({
    //        success: function () {
    //            var view = new SelectView({
    //                el: that.ui.idEvolucao,        // Elemento da DOM
    //                collection: collection,      // Instancia da collection a ser utilizada
    //                childViewOptions: {
    //                    optionLabel: 'st_evolucao', // Propriedade da Model que será utilizada como label do select
    //                    optionValue: 'id_evolucao', // Propriedade da Model que será utilizada como id do option do select
    //                    optionSelected: null       // ID da option que receberá o atributo "selected"
    //                }
    //            });
    //            view.render();
    //        }
    //
    //    });
    //},
    renderizaSelectEntidade: function () {
        loading();
        var that = this;
        var collectionEntidade = new EntidadeCollection();
        collectionEntidade.fetch({
            success: function () {
                var view = new SelectView({
                    el: that.ui.idEntidade,        // Elemento da DOM
                    collection: collectionEntidade,      // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_nomeentidade', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_entidade', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: null       // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();
                loaded();
            }
        });

    },
    renderizaSelectTurma: function () {
        $('#btn_imprimir_relatorio').prop('disabled',true);
        $('#btn_gerar_xls').prop('disabled',true);
        var that = this;
        var valor = this.ui.idEntidade.val();
        if (valor) {
            $('#id_turma').val("");
            $('#id_turma').prop('disabled', false);
            collect = new TurmaCollection();
            collect.url = '/folha-de-numeros/retorna-turma/id/' + valor;
            collect.fetch({
                beforeSend: function () {
                    loading();
                    that.ui.idTurma.empty();
                    that.ui.idTurma.prepend("<option value=''>Selecione</option>");
                },
                success: function () {
                    var view = new SelectView({
                        el: that.ui.idTurma,        // Elemento da DOM
                        collection: collect,      // Instancia da collection a ser utilizada
                        childViewOptions: {
                            optionLabel: 'st_turma', // Propriedade da Model que será utilizada como label do select
                            optionValue: 'id_turma', // Propriedade da Model que será utilizada como id do option do select
                            optionSelected: null       // ID da option que receberá o atributo "selected"
                        }
                    });
                    view.render();

                },
                complete: function () {
                    loaded();
                }
            });
        } else {
            $('#id_turma').val("");
            $('#id_turma').prop('disabled', true);
        }
    },
    gerarRelatorio: function () {
        if (this.validaFiltros()) {
            var that = this;
            loading();
            //layoutFolha.relatorio.show(new relatorioView);
            //layoutFolha.itensRelatorio.show(new RelatorioCollectionView);

            var collectionResultRelatorio = Backbone.Collection.extend({
                model: ModelRelatorio
            });
            var collectionFetch = new collectionResultRelatorio();
            collectionFetch.url = '/folha-de-numeros/retorna-relatorio';
            collectionFetch.fetch({
                data: {
                    'id_entidade': that.ui.idEntidade.val(),
                    'id_turma': that.ui.idTurma.val(),
                    //'id_evolucao': that.ui.idEvolucao.val(),
                    'dt_inicio': that.ui.dtInicio.val()
                },
                type: 'POST',

                success: function () {
                    if(collectionFetch.models==""){
                        $.pnotify({
                            title: 'Aviso!',
                            text: 'Nenhum dado encontrado para gerar relatório.',
                            type: 'warning'
                        });

                        $('#btn_imprimir_relatorio').prop('disabled',true);
                        $('#btn_gerar_xls').prop('disabled',true);

                        $('#container-table').hide('fast');

                        loaded();

                    }else{

                        $('#btn_imprimir_relatorio').prop('disabled',false);
                        $('#btn_gerar_xls').prop('disabled',false);

                        var CollectionTable = Backbone.Collection.extend({
                            model: ModelRelatorio
                        });
                        var produtoView = Marionette.ItemView.extend({
                            tagName: 'tr',
                            template: '#item-relatorioa',
                            onBeforeRender: function () {
                                var v1 = parseInt(this.model.get('qtd_GD'));
                                var v2 = parseInt(this.model.get('qtd_GD2'));
                                var v3 = parseInt(this.model.get('qtd_GD3'));
                                var v4 = parseInt(this.model.get('qtd_GD4'));
                                var v5 = parseInt(this.model.get('qtd_GD5'));
                                var v6 = parseInt(this.model.get('qtd_GD6'));

                                var v1_1 = parseInt(this.model.get('qtd_FP'));
                                var v2_2 = parseInt(this.model.get('qtd_FP2'));
                                var v3_3 = parseInt(this.model.get('qtd_FP3'));
                                var v4_4 = parseInt(this.model.get('qtd_FP4'));
                                var v5_5 = parseInt(this.model.get('qtd_FP5'));
                                var v6_6 = parseInt(this.model.get('qtd_FP6'));
                                var garantia_duo = v1 + v2 + v3 + v4 + v5 + v6;
                                var free_pass = v1_1 + v2_2 + v3_3 + v4_4 + v5_5 + v6_6;
                                this.model.set('garantia_duo', garantia_duo);
                                this.model.set('free_pass', free_pass);
                            }
                        });

                        var projetoProdutos = Marionette.CompositeView.extend({
                            childView: produtoView,
                            template: "#script_tabela_relatorio",
                            childViewContainer: 'tbody',
                            initialize: function () {
                                var collection = new CollectionTable(this.model.get('produtos'));
                                this.collection = collection;
                                this.calculaCoisa();
                            },
                            onShow: function () {
                                debug(this.model);
                                return this;
                            },
                            calculaCoisa: function () {
                                var total1 = 0, total2 = 0, total3 = 0, total4 = 0, total5 = 0, total6 = 0,
                                    total7 = 0, total8 = 0, total9 = 0, total10 = 0, total11 = 0, total12 = 0,
                                    total13 = 0, total14 = 0, total15 = 0, total16 = 0, total17 = 0, total18 = 0;

                                this.collection.each(function (model, i) {
                                    t1 = 0, t2 = 0, t3 = 0, t4 = 0, t5 = 0, t6 = 0, t7 = 0, t8 = 0, t9 = 0, t10 = 0,
                                        t10 = 0, t11 = 0, t12 = 0, t13 = 0, t14 = 0, t15 = 0, t16 = 0, t17 = 0, t18 = 0;
                                    if (model.get('qtd_GD6')) {
                                        t18 = parseInt(model.get('qtd_GD6'))
                                        total18 += t18;
                                    }
                                    ;
                                    if (model.get('qtd_GD5')) {
                                        t17 = parseInt(model.get('qtd_GD5'));
                                        total17 += t17;
                                    }
                                    if (model.get('qtd_GD4')) {
                                        t16 = parseInt(model.get('qtd_GD4'));
                                        total16 += t16;
                                    }
                                    if (model.get('qtd_GD3')) {
                                        t15 = parseInt(model.get('qtd_GD3'));
                                        total15 += t15;
                                    }
                                    if (model.get('qtd_GD2')) {
                                        t14 = parseInt(model.get('qtd_GD2'));
                                        total14 += t14;
                                    }
                                    if (model.get('qtd_GD')) {
                                        t13 = parseInt(model.get('qtd_GD'));
                                        total13 += t13;
                                    }
                                    if (model.get('qtd_FP6')) {
                                        t12 = parseInt(model.get('qtd_FP6'));
                                        total12 += t12;
                                    }
                                    if (model.get('qtd_FP5')) {
                                        t11 = parseInt(model.get('qtd_FP5'));
                                        total11 += t11;
                                    }
                                    if (model.get('qtd_FP4')) {
                                        t10 = parseInt(model.get('qtd_FP4'));
                                        total10 += t10;
                                    }
                                    if (model.get('qtd_FP3')) {
                                        t9 = parseInt(model.get('qtd_FP3'));
                                        total9 += t9;
                                    }
                                    if (model.get('qtd_FP2')) {
                                        t8 = parseInt(model.get('qtd_FP2'));
                                        total8 += t8;
                                    }
                                    if (model.get('qtd_FP')) {
                                        t7 = parseInt(model.get('qtd_FP'));
                                        total7 += t7;
                                    }
                                    t6 = parseInt(model.get('nu_dia6'));
                                    t5 = parseInt(model.get('nu_dia5'));
                                    t4 = parseInt(model.get('nu_dia4'));
                                    t3 = parseInt(model.get('nu_dia3'));
                                    t2 = parseInt(model.get('nu_dia2'));
                                    t1 = parseInt(model.get('nu_dia1'));
                                    total6 += t6;
                                    total5 += t5;
                                    total4 += t4;
                                    total3 += t3;
                                    total2 += t2;
                                    total1 += t1;
                                    model.set('totalTurma', t1+t2+t3+t4+t5+t6);
                                });
                                this.model.set('total1', total1);
                                this.model.set('total2', total2);
                                this.model.set('total3', total3);
                                this.model.set('total4', total4);
                                this.model.set('total5', total5);
                                this.model.set('total6', total6);
                                this.model.set('total7', total7);
                                this.model.set('total8', total8);
                                this.model.set('total9', total9);
                                this.model.set('total10', total10);
                                this.model.set('total11', total11);
                                this.model.set('total12', total12);
                                this.model.set('total13', total13);
                                this.model.set('total14', total14);
                                this.model.set('total15', total15);
                                this.model.set('total16', total16);
                                this.model.set('total17', total17);
                                this.model.set('total18', total18);
                                this.model.set('total', total1 + total2 + total3 + total4 + total5 + total6 +
                                    total7 + total8 + total9 + total10 + total11 + total12,
                                    total13 + total14 + total5 + total16 + total17 + total18);

                            }
                        });
                        var projetosView = Marionette.CompositeView.extend({
                            childView: projetoProdutos,
                            template: _.template('<div class="corpo"></div>'),
                            childViewContainer: '.corpo'
                        });
                        viewRelatorio = new projetosView({
                            el: that.$el.find('#container-table'),
                            collection: collectionFetch
                        });
                        $('#container-table').show();
                        viewRelatorio.render();
                        loaded()
                    }

                }
            });
        }

    },
    validaFiltros: function () {
        flag = true;
        //if(!$('#id_evolucao').val()){
        //    $.pnotify({
        //        title: 'Aviso!',
        //        text: 'Selecione uma evolução.',
        //        type: 'warning'
        //    });
        //    $('#div-combo-evolucao').addClass('control-group error');
        //    flag = false;
        //}
        //else{
        //    $('#div-combo-evolucao').removeClass('control-group error');
        //}

        if(!$('#dt_filtro').val()){
            $.pnotify({
                title: 'Aviso!',
                text: 'Preencha uma data inicial.',
                type: 'warning'
            });
            $('#div-data-filtro').addClass('control-group error');
            flag = false;
        }else{
            $('#div-data-filtro').removeClass('control-group error');
        }
        return flag;
    },
    gerarXLS: function(){
        that = this;
        if(this.validaFiltros()){
            var data = {
                'id_entidade': that.ui.idEntidade.val(),
                'id_turma': that.ui.idTurma.val(),
                'id_evolucao': that.ui.idEvolucao.val(),
                'dt_inicio': that.ui.dtInicio.val()
            }
            var url = '/folha-de-numeros/gerar-xls/?tipo=xls&id_entidade=' + that.ui.idEntidade.val() + '&id_turma=' + that.ui.idTurma.val() + '&id_evolucao=' + that.ui.idEvolucao.val() + '&dt_inicio=' + that.ui.dtInicio.val();
            window.open(url, '_blank');
        }
    },
    imprimirRelatorio: function(){
        that = this;
        if(this.validaFiltros()){
            var data = {
                'id_entidade': that.ui.idEntidade.val(),
                'id_turma': that.ui.idTurma.val(),
                'id_evolucao': that.ui.idEvolucao.val(),
                'dt_inicio': that.ui.dtInicio.val()
            }
            var url = '/folha-de-numeros/gerar-xls/?tipo=htm&id_entidade='+that.ui.idEntidade.val()+'&id_turma='+that.ui.idTurma.val()+'&id_evolucao='+that.ui.idEvolucao.val()+'&dt_inicio='+that.ui.dtInicio.val();
            window.open(url,'_blank');


        }
    },
    bloqueiaBotoes: function(){
        $('#btn_imprimir_relatorio').prop('disabled',true);
        $('#btn_gerar_xls').prop('disabled',true);
    },
    events: {
        'change #id_entidade': 'renderizaSelectTurma',
        'change #id_turma': 'bloqueiaBotoes',
        'click #btn_gerar_relatorio': 'gerarRelatorio',
        'click #btn_gerar_xls': 'gerarXLS',
        'click #btn_imprimir_relatorio': 'imprimirRelatorio'
    }
})

/**
 * ******************************************************
 * ******                 ITEMVIEW                *******
 * ******************************************************
 */
var tabelaRelatorioView = Marionette.ItemView.extend({
    template: '#item-relatorio',
    tagName: 'tr',
    ui: {},
    onRender: function () {
    }
});

var relatorioView = Marionette.ItemView.extend({
    template: '#script_tabela_relatorio',
    ui: {},
    onRender: function () {
    }

});

/**
 * ******************************************************
 * ******               COLLECTIONVIEW            *******
 * ******************************************************
 */
var RelatorioCollectionView = Marionette.CollectionView.extend({
    template: '#tabela-relatorio',
    el: '#tabelaDadosRelatorio',
    childView: tabelaRelatorioView
});


/**
 * ******************************************************
 * ******              COMPOSITEVIEW              *******
 * ******************************************************
 */







var layoutFolha = new FolhaLayout();
G2S.show(layoutFolha);
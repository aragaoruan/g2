/**
 * Transformando a aba preparacao da entidade em uma funcionalidade
 * Created by Vinicius Avelino Alcantara <vinicius.alcantara@unyleya.com.br>
 * @since: 01/09/2015
 */

//TAB PREPARACAO ENTIDADE
// MODEL


$.ajaxSetup({async: false});
var comboSituacao = [];
$.get('/api/situacao/?st_tabela=tb_itemativacaoentidade', function (response) {
    comboSituacao = response;
});

/*var EntidadeView = Marionette.ItemView.extend({
 tagName: 'tr',
 template: '#entidade-view',
 events: {
 'click .actionEdit': 'actionEdit'
 },
 actionEdit: function( e ) {
 e.preventDefault();
 var that = this;

 G2S.layout.content.show(new DepartamentoComposite({
 id_entidade: that.model.get('id_entidade')
 }));
 }
 });*/

var EntidadeCollection = Backbone.Collection.extend({
    url: '/api/entidade'
});
/*var EntidadeComposite = Marionette.CompositeView.extend({
 template: '#entidade-composite',
 collection: new EntidadeCollection(),
 childView: EntidadeView,
 childViewContainer: 'tbody',
 initialize: function() {
 loading();
 this.collection.fetch({
 data: {
 order_by: 'st_nomeentidade',
 bl_ativo: 1
 },
 complete: loaded
 });
 }
 });*/

var DepartamentoView = Marionette.ItemView.extend({
    tagName: 'li',
    template: '#departamento-view',
    initialize: function (options) {
        this.model.set(options);
    },
    events: {
        click: 'renderPreparacao'
    },
    renderPreparacao: function () {
        var that = this;
        var preparacaoIni = new PreparacaoComposite({
            //id_entidade: that.model.get('id_entidade'),
            id_departamento: that.model.get('id_departamento')
        });
        $('#preparacao-container').html(preparacaoIni.render().$el);
    }
});
var DepartamentoCollection = Backbone.Collection.extend({
    url: '/api/departamento'
});
var DepartamentoComposite = Marionette.CompositeView.extend({
    template: '#departamento-composite',
    collection: new DepartamentoCollection(),
    childView: DepartamentoView,
    childViewContainer: 'ul',
    onShow: function () {
        this.$el.find('li:first a').trigger('click');
    },

    /*actionBack: function( e ) {
     e.preventDefault();
     G2S.layout.content.show(new EntidadeComposite());
     },
     */
    initialize: function (options) {
        this.childViewOptions = options;
        this.collection.fetch();
    }
});

var PreparacaoModel = Backbone.Model.extend({
    idAttribute: 'id_itemativacao',
    defaults: {
        id_itemativacao: null,
        st_itemativacao: null,
        id_entidade: null,
        id_itemativacaoentidade: null,
        id_situacao: null,
        st_situacao: null,
        id_usuarioatualizacao: null,
        dt_atualizacao: null,
        // SOMENTE INFO
        st_descricao: null,
        st_responsavel: null,
        st_caminhosistema: null
    },
    url: function () {
        return '/api/vw-item-ativacao-entidade/' + (this.id || '');
    }
});

var PreparacaoSituacaoCollection = Backbone.Collection.extend({});

// ITEM VIEW
var PreparacaoView = Marionette.ItemView.extend({
    model: new PreparacaoModel(),
    template: '#preparacao-view',
    tagName: 'tr',
    onRender: function () {
        var that = this;
        var elem = this.$el;

        // Definir o id_situacao 150 ("NOK", padrao), caso nao tenha registro no banco
        if (!Number(this.model.get('id_situacao')))
            this.model.set('id_situacao', 150);

        //CHECKBOXES SITUACAO
        var Situacoes = Marionette.CollectionView.extend({
            collection: new PreparacaoSituacaoCollection(comboSituacao),
            initialize: function () {
                this.childView = Marionette.ItemView.extend({
                    template: _.template('<input class="id_situacao" type="radio" name="id_situacao[' + that.model.get('id_itemativacao') + ']" value="<%= id_situacao %>"> <%= st_situacao %>'),
                    tagName: 'label',
                    className: 'radio',
                    onRender: function () {
                        var elem = this.$el;
                        var id_sitacao = Number(this.model.get('id_situacao'));

                        if (Number(that.model.get('id_situacao')) != 150)
                            elem.find('input').attr('disabled', 'disabled');
                        if (Number(that.model.get('id_situacao')) == id_sitacao)
                            elem.find('input').attr('checked', 'checked');
                    }
                });
            }
        });

        var SituacoesIni = new Situacoes();
        elem.find('#situacao-container').html(SituacoesIni.render().$el);

        elem.removeAttr('class');
        if (Number(this.model.get('id_situacao')) == 151)
            elem.addClass('success');
        else if (Number(this.model.get('id_situacao')) == 152)
            elem.addClass('warning');
    },
    events: {
        'change .id_situacao': 'ationSavePreparacao'
    },
    ationSavePreparacao: function (e) {
        var that = this;
        var input = $(e.target);

        bootbox.confirm('Deseja alterar a situação deste registro? Esta ação não pode ser desfeita.', function (response) {
            if (response) {
                loading();
                that.model.save({
                    id_situacao: Number(input.val()) || 150 // 150 = NOK
                }, {
                    success: function (model, response) {
                        if (response.type == 'success' && response.codigo) {
                            that.model.set(response.mensagem);
                            that.render();
                        }
                    },
                    complete: function (response) {
                        response = $.parseJSON(response.responseText);
                        $.pnotify(response);
                        loaded();
                    }
                });
            } else {
                that.render();
            }
        });
    }
});

// COLLECTION
var PreparacaoCollection = Backbone.Collection.extend({
    model: PreparacaoModel,
    url: '/api/vw-item-ativacao-entidade?order_by=st_itemativacao'
});

var ViewEmpty = Marionette.ItemView.extend({
    tagName: 'tr',
    className: 'empty-view',
    template: _.template('<td colspan="4">Nenhum registro encontrado.</td>')
});

// COMPOSITE VIEW
var PreparacaoComposite = Marionette.CompositeView.extend({
    template: '#preparacao-template',
    collection: new PreparacaoCollection(),
    childView: PreparacaoView,
    childViewContainer: 'tbody',
    emptyView: ViewEmpty,
    initialize: function (options) {
        loading();
        this.collection.fetch({
            data: options,
            complete: loaded
        });
    }
});


G2S.layout.content.show(new DepartamentoComposite());
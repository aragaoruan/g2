var viewLayout,
    viewGrid,
    viewPaginator,
    collectionPesquisa;

var LayoutView = Marionette.LayoutView.extend({
    template: '#layout-template',
    regions: {
        pesquisa: '#pesquisa-container',
        paginacao: '#pesquisa-paginacao'
    },
    ui: {
        form: 'form',
        dt_agendamento_min: '#dt_agendamento_min',
        dt_agendamento_max: '#dt_agendamento_max',
        st_nomecompleto: '#st_nomecompleto',
        st_cpf: '#st_cpf',
        id_entidade: '#id_entidade',
        id_aplicadorprova: '#id_aplicadorprova',
        nu_anoaplicacao: '#nu_anoaplicacao',
        nu_mesaplicacao: '#nu_mesaplicacao',
        id_avaliacaoaplicacao: '#id_avaliacaoaplicacao',
        id_projetopedagogico: '#id_projetopedagogico',
        st_responsavel: '#st_responsavel',
        // PAGER
        nu_registros: '#nu_registros',
        //BUTTONS
        btnPrint: '.btnPrint',
        btnXls: '.btnXls',
        // OTHER
        totalCount: '.total-box'
    },
    events: {
        "change @ui.id_aplicadorprova": "loadComboAno",
        "change @ui.id_aplicadorprova,@ui.nu_anoaplicacao": "loadComboMes",
        "change @ui.id_aplicadorprova,@ui.nu_anoaplicacao,@ui.nu_mesaplicacao": "loadComboData",

        'change @ui.dt_agendamento_min': 'resetDtMax',
        'change @ui.dt_agendamento_max': 'validateDtRange',

        'submit form': 'actionSearch',
        'click @ui.btnPrint': 'actionExport',
        'click @ui.btnXls': 'actionExport'
    },
    onRender: function () {
        // Mascaras
        this.ui.st_cpf.mask('999.999.999-99', {
            autoclear: false
        });

        // Carregar combo Local de Prova
        this.loadComboLocal();

        // Carregar combo Curso
        this.loadComboCurso();
    },
    loadComboLocal: function () {
        var that = this;

        this.ui.id_aplicadorprova.html("<option value=\"\">Carregando...</option>");

        var params = {
            id_entidadepai: G2S.loggedUser.get("id_entidade")
        };

        // Carregar combo Local de Prova
        ComboboxView({
            el: this.ui.id_aplicadorprova,
            url: "/api/aplicador-prova-entidade?" + $.param(params),
            optionLabel: 'st_aplicadorprova',
            model: Backbone.Model.extend({
                idAttribute: 'id_aplicadorprova'
            }),
            onChange: function (model) {
                if (model) {
                    that.ui.id_entidade.val(model.get("id_entidade"));
                    that.loadComboCurso();
                }
            }
        }, function () {
            that.ui.id_aplicadorprova.prop("disabled", false);
        });
    },
    loadComboCurso: function () {
        var params = {
            id_entidade: this.ui.id_entidade.val()
        };

        // Carregar combo Curso
        ComboboxView({
            el: this.ui.id_projetopedagogico,
            url: "/default/relatorio-alunos-agendados/retorna-projetos?" + $.param(params),
            optionLabel: 'st_projetopedagogico',
            emptyOption: 'Todos',
            model: Backbone.Model.extend({
                idAttribute: 'id_projetopedagogico'
            })
        });
    },
    loadComboAno: function () {
        var that = this;

        var params = {
            order: 'nu_anoaplicacao',
            sort: 'DESC',
            id_aplicadorprova: this.ui.id_aplicadorprova.val()
        };

        if (!params.id_aplicadorprova) {
            this.ui.nu_anoaplicacao
                .html("<option value=\"\">[Selecione o Local de Prova]</option>")
                .prop("disabled", true)
                .val("");

            return false;
        }

        this.ui.nu_anoaplicacao.html("<option value=\"\">Carregando...</option>");

        // Carregar combo Ano
        ComboboxView({
            el: this.ui.nu_anoaplicacao,
            url: '/default/relatorio-alunos-agendados/retorna-dados-combos/?' + $.param(params),
            optionLabel: 'nu_anoaplicacao',
            emptyOption: '[Selecione]',
            model: Backbone.Model.extend({
                idAttribute: 'nu_anoaplicacao'
            })
        }, function () {
            that.ui.nu_anoaplicacao.prop('disabled', false);
        });
    },
    loadComboMes: function () {
        var that = this;

        var params = {
            order: 'nu_mesaplicacao',
            sort: 'ASC',
            id_aplicadorprova: this.ui.id_aplicadorprova.val(),
            nu_anoaplicacao: this.ui.nu_anoaplicacao.val()
        };

        if (!params.nu_anoaplicacao) {
            this.ui.nu_mesaplicacao
                .html("<option value=\"\">[Selecione o Ano]</option>")
                .prop("disabled", true)
                .val("");

            return false;
        }

        this.ui.nu_mesaplicacao.html("<option value=\"\">Carregando...</option>");

        // Carregar combo Mês
        ComboboxView({
            el: this.ui.nu_mesaplicacao,
            url: '/default/relatorio-alunos-agendados/retorna-dados-combos/?' + $.param(params),
            optionLabel: 'st_mesaplicacao',
            emptyOption: '[Selecione]',
            model: Backbone.Model.extend({
                idAttribute: 'nu_mesaplicacao'
            })
        }, function () {
            that.ui.nu_mesaplicacao.prop('disabled', false);
        });
    },
    loadComboData: function () {
        var that = this;

        var params = {
            order: 'dt_aplicacao',
            sort: 'DESC',
            id_aplicadorprova: this.ui.id_aplicadorprova.val(),
            nu_anoaplicacao: this.ui.nu_anoaplicacao.val(),
            nu_mesaplicacao: this.ui.nu_mesaplicacao.val(),
            id_entidade: this.ui.id_entidade.val()
        };

        if (!params.nu_mesaplicacao) {
            this.ui.id_avaliacaoaplicacao
                .html("<option value=\"\">[Selecione o Mês]</option>")
                .prop("disabled", true)
                .val("");

            return false;
        }

        this.ui.id_avaliacaoaplicacao.html("<option value=\"\">Carregando...</option>");

        // Carregar combo Data da Aplicação
        ComboboxView({
            el: this.ui.id_avaliacaoaplicacao,
            url: '/default/relatorio-alunos-agendados/retorna-dados-combos/?' + $.param(params),
            // optionLabel: 'dt_aplicacao',
            optionLabelConstruct: function (model) {
                return model.get('dt_aplicacao') + ' (' + model.get('nu_relatoriosagendamento') + ')';
            },
            emptyOption: 'Todas',
            model: Backbone.Model.extend({
                idAttribute: 'id_avaliacaoaplicacao'
            })
        }, function () {
            that.ui.id_avaliacaoaplicacao.prop('disabled', false).trigger('change');
        });
    },
    resetDtMax: function () {
        this.ui.dt_agendamento_max.val('');
    },
    validate: function () {
        var valid_date = this.ui.dt_agendamento_min.val() && this.ui.dt_agendamento_max.val();
        var valid_name = this.ui.st_nomecompleto.val().trim();
        var valid_cpf = this.ui.st_cpf.val().trim();
        var valid_local = this.ui.id_aplicadorprova.val() || this.ui.nu_anoaplicacao.val() || this.ui.nu_mesaplicacao.val();

        // Com os valores de cada um, fazer a validação específica de cada
        if (valid_date) {
            return this.validateDtRange();
        } else if (valid_name) {
            return this.validateName();
        } else if (valid_cpf) {
            return this.validateCpf();
        } else if (valid_local) {
            return this.validateLocal();
        }

        var bl_valid = valid_date || valid_name || valid_cpf || valid_local;

        if (!bl_valid) {
            $.pnotify({
                type: 'warning',
                title: 'Campos Obrigatórios',
                text: 'Preencha a data de realização do agendamento, o nome, o CPF ou a data da aplicação'
            });
        }

        return bl_valid;
    },
    validateDtRange: function () {
        var min = this.ui.dt_agendamento_min.val();
        var max = this.ui.dt_agendamento_max.val();

        var diff = daysDiff(min, max);

        if (diff < 0) {
            $.pnotify({
                type: 'warning',
                title: 'Data Incorreta',
                text: 'A segunda data deve ser maior que a primeira'
            });
            this.ui.dt_agendamento_max.val('');
            return false;
        } else if (G2S.loggedUser.get('id_linhadenegocio') !== LINHA_DE_NEGOCIO.POS_GRADUACAO && (diff > 90)) {
            $.pnotify({
                type: 'warning',
                title: 'Data Incorreta',
                text: 'O intervalo entre as datas deve ser no máximo 90 dias'
            });
            this.ui.dt_agendamento_max.val('');
            return false;
        } else if (G2S.loggedUser.get('id_linhadenegocio') === LINHA_DE_NEGOCIO.POS_GRADUACAO && (diff > 365)) {
            $.pnotify({
                type: 'warning',
                title: 'Data Incorreta',
                text: 'O intervalo entre as datas deve ser no máximo 1 ano'
            });
            this.ui.dt_agendamento_max.val('');
            return false;
        }
        return true;
    },
    validateName: function () {
        var value = this.ui.st_nomecompleto.val().trim();

        if (value.length < 6) {
            $.pnotify({
                type: 'warning',
                title: 'Aviso',
                text: 'Prezado Usuário, Digite no mínimo 6 caracteres no campo Nome'
            });
            return false;
        }

        return true;
    },
    validateCpf: function () {
        var value = this.ui.st_cpf.val().replace(/\D/g, '');

        if (value.length != 11) {
            $.pnotify({
                type: 'warning',
                title: 'CPF Inválido',
                text: 'Prezado Usuário, CPF inválido! Digite o CPF corretamente'
            });
            return false;
        }

        return true;
    },
    validateLocal: function () {
        if (!this.ui.id_aplicadorprova.val() || !this.ui.nu_anoaplicacao.val() || !this.ui.nu_mesaplicacao.val()) {
            $.pnotify({
                type: 'warning',
                title: 'Local de Prova',
                text: 'Para pesquisar pelo local de prova é necessário incluir ano, mês e data de aplicação'
            });
            return false;
        }

        return true;
    },
    actionSearch: function (e) {
        e && e.preventDefault ? e.preventDefault() : '';

        var that = this;

        if (!this.validate()) {
            return false;
        }

        var data = Backbone.Syphon.serialize(this);

        // Backgrid CLASSES
        var PesquisaModel = Backbone.Model.extend({
            idAttribute: 'id_avaliacaoagendamento'
        });

        var PesquisaCollection = Backbone.PageableCollection.extend({
            model: PesquisaModel,
            url: '/default/relatorio-alunos-agendados/pesquisar/',
            mode: 'client', // Paginacao no lado do cliente (via JS)
            state: {
                pageSize: parseInt(data.nu_registros)
            }
        });

        collectionPesquisa = new PesquisaCollection();

        viewGrid = new Backgrid.Grid({
            className: 'backgrid table table-striped table-bordered table-hover',
            columns: columns,
            collection: collectionPesquisa
        });

        viewPaginator = new Backgrid.Extension.Paginator({
            collection: collectionPesquisa
        });

        // Region Pesquisa
        this.pesquisa.show(viewGrid);
        this.paginacao.show(viewPaginator);

        this.fetched = collectionPesquisa.fetch({
            data: data,
            reset: true,
            beforeSend: loading,
            complete: function () {
                that.ui.totalCount.show().find('span').html(collectionPesquisa.fullCollection.length);

                if (!collectionPesquisa.length) {
                    $.pnotify({
                        type: 'warning',
                        title: 'Aviso',
                        text: 'Nenhum registro encontrado.'
                    })
                }

                loaded();
            }
        });
    },
    actionExport: function (e) {
        e && e.preventDefault ? e.preventDefault() : '';

        if (!this.fetched) {
            this.actionSearch();
        }

        var data = Backbone.Syphon.serialize(this);

        var url = '/default/relatorio-alunos-agendados/pesquisar/?' + $.param(data);
        var win;

        if (e && e.currentTarget && $(e.currentTarget).hasClass('btnPrint')) {
            url += '&type=html';
            win = window.open(url, '_blank');
        } else {
            url += '&type=xls';
            win = window.open(url, '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=780, height=200, top=0, left=0');
        }

        if (!win) {
            $.pnotify({
                type: 'warning',
                title: 'Aviso',
                text: 'Seu navegador bloqueou o popup.'
            });
        }
    }
});

// CONFIG GRID COLUMNS
var columns = [
    {
        name: 'st_nomecompleto', // JSON Field
        label: 'Nome', // Label que aparecera no Header da tabela
        editable: false,
        cell: 'string'
    },
    {
        name: "st_cpf",
        label: "CPF",
        editable: false,
        cell: "string"
    },
    {
        name: 'st_email',
        label: 'E-mail',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_telefone',
        label: 'Telefone Principal',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_telefonealternativo',
        label: 'Telefone Secundário',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_projetopedagogico',
        label: 'Curso',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_possuiprova',
        label: 'Possui Prova',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_aplicadorprova',
        label: 'Local de Prova',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_horarioaula',
        label: 'Horário',
        editable: false,
        cell: 'string'
    },
    {
        name: 'dt_aplicacao',
        label: 'Data da Aplicação',
        editable: false,
        cell: 'string'
    },
    {
        name: 'dt_agendamento',
        label: 'Data da Realização do Agendamento',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_responsavel',
        label: 'Responsável pelo Agendamento',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_avaliacao',
        label: 'Tipo de Avaliação',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_disciplinas',
        label: 'Disciplinas',
        editable: false,
        cell: Backgrid.Cell.extend({
            render: function () {
                $(this.el).html('<div style="white-space: nowrap">' + this.model.get('st_disciplinas') + '</div>');
                return this;
            }
        })
    },
    {
        name: 'bl_temprovaintegrada',
        label: 'Prova Integrada',
        editable: false,
        cell: 'string'
    }
];

if (G2S.loggedUser.get('id_linhadenegocio') === LINHA_DE_NEGOCIO.POS_GRADUACAO) {
    columns.push({
        name: 'st_presenca',
        label: 'Status do Aluno',
        cell: 'string'
    });
    columns.push({
        name: 'st_documentacao',
        label: 'Documentação',
        cell: 'string'
    });
}


// Render layout template
viewLayout = new LayoutView();
G2S.layout.content.show(viewLayout);
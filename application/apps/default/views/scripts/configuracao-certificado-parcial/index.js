var DisciplinaCertificadoParcial = Backbone.Collection.extend({
    model: Backbone.Model,
    url: "/configuracao-certificado-parcial/retorna-disciplina"
});
var TextoSistema = Backbone.Collection.extend({
    model: Backbone.Model,
    url: "/configuracao-certificado-parcial/retorna-texto-sistema"
});

var RowGrid = Backgrid.Row.extend({
    initialize: function (options) {
        RowGrid.__super__.initialize.apply(this, arguments);
    }
});

var configuracaoCertificacaoParcialLayout = Marionette.LayoutView.extend({
    template: '#template-configuracao-certificacao-parcial',
    model: new Backbone.Model,
    ui: {
        stCertificadoparcial: '#st_certificadoparcial',
        dtIniciovigencia: '#dt_iniciovigencia',
        dtFimvigencia: '#dt_fimvigencia',
        textoSistema: '#texto-sistema',
        btnSalvar: '#btn-salvar'
    },
    events: {
        'click @ui.btnSalvar': 'salvarCertificado',
        'onblur @ui.dtFimvigencia': 'verificarData'
    },
    initialize: function () {
        if (G2S.editModel) {
            this.model.set(G2S.editModel);
            this.collection = new Backbone.Collection(this.model.get('disciplinas'));
        }
        return this;
    }, onRender: function () {
        this.carregarTextoSistema();
        this.carregarDisciplinas();
    },
    salvarCertificado: function () {

        if (this.validate()) {
            $.ajax({
                dataType: "json",
                type: "post",
                url: "/configuracao-certificado-parcial/salvar-certificado-parcial",
                data: {
                    "disciplinas": this.collection.toJSON(),
                    "st_certificadoparcial": this.ui.stCertificadoparcial.val(),
                    "dt_iniciovigencia": this.ui.dtIniciovigencia.val(),
                    "dt_fimvigencia": this.ui.dtFimvigencia.val(),
                    "id_textoSistema": this.ui.textoSistema.val(),
                    "id_certificadoparcial": this.model.get("id_certificadoparcial")
                },
                beforeSend: function () {
                    loading();
                },
                success: function (data) {
                    if (data.type === 'success') {
                        $.pnotify({
                            type: 'success',
                            title: 'Sucesso',
                            text: data.mensagem
                        });
                        window.location.href = '/#/pesquisa/CadastroConfiguracaoCertificadoParcial';
                    } else {
                        $.pnotify({
                            type: 'error',
                            title: 'Erro',
                            text: data.mensagem
                        });
                    }
                    loaded();
                }
            });
        }


    },
    carregarDisciplinas: function () {

        if (this.model.get('bl_gerado')) {

            this.ui.textoSistema.attr("disabled", true);
            this.ui.dtIniciovigencia.attr("disabled", true);
            this.ui.stCertificadoparcial.attr("disabled", true);

            this.ui.textoSistema.attr("disabled", true);
            var Disciplinas = Backbone.PageableCollection.extend({
                model: Backbone.Model,
                url: "/configuracao-certificado-parcial/retornar-disciplinas",
                mode: "client",
                state: {
                    pageSize: 10
                }
            });

            var data = {
                id_certificadoparcial: this.model.get('id_certificadoparcial')
            };

            var disciplinas = new Disciplinas();
            disciplinas.fetch({
                data: data,
                beforeSend: function () {
                    loading();
                }.bind(this),
                complete: loaded,
                success: function (collection) {
                    var pageableGridLog = new Backgrid.Grid({
                        className: 'backgrid table table-bordered',
                        columns: this.getColumns(),
                        collection: collection,
                        emptyText: "Nenhum registro encontrado",
                        row: RowGrid
                    });

                    var paginator = new Backgrid.Extension.Paginator({
                        collection: collection
                    });
                    this.$el.find('#container-DataGrid').show();

                    var $elemento = this.$el.find('#template-data-grade');
                    $elemento.empty();
                    $elemento.append(pageableGridLog.render().$el);

                    $elemento.append(paginator.render().el);
                }.bind(this)
            });

        } else {

            var disciplinaCertificado = new DisciplinaCertificadoParcial();
            disciplinaCertificado.fetch({
                beforeSend: function () {
                    loading();
                }.bind(this),
                complete: loaded,
                success: function (collection) {
                    var myItem = new SideBySideView({
                        collections: {
                            left: collection,
                            right: this.collection
                        },
                        label: 'st_disciplina',
                        label_search: 'id_disciplina',
                        fetch: false,
                        addAll: true,
                        globalSearch: true,
                        idAttributeGlobal: 'id_disciplina'
                    });
                    this.$el.find('.diciplina').html(myItem.render().$el);
                }.bind(this)

            });
        }
    },
    getColumns: function () {
        return [
            {
                name: 'st_disciplina',
                label: 'Disciplinas',
                editable: false,
                cell: 'string'
            }
        ];
    },
    carregarTextoSistema: function () {
        var textoSistema = new TextoSistema();
        textoSistema.fetch({
            beforeSend: function () {
                loading();
            }.bind(this),
            complete: loaded,
            success: function (collection) {
                var viewSelectTextoSistema = new SelectView({
                    el: "#texto-sistema",
                    collection: collection,
                    childViewOptions: {
                        optionLabel: 'st_textosistema',
                        optionValue: 'id',
                        optionSelected: this.model.get('id_textosistema')
                    }
                });

                viewSelectTextoSistema.render();
            }.bind(this)

        });
    },
    validate: function () {
        var error = null;

        if (this.ui.stCertificadoparcial.val() === '' ||
            this.ui.stCertificadoparcial.val() === null) {

            error = 'Informe o nome do certificado parcial !';

        } else if (
            this.ui.dtIniciovigencia.val() === '' ||
            this.ui.dtIniciovigencia.val() === null) {

            error = 'Informe o período da vigência inicial !';

        } else if (this.ui.textoSistema.val() === '' ||
            this.ui.textoSistema.val() === null) {
            error = 'Selecione um texto de sistema para o certificado parcial !';
        } else if (this.collection.length === 0) {
            error = 'Adicione pelo menos uma disciplina para salvar o certificado parcial !';

        }
        else if (this.verificarData()) {
            error = 'Data Fim da Vigencia deve ser maior que Data Inicio da Vigencia !';
        }

        if (error) {
            $.pnotify({
                type: 'warning',
                title: 'Obrigatório',
                text: error
            });
            return false;
        }

        return true;
    },
    verificarData: function () {
        var dtIniciovigencia = this.ui.dtIniciovigencia.val();
        var dtFimvigencia = this.ui.dtFimvigencia.val();

        if (dtFimvigencia === null || dtFimvigencia === '') {

            return false;

        } else if (dtFimvigencia !== null || dtFimvigencia !== '') {
            var dtaIniciovigencia = parseInt(dtIniciovigencia.split("/")[2]
                .toString() + dtIniciovigencia.split("/")[1]
                .toString() + dtIniciovigencia.split("/")[0]
                .toString());
            var dtaFimvigencia = parseInt(dtFimvigencia.split("/")[2]
                .toString() + dtFimvigencia.split("/")[1]
                .toString() + dtFimvigencia.split("/")[0]
                .toString());

            if ((dtaIniciovigencia < dtaFimvigencia) || (dtaIniciovigencia === dtaFimvigencia)) {
                return false;
            } else {
                return true;
            }
        }

        return true;
    }
});
G2S.show(new configuracaoCertificacaoParcialLayout);

/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * *                             MODELS                                * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 */

var SelectModel = Backbone.Model.extend({});

var PABoasVindas = Backbone.Model.extend({
    url: function() {
        return this.id ? '/api/p-a-boas-vindas/' + this.id : '/api/p-a-boas-vindas';
    },
    idAttribute: 'id_pa_boasvindas',
});
var PAMensagemInformativa = Backbone.Model.extend({
    url: function() {
        return this.id ? '/api/p-a-mensagem-informativa/' + this.id : '/api/p-a-mensagem-informativa';
    },
    idAttribute: 'id_pa_mensageminformativa',
});

var PADadosContato = Backbone.Model.extend({
    url: function() {
        return this.id ? '/api/p-a-dados-contato/' + this.id : '/api/p-a-dados-contato';
    },
    idAttribute: 'id_pa_dadoscontato',
});

var PADadosCadastrais = Backbone.Model.extend({
    url: function() {
        return this.id ? '/api/p-a-dados-cadastrais/' + this.id : '/api/p-a-dados-cadastrais';
    },
    idAttribute: 'id_pa_dadoscadastrais',
});
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * *                        COLLECTIONS                                * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 */

var SelectCollection = Backbone.Collection.extend({
    model: SelectModel,
    url: ''
});

/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * *                             VIEWS                                 * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 */

var TagView = Backbone.View.extend({
    tagName: 'span',
    className: 'label',
    hasCombos: false,
    initialize: function() {
        _.bindAll(this, 'render');
    },
    render: function() {
        var tag = _.template('<%=text%>', this.model.toJSON());
        this.el.innerHTML = tag;
        return this;
    },
    events: {
        'click': 'adicionaTag'
    },
    adicionaTag: function() {
        tinymce.activeEditor.execCommand('mceInsertContent', false, this.model.get('text') + ' ');
    }
});

var SelectView = Backbone.View.extend({
    tagName: 'option',
    initialize: function() {
        // _.bindAll(this, 'render');
    },
    render: function() {
        this.$el.attr('value', this.model.get('id')).text(this.model.get('text'));
        return this;
    }

});

var PrimeiroAcessoView = Backbone.View.extend({
    el: '#main-primeiro-acesso',
    currentView: 'boas-vindas',
    variaveisCollection: new SelectCollection(),
    assuntoCoPaiCollection: new SelectCollection(),
    subAssuntoCoCollection: new SelectCollection(),
    categoriaOcorrenciaCollection: new SelectCollection(),
    initialize: function(options) {
        if (options.currentView !== undefined && options.currentView != '') {
            this.currentView = options.currentView;
        }

        //popula a colecao de variveis
        this.variaveisCollection.url = '/textos/get-variaveis-by-categoria/idTextoCategoria/9';
        this.variaveisCollection.fetch();

        //popula a colecao de assuntos 
        this.assuntoCoPaiCollection.url = '/primeiro-acesso/find-assuntos-pai';
        this.assuntoCoPaiCollection.fetch();

        //popula a coelcao de ocorrencias
        this.categoriaOcorrenciaCollection.url = '/primeiro-acesso/find-categorias-ocorrencia';
        this.categoriaOcorrenciaCollection.fetch();

        this.render();
    },
    render: function() {
        var menuTemplate = _.template($('#menu-boas-vindas-template').html());
        var elTemplate = '';
        switch (this.currentView) {
            case '':
            case 'boas-vindas':
                this.model = new PABoasVindas();
                elTemplate = _.template($("#boas-vindas-template-edit").html());
                this.hasCombos = false;
                break;
            case 'dados-contato':
                this.model = new PADadosContato();
                elTemplate = _.template($('#dados-contato-template-edit').html());
                this.hasCombos = true;
                break;
            case 'dados-cadastrais':
                this.model = new PADadosCadastrais();
                elTemplate = _.template($('#dados-cadastrais-template-edit').html());
                this.hasCombos = true;
                break
            case 'mensagem-informativa':
                this.model = new PAMensagemInformativa();
                elTemplate = _.template($('#mensagem-informativa-template-edit').html());
                this.hasCombos = true;
                break;

        }
        loading();
        this.model.fetch({async: false, complete: loaded});

        var mainTemplate = _.template($('#main-template').html());
        $(this.el).html(mainTemplate({menu: menuTemplate({current: this.currentView}), content: elTemplate(this.model.toJSON())}));
        this.mostraVariaveis();
        if (this.hasCombos) {
            this.populateSelects();
        }
        loaded();
        startTinyMCE();
        return this;
    },
    populateSelects: function() {
        var comboAssunto = new CollectionView({
            collection: this.assuntoCoPaiCollection,
            childViewConstructor: SelectView,
            childViewTagName: 'option',
            el: $('#id_assuntocopai')
        });
        comboAssunto.render();
//
        $('#id_assuntocopai').val(this.model.get('id_assuntocopai'));
        $('#id_assuntocopai').trigger('change');

        var comboOcorrencia = new CollectionView({
            collection: this.categoriaOcorrenciaCollection,
            childViewConstructor: SelectView,
            childViewTagName: 'option',
            el: $('#id_categoriaocorrencia')
        });

        comboOcorrencia.render();
        $('#id_categoriaocorrencia').val(this.model.get('id_categoriaocorrencia'));
        $('#id_categoriaocorrencia').trigger('change');


        this.updateSubAssunto();

    },
    changeAssunto: function(){ 
        this.model.set('id_assuntocopai', $('#id_assuntocopai').val());
        this.updateSubAssunto();
    },
    updateSubAssunto: function() {
        //popula a coleção de subassuntos
        this.subAssuntoCoCollection.url = '/primeiro-acesso/find-sub-assuntos/id_pai/' + (this.model.get('id_assuntocopai') && this.model.get('id_assuntocopai') != 'null' ? this.model.get('id_assuntocopai') : '');
        this.subAssuntoCoCollection.fetch({async: false});

        var comboSubAssunto = new CollectionView({
            collection: this.subAssuntoCoCollection,
            childViewConstructor: SelectView,
            childViewTagName: 'option',
            el: $('#id_assuntoco')
        });

        comboSubAssunto.render();
        $('#id_assuntoco').val(this.model.get('id_assuntoco'));
        $('#id_assuntoco').trigger('change');

    },
    mostraVariaveis: function() {
        var renderer = new CollectionView({
            collection: this.variaveisCollection,
            childViewConstructor: TagView,
            childViewTagName: 'span',
            el: $('#variaveis')
        });
        renderer.render();
    },
    renderPreview: function(e) {
        e.preventDefault();
        var target = $(e.currentTarget);
        var url = target.attr('href');
        if (url.indexOf('#') == 0) {
            $(url).modal('open');
        } else {
            $.get(url, function(data) {
                $('<div class="modal hide fade">' + data + '</div>').modal();
            }).success(function() {
                $('input:text:visible:first').focus();
            });
        }
    },
    save: function() {
        tinyMCE.triggerSave();
        this.model.set(formToObject('#formDados'));
        this.model.save(null, {
            success: function(model, response) {
                $.pnotify({title: 'Registro Atualizado', text: 'O registro foi atualizado com sucesso!', type: 'success'});
            },
            error: function(model, response) {
                $.pnotify({title: 'Erro ao atualizar o registro', text: 'Houve um problema ao atualizar o registro, tente novamente', type: 'error'});
            }
        });
    },
    events: {
        'click a.save-link': 'save',
        'click a.preview-link': 'renderPreview',
        'change #id_assuntocopai': 'changeAssunto'
    }

});
//
//
RouterPrimeiroAcesso = Backbone.Router.extend({
    routes: {
        "primeiro-acesso/:view": "renderGeral"
    },
    renderGeral: function(view) {
        primeiroAcessoView.currentView = view;
        primeiroAcessoView.render();
    }
});


Backbone.View.prototype.close = function() {
    this.remove();
    this.unbind();
}

/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * *                       OUTRAS FUNCIONALIDADES                      * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 */

startTinyMCE = function() {
    tinyMCE.remove(); //resolve bug de salvamentos subsequentes, zernado o TinyMCE antes de reiniciá-lo
    tinyMCE.init({
        selector: 'textarea',
        elements: ".mce",
        font_size_style_values: "8px,10px,12px,13px,14px,16px,18px,20px,22px",
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "save table contextmenu directionality emoticons template paste textcolor"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l ink image | print preview media fullpage | fontsizeselect | forecolor backcolor emoticons",
        style_formats: [
            {title: 'Bold text', inline: 'b'},
            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
            {title: 'Example 1', inline: 'span', classes: 'example1'},
            {title: 'Example 2', inline: 'span', classes: 'example2'},
            {title: 'Table styles'},
            {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
        ]
    });
};

var hash = window.location.hash.split('/');

if (hash[2] !== undefined) {
    primeiroAcessoView = new PrimeiroAcessoView({currentView: hash[2]});
} else {
    primeiroAcessoView = new PrimeiroAcessoView({ currentView: 'boas-vindas'});
}
router = new RouterPrimeiroAcesso();


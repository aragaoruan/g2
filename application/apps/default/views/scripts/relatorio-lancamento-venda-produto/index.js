var viewLayout,
    viewGrid,
    viewPaginator,
    collectionPesquisa;

var LayoutView = Marionette.LayoutView.extend({
    template: '#layout-template-rlvp',
    regions: {
        pesquisa: '#pesquisa-container-rlvp',
        paginacao: '#pesquisa-paginacao-rlvp'
    },
    ui: {
        form: 'form',
        id_entidade: '#id_entidade',
        id_atendente: '#id_atendente',
        id_tipoproduto: '#id_tipoproduto',
        dt_confirmacao_min: '#dt_confirmacao_min',
        dt_confirmacao_max: '#dt_confirmacao_max',
        dt_baixa_min: '#dt_baixa_min',
        dt_baixa_max: '#dt_baixa_max',
        dt_criacao_min: '#dt_criacao_min',
        dt_criacao_max: '#dt_criacao_max',
        dt_vencimento_min: '#dt_vencimento_min',
        dt_vencimento_max: '#dt_vencimento_max',
        // PAGER
        nu_registros: '#nu_registros',
        //BUTTONS
        btnPrint: '.btnPrint',
        btnXls: '.btnXls',
        // OTHER
        totalCount: '.total-box'
    },
    events: {
        'change @ui.dt_criacao_min': function () {
            this.validateDtRange('criacao');
        },
        'change @ui.dt_criacao_max': function () {
            this.validateDtRange('criacao');
        },
        'change @ui.dt_vencimento_min': function () {
            this.validateDtRange('vencimento');
        },
        'change @ui.dt_vencimento_max': function () {
            this.validateDtRange('vencimento');
        },
        'change @ui.dt_baixa_min': function () {
            this.validateDtRange('baixa');
        },
        'change @ui.dt_baixa_max': function () {
            this.validateDtRange('baixa');
        },
        'change @ui.dt_confirmacao_min': function () {
            this.validateDtRange('confirmacao');
        },
        'change @ui.dt_confirmacao_max': function () {
            this.validateDtRange('confirmacao');
        },
        'change @ui.id_entidade': function () {
            this.loadComboAtendentesEntidade();
        },

        'submit form': 'actionSearch',
        'click @ui.btnPrint': 'actionExport',
        'click @ui.btnXls': 'actionExport'
    },
    onRender: function () {
        //Carrega combo com a lista de Entidades.
        this.loadComboEntidade();
        //Carrega combo TipoProduto
        this.loadComboTipoProduto();
    },

    loadComboEntidade: function () {
        //Carregar combo entidades
        var that = this;
        ComboboxView({
            el: this.ui.id_entidade,
            url: '/default/relatorio-lancamento-venda-produto/retorna-entidades-ativas',
            optionLabel: 'st_nomeentidade',
            emptyOption: 'Todas',
            model: Backbone.Model.extend({
                idAttribute: 'id_entidade'
            })
        }, function ($el, composite) {
            // Se houver apenas uma entidade, remove a opção 'todas' e desabilita o select.
            if (composite.collection.length == 1) {
                that.ui.id_entidade.find('option:first').remove();
                that.ui.id_entidade.prop('disabled', true);
                that.loadComboAtendentesEntidade();
            } else {
                that.disableSelectAtendentes("Selecione uma entidade", 1);
            }
        });
    },

    loadComboTipoProduto: function () {
        //Carregar combo TipoProduto
        ComboboxView({
            el: this.ui.id_tipoproduto,
            url: '/default/relatorio-lancamento-venda-produto/retorna-tipo-produto/?id_entidade=' + G2S.loggedUser.get('id_entidade'),
            optionLabel: 'st_tipoproduto',
            emptyOption: 'Selecione',
            model: Backbone.Model.extend({
                idAttribute: 'id_tipoproduto'
            })
        });
    },

    loadComboAtendentesEntidade: function () {

        var that = this;
        var idEntidadeSelecionada = this.ui.id_entidade.val();

        if (idEntidadeSelecionada) {
            ComboboxView({
                el: this.ui.id_atendente,
                url: '/default/relatorio-lancamento-venda-produto/retorna-atendentes-entidade/?id_entidade=' + idEntidadeSelecionada,
                optionLabel: 'st_nomecompleto',
                emptyOption: 'Selecione',
                model: Backbone.Model.extend({
                    idAttribute: 'id_usuario'
                })
            }, function ($el, composite) {
                if (!composite.collection.length) {
                    that.disableSelectAtendentes('Entidade sem atendentes', 2);
                }
            });
            this.ui.id_atendente.removeAttr('disabled');
        } else {
            this.disableSelectAtendentes("Selecione uma entidade", 1);
        }

    },
    validateDtRange: function (tipoData) {
        var min;
        var max;

        maxDias = 0;

        if (tipoData == 'criacao') {
            min = this.ui.dt_criacao_min;
            max = this.ui.dt_criacao_max;
            maxDias = 90;
        } else if (tipoData == 'vencimento') {
            min = this.ui.dt_vencimento_min;
            max = this.ui.dt_vencimento_max;
            maxDias = 90;
        } else if (tipoData == 'baixa') {
            min = this.ui.dt_baixa_min;
            max = this.ui.dt_baixa_max;
            maxDias = 90;
        } else if (tipoData == 'confirmacao') {
            min = this.ui.dt_confirmacao_min;
            max = this.ui.dt_confirmacao_max;
            maxDias = 180;
        } else {
            return false;
        }

        var diff = daysDiff(min.val(), max.val());

        if (diff < 0) {
            $.pnotify({
                type: 'warning',
                title: 'Data incorreta',
                text: 'A data final deve ser maior que a data inicial.'
            });
            max.val('');
            return false;
        } else if (maxDias > 0 && diff > maxDias) {
            $.pnotify({
                type: 'warning',
                title: 'Data incorreta',
                text: 'O intervalo entre as datas deve ser de no máximo ' + maxDias + ' dias.'
            });
            max.val('');
            return false;
        }

        return true;
    },
    montaUrlPesquisaCollection: function (idEntidade, idAtendente, idTipoProduto) {

        /* Relatório Lançamento Venda Produto
         (Constantes disponíveis em g2\library\G2\Constante\RelatorioLancamentoVendaProduto.php).*/

        var TIPO_REL_LANCAMENTO_VENDA_PRODUTO = 1;

        var url = '/default/relatorio-lancamento-venda-produto/pesquisar/?';
        var paramPrevio = 0;

        if (idAtendente != '' && idAtendente != null) {
            paramPrevio ? url += '&' : '';
            url += ('id_atendente=' + idAtendente);
            paramPrevio++;
        }

        if (idEntidade != '' && idEntidade != null) {
            paramPrevio ? url += '&' : '';
            url += ('id_entidade=' + idEntidade);
            paramPrevio++;
        }

        if (idTipoProduto != '' && idEntidade != null) {
            paramPrevio ? url += '&' : '';
            url += ('id_tipoproduto=' + idTipoProduto);
            paramPrevio++;
        }

        //Indica que o relatório é o Lançamento Venda Produto.
        paramPrevio ? url += '&' : '';
        url += ('id_tiporelatoriolvp=' + TIPO_REL_LANCAMENTO_VENDA_PRODUTO);

        return url;
    },
    actionSearch: function (e) {
        e && e.preventDefault ? e.preventDefault() : '';

        var that = this;
        var data = Backbone.Syphon.serialize(this);

        var idEntidade = this.ui.id_entidade.val();
        var idAtendente = this.ui.id_atendente.val();
        var idTipoProduto = this.ui.id_tipoproduto.val() ? this.ui.id_tipoproduto.val() : '';

        urlPesquisaCollection = this.montaUrlPesquisaCollection(
            idEntidade, idAtendente, idTipoProduto);
        // Backgrid CLASSES
        var PesquisaModel = Backbone.Model.extend({});

        var PesquisaCollection = Backbone.PageableCollection.extend({
            model: PesquisaModel,
            url: urlPesquisaCollection,
            mode: 'client', // Paginacao no lado do cliente (via JS)
            state: {
                pageSize: parseInt(data.nu_registros)
            }
        });

        collectionPesquisa = new PesquisaCollection();

        viewGrid = new Backgrid.Grid({
            className: 'backgrid table table-striped table-bordered table-hover',
            columns: columns,
            collection: collectionPesquisa
        });

        viewPaginator = new Backgrid.Extension.Paginator({
            collection: collectionPesquisa
        });

        // Region Pesquisa
        this.pesquisa.show(viewGrid);
        this.paginacao.show(viewPaginator);

        this.fetched = collectionPesquisa.fetch({
            data: data,
            reset: true,
            beforeSend: loading,
            complete: function () {
                that.ui.totalCount.show().find('span').html(collectionPesquisa.fullCollection.length);

                if (!collectionPesquisa.length) {
                    $.pnotify({
                        type: 'warning',
                        title: 'Aviso',
                        text: 'Não foi encontrado nenhum resultado para os critérios pesquisados.'
                    })
                }

                loaded();
            }
        });
    },
    actionExport: function (e) {
        e && e.preventDefault ? e.preventDefault() : '';

        if (!this.fetched) {
            this.actionSearch();
        }

        var data = Backbone.Syphon.serialize(this);

        var url = '/default/relatorio-lancamento-venda-produto/pesquisar/?id_tiporelatoriolvp=1&' + $.param(data);
        var win;

        if (e && e.currentTarget && $(e.currentTarget).hasClass('btnPrint')) {
            url += '&type=html';
            win = window.open(url, '_blank');
        } else {
            url += '&type=xls';
            win = window.open(url, '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=780, height=200, top=0, left=0');
        }

        if (!win) {
            $.pnotify({
                type: 'warning',
                title: 'Aviso',
                text: 'Seu navegador bloqueou o popup.'
            });
        }
    },

    /** @function disableSelectAtendentes - Desabilita o select de atendentes.
     * @author Marcus Pereira <marcus.pereira@unyleya.com.br>
     * @param mensagem - Mensagem a ser inserida no select desabilitado.
     * @param origem - Origem do disable:
     * 1 para select de entidades com a opção 'Todos' selecionada,
     * 2 para entidade selecionada sem atendentes cadastrados.
     * */

    disableSelectAtendentes: function (mensagem, origem) {
        if (origem == 1) {
            if (!this.ui.id_entidade.val()) {
                this.ui.id_atendente.prop('disabled', true);
                this.ui.id_atendente.prepend("<option value='' selected='selected'>" + mensagem + "</option>");
            }
        } else if (origem == 2) {
            this.ui.id_atendente.prop('disabled', true);
            this.ui.id_atendente.prepend("<option value='' selected='selected'>" + mensagem + "</option>");
        }
    }
});

// CONFIG GRID COLUMNS
var columns = [
    {
        name: 'st_nomeentidade',
        label: 'Entidade',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_atendente', // JSON Field
        label: 'Atendente', // Label que aparecera no Header da tabela
        editable: false,
        cell: 'string'
    },
    {
        name: "st_aluno",
        label: "Aluno",
        editable: false,
        cell: "string"
    },
    {
        name: 'st_cpf',
        label: 'CPF',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_rg',
        label: 'RG',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_email',
        label: 'E-mail',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_ddd',
        label: 'DDD',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_telefone',
        label: 'Telefone',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_cep',
        label: 'CEP',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_endereco',
        label: 'Endereço',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_bairro',
        label: 'Bairro',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_complemento',
        label: 'Complemento',
        editable: false,
        cell: 'string'
    },
    {
        name: 'nu_numero',
        label: 'Número',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_cidade',
        label: 'Cidade',
        editable: false,
        cell: 'string'
    },
    {
        name: 'sg_uf',
        label: 'UF',
        editable: false,
        cell: 'string'
    },
    {
        name: 'id_venda',
        label: 'ID Venda',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_meiopagamento',
        label: 'Meio de pagamento',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_valorliquido',
        label: 'Valor Liquido',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_valorbruto',
        label: 'Valor Bruto',
        editable: false,
        cell: 'string'
    },
    {
        name: 'id_aluno',
        label: 'ID Usuário',
        editable: false,
        cell: 'string'
    },
    {
        name: 'dt_confirmacao',
        label: 'Data confirmação venda',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_produto',
        label: 'Nome do Produto',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_idproduto',
        label: 'ID Produto',
        editable: false,
        cell: 'string'
    },
    {
        name: 'dt_vencimento',
        label: 'Data Vencimento',
        editable: false,
        cell: 'string'
    },
    {
        name: 'dt_baixa',
        label: 'Data Baixa',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_valorlancamento',
        label: 'Valor lançamento',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_valorlancamentopago',
        label: 'Valor pago do lançamento',
        editable: false,
        cell: 'string'
    },
    {
        name: 'dt_cadastro',
        label: 'Data da Criação',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_valorproduto',
        label: 'Valor do Produto',
        editable: false,
        cell: 'string'
    },
    {
        name: 'id_lancamento',
        label: 'ID Lançamento',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_ordem',
        label: 'Número Lançamento',
        editable: false,
        cell: 'string'
    }
];

// Render layout template
viewLayout = new LayoutView();
G2S.layout.content.show(viewLayout);
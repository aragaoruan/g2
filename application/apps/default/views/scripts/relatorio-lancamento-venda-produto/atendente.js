var viewLayout,
    viewGrid,
    viewPaginator,
    collectionPesquisa;

var LayoutView = Marionette.LayoutView.extend({
    template: '#layout-template',
    regions: {
        pesquisa: '#pesquisa-container',
        paginacao: '#pesquisa-paginacao'
    },
    ui: {
        form: 'form',
        st_atendente: '#st_atendente',
        dt_criacao_min: '#dt_criacao_min',
        dt_criacao_max: '#dt_criacao_max',
        dt_vencimento_min: '#dt_vencimento_min',
        dt_vencimento_max: '#dt_vencimento_max',
        dt_baixa_min: '#dt_baixa_min',
        dt_baixa_max: '#dt_baixa_max',
        dt_confirmacao_min: '#dt_confirmacao_min',
        dt_confirmacao_max: '#dt_confirmacao_max',
        // PAGER
        nu_registros: '#nu_registros',
        //BUTTONS
        btnPrint: '.btnPrint',
        btnXls: '.btnXls',
        // OTHER
        totalCount: '.total-box'
    },
    events: {
        'change @ui.dt_criacao_min': function () {
            this.validateDtRange('criacao');
        },
        'change @ui.dt_criacao_max': function () {
            this.validateDtRange('criacao');
        },
        'change @ui.dt_vencimento_min': function () {
            this.validateDtRange('vencimento');
        },
        'change @ui.dt_vencimento_max': function () {
            this.validateDtRange('vencimento');
        },
        'change @ui.dt_baixa_min': function () {
            this.validateDtRange('baixa');
        },
        'change @ui.dt_baixa_max': function () {
            this.validateDtRange('baixa');
        },
        'change @ui.dt_confirmacao_min': function () {
            this.validateDtRange('confirmacao');
        },
        'change @ui.dt_confirmacao_max': function () {
            this.validateDtRange('confirmacao');
        },

        'submit form': 'actionSearch',
        'click @ui.btnPrint': 'actionExport',
        'click @ui.btnXls': 'actionExport'
    },
    onRender: function () {
        //carrega o campo do atendente com o que está logado
        this.ui.st_atendente.val(G2S.loggedUser.get('nomeUsuario'));
    },
    validateDtRange: function (tipoData) {
        var min;
        var max;

        maxDias = 0;

        if (tipoData == 'criacao') {
            min = this.ui.dt_criacao_min;
            max = this.ui.dt_criacao_max;
            maxDias = 90;
        } else if (tipoData == 'vencimento') {
            min = this.ui.dt_vencimento_min;
            max = this.ui.dt_vencimento_max;
            maxDias = 90;
        } else if (tipoData == 'baixa') {
            min = this.ui.dt_baixa_min;
            max = this.ui.dt_baixa_max;
            maxDias = 90;
        } else if (tipoData == 'confirmacao') {
            min = this.ui.dt_confirmacao_min;
            max = this.ui.dt_confirmacao_max;
            maxDias = 180;
        } else {
            return false;
        }

        var diff = daysDiff(min.val(), max.val());

        if (diff < 0) {
            $.pnotify({
                type: 'warning',
                title: 'Data incorreta',
                text: 'A data final deve ser maior que a data inicial.'
            });
            max.val('');
            return false;
        } else if (maxDias > 0 && diff > maxDias) {
            $.pnotify({
                type: 'warning',
                title: 'Data incorreta',
                text: 'O intervalo entre as datas deve ser de no máximo ' + maxDias + ' dias.'
            });
            max.val('');
            return false;
        }

        return true;
    },
    actionSearch: function (e) {
        e && e.preventDefault ? e.preventDefault() : '';

        var that = this;

        /* Relatório Lançamento Venda Produto por Atendente
         (Constantes disponíveis em g2\library\G2\Constante\RelatorioLancamentoVendaProduto.php).*/

        var TIPO_REL_LANCAMENTO_VENDA_PRODUTO = 2;

        var data = Backbone.Syphon.serialize(this);

        // Backgrid CLASSES
        var PesquisaModel = Backbone.Model.extend({});

        var PesquisaCollection = Backbone.PageableCollection.extend({
            model: PesquisaModel,
            url: '/default/relatorio-lancamento-venda-produto/pesquisar/?id_atendente='
            + G2S.loggedUser.get('id_usuario')
            + '&id_tiporelatoriolvp=' + TIPO_REL_LANCAMENTO_VENDA_PRODUTO,
            mode: 'client', // Paginacao no lado do cliente (via JS)
            state: {
                pageSize: parseInt(data.nu_registros)
            }
        });

        collectionPesquisa = new PesquisaCollection();

        viewGrid = new Backgrid.Grid({
            className: 'backgrid table table-striped table-bordered table-hover',
            columns: columns,
            collection: collectionPesquisa
        });

        viewPaginator = new Backgrid.Extension.Paginator({
            collection: collectionPesquisa
        });

        // Region Pesquisa
        this.pesquisa.show(viewGrid);
        this.paginacao.show(viewPaginator);

        this.fetched = collectionPesquisa.fetch({
            data: data,
            reset: true,
            beforeSend: loading,
            complete: function () {
                that.ui.totalCount.show().find('span').html(collectionPesquisa.fullCollection.length);

                if (!collectionPesquisa.length) {
                    $.pnotify({
                        type: 'warning',
                        title: 'Aviso',
                        text: 'Não foi encontrado nenhum resultado para os critérios pesquisados.'
                    })
                }

                loaded();
            }
        });
    },
    actionExport: function (e) {
        e && e.preventDefault ? e.preventDefault() : '';

        if (!this.fetched) {
            this.actionSearch();
        }

        var data = Backbone.Syphon.serialize(this);
        data.id_atendente = G2S.loggedUser.get('id_usuario');


        var url = '/default/relatorio-lancamento-venda-produto/pesquisar/?id_tiporelatoriolvp=2&' + $.param(data);
        var win;

        if (e && e.currentTarget && $(e.currentTarget).hasClass('btnPrint')) {
            url += '&type=html';
            win = window.open(url, '_blank');
        } else {
            url += '&type=xls';
            win = window.open(url, '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=780, height=200, top=0, left=0');
        }

        if (!win) {
            $.pnotify({
                type: 'warning',
                title: 'Aviso',
                text: 'Seu navegador bloqueou o popup.'
            });
        }
    }
});

// CONFIG GRID COLUMNS
var columns = [
    {
        name: 'st_nomeentidade',
        label: 'Entidade',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_atendente', // JSON Field
        label: 'Atendente', // Label que aparecera no Header da tabela
        editable: false,
        cell: 'string'
    },
    {
        name: "st_aluno",
        label: "Aluno",
        editable: false,
        cell: "string"
    },
    {
        name: 'st_cpf',
        label: 'CPF',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_rg',
        label: 'RG',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_email',
        label: 'E-mail',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_ddd',
        label: 'DDD',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_telefone',
        label: 'Telefone',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_cep',
        label: 'CEP',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_endereco',
        label: 'Endereço',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_bairro',
        label: 'Bairro',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_complemento',
        label: 'Complemento',
        editable: false,
        cell: 'string'
    },
    {
        name: 'nu_numero',
        label: 'Número',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_cidade',
        label: 'Cidade',
        editable: false,
        cell: 'string'
    },
    {
        name: 'sg_uf',
        label: 'UF',
        editable: false,
        cell: 'string'
    },
    {
        name: 'id_venda',
        label: 'ID Venda',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_meiopagamento',
        label: 'Meio de pagamento',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_valorliquido',
        label: 'Valor Liquido',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_valorbruto',
        label: 'Valor Bruto',
        editable: false,
        cell: 'string'
    },
    {
        name: 'id_aluno',
        label: 'ID Usuário',
        editable: false,
        cell: 'string'
    },
    {
        name: 'dt_confirmacao',
        label: 'Data confirmação venda',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_produto',
        label: 'Nome do Produto',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_idproduto',
        label: 'ID Produto',
        editable: false,
        cell: 'string'
    },
    {
        name: 'dt_vencimento',
        label: 'Data Vencimento',
        editable: false,
        cell: 'string'
    },
    {
        name: 'dt_baixa',
        label: 'Data Baixa',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_valorlancamento',
        label: 'Valor lançamento',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_valorlancamentopago',
        label: 'Valor pago do lançamento',
        editable: false,
        cell: 'string'
    },
    {
        name: 'dt_cadastro',
        label: 'Data da Criação',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_valorproduto',
        label: 'Valor do Produto',
        editable: false,
        cell: 'string'
    },
    {
        name: 'id_lancamento',
        label: 'ID Lançamento',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_ordem',
        label: 'Número Lançamento',
        editable: false,
        cell: 'string'
    }
];

// Render layout template
viewLayout = new LayoutView();
G2S.layout.content.show(viewLayout);
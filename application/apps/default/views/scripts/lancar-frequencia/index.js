/*******************************************************************************
 * Imports
 ******************************************************************************/
$.ajaxSetup({'async': false});
$.getScript('/js/backbone/views/LancarFrequencia/LancarFrequenciaView.js');

/*******************************************************************************
 *  Views
 ******************************************************************************/

    var view = new LancarFrequenciaView();
    G2S.layout.content.show(view);

$.ajaxSetup({'async': true});
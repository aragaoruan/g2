//1 - Pesquisa Individual; 2 - Pesquisa por Lote;
var funcaoBotaoPesquisa = 0;

var vwGerarEtiquetasModel = Backbone.Model.extend({
    defaults: {
        id_entregadeclaracao: '',
        id_matricula: '',
        id_situaca: '',
        id_usuario: '',
        id_entidadematricula: '',
        st_nomecompleto: '',
        st_cpf: '',
        st_textosistema: '',
        st_endereco: '',
        st_complemento: '',
        st_bairro: '',
        st_cep: '',
        nu_numero: '',
        st_nomemunicipio: '',
        sg_uf: ''
    }
});

var vwGerarEtiquetasCollection = Backbone.Collection.extend({
    model: vwGerarEtiquetasModel,
    url: 'api/vw-gerar-etiquetas/',
    parse: function(response){
        $.each(response, function(i, val){
            var dataHora = val.dt_solicitacao.split(' ');
            var data = dataHora[0].split('-');
            val.dt_solicitacao = data[2] + '/' + data[1] + '/' + data[0];
        });
        return response;
    }
});

var dadosCollection = new vwGerarEtiquetasCollection();

var botoesAcao = Backgrid.Cell.extend({
    template: _.template('<div class="text-center">' +
        '<button class="btn baixarEtiqueta" title="Download da etiqueta em XLS">' +
        '<i class="icon-download"></i>' +
        '</button><button class="btn imprimirEtiqueta" title="Imprimir etiqueta">' +
        '<i class="icon-print"></i></button>' +
        '</div>'),
    events: {
        'click .imprimirEtiqueta': 'imprimirEtiqueta',
        'click .baixarEtiqueta' : 'baixarEtiqueta'
    },
    imprimirEtiqueta: function(){
        var that = this;

        var janelaImpressao = window.open('/gerar-etiqueta/gerar-xls/?' +
            $.param({
                id_entregadeclaracao: that.model.get('id_entregadeclaracao') ? that.model.get('id_entregadeclaracao') : '',
                impressao: true
            })
            , '_blank');
        janelaImpressao.onload = function() {
            janelaImpressao.print();
        }
    },
    baixarEtiqueta: function(){
        var that = this;

        window.open('/gerar-etiqueta/gerar-xls/?' +
            $.param({
                id_entregadeclaracao: that.model.get('id_entregadeclaracao') ? that.model.get('id_entregadeclaracao') : ''
            })
            , '_blank');
    },
    render: function(){
        this.$el.html(this.template());
        this.delegateEvents();
        return this;
    }
});

var colunasGrid = [
    {
        name: '',
        cell: 'select-row',
        headerCell: 'select-all'
    },
    {
        name: 'st_nomecompleto',
        label: 'Nome do Aluno',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_cpf',
        label: 'CPF',
        editable: false,
        cell: 'string'
    },
    {
        name: 'st_textosistema',
        label: 'Declaração',
        editable: false,
        cell: 'string'
    },
    {
        name: 'dt_solicitacao',
        label: 'Data de Confecção',
        editable: false,
        cell: 'string'
    },
    {
        name: '',
        label: 'Ação',
        editable: false,
        cell: botoesAcao
    }
];

var gridResultados = new Backgrid.Grid({
    columns: colunasGrid,
    collection: dadosCollection
});

var pesquisaIndividualModel = Backbone.Model.extend({
    defaults: {
        st_nomecompleto: '',
        st_email: '',
        st_cpf: ''
    }
});

var tipoBuscaItemView = Marionette.ItemView.extend({
    template: '#tipoBuscaTemplate'
});

var pesquisaIndividualItemView = Marionette.ItemView.extend({
    template: '#pesquisaIndividualTemplate',
    model: new pesquisaIndividualModel()
});

var gridResultadosItemView = Marionette.ItemView.extend({
    template: '#gridResultadosTemplate'
});

var gerarEtiquetaLayoutView = Marionette.LayoutView.extend({
    template: '#template-layout',
    initialize: function(){

    },
    regions: {
        tipoBusca: '#regTipoBusca',
        buscaForm: '#regBuscaIndividualForm',
        gridResultados: '#regGridResultados'
    },
    events: {
        'change #selectTipoBusca' : 'alteraTipoBusca',
        'submit #formTipoBusca' : 'buscaPorLote',
        'submit #formBuscaIndividual': 'buscaIndividual',
        'click #gerarEtiquetas' : 'baixarEtiquetaLotes'
    },
    onRender: function(){
        //Não há a necessidade de se utilizar this.regions para acessar a região.
        this.tipoBusca.show(new tipoBuscaItemView());
    },
    alteraTipoBusca: function(){
        this.gridResultados.reset();
        var itemSelecionado = $('#selectTipoBusca').val();

        if (itemSelecionado == 1) {
            funcaoBotaoPesquisa = 1;
            this.buscaForm.show(new pesquisaIndividualItemView());
            $('#botaoPesquisa').attr('form', 'formBuscaIndividual');
        } else if (itemSelecionado == 2) {
            this.buscaForm.reset();
            funcaoBotaoPesquisa = 2;
            $('#botaoPesquisa').attr('form', 'formTipoBusca');
        } else {
            this.buscaForm.reset();
            funcaoBotaoPesquisa = 0;
            $('#botaoPesquisa').attr('form', 'formTipoBusca');
        }
    },
    buscaPorLote: function(e) {
        var that = this;
        this.gridResultados.reset();
        e.preventDefault();

        if(funcaoBotaoPesquisa == 2) {
            loading();
            dadosCollection.fetch({
                data: {
                    id_entidadematricula: G2S.loggedUser.get('id_entidade')
                }
            }).success(function(response){
                loaded();
                if(response && response.length) {
                    that.gridResultados.show(new gridResultadosItemView());
                    $('#gridResultados').append(gridResultados.render().el);
                    $('.backgrid').addClass('table table-striped table-bordered');
                    //$('#gridResultados').append(gridResultados.render().el);
                } else {
                    $.pnotify({
                        type: 'Error',
                        title: 'Alerta:',
                        text: 'Não há alunos disponíveis nesta entidade.'
                    });
                }
            });
        } else if(funcaoBotaoPesquisa == 0) {
            $.pnotify({
                type: 'error',
                title: 'Erro',
                text: 'Antes de pesquisar, selecione o tipo.'
            });
        }
    },
    buscaIndividual: function(e){
        var that = this;
        this.gridResultados.reset();
        e.preventDefault();

        if(this.validaBuscaIndividual()){
            var values = Backbone.Syphon.serialize(this.buscaForm.currentView);
            for (var i in values){
                if(!values[i]){
                    delete values[i];
                }
            }

            dadosCollection.fetch({
                data: values
            }).success(function(response){
                loaded();
                if(response && response.length) {
                    that.gridResultados.show(new gridResultadosItemView());
                    $('#gridResultados').append(gridResultados.render().el);
                    $('.backgrid').addClass('table table-striped table-bordered');
                    //$('#gridResultados').append(gridResultados.render().el);
                } else {
                    $.pnotify({
                        type: 'Error',
                        title: 'Alerta',
                        text: 'Nenhum aluno encontrado.'
                    });
                }
            });
        } else {
            $.pnotify({
                type: 'warning',
                text: 'Entre com pelo menos um dos campos abaixo antes de prosseguir.',
                title: 'Alerta'
            });
        }
    },

    baixarEtiquetaLotes: function(){
        var selectedModels = gridResultados.getSelectedModels();
        if(selectedModels && selectedModels.length) {
            var queryStringArray = '?';
            for(var i=0; i<selectedModels.length; i++){
                queryStringArray += 'id_entregadeclaracao[]=' + selectedModels[i].get('id_entregadeclaracao') + '&';
            }
            window.open(
                '/gerar-etiqueta/gerar-xls' + queryStringArray, '_blank'
            );
        } else {
            $.pnotify({
                'type': 'alert',
                'text': 'Selecione pelo menos um usuário para gerar as etiquetas.',
                'title': 'Atenção'
            });
        }
    },

    /* Verifica se há pelo menos um campo preenchido.*/
    validaBuscaIndividual: function(){
        var existeCampoPreenchido = 0;
        $('#formBuscaIndividual').find('input').each(function(){
            if(($(this).val())){
                existeCampoPreenchido++;
            }
        });
        return existeCampoPreenchido ? true : false;
    }
});

G2S.layout.content.show(new gerarEtiquetaLayoutView());
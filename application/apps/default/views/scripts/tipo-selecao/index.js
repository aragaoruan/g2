/**
 * Valida o campo de tipo de seleção
 * @param value
 * @returns {string}
 */
var validaStTipoSelecao = function (value) {
    var titulo = value.trim();
    if (!titulo.length)
        return "Campo Tipo de Ingresso não pode ser vazio.";


    if (titulo.length > 100)
        return "Campo Tipo de Ingresso não pode conter mais do que 100 caracteres.";

};

/**
 * Valida o campo de descrição
 * @param value
 * @returns {string}
 */
var validaStDescricao = function (value) {
    var descricao = value.trim();
    if (!descricao.length)
        return "Campo Texto da Forma de Ingresso não pode ser vazio.";


    if (descricao.length > 200)
        return "Campo Texto da Forma de Ingresso não pode conter mais do que 200 caracteres.";

};

/**
 * Cria uma nova variavel para extender a model padrão do backbone e criar os validators
 */
var TipoSelecaoModel = TipoSelecao.extend({
    validation: {
        "st_tiposelecao": validaStTipoSelecao,
        "st_descricao": validaStDescricao
    },
    idAttribute: "id_tiposelecao"
});


/**
 * Item view do formulário
 */
var FormaIngressoItemView = Marionette.ItemView.extend({
    template: "#template-form",
    tagName: "div",
    className: "row-fluid",
    ui: {
        stDescricao: "#st_descricao",
        stTipoSelecao: "#st_tiposelecao",
        selectVariaveis: "#variaveis",
        formCadastro: "#form-cadastro",
        btnSalvar: "#btn-salvar"
    },
    events: {
        "click @ui.selectVariaveis": "addVariavel",
        "submit @ui.formCadastro": "salvarDados",
    },
    initialize: function () {
        if (G2S.editModel) {
            this.model.set(G2S.editModel);
        }

        Backbone.Validation.bind(this, {
            invalid: function (view, attr, error) {
                $.pnotify({
                    type: "warning",
                    title: "Aviso",
                    text: error
                });
            }
        });
        return this;
    },
    onShow: function () {
        if ((typeof habilitarEdicao != "undefined") && !habilitarEdicao) {
            this.bloquearCampos();
        }
        return this;
    },
    bloquearCampos: function () {
        this.ui.stTipoSelecao.attr("disabled", "disabled");
    },
    addVariavel: function () {
        var variavel = this.ui.selectVariaveis.find("option:selected").text();
        if (variavel) {
            var text = this.ui.stDescricao.val();
            this.ui.stDescricao.val(text + variavel);
        }
    },
    salvarDados: function (e) {
        e.preventDefault();

        this.model.set("st_descricao", this.ui.stDescricao.val());
        this.model.set("st_tiposelecao", this.ui.stTipoSelecao.val());


        this.model.save(null, {
            beforeSend: loading,
            error: function (model, response) {
                $.pnotify({
                    title: "Erro!",
                    type: "error",
                    text: "Erro ao tentar salvar dados. " + response.responseText
                });
            },
            complete: loaded,
            success: function () {
                $.pnotify({
                    title: "Sucesso!",
                    type: "success",
                    text: "Salvo com sucesso!"
                });
            }
        });
    }
});

var formaIngresso = new FormaIngressoItemView({
    model: new TipoSelecaoModel()
});
G2S.show(formaIngresso);
/**
 * JavaScript para funcionalidade de Grade Horaria
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-11-24
 */

/**
 * Função para gerar Select
 * @param object el
 * @param object collection
 * @param string optionLabel
 * @param string optionValue
 * @param mixed optionSelected
 * @param boolean ordenar
 */
function geraSelect(el, collection, optionLabel, optionValue, optionSelected, ordenar) {

    var view = new SelectView({
        el: el,
        collection: collection,
        sort: ordenar,
        childViewOptions: {
            optionLabel: optionLabel,
            optionValue: optionValue,
            optionSelected: optionSelected,
            optionData: 2
        }
    });
    view.render();
}


/**
 ******************************************************************************************
 ******************************************************************************************
 ******************************** GLOBAIS *************************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var thatViewGrade;
var thatGradeHorariaCompositeView;
var periodoCollection;
/**
 ******************************************************************************************
 ******************************************************************************************
 ******************************** MODELS **************************************************
 ******************************************************************************************
 ******************************************************************************************
 */

//Model fixa para retornar unidades
var UnidadeModel = Backbone.Model.extend({
    defaults: {
        id_entidade: '',
        st_nomeentidade: ''
    },
    url: '/entidade/retornar-entidade-recursiva-id'
});

var ProfessoresTirinha = Backbone.Model.extend({
    defaults: {
        id_usuario: '',
        st_nomecompleto: ''
    },
    idAttribute: 'id_usuario'
});

var ListaHorariosModel = Backbone.Model.extend({
    url: function () {
        return this.id ? '/api/item-grade-horaria/?id=' + this.id + '&id_gradehoraria=' + this.get('id_gradehoraria') + '&id_disciplina=' + this.get('id_disciplina') : '/api/item-grade-horaria/';
    },
});

var GradeHorariaModel = GradeHoraria.extend({
    idAttribute: 'id_gradehoraria'
});

var ItemGradeHorariaModel = ItemGradeHoraria.extend({
    idAttribute: 'id_itemgradehoraria'
});

var ItemGradeHorariaTurmaModel = ItemGradeHorariaTurma.extend({
    idAttribute: 'id_itemgradehorariaturma'
});

var PeriodoEncontroModel = Backbone.Model.extend();


/**
 ******************************************************************************************
 ******************************************************************************************
 ******************************** COLLECTIONS *********************************************
 ******************************************************************************************
 ******************************************************************************************
 */

var ProfessoresTirinhaCollection = Backbone.Collection.extend({
    model: ProfessoresTirinha,
    url: '/grade-horaria/professores-tirinha'
});

var SituacaoCollection = Backbone.Collection.extend({
    model: Situacao,
    url: '/api/situacao'
});

var TurnoCollection = Backbone.Collection.extend({
    model: Turno,
    url: '/projeto-pedagogico/retornar-turno-by-projeto'
});

var UnidadesCollection = Backbone.Collection.extend({
    model: UnidadeModel,
    url: '/projeto-pedagogico/retornar-entidade-com-turma-by-projeto/'
});

var LocalAulaCollection = Backbone.Collection.extend({
    model: LocalAula,
    url: '/api/local-aula'
});

var DiaSemanaCollection = Backbone.Collection.extend({
    model: DiaSemana,
    url: '/turma/retornar-dia-semana-by-turma'
});

var VwProfessorDisciplinaCollection = Backbone.Collection.extend({
    model: VwUsuarioPerfilEntidadeReferencia,
    url: '/api/vw-usuario-perfil-entidade-referencia'
});

var VwTurmaDisciplinaCollection = Backbone.Collection.extend({
    model: VwTurmaDisciplina,
    url: '/turma/retornar-vw-turma-disciplina',
    //parse: function (response) {
    //$.each(response, function (i, obj) {
    //    response[i].st_turma = obj.id_turma + ' - ' + obj.st_turma;
    //});
    //
    //return response;
    //}
});

var VwTurmaProjetoPedagogicoCollection = Backbone.Collection.extend({
    model: VwTurmaProjetoPedagogico,
    url: '/projeto-pedagogico/projeto-turma-entidade-recursiva',
    //parse: function (response) {
    //$.each(response, function (i, obj) {
    //    response[i].st_projetopedagogico = obj.id_projetopedagogico + ' - ' + obj.st_projetopedagogico;
    //});

    //return response;
    //}
});


/**
 * Collection da Disciplina
 */
var DisciplinaCollection = Backbone.Collection.extend({
    model: Disciplina,
    url: function () {
        //return '/disciplina/retornar-disciplinas-entidade-recursiva/?' + $.param(_.defaults(this.data));
        return '/projeto-pedagogico/retornar-disciplina-projeto-unidade/?' + $.param(_.defaults(this.data));
    },
    data: {
        id_projeto: null,
        id_entidade: null,
        id_turma: null
    },
    disciplinasSemCargaHoraria: [],
    parse: function (response) {
        var that = this;
        $.each(response, function (i, obj) {
            var complemento = " ";
            if (obj.nu_cargahoraria == 0) {
                complemento += "- [Carga horaria da disciplina não definida]";
                that.disciplinasSemCargaHoraria.push(obj);
            }
            response[i].st_disciplina = obj.st_disciplina + ' - Encontros ' + obj.nu_encontros + '/' + obj.nu_totalencontros + complemento;
        });
        return response;
    }
});

var ListaHorariosCollection = Backbone.Collection.extend({
    model: ListaHorariosModel
});

var ItemGradeHorariaTurmaCollection = Backbone.Collection.extend({
    model: ItemGradeHorariaTurmaModel,
    url: '/api/item-grade-horaria-turma'
});

/**
 * Collection para periodo da grade horaria
 */
var PeriodoEncontroCollection = Backbone.Collection.extend({
    model: PeriodoEncontroModel
});


/**
 ******************************************************************************************
 ******************************************************************************************
 ******************************** INTANCIAS ***********************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var gradeViewModel = new GradeHorariaModel();
var itemGradeHorariaModel = new ItemGradeHorariaModel();
var itemGradeHorariaTurmaModel = new ItemGradeHorariaTurmaModel();
var listaHorariosCollection = new ListaHorariosCollection();


var disciplinaCollection = new DisciplinaCollection();

/**
 ******************************************************************************************
 ******************************************************************************************
 *************************** LAYOUT FUNCIONALIDADE ****************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var TelaGradeLayout = Marionette.LayoutView.extend({
    template: '#template-layout-grade',
    tagName: 'div',
    //className: 'container-fluid',
    regions: {
        formgrade: '#regiao-grade',
        formHorario: '#regiao-horario',
        horarios: '#regiao-lista-horarios',
        modal: '#regiao-modal'
    },
    initialize: function(){
        G2S.isEditing = false
    },
    onShow: function () {

        this.renderizaFormGrade();
        loaded();
        return this;
    },
    renderizaFormGrade: function () {
        var gradeView = new FormGradeCompositeView({
            model: gradeViewModel
        });
        this.formgrade.show(gradeView);
    }
});

/**
 ******************************************************************************************
 ******************************************************************************************
 *************************** FORM DA GRADE ****************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var FormGradeCompositeView = Marionette.CompositeView.extend({
        initialize: function () {
            if (G2S.editModel) {
                this.model.set(G2S.editModel);
            }
            return this;
        },
        template: '#template-form-grade',
        tagName: 'fieldset',
        ui: {
            'stNomeGrade': 'input[name="st_nomegrade"]',
            'dtInicial': 'input[name="dt_inicial"]',
            'dtTermino': 'input[name="dt_termino"]',
            'idSituacao': 'select[name="id_situacao"]',
            'btnSalvar': '#btn-salvar'
        },
        onShow: function () {
            this.populaSelectSituacao();
            if (this.model.id) {
                this.mostraTelaHorarios();
            }

            return this;
        },
        populaSelectSituacao: function () {
            var that = this;
            var collection = new SituacaoCollection();
            collection.url += '/?st_tabela=tb_gradehoraria';
            collection.fetch({
                beforeSend: function () {
                    loading();
                },
                success: function () {
                    geraSelect(that.ui.idSituacao, collection, 'st_situacao', 'id_situacao', that.model.get('id_situacao'));
                }
            })
        },
        validaForm: function () {
            var valid = true;
            this.$el.find('[required]').each(function (i, elem) {
                var valor = $(elem).val();
                if (!valor) {
                    $.pnotify({
                        'type': 'warning',
                        'title': 'Atenção!',
                        'text': 'O campo ' + $(elem).attr('title') + ' é de preenchimento obrigatório.'
                    });
                    valid = false;
                }
            });
            return valid;
        },
        fechaView: function () {
            telaGrade.formgrade.destroy();
        },
        mostraTelaHorarios: function () {
            var viewGradeHorarios = new GradeHorariosCompositeView({
                model: itemGradeHorariaModel
            });
            telaGrade.formHorario.show(viewGradeHorarios);
        },
        salvarGrade: function (e) {
            e.preventDefault();
            if (this.validaForm()) {
                var that = this;
                that.model.set({
                        st_nomegradehoraria: that.ui.stNomeGrade.val(),
                        dt_iniciogradehoraria: that.ui.dtInicial.val(),
                        dt_fimgradehoraria: that.ui.dtTermino.val(),
                        id_situacao: that.ui.idSituacao.val(),
                        bl_ativo: true
                    }
                );
                that.model.save(null, {
                    beforeSend: function () {
                        loading();
                    },
                    complete: function () {
                        loaded();
                    },
                    error: function (model, response) {
                        var textResponse = response.responseText;
                        $.pnotify({
                            title: 'Erro!',
                            text: textResponse,
                            type: 'error'
                        });
                    },
                    success: function (model, response) {
                        $.pnotify({
                            title: 'Sucesso!',
                            text: 'Dados salvo com sucesso!',
                            type: 'success'
                        });
                        //that.fechaView();//fecha a view de grade
                        //if (!G2S.editModel)
                        that.mostraTelaHorarios(); //mostra a view de horarios
                    }
                });
            }
        },
        calcularDataFim: function () {
            var dt_inicio = this.ui.dtInicial.val();
            var qtdDias = 6;
            var dt_proxima = proximaDataVencimento(dt_inicio, qtdDias, 'd');
            this.ui.dtTermino.val(formatarDataParaString(dt_proxima));
        },
        events: {
            'submit form[name="form-grade"]': 'salvarGrade',
            'change input[name="dt_inicial"]': 'calcularDataFim'
        }
    })
    ;

/**
 ******************************************************************************************
 ******************************************************************************************
 *************************** FORM DOS ITENS DA GRADE **************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var GradeHorariosCompositeView = Marionette.CompositeView.extend({
    template: '#tempalte-form-horario',
    tagName: 'fieldset',
    initialize: function () {
        thatGradeHorariaCompositeView = this;
        return this;
    },
    ui: {
        'idProjeto': '#id_projetopedagogico',
        'idUnidade': '#id_unidade',
        'idTurno': '#id_turno',
        'idLocalAula': '#id_localaula',
        'idDiaSemana': '#id_diasemana',
        'nuQtdAula': '#nu_quantidadeaula',
        'nuNumeroAula': '#nu_numeroaula',
        'idDisciplina': '#id_disciplina',
        'btnSalvar': '#btn-salvar-item-grade',
        'btnImprimir': '#btn-imprimir',
        'btnTirinha': '#btn-tirinhas',
        'idTurma': '#id_turma',
        'stObservacao': '#st_observacao',
        'nu_caracteresobs': '#nu_caracteresobs',
        'bl_manterselecionados': '#bl_manterselecionados',
        'professores': '#container-professores tbody'
    },
    events: {
        'change select[name="id_disciplina"]': function () {
            this.populaListaProfessores();
        },
        'submit form#form-horario': 'salvarHorario',
        'click #btn-imprimir': 'imprimirGrade',
        'click #btn-tirinhas': 'abreModal',
        'change select[name="id_projetopedagogico"]': function () {
            this.populaSelectUnidade();
            this.ui.idDisciplina.empty().html('<option value="">Selecione</option>');
            this.ui.idTurma.empty().html('<option value="">Selecione</option>');
            this.ui.idDiaSemana.empty().html('<option value="">Selecione</option>');
            this.ui.idTurno.empty().html('<option value="">Selecione</option>');
        },
        'keyup #st_observacao': 'contadorCaracteresObservacao',
        'change #id_turno': function () {
            this.populaSelectTurma();
        },
        'change #id_unidade': function () {
            this.populaSelectTurno();
            this.ui.idTurma.empty().html('<option value="">Selecione</option>');
            this.ui.idDiaSemana.empty().html('<option value="">Selecione</option>');
            this.ui.idDisciplina.empty().html('<option value="">Selecione</option>');
        },
        'change #id_turma': function () {
            this.populaSelectDiaSemana();
            this.populaSelectDisciplina();
        }
    },
    onShow: function () {
        this.renderizaListaHorarios();
        return this;
    },
    onRender: function () {

        this.populaSelectProjeto();

        this.contadorCaracteresObservacao();

        if (!this.model.id) {
            this.ui.btnSalvar.html('Adicionar');
        } else {
            this.ui.btnSalvar.html('Salvar Alterações');
        }

        return this;
    },
    contadorCaracteresObservacao: function () {
        var total = 250;
        var caracteresDigitados = this.ui.stObservacao.val().length;

        var caracteres = total - caracteresDigitados;

        if (caracteres <= 10) {
            this.ui.nu_caracteresobs.removeClass('badge-success').addClass('badge-important');
        } else {
            this.ui.nu_caracteresobs.removeClass('badge-important').addClass('badge-success');
        }

        this.ui.stObservacao.attr('maxlength', total);
        this.ui.nu_caracteresobs.html(caracteres);
    },
    editarItem: function (id) {
        var that = this;
        this.model.id = id;
        this.model.fetch({
            beforeSend: function () {
                loading();
            },
            complete: function () {
                loaded();
            },
            success: function (model) {
                that.render();
                G2S.isEditing = true;
            }
        });
    },
    validaForm: function () {
        var valid = true;
        var mensagem = '';
        this.$el.find('[required]').each(function (i, elem) {
            var valor = $(elem).val();
            if (!valor) {
                mensagem += 'O campo <strong>' + $(elem).attr('title') + '</strong> é de preenchimento obrigatório.<br>'
                valid = false;
            }
        });

        if (!valid) {
            $.pnotify({
                'type': 'warning',
                'title': 'Atenção!',
                'text': mensagem
            });
        }


        var idProfessor = this.$el.find('input[name="id_professor"]:checked').val();
        if (!idProfessor) {
            $.pnotify({
                'type': 'warning',
                'title': 'Atenção!',
                'text': 'Selecione um professor para vincular a Grade Horaria.'
            });
            valid = false;
        }

        return valid;
    },
    salvarHorario: function (e) {
        e.preventDefault();
        var that = this;
        var idProfessor = this.$el.find('input[name="id_professor"]:checked').val();
        if (this.validaForm()) {
            var diaSemana = periodoCollection.findWhere({dt_periodo: that.ui.idDiaSemana.val()});

            this.model.set({
                dt_diasemana: diaSemana ? diaSemana.get('dt_periodo') : null,
                id_diasemana: diaSemana ? diaSemana.get('id_diasemana') : null,
                id_disciplina: that.ui.idDisciplina.val(),
                id_gradehoraria: gradeViewModel.get('id_gradehoraria'),
                id_professor: idProfessor,
                id_projetopedagogico: that.ui.idProjeto.val(),
                id_turma: that.ui.idTurma.val(),
                id_turno: that.ui.idTurno.val(),
                id_unidade: that.ui.idUnidade.val(),
                st_observacao: that.ui.stObservacao.val(),
                id_turnoaula:(that.ui.idDiaSemana.val()).split(/-/)[1],
                id_tipoaula:(that.ui.idDiaSemana.val()).split(/-/)[2]
            });

            var parametros = {
                'model': this.model.toJSON(),
                'valida': true
            };

            $.ajax({
                type: 'post',
                data: parametros,
                url: '/api/item-grade-horaria',
                success: function (response) {
                    var that = this;
                    if (response != 'false') {
                        var textResponse = JSON.parse(response);
                        bootbox.confirm(textResponse.text + "<br><b>Tem certeza de que deseja continuar o registro?</b>", function (result) {
                            if (result) {
                                that.saveTime();
                            }
                        });
                    } else {
                        that.saveTime();
                    }
                },
                saveTime: function () {
                    that.model.save(null, {
                        beforeSend: function () {
                            loading();
                        },
                        complete: function () {
                            loaded();
                        },
                        error: function (model, response) {
                            var textResponse = JSON.parse(response.responseText);
                            $.pnotify(textResponse);
                        },
                        success: function (model) {
                            $.pnotify({
                                title: 'Sucesso!',
                                type: 'success',
                                text: 'Encontro salvo com sucesso!'
                            });
                            thatGradeHorariaCompositeView.renderizaListaHorarios();
                            G2S.isEditing = false;
                            that.model = new ItemGradeHorariaModel();
                            if (!$('#bl_manterselecionados').is(':checked')) {
                                that.render();
                                G2S.isEditing = false;
                            } else {
                                that.populaSelectDisciplina();
                            }

                        }
                    })
                }
            });

        }
    },
    populaSelectProjeto: function () {
        var that = this;
        var turmaProjetoCollection = new VwTurmaProjetoPedagogicoCollection();
        turmaProjetoCollection.fetch({
            beforeSend: function () {
                loading();
            },
            complete: function () {
                if (that.model.get('id_projetopedagogico')) {
                    that.populaSelectUnidade();
                    that.populaSelectTurno();
                    if ($('#id_situacao').val() == 135 && G2S.isEditing) {
                        that.ui.idProjeto.attr('disabled', true);
                    } else {
                        that.ui.idProjeto.attr('disabled', false);
                    }
                }


                loaded();
            },
            success: function () {
                geraSelect(that.ui.idProjeto, turmaProjetoCollection, 'st_projetopedagogico', 'id_projetopedagogico', that.model.get('id_projetopedagogico'));
                that.ui.idProjeto.select2();

            }
        });
    },
    populaSelectDisciplina: function () {
        disciplinaCollection = new DisciplinaCollection();
        disciplinaCollection.data = {
            id_projeto: this.ui.idProjeto.val(),
            id_entidade: this.ui.idUnidade.val(),
            id_turma: this.ui.idTurma.val()
        };

        var that = this;
        disciplinaCollection.fetch({
            beforeSend: function () {
                loading();
                that.ui.idDisciplina.empty().html('<option value="">Selecione</option>');
            },
            complete: function () {
                that.ui.idDisciplina.select2({
                    //minimumInputLength: 5,
                    placeholder: "Selecione"
                });

                //desabilita os options das disciplina sem carga horaria
                $.each(disciplinaCollection.disciplinasSemCargaHoraria, function (i, obj) {
                    var id_disciplina = obj.id_disciplina;
                    that.ui.idDisciplina.find("option[value='" + id_disciplina + "']").attr('disabled', 'disabled');
                });

                if (that.model.get('id_disciplina')) {
                    that.populaListaProfessores();
                }
                that.ui.idDisciplina.select2();

                loaded();
            },
            error: function (model, response) {
                $.pnotify({
                    type: 'warning',
                    title: 'Atenção!',
                    text: response.responseText
                });
            },
            success: function () {
                /*
                 * altera o numero de apagars na edição de um registros
                 * setando o numero inicial para o do encontro do registro que esta sendo editado
                 */
                if (that.model.get('id_disciplina')) {
                    //busca a disciplina pegando o id e forcando ele pra string
                    var disciplinaModel = disciplinaCollection.findWhere({id_disciplina: String(that.model.get('id_disciplina'))});
                    if (disciplinaModel) {//verifica se encontro a disciplina pra evitar erro de javascript
                        //quebra o nome da disciplina separando o nome e os encontros que é setado no parse da collection
                        var disciplinaNome = disciplinaModel.get('st_disciplina').split(" - ");
                        //verifica se tem valor pra evitar erro
                        if (disciplinaNome[0]) {
                            //monsta uma string com o nome da disciplina
                            var stringEncontro = disciplinaNome[0] + " - Encontros " + that.model.get('nu_encontro') + "/" + disciplinaModel.get('nu_totalencontros');
                            //seta o nome da disciplina
                            disciplinaModel.set('st_disciplina', stringEncontro);
                        }
                    }
                }
                geraSelect(that.ui.idDisciplina, disciplinaCollection, 'st_disciplina', 'id_disciplina', that.model.get('id_disciplina'));
            }
        });
    },
    populaSelectTurma: function () {
        var that = this;
        if (!that.ui.idUnidade.val()) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Selecione a Unidade para poder buscar as Turmas.'
            });
            return false;
        } else if (!that.ui.idProjeto.val()) {
            return false;
        }


        var collection = new VwTurmaDisciplinaCollection();
        collection.url = '/projeto-pedagogico/retornar-turma-projeto-unidade/id_projeto/' + that.ui.idProjeto.val() + '/id_entidade/' + that.ui.idUnidade.val() + '/id_turno/' + that.ui.idTurno.val();
        collection.fetch({
            beforeSend: function () {
                loading();
                that.ui.idTurma.empty().html('<option value="">Selecione</option>');
            },
            complete: function () {
                loaded();
                if (that.model.get('id_turma')) {
                    that.populaSelectDiaSemana();
                    that.populaSelectDisciplina();
                }
                if ($('#id_situacao').val() == 135 && G2S.isEditing) {
                    that.ui.idTurma.attr('disabled', true);
                } else {
                    that.ui.idTurma.attr('disabled', false);
                }

            },
            success: function () {
                geraSelect(that.ui.idTurma, collection, 'st_turma', 'id_turma', that.model.get('id_turma'));
            }
        });

    },
    populaListaProfessores: function () {
        var that = this;
        var id_disciplina = this.ui.idDisciplina.val();
        var id_perfil = perfilProfessor;

        if (!id_perfil) {
            $.pnotify({
                type: 'danger',
                title: 'Erro!',
                text: 'Não é possível buscar professores. Perfil não encontrato.'
            })
        }


        if (id_disciplina) {
            var collectionProf = new VwProfessorDisciplinaCollection();
            collectionProf.url += '/id_disciplina/' + id_disciplina + '/id_perfil/' + id_perfil;
            collectionProf.fetch({
                beforeSend: function () {
                    loading();
                    that.$el.find('#container-professores').empty();
                },
                complete: function () {

                    loaded();
                },
                success: function () {
                    var viewProfessores = new ProfessoresCollectionView({
                        el: that.$el.find('#container-professores'),
                        collection: collectionProf
                    });
                    viewProfessores.render();

                    //marca como selecionado o checkbox
                    if (that.model.get('id_professor')) {
                        that.$el.find('input[name="id_professor"][value="' + that.model.get('id_professor') + '"]').attr('checked', 'checked');
                    }

                }
            });
        }
    },
    populaSelectTurno: function () {
        var that = this;
        var collectionTurno = new TurnoCollection();
        collectionTurno.url += '/id_projeto/' + this.ui.idProjeto.val();
        collectionTurno.fetch({
            beforeSend: function () {
                that.ui.idTurno.empty().html('<option value="">Selecione</option>');
                loading();
            },
            complete: function () {
                if ($('#id_situacao').val() == 135 && G2S.isEditing) {
                    that.ui.idTurno.attr('disabled', true);
                } else {
                    that.ui.idTurno.attr('disabled', false);
                }
                if (that.model.get('id_turno')) {
                    that.populaSelectTurma();
                }
                loaded();
            },
            success: function () {
                geraSelect(that.ui.idTurno, collectionTurno, 'st_turno', 'id_turno', that.model.get('id_turno'));
            }
        });


    },
    populaSelectUnidade: function () {
        var that = this;
        var collectionUnidades = new UnidadesCollection();
        collectionUnidades.url += 'id_projeto/' + this.ui.idProjeto.val();
        collectionUnidades.fetch({
            beforeSend: function () {
                that.ui.idUnidade.empty().html('<option value="">Selecione</option>');
                loading();
            },
            complete: function () {
                if (that.model.get('id_unidade')) {
                    that.populaSelectTurno();
                }
                if ($('#id_situacao').val() == 135 && G2S.isEditing) {
                    that.ui.idUnidade.attr('disabled', true);
                } else {
                    that.ui.idUnidade.attr('disabled', false);
                }

                loaded();
            },
            success: function () {
                geraSelect(that.ui.idUnidade, collectionUnidades, 'st_nomeentidade', 'id_entidade', that.model.get('id_unidade'));
            }
        });
    },
    populaSelectDiaSemana: function () {
        var that = this;
        //Dias da semana
        var collectionDiaSemana = new DiaSemanaCollection();
        collectionDiaSemana.url += "/id_turma/" + this.ui.idTurma.val();
        collectionDiaSemana.fetch({
            beforeSend: function () {
                loading();
                periodoCollection = new PeriodoEncontroCollection();
                //periodoCollection.reset();
                that.ui.idDiaSemana.empty().html('<option value="">Selecione</option>');
            },
            complete: function () {
                //quando completar o fetch verifica se a model principal tem o id_diasemana
                if (that.model.get('id_diasemana')) {
                    //procura o dia da semana na collection
                    var diaSemana = collectionDiaSemana.findWhere({id_diasemana: that.model.get('id_diasemana')});

                    //monta a string
                    var textDiaSemana = diaSemana.get('st_tipoaula') + diaSemana.get('st_diasemana') + ' - ' + that.model.get('dt_diasemana');

                    //procura os elementos com o value e text e seta como selecionado
                    that.ui.idDiaSemana.find('option[value="' + that.model.get('dt_diasemana') + '"]:contains(' + textDiaSemana + ')').prop('selected', true);

                }

                if ($('#id_situacao').val() == 135 && G2S.isEditing) {
                    that.ui.idDiaSemana.attr('disabled', true);
                } else {
                    that.ui.idDiaSemana.attr('disabled', false);
                }

                that.ui.idDiaSemana.select2({
                    //minimumInputLength: 5,
                    placeholder: "Selecione"
                });

                loaded();
            },
            success: function () {

                //calcula a quantidadde de dia entre a data inicial e a data finala da grade horaria
                var qtdDias = diferencaEntreDatas(gradeViewModel.get('dt_iniciogradehoraria'), gradeViewModel.get('dt_fimgradehoraria'));
                //faz o loop de acordo com a quantidade de dia no intervalo sempre incrementado um na contagem
                for (var i = 0; i <= qtdDias; i++) {
                    //calcula a proxima data
                    var dt_periodo = proximaDataVencimento(gradeViewModel.get('dt_iniciogradehoraria'), i, 'd');

                    //pega o dia da semana da data
                    var id_diasemana = (dt_periodo.getDay() == 0 ? 7 : dt_periodo.getDay());
                    var diaSemana = collectionDiaSemana.where({id_diasemana: id_diasemana});

                    if (diaSemana) {
                        //seta os valores na model do periodo
                        
                        _.each(diaSemana, function (elem, index) {
                            
                            if(elem.get('id_tipoaula') != 1){
                                var st_texto = elem.get('st_tipoaula') + ' - ' + elem.get('st_diasemana')+' - '+ elem.get('st_turno') + " - " + formatarDataParaString(dt_periodo)
                            }else{
                                var st_texto = elem.get('st_diasemana') +' - '+ elem.get('st_turno') + " - " + formatarDataParaString(dt_periodo)
                            }

                            var periodoModel = new PeriodoEncontroModel({
                                'id_diasemana': elem.get('id_diasemana'),
                                'st_diasemana': elem.get('st_diasemana'),
                                'st_tipoaula': elem.get('st_tipoaula'),
                                'dt_periodo': formatarDataParaString(dt_periodo)+'-'+elem.get('id_turno')+'-'+elem.get('id_tipoaula'),
                                'st_texto': st_texto
                            });

                            //adiciona a model na collection
                            periodoCollection.add(periodoModel);

                        })

                    }
                }
                geraSelect(that.ui.idDiaSemana, periodoCollection, 'st_texto', 'dt_periodo', null, false);
            }
        });


    },
    renderizaListaHorarios: function () {
        loading();
        if (gradeViewModel.id) {
            var that = this;
            listaHorariosCollection.url = '/grade-horaria/get-lista-itens-grade/id_gradehoraria/' + gradeViewModel.id;
            listaHorariosCollection.reset();
            listaHorariosCollection.fetch({
                beforeSend: function () {

                },
                complete: function () {

                },
                success: function () {
                    var listaView = new HorariosCompositeView({
                        collection: listaHorariosCollection
                    });
                    telaGrade.horarios.show(listaView);
                }
            });
        }
        loaded();
    }
    ,
    imprimirGrade: function () {
        window.open('/relatorio-grade-projeto-entidade/retorno-relatorio-print/id_gradehoraria/' + gradeViewModel.get('id_gradehoraria'), 'janela', 'width=800, height=600, top=100, left=100, scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no');
    }
    ,
    abreModal: function () {
        var collectionTirinha = new ProfessoresTirinhaCollection();
        collectionTirinha.url += '/id_gradehoraria/' + gradeViewModel.get('id_gradehoraria');
        collectionTirinha.fetch({
            beforeSend: function () {
                loading();
            },
            complete: function () {
                loaded();
            },
            success: function () {
                var view = new TelaModalTirinhasCompositeView({
                    collection: collectionTirinha
                });
                telaGrade.modal.show(view);
            }
        });
    }
});


/**
 ******************************************************************************************
 ******************************************************************************************
 ******************************** PROFESSORES *********************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var ProfessoresEmptyView = Marionette.ItemView.extend({
    template: '#template-professores-lista-vazia',
    tagName: 'tr'
});

var ProfessoresItemView = Marionette.ItemView.extend({
    template: '#template-row-professores',
    tagName: 'tr',
    abreAgenda: function () {
        if (gradeViewModel.get('id_gradehoraria'))
            window.open("/grade-horaria/agenda-professor/id_gradehoraria/" + gradeViewModel.get('id_gradehoraria') + '/id_professor/' + this.model.get('id_usuario'), '_blank');
    },
    events: {
        'click .btn-professor': 'abreAgenda'
    }
});

var ProfessoresCollectionView = Marionette.CollectionView.extend({
    tagName: 'table',
    className: 'table table-striped table-hover table-bordered',
    emptyView: ProfessoresEmptyView,
    childView: ProfessoresItemView
});


/**
 ******************************************************************************************
 ******************************************************************************************
 *********************************** TIRINHAS *********************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var ItemProfessoresTirinhasItemView = Marionette.ItemView.extend({
    template: '#tempalte-itens-tirinhas',
    tagName: 'tr',
    events: {
        'click .brn-imprimir-tirinha': 'imprimeTirinha',
        'click .btn-enviar-email': 'enviarTirinha'
    },
    abrirPopup: function (url) {
        window.open(url, 'janela', 'width=800, height=600, top=100, left=100, scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no');
    },
    imprimeTirinha: function () {
        var url = "/grade-horaria/agenda-professor/pdf/1/id_gradehoraria/" + gradeViewModel.get('id_gradehoraria') + '/id_professor/' + this.model.get('id_usuario');
        this.abrirPopup(url);
    },
    enviarTirinha: function () {
        var url = "/grade-horaria/enviar-agenda-professor-email/";
        $.ajax({
            dataType: 'json',
            type: 'get',
            url: url,
            data: {
                id_gradehoraria: gradeViewModel.get('id_gradehoraria'),
                id_professor: this.model.get('id_usuario')
            },
            beforeSend: function () {
                loading();
            },
            success: function (data) {
                $.pnotify({
                    type: data.type,
                    title: data.title,
                    text: data.text
                });
            },
            complete: function () {
                loaded();
            },
            error: function (response) {
                var textResponse = JSON.parse(response.responseText);
                $.pnotify(textResponse);
            }
        });
    }
});

var TelaModalTirinhasCompositeView = Marionette.CompositeView.extend({
    template: "#tempalte-modal-tirinhas",
    tagName: 'div',
    className: 'modal hide fade',
    childView: ItemProfessoresTirinhasItemView,
    emptyView: ProfessoresEmptyView,
    childViewContainer: 'tbody',
    events: {
        'click #btn-enviar-todos': 'enviarEmailProfessores',
        'click #btn-imprimir-todas-tirinhas': 'imprimirTodasTirinhas'
    },
    onShow: function () {
        this.$el.modal('show');
        return this;
    },
    imprimirTodasTirinhas: function(){
        if (this.collection.length) {
            var that = this;
            var arrProfessores = [];
            this.collection.each(function (model, i) {
                arrProfessores[i] = model.get('id_usuario');
            });

            var url = "/grade-horaria/imprimir-todas-tirinhas/";

            window.open("/grade-horaria/imprimir-todas-tirinhas/id_gradehoraria/" + gradeViewModel.get('id_gradehoraria') + '/professores/' + arrProfessores, '_blank');
        }
    },
    enviarEmailProfessores: function () {
        if (this.collection.length) {
            var that = this;
            var arrProfessores = [];
            this.collection.each(function (model, i) {
                arrProfessores[i] = model.get('id_usuario');
            });

            var url = "/grade-horaria/enviar-emails-professores-em-cadeia/";
            $.ajax({
                dataType: 'json',
                type: 'get',
                url: url,
                data: {
                    id_gradehoraria: gradeViewModel.get('id_gradehoraria'),
                    professores: arrProfessores
                },
                beforeSend: function () {
                    loading();
                },
                success: function (data) {
                    $.pnotify({
                        type: data.type,
                        title: data.title,
                        text: data.text
                    });
                    if (data.type == 'success') {
                        that.$el.modal('hide');
                    }
                },
                complete: function () {
                    loaded();
                },
                error: function (response) {
                    var textResponse = JSON.parse(response.responseText);
                    $.pnotify(textResponse);
                }
            });
        }
    }
});

/**
 ******************************************************************************************
 ******************************************************************************************
 *************************** LISA DE HORARIOS *********************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var EmptyView = Marionette.ItemView.extend({
    template: '#template-lista-vazia',
    tagName: 'tr'
});

var HorarioItemView = Marionette.ItemView.extend({
    template: '#template-row-item-grade-turma',
    tagName: 'tr',
    apagar: function () {
        var that = this;
        bootbox.confirm("Tem certeza de que deseja apagar o registro?", function (result) {
            if (result) {


                that.model.destroy({
                    beforeSend: function () {
                        loading();
                    },
                    complete: function () {
                        thatGradeHorariaCompositeView.renderizaListaHorarios();
                        if(thatGradeHorariaCompositeView.ui.idProjeto.val() && thatGradeHorariaCompositeView.ui.idUnidade.val() && thatGradeHorariaCompositeView.ui.idTurma.val()){
                            thatGradeHorariaCompositeView.populaSelectDisciplina();
                        }
                        loaded();
                    },
                    success: function () {

                        that.remove();
                        $.pnotify({
                            title: 'Registro Removido',
                            text: 'O registro foi removido com sucesso!',
                            type: 'success'
                        });
                    },
                    error: function (model, response) {
                        $.pnotify({
                            title: 'ERRO!',
                            text: response.responseText,
                            type: 'error'
                        });
                    }
                });

            }
        });
    },

    editarItem: function () {
        thatGradeHorariaCompositeView.editarItem(this.model.id);
    },
    events: {
        'click .btn-remover-item': 'apagar',
        'click .btn-editar-item': 'editarItem'
    }
});

var HorariosCompositeView = Marionette.CompositeView.extend({
    template: '#template-lista-horarios',
    tagName: 'fieldset',
    emptyView: EmptyView,
    childView: HorarioItemView,
    childViewContainer: 'tbody',
    onShow: function () {
        return this;
    }
});


/**
 ******************************************************************************************
 ******************************************************************************************
 ************************************* TURMAS *********************************************
 ******************************************************************************************
 ******************************************************************************************
 */

var TurmaItemView = Marionette.ItemView.extend({
    template: '#template-row-turma',
    tagName: 'tr'
});

var TurmaCollectionView = Marionette.CollectionView.extend({
    tagName: 'table',
    className: 'table table-striped table-hover table-bordered',
    emptyView: ProfessoresEmptyView,
    childView: TurmaItemView
});

/**
 ******************************************************************************************
 ******************************************************************************************
 ******************************** SHOW LAYOUT *********************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var telaGrade = new TelaGradeLayout();
G2S.show(telaGrade);

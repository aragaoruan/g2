var esquemaPerguntaModel, categoriaPerguntaModel;
var categoriasCollection, perguntaCollection;
var viewFormCategoria, viewCategoriasAbas, viewFormEsquema;

/**
 * setando o atributo de "chave primaria"
 */
var EsquemaPerguntaModel = EsquemaPergunta.extend({
    idAttribute: 'id_esquemapergunta'
});

var PerguntaModel = Pergunta.extend({
    idAttribute: 'id_pergunta'
});

var CategoriaPerguntaModel = CategoriaPergunta.extend({
    idAttribute: 'id_categoriapergunta',
    isEdit: false
});

/**
 * collection para pergutnas
 */
var PerguntasCollection = Backbone.Collection.extend({
    model: PerguntaModel,
    url: '/api/pergunta'
});

/**
 * Collection para categorias, lista e abas.
 */
var CategoriasPerguntaCollection = Backbone.Collection.extend({
    model: CategoriaPerguntaModel,
    url: '/api/categoria-pergunta'
});


/**
 * Layout com as regiões da tela
 */
var EsquemaLayout = Marionette.LayoutView.extend({
    template: '#layout-esquema-template',
    tagName: 'div',
    regions: {
        boxFormEsquema: '#box-form-esquema',
        boxCategorias: '#box-categorias',
    },
    events: {
        'click #btn-salvar': 'salvarEsquema'
    },
    salvarEsquema: function () {

        if (!viewFormEsquema.ui.stEsquemaPergunta.val().length) {
            $.pnotify({
                title: "Atenção!",
                type: 'warning',
                text: 'Não é possível salvar dados sem o nome do esquema.'
            });
            return false;
        } else {
            var that = this;
            esquemaPerguntaModel.set({
                st_esquemapergunta: viewFormEsquema.ui.stEsquemaPergunta.val(),
                bl_ativo: 1
            });
            esquemaPerguntaModel.save(null, {
                beforeSend: function () {
                    loading();
                },
                complete: function () {
                    loaded();
                },
                success: function (response) {

                    that.salvarCategorias();


                    $.pnotify({
                        type: 'success',
                        title: 'Sucesso!',
                        text: 'Esquema salvo com sucesso.'
                    });
                },
                error: function () {
                    loaded();
                    $.pnotify({
                        type: 'error',
                        title: 'Erro!',
                        text: 'Houve um erro ao tentar salvar o Esquema.'
                    });
                }
            });
        }


    },
    salvarCategorias: function () {
        var idEsquema = esquemaPerguntaModel.get('id_esquemapergunta');
        if (idEsquema && categoriasCollection.length) {
            var that = this;
            categoriasCollection.each(function (model) {
                model.set('id_esquemapergunta', idEsquema);
                if (model.get('isNew')) {
                    model.set('id_categoriapergunta', '');
                }
            });

            Backbone.sync('create', categoriasCollection, {
                    url: 'perguntas-frequentes/salvar-array-categorias',
                    beforeSend: function () {
                        loading();
                    },
                    success: function (response) {
                        that.salvarPerguntas(response);
                        categoriasCollection.set(response);
                        $.pnotify({
                            title: 'Sucesso!',
                            text: 'Categorias salva com sucesso.',
                            type: 'success'
                        });

                    },
                    error: function () {
                        loaded();
                        $.pnotify({
                            title: 'Erro!',
                            text: 'Houve um erro ao tentar salvar as Categorias.',
                            type: 'error'
                        });
                    },
                    complete: function () {
                        loaded();
                    }
                }
            );

        }
    },
    salvarPerguntas: function (categorias) {
        var collectionCategoria = new CategoriasPerguntaCollection(categorias);
        if (perguntaCollection.length) {
            perguntaCollection.each(function (model) {
                var categoria = collectionCategoria.findWhere({'hashId': model.get('hashId')});
                if (categoria) {
                    model.set('id_categoriapergunta', categoria.get('id_categoriapergunta'));
                    if (model.get('hasNew')) {
                        model.set('id_pergunta', "");
                    }
                }
            });

            Backbone.sync('create', perguntaCollection, {
                url: 'perguntas-frequentes/salvar-array-perguntas',
                beforeSend: function () {
                    loading();
                },
                success: function (response) {
                    perguntaCollection.set(response);
                    $.pnotify({
                        title: 'Sucesso!',
                        text: 'Perguntas salva com sucesso.',
                        type: 'success'
                    });
                },
                error: function () {
                    loaded();
                    $.pnotify({
                        title: 'Erro!',
                        text: 'Houve um erro ao tentar salvar as Perguntas.',
                        type: 'error'
                    });
                },
                complete: function () {
                    loaded();
                }
            });
        }

    },
    onShow: function () {
        this.renderizaFormEsquema();
        this.renderizaBoxCategorias();
        return this;
    }
    ,
    renderizaBoxCategorias: function () {
        var boxCategorias = new CategoriasLayout();
        this.boxCategorias.show(boxCategorias);
    }
    ,
    renderizaFormEsquema: function () {
        esquemaPerguntaModel = new EsquemaPerguntaModel(G2S.editModel);
        viewFormEsquema = new EsquemaPerguntaView({
            model: esquemaPerguntaModel
        });
        this.boxFormEsquema.show(viewFormEsquema);
    }
});

/**
 * View para formulário do esquema
 */
var EsquemaPerguntaView = Marionette.ItemView.extend({
    template: '#form-esquema-template',
    className: "well well-small",
    ui: {
        stEsquemaPergunta: 'input[name="st_esquemapergunta"]'
    }
});

/**
 * Layout para parte de baixo, formulário e abas de categorias
 */
var CategoriasLayout = Marionette.LayoutView.extend({
    template: '#container-categorias-template',
    tagName: 'div',
    className: 'well well-small',
    regions: {
        boxFormCategoria: '#box-form-categoria',
        boxCategoriaList: '#box-categorias-list',
    },
    onShow: function () {
        this.renderizaFormCategoria();
        this.renderizaCategoriasList();
        return this;
    },
    renderizaFormCategoria: function () {
        categoriaPerguntaModel = new CategoriaPerguntaModel();
        viewFormCategoria = new FormCategoriaView({model: categoriaPerguntaModel});
        this.boxFormCategoria.show(viewFormCategoria);
    },
    renderizaCategoriasList: function () {
        categoriasCollection = new CategoriasPerguntaCollection();
        var that = this;
        if (esquemaPerguntaModel.get('id_esquemapergunta')) {
            categoriasCollection.url += "?id_esquemapergunta=" + esquemaPerguntaModel.get('id_esquemapergunta')
            categoriasCollection.fetch({
                beforeSend: function () {
                    loading();
                },
                complete: function () {
                    loaded();
                },
                success: function () {
                    if (categoriasCollection.length) {
                        categoriasCollection.at(0).set('isActive', 1);
                    }
                    viewCategoriasAbas = new CategoriasAbasView({
                        collection: categoriasCollection
                    });
                    that.boxCategoriaList.show(viewCategoriasAbas);
                },
                error: function () {
                    $.pnotify({
                        title: 'Erro!',
                        text: 'Houve um erro ao tentar carregar categorias.',
                        type: 'error'
                    });
                }
            })
        } else {
            viewCategoriasAbas = new CategoriasAbasView({
                collection: categoriasCollection
            });
            that.boxCategoriaList.show(viewCategoriasAbas);
        }

    }
});

/**
 * Formulário para inserir para categoria
 */
var FormCategoriaView = Marionette.ItemView.extend({
    template: '#form-categoria-template',
    ui: {
        stCategoriaPergunta: "input[name='st_categoriapergunta']",
        btnSalvar: 'button.btn-adicionar',
        btnCancelar: 'button.btn-cancelar',
        btnEditar: 'button.btn-editar'
    },
    events: {
        'submit #form-categoria': function (e) {
            e.preventDefault();
            if (this.model.isEdit) {
                this.editarCategoria();
            } else {
                this.adicionarCategoria();
            }
        },
        'click button.btn-adicionar': 'adicionarCategoria',
        'click button.btn-editar': 'editarCategoria',
        'click button.btn-cancelar': 'cancelarEdicao',
        'keyup input[name="st_categoriapergunta"], keydown input[name="st_categoriapergunta"]': 'toggleBtnAdicionar',
        'change input[name="st_categoriapergunta"], blur input[name="st_categoriapergunta"]': 'toggleBtnAdicionar'
    },
    onRender: function () {
        this.toggleShowBtns();
        return this;
    },
    cancelarEdicao: function () {
        this.model = new CategoriaPerguntaModel();
        this.render();
    },
    toggleShowBtns: function () {
        if (this.model.isEdit) {
            this.ui.btnSalvar.hide();
            this.ui.btnEditar.show().removeAttr('disabled');
            this.ui.btnCancelar.show().removeAttr('disabled');
        } else {
            this.ui.btnCancelar.hide();
            this.ui.btnEditar.hide();
            this.ui.btnSalvar.show();
        }
    },
    toggleBtnAdicionar: function () {
        if (this.ui.stCategoriaPergunta.val().length) {
            if (!this.model.isEdit) {
                this.ui.btnSalvar.removeAttr('disabled');
            } else {
                this.ui.btnEditar.removeAttr('disabled');
            }
        } else {
            if (!this.model.isEdit) {
                this.ui.btnSalvar.attr('disabled', 'disabled');
            } else {
                this.ui.btnEditar.attr('disabled', 'disabled');
            }
        }
    },
    editarCategoria: function () {
        var nomeCategoria = this.ui.stCategoriaPergunta.val();
        if (!nomeCategoria.length) {
            $.pnotify({
                title: 'Atenção!',
                type: 'warning',
                text: 'Não é possível editar uma categoria sem nome.'
            });
        } else {

            var modelEdited = viewCategoriasAbas.collection.findWhere({hashId: this.model.get('hashId')});
            modelEdited.set('st_categoriapergunta', nomeCategoria);

            this.model = new CategoriaPerguntaModel();
            //this.ui.stCategoriaPergunta.val('');
            this.render();
        }
    },
    adicionarCategoria: function (e) {
        //e.preventDefault();
        var nomeCategoria = this.ui.stCategoriaPergunta.val();
        if (!nomeCategoria.length) {
            $.pnotify({
                title: 'Atenção!',
                type: 'warning',
                text: 'Não é possível adicionar uma categoria sem nome.'
            });
        } else {
            this.model = new CategoriaPerguntaModel();
            var hash = this.gerarHashId();
            this.model.set({
                bl_ativo: 1,
                st_categoriapergunta: nomeCategoria,
                hashId: hash,
                isActive: 1,
                id_categoriapergunta: hash,
                isNew: 1
            });

            if (categoriasCollection.length) {
                //var that = this;
                categoriasCollection.each(function (model) {
                    //if (model.get('hashId') != that.model.get('hashId')) {
                    model.set('isActive', 0);
                    //}
                });
            }
            categoriasCollection.add(this.model);
            //viewCategoriasAbas.render();
            this.ui.stCategoriaPergunta.val('');

        }
    },
    gerarHashId: function () {
        var date = new Date();
        var string = String(date.getFullYear())
            + String(date.getMonth())
            + String(date.getDate())
            + String(date.getHours())
            + String(date.getMinutes())
            + String(date.getSeconds())
            + String(date.getMilliseconds());
        return string;
    }
});

/**
 * View para criar aba quando não houver categorias
 */
var ItensAbaEmptyView = Marionette.ItemView.extend({
    template: _.template('<a href="#" data-toggle="tab">Sem Categoria</a>'),
    tagName: 'li',
    className: 'active'
});

/**
 * View para abas de categorias
 */
var ItensAbaView = Marionette.ItemView.extend({
    template: "#item-aba-categoria-template",
    tagName: 'li',
    modelEvents: {
        'change:st_categoriapergunta': function () {
            this.render();
        }
    },
    onRender: function () {
        if (this.model.get('isActive')) {
            this.$el.addClass('active');
            this.$el.find('.btn-close').show();
        } else {
            this.$el.removeClass('active');
            this.$el.find('.btn-close').hide();
        }
        this.$el.find('.btn-close').css({'cursor': 'pointer'});
        return this;
    },
    events: {
        'click a': 'toggleBtnExcluir',
        'click a span.btn-close': 'apagarCategoria'
    },
    apagarCategoria: function () {
        var that = this;
        bootbox.confirm("Tem certeza de que deseja remover esta categoria?", function (result) {
            if (result) {
                that.model.destroy({
                    beforeSend: function () {
                        loading();
                    },
                    complete: function () {
                        loaded();
                    },
                    success: function () {
                        that.remove();
                        $.pnotify({
                            title: 'Registro Removido!',
                            text: 'O registro foi removido com sucesso!',
                            type: 'success'
                        });
                    },
                    error: function (model, response) {
                        $.pnotify({
                            title: 'ERRO!',
                            text: "Houve um erro ao tentar remover registro. " + response.responseText,
                            type: 'error'
                        });
                    }
                });
            }
        });
    },
    toggleBtnExcluir: function () {
        $.each(this.$el.parent('ul').find('li'), function (index, obj) {
            $(obj).find('.btn-close').hide();
        });
        this.$el.find('.btn-close').show();
    }

});

/**
 * View geral para parte de categorias
 */
var CategoriasAbasView = Marionette.CompositeView.extend({
    template: "#categorias-list-template",
    tagName: 'div',
    className: 'well well-small',
    emptyView: ItensAbaEmptyView,
    childView: ItensAbaView,
    childViewContainer: 'ul.nav-tabs',
    collectionEvents: {
        'add': function () {
            this.renderizaContainerPerguntas(false);
        }
    },
    //modelEvents: {
    //    'change:st_nomecategoriapergunta': function () {
    //        debug('mudou o nome')
    //    }
    //},
    ui: {
        containerPerguntas: 'div.tab-content'
    },
    onShow: function () {
        this.$el.find('div').find('ul').css({'margin-right': '0'});
        perguntaCollection = new PerguntasCollection();
        if (this.collection.length && esquemaPerguntaModel.get('id_esquemapergunta')) {
            this.renderizaContainerPerguntas(true);
        }
    },
    onRender: function () {
        //if (this.collection.length && esquemaPerguntaModel.get('id_esquemapergunta')) {
        //    this.renderizaContainerPerguntas(true);
        //}
    },
    renderizaContainerPerguntas: function (exec_fetch) {
        var that = this;
        if (this.collection.length) {
            //Instancia a collection de perguntas
            that.ui.containerPerguntas.empty();
            this.collection.each(function (model) {
                var perguntaCollectionView = new PerguntasCollection();
                //if (esquemaPerguntaModel.get('id_esquemapergunta')) {
                if (esquemaPerguntaModel.get('id_esquemapergunta') && exec_fetch) {
                    perguntaCollectionView.url += "?id_categoriapergunta=" + model.get('id_categoriapergunta');
                    perguntaCollectionView.fetch({
                        beforeSend: function () {
                            loading();
                        },
                        complete: function () {
                            loaded();
                        },
                        success: function () {
                            perguntaCollection.add(perguntaCollectionView.models);
                            var itemContaineCategoria = new ItemContainerCategoriaView({
                                model: model,
                                collection: perguntaCollectionView
                            });
                            that.ui.containerPerguntas.append(itemContaineCategoria.render().$el);
                        },
                        error: function () {
                            $.pnotify({
                                title: 'Erro!',
                                text: 'Houve um erro ao tentar carregar perguntas.',
                                type: 'error'
                            });
                        }
                    });
                } else {
                    that.$el.find('div').find('ul').find('li')
                        .find("a[href='#" + model.get('hashId') + "']")
                        .parent('li.active').removeClass('active');
                    var perguntas = perguntaCollection.where({hashId: model.get('hashId')});
                    if (perguntas) {
                        perguntaCollectionView.add(perguntas);
                    }

                    var itemContaineCategoria = new ItemContainerCategoriaView({
                        model: model,
                        collection: perguntaCollectionView
                    });
                    that.ui.containerPerguntas.append(itemContaineCategoria.render().$el);
                }


            });
        }
    }
});


/**
 * Item view para cada formulário de pergunta
 */
var ItemPerguntaView = Marionette.ItemView.extend({
    template: "#item-container-pergunta-template",
    className: 'well',
    ui: {
        stPergunta: 'textarea[name="st_pergunta"]',
        stResposta: 'textarea[name="st_resposta"]'
    },
    events: {
        'click .btn-remover-pergunta': 'removerPergunta',
        'keyup textarea[name="st_pergunta"], keydown textarea[name="st_pergunta"]': 'setStPergunta',
        'keyup textarea[name="st_resposta"], keydown textarea[name="st_resposta"]': 'setStResposta'
    },
    setStPergunta: function () {
        this.model.set('st_pergunta', this.ui.stPergunta.val());
    },
    setStResposta: function () {
        this.model.set('st_resposta', this.ui.stResposta.val());
    },
    removerPergunta: function () {
        var that = this;
        bootbox.confirm("Tem certeza de que deseja remover esta pergunta?", function (result) {
            if (result) {
                if (that.model.get('hasNew')) {
                    that.model.set('id_pergunta', '');
                    that.model.clear();
                }
                that.model.destroy({
                    beforeSend: function () {
                        loading();
                    },
                    complete: function () {
                        loaded();
                    },
                    success: function () {
                        that.remove();
                        $.pnotify({
                            title: 'Registro Removido!',
                            text: 'O registro foi removido com sucesso!',
                            type: 'success'
                        });
                    },
                    error: function (model, response) {
                        $.pnotify({
                            title: 'ERRO!',
                            text: "Houve um erro ao tentar remover registro. " + response.responseText,
                            type: 'error'
                        });
                    }
                });
            }
        });
    }

});

/**
 * Item view para quando não houver perguntas
 */
var PerguntaEmptyView = Marionette.ItemView.extend({
    template: "#item-pergunta-empty-template"
});


/**
 * View para adicioanr os containers das abas
 */
var ItemContainerCategoriaView = Marionette.CompositeView.extend({
    template: "#container-item-categoria-template",
    tagName: 'div',
    className: ' tab-pane',
    //emptyView: PerguntaEmptyView,
    childView: ItemPerguntaView,
    events: {
        'click .btn-add-pergunta': 'adicionarPergunta',
        'click .btn-edit-categoria': 'editarCategoria'
    },
    onRender: function () {
        this.$el.attr('id', this.model.get('hashId')).css({
            'background-color': '#fff',
            'padding': '10px'
        });
        if (this.model.get('isActive')) {
            this.$el.addClass('active');
        } else {
            this.$el.removeClass('active');
        }
        return this;
    },
    adicionarPergunta: function () {

        var perguntaModel = new PerguntaModel();
        var nu_ordem = perguntaCollection.length;
        perguntaModel.set({
            bl_ativo: 1,
            hashId: this.model.get('hashId'),
            id_pergunta: ++nu_ordem,
            hasNew: 1
        });

        var viewPergunta = new ItemPerguntaView({
            model: perguntaModel
        });

        if (!perguntaCollection.length) {
            this.$el.find('.box-container-perguntas').empty();
        }
        this.$el.find('.box-container-perguntas').append(viewPergunta.render().$el);
        perguntaCollection.add(perguntaModel);
    },
    editarCategoria: function () {
        viewFormCategoria.model.set(this.model.toJSON());
        viewFormCategoria.model.isEdit = true;
        viewFormCategoria.render();
    }
});


var telaEsquema = new EsquemaLayout();
G2S.show(telaEsquema);

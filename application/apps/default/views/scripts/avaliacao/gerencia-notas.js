/* 
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 * JS para funcionalidade Gerencia Notas
 */

$.ajaxSetup({async: false});

//Definicao de variaveis estatica para id_situacao para tabela avaliacao aluno
var SituacaoAvaliacaoAluno = {
    ATIVA: 86,
    INATIVA: 87,
    REVISADA: 88
};

//Definicao de variaveis estaticas para tipos de avaliacao
var TipoAvaliacao = {
    NORMAL: 1,
    RECUPERACAO: 2,
    PROVA_PRESENCIAL: 4,
    ATIVIDADE_DISTANCIA: 5,
    TCC: 6,
    CONCEITUAL: 7
};

//Definicao de variaveis estaticas para tipo de notas
var TipoNota = {
    NORMAL: 1,
    CREDITO: 2
};

var notaConceitual = {
    CONCEITUAL: 2,
    NAO_CONCEITUAL: 1
};

var permissaoAlterarNotas = false;

var VwUsuarioMatriculaProjetoCollectionExtend = Backbone.Collection.extend({
    url: '/api/vw-usuario-matricula-projeto/',
    model: VwUsuarioMatriculaProjeto
});

var VwAvaliacaoAlunoCollectionExtend = Backbone.Collection.extend({
    url: '/api/vw-avaliacao-aluno',
    model: VwAvaliacaoAluno.extend({
        //idAttribute: 'id_avaliacaoaluno'
        idAttribute: ['id_disciplina', 'id_avaliacao']
    }),
    // Por algum BUG do backbone, o collection nao ta filtrando corretamente o idAtributte, pra nao repetir, entao o parse foi necessario
    parse: function (response) {
        var results = [];
        var added = [];

        $.each(response, function (i, obj) {
            var index = (obj.id_disciplina || '') + '' + (obj.id_avaliacao || '') + '' + (obj.id_avaliacaoaluno || '');
            if (added.indexOf(index) == -1) {
                results.push(obj);
                added.push(index);
            }
        });

        return results;
    }
});

var VwAvaliacaoAlunoRegistradaCollectionExtend = Backbone.Collection.extend({
    url: '/api/vw-avaliacao-aluno-registrada',
    model: VwAvaliacaoAlunoRegistrada
});

/**
 * Variaveis globais utilizadas para definir mesclagem de celulas da grid
 * quando nome da disciplina se repetir
 */
var rowBack = 0;
var rowspan = 0;

//Item view para disciplinas
var AvaliacaoItemViewExtend = Marionette.ItemView.extend({
    tagName: 'tr',
    template: '#template-grid-tbody',
    ui: {
        itemFormLancarNota: '.item-form-lancar-nota',
        itemBtnEditar: '.btn-editar',
        itemBtnVoltar: '.btn-voltar',
        formUpload: '.form-upload',
        itemBtnLancarAproveitamento: '.btn-lancar-aproveitamento',
        itemSpanTituloAvaliacao: '.itemSpanTituloAvaliacao',
        itemDivInputTituloAvaliacao: '.itemDivInputTituloAvaliacao',
        itemFormActions: '.item-form-actions',
        itemLinkStUpload: '.itemLinkStUpload',
        inputTituloAvaliacao: '.input-titulo-avaliacao',
        inputNotaNormal: '.input-nota-normal',
        inputNotaAproveitamento: '.input-nota-aproveitamento',
        inputDtCadastroAvaliacao: '.input-dt-cadastro-avaliacao',
        itemFormLancarAproveitamento: '.item-form-lancar-aproveitamento',
        inputConceitoNota: 'input-conceito'
    },
    bl_showtd: true,
    initialize: function () {

        if (rowBack == this.model.get('id_disciplina')) {
            this.model.set('st_tituloexibicaodisciplina', '');
            this.bl_showtd = false;
        } else {
            rowspan = this.model.collection.where({id_disciplina: this.model.get('id_disciplina')}).length;
        }
        rowBack = this.model.get('id_disciplina');

        /**
         * Verificacao de existencia de data registrada para preenchimento
         * automatico do campo com data atual ou data registrada em banco
         */
        if (!this.model.get('dt_avaliacao')) {
            var dt = new Date();
            var date = [
                dt.getDate() > 9 ? dt.getDate() : "0" + dt.getDate(),
                parseInt(dt.getMonth() > 9 ? dt.getMonth() : "0" + dt.getMonth()) + 1,
                dt.getFullYear()
            ];

            this.model.set('st_dtavaliacao', date.join('/'));
        }
    },
    verificaPermissaoBotoes: function () {
        var idMatriculaSelected = GerenciarNotasView.ui.idMatriculaSelect.val();
        var modelMatriculaSelected = GerenciarNotasView.MatriculasCollection.findWhere({id_matricula: parseInt(idMatriculaSelected)});

        //Desabilita os botões NOTA e APROVEITAMENTO se o usuario não tiver permissão e se a ST_EVOLUCAO for 15 ou 16
        if (permissaoAlterarNotas) {
            this.ui.itemBtnLancarAproveitamento.attr("disabled", false);
            this.ui.itemBtnEditar.attr("disabled", false);
            return true;
        } else {
            if (modelMatriculaSelected.get("st_evolucao") == "Concluinte"
                || modelMatriculaSelected.get("st_evolucao") == "Certificado") {
                this.ui.itemBtnLancarAproveitamento.attr("disabled", true);
                this.ui.itemBtnEditar.attr("disabled", true);
                return false;
            }else{
                return true;
            }
        }
    },
    onRender: function () {

        if (!this.bl_showtd) {
            this.$el.find('td').first().addClass('td-remove');
            this.$el.find('td').first().css('vertical-align', 'bottom');
        } else {
            this.$el.find('td').first().addClass('td-rowspan');
            this.$el.find('td').first().css('vertical-align', 'bottom');
        }


        var that = this;
        that.verificaPermissaoBotoes()

    },
    validateModel: function () {
        if (this.model.get('id_tipoavaliacao') != TipoAvaliacao.CONCEITUAL) {
            this.model.validate = function (attrs, options) {
                if (!attrs.st_nota || !$.isNumeric(attrs.st_nota) || attrs.st_nota < 0) {
                    return MensagensG2.MSG_GERENCIA_NOTAS_01;
                }

                if (parseInt(attrs.st_nota) > parseInt(attrs.nu_notamax)) {
                    return sprintf(MensagensG2.MSG_GERENCIA_NOTAS_02, parseInt(attrs.nu_notamax));
                }
            };
        }
    },
    buscarStatusAgendamento: function () {
        return $.get('/avaliacao/verificar-status-nota', {
            id_matriculadisciplina: this.model.get('id_matriculadisciplina'),
            st_nota: this.model.get('st_nota')
        });
    },
    salvarAvaliacaoAluno: function () {
        var that = this;

        this.ui.formUpload.ajaxSubmit({
            url: '/avaliacao/salvar-avaliacao-aluno',
            data: {avaliacaoaluno: that.model.toJSON()},
            type: 'post',
            dataType: 'json',
            beforeSend: loading,
            complete: loaded,
            success: function (response) {
                that.model.set(response);
                $.pnotify({title: 'Alerta', text: MensagensG2.MSG_SAVE_SUCCESS, type: 'success'});
            },
            error: function () {
                $.pnotify({title: 'Alerta', text: MensagensG2.MSG_SAVE_ERROR, type: 'danger'});
            }
        });

        this.render();
    },
    events: {
        'click button.btn-editar': function () {
            if(!this.verificaPermissaoBotoes())
                return;
            this.ui.itemLinkStUpload.hide();
            this.ui.formUpload.show();
            this.ui.itemSpanTituloAvaliacao.hide();
            this.ui.itemDivInputTituloAvaliacao.show();
            this.ui.itemFormActions.toggle();
            this.ui.itemFormLancarNota.toggle();
        },
        'click button.btn-voltar': function () {
            this.ui.itemLinkStUpload.show();
            this.ui.formUpload.hide();
            this.ui.itemSpanTituloAvaliacao.show();
            this.ui.itemDivInputTituloAvaliacao.hide();
            this.ui.itemFormActions.toggle();
            this.ui.itemFormLancarNota.toggle();
        },
        'click .btn-salvar-nota-normal': function () {
            var that = this;

            //gerando os validates model
            this.validateModel();
            var avaConjRef = this.model.get('id_avaliacaoconjuntoreferencia');
            var conceitual = this.model.get('id_tipoavaliacao') === TIPO_AVALIACAO.CONCEITUAL;

            if (conceitual && $(".input-conceito" + avaConjRef).val() === 'Escolha') {
                $.pnotify({title: "Alerta", text: MensagensG2.MSG_CONCEITO});
                return false;
            }

            this.model.set({
                id_usuarioalteracao: G2S.loggedUser.get('id_usuario'),
                st_nota: conceitual ? null : this.ui.inputNotaNormal.val(),
                dt_avaliacao: this.ui.inputDtCadastroAvaliacao.val(),
                id_tiponota: TipoNota.NORMAL,
                id_situacao: SituacaoAvaliacaoAluno.ATIVA, //id_situacao tabela avaliacao aluno ativa
                id_notaconceitual: conceitual ? $(".input-conceito" + avaConjRef).val() : null
            });

            //Atributo setado somente quando avaliacao do tipo TCC
            if (this.model.get('id_tipoavaliacao') == TipoAvaliacao.TCC) {
                this.model.set('st_tituloavaliacao', this.ui.inputTituloAvaliacao.val());
            }

            if (!this.model.isValid()) {
                this.ui.inputNotaNormal.parent().addClass('error');
                $.pnotify({title: 'Alerta', text: this.model.validationError});
            } else {
                loading();
                this.buscarStatusAgendamento().done(function (response) {
                    if (response.id_situacaoagendamento == SITUACAO.TB_MATRICULA.AGENDAMENTO_APTO && !response.bl_satisfatorio) {
                        bootbox.confirm('O aluno encontra-se APTO ao agendamento de prova, caso a alteração seja realizada o aluno ficará NÃO APTO. Deseja continuar?', function (confirm) {
                            if (confirm) {
                                that.salvarAvaliacaoAluno();
                            }
                        });
                    } else {
                        that.salvarAvaliacaoAluno();
                    }
                    loaded();
                });
            }

        },
        'click .btn-lancar-aproveitamento': function () {
            if(!this.verificaPermissaoBotoes())
                return;

            this.ui.itemFormActions.toggle();
            this.ui.itemFormLancarAproveitamento.toggle();
        },
        'click .btn-voltar-form-aproveitamento': function () {
            this.ui.itemFormActions.toggle();
            this.ui.itemFormLancarAproveitamento.toggle();
        },
        'click .btn-salvar-nota-aproveitamento': function () {

            //gerando os validates model
            this.validateModel();

            this.model.set({
                id_usuarioalteracao: G2S.loggedUser.get('id_usuario'),
                st_nota: this.ui.inputNotaAproveitamento.val(),
                dt_avaliacao: this.ui.inputDtCadastroAvaliacao.val(),
                id_tiponota: TipoNota.CREDITO,
                id_situacao: SituacaoAvaliacaoAluno.REVISADA
            });

            if (!this.model.isValid()) {
                this.ui.inputNotaAproveitamento.parent().addClass('error');
                $.pnotify({title: 'Alerta', text: this.model.validationError, type: 'warning'});
            } else {
                loading();

                that = this;

                $.ajax({
                    url: '/avaliacao/salvar-avaliacao-aluno',
                    data: {avaliacaoaluno: that.model.toJSON()},
                    type: 'post',
                    dataType: 'json',
                    success: function (response) {
                        that.model.set(response);
                        $.pnotify({title: 'Alerta', text: MensagensG2.MSG_SAVE_SUCCESS, type: 'success'});
                    },
                    error: function () {
                        $.pnotify({title: 'Alerta', text: MensagensG2.MSG_SAVE_ERROR, type: 'success'});
                    }
                });

                this.render();
                loaded();
            }
        },
        'click .btn-excluir-nota': function () {

            this.model.set('bl_ativo', 0);
            this.model.set('id_situacao', SituacaoAvaliacaoAluno.INATIVA);
            this.model.set('id_usuarioalteracao', G2S.loggedUser.get('id_usuario'));
            that = this;

            bootbox.confirm(MensagensG2.MSG_QUESTION_OPERATION, function (result) {
                if (result) {
                    loading();

                    $.ajax({
                        url: '/avaliacao/editar-avaliacao-aluno',
                        data: that.model.toJSON(),
                        type: 'post',
                        dataType: 'json',
                        success: function (response) {
                            that.model.set(response);
                            $.pnotify({title: 'Alerta', text: MensagensG2.MSG_SAVE_SUCCESS, type: 'success'});
                        },
                        error: function () {
                            $.pnotify({title: 'Alerta', text: MensagensG2.MSG_SAVE_ERROR, type: 'danger'});
                        }
                    });

                    that.render();
                    loaded();
                }
            });
        },
        'click .btn-visualizar-historico': function () {

            loading();

            var currentModel = this.model;

            var VwAvaliacaoAlunoRegistradaCollection = new VwAvaliacaoAlunoRegistradaCollectionExtend();
            VwAvaliacaoAlunoRegistradaCollection.url += '/index' +
                '/id_avaliacao/' + this.model.get('id_avaliacao') +
                '/id_matricula/' + this.model.get('id_matricula') +
                '/id_avaliacaoconjuntoreferencia/' + this.model.get('id_avaliacaoconjuntoreferencia') +
                '?order[]=dt_alteracao=desc&order[]=id_avaliacaoaluno=desc&order[]=id_ordem=asc'
            VwAvaliacaoAlunoRegistradaCollection.fetch();

            (new ModalHistoricoCompositeViewExtend({
                model: currentModel,
                collection: VwAvaliacaoAlunoRegistradaCollection,
                el: '#container-modal-historico'
            }).render());

            loaded();
        }
    }
});


var ModalHistoricoItemViewExtend = Marionette.ItemView.extend({
    template: '#template-item-modal-historico',
    tagName: 'tr'
});

var ModalHistoricoCompositeViewExtend = Marionette.CompositeView.extend({
    template: '#template-modal-historico',
    childView: ModalHistoricoItemViewExtend,
    childViewContainer: '#table-modal-historico',
    onRender: function () {
        this.$el.modal('show');
    }
});

/**
 * EmptyView para Composite de produtos
 * @type {ItemView}
 */
var EmptyView = Marionette.ItemView.extend({
    template: '#template-lista-vazia',
    tagName: 'tr'
});

var AvaliacoesCompositeViewExtend = Marionette.CompositeView.extend({
    childView: AvaliacaoItemViewExtend,
    template: "#template-table",
    childViewContainer: '#body-table',
    emptyView: EmptyView,
    ui: {
        btnToogleTabela: '#btn-toogle-tabela',
        body: '#body-table'
    },
    onShow: function () {
        /*
         * Limpando conteudo da variavel global utilizada que faz verificacao 
         * e mesclagem de celulas.
         */
        rowBack = 0;

        $('.inputMaskData').mask('99-99-9999');
        $('.tooltipClass').tooltip();
        $('.popoverClass').popover({position: 'left'});

        $('.td-remove').css('border-top', '0px solid');
    },
    events: {
        'click .btn-toogle-tabela': function () {
            this.ui.body.toggle();
        }
    }
});

var GerenciaNotasCompositeView = Marionette.CompositeView.extend({
    template: '#template-gerencia-notas',
    className: 'row-fluid',
    initialize:function () {
        VerificarPerfilPermissao(34, 272, true).done(function (response) {
            // alert(response.length);
            if (response.length == 1) {
                permissaoAlterarNotas = true;
            } else {
                permissaoAlterarNotas = false;
            }

        });

    },
    ui: {
        idMatriculaSelect: '#id-matricula-select',
        formComboMatricula: '#form-combo-matricula',
        containerTable: '#container-table',
        containerPrr: '#container-prr',
        containerTablePrr: '#container-table-prr',
        itemAutoComplete: '.item-autocomplete',
        btnClearAutoComplete: '#btnClearAutocomplete'
    },
    onShow: function () {

        if (localStorage.getItem('pessoaAutocomplete')) {
            var pessoa = JSON.parse(localStorage.getItem('pessoaAutocomplete'));
            this.loadComboMatriculas(pessoa);
        }

        that = this;

        componenteAutoComplete.main({
            model: VwPesquisarPessoa,
            element: this.$el.find('#autoComplete')
        }, function () {
        }, function () {
            carregaTelaGrade();
            // G2S.layout.content.show(new GerenciaNotasCompositeView());
            // that.ui.containerTable.empty();
            // that.ui.containerPrr.hide();
            // that.ui.formComboMatricula.hide();
        });

    },
    MatriculasCollection: null,
    loadComboMatriculas: function (pessoa) {
        loading();

        this.ui.formComboMatricula.show();
        this.ui.idMatriculaSelect.html('<option value="">--</option>');

        this.MatriculasCollection = new VwUsuarioMatriculaProjetoCollectionExtend();
        this.MatriculasCollection.url += 'id_usuario/' + pessoa.id_usuario + '/bl_ativo/1/id_entidadeatendimento/' + G2S.loggedUser.get('id_entidade');
        this.MatriculasCollection.fetch();

        var ComboMatriculasView = Marionette.ItemView.extend({
            tagName: 'option',
            template: _.template('<%=id_matricula%> - <%=st_tituloexibicao%> - <%=st_evolucao%>'),
            onRender: function () {
                this.$el.attr('value', this.model.get('id_matricula'));
            }
        });

        var ComboMatriculasCollection = Marionette.CollectionView.extend({
            tagName: 'select',
            childView: ComboMatriculasView,
            labelName: 'Matriculas: '
        });

        (new ComboMatriculasCollection({
            collection: this.MatriculasCollection,
            el: this.ui.idMatriculaSelect
        })).render();

        loaded();
    },
    carregaGrid: function (idMatricula) {
        loading();

        var idMatriculaSelected = this.ui.idMatriculaSelect.val();
        if (!idMatriculaSelected) {
            return;
        }
        // var matriculaSelected = this.MatriculasCollection.findWhere({"id_matricula":parseInt(idMatriculaSelected)});

        var VwAvaliacaoAlunoCollection = new VwAvaliacaoAlunoCollectionExtend();
        VwAvaliacaoAlunoCollection.url += '/index/id_matricula/' + idMatriculaSelected;
        VwAvaliacaoAlunoCollection.fetch();

        //Montando dados da grid de avaliacoes de salas normal
        var VwAvaliacaoAlunoFilterCategoriaSala = new
            VwAvaliacaoAlunoCollectionExtend(VwAvaliacaoAlunoCollection.where({id_categoriasala: 1}));

        //Render content avaliacoes sala normal
        (new AvaliacoesCompositeViewExtend({
            collection: VwAvaliacaoAlunoFilterCategoriaSala,
            el: this.ui.containerTable,
        })).render();

        //Montando dados da grid de avaliacoes de salas prr
        var VwAvaliacaoAlunoFilterCategoriaSalaPrr = new
            VwAvaliacaoAlunoCollectionExtend(VwAvaliacaoAlunoCollection.where({id_categoriasala: 2}));


        //Render content avaliacoes sala PRR
        (new AvaliacoesCompositeViewExtend({
            collection: VwAvaliacaoAlunoFilterCategoriaSalaPrr,
            el: this.ui.containerTablePrr
        })).render();
        this.ui.containerPrr.show();

        loaded();
    },
    events: {
        'change #id-matricula-select': "carregaGrid",

        'click .item-autocomplete': function () {
            var pessoa = JSON.parse(localStorage.getItem('pessoaAutocomplete'));
            this.loadComboMatriculas(pessoa);
        }
    }
});

function carregaTelaGrade(){
    GerenciarNotasView = new GerenciaNotasCompositeView();
    G2S.layout.content.show(GerenciarNotasView);
}

$(function () {
    carregaTelaGrade();
});

// Main region
G2S.layout.addRegions({
    'global': '#global-content'
});

// FETCHS NECESSARIOS SOMENTE UMA VEZ
var arrayOrganizacao = null;

var GlobalModel = Backbone.Model.extend({
    idAttribute: 'id_avaliacao',
    defaults: {
        id_avaliacao: null,
        id_situacao: null,
        id_tipoavaliacao: null,
        st_avaliacao: '',
        nu_valor: 0,
        dt_cadastro: null,
        bl_recuperacao: false,
        disciplinas: []
    },
    url: function () {
        return '/api/avaliacao/' + (this.id || '');
    },
    parse: function (response) {
        if (response.mensagem) {
            return response.mensagem.length ? response.mensagem[0] : response.mensagem;
        }

        return response;
    },
    validation: function () {
        if ($('#id_tipoavaliacao option:selected').val() != TIPO_AVALIACAO.CONCEITUAL) {
            return {
                st_avaliacao: {
                    required: true,
                    msg: 'Insira um título para esta avaliação'
                },
                id_tipoavaliacao: {
                    required: true,
                    msg: 'Selecione o tipo de avaliação'
                },
                id_situacao: {
                    required: true,
                    msg: 'Selecione uma situação'
                },
                nu_valor: {
                    required: true,
                    range: [1, 100],
                    msg: 'Insira um valor entre 1 e 100'
                }
            };
        } else {
            return {
                st_avaliacao: {
                    required: true,
                    msg: 'Insira um título para esta avaliação'
                },
                id_tipoavaliacao: {
                    required: true,
                    msg: 'Selecione o tipo de avaliação'
                },
                id_situacao: {
                    required: true,
                    msg: 'Selecione uma situação'
                }
            };
        }
    }
});

var GlobalView = Marionette.ItemView.extend({
    model: new GlobalModel(),
    template: '#global-view',
    tagName: 'div',
    ui: {
        form: 'form'
    },
    events: {
        'submit @ui.form': 'actionSave',
        'change [name="disciplina_organizacao"]': 'renderDisciplinas',
        'change [name="id_tipoavaliacao"]': 'recuperacaoPresencial'
    },
    initialize: function () {
        var that = this;
        Backbone.Validation.bind(this, {
            invalid: function (view, attr, error) {
                that.$el.find('[name="' + attr + '"]').focus();

                if (typeof error == 'object') {
                    $.pnotify({type: 'warning', title: 'Aviso', text: error.msg});
                } else {
                    $.pnotify({
                        type: 'warning',
                        title: 'Aviso',
                        text: error
                    });
                }
            }
        });
    },
    onRender: function () {
        var that = this;
        var elem = this.$el;

        if (!arrayOrganizacao) {
            $.ajaxSetup({async: false});
            $.getJSON('/avaliacao/get-organizacao-projeto/', function (response) {
                arrayOrganizacao = response.mensagem;
            });
            $.ajaxSetup({async: true});
        }

        // COMBOBOX SITUACAO
        var Model = Backbone.Model.extend({
            idAttribute: 'id_situacao',
            defaults: {
                'id_situacao': null,
                'st_situacao': ''
            }
        });
        ComboboxView({
            el: elem.find('[name="id_situacao"]'),
            model: Model,
            url: '/api/situacao?st_tabela=tb_avaliacao',
            emptyOption: '[Selecione]',
            optionLabel: 'st_situacao',
            optionSelectedId: that.model.get('id_situacao')
        });

        // COMBOBOX TIPO
        Model = Backbone.Model.extend({
            idAttribute: 'id_tipoavaliacao',
            defaults: {
                'id_tipoavaliacao': null,
                'st_tipoavaliacao': ''
            }
        });
        ComboboxView({
            el: elem.find('[name="id_tipoavaliacao"]'),
            model: Model,
            url: '/avaliacao/get-tipo-avaliacao',
            emptyOption: '[Selecione]',
            optionLabel: 'st_tipoavaliacao',
            optionSelectedId: that.model.get('id_tipoavaliacao')
        });


        // Render disciplinas ja adicionadas
        DisciplinaaddedIni = new DisciplinaaddedComposite({
            collection: new DisciplinaaddedCollection(that.model.get('disciplinas'))
        });
        elem.find('.disciplina-right').html(DisciplinaaddedIni.render().$el);

        this.renderDisciplinas();
    },
    recuperacaoPresencial: function () {
        var tpAvaliacao = $('#id_tipoavaliacao option:selected').val();
        if (tpAvaliacao == TIPO_AVALIACAO.AVALIACAO_PRESENCIAL) {
            this.$el.find('#provaRecuperacao').removeClass('hide');
        } else {
            this.$el.find('#provaRecuperacao').addClass('hide');
        }

        if (tpAvaliacao == TIPO_AVALIACAO.CONCEITUAL) {
            this.$el.find('#valorAvaliacao').addClass('hide');
            this.$el.find('#msgAvaliacaoConceitual').removeClass('hide');
        } else {
            this.$el.find('#valorAvaliacao').removeClass('hide');
            this.$el.find('#msgAvaliacaoConceitual').addClass('hide');
        }
    },
    renderDisciplinas: function () {
        var elem = this.$el;

        loading();

        var idEntidade = elem.find('[name="disciplina_organizacao"]').val();

        DisciplinaIni = new DisciplinaComposite();
        DisciplinaIni.collection.url = '/api/disciplina/' + (idEntidade ? '?id_entidade=' + idEntidade : '');
        DisciplinaIni.collection.fetch({
            success: function () {
                elem.find('.disciplina-left').html(DisciplinaIni.render().$el);
                loaded();
            }
        });
    },
    actionSave: function (e) {
        e.preventDefault();
        var elem = this.$el;

        var values = {
            id_situacao: elem.find('[name="id_situacao"]').val(),
            id_tipoavaliacao: elem.find('[name="id_tipoavaliacao"]').val(),
            nu_valor: Number(elem.find('[name="nu_valor"]').val()),
            st_avaliacao: elem.find('[name="st_avaliacao"]').val(),
            bl_recuperacao: elem.find('[name="bl_recuperacao"]').val(),
            disciplinas: DisciplinaaddedIni.collection.toJSON()
        };

        this.model.save(values, {
            beforeSend: loading,
            success: function () {
                window.location.hash = 'pesquisa/' + G2S.funcionalidadeAtual.get('classeFlexBotao');
            },
            complete: function (response) {
                loaded();
                response = $.parseJSON(response.responseText);
                $.pnotify(response);
            }
        });

    }
});

/**
 * Listar Disciplinas
 */
var DisciplinaModel = Backbone.Model.extend({
    idAttribute: 'id_disciplina',
    defaults: {
        id_disciplina: null,
        id_avaliacao: null,
        id_avaliacaodisciplina: null,
        st_disciplina: ''
    }
});
var DisciplinaView = Marionette.ItemView.extend({
    model: new DisciplinaModel(),
    template: '#disciplina-view',
    tagName: 'tr',
    onRender: function () {
        var elem = this.$el;
        elem.attr('data-label', this.model.get('st_disciplina').removeAccents().trim().toLowerCase());
    },
    events: {
        'click .actionAdd': 'actionAdd'
    },
    actionAdd: function () {
        DisciplinaaddedIni.collection.add(this.model);
        DisciplinaIni.collection.remove(this.model);
    }
});
var DisciplinaCollection = Backbone.Collection.extend({
    model: DisciplinaModel,
    url: '/api/disciplina/'
});
var DisciplinaComposite = Marionette.CompositeView.extend({
    template: '#disciplina-template',
    collection: new DisciplinaCollection(),
    childView: DisciplinaView,
    childViewContainer: 'tbody',
    idOrganizacao: '',
    onBeforeRender: function () {
        this.idOrganizacao = $('[name="disciplina_organizacao"]').val();

        var adicionadas = DisciplinaaddedIni.collection.models;
        if (adicionadas.length)
            DisciplinaIni.collection.remove(DisciplinaaddedIni.collection.models);
    },
    onRender: function () {
        var elem = this.$el;

        this.verifyEmpty();

        // COMBOBOX FILTRAR DISCIPLINA
        var Model = Backbone.Model.extend({
            idAttribute: 'id_entidade',
            defaults: {
                'id_entidade': null,
                'st_nomeentidade': ''
            }
        });
        ComboboxView({
            el: elem.find('[name="disciplina_organizacao"]'),
            model: Model,
            collection: arrayOrganizacao,
            emptyOption: '[Selecione]',
            optionLabel: 'st_nomeentidade',
            optionSelectedId: this.idOrganizacao || GlobalIni.model.get('id_entidade')
        });
    },
    events: {
        'keyup [name="disciplina_search"]': 'disciplinaSearch'
    },
    disciplinaSearch: function (e) {
        var elem = this.$el;
        var value = $(e.target).val().removeAccents().trim().toLowerCase();

        var views = elem.find('tbody tr');
        var toShow = views.filter('[data-label^="' + value + '"]');
        var toHide = views.not(toShow);

        if (value != '') {
            toHide.hide();
            toShow.show();
        } else {
            views.show();
        }
    },
    collectionEvents: {
        'add': 'verifyEmpty',
        'remove': 'verifyEmpty'
    },
    verifyEmpty: function () {
        var elem = this.$el;

        if (this.collection.length) {
            elem
                .find('.empty-view').remove();
        } else {
            var ViewEmpty = Marionette.ItemView.extend({
                tagName: 'tr',
                className: 'empty-view',
                template: _.template('<td colspan="2">Nenhuma disciplina encontrada.</td>')
            });

            elem
                .find('tbody').append((new ViewEmpty()).render().$el);
        }
    }
});

var DisciplinaaddedView = Marionette.ItemView.extend({
    model: new DisciplinaModel(),
    template: '#disciplinaadded-view',
    tagName: 'tr',
    events: {
        'click .actionRemove': 'actionRemove'
    },
    actionRemove: function () {
        DisciplinaIni.collection.add(this.model);
        DisciplinaaddedIni.collection.remove(this.model);
    }
});
var DisciplinaaddedCollection = Backbone.Collection.extend({
    model: DisciplinaModel,
    url: '/api/disciplina/'
});
var DisciplinaaddedComposite = Marionette.CompositeView.extend({
    template: '#disciplina-template',
    collection: new DisciplinaaddedCollection(),
    childView: DisciplinaaddedView,
    childViewContainer: 'tbody',
    onRender: function () {
        this.verifyEmpty();
    },
    collectionEvents: {
        'add': 'verifyEmpty',
        'remove': 'verifyEmpty'
    },
    verifyEmpty: function () {
        var elem = this.$el;

        if (this.collection.length) {
            elem
                .find('.empty-view').remove();
        } else {
            var ViewEmpty = Marionette.ItemView.extend({
                tagName: 'tr',
                className: 'empty-view',
                template: _.template('<td colspan="2">Nenhuma disciplina adicionada.</td>')
            });

            elem
                .find('tbody').append((new ViewEmpty()).render().$el);
        }
    }
});

DisciplinaaddedIni = null;
DisciplinaIni = null;

var GlobalIni = new GlobalView();
// Verificar se possui id no url
var idParam = Number(window.location.href.split('/').pop());
if (!isNaN(idParam)) {
    $.ajaxSetup({async: false});
    GlobalIni.model.id = idParam;
    GlobalIni.model.fetch({
        success: function (model, response) {
            GlobalIni.model.set(response);
        }
    });
    $.ajaxSetup({async: true});
}

G2S.layout.global.show(GlobalIni);

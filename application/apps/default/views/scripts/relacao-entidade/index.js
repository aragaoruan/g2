$.ajaxSetup({async: false});
$.getScript('js/backbone/views/RegraPagamento/SelectView.js');

var checks;
var entidade;
var unidade_id;
var obj_corrente;
var option__selecionada;
var aux_projs_pedagogico;
var all_selecteds = false;
var projs_pedagogico = new Array();

// Seleciona e desseleciona todos os Checks
function btn_select_all_event() {
    setTimeout(function(){
        $("#select_all").click(function(){
            var input_checks = $('input[type="checkbox"]');

            if(all_selecteds) {
                input_checks.prop('checked', false);
                all_selecteds = false;
            }else {
                input_checks.prop('checked', true);
                all_selecteds = true;
            }
        });
    }, 700);
}

function get_selected(elements) {
    for(element in elements) if (elements[element].selected) return unidades.models[element];
}

// Seta o check nos attrs passados como attrs relacionados a array de objs inicial
function set_checks(objs, checks, attr_to_check){
    for(var i=0; i < checks.length; i++) {
        obj_corrente = checks[i];
        proj_pedagogico = projs_pedagogico[obj_corrente['id_projetopedagogico']];

        if(proj_pedagogico==undefined || proj_pedagogico === undefined) continue;
        else proj_pedagogico.set({association_status:"checked"});
    }
}

function setup_associations(projs) {
    var urlProjsPedagogicosUnidadeSelecionada = 'relacao-entidade/retornar-projeto-pedagogico-da-unidade/unidade_id/'+unidade_id;
    $.ajax({
        url:urlProjsPedagogicosUnidadeSelecionada,
        async:   false,
        success: function(resp){
            checks = resp;
            objs = projs;
            attr_to_check = 'id_projetopedagogico';
            set_checks(objs, checks, attr_to_check);
            btn_select_all_event();
        }
    });
}

/**
 * COLLECTION PARA UNIDADE
 */
var EntidadesCollection = Backbone.Collection.extend({
    url: '/relacao-entidade/pesquisar-entidade-filha',
    model: Entidade,
    parse: function(response) {
        response.sort(function(a, b) {
            if (a.st_nomeentidade.toUpperCase().removeAccents() < b.st_nomeentidade.toUpperCase().removeAccents())
                return -1;
            if (a.st_nomeentidade.toUpperCase().removeAccents() > b.st_nomeentidade.toUpperCase().removeAccents())
                return 1;
            return 0;
        });
        return response;
    }
});

/**
 * COMBO PARA UNIDADE
 */
var unidadeCollection = new EntidadesCollection();
unidadeCollection.fetch();

var ComboUnidadeView = Marionette.ItemView.extend({
    tagName: 'option',
    template: _.template('<%=st_nomeentidade%>'),
    onRender: function () {
        this.$el.attr('value', this.model.get('id_entidade'));
    }
});

var ComboUnidadeCollection = Marionette.CollectionView.extend({
    tagName: 'select',
    childView: ComboUnidadeView,
    labelName: 'Unidade: ',
    onRender: function() {
        // Remover option da entidade logada
        var elem = this.$el;
        elem.find('option').each(function() {
            if (parseInt($(this).attr('value')) == parseInt(G2S.loggedUser.get('id_entidade')))
                $(this).remove();
        });
    }
});

// It collection instantiated
var unidades = new EntidadesCollection();

/**
 * FIM DO COMBO PARA UNIDADES
 */


// Coleção de PROJETOS da entidade da sessao
var ProjetoPedagogicoCollection = Backbone.Collection.extend({
    model: ProjetoPedagogico
});
var projetosCollection = new ProjetoPedagogicoCollection();
projetosCollection.url = 'relacao-entidade/retornar-projeto-pedagogico';
loading();
projetosCollection.fetch();
loaded();

// Script do componente RELACAO-ENTIDADE (Label+InputSelect)
var RelacaoEntidadeLayout = Marionette.LayoutView.extend({
    template: '#template_relacao_entidade',
    tagName: 'div',
    className: 'container-fluid',
    regions: {
        seleciona_unidade: '#seleciona_unidade',
        seleciona_relacao: '#seleciona_relacao',
        projeto_pedagogico: '#projeto_pedagogico'
    },
    ui: {
        comboUnidade: '#id-unidade-select',
        containerProjeto: '#container-projeto-ped'
    },
    onShow: function(){
        this.loadComboUnidade();
    },
    onRender: function () {
        return this;
    },
    loadComboUnidade: function () {
        loading();
        this.ui.comboUnidade.show();
        this.ui.comboUnidade.html('<option value="">--</option>');

        (new ComboUnidadeCollection({
            collection: unidadeCollection,
            el: this.ui.comboUnidade
        })).render();

        loaded();
    },
    events:{
        'click button.btn.btn-success': 'salva_relacao',
        'change #id-unidade-select' : 'buscaProjetos',
        'click button#select_all': 'selectAllProjetos'
    },
    selectAllProjetos: function(e){
        var input_checks = $('input[type="checkbox"]');
        if(all_selecteds) {
            input_checks.prop('checked', false);
            all_selecteds = false;
        }else {
            input_checks.prop('checked', true);
            all_selecteds = true;
        }
    },
    buscaProjetos: function(e){
        var that = this;
        loading();
        // TIMEOUT porque ao alterar unidade no CHROME, o loading nao aparecia e tela travava
        setTimeout(function() {
            var id = $(e.currentTarget).val();
            if(id != undefined){
                unidade_id = id;
                var projetosSelecionadosCollection = new ProjetoPedagogicoCollection();
                projetosSelecionadosCollection.url = 'relacao-entidade/retornar-projeto-pedagogico-da-unidade/unidade_id/'+unidade_id;
                projetosSelecionadosCollection.fetch({
                    complete: function(response) {
                        response = $.parseJSON(response.responseText);
                        if (!response.length) {
                            $.pnotify({
                                type: 'warning',
                                title: 'Aviso',
                                text: 'Essa Entidade não possui Projeto Pedagógico cadastrado.'
                            });
                            loaded();
                        }
                    }
                });

                aux_projs_pedagogico = projetosCollection.models;
                var aux_projs_pedagogicoSelecionados = projetosSelecionadosCollection.models;
                for(var i=0; i< projetosCollection.models.length; i++) {
                    projetosCollection.models[i].set({association_status:""});
                    for(var y=0; y < aux_projs_pedagogicoSelecionados.length; y++){
                        if(projetosCollection.models[i].get('id_projetopedagogico') == aux_projs_pedagogicoSelecionados[y].get('id_projetopedagogico') ){
                            projetosCollection.models[i].set({association_status:"checked"});
                        }
                    }
                }
                //projs_pedagogico.models.set(aux_projs_pedagogico);

                /**
                 * Usando a funcao compareSortProjetoPedagogico() para verficar e
                 * determinar ordem dos objetos alfabeticamente por
                 * st_projetopedagogico a serem renderizados
                 */
                projetosCollection.models.sort(compareSortProjetoPedagogico);

                var renderizar = (new ProjetoPedagogicoCompositeView({
                    collection: projetosCollection,
                    el: that.ui.containerProjeto
                }));
                that.projeto_pedagogico.show(renderizar.render());
                loaded();

            }
        }, 100);
        loaded();
    },
    salva_relacao: function() {
        loading();
        // TIMEOUT porque ao salvar no CHROME, o loading nao aparecia e tela travava
        setTimeout(function() {
            projs_pedagogicos_ids = new Array();
            projs_jquery = $('input:checked[name="projetos_pedagogicos[]"]');
            for (var i = 0; i < projs_jquery.length; i++)
                projs_pedagogicos_ids.push(projs_jquery[i].value);

            $.post('relacao-entidade/salvar-entidade-relacao', {
                'projetos_pedagogicos[]': projs_pedagogicos_ids,
                'entidade': unidade_id
            }, function (resp) {
                if (!(typeof resp[0] == "string")) {
                    loaded();
                    $.pnotify({
                        title: 'Sucesso!',
                        text: 'Projetos pedagógicos relacionados com sucesso!',
                        type: 'success'
                    });
                } else {
                    loaded();
                    $.pnotify({
                        title: 'Erro!',
                        text: 'Não foi possível relacionar os Projetos pedagógicos!',
                        type: 'error'
                    });
                }
            });
        }, 100);
    },
    initialize: function () {
        console.log('renderizando o layout Entidade');
    }
});



var ProjetoPedagogicoItemView = Marionette.ItemView.extend({
    tagName: 'div',
    template: '#template-item-projeto'
    /*collection: new ProjetoPedagogicoCollection(),
     initialize: function () {
     this.$el.attr('value', this.model.get('id_entidadecadastro'));
     }*/
});

var ProjetoPedagogicoCompositeView = Marionette.CompositeView.extend({
    childView: ProjetoPedagogicoItemView,
    tagName: 'div',
    template:'#template_container_projeto',
    childViewContainer: '#listaProjetos'
});


var relacaoEntidade = new RelacaoEntidadeLayout();
G2S.show(relacaoEntidade);

/**
 * Funcao usada pra ordenar objetos pelo atributo informado
 * @param Object a
 * @param Object b
 * @returns boolean
 */
function compareSortProjetoPedagogico(a, b) {
    //G2S_StringRegex aplicado para retirar caracteres especiais
    if (a.attributes.st_projetopedagogico.toUpperCase().removeAccents() < b.attributes.st_projetopedagogico.toUpperCase().removeAccents())
        return -1;
    if (a.attributes.st_projetopedagogico.toUpperCase().removeAccents() > b.attributes.st_projetopedagogico.toUpperCase().removeAccents())
        return 1;
    return 0;
}
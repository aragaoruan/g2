/****************************************************
 **************   MODEL HISTORICO  ******************
 ****************************************************/
var HistoricoModel = Backbone.Model.extend({
    idAttribute: "id_log"
});
/****************************************************
 **************   COLLECTION HISTORICO  *************
 ****************************************************/
var HistoricoCollection = Backbone.Collection.extend({
    url: '/esquema-configuracao/retornar-historico-parametro-renovacao',
    model: HistoricoModel
});

/*********************************************
 **************   ITEMVIEW  ******************
 *********************************************/

var ViewDadosConfiguracao = Marionette.ItemView.extend({
    template: '#template-dados-configuracao',
    tagName: 'div',
    id: 'container-dados-esquema',
    initialize: function (dados) {
        //G2S.editModel = dados;
    },
    onShow: function () {
        this.carregarDadosEsquemaAtualizado();
    },
    carregarDadosEsquemaAtualizado: function () {
        if (G2S.editModel) {

            var that = this;
            parametros = {
                id: G2S.editModel[0].id_esquemaconfiguracao.id_esquemaconfiguracao,
            };

            $.ajax({
                type: 'post',
                url: '/esquema-configuracao/retornar-esquema-configuracao',
                data: parametros,
                beforeSend: function () {
                    loading();
                },
                success: function (data) {
                    G2S.editModel = data.mensagem;
                    $('#id_esquemaconfiguracao').val(G2S.editModel[0].id_esquemaconfiguracao.id_esquemaconfiguracao);
                    $('#st_esquemaconfiguracao').val(G2S.editModel[0].id_esquemaconfiguracao.st_esquemaconfiguracao);

                    $.each(G2S.editModel, function () {
                        $('#id_configuracao_' + this.id_itemconfiguracao.id_itemconfiguracao).val(this.st_valor)
                    });

                },
                complete: function () {
                    loaded();
                }
            });
        }
    },
    saveDadosEsquema: function (parametros) {
        var that = this;

        if (($('#st_esquemaconfiguracao').val() !== '')) {
            $.ajax({
                type: 'post',
                url: '/esquema-configuracao/salvar-esquema',
                data: parametros,
                beforeSend: function () {
                    loading();
                },
                success: function (data) {
                    $.pnotify({
                        title: data['title'],
                        text: data['mensagem']['message'],
                        type: data['type']
                    });
                    if (data['type'] == 'success') {
                        G2S.editModel = data['mensagem']['data']['mensagem']
                    }
                    that.carregarDadosEsquemaAtualizado();
                },
                complete: function () {
                    loaded();
                }
            });
        } else {
            $.pnotify({
                title: 'Aviso',
                text: 'É necessário informar o nome do esquema',
                type: 'warning'
            });
        }
    }
});

var ViewDadosRegraFuncionamento = Marionette.ItemView.extend({
    template: '#template-dados-regras-funcionamento',
    tagName: 'div',
    className: 'content',
    id: 'container-regras-funcionamento',
    dadosConfig: new ViewDadosConfiguracao,
    events: {
        'click #btn-salvar-regras-funcionamento': 'saveRegraFuncionamento'
    },
    ui: {
        st_esquemaconfiguracao: '#st_esquemaconfiguracao',
        id_esquemaconfiguracao: '#id_esquemaconfiguracao',

        st_permitir_imprimir_contrato_pendente: "#id_configuracao_1",
        id_permitir_imprimir_contrato_pendente: "#id_permitir_imprimir_contrato_pendente",

        st_impedir_cadastro_de_emails_duplicados_nessa_instituicao: '#id_configuracao_2',
        id_impedir_cadastro_de_emails_duplicados_nessa_instituicao: '#id_impedir_cadastro_de_emails_duplicados_nessa_instituicao',

        st_linha_negocio: '#id_configuracao_22',
        id_linha_negocio: '#id_linha_negocio',

        st_utilizar_mensagem_padrao_entidade_mae: '#id_configuracao_33',
        id_utilizar_mensagem_padrao_entidade_mae: '#id_utilizar_mensagem_padrao_entidade_mae',

        st_realizar_agendamento_1_automaticamente: '#id_configuracao_23',
        id_realiza_agendamento_1_automaticamente: '#id_realiza_agendamento_1_automaticamente',

        st_avaliacoes_feitas_por_disciplina: '#id_configuracao_24',
        id_avaliacoes_feitas_por_disciplina: '#id_avaliacoes_feitas_por_disciplina',

        st_utiliza_recuperacao_com_media_variavel: '#id_configuracao_25',
        id_utiliza_recuperacao_com_media_variavel: '#id_utiliza_recuperacao_com_media_variavel',

    },
    onRender: function () {
        var that = this;
        ComboboxView({
            el: that.ui.st_linha_negocio,
            url: '/api/linha-de-negocio',
            optionLabel: 'st_linhadenegocio',
            emptyOption: '[Selecione]',
            model: Backbone.Model.extend({
                idAttribute: 'id_linhadenegocio',
            })
        })
    },
    onShow: function () {
        this.dadosConfig.carregarDadosEsquemaAtualizado();
    },
    saveRegraFuncionamento: function () {
        if (!this.validaCamposObrigagorios()) {
            return false;
        } else {
            parametros = {
                'st_esquemaconfiguracao': $('#st_esquemaconfiguracao').val(),
                'id_esquemaconfiguracao': $('#id_esquemaconfiguracao').val(),

                'regra_funcionamento[impedir_cadastro_de_emails_duplicados_nessa_instituicao][id_itemconfiguracao]': this.ui.id_impedir_cadastro_de_emails_duplicados_nessa_instituicao.val(),
                'regra_funcionamento[impedir_cadastro_de_emails_duplicados_nessa_instituicao][st_default]': this.ui.st_impedir_cadastro_de_emails_duplicados_nessa_instituicao.val(),

                'regra_funcionamento[permitir_imprimir_contrato_pendente][id_itemconfiguracao]': this.ui.id_permitir_imprimir_contrato_pendente.val(),
                'regra_funcionamento[permitir_imprimir_contrato_pendente][st_default]': this.ui.st_permitir_imprimir_contrato_pendente.val(),

                'regra_funcionamento[linha_negocio][id_itemconfiguracao]': this.ui.id_linha_negocio.val(),
                'regra_funcionamento[linha_negocio][st_default]': this.ui.st_linha_negocio.val(),

                'regra_funcionamento[utilizar_mensagem_padrao_entidade_mae][id_itemconfiguracao]': this.ui.id_utilizar_mensagem_padrao_entidade_mae.val(),
                'regra_funcionamento[utilizar_mensagem_padrao_entidade_mae][st_default]': this.ui.st_utilizar_mensagem_padrao_entidade_mae.val(),


                'regra_funcionamento[realiza_primeiro_agendamento_automatico][id_itemconfiguracao]': this.ui.id_realiza_agendamento_1_automaticamente.val(),
                'regra_funcionamento[realiza_primeiro_agendamento_automatico][st_default]': this.ui.st_realizar_agendamento_1_automaticamente.val(),

                'regra_funcionamento[avaliacoes_feitas_por_disciplina][id_itemconfiguracao]': this.ui.id_avaliacoes_feitas_por_disciplina.val(),
                'regra_funcionamento[avaliacoes_feitas_por_disciplina][st_default]': this.ui.st_avaliacoes_feitas_por_disciplina.val(),

                'regra_funcionamento[utiliza_recuperacao_com_media_variavel][id_itemconfiguracao]': this.ui.id_utiliza_recuperacao_com_media_variavel.val(),
                'regra_funcionamento[utiliza_recuperacao_com_media_variavel][st_default]': this.ui.st_utiliza_recuperacao_com_media_variavel.val(),

            };

            this.dadosConfig.saveDadosEsquema(parametros);
        }
    },
    validaCamposObrigagorios: function () {
        var retorno = true;
        if (this.ui.st_linha_negocio.val() == '') {
            $.pnotify({
                title: 'Aviso',
                text: 'Selecione um tipo de negócio para salvar',
                type: 'warning'
            });
            retorno = false;
        }

        if (this.ui.st_realizar_agendamento_1_automaticamente.val() == '') {
            $.pnotify({
                title: 'Aviso',
                text: 'Selecione uma opção para "Realizar o 1º agendamento automaticamente".',
                type: 'warning'
            });
            retorno = false;
        }

        if (this.ui.st_avaliacoes_feitas_por_disciplina.val() == '') {
            $.pnotify({
                title: 'Aviso',
                text: 'Selecione uma opção para "Avaliações feitas por disciplinas".',
                type: 'warning'
            });
            retorno = false;
        }


        if (this.ui.st_utiliza_recuperacao_com_media_variavel.val() == '') {
            $.pnotify({
                title: 'Aviso',
                text: 'Selecione uma opção para "Utiliza recuperação com média variável".',
                type: 'warning'
            });
            retorno = false;
        }

        return retorno;

    }
});


var ViewDadosRegraPedagogico = Marionette.ItemView.extend({
    template: '#template-dados-regras-pedagogico',
    tagName: 'div',
    className: 'content',
    id: 'container-regras-pedagogico',
    dadosConfig: new ViewDadosConfiguracao,
    events: {
        'click #btn-salvar-regras-pedagogico': 'saveRegraPedagogico'
    },
    ui: {
        st_esquemaconfiguracao: '#st_esquemaconfiguracao',
        id_esquemaconfiguracao: '#id_esquemaconfiguracao',

        st_numero_de_horas_de_um_encontro: "#id_configuracao_3",
        id_numero_de_horas_de_um_encontro: "#id_numero_de_horas_de_um_encontro",

        st_numero_de_dias_de_extensao_para_o_professor: '#id_configuracao_4',
        id_numero_de_dias_de_extensao_para_o_professor: '#id_numero_de_dias_de_extensao_para_o_professor',

        st_numero_de_dias_que_o_professor_tem_para_encerrar_a_sala: '#id_configuracao_5',
        id_numero_de_dias_que_o_professor_tem_para_encerrar_a_sala: '#id_numero_de_dias_que_o_professor_tem_para_encerrar_a_sala',

        st_permitir_ao_professor_encerrar_alunos_que_nao_acessaram_a_sala: '#id_configuracao_6',
        id_permitir_ao_professor_encerrar_alunos_que_nao_acessaram_a_sala: '#id_permitir_ao_professor_encerrar_alunos_que_nao_acessaram_a_sala',

        st_modelo_da_grade_de_nota: '#id_configuracao_7',
        id_modelo_da_grade_de_nota: '#id_modelo_da_grade_de_nota',

        //para graduação
        st_padrao_disicplinas_por_periodo: "#id_configuracao_19",
        nu_padrao_disicplinas_por_periodo: '#nu_padrao_disicplinas_por_periodo',

        st_minimo_disicplinas_por_periodo: "#id_configuracao_20",
        nu_minimo_disicplinas_por_periodo: '#nu_minimo_disicplinas_por_periodo',

        st_maximo_disicplinas_por_periodo: "#id_configuracao_21",
        nu_maximo_disicplinas_por_periodo: '#nu_maximo_disicplinas_por_periodo',

        st_quantidade_dias_sem_acesso_aluno: '#id_configuracao_28',
        id_quantidade_dias_sem_acesso_aluno: '#id_quantidade_dias_sem_acesso_aluno',

        st_qtd_meses_apartir_inicio: '#id_configuracao_29',
        id_qtd_meses_apartir_inicio: '#id_qtd_meses_apartir_inicio',

        st_numero_dias_enviar_aviso_aluno: '#id_configuracao_30',
        nu_numero_dias_enviar_aviso_aluno: '#nu_numero_dias_enviar_aviso_aluno',

        st_parametro_para_nota_recuperacao: '#id_configuracao_32',
        id_parametro_para_nota_recuperacao: '#id_parametro_para_nota_recuperacao',
    },
    onShow: function () {
        this.dadosConfig.carregarDadosEsquemaAtualizado();
        $('.tootiperGrad').tooltip();
    },
    saveRegraPedagogico: function () {


        if (this.ui.st_parametro_para_nota_recuperacao.val() > 29) {
            $.pnotify({
                title: 'Aviso',
                text: 'Parâmetro para nota de recuperação deve ser menor que 29.',
                type: 'warning'
            });
            return;
        }

        if (!this.validaCampoNumerico()) {
            return false;
        } else {
            that = this;

            parametros = {
                'st_esquemaconfiguracao': $('#st_esquemaconfiguracao').val(),
                'id_esquemaconfiguracao': $('#id_esquemaconfiguracao').val(),

                'regra_pedagogico[st_numero_de_horas_de_um_encontro][st_default]': this.ui.st_numero_de_horas_de_um_encontro.val(),
                'regra_pedagogico[st_numero_de_horas_de_um_encontro][id_itemconfiguracao]': this.ui.id_numero_de_horas_de_um_encontro.val(),

                'regra_pedagogico[st_numero_de_dias_de_extensao_para_o_professor][st_default]': this.ui.st_numero_de_dias_de_extensao_para_o_professor.val(),
                'regra_pedagogico[st_numero_de_dias_de_extensao_para_o_professor][id_itemconfiguracao]': this.ui.id_numero_de_dias_de_extensao_para_o_professor.val(),

                'regra_pedagogico[st_numero_de_dias_que_o_professor_tem_para_encerrar_a_sala][st_default]': this.ui.st_numero_de_dias_que_o_professor_tem_para_encerrar_a_sala.val(),
                'regra_pedagogico[st_numero_de_dias_que_o_professor_tem_para_encerrar_a_sala][id_itemconfiguracao]': this.ui.id_numero_de_dias_que_o_professor_tem_para_encerrar_a_sala.val(),

                'regra_pedagogico[st_permitir_ao_professor_encerrar_alunos_que_nao_acessaram_a_sala][st_default]': this.ui.st_permitir_ao_professor_encerrar_alunos_que_nao_acessaram_a_sala.val(),
                'regra_pedagogico[st_permitir_ao_professor_encerrar_alunos_que_nao_acessaram_a_sala][id_itemconfiguracao]': this.ui.id_permitir_ao_professor_encerrar_alunos_que_nao_acessaram_a_sala.val(),

                'regra_pedagogico[st_modelo_da_grade_de_nota][st_default]': this.ui.st_modelo_da_grade_de_nota.val(),
                'regra_pedagogico[st_modelo_da_grade_de_nota][id_itemconfiguracao]': this.ui.id_modelo_da_grade_de_nota.val(),


                'regra_pedagogico[st_padrao_disicplinas_por_periodo][st_default]': this.ui.st_padrao_disicplinas_por_periodo.val(),
                'regra_pedagogico[st_padrao_disicplinas_por_periodo][id_itemconfiguracao]': this.ui.nu_padrao_disicplinas_por_periodo.val(),

                'regra_pedagogico[st_minimo_disicplinas_por_periodo][st_default]': this.ui.st_minimo_disicplinas_por_periodo.val(),
                'regra_pedagogico[st_minimo_disicplinas_por_periodo][id_itemconfiguracao]': this.ui.nu_minimo_disicplinas_por_periodo.val(),

                'regra_pedagogico[st_maximo_disicplinas_por_periodo][st_default]': this.ui.st_maximo_disicplinas_por_periodo.val(),
                'regra_pedagogico[st_maximo_disicplinas_por_periodo][id_itemconfiguracao]': this.ui.nu_maximo_disicplinas_por_periodo.val(),

                'regra_pedagogico[st_numero_dias_enviar_aviso_aluno][st_default]': this.ui.st_numero_dias_enviar_aviso_aluno.val(),
                'regra_pedagogico[st_numero_dias_enviar_aviso_aluno][id_itemconfiguracao]': this.ui.nu_numero_dias_enviar_aviso_aluno.val(),

                'regra_funcionamento[quantidade_dias_sem_acesso_aluno][id_itemconfiguracao]': this.ui.id_quantidade_dias_sem_acesso_aluno.val(),
                'regra_funcionamento[quantidade_dias_sem_acesso_aluno][st_default]': this.ui.st_quantidade_dias_sem_acesso_aluno.val(),

                'regra_funcionamento[qtd_meses_apartir_inicio][id_itemconfiguracao]': this.ui.id_qtd_meses_apartir_inicio.val(),
                'regra_funcionamento[qtd_meses_apartir_inicio][st_default]': this.ui.st_qtd_meses_apartir_inicio.val(),

                'regra_pedagogico[st_parametro_para_nota_recuperacao][st_default]': this.ui.st_parametro_para_nota_recuperacao.val(),
                'regra_pedagogico[st_parametro_para_nota_recuperacao][id_itemconfiguracao]': this.ui.id_parametro_para_nota_recuperacao.val(),
            };

            this.dadosConfig.saveDadosEsquema(parametros);
        }
    },
    validaCampoNumerico: function () {
        retorno = true;
        if (isNaN(this.ui.st_quantidade_dias_sem_acesso_aluno.val())) {
            $.pnotify({
                title: 'Aviso',
                text: 'Digite um número válido para indicar o número de dias que o aluno não acessa o Portal.',
                type: 'warning'
            });
            retorno = false;
        }

        if (isNaN(this.ui.st_qtd_meses_apartir_inicio.val())) {
            $.pnotify({
                title: 'Aviso',
                text: 'Digite um número válido para indicar o número de meses a partir da data de início da turma.',
                type: 'warning'
            });
            retorno = false;
        }

        return retorno;
    }
});

var ViewDadosRegraPortalAluno = Marionette.ItemView.extend({
    template: '#template-dados-regras-portal-aluno',
    tagName: 'div',
    className: 'content',
    id: 'container-regras-portal-aluno',
    dadosConfig: new ViewDadosConfiguracao,
    events: {
        'click #btn-salvar-regras-portal-aluno': 'saveRegraPortalAluno'
    },
    ui: {
        st_esquemaconfiguracao: '#st_esquemaconfiguracao',
        id_esquemaconfiguracao: '#id_esquemaconfiguracao',

        st_exibir_tutorial_de_acesso_do_aluno_ao_portal: "#id_configuracao_8",
        id_exibir_tutorial_de_acesso_do_aluno_ao_portal: "#id_exibir_tutorial_de_acesso_do_aluno_ao_portal",

        st_utilizar_a_ferramenta_de_primeiro_acesso_para_os_novos_alunos: "#id_configuracao_9",
        id_utilizar_a_ferramenta_de_primeiro_acesso_para_os_novos_alunos: "#id_utilizar_a_ferramenta_de_primeiro_acesso_para_os_novos_alunos",

        st_permitir_login_simultaneo: "#id_configuracao_10",
        id_permitir_login_simultaneo: "#id_permitir_login_simultaneo",
    },
    onShow: function () {
        this.dadosConfig.carregarDadosEsquemaAtualizado();
    },
    saveRegraPortalAluno: function () {

        that = this;

        parametros = {
            'st_esquemaconfiguracao': $('#st_esquemaconfiguracao').val(),
            'id_esquemaconfiguracao': $('#id_esquemaconfiguracao').val(),

            'regra_portal_aluno[st_exibir_tutorial_de_acesso_do_aluno_ao_portal][id_itemconfiguracao]': this.ui.id_exibir_tutorial_de_acesso_do_aluno_ao_portal.val(),
            'regra_portal_aluno[st_exibir_tutorial_de_acesso_do_aluno_ao_portal][st_default]': this.ui.st_exibir_tutorial_de_acesso_do_aluno_ao_portal.val(),

            'regra_portal_aluno[st_utilizar_a_ferramenta_de_primeiro_acesso_para_os_novos_alunos][id_itemconfiguracao]': this.ui.id_utilizar_a_ferramenta_de_primeiro_acesso_para_os_novos_alunos.val(),
            'regra_portal_aluno[st_utilizar_a_ferramenta_de_primeiro_acesso_para_os_novos_alunos][st_default]': this.ui.st_utilizar_a_ferramenta_de_primeiro_acesso_para_os_novos_alunos.val(),

            'regra_portal_aluno[st_permitir_login_simultaneo][id_itemconfiguracao]': this.ui.id_permitir_login_simultaneo.val(),
            'regra_portal_aluno[st_permitir_login_simultaneo][st_default]': this.ui.st_permitir_login_simultaneo.val(),
        };

        this.dadosConfig.saveDadosEsquema(parametros);
    }
});

var ViewDadosRegraComercial = Marionette.ItemView.extend({
    template: '#template-dados-regras-comercial',
    tagName: 'div',
    className: 'content',
    id: 'container-regras-comercial',
    dadosConfig: new ViewDadosConfiguracao,
    events: {
        'click #btn-salvar-regras-comercial': 'saveRegraComercial'
    },
    ui: {
        st_esquemaconfiguracao: '#st_esquemaconfiguracao',
        id_esquemaconfiguracao: '#id_esquemaconfiguracao',

        st_solicitar_endereco_na_loja: "#id_configuracao_11",
        id_solicitar_endereco_na_loja: "#id_solicitar_endereco_na_loja",

        st_configuracao_de_venda_por_cartao_de_credito: "#id_configuracao_12",
        id_configuracao_de_venda_por_cartao_de_credito: "#id_configuracao_de_venda_por_cartao_de_credito",

        st_configuracao_de_venda_por_boleto: "#id_configuracao_13",
        id_configuracao_de_venda_por_boleto: "#id_configuracao_de_venda_por_boleto",
    },
    onShow: function () {
        this.dadosConfig.carregarDadosEsquemaAtualizado();
    },
    saveRegraComercial: function () {

        that = this;

        parametros = {
            'st_esquemaconfiguracao': $('#st_esquemaconfiguracao').val(),
            'id_esquemaconfiguracao': $('#id_esquemaconfiguracao').val(),

            'regra_comercial[st_solicitar_endereco_na_loja][id_itemconfiguracao]': this.ui.id_solicitar_endereco_na_loja.val(),
            'regra_comercial[st_solicitar_endereco_na_loja][st_default]': this.ui.st_solicitar_endereco_na_loja.val(),

            'regra_comercial[st_configuracao_de_venda_por_cartao_de_credito][id_itemconfiguracao]': this.ui.id_configuracao_de_venda_por_cartao_de_credito.val(),
            'regra_comercial[st_configuracao_de_venda_por_cartao_de_credito][st_default]': this.ui.st_configuracao_de_venda_por_cartao_de_credito.val(),

            'regra_comercial[st_configuracao_de_venda_por_boleto][id_itemconfiguracao]': this.ui.id_configuracao_de_venda_por_boleto.val(),
            'regra_comercial[st_configuracao_de_venda_por_boleto][st_default]': this.ui.st_configuracao_de_venda_por_boleto.val(),
        };

        this.dadosConfig.saveDadosEsquema(parametros);
    }
});

var ViewDadosRegraFinanceiro = Marionette.ItemView.extend({
    template: '#template-dados-regras-financeiro',
    tagName: 'div',
    className: 'content',
    id: 'container-regras-financeiro',
    dadosConfig: new ViewDadosConfiguracao,
    events: {
        'click #btn-salvar-regras-financeiro': 'saveRegraFinanceiro',
        'click @ui.btnHistorico': 'abreHistorico'
    },
    ui: {
        st_esquemaconfiguracao: '#st_esquemaconfiguracao',
        id_esquemaconfiguracao: '#id_esquemaconfiguracao',

        st_confirmar_recebimento_automatico: "#id_configuracao_14",
        id_confirmar_recebimento_automatico: "#id_confirmar_recebimento_automatico",

        st_quantidade_dias_cobranca_recorrente: "#id_configuracao_34",
        id_quantidade_dias_cobranca_recorrente: "#id_quantidade_dias_cobranca_recorrente",

        st_multa_para_cancelamento_sem_carta_de_credito: "#id_configuracao_15",
        id_multa_para_cancelamento_sem_carta_de_credito: "#id_multa_para_cancelamento_sem_carta_de_credito",

        st_multa_para_cancelamento_com_cartao_de_credito: "#id_configuracao_16",
        id_multa_para_cancelamento_com_cartao_de_credito: "#id_multa_para_cancelamento_com_cartao_de_credito",

        nu_validade_da_carta_de_credito: "#id_configuracao_18",
        id_validade_da_carta_de_credito: "#id_validade_da_carta_de_credito",

        st_parametro_renovacao: "#id_configuracao_31",
        id_parametro_renovacao: "#id_parametro_renovacao_automatica",

        btnHistorico: '#btn_historico'

    },
    onShow: function () {
        $('.tootiperGrad').tooltip();
        this.dadosConfig.carregarDadosEsquemaAtualizado();
    },
    abreHistorico: function () {

        if (typeof G2S.editModel[0].id_esquemaconfiguracao.id_esquemaconfiguracao == 'undefined') {
            $.pnotify({
                title: 'Aviso',
                text: 'Não é possível consultar o histórico sem o ID do esquema.',
                type: 'warning'
            });
            return;
        }

        var historicoCollection = new HistoricoCollection();
        historicoCollection.fetch({
            data: {
                id_esquemaconfiguracao: G2S.editModel[0].id_esquemaconfiguracao.id_esquemaconfiguracao
            },
            beforeSend: loading,
            complete: loaded,
            error: function (collection, response) {
                loaded();
                $.pnotify({
                    title: 'Aviso',
                    text: 'Erro ao consultar dados do histórico. ' + response.responseText,
                    type: 'error'
                });
            },
            success: function () {
                var viewHistorico = new HistoricoRenovacaoCompositeView({
                    collection: historicoCollection
                });
                layoutEsquemaConfig.regiao_modal.show(viewHistorico);
            }
        });

    },
    saveRegraFinanceiro: function () {
        that = this;


        if (!that.ui.st_parametro_renovacao.val()) {
            $.pnotify({
                title: 'Aviso',
                text: 'O parâmetro para renovação automática não foi definido.',
                type: 'warning'
            });
            that.ui.st_parametro_renovacao.focus();
            return;
        }


        parametros = {
            'st_esquemaconfiguracao': $('#st_esquemaconfiguracao').val(),
            'id_esquemaconfiguracao': $('#id_esquemaconfiguracao').val(),
            'st_validade_da_carta_de_credito': $('#nu_validadecartacredito').val(),

            'regra_financeiro[st_confirmar_recebimento_automatico][id_itemconfiguracao]': this.ui.id_confirmar_recebimento_automatico.val(),
            'regra_financeiro[st_confirmar_recebimento_automatico][st_default]': this.ui.st_confirmar_recebimento_automatico.val(),

            'regra_financeiro[st_quantidade_dias_cobranca_recorrente][id_itemconfiguracao]': this.ui.id_quantidade_dias_cobranca_recorrente.val(),
            'regra_financeiro[st_quantidade_dias_cobranca_recorrente][st_default]': this.ui.st_quantidade_dias_cobranca_recorrente.val(),

            'regra_financeiro[st_multa_para_cancelamento_sem_carta_de_credito][id_itemconfiguracao]': this.ui.id_multa_para_cancelamento_sem_carta_de_credito.val(),
            'regra_financeiro[st_multa_para_cancelamento_sem_carta_de_credito][st_default]': this.ui.st_multa_para_cancelamento_sem_carta_de_credito.val(),

            'regra_financeiro[st_multa_para_cancelamento_com_cartao_de_credito][id_itemconfiguracao]': this.ui.id_multa_para_cancelamento_com_cartao_de_credito.val(),
            'regra_financeiro[st_multa_para_cancelamento_com_cartao_de_credito][st_default]': this.ui.st_multa_para_cancelamento_com_cartao_de_credito.val(),

            'regra_financeiro[st_validade_da_carta_de_credito][id_itemconfiguracao]': this.ui.id_validade_da_carta_de_credito.val(),
            'regra_financeiro[st_validade_da_carta_de_credito][st_default]': this.ui.nu_validade_da_carta_de_credito.val(),

            'regra_financeiro[st_parametro_renovacao][id_itemconfiguracao]': this.ui.id_parametro_renovacao.val(),
            'regra_financeiro[st_parametro_renovacao][st_default]': this.ui.st_parametro_renovacao.val(),
        };

        this.dadosConfig.saveDadosEsquema(parametros);
    }
});


/*********************************************
 ****** VIEW DE AUXILIO PARA HISTORICO *******
 *********************************************/
var HistoricoRenovacaoItemView = Marionette.ItemView.extend({
    template: '#template-historico-lista',
    tagName: 'tr',
});

var HistoricoEmptyView = Marionette.ItemView.extend({
    template: '#template-historico-lista-vazia',
    tagName: 'tr'
});

var HistoricoRenovacaoCompositeView = Marionette.CompositeView.extend({
    template: "#tempalte-modal-historico-parametro-renovacao",
    tagName: 'div',
    className: 'modal hide fade',
    childView: HistoricoRenovacaoItemView,
    emptyView: HistoricoEmptyView,
    childViewContainer: 'tbody',
    onShow: function () {
        this.$el.modal('show');
        return this;
    }
});

/*********************************************
 ************** LAYOUT ***********************
 *********************************************/
var LayoutEsquemaConfiguracao = Marionette.LayoutView.extend({
    template: '#template-dados-esquema-configuracao',
    tagName: 'div',
    className: 'container-fluid',
    regions: {
        regras_funcionamento: '#tab-regras-funcionamento',
        regras_pedagogico: '#tab-regras-pedagogico',
        regras_portal_aluno: '#tab-regras-portal-aluno',
        regras_comercial: '#tab-regras-comercial',
        regras_financeiro: '#tab-regras-financeiro',
        dados_esquema: '#container-dados-esquema',
        regiao_modal: '#regiao-modal'
    },
    events: {
        "click #link-regras-funcionamento": "showRegraFuncionamento",
        "click #link-regras-pedagogico": "showRegraPedagogico",
        "click #link-regras-portal-aluno": "showRegraPortalAluno",
        "click #link-regras-comercial": "showRegraComercial",
        "click #link-regras-financeiro": "showRegraFinanceiro",
    },
    initialize: function () {
        thatLayout = this;
    },
    onShow: function () {
        this.showRegraFuncionamento();
        this.dados_esquema.show(new ViewDadosConfiguracao());
    },
    showRegraFuncionamento: function () {
        console.log('Carregando View Regra Funcionamento')
        this.regras_funcionamento.show(new ViewDadosRegraFuncionamento());
    },
    showRegraPedagogico: function () {
        console.log('Carregando View Regra Pedagógico')
        this.regras_pedagogico.show(new ViewDadosRegraPedagogico());
    },
    showRegraPortalAluno: function () {
        console.log('Carregando View Regra Portal do Aluno')
        this.regras_portal_aluno.show(new ViewDadosRegraPortalAluno());
    },
    showRegraComercial: function () {
        console.log('Carregando View Regra Comercial')
        this.regras_comercial.show(new ViewDadosRegraComercial());
    },
    showRegraFinanceiro: function () {
        console.log('Carregando View Regra Financeiro')
        this.regras_financeiro.show(new ViewDadosRegraFinanceiro());
    }
});

/*********************************************
 ************** INICIALIZAÇÃO ****************
 *********************************************/

var layoutEsquemaConfig = new LayoutEsquemaConfiguracao();
G2S.show(layoutEsquemaConfig);
var DisciplinaModel = Disciplina.extend({
    initialize: function () {
        this.attributes.is_editing = false;
    },
    idAttribute: 'id_disciplina',
    toggleEdit: function () {
        this.is_editing = !this.is_editing;
    },
    parse: function (response) {
        var result = response[0];
        if (result) {
            result.bl_compartilhargrupo = result.bl_compartilhargrupo ? 1 : 0;
            result.bl_provamontada = result.bl_provamontada ? 1 : 0;
            result.bl_disciplinasemestre = result.bl_disciplinasemestre ? 1 : 0;
            result.bl_coeficienterendimento = result.bl_coeficienterendimento ? 1 : 0;
            result.bl_cargahorariaintegralizada = result.bl_cargahorariaintegralizada ? 1 : 0;
            result.sg_uf = result.sg_uf ? result.sg_uf : null;
        }
        return result;
    }
});

/**
 * Model criada para select de professores
 * @type Model @exp;Backbone@pro;Model@call;extend
 */
var SelectProfessorModel = Backbone.Model.extend({});

/**
 * @type Collection para Select de Professores @exp;Backbone@pro;Collection@call;extend
 */
var SelectProfessorCollection = Backbone.Collection.extend({
    model: SelectProfessorModel
});


/**
 * @type VwUsuarioPerfilEntidadeReferenciaCollection
 * @exp;Backbone
 * @pro;Model
 * @call;extend
 */
var VwUsuarioPerfilEntidadeReferenciaCollection = Backbone.Collection.extend({
    model: VwUsuarioPerfilEntidadeReferenciaModel,
    url: '/api/vw-usuario-perfil-entidade-referencia/'
});
/**
 * @type UsuarioPerfilEntidadeReferenciaCollection
 * @exp;Backbone
 * @pro;Model
 * @call;extend
 */
var UsuarioPerfilEntidadeReferenciaCollection = Backbone.Collection.extend({
    model: UsuarioPerfilEntidadeReferenciaModel,
    url: '/api/usuario-perfil-entidade-referencia/'
});
/**
 * @type NivelEnsinoCollection
 * @exp;Backbone
 * @pro;Collection
 * @call;extend
 */
var NivelEnsinoCollection = Backbone.Collection.extend({
    model: NivelEnsinoModel,
    url: '/api/nivel-ensino'
});
/**
 * @type VwSerieNivelEnsinoCollection
 * @exp;Backbone
 * @pro;Collection
 * @call;extend
 */
var VwSerieNivelEnsinoCollection = Backbone.Collection.extend({
    model: VwSerieNivelEnsinoModel,
    url: '/api/vw-serie-nivel-ensino'
});
/**
 * @type AreaConhecimentoCollection
 * @exp;Backbone
 * @pro;Collection
 * @call;extend
 */
var AreaConhecimentoCollection = Backbone.Collection.extend({
    model: AreaConhecimento,
    url: '/api/area-conhecimento'
});
/**
 * @type NivelEnsinoCollection
 * @exp;Backbone
 * @pro;Collection
 * @call;extend
 */
var EntidadeCollection = Backbone.Collection.extend({
    model: EntidadeModel,
    url: '/util/get-entidade-recursiva'
});
/**
 * @type DisciplinaConteudo
 * @exp;Backbone
 * @pro;Model
 * @call;extend
 */
var DisciplinaConteudoExtend = DisciplinaConteudo.extend({
    fileAttribute: 'attachment'
});
/**
 * @type DisciplinaConteudoCollection
 * @exp;Backbone
 * @pro;Collection
 * @call;extend
 */
var DisciplinaConteudoCollection = Backbone.Collection.extend({
    model: DisciplinaConteudoExtend,
    url: '/api/disciplina-conteudo'
});
/**
 * @type VwDisciplinaIntegracaoMoodleCollection
 * @exp;Backbone
 * @pro;Collection
 * @call;extend
 */
var VwDisciplinaIntegracaoMoodleCollection = Backbone.Collection.extend({
    model: VwDisciplinaIntegracaoMoodleModel,
    url: '/api/vw-disciplina-integracao-moodle'
});


/**
 * @type ViewVwUsuarioPerfilEntidadeReferencia
 * @exp;Backbone
 * @pro;View
 * @call;extend
 */
var ViewVwUsuarioPerfilEntidadeReferencia = Backbone.View.extend({
    initialize: function () {
        this.render = _.bind(this.render, this);
        this.model.bind('change', this.render);
    },
    template: function () {
        var templateHtml = '';
        if (!this.model.is_editing) {
            templateHtml = '<td><%=st_nomecompleto%></td>'
                + '<td><%=nu_porcentagem%>%</td>'
                + '<td>'
                + '<a class="btn edit-link" data-toggle="tooltip" title="Editar Item">Editar</a>'
                + '</td>';
        } else {
            templateHtml = '<td>'
                + '<input type="hidden" name="id_perfilreferencia">'
                + '<select name="id_usuario" class="span12">'
                + '<option value="">Selecione</option>'
                + '</select>'
                + '</td>'
                + '<td>'
                + '<div class="input-append">'
                + '<input class="span8 numeric" type="text" name="nu_porcentagem" value="<%=nu_porcentagem%>">'
                + '<span class="add-on">%</span>'
                + '</div>'
                + '</td>'
                + '<td>'
                + '<a class="btn btn-success salva-autor" data-toggle="tooltip" title="Salvar Item"><i class="icon-ok icon-white"></i></a>'
                + '<a class="btn-danger btn remove-autor" data-toggle="tooltip" title="Remover Item"><i class="icon-remove icon-white"></i></a>'
                + '</td>';
        }
        return templateHtml;
    },
    populaSelectAutor: function () {
        that = this;
        var ModeloSelect = Backbone.Model.extend({});
        var CollectionSelect = Backbone.Collection.extend({
            model: ModeloSelect
        });

        var SelectUserView = Backbone.View.extend({
            render: function () {
                this.$el.attr('value', this.model.get('id_usuario')).text(this.model.get('st_nomecompleto'));
                return this;
            }
        });

        var collectionSelect = new CollectionSelect();
        if (typeof (Autores) != 'undefined') {
            collectionSelect = new CollectionSelect(Autores);
        }
        var renderer = new CollectionView({
            collection: collectionSelect,
            childViewConstructor: SelectUserView,
            childViewTagName: 'option',
            el: that.$el.find('td').find('select[name="id_usuario"]')
        });
        renderer.render();

    },
    render: function () {
        var variaveis = this.model.toJSON();
        this.el.innerHTML = _.template(this.template(), variaveis);
        if (this.model.is_editing) {
            $(this.el).addClass('edit');
            this.populaSelectAutor();
        }
        if (!this.model.id) {
            $(this.el).find('.salva-autor').hide();
        }
        if (this.model.is_editing && this.model.get('id_usuario')) {
            $(this.el).find('select[name="id_usuario"]').val(this.model.get('id_usuario'));
        }
        return this;
    },
    toggleEdit: function () {
        this.oldSettings = this.model.toJSON();
        this.model.toggleEdit();
        this.render();
    },
    removeAutor: function (e) {
        elemento = e.currentTarget;
        that = this;
        if (this.model.id) {
            bootbox.confirm("Tem certeza de que deseja desvincular este registro?", function (result) {
                if (result) {
                    that.model.destroy({});
                    that.remove();
                    bootbox.hideAll();
                    $.pnotify({
                        title: 'Registro Removido',
                        text: 'O registro foi desvinculado com sucesso!',
                        type: 'success'
                    });
                }
            });
        } else {
            $(elemento).parent('td').parent('tr').remove();
            return false;
        }
    },
    salvaAutor: function () {
        that = this;
        var id_usuario = $(this.el).find('select[name="id_usuario"]').val();
        var st_nomecompleto = $(this.el).find('select[name="id_usuario"]').find('option:selected').text();
        var nu_porcentagem = $(this.el).find('input[name="nu_porcentagem"]').val();
        nu_porcentagem = parseFloat(nu_porcentagem.replace(',', '.'), 10).toFixed(2);
        var id_perfil = $("#id_perfil").val();
        if (!this.model.id) {
            this.model.id = null;
            this.model.set({id_disciplina: $("#id").val()});
        }
        this.model.set({
            id_usuario: id_usuario,
            nu_porcentagem: nu_porcentagem,
            id_perfil: id_perfil,
            st_nomecompleto: st_nomecompleto
        });
        this.model.save(null, {
            success: function (model, response) {
                that.toggleEdit();
                $.pnotify({title: 'Registro Salvo', text: 'Vinculos cadastrados com Sucesso!', type: 'success'});
            },
            error: function (model, response) {
                $.pnotify({
                    title: 'Erro ao salvar o registro',
                    text: 'Houve um problema ao cadastrar vínculos de autor , tente novamente',
                    type: 'error'
                });
            }
        });
    },
    events: {
        'click .edit-link': 'toggleEdit',
        'click .remove-autor': 'removeAutor',
        'click .salva-autor': 'salvaAutor'
    }
});
/**
 * @type ViewNivelEnsino
 * @exp;Backbone
 * @pro;View
 * @call;extend
 */
var ViewNivelEnsino = Backbone.View.extend({
    initialize: function () {
        this.render = _.bind(this.render, this);
    },
    render: function () {
        var variaveis = this.model.toJSON();
        var elemento = $(this.el).attr('value', variaveis.id_nivelensino).text(variaveis.st_nivelensino);
        $(this.el).append(elemento); //renderiza o template
        return this;
    }
});
/**
 * @type ViewVwSerieNivelEnsino
 * @exp;Backbone
 * @pro;View
 * @call;extend
 */
var ViewVwSerieNivelEnsino = Backbone.View.extend({
    className: 'checkbox',
    initialize: function () {
        this.render = _.bind(this.render, this);
    },
    template: '<input type="checkbox" name="id_serie" value="<%=id_serie%>"> <%=st_serie%>',
    render: function () {
        var variaveis = this.model.toJSON();
        var tempHtml = _.template(this.template, variaveis);
        $(this.el).append(tempHtml); //renderiza o template
        return this;
    }
});
/**
 * @type ViewAreaConhecimento
 * @exp;Backbone
 * @pro;View
 * @call;extend
 */
var ViewAreaConhecimento = Backbone.View.extend({
    className: "row-fluid",
    initialize: function () {
        this.render = _.bind(this.render, this);
    },
    template: '<input type="checkbox" name="id_areaconhecimento" value="<%=id_areaconhecimento%>"> <%=st_areaconhecimento%>',
    render: function () {
        var variaveis = this.model.toJSON();
        var tempHtml = _.template(this.template, variaveis);
        $(this.el).append(tempHtml);
        return this;
    },
});
/**
 * @type ViewDisciplinaSerieNivelEnsino
 * @exp;Backbone
 * @pro;View
 * @call;extend
 */
var ViewDisciplinaSerieNivelEnsino = Backbone.View.extend({
    arrNivelEnsino: {},
    initialize: function () {
        this.render = _.bind(this.render, this);
    },
    template: function () {
        var html = '<div class="row-fluid item-escolaridade">'
            + '<div class="span10">'
            + '<div class="span5">'
            + '<input type="hidden" name="id_nivelensino" value="<%=id_nivelensino%>">'
            + '<%=st_nivelensino%>'
            + '</div>'
            + '<div class="span7">'
            + '<%=st_series%>'
            + '</div>'
            + '</div>'
            + '<div class="span2">'
            + '<a class="btn-danger btn delete-item pull-right" data-toggle="tooltip" title="Remover Item"><i class="icon-remove icon-white"></i></a>'
            + '</div>'
            + '</div>';
        return html;
    },
    render: function () {
        var data = this.model.toJSON();
        this.el.innerHTML = _.template(this.template(), data);
        return this;
    },
    populeIdNivelEnsino: function () {
        that = this;
        var collectionSelect = new SelectProfessorCollection();
        if (typeof (NivelEnsino) != 'undefined') {
            collectionSelect = new SelectProfessorCollection(NivelEnsino);
        }
        var renderer = new CollectionView({
            collection: collectionSelect,
            childViewConstructor: ViewNivelEnsino,
            childViewTagName: 'option',
            el: that.$el.find('div.span5').find('select[name="id_nivelensino"]')
        });
        renderer.render();
    }

});


var EscolaridadeView = Backbone.View.extend({
    render: function () {
        this.el.innerHTML = _.template(this.template(), {});
        this.populaIdNivelEnsino();
        return this;
    },
    template: function () {
        return '<div class="row-fluid item-escolaridade">'
            + '<div class="span10">'
            + '<div class="span5">'
            + '<select name="id_nivelensino" class="span12" required="required">'
            + '<option value="">Selecione</option>'
            + '</select>'
            + '</div>'
            + '<div class="span7"></div>'
            + '</div>'
            + '<div class="span2">'
            + '<a class="btn-danger btn delete-item pull-right" data-toggle="tooltip" title="Remover Item"><i class="icon-remove icon-white"></i></a>'
            + '</div>'
            + '</div>';
    },
    populaIdNivelEnsino: function () {
        that = this;
        var collectionSelect = new SelectProfessorCollection();
        if (typeof (NivelEnsino) != 'undefined') {
            collectionSelect = new SelectProfessorCollection(NivelEnsino);
        }

        var emptyOption = new SelectProfessorModel();
        emptyOption.set({id_nivelensino: "", st_nivelensino: '[Selecione]'});
        collectionSelect.models.unshift(emptyOption);

        var renderer = new CollectionView({
            collection: collectionSelect,
            childViewConstructor: ViewNivelEnsino,
            childViewTagName: 'option',
            el: that.$el.find('div.span5').find('select[name="id_nivelensino"]')
        });
        renderer.render();
    },
    getSerieNivelEnsino: function (e) {
        var value = $(e.currentTarget).val();
        if (value) {
            loading();
            var collectionSerie = new VwSerieNivelEnsinoCollection();
            collectionSerie.url = '/disciplina/get-serie-nivel-ensino/' + (value ? 'id_nivelensino/' + value : '');
            collectionSerie.fetch({
                success: function () {
                    renderer = new CollectionView({
                        collection: collectionSerie,
                        childViewConstructor: ViewVwSerieNivelEnsino,
                        childViewTagName: 'label',
                        el: $(e.currentTarget).parent('div.span5').next('div.span7')
                    });
                    renderer.render();
                    loaded();
                }
            });
        } else {
            $(e.currentTarget).parent('div.span5').next('div.span7').empty();
        }
    },
    events: {
        'change select[name="id_nivelensino"]': 'getSerieNivelEnsino'
    }
});


var ViewProfessor = Backbone.View.extend({
    className: 'row-fluid',
    initialize: function () {
        this.render = _.bind(this.render, this);
        this.model.bind('change', this.render);
    },
    render: function () {
        var variaveis = this.model.toJSON();
        this.el.innerHTML = _.template(this.toggleTemplate(), variaveis);
        if (this.model.is_editing) {
            this.populaSelectProfessor();
        }
        return this;
    },
    toggleTemplate: function () {
        var elementoHtml = '';
        if (this.model.is_editing) {
            elementoHtml += '<div class="span9">'
                + '<select name="professor" class="span12">'
                + '<option value="">Selecione</option>'
                + '</select>'
                + '</div>'
                + '<div class="span3">'
                + '<a class="btn-danger btn delete-item pull-right" data-toggle="tooltip" title="Remover Item"><i class="icon-remove icon-white"></i></a>'
                + '</div>';
        } else {
            elementoHtml += '<div class="span9">'
                + '<%=st_nomecompleto%>'
                + '</div>'
                + '<div class="span3">'
                + '<a class="btn-danger btn delete-item pull-right" data-toggle="tooltip" title="Remover Item"><i class="icon-remove icon-white"></i></a>'
                + '</div>';
        }
        return elementoHtml;
    },
    populaSelectProfessor: function () {
        that = this;
        var collectionSelect = new SelectProfessorCollection();
        if (typeof (Professores) != 'undefined') {
            collectionSelect = new SelectProfessorCollection(Professores);
        }
        var selectOptions = new CollectionView({
            collection: collectionSelect,
            childViewConstructor: SelectProfessorView,
            childViewTagName: 'option',
            el: that.$el.find("div.span9").find('select[name="professor"]')
        });
        selectOptions.render();
    },
    removeProfessor: function () {
        that = this;
        if (that.model.id) {
            bootbox.confirm("Tem certeza de que deseja desvincular este registro?", function (result) {
                if (result) {
                    that.model.destroy({});
                    that.remove();
                    bootbox.hideAll();
                    $.pnotify({
                        title: 'Registro Removido',
                        text: 'O registro foi desvinculado com sucesso!',
                        type: 'success'
                    });
                }
            });
        }
        that.remove();
    },
    events: {
        'click .delete-item': 'removeProfessor'
    }
});


/**
 * @type ItemView para select de professoress @exp;Marionette@pro;ItemView@call;extend
 */
var SelectProfessorView = Backbone.View.extend({
    render: function () {
        this.$el.attr('value', this.model.get('id_usuario')).text(this.model.get('st_nomecompleto'));
        return this;
    }
});
//-------------------------- INTEGRAÇÃO ---------------------------//
/**
 * @type ViewDisciplinaIntegracaoMoodle
 * @exp;Backbone
 * @pro;View
 * @call;extend
 */
var ViewDisciplinaIntegracaoMoodle = Backbone.View.extend({
    initialize: function () {
        if (!this.model) {
            this.model = new VwDisciplinaIntegracaoMoodleModel();
        }
        this.render = _.bind(this.render, this);
    },
    template: function () {
        var html = '<div class="row-fluid item-integracao">'
            + '<div class="span10">'
            + '<div class="span3">'
            + '<input type="hidden" id="id_sistema" name="id_sistema" value="<%=id_sistema%>">'
            + '<label class="span12">Nome Sala de Referência: <%=st_salareferencia%></label>'
            + '</div>'

            + '<div class="span3">'
            + '<input type="hidden" name="id_disciplinaintegracao" value="<%=id_disciplinaintegracao%>">'
            + '<label class="span12">Código integração: <%=st_codsistema%></label>'
            + '</div>'

            + '<div class="span3">'
            + '<label class="span12">Entidade: <%=st_nomeentidade%></label>'
            + '</div>'

            + '<div class="span3">'
            + '<label class="span12">Moodle: <%=st_moodleintegracao%></label>'
            + '</div>'
            + '</div>'

            + '<div class="span2">'
            + '<a class="btn-danger btn delete-item pull-right" data-toggle="tooltip" title="Remover Item" id="<%=id_disciplinaintegracao%>"><i class="icon-remove icon-white"></i></a>'
            + '</div>'
            + '</div>'

            + '</div>';
        return html;
    },
    render: function () {
        var data = this.model.toJSON();
        this.el.innerHTML = _.template(this.template(), data);
        return this;
    }

});


var IntegracaoFabricaDeProvasCollection = Backbone.Collection.extend({
    model: Backbone.Model,
    url: 'integracao-fabrica-de-provas/listar-provas',
    parse: function (response) {
        return response.mensagem || response;
    }
});

var IntegracaoFabricaDeProvasItem = Marionette.ItemView.extend({
    template: '#template-linha-provas',
    tagName: 'tr'
});

var IntegracaoFabricaDeProvas = Marionette.LayoutView.extend({
    model: new DisciplinaModel(),
    ui: {
        nomeProvaVinculada: '#prova-vinculada',
        detalhesIntegracao: '#detalhes-integracao',
        btnDesvincular: '#btn-desvincular',
        btnVincular: '#btn-vincular'
    },
    regions: {
        modal: '#modal-container'
    },
    initialize: function (options) {
        this.tipoProva = options.tipoProva;
        this.model.set('id_disciplina', G2S.editModel.id_disciplina);
        this.model.fetch({async: false});
    },
    onRender: function () {
        this.verificaProvaVinculada();
    },
    template: '#integracao-fabrica-provas',
    events: {
        "click #btn-vincular": "abreModal",
        "click #btn-desvincular": "desvincularProva"
    },
    abreModal: function () {
        var modalComposite = new ModalIntegracaoFabricaProvasComposite({
            tipoProva: this.tipoProva,
            model: this.model
        });
        this.modal.show(modalComposite);
        modalComposite.$el.modal("show");
    },
    verificaProvaVinculada: function () {

        if (this.tipoProva === TIPO_PROVA_GRADUACAO.FINAL) {
            this.ui.detalhesIntegracao.text("Prova Final - " + this.model.get('st_disciplina'));
            if (this.model.get("st_provarecuperacaointegracao")) {
                this.ui.nomeProvaVinculada.text(this.model.get('st_provarecuperacaointegracao'));
                this.ui.btnDesvincular.removeClass("hide");
                this.ui.btnVincular.text("Alterar");
            }
            return true;
        }

        this.ui.detalhesIntegracao.text("Prova Presencial - " + this.model.get('st_disciplina'));
        if (this.model.get("st_provaintegracao")) {
            this.ui.nomeProvaVinculada.text(this.model.get('st_provaintegracao'));
            this.ui.btnDesvincular.removeClass("hide");
            this.ui.btnVincular.text("Alterar");
        }
    },
    desvincularProva: function () {
        var that = this;
        bootbox.confirm('Deseja realmente desvincular essa prova?', function (response) {
            if (response) {
                // Backbone.save() não permite setar e salvar valores como null.
                if (that.tipoProva === TIPO_PROVA_GRADUACAO.FINAL) {
                    that.model.save({
                        st_provarecuperacaointegracao: "",
                        st_codprovarecuperacaointegracao: ""
                    }).always(function () {
                        that.render();
                    });
                    return true;
                }

                that.model.save({
                    st_provaintegracao: "",
                    st_codprovaintegracao: ""
                }).always(function () {
                    that.render();
                });
            }
        });
    }
});

var ModalIntegracaoFabricaProvasComposite = Marionette.CompositeView.extend({
    initialize: function (options) {
        this.tipoProva = options.tipoProva;
        this.model = options.model;
    },
    template: '#modal-integracao-fabrica-de-provas',
    className: 'modal hide fade',
    collection: new IntegracaoFabricaDeProvasCollection(),
    childViewContainer: 'tbody',
    childView: IntegracaoFabricaDeProvasItem,
    emptyView: Marionette.ItemView.extend({
        tagName: 'tr',
        template: _.template('<td colspan="2">Nenhuma prova encontrada.</td>')
    }),
    ui: {
        tabela: 'table',
        form: 'form',
        btnSalvarProvaModal: '#btn-salvar-prova-modal'
    },
    events: {
        'submit @ui.form': 'actionSearch',
        'click @ui.btnSalvarProvaModal': 'vincularProva'
    },
    actionSearch: function (e) {
        var that = this;
        e && e.preventDefault ? e.preventDefault() : null;
        this.ui.tabela.removeClass('hide');
        that.collection.fetch({
            data: Backbone.Syphon.serialize(this),
            async: false,
            beforeSend: loading,
            complete: function () {
                loaded();
            },success: function () {
                that.collection.length
                    ? that.ui.btnSalvarProvaModal.removeClass("hide")
                    : that.ui.btnSalvarProvaModal.addClass("hide");
            },error: function (c,response) {
                $.pnotify({
                    'type': "alert",
                    "title": "Alerta",
                    "text": response.responseJSON.text
                });
            }
        });
    },
    vincularProva: function () {
        var $el_cod = this.$el.find("input[type=radio]:checked");
        var $el_titulo = $el_cod.closest('tr').find('input[type=hidden]');

        if (!$el_cod.length) {
            $.pnotify({
                'type': "alert",
                "title": "Alerta",
                "text": "Selecione uma prova para vinculação."
            });
            return false;
        }

        this.tipoProva == TIPO_PROVA_GRADUACAO.FINAL
            ? this.vincularProvaFinal($el_cod, $el_titulo)
            : this.vincularProvaPresencial($el_cod, $el_titulo);
    },
    vincularProvaFinal: function (cod, titulo) {
        var that = this;

        this.model.save({
            "st_codprovarecuperacaointegracao": cod.val(),
            "st_provarecuperacaointegracao": titulo.val()
        }, {
            success: function (model, response) {
                $.pnotify({
                    'title': 'Sucesso',
                    'text': 'Prova vinculada com sucesso.',
                    'type': 'success'
                });
            },
            error: function (model, response) {
                $.pnotify({
                    'title': 'Erro',
                    'texto': 'Não foi possível vincular a prova.',
                    'type': 'error'
                });
            },
            complete: function (model, response) {
                that.$el.modal('hide');
                integracaoFabricaDeProvasFinalIni.render();
                integracaoFabricaDeProvasPresencialIni.render();
            }
        });
    },
    vincularProvaPresencial: function (cod, titulo) {
        var that = this;

        this.model.save({
            "st_codprovaintegracao": cod.val(),
            "st_provaintegracao": titulo.val()
        }, {
            success: function (model, response) {
                $.pnotify({
                    'title': 'Sucesso',
                    'text': 'Prova vinculada com sucesso.',
                    'type': 'success'
                });
            },
            error: function (model, response) {
                $.pnotify({
                    'title': 'Erro',
                    'texto': 'Não foi possível vincular a prova.',
                    'type': 'error'
                });
            },
            complete: function (model, response) {
                that.$el.modal('hide');
                integracaoFabricaDeProvasFinalIni.render();
                integracaoFabricaDeProvasPresencialIni.render();
            }
        });
    }
});

var integracaoFabricaDeProvasFinalIni = new IntegracaoFabricaDeProvas({tipoProva: TIPO_PROVA_GRADUACAO.FINAL});
var integracaoFabricaDeProvasPresencialIni = new IntegracaoFabricaDeProvas({tipoProva: TIPO_PROVA_GRADUACAO.PRESENCIAL});

/**
 * @type ViewEntidades
 * @exp;Backbone
 * @pro;View
 * @call;extend
 */
var ViewEntidades = Backbone.View.extend({
    initialize: function () {
        this.render = _.bind(this.render, this);
    },
    render: function () {
        var variaveis = this.model.toJSON();
        var elemento = $(this.el).attr('value', variaveis.id_entidade).text(variaveis.st_nomeentidade);
        $(this.el).append(elemento); //renderiza o template
        return this;
    }
});


var IntegracaoView = Marionette.CompositeView.extend({
    onRender: function () {
        this.ui.idSistema.val(SISTEMAS.MOODLE);
        this.populaIdDisciplinaIntegracao();
        return this;
    },
    ui: {
        "idEntidade": "select[name='id_entidade']",
        "idMoodle": "select[name='id_moodle']",
        "idSistema": "#id_sistema"
    },
    template: '#integracao-disciplina-new-template',
    events: {
        "change @ui.idEntidade": "populaSelectMoodle"
    },
    populaIdDisciplinaIntegracao: function () {
        if (typeof Entidades == "undefined") {
            $.pnotify({
                "title": "Atenção!",
                "text": "Nenhuma Entidade encontrada.",
                "type": "warning",
            })
            return;
        }

        var collectionSelect = new SelectProfessorCollection();
        collectionSelect.add(Entidades);
        (new SelectView({
            el: this.ui.idEntidade,
            collection: collectionSelect,
            sort: "st_nomeentidade",
            childViewOptions: {
                optionLabel: "st_nomeentidade",
                optionValue: "id_entidade",
            }
        })).render();
    },
    populaSelectMoodle: function () {
        var idEntidade = this.ui.idEntidade.val();
        if (idEntidade) {
            ComponenteSelectMoodle
                .render({
                    el: this.ui.idMoodle,
                    data: {
                        id_entidade: idEntidade,
                        id_sistema: this.ui.idSistema.val()
                    }
                });
        }

    }
});
//-------------------------- FIM  INTEGRAÇÃO ---------------------------//

//-------------------------- CONTEUDO ---------------------------//

/**
 * Item view para conteudo da tabela de conteudos cadastrados da disciplina
 *
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
var ConteudoRowView = Marionette.ItemView.extend({
    template: '#templateItemTableDisciplinaConteudo',
    tagName: 'tr',
    events: {
        'click .btn-danger': 'delete'
    },
    delete: function () {
        var that = this;

        bootbox.confirm("Tem certeza de que deseja apagar o registro?", function (result) {
            if (result) {

                that.model.url = '/disciplina/salvar-conteudo';
                that.model.set('bl_ativo', false);
                that.model.unset('attachment');
                that.model.save(null, {
                    beforeSend: function () {
                        loading();
                    },
                    complete: function () {
                        that.remove();
                        loaded();
                    },
                    success: function () {
                        $.pnotify({
                            title: 'Sucesso!',
                            text: 'Conteúdo removido com sucesso.',
                            type: 'success'
                        });
                    },
                    error: function (model, response) {
                        $.pnotify({
                            title: 'Erro!',
                            text: response.responseText,
                            type: 'error'
                        });
                    }
                });
            }
        });


    }
});

/**
 * Item View para formulario de envio de conteudo
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
var FormConteudoItemView = Marionette.ItemView.extend({
    template: '#templateItemFormDisciplinaConteudo',
    tagName: 'div',
    className: 'controls',
    isSend: false,
    ui: {
        inputText: 'input[type="text"]',
        inputFile: 'input[type="file"]',
        progressBar: '.progress'
    },
    events: {
        'click .btn-danger': 'removeFormConteudoItem',
        'change input[type="text"]': 'setDescricaoConteudo',
        'change input[type="file"]': 'setAttachment',
        'click .btn-success': 'validaAcaoUploadPorItem'
    },
    onRender: function () {
        this.model.set('id_usuariocadastro', G2S.loggedUser.get('id_usuario'));
    },
    validaAcaoUploadPorItem: function () {
        if (!this.ui.inputText.val()) {
            this.ui.inputText.focus();
            return false;
        }

        if (!this.ui.inputFile.val()) {
            this.ui.inputFile.focus();
            return false;
        }

        var fileObject = this.ui.inputFile[0].files[0];
        if (fileObject.type !== 'application/pdf' && fileObject.type !== 'application/zip') {
            $.pnotify({title: 'Atenção!', type: 'warning', text: MensagensG2.MSG_DISCIPLINA_04});
            return false;
        }

        this.upload();
    },
    upload: function () {

        if (!this.isSend) {

            var that = this;
            this.ui.progressBar.show();

            this.model.url = '/disciplina/salvar-conteudo';
            this.model.save(null, {
                async: false,
                beforeSend: function () {
                    loading();
                    that.isSend = true;
                    that.ui.progressBar.find('.bar').css({width: 50 + '%'});
                },
                complete: function () {
                    loaded();
                    that.ui.progressBar.find('.bar').css({width: 100 + '%'});
                },
                success: function () {
                    that.triggerMethod('add:model');
                },
                error: function () {
                    that.isSend = false;
                    $.pnotify({title: 'Alerta', type: 'warning', text: MensagensG2.MSG_DISCIPLINA_03});
                }
            });

        } else {
            $.pnotify({title: 'Alerta', type: 'warning', text: MensagensG2.MSG_DISCIPLINA_02});
        }
    },
    setDescricaoConteudo: function () {
        this.model.set('st_descricaoconteudo', this.ui.inputText.val());
    },
    setAttachment: function () {
        var fileObject = this.ui.inputFile[0].files[0];
        this.model.set('attachment', fileObject);

        if (fileObject.type == 'application/pdf') {
            this.model.set('id_formatoarquivo', EXTENSAO_ARQUIVO.PDF);
        }
        if (fileObject.type == 'application/zip') {
            this.model.set('id_formatoarquivo', EXTENSAO_ARQUIVO.ZIP);
        }
    },
    removeFormConteudoItem: function () {
        if (this._index != 0) {
            this.model.destroy();
        } else {
            this.render();
        }
    }
});

/**
 * Colection view para formularios de envio de conteudo
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
var FormConteudoCollectionView = Marionette.CollectionView.extend({
    childView: FormConteudoItemView,
    uploadAllModels: function () {
        var that = this;

        //Pega cada item view do collection view e da ação de enviar o conteudo de cada um
        this.collection.each(function (model) {
            that.children.findByModel(model).upload();
        });
    },
    childEvents: {
        'add:model': function (itemView) {
            ConteudoCompositeView.collection.add(itemView.model);
        }
    }
});

/**
 * Composite view para aba conteudo da disciplina
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
var ConteudoCompositeViewExtend = Marionette.CompositeView.extend({
    template: '#templateConteudo',
    childViewContainer: 'tbody',
    childView: ConteudoRowView,
    collectionItensForm: {},
    collectionViewItensForm: {},
    disciplinaModel: {},
    initialize: function (params) {
        this.disciplinaModel = params.disciplinaModel;
    },
    onRender: function () {

        this.collection.fetch({
            data: {
                bl_ativo: 1,
                id_disciplina: this.disciplinaModel.get('id_disciplina')
            },
            beforeSend: function () {
                loading()
            },
            complete: function () {
                loaded()
            }
        });

        this.collectionItensForm = new DisciplinaConteudoCollection();

        this.collectionViewItensForm = new FormConteudoCollectionView({
            el: '#container-itens-form-disciplina-conteudo',
            collection: this.collectionItensForm
        });
        this.collectionViewItensForm.render();

        //Preenchendo collection view com 1 formulario inicial de envio de conteudo
        this.collectionItensForm.add(new DisciplinaConteudoExtend({
            id_disciplina: this.disciplinaModel.get('id_disciplina')
        }));
    },
    events: {
        'click #btn-add-mais-conteudo': function () {
            //Adicionando itens do tipo formulario para usuario enviar mais de um conteudo por vez
            if (this.collectionItensForm.length <= 6) {
                this.collectionItensForm.add(new DisciplinaConteudoExtend({
                    id_disciplina: this.disciplinaModel.get('id_disciplina')
                }));
            }
        },
        'submit form': function (e) {
            e.preventDefault();
            //Invocando metodo da collection view para envio de varios conteudos por vez
            if (this.collectionViewItensForm.collection.length) {
                this.collectionViewItensForm.uploadAllModels();
                $.pnotify({title: 'Sucesso', type: 'success', text: MensagensG2.MSG_DISCIPLINA_01});
            }
        }
    }
});
//------------------------ FIM CONTEUDO ------------------------//

/**
 * @type ViewForm View principal do formulário
 * @exp;Backbone
 * @pro;View
 * @call;extend
 */
var ViewForm = Marionette.LayoutView.extend({
    regions: {
        integracaoFabricaDeProvasPresencial: '#form-integracao-fabrica-de-provas-presencial',
        integracaoFabricaDeProvasFinal: '#form-integracao-fabrica-de-provas-final'
    },
    areaDisciplinaModel: null,
    disciplinaSerieNivelEnsinoModel: null,
    usuarioPerfilModel: null,
    disciplinaIntegracaoModel: null,
    disciplinaModel: null,
    initialize: function () {
        this.areaDisciplinaModel = new AreaDisciplina();
        this.disciplinaSerieNivelEnsinoModel = new DisciplinaSerieNivelEnsino();
        this.usuarioPerfilModel = new UsuarioPerfilEntidadeReferenciaModel();
        this.disciplinaIntegracaoModel = new DisciplinaIntegracaoModel();
        this.disciplinaModel = new DisciplinaModel();
        //_.bind(this, 'render');
        return this;
    },
    render: function () {

        if (G2S.editModel.id_disciplina) {
            this.model.url = '/api/disciplina/' + G2S.editModel.id_disciplina;
            this.model.fetch({async: false});
        }

        this.disciplinaModel.set('id_disciplina', this.model.id);
        this.disciplinaModel.fetch();

        var variaveis = this.model.toJSON();
        var tempHtml = _.template($("#templateFormulario").html(), variaveis);
        this.el.innerHTML = tempHtml;

        this.$el = $(this.el);

        var bl_compartilhargrupo = parseInt(this.model.get("id_tipodisciplina"));
        var bl_provamontada = parseInt(this.model.get("bl_provamontada"));
        var bl_disciplinasemestre = parseInt(this.model.get("bl_disciplinasemestre"));
        var bl_coeficienterendimento = parseInt(this.model.get("bl_coeficienterendimento"));
        var bl_cargahorariaintegralizada = parseInt(this.model.get("bl_cargahorariaintegralizada"));

        this.$el.find("#id_situacao").val(this.model.get("id_situacao"));
        this.$el.find("#id_tipodisciplina").val(this.model.get("id_tipodisciplina"));
        this.$el.find("#bl_compartilhargrupo").val(bl_compartilhargrupo);
        this.$el.find("#bl_coeficienterendimento").val(bl_coeficienterendimento);
        this.$el.find("#bl_cargahorariaintegralizada").val(bl_cargahorariaintegralizada);
        this.$el.find("#bl_provamontada").val(bl_provamontada);
        this.$el.find("#bl_disciplinasemestre").val(bl_disciplinasemestre);
        this.$el.find("#sg_uf").val(this.model.get("sg_uf"));

        this.renderComboGrupo();
        this.populeNivelEnsino();
        this.carregaAreasConhecimento();
        this.carregaAutores();
        this.carregaProfessores();

        // Renderiza telas de integração com a Fábrica de Negócio apenas quando Graduação
        if (G2S.loggedUser.get('id_linhadenegocio') === LINHA_DE_NEGOCIO.GRADUACAO) {
            this.integracaoFabricaDeProvasPresencial.show(integracaoFabricaDeProvasPresencialIni);
            this.integracaoFabricaDeProvasFinal.show(integracaoFabricaDeProvasFinalIni);
        }

        loadTinyMCE();

        return this;
    },
    renderComboGrupo: function () {
        var that = this;

        ComboboxView({
            el: that.$el.find('#id_grupodisciplina'),
            url: '/api/grupo-disciplina',
            optionSelectedId: that.model.get('id_grupodisciplina'),
            optionLabel: 'st_grupodisciplina',
            emptyOption: 'Selecione',
            model: Backbone.Model.extend({
                idAttribute: 'id_grupodisciplina'
            })
        });
    },
    salvaDisciplina: function (e) {

        var that = this;
        e.preventDefault();
        if (that.validaForm()) {
            //Envia o formulário e a imagem.
            this.$el.find("#formCadastro").ajaxSubmit({
                // Necessário para detectar o contexto correto. Na criação de disciplinas, function. Na edição, string.
                url: typeof this.model.url === 'function' ? this.model.url() : this.model.url,
                type: 'POST', //this.model.id ? 'PUT' : 'POST',
                data: {id_usuariocadastro: G2S.loggedUser.get('id_usuario')},
                dataType: 'json',
                beforeSend: loading,
                complete: loaded,
                success: function (response) {
                    that.model.set(response);
                    //limpa o campo
                    that.$el.find("#imagem_disciplina").val('');
                    $(that.el).find("#id").val(response.id_disciplina);
                    $("#btnSalvar").text('Salvar').removeAttr("disabled");
                    $.pnotify({title: 'Registro Salvo', text: 'Disciplina salva com sucesso!', type: 'success'});
                    that.salvaAreaConhecimento();
                    that.salvaNivelEscolaridade();
                    that.salvaProfessores();
                    that.$el.find("#imagem_disciplina").val('');
                    that.render();
                },
                error: function (response) {
                    loaded();
                    $("#btnSalvar").text('Salvar').removeAttr("disabled");
                    $.pnotify({
                        title: 'Erro ao salvar o registro',
                        text: response.responseText,
                        type: 'error'
                    });
                }
            });
        }
        return true;

    },
    salvaConteudos: function (e) {
        var that = this;

        if (!this.validaForm())
            return false;

        // VALIDAR CAMPO REPETICAO SEPARADAMENTE
        var nu_repeticao = $('[name="nu_repeticao"]').val();
        var errorMsg = false;
        if (!nu_repeticao)
            errorMsg = 'O campo Repetição é obrigatório.';
        else if (nu_repeticao < 1)
            errorMsg = 'O campo Repetição deve ser maior que 0.';
        else if (nu_repeticao > 3)
            errorMsg = 'O valor máximo para o campo Repetição é 3.';

        if (errorMsg) {
            $.pnotify({
                type: 'warning',
                title: 'Campo Incorreto',
                text: errorMsg
            });
            return false;
        }

        $("#btnSalvar").text('Salvando...').attr('disabled', 'disabled');
        loading();

        this.model.set({
            nu_repeticao: $("input[name='nu_repeticao']").val(),
            sg_uf: $("select[name='sg_uf']").val()
        });

        this.model.save(null, {
            success: function (model, response) {
                that.salvaAutores();

                $.pnotify({title: 'Registro Salvo', text: 'Os conteúdos foram atualizados.', type: 'success'});
                that.render();
            },
            error: function (model, response) {
                loaded();
                $.pnotify({
                    title: 'Erro',
                    text: 'Houve um problema ao atualizar os conteúdos.',
                    type: 'error'
                });
            },
            complete: function () {
                $("#btnSalvar").text('Salvar').removeAttr("disabled");
                loaded();
            }
        });

        return false;
    },
    salvaAreaConhecimento: function () {
        var that = this;
        //debug(that.model);
        var arrData = new Array();
        areaDisciplinaModel = that.areaDisciplinaModel;

        var areasSelecionadas = that.$el.find('input[name="id_areaconhecimento"]:checked');

        //percorre os checkbox selecionados
        if (areasSelecionadas.length) {
            areasSelecionadas.each(function (i, field) {
                arrData[i] = $(field).val(); //atribui o valor em um array
            });
            //seta o modelo
            areaDisciplinaModel.set({
                id_disciplina: that.model.id,
                id_areaconhecimento: arrData
            });
            areaDisciplinaModel.url = "/disciplina/salva-area-join-disciplina"; // altera a url
            areaDisciplinaModel.save(null, {
                success: function (model, response) {
                    $.pnotify({
                        title: 'Registro Salvo',
                        text: 'Áreas de Conhecimento salva com sucesso!',
                        type: 'success'
                    });
                },
                error: function (model, response) {
                    $.pnotify({
                        title: 'Erro ao salvar o registro',
                        text: 'Houve um problema ao salvar Áreas de Conhecimento, tente novamente',
                        type: 'error'
                    });
                }
            });
        }
    },
    validaNivelEscolaridade: function () {
        var valid = true; //inicia como valido
        var error = '';

        var items = this.$el.find('#containerNivelEnsino').find('div.item-escolaridade');

        if (!items.length) {
            valid = false;
        } else {
            items.find('select[name="id_nivelensino"]').each(function () {
                var elem = $(this);

                var id_nivelensivo = elem.val();
                var el_series = elem.closest('.item-escolaridade').find('[name="id_serie"]');

                if (!id_nivelensivo) {
                    valid = false;
                    error = 'Selecione pelo menos um Nível de Ensino.';
                } else if (/*el_series.length &&*/ !el_series.filter(':checked').length) {
                    valid = false;
                    error = 'É necessário informar a série do Nível de Ensino selecionado. Caso não possua algum, faça o cadastro com o nome "Único" para este nível de ensino.';
                }
            });
        }

        //se for false mostra pedindo pro cara selecionar
        if (!valid) {
            $.pnotify({title: 'Atenção!', text: error, type: 'warning'});
        }

        //retorna
        return valid;
    },
    salvaNivelEscolaridade: function () {
        var that = this;
        //valida
        if (this.validaNivelEscolaridade()) {
            var contador = 0;
            var max = this.$el.find('#containerNivelEnsino div').find('div.item-escolaridade').length;
            this.$el.find('#containerNivelEnsino div').find('div.item-escolaridade').each(function (i, child) {
                var id_nivelensino = $(child).find('select[name="id_nivelensino"]').val();
                if (id_nivelensino) {
                    that.disciplinaSerieNivelEnsinoModel = new DisciplinaSerieNivelEnsino();
                    that.disciplinaSerieNivelEnsinoModel.set({
                        id_disciplina: that.model.id,
                        id_nivelensino: id_nivelensino,
                        id_serie: that.getChecksIdSerie(child)
                    });
                    that.disciplinaSerieNivelEnsinoModel.save(null, {
                        success: function (model, response) {
                            $.pnotify({
                                title: 'Registro Salvo',
                                text: 'Vinculos Nível de Ensino Disciplina cadastrados com sucesso com Sucesso!',
                                type: 'success'
                            });
                            contador++;
                        },
                        error: function (model, response) {
                            $.pnotify({
                                title: 'Erro ao salvar o registro',
                                text: 'Houve um problema ao salvar Nível de Ensino Disciplina, tente novamente',
                                type: 'error'
                            });
                        },
                        complete: function () {
                            if (max == contador) {
                                that.populeNivelEnsino();
                            }

                        }
                    });
                } else {
                    max--;
                }
            });

        }


    },
    salvaAutores: function () {
        that = this;
        var id_perfil = this.$el.find("#id_perfil").val();
        if (id_perfil) {
            $('#tableAutores tbody tr.edit').each(function (i, child) {
                var id_usuario = $(child).find('select[name="id_usuario"]').val();
                var nu_porcentagem = $(child).find('input[name="nu_porcentagem"]').val();
                nu_porcentagem = parseFloat(nu_porcentagem.replace(',', '.'), 10).toFixed(2);
                if (id_usuario) {
                    that.usuarioPerfilModel.set({
                        id_usuario: id_usuario,
                        nu_porcentagem: nu_porcentagem,
                        id_disciplina: that.model.id,
                        id_perfil: id_perfil
                    });
                    that.usuarioPerfilModel.save(null, {
                        success: function (model, response) {
                            $.pnotify({
                                title: 'Registro Salvo',
                                text: 'Vinculos cadastrados com Sucesso!',
                                type: 'success'
                            });
                            $(child).find('input[name="id_perfilreferencia"]').val(response.id);
                        },
                        error: function (model, response) {
                            $.pnotify({
                                title: 'Erro ao salvar o registro',
                                text: 'Houve um problema ao cadastrar vínculos de autor , tente novamente',
                                type: 'error'
                            });
                        }
                    });
                }
            });
        }
    },
    salvaProfessores: function () {
        var that = this;
        var id_perfil = $(this.el).find("#idPerfilProfessor").val();
        if (id_perfil) {
            this.$el.find("#containerProfessores").find('select[name="professor"]').each(function (i, elem) {
                var id_professor = $(elem).val();
                if (id_professor) {
                    that.usuarioPerfilModel.set({
                        id_usuario: id_professor,
                        id_disciplina: that.model.id,
                        id_perfil: id_perfil
                    });
                    that.usuarioPerfilModel.save(null, {
                        beforeSend: function () {
                            loading();
                        },
                        complete: function () {
                            that.carregaProfessores();
                            loaded();
                        },
                        success: function (model, response) {
                            $.pnotify({
                                title: 'Registro Salvo',
                                text: 'Vinculos com Professor cadastrados com Sucesso!',
                                type: 'success'
                            });
                            $(elem).find('input[name="id_perfilreferencia"]').val(response.id);
                        },
                        error: function (model, response) {
                            $.pnotify({
                                title: 'Erro ao salvar o registro',
                                text: 'Houve um problema ao cadastrar vínculos de professor , tente novamente',
                                type: 'error'
                            });
                        }
                    });
                }
            });
        } else {
            $.pnotify({
                title: 'Atenção!',
                text: 'Não é possível salvar dados do professor pois não existe nenhum perfil configurado.',
                type: 'warning'
            });
        }
    },
    getChecksIdSerie: function (childRow) {
        var dataIdSerie = [];
        $(childRow).find('input[name="id_serie"]:checked').each(function (i, field) {
            dataIdSerie[i] = $(field).val();
        });
        return dataIdSerie;
    },
    validaForm: function () {
        var retorno = true; //atribui o valor true a variavel de retorno

        //percorre todos os elementos com o atributo required
        $('#formCadastro').find('*[required="required"]').each(function (i, field) {
            var label = $(field).prev('label').text(); //atribui a label do elemento a variavel
            var valor = $(field).val(); //atribui o valor do elemento
            var fieldName = $(field).attr('name'); //atribui o name do elemento a variavel

            //Verifica se o valor foi preenchido
            if (!valor) {
                $.pnotify({
                    type: 'warning',
                    title: 'Atenção!',
                    text: 'Campo <b>' + label + '</b> é de preenchimento obrigatório.'
                });
                retorno = false;
            } else {
                switch (fieldName) {
                    case 'nu_cargahoraria':
                        if (!validaNumero(valor)) {
                            $.pnotify({
                                type: 'warning',
                                title: 'Atenção!',
                                text: 'Campo <b>' + label + '</b> é do tipo número inteiro.'
                            });
                            retorno = false; //atribui o valor false para a variavel de retorno
                        }
                        break;
                    default:
                        break;
                }
            }
        });


        if (!this.validaNivelEscolaridade())
            return false;


        /**
         * verifica campos que não estão com o atributo required
         */
        //verifica se nu_creditos foi preenchido e se o valor é um número
        if ($(this.el).find('#nu_creditos').val() && !validaNumero($(this.el).find('#nu_creditos').val())) {
//mostra mensagem
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Campo <b>' + $('#nu_creditos').prev('label').html() + '</b> é do tipo número inteiro.'
            });
            retorno = false; //atribui o valor false para a variavel de retorno
        }

        if ($("input[name='st_apelido']").val().length > 15) {
            $.pnotify({title: 'Erro', text: 'Campo apelido deve conter no máximo 15 caracteres!', type: 'error'});
            retorno = false;
            $('#divApelido').addClass('control-group error');
        } else {
            $('#divApelido').removeClass('control-group error');
        }

        return retorno; //retorna

    },
    carregaAutores: function () {
        var idDisciplina = this.model.id;
        var idPerfil = $(this.el).find("#id_perfil").val();
        if (idDisciplina) {
//Instancia a collection
            var collectionVw = new VwUsuarioPerfilEntidadeReferenciaCollection();
            collectionVw.url += 'id_disciplina/' + idDisciplina + '/id_perfil/' + idPerfil; // altera a url da collection passando parametro
            //fetch 
            collectionVw.fetch({
                success: function () {
                    renderer = new CollectionView({
                        collection: collectionVw,
                        childViewConstructor: ViewVwUsuarioPerfilEntidadeReferencia,
                        childViewTagName: 'tr',
                        el: $('#tableAutores tbody')
                    });
                    renderer.render();
                }
            });
        }
    },
    carregaAreasConhecimento: function () {
        var that = this;
        var collectionArea = new AreaConhecimentoCollection;
        collectionArea.url = '/disciplina/get-arvore-area-conhecimento';
        collectionArea.fetch({
            success: function () {
                var renderer = new CollectionView({
                    collection: collectionArea,
                    childViewConstructor: ViewAreaConhecimento,
                    childViewTagName: 'div',
                    el: viewFormRendered.$el.find("#containerAreaConhecimento")
                });
                renderer.render();
            },
            complete: function () {
                //Popula e marca checkbox
                that.populaAreaConhecimento();
            }
        });
    },
    populaAreaConhecimento: function () {
        var idDisciplina = this.model.id;
        if (idDisciplina) {
            var collectionArea = new AreaDisciplinaCollection;
            collectionArea.url += '/id_disciplina/' + idDisciplina;
            collectionArea.fetch({
                success: function () {
                    collectionArea.each(function (model, i) {
                        var elem = viewFormRendered.$el.find("#containerAreaConhecimento")
                            .find('div')
                            .find('input[value="' + model.get('id_areaconhecimento') + '"]');
                        if (elem) {
                            $(elem).attr('checked', 'checked');
                        }

                    });
                }
            });
        }
    },
    populeNivelEnsino: function () {
        var idDisciplina = this.model.id;
        var arrSeries = [];
        if (idDisciplina) {
            $.ajax({
                url: '/disciplina/get-disciplina-serie-nivel-ensino/id_disciplina/' + idDisciplina,
                dataType: 'json',
                beforeSend: function () {
                    $('#containerNivelEnsino').html('Carregando...');
                },
                success: function (data) {
                    $('#containerNivelEnsino').empty();
                    if (data) {
                        var i = 0;

                        $.each(data, function (index, row) {
                            var str_series = "";
                            if (row.series) {
                                $.each(row.series, function (j, serie) {
                                    str_series += _.template('<input type="hidden" name="id_serie" value="<%=id_serie%>"/> <%=st_serie%> |', serie);
                                });
                            }
                            arrSeries[i] = {
                                id_serie: row.id_serie,
                                id_nivelensino: row.id_nivelensino,
                                st_nivelensino: row.st_nivelensino,
                                st_series: str_series
                            };
                            i++;
                        });

                        var collectionSerie = new VwSerieNivelEnsinoCollection(arrSeries);
                        var renderer = new CollectionView({
                            collection: collectionSerie,
                            childViewConstructor: ViewDisciplinaSerieNivelEnsino,
                            childViewTagName: 'div',
                            el: $('#containerNivelEnsino')
                        });
                        renderer.render();

                    }
                }
            });
        }
    },
    addItemEscolaridade: function () {
//        var elemTemplate = _.template($("#templateEscolaridade").html());
//        $('#containerNivelEnsino').append(elemTemplate);
        var renderer = new CollectionView({
            collection: new VwSerieNivelEnsinoCollection(),
            childViewConstructor: EscolaridadeView,
            childViewTagName: 'div',
            el: this.$el.find('#containerNivelEnsino')
        });
        renderer._rendered = true;
        var model = new VwSerieNivelEnsinoModel();
        renderer.add(model);
    },
    removeItemEscolaridade: function (e) {
        that = this;
        var idDisciplina = this.model.id;
        var id_nivelensino = $(e.currentTarget).parent('.span2').parent('.item-escolaridade')
            .find('.span10').find('.span5').find('input[name="id_nivelensino"]').val();
        if (idDisciplina && id_nivelensino) {
            bootbox.confirm("Tem certeza de que deseja apagar o registro?", function (result) {
                if (result) {
                    loading();
                    $.ajax({
                        url: '/disciplina/remove-vinculo-disciplina-nivel-ensino',
                        dataType: 'json',
                        type: 'post',
                        data: {
                            id_disciplina: idDisciplina,
                            id_nivelensino: id_nivelensino
                        },
                        success: function (data) {
                            loaded();
                            $(e.currentTarget).parent('.span2').parent('.item-escolaridade').remove();
                            $.pnotify({
                                title: 'Registro Removido',
                                text: 'O registro foi removido do banco de dados com sucesso!',
                                type: 'success'
                            });
                        },
                        error: function (data) {
                            loaded();
                            $.pnotify({
                                type: 'error',
                                title: 'Atenção!',
                                text: 'Erro ao tentar remover registro.'
                            });
                        }
                    });
                }
            });
        } else {
            $(e.currentTarget).parent('.span2').parent('.item-escolaridade').remove();
        }
    },
    addItemAutores: function () {
        renderer = new CollectionView({
            collection: new VwUsuarioPerfilEntidadeReferenciaCollection,
            childViewConstructor: ViewVwUsuarioPerfilEntidadeReferencia,
            childViewTagName: 'tr',
            el: $('#tableAutores tbody'),
        });
        renderer._rendered = true;
        var model = new UsuarioPerfilEntidadeReferenciaModel();
        model.toggleEdit();
        renderer.add(model);
    },
    salvaItemEscolaridade: function (e) {
        var elemento = e.currentTarget;
        var dataSeries = [];
        //recupera o id_nivelensino
        var id_nivelensino = $(elemento).parent('.span2')
            .parent('.item-escolaridade')
            .find('select[name="id_nivelensino"]').val();
        //recupera os valores dos checkbox que foram selecionados
        $(elemento).parent('.span2')
            .parent('.item-escolaridade')
            .find('input[name="id_serie"]:checked').each(function (i, el) {
            dataSeries[i] = $(el).val();
        });
        disciplinaSerieNivelEnsinoModel = new DisciplinaSerieNivelEnsino();
        disciplinaSerieNivelEnsinoModel.set({
            id_nivelensino: id_nivelensino,
            id_serie: dataSeries,
            id_disciplina: null
        });
        disciplinaSerieNivelEnsinoModel.toggleEdit();
    },
    editItemEscolaridade: function (e) {
        return false;
    },
    carregaProfessores: function () {
        var idDisciplina = this.model.id;
        var idPerfil = $(this.el).find("#idPerfilProfessor").val();
        var collectionProfessor = new VwUsuarioPerfilEntidadeReferenciaCollection();
        if (idDisciplina) {
            collectionProfessor.url += 'id_disciplina/' + idDisciplina + '/id_perfil/' + idPerfil
            collectionProfessor.fetch({
                success: function () {
                    renderer = new CollectionView({
                        collection: collectionProfessor,
                        childViewConstructor: ViewProfessor,
                        childViewTagName: 'div',
                        el: $('#containerProfessores')
                    }).render();
                }
            });
        } else {
            this.addItemProfessor();
        }
    },
    addItemProfessor: function () {
        var collectionProfessor = new VwUsuarioPerfilEntidadeReferenciaCollection();
        renderer = new CollectionView({
            collection: collectionProfessor,
            childViewConstructor: ViewProfessor,
            childViewTagName: 'div',
            el: $('#containerProfessores')
        });
        renderer._rendered = true;
        var model = new VwUsuarioPerfilEntidadeReferenciaModel();
        model.toggleEdit();
        renderer.add(model);
    },
    validaNum: function (event) {
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || (event.keyCode == 65 && event.ctrlKey === true) || (event.keyCode >= 35 && event.keyCode <= 39)) {
            return;
        } else {
            /* Testar acionamento de outras teclas */
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
            }
        }
    },
    carregaIntegracao: function () {
        var $containerIntegracao = this.$el.find("#containerIntegracao");
        var idDisciplina = this.model.id;
        if (idDisciplina) {
            var collectionIntegracao = new VwDisciplinaIntegracaoMoodleCollection;
            collectionIntegracao.fetch({
                url: "/disciplina/get-vw-disciplina-integracao-moodle/",
                data: {
                    "id_disciplina": idDisciplina
                },
                beforeSend: function () {
                    $containerIntegracao.empty().append('Carregando...');
                    loading()
                }.bind(this),
                complete: loaded,
                success: function () {
                    $containerIntegracao.empty();
                    (new CollectionView({
                        collection: collectionIntegracao,
                        childViewConstructor: ViewDisciplinaIntegracaoMoodle,
                        childViewTagName: 'div',
                        el: this.$el.find("#containerIntegracao")
                    })).render();
                }.bind(this)
            });
        } else {
            $containerIntegracao.empty()
                .html('Cadastre a disciplina na aba anterior antes de prosseguir com a integração.');
        }
    },
    addItemIntegracao: function () {
        var renderer = new CollectionView({
            collection: new VwDisciplinaIntegracaoMoodleCollection,
            childViewConstructor: IntegracaoView,
            childViewTagName: 'div',
            el: this.$el.find("#containerIntegracao")
        });
        renderer._rendered = true;
        var model = new VwDisciplinaIntegracaoMoodleModel();
        renderer.add(model);
    },
    removeItemIntegracao: function (e) {
        var that = this;
        var idDisciplina = this.model.id;
        var id_disciplinaintegracao = e.currentTarget.id;
        if (idDisciplina && id_disciplinaintegracao) {
            bootbox.confirm("Tem certeza de que deseja apagar o registro?", function (result) {
                if (result) {
                    loading();
                    $.ajax({
                        url: '/disciplina/remove-vinculo-disciplina-integracao',
                        dataType: 'json',
                        type: 'post',
                        data: {
                            id_disciplina: idDisciplina,
                            id_disciplinaintegracao: id_disciplinaintegracao
                        },
                        success: function (data) {
                            loaded();
                            $(e.currentTarget).parent('.span2').parent('.item-integracao').remove();
                            $.pnotify({
                                title: 'Registro Removido',
                                text: 'O registro foi removido do banco de dados com sucesso!',
                                type: 'success'
                            });
                        },
                        error: function (data) {
                            loaded();
                            $.pnotify({
                                type: 'error',
                                title: 'Atenção!',
                                text: 'Erro ao tentar remover registro.'
                            });
                        }
                    });
                }
            });
        } else {
            $(e.currentTarget).parent('.span2').parent('.item-integracao').remove();
        }
    },
    salvaIntegracao: function () {
        var that = this;
        var erro = 0;

        loading();

        $.ajaxSetup({async: false});

        $.each(this.$el.find("#containerIntegracao").find("div.integracao-new"), function (index, child) {
            this.disciplinaIntegracaoModel = new DisciplinaIntegracaoModel();

            var st_codsistema = $(child).find('[name="st_codsistema"]').val();
            var id_sistema = $(child).find('[name="id_sistema"]').val();
            var id_entidade = $(child).find('[name="id_entidade"]').val();
            var st_salareferencia = $(child).find('[name="st_salareferencia"]').val();
            var id_entidadeintegracao = $(child).find("select[name='id_moodle']").val();

            if (st_codsistema && id_sistema && id_entidade && st_salareferencia) {
                this.disciplinaIntegracaoModel.save({
                    id_disciplina: this.model.id,
                    st_codsistema: st_codsistema,
                    bl_ativo: true,
                    id_sistema: id_sistema,
                    id_entidade: id_entidade,
                    st_salareferencia: st_salareferencia,
                    id_entidadeintegracao: id_entidadeintegracao
                }, {
                    success: function (model, response) {
                        $.pnotify({
                            title: 'Registro Salvo',
                            text: 'Integrações cadastradas com sucesso!',
                            type: 'success'
                        });
                    },
                    error: function (model, response) {
                        $.pnotify({
                            title: 'Erro ao salvar o registro',
                            text: 'Houve um problema ao salvar as integrações, tente novamente',
                            type: 'error'
                        });
                        erro++;
                    }
                });
            }
        }.bind(this));
        if (erro == 0) {
            that.carregaIntegracao();
        }
        loaded();
        $.ajaxSetup({async: true});
    },
    carregaAbaConteudo: function () {

        var model = this.model;

        ConteudoCompositeView = new ConteudoCompositeViewExtend({
            el: '#container-conteudo',
            collection: new DisciplinaConteudoCollection(),
            disciplinaModel: model
        });
        ConteudoCompositeView.render();

    },
    events: {
        'click #fieldSetEscolaridade a.add-item': 'addItemEscolaridade',
        'click #containerNivelEnsino a.edit-link': 'editItemEscolaridade',
        'click .item-escolaridade a.delete-item': 'removeItemEscolaridade',
        'click .item-escolaridade a.salva-item': 'salvaItemEscolaridade',
        'click #fieldsetAutores .add-item': 'addItemAutores',
        'click #fieldSetProfessores .add-item': 'addItemProfessor',
        'submit form#formCadastro': 'salvaDisciplina',
        'submit form#formConteudos': 'salvaConteudos',
        'keydown .numeric': 'validaNum',
        'click #link-tab2': 'carregaIntegracao',
        'click #link-tab4': 'carregaAbaConteudo',
        'click #fieldSetIntegracao a.add-item': 'addItemIntegracao',
        'click .item-integracao a.delete-item': 'removeItemIntegracao',
        'click #btnSalvarIntegracao': 'salvaIntegracao',
        'keyup #st_tituloexibicao,#st_disciplina': 'verificaTituloIgual',
        'blur #st_tituloexibicao,#st_disciplina': 'verificaTituloIgual'
    },
    verificaTituloIgual: function (e) {
        var st_disciplina = $('#st_disciplina').val().trim();
        var st_tituloexibicao = $('#st_tituloexibicao').val().trim();

        if (st_tituloexibicao && st_tituloexibicao == st_disciplina) {
            $('.titulo-alert').removeClass('hide').closest('control-group').addClass('error');

            if (e.type == 'blur' || e.type == 'focusout') {
                $('#st_tituloexibicao').val('');
            }
        } else {
            $('.titulo-alert').addClass('hide').closest('control-group').removeClass('error');
        }
    }
});

/**
 * Testa se o valor é um número através da expressão regular
 * @param {mixed} valor
 * @returns {Boolean}
 */
function validaNumero(valor) {
    var er = /^[0-9]+$/;
    return er.test(valor);
}

var viewFormRendered;
var MainView = Marionette.ItemView.extend({
    template: "#main-view-template",
    onRender: function () {
        var that = this;
        viewFormRendered = new ViewForm({
            model: new DisciplinaModel(),
            el: that.$el.find("#formCadastroDisciplinas")
        });
        viewFormRendered.render();
    },
    onShow: function () {
        loadTinyMCE();
    }
});
G2S.layout.content.show(new MainView());
$(function () {
    loaded();
    $('*[data-toggle]').tooltip();
    $(".numeric").numeric(",");
});
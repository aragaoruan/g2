/**
 * ARQUIVO JS PARA FOLHA DE NUMEROS DE APOIO PARA TURMAS
 */
var viewRelatorio;
var totalMatriculas = 0;
var totalFreepass = 0;
var totalGarantiaduo = 0;

/**
 * MODELS
 */

var ModelRelatorio = Backbone.Model.extend({
    id_entidade: '',
    st_entidade: '',
    categoria: []
});

var ModelCategoria = Backbone.Model.extend({
    id_categoria: '',
    st_categoria: '',
    turma: [],
    'totalMatriculas': totalMatriculas,
    'totalFreepass': totalFreepass,
    'totalGarantiaduo': totalGarantiaduo
});

var ModelTurma = Backbone.Model.extend({
    dt_inicio: '',
    id_entidade: '',
    id_evolucao: '',
    id_turma: '',
    st_cargahoraria: '',
    st_evolucao: '',
    st_entidade: '',
    st_produto: '',
    st_turma: '',
    nu_matriculas: '',
    nu_garantiaduo: '',
    nu_freepass: '',
    st_turno: ''
});

/**
 * COLLECTIONS
 */

var CategoriaCollection = Backbone.Collection.extend({
    url: '/folha-de-numeros-apoio/retorna-categorias',
    model: Categoria
});

var EvolucaoCollection = Backbone.Collection.extend({
    url: '/api/evolucao/?st_tabela=tb_turma',
    model: Evolucao
});

var EntidadeCollection = Backbone.Collection.extend({
    url: '/folha-de-numeros-apoio/retorna-entidade',
    model: VwEntidadeRecursiva
});

/**
 * LAYOUT E EVENTOS
 */

var FolhaLayout = Marionette.LayoutView.extend({
    template: '#folha_layout',
    tagname: 'div',
    className: 'containder-fluid',
    regions: {},
    ui: {
        'idCategoria': '#id_categoria',
        'idEvolucao': '#id_evolucao',
        'idEntidade': '#id_entidade'
    },
    events: {
        'click #btn_gerar_relatorio': 'gerarRelatorio'
    },
    initialize: function () {
        thisLayout = this;
    },
    onShow: function () {
        this.renderSelectCategoria();
        this.renderSelectEvolucao();
        this.renderSelectEntidade();
        loaded();
    },
    renderSelectCategoria: function () {
        var that = this;
        var collection = new CategoriaCollection();
        collection.fetch({
            success: function () {
                var view = new SelectView({
                    el: that.ui.idCategoria,        // Elemento da DOM
                    collection: collection,      // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_categoria', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_categoria', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: null       // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();
            }


        });
    },
    renderSelectEvolucao: function () {
        var that = this;
        var collection = new EvolucaoCollection();
        collection.fetch({
            success: function () {
                var view = new SelectView({
                    el: that.ui.idEvolucao,        // Elemento da DOM
                    collection: collection,      // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_evolucao', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_evolucao', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: 46       // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();
            }

        });
    },
    renderSelectEntidade: function () {
        var that = this;
        var collectionEntidade = new EntidadeCollection();
        collectionEntidade.fetch({
            success: function () {
                var view = new SelectView({
                    el: that.ui.idEntidade,        // Elemento da DOM
                    collection: collectionEntidade,      // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_nomeentidade', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_entidade', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: null       // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();
            }
        });

    },
    gerarRelatorio: function () {
        var that = this;
        loading();
        var collectionResultRelatorio = Backbone.Collection.extend({
            model: ModelRelatorio
        });

        var collectionFetch = new collectionResultRelatorio();
        collectionFetch.url = '/folha-de-numeros-apoio/gerar-relatorio',
            collectionFetch.fetch({
                data: {
                    'id_entidade': that.ui.idEntidade.val(),
                    'id_categoria': that.ui.idCategoria.val(),
                    'id_evolucao': that.ui.idEvolucao.val()
                },
                type: 'POST',
                success: function () {

                    //debug(collectionFetch);return false;
                    var CollectionCategorias = Backbone.Collection.extend({
                        model: ModelCategoria
                    });
                    var CollectionTurmas = Backbone.Collection.extend({
                        model: ModelTurma
                    });

                    var NoDadoView = Backbone.Marionette.ItemView.extend({
                        tagName: 'div',
                        template: "#no-dado"
                    });

                    var turmas = Marionette.CompositeView.extend({
                        tagName: 'tr',
                        template: ('#item-relatorio')
                    });

                    var categorias = Marionette.CompositeView.extend({
                        childView: turmas,
                        template: ('#script_tabela_relatorio'),
                        childViewContainer: 'tbody',
                        emptyView: NoDadoView,
                        initialize: function () {

                            totalMatriculas = 0;
                            totalFreepass = 0;
                            totalGarantiaduo = 0;
                            _.each(this.model.get('turma'), function (obj, index) {
                                totalMatriculas = totalMatriculas + obj.nu_matriculas;
                                totalGarantiaduo = totalGarantiaduo + obj.nu_garantiaduo;
                                totalFreepass = totalFreepass + obj.nu_freepass;
                            });

                            var collection = new CollectionTurmas(this.model.get('turma'));
                            this.collection = collection;
                        }
                    });

                    var unidade = Marionette.CompositeView.extend({
                        tagName: 'fieldset',
                        childView: categorias,
                        template: _.template('<legend><%=st_entidade%></legend><div class="categorias"></div>'),
                        childViewContainer: '.categorias',
                        initialize: function () {
                            var collection = new CollectionCategorias();
                            $.each(this.model.get('categoria'), function (index, obj) {
                                collection.add(obj);
                            });
                            this.collection = collection;
                        },
                        onRender: function () {
                            return this;
                        }

                    });


                    var entidadeView = Marionette.CompositeView.extend({
                        childView: unidade,
                        template: _.template('<div class="corpo well well-large"></div>'),
                        childViewContainer: '.corpo',
                        emptyView: NoDadoView,
                        initialize: function () {
                            //console.log('entidades: ', this.collection);
                        }
                    });

                    viewRelatorio = new entidadeView({
                        el: that.$el.find('#container-table'),
                        collection: collectionFetch
                    });

                    $('#container-table').show();
                    viewRelatorio.render();
                    loaded()
                }
            })
    }
});

var layoutFolha = new FolhaLayout();
G2S.show(layoutFolha);
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * *                             VARÍAVEIS                             * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */

var that;
var dataRequest = {};
var professor = $('#id_professordisciplina');


/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * *                        COLLECTIONS                                * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
var TipoDisciplinaCollection = Backbone.Collection.extend({
    model: TipoDisciplina
});

var CategoriaSalaCollection = Backbone.Collection.extend({
    model: CategoriaSala
});

var ProfessorCollection = Backbone.Collection.extend({
    model: VwProfessorDisciplina
});

var ConsultaPagamentoCollection = Backbone.PageableCollection.extend({
    url: '/consulta-pagamento/retorna-consulta-pagamento',
    model: VwConsultaPagamento,
    state: {
        pageSize: 10
    },
    mode: "client"
});

var collection = new ConsultaPagamentoCollection();

/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * *                             VIEWS                                 * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
var FormPesquisaView = Backbone.View.extend({
    el: '#form-pesquisa-consulta',
    collection: collection,
    initialize: function () {
        this.render();
    },
    render: function () {
        that = this;
        this.popularTipoDisciplina();
        this.popularCategoriaSala();
        this.popularProfessor();
        loaded();
    },
    events: {
        "change #id_tipodisciplina": "changeTipoDisciplina",
        "click .exportarXls": "exportarXls",
        "click .pesquisaPagamento" : "renderConsultaView"
    },
    consultaView: null,
    changeTipoDisciplina: function(e){
        this.popularProfessor();
    },
    popularTipoDisciplina: function () {
        var tipoCollection = new TipoDisciplinaCollection();
        tipoCollection.url = '/api/tipo-disciplina';
        tipoCollection.fetch({
                success: function () {
                    var comboView = new CollectionView({
                        collection: tipoCollection,
                        childViewTagName: 'option',
                        childViewConstructor: OptionTipoDisciplinaView,
                        el: $('#id_tipodisciplina')
                    });
                    comboView.render();
                    $(comboView.el).prepend("<option value='' selected>Selecione</option>");
                }}
        );
    },
    popularProfessor: function () {
        var tipoCollection = new ProfessorCollection();
        tipoCollection.url = '/default/consulta-pagamento/retorna-professor-coordenador';
        tipoCollection.fetch({
                data: $.param({
                    tipo_disciplina: $('#id_tipodisciplina :selected ').val()
                }),
                success: function () {
                    var comboView = new CollectionView({
                        collection: tipoCollection,
                        childViewTagName: 'option',
                        childViewConstructor: OptionProfessorDisciplinaView,
                        el: $('#id_professordisciplina')
                    });
                    comboView.render();
                    $(comboView.el).prepend("<option value='' selected>Todos</option>");
                }}
        );
    },
    popularCategoriaSala: function () {
        var categoriaSalaCollection = new CategoriaSalaCollection();
        categoriaSalaCollection.url = '/api/categoria-sala';
        categoriaSalaCollection.fetch({success: function () {
                var comboView = new CollectionView({
                    collection: categoriaSalaCollection,
                    childViewTagName: 'option',
                    childViewConstructor: OptionCategoriaSalaView,
                    el: $('#id_categoriasala')
                });
                comboView.render();
            }}
        );
    },
    renderEncerramentoViewProfessor: function () {
        if (this.consultaView) {
            this.consultaView.destroyMe();
        }

        dataRequest.id_professor = professor.val();

        this.encerramentoView = new EncerramentoView();
    },
    renderConsultaView: function () {
        if (this.consultaView) {
            this.consultaView.destroyMe();
        }
        if ($('#id_tipodisciplina').val() == '') {
            $.pnotify({title: 'Erro', type: 'error', text: 'Escolha o tipo de disciplina antes de prosseguir!'});
            return false;
        }
        if(professor.val() != ''){
            dataRequest.id_professor = professor.val();
        }
        this.consultaView = new ConsultaView();
    },
    renderLiberadoView: function (e) {
        this.consultaView = new ConsultaView();
        if ($(e.currentTarget).val() == 'financeiro') {

        }
    },

    exportarXls: function (e) {
        var dados = [];
        var tipo_disciplina = $('#id_tipodisciplina').val();
        var cabecalho = {};

        _(this.collection.fullCollection.models).each(function (model, d) {
            dados.push(model.toJSON());
        });

        if (tipo_disciplina == 2) {
            cabecalho = {
                id_encerramentosala: 'ID',
                st_saladeaula: 'Sala de Aula',
                nu_cargahoraria: 'C.H.',
                st_professor: 'Professor',
                st_projetopedagogico: 'Curso',
                st_tituloavaliacao: 'Título da Monografia',
                st_aluno: 'Aluno',
                dt_encerramentoprofessor: 'Coordenador',
                dt_encerramentocoordenador: 'Pedagógico',
                dt_encerramentopedagogico: 'Financeiro',
                nu_valorpago: 'Pagamento'

            };
        } else {
            cabecalho = {
                id_encerramentosala: 'ID',
                st_saladeaula: 'Sala de Aula',
                st_disciplina: 'Disciplina',
                nu_cargahoraria: 'C.H.',
                st_professor: 'Professor',
                nu_alunos: 'Alunos',
                dt_encerramentoprofessor: 'Coordenador',
                dt_encerramentocoordenador: 'Pedagógico',
                dt_encerramentopedagogico: 'Financeiro',
                nu_valorpago: 'Pagamento'
            };
        }

        $.ajax({
            url: "/relatorio/gerar-xls",
            dataType: 'html',
            type: 'post',
            data: {
                'dados': JSON.stringify(dados),
                'cabecalho': JSON.stringify(cabecalho),
                'converterUtf8': true
            },
            success: function (resp) {
                var win = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(resp), "", "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=780, height=200, top=0, left=0");
                setTimeout(function () {
                    win.destroy();
                }, 4000)
            },
            complete: function (resp) {

            }
        })
    }
});

var ConsultaView = Backbone.View.extend({
    initialize: function () {
        this.render();
    },
    render: function () {
        this.renderGrid();
        return this;
    },
    destroyMe: function () {
        this.undelegateEvents();
        $(this.el).removeData().unbind();
        this.el.remove();
        Backbone.View.prototype.remove.call(this);
    },
    renderGrid: function () {
        var containerGrid = $('#container-grid');
        var colecao = that.collection;
        var tipo_liberacao = $('#liberacao').val();
        var tipo_disciplina = $('#id_tipodisciplina').val();
        var categoria_sala = $('#id_categoriasala').val();

        var cellSalaDeAula = Backgrid.Cell.extend({
            events: {
                "click .st_saladeaula": "abrirDetalhamento"
            },
            render: function () {
                this.$el.html(_.template('<a href="javascript:void(0)" class="st_saladeaula">'+this.model.get('st_saladeaula')+'</a>'));
                this.delegateEvents();
                return this;
            },
            abrirDetalhamento: function () {
                $.ajax({
                    url: '/vw-encerramento-salas/retornar-dados-detalhamento-sala',
                    type: 'GET',
                    data: {
                        id_encerramentosala: this.model.get('id_saladeaula')
                    },
                    dataType: 'HTML',
                    success: function (response) {
                        var modal_detalhamento = $('#modal-detalhamento');
                        modal_detalhamento.html(response);
                        modal_detalhamento.modal('show');
                    },
                    error: function (response) {

                    },
                    complete: function () {
                        loaded();
                    },
                    beforeSend: function () {
                        loading();
                    }
                })
            }
        });

        var cellTcc = Backgrid.Cell.extend({
            events: {
                "click .interacoesTCC": "abrirRelatorio"
            },
            render: function () {
                var interacao, versao_final;

                interacao = '<a class="btn interacoesTCC" title="Visualizar interações" href="javascript:void(0)"><i class="icon-tags"></i></a>';

                if (this.model.get('st_upload') != '') {
                    versao_final = '<a class="btn" target="_blank" title="Versão final do tcc" href="/upload/avaliacao/' + this.model.get('st_upload') + '"><i class="icon-file"></i></a>';
                } else {
                    versao_final = '';
                }

                this.$el.html(_.template("<div class='btn-toolbar'><div class='btn-group'>" + interacao + versao_final + "</div></div>"));
                this.delegateEvents();
                return this;
            },
            abrirRelatorio: function () {
                var that = this;
                $.ajax({
                    dataType: 'json',
                    type: 'post',
                    url: '/vw-encerramento-salas/retornar-link-interacoes',
                    data: { to: 'VwEncerramentoCoordenadorTO'
                        ,obj: that.model.toJSON()},
                    beforeSend: function () {
                        loading();
                    },
                    success: function (dataResponse) {
                        if(dataResponse.tipo == 0){
                            $.pnotify({title: dataResponse.retorno, text: dataResponse.mensagem, type: dataResponse.type});
                        }else{
                            var win = window.open(dataResponse.mensagem, '_blank');
                            if(win){
                                win.focus();
                            }else{
                                $.pnotify({title: 'Aviso', text: 'Desbloqueie o pop-up do navegador!', type: 'warning'});
                                loaded();
                            }
                        }
                    },
                    complete: function () {
                        loaded();
                    }
                });
            }
        });

        //coluna do relatorio do moodle
        var cellRelatorio = Backgrid.Cell.extend({
            events: {
                "click .relatorio": "abrirRelatorioMoodle"
            },
            render: function () {
                this.$el.html(_.template("<div class='btn-toolbar'><div class='btn-group'> <a class='btn  btn-success relatorio' title='Relatório' href='javascript:void(0)'>" +
                "<i class='icon icon-white icon-list-alt'></i></a></div></div>"));
                this.delegateEvents();
                return this;
            },
            abrirRelatorioMoodle: function () {
                var that = this;
                $.ajax({
                    dataType: 'json',
                    type: 'post',
                    url: '/vw-encerramento-salas/retornar-link-relatorio',
                    data: { to: 'VwEncerramentoCoordenadorTO'
                        ,obj: that.model.toJSON()},
                    beforeSend: function () {
                        loading();
                    },
                    success: function (dataResponse) {
                        if(dataResponse.tipo == 0){
                            $.pnotify({title: dataResponse.retorno, text: dataResponse.mensagem, type: dataResponse.type});
                        }else{
                            var win = window.open(dataResponse.mensagem, '_blank');
                            if(win){
                                win.focus();
                            }else{
                                $.pnotify({title: 'Aviso', text: 'Desbloqueie o pop-up do navegador!', type: 'warning'});
                                loaded();
                            }
                        }
                    },
                    complete: function () {
                        loaded();
                    }
                });
            }
        });

        //fim coluna relatorio moodle

        var columns = null;
            columns = [
                {
                    name: "id_encerramentosala",
                    label: "ID",
                    editable: false,
                    cell: "string"
                },
                {
                    name: "st_saladeaula",
                    label: "Sala de Aula",
                    editable: false,
                    cell: cellSalaDeAula
                },
                {
                    name: "nu_cargahoraria",
                    label: "Carga Horária",
                    editable: false,
                    cell: "string"
                },
                {
                    name: "st_disciplina",
                    label: "Disciplina",
                    editable: false,
                    cell: "string"
                },
                {
                    name: "st_professor",
                    label: "Professor",
                    editable: false,
                    cell: "string"
                },
                {
                    name: "relatorio",
                    label: "Relatório",
                    editable: false,
                    cell: cellRelatorio
                },
                {
                    name: "nu_alunos",
                    label: "Alunos",
                    editable: false,
                    cell: "string"
                },
                {
                    name: "st_encerramentoprofessor",
                    label: "Coordenador",
                    editable: false,
                    cell: 'string'
                },
                {
                    name: "st_encerramentocoordenador",
                    label: "Pedagógico",
                    editable: false,
                    cell: 'string'
                },
                {
                    name: "st_encerramentopedagogico",
                    label: "Financeiro",
                    editable: false,
                    cell: "string"
                },
                {
                    name: "nu_valorpago",
                    label: "Pagamento",
                    editable: false,
                    cell: Backgrid.Cell.extend({
                        render: function () {
                            var texto = null;
                            if (this.model.get('nu_valorpago').toString() !== '' ) {
                                texto = 'R$ '+ this.model.get('nu_valorpago') +',00';
                            } else {
                                texto = 'Sem informação de data';
                            }
                            this.$el.html(texto);
                            return this;
                        }
                    })
                }
            ];


        var acao = $('.acao');
        var acaoFinal = $('.acao.final');
        if (tipo_disciplina == 2) {
            var new_columns = [];

            for (var i = 0; i < columns.length; i++) {
                if (columns[i].name == 'nu_alunos') {
                    this.removeColumns(columns, i);
                }

                if (columns[i].name == 'nu_cargahoraria') {
                    this.removeColumns(columns, i);
                }

                if (columns[i].name == 'st_disciplina') {
                    this.removeColumns(columns, i);
                }

                if (columns[i].name == 'relatorio') {
                    this.removeColumns(columns, i);
                }

                if (columns[i].name == 'st_professor') {
                    new_columns.push(columns[i]);
                    new_columns.push(
                        {
                            name: "st_projetopedagogico",
                            label: "Curso",
                            editable: false,
                            cell: "string"
                        },
                        {
                            name: "st_tituloavaliacao",
                            label: "Título da Monografia",
                            editable: false,
                            cell: "string"
                        },
                        {
                            name: '',
                            label: "TCC",
                            editable: false,
                            cell: cellTcc
                        },
                        {
                            name: "st_aluno",
                            label: "Aluno",
                            editable: false,
                            cell: "string"
                        }
                    )
                } else {
                    new_columns.push(columns[i]);
                }
            }
            columns = new_columns;
        }

        dataRequest.id_tipodisciplina = tipo_disciplina;
        dataRequest.id_categoriasala = categoria_sala;
        dataRequest.tipo_liberacao = tipo_liberacao;
        dataRequest.id_professor = professor.val();

        var grid = new Backgrid.Grid({
            className: 'backgrid table table-bordered table-hover encerramento',
            columns: columns,
            collection: colecao
        });

        var paginacaoPrincipal = new Backgrid.Extension.Paginator({
            collection: colecao
        });

        colecao.fetch({
            data: dataRequest,
            complete: function (collection, response) {
                containerGrid.empty().append(grid.render().el).append(paginacaoPrincipal.render().el);
                loaded();
            },
            beforeSend: function () {
                loading();
            }
        });
    },
    removeColumns: function (columns, index) {
        columns.splice(index, 1);
    }
});

var OptionProfessorDisciplinaView = Backbone.View.extend({
    template: _.template('<%=id_usuario%>'),
    render: function () {
        this.$el.attr('value', this.model.id_usuario ? this.model.id_usuario : this.model.get('id_usuario'));
        this.$el.text(this.model.get('st_nomecompleto'));
        return this;
    }
});

var OptionTipoDisciplinaView = Backbone.View.extend({
    template: _.template('<%=id_tipodisciplina%>'),
    render: function () {
        this.$el.attr('value', this.model.id_tipodisciplina ? this.model.id_tipodisciplina : this.model.get('id_tipodisciplina'));
        this.$el.text(this.model.get('st_tipodisciplina'));
        return this;
    }
});

var OptionCategoriaSalaView = Backbone.View.extend({
    template: _.template('<%=id_categoriasala%>'),
    render: function () {
        this.$el.attr('value', this.model.id_categoriasala ? this.model.id_categoriasala : this.model.get('id_categoriasala'));
        this.$el.text(this.model.get('st_categoriasala'));
        return this;
    }
});

/**
 *******************************************************************************
 *******************************************************************************
 **                Outras funcionalidades
 *******************************************************************************
 *******************************************************************************
 */

var formPesquisaView = new FormPesquisaView();

$.ajaxSetup({async: true});

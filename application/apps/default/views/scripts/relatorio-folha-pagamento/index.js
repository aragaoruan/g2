/******************************************************************************
 *              Relatorio Folha de Pagamento
 *              Neemias Santos  <neemias.santos@unyleya.com.br>
 *              2015-12-17
 * ****************************************************************************
 */


/**
 * Model criada para select de professores
 * @type Model @exp;Backbone@pro;Model@call;extend
 */
var ProfessorModel = Backbone.Model.extend({});

/**
 * @type Collection para Entidade @exp;Backbone@pro;Collection@call;extend
 */
var EntidadeCollection = Backbone.Collection.extend({
    url: '/entidade/retornar-entidade-recursiva-id',
    model: VwEntidadeRecursivaId
});

/**
 * @type Collection para Area Conhecimento @exp;Backbone@pro;Collection@call;extend
 */
var AreaConhecimentoCollection = Backbone.Collection.extend({
    model: AreaConhecimento,
    url: "/api/area-conhecimento"
});

/**
 * @type Collection para Select de Professores @exp;Backbone@pro;Collection@call;extend
 */
var ProfessorCollection = Backbone.Collection.extend({
    model: ProfessorModel
});
/**
 ******************************************************************************************
 ******************************************************************************************
 *************************** LAYOUT FUNCIONALIDADE ****************************************
 ******************************************************************************************
 ******************************************************************************************
 */

var TelaGradeLayout = Marionette.LayoutView.extend({
    template: '#template-folha-pagamento',
    tagName: 'div',
    regions: {
        tabelaGrade: "#regiao-grade-data",
        formgrade: '#regiao-grade'
    },
    onShow: function () {
        this.renderizaFormGrade();
        return this;
    },
    renderizaFormGrade: function () {
        var gradeView = new FormGradeCompositeView({});
        this.formgrade.show(gradeView);
    }
});

/**
 ******************************************************************************************
 ******************************************************************************************
 *********************************** FORM DA GRADE ****************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var FormGradeCompositeView = Marionette.CompositeView.extend({
    template: '#template-form-folha-pagamento',
    tagName: 'fieldset',
    ui: {
        'id_unidade': 'select[name="id_unidade"]',
        'id_areaconhecimento': 'select[name="id_areaconhecimento"]',
        'id_professor': 'select[name="id_professor"]',
        'dt_mes_ano': 'input[name="dt_mes_ano"]',
        'btnPesquisar': '#btnPesquisar',
        'btnGeralExcel': '#btn_gerar_relatorio'
    },
    events: {
        'click #btnPesquisar': 'pesquisarDados',
        'click #btnGeralExcel': 'btnGeralExcel'
    },
    onShow: function () {
        this.populaSelectUnidade();
        this.populaSelectAreaConhecimento();
        this.populaSelectProfessor();
    },
    populaSelectUnidade: function () {
        var that = this;


        collectionEntidade = new EntidadeCollection();
        collectionEntidade.fetch({
            async: false,
            beforeSend: function () {
                loading();
            },
            success: function () {
                var view = new SelectView({
                    el: that.$el.find("select[name='id_unidade']"),        // Elemento da DOM
                    collection: collectionEntidade,      // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_nomeentidade', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_entidade', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: null       // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();
            },
            complete: function () {
                that.ui.id_unidade.prepend('<option value="0" >Todas</option>');
                that.ui.id_unidade.prepend('<option value="" selected="selected">Escolha uma opção</option>');
                loaded();
            }
        });
    },
    populaSelectAreaConhecimento: function () {
        var that = this;

        collectionAreaConhecimento = new AreaConhecimentoCollection();
        collectionAreaConhecimento.fetch({
            async: false,
            beforeSend: function () {
                loading();
            },
            success: function () {
                var view = new SelectView({
                    el: that.ui.id_areaconhecimento,        // Elemento da DOM
                    collection: collectionAreaConhecimento,      // Instancia da collection a ser utilizada
                    childViewOptions: {
                        optionLabel: 'st_areaconhecimento', // Propriedade da Model que será utilizada como label do select
                        optionValue: 'id_areaconhecimento', // Propriedade da Model que será utilizada como id do option do select
                        optionSelected: null       // ID da option que receberá o atributo "selected"
                    }
                });
                view.render();
                that.ui.id_areaconhecimento.select2();
            },
            complete: function () {
                var newOption = $('<option value="0" selected>Todas</option>');
                newOption.insertAfter('#id_areaconhecimento option[value=""]');
                loaded();
            }
        });
    },
    populaSelectProfessor: function () {
        var that = this;

        loading();
        var collectionProfessor = new ProfessorCollection();
        if (typeof (Professores) != 'undefined') {
            collectionProfessor = new ProfessorCollection(Professores);
        }
        var view = new SelectView({
            el: that.ui.id_professor,        // Elemento da DOM
            collection: collectionProfessor,      // Instancia da collection a ser utilizada
            childViewOptions: {
                optionLabel: 'st_nomecompleto', // Propriedade da Model que será utilizada como label do select
                optionValue: 'id_usuario', // Propriedade da Model que será utilizada como id do option do select
                optionSelected: null       // ID da option que receberá o atributo "selected"
            }
        });
        view.render();
        var newOption = $('<option value="0" selected>Todas</option>');
        newOption.insertAfter('#id_professor option[value=""]');
        that.ui.id_professor.select2();
        loaded();


    },
    pesquisarDados: function () {
        var that = this;
        if (!that.ui.id_unidade.val()) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Selecione a Entidade.'
            });
            return false;
        }
        if (!that.ui.id_areaconhecimento.val()) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Selecione a area de conhecimento.'
            });
            return false;
        }

        if (!that.ui.id_professor.val()) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Selecione o os professores.'
            });
            return false;
        }

        this.$el.find('form').attr('target', '_blank').attr('action', 'relatorio-folha-pagamento/pesquisa-dados').submit();
        this.$el.find('form').removeAttr('target');
    },
    btnGeralExcel: function () {
        var that = this;
        if (!that.ui.id_unidade.val()) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Selecione a Entidade.'
            });
            return false;
        }
        if (!that.ui.id_areaconhecimento.val()) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Selecione a area de conhecimento.'
            });
            return false;
        }

        if (!that.ui.id_professor.val()) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'Selecione o os professores.'
            });
            return false;
        }

        loading();
        this.$el.find('form').attr('target', '_blank').attr('action', 'relatorio-folha-pagamento/gerar-xls').submit();
        this.$el.find('form').removeAttr('target');
        loaded();
    }
});


/**
 ******************************************************************************************
 ******************************************************************************************
 ******************************** SHOW LAYOUT *********************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var telaGrade = new TelaGradeLayout();
G2S.show(telaGrade);
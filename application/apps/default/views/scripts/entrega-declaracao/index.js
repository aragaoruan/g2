/******************************************************************************
 *              Funcionalidade Entrega de declaracao
 *              Neemias Santos  <neemias.santos@unyleya.com.br>
 *              14/01/2015
 * ****************************************************************************
 */

var VwMatriculaCollectionExtend = Backbone.Collection.extend({
    url: '/api/vw-matricula/',
    model: VwMatricula
});
var VwEntregaDeclaracaoColletionExtend = Backbone.Collection.extend({
    url: '/api/vw-entrega-declaracao/',
    model: VwEntregaDeclaracao
});

var VwTextosSistemaCollectionExtend = Backbone.Collection.extend({
    url: "/api/vw-texto-sistema",
    model: VwTextoSistema,
    parse: function (response) {
        jQuery.each(response.mensagem, function (i, val) {
            //Formata dt_inicio e dt_fim como strings.

            if(val.dt_inicio == null) {
                val.dt_inicio = '';
            } else {
                var dataHora = val.dt_inicio['date'].split(' ');
                var data = dataHora[0].split('-');
                val.dt_inicio = data[2] + '/' + data[1] + '/' + data[0];
            }

            if(val.dt_fim == null) {
                val.dt_fim = '';
            } else {
                var dataHora = val.dt_fim['date'].split(' ');
                var data = dataHora[0].split('-');
                val.dt_fim = data[2] + '/' + data[1] + '/' + data[0];
            }
        });

        return response.mensagem;
    }
});

var categoriatexto = new VwTextoSistema({
    id_categoriatexto: 3
});

var itemDeclaracao = [];
var idSolicitacaoEntregue = 80;
var viewGlobal;
var idMatricula;
/**
 ******************************************************************************************
 ******************************************************************************************
 *************************** ITEM DECLARAÇÃO ITEM *****************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var ItemDeclaracaoItemView = Marionette.ItemView.extend({
    template: '#template-grid-tbody',
    tagName: 'tr',
    ui: {
        btnFinalizaEntrega: '#btn-finaliza-entrega'
    },
    confirmaEntrega: function () {
        var id_entregadeclaracao = this.model.get('id_entregadeclaracao');
        bootbox.confirm("Deseja realmente marcar esta declaração como Entregue?", function (result) {
            if (result) {
                $.post('/default/entrega-declaracao/salvar-entrega-declaracao', {
                    'id_entregadeclaracao': id_entregadeclaracao,
                    'id_situacao': idSolicitacaoEntregue
                }, function (resp) {
                    $.pnotify({title: 'Alerta', text: MensagensG2.MSG_SAVE_SUCCESS, type: 'success'});
                    viewGlobal.renderizaTabela();
                    loaded();
                });
            }
        });
    },
    onShow: function () {
    },
    events: {
        'click .item-autocomplete': function () {
            var pessoa = componenteAutoComplete.getFixModel();
        },
        'click #btn-finaliza-entrega': 'confirmaEntrega'
    }
});

/**
 ******************************************************************************************
 ******************************************************************************************
 *************************** ADD DECLARAÇÃO ITEM *****************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var ListaDeclaracaoItemView = Marionette.ItemView.extend({
    template: '#template-grid-add-declaracao',
    tagName: 'tr',
    events: {
        'click .radio-lista-declaracao ': 'selecionaDeclaracao'
    },
    selecionaDeclaracao: function () {

        var id_textosistema = this.model.get('id_textosistema');
        var id_situacao = this.model.get('id_situacao');

        itemDeclaracao['id_textosistema'] = id_textosistema;
        itemDeclaracao['id_situacao'] = id_situacao;
    }
});

/**
 ******************************************************************************************
 ******************************************************************************************
 *************************** TABELA DECLARAÇÃO COMPOSITE **********************************
 ******************************************************************************************
 ******************************************************************************************
 */
var TabelaDeclaracaoCompositeView = Marionette.CompositeView.extend({
    template: '#template-table-documento',
    className: 'row-fluid',
    childView: ItemDeclaracaoItemView,
    childViewContainer: '#body-table-grid',

    onShow: function () {
    }

});

/**
 ******************************************************************************************
 ******************************************************************************************
 *************************** MODAL ADICIONA DECLARACAO  ***********************************
 ******************************************************************************************
 ******************************************************************************************
 */

var TelaModalAddDeclaracaoCompositeView = Marionette.CompositeView.extend({
    template: "#template-modal-add-declaracao",
    tagName: 'div',
    className: 'modal hide fade',
    childView: ListaDeclaracaoItemView,
    childViewContainer: '#body-table-grid-add-declaracao',
    events: {
        'click #btn-gerar-declaracao ': 'gerarDeclaracao'
    },
    model: new Backbone.Model(),
    initialize: function () {
        this.model.set("id_matricula", $('#id-matricula').val());
    },
    gerarDeclaracao: function () {
        if ($('.radio-lista-declaracao').is(":checked")) {
            loading();
            this.$el.modal('hide');
            $.post('/default/entrega-declaracao/salvar-entrega-declaracao', {
                'id_textosistema': itemDeclaracao['id_textosistema'],
                'id_situacao': 79,
                'id_matricula': itemDeclaracao['id_matricula']
            }, function (resp) {
                $.pnotify({title: 'Alerta', text: MensagensG2.MSG_SAVE_SUCCESS, type: 'success'});
                viewGlobal.renderizaTabela();
                loaded();
            });
        } else {
            $.pnotify({
                title: 'Alerta',
                text: sprintf(MensagensG2.MSG_CAMPO_OBRIGATORIO, 'declaração'),
                type: 'warning'
            });
        }
    },
    onShow: function () {
        this.$el.modal('show');
        return this;
    }
});

/**
 ******************************************************************************************
 ******************************************************************************************
 *************************** ADICIONA DECLARAÇÃO  *****************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var AdicionaDeclaracaoCompositeView = Marionette.CompositeView.extend({
    template: '#template-adiciona-declaracao',
    className: 'row-fluid',
    ui: {},
    onShow: function () {
    },
    events: {}
});

/**
 ******************************************************************************************
 ******************************************************************************************
 *************************** SELECT MATRICULAS  *******************************************
 ******************************************************************************************
 ******************************************************************************************
 */

var SelectMatriculaCompositeView = Marionette.CompositeView.extend({
    template: '#template-entrega-declaracao',
    className: 'row-fluid',
    ui: {
        matricula: '#id-matricula',
        'btn-add-declaracao': '#btn-add-declaracao'
    },
    initialize: function () {
        viewGlobal = this;
    },
    onShow: function () {
        $('#btn-add-declaracao').hide();
        var that = this;
        var pessoa = componenteAutoComplete.getFixModel();
        if (pessoa != null) {
            var collectionMatricula = new VwMatriculaCollectionExtend();
            collectionMatricula.url = '/cancelamento/retorna-matriculas/id/' + pessoa.id;
            //collectionMatricula.url = '/api/vw-matricula/id_usuario/' + pessoa.id;

            collectionMatricula.fetch({
                success: function () {
                    var ComboView = new SelectView({
                        el: that.ui.matricula,        // Elemento da DOM
                        collection: collectionMatricula,      // Instancia da collection a ser utilizada
                        childViewOptions: {
                            optionLabel: 'st_labelselect', // Propriedade da Model que será utilizada como label do select
                            optionValue: 'id_matricula', // Propriedade da Model que será utilizada como id do option do select
                            optionSelected: null       // ID da option que receberá o atributo "selected"
                        }
                    });
                    ComboView.render();
                }
            });
        }
    },
    renderizaTabela: function () {
        var vwEntregaDeclaracaoColletion = new VwEntregaDeclaracaoColletionExtend();
        vwEntregaDeclaracaoColletion.url += '?id_matricula=' + this.ui.matricula.val();
        itemDeclaracao['id_matricula'] = this.ui.matricula.val();

        loading();
        vwEntregaDeclaracaoColletion.fetch({
            success: function () {
                var TabelaDeclaracao = new TabelaDeclaracaoCompositeView({
                    collection: vwEntregaDeclaracaoColletion
                });
                telaGrade.dados.show(TabelaDeclaracao);
                $('#btn-add-declaracao').show();
            },
            complete: loaded
        });

    },
    abreModal: function () {
        idMatricula = this.ui.matricula.val();
        var collectionTextos = new VwTextosSistemaCollectionExtend();
        collectionTextos.url += '?id_textocategoria=' + categoriatexto.get('id_categoriatexto');

        collectionTextos.fetch({
            beforeSend: function () {
                loading();
            },
            complete: function () {
                loaded();
            },
            success: function () {
                var viewTextos = new TelaModalAddDeclaracaoCompositeView({
                    collection: collectionTextos
                });
                telaGrade.modal.show(viewTextos);
            }
        });
    },
    events: {
        'change #id-matricula': 'renderizaTabela',
        'click #btn-add-declaracao': 'abreModal'
    }
});


/**
 ******************************************************************************************
 ******************************************************************************************
 *************************** LAYOUT FUNCIONALIDADE ****************************************
 ******************************************************************************************
 ******************************************************************************************
 */
var TelaGradeLayout = Marionette.LayoutView.extend({
    template: '#template-layout-grade',
    tagName: 'div',
    className: 'container-fluid',
    regions: {
        formPesquisa: '#regiao-pesquisa-matricula',
        dados: '#regiao-dados',
        modal: '#regiao-modal'
    },
    onShow: function () {
        componenteAutoComplete.main({
                model: VwPesquisarPessoa,
                element: this.$el.find('#regiao-pesquisa')
            }, function () {
                var SelectView = new SelectMatriculaCompositeView();
                telaGrade.formPesquisa.show(SelectView);
            }, function () {
                telaGrade.formPesquisa.destroy();
            }
        );
        loaded();
    }
});

var telaGrade = new TelaGradeLayout();
G2S.show(telaGrade);
$.ajaxSetup({async: true});
<?php

/**
 *
 * Bootrstrap Específico para o Módulo API, responsável pelas ações REST
 * da aplicação. É dentro desse módulo também que implementamos os webservices
 * que interagem com a aplicação.
 *
 */
class Default_Bootstrap extends Zend_Application_Module_Bootstrap
{

    protected function _bootstrap($resource = NULL)
    {
        $_conf = new Zend_Config_Ini(APPLICATION_PATH . "/apps/" . strtolower($this->getModuleName()) . "/config/application.ini", APPLICATION_ENV);
        $this->_options = array_merge($this->_options, $_conf->toArray());
        parent::_bootstrap();
    }

    protected function _initRoutes()
    {
        $ctrl = Zend_Controller_Front::getInstance();
        $router = $ctrl->getRouter();
        $router->addRoute(
            'seleciona_perfil', new Zend_Controller_Router_Route('/index/perfil/:id_entidade/:id_perfil', array('controller' => 'index',
                'action' => 'select-profile'))
        );

        $router->addRoute(
            'troca_perfil', new Zend_Controller_Router_Route('/index/trocar-perfil/', array('controller' => 'index',
                'action' => 'change-profile'))
        );
        $router->addRoute(
            'tipo_busca', new Zend_Controller_Router_Route('/pesquisa/:tipo', array('controller' => 'pesquisa',
                'action' => 'index'))
        );
        $router->addRoute(
            'pesquisar', new Zend_Controller_Router_Route('/pesquisa/pesquisar', array('controller' => 'pesquisa',
                'action' => 'pesquisar'))
        );
        $router->addRoute(
            'funcionalidade_relatorio', new Zend_Controller_Router_Route('relatorio/master-relatorio/:funcionalidade', array('controller' => 'relatorio',
                'action' => 'master-relatorio'))
        );
        $router->addRoute(
            'backbone_modelo',
            new Zend_Controller_Router_Route(
                'modelo-backbone/:entity/:controllerApi',
                array(
                    'module' => 'default',
                    'controller' => 'modelo-backbone',
                    'action' => 'index',
                    'controllerApi' => null
                )
            )
        );
        $router->addRoute('carrega_js', new Zend_Controller_Router_Route('pessoa/cadastro-rapido/js', array('controller' => 'pessoa', 'action' => 'js')));


        /** Inicialização das Rotas REST para os módulos listados no Array abaixo * */
        // exemplo de configuração:
        // $modulosRest = array(
        //                     'nomedomodulo', <---- Esse módulo inteiro será considerado REST
        //                     'nomedooutromodulo' => array('controlador1', 'controlador2'); <---- Os controladores desse array serão REST
        //                );

        $modulosRest = array(
            'api',
//            'app-api'
        );

        $restRouteUL = new Zend_Rest_Route($ctrl, array(), $modulosRest);
        $router->addRoute('rest', $restRouteUL);

        /** Final da inicialização dos módulos REST * */
    }

    protected function _initSetupBaseUrl()
    {
        $controller = Zend_Controller_Front::getInstance();
        $controller->setBaseUrl('/');
    }

}

<?php

/**
 * Classe para alterar as configurações iniciais, adequanto ao site em questão
 * @author Elcio Mauro Guimarães
 */
class TempoBootstrap extends Bootstrap {
	
	/**
	 * Método para configurar os módulos
	 */
	protected function _initFrontModules() {
		
		$front = Zend_Controller_Front::getInstance();
		$front->setControllerDirectory(
				array(
						'default' 	=> APPLICATION_PATH . '/apps/sites/apps/tempodeconcurso/controllers'
// 						,'rodrigo' 	=> APPLICATION_PATH . '/apps/sites/tempodeconcurso/controllers'
				)
		);
// 		$front->setParam('prefixDefaultModule', true);
	}
	
}
<?php
/**
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 *
 */
class TempoBase extends SitesController {
	
	/**
	 * Método que inicializa as configurações gerais do ambiente
	 */
	public function init(){
		
		
		$dir = Zend_Controller_Front::getInstance()->getModuleDirectory();
		
		$this->_helper
		->layout()
		->setLayoutPath($dir.'/views/layouts');
		
		$this->_helper->layout()->setLayout('tempo');
		
	}
	
	public function _Json($data){
		Zend_Wildfire_Channel_HttpHeaders::getInstance()->flush();
		return $this->_helper->json($data);
	}
	
}
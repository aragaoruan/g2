<?php
/**
 * 
 * Classe com implementações gerais para a camada de Controller
 * @author eder.mariano
 *
 */
class SitesController extends Zend_Controller_Action {

	/**
	 * Zera as configs padronizadas para modulos 
	 */
	public function init(){ }

	/**
	 * Método que desabilita as renderizações de PHTML. Utilizado para requisições AJAX.
	 */
	public function ajax(){
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
	}
	
	
	
}
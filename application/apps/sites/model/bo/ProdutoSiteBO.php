<?php

/**
 * Essa classe trata as particularidades de Produto referente aos Sites
 * Ela extende a ProdutoBO
 * 
 * @author Elcio Mauro Guimarães
 * @since  21/03/2013
 */
class ProdutoSiteBO extends ProdutoBO {

	
	/**
	 * Método que retorna os cursos de determinada Entidade
	 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
	 * @since 21/03/2013
	 * @see CursoController::listaDeCursos() 
	 * @param int $id_entidade
	 * @return Ead1_Mensageiro
	 */
	public function listarProdutosEntidade($id_entidade){
		
		$vwpTO = new VwProdutoTO();
		$vwpTO->setId_entidade($id_entidade);
		$produtoBO = new ProdutoBO();
		return $produtoBO->retornarVwProduto($vwpTO);
		
	}
	
	
	
	
}

<?php

class AppApi_AlunoController extends Ead1_AppApi
{
    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
    }

    public function getAction()
    {

    }

    public function postAction()
    {

    }

    public function putAction()
    {

    }

    public function deleteAction()
    {

    }

    public function headAction()
    {

    }

    /**
     * Retorna os cursos vinculados ao aluno
     */
    public function getCursosAction()
    {
        //TODO tirar esse if para produção
        if (!$this->getRequest()->isOptions()) {
            //pega o cabeçalho
            $header = $this->_request->getHeader('user_token');
            $tokenObj = JWT::decode($header, null, array('HS256'));//pega o token
            $idUser = $tokenObj->user_id;//pega o id do usuario
            $idEntidade = $tokenObj->entidade_id ? $tokenObj->entidade_id : EntidadeTO::AVM;//pega o id da entidade;

            //instancia a negocio
            $business = new \G2\Negocio\Matricula();
            $result = $business->retornarCursosByAluno($idUser, $idEntidade); //busca os dados

            $this->getResponse()->appendBody(json_encode($result));
        }
    }

    public function getMensagensAction()
    {
        try {
            if (!$this->getRequest()->isOptions()) {
                $header = $this->_request->getHeader('user_token');
                $tokenObj = JWT::decode($header, null, array('HS256'));

                $arg = array(
                    "id_usuario" => $tokenObj->user_id
                );

                $mensagemNegocio = new \G2\Negocio\MensagemPortal;
                $mensagens = $mensagemNegocio->findVwEnviarMensagemAluno($arg, true);

                if ($mensagens) {
                    $arrIdEnviomensagens = array_unique(array_column($mensagens, 'id_enviomensagem'));
                    $mensagemNegocio->updateEnvioDestinatarioEntregueEmLote($arrIdEnviomensagens);
                }

                $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode($mensagens));
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function putAtualizarMensagemAction()
    {
        try {
            $params = $data = Zend_Json::decode($this->getRequest()->getRawBody());

            $mensagemNegocio = new \G2\Negocio\MensagemPortal();
            if ($mensagemNegocio->atualizarMensagem($params)) {
                $this->getResponse()->setHttpResponseCode(200)->appendBody(json_encode(['retorno' => true]));
            } else {
                $this->getResponse()->setHttpResponseCode(200)->appendBody(json_encode(['retorno' => false]));
            }

        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getNovasMensagensAction()
    {
        try {
            if (!$this->getRequest()->isOptions()) {
                $header = $this->_request->getHeader('user_token');
                $tokenObj = JWT::decode($header, null, array('HS256'));

                $arg = array(
                    'id_usuario' => $tokenObj->user_id,
                    'id_evolucao' => 42,
                    'bl_entrega' => false
                );

                $mensagemNegocio = new \G2\Negocio\MensagemPortal();
                $mensagens = $mensagemNegocio->findVwEnviarMensagemAluno($arg, true);

                if ($mensagens) {
                    $arrIdEnviomensagens = array_unique(array_column($mensagens, 'id_enviomensagem'));
                    $mensagemNegocio->updateEnvioDestinatarioEntregueEmLote($arrIdEnviomensagens);
                }

                $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode($mensagens));
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}
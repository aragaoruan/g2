<?php

/**
 * Class AppApi_DisciplinaController
 */
class AppApi_ConteudoController extends Ead1_AppApi
{

    public function indexAction()
    {
        $array = array();

        $params = $this->getRequest()->getParams();
        unset($params['module'], $params['controller'], $params['action']);

        $negocio = new \G2\Negocio\Disciplina();
        $result = $negocio->findByDisciplinaConteudo($params);

        foreach ($result as $key => $value) {
            $array[$key] = $negocio->toArrayEntity($value);
        }

        $this->_helper->json($array);
    }
}
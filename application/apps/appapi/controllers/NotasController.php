<?php

/**
 * Class AppApi_NotasController
 */
class AppApi_NotasController extends Ead1_AppApi {

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $negocio = new G2\Negocio\Notas();
        $params = $this->getRequest()->getParams();
        unset($params['module'], $params['controller'], $params['action'], $params['id']);
        $results = $negocio->getNotasFormatoApp($params['id_matricula']);
        $this->_helper->json($results);
    }
}
<?php

/**
 * Class AppApi_DisciplinaController
 */
class AppApi_DisciplinaController extends Ead1_AppApi
{

    public function init()
    {
        parent::init();
    }


    public function indexAction()
    {
        //TODO tirar esse if para produção
        if (!$this->getRequest()->isOptions()) {
            $params = $this->getAllParams();//pega os parametros que veio
            unset($params['controller'], $params['action'], $params['module']);//remove alguns desnecessáriso

            $header = $this->_request->getHeader('user_token');
            $tokenObj = JWT::decode($header, null, array('HS256'));//pega o token
            if ($tokenObj->user_id) {
                $idUser = $tokenObj->user_id;//pega o id do usuario
            }

            $negocio = new \G2\Negocio\Disciplina();
            $result = $negocio->retornarDisciplinasAluno($idUser, $params);
            $this->getResponse()
                ->appendBody(json_encode($result));
        }
    }


    public function getAction()
    {
        ;
    }

    public function detailsDisciplinaSalaAction()
    {
        if (!$this->getRequest()->isOptions()) {
            $negocio = new \G2\Negocio\GerenciaSalas();

            $params = $this->getRequest()->getParams();
            unset($params['module'], $params['controller'], $params['action']);

            $result = $negocio->retornaUnicoRegistroVwAlunoGradeIntegracao($params);
            $toArray = $negocio->toArrayEntity($result);
            $toArray['dt_abertura'] = $result->getDt_abertura()->format('d/m/Y');

            $dt = new \DateTime($result->getDt_encerramentosala());
            $toArray['dt_encerramentosala'] = $dt->format('d/m/Y');

            $this->_helper->json($toArray);
        }
    }

    public function listConteudoDisciplinaAction()
    {
        if (!$this->getRequest()->isOptions()) {
            $array = array();

            $params = $this->getRequest()->getParams();
            unset($params['module'], $params['controller'], $params['action']);

            $negocio = new \G2\Negocio\Disciplina();
            $result = $negocio->findByDisciplinaConteudo($params);

            foreach ($result as $key => $value) {
                $array[$key] = $negocio->toArrayEntity($value);
            }

            $this->_helper->json($array);
        }
    }


}
<?php

class AppApi_GcmController extends Ead1_AppApi
{
    public function init()
    {
        parent::init();
    }

    public function postAction()
    {
        try {
            $params = Zend_Json::decode($this->getRequest()->getRawBody());

            $result = array();
            $header = $this->_request->getHeader('user_token');
            $tokenObj = JWT::decode($header, null, array('HS256'));

            if ($tokenObj) {
                $negocio = new \G2\Negocio\Negocio();

                $ref = $negocio->getReference('\G2\Entity\Usuario', $tokenObj->user_id);

                $entity = new \G2\Entity\UsuarioDevice();
                $entity->setId_usuario($ref);
                $entity->setSt_key($params['st_key']);
                $entity->setSt_infodevice($params['st_infodevice']);
                $entity->setDt_cadastro(new DateTime());

                $save = $negocio->save($entity);
                $result = $negocio->toArrayEntity($save);
            }
            $this->_helper->json($result);

        } catch (\Doctrine\ORM\ORMException $e) {
            $this->_helper->json($e->getMessage());
        }
    }

    public function deleteAction()
    {
        try {
            $params = Zend_Json::decode($this->getRequest()->getRawBody());

            $where = array(
                'st_key' => trim($params['st_key'])
            );

            if ($where) {
                $negocio = new \G2\Negocio\Negocio();
                $result = $negocio->findOneBy('\G2\Entity\UsuarioDevice', $where);

                if ($result) {
                    $negocio->delete($result);
                }
            }
            $this->_helper->json($where);

        } catch (\Doctrine\ORM\ORMException $e) {
            $this->_helper->json($e->getMessage());
        }
    }
}
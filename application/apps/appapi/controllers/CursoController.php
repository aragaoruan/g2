<?php

class AppApi_CursoController extends Ead1_AppApi
{
    public function indexAction()
    {
        //pega o cabeçalho
        $header = $this->_request->getHeader('user_token');
        $tokenObj = JWT::decode($header, null, array('HS256'));//pega o token
        $idUser = $tokenObj->user_id;//pega o id do usuario
        $idEntidade = $tokenObj->entidade_id ? $tokenObj->entidade_id : EntidadeTO::AVM;//pega o id da entidade;

        //instancia a negocio
        $business = new \G2\Negocio\Matricula();
        $result = $business->retornarCursosByAluno($idUser, $idEntidade); //busca os dados

        $this->getResponse()->appendBody(json_encode($result));
    }
}
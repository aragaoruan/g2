<?php

class AppApi_MensagemController extends Ead1_AppApi
{
    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $header = $this->_request->getHeader('user_token');
        $tokenObj = JWT::decode($header, null, array('HS256'));

        $arg = array(
            "id_usuario" => $tokenObj->user_id
        );

        $mensagemNegocio = new \G2\Negocio\MensagemPortal;
        $mensagens = $mensagemNegocio->findVwEnviarMensagemAluno($arg, true);

        if ($mensagens) {
            $arrIdEnviomensagens = array_unique(array_column($mensagens, 'id_enviomensagem'));
            $mensagemNegocio->updateEnvioDestinatarioEntregueEmLote($arrIdEnviomensagens);
        }

        $this->getResponse()
            ->setHttpResponseCode(200)
            ->appendBody(json_encode($mensagens));
    }

    public function putAction()
    {
        try {
            $params = $data = Zend_Json::decode($this->getRequest()->getRawBody());

            $mensagemNegocio = new \G2\Negocio\MensagemPortal();
            if ($mensagemNegocio->atualizarMensagem($params)) {
                $this->getResponse()->setHttpResponseCode(200)->appendBody(json_encode(['retorno' => true]));
            } else {
                $this->getResponse()->setHttpResponseCode(200)->appendBody(json_encode(['retorno' => false]));
            }

        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}
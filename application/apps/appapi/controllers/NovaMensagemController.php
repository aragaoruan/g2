<?php

class AppApi_NovaMensagemController extends Ead1_AppApi
{
    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        try {
            $header = $this->_request->getHeader('user_token');
            $tokenObj = JWT::decode($header, null, array('HS256'));

            $arg = array(
                'id_usuario' => $tokenObj->user_id,
                'id_evolucao' => 42,
                'bl_entrega' => false
            );

            $mensagemNegocio = new \G2\Negocio\MensagemPortal();
            $mensagens = $mensagemNegocio->findVwEnviarMensagemAluno($arg, true);

            if ($mensagens) {
                $arrIdEnviomensagens = array_unique(array_column($mensagens, 'id_enviomensagem'));
                $mensagemNegocio->updateEnvioDestinatarioEntregueEmLote($arrIdEnviomensagens);
            }

            $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($mensagens));

        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}
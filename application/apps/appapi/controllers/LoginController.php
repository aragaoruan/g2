<?php

class AppApi_LoginController extends Ead1_AppApi
{
    public function init()
    {
        $this->_helper->viewRenderer->setNoRender(true);
    }

    public function indexAction()
    {

    }

    public function getAction()
    {

    }

    public function postAction()
    {
        try {
            $params = $data = Zend_Json::decode($this->getRequest()->getRawBody());

            if (isset($params['login']) && isset($params['senha'])) {
                $user = new \G2\Entity\VwValidaLoginPortal();
                $user->setSt_login($params['login']);
                $user->setSt_email($params['login']);
                $user->setSt_senha(md5($params['senha']));
                $user->setSt_senhaentidade($params['senha']);
                $user->setBl_acessoapp(true);

                $login = new \G2\Negocio\Auth();
                $auth = $login->retornaSelectLoginUsuarioPortal($user);

                if ($auth) {
                    $token = array(
                        "user_id" => $auth->getId_usuario(),
                        "user_name" => $auth->getSt_nomecompleto(),
                        "user_avatar" => $auth->getSt_urlavatar() != '' ? Ead1_Ambiente::geral()->st_url_gestor2 . $auth->getSt_urlavatar() : '',
                        "login" => $auth->getSt_login(),
                        "entidade_id" => $params['entidadeId']
                    );

                    $authToken = new \G2\Entity\AuthToken();
                    $this->token = JWT::encode($token, $authToken->getSecretToken());

                    $this->getResponse()->setHttpResponseCode(200)
                        ->setBody(Zend_Json_Encoder::encode([
                            'retorno' => true,
                            'messagem' => 'Sucesso! Usuário logado com sucesso.',
                            'token' => $this->token
                        ]));
                } else {
                    $this->getResponse()->setHttpResponseCode(200)->setBody(Zend_Json_Encoder::encode(['retorno' => false, 'messagem' => 'Erro ao validar suas credencias.']));
                }

                return true;
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

        $this->getResponse()->setHttpResponseCode(200)->setBody(Zend_Json_Encoder::encode(['retorno' => false, 'messagem' => 'Você não possui permissão para acessar ou alguns dados estão faltando.']));
        return false;
    }

    public function putAction()
    {
        die;
    }

    public function deleteAction()
    {
        die;
    }

    public function headAction()
    {
        die;
    }

    public function postGcmKeyUserAction()
    {
        try {
            if (!$this->getRequest()->isOptions()) {
                $params = Zend_Json::decode($this->getRequest()->getRawBody());

                $result = array();
                $header = $this->_request->getHeader('user_token');
                $tokenObj = JWT::decode($header, null, array('HS256'));

                if ($tokenObj) {
                    $negocio = new \G2\Negocio\Negocio();

                    $ref = $negocio->getReference('\G2\Entity\Usuario', $tokenObj->user_id);

                    $entity = new \G2\Entity\UsuarioDevice();
                    $entity->setId_usuario($ref);
                    $entity->setSt_key($params['st_key']);
                    $entity->setSt_infodevice($params['st_infodevice']);
                    $entity->setDt_cadastro(new DateTime());

                    $save = $negocio->save($entity);
                    $result = $negocio->toArrayEntity($save);
                }

                $this->_helper->json($result);
            }
        } catch (\Doctrine\ORM\ORMException $e) {
            $this->_helper->json($e->getMessage());
        }
    }

    public function deleteGcmKeyUserAction()
    {
        try {
            if (!$this->getRequest()->isOptions()) {

                $params = Zend_Json::decode($this->getRequest()->getRawBody());

                $where = array(
                    'st_key' => trim($params['st_key'])
                );

                if ($where) {
                    $negocio = new \G2\Negocio\Negocio();
                    $result = $negocio->findOneBy('\G2\Entity\UsuarioDevice', $where);

                    if ($result) {
                        $negocio->delete($result);
                    }
                }

                $this->_helper->json($where);
            }
        } catch (\Doctrine\ORM\ORMException $e) {
            $this->_helper->json($e->getMessage());
        }
    }
}
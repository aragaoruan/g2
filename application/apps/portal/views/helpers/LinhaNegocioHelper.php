<?php

/**
 * Habilita o script do hi-bot
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2018-03-01
 */
class Zend_View_Helper_LinhaNegocioHelper extends Zend_View_Helper_Abstract
{
    public static function linhaNegocioHelper($id_entidade = false)
    {
        $linhas = array(2 => 'Graduação', 3 => 'Pós');
        //busca os dados da linha de negócio da entidade logada
        $linhaNegocio = (new \G2\Negocio\EsquemaConfiguracao())
            ->retornaItemPorEntidade(\G2\Constante\ItemConfiguracao::LINHA_DE_NEGOCIO,
                !empty($id_entidade)? $id_entidade :Ead1_Sessao::getSessaoEntidade()->id_entidade);

        if ($linhaNegocio) {
            return $linhas[$linhaNegocio["st_valor"]];
        }

    }
}

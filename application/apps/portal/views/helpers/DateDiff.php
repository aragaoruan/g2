<?php

/**
 * Class Portal_View_Helper_DateDiff
 * Classe helper para calcular diferença entre datas
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Portal_View_Helper_DateDiff extends Zend_View_Helper_Abstract
{

    /**
     * Método para calcular o número de dias entre duas datas
     * @param string $dt_inicio
     * @param string $dt_fim
     * @return mixed
     */
    public function dateDiff($dt_inicio, $dt_fim)
    {
        $bo = new Ead1_BO();
        return $bo->diferencaDias($dt_inicio, $dt_fim);
    }


}
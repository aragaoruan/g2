<?php

/**
 * Verifica se o aluno deve ou n?o passar pelo primeiro acesso
 * @author Helder Silva <helder.silva@unyleya.com.br>
 * @since 2017-08-16
 */
class Portal_View_Helper_VerificaPrimeiroAcesso extends Zend_View_Helper_Abstract
{

    /**
     * Verifica primeiro acesso do aluno
     * @return bool
     */
    public function verificaPrimeiroAcesso()
    {
        $primeiroAcessoBO = new PrimeiroAcessoBO();
        //Verifica se � aluno e se j� concluiu todo o primeiro acesso
        if (Ead1_Sessao::getSessaoPerfil()->is_aluno() &&
            !Ead1_Sessao::getSessaoPerfil()->is_alunoInstitucional() &&
            !$primeiroAcessoBO->retornarProgresso()->getBl_mensageminformativa() &&
            $primeiroAcessoBO->passarPeloPrimeiroAcesso()
        ) {
            return true;
        }
        return false;
    }

}
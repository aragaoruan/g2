<?php


use \G2\Constante\Situacao;

/**
 * Helper para retornar o nome da classe css do respectivo status
 * Class Portal_View_Helper_AtividadeStatusColor
 */
class Portal_View_Helper_AtividadeStatusColor extends Zend_View_Helper_Abstract
{
    /**
     * Retorna as classes css das cores de cada status, segue o padrão utilizado na tela principal do sistema
     * @param $status
     * @return string
     */
    public function atividadeStatusColor($status)
    {
        switch ($status) {
            default:
                return "";
                break;
            case Situacao::TB_ATIVIDADECOMPLEMENTAR_DEFERIDO:
                return "text-success";
                break;
            case Situacao::TB_ATIVIDADECOMPLEMENTAR_INDEFERIDO:
                return "text-danger";
                break;
            case Situacao::TB_ATIVIDADECOMPLEMENTAR_ENVIADO_PARA_ANALISE:
                return "text-primary";
                break;

        }
    }


}

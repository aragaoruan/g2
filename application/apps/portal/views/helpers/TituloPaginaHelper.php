<?php

/**
 * @author Elcio Mauro Guimarães
 */
class Zend_View_Helper_TituloPaginaHelper  extends Zend_View_Helper_Abstract
{
    
	
	/**
	 * Helper que Adiciona navegabilidade e titulo ao portal
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @param String $st_titulo
	 * @param array $navegacao = array(
	 * 									 'st_destino'=>'conteudo'
	 * 									,'st_url'=>'/portal/sala-de-aula/gestao'
	 * 									,'st_parametros'=>'id_saladeaula=12'
	 * 									,'st_titulo'=>'Gestão da Sala de Aula'
	 * 								);
	 * 				
	 * @param Boolean $ajax
	 */
	public static function tituloPaginaHelper($st_titulo, array $navegacao = null, $ajax = true){
    	
		$links = false;		
		if($navegacao){
			foreach($navegacao as $link){
				if($links){
					$links .= ' >> ';
				}
				
				if($ajax){
					$links .= "<a style=\"text-decoration:none; font-size:10px\" href=\"javascript: Portal.Ajax.loadHTML('".$link['st_destino']."','".$link['st_url']."','".$link['st_parametros']."') \">".$link['st_titulo']."</a>";
				} else {
					$links .= "<a style=\"text-decoration:none; font-size:10px\" href=\"".Ead1_Ambiente::geral()->baseUrl.'/portal'.$link['st_url']."/?".$link['st_parametros']."\" target=\"".$link['st_destino']."\">".$link['st_titulo']."</a>";
				}
			}
			
			if($links){
				$links = "<div>".$links."</div>";
			}
			
		}
		echo "<div style=\"font-family:Verdana, Geneva, sans-serif; padding-left:5px; margin-bottom:10px;\"><div style=\"font-size:16px; padding-bottom:7px;\">".$st_titulo."</div>".$links."</div>";

	}
    
    
}
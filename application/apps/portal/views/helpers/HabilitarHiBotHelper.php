<?php

/**
 * Habilita o script do hi-bot
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2018-03-01
 */
class Zend_View_Helper_HabilitarHiBotHelper extends Zend_View_Helper_Abstract
{

    public static function habilitarHiBotHelper()
    {
        //array com os ids dos projetos que serão habilitados o bot, isso deverá ser uma configuração do projeto
        if (Ead1_Sessao::getSessaoUsuario()->id_usuario &&
            (Ead1_Sessao::getSessaoPerfil()->is_aluno() || Ead1_Sessao::getSessaoPerfil()->is_alunoInstitucional())
            && in_array(Ead1_Sessao::getSessaoProjetoPedagogico()->id_projetopedagogico, [2209, 4526])
        ) {
            return true;
        }
        return false;
    }

}

<?php

/**
 * Gerencia a cor do sistema de acordo com o ambiente
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2013-08-01
 */
class Zend_View_Helper_LayoutAmbienteHelper extends Zend_View_Helper_Abstract
{

    /**
     * Retorna a cor de background de acordo com o ambiente
     * @return string retorna a cor
     */
    public static function layoutAmbienteHelper ()
    {
        //recupera o ambiente
        $ambiente = Ead1_Ambiente::getAmbiente();
        switch ($ambiente) {
            case 'desenvolvimento':
                $arrReturn = array(
                    'cor' => '#009966',
                    'nome' => 'DESENVOLVIMENTO'
                );
                break;
            case 'homologacao':
                $arrReturn = array(
                    'cor' => '#009966',
                    'nome' => 'HOMOLOGAÇÃO'
                );
                break;
            default :
                $arrReturn = array(
                    'cor' => '#006699',
                    'nome' => ''
                );
                break;
        }

        return $arrReturn;
    }

}
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * *                             VAR�AVEIS                             * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */

var that;
var dataRequest = {};
//var idmes = $('#id_mesextrato');
//var idano = $('#id_anoextrato');
var mesesporextenso = [];

/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * *                             MODELS                                * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */

/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * *                        COLLECTIONS                                * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */


var DataPagamentoCollection = Backbone.PageableCollection.extend({
    url: '/vw-encerramento-salas/retornar-encerramento',
    model: VwEncerramentoSalas,
    state: {
        pageSize: 10
    },
    mode: "client",
    pesquisar: function() {
        var encerramento = [];
        _(this.models).each(function (model) {
            if (model.get('dt_encerramentofinanceiro') != '')//&& model.get('id_usuario') == )
                encerramento.push(model.toJSON());
        });
        /*if (encerramento.length > 0)
        {
            ajax({

            })
        }*/
    }
});

var OptionMesView = Backbone.View.extend({
    template: _.template('id_usuario'),
    render: function() {
        this.$el.attr('value', this.model.id_usuario ? this.model.id_usuario : this.model.get('id_usuario'));
        if(this.model.get('dt_encerramentofinanceiro') != '' && this.model.get('nu_valorpago') != null)
        {
            var data = new Date(this.model.get('dt_encerramentofinanceiro'));
            var mes = data.getMonth();
            var mesretorno = [];
            mesesporextenso = ["Janeiro","Fevereiro","Mar�o","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"];
            var i;
            var j = 0;
            if(mesretorno.length()==0)
            {
                mesretorno[0] = mesesporextenso[mes-1];
            }else
            {
                for (i = 0; i < mesretorno.length(); i++)
                {
                    if(mesesporextenso[mes-1] != mesretorno[i])
                    {
                        j++;
                    }
                }
                if(j == mesretorno.length())
                {
                    mesretorno.push(mesesporextenso[mes]);
                    return mesesporextenso[mes];
                }
            }
        }
    }
});

var OptionAnoView = Backbone.View.extend({
    template: _.template('id_usuario'),
    render: function() {
        if(this.model.get('dt_encerramentofinanceiro') != '' && this.model.get('nu_valorpago') != null)
        {
            this.$el.attr('value', this.model.id_usuario ? this.model.id_usuario : this.model.get('id_usuario'));
            var data = new Date(this.model.get('dt_encerramentofinanceiro'));
            var ano = data.getYear();
            var anoretorno = [];
            var i;
            var j = 0;
            if(anoretorno.length()==0)
            {
                anoretorno[0] = ano;
            }else
            {
                for (i = 0; i < anoretorno.length(); i++)
                {
                    if(ano != anoretorno[i])
                    {
                        j++;
                    }
                }
                if(j == anoretorno.length())
                {
                    anoretorno.push(ano);
                    return ano;
                }
            }
        }
    }
});

var FormPesquisaExtratoView = Backbone.View.extend({
    template: '#template-extrato-pagamento',
    initialize: function() {
        this.render();
    },
    render: function()
    {
        that = this;
        this.popularMes();
        this.popularAno();
        loaded();
    },
    events: {
        "click .pesquisaExtrato": "renderExtratoView"
    },
    extratoView: null,

    popularMes: function()
    {
        var collection = new DataPagamentoCollection();
        collection.url = '/api/vw-encerramento-salas';
        collection.fetch({
            success: function () {
                var comboView = new CollectionView({
                    collection: collection,
                    childViewTagName: 'option',
                    childViewConstructor: OptionMesView,
                    el: $('#id_mesextrato')
                });
                comboView.render();
                $(comboView.el).prepend("<option value='' selected>Todos</option>");
            }
        });

    },

    popularAno: function()
    {
        var collection = new DataPagamentoCollection();
        collection.url = '/api/vw-encerramento-salas';
        collection.fetch({
            success: function () {
                var comboView = new CollectionView({
                    collection: collection,
                    childViewTagName: 'option',
                    childViewConstructor: OptionAnoView,
                    el: $('#id_anoextrato')
                });
                comboView.render();
                $(comboView.el).prepend("<option value='' selected>Todos</option>");
            }
        });
    },

    renderExtratoView: function ()
    {
        if (this.extratoView) {
            this.extratoView.destroyMe();
        }
        this.extratoView = new ExtratoView();
    }

});


var ExtratoView = Backbone.View.extend({
    initialize: function () {
        this.render();
    },
    render: function() {
        this.renderExtrato();
        return this;
    },
    destroyMe: function() {
        //COMPLETELY UNBIND THE VIEW
        this.undelegateEvents();
        $(this.el).removeData().unbind();
        //Remove view from DOM
        this.el.remove();
        Backbone.View.prototype.remove.call(this);
    },
    renderExtrato: function() {
        var containerExtrato = $('#container-extrato');
        var colecao = that.collection;
        var mespagamento = idmes.val();
        var anopagamento = idano.val();

        var cellInformacoes = Backgrid.Cell.extend({
            events: {
                "click .detalhes": "abrirDetalhamento"
            },

            render: function () {
                this.$el.html(_.template("<div class='btn-toolbar'><div class='btn-group'> <a class='btn  btn-success detalhes' title='Detalhes' href='javascript:void(0)'>" +
                    "<i class='icon icon-white icon-list-alt'></i>Detalhes</a></div></div>"));
                return this;
            },

            abrirDetalhamento: function () {
                var columns = null;
                columns = [
                    {
                        name: "id_encerramentosala",
                        label: "ID",
                        editable: false,
                        cell: "string"
                    },
                    {
                        name: "st_saladeaula",
                        label: "Sala de Aula",
                        editable: false,
                        cell: cellSalaDeAula
                    },
                    {
                        name: "nu_cargahoraria",
                        label: "C.H",
                        editable: false,
                        cell: "string"
                    },
                    {
                        name: "st_disciplina",
                        label: "Disciplina",
                        editable: false,
                        cell: "string"
                    },
                    {
                        name: "nu_alunos",
                        label: "Alunos",
                        editable: false,
                        cell: "string"
                    },
                    {
                        name: "st_encerramentoprofessor",
                        label: "Coordenador",
                        editable: false,
                        cell: "string"
                    },
                    {
                        name: "st_encerramentocoordenador",
                        label: "Pedag�gico",
                        editable: false,
                        cell: "string"
                    },
                    {
                        name: "st_encerramentopedagogico",
                        label: "Financeiro",
                        editable: false,
                        cell: "string"
                    },
                    {
                        name: "st_encerramentofinanceiro",
                        label: "Pagamento",
                        editable: false,
                        cell: "string"
                    },
                    {
                        name: "nu_valorpago",
                        label: "Valor pago",
                        editable: false,
                        cell: "number"
                    }
                ];

            }
        });

        var columns = null;
        var data = new Date(this.model.get('dt_encerramentofinanceiro'));
        var pagamento = 0;

        if(mespagamento == "" && anopagamento == "")
        {

            columns.push({
                name: "mespagamento",
                label: "Valor Total",
                editable: false,
                cell: float
            });

            columns.push({
                name: "anopagamento",
                label: "Ano",
                editable: false,
                cell: float
            });

        }else {
            if (mespagamento != "" && anopagamento == "") {
                if (mespagamento == data.getMonth()) {
                    columns.push({
                        name: "mespagamento",
                        label: "M�s",
                        editable: false,
                        cell: "string"
                    });
                    var ano = data.getMonth();
                    columns.push({
                        name: "ano",
                        label: "Ano",
                        editable: false,
                        cell: int
                    });
                    pagamento = pagamento + this.model.get('nu_valorpago');
                }
            } else {
                if(anopagamento == data.getYear())
                {
                    columns.push({
                        name: "anopagamento",
                        label: "Ano",
                        editable: false,
                        cell: int
                    });
                    var mes = data.getMonth();
                    mes = mesesporextenso[mes];
                    columns.push({
                        name: "mes",
                        label: "M�s",
                        editable: false,
                        cell: "string"
                    });
                }
            }
            columns = [
                {
                    name: "detalhes",
                    label: "Informa��es",
                    editable: false,
                    cell: cellInformacoes
                }
            ];
        }
        var paginacaoPrincipal = new Backgrid.Extension.Paginator({
            collection: colecao
        });

        colecao.fetch({
            data: dataRequest,
            complete: function(collection, response) {
                containerGrid.empty().append(grid.render().el).append(paginacaoPrincipal.render().el);
                loaded();
            },
            beforeSend: function() {
                loading();
            }
        });
    },
    removeColumns: function(columns, index) {
        columns.splice(index, 1);
    }


});
alert("Teste");
$(document).ready(function(){
    var viewPesquisa = new FormPesquisaExtratoView({
        el: "#template-extrato-professor"
    });
    alert("Teste");
    viewPesquisa.render();
});
<?php

/**
 * Classe de Encapsulamento de Regra de Négocio de Login do Portal
 * @author eduardoromao
 */
class PortalLoginBO extends Ead1_BO
{

    private $dadosAcesso;

    /**
     * Método construtor
     */
    public function __construct()
    {
        $this->mensageiro = new Ead1_Mensageiro();
        $this->dadosAcesso = new DadosAcessoTO();
    }

    /**
     * Método que valida o Login e Usuario
     * @param VwUsuarioPerfilPedagogicoTO $vwUsuarioPPTO
     * @return Ead1_Mensageiro
     */
    public function validateUserPass(VwUsuarioPerfilPedagogicoTO $vwUsuarioPPTO)
    {
        try {
            /*
             * Refatorei o código para validar tudo em um if apenas, e adicionei a validação do email.
             * */
            $negocioAuth = new \G2\Negocio\Auth();
            $usuario = $this->validaLoginUsuario($vwUsuarioPPTO->getSt_login());
            $email = $this->validaEmail($vwUsuarioPPTO->getSt_login());
            $senha = $this->validaLoginUsuario($vwUsuarioPPTO->getSt_senha());

            if (($usuario || $email) && $senha) {
                $this->dadosAcesso->st_login = $vwUsuarioPPTO->getSt_login();
                $dados = $negocioAuth->retornaSelectLoginUsuarioPortal($vwUsuarioPPTO);
            } else {
                THROW new Zend_Validate_Exception("ERR001: Usuário ou senha incorretos.");
            }
            if (!empty($dados)) {
                Ead1_Sessao::sessionStart();
                $negocioG2 = new G2\Negocio\Negocio();
                $arrVwUsuarioPerfilPedagogicoTO = $negocioG2->toArrayEntity($dados);
                if (count($dados) == 1) {
                    Ead1_Sessao::setSessionGeral($arrVwUsuarioPerfilPedagogicoTO);
                    //Ead1_Sessao::setSessionEntidade($arrVwUsuarioPerfilPedagogicoTO);
                    Ead1_Sessao::setSessionPerfil($arrVwUsuarioPerfilPedagogicoTO);
                    Ead1_Sessao::setSessionProjetoPedagogico($arrVwUsuarioPerfilPedagogicoTO);
                }

                // Descobrir nome do usuario logado (st_nomecompleto);
                $usuarioLogado = $negocioG2->find('\G2\Entity\Usuario', $arrVwUsuarioPerfilPedagogicoTO['id_usuario']);
                if ($usuarioLogado && $usuarioLogado->getId_usuario())
                    $arrVwUsuarioPerfilPedagogicoTO['st_nomecompleto'] = $usuarioLogado->getSt_nomecompleto();

                Ead1_Sessao::setSessionUsuario($arrVwUsuarioPerfilPedagogicoTO);
                $this->mensageiro->setMensageiro($arrVwUsuarioPerfilPedagogicoTO, Ead1_IMensageiro::SUCESSO, "login/selecionaperfil");
            } else {
                THROW new Zend_Validate_Exception("ERR002: Usuário ou senha incorretos.");
            }
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro("ERR003: Erro ao Verificar Usuário", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }


    /**
     * Valida o acesso direto ao Portal por URL
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param VwUsuarioPerfilPedagogicoTO $usTO
     * @throws Zend_Validate_Exception
     * @return Ead1_Mensageiro
     */
    public function validaAcessoDireto(VwUsuarioPerfilPedagogicoTO $usTO, $st_gravalog = 'sim', $id_usuariooriginal = null)
    {
        try {
            //Aceita alfanumerico e ponto
            $retorno = array();
            if (!$usTO->getId_usuario()) {
                THROW new Zend_Exception("Usuário não informado.");
            }
            if (!$usTO->getId_entidade()) {
                THROW new Zend_Exception("Entidade não informada.");
            }
            if (!$usTO->getSt_urlacesso()) {
                THROW new Zend_Exception("Código de Acesso não informado.");
            }

            $pessoaDAO = new PessoaDAO();
            $dados = $pessoaDAO->retornarVwUsuarioperfilPedagogico($usTO)->toArray();

            if (!empty($dados)) {
                Ead1_Sessao::sessionStart();
                $vwUsuarioPerfilPedagogicoTO = Ead1_TO_Dinamico::encapsularTo($dados, new VwUsuarioPerfilPedagogicoTO(), true);
                $arrVwUsuarioPerfilPedagogicoTO = $vwUsuarioPerfilPedagogicoTO->toArray();
                $arrVwUsuarioPerfilPedagogicoTO['st_gravalog'] = $st_gravalog;
                $arrVwUsuarioPerfilPedagogicoTO['id_usuariooriginal'] = ($id_usuariooriginal != null ? $id_usuariooriginal : Ead1_Sessao::getSessaoUsuario()->id_usuario);
                unset($arrVwUsuarioPerfilPedagogicoTO['st_senha']);

                if (count($dados) == 1) {
                    Ead1_Sessao::setSessionGeral($arrVwUsuarioPerfilPedagogicoTO);
                    Ead1_Sessao::setSessionEntidade($arrVwUsuarioPerfilPedagogicoTO);
                    Ead1_Sessao::setSessionPerfil($arrVwUsuarioPerfilPedagogicoTO);
                    Ead1_Sessao::setSessionProjetoPedagogico($arrVwUsuarioPerfilPedagogicoTO);
                    Ead1_Sessao::setSessionUsuario($arrVwUsuarioPerfilPedagogicoTO);
                    $this->mensageiro->setMensageiro("Usuário validado com sucesso.", Ead1_IMensageiro::SUCESSO, "Index/principal");
                } else {
                    THROW new Zend_Exception("Erro ao tentar logar o usuário: Mais de um perfil encontrado com os dados informados.");
                }
            } else {
                THROW new Zend_Exception("Usuário ou senha incorretos.");
            }
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro;
    }

    /**
     * Método que valida os parametros de acesso do perfil do usuario
     * @param VwUsuarioPerfilPedagogicoTO $to
     * @throws Zend_Validate_Exception
     */
    protected function validaPerfilAcesso(VwUsuarioPerfilPedagogicoTO $to)
    {
        if ($to->getId_perfilpedagogico() == PerfilPedagogicoTO::ALUNO) {
            if (!$to->getId_matricula()) {
                THROW new Zend_Validate_Exception("Matrícula do Usuário não Definida!");
            }
        }
        if (!$to->getId_perfil()) {
            THROW new Zend_Validate_Exception("Perfil do Usuário não Definido!");
        }
        if (!$to->getId_usuario()) {
            THROW new Zend_Validate_Exception("Usuário não Identificado!");
        }
        if (!$to->getId_perfilpedagogico()) {
            THROW new Zend_Validate_Exception("Perfil Pedagógico do Usuário não Definido!");
        }
        if (!$to->getId_entidade()) {
            THROW new Zend_Validate_Exception("Instituição do Usuário não Definida!");
        }
    }

    /**
     * Método que retorna os Perfis do Usuario
     * @param UsuarioTO $usuarioTO
     * @return Ead1_Mensageiro
     */
    public function retornarPerfilUsuario(UsuarioTO $usuarioTO)
    {
        try {
            if (!$usuarioTO->getId_usuario()) {
                $usuarioTO->setId_usuario(Ead1_Sessao::getSessaoUsuario()->id_usuario);
            }
            $pessoaDAO = new PessoaDAO();
            $usuarioORM = new UsuarioORM();
            $select = $usuarioORM->select()->distinct('id_entidade')
                ->from(
                    array('us' => 'vw_usuarioperfilpedagogico'),
                    array('id_entidade', 'st_nomeentidade', 'st_razaosocial', 'st_urlimglogo')
                )
                ->setIntegrityCheck(false)
                ->where('us.id_perfilpedagogico != ? ', PerfilPedagogicoTO::TITULAR_CERTIFICACAO)
                ->where('us.id_usuario = ? ', $usuarioTO->getId_usuario())->order('id_entidade');

            /*
             * Verifico se existe a varíavel de escopo e seto a consulta para aparecer apenas os perfis daquela entidade.
             * */
            if (getenv('id_entidade') != null) {
                $select->where('us.id_entidadeportal = ? ', getenv('id_entidade'));
            }

            $entidadesUsuario = $pessoaDAO->retornarVwUsuarioperfilPedagogico(new VwUsuarioPerfilPedagogicoTO(), $select)->toArray();

            if (empty($entidadesUsuario)) {
                THROW new Zend_Validate_Exception("Não foram encontrados os Perfis das Instituições do Usuário!");
            }
            $entidadesUsuario = Ead1_TO_Dinamico::encapsularTo($entidadesUsuario, new EntidadeTO());
            foreach ($entidadesUsuario as $index => $entidadeTO) {
                $negocio = new G2\Negocio\Negocio();
                $pj_entity = $negocio->find('G2\Entity\Entidade', $entidadeTO->getId_entidade());
                if ($pj_entity) {
                    $entidadesUsuario[$index]->str_urlimglogoentidade = $pj_entity->getStr_urlimglogoentidade();
                }
                $select = $usuarioORM->select()->distinct('id_usuario')
                    ->from(
                        array('us' => 'vw_usuarioperfilpedagogico'),
                        array(
                            'id_usuario', 'st_login', 'st_nomeexibicao',
                            'id_entidade', 'st_nomeentidade', 'st_razaosocial',
                            'st_nomeperfil', 'id_perfil', 'st_perfilpedagogico',
                            'id_perfilpedagogico', 'id_matricula', 'id_projetopedagogico',
                            'st_tituloexibicao', 'st_perfilpedagogico')
                    )
                    ->setIntegrityCheck(false)
                    ->where('us.id_usuario = ? ', $usuarioTO->getId_usuario())
                    ->where('(us.id_perfilpedagogico <> ' . PerfilPedagogicoTO::ALUNO . ' or us.id_projetopedagogico is not null)')
                    ->where('us.id_perfilpedagogico != ? ', PerfilPedagogicoTO::TITULAR_CERTIFICACAO)
                    ->where('us.id_entidade = ?', $entidadeTO->getId_entidade())->order('id_matricula DESC');

                $perfis = $pessoaDAO->retornarUsuarios(new UsuarioTO(), $select)->toArray();
                $perfis = Ead1_TO_Dinamico::encapsularTo($perfis, new VwUsuarioPerfilPedagogicoTO());

                foreach ($perfis as $perfil) {
                    $entidadesUsuario[$index]->perfis[$perfil->getId_perfilpedagogico()]['st_perfilpedagogico'] = $perfil->getSt_perfilpedagogico();
                    if ($perfil->getId_perfilpedagogico() == PerfilPedagogicoTO::ALUNO) {
                        $entidadesUsuario[$index]->perfis[$perfil->getId_perfilpedagogico()]['id_perfil'] = 0;
                    } else {
                        $entidadesUsuario[$index]->perfis[$perfil->getId_perfilpedagogico()]['id_perfil'] = $perfil->getId_perfil();
                    }
                    $entidadesUsuario[$index]->perfis[$perfil->getId_perfilpedagogico()]['perfis'][] = $perfil;
                }
            }
            $this->mensageiro->setMensageiro($entidadesUsuario, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar os Perfis do Usuário!", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna o perfil de acesso do usuario
     * @param VwUsuarioPerfilPedagogicoTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarUsuarioPerfilAcesso(VwUsuarioPerfilPedagogicoTO $to)
    {
        try {
            $this->validaPerfilAcesso($to);
            $usuarioORM = new UsuarioORM();
            $select = $usuarioORM->select()
                ->from(array('us' => 'vw_usuarioperfilpedagogico')
                    , '*')
                ->setIntegrityCheck(false)
                ->where('us.id_usuario = ? ', $to->getId_usuario())
                ->where('us.id_entidade = ? ', $to->getId_entidade())
                ->where('us.id_perfil = ?', $to->getId_perfil())
                ->where('us.id_perfilpedagogico = ?', $to->getId_perfilpedagogico());
            if ($to->getId_perfilpedagogico() == PerfilPedagogicoTO::ALUNO || $to->getId_perfilpedagogico() == PerfilPedagogicoTO::ALUNO_INSTITUCIONAL) {
                $select
                    ->where('us.id_projetopedagogico = ?', $to->getId_projetopedagogico())
                    ->where('us.id_matricula = ?', $to->getId_matricula())
                    ->where('us.id_projetopedagogico is not null');
            }
            $pessoaDAO = new PessoaDAO();
            $dados = $pessoaDAO->retornarVwUsuarioperfilPedagogico(new VwUsuarioPerfilPedagogicoTO(), $select)->toArray();
            if (empty($dados)) {
                THROW new Zend_Validate_Exception("Perfil Não Encontrado!");
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwUsuarioPerfilPedagogicoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Verificar Perfil do Usuário!", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Verifica e retorna os dados para Redefinição de senha
     * @param UsuarioTO $usTO
     * @param int $id_usuario , int $id_entidade
     * @return Ead1_Mensageiro
     */
    public function retornaDadosRedefinicaoSenha($id_usuario, $id_entidade)
    {


        try {

            $usTO = new UsuarioTO();
            $usTO->setId_usuario($id_usuario);
            $usTO->fetch(true, true, true);

            $enTO = new EntidadeTO();
            $enTO->setId_entidade($id_entidade);
            $enTO->fetch(true, true, true);

            if (!$usTO->getBl_ativo()) {
                throw new Zend_Exception('Usuário não encontrado ou Inativo');
            }
            if (!$enTO->getBl_ativo()) {
                throw new Zend_Exception('Entidade não encontrada ou Inativa');
            }

            if ($usTO->getbl_redefinicaosenha()) {
                throw new  Zend_Validate_Exception('Usuário já redefiniu sua senha');
            }

            return new Ead1_Mensageiro(array($usTO, $enTO), Ead1_IMensageiro::SUCESSO);

        } catch (Zend_Validate_Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }


    }


    /**
     * Salva a redefinição de senha, caso seja necessário a troca da senha mesmo com o bl_redefinicaosenha true,
     * envie esse parametro como false.
     * 
     * @param $id_usuario
     * @param $id_entidade
     * @param $st_senha_nova
     * @param $st_senha_nova_confirma
     * @param bool $bl_redefinicaosenha
     * @return Ead1_Mensageiro
     */
    public function salvarRedefinicaoSenha($id_usuario, $id_entidade, $st_senha_nova, $st_senha_nova_confirma, $bl_redefinicaosenha = true)
    {
        try {
            if ($bl_redefinicaosenha) {
                $meDef = $this->retornaDadosRedefinicaoSenha($id_usuario, $id_entidade);
                if ($meDef->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception($meDef->getFirstMensagem());
                }
                $uTO = $meDef->getFirstMensagem();
                $uTO->setBl_redefinicaosenha(true);
            } else {
                $uTO = new UsuarioTO();
                $uTO->setId_usuario($id_usuario);
            }

            $novosUsuarioSenha = new stdClass();
            $novosUsuarioSenha->st_senha_nova = $st_senha_nova;
            $novosUsuarioSenha->st_senha_nova_confirma = $st_senha_nova_confirma;

            $peBO = new PessoaBO();
            return $peBO->alterarUsuarioSenha($uTO, $novosUsuarioSenha, $id_entidade);
        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Chama o serviço do G2 para recuperar senha do usuário
     * @param VwPessoaTO $params
     * @return Ead1_Mensageiro
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     */
    public function recuperarSenha(VwPessoaTO $to)
    {
        $mensageiro = new Ead1_Mensageiro(); //instancia o mensageiro
        try {
            //verifica se foi passado o id da entidade na to
            if (!$to->getId_entidade())
                //verifica se existe uma variavel de ambiente chamada id_entidade setada e seta o valor na to
                $to->setId_entidade(getenv("id_entidade") ? getenv("id_entidade") : NULL);
            //chama a BO de login
            $loginBo = new LoginBO();
            return $loginBo->recuperarSenhaPessoa($to); //passa a to para o método da bo e retorna

        } catch (Exception $e) {
            return $mensageiro->setMensageiro("Erro ao tentar recuperar senha. " . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

}
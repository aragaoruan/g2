<?php

/**
 * Classe com regras de negócio para Minha Pasta
 */
class PortalMinhaPastaBO extends Ead1_BO
{

    private $primeiroAcessoBO = null;

    public function __construct()
    {
        parent::__construct();
        $this->primeiroAcessoBO = new PrimeiroAcessoBO();
        $this->mensageiro = new Ead1_Mensageiro();
    }

    public function criarMinhaPasta($id_venda)
    {

        if (!$id_venda) {
            $id_venda = $this->primeiroAcessoBO->retornarProgresso()->getId_venda();
        }

        $pasta = realpath(APPLICATION_PATH . '/../public/upload/minhapasta/') . '/' . $id_venda;
        if (!is_dir($pasta)) {
            mkdir($pasta, 0777, true);
        }
    }

  /**
   * Alterações efetuadas para retornar arquivos quando uma matricula tem mais de uma venda
   *
   * Novas implementaçoes pra usar esse metodo no portal:
   * 1º Para usar esse metodo da API-V2 e necessario envar o id_matricula, pois nao existe sessao
   * 2º Deve-se enviar o parametro $api como true, pois ele vai direcionar para pasta correta, assim ele vai
   *    encontrar o caminho do link simbolico para ler a pasta
   *
   * @param null $id_venda
   * @param $listaArquivosBanco
   * @param null $id_matricula
   * @param bool $api
   * @return array
   * @throws Zend_Exception
   */
    public function retornarAquivosMinhaPasta($id_venda = null, $listaArquivosBanco, $id_matricula = null, $api = false)
    {
        $arrVendas = array();
        if (!$id_venda) {
            $negocioVP = new \G2\Negocio\Negocio();
            //caso o id matricula for enviado, diferente de null, ele vai considerar o enviado e ignorar a sessao
            $matricula = (!isset($id_matricula)) ? Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula : $id_matricula;
            $arrVP = $negocioVP->findBy('\G2\Entity\VendaProduto', array('id_matricula' => $matricula));

            if ($arrVP) {
                foreach ($arrVP as $value) {
                    array_push($arrVendas, $value->getId_venda()->getId_venda());
                }
            }
        } else {
            array_push($arrVendas, $id_venda);
        }

        $listaArquivos = array();
        foreach ($arrVendas as $id_venda) {

            $this->criarMinhaPasta($id_venda);

            $dir = realpath(APPLICATION_PATH . '/../public/upload/minhapasta/') . '/' . $id_venda;

            if(is_dir($dir)){
                $d = opendir($dir);
                $i = 0;

                $nome = readdir($d);


                $arquivos = array();
                while ($nome !== false) {
                    if (!is_dir($nome) and ($nome != 'Thumbs.db')) {
                        $arquivos[$i] = $nome;
                        $i++;
                    }
                    $nome = readdir($d);
                }


              /**
               * verifica se os arquivos que esta salvo somente no servidor não são os mesmos
               * salvos no banco, caso seja remove da listagem do servidor
               */
              foreach ($arquivos as $key => $item) {
                  foreach ($listaArquivosBanco as $itemAB) {
                      if ($itemAB['st_contratoUrl'] == $item) {
                          unset($arquivos[$key]);
                      }
                  }
              }


              if (count($arquivos) > 0) {
                  foreach ($arquivos as $key => $arq) {
                      $arrayNome = explode('.', $arq);
                      $imgSrc = $arrayNome[(count($arrayNome) - 1)];

                      $arquivoTO = new ArquivoTO();
                      $arquivoTO->setSt_extensaoarquivo($arrayNome[(count($arrayNome) - 1)]);
                    if($api){
                      $listaArquivos[$key]['url'] = "upload/minhapasta/$id_venda/$arq";
                      $listaArquivos[$key]['st_contrato'] = str_replace('_',' ' ,$arq );
                    }else{
                      $str = "<li>
                                          <a href=\"/upload/minhapasta/$id_venda/$arq\" target=\"_blank\">
                                              $arq
                                          </a>
                                     </li>";
                      $listaArquivos[$arq] = $str;
                    }
                  }
              }
            }
        }
        return $listaArquivos;
    }

}

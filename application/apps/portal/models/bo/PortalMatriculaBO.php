<?php

/**
 * Classe para interações de matrícula
 * @author edermariano
 *
 */
class PortalMatriculaBO extends MatriculaBO
{

    /**
     * Método Construtor
     */
    public function __construct()
    {
        parent::__construct();
        $this->mensageiro = new Ead1_Mensageiro();
    }

    /**
     * Método que retorna as grades da matrícula da sessão
     * @param VwMatriculaGradeSalaTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarGrade(VwMatriculaGradeSalaTO $to)
    {
        try {
            if (!$to->getId_matricula()) {
                THROW new Zend_Validate_Exception("Matrícula Não Encontrada!");
            }
            $matriculaBO = new MatriculaBO();
            $this->mensageiro = $matriculaBO->retornarVwMatriculaGradeSala($to);
            $this->mensageiro->subtractMensageiro();
            $arDados = array();
            $arDadosTratado = array();
            $dadosRO = $this->mensageiro->getMensagem();
            foreach ($dadosRO as $chave => $disciplinas) {
                foreach ($dadosRO[$chave]["disciplinas"] as $chaveDisciplina => $valor) {
                    $arDados[$chave]['st_modulo'] = ($valor->st_moduloexibicao ? $valor->st_moduloexibicao : $valor->st_modulo);
                    $valor->st_disciplina = ($valor->st_disciplinaexibicao ? $valor->st_disciplinaexibicao : $valor->st_disciplina);
                    $valor->nu_nota = (int) $valor->nu_notafinal;
                    $valor->dt_encerramento = ($valor->dt_encerramento ? $valor->dt_encerramento->toString('dd/MM/YYYY') : '-');
                    $valor->dt_abertura = ($valor->dt_abertura ? $valor->dt_abertura->toString('dd/MM/YYYY') : '-');
                    $valor->st_professortitular = ($valor->st_professortitular ? $valor->st_professortitular : 'A definir');
                    $arDados[$chave]['dados'][] = $valor;
                    $curso = $valor->st_projetopedagogico;
                    $coordenador = $valor->st_coordenadorprojeto;
                }
            }
            if (!empty($arDados)) {
                foreach ($arDados as $idModulo => $dadosAr) {
                    $arDadosTratado[$idModulo]['st_modulo'] = $dadosAr['st_modulo'];
                    $arDadosTratado[$idModulo]['dados'] = Zend_Json::encode($dadosAr['dados']);
                }
                $arDadosTratado['curso'] = $curso;
                $arDadosTratado['coordenador'] = $coordenador;
            } else {
                THROW new Zend_Validate_Exception("Nenhuma Grade Disponível.");
            }
            $this->mensageiro->setMensageiro($arDadosTratado, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar Grade!", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna as grades da matrícula da sessão por tipo
     * @param VwGradeTipoTO $vwGradeTipoTO
     * @return Ead1_Mensageiro
     */
    public function retornarGradePorTipo(VwGradeTipoTO $vwGradeTipoTO)
    {
        try {
            if (!$vwGradeTipoTO->getId_matricula()) {
                THROW new Zend_Validate_Exception("Matrícula Não Encontrada!");
            }
            $matriculaRO = new MatriculaRO();
            $this->mensageiro = $matriculaRO->retornarVwGradeTipo($vwGradeTipoTO);
            $this->mensageiro->subtractMensageiro();
            $arDados = array();
            $arDadosTratado = array();
            $dadosRO = $this->mensageiro->getMensagem();
            foreach ($dadosRO as $chaveModulo => $disciplinas) {
                foreach ($disciplinas["disciplinas"] as $valor) {
                    $vwAux = $disciplinas['modulos'];
                    $arDados[$chaveModulo]['st_modulo'] = $vwAux->st_modulo;
                    $arDados[$chaveModulo]['dados'][] = $valor;
                }
                $arDados['tiposAvaliacao'] = $disciplinas['tiposAvaliacao'];
            }
            if (empty($arDados)) {
                THROW new Zend_Validate_Exception("Nenhuma Grade Disponível.");
            }
            $this->mensageiro->setMensageiro($arDados, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar Grade!", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }


    /**
     * Método que retorna as grades da matrícula da sessão por tipopara os alunos (sem tcc caso nao tenha nota ou conversando)
     * @param VwGradeTipoTccTO $vwGradeTipoTO
     * @return Ead1_Mensageiro
     */
    public function retornarGradePorTipoTcc(VwGradeTipoTccTO $vwGradeTipoTO)
    {
        try {
            if (!$vwGradeTipoTO->getId_matricula()) {
                THROW new Zend_Validate_Exception("Matrícula Não Encontrada!");
            }
            $dados = array();
            $arrTipoAvaliacao = array();
            $dadosModulos = $this->dao->retornarVwGradeTipoModuloTcc($vwGradeTipoTO)->toArray();

            if (empty($dadosModulos)) {
                return $this->mensageiro->setMensageiro(null, Ead1_IMensageiro::SUCESSO);
            }
            $dadosModulos = Ead1_TO_Dinamico::encapsularTo($dadosModulos, new VwGradeTipoTO());

            foreach ($dadosModulos as $indexModulo => $modulo) {
                $vwGradeTipoTO->setId_modulo($modulo->getId_modulo());
                $dados[$indexModulo]['modulos'] = $modulo;
                $dadosDisciplinas = $this->dao->retornarVwGradeTipoTcc($vwGradeTipoTO);
                $dadosDisciplinas = Ead1_TO_Dinamico::encapsularTo($dadosDisciplinas, new VwGradeTipoTccTO());

                foreach ($dadosDisciplinas as $indexDisciplina => $disciplina) {
                    if (empty($arrTipoAvaliacao[$disciplina->getId_tipoavaliacao()]) && $disciplina->getSt_labeltipo() != null) {
                        $arrTipoAvaliacao[$disciplina->getId_tipoavaliacao()] = $disciplina->getSt_labeltipo();
                    }
                    $dados[$indexModulo]['disciplinas'][$disciplina->getId_disciplina()][$disciplina->getId_tipoavaliacao()][] = $disciplina;
                }

                $dados[$indexModulo]['tiposAvaliacao'] = $arrTipoAvaliacao;
            }

            $arDados = array();
            $dadosRO = $dados;
            foreach ($dadosRO as $chaveModulo => $disciplinas) {
                foreach ($disciplinas["disciplinas"] as $valor) {
                    $vwAux = $disciplinas['modulos'];
                    $arDados[$chaveModulo]['st_modulo'] = $vwAux->st_modulo;
                    $arDados[$chaveModulo]['dados'][] = $valor;
                }
                $arDados['tiposAvaliacao'] = $disciplinas['tiposAvaliacao'];
            }
            if (empty($arDados)) {
                THROW new Zend_Validate_Exception("Nenhuma Grade Disponível.");
            }
            $this->mensageiro->setMensageiro($arDados, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar Grade!", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }
}
<?php
/**
 * Classe com regras de negócio para alocação
 * @author eduardoromao
 */
class PortalAlocacaoBO extends Ead1_BO{
	
	
	public function __construct(){
		parent::__construct();
		$this->mensageiro = new Ead1_Mensageiro();
	}
	
	/**
	 * Método que busca os dados de disciplinas para alocação
	 * @param MatriculaTO $to
	 * @return Ead1_Mensageiro
	 */
	public function dadosAlocacao(MatriculaTO $to){
		try{
			$vwDadosAlocacao = new VwDadosAlocacaoORM();
			$dado = $vwDadosAlocacao->fetchRow("id_matricula = ".$to->getId_matricula());
			if(!$dado){
				THROW new Zend_Validate_Exception("Nenhum Dado de Alocação Encontrado.");
			}
			$vwDadosAlocacaoTO = new VwDadosAlocacaoTO($dado->toArray());
			$matriculaBO = new MatriculaBO();
			$pendencias = $matriculaBO->verificaPendenciasMatricula($to, true);
			
			$atributo = ($pendencias ? "nu_disciplinassimultaneaspen" : "nu_disciplinassimultaneasmai");
			if($vwDadosAlocacaoTO->$atributo == 0){
				$dado->$atributo = '∞';	
			}
			$vwDadosAlocacaoTO->nu_disciplinasdisponiveis = $vwDadosAlocacaoTO->$atributo;
			if($vwDadosAlocacaoTO->nu_disciplinasdisponiveis != '∞' ){
				$sub = ($vwDadosAlocacaoTO->nu_disciplinasdisponiveis - $vwDadosAlocacaoTO->getNu_disciplinasalocadas());
				$disponiveis = ($sub < 0 ? 0 : $sub);
			}else{
				$disponiveis = '∞';					
			}
			$vwDadosAlocacaoTO->nu_disciplinasdisponiveis = $disponiveis;
			$this->mensageiro->setMensageiro($vwDadosAlocacaoTO,Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Validate_Exception $e){
			$this->mensageiro->setMensageiro($e->getMessage(),Ead1_IMensageiro::AVISO);	
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro("Erro ao Retornar Dados de Alocação.",Ead1_IMensageiro::ERRO,$e->getMessage());	
		}
		return $this->mensageiro;
	}
	
	/**
	 * Método que retorna o combo de módulos
	 * @param ModuloTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarComboModulo(ModuloTO $to){
		try{
			$combo		= array();
			$ro = new ProjetoPedagogicoRO();
			$mensageiro = $ro->retornarModulo($to);
			$mensageiro->subtractMensageiro();
			$dadosMensagem = $mensageiro->getMensagem();
			foreach($dadosMensagem as $obj){
				$combo[$obj->id_modulo]['label'] = ($obj->st_tituloexibicao ? $obj->st_tituloexibicao : $obj->st_modulo); 
				$combo[$obj->id_modulo]['value'] = $obj->id_modulo; 
			}
			$this->mensageiro->setMensageiro($combo,Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Validate_Exception $e){
			$this->mensageiro->setMensageiro($e->getMessage(),Ead1_IMensageiro::AVISO);
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro("Erro ao Retornar Módulos.",Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Método que retorna os dados de disciplina por módulo
	 * @param ModuloTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarDisciplinasPorModulo(ModuloTO $to){
		if($to->getId_modulo()){
			$matriculaTO = new MatriculaTO();
			$matriculaTO->setId_matricula(Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula);
			
			$matriculaNivelSerieTO = new MatriculaNivelSerieTO();
			$matriculaNivelSerieTO->setId_matricula($matriculaTO->getId_matricula());
			
			$matriculaNivelSerieORM = new MatriculaNivelSerieORM();
			$matriculasNivelSerie = $matriculaNivelSerieORM->consulta($matriculaNivelSerieTO);
			
			if($matriculasNivelSerie){
				foreach($matriculasNivelSerie as $matriculaNivelSerie){
					$vwModuloDisciplinaProjetoTrilhaTO = new VwModuloDisciplinaProjetoTrilhaTO();
					$vwModuloDisciplinaProjetoTrilhaTO->setId_trilha(Ead1_Sessao::getSessaoProjetoPedagogico()->id_trilha);
					$vwModuloDisciplinaProjetoTrilhaTO->setId_modulo($to->getId_modulo());
					$vwModuloDisciplinaProjetoTrilhaTO->setBl_ativo(true);
					$vwModuloDisciplinaProjetoTrilhaTO->setId_serie($matriculaNivelSerie->getId_serie());
					$vwModuloDisciplinaProjetoTrilhaTO->setId_nivelensino($matriculaNivelSerie->getId_nivelensino());
				}
			}else{
					$vwModuloDisciplinaProjetoTrilhaTO = new VwModuloDisciplinaProjetoTrilhaTO();
					$vwModuloDisciplinaProjetoTrilhaTO->setId_trilha(Ead1_Sessao::getSessaoProjetoPedagogico()->id_trilha);
					$vwModuloDisciplinaProjetoTrilhaTO->setId_modulo($to->getId_modulo());
					$vwModuloDisciplinaProjetoTrilhaTO->setBl_ativo(true);
			}
			
			$ro = new ProjetoPedagogicoRO();
			return $ro->retornarModuloDisciplinaStatus($vwModuloDisciplinaProjetoTrilhaTO, $matriculaTO);
		}
	}
	
	/**
	 * Método que retorna os dados de sala de aula por disciplina da matrícula em questão
	 * @param MatriculaDisciplinaTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarSalaDeAulaDisciplinaProjeto(MatriculaDisciplinaTO $to){
		$vwSalaDisciplinaProjetoTO = new VwSalaDisciplinaProjetoTO();
		$vwSalaDisciplinaProjetoTO->setId_matricula($to->getId_matricula());
		$vwSalaDisciplinaProjetoTO->setId_disciplina($to->getId_disciplina());
		
		$ro = new SalaDeAulaRO();
		return $ro->retornarVwSalaDisciplinaProjeto($vwSalaDisciplinaProjetoTO);
	}
	
	/**
	 * Método que aloca o aluno em questão na sala de aula
	 * @param array $dados
	 * @return Ead1_Mensageiro
	 */
	public function alocar($dados){
		try{
			$matriculaTO = new MatriculaTO();
			$matriculaTO->setId_matricula(Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula);
			if(Ead1_Sessao::getSessaoProjetoPedagogico()->bl_bloqueiadisciplina){
				$tbMatriculaDisciplinaORM = new MatriculaDisciplinaORM();
				$select = $tbMatriculaDisciplinaORM->select()->from($tbMatriculaDisciplinaORM->_name, 'count(id_evolucao) AS nu_naoiniciada')
					->where('id_evolucao = ?', 11)->where('id_matricula = ?', $matriculaTO->getId_matricula());
				$result = $tbMatriculaDisciplinaORM->fetchRow($select);
				if(isset($result->nu_naoiniciada) && $result->nu_naoiniciada == 1){
					$matriculaBO = new MatriculaBO();
					$pendencias = $matriculaBO->verificaPendenciasMatricula($matriculaTO, true, true);
					if($pendencias){
						THROW new Zend_Validate_Exception("Verifique suas pendências junto à secretaria.");
					}
				}
			}
			$mensageiroAlocacao = $this->dadosAlocacao($matriculaTO);
			$mensageiroAlocacao->subtractMensageiro();
			$vwDadosAlocacaoTO = $mensageiroAlocacao->getFirstMensagem();
			if(($vwDadosAlocacaoTO->nu_disciplinasdisponiveis == '∞') || ($vwDadosAlocacaoTO->nu_disciplinasdisponiveis > 0)){
				$alocacaoTO = new AlocacaoTO();
				$alocacaoTO->setId_saladeaula($dados['id_saladeaula']);
				$alocacaoTO->setId_matriculadisciplina($dados["id_matriculadisciplina"]);
				$ro = new SalaDeAulaRO();
				$mensageiroAlocacao = $ro->salvarAlocacaoAluno($alocacaoTO);
				$mensageiroAlocacao->subtractMensageiro();
				$this->mensageiro = $mensageiroAlocacao;
			}else{
				THROW new Zend_Exception("Não é possível alocar-se! Você está cursando o máximo de disciplinas possíveis!");
			}
		}catch (Zend_Validate_Exception $e){
			$this->mensageiro->setMensageiro($e->getMessage(),Ead1_IMensageiro::AVISO);
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro("Erro ao Alocar Aluno!",Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Método que verifica idade do aluno
	 */
	public function maiorDe18(){
		$dataNascimento = new Zend_Date($this->sessao->dt_nascimento);
		$dataAtual = new Zend_Date();
		$diferencaAnos = ($dataAtual->get("Y") - $dataNascimento->get("Y"));
		if($diferencaAnos >= 18){
			if($diferencaAnos ==  18){
				if($dataNascimento->get("M") > $dataAtual->get("M")){
					return true;
				}else if($dataNascimento->get("M") == $dataAtual->get("M")){
					if($dataNascimento->get("D") < $dataAtual->get("D")){
						return false;
					}else{
						return true;
					}
				}else{
					return false;
				}
			}
		}else{
			return false;
		}
	}
}
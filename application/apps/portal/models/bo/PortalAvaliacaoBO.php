<?php

class PortalAvaliacaoBO extends Ead1_BO {

    public function __construct() {
        $this->mensageiro = new Ead1_Mensageiro();
    }

    /**
     * Metodo que verifica se já existe uma avaliacao agendada para o mesmo dia
     * @param VwAvaliacaoAplicacaoAgendamentoTO $to
     * @return Boolean
     */
    public function verificaDataAgendamento(VwAvaliacaoAplicacaoAgendamentoTO $to) {
        $avaliacaoDAO = new AvaliacaoDAO();
        $arrDadosVerifica = $avaliacaoDAO->retornarVwAvaliacaoAplicacaoAgendamento(
                        new VwAvaliacaoAplicacaoAgendamentoTO(
                        array('id_matricula' => $to->getId_matricula(),
                    'id_avaliacao' => $to->getId_avaliacao(),
                    'dt_agendamento' => $to->getDt_agendamento())
                        )
                )->toArray();
        return (empty($arrDadosVerifica) ? true : false);
    }

    /**
     * Retorna os módulos em que o aluno
     * possuis disciplinas na situação de cursando
     * 
     * @param MatriculaTO $to 
     * @return Ead1_Mensageiro
     */
    public function retornarModulosDisciplinaCursandoMatricula(MatriculaTO $to) {
        try {
            $avaliacaoDAO = new AvaliacaoDAO();
            $dados = $avaliacaoDAO->retornarModulosDisciplinaCursandoMatricula($to)->toArray();
            if (count($dados) < 1) {
                THROW new Zend_Validate_Exception('Para a matrícula informada não foram encontrado módulos vinculados a disciplina cursando.');
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new ModuloTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao listar os Módulos!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    public function retornaSituacaoProvaMatricula($id) {
        try {
            $negocio = new G2\Negocio\GerenciaProva();
            $param = array();
            $param['id_matricula'] = $id;

            $result = $negocio->findByVwAlunosAgendamento($param);

            if (is_array($result) && isset($result[0])) {
                return $result[0]->getId_situacaoagendamento();
            }
        } catch (Zend_Validate_Exception $e) {
            return  $this->mensageiro->setMensageiro('Erro ao listar situação do agendamento!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }

    }

    public function retornaMostraBoxAgendamento($id_matricula, $id_entidade) {
        try {
            $negocioGerencia = new G2\Negocio\GerenciaProva();
            $params = array();
            $params['id_matricula'] = $id_matricula;

            $retorno = false;

            $apto = $this->retornaSituacaoProvaMatricula($id_matricula);
            if ($apto == (int)G2\Entity\Matricula::APTO_FINAL) {
                $result = $negocioGerencia->findByVwAvaliacaoAgendamento($params);

                if ($result) {
                    foreach ($result as $value) {
                        // Verificar se existe agendamento ativo
                        if ($value->getBl_ativo()) {
                            $situacoes_liberar = array($value->getId_situacao() == G2\Entity\AvaliacaoAgendamento::APTO, G2\Entity\AvaliacaoAgendamento::LIBERADO, $value->getId_situacao() == G2\Entity\AvaliacaoAgendamento::ABONADO);
                            // Se a situacao situacao do resultado é alguma situacao permitida para o agendamento,
                            // entao retorna A AVALIACAO em questao (truthy)
                            if (in_array($value->getId_situacao(), $situacoes_liberar)) {
                                return $value;
                            }
                        }
                    }
                } else {
                    // Se for apto e não tiver nenhum agendamento retorna true
                    // pois o primeiro agendamento não precisa de liberação
                    return true;
                }
            }

            return $retorno;
        } catch (Zend_Validate_Exception $e) {
            THROW new Zend_Validate_Exception('Erro ao buscar Matrícula');
        }
    }

    public function retornaMostraMenuAgendamento($id_matricula) {
        try {
            $negocioGerencia = new G2\Negocio\GerenciaProva();
            $params = array();
            $params['id_matricula'] = $id_matricula;
            $retorno = false;


            $apto = $this->retornaSituacaoProvaMatricula($id_matricula);
            if ($apto == G2\Entity\Matricula::APTO_FINAL) {
                $result = $negocioGerencia->findByVwAvaliacaoAgendamento($params);

                if ($result) {
                    foreach ($result as $value) {
                        if ($value->getBl_ativo()) { //verifica se existe agendamento ativo
                            if ($value->getId_situacao() != G2\Entity\AvaliacaoAgendamento::CANCELADO && $value->getId_situacao() != G2\Entity\AvaliacaoAgendamento::REAGENDADO) {
                                if ($value->getId_situacao() == G2\Entity\AvaliacaoAgendamento::AGENDADO &&($value->getId_situacaopresenca()!=1 || $value->getId_situacaopresenca()!=2)) {
                                    return true; //caso tenha um agendamento ativo e com data superior a atual,
                                    //o aluno pode visualizar a opção de agendamento para reagendar ou modificar algum dado
                                } else {
                                    if ($value->getId_situacao() != G2\Entity\AvaliacaoAgendamento::AGENDADO) {
                                        return true; //nesse caso seria liberado ou abonado
                                    }
                                }
                            }
                        }
                    }
                } else {
                    return true;
                }
            }
            /*Comentado porque alguns alunos estão acessando o portal novo antes da data e conseguindo agendar a prova
            e a funcionalidade não foi validada ainda.
            View Atualizada em produção
            29-07-2014 AC-26239 */
            //return false;
            return $retorno;  //voltando para validação do agendamento - FINALMENTE - 25/09/2014
            //se naõ for apto, não pode fazer agendamento
        } catch (Zend_Validate_Exception $e) {
            THROW new Zend_Validate_Exception('Erro ao buscar Matrícula');
        }
    }

}

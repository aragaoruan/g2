<?php

/**
 * Classe que encapsula regras de negócio do menu do portal
 * @author eduardoromao
 */
class PortalMenuBO extends Ead1_BO
{

    private $_dao;

    /**
     * Método Construtor
     */
    public function __construct()
    {
        $this->mensageiro = new Ead1_Mensageiro();
        $this->_dao = new MenuDAO();
    }

    /**
     * Método que traz o menu do topo
     * @return Ead1_Mensageiro
     */
    public function retornarMenuTopo()
    {
        try {
            $retorno = array();
            $botaoTccVisivel = false;
            if (Ead1_Sessao::getSessaoPerfil()->id_perfil && Ead1_Sessao::getSessaoEntidade()->id_entidade) {
                $dados = $this->_dao->mostrarMenuPorPerfilEntidadeTodos(Ead1_Sessao::getSessaoPerfil()->id_perfil, Ead1_Sessao::getSessaoEntidade()->id_entidade, SistemaTO::PORTAL_ALUNO);
                if (empty($dados)) {
                    THROW new Zend_Validate_Exception("Não foi Encontrada Nenhuma Funcionalidade para este Perfil!");
                }

                //caso o aluno não esteja com o envio de tcc liberado para o professor, o botão de enviar tcc é escondido no menu do portal.
                //caso o aluno não esteja apto para prova presencial ou com prova agendada o botão de agendar prova não aparece no portal.

                if (Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula && Ead1_Sessao::getSessaoPerfil()->is_aluno()) {
                    $mostrarTcc = $this->_dao->verificaSituacaoTcc(Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula, Ead1_Sessao::getSessaoEntidade()->id_entidade);
                    if (empty($mostrarTcc) || $mostrarTcc[0]['id_situacaotcc'] == AlocacaoTO::SITUACAOTCC_NAOAPTO) {
                        for ($i = 0; $i < count($dados); $i++) {
                            if ($dados[$i]['id_funcionalidade'] == 567) {
                                unset($dados[$i]);
                            }
                        }
                    } else {
                        Ead1_Sessao::setSessionMenu(array('bl_tcc' => '1'));
                    }
//                    $situacaoProvaPresencial = $this->_dao->verificaSituacaoProvaPresencial(Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula);
                    $situacaoProvaPresencial = false;
                    if ($situacaoProvaPresencial === false) {
                        sort($dados);
                        for ($i = 0; $i < count($dados); $i++) {
                            if ($dados[$i]['id_funcionalidade'] == 636) {
                                unset($dados[$i]);
                            }
                        }
                    }
                } else {
                    sort($dados);
                    for ($i = 0; $i < count($dados); $i++) {
                        if ($dados[$i]['id_funcionalidade'] == 636) {
                            unset($dados[$i]);
                        }
                    }
                }
                $funcionalidades = Ead1_TO_Dinamico::encapsularTo($dados, new VwPerfilEntidadeFuncionalidadeTO);

                foreach ($funcionalidades as $vwPerfilEntidadeFuncionalidadeTO) {

                    //@todo verificar se esta correto isso!
                    if ($vwPerfilEntidadeFuncionalidadeTO->getBl_visivel() && $vwPerfilEntidadeFuncionalidadeTO->getBl_ativo() && !$vwPerfilEntidadeFuncionalidadeTO->getId_funcionalidadepai()) {
                        $retorno = $this->menuRecursivo($funcionalidades, $vwPerfilEntidadeFuncionalidadeTO->getId_funcionalidadepai());
                        break;
                    }
                }
            } else {
                $vwPerfilEntidadeFuncionalidadeTO = new VwPerfilEntidadeFuncionalidadeTO();
                $vwPerfilEntidadeFuncionalidadeTO->setSt_urlicone("/imagens/icones/principal.png");
                $vwPerfilEntidadeFuncionalidadeTO->setSt_classeflex("/login/selecionaperfil");
                $vwPerfilEntidadeFuncionalidadeTO->setSt_label("Selecione um perfil!");
                $retorno[0] = $this->encapsularItemMenu($vwPerfilEntidadeFuncionalidadeTO);
            }
            $this->mensageiro->setMensageiro($retorno, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar as Funcionalidades do Sistema!", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que monta o menu de forma recursiva
     * @param array $dados
     * @param int $id_funcionalidadepai
     */
    protected function menuRecursivo($dados, $id_funcionalidadepai)
    {
        if ($dados) {
            $dadosMenu = array();
            foreach ($dados as $vwPerfilEntidadeFuncionalidadeTO) {
                if ($vwPerfilEntidadeFuncionalidadeTO->getId_funcionalidadepai() == $id_funcionalidadepai) {
                    $filhos = $this->menuRecursivo($dados, $vwPerfilEntidadeFuncionalidadeTO->getId_funcionalidade());
                    $dadosMenu[] = $this->encapsularItemMenu($vwPerfilEntidadeFuncionalidadeTO, $filhos);
                }
            }
            return $dadosMenu;
        } else {
            return null;
        }
    }

    /**
     * Método que encasula para o formato necessário o item de menu
     * @param VwPerfilEntidadeFuncionalidadeTO $vwPerfilEntidadeFuncionalidadeTO
     * @param mixed $filhos
     * @return array
     */
    private function encapsularItemMenu(VwPerfilEntidadeFuncionalidadeTO $vwPerfilEntidadeFuncionalidadeTO, $filhos = null)
    {
        $itemMenu = array();
        $itemMenu["id"] = ($vwPerfilEntidadeFuncionalidadeTO->getId_funcionalidade() ? $vwPerfilEntidadeFuncionalidadeTO->getId_funcionalidade() : null);
        $itemMenu["id_pai"] = ($vwPerfilEntidadeFuncionalidadeTO->getId_funcionalidadepai() ? $vwPerfilEntidadeFuncionalidadeTO->getId_funcionalidadepai() : null);
        $itemMenu["img"] = $vwPerfilEntidadeFuncionalidadeTO->getSt_urlicone();
        $itemMenu["link"] = ($vwPerfilEntidadeFuncionalidadeTO->getSt_classeflex() ? $vwPerfilEntidadeFuncionalidadeTO->getSt_classeflex() : "#");
        $itemMenu["texto"] = $vwPerfilEntidadeFuncionalidadeTO->getSt_label() ? $vwPerfilEntidadeFuncionalidadeTO->getSt_label() : $vwPerfilEntidadeFuncionalidadeTO->getSt_funcionalidade();
        $itemMenu["type"] = $vwPerfilEntidadeFuncionalidadeTO->getSt_classeflexbotao();
        $itemMenu["target"] = $vwPerfilEntidadeFuncionalidadeTO->getSt_target();
        $itemMenu["filhos"] = (empty($filhos) ? null : $filhos);
        $itemMenu['bl_visivel'] = $vwPerfilEntidadeFuncionalidadeTO->getBl_visivel() ? true : false;
        $itemMenu['bl_funcionalidadevisivel'] = $vwPerfilEntidadeFuncionalidadeTO->getBl_funcionalidadevisivel() ? true : false;
        return $itemMenu;
    }

    public function setEnviarTcc()
    {
        $mostrarTcc = $this->_dao->verificaSituacaoTcc(Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula, Ead1_Sessao::getSessaoEntidade()->id_entidade);

        if ($mostrarTcc == null || $mostrarTcc[0]['id_situacaotcc'] == AlocacaoTO::SITUACAOTCC_NAOAPTO) {
            return false;
        } else {
            return true;
        }
    }
}

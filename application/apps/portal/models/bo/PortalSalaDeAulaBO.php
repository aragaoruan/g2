<?php

/**
 * Classe que encapsula dados de regra de negocio de sala de aula do portal
 * @author eduardoromao
 */
class PortalSalaDeAulaBO extends Ead1_BO
{

    public function __construct()
    {
        parent::__construct();
        $this->mensageiro = new Ead1_Mensageiro();
    }

    /**
     * Método que valida os parametros passados para a consulta de sala de aula
     * @param VwAlunoGradeIntegracaoTO $to
     * @throws Zend_Validate_Exception
     */
    protected function validaParametrosAlunoSalaDeAula(VwAlunoGradeIntegracaoTO $to)
    {
        if (!$to->getId_matricula()) {
            throw new Zend_Validate_Exception("Matrícula não Encontrada!");
        }
        if (!$to->getId_usuario()) {
            throw new Zend_Validate_Exception("Usuário não Encontrado!");
        }
        if (!$to->getId_projetopedagogico()) {
            throw new Zend_Validate_Exception("Projeto Pedagógico não Definido!");
        }
    }

    /**
     * Método que retorna a grade e sala de aula do Aluno
     * @param VwAlunoGradeIntegracaoTO $to
     * @param bool $isPosGraduacao
     * @return Ead1_Mensageiro
     */
    public function retornarAlunoSalaDeAula(VwAlunoGradeIntegracaoTO $to, $isPosGraduacao = false)
    {
        try {
            if (!$to->getId_usuario()) {
                $to->setId_usuario(Ead1_Sessao::getSessaoUsuario()->id_usuario);
            }
            $this->validaParametrosAlunoSalaDeAula($to);
            $matriculaDAO = new MatriculaDAO();
            $arrWhere = array();
            $arrWhere[] = 'id_matricula = ' . $to->getId_matricula();
            $arrWhere[] = 'id_usuario = ' . $to->getId_usuario();
            $arrWhere[] = 'id_projetopedagogico = ' . $to->getId_projetopedagogico();
            $where = implode(" AND ", $arrWhere);


            if ($isPosGraduacao) {
                $dados = $this->negocio->getRepository(\G2\Entity\VwAlunoGradeIntegracaoPos::class)
                    ->retornarVwAlunoGradeIntegracaoPos($arrWhere);
            } else {
                $dados = parent::resultToArrayIfObjectOrReturnEmptyArray(
                    $matriculaDAO->retornarVwAlunoGradeIntegracao($to, $where)
                );
            }

            $arrVwAlunoGradeIntegracaoTO = Ead1_TO_Dinamico::encapsularTo($dados, new VwAlunoGradeIntegracaoTO());
            $arrRetorno = array();
            foreach ($arrVwAlunoGradeIntegracaoTO as $row) {
                $idModulo = $row->getId_modulo();
                $idDisciplina = $row->getId_disciplina();
                $arrRetorno[$idModulo]['st_tituloexibicaomodulo'] = $row->getSt_tituloexibicaomodulo();
                $arrRetorno[$idModulo]['disciplinas'][$idDisciplina] = $row;
            }

            $this->mensageiro->setMensageiro($arrRetorno, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar a Grade!", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna a grade e sala de aula do Aluno
     * @param VwAlunoGradeIntegracaoTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarAlunoSalaDeAulaIntegracao(VwAlunoGradeIntegracaoTO $to)
    {
        try {
            if (!$to->getId_usuario()) {
                $to->setId_usuario(Ead1_Sessao::getSessaoUsuario()->id_usuario);
            }
            $orm = new VwAlunoGradeIntegracaoORM();
            $matriculaDAO = new MatriculaDAO();
            $arrWhere = array();
            ($to->getId_disciplina() ? $arrWhere[] = 'id_disciplina = ' . $to->getId_disciplina() : null);
            $arrWhere[] = 'id_usuario = ' . $to->getId_usuario();
            $arrWhere[] = 'id_projetopedagogico = ' . $to->getId_projetopedagogico();
            $arrWhere[] = 'id_matricula = ' . $to->getId_matricula();
            ($to->getId_categoriasala() ? $arrWhere[] = 'id_categoriasala = ' . $to->getId_categoriasala() : null);
            $arrWhere[] = ($to->getId_saladeaula() ? 'id_saladeaula = ' . $to->getId_saladeaula() : 'id_saladeaula is not null');
            $where = implode(" AND ", $arrWhere);
            $dados = parent::resultToArrayIfObjectOrReturnEmptyArray(
                $matriculaDAO->retornarVwAlunoGradeIntegracao($to, $where)
            );
            if (!empty($dados)) {
                $dadosRetorno = Ead1_TO_Dinamico::encapsularTo($dados, new VwAlunoGradeIntegracaoTO(), true);
                if ($dadosRetorno->getSt_integracao() == null && $dadosRetorno->getId_alocacao()) {
                    $alocacaoTO = new AlocacaoTO();
                    $alocacaoTO->setId_alocacao($dadosRetorno->getId_alocacao());
                    $salaBO = new SalaDeAulaBO();
                    $mensageiroReintegrar = $salaBO->reintegrarAlocacaoAluno($alocacaoTO);
                    if ($mensageiroReintegrar->getTipo() != Ead1_IMensageiro::SUCESSO) {
                        throw new Zend_Validate_Exception("Erro ao reintegrar aluno a sala de aula!!" . $mensageiroReintegrar->getFirstMensagem());
                    } else {
                        $arrWhere[] = 'st_integracao is not null';
                        $dados = $matriculaDAO->retornarVwAlunoGradeIntegracao($to, $where)->toArray();
                        $dadosRetorno = Ead1_TO_Dinamico::encapsularTo($dados, new VwAlunoGradeIntegracaoTO(), true);
                    }
                }
            } else {
                throw new Zend_Validate_Exception("Nenhuma Sala de Aula Encontrada!");
            }
            $this->mensageiro->setMensageiro($dadosRetorno, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar a Grade!" . $e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna as salas de aula do professor
     * @param VwSalaDeAulaProfessorIntegracaoTO $to
     * @return Ead1_Mensageiro
     * @deprecated 2015-08-09
     */
    public function retornarSalaDeAulaProfessor(VwSalaDeAulaProfessorIntegracaoTO $to, $order = null, $diff = null)
    {
        try {
            if ($order && is_string($order))
                $order = array($order => 'ASC');

            $where = array();

            foreach ($to as $attr => $val) {
                if ($val)
                    $where[$attr] = $val;
            }

            if ($diff and is_array($diff)) {
                foreach ($diff as $key => $dif) {
                    $where[$key] = $dif;
                }
            }

            $negocio = new \G2\Negocio\VwSaladeAulaIntegracao();
            $results = $negocio->getSalasProfessor($where, $order);

            $mensageiro = new \Ead1_Mensageiro();

            if ($results) {
                $mensageiro->setMensageiro($results, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro->setMensageiro(array("Nenhuma sala de aula encontrada"), \Ead1_IMensageiro::ERRO);
            }

            return $mensageiro;

        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Método que retorna as salas de aula do professor
     * @param VwSalaDeAulaProfessorIntegracaoTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarSalaDeAulaCoordenador
    (
        VwSalaDeAulaPerfilProjetoIntegracaoTO $to
        , $order = null
        , $count = null
        , $offset = null
        , $where = ''
    )
    {
        try {
            $salaBO = new SalaDeAulaBO();
            $to->setId_entidade(Ead1_Sessao::getSessaoEntidade()->id_entidade);
            $mensageiro = $salaBO->retornarVwSalaDeAulaPerfilProjetoIntegracao($to, $order, $count, $offset, $where);
            $mensageiro->subtractMensageiro();
            $arrSalas = array();
            $disAreaExistente = array();

            $entrar = Ead1_Portal_Funcionalidade::verificaAcesso(479); //479	Entrar
            $gestao = Ead1_Portal_Funcionalidade::verificaAcesso(480); //480	Gestão

            foreach ($mensageiro->getMensagem() as $to) {
                $to->lb_sala = '';
                if ($entrar) {
                    $to->lb_sala .= '<label class="salaSelected" id="' . $to->getId_saladeaula() . '-' . $to->getId_disciplina() . '">Entrar</label>';
                }

                if ($gestao) {
                    $to->lb_sala .= '&nbsp;&nbsp; | &nbsp;&nbsp;<label class="salaGestao" id="' . $to->getId_saladeaula() . '-' . $to->getId_disciplina() . '">Gestão</label>';
                }
                $arrSalas[$to->getId_projetopedagogico()]['st_projetopedagogico'] = $to->getSt_projetopedagogico();
                $arrSalas[$to->getId_projetopedagogico()]['projetos'][] = $to;
            }
            $this->mensageiro->setMensageiro($arrSalas, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $mensageiro->getCodigo());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que retorna dados para configuração da saladeaula Portal>>gestao
     * @param VwSalaConfiguracaoTO $to
     * @throws Zend_Validate_Exception
     * @return Ead1_Mensageiro
     */
    public function retornaVwSalaConfiguracao(VwSalaConfiguracaoTO $to)
    {
        try {
            $dao = new SalaDeAulaDAO();
            $dados = parent::resultToArrayIfObjectOrReturnEmptyArray($dao->retornaVwSalaConfiguracao($to));
            if (empty($dados)) {
                throw new Zend_Validate_Exception("Nenhuma Sala de Aula Encontrada para ser configurada!");
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwSalaConfiguracaoTO(), true), Ead1_IMensageiro::SUCESSO);

        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna os alunos para encerramento
     * @param VwEncerramentoAlunosTO $vwencerramento
     * @param int $id_encerramento [tipo de alunos do encerramento a serem listados]
     * @throws Zend_Validate_Exception
     * @return Ead1_Mensageiro
     */
    public function retornaAlunosEncerramento(VwEncerramentoAlunosTO $vwencerramento, $id_encerramento)
    {
        $where = '';
        try {
            if ($id_encerramento == VwEncerramentoAlunosTO::NAO_REPASSADO):
                $where = ' dt_encerramentoprofessor IS NULL AND dt_encerramentocoordenador IS NULL AND dt_encerramentopedagogico IS NULL AND dt_encerramentofinanceiro IS NULL ';
            elseif ($id_encerramento == VwEncerramentoAlunosTO::COORDENADOR):
                $where = ' dt_encerramentoprofessor IS NOT NULL AND dt_encerramentocoordenador IS NULL AND dt_encerramentopedagogico IS NULL AND dt_encerramentofinanceiro IS NULL ';
            elseif ($id_encerramento == VwEncerramentoAlunosTO::PEDAGOGICO):
                $where = ' dt_encerramentoprofessor IS NOT NULL AND dt_encerramentocoordenador IS NOT NULL AND dt_encerramentopedagogico IS NULL AND dt_encerramentofinanceiro IS NULL ';
            elseif ($id_encerramento == VwEncerramentoAlunosTO::PAGAMENTO):
                $where = ' dt_encerramentoprofessor IS NOT NULL AND dt_encerramentocoordenador IS NOT NULL AND dt_encerramentopedagogico IS NOT NULL AND dt_encerramentofinanceiro IS NULL ';
            elseif ($id_encerramento == VwEncerramentoAlunosTO::PAGOS):
                $where = ' dt_encerramentoprofessor IS NOT NULL AND dt_encerramentocoordenador IS NOT NULL AND dt_encerramentopedagogico IS NOT NULL AND dt_encerramentofinanceiro IS NOT NULL ';
            elseif ($id_encerramento == VwEncerramentoAlunosTO::PAGAMENTO_RECUSADO):
                $where = ' dt_recusa IS NOT NULL ';
            endif;

            $dao = new SalaDeAulaDAO();
            $dados = $dao->retornaAlunosEncerramento($vwencerramento, $where);
            if (empty($dados)) {
                throw new Zend_Validate_Exception("Nenhum aluno encontrado!");
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwEncerramentoAlunosTO()), Ead1_IMensageiro::SUCESSO);

        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro;
    }


    /**
     * Método que repassa para o coordenador as salas de aula periodicas
     * @param unknown_type $dados
     * @return Ead1_Mensageiro|Ambigous <Ambigous, Ead1_Mensageiro, Ead1_Mensageiro>
     */
    public function repasseProfessorSalaPeriodica($dados)
    { //2
        try {
            $to = new VwEncerramentoAlunosTO();
            $to->setId_saladeaula($dados['id_saladeaula']);
            $alunos = $this->retornaAlunosEncerramento($to, $dados['id_liberar']);
            $alunos->subtractMensageiro();
            $retorno = $this->validarAlunosEncerramento($alunos->getMensagem(), TipoSalaDeAulaTO::SALA_PERIODICA);
            if ($retorno->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $retorno1 = $this->repassarAlunoCoordenador($retorno->getMensagem());
                return new Ead1_Mensageiro($retorno1);
            } else {
                return new Ead1_Mensageiro($retorno);
            }

        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Método que trata os dados das salas permanentes para encerramento do professor
     * @param unknown_type $dados
     * @throws Zend_Exception
     * @return Ead1_Mensageiro|Ambigous <Ead1_Mensageiro, Ead1_Mensageiro>
     */
    public function repasseProfessorSalaPermanente($dados)
    { //1 Aqui que chama o metodo do moodleBO para inativar o perfil
        try {
            if (empty($dados['id_alocacao'])) {
                throw new Zend_Exception('Nenhum aluno selecionado!');
            } else {
                $alunos = array();
                $alunosMoodle = array();
                foreach ($dados['id_alocacao'] as $id_alocacao) {
                    $to = new VwEncerramentoAlunosTO();
                    $to->setId_alocacao($id_alocacao);
                    $to->fetch(false, true, true);
                    if ($to->getId_sistema() == SistemaTO::MOODLE) {
                        $alunosMoodle[] = $to;
                    }
                    $alunos[] = $to;
                }
            }

            $retorno = $this->validarAlunosEncerramento($alunos, TipoSalaDeAulaTO::SALA_PERMANENTE);
            if ($retorno->getTipo() == Ead1_IMensageiro::SUCESSO) {
                if (!empty($alunosMoodle)) {
                    $moodleBo = new MoodleBO();
                    $moodleBo->alterarPapelAluno($alunosMoodle);
                }
                $retorno = $this->repassarAlunoCoordenador($alunos);
                if ($retorno->getTipo() == Ead1_IMensageiro::SUCESSO) {
                    return new Ead1_Mensageiro('Aluno(s) transferido(s) para o Coordenador!', Ead1_IMensageiro::SUCESSO);
                } else {
                    return new Ead1_Mensageiro($retorno->getFirstMensagem(), Ead1_IMensageiro::ERRO);
                }
            } else {
                return new Ead1_Mensageiro($retorno->getFirstMensagem(), Ead1_IMensageiro::ERRO);
            }

        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Valida o encerramento/repasse do aluno na entrega do TCC
     * 1 - O aluno só pode fazer o repasse se tiver enviado a versão final do TCC
     * 2 - O aluno tem que ter nota
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param VwEncerramentoAlunosTO $to
     */
    public function validarEncerramentoTCC(VwEncerramentoAlunosTO $to)
    {
        if (!$to->getSt_nota()) {
            return new Ead1_Mensageiro("O aluno " . $to->getSt_nomecompleto() . ", matrícula " . $to->getId_matricula() . ", não tem nota de TCC", Ead1_IMensageiro::ERRO);
        }

        return new Ead1_Mensageiro("Aluno apto para o encerramento!", Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Método que verifica se todos os alunos tem nota para a sala ou os alunos serem encerrados
     * @param array $vwencerramentoalusnoTo
     * @return Ambigous <Ead1_Mensageiro, Ead1_Mensageiro>
     */
    public function validarAlunosEncerramento(array $vwencerramentoalunosTo, $id_tiposaladeaula)
    {
        $erro = false;
        $cont = 0;
        foreach ($vwencerramentoalunosTo as $alunos) {

            $encerramentoAlocacao = new EncerramentoAlocacaoTO();
            $encerramentoAlocacao->setId_alocacao($alunos->getId_alocacao());
            $encerramentoAlocacao->fetch(false, true, true);

            if ($encerramentoAlocacao->getId_encerramentosala()):
                unset($vwencerramentoalunosTo[$cont]);
                $cont++;
                continue;
            endif;

            if ($id_tiposaladeaula == TipoSalaDeAulaTO::SALA_PERIODICA) {
                if (!$alunos->getId_logacesso() || $alunos->getId_tiponota() == 2):
                    unset($vwencerramentoalunosTo[$cont]);
                    $cont++;
                    continue;
                endif;
            }

            if ($alunos->getid_tipodisciplina() == TipoDisciplinaTO::TCC) {
                $validacao = $this->validarEncerramentoTCC($alunos);
                if ($validacao->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    if ($erro) {
                        $erro .= "<br>";
                    }
                    $erro .= $validacao->getFirstMensagem();
                }
            } else {
                if (!$alunos->getSt_nota()) {
                    if ($erro) {
                        $erro .= "<br>";
                    }
                    $erro .= "O Aluno " . $alunos->getSt_nomecompleto() . " Não tem nota!";
                };
            }

            $cont++;
        }

        if ($erro) {
            if ($id_tiposaladeaula == TipoSalaDeAulaTO::SALA_PERMANENTE):
                return $this->mensageiro->setMensageiro($erro, Ead1_IMensageiro::ERRO);
            else:
                return $this->mensageiro->setMensageiro($erro, Ead1_IMensageiro::ERRO);
            endif;
        } else {
            return $this->mensageiro->setMensageiro($vwencerramentoalunosTo, Ead1_IMensageiro::SUCESSO);
        }
    }

    /**
     * Método que repassa os alunos para o coordenador
     * @param array $alunos
     * @return Ead1_Mensageiro
     */
    public function repassarAlunoCoordenador(array $alunos)
    {
        try {
            $dao = new SalaDeAulaDAO();
            $dao->beginTransaction();
            $inserir = true;
            $alunosM = array();
            if (empty($alunos)) {
                throw new Zend_Exception('Nenhum aluno para repassar!');
            }
            foreach ($alunos as $aluno) {
                if ($inserir):
                    $encerramentosalato = new EncerramentoSalaTO();
                    $encerramentosalato->setId_saladeaula($aluno->getId_saladeaula());
                    $encerramentosalato->setId_usuarioprofessor($encerramentosalato->getSessao()->id_usuario);
                    $encerramentosalato->setDt_encerramentoprofessor(Zend_Date::now());

                    $id_encerramentosala = $dao->cadastrarEncerramentoSala($encerramentosalato);
                    $inserir = false;

                    if ($aluno->getId_tipodisciplina() == TipoDisciplinaTO::TCC) {
                        $inserir = true; // TCC RECEBE UM NOVO ENCERRAMENTOSALA PRA CADA ALUNO
                    }
                endif;

                $enceramentoalocacao = new EncerramentoAlocacaoTO();
                $enceramentoalocacao->setId_encerramentosala($id_encerramentosala);
                $enceramentoalocacao->setId_alocacao($aluno->getId_alocacao());

                $dao->cadastrarEncerramentoAlocacao($enceramentoalocacao);
                if ($aluno->getId_sistema() == SistemaTO::MOODLE) {
                    $alunosM[] = $aluno;
                }
            }
            $dao->commit();
            if (!empty($alunosM)) {
                $this->sincronizarNotasEncerramento($alunosM);
            }
            $this->mensageiro->setMensageiro('Aluno transferido para o Coordenador.', Ead1_IMensageiro::SUCESSO);

        } catch (Zend_Exception $e) {
            $dao->rollBack();
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO, $e->getMessage());
        }

        return $this->mensageiro;
    }

    /**
     * Método que retorna os alunos para sincronização de notas
     * @param $courseid
     * @param $id_saladeaula
     * @param null $id_entidade
     * @param null $id_usermoodle
     * @return Ead1_Mensageiro
     */
    public function retornaAlunosNotaSincronizacao($courseid, $id_saladeaula, $id_entidade = null, $id_usermoodle = null)
    {
        try {
            /** @var \G2\Entity\SalaDeAula $sala */
            $sala = $this->negocio->find(\G2\Entity\SalaDeAula::class, $id_saladeaula);

            $moodlews = new MoodleAlocacaoWebServices($id_entidade, $sala->getid_entidadeintegracao());
            $mensageiro = $moodlews->recuperarNotas($courseid, $id_saladeaula, $id_entidade, $id_usermoodle);

            if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
                return $mensageiro;
            } else {
                $moodlews = new MoodleAlocacaoWebServices();
            }

            $mensageiro = $moodlews->recuperarNotas($courseid, $id_saladeaula, $id_entidade, $id_usermoodle);

            if ($mensageiro->getTipo() !== Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Exception($mensageiro->getFirstMensagem());
            }
            return $mensageiro;
        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Sincroniza as notas do aluno com o AVA
     * @param array $dados
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function sincronizarNotas($dados)
    {
        try {
            if (empty($dados['dados'])) {
                throw new Zend_Exception('Nenhum aluno selecionado para sincronização das notas!');
            } else {
                $array_to = array();
                foreach ($dados['dados'] as $info) {
                    $dado = explode('#', $info);
                    $vw = new VwAvaliacaoAlunoTO();
                    $vw->setId_matricula($dado[0]);
                    $vw->setId_avaliacao($dado[1]);
                    $vw->setId_avaliacaoconjuntoreferencia($dado[2]);
                    $vw->setSt_notaava($dado[3]);
                    $vw->setId_tipoavaliacao($dado[4]);

                    $array_to[] = $vw;
                }
                $moodlews = new MoodleAlocacaoWebServices();
                return $moodlews->sincronizarNotas($array_to);
            }
        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }

    public function sincronizarNotasEncerramento(array $alunos)
    {
        try {
            if (!$alunos[0]->getId_saladeaula()) {
                throw new Zend_Exception('O id_saladeaula é obrigatório e não foi passado!');
            }

            $salaDeAulaIntegracao = new SalaDeAulaIntegracaoTO();
            $salaDeAulaIntegracao->setId_saladeaula($alunos[0]->getId_saladeaula());
            $salaDeAulaIntegracao->getId_sistema(SistemaTO::MOODLE);
            $salaDeAulaIntegracao->fetch(false, true, true);

            if (!$salaDeAulaIntegracao->getSt_codsistemacurso()) {
                throw new Zend_Exception('Sala de aula não integrada ao moodle!');
            }

            $notasMoodle = $this->retornaAlunosNotaSincronizacao($salaDeAulaIntegracao->getSt_codsistemacurso(), $alunos[0]->getId_saladeaula());

            if ($notasMoodle->getTipo() == Ead1_IMensageiro::SUCESSO):
                $notas = $notasMoodle->getMensagem();
                foreach ($alunos as $aluno) {
                    foreach ($notas as $nota) {
                        if ($aluno->getId_matricula() == $nota->id_matricula) {
                            $vw = new VwAvaliacaoAlunoTO();
                            $vw->setId_matricula($nota->id_matricula);
                            $vw->setId_avaliacao($nota->id_avaliacao);
                            $vw->setId_avaliacaoconjuntoreferencia($nota->id_avaliacaoconjuntoreferencia);
                            $vw->setSt_notaava($nota->st_notaava);

                            $array_to[] = $vw;
                        }
                    }

                }
                $moodlews = new MoodleAlocacaoWebServices();
                return $moodlews->sincronizarNotas($array_to);
            endif;

        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * @param VwListagemProjetosSalaTO $vw
     * @return Ead1_Mensageiro
     */
    public function retornaListagemProjetosSala(VwListagemProjetosSalaTO $vw, $page = 1, $rowCount = 90000)
    {
        try {
            $dao = new SalaDeAulaDAO();
            if (Ead1_Sessao::getSessaoPerfil()->id_projetopedagogicopesquisa) {
                $vw->setId_projetopedagogico(Ead1_Sessao::getSessaoPerfil()->id_projetopedagogicopesquisa);
            }
            $busca = array("st_projetopedagogico", "id_usuario", "id_entidade", "id_perfil", "id_perfilpedagogico", "id_projetopedagogico", "st_coordenador");
            if ($vw->getId_status()) {
                array_push($busca, "id_status", "st_status");
            }

            $dados = $dao->retornaVwListagemProjetosSala($vw, $busca, $page, $rowCount)->toArray();

            if (empty($dados)) {
                throw new Zend_Validate_Exception('Nenhum projeto pedagógico encontrado!');
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwListagemProjetosSalaTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao retornar os projetos pedagógicoss!", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }


    public function retornaListagemProjetosSalaDoctrine($params, $page = 1, $rowCount = 90000)
    {
        try {
            $arrReturn = array();

            $offset = ($page == 1 || !$page) ? 0 : ($page * $rowCount) - $rowCount;

            $vw = new \G2\Entity\VwListagemProjetosSala();
            $result = $vw->findByCustom($params, array('st_projetopedagogico' => 'ASC'), $rowCount, $offset);

            foreach ($result as $row) {
                if ($row instanceof \G2\Entity\VwListagemProjetosSala)
                    $arrReturn[] = $this->toArrayEntity($row);
            }

            return $arrReturn;

        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }
}

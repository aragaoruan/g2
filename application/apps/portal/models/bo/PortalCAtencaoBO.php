<?php

/**
 * Classe com regras de negócio para Central de Atenção
 */
class PortalCAtencaoBO extends Ead1_BO
{
    public function __construct()
    {
        parent::__construct();
        $this->mensageiro = new Ead1_Mensageiro();
    }

    /**
     * Método que retorna o combo de Evolucao
     * @return Ead1_Mensageiro
     * @see CAtencaoController
     */
    public function retornarComboEvolucao()
    {
        try {
            $combo = array();

            $toEvolucao = new EvolucaoTO();
            $toEvolucao->setSt_tabela('tb_ocorrencia');
            $mensageiro = new Ead1_Mensageiro($toEvolucao->fetch());
            $mensageiro->subtractMensageiro();
            $dadosMensagem = $mensageiro->getMensagem();
            foreach ($dadosMensagem as $obj) {
                $combo[$obj->id_evolucao]['label'] = ($obj->st_evolucao);
                $combo[$obj->id_evolucao]['value'] = $obj->id_evolucao;
            }
            $this->mensageiro->setMensageiro($combo, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro
                ->setMensageiro("Erro ao Retornar Evolução.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }

        return $this->mensageiro;
    }


    /**
     * Método que retorna o combo de situações
     * @return Ead1_Mensageiro
     */
    public function retornarComboSituacao()
    {
        try {
            $combo = array();

            $toSituacao = new SituacaoTO();
            $toSituacao->setSt_tabela('tb_ocorrencia');
            $mensageiro = new Ead1_Mensageiro($toSituacao->fetch());
            $mensageiro->subtractMensageiro();
            $dadosMensagem = $mensageiro->getMensagem();
            foreach ($dadosMensagem as $obj) {
                $combo[$obj->id_situacao]['label'] = ($obj->st_situacao);
                $combo[$obj->id_situacao]['value'] = $obj->id_situacao;
            }
            $this->mensageiro->setMensageiro($combo, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro
                ->setMensageiro("Erro ao Retornar Situação.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }

        return $this->mensageiro;
    }


    /**
     * Método que retorna o combo de Categoria da Ocorrência (tipo)
     * @return Ead1_Mensageiro
     */
    public function retornarComboCategoriaOcorrencia()
    {
        try {
            $combo = array();

            $toTipo = new CategoriaOcorrenciaTO();

            //Caso seja aluno carregar tipos de ocorrencia de aluno
            if (Ead1_Sessao::getSessaoPerfil()->id_perfilpedagogico == PerfilPedagogicoTO::ALUNO) {
                $toTipo->setId_tipoocorrencia(TipoOcorrenciaTO::ALUNO);
            }

            //Caso seja aluno carregar tipos de ocorrencia de professor
            if (Ead1_Sessao::getSessaoPerfil()->id_perfilpedagogico == PerfilPedagogicoTO::PROFESSOR) {
                $toTipo->setId_tipoocorrencia(TipoOcorrenciaTO::TUTOR);
            }

            $toTipo->setBl_ativo(true);
            $toTipo->setId_entidadecadastro(Ead1_Sessao::getSessaoEntidade()->id_entidade);
            $mensageiro = new Ead1_Mensageiro($toTipo->fetch());
            $mensageiro->subtractMensageiro();
            $dadosMensagem = $mensageiro->getMensagem();
            foreach ($dadosMensagem as $obj) {
                $combo[$obj->id_categoriaocorrencia]['label'] = $obj->st_categoriaocorrencia;
                $combo[$obj->id_categoriaocorrencia]['value'] = $obj->id_categoriaocorrencia;
            }
            $this->mensageiro->setMensageiro($combo, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro
                ->setMensageiro("Erro ao Retornar Módulos.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }


    /**
     * Método que retorna combo com os assuntos Pai
     * @param VwAssuntosTO $assunto
     * @return Ead1_Mensageiro
     */
    public function retornaComboAssuntoPai(VwAssuntosTO $assunto)
    {
        try {
            $dao = new AssuntoCoDAO();

            if (Ead1_Sessao::getSessaoPerfil()->is_aluno() || Ead1_Sessao::getSessaoPerfil()->is_alunoInstitucional()):
                $assunto->setId_tipoocorrencia(AssuntoCoTO::TIPO_OCORRENCIA_ALUNO);
            else:
                $assunto->setId_tipoocorrencia(AssuntoCoTO::TIPO_OCORRENCIA_TUTOR);
            endif;

            $assunto->setBl_ativo(true);
            $assunto->setId_entidade(Ead1_Sessao::getSessaoEntidade()->id_entidade);

            $strWhere = " AND id_assuntocopai is null AND bl_abertura = 1";

            if (!is_null($assunto->getBl_interno())) {
                $strWhere .= "  AND bl_interno = " . (int)$assunto->getBl_interno();
            }

            $mensageiro = new Ead1_Mensageiro($assunto
                ->encapsularTo($dao->retornaVwAssuntos($assunto, $strWhere), $assunto));

            $mensageiro->subtractMensageiro();
            $dadosMensagem = $mensageiro->getMensagem();

            foreach ($dadosMensagem as $obj) {
                $combo[$obj->id_assuntoco]['label'] = $obj->st_assuntoco;
                $combo[$obj->id_assuntoco]['value'] = $obj->id_assuntoco;
            }
            $this->mensageiro->setMensageiro($combo, Ead1_IMensageiro::SUCESSO);

        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar AssuntoPai.",
                Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna o combo com os assuntos (assuntoco)
     * @param VwAssuntosTO $assunto
     * @param bool $bl_abertura
     * @return Ead1_Mensageiro
     */
    public function retornaComboAssuntoco(VwAssuntosTO $assunto, $bl_abertura = true)
    {
        try {
            if ($bl_abertura) {
                $where = " AND bl_abertura = 1 ";
            } else {
                $where = "";
            }


            if (!is_null($assunto->getBl_interno())) {
                $where .= "  AND bl_interno = " . (int)$assunto->getBl_interno();
            }

            $dao = new AssuntoCoDAO();

            $assunto->setBl_ativo(true);
            $assunto->setId_situacao(91);
            $assunto->setId_entidade(Ead1_Sessao::getSessaoEntidade()->id_entidade);
            $assunto->setBl_abertura(true);

            $mensageiro = new Ead1_Mensageiro(Ead1_TO_Dinamico::encapsularTo($dao
                ->retornaVwAssuntos($assunto, $where), $assunto));
            $dadosMensagem = null;
            $dadosMensagem = $mensageiro->getMensagem();
            if (!empty($dadosMensagem)) {
                foreach ($dadosMensagem as $obj) {
                    $combo[$obj->id_assuntoco]['label'] = $obj->st_assuntoco;
                    $combo[$obj->id_assuntoco]['value'] = $obj->id_assuntoco;
                    $combo[$obj->id_assuntoco]['id_textosistema'] = $obj->id_textosistema;
                }

                $this->mensageiro->setMensageiro($combo, Ead1_IMensageiro::SUCESSO);
            } else {
                $this->mensageiro->setMensageiro('Nenhum assunto encontrado', Ead1_IMensageiro::AVISO);
            }

        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar Assunto.  " . $e->getMessage(),
                Ead1_IMensageiro::ERRO, $e->getMessage());
        }

        return $this->mensageiro;
    }

    /**
     * Método que retorna o combo com as salas de aula
     * Se for aluno -> filtra pelo id_matricula
     * @return Ead1_Mensageiro
     */
    public function retornaComboSala()
    {
        $combo = array();

        //Retorna salas de aula de acordo com id_matricula
        if (Ead1_Sessao::getSessaoPerfil()->is_aluno() || Ead1_Sessao::getSessaoPerfil()->is_alunoInstitucional()):
            $to = new MatriculaDisciplinaTO();
            $to->setId_matricula(Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula);
            $vwSalaDisciplinaAlocacaoTO = new VwSalaDisciplinaAlocacaoTO();
            $vwSalaDisciplinaAlocacaoTO->setId_matricula($to->getId_matricula());
            $vwSalaDisciplinaAlocacaoTO->setId_disciplina($to->getId_disciplina());

            $ro = new SalaDeAulaRO();
            $mensageiro = $ro->retornarVwSalaDisciplinaAlocacao($vwSalaDisciplinaAlocacaoTO);
            $mensageiro->subtractMensageiro();
            $dadosMensagem = $mensageiro->getMensagem();
            foreach ($dadosMensagem as $obj) {
                if ($obj->id_alocacao) {
                    $combo[$obj->id_saladeaula]['label'] = $obj->st_saladeaula;
                    $combo[$obj->id_saladeaula]['value'] = $obj->id_saladeaula;
                }
            }
        else: //Retorna salas de aula de professor
            $salaBO = new PortalSalaDeAulaBO();
            $to = new VwSalaDeAulaProfessorIntegracaoTO();
            $to->setId_usuario(Ead1_Sessao::getSessaoUsuario()->id_usuario);
            $to->setId_entidade(Ead1_Sessao::getSessaoEntidade()->id_entidade);
            $to->setId_perfilpedagogico(Ead1_Sessao::getSessaoPerfil()->id_perfilpedagogico);
            $mensageiro = $salaBO->retornarSalaDeAulaProfessor($to);
            $mensageiro->subtractMensageiro();
            $dadosMensagem = $mensageiro->getMensagem();
            foreach ($dadosMensagem as $disciplinas) {
                foreach ($disciplinas['disciplinas'] as $obj):
                    $combo[$obj->id_saladeaula]['label'] = $obj->st_saladeaula;
                    $combo[$obj->id_saladeaula]['value'] = $obj->id_saladeaula;
                endforeach;
            }
        endif;

        return $this->mensageiro->setMensageiro($combo, Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Método que salva uma ocorrência
     * @param OcorrenciaTO $ocorrencia
     * @return Ead1_Mensageiro
     * @deprecated new G2\Negocio\CAtencao
     */
    public function salvarOcorrencia(OcorrenciaTO $ocorrencia)
    {
        $dao = new CAtencaoDAO();
        try {
            $dao->beginTransaction();

            $arNaoObrigatorios[] = 'id_ocorrencia';
            $arNaoObrigatorios[] = 'id_saladeaula';
            $arNaoObrigatorios[] = 'id_ocorrenciaoriginal';
            $arNaoObrigatorios[] = 'id_motivoocorrencia';
            $arNaoObrigatorios[] = 'dt_cadastroocorrencia';
            $arNaoObrigatorios[] = 'dt_cadastro';

            if (!Ead1_Sessao::getSessaoPerfil()->is_aluno()
                && !Ead1_Sessao::getSessaoPerfil()->is_alunoInstitucional()
            ) {
                $arNaoObrigatorios[] = 'id_matricula';
            }

            $this->verificaCamposObrigatorios($ocorrencia, $arNaoObrigatorios);

            $criacao = false;
            if (!$ocorrencia->getId_ocorrencia()) {
                $criacao = true;
            }

            $ocorrencia->setId_ocorrencia($dao->cadastrarOcorrencia($ocorrencia));
            if ($criacao) {
                $stTramite = 'Ocorrência N. ';
                $stTramite .= $ocorrencia->getId_ocorrencia();
                $stTramite .= ' criada por ';
                $stTramite .= Ead1_Sessao::getSessaoUsuario()->st_nomeexibicao . '.';

                $tramite = new TramiteTO();
                $tramite->setSt_tramite($stTramite);
                $tramite->setId_usuario(Ead1_Sessao::getSessaoUsuario()->id_usuario);
                $tramite->setId_entidade(Ead1_Sessao::getSessaoUsuario()->id_entidade);
                $this->salvaTramite($ocorrencia, $tramite, 0, 0, 0, 0);
            }

            $dao->commit();
            return new Ead1_Mensageiro($ocorrencia, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            return new Ead1_Mensageiro("Erro ao cadastrar Ocorrencia. "
                . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }


    /**
     * Faz o upload de um arquivo anexo a um tramite
     * @deprecated new G2\Negocio\CAtencao
     * @param TramiteTO $tramite
     * @param $arquivo
     * @return Ead1_Mensageiro
     */
    public function salvarArquivoTramite(TramiteTO $tramite, $arquivo)
    {

        $dao = new CAtencaoDAO();
        try {


            if ($arquivo instanceof ArquivoTO) {
                $arquivo->setSt_nomearquivo($tramite->getId_tramite());
                $retorno = $this->uploadBasico($arquivo);
            } elseif (!empty($arquivo['name'])) {
                $diretorio = 'upload' . DIRECTORY_SEPARATOR . 'tramite' . DIRECTORY_SEPARATOR;
                if (!file_exists($diretorio)) {
                    mkdir('upload' . DIRECTORY_SEPARATOR . 'tramite' . DIRECTORY_SEPARATOR, 0777);
                }
                $renomear = explode('.', $arquivo['name']);
                $arquivo['name'] = $tramite->getId_tramite() . '.' . $renomear[1];

                $adapter = new Zend_File_Transfer_Adapter_Http();
                $adapter->addFilter('Rename', $arquivo['name']);
                $adapter->setDestination($diretorio);

                if (!$adapter->receive()) {
                    $messages = $adapter->getMessages();
                    echo implode("\n", $messages);
                }

                $upload = new Zend_File_Transfer();
                $upload->receive();

            };


            $upload = new UploadTO();
            //Concatenação do nome do arquivo retirado da linha 273 e inserido na linha seguinte,
            //pois estava duplicando a extensão no aquivo físico e faltando a extensão no UploadTO

            $stUpload = $arquivo['name'];

            if ($arquivo instanceof ArquivoTO) {
                $stUpload = $arquivo->getSt_nomearquivo() . '.' . $arquivo->getSt_extensaoarquivo();
            }

            $upload->setSt_upload($stUpload);
            $id_upload = $dao->salvarImagemUpload($upload); //aqui que tenho q tratar se tinha arquivo ou nao
            $tramite->setId_tramite($tramite->getId_tramite());
            $tramite->setId_upload($id_upload);
            $dao->updateTramite($tramite);


            return new Ead1_Mensageiro('Arquivo salvo com sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            return new Ead1_Mensageiro('Erro no Uploado do arquivo relativo ao Tramite: '
                . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }


    }


    /**
     * Método que salva um trâmite para a ocorrência
     * e faz o upload de um arquivo
     * @param OcorrenciaTO $ocorrencia
     * @param TramiteTO $tramite
     * @param null $arquivo
     * @param null $sistema
     * @param bool $bl_notificacao
     * @param bool $bl_interessado
     * @return Ead1_Mensageiro
     * @throws Exception
     * @see CAtencaoBO::novaInteracao
     * @see CAtencaoController::salvarTramiteAction
     * @see CAtencaoController::salvarOcorrenciaAction
     */
    public function salvaTramite(
        OcorrenciaTO $ocorrencia,
        TramiteTO $tramite,
        $arquivo = null,
        $sistema = null,
        $bl_notificacao = false,
        $bl_interessado = false
    )
    { //passar o tramite

        $daoTramite = new TramiteDAO();
        $dao = new CAtencaoDAO();
        $dao->beginTransaction();

        try {
            $ocorrenciaTramite = new \G2\Entity\Ocorrencia();
            $ocorrenciaTramite->find($ocorrencia->getId_ocorrencia());

            //verifica se é uma instancia do obejto Ocorrencia para evitar chamada no objeto nao instanciado
            if (!($ocorrenciaTramite instanceof \G2\Entity\Ocorrencia)) {
                throw new Exception("Ocorrencia não encontrada.");
            }

            //atribuia variavel a data do tramite como a data atual
            $dt_ultimotramite = new \DateTime();

            //verifica se a ocorrencia retornou a data do ultimo tramite
            if ($ocorrenciaTramite->getDt_ultimotramite()) {
                //converte a string da ta para um datetime
                $dt_ultimotramite = $this->negocio
                    ->converterData($ocorrenciaTramite->getDt_ultimotramite(), 'datetime');
            }

            $dttramite = $dt_ultimotramite;
            $now = new DateTime();
            $diferenca = $dttramite->diff($now)->format('%i');

            $id_tramite = $daoTramite->cadastrarTramite($tramite);
            if ($arquivo) {
                $tramite->setId_tramite($id_tramite);
                $mensageiro = $this->salvarArquivoTramite($tramite, $arquivo);
                if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception($mensageiro->getFirstMensagem());
                }
            }

            /*
             * Alteração na lógica do if para atender no caso de persistencias via aplicativo onde não é salvo os dados
             * do usuário na sessão do php, entende-se então que o id do usuário do tramite deve ser igual ao id do
             * usuário que deveria ser salvo na sessão, ver ApiApp_InteracaoController::postAction() onde o id da
             * entidade e o id do usuário são recuperados do token e seguindo o fluxo dos métodos é armazenado nas TO's
             */
            if (($ocorrencia->getId_usuariointeressado() == $ocorrencia->getSessao()->id_usuario
                    || $ocorrencia->getId_usuariointeressado() == $tramite->getId_usuario())
                && $bl_interessado
            ) {
                $ocorrencia->setId_evolucao(OcorrenciaTO::EVOLUCAO_INTERESSADO_INTERAGIU);
                $ocorrencia->setId_situacao(OcorrenciaTO::SITUACAO_EM_ANDAMENTO);

                $this->alterarOcorrencia($ocorrencia);
            }

            $tramiteOcorrencia = new TramiteOcorrenciaTO();
            $tramiteOcorrencia->setId_tramite($id_tramite);
            $tramiteOcorrencia->setId_ocorrencia($ocorrencia->getId_ocorrencia());
            $id_ocorrencia = $ocorrencia->getId_ocorrencia();
//            if(!empty($id_ocorrencia)){
//            }

            if ($sistema == SistemaTO::GESTOR) {
                $tramiteOcorrencia->setBl_interessado(false);
            } else if ($sistema == SistemaTO::PORTAL_ALUNO) {
                $tramiteOcorrencia->setBl_interessado(true);
            };


            $dao->salvarTramiteOcorrencia($tramiteOcorrencia);

            $dao->commit();

            if ((int)$diferenca >= 5)
                $this->enviaNotificacaoOcorrencia($ocorrencia, $bl_notificacao);


            return new Ead1_Mensageiro($id_tramite, Ead1_IMensageiro::SUCESSO);

        } catch (Exception $e) {
            $dao->rollBack();
            return new Ead1_Mensageiro('Erro ao cadastrar trâmite -'
                . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Retorna lista de ocorrências
     * @param VwOcorrenciaTO $to
     * @param VwOcorrenciaTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarOcorrencias(VwOcorrenciaTO $to)
    {

        if (Ead1_Sessao::getSessaoUsuario()->id_usuario) {
            $to->setId_usuariointeressado(Ead1_Sessao::getSessaoUsuario()->id_usuario);
        }

        if ($to->getId_usuariointeressado()) {
            $dao = new CAtencaoDAO();
            $ocorrencias = $to->encapsularTo($dao->retornaOcorrencias($to), new VwOcorrenciaTO());

            $mensageiro = $this->mensageiro->setMensageiro(
                'Nenhuma ocorrência encontrada!',
                Ead1_IMensageiro::AVISO
            );

            if (!empty($ocorrencias)) {
                $mensageiro = $this->mensageiro->setMensageiro($ocorrencias, Ead1_IMensageiro::SUCESSO);
            }
            return $mensageiro;
        }
    }

    /**
     * Vincula um responsável a ocorrência
     * @param OcorrenciaTO $ocorrencia
     * @throws Zend_Exception
     * @return boolean|Ead1_Mensageiro
     */
    public function vinculaResponsavel(OcorrenciaTO $ocorrencia)
    {

        $daoTramite = new CAtencaoDAO();
        $daoTramite->beginTransaction();

        try {

            $assunto = new AssuntoCoTO();
            $assunto->setId_assuntoco($ocorrencia->getId_assuntoco());
            $assunto->fetch(true, true, true);
            if ($assunto->getBl_autodistribuicao()) {
                $responsavel = new VwNucleoPessoaCoTO();
                $responsavel->setId_assuntoco($ocorrencia->getId_assuntoco());
                $responsavel->setBl_prioritario(true);
                $responsavel->setId_funcao(VwNucleoPessoaCoTO::FUNCAO_RESPONSAVEL);
                $responsavel->fetch(false, true, true);

                if ($responsavel->getId_usuario()) {
                    $to = new OcorrenciaResponsavelTO();
                    $to->setBl_ativo(true);
                    $to->setId_ocorrencia($ocorrencia->getId_ocorrencia());
                    $to->setId_usuario($responsavel->getId_usuario());
                    $dao = new CAtencaoDAO();
                    $retorno = $dao->vincularResponsavelOcorrencia($to);
                }

            }
            $daoTramite->commit();
            return true;
        } catch (Zend_Exception $e) {
            throw new Zend_Exception("Erro : " . $e->getMessage());
            $daoTramite->rollBack();
            return new Ead1_Mensageiro('Erro ao vincular resposável -' . $e->getMessage(),
                Ead1_IMensageiro::ERRO, $e->getMessage());
        }

    }

    /**
     * Retorna o responsavel por uma ocorrencia (nucleoco)
     * @param VwOcorrenciaTO $ocorrencia
     * @return string st_nomecompleto
     */
    public function retornaResponsavel(VwOcorrenciaTO $ocorrencia)
    {
        $responsavel = new VwNucleoPessoaCoTO();
        $responsavel->setId_assuntoco($ocorrencia->getId_assuntoco());
        $responsavel->setBl_prioritario(true);
        $responsavel->setId_funcao(VwNucleoPessoaCoTO::FUNCAO_RESPONSAVEL);
        $responsavel->fetch(false, true, true);
        return $responsavel->getSt_nomecompleto();
    }

    /**
     * Retorna as interações de uma ocorrencia
     * @param VwOcorrenciaTO $ocorrencia
     * @return Ead1_Mensageiro
     */
    public function retornaInteracoesOcorrencia(VwOcorrenciaTO $ocorrencia)
    {
        try {
            if (!$ocorrencia->getId_ocorrencia()) {
                Throw new Zend_Exception('O id_ocorrencia é obrigatório e não foi passado!');
            }

            $dao = new CAtencaoDAO();
            $vwOcorrenciaInteracao = new VwOcorrenciaInteracaoTO();
            $vwOcorrenciaInteracao->setId_ocorrencia($ocorrencia->getId_ocorrencia());

            $retorno = $dao->retornaVwOcorrenciaInteracao($vwOcorrenciaInteracao)->toArray();

            $this->mensageiro
                ->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VwOcorrenciaInteracaoTO()),
                    Ead1_IMensageiro::SUCESSO);

            if (empty($retorno)) {
                $this->mensageiro
                    ->setMensageiro('Nenhuma interação encontrada', Ead1_IMensageiro::AVISO);
            }
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }

        return $this->mensageiro;
    }


    /**
     * Altera uma ocorrencia
     * @param OcorrenciaTO $to
     * @param TramiteTO $tramiteTo =null
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function alterarOcorrencia(OcorrenciaTO $to,
                                      TramiteTO $tramiteTo = null,
                                      \G2\Utils\User $user = null,
                                      $saveLog = true)
    {
        try {
            if (((int)$to->getId_ocorrencia()) == false
                || ((int)$to->getId_evolucao()) == false
                || ((int)$to->getId_situacao()) == false) {

                Throw new Zend_Exception('O id da ocorrencia não foi informado!');
            }
            $dao = new CAtencaoDAO();

            if ($dao->updateOcorrencia($to)) {

                if ($to->getId_situacao() == OcorrenciaTO::SITUACAO_ENCERRADA
                    || $to->getId_evolucao() == OcorrenciaTO::EVOLUCAO_REABERTURA_PELO_INTERESSADO
                ) {
                    $tramite = new TramiteTO();

                    //seta as variaveis que deveriam vim da sessão
                    $stNomeUsuario = Ead1_Sessao::getSessaoUsuario()->st_nomeexibicao ? Ead1_Sessao::getSessaoUsuario()
                        ->st_nomeexibicao : $user->getuser_name();

                    $idUsuario = Ead1_Sessao::getSessaoUsuario()->id_usuario ? Ead1_Sessao::getSessaoUsuario()
                        ->id_usuario : $user->getuser_id();

                    $idEntidade = Ead1_Sessao::getSessaoUsuario()->id_entidade ? Ead1_Sessao::getSessaoUsuario()
                        ->id_entidade : $user->getentidade_id();

                    if ($to->getId_situacao() == OcorrenciaTO::SITUACAO_ENCERRADA) {
                        $tramite->setSt_tramite('Ocorrência Nº ' . $to->getId_ocorrencia()
                            . ' encerrada por ' . $stNomeUsuario . '.');
                    } elseif ($to->getId_evolucao() == OcorrenciaTO::EVOLUCAO_REABERTURA_PELO_INTERESSADO) {
                        $tramite->setSt_tramite('Ocorrência N. ' . $to->getId_ocorrencia()
                            . ' reaberta por ' . $stNomeUsuario . ' por motivo: ' . $tramiteTo->getSt_tramite() . ' .');
                    }

                    $tramite->setId_usuario($idUsuario);
                    $tramite->setId_entidade($idEntidade);
                    $tramite->setBl_visivel(true);
                    $this->salvaTramite($to, $tramite);

                    if ($saveLog) {
                        $writer = new Zend_Log_Writer_Stream(
                            realpath(APPLICATION_PATH . '/../docs/logs') . '/ocorrencia.log');
                        $log = new Zend_Log();
                        $log->log($tramite->getSt_tramite(), 3);
                    }

                }
                return $this->mensageiro
                    ->setMensageiro('Ocorrência alterada com sucesso! ', Ead1_IMensageiro::SUCESSO);

            }
        } catch (Zend_Exception $e) {
            return $this->mensageiro
                ->setMensageiro('Erro ao alterar ocorrência -  ' . $e, Ead1_IMensageiro::ERRO, $e);
        }

    }


    /**
     * Retorna texto para ocorrência padrao do assunto
     * @param TextoSistemaTO $texto
     * @return Ead1_Mensageiro|Ambigous <Ead1_Mensageiro, Ead1_Mensageiro>
     */
    public function retornaTextoAssunto(TextoSistemaTO $texto)
    {
        try {
            $params = array('id_entidade' => $texto->getSessao()->id_entidade
            , 'id_usuario' => $texto->getSessao()->id_usuario);
            $textoBO = new TextoSistemaBO();

            $retorno = $textoBO->gerarTexto($texto, $params);
            if ($retorno->getFirstMensagem() instanceof TextoSistemaTO) {
                $retorno->getFirstMensagem()->setSt_texto($retorno->getFirstMensagem()->getSt_texto());
            }
            return $retorno;

        } catch (Zend_Exception $e) {
            return $this->mensageiro
                ->setMensageiro('Erro ao retornar assunto: '
                    . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }


    /**
     * Método para envio da notificacao - aguardando acertos nas tabelas (ou não)
     * @param OcorrenciaTO $ocorrencia
     * @return Ead1_Mensageiro
     * @see PortalCatencaoBO::salvaTramite
     */
    public function enviaNotificacaoOcorrencia(OcorrenciaTO $ocorrencia, $verificaEnvio = true, $id_entidade = null)
    {
        try {
            if (((int)$ocorrencia->getId_ocorrencia()) == false) {
                Throw new Zend_Exception('O id da ocorrencia não foi informado!');
            }
            $ocorrencia->fetch(false, true, true);
            $ocorrenciaResponsavel = new OcorrenciaResponsavelTO();
            $ocorrenciaResponsavel->setId_ocorrencia($ocorrencia->getId_ocorrencia());
            $ocorrenciaResponsavel->setBl_ativo(true);
            $ocorrenciaResponsavel->fetch(false, true, true);

            //Verifica se a ocorrência tem atendente responsavel
            if ($ocorrenciaResponsavel->getId_ocorrenciaresponsavel()) {
                if (!$id_entidade) {
                    $id_entidade = (Ead1_Sessao::getSessaoEntidade()->id_entidade
                        ? Ead1_Sessao::getSessaoEntidade()->id_entidade
                        : $ocorrenciaResponsavel->getSessao()->id_entidade);
                }
                $dao = new CAtencaoDAO();
                //Busca o texto a partir do assunto
                $vwNucleoAssunto = new VwNucleoAssuntoCoTO();
                $vwNucleoAssunto->setId_assuntoco($ocorrencia->getId_assuntoco());
                $vwNucleoAssunto->setId_entidade($id_entidade);

                $vwNucleoAssuntoN = new VwNucleoAssuntoCoTO();

                if (!$ocorrencia->getId_assuntoco()) {
                    throw new Zend_Exception("Id_assuntoco não pode ser vazio!");
                }

                $vwNucleoAssuntoN = $dao->retornaTextoNotificaoCA($vwNucleoAssunto);

                if (!empty($vwNucleoAssuntoN)) {
                    $vwNucleoAssuntoN = $vwNucleoAssuntoN->toArray();
                    if (empty($vwNucleoAssuntoN['id_textonotificacao'])) {
                        $this->mensageiro
                            ->setMensageiro('Nenhum texto cadastrado para esse assunto!',
                                Ead1_IMensageiro::AVISO);
                    } else {
                        if ($verificaEnvio || $vwNucleoAssuntoN['bl_notificaatendente']) {
                            $texto = new TextoSistemaTO();
                            $texto->setId_textosistema($vwNucleoAssuntoN['id_textonotificacao']);
                            $vwNucleoAssunto->setId_nucleoco($vwNucleoAssuntoN['id_nucleoco']);

                            // Busca o id_emailconfig a partir do id_assunto
                            $email = $dao->retornarIdEmailConfiguracao($vwNucleoAssunto);
                            if (!empty($email)) {
                                //Envia a notificação
                                $mensagem = new MensagemBO();

                                if ($vwNucleoAssuntoN['bl_notificaatendente']) {
                                    $this->mensageiro = $mensagem->enviarNotificacaoTextoSistema($texto, array(
                                        'id_ocorrencia' => $ocorrencia->getId_ocorrencia()
                                    ), $email['id_emailconfig'], $ocorrenciaResponsavel->getId_usuario(), $id_entidade);

                                }

                                if ($verificaEnvio && !empty($texto->getId_textosistema())
                                    && !empty($ocorrencia->getId_ocorrencia())
                                ) {

                                    $mensageiroMensagem = $mensagem
                                        ->enviarNotificacaoTextoSistema($texto, array(
                                            'id_ocorrencia' => $ocorrencia->getId_ocorrencia()
                                        ),
                                            $email['id_emailconfig'],
                                            $ocorrencia->getId_usuariointeressado(),
                                            $id_entidade);

                                    $this->mensageiro->setMensageiro($mensageiroMensagem->getFirstMensagem());
                                }
                            } else {
                                $this->mensageiro->setMensageiro('Nenhum e-mail cadastrado para envio!');
                            }


                        } else {
                            $this->mensageiro->setMensageiro('Notificação não enviada');
                        }
                    }
                } else {
                    $this->mensageiro
                        ->setMensageiro('Nenhum texto cadastrado para esse assunto!',
                            Ead1_IMensageiro::AVISO);
                }

            } else {
                $this->mensageiro->setMensageiro('Nenhum responsável encontrado para a ocorrência!');
            }

        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        }
        return $this->mensageiro;
    }

}

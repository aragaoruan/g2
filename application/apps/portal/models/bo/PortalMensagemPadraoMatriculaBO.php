<?php

/**
 * Description of PortalGerarAlertaBO
 *
 * @author Yannick Naquis Roulé
 */
class PortalMensagemPadraoMatriculaBO extends Ead1_BO
{

    public function __construct()
    {
        parent::__construct();
        $this->mensageiro = new Ead1_Mensageiro();
    }

    public function cadastrarAlerta(AlertaTO $aTO)
    {

        $dao = new PortalMensagemPadraoMatriculaDAO();
        try {
            $this->mensageiro = $dao->cadastrarAlerta($aTO);
            $dao->commit();
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            $this->mensageiro->setMensageiro("Erro ao salvar a alteração da alerta: " . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro;
    }

    public function retornarAlerta(AlertaTO $aTO)
    {
        $dao = new PortalMensagemPadraoMatriculaDAO();
        try {
            $arrWhere = array();
            $arrWhere[] = 'id_matricula = ' . $aTO->getId_matricula();
            $arrWhere[] = 'id_mensagempadrao = ' . $aTO->getId_mensagempadrao();
            $where = implode(" AND ", $arrWhere);

            $dados = $dao->retornarAlerta($aTO, $where)->toArray();
            //print_r($dados);exit;
            /*if (empty($dados)) {
                THROW new Zend_Validate_Exception("Nenhum alerta encontrada.");
            }*/
            $this->mensageiro->setMensageiro($dados, Ead1_IMensageiro::SUCESSO);

            //$this->mensageiro = $dao->retornarAlerta($aTO, $where);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao retornar da alerta: " . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro;

    }
}

?>

<?php

/**
 * Classe com regras de negócio para o TCCBO
 * @author Elcio Mauo Guimarães - <elcioguimaraes@gmail.com>
 * @since 24/11/2012
 * @package models
 * @subpackage bo
 */
class PortalTCCBO extends TCCBO
{


    /**
     * Método que faz o UPLOAD do arquivo de TCC
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param array $dados
     * @param ArquivoTO | array $arquivo
     * @return Ead1_Mensageiro
     * @internal param de $array $arApTO AvaliacaoParametrosTO
     * @internal param AvaliacaoAlunoTO $aaTO
     */
    public function salvarArrNotaTCC(array $dados, $arquivo = null)
    {
        $mensagemRetorno = '';
        $this->dao->beginTransaction();
        try {

            if (empty($dados)) {
                throw new Zend_Exception('Nada foi informado!');
            }

            if (empty($dados["nu_notaparametro"])) {
                throw new Zend_Exception('Nenhuma nota informada!');
            }

            $aaTO = new AvaliacaoAlunoTO();
            $arApTO = null;
            $nu_nota_total = 0;

            $vwencerramento = new VwEncerramentoAlunosTO();
            $vwencerramento->setId_matricula($dados['id_matricula']);
            $vwencerramento->setId_saladeaula($dados['id_saladeaula']);
            $vwencerramento->fetch(false, true, true);
            if ($vwencerramento->getDt_encerramentoprofessor()) {
                throw new Zend_Exception('TCC já encerrado!');
            }

            //Parametros
            $tccto = new VwAvaliacaoParametroTCCTO();
            $tccto->setId_matricula($dados['id_matricula']);
            $tccto->setId_saladeaula($dados['id_saladeaula']);
            $parametros = $this->retornaVwAvaliacaoParametroTCC($tccto);
            if ($parametros->getTipo() == Ead1_IMensageiro::SUCESSO) {

                foreach ($parametros->getMensagem() as $to) {

                    /**
                     * Excluindo duplicidades
                     */
                    $apVTO = new AvaliacaoParametrosTO();
                    $apVTO->setId_avaliacaoaluno($to->getid_avaliacaoaluno());
                    $apVTO->setId_parametrotcc($to->getid_parametrotcc());
                    $mensageiro = $this->excluirAvaliacaoParametroTCC($apVTO);
                    if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                        throw new Zend_Exception($mensageiro->getFirstMensagem());
                    }
                }

                $parametros = $this->retornaVwAvaliacaoParametroTCC($tccto);

                foreach ($parametros->getMensagem() as $to) {

                    if ($dados["nu_notaparametro"][$to->getId_parametrotcc()] == "") {
                        throw new Zend_Exception('Todas as Notas são Obrigatórias!');
                    }

                    $apTO = new AvaliacaoParametrosTO();
                    $apTO->setId_parametrotcc($to->getId_parametrotcc());
                    $apTO->setNu_valor($dados["nu_notaparametro"][$to->getId_parametrotcc()]);

                    if ($apTO->getNu_valor() > $to->getNu_notamaxparam()) {
                        throw new Zend_Exception('A Nota do parâmetro <strong>' . $to->getSt_parametrotcc() . '</strong> não pode ser superior a <strong>' . $to->getNu_notamaxparam() . '</strong>.');
                    }

                    $nu_nota = str_replace(',', '.', $dados["nu_notaparametro"][$to->getId_parametrotcc()]);
                    $nu_nota_total += number_format($nu_nota, 2, '.', '');
                    $arApTO[] = $apTO;

                }

            }

            $aaTO->setid_matricula($dados['id_matricula']);
            $aaTO->setId_avaliacao($dados['id_avaliacao']);
            $aaTO->setSt_nota($nu_nota_total);
            $aaTO->setId_avaliacaoconjuntoreferencia($dados['id_avaliacaoconjuntoreferencia']);
            $aaTO->setDt_avaliacao(new Zend_Date());
            $aaTO->setSt_tituloavaliacao($dados['st_tituloavaliacao']);

            if (!$arquivo) {
                $aaTO->setId_upload($dados['id_upload']);
            }

            $me = self::salvarNotaTCC($aaTO, $arApTO, $arquivo);

            if ($me->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Exception($me->getFirstMensagem());
            }

            $mensagemRetorno = $me->getFirstMensagem();

            $parametrosDeBusca = array(
                'id_saladeaula' => (int)$dados['id_saladeaula'],
                'id_matriculadisciplina' => (int)$vwencerramento->getId_matriculadisciplina(),
                'bl_ativo' => 1
            );

            $alocacao = (new \G2\Negocio\Alocacao())
                ->atualizaSituacaoTcc($parametrosDeBusca, (int)$dados['id_situacaotcc']);

            if (!$alocacao) {
                throw new Zend_Exception('Não foi possível modificar a situacao do TCC.');
            }

            $st_tramite = 'TCC recebido pelo Orientador.';

            //salva tramite da devolução do TCC ao aluno
            $ngTramite = new \G2\Negocio\Tramite();
            if (!$ngTramite->salvarTramiteMatricula($st_tramite, $dados['id_matricula'],
                    \G2\Constante\TipoTramite::MATRICULA_TCC) instanceof \G2\Entity\TramiteMatricula
            ) {
                $mensagemRetorno .= ' Não foi possível salvar tramite.';
            }

            $this->dao->commit();
            return new Ead1_Mensageiro($mensagemRetorno, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $this->dao->rollBack();
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);

        }

    }

}

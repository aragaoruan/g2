<?php

/**
 * Description of PortalMensagemBO
 *
 * @author Felipe Pastor
 */
class PortalMensagemBO extends Ead1_BO
{

    public function __construct()
    {
        parent::__construct();
    }

    public function cadastrarAlerta(AlertaTO $aTO)
    {

    }

    public function retornarMensagemAluno(array $arg = null)
    {
        try {
            $meiosPagamento = new \G2\Negocio\MensagemPortal;
            $arg = array(
                "id_usuario" => Ead1_Sessao::getSessaoUsuario()->id_usuario,
                "id_matricula" => Ead1_Sessao::getSessaoProjetoPedagogico()->getId_matricula()
            );

            $result = $meiosPagamento->findVwEnviarMensagemAluno($arg);

            if ($result != false) {
                $nao_lidas = 0;

                foreach ($result as $k => $v) {
                    $result[$k]['dt_enviar'] = date('d-m-Y G:i', strtotime($v['dt_enviar']));

                    if ($v['id_evolucao'] == 42) {
                        $nao_lidas++;
                    }
                }

                $arg_sessao = array(
                    'nr_mensagem' => sizeof($result),
                    'nr_mensagem_nao_lidas' => $nao_lidas,
                    'bl_mensagem' => sizeof($result) != 0 ? '1' : '0'
                );

                Ead1_Sessao::setSessionMensagem($arg_sessao);
                return $result;
            } else {
                return false;
            }
        } catch (Zend_Exception $e) {
            throw new Zend_Exception($e->getMessage());
        }

    }


    public function verificaMensagemAgendamento()
    {
//        $mensagemPortal = new \G2\Negocio\MensagemPortal;
    }

    public function verificaMensagemAgendamentoRecuperacao()
    {

    }
}
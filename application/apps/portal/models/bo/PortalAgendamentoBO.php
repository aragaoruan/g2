<?php

/**
 * Classe Que encapsula regras de negocio de agendamento do portal
 * @author eduardoromao
 */
class PortalAgendamentoBO extends Ead1_BO {

      public $mensageiro;
    private $dao;
    
    public function __construct() {
        $this->mensageiro = new Ead1_Mensageiro();
        $this->dao = new PessoaDAO();
    }

    /**
     * Método que possui regras específicas do portal para confirmar agendamentos
     * @param AvaliacaoAgendamentoTO $to
     * @return Ead1_Mensageiro
     */
    public function confirmarAgendamento(AvaliacaoAgendamentoTO $to) {
        try {
            $this->pesquisaGratuidadeAvaliacao($to);
            $this->pesquisarAgendamentoNoPrazo($to);
            $to->setId_situacao(AvaliacaoAgendamentoTO::SITUACAO_AGENDADO);
            $to->setBl_ativo(true);
            $portalAvaliacaoBO = new PortalAvaliacaoBO();
            $dadosValidacao = array('dt_agendamento' => $to->getDt_agendamento(),
                'id_matricula' => $to->getId_matricula(),
                'id_avaliacao' => $to->getId_avaliacao(),
                'id_situacao' => $to->getId_situacao());
            if ($portalAvaliacaoBO->verificaDataAgendamento(new VwAvaliacaoAplicacaoAgendamentoTO($dadosValidacao))) {
                $avaliacaoRO = new AvaliacaoRO();
                $mensageiroAvaliacao = $avaliacaoRO->salvarAvaliacaoAgendamento($to);
                $mensageiroAvaliacao->subtractMensageiro();
            } else {
                THROW new Zend_Validate_Exception('Já Existe uma Avaliação Agendada para o Dia Selecionado.');
            }

            $this->mensageiro = $mensageiroAvaliacao;
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro;
    }

    /**
     * Método que verifica se a data é possível para o agendamento!
     * @param AvaliacaoAgendamentoTO $to
     * @throws Zend_Exception
     */
    public function pesquisarAgendamentoNoPrazo(AvaliacaoAgendamentoTO $to) {
        $projetoPedagogicoTO = new ProjetoPedagogicoTO();
        $projetoPedagogicoTO->setId_projetopedagogico(Ead1_Sessao::getSessaoProjetoPedagogico()->id_projetopedagogico);
        $projetoPedagogicoTO->fetch(true, true, true);

        $avaliacaoAgendamentoORM = new AvaliacaoAgendamentoORM();
        $agendamento = $avaliacaoAgendamentoORM->fetchRow("id_avaliacao = " . $to->getId_avaliacao() . " AND id_matricula = " . $to->getId_matricula() . " AND bl_ativo = '1'", "dt_agendamento DESC");


        if ($agendamento) {
            if ($agendamento->dt_agendamento instanceof DateTime) {
                list($ano, $mes, $dia) = explode("-", $agendamento->dt_agendamento->format('Y-m-d'));
            } else {
                list($ano, $mes, $dia) = explode("-", $agendamento->dt_agendamento);
            }
        } else {
            $ano = date("Y");
            $mes = date("m");
            $dia = date("d");
        }


        $hoje = new DateTime();
        if (!$this->comparaData($hoje->format('Y-m-d'), $to->getDt_agendamento()->toString('Y-MM-dd'))) {
            $mensagem = "A data precisa ser posterior à hoje dia " . $hoje->format('d/m/Y');
            THROW new Zend_Validate_Exception($mensagem);
        }

        $dataLimite = mktime(0, 0, 0, $mes, $dia + $projetoPedagogicoTO->getNu_prazoagendamento(), $ano);
        $dataInicialParaAgendamento = strftime("%Y-%m-%d", $dataLimite);
        if (!$this->comparaData($dataInicialParaAgendamento, $to->getDt_agendamento()->toString('dd/MM/Y'))) {
            $mensagem = "A data precisa ser posterior à " . strftime("%d/%m/%Y", $dataLimite);
            THROW new Zend_Validate_Exception($mensagem);
        }

        if (!$this->comparaData($hoje->format('Y-m-d'), $dataInicialParaAgendamento)) {
            $mensagem = "Essa avaliação já foi agendada para a data" . strftime("%d/%m/%Y", $dataLimite) . "Essa avaliação só pode ser agendada novamente após esse agendamento.";
            THROW new Zend_Validate_Exception($mensagem);
        }
    }

    /**
     * Método que pesquisa gratuidade da avaliação e retorna um Throw caso não seja possível realizar um agendamento por não 
     * 	ter mais agendamentos gratuitos naquela avaliação.
     * @param AvaliacaoAgendamentoTO $to
     * @throws Zend_Exception
     */
    public function pesquisaGratuidadeAvaliacao(AvaliacaoAgendamentoTO $to) {
        $produtoAvaliacaoTO = new ProdutoAvaliacaoTO();
        $produtoAvaliacaoTO->setId_avaliacao($to->getId_avaliacao());
        $produtoAvaliacaoTO->setId_entidade(Ead1_Sessao::getSessaoEntidade()->id_entidade);
        $produtoAvaliacaoTO->fetch(false, true, true);
        if ($produtoAvaliacaoTO->getId_produto()) {
            $produtoTO = new ProdutoTO();
            $produtoTO->setId_produto($produtoAvaliacaoTO->getId_produto());
            $produtoTO->fetch(true, true, true);
            $avaliacaoAgendamentoTO = new AvaliacaoAgendamentoTO();
            $avaliacaoAgendamentoTO->setId_avaliacao($to->getId_avaliacao());
            $avaliacaoAgendamentoTO->setId_matricula($to->getId_matricula());
            $quantidadeAgendamentos = count($avaliacaoAgendamentoTO->fetch());

            if ($produtoTO->getNu_gratuito() <= $quantidadeAgendamentos) {
                THROW new Zend_Validate_Exception("Para reagendar essa avalição por favor procure a secretária da sua instituição.");
            }
        } else {
            THROW new Zend_Validate_Exception("Agendamento Não Disponível, entre em contato com a secretária.");
        }
    }

    /**
     * Método que verifica quantos agendamentos da avaliação foram feitos e se podem ser feitos outros
     * @param int $idAvaliacao
     * @return Ead1_Mensageiro
     */
    public function verificaProdutoAvaliacaoAgendamento($idAvaliacao) {
        $session = Zend_Session::namespaceGet('p_projetopedagogico');
        $idMatricula = $session['id_matricula'];

        $produtoAvaliacaoTO = new ProdutoAvaliacaoTO();
        $produtoAvaliacaoTO->setId_avaliacao($idAvaliacao);
        $produtoAvaliacaoTO->setId_entidade($session['id_entidade']);
        $produtoAvaliacaoTO->fetch(true, true, true);

        if ($produtoAvaliacaoTO->getId_produto()) {
            $vwProdutoTO = new VwProdutoTO();
            $vwProdutoTO->setId_produto($produtoAvaliacaoTO->getId_produto());
            $vwProdutoTO->fetch(true, true, true);

            // TODO validar quantidade de vezes que este produto foi solicitado para a matrícula em questão
        }


        return new Ead1_Mensageiro($idAvaliacao);
    }

    /**
     * Atualizar dados de usuário
     * @author Débora Castro <debora.castro@unyleya.com.br>
     */
    public function atualizarDadosBasicosUsuario(array $data) {
        try {
            if ($data) {
                $this->dao->beginTransaction();

                $negocioPessoa = new \G2\Negocio\Pessoa();
                //set dados
                $uTO = $negocioPessoa->preencheUsuarioTO($data); //Preenche usuario
//              Zend_Debug::dump($uTO);die;
                //Endereço            
                $endTO = $negocioPessoa->preencheEnderecoTO($data);

//              Zend_Debug::dump($endTO);die;
                $pendTO = new PessoaEnderecoTO();
                $pendTO->setId_usuario($uTO->getId_usuario());
                $pendTO->setId_endereco($endTO->getId_endereco());
                $pendTO->setBl_padrao(true);
                $pendTO->fetch(false, true, true);

                //envia para BO reutilizando metodo 
                $pessoaBo = new PessoaBO();
                $result = $pessoaBo->_procedimentoSalvarPessoaEndereco($uTO, $pendTO, $endTO);
                $this->dao->commit();
                return $this->mensageiro->setMensageiro($result, Ead1_IMensageiro::SUCESSO);
            }
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro;
        }
    }
}
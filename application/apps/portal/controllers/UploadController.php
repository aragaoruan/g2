<?php

/**
 * Controller de Index (Portal) do Portal do Aluno
 * @author eduardoromao
 */
class Portal_UploadController extends \Ead1_Controller
{


    /**
     * @see PortalController::init()
     */
    public function init()
    {
        parent::init();

        $this->ajax();

        $aruri = explode('?',$this->getRequest()->getRequestUri());

        $pos = strrpos($aruri[0], '.') + 1;
        $ext = substr($aruri[0], $pos);
        $arquivo = realpath(APPLICATION_REAL_PATH . $aruri[0]);

        if ($arquivo) {
            header("Content-Type: " . $ext); // informa o tipo do arquivo ao navegador
            header("Content-Length: " . filesize($arquivo)); // informa o tamanho do arquivo ao navegador
            header("Content-Disposition: attachment; filename=" . basename($arquivo)); // informa ao navegador que é tipo anexo e faz abrir a janela de download, tambem informa o nome do arquivo
            readfile($arquivo);
        } else {

            $this->getResponse()->setHttpResponseCode(404);
            $this->json(array('status' => 'erro',
                'mensagem' => 'Arquivo não encontrado!'));

        }
    }
}


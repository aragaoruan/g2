<?php
/**
 * Classe com métodos de alocação em sala de aula
 * @author eduardoromao
 */
class Portal_AlocacaoController extends PortalBase{
	
	private $_bo;
	
	public function init(){
		parent::init();
		$this->_bo = new PortalAlocacaoBO();
	}
	
	/**
	 * Tela inicial de alocação
	 */
	public function indexAction(){
		try{
			
			//Grava log de acesso
			$logTO = new LogAcessoTO();
			$logTO->setId_funcionalidade(LogAcessoTO::ALOCACAO);
			$logRO = new LogRO();
			$logRO->gravaLog($logTO);
			
			$this->view->cbModulo = array();
			$tempObject = new VwDadosAlocacaoTO();
			$tempObject->nu_disciplinasdisponiveis = null; 	
			$this->view->dadosAlocacaoTO = $tempObject;
			$this->view->erro = null;
			$erros = null;
			$moduloTO = new ModuloTO();
			$moduloTO->setBl_ativo(true);
			$moduloTO->setId_projetopedagogico(Ead1_Sessao::getSessaoProjetoPedagogico()->id_projetopedagogico);
			$mensageiroModulo = $this->_bo->retornarComboModulo($moduloTO);
			$arrComboModulo = array();
			try{
				$mensageiroModulo->subtractMensageiro();
				$arrComboModulo = $mensageiroModulo->getMensagem();
			}catch (Zend_Exception $e){
				$erros .= $e->getMessage()."\n";
			}
			$this->view->cbModulo = $this->gerarCombo($arrComboModulo);
			$matriculaTO = new MatriculaTO();
			$matriculaTO->setId_matricula(Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula);
			$mensageiroAlocacao = $this->_bo->dadosAlocacao($matriculaTO);
			try{
				$mensageiroAlocacao->subtractMensageiro();
				$this->view->dadosAlocacaoTO = $mensageiroAlocacao->getFirstMensagem();
			}catch (Zend_Exception $e){
				$erros .= $e->getMessage()."\n";
			}
			if($erros){
				THROW new Zend_Exception($erros);
			}
		}catch (Zend_Exception $e){
			$this->view->erro = new Ead1_Mensageiro($erros,
						($mensageiroAlocacao->getTipo() == Ead1_IMensageiro::ERRO || $mensageiroModulo->getTipo() == Ead1_IMensageiro::ERRO ? 
								Ead1_IMensageiro::ERRO : Ead1_IMensageiro::AVISO)
					);
		}
	}
	
	/**
	 * Action que retorna as disciplinas por Modulo
	 */
	public function disciplinasPorModuloAction(){
		$moduloTO = new ModuloTO();
		$moduloTO->setId_modulo($this->_getParam('id_modulo'));
		$resposta = $this->_bo->retornarDisciplinasPorModulo($moduloTO);
		$this->resposta($resposta); 
	}
	
	/**
	 * Action que retorna as salas de aula de uma disciplina de um projeto
	 */
	public function salaDeAulaAction(){
		$to = new MatriculaDisciplinaTO();
		$to->setId_matricula(Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula);
		$to->setId_disciplina($this->_getParam('id_disciplina'));
		$resposta = $this->_bo->retornarSalaDeAulaDisciplinaProjeto($to);
		$this->resposta($resposta); 
	}
	
	/**
	 * Action de alocar aluno
	 */
	public function alocarAction(){
		$resposta = $this->_bo->alocar($this->getAllParams());
		$this->resposta($resposta); 		
	}
}
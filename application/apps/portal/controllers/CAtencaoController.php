<?php

/**
 * Classe com métodos da central de atenção
 */

class Portal_CAtencaoController extends PortalBase
{

    /** @var  PortalCAtencaoBO */
    private $_bo;

    public function init()
    {
        parent::init();
        $this->_bo = new PortalCAtencaoBO();

        if ($this->_helper->FlashMessenger->hasMessages()) {
            $this->view->messages = $this->_helper->FlashMessenger->getMessages();
        }
    }

    /**
     * Tela inicial da Central de Atenção
     */
    public function indexAction()
    {
        $erros = '';
        try {

            $logTO = new LogAcessoTO();
            $logTO->setId_funcionalidade(LogAcessoTO::CENTRAL_DE_ATENCAO);
            $log = new LogBO();
            $me = $log->gravaLog($logTO);

            $mensageiroEvolucao = $this->_bo->retornarComboEvolucao();
            $arrComboEvolucao = array();
            try {
                $mensageiroEvolucao->subtractMensageiro();
                $arrComboEvolucao = $mensageiroEvolucao->getMensagem();
            } catch (Zend_Exception $e) {
                $erros .= $e->getMessage() . "\n";
            }
            $this->view->cbEvolucao = $this->gerarCombo($arrComboEvolucao, ($this->_getParam('id_evolucao') != null ? $this->_getParam('id_evolucao') : ''));

            $mensageiroSituacao = $this->_bo->retornarComboSituacao();
            $arrComboSituacao = array();
            try {
                $mensageiroSituacao->subtractMensageiro();
                $arrComboSituacao = $mensageiroSituacao->getMensagem();
            } catch (Zend_Exception $e) {
                $erros .= $e->getMessage() . "\n";
            }
            $this->view->cbSituacao = $this->gerarCombo($arrComboSituacao, ($this->_getParam('id_situacao') != null ? $this->_getParam('id_situacao') : ''));

        } catch (Zend_Exception $e) {
            $this->view->erro = new Ead1_Mensageiro($erros, ($mensageiroEvolucao->getTipo() == Ead1_IMensageiro::ERRO || $mensageiroSituacao->getTipo() == Ead1_IMensageiro::ERRO ?
                Ead1_IMensageiro::ERRO : Ead1_IMensageiro::AVISO));
        }

        if (!$this->getRequest()->isXmlHttpRequest()):
            $this->_helper->layout->setLayout('layout');
        endif;
    }

    /**
     * Método que lista as ocorrências
     * com ou sem parametros
     */
    public function listaOcorrenciasAction()
    {
        $negocio = new \G2\Negocio\Ocorrencia();
        //tentativas de acesso via url sem selecão de perfil ou no caso de aluno sem seleção da matrícula
        if (is_null($negocio->sessao->id_perfilpedagogico)
            || ($negocio->sessao->id_perfilpedagogico == \G2\Constante\PerfilPedagogico::ALUNO
                && is_null($negocio->sessao->id_matricula))
        ) {
            $this->_helper->layout->setLayout('layout');
            $this->view
                ->erro = 'Caro usuário, é necessário selecionar um perfil de acesso para utilizar essa ferramenta.';
            $this->view->erroSessao = Ead1_IMensageiro::ERRO;
        } else {
            $negocio = new \G2\Negocio\CAtencao();
            $ocorrenciaTO = new VwOcorrenciaTO();

            if ($this->_getParam('id_evolucao') != null) {
                $ocorrenciaTO->setId_evolucao($this->_getParam('id_evolucao'));
            }

            if ($this->_getParam('id_situacao') != null) {
                $ocorrenciaTO->setId_situacao($this->_getParam('id_situacao'));
            }

            if ($this->_getParam('cb_id_assuntoco') != null && $this->_getParam('cb_id_assuntopai')) {
                $ocorrenciaTO->setId_assuntoco($this->_getParam('cb_id_assuntoco'));
                $ocorrenciaTO->setId_assuntocopai($this->_getParam('cb_id_assuntopai'));
            }

            $ocorrenciaTO->setId_tipoocorrencia(AssuntoCoTO::TIPO_OCORRENCIA_TUTOR);
            $ocorrenciaTO->setId_entidade($negocio->sessao->id_entidade);

            if (($negocio->sessao->id_perfilpedagogico == PerfilPedagogicoTO::ALUNO)
                || ($negocio->sessao->id_perfilpedagogico == PerfilPedagogicoTO::ALUNO_INSTITUCIONAL)
            ) {
                $ocorrenciaTO->setId_matricula($negocio->sessao->id_matricula);
                $ocorrenciaTO->setId_tipoocorrencia(AssuntoCoTO::TIPO_OCORRENCIA_ALUNO);
            }

            $resposta = $this->_bo->retornarOcorrencias($ocorrenciaTO);
            if ($resposta->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $resposta->subtractMensageiro();
                $this->view->ocorrencias = Zend_Json::encode($resposta->getMensagem());
            } else {
                $this->view->ocorrencias = Zend_Json::encode(null);
            }
            $this->view->retorno = $resposta->getTipo();
        }


    }

    /**
     * Tela para cadastro de uma nova ocorrência
     */
    public function novaOcorrenciaAction()
    {
        $negocio = new \G2\Negocio\CAtencao();
        $this->_helper->layout->setLayout('layout');
        $erros = null;
        try {
            //tentativas de acesso via url sem selecão de perfil ou no caso de aluno sem seleção da matrícula
            if (is_null($negocio->sessao->id_perfilpedagogico) || ($negocio->sessao->id_perfilpedagogico == 5 && is_null($negocio->sessao->id_matricula))) {
                $mensageiroSessao = new Ead1_Mensageiro(null, Ead1_IMensageiro::ERRO);
                throw new Zend_Exception('Caro usuário, é necessário selecionar um perfil de acesso para utilizar essa ferramenta.');
            }
            $mensageiroTipo = $this->_bo->retornarComboCategoriaOcorrencia();
            $arrComboTipo = array();
            try {
                $mensageiroTipo->subtractMensageiro();
                $arrComboTipo = $mensageiroTipo->getMensagem();
            } catch (Zend_Exception $e) {
                $erros .= $e->getMessage() . "\n";
            }
            $this->view->cbCategoriaOcorrencia = $this->gerarCombo($arrComboTipo);

            $assunto = new VwAssuntosTO();
            //aluno
            if (($negocio->sessao->id_perfilpedagogico == PerfilPedagogicoTO::ALUNO)
                || ($negocio->sessao->id_perfilpedagogico == PerfilPedagogicoTO::ALUNO_INSTITUCIONAL)
            ) {
                $assunto->setId_tipoocorrencia(TipoOcorrenciaTO::ALUNO);
                $assunto->setBl_interno(false);
            } elseif (Ead1_Sessao::getSessaoPerfil()->id_perfilpedagogico == PerfilPedagogicoTO::PROFESSOR) {
                $assunto->setId_tipoocorrencia(TipoOcorrenciaTO::TUTOR);
            }

            $mensageiroAssuntoPai = $this->_bo->retornaComboAssuntoPai($assunto);
            $arrComboAssuntoPai = array();
            try {
                $mensageiroAssuntoPai->subtractMensageiro();
                $arrComboAssuntoPai = $mensageiroAssuntoPai->getMensagem();
            } catch (Zend_Exception $e) {
                $erros .= $e->getMessage() . "\n";
            }
            $this->view->cbAssuntoPai = $this->gerarCombo($arrComboAssuntoPai);

            if ($negocio->sessao->id_saladeaula != null) {
                $selected = $negocio->sessao->id_saladeaula;
            } else {
                $selected = null;
            }


            $this->view->id_entidade = $negocio->sessao->id_entidade;
            $this->view->id_usuario = $negocio->sessao->id_usuario;
            $this->view->id_matricula = $negocio->sessao->id_matricula;

            $mensageiroSala = $this->_bo->retornaComboSala();
            $arrComboSala = array();
            try {
                $mensageiroSala->subtractMensageiro();
                $arrComboSala = $mensageiroSala->getMensagem();
            } catch (Zend_Exception $e) {

                $erros .= $e->getMessage() . "\n";
            }
            $this->view->cbSalaDeAula = $this->gerarCombo($arrComboSala, $selected);
            $bl_comboSala = true;
            if (($negocio->sessao->id_perfilpedagogico == PerfilPedagogicoTO::ALUNO) || ($negocio->sessao->id_perfilpedagogico == PerfilPedagogicoTO::ALUNO_INSTITUCIONAL)) {
                $matriculaTO = new MatriculaTO();
                $matriculaTO->setId_matricula($negocio->sessao->id_matricula);
                $matriculaTO->fetch(true, true, true);
                if ($matriculaTO->getId_evolucao() == MatriculaTO::EVOLUCAO_BLOQUEADO || $matriculaTO->getId_evolucao() == MatriculaTO::EVOLUCAO_TRANCADO) {
                    $bl_comboSala = false;
                }
            }
            $this->view->bl_combosala = $bl_comboSala;

        } catch (Zend_Exception $e) {
            if (isset($mensageiroSessao)) {
                $this->view->erroSessao = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
            } else {
                $this->view->erro = new Ead1_Mensageiro($e->getMessage(), ($mensageiroTipo->getTipo() == Ead1_IMensageiro::ERRO || $mensageiroAssuntoPai->getTipo() == Ead1_IMensageiro::ERRO ?
                    Ead1_IMensageiro::ERRO : Ead1_IMensageiro::AVISO)
                );
            }
        }
    }

    /**
     * Retorna combo de Assuntos a partir da escolha de um assunto-pai
     * Retorno via AJAX
     */
    public function retornaComboAssuntocoAction()
    {
        $erros = '';
        $negocio = new \G2\Negocio\CAtencao();
        try {
            $assunto = new VwAssuntosTO();

            if ($negocio->sessao->id_perfilpedagogico == \G2\Constante\PerfilPedagogico::ALUNO) { //aluno
                $assunto->setId_tipoocorrencia(\G2\Constante\TipoOcorrencia::ALUNO);
                $assunto->setBl_interno(false);
            } else if ($negocio->sessao->id_perfilpedagogico == \G2\Constante\PerfilPedagogico::PROFESSOR) { //tutor
                $assunto->setId_tipoocorrencia(\G2\Constante\TipoOcorrencia::TUTOR);
            }


            $assunto->setId_assuntocopai($this->_getParam('id_assuntocopai'));

            $mensageiroAssunto = $this->_bo->retornaComboAssuntoco($assunto);

            $arrComboAssunto = array();
            try {
                $mensageiroAssunto->subtractMensageiro();
                $arrComboAssunto = $mensageiroAssunto->getMensagem();
            } catch (Zend_Exception $e) {
                $erros .= $e->getMessage() . "\n";
            }

            $this->view->conteudo = $this->gerarCombo($arrComboAssunto);

        } catch (Zend_Exception $e) {
            $this->view->erro = new Ead1_Mensageiro($erros, Ead1_IMensageiro::ERRO);
        }
    }

    public function retornaDescricaoAssuntocoAction()
    {
        try {
            if (!$this->_getParam('id_assuntoco')) {
                throw new Zend_Exception('o id_assuntoco é obrigatório e não foi passado!');
            }
            $assunto = new AssuntoCoTO();
            $assunto->setId_assuntoco($this->_getParam('id_assuntoco'));
            $assunto->fetch(false, true, true);
            if (!$assunto->getId_textosistema()) {
                throw new Zend_Exception('Nenhum texto cadastrado para o assunto!');
            }
            $texto = new TextoSistemaTO();
            $texto->setId_textosistema($assunto->getId_textosistema());

            $this->view->conteudo = $this->_bo->retornaTextoAssunto($texto);

        } catch (Zend_Exception $e) {
            $this->ajax();
            $this->resposta(null);
        }
    }

    /**
     * Salva Ocorrência
     */
    public function salvarOcorrenciaAction()
    {
        $this->_helper->layout->setLayout('layout');
        try {
            $negocio = new \G2\Negocio\CAtencao();
            $anexo = isset($_FILES['anexo']) ? $_FILES['anexo'] : null;

            $return = $negocio->salvarOcorrencia($this->getAllParams(), $anexo);
            if ($return->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $this->_helper->flashMessenger->addMessage("
                                                    <div class='info-box green uny ocorrencia-box'>
                                                            <i class='icon-ok-sign'></i>
                                                        <div class='msg'>
                                                        Ocorrência cadastrada com sucesso!
                                                        </div>
                                                        <a href='javascript:void(0)' class='info-box-remove'><i class='icon-remove'></i></a>
                                                    </div>");
                $this->redirect('/portal/C-atencao');
            } else {
                $this->_helper->flashMessenger->addMessage('
                                                            <div class="info-box yellow uny ocorrencia-box">
                                                                    <i class="icon-warning-sign"></i>
                                                                <div class="msg">
                                                                ' . $return->getFirstMensagem() . '
                                                                </div>
                                                                <a href="javascript:void(0)" class="info-box-remove"><i class="icon-remove"></i></a>
                                                            </div>');
                $this->redirect('/portal/C-atencao');
            }
        } catch (Zend_Exception $e) {
            $this->_helper->flashMessenger->addMessage('
            <div class="info-box yellow uny ocorrencia-box">
                    <i class="icon-warning-sign"></i>
                <div class="msg">
                Erro ao cadastrar ocorrência. Favor tentar novamente mais tarde ou contate-nos através do fale conosco.
                </div>
                <a href="javascript:void(0)" class="info-box-remove"><i class="icon-remove"></i></a>
            </div>');
            $this->redirect('/portal/C-atencao');
        }
    }

    /**
     *  Salva Trâmite manual
     */
    public function salvarTramiteAction()
    {
        try {
            if (!empty($_FILES['anexo']['name'])) {
                $arquivo = $_FILES['anexo'];
            } else {
                $arquivo = null;
            }
            $negocio = new \G2\Negocio\CAtencao();
            $return = $negocio->novoTramite($this->getAllParams(), $arquivo);

            $this->view->id_ocorrencia = $this->_getParam('id_ocorrencia');
            $this->view->id_evolucao = $this->_getParam('id_evolucao');
            $this->view->id_situacao = $this->_getParam('id_situacao');
            $this->_helper->layout->setLayout('layout');

            if ($return->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $this->_helper->flashMessenger->addMessage("
                                                            <div class='info-box green uny ocorrencia-box'>
                                                                    <i class='icon-ok-sign'></i>
                                                                <div class='msg'>
                                                                Trâmite cadastrado com sucesso!
                                                                </div>
                                                                <a href='javascript:void(0)' class='info-box-remove'><i class='icon-remove'></i></a>
                                                            </div>");
            } else {
                $mensagem = $return->getFirstMensagem();
                $this->_helper->flashMessenger->addMessage("
                                                            <div class='info-box red uny ocorrencia-box'>
                                                                    <i class='icon-ok-sign'></i>
                                                                <div class='msg'>
                                                                $mensagem
                                                                </div>
                                                                <a href='javascript:void(0)' class='info-box-remove'><i class='icon-remove'></i></a>
                                                            </div>");
            }
        } catch (Zend_Exception $e) {
            $this->_helper->flashMessenger->addMessage('
            <div class="info-box yellow uny ocorrencia-box">
                    <i class="icon-warning-sign"></i>
                <div class="msg">
                Erro ao cadastrar uma nova interação. Por favor, contate nos através do fale conosco.
                </div>
                <a href="javascript:void(0)" class="info-box-remove"><i class="icon-remove"></i></a>
            </div>');
        }

        $this->redirect('/portal/c-atencao/ocorrencia/id_ocorrencia/' . $this->_getParam('id_ocorrencia'));
    }

    /**
     *  Mostra detalhes da ocorrência e seus respectivos trâmites e uploads
     * @deprecated new $this->ocorrenciaAction
     */
    public function detalharocorrenciaAction()
    {
        $ocorrencia = new VwOcorrenciaTO();
        $ocorrencia->setId_ocorrencia($this->_getParam('id_ocorrencia'));
        $ocorrencia = $this->_bo->retornarOcorrencias($ocorrencia)->getFirstMensagem();
        $this->view->interacoes = $this->_bo->retornaInteracoesOcorrencia($ocorrencia);
        $this->view->ocorrencia = $ocorrencia;
        $this->view->id_evolucao = $this->_getParam('id_evolucao');
        $this->view->id_situacao = $this->_getParam('id_situacao');

        $vwNucleoOcorrencia = new VwNucleoOcorrenciaTO();
        $vwNucleoOcorrencia->setId_ocorrencia($ocorrencia->getId_ocorrencia());
        $vwNucleoOcorrencia->fetch(true, true, true);
        /**
         * @var VwNucleoOcorrenciaTO
         */
        $this->view->vwNucleoOcorrencia = $vwNucleoOcorrencia;

        if (!$this->getRequest()->isXmlHttpRequest()):
            $this->_helper->layout->setLayout('layout');
        endif;
    }

    /**
     *  Método que altera uma ocorrência
     */
    public function alterarOcorrenciaAction()
    {
        try {

//            Validando sempre ir com os paramentros corretos
            if (!empty($this->_getParam('id_ocorrencia'))
                || !empty($this->_getParam('id_evolucao'))
                || !empty($this->_getParam('id_situacao'))) {

                $to = new OcorrenciaTO();
                $to->setId_ocorrencia($this->_getParam('id_ocorrencia'));
                $to->setId_evolucao($this->_getParam('id_evolucao'));
                $to->setId_situacao($this->_getParam('id_situacao'));


                $resposta = $this->_bo->alterarOcorrencia($to, null, null, false);
                if ($resposta->getTipo() == Ead1_IMensageiro::SUCESSO) {
                    $this->view->class = 'green';
                    $this->view->subClass = 'ok';
                    $this->view->retorno = 'Ocorrência encerrada com sucesso.';
                } else {
                    $this->view->class = 'red';
                    $this->view->subClass = 'warning';
                    $this->view->retorno = 'Erro no encerramento da ocorrência. Por favor, entre em contato pelo fale conosco.';
                }
            } else {
                $this->view->class = 'red';
                $this->view->subClass = 'warning';
                $this->view->retorno = 'Erro no encerramento da ocorrência. Por favor, entre em contato pelo fale conosco.';
            }


        } catch (Exception $e) {
            $this->view->class = 'red';
            $this->view->subClass = 'warning';
            $this->view->retorno = 'Erro no encerramento da ocorrência. Por favor, entre em contato pelo fale conosco.';
        }

    }

    /**
     * Método que reabre uma ocorrência
     * Salva uma interação como justificativa para a reabertura
     * @throws Zend_Exception
     */
    public function reabrirOcorrenciaAction()
    {

        $this->view->showClass = 'show';
        $negocio = new \G2\Negocio\CAtencao();
        $return = $negocio->reabrirOcorrencia($this->getAllParams());
        if ($return->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $this->_helper->flashMessenger->addMessage("
            <div class='info-box green uny ocorrencia-box'>
                    <i class='icon-ok-sign'></i>
                <div class='msg'>
                Ocorrência reaberta com sucesso
                </div>
                <a href='javascript:void(0)' class='info-box-remove'><i class='icon-remove'></i></a>
            </div>");
        } else {
            $this->_helper->flashMessenger->addMessage('
            <div class="info-box yellow uny ocorrencia-box">
                    <i class="icon-warning-sign"></i>
                <div class="msg">
                Erro ao reabrir a ocorrência. Por favor, contate nos através do fale conosco.
                </div>
                <a href="javascript:void(0)" class="info-box-remove"><i class="icon-remove"></i></a>
            </div>');
        }
        $this->redirect('/portal/c-atencao/ocorrencia/id_ocorrencia/' . $this->_getParam('id_ocorrencia'));

    }

    /**
     *  mostra ocorrencia unica por ID da ocorrencia
     * @author Denise Xavier <denise.xavier@unyleya.com.br>
     * @param id_ocorrencia
     * @name ocorrenciaAction
     */
    public function ocorrenciaAction()
    {
        $this->_helper->layout()->setLayout('layout');

        $id_ocorrencia = $this->getParam('id_ocorrencia');

        if ($id_ocorrencia) {
            $negocio = new \G2\Negocio\CAtencao();
            $retorno = $negocio->detalhaOcorrencia($id_ocorrencia);

            if ($retorno->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $this->view->interacoes = $retorno->getMensagem('interacoes');
                $this->view->ocorrencia = $retorno->getMensagem('ocorrencia');

                if ($this->view->ocorrencia && $this->view->ocorrencia->getId_saladeaula()) {
                    // Buscar sala de aula
                    $this->view->sala = $negocio->find('G2\Entity\SalaDeAula', $this->view->ocorrencia->getId_saladeaula());
                } else {
                    $this->view->sala = null;
                }
            } else {
                $this->view->mensagem = $retorno->getMensagem();
            }

        } else {
            /*
             * Caso o parametro necessario para o carregamento da ocorrencia
             * então o usuario e automaticamente redirecionado para sua lista
             * de ocorrencias.
             */
            $this->redirect('/portal/c-atencao/index');
        }

    }

}

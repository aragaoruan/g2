<?php

/**
 * Class Portal_PerguntasFrequentesController
 * Classe controller para perguntas frequentes
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Portal_PerguntasFrequentesController extends PortalBase
{
    private $negocio;
    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\PerguntasFrequentes();
    }

    public function indexAction()
    {
        $this->_helper->layout->setLayout('layout');
        $request = $this->getRequest();

        $params = array();
        if($request->isPost()){
            $params = $request->getPost();
        }


        $perguntas = $this->negocio->retornarPesquisaPerguntas($params);
        $this->view->dados = $perguntas;

    }
}
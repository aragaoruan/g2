<?php

class PortalBase extends Ead1_Controller
{

    protected $baseUrl;
    protected $baseUrlModulo;

    /**
     * Método que inicializa as configurações gerais do ambiente
     */
    public function init()
    {

//        if ($this->_helper->FlashMessenger->hasMessages()) {
//            $this->view->messages = $this->_helper->FlashMessenger->getMessages();
//        }

        $config = Ead1_Ambiente::geral();
        $this->view->flexFolder = $config->flexFolder;
        $bootBaseUrl = $this->getInvokeArg('bootstrap')->getOption('baseUrl');
        $this->view->baseUrl = ($bootBaseUrl ? $bootBaseUrl : $config->baseUrl);
        if ($this->_getParam('module') == 'portal') {
            $this->baseUrl = $this->view->baseUrl;
            $this->baseUrlModulo = $this->baseUrl . "/portal";
            $this->view->module = $this->_getParam('module');
            $this->view->baseUrlModulo = $this->baseUrlModulo;

            $blSessao = (boolean)Ead1_Sessao::getSessaoUsuario()->id_usuario;
            if (!$this->validaExecoesURL() && !$blSessao) {
                $redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('Redirector');
                $redirector->gotoSimpleAndExit('index', 'index', 'portal');
            }
        }


        $this->setLayoutPath();
    }


    /**
     * Método que controla as permissões das requisições paras as actions e funcionalidadess
     * @see Zend_Controller_Action::preDispatch()
     */
    public function preDispatch()
    {
        //todo implementar logica de validacao
    }

    /**
     * @return TRUE-FALSE Metodo que valida se a URL acessada e publica
     * (modificação feita para utilização do formato do Zend).
     *
     * @author Rafael Bruno <rafaelbruno.ti@gmail.com>
     * @param boolean TRUE or FALSE
     */
    public function validaExecoesURL()
    {

        $urlPublica = array(
            array(
                'module' => 'portal',
                'controller' => 'index',
                'action' => array('index', 'logout', 'principal', 'validarecuperacaosenha', 'url')),
            array(
                'module' => 'portal',
                'controller' => 'login',
                'action' => array('validar', 'autenticacao-externa', 'autenticacao-direta', 'valida-url-resumida')),
            array(
                'module' => 'portal',
                'controller' => 'usuario',
                'action' => array('redefinir-senha', 'salvar-redefinicao-senha')),
        );

        foreach ($urlPublica as $value) {

            $module = ($value['module'] == $this->_request->getModuleName()) ? TRUE : FALSE;
            $controller = ($value['controller'] == $this->_request->getControllerName()) ? TRUE : FALSE;
            $action = (in_array($this->_request->getActionName(), $value['action']) or in_array('*', $value['action']));

            if ($module && $controller && $action) {
                return TRUE;
            }
        }

        return FALSE;
    }

}

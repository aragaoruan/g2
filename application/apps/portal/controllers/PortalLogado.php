<?php

class PortalLogado extends PortalBase {

    /**
     * Método que inicializa as configurações gerais do ambiente
     */
    public function init() {
        parent::init();
        $usuario = new UsuarioTO();
        if (!$usuario->getSessao()->id_usuario) {
            $this->_redirect('/portal');
        }
    }

}

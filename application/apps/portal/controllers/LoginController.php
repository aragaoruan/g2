<?php

/**
 * Classe de Controle de Login do Portal do Aluno
 * @author eduardoromao
 */
class Portal_LoginController extends PortalBase
{

    /**
     * @var PortalLoginBO $_bo
     */
    private $_bo;

    /**
     * @see PortalController::init()
     */
    public function init()
    {
        parent::init();
        $this->_bo = new PortalLoginBO();

        if ($this->_helper->FlashMessenger->hasMessages()) {
            $this->view->messages = $this->_helper->FlashMessenger->getMessages();
        }
    }

    /**
     * Action de Validação de Login
     *
     * @author helder silva<helder.silva@example.com>
     */
    public function validarAction()
    {
        try {

            $vwUsuarioPPTO = new VwUsuarioPerfilPedagogicoTO();

            if ($this->getRequest()->getPost()) {
                $body = $this->getRequest()->getPost();
            } else {
                $body = array();
                $body['login'] = '';
                $body['senha'] = '';
            }

            $login = (!isset($body['login']) || $body['login'] == '') ? Ead1_Sessao::getSessaoUsuario()->st_login : $body['login'];
            $senha = (!isset($body['senha']) || $body['senha'] == '') ? Ead1_Sessao::getSessaoUsuario()->st_senha : $body['senha'];
            $senha_sem_md5 = (!isset($body['senha']) || $body['senha'] == '') ? Ead1_Sessao::getSessaoUsuario()->st_senhaentidade : $body['senha'];

            $vwUsuarioPPTO->setSt_login($login);
            $vwUsuarioPPTO->setSt_email($login);
            $vwUsuarioPPTO->setSt_senha($senha_sem_md5);
            $vwUsuarioPPTO->setSt_senhaentidade($senha);

//            $params = array('st_senha' => $senha_sem_md5);

            $mensageiro = $this->_bo->validateUserPass($vwUsuarioPPTO);

            //Se houver algum usuário com determinada senha, entra no if, senão retorna senha incorreta


            if ($mensageiro && $mensageiro->tipo == 1) {
                $usersession = new \G2\Negocio\UsuarioSessao();
                // verifica se é permitido p login simultaneo
                $loginsimultaneo = true;
                if (getenv('id_entidade')) {
                    $esquemaNegocio = new \G2\Negocio\EsquemaConfiguracao();
                    $loginsimultaneo = $esquemaNegocio->retornarConfiguracaoLoginSimultaneo(getenv('id_entidade'));
                }
                //verifica se a sessão ja existe e é a atual
                $verificasessao = false;
                if ($loginsimultaneo == false) {
                    $verificasessao = $usersession->verificaHash($_COOKIE['PHPSESSID']);
                };
                //se houver post, efetua toda a vefificação de sessão
                if ($this->getRequest()->isPost()) {
                    $body = $this->getRequest()->getPost();
                    if ($verificasessao == true) {
                        //se a decisão do usuário for de kickar a outra sessão existente
                        if ($body['hash'] == "true") {
                            $usersession->deletaHash($_COOKIE['PHPSESSID']);
                            $usersession->gravaHash($_COOKIE['PHPSESSID']);
                            $this->_helper->json(array('redirect' => '/portal/login/selecionaperfil'));
                        } else {
                            session_destroy();
                            $this->_helper->json(array('flag' => true, 'senha' => $body['senha'], 'login' => $body['login']));
                        }
                        //se não houver nenhuma sessão ativa
                    } else {
                        $usersession->gravaSessao();
                        $this->_helper->json(array('redirect' => '/portal/login/selecionaperfil', 'flag' => false));
                        $usersession->gravaHash($_COOKIE['PHPSESSID']);
                    }
                } else {
                    if ($this->getRequest()->isXmlHttpRequest()) {
                        $this->_helper->json(array('redirect' => '/portal/login/selecionaperfil', 'flag' => false));
                    }else {
                        $this->redirect('/portal/login/selecionaperfil');
                    }
                }
            } else {//se a senha for incorreta
                if ($this->getRequest()->isXmlHttpRequest()) {
                    $this->_helper->json(array('message' => $mensageiro->getMensagem(), 'senha' => false));
                }else {
                    $this->redirect('/portal/index');
                }
//                $this->_helper->json(array('senha' => false));
            }
        } catch (Zend_Exception $e) {
            if ($this->getRequest()->isXmlHttpRequest()) {
                $this->_helper->json(array('message' => $e->getMessage(), 'senha' => false));
            }else {
                $this->_helper->flashMessenger->addMessage("
                    <div class='info-box red uny ocorrencia-box'>
                            <i class='icon-warning-sign'></i>
                        <div class='msg'>
                        Erro no servidor, por favor entre em contato com o administrador do sistema
                        </div>
                        <a href='javascript:void(0)' class='info-box-remove'><i class='icon-remove'></i></a>
                    </div>");
                $this->redirect('/portal/index');
            }
        }
    }

    /**
     * Action para cancelar o login, caso usuário não deseje kickar a outra sessão
     * @author helder silva <helder.silva@unyleya.com.br>
     */
    public function cancelarloginAction()
    {
        try {
            $usersession = new \G2\Negocio\UsuarioSessao();
            $resultado = $usersession->cancelaHash($_COOKIE['PHPSESSID']);
            session_destroy();
            $this->_helper->json("Login Cancelado");
        } catch (\Exception $e) {
            $this->_helper->flashMessenger->addMessage("
            <div class='info-box red uny ocorrencia-box'>
                    <i class='icon-warning-sign'></i>
                <div class='msg'>
                Erro no servidor, por favor entre em contato com o administrador do sistema
                </div>
                <a href='javascript:void(0)' class='info-box-remove'><i class='icon-remove'></i></a>
            </div>");
            $this->_redirect('/portal');
        }
    }

    /**
     * Action que seleciona o perfil de Acesso
     */
    public function selecionaperfilacessoAction()
    {

        $id_perfil = $this->_getParam('id_perfil');
        $id_perfilpedagogico = $this->_getParam('id_perfilpedagogico');
        $id_projetopedagogico = $this->_getParam('id_projetopedagogico');
        $id_matricula = $this->_getParam('id_matricula');
        $id_entidade = $this->_getParam('id_entidade');

        try {
            $vwUsuarioPerfilPedagogicoTO = new VwUsuarioPerfilPedagogicoTO();
            $vwUsuarioPerfilPedagogicoTO->setId_usuario(Ead1_Sessao::getSessaoUsuario()->id_usuario);
            $vwUsuarioPerfilPedagogicoTO->setId_perfil($id_perfil);
            $vwUsuarioPerfilPedagogicoTO->setId_perfilpedagogico($id_perfilpedagogico);
            $vwUsuarioPerfilPedagogicoTO->setId_projetopedagogico($id_projetopedagogico);
            $vwUsuarioPerfilPedagogicoTO->setId_matricula($id_matricula);
            $vwUsuarioPerfilPedagogicoTO->setId_entidade($id_entidade);
            $mensageiro = $this->_bo->retornarUsuarioPerfilAcesso($vwUsuarioPerfilPedagogicoTO);
            $mensageiro->subtractMensageiro();

            $return = $mensageiro->getFirstMensagem()->toArray();

            Ead1_Sessao::setSessionGeral($return);
            Ead1_Sessao::setSessionEntidade($return);
            Ead1_Sessao::setSessionPerfil($return);
            Ead1_Sessao::setSessionUsuario($return);

            if (Ead1_Sessao::getSessaoPerfil()->is_aluno() || Ead1_Sessao::getSessaoPerfil()->is_alunoInstitucional()) {
                Ead1_Sessao::setSessionProjetoPedagogico($return);

                // Se for aluno, atualiza a data do ultimo acesso (dt_ultimoacesso)
                $negocio = new \G2\Negocio\Negocio();
                $matricula = $negocio->find('\G2\Entity\Matricula', $id_matricula);

                if ($matricula && $matricula->getId_matricula()) {
                    $matricula->setDt_ultimoacesso(date('Y-m-d H:i:s'));
                    $negocio->save($matricula);
                }
            }

            $primeiroAcessoBO = new PrimeiroAcessoBO();
            //Verifica se é aluno e se já concluiu todo o primeiro acesso acessando a helper
            if ($this->view->verificaPrimeiroAcesso()) {
                $mensageiro = new Ead1_Mensageiro(null, Ead1_IMensageiro::SUCESSO, '/primeiro-acesso/');
            } else {
                //Operação necessária para compatibilidade com forma anterior de validação.
                $this->_getParam('url-direta') ? $mensageiro->setCodigo($this->_getParam('url-direta')) : $mensageiro->setCodigo("/index/principal");
            }

            $this->redirect($this->baseUrlModulo . $mensageiro->codigo);

        } catch (Exception $e) {
            die("<script> alert('" . $e->getMessage() . "'); location.replace('" . $this->baseUrlModulo . "/index'); </script>");
        }
    }

    /**
     * Action que apresenta a view de listas de perfis do Usuário
     */
    public function selecionaperfilAction()
    {
        try {
            Ead1_Sessao::setSessionProjetoPedagogico(array('id_matricula'=> false));
            $this->_helper->layout()->setLayout('layout');
            Zend_Session::namespaceUnset('mensagem');
            Zend_Session::namespaceUnset('entidade');

            //Grava-log
            $logTO = new LogAcessoTO();
            $logTO->setId_funcionalidade(LogAcessoTO::PRINCIPAL);
            $logRO = new LogRO();
            $logRO->gravaLog($logTO);

            $sessaoUsuario = Ead1_Sessao::getSessaoUsuario();
            Ead1_Sessao::setSessionUsuario($sessaoUsuario->toArray());
            $this->view->listaPerfil = array();
            $this->view->erro = null;

            $to = new UsuarioTO();
            $to->setId_usuario(Ead1_Sessao::getSessaoUsuario()->id_usuario);
            $mensageiro = $this->_bo->retornarPerfilUsuario($to);
            /**
             * Redirecionamento direto caso o usuário só tenha um perfil e uma sala
             */
            $entidadesPerfis = $mensageiro->getMensagem();

            $countPerfis = 0;
            $arrayPerfil = array();
            foreach ($entidadesPerfis as $entidade) {
                if (isset($entidade->perfis) && $entidade->perfis) {
                    $arrayPerfil[] = $entidade;
                    $countPerfis++;
                }
            }

            $this->view->listaPerfil = $arrayPerfil;
            $this->view->dir = realpath('.').DIRECTORY_SEPARATOR;

            // Se possuir somente um Perfil e uma Disciplina, logar nele diretamente
            if ($countPerfis == 1) {
                if (isset($arrayPerfil[0]->perfis) && count($arrayPerfil[0]->perfis) == 1) {
                    $ref = current($arrayPerfil[0]->perfis);
                    if (isset($ref['perfis']) && count($ref['perfis']) == 1) {

                        $perfilUnico = current($ref['perfis']);

                        $id_perfil = $perfilUnico->id_perfil;
                        $id_perfilpedagogico = $perfilUnico->id_perfilpedagogico;
                        $id_projetopedagogico = $perfilUnico->id_projetopedagogico;
                        $id_matricula = $perfilUnico->id_matricula;
                        $id_entidade = $perfilUnico->id_entidade;

                        $acesso_direto = true;
                        $this->forward('selecionaperfilacesso', 'login', 'portal', array(
                            'id_perfil' => $id_perfil,
                            'id_perfilpedagogico' => $id_perfilpedagogico,
                            'id_projetopedagogico' => $id_projetopedagogico,
                            'id_matricula' => $id_matricula,
                            'id_entidade' => $id_entidade,
                            'acesso_direto' => $acesso_direto
                        ));

                    }
                }
            } else if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $this->view->listaPerfil = $arrayPerfil;
            } else {
                $this->view->erro = 'Prezado(a) Usuário(a), você não tem nenhum perfil ativo conosco. Por favor, procure a sua instituição!';
            }

        } catch (Exception $e) {
            $this->view->erro = 'Erro ao buscar perfil!';
        }
    }

    /**
     * Action de Autenticação de Login externa
     */
    public function autenticacaoExternaAction()
    {
        try {

            $vwUsuarioPerfilPTO = new VwUsuarioPerfilPedagogicoTO();
            $vwUsuarioPerfilPTO->setSt_login($this->getRequest()->getParam('nomusuario'));
            $vwUsuarioPerfilPTO->setSt_senha($this->getRequest()->getParam('dessenha'));
            $vwUsuarioPerfilPTO->setSt_email($this->getRequest()->getParam('nomusuario'));
            $vwUsuarioPerfilPTO->setSt_senhaentidade($this->getRequest()->getParam('dessenha'));

            $mensageiro = $this->_bo->validateUserPass($vwUsuarioPerfilPTO);
            if ($mensageiro->getTipo() != \Ead1_IMensageiro::SUCESSO)
                throw new Zend_Exception($mensageiro->getFirstMensagem());


            $mensageiro->subtractMensageiro();
            $this->redirect($this->baseUrlModulo . '/' . ($mensageiro->getCodigo() ? $mensageiro->getCodigo() : "login/selecionaperfil"));
        } catch (Zend_Exception $e) {
            if ($this->hasParam('url_retorno')) {
                $urlRetorno = $this->_getParam('url_retorno') . "?mensagem_erro=" . $mensageiro->getFirstMensagem();
                $this->redirect($urlRetorno);
            } else {
                $this->_helper->flashMessenger->addMessage($mensageiro->getFirstMensagem());
                $this->redirect('/portal');
            }
        }
    }

    /**
     * Autenticação por Url
     * @param int p    = id_usuario
     * @param string u    = st_urlacesso
     * @param int e    = id_entidade
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     */
    public function autenticacaoDiretaAction()
    {
        try {

            $usTO = new VwUsuarioPerfilPedagogicoTO();
            $usTO->setId_usuario($this->getRequest()->getParam('p'));
            $usTO->setSt_urlacesso($this->getRequest()->getParam('u'));
            $usTO->setId_entidade($this->getRequest()->getParam('e'));
            $usTO->setId_entidadeportal($this->getRequest()->getParam('e'));

            $l = $this->getRequest()->getParam('l', 'sim');
            $id_usuariooriginal = $this->getRequest()->getParam('m', null);

            $mensageiro = $this->_bo->validaAcessoDireto($usTO, $l, $id_usuariooriginal);
            $mensageiro->subtractMensageiro();

            if ($id_usuariooriginal == null) {
                $primeiroAcessoBO = new PrimeiroAcessoBO();
                //Verifica se é aluno e se já concluiu todo o primeiro acesso
                if (Ead1_Sessao::getSessaoPerfil()->is_aluno() &&
                    !Ead1_Sessao::getSessaoPerfil()->is_alunoInstitucional() &&
                    !$primeiroAcessoBO->retornarProgresso()->getBl_mensageminformativa() &&
                    $primeiroAcessoBO->passarPeloPrimeiroAcesso()
                ) {
                    $mensageiro = new Ead1_Mensageiro(null, Ead1_IMensageiro::SUCESSO, 'primeiro-acesso/');
                }
            }
            $this->_redirect($this->baseUrlModulo . '/' . $mensageiro->getCodigo());
        } catch (Zend_Exception $e) {
            if ($this->_hasParam('url_retorno')) {
                $urlRetorno = $this->_getParam('url_retorno') . "?mensagem_erro=" . $mensageiro->getFirstMensagem();
                $this->_redirect($urlRetorno);
            } else {
                $this->_helper->flashMessenger->addMessage($mensageiro->getFirstMensagem());
                $this->_redirect('/portal');
            }
        }
    }

}

<?php
/**
 * Created by PhpStorm.
 * User: Alex Alexandre
 * E-mail: <alex.alexandre@unyleya.com.br>
 * Date: 18/12/17
 * Time: 09:36
 */

/**
 *                                      ************  ATENÇÃO ***************
 *
 * FOI NECESSÁRIO COLOCAR TODOS OS METODOS DA CAMADA DE NEGOCIO, QUE ANTES ESTAVAM EM library/G2/Negocio/Maximize.php
 * E DA CAMADA application/webservices/clients/Maximize/MaximizeWebServices.php PARA DENTRO DESTE CONTROLLER
 *
 * ESTA SOLUÇÃO APESAR DE NÃO RECOMENDADA FOI A ÚNICA ENCONTRADA PARA RESOLVER UM ERRO EM PRODUÇÃO QUE ATÉ O MOMENTO CONTINUA SEM EXPLICAÇÃO
 * FAVOR, NÃO MEXER NESTA CLASSE SEM COMUNICAÇÃO IMEDIATA AO RONALDO OU ALEX
 *
 * <ronaldo.melo@unyleya.com.br>
 * <alex.alexandre@unyleya.com.br>
 */
class Portal_VistaDeProvaController extends PortalBase
{
    /**
     * @var \G2\Negocio\Negocio
     */
    private $negocio;
    protected $tokenAutenticacao;

    public function init()
    {
        $this->negocio = new G2\Negocio\Negocio();
        $this->_helper->layout()->setLayout("layout");

        parent::init();

    }

    public function indexAction()
    {
        $this->_helper->layout()->setLayout('layout');
        $res = $this->verificaAlunoIntegrado($this->negocio->sessao->id_usuario);
        if (!is_array($res)) {
            $this->redirect($res);
        }
        $this->view->mensagemRetorno = array(
            "message" => $res["message"],
            "cssClass" => $res["cssClass"],
            'type' => $res["type"]
        );
    }

    /**
     * Função responsável por verificar se um aluno já esta integrado
     *
     * @param $idAluno
     * @return array|\Ead1_Mensageiro
     * @throws \Exception
     * @throws \Zend_Exception
     */
    public function verificaAlunoIntegrado($idAluno)
    {
        $aluno = $this->negocio->findOneBy("G2\Entity\UsuarioIntegracao", array("id_usuario" => $idAluno));

        if (!$aluno) {
            return array(
                "message" => "Você não tem provas para revisão",
                "type" => "error",
                "cssClass" => "red");
        }

        return $this->verificarProva($aluno->getId_usuario());
    }

    /**
     * Função responsável por verificar se existe uma prova para o aluno
     *
     * @param $idAluno
     * @return array|\Ead1_Mensageiro
     * @throws \Exception
     * @throws \Zend_Exception
     */
    public function verificarProva($idAluno)
    {
        $prova = $this->negocio->findOneBy("G2\Entity\VwAvaliacaoAgendamento", array("id_usuario" => $idAluno));

        if (!$prova) {
            return array(
                "message" => "Você não tem provas para revisão",
                "type" => "error",
                "cssClass" => "red");
        }

        return $this->montarUrlAutenticacao($idAluno);
    }

    /**
     * Função responsável por disparar uma requisição para gerar o token de acesso
     * E montar a url final, que irá redirecionar o aluno para a fabrica de provas
     *
     * @param $idAluno (O $idAluno é o campo codigoSistemaOrigem)
     * @return \Ead1_Mensageiro
     * @throws \Exception
     */
    public function montarUrlAutenticacao($idAluno)
    {
        $sistema = $this->negocio->find('\G2\Entity\Sistema', \G2\Constante\Sistema::MAXIMIZE);

        $url = $sistema->getst_conexao();
        $this->setTokenAutenticacao($sistema->getst_chaveacesso());

        $token = $this->exec(
            \G2\Constante\Maximize::ENDPOINT_GERAR_CHAVE_ACESSO,
            $idAluno,
            $url,
            'string',
            true
        );

        if (!$token) {
            return array(
                "message" => "Não foi possível gerar um token, por favor contate o admim do sistema",
                "type" => "error",
                "cssClass" => "red");
        }

        $fullUrl = $url . \G2\Constante\Maximize::ENDPOINT_AUTENTICAR_USUARIO . $token;

        return $fullUrl;
    }

    /**
     * @param $method (Eg.: '/c/prova/listar')
     * @param $data (Eg.: array('buscaTextual' => 'teste') )
     * @param $urlP ( url do sistema maximize
     * @param string $format (Eg.: 'json')
     * @param $paramUrl (Para diferenciar se o paramentro vai no body ou na url)
     * @return Ead1_Mensageiro
     */
    public function exec($method, $data, $urlP, $format = 'json', $paramUrl = false)
    {
        try {
            $url = $urlP . $method;

            if ($paramUrl) {
                $url = $urlP . $method . $data;
            }

            $ch = curl_init($url);
            curl_setopt_array($ch, array(
                CURLOPT_POST => true,
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_POSTFIELDS => json_encode($data),
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER => $this->getHttpHeader()
            ));
            $response = curl_exec($ch);
            if ($response === false) {
                throw new Exception(curl_error($ch));
            }

            curl_close($ch);

            if ($format === 'json') {
                $return = json_decode($response);

                if (!empty($return->msgErro)) {
                    throw new Exception($return->msgErro);
                }
                return new Ead1_Mensageiro($return);

            } else if ($format === 'string') {
                return $response;
            }

            throw new Exception('Formato de retorno não implementado');

        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }

    }

    /**
     * @return array
     */
    private function getHttpHeader()
    {
        $httpHeader = ["Content-Type: application/json"];

        if ($this->getTokenAutenticacao()) {
            array_push($httpHeader, "Token-Autenticacao: {$this->getTokenAutenticacao()}");
        }
        return $httpHeader;

    }

    /**
     * @return mixed
     */
    public function getTokenAutenticacao()
    {
        return $this->tokenAutenticacao;
    }

    /**
     * @param $token
     * @return $this
     */
    public function setTokenAutenticacao($token)
    {
        $this->tokenAutenticacao = $token;
        return $this;
    }

}

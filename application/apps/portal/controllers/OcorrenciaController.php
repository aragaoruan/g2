<?php
/**
 * Created by PhpStorm.
 * User: aragaoruan
 * Date: 15/05/18
 * Time: 18:03
 */

class Portal_OcorrenciaController extends PortalBase
{
    /**
     * @var \G2\Negocio\Ocorrencia
     */
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Ocorrencia();
    }

    /**
     * Função que retorna um Json de ocorrencia com a evolução Aguardando Interesado
     * @throws Zend_Controller_Action_Exception
     */
    public function ocorrenciaAguardandoInteressadoAction()
    {
        $result = $this->negocio->ocorrenciaAguardandoInteresado($this->_getParam('id_matricula'));
        $this->_helper->json($result);
    }

    /**Funcao para adicionar o tramite que visualizou ou tomor ciencia
     * @throws Zend_Controller_Action_Exception
     */
    public function aguardandoRespostaTramiteAction()
    {
        $ocorrencias = $this->_getParam('ocorrencias');
        $nomeBotao = $this->_getParam('nomeBotao');
        $idUsuarioOriginal = $this->_getParam('idUsuarioOriginal');
        $result = $this->negocio->aguardandoRespostaTramite($ocorrencias, $nomeBotao, $idUsuarioOriginal);
        $this->_helper->json($result);
    }
}

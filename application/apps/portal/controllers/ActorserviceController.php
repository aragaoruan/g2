<?php

class Portal_ActorserviceController extends Ead1_Controller
{


    public function indexAction()
    {

    	
   	
    }
    
    /*
     * @param filtro array
     * @param filtro['codperfil'] Perfil no Actor
     * @param filtro['codtipopess'] Tipo de Pessoa no Actor
     * @param filtro['codpapel'] O código do papel
     * 
     */   
    
    public  function cursoAction(){
    	
         $client = new Zend_Rest_Client('http://actor.ead1.com.br/services/curso.php');
		
    	 try {
		 $client->listarcurso()
				  ->parametros(array('codarea'=>2))
				  ->webservicecodentidade('c9f0f895fb98ab9159f51fd0297e236d')
				  ->webservicechave('4df8f46ac296d2e633386a010d279d14');
			$dados =  Ead1_Util::object_to_array($client->get()->listarcurso);
			echo '<fieldset><legend>Retornando um Curso do Actor 2</legend>';
			Zend_Debug::dump($dados);
			echo '</fieldset>';
		} catch (exception $exception) {
    		Zend_Debug::dump($exception);
		}
				
		exit;
    }
    
    
    
    /*
     * @param filtro array
    * @param filtro['codperfil'] Perfil no Actor
    * @param filtro['codtipopess'] Tipo de Pessoa no Actor
    * @param filtro['codpapel'] O código do papel
    *
    */
    
    public  function papelAction(){
    
    	$client = new Zend_Rest_Client('http://actor.admescolar.com.br/services/papel.php');
    
    	try {
    		$client->listarpapel()
    		->filtro(array('codperfil'=>8,'codtipopess'=>3,'codpapel'=>114279))
    		->webservicecodentidade('c9f0f895fb98ab9159f51fd0297e236d')
    		->webservicechave('4df8f46ac296d2e633386a010d279d14');
    		$dados =  Ead1_Util::object_to_array($client->get()->listarpapel);
    		echo '<fieldset><legend>Retornando o Papel do Actor 2</legend>';
    		Zend_Debug::dump($dados);
    		echo '</fieldset>';
    	} catch (exception $exception) {
    		Zend_Debug::dump($exception);
    	}
    
    	exit;
    }
    
    
    /*
     * @param dados array
     * @param dados['codperfil'] Perfil no Actor
     * @param dados['codtipopess'] Tipo de Pessoa no Actor
     * @param dados['codpessoa'] A Pessoa que vai receber o Papel
     * @param dados['codcurso']
     * @param dados['codturma']
     * @param dados['datinicio'] A Data de início do papel formato DD/MM/YYYY
     * @param dados['dattermino'] A Data de término do papel formato DD/MM/YYYY
     * @param dados['flamaster'] Define se das pessoas que tem aquele papel, qual é a principal. S ou N, default N
     * 
     */   
    public function inserirpapelAction(){
    	
    	
    	$client = new Zend_Rest_Client('http://actor.admescolar.com.br/services/papel.php');
    	
    	try {
		$client	->inserirpapel()
				->webservicecodentidade('c9f0f895fb98ab9159f51fd0297e236d')
				->webservicechave('4df8f46ac296d2e633386a010d279d14')
				->dados(
						array(	  
							  'codtipopess'=>1
							, 'codperfil'=>8
							, 'codpessoa'=>4781
							, 'codcurso'=>832
							, 'codturma'=>3991
							, 'datinicio'=>'10/10/2012'
							, 'dattermino'=>'10/10/2020'
							, 'flamaster'=>'S'								
							)
					);

		$dados =  Ead1_Util::object_to_array($client->post()->inserirpapel);
		    echo '<fieldset><legend>Inserindo o Papel</legend>';
			Zend_Debug::dump($dados);
			echo '</fieldset>';
		} catch (exception $exception) {
    		Zend_Debug::dump($exception);
    		exit;
		}
    	
    	
    	
    	exit;
    }
    
    
    
    /*
     * @param dados array
     * @param dados['codpapel'] 
     * @param dados['datinicio'] A Data de início do papel formato DD/MM/YYYY
     * @param dados['dattermino'] A Data de término do papel formato DD/MM/YYYY
     * @param dados['flamaster'] Define se das pessoas que tem aquele papel, qual é a principal. S ou N, default N
     * @param dados['flastatus'] Define se o Papel está ativo ou Inativo. A ou I, default A
     * 
     */   
    public function atualizarpapelAction(){
    	
    	
    	$client = new Zend_Rest_Client('http://actor.admescolar.com.br/services/papel.php');
    	
    	try {
		$client	->atualizarpapel()
				->webservicecodentidade('c9f0f895fb98ab9159f51fd0297e236d')
				->webservicechave('4df8f46ac296d2e633386a010d279d14')
				->dados(
						array(	  
							  'codpapel'=>766592
							, 'flastatus'=>'A'
							, 'datinicio'=>'10/10/2010'
							, 'dattermino'=>'10/10/2022'
							, 'flamaster'=>'N'								
							)
					);

		$dados =  Ead1_Util::object_to_array($client->post()->atualizarpapel);
		    echo '<fieldset><legend>Inserindo o Papel</legend>';
			Zend_Debug::dump($dados);
			echo '</fieldset>';
		} catch (exception $exception) {
    		Zend_Debug::dump($exception);
    		exit;
		}
    	
    	
    	
    	exit;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /*
     * @param filtro array
     * @param filtro['codperfil'] Perfil no Actor
     * @param filtro['codtipopess'] Tipo de Pessoa no Actor
     * @param filtro['so_vinculados'] sim : nao Se informado sim, só virão listados os Navegadores Vinculados. O padrão é nao.
     * 
     */   
    
    public  function navegacaoAction(){
    	
         $client = new Zend_Rest_Client('http://actor.admescolar.com.br/services/navegacao.php');
		
    	 try {
		 $client->listarnavegacao()
				  ->filtro(array('codperfil'=>8,'codtipopess'=>3,'so_vinculados'=>'sim'))
				  ->webservicecodentidade('c9f0f895fb98ab9159f51fd0297e236d')
				  ->webservicechave('4df8f46ac296d2e633386a010d279d14');
		$dados =  Ead1_Util::object_to_array($client->get()->listarnavegacao);
			echo '<fieldset><legend>Retornando a Navegação básica do Actor 2</legend>';
			Zend_Debug::dump($dados);
			echo '</fieldset>';
		} catch (exception $exception) {
    		Zend_Debug::dump($exception);
		}
				
		exit;
    }
    
    
    
    /*
     * @param dados array
     * @param dados['codperfil'] Perfil no Actor
     * @param dados['codtipopess'] Tipo de Pessoa no Actor
     * @param dados['codnavegador'] Os códigos dos navegadores a serem atribuidos ao perfil
     * , essa rotina exclui previamente os vinculos antigos, então é necessário passar todos os códigos sempre.
     * 
     */   
    public function inserirnavegacaoAction(){
    	
    	
    	$client = new Zend_Rest_Client('http://actor.admescolar.com.br/services/navegacao.php');
    	
    	try {
		$client	->inserirnavegacao()
				->webservicecodentidade('c9f0f895fb98ab9159f51fd0297e236d')
				->webservicechave('4df8f46ac296d2e633386a010d279d14')
				->dados(
						array(	  
							  'codtipopess'=>3
							, 'codperfil'=>8
							, 'codnavegador'=>array(84,85,73,72,15,88,48,3,14,42,79,1,39)								
							)
					);

		$dados =  Ead1_Util::object_to_array($client->post()->inserirnavegacao);
		    echo '<fieldset><legend>Inserindo a Navegação básica</legend>';
			Zend_Debug::dump($dados);
			echo '</fieldset>';
		} catch (exception $exception) {
    		Zend_Debug::dump($exception);
    		exit;
		}
    	
    	
    	
    	exit;
    }
    
    
    public function pessoaAction(){
    	exit;
    	/*
    	 * PESSOA
    	 **/
    	 $client = new Zend_Rest_Client('http://actor.admescolar.com.br/services/pessoa.php');
		
    	 try {
		 $client->retornapessoa()
				  ->codpessoa(4781)
				  ->webservicecodentidade('c9f0f895fb98ab9159f51fd0297e236d')
				  ->webservicechave('4df8f46ac296d2e633386a010d279d14');
		$dados =  Ead1_Util::object_to_array($client->get()->retornapessoa);
			echo '<fieldset><legend>Retornando uma Pessoa no Actor 2</legend>';
			Zend_Debug::dump($dados);
			echo '</fieldset>';
		} catch (exception $exception) {
    		Zend_Debug::dump($exception);
		}
    	
		
		
		try {
		$client	->inserirpessoa()
				->webservicecodentidade('c9f0f895fb98ab9159f51fd0297e236d')
				->webservicechave('4df8f46ac296d2e633386a010d279d14')
				->dados(array('nompessoa'=>'Criado pelo WebService', 'numcpf'=>'8888888888'));
		//Zend_Debug::dump($client->post());
		$dados =  Ead1_Util::object_to_array($client->post()->inserirpessoa);
		    echo '<fieldset><legend>Inserindo uma Pessoa no Actor 2</legend>';
			Zend_Debug::dump($dados);
			echo '</fieldset>';
		} catch (exception $exception) {
    		Zend_Debug::dump($exception);
    		exit;
		}
		
		
		$codpessoa = $dados['pessoa']['codpessoa'];
		$nompessoa = $dados['pessoa']['nompessoa'];
		
		try {
		$client	->atualizarpessoa()
				->webservicecodentidade('c9f0f895fb98ab9159f51fd0297e236d')
				->webservicechave('4df8f46ac296d2e633386a010d279d14')
				->dados(array('codpessoa'=>$codpessoa, 'nompessoa'=>$nompessoa.' e Atualizado pelo WebService', 'numcpf'=>'89285243676'));
		//Zend_Debug::dump($client->post());
		$dados =  Ead1_Util::object_to_array($client->post()->atualizarpessoa);
		    echo '<fieldset><legend>Atualizando uma Pessoa no Actor 2</legend>';
			Zend_Debug::dump($dados);
			echo '</fieldset>';
		} catch (exception $exception) {
    		Zend_Debug::dump($exception);
		}
    	
    	
    }
    
    public function disciplinaAction(){
    	//exit;

        $client = new Zend_Rest_Client('http://actor.ead1.com.br/services/disciplina.php');
		
    	/*
    	 * Criando UMA DISCIPLINA no Actor 2
    	 * */
    	try {
			
			$client	->inserirdisciplina()
					->webservicecodentidade('c9f0f895fb98ab9159f51fd0297e236d')
					->webservicechave('4df8f46ac296d2e633386a010d279d14')
					->dados(
						array(
							 'nommodulo'	=>'Criado pelo WebService'
							,'nommodulosite'=>'Criado pelo WebService'
							,'codarea'		=>'2'
							,'desmodulo'	=>'DescriÃ§Ã£o do mÃ³dulo Criado pelo WebService'
							,'desurl'		=>'http://www.globo.com'
							,'numhoras'		=>'450'
						)
					);
			$dados =  Ead1_Util::object_to_array($client->post()->inserirdisciplina);
			echo '<fieldset><legend>Criando UMA DISCIPLINA no Actor 2</legend>';
    		Zend_Debug::dump($dados);
    		echo '</fieldset>';
		} catch (exception $exception) {
    		Zend_Debug::dump($exception);
		}
    	
		$codmodulo = $dados['disciplina']['codmodulo'];
    	
    	/*
    	 * Alterando UMA DISCIPLINA no Actor 2
    	 * */
    		try {
			
			$client	->atualizardisciplina()
					->webservicecodentidade('c9f0f895fb98ab9159f51fd0297e236d')
					->webservicechave('4df8f46ac296d2e633386a010d279d14')
					->dados(
						array(
							 'codmodulo'	=>$codmodulo
							,'nommodulo'	=>' Alterado pelo WebService '
							,'nommodulosite'=>' Alterado pelo WebService '
							,'codarea'		=>'2'
							,'desmodulo'	=>'DescriÃ§Ã£o do mÃ³dulo Criado pelo WebService'
							,'desurl'		=>'http://www.globo.com'
							,'numhoras'		=>'450'
						)
					);
			$dados =  Ead1_Util::object_to_array($client->post()->atualizardisciplina);
			echo '<fieldset><legend>Alterando UMA DISCIPLINA no Actor 2</legend>';
    		Zend_Debug::dump($dados);
    		echo '</fieldset>';
	
		} catch (exception $exception) {
			Zend_Debug::dump($exception);
		}
		
		
            /*
    	 * Recuperando UMA DISCIPLINA no Actor 2
    	 * */
    	 try {
			 $client->retornadisciplina()
					  ->codmodulo($codmodulo)
					  ->webservicecodentidade('c9f0f895fb98ab9159f51fd0297e236d')
					  ->webservicechave('4df8f46ac296d2e633386a010d279d14');
			$dados =  Ead1_Util::object_to_array($client->get()->retornadisciplina);
			echo '<fieldset><legend>Recuperando UMA DISCIPLINA no Actor 2</legend>';
    		Zend_Debug::dump($dados);
    		echo '</fieldset>';
    	} catch (exception $exception) {
			Zend_Debug::dump($exception);
		}
		
         /*
    	 * Listando os mÃ³dulos no Actor 2
    	 * */
    	 try {
			 $client->listardisciplina()
					  ->webservicecodentidade('c9f0f895fb98ab9159f51fd0297e236d')
					  ->webservicechave('4df8f46ac296d2e633386a010d279d14');
			$dados =  Ead1_Util::object_to_array($client->get()->listardisciplina);
			echo '<fieldset><legend>Listando AS DISCIPLINAS no Actor 2</legend>';
    		Zend_Debug::dump($dados);
    		echo '</fieldset>';
    	} catch (exception $exception) {
			Zend_Debug::dump($exception);
		}
    	
    }

    public function saladeaulaAction(){
    	exit;
    	$client = new Zend_Rest_Client('http://actor.admescolar.com.br/services/saladeaula.php');
    	
    	/*
    	 * INSERINDO UMA DISCIPLINA
    	*/ 
		try {
			
			$client	->inserirsaladeaula()
					->webservicecodentidade('c9f0f895fb98ab9159f51fd0297e236d')
					->webservicechave('4df8f46ac296d2e633386a010d279d14')
					->dados(
						array(
							 'codmodulosaladeaula' 	=>'3921' // ObrigatÃ³rio, ele indica qual mÃ³dulo serÃ¡ usado para a saladeaula.
							,'destitulo'			=>'Criado pelo WebService'
							,'datiniciocurso' 		=>'15/12/2010'
							,'datterminocurso'		=>'15/12/2015'
							,'datinicioturma' 		=>'15/12/2011' // OPCIONAL
							,'datterminoturma'		=>'15/12/2014' // OPCIONAL
							,'nomturma'				=>'Nome da Turma' // OPCIONAL
							//,'codcurso'				=>'7042' // INFORMANDO O CODCURSO NESSA PARTE, O SISTEMA VAI INSERIR APENAS A TURMA, NÃƒO INFORMANDO, ELE INSERE TUDO
							,'codarea'				=>'2'
							,'descurso'				=>'DescriÃ§Ã£o do curso Criado pelo WebService'
							,'desurl'				=>'http://www.globo.com'
							,'numhoras'				=>'450'
							,'codnivel'				=>'2'
							,'desemail'				=>'elciomgdf@gmail.com'
						)
					);
			$dados =  Ead1_Util::object_to_array($client->post()->inserirsaladeaula);
			echo '<fieldset><legend>INSERINDO UMA SALA DE AULA NO ACTOR 2</legend>';
    		Zend_Debug::dump($dados);
    		echo '</fieldset>';
		} catch (exception $exception) {
			Zend_Debug::dump($exception);
		}
    	 
		
		// ATUALIZANDO UMA DISCIPLINA NO ACTOR 2
		// vamos usar o codcurso inserido como exemplo
		$codcurso = $dados["saladeaula"]["curso"]["codcurso"];
		$codturma = $dados["saladeaula"]["turma"]["codturma"];
		
		try {
			
			$client	->atualizarsaladeaula()
					->webservicecodentidade('c9f0f895fb98ab9159f51fd0297e236d')
					->webservicechave('4df8f46ac296d2e633386a010d279d14')
					->dados(
						array(
						
							 'codcurso'				=>$codcurso
							,'destitulo'			=>'Atualizado pelo WebService usando o programa '
							,'descurso'				=>'DescriÃ§Ã£o do mÃ³dulo Criado pelo WebService'
							,'datiniciocurso' 		=>'15/12/2010'
							,'datterminocurso'		=>'15/12/2015'
							,'codarea'				=>'2'
							,'codnivel'				=>'2'
							,'desurl'				=>'http://www.globo.com'
							,'numhoras'				=>'450'
							,'flasituacao'			=>'A'
							,'desemail'				=>'elciomgdf@gmail.com'
							
							,'codturma'				=>$codturma // INFORMANDO O CODTURMA VC ALTERA A DISCIPLINA
							,'nomturma'				=>'Novo nome da turma'
							,'datinicioturma'		=>'10/10/2010'
							,'datterminoturma'		=>'10/10/2020'
							
						)
					);
			$dados =  Ead1_Util::object_to_array($client->post()->atualizarsaladeaula);
			echo '<fieldset><legend>ATUALIZANDO UMA SALA DE AULA NO ACTOR 2</legend>';
    		Zend_Debug::dump($dados);
    		echo '</fieldset>';	
		} catch (exception $exception) {
			Zend_Debug::dump($exception);
		}
		
		// LISTANDO AS DISCIPLINAS NO ACTOR 2
		
        try {
			 $client->listarsaladeaula()
					  ->codcurso($codcurso)
			 		  ->webservicecodentidade('c9f0f895fb98ab9159f51fd0297e236d')
					  ->webservicechave('4df8f46ac296d2e633386a010d279d14');
			$dados =  Ead1_Util::object_to_array($client->get()->listarsaladeaula);
			echo '<fieldset><legend>LISTANDO AS SALAS DE AULA NO ACTOR 2</legend>';
    		Zend_Debug::dump($dados);
    		echo '</fieldset>';
    	} catch (exception $exception) {
    		Zend_Debug::dump($exception);
		}
		
       	// RECUPERANDO UMA DISCIPLINA NO ACTOR 2
		try {
			 $client->retornasaladeaula()
					  ->codcurso($codcurso)->codturma($codturma)
			 		  ->webservicecodentidade('c9f0f895fb98ab9159f51fd0297e236d')
					  ->webservicechave('4df8f46ac296d2e633386a010d279d14');
			$dados =  Ead1_Util::object_to_array($client->get()->retornasaladeaula);
			echo '<fieldset><legend>RECUPERANDO UMA SALA DE AULA NO ACTOR 2</legend>';
    		Zend_Debug::dump($dados);
    		echo '</fieldset>';
    	} catch (exception $exception) {
    		Zend_Debug::dump($exception);
		}
    	
    }

    public function matriculaAction(){
    	exit;

        $client = new Zend_Rest_Client('http://actor.admescolar.com.br/services/matricula.php');
		
    	/*
    	 * Criando UMA DISCIPLINA no Actor 2
    	 * */
    	try {
			//exec sp_matricular_pessoa_sem_transacao 3976,1700,345,4781,2, null, null
			$client	->inserirmatricula()
					->webservicecodentidade('c9f0f895fb98ab9159f51fd0297e236d')
					->webservicechave('4df8f46ac296d2e633386a010d279d14')
					->dados(
						array(
							 'codpessoa'			=>'4781' // Este Ã© o codpessoa do Elcio, altere.
							,'codcurso'				=>'9044'
							,'codcursoreferencia'	=>'9041'
						)
					);
			$dados =  Ead1_Util::object_to_array($client->post()->inserirmatricula);
			echo '<fieldset><legend>Criando UMA MATRÃ�CULA no Actor 2</legend>';
    		Zend_Debug::dump($dados);
    		echo '</fieldset>';
		} catch (exception $exception) {
    		Zend_Debug::dump($exception);
    		exit;
		}
    	
		if($dados['retorno']!='erro'):

		
		
		$codmatricula 	= $dados['matricula']['codmatricula'];
		$codpessoa 		= $dados['matricula']['codpessoa'];
    	
         /*
    	 * Recuperando UMA DISCIPLINA no Actor 2
    	 * */
    	 try {
			 $client->retornamatricula()
					  ->codmatricula($codmatricula)
					  ->webservicecodentidade('c9f0f895fb98ab9159f51fd0297e236d')
					  ->webservicechave('4df8f46ac296d2e633386a010d279d14');
			$dados =  Ead1_Util::object_to_array($client->get()->retornamatricula);
			echo '<fieldset><legend>Recuperando UMA MATRÃ�CULA no Actor 2</legend>';
    		Zend_Debug::dump($dados);
    		echo '</fieldset>';
    	} catch (exception $exception) {
			Zend_Debug::dump($exception);
		}
		
         /*
    	 * Listando os mÃ³dulos no Actor 2
    	 * */
    	 try {
			 $client->listarmatricula()
					  ->codpessoa($codpessoa)
					  ->webservicecodentidade('c9f0f895fb98ab9159f51fd0297e236d')
					  ->webservicechave('4df8f46ac296d2e633386a010d279d14');
			$dados =  Ead1_Util::object_to_array($client->get()->listarmatricula);
			echo '<fieldset><legend>Listando AS MATRÃ�CULAS no Actor 2</legend>';
    		Zend_Debug::dump($dados);
    		echo '</fieldset>';
    	} catch (exception $exception) {
			Zend_Debug::dump($exception);
		}
		
		endif; //if($dados['retorno']!='erro'):
    	
    }

    public function loginlmsAction(){
    	
        $client = new Zend_Rest_Client('http://actor.localhost.com.br/services/login.php');
		
    	try {
		$client->loginaluno()
					->dados(array(
							 'codmatricula'			=>'98922' 
							,'nomloginsugestao'		=>'teste29/04_a914'
							,'dessenhasugestao'		=>'37f1e6'
						))
				  ->webservicecodentidade('c9f0f895fb98ab9159f51fd0297e236d')
				  ->webservicechave('4df8f46ac296d2e633386a010d279d14');
		$dados =  Ead1_Util::object_to_array($client->post()->loginaluno);
			echo '<fieldset><legend>Logando uma Pessoa no Actor 2</legend>';
			Zend_Debug::dump($dados);
			echo '</fieldset>';
		} catch (exception $exception) {
    		Zend_Debug::dump($exception);
		}
    	
    }
}


<?php

class Portal_CronogramaController extends PortalBase
{
    /** @var PortalMatriculaBO $_bo */
    private $_bo;

    public function init()
    {
        parent::init();
        $this->_bo = new PortalMatriculaBO();
    }

    public function cronogramaAction()
    {
        try {
            $this->_helper->layout()->setLayout('layout');

            $this->view->status = Ead1_IMensageiro::SUCESSO;

            //Grava log de acesso
            $logTO = new LogAcessoTO();
            $logTO->setId_funcionalidade(LogAcessoTO::CRONOGRAMA);
            $logRO = new LogRO();
            $logRO->gravaLog($logTO);

            $this->view->dados = array();
            $this->view->erro = null;

            $vwMatriculaGradeSalaTO = new VwMatriculaGradeSalaTO();
            $vwMatriculaGradeSalaTO->setId_matricula(Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula);
            $mensageiro = $this->_bo->retornarGrade($vwMatriculaGradeSalaTO);

            if ($mensageiro->getTipo() === Ead1_IMensageiro::ERRO || $mensageiro->getTipo() === Ead1_IMensageiro::AVISO) {
                $this->view->status = $mensageiro->getTipo();
                return false;
            }

            $mensageiro->subtractMensageiro();
            $dados = $mensageiro->getMensagem();
            $this->view->curso = $dados['curso'];
            $this->view->coordenador = $dados['coordenador'];
            unset($dados['curso']);
            unset($dados['coordenador']);
            $this->view->dados = $dados;
        } catch (Zend_Exception $e) {
            $this->view->erro = $mensageiro;
        }
    }
}
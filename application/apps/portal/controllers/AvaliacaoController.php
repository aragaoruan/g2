<?php

/**
 * @package portal
 * @subpackage controllers
 *
 * @author Arthur Cláudio de Almeida Pereira < arthur.almeida@ead1.com.br >
 * @update Débora Castro <debora.castro@unyleya.com.br
 * @update Denise 21/10/214 - AC-26494
 * @update Reinaldo 30/06/2017 - GII-7817
 */
class Portal_AvaliacaoController extends PortalBase
{

    /**
     * @var \G2\Negocio\GerenciaProva
     */
    public $negocio;

    /**
     * @var PortalAvaliacaoBO
     */
    public $boPortalAvaliacao;

    public function init()
    {
        parent::init();

        if ($this->_helper->FlashMessenger->hasMessages()) {
            $this->view->messages = $this->_helper->FlashMessenger->getMessages();
        }
        $this->negocio = new \G2\Negocio\GerenciaProva();
        $this->boPortalAvaliacao = new PortalAvaliacaoBO();
    }

    public function agendamentoAction()
    {
        $this->_helper->layout->setLayout('layout');

        $id_matricula = Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula;

        // Verificar se o aluno está apto
        //if (!$this->negocio->verificarAlunoAptoAgendamento($id_matricula)) {
        if (!$this->negocio->alunoEstaAptoAgendamento($id_matricula)) {
            return $this->_helper->viewRenderer->setRender('agendamento-indisponivel');
        }

        //Grava log de acesso
        $logTO = new LogAcessoTO();
        $logTO->setId_funcionalidade(LogAcessoTO::AGENDAMENTO_DE_AVALIACAO);
        $logRO = new LogRO();
        $logRO->gravaLog($logTO);

        $bl_provapordisciplina = (new \G2\Negocio\EsquemaConfiguracao())->isProvaPorDisciplina();

        $ng_pessoa = new \G2\Negocio\Pessoa();
        $this->view->endereco = $ng_pessoa->getDadosCadastroRapidoUsuario(Ead1_Sessao::getSessaoUsuario()->id_usuario);

        $this->view->exibirMinhasProvas = null;
        $configuracaoEntidade = new \G2\Negocio\ConfiguracaoEntidade();
        $resultConfigEntidade = $configuracaoEntidade->findConfiguracaoEntidade($configuracaoEntidade->getId_entidade(), true);
        if ($resultConfigEntidade['bl_minhasprovas']) {
            $this->view->exibirMinhasProvas = $resultConfigEntidade['bl_minhasprovas'];
        }

        $this->view->dados = array(
            'id_entidade' => Ead1_Sessao::getSessaoEntidade()->id_entidade,
            'id_usuario' => Ead1_Sessao::getSessaoUsuario()->id_usuario,
            'id_matricula' => $id_matricula,
            'bl_provapordisciplina' => $bl_provapordisciplina
        );
    }

    public function salvarAgendamentoAction()
    {
        // Alterado por Reinaldo para receber os parâmetros através do Zend_Json
        $body = $this->getRequest()->getRawBody();
        $params = Zend_Json_Decoder::decode($body);

        if (!empty($params['id_matricula']) && empty($params['id_entidade'])) {
            $matricula = $this->negocio->find('\G2\Entity\Matricula', $params['id_matricula']);
            if ($matricula instanceof \G2\Entity\Matricula) {
                $params['id_entidade'] = $matricula->getId_entidadematricula();
            }
        }
        // Zerando a variável retorno para não precisar validar null
        $retorno = null;
        // $params sendo validado se existe e validando ser um array
        if (!empty($params['id_avaliacaoagendamento'])) {
            $retorno = $this->negocio->salvarReagendamentoPortal($params);
        } elseif (!empty($params) && is_array($params)) {
            $retorno = $this->negocio->salvarAgendamentoPortal($params);
        }

        if ($retorno instanceof Ead1_Mensageiro
            && $retorno->getTipo() == Ead1_IMensageiro::SUCESSO
        ) {
            $to = new MatriculaTO();
            $to->setId_matricula(Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula);
            $to->setId_usuario(Ead1_Sessao::getSessaoUsuario()->id_usuario);

            $mensagemBO = new MensagemBO();
            $mensageiro = $mensagemBO->procedimentoGerarEnvioMensagemPadraoEntidade(MensagemPadraoTO::AVISO_AGENDAMENTO, $to, TipoEnvioTO::EMAIL, false);

            if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                $this->_helper->json($mensageiro);
            }
        } else {
            $this->getResponse()->setHttpResponseCode(400);
        }

        $this->_helper->json($retorno);
    }

    public function historicoAgendamentoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $params['id_matricula'] = Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula;

        $result = $this->negocio->findByVwHistoricoAgendamento($params);
        $arrReturn = array();
        if (is_array($result)) {

            foreach ($result as $row) {
                $arrReturn[] = array(
                    'st_tipodeavaliacao' => $row->getSt_tipodeavaliacao(),
                    'st_tramite' => $row->getSt_tramite(),
                    'st_situacao' => $row->getSt_situacao(),
                    'st_nomecompleto' => $row->getSt_nomecompleto(),
                    'dt_cadastro' => $row->getDt_cadastro()->format('d/m/Y')
                );
            }
        }
        $this->_helper->json(array_reverse($arrReturn));
    }

    /**
     * @deprecated
     * Use /default/gerencia-prova/busca-avaliacoes instead
     */
    public function retornaLocaisProvaAction()
    {
        $this->_helper->redirector('busca-avaliacoes', 'gerencia-prova', 'default', $this->getAllParams());
    }


    public function sincronizarAvaliacaoMoodleAction()
    {
        $ws = new MoodleAlocacaoWebServices();
        $this->resposta($ws->sincronizarNotas($this->_getParam('courseid'), $this->_getParam('id_saladeaula')));
    }

    /**
     * Método responsavel por fazer o envio do TCC do aluno
     * @author Rafael Bruno (RBD) <rafaelbruno.ti@gmail.com>
     */
    public function uploadTccAction()
    {

        $this->_helper->viewRenderer->setNoRender(true);

        //Fazendo upload do tcc
        $bo = new TCCBO();
        $arquivo = null;
        $post = $this->_request->getPost();

        $mensageiro = new Ead1_Mensageiro();

        if (isset($_FILES['arquivo_tcc']['tmp_name']) && $_FILES['arquivo_tcc']['tmp_name'] != '') {
            $arquivo = $_FILES['arquivo_tcc'];
        }

        if ($arquivo != null && $post['st_tituloavaliacao'] != null) {
            //Salvando dados da avaliacao do aluno
            $aaTO = new AvaliacaoAlunoTO();
            $aaTO->setId_matricula($post['id_matricula']);
            $aaTO->setId_avaliacaoconjuntoreferencia($post['id_avaliacaoconjuntoreferencia']);
            $aaTO->setId_avaliacao($post['id_avaliacao']);
            $aaTO->setSt_tituloavaliacao($post['st_tituloavaliacao']);
            $aaTO->setSt_nota(null, false);

            $avParamTO = new AvaliacaoParametrosTO();
            $avParamTO->setId_avaliacaoaluno($post['id_avaliacao']);

            $mensageiro = $bo->salvarNotaTCC($aaTO, array($avParamTO), $arquivo);
        } else {
            $this->_helper->json($mensageiro->setMensagem('Ops... Por favor, verifique o nome do seu tcc e o arquivo que está selecionado.', Ead1_IMensageiro::ERRO));
            return false;
        }


        //Alterando a situacao do TCC para entregue
        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $alocacaoTO = new AlocacaoTO();
            $alocacaoTO->setId_alocacao($post['id_alocacao']);
            $alocacaoTO->setId_saladeaula($post['id_saladeaula']);
            $alocacaoTO->setId_situacaotcc(\G2\Constante\Situacao::TB_ALOCACAO_EM_ANALISE);

            $avaliacaoBO = new AvaliacaoBO();
            $mensageiro = $avaliacaoBO->alteraSituacaoTCC($alocacaoTO);

            $toEmail = new VwAlunosSalaTO();
            $toEmail->setId_saladeaula($post['id_saladeaula']);
            $toEmail->setId_alocacao($post['id_alocacao']);
            $toEmail->setId_entidade($toEmail->getSessao()->id_entidade);
            $toEmail->setId_matricula($post['id_matricula']);

            //Enviado email para o aluno com a confirmação de envio de TCC
            if ($toEmail->fetch(false, true, true)) {
                $mensageiro = $avaliacaoBO->enviarEmailEnvioTcc($toEmail);
            }
            echo Zend_Json::encode($mensageiro->setMensagem('Parabéns, seu arquivo foi enviado com sucesso :)'));
        }
    }

    /**
     * Action responsavel por trazer os dados do aluno referentes ao TCC
     * @author Débora <debora.pm@gmail.com> e Rafael Bruno (RBD) <rafaelbruno.ti@gmail.com>
     * @update Débora 09-04-2014 - AC-1207
     * @update Denise 21/10/214 - AC-26494
     */
    public function enviarTccAction()
    {

        if (!$this->getRequest()->isXmlHttpRequest())
            $this->_helper->layout->setLayout('layout');

        $this->view->TCCLiberado = true;

        $vwAlunoGradeIntegracao = new VwAlunoGradeIntegracaoTO();
        $vwAlunoGradeIntegracao->setId_matricula(Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula);
        $vwAlunoGradeIntegracao->setId_entidade(Ead1_Sessao::getSessaoEntidade()->id_entidade);
        $vwAlunoGradeIntegracao->setId_tipodisciplina(2);
        $vwAlunoGradeIntegracao->setBl_encerrado(0);

        $matriculaDAO = new MatriculaDAO();
        $dados = $matriculaDAO->retornarVwAlunoGradeIntegracao($vwAlunoGradeIntegracao)->toArray();
        $vw_AlunoGradeIntegracao = Ead1_TO_Dinamico::encapsularTo($dados, new VwAlunoGradeIntegracaoTO(), true);

        //Verifica se a turma não foi encerrada ainda caso exista sala de PRR
        if (sizeof($dados) > 0) {
            foreach ($dados as $vw) {
                if ($vw['bl_encerrado'] == 0) {
                    $vw_AlunoGradeIntegracao = new VwAlunoGradeIntegracaoTO($vw);
                }
            }

        }
        $this->view->dadosAluno = $vw_AlunoGradeIntegracao;

        if (sizeof($vw_AlunoGradeIntegracao) > 0) {
            $id_saladeaula = $vw_AlunoGradeIntegracao->getId_saladeaula();
            $id_matricula = $vw_AlunoGradeIntegracao->getId_matricula();
        }

        if (!isset($id_saladeaula) || !isset($id_matricula) || $vw_AlunoGradeIntegracao->getId_situacaotcc() == AlocacaoTO::SITUACAOTCC_NAOAPTO) {
            $this->_helper->viewRenderer->setNoRender(true);
        } else {
            //Consultando os dados da avaliacao do aluno
            $vwAvaliacaoAlunoTO = new VwAvaliacaoAlunoTO();
            $vwAvaliacaoAlunoTO->setId_matricula($id_matricula);
            $vwAvaliacaoAlunoTO->setId_saladeaula($id_saladeaula);
            $vwAvaliacaoAlunoTO->setId_tipoavaliacao(6);
//            $vwAvaliacaoAlunoTO->fetch(FALSE, TRUE, TRUE);
            $AvaliacaoBO = new AvaliacaoBO();
            $dadosAvaliacao = $AvaliacaoBO->retornarVwAvaliacaoAluno($vwAvaliacaoAlunoTO);
//            Zend_Debug::dump($dadosAvaliacao->getFirstMensagem());die;

            $this->view->avaliacao = $dadosAvaliacao->getFirstMensagem();

            //Verificando se a avaliação não foi encerrada pelo professor
            $vwencerramento = new VwEncerramentoAlunosTO();
            $vwencerramento->setId_matricula($id_matricula);
            $vwencerramento->setId_saladeaula($id_saladeaula);
            $vwencerramento->fetch(false, true, true);
            $this->view->encerramento = $vwencerramento;

            $this->view->encerrada = false;
            if ($vwencerramento->getDt_encerramentoprofessor()) {
                $this->view->encerrada = true;
            }

            $this->view->podePublicar = $vw_AlunoGradeIntegracao->getId_situacaotcc() == AlocacaoTO::SITUACAOTCC_APTO ? true : false;
            $this->view->url_upload = $this->baseUrl . Ead1_Ambiente::geral()->baseUrlUpload . '/avaliacao/';

            if ($vw_AlunoGradeIntegracao->getId_upload()) {
                $up = new UploadTO();
                $up->setId_upload($vw_AlunoGradeIntegracao->getId_upload());
                $up->fetch(true, true, true);
                $this->view->upload = $up;
            }
        }

        /* As linhas abaixo impedem que o TCC seja enviado caso a data
        * de encerramento da sala (somada aos dias de extensão) seja
        * menor que a data atual. A variável TCCLiberado serve como controle
        * para a exibição do modal (com a opção de envio ou mensagem de erro)
        * no enviar-tcc.phtml */
        if (isset($id_saladeaula) && !empty($id_saladeaula)) {
            $saladeaula = $this->negocio->find('\G2\Entity\SalaDeAula', $id_saladeaula);

            // Ajusta a dt_encerramento, somando os dias de extensão.
            if ($saladeaula->getDt_encerramento()) {
                $dataUsa = $saladeaula->getDt_encerramento()->format('Y-m-d');
                $saladeaula->setDt_encerramento(date('Y/m/d', strtotime("+{$saladeaula->getNu_diasextensao()} days", strtotime($dataUsa))));
            } else {
                $saladeaula->setDt_encerramento('-');
            }

            $dataAtual = date('Y/m/d');

            if ($dataAtual > $saladeaula->getDt_encerramento()) {
                $this->view->TCCLiberado = false;
            }
        }
    }

    /**
     * Método responsavel por consultar os dados de um aluno na VwAlunoIntegracao
     * @author Rafael Bruno (RBD) <rafaelbruno.ti@gmail.com>
     * @return JSON Data
     */
    public function carregaVwAlunoGradeIntegracaoAction()
    {
        $vwAlunoGradeIntegracao = new VwAlunoGradeIntegracaoTO();

        $vwAlunoGradeIntegracao->setId_matricula(Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula);
        $vwAlunoGradeIntegracao->setId_entidade(Ead1_Sessao::getSessaoUsuario()->id_entidade);
        $vwAlunoGradeIntegracao->setId_tipodisciplina(2);
        $vwAlunoGradeIntegracao->setId_usuario(Ead1_Sessao::getSessaoUsuario()->id_usuario);
        $vwAlunoGradeIntegracao->setId_situacaotcc(AlocacaoTO::SITUACAOTCC_APTO);

        $vwAlunoGradeIntegracao->fetch(false, true, true);

        return $this->_helper->json(array($vwAlunoGradeIntegracao->toArray()));
    }

    /**
     * Salva dados básicos do usuário antes do agendamento.
     */
    public function salvarDadosBasicosUsuarioAction()
    {
        $this->_helper->layout->setLayout('layout');

        $postdata = (array)json_decode($this->getRequest()->getRawBody(), true);
        $params = array_merge($this->limpaGetAllParams($this->getAllParams()), $postdata);

        $portalAgendamentoBo = new PortalAgendamentoBO();
        $retorno = $portalAgendamentoBo->atualizarDadosBasicosUsuario($params);

        if ($retorno && $retorno->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->setHttpResponseCode(200);
        } else {
            $this->getResponse()->setHttpResponseCode(400);
        }

        $this->_helper->json($retorno);
    }

    /*
     * Método para carregar combos de UF
     */

    public function buscaUfAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $bo = new PesquisarBO();
        $uf = $bo->retornarSelectUf();
        $arrReturn = array();
        if ($uf) {
            foreach ($uf as $value) {
                $arrReturn[] = array(
                    'id_value' => $value['valor'],
                    'st_text' => $value['label']
                );
            }
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    /*
     * Método para carregar combos de municípios com aplicações ativas
     * (agendamento de prova)
     */

    public function retornaMunicipioAplicacaoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $params = null;
        $params['sg_uf'] = $this->getParam('sg_uf');

        $result = $this->negocio->findByVwMunicipioAplicacao($params);
        $arrReturn = array();
        if ($result) {
            foreach ($result as $value) {
                $arrReturn[] = array(
                    'id_municipio' => $value->getId_municipio(),
                    'st_nomemunicipio' => $value->getSt_nomemunicipio()
                );
            }
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    public function retornaDadosAgendamentoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $params = array(
            'id_matricula' => $this->getParam('id_matricula'),
            'bl_ativo' => true,
            'id_situacao' => array(
                \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_AGENDADO,
                \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_REAGENDADO
            )
        );

        $result = $this->negocio->findByVwAvaliacaoAgendamento($params);
        $arrReturn = array();

        if ($result) {
            foreach ($result as $value) {
                $item = array(
                    'id_matricula' => $value->getId_matricula(),
                    'id_usuario' => $value->getId_usuario(),
                    'id_avaliacao' => $value->getId_avaliacao(),
                    'id_tipodeprova' => $value->getId_tipodeavaliacao(),
                    'id_tipodeavaliacao' => $value->getId_tipodeavaliacao(),
                    'id_situacao' => $value->getId_situacao(),
                    'id_mensagens' => $value->getId_mensagens(),
                    'id_aplicadorprova' => $value->getId_aplicadorprova(),
                    'st_aplicadorprova' => $value->getSt_aplicadorprova(),
                    'bl_provaglobal' => $value->getBl_provaglobal(),
                    'bl_ativo' => $value->getBl_ativo(),
                    'st_endereco' => $value->getSt_endereco(),
                    'st_cidade' => $value->getSt_cidade(),
                    'st_telefoneaplicador' => $value->getSt_telefoneaplicador() ? $value->getSt_telefoneaplicador() : '',
                    'id_avaliacaoaplicacao' => $value->getId_avaliacaoaplicacao(),
                    'st_projetopedagogico' => $value->getSt_projetopedagogico(),
                    'id_projetopedagogico' => $value->getId_projetopedagogico(),
                    'sg_uf' => $value->getSg_uf(),
                    'id_avaliacaoagendamento' => $value->getId_avaliacaoagendamento(),
                    'dt_aplicacao' => \G2\Utils\Helper::converterData($value->getDt_aplicacao(), "d/m/Y"),
                    'hr_inicioprova' => \G2\Utils\Helper::converterData($value->getHr_inicio(), "H:i"),
                    'hr_fimprova' => \G2\Utils\Helper::converterData($value->getHr_fim(), 'H:i'),
                    'id_disciplina' => $value->getId_disciplina(),
                    'st_disciplina' => $value->getSt_disciplina(),
                    'disciplinas' => array()
                );

                $bl_provapordisciplina = (new \G2\Negocio\EsquemaConfiguracao())->isProvaPorDisciplina();

                // BUSCAR DISCIPLINAS E REFERENCIAS
                if ($item['id_avaliacaoagendamento'] && !$bl_provapordisciplina) {
                    $disciplinas = $this->negocio->findByVwDisciplinasAgendamento(array(
                        'id_matricula' => $item['id_matricula'],
                        'id_avaliacao' => $item['id_avaliacao'],
                        'id_avaliacaoagendamento' => $item['id_avaliacaoagendamento'],
                        'bl_agendado' => 1
                    ), array(
                        // ORDER BY
                        'st_disciplina' => 'ASC'
                    ));
                    $method = 'getSt_disciplina';
                } else {
//                    echo 'a'; exit;
                    $ng_avaliacao = new \G2\Negocio\Avaliacao();
                    $disciplinas = $ng_avaliacao->findVwAvaliacaoAluno(array(
                        'id_matricula' => $item['id_matricula'],
                        'id_avaliacao' => $item['id_avaliacao'],
                        'id_disciplina' => $item['id_disciplina']
//                        'id_avaliacaoagendamento' => $item['id_avaliacaoagendamento']
                    ), array(
                        // ORDER BY
                        'st_tituloexibicaodisciplina' => 'ASC'
                    ));
                    $method = 'getSt_tituloexibicaodisciplina';
                }

                if (is_array($disciplinas) && $disciplinas) {
                    foreach ($disciplinas as $disciplina) {
                        $item['disciplinas'][$disciplina->getId_disciplina()] = array(
                            'id_disciplina' => $disciplina->getId_disciplina(),
                            'st_disciplina' => $disciplina->$method()
                        );
                        $item['id_avaliacaoconjuntoreferencia'][] = $disciplina->getId_avaliacaoconjuntoreferencia();
                    }
                    $item['disciplinas'] = array_values($item['disciplinas']);
                }

                $arrReturn[] = $item;
            }
        }
        $this->_helper->json($arrReturn);
    }


    /**
     * Exibe uma caixa de informação na home do portal, indicando
     * que o aluno está apto a agendar uma prova final,
     * uma prova de recuperação ou reagendar.
     */
    public function boxAgendamentoAction()
    {
        $this->view->podeAgendar = false;
        $this->view->podeReagendar = false;
        $this->view->st_avaliacao = '';

        $id_matricula = \Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula;

        // Verificar se é GRA ou PÓS
        $ng_matricula = new \G2\Negocio\Matricula();
        $bl_matriculagraduacao = $ng_matricula->isMatriculaGraduacao($id_matricula);

        // Verificar se o aluno está apto
        $status = $this->negocio->verificarAlunoAptoAgendamento($id_matricula);

        /**
         * @history GII-9192
         */
        if ($status == 3 || $status == 4) {
            $this->view->podeReagendar = true;
        } else if (!$bl_matriculagraduacao) {
            if ($status == 1) {
                $this->view->podeAgendar = true;
                $this->view->st_avaliacao = 'DE SUA PROVA FINAL (PRESENCIAL)';
            } else if ($status == 2) {
                $this->view->podeAgendar = true;
                $this->view->st_avaliacao = 'DE SUA PROVA DE RECUPERAÇÃO (PRESENCIAL)';
            }
        }
    }

    public function listaDisciplinasAction()
    {
        $negocio = new \G2\Negocio\GerenciaProva();
        $this->_helper->viewRenderer->setNoRender(true);

        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);
        $params['bl_agendado'] = 1;

        $result = $negocio->findByVwDisciplinasAgendamento($params);
        $arrReturn = array();
        if ($result && is_array($result)) {
            foreach ($result as $row) {
                $arrReturn[] = array(
                    'id_disciplina' => $row->getId_disciplina(),
                    'st_disciplina' => $row->getSt_disciplina(),
                    'id_avaliacaoagendamento' => $row->getId_avaliacaoagendamento()
                );
            }
        }
        $this->getResponse()->appendBody(json_encode(array_reverse($arrReturn)));
    }

    public function imprimirAgendamentoAction()
    {
        if ($this->_getParam('id_avaliacaoagendamento') && Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula) {
            $params['id_avaliacaoagendamento'] = $this->_getParam('id_avaliacaoagendamento');
            $result = $this->negocio->findByVwAvaliacaoAgendamento($params);

            if ($result) {
                $negocio = new \G2\Negocio\GerenciaProva();
                $this->view->texto = utf8_decode($negocio->retornaTextoMensagemAgendamento($negocio->toArrayEntity($result[0])));
            }
        } else {
            $this->view->texto = 'Agendamento inválido!';
        }
    }


    /**
     * Tela responsável por abrir envio de TCC para o aluno dentro do portal
     * @author Denise Xavier <denise.xaiver@unyleya.com.br>
     */
    public function enviarTccMoodleAction()
    {
        $this->_helper->layout()->disableLayout();

        $negocioAvaliacao = new \G2\Negocio\Avaliacao();
        $mensageiro = $negocioAvaliacao->verificaEnvioTccMoodle($this->getAllParams(), $this->baseUrl);
        $this->view->baseUrl = $this->baseUrl;
        $this->view->mensageiro = $mensageiro;
    }
}

<?php

use \G2\Transformer\AtividadeComplementar\AtividadeAlunoTransformer,
    \G2\Constante\Situacao,
    \G2\Transformer\AtividadeComplementar\TipoAtividadeComplementarTransformer;

/**
 * Class Portal_AtividadeComplementarController
 */
class Portal_AtividadeComplementarController extends PortalBase
{
    /** @var  \G2\Negocio\AtividadeComplementar */
    private $negocio;

    public function init()
    {
        $this->negocio = new \G2\Negocio\AtividadeComplementar();
        $this->_helper->layout()->setLayout("layout");

        if ($this->_helper->FlashMessenger->hasMessages()) {
            $this->view->messages = $this->_helper->FlashMessenger->getMessages();
        }

        parent::init();
    }

    public function indexAction()
    {
        try {
            $this->view->atividades = [];

            $result = $this->negocio->findBy(\G2\Entity\AtividadeComplementar::class, [
                "id_matricula" => Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula,
                "bl_ativo" => true
            ], ["id_atividadecomplementar" => "DESC"]);


            $projetoNegocio = new \G2\Negocio\ProjetoPedagogico();
            $projeto = $projetoNegocio->findOneProjetoPedagogico([
                'id_projetopedagogico' => Ead1_Sessao::getSessaoProjetoPedagogico()->id_projetopedagogico
            ]);

            if ($projeto) {
                $this->view->qtdHorasAprovacao = $projeto->getNu_horasatividades();
            }

            if ($result) {
                $this->view->atividades = (new AtividadeAlunoTransformer($result))->getTransformer();

                $this->view->qtdHorasAprovadas = $this->negocio
                    ->calculaTotalHoras(
                        $this->view->atividades,
                        'id_situacao',
                        Situacao::TB_ATIVIDADECOMPLEMENTAR_DEFERIDO
                    );
            }

        } catch (Exception $e) {
            $this->view->mensagemRetorno = array(
                "message" => $e->getMessage(),
                "icon" => "icon-warning-sign",
                "cssClass" => "red",
                'type' => 'error'
            );
        }

    }

    public function cadastrarAction()
    {
        $this->view->tipoAtividade = $this->negocio
            ->findBy(\G2\Entity\TipoAtividadeComplementar::class, [
                "bl_ativo" => true
            ], [
                "st_tipoatividadecomplementar" => "ASC"
            ]);


    }

    public function salvarAtividadeAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        try {
            if ($this->getRequest()->isPost()) {
                $dados = $this->getRequest()->getPost();
                $anexo = null;

                $adapter = new Zend_File_Transfer_Adapter_Http();
                $file = $adapter->getFileInfo();

                if ($file) {
                    $anexo = $file['anexo'];
                }

                $dados['id_matricula'] = Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula;
                $result = $this->negocio->salvarAtividade($dados, $anexo);

                if (!($result instanceof \G2\Entity\AtividadeComplementar)) {
                    throw new Exception("Ops! Houve um erro inesperado, objeto não esperado.");
                }

                $this->view->mensagemRetorno = array(
                    "message" => "Seu arquivo foi enviado para análise.",
                    "icon" => "icon-ok",
                    "cssClass" => "green",
                    'type' => 'success'
                );

                $this->_helper->flashMessenger
                    ->addMessage($this->view->render("atividade-complementar/alert-template.phtml"));
                $this->redirect('/portal/atividade-complementar');

            }
        } catch (Exception $e) {
            $this->view->mensagemRetorno = array(
                "message" => $e->getMessage(),
                "icon" => "icon-warning-sign",
                "cssClass" => "red",
                'type' => 'error'
            );
            $this->_helper->flashMessenger
                ->addMessage($this->view->render("atividade-complementar/alert-template.phtml"));
            $this->redirect('/portal/atividade-complementar/cadastrar');
        }
    }


    public function removerAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        try {

            $idAtividade = $this->getParam("id");

            $result = $this->negocio->removerAtividade($idAtividade);

            if (!($result instanceof \G2\Entity\AtividadeComplementar)) {
                throw new Exception("Ops! Houve um erro inesperado, objeto não esperado.");
            }

            $this->view->mensagemRetorno = array(
                "message" => "Atividade removida com sucesso.",
                "icon" => "icon-ok",
                "cssClass" => "green",
                'type' => 'success'
            );

        } catch (Exception $e) {
            $this->view->mensagemRetorno = array(
                "message" => $e->getMessage(),
                "icon" => "icon-warning-sign",
                "cssClass" => "red",
                'type' => 'error'
            );
        }
        $this->_helper->flashMessenger->addMessage($this->view->render("atividade-complementar/alert-template.phtml"));
        $this->redirect('/portal/atividade-complementar');
    }

}

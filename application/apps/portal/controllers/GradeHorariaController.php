<?php


/**
 * Class Portal_GradeHorariaController
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Portal_GradeHorariaController extends PortalBase
{
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\GradeHoraria();
        $this->_helper->layout()->setLayout('layout');
    }

    public function indexAction()
    {
        $diasSemanaNegocio = new \G2\Negocio\DiaSemana();
        $getSessaoProjeto = Ead1_Sessao::getSessaoProjetoPedagogico();

        $idMatricula = $getSessaoProjeto->getId_matricula();
        $result = $this->negocio->retornarGradeHorariaByMatricula($idMatricula);

        $this->view->diasSemana = $diasSemanaNegocio->findAll();
        $this->view->dadosProjeto = $getSessaoProjeto;
        $this->view->dadosGrade = $result;
    }

} 
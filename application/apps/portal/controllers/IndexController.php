<?php

/**
 * Controller de Index (Portal) do Portal do Aluno
 * @author eduardoromao
 */
class Portal_IndexController extends PortalBase
{

    public $dadosSala;
    public $_SalaDeAulaBO;
    /** @var  PortalSalaDeAulaBO */
    private $_salaBO;
    private $perfilBO;

    public function preDispatch()
    {
        parent::preDispatch();
    }


    /**
     * @see PortalController::init()
     */
    public function init()
    {
        parent::init();
        $this->_salaBO = new PortalSalaDeAulaBO();
        $this->view->dadosSala = $this->dadosSala;
        $this->_SalaDeAulaBO = new SalaDeAulaBO();
        $this->perfilBO = new PerfilBO();
    }

    /**
     * Action inicial da Controller Index
     * - Monta Layout
     * - Destroi a Sessão
     */
    public function indexAction()
    {
        //Redirecionando para a validação dos dados caso a sessão já esteja ativa
        if (Ead1_Sessao::getSessaoUsuario()->id_usuario != null) {

            $bo = new PortalLoginBO();
            $vwUsuarioPPTO = new VwUsuarioPerfilPedagogicoTO();
            $vwUsuarioPPTO->setSt_login(Ead1_Sessao::getSessaoUsuario()->st_login);
            $vwUsuarioPPTO->setSt_email(Ead1_Sessao::getSessaoUsuario()->st_login);
            $vwUsuarioPPTO->setSt_senha(Ead1_Sessao::getSessaoUsuario()->st_senhaentidade);
            $vwUsuarioPPTO->setSt_senhaentidade(Ead1_Sessao::getSessaoUsuario()->st_senha);
            $mensageiro = $bo->validateUserPass($vwUsuarioPPTO);
            if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                $this->_helper->layout()->setLayout('login');
            } else {
                $this->forward('validar', 'login', 'portal');
            }

        } else {
            $this->_helper->layout()->setLayout('login');
        }

    }

    public function logoutAction()
    {
        $usersession = new \G2\Negocio\UsuarioSessao();
        $usersession->logoutHash();
        if (getenv('id_entidade')) {
            $ng = new \G2\Negocio\Entidade();
            $objetoEntidade = $ng->retornaUrlLogoutPortal(getenv('id_entidade'));
            if ($objetoEntidade->getSt_urllogoutportal()) {
                $this->_redirect($objetoEntidade->getSt_urllogoutportal());
            } else {
                //Buscando URL do moodle para fazer logout
                $session = new Zend_Session_Namespace('moodle');
                $this->view->urlMoodle = substr($session->url, 0, -26);
            }
        } else {
            //Buscando URL do moodle para fazer logout
            $session = new Zend_Session_Namespace('moodle');
            $this->view->urlMoodle = substr($session->url, 0, -26);
        }
        session_destroy();
    }

    /**
     * Action da tela principal de Alunos
     */
    public function alunoAction()
    {
        $negocio = new \G2\Negocio\Negocio();
        //Grava-log
        $logTO = new LogAcessoTO();
        $logTO->setId_funcionalidade(LogAcessoTO::PRINCIPAL);
        $logRO = new LogBO();
        $logRO->gravaLog($logTO);
        $this->_helper->layout()->setLayout('layout');
        try {
            $idEntidade = Ead1_Sessao::getSessaoEntidade()->id_entidade;

            //busca os dados da linha de negócio da entidade logada
            $linhaNegocio = (new \G2\Negocio\EsquemaConfiguracao())
                ->retornaItemPorEntidade(\G2\Constante\ItemConfiguracao::LINHA_DE_NEGOCIO, $idEntidade);
            //verifica e define se a linha de negócio é pos
            $isPosGraduacao = false;
            if ($linhaNegocio && (int)$linhaNegocio["st_valor"] === \G2\Constante\LinhaDeNegocio::POS_GRADUACAO) {
                $isPosGraduacao = true;
            }

            //passa o valor para a view
            $this->view->isPosGraduacao = $isPosGraduacao;
            $this->view->listaSaladeaula = [];
            $sessaoPerfil = Ead1_Sessao::getSessaoPerfil();

            if ($sessaoPerfil->is_aluno() || $sessaoPerfil->is_alunoInstitucional()) {

                $sessaoProjeto = Ead1_Sessao::getSessaoProjetoPedagogico();
                $this->view->matricula = $sessaoProjeto->id_matricula;
                $vwAlunoGradeIntegracaoTO = new VwAlunoGradeIntegracaoTO();
                $vwAlunoGradeIntegracaoTO->setId_matricula($sessaoProjeto->id_matricula);
                $vwAlunoGradeIntegracaoTO->setId_projetopedagogico($sessaoProjeto->id_projetopedagogico);
                $vwAlunoGradeIntegracaoTO->setId_usuario(Ead1_Sessao::getSessaoUsuario()->id_usuario);

                $mensageiro = $this->_salaBO->retornarAlunoSalaDeAula($vwAlunoGradeIntegracaoTO, $isPosGraduacao);
                $mensageiro->subtractMensageiro();

                $this->view->listaSaladeaula = $mensageiro->getMensagem();

                $pp = new ProjetoPedagogicoTO();
                $pp->setId_projetopedagogico($sessaoProjeto->id_projetopedagogico);
                $pp->fetch(true, true, true);
                $this->view->pp = $pp;

                $detalhesCoordenador = $negocio->findOneBy("\G2\Entity\VwUsuarioPerfilEntidadeReferencia",
                    array('id_projetopedagogico' => Ead1_Sessao::getSessaoProjetoPedagogico()->id_projetopedagogico,
                        'id_perfilpedagogico' => G2\Constante\PerfilPedagogico::COORDENADOR_DE_PROJETO,
                        'bl_titular' => 1));

                if ($detalhesCoordenador instanceof \G2\Entity\VwUsuarioPerfilEntidadeReferencia) {
                    $this->view->nomeCoordenadorTitular = $detalhesCoordenador->getSt_nomecompleto();
                }


                /*
                 * Verificando se o usuário possui mensagens e seta na sessão
                 * Verifica apenas se o usuário já tiver id_entidade e id_perfil na sessão
                 * */
                if ($sessaoPerfil->id_perfil
                    && $idEntidade
                    && $sessaoPerfil->is_aluno()) {
                    $mensagem = new PortalMensagemBO();
                    $mensagem->retornarMensagemAluno();
                }

                /*
                 * Gerando mensagem a ser apresentada na tela ao aluno caso entre em um  dos fluxus abaixo
                 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
                 */
                $this->view->message = "Estamos preparando seu ambiente de estudos. Aguarde alguns minutos e recarregue as salas!";

                $arrMensagem = [
                    \G2\Constante\Evolucao::TB_MATRICULA_CERTIFICADO => "Não existe curso em andamento para o usuário informado!",
                    \G2\Constante\Evolucao::TB_MATRICULA_DECURSO_DE_PRAZO => "Sua matrícula não está mais ativa no Portal do Aluno.",
                ];

                $evolucaoMatricula = Ead1_Sessao::getSessaoGeral()->id_evolucaomatricula;

                if (in_array($evolucaoMatricula, $arrMensagem)) {
                    $this->view->message = $arrMensagem[$evolucaoMatricula];
                }
            } else {
                $this->redirect($this->baseUrlModulo);
            }


        } catch (Zend_Exception $e) {
            $this->view->erro = $e->getMessage();
        }
    }

    private function retornarAlertaResult($id_mensagempadrao, $view_varname)
    {
        $aTO = new AlertaTO();
        $pmpmBO = new PortalMensagemPadraoMatriculaBO();
        $aTO->setId_matricula(Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula);
        // Pop-in de imposto de renda
        $aTO->setId_mensagempadrao($id_mensagempadrao);
        $mensageiroAlerta = $pmpmBO->retornarAlerta($aTO);
        if ($mensageiroAlerta->getTipo() != Ead1_IMensageiro::SUCESSO) {
            throw new Zend_Exception($mensageiroAlerta->getMensagem());
        } else {
            $result = $mensageiroAlerta->getFirstMensagem();
            $this->view->$view_varname = !$result;
        }
    }

    /**
     * Action que gerar os pop-in de alerta na tela
     */
    public function gerarAlertaAction()
    {

        if ($_POST["aviso"] == "true") {
            try {
                $bo = new PortalMensagemPadraoMatriculaBO();
                $to = new AlertaTO();
                $to->setId_mensagempadrao($_POST["id_mensagempadrao"]);
                $to->setId_matricula(Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula);
                $mensageiro = $bo->cadastrarAlerta($to);
            } catch (Zend_Exception $e) {
                $this->_redirect($this->baseUrlModulo);
            }
        }
        $this->_helper->viewRenderer->setNoRender(true);
        return true;
    }

    /**
     * Action da tela principal de Professores!
     */
    public function professorAction()
    {

        $this->view->filtro = $this->_getParam('filtro');

        $page = $this->getParam('page', 1);

        $limit = 20;
        $offset = ($page * $limit) - $limit;

        try {
            //Grava-log
            $logTO = new LogAcessoTO();
            $logTO->setId_funcionalidade(LogAcessoTO::PRINCIPAL);
            $logRO = new LogRO();
            $logRO->gravaLog($logTO);

            $this->view->erro = null;
            $this->view->listSalas = null;


            if (Ead1_Sessao::getSessaoPerfil()->is_professor() && $this->getParam('json') == true) {
                //Parametro responsável pelo o retorno em json.
                $this->_helper->viewRenderer->setNoRender(true);

                $id_status = ($this->_getParam('filtro') != ''
                    ? ($this->_getParam('filtro') == 4 ? null : $this->_getParam('filtro')) : 0);
                $id_categoriasala = $this->getParam('id_categoriasala', null);
                $id_tipodisciplina = $this->getParam('id_tipodisciplina', null);
                $id_tipodisciplina_dif = $this->getParam('id_tipodisciplina_dif', null);

                $where = array(
                    'id_usuario' => Ead1_Sessao::getSessaoUsuario()->id_usuario,
                    'id_entidade' => Ead1_Sessao::getSessaoEntidade()->id_entidade,
                    'id_perfilpedagogico' => Ead1_Sessao::getSessaoPerfil()->id_perfilpedagogico,
                    'id_status' => $id_status,
                );

                if ($id_categoriasala) {
                    $where['id_categoriasala'] = $id_categoriasala;
                }
                if ($id_tipodisciplina) {
                    $where['id_tipodisciplina'] = $id_tipodisciplina;
                }
                if ($id_tipodisciplina_dif) {
                    $where['id_tipodisciplina !='] = $id_tipodisciplina_dif;
                }
                if (!empty($this->_getParam('text'))) {
                    $where['st_disciplina'] = $this->_getParam('text');
                }

                if ($where['id_status'] == '0') {
                    unset($where['id_status']);
                }

                $negocio = new \G2\Negocio\VwSaladeAulaIntegracao();
                $results = $negocio->getDisciplinasProfessor($where, array('st_disciplina' => 'ASC'), $limit, $offset);


                $this->_helper->json($results);

            }
        } catch (Zend_Exception $e) {
            if ($this->getParam('json') == true) {
                $this->_helper->viewRenderer->setNoRender(true);
                echo Zend_Json::encode(array('st_disciplina' => $e->getMessage()));
            }
        }
    }

    /**
     * Action da tela principal de Professores!
     */
    public function coordenadorAction()
    {

        $this->view->filtro = $this->_getParam('filtro');

        $page = $this->_getParam('page', 1);
        $rowCount = $this->_getParam('count', 20);
        $text = $this->_getParam('text', false);

        try {
            //Grava-log
            $logTO = new LogAcessoTO();
            $logTO->setId_funcionalidade(LogAcessoTO::PRINCIPAL);
            $logRO = new LogRO();
            $logRO->gravaLog($logTO);

            $this->view->erro = null;
            $this->view->listSalas = array();
            $this->view->idStatus = ($this->_getParam('id_status') != ''
                ? $this->_getParam('id_status') : 1);
            if (Ead1_Sessao::getSessaoPerfil()->is_coordenadorProjetoPedagogico() ||
                Ead1_Sessao::getSessaoPerfil()->is_coordenadorDisciplina() ||
                Ead1_Sessao::getSessaoPerfil()->is_observadorInstitucional() ||
                Ead1_Sessao::getSessaoPerfil()->is_suporte()
            ) {
                $salaBO = new PortalSalaDeAulaBO();

                $retorno_projetos = $salaBO->retornaListagemProjetosSalaDoctrine(array(
                    'id_usuario' => Ead1_Sessao::getSessaoUsuario()->id_usuario,
                    'id_entidade' => Ead1_Sessao::getSessaoEntidade()->id_entidade,
                    'id_perfilpedagogico' => Ead1_Sessao::getSessaoPerfil()->id_perfilpedagogico,
                    'st_projetopedagogico' => $text,
                    'id_status' => null
                ), $page, $rowCount);


                //Parametro responsável pelo o retorno em json.
                if ($this->getParam('json') == true) {
                    $this->_helper->viewRenderer->setNoRender(true);
                    if ($retorno_projetos) {
                        echo Zend_Json::encode($retorno_projetos);
                    } else {
                        echo Zend_Json::encode(array(
                            'st_projetopedagogico' => 'Nenhum curso encontrado',
                            'st_coordenador' => null
                        ));
                    }
                } else {
                    $this->_helper->layout()->setLayout('layout');
                    $this->view->listSalas = $retorno_projetos;
                }
            } else {
                $this->redirect('/portal/index/logout');
            }
        } catch (Zend_Exception $e) {
            if ($this->getParam('json') == true) {
                $this->_helper->viewRenderer->setNoRender(true);
                echo Zend_Json::encode(array('st_projetopedagogico' => $e->getMessage()));
            }

        }
    }


    /**
     * Action da Tela de Seleção de sala de aula
     */
    public function principalAction()
    {
        //verifica se o aluno deve passar pelo primeiro acesso acessando atraves da helper.
        if ($this->view->verificaPrimeiroAcesso()) {
            $this->redirect($this->baseUrlModulo . '/primeiro-acesso/');
        } else {
            $this->_helper->layout()->setLayout('layout');
        }
    }

    /**
     * Action do LMS??
     */
    public function lmsAction()
    {
        try {
            //Grava-log
            $logTO = new LogAcessoTO();
            $logTO->setId_funcionalidade(LogAcessoTO::ACESSO_SALA_DE_AULA);
            $logRO = new LogRO();

            //validar data de abertura da sala de aula
            $this->view->urlSalaDeAula = null;
            $url = null;

            $frm_acesso_turma_id_disciplina = $this->_getParam('id_disciplina', null);
            $id_categoriasala = $this->_getParam('id_categoriasala', null);

            if (!$frm_acesso_turma_id_disciplina) {
                THROW new Zend_Exception("Disciplina não informada!");
            }
            $URL = null;
            $USUARIO_MOODLE = null;
            $SENHA_MOODLE = null;
            $CURSO_MOODLE = null;
            $INTEGRACAO = null;
            $COD_USUARIO = null;
            $COD_SISTEMA_ENT = null;

            if ((Ead1_Sessao::getSessaoPerfil()->is_aluno())
                || (Ead1_Sessao::getSessaoPerfil()->is_alunoInstitucional())
            ) {
                $vwAlunoGradeIntegracaoTO = new VwAlunoGradeIntegracaoTO();
                $vwAlunoGradeIntegracaoTO
                    ->setId_projetopedagogico(Ead1_Sessao::getSessaoProjetoPedagogico()->id_projetopedagogico);

                $vwAlunoGradeIntegracaoTO->setId_usuario(Ead1_Sessao::getSessaoUsuario()->id_usuario);
                $vwAlunoGradeIntegracaoTO->setId_disciplina($frm_acesso_turma_id_disciplina);
                $vwAlunoGradeIntegracaoTO->setId_matricula(Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula);
                $vwAlunoGradeIntegracaoTO->setId_categoriasala($id_categoriasala);

                $negocio = new \G2\Negocio\SalaDeAula();
                $sala = $negocio->retornarAlunoSalaDeAulaIntegracao($vwAlunoGradeIntegracaoTO);

                if (!($sala instanceof \G2\Entity\VwAlunoGradeIntegracao)) {
                    THROW new \Zend_Exception('Não foram encontrados registros para a sala selecionada!');
                }

                //Validando datas de abertura
                $hoje = new Zend_Date(null, Zend_Date::ISO_8601);
                $dt_abertura = new Zend_Date($sala->getDt_abertura()->format('Y-m-d'), \Zend_Date::ISO_8601);

                $logTO->setId_saladeaula($sala->getId_saladeaula());
                if ($hoje->isEarlier($dt_abertura)) {
                    THROW new \Zend_Exception('A Sala de Aula não Esta Aberta.');
                }
                if (!is_null($sala->getDt_encerramento())) {
                    $dt_encerramento = new \Zend_Date($sala->getDt_encerramento(), \Zend_Date::ISO_8601);
                    if ($hoje->isLater($dt_encerramento)) {
                        THROW new Zend_Exception('A Sala de Aula já Esta Encerrada!');
                    }
                }
                //Seta sala da aula na sessao
                Ead1_Sessao::setSessionProjetoPedagogico(array('id_saladeaula' => $sala->getId_saladeaula()));

                $USUARIO_MOODLE = $sala->getSt_loginintegrado();
                $SENHA_MOODLE = $sala->getSt_senhaintegrada();
                $CURSO_MOODLE = $sala->getSt_codsistemacurso();
                $INTEGRACAO = $sala->getSt_integracao();
                $COD_USUARIO = $sala->getSt_codusuario();
                $COD_SISTEMA_ENT = $sala->getSt_codsistemaent();
                $st_caminho = $sala->getst_caminho();

            } elseif (Ead1_Sessao::getSessaoPerfil()->is_professor()) {

                $where = array(
                    'id_usuario' => Ead1_Sessao::getSessaoUsuario()->id_usuario,
                    'id_entidade' => Ead1_Sessao::getSessaoEntidade()->id_entidade,
                    'id_disciplina' => $frm_acesso_turma_id_disciplina,
                    'id_saladeaula' => $this->_getParam('id_saladeaula', null),
                );

                $negocio = new \G2\Negocio\VwSaladeAulaIntegracao();
                $resultado = $negocio->getVwSalaDeAulaProfessorIntegracao($where);

                if ($resultado) {
                    $sala = array_shift($resultado);
                } else {
                    $sala = new \G2\Entity\VwSalaDeAulaProfessorIntegracao();
                }

                $USUARIO_MOODLE = $sala->getSt_loginintegrado();
                $SENHA_MOODLE = $sala->getSt_senhaintegrada();
                $CURSO_MOODLE = $sala->getSt_codsistemacurso();
                $COD_SISTEMA_ENT = $sala->getSt_codsistema();
                $st_caminho = $sala->getSt_caminho();

                $logTO->setId_saladeaula($this->_getParam('id_saladeaula', null));
            } else {
                $salaBO = new SalaDeAulaBO();
                $sala = new VwSalaDeAulaPerfilProjetoIntegracaoTO();
                $sala->setId_entidade(Ead1_Sessao::getSessaoEntidade()->id_entidade);
                $sala->setId_usuario(Ead1_Sessao::getSessaoUsuario()->id_usuario);
                $sala->setId_disciplina($frm_acesso_turma_id_disciplina);
                $sala->setId_saladeaula($this->_getParam('id_saladeaula', null));
                $mensageiro = $salaBO->retornarVwSalaDeAulaPerfilProjetoIntegracao($sala);
                $mensageiro->subtractMensageiro();
                $sala = $mensageiro->getFirstMensagem();

                $USUARIO_MOODLE = $sala->getSt_loginintegrado();
                $SENHA_MOODLE = $sala->getSt_senhaintegrada();
                $CURSO_MOODLE = $sala->getSt_codsistemacurso();
                $COD_SISTEMA_ENT = $sala->getSt_codsistema();
                $st_caminho = $sala->getst_caminho();
                $logTO->setId_saladeaula($this->_getParam('id_saladeaula', null));
            }

            /*
             * Criticar o looping
             * */

            switch ($sala->getId_sistema()) {
                case SistemaTO::MOODLE:
                    $sessaoMoodle = new Zend_Session_Namespace('moodle');
                    $sessaoMoodle->usuario = $USUARIO_MOODLE;
                    $sessaoMoodle->senha = $SENHA_MOODLE;
                    $sessaoMoodle->curso = $CURSO_MOODLE;
                    $sessaoMoodle->url = $COD_SISTEMA_ENT;

                    $idUsuarioOriginal = Ead1_Sessao::getSessaoUsuario()->id_usuariooriginal;

                    if (isset($idUsuarioOriginal)
                        && (Ead1_Sessao::getSessaoUsuario()->st_gravalog == 'nao')
                        && (Ead1_Sessao::getSessaoUsuario()->id_usuario != $idUsuarioOriginal)
                    ) {
                        $tb_usuariointegracao = new UsuarioIntegracaoTO();
                        $tb_usuariointegracao->setId_usuario($idUsuarioOriginal);
                        $tb_usuariointegracao->setId_sistema(SistemaTO::MOODLE);
                        $tb_usuariointegracao->setId_entidade($sala->getId_entidadesala());
                        $tb_usuariointegracao->setId_entidadeintegracao($sala->getId_entidadeintegracao());
                        $tb_usuariointegracao->fetch(false, true, true);

                        if (!$tb_usuariointegracao->getId_usuariointegracao()) {
                            throw new Zend_Exception('Você não tem perfil para acessar o moodle. 
                            Não é possível logar como aluno no sistema!');
                        }

                        //Seta a url do moodle com os dados para carregar no iframe
                        $URL = substr($COD_SISTEMA_ENT, 0,
                                -26)
                            . "login/indexg2.php?u=" . $tb_usuariointegracao->getSt_loginintegrado()
                            . "&p=" . $tb_usuariointegracao->getSt_senhaintegrada()
                            . "&c=" . $CURSO_MOODLE
                            . "&al=" . $COD_USUARIO;

                    } else {
                        $URL = substr($COD_SISTEMA_ENT, 0,
                                -26)
                            . "login/indexg2.php?u=" . $USUARIO_MOODLE
                            . "&p=" . $SENHA_MOODLE
                            . "&c=" . $CURSO_MOODLE;
                    }
                    break;

                case
                SistemaTO::ACTOR:
                    $URL = Ead1_Ambiente::geral()->portalWsUrl . "/Portal?c=" . $INTEGRACAO . "&p=" . $COD_USUARIO;
                    break;

                case SistemaTO::AMAIS:
                    if ($INTEGRACAO) {
                        $URL = $st_caminho . "/login-externo.jsp?l="
                            . $USUARIO_MOODLE . "&s=" . $SENHA_MOODLE . "&c=lms.css";
                    }
                    break;

                case SistemaTO::BLACKBOARD:
                    $userId = $sala->getId_entidade() . '_' . $sala->getId_usuario();
                    $id_saladeaula = $sala->getId_saladeaula();
                    $date = new DateTime();
                    $timestamp = $date->getTimestamp();

                    $URL = $sala->getSt_caminho()
                        . "/webapps/ME-autosignon-BBLEARN/autoSignon.do?courseId="
                        . $id_saladeaula . "&timestamp=" . $timestamp . "&userId=" . $userId
                        . "&auth=" . md5($timestamp . $userId . 'unyleya2014');

                    break;

                default:
                    THROW new Zend_Exception("Sistema não implementado!");
                    break;
            }
            $logRO->gravaLog($logTO);
            $this->view->salaView = $sala;
            $this->view->urlSalaDeAula = $URL;


            /*
             * Autor: Felipe Pastor
             * Descrição: Tive que replicar o código da função alunoAction/professoAction/coordenadorAction, por estar muito acomplado a view,
             * fica impossível manipular os dados dessa forma; O interessante seria criar uma função genérica.
             * Tentei normalizar o máximo possível como os dados serão renderizados na tela, para isso tive que utilizar cast.
            */

            /*
             * Lógica para setar o ID STATUS
             * */
            $id_status = null;
            if ($this->_getParam('id_status') != null) {
                $id_status = $this->_getParam('id_status');
            } elseif ($this->_getParam('id_status') == 4 || $this->_getParam('id_status') == '') {
                $id_status = null;
            }

            $this->view->listSalas = array();
            $this->view->idDisciplina = $this->_getParam('id_disciplina', null);
            $this->view->idSalaDeAula = $this->_getParam('id_saladeaula', null);

            if (Ead1_Sessao::getSessaoPerfil()->is_aluno()
                || Ead1_Sessao::getSessaoPerfil()->is_alunoInstitucional()
            ) {
                try {
                    $vwAlunoGradeIntegracaoTO = new VwAlunoGradeIntegracaoTO();
                    $vwAlunoGradeIntegracaoTO->setId_matricula(Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula);
                    $vwAlunoGradeIntegracaoTO
                        ->setId_projetopedagogico(Ead1_Sessao::getSessaoProjetoPedagogico()->id_projetopedagogico);
                    $vwAlunoGradeIntegracaoTO->setId_usuario(Ead1_Sessao::getSessaoUsuario()->id_usuario);
                    $mensageiro = $this->_salaBO->retornarAlunoSalaDeAula($vwAlunoGradeIntegracaoTO);
                    $mensageiro->subtractMensageiro();
                    $this->view->perfil = 'aluno';
                    $auxListSalas = $mensageiro->getMensagem();
                    foreach ($auxListSalas as $dadosModulo) {
                        foreach ($dadosModulo['disciplinas'] as $to) {
                            $listSalas[] = $to;
                        }
                    }
                    $this->view->listSalas = $listSalas;

                } catch (Exception $e) {
                    return $e->getMessage();
                }
            } elseif (Ead1_Sessao::getSessaoPerfil()->is_professor() == true) {


                $where = array(
                    'id_usuario' => Ead1_Sessao::getSessaoUsuario()->id_usuario,
                    'id_entidade' => Ead1_Sessao::getSessaoEntidade()->id_entidade,
                    'id_perfilpedagogico' => Ead1_Sessao::getSessaoPerfil()->id_perfilpedagogico
                );

                (!empty($id_status)) ? $where['id_status'] = $id_status : '';

                $negocio = new \G2\Negocio\VwSaladeAulaIntegracao();
                $resultado = $negocio->getVwSalaDeAulaProfessorIntegracao($where);

                foreach ($resultado as $object) {
                    $listSalas[] = $negocio->entityToTO($object);;
                }

                $this->view->listSalas = $listSalas;

            } elseif (Ead1_Sessao::getSessaoPerfil()->is_coordenadorProjetoPedagogico()
                || Ead1_Sessao::getSessaoPerfil()->is_coordenadorDisciplina()
                || Ead1_Sessao::getSessaoPerfil()->is_observadorInstitucional()
                || Ead1_Sessao::getSessaoPerfil()->is_suporte()
            ) {

                $salaBO = new PortalSalaDeAulaBO();
                $to = new VwSalaDeAulaPerfilProjetoIntegracaoTO();
                $to->setId_usuario(Ead1_Sessao::getSessaoUsuario()->id_usuario);
                $to->setId_entidade(Ead1_Sessao::getSessaoEntidade()->id_entidade);
                $to->setId_perfilpedagogico(Ead1_Sessao::getSessaoPerfil()->id_perfilpedagogico);
                $to->setId_status($id_status);
                $to->setId_projetopedagogico($sala->getId_projetopedagogico());
                $mensageiro = $salaBO->retornarSalaDeAulaCoordenador($to);

                $mensageiro->subtractMensageiro();
                $auxListSalas = $mensageiro->getMensagem();

                foreach ($auxListSalas as $dadosModulo) {
                    foreach ($dadosModulo['projetos'] as $to) {
                        $listSalas[] = $to;
                    }
                }

                $this->view->listSalas = $listSalas;
            }

        } catch (Zend_Exception $e) {
            $this->view->erro = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        }

        $this->_helper->layout()->setLayout('lms_frame');
    }

    /**
     * Action o header do LMS (Portal?)
     */
    public function lmsheaderAction()
    {
        Ead1_Sessao::setSessionProjetoPedagogico(array('id_saladeaula' => $this->_getParam('id_saladeaula')));
        $this->view->dadosDisciplina = $this->_getParam('id_tiposaladeaula');

        if ((Ead1_Sessao::getSessaoPerfil()->is_aluno())
            || (Ead1_Sessao::getSessaoPerfil()->is_alunoInstitucional())
        ):
            $vwAlunoGradeIntegracaoTO = new VwAlunoGradeIntegracaoTO();
            $vwAlunoGradeIntegracaoTO->setId_usuario(Ead1_Sessao::getSessaoUsuario()->id_usuario);
            $vwAlunoGradeIntegracaoTO
                ->setId_projetopedagogico(Ead1_Sessao::getSessaoProjetoPedagogico()->id_projetopedagogico);
            $vwAlunoGradeIntegracaoTO->setId_saladeaula(Ead1_Sessao::getSessaoProjetoPedagogico()->id_saladeaula);
            $vwAlunoGradeIntegracaoTO->setId_matricula(Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula);
            $mensageiro = $this->_salaBO->retornarAlunoSalaDeAulaIntegracao($vwAlunoGradeIntegracaoTO);
            $this->view->dadosSala = $mensageiro->getFirstMensagem();
        endif;

        $this->_helper->layout()->setLayout('lms_frame_header');
    }

    /**
     * Action do Moodle (???)
     */
    public function moodleformAction()
    {
        $this->ajax();
        $sessaoMoodle = new Zend_Session_Namespace('moodle');
        $html = '<body onload="document.formMoodle.submit();">';
        $html .= '<form id="formMoodle" name="formMoodle" ';
        $html .= 'method="post" action="http://www.moodledev.ead1.net/login/index.php">';
        $html .= '<input type="hidden" name="username" value="' . $sessaoMoodle->usuario . '" />';
        $html .= '<input type="hidden" name="password" value="' . $sessaoMoodle->senha . '" />';
        $html .= '</form></body>';
        echo $html;
    }

    public function testeAction()
    {
        Zend_Debug::dump(
            substr("http://www.moodledev.ead1.net/webservice/rest/server.php", 0, -26));
        exit;
    }

    /**
     * Utilizando o parametro url vindo por GET, pega uma url ficticia
     * e busca a sua url verdadeira na base de dados redirecionando o usuário.
     * @author Rafael Bruno <rafaelbruno.ti@gmail.com>
     */
    public function urlAction()
    {
        $this->ajax();
        $mensageiro = new Ead1_Mensageiro();

        if ($this->_getParam('st_urlresumida')) {
            $bo = new UrlResumidaBO();
            $urlResumidaTO = new UrlResumidaTO();
            $urlResumidaTO->setSt_urlresumida($this->_getParam('st_urlresumida'));

            $result = $bo->buscaPorUrlResumida($urlResumidaTO);
            if ($result) {
                $this->_redirect($result->getSt_urlcompleta());
            } else {
                $mensageiro->addMensagem('Url não encontrada!');
                $this->_redirect('/portal/index');
            }
        } else {
            $mensageiro->addMensagem('Url não encontrada!');
            $this->_redirect('/portal/index');
        }
    }

    /**
     * Marca o projeto pedagogico na sessão para pesquisar apenas por ele
     */
    public function setaprojetopesquisaAction()
    {
        $this->ajax();
        $arrayPerfil = Ead1_Sessao::getSessaoPerfil()->toArray();
        $arrayPerfil['id_projetopedagogicopesquisa'] = $this->_getParam('id_projetopedagogico');
        Ead1_Sessao::setSessionPerfil($arrayPerfil);
        if (Ead1_Sessao::getSessaoPerfil()->is_professor()) {
            $perfil = 'professor';
        } else {
            $perfil = 'coordenador';
        }
        echo json_encode(array('retorno' => 'true', 'metodo' => $perfil));
    }

    /**
     * limpa um projeto pedagogico de pesquisa da sessao Ead1_Sessao->getSessaoPerfil()
     */
    public function limpaprojetopesquisaAction()
    {
        $this->ajax();
        $arrayPerfil = Ead1_Sessao::getSessaoPerfil()->toArray();
        $arrayPerfil['id_projetopedagogicopesquisa'] = null;
        Ead1_Sessao::setSessionPerfil($arrayPerfil);
        if (Ead1_Sessao::getSessaoPerfil()->is_professor()) {
            $perfil = 'professor';
        } else {
            $perfil = 'coordenador';
        }
        echo json_encode(array('retorno' => 'true', 'metodo' => $perfil));
    }

    /**
     * Pesquisa salas por projeto pedagogico para cordenadores e outros perfis
     */

    public function pesquisasalascoordenadorprojetoAction()
    {
        try {
            $salaBO = new PortalSalaDeAulaBO();

            if ($this->_getParam('id_projetopedagogico')) {
                $id_projeto = $this->_getParam('id_projetopedagogico');
            } else {
                if (Ead1_Sessao::getSessaoProjetoPedagogico()->id_projetopedagogico) {
                    $id_projeto = Ead1_Sessao::getSessaoProjetoPedagogico()->id_projetopedagogico;
                } else {
                    throw new Exception("Necessário informar id_projetopedagogico.");
                }
            }

            $id_status = ($this->_getParam('id_status') != ''
                ? ($this->_getParam('id_status') == 4 ? null : $this->_getParam('id_status')) : 1);
            $id_categoriasala = $this->getParam('id_categoriasala', null);
            $id_tipodisciplina = $this->getParam('id_tipodisciplina', null);
            $id_tipodisciplina_dif = $this->getParam('id_tipodisciplina_dif', null);
            $id_situacaosala_dif = $this->getParam('id_situacaosala_dif', null);

            if (Ead1_Sessao::getSessaoPerfil()->id_perfilpedagogico !== \G2\Constante\PerfilPedagogico::PROFESSOR) {
                $to = new VwSalaDeAulaPerfilProjetoIntegracaoTO();
                $to->setId_usuario(Ead1_Sessao::getSessaoUsuario()->id_usuario);
                $to->setId_entidade(Ead1_Sessao::getSessaoEntidade()->id_entidade);
                $to->setId_perfilpedagogico(Ead1_Sessao::getSessaoPerfil()->id_perfilpedagogico);
                $to->setId_projetopedagogico($id_projeto);
                $to->setId_status($id_status);
                $to->setId_categoriasala($id_categoriasala);
                $to->setid_tipodisciplina($id_tipodisciplina);


                if ($id_tipodisciplina_dif) {
                    $where_string = 'id_tipodisciplina != ' . $id_tipodisciplina_dif;
                } else {
                    $where_string = false;
                }

                $mensageiro = $salaBO
                    ->retornarSalaDeAulaCoordenador($to, 'dt_abertura', null, null, $where_string);
                $mensageiro->subtractMensageiro();
                $mensageiro = $mensageiro->getMensagem();

                foreach ($mensageiro as &$projetopedagogico) {
                    if (isset($projetopedagogico['projetos']) && $projetopedagogico['projetos']) {
                        foreach ($projetopedagogico['projetos'] as &$saladeaula) {
                            $negocio = new \G2\Negocio\Negocio();
                            $sala = $negocio->find('\G2\Entity\SalaDeAula', $saladeaula->id_saladeaula);
                            $saladeaula->nu_diasextensao = $sala->getNu_diasextensao();
                            $saladeaula->st_encerramentoextensao = '-';

                            // Calcula data de termino, somando os dias de extensao
                            if ($saladeaula->st_encerramento && $saladeaula->st_encerramento != '-') {
                                $dataUsa = implode('-', array_reverse(
                                    explode('/', $saladeaula->st_encerramento)
                                ));
                                $saladeaula->st_encerramentoextensao = date('d/m/Y',
                                    strtotime("+{$saladeaula->nu_diasextensao} days", strtotime($dataUsa)));
                            }
                        }
                    }
                }

                $this->view->listSalas = $mensageiro;

                $this->resposta($mensageiro);
            } else {
                $to = new VwSalaDeAulaProfessorIntegracaoTO();
                $to->setId_usuario(Ead1_Sessao::getSessaoUsuario()->id_usuario);
                $to->setId_entidade(Ead1_Sessao::getSessaoEntidade()->id_entidade);
                $to->setId_perfilpedagogico(Ead1_Sessao::getSessaoPerfil()->id_perfilpedagogico);
                $to->setId_disciplina($id_projeto);
                $to->setId_status($id_status);
                $to->setId_categoriasala($id_categoriasala);
                $to->setid_tipodisciplina($id_tipodisciplina);
                $diff = null;
                if ($id_situacaosala_dif) {
                    $diff['id_situacaosala'] = '!= ' . $id_situacaosala_dif;
                }

                $mensageiro = $salaBO->retornarSalaDeAulaProfessor($to, 'dt_abertura', $diff);

                if ($mensageiro->getTipo() !== Ead1_IMensageiro::SUCESSO) {
                    $this->_helper->json([]);
                }

                $arrResponse = $mensageiro->getFirstMensagem();
                $mensageiro = $arrResponse[$to->getId_disciplina()]["disciplinas"];


                $negocio = new \G2\Negocio\Negocio();
                foreach ($mensageiro as &$saladeaula) {
                    /** @var \G2\Entity\SalaDeAula $sala */
                    $sala = $negocio->find(\G2\Entity\SalaDeAula::class, $saladeaula->id_saladeaula);
                    $saladeaula->nu_diasextensao = $sala->getNu_diasextensao();

                    $saladeaula->st_encerramentoextensao = '-';
                    $saladeaula->nu_interacoes = "-";


                    // Calcula data de termino, somando os dias de extensao
                    if ($saladeaula->st_encerramento && $saladeaula->st_encerramento != '-') {
                        /** @var Zend_date $dataTeste */
                        $dataTeste = $negocio
                            ->somarData($saladeaula->st_encerramento, $saladeaula->nu_diasextensao);
                        $saladeaula->st_encerramentoextensao = $dataTeste->toString("dd/MM/yyyy");
                    }

                    /*
                     * se o perfil for de Professor(Tutor) e a sala estiver aberta,
                     * recupera pelo webservice com o Moodle, as interações pendentes
                    */
                    if (Ead1_Sessao::getSessaoPerfil()->id_perfilpedagogico
                        === \G2\Constante\PerfilPedagogico::PROFESSOR
                        && $saladeaula->id_status === 1
                        && $sala->getid_entidadeintegracao()
                    ) {

                        //Passa os parametros para conectar no moodle
                        $moodle = new MoodleUnyleyaWebServices(
                            $to->getId_entidade(),
                            $sala->getid_entidadeintegracao()->getId_entidadeintegracao()
                        );

                        $mensageiroMoodle = $moodle->listRecentInteractions(
                            (int)$to->getId_usuario(),
                            $saladeaula->id_saladeaula
                        );

                        if ($mensageiroMoodle->getTipo() === Ead1_IMensageiro::SUCESSO) {
                            $nu_interacoes = 0;
                            if (!empty($mensageiroMoodle->getMensagem())) {
                                //conta as recentes interações não lidas
                                foreach ($mensageiroMoodle->getMensagem() as $recentInteractions) {
                                    if (isset($recentInteractions->unread) && $recentInteractions->unread) {
                                        $nu_interacoes += $recentInteractions->unread;
                                    }
                                }
                            }

                            $saladeaula->nu_interacoes = $nu_interacoes;

                        }
                    }
                }


                $this->_helper->json($mensageiro);
            }

        } catch (Zend_Exception $e) {
            $this->view->erro = $mensageiro;
        }
    }

    public function processarTutorialAction()
    {
        try {
            $this->_helper->viewRenderer->setNoRender(true);

            $negocio = new \G2\Negocio\UsuarioPerfilEntidade();
            $result = $negocio->saveTutorialPortal(true);

            if ($result) {
                $this->getResponse()->setHttpResponseCode(200);
            } else {
                $this->getResponse()->setHttpResponseCode(204);
                $this->_helper->json('Nenhum usuário perfil entidade encontrado!');
            }

        } catch (Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json($e->getMessage());
        }
    }

    public function retornarTutorialAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $entidadeUsuarioTutorial = new \G2\Negocio\Entidade();

        $result = $entidadeUsuarioTutorial->retornaEntidadeUsuario(array(
                'id_usuario' => Ead1_Sessao::getSessaoUsuario()->id_usuario,
                'id_entidade' => Ead1_Sessao::getSessaoEntidade()->id_entidade,
                'id_perfil' => Ead1_Sessao::getSessaoPerfil()->id_perfil
            )
        );

        $response = false;
        if ($result != false
            && $result['bl_tutorialentidade'] == true
            && ($result['bl_tutorialaluno'] == null || $result['bl_tutorialaluno'] == false)
        ) {
            $response = true;
        }

        echo Zend_Json::encode($response);
    }

    /**
     * Action para ação de recuperar a senha do usuario
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     * @since 2014-08-14
     */
    public function validarecuperacaosenhaAction()
    {
        $this->_helper->viewRenderer->setNoRender(); //Não rederiza a view
        try {
            if ($this->getRequest()->isPost()) {
                $params = $this->getAllParams(); //Recupera os parametros passado
                //instancia a TO e a BO do portal
                $vwPessoaTO = new VwPessoaTO();
                $loginBO = new PortalLoginBO();
                //Remove as chaves de controller, action e modulo do zend
                unset($params['controller'], $params['action'], $params['module']);

                if (empty($params['email'])) {
                    throw new Exception("E-mail é obrigatório");
                }

                if (empty($params['login']) && empty($params['cpf'])) {
                    throw new Exception("Login ou CPF devem ser informados.");
                }

                //Seta os parametros na to
                $vwPessoaTO->setSt_email($params['email']);
                $vwPessoaTO->setSt_login(isset($params['login']) ? $params['login'] : null);
                $vwPessoaTO->setSt_cpf(isset($params['cpf']) ? $params['cpf'] : null);

                //chama o método do recuperar senha da BO
                $return = $loginBO->recuperarSenha($vwPessoaTO);
                $this->_helper->json($return->toArrayAll()); //retorna o json com o retorno do mensageiro do método
            }
        } catch (Exception $e) {
            return $this->getResponse()->setHttpResponseCode(500)
                ->appendBody($e->getMessage());
        }

    }

    /**
     * Action que carrega o iframe com a URL que redireciona para
     * os tutoriais do Moodle (cadastrados no banco).
     * @author Marcus Pereira <marcus.pereira@unyleya.com.br>
     */
    public function tutoriaisAction()
    {
        $idEntidade = Ead1_Sessao::getSessaoEntidade()->id_entidade;

        $negocio = new \G2\Negocio\ConfiguracaoEntidade();
        $configEntidade = $negocio->findConfiguracaoEntidade($idEntidade);

        if (isset($configEntidade['st_informacoestecnicas'])) {
            $this->view->tutorialUrl = $configEntidade['st_informacoestecnicas'];
        }
        $this->_helper->layout->setLayout('layout');
    }
}

<?php

/**
 * Controller de Minha Pasta do Portal do Aluno
 * @author Rafael Rocha
 */
class Portal_MinhaPastaController extends PortalBase
{

    /** @var PortalMinhaPastaBO */
    private $_bo;

    public function preDispatch()
    {
        parent::preDispatch();
    }

    /**
     * @see PortalController::init()
     */
    public function init()
    {
        parent::init();
        $this->_bo = new PortalMinhaPastaBO();
        $this->_textoSistemaBO = new TextoSistemaBO();
    }

    public function indexAction()
    {
        $this->_helper->layout()->setLayout('layout');
        $minhaPastaNegocio = new \G2\Negocio\MinhaPasta();

        $usuario = Ead1_Sessao::getSessaoUsuario()->id_usuario;
        $vw_matricula = new \G2\Negocio\Matricula();

        $venda = $vw_matricula->getRepository('\G2\Entity\Matricula')->retornaVendaMatricula(array(
                'id_matricula' => Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula)
        );

        //se não achar a venda pesquisa na tb_vendaaditivo
        if(!$venda){
            $negocio = new \G2\Negocio\Negocio();
            $vendaaditivo = $negocio->findOneBy('\G2\Entity\VendaAditivo',array(
                'id_matricula' => Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula));
            if($vendaaditivo instanceof G2\Entity\VendaAditivo){
                $venda['id_venda'] = $vendaaditivo->getId_venda()->getId_venda();
            }
        }

        //Lista arquivos que est�o salvos no banco
        $listaArquivosBanco = $minhaPastaNegocio->retornaMinhaPasta(array(
            'id_situacao' => G2\Constante\Situacao::TB_MINHAPASTA_ATIVO,
            'id_usuario' => $usuario,
            'id_venda' => $venda['id_venda']
        ));

        //Lista arquivos que est�o salvos somente no servidor
        $listaArquivosServidor = $this->_bo->retornarAquivosMinhaPasta(null, $listaArquivosBanco);

        $this->view->listaArquivosBanco = $listaArquivosBanco;
        $this->view->listaArquivosServidor = $listaArquivosServidor;
    }
}

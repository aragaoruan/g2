<?php

/**
 * Controller de Index (Portal) do Portal do Aluno
 * @author eduardoromao
 */
class Portal_UsuarioController extends PortalBase
{

    private $_vwpessoaTO;
    private $_pessoaEnderecoTO;
    private $_pessoaTelefoneTO;
    private $_pessoaRedeTO;
    private $_pessoaEmailTO;
    private $_pessoaBO;

    /**
     * @see PortalController::init()
     */
    public function init()
    {
        parent::init();
        $this->_vwpessoaTO = new VwPessoaTO();
        $this->_pessoaBO = new PessoaBO();
        $this->_pessoaEnderecoTO = new VwPessoaEnderecoTO();
        $this->_pessoaTelefoneTO = new VwPessoaTelefoneTO();
        $this->_pessoaRedeTO = new VwPessoaRedeSocialTO();
        $this->_pessoaEmailTO = new VwPessoaEmailTO();

        if ($this->_helper->FlashMessenger->hasMessages()) {
            $this->view->messages = $this->_helper->FlashMessenger->getMessages();
        }
    }

    public function meusdadosAction()
    {
        //Grava log de acesso
        $logTO = new LogAcessoTO();
        $logTO->setId_funcionalidade(LogAcessoTO::MEUS_DADOS);
        $logRO = new LogRO();
        $logRO->gravaLog($logTO);

        $this->_helper->layout()->setLayout('layout');

        $this->_vwpessoaTO->setId_usuario(Ead1_Sessao::getSessaoUsuario()->id_usuario);
        $this->_vwpessoaTO->setId_entidade(Ead1_Sessao::getSessaoEntidade()->id_entidade);
        $this->view->pessoa = $this->_vwpessoaTO->fetch(false, true, true);

        $this->_pessoaEnderecoTO->setId_usuario(Ead1_Sessao::getSessaoUsuario()->id_usuario);
        $this->_pessoaEnderecoTO->setId_entidade(Ead1_Sessao::getSessaoEntidade()->id_entidade);
        $this->_pessoaEnderecoTO->setBl_ativo(true);
        $this->_pessoaEnderecoTO->setBl_padrao(true);
        $this->view->endereco = $this->_pessoaEnderecoTO->fetch(false, false, false);

        $this->_pessoaTelefoneTO->setId_usuario(Ead1_Sessao::getSessaoUsuario()->id_usuario);
        $this->_pessoaTelefoneTO->setId_entidade(Ead1_Sessao::getSessaoEntidade()->id_entidade);
        $this->_pessoaTelefoneTO->setBl_padrao(true);
        $this->view->telefone = $this->_pessoaTelefoneTO->fetch(false, false, false);

        $this->_pessoaRedeTO->setId_usuario(Ead1_Sessao::getSessaoUsuario()->id_usuario);
        $this->_pessoaRedeTO->setId_entidade(Ead1_Sessao::getSessaoEntidade()->id_entidade);
        $this->view->rede = $this->_pessoaRedeTO->fetch(false, false, false);

        $this->_pessoaEmailTO->setId_usuario(Ead1_Sessao::getSessaoUsuario()->id_usuario);
        $this->_pessoaEmailTO->setId_entidade(Ead1_Sessao::getSessaoEntidade()->id_entidade);
        $this->_pessoaEmailTO->setBl_ativo(true);
        $this->_pessoaEmailTO->setBl_padrao(true);
        $this->view->email = $this->_pessoaEmailTO->fetch(false, false, false);
    }

    public function trocarminhasenhaAction()
    {
        $mensageiro = new Ead1_Mensageiro();

        try {
            $usuarioTO = new UsuarioTO();
            $usuarioTO->setId_usuario(Ead1_Sessao::getSessaoUsuario()->id_usuario);
            $usuarioTO->fetch(false, true, true);

            $novosUsuarioSenha = new stdClass;
            $novosUsuarioSenha->st_senha_nova = trim($this->_getParam('st_senha_nova'));
            $novosUsuarioSenha->st_senha_nova_confirma = trim($this->_getParam('st_senha_nova_confirma'));

            if ($novosUsuarioSenha->st_senha_nova != $novosUsuarioSenha->st_senha_nova_confirma) {
                $this->_helper->flashMessenger->addMessage("
            <div class='info-box yellow uny ocorrencia-box'>
                    <i class='icon-warning-sign'></i>
                <div class='msg'>
                As novas senhas são diferentes!
                </div>
                <a href='javascript:void(0)' class='info-box-remove'><i class='icon-remove'></i></a>
            </div>");
                $this->_redirect('/portal/usuario/meusdados');
            }

            try {
                $retorno = $this->_pessoaBO->alterarUsuarioSenha($usuarioTO, $novosUsuarioSenha, Ead1_Sessao::getSessaoEntidade()->id_entidade);
                $msg = $retorno->mensagem[0];
                $this->_helper->flashMessenger->addMessage("
            <div class='info-box green uny ocorrencia-box'>
                    <i class='icon-ok'></i>
                <div class='msg'>$msg</div><a href='javascript:void(0)' class='info-box-remove'><i class='icon-remove'></i></a></div>");
            } catch (Exception $e) {
                $this->_helper->flashMessenger->addMessage("
            <div class='info-box red uny ocorrencia-box'>
                    <i class='icon-warning-sign'></i>
                <div class='msg'>
                Erro ao alterar a sua senha, por favor, entre em contato com o administrador.
                </div>
                <a href='javascript:void(0)' class='info-box-remove'><i class='icon-remove'></i></a>
            </div>");
            }

            $this->_redirect('/portal/usuario/meusdados');

        } catch (Zend_Exception $e) {
            $mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
            echo Zend_Json::encode($mensageiro);
        }
    }

    public function redefinirSenhaAction()
    {

        $q = $this->getRequest()->getParam('q', false);
        $e = $this->getRequest()->getParam('e', false);

        $id_usuario = base64_decode($q);
        $id_entidade = base64_decode($e);
        try {

            $login = new PortalLoginBO();
            $mensageiro = $login->retornaDadosRedefinicaoSenha($id_usuario, $id_entidade);
            if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Exception($mensageiro->getFirstMensagem());
            }

            $arTO = $mensageiro->getMensagem();

            $this->view->usuario = $arTO[0];
            $this->view->entidade = $arTO[1];
            $this->view->q = $q;
            $this->view->e = $e;

        } catch (Exception $e) {

            die($e->getMessage());

        }

    }

    public function salvarRedefinicaoSenhaAction()
    {
        $q = $this->getRequest()->getParam('q', false);
        $e = $this->getRequest()->getParam('e', false);
        $st_senha_nova = $this->getRequest()->getParam('st_senha_nova', false);
        $st_senha_nova_confirma = $this->getRequest()->getParam('st_senha_nova_confirma', false);
        $id_usuario = base64_decode($q);
        $id_entidade = base64_decode($e);

        $login = new PortalLoginBO();
        $mensageiro = $login->salvarRedefinicaoSenha($id_usuario, $id_entidade, $st_senha_nova, $st_senha_nova_confirma);
        $this->myJson($mensageiro);

    }

    public function uploadFotoAction()
    {

        if ($this->getRequest()->isPost()) {
            $targ_w = $targ_h = 150;
            $quality = 90;
            $uploadImg = null;

            $src = $_FILES['imagem-aluno']['tmp_name'];
            $name_img = $_FILES['imagem-aluno']['name'];

            $ext = pathinfo($name_img, PATHINFO_EXTENSION);
            $dst_r = ImageCreateTrueColor($targ_w, $targ_h);

            if (isset($_POST['x']) && trim($_POST['x']) && isset($_POST['y']) && trim($_POST['y'])) {
                if (($ext == 'jpg' || $ext == 'jpeg') && mime_content_type($src) == 'image/jpeg') {
                    $img_r = imagecreatefromjpeg($src);
                    imagecopyresampled($dst_r, $img_r, 0, 0, $_POST['x'], $_POST['y'], $targ_w, $targ_h, $_POST['w'], $_POST['h']);
                    $uploadImg = imagejpeg($dst_r, 'upload/usuario/' . Ead1_Sessao::getSessaoUsuario()->st_login . '.avatar.' . $ext, $quality);
                } elseif ($ext == 'png' && mime_content_type($src) == 'image/png') {
                    $img_r = imagecreatefrompng($src);
                    imagecopyresampled($dst_r, $img_r, 0, 0, $_POST['x'], $_POST['y'], $targ_w, $targ_h, $_POST['w'], $_POST['h']);
                    $uploadImg = imagepng($dst_r, 'upload/usuario/' . Ead1_Sessao::getSessaoUsuario()->st_login . '.avatar.png');
                }
            }

            if ($uploadImg) {
                $pessoa = new PessoaTO();
                $pessoa->setId_usuario(Ead1_Sessao::getSessaoUsuario()->id_usuario);
                $pessoa->setSt_urlavatar('/upload/usuario/' . Ead1_Sessao::getSessaoUsuario()->st_login . '.avatar.' . $ext);
                $pessoa->setBl_ativo(true);

                $usuario = new UsuarioTO();
                $usuario->setId_usuario((Ead1_Sessao::getSessaoUsuario()->id_usuario));
                $edicao = $this->_pessoaBO->editarPessoa($usuario, $pessoa);

                if ($edicao->tipo == 1) {
                    $arrayUrlAvatar = array('st_urlavatar' => $pessoa->getSt_urlavatar());
                    Ead1_Sessao::setSessionUsuario($arrayUrlAvatar);

                    $this->_helper->flashMessenger->addMessage("
                    <div class='info-box green uny ocorrencia-box'>
                            <i class='icon-ok'></i>
                        <div class='msg'>
                        Parabéns, sua imagem foi alterada com sucesso.
                        </div>
                        <a href='javascript:void(0)' class='info-box-remove'><i class='icon-remove'></i></a>
                    </div>"
                    );
                } else {
                    $this->_helper->flashMessenger->addMessage("
                    <div class='info-box red uny ocorrencia-box'>
                            <i class='icon-warning-sign'></i>
                        <div class='msg'>
                        Erro ao salvar a nova imagem, por favor entre em contato com o administrador do sistema.
                        </div>
                        <a href='javascript:void(0)' class='info-box-remove'><i class='icon-remove'></i></a>
                    </div>");
                }
            } else {
                $this->_helper->flashMessenger->addMessage("
                    <div class='info-box red uny ocorrencia-box'>
                            <i class='icon-warning-sign'></i>
                        <div class='msg'>
                        Erro ao processar imagem, por favor verifique a extensão e tente novamente.
                        </div>
                        <a href='javascript:void(0)' class='info-box-remove'><i class='icon-remove'></i></a>
                    </div>");
            }
        }
    }
}


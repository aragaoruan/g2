<?php

/**
 * Grade de disciplinas para renovação de matrícula
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class Portal_GradeDisciplinaController extends PortalBase
{

    /**
     * @var G2\Negocio\GradeDisciplina
     */
    private $_negocio;

    /**
     * @var \G2\Negocio\VendaGraduacao
     */
    private $negocio;

    public function init()
    {
        parent::init();
        $this->_negocio = new \G2\Negocio\GradeDisciplina();
        $this->negocio = new \G2\Negocio\VendaGraduacao();
        $this->_helper->layout()->setLayout('layout');
    }

    public function indexAction()
    {
        try {


        } catch (Zend_Exception $e) {
        }
    }

    /**
     * Verifica se a mensagem de renovação da matricula é mostrada ao aluno
     */
    public function renovacaoAction()
    {
        $this->view->mostrar = false;

        $response = $this->_negocio->verificaMensagemRenovacao();

        if (!empty($response['dt_limiterenovacao'])) {
            $dt_limiterenovacao = new \DateTime($response['dt_limiterenovacao']);

            if ($dt_limiterenovacao >= new \DateTime()) {
                $this->view->mostrar = true;
            }
        }
    }

    /**
     * Retorna as salas por turma
     */
    public function getSalasByTurmaAction()
    {
        $results = array();

        $id_produto = $this->getParam('id_produto');
        $id_usuario = $this->getParam('id_usuario');

        $params = $this->limpaGetAllParams($this->getAllParams());
        unset($params['id_produto'], $params['id_usuario']);

        if ($id_produto && $id_usuario) {
            $results = $this->negocio->findSalasDisponiveisOfertaTurma($id_produto, $id_usuario, $params, true);
        }

        $this->_helper->json($results);
    }

    /**
     * Retorna as informações primárias para carregar a tela
     * @throws Zend_Exception
     */

    public function retornaInformacoesAction()
    {
        try {
            $ng_matricula = new \G2\Negocio\Matricula();
            $ng_venda = new \G2\Negocio\Venda();
            $ng_vendagraduacao = new \G2\Negocio\VendaGraduacao();
            $ng_campanhacomercial = new \G2\Negocio\CampanhaComercial();

            /** @var \G2\Entity\VwMatricula $matricula */
            $matricula = $ng_matricula->consultaVwMatricula(array('id_matricula' => $ng_matricula->sessao->id_matricula))[0];

            /** @var \G2\Repository\Matricula $rep */
            $rep = $ng_matricula->getRepository('\G2\Entity\Matricula');
            $venda = $rep->retornaVendaMatricula(array(
                'bl_renovacao' => true,
                'id_matricula' => $ng_matricula->sessao->id_matricula
            ));

            //busca venda produto para incluir o id_campanha comercial
            /** @var \G2\Entity\VendaProduto $vendaProduto */
            $vendaProduto = $ng_venda->findOneBy('G2\Entity\VendaProduto', array('id_venda' => $venda['id_venda']));

            /** @var \G2\Entity\VwVendaLancamento $lancamento */
            $lancamento = $ng_venda->findOneBy('\G2\Entity\VwVendaLancamento', array('id_venda' => $venda['id_venda']));

            $informacoes = $ng_vendagraduacao->retornaInformacoesValores(array(
                'id_venda'                => $venda['id_venda'],
                'id_usuario'              => $matricula->getId_usuario(),
                'id_matricula'            => $matricula->getId_matricula(),
                'id_produto'              => $matricula->getId_produto(),
                'id_campanhacomercial'    => $vendaProduto->getId_campanhacomercial()
                    ? $vendaProduto->getId_campanhacomercial()->getId_campanhacomercial()
                    : null,
                'id_campanhapontualidade' => $venda['id_campanhapontualidade']
            ));

            if (!empty($informacoes['error'])) {
                throw new \Zend_Exception($informacoes['error']);
            }

            $arr = array(
                'id_venda' => $venda['id_venda'],
                'id_matricula' => $matricula->getId_matricula(),
                'id_usuario' => $matricula->getId_usuario(),
                'id_turma' => $matricula->getId_turma(),
                'st_nomecompleto' => $matricula->getSt_nomecompleto(),
                'st_turma' => $matricula->getSt_turma(),
                'id_produto' => $matricula->getId_produto(),
                'id_projetopedagogico' => $matricula->getId_projetopedagogico(),
                'st_curso' => $vendaProduto->getId_produto()->getSt_produto(),
                'id_campanhacomercial' => $vendaProduto->getId_campanhacomercial() ? $vendaProduto->getId_campanhacomercial()->getId_campanhacomercial() : null,
                'id_campanhapontualidade' => $venda['id_campanhapontualidade'],
                'st_campanhapontualidade' => $venda['id_campanhapontualidade']
                    ? $ng_campanhacomercial->findCampanha($venda['id_campanhapontualidade'])->getSt_campanhacomercial()
                    : 'Nenhuma campanha selecionada.',
                'id_meiopagamento' => $lancamento ? $lancamento->getId_meiopagamento() : null
            );

            //Insere no arr as informações de valor.
            foreach ($informacoes as $key => $value) {
                $arr[$key] = $value;
            };

            //Calcula os valores referentes em disciplinas
            if ($arr['disciplinas']) {
                $arrDisciplinas = array();
                foreach ($arr['disciplinas'] as $value) {
                    array_push($arrDisciplinas, $value['id_disciplina']);
                }
                $contrat = $this->negocio->calculaValoresContrato($arrDisciplinas, array(
                    'id_produto' => $arr['id_produto'],
                    'id_campanhacomercial' => $arr['id_campanhacomercial'],
                    'id_campanhapontualidade' => $arr['id_campanhapontualidade']
                ));

                if ($contrat->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                    foreach ($contrat->getFirstMensagem() as $key => $value) {
                        $arr[$key] = $value;
                    };
                }
            }

            $this->_helper->json($arr);
        } catch (Zend_Exception $e) {
            throw new \Zend_Exception('Erro ao retorna informações de valor. ' . $e->getMessage());
        }
    }

    /**
     * Calcula os valores do contrato de acordo com as disciplinas selecionadas.
     * @throws Zend_Controller_Response_Exception
     */

    public function calculaValoresAction()
    {
        $mensageiro = array();

        $disciplinas = $this->getParam('id_disciplina');
        $params = $this->limpaGetAllParams($this->getAllParams());
        unset($params['id_disciplina']);
        if ($disciplinas) {
            $mensageiro = $this->negocio->calculaValoresContrato($disciplinas, $params);

            if ($mensageiro->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                $mensageiro = $mensageiro->getFirstMensagem();
            } else {
                $this->getResponse()->setHttpResponseCode(400);
            }
        }
        $this->_helper->json($mensageiro);
    }

    /**
     * Retorna as salas padrões que devem ser selecionadas no inicio da grade
     * @throws Zend_Exception
     */
    public function retornaSalasPadraoAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->_negocio->retornaSalasMarcadas($params);
        $arr = array();
        foreach ($result as $value) {
            $variavel = $this->_negocio->toArrayEntity($value);
            $variavel['id_saladeaula'] = $variavel['id_saladeaula']['id_saladeaula'];
            array_push($arr, $variavel);
        }
        $this->_helper->json($arr);
    }

    /**
     * Salva a grade
     * @throws Zend_Exception
     */
    public function salvarGradeAction()
    {
        $params = $this->limpaGetAllParams($this->getAllParams());
        $result = $this->_negocio->saveGrade($params, true);
        $this->_helper->json($result);
    }
}

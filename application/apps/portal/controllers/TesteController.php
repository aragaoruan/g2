<?php

class Portal_TesteController extends PortalBase
{

    public function indexAction()
    {
        Zend_Debug::dump(Ead1_Sessao::getSessionMensagem());
        die;
    }


    /**
     * Action de Autenticação de Login externa
     */
    public function autenticacaoExternaAction() {
        try {

            $vwUsuarioPerfilPTO = new VwUsuarioPerfilPedagogicoTO();
            $vwUsuarioPerfilPTO->setSt_login($this->getRequest()->getParam('nomusuario'));
            $vwUsuarioPerfilPTO->setSt_senha($this->getRequest()->getParam('dessenha'));
            $vwUsuarioPerfilPTO->setSt_email($this->getRequest()->getParam('nomusuario'));

            \Zend_Debug::dump($vwUsuarioPerfilPTO, __CLASS__ . '(' . __LINE__ . ')');
            $mensageiro = $this->_bo->validateUserPass($vwUsuarioPerfilPTO);
            \Zend_Debug::dump($mensageiro, __CLASS__ . '(' . __LINE__ . ')');
            exit;
            if($mensageiro->getTipo()!=\Ead1_IMensageiro::SUCESSO)
                throw new Zend_Exception($mensageiro->getFirstMensagem());


            $mensageiro->subtractMensageiro();
            $this->redirect($this->baseUrlModulo . '/' . ($mensageiro->getCodigo() ? $mensageiro->getCodigo() : "login/selecionaperfil"));
        } catch (Zend_Exception $e) {
            if ($this->hasParam('url_retorno')) {
                $urlRetorno = $this->_getParam('url_retorno') . "?mensagem_erro=" . $mensageiro->getFirstMensagem();
                $this->redirect($urlRetorno);
            } else {
                $this->_helper->flashMessenger->addMessage($mensageiro->getFirstMensagem());
                $this->redirect('/portal');
            }
        }
    }


}
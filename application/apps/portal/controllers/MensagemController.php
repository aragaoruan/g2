<?php

/**
 * Controller de Index (Portal) do Portal do Aluno
 * @author Felipe Pastor
 */
class Portal_MensagemController extends PortalBase
{

    /** @var \PortalMensagemBO */
    private $msgBO;

    public function preDispatch()
    {
        parent::preDispatch();
    }

    /**
     * @see PortalController::init()
     */
    public function init()
    {
        $this->msgBO = new PortalMensagemBO();
        $this->_helper->layout()->setLayout('layout');
        parent::init();
    }

    /**
     * Action inicial da Controller Index
     */
    public function indexAction()
    {

    }

    public function alunoAction()
    {
        $arg = $this->getAllParams();
        if (isset($arg['json']) && $arg['json'] = true) {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender(TRUE);
            echo Zend_Json_Encoder::encode($this->msgBO->retornarMensagemAluno($arg));
        } else {
            $this->view->mensagens = $this->msgBO->retornarMensagemAluno($arg);
        }
    }

}


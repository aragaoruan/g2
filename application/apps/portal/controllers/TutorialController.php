<?php

/**
 * Controller de Index (Portal) do Portal do Aluno
 * @author Felipe Pastor
 */
class Portal_MensagemController extends PortalBase
{

    public function preDispatch()
    {
        parent::preDispatch();
    }

    /**
     * @see PortalController::init()
     */
    public function init()
    {

        parent::init();
    }

    /**
     * Action inicial da Controller Index
     */
    public function indexAction()
    {

    }

    public function alunoTutorialAction()
    {

    }
}


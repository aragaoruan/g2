<?php

const ID_PROMOCOES_PORTAL = 808;
const ID_MENU_SERVICO_ATENCAO = 314;
const ID_EVOLUCAO_ALERTA_PORTAL = 30;

/**
 * Classe de controller para menus
 * @author edermariano
 *
 */
class Portal_MenuController extends PortalBase
{

    /** @var PortalMenuBO $_bo */
    private $_bo;

    // TextoSistema genérico de imposto de renda (cadastrado na entidade 14).
    const ID_TEXTO_IMPOSTO_DE_RENDA_GENERICO = 175;

    /**
     * @see PortalController::init()
     */
    public function init()
    {
        parent::init();
        $this->_bo = new PortalMenuBO();
    }

    /**
     * Navegação principal do sistema
     */
    public function navAction()
    {
        try {
            $arNav = array();
            $this->view->erro = null;
            $this->view->dados = array();
            $mensageiro = $this->_bo->retornarMenuTopo();
            $mensageiro->subtractMensageiro();
            $arNav['dados'] = $mensageiro->getMensagem();
            $arNav['layout'] = Ead1_Sessao::getSessaoEntidade()->st_layout;
            $this->view->dados = $mensageiro->getMensagem();
            $mensageiro->setMensageiro($arNav, Ead1_IMensageiro::SUCESSO);

            $this->view->conteudo = $this->menuDinamicoRecursivo($mensageiro->mensagem['dados']);

        } catch (Zend_Exception $e) {
            $this->view->erro = $mensageiro;
        }
    }

    public function menuDinamicoRecursivo($dados)
    {
        //Montando o menu
        $html = '';
        foreach ($dados as $itemMenu) {

            if ($itemMenu['filhos'] == null) {
                $classe = "pai-sem-filho";
            } else {
                $classe = '';
            }
            $html .= "<li class='" . $classe . "' id='id_funcionalidadenav-" . $itemMenu['id'] . "'>";
            if ($itemMenu['link'] && $itemMenu['link'] != "#") {
                if ($itemMenu['target'] === '_blank') {
                    $target = '_blank';
                } else {
                    $target = '_self';
                }
                $html .= "<a href='/portal" . $itemMenu['link'] . "' target='" . $target .
                    "' data-change='" . $itemMenu['type'] . "'>" .
                    $itemMenu['texto'];

                if ($itemMenu['id'] == ID_PROMOCOES_PORTAL) {

                    $idEntidade = \Ead1_Sessao::getSessaoGeral()->id_entidade;//pega o id da entidade

                    $params = array();

                    $params['bl_vigente'] = 1;
                    $params['bl_ativo'] = 1;
                    $params['id_situacao'] = \G2\Constante\Situacao::TB_CAMPANHACOMERCIAL_ATIVO;
                    $params['bl_portaldoaluno'] = $this->_getParam('bl_portaldoaluno', 1);
                    $params['id_matricula'] = \Ead1_Sessao::getSessaoGeral()->id_matricula;
                    $params['bl_visualizado'] = 0;

                    $negocio = new \G2\Negocio\CampanhaComercial();
                    $promocoes = $negocio->retornarVwCampanhaComercial($params, $idEntidade, false);

                    if (!empty($promocoes)) {
                        $html .= '<span class="badge badge-menu badge-promocoes">' . count($promocoes) . '</span>';
                    }
                }
                // Coloca um aviso caso o exista alguma ocorrência com evolução: "aguardando interessado".
                if ($itemMenu['id'] == ID_MENU_SERVICO_ATENCAO) {
                    $ocorrencias = new PortalCAtencaoBO();
                    $ocorrenciaTO = new VwOcorrenciaTO();

                    $ocorrenciaTO->setId_tipoocorrencia(AssuntoCoTO::TIPO_OCORRENCIA_TUTOR);
                    $ocorrenciaTO->setId_entidade($ocorrencias->sessao->id_entidade);

                    if (($ocorrencias->sessao->id_perfilpedagogico == PerfilPedagogicoTO::ALUNO)
                        || ($ocorrencias->sessao->id_perfilpedagogico == PerfilPedagogicoTO::ALUNO_INSTITUCIONAL)
                    ) {
                        $ocorrenciaTO->setId_matricula($ocorrencias->sessao->id_matricula);
                        $ocorrenciaTO->setId_tipoocorrencia(AssuntoCoTO::TIPO_OCORRENCIA_ALUNO);
                    }
                    $ocorrencias = $ocorrencias->retornarOcorrencias($ocorrenciaTO);
                    $aviso_ocorrencia = false;

                    if (is_object($ocorrencias->getMensagem()[0])) {
                        foreach ($ocorrencias->getMensagem() as $oc) {
                            if ($oc->getId_evolucao() == ID_EVOLUCAO_ALERTA_PORTAL) {
                                $aviso_ocorrencia = true;
                            }
                        }
                    }
                    $aviso_ocorrencia ? $html .= '<span style="font-size: 16px;color:#FF0000;position: inherit;left:140px;background-color: transparent" 
                                                    class="icon-exclamation-sign badge-menu"></span>' : '';
                }
            } else {
                $html .= "<a href='#'>" . $itemMenu['texto'] . "</a>";
            }

            if ($itemMenu['filhos'] != null) {
                $html .= "<ul class='filho'>";
                $html .= $this->menuDinamicoRecursivo($itemMenu['filhos']);
                $html .= "</ul>";
            }
            $html .= "</li>";

        }
        return $html;
    }

    public function manualDoAlunoAction()
    {
        $negocio = new G2\Negocio\ConfiguracaoEntidade();
        $entity = $negocio->findConfiguracaoEntidade($negocio->getId_entidade(), false);

        if ($entity->getId_manualaluno()) {
            $this->redirect(\Ead1_Ambiente::geral()->st_url_gestor2 . '/upload/manual/' . $entity->getId_manualaluno()->getSt_upload());
        } else {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout()->setLayout('layout');
            $this->view->error = "Não foi possível localizar configurações para o manual do aluno, entre em contato com {$negocio->sessao->st_nomeentidade}.";
            echo $this->view->render('/menu/tabela-preco.phtml');
        }
    }

    public function tabelaPrecoAction()
    {
        $negocio = new G2\Negocio\ConfiguracaoEntidade();
        $entity = $negocio->findConfiguracaoEntidade($negocio->getId_entidade(), false);

        if ($entity->getId_tabelapreco()) {
            $this->redirect('/upload/' . $negocio->getId_entidade() . '/' . $entity->getId_tabelapreco()->getSt_upload());
        } else {
            $this->_helper->layout()->setLayout('layout');
            $this->view->error = "Não foi possível localizar configurações para tabela de preço, entre em contato com {$negocio->sessao->st_nomeentidade}.";
        }
    }

    public function gerarImpostoRendaAction()
    {

        $erro = '<div class="container cabecalho prepend-top erro span-24" >';
        try {

            if (!Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula) {
                throw new Exception("Não foi possível localizar o código da matricula.");
            }

            /* Busca a Entidade Financeira, necessária para se obter o id_textoimpostorenda,
            que é o id_textosistema correspondente. */
            $EntidadeFinBO = new EntidadeFinanceiroTO();
            $EntidadeFinBO->setId_entidade(Ead1_Sessao::getSessaoEntidade()->id_entidade);
            $EntidadeFinBO->fetch(false, true, true);

            // Busca o TextoSistema baseado no id obtido no cadastro da entidade financeira.
            $bo = new TextoSistemaBO();
            $textoSistemaTO = new TextoSistemaTO();

            $textoSistemaTO->setId_textosistema(
                $EntidadeFinBO->getId_textoimpostorenda()
                    ? $EntidadeFinBO->getId_textoimpostorenda()
                    : self::ID_TEXTO_IMPOSTO_DE_RENDA_GENERICO
            );

            $textoSistemaTO->fetch(true, true, true);

            if (!$textoSistemaTO->getSt_texto()) {
                throw new Exception("Texto sistema não localizado.");
            }

            $vwGerarDeclaracaoImpostoRendaTO = new VwGerarDeclaracaoImpostoRendaTO();
            $vwGerarDeclaracaoImpostoRendaTO->setId_matricula(Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula);
            $vwGerarDeclaracaoImpostoRendaTO->fetch(false, true, true);

            if (!$vwGerarDeclaracaoImpostoRendaTO->getId_venda()) {
                throw new Exception("Não foi possível recuperar o código da venda relacionada a matrícula.");
            }

            $params['id_venda'] = $vwGerarDeclaracaoImpostoRendaTO->getId_venda();

            if ($vwGerarDeclaracaoImpostoRendaTO->getSessao()->st_nomeperfil !== 'Aluno') {
                throw new Exception(
                    'Não existem dados financeiros para geração da declaração de Imposto de Renda.'
                );
            }

            $textoSistemaToConsulta = new TextoSistemaTO();
            $textoSistemaToConsulta->setId_textosistema($textoSistemaTO->getId_textosistema());
            $textoSistemaToConsulta->setId_entidade($textoSistemaTO->getId_entidade());

            unset($textoSistemaTO);

            $result = $bo->gerarHTML(
                $textoSistemaToConsulta,
                $params,
                true
            )->subtractMensageiro();

            $texto = $result->mensagem[0]->getSt_texto();

            $str = "Bras&iacute;lia";
            $str_lg = strlen($str) + 2;
            $data_str = substr($texto, strripos($texto, $str) + $str_lg, 10);
            $data = new Zend_Date($data_str, 'pt_BR');
            $data_loc = $data->get("d 'de' MMMM 'de' YYYY");
            $texto_alt = str_replace($data_str, $data_loc, $texto);

            $this->view->conteudo = $texto_alt;

        } catch (Zend_Exception $e) {
            $this->_helper->viewRenderer->setNoRender(true);

            $erro .= '<p>Erro: ' . $e->getMessage() . '</p>';
            $erro .= '</div>';

            echo $erro;

        } catch (Exception $e) {
            $erro .= '<p><strong>Prezado(a) Aluno</strong>,</p>';
            $erro .= '<p>' . $e->getMessage() . '</p>';
            $erro .= '</div>';

            echo $erro;
        }
    }

    /**
     * Método que valida os Parametros de Geração de Texto Sistema
     */
    private function _validaParametrosGeracaoTextoSistema()
    {
        if (
            !$this->hasParam('arrParams') ||
            !$this->hasParam('id_textosistema') ||
            !$this->hasParam('type') ||
            !$this->hasParam('filename')
        ) {
            throw new Zend_Validate_Exception("Faltam de Argumentos de Paramêtros!");
        }
    }

}

<?php
/**
 * Controller de Primeiro Acesso do Portal do Aluno
 * @author Rafael Rocha
 */
class Portal_PrimeiroAcessoController extends PortalLogado
{

    /** @var PrimeiroAcessoBO */
    private $_bo;

    /** @var TextoSistemaBO */
    private $_textoSistemaBO;

    public function preDispatch()
    {
        parent::preDispatch();
    }

    /**
     * @see PortalController::init()
     */
    public function init()
    {
        parent::init();

        $this->_bo = new PrimeiroAcessoBO();
        $this->_textoSistemaBO = new TextoSistemaBO();
    }

    public function indexAction()
    {
        $this->redirecionarPrimeiroAcesso();
    }

    public function boasVindasAction()
    {
        $this->_helper->layout()->setLayout('layout');

        if (! $this->getRequest()->getParam('ok')) {
            $this->redirecionarPrimeiroAcesso();
        }

        $etapa = new PABoasVindasTO();
        $etapa->setId_entidade($etapa->getSessao()->id_entidade);
        $etapa->fetch(false, true, true);

        $textoSistema = new TextoSistemaTO();
        $textoSistema->setId_textosistema($etapa->getId_textosistema());
        $textoSistema->fetch(true, true, true);

        $progresso = $this->_bo->retornarProgresso();

        $textoSistema = $this->_textoSistemaBO
                             ->gerarPrimeiroAcesso(
                                    $textoSistema,
                                    array('id_venda' => $progresso->getId_venda())
                            )->getFirstMensagem();

        $this->view->textoBoasVindas = $textoSistema instanceof TextoSistemaTO ? $textoSistema->getSt_texto() : '';
    }

    public function dadosContatoAction()
    {
        $this->_helper->layout()->setLayout('layout');
        if (! $this->getRequest()->getParam('ok')) {
            $this->redirecionarPrimeiroAcesso();
        }

        $etapa = new PADadosContatoTO();
        $etapa->setId_entidade($etapa->getSessao()->id_entidade);
        $etapa->fetch(false, true, true);

        $textoInstrucao = new TextoSistemaTO();
        $textoInstrucao->setId_textosistema($etapa->getId_textoinstrucao());
        $textoInstrucao->fetch(true, true, true);

        $progresso = $this->_bo->retornarProgresso();

        $textoInstrucao = $this->_textoSistemaBO->gerarPrimeiroAcesso($textoInstrucao, array('id_venda' => $progresso->getId_venda()))->getFirstMensagem();

        $this->view->textoInstrucao = $textoInstrucao->getSt_texto();

        $textoMensagem = new TextoSistemaTO();
        $textoMensagem->setId_textosistema($etapa->getId_textomensagem());
        $textoMensagem->fetch(true, true, true);

        $textoMensagem = $this->_textoSistemaBO->gerarPrimeiroAcesso($textoMensagem, array('id_venda' => $progresso->getId_venda()))->getFirstMensagem();

        $this->view->textoMensagem = $textoMensagem->getSt_texto();

        $ro = new Ead1_RO();
        $this->view->mensageiroUF = $ro->uf();
    }

    public function dadosCadastraisAction()
    {
        $this->_helper->layout()->setLayout('layout');
        if (!$this->getRequest()->getParam('ok')) {
            $this->redirecionarPrimeiroAcesso();
        }

        $etapa = new PADadosCadastraisTO();
        $etapa->setId_entidade($etapa->getSessao()->id_entidade);
        $etapa->fetch(false, true, true);

        $textoInstrucao = new TextoSistemaTO();
        $textoInstrucao->setId_textosistema($etapa->getId_textoinstrucao());
        $textoInstrucao->fetch(true, true, true);

        $progresso = $this->_bo->retornarProgresso();

        $textoInstrucao = $this->_textoSistemaBO->gerarPrimeiroAcesso(
            $textoInstrucao
            , array('id_venda' => $progresso->getId_venda())
        )->getFirstMensagem();

        $this->view->textoInstrucao = $textoInstrucao->getSt_texto();

        $textoMensagem = new TextoSistemaTO();
        $textoMensagem->setId_textosistema($etapa->getId_textomensagem());
        $textoMensagem->fetch(true, true, true);

        $textoMensagem = $this->_textoSistemaBO->gerarPrimeiroAcesso(
            $textoMensagem,
            array('id_venda' => $progresso->getId_venda())
        )->getFirstMensagem();

        $this->view->textoMensagem = $textoMensagem->getSt_texto();
        $this->view->mostrarProsseguir = !count($this->_bo->retornarOcorrenciasAbertasDadosCadastrais());
    }

    public function aceitacaoContratoAction()
    {
        $this->_helper->layout()->setLayout('layout');

        if (! $this->getRequest()->getParam('ok')) {
            $this->redirecionarPrimeiroAcesso();
        }

        $this->view->textoContrato = $this->_bo->retornarTextoContrato();
    }

    public function mensagemInformativaAction()
    {
        $this->_helper->layout()->setLayout('layout');
        if (! $this->getRequest()->getParam('ok')) {
            $this->redirecionarPrimeiroAcesso();
        }

        $etapa = new PAMensagemInformativaTO();
        $etapa->setId_entidade($etapa->getSessao()->id_entidade);
        $etapa->fetch(false, true, true);

        $progresso = $this->_bo->retornarProgresso();

        $textoMensagem = new TextoSistemaTO();
        $textoMensagem->setId_textosistema($etapa->getId_textomensagem());
        $textoMensagem->fetch(true, true, true);

        $textoMensagem = $this->_textoSistemaBO->gerarPrimeiroAcesso(
            $textoMensagem,
            array('id_venda' => $progresso->getId_venda())
        )->getFirstMensagem();

        $this->view->textoMensagem = $textoMensagem->getSt_texto();
    }

    private function redirecionarPrimeiroAcesso()
    {
        $marcacaoEtapaTO = $this->_bo->retornarProgresso();

        if ($marcacaoEtapaTO->getSessao()->bl_pa_lembrarmaistarde) {
            $this->_redirect('portal/index/principal');
        }

        if (! $marcacaoEtapaTO->getBl_boasvindas()) {
            $this->getRequest()->setParam('ok', '1');
            $this->_forward('boas-vindas', 'primeiro-acesso');

        } elseif (! $marcacaoEtapaTO->getBl_dadoscontato()) {
            $this->getRequest()->setParam('ok', '1');
            $this->_forward('dados-contato', 'primeiro-acesso');

        } elseif (! $marcacaoEtapaTO->getBl_dadoscadastrais()) {
            $this->getRequest()->setParam('ok', '1');
            $this->_forward('dados-cadastrais', 'primeiro-acesso');

        } elseif (! $marcacaoEtapaTO->getBl_aceitacaocontrato()) {
            $this->getRequest()->setParam('ok', '1');
            $this->_forward('aceitacao-contrato', 'primeiro-acesso');

        } elseif (! $marcacaoEtapaTO->getBl_mensageminformativa()) {
            $this->getRequest()->setParam('ok', '1');
            $this->_forward('mensagem-informativa', 'primeiro-acesso');

        } else {
            $this->_redirect('portal/index/principal');
        }
    }

    public function prosseguirAction()
    {
        $etapa = $this->getRequest()->getParam('etapa');

        if ($etapa) {
            $retornoEtapa = $this->_bo->prosseguirEtapa($etapa);

            if ($retornoEtapa && $retornoEtapa->getType() == Ead1_IMensageiro::SUCESSO) {
                $this->redirecionarPrimeiroAcesso();
            } else {
                $this->_redirect('/portal/primeiro-acesso/' . $etapa);
            }
        }
    }

    public function salvarAlteracoesDadosContatoAction()
    {
        $vwPessoa = new VwPessoaTO();
        $vwPessoa->setId_usuario($vwPessoa->getSessao()->id_usuario);
        $vwPessoa->setId_entidade($vwPessoa->getSessao()->id_entidade);
        $vwPessoa->fetch(false, true, true);
        $alteracoes = null;

        if ($this->getRequest()->getParam('st_endereco')) {
            $vwPessoa->setSt_endereco($this->getRequest()->getParam('st_endereco'));
            $alteracoes['Endereço'] = $this->getRequest()->getParam('st_endereco');
            $alteracoes['alterar']['endereco'] = true;
        }

        if ($this->getRequest()->getParam('nu_numero')) {
            $vwPessoa->setNu_numero($this->getRequest()->getParam('nu_numero'));
            $alteracoes['Número'] = $this->getRequest()->getParam('nu_numero');
            $alteracoes['alterar']['endereco'] = true;
        }

        if ($this->getRequest()->getParam('st_complemento')) {
            $vwPessoa->setSt_complemento($this->getRequest()->getParam('st_complemento'));
            $alteracoes['Complemento'] = $this->getRequest()->getParam('st_complemento');
            $alteracoes['alterar']['endereco'] = true;
        }

        if ($this->getRequest()->getParam('st_bairro')) {
            $vwPessoa->setSt_bairro($this->getRequest()->getParam('st_bairro'));
            $alteracoes['Bairro'] = $this->getRequest()->getParam('st_bairro');
            $alteracoes['alterar']['endereco'] = true;
        }

        if ($this->getRequest()->getParam('sg_uf')) {
            $vwPessoa->setSg_uf($this->getRequest()->getParam('sg_uf'));
            $alteracoes['Uf'] = $this->getRequest()->getParam('sg_uf');
            $alteracoes['alterar']['endereco'] = true;
        }

        if ($this->getRequest()->getParam('id_municipio')) {
            $vwPessoa->setId_municipio($this->getRequest()->getParam('id_municipio'));
            $alteracoes['Cidade'] = $this->getRequest()->getParam('id_municipio');
            $alteracoes['alterar']['endereco'] = true;
        }

        if ($this->getRequest()->getParam('st_cep')) {
            $vwPessoa->setSt_cep($this->getRequest()->getParam('st_cep'));
            $alteracoes['CEP'] = $this->getRequest()->getParam('st_cep');
            $alteracoes['alterar']['endereco'] = true;
        }

        if ($this->getRequest()->getParam('nu_ddd')) {
            $vwPessoa->setNu_ddd($this->getRequest()->getParam('nu_ddd'));
            $alteracoes['DDD'] = $this->getRequest()->getParam('nu_ddd');
            $alteracoes['alterar']['telefone'] = true;
        }

        if ($this->getRequest()->getParam('nu_telefone')) {
            $vwPessoa->setNu_telefone($this->getRequest()->getParam('nu_telefone'));
            $alteracoes['Telefone'] = $this->getRequest()->getParam('nu_telefone');
            $alteracoes['alterar']['telefone'] = true;
        }

        if ($this->getRequest()->getParam('nu_dddalternativo')) {
            $vwPessoa->setNu_dddalternativo($this->getRequest()->getParam('nu_dddalternativo'));
            $alteracoes['DDD Alternativo'] = $this->getRequest()->getParam('nu_dddalternativo');
            $alteracoes['alterar']['telefone_alt'] = true;
        }

        if ($this->getRequest()->getParam('nu_telefonealternativo')) {
            $vwPessoa->setNu_telefonealternativo($this->getRequest()->getParam('nu_telefonealternativo'));
            $alteracoes['Telefone Alternativo'] = $this->getRequest()->getParam('nu_telefonealternativo');
            $alteracoes['alterar']['telefone_alt'] = true;
        }

        if ($this->getRequest()->getParam('st_email')) {
            $vwPessoa->setSt_email($this->getRequest()->getParam('st_email'));
            $alteracoes['Email'] = $this->getRequest()->getParam('st_email');
            $alteracoes['alterar']['email'] = true;
        }

        if ($this->getRequest()->getParam('st_enderecocorrespondencia')) {
            $vwPessoa->st_enderecocorrespondencia = $this->getRequest()->getParam('st_enderecocorrespondencia');
            $alteracoes['st_enderecocorrespondencia'] = $this->getRequest()->getParam('st_enderecocorrespondencia');
            $alteracoes['alterar']['endereco_correspondencia'] = true;
        }

        if ($this->getRequest()->getParam('nu_numerocorrespondencia')) {
            $vwPessoa->nu_numerocorrespondencia = $this->getRequest()->getParam('nu_numerocorrespondencia');
            $alteracoes['nu_numerocorrespondencia'] = $this->getRequest()->getParam('nu_numerocorrespondencia');
            $alteracoes['alterar']['endereco_correspondencia'] = true;
        }

        if ($this->getRequest()->getParam('st_complementocorrespondencia')) {
            $vwPessoa->st_complementocorrespondencia = $this->getRequest()->getParam('st_complementocorrespondencia');
            $alteracoes['st_complementocorrespondencia'] = $this->getRequest()->getParam('st_complementocorrespondencia');
            $alteracoes['alterar']['endereco_correspondencia'] = true;
        }

        if ($this->getRequest()->getParam('st_bairrocorrespondencia')) {
            $vwPessoa->st_bairrocorrespondencia = $this->getRequest()->getParam('st_bairrocorrespondencia');
            $alteracoes['st_bairrocorrespondencia'] = $this->getRequest()->getParam('st_bairrocorrespondencia');
            $alteracoes['alterar']['endereco_correspondencia'] = true;
        }

        if ($this->getRequest()->getParam('st_cepcorrespondencia')) {
            $vwPessoa->st_cepcorrespondencia = $this->getRequest()->getParam('st_cepcorrespondencia');
            $alteracoes['st_cepcorrespondencia'] = $this->getRequest()->getParam('st_cepcorrespondencia');
            $alteracoes['alterar']['endereco_correspondencia'] = true;
        }

        if ($this->getRequest()->getParam('id_municipiocorrespondencia')) {
            $vwPessoa->id_municipiocorrespondencia = $this->getRequest()->getParam('id_municipiocorrespondencia');
            $alteracoes['id_municipiocorrespondencia'] = $this->getRequest()->getParam('id_municipiocorrespondencia');
            $alteracoes['alterar']['endereco_correspondencia'] = true;
        }

        if ($this->getRequest()->getParam('sg_ufcorrespondencia')) {
            $vwPessoa->sg_ufcorrespondencia = $this->getRequest()->getParam('sg_ufcorrespondencia');
            $alteracoes['sg_ufcorrespondencia'] = $this->getRequest()->getParam('sg_ufcorrespondencia');
            $alteracoes['alterar']['endereco_correspondencia'] = true;
        }

        $alteracoes['Motivo'] = $this->getRequest()->getParam('motivo');
        $alteracoes['etapa'] = 'dados-contato';

        die($this->_helper->json($this->_bo->salvarAlteracoesDadosContato($vwPessoa, $alteracoes)));
    }

    public function salvarAlteracoesDadosCadastraisAction()
    {
        $alteracoes = null;

        if ($this->getRequest()->getParam('st_nomecompleto')) {
            $alteracoes['Nome do Aluno'] = $this->getRequest()->getParam('st_nomecompleto');
        }

        if ($this->getRequest()->getParam('st_cpf')) {
            $alteracoes['CPF'] = $this->getRequest()->getParam('st_cpf');
        }

        if ($this->getRequest()->getParam('dt_nascimento')) {
            $alteracoes['Data de Nascimento'] = $this->getRequest()->getParam('dt_nascimento');
        }

        if ($this->getRequest()->getParam('st_rg')) {
            $alteracoes['RG'] = $this->getRequest()->getParam('st_rg');
        }

        if ($this->getRequest()->getParam('st_orgaoexpeditor')) {
            $alteracoes['Órgão Expedidor'] = $this->getRequest()->getParam('st_orgaoexpeditor');
        }

        if ($this->getRequest()->getParam('dt_dataexpedicao')) {
            $alteracoes['Data de expedição'] = $this->getRequest()->getParam('dt_dataexpedicao');
        }

        if ($this->getRequest()->getParam('st_cidadenascimento')) {
            $alteracoes['Naturalidade'] = $this->getRequest()->getParam('st_cidadenascimento');
        }

        if ($this->getRequest()->getParam('st_nacionalidade')) {
            $alteracoes['Nacionalidade'] = $this->getRequest()->getParam('st_nacionalidade');
        }

        $alteracoes['Motivo'] = $this->getRequest()->getParam('motivo');
        $alteracoes['etapa'] = 'dados-cadastrais';

        die($this->_helper->json($this->_bo->salvarAlteracoesDadosCadastrais($alteracoes)));
    }

    public function lembrarMaisTardeAction()
    {
        $this->_redirect('portal/index/principal');
    }
}

<?php
/**
 * Classe de controle de declarações da matricula
* @author eduardoromao
*/
class Portal_MatriculadeclaracaoController extends PortalBase {
	
	/**
	 * Método que chama a view de entrega de Declaracao
	 */
	public function entregadeclaracaoAction(){
		try{
			//Grava log de acesso
			$logTO = new LogAcessoTO();
			$logTO->setId_funcionalidade(LogAcessoTO::DECLARACAO);
			$logRO = new LogRO();
			$logRO->gravaLog($logTO);
			
			$this->view->listDeclaracoesJSON = array();
			$this->view->erro = null;
			$declaracaoRO = new DeclaracaoRO();
			$vwEntregaDeclaracaoTO = new VwEntregaDeclaracaoTO();
			$vwEntregaDeclaracaoTO->setId_matricula(Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula);
			$arrListDeclaracoes = array();
			$mensageiro = $declaracaoRO->retornarVwEntregaDeclaracao($vwEntregaDeclaracaoTO);
			$mensageiro->subtractMensageiro();
			foreach ($mensageiro->getMensagem() as $msgVwEntregaDeclaracaoTO){
				if($msgVwEntregaDeclaracaoTO->getDt_solicitacao()){
					$msgVwEntregaDeclaracaoTO->setDt_solicitacao($msgVwEntregaDeclaracaoTO->getDt_solicitacao()->toString("dd/M/Y"));
				}
				if($msgVwEntregaDeclaracaoTO->getDt_entrega()){
					$msgVwEntregaDeclaracaoTO->setDt_entrega($msgVwEntregaDeclaracaoTO->getDt_entrega()->toString("dd/M/Y"));
				}
				$arrListDeclaracoes[] = $msgVwEntregaDeclaracaoTO;
			}
			$this->view->listDeclaracoesJSON = Zend_Json::encode($arrListDeclaracoes);
		}catch (Zend_Exception $e){
			$this->view->erro = $mensageiro;
		}
	}

	/**
	 * Método que chama a view de listagem de produtos do tipo declaracao
	 */
	public function listaprodutodeclaracaoAction(){
		try{
			$this->view->listProdutosJSON = array();
			$this->view->erro = null;
			$produtoRO = new ProdutoRO();
			$vwProdutoTextoSistema = new VwProdutoTextoSistemaTO();
			$vwProdutoTextoSistema->setId_entidade(Ead1_Sessao::getSessaoEntidade()->id_entidade);
			$vwProdutoTextoSistema->setId_tipoproduto(TipoProdutoTO::DECLARACOES);
			$arrListProdutos = array();
			$mensageiro = $produtoRO->retornarVwProdutoTextoSistema($vwProdutoTextoSistema);
			$mensageiro->subtractMensageiro();
			foreach ($mensageiro->getMensagem() as $msgVwProdutoTextoSistema){
				$msgVwProdutoTextoSistema->chkbox = '<input type="checkbox" name="textosistema[]" id="textosistema" value="'.$msgVwProdutoTextoSistema->getId_textoSistema().'"/>'; 
				$arrListProdutos[] = $msgVwProdutoTextoSistema;
			}
			$this->view->listProdutosJSON = Zend_Json::encode($arrListProdutos);
		}catch (Zend_Exception $e){
			$this->view->erro = $mensageiro;
		}
	}

	/**
	 * Método que cadastra o produto declaracao
	 */
	public function cadastrarprodutodeclaracaoAction(){
		$resposta = null;
		$ro = new DeclaracaoBO();
		if(!count($this->_getParam('id_textosistema',array()))){
			$resposta = new Ead1_Mensageiro("É Necessário informar Pelo menos uma Declaração!",Ead1_IMensageiro::AVISO);
		}else{
			$arrEntregaDeclaracaoTO = array();
			foreach ($this->_getParam('id_textosistema') as $id_textosistema){
				$entregaDeclaracaoTO = new EntregaDeclaracaoTO();
				$entregaDeclaracaoTO->setId_textosistema($id_textosistema);
				$entregaDeclaracaoTO->setId_matricula(Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula);
				$entregaDeclaracaoTO->setId_situacao(EntregaDeclaracaoTO::SITUACAO_PENDENTE);
				$arrEntregaDeclaracaoTO[] = $entregaDeclaracaoTO;
			}
			$resposta = $ro->cadastrarAlunoEntregaDeclaracao($arrEntregaDeclaracaoTO);
		}
		$this->resposta($resposta);
	}
}
<?php

/**
 * @author Denise
 * Classe com métodos de vendas/financeiro do aluno
 *
 */
class Portal_FinanceiroController extends PortalBase
{

    private $_bo;

    public function init()
    {
        parent::init();
        $this->_bo = new PortalFinanceiroBO();
    }

    /**
     * Método usado para chamar a index.phtml e retornar os scripts do backbone
     */
    public function indexAction()
    {
        $this->_helper->layout()->setLayout('layout');
        try {
            if (!Ead1_Sessao::getSessaoUsuario()->id_usuario) {
                throw new Zend_Exception('O id_usuario é obrigatório e não foi passado!');
            }
            $vwCVTO = new VwClienteVendaTO();
            $vwCVTO->setId_usuario(Ead1_Sessao::getSessaoUsuario()->id_usuario);

            $this->view->vendasaluno = $this->_bo->retornaVendasCliente($vwCVTO);
        } catch (Zend_Exception $e) {
            $this->view->erro = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    public function jsPlanoPagamentoAction()
    {
        $this->renderScript('financeiro/js-plano-pagamento.js');
    }

    /**
     * Método usado para chamar a index.phtml e retornar os scripts do backbone
     */
    public function planoPagamentoAction()
    {
        $textoBO = new TextoSistemaBO();

        $to = new TextoSistemaTO();
        $to->setId_textocategoria(G2\Constante\TextoCategoria::PLANO_PAGAMENTO);
        $to->setId_entidade(Ead1_Sessao::getSessaoGeral()->id_entidade);
        $to->fetch(false, true, true);

        if (!$to->getSt_texto()) {
            $texto = "<br><br>Nenhum texto sistema para Plano de Pagamento foi encontrado.";
        }

        if (Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula) {
            $matriculaContrato = new ContratoMatriculaTO();
            $matriculaContrato->setId_matricula(Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula);
            $matriculaContrato->fetch(false, true, true);

            if ($matriculaContrato->getId_contrato()) {
                $firstMessage = $textoBO->gerarHTML($to, array('id_contrato' => $matriculaContrato->getId_contrato()));
                if ($firstMessage->getFirstMensagem() instanceof TextoSistemaTO) {
                    $texto = $firstMessage->getFirstMensagem()->getSt_texto();
                } else {
                    $texto = '<br/><br/>Nenhum texto foi encontrado para esta entidade';
                }
            } else {
                $texto = '<br/><br/>Nenhum contrato localizado para a matrícula ' . Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula . '.';
            }
        } else {
            $texto = "<br><br>Não foi possível localizar o código da matricula.";
        }
        $this->view->planoPagamento = $texto;
        if ($this->getParam('imprimir')) {
            $this->view->imprimir = true;
        } else {
            //$this->_helper->layout()->setLayout('layout');
        }
    }

    /**
     * Retorna as vendas do aluno pelo id_usuario
     * @throws Zend_Exception
     */
    public function retornaVendasAlunoAction()
    {
        try {
            if (!Ead1_Sessao::getSessaoUsuario()->id_usuario) {
                throw new Zend_Exception('O id_usuario é obrigatório e não foi passado!');
            }
            $vwCVTO = new VwClienteVendaTO();
            $vwCVTO->setId_usuario(Ead1_Sessao::getSessaoUsuario()->id_usuario);

            $this->view->vendasaluno = $this->_bo->retornaVendasCliente($vwCVTO);
        } catch (Zend_Exception $e) {
            $this->view->erro = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Retorna as vendas do aluno pelo id_usuario
     * @throws Zend_Exception
     */
    public function retornarVendasAlunoJsonAction()
    {
        $this->getHelper('viewRenderer')->setNoRender();
        try {
            if (!Ead1_Sessao::getSessaoUsuario()->id_usuario) {
                throw new Zend_Exception('O id_usuario é obrigatório e não foi passado!');
            }

            if (!Ead1_Sessao::getSessaoUsuario()->id_entidade) {
                throw new Zend_Exception('O id_entidade é obrigatório e não foi passado!');
            }

            $vwCVTO = new VwClienteVendaTO();
            $vwCVTO->setId_usuario(Ead1_Sessao::getSessaoUsuario()->id_usuario);
            $vwCVTO->setId_entidade(Ead1_Sessao::getSessaoUsuario()->id_entidade);
            $vwCVTO->montaToDinamico($this->getAllParams());

            $this->_helper->json($this->_bo->retornaVendasCliente($vwCVTO, true));
        } catch (Zend_Exception $e) {
            $this->view->erro = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    public function extratoColaboradorAction()
    {
        $this->_helper->layout()->setLayout('layout');
        $this->view->usuario = Ead1_Sessao::getSessaoUsuario();
        $this->view->anos = $this->retornarAnosSelect();
    }

    public function retornarExtratoAction()
    {
        $ano = $this->getParam('ano') ? $this->getParam('ano') : date('Y');
        $mes = $this->getParam('mes') ? $this->getParam('mes') : date('m');
        $idUsuario = Ead1_Sessao::getSessaoUsuario()->id_usuario;
        $params = array('id' => $idUsuario, 'ano' => $ano, 'mes' => $mes);
        $negocio = new \G2\Negocio\PagamentoColaborador();
        $result = $negocio->retornarExtratoDetalhadoColaborador($params);
        $this->_helper->json($result);
    }

    /**
     * Retorna array com anos abaixo do atual
     * @return array
     */
    private function retornarAnosSelect()
    {
        $anoAtual = intval(date("Y"));
        $anoIni = $anoAtual - 10;
        $arrAnos = array();
        for ($i = $anoAtual; $i >= $anoIni; $i--) {
            $arrAnos[] = $i;
        }
        return $arrAnos;
    }

    public function mudarCartaoRecorrenteAction()
    {

        try {
            $id_venda = $this->_getParam('venda', null);
            if (!$id_venda) {
                $id_venda = $this->_getParam('id_venda', null);
                if (!$id_venda) {
                    die('Lançamento não encontrado.');
                }
            }

            $financeiroNegocio = new \G2\Negocio\Financeiro();
            $dados = $financeiroNegocio->retornaDadosVendaRecorrente($id_venda);

            $this->view->id_entidade = $dados['venda']->getIdEntidade();
            $this->view->venda = $dados['venda'];
            $this->view->pessoa = $dados['pessoa'];
            $this->view->vendaLancamentos = $dados['vendaLancamentos'];
            $this->view->entidadeintegracao = $dados['entidadeintegracao'];

            if ($dados['pagamento']) {

                $this->view->pagamento = $dados['pagamento'];

                $mp = new \G2\Negocio\MensagemPadrao();
                $mpadrao = $mp->getSistemaEntidadeMensagem(array('id_mensagempadrao' => 20));

                if (isset($mpadrao[0]) && $mpadrao[0] instanceof \G2\Entity\SistemaEntidadeMensagem) {

                    $textoSistema = new TextoSistemaTO();
                    $textoSistema->setId_textosistema($mpadrao[0]->getId_textosistema()->getId_textosistema());
                    $textoSistema->fetch(true, true, true);

                    if ($textoSistema->getSt_texto()) {

                        $txtBO = new TextoSistemaBO();
                        $textoSistema = $txtBO->gerarTextoLoja($textoSistema, array('id_venda' => $dados['venda']->getIdVenda()))->getFirstMensagem();
                        $this->view->textopagamento = $textoSistema instanceof TextoSistemaTO ? $textoSistema->getSt_texto() : '';
                    }
                }

                $mpadrao = $mp->getSistemaEntidadeMensagem(array('id_mensagempadrao' => 21));

                if (isset($mpadrao[0]) && $mpadrao[0] instanceof \G2\Entity\SistemaEntidadeMensagem) {

                    $textoSistema = new TextoSistemaTO();
                    $textoSistema->setId_textosistema($mpadrao[0]->getId_textosistema()->getId_textosistema());
                    $textoSistema->fetch(true, true, true);

                    if ($textoSistema->getSt_texto()) {

                        $txtBO = new TextoSistemaBO();
                        $textoSistema = $txtBO->gerarTextoLoja($textoSistema, array('id_venda' => $dados['venda']->getIdVenda()))->getFirstMensagem();
                        $this->view->textoproduto = $textoSistema instanceof TextoSistemaTO ? $textoSistema->getSt_texto() : '';
                    }
                }
            }
        } catch (Exception $e) {
            $this->view->mensagem = $e->getMessage();
        }


        $this->renderScript('financeiro/mudar-cartao-recorrente.phtml');
    }

    /**
     * Realiza o pagamento por cartão de crédito
     * @return void
     */
    public function mudarCartaoAction()
    {
        $id_venda = $this->_getParam('id_venda', null);

        if (!$id_venda) {
            $mensageiro = new Ead1_Mensageiro('Lançamento não encontrado!', Ead1_IMensageiro::ERRO);
        } else {
            try {

                $venda = new VendaTO();
                $venda->setId_venda($id_venda);
                $venda->fetch(true, true, true);

                $post = $this->limpaGetAllParams($this->getAllParams());

                $negocio = new \G2\Negocio\Pagarme();
                /** @var \G2\Entity\VwResumoVendaLancamento $lancamento */
                $lancamento = $negocio->findOneBy('G2\Entity\VwResumoVendaLancamento', array('id_venda' => $id_venda));

                if ($lancamento) {
                    if (
                        $lancamento->getid_sistemacobranca() == \G2\Constante\Sistema::PAGARME
                        &&
                        substr($lancamento->getRecorrenteOrderid(), -3) != 'G1U'
                        &&
                        substr_count($lancamento->getRecorrenteOrderid(), '-') != 4
                    ) {
                        $mensageiro = $negocio->mudarCartaoAssinatura($post);
                    } else {
                        $parcelaValor = explode('_', $post['nu_parcelas']);
                        $post['id_venda'] = $id_venda;
                        $pagamento = new \BraspagRecorrenteV2BO($venda->getId_entidade());
                        $post['id_cartaobandeira'] = $post['nu_cartaoUtilizadoRecorrente'] ? $post['nu_cartaoUtilizadoRecorrente'] : CartaoBandeiraTO::BANDEIRA_VISA;

                        $mensageiro = $pagamento->trocarCartaoRecorrente($post);
                    }
                }

                SessaoLojaBO::limparSessaoLoja();
                if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    $pagarme = new \G2\Negocio\Pagarme();
                    $status = $pagarme->getSubscription($venda->getRecorrenteOrderid(), $venda->getId_entidade());
                    if (!empty($mensageiro) &&
                        $mensageiro instanceof Ead1_Mensageiro &&
                        $mensageiro->getCodigo() instanceof PagarMe_Error &&
                        $mensageiro->getCodigo()->getType() === 'action_forbidden' &&
                        $status->getMensagem()[0]['status'] === 'canceled') {
                            $mensageiro = $this->mudarCartaoCancelado($post);
                    } else {
                        $mensageiro = new Ead1_Mensageiro($mensageiro->getFirstMensagem(), Ead1_IMensageiro::AVISO);
                    }

                } else {
                    $mensageiro = new Ead1_Mensageiro($mensageiro->getFirstMensagem(), Ead1_IMensageiro::SUCESSO);
                }
            } catch (Exception $e) {
                $mensageiro = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
            }
        }
        $this->_helper->json($mensageiro);
    }


    /**
     * Realiza a emissão de boletos
     *
     * @todo verificar forma dinâmica de imagem de entidade
     * @todo testar com o valor a partir do FLEX
     * @todo remover id_lancamento fixo no código para testes
     * @return void
     */
    public function gerarBoletoAction()
    {
        $this->ajax();
        try {

            $id_lancamento = $this->getRequest()->getParam('id_lancamento', null);

            if (!$id_lancamento) {
                throw new \Exception('Não foi possível gerar o Boleto.');
            }

            $boletoPHP = new \G2\Negocio\BoletoPHP();
            $lan = $boletoPHP->emitirBoleto($id_lancamento, true);

            if (!$lan) {
                throw new \Exception('Não foi possível gerar o Boleto.');
            }

            // A rotina só entra neste IF se o bl_json for verdadeiro
            if ($lan instanceof \G2\Entity\Lancamento) {
                if ($lan->getSt_urlboleto()) {
                    $this->json(array('st_urlboleto' => $lan->getSt_urlboleto(), 'st_mensagem' => 'O Boleto já está disponível para pagamento. Caso o mesmo não abra, por favor desbloqueie as janelas popup para este site e tente novamente.'));
                } else {

                    $vw = new \G2\Entity\VwLancamento();
                    $vw->find($id_lancamento);
                    if ($vw->getId_venda()) {
                        $venda = new \G2\Entity\VwClienteVenda();
                        $venda->findOneBy(array('id_venda' => $vw->getId_venda()));
                        if ($venda->getSt_email()) {
                            $mensagem = "Caro Usuário, a solicitação para geração de boleto foi enviada com sucesso!<br>
Enviaremos uma notificação para o e-mail {$venda->getSt_email()} quando o boleto estiver disponível para pagamento.";
                        }
                    }

                    $this->json(array('st_urlboleto' => null, 'st_mensagem' => $mensagem));

                }
            } else {
                throw new \Exception('Não foi possível gerar o Boleto.');
            }

        } catch (Exception $exc) {
            $this->json(array('st_urlboleto' => null, 'st_mensagem' => $exc->getMessage()));
        }
    }

    private function mudarCartaoCancelado($data){
        $pagarme = new \G2\Negocio\Pagarme();

        try {
            if (empty($data)) {
                throw new Exception('Informe os dados para reativação da assinatura.');
            }

            return $pagarme->trocarCartaoAssinaturaCancelada($data);
        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }
}

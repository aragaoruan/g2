<?php


class Portal_PromocoesController extends PortalBase
{


    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $this->_helper->layout()->setLayout('layout');

        try {

            $idEntidade = \Ead1_Sessao::getSessaoGeral()->id_entidade;//pega o id da entidade

            $params = array();

            $params['bl_vigente'] = 1;
            $params['id_situacao'] = \G2\Constante\Situacao::TB_CAMPANHACOMERCIAL_ATIVO;
            $params['bl_portaldoaluno'] = $this->_getParam('bl_portaldoaluno', 1);
            $params['id_matricula'] = \Ead1_Sessao::getSessaoGeral()->id_matricula;

            $params['order'] = array(
                'bl_visualizado' => 'ASC',
                'dt_cadastro' => 'DESC'
            );

            $negocio = new \G2\Negocio\CampanhaComercial();
            $this->view->promocoes = $negocio->retornarVwCampanhaComercial($params, $idEntidade, false);

        } catch (\Exception $exception) {
            $this->view->erro = new Ead1_Mensageiro($exception->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    public function promocaoAction()
    {

        try {

            if (empty($this->_getParam('id_campanha', null))) {
                throw new Exception('Id da campanha não informado!');
            }

            $negocio = new \G2\Negocio\CampanhaComercial();

            $params['bl_vigente'] = 1;
            $params['bl_ativo'] = 1;
            $params['id_situacao'] = \G2\Constante\Situacao::TB_CAMPANHACOMERCIAL_ATIVO;
            $params['bl_portaldoaluno'] = $this->_getParam('bl_portaldoaluno', 1);
            $params['id_matricula'] = \Ead1_Sessao::getSessaoGeral()->id_matricula;

            $promocao = $negocio->retornarVwCampanhaComercial(
                array_merge($params,
                    array(
                        'id_campanhacomercial' => $this->_getParam('id_campanha', null)
                    )
                ),
                \Ead1_Sessao::getSessaoGeral()->id_entidade,
                false
            );

            if (isset($promocao[0]['bl_visualizado']) && $promocao[0]['bl_visualizado'] == false) {
                $negocio->marcarCampanhaViualizada(
                    array(
                        'id_usuario' => \Ead1_Sessao::getSessaoGeral()->id_usuario,
                        'id_campanhacomercial' => $promocao[0]['id_campanhacomercial'],
                        'id_matricula' => \Ead1_Sessao::getSessaoGeral()->id_matricula
                    )
                );
            }

            $params['bl_visualizado'] = 0;
            $params['columns'] = array('id_campanhacomercial');

            $negocio = new \G2\Negocio\CampanhaComercial();
            $promocoes = $negocio->retornarVwCampanhaComercial(
                $params,
                \Ead1_Sessao::getSessaoGeral()->id_entidade,
                false
            );

            $this->view->quantidadeNovas = count($promocoes);
            $this->view->promocao = $promocao[0];

        } catch (\Exception $exception) {
            $this->view->erro = new Ead1_Mensageiro($exception->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

}

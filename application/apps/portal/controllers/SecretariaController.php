<?php

/**
 *
 * Classe de controller para Secretaria do Portal
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 *
 */
class Portal_SecretariaController extends PortalBase
{

    public function init()
    {
        parent::init();
    }
    

    public function requerimentoAction()
    {
        $this->_helper->layout->setLayout('layout');

        $secretaria = new \G2\Negocio\Secretaria();
        $retorno = $secretaria->buscaOcorrencia();
        $this->view->ocorrencia = $retorno;
    }

    public function listaServicosAction()
    {
        $this->_helper->getHelper('viewRenderer')->setNoRender();
        $negocio = new \G2\Negocio\Negocio();
        $produto = new \G2\Negocio\Produto();
        $data = array(
            'id_tipoproduto' => \G2\Constante\TextoCategoria::DECLARACOES,
            'id_situacao' => \G2\Constante\Situacao::TB_PRODUTO_ATIVO
        );

        $result = $produto->retornarProduto($data);
        $resultado = array();
        foreach ($result as $value) {
            $resultado[] = $negocio->toArrayEntity($value);
        }
        $this->_helper->json($resultado);
    }

    public function  novoRequerimentoAction()
    {
        $this->_helper->layout->setLayout('layout');
        $sessao = Ead1_Sessao::getSessaoUsuario();

        $entidade = new \G2\Negocio\Entidade();
        $dadosEntidade = $entidade->findEntidade(array('id_entidade' => $sessao->id_entidade), true);

        $this->view->usuario = $sessao->toArray();
        $this->view->matricula = Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula;
        $this->view->dadosEntidade = $dadosEntidade;
    }

    public function visualizarAction()
    {
        $this->_helper->layout->setLayout('layout');
        $requerimento = $this->getRequest()->getParam('requerimento');

        $secretaria = new \G2\Negocio\Secretaria();
        $retorno = $secretaria->buscaOcorrenciaProduto(array('id_ocorrencia' => $requerimento));
        $this->view->retorno = $retorno;
    }

    /**
     * Action que gera o Texto do Sistema
     */
    public function geracaoAction()
    {
        $this->_helper->layout->disableLayout();
        try {
            $bo = new TextoSistemaBO();
            $this->_validaParametrosGeracaoTextoSistema();
            $textoSistemaTO = new TextoSistemaTO();
            $textoSistemaTO->setId_textosistema((int)$this->_getParam('id_textosistema'));
            switch ($this->_getParam('type')) {
                case TextoSistemaBO::HTML:
                    $mensageiro = $bo->gerarHTML($textoSistemaTO, $this->_getParam('arrParams'))->subtractMensageiro();
                    echo $mensageiro->getFirstMensagem()->getSt_texto();
                    break;
                case TextoSistemaBO::PDF:
                    $path = realpath(APPLICATION_PATH . '/../public/upload/declaracoes/') . '/';
                    $pdf = $bo->gerarPDF($textoSistemaTO, $this->_getParam('arrParams'), $this->_getParam('filename'),$path)->subtractMensageiro();
                    $this->_redirect('/upload/declaracoes/' . $pdf->filename);
                default:
                    THROW new Zend_Validate_Exception("Tipo de Gera��o n�o Suportado. (" . strtoupper($this->_getParam('file')) . ")");
                    break;
            }
        } catch (Zend_Exception $e) {
            $this->_helper->viewRenderer->setNoRender(false);
            $erro = '<div class="container cabecalho prepend-top erro span-24" >';
            $erro .= '<p>Erro: ' . $e->getMessage() . '</p>';
            $erro .= '</div>';
            $this->view->cabecalho = $erro;
        }
    }


    /**
     * M�todo que valida os Parametros de Gera��o de Texto Sistema
     */
    private function _validaParametrosGeracaoTextoSistema()
    {
        if (!$this->hasParam('arrParams') || !$this->hasParam('id_textosistema') || !$this->hasParam('type') || !$this->hasParam('filename')) {
            THROW new Zend_Validate_Exception("Faltam de Argumentos de Param�tros!");
        }
    }
    

   

    public function bibliotecaVirtualAction()
    {
        $this->_helper->layout()->setLayout('layout');
        $integracaoNegocio = new \G2\Negocio\EntidadeIntegracao();
        $integracao = $integracaoNegocio->retornarIntegracaoPearson();
        $this->view->integracao = $integracao;
    }

    public function extratoPagamentoAction()
    {
        $this->_helper->layout()->setLayout('layout');
        if(Ead1_Sessao::getSessaoPerfil()->is_professor()) {
            $id_usuario = Ead1_Sessao::getSessaoUsuario()->id_usuario;
            $nome_usuario = Ead1_Sessao::getSessaoUsuario()->st_nomecompleto;
            $this->view->nome_usuario = $nome_usuario;
            $this->view->id_usuario = $id_usuario;
        }
    }
}

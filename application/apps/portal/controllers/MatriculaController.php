<?php

/**
 * Classe de Controller de matrícula
 * @author eduardoromao
 */
class Portal_MatriculaController extends PortalBase
{

    /**
     * @var PortalMatriculaBO
     */
    private $_bo;

    public function init()
    {
        parent::init();
        $this->_bo = new PortalMatriculaBO();
    }

    /**
     * Método que renderiza a tela da grade da Matrícula
     */
    public function gradeAction()
    {
        try {
            $this->_helper->layout()->setLayout('layout');
            //Grava log de acesso
            $logTO = new LogAcessoTO();
            $logTO->setId_funcionalidade(LogAcessoTO::GRADE_DE_NOTAS);
            $logRO = new LogRO();
            $logRO->gravaLog($logTO);

            $this->view->dados = array();
            $this->view->dadosPRR = array();
            $this->view->erro = null;
            $this->view->tiposAvaliacao = array();
            $this->view->bl_temnotafinal = null;
            $this->view->bl_mostratcc = null;
            $this->view->id_linhadenegocio = null;
            $this->view->dadosPRRSemDiscOrig = array();
            $this->view->exibirMinhasProvas = null;

            $esquemaConfiguracao = new \G2\Negocio\EsquemaConfiguracao();
            $configModeloGradeNota = $esquemaConfiguracao->retornarConfiguracaoModeloGradeNotas();
            $result = $esquemaConfiguracao->retornarEsquemaValores(array(
                'id_itemconfiguracao' => \G2\Constante\ItemConfiguracao::LINHA_DE_NEGOCIO
            ));
            $arrEsqConfiguracao = is_array($result->getMensagem()) ? $result->getMensagem()[0] : null;
            if (isset($arrEsqConfiguracao['st_valor'])) {
                $this->view->id_linhadenegocio = $arrEsqConfiguracao['st_valor'];
            }

            $configuracaoEntidade = new \G2\Negocio\ConfiguracaoEntidade();
            $resultConfigEntidade = $configuracaoEntidade->findConfiguracaoEntidade($configuracaoEntidade->getId_entidade(), true);
            if ($resultConfigEntidade['bl_minhasprovas']) {
                $this->view->exibirMinhasProvas = $resultConfigEntidade['bl_minhasprovas'];
            }

            $dados = array();
            $_negocio = new \G2\Negocio\Matricula();

            $params = array(
                'id_matricula' => (int)$_negocio->sessao->id_matricula,
                'id_categoriasala' => G2\Constante\CategoriaSala::NORMAL,
                'id_situacao' => G2\Constante\Situacao::TB_MATRICULADISCIPLINA_CONCEDIDA
            );
            $result = $_negocio->retornarGradeAlunoPortal($params);

            $paramsPRR = array(
                'id_matricula' => (int)$_negocio->sessao->id_matricula,
                'id_categoriasala' => G2\Constante\CategoriaSala::PRR
            );
            $resultPRR = $_negocio->retornarGradePrrAlunoPortal($paramsPRR);

            if ($result->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $dados = $result->getMensagem();
            }
            $discipinasPRR = array();
            $dadosPRR = array();

            if ($resultPRR->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $ResultPRR = $resultPRR->getMensagem();
                foreach ($ResultPRR as $key => $prr) {
                    // Se o PRR tiver nota satisfatoria, setar o status da disciplina como satisfatório
                    if ($prr["st_status"] == 'Satisfatório') {
                        foreach ($dados['mod'] AS $val) {
                            foreach ($dados[$val]['dados'] AS &$data) {
                                //Se for a mesma disciplina e na disciplina original ele não esta 'satisfatório' e (não é disciplina de TCC ou é para mostrar a nota de TCC )
                                if ($data['id_disciplina'] == $prr["id_disciplina"] &&
                                    $data['bl_status'] == 0 &&
                                    ($data['id_tipodisciplina'] != 2 || $prr["bl_mostratcc"] == 1)
                                ) {

                                    $data['st_status'] = $prr['st_status'];
                                }

                            }
                        }
                    }


                    if (in_array($prr["id_disciplina"], $discipinasPRR)) {

                        $k = array_search($prr["id_disciplina"], $discipinasPRR);
                        if ($prr['st_notaead'] > $discipinasPRR[$k]['st_notaead']) {
                            $dadosPRR[$k] = $prr;
                        }
                    } else {
                        $dadosPRR[] = $prr;
                        $discipinasPRR[] = $prr["id_disciplina"];
                    }
                }
                //Cria array disciplinas PRR sem disciplinas originais
                $dadosPRRSemDiscOrig = array();
                foreach ($discipinasPRR AS $discPRR) {
                    $flagDisciplinasPrrIguais = 0;
                    foreach ($dados['mod'] AS $keymod => $modulo) {
                        foreach ($dados[$modulo]['dados'] AS $disciplinas) {
                            if ($discPRR == $disciplinas['id_disciplina']) {
                                $flagDisciplinasPrrIguais = 1;
                            }
                        }
                    }
                    if ($flagDisciplinasPrrIguais == 0) {
                        $dadosPRRSemDiscOrig[$modulo][] = $discPRR;
                    }
                }
            }

            unset($dados['mod']);
            unset($dados['curso']);
            unset($dados['coordenador']);

            if (!empty($dados['tiposAvaliacao'])) {
                $this->view->tiposAvaliacao = $dados['tiposAvaliacao'];
                unset($dados['tiposAvaliacao']);
            } elseif (isset($dados['tiposAvaliacao']) && !count($dados['tiposAvaliacao'])) {
                unset($dados['tiposAvaliacao']);
            }

            if (array_key_exists('bl_temnotafinal', $dados)) {
                $this->view->bl_temnotafinal = $dados['bl_temnotafinal'];
                unset($dados['bl_temnotafinal']);
            }

            if (array_key_exists('bl_mostratcc', $dados)) {
                if (!empty($dadosPRR)) {
                    foreach ($dadosPRR as $k => $ddPRR) {
                        if ($ddPRR['st_notatcc']) {//&& $ddPRR['bl_mostratcc']){
                            $this->view->bl_mostratcc = true;//$ddPRR['bl_mostratcc'];
                        }
                        unset($dados['bl_mostratcc']);
                    }
                } else {
                    $this->view->bl_mostratcc = $dados['bl_mostratcc'];
                    unset($dados['bl_mostratcc']);
                }
            }

            $this->view->dados = $dados;
            $this->view->dadosPRR = $dadosPRR;
            $this->view->disciplinasPRR = $discipinasPRR;
//            $this->view->dadosPRRSemDiscOrig = $dadosPRRSemDiscOrig;


            $this->renderScript('matricula/gradenota.phtml');

        } catch (Zend_Exception $e) {
            $mensageiro = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
            $this->view->erro = $mensageiro;
        }
    }


    public function pesquisarAlunoAction()
    {
        try {
            $this->_helper->layout()->setLayout('layout');
            $this->renderScript('matricula/pesquisaraluno.phtml');

        } catch (Zend_Exception $e) {
            $this->view->erro = 'erro';
        }
    }

    public function pesquisaAlunoPerfilAction()
    {
        // sem este array inicializado a página não carrega como deveria
        $retorno = array('id_matricula' => '', 'st_nomecompleto' => '', 'st_projetopedagogico' => '');

        $this->_helper->viewRenderer->setNoRender(true);
        $params = array();
        $string = $this->getParam('st_dadopesquisa');

        if ($string) {
            $retorno = null;

            $negocio = new \G2\Negocio\Matricula();
            $negocioEntidade = new \G2\Negocio\Entidade();
            $params['st_dadopesquisa'] = $string;
            $params['id_entidade'] = Ead1_Sessao::getSessaoEntidade()->id_entidade;
            $params['id_esquemaconfiguracao'] = $negocioEntidade->retornaEsquemaConfiguracao($params['id_entidade']);

            if (Ead1_Sessao::getSessaoPerfil()->is_observadorInstitucional()
                || Ead1_Sessao::getSessaoPerfil()->is_suporte()
            ) {
                $params['id_entidadeatendimento'] = Ead1_Sessao::getSessaoEntidade()->id_entidade;
                // TODO: duplicar este método para uma versão de graduação(pesquisarAlunosMatriculaGraduacao)
                $retorno = $negocio->pesquisarAlunosMatricula($params);

            }

            if (Ead1_Sessao::getSessaoPerfil()->is_professor()
                || Ead1_Sessao::getSessaoPerfil()->is_coordenadorProjetoPedagogico()
                || Ead1_Sessao::getSessaoPerfil()->is_coordenadorDisciplina()
            ) {

                $params['id_entidade'] = Ead1_Sessao::getSessaoEntidade()->id_entidade;
                $params['id_usuarioperfil'] = Ead1_Sessao::getSessaoUsuario()->id_usuario;
                $params['id_perfilpedagogico'] = Ead1_Sessao::getSessaoPerfil()->id_perfilpedagogico;

                // TODO: duplicar este método para uma versão de graduação(pesquisarAlunosMatriculaPerfilGraduacao)
                $retorno = $negocio->pesquisarAlunosMatriculaPerfil($params);

            }

            if ($retorno) {
                foreach ($retorno as $i => $row) {
                    if (!empty($row['dt_inicio'])) {
                        $retorno[$i]['dt_inicio'] = \G2\Utils\Helper::converterData($row['dt_inicio'],
                            'd/m/Y H:i');
                    }
                }
            }
        }

        $this->getResponse()->appendBody(json_encode($retorno));
    }

    public function pesquisaGradeAlunoPerfilAction()
    {

        $this->_helper->viewRenderer->setNoRender(true);
        $return = array();
        try {

            $matricula = new \G2\Negocio\Matricula();
            $param['id_matricula'] = $this->getParam('id_matricula');
            $param['id_categoriasala'] = $this->getParam('id_categoriasala');
            $vws = $matricula->findByVwGradeNota($param);

            foreach ($vws as $key => $vw) {
                if ($vw instanceof \G2\Entity\VwGradeNota) {
                    $return[$key]['id_saladeaula'] = $vw->getId_saladeaula();
                    $return[$key]['st_saladeaula'] = $vw->getSt_saladeaula();
                    $return[$key]['id_disciplina'] = $vw->getId_disciplina();
                    $return[$key]['dt_abertura'] = $vw->getDt_abertura() instanceof DateTime
                        ? $vw->getDt_abertura()->format("d/m/Y") : $vw->getDt_abertura();
                    $return[$key]['dt_encerramento'] = $vw->getDt_encerramento() instanceof DateTime
                        ? $vw->getDt_encerramento()->format("d/m/Y") : $vw->getDt_encerramento();
                    $return[$key]['id_projetopedagogico'] = $vw->getId_projetopedagogico();
                    $return[$key]['st_projetopedagogico'] = $vw->getSt_projetopedagogico();
                    $return[$key]['st_notatcc'] = is_null($vw->getSt_notatcc()) ? '-' : (int)$vw->getSt_notatcc();
                    $return[$key]['st_notaead'] = is_null($vw->getSt_notaead()) ? '-' : (int)$vw->getSt_notaead();
                    $return[$key]['st_notafinal'] = is_null($vw->getSt_notafinal()) ? '-' : (int)$vw->getSt_notafinal();
                    $return[$key]['st_disciplina'] = $vw->getSt_disciplina();
                    $return[$key]['id_tipodisciplina'] = $vw->getId_tipodisciplina();
                    $return[$key]['st_disciplina'] = $vw->getSt_tituloexibicaodisciplina();
                    $return[$key]['id_matricula'] = $vw->getId_matricula();
                    $return[$key]['nu_notafinal'] = is_null($vw->getNu_notafinal()) ? '-' : (int)$vw->getNu_notafinal();
                    $return[$key]['id_evolucao'] = $vw->getId_evolucao();
                    $return[$key]['st_evolucao'] = $vw->getSt_evolucao();
                    $return[$key]['id_situacao'] = $vw->getId_situacao();
                    $return[$key]['st_situacao'] = $vw->getSt_situacao();
                    $return[$key]['nu_cargahoraria'] = (int)$vw->getNu_cargahoraria() ? (int)$vw->getNu_cargahoraria() : ' - ';
                    $return[$key]['nu_notatotal'] = $vw->getNu_notatotal();
                    $return[$key]['st_status'] = $vw->getSt_status();
                    $return[$key]['st_statusdisciplina'] = $vw->getSt_statusdisciplina();
                    $return[$key]['bl_status'] = $vw->getBl_status();
                    $return[$key]['id_matriculadisciplina'] = $vw->getId_matriculadisciplina();
                    $return[$key]['id_avaliacaoconjuntoreferencia'] = $vw->getId_avaliacaoconjuntoreferencia();
                    $return[$key]['id_tiponotatcc'] = $vw->getId_tiponotatcc();
                    $return[$key]['id_tiponotafinal'] = $vw->getId_tiponotafinal();
                    $return[$key]['id_tiponotaead'] = $vw->getId_tiponotaead();
                    $return[$key]['st_notaatividade'] = number_format($vw->getSt_notaatividade(), 0);
                }
            }
        } catch (\Exception $e) {
            $return = array();
        }

        $this->getResponse()->appendBody(json_encode($return));

    }

    /**
     * Action para funcionalidade de visualizacao de materiais enviados ao aluno
     *
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     */
    public function materiaisAction()
    {
        $this->_helper->layout()->setLayout('layout');

        //Filtrando materiais enviados pela matricula e situacao entregue
        $where = array(
            'id_matricula' => Ead1_Sessao::getSessaoGeral()->id_matricula,
            'id_situacaopacote' => \G2\Constante\Situacao::TB_PACOTE_ENVIADO
        );

        $material = new \G2\Negocio\Material();
        $materiais = $material->findByVwEntregaMaterial($where, array('id_pacote' => 'ASC'));

        /**
         * Logica para organizar registros para aparecerem itens por pacote, agrupados
         * na tabela de forma separada, sem repeticao do id do pacote, somente
         * dos seus itens.
         */
        $current = NULL;
        $arrayMateriais = array();
        foreach ($materiais as $item) {
            $key = $item->getId_pacote();
            $arrayMateriais[$key]['id_pacote'] = $item->getId_pacote();

            if ($item->getDt_entrega()) {
                $arrayMateriais[$key]['dt_entrega'] = $item->getDt_entrega()->format('d-m-Y');
            }

            $arrayMateriais[$key]['st_codrastreamento'] = $item->getSt_codrastreamento();

            if ($item->getId_pacote() == $current) {
                $arrayMateriais[$key]['disciplinas'][] = $item->getSt_disciplina();
                $arrayMateriais[$key]['itens'][] = $item->getSt_itemdematerial();
            } else {
                $arrayMateriais[$key]['disciplinas'] = array($item->getSt_disciplina());
                $arrayMateriais[$key]['itens'] = array($item->getSt_itemdematerial());
            }

            $current = $item->getId_pacote();
        }

        $this->view->materiais = $arrayMateriais;
    }
}

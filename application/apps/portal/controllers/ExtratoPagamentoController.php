<?php
/**
 * Created by vinicius.alcantara@unyleya.com.br
 * Date: 19/10/2015
 * Controller para realizar a busca dos dados do extrato de pagamento do professor no banco
 */

class Portal_ExtratoPagamentoController extends PortalBase
{
    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {

    }

    public function retornarExtratoPagamentoAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        //$id_professor = Ead1_Sessao::getSessaoUsuario()->id_usuario;
        $id_professor = 185;
        try{
            $bo = new SecretariaBO();
            $vw = new VwEncerramentoSalasTO();
            if (isset($id_professor)) {
                $vw->setId_usuarioprofessor($id_professor);
            }
            $return = $bo->retornarVwEncerramentoSalas_Pagos($vw,false);

            if($return==false){
                return false;
            }

            $arReturn   = $return->getMensagem();

            foreach ($arReturn as $k => $vw) {
                $arReturn[$k] = $vw;
            }
        }catch(Exception $e){
            throw $e;
        }
        $this->getResponse()->appendBody(Zend_Json::encode($arReturn));
    }

    public function retornarExtratoMesAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $mes = $_GET['mes'];
        $ano = $_GET["ano"];
        //$id_professor = Ead1_Sessao::getSessaoUsuario()->id_usuario;
        $id_professor = 185;
        try{
            $bo = new SecretariaBO();
            $vw = new VwEncerramentoSalasTO();
            if (isset($id_professor)) {
                $vw->setId_usuarioprofessor($id_professor);
            }
            $return = $bo->retornarVwEncerramentoSalas_Pagos($vw,false);

            if($return==false){
                return false;
            }
            //$data = ''.'/'.$mes.'/'.$ano;
            $arReturn   = $return->getMensagem();
            $valor = 0;

            foreach ($arReturn as $k => $vw) {
                if ($vw->getSt_encerramentofinanceiro()) {
                    $data = $vw->getSt_encerramentofinanceiro();
                    $comp = explode("/",$data);
                    if($mes == $comp[1] && $ano == $comp[2])
                    {
                        $nu_valorpago = $vw->getNu_valorpago();
                        $valor = $valor + $nu_valorpago;
                    }
                }
            }
        }catch(Exception $e){
            throw $e;
        }
        $this->getResponse()->appendBody(Zend_Json::encode(array('valor' => $valor)));
    }

    public function retornarExtratoMesCompletoAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $mes = $_GET['mes'];
        $ano = $_GET["ano"];
        //$id_professor = Ead1_Sessao::getSessaoUsuario()->id_usuario;
        $id_professor = 185;
        try{
            $bo = new SecretariaBO();
            $vw = new VwEncerramentoSalasTO();
            if (isset($id_professor)) {
                $vw->setId_usuarioprofessor($id_professor);
            }
            $return = $bo->retornarVwEncerramentoSalas_Pagos($vw,false);

            if($return==false){
                return false;
            }
            //$data = ''.'/'.$mes.'/'.$ano;
            $arReturn   = $return->getMensagem();
            $detalhes = array();
            foreach ($arReturn as $k => $vw) {
                if ($vw->getSt_encerramentofinanceiro()) {
                    $data = $vw->getSt_encerramentofinanceiro();
                    $comp = explode("/",$data);
                    if($mes == $comp[1] && $ano == $comp[2])
                    {
                        array_push($detalhes,$vw);
                    }
                }
            }
        }catch(Exception $e){
            throw $e;
        }
        $this->getResponse()->appendBody(Zend_Json::encode($detalhes));
    }
}
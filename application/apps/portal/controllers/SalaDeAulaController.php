<?php

use G2\Negocio\Alocacao;

/**
 * Classe com métodos da central de atenção
 */
class Portal_SalaDeAulaController extends PortalBase
{

//    private $avaliacao_bo;

    public function init()
    {
        parent::init();


        if ($this->_helper->FlashMessenger->hasMessages()) {
            $this->view->messages = $this->_helper->FlashMessenger->getMessages();
        }
    }

    /**
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @throws Zend_Exception
     */
    public function gestaoAction()
    {
        if (Ead1_Sessao::getSessaoPerfil()->id_perfil != 5 && Ead1_Sessao::getSessaoPerfil()->id_perfil != 53) {
            try {
                $this->_helper->layout->setLayout('layout');
//                if (!$this->_getParam('id_saladeaula')) {
                if (!preg_match('/^[0-9]+$/', $this->_getParam('id_saladeaula'))) {
                    throw new Zend_Exception('Sala de aula não informada.');
                }

                $this->view->id_saladeaula = $this->getParam('id_saladeaula');

                $sdaTO = new VwSalaConfiguracaoTO();
                $sdaTO->setId_saladeaula($this->_getParam('id_saladeaula'));
                $sdaTO->fetch(false, true, true);
                if (!$sdaTO->getSt_saladeaula()) {
                    throw new Zend_Exception('Sala de aula não encontrada.');
                }

                $vwAreaProjetoSalaTO = new VwAreaProjetoSalaTO();
                $vwAreaProjetoSalaTO->setId_saladeaula($sdaTO->getId_saladeaula());
                $vwAreaProjetoSalaTO->setId_perfilpedagogico(PerfilPedagogicoTO::COORDENADOR_DE_PROJETO_PEDAGOGICO);
                $salaBO = new SalaDeAulaBO();
                $projetos = $salaBO->retornarVwAreaProjetoSalaGestao($vwAreaProjetoSalaTO);

                if ($projetos->getTipo() == Ead1_IMensageiro::SUCESSO) {
                    $projetos = $projetos->getMensagem();
                    $this->view->projetos = $projetos;
                    if ($projetos != null) {

                        $salaTO = new VwAlunosSalaTO();
                        $salaTO->setId_saladeaula($sdaTO->getId_saladeaula());

                        $menAlunos = $salaBO->retornarVwAlunosSala($salaTO);
                        $negocio = new \G2\Negocio\Negocio();

                        if ($menAlunos->getTipo() == Ead1_IMensageiro::SUCESSO) {
                            foreach ($menAlunos->getMensagem() as $key => $alunos) {
                                if (!empty($alunos->getDt_inicio())) {
                                    $alunos->setDt_inicio($negocio->converterData($alunos->getDt_inicio(), 'd/m/Y'));
                                }
                                if (!empty($alunos->getDt_termino())) {
                                    $alunos->setDt_termino($negocio->converterData($alunos->getDt_termino(), 'd/m/Y'));
                                }
                            }
                        }
                        $this->view->alunos = $menAlunos;
                    }
                }

                $this->view->sala = $sdaTO;
            } catch (Exception $e) {
                $this->view->erro = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
            }
        } else {
            $this->redirect('/portal/index/principal');
        }
    }

    /**
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @throws Zend_Exception
     */
    public function extenderAction()
    {

        try {
            if (!$this->_getParam('id_saladeaula')) {
                $this->_helper->flashMessenger->addMessage("
            <div class='info-box red yellow ocorrencia-box'>
                    <i class='icon-warning-sign'></i>
                <div class='msg'>
                Sala de Aula não informada.
                </div>
                <a href='javascript:void(0)' class='info-box-remove'><i class='icon-remove'></i></a>
            </div>");
                $this->redirect('/portal/index/principal');
            }
            if (!$this->_getParam('nu_diasextensao')) {
                $this->_helper->flashMessenger->addMessage("
            <div class='info-box red yellow ocorrencia-box'>
                    <i class='icon-warning-sign'></i>
                <div class='msg'>
                Quantidade de dias para extensão não informados.
                </div>
                <a href='javascript:void(0)' class='info-box-remove'><i class='icon-remove'></i></a>
            </div>");
                $this->redirect('/portal/index/principal');
            }

            $id_saladeaula = $this->_getParam('id_saladeaula');
            $nu_diasextensao = $this->_getParam('nu_diasextensao');
            $id_disciplina = $this->_getParam('id_disciplina');

            $to = new SalaDeAulaTO();
            $to->setId_saladeaula($id_saladeaula);
            $to->setnu_diasextensao($nu_diasextensao);

            $salaBO = new SalaDeAulaBO();
            $salaBO->editarSalaDeAula($to);

            $this->_helper->flashMessenger->addMessage("
            <div class='info-box green uny ocorrencia-box'>
                    <i class='icon-warning-sign'></i>
                <div class='msg'>
                Curso estendido com sucesso
                </div>
                <a href='javascript:void(0)' class='info-box-remove'><i class='icon-remove'></i></a>
            </div>");
            $this->redirect('/portal/sala-de-aula/gestao?id_disciplina=' . $id_disciplina . '&id_saladeaula=' . $id_saladeaula);

        } catch (Zend_Exception $e) {
            $this->_helper->flashMessenger->addMessage("
            <div class='info-box red uny ocorrencia-box'>
                    <i class='icon-warning-sign'></i>
                <div class='msg'>
                Erro na aplicação, por favor, entre em contato com o administrador do sistema.
                </div>
                <a href='javascript:void(0)' class='info-box-remove'><i class='icon-remove'></i></a>
            </div>");
            $this->redirect('/portal/sala-de-aula/gestao?id_disciplina=' . $id_disciplina . '&id_saladeaula=' . $id_saladeaula);
        }
    }

    /**
     * Abre tela para encerramento da sala de aula e repasse dos alunos pelo professor para o coordenador
     * @throws Zend_Exception
     */
    public function encerrarTccAction()
    {
        try {
            if (!$this->_getParam('id_saladeaula')):
                throw new Zend_Exception('Sala de Aula não informada!');
            endif;
            $salaBo = new PortalSalaDeAulaBO();
            $to = new VwSalaConfiguracaoTO();
            $to->setId_saladeaula($this->_getParam('id_saladeaula'));
            $this->view->dados = $salaBo->retornaVwSalaConfiguracao($to)->getFirstMensagem();
            $this->view->fla_encerramento = $this->_getParam('fla_encerramento');
        } catch (Zend_Exception $e) {
            $this->erro = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Abre tela para encerramento da sala de aula e repasse dos alunos pelo professor para o coordenador
     * @throws Zend_Exception
     */
    public function encerrarAction()
    {
        try {
            if (!$this->_getParam('id_saladeaula')) {
                $this->_helper->flashMessenger->addMessage("
            <div class='info-box red yellow ocorrencia-box'>
                    <i class='icon-warning-sign'></i>
                <div class='msg'>
                Sala de Aula não informada.
                </div>
                <a href='javascript:void(0)' class='info-box-remove'><i class='icon-remove'></i></a>
            </div>");
                $this->redirect('/portal/index/principal');
            }

            $id_saladeaula = $this->_getParam('id_saladeaula');
            $id_disciplina = $this->_getParam('id_disciplina');

            $salaBo = new PortalSalaDeAulaBO();
            $to = new VwSalaConfiguracaoTO();
            $to->setId_saladeaula($id_saladeaula);
            $this->view->dados = $salaBo->retornaVwSalaConfiguracao($to)->getFirstMensagem();

//            $this->_helper->flashMessenger->addMessage("
//            <div class='info-box green uny ocorrencia-box'>
//                    <i class='icon-warning-sign'></i>
//                <div class='msg'>
//                Curso encerrado com sucesso.
//                </div>
//                <a href='javascript:void(0)' class='info-box-remove'><i class='icon-remove'></i></a>
//            </div>");
//            $this->redirect('/portal/sala-de-aula/gestao?id_disciplina=' . $id_disciplina . '&id_saladeaula=' . $id_saladeaula);

        } catch (Zend_Exception $e) {
            $this->erro = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Função que salva o encerramento de uma sala de aula e verifica as suas justificativas
     * @throws Zend_Exception
     */
    public function salvarencerramentoAction()
    {
        $this->ajax();

        $mensageiro = new Ead1_Mensageiro();
        try {
            if (!$this->_getParam('st_justificativaacima') || !$this->_getParam('st_justificativaabaixo') || !$this->_getParam('st_pontosnegativos') ||
                !$this->_getParam('st_pontospositivos') || !$this->_getParam('st_pontosmelhorar') || !$this->_getParam('st_pontosresgate') ||
                !$this->_getParam('st_conclusoesfinais')
            ) {
                $mensageiro->setMensageiro('Dados inválidos', Ead1_IMensageiro::ERRO);
                die(Zend_Json::encode($mensageiro));
            }

            $salaTO = new SalaDeAulaTO();
            $salaTO->setId_saladeaula($this->_getParam('id_saladeaula'));
            $salaTO->setSt_justificativaacima($this->_getParam('st_justificativaacima'));
            $salaTO->setSt_justificativaabaixo($this->_getParam('st_justificativaabaixo'));
            $salaTO->setSt_pontospositivos($this->_getParam('st_pontospositivos'));
            $salaTO->setSt_pontosnegativos($this->_getParam('st_pontosnegativos'));
            $salaTO->setSt_pontosmelhorar($this->_getParam('st_pontosmelhorar'));
            $salaTO->setSt_pontosresgate($this->_getParam('st_pontosresgate'));
            $salaTO->setSt_conclusoesfinais($this->_getParam('st_conclusoesfinais'));

            $salaBO = new SalaDeAulaBO();

            $retorno = $salaBO->editarSalaDeAula($salaTO);
            die(Zend_Json::encode($retorno));
        } catch (Zend_Exception $e) {
            $mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
            die(Zend_Json::encode($mensageiro));
        }
    }

    /**
     * Retorna os alunos para o encerramento
     * @throws Zend_Exception
     */
    public function listaalunosencerramentoAction()
    {
        if (!$this->_getParam('id_saladeaula')):
            throw new Zend_Exception('Sala de Aula não informada!');
        endif;
        $to = new VwEncerramentoAlunosTO();
        $to->setId_saladeaula($this->_getParam('id_saladeaula'));
        $bo = new PortalSalaDeAulaBO();
        $resposta = $bo->retornaAlunosEncerramento($to, $this->_getParam('id_liberar'));

        if ($resposta->getTipo() == Ead1_IMensageiro::SUCESSO)
            $resposta->subtractMensageiro();

        $salaTO = new VwSalaDeAulaTO();
        $salaTO->setId_saladeaula($this->_getParam('id_saladeaula'));
        $salaTO->fetch(false, true, true);

        foreach ($resposta->getMensagem() as $aluno) {
            $aluno->isSalaAntiga = $this->_helper->isSalaAntiga($aluno->situacaotcc);
        }

        $this->view->id_liberar = $this->_getParam('id_liberar');
        $this->view->sala = $salaTO;
        $this->view->retorno = $resposta->getTipo();
        $this->view->alunos = $resposta->getMensagem();
        $this->view->fla_encerramento = $this->_getParam('fla_encerramento');

        if ($this->_getParam('id_liberar') == VwEncerramentoAlunosTO::PAGAMENTO_RECUSADO) {
            $this->renderScript('sala-de-aula/pagamentosrecusados.phtml');
        }
    }

    /**
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * Modificacoes em 07/08/2013 por Rafael Bruno (RBD) <rafaelbruno.ti@gmail.com>
     * Modificacoes em 01/07/2014 por Felipe Pastor <felipe.pastor@unyleya.com.br>
     */
    public function salvarNotaTccAction()
    {

        $this->_helper->viewRenderer->setNoRender(true);

        $bo = new PortalTCCBO();
        $arquivo = null;

        //Obtendo dados do POST
        $post = $this->_request->getPost();

        //Obtendo informacoes do arquivo enviado para upload
        if (isset($_FILES['arquivo_tcc']['tmp_name'])) {
            $arquivo = $_FILES['arquivo_tcc'];
        } elseif ($post['id_upload'] != null) {
            $arquivo = $post['id_upload'];
        }

        if ($arquivo == null) {
            if ($post['nu_notaparametro'][$post['id_parametrotcc']] >= $post['nu_notaminima']) {
                $this->_helper->json(new Ead1_Mensageiro('Nenhum arquivo postado. A Nota  não pode ser maior ou igual a ' . $post['nu_notaminima'] . '.', Ead1_IMensageiro::ERRO));
                return false;
            }
        }

        /**
         * Sets e fetch utilizado para obter as informacoes referentes
         * ao TCC enviado pelo aluno para o professor.
         */
        $aaTO = new AvaliacaoAlunoTO();
        $aaTO->setId_matricula($post['id_matricula']);
        $aaTO->setId_upload($post['id_upload']);
        $aaTO->setId_avaliacaoconjuntoreferencia($post['id_avaliacaoconjuntoreferencia']);
        $aaTO->setId_avaliacao($post['id_avaliacao']);
        $aaTO->fetch(false, true, true);

        //Adicionando ao POST atributo necessário para salvar avaliacao
        $post['st_tituloavaliacao'] = $aaTO->getSt_tituloavaliacao();

        if ($arquivo == $post['id_upload'])
            $arquivo = null;

        //Executando metodo de salvamento de nota
        $me = $bo->salvarArrNotaTCC($post, $arquivo);

        if ($this->getRequest()->getParam('id_situacaotcc') == \G2\Constante\Situacao::TB_ALOCACAO_EM_ANALISE) {
            $parametrosDeBusca = [
                'id_alocacao' => $this->getRequest()->getParam('id_alocacao'),
            ];
            (new Alocacao)->atualizaSituacaoTcc($parametrosDeBusca, $this->getRequest()->getParam('id_situacaotcc'));
        }

        $this->_helper->json($me);

    }

    /**
     * lançamento de Nota do TCC
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     */
    public function lancarNotaTccAction()
    {

        $id_saladeaula = $this->_getParam('id_saladeaula', false);
        $id_matricula = $this->_getParam('id_matricula', false);

        if (!$id_saladeaula || !$id_matricula) {
            $this->view->erro = new Ead1_Mensageiro('Dados inválidos', Ead1_IMensageiro::ERRO);
        } else {
            $this->view->id_saladeaula = $id_saladeaula;
            $this->view->id_matricula = $id_matricula;

            $to = new VwSalaDeAulaTO();
            $to->setId_saladeaula($id_saladeaula);
            $to->fetch(false, true, true);
            $this->view->sala = $to;

            $salaTO = new VwAlunosSalaTO();
            $salaTO->setid_matricula($id_matricula);
            $salaTO->setId_saladeaula($id_saladeaula);
            $salaTO->fetch(false, true, true);
            $this->view->alunosala = $salaTO;
            $this->view->id_usuario = $salaTO->getId_usuario();

            //Parametros
            $bo = new TCCBO();
            $tccto = new VwAvaliacaoParametroTCCTO();
            $tccto->setId_matricula($salaTO->getId_matricula());
            $tccto->setId_saladeaula($id_saladeaula);
            $parametros = $bo->retornaVwAvaliacaoParametroTCC($tccto);

            if ($parametros->getTipo() != Ead1_IMensageiro::SUCESSO) {
                $this->view->erro = $parametros;
            } else {
                $this->view->parametros = $parametros->getMensagem();

                /**
                 * Método que retorna a avaliação refatorado para pegar o ultimo registro da avaliação aluno,
                 * garantindo que por mais registros de envio existam sempre vai pegar o ultimo enviado.
                 * @author Helder Fernandes <helder.silva@unyleya.com.br>
                 * todo: Refatorar método de envio para inativar as outras avaliações quando for reenviado.
                 */

                $negocio = new \G2\Negocio\Negocio();
                $objAvaliacaoAluno = $negocio->findOneBy('\G2\Entity\AvaliacaoAluno',
                    array('id_matricula' => $negocio->getReference('\G2\Entity\Matricula', $salaTO->getId_matricula()),
                        'id_avaliacao' => $negocio->getReference('\G2\Entity\Avaliacao', $parametros->getFirstMensagem()->getId_avaliacao()),
                        'id_situacao' => $negocio->getReference('\G2\Entity\Situacao', AvaliacaoAlunoTO::ATIVO)), array('dt_cadastro' => 'desc'));

                $this->view->avaliacao = $objAvaliacaoAluno;

                if ($objAvaliacaoAluno && $objAvaliacaoAluno->getId_upload()) {
                    $up = new UploadTO();
                    $up->setId_upload($objAvaliacaoAluno->getId_upload());
                    $up->fetch(true, true, true);
                    $this->view->stUpload = $up->getSt_upload();
                    $this->view->idUpload = $up->getId_upload();
                }

                $vwencerramento = new VwEncerramentoAlunosTO();
                $vwencerramento->setId_matricula($salaTO->getId_matricula());
                $vwencerramento->setId_saladeaula($salaTO->getId_saladeaula());
                $vwencerramento->fetch(false, true, true);

                $this->view->encerrada = false;
                if ($vwencerramento->getDt_encerramentoprofessor()) {
                    $this->view->encerrada = true;;
                }
            }

            // BUSCAR TRAMITES RELACIONADOS AO TCC
            $tramiteNegocio = new \G2\Negocio\Tramite();
            // TipoTramite = 15 = TCC
            $mensageiro = $tramiteNegocio->retornarTramiteSp(CategoriaTramiteTO::MATRICULA, array(0 => $id_matricula), 15);

            $this->view->tramites = array();
            if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
                foreach ($mensageiro->getMensagem() as $tramite) {
                    $tramite->setSt_tramite(nl2br($tramite->getSt_tramite()));
                    $this->view->tramites[] = $tramite->toBackboneArray();
                }
            }
        }
    }

    /**
     * Salva o repasse do professor para o coordenador
     * Modificado em 16-08-2014 por: @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com>
     */
    public function repassarprofessorAction()
    {
        $bo = new PortalSalaDeAulaBO();
        $mensageiro = new Ead1_Mensageiro();

        $salaDeAula = new \SalaDeAulaTO();
        $salaDeAula->setId_entidade($salaDeAula->getSessao()->id_entidade);
        $salaDeAula->setId_saladeaula($this->_getParam('id_saladeaula'));
        $salaDeAula->fetch(false, true, true);
        $mensageiro->setMensagem($salaDeAula);

        //Condicao para o caso de a sala ser do tipo permanente ou a disciplina do tipo TCC
        if ($this->_getParam('id_tiposaladeaula') == TipoSalaDeAulaTO::SALA_PERMANENTE || $this->_getParam('id_tipodisciplina') == TipoDisciplinaTO::TCC) {

            if ($this->_getParam('id_tiposaladeaula') == TipoSalaDeAulaTO::SALA_PERMANENTE && $this->getRequest()->getParam('id_alocacao') == NULL) {
                $retorno = new Ead1_Mensageiro('Nenhum aluno selecionado para o repasse.', Ead1_IMensageiro::ERRO);
            } else {
//                foreach ($this->getRequest()->getParam('id_alocacao') as $value) {
//                    $encerramentoAlocacao = new EncerramentoAlocacaoTO();
//                    $encerramentoAlocacao->setId_alocacao($value);
//                    $encerramentoAlocacao->fetch(false, true, true);

//                    if (!$encerramentoAlocacao->getId_encerramentosala()) {
                $retorno = $bo->repasseProfessorSalaPermanente($this->getAllParams());
//                    } else {
//                        $mensageiro->setTipo(0);
//                        $mensageiro->setText('Este aluno já se encontra encerrado');
//                        $retorno = $mensageiro;
//                    }
//                }
            }

        } else {
            if (!is_null($salaDeAula->getDt_encerramento()) && strtotime('now') < strtotime($salaDeAula->getDt_encerramento()->toString('d-M-Y'))) {
                $mensageiro->setTipo(0);
                $mensageiro->setText('Esta sala não pode ser repassada, esta se encontra aberta.');
                $retorno = $mensageiro;
            } else {
                $retorno = $bo->repasseProfessorSalaPeriodica($this->getAllParams());
            }
        }

        $this->_helper->json($retorno);
    }

    public function abrirsincronizarAction()
    {
        try {

            $bo = new PortalSalaDeAulaBO();
            if (!$this->_getParam('id_saladeaula')) {
                throw new Zend_Exception('o id_saladeaula é obrigatório e não foi passado!');
            }
            if (!$this->_getParam('st_codsistemacurso')) {
                throw new Zend_Exception('o st_codsistemacurso é obrigatório e não foi passado!');
            }


            $salaDeAula = (new \G2\Negocio\Negocio())
                ->find(\G2\Entity\SalaDeAula::class, $this->_getParam('id_saladeaula'));

            if (!$salaDeAula) {
                throw  new Zend_Exception("Sala de aula não encontrada.");
            }

            $retorno = $bo->retornaAlunosNotaSincronizacao(
                $this->_getParam('st_codsistemacurso'),
                $this->_getParam('id_saladeaula'),
                $salaDeAula->getId_entidade()
            );

            if ($retorno->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Exception($retorno->getFirstMensagem());
            }

            $arrRetorno = $retorno->getMensagem();
            //recebe as informações das interações do moodle dos alunos
            $alunosParaSincronizar = $retorno->getMensagem();

            //retirando da lista de sincronização os alunos que só enviaram o arquivo, ainda não receberam nota
            foreach ($alunosParaSincronizar as $key => $row){
                if(empty($row->getSt_notaava())) {
                    unset($alunosParaSincronizar[$key]);
                }
            }

            //setando o novo array no mensageiro
            $retorno->setMensagem($alunosParaSincronizar);

            $this->view->retorno = $retorno;
            $this->view->alunos = $retorno->getMensagem();
            $this->view->id_saladeaula = $this->_getParam('id_saladeaula');
            $this->view->st_codsistemacurso = $this->_getParam('st_codsistemacurso');
            $this->view->id_tipoavaliacao = $arrRetorno[0]->id_tipoavaliacao;

            // Buscar a evolucao da matricula
            $negocio = new \G2\Negocio\Negocio();
            foreach ($this->view->alunos as &$aluno) {
                $aluno->id_evolucaomatricula = $negocio->find('\G2\Entity\Matricula', $aluno->id_matricula)
                    ->getId_evolucao()->getId_evolucao();
                $aluno->bl_aproveitamento = $aluno->id_tiponota == \G2\Constante\TipoNota::APROVEITAMENTO;
                $aluno->bl_evolucao = in_array($aluno->id_evolucaomatricula, array(
                    \G2\Constante\Evolucao::TB_MATRICULA_CONCLUINTE,
                    \G2\Constante\Evolucao::TB_MATRICULA_CERTIFICADO
                ));
            }
        } catch (Exception $e) {
            $this->view->retorno = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Sincroniza as notas
     */
    public function sincronizarAction()
    {
        $this->ajax();
        if ($this->getRequest()->isPost()) {
            $bo = new PortalSalaDeAulaBO();
            $retorno = $bo->sincronizarNotas($this->_request->getPost());
            echo(Zend_Json::encode($retorno));
        }
    }

    public function liberarTccAction()
    {
        try {

            if (!$this->_getParam('id_saladeaula')) {
                throw new Zend_Exception('o id_saladeaula é obrigatório e não foi passado!');
            }
            $id_saladeaula = $this->_getParam('id_saladeaula');
            $bo = new SalaDeAulaBO();

            $vw_alunosTo = new VwAlunosSalaTO();
            $vw_alunosTo->setId_entidade(Ead1_Sessao::getSessaoEntidade()->id_entidade);
            $vw_alunosTo->setId_saladeaula($id_saladeaula);

            $retorno = $bo->retornarVwAlunosSala($vw_alunosTo);
            if ($retorno->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Exception($retorno->getFirstMensagem());
            }

            $to = new VwSalaConfiguracaoTO();
            $to->setId_saladeaula($id_saladeaula);
            $salaBo = new PortalSalaDeAulaBO();
            $this->view->dados = $salaBo->retornaVwSalaConfiguracao($to)->getFirstMensagem();

            $this->view->retorno = $retorno;
            $this->view->alunos = $retorno->getMensagem();
            $this->view->id_saladeaula = $this->_getParam('id_saladeaula');

            if (!$this->getRequest()->isXmlHttpRequest()):
                $this->_helper->layout->setLayout('layout');
            endif;
        } catch (Exception $e) {

            $this->view->retorno = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    public function salvarliberacaotccAction()
    {

        $this->ajax();

        $mensageiro = new Ead1_Mensageiro();

        try {
            if (!$this->_getParam('id_saladeaula')):
                throw new Zend_Exception('Sala de Aula não informada!');
            endif;
            if (!$this->_getParam('id_alocacao')):
                throw new Zend_Exception('Nenhum aluno informado!');
            endif;
            $bo = new SalaDeAulaBO();
            $retorno = $bo->liberarSituacaoTcc($this->getAllParams());
            echo(Zend_Json::encode($retorno));
        } catch (Zend_Exception $e) {
            $mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
            echo(Zend_Json::encode($mensageiro));
        }
    }

    public function cancelarliberacaotccAction()
    {

        $id_avaliacaoaluno = $this->_getParam('id_avaliacaoaluno');

        $salaTO = new VwAlunosSalaTO();
        $salaTO->setid_matricula($this->_getParam('id_matricula'));
        $salaTO->setId_saladeaula($this->_getParam('id_saladeaula'));
        $salaTO->fetch(false, true, true);
        $this->view->alunosala = $salaTO;
        $this->view->avaliacaoaluno = $id_avaliacaoaluno;

        if ($this->_request->isPost()) {
            $this->ajax();
            $post = $this->_request->getPost();

            $mensageiro = new Ead1_Mensageiro();
            try {
                if (!$post['id_saladeaula']) {
                    throw new Zend_Exception('Sala de Aula não informada!');
                }
                if (!$post['id_alocacao']) {
                    throw new Zend_Exception('Nenhum aluno informado!');
                }

                $bo = new SalaDeAulaBO();

                $retorno = $bo->cancelarSituacaoTcc($post);

                echo $this->_helper->json($retorno);
            } catch (Zend_Exception $e) {
                $mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
                echo Zend_Json::encode($mensageiro);
            }
        }
    }

    /**
     * Rotina criada com o intuito de facilitar a execução de atualizacao de
     * dados de TCC dos alunos anteriores a existencia da rotina de TCC.
     * Base: Historia AC-543 - Academico.
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     * @param array $id_alocacao .
     */
    public function rotinaAtualizaDadosTccAction()
    {
        $this->ajax();
        $bo = new PortalSalaDeAulaBO();

        $arrayAlunos['id_alocacao'] = array(
            4151, 5632, 6345, 6757, 8309, 10399, 13854, 14884, 16657, 16667,
            16737, 16797, 16817, 17418, 17518, 17850, 19060, 20392, 29611, 33251,
            20442, 20727, 22737, 22746, 22872, 26157, 28049, 33525, 33588, 34070,
            35886, 36659, 38104, 38218, 38394, 38903, 39499, 40225, 41178, 50441,
            53667, 53956, 57901, 62317, 74684, 104085, 16687, 16717, 16727, 28892,
            28905, 30987, 32550, 33300, 33731, 34377, 38126, 39127, 43045, 45567,
            47396, 48946, 50389, 69409, 82691);

        //Parametros necessarios para execução dos processos.
        $this->_setParam('id_liberar', 1);

        $result = $bo->repasseProfessorSalaPermanente($arrayAlunos);

        if ($result) {
            $msg = $result->getMensagem();
            echo $msg[0];
        }
    }

    public function devolverTccAction()
    {
        try {

            $params = $this->getAllParams();
            $ngAvaliacao = new \G2\Negocio\Avaliacao();

            $ngAvaliacao->beginTransaction();

            if (!$params['id_avaliacaoaluno']) {
                throw new \Exception("Id da Avaliacao do Aluno não foi informado.");
            }

            $arrAvaliacaoAluno = array(
                'id_situacao' => \G2\Constante\Situacao::TB_AVALIACAOALUNO_ATIVA,
                'id_avaliacaoaluno' => $params['id_avaliacaoaluno'],
                'st_justificativa' => $params['st_motivodevolucao'],
                'bl_ativo' => 1
            );

            //Salva ou edita os dados da avaliacaoaluno
            if (!$ngAvaliacao->salvarAvaliacaoAluno($arrAvaliacaoAluno) instanceof \G2\Entity\AvaliacaoAluno) {
                throw new \Exception("Não foi possível salvar a avaliação do aluno.");
            }

            if (!$params['id_alocacao']) {
                throw new \Exception("Id da Alocação não foi informado.");
            }

            $ngAlocacao = new \G2\Negocio\Alocacao();
            $parametrosDeBusca = array('id_alocacao' => $params['id_alocacao']);

            // atualiza a situacao do TCC
            if (!$ngAlocacao->atualizaSituacaoTcc(
                $parametrosDeBusca, \G2\Constante\Situacao::TB_ALOCACAO_DEVOLVIDO_AO_ALUNO)
            ) {

                throw new \Exception("Não foi possível atualizar a situação do TCC.");
            }

            if (!$params['id_matricula']) {
                throw new \Exception("Id da matricula não foi informado.");
            }

            $st_tramite = 'Devolvido ao aluno pelo orientador.';

            //Se tiver um motivo de devolução concatena com o st_tramite acima
            if (isset($params['st_motivodevolucao']) && trim($params['st_motivodevolucao']) != '')
                $st_tramite .= (' Motivo: ' . $params['st_motivodevolucao']);

            //salva tramite da devolução do TCC ao aluno
            $ngTramite = new \G2\Negocio\Tramite();
            if (!$ngTramite->salvarTramiteMatricula($st_tramite, $params['id_matricula'],
                    \G2\Constante\TipoTramite::MATRICULA_TCC) instanceof \G2\Entity\TramiteMatricula
            ) {

                $tipo = 1;
                $texto = 'Situação atualizada com sucesso. Não foi possível salvar tramite.';
            } else {
                $tipo = 1;
                $texto = 'Situação atualizada com sucesso';
            }

            $ngAvaliacao->commit();

        } catch (\Exception $e) {
            $ngAvaliacao->rollback();

            $tipo = 2;
            $texto = $e->getMessage();
        }

        $this->_helper->json([
            'tipo' => $tipo,
            'text' => $texto
        ]);
    }

    public function tramitesEncerramentoTccAction()
    {

        try {
            if (!$this->_getParam('id_saladeaula'))
                throw new Exception('Sala de Aula não informada.');

            $to = new VwEncerramentoAlunosTO();
            $to->setId_saladeaula($this->_getParam('id_saladeaula'));
            $bo = new PortalSalaDeAulaBO();
            $resposta = $bo->retornaAlunosEncerramento($to, $this->_getParam('id_liberar'));

            if ($resposta->getTipo() != Ead1_IMensageiro::SUCESSO)
                throw new Exception('Nenhum trâmite encontrado.');

            $arrResult = $resposta->getMensagem();
            $arrTramites = array();

            foreach ($arrResult as $result) {
                if ($result->getId_situacaotcc() == \G2\Constante\Situacao::TB_ALOCACAO_DEVOLVIDO_AO_TUTOR) {

                    $enTramiteSala = new \G2\Entity\TramiteSalaDeAula();
                    $arrTramiteSala = $enTramiteSala->findBy(array(
                        'id_saladeaula' => $this->_getParam('id_saladeaula'),
                        'id_usuario' => $result->getId_usuario()), array('id_tramitesaladeaula' => 'DESC'));

                    if (!empty($arrTramiteSala)) {
                        foreach ($arrTramiteSala as $key => $tramiteSala) {
                            $arrTramites[$result->getId_usuario()][$key]['st_nomecompleto'] = $result->getSt_nomecompleto();
                            $arrTramites[$result->getId_usuario()][$key]['st_tramite'] = $tramiteSala->getId_tramite()->getSt_tramite();
                            $arrTramites[$result->getId_usuario()][$key]['dt_cadastro'] = \G2\Utils\Helper::converterData($tramiteSala->getId_tramite()->getDt_cadastro(), 'd/m/Y H:i:s');
                            $arrTramites[$result->getId_usuario()][$key]['st_usuariocadastro'] = $tramiteSala->getId_tramite()->getId_usuario()->getSt_nomecompleto();
                        }
                    }
                }
            }

            $this->view->tramites = $arrTramites;

        } catch (Exception $e) {
            $this->view->erro = $e->getMessage();
        }
    }
}


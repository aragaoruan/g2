<?php
/**
 * Classe de captura de erros
 * 
 * @author edermariano
 *
 * @package application
 * @subpackage controller
 */
class Portal_ErrorController extends Zend_Controller_Action
{

	public function errorAction()
    {
        $errors = $this->_getParam('error_handler');

        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
        
                // 404 error -- controller or action not found
                $this->getResponse()->setHttpResponseCode(404);
                $priority = Zend_Log::NOTICE;
                $this->view->message = 'Ops, ocorreu um erro: Página não encontrada.'.dirname('.');
                break;
            default:
                // application error
                $this->getResponse()->setHttpResponseCode(500);
            	$priority = Zend_Log::CRIT;
                $this->view->message = 'Ops, ocorreu um erro: Erro de aplicação.';
                
                
                break;
        }
        
        // Log exception, if logger available
        $log = $this->getLog();
        if ($log) {
            $log->log(
            		
            		$errors->exception->getMessage() . "\n" .
            				$errors->exception->getTraceAsString()
            		
            		, $priority, $errors->exception);
            $log->crit($this->view->message, $errors->exception);
        }
        
        // conditionally display exceptions
        if ($this->getInvokeArg('displayExceptions') == true) {
            $this->view->exception = $errors->exception;
        }
        
        $this->view->request   = $errors->request;
    }

    public function getLog()
    {
        $bootstrap = $this->getInvokeArg('bootstrap');
        if (!$bootstrap->hasPluginResource('Log')) {
            return false;
        }
        $log = $bootstrap->getResource('Log');
        return $log;
    }


}


<?php

/**
 * Controller de Senha (Unico) do sistema de Login Único
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 *
 */
class Unico_SenhaController extends UnicoBase {
	
	
	/* (non-PHPdoc)
	 * @see UnicoController::init()
	 */
	public function init(){
		parent::init();
	}


    /**
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     */
    public function indexAction(){
		exit;
    }
    
    
    /**
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     */
    public function recuperarAction(){
    	
    	
    	if($this->_getParam('st_cpf') && $this->_getParam('st_email')){
    		
	    	$l = new LoginUnicoBO();
	    	$r = $l->recuperarSenha($this->_getParam('st_cpf'), $this->_getParam('st_email'), $this->_getParam('entidade', 14));
	    	$this->view->mensagem = $r->getFirstMensagem();
    		
    	}
	    
    	$this->view->id_entidade = $this->_getParam('entidade', 14);
    	    	
    	
    }
    
    

}


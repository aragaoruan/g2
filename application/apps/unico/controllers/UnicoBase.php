<?php
/**
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 *
 */
class UnicoBase extends Ead1_Controller{
	
	protected $baseUrl;
	protected $baseUrlModulo;
	
	/**
	 * Método que inicializa as configurações gerais do ambiente
	 */
	public function init(){
		$this->_helper->layout->disableLayout();
		$config 				= Ead1_Ambiente::geral();
    	$this->view->flexFolder = $config->flexFolder;
    	$bootBaseUrl = $this->getInvokeArg('bootstrap')->getOption('baseUrl');
		$this->view->baseUrl = ($bootBaseUrl ? $bootBaseUrl : $config->baseUrl);
		if($this->_getParam('module') == 'unico'){
			$this->baseUrl		 = $this->view->baseUrl;
			$this->baseUrlModulo = $this->baseUrl . "/unico";
			$this->view->module = $this->_getParam('module'); 
			$this->view->baseUrlModulo = $this->baseUrlModulo;
		}
	}
	
}
<?php

/**
 * Controller de Index (Unico) do sistema de Login Único
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 *
 */
class Unico_IndexController extends UnicoBase {
	
	
	/* (non-PHPdoc)
	 * @see UnicoController::init()
	 */
	public function init(){
		parent::init();
	}


    /**
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     */
    public function indexAction(){
    	$this->view->id_entidade = $this->_getParam('entidade', 14);
    }
    
    /**
     * Método que vai Exibir os Links de Acesso
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     */
    public function acessarAction(){
    	
    	$post = $this->limpaGetAllParams($this->_getAllParams());
    	
    	$this->view->id_entidade 	= isset($post['entidade']) ? $post['entidade'] : 14;
    	
    	if(empty($post['st_usuario']) || empty($post['st_senha'])){
    		die('<script>alert(\'Usuário ou Senha não informados!\');location.replace(\''.$this->baseUrl.'/unico?entidade='.$this->view->id_entidade.'\');</script>');
    	}
    	
    	$pe = new LoginUnicoBO();
    	$mensageiro = $pe->verificarAcesso($post['st_usuario'], $post['st_senha'], $this->view->id_entidade);
    	
    	if($mensageiro->getTipo()!==Ead1_IMensageiro::SUCESSO){
    		die('<script>alert(\'Usuário ou Senha inválidos!\');location.replace(\''.$this->baseUrl.'/unico?entidade='.$this->view->id_entidade.'\');</script>');
    	} else {
    		$this->view->sistemas 		= $mensageiro->getMensagem();
    		$entidade = new EntidadeTO();
    		$entidade->setId_entidade($this->view->id_entidade);
    		$entidade->fetch(true, true, true);
    		$this->view->st_urlimglogo 	= $entidade->getSt_urlimglogo();
    	}
    	
    }
    
    public function testeAction(){
    	
    	exit;
    	$bo = new LoginUnicoBO();
    	$d = $bo->_recuperarSenhaPortalDoAluno('95963707315', 'chellypi22@gmail.com', 1);
    	Zend_Debug::dump($d,__CLASS__.'('.__LINE__.')');exit;
    	
    	$pe = new PessoaWebServices(1);
    	$a2 = $pe->retornarDadosLoginUnico('homologa', 'itto01');
    	Zend_Debug::dump($a2,__CLASS__.'('.__LINE__.')');exit;
    	
    }
        

}


<?php


/**
 * Classe com regras de negócio para o LoginUnico
 * @author Elcio Mauo Guimarães - <elcioguimaraes@gmail.com>
 * @since 18/09/2012
 * @package models
 * @subpackage bo
 */
class LoginUnicoBO extends Ead1_BO {

	

	/**
	 * Variável que contem a lista de métodos que devem ser executados no Login
	 * @var array
	 */
	protected $_metodos_login = array('_verificarAcessoActor2', '_verificarAcessoPortalDoAluno');
    
	/**
	 * Variável que contem a lista de métodos que devem ser executados no Recuperar Senha
	 * @var array
	 */
	protected $_metodos_recuperar = array('_recuperarSenhaActor2', '_recuperarSenhaPortalDoAluno');

	
	/**
	 * Executa a recuperação de senha de acesso para cada sistema integrado
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @param string $cpf
	 * @param string $email
	 * @return Ead1_Mensageiro
	 */
	public function recuperarSenha($st_cpf, $st_email, $id_entidade){
		$achou = false;
		foreach($this->_metodos_recuperar as $key => $metodo){
			$mensageiro = $this->{$metodo}($st_cpf, $st_email, $id_entidade);
			if($mensageiro->getTipo()==Ead1_IMensageiro::SUCESSO){
				$achou = true;
			}elseif ($mensageiro->getTipo() == Ead1_IMensageiro::ERRO){
				return $mensageiro;
			}
		}
		
		if($achou){
			return new Ead1_Mensageiro('Usuário e senha recuperados com sucesso!', Ead1_IMensageiro::SUCESSO);
		}
		return new Ead1_Mensageiro('Não foi possível encontrar os dados de acesso!', Ead1_IMensageiro::AVISO);

	}
	
	/**
	 * Método que solicita a recuperação de senha do Portal do Aluno
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @param string $cpf
	 * @param string $email
	 * @return Ead1_Mensageiro
	 */
	public function _recuperarSenhaPortalDoAluno($st_cpf, $st_email, $id_entidade){
		try {
			$vwPessoaTO = new VwPessoaTO();
			$vwPessoaTO->setId_entidade($id_entidade ? $id_entidade : null);
    		$vwPessoaTO->setSt_cpf($st_cpf ? $st_cpf : null);
    		$vwPessoaTO->setSt_email($st_email ? $st_email : null);
    		
			$loginBO = new LoginBO();
			$mensageiro = $loginBO->recuperarSenhaPessoa($vwPessoaTO);
			
			if($mensageiro->getTipo()==Ead1_IMensageiro::SUCESSO){
				return new Ead1_Mensageiro("Portal do Aluno: Fique de Olho na sua Caixa de Entrada, o Email com seus Dados de Acesso Deve Chegar em Breve.", Ead1_IMensageiro::SUCESSO);
			} else {
				throw new Zend_Exception($mensageiro->getFirstMensagem());
			}
			
		}catch (Zend_Validate_Exception $e){
			return new Ead1_Mensageiro('Portal do Aluno: Usuário não encontrado no Portal do Aluno', Ead1_IMensageiro::AVISO);
		}catch (Exception $e){
			return new Ead1_Mensageiro('Portal do Aluno: Erro ao Recuperar os Dados de Acesso', Ead1_IMensageiro::ERRO, $e->getMessage());
		}
		
	}
	
	
	
	/**
	 * Método que solicita a recuperação de senha do Actor 2
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @param string $cpf
	 * @param string $email
	 * @return Ead1_Mensageiro
	 */
	private function _recuperarSenhaActor2($st_cpf, $st_email, $id_entidade){
		
		$pe = new PessoaWebServices($id_entidade);
		$a2 = $pe->recuperarSenha($st_cpf, $st_email);
		if($a2->getTipo()==Ead1_IMensageiro::SUCESSO){
			return new Ead1_Mensageiro('Actor 2: Dados recuperados com sucesso!', Ead1_IMensageiro::SUCESSO);
		} else {
			return new Ead1_Mensageiro('Actor 2: Não foi possível encontrar os dados de acesso!', Ead1_IMensageiro::AVISO);
		}

	}
	
	
	
	
	/**
	 * Variável que vai ter todas as opções de acesso
	 * @var array
	 */
	protected $_acessos_disponiveis = array();
	

	/**
	 * Executa a busca por dados de acesso para cada sistema integrado
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @param string $st_usuario
	 * @param string $st_senha
	 * @param int $id_entidade
	 * @return Ead1_Mensageiro
	 */
	public function verificarAcesso($st_usuario, $st_senha, $id_entidade){
		
		foreach($this->_metodos_login as $metodo){
			$this->{$metodo}($st_usuario, $st_senha, $id_entidade);
		}
		if(empty($this->_acessos_disponiveis)){
			return new Ead1_Mensageiro('Nenhum perfil encontrado!', Ead1_IMensageiro::AVISO);
		} else {
			return new Ead1_Mensageiro($this->_acessos_disponiveis, Ead1_IMensageiro::SUCESSO);
		}
		
	}
	
	/**
	 * Método que verifica o acesso ao Actor 2
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @param string $st_usuario
	 * @param string $st_senha
	 * @param int $id_entidade
	 */
	private function _verificarAcessoActor2($st_usuario, $st_senha, $id_entidade){
		
		$pe = new PessoaWebServices($id_entidade);
		$a2 = $pe->retornarDadosLoginUnico($st_usuario, $st_senha);
		if($a2->getTipo()==Ead1_IMensageiro::SUCESSO){
			$dados = $a2->getMensagem();
			if(!empty($dados['acesso'])){
				$this->_acessos_disponiveis['actor']['sistema'] = 'Actor 2';
				foreach($dados['acesso'] as $acesso){
					$luTO = new LoginUnicoTO();
					$luTO->setSt_nome($acesso['nompessoa']);
					
					if($acesso['codtipopess']==1 && $acesso['codperfil']==8){
						$luTO->setSt_titulo($acesso['codmatricula'].' - '.$acesso['destituloreferencia'].' -> '.$acesso['nomturma']);
					} else {
						$luTO->setSt_titulo($acesso['codmatricula'].' - '.$acesso['destituloreferencia']);
					}
					
					$luTO->setSt_perfil($acesso['destipopess'].' de '.$acesso['nomperfil']);
					
					$luTO->setSt_url($acesso['_link']);
					$luTO->setSt_nomesistema('Actor 2');
					$luTO->setSt_iconesistema(null);
					$this->_acessos_disponiveis['actor']['acesso'][] = $luTO;
				}
			}
		}
				
	} 	
	
	
	/**
	 * Método que verifica o Acesso ao Portal do Aluno
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @param string $st_usuario
	 * @param string $st_senha
	 * @param int $id_entidade
	 */
	private function _verificarAcessoPortalDoAluno($st_usuario, $st_senha, $id_entidade){
		
		
		$config 				= Ead1_Ambiente::geral();
		if(!$config->st_url_portal){
			return false;
		}
	
		
		$usTO = new VwUsuarioPerfilPedagogicoTO();
		$usTO->setId_entidade($id_entidade);
		$usTO->setSt_login($st_usuario);
		$usTO->setSt_senha($st_senha);
		$usTO->setId_perfilpedagogico(PerfilPedagogicoTO::ALUNO);
		
		$to = new LoginTO();
		$to->setSenha($st_senha);
		$to->setUsuario($st_usuario);

		$login = new LoginBO();
		$mensageiro = $login->retornaVwPessoaLogin($to, $usTO->getId_entidade());
		if($mensageiro->getTipo()!=Ead1_IMensageiro::SUCESSO){
			return false;
		}
		
		$orm = new VwUsuarioPerfilPedagogicoORM();
		$dados = $orm->consulta($usTO, false, false, array('st_tituloexibicao', 'st_saladeaula'));
		
		
		if(!empty($dados)){
			$this->_acessos_disponiveis['portal']['sistema'] = 'Portal do Aluno';
			foreach($dados as $acesso){
// 				$acesso = new VwUsuarioPerfilPedagogicoTO();
				$luTO = new LoginUnicoTO();
				$luTO->setSt_nome($acesso->getSt_nomecompleto());
				$luTO->setSt_perfil(  $acesso->getSt_nomeentidade().' - '.$acesso->getSt_nomeperfil());
				
				//Nº Matricula - Nome do curso de referencia ou projeto pedagogico
				$luTO->setSt_titulo($acesso->getId_matricula().' - '.$acesso->getSt_tituloexibicao());
				$luTO->setSt_url($config->st_url_portal.'/portal/login/autenticacao-direta?p='.$acesso->getId_usuario()
						.'&e='.$acesso->getId_entidade().'&u='.$acesso->getSt_urlacesso());
				$luTO->setSt_nomesistema('Portal do Aluno');
				$luTO->setSt_iconesistema(null);
				$this->_acessos_disponiveis['portal']['acesso'][] = $luTO;
			}
			
		}
		
	} 	
	

}
<?php 

/**
 * Classe de LoginUnicoTO
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage to
 */
 class LoginUnicoTO extends Ead1_TO_Dinamico {

	public $st_nomesistema;
	public $st_iconesistema;
	public $st_nome;
	public $st_perfil;
	public $st_url;
	public $st_titulo;
	/**
	 * @return the $st_nomesistema
	 */
	public function getSt_nomesistema() {
		return $this->st_nomesistema;
	}

	/**
	 * @return the $st_iconesistema
	 */
	public function getSt_iconesistema() {
		return $this->st_iconesistema;
	}

	/**
	 * @return the $st_nome
	 */
	public function getSt_nome() {
		return $this->st_nome;
	}

	/**
	 * @return the $st_perfil
	 */
	public function getSt_perfil() {
		return $this->st_perfil;
	}

	/**
	 * @return the $st_url
	 */
	public function getSt_url() {
		return $this->st_url;
	}

	/**
	 * @return the $st_titulo
	 */
	public function getSt_titulo() {
		return $this->st_titulo;
	}

	/**
	 * @param field_type $st_nomesistema
	 */
	public function setSt_nomesistema($st_nomesistema) {
		$this->st_nomesistema = $st_nomesistema;
	}

	/**
	 * @param field_type $st_iconesistema
	 */
	public function setSt_iconesistema($st_iconesistema) {
		$this->st_iconesistema = $st_iconesistema;
	}

	/**
	 * @param field_type $st_nome
	 */
	public function setSt_nome($st_nome) {
		$this->st_nome = $st_nome;
	}

	/**
	 * @param field_type $st_perfil
	 */
	public function setSt_perfil($st_perfil) {
		$this->st_perfil = $st_perfil;
	}

	/**
	 * @param field_type $st_url
	 */
	public function setSt_url($st_url) {
		$this->st_url = $st_url;
	}

	/**
	 * @param field_type $st_titulo
	 */
	public function setSt_titulo($st_titulo) {
		$this->st_titulo = $st_titulo;
	}

 	
 	
 	
}
<?php

/**
 * Helper que trata mensagens resultantes de Zend_Form e as retorna em formatos
 * JSON, String ou Array
 *
 * Metodo toArray, retorna mensagem em formato de array
 * Metodo toJson, retorna mensagens em formato de JSON
 * Metodo toString, recebe como parametro o formato de quebra para a juncao do
 * array que sera tornado em string (<br>)
 * 
 * @author rafael.oliveira <rafael.oliveira@unyleya.com.br>
 */
class Api_View_Helper_Messages extends Zend_View_Helper_Abstract {

    public $_result = array();

    /**
     * Metodo construtor da Helper
     * @param Zend_Form $form
     * @return Api_View_Helper_Messages
     */
    public function messages(Zend_Form $form)
    {
        $this->proccessMessages($form);
        return $this->_messages = $this;
    }

    /**
     * Processa as mensagens de um Zend_Form passado
     * @param type $form
     */
    private function proccessMessages(Zend_Form $form)
    {
        $arrErros = array();
        $formMens = $form->getMessages();
        if ($formMens) {
            $i = 0;
            foreach ($formMens as $erros) {
                if ($erros) {
                    foreach ($erros as $mens) {
                        $arrErros[$i] = $mens;
                    }
                }
                $i++;
            }
        }

        $this->_result = $arrErros;
    }

    /**
     * Retorna as mensagens processadas em forma de array
     * @return array
     */
    public function toArray()
    {
        return $this->_result;
    }

    /**
     * Retorna as mensagens processadas em forma de JSON
     * @return Zend_Json_Encoder
     */
    public function toJson()
    {
        return Zend_Json_Encoder::encode($this->_result);
    }

    /**
     * Retorna as mensagens processadas em forma de String
     * @param type $joinSeparator
     * @return String
     */
    public function toString($joinSeparator = '<br>')
    {
        return implode($joinSeparator, $this->_result);
    }

}

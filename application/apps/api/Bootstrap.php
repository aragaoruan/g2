<?php

/**
 * 
 * Bootrstrap Específico para o Módulo Gestor2
 * 
 */
class Api_Bootstrap extends Zend_Application_Module_Bootstrap
{

    protected function _bootstrap ($resource = NULL)
    {
        $_conf = new Zend_Config_Ini(APPLICATION_PATH . "/apps/" . strtolower($this->getModuleName()) . "/config/application.ini", APPLICATION_ENV);
        $this->_options = array_merge($this->_options, $_conf->toArray());
        parent::_bootstrap();
    }

}

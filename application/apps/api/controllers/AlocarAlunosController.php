<?php

/**
 * Controller API para alocar alunos
 * @author Débora Castro <debora.castro@unyleya.com.br>
 * @since  09-04-2014
 * @package application
 * @subpackage controller
 */
class Api_AlocarAlunosController extends Ead1_RestRestrita {

    private $negocio;

    public function init() {
        $this->_helper->viewRenderer->setNoRender(true);
        parent::init();
        $this->negocio = new \G2\Negocio\Alocacao();
    }

    public function indexAction() {
        
    }

    public function getAction() {
        
    }

    public function postAction() {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json_Decoder::decode($body);
        $this->getResponse()->appendBody(($this->negocio->salvarAlocacao($data)->toJson()));
        $this->getResponse()->setHttpResponseCode(201);
    }

    public function putAction() {
        
    }

    public function deleteAction() {
 
    }

    public function headAction() {
        
    }

}

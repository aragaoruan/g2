<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 31/03/2015
 * Time: 14:37
 */
class Api_CartaCreditoController extends Ead1_RestRestrita
{

    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\CartaCredito();
    }

    public function indexAction()
    {

    }

    public function getAction()
    {

    }

    public function postAction()
    {

    }

    public function putAction()
    {
    }

    public function deleteAction()
    {
        $id = (int)$this->getRequest()->getParam('id');
        if ($id) {
            $resultado = $this->negocio->deleta($id);
            if ($resultado) {
                $this->getResponse()->setHttpResponseCode(204)->appendBody('Registro removido');
            } else {
                $this->getResponse()->setHttpResponseCode(400)->appendBody('Erro ao tentar remover registro');
            }
        }
    }

    public function headAction()
    {
    }

}
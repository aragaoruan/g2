<?php

/**
 * @package application
 * @subpackage controller
 */
class Api_RecebimentoController extends Ead1_RestRestrita {

    private $doctrineContainer;
    private $em;
    private $bo;

    public function init() {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
        $this->bo = new G2\Negocio\RegraPagamento();
    }

    public function indexAction() {

        $mensageiro = $this->bo->findAll();

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $mensageiro->setText(count($mensageiro->getMensagem()) . ' registros encontrados!');
        }

        $this->getResponse()
                ->appendBody(json_encode($mensageiro->getMensagem()));
    }

    public function getAction() {

        $mensageiro = $this->bo->findById($this->_getParam('id', null));

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $mensageiro->setText(count($mensageiro->getMensagem()) . ' registros encontrados!');
        }

        $this->getResponse()->appendBody(json_encode($mensageiro->getFirstMensagem()));
    }

    public function postAction() {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);

        $mensageiro = $this->bo->save($data);

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->appendBody(json_encode($mensageiro->toArray()));
            $this->getResponse()->setHttpResponseCode(201);
        } else {
            $this->getResponse()->appendBody(json_encode(array('erro' => $mensageiro->getMensagem())));
            $this->getResponse()->setHttpResponseCode(500);
        }
    }

    public function putAction() {

        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);

        $mensageiro = $this->bo->save($data);

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->appendBody(json_encode($mensageiro->toArray()));
            $this->getResponse()->setHttpResponseCode(204);
        } else {
            $this->getResponse()->appendBody(json_encode(array('erro' => $mensageiro->getMensagem())));
            $this->getResponse()->setHttpResponseCode(500);
        }
    }

    public function deleteAction() {
        try {
            if ($this->bo->delete($this->bo->find('G2\Entity\RegraPagamento', $this->getParam('id')))) {
                $this->getResponse()->setHttpResponseCode(204)->appendBody('Regra de pagamento removida com sucesso.');
            } else {
                throw new Exception('', '', '');
            }
        } catch (Zend_Exception $e) {
            $this->getResponse()->setHttpResponseCode(500)->appendBody('Não foi possível apagar o registro');
        }
    }

    public function headAction() {
        
    }

}

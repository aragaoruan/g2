<?php

/**
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @since 2014-01-07
 * @package application
 * @subpackage controller
 */
class Api_VwModuloDisciplinaController extends Ead1_RestRestrita
{


    public function indexAction()
    {

        $to = new VwModuloDisciplinaTO();
        $to->montaToDinamico($this->_getAllParams());
        $ro = new ProjetoPedagogicoRO();
        $mensageiro = $ro->retornarVwModuloDisciplina($to);
        if ($mensageiro->getTipo() == Ead1_IMensageiro::AVISO) {
            $this->_helper->json(array());
        } else {
            $this->_helper->json($mensageiro->getMensagem());
        }
    }

    public function getAction()
    {
        $to = new VwModuloDisciplinaTO();
        $to->setId_modulodisciplina($this->_getParam('id'));
        $to->fetch(true, true, true);
        $this->_helper->json($to->toArray());

    }

    public function postAction()
    {
    }

    public function putAction()
    {
    }

    public function deleteAction()
    {

    }

    public function headAction()
    {

    }


}
<?php


/**
 * Controller modolo API para VwCartaCredito
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2015-13-01
 */
class Api_VwCartaCreditoController extends Ead1_RestRestrita
{
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new G2\Negocio\CartaCredito();
    }

    public function deleteAction()
    {

    }

    public function getAction()
    {

    }

    public function headAction()
    {

    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['module'], $params['action'], $params['controller']);
        $result = $this->negocio->retornarDadosVwCartaCredito($params, true);
        $this->getResponse()->appendBody(json_encode($result));
    }

    public function postAction()
    {

    }

    public function putAction()
    {

    }
} 
<?php

/**
 * Controller para gerenciar as áreas
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @since 2013-12-16
 * @package application
 * @subpackage controller
 */
class Api_VwProjetoAreaController extends Ead1_RestRestrita
{

    public function indexAction()
    {

        try {

            $array = $this->getAllParams();
            unset($array['module'], $array['controller'], $array['action']);

            $vsneTO = new \VwProjetoAreaTO($array);
            $ro = new \ProjetoPedagogicoBO();
            $mensageiro = $ro->retornarVwProjetoAreaCadastro($vsneTO);

        } catch (Exception $e) {
            $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json($mensageiro);
        } else {
            $this->getResponse()->setHttpResponseCode(201);
            $this->_helper->json($mensageiro->getMensagem());
        }

    }

    public function deleteAction()
    {

        $id = intval($this->getParam('id'));
        if (!is_int($id)) {
            $this->getResponse()->setHttpResponseCode(400)->setBody('Não posso apagar um registro com ID inválido!');
        } else {
            $negocio = new \G2\Negocio\GerenciaSalas();
            $result = $negocio->removeAreaProjetoSalaById($id);
            if ($result && is_bool($result)) {
                $this->getResponse()->setHttpResponseCode(204)->appendBody('Registro removido');
            } else {
                $this->getResponse()->setHttpResponseCode(400)->appendBody('Erro ao tentar remover registro. ' . $result);
            }
        }

    }


}
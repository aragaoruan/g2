<?php

/**
 * Class Api_DiaSemanaController
 * Controller APi para retornar dias da semana
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-12-01
 */
class Api_DiaSemanaController extends Zend_Rest_Controller
{

    private $negocio;

    public function init()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->negocio = new \G2\Negocio\DiaSemana();
    }


    public function indexAction()
    {
        $result = $this->negocio->findAll();
        if ($result) {
            foreach ($result as $key => $row) {
                $result[$key] = array(
                    'id_diasemana' => $row->getId_diasemana(),
                    'st_diasemana' => $row->getSt_diasemana(),
                    'st_abreviatura' => $row->getSt_abreviatura()
                );
            }
        }
        $this->getResponse()
            ->appendBody(json_encode($result));
    }

    /**
     * The get action handles GET requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function getAction()
    {
        // TODO: Implement getAction() method.
    }

    /**
     * The head action handles HEAD requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function headAction()
    {
        // TODO: Implement headAction() method.
    }

    /**
     * The post action handles POST requests; it should accept and digest a
     * POSTed resource representation and persist the resource state.
     */
    public function postAction()
    {
        // TODO: Implement postAction() method.
    }

    /**
     * The put action handles PUT requests and receives an 'id' parameter; it
     * should update the server resource state of the resource identified by
     * the 'id' value.
     */
    public function putAction()
    {
        // TODO: Implement putAction() method.
    }

    /**
     * The delete action handles DELETE requests and receives an 'id'
     * parameter; it should update the server resource state of the resource
     * identified by the 'id' value.
     */
    public function deleteAction()
    {
        // TODO: Implement deleteAction() method.
    }
}
<?php

/**
 * Controller para VwGradeNota
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 * @package application
 * @subpackage controller
 */
class Api_VwGradeNotaController extends Ead1_RestRestrita {

    private $_negocio;

    public function init() {
        parent::init();
        $this->_negocio = new \G2\Negocio\Matricula();
    }

    public function indexAction() {

        $array = array();

        $this->_negocio = new \G2\Negocio\Matricula();

        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);

        if ($params) {

            $result = $this->_negocio->findVwGradeNota($params);

            foreach ($result as $key => &$values) {
                array_push($array, $this->_negocio->toArrayEntity($values));
            }
        }

        $this->_helper->json($array);
    }

    public function getAction() {

        $array = array();

        if ($this->getParam('id')) {
            $this->_negocio = new \G2\Negocio\Matricula();
            $params = array('id_matricula' => (int)$this->getParam('id'));
            $result = $this->_negocio->findVwGradeNota($params);

            foreach ($result as $key => &$values) {
                array_push($array, $this->_negocio->toArrayEntity($values));
            }
        }

        $this->_helper->json($array);
    }

    public function postAction() {
        
    }

    public function putAction() {
        
    }

    public function deleteAction() {
        
    }

    public function headAction() {
        
    }

}

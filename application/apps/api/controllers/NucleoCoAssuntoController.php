<?php

/**
 * Controller para gerenciar as NucleoCo
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2014-12-17
 * @package application
 * @subpackage controller
 */

class Api_NucleoCoAssuntoController extends Ead1_RestRestrita
{

    private $negocio;
    private $bo;

    public function init ()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Negocio();
        $this->bo = new NucleoCoBO();
    }

    public function indexAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $params = $this->getAllParams();

        $to = new VwNucleoAssuntoCoTO();
        foreach ($params as $key => $value) {
            if (array_key_exists($key, $to)) {
                $to->{$key} = $value;
            }
        }

        $mensageiro = $this->bo->listarAssuntosNucleoAdicionar($to);
        $this->_helper->json($mensageiro);
    }

    public function getAction()
    {
        $id = $this->getParam('id');
    }

    public function postAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);

        $to = new NucleoPessoaCoTO($data);

        $mensageiro = $this->bo->salvarNucleoPessoaCo($to);

        if ($mensageiro->type == 'success' && is_array($mensageiro->mensagem))
            $mensageiro->mensagem = array_shift($mensageiro->mensagem);

        $this->_helper->json($mensageiro);
    }

    public function putAction()
    {
        $this->postAction();
    }

    public function deleteAction()
    {
        $id = $this->getParam('id');

        if ($id) {
            $to = new VwNucleoAssuntoCoTO();
            $to->setId_nucleoassuntoco($id);

            $mensageiro = $this->bo->excluirNucleoAssuntoCo($to);

            $this->_helper->json($mensageiro);
        }
    }

    public function headAction()
    {
    }

}
<?php

/**
 * Controller para Mensagem do tipo sistema
 * @author Denise Xavier <denise.xavoer@unyleya.com.br>
 * @since 2014-05-07
 * @package application
 * @subpackage controller
 */
use G2\Negocio\MensagemPadrao;

class Api_SistemaEntidadeMensagemController extends Ead1_RestRestrita
{

    private $negocio;

    public function init ()
    {
        parent::init();
        $this->negocio = new MensagemPadrao();
    }


    public function indexAction ()
    {
        $result = $this->negocio->getVwSistemaEntidadeMensagem();
        $arrReturn = array();
        if ($result) {
            foreach ($result as $row) {
                $arrReturn[] = array(
                    'id' => $row->getId_sistemaentidademensagem(),
                    'id_mensagempadrao' => $row->getId_mensagempadrao(),
                    'st_mensagempadrao' => $row->getSt_mensagempadrao(),
                    'id_textosistema' => $row->getId_textosistema(),
                    'st_textosistema' => $row->getSt_textosistema()
                );
            }
        }
        $this->getResponse()
            ->appendBody(json_encode($arrReturn));
    }

    public function postAction ()
    {
    	
    	try {

    		$body = $this->getRequest()->getRawBody();
	        $data = Zend_Json::decode($body);
	        $result = $this->negocio->saveSistemaEntidadeMensagem($data);
	        $this->getResponse()->appendBody(json_encode(array(
	            'id' => $result->getId_sistemaentidademensagem(),
	            'type' => 'success',
	            'title' => 'Registro Inserido',
	            'text' => 'O registro foi atualizado com sucesso!'
	        )));
	        $this->getResponse()->setHttpResponseCode(201);
    		
    	} catch (Exception $e) {

    		$this->getResponse()->appendBody(json_encode(array(
	            'id' => $result->getId_sistemaentidademensagem(),
	            'type' => 'error',
	            'title' => 'Erro ao Inserir o Registro',
	            'text' => $e->getMessage()
	        )));
	        $this->getResponse()->setHttpResponseCode(400);
    		
    	}
    	
    }

    public function putAction ()
    {
    	
    	try {
    		
	        $body = $this->getRequest()->getRawBody();
	        $data = Zend_Json::decode($body);
	        $result = $this->negocio->updateSistemaEntidadeMensagem($data);
	        $this->getResponse()->appendBody(json_encode(array(
	            'id' => $result->getId_sistemaentidademensagem(),
	            'type' => 'success',
	            'title' => 'Registro Atualizado',
	            'text' => 'O registro foi atualizado com sucesso!'
	        )));
	        $this->getResponse()->setHttpResponseCode(201);
    		
    	} catch (Exception $e) {

    		$this->getResponse()->appendBody(json_encode(array(
	            'id' => $result->getId_sistemaentidademensagem(),
	            'type' => 'error',
	            'title' => 'Erro ao Atualizar o Registro',
	            'text' => $e->getMessage()
	        )));
	        $this->getResponse()->setHttpResponseCode(400);
    		
    	}
    }

}
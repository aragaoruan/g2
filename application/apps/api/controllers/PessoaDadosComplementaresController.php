<?php

/**
 * Controller para gerenciar PessoaDadosComplementares
 * @author Marcus Pereira <marcus.pereira@unyleya.com.br>
 * @since 2015-10-29
 * @package application
 * @subpackage controller
 */

/**
 * Class Api_PessoaDadosComplementaresController
 */
class Api_PessoaDadosComplementaresController extends Ead1_RestRestrita
{

    /**
     * @var \G2\Negocio\PessoaDadosComplementares $negocio
     */
    private $negocio;
    private $repositoryName = '\G2\Entity\PessoaDadosComplementares';

    public function init()
    {
        parent::init();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->negocio = new \G2\Negocio\PessoaDadosComplementares();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['module'], $params['controller'], $params['action'], $params['order_by'], $params['sort'], $params['search'], $params['search']);

        $mensageiro = $this->negocio->findOneBy($this->repositoryName, $params);
        if ($mensageiro) {
            return $this->_helper->json($this->negocio->toArrayEntity($mensageiro));
        } else {
            return $this->_helper->json(array());
        }


    }

    public function getAction()
    {
        $entity = $this->negocio->find($this->repositoryName, $this->getParam('id'));
        if ($entity)
            $this->_helper->json($this->negocio->toArrayEntity($entity));
        else $this->getResponse()->setHttpResponseCode(400);
    }

    public function postAction()
    {
        $data = Zend_Json::decode($this->getRequest()->getRawBody());

        $mensageiro = $this->negocio->salvarDadosComplementares($data);
        $this->_helper->json($mensageiro);
    }

    public function putAction()
    {
        $this->postAction();
    }

    public function deleteAction()
    {
        $id = $this->getParam('id');

        if ($id) {
            $mensageiro = $this->negocio->find($this->repositoryName, $id);
            $mensageiro = $this->negocio->delete($mensageiro);

            $this->_helper->json($mensageiro);
        }
    }


}

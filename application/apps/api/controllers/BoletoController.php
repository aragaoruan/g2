<?php

/**
 * Controller para Entidades
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @package default
 * @subpackage controller
 * @since 2013-12-09
 */
use G2\Negocio\Boleto;

class Api_BoletoController extends Ead1_RestRestrita
{

    private $_negocio;

    public function init()
    {
        parent::init();
        $this->_negocio = new Boleto();
    }

    public function indexAction()
    {

    }

    public function deleteAction()
    {

    }

    public function getAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        try {
            $id_contaentidade = $this->getParam('id_contaentidade');

            if ($id_contaentidade) {
                $result = $this->_negocio->retornaDadosBoletoConfig($id_contaentidade);
                $this->getResponse()->appendBody(json_encode($result));
            }
        } catch (Exception $e) {
            throw $e;
        }

    }

    public function headAction()
    {

    }

    public function postAction()
    {

    }

    public function putAction()
    {

    }

}
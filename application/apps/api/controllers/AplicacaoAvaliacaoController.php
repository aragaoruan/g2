<?php

/**
 * Controller para criar aplicações para avaliação (locais de prova para agendamento de prova final)
 * @author Débora Castro <debora.castro@unyleya.com.br>
 * @since  04-12-2013
 * @package application
 * @subpackage controller
 */
class Api_AplicacaoAvaliacaoController extends Ead1_RestRestrita
{

    private $negocio;

    public function init()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        parent::init();
        $this->negocio = new \G2\Negocio\AplicacaoAvaliacao();
    }

    public function indexAction()
    {

    }

    public function getAction()
    {

    }

    public function postAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json_Decoder::decode($body);

        $this->getResponse()->appendBody(($this->negocio->salvaAplicacaoAvaliacao($data)->toJson()));
        $this->getResponse()->setHttpResponseCode(201);
    }

    public function putAction()
    {

    }

    public function deleteAction()
    {
        $id = intval($this->getParam('id'));

//        if (is_int($id)) {
//            $this->getResponse()->appendBody(($this->negocio->deletarAvaliacaoAplicacao($id)->toJson()));
//            $this->getResponse()->setHttpResponseCode(201);
//        } else {
//            $this->getResponse()->setHttpResponseCode(400);
//            $this->getResponse()->setBody('Não posso apagar um registro com ID inválido!');
//        }
        //não pode deletar por causa dos agendamentos ativos e os demais já agendados.
    }

    public function headAction()
    {

    }

}

<?php

/**
 * Controller API para alterar Tabela de Preços
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 * @since  08/01/2015
 * @package application
 * @subpackage controller
 */
class Api_TabelaPrecosController extends Ead1_RestRestrita
{


    public function indexAction()
    {
        $texto = new \G2\Negocio\ConfiguracaoEntidade();
        $mensageiro = $texto->findConfiguracaoEntidade($texto->sessao->id_entidade, false);
        $return = array(
            'id_entidade' => $mensageiro->getId_entidade(),
            'id_upload' => ($mensageiro->getId_tabelapreco()) ? $mensageiro->getId_tabelapreco()->getId_upload() : null,
            'st_upload' => ($mensageiro->getId_tabelapreco()) ? $mensageiro->getId_tabelapreco()->getSt_upload() : null
        );

        echo json_encode($return);
    }

    public function getAction()
    {

    }

    public function postAction()
    {

    }

    public function putAction()
    {

    }

    public function deleteAction()
    {

    }

    public function headAction()
    {

    }

}

<?php

/**
 * Created by PhpStorm.
 * User: kayo.silva
 * Date: 25/09/2014
 * Time: 16:54
 * Class Api_VwProdutoVendaController
 * Classe api para vw
 * @Author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Api_VwVendaLancamentoController extends Ead1_RestRestrita
{
    /**
     * @var \G2\Negocio\Venda
     */
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Venda();
    }

    /**
     * The index action handles index/list requests; it should respond with a
     * list of the requested resources.
     */
    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);
        $result = $this->negocio->retornaVwVendaLancamento($params);
        $arrReturn = array();

        if ($result) {
            foreach ($result as $i => $row) {
                if($row instanceof \G2\Entity\VwVendaLancamento){
                    $dt_vencimento = $row->getDt_vencimento();
                    if ($dt_vencimento instanceof DateTime) {
                        $dt_vencimento = $dt_vencimento->format("d/m/Y");
                    }
                    $arrReturn[$row->getNu_ordem() . $i] = $this->negocio->toArrayEntity($row);
                    $arrReturn[$row->getNu_ordem() . $i]['dt_vencimento'] = $dt_vencimento;
                    if (
                        $row->getId_evolucao() == \G2\Constante\Evolucao::TB_VENDA_AGUARDANDO_RECEBIMENTO &&
                        $row->getBl_entrada() &&
                        in_array($row->getId_meiopagamento(), array(1, 11, 2))
                    ) {
                        $arrReturn[$row->getNu_ordem() . $i]['st_urlpagamento'] =
                            \Ead1_Ambiente::geral()->st_url_loja . '/loja/recebimento/?venda=' .
                            $row->getId_venda() . '&lancamento=' . $row->getId_lancamento();
                    } else if ($row->getId_evolucao() == \G2\Constante\Evolucao::TB_VENDA_AGUARDANDO_RECEBIMENTO &&
                        in_array($row->getId_meiopagamento(), array(\G2\Constante\MeioPagamento::BOLETO)) &&
                        !empty($row->getst_urlboleto())
                    ) {
                        $arrReturn[$row->getNu_ordem() . $i]['st_urlpagamento'] =
                            \Ead1_Ambiente::geral()->st_url_loja . '/loja/recebimento/?venda=' .
                            $row->getId_venda() . '&lancamento=' . $row->getId_lancamento();
                    }
                }
            }
            ksort($arrReturn);
            $arrReturn = array_values($arrReturn);
        }
        $this->_helper->json($arrReturn);
    }

    /**
     * The get action handles GET requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function getAction()
    {
        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);
        $result = $this->negocio->retornaVwVendaLancamento($params);
        $arrReturn = array();
        if ($result) {
            foreach ($result as $i => $row) {
                if($row instanceof \G2\Entity\VwVendaLancamento){
                    $dt_vencimento = $row->getDt_vencimento();
                    if ($dt_vencimento instanceof DateTime) {
                        $dt_vencimento = $dt_vencimento->format("d/m/Y");
                    }
                    $arrReturn[$row->getNu_ordem() . $i] = $this->negocio->toArrayEntity($row);
                    $arrReturn[$row->getNu_ordem() . $i]['dt_vencimento'] = $dt_vencimento;
                    if ($row->getId_evolucao() == \G2\Constante\Evolucao::TB_VENDA_AGUARDANDO_RECEBIMENTO &&
                        $row->getBl_entrada() &&
                        in_array($row->getId_meiopagamento(), array(1, 11, 2))
                    ) {
                        $arrReturn[$row->getNu_ordem() . $i]['st_urlpagamento'] =
                            \Ead1_Ambiente::geral()->st_url_loja . '/loja/recebimento/?venda=' .
                            $row->getId_venda() . '&lancamento=' . $row->getId_lancamento();
                    } else if (
                        in_array($row->getId_meiopagamento(), array(\G2\Constante\MeioPagamento::BOLETO)) &&
                        !empty($row->getst_urlboleto())
                    ) {
                        $arrReturn[$row->getNu_ordem() . $i]['st_urlpagamento'] =
                            \Ead1_Ambiente::geral()->st_url_loja . '/loja/recebimento/?venda=' .
                            $row->getId_venda() . '&lancamento=' . $row->getId_lancamento();
                    }
                }
            }
            ksort($arrReturn);
            $arrReturn = array_values($arrReturn);
        }

        $this->_helper->json($arrReturn);
    }

    /**
     * The head action handles HEAD requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function headAction()
    {
        // TODO: Implement headAction() method.
    }

    /**
     * The post action handles POST requests; it should accept and digest a
     * POSTed resource representation and persist the resource state.
     */
    public function postAction()
    {
        // TODO: Implement postAction() method.
    }

    /**
     * The put action handles PUT requests and receives an 'id' parameter; it
     * should update the server resource state of the resource identified by
     * the 'id' value.
     */
    public function putAction()
    {
        // TODO: Implement putAction() method.
    }

    /**
     * The delete action handles DELETE requests and receives an 'id'
     * parameter; it should update the server resource state of the resource
     * identified by the 'id' value.
     */
    public function deleteAction()
    {
        // TODO: Implement deleteAction() method.
    }
}

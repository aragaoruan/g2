<?php

/**
 * Controller para gerenciar Vinculo de entidade com Livros
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-27-01
 * @package api
 * @subpackage controller
 */
class Api_LivroEntidadeController extends Ead1_RestRestrita
{

    private $negocio;

    public function init ()
    {
        parent::init();
        $this->negocio = new G2\Negocio\Livro();
    }

    public function indexAction ()
    {
        $result = $this->negocio->retornaLivroEntidade();
        $arrReturn = array();
        if ($result->getType() == Ead1_IMensageiro::SUCESSO) {
            foreach ($result->getMesagem() as $row) {
                $arrReturn[] = array(
                    'id' => $row->getId_livroentidade(),
                    'id_livroentidade' => $row->getId_livroentidade(),
                    'id_livro' => ($row->getId_livro() ? $row->getId_livro()->getId_livro() : null),
                    'id_entidade' => ($row->getId_entidade() ? $row->getId_entidade()->getId_entidade() : null)
                );
            }
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    public function getAction ()
    {
        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);
        $result = $this->negocio->retornaLivroEntidade($params);
        $arrReturn = array();
        if ($result->getType() == 'success') {
            foreach ($result->getMensagem() as $row) {
                $arrReturn[] = array(
                    'id' => $row->getId_livroentidade(),
                    'id_livroentidade' => $row->getId_livroentidade(),
                    'id_livro' => ($row->getId_livro() ? $row->getId_livro()->getId_livro() : null),
                    'id_entidade' => ($row->getId_entidade() ? $row->getId_entidade()->getId_entidade() : null)
                );
            }
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    public function postAction ()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
    }

    public function putAction ()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
    }

    public function deleteAction ()
    {
        
    }

    public function headAction ()
    {
        
    }

}
<?php

/**
 * Controller para gerenciar os produtos
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @since 2013-11-05
 * @package application
 * @subpackage controller
 */
class Api_TipoProdutoController extends Ead1_RestRestrita
{

    private $doctrineContainer;
    private $em;

    public function init()
    {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
    }

    public function indexAction()
    {

        $contrato = new \G2\Negocio\TipoProduto();
        $mensageiro = $contrato->findAll();

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $mensageiro->setText(count($mensageiro->getMensagem()) . ' registros encontrados!');
            $this->getResponse()->appendBody(json_encode($mensageiro->toArrayAll()));
            $this->getResponse()->setHttpResponseCode(200);
        } else {
            $this->getResponse()->appendBody(json_encode($mensageiro->toArrayAll()));
            $this->getResponse()->setHttpResponseCode(400);
        }


//        $this->_helper->json($mensageiro);
    }

    public function getAction()
    {

        $contrato = new \G2\Negocio\TipoProduto();
        $mensageiro = $contrato->findId($this->_getParam('id', null));

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $mensageiro->setText(count($mensageiro->getMensagem()) . ' registros encontrados!');
        }

        $this->getResponse()
            ->appendBody(json_encode($mensageiro->getFirstMensagem()));

    }

    public function postAction()
    {
    }

    public function putAction()
    {


    }

    public function deleteAction()
    {

    }

    public function headAction()
    {

    }


}
<?php

/**
 * Controller para VwEntregaDocumento
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @since 15/10/2014
 * @package application
 * @subpackage controller
 */
class Api_VwEntregaDocumentoController extends Ead1_RestRestrita {

    private $doctrineContainer;
    private $em;

    public function init() {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
    }

    public function indexAction() {

    }

    public function getAction() {
        $negocio = new \G2\Negocio\EntregaDocumentos();
        $params = $this->getRequest()->getParams();
        $order = array('id_entidade' => 'ASC', 'nu_ordenacao' => 'ASC');

        unset($params['module']);
        unset($params['action']);
        unset($params['controller']);

        $retorno = $negocio->buscarVwEntregaDocumentos($params, $order);

        $this->_helper->json($retorno);
    }

    public function postAction() {

    }

    public function putAction() {

    }

    public function deleteAction() {

    }

    public function headAction() {

    }

}

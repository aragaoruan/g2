<?php

/**
 * Controller para gerenciar as categorias dos produtos
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @since 2013-11-11
 * @package application
 * @subpackage controller
 */
class Api_CategoriaProdutoController extends Ead1_RestRestrita
{
    
    private $doctrineContainer;
    private $em;
    

     public function indexAction()
    {
        
        
    }

    public function getAction()
    {
        
        
    }
    
    public function postAction()
    {
    	try {
    	
    		$data       = Zend_Json::decode($this->getRequest()->getRawBody());
    		
    		if(!isset($data['id_produto']) || $data['id_produto']==false){
    			throw new Zend_Exception("ID do Produto é obrigatório!");
    		}
    		
    		$pTO = new ProdutoTO();
    		$pTO->setId_produto($data['id_produto']);
    		
    		$arcategoriasProduto = array();
    		
    		if(isset($data['id_categoria']) || $data['id_categoria']==true){
    			foreach($data['id_categoria'] as $id_categoria){
    				$cTO = new CategoriaProdutoTO();
    				$cTO->setId_categoria($id_categoria);
    				$arcategoriasProduto[] = $cTO;
    				
    			}
    		}
    		
    		$bo = new ProdutoBO();
    		$mensageiro = $bo->salvarArCategoriasProduto($arcategoriasProduto,  $pTO);
    		
    	
    	} catch (Exception $exc) {
    		$mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
    	}
    	
    	if($mensageiro->getTipo()==Ead1_IMensageiro::SUCESSO){
    		$this->getResponse()->setHttpResponseCode(201);
    		$this->_helper->json($data);
    	} else {
    		$this->getResponse()->setHttpResponseCode(400);
    		$this->_helper->json($mensageiro);
    	}
    }
    
 
    
    public function putAction()
    {
    	try {
    	
    		$data       = Zend_Json::decode($this->getRequest()->getRawBody());
    		
    		if(!isset($data['id_produto']) || $data['id_produto']==false){
    			throw new Zend_Exception("ID do Produto é obrigatório!");
    		}
    		
    		$pTO = new ProdutoTO();
    		$pTO->setId_produto($data['id_produto']);
    		
    		$arcategoriasProduto = array();
    		
    		if(isset($data['id_categoria']) || $data['id_categoria']==true){
    			foreach($data['id_categoria'] as $id_categoria){
    				$cTO = new CategoriaProdutoTO();
    				$cTO->setId_categoria($id_categoria);
    				$arcategoriasProduto[] = $cTO;
    				
    			}
    		}
    		
    		$bo = new ProdutoBO();
    		$mensageiro = $bo->salvarArCategoriasProduto($arcategoriasProduto,  $pTO);
    		
    	
    	} catch (Exception $exc) {
    		$mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
    	}
    	
    	if($mensageiro->getTipo()==Ead1_IMensageiro::SUCESSO){
    		$this->getResponse()->setHttpResponseCode(201);
    		$this->_helper->json($data);
    	} else {
    		$this->getResponse()->setHttpResponseCode(400);
    		$this->_helper->json($mensageiro);
    	}
    }
    
 
    
    public function deleteAction(){
        
    }

    public function headAction(){
        
    }
    

}
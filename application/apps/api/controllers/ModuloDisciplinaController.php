<?php

/**
 * Controller para gerenciar os ModuloDisciplina
 * @author Elcio Mauro Guimarães
 * <elcioguimaraes@gmail.com>
 * @since 2014-01-08
 * @package application
 * @subpackage controller
 */
class Api_ModuloDisciplinaController extends Ead1_RestRestrita
{


    public function getAction()
    {

        $to = new \ModuloDisciplinaTO();
        $to->setId_modulodisciplina($this->_getParam('id'));
        $to->fetch(true, true, true);
        $this->_helper->json($to->toArray());

    }

    public function postAction()
    {
        $data = Zend_Json::decode($this->getRequest()->getRawBody());
        $ro = new ProjetoPedagogicoBO();
        $to = new ModuloDisciplinaTO();
//formata o numero para inserir no banco
        if (array_key_exists('nu_ponderacaoaplicada', $data) && !empty($data['nu_ponderacaoaplicada'])) {
            $data['nu_ponderacaoaplicada'] = str_replace(",", ".", $data['nu_ponderacaoaplicada']);
        }
        $to->montaToDinamico($data);

        if (array_key_exists('nu_obrigatorioalocacao', $data) && !empty($data['nu_obrigatorioalocacao'])) {
            $negocio = new \G2\Negocio\ProjetoPedagogico();
            $to->setNu_percentualsemestre($negocio->setNuPercentualSemestre($data['id_projetopedagogico'], $data['nu_obrigatorioalocacao']));
        }

        $mensageiro = $ro->inserirModuloDisciplina($to);
        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {

            $this->getResponse()->setHttpResponseCode(201);
            $this->_helper->json($to->toArray());
        } else {
            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json($mensageiro);
        }

    }

    public function putAction()
    {

        $data = Zend_Json::decode($this->getRequest()->getRawBody());
        $ro = new ProjetoPedagogicoBO();
        $to = new ModuloDisciplinaTO();
//formata o numero para inserir no banco
        if (array_key_exists('nu_ponderacaoaplicada', $data) && !empty($data['nu_ponderacaoaplicada'])) {
            $data['nu_ponderacaoaplicada'] = str_replace(",", ".", $data['nu_ponderacaoaplicada']);
        }

        $to->montaToDinamico($data);
        if (array_key_exists('nu_obrigatorioalocacao', $data) && !empty($data['nu_obrigatorioalocacao'])) {
            $negocio = new \G2\Negocio\ProjetoPedagogico();
            $to->setNu_percentualsemestre($negocio->setNuPercentualSemestre($data['id_projetopedagogico'], $data['nu_obrigatorioalocacao']));
        }

        $mensageiro = $ro->editarModuloDisciplina($to);
        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->setHttpResponseCode(201);
            $this->_helper->json($to->toArray());
        } else {
            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json($mensageiro);
        }

    }

    public function deleteAction()
    {

        $id_modulodisciplina = $this->_getParam('id', null);
        if ($id_modulodisciplina) {
            $bo = new ProjetoPedagogicoBO();

            $to = new ModuloDisciplinaTO();
            $to->setId_modulodisciplina($id_modulodisciplina);
            $to->fetch(true, true, true);
            $to->setBl_ativo(false);

            $mensageiro = $bo->editarModuloDisciplina($to);
        } else {
            $mensageiro = new Ead1_Mensageiro("Id não encontrado.", Ead1_IMensageiro::AVISO);
        }
        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->setHttpResponseCode(201);
            $this->_helper->json($to->toArray());
        } else {
            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json($mensageiro);
        }

    }

    public function headAction()
    {

    }


}
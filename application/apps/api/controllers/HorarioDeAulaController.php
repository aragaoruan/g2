<?php

/**
 * Controller API para Horario de Aula
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since  09-09-2014
 * @package application
 * @subpackage controller
 */
class Api_HorarioDeAulaController extends Ead1_RestRestrita
{

    /**
     * @var \G2\Negocio\HorarioAula $negocio
     */
    private $negocio;

    /**
     * @var \G2\Negocio\VwHorarioAula $vw
     */
    private $vw;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\HorarioAula();
        $this->vw = new \G2\Negocio\VwHorarioAula();
    }

    public function indexAction()
    {
        $mensageiro = $this->vw->findAll();

        $this->getResponse()->appendBody(json_encode($mensageiro));
    }

    public function getAction()
    {
    }

    public function postAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);

        $mensageiro = $this->negocio->save($data);

        // Se salvar com sucesso, retornaro id_horarioaula
        // e o id_entidade para o ItemView do Marionette
        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $data = array(
                'id_horarioaula' => $mensageiro->getCodigo(),
                'id_entidade' => array(
                    'id_entidade' => $this->negocio->sessao->id_entidade
                )
            );
            $this->getResponse()->appendBody(json_encode($data));
            $this->getResponse()->setHttpResponseCode(200);
        } else {
            $this->getResponse()->appendBody(json_encode(array('erro' => $mensageiro->getMensagem())));
            $this->getResponse()->setHttpResponseCode(500);
        }
    }

    public function putAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);

        $mensageiro = $this->negocio->save($data);


        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->appendBody(json_encode($mensageiro->toArray()));
            $this->getResponse()->setHttpResponseCode(200);
        } else {
            $this->getResponse()->appendBody(json_encode(array('erro' => $mensageiro->getMensagem())));
            $this->getResponse()->setHttpResponseCode(500);
        }
    }

    public function deleteAction()
    {
        try{
            $this->vw->delete( $this->vw->find('G2\Entity\HorarioAula', $this->getParam('id')) );
            $this->getResponse()->setHttpResponseCode(204);
        } catch (Zend_Exception $e) {
            $this->getResponse()->setHttpResponseCode(500);
        }
    }

    public function headAction()
    {
    }

}

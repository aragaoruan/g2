<?php

class Api_VwGerarEtiquetasController extends Ead1_RestRestrita {

    /**
     * @var \G2\Negocio\Negorio $negocio
     */
    private $negocio;
    private $repositoryName = '\G2\Entity\VwGerarEtiquetas';

    public function init ()
    {
        parent::init();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->negocio = new \G2\Negocio\Negocio();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();


        $orderBy = $this->getParam('order_by') ? array($this->getParam('order_by') => $this->getParam('sort') ? $this->getParam('sort') : 'ASC') : null;
        unset($params['module'], $params['controller'], $params['action'], $params['order_by'], $params['sort'], $params['search'], $params['search'], $params['st_nomecompleto']);
        $paramsLike = array('st_nomecompleto' => $this->getParam('st_nomecompleto'));

        $mensageiro = $this->negocio->findBySearch($this->repositoryName, $paramsLike, $params, $orderBy);

        foreach ($mensageiro as $key => &$entity) {
            $entity = $entity->toArray();
        }

        $this->_helper->json($mensageiro);
    }

//    public function getAction()
//    {
//        $params = $this->getAllParams();
//
//
//        $entity = $this->negocio->find($this->repositoryName, $this->getParam('id'));
//        if ($entity)
//            $this->_helper->json($this->negocio->toArrayEntity($entity));
//    }
//
//    public function postAction()
//    {
//        $data = Zend_Json::decode($this->getRequest()->getRawBody());
//
//        $mensageiro = $this->negocio->save($data);
//        $this->_helper->json($mensageiro);
//    }
//
//    public function putAction()
//    {
//        $this->postAction();
//    }
//
//    public function deleteAction()
//    {
//        $id = $this->getParam('id');
//
//        if ($id) {
//            $mensageiro = $this->negocio->find($this->repositoryName, $id);
//            $mensageiro = $this->negocio->delete($mensageiro);
//
//            $this->_helper->json($mensageiro);
//        }
//    }

}
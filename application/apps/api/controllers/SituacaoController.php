<?php

/**
 * Controller para gerenciar as Situações
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @since 2013-11-05
 * @package application
 * @subpackage controller
 */
class Api_SituacaoController extends Ead1_RestRestrita
{
    
    private $doctrineContainer;   
    private $em;
    
    public function init ()
    {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
    }
    
     public function indexAction()
    {
        
        $parametros = array();
        $st_tabela = $this->_getParam('st_tabela', false);
        if($st_tabela){
           $parametros['st_tabela'] = $st_tabela;
        } 
         
        $situacao = new \G2\Negocio\Situacao();
        $mensageiro = $situacao->retornaSituacaoFindBy($parametros);
        $this->_helper->json($mensageiro->getMensagem());
        
        
        
    }

    public function getAction()
    {
        
        $situacao = new \G2\Negocio\Situacao();
        $mensageiro = $situacao->retornaSituacaoFindBy(array('id'=>$this->_getParam('id', null)));

        if($mensageiro->getTipo()!=Ead1_IMensageiro::SUCESSO){
        	$this->getResponse()->setHttpResponseCode(400);
        	$this->_helper->json($mensageiro);
        } else {
        	$this->getResponse()->setHttpResponseCode(201);
        	$this->_helper->json($mensageiro->getFirstMensagem());
        }
        
        
       
        
    }
    
    public function postAction()
    {
    }
    
    public function putAction()
    {

    }
    
    public function deleteAction(){
        
    }

    public function headAction(){
        
    }
    

}
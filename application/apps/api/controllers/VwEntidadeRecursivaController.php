<?php

/**
 * Controller para gerenciar VwEntidadeRecursiva
 * @author Neemias santos <neemias.santos@unyleya.com.br>
 * @since 2015-04-29
 * @package application
 * @subpackage controller
 */

class Api_VwEntidadeRecursivaController extends Ead1_RestRestrita {

    private $negocio;
    private $repositoryName = '\G2\Entity\VwEntidadeRecursiva';

    public function init ()
    {
        parent::init();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->negocio = new \G2\Negocio\Negocio();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        $orderBy = $this->getParam('order_by') ? array($this->getParam('order_by') => $this->getParam('sort') ? $this->getParam('sort') : 'ASC') : null;
        unset($params['module'], $params['controller'], $params['action'], $params['order_by'], $params['sort']);

        if (!isset($params['id_entidade']))
            $params['id_entidade'] = $this->negocio->sessao->id_entidade;

        $mensageiro = $this->negocio->findBy($this->repositoryName, $params, $orderBy);
        $results = array();
        foreach ($mensageiro as $entity) {
            $results[] = $this->negocio->toArrayEntity($entity);
        }

        $this->_helper->json($results);
    }

    public function getAction()
    {
        $id = $this->getParam('id');

        if ($id) {
            $mensageiro = $this->negocio->find($this->repositoryName, $id);
            $mensageiro = $this->negocio->toArrayEntity($mensageiro);

            $this->_helper->json($mensageiro);
        }
    }

}
<?php

/**
 * Controller para gerenciar VwGestaoDeOcorrencias
 * @author Marcus Pereira <marcus.pereira@unyleya.com.br>
 * @since 2015-08-04
 * @package application
 * @subpackage controller
 */

class Api_VwGestaoDeOcorrenciasController extends Ead1_RestRestrita
{

    /**
     * @var \G2\Negocio\Negocio $negocio
     */
    private $negocio;
    private $repositoryName = '\G2\Entity\VwGestaoDeOcorrencias';

    public function init() {
        parent::init();

        $this->_helper->viewRenderer->setNoRender(true);
        $this->negocio = new \G2\Negocio\Ocorrencia();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        $orderBy = $this->getParam('order_by') ? array($this->getParam('order_by') => $this->getParam('sort') ? $this->getParam('sort') : 'ASC') : null;
        unset($params['module'], $params['controller'], $params['action'], $params['order_by'], $params['sort'], $params['search']);
        $paramsLike = $this->getParam('search');

        $mensageiro = $this->negocio->findBySearchVwGestaoDeOcorrencias($paramsLike, $params, $orderBy);
        foreach ($mensageiro as $key => &$entity) {
            $entity = $this->negocio->toArrayEntity($entity);
        }
        $this->_helper->json($mensageiro);
    }

    public function getAction(){
        $mensageiro = $this->negocio->find($this->repositoryName, $this->getParam('id'));
        if ($mensageiro) {
            $this->_helper->json($this->negocio->toArrayEntity($mensageiro));
        }
    }

}
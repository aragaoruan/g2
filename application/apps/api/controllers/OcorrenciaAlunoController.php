<?php

/**
 * Description of OcorrenciaAlunoController
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class Api_OcorrenciaAlunoController extends Ead1_RestRestrita
{


    /**
     * Action Api responsavel por criar nova ocorrencia
     * @param json data
     * @return json data
     */
    public function postAction()
    {
        $negocio = new \G2\Negocio\Ocorrencia();

        $id_usuario = $negocio->sessao->id_usuario;
        $id_entidade = $negocio->sessao->id_entidade;

        //$data = Zend_Json_Decoder::decode($json);
        $data = $this->getRequest()->getPost();

        //Verifica se um arquivo foi enviado
        $arquivo = array_key_exists('arquivo', $_FILES) ? $_FILES['arquivo'] : null;

        if ($arquivo)
            $data['arquivo'] = $arquivo;

        $form = new OcorrenciaForm();

        if ($form->isValid($data['ocorrencia'])) {
            try {
                $mensageiro = $negocio->salvarOcorrencia($data, false);
                if ($mensageiro->getType() == Ead1_IMensageiro::TYPE_SUCESSO) {
                    $toArray = $negocio->toArrayEntity($mensageiro->getFirstMensagem());
                    $this->_helper->json($toArray);
                }
            } catch (Exception $e) {
                $this->getResponse()->setHttpResponseCode(400);
                $this->_helper->json(['msg' => $e->getMessage()]);
            }

        } else {
            var_dump($form->getErrors());
        }
    }

    /**
     * Action Api responsavel por fazer diversas alteracoes em ocorrencia,
     * metodo construido neste formato para reutilizacao de BO
     *
     * @param json data
     * @return json data
     */
    public function putAction()
    {
        $json = $this->getRequest()->getRawBody();
        $form = new OcorrenciaForm();
        $data = Zend_Json_Decoder::decode($json);

        switch ($data['tipo_update']) {
            case 1:
                //update comum
                if ($form->isValid($data)) {
                    $negocio = new G2\Negocio\Ocorrencia();
                    $entity = $negocio->salvarOcorrencia($data);
                    $toArray = $negocio->toArrayEntity($entity);
                    $this->_helper->json($toArray);
                }
                break;

            case 2:
                //update encerramento
                $ocorrencia = new G2\Negocio\Ocorrencia();
                $entity = $ocorrencia->encerrarOcorrencia($data);
                $toArray = $ocorrencia->toArrayEntity($entity);
                $this->_helper->json($toArray);
                break;

            case 3:
                //update devolver ocorrencia
                $ocorrencia = new G2\Negocio\Ocorrencia();
                $entity = $ocorrencia->devolverOcorrencia($data);
                $toArray = $ocorrencia->toArrayEntity($entity);
                $this->_helper->json($toArray);
                break;

            case 4:
                //update reabrir ocorrencia
                $ocorrencia = new G2\Negocio\Ocorrencia();
                $entity = $ocorrencia->reabrirOcorrencia($data);
                $toArray = $ocorrencia->toArrayEntity($entity);
                $this->_helper->json($toArray);
                break;

            case 5:
                //update solicitar encerramento
                $ocorrencia = new G2\Negocio\Ocorrencia();
                $entity = $ocorrencia->solicitarEncerramento($data);
                $toArray = $ocorrencia->toArrayEntity($entity);
                $this->_helper->json($toArray);
                break;

            case 6:
                //update encaminhar ocorrencia
                $ocorrencia = new G2\Negocio\Ocorrencia();
                $entity = $ocorrencia->encaminharOcorrencia($data);
                $toArray = $ocorrencia->toArrayEntity($entity);
                $this->_helper->json($toArray);
            default:
                return 'Tipo de alteração incorreto';
        }
    }

}

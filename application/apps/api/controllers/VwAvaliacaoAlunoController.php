<?php

/**
 * Controller para VwAvaliacaoAluno
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @since 2014-03-14
 * @package application
 * @subpackage controller
 */
class Api_VwAvaliacaoAlunoController extends Ead1_RestRestrita {

    private $doctrineContainer;
    private $em;

    public function init() {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
    }

    public function indexAction() {
        $negocio = new \G2\Negocio\Avaliacao();
        $params = $this->getRequest()->getParams();
        $order = array('id_disciplina' => 'ASC');
        $array = array();
        
        unset($params['module']);
        unset($params['action']);
        unset($params['controller']);
        
        foreach ($negocio->findVwAvaliacaoAluno($params, $order) as $key => $value){
            $array[$key] = $negocio->toArrayEntity($value);
        }
        
        $this->_helper->json($array);
    }

    public function getAction() {
        $negocio = new \G2\Negocio\Avaliacao();
        $array_find = array();
        if($this->_getParam('id_matricula', null) != null){
            $array_find['id_matricula'] = $this->_getParam('id_matricula');
        }
        if($this->_getParam('id_tipoavaliacao', null) != null){
            $array_find['id_tipoavaliacao'] = $this->_getParam('id_tipoavaliacao');
        }
        $return = $negocio->findVwAvaliacaoAluno($array_find, array('dt_cadastroavaliacao' => 'DESC'));
        $serialize = new \Ead1\Doctrine\EntitySerializer($this->em);
        if(!empty($return)):
            foreach ($return as $key => $obj) {
                $retorno[$key] = $serialize->toArray($obj);
            }
            $retorno = $retorno[0];
        else:
            $retorno = $return;
        endif;

        $this->_helper->json($retorno);
    }

    public function postAction() {
        
    }

    public function putAction() {
        
    }

    public function deleteAction() {
        
    }

    public function headAction() {
        
    }

}

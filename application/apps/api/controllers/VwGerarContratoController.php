<?php

/**
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-10-29
 * @package api
 * @subpackage controller
 */
class Api_VwGerarContratoController extends Ead1_RestRestrita
{

    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Contrato();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);
        $result = $this->negocio->retornarVwGerarContrato($params);
        if ($result) {
            foreach ($result as $i => $row) {
                $result[$i] = $this->negocio->toArrayEntity($row);
            }
        }
        $this->getResponse()
            ->appendBody(json_encode($result));
    }

    public function getAction()
    {
        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);

        if (array_key_exists('id', $params) && !empty($params['id'])) {
            $result = $this->negocio->retornarVwGerarContratoById($params);
            if ($result) {
                $result = $this->negocio->toArrayEntity($result);
            }
        } else {
            $result = $this->negocio->retornarVwGerarContrato($params);
            if ($result) {
                foreach ($result as $i => $row) {
                    $result[$i] = $this->negocio->toArrayEntity($row);
                }
            }
        }
        $this->getResponse()
            ->appendBody(json_encode($result));

    }

    public function headAction()
    {
    }

    public function postAction()
    {
    }

    public function putAction()
    {
    }

    public function deleteAction()
    {
    }
}
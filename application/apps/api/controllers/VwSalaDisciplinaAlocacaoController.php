<?php

/**
 * Controller para VwSalaDisciplinaAlocacao
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class Api_VwSalaDisciplinaAlocacaoController extends Ead1_RestRestrita
{

    public function init()
    {

    }

    public function indexAction()
    {
        $negocio = new \G2\Negocio\SalaDeAula();
        $params = $this->getRequest()->getParams();
        $array = array();

        unset($params['module']);
        unset($params['action']);
        unset($params['controller']);

        foreach ($negocio->retornaVwSalaDisciplinaAlocacao($params) as $key => $value) {
            $array[$key] = $negocio->toArrayEntity($value);
        }

        $this->_helper->json($array);
    }

    public function getAction()
    {
        $negocio = new \G2\Negocio\SalaDeAula();
        $array = array();

        $where = array(
            'id_saladeaula' => $this->getRequest()->getParam('id')
        );

        foreach ($negocio->retornaVwSalaDisciplinaAlocacao($where) as $key => $value) {
            $array[$key] = $negocio->toArrayEntity($value);
        }

        $this->_helper->json($array);
    }

    public function postAction()
    {

    }

    public function putAction()
    {

    }

    public function deleteAction()
    {

    }

    public function headAction()
    {

    }

}

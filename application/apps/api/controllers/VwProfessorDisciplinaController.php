<?php

/**
 * Controller para criar funcionalidades para os professores/disciplinas
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 * @since  2014-06-27
 * @package Api
 * @subpackage controller
 */
class Api_VwProfessorDisciplinaController extends Ead1_RestRestrita
{

    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Disciplina();
    }

    public function deleteAction()
    {

    }

    public function getAction()
    {
        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);
        $result = $this->negocio->retornarVwProfessorDisciplina($params);
        $this->getResponse()->appendBody(json_encode($result));
    }

    public function headAction()
    {

    }

    public function indexAction()
    {
        $params = $this->getAllParams();

        $bo = new DisciplinaBO();
        $vw = new VwProfessorDisciplinaTO();

        $vw->setId_tipodisciplina($params['tipo_disciplina']);

        $this->getResponse()->appendBody(json_encode($bo->retornarProfessor($vw, 'st_nomecompleto')->getMensagem()));
    }

    public function postAction()
    {

    }

    public function putAction()
    {

    }

    public function getRetornarVwProfessorDisciplinaAction()
    {

    }

}

<?php

/**
 * Controller para gerenciar Série
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @since 2013-12-16
 * @package application
 * @subpackage controller
 */
class Api_VwSerieNivelEnsinoController extends Ead1_RestRestrita
{


    public function indexAction ()
    {
    	
        try {
        	
        	$array = $this->_getAllParams();
        	unset($array['module'], $array['controller'], $array['action']);
        	
        	$vsneTO = new \VwSerieNivelEnsinoTO($array);
        	$ro 	= new \Ead1_RO();
        	$mensageiro = $ro->serieNivelEnsino( $vsneTO);
        	
        } catch (Exception $e) {
        	$mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO );
        }
    	
        
        if($mensageiro->getTipo()!=Ead1_IMensageiro::SUCESSO){
    		$this->getResponse()->setHttpResponseCode(400);
    		$this->_helper->json($mensageiro);
    	} else {
    		$this->getResponse()->setHttpResponseCode(201);
	    	$this->_helper->json($mensageiro->getMensagem());
    	}
    	
    }

    public function getAction ()
    {
        
    }

    public function postAction ()
    {
        
    }

    public function putAction ()
    {

       
    }

    public function deleteAction ()
    {
        
    }

    public function headAction ()
    {
        
    }

}
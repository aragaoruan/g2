<?php

/**
 * Controller para gerenciar os produtos
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @since 2013-11-12
 * @package application
 * @subpackage controller
 */
class Api_PlanoPagamentoController extends Ead1_RestRestrita
{
    
    private $doctrineContainer;
    private $em;
    
    public function init ()
    {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
    }
    
     public function indexAction()
    {
        
        
    }

    public function getAction()
    {
        
        $produto = new \G2\Negocio\Produto();
        $mensageiro = $produto->findPlanoPagamentoById($this->_getParam('id', null));
        
        if($mensageiro->getTipo()==Ead1_IMensageiro::SUCESSO){
            $mensageiro->setText(count($mensageiro->getMensagem()).' registros encontrados!');
        }
        
        $this->getResponse()
                ->appendBody(json_encode($mensageiro->getFirstMensagem()));
        
    }
    
    public function postAction()
    {
        try {

            $body       = $this->getRequest()->getRawBody();
            $data       = Zend_Json::decode($body);
            $produto   = new \G2\Negocio\Produto();
            
            $mensageiro = $produto->salvarPlanoPagamento($data);
            
        } catch (Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        if($mensageiro->getTipo()==Ead1_IMensageiro::SUCESSO){
        	$this->getResponse()->setHttpResponseCode(201);
        	$this->_helper->json($mensageiro->getFirstMensagem());
        } else {
        	$this->getResponse()->setHttpResponseCode(400);
        	$this->_helper->json($mensageiro);
        }
        
    }
    
    public function putAction()
    {
        try {

            $body       = $this->getRequest()->getRawBody();
            $data       = Zend_Json::decode($body);
            $produto   = new \G2\Negocio\Produto();
            
            $mensageiro = $produto->salvarPlanoPagamento($data);
            
        } catch (Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        
        if($mensageiro->getTipo()==Ead1_IMensageiro::SUCESSO){
        	$this->getResponse()->setHttpResponseCode(201);
        	$this->_helper->json($mensageiro->getFirstMensagem());
        } else {
        	$this->getResponse()->setHttpResponseCode(400);
        	$this->_helper->json($mensageiro);
        }

    }
    
    public function deleteAction(){
        
    }

    public function headAction(){
        
    }
    

}
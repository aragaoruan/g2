<?php

/**
 * Controller para gerenciar locais de prova
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @since 2013-12-02
 * @package application
 * @subpackage controller
 */
class Api_AplicadorProvaController extends Ead1_RestRestrita {

    /**
     * @var \G2\Negocio\AplicadorProva $negocio
     */
    private $negocio;
    private $repositoryName = '\G2\Entity\AplicadorProva';

    public function init() {
        $this->_helper->viewRenderer->setNoRender(true);
        parent::init();
        $this->negocio = new \G2\Negocio\AplicadorProva();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['module'], $params['controller'], $params['action']);

        $results = $this->negocio->findBy($this->repositoryName, $params, array('st_aplicadorprova' => 'ASC'));

        $this->_helper->json($this->negocio->toArrayEntity($results));
    }

    public function deleteAction() {
        try {
            $id = intval($this->getParam('id'));

            if (!is_int($id)) {
                $this->getResponse()->setHttpResponseCode(400);
                $this->getResponse()->setBody('Não posso apagar um registro com ID inválido!');
            } else {
                $entity = $this->negocio->find($this->repositoryName, $id);;
                $result = $this->negocio->delete($entity);
                $this->_helper->json(array(
                    'id' => $result->getId(),
                    'type' => 'success',
                    'title' => 'Ação bem sucedida!',
                    'text' => 'Registro ' . $entity->getSt_aplicadorprova() . ' removido.'
                ));

                $this->getResponse()->setHttpResponseCode(204)->appendBody('Registro ' . $entity->getSt_aplicadorprova() . ' removido');
            }
        } catch (Zend_Exception $e) {
            $this->getResponse()->setHttpResponseCode(400)->appendBody('Não foi possível apagar o registro' . $e);
        }
    }

    public function getAction() {
        $id = (int) $this->getRequest()->getParam('id');

        if ($id) {
            $negocio = new \G2\Negocio\Negocio();
            $result = $negocio->find('\G2\Entity\VwAplicadorProva', $id);
            $result = $negocio->toArrayEntity($result);
            /*$result['id_usuarioaplicador'] = is_null($result['id_usuarioaplicador']) ? null : $result['id_usuarioaplicador']['id_usuario'];
            $result['id_entidadeaplicador'] = (is_null($result['id_entidadeaplicador']) ? null : $result['id_entidadeaplicador']['id_entidade']);
            $result['id_usuariocadastro'] = is_null($result['id_usuariocadastro']) ? null : $result['id_usuariocadastro']['id_usuario'];
            $result['id_entidadecadastro'] = (is_null($result['id_entidadecadastro']) ? null : $result['id_entidadecadastro']['id_entidade']);*/
            $this->getResponse()->appendBody(json_encode($result));
        }
    }

    public function headAction() {

    }

    public function postAction() {

        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json_Decoder::decode($body);
        $result = $this->negocio->salvar($data); //salva
        $this->getResponse()->appendBody(json_encode($result));
        /*if ($result->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->appendBody(json_encode($result->getMensagem()));
        } else {
            $arrMensagem = array(
                'type' => 'error',
                'title' => 'Atenção',
                'text' => 'Preencha todos os campos obrigatórios!'
            );
            $this->getResponse()->appendBody(json_encode($arrMensagem));
        }*/
    }

    public function putAction() {

        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
        $dataValidate = $data;
        $dataValidate['st_aplicadorprova'] = trim(strip_tags($dataValidate['st_aplicadorprova']));
        $result = $this->negocio->salvar($data);
        $this->getResponse()->appendBody(json_encode($result));
        if($result->getTipo() == Ead1_IMensageiro::SUCESSO){
            $this->getResponse()->setHttpResponseCode(201);
        }else{
            $this->getResponse()->setHttpResponseCode(201);
        }


        /*if ($this->form->isValid($dataValidate)) {
            $result = $this->negocio->salvar($data);
            $this->getResponse()->appendBody(json_encode($result));
            $this->getResponse()->setHttpResponseCode(201);
        } else {
            $arrErros = $this->view->messages($this->form)->toString();
            if ($arrErros) {
                $arrErros = array(
                    'type' => 'error',
                    'title' => 'Atenção!',
                    'text' => $this->view->messages($this->form)->toString()
                );
            }
            $this->getResponse()->appendBody(json_encode($arrErros));
            $this->getResponse()->setHttpResponseCode(201);
        }*/
    }

}

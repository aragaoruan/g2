<?php

use G2\Negocio\TextoSistema;

/**
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Api_TextosController extends Ead1_RestRestrita
{

    /**
     * @var G2\Negocio\TextoSistema
     */
    private $negocio;
    private $form;

    public function init ()
    {
        parent::init();
        $this->negocio = new TextoSistema();
        $this->form = new TextosSistemaForm();
    }

    public function deleteAction ()
    {
        try {
            $id = intval($this->getParam('id'));

            if (!is_int($id)) {
                $this->getResponse()->setHttpResponseCode(400);
                $this->getResponse()->setBody('Não posso apagar um registro com ID inválido!');
            } else {
                $entity = $this->negocio->findTextoSistema($id);
                $result = $this->negocio->delete($entity);
                $this->_helper->json(array(
                    'id' => $result->getId_textosistema(),
                    'type' => 'success',
                    'title' => 'Sucesso!',
                    'text' => 'Registro ' . $result->getSt_textosistema() . ' removido.'
                ));
            }
        } catch (Zend_Exception $e) {
            $this->appendBody(array('title' => 'Erro de operação!', 'type' => 'error', 'text' => 'Erro ao apagar registro!'));
            $this->getResponse()->setHttpResponseCode(204);
        }
    }

    public function getAction ()
    {

    }

    public function headAction ()
    {

    }

    public function indexAction ()
    {
        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);
        $result = $this->negocio->findByEntidadeSessao($params);
        $arrReturn = array();
        if ($result) {
            foreach ($result as $row) {
                $arrReturn[] = array(
                    'id' => $row->getId_textosistema(),
                    'st_textosistema' => $row->getSt_textosistema(),
                    'dt_inicio' => $row->getDt_inicio(),
                    'dt_fim' => $row->getDt_fim(),
                    'st_texto' => $row->getSt_texto(),
                );
            }
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    /**
     * Metodo REST para salvar registro
     */
    public function postAction ()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
//remove esses parametros
        unset($data['id']); //, $data['id_textosistema'], $data['is_editing']);
        $this->form->getElement('id_textocategoria')->setRegisterInArrayValidator(false); //Particularidade para selects preechidos via ajax, força o valor recebido
        $dataValidate = $data;
        $dataValidate['st_texto'] = trim(strip_tags($dataValidate['st_texto'])); // trata o parametro de texto para enviar para o isValid() do zend form
        if ($this->form->isValid($dataValidate)) {//valida o form
            $result = $this->negocio->salvarTextoSistema($data); //salva
//pega o regorno do save e envia par ao backbone
            $this->getResponse()->appendBody(json_encode(array(
                'id' => $result->getId_textosistema(),
                'type' => 'success',
                'title' => 'Salvo com Sucesso',
                'text' => 'O registro foi salvo com sucesso!'
            )));
            $this->getResponse()->setHttpResponseCode(201);
        } else {
//se não form valido pega as mensagens de erro e manda para o backbone
            $arrErros = $this->view->messages($this->form)->toString();
            if ($arrErros) {
                $arrErros = array(
                    'title' => 'Atenção!',
                    'text' => $arrErros
                );
            }
            $this->getResponse()->appendBody(json_encode($arrErros));
            $this->getResponse()->setHttpResponseCode(201);
        }
    }

    /**
     * Metodo REST para update
     */
    public function putAction ()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
        $this->form->getElement('id_textocategoria')->setRegisterInArrayValidator(false);
        $dataValidate = $data;
        $dataValidate['st_texto'] = trim(strip_tags($dataValidate['st_texto']));
        if ($this->form->isValid($dataValidate)) {
            $result = $this->negocio->salvarTextoSistema($data);
            $this->getResponse()->appendBody(json_encode(array(
                'id' => $result->getId_textosistema(),
                'type' => 'success',
                'title' => 'Registro Atualizado',
                'text' => 'O registro foi atualizado com sucesso!'
            )));
            $this->getResponse()->setHttpResponseCode(201);
        } else {
            $arrErros = $this->view->messages($this->form)->toString();
            if ($arrErros) {
                $arrErros = array(
                    'title' => 'Atenção!',
                    'text' => $this->view->messages($this->form)->toString()
                );
            }
            $this->getResponse()->appendBody(json_encode($arrErros));
            $this->getResponse()->setHttpResponseCode(201);
        }
    }

}

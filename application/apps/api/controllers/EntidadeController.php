<?php

/**
 * Controller para Entidades
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @package default
 * @subpackage controller
 * @since 2013-12-09
 */
use G2\Negocio\Entidade;

class Api_EntidadeController extends Ead1_RestRestrita
{

    private $_negocio;

    public function init()
    {
        parent::init();
        $this->_negocio = new Entidade();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        $orderBy = $this->getParam('order_by') ? array($this->getParam('order_by') => $this->getParam('sort') ? $this->getParam('sort') : 'ASC') : null;
        unset($params['module'], $params['controller'], $params['action'], $params['order_by'], $params['sort'], $params['search'], $params['search']);
        $paramsLike = $this->getParam('search');

        $mensageiro = $this->_negocio->findBySearch('\G2\Entity\Entidade', $paramsLike, $params, $orderBy);
        foreach ($mensageiro as $key => &$entity) {
            $entity = $this->_negocio->toArrayEntity($entity);
        }

        $this->_helper->json($mensageiro);
    }

    public function deleteAction()
    {

    }

    /**
     * Metodo Get API, retorna Entidade
     * @param GET 'str'
     */
    public function getAction()
    {

        $arrParams = $this->getAllParams(); //pega todos os parametros passado via url

        unset($arrParams['controller'], $arrParams['module'], $arrParams['action']);
        //busca seta os parametros para consultar
        $return = $this->_negocio->retornaPesquisarEntidade($arrParams);
        //trata o retorno
        $arrReturn = array();
        if ($return) {
            foreach ($return as $row) {
                $arrReturn[] = array(
                    'id' => $row->getId_entidade(),
                    'id_entidade' => $row->getId_entidade(),
                    'st_nomeentidade' => $row->getSt_nomeentidade(),
                    'st_urlimglogo' => $row->getSt_urlimglogo(),
                    'bl_acessasistema' => $row->getBl_acessasistema(),
                    'nu_cnpj' => $row->getNu_cnpj(),
                    'bl_ativo' => $row->getbl_ativo(),
                    'id_situacao' => $row->getId_situacao(),
                    'st_razaosocial' => $row->getSt_razaosocial(),
                    'st_cnpj' => $row->getSt_cnpj()
                );
            }
        }
        $this->getResponse()
            ->appendBody(json_encode($arrReturn));
    }

    public function headAction()
    {

    }

    public function postAction()
    {

    }

    public function putAction()
    {

    }

}
<?php
/**
 * Controller para gerenciar VwOcorrencia
 * @author Marcus Pereira <marcus.pereira@unyleya.com.br>
 * @since 2015-07-21
 * @package application
 * @subpackage controller
 */
class Api_VwOcorrenciaController extends Ead1_RestRestrita
{
    /**
     * @var \G2\Negocio\Negocio $negocio
     */
    private $negocio;
    private $repositoryName = '\G2\Entity\VwOcorrencia';

    public function init()
    {
        parent::init();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->negocio = new \G2\Negocio\Ocorrencia();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        $params = self::clearRouteParam($params);
        self::validate($params);

        $result = $this->negocio->findByOcorrenciaMatricula(
            self::paramByLike($params),
            self::paramByOrder($params)
        );

        $this->_helper->json($this->parseResult($result));
    }

    public function getAction()
    {
        $mensageiro = $this->negocio->find($this->repositoryName, $this->getParam('id'));

        if ($mensageiro) {
            $this->_helper->json($this->negocio->toArrayEntity($mensageiro));
        }
    }

    private static function paramByLike(array $params)
    {
        $ignore = array('order_by', 'sort');
        $return = array();

        foreach ($params as $key => $value) {
            if (isset($ignore[$key])) {
                continue;
            }

            $return[$key] = $value;
        }

        return $return;
    }

    private function paramByOrder(array $params)
    {
        return $this->getParam('order_by')
               ? array(
                    $this->getParam('order_by') => $this->getParam('sort')
                    ? $this->getParam('sort')
                    : 'ASC'
                 )
               : array('id_ocorrencia' => 'ASC')
               ;
    }

    private function validate(array $params)
    {
        if (isset($params['id_entidade']) && isset($params['id_matricula'])) {
            return;
        }

        # https://tools.ietf.org/html/rfc4918#section-11.2
        $this->getResponse()->setHttpResponseCode(self::RESPONSE_CODE_BAD_REQUEST);

        $this->_helper->json(array(
            'message' => 'um ou mais parâmetro requerito não foi informado',
            'data'    => array(),
            'code'    => self::RESPONSE_CODE_BAD_REQUEST,
        ));
    }

    /**
     * remove os paramentros que não são usados no négocio
     *
     * @param  array $param
     * @return array
     * */
    private static function clearRouteParam(array $param)
    {
        unset(
            $param['module'],
            $param['controller'],
            $param['action']
        );

        return $param;
    }

    private function parseResult(array $ocorrencias)
    {
        $processIt = array(
            'id_ocorrencia' => function ($data) {
                return preg_replace(
                    '/(\d{0,3})(\d{1,3})/'
                    , '$1.$2'
                    , $data);
            },
            'st_assuntocopai' => null,
            'st_assuntoco' => null,
            'st_titulo' => null,
            'st_evolucao' => null,
            'st_situacao' => null,
            'dt_cadastro' => function ($data) {
                return preg_replace(
                    '/(\d{4})-(\d{1,2})-(\d{1,2})\s+(\d{1,2}):(\d{1,2}):(\d{1,2})\.0*/'
                    , '$3/$2/$1 às $4:$5:$6'
                    , $data
                );
            }
        );

        $returnData = array();

        foreach ($ocorrencias as $key => $ocorrencia) {

            $arrResult = $this->negocio->toArrayEntity($ocorrencia);

            foreach ($processIt as $elm => $callback) {
                $swap = $arrResult[$elm];

                if ($callback) {
                    $swap = $callback($swap);
                }

                $returnData[$key][$elm] = $swap;
            }
        }

        return $returnData;
    }
}
<?php

/**
 * Controller para gerenciar o vinculo dos produtos com as entidades
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @since 2014-05-15
 * @package application
 * @subpackage controller
 */
class Api_ProdutoEntidadeController extends Ead1_RestRestrita
{
    
    private $doctrineContainer;
    private $em;
    
    public function init ()
    {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
    }
    
     public function indexAction()
    {
    	$retorno 		= array();
    	$result 		= array();
    	$id_produto 	= $this->_getParam('id_produto', null);
    	$id_entidade 	= $this->_getParam('id_entidade', null);
    	
    	if($id_produto){
    		$result = $entity = $this->em->getRepository('G2\Entity\ProdutoEntidade')->findBy(array('id_produto' => $id_produto));
    	} elseif($id_entidade){
    		$result = $entity = $this->em->getRepository('G2\Entity\ProdutoEntidade')->findBy(array('id_entidade' => $id_entidade));
    	}
    	
    	if($result){
    		foreach ($result as $entity){
    			if($entity instanceof G2\Entity\ProdutoEntidade){
    				$retorno[] = $entity->toBackboneArray();
    			}
    		}
    	}
    	
    	$this->_helper->json($retorno);
    	
    }

    public function getAction()
    {
//     	$result = $entity = $this->em->getRepository('G2\Entity\ProdutoEntidade')->find($this->_getParam('id', null));
//     	Zend_Debug::dump($result,__CLASS__.'('.__LINE__.')');exit;
        
    }
    
    public function postAction()
    {
        $body       = $this->getRequest()->getRawBody();
        $data       = Zend_Json::decode($body);
    	// salvar 
    	$bo = new \G2\Negocio\Produto();
    	$bo->vinculaEntidadeProdutoWS ($data['id_entidade'], $data['id_produto']);
    	
    }
    
    public function putAction()
    {
//         try {

//             $body       = $this->getRequest()->getRawBody();
//             $data       = Zend_Json::decode($body);
//             $produto   = new \G2\Negocio\Produto();
            
//             $this->getResponse()
//                     ->appendBody($produto->salvarProdutoEntidade($data)->toJson());
            
//         } catch (Exception $exc) {
//             $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
//             $this->getResponse()
//                     ->appendBody($mensageiro->toJson());
//         }

    }
    
    public function deleteAction(){
        
    }

    public function headAction(){
        
    }
    

}
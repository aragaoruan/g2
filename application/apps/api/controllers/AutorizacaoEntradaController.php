<?php

/**
 * Controller para criar registros de autorizacao de entrada
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since  2015-06-05
 * @package Api
 * @subpackage controller
 */
class Api_AutorizacaoEntradaController extends Ead1_RestRestrita
{

    private $negocio;

    public function init ()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\AutorizacaoEntrada();
    }

    public function deleteAction ()
    {
        
    }

    public function getAction ()
    {
        
    }

    public function headAction ()
    {
        
    }

    public function indexAction ()
    {
        
    }

    public function postAction ()
    {
        try{
            $this->_helper->viewRenderer->setNoRender(true);
            $body = $this->getRequest()->getRawBody();
            $data = Zend_Json::decode($body);

            $result = $this->negocio->salvarAutorizacaoEntrada($data);
            if ($result) {
                $arrReturn = array(
                    'id_autorizacaoentrada' => $result->getId_autorizacaoentrada(),
                    'id_usuario' => $result->getId_usuario() ? $result->getId_usuario()->getId_usuario() : null,
                    'id_usuariocadastro' => $result->getId_usuariocadastro() ? $result->getId_usuariocadastro()->getId_usuario() : null,
                    'id_textosistema' => $result->getId_textosistema() ? $result->getId_textosistema()->getId_textosistema() : null,
                    'id_entidade' => $result->getId_entidade() ? $result->getId_entidade()->getId_entidade() : null,
                    'dt_cadastro' => $result->getDt_cadastro(),
                    'bl_ativo' => $result->getBl_ativo()
                );

                $this->getResponse()->appendBody(json_encode($arrReturn));
                $this->getResponse()->setHttpResponseCode(201);
            } else {
                $this->getResponse()->appendBody(json_encode("Erro ao salvar autorizacao."));
                $this->getResponse()->setHttpResponseCode(400);
            }
        }catch(Exception $e) {
            throw new Zend_Exception($e->getMessage());
        }

    }

    public function putAction ()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
    }

}

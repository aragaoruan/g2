<?php

/**
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2015-06-05
 * @package application
 * @subpackage controller
 */
class Api_VwEmissoesAutorizacaoEntradaController extends Ead1_RestRestrita
{

    private $negocio;

    public function init ()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\AutorizacaoEntrada();
    }

    public function indexAction ()
    {
        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);
    }

    public function getAction ()
    {
        $result = $this->negocio->retornaVwAutorizacaoEntradaByUsuario($this->getParam('id'));
        if ($result) {
            $result = $this->negocio->toArrayEntity($result);
        }

        $this->getResponse()
                ->appendBody(json_encode($result));
    }

    public function postAction ()
    {
        
    }

    public function putAction ()
    {
        
    }

    public function deleteAction ()
    {
        
    }

    public function headAction ()
    {
        
    }

}

<?php

/**
 * Controller API para Secretaria > Material > Item Material
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since  24-10-2014
 * @package application
 * @subpackage controller
 */
class Api_ItemDeMaterialController extends Ead1_RestRestrita
{
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\ItemDeMaterial();
    }

    public function indexAction()
    {
//        $data = $this->getRequest()->getQuery();
//
//        $mensageiro = $this->negocio->findByEntidade($data);
//
//        $this->getResponse()->appendBody(json_encode($mensageiro));
    }

    public function getAction()
    {
        $id = (int) $this->getRequest()->getParam('id');

        if ($id) {
            $negocio = new \G2\Negocio\Negocio();

            $result = $negocio->find('\G2\Entity\ItemDeMaterial', $id);
            $st_upload = $result->getId_upload()?$result->getId_upload()->getSt_upload():'';
            $result = $negocio->toArrayEntity($result);
            $result['st_upload'] = $st_upload;
//            $result['disciplinas'] = array();
            $result['projetosPedagogicos'] = array();


            // Buscar disciplinas associadas
//            $disciplinas = $negocio->findBy('\G2\Entity\ItemDeMaterialDisciplina', array(
//                'id_itemdematerial' => $id
//            ));
//            foreach ($disciplinas as $disciplina) {
//                $result['disciplinas'][] = array('id_disciplina'=>$disciplina->getId_disciplina()->getId_disciplina(),'st_disciplina'=>$disciplina->getId_disciplina()->getSt_disciplina());
//            }

//            // Buscar projetos pedagogicos associados
//            $projetos = $negocio->findBy('\G2\Entity\MaterialProjeto', array(
//                'id_itemdematerial' => $id,
//                'id_usuariocadastro' => $negocio->sessao->id_usuario
//            ));
//            foreach ($projetos as $projeto) {
//                $result['projetosPedagogicos'][] = $projeto->getId_projetopedagogico();
//            }

            $this->getResponse()->appendBody(json_encode($result));
        }
    }

    public function postAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);

        $mensageiro = $this->negocio->save($data);

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->appendBody(json_encode($mensageiro->toArray()));
            $this->getResponse()->setHttpResponseCode(200);
        } else {
            $this->getResponse()->appendBody(json_encode(array('erro' => $mensageiro->getMensagem())));
            $this->getResponse()->setHttpResponseCode(500);
        }
    }

    public function putAction()
    {
        $this->postAction();
    }

    public function deleteAction()
    {
        $id = (int) $this->getParam('id');

        try{
            $negocio = new \G2\Negocio\Negocio();
            $entity = $this->negocio->find('G2\Entity\ItemDeMaterial', $this->getParam('id'));

            // Buscar ItemDeMaterialDisciplina e remover todos
            // para poder inserir somente os selecionados
            $ItemsEntity = $negocio->findBy('\G2\Entity\ItemDeMaterialDisciplina', array(
                'id_itemdematerial' => $id
            ));
            foreach ($ItemsEntity as $ItemEntity) {
                $negocio->delete($ItemEntity);
            }


            // Buscar MaterialProjeto e remover todos
            // para poder inserir somente os selecionados
            $ItemsEntity = $negocio->findBy('\G2\Entity\MaterialProjeto', array(
                'id_itemdematerial' => $id,
                'id_usuariocadastro' => $negocio->sessao->id_usuario
            ));
            foreach ($ItemsEntity as $ItemEntity) {
                $negocio->delete($ItemEntity);
            }

            $this->negocio->delete($entity);
            $this->getResponse()->setHttpResponseCode(204);
        } catch (Zend_Exception $e) {
            $this->getResponse()->setHttpResponseCode(500);
        }
    }

    public function headAction()
    {
    }

}

<?php

class Api_ArquivoRetornoController extends Ead1_RestRestrita
{
    /** @var $doctrineContainer Bisna\Doctrine\Container */
    private $doctrineContainer;
    /** @var $em Doctrine\ORM\EntityManager */
    private $em;
    /** @var $bo ArquivoRetornoBO */
    private $bo;

    public function init (){
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
        $this->bo = new ArquivoRetornoBO();
    }

    /**
     * The index action handles index/list requests; it should respond with a
     * list of the requested resources.
     */
    public function indexAction(){

    }

    /**
     * The get action handles GET requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function getAction(){

        $arquivoRetornoBO = new ArquivoRetornoBO();
        $mensageiro = $arquivoRetornoBO->retornoLancamentos($this->getAllParams());

        $arrVwRetornoTo = $mensageiro->getMensagem();
        $valorTotal = 0;
        /** @var $to \G2\Entity\VwRetornoLancamento */
        foreach($arrVwRetornoTo as &$to){
            $valorTotal = $valorTotal+$to['nu_valor'];
        }

        /** @var $to \G2\Entity\VwRetornoLancamento */
        foreach($arrVwRetornoTo as &$to){
            $to['bl_quitado'] =  $to['bl_quitado'] ? 'SIM' : 'N&Atilde;O';
            if (!is_null($to['dt_quitado'])){
                $dt_quitado = new \DateTime($to['dt_quitado']);
                $to['dt_quitado'] = $dt_quitado->format('d/m/Y');
            }
            if (!is_null($to['dt_vencimento'])){
                $dt_quitado = new \DateTime($to['dt_vencimento']);
                $to['dt_vencimento'] = $dt_quitado->format('d/m/Y');
            }
            if($to['nu_valorretorno']){
                $to['nu_valorretorno'] = number_format($to['nu_valorretorno'], 2, ',', '.');
            }
            if($to['nu_valor']){
                $to['nu_valor'] = number_format($to['nu_valor'], 2, ',', '.');
            }
            if($to['nu_valornominal']){
                $to['nu_valornominal'] = number_format($to['nu_valornominal'], 2, ',', '.');
            }
            $to['nu_valortotal'] = number_format($valorTotal, 2, ',', '.');
        }

        $this->_helper->json($arrVwRetornoTo);
    }

    /**
     * The head action handles HEAD requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function headAction()
    {
        // TODO: Implement headAction() method.
    }

    /**
     * The post action handles POST requests; it should accept and digest a
     * POSTed resource representation and persist the resource state.
     */
    public function postAction()
    {
        // TODO: Implement postAction() method.
    }

    /**
     * The put action handles PUT requests and receives an 'id' parameter; it
     * should update the server resource state of the resource identified by
     * the 'id' value.
     */
    public function putAction()
    {
        // TODO: Implement putAction() method.
    }

    /**
     * The delete action handles DELETE requests and receives an 'id'
     * parameter; it should update the server resource state of the resource
     * identified by the 'id' value.
     */
    public function deleteAction()
    {
        // TODO: Implement deleteAction() method.
    }
}
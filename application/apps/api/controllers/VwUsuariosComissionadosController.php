<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of VwUsuariosComissionadosController
 *
 * @author kayo.silva
 */
class Api_VwUsuariosComissionadosController extends Ead1_RestRestrita
{
    /**
     * @var G2\Negocio\PagamentoColaborador
     */
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new G2\Negocio\PagamentoColaborador();
    }

    public function deleteAction()
    {

    }

    public function getAction()
    {
        $params = $this->getAllParams();
        unset($params['module'], $params['controller'], $params['action']);
        $result = $this->negocio->retornarUsuariosComissionados($params, true);
        $this->getResponse()
            ->appendBody(json_encode($result));
    }

    public function headAction()
    {

    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['module'], $params['controller'], $params['action']);
        $result = $this->negocio->retornarUsuariosComissionados($params, true);
        $this->getResponse()
            ->appendBody(json_encode($result));
    }

    public function postAction()
    {

    }

    public function putAction()
    {

    }

}

<?php

/**
 * Controller para VwAplicadorProva
 * @author Debora Castro <debora.castro@unyleya.com.br>
 * @package application
 * @subpackage controller
 */
class Api_VwAplicadorProvaController extends Ead1_RestRestrita
{

    /*
     * @var Negocio AplicadorProva
     */
    private $_negocio;

    public function init()
    {
        parent::init();
        $this->_negocio = new \G2\Negocio\AplicadorProva();
    }

    public function indexAction()
    {

        $array = array();

        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);

        if ($params) {

            $result = $this->_negocio->findByVwAplicadorProva($params);

            if ($result->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $array = $result->getMensagem();

            }
        }

        $this->_helper->json($array);
    }

    public function getAction()
    {


    }

    public function postAction()
    {

    }

    public function putAction()
    {

    }

    public function deleteAction()
    {

    }

    public function headAction()
    {

    }

}

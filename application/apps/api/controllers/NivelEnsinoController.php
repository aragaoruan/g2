<?php

/**
 * Controller para gerenciar local de aula
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @since 2013-11-21
 * @package application
 * @subpackage controller
 */
class Api_NivelEnsinoController extends Ead1_RestRestrita
{

    private $negocio;
    private $form;

    public function init ()
    {
        parent::init();
       $this->negocio = new \G2\Negocio\NivelEnsino();
    }

    public function indexAction ()
    {
        
    	$mensageiro = $this->negocio->findAll();
    	
    	if($mensageiro->getTipo()!=Ead1_IMensageiro::SUCESSO){
    		$this->getResponse()->setHttpResponseCode(400);
    		$this->_helper->json($mensageiro);
    	} else {
    		$this->getResponse()->setHttpResponseCode(201);
    		$this->_helper->json($mensageiro->getMensagem());
    	}
    	
    }

    public function getAction ()
    {
        
    }

    public function postAction ()
    {
        
    }

    public function putAction ()
    {

       
    }

    public function deleteAction ()
    {
        
    }

    public function headAction ()
    {
        
    }

}
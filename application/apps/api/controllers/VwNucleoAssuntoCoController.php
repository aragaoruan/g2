<?php

/**
 * Controller para gerenciar os núcleos de assunto
 * @author Paulo Silva <paulo.silva@unyleya.com.br>
 * @since 2014-06-11
 * @package application
 * @subpackage controller
 */
class Api_VwNucleoAssuntoCoController extends Ead1_RestRestrita
{


    public function indexAction ()
    {  
        $negocio = new G2\Negocio\Ocorrencia();
        try {
            $result = $negocio->findNucleoAssuntoCo(array('id_tipoocorrencia'=>3));
            
           
            $arr = array();
           
            $arr[] = array('id_nucleoco'=>'','st_nucleoco'=>'- Selecione -');
            
            foreach($result as $resultado) {
                $arr[] = $negocio->toArrayEntity($resultado);
            }

            $this->_helper->json($arr);
            
        } catch (Exception $e) {
 
        }
    	
    }

    public function getAction ()
    {
        
    }

    public function postAction ()
    {
        
    }

    public function putAction ()
    {

       
    }

    public function deleteAction ()
    {
        
    }

    public function headAction ()
    {
        
    }

}
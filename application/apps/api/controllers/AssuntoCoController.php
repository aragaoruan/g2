<?php


/**
 * Controller API para cadastro de assunto da central de Atençaõ
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @since  23-10-2014
 * @package application
 * @subpackage controller
 */
class Api_AssuntoCoController extends Ead1_RestRestrita {


    /**
     * The index action handles index/list requests; it should respond with a
     * list of the requested resources.
     */
    public function indexAction()
    {
        $assuntoNegocio = new \G2\Negocio\AssuntoCo();
        $mensageiro = $assuntoNegocio->findAssuntoCo($assuntoNegocio->sessao->id_entidade, false);
        echo json_encode($mensageiro);
    }

    /**
     * The get action handles GET requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function getAction()
    {
        $assuntoNegocio = new \G2\Negocio\AssuntoCo();
        $mensageiro = $assuntoNegocio->findAssuntoCo($this->getParam('id', null));

        if($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO){
            /** @var \G2\Negocio\AssuntoCo $obj */
            $obj = $mensageiro->getFirstMensagem();
           //$obj = new \G2\Entity\AssuntoCo();
            //Zend_Debug::dump($obj->getId_textosistema());die;
            $return = array(
                'id_assuntoco' => $obj->getId_assuntoco(),
                'st_assuntoco' => $obj->getSt_assuntoco(),
                'id_situacao' => $obj->getId_situacao() instanceof \G2\Entity\Situacao ? $obj->getId_situacao()->getId_situacao() : '' ,
                'id_assuntocopai' => $obj->getId_assuntocopai() instanceof \G2\Entity\AssuntoCo ? $obj->getId_assuntocopai()->getId_assuntoco() : null,
                'id_entidadecadastro' => $obj->getId_entidadecadastro() instanceof \G2\Entity\Entidade ? $obj->getId_entidadecadastro()->getId_entidade() : null,
                'id_usuariocadastro'  => $obj->getId_usuariocadastro() instanceof \G2\Entity\Usuario ? $obj->getId_usuariocadastro()->getId_usuario() : '',
                'id_tipoocorrencia' => $obj->getId_tipoocorrencia() instanceof \G2\Entity\TipoOcorrencia ? $obj->getId_tipoocorrencia()->getId_tipoocorrencia() : null,
                'bl_autodistribuicao' =>  $obj->getBl_autodistribuicao(),
                'bl_abertura' => $obj->getBl_abertura(),
                'dt_cadastro' => $obj->getDt_cadastro() instanceof DateTime ? $obj->getDt_cadastro() : $obj->getDt_cadastro(),
                'bl_ativo' => $obj->getBl_ativo(),
                'id_textosistema' => $obj->getId_textosistema() instanceof \G2\Entity\TextoSistema ? $obj->getId_textosistema()->getId_textosistema() : $obj->getId_textosistema(),
                'bl_cancelamento' => $obj->getBl_cancelamento(),
                'bl_trancamento' => $obj->getBl_trancamento(),
                'st_assuntocopai' => $obj->getId_assuntocopai() instanceof \G2\Entity\AssuntoCo ? $obj->getId_assuntocopai()->getSt_assuntoco() : null,
                'id_assuntocategoria' => $obj->getId_assuntocategoria(),
                'bl_interno' => $obj->getBl_interno(),
                'bl_recuperacao_resgate' => $obj->getBl_recuperacao_resgate(),
            );
        }else{
            $this->getResponse()->setHttpResponseCode(400);
            $return = array();
        }

        echo json_encode($return);
    }

    /**
     * The head action handles HEAD requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function headAction()
    {
        // TODO: Implement headAction() method.
    }

    /**
     * The post action handles POST requests; it should accept and digest a
     * POSTed resource representation and persist the resource state.
     */
    public function postAction()
    {
        // TODO: Implement postAction() method.
    }

    /**
     * The put action handles PUT requests and receives an 'id' parameter; it
     * should update the server resource state of the resource identified by
     * the 'id' value.
     */
    public function putAction()
    {
        // TODO: Implement putAction() method.
    }

    /**
     * The delete action handles DELETE requests and receives an 'id'
     * parameter; it should update the server resource state of the resource
     * identified by the 'id' value.
     */
    public function deleteAction()
    {
        // TODO: Implement deleteAction() method.
    }


}

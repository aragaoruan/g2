<?php

/**
 * Controller modolo API para gerenciar categoria
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-03-11
 */
class Api_CampanhaCategoriasController extends Ead1_RestRestrita
{

    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new G2\Negocio\CampanhaComercial();
    }

    public function deleteAction()
    {
        $params = $this->getParam('id');

        $result = $this->negocio->deleteCampanhaCategoria($params);

        if ($result->getType() == Ead1_IMensageiro::TYPE_SUCESSO) {
            $this->getResponse()->setHttpResponseCode(204)->appendBody('Registro removido');
        } else {
            $this->getResponse()->setHttpResponseCode(400)->appendBody('Erro ao tentar remover registro ' . $result->getText());
        }

    }

    public function getAction()
    {
        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);
        $arrResult = array();
        $result = $this->negocio->retornaCampanhaCategorias($params);
        if ($result) {
            foreach ($result as $categoria) {
                $arrResult[] = array(
                    'id' => $categoria->getId_campanhacategoris(),
                    'id_campanhacategoris' => $categoria->getId_campanhacategoris(),
                    'id_categoria' => $categoria->getId_categoria() ? $categoria->getId_categoria()->getId_categoria() : NULL,
                    'st_categoria' => $categoria->getId_categoria() ? $categoria->getId_categoria()->getSt_categoria() : NULL,
                    'id_campanhacomercial' => $categoria->getId_campanhacomercial() ? $categoria->getId_campanhacomercial()->getId_campanhacomercial() : NULL,
                );
            }
        }
        $this->getResponse()
            ->appendBody(json_encode($arrResult));
    }

    public function headAction()
    {

    }

    public function indexAction()
    {

    }

    public function postAction()
    {

    }

    public function putAction()
    {

    }

}

<?php

/**
 * @package application
 * @subpackage controller
 */
class Api_VwClienteVendaController extends Ead1_RestRestrita
{

    private $doctrineContainer;
    private $em;
    private $bo;

    public function init()
    {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
        $this->bo = new G2\Negocio\VwClienteVenda();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();

        foreach ($params as $k => $p) {
            if (!$p || in_array($k, array('action', 'controller', 'module'))) {
                unset($params[$k]);
            }
        }

        $array = $this->bo->pesquisar($params);
        $retorno = array();

        foreach ($array as $key => $vw) {
            $retorno[$key] = $this->bo->toArrayEntity($vw);

            $vpsTO = new VendaProdutoTO();
            $vpsTO->setId_venda($vw->getId_venda());
            $vpsTO = $vpsTO->fetch();

            //Concatena todos os produtos da venda para mostrar na pesquisa
            $cursos = '';
            if (is_array($vpsTO)) {
                foreach ($vpsTO as $vp) {
                    $pTO = new ProdutoTO();
                    $pTO->setId_produto($vp->getId_produto());
                    $pTO->fetch(true, true, true);
                    $cursos .= $pTO->getSt_produto() . '&#13;&#10;';
                }
            } else {
                $cursos = $vpsTO->getSt_produto();
            }

            $retorno[$key]['st_projetopedagogico'] = $cursos;
        }

        $this->getResponse()
            ->appendBody(json_encode($retorno));
    }

    public function getAction()
    {

        $mensageiro = $this->bo->findById($this->_getParam('id', null));

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $mensageiro->setText(count($mensageiro->getMensagem()) . ' registros encontrados!');
        }

        $this->getResponse()->appendBody(json_encode($mensageiro->getFirstMensagem()));
    }

    public function postAction()
    {

    }

    public function putAction()
    {

    }

    public function deleteAction()
    {
        try {
            $this->bo->delete($this->bo->find('G2\Entity\RegraPagamento', $this->getParam('id')));
            $this->getResponse()->setHttpResponseCode(204);
        } catch (Exception $e) {
            $this->getResponse()->setHttpResponseCode(500);
        }
    }

    public function headAction()
    {

    }

}

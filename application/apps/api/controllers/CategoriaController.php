<?php

/**
 * Controller para Categorias
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @update 2014-11-10 Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2014-03-21
 * @package application
 * @subpackage controller
 */
class Api_CategoriaController extends Ead1_RestRestrita
{

    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Categoria();
    }

    public function indexAction()
    {
        $mensageiro = $this->negocio->retornarCategoriaEntidade(array(
            'bl_ativo' => 1
        ));

        $this->_helper->json($mensageiro);

//      ******************CODIGO ANTES UTILIZADO****************
//    	if(isset($_GET['bl_ativo'])){
//    		$param['bl_ativo'] = (boolean)$_GET['bl_ativo'];
//    	}
//    	
//    	$to = new CategoriaTO();
//    	$to->montaToDinamico($_GET);
//    	$to->setId_entidadecadastro($to->getSessao()->id_entidade);
//    	
//    	$bo = new CategoriaBO();
//    	$mensageiro = $bo->retornarCategoria($to);
//
//    	$retorno = array();
//    	
//    	if($mensageiro->getTipo()==Ead1_IMensageiro::SUCESSO){
//    		foreach($mensageiro->getMensagem() as $categoria){
//				if($categoria instanceof CategoriaTO){
//					$retorno[] = $categoria->toBackboneArray();
//				}    			
//    		}
//    	}
//        **********************************************************
//    	if($retorno){
//    		$this->getResponse()->setHttpResponseCode(201);
//    	} else {
//	    	$this->getResponse()->setHttpResponseCode(400);
//    	}
//    	$this->_helper->json($retorno);
    }

    public function getAction()
    {
        $id = $this->getParam('id');
        if ($id) {
            $negocio = new \G2\Negocio\Negocio();
            $mensageiro = $negocio->find('\G2\Entity\Categoria', $id);
            if ($mensageiro) {
                $result = array(
                    'id_categoria' => $mensageiro->getId_categoria(),
                    'id_entidadecadastro' => $mensageiro->getId_entidadecadastro(),
                    'st_nomeexibicao' => $mensageiro->getSt_nomeexibicao(),
                    'st_categoria' => $mensageiro->getSt_categoria(),
                    'dt_cadastro' => $mensageiro->getDt_cadastro(),
                    'bl_ativo' => $mensageiro->getBl_ativo(),
                    'id_usuariocadastro' => $mensageiro->getId_usuariocadastro() ? $mensageiro->getId_usuariocadastro()->getId_usuario() : null,
                    'id_uploadimagem' => $mensageiro->getId_uploadimagem() ? $mensageiro->getId_uploadimagem()->getId_upload() : null,
                    'id_categoriapai' => $mensageiro->getId_categoriapai() ? $mensageiro->getId_categoriapai()->getId_categoria() : null,
                    'id_situacao' => $mensageiro->getId_situacao() ? $mensageiro->getId_situacao()->getId_situacao() : null
                );

                $this->getResponse()->appendBody(json_encode($result));
            } else {
                $this->getResponse()->setHttpResponseCode(200);
            }
        }
    }

    public function postAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);

        $mensageiro = $this->negocio->save($data);

//        Zend_Debug::dump($mensageiro->getMensagem());
//        exit;

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->appendBody(json_encode($mensageiro->getMensagem()));
            $this->getResponse()->setHttpResponseCode(201);
        } else {
            $this->getResponse()->appendBody(json_encode(array('erro' => $mensageiro->getMensagem())));
            $this->getResponse()->setHttpResponseCode(400);
        }
    }

    public function putAction()
    {
        $this->postAction();
    }

    public function deleteAction()
    {
        $id = (int)$this->getRequest()->getParam('id');

        if ($id) {

            /**
             * CategoriaEntidade
             */
            // Buscar CategoriaEntidade e remover todos
            $ItemsEntity = $this->negocio->findBy('\G2\Entity\CategoriaEntidade', array(
                'id_categoria' => $id
            ));
            // Apagar cada registro encontrado
            foreach ($ItemsEntity as $ItemEntity) {
                $this->negocio->delete($ItemEntity);
            }


            // Remover todas as categorias que tem essa como categoria pai
            $ItemsEntity = $this->negocio->findBy('\G2\Entity\Categoria', array(
                'id_categoriapai' => $id
            ));
            $negocio = new \G2\Negocio\Negocio();
            foreach ($ItemsEntity as $ItemEntity) {
                $ItemEntity->setId_categoriapai(null);
                $negocio->save($ItemEntity);
            }

            // Finalmente apagar a categoria selecionada
            $entity = $this->negocio->find('\G2\Entity\Categoria', $id);

            try {
                $this->negocio->delete($entity);
                $this->getResponse()->setHttpResponseCode(204);
            } catch (Zend_Exception $e) {
                $this->getResponse()->setHttpResponseCode(500);
            }

        }
    }

    public function headAction()
    {

    }

}
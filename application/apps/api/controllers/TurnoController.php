<?php

/**
 * Class Api_TurnoController
 * Classe Controller para Turno
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Api_TurnoController extends Zend_Rest_Controller
{

    private $negocio;

    public function init()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->negocio = new \G2\Negocio\Turno();
    }

    public function indexAction()
    {
        $result = $this->negocio->findAll();
        $this->getResponse()
            ->appendBody(json_encode($result));
    }

    public function getAction()
    {
        $params = $this->getAllParams();
        unset($params['controller'], $params['module'], $params['action']);
        $result = $this->negocio->findByTurno($params, true);
        $this->getResponse()
            ->appendBody(json_encode($result));
    }

    /**
     * The head action handles HEAD requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function headAction()
    {
        // TODO: Implement headAction() method.
    }

    /**
     * The post action handles POST requests; it should accept and digest a
     * POSTed resource representation and persist the resource state.
     */
    public function postAction()
    {
        // TODO: Implement postAction() method.
    }

    /**
     * The put action handles PUT requests and receives an 'id' parameter; it
     * should update the server resource state of the resource identified by
     * the 'id' value.
     */
    public function putAction()
    {
        // TODO: Implement putAction() method.
    }

    /**
     * The delete action handles DELETE requests and receives an 'id'
     * parameter; it should update the server resource state of the resource
     * identified by the 'id' value.
     */
    public function deleteAction()
    {
        // TODO: Implement deleteAction() method.
    }
}
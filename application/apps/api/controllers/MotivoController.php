<?php

/**
 * Controller para gerenciar os Motivos
 * @author Helder Silva
 * <helder.silva@unyleya.com.br>
 * @since 2015-01-11
 * @package application
 * @subpackage controller
 */
class Api_MotivoController extends Ead1_RestRestrita
{

    private $_negocio;

    public function init()
    {
        parent::init();
        $this->_negocio = new \G2\Negocio\Motivo();
    }

    public function indexAction()
    {
        $result = $this->_negocio->retornarMotivos();
        $this->_helper->json($result);
    }

    public function getAction()
    {


    }

    public function postAction()
    {


    }

    public function putAction()
    {

    }

    public function deleteAction()
    {
        $id = intval($this->getParam('id'));
        if (!is_int($id)) {
            $this->getResponse()->setHttpResponseCode(400)->setBody('Não posso apagar um registro com ID inválido!');
        } else {
            $result = $this->_negocio->deleteMotivo($id);
            if ($result) {
                $this->getResponse()->setHttpResponseCode(204)->appendBody('Registro removido');
            } else {
                $this->getResponse()->setHttpResponseCode(400)->appendBody('Erro ao tentar remover registro');
            }
        }

    }

    public function headAction()
    {

    }


}
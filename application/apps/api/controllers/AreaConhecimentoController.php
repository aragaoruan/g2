<?php

/**
 * Controller para gerenciar local de aula
 * @author Débora Castro <debora.castro@unyleya.com.br>
 * @since 2013-09-17
 * @package application
 * @subpackage controller
 */
class Api_AreaConhecimentoController extends Ead1_RestRestrita
{

    /** @var $negocio \G2\Negocio\AreaConhecimento */
    private $negocio;
    private $form;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\AreaConhecimento();
        $this->form = new AreaConhecimentoForm();
    }

    public function indexAction()
    {
        $id_origemareaconhecimento = $this->getParam('id_origemareaconhecimento');
        $result = null;

        if ($id_origemareaconhecimento) {
            $result = $this->negocio->findByEntidadeSessao(null, $id_origemareaconhecimento);
        } else {
            $result = $this->negocio->findByEntidadeSessao();
        }

        $arrReturn = array();

        if ($result) {
            foreach ($result as $row) {

                if ($this->_getParam('backbone')) {
                    $arrReturn[] = $row->toBackBoneArray();
                } else {
                    $arrReturn[] = $this->negocio->toArrayEntity($row);
                }

            }
        }
        $this->getResponse()->setHttpResponseCode(200);
        $this->_helper->json($arrReturn);
    }

    public function getAction()
    {

    }

    public function postAction()
    {
        if ($this->getRequest()->getRawBody()) {
            $data = Zend_Json::decode($this->getRequest()->getRawBody());
        } else {
            $data = $this->getRequest()->getPost();
        }


        if ($this->form->isValid($data)) {
            if (isset($data['id_areaconhecimento']) && !empty($data['id_areaconhecimento'])) {
                $result = $this->negocio->updateAreaConhecimento($data);
                $id = $result->getId();
                $result = array(
                    'id' => $result->getId(),
                    'type' => 'success',
                    'title' => 'Registro Atualizado',
                    'text' => 'O registro foi atualizado com sucesso!'
                );
            } else {
                $result = $this->negocio->salvarAreaConhecimento($data);
                $id = $result['id'];
            }


            //verifica se salvou
            if ($id) {
                //verifica se tem imagem e envia a imagem
                if (isset($_FILES['attachment'])) {
                    $imagem = $this->negocio->uploadImagem($id, $_FILES['attachment']);
                    $result['st_imagemarea'] = $imagem;
                }

                $this->getResponse()->appendBody(json_encode($result));
                $this->getResponse()->setHttpResponseCode(201);
            } else {
                $this->getResponse()->appendBody(json_encode(array(
                    'title' => 'Atenção!',
                    'text' => "Houve um erro ao tentar salvar dados.",
                    'type' => 'error'
                )));
                $this->getResponse()->setHttpResponseCode(400);
            }


        } else {
            if ($this->form->getMessages()) {
                $arrErros = array();
                $i = 0;
                foreach ($this->form->getMessages() as $field => $erros) {
                    if ($erros) {
                        foreach ($erros as $mens) {
                            $arrErros[$i] = $mens;
                        }
                    }
                    $i++;
                }
            }
            $this->getResponse()->appendBody(json_encode(array(
                'title' => 'Atenção!',
                'text' => implode('<br>', $arrErros)
            )));
            $this->getResponse()->setHttpResponseCode(201);
        }
    }

    public function putAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
        $this->form->getElement('id_areaconhecimento');
        $data['st_areaconhecimento'] = trim(strip_tags($data['st_areaconhecimento']));
        if ($this->form->isValid($data)) {
            $result = $this->negocio->updateAreaConhecimento($data, $_FILES);
            $this->getResponse()->appendBody(json_encode(array(
                'id' => $result->getId(),
                'type' => 'success',
                'title' => 'Registro Atualizado',
                'text' => 'O registro foi atualizado com sucesso!'
            )));
            $this->getResponse()->setHttpResponseCode(201);
        } else {
            $arrErros = $this->view->messages($this->form)->toString();
            if ($arrErros) {
                $arrErros = array(
                    'type' => 'error',
                    'title' => 'Atenção!',
                    'text' => $this->view->messages($this->form)->toString()
                );
            }
            $this->getResponse()->appendBody(json_encode($arrErros));
            $this->getResponse()->setHttpResponseCode(201);
        }
    }

    public function deleteAction()
    {
        try {
            $id = intval($this->getParam('id'));

            if (!is_int($id)) {
                $this->getResponse()->setHttpResponseCode(400);
                $this->getResponse()->setBody('Não posso apagar um registro com ID inválido!');
            } else {
                $entity = $this->negocio->findAreaConhecimento($id);
                $result = $this->negocio->deletarArea($id);
                $this->_helper->json(array(
                    'id' => $result->getId_areaconhecimento(),
                    'type' => 'success',
                    'title' => 'Ação bem sucedida!',
                    'text' => 'Registro ' . $result->getSt_areaconhecimento() . ' removido.'
                ));

                $this->getResponse()->setHttpResponseCode(204)->appendBody('Registro ' . $entity->getSt_areaconhecimento() . ' removido');
            }
        } catch (Zend_Exception $e) {
            $this->getResponse()->setHttpResponseCode(400)->appendBody('Não foi possível apagar o registro' . $e);
        }
    }

    public function headAction()
    {

    }

}
<?php

/**
 * Controller para listar os modelocarteirinha por entidade
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @since 2014-12-19
 * @package application
 * @subpackage controller
 */
class Api_ModeloCarteirinhaPorEntidadeController extends Ead1_RestRestrita
{

    var $em;
    var $repositoryName;

    public function init ()
    {
        parent::init();
        $bootstrap = \Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $this->em = $bootstrap->getResource('doctrine')->getEntityManager();
        $this->repositoryName = 'G2\Entity\ModeloCarteirinhaEntidade';
    }

    public function indexAction ()
    {
        $arrMc = null;
        $sessao = \Ead1_Sessao::getObjetoSessaoGeral();
        $entities = $this->em->getRepository($this->repositoryName)->findBy(array('id_entidade'=> $sessao->id_entidade));
        if($entities){
            foreach($entities as $entity){
                if($entity instanceof \G2\Entity\ModeloCarteirinhaEntidade){
                    if($entity->getid_modelocarteirinha()->getId_situacao()->getId_situacao()==\G2\Entity\ModeloCarteirinha::ATIVA){
                        $arrMc[] = $entity->getid_modelocarteirinha()->toBackboneArray();
                    }
                }
            }
        }
        if($arrMc){
    		$this->getResponse()->setHttpResponseCode(200);
            $this->_helper->json($arrMc);
    	} else {
	    	$this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json(new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO));
    	}
    }

    public function getAction ()
    {
        
    }

    public function postAction ()
    {
    }

    public function putAction ()
    {
    }

    public function deleteAction ()
    {
    }

    public function headAction ()
    {
        
    }

}
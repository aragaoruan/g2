<?php

/**
 * Class Api_PrrController
 *
 * @author rafaelbruno
 */
class Api_MensagemPortalController extends Ead1_RestRestrita
{

    private $_negocio;

    public function init()
    {
        parent::init();
        $this->_negocio = new \G2\Negocio\MensagemPortal();
    }


    public function deleteAction()
    {

    }

    public function getAction()
    {
        $args = $this->getAllParams();

        if (isset($args['json']) && $args['json'] = true) {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender(TRUE);

            $arg = array(
                "id_usuario" => Ead1_Sessao::getSessaoUsuario()->id_usuario,
                "id_matricula" => Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula,
                "id_enviomensagem" => $args['id']
            );

            $result = $this->_negocio->findVwEnviarMensagemAluno($arg);
            $result[0]['dt_enviar'] = date('d-m-Y G:i', strtotime($result[0]['dt_enviar']));

            echo Zend_Json_Encoder::encode($result);
        }
    }

    public function headAction()
    {

    }

    public function indexAction()
    {
        $arg = $this->getAllParams();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);

        if (isset($arg['json']) && $arg['json'] = true) {
            $meiosPagamento = new \G2\Negocio\MensagemPortal;

            $params = array(
                "id_usuario" => Ead1_Sessao::getSessaoUsuario()->id_usuario,
                "id_matricula" => Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula,
            );

            if (isset($arg['filtro'])) {
                switch ($arg['filtro']) {
                    case 'importante':
                        $params['bl_importante'] = 1;
                        break;
                }
            }

            $result = $meiosPagamento->findVwEnviarMensagemAluno($params);

            if ($result != false) {
                $nao_lidas = array();

                foreach ($result as $k => $v) {
                    $result[$k]['dt_enviar'] = date('d-m-Y G:i', strtotime($v['dt_enviar']));

                    if ($v['id_evolucao'] == 42) {
                        $nao_lidas[] = $result[$k];
                    }
                }

                $arg_sessao = array(
                    'nr_mensagem' => sizeof($result),
                    'nr_mensagem_nao_lidas' => sizeof($nao_lidas),
                    'bl_mensagem' => sizeof($result) != 0 ? '1' : '0'
                );

                Ead1_Sessao::setSessionMensagem($arg_sessao);
            }
            $resultado = $result;
        } else {
            $resultado = false;
        }

        echo Zend_Json_Encoder::encode($resultado);
    }

    public function postAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);

        $json = $this->getRequest()->getRawBody();
        $data = Zend_Json_Decoder::decode($json);

        $entity = $this->_negocio->atualizarMensagem($data);

        echo Zend_Json_Encoder::encode($entity);
    }

    public function postMensagemAction()
    {

    }

    public function putAction()
    {

    }

}

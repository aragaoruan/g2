<?php
/**
 * @author Vinicius Avelino Alcantara <vinicius.alcantara@unyleya.com.br>
 * @since 2015-09-01
 */



class Api_DepartamentoController extends Ead1_RestRestrita {

    /**
     * @var \G2\Negocio\Negocio $negocio
     */
    private $negocio;
    private $repositoryName = '\G2\Entity\Departamento';

    public function init ()
    {
        parent::init();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->negocio = new \G2\Negocio\Negocio();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        $orderBy = $this->getParam('order_by') ? array($this->getParam('order_by') => $this->getParam('sort') ? $this->getParam('sort') : 'ASC') : null;
        unset($params['module'], $params['controller'], $params['action'], $params['order_by'], $params['sort'], $params['search'], $params['search']);
        $paramsLike = $this->getParam('search');

        $mensageiro = $this->negocio->findBySearch($this->repositoryName, $paramsLike, $params, $orderBy);
        foreach ($mensageiro as $key => &$entity) {
            $entity = $this->negocio->toArrayEntity($entity);
        }

        $this->_helper->json($mensageiro);
    }

    public function getAction()
    {
        $entity = $this->negocio->find($this->repositoryName, $this->getParam('id'));
        if ($entity)
            $this->_helper->json($this->negocio->toArrayEntity($entity));
    }

}
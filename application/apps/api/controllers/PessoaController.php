<?php

/**
 * Controller para Pessoas
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @package default
 * @subpackage controller
 * @since 2013-10-15
 */

class Api_PessoaController extends Ead1_RestRestrita
{

    /**
     * @var \G2\Negocio\Pessoa $_negocio
     */
    private $_negocio;

    /**
     * @var \PessoaBO $bo
     */
    private $bo;

    public function init()
    {
        parent::init();
        $this->_negocio = new \G2\Negocio\Pessoa();
        $this->bo = new \PessoaBO();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['module'], $params['controller'], $params['action']);

        $result = $this->_negocio->retornaPesquisarPessoa($params);
        if ($result) {
            foreach ($result as $key => $row) {
                $result[$key] = $this->_negocio->toArrayEntity($row);
            }
        }
        $this->getResponse()
            ->appendBody(json_encode($result));
    }

    public function deleteAction()
    {

        $id = intval($this->getParam('id'));
        if (!$id) {
            if ($this->getParam('cadastro-rapido'))
                $id = $this->getParam('cadastro-rapido');
        }
        if (!$id) {
            $this->getResponse()->setHttpResponseCode(400);
            $this->getResponse()->setBody('Não posso apagar um registro com ID inválido!');
        } else {
            $this->_negocio->exclusaoLogicaDadosUsuario($id);
            $this->getResponse()->setHttpResponseCode(204)->appendBody('Registro removido');
        }
    }


    /**
     * @param $string
     * @return array
     */
    private function setParameters($string)
    {
        $params = [];
        //verify if the param is an int or string
        if (is_numeric($string)) {
            $params["id_usuario"] = $string;
        }

        if (\G2\Utils\FormValidator::cpf($string)) {
            $params["st_cpf"] = $string;
        }

        // if the param is a string and contains one space, set st_nomecompleto to search
        if (strstr($string, ' ')) {
            $params["st_nomecompleto"] = $string;
        }

        // if the param is a string and contains @ or ., set st_email to search
        if (\G2\Utils\FormValidator::email($string)) {
            $params["st_email"] = $string;
        }

        if (!$params) {
            // if no one conditions before is true set st_nomecompleto, st_email and st_nomeresponsavellegal to search
            $params = array(
                'st_nomecompleto' => $string,
                'st_email' => $string,
                'st_nomeresponsavellegal' => $string,
            );
        }
        return $params;
    }

    /**
     * Metodo Get API, retorna vwPesquisarPessoa
     * @param GET 'str'
     * @throws Zend_Exception
     */
    public function getAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        //trata o parametro da string
        $string = $this->getParam('str', null);
        if ($string) {
            $params = $this->setParameters($string);

            if ($this->getParam('all')) {
                $params['allEntidades'] = true;
            } else if ($this->getParam('entidadesMesmaHolding')) {
                $params['entidadesMesmaHolding'] = true;
            }

            $id_evolucao = $this->getParam('id_evolucao');

            //busca seta os parametros para consultar
            /**
             * @var $return \G2\Entity\VwPesquisarPessoa
             * Limit foi setado 30, para não retornar muito registro e não sobrecarregar o banco.
             */
            $return = $this->_negocio->retornaPesquisarPessoa($params, null, 30);

            //trata o retorno
            // Valida se o retorno tem "rows" e adiciona no resultado.
            if (isset($return->rows)) {
                $return = $return->rows;
            }

            $result = [];
            if ($return) {
                $arrayMatriculasUsuario = [];

                foreach ($return as $key => $row) {

                    if (!array_key_exists($row->getId_usuario(), $arrayMatriculasUsuario)) {
                        $arrayMatriculasUsuario[$row->getId_usuario()] = [];
                    }

                    $where = array(
                        'bl_ativo' => true,
                        'id_usuario' => $row->getId_usuario(),
                        'id_entidadematricula' => $row->getId_entidade()
                    );

                    if ($id_evolucao) {
                        $where['id_evolucao'] = $id_evolucao;
                    }

                    if (!array_key_exists($row->getId_usuario(), $arrayMatriculasUsuario)) {
                        $arrayMatriculasUsuario[$row->getId_usuario()] = [];
                    }

                    $mat = $this->_negocio->findOneBy('\G2\Entity\Matricula', $where);

                    if ($mat) {
                        if ($row->getId_entidade() == $this->sessao->id_entidade) {
                            array_push($arrayMatriculasUsuario[$row->getId_usuario()],
                                '<b>' . $row->getSt_entidade() . '</b>');
                        } else {
                            array_push($arrayMatriculasUsuario[$row->getId_usuario()], $row->getSt_entidade());
                        }
                    }
                }
                foreach ($return as $key => $row) {

                    $id_usuario = $row->getId_usuario();

                    if (!array_key_exists($id_usuario, $result)) {
                        $result[$id_usuario] = $this->_negocio->toArrayEntity($row);
                        $result[$id_usuario]['id'] = $id_usuario;
                        $result[$id_usuario]['id_formatado'] = str_pad($id_usuario,
                            10, 0, STR_PAD_LEFT);

                        sort($arrayMatriculasUsuario[$id_usuario]);

                        $result[$id_usuario]['informacao_responsavel'] =
                            array_key_exists('st_nomeresponsavellegal',
                                $result[$id_usuario]) && $result[$id_usuario]['st_nomeresponsavellegal']
                                ? $result[$id_usuario]['st_nomeresponsavellegal']
                                . ' / (' . $result[$id_usuario]['nu_dddresponsavellegal'] . ')'
                                . $result[$id_usuario]['nu_telefoneresponsavellegal']
                                : null;

                        $entidades_matriculadas = implode(', ', $arrayMatriculasUsuario[$id_usuario]);

                        $result[$id_usuario]['entidades_matriculadas'] = !empty($entidades_matriculadas)
                            ? implode(', ', $arrayMatriculasUsuario[$id_usuario])
                            : '';

                        // Remover os resultados que não batem, caso possua o filtro de EVOLUÇÃO DA MATRÍCULA
                        if (empty($entidades_matriculadas) && $id_evolucao) {
                            unset($result[$id_usuario]);
                        }

                        if ((!empty($params['entidadesMesmaHolding']) && $params['entidadesMesmaHolding'])
                            && $result[$id_usuario]['entidades_matriculadas'] == '') {
                            unset($result[$id_usuario]);
                        }
                    }
                }

                $result = array_values($result);
            }
            $this->getResponse()
                ->appendBody(json_encode($result));


        } elseif ($this->getParam('id_usuario') || $this->getParam('id')) {
            if ($this->getParam('id_usuario')) {
                $params['id_usuario'] = $this->getParam('id_usuario');
            } elseif ($this->getParam('id')) {
                $params['id_usuario'] = $this->getParam('id');
            }

            if ($this->getParam('all')) {
                $params['allEntidades'] = true;
            }

            $return = $this->_negocio->retornaPesquisarPessoa($params);

            $result = [];

            if ($return) {


                $arrayMatriculasUsuario = [];

                /* Adiciona no array $arrayMatriculasUsuario a lista de disciplinas matriculadas
                de cada usuário. */

                foreach ($return as $key => $row) {
                    if (!array_key_exists($row->getId_usuario(), $arrayMatriculasUsuario))
                        $arrayMatriculasUsuario[$row->getId_usuario()] = [];

                    if ($row->getId_entidade() == $this->sessao->id_entidade) {
                        array_push($arrayMatriculasUsuario[$row->getId_usuario()], '<b>' . $row->getSt_entidade() . '</b>');
                    } else {
                        array_push($arrayMatriculasUsuario[$row->getId_usuario()], $row->getSt_entidade());
                    }

                }

                foreach ($return as $key => $row) {

                    $id_usuario = $row->getId_usuario();

                    if (!array_key_exists($id_usuario, $result)) {
                        $result[$id_usuario] = $this->_negocio->toArrayEntity($row);
                        $result[$id_usuario]['id'] = $id_usuario;
                        $result[$id_usuario]['id_formatado'] = str_pad($id_usuario,
                            10, 0, STR_PAD_LEFT);
                        $result[$id_usuario]['informacao_responsavel'] =
                            array_key_exists('st_nomeresponsavellegal',
                                $result[$id_usuario])
                            && $result[$id_usuario]['st_nomeresponsavellegal'] ?
                                $result[$id_usuario]['st_nomeresponsavellegal']
                                . ' / (' . $result[$id_usuario]['nu_dddresponsavellegal'] . ')'
                                . $result[$id_usuario]['nu_telefoneresponsavellegal'] : null;
                        $entidades_matriculadas = implode(', ', $arrayMatriculasUsuario[$id_usuario]);
                        $result[$id_usuario]['entidades_matriculadas'] =
                            !empty($entidades_matriculadas) ?
                                implode(', ', $arrayMatriculasUsuario[$id_usuario]) : '';

                        if ((!empty($params['entidadesMesmaHolding']) && $params['entidadesMesmaHolding'])
                            && $result[$id_usuario]['entidades_matriculadas'] == '') {
                            unset($result[$id_usuario]);
                        }

                    }
                }
                $result = array_values($result);
            }

            $this->getResponse()
                ->appendBody(json_encode($result));
        }

    }

    public function headAction()
    {

    }

    public function postAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
        $result = $this->_negocio->salvarPessoa($data);
        $arrReturn = array();
        if ($result) {
            foreach ($result as $row) {
                $arrReturn = array(
                    "id_usuario" => $row->getId_usuario(),
                    "id" => $row->getId_usuario(),
                    "id_entidade" => $row->getId_entidade(),
                    "st_sexo" => $row->getSt_sexo(),
                    "st_nomeexibicao" => $row->getSt_nomeexibicao(),
                    "dt_nascimento" => $row->getDt_nascimento(),
                    "st_nomepai" => $row->getSt_nomepai(),
                    "st_nomemae" => $row->getSt_nomemae(),
                    "id_pais" => $row->getId_pais(),
                    "sg_uf" => $row->getSg_uf(),
                    "id_municipio" => $row->getId_municipio(),
                    "dt_cadastro" => $row->getDt_cadastro(),
                    "id_usuariocadastro" => $row->getid_usuariocadastro(),
                    "st_passaporte" => $row->getSt_passaporte(),
                    "id_situacao" => $row->getId_situacao(),
                    "bl_ativo" => $row->getBl_ativo(),
                    "id_estadocivil" => $row->getId_estadocivil(),
                    "st_urlavatar" => $row->getSt_urlavatar(),
                    "st_cidade" => $row->getSt_cidade()
                );
            }
        }
        $this->getResponse()
            ->appendBody(json_encode($arrReturn));
    }

    public function putAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);

        if (is_array($data)) {
            foreach ($data as $key => $value) {
                /*
                 * Conserto paliativo passando a identificação na mão.
                 * Caso queiram salvar os dados da identificação
                 * */
                if (!$value && $key != 'st_identificacao')
                    unset($data[$key]);
            }
        }

        $result = $this->_negocio->editarPessoa($data);

        if ($result) {
            $this->getResponse()->appendBody(json_encode($result));
            $this->getResponse()->setHttpResponseCode(201);
        } else {
            $this->getResponse()->appendBody(json_encode("Erro ao editar Pessoa"));
            $this->getResponse()->setHttpResponseCode(400);
        }
    }
}

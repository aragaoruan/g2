<?php

/**
 * Controller para VwAvaliacaoAgendamento
 * @author Debora Castro <debora.castro@unyleya.com.br>
 * @since 16/10/2014
 * @package application
 * @subpackage controller
 */
class Api_VwAvaliacaoAgendamentoController extends Ead1_RestRestrita
{

    private $doctrineContainer;
    private $em;

    public function init()
    {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
    }

    public function indexAction()
    {
        $negocio = new \G2\Negocio\GerenciaProva();

        $params = $this->getAllParams();
        unset($params['module'], $params['controller'], $params['action']);

        $ar_return = array();

        $results = $negocio->findByVwAvaliacaoAgendamento($params);

        if (is_array($results)) {
            foreach ($results as $key => $result) {
                $ar_return[$key] = $negocio->toArrayEntity($result);
                $ar_return[$key]['dt_aplicacao'] = $result->getDt_aplicacao() ? $negocio->converterData($result->getDt_aplicacao(), 'd/m/Y') : 'Sem data definida';
                $ar_return[$key]['dt_agendamento'] = $result->getDt_agendamento() ? $negocio->converterData($result->getDt_agendamento(), 'd/m/Y') : '';
            }
        }

        $this->_helper->json($ar_return);
    }

    public function getAction()
    {
        $negocio = new \G2\Negocio\GerenciaProva();
        $array_find = array();
        if ($this->_getParam('id_matricula', null) != null) {
            $array_find['id_matricula'] = $this->_getParam('id_matricula');
        }
        if ($this->_getParam('id_avaliacaoagendamento', null) != null) {
            $array_find['id_tipoavaliacao'] = $this->_getParam('id_tipoavaliacao');
        }
        $return = $negocio->findByVwAvaliacaoAgendamento($array_find);
        $serialize = new \Ead1\Doctrine\EntitySerializer($this->em);
        if (!empty($return)):
            foreach ($return as $key => $obj) {
                $retorno[$key] = $serialize->toArray($obj);
            }
            $retorno = $retorno[0];
        else:
            $retorno = $return;
        endif;

        $this->_helper->json($retorno);
    }

    public function postAction()
    {

    }

    public function putAction()
    {

    }

    public function deleteAction()
    {

    }

    public function headAction()
    {

    }

}

<?php

/**
 * Controller API para Conclusao
 * @author Débora Castro <debora.castro@unyleya.com.br>
 * @since  04-11-2014
 * @package application
 * @subpackage controller
 */
class Api_ConclusaoController extends Ead1_RestRestrita {

    private $negocio;

    public function init() {
        $this->_helper->viewRenderer->setNoRender(true);
        parent::init();
        $this->negocio = new \G2\Negocio\Conclusao();
    }

    public function indexAction() {
        
    }

    public function getAction() {
        
    }

    public function postAction() {

    }

    public function putAction() {
        
    }

    public function deleteAction() {
 
    }

    public function headAction() {
        
    }

}

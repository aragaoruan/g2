<?php

/**
 * Controller para gerenciar as UsuarioPerfilEntidadeReferencia (Titular de Certificação)
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2015-01-08
 * @package application
 * @subpackage controller
 */

class Api_TitularCertificacaoController extends Ead1_RestRestrita
{

    /**
     * @var \G2\Negocio\UsuarioPerfilEntidadeReferencia
     */
    private $negocio;

    public function init ()
    {
        parent::init();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->negocio = new \G2\Negocio\UsuarioPerfilEntidadeReferencia();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['module'], $params['controller'], $params['action']);

        if (!isset($params['id_entidade']))
            $params['id_entidade'] = $this->negocio->sessao->id_entidade;

        $mensageiro = $this->negocio->findAllBy($params);
        $this->_helper->json($mensageiro);
    }

    public function getAction()
    {
        $id = $this->getParam('id');

        if ($id) {
            $mensageiro = $this->negocio->find($this->negocio->repositoryName, $id);
            $mensageiro = $this->negocio->toArrayEntity($mensageiro);

            $this->_helper->json($mensageiro);
        }
    }

    public function postAction()
    {
        $data = Zend_Json::decode($this->getRequest()->getRawBody());

        // Verificar se existem cadastros com o mesmo id_disciplina, id_areaconhecimento e dt_inicio
        $where = array(
            'id_perfilreferencia' => isset($data['id_perfilreferencia']) ? $data['id_perfilreferencia'] : null,
            'id_disciplina' => isset($data['id_disciplina']['id_disciplina']) ? $data['id_disciplina']['id_disciplina'] : $data['id_disciplina'],
            'id_areaconhecimento' => isset($data['id_areaconhecimento']['id_areaconhecimento']) ? $data['id_areaconhecimento']['id_areaconhecimento'] : $data['id_areaconhecimento'],
            'id_projetopedagogico' => isset($data['id_projetopedagogico']['id_projetopedagogico']) ? $data['id_projetopedagogico']['id_projetopedagogico'] : $data['id_projetopedagogico'],
            'dt_inicio' => $data['dt_inicio'] ? $data['dt_inicio'] : date('d/m/Y H:i:s'),
            'dt_fim' => $data['dt_fim'],
            'id_entidade' => $this->negocio->sessao->id_entidade,
            'bl_titular' => 1,
            'bl_ativo' => 1
        );
        $exists = $this->negocio->findByDisciplinaAreaProjetoPeriodoEntidade($where);
        $exists = array_shift($exists);

        if ($exists) {
            // Se existir, não permite salvar
            $mensageiro = new \Ead1_Mensageiro($this->negocio->toArrayEntity($exists), \Ead1_IMensageiro::ERRO);
            $mensageiro->setText('Esta disciplina já possui um titular (' . $exists->getId_usuario()->getSt_nomecompleto() . ') cadastrado na Área e Projeto selecionados.');
            $this->getResponse()->setHttpResponseCode(400);
        } else {
            //Zend_Debug::dump($data['id_titulacao']);die;
            if(!$data['id_titulacao']){
                $mensageiro = new \Ead1_Mensageiro('', \Ead1_IMensageiro::ERRO);
                $mensageiro->setText('Escolha uma titulação para salvar');
                $this->getResponse()->setHttpResponseCode(400);
            }else{
                $mensageiro = $this->negocio->save($data);
                $this->getResponse()->setHttpResponseCode(200);
            }

        }

        $this->_helper->json($mensageiro);
    }

    public function putAction()
    {
        $this->postAction();
    }

    public function deleteAction()
    {
        $valores = json_decode($this->getRequest()->getRawBody());
        // O DELETE APENAS ALTERA A SITUACAO PARA INATIVO
        $data = array(
            'id_perfilreferencia' => $this->getParam('id'),
            'bl_ativo' => 0,
            'dt_inicio' => $valores->dt_inicio
            //'dt_fim' => date('d/m/Y')
        );

        if ($data['id_perfilreferencia']) {
            $mensageiro = $this->negocio->save($data);
            $this->_helper->json($mensageiro);
        }
    }

}
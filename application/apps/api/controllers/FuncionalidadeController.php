<?php

/**
 * Controller para gerenciar as Funcionalidades
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2014-11-20
 * @package application
 * @subpackage controller
 */
class Api_FuncionalidadeController extends Ead1_RestRestrita
{

    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Negocio();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['action'], $params['module'], $params['controller']);

    }

    public function getAction()
    {
        try {
            $idFuncionalidade = $this->getParam("id");
            if (!$idFuncionalidade) {
                throw new Exception("Id não informado");
            }

            $entity = new \G2\Entity\Funcionalidade();
            $entity->find($idFuncionalidade);
            $this->getResponse()->appendBody(json_encode($entity->toArray()));

        } catch (Exception $e) {
            $this->getResponse()->setHttpResponseCode(500)->setBody($e->getMessage());
        }
    }

    public function postAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);

        $bo = new EntidadeBO();
        $mensageiro = array();

        if (isset($data['ativacao']) && $data['ativacao']) {
            $ativacaoTo = array();

            foreach ($data['ativacao'] as $item) {
                $to = new EntidadeFuncionalidadeTO();
                $to->setId_funcionalidade($item['id_funcionalidade']);
                $to->setId_entidade($item['id_entidade']);
                $to->setId_situacao($item['id_situacao']);
                $to->setBl_ativo($item['bl_ativo']);
                $to->setBl_visivel(0);

                $ativacaoTo[] = $to;
            }

            $mensageiro = $bo->salvarArrEntidadeFuncionalidadeSuperUsuario($ativacaoTo);
        }

        if (isset($data['visualizacao']) && $data['visualizacao']) {
            $visualizacaoTo = array();

            foreach ($data['visualizacao'] as $item) {
                $to = new EntidadeFuncionalidadeTO();
                $to->setId_funcionalidade($item['id_funcionalidade']);
                $to->setId_entidade($item['id_entidade']);
                $to->setId_situacao($item['id_situacao']);
                $to->setBl_ativo($item['bl_ativo']);
                $to->setBl_visivel(1);

                $visualizacaoTo[] = $to;
            }

            $mensageiro = $bo->salvarArrEntidadeFuncionalidadeUsuario($visualizacaoTo);
        }

        $this->_helper->json($mensageiro);

    }

    public function putAction()
    {
        $this->postAction();
    }

    public function deleteAction()
    {
    }

    public function headAction()
    {
    }

}

<?php

/**
 * Controller para criar agendamento de provas finais e de avaliação
 * @author Débora Castro <debora.castro@unyleya.com.br>
 * @since  19-12-2013
 * @package application
 * @subpackage controller
 */

class Api_GerenciaProvaController extends Ead1_RestRestrita {

    private $negocio;

    public function init() {
        parent::init();
        $this->negocio = new \G2\Negocio\GerenciaProva();
    }

    public function indexAction() {

    }

    public function getAction() {
        
    }

    public function postAction() {

        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json_Decoder::decode($body);

        $this->getResponse()->appendBody(($this->negocio->salvaAvaliacaoAgendamento($data)->toJson()));
        $this->getResponse()->setHttpResponseCode(201);
    }

    public function putAction() {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json_Decoder::decode($body);
        $this->getResponse()->appendBody(($this->negocio->salvaAvaliacaoAgendamento($data)->toJson()));
        $this->getResponse()->setHttpResponseCode(201);
    }

    public function deleteAction() {
 
    }

    public function headAction() {
        
    }

}

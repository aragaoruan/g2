<?php

/**
 * Class Api_VwProdutoVendaController
 * Classe api para vw
 * @Author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Api_VwProdutoVendaController extends Ead1_RestRestrita
{
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Venda();
    }

    public function indexAction()
    {
        $result = $this->negocio->retornarVwProdutoVenda();
        $arrReturn = array();
        if ($result) {
            foreach ($result as $row) {
                $arrReturn[] = $this->negocio->toArrayEntity($row);
            }
        }

    $this->_helper->json($arrReturn);
    }

    public function getAction()
    {
        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);
        $result = $this->negocio->retornarVwProdutoVenda($params);
        $arrReturn = array();
        if ($result) {
            foreach ($result as $row) {
                $arrReturn[] = $this->negocio->toArrayEntity($row);
            }
        }

        $this->_helper->json($arrReturn);

    }

    public function headAction()
    {
    }

    public function postAction()
    {
    }

    public function putAction()
    {
    }

    public function deleteAction()
    {
        $id = intval($this->getParam('id'));
        if (!is_int($id)) {
            $this->getResponse()->setHttpResponseCode(400)->setBody('Não posso apagar um registro com ID inválido!');
        } else {
            if ($this->negocio->deleteVendaProduto($id)) {
                $this->getResponse()->setHttpResponseCode(204)->appendBody('Registro removido');
            } else {
                $this->getResponse()->setHttpResponseCode(400)->appendBody('Erro ao tentar remover registro');
            }
        }
    }
}
<?php

/**
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @date 2017-02-15
 */
class Api_VendedorController extends Ead1_RestRestrita
{
    /**
     * @var \G2\Negocio\Negocio $negocio
     */
    private $negocio;
    private $repositoryName = '\G2\Entity\VwVendedorEntidade';

    public function init()
    {
        parent::init();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->negocio = new \G2\Negocio\Negocio();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['module'], $params['controller'], $params['action']);

        $results = $this->negocio->findBy($this->repositoryName, $params, array('st_vendedor' => 'ASC'));
        $this->_helper->json($this->negocio->toArrayEntity($results));
    }

    public function getAction()
    {
        $entity = $this->negocio->find($this->repositoryName, $this->getParam('id'));
        $result = $entity ? $this->negocio->toArrayEntity($entity) : new stdClass();
        $this->_helper->json($result);
    }
}
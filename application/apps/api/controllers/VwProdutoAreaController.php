<?php

/**
 * Controller para gerenciar as áreas
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @since 2014-03-19
 * @package application
 * @subpackage controller
 */
class Api_VwProdutoAreaController extends Ead1_RestRestrita
{


    public function indexAction()
    {

        try {

            $array = $this->_getAllParams();
            unset($array['module'], $array['controller'], $array['action']);

            $bo = new G2\Negocio\Produto();
            $mensageiro = $bo->retornarVwAreasProduto($array['id_produto']);

        } catch (Exception $e) {
            $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json($mensageiro);
        } else {
            $this->getResponse()->setHttpResponseCode(201);
            $array = null;
            foreach ($mensageiro->getMensagem() as $entity) {
//         		if($entity instanceof \G2\Entity\AreaConhecimento){
//         			$arraye  = null;
//         			$arraye  = $entity->toBackboneArray();
//         			$entity = new \G2\Entity\VwProdutoArea();
//         			$entity->montarEntity($arraye);
//         		}
// //         		\Zend_Debug::dump($entity->toBackboneArray(),__CLASS__.'('.__LINE__.')');

                $array[] = $entity->toBackboneArray();
            }
            $this->_helper->json($array);
        }

    }

    public function getAction()
    {
        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);

        $bo = new G2\Negocio\Produto();
        $result = $bo->retornarVwProdutoArea($params);
        $arrReturn = array();
        if ($result) {
            foreach ($result as $row) {
                if($row->getId_situacao() == 71){
                    continue;
                }else{
                    $arrReturn[] = $bo->toArrayEntity($row);
                }
            }
        }
        $this->getResponse()
            ->appendBody(json_encode($arrReturn));
    }

    public function postAction()
    {

    }

    public function putAction()
    {


    }

    public function deleteAction()
    {

    }

    public function headAction()
    {

    }
}
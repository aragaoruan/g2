<?php

/**
 * Controller para gerenciar local de aula
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2013-09-23
 * @package application
 * @subpackage controller
 */
use G2\Negocio\MensagemPadrao;

class Api_SmsEntidadeMensagemController extends Ead1_RestRestrita
{

    private $negocio;

    public function init ()
    {
        parent::init();
        $this->negocio = new MensagemPadrao();
    }

    public function deleteAction ()
    {

    }

    public function getAction ()
    {

    }

    public function headAction ()
    {

    }

    public function indexAction ()
    {
        $result = $this->negocio->getSmsEntidadeMensagem();

        $this->getResponse()
                ->appendBody(json_encode($result));
    }

    public function postAction ()
    {

    }

    public function putAction ()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
        $result = $this->negocio->updateSmsEntidadeMensagem($data);
        $array = array();
        if($result){
            $array['dt_cadastro'] = $result->getDt_cadastro();
            $array['id_mensagempadrao'] = $result->getId_mensagempadrao()->getId_mensagempadrao();
            $array['id_entidade'] = $result->getId_entidade()->getId_entidade();
            $array['bl_ativo'] = $result->getBl_ativo();
            $array['id_usuariocadastro'] = $result->getId_usuariocadastro()->getId_usuario();
            $array['id_sistema'] = $result->getId_sistema()->getId_sistema();
        }

        $this->getResponse()->appendBody(json_encode(array(
            'id' => $result->getId_smsentidademensagem(),
            'type' => 'success',
            'title' => 'Registro Atualizado',
            'text' => 'O registro foi atualizado com sucesso!'
        )));
        $this->getResponse()->setHttpResponseCode(201);
    }

}

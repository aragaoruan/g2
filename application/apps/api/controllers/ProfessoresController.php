<?php

/**
 * Controller Professores sala de aula lote
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @since 2015-05-13
 * @package application
 * @subpackage controller
 */

class Api_ProfessoresController extends Ead1_RestRestrita
{

    private $negocio;

    public function init ()
    {
        parent::init();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->negocio = new \G2\Negocio\UsuarioPerfilEntidadeReferencia();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['module'], $params['controller'], $params['action']);

        if (!isset($params['id_entidade']))
            $params['id_entidade'] = $this->negocio->sessao->id_entidade;

        $mensageiro = $this->negocio->findAllBy($params);
        $this->_helper->json($mensageiro);
    }

    public function getAction()
    {
    }

    public function postAction()
    {
    }

    public function putAction()
    {
    }

    public function deleteAction()
    {
    }

}
<?php

/**
 * Controller para gerenciar os livros
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @since 2013-11-28
 * @package application
 * @subpackage controller
 */
class Api_LivroController extends Ead1_RestRestrita
{

    private $doctrineContainer;
    private $em;
    private $negocio;

    public function init ()
    {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
        $this->negocio = new G2\Negocio\Livro();
    }

    public function indexAction ()
    {

//        $produto = new \G2\Negocio\Livro();
//        $mensageiro = $produto->findAllRegra();
//        
//        if($mensageiro->getTipo()==Ead1_IMensageiro::SUCESSO){
//            $mensageiro->setText(count($mensageiro->getMensagem()).' registros encontrados!');
//        }
//        
//        $this->getResponse()
//                ->appendBody($mensageiro->toJson());
//        
    }

    public function getAction ()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $produto = new \G2\Negocio\Livro();
        $mensageiro = $produto->findLivroById($this->getParam('id', null));

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()
                    ->appendBody(json_encode($mensageiro->getFirstMensagem()));
        }
    }

    public function postAction ()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
        $form = new LivroForm();
        if ($form->isValid($data)) {
            $result = $this->negocio->salvaLivro($data);
            $arrReturn = array();
            if ($result->getType() == 'success') {
                foreach ($result->getMensagem() as $livro) {
                    $arrReturn = $livro->toArray();
                    $arrReturn['id'] = $livro->getId_livro();
                }
                $this->getResponse()->setHttpResponseCode(201)->appendBody(json_encode($arrReturn));
            } else {
                $this->getResponse()->setHttpResponseCode(400)->setBody($result->getText());
            }
        } else {
            $strErros = $this->view->messages($form)->toString();
            $this->getResponse()->setHttpResponseCode(400)->setBody($strErros);
        }
    }

    public function putAction ()
    {
//         try {
//             $body       = $this->getRequest()->getRawBody();
//             $data       = Zend_Json::decode($body);
//             $produto   = new \G2\Negocio\Livro();
//             $this->getResponse()
//                     ->appendBody($produto->salvarLivro($data)->toJson());
//         } catch (Exception $exc) {
//             $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
//             $this->getResponse()
//                     ->appendBody($mensageiro->toJson());
//         }
    }

    public function deleteAction ()
    {
        $id = intval($this->getParam('id'));
        if (!is_int($id)) {
            $this->getResponse()->setHttpResponseCode(400)->setBody('Não posso apagar um registro com ID inválido!');
        } else {
            $result = $this->negocio->deletaLivro($id);
            if ($result->getType() == 'success') {
                $this->getResponse()->setHttpResponseCode(204)->appendBody('Registro removido');
            } else {
                $this->getResponse()->setHttpResponseCode(400)->appendBody('Erro ao tentar remover registro');
            }
        }
    }

    public function headAction ()
    {
        
    }

}
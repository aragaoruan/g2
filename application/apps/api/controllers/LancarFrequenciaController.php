<?php

/**
 * Controller API para lançar frequência de alunos que realizaram avaliações presenciais. (provas finais e de recuperação)
 * @author Débora Castro <debora.castro@unyleya.com.br>
 * @since  22-01-2014
 * @package application
 * @subpackage controller
 */
class Api_LancarFrequenciaController extends Ead1_RestRestrita
{

    /**
     * @var G2\Negocio\GerenciaProva
     */
    private $negocio;

    public function init ()
    {
        parent::init();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->negocio = new \G2\Negocio\GerenciaProva();
    }

    public function indexAction()
    {
    }

    public function postAction()
    {
        $data = Zend_Json_Decoder::decode($this->getRequest()->getRawBody());

        $retorno = $this->negocio->salvarFrequencia($data);

        $this->_helper->json($retorno);
    }

}

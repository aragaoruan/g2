<?php

/**
 * Controller para gerenciar as CampoRelatorio
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2014-11-13
 * @package application
 * @subpackage controller
 */

class Api_CampoRelatorioController extends Ead1_RestRestrita
{

    private $negocio;
    private $repositoryName = 'G2\Entity\CampoRelatorio';

    public function init ()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Negocio();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['module'], $params['controller'], $params['action']);

        $mensageiro = $this->negocio->findBy($this->repositoryName, $params, array('nu_ordem' => 'ASC'));
        $results = array();
        foreach ($mensageiro as $entity) {
            $build = $this->negocio->toArrayEntity($entity);
            $build['propriedade_campo_relatorio'] = array(
                1 => null,
                2 => null,
                3 => null,
                4 => null,
                5 => null,
                6 => null,
                7 => null,
                8 => null
            );

            // Buscas PropriedadeCampoRelatorio
            $props = $this->negocio->findBy('G2\Entity\PropriedadeCampoRelatorio', array(
                'id_camporelatorio' => $build['id_camporelatorio']
            ));
            foreach ($props as $prop) {
                $build['propriedade_campo_relatorio'][$prop->getId_tipopropriedadecamporel()] = $prop->getSt_valor();
            }

            $results[] = $build;
        }

        $this->getResponse()->setHttpResponseCode(200);
        $this->getResponse()->appendBody(json_encode($results));
    }

    public function getAction()
    {
        $id = $this->getParam('id');

        if ($id) {
            $mensageiro = $this->negocio->find($this->repositoryName, $id);
            $mensageiro = $this->negocio->toArrayEntity($mensageiro);

            $this->getResponse()->appendBody(json_encode($mensageiro));
        }
    }

    public function postAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);

        $mensageiro = $this->negocio->save($data);

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->appendBody(json_encode($mensageiro->toArray()));
            $this->getResponse()->setHttpResponseCode(200);
        } else {
            $this->getResponse()->appendBody(json_encode(array('erro' => $mensageiro->getMensagem())));
            $this->getResponse()->setHttpResponseCode(500);
        }
    }

    public function putAction()
    {
        $this->postAction();
    }

    public function deleteAction()
    {
        $id = $this->getParam('id');

        if ($id) {
            $entity = $this->negocio->find($this->repositoryName, $id);

            try {
                $this->negocio->delete($entity);
                $this->getResponse()->setHttpResponseCode(204);
            } catch (Zend_Exception $e) {
                $this->getResponse()->setHttpResponseCode(500);
            }

        }
    }

    public function headAction()
    {
    }

}
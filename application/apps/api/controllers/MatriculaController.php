<?php

/**
 * Controller para Matricula
 * @author Rafael Rocha <rafael.rocha.mg@gmail.com>
 * @since 2014-01-28
 * @package application
 * @subpackage controller
 */
class Api_MatriculaController extends Ead1_RestRestrita {

    private $doctrineContainer;
    private $em;

    public function init() {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
    }

    public function indexAction() {

        $produto = new \G2\Negocio\Produto();
        $mensageiro = $produto->findAllProduto();

        if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json($mensageiro);
        } else {
            $this->getResponse()->setHttpResponseCode(201);
            $this->_helper->json($mensageiro->getMensagem());
        }
    }

    public function getAction() {
        $to = new \MatriculaTO();
        $to->setId_matricula($this->_getParam('id'));
        $to->fetch(true, true, true);
        $this->_helper->json($to->toBackboneArray());
    }

    public function postAction() {
        try {
            $body = $this->getRequest()->getRawBody();
            $data = Zend_Json::decode($body);
            $matricula = new \G2\Negocio\Matricula();

            $mensageiro = $matricula->salvarMatricula($data);
        } catch (Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json($mensageiro);
        } else {
            $this->getResponse()->setHttpResponseCode(201);
            $to = new \MatriculaTO();
            $to->setId_matricula($mensageiro->getFirstMensagem()->getId());
            $to->fetch(true, true, true);
            $this->_helper->json($to->toBackboneArray());
        }
    }

    public function putAction() {
        try {

            $body = $this->getRequest()->getRawBody();
            $data = Zend_Json::decode($body);
            $produto = new \G2\Negocio\Produto();

            $mensageiro = $produto->salvarProduto($data);
        } catch (Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json($mensageiro);
        } else {
            $this->getResponse()->setHttpResponseCode(201);
            $to = new \ProdutoTO();
            $to->setId_produto($mensageiro->getId());
            $to->fetch(true, true, true);
            $this->_helper->json($to->toBackboneArray());
        }
    }

    public function deleteAction() {
        
    }

    public function headAction() {
        
    }

}

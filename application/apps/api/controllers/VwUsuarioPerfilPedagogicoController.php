<?php

/**
 * Controller para gerenciar as VwUsuarioPerfilPedagogico
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2015-01-05
 * @package application
 * @subpackage controller
 */

class Api_VwUsuarioPerfilPedagogicoController extends Ead1_RestRestrita
{

    private $negocio;
    private $repositoryName = '\G2\Entity\VwUsuarioPerfilPedagogico';

    public function init ()
    {
        parent::init();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->negocio = new \G2\Negocio\VwUsuarioPerfilPedagogico();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['module'], $params['controller'], $params['action']);

        $mensageiro = $this->negocio->findBy($this->repositoryName, $params);
        $results = array();
        foreach ($mensageiro as $entity) {
            $results[] = $this->negocio->toArrayEntity($entity);
        }

        $this->_helper->json($results);
    }

    public function getAction()
    {
        $id = $this->getParam('id');

        if ($id) {
            $mensageiro = $this->negocio->find($this->repositoryName, $id);
            $mensageiro = $this->negocio->toArrayEntity($mensageiro);

            $this->_helper->json($mensageiro);
        }
    }

    public function postAction()
    {
        $data = Zend_Json::decode($this->getRequest()->getRawBody());

        $mensageiro = $this->negocio->save($data);
        $this->_helper->json($mensageiro);
    }

    public function putAction()
    {
        $this->postAction();
    }

    public function deleteAction()
    {
        $id = $this->getParam('id');

        if ($id) {
            $entity = $this->negocio->find($this->repositoryName, $id);

            $mensageiro = $this->negocio->delete($entity);
            $this->_helper->json($mensageiro);
        }
    }

    public function headAction()
    {
    }

}
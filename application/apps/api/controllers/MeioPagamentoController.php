<?php

/**
 * Controller para gerenciar as MeioPagamento
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2014-10-30
 * @package application
 * @subpackage controller
 */
class Api_MeioPagamentoController extends Ead1_RestRestrita
{
    
    private $negocio;
    
    public function init ()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\MeioPagamento();
    }
    
     public function indexAction()
    {
        $params = $this->getRequest()->getParams();
        $order = array();

        //Verficia e aplica parametro para ordenar resultado da busca
        if ($this->getRequest()->getParam('order')){
            $order = array(
                $this->getRequest()->getParam('order') => $params['type']
            );
        }

        unset($params['module'], $params['action'], $params['controller'], $params['order'], $params['type']);

        $results = $this->negocio->retornaMeioPagamento($params, $order);
        $arr = array();

        if ($results) {
            foreach ($results as $key => $result) {
                $arr[] = $this->negocio->toArrayEntity($result);
            }
        }
        
        $this->getResponse()->appendBody(json_encode($arr));
    }

    public function getAction()
    {
    }
    
    public function postAction()
    {
    }
    
    public function putAction()
    {
    }
    
    public function deleteAction(){
    }

    public function headAction(){
    }
    

}
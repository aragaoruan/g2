<?php

/**
 * Controller para gerenciar os ProjetoPedagogicoSerieNivelEnsino
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @since 2013-12-18
 * @package application
 * @subpackage controller
 */
class Api_ProjetoPedagogicoSerieNivelEnsinoController extends Ead1_RestRestrita
{
    
    private $doctrineContainer;
    private $em;
    
    public function getAction()
    {
        
    	$to = new \ProjetoPedagogicoSerieNivelEnsinoTO();
    	$to->setId_projetopedagogico($this->_getParam('id'));
    	$arrTO = $to->fetch(false, false, false);
    	$arrRetorno = array();
    	$id_serie = array();
    	foreach($arrTO as $to){
    		$id_serie[] = $to->getId_serie();
    		$arrRetorno = array('id_projetopedagogico'=>$to->getId_projetopedagogico(), 'id_nivelensino'=>$to->getId_nivelensino(), 'id_serie'=>$id_serie );
    	}
    	$this->_helper->json($arrRetorno);
        
    }
    
    public function postAction()
    {
    }
    
    public function putAction()
    {
    	try {
    	
    		$body       = $this->getRequest()->getRawBody();
    		$data       = Zend_Json::decode($body);
    		
    		if(!$data['id_serie']){
    			throw new Zend_Exception("Informe a(s) Série(s)!");
    		}
    		
    		$arrTO = array();
    		foreach($data['id_serie'] as $id_serie){
    			
    			$to = new \ProjetoPedagogicoSerieNivelEnsinoTO();
    			$to->setId_nivelensino($data['id_nivelensino']);
    			$to->setId_projetopedagogico($data['id_projetopedagogico']);
    			$to->setId_serie($id_serie);
    			$arrTO[] = $to;
    			
    		}
    		
    		$ro = new ProjetoPedagogicoRO();
    		$mensageiro = $ro->salvarArrayProjetoPedagogicoSerieNivelEnsino($arrTO);
    		if($mensageiro->getTipo()==\Ead1_IMensageiro::SUCESSO){
    			$mensageiro->setId($data['id_projetopedagogico']);
    		}
    	
    	} catch (Exception $exc) {
    		$mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
    	}
    	
        if($mensageiro->getTipo()!=Ead1_IMensageiro::SUCESSO){
    		$this->getResponse()->setHttpResponseCode(400);
    		$this->_helper->json($mensageiro);
    	} else {
    		$this->getResponse()->setHttpResponseCode(201);
    		
    		
    		$rto = new \ProjetoPedagogicoSerieNivelEnsinoTO();
    		$rto->setId_projetopedagogico($data['id_projetopedagogico']);
    		$arrTO = $rto->fetch(false, false, false);
    		
    		$id_serie = array();
    		foreach($arrTO as $rto){
    			$id_serie[] = $rto->getId_serie();
    			$arrRetorno = array('id_projetopedagogico'=>$rto->getId_projetopedagogico(), 'id_nivelensino'=>$rto->getId_nivelensino(), 'id_serie'=>$id_serie );
    		}
    		
    		$this->_helper->json($arrRetorno);
    		
    	}
    }
    
    public function deleteAction(){
        
    }

    public function headAction(){
        
    }
    

}
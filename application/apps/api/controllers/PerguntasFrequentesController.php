<?php

/**
 * Created by PhpStorm.
 * User: kayo.silva
 * Date: 26/10/2015
 * Time: 17:29
 */
class Api_PerguntasFrequentesController extends Ead1_RestRestrita
{
    private $_negocio;

    public function init()
    {
        parent::init();
        $this->_negocio = new \G2\Negocio\PerguntasFrequentes();
    }

    public function indexAction()
    {
    }

    public function deleteAction()
    {
        $id = intval($this->getParam('id'));
        if (!is_int($id)) {
            $this->getResponse()->setHttpResponseCode(400)->setBody('Não posso apagar um registro com ID inválido!');
        } else {

            $result = $this->_negocio->salvarEsquema(array(
                'bl_ativo' => false,
                'id_esquemapergunta' => $id
            ));
            if ($result) {
                $this->getResponse()->setHttpResponseCode(204)->appendBody('Registro removido');
            } else {
                $this->getResponse()->setHttpResponseCode(400)->appendBody('Erro ao tentar remover registro');
            }
        }
    }

    public function getAction()
    {

    }

    public function headAction()
    {

    }

    public function postAction()
    {
    }

    public function putAction()
    {
    }
}
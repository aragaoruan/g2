<?php

/**
 * Controller para Matricula
 * @author Rafael Rocha <rafael.rocha.mg@gmail.com>
 * @since 2014-01-28
 * @package application
 * @subpackage controller
 */
class Api_MinhaPastaController extends Ead1_RestRestrita
{

    private $negocio;

    public function init()
    {
        $this->negocio = new \G2\Negocio\MinhaPasta();
        parent::init();
    }

    public function indexAction()
    {
        $params = $this->getRequest()->getParams();
        unset($params['controller'], $params['action'], $params['module']);

        $mensageiro = $this->negocio->retornaMinhaPasta($params);

        $this->_helper->json($mensageiro);
    }

    public function getAction()
    {
        $params = $this->getRequest()->getParams();
        unset($params['controller'], $params['action'], $params['module']);

        $mensageiro = $this->negocio->retornaMinhaPasta($params);

        $this->_helper->json($mensageiro);
    }

    public function postAction()
    {
    }

    public function putAction()
    {
    }

    public function deleteAction()
    {
        $id = $this->getParam('id');

        if (isset($id)) {
            $retorno = $this->negocio->removerMinhaPasta((int)$id);

            if ($retorno->getTipo()) {
                $this->getResponse()->setHttpResponseCode(200)->appendBody(json_encode($retorno));
            } else {
                $this->getResponse()->setHttpResponseCode(400);
                $this->getResponse()->appendBody(json_encode($retorno));
            }
        }
    }

    public function headAction()
    {

    }

}

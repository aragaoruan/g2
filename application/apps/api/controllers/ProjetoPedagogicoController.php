<?php

/**
 * Controller para gerenciar os ProjetoPedagogico
 *
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @since 2013-11-25
 * @package application
 * @subpackage controller
 */
class Api_ProjetoPedagogicoController extends Ead1_RestRestrita
{

    private $doctrineContainer;
    private $em;

    public function init()
    {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
        $this->negocio = new \G2\Negocio\Turma();
    }

    public function indexAction()
    {
        $array = $this->_getAllParams();
        unset($array['module'], $array['controller'], $array['action']);

        $produto = new \G2\Negocio\ProjetoPedagogico();
        $mensageiro = $produto->findAllProjetoPedagogico();

        if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json($mensageiro);
        } else {
            $this->getResponse()->setHttpResponseCode(201);
            $this->_helper->json($mensageiro->getMensagem());
        }
    }

    public function getAction()
    {
        $ppo = new \G2\Negocio\ProjetoPedagogico();
        $entity = $ppo->findOneProjetoPedagogico(array('id_projetopedagogico' => $this->_getParam('id')));

        if ($entity) {
            $return = $entity->toBackboneArray();
            $this->_helper->json($return);
        } else {
            $this->_helper->json('');
        }
    }

    public function postAction()
    {
        try {
            $data = $this->getRequest()->getPost();
            if (!$data) {
                $body = $this->getRequest()->getRawBody();
                $data = Zend_Json::decode($body);
            }
            $fileImg = null;
            if (isset($_FILES['imagem_curso']['tmp_name'])) {
                $fileImg = $_FILES['imagem_curso'];
            }

            $to = new \ProjetoPedagogicoTO();
            $to->montaToDinamico($data);
            $to->setBl_ativo(1);

            $bo = new ProjetoPedagogicoBO();
            $mensageiro = $bo->cadastrarProjetoPedagogico($to);

            //Faz upload da imagem
            if ($fileImg) {
                $negocioProjetoPedagogico = new \G2\Negocio\ProjetoPedagogico();
                $imagem = $negocioProjetoPedagogico->uploadImagemProjeto($fileImg, $to->getId_projetopedagogico());
                if ($imagem['type'] == 'success') {
                    $to->setSt_imagem($imagem['file']);
                } else {
                    $mensageiro = new \Ead1_Mensageiro($imagem, \Ead1_IMensageiro::ERRO);
                    $this->getResponse()->setHttpResponseCode(500);
                    $this->_helper->json($mensageiro);
                }
            }

            if ($mensageiro->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                $mensageiro->setId($to->getId_projetopedagogico());
            }

        } catch (Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json($mensageiro);
        } else {
            $this->getResponse()->setHttpResponseCode(201);
            $this->_helper->json($to->toBackboneArray());
        }

    }

    public function putAction()
    {
        try {
            $data = $this->getRequest()->getPost();
            if (!$data) {
                $body = $this->getRequest()->getRawBody();
                $data = Zend_Json::decode($body);
            }
            $fileImg = null;
            if (isset($_FILES['imagem_curso']['tmp_name'])) {
                $fileImg = $_FILES['imagem_curso'];
            }

            $to = new \ProjetoPedagogicoTO();
            /*
             * seta o id do projeto para busca-lo no banco, isso faz-se necessário para sanar o bug que estava acontecendo
             * quando mandava salvar os dados da aba de Textos da tela de projeto pedagogico, ao alterar a forma de salvar
             * os dados, apenas os dados referentes a respectiva aba estava sendo passado causando erro nas verificações
             * do código legado.
             */
            $to->setId_projetopedagogico($data['id_projetopedagogico']);
            $to->fetch(true, true, true);

            //seta os parametros passados
            $to->montaToDinamico($data);
            $bo = new ProjetoPedagogicoBO();

            //Faz upload da imagem
            if ($fileImg) {
                $negocioProjetoPedagogico = new \G2\Negocio\ProjetoPedagogico();
                $imagem = $negocioProjetoPedagogico->uploadImagemProjeto($fileImg, $to->getId_projetopedagogico());
                if ($imagem['type'] == 'success') {
                    $to->setSt_imagem($imagem['file']);
                } else {
                    $mensageiro = new \Ead1_Mensageiro($imagem, \Ead1_IMensageiro::ERRO);
                    $this->getResponse()->setHttpResponseCode(500);
                    $this->_helper->json($mensageiro);
                }
            }

            if (is_array($to->getId_anexoementa())) {
                $anexo = $to->getId_anexoementa();
                $to->setId_anexoementa($anexo['id_upload']);
            }

            $mensageiro = $bo->editarProjetoPedagogico($to);

            if ($mensageiro->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                $mensageiro->setId($to->getId_projetopedagogico());
            }

        } catch (Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->setHttpResponseCode(500);
            $this->_helper->json($mensageiro);
        } else {
            $this->getResponse()->setHttpResponseCode(201);
            $this->_helper->json($to->toArray());
        }


    }

    public function deleteAction()
    {

    }

    public function headAction()
    {

    }
}

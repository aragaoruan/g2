<?php

/**
 * Controller API para alterar Manual do Aluno
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since  04-09-2014
 * @package application
 * @subpackage controller
 */
class Api_ManualDoAlunoController extends Ead1_RestRestrita {


    public function indexAction() {
        $texto = new \G2\Negocio\ConfiguracaoEntidade();
        $mensageiro = $texto->findConfiguracaoEntidade($texto->sessao->id_entidade, false);

        $return = array(
            'id_upload' => ($mensageiro->getId_manualaluno()) ? $mensageiro->getId_manualaluno()->getId_upload() : null ,
            'st_upload' => ($mensageiro->getId_manualaluno()) ?  $mensageiro->getId_manualaluno()->getSt_upload() : null
        );

        echo json_encode($return);
    }

    public function getAction() {

    }

    public function postAction() {

    }

    public function putAction() {
        
    }

    public function deleteAction() {
 
    }

    public function headAction() {
        
    }

}

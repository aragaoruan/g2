<?php
use G2\Negocio\TipoTramite;

/**
 * Controller para Tipo de Tramite
 * @author Rafael Leite <rafael.leite@unyleya.com.br>
 * @package api
 * @subpackage controller
 * @since 2017-03-27
 */
class Api_TipoTramiteController extends Ead1_RestRestrita
{

    private $_negocio;

    public function init()
    {
        parent::init();
        $this->_negocio = new \G2\Negocio\TipoTramite();
    }

    public function deleteAction()
    {

    }

    public function getAction()
    {
        $params = $this->getAllParams();
        $result = $this->_negocio->findTipoTramiteByIdCategoria($params['id_categoriatramite']);
        $retorno = array();
        foreach($result as $key=>$value){
            $retorno[$key]['id_tipotramite'] = $value->getId_tipotramite();
            $retorno[$key]['st_tipotramite'] = $value->getSt_tipotramite();
        }
        $this->_helper->json($retorno);
    }

    public function headAction()
    {

    }

    public function indexAction()
    {

    }

    public function postAction()
    {

    }

    public function putAction()
    {

    }

}
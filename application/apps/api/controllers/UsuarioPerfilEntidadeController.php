<?php

/**
 * Controller para gerenciar as UsuarioEntidade
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2014-12-02
 * @package application
 * @subpackage controller
 */

class Api_UsuarioPerfilEntidadeController extends Ead1_RestRestrita
{

    private $negocio;
    private $mensageiro;

    public function init ()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Negocio();
        $this->mensageiro = new \Ead1_Mensageiro();
    }

    public function indexAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $params = $this->getAllParams();
        $to = new VwPessoaPerfilTO();
        $to->montaToDinamico($params);

        $bo = new PerfilBO();
        $results = $bo->retornaPessoasPerfil($to);

        $json = array();
        if (isset($results->type) && $results->type == 'success') {
            foreach ($results->mensagem as $result) {
                $id_usuario = $result->getId_usuario();

                if (isset($json[$id_usuario])) {
                    // Se ja existir o user com o MESMO ID adicionado, adicionar o id_entidade ao seu array de entidades
                    $idEntidade = $result->getId_entidade();
                    $json[$id_usuario]['id_entidade'][] = $idEntidade;
                    $json[$id_usuario]['entidades_remover'][] = $idEntidade;
                    $json[$id_usuario]['st_nomeentidade'] .= ', ' . $result->st_nomeentidade;
                } else {
                    // Se nao, adicionar o usuario completo
                    $entidadesCadastradas = (array) $result->getId_entidade();
                    $json[$id_usuario] = array(
                        'id_situacao' => $result->getId_situacaousuarioperfilentidade(),
                        'bl_ativo' => $result->getBl_ativo(),
                        'id_entidade' => $entidadesCadastradas,
                        'id_perfil' => $result->getId_perfil(),
                        'id_usuario' => $id_usuario,
                        'dt_inicio' => $result->getDt_inicio() ? date('d/m/Y', $result->getDt_inicio()->getTimestamp()) : null,
                        'dt_termino' => $result->getDt_termino() ? date('d/m/Y', $result->getDt_termino()->getTimestamp()) : null,
                        'dt_cadastro' => $result->getDt_cadastro() ? date('d/m/Y', $result->getDt_cadastro()->getTimestamp()) : null,
                        'st_nomecompleto' => $result->getSt_nomecompleto(),
                        'st_situacaoperfil' => $result->getSt_situacaoperfil(),
                        'st_nomeentidade' => $result->st_nomeentidade,
                        'entidades_remover' => $entidadesCadastradas
                    );

                }

            } // foreach

            // Resetar as keys do array pro json entender que é um array
            $json = array_values($json);
        }

        $this->_helper->json($json);
    }

    public function getAction()
    {
    }

    public function postAction()
    {
        try {
            $this->_helper->viewRenderer->setNoRender(true);

            $body = $this->getRequest()->getRawBody();
            $data = Zend_Json::decode($body);

            $bo = new PerfilBO();
            $mensageiro = array();

            $data['id_entidade'] = (array) (isset($data['id_entidade']) ? $data['id_entidade'] : array());

            // Primeiramente, remover o usuario de entidades nao marcadas, caso ele esteja cadastrado
            if (isset($data['entidades_remover'])) {
                foreach ($data['entidades_remover'] as $id_entidade) {
                    if (!in_array($id_entidade, $data['id_entidade'])) {
                        $to = new UsuarioPerfilEntidadeTO();
                        $to->setId_entidade($id_entidade);
                        $to->setId_perfil($data['id_perfil']);
                        $to->setId_usuario($data['id_usuario']);
                        $to->fetch(false, true, true);

                        if ($to->getId_usuario()) {
                            $to->setBl_ativo(0);
                            $to->setDt_termino(date('Y-m-d H:i:s'));

                            $mensageiro = $bo->salvarUsuarioPerfilEntidade($to);
                        }
                    }
                }
            }

            // Adicionar o usuario em todas as entidades selecionadas
            if (isset($data['id_entidade'])) {
                foreach ($data['id_entidade'] as $id_entidade) {
                    $to = new UsuarioPerfilEntidadeTO();
                    $to->setId_situacao($data['id_situacao']);
                    $to->setBl_ativo($data['bl_ativo']);
                    $to->setId_entidade($id_entidade);
                    $to->setId_perfil($data['id_perfil']);
                    $to->setId_usuario($data['id_usuario']);
                    $to->setDt_inicio($data['dt_inicio'] ?: date('Y-m-d H:i:s'));
                    $to->setDt_termino($data['dt_termino'] ?: null);

                    $mensageiro = $bo->salvarUsuarioPerfilEntidade($to);
                }
            }

        } catch (Exception $e) {
            $this->getResponse()->setHttpResponseCode(500);
            $mensageiro = $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }

        $this->_helper->json($mensageiro);
    }

    public function putAction()
    {
        $this->postAction();
    }

    public function deleteAction()
    {
    }

    public function headAction()
    {
    }

}
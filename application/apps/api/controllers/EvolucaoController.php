<?php

/**
 * Clase Api para Evolução
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Api_EvolucaoController extends Ead1_RestRestrita
{

    /** @var \G2\Negocio\Evolucao $_negocio * */
    private $_negocio;

    public function init()
    {
        parent::init();
        $this->_negocio = new \G2\Negocio\Evolucao();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);
        $result = $this->_negocio->retornarEvolucao($params);
        $arrReturn = array();
        if ($result) {
            foreach ($result as $evolucao) {
                $arrReturn[] = $this->_negocio->toArrayEntity($evolucao);
            }
        }
        $this->getResponse()
            ->appendBody(json_encode($arrReturn));
    }

    public function getAction()
    {
        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);
        $result = $this->_negocio->retornarEvolucao($params);
        $arrReturn = array();
        if ($result) {
            foreach ($result as $evolucao) {
                $arrReturn[] = $this->_negocio->toArrayEntity($evolucao);
            }
        }
        $this->getResponse()
            ->appendBody(json_encode($arrReturn));
    }

    public function headAction()
    {

    }

    public function postAction()
    {

    }

    public function putAction()
    {

    }

    public function deleteAction()
    {

    }

}

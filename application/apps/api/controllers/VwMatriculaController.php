<?php

/**
 * Controller para VwMatricula
 * @author Rafael Rocha <rafael.rocha.mg@gmail.com>
 * @since 2014-01-28
 * @package application
 * @subpackage controller
 */
class Api_VwMatriculaController extends Ead1_RestRestrita
{

    private $doctrineContainer;
    private $em;

    public function init()
    {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
    }

    public function indexAction()
    {

        $produto = new \G2\Negocio\Produto();
        $mensageiro = $produto->findAllProduto();

        if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json($mensageiro);
        } else {
            $this->getResponse()->setHttpResponseCode(201);
            $this->_helper->json($mensageiro->getMensagem());
        }
    }

    /**
     * @return mixed
     */
    private function getIdParameter()
    {
        $id = $this->getParam('id');

        if (is_array($id) && !empty($id["id_matricula"])) {
            $id = $id["id_matricula"];
        }

        if (!$id && $this->hasParam('id_matricula')) {
            $id = $this->getParam('id_matricula');
        }

        if ($id && !is_numeric($id)) {
            throw new InvalidArgumentException(\G2\Constante\MensagemSistema::SEM_MATRICULA);
        }

        return $id;
    }

    public function getAction()
    {
        $negocio = new \G2\Negocio\Matricula();
        $matriculaBO = new MatriculaBO();
        $to = new \VwMatriculaTO();
        try {
            $id = $this->getIdParameter();
        } catch (InvalidArgumentException $argException) {
            return $this->getResponse()->setHttpResponseCode(500)
                ->appendBody($argException->getMessage());
        }

        $to->setId_matricula($id);
        $to->setId_usuario($this->_getParam('id_usuario'));
        $to->setBl_ativo(true);
        if (!$to->getSessao()->id_entidade) {
            throw new Exception(\G2\Constante\MensagemSistema::SEM_ENTIDADE_SESSAO);
        }
        if ($this->_getParam('id_evolucao', null) != null) {
            $to->setId_evolucao($this->_getParam('id_evolucao'));
        }

        $id_entidade = $this->getParam('id_entidade');

        if ($this->getParam('id_entidadematricula')) {
            $to->setId_entidadematricula($this->getParam('id_entidadematricula'));
        }

        $responsavelLegal = $matriculaBO->retornaResponsavelLegal($this->_getParam('id_usuario'));
        $mensageiro = $matriculaBO->retornarMatriculaRelacaoEntidadeSessao($to, $id_entidade);
        $retorno = array();
        $result = $mensageiro->getMensagem();
        //$to = $to->fetch(false, $this->_getParam('id'));
        $format = 'd/m/Y H:i';
        $format2 = 'd/m/Y';

        if (is_array($result)) {
            if ($this->getParam('id')) {
                if (isset($result[0]) && is_object($result[0])) {
                    $id_matricula = $result[0]->getId_matricula();

                    $entity = $negocio->findOneBy('\G2\Entity\VwMatricula', array(
                        'id_matricula' => $id_matricula
                    ));
                    $retorno = $negocio->toArrayEntity($entity);

                    // Calcular o Coeficiente de Rendimento Acumulado
                    $retorno['nu_coeficienterendimento'] = $negocio->retornarCoeficienteRendimento($id_matricula);

                    // Ajustar formato das datas
                    $retorno['dt_inicio'] = \G2\Utils\Helper::converterData($retorno['dt_inicio'], $format);
                    $retorno['dt_termino'] = \G2\Utils\Helper::converterData($retorno['dt_termino'], $format);
                    $retorno['dt_cadastro'] = \G2\Utils\Helper::converterData($retorno['dt_cadastro'], $format);
                    $retorno['dt_concluinte'] = \G2\Utils\Helper::converterData($retorno['dt_concluinte'], $format2);
                    $retorno['dt_terminomatricula'] = \G2\Utils\Helper::converterData(
                        $retorno['dt_terminomatricula']['date'],
                        $format
                    );
                    $retorno['dt_colacao'] = \G2\Utils\Helper::converterData($retorno['dt_colacao'], $format2);

                    if ($retorno['st_urlportalmatricula']) {

                        if (array_key_exists('bl_novoportal', $retorno) && $retorno['bl_novoportal']) {
                            $retorno['st_urlacesso'] = $retorno['st_urlnovoportal']
                                . '/login/direto/0/' . $to->getSessao()->id_usuario
                                . '/' . $retorno['id_entidadematricula']
                                . '/' . $retorno['id_usuario']
                                . '/' . $retorno['st_urlacesso'];
                        } else {
                            $retorno['st_urlacesso'] = Ead1_Ambiente::geral()->st_url_portal
                                . '/portal/login/autenticacao-direta?'
                                . 'l=nao&m=' . $to->getSessao()->id_usuario
                                . '&e=' . $retorno['id_entidadematricula']
                                . '&p=' . $retorno['id_usuario']
                                . '&u=' . $retorno['st_urlacesso'];
                        }

                    } else {
                        $retorno['st_urlacesso'] = null;
                    }

                    $cargaHoraria = $negocio->getRepository('\G2\Entity\VwMatriculaCargaHoraria')
                        ->find($id_matricula);

                    $retorno['nu_cargahorariaintegralizada'] = $cargaHoraria ? $cargaHoraria->getNu_cargahorariaintegralizada() : 0;
                }
            } else {
                if (isset($result[0]) && is_object($result[0])) {
                    foreach ($result as $key => $obj) {
                        $id_matricula = $obj->getId_matricula();

                        $entity = $negocio->findOneBy('\G2\Entity\VwMatricula', array(
                            'id_matricula' => $id_matricula
                        ));
                        $retorno[$key] = $negocio->toArrayEntity($entity);

                        // Calcular o Coeficiente de Rendimento Acumulado
                        $retorno[$key]['nu_coeficienterendimento'] = $negocio
                            ->retornarCoeficienteRendimento($id_matricula);

                        // Ajustar formato das datas
                        $retorno[$key]['dt_inicio'] = \G2\Utils\Helper::converterData(
                            $retorno[$key]['dt_inicio'],
                            $format
                        );
                        $retorno[$key]['dt_termino'] = \G2\Utils\Helper::converterData(
                            $retorno[$key]['dt_termino'],
                            $format
                        );
                        $retorno[$key]['dt_cadastro'] = \G2\Utils\Helper::converterData(
                            $retorno[$key]['dt_cadastro'],
                            $format
                        );
                        $retorno[$key]['dt_concluinte'] = \G2\Utils\Helper::converterData(
                            $retorno[$key]['dt_concluinte'],
                            $format2
                        );
                        $retorno[$key]['dt_terminomatricula'] = \G2\Utils\Helper::converterData(
                            $retorno[$key]['dt_terminomatricula']['date'],
                            $format
                        );
                        $retorno[$key]['dt_colacao'] = \G2\Utils\Helper::converterData(
                            $retorno[$key]['dt_colacao'],
                            $format2
                        );

                        if ($retorno[$key]['st_urlportalmatricula']) {

                            if (array_key_exists('bl_novoportal', $retorno) && $retorno[$key]['bl_novoportal']) {
                                $retorno[$key]['st_urlacesso'] = $retorno['st_urlnovoportal']
                                    . '/login/direto/0/' . $to->getSessao()->id_usuario
                                    . '/' . $to->getSessao()->id_entidade
                                    . '/' . $retorno[$key]['id_usuario']
                                    . '/' . $retorno[$key]['st_urlacesso'];
                            } else {
                                $retorno[$key]['st_urlacesso'] = $retorno[$key]['st_urlportalmatricula']
                                    . '/portal/login/autenticacao-direta?' . 'l=nao&m='
                                    . $to->getSessao()->id_usuario
                                    . '&e=' . $to->getSessao()->id_entidade . '&p='
                                    . $retorno[$key]['id_usuario']
                                    . '&u=' . $retorno[$key]['st_urlacesso'];
                            }
                        } else {
                            $retorno[$key]['st_urlacesso'] = null;
                        }

                        $cargaHoraria = $negocio->getRepository('\G2\Entity\VwMatriculaCargaHoraria')
                            ->find($id_matricula);

                        $retorno[$key]['nu_cargahorariaintegralizada'] = $cargaHoraria ? $cargaHoraria->getNu_cargahorariaintegralizada() : 0;
                    }
                }
            }
        }
        $this->_helper->json($retorno);
    }

    public function postAction()
    {

    }

    public function putAction()
    {

    }

    public function deleteAction()
    {

    }

    public function headAction()
    {

    }

}



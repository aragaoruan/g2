<?php

/**
 * Controller no modulo api para log de acessos
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-04-11
 * @package application
 * @subpackage controller
 */
class Api_LogAcessoController extends Ead1_RestRestrita
{

    private $negocio;

    public function init ()
    {
        parent::init();
        $this->negocio = new G2\Negocio\LogAcesso();
    }

    public function deleteAction ()
    {
        
    }

    public function getAction ()
    {
        
    }

    public function headAction ()
    {
        
    }

    public function indexAction ()
    {
        
    }

    public function postAction ()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
        if ($data) {
            $result = $this->negocio->salvaLogAcesso($data);
            if ($result) {
                $arrReturn = array(
                    'id_logacesso' => $result->getId_logacesso(),
                    'id_funcionalidade' => $result->getId_funcionalidade(),
                    'id_perfil' => $result->getId_perfil(),
                    'id_usuario' => $result->getId_usuario(),
                    'id_saladeaula' => $result->getId_saladeaula(),
                    'id_entidade' => $result->getId_entidade(),
                    'dt_cadastro' => $result->getDt_cadastro()
                );
                $this->getResponse()->setHttpResponseCode(201)->appendBody(json_encode($arrReturn));
            } else {
                $this->getResponse()->setHttpResponseCode(400)->appendBody(json_encode($result));
            }
        } else {
            $this->getResponse()->setHttpResponseCode(400)->appendBody('Post inválido, sem dados para postar.');
        }
    }

    public function putAction ()
    {
        
    }

}

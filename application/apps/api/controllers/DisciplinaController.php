<?php

/**
 * Controller para criar aplicações para avaliação (locais de prova para agendamento de prova final)
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since  2013-18-12
 * @package Api
 * @subpackage controller
 */
class Api_DisciplinaController extends Ead1_RestRestrita
{

    /**
     * @var \G2\Negocio\Disciplina
     */
    private $negocio;
    private $repositoryName;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Disciplina();
        $this->repositoryName = new \G2\Entity\Disciplina();
    }

    public function deleteAction()
    {
        $id = intval($this->getParam('id'));
        if (!is_int($id)) {
            $this->getResponse()->setHttpResponseCode(400)->setBody('Não posso apagar um registro com ID inválido!');
        } else {
            $result = $this->negocio->deletaDisciplina($id);
            if ($result) {
                $this->getResponse()->setHttpResponseCode(204)->appendBody('Registro removido');
            } else {
                $this->getResponse()->setHttpResponseCode(400)->appendBody('Erro ao tentar remover registro');
            }
        }
    }

    public function getAction()
    {
        $params = $this->getAllParams();
        if (isset($params['id'])) {
            $params['id_disciplina'] = $params['id'];
            unset($params['id']);
        }

        unset($params['controller'], $params['action'], $params['module']);
        $result = $this->negocio->findOneBy($this->repositoryName, $params);
        $arrReturn = [];

        if ($result instanceof \G2\Entity\Disciplina) {
            $arrReturn = (new \G2\Transformer\Disciplina\DisciplinaTransformer(array($result)))->getTransformer();
        }

        $this->getResponse()
            ->appendBody(json_encode($arrReturn));
    }

    public function headAction()
    {

    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);

        $result = $this->negocio->findByEntidadeSessao($params);
        $arrReturn = (new \G2\Transformer\Disciplina\DisciplinaTransformer($result))->getTransformer();


        $this->getResponse()
            ->appendBody(json_encode($arrReturn));
    }

    public function postAction()
    {
        $data = $this->getAllParams();
        $data['bl_ativa'] = true;

        $fileImg = null;
        if (isset($_FILES['imagem_disciplina']['tmp_name'])) {
            $fileImg = $_FILES['imagem_disciplina'];
        }

        try {
            $result = $this->negocio->salvaDisciplina($data, $fileImg);

            if ($result instanceof \G2\Entity\Disciplina) {
                $arrReturn = (new \G2\Transformer\Disciplina\DisciplinaTransformer(array($result)))->getTransformer();
                $this->getResponse()->appendBody(json_encode($arrReturn[0]));
                $this->getResponse()->setHttpResponseCode(201);
            } else {
                $this->getResponse()->appendBody(json_encode("Erro ao salvar Disciplina"));
                $this->getResponse()->setHttpResponseCode(400);
            }
        } catch (\Exception $e) {
            $this->getResponse()->appendBody(json_encode($e->getMessage()));
            $this->getResponse()->setHttpResponseCode(400);
        }


    }

    public function putAction()
    {

        $data = $this->getRequest()->getPost();
        if (!$data) {
            $body = $this->getRequest()->getRawBody();
            $data = Zend_Json::decode($body);
        }

        if(isset($data['id']) && $data['id']){
            $data['id_disciplina'] = $data['id'];
            unset($data['id']);
        }

        $fileImg = null;
        if (isset($_FILES['imagem_disciplina']['tmp_name'])) {
            $fileImg = $_FILES['imagem_disciplina'];
        }
        try {
            $result = $this->negocio->salvaDisciplina($data, $fileImg);

            if ($result instanceof \G2\Entity\Disciplina) {
                $arrReturn = (new \G2\Transformer\Disciplina\DisciplinaTransformer(array($result)))->getTransformer();
                $this->getResponse()->appendBody(json_encode($arrReturn[0]));
                $this->getResponse()->setHttpResponseCode(201);
            } else {
                $this->getResponse()->appendBody(json_encode("Erro ao salvar Disciplina"));
                $this->getResponse()->setHttpResponseCode(400);
            }
        } catch (\Exception $e) {
            $this->getResponse()->appendBody($e->getMessage());
            $this->getResponse()->setHttpResponseCode(400);
        }
    }

    public function getRetornarVwProfessorDisciplinaAction()
    {
        $params = $this->getAllParams();
//        $tipo_disciplina = $this->getParam('tipo_disciplina');

        Zend_Debug::dump($params);
        die;

        $bo = new DisciplinaBO();
        $vw = new VwProfessorDisciplinaTO();

        $vw->setId_tipodisciplina($tipo_disciplina);

        Zend_Debug::dump($bo->retornarVwProfessorDisciplina($vw));
        die;
    }
}

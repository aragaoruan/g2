<?php

/**
 * @package application
 * @subpackage controller
 */
class Api_CategoriaSalaController extends Ead1_RestRestrita {

    private $doctrineContainer;
    private $em;
    private $bo;

    public function init() {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
        $this->bo = new \G2\Negocio\CategoriaSala();
    }

    public function indexAction() {

        $mensageiro = $this->bo->findAll();

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $mensageiro->setText(count($mensageiro->getMensagem()) . ' registros encontrados!');
        }

        $this->getResponse()
                ->appendBody(json_encode($mensageiro->getMensagem()));
    }

    public function getAction() {

        $mensageiro = $this->bo->findById($this->_getParam('id', null));

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $mensageiro->setText(count($mensageiro->getMensagem()) . ' registros encontrados!');
        }

        $this->getResponse()
                ->appendBody(json_encode($mensageiro->getFirstMensagem()));
    }

    public function postAction() {
        
    }

    public function putAction() {
        
    }

    public function deleteAction() {
        
    }

    public function headAction() {
        
    }

}

<?php

/**
 * Controller API para Funcionalidade Contatos do Site
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2013-11-06
 */
use G2\Negocio\Prevenda;

class Api_ContatosSiteController extends Ead1_RestRestrita
{

    /** @var \G2\Negocio\Prevenda $_negocio */
    private $_negocio;

    public function init ()
    {
        parent::init();
        $this->_negocio = new Prevenda();
    }

    public function deleteAction ()
    {

    }

    public function getAction ()
    {

    }

    public function headAction ()
    {

    }

    public function indexAction()
    {

        $params['id_situacao'] = $this->getParam('id_situacao');
        $params['dt_inicio'] = $this->getParam('dt_inicio');
        $params['dt_fim'] = $this->getParam('dt_fim');
        $params['id_usuarioatendimento'] = $this->getParam('id_atendente');
        $params['dt_ultimainteracao'] = $this->getParam('dt_interacao');
        $params['st_nomecompleto'] = $this->getParam('st_nomecompleto', null);
        $params['busca'] = $this->getParam('busca', null);
        $return = $this->_negocio->returnPreVendas($params);
        $this->_helper->json($return);
    }

    public function postAction ()
    {

    }

    public function putAction ()
    {

    }

}

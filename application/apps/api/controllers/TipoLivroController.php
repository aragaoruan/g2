<?php

/**
 * Controller para gerenciar os livros
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @since 2013-11-28
 * @package application
 * @subpackage controller
 */
class Api_TipoLivroController extends Ead1_RestRestrita
{
    
    private $doctrineContainer;
    private $em;
    
    public function init ()
    {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
    }
    
     public function indexAction()
    {
    	
    	$negocio = new \G2\Negocio\Livro();
    	
    	$result = $negocio->findAllTipoLivro();
    	$this->_helper->json($result);
    }

    public function getAction()
    {
        
//         $produto = new \G2\Negocio\Livro();
//         $mensageiro = $produto->findTipoLivroById($this->_getParam('id', null));
        
//         if($mensageiro->getTipo()==Ead1_IMensageiro::SUCESSO){
//             $this->getResponse()
//                     ->appendBody(json_encode($mensageiro->getFirstMensagem()));
            
//         }
        
        
    }
    
    public function postAction()
    {
    }
    
    public function putAction()
    {
//         try {

//             $body       = $this->getRequest()->getRawBody();
//             $data       = Zend_Json::decode($body);
//             $produto   = new \G2\Negocio\TipoLivro();
            
//             $this->getResponse()
//                     ->appendBody($produto->salvarTipoLivro($data)->toJson());
            
//         } catch (Exception $exc) {
//             $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
//             $this->getResponse()
//                     ->appendBody($mensageiro->toJson());
//         }

    }
    
    public function deleteAction(){
        
    }

    public function headAction(){
        
    }
    

}
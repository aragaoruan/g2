<?php

/**
 * Controller para gerenciar Municipio
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2015-10-01
 * @package application
 * @subpackage controller
 */
class Api_MunicipioController extends Ead1_RestRestrita
{

    /**
     * @var \G2\Negocio\Negocio $negocio
     */
    private $negocio;
    private $repositoryName = '\G2\Entity\Municipio';

    public function init()
    {
        parent::init();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->negocio = new \G2\Negocio\Negocio();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['module'], $params['controller'], $params['action']);

        $results = $this->negocio->findBy($this->repositoryName, $params, array('st_nomemunicipio' => 'ASC'));
        foreach ($results as $key => &$entity) {
            $entity = $this->negocio->toArrayEntity($entity);
        }

        $this->_helper->json($results);
    }

}
<?php

/**
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @since 2014-01-10
 * @package application
 * @subpackage controller
 */
class Api_VwDisciplinaSerieNivelController extends Ead1_RestRestrita
{


    public function indexAction()
    {

        $params = $this->getAllParams();

        $negocio = new \G2\Negocio\Disciplina();
        $mensageiro = $negocio->retornarVwDisciplinaSerieNivel([
            'st_disciplina' => $params['st_disciplina'],
            'id_entidade' => $negocio->sessao->id_entidade
        ]);

        if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json($mensageiro);
        } else {
            $this->getResponse()->setHttpResponseCode(201);
            $this->_helper->json($mensageiro->getMensagem());
        }
    }

    public function getAction()
    {

    }

    public function postAction()
    {
    }

    public function putAction()
    {
    }

    public function deleteAction()
    {

    }

    public function headAction()
    {

    }


}
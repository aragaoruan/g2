<?php

/**
 * Controller para gerenciar os tipos de contrato
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @since 2013-10-15
 * @package application
 * @subpackage controller
 */
class Api_ProjetoContratoDuracaoTipoController extends Ead1_RestRestrita
{
    
    private $doctrineContainer;
    private $em;
    
    public function init ()
    {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
    }
    
     public function indexAction()
    {
        
        $contrato = new \G2\Negocio\Contrato();
        $mensageiro = $contrato->findAllProjetoContratoDuracaoTipo();
        
        if($mensageiro->getTipo()==Ead1_IMensageiro::SUCESSO){
            $mensageiro->setText(count($mensageiro->getMensagem()).' registros encontrados!');
        }
        
        $this->getResponse()
                ->appendBody(json_encode($mensageiro->mensagem));
        
        
    }

    public function getAction()
    {
        $mensageiro = new \Ead1_Mensageiro("Funcionalidade não implementada!", \Ead1_IMensageiro::AVISO);
        $this->getResponse()
                ->appendBody($mensageiro->toJson());
        
    }
    
    public function postAction()
    {
        $mensageiro = new \Ead1_Mensageiro("Funcionalidade não implementada!", \Ead1_IMensageiro::AVISO);
        $this->getResponse()
                ->appendBody($mensageiro->toJson());
    }
    
    public function putAction()
    {

        $mensageiro = new \Ead1_Mensageiro("Funcionalidade não implementada!", \Ead1_IMensageiro::AVISO);
        $this->getResponse()
                ->appendBody($mensageiro->toJson());
    }
    
    public function deleteAction()
    {
        $mensageiro = new \Ead1_Mensageiro("Funcionalidade não implementada!", \Ead1_IMensageiro::AVISO);
        $this->getResponse()
                ->appendBody($mensageiro->toJson());
        
    }

    public function headAction(){
        
    }
    

}
<?php

/**
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @date 2017-10-16
 */
class Api_MatriculaDiplomacaoController extends Ead1_RestRestrita
{

    /**
     * @var \G2\Negocio\Negocio $negocio
     */
    private $negocio;
    private $repositoryName = '\G2\Entity\MatriculaDiplomacao';

    public function init()
    {
        parent::init();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->negocio = new \G2\Negocio\Negocio();
    }

    public function indexAction()
    {
        try {
            $params = $this->getAllParams();
            unset($params['module'], $params['controller'], $params['action']);

            $results = $this->negocio->findBy($this->repositoryName, array_merge(array(
                'bl_ativo' => true
            ), $params));

            $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($results ? $this->negocio->toArrayEntity($results) : array()));
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(500)->appendBody($e->getMessage());
        }
    }

    public function getAction()
    {
        try {
            $result = $this->negocio->find($this->repositoryName, $this->getParam('id'));
            $this->_helper->json($result ? $this->negocio->toArrayEntity($result) : (new stdClass()));
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(500)->appendBody($e->getMessage());
        }
    }

    public function postAction()
    {
        try {
            $data = Zend_Json::decode($this->getRequest()->getRawBody());

            /** @var \G2\Entity\MatriculaDiplomacao $entity */
            $entity = $this->negocio
                ->preencherEntity($this->repositoryName, 'id_matriculadiplomacao', $data, array(
                    'bl_ativo' => true,
                    'dt_cadastro' => date('Y-m-d H:i:s'),
                    'id_entidade' => $this->negocio->sessao->id_entidade,
                    'id_usuariocadastro' => $this->negocio->sessao->id_usuario
                ));

            $response = new stdClass();
            if ($this->negocio->save($entity)) {
                $results = $this->negocio->findBy('\G2\Entity\VwMatriculaDiplomacao', array(
                    'id_matriculadiplomacao' => $entity->getId_matriculadiplomacao()
                ));

                $response = array_shift(
                    (new \G2\Transformer\MatriculaDiplomacao\MatriculaDiplomacaoTransformer($results))
                        ->getTransformer());
            }

            $this->getResponse()->setHttpResponseCode(201)->appendBody(json_encode($response));
        } catch (\Zend_Exception $e) {
            $this->getResponse()->setHttpResponseCode(500)->appendBody(json_encode(array('text' => $e->getMessage())));
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(500)->appendBody(json_encode(array(
                'type' => 'error',
                'title' => 'Erro',
                'text' => 'Ocorreu um erro ao tentar realizar a operação.'
            )));
        }
    }

    public function putAction()
    {
        $this->postAction();
    }


}

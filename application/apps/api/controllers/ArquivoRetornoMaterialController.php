<?php

/**
 * Controller API para arquivo retorno de material
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @since  30-03-2015
 * @package application
 * @subpackage controller
 */
class Api_ArquivoRetornoControllerController extends Ead1_RestRestrita {

    private $negocio;

    public function init() {
        $this->_helper->viewRenderer->setNoRender(true);
        parent::init();
        $this->negocio = new \G2\Negocio\ArquivoRetornoMaterial();
    }

    public function indexAction() {
        
    }

    public function getAction() {
        
    }

    public function postAction() {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json_Decoder::decode($body);
        $this->getResponse()->appendBody(($this->negocio->salvarArquivoRetorno($data)->toJson()));
        $this->getResponse()->setHttpResponseCode(201);
    }

    public function putAction() {
        
    }

    public function deleteAction() {
 
    }

    public function headAction() {
        
    }

}

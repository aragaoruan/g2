<?php

/**
 * Controller para gerenciar VwNucleoCoEmail
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2014-12-23
 * @package application
 * @subpackage controller
 */

class Api_VwNucleoCoEmailController extends Ead1_RestRestrita {

    private $bo;
    private $negocio;

    public function init ()
    {
        parent::init();
        $this->bo = new NucleoCoBO();
        $this->negocio = new \G2\Negocio\Negocio();
    }

    public function indexAction()
    {
        $data = $this->getAllParams();

        $to = new VwNucleoCoEmailTO();
        foreach ($data as $key => $value) {
            if (array_key_exists($key, $to)) {
                $to->{$key} = $value;
            }
        }

        if (!$to->getId_entidade())
            $to->setId_entidade($this->negocio->sessao->id_entidade);

        $mensageiro = $this->bo->retornaEmail($to);
        $this->_helper->json($mensageiro);
    }

    public function getAction()
    {
        $id = $this->getParam('id');

        if ($id) {
            $to = new VwNucleoCoEmailTO();
            $to->setId_entity($id);

            $mensageiro = $this->bo->retornaEmail($to);

            if ($mensageiro->type == 'success' && is_array($mensageiro->mensagem))
                $mensageiro->mensagem = array_shift($mensageiro->mensagem);

            $this->_helper->json($mensageiro);
        }
    }

    public function postAction()
    {
        // Necessario passar um array com os IDS dos emails no RAWBODY
        $emails = Zend_Json::decode($this->getRequest()->getRawBody());

        // Necessario passar o id do NucleoCo nos parametros GET ou POST
        $data = $this->getAllParams();

        if (isset($emails['emails'])) {

            $to = new NucleoCoEmailTO();
            foreach ($data as $key => $value) {
                if (array_key_exists($key, $to)) {
                    if (substr($key, 0, 3) == 'st_')
                        $value = utf8_decode($value);
                    $to->{$key} = $value;
                }
            }

            $mensageiro = $this->bo->salvarEmails($emails['emails'], $to);
            $this->_helper->json($mensageiro);
        }

    }

    public function putAction()
    {
        $this->postAction();
    }

    public function deleteAction()
    {
        $data = $this->getAllParams();

                $id = $this->getParam('id');

        if ($id) {
            $to = new NucleoCoEmailTO();
            $to->setId_emailconfig($id);

            foreach ($data as $key => $value) {
                if (array_key_exists($key, $to)) {
                    if (substr($key, 0, 3) == 'st_')
                        $value = utf8_decode($value);
                    $to->{$key} = $value;
                }
            }

            $mensageiro = $this->bo->excluirEmail($to);
            $this->_helper->json($mensageiro);
        }
    }

    public function headAction()
    {
    }

}
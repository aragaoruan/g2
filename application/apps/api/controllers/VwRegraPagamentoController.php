<?php

/**
 * @package application
 * @subpackage controller
 */
class Api_VwRegraPagamentoController extends Ead1_RestRestrita {

    private $doctrineContainer;
    private $em;
    private $bo;

    public function init() {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
        $this->bo = new G2\Negocio\VwRegraPagamento();
    }

    public function indexAction() {

        $mensageiro = $this->bo->findAll();

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $mensageiro->setText(count($mensageiro->getMensagem()) . ' registros encontrados!');
        }

        $this->getResponse()
                ->appendBody(json_encode($mensageiro->getMensagem()));
    }

    public function getAction() {

        $mensageiro = $this->bo->findById($this->_getParam('id', null));

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $mensageiro->setText(count($mensageiro->getMensagem()) . ' registros encontrados!');
        }

        $this->getResponse()->appendBody(json_encode($mensageiro->getFirstMensagem()));
    }

    public function postAction() {
        
    }

    public function putAction() {
        
    }

    public function deleteAction() {
        try {
            $this->bo->delete($this->bo->find('G2\Entity\RegraPagamento', $this->getParam('id')));
            $this->getResponse()->setHttpResponseCode(204);
        } catch (Exception $e) {
            $this->getResponse()->setHttpResponseCode(500);
        }
    }

    public function headAction() {
        
    }

}

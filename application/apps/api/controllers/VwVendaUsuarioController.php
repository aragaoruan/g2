<?php


/**
 * Class Api_VwVendaUsuarioController classe api para vw
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-09-25
 */
class Api_VwVendaUsuarioController extends Ead1_RestRestrita
{

    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Venda();
    }

    public function indexAction()
    {
        $result = $this->negocio->retornarVwVendaUsuario();
        $arrReturn = array();
        if ($result) {
            foreach ($result as $row) {
                $arrReturn[] = $this->negocio->toArrayEntity($row);
            }
        }

        $this->getResponse()
            ->appendBody(json_encode($arrReturn));
    }

    public function getAction()
    {
        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);

        if (array_key_exists('id', $params) && !empty($params['id'])) {
            $params['id_venda'] = $params['id'];
            unset($params['id']);
        }

        $result = $this->negocio->retornarVwVendaUsuario($params);
        $arrReturn = array();
        if ($result) {
            foreach ($result as $row) {
                $arrReturn[] = $this->negocio->toArrayEntity($row);
            }

            if (count($result) == 1) {
                $arrReturn = $arrReturn[0];
            }

        }

        $this->getResponse()
            ->appendBody(json_encode($arrReturn));
    }

    public function headAction()
    {
    }

    public function postAction()
    {
    }

    public function putAction()
    {
    }

    public function deleteAction()
    {
    }
}
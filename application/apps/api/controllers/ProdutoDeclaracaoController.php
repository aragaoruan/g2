<?php

/**
 * Controller para gerenciar os produtos
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 * @since 18/06/2015
 * @package application
 * @subpackage controller
 */
class Api_ProdutoDeclaracaoController extends Ead1_RestRestrita
{

    private $doctrineContainer;
    private $em;
    private $negocio;

    public function init()
    {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
        $this->negocio = new \G2\Negocio\TextoSistema();
    }

    public function indexAction()
    {
        $textoNegocio = new \G2\Negocio\TextoSistema();
        $data = $this->getRequest()->getParams();
        unset($data['controller'], $data['action'], $data['module']);

        $result = $textoNegocio->findByProdutoTextoSistemaArray($data);
        $result = $this->_helper->json($result);

        return $result;

    }

    public function getAction()
    {
        $textoNegocio = new \G2\Negocio\TextoSistema();
        $data = $this->getRequest()->getParams();
        unset($data['controller'], $data['action'], $data['module']);

        $result = $textoNegocio->findByTextoSistemaArray($data);
        $result = $this->_helper->json($result);
        return $result;
    }

    public function postAction()
    {
        $data = $this->getRequest()->getPost();

        $mensageiro = $this->negocio->salvarProdutoDeclaracao($data);

        if ($mensageiro->getTipo() == 1) {
            $this->getResponse()->setHttpResponseCode(201);
            $this->_helper->json($data);
        } else {
            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json($mensageiro);
        }
    }

    public function putAction()
    {
        try {

            $body = $this->getRequest()->getRawBody();
            $data = Zend_Json::decode($body);
            $produto = new \G2\Negocio\Produto();

            $this->getResponse()
                ->appendBody($produto->salvarProdutoLivro($data)->toJson());

        } catch (Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
            $this->getResponse()
                ->appendBody($mensageiro->toJson());
        }

    }

    public function deleteAction()
    {

    }

    public function headAction()
    {

    }


}
<?php

/**
 * Controller para gerenciar salas
 * @author Débora Castro <debora.castro@unyleya.com.br>
 * @since  07-10-2013
 * @package application
 * @subpackage controller
 */
class Api_GerenciaSalasController extends Ead1_RestRestrita {

    /**
     * @var \G2\Negocio\GerenciaSalas $negocio
     */
    private $negocio;

    public function init() {
        parent::init();
        $this->negocio = new \G2\Negocio\GerenciaSalas();
    }

    public function indexAction() {
        
    }

    public function getAction() {
        
    }

    public function postAction() {

        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json_Decoder::decode($body);
        $this->getResponse()->appendBody((json_encode($this->negocio->salvaSalaDeAula($data))));
    }

    public function putAction() {
    }

    public function deleteAction() {
        
    }

    public function headAction() {
        
    }

}

<?php

/**
 * Controller API para cadastro/edição Sala de Aula
 * @author Débora Castro <debora.castro@unyleya.com.br>
 * @since  10-07-2014
 * @package application
 * @subpackage controller
 */
class Api_SalaDeAulaController extends Ead1_RestRestrita
{

    /** @var \G2\Negocio\GerenciaSalas */
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\GerenciaSalas();
    }

    public function indexAction()
    {

    }

    public function getAction()
    {

    }

    public function postAction()
    {
        try {
            $body = $this->getRequest()->getRawBody();
            $data = Zend_Json_Decoder::decode($body);
            $return = $this->negocio->salvarSalaDeAulaDadosBasicos($data);

            if (is_array($return) && isset($return['existeSalaComMesmoNome'])) {
                throw new Exception($return["text"]);
            }

            if (($return instanceof Ead1_Mensageiro) && $return->getType() != Ead1_IMensageiro::TYPE_SUCESSO) {
                throw new Exception($return->getText());
            }

            return $this->getResponse()->setHttpResponseCode(201)
                ->appendBody(json_encode($return->toArrayAll()));
        } catch (Exception $e) {
            return $this->getResponse()->setHttpResponseCode(500)
                ->appendBody($e->getMessage());
        }
    }

    public function putAction()
    {

    }

    public function deleteAction()
    {

    }

    public function headAction()
    {

    }

}

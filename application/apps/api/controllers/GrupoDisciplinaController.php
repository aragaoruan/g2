<?php

/**
 * Controller para gerenciar GrupoDisciplina
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2015-12-16
 * @package application
 * @subpackage controller
 */

class Api_GrupoDisciplinaController extends Ead1_RestRestrita {

    /**
     * @var \G2\Negocio\Negocio $negocio
     */
    private $negocio;
    private $repositoryName = '\G2\Entity\GrupoDisciplina';

    public function init ()
    {
        parent::init();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->negocio = new \G2\Negocio\Negocio();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        $orderBy = $this->getParam('order_by') ? array($this->getParam('order_by') => $this->getParam('sort') ? $this->getParam('sort') : 'ASC') : null;
        unset($params['module'], $params['controller'], $params['action'], $params['order_by'], $params['sort']);

        $mensageiro = $this->negocio->findBy($this->repositoryName, $params, $orderBy);

        foreach ($mensageiro as $key => &$entity) {
            $entity = $entity->toArray();
        }

        $this->_helper->json($mensageiro);
    }

    public function getAction()
    {
        $entity = $this->negocio->find($this->repositoryName, $this->getParam('id'));
        if ($entity)
            $this->_helper->json($this->negocio->toArrayEntity($entity));
    }

}
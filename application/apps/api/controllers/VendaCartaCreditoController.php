<?php

/**
 * Class Api_VendaCartaCreditoController
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2015-19-01
 */
class Api_VendaCartaCreditoController extends Ead1_RestRestrita
{
    /**
     * @var \G2\Negocio\CartaCredito
     */
    private $_negocio;

    public function init()
    {
        parent::init();
        $this->_negocio = new \G2\Negocio\CartaCredito();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['module'], $params['action'], $params['controller']);
        $result = $this->_negocio->retornarDadosVendaCartaCredito($params, true);
        $this->getResponse()
            ->appendBody(json_encode($result));
    }

    public function getAction()
    {
    }

    public function headAction()
    {
    }

    public function postAction()
    {
    }

    public function putAction()
    {
    }

    public function deleteAction()
    {
    }
}
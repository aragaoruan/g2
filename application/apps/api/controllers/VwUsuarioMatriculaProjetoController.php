<?php

/**
 * Description of VwUsuarioMatriculaProjetoController
 *
 * @author Rafael Bruno(RBD) <rafael.oliveira@unyleya.com.br>
 */
class Api_VwUsuarioMatriculaProjetoController extends Ead1_RestRestrita {

    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new G2\Negocio\Matricula();
    }

    public function getAction()
    {
        $params = $this->getAllParams();
        unset($params['module'], $params['controller'], $params['action']);
        $result = $this->negocio->retornaVwUsuarioMatriculaProjeto($params);
        $toArray = array();

        foreach ($result as $entity) {
            $toArray[] = $this->negocio->toArrayEntity($entity);
        }

        $this->_helper->json($toArray);
    }

    public function deleteAction()
    {
        
    }

    public function headAction()
    {
        
    }

    public function indexAction()
    {
        
    }

    public function postAction()
    {
        
    }

    public function putAction()
    {
        
    }

}

<?php

/**
 * Controller para gerenciar NucleoFinalidade
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2015-01-01
 * @package application
 * @subpackage controller
 */

class Api_NucleoFinalidadeController extends Ead1_RestRestrita {

    private $bo;

    public function init ()
    {
        parent::init();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->bo = new NucleoCoBO();
    }

    public function indexAction()
    {
        $data = $this->getAllParams();

        $to = new NucleoFinalidadeTO();
        foreach ($data as $key => $value) {
            if (array_key_exists($key, $to)) {
                $to->{$key} = $value;
            }
        }

        $mensageiro = $this->bo->retornarNucleoFinalidadeTO($to);
        $this->_helper->json($mensageiro);
    }

    public function getAction()
    {
        $id = $this->getParam('id');

        if ($id) {
            $to = new NucleoFinalidadeTO();
            $to->setId_nucleofinalidade($id);

            $mensageiro = $this->bo->retornarNucleoFinalidadeTO($to);

            if ($mensageiro->type == 'success' && is_array($mensageiro->mensagem))
                $mensageiro->mensagem = array_shift($mensageiro->mensagem);

            $this->_helper->json($mensageiro);
        }
    }

}
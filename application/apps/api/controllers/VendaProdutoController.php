<?php

/**
 * Description of VendaController
 *
 * @author rafaelbruno
 */
class Api_VendaProdutoController extends Ead1_RestRestrita {

    private $_negocio;


    public function deleteAction() {
        if ($this->_getParam('id')) {
            $this->_negocio = new G2\Negocio\Venda();
            $result = $this->_negocio->deleteVendaProduto($this->getParam('id'));
            $entity = $this->_negocio->toArrayEntity($result);
            $this->_helper->json($entity);
        }
    }

    public function getAction() {

    }

    public function headAction() {
        
    }

    public function indexAction() {
        
    }

    public function postAction() {
        
    }

    public function putAction() {
        
    }

}
<?php

/**
 * Classe Controller API para Pagamento de Colaborador
 */
class Api_PagamentoColaboradorController extends Ead1_RestRestrita
{

    private $negocio;

    public function init ()
    {
        parent::init();
         $this->negocio = new G2\Negocio\PagamentoColaborador();
    }

    public function deleteAction ()
    {
        
    }

    public function getAction ()
    {
        
    }

    public function headAction ()
    {
        
    }

    public function indexAction ()
    {
        
    }

    public function postAction ()
    {
        
    }

    public function putAction ()
    {
        
    }

}

<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 31/03/2015
 * Time: 14:37
 */
class Api_ItemDeMaterialPresencialController extends Ead1_RestRestrita
{

    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\ItemDeMaterialPresencial();
    }

    public function indexAction()
    {
//        $data = $this->getRequest()->getQuery();
//
//        $mensageiro = $this->negocio->findByEntidade($data);
//
//        $this->getResponse()->appendBody(json_encode($mensageiro));


    }

    public function getAction()
    {
//        $id = (int) $this->getRequest()->getParam('id');
//
//        if ($id) {
//            $negocio = new \G2\Negocio\Negocio();
//
//            $result = $negocio->find('\G2\Entity\ItemDeMaterial', $id);
//            $st_upload = $result->getId_upload()?$result->getId_upload()->getSt_upload():'';
//            $result = $negocio->toArrayEntity($result);
//            $result['st_upload'] = $st_upload;
////            $result['disciplinas'] = array();
//            $result['projetosPedagogicos'] = array();
//
//
//            // Buscar disciplinas associadas
////            $disciplinas = $negocio->findBy('\G2\Entity\ItemDeMaterialDisciplina', array(
////                'id_itemdematerial' => $id
////            ));
////            foreach ($disciplinas as $disciplina) {
////                $result['disciplinas'][] = array('id_disciplina'=>$disciplina->getId_disciplina()->getId_disciplina(),'st_disciplina'=>$disciplina->getId_disciplina()->getSt_disciplina());
////            }
//
////            // Buscar projetos pedagogicos associados
////            $projetos = $negocio->findBy('\G2\Entity\MaterialProjeto', array(
////                'id_itemdematerial' => $id,
////                'id_usuariocadastro' => $negocio->sessao->id_usuario
////            ));
////            foreach ($projetos as $projeto) {
////                $result['projetosPedagogicos'][] = $projeto->getId_projetopedagogico();
////            }
//
//            $this->getResponse()->appendBody(json_encode($result));
//        }


    }

    public function postAction()
    {
        $body = $this->getRequest()->getPost();
        if(!$body){
            $data = $this->getRequest()->getRawBody();
            $body = Zend_Json::decode($data);
        }
//        Zend_Debug::dump($_FILES['attachment']);
//        exit;
        $result = $this->negocio->salvaMaterial($body);
        if ($result) {
            $arrResult = array();

            $arrResult = $this->negocio->toArrayEntity($result['id_itemdematerial']);
            $arrResult['id_turma'] = $result['id_turma'];
            $arrResult['id_disciplina'] = $result['id_disciplina'];
            $arrResult['id_professor'] = $result['id_itemdematerial']->getId_professor()->getId_usuario();
            $arrResult['id_tipo'] = $result['id_itemdematerial']->getId_tipodematerial()->getId_tipodematerial();
            $arrResult['id_situacao'] = $result['id_itemdematerial']->getId_situacao()->getId_situacao();
            $arrResult['st_upload'] = $result['id_itemdematerial']->getId_upload()!=NULL?$this->negocio->retornaNomeUpload($result['id_itemdematerial']->getId_upload()->getId_upload()): null;
            $this->getResponse()->appendBody(json_encode($arrResult));
            $this->getResponse()->setHttpResponseCode(201);
        } else {
            $this->getResponse()->appendBody(json_encode("Erro ao salvar material"));
            $this->getResponse()->setHttpResponseCode(400);
        }

    }

    public function putAction()
    {
    }

    public function deleteAction()
    {
        $id = (int)$this->getRequest()->getParam('id');
        if ($id) {
            $resultado = $this->negocio->deleta($id);
            if ($resultado) {
                $this->getResponse()->setHttpResponseCode(204)->appendBody('Registro removido');
            } else {
                $this->getResponse()->setHttpResponseCode(400)->appendBody('Erro ao tentar remover registro');
            }
        }
    }

    public function headAction()
    {
    }

}
<?php

/**
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2013-13-11
 * @package api
 * @subpackage controller
 */
use G2\Negocio\Pessoa;

class Api_UsuarioController extends Ead1_RestRestrita
{

    public function init ()
    {
        parent::init();
        $this->_negocio = new Pessoa();
    }

    public function indexAction ()
    {
        $this->getResponse()->appendBody(json_encode(array(
            'id' => '',
            'id_usuario' => '',
            'st_login' => '',
            'st_senha' => '',
            'st_nomecompleto' => '',
            'id_registropessoa' => '',
            'st_cpf' => '',
            'bl_ativo' => '',
            'bl_redefinicaosenha' => '',
            'id_usuariopai' => '',
            'st_nomeentidade' => '',
            'id_entidade' => ''
        )));
    }

    public function getAction ()
    {
        $id = $this->getParam('id');

        if ($id) {
            $mensageiro = $this->_negocio->find('G2\Entity\Usuario', $id);
            $mensageiro = $this->_negocio->toArrayEntity($mensageiro);

            $this->_helper->json($mensageiro);
        }
    }

    public function postAction ()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
        $result = $this->_negocio->salvarUsuario($data);
        $arrReturn = array();
        if ($result) {
            foreach ($result as $row) {
                $arrReturn = array(
                    'id' => $row->getId_usuario(),
                    'id_usuario' => $row->getId_usuario(),
                    'st_login' => $row->getSt_login(),
                    'st_senha' => $row->getSt_senha(),
                    'st_nomecompleto' => $row->getSt_nomecompleto(),
                    'id_registropessoa' => $row->getId_registropessoa(),
                    'st_cpf' => $row->getSt_cpf(),
                    'bl_ativo' => $row->getBl_ativo(),
                    'bl_redefinicaosenha' => $row->getBl_redefinicaosenha(),
                    'id_usuariopai' => $row->getId_usuariopai(),
                    'id_usuariopai' => $row->getId_usuariopai(),
                    "st_nomeentidade" => NULL,
                    "id_entidade" => NULL
                );
            }
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    public function putAction ()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
        Zend_Debug::dump($data);
        exit;
    }

    public function deleteAction ()
    {
        
    }

    public function headAction ()
    {
        
    }



}
<?php

/**
 * Description of Tramite
 *
 * @author Rafael Bruno (RBD) <rafael.oliveira@gmail.com>
 */
class Api_TramiteController extends Ead1_RestRestrita {
    

    public function putAction()
    {
        $tramite = new G2\Negocio\Tramite();
        $data = Zend_Json_Decoder::decode($this->getRequest()->getRawBody());
        $result = $tramite->toArrayEntity($tramite->salvar($data));
        
        $this->_helper->json($result);
    }
}

<?php

/**
 * Controller para Venda de Assinatura
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Api_VendaAssinaturaController extends Ead1_RestRestrita
{

    private $_negocio;

    public function init()
    {
        parent::init();
        $this->_negocio = new \G2\Negocio\Venda();
    }

    public function deleteAction()
    {
    }

    public function getAction()
    {
        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);
        $result = $this->_negocio->retornaVendaProdAssinatura($params);
        $arrReturn = array();
        if ($result) {
            foreach ($result as $i => $row) {
                $arrReturn[$i] = $row;
            }
        }
        $this->getResponse()
            ->appendBody(json_encode($arrReturn));
    }

    public function headAction()
    {

    }

    public function indexAction()
    {
        $result = $this->_negocio->retornaVendaProdAssinatura();
        $arrReturn = array();
        if ($result) {
            foreach ($result as $i => $row) {
                $arrReturn[$i] = $row;
            }
        }
        $this->getResponse()
            ->appendBody(json_encode($arrReturn));
    }

    public function postAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
        $venda = $this->_negocio->findVenda($data['id_venda']); //procura a venda
        if ($venda) { //se venda existir
            //seta um array
            $arrData['id'] = $venda->getIdVenda();
            $arrData['id_evolucao'] = $data['id_evolucao'];
            //salva
            if ($this->_negocio->salvarVenda($arrData)) {
                $this->getResponse()->appendBody(json_encode($data));
                $this->getResponse()->setHttpResponseCode(201);
            } else {
                $this->getResponse()->setHttpResponseCode(400)->appendBody('Erro ao tentar cancelar venda!');
            }
        } else {
            $this->getResponse()->setHttpResponseCode(400)->appendBody('Venda não encontrada!');
        }
    }

    public function putAction()
    {

    }

}

<?php

/**
 * Controller para gerenciar local de aula
 * @author Paulo Silva <paulo.silva@unyleya.com.br>
 * @since 2013-08-14
 * @package application
 * @subpackage controller
 */
class Api_LocalAulaController extends Ead1_RestRestrita
{

    private $doctrineContainer;
    private $em;

    public function init()
    {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
    }

    public function indexAction()
    {

        $params = $this->getAllParams();
        unset($params['controller'], $params['module'], $params['action']);
        $params['id_entidade'] = isset($params['id_entidade']) ? $params['id_entidade'] : $this->sessao->id_entidade;
        $locais = $this->em->getRepository('G2\Entity\LocalAula')->findBy($params);
//        $locais = $this->em->getRepository('G2\Entity\LocalAula')->findBy(array('id_entidade' => $this->sessao->id_entidade));

        $arrLocais = array();
        foreach ($locais as $local) {
            $arrLocais[] = array(
                'st_localaula' => $local->getSt_localaula(),
                'id_catraca' => ($local->getId_catraca()) ? $local->getId_catraca()->getId_catraca() : '--',
                'st_codigocatraca' => ($local->getId_catraca()) ? $local->getId_catraca()->getSt_codigocatraca() : '--',
                'id_entidade' => $local->getId_entidade(),
                'nu_maxalunos' => $local->getNu_maxalunos(),
                'id' => $local->getId()
            );
        }
        $this->getResponse()
            ->appendBody(json_encode($arrLocais));
    }

    public function getAction()
    {
        $id = $this->getParam('id');
        $localAula = $this->em->getRepository('G2\Entity\LocalAula')->find($id);
        $obj = array();

        $obj['id'] = $localAula->getId_localaula();
        $obj['st_localaula'] = $localAula->getSt_localaula();
        $obj['dt_cadastro'] = $localAula->getDt_cadastro();
        $obj['nu_maxalunos'] = $localAula->getNu_maxalunos();

        echo json_encode($obj);

    }

    public function postAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);


        $novoLocalAula = new G2\Entity\LocalAula();
        $novoLocalAula->setSt_localaula($data['st_localaula']);
        $novoLocalAula->setId_entidade($this->sessao->id_entidade);
        $novoLocalAula->setNu_maxalunos($data['nu_maxalunos']);


        if (array_key_exists('id_catraca', $data) && !empty($data['id_catraca'])) {
            $catracaEntity = $this->em->getReference('G2\Entity\Catraca', $data['id_catraca']);
            $novoLocalAula->setId_catraca($catracaEntity);
        }

        $date = new DateTime();

        $novoLocalAula->setDt_cadastro($date);
        $novoLocalAula->setId_situacao(60);
        $novoLocalAula->setId_usuariocadastro(1);

        $this->em->persist($novoLocalAula);
        $this->em->flush();

        $this->getResponse()->appendBody(json_encode(array('id' => $novoLocalAula->getId())));
        $this->getResponse()->setHttpResponseCode(201);
    }

    public function putAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);


        $la = $this->em->getRepository('G2\Entity\LocalAula')->findOneBy(array('id_localaula' => $this->getParam('id')));
        $la->setId_entidade($this->sessao->id_entidade);

        $date = new DateTime();

        $la->setSt_localaula($data['st_localaula']);
        $la->setNu_maxalunos($data['nu_maxalunos']);
        $la->setDt_cadastro($date);
        $la->setId_situacao(60);
        $la->setId_usuariocadastro(1);


        if (array_key_exists('id_catraca', $data) && !empty($data['id_catraca'])) {
            $catracaEntity = $this->em->getReference('G2\Entity\Catraca', $data['id_catraca']);
            $la->setId_catraca($catracaEntity);
        } else {
            $la->setId_catraca(NULL);
        }

        $this->em->persist($la);
        $this->em->flush();

        $this->getResponse()->appendBody(json_encode(array('id' => $la->getId())));
        $this->getResponse()->setHttpResponseCode(201);
    }

    public function deleteAction()
    {
        $id = intval($this->getParam('id'));

        if (!is_int($id)) {
            $this->getResponse()->setHttpResponseCode(400);
            $this->getResponse()->setBody('Não posso apagar um registro com ID inválido!');
        } else {

            $localAula = $this->em->getRepository('G2\Entity\LocalAula')->findOneBy(array('id_localaula' => $id));

            $this->em->remove($localAula);
            $this->em->flush();

            $this->getResponse()->setHttpResponseCode(204)->appendBody('Registro ' . $localAula->getSt_localaula() . ' removido');
            //->appendBody($id);

        }


    }

    public function headAction()
    {

    }


}
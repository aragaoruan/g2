<?php

/**
 * Controller para gerenciar os UsuarioPerfilEntidadeReferencia
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @author Caio Eduardio <caio.teixeira@unyleya.com.br>
 * @since 2014-01-06
 * @update 2015-01-05
 * @package application
 * @subpackage controller
 */
class Api_UsuarioPerfilEntidadeReferenciaController extends Ead1_RestRestrita
{

    private $doctrineContainer;
    private $em;

    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['module'], $params['controller'], $params['action']);

        $negocio = new \G2\Negocio\UsuarioPerfilEntidadeReferencia();

        $mensageiro = $negocio->findAllBy($params);
        $this->_helper->json($mensageiro);
    }

    public function getAction()
    {

        $to = new \UsuarioPerfilEntidadeReferenciaTO();
        $to->setId_perfilReferencia($this->_getParam('id'));
        $to->fetch(true, true, true);
        $this->_helper->json($to->toArray());

    }

    public function postAction()
    {
        $data = Zend_Json::decode($this->getRequest()->getRawBody());
        $marca = false;

        $ro = new ProjetoPedagogicoBO();
        $to = new UsuarioPerfilEntidadeReferenciaTO();
        $to->montaToDinamico($data);
        $to->setBl_titular($to->getBl_titular() ? 1 : 0);
        if($to->getBl_titular()){
            $marca = true;
        }
        $mensageiro = $ro->salvarCoordenadorUnico($to);
        if($mensageiro->getTipo()==Ead1_IMensageiro::SUCESSO){

            if($marca){
                if($mensageiro->getMensagem()){
                    $mensagem = current($mensageiro->getMensagem());
                    $id_perfilreferencia = $mensagem->getId_perfilReferencia();

                    if ($id_perfilreferencia) {
                        $to->setId_perfilreferencia($id_perfilreferencia);
                    }
                }
                $to->setBl_titular(1);
                $ro->setarCoordenador($to);
                $to->fetch(true, true, true);
            }

            $this->getResponse()->setHttpResponseCode(201);
            $this->_helper->json($to->toArray());
        } else {
            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json($mensageiro);
        }
    }

    public function putAction()
    {
        $data = Zend_Json::decode($this->getRequest()->getRawBody());
        $marca = false;

        $ro = new ProjetoPedagogicoBO();
        $to = new UsuarioPerfilEntidadeReferenciaTO();
        $to->montaToDinamico($data);
        if($to->getBl_titular()){
            $marca = true;
        }
        $mensageiro = $ro->salvarCoordenadorUnico($to);
        if($mensageiro->getTipo()==Ead1_IMensageiro::SUCESSO){

            if($mensageiro->getMensagem()){
                $mensagem = $mensageiro->getMensagem();
                if(isset($mensagem['id_perfilreferencia'])){
                    $to->setId_perfilreferencia($mensagem['id_perfilreferencia']);
                }
            }

            if($marca){
                $to->setBl_titular(1);
                $ro->setarCoordenador($to);
                $to->fetch(true, true, true);
            }

            $this->getResponse()->setHttpResponseCode(201);
            $this->_helper->json($to->toArray());
        } else {
            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json($mensageiro->getText());
        }
    }

    public function deleteAction()
    {

        $id_perfilreferencia = $this->_getParam('id', null);
        if ($id_perfilreferencia) {
            $bo = new ProjetoPedagogicoBO();

            $to = new UsuarioPerfilEntidadeReferenciaTO();
            $to->setId_perfilReferencia($id_perfilreferencia);
            $to->fetch(true, true, true);

            $mensageiro = $bo->excluirCoordenador($to);
        } else {
            $mensageiro = new Ead1_Mensageiro("Id não encontrado.", Ead1_IMensageiro::AVISO);
        }
        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->setHttpResponseCode(201);
            $this->_helper->json($to->toArray());
        } else {
            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json($mensageiro);
        }

    }

    public function headAction()
    {

    }


}

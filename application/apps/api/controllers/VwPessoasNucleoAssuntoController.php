<?php

/**
 * Controller para gerenciar VwPessoaNucleoAssunto
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2014-12-19
 * @package application
 * @subpackage controller
 */

class Api_VwPessoasNucleoAssuntoController extends Ead1_RestRestrita {

    private $bo;
    private $negocio;

    public function init ()
    {
        parent::init();
        $this->bo = new NucleoCoBO();
        $this->negocio = new \G2\Negocio\Negocio();
    }

    public function indexAction()
    {
        $data = $this->getAllParams();

        $to = new VwPessoasNucleoAssuntoTO();
        foreach ($data as $key => $value) {
            if (array_key_exists($key, $to)) {
                $to->{$key} = $value;
            }
        }

        $mensageiro = $this->bo->retornaPessoasNucleoAssunto($to);
        $this->_helper->json($mensageiro);
    }

    public function getAction()
    {
        // O METODO GET RETORNA AS FUNCOES DA PESSOA NO NUCLEO // VwAssuntoNucleoPessoaTO
        $data = $this->getAllParams();

        $data['id_usuario'] = $this->getParam('id');

        $to = new VwAssuntoNucleoPessoaTO();
        foreach ($data as $key => $value) {
            if (array_key_exists($key, $to)) {
                $to->{$key} = $value;
            }
        }

        $mensageiro = $this->bo->retornaFuncoesPessoaNucleo($to);
        $this->_helper->json($mensageiro);
    }

    public function postAction()
    {
        $data = Zend_Json::decode($this->getRequest()->getRawBody());

        /* Verifica as fun��es da pessoa. Caso todos os assuntos de uma fun��o seja atribuido,
        busca no banco cadastros individuais pr�vios e os remove (evitando duplica��es). */

        $quantidadeAlterados = count($data['changed_items']);

        for ($i=0; $i<$quantidadeAlterados; $i++){
            if($data['changed_items'][$i]['bl_todos'] == 1){

            $toRemove = $this->negocio->findBy('G2\Entity\NucleoPessoaCo', array('id_nucleoco' => $data['id_nucleoco'],
                'id_usuario' => $data['id_usuario'], 'id_funcao' => $data['changed_items'][$i]['id_funcao']));

            foreach($toRemove as $itemToRemove) {
                $this->negocio->delete($itemToRemove);
                }
            }
        }

        //Prossegue normalmente, salvando as altera��es.

        if (isset($data['changed_items'])) {
            $arrTO = array();

            foreach ($data['changed_items'] as $item) {
                $to = new NucleoPessoaCoTO();
                // SETAR OS ATRIBUTOS
                foreach ($item as $key => $value) {
                    if (array_key_exists($key, $to)) {
                        if (substr($key, 0, 3) == 'st_')
                            $value = utf8_decode($value);
                        $to->{$key} = $value;
                    }
                }
                if (!$to->getId_usuariocadastro())
                    $to->setId_usuariocadastro($this->negocio->sessao->id_usuario);

                $arrTO[] = $to;
            }

            $mensageiro = $this->bo->salvarArrayFuncoes($arrTO);
            $this->_helper->json($mensageiro);
        }

    }

    public function putAction()
    {
        $this->postAction();
    }

    public function deleteAction()
    {
        $id = $this->getParam('id');
        if ($id) {
            $to = new EntityTO();
            $to->setId_entity($id);

            $mensageiro = $this->bo->excluirEntity($to);
            $this->_helper->json($mensageiro);
        }
    }

    public function headAction()
    {
    }
}
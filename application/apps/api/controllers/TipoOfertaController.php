<?php

/**
 * Controller para listar os tipos de ofertas
 * @author Rafael Leite <rafeal.leite@unyleya.com.br>
 * @since 2017-03-02
 * @package application
 * @subpackage controller
 */
class Api_TipoOfertaController extends Ead1_RestRestrita
{

    /**
     * @var \G2\Negocio\Negocio $negocio
     */
    private $negocio;
    private $repositoryName = '\G2\Entity\TipoOferta';

    public function init ()
    {
        parent::init();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->negocio = new \G2\Negocio\Negocio();
    }

    public function indexAction ()
    {
        $result = $this->negocio->findAll($this->repositoryName);

        $retorno = array();

        foreach($result as $key=>$value){
            $retorno[$key]['id_tipooferta'] = $value->getId_tipooferta();
            $retorno[$key]['st_tipooferta'] = $value->getSt_tipooferta();
        }

        $this->_helper->json($retorno);
    }

    public function getAction ()
    {
    }

    public function postAction ()
    {
    }

    public function putAction ()
    {
    }

    public function deleteAction ()
    {
    }

    public function headAction ()
    {
    }

}
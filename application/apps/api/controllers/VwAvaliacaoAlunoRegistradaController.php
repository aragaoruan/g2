<?php

/**
 * Description of VwAvaliacaoAlunoRegistradaController
 *
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class Api_VwAvaliacaoAlunoRegistradaController extends Ead1_RestRestrita {

    private $_negocio;
    
    public function init() {
        parent::init();
        $this->_negocio = new \G2\Negocio\Avaliacao();
    }

    /**
     * Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     * 
     * Action responsavel por requisitar negocio e retornar json de avaliacoes
     * registradas.
     * 
     * @param $params, $order Ex: id_usuario=ASC
     * @return Zend_Helper::Json
     */
    public function indexAction() {
        
        $params = $this->getRequest()->getParams();
        $order = array();
        
        //Verficia e aplica parametro para ordenar resultado da busca
        if ($this->getRequest()->getParam('order')){
            foreach ($this->getRequest()->getParam('order') as $value) {
                $exp = explode('=', $value);
                $order[$exp[0]] = $exp[1];
            }
        }
        
        unset($params['module'], $params['action'], $params['controller'], $params['order']);
        
        $result = $this->_negocio->findVwAvaliacaoAlunoRegistrada($params, $order);
        $array = array();
        
        foreach($result as $key => $value){
            $array[$key] = $this->_negocio->toArrayEntity($value);
        }
        
        $this->_helper->json($array);
    }

    public function deleteAction() {
        
    }

    public function getAction() {
        
    }

    public function headAction() {
        
    }

    public function postAction() {
        
    }

    public function putAction() {
        
    }

}

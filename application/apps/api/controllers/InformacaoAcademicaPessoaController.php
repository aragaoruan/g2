<?php

/**
 * Class Api_InformacaoAcademicaPessoaController
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Api_InformacaoAcademicaPessoaController extends Ead1_RestRestrita
{

    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Pessoa();
    }

    /**
     * The index action handles index/list requests; it should respond with a
     * list of the requested resources.
     */
    public function indexAction()
    {
        $result = $this->negocio->retornarInformacaoAcademicaPessoa();
        $arReturn = array();
        if ($result) {
            foreach ($result as $i => $row) {
                $arReturn[] = array(
                    'id_informacaoacademicapessoa' => $row->getId_informacaoacademicapessoa(),
                    'id_usuario' => $row->getId_usuario() ? $row->getId_usuario()->getId_usuario() : NULL,
                    'id_entidade' => $row->getId_entidade() ? $row->getId_entidade()->getId_entidade() : NULL,
                    'id_nivelensino' => $row->getId_nivelensino() ? $row->getId_nivelensino()->getId_nivelensino() : NULL,
                    'st_curso' => $row->getSt_curso(),
                    'st_nomeinstituicao' => $row->getSt_nomeinstituicao(),
                );
            }
        }
        $this->getResponse()->appendBody(json_encode($arReturn));
    }

    /**
     * The get action handles GET requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function getAction()
    {
        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);
        $result = $this->negocio->retornarInformacaoAcademicaPessoa($params);
        $arReturn = array();
        if ($result) {
            foreach ($result as $i => $row) {
                $arReturn[] = array(
                    'id_informacaoacademicapessoa' => $row->getId_informacaoacademicapessoa(),
                    'id_usuario' => $row->getId_usuario() ? $row->getId_usuario()->getId_usuario() : NULL,
                    'id_entidade' => $row->getId_entidade() ? $row->getId_entidade()->getId_entidade() : NULL,
                    'id_nivelensino' => $row->getId_nivelensino() ? $row->getId_nivelensino()->getId_nivelensino() : NULL,
                    'st_curso' => $row->getSt_curso(),
                    'st_nomeinstituicao' => $row->getSt_nomeinstituicao(),
                );
            }
        }
        $this->getResponse()->appendBody(json_encode($arReturn));
    }

    /**
     * The head action handles HEAD requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function headAction()
    {
        // TODO: Implement headAction() method.
    }

    /**
     * The post action handles POST requests; it should accept and digest a
     * POSTed resource representation and persist the resource state.
     */
    public function postAction()
    {
        // TODO: Implement postAction() method.
    }

    /**
     * The put action handles PUT requests and receives an 'id' parameter; it
     * should update the server resource state of the resource identified by
     * the 'id' value.
     */
    public function putAction()
    {
        // TODO: Implement putAction() method.
    }

    /**
     * The delete action handles DELETE requests and receives an 'id'
     * parameter; it should update the server resource state of the resource
     * identified by the 'id' value.
     */
    public function deleteAction()
    {
        // TODO: Implement deleteAction() method.
    }
}
<?php

/**
 * Controller para gerenciar local de aula
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 * @since 2013-10-14
 * @package application
 * @subpackage controller
 */
class Api_PrimeiroAcessoController extends Ead1_RestRestrita {

    private $negocio;
    private $form;

    public function init() {
        parent::init();
        $this->negocio = new \G2\Negocio\PrimeiroAcesso();
        $this->form = new PABoasVindasForm();
    }

    public function indexAction() {
        
    }

    public function deleteAction() {
        
    }

    public function getAction() {
        
    }

    public function headAction() {
        
    }

    public function postAction() {
        
    }

    public function putAction() {
        
    }

}

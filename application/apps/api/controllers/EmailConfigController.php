<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EmailConfigController
 *
 * @author kayo.silva
 */
class Api_EmailConfigController extends Ead1_RestRestrita
{

    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\EmailConfig();
    }

    /**
     * The index action handles index/list requests; it should respond with a
     * list of the requested resources.
     */
    public function indexAction()
    {
        $dados = $this->negocio->findByEntidade();
        $result = array();
        $senha_generica = "Doctor Who?";
        foreach ($dados as $i => $dad) {
            $result[$i] = $this->negocio->toArrayEntity($dad);
            $result[$i]['id_tipoconexaoemailsaida'] = $result[$i]['id_tipoconexaoemailsaida']['id_tipoconexaoemail'];
            $result[$i]['id_tipoconexaoemailentrada'] = $result[$i]['id_tipoconexaoemailentrada']['id_tipoconexaoemail'];
            $result[$i]['id'] = $dad->getId_emailconfig();
            //alteracao para evitar que a senha seja requisitada e fique insegura
            $result[$i]['st_senha'] = $dad->getSt_senha();
        }

        $this->getResponse()
            ->appendBody(json_encode($result));

    }

    /**
     * The get action handles GET requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function getAction()
    {
        // TODO: Implement getAction() method.
    }

    /**
     * The head action handles HEAD requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function headAction()
    {
        // TODO: Implement headAction() method.
    }

    /**
     * The post action handles POST requests; it should accept and digest a
     * POSTed resource representation and persist the resource state.
     */
    public function postAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
        $result = $this->negocio->salva($data);
        $array = $this->negocio->toArrayEntity($result);
        $this->_helper->json($array);
    }

    /**
     * The put action handles PUT requests and receives an 'id' parameter; it
     * should update the server resource state of the resource identified by
     * the 'id' value.
     */
    public function putAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
        $result = $this->negocio->salva($data);
        $array = $this->negocio->toArrayEntity($result);
        $this->_helper->json($array);
    }

    /**
     * The delete action handles DELETE requests and receives an 'id'
     * parameter; it should update the server resource state of the resource
     * identified by the 'id' value.
     */
    public function deleteAction()
    {
        $id = intval($this->getParam('id'));
        if (!is_int($id)) {
            $this->getResponse()->setHttpResponseCode(400)->setBody('Não posso apagar um registro com ID inválido!');
        } else {
            $array = array(
                'id_emailconfig' => $id,
                'bl_ativo' => false
            );
            $result = $this->negocio->salva($array);
            if ($result) {
                $this->getResponse()->setHttpResponseCode(204)->appendBody('Registro removido');
            } else {
                $this->getResponse()->setHttpResponseCode(400)->appendBody('Erro ao tentar remover registro');
            }
        }
    }
}
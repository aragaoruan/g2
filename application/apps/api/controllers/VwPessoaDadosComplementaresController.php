<?php

/**
 * Controller para gerenciar VwPessoaDadosComplementares
 * @author Marcus Pereira <marcus.pereira@unyleya.com.br>
 * @since 2015-10-28
 * @package application
 * @subpackage controller
 */

class Api_VwPessoaDadosComplementaresController extends Ead1_ControllerRestrita {
    /**
     * @var \G2\Negocio\Negorio $negocio
     */
    private $negocio;
    private $repositoryName = '\G2\Entity\VwPessoaDadosComplementares';

    public function init ()
    {
        parent::init();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->negocio = new \G2\Negocio\Negocio();
    }

//    public function indexAction()
//    {
//        $params = $this->getAllParams();
//        $orderBy = $this->getParam('order_by') ? array($this->getParam('order_by') => $this->getParam('sort') ? $this->getParam('sort') : 'ASC') : null;
//        unset($params['module'], $params['controller'], $params['action'], $params['order_by'], $params['sort'], $params['search'], $params['search']);
//        $paramsLike = $this->getParam('search');
//
//        $mensageiro = $this->negocio->findBySearch($this->repositoryName, $paramsLike, $params, $orderBy);
//        foreach ($mensageiro as $key => &$entity) {
//            $entity = $entity->toArray();
//        }
//
//        $this->_helper->json($mensageiro);
//    }

    public function getAction()
    {
        //Faz a busca baseando no id_usuario e no id_entidade.
        $where = array('id_usuario' => $this->getParam('id'), 'id_entidade' => $this->getParam('id_entidade'));
        $entity = $this->negocio->findOneBy($this->repositoryName, $where);
        if ($entity)
            $this->_helper->json($this->negocio->toArrayEntity($entity));
        else $this->getResponse()->setHttpResponseCode(400);
    }
}
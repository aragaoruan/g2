<?php

/**
 * Controller modolo API para gerenciar categoria
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-03-11
 */
class Api_CampanhaComercialController extends Ead1_RestRestrita
{

    /**
     * @var \G2\Negocio\CampanhaComercial $negocio
     */
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\CampanhaComercial();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['module'], $params['controller'], $params['action']);

        $results = $this->negocio->retornarCampanhas($params, true);

        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->appendBody(json_encode($results));
    }

    public function getAction()
    {
        $id = $this->_getParam('id');
        $result = $this->negocio->findCampanha($id);
        $arrResult = array();
        if ($result) {
            $arrResult = array(
                'id' => $result->getId_campanhacomercial(),
                'id_campanhacomercial' => $result->getId_campanhacomercial(),
                'st_campanhacomercial' => $result->getSt_campanhacomercial(),
                'bl_aplicardesconto' => $result->getBl_aplicardesconto(),
                'bl_ativo' => $result->getBl_ativo(),
                'bl_disponibilidade' => $result->getBl_disponibilidade(),
                'dt_cadastro' => $result->getDt_cadastro(),
                'dt_fim' => $result->getDt_fim() instanceof DateTime
                    ? $result->getDt_fim()->format('d/m/Y') : $result->getDt_fim(),
                'dt_inicio' => $result->getDt_inicio() instanceof DateTime
                    ? $result->getDt_inicio()->format('d/m/Y') : $result->getDt_inicio(),
                'nu_disponibilidade' => $result->getNu_disponibilidade(),
                'st_descricao' => $result->getSt_descricao(),
                'bl_todosprodutos' => $result->getBl_todosprodutos(),
                'bl_todasformas' => $result->getBl_todasformas(),
                'id_entidade' => $result->getId_entidade() ? $result->getId_entidade()->getId_entidade() : NULL,
                'id_situacao' => $result->getId_situacao() ? $result->getId_situacao()->getId_situacao() : NULL,
                'id_tipodesconto' => $result->getId_tipodesconto()
                    ? $result->getId_tipodesconto()->getId_tipodesconto() : NULL,
                'id_usuariocadastro' => $result->getId_usuariocadastro()
                    ? $result->getId_usuariocadastro()->getId_usuario() : NULL,
                'id_tipocampanha' => $result->getId_tipocampanha()
                    ? $result->getId_tipocampanha()->getId_tipocampanha() : NULL,
                'nu_valordesconto' => $result->getNu_valordesconto(),
                'id_categoriacampanha' => $result->getId_categoriacampanha()
                    ? $result->getId_categoriacampanha()->getId_categoriacampanha() : NULL,
                'nu_diasprazo' => $result->getNu_diasprazo(),
                'id_finalidadecampanha' => $result->getId_finalidadecampanha() instanceof \G2\Entity\FinalidadeCampanha
                    ? $result->getId_finalidadecampanha()->getId_finalidadecampanha() : NULL,
                'st_link' => $result->getSt_link() ? $result->getSt_link() : NULL
            );
        }
        $this->getResponse()
            ->appendBody(json_encode($arrResult));
    }

    public function postAction()
    {
        $data = $this->getRequest()->getParams();
        if (!empty($_FILES)) {
            $data['imagem'] = $_FILES;
        }
        $result = $this->negocio->salvarCampanha($data);

        if ($result instanceof \G2\Entity\CampanhaComercial) {
            $arrReturn = array(
                'id' => $result->getId_campanhacomercial(),
                'id_campanhacomercial' => $result->getId_campanhacomercial(),
                'st_campanhacomercial' => $result->getSt_campanhacomercial(),
                'bl_aplicardesconto' => $result->getBl_aplicardesconto(),
                'bl_ativo' => $result->getBl_ativo(),
                'bl_disponibilidade' => $result->getBl_disponibilidade(),
                'dt_cadastro' => $result->getDt_cadastro(),
                'dt_fim' => $result->getDt_fim() ? $result->getDt_fim()->format('d/m/Y') : NULL,
                'dt_inicio' => $result->getDt_inicio() ? $result->getDt_inicio()->format('d/m/Y') : NULL,
                'nu_disponibilidade' => $result->getNu_disponibilidade(),
                'st_descricao' => $result->getSt_descricao(),
                'bl_todosprodutos' => $result->getBl_todosprodutos(),
                'bl_todasformas' => $result->getBl_todasformas(),
                'id_entidade' => $result->getId_entidade() ? $result->getId_entidade()->getId_entidade() : NULL,
                'id_situacao' => $result->getId_situacao() ? $result->getId_situacao()->getId_situacao() : NULL,
                'id_tipodesconto' => $result->getId_tipodesconto()
                    ? $result->getId_tipodesconto()->getId_tipodesconto() : NULL,
                'id_usuariocadastro' => $result->getId_usuariocadastro()
                    ? $result->getId_usuariocadastro()->getId_usuario() : NULL,
                'id_tipocampanha' => $result->getId_tipocampanha()
                    ? $result->getId_tipocampanha()->getId_tipocampanha() : NULL,
                'nu_valordesconto' => $result->getNu_valordesconto(),
                'id_categoriacampanha' => $result->getId_categoriacampanha()
                    ? $result->getId_categoriacampanha()->getId_categoriacampanha() : NULL,
                'nu_diasprazo' => $result->getNu_diasprazo(),
                'id_finalidadecampanha' => $result->getId_finalidadecampanha()
                    ->getId_finalidadecampanha() instanceof \G2\Entity\FinalidadeCampanha
                    ? $result->getId_finalidadecampanha()->getId_finalidadecampanha() : NULL,
                'st_link' => $result->getSt_link() ? $result->getSt_link() : NULL
            );
            $this->getResponse()->appendBody(json_encode($arrReturn));
            $this->getResponse()->setHttpResponseCode(201);
        } else {
            $this->getResponse()->appendBody(json_encode($result));
            $this->getResponse()->setHttpResponseCode(400);
        }
    }

    public function putAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
        $result = $this->negocio->salvarCampanha($data);
        if ($result instanceof \G2\Entity\CampanhaComercial) {
            $arrReturn = array(
                'id' => $result->getId_campanhacomercial(),
                'id_campanhacomercial' => $result->getId_campanhacomercial(),
                'st_campanhacomercial' => $result->getSt_campanhacomercial(),
                'bl_aplicardesconto' => $result->getBl_aplicardesconto(),
                'bl_ativo' => $result->getBl_ativo(),
                'bl_disponibilidade' => $result->getBl_disponibilidade(),
                'dt_cadastro' => $result->getDt_cadastro(),
                'dt_fim' => $result->getDt_fim() ? $result->getDt_fim()->format('d/m/Y') : NULL,
                'dt_inicio' => $result->getDt_inicio() ? $result->getDt_inicio()->format('d/m/Y') : NULL,
                'nu_disponibilidade' => $result->getNu_disponibilidade(),
                'st_descricao' => $result->getSt_descricao(),
                'bl_todosprodutos' => $result->getBl_todosprodutos(),
                'bl_todasformas' => $result->getBl_todasformas(),
                'id_entidade' => $result->getId_entidade() ? $result->getId_entidade()->getId_entidade() : NULL,
                'id_situacao' => $result->getId_situacao() ? $result->getId_situacao()->getId_situacao() : NULL,
                'id_tipodesconto' => $result->getId_tipodesconto()
                    ? $result->getId_tipodesconto()->getId_tipodesconto() : NULL,
                'id_usuariocadastro' => $result->getId_usuariocadastro()
                    ? $result->getId_usuariocadastro()->getId_usuario() : NULL,
                'id_tipocampanha' => $result->getId_tipocampanha()
                    ? $result->getId_tipocampanha()->getId_tipocampanha() : NULL,
                'nu_valordesconto' => $result->getNu_valordesconto(),
                'id_categoriacampanha' => $result->getId_categoriacampanha()
                    ? $result->getId_categoriacampanha()->getId_categoriacampanha() : NULL,
                'nu_diasprazo' => $result->getNu_diasprazo()
            );
            $this->getResponse()->appendBody(json_encode($arrReturn));
            $this->getResponse()->setHttpResponseCode(201);
        } else {
            $this->getResponse()->appendBody(json_encode($result));
            $this->getResponse()->setHttpResponseCode(400);
        }
    }

    public function deleteAction()
    {
        $id = intval($this->getParam('id'));
        if (!is_int($id)) {
            $this->getResponse()->setHttpResponseCode(400)
                ->setBody('Não posso apagar um registro com ID inválido!');
        } else {
            $result = $this->negocio->deletarCampanha($id);

            if ($result) {
                $this->getResponse()->setHttpResponseCode(204)->appendBody('Registro removido');
            } else {
                $this->getResponse()->setHttpResponseCode(400)->appendBody('Erro ao tentar remover registro');
            }
        }
    }

}

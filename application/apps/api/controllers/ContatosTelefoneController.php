<?php

/**
 * Controller para Endereco
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @package api
 * @subpackage controller
 * @since 2013-11-20
 */
use G2\Negocio\ContatosTelefone;

class Api_ContatosTelefoneController extends Ead1_RestRestrita
{

    private $_negocio;

    public function init ()
    {
        parent::init();
        $this->_negocio = new ContatosTelefone();
    }

    public function deleteAction ()
    {
        
    }

    public function getAction ()
    {
        
    }

    public function headAction ()
    {
        
    }

    public function indexAction ()
    {
        
    }

    public function postAction ()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
    }

    public function putAction ()
    {
        
    }

}

<?php

/**
 * Controller para retornar as Turmas de um Projeto Pedagógico
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @since 2014-09-24
 * @package application
 * @subpackage controller
 */
class Api_TurmasProjetoController extends Ead1_RestRestrita
{

    private $doctrineContainer;
    private $em;
    private $negocio;

    public function init()
    {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
        $this->negocio = new G2\Negocio\ProdutoProjetoPedagogico();
    }

    public function indexAction()
    {

        $id_projetopedagogico = $this->getParam('id_projetopedagogico', false);

        $retorno = $this->negocio->retornaTurmaPorProjetoEntidade($id_projetopedagogico);

        $this->_helper->json($retorno);
    }

    public function getAction()
    {
    }

    public function postAction()
    {
    }

    public function putAction()
    {
    }

    public function deleteAction()
    {
    }

    public function headAction()
    {
    }


}
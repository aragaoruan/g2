<?php

/**
 * @package application
 * @subpackage controller
 */
class Api_VwProfessorController extends Ead1_RestRestrita {

    private $doctrineContainer;
    private $em;
    private $bo;

    public function init() {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
        $this->bo = new G2\Negocio\VwProfessor();
    }

    public function indexAction() {

        $mensageiro = $this->bo->findByEntidade();

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $mensageiro->setText(count($mensageiro->getMensagem()) . ' registros encontrados!');
        }

        $this->getResponse()
                ->appendBody(json_encode($mensageiro->getMensagem()));
    }

    public function getAction() {

        $vw = $this->bo->findOneBy('G2\Entity\VwProfessor', array('id_usuario' => $this->_getParam('id', null)));

        if ($vw->getId_usuario()) {
            
        }

        $this->getResponse()->appendBody(json_encode($this->bo->toArrayEntity($vw)));
    }

    public function postAction() {
        
    }

    public function putAction() {
        
    }

    public function deleteAction() {
        
    }

    public function headAction() {
        
    }

}

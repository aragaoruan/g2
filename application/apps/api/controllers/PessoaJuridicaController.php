<?php

/**
 * Controller para Pessoa Jurídica
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 * @package default
 * @subpackage controller
 * @since 2013-12-09
 */
use G2\Negocio\Entidade;

class Api_PessoaJuridicaController extends Ead1_RestRestrita
{

    private $_negocio;

    public function init()
    {
        parent::init();
        $this->_negocio = new Entidade();
    }

    public function indexAction()
    {

    }

    public function deleteAction()
    {

    }

    /**
     * Metodo Get API, retorna Entidade
     * @param GET 'str'
     */
    public function getAction()
    {
        //trata o parametro da string
        $string = null;
        if ($this->getParam('str')) {
            $string = $this->getParam('str');
        }
        //busca seta os parametros para consultar
        $return = $this->_negocio->retornaPesquisarEntidade(array(
            'st_razaosocial' => $string,
            'st_cnpj' => $string
        ));
        //trata o retorno
        $arrReturn = array();
        if ($return) {
            foreach ($return as $row) {
                $arrReturn[] = array(
                    'id' => $row->getId_entidade(),
                    'id_entidade' => $row->getId_entidade(),
                    'st_nomeentidade' => $row->getSt_nomeentidade(),
                    'st_siglaentidade' => $row->getSt_siglaentidade(),
                    'st_urlimglogo' => $row->getSt_urlimglogo(),
                    'bl_acessasistema' => $row->getBl_acessasistema(),
                    'nu_cnpj' => $row->getNu_cnpj(),
                    'bl_ativo' => $row->getbl_ativo(),
                    'id_situacao' => $row->getId_situacao(),
                    'st_razaosocial' => $row->getSt_razaosocial(),
                    'st_cnpj' => $row->getSt_cnpj()
                );
            }
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
    }

    public function headAction()
    {

    }

    public function postAction()
    {

    }

    public function putAction()
    {

    }

}
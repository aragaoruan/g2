<?php

/**
 * Controller para gerenciar as NucleoCo
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2014-12-15
 * @package application
 * @subpackage controller
 */
class Api_NucleoCoController extends Ead1_RestRestrita
{

    /**
     * @var \G2\Negocio\Negocio $negocio
     */
    private $negocio;

    /**
     * @var \NucleoCoBO $bo
     */
    private $bo;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Negocio();
        $this->bo = new \NucleoCoBO();
    }

    public function indexAction()
    {
        $to = new VwNucleoCoTO();
        $params = $this->getAllParams();
        if ($params) {
            $to->montaToDinamico($params);
        }

        if (is_array($to->id_assuntocancelamento)) {

            $this->getResponse()->setHttpResponseCode(400);

            $this->_helper->json(new Ead1_Mensageiro('id_assuntocancelamento não pode ser um array',
                Ead1_IMensageiro::AVISO));

            \G2\Utils\LogSistema::enviarLogNewRelic(json_encode($to->id_assuntocancelamento));
        }

        $mensageiro = $this->bo->retornaVwNucleo($to);
        $this->_helper->json($mensageiro);
    }

    public function getAction()
    {
        $id = $this->getParam('id');

        if ($id) {
            $to = new NucleoCoTO();
            $to->setId_nucleoco($id);

            $mensageiro = $this->bo->listarNucleoCo($to);
            if ($mensageiro->type == 'success' && $mensageiro->mensagem) {
                $mensageiro = array_shift($mensageiro->mensagem);

                // Criar arary de evlucoes relacionadas
                $mensageiro->evolucoes = array();
                $eto = new NucleoEvolucaoCoTO();
                $eto->setId_nucleoco($to->getId_nucleoco());

                $evolucoes = $this->bo->retornaNucleoEvolucaoCo($eto);

                if ($evolucoes->type == 'success' && $evolucoes->mensagem) {
                    foreach ($evolucoes->mensagem as $evolucao) {
                        /** @var \NucleoEvolucaoCoTO $evolucao */
                        $mensageiro->evolucoes[$evolucao->getId_evolucao()][] = $evolucao->getId_funcao();
                    }
                }

                // Criar array de assuntos relacionados
                $mensageiro->assuntos = array();
                $ato = new VwNucleoAssuntoCoTO();
                $ato->setId_tipoocorrencia($mensageiro->getId_tipoocorrencia());
                $ato->setId_nucleoco($to->getId_nucleoco());
                $assuntos = $this->bo->retornaVwNucleoAssunto($ato);
                if ($assuntos->type == 'success' && $assuntos->mensagem) {
                    $mensageiro->assuntos = $assuntos->mensagem;
                }

            }
            $this->_helper->json($mensageiro);
        }
    }

    public function postAction()
    {
        $data = Zend_Json::decode($this->getRequest()->getRawBody());

        $to = new NucleoCoTO();
        $to->montaToDinamico($data);

        // Campos obrigatorios generalizados
        if (!$to->getId_entidade()) {
            $to->setId_entidade($this->negocio->sessao->id_entidade);
        }

        if (!$to->getId_usuariocadastro()) {
            $to->setId_usuariocadastro($this->negocio->sessao->id_usuario);
        }

        if (isset($data['assuntos_adicionados']) && $data['assuntos_adicionados']) {
            // Apenas adicionar novos assuntos ao Nucleo Selecionado
            $mensageiro = $this->bo->salvarAssuntosNucleo($data['assuntos_adicionados'], $to);
        } else {
            // Salvar o nucleo por completo
            $mensageiro = $this->bo->salvarNucleo($to);

            if ($mensageiro->type == 'success' && $mensageiro->mensagem) {
                $mensageiro->mensagem = array_shift($mensageiro->mensagem);

                // Salvar as evolucoes
                if (isset($data['evolucoes']) && $data['evolucoes']) {
                    $arrNucleosEvolucao = array();
                    foreach ($data['evolucoes'] as $idEvolucao => $arrIdFuncao) {
                        foreach ($arrIdFuncao as $idFuncao) {
                            $neto = new NucleoEvolucaoCoTO();
                            $neto->setId_nucleoco($mensageiro->mensagem->id_nucleoco);
                            $neto->setId_evolucao($idEvolucao);
                            $neto->setId_funcao($idFuncao);
                            $arrNucleosEvolucao[$idEvolucao][] = $neto;
                        }
                    }
                    if ($arrNucleosEvolucao)
                        $this->bo->salvarNucleoEvolucaoCo(array_values($arrNucleosEvolucao));
                }
            }
        }

        $this->_helper->json($mensageiro);
    }

    public function putAction()
    {
        $this->postAction();
    }

    public function deleteAction()
    {
        $id = $this->getParam('id');

        if ($id) {
            $to = new NucleoCoTO();
            $to->setId_nucleoco($id);

            $mensageiro = $this->bo->deletarNucleo($to);
            $this->_helper->json($mensageiro);
        }
    }

    public function headAction()
    {
    }

}

<?php

/**
 * Classe Controller APi para SpTramite
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @package default
 * @subpackage controller
 * @since 2014-11-04
 */
class Api_SpTramiteController extends Ead1_RestRestrita
{

    private $negocio;

    public function init ()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Tramite();
    }

    public function deleteAction ()
    {
        
    }

    public function getAction ()
    {
        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);
        Zend_Debug::dump($params);
        exit;
//        if (array_key_exists('id_categoriarramite',$params) && ) {
//            $result = $this->negocio->retornarTramiteSp();
//        }
    }

    public function headAction ()
    {
        
    }

    public function indexAction ()
    {
        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);
        if (array_key_exists('id_categoriarramite', $params) && array_key_exists('chave', $params)) {
            $result = $this->negocio->retornarTramiteSp($params['id_categoriarramite'], $params['chave']);
            $arrResult = array();
            if ($result->getTipo() == Ead1_IMensageiro::SUCESSO) {
                foreach ($result->getMensagem() as $i => $tramite) {
                    $arrResult[$i] = $tramite->toArray();

                    if ($tramite->getDt_cadastro() instanceof Zend_Date) {
                        $arrResult[$i]['dt_cadastro'] = $tramite->getDt_cadastro()->toString('dd/MM/Y H:m:s');
                    }
                }
            }
            $this->getResponse()
                    ->appendBody(json_encode($arrResult));
        }
    }

    public function postAction ()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
    }

    public function putAction ()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
        //salva o tramite
        $result = $this->negocio->salvarTramite($data);

        //pega o resultado e verifica se esta com sucesso
        if ($result->getTipo() == Ead1_IMensageiro::SUCESSO) {
            //retorna sucesso
            $this->getResponse()->appendBody(json_encode($result->getFirstMensagem()->toArray()));
            $this->getResponse()->setHttpResponseCode(201);
        } else {
            //restorn erro
            $this->getResponse()->appendBody(json_encode($result->toArrayAll()));
            $this->getResponse()->setHttpResponseCode(400);
        }
    }

}

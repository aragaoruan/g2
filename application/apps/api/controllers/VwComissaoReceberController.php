<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of VwComissaoReceberController
 *
 * @author kayo.silva
 */
class Api_VwComissaoReceberController extends Ead1_RestRestrita
{

    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new G2\Negocio\PagamentoColaborador();
    }

    public function deleteAction()
    {

    }

    public function getAction()
    {

    }

    public function headAction()
    {

    }

    public function indexAction()
    {
        $this->getResponse()
            ->appendBody(json_encode(array()));
    }

    public function postAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = array();
        foreach (Zend_Json::decode($body) as $key => $value) {
            if ($value)
                $data[$key] = $value;
        }

        $result = $this->negocio->salvarComissaoReceber($data);
        $arrReturn = array();
        if (is_array($result) && $result) {
            foreach ($result as $i => $row) {
                $result[$i]['id'] = $row['id_usuario'];
            }
            $this->getResponse()->setHttpResponseCode(201)->appendBody(json_encode($result));
        } else {
            $this->getResponse()->setHttpResponseCode(400)->appendBody('Erro ao tentar salvar. ' . $result);
        }
    }

    public function putAction()
    {

    }

//put your code here
}

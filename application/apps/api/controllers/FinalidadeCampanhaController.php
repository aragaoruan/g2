<?php


class Api_FinalidadeCampanhaController extends Ead1_RestRestrita
{
    private $negocio;
    private $repositoryName = '\G2\Entity\FinalidadeCampanha';

    public function init()
    {
        parent::init();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->negocio = new \G2\Negocio\FinalidadeCampanha();
    }

    public function indexAction()
    {
        $this->getAction();
    }


    public function getAction()
    {
        $id = $this->getParam('id');
        $arrReturn = $this->negocio->retornaFinalidadesCampanhaAtivas($id);
        $this->_helper->json($arrReturn);
    }

}
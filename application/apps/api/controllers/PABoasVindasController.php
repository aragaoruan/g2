<?php

/**
 * Controller para gerenciar local de aula
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 * @since 2013-09-14
 * @package application
 * @subpackage controller
 */
class Api_PABoasVindasController extends Ead1_RestRestrita {

    private $doctrineContainer;
    private $em;

    public function init() {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
    }

    public function indexAction() {

        $etapa = $this->em->getRepository('\G2\Entity\PABoasVindas')->findByEntidadeSessao();
        if (!$etapa) {
            $etapa = new \G2\Entity\PABoasVindas();
        }
        //$serializer = new \Ead1\Doctrine\EntitySerializer($this->em);
        //$serializer->entityToArray($boasVindas);
        $retorno = array(
            'id_pa_boasvindas' => $etapa->getId_pa_boasvindas(),
            'id_usuariocadastro' => $etapa->getId_usuariocadastro()->getId_usuario(),
            'id_entidade' => $etapa->getId_entidade()->getId_entidade(),
            'id_textosistema' => $etapa->getId_textosistema()->getId_textosistema(),
            'st_texto' => $etapa->getId_textosistema()->getSt_texto(),
        );
        $this->getResponse()->appendBody(json_encode($retorno));
    }

    public function deleteAction() {
//        $id = intval($this->getParam('id'));
//
//        if (!is_int($id)) {
//            $this->getResponse()->setHttpResponseCode(400);
//            $this->getResponse()->setBody('Não posso apagar um registro com ID inválido!');
//        } else {
//            $return = $this->negocio->delete($id);
//            $this->getResponse()->setHttpResponseCode(204)
//                    ->appendBody('Registro ' . $return->getSt_tipodematerial() . ' removido');
//        }
    }

    public function getAction() {
        $etapa = $this->em->getRepository('\G2\Entity\PABoasVindas')->findByEntidadeSessao();
        if (!$etapa) {
            $etapa = new \G2\Entity\PABoasVindas();
        }
        //$serializer = new \Ead1\Doctrine\EntitySerializer($this->em);
        //$serializer->entityToArray($boasVindas);
        $retorno = array(
            'id_pa_boasvindas' => $etapa->getId_pa_boasvindas(),
            'id_usuariocadastro' => $etapa->getId_usuariocadastro()->getId_usuario(),
            'id_entidade' => $etapa->getId_entidade()->getId_entidade(),
            'id_textosistema' => $etapa->getId_textosistema()->getId_textosistema(),
            'st_texto' => $etapa->getId_textosistema()->getSt_texto(),
        );
        $this->getResponse()->appendBody(json_encode($retorno));
    }

    public function headAction() {
        
    }

    public function postAction() {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
        $result = $this->em->getRepository('\G2\Entity\PABoasVindas')->save($data);
        $this->getResponse()->appendBody(json_encode(array(
            'id_pa_boasvindas' => $result->getId_pa_boasvindas(),
            'type' => 'success',
            'title' => 'Salvo com Sucesso',
            'text' => 'O registro foi salvo com sucesso!'
        )));
        $this->getResponse()->setHttpResponseCode(201);
    }

    public function putAction() {

        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
        $result = $this->em->getRepository('\G2\Entity\PABoasVindas')->update($data);
        $this->getResponse()->appendBody(json_encode(array(
            'id_pa_boasvindas' => $result->getId_pa_boasvindas(),
            'type' => 'success',
            'title' => 'Atualizado com Sucesso',
            'text' => 'O registro foi atualizado com sucesso!'
        )));
        $this->getResponse()->setHttpResponseCode(201);
    }

}

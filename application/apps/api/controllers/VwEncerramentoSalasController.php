<?php

/**
 * Controller para VwMatricula
 * @author Rafael Rocha <rafael.rocha.mg@gmail.com>
 * @since 2014-01-28
 * @package application
 * @subpackage controller
 */
class Api_VwEncerramentoSalasController extends Ead1_RestRestrita
{

    private $doctrineContainer;
    private $em;

    public function init()
    {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
    }

    public function indexAction()
    {

        $produto = new \G2\Negocio\Produto();
        $mensageiro = $produto->findAllProduto();

        if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json($mensageiro);
        } else {
            $this->getResponse()->setHttpResponseCode(201);
            $this->_helper->json($mensageiro->getMensagem());
        }
    }

    public function getAction()
    {
        $matriculaBO = new MatriculaBO();

        $to = new \VwMatriculaTO();
        $to->setId_matricula($this->_getParam('id'));
        $to->setId_usuario($this->_getParam('id_usuario'));
        $to->setBl_ativo(true);

        $mensageiro = $matriculaBO->retornarMatriculaRelacaoEntidadeSessao($to);

        //$to = $to->fetch(false, $this->_getParam('id'));

        if ($this->getParam('id') && is_array($mensageiro->getMensagem())) {
            $retorno = $mensageiro->getMensagem();
            $retorno = $retorno[0]->toBackboneArray();
        } else {
            $retorno = $mensageiro->getMensagem();
            foreach ($retorno as $key => $obj) {
                $retorno[$key] = $obj->toBackboneArray();
            }
        }
        $this->_helper->json($retorno);
    }

    public function postAction()
    {

    }

    public function putAction()
    {

    }

    public function deleteAction()
    {

    }

    public function headAction()
    {

    }

}

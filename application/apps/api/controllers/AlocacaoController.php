<?php

/**
 * Controller para TbAlocacao
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class Api_AlocacaoController extends Ead1_RestRestrita
{
    public function init()
    {

    }

    public function indexAction()
    {
        $negocio = new \G2\Negocio\Alocacao();
        $params = $this->getRequest()->getParams();
        $array = array();

        unset($params['module']);
        unset($params['action']);
        unset($params['controller']);

        foreach ($negocio->findAllBy($params) as $key => $value) {
            $array[] = $result = array(
                'id_alocacao' => $value->getId_alocacao(),
                'id_situacaotcc' => $value->getId_situacaotcc(),
                'id_categoriasala' => $value->getId_categoriasala(),
                'bl_ativo' => $value->getBl_ativo(),
                'bl_desalocar' => $value->getBl_desalocar(),
                'nu_diasextensao' => $value->getNu_diasextensao(),
                'dt_cadastro' => $value->getDt_cadastro() ? $value->getDt_cadastro()->format('d/m/Y H:i:s') : NULL,
                'id_saladeaula' => $value->getId_saladeaula() ? $value->getId_saladeaula()->getId_saladeaula() : NULL,
                'id_evolucao' => $value->getId_evolucao() ? $value->getId_evolucao()->getId_evolucao() : NULL,
                'id_situacao' => $value->getId_situacao() ? $value->getId_situacao()->getId_situacao() : NULL,
                'id_usuariocadastro' => $value->getId_usuariocadastro() ? $value->getId_usuariocadastro()->getId_usuario() : NULL,
                'id_matriculadisciplina' => $value->getId_matriculadisciplina() ? $value->getId_matriculadisciplina()->getId_matriculadisciplina() : NULL,
                'dt_inicio' => $value->getDt_inicio() ? $value->getDt_inicio()->format('d/m/Y H:i:s') : NULL
            );
        }

        $this->_helper->json($array);
    }

    public function getAction()
    {
        $negocio = new \G2\Negocio\Alocacao();
        $id_alocacao = $this->getRequest()->getParam('id');

        $find = $negocio->findAlocacao($id_alocacao);

        if ($find) {

            $result = array(
                'id_alocacao' => $find->getId_alocacao(),
                'id_situacaotcc' => $find->getId_situacaotcc(),
                'id_categoriasala' => $find->getId_categoriasala(),
                'bl_ativo' => $find->getBl_ativo(),
                'bl_desalocar' => $find->getBl_desalocar(),
                'nu_diasextensao' => $find->getNu_diasextensao(),
                'dt_cadastro' => $find->getDt_cadastro() ? $find->getDt_cadastro()->format('d/m/Y H:i:s') : NULL,
                'id_saladeaula' => $find->getId_saladeaula() ? $find->getId_saladeaula()->getId_saladeaula() : NULL,
                'id_evolucao' => $find->getId_evolucao() ? $find->getId_evolucao()->getId_evolucao() : NULL,
                'id_situacao' => $find->getId_situacao() ? $find->getId_situacao()->getId_situacao() : NULL,
                'id_usuariocadastro' => $find->getId_usuariocadastro() ? $find->getId_usuariocadastro()->getId_usuario() : NULL,
                'id_matriculadisciplina' => $find->getId_matriculadisciplina() ? $find->getId_matriculadisciplina()->getId_matriculadisciplina() : NULL,
                'dt_inicio' => $find->getDt_inicio() ? $find->getDt_inicio()->format('d/m/Y H:i:s') : NULL
            );

        } else {
            $result = array();
        }

        $this->_helper->json($result);

    }

    public function postAction()
    {

    }

    public function putAction()
    {

    }

    public function deleteAction()
    {

    }

    public function headAction()
    {

    }

}

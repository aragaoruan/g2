<?php

/**
 * Class Api_VwFormaMeioDivisaoController
 * @Author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Api_VwFormaMeioDivisaoController extends Ead1_RestRestrita
{
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\FormaPagamento();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['module'], $params['controller'], $params['action'], $params['order_by'], $params['sort']);

        $result = $this->negocio->retornarTipoDivisaoFinanceira($params);

        if ($result) {
            foreach ($result as $i => $row) {
                $result[$i] = $this->negocio->toArrayEntity($row);
            }
        }

        $this->getResponse()
            ->appendBody(json_encode($result));
    }

    public function getAction()
    {
        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);
        $result = $this->negocio->retornarTipoDivisaoFinanceira($params);

        if ($result) {
            foreach ($result as $i => $row) {
                $result[$i] = $this->negocio->toArrayEntity($row);
            }
        }

        $this->getResponse()
            ->appendBody(json_encode($result));
    }

    public function headAction()
    {
        // TODO: Implement headAction() method.
    }

    public function postAction()
    {
        // TODO: Implement postAction() method.
    }

    public function putAction()
    {
        // TODO: Implement putAction() method.
    }

    public function deleteAction()
    {
        // TODO: Implement deleteAction() method.
    }
}
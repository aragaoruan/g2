<?php

/**
 * @package application
 * @subpackage controller
 */
class Api_TesteRegraPagamentoController extends Ead1_RestRestrita {

    private $doctrineContainer;
    private $em;
    private $bo;

    public function init() {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
        $this->bo = new G2\Negocio\RegraPagamento();
    }

    public function indexAction() {
        $this->getAction();
    }

    public function getAction() {
        try {
            $body = '{"id_regrapagamento":1,"id_entidade":14,"st_entidade":"AVM","id_areaconhecimento":null,"st_areaconhecimento":null,"id_projetopedagogico":null,"st_projetopedagogico":null,"id_professor":null,"st_professor":null,"id_cargahoraria":1,"nu_cargahoraria":40,"nu_valor":50,"id_tipodisciplina":2,"st_tipodisciplina":"TCC","dt_cadastro":{"date":"2014-09-06 22:58:04","timezone_type":3,"timezone":"America\/Sao_Paulo"}}';
            $data = Zend_Json::decode($body);

            $result = $this->bo->save($data);
            $this->getResponse()->appendBody(json_encode($result));
            $this->getResponse()->setHttpResponseCode(201);
        } catch (Exception $e) {
            $this->getResponse()->appendBody(json_encode(array('error' => $e->getMessage())));
            $this->getResponse()->setHttpResponseCode(201);
        }
    }

    public function postAction() {
        
    }

    public function putAction() {

        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
        $this->form->getElement('id_areaconhecimento');
        $dataValidate = $data;
        $dataValidate['st_areaconhecimento'] = trim(strip_tags($dataValidate['st_areaconhecimento']));
        if ($this->form->isValid($dataValidate)) {
            $result = $this->negocio->updateAreaConhecimento($data);
            $this->getResponse()->appendBody(json_encode(array(
                'id' => $result->getId(),
                'type' => 'success',
                'title' => 'Registro Atualizado',
                'text' => 'O registro foi atualizado com sucesso!'
            )));
            $this->getResponse()->setHttpResponseCode(201);
        } else {
            $arrErros = $this->view->messages($this->form)->toString();
            if ($arrErros) {
                $arrErros = array(
                    'type' => 'error',
                    'title' => 'Atenção!',
                    'text' => $this->view->messages($this->form)->toString()
                );
            }
            $this->getResponse()->appendBody(json_encode($arrErros));
            $this->getResponse()->setHttpResponseCode(201);
        }
    }

    public function deleteAction() {
        try {
            $id = intval($this->getParam('id_areaconhecimento'));

            if (!is_int($id)) {
                $this->getResponse()->setHttpResponseCode(400);
                $this->getResponse()->setBody('Não posso apagar um registro com ID inválido!');
            } else {
                $entity = $this->negocio->findAreaConhecimento($id);
                $result = $this->negocio->deletarArea($id);
                $this->_helper->json(array(
                    'id' => $result->getId_areaconhecimento(),
                    'type' => 'success',
                    'title' => 'Ação bem sucedida!',
                    'text' => 'Registro ' . $result->getSt_areaconhecimento() . ' removido.'
                ));

                $this->getResponse()->setHttpResponseCode(204)->appendBody('Registro ' . $entity->getSt_areaconhecimento() . ' removido');
            }
        } catch (Zend_Exception $e) {
            $this->getResponse()->setHttpResponseCode(400)->appendBody('Não foi possível apagar o registro' . $e);
        }
    }

    public function headAction() {
        
    }

}

<?php

/**
 * Controller para gerenciar as DisciplinaConteudo
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */

class Api_DisciplinaConteudoController extends Ead1_RestRestrita
{

    public function indexAction()
    {
        $negocio = new \G2\Negocio\Disciplina();

        $limit = NULL;
        $offset = NULL;
        $params = $this->getAllParams();

        if (array_key_exists('limit', $params)){
            $limit = $params['limit'];
        }

        unset($params['module'], $params['controller'], $params['action'], $params['limit'], $params['offset']);

        $result = $negocio->findByDisciplinaConteudo($params, array(), $limit, $offset);
        $array = array();
        foreach ($result as $key => $entity) {
            $array[$key] = $negocio->toArrayEntity($entity);
            $array[$key]['id_formatoarquivo'] = $entity->getId_formatoarquivo()->getId_formatoarquivo();
            $array[$key]['st_formatoarquivo'] = $entity->getId_formatoarquivo()->getSt_formatoarquivo();
            $array[$key]['id_disciplina'] = $entity->getId_disciplina()->getId_disciplina();
            $array[$key]['id_usuariocadastro'] = $entity->getId_usuariocadastro()->getId_usuario();
        }

        $this->_helper->json($array);
    }

    public function getAction()
    {
        ;
    }

    public function postAction()
    {
        ;
    }

    public function putAction()
    {
        ;
    }

    public function deleteAction()
    {
        ;
    }

    public function headAction()
    {
    }

}
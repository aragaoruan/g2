<?php

/**
 * Controller para gerenciar as Perfil
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2014-12-02
 * @package application
 * @subpackage controller
 */

class Api_PerfilController extends Ead1_RestRestrita
{

    private $bo;
    private $negocioPerfil;

    public function init()
    {
        parent::init();
        $this->bo = new PerfilBO();
        $this->negocioPerfil = new \G2\Negocio\Perfil();
    }

    public function indexAction()
    {
        $to = new VwPerfisEntidadeTO();

        $results = $this->bo->retornaPerfisEntidade($to);

        $this->_helper->json($results);
    }

    public function getAction()
    {
    }

    /**
     * Author: Helder Silva <helder.silva@unyleya.com.br>
     * Método refatorado para doctrine em 27/09/17
     */
    public function postAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
        try {

            $objPerfil = $this->negocioPerfil->persistirPerfil($data);
            if ($objPerfil) {
                $this->negocioPerfil->persistirPerfilEntidade($objPerfil, $this->sessao->id_entidade);
            }

            // Adicionar as organizacoes do JSTREE
            if (isset($data['organizacao_selected']) && !empty($data['organizacao_selected'])) {
                $this->negocioPerfil->persistirPerfilEntidadeArvore($data['organizacao_selected'], $objPerfil);
            }

            // Adicionar funcionalidades do JSTREE
            if (isset($data['funcionalidade_selected']) && !empty($data['funcionalidade_selected'])) {
                $this->negocioPerfil->persistirPerfilFuncionalidade($data['funcionalidade_selected'], $objPerfil);
            }

            // Adicionar permissoes selecionadas
            if (isset($data['permissao_selected']) && !empty($data['permissao_selected'])) {

                // este trecho de código serve para selecionar quais funcionalidades seráo inseridas. Foi necessário por causa que ao remové-las as mesmas eram reinseridas.
                $permissoes = array();
                foreach ($data['permissao_selected'] as $index){
                    if(in_array($index['id_funcionalidade'], $data['funcionalidade_selected'])){
                        $permissoes[] = $index;
                    }
                }

                $this->negocioPerfil->vincularPerfilPermissaoFuncionalidade($permissoes, $objPerfil);
            } else {
                $this->negocioPerfil->excluirPermissoesPerfil($objPerfil);
            }

            $mensageiro = new Ead1_Mensageiro(\G2\Constante\MensagemSistema::FUNC_SALVAS, Ead1_IMensageiro::SUCESSO);

        } catch (Zend_Validate_Exception $ze) {
            $this->getResponse()->setHttpResponseCode(400);
            $mensageiro = new Ead1_Mensageiro($ze->getMessage(), Ead1_IMensageiro::ERRO);
        } catch (Zend_Exception $ze) {
            $this->getResponse()->setHttpResponseCode(400);
            $mensageiro = new Ead1_Mensageiro($ze->getMessage(), Ead1_IMensageiro::ERRO);
        } catch (Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $mensageiro = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }

        $this->_helper->json($mensageiro);
    }

    public function putAction()
    {
        $this->postAction();
    }

    public function deleteAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $idPerfil = $this->getParam('id');

        if ($idPerfil) {
            $to = new PerfilTO();
            $to->setId_perfil($idPerfil);
            $to->setBl_ativo(false);

            $mensageiro = $this->bo->excluirPerfil($to, '');

            $this->_helper->json($mensageiro);
        }
    }

    public
    function headAction()
    {
    }

}

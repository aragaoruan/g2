<?php

/**
 * Controller para gerenciar os periodos letivos
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 * @package application
 * @subpackage controller
 */
class Api_PeriodoLetivoController extends Zend_Rest_Controller
{

    private $negocio;

    public function init()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->negocio = new \G2\Negocio\PeriodoLetivo();
    }

    public function indexAction()
    {
        try {
            $periodos = $this->negocio->findAllPeriodos();

            $retorno = [];
            foreach ($periodos as $k => $p) {
                $retorno[$k]['id_periodoletivo'] = $p->getId_periodoletivo();
                $retorno[$k]['st_periodoletivo'] = $p->getSt_periodoletivo();
                $retorno[$k]['id_tipooferta'] = $p->getId_tipooferta() ? $p->getId_tipooferta()->getId_tipooferta() : '';
                $retorno[$k]['st_tipooferta'] = $p->getId_tipooferta() ? $p->getId_tipooferta()->getSt_tipooferta() : '';
                $retorno[$k]['dt_inicioinscricao'] = $p->getDt_inicioinscricao() ? $p->getDt_inicioinscricao()->format('d/m/Y') : '';
                $retorno[$k]['dt_fiminscricao'] = $p->getDt_fiminscricao() ? $p->getDt_fiminscricao()->format('d/m/Y') : '';
                $retorno[$k]['dt_abertura'] = $p->getDt_abertura() ? $p->getDt_abertura()->format('d/m/Y') : '';
                $retorno[$k]['dt_encerramento'] = $p->getDt_encerramento() ? $p->getDt_encerramento()->format('d/m/Y') : '';
            }
            $this->getResponse()->appendBody(json_encode($retorno));
        } catch (Exception $e) {
            return new Exception($e->getMessage());
        }

        return false;
    }

    public function getAction()
    {
        $entity = $this->negocio->find('G2\Entity\PeriodoLetivo', $this->getParam('id'));
        if ($entity) {

            /**
             * Prepara os campos de data para o backbone
             */

            if ($entity->getDt_inicioinscricao()) {
                $entity->setDt_inicioinscricao($entity->getDt_inicioinscricao()->format('Y-m-d H:i:s'));
            }

            if ($entity->getDt_fiminscricao()) {
                $entity->setDt_fiminscricao($entity->getDt_fiminscricao()->format('Y-m-d H:i:s'));
            }

            if ($entity->getDt_abertura()) {
                $entity->setDt_abertura($entity->getDt_abertura()->format('Y-m-d H:i:s'));
            }

            if ($entity->getDt_encerramento()) {
                $entity->setDt_encerramento($entity->getDt_encerramento()->format('Y-m-d H:i:s'));
            }

            $this->_helper->json($this->negocio->toArrayEntity($entity));
        }
    }

    public function postAction()
    {
        $body = $this->getRequest()->getRawBody();
        $dados = Zend_Json::decode($body);

        $periodos = $this->negocio->salvarDadosPeriodoLetivo($dados);

        if ($periodos->getTipo()) {
            $retorno = [];
            foreach ($periodos->getMensagem() as $k => $p) {
                $retorno[$k]['id_periodoletivo'] = $p->getId_periodoletivo();
                $retorno[$k]['st_periodoletivo'] = $p->getSt_periodoletivo();
                $retorno[$k]['id_tipooferta'] = $p->getId_tipooferta()->getId_tipooferta();
                $retorno[$k]['st_tipooferta'] = $p->getId_tipooferta()->getSt_tipooferta();
                $retorno[$k]['dt_inicioinscricao'] = $p->getDt_inicioinscricao() ? $p->getDt_inicioinscricao()->format('d/m/Y') : '';
                $retorno[$k]['dt_fiminscricao'] = $p->getDt_fiminscricao() ? $p->getDt_fiminscricao()->format('d/m/Y') : '';
                $retorno[$k]['dt_abertura'] = $p->getDt_abertura() ? $p->getDt_abertura()->format('d/m/Y') : '';
                $retorno[$k]['dt_encerramento'] = $p->getDt_encerramento() ? $p->getDt_encerramento()->format('d/m/Y') : '';
            }

            $this->getResponse()->setHttpResponseCode(200)->appendBody(json_encode($retorno[0]));
        } else {
            $this->getResponse()->setHttpResponseCode(401)->appendBody($periodos->getMensagem()[0]);
        }
    }

    public function putAction()
    {
        $body = $this->getRequest()->getRawBody();
        $dados = Zend_Json::decode($body);

        $periodos = $this->negocio->salvarDadosPeriodoLetivo($dados);

        if ($periodos->getTipo()) {
            $retorno = [];
            foreach ($periodos->getMensagem() as $k => $p) {
                $retorno[$k]['id_periodoletivo'] = $p->getId_periodoletivo();
                $retorno[$k]['st_periodoletivo'] = $p->getSt_periodoletivo();
                $retorno[$k]['id_tipooferta'] = $p->getId_tipooferta()->getId_tipooferta();
                $retorno[$k]['st_tipooferta'] = $p->getId_tipooferta()->getSt_tipooferta();
                $retorno[$k]['dt_inicioinscricao'] = $p->getDt_inicioinscricao() ? $p->getDt_inicioinscricao()->format('d/m/Y') : '';
                $retorno[$k]['dt_fiminscricao'] = $p->getDt_fiminscricao() ? $p->getDt_fiminscricao()->format('d/m/Y') : '';
                $retorno[$k]['dt_abertura'] = $p->getDt_abertura() ? $p->getDt_abertura()->format('d/m/Y') : '';
                $retorno[$k]['dt_encerramento'] = $p->getDt_encerramento() ? $p->getDt_encerramento()->format('d/m/Y') : '';
            }

            $this->getResponse()->setHttpResponseCode(200)->appendBody(json_encode($retorno[0]));
        } else {
            $this->getResponse()->setHttpResponseCode(401)->appendBody($periodos->getMensagem()[0]);
        }
    }

    public function deleteAction()
    {
        $id = $this->getParam('id');

        if (isset($id)) {
            $retorno = $this->negocio->removerPeriodoLetivo((int)$id);

            if ($retorno->getTipo())
                $this->getResponse()->setHttpResponseCode(200)->appendBody(json_encode($retorno->getMensagem()[0]));
            else
                $this->getResponse()->setHttpResponseCode(501)->setBody('A oferta de sala não pode ser excluída porque já está vinculado a uma sala de aula.');
        }
    }

    public function headAction()
    {

    }
}
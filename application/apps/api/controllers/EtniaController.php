<?php

/**
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @date 2016-05-02
 */
class Api_EtniaController extends Ead1_RestRestrita
{

    /**
     * @var \G2\Negocio\Negocio $negocio
     */
    private $negocio;
    private $repositoryName = '\G2\Entity\Etnia';

    public function init()
    {
        parent::init();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->negocio = new \G2\Negocio\Negocio();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['module'], $params['controller'], $params['action']);

        $results = $this->negocio->findBy($this->repositoryName, $params, array('id_etnia' => 'ASC'));

        foreach ($results as $key => &$entity) {
            $entity = $this->negocio->toArrayEntity($entity);
        }

        $this->_helper->json($results);
    }

    public function getAction()
    {
        $entity = $this->negocio->find($this->repositoryName, $this->getParam('id'));
        $this->_helper->json($this->negocio->toArrayEntity($entity));
    }

}
<?php

/**
 * Class Api_VwTurmaProjetoPedagogicoController
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2015-01-05
 */
class Api_VwTurmaProjetoPedagogicoController extends Ead1_RestRestrita
{
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\ProjetoPedagogico();
    }

    public function getAction()
    {
    }

    public function deleteAction()
    {

    }

    public function headAction()
    {

    }

    public function indexAction()
    {

    }

    public function postAction()
    {

    }

    public function putAction()
    {

    }


}
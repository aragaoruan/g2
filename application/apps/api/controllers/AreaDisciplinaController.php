<?php

/**
 * Controller para criar aplicações para avaliação (locais de prova para agendamento de prova final)
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since  2013-18-12
 * @package Api
 * @subpackage controller
 */
class Api_AreaDisciplinaController extends Ead1_RestRestrita
{

    private $negocio;

    public function init ()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Disciplina();
    }

    public function deleteAction ()
    {
        
    }

    public function getAction ()
    {
        $params = $this->_getAllParams();
        unset($params['controller'], $params['module'], $params['action']);
        $result = $this->negocio->findAreasJoinDisciplina($params);
        $arrReturn = array();
        if ($result->getType() == 'success') {
            foreach ($result->getMensagem() as $row) {
                $arrReturn[] = array(
                    'id_disciplina' => $row->getId_disciplina(),
                    'id_areaconhecimento' => $row->getId_areaconhecimento()
                );
            }
            $this->getResponse()->appendBody(json_encode($arrReturn));
            $this->getResponse()->setHttpResponseCode(200);
        } else {
            $this->getResponse()->appendBody(json_encode($result->getText()));
            $this->getResponse()->setHttpResponseCode(200);
        }
    }

    public function headAction ()
    {
        
    }

    public function indexAction ()
    {
        
    }

    public function postAction ()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
        $result = $this->negocio->salvaAreaConhecimentoJoinDisciplina($data);
        if ($result->getType() == 'success') {
            foreach ($result->getMensagem() as $row) {
                $arrReturn = $row->toArray();
            }
            $this->getResponse()->appendBody(json_encode($arrReturn));
            $this->getResponse()->setHttpResponseCode(201);
        } else {
            $this->getResponse()->appendBody(json_encode($result->getText()));
            $this->getResponse()->setHttpResponseCode(400);
        }
    }

    public function putAction ()
    {
        
    }

}
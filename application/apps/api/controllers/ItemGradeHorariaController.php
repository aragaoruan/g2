<?php


/**
 * Class Api_ItemGradeHorariaController Rest para item de grade horaria
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-12-05
 */
class Api_ItemGradeHorariaController extends Zend_Rest_Controller
{
    /**
     * @var \G2\Negocio\GradeHoraria $negocio
     */
    private $negocio;

    public function init()
    {
        $this->_helper->viewRenderer->setNoRender();
        parent::init();
        $this->negocio = new \G2\Negocio\GradeHoraria();
    }


    /**
     * The index action handles index/list requests; it should respond with a
     * list of the requested resources.
     */
    public function indexAction()
    {
        // TODO: Implement indexAction() method.
    }

    /**
     * The get action handles GET requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function getAction()
    {
        if ($this->getParam('id')) {
            $id = $this->getParam('id');
            $result = $this->negocio->findItemGrade($id);

            if ($result instanceof \G2\Entity\ItemGradeHoraria) {

                $result = array(
                    'id' => $result->getId_itemgradehoraria(),
                    'id_itemgradehoraria' => $result->getId_itemgradehoraria(),
                    'id_gradehoraria' => $result->getId_gradehoraria() ? $result->getId_gradehoraria()->getId_gradehoraria() : null,
                    'id_unidade' => $result->getId_unidade() ? $result->getId_unidade()->getId_entidade() : null,
                    'id_entidadecadastro' => $result->getId_entidadecadastro() ? $result->getId_entidadecadastro()->getId_entidade() : null,
                    'id_turno' => $result->getId_turno() ? $result->getId_turno()->getId_turno() : null,
                    'id_turma' => $result->getId_turma() ? $result->getId_turma()->getId_turma() : null,
                    'id_projetopedagogico' => $result->getId_projetopedagogico() ? $result->getId_projetopedagogico()->getId_projetopedagogico() : null,
                    'id_diasemana' => $result->getId_diasemana() ? $result->getId_diasemana()->getId_diasemana() : null,
                    'dt_diasemana' => $result->getDt_diasemana() ? $result->getDt_diasemana()->format('d/m/Y') : null,
                    'id_professor' => $result->getId_professor() ? $result->getId_professor()->getId_usuario() : null,
                    'id_disciplina' => $result->getId_disciplina() ? $result->getId_disciplina()->getId_disciplina() : null,
                    'bl_ativo' => $result->getBl_ativo(),
                    'dt_cadastro' => $result->getDt_cadastro(),
                    'st_observacao' => $result->getSt_observacao(),
                    'nu_encontro' => $result->getNu_encontro(),
                    'id_tipoaula' => $result['id_tipoaula'],
                    'st_tipoaula' => $result['st_tipoaula']
                );
            }
            $this->getResponse()->appendBody(json_encode($result));
        }
    }

    /**
     * The head action handles HEAD requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function headAction()
    {
        // TODO: Implement headAction() method.
    }

    /**
     * The post action handles POST requests; it should accept and digest a
     * POSTed resource representation and persist the resource state.
     */
    public function postAction()
    {
        $body = $this->getRequest()->getPost();
        if (!$body) {
            $data = $this->getRequest()->getRawBody();
            $body = Zend_Json::decode($data);
        }

        try {
            $result = $this->negocio->salvarItemGradeHoraria($body['model'], $body['valida']);
            $this->getResponse()->appendBody(json_encode($result));
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400)->setBody($e->getMessage());
        }

    }

    /**
     * The put action handles PUT requests and receives an 'id' parameter; it
     * should update the server resource state of the resource identified by
     * the 'id' value.
     */
    public function putAction()
    {
        try {
            $body = $this->getRequest()->getRawBody();
            $data = Zend_Json_Decoder::decode($body);
            if ($data) {
                $result = $this->negocio->salvarItemGradeHoraria($data, null);
                if ($result instanceof \G2\Entity\ItemGradeHoraria) {

                    $arrReturn = array(
                        'id' => $result->getId_itemgradehoraria(),
                        'id_itemgradehoraria' => $result->getId_itemgradehoraria(),
                        'id_gradehoraria' => $result->getId_gradehoraria() ? $result->getId_gradehoraria()->getId_gradehoraria() : null,
                        'id_unidade' => $result->getId_unidade() ? $result->getId_unidade()->getId_entidade() : null,
                        'id_entidadecadastro' => $result->getId_entidadecadastro() ? $result->getId_entidadecadastro()->getId_entidade() : null,
                        'id_turno' => $result->getId_turno() ? $result->getId_turno()->getId_turno() : null,
                        'id_turma' => $result->getId_turma() ? $result->getId_turma()->getId_turma() : null,
                        'id_projetopedagogico' => $result->getId_projetopedagogico() ? $result->getId_projetopedagogico()->getId_projetopedagogico() : null,
                        'id_diasemana' => $result->getId_diasemana() ? $result->getId_diasemana()->getId_diasemana() : null,
                        'dt_diasemana' => $result->getDt_diasemana() ? $result->getDt_diasemana()->format('d/m/Y') : null,
                        'id_professor' => $result->getId_professor() ? $result->getId_professor()->getId_usuario() : null,
                        'id_disciplina' => $result->getId_disciplina() ? $result->getId_disciplina()->getId_disciplina() : null,
                        'bl_ativo' => $result->getBl_ativo(),
                        'dt_cadastro' => $result->getDt_cadastro(),
                        'st_observacao' => $result->getSt_observacao(),
                        'nu_encontro' => $result->getNu_encontro(),
                        'id_turnoaula' => $result->getId_turnoaula() ? $result->getId_turnoaula()->getId_turno() : null
                    );

                    $this->getResponse()->setHttpResponseCode(201)->appendBody(json_encode($arrReturn));

                } elseif ($result instanceof Ead1_Mensageiro) {
                    $this->getResponse()->setHttpResponseCode(400)->setBody(json_encode(array(
                        'text' => $result->getText(),
                        'type' => $result->getType(),
                        'title' => $result->getTitle()
                    )));
                } else {
                    $this->getResponse()->setHttpResponseCode(400)->setBody("Erro ao salvar item da grade horaria");
                }
            }
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400)->setBody($e->getMessage());
        }
    }

    /**
     * The delete action handles DELETE requests and receives an 'id'
     * parameter; it should update the server resource state of the resource
     * identified by the 'id' value.
     */
    public function deleteAction()
    {
        $id = intval($this->getParam('id'));
        $id_disciplina = intval($this->getParam('id_disciplina'));
        $id_gradehoraria = intval($this->getParam('id_gradehoraria'));

        if (!is_int($id)) {
            $this->getResponse()->setHttpResponseCode(400)->setBody('Não posso apagar um registro com ID inválido!');
        } else {
            $result = $this->negocio->deletarItemGradeHoraria($id, $id_gradehoraria, $id_disciplina);
            if ($result && is_bool($result)) {
                $this->getResponse()->setHttpResponseCode(204)->appendBody('Registro removido');
            } else {
                $this->getResponse()->setHttpResponseCode(400)->appendBody('Erro ao tentar remover registro. ' . $result);
            }
        }
    }
}
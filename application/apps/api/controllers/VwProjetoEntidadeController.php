<?php

class Api_VwProjetoEntidadeController extends Ead1_RestRestrita
{

    /**
     * @var \G2\Negocio\Negocio $negocio
     */
    private $negocio;
    private $repositoryName = '\G2\Entity\VwProjetoEntidade';

    public function init()
    {
        parent::init();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->negocio = new \G2\Negocio\Negocio();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();

        unset(
            $params['module'],
            $params['controller'],
            $params['action'],
            $params['idEntidade'],
            $params['idSituacao']
        );

        $results = $this->negocio->findBy($this->repositoryName, $params, array('st_projetopedagogico' => 'ASC'));

        $this->_helper->json($this->negocio->toArrayEntity($results));
    }

    public function getAction()
    {
        $entity = $this->negocio->find($this->repositoryName, $this->getParam('id'));

        $result = $entity ? $this->negocio->toArrayEntity($entity) : new stdClass();

        $this->_helper->json($result);
    }
}

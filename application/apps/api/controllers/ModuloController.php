<?php

/**
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @since 2014-01-10
 * @package application
 * @subpackage controller
 */
class Api_ModuloController extends Ead1_RestRestrita
{


    public function indexAction()
    {

        $to = new ModuloTO();
        $to->montaToDinamico($this->_getAllParams());
        $bo = new ProjetoPedagogicoRO();
        $mensageiro = $bo->retornarModulo($to);


        if ($mensageiro->getTipo() == Ead1_IMensageiro::ERRO) {
            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json($mensageiro);
        } else if ($mensageiro->getTipo() == Ead1_IMensageiro::AVISO) {
            $this->getResponse()->setHttpResponseCode(201);
            $this->_helper->json(array());
        } else {
            $this->getResponse()->setHttpResponseCode(201);
            $this->_helper->json($mensageiro->getMensagem());
        }

    }

    public function getAction()
    {

        $to = new ModuloTO();
        $to->setId_modulo($this->_getParam('id'));
        $to->fetch(true, true, true);
        $this->getResponse()->setHttpResponseCode(201);
        $this->_helper->json($to->toArray());

    }

    public function postAction()
    {
        $json = utf8_encode($this->getRequest()->getRawBody());
        $data = Zend_Json::decode($json);

        $to = new ModuloTO();
        $to->montaToDinamico($data);

        $bo = new ProjetoPedagogicoRO();
        $to->setSt_modulo(utf8_decode($to->getSt_modulo()));
        $to->setSt_descricao(utf8_decode($to->getSt_descricao()));
        $to->setSt_tituloexibicao(utf8_decode($to->getSt_tituloexibicao()));
        $mensageiro = $bo->cadastrarModulo($to);

        if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json($mensageiro);
        } else {
            $this->getResponse()->setHttpResponseCode(201);
            $this->_helper->json($to->toArray());
        }

    }

    public function putAction()
    {
        $json = utf8_encode($this->getRequest()->getRawBody());
        $data = Zend_Json::decode($json);
        $to = new ModuloTO();
        $to->montaToDinamico($data);
        $bo = new ProjetoPedagogicoRO();
        $to->setSt_modulo(utf8_decode($to->getSt_modulo()));
        $to->setSt_descricao(utf8_decode($to->getSt_descricao()));
        $to->setSt_tituloexibicao(utf8_decode($to->getSt_tituloexibicao()));

        $mensageiro = $bo->editarModulo($to);
        if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json($mensageiro);
        } else {
            $this->getResponse()->setHttpResponseCode(201);
            $this->_helper->json($to->toArray());
        }
    }

    public function deleteAction()
    {
        try {
            $id = intval($this->getParam('id'));
            $negocio = new \G2\Negocio\ProjetoPedagogico();
            if (!is_int($id)) {
                $this->getResponse()->setHttpResponseCode(400);
                $this->getResponse()->setBody('Não posso apagar um registro com ID inválido!');
            } else {
                $result = $negocio->removeModuloProjeto($id);

                $this->_helper->json(array(
                    'id' => $result->getId_modulo(),
                    'type' => 'success',
                    'title' => 'Ação bem sucedida!',
                    'text' => 'Registro ' . $result->getSt_modulo() . ' removido.'
                ));
            }
        } catch (Zend_Exception $e) {
            $this->getResponse()->setHttpResponseCode(400)->appendBody('Não foi possível apagar o módulo' . $e);
        }
    }

    public function headAction()
    {

    }


}
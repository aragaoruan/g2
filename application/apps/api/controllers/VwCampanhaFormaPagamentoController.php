<?php

/**
 * Classe de controller MVC para controlar os dados da Vw_AreaProjetoNivelEnsino
 * @author Kayo Silva <kayo.silva@unyleya.com.br> 2014-13-03
 * @package application
 * @subpackage controller
 */
class Api_VwCampanhaFormaPagamentoController extends Ead1_RestRestrita
{

    private $negocio;

    public function init ()
    {
        parent::init();
        $this->negocio = new G2\Negocio\CampanhaComercial();
    }

    public function deleteAction ()
    {
        $idCampanha = $this->getParam('id_campanha');
        $idFormaPagamento = $this->getParam('id_formapagamento');

        $return = $this->negocio->deletaCampanhaFormaPagamento($idCampanha, $idFormaPagamento);

        if($return != false)
        {
            $this->_helper->json(array(
                'id_campanhacomercial' => $idCampanha,
                'id_formapagamento' => $idFormaPagamento,
                'type' => 'success',
                'title' => 'Ação bem sucedida!',
                'text' => 'Forma de pagamento removida com sucesso.'
            ));

        }
        else
        {
            $this->_helper->json(array(
                'type' => 'warning',
                'title' => 'Alerta!',
                'text' => 'Nada foi removido.'
            ));
        }
    }

    public function getAction ()
    {
        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);

        $result = $this->negocio->retornarVwCampanhaFormaPagamento($params);

        $arrResult = array();
        if ($result) {
            foreach ($result as $row) {
                $arrResult[] = $this->negocio->toArrayEntity($row);
            }
        }
        $this->getResponse()
            ->appendBody(json_encode($arrResult));
    }

    public function headAction ()
    {

    }

    public function indexAction ()
    {

    }

    public function postAction ()
    {
        $param = $this->getAllParams();

        unset($param['module']);
        unset($param['controller']);
        unset($param['action']);
        $id_formapagamento =  $param["data"]["id_formapagamento"];
        $id_campanhacomercial = $param["data"]["id_campanhacomercial"];

        $result = $this->negocio->inserirCampanhaFormaPagamento($id_campanhacomercial, $id_formapagamento);

        if($result != false){
            $this->_helper->json(array(
                'id_campanhacomercial' => $id_campanhacomercial,
                'id_formapagamento' => $id_formapagamento,
                'type' => 'success',
                'title' => 'Ação bem sucedida!',
                'text' => 'Forma de pagamento inserida com sucesso.'
            ));
        }else{
            $this->_helper->json(array(
                'type' => 'warning',
                'title' => 'Alerta!',
                'text' => 'Nada foi inserido.'
            ));
        }

    }

    public function putAction ()
    {

    }

}
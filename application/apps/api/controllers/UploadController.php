<?php

/**
 * Controller para gerenciar as Upload
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2014-11-05
 * @package application
 * @subpackage controller
 */
class Api_UploadController extends Ead1_RestRestrita
{

    private $negocio;
    private $repositoryName = 'G2\Entity\Upload';

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Negocio();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['module'], $params['controller'], $params['action']);

        $mensageiro = $this->negocio->findBy($this->repositoryName, $params);
        $results = array();
        foreach ($mensageiro as $entity) {
            $results[] = $this->negocio->toArrayEntity($entity);
        }

        $this->getResponse()->setHttpResponseCode(200);
        $this->getResponse()->appendBody(json_encode($results));
    }

    public function getAction()
    {
        $id = $this->getParam('id');

        if ($id) {
            $mensageiro = $this->negocio->find($this->repositoryName, $id);
            $mensageiro = $this->negocio->toArrayEntity($mensageiro);

            $this->getResponse()->appendBody(json_encode($mensageiro));
        }
    }

    public function postAction()
    {
        $params = $this->getAllParams();
        unset($params['module'], $params['controller'], $params['action']);

        // Define o diretório
        if (!isset($params['directory'])) {
            $params['directory'] = 'upload/';
        } else {
            // Verificar se o diretório possui barra no final
            $params['directory'] = trim($params['directory']);

            $regex = "/[\/]{1}$/";
            if (!preg_match($regex, $params['directory']))
                $params['directory'] .= '/';
        }

        if (isset($_FILES)) {
            foreach ($_FILES as $key => $file) {

                // Validar extensao
                if (isset($params['extensions'])) {
                    $params['extensions'] = array($params['extensions']);
                    $fileExtension = pathinfo($file['name'], PATHINFO_EXTENSION);

                    if (!in_array($fileExtension, $params['extensions'])) {
                        $this->getResponse()->appendBody(json_encode(array('erro' => 'O formato do arquivo é inválido.')));
                        $this->getResponse()->setHttpResponseCode(500);
                        break;
                    }
                }

                // Verifica se arquivo já existe, e se existir, gerar novo nome
                $name = pathinfo($file['name'], PATHINFO_FILENAME); // retorna somente o nome do arquivo
                $ext = pathinfo($file['name'], PATHINFO_EXTENSION); // retorna somente a extensao do arquivo, sem o ponto
                $count = 2;
                while (file_exists($params['directory'] . $file['name'])) {
                    $file['name'] = $name . '_' . $count . '.' . $ext;
                    $count++;
                }

                // Fazer upload, copiar arquivo e salvar no banco (tb_upload)
                if (move_uploaded_file($file['tmp_name'], $params['directory'] . $file['name'])) {
                    $entity = new \G2\Entity\Upload();
                    $entity->setSt_upload($file['name']);

                    $mensageiro = $this->negocio->save($entity);

                    $this->getResponse()->appendBody(json_encode($this->negocio->toArrayEntity($mensageiro)));
                    $this->getResponse()->setHttpResponseCode(200);
                    return;
                }

            }
        }

        $this->getResponse()->appendBody(json_encode(array()));
        $this->getResponse()->setHttpResponseCode(200);

    }

    public function putAction()
    {
    }

    public function deleteAction()
    {
        $id = $this->getParam('id');

        if ($id) {
            $entity = $this->negocio->find($this->repositoryName, $id);

            try {
                $this->negocio->delete($entity);
                $this->getResponse()->setHttpResponseCode(204);
            } catch (Zend_Exception $e) {
                $this->getResponse()->setHttpResponseCode(500);
            }

        }
    }

    public function headAction()
    {
    }

}
<?php

/**
 * Controller para relatorio de provas por polo
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @since  23-01-2014
 * @package application
 * @subpackage controller
 */
class Api_ProvasPoloController extends Ead1_RestRestrita {

    private $negocio;

    public function init() {
        parent::init();
        $this->negocio = new \G2\Negocio\ProvasPolo();
    }

    public function indexAction() {
        
    }

    public function getAction() {
        
    }

    public function postAction() {

    }

    public function putAction() {
        
    }

    public function deleteAction() {
 
    }

    public function headAction() {
        
    }

}

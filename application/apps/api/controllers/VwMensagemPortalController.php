<?php


/**
 * Controller API para cadastro de mensagens no gestor para o portal do aluno
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @since  19-02-2015
 * @package application
 * @subpackage controller
 */
class Api_VwMensagemPortalController extends Ead1_RestRestrita
{


    /**
     * The index action handles index/list requests; it should respond with a
     * list of the requested resources.
     */
    public function indexAction()
    {
    }

    /**
     * The get action handles GET requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function getAction()
    {
        $mensagemNegocio = new \G2\Negocio\MensagemPortal();
        $mensageiro = $mensagemNegocio->findVwMensagemPortal($this->getParam('id', null));

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $obj = $mensageiro->getFirstMensagem();
            //$obj = new \G2\Entity\VwPesquisaMensagemPortal();
            //$return = $mensagemNegocio->toArrayEntity($obj);
            $return = array(
                'id_mensagem' => $obj->getId_mensagem(),
                'st_mensagem' => $obj->getSt_mensagem(),
                'bl_importante' => $obj->getBl_importante(),
                'id_usuariocadastro' => $obj->getId_usuariocadastro(),
                'st_texto' => $obj->getSt_texto(),
                'dt_cadastromsg' => $obj->getDt_cadastromsg(),
                'id_entidade' => $obj->getId_entidade(),
                'id_situacao' => $obj->getId_situacao(),
                'id_projetopedagogico' => $obj->getId_projetopedagogico(),
                'id_areaconhecimento' => $obj->getId_areaconhecimento(),
                'id_turma' => $obj->getId_turma(),
                'id_evolucao' => $obj->getId_evolucao(),
                'dt_envio' => $obj->getDt_envio() instanceof DateTime
                    ? $obj->getDt_envio()->format('d/m/Y') : $obj->getDt_envio(),
                'dt_enviar' => $obj->getDt_enviar() instanceof DateTime
                    ? $obj->getDt_enviar()->format('d/m/Y') : $obj->getDt_enviar(),
                'st_evolucao' => $obj->getSt_evolucao(),
                'st_projetopedagogico' => $obj->getSt_projetopedagogico(),
                'st_areaconhecimento' => $obj->getSt_areaconhecimento(),
                'st_turma' => $obj->getSt_turma(),
                'dt_saida' => $obj->getDt_saida() instanceof DateTime
                    ? $obj->getDt_saida()->format('d/m/Y') : $obj->getDt_saida(),
            );
        } else {
            $this->getResponse()->setHttpResponseCode(400);
            $return = array();
        }
        echo json_encode($return);
    }

    /**
     * The head action handles HEAD requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function headAction()
    {
        // TODO: Implement headAction() method.
    }

    /**
     * The post action handles POST requests; it should accept and digest a
     * POSTed resource representation and persist the resource state.
     */
    public function postAction()
    {
        // TODO: Implement postAction() method.
    }

    /**
     * The put action handles PUT requests and receives an 'id' parameter; it
     * should update the server resource state of the resource identified by
     * the 'id' value.
     */
    public function putAction()
    {
        // TODO: Implement putAction() method.
    }

    /**
     * The delete action handles DELETE requests and receives an 'id'
     * parameter; it should update the server resource state of the resource
     * identified by the 'id' value.
     */
    public function deleteAction()
    {
        // TODO: Implement deleteAction() method.
    }


}

<?php

/**
 * Controller para gerenciar os Parametros de TCC
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @since 2014-04-25
 * @package application
 * @subpackage controller
 */
class Api_ParametroTccController extends Ead1_RestRestrita
{
    
    private $doctrineContainer;
    private $em;
    
    public function init ()
    {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
    }
    
     public function indexAction()
    {
        
    	$array = $this->_getAllParams();
    	unset($array['module'], $array['controller'], $array['action']);
    	$produto = new \G2\Negocio\ProjetoPedagogico();
    	$mensageiro = $produto->findParametrosTccProjetoPedagogico($array);
    	if($mensageiro->getTipo()==Ead1_IMensageiro::ERRO){
    		$this->getResponse()->setHttpResponseCode(400);
    		$this->_helper->json($mensageiro);
    	}elseif($mensageiro->getTipo()==Ead1_IMensageiro::AVISO){
            $this->getResponse()->setHttpResponseCode(201);
            $this->_helper->json(array());
        }else {
    		$this->getResponse()->setHttpResponseCode(201);
	    	$this->_helper->json($mensageiro->getMensagem());
    	}
    	
               
        
    }

    public function getAction()
    {
        
    	$to = new \ParametroTCCTO();
    	$to->setId_parametrotcc($this->_getParam('id'));
    	$to->fetch(true, true, true);
    	$this->_helper->json($to->toBackboneArray());
        
    }
    
    public function postAction()
    {

    	
    	try {
    	
    		$body       = $this->getRequest()->getRawBody();
    		$data       = Zend_Json::decode($body);
    	
    		$to = new \ParametroTCCTO();
    		$to->montaToDinamico($data);
    		$to->setBl_ativo(1);
    		$bo = new TCCBO();
    		$mensageiro = $bo->salvarParametroTCC($to);
    		if($mensageiro->getTipo()==\Ead1_IMensageiro::SUCESSO){
    			$mensageiro->setId($to->getId_parametrotcc());
                $mensageiro->setMensagem($to->toBackboneArray());
    		}
    	
    	} catch (Exception $exc) {
    		$mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
    	}
    	
    	if($mensageiro->getTipo()!=Ead1_IMensageiro::SUCESSO){
    		$this->getResponse()->setHttpResponseCode(400);
    		$this->_helper->json($mensageiro);
    	} else {
    		$this->getResponse()->setHttpResponseCode(201);
//    		$this->_helper->json($to->toBackboneArray());
            $this->_helper->json($mensageiro);
    	}
    	
    }
    
    public function putAction()
    {

        try {

            $body       = $this->getRequest()->getRawBody();
            $data       = Zend_Json::decode($body);

            $to = new \ParametroTCCTO();
            $to->montaToDinamico($data);
            $to->setBl_ativo(1);
            $bo = new TCCBO();
            $mensageiro = $bo->salvarParametroTCC($to);
            if($mensageiro->getTipo()==\Ead1_IMensageiro::SUCESSO){
                $mensageiro->setId($to->getId_parametrotcc());
                $mensageiro->setMensagem($to->toBackboneArray());
            }

        } catch (Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }

//        if($mensageiro->getTipo()==Ead1_IMensageiro::ERRO){
//            $this->getResponse()->setHttpResponseCode(201);
//            $this->_helper->json($mensageiro);
//        } else if($mensageiro->getTipo()==Ead1_IMensageiro::SUCESSO) {
//            $this->getResponse()->setHttpResponseCode(201);
//            $this->_helper->json($to->toBackboneArray());
//        }else{
//            $this->getResponse()->setHttpResponseCode(201);
//            $this->_helper->json($mensageiro);
//        }
        $this->getResponse()->setHttpResponseCode(201);
        $this->_helper->json($mensageiro);


    }
    
    public function deleteAction(){
        try {
            $id = intval($this->getParam('id'));
            $negocio = new \G2\Negocio\ProjetoPedagogico();
            if (!is_int($id)) {
                $this->getResponse()->setHttpResponseCode(400);
                $this->getResponse()->setBody('Não posso apagar um registro com ID inválido!');
            } else {
                $entity = $negocio->find('G2\Entity\ParametroTcc', $id);;
                $result = $negocio->delete($entity);
                $this->_helper->json(array(
                    'id' => $result->getId(),
                    'type' => 'success',
                    'title' => 'Ação bem sucedida!',
                    'text' => 'Registro ' . $entity->getSt_parametrotcc() . ' removido.'
                ));

                $this->getResponse()->setHttpResponseCode(204)->appendBody('Registro ' . $entity->getSt_parametrotcc() . ' removido');
            }
        } catch (Zend_Exception $e) {
            $this->getResponse()->setHttpResponseCode(400)->appendBody('Não foi possível apagar o registro' . $e);
        }
    }

    public function headAction(){
        
    }
    

}
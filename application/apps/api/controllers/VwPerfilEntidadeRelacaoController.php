<?php

/**
 * Controller para VwPerfilEntidadeRelacao
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class Api_VwPerfilEntidadeRelacaoController extends Ead1_RestRestrita
{

    public function init()
    {

    }

    public function indexAction()
    {
        $negocio = new \G2\Negocio\Perfil();
        $params = $this->getRequest()->getParams();
        $array = array();

        unset($params['module']);
        unset($params['action']);
        unset($params['controller']);

        foreach ($negocio->retornaVwPerfilEntidadeRelacao($params) as $key => $value) {
            $array[$key] = $negocio->toArrayEntity($value);
        }

        $this->_helper->json($array);
    }

    public function getAction()
    {
        $negocio = new \G2\Negocio\Perfil();
        $array = array();

        $where = array(
            'id_perfil' => $this->getRequest()->getParam('id')
        );

        foreach ($negocio->retornaVwPerfilEntidadeRelacao($where) as $key => $value) {
            $array[$key] = $negocio->toArrayEntity($value);
        }

        $this->_helper->json($array);
    }

    public function postAction()
    {

    }

    public function putAction()
    {

    }

    public function deleteAction()
    {

    }

    public function headAction()
    {

    }

}
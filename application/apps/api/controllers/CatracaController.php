<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CatracaController
 *
 * @author helder.silva
 */
class Api_CatracaController extends Ead1_ControllerRestrita
{
    /**
     * The index action handles index/list requests; it should respond with a
     * list of the requested resources.
     */
    private $negocio;

    public function init()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->negocio = new \G2\Negocio\Catraca();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['action'], $params['controller'], $params['module']);
        $retorno = $this->negocio->retornarDadosCatraca($params);
        $situacao = new \G2\Negocio\Situacao();
        $array = array();
        foreach ($retorno as $key => $ret) {
            $array[$key]['id'] = $ret->getId_catraca();
            $array[$key]['st_codigocatraca'] = $ret->getSt_codigocatraca();
            $sit = $situacao->getReference($ret->getId_situacao());
            $array[$key]['st_situacaocatraca'] = $sit->getSt_situacao();
            $array[$key]['id_situacao'] = $ret->getId_situacao();
        }

        \Zend_Debug::dump($array); die;

        $this->getResponse()->appendBody(json_encode($array));
    }

    /**
     * The get action handles GET requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function getAction()
    {

    }

    /**
     * The head action handles HEAD requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function headAction()
    {
        // TODO: Implement headAction() method.
    }

    /**
     * The post action handles POST requests; it should accept and digest a
     * POSTed resource representation and persist the resource state.
     */
    public function postAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
        $retorno = $this->negocio->salvaCatraca($data);
        $array = $this->negocio->toArrayEntity($retorno);
        $this->_helper->json($array);
    }

    /**
     * The put action handles PUT requests and receives an 'id' parameter; it
     * should update the server resource state of the resource identified by
     * the 'id' value.
     */
    public function putAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
        Zend_Debug::dump($data);exit;
        $result = $this->negocio->salvaCatraca($data);

    }

    /**
     * The delete action handles DELETE requests and receives an 'id'
     * parameter; it should update the server resource state of the resource
     * identified by the 'id' value.
     */
    public function deleteAction()
    {
        $params = $this->getAllParams();
        $retorno = $this->negocio->deleteCatraca($params['id']);
        $array = $this->negocio->toArrayEntity($retorno);
        $this->_helper->json($array);
    }

}
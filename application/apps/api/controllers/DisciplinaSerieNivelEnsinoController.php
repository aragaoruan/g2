<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DisciplinaSerieNivelEnsinoController
 *
 * @author kayo.silva
 */
class Api_DisciplinaSerieNivelEnsinoController extends Ead1_RestRestrita
{

    private $negocio;

    public function init ()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Disciplina();
    }

    public function deleteAction ()
    {
        
    }

    public function getAction ()
    {
        $params = $this->_getAllParams();
        unset($params['controller'], $params['module'], $params['action']);
        $result = $this->negocio->findDisciplinaSerieNivel($params);
        $arrReturn = array();
        if ($result->getType() == 'success') {
            foreach ($result->getMensagem() as $row) {
                $arrReturn[] = $row->toArray();
            }
        }
        $this->getResponse()->appendBody(json_encode($arrReturn));
        $this->getResponse()->setHttpResponseCode(200);
    }

    public function headAction ()
    {
        
    }

    public function indexAction ()
    {
        
    }

    public function postAction ()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);

        $result = $this->negocio->salvaDisciplinaSerieNivelEnsino($data);
        if ($result->getType() == 'success') {
            $this->getResponse()->appendBody(json_encode($data));
            $this->getResponse()->setHttpResponseCode(201);
        } else {
            $this->getResponse()->appendBody(json_encode($result->getText()));
            $this->getResponse()->setHttpResponseCode(400);
        }
    }

    public function putAction ()
    {
        
    }

}
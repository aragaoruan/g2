<?php
/**
 * Controller API para VwEntregaMaterial
 */

class Api_VwEntregaMaterialController extends Ead1_RestRestrita {

    protected $_negocio;

    public function init(){
        $this->_negocio = new \G2\Negocio\Material();
        parent::init();
    }

    /**
     * The index action handles index/list requests; it should respond with a
     * list of the requested resources.
     */
    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['module'], $params['action'],$params['controller']);

        $this->_negocio = new \G2\Negocio\Material();
        $result = $this->_negocio->findByVwEntregaMaterial($params);

        $array = array();
        foreach ($result as $key => $value){
            $array[$key] = $this->_negocio->toArrayEntity($value);

            if ($value->getDt_criacaopacote()) {
                $array[$key]['dt_criacaopacote'] = $value->getDt_criacaopacote()->format('d/m/Y');
            }
            if ($value->getDt_entrega()) {
                $array[$key]['dt_entrega'] = $value->getDt_entrega()->format('d/m/Y');
            }
        }

        $this->_helper->json($array);
    }

    /**
     * The get action handles GET requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function getAction()
    {

    }

}
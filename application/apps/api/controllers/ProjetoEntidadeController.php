<?php
/**
 * Controller para gerenciar o vínculo do projeto pedagógico com as entidades
 * @author Vinícius Avelino Alcântara <vinicius.alcantara@unyleya.com.br>
 * @since 2016-01-21
 * @package: application
 * @subpackage controller
 */

class Api_ProjetoEntidadeController extends Ead1_RestRestrita
{
    /** @var \G2\Negocio\ProjetoPedagogico */
    private $negocio;

    private $doctrineContainer;
    private $em;

    public function init()
    {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
        $this->negocio = new \G2\Negocio\ProjetoPedagogico();
    }

    public function indexAction()
    {
        $response = array();
        $params = array();

        if ($this->hasParam('id_projeto')) {
            $params['id_projetopedagogico'] = $this->getParam('id_projeto');
        }

        if ($this->hasParam('id_projetopedagogico')) {
            $params['id_projetopedagogico'] = $this->getParam('id_projetopedagogico');
        }

        if ($this->hasParam('id_entidade')) {
            $params['id_entidade'] = $this->getParam('id_entidade');
        }

        $results = $this->negocio->findBy('G2\Entity\ProjetoEntidade', $params);

        if ($results) {
            $response = $this->negocio->toArrayEntity($results);
        }

        return $this->_helper->json($response);
    }

    public function postAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);

        // salvar
        $result = $this->negocio->vincularProjetoEntidade($data['id_entidade'], $data['id_projeto']);
        return $this->_helper->json($result);
    }
}

<?php

/**
 * Controller para gerenciar Coleções de Livro
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @author Kayo Silva <kayo.silva@unyleya.com.br> 2014-01-23
 * @since 2013-11-28
 * @package api
 * @subpackage controller
 */
use G2\Negocio\Livro;

class Api_LivroColecaoController extends Ead1_RestRestrita
{

    /**
     *
     * @var type 
     */
    private $negocio;

    public function init ()
    {
        parent::init();
        $this->negocio = new Livro();
    }

    public function indexAction ()
    {
        $result = $this->negocio->findAllLivroColecao();
        $arrReturn = array();
        if ($result->getType() == 'success') {
            foreach ($result->getMensagem() as $row) {
                $arrReturn[] = array(
                    'id' => $row['id_livrocolecao'],
                    'id_livrocolecao' => $row['id_livrocolecao'],
                    'id_entidadecadastro' => $row['id_entidadecadastro'],
                    'id_usuariocadastro' => $row['id_usuariocadastro'],
                    'st_livrocolecao' => $row['st_livrocolecao'],
                    'bl_ativo' => $row['bl_ativo'],
                    'dt_cadastro' => $row['dt_cadastro'],
                );
            }
        }
        $this->_helper->json($arrReturn);
    }

    public function getAction ()
    {
        
    }

    public function postAction ()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);

        $result = $this->negocio->salvaLivroColecao($data);
        $arrReturn = array();
        if ($result->getType() == 'success') {
            foreach ($result->getMensagem() as $row) {
                $arrReturn = array(
                    'id' => $row->getId_livrocolecao(),
                    'id_livrocolecao' => $row->getId_livrocolecao(),
                    'id_entidadecadastro' => $row->getId_entidadecadastro(),
                    'id_usuariocadastro' => $row->getId_usuariocadastro(),
                    'st_livrocolecao' => $row->getSt_livrocolecao(),
                    'bl_ativo' => $row->getBl_ativo(),
                    'dt_cadastro' => $row->getDt_cadastro(),
                );
            }
            $this->getResponse()->setHttpResponseCode(201)->appendBody(json_encode($arrReturn));
        } else {
            $this->getResponse()->setHttpResponseCode(400)->appendBody(json_encode($result->getText()));
        }
    }

    public function putAction ()
    {

        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);

        $result = $this->negocio->salvaLivroColecao($data);

        if ($result->getType() == 'success') {
            $data['id'] = $data['id_livrocolecao'];
            $this->getResponse()->appendBody(json_encode($data));
            $this->getResponse()->setHttpResponseCode(201);
        } else {
            $this->getResponse()->appendBody(json_encode($result->getText()));
            $this->getResponse()->setHttpResponseCode(400);
        }
    }

    public function deleteAction ()
    {
        $id = intval($this->getParam('id'));
        if (!is_int($id)) {
            $this->getResponse()->setHttpResponseCode(400);
            $this->getResponse()->setBody('Não posso apagar um registro com ID inválido!');
        } else {
            Zend_Debug::dump($id);
            $result = $this->negocio->deleteLivroColecao($id);
            if ($result->getType() == 'success') {
                $this->getResponse()->setHttpResponseCode(204)->appendBody('Registro removido');
            } else {
                $this->getResponse()->setHttpResponseCode(400)->appendBody($result->getMensagem());
            }
        }
    }

    public function headAction ()
    {
        
    }

}
<?php

/**
 * Controller para gerenciar VwItemAtivacaoEntidade
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2015-01-01
 * @package application
 * @subpackage controller
 */

class Api_VwItemAtivacaoEntidadeController extends Ead1_RestRestrita {

    private $negocio;
    private $repositoryName = '\G2\Entity\VwItemAtivacaoEntidade';

    public function init ()
    {
        parent::init();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->negocio = new \G2\Negocio\ItemAtivacaoEntidade();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        $orderBy = $this->getParam('order_by') ? array($this->getParam('order_by') => $this->getParam('sort') ? $this->getParam('sort') : 'ASC') : null;
        unset($params['module'], $params['controller'], $params['action'], $params['order_by'], $params['sort']);

        $mensageiro = $this->negocio->findBy($this->repositoryName, $params, $orderBy);
        $results = array();
        foreach ($mensageiro as $entity) {
            $results[] = $this->negocio->toArrayEntity($entity);
        }

        $this->_helper->json($results);
    }

    public function getAction()
    {
        $id = $this->getParam('id');

        if ($id) {
            $mensageiro = $this->negocio->find($this->repositoryName, $id);
            $mensageiro = $this->negocio->toArrayEntity($mensageiro);

            $this->_helper->json($mensageiro);
        }
    }

    public function postAction()
    {
        $data = Zend_Json::decode($this->getRequest()->getRawBody());

        $negocio = new G2\Negocio\ItemAtivacaoEntidade();
        $mensageiro = $negocio->save($data);

        // Trazer o resultado como VW
        if (isset($mensageiro->type) && $mensageiro->type == 'success' && $mensageiro->codigo) {
            $entity = $negocio->findOneBy('\G2\Entity\VwItemAtivacaoEntidade', array(
                'id_itemativacaoentidade' => $mensageiro->codigo
            ));
            $mensageiro->mensagem = $negocio->toArrayEntity($entity);
        }

        $this->_helper->json($mensageiro);
    }

    public function putAction()
    {
        $this->postAction();
    }

    public function deleteAction()
    {
//        $id = $this->getParam('id');
//
//        if ($id) {
//            $entity = $this->negocio->find($this->repositoryName, $id);
//
//            $mensageiro = $this->negocio->delete($entity);
//            $this->_helper->json($mensageiro);
//        }
    }

}
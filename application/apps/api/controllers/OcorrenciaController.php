<?php

/**
 * Description of OcorrenciaController
 *
 * @author rafael.bruno
 */
class Api_OcorrenciaController extends Ead1_RestRestrita
{

    /**
     * @var \G2\Negocio\Ocorrencia
     */
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Ocorrencia();
    }

    /**
     * Action Api responsavel por retornar ocorrencias atravez do formulario de consulta
     */
    public function indexAction()
    {
        try {
            $params = $this->getRequest()->getParam('paramsSearch');

            $limit = (int)$this->getParam('per_page');
            $offset = (intval($this->getParam('page')) - 1) * $limit;

            $grid = $this->negocio->findByOcorrenciaPessoa($params, $limit, $offset, true);

            // Verificar se retornou nenhum resultado e retorna um json vazio
            if (!$grid) {
                $this->getResponse()->setHttpResponseCode(404)->appendBody(json_encode([]));
            } else {
                $this->getResponse()->setHttpResponseCode(200)->appendBody(json_encode($grid));
            }
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(500)->setBody($e->getMessage());
        }
    }

    public function getAction()
    {
        $id = intval($this->getParam('id'));
        $this->_helper->json($this->negocio->findOcorrencia($id));
    }

    /**
     * Action Api responsavel por criar nova ocorrencia
     */
    public function postAction()
    {
        $id_usuario = $this->negocio->sessao->id_usuario;
        $id_entidade = $this->negocio->sessao->id_entidade;

        //$data = Zend_Json_Decoder::decode($json);
        $data = $this->getRequest()->getPost();
        if (array_key_exists('graduacao', $data) && $data['graduacao']) {
            $data['ocorrencia'] = Zend_Json::decode($data['ocorrencia']);
        } else {
            $data['ocorrencia']['id_usuariointeressado'] = $id_usuario;
        }
        $data['ocorrencia']['id_usuariocadastro'] = $id_usuario;
        $data['ocorrencia']['id_entidade'] = $id_entidade;


        //Verificando se um arquivo foi enviano na interacao
        $arquivo = array_key_exists('arquivo', $_FILES) ? $_FILES['arquivo'] : null;

        if ($arquivo)
            $data['arquivo'] = $arquivo;

        $form = new OcorrenciaForm();

        if ($form->isValid($data['ocorrencia'])) {

            if (array_key_exists('ocorrencia_vinculada', $data['ocorrencia']) &&
                $data['ocorrencia']['ocorrencia_vinculada']) {
                $tramiteOcorrencia = true;
                unset($data['ocorrencia']['ocorrencia_vinculada']);
            }

            $entity = $this->negocio->salvarOcorrencia($data);

            $idOcorrencia = $entity->getFirstMensagem()->getId_ocorrencia() ?
                $entity->getFirstMensagem()->getId_ocorrencia() :
                false;

            //verificando se é uma instancia do mensageiro.
            if ($entity instanceof Ead1_Mensageiro) {
                //verifica se o que veio no mensageiro é uma entity de ocorrencia e faz o parse
                if ($entity->getFirstMensagem() instanceof \G2\Entity\Ocorrencia) {
                    //busca os dados da vw_ocorrencia, combinado fazer assimz na daily dia 30 de set 2016
                    $ocorrencia = $this->negocio->retornaVwOcorrencia($entity->getFirstMensagem()->getId_ocorrencia());
                    $entity->setMensageiro($this->negocio->toArrayEntity($ocorrencia));
                }

                if ($tramiteOcorrencia) {

                    $ocorrenciaPai = $this->negocio->findOcorrencia($data['ocorrencia']['id_ocorrenciaoriginal']);

                    $ocorrenciaPai['id_evolucao'] = \G2\Constante\Evolucao::AGUARDANDO_OUTROS_SETORES;
                    $ocorrenciaPai = $this->negocio->salvarOcorrencia($ocorrenciaPai);

                    if ($idOcorrencia) {
                        $msgOcorrencia = 'Ocorrencia vinculada id: ' . $idOcorrencia;
                        $msgVinculada = 'Ocorrência gerada a partir da ocorrência Id ' .
                            $data['ocorrencia']['id_ocorrenciaoriginal'];

                        //Salvando tramite da ocorrencia original o ID da ocorrencia vinculada
                        $this->negocio->tramiteOcorrenciaVinculada(
                            $data['ocorrencia']['id_ocorrenciaoriginal'],
                            $msgOcorrencia
                        );

                        //Salvando tramite da ocorrencia vinculada ID da ocorrencia original
                        $this->negocio->tramiteOcorrenciaVinculada(
                            $idOcorrencia,
                            $msgVinculada
                        );
                    }
                }

                $this->_helper->json($entity->getFirstMensagem());
            }

        } else {
            $erros = $this->view->messages($form)->toString();
            $this->getResponse()->setHttpResponseCode(500)->setBody(json_encode($erros));
        }
    }

    /**
     * Action Api responsavel por fazer diversas alteracoes em ocorrencia,
     * metodo construido neste formato para reutilizacao de BO
     */
    public function putAction()
    {
        $json = $this->getRequest()->getRawBody();
        $form = new OcorrenciaForm();
        $data = Zend_Json_Decoder::decode($json);
        try {
            switch ($data['tipo_update']) {
                case 1:
                    //update comum
                    if ($form->isValid($data)) {
                        $entity = $this->negocio->salvarOcorrencia($data);
                        if ($entity instanceof Ead1_Mensageiro) {
                            $entity = $entity->getFirstMensagem();

                            //verifica se o que veio no mensageiro é uma entity de ocorrencia e faz o parse
                            if ($entity instanceof \G2\Entity\Ocorrencia) {
                                $toArray = $this->negocio->toArrayEntity($entity);

                                $toArray['id_entidadematricula'] = null;
                                $toArray['st_entidadematricula'] = null;

                                if ($entity->getId_matricula()) {
                                    $entidade = $this->negocio->find('\G2\Entity\Entidade', $entity->getId_matricula()->getId_entidadematricula());
                                    $toArray['id_entidadematricula'] = $entidade->getId_entidade();
                                    $toArray['st_entidadematricula'] = $entidade->getSt_nomeentidade();
                                }

                                $this->_helper->json($toArray);
                            }
                        }
                    }
                    break;

                case 2:
                    //update encerramento
                    $ocorrencia = new G2\Negocio\Ocorrencia();
                    $updateOcorrenciaVinculada = $ocorrencia->updateOcorrenciaVinculada(
                        $data['id_ocorrencia']
                    );

                    if ($updateOcorrenciaVinculada != false) {
                        $this->_helper->json($updateOcorrenciaVinculada);
                    } else {
                        $entity = $ocorrencia->encerrarOcorrencia($data);
                        $toArray = $ocorrencia->toArrayEntity($entity);
                        $this->_helper->json($toArray);
                    }
                    break;

                case 3:
                    //update devolver ocorrencia
                    $ocorrencia = new G2\Negocio\Ocorrencia();
                    $entity = $ocorrencia->devolverOcorrencia($data);
                    $toArray = $ocorrencia->toArrayEntity($entity);
                    $this->_helper->json($toArray);
                    break;

                case 4:
                    //update reabrir ocorrencia
                    $ocorrencia = new G2\Negocio\Ocorrencia();
                    $entity = $ocorrencia->reabrirOcorrencia($data);
                    $toArray = $ocorrencia->toArrayEntity($entity);
                    $this->_helper->json($toArray);
                    break;

                case 5:
                    //update solicitar encerramento
                    $ocorrencia = new G2\Negocio\Ocorrencia();
                    $entity = $ocorrencia->solicitarEncerramento($data);
                    $toArray = $ocorrencia->toArrayEntity($entity);
                    $this->_helper->json($toArray);
                    break;

                case 6:
                    //update encaminhar ocorrencia
                    $ocorrencia = new G2\Negocio\Ocorrencia();
                    $entity = $ocorrencia->encaminharOcorrencia($data);
                    $toArray = $ocorrencia->toArrayEntity($entity);
                    $this->_helper->json($toArray);
                    break;
                default:
                    return 'Tipo de alteração incorreto';
            }
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(400)->setBody($e->getMessage());
        }
    }

}

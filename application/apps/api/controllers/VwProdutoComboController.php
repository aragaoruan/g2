<?php

/**
 * Controller para gerenciar os VwProdutos
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @since 2013-12-03
 * @package application
 * @subpackage controller
 */
class Api_VwProdutoComboController extends Ead1_RestRestrita
{
    
    private $doctrineContainer;
    private $em;
    
    public function init ()
    {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
    }
    
     public function indexAction()
    {
    	
    	$array = $this->_getAllParams();
    	unset($array['module'], $array['controller'], $array['action']);
    	
        $VwProduto = new \G2\Negocio\Produto();
        $mensageiro = $VwProduto->findVwProdutoComboByArray($array);
        
        if($mensageiro->getTipo()==Ead1_IMensageiro::SUCESSO){
        	$mensageiro->setText(count($mensageiro->getMensagem()).' Produtos encontrados neste Combo');
        }
        
        $this->getResponse()->appendBody(json_encode($mensageiro));
        
    }

    public function getAction()
    {
        
        $VwProduto = new \G2\Negocio\Produto();
        $mensageiro = $VwProduto->findVwProdutoComboByArray(array('id_produtocombo'=>$this->_getParam('id', null)));
        
        if($mensageiro->getTipo()==Ead1_IMensageiro::SUCESSO){
            $this->getResponse()
                    ->appendBody(json_encode($mensageiro->getFirstMensagem()));
            
        }
        
        
    }
    
    public function postAction()
    {
    }
    
    public function putAction()
    {
       
    }
    
    public function deleteAction(){
        
    }

    public function headAction(){
        
    }
    

}
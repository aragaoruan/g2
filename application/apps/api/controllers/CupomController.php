<?php

/**
 * Controller modolo API para gerenciar categoria
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-03-11
 */
class Api_CupomController extends Ead1_RestRestrita
{
    /**
     * @var G2\Negocio\Cupom
     */
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new G2\Negocio\Cupom();
    }

    public function deleteAction()
    {

    }

    public function getAction()
    {

    }

    public function headAction()
    {

    }

    public function indexAction()
    {

    }

    public function postAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
        $result = $this->negocio->procedimentoSalvarCupom($data);
        $arrResult = array();
        if ($result instanceof \G2\Entity\Cupom) {
            $arrResult = array(
                'id' => $result->getId_cupom(),
                'id_cupom' => $result->getId_cupom(),
                'st_prefixo' => $result->getSt_prefixo(),
                'st_complemento' => $result->getSt_complemento(),
                'st_codigocupom' => $result->getSt_codigocupom(),
                'nu_desconto' => $result->getNu_desconto(),
                'id_tipodesconto' => $result->getId_tipodesconto() ? $result->getId_tipodesconto()->getId_tipodesconto() : NULL,
                'id_usuariocadastro' => $result->getId_usuariocadastro() ? $result->getId_usuariocadastro()->getId_usuario() : NULL,
                'id_campanhacomercial' => $result->getId_campanhacomercial() ? $result->getId_campanhacomercial()->getId_campanhacomercial() : NULL,
                'dt_inicio' => $result->getDt_inicio() ? $result->getDt_inicio()->format('Y-m-d H:i:s') : NULL,
                'dt_fim' => $result->getDt_fim() ? $result->getDt_fim()->format('Y-m-d H:i:s') : NULL,
                'dt_cadastro' => $result->getDt_cadastro() ? $result->getDt_cadastro()->format("Y-m-d H:i:s") : NULL,
                'bl_unico' => $result->getBl_unico(),
                'bl_ativo' => $result->getBl_ativo(),
                'bl_ativacao' => $result->getBl_ativacao()
            );
            $this->getResponse()->appendBody(json_encode($arrResult));
            $this->getResponse()->setHttpResponseCode(201);
        } else {
            $this->getResponse()->appendBody(json_encode($result));
            $this->getResponse()->setHttpResponseCode(400);
        }
    }

    public function putAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
        $result = $this->negocio->procedimentoSalvarCupom($data);
        $arrResult = array();
        if ($result instanceof \G2\Entity\Cupom) {
            $arrResult = array(
                'id' => $result->getId_cupom(),
                'id_cupom' => $result->getId_cupom(),
                'st_prefixo' => $result->getSt_prefixo(),
                'st_complemento' => $result->getSt_complemento(),
                'st_codigocupom' => $result->getSt_codigocupom(),
                'nu_desconto' => $result->getNu_desconto(),
                'id_tipodesconto' => $result->getId_tipodesconto() ? $result->getId_tipodesconto()->getId_tipodesconto() : NULL,
                'id_usuariocadastro' => $result->getId_usuariocadastro() ? $result->getId_usuariocadastro()->getId_usuario() : NULL,
                'id_campanhacomercial' => $result->getId_campanhacomercial() ? $result->getId_campanhacomercial()->getId_campanhacomercial() : NULL,
                'dt_inicio' => $result->getDt_inicio() ? $result->getDt_inicio()->format('Y-m-d H:i:s') : NULL,
                'dt_fim' => $result->getDt_fim() ? $result->getDt_fim()->format('Y-m-d H:i:s') : NULL,
                'dt_cadastro' => $result->getDt_cadastro(),
                'bl_unico' => $result->getBl_unico(),
                'bl_ativo' => $result->getBl_ativo(),
                'bl_ativacao' => $result->getBl_ativacao()
            );
            $this->getResponse()->appendBody(json_encode($arrResult));
            $this->getResponse()->setHttpResponseCode(201);
        } else {
            $this->getResponse()->appendBody(json_encode($result));
            $this->getResponse()->setHttpResponseCode(400);
        }
    }

}

<?php

/**
 * Controller para gerenciar as Situações
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @since 2014-06-18
 * @package application
 * @subpackage controller
 */
class Api_ModeloVendaController extends Ead1_RestRestrita
{
    
    private $doctrineContainer;
    private $em;
    
    public function init ()
    {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
    }
    
     public function indexAction()
    {
        
        $parametros = array();
        $st_modelovenda = $this->_getParam('st_modelovenda', false); 
        if($st_modelovenda){
           $parametros['st_modelovenda'] = $st_modelovenda;
        } 

        
        $retorno = array();
        $negocio = new \G2\Negocio\Produto();
        $modelos = $negocio->findByModeloVenda($parametros);
        if($modelos){
        	foreach($modelos as $modelo){
        		$retorno[] = $modelo->toBackboneArray();
        	}
        }
        
        $this->_helper->json($retorno);
        
        
    }

    public function getAction()
    {
        
        
    }
    
    public function postAction()
    {
    }
    
    public function putAction()
    {

    }
    
    public function deleteAction(){
        
    }

    public function headAction(){
        
    }
    

}
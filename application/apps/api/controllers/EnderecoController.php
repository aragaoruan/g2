<?php

/**
 * Controller para Endereco
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @package api
 * @subpackage controller
 * @since 2013-11-18
 */
use G2\Negocio\Endereco;

class Api_EnderecoController extends Ead1_RestRestrita
{

    private $_negocio;

    public function init ()
    {
        parent::init();
        $this->_negocio = new Endereco();
    }


    public function indexAction ()
    {
        $params = $this->getAllParams();

        if (isset($params['id_entidade'])) {
            $negocio = new Endereco();

            $resultEnderecos = $negocio->retornaEnderecos($params['id_entidade']);

            $this->getResponse()->appendBody(json_encode($resultEnderecos));
        }
    }

    public function postAction ()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
    }


}
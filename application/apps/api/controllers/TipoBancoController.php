<?php
use G2\Negocio\Banco;

/**
 * Controller para Endereco
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @package api
 * @subpackage controller
 * @since 2013-11-18
 */
class Api_TipoBancoController extends Ead1_RestRestrita
{

    private $_negocio;

    public function init()
    {
        parent::init();
        $this->_negocio = new Banco();
    }

    public function deleteAction()
    {

    }

    public function getAction()
    {

    }

    public function headAction()
    {

    }

    public function indexAction()
    {
        $result = $this->_negocio->retornaBancos();
        $this->getResponse()->appendBody(json_encode($result));
    }

    public function postAction()
    {

    }

    public function putAction()
    {

    }

}
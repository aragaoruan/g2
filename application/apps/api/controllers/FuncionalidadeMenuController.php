<?php

/**
 * Controller para gerenciar Funcionalidade
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2015-03-19
 * @package application
 * @subpackage controller
 */

class Api_FuncionalidadeMenuController extends Ead1_RestRestrita {

    private $negocio;
    private $repositoryName = '\G2\Entity\Funcionalidade';

    public function init ()
    {
        parent::init();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->negocio = new \G2\Negocio\Funcionalidade();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        $orderBy = $this->getParam('order_by') ? array($this->getParam('order_by') => $this->getParam('sort') ? $this->getParam('sort') : 'ASC') : null;
        unset($params['module'], $params['controller'], $params['action'], $params['order_by'], $params['sort']);

        $mensageiro = $this->negocio->findBy($this->repositoryName, $params, $orderBy);
        $results = array();
        foreach ($mensageiro as $entity) {
            $results[] = $this->negocio->toArrayEntity($entity);
        }

        $this->_helper->json($results);
    }

    public function getAction()
    {
        $id = $this->getParam('id');

        if ($id) {
            $mensageiro = $this->negocio->find($this->repositoryName, $id);
            $mensageiro = $this->negocio->toArrayEntity($mensageiro);

            $this->_helper->json($mensageiro);
        }
    }

    public function postAction()
    {
        $data = Zend_Json::decode($this->getRequest()->getRawBody());

        $mensageiro = $this->negocio->save($data);
        $this->_helper->json($mensageiro);
    }

    public function putAction()
    {
        $this->postAction();
    }

    public function deleteAction()
    {
//        $id = $this->getParam('id');
//
//        if ($id) {
//            $entity = $this->negocio->find($this->repositoryName, $id);
//
//            $mensageiro = $this->negocio->delete($entity);
//            $this->_helper->json($mensageiro);
//        }
    }

}
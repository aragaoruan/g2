<?php

/**
 * Controller para VwAvaliacaoConjuntoReferencia
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @since 2014-03-14
 * @package application
 * @subpackage controller
 */

class Api_VwAvaliacaoConjuntoReferenciaController extends Ead1_RestRestrita {

    private $doctrineContainer;
    private $em;

    public function init() {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
    }

    public function indexAction() {
        $params = $this->getRequest()->getParams();
        $bo = new \G2\Negocio\VwAvaliacaoConjunto();
        unset($params['module']);
        unset($params['action']);
        unset($params['controller']);
//        Zend_Debug::dump($params);die;
        $mensageiro = $bo->findVwAvaliacaoConjuntoReferencia($params);

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $mensageiro->setText(count($mensageiro->getMensagem()) . ' registros encontrados!');
            $retorno = $mensageiro->getMensagem();
        } else {
            $retorno = array();
        }

        $this->getResponse()
            ->appendBody(json_encode($retorno));
    }

    public function getAction() {

    }

    public function postAction() {
        
    }

    public function putAction() {
        
    }

    public function deleteAction() {
        
    }

    public function headAction() {
        
    }

}

<?php

/**
 * Class Api_PerguntaController
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Api_PerguntaController extends Ead1_RestRestrita
{
    private $_negocio;

    public function init()
    {
        parent::init();
        $this->_negocio = new \G2\Negocio\PerguntasFrequentes();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);
        $result = $this->_negocio->retornarPerguntas($params, true);
        $this->getResponse()
            ->appendBody(json_encode($result));
    }

    public function getAction()
    {
    }

    public function headAction()
    {

    }

    public function postAction()
    {

    }

    public function putAction()
    {
    }

    public function deleteAction()
    {
        $id = intval($this->getParam('id'));
        if (!is_int($id)) {
            $this->getResponse()->setHttpResponseCode(400)->setBody('Não posso apagar um registro com ID inválido!');
        } else {
            $data['id_pergunta'] = $id;
            $data['bl_ativo'] = false;
            $result = $this->_negocio->salvarPergunta($data);
            if ($result) {
                $this->getResponse()->setHttpResponseCode(204)->appendBody('Registro removido');
            } else {
                $this->getResponse()->setHttpResponseCode(400)->appendBody('Erro ao tentar remover registro');
            }
        }
    }
}
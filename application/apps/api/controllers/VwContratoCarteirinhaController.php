<?php

/**
 * Class Api_VwContratoCarteirinhaController
 * Controller rest para vw
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2015-01-02
 */
class Api_VwContratoCarteirinhaController extends Ead1_RestRestrita
{
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Contrato();
    }


    public function deleteAction()
    {

    }

    public function getAction()
    {

    }

    public function headAction()
    {

    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['module'], $params['action'], $params['controller']);

        $result = $this->negocio->retornarDadosVwContratoCarteirinha($params);

        if ($result) {
            foreach ($result as $key => $row) {
                $result[$key] = $this->negocio->toArrayEntity($row);
            }
        }
        $this->getResponse()
            ->appendBody(json_encode($result));
    }

    public function postAction()
    {
    }

    public function putAction()
    {

    }
} 
<?php
/**
 * Controller para gerenciar VwNucleoPessoaCoController.php
 * @author Marcus Pereira <marcus.pereira@unyleya.com.br>
 * @since 2015-09-04
 * @package application
 * @subpackage controller
 */

Class Api_VwNucleoPessoaCoController extends Ead1_RestRestrita {


    public function init ()
    {
        parent::init();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->negocio = new \G2\Negocio\NucleoPessoaCo();
    }


    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['module'], $params['controller'], $params['action']);

        $mensageiro = new \Ead1_Mensageiro();

        $results = $this->negocio->findVwByAssunto($params);
        if ($results) {
            foreach ($results as $key => &$entity) {
                $entity = $this->negocio->toArrayEntity($entity);
            }

            $mensageiro->setMensageiro($results, \Ead1_IMensageiro::SUCESSO);
        } else {
            $mensageiro->setMensageiro(array(), \Ead1_IMensageiro::ERRO);
        }

        $this->_helper->json($mensageiro);
    }

}
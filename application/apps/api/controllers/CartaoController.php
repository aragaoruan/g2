<?php

/**
 * Controller para Pessoa Jurídica
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 * @package default
 * @subpackage controller
 * @since 2013-12-09
 */
class Api_CartaoController extends Ead1_RestRestrita
{

    private $_negocio;

    public function init()
    {
        parent::init();
        $this->_negocio = new \G2\Negocio\Cartao();
    }

    public function indexAction()
    {
        $id_entidade = $this->getParam('id_entidade');
        $id_sistema = $this->getParam('id_sistema', null);
        $id_cartaobandeira = $this->getParam('id_cartaobandeira', null);

        $params = array('id_entidade' => $id_entidade);

        if (!empty($id_sistema)){
            $params['id_sistema'] = $id_sistema;
        }

        if (!empty($id_cartaobandeira)){
            $params['id_cartaobandeira'] = $id_cartaobandeira;
        }

        $retorno = $this->_negocio->findByVwEntidadeCartao($params);

        $arrayResult = array();
        foreach ($retorno as $r) {
            $arrayResult[] = $this->_negocio->toArrayEntity($r);
        }

        $this->_helper->json($arrayResult);
    }

    public function deleteAction()
    {

    }

    public function getAction()
    {

    }

    public function headAction()
    {

    }

    public function postAction()
    {

    }

    public function putAction()
    {

    }
}
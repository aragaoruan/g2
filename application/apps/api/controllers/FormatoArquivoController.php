<?php

/**
 * Controller para gerenciar os Formatos de Arquivos
 * @author Rafael Leite <rafael.leite@unyleya.com.br>
 * @since 2016-09-29
 * @package application
 * @subpackage controller
 */
class Api_FormatoArquivoController extends Ead1_RestRestrita
{

    private $doctrineContainer;
    private $em;

    public function init()
    {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
    }

    public function indexAction()
    {

        $negocio = new \G2\Negocio\Negocio();

        $result = $negocio->findAll('\G2\Entity\FormatoArquivo');
        $retorno = array();
        foreach($result as $key=>$value){
            $retorno[$key]['id_formatoarquivo'] = $value->getId_formatoarquivo();
            $retorno[$key]['st_formatoarquivo'] = $value->getSt_formatoarquivo();
        }
//        Zend_Debug::dump($retorno);die();
        $this->_helper->json($retorno);
    }

    public function getAction()
    {

    }

    public function postAction()
    {
    }

    public function putAction()
    {

    }

    public function deleteAction(){

    }

    public function headAction(){

    }

}
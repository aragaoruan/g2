<?php

/**
 * Controller para gerenciar os produtos combo
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @since 2013-12-05
 * @package application
 * @subpackage controller
 */
class Api_ProdutoComboController extends Ead1_RestRestrita
{
    
    private $doctrineContainer;
    private $em;
    
    public function init ()
    {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
    }
    
     public function indexAction()
    {
        
        
    }

    public function getAction()
    {
        
        $produto = new \G2\Negocio\Produto();
        $mensageiro = $produto->findProdutoComboById($this->_getParam('id', null));
        
        if($mensageiro->getTipo()==Ead1_IMensageiro::SUCESSO){
	        die(json_encode($mensageiro->getFirstMensagem()));
        }
        
    }
    
    public function postAction()
    {
    	
    	try {
    	
    		$body       = $this->getRequest()->getRawBody();
    		$data       = Zend_Json::decode($body);
    		$produto   = new \G2\Negocio\Produto();
    	
    		$mensageiro = $produto->salvarProdutoCombo($data);
    	
    	} catch (Exception $exc) {
    		$mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
    	}
    	
    	if($mensageiro->getTipo()==Ead1_IMensageiro::SUCESSO){
    		$this->getResponse()->setHttpResponseCode(201);
    		$this->_helper->json($mensageiro->getFirstMensagem());
    	} else {
    		$this->getResponse()->setHttpResponseCode(400);
    		$this->_helper->json($mensageiro);
    	}
    	
    }
    
    public function putAction()
    {
    	
        try {
    	
    		$body       = $this->getRequest()->getRawBody();
    		$data       = Zend_Json::decode($body);
    		$produto   = new \G2\Negocio\Produto();
    	
    		$mensageiro = $produto->salvarProdutoCombo($data);
    	
    	} catch (Exception $exc) {
    		$mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
    	}
    	
    	if($mensageiro->getTipo()==Ead1_IMensageiro::SUCESSO){
    		$this->getResponse()->setHttpResponseCode(201);
    		$this->_helper->json($mensageiro->getFirstMensagem());
    	} else {
    		$this->getResponse()->setHttpResponseCode(400);
    		$this->_helper->json($mensageiro);
    	}

    }
    
    public function deleteAction(){
    	try {
    	
    		$body       = $this->getRequest()->getRawBody();
    		$data       = Zend_Json::decode($body);
    		$produto   = new \G2\Negocio\Produto();
    	
    		die(json_encode($produto->excluirProdutoCombo($this->_getParam('id', null))));
    	
    	} catch (Exception $exc) {
    		die(json_encode(new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO)));
    	}
    	
    }

    public function headAction(){
        
    }
    

}
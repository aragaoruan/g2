<?php

/**
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since  2014-19-10
 * @package Api
 * @subpackage controller
 */
class Api_VwEntidadeCartaoController extends Ead1_RestRestrita
{

    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Entidade();
    }

    public function deleteAction()
    {
        $id = intval($this->getParam('id'));
        if (!is_int($id)) {
            $this->getResponse()->setHttpResponseCode(400)->setBody('Não posso apagar um registro com ID inválido!');
        } else {
            $this->getResponse()->setHttpResponseCode(400)->appendBody('Não implementado');
        }
    }

    public function getAction()
    {
        $params = $this->getAllParams();

        unset($params['controller'], $params['action'], $params['module']);
        $result = $this->negocio->retornaVwEntidadeCartao($params);
        if ($result) {
            foreach ($result as $i => $row) {
                $result[$i] = $this->negocio->toArrayEntity($row);
                $result[$i]['id'] = $row->getId_cartaoconfig();
            }
        }


        $this->getResponse()
            ->appendBody(json_encode($result));
    }

    public function headAction()
    {

    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);

        $result = $this->negocio->retornaVwEntidadeCartao($params);
        if ($result) {
            foreach ($result as $i => $row) {
                $result[$i] = $this->negocio->toArrayEntity($row);
                $result[$i]['id'] = $row->getId_cartaoconfig();
            }
        }

        $this->getResponse()
            ->appendBody(json_encode($result));
    }

    public function postAction()
    {
    }

    public function putAction()
    {
    }

}

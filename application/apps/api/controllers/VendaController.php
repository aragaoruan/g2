<?php

/**
 * Description of VendaController
 *
 * @author rafaelbruno
 */
class Api_VendaController extends Ead1_RestRestrita
{

    private $_negocio;

    public function init()
    {
        parent::init();
        $this->_negocio = new \G2\Negocio\Venda();
    }

    public function deleteAction()
    {
        $id = intval($this->getParam('id'));
        if (!is_int($id)) {
            $this->getResponse()->setHttpResponseCode(400)->setBody('Não posso apagar um registro com ID inválido!');
        } else {
            $data['id_venda'] = $id;
            $data['bl_ativo'] = false;

            $result = $this->_negocio->salvarVenda($data);
            if ($result) {
                $this->getResponse()->setHttpResponseCode(204)->appendBody('Registro removido');
            } else {
                $this->getResponse()->setHttpResponseCode(400)->appendBody('Erro ao tentar remover registro');
            }
        }
    }

    public function getAction()
    {
        try {
            $response = array();

            $result = $this->_negocio->find('\G2\Entity\Venda', $this->getParam('id'));

            if ($result instanceof \G2\Entity\Venda) {
                $ng_vendagraduacao = new \G2\Negocio\VendaGraduacao();
                $response = $ng_vendagraduacao->retornaVenda($result);
            }

            $this->_helper->json($response ?: (new stdClass()));
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(500)->appendBody($e->getMessage());
        }
    }

    public function headAction()
    {

    }

    public function indexAction()
    {

    }

    public function postAction()
    {

    }

    public function putAction()
    {
        $json = $this->getRequest()->getRawBody();
        $post = Zend_Json::decode($json);
        $entity = $this->_negocio->salvarVenda($post);

        if ($entity) {
            $result = array(
                'id_venda' => $entity->getIdVenda(),
                'dt_cadastro' => $entity->getDtCadastro(),
                'nu_descontoporcentagem' => $entity->getNuDescontoporcentagem(),
                'nu_descontovalor' => $entity->getNuDescontovalor(),
                'nu_juros' => $entity->getNuJuros(),
                'bl_ativo' => $entity->getBlAtivo(),
                'id_usuariocadastro' => $entity->getIdUsuariocadastro(),
                'nu_valorliquido' => $entity->getNuValorliquido(),
                'nu_valorbruto' => $entity->getNuValorbruto(),
                'id_entidade' => $entity->getIdEntidade(),
                'nu_parcelas' => $entity->getNuParcelas(),
                'bl_contrato' => $entity->getBlContrato(),
                'nu_diamensalidade' => $entity->getNuDiamensalidade(),
                'id_origemvenda' => $entity->getIdOrigemvenda(),
                'dt_agendamento' => $entity->getDtAgendamento(),
                'dt_confirmacao' => $entity->getDtConfirmacao(),
                'st_observacao' => $entity->getStObservacao(),
                'nu_valoratualizado' => $entity->getNuValoratualizado(),
                'recorrete_orderid' => $entity->getRecorreteOrderid(),
                'id_formapagamento' => $entity->getIdFormapagamento() ? $entity->getIdFormapagamento()->getId_formapagamento() : NULL,
                'id_campanhacomercial' => $entity->getIdCampanhacomercial() ? $entity->getIdCampanhacomercial()->getId_campanhacomercial() : NULL,
                'id_evolucao' => $entity->getIdEvolucao() ? $entity->getIdEvolucao()->getId_evolucao() : NULL,
                'id_situacao' => $entity->getIdSituacao() ? $entity->getIdSituacao()->getId_situacao() : NULL,
                'id_usuario' => $entity->getIdUsuario() ? $entity->getIdUsuario()->getId_usuario() : NULL,
                'id_tipocampanha' => $entity->getIdTipocampanha() ? $entity->getIdTipocampanha()->getId_tipocampanha() : NULL,
                'id_prevenda' => $entity->getIdPrevenda(),
                'id_protocolo' => $entity->getIdProtocolo() ? $entity->getIdProtocolo()->getIdProtocolo() : NULL,
                'id_enderecoentrega' => $entity->getIdEnderecoentrega() ? $entity->getIdEnderecoentrega()->getId_endereco() : NULL,
                'id_ocorrencia' => $entity->getIdOcorrencia(),
                'id_cupom' => $entity->getIdCupom() ? $entity->getIdCupom()->getId_cupom() : NULL,
                'id_atendente' => $entity->getIdAtendente() ? $entity->getIdAtendente()->getId_usuario() : NULL,
                'id_ocorrenciaaproveitamento' => $entity->getId_ocorrenciaaproveitamento() ? $entity->getId_ocorrenciaaproveitamento()->getId_ocorrencia() : NULL
            );

            $this->getResponse()->appendBody(json_encode($result));
            $this->getResponse()->setHttpResponseCode(201);
        } else {
            $this->getResponse()->appendBody(json_encode("Erro ao salvar venda"));
            $this->getResponse()->setHttpResponseCode(400);
        }

    }

}

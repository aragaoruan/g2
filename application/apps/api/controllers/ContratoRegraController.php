<?php

/**
 * Controller para gerenciar as regras de contrato
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @since 2013-10-07
 * @package application
 * @subpackage controller
 */
class Api_ContratoRegraController extends Ead1_RestRestrita
{

    /**
     * @var \G2\Negocio\Contrato $negocio
     */
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Contrato();
    }

    public function indexAction()
    {
        $id_entidade = $this->getParam('id_entidade');
        $mensageiro = $this->negocio->findAllRegra($id_entidade);

        $this->getResponse()->appendBody(json_encode($mensageiro->mensagem));
    }

    public function getAction()
    {
        $id = $this->getParam('id');
        if ($id) {
            $mensageiro = $this->negocio->findRegraById($id);
            $this->getResponse()->appendBody(json_encode($mensageiro->mensagem));
        }
    }

    public function postAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);

        $negocio = new \G2\Negocio\ContratoRegra();

        $mensageiro = $negocio->save($data);

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->appendBody(json_encode($mensageiro->toArray()));
            $this->getResponse()->setHttpResponseCode(200);
        } else {
            $this->getResponse()->appendBody(json_encode(array('erro' => $mensageiro->getMensagem())));
            $this->getResponse()->setHttpResponseCode(500);
        }
    }

    public function putAction()
    {
        $this->postAction();
    }

    public function deleteAction ()
    {
        $id = (int) $this->getRequest()->getParam('id');

        if ($id) {

            $negocio = new \G2\Negocio\ContratoRegra();

            $entity = $negocio->find('\G2\Entity\ContratoRegra', $id);

            try {
                $negocio->delete($entity);
                $this->getResponse()->setHttpResponseCode(204);
            } catch (Zend_Exception $e) {
                $this->getResponse()->setHttpResponseCode(500);
            }

        }
    }

    public function headAction()
    {
    }


}

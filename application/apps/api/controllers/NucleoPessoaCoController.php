<?php

/**
 * Controller para gerenciar NucleoPessoaCo
 * @author Marcus Pereira <marcus.pereira@unyleya.com.br>
 * @since 2015-12-03
 * @package application
 * @subpackage controller
 */

class Api_NucleoPessoaCoController extends Ead1_RestRestrita {

    /**
     * @var \G2\Negocio\NucleoPessoaCo $negocio
     */
    private $negocio;
    private $repositoryName = '\G2\Entity\NucleoPessoaCo';

    public function init ()
    {
        parent::init();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->negocio = new \G2\Negocio\NucleoPessoaCo();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        $orderBy = $this->getParam('order_by') ? array($this->getParam('order_by') => $this->getParam('sort') ? $this->getParam('sort') : 'ASC') : null;
        unset($params['module'], $params['controller'], $params['action'], $params['order_by'], $params['sort'], $params['search'], $params['search']);
        $paramsLike = $this->getParam('search');

        $mensageiro = $this->negocio->findBySearch($this->repositoryName, $paramsLike, $params, $orderBy);
        foreach ($mensageiro as $key => &$entity) {
            $entity = $this->negocio->model($entity);
        }

        $this->_helper->json($mensageiro);
    }

    public function getAction()
    {
        $entity = $this->negocio->find($this->repositoryName, $this->getParam('id'));
        if ($entity)
            $this->_helper->json($this->negocio->toArrayEntity($entity));
    }

    public function postAction()
    {
        $data = Zend_Json::decode($this->getRequest()->getRawBody());

        $mensageiro = $this->negocio->save($data);
        $this->_helper->json($mensageiro);
    }

    public function putAction()
    {
        $this->postAction();
    }

    public function deleteAction()
    {
        $params = $this->getAllParams();

        //Caso seja o request de exclusão de pessoa do núcleo (baseado em id_usuario + id_nucleoco.
        if ($params['deletarPessoaNucleo']) {
            $mensageiro = $this->negocio->findBy('\G2\Entity\NucleoPessoaCo', array(
                'id_usuario' => $this->negocio->find('\G2\Entity\Usuario', $params['id_usuario']),
                'id_nucleoco' => $this->negocio->find('\G2\Entity\NucleoCo', $params['id_nucleoco'])
            ));
            foreach ($mensageiro as $entitiy) {
                $entitiy = $this->negocio->delete($entitiy);
            }
            $this->_helper->json($mensageiro);
        }

        $id = $this->getParam('id');

        if ($id) {
            $mensageiro = $this->negocio->find($this->repositoryName, $id);
            $mensageiro = $this->negocio->delete($mensageiro);

            $this->_helper->json($mensageiro);
        }
    }


}
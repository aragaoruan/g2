<?php
/**
 * Controller para funcionalidade de Entrega de Declaraçao
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 * @since  14-01-2015
 * @package application
 * @subpackage controller
 */

class Api_EntregaDeclaracaoController extends Ead1_RestRestrita {

    private $doctrineContainer;
    private $em;

    public function init() {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
    }

    public function indexAction() {

    }

    public function getAction() {

    }

    public function postAction() {

    }

    public function putAction() {

    }

    public function deleteAction() {

    }

    public function headAction() {

    }

}
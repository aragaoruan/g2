<?php

/**
 * Controller modolo API para gerenciar categoria
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-03-12
 */
class Api_CampanhaDescontoController extends Ead1_RestRestrita
{

    private $negocio;

    public function init ()
    {
        parent::init();
        $this->negocio = new G2\Negocio\CampanhaComercial();
    }

    public function deleteAction ()
    {
        
    }

    public function getAction ()
    {
        
    }

    public function headAction ()
    {
        
    }

    public function indexAction ()
    {
        
    }

    public function postAction ()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
        $result = $this->negocio->salvarCampanhaDesconto($data);
        if ($result) {
            $arrReturn = array(
                'id' => $result->getId_campanhadesconto(),
                'id_campanhadesconto' => $result->getId_campanhadesconto(),
                'id_campanhacomercial' => $result->getId_campanhacomercial()->getId_campanhacomercial(),
                'id_meiopagamento' => $result->getId_meiopagamento()->getId_meiopagamento(),
                'nu_descontomaximo' => $result->getNu_descontomaximo(),
                'nu_descontominimo' => $result->getNu_descontominimo(),
                'nu_valormax' => $result->getNu_valormax(),
                'nu_valormin' => $result->getNu_valormin()
            );
            $this->getResponse()->appendBody(json_encode($arrReturn));
            $this->getResponse()->setHttpResponseCode(201);
        } else {
            $this->getResponse()->appendBody(json_encode($result));
            $this->getResponse()->setHttpResponseCode(400);
        }
    }

    public function putAction ()
    {
        
    }

}

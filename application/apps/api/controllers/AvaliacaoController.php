<?php

/**
 * Controller para gerenciar as Avaliacao
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2014-12-10
 * @package application
 * @subpackage controller
 */

class Api_AvaliacaoController extends Zend_Rest_Controller
{

    private $bo;

    public function init ()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->bo = new AvaliacaoBO();
    }

    public function indexAction()
    {
        $to = new AvaliacaoTO();

        $mensageiro = $this->bo->retornarAvaliacao($to);

        $this->_helper->json($mensageiro);
    }

    public function getAction()
    {
        $id = $this->getParam('id');
        if ($id) {
            $to = new AvaliacaoTO();
            $to->setId_avaliacao($id);

            $mensageiro = $this->bo->retornarAvaliacao($to);

            if (isset($mensageiro->type) && $mensageiro->type == 'success' && $mensageiro->mensagem) {
                foreach ($mensageiro->mensagem as $key => $item) {
                    $result = array(
                        'id_avaliacao' => $item->getId_avaliacao(),
                        'st_avaliacao' => $item->getSt_avaliacao(),
                        'id_tipoavaliacao' => $item->getId_tipoavaliacao(),
                        'nu_valor' => $item->getNu_valor(),
                        'dt_cadastro' => $item->getDt_cadastro() ? date('d/m/Y', $item->getDt_cadastro()->getTimestamp()) : null,
                        'id_usuariocadastro' => $item->getId_usuariocadastro(),
                        'id_entidade' => $item->getId_entidade(),
                        'id_situacao' => $item->getId_situacao(),
                        'id_tiporecuperacao' => $item->getId_tiporecuperacao(),
                        'st_descricao' => $item->getSt_descricao(),
                        'nu_quantquestoes' => $item->getNu_quantquestoes(),
                        'st_linkreferencia' => $item->getSt_linkreferencia(),
                        'st_codigoref' => $item->getSt_codigoref(),
                        'bl_recuperacao' => $item->getBl_recuperacao(),
                        // Array com a referencias das discplinas
                        'disciplinas' => array()
                    );

                    // Buscar disciplinas
                    $disciplinas = $this->bo->retornarAvaliacaoDisciplina($to);
                    if ($disciplinas->type == 'success' && $disciplinas->mensagem) {
                        $result['disciplinas'] = $disciplinas->mensagem;
//                        foreach ($disciplinas->mensagem as $disc) {
//                            $result['disciplinas'][] = $disc->getId_disciplina();
//                        }
                    }
                    $mensageiro->mensagem[$key] = $result;
                }
            }

            $this->_helper->json($mensageiro);
        }
    }

    public function postAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);

        $st_avaliacao = $data['st_avaliacao'];

        $to = new AvaliacaoTO();
        if ($data['id_avaliacao'])
            $to->setId_avaliacao($data['id_avaliacao']);
        $to->setId_situacao($data['id_situacao']);
        $to->setId_tipoavaliacao($data['id_tipoavaliacao']);
        $to->setSt_avaliacao($st_avaliacao);
        $to->setNu_valor($data['nu_valor']);
        $to->setBl_recuperacao($data['bl_recuperacao']);

        if ($data['id_tipoavaliacao'] == \G2\Constante\TipoAvaliacao::CONCEITUAL) {
            $to->setNu_valor(0);
        }

        $arrDisciplinas = array();
        foreach($data['disciplinas'] as $disciplina) {
            $dto = new AvaliacaoDisciplinaTO();
            $dto->setId_avaliacao($to->getId_avaliacao());
            $dto->setId_disciplina($disciplina['id_disciplina']);
            $dto->setId_avaliacao($disciplina['id_avaliacao']);
            $dto->setId_avaliacaodisciplina($disciplina['id_avaliacaodisciplina']);
            $arrDisciplinas[] = $dto;
        }

        $mensageiro = $this->bo->salvarAvaliacao($to, false, $arrDisciplinas);

        $this->_helper->json($mensageiro);
    }

    public function putAction()
    {
        $this->postAction();
    }

    public function deleteAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $id = $this->getParam('id');

        if ($id) {
            $to = new AvaliacaoTO();
            $to->setId_avaliacao($id);

            // Buscar disciplinas para remover
            $disciplinas = $this->bo->retornarAvaliacaoDisciplina($to);
            if ($disciplinas->type == 'success' && $disciplinas->mensagem) {
                foreach ($disciplinas->mensagem as $disciplina) {
                    $disc = new AvaliacaoDisciplinaTO();
                    $disc->setId_avaliacaodisciplina($disciplina->getId_avaliacaodisciplina());
                    $disc->setId_avaliacao($disciplina->getId_avaliacao());

                    $this->bo->deletarAvaliacaoDisciplina($disc);
                }
            }

            $mensageiro = $this->bo->deletarAvaliacao($to);

            $this->_helper->json($mensageiro);
        }
    }

    public function headAction()
    {
    }

}

<?php

/**
 * Controller para gerenciar os AreaProjetoPedagogico
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @since 2013-12-18
 * @package application
 * @subpackage controller
 */
class Api_AreaProjetoPedagogicoController extends Ead1_RestRestrita
{
    
    private $doctrineContainer;
    private $em;
    
    public function init ()
    {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
    }
    
     public function indexAction()
    {
        
               
        
    }

    public function getAction()
    {
        
    	$to = new \AreaProjetoPedagogicoTO();
    	$to->setId_projetopedagogico($this->_getParam('id'));
    	$arrTO = $to->fetch(false, false, false);
    	
    	$id_areaconhecimento = array();
    	foreach($arrTO as $to){
    		$id_areaconhecimento[] = $to->getId_areaconhecimento();
    		$arrRetorno = array('id_projetopedagogico'=>$to->getId_projetopedagogico(), 'id_areaconhecimento'=>$id_areaconhecimento );
    	}
    	
    	$this->_helper->json(new \Ead1_Mensageiro($arrRetorno, Ead1_IMensageiro::SUCESSO));
        
    }
    
    public function postAction()
    {
    }
    
    public function putAction()
    {
    	try {
    	
    		$body       = $this->getRequest()->getRawBody();
    		$data       = Zend_Json::decode($body);
    		
    		if(!$data['id_areaconhecimento']){
    			throw new Zend_Exception("Informe a(s) Série(s)!");
    		}
    		
    		$arrTO = array();
    		$appTO = null;
    		foreach($data['id_areaconhecimento'] as $id_areaconhecimento){
    			
    			if(!$appTO) {
	    			$appTO = new \AreaProjetoPedagogicoTO();
	    			$appTO->setId_projetopedagogico($data['id_projetopedagogico']);
    			}
    			
    			$to = new AreaConhecimentoTO();
    			$to->setId_areaconhecimento($id_areaconhecimento);
    			$arrTO[] = $to;
    			
    		}
    		
    		$ro = new ProjetoPedagogicoRO();
    		$mensageiro = $ro->salvarArrayAreaProjetoPedagogico($arrTO, $appTO);
    		if($mensageiro->getTipo()==\Ead1_IMensageiro::SUCESSO){
    			$mensageiro->setId($data['id_projetopedagogico']);
    		}
    	
    	} catch (Exception $exc) {
    		$mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
    	}
    	
    	if($mensageiro->getTipo()!=Ead1_IMensageiro::SUCESSO){
    		$this->getResponse()->setHttpResponseCode(400);
    		$this->_helper->json($mensageiro);
    	} else {
    		$this->getResponse()->setHttpResponseCode(201);
    		
    		$vto = new \AreaProjetoPedagogicoTO();
    		$vto->setId_projetopedagogico($data['id_projetopedagogico']);
    		$arrTO = $vto->fetch(false, false, false);
    		
    		$id_areaconhecimento = array();
    		foreach($arrTO as $vto){
    			$id_areaconhecimento[] = $vto->getId_areaconhecimento();
    			$arrRetorno = array('id_projetopedagogico'=>$vto->getId_projetopedagogico(), 'id_areaconhecimento'=>$id_areaconhecimento );
    		}
    		
    		$this->_helper->json($arrRetorno);
    	}
    	
    	
    }
    
    public function deleteAction(){
        
    }

    public function headAction(){
        
    }
    

}
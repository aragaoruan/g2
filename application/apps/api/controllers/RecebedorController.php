<?php

/**
 * Controller para gerenciar os recebedores
 * @author Helder Silva <helder.silva@unyleya.com.br>
 * @package application
 * @subpackage controller
 */
class Api_RecebedorController extends Ead1_RestRestrita
{

    private $doctrineContainer;
    private $negocio;

    public function init ()
    {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
        $this->negocio = new \G2\Negocio\Negocio();
    }

    public function indexAction ()
    {
        $recebedor = new \G2\Negocio\Recebedor();
        $result = $recebedor->findAll('\G2\Entity\Recebedor');
        if($result){
            foreach ($result as $key=>$item) {
                $result[$key] = $this->negocio->toArrayEntity($item);
            }
            $this->getResponse()->appendBody(json_encode($result));
        }
    }

    public function getAction ()
    {

    }

    public function postAction ()
    {

    }

    public function putAction ()
    {

    }

    public function deleteAction ()
    {

    }

    public function headAction ()
    {
        
    }

}
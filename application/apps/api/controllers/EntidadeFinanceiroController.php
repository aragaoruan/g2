<?php

/**
 * @package application
 * @subpackage controller
 */
class Api_EntidadeFinanceiroController extends Ead1_RestRestrita
{

    private $doctrineContainer;
    private $em;
    /** @var \G2\Negocio\EntidadeFinanceiro() $bo */
    private $bo;

    public function init()
    {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
        $this->bo = new G2\Negocio\EntidadeFinanceiro();
    }

    public function indexAction()
    {

        if ($this->getParam('id_entidade')) {
            $mensageiro = $this->bo->findByEntidade($this->getParam('id_entidade'));
        } else {
            $retorno = $this->bo->findAll('\G2\Entity\EntidadeFinanceiro');
            $mensageiro = new \Ead1_Mensageiro($retorno, \Ead1_IMensageiro::SUCESSO);
        }
        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO ) {
            $serializer = new \Ead1\Doctrine\EntitySerializer($this->em);
            $obj = $serializer->toArray($mensageiro->getFirstMensagem());
            $mensageiro->setMensagem($obj);
            $this->_helper->json($mensageiro->toArray());
        } else {
            $this->_helper->json(array('mensagem'=>$mensageiro->getFirstMensagem()));
        }

    }

    public function getAction()
    {

        if ($this->getParam('id_entidade')) {
            $mensageiro = $this->bo->findByEntidade($this->getParam('id_entidade'));
        } else {
            $mensageiro = $this->bo->findById();
        }

        $serializer = new \Ead1\Doctrine\EntitySerializer($this->em);
        $obj = $serializer->toArray($mensageiro->getFirstMensagem());
        $mensageiro->setMensagem($obj);
        $this->_helper->json($mensageiro->toArray());
    }

    public function postAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);

        $mensageiro = $this->bo->save($data);

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->appendBody(json_encode($mensageiro->toArray()));
            $this->getResponse()->setHttpResponseCode(201);
        } else {
            $this->getResponse()->appendBody(json_encode(array('erro' => $mensageiro->getMensagem())));
            $this->getResponse()->setHttpResponseCode(500);
        }
    }

    public function putAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);

        $mensageiro = $this->bo->save($data);

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->appendBody(json_encode($mensageiro->toArray()));
            $this->getResponse()->setHttpResponseCode(200);
        } else {
            $this->getResponse()->appendBody(json_encode(array('erro' => $mensageiro->getMensagem())));
            $this->getResponse()->setHttpResponseCode(500);
        }
    }

    public function deleteAction()
    {
        try {
            if ($this->bo->delete($this->bo->find('G2\Entity\RegraPagamento', $this->getParam('id')))) {
                $this->getResponse()->setHttpResponseCode(204)->appendBody('Regra de pagamento removida com sucesso.');
            } else {
                throw new Exception('', '', '');
            }
        } catch (Zend_Exception $e) {
            $this->getResponse()->setHttpResponseCode(500)->appendBody('Não foi possível apagar o registro');
        }
    }

    public function headAction()
    {

    }

}

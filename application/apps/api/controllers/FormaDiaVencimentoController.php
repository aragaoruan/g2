<?php

/**
 * Class Api_FormaDiaPagamentoController
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Api_FormaDiaVencimentoController extends Ead1_RestRestrita
{

    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\FormaPagamento();
    }

    public function indexAction()
    {
        $result = $this->negocio->retornarFormaDiaVencimento();
        $this->getResponse()
            ->appendBody(json_encode($result));
    }

    public function getAction()
    {
        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);

        $result = $this->negocio->retornarFormaDiaVencimento($params, true);
        $this->getResponse()
            ->appendBody(json_encode($result));
    }

    public function headAction()
    {
        // TODO: Implement headAction() method.
    }

    public function postAction()
    {
        // TODO: Implement postAction() method.
    }

    public function putAction()
    {
        // TODO: Implement putAction() method.
    }

    public function deleteAction()
    {
        // TODO: Implement deleteAction() method.
    }
}
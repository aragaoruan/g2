<?php

/**
 * Class Api_GradeHorariaController para rest de grade horaria
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-12-04
 */
class Api_GradeHorariaController extends \Ead1_RestRestrita
{

    /**
     * @var \G2\Negocio\GradeHoraria $negocio
     */
    private $negocio;

    /**
     * @var GradeHorariaForm $form
     */
    private $form;

    public function init()
    {
        $this->_helper->viewRenderer->setNoRender();
        parent::init();
        $this->negocio = new \G2\Negocio\GradeHoraria();
        $this->form = new GradeHorariaForm();
    }

    public function indexAction()
    {
        $data = $this->getRequest()->getParams();
        unset($data['module'], $data['controller'], $data['action']);

        $result = $this->negocio->retornarPesquisaGradeHoraria($data);

        $this->getResponse()->setHttpResponseCode(201)->appendBody(json_encode($result));
    }

    /**
     * The get action handles GET requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function getAction()
    {
        // TODO: Implement getAction() method.
    }

    /**
     * The head action handles HEAD requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function headAction()
    {
        // TODO: Implement headAction() method.
    }

    /**
     * The post action handles POST requests; it should accept and digest a
     * POSTed resource representation and persist the resource state.
     */
    public function postAction()
    {
        // TODO: Implement postAction() method.
    }

    public function putAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json_Decoder::decode($body);
        if ($this->form->isValid($data)) {

            try {
                $result = $this->negocio->salvarGrade($data);
                if ($result) {
                    $arrReturn = array(
                        'id_gradehoraria' => $result->getId_gradehoraria(),
                        'st_nomegradehoraria' => $result->getSt_nomegradehoraria(),
                        'id_situacao' => $result->getId_situacao() ? $result->getId_situacao()->getId_situacao() : NULL,
                        'id_entidade' => $result->getId_entidade() ? $result->getId_entidade()->getId_entidade() : NULL,
                        'dt_iniciogradehoraria' => $result->getDt_iniciogradehoraria()->format('d/m/Y'),
                        'dt_fimgradehoraria' => $result->getDt_fimgradehoraria()->format('d/m/Y'),
                        'dt_cadastro' => $result->getDt_cadastro(),
                        'bl_sincronizado' => $result->getBl_sincronizado(),
                        'bl_ativo' => $result->getBl_ativo()
                    );
                    $this->getResponse()->setHttpResponseCode(201)->appendBody(json_encode($arrReturn));
                } else {
                    $this->getResponse()->setHttpResponseCode(400)->setBody("Erro ao salvar grade.");
                }
            } catch (\Exception $e) {
                $this->getResponse()->setHttpResponseCode(400)->setBody($e->getMessage());
            }


        } else {
            $strErros = $this->view->messages($this->form)->toString();
            $this->getResponse()->setHttpResponseCode(400)->setBody($strErros);
        }
    }

    public function deleteAction()
    {
        $id = intval($this->getParam('id'));
        if (!is_int($id)) {
            $this->getResponse()->setHttpResponseCode(400)->setBody('Não posso apagar um registro com ID inválido!');
        } else {
            $data['id_gradehoraria'] = $id;
            $data['bl_ativo'] = false;
            $result = $this->negocio->salvarGrade($data);
            if ($result) {
                $this->getResponse()->setHttpResponseCode(204)->appendBody('Registro removido');
            } else {
                $this->getResponse()->setHttpResponseCode(400)->appendBody('Erro ao tentar remover registro');
            }
        }
    }
}
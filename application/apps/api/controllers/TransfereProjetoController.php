<?php

/**
 * Controller API para Transferência de Projeto Pedagógico
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @since  11-07-2014
 * @package application
 * @subpackage controller
 */
class Api_TransfereProjetoController extends Ead1_RestRestrita {

    private $negocio;

    public function init() {
        parent::init();
        $this->negocio = new \G2\Negocio\Transferencia();
    }

    public function indexAction() {
        
    }

    public function getAction() {
        
    }

    public function postAction() {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json_Decoder::decode($body);
        $this->getResponse()->appendBody(($this->negocio->salvarTransferencia($data)->toJson()));
        $this->getResponse()->setHttpResponseCode(201);
    }

    public function putAction() {
        
    }

    public function deleteAction() {
 
    }

    public function headAction() {
        
    }

}

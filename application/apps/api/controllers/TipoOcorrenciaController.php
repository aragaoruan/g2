<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TipoOcorrenciaController
 *
 * @author rafael.bruno
 */
class Api_TipoOcorrenciaController extends Ead1_RestRestrita
{

    /**
     * @var \G2\Negocio\Ocorrencia $negocio
     */
    public $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Ocorrencia();
    }

    public function deleteAction()
    {
        try {
            $data = $this->getAllParams();

            $this->negocio->deletarTipoOcorrencia($data);
            $this->_helper->json(array('title' => 'Tipo Ocorrência', 'type' => 'success', 'text' => 'Registro apagado com sucesso!'));
        } catch (Zend_Exception $e) {
            $this->_helper->json(array('title' => 'Erro de operação', 'type' => 'error', 'text' => 'Erro ao apagar registro!'));
        }
    }

    /**
     * @Update 2013-10-29 Kayo Silva <kayo.silva@unyleya.com.br>
     */
    public function indexAction()
    {
        $returnArray = array();

        $params = array(
            'id_entidadecadastro' => $this->getParam('id_entidadecadastro') ?: $this->negocio->sessao->id_entidade,
            'bl_ativo' => true
        );

        if ($this->getParam('id_tipoocorrencia')) {
            $params['id_tipoocorrencia'] = $this->getParam('id_tipoocorrencia');
        }

        $find = $this->negocio->findBy('G2\Entity\CategoriaOcorrencia', $params);

        foreach ($find as $entity) {
            $returnArray[] = array(
                'id' => $entity->getId_categoriaocorrencia(),
                'id_situacao' => $entity->getId_situacao()->getId_situacao(),
                'st_situacao' => $entity->getId_situacao()->getSt_situacao(),
                'id_tipoocorrencia' => $entity->getId_tipoocorrencia()->getId_tipoocorrencia(),
                'st_tipoocorrencia' => $entity->getId_tipoocorrencia()->getSt_tipoocorrencia(),
                'st_categoriaocorrencia' => $entity->getSt_categoriaocorrencia()
            );
        }

        $this->_helper->json($returnArray);
    }

    public function postAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
        $this->salvar($data);
    }

    public function putAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
        $this->salvar($data);
    }

    public function salvar($data)
    {
        $this->negocio->salvarTipoOcorrencia($data);

        $this->_helper->json(array('title' => 'Tipo Ocorrência', 'type' => 'success', 'text' => 'Registro inserido com sucesso!'));
    }

}

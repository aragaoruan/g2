<?php

/**
 * Controller para gerenciar os tipo de regras de contrato
 * @author Helder Fernandes Silva <helder.silva@unyleya.com.br>
 * @since 2015-05-12
 * @package application
 * @subpackage controller
 */
class Api_TipoRegraContratoController extends Ead1_RestRestrita
{


    public function init ()
    {
        parent::init();
    }

    public function indexAction()
    {

        $tiporegracontrato = new \G2\Negocio\TipoRegraContrato();
        $mensageiro = $tiporegracontrato->retornarTipoRegraContrato();
        $arrayResult = array();
        foreach($mensageiro as $key=>$value){
            $arrayResult[$key]['id_tiporegracontrato'] = $value->getId_tiporegracontrato();
            $arrayResult[$key]['st_tiporegracontrato'] = $value->getSt_tiporegracontrato();
        }
        $this->_helper->json($arrayResult);

    }

    public function getAction()
    {

    }

    public function postAction()
    {
    }

    public function putAction()
    {

    }

    public function deleteAction(){

    }

    public function headAction(){

    }


}
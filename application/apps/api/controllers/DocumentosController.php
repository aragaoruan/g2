<?php

/**
 * Controller API para Secretaria > Documentação > Documentos
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since  25-09-2014
 * @package application
 * @subpackage controller
 */
class Api_DocumentosController extends Ead1_RestRestrita
{
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Documentos();
    }

    public function indexAction()
    {
        $data = $this->getRequest()->getQuery();

        $mensageiro = $this->negocio->findByEntidade($data);

        $this->getResponse()->appendBody(json_encode($mensageiro));
    }

    public function getAction()
    {
    }

    public function postAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);

        $mensageiro = $this->negocio->save($data);

        // Se salvar com sucesso, retornaro id_documentos
        // para o ItemView do Marionette
        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $data = array(
                'id_documentos' => $mensageiro->getCodigo()
            );

            $this->getResponse()->appendBody(json_encode($data));
            $this->getResponse()->setHttpResponseCode(200);
        } else {
            $this->getResponse()->appendBody(json_encode(array('erro' => $mensageiro->getMensagem())));
            $this->getResponse()->setHttpResponseCode(500);
        }
    }

    public function putAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);

        $mensageiro = $this->negocio->save($data);

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->appendBody(json_encode($mensageiro->toArray()));
            $this->getResponse()->setHttpResponseCode(200);
        } else {
            $this->getResponse()->appendBody(json_encode(array('erro' => $mensageiro->getMensagem())));
            $this->getResponse()->setHttpResponseCode(500);
        }
    }

    public function deleteAction()
    {
        try{
            $entity = $this->negocio->find('G2\Entity\Documentos', $this->getParam('id'));

            $negocio = new \G2\Negocio\Negocio();

            // Buscar DocumentosProjetoPedagogico e remover todos
            $ProjetosPedagogicos = $negocio->findBy('\G2\Entity\DocumentosProjetoPedagogico', array(
                'id_documentos' => $entity->getId_documentos()
            ));
            foreach ($ProjetosPedagogicos as $ProjetoEntity) {
                $negocio->delete($ProjetoEntity);
            }

            // Buscar DocumentosUtilizacaoRelacao e remover todos
            $DocumentosUtilizacaoRelacao = $negocio->findBy('\G2\Entity\DocumentosUtilizacaoRelacao', array(
                'id_documentos' => $entity->getId_documentos()
            ));
            foreach ($DocumentosUtilizacaoRelacao as $UtilizacaoEntity) {
                $negocio->delete($UtilizacaoEntity);
            }

            $this->negocio->delete($entity);
            $this->getResponse()->setHttpResponseCode(204);
        } catch (Zend_Exception $e) {
            $this->getResponse()->setHttpResponseCode(500);
        }
    }

    public function headAction()
    {
    }

}

<?php

/**
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @date 2017-11-08
 */
class Api_TipoSanguineoController extends Ead1_RestRestrita
{

    /**
     * @var \G2\Negocio\Negocio $negocio
     */
    private $negocio;
    private $repositoryName = '\G2\Entity\TipoSanguineo';

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Negocio();
    }

    public function indexAction()
    {
        try {
            $params = $this->getAllParams();
            unset($params['module'], $params['controller'], $params['action']);

            $results = $this->negocio->findBy($this->repositoryName, $params);
            $this->_helper->json($results ? $this->negocio->toArrayEntity($results) : array());
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(500)->appendBody($e->getMessage());
        }
    }

    public function getAction()
    {
        try {
            $result = $this->negocio->find($this->repositoryName, $this->getParam('id'));
            $this->_helper->json($result ? $this->negocio->toArrayEntity($result) : (new stdClass()));
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(500)->appendBody($e->getMessage());
        }
    }

}

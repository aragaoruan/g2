<?php

/**
 * Controller para gerenciar as VwSalaDeAulaQuantitativos
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */

class Api_VwSalaDeAulaQuantitativosController extends Ead1_RestRestrita
{

    public function indexAction()
    {
        $negocio = new \G2\Negocio\SalaDeAula();

        $limit = NULL;
        $offset = NULL;
        $params = $this->getAllParams();

        if (array_key_exists('limit', $params)){
            $limit = $params['limit'];
        }

        unset($params['module'], $params['controller'], $params['action'], $params['limit'], $params['offset']);

        $mensageiro = $negocio->retornaVwSalaDeAulaQuantitativos($params, array(), $limit, $offset);
        $results = array();
        foreach ($mensageiro as $entity) {
            $results[] = $negocio->toArrayEntity($entity);
        }

        $this->_helper->json($results);
    }

    public function getAction()
    {
        ;
    }

    public function postAction()
    {
        ;
    }

    public function putAction()
    {
        ;
    }

    public function deleteAction()
    {
        ;
    }

    public function headAction()
    {
    }

}
<?php


/**
 * Class Api_ItemGradeHorariaController Rest para item de grade horaria
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-12-05
 */
class Api_ItemGradeHorariaTurmaController extends Zend_Rest_Controller
{

    private $negocio;

    public function init()
    {
        $this->_helper->viewRenderer->setNoRender();
        parent::init();
        $this->negocio = new \G2\Negocio\GradeHoraria();
    }


    /**
     * The index action handles index/list requests; it should respond with a
     * list of the requested resources.
     */
    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['module'], $params['action'], $params['controller']);
        $result = $this->negocio->retornarItemGradeHorariaTurma($params);
        if ($result) {
            foreach ($result as $key => $row) {
                $result[$key] = array(
                    'id_itemgradehorariaturma' => $row->getId_itemgradehorariaturma(),
                    'dt_cadastro' => $row->getDt_cadastro(),
                    'id_itemgradehoraria' => $row->getId_itemgradehoraria()->getId_itemgradehoraria(),
                    'id_turma' => $row->getId_turma()->getId_turma(),
                    'bl_ativo' => $row->getBl_ativo()
                );
            }
        }
        $this->getResponse()->appendBody(json_encode($result));
    }

    /**
     * The get action handles GET requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function getAction()
    {
    }

    /**
     * The head action handles HEAD requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function headAction()
    {
    }

    /**
     * The post action handles POST requests; it should accept and digest a
     * POSTed resource representation and persist the resource state.
     */
    public function postAction()
    {
    }

    /**
     * The put action handles PUT requests and receives an 'id' parameter; it
     * should update the server resource state of the resource identified by
     * the 'id' value.
     */
    public function putAction()
    {


    }

    /**
     * The delete action handles DELETE requests and receives an 'id'
     * parameter; it should update the server resource state of the resource
     * identified by the 'id' value.
     */
    public function deleteAction()
    {
    }
}
<?php

/**
 * Controller para gerenciar local de aula
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 * @since 2013-09-14
 * @package application
 * @subpackage controller
 */
class Api_PADadosCadastraisController extends Ead1_RestRestrita {

    private $doctrineContainer;
    private $em;

    public function init() {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
    }

    public function indexAction() {

        $etapa = $this->em->getRepository('\G2\Entity\PADadosCadastrais')->findByEntidadeSessao();
        if (!$etapa) {
            $etapa = new \G2\Entity\PADadosCadastrais();
        }
        //$serializer = new \Ead1\Doctrine\EntitySerializer($this->em);
        //$serializer->entityToArray($boasVindas);
        $retorno = array(
            'id_pa_dadoscadastrais' => $etapa->getId_pa_dadoscadastrais(),
            'id_usuariocadastro' => $etapa->getId_usuariocadastro()->getId_usuario(),
            'id_entidade' => $etapa->getId_entidade()->getId_entidade(),
            'id_textomensagem' => $etapa->getId_textomensagem()->getId_textosistema(),
            'st_textomensagem' => $etapa->getId_textomensagem()->getSt_texto(),
            'id_textoinstrucao' => $etapa->getId_textoinstrucao()->getId_textosistema(),
            'st_textoinstrucao' => $etapa->getId_textoinstrucao()->getSt_texto(),
            'id_assuntoco' => $etapa->getId_assuntoco()->getId_assuntoco(),
            'id_assuntocopai' => $etapa->getId_assuntoco()->getId_assuntocopai() ? $etapa->getId_assuntoco()->getId_assuntocopai()->getId_assuntoco() : null,
            'id_categoriaocorrencia' => $etapa->getId_categoriaocorrencia()->getId_categoriaocorrencia(),
        );
        $this->getResponse()->appendBody(json_encode($retorno));
    }

    public function deleteAction() {
//        $id = intval($this->getParam('id'));
//
//        if (!is_int($id)) {
//            $this->getResponse()->setHttpResponseCode(400);
//            $this->getResponse()->setBody('Não posso apagar um registro com ID inválido!');
//        } else {
//            $return = $this->negocio->delete($id);
//            $this->getResponse()->setHttpResponseCode(204)
//                    ->appendBody('Registro ' . $return->getSt_tipodematerial() . ' removido');
//        }
    }

    public function getAction() {
        $etapa = $this->em->getRepository('\G2\Entity\PADadosCadastrais')->findByEntidadeSessao();
        if (!$etapa) {
            $etapa = new \G2\Entity\PADadosCadastrais();
        }
        //$serializer = new \Ead1\Doctrine\EntitySerializer($this->em);
        //$serializer->entityToArray($boasVindas);
        $retorno = array(
            'id_pa_dadoscadastrais' => $etapa->getId_pa_dadoscadastrais(),
            'id_usuariocadastro' => $etapa->getId_usuariocadastro()->getId_usuario(),
            'id_entidade' => $etapa->getId_entidade()->getId_entidade(),
            'id_textomensagem' => $etapa->getId_textomensagem()->getId_textosistema(),
            'st_textomensagem' => $etapa->getId_textomensagem()->getSt_texto(),
            'id_textoinstrucao' => $etapa->getId_textoinstrucao()->getId_textosistema(),
            'st_textoinstrucao' => $etapa->getId_textoinstrucao()->getSt_texto(),
            'id_assuntoco' => $etapa->getId_assuntoco()->getId_assuntoco(),
            'id_assuntocopai' => $etapa->getId_assuntoco()->getId_assuntocopai() ? $etapa->getId_assuntoco()->getId_assuntocopai()->getId_assuntoco() : null,
            'id_categoriaocorrencia' => $etapa->getId_categoriaocorrencia()->getId_categoriaocorrencia(),
        );
        $this->getResponse()->appendBody(json_encode($retorno));
    }

    public function headAction() {
        
    }

    public function postAction() {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
        $result = $this->em->getRepository('\G2\Entity\PADadosCadastrais')->save($data);
        $this->getResponse()->appendBody(json_encode(array(
            'id' => $result->getId_pa_dadoscadastrais(),
            'type' => 'success',
            'title' => 'Salvo com Sucesso',
            'text' => 'O registro foi salvo com sucesso!'
        )));
        $this->getResponse()->setHttpResponseCode(201);
    }

    public function putAction() {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
        $result = $this->em->getRepository('\G2\Entity\PADadosCadastrais')->update($data);
        $this->getResponse()->appendBody(json_encode(array(
            'id_pa_dadoscadastrais' => $result->getId_pa_dadoscadastrais(),
            'type' => 'success',
            'title' => 'Atualizado com Sucesso',
            'text' => 'O registro foi atualizado com sucesso!'
        )));
        $this->getResponse()->setHttpResponseCode(201);
    }

}

<?php

/**
 * Controller para gerenciar os ProjetoPedagogico
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @since 2013-12-21
 * @package application
 * @subpackage controller
 */
class Api_VwUsuarioPerfilEntidadeReferenciaController extends Ead1_RestRestrita {

    public function indexAction ()
    {
        $to = new VwUsuarioPerfilEntidadeReferenciaTO();
        $to->montaToDinamico($this->_getAllParams());
        $ro = new ProjetoPedagogicoRO();
        $mensageiro = $ro->retornaCoordenador($to);
        $this->_helper->json($mensageiro->getMensagem());
    }

    /**
     * Metódo REST GET
     * @author Kayo Silva <kayo.silva@unyleya.com.br> Date 2014-01-06
     */
    public function getAction ()
    {
        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);
        $to = new VwUsuarioPerfilEntidadeReferenciaTO();
        $to->montaToDinamico($params);
        $ro = new ProjetoPedagogicoRO();
        $mensageiro = $ro->retornaCoordenador($to);
        $arrReturn = array();
        if ($mensageiro->getType() == 'success') {
            foreach ($mensageiro->getMensagem() as $row) {
                $arrReturn[] = array(
                    'id' => $row->getId_perfilreferencia(),
                    'id_perfilreferencia' => $row->getId_perfilreferencia(),
                    'id_usuario' => $row->getId_usuario(),
                    'id_entidade' => $row->getId_entidade(),
                    'id_perfil' => $row->getId_perfil(),
                    'id_areaconhecimento' => $row->getId_areaconhecimento(),
                    'id_projetopedagogico' => $row->getId_projetopedagogico(),
                    'id_saladeaula' => $row->getId_saladeaula(),
                    'id_disciplina' => $row->getId_disciplina(),
                    'id_livro' => $row->getId_livro(),
                    'id_perfilreferenciaintegracao' => $row->getId_perfilreferenciaintegracao(),
                    'bl_ativo' => $row->getBl_ativo(),
                    'bl_titular' => $row->getBl_titular(),
                    'bl_autor' => $row->getBl_autor(),
                    'nu_porcentagem' => $row->getNu_porcentagem(),
                    'st_projetopedagogico' => $row->getSt_projetopedagogico(),
                    'st_nomecompleto' => $row->getSt_nomecompleto(),
                    'st_saladeaula' => $row->getSt_saladeaula(),
                );
            }
        }

        $this->getResponse()
                ->appendBody(json_encode($arrReturn));
    }

    public function postAction ()
    {

    }

    public function putAction ()
    {
        $data = Zend_Json::decode($this->getRequest()->getRawBody());
        $to = new UsuarioPerfilEntidadeReferenciaTO();
        $to->montaToDinamico($data);
        $bo = new ProjetoPedagogicoBO();
        $mensageiro = $bo->salvarCoordenador($to);
        $arrReturn = array();
        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            foreach ($mensageiro->getMensagem() as $row) {
                $arrReturn = $row->toArray();
            }
            $this->getResponse()->setHttpResponseCode(201);
            $this->getResponse()
                    ->appendBody(json_encode($arrReturn));
        } else {
            $this->getResponse()->setHttpResponseCode(400);
            $this->getResponse()
                    ->appendBody(json_encode($mensageiro->getText()));
        }
    }

    public function deleteAction ()
    {
        $id = intval($this->getParam('id'));

        if (!is_int($id)) {
            $this->getResponse()->setHttpResponseCode(400);
            $this->getResponse()->setBody('Não posso apagar um registro com ID inválido!');
        } else {
            $to = new UsuarioPerfilEntidadeReferenciaTO();
            $to->setId_perfilReferencia($id);
            $bo = new ProjetoPedagogicoBO();
            $mensageiro = $bo->excluirCoordenador($to);
            if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $this->getResponse()->setHttpResponseCode(204)->appendBody($mensageiro->getText());
            } else {
                $this->getResponse()->setHttpResponseCode(400)->appendBody($mensageiro->getText());
            }
        }
    }


}
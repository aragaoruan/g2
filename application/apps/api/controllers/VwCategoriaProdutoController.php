<?php

/**
 * Controller para gerenciar as áreas
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @since 2014-03-18
 * @package application
 * @subpackage controller
 */
class Api_VwCategoriaProdutoController extends Ead1_RestRestrita
{
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new G2\Negocio\VwCategoriaProduto();
    }

    public function indexAction()
    {

        try {

            $array = $this->_getAllParams();
            unset($array['module'], $array['controller'], $array['action']);

            $bo = new G2\Negocio\Produto();
            $mensageiro = $bo->retornarCategoriasProduto($array['id_produto']);

        } catch (Exception $e) {
            $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json($mensageiro);
        } else {
            $this->getResponse()->setHttpResponseCode(201);
            $array = null;
            foreach ($mensageiro->getMensagem() as $entity) {
                $array[] = $entity->entityToArray();
            }
            $this->_helper->json($array);
        }

    }

    public function getAction()
    {
        $params = $this->_getAllParams();

        unset($params['controller'], $params['module'], $params['action']);

        $result = $this->negocio->retornarProdutoCategoriaParams($params);

        $this->_helper->json($result);
    }

    public function postAction()
    {

    }

    public function putAction()
    {


    }

    public function deleteAction()
    {

    }

    public function headAction()
    {

    }

}
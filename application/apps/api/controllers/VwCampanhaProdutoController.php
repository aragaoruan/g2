<?php

/**
 * Controller modolo API para gerenciar categoria
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-03-11
 */
class Api_VwCampanhaProdutoController extends Ead1_RestRestrita
{

    private $negocio;

    public function init ()
    {
        parent::init();
        $this->negocio = new G2\Negocio\CampanhaComercial();
    }

    public function deleteAction ()
    {
        
    }

    public function getAction ()
    {
        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);
        $result = $this->negocio->retornarVwCampanhaProduto($params);
        $arrResult = array();
        if ($result) {
            foreach ($result as $row) {
                $arrResult[] = $this->negocio->toArrayEntity($row);
            }
        }
        $this->getResponse()
                ->appendBody(json_encode($arrResult));
    }

    public function headAction ()
    {
        
    }

    public function indexAction ()
    {
        
    }

    public function postAction ()
    {
        
    }

    public function putAction ()
    {
        
    }

//put your code here
}

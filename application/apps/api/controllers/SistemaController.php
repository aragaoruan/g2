<?php

/**
 * Controller para gerenciar Sistema
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2015-01-01
 * @package application
 * @subpackage controller
 */

class Api_SistemaController extends Ead1_RestRestrita {

    private $negocio;

    public function init ()
    {
        parent::init();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->negocio = new \G2\Negocio\Negocio();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();

        $to = new SistemaTO();
        $to->montaToDinamico($params);

        $orm = new SistemaORM();
        $mensageiro = new Ead1_Mensageiro($orm->consulta($to, false, false, 'st_sistema ASC'));

        $this->_helper->json($mensageiro);
    }

}
<?php

/**
 * Controller para cadastrar avaliações
 * @author Débora Castro
 * @since  20/10/2014
 * @package Api
 * @subpackage controller
 */
class Api_AvaliacaoConjunto extends Ead1_RestRestrita
{

    private $negocio;

    public function init ()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\AvaliacaoConjunto();
    }

    public function deleteAction ()
    {

    }

    public function getAction ()
    {


    }

    public function headAction ()
    {
        
    }

    public function indexAction ()
    {

    }

    public function postAction ()
    {

    }

    public function putAction ()
    {
    }
}

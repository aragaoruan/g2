<?php

/**
 * Class Api_EsquemaPerguntasController
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Api_EsquemaPerguntaController extends Ead1_RestRestrita
{
    private $_negocio;

    public function init()
    {
        parent::init();
        $this->_negocio = new \G2\Negocio\PerguntasFrequentes();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);
        $arrReturn = $this->_negocio->retornarEsquemas($params, true);
        $this->getResponse()
            ->appendBody(json_encode($arrReturn));
    }

    public function getAction()
    {
    }

    public function headAction()
    {

    }

    public function postAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json_Decoder::decode($body);
        $result = $this->_negocio->salvarEsquema($data);
        if ($result) {
            $arrReturn = array(
                'id_entidadecadastro' => $result->getId_entidadecadastro() ? $result->getId_entidadecadastro()->getId_entidade() : NULL,
                'id_esquemapergunta' => $result->getId_esquemapergunta(),
                'st_esquemapergunta' => $result->getSt_esquemapergunta(),
                'dt_cadastro' => $result->getDt_cadastro(),
                'bl_ativo' => $result->getBl_ativo()
            );
            $this->getResponse()->setHttpResponseCode(201)->appendBody(json_encode($arrReturn));
        } else {
            $this->getResponse()->setHttpResponseCode(400)->setBody("Erro ao salvar esquema.");
        }

    }

    public function putAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json_Decoder::decode($body);
        $result = $this->_negocio->salvarEsquema($data);
        if ($result) {
            $arrReturn = array(
                'id_entidadecadastro' => $result->getId_entidadecadastro() ? $result->getId_entidadecadastro()->getId_entidade() : NULL,
                'id_esquemapergunta' => $result->getId_esquemapergunta(),
                'st_esquemapergunta' => $result->getSt_esquemapergunta(),
                'dt_cadastro' => $result->getDt_cadastro(),
                'bl_ativo' => $result->getBl_ativo()
            );
            $this->getResponse()->setHttpResponseCode(201)->appendBody(json_encode($arrReturn));
        } else {
            $this->getResponse()->setHttpResponseCode(400)->setBody("Erro ao salvar esquema.");
        }
    }

    public function deleteAction()
    {

    }
}
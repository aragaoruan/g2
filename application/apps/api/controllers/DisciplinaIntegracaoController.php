<?php
/**
 * Description of DisciplinaIntegracaoController
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class Api_DisciplinaIntegracaoController extends Ead1_RestRestrita
{

    /** @var  \G2\Negocio\Disciplina */
    private $negocio;

    public function init ()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Disciplina();
    }

    public function deleteAction ()
    {

    }
    public function putAction(){

    }
    public function postAction(){
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);

        $result = $this->negocio->salvaDisciplinaIntegracao($data);
        if ($result->getType() == 'success') {
            $this->getResponse()->appendBody(json_encode($data));
            $this->getResponse()->setHttpResponseCode(201);
        } else {
            $this->getResponse()->appendBody(json_encode($result->getText()));
            $this->getResponse()->setHttpResponseCode(400);
        }
    }

    public function indexAction(){
        $params = $this->getAllParams();
        unset($params['controller'], $params['module'], $params['action']);

        if (!array_key_exists('id_entidade', $params)) {
            $params['id_entidade'] = $this->negocio->sessao->id_entidade;
            $params['bl_ativo'] = 1;
        }

        $disciplinasIntegracao = $this->negocio->findBy('\G2\Entity\DisciplinaIntegracao', $params);
        $arrReturn = array(); //Array para retorno
        if ($disciplinasIntegracao) {
            foreach ($disciplinasIntegracao as $row) {
                $arrReturn[] =  $this->negocio->toArrayEntity($row);
            }
        }

        $this->getResponse()->appendBody(json_encode($arrReturn));
        $this->getResponse()->setHttpResponseCode(200);
    }
    public function headAction ()
    {

    }
    public function getAction(){
        $params = $this->_getAllParams();
        unset($params['controller'], $params['module'], $params['action']);

        $disciplinaTO = new \DisciplinaIntegracaoTO();
        $disciplinaTO->setId_disciplina($params['id_disciplinaintegracao']);
        $disciplinaTO->fetch(true, false, true);

        $arrReturn = array(); //Array para retorno
        if ($disciplinaTO) {
            foreach ($disciplinaTO as $i => $row) {
                $arrReturn[] = $row->toBackboneArray();
            }
        }

        $this->getResponse()->appendBody(json_encode($arrReturn));
        $this->getResponse()->setHttpResponseCode(200);
    }

}

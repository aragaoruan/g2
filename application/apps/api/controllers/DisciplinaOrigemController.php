<?php

/**
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @date 2017-10-05
 */
class Api_DisciplinaOrigemController extends Ead1_RestRestrita
{

    /**
     * @var \G2\Negocio\Negocio $negocio
     */
    private $negocio;
    private $repositoryName = '\G2\Entity\DisciplinaOrigem';

    public function init()
    {
        parent::init();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->negocio = new \G2\Negocio\Negocio();
    }

    public function indexAction()
    {
        try {
            $params = $this->getAllParams();
            unset($params['module'], $params['controller'], $params['action']);

            $results = $this->negocio->findBy($this->repositoryName, $params, array('st_disciplina' => 'ASC'));
            $this->_helper->json($results ? $this->negocio->toArrayEntity($results) : array());
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(500)->appendBody($e->getMessage());
        }
    }

    public function getAction()
    {
        try {
            $result = $this->negocio->find($this->repositoryName, $this->getParam('id'));
            $this->_helper->json($result ? $this->negocio->toArrayEntity($result) : (new stdClass()));
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(500)->appendBody($e->getMessage());
        }
    }

    public function postAction()
    {
        try {
            $data = Zend_Json::decode($this->getRequest()->getRawBody());

            /** @var \G2\Entity\DisciplinaOrigem $entity */
            $entity = $this->negocio
                ->preencherEntity($this->repositoryName, 'id_disciplinaorigem', $data, array(
                    'bl_ativo' => true,
                    'dt_cadastro' => date('Y-m-d H:i:s'),
                    'id_usuariocadastro' => $this->negocio->sessao->id_usuario
                ));

            // Verificar se possui referências
            if ($entity->getId_disciplinaorigem() && !$entity->getBl_ativo()) {
                $references = $this->negocio
                    ->findBy('\G2\Entity\DiscMatriculaDiscOrigem', array(
                        'id_disciplinaorigem' => $entity->getId_disciplinaorigem()
                    ));

                // Se possuir referências não permite remover o item (setar o bl_ativo = false;
                if ($references) {
                    throw new Exception('Não foi possível remover o registro porque ele está sendo utilizado.');
                }
            }


            $response = $this->negocio->save($entity);
            $this->_helper->json($response ? $this->negocio->toArrayEntity($response) : (new stdClass()));
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(500)->appendBody(json_encode(array('text' => $e->getMessage())));
        }
    }

    public function putAction()
    {
        $this->postAction();
    }

}

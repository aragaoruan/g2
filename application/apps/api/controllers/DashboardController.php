<?php

/**
 * Controller para gerenciar Dashboard
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2015-01-01
 * @package application
 * @subpackage controller
 */

class Api_DashboardController extends Ead1_RestRestrita {

    /**
     * @var \G2\Negocio\Dashboard $negocio
     */
    private $negocio;
    private $repositoryName = '\G2\Entity\Dashboard';

    public function init ()
    {
        parent::init();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->negocio = new \G2\Negocio\Dashboard();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        $orderBy = $this->getParam('order_by') ? array($this->getParam('order_by') => $this->getParam('sort') ? $this->getParam('sort') : 'ASC') : null;
        unset($params['module'], $params['controller'], $params['action'], $params['order_by'], $params['sort']);

        $mensageiro = $this->negocio->getAll($params, $orderBy);
        $this->_helper->json($mensageiro);
    }

    public function getAction()
    {
        $id = $this->getParam('id');

        if ($id) {
            $mensageiro = $this->negocio->find($this->repositoryName, $id);
            $mensageiro = $this->negocio->toArrayEntity($mensageiro);

            $this->_helper->json($mensageiro);
        }
    }

    public function postAction()
    {
        $data = Zend_Json::decode($this->getRequest()->getRawBody());

        $mensageiro = $this->negocio->save($data);
        $this->_helper->json($mensageiro);
    }

    public function putAction()
    {
        $this->postAction();
    }

    public function deleteAction()
    {
        $id = $this->getParam('id');

        if ($id) {
            $data = array(
                'id_dashboard' => $id,
                'bl_ativo' => '0'
            );
            $mensageiro = $this->negocio->save($data);
            $this->_helper->json($mensageiro);

            //$mensageiro = $this->negocio->find($this->repositoryName, $id);
            //$mensageiro = $this->negocio->delete($mensageiro);
            //$this->_helper->json($mensageiro);
        }
    }

}
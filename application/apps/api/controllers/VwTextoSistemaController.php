<?php

/**
 * Controller para retornar os Textos Sistemas
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2013-10-15
 * @package application
 * @subpackage controller
 */
class Api_VwTextoSistemaController extends Ead1_RestRestrita
{
    
    private $doctrineContainer;
    private $em;
    
    public function init ()
    {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
    }
    
    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['module'], $params['controller'], $params['action']);

        $texto = new \G2\Negocio\TextoSistema();
        $mensageiro = $texto->findVwTextoSistema($params);
        
        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $mensageiro->setText(count($mensageiro->getMensagem()) . ' registros encontrados!');
        }
        
        $this->_helper->json($mensageiro);
    }

    public function getAction()
    {
        $mensageiro = new \Ead1_Mensageiro("Funcionalidade não implementada!", \Ead1_IMensageiro::AVISO);
        $this->getResponse()
                ->appendBody($mensageiro->toJson());
        
    }
    
    public function postAction()
    {
        $mensageiro = new \Ead1_Mensageiro("Funcionalidade não implementada!", \Ead1_IMensageiro::AVISO);
        $this->getResponse()
                ->appendBody($mensageiro->toJson());
    }
    
    public function putAction()
    {

        $mensageiro = new \Ead1_Mensageiro("Funcionalidade não implementada!", \Ead1_IMensageiro::AVISO);
        $this->getResponse()
                ->appendBody($mensageiro->toJson());
    }
    
    public function deleteAction()
    {
        $mensageiro = new \Ead1_Mensageiro("Funcionalidade não implementada!", \Ead1_IMensageiro::AVISO);
        $this->getResponse()
                ->appendBody($mensageiro->toJson());
        
    }

    public function headAction(){
        
    }
    

}
<?php

/**
 * Controller para gerenciar os ProjetoPedagogico
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @since 2013-12-21
 * @package application
 * @subpackage controller
 */
class Api_VwUsuarioPerfilEntidadeController extends Zend_Rest_Controller
{


    public function init()
    {
        $this->_helper->viewRenderer->setNoRender(true);
    }

    public function indexAction()
    {

        $to = new VwUsuarioPerfilEntidadeTO();
        $to->montaToDinamico($this->_getAllParams());
        $to->setId_entidadecadastro($to->getSessao()->id_entidade);
        $ro = new ProjetoPedagogicoRO();
        $mensageiro = $ro->retornarVwUsuarioPerfilEntidade($to);
        $this->_helper->json($mensageiro->getMensagem());

    }

    public function getAction()
    {
        $params = $this->getAllParams();
        unset($params['module'], $params['controller'], $params['action']);
        $negocio = new \G2\Negocio\Disciplina();
        $result = $negocio->retornarDadosCoordenadorDisciplina($params);
        $this->_helper->json($result);
    }

    public function postAction()
    {
    }

    public function putAction()
    {
    }

    public function deleteAction()
    {

    }

    public function headAction()
    {

    }


}
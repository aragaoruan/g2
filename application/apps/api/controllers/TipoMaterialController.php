<?php

use G2\Negocio\TipodeMaterial;

/**
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Api_TipoMaterialController extends Ead1_RestRestrita
{

    private $negocio;
    private $repositoryName = '\G2\Entity\TipodeMaterial';

    public function init ()
    {
        parent::init();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->negocio = new \G2\Negocio\TipodeMaterial();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        $orderBy = $this->getParam('order_by') ? array($this->getParam('order_by') => $this->getParam('sort') ? $this->getParam('sort') : 'ASC') : null;
        unset($params['module'], $params['controller'], $params['action'], $params['order_by'], $params['sort']);

        if (!isset($params['id_entidade']) || !$params['id_entidade'])
            $params['id_entidade'] = $this->negocio->sessao->id_entidade;

        $mensageiro = $this->negocio->findBy($this->repositoryName, $params, $orderBy);
        foreach ($mensageiro as $key => &$entity) {
            $entity = $entity->toArray();
        }

        $this->_helper->json($mensageiro);
    }

    public function getAction()
    {
        $mensageiro = $this->negocio->find($this->repositoryName, $this->getParam('id'));
        if ($mensageiro)
            $this->_helper->json($mensageiro->toArray());
    }

    public function postAction()
    {
        $data = Zend_Json::decode($this->getRequest()->getRawBody());

        $mensageiro = $this->negocio->save($data);
        $this->_helper->json($mensageiro);
    }

    public function putAction()
    {
        $this->postAction();
    }

    public function deleteAction()
    {
        $id = $this->getParam('id');

        if ($id) {
            $entity = $this->negocio->find($this->repositoryName, $id);

            $mensageiro = $this->negocio->delete($entity);
            $this->_helper->json($mensageiro);
        }
    }

}
<?php

/**
 * Controller para criar aplicações controle da tb_avaliacaoagendamento (Gerencia Prova)
 * @author Débora Castro <debora.castro@unyleya.com.br>
 * @since  28/05/2015
 * @package application
 * @subpackage controller
 */
class Api_AvaliacaoAgendamentoController extends Ead1_RestRestrita
{
    /**
     * @var \G2\Negocio\GerenciaProva
     */
    private $negocio;

    public function init()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        parent::init();
        $this->negocio = new \G2\Negocio\GerenciaProva();
    }

    public function indexAction()
    {
        $array = array();

        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);

        if ($params) {
            $result = $this->negocio->findByAvaliacaoAgendamento($params);
            foreach ($result as $key => &$values) {
                array_push($array, $this->negocio->toArrayEntity($values));
            }
        }
        $this->_helper->json($array);
    }

    public function getAction()
    {

    }

    public function postAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json_Decoder::decode($body);

        $mensageiro = $this->negocio->salvaAvaliacaoAgendamento($data);

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->setHttpResponseCode(201);
        } else {
            $this->getResponse()->setHttpResponseCode(500);
        }

        $this->getResponse()->appendBody(json_encode($mensageiro->toArray()));
    }

    public function putAction()
    {
        $this->postAction();
    }

    public function deleteAction()
    {

    }

    public function headAction()
    {

    }

}

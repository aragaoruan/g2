<?php

/**
 * Created by PhpStorm.
 * User: helder.silva
 * Date: 03/03/2015
 * Time: 10:40
 */
class Api_MotivoEntidadeController extends Ead1_RestRestrita
{
    private $_negocio;

    public function init()
    {
        parent::init();
        $this->_negocio = new \G2\Negocio\Motivo();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);
        $result = $this->_negocio->retornarMotivoEntidade($params);

        $arrReturn = array();
        if ($result) {
            foreach ($result as $key => $motivo) {
                $arrReturn[$key] = $this->_negocio->toArrayEntity($motivo);
                $arrReturn[$key]['id_usuariocadastro'] = $motivo->getId_usuariocadastro() ? $motivo->getId_usuariocadastro()->getId_usuario() : NULL;
                $arrReturn[$key]['id_motivo'] = $motivo->getId_motivo() ? $motivo->getId_motivo()->getId_motivo() : NULL;
                $arrReturn[$key]['id_entidade'] = $motivo->getId_entidade() ? $motivo->getId_entidade()->getId_entidade() : NULL;
            }
        }

        $this->getResponse()
            ->appendBody(json_encode($arrReturn));

    }

    public function getAction()
    {


    }

    public function postAction()
    {


    }

    public function putAction()
    {

    }

    public function deleteAction()
    {


    }

    public function headAction()
    {

    }

}
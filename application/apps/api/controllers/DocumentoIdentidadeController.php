<?php

/**
 * Class Api_DocumentoIdentidadeController
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Api_DocumentoIdentidadeController extends Ead1_RestRestrita
{

    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Pessoa();
    }

    /**
     * The index action handles index/list requests; it should respond with a
     * list of the requested resources.
     */
    public function indexAction()
    {
        $result = $this->negocio->retornarDadosIdentidade();
        $arrResult = array();
        if ($result) {
            foreach ($result as $i => $identidade) {
                $arrResult[$i] = $this->negocio->toArrayEntity($identidade);
                if ($identidade->getDt_dataexpedicao()) {
                    $dt_dataexpedicao = $identidade->getDt_dataexpedicao();
                    $arrResult[$i]['dt_dataexpedicao'] = $dt_dataexpedicao->format('d/m/Y');
                }
            }
        }

        $this->getResponse()
            ->appendBody(json_encode($arrResult));
    }

    /**
     * The get action handles GET requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public
    function getAction()
    {
        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);
        $result = $this->negocio->retornarDadosIdentidade($params);
        $arrResult = array();
        if ($result) {
            foreach ($result as $i => $identidade) {
                $arrResult[$i] = $this->negocio->toArrayEntity($identidade);
                if ($identidade->getDt_dataexpedicao()) {
                    $dt_dataexpedicao = $identidade->getDt_dataexpedicao();
                    $arrResult[$i]['dt_dataexpedicao'] = $dt_dataexpedicao->format('d/m/Y');
                }
            }
        }

        $this->getResponse()
            ->appendBody(json_encode($arrResult));
    }

    /**
     * The head action handles HEAD requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public
    function headAction()
    {
        // TODO: Implement headAction() method.
    }

    /**
     * The post action handles POST requests; it should accept and digest a
     * POSTed resource representation and persist the resource state.
     */
    public
    function postAction()
    {
        // TODO: Implement postAction() method.
    }

    /**
     * The put action handles PUT requests and receives an 'id' parameter; it
     * should update the server resource state of the resource identified by
     * the 'id' value.
     */
    public
    function putAction()
    {
        // TODO: Implement putAction() method.
    }

    /**
     * The delete action handles DELETE requests and receives an 'id'
     * parameter; it should update the server resource state of the resource
     * identified by the 'id' value.
     */
    public
    function deleteAction()
    {
        // TODO: Implement deleteAction() method.
    }
}
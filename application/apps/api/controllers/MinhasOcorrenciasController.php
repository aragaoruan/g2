<?php

/**
 * Description of MinhasOcorrenciasController
 *
 * @author paulo.silva
 */
class Api_MinhasOcorrenciasController extends Ead1_RestRestrita
{


    /**
     * Action Api responsavel por retornar ocorrencias atravez do formulario de consulta
     * @param json data
     * @return json data
     */
    public function indexAction()
    {
        $negocio = new G2\Negocio\Ocorrencia();
        $result = array();
        $value = new G2\Entity\VwMinhasOcorrencias();
        $config = Ead1_Ambiente::geral();


        foreach ($negocio->findMinhasOcorrencias() as $value) {
            $result[] = array(
                'st_nomeresponsavel' => $value->getSt_nomeresponsavel(),
                'st_email' => $value->getSt_email(),
                'st_evolucao' => $value->getSt_evolucao(),
                'st_categoriaocorrencia' => $value->getSt_categoriaocorrencia(),
                'st_assuntocopai' => $value->getSt_assuntocopai(),
                'id' => $value->getId_ocorrencia(),
                'st_nomeinteressado' => $value->getSt_nomeinteressado(),
                'id_ocorrencia' => (int)$value->getId_ocorrencia(),
                'st_titulo' => $value->getSt_titulo(),
                'id_evolucao' => $value->getId_evolucao(),
                'dt_cadastro' => date("d/m/Y h:i", strtotime($value->getDt_cadastro())),
                'dt_ultimotramite' => $value->getDt_ultimotramite(),
                'st_assuntoco' => $value->getSt_assuntoco(),
                'id_assuntoco' => $value->getId_assuntoco(),
                'id_matricula' => $value->getId_matricula(),
                'st_ocorrencia' => $value->getSt_ocorrencia(),
                'id_situacao' => $value->getId_situacao(),
                'id_usuariointeressado' => $value->getId_usuariointeressado(),
                'id_categoriaocorrencia' => $value->getId_categoriaocorrencia(),
                'id_saladeaula' => $value->getId_saladeaula(),
                'id_entidade' => $value->getId_entidade(),
                'id_tipoocorrencia' => $value->getId_tipoocorrencia(),
                'st_situacao' => $value->getSt_situacao(),
                'st_ultimotramite' => $value->getSt_ultimotramite(),
                'st_tramite' => $value->getSt_tramite(),
                'st_cpf' => $value->getSt_cpf(),

            );
        }
        $this->_helper->json($result);
    }

    /**
     * Action Api responsavel por criar nova ocorrencia
     * @param json data
     * @return json data
     */
    public function postAction()
    {

        //$json = $this->getRequest()->getRawBody();
        $negocio = new \G2\Negocio\Ocorrencia();

        $id_usuario = $negocio->sessao->id_usuario;
        $id_entidade = $negocio->sessao->id_entidade;

        //$data = Zend_Json_Decoder::decode($json);
        $data = $this->getRequest()->getPost();

        $data['ocorrencia']['id_usuariocadastro'] = $id_usuario;
        $data['ocorrencia']['id_usuariointeressado'] = $id_usuario;
        $data['ocorrencia']['id_entidade'] = $id_entidade;


        //Verificando se um arquivo foi enviano na interacao
        $arquivo = array_key_exists('arquivo', $_FILES) ? $_FILES['arquivo'] : null;

        if ($arquivo)
            $data['arquivo'] = $arquivo;

        $form = new OcorrenciaForm();

        if ($form->isValid($data['ocorrencia'])) {
            try {
                $mensageiro = $negocio->salvarOcorrencia($data);

                if ($mensageiro->getType() != Ead1_IMensageiro::TYPE_SUCESSO) {
                    throw new Exception($mensageiro->getText());
                }

                $toArray = $negocio->toArrayEntity($mensageiro->getFirstMensagem());
                $this->_helper->json($toArray);

            } catch (Exception $e) {
                $this->_helper->json($e);
            }

        } else {
            var_dump($form->getErrors());
            //throw new Zend_Exception('Dados incompletos');
        }

    }

    /**
     * Action Api responsavel por fazer diversas alteracoes em ocorrencia,
     * metodo construido neste formato para reutilizacao de BO
     *
     * @param json data
     * @return json data
     */
    public function putAction()
    {
        $json = $this->getRequest()->getRawBody();
        $form = new OcorrenciaForm();
        $data = Zend_Json_Decoder::decode($json);

        switch ($data['tipo_update']) {
            case 1:
                //update comum
                if ($form->isValid($data)) {
                    $negocio = new G2\Negocio\Ocorrencia();

                    try {
                        $mensageiro = $negocio->salvarOcorrencia($data);

                        if ($mensageiro->getType() != Ead1_IMensageiro::TYPE_SUCESSO) {
                            throw new Exception($mensageiro->getText());
                        }

                        $toArray = $negocio->toArrayEntity($mensageiro->getFirstMensagem());
                        $this->_helper->json($toArray);

                    } catch (Exception $e) {
                        $this->_helper->json($e);
                    }

                }
                break;

            case 2:
                //update encerramento
                $ocorrencia = new G2\Negocio\Ocorrencia();
                $entity = $ocorrencia->encerrarOcorrencia($data);
                $toArray = $ocorrencia->toArrayEntity($entity);
                $this->_helper->json($toArray);
                break;

            case 3:
                //update devolver ocorrencia
                $ocorrencia = new G2\Negocio\Ocorrencia();
                $entity = $ocorrencia->devolverOcorrencia($data);
                $toArray = $ocorrencia->toArrayEntity($entity);
                $this->_helper->json($toArray);
                break;

            case 4:
                //update reabrir ocorrencia
                $ocorrencia = new G2\Negocio\Ocorrencia();
                $entity = $ocorrencia->reabrirOcorrencia($data);
                $toArray = $ocorrencia->toArrayEntity($entity);
                $this->_helper->json($toArray);
                break;

            case 5:
                //update solicitar encerramento
                $ocorrencia = new G2\Negocio\Ocorrencia();
                $entity = $ocorrencia->solicitarEncerramento($data);
                $toArray = $ocorrencia->toArrayEntity($entity);
                $this->_helper->json($toArray);
                break;

            case 6:
                //update encaminhar ocorrencia
                $ocorrencia = new G2\Negocio\Ocorrencia();
                $entity = $ocorrencia->encaminharOcorrencia($data);
                $toArray = $ocorrencia->toArrayEntity($entity);
                $this->_helper->json($toArray);
            default:
                throw new Zend_Exception('Tipo de alteração incorreto');
        }
    }

}

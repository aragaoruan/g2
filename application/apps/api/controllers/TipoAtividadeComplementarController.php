<?php

use G2\Transformer\AtividadeComplementar\TipoAtividadeComplementarTransformer;

/**
 * Class Api_TipoAtividadeComplementarController
 */
class Api_TipoAtividadeComplementarController extends Ead1_RestRestrita
{
    /**
     * @var \G2\Negocio\AtividadeComplementar
     */
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\AtividadeComplementar();
    }

    public function deleteAction()
    {
    }

    public function getAction()
    {

    }

    public function headAction()
    {

    }

    public function indexAction()
    {
        try {
            $params = $this->getAllParams();
            unset($params['module'], $params['action'], $params['controller']);

            if (!isset($params['bl_ativo'])) {
                $params['bl_ativo'] = true;
            }
            $result = $this->negocio->findBy('\G2\Entity\TipoAtividadeComplementar', $params);

            if ($result) {
                $result = (new TipoAtividadeComplementarTransformer($result))
                    ->getTransformer();
            }

            $this->getResponse()
                ->appendBody(json_encode($result));
        } catch (Exception $e) {
            $this->getResponse()->setHttpResponseCode(500)
                ->appendBody($e->getMessage());
        }
    }

    public function putAction()
    {
        $this->postAction();
    }

    public function postAction()
    {
        try {
            $body = $this->getRequest()->getRawBody();
            $data = (Array)json_decode($body);

            $result = $this->negocio->salvarTipoAtividade($data);
            if ($result instanceof \G2\Entity\TipoAtividadeComplementar) {
                $this->getResponse()->setHttpResponseCode(201)
                    ->appendBody(json_encode($result->toBackboneArray()));
            }
        } catch (Exception $e) {
            $this->getResponse()->setHttpResponseCode(500)
                ->appendBody($e->getMessage());
        }

    }
}

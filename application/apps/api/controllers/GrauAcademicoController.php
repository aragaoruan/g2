<?php

/**
 * Controller para gerenciar graus academicos
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @since 2013-10-07
 * @package application
 * @subpackage controller
 */
class Api_GrauAcademicoController extends Ead1_RestRestrita
{

    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Contrato();
    }

    public function indexAction()
    {
        $mensageiro = $this->negocio->findAllGrausAcademicos();

        $this->getResponse()->appendBody(json_encode($mensageiro));
    }

    public function getAction()
    {

    }

    public function postAction()
    {

    }

    public function putAction()
    {
        $this->postAction();
    }

    public function deleteAction()
    {

    }

    public function headAction()
    {

    }

}

<?php


class Api_MaximizeController extends Zend_Rest_Controller
{
    protected $API_TOKEN;

    private $negocio;
    private $vwAvaliacaoAgendamento;

    public function __construct(
        Zend_Controller_Request_Abstract $request,
        Zend_Controller_Response_Abstract $response,
        array $invokeArgs = [])
    {
        parent::__construct($request, $response, $invokeArgs);
        $front = $this->getFrontController();
        $bootstrap = $front->getParam('bootstrap');

        $this->negocio = new \G2\Negocio\Maximize();
        $this->vwAvaliacaoAgendamento = new \G2\Entity\VwAvaliacaoAgendamento();
        //pega o api token que foi definido no application.ini
        $this->API_TOKEN = $bootstrap->getOption("maximizeApiToken");

        //verifica se não tem valor e gera a exception
        if (!$this->API_TOKEN) {
            throw new Exception("API TOKEN não definido, verifique as configurações do sistema.");
        }
    }

    public function init()
    {
        parent::init();
        $this->_helper->viewRenderer->setNoRender();
    }

    /**
     * Prepara um Response Comum para WebServices
     * @param $httpCode
     * @param $data
     */
    private function response($httpCode, $data)
    {
        if (is_array($data)) {
            $data = Zend_Json::encode($data);
        }

        $this->getResponse()
            ->setHttpResponseCode($httpCode)
            ->setHeader('Content-Type', 'application/json')
            ->setBody($data);
    }

    /**
     * Verifica se o usuário está autorizado pelo Token-Autenticacao
     * @return bool
     */
    private function authorize()
    {
        $apiToken = $this->getRequest()->getHeader('Token-Autenticacao');

        return $apiToken == $this->API_TOKEN;
    }

    /**
     * Recebe a requisição POST da Maximize para importação de notas
     */
    public final function postAction()
    {
        if (!$this->authorize()) {
            return $this->response(403, 'Não autorizado.');
        }

        try {
            $data = Zend_Json::decode($this->getRequest()->getRawBody());
            if ($this->negocio->validateNota($data)) {
                $where = array(
                    'cod_prova' => $data['codProva'],
                    'id_usuario' => $data['codigoUsuarioSistemaOrigem'],
                );

                /*
                 * TODO Foi solicitado pelo academico a alteração no processamento da nota, onde pegavamos a "nota"
                 * TODO que vinha da maximize e agora querem que seja capturado como nota a "pontuacaoObtida",
                 * TODO posteriormente isso deve ser evoluido para pegar de acordo com alguma configuração que será definido
                 */
                $getAvaliacao = $this->negocio->getAvaliacaoAgendamento($where);
                $result = $this->negocio
                    ->salvarNotas(
                        $this->negocio->prepararNotas($data['pontuacaoObtida'], $getAvaliacao),
                        $getAvaliacao->getId_matricula(),
                        $getAvaliacao->getId_entidade()
                    );


                if ($result->getTipo() === \Ead1_IMensageiro::SUCESSO) {
                    return $this->response(200, array("msg" => "Nota importada com sucesso"));
                }

                return $this->response(503, array(
                        'error' => $result->getText(),
                        'codProva' => $data['codProva'],
                        'codigoUsuarioSistemaOrigem' => $data['codigoUsuarioSistemaOrigem'])
                );
            }
            return $this->response(400, 'Bad Request');
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * The index action handles index/list requests; it should respond with a
     * list of the requested resources.
     */
    public function indexAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
    }

    /**
     * The get action handles GET requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function getAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
    }

    /**
     * The head action handles HEAD requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function headAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
    }

    /**
     * The put action handles PUT requests and receives an 'id' parameter; it
     * should update the server resource state of the resource identified by
     * the 'id' value.
     */
    public function putAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
    }

    /**
     * The delete action handles DELETE requests and receives an 'id'
     * parameter; it should update the server resource state of the resource
     * identified by the 'id' value.
     */
    public function deleteAction()
    {
        $this->getResponse()->setHttpResponseCode(404);
    }

    public function optionsAction()
    {
        $this->getResponse()->setBody(null);
        $this->getResponse()->setHeader('Allow', 'OPTIONS, POST');
    }
}

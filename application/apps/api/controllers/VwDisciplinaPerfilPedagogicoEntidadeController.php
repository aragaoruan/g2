<?php

/**
 * Controller para gerenciar as VwDisciplinaPerfilPedagogicoEntidade
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2015-01-09
 * @package application
 * @subpackage controller
 */

class Api_VwDisciplinaPerfilPedagogicoEntidadeController extends Ead1_RestRestrita
{

    private $negocio;
    private $repositoryName = '\G2\Entity\VwDisciplinaPerfilPedagogicoEntidade';

    public function init ()
    {
        parent::init();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->negocio = new \G2\Negocio\Negocio();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['module'], $params['controller'], $params['action']);

        if (!isset($params['id_entidade']) || !$params['id_entidade'])
            $params['id_entidade'] = $this->negocio->sessao->id_entidade;

        $mensageiro = $this->negocio->findBy($this->repositoryName, $params, array('st_disciplina' => 'ASC'));
        $results = array();
        foreach ($mensageiro as $entity) {
            $results[$entity->getId_disciplina()] = $this->negocio->toArrayEntity($entity);
        }

        $this->_helper->json(array_values($results));
    }

    public function getAction()
    {
        $id = $this->getParam('id');

        if ($id) {
            $mensageiro = $this->negocio->find($this->repositoryName, $id);
            $mensageiro = $this->negocio->toArrayEntity($mensageiro);

            $this->_helper->json($mensageiro);
        }
    }

    public function postAction()
    {
        $data = Zend_Json::decode($this->getRequest()->getRawBody());

        $mensageiro = $this->negocio->save($data);
        $this->_helper->json($mensageiro);
    }

    public function putAction()
    {
        $this->postAction();
    }

    public function deleteAction()
    {
        $id = $this->getParam('id');

        if ($id) {
            $entity = $this->negocio->find($this->repositoryName, $id);

            $mensageiro = $this->negocio->delete($entity);
            $this->_helper->json($mensageiro);
        }
    }

    public function headAction()
    {
    }

}
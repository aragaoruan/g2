<?php

/**
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */
class Api_MensagemCobrancaController extends Ead1_RestRestrita {

    private $negocio;

    public function init() {
        parent::init();
        $this->negocio = new G2\Negocio\MensagemCobranca();
    }

    public function deleteAction() {
        $this->negocio->delete($this->getParam('id'));
        $this->getResponse()->appendBody('ok')
                ->setHttpResponseCode(200);
    }

    public function getAction() {
        
    }

    public function headAction() {
        
    }

    public function indexAction() {
        $return = $this->negocio->findBy('G2\Entity\MensagemCobranca', array('id_entidade' => Ead1_Sessao::getSessaoGeral()->id_entidade));

        $arrReturn = array();
        if ($return) {
            foreach ($return as $row) {

                $dt_cadastro = '';

                if ($row->getDt_cadastro()) {
                    $dt_cadastro = new DateTime($row->getDt_cadastro());
                    $dt_cadastro = $dt_cadastro->format('d/m/Y');
                }

                $arrReturn[] = array(
                    'id_mensagemcobranca' => $row->getId_mensagemcobranca(),
                    'id_textosistema' => $row->getId_textosistema()->getId_textosistema(),
                    'st_textosistema' => $row->getId_textosistema()->getSt_textosistema(),
                    'id_entidade' => $row->getId_entidade()->getId_entidade(),
                    'nu_diasatraso' => (int) $row->getNu_diasatraso(),
                    'dt_cadastro' => $dt_cadastro,
                    'bl_ativo' => $row->getBl_ativo(),
                    'bl_antecedencia' => $row->getBl_antecedencia()
                );
            }
        }
        $this->getResponse()
                ->appendBody(json_encode($arrReturn));
    }

    public function postAction() {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
        $return = $this->negocio->save($data);

        $this->getResponse()->appendBody(json_encode(array(
            'id' => $return->getId_mensagemcobranca(),
            'type' => 'success',
            'title' => 'Registro Atualizado',
            'text' => 'O registro foi atualizado com sucesso!'
        )));
        $this->getResponse()->setHttpResponseCode(201);
    }

    public function putAction() {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);
        $return = $this->negocio->save($data);

        $this->getResponse()->appendBody(json_encode(array(
            'id' => $return->getId_mensagemcobranca(),
            'type' => 'success',
            'title' => 'Registro Atualizado',
            'text' => 'O registro foi atualizado com sucesso!'
        )));
        $this->getResponse()->setHttpResponseCode(201);
    }

}

<?php

/**
 * Controller para VwHistoricoAgendamento
 * @author Debora Castro <debora.castro@unyleya.com.br>
 * @since 08/06/2015
 * @package application
 * @subpackage controller
 */
class Api_VwHistoricoAgendamentoController extends Ead1_RestRestrita
{

    private $doctrineContainer;
    private $em;

    public function init()
    {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
    }

    public function indexAction()
    {
        $negocio = new \G2\Negocio\GerenciaProva();
        $params = $this->getRequest()->getParams();
        $array = array();

        unset($params['module']);
        unset($params['action']);
        unset($params['controller']);

        $retorno = $negocio->findByVwHistoricoAgendamento($params);
        if (is_array($retorno)) {
            foreach ($retorno as $key => $result) {
                $array[] = $negocio->toArrayEntity($result);
                $array[$key]['dt_cadastro']  =  ($result->getDt_cadastro()!='') ? (date_format($result->getDt_cadastro(), 'd/m/Y')) : 'Sem data definida';
            }
        }

        $this->_helper->json($array);
    }

    public function getAction()
    {

    }

    public function postAction()
    {

    }

    public function putAction()
    {

    }

    public function deleteAction()
    {

    }

    public function headAction()
    {

    }

}

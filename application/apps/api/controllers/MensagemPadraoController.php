<?php

use G2\Negocio\EmailEntidadeMensagem;

/**
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Api_MensagemPadraoController extends Ead1_RestRestrita
{
    /** @var \G2\Negocio\EmailEntidadeMensagem $negocio * */
    private $negocio;

    public function init ()
    {
        parent::init();
        $this->negocio = new EmailEntidadeMensagem();
    }

    public function deleteAction ()
    {

    }

    public function getAction ()
    {

    }

    public function headAction ()
    {

    }

    public function indexAction ()
    {
        $return = $this->negocio->findEmailEntidadeMensagemByEntidadeSessao();
        $arrReturn = array();
        if ($return) {
            foreach ($return as $row) {
                $arrReturn[] = array(
                    'id' => $row->getId_mensagempadrao(),
                    'st_mensagempadrao' => $row->getSt_mensagempadrao(),
                    'id_textosistema' => $row->getId_textosistema(),
                    'st_textosistema' => $row->getSt_textosistema(),
                    'id_emailconfig' => $row->getId_emailconfig(),
                    'st_conta' => $row->getSt_conta(),
                    'id_textocategoria' => $row->getId_textocategoria() ? $row->getId_textocategoria()->getId_textocategoria() : null,
                    'st_textocategoria' => $row->getId_textocategoria() ? $row->getId_textocategoria()->getSt_textocategoria() : null
                );
            }
        }
        $this->_helper->json($arrReturn);
    }

    public function postAction ()
    {

    }

    public function putAction ()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);

        $return = $this->negocio->save($data);

        $this->getResponse()->appendBody(json_encode(array(
            'id' => $return->getId_mensagempadrao(),
            'type' => 'success',
            'title' => 'Registro Atualizado',
            'text' => 'O registro foi atualizado com sucesso!'
        )));
        $this->getResponse()->setHttpResponseCode(201);
    }

}

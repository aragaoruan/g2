<?php

/**
 * Controller para gerenciar Funcao
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2015-01-01
 * @package application
 * @subpackage controller
 */
class Api_FuncaoController extends Ead1_RestRestrita
{

    private $bo;

    public function init()
    {
        parent::init();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->bo = new FuncaoBO();
    }

    public function indexAction()
    {
        $data = $this->getAllParams();

        $to = new FuncaoTO();
        foreach ($data as $key => $value) {
            if (array_key_exists($key, $to)) {
                $to->{$key} = $value;
            }
        }

        $mensageiro = $this->bo->retornarFuncao($to);
        $this->_helper->json($mensageiro);
    }

    public function getAction()
    {
        $id = $this->getParam('id');
        $id_tipoFuncao = $this->getParam('id_tipofuncao');

        if ($id) {
            $to = new FuncaoTO();
            $to->setId_funcao($id);

            $mensageiro = $this->bo->retornarFuncao($to);

            if ($mensageiro->type == 'success' && is_array($mensageiro->mensagem))
                $mensageiro->mensagem = array_shift($mensageiro->mensagem);

            $this->_helper->json($mensageiro);

        } else if ($id_tipoFuncao) {
            $to = new FuncaoTO();
            $to->setId_tipofuncao($id_tipoFuncao);

            $mensageiro = $this->bo->retornarFuncao($to);

            $this->_helper->json($mensageiro->mensagem);
        }
    }

}
<?php

/**
 * @package application
 * @subpackage controller
 * @autor: Débora Castro <debora.castro@unyleya.com>
 */
class Api_VwAvaliacaoConjuntoDisciplinaController extends Ead1_RestRestrita {

    private $doctrineContainer;
    private $em;
    private $bo;

    public function init() {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
        $this->bo = new \G2\Negocio\VwAvaliacaoConjuntoDisciplina();
    }

    public function indexAction() {

        $params = $this->getRequest()->getParams();

        unset($params['module']);
        unset($params['action']);
        unset($params['controller']);
        $params['id_entidade'] = $this->bo->sessao->id_entidade;
//        Zend_Debug::dump($params);die;
        $mensageiro = $this->bo->findByVw($params);

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $mensageiro->setText(count($mensageiro->getMensagem()) . ' registros encontrados!');
        }

        $this->getResponse()
                ->appendBody(json_encode($mensageiro->getMensagem()));
    }

    public function getAction() {

    }

    public function postAction() {
        
    }

    public function putAction() {
        
    }

    public function deleteAction() {
        
    }

    public function headAction() {
        
    }

}

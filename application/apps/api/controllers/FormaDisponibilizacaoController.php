<?php

/**
 * Class Api_FormaDisponibilizacaoController
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */
class Api_FormaDisponibilizacaoController extends Ead1_RestRestrita
{

    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Produto();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);

        $result = $this->negocio->findAllFormaDisponibilizacao();
        $this->getResponse()
            ->appendBody(json_encode($result));
    }

    public function getAction()
    {
        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);

        $result = $this->negocio->findByFormaDisponibilizacao();
        $this->getResponse()
            ->appendBody(json_encode($result));
    }

    public function headAction()
    {
        // TODO: Implement headAction() method.
    }

    public function postAction()
    {
        // TODO: Implement postAction() method.
    }

    public function putAction()
    {
        // TODO: Implement putAction() method.
    }

    public function deleteAction()
    {
        // TODO: Implement deleteAction() method.
    }

}
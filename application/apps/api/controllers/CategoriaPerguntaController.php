<?php

/**
 * Class Api_CategoriaPerguntaController
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Api_CategoriaPerguntaController extends Ead1_RestRestrita
{
    private $_negocio;

    public function init()
    {
        parent::init();
        $this->_negocio = new \G2\Negocio\PerguntasFrequentes();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        unset($params['controller'], $params['action'], $params['module']);
        $result = $this->_negocio->retornarCategorias($params, true);
        $this->getResponse()
            ->appendBody(json_encode($result));
    }

    public function getAction()
    {
    }

    public function headAction()
    {

    }

    public function postAction()
    {

    }

    public function putAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json_Decoder::decode($body);
        $result = $this->_negocio->salvarCategoriaPergunta($data);
        if ($result) {
            $this->getResponse()->setHttpResponseCode(201)->appendBody(json_encode($data));
        } else {
            $this->getResponse()->setHttpResponseCode(400)->setBody("Erro ao salvar categorias.");
        }
    }

    public function deleteAction()
    {
        $id = intval($this->getParam('id'));
        if (!is_int($id)) {
            $this->getResponse()->setHttpResponseCode(400)->setBody('Não posso apagar um registro com ID inválido!');
        } else {
            $result = $this->_negocio->apagarCategoriaPergunta($id);
            if ($result) {
                $this->getResponse()->setHttpResponseCode(204)->appendBody('Registro removido');
            } else {
                $this->getResponse()->setHttpResponseCode(400)->appendBody('Erro ao tentar remover registro');
            }
        }
    }
}
<?php

/**
 * Controller API para Matrícula Institucional
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since  18-09-2014
 * @package application
 * @subpackage controller
 */
class Api_MatriculaInstitucionalController extends Ead1_RestRestrita
{
    private $negocio;

    public function init()
    {
        parent::init();
        $this->negocio = new \G2\Negocio\Negocio();
    }

    public function indexAction()
    {
    }

    public function getAction()
    {
    }

    public function postAction()
    {

        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);

        $to = new MatriculaTO();
        $to->setId_projetopedagogico($data['id_projeto']);
        $to->setId_usuario($data['id_usuario']);
        $to->setId_turma($data['id_turma']!='' ? $data['id_turma'] : null);

        $bo = new MatriculaBO();
        $mensageiro = $bo->matricularDireto($to);

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->appendBody(json_encode($data));
            $this->getResponse()->setHttpResponseCode(200);
        } else {
            $this->getResponse()->appendBody(json_encode(array('erro' => $mensageiro->getMensagem())));
            $this->getResponse()->setHttpResponseCode(500);
        }

    }

    public function putAction()
    {
    }

    public function deleteAction()
    {
    }

    public function headAction()
    {
    }

}

<?php

/**
 * Controller para Pessoa Jurídica
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 * @package default
 * @subpackage controller
 * @since 2013-12-09
 */
use G2\Negocio\Conta;

class Api_ContaEntidadeController extends Ead1_RestRestrita
{

    private $_negocio;


    public function indexAction()
    {
        $params = $this->getAllParams();

        if (isset($params['id_entidade'])) {
            $negocio = new Conta();

            $resultContas = $negocio->retornaContaEntidade($params['id_entidade']);

            $this->getResponse()->appendBody(json_encode($resultContas));
        }
    }

    public function deleteAction()
    {

    }

    public function getAction()
    {

    }

    public function headAction()
    {

    }

    public function postAction()
    {

    }

    public function putAction()
    {

    }
}
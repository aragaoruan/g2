<?php
/**
 * Created by PhpStorm.
 * @author Ilson Nóbrega
 * Date: 19/08/14
 * Time: 14:41
 */

class  Api_VwMeioPagamentoIntegracaoController extends Ead1_RestRestrita{

    /**
     * The index action handles index/list requests; it should respond with a
     * list of the requested resources.
     */
    public function indexAction()
    {

        $to = new VwMeioPagamentoIntegracaoTO();

        $bo = new FormaPagamentoBO();
        $mensageiro = $bo->retornarVwMeioPagamentoIntegracao($to);

        $this->_helper->json($mensageiro->getMensagem());

    }

    /**
     * The get action handles GET requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function getAction()
    {
        $id_sistema = $this->getRequest()->getParam('id_sistema');

        $to = new VwMeioPagamentoIntegracaoTO();
        $id_entidade = $to->getSessao()->id_entidade;
        $to->setId_sistema($id_sistema);
        $to->setId_entidade($id_entidade);

        $bo = new FormaPagamentoBO();
        $mensageiro = $bo->retornarVwMeioPagamentoIntegracao($to);

//        $this->_helper->json($mensageiro->getMensagem());
        $this->getResponse()
            ->appendBody(json_encode($mensageiro->getMensagem()));
    }

    /**
     * The head action handles HEAD requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function headAction()
    {
        // TODO: Implement headAction() method.
    }

    /**
     * The post action handles POST requests; it should accept and digest a
     * POSTed resource representation and persist the resource state.
     */
    public function postAction()
    {

    }

    /**
     * The put action handles PUT requests and receives an 'id' parameter; it
     * should update the server resource state of the resource identified by
     * the 'id' value.
     */
    public function putAction()
    {
        // TODO: Implement putAction() method.
    }

    /**
     * The delete action handles DELETE requests and receives an 'id'
     * parameter; it should update the server resource state of the resource
     * identified by the 'id' value.
     */
    public function deleteAction()
    {
        // TODO: Implement deleteAction() method.
    }
}
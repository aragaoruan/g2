<?php

/**
 * Controller API para lançar nota de provas presenciais por aluno (final e recuperação)
 * @author Débora Castro <debora.castro@unyleya.com.br>
 * @since  05-02-2014
 * @package application
 * @subpackage controller
 */
class Api_LancamentoPorAlunoController extends Ead1_RestRestrita {

    private $negocio;

    public function init() {
        parent::init();
        $this->negocio = new \G2\Negocio\AvaliacaoAluno();
    }

    public function indexAction() {
        
    }

    public function getAction() {
        
    }

    public function postAction() {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json_Decoder::decode($body);
        $this->getResponse()->appendBody(($this->negocio->salvarNotaAvaliacaoPresencial($data)->toJson()));
        $this->getResponse()->setHttpResponseCode(201);
    }

    public function putAction() {
        
    }

    public function deleteAction() {
 
    }

    public function headAction() {
        
    }

}

<?php
/**
 * Controller para Entidades de Integração
 * @author Ilson Nóbrega <ilson.nobrega@unyleya.com.br>
 * @package default
 * @subpackage controller
 * @since 2014-8-27
 */

class Api_EntidadeIntegracaoController extends Ead1_RestRestrita{


    public function init ()
    {
        parent::init();
        $this->_negocio = new \G2\Negocio\EntidadeIntegracao();
    }

    /**
     * The index action handles index/list requests; it should respond with a
     * list of the requested resources.
     */
    public function indexAction(){

        $param = $this->getAllParams();

        $to = new EntidadeIntegracaoTO($param);
        $bo = new EntidadeBO();
        $mensageiro = $bo->retornaEntidadeIntegracao($to);
        if($mensageiro->getTipo()==\Ead1_IMensageiro::SUCESSO){
            $this->_helper->json($mensageiro->getMensagem());
        } else {
            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json($mensageiro->toArray());
        }

    }

    /**
     * The get action handles GET requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function getAction()
    {
        $param = $this->getRequest()->getParam('id_sistema');

        $to = new EntidadeIntegracaoTO();
        $to->setId_sistema($param);

        $bo = new EntidadeBO();
        $return = $bo->retornaEntidadeIntegracao($to);

//        $this->_helper->json($return->getMensagem());
        $this->getResponse()
            ->appendBody(json_encode($return->getMensagem()));
    }

    /**
     * The head action handles HEAD requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function headAction()
    {
        // TODO: Implement headAction() method.
    }

    /**
     * The post action handles POST requests; it should accept and digest a
     * POSTed resource representation and persist the resource state.
     */
    public function postAction()
    {
        // TODO: Implement postAction() method.
    }

    /**
     * The put action handles PUT requests and receives an 'id' parameter; it
     * should update the server resource state of the resource identified by
     * the 'id' value.
     */
    public function putAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);

        $idUsuario = Zend_Auth::getInstance()->getIdentity();

        $to = new EntidadeIntegracaoTO();
        $to->setId_sistema($data['id_sistema']);
        $to->setId_entidade($data['id_entidade']);
        $to->setId_usuariocadastro($idUsuario);
        $to->setId_entidadeintegracao($data['id_entidadeintegracao']);
        $to->setSt_codsistema($data['st_codsistema']);
        $to->setDt_cadastro(new Zend_Date());
        $to->setSt_codchave($data['st_codchave']);
        $to->setSt_codarea($data['st_codarea']);
        $to->setNu_perfilsuporte($data['nu_perfilsuporte']);
        $to->setNu_perfilprojeto($data['nu_perfilprojeto']);
        $to->setNu_perfildisciplina($data['nu_perfildisciplina']);
        $to->setNu_perfilobservador($data['nu_perfilobservador']);
        $to->setNu_perfilalunoobs($data['nu_perfilalunoobs']);
        $to->setNu_perfilalunoencerrado($data['nu_perfilalunoencerrado']);
        $to->setNu_contexto($data['nu_contexto']);
        //$to->montaToDinamico($data);

        $bo = new EntidadeBO();
        $return = $bo->salvaEntidadeIntegracao($to);

        $this->_helper->json($return->getMensagem());
    }

    /**
     * The delete action handles DELETE requests and receives an 'id'
     * parameter; it should update the server resource state of the resource
     * identified by the 'id' value.
     */
    public function deleteAction()
    {
        // TODO: Implement deleteAction() method.
    }
}
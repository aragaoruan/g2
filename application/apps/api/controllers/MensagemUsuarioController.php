<?php

/**
 * Class Api_MensagemUsuario
 *
 * @author Débora Castro
 */
class Api_MensagemUsuarioController extends Ead1_RestRestrita
{

    private $_negocio;

    public function init()
    {
        parent::init();
        $this->_negocio = new \G2\Negocio\MensagemUsuario();
    }

    public function deleteAction()
    {

    }

    public function getAction()
    {
        $args = $this->getAllParams();
        unset($args['controller'], $args['action'], $args['module']);

        $result = $this->_negocio->retonarMensagensUsuario($args);

        $this->getResponse()->appendBody(json_encode($result));
    }

    public function headAction()
    {

    }

    public function indexAction()
    {
        $args = $this->getAllParams();
        unset($args['controller'], $args['action'], $args['module']);

        $resultado = $this->_negocio->retonarMensagensUsuario($args);

        $this->getResponse()->appendBody(json_encode($resultado));
    }

    public function postAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);

        $resultado = $this->_negocio->atualizarMensagemUsuario($data);

        $this->getResponse()->appendBody(json_encode($resultado));
        $this->getResponse()->setHttpResponseCode(201);
    }

    public function putAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);

        $resultado = $this->_negocio->atualizarMensagemUsuario($data);

        $this->getResponse()->appendBody(json_encode($resultado));
        $this->getResponse()->setHttpResponseCode(201);
    }
}
<?php

class Api_AplicadorProvaEntidadeController extends Ead1_RestRestrita
{

    /**
     * @var \G2\Negocio\AplicadorProva $negocio
     */
    private $negocio;

    private $repositoryName = '\G2\Entity\AplicadorProvaEntidade';

    public function init()
    {
        parent::init();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->negocio = new \G2\Negocio\AplicadorProva();
    }

    public function indexAction()
    {
        $params = $this->getAllParams();
        $id_entidadepai = $this->getParam('id_entidadepai');

        unset($params['module'], $params['controller'], $params['action'], $params['id_entidadepai']);

        $results = $this->negocio->retornarAplicadorEntidadeRecursiva($params, $id_entidadepai);

        foreach ($results as &$entity) {
            $entity = $this->_model($entity);
        }

        $this->_helper->json($results);
    }

    public function getAction()
    {
        $entity = $this->negocio->find($this->repositoryName, $this->getParam('id'));

        $result = $entity ? $this->_model($entity) : new stdClass();

        $this->_helper->json($result);
    }

    private function _model($entity)
    {
        return array(
            'id_aplicadorprovaentidade' => $entity->getId_aplicadorprovaentidade(),
            'id_aplicadorprova' => $entity->getId_aplicadorprova()->getId_aplicadorprova(),
            'st_aplicadorprova' => $entity->getId_aplicadorprova()->getSt_aplicadorprova(),
            'id_entidade' => $entity->getId_entidade()->getId_entidade(),
            'st_entidade' => $entity->getId_entidade()->getSt_nomeentidade()
        );
    }
}

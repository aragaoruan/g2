<?php

/**
 * Classe API Rest para Formas de Pagamento
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-03-05
 */
use G2\Negocio\FormaPagamento;

class Api_FormaPagamentoController extends Ead1_RestRestrita
{

    private $negocio;

    public function init ()
    {
        parent::init();
        $this->negocio = new FormaPagamento();
    }

    public function getAction()
    {
        $id = (int) $this->getRequest()->getParam('id');

        if ($id) {
            $negocio = new \G2\Negocio\Negocio();

            $result = $negocio->find('\G2\Entity\FormaPagamento', $id);
            $result = $negocio->toArrayEntity($result);
            $result['id_formapagamentoaplicacao'] = 1;
            $result['meios_pagamentos'] = array();
            $result['parcelas'] = array();
            $result['produtos'] = array();

            // Buscar a aplicacao, apesar de ter tabela para relacionamento muitos pra muitos,
            // o relacionamento é feito so 1x
            $apliacacao = $negocio->findOneBy('\G2\Entity\FormaPagamentoAplicacaoRelacao', array(
                'id_formapagamento' => $id
            ));
            if ($apliacacao)
                $result['id_formapagamentoaplicacao'] = $apliacacao->getId_formapagamentoaplicacao();


            // Buscar meios de pagamentos de Entrada
            $pagamentos = $negocio->findBy('\G2\Entity\FormaMeioPagamento', array(
                'id_formapagamento' => $id
            ));
            foreach ($pagamentos as $pagamento) {
                $result['meios_pagamentos'][] = $pagamento->getId_meiopagamento();
            }


            // Buscar parcelamentos associados
            $parcelas = $negocio->findBy('\G2\Entity\FormaPagamentoParcela', array(
                'id_formapagamento' => $id
            ));
            foreach ($parcelas as $parcela) {
                $indexes = $negocio->toArrayEntity($parcela);
                $indexes['id_meiopagamento'] = array(
                    'id_meiopagamento' => $parcela->getId_meiopagamento()->getId_meiopagamento(),
                    'st_meiopagamento' => $parcela->getId_meiopagamento()->getSt_meiopagamento()
                );
                $result['parcelas'][] = $indexes;
            }


            // Buscar produtos associados
            $produtos = $negocio->findBy('\G2\Entity\FormaPagamentoProduto', array(
                'id_formapagamento' => $id
            ));
            foreach ($produtos as $produto) {
                $result['produtos'][] = $produto->getId_produto();
            }

            $this->getResponse()->appendBody(json_encode($result));
        }
    }


    public function headAction ()
    {
        
    }

    public function indexAction ()
    {
        $result = $this->negocio->retornaFormaPagamentoByEntidade();
        $arrResult = array();
        if ($result) {
            foreach ($result as $row) {
                $arrResult[] = array(
                    'id_formapagamento' => $row->getId_formapagamento(),
                    'st_formapagamento' => $row->getSt_formapagamento(),
                    'id_situacao' => $row->getId_situacao()->getId_situacao(),
                    'st_descricao' => $row->getSt_descricao(),
                    'nu_entradavalormin' => $row->getNu_entradavalormin(),
                    'nu_entradavalormax' => $row->getNu_entradavalormax(),
                    'nu_valormin' => $row->getNu_valormin(),
                    'nu_valormax' => $row->getNu_valormax(),
                    'nu_valorminparcela' => $row->getNu_valorminparcela(),
                    'dt_cadastro' => $row->getDt_cadastro(),
                    'id_usuariocadastro' => $row->getId_usuariocadastro(),
                    'id_tipoformapagamentoparcela' => $row->getId_tipoformapagamentoparcela()->getId_tipoformapagamentoparcela(),
                    'id_entidade' => $row->getId_entidade(),
                    'bl_todosprodutos' => $row->getBl_todosprodutos(),
                    'nu_multa' => $row->getNu_multa(),
                    'nu_juros' => $row->getNu_juros(),
                    'nu_maxparcelas' => $row->getNu_maxparcelas(),
                    'nu_jurosmax' => $row->getNu_jurosmax(),
                    'nu_jurosmin' => $row->getNu_jurosmin(),
                    'id_tipocalculojuros' => $row->getId_tipocalculojuros()->getId_tipocalculojuros()
                );
            }
        }
        $this->getResponse()->appendBody(json_encode($arrResult));
    }

    public function postAction()
    {
        $body = $this->getRequest()->getRawBody();
        $data = Zend_Json::decode($body);

        $mensageiro = $this->negocio->save($data);

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->appendBody(json_encode($mensageiro->toArray()));
            $this->getResponse()->setHttpResponseCode(200);
        } else {
            $this->getResponse()->appendBody(json_encode(array('erro' => $mensageiro->getMensagem())));
            $this->getResponse()->setHttpResponseCode(500);
        }
    }

    public function putAction()
    {
        $this->postAction();
    }

    public function deleteAction ()
    {
        $id = (int) $this->getRequest()->getParam('id');

        if ($id) {

            $negocio = new \G2\Negocio\Negocio();

            /**
             * FormaPagamentoAplicacaoRelacao
             */
            // Buscar FormaPagamentoAplicacaoRelacao e remover todos
            $ItemsEntity = $negocio->findBy('\G2\Entity\FormaPagamentoAplicacaoRelacao', array(
                'id_formapagamento' => $id
            ));
            // Apagar o registro encontrado
            foreach ($ItemsEntity as $ItemEntity) {
                $negocio->delete($ItemEntity);
            }


            /**
             * FormaMeioPagamento
             */
            // Buscar FormaMeioPagamento e remover todos
            $ItemsEntity = $negocio->findBy('\G2\Entity\FormaMeioPagamento', array(
                'id_formapagamento' => $id
            ));
            // Apagar cada registro encontrado
            foreach ($ItemsEntity as $ItemEntity) {
                $negocio->delete($ItemEntity);
            }


            /**
             * FormaPagamentoProduto
             */
            // Buscar FormaPagamentoProduto e remover todos
            // para poder inserir somente os selecionados
            $ItemsEntity = $negocio->findBy('\G2\Entity\FormaPagamentoProduto', array(
                'id_formapagamento' => $id
            ));
            // Apagar cada registro encontrado
            foreach ($ItemsEntity as $ItemEntity) {
                $negocio->delete($ItemEntity);
            }


            /**
             * FormaPagamentoParcela
             */
            // Buscar FormaPagamentoParcela e remover todos
            $ItemsEntity = $negocio->findBy('\G2\Entity\FormaPagamentoParcela', array(
                'id_formapagamento' => $id
            ));
            // Apagar cada registro encontrado
            foreach ($ItemsEntity as $ItemEntity) {
                $negocio->delete($ItemEntity);
            }


            /**
             * FormaPagamento
             */
            // Apagar a FormaPagamento
            $entity = $negocio->find('\G2\Entity\FormaPagamento', $id);

            try {
                $negocio->delete($entity);
                $this->getResponse()->setHttpResponseCode(204);
            } catch (Zend_Exception $e) {
                $this->getResponse()->setHttpResponseCode(500);
            }

        }
    }

}

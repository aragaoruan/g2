<?php

/**
 * Controller para gerenciar os processos Seletivos
 * @author Helder Fernandes <helder.silva@unyleya.com.br>
 * @since 2015-12-15
 * @package application
 * @subpackage controller
 */
class Api_TipoSelecaoController extends Ead1_RestRestrita
{
    /** @var  \G2\Negocio\TipoSelecao */
    private $negocio;

    public function init()
    {
        $this->negocio = new \G2\Negocio\TipoSelecao();
        parent::init();
    }

    public function indexAction()
    {
        try {
            $params = $this->getAllParams();
            $result = $this->negocio->retornarPesquisa($params, array('st_tiposelecao' => 'ASC'));
            $this->getResponse()
                ->appendBody(json_encode($result));
        } catch (Exception $e) {
            $this->getResponse()->setHttpResponseCode(400)
                ->appendBody($e->getMessage());
        }
    }

    public function getAction()
    {


    }

    public function postAction()
    {
        try {
            $body = $this->getRequest()->getRawBody();
            $data = Zend_Json::decode($body);
            $result = $this->negocio->salvarDados($data);
            if ($result instanceof \G2\Entity\TipoSelecao) {
                $this->getResponse()->setHttpResponseCode(201)
                    ->appendBody(json_encode($result->toBackboneArray()));
            }
        } catch (Exception $e) {
            $this->getResponse()->setHttpResponseCode(400)
                ->appendBody($e->getMessage());
        }
    }

    public function putAction()
    {
        $this->postAction();
    }

    public function deleteAction()
    {
        $idTipoSelecao = intval($this->getParam('id'));
        try {

            $result = $this->negocio->apagarTipoSelecao($idTipoSelecao);
            if ($result instanceof \G2\Entity\TipoSelecao) {
                $this->getResponse()->setHttpResponseCode(204)->appendBody('Registro removido com sucesso!');
            }

        } catch (Exception $e) {
            $this->getResponse()->setHttpResponseCode(400)
                ->appendBody($e->getMessage());
        }
    }

    public function headAction()
    {

    }


}

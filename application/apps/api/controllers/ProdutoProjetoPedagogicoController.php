<?php

/**
 * Controller para gerenciar os ProdutoProjetoPedagogico
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @since 2013-11-22
 * @package application
 * @subpackage controller
 */
class Api_ProdutoProjetoPedagogicoController extends Ead1_RestRestrita
{

    private $doctrineContainer;
    private $em;

    public function init()
    {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
    }

    public function indexAction()
    {
        $array = $this->getAllParams();
        unset($array['module'], $array['controller'], $array['action']);

        if (!$array) {
            $this->_helper->json(array());
            exit;
        }

        $produto = new \G2\Negocio\Produto();
        $mensageiro = $produto->findProdutoProjetoPedagogicoByArray($array);

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $mensageiro->setText(count($mensageiro->getMensagem()) . ' registros encontrados!');
        }

        $this->_helper->json($mensageiro->getMensagem());
    }

    public function getAction()
    {
        $produto = new \G2\Negocio\Produto();
        $mensageiro = $produto->findProdutoProjetoPedagogicoByArray(array('id_produto' => $this->_getParam('id')));

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $mensageiro->setText(count($mensageiro->getMensagem()) . ' registros encontrados!');
        }

        $this->_helper->json($mensageiro->getFirstMensagem());

    }

    public function postAction()
    {

        try {

            $body = $this->getRequest()->getRawBody();
            $data = Zend_Json::decode($body);
            $produto = new \G2\Negocio\Produto();
            $mensageiro = $produto->salvarProdutoProjetoPedagogico($data);

        } catch (Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        if ($mensageiro->getTipo() == \Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->setHttpResponseCode(201);
            $this->_helper->json($mensageiro->getFirstMensagem()->tobackBoneArray());
        } else {
            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json($mensageiro);
        }

    }

    public function putAction()
    {
        try {

            $body = $this->getRequest()->getRawBody();
            $data = Zend_Json::decode($body);
            $produto = new \G2\Negocio\Produto();
            $mensageiro = $produto->salvarProdutoProjetoPedagogico($data);
        } catch (Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        if ($mensageiro->getTipo() == \Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->setHttpResponseCode(201);
            $this->_helper->json($mensageiro->getFirstMensagem()->toBackboneArray());
        } else {
            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json($mensageiro);
        }

    }

    public function deleteAction()
    {

    }

    public function headAction()
    {

    }


}
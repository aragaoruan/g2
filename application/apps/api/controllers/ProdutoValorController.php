<?php

/**
 * Controller para gerenciar os produtos
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @since 2013-11-11
 * @package application
 * @subpackage controller
 */
class Api_ProdutoValorController extends Ead1_RestRestrita
{

    private $doctrineContainer;
    private $em;

    /**
     * @var \G2\Negocio\Produto
     */
    private $negocio;

    public function init()
    {
        parent::init();
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
        $this->negocio = new \G2\Negocio\Produto();
    }

    public function indexAction()
    {
        $dados = $this->getAllParams();
        unset($dados["module"], $dados["controller"], $dados["action"]);

        $array = array();
        $mensageiro = $this->negocio->findProdutoValorByArray($dados, true);

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {

            $entities = $mensageiro->getMensagem();
            foreach ($entities as $entity) {
                if ($entity instanceof \G2\Entity\ProdutoValor) {
                    $entity->setNu_valor(number_format($entity->getNu_valor(), 2, '.', ''));
                    $entity->setNu_valormensal(number_format($entity->getNu_valormensal(), 2, '.', ''));
                    $array[] = $entity->toBackboneArray($dateTimeFormat = 'd/m/Y', $zendDateFormat = 'dd/MM/yyyy');
                }
            }
        }

        $this->_helper->json($array);
    }

    public function getAction()
    {
        $mensageiro = $this->negocio->findProdutoValorById($this->_getParam('id', null));

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $entity = $mensageiro->getFirstMensagem();
            $entity->setNu_valor(number_format($entity->getNu_valor(), 2, '.', ''));
            $entity->setNu_valormensal(number_format($entity->getNu_valormensal(), 2, '.', ''));
            die(json_encode($mensageiro->getFirstMensagem()));
        }
    }

    public function postAction()
    {
        try {
            $body = $this->getRequest()->getRawBody();
            $data = Zend_Json::decode($body);

            $mensageiro = $this->negocio->salvarProdutoValor($data);
        } catch (Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $this->getResponse()->setHttpResponseCode(201);
            $this->_helper->json($mensageiro->getFirstMensagem()->toBackboneArray());
        } else {
            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json($mensageiro);
        }
    }

    public function putAction()
    {
        $this->postAction();
    }

    public function deleteAction()
    {
        try {
            $result = $this->negocio->find('\G2\Entity\ProdutoValor', $this->getParam('id'));
            $this->negocio->delete($result);
            $this->getResponse()->setHttpResponseCode(204);
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(500)->appendBody($e->getMessage());
        }
    }

}

<?php

/**
 * Classe com regras de negócio para o Loja
 * @author Elcio Mauo Guimarães - <elcioguimaraes@gmail.com>
 * @since 25/09/2012
 * @package models
 * @subpackage bo
 */
class LojaBO extends Ead1_BO
{

    const PAGA = 1;
    const GRATUITA = 2;

    /**
     * Pega os dados de sessao e retorna os produtos
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @return Ead1_Mensageiro
     */
    public function retornaProdutosSessao()
    {

        $sessao = Ead1_Sessao::getSessaoCarrinho();
        $pagamento = new PagamentoBO();
        return $pagamento->retornaProdutosPorID($sessao->getProdutos(), $sessao->getSt_codigocupom());
    }

    public function retornaProdutoPremio($campanhacomercial)
    {

        $negocio = new \G2\Negocio\Negocio();
        $campanha = $negocio->find('\G2\Entity\CampanhaComercial', $campanhacomercial);
        if ($campanha && $campanha->getId_premio()) {
            $premio = $negocio->find('\G2\Entity\Premio', $campanha->getId_premio());
            if ($premio && $premio->getId_tipopremio() == 1) {
                return $negocio->findBy('\G2\Entity\PremioProduto', array('id_premio' => $campanha->getId_premio()));
            }
        }

    }

    public function verificaCupom($campanhacomercial)
    {
        $negocio = new \G2\Negocio\Negocio();
        $campanha = $negocio->find('\G2\Entity\CampanhaComercial', $campanhacomercial);
        $premio = $negocio->find('\G2\Entity\Premio', $campanha->getId_premio());
        if ($premio->getId_tipopremio() == 2) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Pega os dados de sessao e retorna os produtos gratuitos
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @return Ead1_Mensageiro
     */
    public function retornaProdutosGratuitosSessao()
    {

        $sessao = Ead1_Sessao::getSessaoCarrinho();
        $pagamento = new PagamentoBO();
        return $pagamento->retornaProdutosPorID($sessao->getprodutos_gratuitos());
    }

    /**
     * Pega os dados de sessao e retorna os produtos pagos
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @return Ead1_Mensageiro
     */
    public function retornaProdutosPagosSessao()
    {

        $sessao = Ead1_Sessao::getSessaoCarrinho();
        $pagamento = new PagamentoBO();
        return $pagamento->retornaProdutosPorID($sessao->getprodutos_pagos(), $sessao->getSt_codigocupom());
    }

    /**
     * Verifica se o usuario existe pelo CPF
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param string $st_cpf
     * @return Ead1_Mensageiro
     */
    public function verificaCpfUsuario($st_cpf)
    {

        try {

            $vw = new UsuarioTO();
            $vw->setSt_cpf($st_cpf);
            $vw->fetch(false, true, true);

            if (!$vw->getId_usuario()) {
                return new Ead1_Mensageiro('Usuário não encontrado!', Ead1_IMensageiro::AVISO);
            } else {
                return new Ead1_Mensageiro($vw, Ead1_IMensageiro::SUCESSO);
            }
        } catch (Zend_Exception $e) {

            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Salva o cadatro, criando um usuário ou atualizando os dados da Pessoa
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param UsuarioTO $uTO
     * @param array $dados
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function salvarPessoa(UsuarioTO $uTO, array $dados)
    {
        $sessao = Ead1_Sessao::getSessaoCarrinho();
        $dao = new Ead1_DAO();
        $dao->beginTransaction();
        try {
            //verificas se o email veio vazio
            if (empty($dados['st_email'])) {
                throw new Zend_Exception('E-mail obrigatório!');
            }
            //instancia a bo de pessoa
            $peBO = new PessoaBO();
            //verifica se foi não passado o id_usuario
            if (!$uTO->getId_usuario()) {
                $nuTO = new UsuarioTO(); //instancia o usuario TO para buscar pelo cpf
                $nuTO->setSt_cpf($uTO->getSt_cpf());
                $nuTO->fetch(false, true, true);

                //depois de buscar pelo cpf verifica se encontrou usuario
                if ($nuTO->getId_usuario()) {
                    $uTO->setId_usuario($nuTO->getId_usuario());
                    $dadosacesso = new DadosAcessoTO();
                    $dadosacesso->setId_usuario($nuTO->getId_usuario());
                    $dadosacesso->setId_entidade($sessao->getId_entidade());
                    $dadosacesso->setSt_login($uTO->getSt_login());
                    $dadosacesso->setSt_senha($uTO->getSt_senha());
                    $dadosacesso->setBl_ativo(1);
                    $meu = $peBO->cadastrarDadosAcesso($dadosacesso);
                    if ($meu->getTipo() !== Ead1_IMensageiro::SUCESSO) {
                        throw new Zend_Exception($meu->getFirstMensagem());
                    }
                } else {//se não encontrou usuario salva um novo
                    $uTO->setId_usuariopai(1);
                    $meu = $peBO->cadastrarUsuario($uTO, $sessao->getId_entidade());
                    if ($meu->getTipo() !== Ead1_IMensageiro::SUCESSO) {
                        throw new Zend_Exception($meu->getFirstMensagem());
                    } else {
                        $uTO = $meu->getFirstMensagem();
                    }
                }
            }

            //Verificando país
            if (array_key_exists('id_pais', $dados) && !empty($dados['id_pais'])) {
                //verifica se o pais é o brasil e se não foi passado o cpf
                if ($dados['id_pais'] == PaisTO::BRASIL && !$uTO->getSt_cpf()) {
                    throw new Zend_Exception('Brasileiros precisam informar o CPF!');
                }
            }

            //instancia a pessoa TO
            $pTO = new PessoaTO();
            $pTO->setBl_ativo(true);
            $pTO->setId_entidade($sessao->getId_entidade());
            $pTO->setId_pais((isset($dados['id_pais']) ? $dados['id_pais'] : PaisTO::BRASIL));
            $pTO->setId_situacao(PessoaTO::SITUACAO_ATIVO);
            $pTO->setId_usuariocadastro($uTO->getId_usuario());
            $pTO->setId_usuario($uTO->getId_usuario());
//            $pTO->setSg_uf($dados['sg_uf']);
//            $pTO->setId_municipio($dados['id_municipio']);
            $pTO->setId_estadocivil(EstadoCivilTO::SOLTEIRO);
            $pTO->setSt_nomeexibicao(isset($dados['st_nome']) ? $dados['st_nome'] : $dados['st_nomecompleto']);

            $peRO = new PessoaRO();
            $me = $peRO->salvarPessoa($uTO, $pTO);
            if ($me->getTipo() != Ead1_IMensageiro::SUCESSO) {
                $dao->rollBack();
                throw new Zend_Exception($me->getFirstMensagem());
            }

            /**
             * Endereço
             */
            if (array_key_exists('sg_uf', $dados) && !empty($dados['sg_uf'])) {
                $eTO = new EnderecoTO();
                $eTO->setBl_ativo(true);
                $eTO->setId_tipoendereco(TipoEnderecoTO::RESIDENCIAL);
                $eTO->setNu_numero($dados['nu_numero']);
                $eTO->setSt_bairro($dados['st_bairro']);
                $eTO->setSt_cep($dados['st_cep']);
                $eTO->setId_municipio($dados['id_municipio']);
                $eTO->setSt_endereco($dados['st_endereco']);
                $eTO->setSt_complemento($dados['st_complemento']);

                $eTO->setSg_uf($dados['sg_uf']);
                $eTO->setId_pais((!empty($dados['id_pais']) ? $dados['id_pais'] : PaisTO::BRASIL));
                $eTO->setId_endereco((!empty($dados['id_endereco']) ? $dados['id_endereco'] : null));

                $peTO = new PessoaEnderecoTO();
                $peTO->setId_entidade($sessao->getId_entidade());
                $peTO->setId_usuario($uTO->getId_usuario());
                $peTO->setId_endereco($eTO->getId_endereco());
                $peTO->setBl_padrao(true);

                $meend = $peRO->salvarEndereco($uTO, $eTO, $peTO);
                if ($meend->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception('Endereço: ' . $meend->getFirstMensagem());
                }
                $usuario['id_enderecoentrega'] = $eTO->getId_endereco();
            }


            if ((array_key_exists('bl_enderecoentrega', $dados) && $dados['bl_enderecoentrega']) && $dados['bl_enderecoentrega']) {

                $eTO = new EnderecoTO();
                $eTO->setBl_ativo(true);
                $eTO->setId_tipoendereco(TipoEnderecoTO::RESIDENCIAL);
                $eTO->setNu_numero($dados['endereco_entrega']['nu_numero']);
                $eTO->setSt_bairro($dados['endereco_entrega']['st_bairro']);
                $eTO->setSt_cep($dados['endereco_entrega']['st_cep']);
                $eTO->setId_municipio($dados['endereco_entrega']['id_municipio']);
                $eTO->setSt_endereco($dados['endereco_entrega']['st_endereco']);
                $eTO->setSg_uf($dados['endereco_entrega']['sg_uf']);
                $eTO->setId_pais((!empty($dados['endereco_entrega']['id_pais']) ? $dados['id_pais'] : PaisTO::BRASIL));
                $eTO->setId_endereco((!empty($dados['endereco_entrega']['id_endereco']) ? $dados['endereco_entrega']['id_endereco'] : null));
                $eTO->setSt_complemento($dados['endereco_entrega']['st_complemento']);

                $peTO = new PessoaEnderecoTO();
                $peTO->setId_entidade($sessao->getId_entidade());
                $peTO->setId_usuario($uTO->getId_usuario());
                $peTO->setId_endereco($eTO->getId_endereco());
                $peTO->setBl_padrao(true);

                $meend = $peRO->salvarEndereco($uTO, $eTO, $peTO);
                if ($meend->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception('Endereço de Entrega: ' . $meend->getFirstMensagem());
                }
                $usuario['id_enderecoentrega'] = $eTO->getId_endereco();
            }

            /*
             * Salvando o Id do endereço de entrega */
            if (isset($usuario)) {
                SessaoLojaBO::setSessaoUsuarioLoja($usuario);
            }

            /**
             * Telefone Residencial
             */
            if (array_key_exists('nu_telefoneresidencial', $dados) && !empty($dados['nu_telefoneresidencial'])) {

                $ctTO = new ContatosTelefoneTO();
                $ctTO->setId_tipotelefone(TipoTelefoneTO::RESIDENCIAL);
                $ctTO->setNu_ddd($dados['nu_dddresidencial']);
                $ctTO->setNu_ddi(55);
                $ctTO->setNu_telefone($dados['nu_telefoneresidencial']);

                $ctpTO = new ContatosTelefonePessoaTO();
                $ctpTO->setBl_padrao(true);
                $ctpTO->setId_entidade($sessao->getId_entidade());
                $ctpTO->setId_usuario($uTO->getId_usuario());

                $metel = $peRO->salvarTelefone($uTO, $ctTO, $ctpTO);
                if ($metel->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception($metel->getFirstMensagem());
                }
            }

            /**
             * Telefone Celular
             */
            if (array_key_exists('nu_telefonecelular', $dados) && !empty($dados['nu_telefonecelular'])) {

                $ctTO = new ContatosTelefoneTO();
                $ctTO->setId_tipotelefone(TipoTelefoneTO::CELULAR);
                $ctTO->setNu_ddd($dados['nu_dddcelular']);
                $ctTO->setNu_telefone($dados['nu_telefonecelular']);
                $ctTO->setid_telefone((!empty($dados['id_telefonecelular']) ? $dados['id_telefonecelular'] : null));
                $ctTO->setNu_ddi(55);

                $ctpTO = new ContatosTelefonePessoaTO();
                $ctpTO->setBl_padrao(true);
                $ctpTO->setId_entidade($sessao->getId_entidade());
                $ctpTO->setId_telefone($ctTO->getId_telefone());
                $ctpTO->setId_usuario($uTO->getId_usuario());

                $metel = $peRO->salvarTelefone($uTO, $ctTO, $ctpTO);
                if ($metel->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception($metel->getFirstMensagem() . ' Erro: ' . $metel->getCodigo());
                }
            }

            /**
             * Salva Email
             */
            if (!empty($dados['st_email']) && !$sessao->getSt_email()) {

                $ceTO = new ContatosEmailTO();
                $ceTO->setSt_email($dados['st_email']);
                $ceTO->setid_email((!empty($dados['id_email']) ? $dados['id_email'] : null));

                $ceppTO = new ContatosEmailPessoaPerfilTO();
                $ceppTO->setBl_padrao(true);
                $ceppTO->setBl_ativo(true);
                $ceppTO->setId_email($ceTO->getId_email());
                $ceppTO->setId_entidade($sessao->getId_entidade());

                $meail = $peRO->salvarEmail($uTO, $ceTO, $ceppTO);
                if ($meail->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception($meail->getFirstMensagem());
                }
            }

            $uTO->fetch(true, true, true);
            $dao->commit();

            if (!$sessao->getId_usuario()) {
                $usuario['st_nomecompleto'] = $uTO->getSt_nomecompleto();
                $usuario['id_usuario'] = $uTO->getId_usuario();

                if (!empty($dados['st_email']) && !$sessao->getSt_email()) {
                    $usuario['st_email'] = $dados['st_email'];
                }
                if (!empty($dados['st_cpf']) && !$sessao->getSt_cpf()) {
                    $usuario['st_cpf'] = $dados['st_cpf'];
                }
                SessaoLojaBO::setSessaoUsuarioLoja($usuario);
            }

            return new Ead1_Mensageiro($uTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            SessaoLojaBO::limparSessaoUsuarioLoja();
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Método criado para separar o processo de venda, entre pagas e gratuítas
     * @param array $produtos
     * @param unknown_type $tipo
     * @return Ead1_Mensageiro
     */
    public function salvarVenda(VendaTO $vendaTO, $tipo = self::PAGA)
    {

        $sessao = Ead1_Sessao::getSessaoCarrinho();
        $dao = new Ead1_DAO();
        $dao->beginTransaction();
        try {
            $total = 0;
            if ($tipo == self::PAGA) {
                $produtos = $this->retornaProdutosPagosSessao()->getMensagem();
                foreach ($produtos as $chave => $produto) {
                    $total += $produto->getnu_valor();
                }
                $id_evolucao = VendaTO::EVOLUCAO_AGUARDANDO_NEGOCIACAO;
            } else {
                $id_evolucao = VendaTO::EVOLUCAO_AGUARDANDO_RECEBIMENTO;
                $produtos = $this->retornaProdutosGratuitosSessao()->getMensagem();
            }
            $formapagamentoBO = new \FormaPagamentoBO();
            $formaPagamentoTOC = new \FormaPagamentoTO();
            $formaPagamentoTOC->setId_entidade($sessao->getId_entidade());
            $formaPagamentoAplicacaoTO = new \FormaPagamentoAplicacaoTO();
            $formaPagamentoAplicacaoTO->setId_formapagamentoaplicacao(2);
            $mensageiro = $formapagamentoBO->retornarFormaPagamentoPorAplicacao($formaPagamentoTOC, $formaPagamentoAplicacaoTO);

            if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO)
                throw new Zend_Exception($mensageiro->getFirstMensagem());

            $formaPagamentoTO = $mensageiro->getFirstMensagem();

            $vendaTO->setId_entidade($sessao->getId_entidade());
            $vendaTO->setId_usuario($sessao->getId_usuario());
            $vendaTO->setId_usuariocadastro($sessao->getId_usuario());
            $vendaTO->setId_evolucao($id_evolucao);
            $vendaTO->setId_situacao(VendaTO::SITUACAO_VENDA_PENDENTE);
            $vendaTO->setBl_ativo(true);
            $vendaTO->setBl_contrato(true);
            $vendaTO->setId_formapagamento($formaPagamentoTO->getId_formapagamento());

            $vendaTO->setNu_parcelas(1);
            $vendaTO->setNu_valorbruto($total);
            $vendaTO->setNu_valorliquido($total);
//            $vendaTO->setId_enderecoentrega($sessao->getId_enderecoentrega());
            $vendaTO->setId_cupom($sessao->getId_cupom() ? $sessao->getId_cupom() : null);

            //seta na VendaTO os dados das tags UTM
            $this->setUtmTags($vendaTO, $sessao);

            $camp = new CampanhaComercialTO();
            $camp->setId_campanhacomercial((int)$sessao->getId_campanhacomercial());
            $camp->fetch(true, true, true);
            if ($camp->getId_entidade() == $vendaTO->getId_entidade()) {
                $vendaTO->setId_campanhacomercial($sessao->getId_campanhacomercial() ? $sessao->getId_campanhacomercial() : null);
            }
            $vendaTO->setId_campanhacomercialpremio($sessao->getId_campanhacomercialpremio() ? $sessao->getId_campanhacomercialpremio() : null);
            $vendaBO = new VendaBO();
            $mensageirovenda = $vendaBO->cadastrarVenda($vendaTO);
            if ($mensageirovenda->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Exception($mensageirovenda->getFirstMensagem());
            }

            $vendaTO->setId_venda($mensageirovenda->getFirstMensagem()->getid_venda());

            $viTO = new VendaIntegracaoTO();
            $viTO->setId_venda($vendaTO->getId_venda());
            $viTO->setId_sistema($sessao->getId_sistema());
            $viTO->setId_usuariocadastro($sessao->getId_usuario());
            $viTO->setSt_urlconfirmacao($sessao->getSt_urlconfirmacao());
            $viTO->setSt_urlvendaexterna($sessao->getSt_urlvendaexterna());
            $viTO->setSt_vendaexterna($sessao->getSt_vendaexterna());

            $mein = $vendaBO->salvarVendaIntegracaoTO($viTO);
            if ($mein->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Exception("Erro ao cadastrar a integração da Venda!");
            }

            /**
             * $produtoAtual = VwProdutoTO
             */
            $produtoProjeto = false;
            foreach ($produtos as $produto) {

                $resultCombo = $vendaBO->pesquisarCombo($produto->id_produto);

                $produto->setnu_quantidade(($produto->getnu_quantidade() ? $produto->getnu_quantidade() : 1));
                $produto->setNu_valor($produto->getnu_valor() / $produto->getnu_quantidade());

                for ($x = 0; $x < $produto->getnu_quantidade(); $x++) {
                    if ($produto->getId_tipoproduto() == TipoProdutoTO::PROJETO_PEDAGOGICO && !$produtoProjeto) {
                        $produtoProjeto = true;
                        $vWS = new VendaWS();
                        $contratoRegraTO = $vWS->buscarRegraContratoProduto($produto->getId_Produto(), $sessao->getId_entidade(), false)->getFirstMensagem();

                        if (!($contratoRegraTO instanceof ContratoRegraTO)) {
                            throw new Zend_Exception('Erro ao recuperar a Regra do Contrato!');
                        }
                    }

                    if ($resultCombo) {

                        $vlrTotal = $produto->getNu_valor();
                        $totalLiq = 0;
                        $totalBruto = 0;

                        foreach ($resultCombo as $rC) {
                            $totalLiq += $rC["nu_valorprodutoitem"];
                            $totalBruto += $rC["nu_valor"];
                        }

                        foreach ($resultCombo as $rC) {
                            $vendaProdutoTO = new VendaProdutoTO();
                            $vendaProdutoTO->setId_produto($rC['id_produto']);
                            $vendaProdutoTO->setId_venda($vendaTO->getId_venda());
                            $vendaProdutoTO->setNu_desconto($rC['nu_descontoporcentagem']);
                            $vendaProdutoTO->setId_matricula($sessao->getId_matricula());
                            $valorLiq = ($rC["nu_valorprodutoitem"] * $vlrTotal) / $totalLiq;

                            $vendaProdutoTO->setNu_valorliquido($valorLiq);
                            $vendaProdutoTO->setNu_valorbruto($rC["nu_valor"]);
                            $vendaProdutoTO->setId_produtocombo($produto->getId_produto());

                            //Procura em tb_produtoprojetopedagogico se tem registro para recuperar o vinculo com a turma
                            $produtoNegocio = new G2\Negocio\Produto();
                            $resultProdProjePed = $produtoNegocio->findProdutoProjetoPedagogicoByArray(array(
                                'id_produto' => $produto->getId_produto(),
                                'id_entidade' => $this->sessao->id_entidade
                            ));
                            if ($resultProdProjePed->getType() == \Ead1_IMensageiro::TYPE_SUCESSO) {
                                $mensagem = $resultProdProjePed->getFirstMensagem();
                                $vendaProdutoTO->setId_turma($mensagem['id_turma']);
                            }

                            $proTemp = new ProdutoTO();
                            $proTemp->setId_produto($rC['id_produto']);
                            $proTemp->fetch(true, true, true);
                            if ($proTemp->getId_tipoproduto() == TipoProdutoTO::PROJETO_PEDAGOGICO && !$produtoProjeto) {
                                $produtoProjeto = true;
                                $vWS = new VendaWS();
                                $contratoRegraTO = $vWS->buscarRegraContratoProduto($proTemp->getId_Produto(), $sessao->getId_entidade())->getFirstMensagem();

                                if (!($contratoRegraTO instanceof ContratoRegraTO)) {
                                    throw new Zend_Exception('Erro ao recuperar a Regra do Contrato!');
                                }
                            }

                            $arVendaProduto[] = $vendaProdutoTO;
                        }
                    } else {

                        $verificavalor = new VwProdutoTO();
                        $verificavalor->setId_produto($produto->getId_Produto());
                        $verificavalor->fetch(true, true, true);

                        $verificavalor->setNu_valor($verificavalor->getNu_valor() ? $verificavalor->getNu_valor() : 1);

                        $nu_desconto = 100 - (($produto->getNu_valor() * 100) / $verificavalor->getNu_valor());
                        if ($nu_desconto == false) {
                            $nu_desconto = 0;
                        }

                        $vendaProdutoTO = new VendaProdutoTO();
                        $vendaProdutoTO->setId_produto($produto->getId_Produto());
                        $vendaProdutoTO->setId_venda($vendaTO->getId_venda());
                        $vendaProdutoTO->setNu_desconto($nu_desconto);
                        $vendaProdutoTO->setId_matricula($sessao->getId_matricula());
                        $vendaProdutoTO->setNu_valorliquido($produto->getNu_valor());
                        $vendaProdutoTO->setNu_valorbruto($verificavalor->getNu_valor());
                        $vendaProdutoTO->setId_campanhacomercial($sessao->getId_campanhacomercial());

                        //Procura em tb_produtoprojetopedagogico se tem registro para recuperar o vinculo com a turma
                        $produtoNegocio = new G2\Negocio\Produto();
                        $resultProdProjePed = $produtoNegocio->findProdutoProjetoPedagogicoByArray(array(
                            'id_produto' => $produto->getId_produto(),
                            'id_entidade' => $this->sessao->id_entidade
                        ));

                        if ($resultProdProjePed->getType() == \Ead1_IMensageiro::TYPE_SUCESSO) {
                            $mensagem = $resultProdProjePed->getFirstMensagem();
                            $vendaProdutoTO->setId_turma($mensagem['id_turma']);
                        };

                        $arVendaProduto[] = $vendaProdutoTO;
                    }
                }
            }

            $vendaBO = new VendaBO();
            $menArrVP = $vendaBO->salvarArrVendaProduto($arVendaProduto);
            if ($menArrVP->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Exception($menArrVP->getCodigo());
            }

            if ($produtoProjeto) {

                $contratoTO = new ContratoTO();
                $contratoTO->setId_entidade($sessao->getId_entidade());
                $contratoTO->setId_usuario($sessao->getId_usuario());
                $contratoTO->setId_usuariocadastro($sessao->getId_usuario());
                $contratoTO->setId_venda($vendaTO->getId_venda());
                $contratoTO->setId_contratoregra($contratoRegraTO->getId_contratoregra());
                $contratoTO->setId_evolucao(ContratoTO::EVOLUCAO_AGUARDANDO_CONFIRMACAO);
                $contratoTO->setId_situacao(ContratoTO::SITUACAO_PENDENTE);
                $contratoTO->setId_textosistema(null);
                $contratoTO->setBl_ativo(true);

                $contratoResponsavelTO = new ContratoResponsavelTO();
                $contratoResponsavelTO->setId_usuario($sessao->getId_usuario());

                $contratoResponsavelTO->setNu_porcentagem(100);
                $contratoResponsavelTO->setId_tipocontratoresponsavel(TipoContratoResponsavelTO::TIPO_CONTRATO_RESPONSAVEL_PEDAGOGICO); // Pedagógico
                $contratoResponsavelTO->setBl_ativo(true);

                $contratoResponsavelFinanceiroTO = clone $contratoResponsavelTO;
                $contratoResponsavelFinanceiroTO->setId_tipocontratoresponsavel(TipoContratoResponsavelTO::TIPO_CONTRATO_RESPONSAVEL_FINANCEIRO); // Financeiro

                $arContratoResponsavelFinanceiro[] = $contratoResponsavelFinanceiroTO;

                $ro = new ContratoRO();
                $contrato = $ro->salvarContratoMatricula($contratoTO, $arContratoResponsavelFinanceiro, $contratoResponsavelTO, null, null, null, $contratoRegraTO, null);
                if ($contrato->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception('Erro ao Salvar o Contrato!');
                }
            }
            $vendaTO->fetch(true, true, true);
            $dao->commit();
            return new Ead1_Mensageiro("Dados salvos com sucesso!", Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $dao->rollBack();
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Salva o Básico de uma Venda para Prosseguimento do carrinho até o Pagamento
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param UsuarioTO $uTO
     * @return Ead1_Mensageiro
     */
    public function salvarVendaBasica(UsuarioTO $uTO)
    {

        $sessao = Ead1_Sessao::getSessaoCarrinho();

        $dao = new Ead1_DAO();
        $dao->beginTransaction();
        try {

            if (!$sessao->getProdutos()) {
                throw new Zend_Exception('Nenhum Produto informado!');
            }

            $produtos_pagos = $sessao->getprodutos_pagos();
            $produtos_gratuiros = $sessao->getprodutos_gratuitos();

            $vendaTO = new VendaTO();


            if ($produtos_gratuiros) {
                $mensageiro = $this->salvarVenda($vendaTO, self::GRATUITA);
                if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    $dao->rollBack();
                    return new Ead1_Mensageiro($mensageiro->getFirstMensagem(), Ead1_IMensageiro::EmailUtilizado);

                }
            }

            if ($produtos_pagos) {
                $vendaTO = new VendaTO();
                $mensageiro = $this->salvarVenda($vendaTO, self::PAGA);
                if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    $dao->rollBack();
                    return new Ead1_Mensageiro($mensageiro->getFirstMensagem(), Ead1_IMensageiro::EmailUtilizado);
                }
            }

            Ead1_Sessao::setSessionCarrinho(array('id_venda' => $vendaTO->getId_venda()));

            $dao->commit();

            /**
             * a parte do afiliado, foi feita por alguem que perdeu os códigos.
             */
//             //Se vier setado o numero do afiliado, iremos atualizar a venda com o id do afiliado.
//             if ($sessao->getAfiliado_referrer() != null || $sessao->getAfiliado_referrer() != '') {
//                 $vb = new VendaBO();
//                 $vendaTO->setId_contratoafiliado($sessao->getAfiliado_referrer());
//                 $vb->retornarAfiliadoAtualizado($vendaTO);
//             }

            return new Ead1_Mensageiro($vendaTO, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $dao->rollBack();
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Verifica o e-mail na entidade
     * @param string $st_email
     */
    public function verificaEmailEntidade($st_email)
    {

        $sessao = Ead1_Sessao::getSessaoCarrinho();

        $vw = new VwPessoaEmailTO();
        $vw->setSt_email($st_email);
        $vw->setId_entidade($sessao->getId_entidade());
        $vw->setBl_padrao(1);
        $vw->setBl_ativo(1);

        $vw->fetch(false, true, true);

        if ($vw->getId_usuario()) {
            $mensageiro = new Ead1_Mensageiro("Encontramos seu E-mail em nossa base de dados, o que você deseja fazer?", Ead1_IMensageiro::SUCESSO);
        } else {
            $mensageiro = new Ead1_Mensageiro("E-mail não encontrado", Ead1_IMensageiro::AVISO);
        }

        return $mensageiro;
    }

    /**
     * Verifica o cpf na entidade
     * @param string $st_cpf
     */
    public function verificaCpfEntidade($st_cpf)
    {

        $sessao = Ead1_Sessao::getSessaoCarrinho();

        $vw = new VwPessoaTO();
        $vw->setSt_cpf($st_cpf);
        $vw->setId_entidade($sessao->getId_entidade());
        $vw->fetch(false, true, true);
        if ($vw->getId_usuario()) {
            $mensageiro = new Ead1_Mensageiro("Encontramos seu CPF em nossa base de dados, o que você deseja fazer?", Ead1_IMensageiro::SUCESSO, $vw);
        } else {
            $mensageiro = new Ead1_Mensageiro("CPF não encontrado", Ead1_IMensageiro::AVISO);
        }

        return $mensageiro;
    }

    /**
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param UsuarioTO $usTO
     * @return Ead1_Mensageiro
     */
    public function loginLoja($st_login = null, $st_senha = null, $id_usuario = null)
    {

        try {

            $sessao = Ead1_Sessao::getSessaoCarrinho();

            if ($id_usuario) {
                $vwPessoaTO = new VwPessoaTO();
                $vwPessoaTO->setId_usuario($id_usuario);
                $vwPessoaTO->setId_entidade($sessao->getId_entidade());
                $pessoaBO = new PessoaBO();
                $arrpessoa = $pessoaBO->retornarVwPessoa($vwPessoaTO);

                if ($arrpessoa) {
                    $pessoa = Ead1_TO_Dinamico::encapsularTo($arrpessoa, new VwPessoaTO());
                    $retorno = new Ead1_Mensageiro($pessoa[0], Ead1_IMensageiro::SUCESSO);
                } else {
                    $retorno = new Ead1_Mensageiro("Usuário não encontrado", Ead1_IMensageiro::ERRO);
                }
            } else {
                $VwPessoa = new \G2\Negocio\Pessoa();
                $retorno = $VwPessoa->retornaVwPessoaOne(array('id_entidade' => $sessao->getId_entidade(), 'st_email' => $st_login, 'st_senhaentidade' => $st_senha));
            }

            if ($retorno->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $vw = $retorno->getFirstMensagem();

                $usuario['st_nomecompleto'] = $vw->getSt_nomecompleto();
                $usuario['id_usuario'] = $vw->getId_usuario();
                $usuario['st_email'] = $vw->getSt_email();
                $usuario['st_cpf'] = $vw->getSt_cpf();
                Ead1_Sessao::setSessionCarrinho($usuario);
                Ead1_Sessao::setSessionGeral(array('id_usuario' => $vw->getId_usuario()));

                if ($vw->getSt_nomecompleto()) {


                    if ($vw->getSt_nomecompleto())
                        $cadastro['st_nomecompleto'] = $vw->getSt_nomecompleto();

                    if ($vw->getSt_cpf())
                        $cadastro['st_cpf'] = $vw->getSt_cpf();

                    if ($vw->getId_endereco())
                        $cadastro['id_endereco'] = $vw->getId_endereco();

                    if ($vw->getSt_endereco())
                        $cadastro['st_endereco'] = $vw->getSt_endereco();

                    if ($vw->getSt_bairro())
                        $cadastro['st_bairro'] = $vw->getSt_bairro();

                    if ($vw->getNu_numero())
                        $cadastro['nu_numero'] = $vw->getNu_numero();

                    if ($vw->getSt_cep())
                        $cadastro['st_cep'] = $vw->getSt_cep();

                    if ($vw->getSt_bairro())
                        $cadastro['st_bairro'] = $vw->getSt_bairro();

                    if ($vw->getId_municipio())
                        $cadastro['id_municipio'] = $vw->getId_municipio();

                    if ($vw->getsg_uf())
                        $cadastro['sg_uf'] = $vw->getsg_uf();


                    if ($vw->getSt_email())
                        $cadastro['st_email'] = $vw->getSt_email();


                    if ($vw->getid_telefone())
                        $cadastro['id_telefonecelular'] = $vw->getid_telefone();

                    if ($vw->getNu_telefone())
                        $cadastro['nu_telefonecelular'] = $vw->getNu_telefone();

                    if ($vw->getNu_ddd())
                        $cadastro['nu_dddcelular'] = $vw->getNu_ddd();


                    SessaoLojaBO::setSessaoComprasLoja(null, $cadastro);
                }

                $mensageiro = new Ead1_Mensageiro($cadastro, Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new Ead1_Mensageiro('Usuário ou Senha inválidos!', Ead1_IMensageiro::ERRO);
            }
        } catch (Exception $e) {
            $mensageiro = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    /**
     * Método que solicita a recuperação de senha do Portal do Aluno
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param string $cpf
     * @param string $email
     * @return Ead1_Mensageiro
     */
    public function _recuperarSenha($st_cpf, $st_email, $id_entidade)
    {

        try {

            $vwPessoaTO = new VwPessoaTO();
            $vwPessoaTO->setId_entidade($id_entidade ? $id_entidade : null);
            $vwPessoaTO->setSt_cpf($st_cpf ? $st_cpf : null);
            $vwPessoaTO->setSt_email($st_email ? $st_email : null);

            $loginBO = new LoginBO();
            $mensageiro = $loginBO->recuperarSenhaPessoa($vwPessoaTO);

            if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
                return new Ead1_Mensageiro("Fique de olho na sua Caixa de Entrada, o e-mail com seus dados de acesso deve chegar em breve.", Ead1_IMensageiro::SUCESSO);
            } else {
                throw new Zend_Exception($mensageiro->getFirstMensagem());
            }
        } catch (Zend_Validate_Exception $e) {
            return new Ead1_Mensageiro('Usuário não encontrado no Portal do Aluno', Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro('Erro ao Recuperar os Dados de Acesso', Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Seta as tags utm da venda
     * @param VendaTO $vendaTO
     * @param Ead1_Sessao_Carrinho $sessaoCarrinho
     */
    private function setUtmTags(VendaTO &$vendaTO, Ead1_Sessao_Carrinho $sessaoCarrinho)
    {
        if ($sessaoCarrinho) {
            $vendaTO->setUtm_source($sessaoCarrinho->getUtm_source());
            $vendaTO->setUtm_medium($sessaoCarrinho->getUtm_medium());
            $vendaTO->setUtm_campaign($sessaoCarrinho->getUtm_campaign());
            $vendaTO->setUtm_term($sessaoCarrinho->getUtm_term());
            $vendaTO->setUtm_content($sessaoCarrinho->getUtm_content());
            $vendaTO->setOrigem_organica($sessaoCarrinho->getOrigem_organica());
        }
    }

}

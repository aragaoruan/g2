<?php


/**
 * Classe que controla o Fluxo de sessão do carrinho
 * @author Elcio Mauo Guimarães - <elcioguimaraes@gmail.com>
 * @since 25/09/2012
 * @package models
 * @subpackage bo
 */
class SessaoLojaBO  {

	
	/**
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 */
	public static function limparSessaoLoja(){
		
		$userProfileNamespace = new Zend_Session_Namespace('carrinho');
		$userProfileNamespace->unLock();
		Zend_Session::namespaceUnset('carrinho');
		
	}
	
	
	/**
	 * Seta a Sessão do Usuário
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @param array $usuario
	 */
	public static function setSessaoUsuarioLoja(array $usuario){
		Ead1_Sessao::setSessionCarrinho($usuario);
		Ead1_Sessao::setSessionUsuario($usuario);
		Ead1_Sessao::setSessionGeral($usuario);
	}
	
	/**
	 * Limpa a Sessão do Usuário
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 */
	public static function limparSessaoUsuarioLoja(){
		
		$array = array('id_usuario'=>null,'st_nomecompleto'=>null, 'id_usuariopai'=>null);
		
		Ead1_Sessao::setSessionCarrinho($array);
		Ead1_Sessao::setSessionUsuario($array);
		Ead1_Sessao::setSessionGeral($array);
		
	}

	
	/**
	 * Seta a Sessão da Entidade
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @param array $entidade
	 */
	public static function setSessaoEntidadeLoja(array $entidade){
		Ead1_Sessao::setSessionCarrinho($entidade);
		Ead1_Sessao::setSessionEntidade($entidade);
		Ead1_Sessao::setSessionGeral($entidade);
	}
	
	/**
	 * Limpa a Sessão referente a Entidade
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 */
	public static function limparSessaoEntidadeLoja(){
		
		$array = array('id_entidade'=>null,'st_nomeentidade'=>null,'st_urlimglogo'=>null,'id_sistema'=>null);
		
		Ead1_Sessao::setSessionCarrinho($array);
		Ead1_Sessao::setSessionEntidade($array);
		Ead1_Sessao::setSessionGeral($array);
	}
	
	
	/**
	 * Seta os produtos e Cadastro
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @param array $produtos
	 * @param array $cadastro
	 */
	public static function setSessaoComprasLoja(array $produtos = null, array $cadastro = null){
		if($produtos) {
			$array['produtos'] = $produtos;
		}
		if($cadastro){
			$array['cadastro'] = $cadastro;
		}
		Ead1_Sessao::setSessionCarrinho($array);
	}
	
	
	/**
	 * LImpa a Sessão de Compras da Loja
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 */
	public static function limparSessaoComprasLoja(){
		Ead1_Sessao::setSessionCarrinho(array('produtos'=>null,'cadastro'=>null));
	}

}
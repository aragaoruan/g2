<?php
/**
 * Classe de controller de Robos do Sistema para a Loja
 * @author Helder Silva <helder.silva@unyleya.com.br>
 *
 * @package application
 * @subpackage controller
 */
class Loja_RoboController extends Ead1_Controller
{
    /**
     * Método que inicializa as configurações gerais do ambiente
     */
    public function init()
    {
        parent::init();
        $this->bo = new RoboBO();
        $this->setupDoctrine();

        /*
         * Verifica extensao e habilita o background-job newRelic.
         */
        if (extension_loaded('newrelic')) {
            newrelic_background_job(true);
        }
    }

    public function zendJobAtivacaoAutomaticaAction(){
        $recebimento = new \G2\Negocio\Recebimento();
        $params = $recebimento->getCurrentJobParams();
        $resultado = $recebimento->processoAtivacaoAutomatica($params['id_venda']);
        Zend_Debug::dump($resultado->getFirstMensagem());
        die('end');
    }
}
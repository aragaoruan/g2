<?php
/**
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 *
 */
class LojaBase extends Ead1_Controller
{

    protected $baseUrl;
    protected $baseUrlModulo;

    /**
     * Método que inicializa as configurações gerais do ambiente
     */
    public function init()
    {
        $config = Ead1_Ambiente::geral();
        $this->view->setScriptPath(APPLICATION_PATH . '/apps/loja/views/scripts');
        $this->view->flexFolder = $config->flexFolder;
        $this->view->baseUrl = Ead1_Ambiente::geral()->st_url_loja;

        if ($this->_getParam('module') == 'loja') {
            $this->baseUrl = $this->view->baseUrl;
            $this->baseUrlModulo = $this->baseUrl . "";
            $this->view->module = $this->_getParam('module');
            $this->view->baseUrlModulo = $this->baseUrlModulo;
        }

        $id_venda = $this->getParam('venda');

        # este parametro virá do e-mail enviado para renegociacao com o aluno
        $dataSaleCrypt = $this->getParam('key');

        # este bloco é necessário, pois quando o aluno receber o e-mail e clicar
        # no link enviado para efetuar a renegociacao, os dados voltarão cripto-
        # grafados. assim, é necessário fazer este processo a seguir para extrair
        # o identificador da venda
        if ($dataSaleCrypt) {
            $securityKey = $this->config()->security->key;
            $dataSalePlainText = \util\Cipher::decrypt($dataSaleCrypt, $securityKey);

            # o formato esperado de $dataSalePlaintText é:
            # {"id_venda":0000,"id_acordo":0000,"id_lancamento":0000}
            $dataSalePlainText = json_decode($dataSalePlainText);
            $id_venda = $dataSalePlainText->id_venda;
        }

        if ($id_venda) {
            $vendaTO = new VendaTO();
            $vendaTO->setId_venda($id_venda);
            $vendaTO->fetch(true, true, true);
            if (!$vendaTO->getDt_cadastro()) {
                throw new Zend_Exception('Esta venda não foi encontrada em nosso banco de dados!<br><br>');
            }

            $entidadeIntegracao = new EntidadeIntegracaoTO();
            $entidadeIntegracao->setId_entidade($vendaTO->getId_entidade());
            $entidadeIntegracao->setId_sistema(\G2\Constante\Sistema::PAGARME);
            $entidadeIntegracao->fetch(false, true, true);

            # necessário para recupera o st_codsistema que contém a
            # chave criptográfica do pagarme.
            $this->view->entidadeintegracao = $entidadeIntegracao;
        }

        $this->setLayoutPath();
        if (!$this->_getParam('isAjax', false)) {

            $layout = \Zend_Layout::getMvcInstance();
            // Set a layout script path:
            $layout->setLayoutPath(APPLICATION_PATH . '/apps/loja/views/layouts');
            // choose a different layout script:
            $layout->setLayout('layout');
            //$this->_helper->layout()->setLayout('layout');
        } else {
            $this->_helper->layout->disableLayout();
        }

    }

    public function _Json($data)
    {
        Zend_Wildfire_Channel_HttpHeaders::getInstance()->flush();
        return $this->_helper->json($data);
    }

}

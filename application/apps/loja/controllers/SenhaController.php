<?php

/**
 * Controller de Senha (Loja) 
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 *
 */
class Loja_SenhaController extends LojaBase {
	
	
	/* (non-PHPdoc)
	 * @see UnicoController::init()
	 */
	public function init(){
		parent::init();
	}


    /**
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     */
    public function indexAction(){
		exit;
    }
    
    
    /**
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     */
    public function recuperarAction(){
    	
    	
    	if($this->_getParam('st_cpf') || $this->_getParam('st_email')){
    		$sessao = Ead1_Sessao::getSessaoCarrinho();
	    	$l = new LojaBO();
	    	$mensageiro = $l->_recuperarSenha($this->_getParam('st_cpf'), $this->_getParam('st_email'), $sessao->getId_entidade());
    	} else {
    		$mensageiro = new Ead1_Mensageiro('O CPF e o E-mail são obrigatórios para recuperar a senha!', Ead1_IMensageiro::ERRO);
    	}
	    
    	$this->_helper->json($mensageiro);
    	    	
    	
    }
    
    

}


<?php

use \util\Cipher;
use \G2\Constante\Sistema;
use \G2\Negocio\Negociacao;
use \G2\Constante\MeioPagamento;
use \G2\Constante\CartaoBandeira;


/**
 * @author Elcio Mauro Guimarães
 * Classe para possibilitar o recebimento de uma venda que esteja aguardando recebimento
 *
 */
class Loja_RecebimentoController extends LojaBase
{
    private $venda;
    private $id_venda;
    private $id_lancamento;

    /**
     * @return mixed
     */
    public function getid_venda()
    {
        return $this->id_venda;
    }

    /**
     * @return mixed
     */
    public function getid_lancamento()
    {
        return $this->id_lancamento;
    }

    /**
     * @return \G2\Entity\Venda
     */
    public function getvenda()
    {
        return $this->venda;
    }

    public function indexAction()
    {
        try {

            $this->id_venda = (int)$this->_getParam('venda', false);
            $this->id_lancamento = (int)$this->_getParam('lancamento', false);

            if ($this->getid_venda() == false) {
                throw new Zend_Exception("É obrigatório informar a Venda!");
            }
            $pagBO = new PagamentoBO();
            $pagBO->verificaPossibilidadePagamento($this->getid_venda(), false, $this->id_lancamento);
            if ($this->id_lancamento) {
                $pagBO->verificaPossibilidadePagamentoLancamento($this->id_lancamento);
            }
            $venda = new \G2\Entity\Venda();
            $venda->find($this->getid_venda());

            $usuario = new \G2\Entity\VwPessoa();
            $usuario->findOneBy(array(
                'id_usuario' => $venda->getId_usuario()->getId_usuario()
            ,
                'id_entidade' => $venda->getId_entidade()
            ));
            $this->view->id_entidade = $venda->getId_entidade();
            if ($venda->getId_venda()) {
                $this->venda = $venda;
                $this->view->venda = $venda;
                $this->view->email = $usuario->getSt_email();
                $this->view->st_nomecompleto = $usuario->getst_nomecompleto();
                $this->view->st_cpf = $usuario->getst_cpf();

                $this->view->produtos = array();
                $entidadeIntegracao = new EntidadeIntegracaoTO();
                $entidadeIntegracao->setId_entidade($venda->getId_entidade());
                $entidadeIntegracao->setId_sistema(\G2\Constante\Sistema::PAGARME);
                $entidadeIntegracao->fetch(false, true, true);
                $this->view->entidadeintegracao = $entidadeIntegracao;
                $pv = new \G2\Entity\VwProdutoVenda();
                $arpro = $pv->findBy(array('id_venda' => $venda->getId_venda(), 'bl_ativo' => true));
                if ($arpro) {
                    $this->view->produtos = $arpro;
                }
                $eTO = new \G2\Negocio\Entidade();
                $ent = $eTO->findOneBy('\G2\Entity\Entidade', array('id_entidade' => $venda->getId_entidade()));
                if (!$ent->getId_entidade()) {
                    throw new Exception('Entidade não encontrada.');
                }
                $entidade['id_sistema'] = 9;
                $entidade['id_entidade'] = $ent->getId_entidade();
                $entidade['st_nomeentidade'] = $ent->getSt_nomeentidade();
                $entidade['st_urlimglogo'] = $ent->getSt_urlimglogo();
                SessaoLojaBO::setSessaoEntidadeLoja($entidade);
                if ($this->getid_lancamento()) {
                    $negocio = new \G2\Negocio\Negocio();
                    $lancamento = $negocio->findOneBy('\G2\Entity\VwVendaLancamento',
                        array('id_lancamento' => $this->getid_lancamento(), 'id_venda' => $this->getid_venda())
                    );
                    if ($lancamento instanceof \G2\Entity\VwVendaLancamento && $lancamento->getIdLancamento()) {
                        $this->view->lancamento = $lancamento;

                        $this->view->valor_lancamento = str_replace('.', '', $lancamento->getNuValor());
                        $this->view->nu_parcelas = 1;
                        /*
                            1	Cartão Crédito
                            2	Boleto
                            7	Cartão Débito
                            11	Cartão Recorrente
                         */
                        switch ($lancamento->getIdMeiopagamento()) {
                            case  2:
                                $vlancamento = new \G2\Entity\VwVendaLancamento();
                                $arr = $vlancamento->findBy(array(
                                    'id_venda' => $lancamento->getIdVenda(),
                                    'id_meiopagamento' => $lancamento->getIdMeiopagamento()
                                ), array('nu_ordem' => 'ASC'));
                                if ($arr) {
                                    $this->view->nu_parcelas = count($arr);
                                }
                                $boletoPHP = new \G2\Negocio\BoletoPHP();
                                die($boletoPHP->emitirBoleto($lancamento->getIdLancamento()));
                                break;
                            case  1:
                            case 11:
                                if (!$lancamento->getBlEntrada() && $lancamento->getIdMeiopagamento() == 1) {
                                    throw new \Exception("Para pagar uma parcela no Cartão de Crédito direto, é necessário que a mesma seja uma entrada");
                                }

                                $this->view->nu_valor_pagarme = number_format((($venda->getNu_valorliquido()) * 100), 0,
                                    '', '');

                                $vlancamento = new \G2\Entity\VwVendaLancamento();
                                $arr = $vlancamento->findBy(array(
                                    'nu_cartao' => $lancamento->getnu_cartao(),
                                    'id_venda' => $lancamento->getIdVenda(),
                                    'id_meiopagamento' => $lancamento->getIdMeiopagamento()
                                ), array('nu_ordem' => 'ASC'));
                                if ($arr) {
                                    $this->view->nu_parcelas = count($arr);
                                }
                                $fiBO = new FinanceiroBO();
                                if ($lancamento->getIdMeiopagamento() == 1) {
                                    $mbandeira = $fiBO->retornarCartaoBandeira(new CartaoBandeiraTO(),
                                        SistemaTO::BRASPAG);
                                } else {
                                    $mbandeira = $fiBO->retornarCartaoBandeira(new CartaoBandeiraTO(),
                                        SistemaTO::BRASPAG_RECORRENTE);
                                }
                                if ($mbandeira->getTipo() == Ead1_IMensageiro::SUCESSO) {
                                    $this->view->bandeira = $mbandeira->getMensagem();
                                } else {
                                    throw new Zend_Exception("Bandeiras não encontradas!");
                                }

                                $cartoesaceitos = [];
                                foreach ($mbandeira->getMensagem() as $ca) {
                                    $cartoesaceitos[] = $ca->getSt_cartaobandeira();
                                }
                                $this->view->cartoesaceitos = strtolower(implode(", ", $cartoesaceitos));

                                $plano['id'] = null;
                                if ($lancamento->getIdMeiopagamento() == \G2\Constante\MeioPagamento::CARTAO_RECORRENTE) {
                                    $negocioPagarme = new \G2\Negocio\Pagarme();
                                    $plano = $negocioPagarme->createPlanPayment(
                                        str_replace('.', '', $lancamento->getNuValor()),
                                        $venda->getId_entidade(),
                                        30,
                                        $this->id_venda,
                                        count($arr)
                                    );
                                }
                                $this->view->id_plano_pagamento_pagarme = $plano['id'];

                                $this->renderScript('/recebimento/cartao-credito.phtml');
                                break;
                            default:
                                throw new \Exception("Este Meio de Pagamento não pode ser pago através desta rotina.");
                        }
                    } else {
                        throw new Zend_Exception("Parcela não encontrada!");
                    }
                } else {
                    $lancamento = new \G2\Entity\VwVendaLancamento();
                    $arr = $lancamento->findBy(array('id_venda' => $this->getid_venda(), 'bl_entrada' => true),
                        array('nu_ordem' => 'ASC'));
                    if ($arr) {
                        $this->view->lancamentos = $arr;
                        $this->renderScript('/recebimento/venda.phtml');
                    } else {
                        throw new \Exception("Nenhuma parcela encontrada");
                    }
                }
            } else {
                throw new Zend_Exception("Venda não encontrada!");
            }
        } catch (\Exception $e) {
            $this->view->mensagem = $e->getMessage();
            $this->render('index-indisponivel');
        }
    }

    public function autorizarCartaoAction()
    {
        $this->ajax();
        $bo = new Ead1_BO();
        try {
            $id_venda = $this->_getParam('id_venda', null);
            if (!$id_venda) {
                throw new \Exception('É obrigatório informar a Venda!', Ead1_IMensageiro::ERRO);
            }
            $venda = new VendaTO();
            $venda->setId_venda($id_venda);
            $venda->fetch(true, true, true);
            $post = $this->limpaGetAllParams($this->_getAllParams());
            $parcelaValor = explode('_', $post['nu_parcelas']);
            $parcelaValor[1] = $bo->desFormataValor($parcelaValor[1]);
            $lancamento = new LancamentoTO();
            if (isset($post['id_lancamento']) && $post['id_lancamento']) {
                $lancamento->setId_lancamento($post['id_lancamento']);
                $lancamento->fetch(true, true, true);
                $lv = new LancamentoVendaTO();
                $lv->setId_lancamento($lancamento->getId_lancamento());
                $lv->setId_venda($venda->getId_venda());
                $lv->fetch(false, true, true);
                $post['bl_entrada'] = $lv->getBl_entrada();
            }
            $post['id_venda'] = $id_venda;
            $id_cartaobandeira = $this->_getParam('id_cartaobandeira', null);
            if (!$id_cartaobandeira) {
                throw new \Exception('É obrigatório informar a Bandeira!', Ead1_IMensageiro::ERRO);
            }
            $pagamento = new PagamentoCartaoBO($venda->getId_entidade(),
                (!empty($id_cartaobandeira) ? $id_cartaobandeira : 1), $this->_getParam('id_meiopagamento', 1));
            //Recupera as configurações de cartão da entidade e verifica se é instancia vw_entidadecartao
            if ($pagamento->entidadecartao instanceof VwEntidadeCartaoTO) {
                $post['id_cartaooperadora'] = $pagamento->entidadecartao->getId_cartaooperadora(); //cria uma posição no post para cartaooperadora
                $post['id_cartaobandeira'] = $pagamento->entidadecartao->getId_cartaobandeira();
            } else {
                throw new \Exception('Não foi possivel recuperar os dados de Pagamento!', Ead1_IMensageiro::ERRO);
            }
            $id_modelovenda = $pagamento->retornaModeloVenda($id_venda);
            if ($id_modelovenda == \G2\Entity\ModeloVenda::ASSINATURA) {
                $post['nu_parcelas'] = 1;
            } else {
                $post['nu_parcelas'] = $parcelaValor[0];
            }
            $post['id_modelovenda'] = ($id_modelovenda ? $id_modelovenda : 1);
            $post['nu_valortotal'] = $parcelaValor[1] * $parcelaValor[0];
            $post['nu_valorparcela'] = $parcelaValor[1];
            unset($post['id_lancamento']);
            $mensageiro = $pagamento->autorizarCartao($post,
                $lancamento->getnu_cartao() ? $lancamento->getnu_cartao() : 1);
        } catch (Exception $e) {
            $mensageiro = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        $this->_Json($mensageiro);
    }

    public function confirmarCartaoAction()
    {
        try {
            $parametros = $this->limpaGetAllParams($this->getAllParams());
            $this->id_venda = $this->_getParam('id_venda', false);
            $this->id_lancamento = $this->_getParam('id_lancamento', false);
            if ($this->getid_venda() == false) {
                throw new Zend_Exception("É obrigatório informar a Venda!");
            }
            $venda = new \G2\Entity\Venda();
            $venda->find($this->getid_venda());
            $this->view->id_entidade = $venda->getId_entidade();
            $this->view->venda = $venda;

            $entidadeIntegracao = new EntidadeIntegracaoTO();
            $entidadeIntegracao->setId_entidade($venda->getId_entidade());
            $entidadeIntegracao->setId_sistema(\G2\Constante\Sistema::PAGARME);
            $entidadeIntegracao->fetch(false, true, true);

            if (!$entidadeIntegracao) {
                throw new Zend_Exception("Não existe entidade integracao configurada!");
            }

            $this->view->entidadeintegracao = $entidadeIntegracao;

            $transData = array(
                'amount' => $parametros['amount'],
                'installments' => $parametros['nu_parcelas'],
                'card_hash' => $parametros['pagarme']['card_hash'],
                'payment_method' => 'credit_card',
                'async' => 'false',
                'metadata' => array(
                    'id_venda' => $parametros['id_venda'],
                    'id_meiopagamento' => $parametros['id_meiopagamento']
                )
            );
            $informacoes = array(
                'id_venda' => $parametros['id_venda'],
                'id_meiopagamento' => $parametros['id_meiopagamento'],
                'nu_cartao' => $parametros['nu_cartaoUtilizado'],
                'id_lancamento' => $this->id_lancamento,
                'id_acordo' => $parametros['id_acordo']
            );

            $transaction = new \G2\Negocio\Pagarme();
            $mensageiro = $transaction->transactionCreditCard($informacoes, $transData);
            if ($mensageiro->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                if ($venda->getId_evolucao()->getId_evolucao()) {
                    $lancamento = new \G2\Entity\VwVendaLancamento();
                    $lancamento->findOneBy(array(
                        'id_lancamento' => $this->getid_lancamento(),
                        'id_venda' => $this->id_venda
                    ));
                    if ($lancamento->getDtprevquitado() || $lancamento->getBlentrada()) {
                        $this->view->mensagemConfirmacao = 'Pagamento efetuado com sucesso!';
                    } else {
                        $this->view->mensagemConfirmacao = 'Houve algum problema no pagamento, por favor tente novamente!';
                    }
                } else {
                    $this->view->mensagemConfirmacao = 'Não encontramos este pedido em nosso sistema!!';
                }
            } else {
                $this->view->mensagemConfirmacao = 'O seu pagamento não foi autorizado, entre com contato com a operadora do cartão!';
            }
        } catch (Exception $exc) {
            die("Erro no pagamento do cartão." . $exc->getMessage());
        }
    }

    public function negociacaoAction()
    {
        $this->negocio = new \G2\Negocio\Negocio();
        $securityKey = $this->config()->security->key;

        $data = json_decode(
            Cipher::decrypt(
                $this->getParam('key')
                , $securityKey
            )
        );

        try {
            $this->view->st_chavecriptografiapagarme = $this->buscaChavePagarMe($data->id_venda);
        } catch (Exception $e) {
            $this->view->mensagem = $e->getMessage();
            $this->render('index-indisponivel');
            return;
        }

        $this->id_venda = $data->id_venda;
        $this->id_lancamento = $data->id_lancamento;

        $vendaLancamento = null;

        if ($data->id_venda && $data->id_lancamento) {
            $params = array('id_venda' => $data->id_venda, 'id_lancamento' => $data->id_lancamento);
            $vendaLancamento = $this->negocio->findOneBy('G2\Entity\VwResumoFinanceiro', $params);

            $dt_atual = new DateTime();
            $bl_vencido = strtotime($vendaLancamento->getDt_vencimento()->format('Y-m-d')) < strtotime($dt_atual->format('Y-m-d'));

            $transacaoFinanceira = $this->negocio->getRepository('\G2\Entity\TransacaoFinanceira')
                ->retornaTransacaoFinanceiraLancamento(array('id_lancamento' => $data->id_lancamento));


            # caso o lançamento já esteja com o bl_quitado barra o render da tela de pagamento
            if (
                ($vendaLancamento instanceof \G2\Entity\VwResumoFinanceiro && $vendaLancamento->getBl_quitado() )
                || !empty($transacaoFinanceira)
            ) {
                $this->_response->clearBody();
                $this->_response->clearHeaders();
                $this->_response->setHttpResponseCode(404);
                $this->view->mensagem = 'Pagamento já realizado.';

                $this->render('index-indisponivel');
                return;
            }

            # caso o lançamento já esteja vencido barra o render da tela de pagamento
            if ($vendaLancamento instanceof \G2\Entity\VwResumoFinanceiro && $bl_vencido) {
                $this->_response->clearBody();
                $this->_response->clearHeaders();
                $this->_response->setHttpResponseCode(404);
                $this->view->mensagem = 'O pagamento não pode ser realizado porque a data de vencimento já expirou. Por favor entrar em contato com a instituição financeira.';

                $this->render('index-indisponivel');
                return;
            }
        }


        if (!is_object($data)) {
            # @todo verificar para onde enviar o usuário, caso os dados
            # sejam nvalidos
            $this->_response->clearBody();
            $this->_response->clearHeaders();
            $this->_response->setHttpResponseCode(404);

            $this->render('index-indisponivel');
            return;
        }

        try {
            #
            # após este ponto trata-se do caminho feliz da aplicação:
            #
            # os dados id_venda, id_acordo e id_lancamento são tão confiáveis
            # como se estivesse sido retornados do banco de dados. assim, não
            # precisa aplicar nenhuma verificação que não fosse requerida dos
            # dados advindos do banco de dados. # # # # # # # # # # # # # # #
            $negociacao = new Negociacao();
            $this->view->venda = $negociacao->cartao($data);
            $this->view->venda->bandeiras = $this->organizaBandeirasCartao($this->view->venda->bandeiras);
            $this->renderScript('/recebimento/cartao-credito-negociacao.phtml');

        } catch (\Exception $e) {
            dump($e->getMessage());
        }
    }

    private function buscaChavePagarMe($venda)
    {
        $negocio = new \G2\Negocio\Negocio();
        $entidade = $negocio->findOneBy('\G2\Entity\Venda', array('id_venda' => $venda));
        if ($entidade) {
            $retorno = $negocio->findOneBy('\G2\Entity\EntidadeIntegracao',
                array('id_entidade' => $entidade->getId_entidade(), 'id_sistema' => 30));
        } else {
            throw new Exception("Não foi possível recuperar a venda.");
        }


        if ($retorno) {
            return $retorno->getSt_codsistema();
        } else {
            throw new Exception("Não foi possível recuperar a chave para comunicação com a Pagar.me.");
        }
    }

    private function organizaBandeirasCartao($bandeiras)
    {
        if ($bandeiras) {
            foreach ($bandeiras as $index) {
                $bandeiras_organizado[] = strtolower($index->stCartaoBandeira);
            }
            return implode(', ', $bandeiras_organizado);
        } else {
            throw new Exception("Não foi possível recuperar as bandeiras dos cartões, tente novamente mais tarde.");
        }
    }
}

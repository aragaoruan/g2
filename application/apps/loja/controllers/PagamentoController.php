<?php

use \G2\Constante\Sistema;
use \G2\Negocio\Negociacao;
use \G2\Constante\MeioPagamento;
use \G2\Entity\UsuarioIntegracao;
use \G2\Constante\CartaoBandeira;

/**
 * @author Arthur Cláudio de Almeida Pereira
 *
 */
class Loja_PagamentoController extends LojaBase
{
    public function indexAction()
    {
        try {


            $id_venda = $this->_getParam('venda', null);
            if (!$id_venda) {
                $id_venda = $this->_getParam('id_venda', null);
                if (!$id_venda) {
                    die('Lançamento não encontrado.');
                }
            }

            $sessao = Ead1_Sessao::getSessaoCarrinho();
            $this->view->sessaoCarrinho = $sessao;
            $this->view->produtos_gratuitos = $sessao->getProdutos_gratuitos();

            if ($sessao->getId_campanhacomercialpremio()) {
                $lojaBo = new LojaBO();
                $cupomPremio = $lojaBo->verificaCupom($sessao->getId_campanhacomercialpremio());
                if ($cupomPremio) {
                    $this->view->cupomPremio = true;
                }
            }

            $vendaTO = new VendaTO();
            $vendaTO->setId_venda($id_venda);
            $vendaTO->fetch(true, true, true);
            if (!$vendaTO->getDt_cadastro()) {
                throw new Zend_Exception('Esta venda não foi encontrada em nosso banco de dados!<br><br>');
            }

            /**
             * Verifica se a venda eh de um produto gratuíto
             */
            if ($vendaTO->getId_evolucao() == VendaTO::EVOLUCAO_AGUARDANDO_RECEBIMENTO && $vendaTO->getNu_valorliquido() == 0) {
                $viTO = new VendaIntegracaoTO();
                $this->_redirect("/pagamento/confirmar-cartao/id_venda/" . $vendaTO->getId_venda());
            }

            /**
             * Retorna os países
             */
            $form = new CadastroRapidoPessoaForm();
            $this->view->form = $form;


            $pagamentoBO = new PagamentoBO();
            $pagamento = $pagamentoBO->retornaVendaPagamento($vendaTO);

            $this->view->id_entidade = $vendaTO->getId_entidade();
            $vwpessoa = new VwPessoaTO();
            $vwpessoa->setId_entidade($vendaTO->getId_entidade());
            $vwpessoa->setId_usuario($vendaTO->getId_usuario());
            $vwpessoa->fetch(false, true, true);

            $this->view->pessoa = $vwpessoa;

            $entidadeIntegracao = new EntidadeIntegracaoTO();
            $entidadeIntegracao->setId_entidade($vendaTO->getId_entidade());
            $entidadeIntegracao->setId_sistema(\G2\Constante\Sistema::PAGARME);
            $entidadeIntegracao->fetch(false, true, true);

            $this->view->entidadeintegracao = $entidadeIntegracao;

            if ($pagamento->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Exception($pagamento->getFirstMensagem());
            } else {

                $dadosPagamento = $pagamento->getFirstMensagem();
                $this->view->bl_livro = false;
                foreach ($dadosPagamento->carrinho["produtos"] as $produto) {
                    if ($produto->getid_tipoproduto() == 6) {
                        $pagamento = new \G2\Negocio\Produto();
                        $id_tipolivro = $pagamento->findProdutoLivroByProduto($produto->getId_produto());
                        $tipoLivro = $id_tipolivro->getFirstMensagem()->getId_livro()->getId_tipolivro()->getId_tipolivro();
                        if ($tipoLivro == 1) {
                            $this->view->bl_livro = true;
                        }
                    }
                }

                if ($this->view->bl_livro && $vendaTO->getId_enderecoentrega()) {

                    $endereco = new VwPessoaEnderecoTO();
                    $endereco->setId_endereco($vendaTO->getId_enderecoentrega());
                    $endereco->setId_usuario($vendaTO->getId_usuario());
                    $endereco->fetch(false, true, true);

                    $this->view->enderecoentrega = $endereco;
                }


                //Verifica se existe o objeto venda e se ele é uma instancia de VwVendaUsuarioTO VendaTO::EVOLUCAO_AGUARDANDO_RECEBIMENTO
                if (isset($dadosPagamento->venda) && $dadosPagamento->venda instanceof VwVendaUsuarioTO) {
                    $venda = $dadosPagamento->venda;
                    //Verifica se a evolução da venda é 9 e se for redireciona para a tela de confirmação
                    if ($venda->getId_evolucao() == VendaTO::EVOLUCAO_AGUARDANDO_RECEBIMENTO && isset($dadosPagamento->carrinho['formapagamento']->boleto)) {
                        $id_venda = $venda->getId_venda();
                        $id_meiopagamento = MeioPagamentoTO::BOLETO;
                        $id_lancamento = $dadosPagamento->carrinho['formapagamento']->boleto[0]->id_lancamento;

                        if ($id_lancamento) {
                            $this->_redirect("/pagamento/gerar-boleto/id_venda/{$id_venda}/id_lancamento/{$id_lancamento}");
                            exit;
                        }

                    }
                }

                $this->view->pagamento = $dadosPagamento;
                $cartoesaceitos = [];
                foreach ($dadosPagamento->carrinho['formapagamento']->bandeira as $ca) {
                    $cartoesaceitos[] = $ca->st_cartaobandeira;
                }
                $this->view->cartoesaceitos = strtolower(implode(", ", $cartoesaceitos));

                $mp = new \G2\Negocio\MensagemPadrao();
                $mpadrao = $mp->getSistemaEntidadeMensagem(array('id_mensagempadrao' => 20));

                if (isset($mpadrao[0]) && $mpadrao[0] instanceof \G2\Entity\SistemaEntidadeMensagem) {

                    $textoSistema = new TextoSistemaTO();
                    $textoSistema->setId_textosistema($mpadrao[0]->getId_textosistema()->getId_textosistema());
                    $textoSistema->fetch(true, true, true);

                    if ($textoSistema->getSt_texto()) {

                        $txtBO = new TextoSistemaBO();
                        $textoSistema = $txtBO->gerarTextoLoja($textoSistema, array('id_venda' => $vendaTO->getId_venda()))->getFirstMensagem();
                        $this->view->textopagamento = $textoSistema instanceof TextoSistemaTO ? $textoSistema->getSt_texto() : '';
                    }
                } //if($mpadrao[0] instanceof \G2\Entity\SistemaEntidadeMensagem){

                $mpadrao = $mp->getSistemaEntidadeMensagem(array('id_mensagempadrao' => 21));

                if (isset($mpadrao[0]) && $mpadrao[0] instanceof \G2\Entity\SistemaEntidadeMensagem) {

                    $textoSistema = new TextoSistemaTO();
                    $textoSistema->setId_textosistema($mpadrao[0]->getId_textosistema()->getId_textosistema());
                    $textoSistema->fetch(true, true, true);

                    if ($textoSistema->getSt_texto()) {

                        $txtBO = new TextoSistemaBO();
                        $textoSistema = $txtBO->gerarTextoLoja($textoSistema, array('id_venda' => $vendaTO->getId_venda()))->getFirstMensagem();
                        $this->view->textoproduto = $textoSistema instanceof TextoSistemaTO ? $textoSistema->getSt_texto() : '';
                    }
                } //if($mpadrao[0] instanceof \G2\Entity\SistemaEntidadeMensagem){
            }
        } catch (Exception $e) {
            SessaoLojaBO::limparSessaoLoja();
            $this->view->mensagem = $e->getMessage();
            $this->render('index_indisponivel');
        }
    }

    public function mudarCartaoRecorrenteAction()
    {
        try {

            $id_venda = $this->_getParam('venda', null);
            if (!$id_venda) {
                $id_venda = $this->_getParam('id_venda', null);
                if (!$id_venda) {
                    die('Lançamento não encontrado.');
                }
            }

            $financeiroNegocio = new \G2\Negocio\Financeiro();
            $dados = $financeiroNegocio->retornaDadosVendaRecorrente($id_venda);
            $this->view->id_entidade = $dados['venda']->getId_entidade();
            $this->view->venda = $dados['venda'];
            $this->view->pessoa = $dados['pessoa'];
            $this->view->vendaLancamentos = $dados['vendaLancamentos'];
            $this->view->entidadeintegracao = $dados['entidadeintegracao'];

            if ($dados['pagamento']) {

                $this->view->pagamento = $dados['pagamento'];

                $mp = new \G2\Negocio\MensagemPadrao();
                $mpadrao = $mp->getSistemaEntidadeMensagem(array('id_mensagempadrao' => 20));

                if (isset($mpadrao[0]) && $mpadrao[0] instanceof \G2\Entity\SistemaEntidadeMensagem) {

                    $textoSistema = new TextoSistemaTO();
                    $textoSistema->setId_textosistema($mpadrao[0]->getId_textosistema()->getId_textosistema());
                    $textoSistema->fetch(true, true, true);

                    if ($textoSistema->getSt_texto()) {

                        $txtBO = new TextoSistemaBO();
                        $textoSistema = $txtBO->gerarTextoLoja($textoSistema, array('id_venda' => $dados['venda']->getIdVenda()))->getFirstMensagem();
                        $this->view->textopagamento = $textoSistema instanceof TextoSistemaTO ? $textoSistema->getSt_texto() : '';
                    }
                }

                $mpadrao = $mp->getSistemaEntidadeMensagem(array('id_mensagempadrao' => 21));

                if (isset($mpadrao[0]) && $mpadrao[0] instanceof \G2\Entity\SistemaEntidadeMensagem) {

                    $textoSistema = new TextoSistemaTO();
                    $textoSistema->setId_textosistema($mpadrao[0]->getId_textosistema()->getId_textosistema());
                    $textoSistema->fetch(true, true, true);

                    if ($textoSistema->getSt_texto()) {

                        $txtBO = new TextoSistemaBO();
                        $textoSistema = $txtBO->gerarTextoLoja($textoSistema, array('id_venda' => $dados['venda']->getIdVenda()))->getFirstMensagem();
                        $this->view->textoproduto = $textoSistema instanceof TextoSistemaTO ? $textoSistema->getSt_texto() : '';
                    }
                }
            }
        } catch (Exception $e) {
            SessaoLojaBO::limparSessaoLoja();
            $this->view->mensagem = $e->getMessage();
            $this->render('index_indisponivel');
        }
    }

    /**
     * Método que verifica se o usuario ja tem cadastro e solicita a senha, caso nao tenha, envia o mesmo para tela de cadastro do sistema.
     *
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     */
    public function iniciarAction()
    {

        try {

            SessaoLojaBO::limparSessaoLoja();
            SessaoLojaBO::limparSessaoEntidadeLoja();
            SessaoLojaBO::limparSessaoUsuarioLoja();
            $compraexterna = $this->limpaGetAllParams($this->_getAllParams());
            Ead1_Sessao::setSessionCarrinho($compraexterna);
            $sessao = Ead1_Sessao::getSessaoCarrinho();
            $eTO = new \G2\Negocio\Entidade();
            if (!$sessao->getProdutos()) {
                throw new Exception('O Carrinho está vazio!');
            } elseif (!$sessao->getst_codchave()) {
                throw new Exception('O st_codchave de autenticação não foi informado.');
            } else {

                $ent = $eTO->findOneBy('\G2\Entity\Entidade', array('st_wschave' => $sessao->getst_codchave()));
                if (!$ent || empty($ent->getId_entidade())) {
                    throw new Exception('Entidade não encontrada.');
                }
            }

            $entidade['id_sistema'] = Sistema::WORDPRESS_ECOMMERCE;
            $entidade['id_entidade'] = $ent->getId_entidade();
            $entidade['st_nomeentidade'] = $ent->getSt_nomeentidade();
            $entidade['st_urlimglogo'] = $ent->getSt_urlimglogo();
            SessaoLojaBO::setSessaoEntidadeLoja($entidade);
            $sessao = Ead1_Sessao::getSessaoCarrinho();


            /**
             * Separando o que é pago, do que é gratuíto
             */

            $gratuitos = array();
            $lojaBO = new LojaBO();
            $negocio = new G2\Negocio\Negocio();
            $arrPr = array();
            if ($sessao->getId_campanhacomercialpremio()) {
                $produtoPremio = $lojaBO->retornaProdutoPremio($sessao->getId_campanhacomercialpremio());
                if ($produtoPremio) {
                    foreach ($produtoPremio as $value) {
                        array_push($arrPr, $negocio->toArrayEntity($value));
                    }
                }
            }


            $produtos = $lojaBO->retornaProdutosSessao()->getMensagem();
            foreach ($produtos as $chave => $produto) {
                foreach ($arrPr as $value) {
                    if ($produto->getId_produto() == $value['id_produto']['id_produto']) {
                        $gratuitos[] = $produto;
                        unset($produtos[$chave]);
                        continue;
                    }
                }
                if ($produto->getnu_valor() <= 0) {
                    $gratuitos[] = $produto;
                    unset($produtos[$chave]);
                    continue;
                }
            }

            if ($gratuitos) {
                Ead1_Sessao::setSessionCarrinho(array('produtos_gratuitos' => $gratuitos));
                if ($produtos) {
                    Ead1_Sessao::setSessionCarrinho(array('produtos_pagos' => $produtos));
                }
            } else {
                Ead1_Sessao::setSessionCarrinho(array('produtos_pagos' => $produtos));
            }

            ////////////////////////////////////// LOGANDO O USUÁRIO QUANDO O ID_USUARIO É PASSADO ///////////////////////////////////////////////
            if (isset($compraexterna['id_usuario']) && $compraexterna['id_usuario'] == true) {
                $loja = new LojaBO();
                $mensageiro = $loja->loginLoja(null, null, $compraexterna['id_usuario']);

                //se achou o usuario e logou
                if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
                    //instancia a TO
                    $uTO = new UsuarioTO();
                    $uTO->setId_usuario($compraexterna['id_usuario']); //seta o id_usuario
                    //salva a venda
                    $meloja = $loja->salvarVendaBasica($uTO);
                    //pega o retorno da venda
                    if ($meloja->getTipo() == Ead1_IMensageiro::SUCESSO) {
                        //se foi uscesso pega os dados
                        $venda = $meloja->getFirstMensagem();
                        //redireciona para a pagina de venda com o id_venda
                        $this->_redirect('/loja/pagamento/?venda=' . $venda->getId_venda());
                    } elseif ($meloja->getTipo() == Ead1_IMensageiro::EmailUtilizado) {
                        $this->_redirect('/loja/pagamento/emailutilizado?msg=' . $meloja->getFirstMensagem());
                    }
                    $this->_redirect('/loja/pagamento/cadastro'); //redireciona para a tela de cadastro
                }
            }
            ////////////////////////////////////// FIM LOGANDO O USUÁRIO QUANDO O ID_USUARIO É PASSADO ///////////////////////////////////////////////

            $this->_redirect('/loja/pagamento/acesso');
        } catch (Exception $e) {

            $sessao = Ead1_Sessao::getSessaoEntidade();
            $this->view->id_entidade = $sessao->id_entidade;

            SessaoLojaBO::limparSessaoLoja();
            SessaoLojaBO::limparSessaoEntidadeLoja();
            SessaoLojaBO::limparSessaoUsuarioLoja();
            $this->view->mensagem = $e->getMessage();
            $this->render('index_indisponivel');
        }
    }

    public function emailutilizadoAction()
    {
        $this->view->msg = $this->limpaGetAllParams($this->getAllParams())['msg'];
    }

    public function getPaisAction()
    {
        $form = new CadastroRapidoPessoaForm();
        $form->populateIdPais(22);
        $this->view->form = $form;
    }

    public function acessoAction()
    {
        $sessao = Ead1_Sessao::getSessaoCarrinho();
        try {
            if (!$sessao->getId_entidade()) {
                throw new Exception('Nenhuma rotina em sessão para ser paga.');
            }

            $dados = $sessao->getCadastro();
            $this->view->dados = $dados;

            $ceTO = new ConfiguracaoEntidadeTO();
            $ceTO->setId_entidade($sessao->getId_entidade());
            $ceTO->fetch(false, true, true);

            $this->view->bl_emailunico = $ceTO->getBl_emailunico();
            $this->view->id_entidade = $sessao->getId_entidade();
            $this->view->sessao = $sessao;
        } catch (Exception $e) {
            SessaoLojaBO::limparSessaoLoja();
            $this->view->mensagem = $e->getMessage();
            $this->render('index_indisponivel');
        }
    }

    /**
     * Verifica o e-mail na entidade
     */
    public function verificaEmailEntidadeAction()
    {

        $loja = new LojaBO();
        $mensageiro = $loja->verificaEmailEntidade($this->_getParam('st_email', false));
        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $mensageiro = new Ead1_Mensageiro("Encontramos seu E-mail em nossa base de dados.<br> Por favor, clique

                    <a href='" . $this->baseUrlModulo . "/pagamento/acesso' target='_parent'>aqui</a> para fazer seu Login <br>ou recuperar sua senha.", Ead1_IMensageiro::SUCESSO);
        } else {
            $mensageiro = new Ead1_Mensageiro("E-mail disponível.", Ead1_IMensageiro::AVISO);
        }

        echo $this->_Json($mensageiro);
    }

    /**
     * Verifica o cpf na entidade
     */
    public function verificaCpfEntidadeAction()
    {

        $loja = new LojaBO();
        $mensageiro = $loja->verificaCpfEntidade(str_replace(".", "", str_replace("-", "", $this->_getParam('st_cpf', false))));

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $vwPessoaTO = $mensageiro->getCodigo(); //Pega o retono com os dados
            $mensagem = $this->view->mensagem('cpf', $vwPessoaTO->getSt_email());
            $mensageiro = new Ead1_Mensageiro($mensagem, Ead1_IMensageiro::SUCESSO);
        } else {
            $mensageiro = new Ead1_Mensageiro("CPF disponível.", Ead1_IMensageiro::AVISO);
        }
        echo $this->_Json($mensageiro);
    }

    /**
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @atualização Neemias Santos - neemias.santos@gmail.com
     */
    public function cadastroAction()
    {
        try {
            $sessao = Ead1_Sessao::getSessaoCarrinho();
            $sessaoRedesSociais = new Zend_Session_Namespace('redesSociais');
            $form = new CadastroLojaForm();

            if (!$sessao->getId_entidade()) {
                throw new Exception('Ocorreu algum problema, não encontramos sua compra.');
            }
            $dados = $sessao->getCadastro();
            $arrDadosForm = array();

            //se houver sessão para popular o formulário, os dados são carregados
            if ($dados) {
                foreach ($form->getElements() as $key => $el) {
                    if (array_key_exists($key, $dados) && !empty($dados[$key])) {
                        $arrDadosForm[$key] = $dados[$key];
                    }
                }
            }

            if ($this->getParam('st_email')) {
                $arrDadosForm['st_email'] = $this->getParam('st_email');
            }

            if (isset($dados) && array_key_exists('st_nomecompleto', $dados) && !empty($dados['st_nomecompleto'])) {
                $arrDadosForm = $this->montaArrayForm($dados['st_nomecompleto']);
            } elseif (isset($sessaoRedesSociais) && !empty($sessaoRedesSociais->st_nomecompleto)) {
                $arrDadosForm = $this->montaArrayForm($sessaoRedesSociais->st_nomecompleto, $sessaoRedesSociais->st_email);
            }

            $form->populate($arrDadosForm);


            $this->view->dados = $dados;
            $this->view->id_entidade = $sessao->getId_entidade();
            $this->view->form = $form;
            $this->view->sessao = $sessao;

        } catch (Exception $e) {
            SessaoLojaBO::limparSessaoLoja();
            $this->view->mensagem = $e->getMessage();
            $this->render('index_indisponivel');
        }
    }

    public function montaArrayForm($st_nomecompleto, $st_email = null)
    {
        $nome = '';
        $sobrenome = '';
        $nomeSeparado = explode(' ', $st_nomecompleto);
        foreach ($nomeSeparado as $key => $nome2) {
            if ($key == 0) {
                $nome = $nome2;
            } else {
                $sobrenome = $sobrenome . " " . $nome2;
            }
        }
        $arrDadosForm['st_nome'] = $nome;
        $arrDadosForm['st_sobrenome'] = $sobrenome;

        if (isset($st_email) && !empty($st_email)) {
            $arrDadosForm['st_email'] = $st_email;
        }

        return $arrDadosForm;
    }

    /**
     * Salva os dados retornados do Google
     *
     * @author Neemias Santos - neemias.santos@unyleya.com.br
     */
    public function googleAction()
    {
        try {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();

            if ($this->getRequest()->getPost()) {
                $post = $this->getRequest()->getPost();

                $usuarioIntegracao = new \G2\Negocio\UsuarioIntegracao();

                $idGoogle = $usuarioIntegracao->findUsuarioIntegracao(array('st_codusuario' => $post['id_google']));

                /*
                * Faz uma verificação para pegar o id_google e verificar se já foi cadastrado aquele
                * id, depois isso fazer uma verificação se já esta cadastrado na tb_usuario integração
                */

                if (!$idGoogle) {
                    /*
                     * Método cria uma sessão para os dados do Google para serem salvos na tb_usuariointegracao
                     * após salvar o user
                     */
                    $sessaoRedesSociais = new Zend_Session_Namespace('redesSociais');
                    $sessaoRedesSociais->id_sistema = SistemaTO::GOOGLE;
                    $sessaoRedesSociais->st_codusuario = $post['id_google'];
                    $sessaoRedesSociais->st_email = $post['st_email'];
                    $sessaoRedesSociais->st_nomecompleto = $post['st_nomecompleto'];
                    $this->_helper->json('cadastro');
                } else {
                    $tb_usuariointegracao = new UsuarioIntegracaoTO();
                    $tb_usuariointegracao->setId_sistema(SistemaTO::GOOGLE);
                    $tb_usuariointegracao->setSt_codusuario($post['id_google']);
                    $tb_usuariointegracao->fetch(false, true, true);

                    $pessoa = new  \G2\Negocio\Pessoa();
                    $resultPessoa = $pessoa->retornaDadosPessoa(array('id_usuario' => $tb_usuariointegracao->getId_usuariocadastro()));
                    $loja = new LojaBO();
                    $resultLogin = $loja->loginLoja($resultPessoa[0]['st_email'], $resultPessoa[0]['st_senhaentidade'], $resultPessoa[0]['id_usuario']);

                    Ead1_Sessao::setSessionGeral(array('id_usuario' => $tb_usuariointegracao->getId_usuariocadastro()));

                    //pega o primeiro resultado do mensageiro
                    $arrPessoa = $resultLogin->getFirstMensagem();
                    //instancia a TO
                    $uTO = new UsuarioTO();
                    $uTO->montaToDinamico($arrPessoa); //monta os dados na TO
                    //Salva a venda
                    $meloja = $loja->salvarVendaBasica($uTO);

                    //pega o retorno da venda
                    if ($meloja->getTipo() == Ead1_IMensageiro::SUCESSO) {
                        //se foi sucesso pega os dados
                        $venda = $meloja->getFirstMensagem();
                        //redireciona para a pagina de venda com o id_venda
                        $urlReturn = '?venda=' . $meloja->mensagem[0]->id_venda;
                        $this->_helper->json($urlReturn);
                    } else {
                        $this->_redirect('/loja/pagamento/emailutilizado?msg=' . $meloja->getFirstMensagem());
                    }
                }
            }
        } catch (Exception $e) {
            $this->_helper->json($e->getMessage());
        }
    }

    /**
     * Salva o cadastro e cria a venda
     *
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     */
    public function salvarvendaAction()
    {
        try {


            //pega os parametros
            $post = $this->limpaGetAllParams($this->getAllParams());
            unset($post['baseUrl']);//remove essa chave


            //instancia o formulario
            $form = new CadastroLojaForm();
            $form->getElement('id_municipio')->setRegisterInArrayValidator(false);//id_municipio seta como false o valor que chegar, para não verificar se esta no array

            $dao = new Ead1_DAO();//instancia a DAO
            $dao->beginTransaction();//abre tensação

            if (!$form->isValid($post)) {//verifica se o formulario NÃO é valido

                //percorre as mensagens e cria uma string com elas
                $str = null;
                foreach ($form->getMessages() as $erros) {
                    if ($erros) {
                        foreach ($erros as $mens) {
                            $str = $mens;
                            $str .= "<br>";
                        }
                    }
                }
                //faz um trow para matar a transação e retornar o erro para a tela
                throw new Zend_Validate_Exception($str);
            } else {
                $post = $form->getValues();//pega os valores do form e seta na variavel $post

                //valida a senha e confirmar senha
                if ($post['st_senha'] != $post['st_confirmarsenha']) {
                    //faz um trow para matar a transação e retornar o erro para a tela
                    throw new Zend_Validate_Exception("O campo Confirmar Senha deve ser igual ao campo Senha.");
                }


                //monta o nome completo
                $post['st_nomecompleto'] = $post['st_nome'] . ' ' . $post['st_sobrenome'];

                if (isset($post["bl_enderecoentrega"])) {
                    if ($post["bl_enderecoentrega"]) {
                        foreach ($post as $chave => $valor) {
                            if (substr($chave, 0, 8) == 'entrega_') {
                                $post['endereco_entrega'][str_replace('entrega_', '', $chave)] = $valor;
                                unset($post[$chave]);
                            }
                        }
                    }
                } else {
                    $post["bl_enderecoentrega"] = false;
                }

                //remove os pontos e hífen do cpf
                if (array_key_exists('st_cpf', $post) && !empty($post['st_cpf'])) {
                    $post['st_cpf'] = str_replace('-', '', str_replace('.', '', $post['st_cpf']));
                }

                //Verifica se veio os dados do endereço
                $post['st_endereco'] = isset($post['st_endereco']) ? $post['st_endereco'] : '';
                $post['nu_numero'] = isset($post['nu_numero']) ? $post['nu_numero'] : '';
                $post['st_bairro'] = isset($post['st_bairro']) ? $post['st_bairro'] : '';
                $post['st_cep'] = isset($post['st_cep']) ? $post['st_cep'] : '';
                $post['id_municipio'] = isset($post['id_municipio']) ? $post['id_municipio'] : '';
                $post['st_complemento'] = isset($post['st_complemento']) ? $post['st_complemento'] : '';
                $post['sg_uf'] = isset($post['sg_uf']) ? $post['sg_uf'] : '';


                $sessao = Ead1_Sessao::getSessaoCarrinho();

                $loja = new LojaBO();

                if (!$sessao->getSt_email()) {
                    $mensageiro = $loja->verificaEmailEntidade($this->_getParam('st_email', false));
                    if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
                        $mensagem = $this->view->mensagem('email');
                        throw new Zend_Validate_Exception($mensagem);
//                    throw new Zend_Validate_Exception("Encontramos seu E-mail em nossa base de dados.<br> Por favor, clique
//                          <a href='" . $this->baseUrlModulo . "/pagamento/acesso' target='_parent'>aqui</a> para fazer seu Login <br>ou recuperar sua senha.");
                    }
                }

                $mensageiro = $loja->verificaCpfEntidade($post['st_cpf']);
                if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
                    $dadosUsuario = $mensageiro->getCodigo();
                    $mensagem = $this->view->mensagem('cpf', $dadosUsuario->getSt_cpf());
                    throw new Zend_Validate_Exception($mensagem);
//                    throw new Zend_Validate_Exception("Encontramos seu CPF em nossa base de dados.<br> Por favor, clique
//                          <a href='" . $this->baseUrlModulo . "/pagamento/acesso' target='_parent'>aqui</a> para fazer seu Login <br>ou recuperar sua senha.");
                }
                $uTO = new UsuarioTO();
                if ($sessao->getId_usuario()) {
                    $uTO->setId_usuario($sessao->getId_usuario());
                }
                $uTO->setBl_ativo(true);
                $uTO->setSt_cpf($post['st_cpf']);
                $uTO->setSt_senha($post['st_senha'], true);
                $uTO->setSt_nomecompleto($post['st_nomecompleto']);

                $loja = new LojaBO();
                $usuario = $loja->salvarPessoa($uTO, $post);

                Ead1_Sessao::setSessionGeral(array('id_usuario' => $usuario->mensagem[0]->id_usuario));

                $redesSociais = new Zend_Session_Namespace('redesSociais');

                if ($redesSociais->id_sistema && $redesSociais->st_codusuario) {
                    $usuarioIntegracao = new \G2\Negocio\UsuarioIntegracao();
                    $usuarioDados = $usuarioIntegracao->salvarIntegracao(array(
                        'id_sistema' => $redesSociais->id_sistema,
                        'id_usuariocadastro' => $usuario->mensagem[0]->id_usuario,
                        'id_usuario' => $usuario->mensagem[0]->id_usuario,
                        'dt_cadastro' => new \DateTime(),
                        'st_codusuario' => $redesSociais->st_codusuario,
                        'id_entidade' => Ead1_Sessao::getSessaoEntidade()->id_entidade,
                        'bl_encerrado' => 0
                    ));
                }

                if ($usuario->getTipo() == Ead1_IMensageiro::ERRO) {
                    throw new Zend_Exception($usuario->getFirstMensagem());
                } elseif ($usuario->getTipo() == Ead1_IMensageiro::AVISO) {
                    $mensagem = $this->view->mensagem('cpf', $this->_getParam('st_email', false));
                    $mensageiro = new Ead1_Mensageiro($mensagem, Ead1_IMensageiro::AVISO);
//                $mensageiro = new Ead1_Mensageiro('Encontramos seu CPF em nossa base de dados, por favor, efetue o Login para prosseguir!', Ead1_IMensageiro::AVISO);
                } elseif (!$sessao->getId_venda()) {

                    $uTO = $usuario->getFirstMensagem();
                    if (!$uTO instanceof UsuarioTO) {
                        $uTO = new UsuarioTO($uTO);
                    }

                    $meloja = $loja->salvarVendaBasica($uTO);

                    if ($meloja->getTipo() != Ead1_IMensageiro::SUCESSO) {
                        throw new Zend_Exception($meloja->getFirstMensagem());
                    }
                    $mensageiro = new Ead1_Mensageiro('Dados cadastrados com sucesso! Você será direcionado(a) para o próximo passo.'
                        , Ead1_IMensageiro::SUCESSO
                        , $meloja->getFirstMensagem());
                } else {
                    $ve = new VendaTO();
                    $ve->setId_venda($sessao->getId_venda());
                    $ve->fetch(true, true, true);
                    $mensageiro = new Ead1_Mensageiro('Dados atualizados com sucesso! Você será direcionado(a) para a página de pagamento.', Ead1_IMensageiro::SUCESSO, $ve);
                }

                $dao->commit();
            }
        } catch (Zend_Validate_Exception $e) {
            $dao->rollBack();
            SessaoLojaBO::limparSessaoUsuarioLoja();
            $mensageiro = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Exception $e) {
            $dao->rollBack();
            SessaoLojaBO::limparSessaoUsuarioLoja();
            $mensageiro = new Ead1_Mensageiro("Erro: " . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }

        $this->_Json($mensageiro);
    }

    /**
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     */
    public function acessarAction()
    {
        $post = $this->limpaGetAllParams($this->_getAllParams());
        if (empty($post['st_login']) || empty($post['st_senha'])) {
            $this->_Json(new Ead1_Mensageiro('Os Usuário e Senha são obrigatórios!', Ead1_IMensageiro::ERRO));
        } else {
            $loja = new LojaBO();
            $resultLogin = $loja->loginLoja($post['st_login'], $post['st_senha']);

            //verifica se achou o login
            if ($resultLogin->getTipo() == Ead1_IMensageiro::SUCESSO) {
                //pega o primeiro resultado do mensageiro
                $arrPessoa = $resultLogin->getFirstMensagem();
                //instancia a TO
                $uTO = new UsuarioTO();
                $uTO->montaToDinamico($arrPessoa); //monta os dados na TO
                //Salva a venda
                $meloja = $loja->salvarVendaBasica($uTO);
                //pega o retorno da venda
                if ($meloja->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    $mensageiro = $meloja;
                } else {
                    $mensageiro = new Ead1_Mensageiro('Dados cadastrados com sucesso! Você será direcionado(a) para o próximo passo.'
                        , Ead1_IMensageiro::SUCESSO
                        , array('id_venda' => $meloja->getFirstMensagem()->getid_venda()));
                }
            } else {
                $mensageiro = $resultLogin;
            }
            $this->_Json($mensageiro->toArrayAll());
        }

    }

    public function autorizarCartaoAction()
    {
        $id_venda = $this->_getParam('id_venda', null);

        if (!$id_venda) {
            $mensageiro = new Ead1_Mensageiro('Lançamento não encontrado!', Ead1_IMensageiro::ERRO);
        } else {

            try {
                $pagarme = new \G2\Negocio\Pagarme();

                $post = $this->limpaGetAllParams($this->getAllParams());
                $parcelaValor = explode('_', $post['nu_parcelas']);
                $post['nu_parcelas'] = $parcelaValor[0];

                $pv = new VwProdutoVendaTO();
                $pv->setId_venda($id_venda);
                $pv->fetch(false, true, true);

                if (is_null($pv)) {
                    throw new Exception('Venda não encontrada!');
                }

                if ($pv->getId_modelovenda() == \G2\Entity\ModeloVenda::ASSINATURA) {
                    $mensageiro = $pagarme->transactionSubscription($post);
                } else {
                    $mensageiro = $pagarme->transactionCreditCard($post);
                }

                $sessao = (array)Ead1_Sessao::getSessaoCarrinho();

                $carrinho = array(
                    'id_usuario' => $sessao['id_usuario'],
                    'st_nomecompleto' => $sessao['st_nomecompleto'],
                    'id_entidade' => $sessao['id_entidade'],
                    'st_nomeentidade' => $sessao['st_nomeentidade'],
                    'st_urlimglogo' => $sessao['st_urlimglogo'],
                    'id_sistema' => $sessao['id_sistema'],
                    'st_codchave' => $sessao['st_codchave'],
                    'st_urlconfirmacao' => $sessao['st_urlconfirmacao'],
                    'st_email' => $sessao['st_email'],
                    'st_cpf' => $sessao['st_cpf'],
                    'st_vendaexterna' => $sessao['st_vendaexterna'],
                    'st_urlvendaexterna' => $sessao['st_urlvendaexterna']
                );

                \SessaoLojaBO::limparSessaoLoja();

                \Ead1_Sessao::setSessionCarrinho($carrinho);
                if ($mensageiro->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                    $mensageiro = new \Ead1_Mensageiro(
                        $mensageiro->getFirstMensagem()
                        , \Ead1_IMensageiro::AVISO
                        , $mensageiro->getCodigo()
                    );
                } else {
                    $mensageiro = new \Ead1_Mensageiro(
                        $mensageiro->getFirstMensagem()
                        , \Ead1_IMensageiro::SUCESSO
                        , $mensageiro->getCodigo()
                    );
                }

            } catch (Exception $e) {
                $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO, $e);
            }
        }

        $this->_Json($mensageiro);
    }

    /**
     * Realiza o pagamento por cartão de crédito
     *
     * @return void
     */
    public function autorizarCartaoBraspagAction()
    {
        $bo = new Ead1_BO();
        $id_venda = (integer)$this->_getParam('id_venda', null);

        if ($id_venda) {
            try {

                $post = $this->limpaGetAllParams($this->getAllParams());
                $parcelaValor = explode('_', $post['nu_parcelas']);
                $parcelaValor[1] = $bo->desFormataValor($parcelaValor[1]);

                // veridica se a diferenca entre os dois campos ultrapassa 10 (centavos)
                // visto que há arredondamento de valores nas divisões das parcelas
                $vlDiffEntreValores = $parcelaValor[1] - number_format($venda->getNu_valorliquido(), 2, '', '');
                if ($vlDiffEntreValores > 10) {
                    throw new Exception(
                        'Valor da venda inconsistente, favor entrar em contato com instituição.'
                    );
                }

                $post['id_venda'] = $id_venda;
                $post['id_cartaobandeira'] = $this->_getParam('id_cartaobandeira', null);

                $id_cartaobandeira = $this->_getParam('id_cartaobandeira', null);
                $pagamento = new PagamentoCartaoBO(
                    $venda->getId_entidade()
                    , $id_cartaobandeira ?: CartaoBandeira::VISA
                    , $this->_getParam('id_meiopagamento', MeioPagamento::CARTAO_CREDITO)
                );

                //Recupera as configurações de cartão da entidade e verifica se é instancia vw_entidadecartao
                if ($pagamento->entidadecartao instanceof VwEntidadeCartaoTO) {
                    //atribui na variavel
                    $VwEntidadeCartaoTO = $pagamento->entidadecartao;

                    //cria uma posição no post para cartaooperadora
                    $post['id_cartaooperadora'] = $VwEntidadeCartaoTO->getId_cartaooperadora();
                    $post['id_cartaobandeira'] = $VwEntidadeCartaoTO->getId_cartaobandeira();
                    $post['id_cartaoconfig'] = $VwEntidadeCartaoTO->getId_cartaoconfig();
                }

                $venda = new VendaTO();
                $venda->setId_venda($id_venda);
                $venda->fetch(true, true, true);

                $pagamento = new PagamentoCartaoBO($venda->getId_entidade(), (!empty($id_cartaobandeira) ? $id_cartaobandeira : 1), $this->_getParam('id_meiopagamento', 1));

                $id_modelovenda = $pagamento->retornaModeloVenda($id_venda);

                if (\G2\Entity\ModeloVenda::ASSINATURA == $id_modelovenda) {
                    $post['nu_parcelas'] = 1;
                    $post['nu_valortotal'] = $venda->getNu_valorbruto();
                    $post['nu_valorparcela'] = ($venda->getNu_valorbruto() / $parcelaValor[0]);
                    $post['id_modelovenda'] = \G2\Entity\ModeloVenda::ASSINATURA;
                } else {
                    $post['nu_parcelas'] = $parcelaValor[0];
                    $post['nu_valortotal'] = $venda->getNu_valorbruto();
                    $post['nu_valorparcela'] = ($venda->getNu_valorbruto() / $parcelaValor[0]);
                    $post['id_modelovenda'] = $id_modelovenda;
                }

                $mensageiro = $pagamento->autorizarCartao($post);
                $sessao = (array)Ead1_Sessao::getSessaoCarrinho();

                $carrinho = array(
                    'id_usuario' => $sessao['id_usuario'],
                    'st_nomecompleto' => $sessao['st_nomecompleto'],
                    'id_entidade' => $sessao['id_entidade'],
                    'st_nomeentidade' => $sessao['st_nomeentidade'],
                    'st_urlimglogo' => $sessao['st_urlimglogo'],
                    'id_sistema' => $sessao['id_sistema'],
                    'st_codchave' => $sessao['st_codchave'],
                    'st_urlconfirmacao' => $sessao['st_urlconfirmacao'],
                    'st_email' => $sessao['st_email'],
                    'st_cpf' => $sessao['st_cpf'],
                    'st_vendaexterna' => $sessao['st_vendaexterna'],
                    'st_urlvendaexterna' => $sessao['st_urlvendaexterna']
                );

                SessaoLojaBO::limparSessaoLoja();
                Ead1_Sessao::setSessionCarrinho($carrinho);

                if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    $mensageiro = new Ead1_Mensageiro(
                        $mensageiro->getFirstMensagem()
                        , Ead1_IMensageiro::AVISO
                        , $mensageiro->getCodigo()
                    );
                } else {
                    $mensageiro = new Ead1_Mensageiro(
                        $mensageiro->getFirstMensagem()
                        , Ead1_IMensageiro::SUCESSO
                        , $mensageiro->getCodigo()
                    );
                }
            } catch (Exception $exc) {
                $mensageiro = new Ead1_Mensageiro(
                    $exc->getMessage()
                    , Ead1_IMensageiro::ERRO
                    , $exc
                );
            }
        } else {
            $mensageiro = new Ead1_Mensageiro(
                'Lançamento não encontrado!'
                , Ead1_IMensageiro::ERRO
            );
        }

        $this->_Json($mensageiro);
    }

    /**
     * Realiza o pagamento por cartão de crédito
     *
     * @return void
     */
    public function mudarCartaoAction()
    {

        $id_venda = $this->_getParam('id_venda', null);
        if (!$id_venda) {
            $mensageiro = new Ead1_Mensageiro('Lançamento não encontrado!', Ead1_IMensageiro::ERRO);
        } else {
            try {

                $venda = new VendaTO();
                $venda->setId_venda($id_venda);
                $venda->fetch(true, true, true);

                $post = $this->limpaGetAllParams($this->getAllParams());

                $negocio = new \G2\Negocio\Pagarme();
                /** @var \G2\Entity\VwResumoVendaLancamento $lancamento */
                $lancamento = $negocio->findOneBy('G2\Entity\VwResumoVendaLancamento', array('id_venda' => $id_venda));


                if ($lancamento) {

                    if (
                        $lancamento->getid_sistemacobranca() == \G2\Constante\Sistema::PAGARME
                        &&
                        substr($lancamento->getRecorrenteOrderid(), -3) != 'G1U'
                        &&
                        substr_count($lancamento->getRecorrenteOrderid(), '-') != 4
                    ) {
                        $mensageiro = $negocio->mudarCartaoAssinatura($post);
                    } else {
                        $parcelaValor = explode('_', $post['nu_parcelas']);
                        $post['id_venda'] = $id_venda;
                        $pagamento = new \BraspagRecorrenteBO($venda->getId_entidade());
                        $post['id_cartaobandeira'] = $post['nu_cartaoUtilizadoRecorrente'] ? $post['nu_cartaoUtilizadoRecorrente'] : CartaoBandeiraTO::BANDEIRA_VISA;

                        $mensageiro = $pagamento->mudarCartaoRecorrente($post);
                    }

                }
                SessaoLojaBO::limparSessaoLoja();

                if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    $pagarme = new \G2\Negocio\Pagarme();
                    $status = $pagarme->getSubscription($venda->getRecorrenteOrderid(), $venda->getId_entidade());
                    if (!empty($mensageiro) &&
                        $mensageiro instanceof Ead1_Mensageiro &&
                        $mensageiro->getCodigo() instanceof PagarMe_Error &&
                        $mensageiro->getCodigo()->getType() === 'action_forbidden' &&
                        $status->getMensagem()[0]['status'] === 'canceled') {
                            $mensageiro = $this->mudarCartaoCancelado($post);
                    } else {
                        $mensageiro = new Ead1_Mensageiro($mensageiro->getFirstMensagem(), Ead1_IMensageiro::AVISO);
                    }
                } else {
                    $mensageiro = new Ead1_Mensageiro($mensageiro->getFirstMensagem(), Ead1_IMensageiro::SUCESSO);
                }
            } catch (Exception $e) {
                $mensageiro = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
            }
        }
        @$this->_Json($mensageiro);
    }

    /**
     * Realiza a solicitação de Boleto Bancário
     *
     * @return void
     */
    public function solicitarBoletoAction()
    {

        $id_venda = $this->_getParam('id_venda', false);
        if (!$id_venda) {
            $mensageiro = new Ead1_Mensageiro('Venda não encontrada!', Ead1_IMensageiro::ERRO);
        } else {
            try {
                $paBO = new PagamentoBO();
                $mensageiro = $paBO->gerarLancamentoBoleto($id_venda, $this->_getParam('nu_parcelas', 0));
            } catch (Exception $e) {
                $mensageiro = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
            }
        }
        $this->_Json($mensageiro);
    }

    public function boletoVendaAction()
    {
        $this->ajax();
        try {
            $id_venda = $this->getRequest()->getParam('id_venda', null);
            if (!$id_venda) {
                throw new Zend_Exception('Informe a Venda para listagem dos Boletos');
            }
            $vendaTO = new VendaTO();
            $vendaTO->setId_venda($id_venda);
            $vendaTO->fetch(true, true, true);

            $bo = new VendaBO();
            $vendaUsuarioTO = new VwVendaUsuarioTO();
            $vendaUsuarioTO->setId_venda($vendaTO->getId_venda());
            $vendaUsuarioTO->setId_evolucao(VendaTO::EVOLUCAO_AGUARDANDO_RECEBIMENTO);
            $vendaUsuarioTO->setId_situacao(VendaTO::SITUACAO_VENDA_ATIVA);
            $vu = $bo->retornarVwVendaUsuario($vendaUsuarioTO);

            if ($vu->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Exception('Nenhuma Venda encontrada para pagamento.');
            }
            $this->_helper->json($vu);

            $mensagemBO = new MensagemBO();
            $mensagem = $mensagemBO->gerarMensagemEnvioBoleto($vendaTO, TipoEnvioTO::HTML);
            $this->view->mensagem = $mensagem['mensagemTO']->getSt_texto();
            $this->view->st_conversao = $vu->getFirstMensagem()->getSt_conversao();

            if ($vu->getFirstMensagem()->getSt_linkloja()) {
                $this->view->mensagem .= '<br><br><br><a href="' . $vu->getFirstMensagem()->getSt_linkloja() . '" target="_parent">Voltar a Loja</a>';
            }
        } catch (Exception $e) {
            $this->view->mensagem = $e->getMessage();
        }
        $this->render('index_indisponivel');
    }

    /**
     * Realiza a emissão de boletos
     *
     * @todo verificar forma dinâmica de imagem de entidade
     * @todo testar com o valor a partir do FLEX
     * @todo remover id_lancamento fixo no código para testes
     * @param bool $verificaRetorno Este parametro define se a url do boleto vai ser verificada e desta forma
     * se for vazia, exibe mensagem, se contiver a url redireciona para ela.
     * @return void
     */
    public function boletoAction()
    {
        $this->ajax();
        try {

            $id_lancamento = $this->getRequest()->getParam('id_lancamento', null);
            $soDados = $this->getRequest()->getParam('dados', false);
            $verificaRetorno = $this->getRequest()->getParam('verificaretorno', false);
            $boletoPHP = new \G2\Negocio\BoletoPHP();

            $boleto = $boletoPHP->emitirBoleto($id_lancamento, $soDados, true, $verificaRetorno);

            if (!$boleto) {
                throw new \Exception('Não foi possível gerar o Boleto.');
            }

            if ($boleto instanceof Ead1_Mensageiro) {
                if ($boleto->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                    $this->redirect($boleto->getFirstMensagem()->getSt_urlboleto());
                } else if ($boleto->getTipo() == \Ead1_IMensageiro::AVISO) {
                    $email = "cadastrado";

                    $vw = new \G2\Entity\VwLancamento();

                    $vw->find($id_lancamento);
                    if ($vw->getId_venda()) {
                        $venda = new \G2\Entity\VwClienteVenda();
                        $venda->findOneBy(array('id_venda' => $vw->getId_venda()));
                        $negocio = new \G2\Negocio\Negocio();
                        if ($venda->getSt_email()) {
                            $email = $venda->getSt_email();
                        }
                    }

                    $mensagem = "Caro Usuário, a solicitação para geração de boleto foi enviada com sucesso!<br>
                                Enviaremos uma notificação para o e-mail $email quando o boleto estiver disponível para pagamento.";

                    $this->disableAjax();
                    $this->view->resultado = $mensagem;

                } else {
                    throw new \Exception('Não foi possível gerar o Boleto.');
                }
            }

            if ($soDados) {
                $this->_helper->json($boleto);
            }

        } catch (Exception $exc) {
            echo 'Erro: ' . $exc->getMessage();
        }
    }

    /**
     * @author Helder Silva <helder.silva@example.com>
     * Action da tela de geração de boletos, passa os dados do formulário para serem
     * utilizados posteriormente na geração do boleto.
     * @throws Zend_Exception
     * @throws Exception
     */
    public function gerarBoletoAction()
    {
        try {
            $request = $this->getRequest();
            $data = $this->getAllParams();
            unset($data['controller'], $data['action'], $data['module']);
            if ($data) {
                $sessao = Ead1_Sessao::getSessaoCarrinho();
                $this->view->sessaoCarrinho = $sessao;
                $this->view->id_entidade = $sessao->getId_entidade();

                $esquemaNegocio = new \G2\Negocio\EsquemaConfiguracao();

//                $configuracaoEntidade = new \G2\Negocio\ConfiguracaoEntidade;
//                $resultCodeGoogleBoleto = $configuracaoEntidade->findConfiguracaoEntidade($sessao->id_entidade);

                //Instancia venda
                $negocioVenda = new \G2\Negocio\Venda();
                //busca os lancamentos da venda passada pelo parametro
                $resultVenda = $negocioVenda->retornaVwVendaLancamento(array('id_venda' => $data['id_venda']));

                //se o resultado for igual a 1, significa que só tem um lancamento cadastrado
                if (count($resultVenda) == 1) {
                    //então pegamos a posição zero do retorno, lembrando que
                    //o método retornaVwVendaLancamento(), faz um findBY e retorna um array de objetos
                    //por isso pegamos a posição zero e transformamos o objeto em um array
                    //atribuindo o array a variavel data, sobrescrevendo tudo que havia nela.
                    $data = $negocioVenda->toArrayEntity($resultVenda[0]);
                }
                //retorna o $data para a view
                $this->view->dados = $data;
                $this->view->configuracaoEntidade = $esquemaNegocio->retornarConfiguracaoCampoGoogleBoleto();;
//                $this->view->configuracaoEntidade = $resultCodeGoogleBoleto['st_campotextogoogleboleto'];

                $vendaTO = new VendaTO();
                $vendaTO->setId_venda($data['id_venda']);
                $vendaTO->fetch(true, true, true);

                $textoLoja = new G2\Negocio\TextoSistema();
                $resultadoTexto = $textoLoja->retornaTextoMensagemPadrao(26, $vendaTO->getId_entidade(), array('id_venda' => $data['id_venda']));
                $this->view->mensagemConfirmacao = $resultadoTexto;
            } else {
                throw new Exception("Dados da venda não definidos.");
            }
        } catch (Exception $e) {
            throw new Zend_Exception("Erro ao buscar dados para gerar boleto." . $e->getMessage());
        }
    }

    public function confirmarCartaoAction()
    {
        try {
            //ini_set('display_errors', 1);
            $this->view->url_acesso = null;

            $data = $this->limpaGetAllParams($this->getAllParams());

            if (!array_key_exists('id_venda', $data) && !array_key_exists('id_vendarecorrente', $data)) {
                throw new Zend_Exception('ID da venda não encontrado!');
            }

            if (array_key_exists('id_vendarecorrente', $data)) {
                $id_venda = $data['id_vendarecorrente'];
            } else {
                $id_venda = $data['id_venda'];
            }

            //Busca dos dados de conversão do Google
            $sessao = Ead1_Sessao::getSessaoCarrinho();
            $esquemaConfiguracao = new \G2\Negocio\EsquemaConfiguracao();
            $this->view->configuracaoEntidade = $esquemaConfiguracao->retornarConfiguracaoCampoGoogleCredito($sessao->getId_entidade());


            //se existir dados da pagar-me
            if (array_key_exists('pagarme', $data)) {

                //Executa a captura dos dados e seta o metadata na pagarme

                $transData = array(
                    'amount' => $data['amount'],
                    'installments' => $data['pagarme']['installments'],
                    'card_hash' => $data['pagarme']['card_hash'],
                    'payment_method' => 'credit_card',
                    'async' => 'false',
                    'metadata' => array(
                        'id_venda' => $data['id_venda'],
                        'id_meiopagamento' => $data['id_meiopagamento']
                    )
                );

                $informacoes = array('id_venda' => $id_venda,
                    'id_meiopagamento' => $data['id_meiopagamento']);

                $transaction = new \G2\Negocio\Pagarme();
                $mensageiro = $transaction->transactionCreditCard($informacoes, $transData);
                if ($mensageiro->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception($mensageiro->getFirstMensagem());
                }
            }


            if ($data) {

                //cria o job para ativacao automatica. Objetivo é melhorar o desempenho
                //deste processo.

                $negocio = new \G2\Negocio\Negocio();
                $schedule_time = date('Y-m-d H:i:s', time() + 10);

                $negocio->createHttpJob(
                    Ead1_Ambiente::geral()->st_url_gestor2 . '/loja/robo/zend-job-ativacao-automatica', array('id_venda' => $id_venda), array(
                    'name' => 'Ativacao da venda.',
                    'schedule_time' => $schedule_time,
                    'queue_name' => 'Ativacao Automatica'
                ),
                    \G2\Constante\Sistema::GESTOR
                );


                $sessao = Ead1_Sessao::getSessaoCarrinho();
                $this->view->id_entidade = $sessao->getId_entidade();
                $this->view->sessao = $sessao;
                $this->view->sessaoCarrinho = $sessao;
                $this->view->dados = $data;

                $vendaTO = new VendaTO();

                $vendaTO->setId_venda($id_venda);

                $vendaTO->fetch(true, true, true);

                $textoLoja = new G2\Negocio\TextoSistema();
                $resultado = $textoLoja->retornaTextoMensagemPadrao(27, $vendaTO->getId_entidade(), array('id_venda' => $id_venda));
                $this->view->mensagemConfirmacao = $resultado;


                /**
                 * Retorna os países
                 */
                $form = new CadastroRapidoPessoaForm();
                $this->view->form = $form;

                $this->view->id_entidade = $vendaTO->getId_entidade();
                $vwpessoa = new VwPessoaTO();
                $vwpessoa->setId_entidade($vendaTO->getId_entidade());
                $vwpessoa->setId_usuario($vendaTO->getId_usuario());
                $vwpessoa->fetch(false, true, true);

                $this->view->pessoa = ($vwpessoa instanceof VwPessoaTO) ? $vwpessoa : new VwPessoaTO();

                $this->view->venda = $id_venda;
                $this->view->usuario = $vendaTO->getId_usuario();
                $this->view->entidade = $vendaTO->getId_entidade();
                $this->view->dt_nascimento = $vwpessoa->getDt_nascimento() ? $vwpessoa->getDt_nascimento()->toString('dd/MM/YYYY') : '';


                $esquemaConfiguracao = new \G2\Negocio\EsquemaConfiguracao();
                $esquema = $esquemaConfiguracao->retornarEsquemaValores(array('id_entidade' => $sessao->getId_entidade(), 'id_itemconfiguracao' => 17));
                $this->view->formcontrato = $esquema->getTipo();


                $recebimento = new \G2\Negocio\Recebimento();
                $vwmatricula = $recebimento->findOneBy('\G2\Entity\VwMatricula', array('id_venda' => $id_venda));
                if ($vwmatricula) {

                    if (!$vwmatricula->getSt_urlportal()) {
                        $vwmatricula->setSt_urlportal(Ead1_Ambiente::geral()->st_url_portal);
                    }

                    $url_acesso = $vwmatricula->getSt_urlportal() . '/portal/login/autenticacao-direta?l=sim&e=' . $vwmatricula->getId_entidadematricula() . '&p=' . $vwmatricula->getId_usuario() . '&u=' . $vwmatricula->getSt_urlacesso();

                    $this->view->url_acesso = $url_acesso;
                }

            }

        } catch (Exception $exc) {
            throw new Zend_Exception("Erro no pagamento do cartão. " . $exc->getMessage());
        }
    }

    /**
     * Recebe um post do Gateway de pagamento e retorna OK em caso de recebido com sucesso
     *
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     */
    public function verificarStatusVendaBraspagAction()
    {

        $post = $this->limpaGetAllParams($this->_getAllParams());

        if (isset($post['OrderId'])) {

            $tran = new TransacaoFinanceiraTO();
            $tran->setId_venda($post['OrderId']);
        }

//      <status>OK</status>
    }

    public function confirmacaoAction()
    {

        try {
            $id_venda = $this->getRequest()->getParam('id_venda', null);
            if (!$id_venda) {
                throw new Zend_Exception('Informe a Venda');
            }

            $vendaTO = new VendaTO();
            $vendaTO->setId_venda($id_venda);
            $vendaTO->fetch(true, true, true);

            $bo = new VendaBO();
            $vendaUsuarioTO = new VwVendaUsuarioTO();
            $vendaUsuarioTO->setId_venda($vendaTO->getId_venda());
            $vu = $bo->retornarVwVendaUsuario($vendaUsuarioTO);

            if ($vu->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Exception('Nenhuma Venda encontrada para pagamento.');
            }

            $this->view->st_conversao = $vu->getFirstMensagem()->getSt_conversao();
            $this->view->st_linkloja = $vu->getFirstMensagem()->getSt_linkloja();
            $this->view->id_venda = $vu->getFirstMensagem()->getId_venda();
        } catch (Exception $e) {
            $this->view->mensagem = $e->getMessage();
        }
    }

    public function salvaEnderecoAction()
    {
        try {
            $sessao = Ead1_Sessao::getSessaoCarrinho();
            $this->_helper->viewRenderer->setNoRender();
            if ($this->getRequest()->isPost()) {
                $body = $this->getRequest()->getPost();
                $negocio = new \G2\Negocio\PessoaEndereco();
                $result = $negocio->gravaEndereco($body, $sessao->id_usuario);
                $this->_helper->json($result);
            }
        } catch (\Exception $e) {
            throw new Zend_Exception('Problema ao cadastrar endereço.' . $e->getMessage());
        }
    }

    public function validaEnderecoAction()
    {
        try {
            $this->_helper->viewRenderer->setNoRender();
            $body = $this->getRequest()->getPost();
            $negocio = new \G2\Negocio\PessoaEndereco();
            $result = $negocio->validaEndereco($body);
            $this->_helper->json($result);
        } catch (\Exception $e) {
            throw new Zend_Exception('Problema ao valida endereço.' . $e->getMessage());
        }
    }

    /**
     * Salva os dados retornados do Facebook
     *
     * @author Neemias Santos - neemias.santos@unyleya.com.br
     */
    public function facebookAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        if ($this->getRequest()->getPost()) {
            $post = $this->getRequest()->getPost();

            $usuarioIntegracao = new \G2\Negocio\UsuarioIntegracao();

            $idFacebook = $usuarioIntegracao->findStCodusuario($post['id_facebook']);
            /*
            * Faz uma verificação para pegar o id_facebook e verificar se já foi cadastrado aquele
            * id, depois isso fazer uma verificação se já esta cadastrado na tb_usuario integração
            */

            if (!$idFacebook) {
                /*
                 * Método cria uma sessão para os dados do Google para serem salvos na tb_usuariointegracao
                 * após salvar o user
                 */
                $sessaoRedesSociais = new Zend_Session_Namespace('redesSociais');
                $sessaoRedesSociais->id_sistema = SistemaTO::FACEBOOK;
                $sessaoRedesSociais->st_codusuario = $post['id_facebook'];
                $sessaoRedesSociais->st_email = $post['st_email'];
                $sessaoRedesSociais->st_nomecompleto = $post['st_nomecompleto'];
                $this->_helper->json('cadastro');
            } else {
                $tb_usuariointegracao = new UsuarioIntegracaoTO();
                $tb_usuariointegracao->setId_sistema(SistemaTO::FACEBOOK);
                $tb_usuariointegracao->setSt_codusuario($post['id_facebook']);
                $tb_usuariointegracao->fetch(false, true, true);

                $pessoa = new  \G2\Negocio\Pessoa();
                $resultPessoa = $pessoa->retornaDadosPessoa(array('id_usuario' => $tb_usuariointegracao->getId_usuariocadastro()));
                $loja = new LojaBO();
                $resultLogin = $loja->loginLoja($resultPessoa[0]['st_email'], $resultPessoa[0]['st_senhaentidade']);

                //pega o primeiro resultado do mensageiro
                $arrPessoa = $resultLogin->getFirstMensagem();
                //instancia a TO
                $uTO = new UsuarioTO();
                $uTO->montaToDinamico($arrPessoa); //monta os dados na TO
                //Salva a venda
                $meloja = $loja->salvarVendaBasica($uTO);
                $urlReturn = '?venda=' . $meloja->mensagem[0]->id_venda;
                $this->_helper->json($urlReturn);
            }
        }
    }

    public function confirmaContratoAction()
    {
        if ($this->getRequest()->getPost()) {
            $post = $this->getRequest()->getPost();
            $arrayDados['id_usuario'] = $post['id_usuario'];
            $arrayDados['id_venda'] = $post['id_usuario'];
            $arrayDados['id_entidade'] = $post['id_entidade'];
            $arrayDados['st_nomecompleto'] = $post['st_nomecompleto'];
            $arrayDados['dt_nascimento'] = $post['dt_nascimento'];
            $arrayDados['st_sexo'] = $post['st_sexo'] == 1 ? 'M' : 'F';

            $arrayEndereco['correspondencia'] = $post['correspondencia'];
            $arrayEndereco['id_venda'] = $post['id_venda'];
            $arrayEndereco['id_usuario'] = $post['id_usuario'];
            $arrayEndereco['id_entidade'] = $post['id_entidade'];
            $arrayEndereco['st_cep'] = $post['st_cep'];
            $arrayEndereco['st_endereco'] = $post['st_endereco'];
            $arrayEndereco['st_bairro'] = $post['nu_numero'];
            $arrayEndereco['st_complemento'] = $post['st_complemento'];
            $arrayEndereco['nu_numero'] = $post['nu_numero'];
            $arrayEndereco['id_municipio'] = $post['municipio'];

            if ($post['correspondencia']) {
                $arrayEndereco['id_pais_correspondencia'] = $post['id_pais_corresp'];
                $arrayEndereco['sg_uf_correspondencia'] = $post['sg_uf_corresp'];
                $arrayEndereco['st_cep_correspondencia'] = $post['st_cep_correpondencia'];
                $arrayEndereco['st_endereco_correspondencia'] = $post['st_endereco_correspondencia'];
                $arrayEndereco['st_bairro_correspondencia'] = $post['st_bairro_correspondencia'];
                $arrayEndereco['st_complemento_correspondencia'] = $post['st_complemento_correspondencia'];
                $arrayEndereco['nu_numero_correspondencia'] = $post['nu_numero_correspondencia'];
                $arrayEndereco['id_municipio_correspondencia'] = $post['municipios_correspondencia'];
            }

            $negocioPessoa = new \G2\Negocio\Pessoa();
            $negocioPessoa->salvarPessoaLojaContrato($arrayDados);

            $negocio = new \G2\Negocio\Endereco();
            $negocio->salvaEnderecoLoja($arrayEndereco);

            $recebimento = new \G2\Negocio\Recebimento();
            $vwmatricula = $recebimento->findOneBy('\G2\Entity\VwMatricula', array('id_venda' => $post['id_venda']));
            if ($vwmatricula) {

                if (!$vwmatricula->getSt_urlportal()) {
                    $vwmatricula->setSt_urlportal(Ead1_Ambiente::geral()->st_url_portal);
                }

                $url_acesso = $vwmatricula->getSt_urlportal() . '/portal/login/autenticacao-direta?l=sim&e=' . $vwmatricula->getId_entidadematricula() . '&p=' . $vwmatricula->getId_usuario() . '&u=' . $vwmatricula->getSt_urlacesso();

                $this->view->url_acesso = $url_acesso;
            }

            $sessao = Ead1_Sessao::getSessaoCarrinho();
            $this->view->sessaoCarrinho = $sessao;

            $textoLoja = new G2\Negocio\TextoSistema();
            $resultado = $textoLoja->retornaTextoMensagemPadrao(27, $post['id_entidade'], array('id_venda' => $post['id_venda']));
            $this->view->mensagemConfirmacao = $resultado;

        }
    }

    private function mudarCartaoCancelado($data){
        $pagarme = new \G2\Negocio\Pagarme();

        try {
            if (empty($data)) {
                throw new Exception('Informe os dados para reativação da assinatura.');
            }

            return $pagarme->trocarCartaoAssinaturaCancelada($data);
        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }
}

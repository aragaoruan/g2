<?php

/**
 * Helper para retornar mensagem
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Loja_View_Helper_Mensagem extends Zend_View_Helper_Abstract
{

    const EMAIL = 'email';
    const CPF = 'cpf';
    const NO_EMAIL = 'no-email';

    /**
     * @var string email 
     */
    private $stringEmail;

    /**
     * Gera a mensagem
     * @param string $tipo tipo da mensagem a ser gerada
     * @param string $string string, parametro esperado para mensagem do tipo email
     * @return string string com a mensagem
     * @throws Exception
     */
    public function mensagem ($tipo, $string = null)
    {
        try {
            //verifica se o tipo é cpf e se a string é vazia
            if ($tipo == self::CPF && empty($string)) {
                //cria um exception caso a string no tipo email estiver vazia
                return $this->geraString(self::NO_EMAIL); //retorna a mensagem
            } else if ($tipo == self::CPF && !empty($string)) {//verifica se é do tipo cpf e se a string não é vazia
                $this->stringEmail = $string; //seta no atributo private o valor da string que geralmente é o email
                return $this->geraString($tipo); //retorna a string
            }if ($tipo == self::EMAIL) {//verifica se é do tipo email e se a string não é vazia
                return $this->geraString($tipo); //retorna a string
            }
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    /**
     * Gera a string da mensagem
     * @param string $tipo
     * @return string
     * @throws Exception
     */
    private function geraString ($tipo)
    {
        try {
            $mensagem = null;
            //busca a string da mensagem de acordo com o tipo
            switch ($tipo) {
                case self::CPF://mensagem cpf
                    $mensagem = "O CPF utilizado já possui cadastro com o e-mail: {$this->geraMascaraEmail()}.<br>";
                    $mensagem .= "Clique <a href='/pagamento/acesso' target='_parent'>aqui para entrar.</a><br>";
                    $mensagem .= "Caso não lembre a sua senha clique <a href='javascript:void(0);' onClick='recuperarSenha($(this),\"{$this->stringEmail}\")' data-loading-text='Enviando...'>aqui</a> que enviaremos as orientações para redefinição.";
                    break;
                case self::EMAIL:
                    $mensagem = "Encontramos seu E-mail em nossa base de dados.<br> ";
                    $mensagem .= "Por favor, clique <a href='/pagamento/acesso' target='_parent'>aqui</a> para fazer seu Login <br>ou recuperar sua senha.";
                    return $mensagem;
                    break;
                case self::NO_EMAIL:
                    $mensagem = "O CPF informado foi encontrado em nossa base de dados, porém nenhum e-mail foi cadastrado para o CPF.<br>";
                    $mensagem .= "Por favor entre em contato com nosso suporte.";
                    break;
                default:
                    throw new Exception("Mensagem indefinida.");
                    break;
            }
            return $mensagem;
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    /**
     * Pega o email e cria uma "mascara"
     * @return string
     */
    private function geraMascaraEmail ()
    {
        $emailArr = explode("@", $this->stringEmail); //quebra a string pelo @
        $userMail = substr($emailArr[0], 0, 4); //pega o 4 primeiros caracteres 
        $dominio = "@" . $emailArr[1]; //monta o dominio adicionando o @
        $email = $userMail . "****" . $dominio; //monta a string
        return $email;
    }

}

<?php

/**
 * Helper para retornar os dados do google configurados lá na tela de pessoa juridica aba configuração
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Loja_View_Helper_Analytics extends Zend_View_Helper_Abstract
{
    /**
     * @param $idEntidade
     * @return array
     * @throws Exception
     */
    public function analytics($idEntidade)
    {
        try {

            $negocio = new \G2\Negocio\ConfiguracaoEntidade();
            /** @var \G2\Entity\ConfiguracaoEntidade $result */
            $result = $negocio->findConfiguracaoEntidade($idEntidade, false);

            //verifica se tem resultado
            if ($result) {
                return [
                    "google_analytics" => $result->getSt_codgoogleanalytics(),
                    "google_tagmanager" => $result->getSt_googletagmanager()
                ];
            }

            return [];

        } catch (Exception $ex) {
            throw $ex;
        }
    }


}

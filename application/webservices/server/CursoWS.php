<?php
/**
 * Classe de regras de negócio para Serviços relacionados a Curso
 * @author Elcio Mauro Guimarães
 * @since 19/07/2012
 */
class CursoWS {
	
	/**
	 * @var Ead1_Mensageiro
	 */
	public $mensageiro;
	
	
	public function __construct(){
		$this->mensageiro = new Ead1_Mensageiro();
	}

	/**
	 * Método para Listar os Projetos Pedagógicos
	 * @param array $parametros
	 * @return array
	 */
	public function listar(array $parametros){
		
		$to = new VwAreaProjetoProdutoHoldingTO();
		
		if($parametros['id_entidadematriz']!=$to->getSessao()->id_entidade){
			$this->mensageiro->setMensageiro('Acesso inválido', Ead1_IMensageiro::ERRO);
		} else {
			
			try {
				$to->montaToDinamico($parametros);
				$orm = new VwAreaProjetoProdutoHoldingORM();
				$dados = $orm->consulta($to, false, false, array('st_areaconhecimento','st_tituloexibicaoprojeto','st_nomeentidade'));
				$this->mensageiro->setMensageiro($dados, Ead1_IMensageiro::SUCESSO);
			} catch (Exception $e) {
				$this->mensageiro->setMensageiro('Erro ao retornar os dados. Entre em contato com a EAD1.', Ead1_IMensageiro::ERRO);
			}


		}
		
		return Ead1_BO::arrayUtfEncode((array)$this->mensageiro->toArrayAll());
	}
	
	
	/**
	 * Método para Listar os Projetos Pedagógicos
	 * @param array $parametros
	 * @return array
	 */
	public function listarprodutos(array $parametros){
	
		$to = new VwProdutoTO();
	
		if($parametros['id_entidade']!=$to->getSessao()->id_entidade){
			$this->mensageiro->setMensageiro('Acesso inválido', Ead1_IMensageiro::ERRO);
		} else {
	
			try {
				$to->montaToDinamico($parametros);
				$orm = new VwProdutoORM();
				$dados = $orm->consulta($to, false, false, array('st_produto'));
				$this->mensageiro->setMensageiro($dados, Ead1_IMensageiro::SUCESSO);
			} catch (Exception $e) {
				$this->mensageiro->setMensageiro('Erro ao retornar os dados. Entre em contato com a EAD1.', Ead1_IMensageiro::ERRO);
			}
	
	
		}
	
		return Ead1_BO::arrayUtfEncode((array)$this->mensageiro->toArrayAll());
	}
	
	/**
	 * Método para Listar as Turmas por Produto
	 * @param array $parametros
	 * @return array
	 */
	public function listarturmasproduto(array $parametros){
		$to = new VwTurmasProdutoTO();
		
		if($parametros['id_entidade']!=$to->getSessao()->id_entidade){
			$this->mensageiro->setMensageiro('Acesso inválido', Ead1_IMensageiro::ERRO);
		} else {
			
			try {
                unset($parametros['id_entidade']);
				$to->montaToDinamico($parametros);
				$orm = new VwTurmasProdutoORM();
				$dados = $orm->consulta($to, false, false, array('st_tituloexibicao','st_turma'));
				$this->mensageiro->setMensageiro($dados, Ead1_IMensageiro::SUCESSO);
			} catch (Exception $e) {
				$this->mensageiro->setMensageiro('Erro ao retornar os dados. Entre em contato com a EAD1.', Ead1_IMensageiro::ERRO);
			}


		}
		
		return Ead1_BO::arrayUtfEncode((array)$this->mensageiro->toArrayAll());
	}
	
	/**
	 * Método para Retornar um Projeto Pedagógico
	 * @param array $parametros
	 * @return array
	 */
	public function retornar(array $parametros){
		
		try {
			$toProj = new VwProjetoPedagogicoTO();
			$toProj->montaToDinamico($parametros);
			$orm = new VwProjetoPedagogicoORM();

			$campos = array(
					 'id_projetopedagogico'
					,'st_projetopedagogico'
					,'st_tituloexibicao'
					,'nu_cargahoraria'
					,'st_perfilprojeto'
					,'st_coordenacao'
					,'st_horario'
					,'st_publicoalvo'
					,'st_mercadotrabalho'
// 					,'nu_taxamatricula'
					);
			
			$dados = $orm->consultaCampos($toProj, $campos, false, true, array('st_projetopedagogico'));
			
			
			$toPlano = new VwProjetoPlanoMatrizTO();
			$toPlano->setId_projetopedagogico($toProj->getId_projetopedagogico());
			$orm = new VwProjetoPlanoMatrizORM();
			$dados['PlanosPagamento'] = $orm->consultaCampos($toPlano, null, false, false, array('nu_parcelas'));
			
			$toAreas = new VwProjetoAreaTO();
			$toAreas->setId_projetopedagogico($toProj->getId_projetopedagogico());
			$orm = new VwProjetoAreaORM();
			$dados['Areas'] = $orm->consultaCampos($toAreas, array(
					'id_projetopedagogico'
					,'id_areaconhecimento'
					,'st_areaconhecimento'
					,'st_tituloexibicao'), false, false, array('st_tituloexibicao'));
			
			$toDis = new VwModuloDisciplinaProjetoTrilhaTO();
			$toDis->setId_projetopedagogico($toProj->getId_projetopedagogico());
			$orm = new VwModuloDisciplinaProjetoTrilhaORM();
			$dados['Disciplinas'] = $orm->consultaCampos($toDis, array(
					'id_projetopedagogico'
					,'id_disciplina'
					,'st_disciplina'
					,'st_modulo'
					,'nu_cargahoraria'), false, false, array('st_disciplina'));			
			
			$this->mensageiro->setMensageiro($dados, Ead1_IMensageiro::SUCESSO);
		} catch (Exception $e) {
			$this->mensageiro->setMensageiro('Erro ao retornar os dados. Entre em contato com a EAD1.', Ead1_IMensageiro::ERRO);
		}

		return Ead1_BO::arrayUtfEncode((array)$this->mensageiro->toArrayAll());
	}
	
}
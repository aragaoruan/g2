<?php
/**
 * Classe de regras de negócio para Serviços relacionados a Entrega de documento
 * @author João Gabriel
 * @since 30/08/2012
 */
class EntregaDocumentoWS {
	
	/**
	 * @var Ead1_Mensageiro
	 */
	public $mensageiro;
	
	
	public function __construct(){
		$this->mensageiro = new Ead1_Mensageiro();
	}
	
	
	/**
	 * Método que cadastra entrega de documentos
	 */
	public function cadastrar(array $parametros){
		
		$entidadeTO = new EntidadeTO();
		$entidadeTO->setId_entidade($entidadeTO->getSessao()->id_entidade);
		$entidadeTO->fetch(true,true,true);
		
		$id = $entidadeTO->getId_entidade();
		
		$dao = new WebServiceDAO();
		$dao->beginTransaction();
		$pessoaWS = new PessoaWS();
		try{
			$parametros['php'] = true;
			$usuarioPessoaMensagem = $pessoaWS->retornar($parametros);
			if($usuarioPessoaMensagem->getTipo()!=Ead1_IMensageiro::SUCESSO){
				throw new Exception('Usuário não encontrado');
			}
			
			$usuarioPessoa = $usuarioPessoaMensagem->getMensagem();
			
			$secretariaDAO = new SecretariaDAO();
			
			foreach ($parametros['documentos'] as $id_documento) {
				$entregaDocumentoTO = new EntregaDocumentosTO();
				$entregaDocumentoTO->setId_documentos($id_documento);
				$entregaDocumentoTO->setId_usuariocadastro($usuarioPessoa['usuario']->getid_usuario());
				$entregaDocumentoTO->setId_usuario($usuarioPessoa['usuario']->getid_usuario());
				$entregaDocumentoTO->setId_entidade($id);
				$entregaDocumentoTO->setId_situacao(EntregaDocumentosTO::SITUACAO_ENTREGUE_CONFIRMADO);
				
				$entregaDocumentoTO = $secretariaDAO->cadastrarEntregaDocumento($entregaDocumentoTO)->getFirstMensagem();
				$arrEntregaDocumentosTO[] = $entregaDocumentoTO->fetch(true,true,true);
			}
			
			$this->mensageiro->setMensageiro($arrEntregaDocumentosTO, Ead1_IMensageiro::SUCESSO);
			$dao->commit();
		}catch(Exception $e){
			$this->mensageiro->setMensageiro('Erro ao fazer entrega de documento(s)! '.$e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
			$dao->rollBack();
		}
		$retorno = (array)$this->mensageiro->toArrayAll();
		return Ead1_BO::arrayUtfEncode($retorno);
	}
	
}
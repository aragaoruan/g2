<?php

/**
 * Class MatriculaWS
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-06-05
 */
class MatriculaWS
{
    /**
     * @var Ead1_Mensageiro
     */
    private $mensageiro;
    /**
     * @var G2\Negocio\Matricula
     */
    private $negocio;

    public function __construct()
    {
        $this->mensageiro = new Ead1_Mensageiro();
        $this->negocio = new G2\Negocio\Matricula();
    }


    /**
     * Método para serviço de validar matricula
     * @param integer $id
     * @param array $parametros
     * @return array Ead1_Mensageiro
     */
    public function validamatricula($id, array $parametros)
    {
        try {
            if (isset($parametros['id_usuario']) && isset($parametros['id_produto']) && isset($parametros['id_entidade'])
                && !empty($parametros['id_usuario']) && !empty($parametros['id_produto']) && !empty($parametros['id_entidade'])
            ) {
                $result = $this->negocio->retornaMatriculaValida($parametros);
                if ($result) {
                    $this->mensageiro->setMensageiro($result, Ead1_IMensageiro::SUCESSO);
                } else {
                    $this->mensageiro->setMensageiro("Usuário não tem matrícula nesse produto.", Ead1_IMensageiro::AVISO);
                }
            } else {
                $this->mensageiro->setMensageiro("Verifique os parametros para consulta.", Ead1_IMensageiro::AVISO);
            }
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, 'Exception');
        }
        return (array)$this->mensageiro->toArrayAll();
    }

    /**
     * Serviço para retornar dados de matriculas
     * @param array $parametros
     * @return array
     */
    public function retornarmatricula(array $parametros)
    {
        try {
            $arrParams = array();
            foreach ($parametros as $key => $value) {
                if ($value)
                    $arrParams[$key] = $value;
            }
            $result = $this->negocio->consultaVwMatricula($arrParams);
            if ($result) {
                $arrResult = array();
                foreach ($result as $row) {
                    $arrResult[] = $this->negocio->toArrayEntity($row);
                }
                $this->mensageiro->setMensageiro($arrResult, Ead1_IMensageiro::SUCESSO);
            } else {
                $this->mensageiro->setMensageiro("Sem resultado para exibição.", Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO);
            }


        } catch (Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao consultar dados. " . $e->getMessage(), Ead1_IMensageiro::ERRO, 'Exception');
        }
        return (array)$this->mensageiro->toArrayAll();
    }

    /**
     * Serviço para retorno de matrícula de usuário para produtos de assinatura
     * @param array $parametros
     * @return array
     */
    public function retornastatusmatriculaassinante(array $parametros)
    {
        try {
            if (!isset($parametros['id_matricula'])) {
                throw new Exception("Id da matrícula é um parâmetro obrigatório.");
            }
//            if (!isset($parametros['id_usuario'])) {
//                throw new Exception("Id do usuário é um parâmetro obrigatório.");
//            }
            $result = $this->negocio->retornaStatusMatriculaAssinante($parametros);
            if ($result) {
                $this->mensageiro->setMensageiro($result, Ead1_IMensageiro::SUCESSO);
            } else {
                $this->mensageiro->setMensageiro("Matrícula não encontrada.", Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO);
            }
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao consultar matricula do assinate. " . $e->getMessage(), Ead1_IMensageiro::ERRO, 'Exception');
        }
        return (array)$this->mensageiro->toArrayAll();
    }
} 
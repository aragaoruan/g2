<?php

/**
 * Class GradeHorariaWS
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2015-02-20
 */
class GradeHorariaWS
{
    private $negocio;
    private $mensageiro;

    public function __construct()
    {
        $this->negocio = new \G2\Negocio\GradeHoraria();
        $this->mensageiro = new Ead1_Mensageiro();
    }

    /**
     * Método para serviço de grade horaria pela turma
     * @param array $parametros
     * @return array
     */
    public function gradeturma(array $parametros)
    {
        try {
            if (!array_key_exists('id_turma', $parametros) && empty($parametros['id_turma'])) {
                throw new Exception("id_turma não informado.");
            }

            $result = $this->negocio->retornarDadosWSGradeHoraria($parametros);
            if ($result) {
                $this->mensageiro->setMensageiro($result, Ead1_IMensageiro::SUCESSO);
            } else {
                $this->mensageiro->setMensageiro('Sem resultados para a turma informada', Ead1_IMensageiro::AVISO);
            }
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, 'Exception');
        }
        return (array)$this->mensageiro->toArrayAll();
    }
}
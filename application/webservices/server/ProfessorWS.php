<?php

/**
 * Classe de regras de negócio para Serviços relacionados a Professor
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-04-02
 */
class ProfessorWS
{

    public $mensageiro;

    public function __construct ()
    {
        $this->mensageiro = new Ead1_Mensageiro();
    }

    /**
     * Método para listar Professores
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     * @param integer $id Id Entidade
     * @param array $parametros array de parametros para pesquisar
     */
    public function listar ($id, array $parametros)
    {
        try {
            if (!isset($parametros['id_entidade'])) { // Verifica se não veio o id_entidade como parametro
                throw new Exception("Parametro id_entidade não localizado."); //Cria um exception
            } else {
                //Verifica se o id_entidade é diferente do id passado para autenticação no WS
                if ($parametros['id_entidade'] != $id) {
                    throw new Exception("Acesso inválido."); // cria um exception
                }
                $disciplinaNegocio = new G2\Negocio\Disciplina(); // Instancia a Negocio de Disciplina
                //Find Coordenadores de Disciplina
                $professores = $disciplinaNegocio->retornarDadosCoordenadorDisciplina(array(
                    'id_perfilpedagogico' => 1,
                    'id_situacaoperfil' => 15,
                    'id_entidade' => $parametros['id_entidade']
                ));

                if ($professores) {//Check Resultado
                    $arrRetorno = array(); // Cria variavel de array
                    foreach ($professores as $professor) {
                        $arrRetorno[] = array(
                            'id_usuario' => $professor['id_usuario'],
                            'st_nomecompleto' => $professor['st_nomecompleto']
                        );
                    }
                    $this->mensageiro->setMensageiro($arrRetorno, Ead1_IMensageiro::SUCESSO);
                } else {
                    $this->mensageiro->setMensageiro(Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO);
                }
            }
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao retornar os dados. Entre em contato com a EAD1. ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return (array) $this->mensageiro->toArrayAll();
    }

}

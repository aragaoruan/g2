<?php

/**
 * Classe para serviço de Combo
 * @author Kayo Silva <kayo.silva@unyleya.com.br>@abstract
 * @since 2013-07-02
 */
class ComboWS
{

    /**
     * @var Ead1_Mensageiro
     */
    public $mensageiro;
    private $negocio;

    public function __construct ()
    {
        $this->mensageiro = new Ead1_Mensageiro();
        $this->negocio = new \G2\Negocio\Produto();
    }

    /**
     * Metodo para serviço de Listar Combo
     * @param integer $id
     * @param array $parametros
     * @return Ead1_Mensageiro
     * @throws Exception
     */
    public function listar ($id, array $parametros)
    {
        try {
            //Verifica se o id_produto veio
            if (!isset($parametros['id_produto'])) {
                throw new Exception("id_produto deve ser passado.");
            }
            $arrReturn = array();
            //Procura o Produto Combo
            $resultCombo = $this->negocio->findProdutoComboByArray(array('id_produto' => $parametros['id_produto']), false);

            if ($resultCombo->getType() == 'success') {//Verifica se achou
                //Percorre os registros
                foreach ($resultCombo->getMensagem() as $i => $produto) {
                    $idProduto = $produto->getId_produto()->getId_produto();
                    //Set os valores
                    if (!isset($arrReturn[$idProduto])) {
                        $arrReturn[$idProduto]['id_produto'] = $idProduto;
                        $resultItemCombo = $this->negocio->findVwProdutoComboByArray(array('id_combo' => $idProduto));
                        $nuValorLiquido = 0;
                        $nuValorBruto = 0;
                        if ($resultItemCombo->getType() == 'success') {
                            foreach ($resultItemCombo->getMensagem() as $itemCombo) {
                                $arrReturn[$idProduto]['produtoitem'][] = array(
                                    $itemCombo['id_produto'],
                                    $itemCombo['st_produto'],
                                );
                                $nuValorBruto += (float) $itemCombo['nu_valor']; //valor deve ser calculado
                                $nuValorLiquido = (float) $itemCombo['nu_valorcombo']; //Valor já vem calculado da vw
                            }
                        }
                        $arrReturn[$idProduto]['nu_valorliquido'] = $nuValorLiquido;
                        $arrReturn[$idProduto]['nu_valorbruto'] = $nuValorBruto;
                    }
                }

                $this->mensageiro->setMensageiro($arrReturn, Ead1_IMensageiro::SUCESSO);
            } else {
                $this->mensageiro->setMensageiro($resultCombo->getMensagem(), Ead1_IMensageiro::AVISO);
            }
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao retornar os dados. Entre em contato com a EAD1. ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return (array) $this->mensageiro->toArrayAll();
    }

}

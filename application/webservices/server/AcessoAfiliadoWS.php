<?php
/**
 * Classe de regras de negócio para Serviços relacionados a Acesso dos Afiliados
 * @author Elcio Mauro Guimarães
 * @since 28/06/2012
 */
class AcessoAfiliadoWS {
	
	/**
	 * @var Ead1_Mensageiro
	 */
	public $mensageiro;
	
	
	public function __construct(){
		$this->mensageiro = new Ead1_Mensageiro();
	}
	
	/**
	 * Método que cadastra uma pré-venda
	 * @param int $id_contratoafiliado
	 * @return array
	 */
	public function salvar($id_contratoafiliado){
		
		$to = new AcessoAfiliadoTO();
		$to->setId_contratoafiliado($id_contratoafiliado);
		
		$bo = new AcessoAfiliadoBO();
		$this->mensageiro = $bo->cadastrarAcessoAfiliado($to);

		$retorno = (array)$this->mensageiro->toArrayAll();
		return Ead1_BO::arrayUtfEncode($retorno);
	}
	
}
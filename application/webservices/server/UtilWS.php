<?php
/**
 * Classe de regras de negócio para Serviços relacionados a Util
 * @author Elcio Mauro Guimarães
 * @since 30/07/2012
 */
class UtilWS {
	
	/**
	 * @var Ead1_Mensageiro
	 */
	public $mensageiro;
	
	
	public function __construct(){
		$this->mensageiro = new Ead1_Mensageiro();
	}

	/**
	 * Método para Listar as Unidades Federativas
	 * @param array $parametros
	 * @return array
	 */
	public function listaruf(array $parametros = null){
		
			try {
				$to = new UfTO();
				$to->montaToDinamico($parametros);
				$orm = new UfORM();
				$dados = $orm->consulta($to, false, false, array('st_uf'));
				$this->mensageiro->setMensageiro($dados, Ead1_IMensageiro::SUCESSO);
			} catch (Exception $e) {
				$this->mensageiro->setMensageiro('Erro ao retornar as UFs.', Ead1_IMensageiro::ERRO);
			}

		return Ead1_BO::arrayUtfEncode((array)$this->mensageiro->toArrayAll());
	}

	/**
	 * Método para Listar os Municípios
	 * @param array $parametros
	 * @return array
	 */
	public function listarmunicipio(array $parametros = null){
		
			try {
				$to = new MunicipioTO();
				$to->montaToDinamico($parametros);
				$orm = new MunicipioORM();
				$dados = $orm->consulta($to, false, false, array('st_nomemunicipio'));
				$this->mensageiro->setMensageiro($dados, Ead1_IMensageiro::SUCESSO);
			} catch (Exception $e) {
				$this->mensageiro->setMensageiro('Erro ao retornar os Municípios.', Ead1_IMensageiro::ERRO);
			}

		return Ead1_BO::arrayUtfEncode((array)$this->mensageiro->toArrayAll());
	}
	
	
	/**
	 * Método para Listar os Municípios
	 * @param array $parametros
	 * @return array
	 */
	public function listarnivelensino(array $parametros = null){
	
		try {
			$to = new NivelEnsinoTO();
			$to->montaToDinamico($parametros);
			$orm = new NivelEnsinoORM();
			$dados = $orm->consulta($to, false, false, array('st_nivelensino'));
			$this->mensageiro->setMensageiro($dados, Ead1_IMensageiro::SUCESSO);
		} catch (Exception $e) {
			$this->mensageiro->setMensageiro('Erro ao retornar os Níveis de Ensino.', Ead1_IMensageiro::ERRO);
		}
	
		return Ead1_BO::arrayUtfEncode((array)$this->mensageiro->toArrayAll());
	}
	
	
	/**
	 * Método para Listar os Municípios
	 * @param array $parametros
	 * @return array
	 */
	public function listarunidades(array $parametros = null){
	
		try {
			
			if(empty($parametros['id_entidadematriz'])){
				throw new Exception('A entidade Matriz é obrigatória!');
			}
			
			$to = new VwUnidadesTO();
			$to->montaToDinamico($parametros);
			$orm = new VwUnidadesORM();
			$dados = $orm->consulta($to, false, false, array('st_nomeentidade'));
			$this->mensageiro->setMensageiro($dados, Ead1_IMensageiro::SUCESSO);
		} catch (Exception $e) {
			$this->mensageiro->setMensageiro('Erro ao retornar as Unidades.', Ead1_IMensageiro::ERRO);
		}
	
		return Ead1_BO::arrayUtfEncode((array)$this->mensageiro->toArrayAll());
	}
	
	
}
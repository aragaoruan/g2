<?php

/**
 * Classe para Serviços relacionados a Concursos
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-01-20
 */
use G2\Negocio\Concurso;

class ConcursosWS
{

    /**
     * @var Ead1_Mensageiro
     */
    public $mensageiro;

    /**
     * @var G2\Negocio\Concurso 
     */
    private $_negocio;

    public function __construct ()
    {
        $this->_negocio = new Concurso();
        $this->mensageiro = new Ead1_Mensageiro();
    }

    /**
     * Metodo para servico de salvar Concurso
     * @param array $parametros
     * @return Ead_Mensageiro
     */
    public function salvar ($id, array $parametros)
    {
        try {
            if ($parametros) {
                $parametros['id_entidade'] = isset($parametros['id_entidade']) ? $parametros['id_entidade'] : $id;
                $data = $parametros;
                unset($data['ufs'], $data['nivelensino'], $data['cargo']);
                $result = $this->_negocio->salvarConcurso($data);
                if ($result->getType() == 'success') {
                    foreach ($result->getMensagem() as $row) {
                        if (isset($parametros['ufs'])) {
                            $resultUfs = $this->salvaArrayUf($parametros['ufs'], $row->getId_concurso());
                        }
                        if (isset($parametros['nivelensino'])) {
                            $resultUfs = $this->salvaArrayNivelEnsino($parametros['nivelensino'], $row->getId_concurso());
                        }
                        if (isset($parametros['cargo'])) {
                            $resultCargo = $this->salvaArrayCargo($parametros['cargo'], $row->getId_concurso());
                        }
                    }
                    $this->mensageiro->setMensageiro("Registro salvo com sucesso!", Ead1_IMensageiro::SUCESSO);
                } else {
                    $this->mensageiro->setMensageiro($result, Ead1_IMensageiro::ERRO);
                }
            }
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro - ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return (array) $this->mensageiro->toArrayAll();
    }

    /**
     * 
     * @param array $cargos
     * @param integer $id_concurso
     * @return Ead1_Mensageiro
     */
    public function salvaArrayCargo (array $cargos, $id_concurso)
    {
        $data = array();
        $arrResult = array();
        try {
            if ($cargos) {
                foreach ($cargos as $cargo) {
                    $data = array(
                        'id_concurso' => $id_concurso,
                        'st_cargoconcurso' => $cargo['st_cargoconcurso'],
                        'nu_salario' => $cargo['nu_salariocargo']
                    );
                    $arrResult[] = $this->_negocio->salvaCargoConcurso($data);
                }
            }
            $this->mensageiro->setMensageiro($arrResult, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro - ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro;
    }

    /**
     * Salva array de ufs vinculados ao concurso
     * @param array $ufs
     * @param integer $id_concurso
     * @return Ead1_Mensageiro
     */
    private function salvaArrayUf (array $ufs, $id_concurso)
    {
        $data = array();
        $arrResult = array();
        try {
            if ($ufs) {
                foreach ($ufs as $uf) {
                    $data = array(
                        'sg_uf' => $uf,
                        'id_concurso' => $id_concurso
                    );

                    $result = $this->_negocio->salvaConcursoUf($data);
                    if ($result->getType() == 'success') {
                        $arrResult[] = $result->getMensagem();
                    }
                }
            }
            $this->mensageiro->setMensageiro($arrResult, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro - ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro;
    }

    /**
     * Salva array de nivel de ensino vinculado a concurso
     * @param array $niveisEnsino
     * @param integer $id_concurso
     * @return Ead1_Mensageiro
     */
    private function salvaArrayNivelEnsino (array $niveisEnsino, $id_concurso)
    {
        $data = array();
        $arrResult = array();
        try {
            if ($niveisEnsino) {
                foreach ($niveisEnsino as $nivel) {
                    $data = array(
                        'id_nivelensino' => $nivel,
                        'id_concurso' => $id_concurso
                    );
                    $result = $this->_negocio->salvaConcursoNivelEnsino($data);
                    if ($result->getType() == 'success') {
                        $arrResult[] = $result->getMensagem();
                    }
                }
            }
            $this->mensageiro->setMensageiro($arrResult, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro - ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro;
    }

    /**
     * Retorna lista de concurso
     * @param integer $id Id utilizado para autenticação no WS
     * @param array $parametros Parametros para ser consultado
     * @return Ead1_Mensageiro
     */
    public function listar ($id, array $parametros)
    {
        try {
            //Força o id_da entidade ser passado
            $parametros['id_entidade'] = isset($parametros['id_entidade']) ? $parametros['id_entidade'] : $id;
            //Retorna os dados de Concurso
            $result = $this->_negocio->retornaConcursoByParams($parametros);
            if ($result) { // Verifica se retornou resultado
                $arrResult = array();
                //Percorre os registros
                foreach ($result as $i => $row) {
                    //Atribui os dados de concurso ao array
                    $arrResult[$i] = array(
                        'id_concurso' => $row->getId_concurso(),
                        'st_concurso' => $row->getSt_concurso(),
                        'st_orgao' => $row->getSt_orgao(),
                        'dt_prova' => $row->getDt_prova()->format("d/m/Y"),
                        'dt_inicioinscricao' => $row->getDt_inicioinscricao()->format("d/m/Y"),
                        'dt_fiminscricao' => $row->getDt_fiminscricao()->format("d/m/Y"),
                        'nu_vagas' => $row->getNu_vagas(),
                        'st_banca' => $row->getSt_banca()
                    );
                    //Monta o array de cargos
                    $arrResult[$i]['cargos'] = array();
                    if ($row->getCargos()) {
                        foreach ($row->getCargos() as $rowCargo) {
                            $arrResult[$i]['cargos'][] = array(
                                'id_cargoconcurso' => $rowCargo->getId_cargoconcurso(),
                                'st_cargoconcurso' => $rowCargo->getSt_cargoconcurso(),
                                'nu_salariocargo' => $rowCargo->getNu_salario()
                            );
                        }
                    }
                    //Monta o array de ufs
                    $arrResult[$i]['ufs'] = array();
                    if ($row->getUfs()) {
                        foreach ($row->getUfs() as $uf) {
                            $arrResult[$i]['ufs'][] = array(
                                'sg_uf' => $uf->getSg_uf()->getSg_uf(),
                                'st_uf' => $uf->getSg_uf()->getSt_uf()
                            );
                        }
                    }
                    //Monta o array de nível de ensino
                    $arrResult[$i]['nivelensino'] = array();
                    if ($row->getNiveisEnsino()) {
                        foreach ($row->getNiveisEnsino() as $nivelEnsino) {
                            $arrResult[$i]['nivelensino'][] = array(
                                'id_nivelensino' => $nivelEnsino->getId_nivelensino()->getId_nivelensino(),
                                'st_nivelensino' => $nivelEnsino->getId_nivelensino()->getSt_nivelensino()
                            );
                        }
                    }
                }
                $this->mensageiro->setMensageiro($arrResult, Ead1_IMensageiro::SUCESSO);//Atribui no mensageiro
            } else {
                $this->mensageiro->setMensageiro('Sem resultado para exibição.', Ead1_IMensageiro::AVISO);
            }
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro - ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return (array) $this->mensageiro->toArrayAll();
    }

}

<?php

/**
 * Classe para Serviços relacionados a Tags
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-01-16
 */
use \G2\Negocio\Tag;

class TagsWS
{

    /**
     * @var Ead1_Mensageiro
     */
    public $mensageiro;
    private $_negocio;

    public function __construct ()
    {
        $this->_negocio = new Tag();
        $this->mensageiro = new Ead1_Mensageiro();
    }

    /**
     * Metodo para servico de salvar Tags
     * @param array $parametros
     * @return Ead_Mensageiro
     */
    public function salvar ($id, array $parametros)
    {
        try {
            if ($parametros) {
                $result = $this->_negocio->salvarTag($parametros);
                if ($result->getType() == 'success') {
                    foreach ($result->getMensagem() as $row) {
                        $this->mensageiro->setMensageiro($this->_negocio->toArrayEntity($row), Ead1_IMensageiro::SUCESSO);
                    }
                } else {
                    $this->mensageiro->setMensageiro($result, Ead1_IMensageiro::ERRO);
                }
            }
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro - ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return (array) $this->mensageiro->toArrayAll();
    }

    /**
     * Serviço para retorno de tag
     * @param array $parametros array de parametros para critério da busca
     * @return Ead1_Mensageiro
     * @throws Zend_Exception
     */
    public function listar (array $parametros)
    {
        try {
            //Verifica se o parametro obrigatório veio...
            if (!isset($parametros['id_tag'])) {
                throw new Zend_Exception("id_tag é esperado como parâmetro.");
            } else {
                $parametros['id_tag'] = intval($parametros['id_tag']);
                if (is_int($parametros['id_tag']) && $parametros['id_tag'] != 0) {
                    $result = $this->_negocio->findTagBy(array('id_tag' => $parametros['id_tag']));
                    $arrReturn = array();
                    if ($result) {
                        foreach ($result as $rowTag) {
                            $arrReturn[] = array(
                                'id_tag' => $rowTag->getId_tag(),
                                'st_tag' => $rowTag->getSt_tag(),
                                'dt_cadastro' => new DateTime($rowTag->getDt_cadastro())
                            );
                        }
                        $this->mensageiro->setMensageiro($arrReturn, Ead1_IMensageiro::SUCESSO);
                    } else {
                        $this->mensageiro->setMensageiro('Sem resultado para exibição.', Ead1_IMensageiro::AVISO);
                    }
                } else {
                    throw new Zend_Exception("id_tag deve ser do tipo interger.");
                }
            }
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro - ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return (array) $this->mensageiro->toArrayAll();
    }

}

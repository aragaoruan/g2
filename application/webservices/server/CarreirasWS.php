<?php

/**
 * Classe para Serviços de Carreiras
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2013-01-17
 */
class CarreirasWS
{

    /**
     * @var Ead1_Mensageiro
     */
    public $mensageiro;
    private $_negocio;

    public function __construct ()
    {
        $this->_negocio = new G2\Negocio\Carreira();
        $this->mensageiro = new Ead1_Mensageiro();
    }

    /**
     * Metodo para servico de salvar Carreira
     * @param array $parametros
     * @return Ead_Mensageiro
     */
    public function salvar (array $parametros)
    {
        try {
            if ($parametros) {
                $result = $this->_negocio->salvarCarreira($parametros);
                if ($result->getType() == 'success') {
                    foreach ($result->getMensagem() as $row) {
                        $this->mensageiro->setMensageiro($this->_negocio->toArrayEntity($row), Ead1_IMensageiro::SUCESSO);
                    }
                } else {
                    $this->mensageiro->setMensageiro($result, Ead1_IMensageiro::ERRO);
                }
            }
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro - ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return (array) $this->mensageiro->toArrayAll();
    }

    /**
     * Método para serviço de listar Carreiras
     * @param array $parametros
     * @return Ead1_Mensageiro
     * @throws Zend_Exception
     */
    public function retornar (array $parametros)
    {
        try {
            //Verifica se o parametro obrigatório veio...
            if (!isset($parametros['id_carreira'])) {
                throw new Zend_Exception("id_carreira é esperado como parâmetro.");
            } else {
                $parametros['id_carreira'] = intval($parametros['id_carreira']);
                if (is_int($parametros['id_carreira']) && $parametros['id_carreira'] != 0) {
                    $result = $this->_negocio->findByCarreira($parametros);
                    $arrReturn = array();
                    if ($result) {
                        //Percorre o resultado do findBy 
                        //e atribui no array somente os valores que devem ser retornados no ws
                        foreach ($result as $row) {
                            $arrReturn[] = array(
                                'id_carreira' => $row->getId_carreira(),
                                'st_carreira' => $row->getSt_carreira()
                            );
                        }
                        //Adiciona o retorno no mensageiro
                        $this->mensageiro->setMensageiro($arrReturn, Ead1_IMensageiro::SUCESSO);
                    } else { // se não tiver resultado mostra mensagem de sem resultados
                        $this->mensageiro->setMensageiro('Sem resultado para exibição.', Ead1_IMensageiro::AVISO);
                    }
                } else {
                    $this->mensageiro->setMensageiro('id_carreira deve ser um valor do tipo inteiro.', Ead1_IMensageiro::AVISO);
                }
            }
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro - ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return (array) $this->mensageiro->toArrayAll();
    }

}

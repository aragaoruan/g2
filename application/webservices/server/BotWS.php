<?php

class BotWS
{
    /**
     * @var Ead1_Mensageiro
     */
    public $mensageiro;


    public function __construct()
    {
        $this->mensageiro = new Ead1_Mensageiro();
        $this->negocio = new \G2\Negocio\Negocio();
        $this->linhadenegocio = \G2\Constante\ItemConfiguracao::LINHA_DE_NEGOCIO;
    }

    public function gerarBoleto(){
        try {
            $negocio = new \G2\Negocio\Matricula();

            $params = $_REQUEST['params'];
            $nome = (string)$params->nome_usuario;
            $entidade = (string)$params->id_entidade;
            $portal = (string)$params->tipo_portal;
            $matricula = (string)$params->matricula;
            $quantidadeBoletos = (string)$params->quantidadeboletos;
            $url = Ead1_Ambiente::geral()->st_url_loja;

            if (!empty($quantidadeBoletos)) {
                switch ($quantidadeBoletos) {
                    case 'A Vencer':
                        $situacao = ['Pendente'];
                        break;
                    case 'Vencidos':
                        $situacao = ['Vencido'];
                        break;
                    case 'Todos os Boletos':
                        $situacao = ['Pendente', 'Vencido'];
                        break;
                    default:
                        throw new Exception('Informe o tipo de boleto corretamente');
                }
            }

            $resultado = array();

            if (!empty($portal)) {
                if(!empty($entidade)){
                    $query = $negocio->findOneBy('\G2\Entity\Entidade', array('id_entidade'=> $entidade));
                } else {
                    throw new Exception("Informe o id da entidade.");
                }

                // é importante configurar a url na tb_entidade do novo portal e portal antigo
                if ($portal == 'portal') {
                    $resultado['link_financeiro'] = $query->getSt_urlportal()."/portal/financeiro";
                } else {
                    if ($portal == 'novo_portal') {
                        $resultado['link_financeiro'] = $query->getSt_urlnovoportal()."/$matricula/financeiro";
                    } else {
                        throw new Exception("Informe o portal de acesso do aluno.");
                    }
                }
            } else {
                throw new Exception("Informe o portal de acesso do aluno.");
            }

            $id_venda = $this->descobreVendasMatricula($matricula);

            if (empty($id_venda)){
                throw new Exception("Não foi possível recuperar as vendas da matrícula.");
            }

            $query = array();
            foreach ($id_venda as $key){
                $resumo = $negocio->findVwResumoFinanceiro(array(
                    'id_venda' => $key['id_venda']
                ));

                if ($resumo->getTipo() == \Ead1_IMensageiro::ERRO) {
                    throw new Exception("Houve um erro ao recuperar os dados do servidor.");
                }
                $query[] = $resumo->getMensagem();
            }

            $i = 0;
            foreach($query as $index){
                foreach ($index as $key) {
                    if (
                        in_array($key['st_situacao'], $situacao)
                        && $key['id_meiopagamento'] == 2) {
                        $id_lancamento = $key['id_lancamento'];
                        ++$i;
                        $id_venda = $key['id_venda'];
                        $key['st_situacao'] = $key['st_situacao'] == 'Pendente' ? 'A Vencer' : $key['st_situacao'];

                        $resultado["resultado_$i"]['link'] = "$url/pagamento/boleto?id_lancamento=$id_lancamento&verificaretorno=true";
                        $resultado["resultado_$i"]['parcela'] = $key['nu_parcela'];
                        $resultado["resultado_$i"]['texto'] = "Boleto Nº " . $key['nu_parcela'] . " - " . $key['dt_vencimento'] . " - " . $key['st_situacao'];
                    }
                }
            }

            $resultado['mensagem_retorno'] = "identifiquei que você possui $i boleto(s) conosco. Selecione o(s) boleto(s) desejado(s) para pagamento.";

            if ($i == 0) {
                $resultado['mensagem_retorno'] = "não foram encontrados boletos na situação informada.";
            }

            $this->mensageiro->setMensageiro($resultado, Ead1_IMensageiro::SUCESSO);
            return (array)$this->mensageiro->toArrayAll();
        }catch (Exception $e){
            throw $e;
        }
    }

    private function descobreVendasMatricula($id_matricula){
        if (empty($id_matricula)){
            throw new Exception("Informe o id da matrícula.");
        }

        $stmt = $this->negocio->getem()->getConnection()->prepare(
            "
                      SELECT vp.id_venda
                      FROM tb_vendaproduto AS vp
                        JOIN tb_produtoprojetopedagogico AS ppd ON ppd.id_produto = vp.id_produto
                        JOIN tb_venda AS v ON v.id_venda = vp.id_venda AND v.bl_ativo = 1 AND v.id_evolucao = 10
                      WHERE vp.id_matricula = $id_matricula
                      ORDER BY v.id_venda DESC"
        );
        $stmt->execute();
        $linha = $stmt->fetchAll();
        return $linha;
    }
}

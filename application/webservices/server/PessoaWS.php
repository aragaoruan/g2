<?php

/**
 * Classe de regras de negócio para Serviços relacionados a Pessoa
 * @author Elcio Mauro Guimarães
 * @since 28/06/2012
 */
class PessoaWS
{
    /**
     * Retornar uma Pessoa
     * @param array $parametros
     * @return array|Ead1_Mensageiro
     * @throws Exception
     * @throws Zend_Exception
     */
    public function retornar(array $parametros)
    {

        $negocio = new \G2\Negocio\Pessoa();
        $pessoa = false;
        if (empty($parametros['st_cpf']) && empty($parametros['id_usuario'])) {
            $mensageiro = new Ead1_Mensageiro();
            $mensageiro->setMensageiro('O CPF ou o id_usuario é obrigatório!', Ead1_IMensageiro::ERRO);
            return $mensageiro->toArrayAll();
        } else {


            $usuarioTO = new UsuarioTO();

            if (isset($parametros['st_cpf']) && $parametros['st_cpf']) {
                $usuarioTO->setSt_cpf($parametros['st_cpf']);
            }


            if (!empty($parametros['id_usuario'])) {
                $usuarioTO->setId_usuario($parametros['id_usuario']);
            }


            $v = $usuarioTO->toArray();


            $bo = new PessoaBO();

            if ($usuarioTO->getId_usuario()) {
                $usuarioEN = $negocio->find('\G2\Entity\Usuario', (int)$usuarioTO->getId_usuario());
            } else {
                $usuarioEN = $negocio->findOneBy('\G2\Entity\Usuario',
                    array('st_cpf' => trim($usuarioTO->getSt_cpf())));
            }


            if (!$usuarioEN instanceOf \G2\Entity\Usuario || $usuarioEN->getSt_nomecompleto() == false) {
                $mensageiro = new Ead1_Mensageiro("Nenhum Usuário Encontrado!" . serialize($v),
                    Ead1_IMensageiro::AVISO);
            } else {
                //Se o usuário é encontrado, ele encapsula o mesmo no TO para o retorno
                $usuarioTO = new UsuarioTO($negocio->toArrayEntity($usuarioEN));
                $usuarioTO->setId_registropessoa($usuarioEN->getId_registropessoa() instanceof \G2\Entity\RegistroPessoa ? $usuarioEN->getId_registropessoa()->getId_registropessoa() : null);
                $usuarioTO->setId_usuariopai($usuarioEN->getId_usuariopai() instanceof \G2\Entity\Usuario ? $usuarioEN->getId_usuariopai()->getId_usuario() : null);


                $where = array(
                    'id_usuario' => (int)$usuarioEN->getId_usuario()
                ,
                    'id_entidade' => (isset($parametros['id_entidade']) && $parametros['id_entidade']) ? (int)$parametros['id_entidade'] : (int)$usuarioTO->getSessao()->id_entidade
                );

                $vwPessoaTO = $negocio->findOneBy('\G2\Entity\VwPessoa', $where);

                if (!$vwPessoaTO instanceof \G2\Entity\VwPessoa) {
                    $vwPC = $negocio->findOneBy('\G2\Entity\VwPessoa',
                        array('id_usuario' => (int)$usuarioEN->getId_usuario()));
                    if ($vwPC instanceof \G2\Entity\VwPessoa && $vwPC->getId_entidade()) {
                        if ($bo->importarUsuarioEntidade($usuarioEN->getId_usuario(), $vwPC->getId_entidade(),
                            $where['id_entidade'], $usuarioEN->getId_usuario())) {
                            $vwPessoaTO = new VwPessoaTO($where);
                            $pessoa = $bo->retornarVwPessoa($vwPessoaTO);
                        };
                    }
                } else {
                    $vwPessoaTO = new VwPessoaTO($where);
                    $pessoa = $bo->retornarVwPessoa($vwPessoaTO);
                }

                if ($pessoa) {
                    $dados = $pessoa;
                    $pessoaTO = Ead1_TO_Dinamico::encapsularTo($dados, new VwPessoaTO(), true);

                    if (isset($parametros['id_entidadevendag2']) && $parametros['id_entidadevendag2']) {
                        $bo->importarUsuarioEntidade($usuarioEN->getId_usuario(), $pessoaTO->getId_entidade(),
                            $parametros['id_entidadevendag2'], $usuarioEN->getId_usuario());
                    }

                    $WsBO = new WebServiceBO();
                    $pessoaTO = $WsBO->trataVwPessoaParaWebService($pessoaTO);
                    $mensageiro = new Ead1_Mensageiro(array("usuario" => $usuarioTO, "pessoa" => $pessoaTO),
                        Ead1_IMensageiro::SUCESSO, "pessoa");
                } else {
                    $mensageiro = new Ead1_Mensageiro(array(
                        "usuario" => $usuarioTO,
                        "pessoa" => "Nenhuma Pessoa Encontrada!"
                    ), Ead1_IMensageiro::AVISO, "usuario");
                }
            }
        }

        if (empty($parametros['php'])) {
            $retorno = (array)$mensageiro->toArrayAll();
            return Ead1_BO::arrayUtfEncode($retorno);
        } else {
            return $mensageiro;
        }
    }

    /**
     * Método para cadastrar pessoa
     * @param $id
     * @param array|null $parametros
     * @return array|Ead1_Mensageiro
     * @throws Zend_Date_Exception
     * @throws Zend_Exception
     */
    public function cadastrar($id, array $parametros = null)
    {
        $id = (int)$id;

        $usuarioTO = new UsuarioTO();
        $usuarioTO->getSessao()->id_entidade = $id; //isset($parametros['id_entidade']) ? $parametros['id_entidade'] : $id;
        //garantindo que nenhum parametro tera espaço no inicio e no fim
        if ($parametros && is_array($parametros)) {
            $parametros = array_map(function ($n) {
                return trim($n);
            }, $parametros);
        }

        $parametros['st_cpf'] = (isset($parametros['st_cpf']) && $parametros['st_cpf']) ? $parametros['st_cpf'] : null;
        $parametros['st_email'] = (isset($parametros['st_email']) && $parametros['st_email']) ? $parametros['st_email'] : null;


        if (empty($parametros['st_cpf'])) {
            return new Ead1_Mensageiro('CPF é obrigatório!', Ead1_IMensageiro::ERRO);
        }

        if (empty($parametros['st_email'])) {
            return new Ead1_Mensageiro('E-mail é obrigatório!', Ead1_IMensageiro::ERRO);
        }

        if (!new Zend_Validate_EmailAddress($parametros['st_email'])) {
            return new Ead1_Mensageiro('E-mail inválido', Ead1_IMensageiro::ERRO);
        }
        if (empty($parametros['st_nomecompleto'])) {
            return new Ead1_Mensageiro('Nome é obrigatório!', Ead1_IMensageiro::ERRO);
        }
        if (empty($parametros['dt_nascimento'])) {
            return new Ead1_Mensageiro('Data de nascimento é obrigatória!', Ead1_IMensageiro::ERRO);
        }
        if (empty($parametros['st_estadocivil'])) {
            return new Ead1_Mensageiro('Estado civil é obrigatório!', Ead1_IMensageiro::ERRO);
        }
        $usuarioTO->setSt_cpf(isset($parametros['st_cpf']) ? $parametros['st_cpf'] : null);
        $usuarioTO->setSt_nomecompleto($parametros['st_nomecompleto']);
//        $usuarioTO->setSt_codassociado(isset($parametros['st_codassociado']) ? $parametros['st_codassociado'] : NULL);

        $dadosAcessoTO = null;

        if (isset($parametros['st_senha']) && !empty($parametros['st_senha'])) {
            $dadosAcessoTO = new DadosAcessoTO();
            $dadosAcessoTO->setSt_senha($parametros['st_senha']);
            $usuarioTO->setSt_senha($parametros['st_senha'], true);
        }

        //salva um registro para Diário do Associado
        $usuarioIntegracaoTO = new UsuarioIntegracaoTO();
        if (array_key_exists('st_codassociado', $parametros) && !empty($parametros['st_codassociado'])) {
            $usuarioIntegracaoTO->setId_sistema(SistemaTO::DIARIO_ASSOCIADOS); //18 //Código do Sistema fixo em 18 pois é para salvar o código para usuários do Diário do Associado
            $usuarioIntegracaoTO->setSt_codusuario($parametros['st_codassociado']);
        }

        $pessoaTO = new PessoaTO();
        $pessoaTO->setId_entidade((isset($parametros['id_entidade']) && $parametros['id_entidade']) ? $parametros['id_entidade'] : $id);
        $pessoaTO->setId_estadocivil($this->trataEstadoCivil($parametros['st_estadocivil']));
        $pessoaTO->setSt_cidade(((!empty($parametros['st_cidadenascimento']) ? $parametros['st_cidadenascimento'] : null)));
        $pessoaTO->setId_municipionascimento((!empty($parametros['id_municipionascimento']) ? $parametros['id_municipionascimento'] : 0));
        $pessoaTO->setId_municipio((!empty($parametros['id_municipionascimento']) ? $parametros['id_municipionascimento'] : 0));

        if (isset($parametros['sg_fatorrh'])) {
            $pessoaTO->setSg_fatorrh((!empty($parametros['sg_fatorrh']) ? $parametros['sg_fatorrh'] : null));
        }
        if (isset($parametros['id_tiposanguineo'])) {
            $pessoaTO->setId_tiposanguineo((!empty($parametros['id_tiposanguineo']) ? $parametros['id_tiposanguineo'] : null));
        }

        if ($pessoaTO->getId_municipionascimento()) {
            $mun = new MunicipioTO();
            $mun->setId_municipio($pessoaTO->getId_municipionascimento());
            $mun->fetch(true, true, true);
            $pessoaTO->setSg_ufnascimento($mun->getSg_uf());
            $pessoaTO->setSg_uf($mun->getSg_uf());
        }

        $pessoaTO->setSt_nomeexibicao($parametros['st_nomeexibicao']);
        $pessoaTO->setId_situacao(1); // 1
        $pessoaTO->setId_pais(PaisTO::BRASIL);
        $pessoaTO->setSt_sexo((!empty(trim($parametros['st_sexo'])) ? $parametros['st_sexo'] : null));

        if (isset($parametros['dt_nascimento']) && $parametros['dt_nascimento']) {
            $data = explode('/', $parametros['dt_nascimento']);
            $pessoaTO->setDt_nascimento(new Zend_Date(array(
                'year' => $data[2],
                'month' => $data[1],
                'day' => $data[0]
            )));
        }

        $pessoaTO->setSt_nomemae((!empty($parametros['st_nomemae']) ? $parametros['st_nomemae'] : null));
        $pessoaTO->setSt_nomepai((!empty($parametros['st_nomepai']) ? $parametros['st_nomepai'] : null));
        $pessoaTO->setBl_ativo(1);

        $contatoTelefoneTO = new ContatosTelefoneTO();
        $contatoTelefoneTO->setId_tipotelefone((!empty($parametros['st_tipotelefone']) ? $this->trataTipoTelefone($parametros['st_tipotelefone']) : 0));
        $contatoTelefoneTO->setNu_ddi((!empty($parametros['nu_ddi']) ? $parametros['nu_ddi'] : 0));
        $contatoTelefoneTO->setNu_ddd((!empty($parametros['nu_ddd']) ? $parametros['nu_ddd'] : 0));
        $contatoTelefoneTO->setNu_telefone((!empty($parametros['nu_telefone']) ? $parametros['nu_telefone'] : 0));

        $contatoTelefoneTO2 = new ContatosTelefoneTO();
        $contatoTelefoneTO2->setId_tipotelefone((!empty($parametros['st_tipotelefone2']) ? $this->trataTipoTelefone($parametros['st_tipotelefone2']) : 0));
        $contatoTelefoneTO2->setNu_ddi((!empty($parametros['nu_ddi2']) ? $parametros['nu_ddi2'] : 0));
        $contatoTelefoneTO2->setNu_ddd((!empty($parametros['nu_ddd2']) ? $parametros['nu_ddd2'] : 0));
        $contatoTelefoneTO2->setNu_telefone((!empty($parametros['nu_telefone2']) ? $parametros['nu_telefone2'] : 0));

        $contatoTelefoneTO3 = null;
        if (!empty($parametros['nu_telefonecomercial'])) {
            $contatoTelefoneTO3 = new ContatosTelefoneTO();
            $contatoTelefoneTO3->setId_tipotelefone((!empty($parametros['st_tipotelefonecomercial']) ? $this->trataTipoTelefone($parametros['st_tipotelefonecomercial']) : 0));
            $contatoTelefoneTO3->setNu_ddi((!empty($parametros['nu_ddicomercial']) ? $parametros['nu_ddicomercial'] : 0));
            $contatoTelefoneTO3->setNu_ddd((!empty($parametros['nu_dddcomercial']) ? $parametros['nu_dddcomercial'] : 0));
            $contatoTelefoneTO3->setNu_telefone((!empty($parametros['nu_telefonecomercial']) ? $parametros['nu_telefonecomercial'] : 0));
        }

        $documentoIdentidadeTO = new DocumentoIdentidadeTO();
        $documentoIdentidadeTO->setSt_rg(!empty($parametros['st_rg']) ? $parametros['st_rg'] : null);
        $documentoIdentidadeTO->setSt_orgaoexpeditor(!empty($parametros['st_orgaoexpeditor']) ? $parametros['st_orgaoexpeditor'] : null);
        $documentoIdentidadeTO->setDt_dataexpedicao(!empty($parametros['dt_dataexpedicao']) ? $parametros['dt_dataexpedicao'] : null);
        $documentoIdentidadeTO->setId_entidade(!empty($pessoaTO->getId_entidade()) ? $pessoaTO->getId_entidade() : $id);

        $contatoTelefonePessoaTO = new ContatosTelefonePessoaTO();
        $contatoTelefonePessoaTO->setBl_padrao(true);
        $contatoTelefonePessoaTO->setId_entidade($pessoaTO->getId_entidade());

        $contatosEmailTO = new ContatosEmailTO();
        $contatosEmailTO->setSt_email((!empty($parametros['st_email']) ? $parametros['st_email'] : null));


        $contatosEmailPessoaPerfilTO = new ContatosEmailPessoaPerfilTO();
        $contatosEmailPessoaPerfilTO->setBl_ativo(true);
        $contatosEmailPessoaPerfilTO->setBl_padrao(true);
        $contatosEmailPessoaPerfilTO->setId_entidade($pessoaTO->getId_entidade());

        //Dados Endereço Residencial
        $enderecoTO = new EnderecoTO();
        $enderecoTO->setId_municipio((!empty($parametros['id_municipio']) ? $parametros['id_municipio'] : 0));
        $enderecoTO->setSt_cidade((!empty($parametros['st_cidade']) ? $parametros['st_cidade'] : null));
        $enderecoTO->setId_pais((!empty($parametros['id_pais']) ? $parametros['id_pais'] : PaisTO::BRASIL));
        $enderecoTO->setId_tipoendereco(TipoEnderecoTO::RESIDENCIAL);
        $enderecoTO->setSg_uf((!empty($parametros['sg_uf']) ? $parametros['sg_uf'] : null));
        $enderecoTO->setSt_cep((!empty($parametros['st_cep']) ? $parametros['st_cep'] : null));
        $enderecoTO->setSt_endereco((!empty($parametros['st_endereco']) ? $parametros['st_endereco'] : null));
        $enderecoTO->setSt_bairro((!empty($parametros['st_bairro']) ? $parametros['st_bairro'] : null));
        /*
        * POG para salvar o complemento do endereço vazio, não adianta alterar o método que faz o update
        * passando a parametro para aceitar nulo como true, quando faz isso ele reclama de outras colunas vazias
        * que não deveriam ser
        */
        $enderecoTO->setSt_complemento((!empty($parametros['st_complemento']) ? $parametros['st_complemento'] : ""));
        $enderecoTO->setSt_estadoprovincia((!empty($parametros['st_estadoProvincia']) ? $parametros['st_estadoProvincia'] : null));
        $enderecoTO->setNu_numero((!empty($parametros['nu_numero']) ? $parametros['nu_numero'] : 0));

        $pessoaEnderecoTO = new PessoaEnderecoTO();
        $pessoaEnderecoTO->setBl_padrao(true);
        $pessoaEnderecoTO->setBl_padrao(true);
        $pessoaEnderecoTO->setId_entidade($pessoaTO->getId_entidade());

        //Dados Endereço Correspondência
        $enderecoCorresnpondeciaTO = new EnderecoTO();
        $enderecoCorresnpondeciaTO->setId_municipio((!empty($parametros['id_municipiocorrespondencia']) ? $parametros['id_municipiocorrespondencia'] : 0));
        $enderecoCorresnpondeciaTO->setSt_cidade((!empty($parametros['st_cidadecorrespondencia']) ? $parametros['st_cidadecorrespondencia'] : null));
        $enderecoCorresnpondeciaTO->setId_pais((!empty($parametros['id_paiscorrespondencia']) ? $parametros['id_paiscorrespondencia'] : PaisTO::BRASIL));
        $enderecoCorresnpondeciaTO->setId_tipoendereco(TipoEnderecoTO::CORRESPONDENCIA);
        $enderecoCorresnpondeciaTO->setSg_uf((!empty($parametros['sg_ufcorrespondencia']) ? $parametros['sg_ufcorrespondencia'] : null));
        $enderecoCorresnpondeciaTO->setSt_cep((!empty($parametros['st_cepcorrespondencia']) ? $parametros['st_cepcorrespondencia'] : null));
        $enderecoCorresnpondeciaTO->setSt_endereco((!empty($parametros['st_enderecocorrespondencia']) ? $parametros['st_enderecocorrespondencia'] : null));
        $enderecoCorresnpondeciaTO->setSt_bairro((!empty($parametros['st_bairrocorrespondencia']) ? $parametros['st_bairrocorrespondencia'] : null));
        /*
        * POG para salvar o complemento do endereço vazio, não adianta alterar o método que faz o update
        * passando a parametro para aceitar nulo como true, quando faz isso ele reclama de outras colunas vazias
        * que não deveriam ser
        */
        $enderecoCorresnpondeciaTO
            ->setSt_complemento((!empty($parametros['st_complementocorrespondencia']) ? $parametros['st_complementocorrespondencia'] : ""));
        $enderecoCorresnpondeciaTO->setSt_estadoprovincia((!empty($parametros['st_estadoProvinciacorrespondencia']) ? $parametros['st_estadoProvinciacorrespondencia'] : null));
        $enderecoCorresnpondeciaTO->setNu_numero((!empty($parametros['nu_numerocorrespondencia']) ? $parametros['nu_numerocorrespondencia'] : 0));

        $pessoaEnderecoCorrenspondenciaTO = new PessoaEnderecoTO();
        $pessoaEnderecoCorrenspondenciaTO->setBl_padrao(true);
        $pessoaEnderecoCorrenspondenciaTO->setBl_padrao(true);
        $pessoaEnderecoCorrenspondenciaTO->setId_entidade($pessoaTO->getId_entidade());

        $informacaoAcademica = new InformacaoAcademicaPessoaTO();
        $informacaoAcademica->setId_entidade($pessoaTO->getId_entidade());
        $informacaoAcademica->setId_nivelensino((!empty($parametros['id_nivelensino']) ? $parametros['id_nivelensino'] : null));
        $informacaoAcademica->setSt_curso((!empty($parametros['st_curso']) ? $parametros['st_curso'] : null));
        $informacaoAcademica->setSt_nomeinstituicao((!empty($parametros['st_nomeinstituicao']) ? $parametros['st_nomeinstituicao'] : null));

        $dadosBiomedicosTO = new DadosBiomedicosTO();
        $dadosBiomedicosTO->setId_entidade($pessoaTO->getId_entidade());
        $dadosBiomedicosTO->setSt_necessidadesespeciais((!empty($parametros['st_necessidadesespeciais']) ? $parametros['st_necessidadesespeciais'] : null));
        $dadosBiomedicosTO->setId_fatorrh(!empty($parametros['id_fatorrh']) ? $parametros['id_fatorrh'] : null);
        $dadosBiomedicosTO->setId_tiposanguineo(!empty($parametros['id_tiposanguineo']) ? $parametros['id_tiposanguineo'] : null);

        $mensageiro = $this->cadastrarPessoaGeral($usuarioTO, $pessoaTO, $contatoTelefoneTO, $contatoTelefonePessoaTO,
            $contatosEmailTO, $contatosEmailPessoaPerfilTO, $enderecoTO, $pessoaEnderecoTO, $documentoIdentidadeTO,
            $dadosBiomedicosTO, $contatoTelefoneTO2, $usuarioIntegracaoTO, $dadosAcessoTO, $enderecoCorresnpondeciaTO,
            $pessoaEnderecoCorrenspondenciaTO, $informacaoAcademica, $contatoTelefoneTO3 = null);

        if (empty($parametros['php'])) {
            return Ead1_BO::arrayUtfEncode((array)$mensageiro->toArrayAll());
        } else {
            return $mensageiro;
        }
    }

    /**
     * @param $estadocivil
     * @return int
     */
    private function trataEstadoCivil($estadocivil)
    {
        switch ($estadocivil) {
            case 'S':
                $id_estadocivil = EstadoCivilTO::SOLTEIRO;
                break;
            case 'C':
                $id_estadocivil = EstadoCivilTO::CASADO;
                break;
            case 'D':
                $id_estadocivil = EstadoCivilTO::SEPARADO;
                break;
            case null:
                $id_estadocivil = 0;
                break;
            default:
                $id_estadocivil = EstadoCivilTO::VIUVO;
                break;
        }
        return $id_estadocivil;
    }

    /**
     * @param $tipotelefone
     * @return int
     */
    private function trataTipoTelefone($tipotelefone)
    {
        switch ($tipotelefone) {
            case 'M':
                $id_tipotelefone = TipoTelefoneTO::CELULAR;
                break;
            case 'C':
                $id_tipotelefone = TipoTelefoneTO::COMERCIAL;
                break;
            case null:
                $id_tipotelefone = 0;
                break;
            default:
                $id_tipotelefone = TipoTelefoneTO::RESIDENCIAL;
                break;
        }
        return $id_tipotelefone;
    }


    /**
     *  Método que possui a lógica para o cadastramento geral de usuários
     * @param UsuarioTO $usuarioTO
     * @param PessoaTO $pessoaTO
     * @param ContatosTelefoneTO $contatoTelefoneTO
     * @param ContatosTelefonePessoaTO $contatoTelefonePessoaTO
     * @param ContatosEmailTO $contatosEmailTO
     * @param ContatosEmailPessoaPerfilTO $contatosEmailPessoaPerfilTO
     * @param EnderecoTO $enderecoTO
     * @param PessoaEnderecoTO $pessoaEnderecoTO
     * @param DocumentoIdentidadeTO $documentoIdentidadeTO
     * @param DadosBiomedicosTO $dadosBiomedicosTo
     * @param ContatosTelefoneTO $contatoTelefoneTO2
     * @param UsuarioIntegracaoTO $usuarioIntegracaoTO
     * @param null $dadosAcessoTO
     * @param EnderecoTO $enderecoCorrespondenciaTO
     * @param PessoaEnderecoTO $pessoaEnderecoCorrespondenciaTO
     * @param InformacaoAcademicaPessoaTO $dadosInformacaoAcademica
     * @param ContatosTelefoneTO|null $contatoTelefoneTO3
     * @return Ead1_Mensageiro
     */
    private function cadastrarPessoaGeral(
        UsuarioTO $usuarioTO,
        PessoaTO $pessoaTO,
        ContatosTelefoneTO $contatoTelefoneTO,
        ContatosTelefonePessoaTO $contatoTelefonePessoaTO,
        ContatosEmailTO $contatosEmailTO,
        ContatosEmailPessoaPerfilTO $contatosEmailPessoaPerfilTO,
        EnderecoTO $enderecoTO,
        PessoaEnderecoTO $pessoaEnderecoTO,
        DocumentoIdentidadeTO $documentoIdentidadeTO
        ,
        DadosBiomedicosTO $dadosBiomedicosTo
        ,
        ContatosTelefoneTO $contatoTelefoneTO2,
        UsuarioIntegracaoTO $usuarioIntegracaoTO,
        $dadosAcessoTO = null
        ,
        EnderecoTO $enderecoCorrespondenciaTO,
        PessoaEnderecoTO $pessoaEnderecoCorrespondenciaTO,
        InformacaoAcademicaPessoaTO $dadosInformacaoAcademica
        ,
        ContatosTelefoneTO $contatoTelefoneTO3 = null
    )
    {
        $dao = new Ead1_DAO();
        try {

            //$dao->beginTransaction();

            $senha = $usuarioTO->getSt_senha();
            $parametros['st_cpf'] = $usuarioTO->getSt_cpf();
            $parametros['php'] = true;

            $bo = new PessoaBO();

            $consultaUsuarioPessoa = $this->retornar($parametros);

            $st_nomecompleto = $usuarioTO->getSt_nomecompleto();
            $st_cpf = $usuarioTO->getSt_cpf();

            switch ($consultaUsuarioPessoa->getCodigo()) {
                case 'pessoa':
                    /** @var \UsuarioTO $usuarioTO */
                    $usuarioTO = $consultaUsuarioPessoa->getCurrent();

                    $usuarioTO->setSt_nomecompleto($st_nomecompleto);
                    $usuarioTO->setSt_cpf($st_cpf);
                    $usuarioTO->setSt_senha(null);
                    $pessoaTO->setId_usuario($usuarioTO->getId_usuario());

                    //Necessário para atualizar o nome do usuario em caso de atualização
                    $bo->_procedimentoSalvarUsuario($usuarioTO, true, $usuarioTO->getSessao()->id_entidade);
                    $bo->editarPessoa($usuarioTO, $pessoaTO);
                    break;
                case 'usuario':
                    $usuarioTO = $consultaUsuarioPessoa->getCurrent();

                    $usuarioTO->setSt_nomecompleto($st_nomecompleto);
                    $usuarioTO->setSt_cpf($st_cpf);
                    //Necessário para atualizar o nome do usuario em caso de atualização
                    $bo->_procedimentoSalvarUsuario($usuarioTO, true, $usuarioTO->getSessao()->id_entidade);

                    $pessoaTO->setId_usuariocadastro($usuarioTO->getId_usuario());
                    $pessoaTO->setId_usuario($usuarioTO->getId_usuario());
                    $pessoaTO = $bo->cadastrarPessoa($usuarioTO, $pessoaTO)->getFirstMensagem();

                    break;

                default:
                    $usuarioTO->setSt_nomecompleto($st_nomecompleto);
                    $usuarioTO->setSt_cpf($st_cpf);

                    $usuarioTO = $this->cadastrarUsuario($usuarioTO);

                    $pessoaTO->setId_usuariocadastro($usuarioTO->getId_usuario());
                    $pessoaTO->setId_usuario($usuarioTO->getId_usuario());
                    $bo->cadastrarPessoa($usuarioTO, $pessoaTO);

                    break;
            }

            $senhaDadosAcesso = "";
            $loginDadosAcesso = "";

            //se os dois ids entidade forem diferentes, então se trata de um cadastro do polo
            if ((int)$pessoaTO->getId_entidade() != (int)$usuarioTO->getSessao()->id_entidade) {

                //Pesquisa o usuário na tb_dadosacesso com o id de entidade do Polo
                $negocio = new \G2\Negocio\Negocio();
                $dadosAcessoEN = $negocio->findOneBy(
                    '\G2\Entity\DadosAcesso', array(
                        'id_usuario' => $pessoaTO->getId_usuario(),
                        'id_entidade' => (int)$pessoaTO->getId_entidade()
                    )
                );

                //Se não tiver dados na tb_dadosacesso para esse usuário e esse Polo,
                //  verifica se existe registro do Pai do Polo
                if (!$dadosAcessoEN) {
                    $dadosAcessoPaiEN = $negocio->findOneBy(
                        '\G2\Entity\DadosAcesso', array(
                            'id_usuario' => $pessoaTO->getId_usuario(),
                            'id_entidade' => (int)$usuarioTO->getSessao()->id_entidade
                        )
                    );

                    //Se tiver os dados do Pai do Polo seta a senha e o login
                    if ($dadosAcessoPaiEN) {
                        $senhaDadosAcesso = $dadosAcessoPaiEN->getSt_senha();
                        $loginDadosAcesso = $dadosAcessoPaiEN->getSt_login();
                    }
                }
            }

            if (!empty($senhaDadosAcesso) && !empty($loginDadosAcesso)) {
                $dadosAcessoTO = new DadosAcessoTO();
                //seta a senha que ja existe para esse usuário
                if (!empty($senhaDadosAcesso)) $dadosAcessoTO->setSt_senha($senhaDadosAcesso);
            }

            if ($dadosAcessoTO instanceof DadosAcessoTO) {
                $da = new DadosAcessoTO();
                $da->setId_usuario($usuarioTO->getId_usuario());
                $da->setId_entidade($pessoaTO->getId_entidade() ?
                    $pessoaTO->getId_entidade() : $usuarioTO->getSessao()->id_entidade);
                $da->setBl_ativo(true);
                $da->fetch(false, true, true);

                if ($da->getSt_senha() != $dadosAcessoTO->getSt_senha()) {
                    if (!empty($loginDadosAcesso)) $da->setSt_login($loginDadosAcesso);
                    $da->setSt_senha($dadosAcessoTO->getSt_senha());
                    $da->setId_dadosacesso(null);

                    $meda = $bo->cadastrarDadosAcesso($da);
                    if ($meda->getTipo() != Ead1_IMensageiro::SUCESSO) {
                        throw new Zend_Exception($meda->getFirstMensagem());
                    }
                }
            }

            if (!empty($enderecoTO->st_endereco)) {

                $end = new VwPessoaEnderecoTO();
                $end->setId_tipoendereco(TipoEnderecoTO::RESIDENCIAL);
                $end->setBl_ativo(true);
                $end->setId_entidade($pessoaTO->getId_entidade());
                $end->setId_usuario($usuarioTO->getId_usuario());
                $end->fetch(false, true, true);
                if ($end->getId_endereco()) {
                    $enderecoTO->setId_endereco($end->getId_endereco());
                    $pessoaEnderecoTO->setId_endereco($end->getId_endereco());
                }

                $this->cadastrarEndereco($usuarioTO, $enderecoTO, $pessoaEnderecoTO);
            }

            if (!empty($enderecoCorrespondenciaTO->st_endereco)) {

                $endCorresp = new VwPessoaEnderecoTO();
                $endCorresp->setId_tipoendereco(TipoEnderecoTO::CORRESPONDENCIA);
                $endCorresp->setBl_ativo(true);
                $endCorresp->setId_entidade($pessoaTO->getId_entidade());
                $endCorresp->setId_usuario($usuarioTO->getId_usuario());
                $endCorresp->fetch(false, true, true);
                if ($endCorresp->getId_endereco()) {
                    $enderecoCorrespondenciaTO->setId_endereco($endCorresp->getId_endereco());
                    $pessoaEnderecoCorrespondenciaTO->setId_endereco($endCorresp->getId_endereco());
                }

                $this->cadastrarEndereco($usuarioTO, $enderecoCorrespondenciaTO, $pessoaEnderecoCorrespondenciaTO);
            }


            if (!empty($contatosEmailTO->st_email)) {
                $contatosEmailPessoaPerfilTO->setId_usuario($usuarioTO->getId_usuario());
                $bo->_procedimentoSalvarContatosEmailPessoaPerfil($usuarioTO, $contatosEmailTO,
                    $contatosEmailPessoaPerfilTO);
            }

            if (!empty($contatoTelefoneTO->nu_telefone)) {
                $contatoTelefonePessoaTO->setId_usuario($usuarioTO->getId_usuario());
                $bo->_procedimentoSalvarContatosTelefonePessoa($usuarioTO, $contatoTelefoneTO,
                    $contatoTelefonePessoaTO);
            }

            if ($contatoTelefoneTO3 && $contatoTelefoneTO3->getNu_telefone()) {
                $contatoTelefonePessoaTO->setBl_padrao(false);
                $contatoTelefonePessoaTO->setId_telefone(null);
                $this->cadastrarTelefone($usuarioTO, $contatoTelefoneTO3, $contatoTelefonePessoaTO);
            }

            if (!empty($documentoIdentidadeTO->st_rg)) {
                $documentoIdentidadeTO->setId_usuario($usuarioTO->getId_usuario());
                $bo->_procedimentoSalvarDocumentoIdentidade($usuarioTO, $documentoIdentidadeTO);
            }

            if (!empty($dadosBiomedicosTo->st_necessidadesespeciais)) {
                $dadosBiomedicosTo->setId_usuario($usuarioTO->getId_usuario());
                $this->cadastrarDadosBiomedicos($usuarioTO, $dadosBiomedicosTo);
            }

            if (!empty($dadosInformacaoAcademica->id_nivelensino)) {

                $inforAcad = new InformacaoAcademicaPessoaTO();
                $inforAcad->setId_entidade($pessoaTO->getId_entidade());
                $inforAcad->setId_usuario($usuarioTO->getId_usuario());
                $inforAcad->fetch(false, true, true);

                if ($inforAcad->getId_informacaoacademicapessoa()) {
                    $dadosInformacaoAcademica
                        ->setId_informacaoacademicapessoa($inforAcad->getId_informacaoacademicapessoa());
                    $dadosInformacaoAcademica->setId_entidade($inforAcad->getId_entidade());
                }

                $dadosInformacaoAcademica->setId_usuario($usuarioTO->getId_usuario());
                $dadosInformacaoAcademica->setId_entidade($pessoaTO->getId_entidade());
                $this->cadastrarInformacoesAcedemicasPessoa($usuarioTO, $dadosInformacaoAcademica);
            }

            if (!empty($contatoTelefoneTO2->nu_telefone)) {
                if (!empty($contatoTelefoneTO->nu_telefone)) {
                    $contatoTelefonePessoaTO->setBl_padrao(false);
                }
                $contatoTelefonePessoaTO->setId_telefone(null);
                $contatoTelefonePessoaTO->setId_usuario($usuarioTO->getId_usuario());
                $bo->_procedimentoSalvarContatosTelefonePessoa($usuarioTO, $contatoTelefoneTO2,
                    $contatoTelefonePessoaTO);
                // $this->cadastrarTelefone($usuarioTO, $contatoTelefoneTO2, $contatoTelefonePessoaTO);
            }

            // verifica se o st_codusuario veio preenchido
            if ($usuarioIntegracaoTO->getSt_codusuario()) {
                //seta os valores
                $usuarioIntegracaoTO->setId_usuario($usuarioTO->getId_usuario());
                $usuarioIntegracaoTO->setId_usuariocadastro($usuarioTO->getId_usuario());
                $usuarioIntegracaoTO->setDt_cadastro(new DateTime());
                $usuarioIntegracaoTO->setSt_loginintegrado($usuarioTO->getSt_login());
                $usuarioIntegracaoTO->setSt_senhaintegrada($usuarioTO->getSt_senha());
                $this->cadastraUsuarioIntegracao($usuarioIntegracaoTO);
            }

            $dados['usuario'] = $usuarioTO;
            $dados['pessoa'] = $pessoaTO;
            $dados['endereco'] = $enderecoTO;
            $dados['email'] = $contatosEmailTO;
            $dados['telefone'] = $contatoTelefoneTO;
            $dados['identidade'] = $documentoIdentidadeTO;
            $dados['biomedicos'] = $dadosBiomedicosTo;
            $dados['telefone2'] = $contatoTelefoneTO2;
            $dados['informacaoAcademica'] = $dadosInformacaoAcademica;

            $mensageiro = new Ead1_Mensageiro($dados, Ead1_IMensageiro::SUCESSO);
            //$dao->commit();
        } catch (Exception $e) {
            //$dao->rollBack();
            $mensageiro = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, 'Exception');
        } catch (Zend_Exception $e) {
            // $dao->rollBack();
            $mensageiro = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, 'Zend_Exception');
        } catch (Zend_Db_Exception $e) {
            // $dao->rollBack();
            $mensageiro = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, 'Zend_Db_Exception');
        }
        return $mensageiro;
    }


    /**
     * @param UsuarioIntegracaoTO $usuarioIntegracaoTO
     * @return \G2\Entity\UsuarioIntegracao
     * @throws Zend_Exception
     */
    private function cadastraUsuarioIntegracao(UsuarioIntegracaoTO $usuarioIntegracaoTO)
    {
        $negocio = new \G2\Negocio\Pessoa();
        return $negocio->salvaUsuarioIntegracao($usuarioIntegracaoTO->toArray());
    }

    /**
     * Método que cadastra telefones
     * @param UsuarioTO $usuarioTO
     * @param ContatosTelefoneTO $contatoTelefoneTO
     * @param ContatosTelefonePessoaTO $contatoTelefonePessoaTO
     * @return Ead1_TO|mixed|the
     * @throws Exception
     */
    private function cadastrarTelefone(
        UsuarioTO $usuarioTO,
        ContatosTelefoneTO $contatoTelefoneTO,
        ContatosTelefonePessoaTO $contatoTelefonePessoaTO
    )
    {
        $ro = new PessoaRO();
        return $this->trataRetornoMensageiro($ro->salvarTelefone($usuarioTO, $contatoTelefoneTO,
            $contatoTelefonePessoaTO), $contatoTelefoneTO);
    }


    /**
     * Método que cadastra os dadosBiometricos
     * @param UsuarioTO $usuarioTO
     * @param DadosBiomedicosTO $dadosBiomedicosTo
     * @return Ead1_TO|mixed|the
     * @throws Exception
     */
    private function cadastrarDadosBiomedicos(UsuarioTO $usuarioTO, DadosBiomedicosTO $dadosBiomedicosTo)
    {
        $ro = new PessoaRO();
        return $this->trataRetornoMensageiro($ro->salvarDadosBiomedicos($usuarioTO, $dadosBiomedicosTo));
    }

    /**
     * Método para cadastro de endereço vinculado a uma pessoa
     * @param UsuarioTO $usuarioTO
     * @param EnderecoTO $enderecoTO
     * @param PessoaEnderecoTO $pessoaEnderecoTO
     * @return Ead1_TO|mixed|the
     * @throws Exception
     */
    private function cadastrarEndereco(UsuarioTO $usuarioTO, EnderecoTO $enderecoTO, PessoaEnderecoTO $pessoaEnderecoTO)
    {
        $ro = new PessoaRO();
        return $this->trataRetornoMensageiro($ro->salvarEndereco($usuarioTO, $enderecoTO, $pessoaEnderecoTO),
            $enderecoTO);
    }


    /**
     * Método que trata o retorno dos mensageiros para capturar o TO desejado no mensageiro
     * @param Ead1_Mensageiro $mensageiro
     * @param Ead1_TO|null $to
     * @return Ead1_TO|mixed|the
     * @throws Exception
     */
    private function trataRetornoMensageiro(Ead1_Mensageiro $mensageiro, Ead1_TO $to = null)
    {
        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            while ($mensagem = $mensageiro->getCurrent()) {
                if ($mensagem instanceOf Ead1_TO) {
                    $to = $mensagem;
                    return $to;
                } elseif (is_string($mensagem)) {
                    return $mensagem;
                } elseif ($mensagem instanceOf \G2\G2Entity) {
                    $negocio = new \G2\Negocio\Negocio();
                    return $negocio->entityToTO($mensagem);
                }

            }
        } else {
            throw new Exception("Erro no serviço! " . get_class($to) . ", " . $mensageiro->getCurrent() . "({$mensageiro->getCodigo()})");
        }
    }

    /**
     * Método que cadastra Usuarios
     * @param UsuarioTO $usuarioTO
     * @return Ead1_TO|mixed|the
     * @throws Exception
     */
    private function cadastrarUsuario(UsuarioTO $usuarioTO)
    {
        $ro = new PessoaRO();
        return $this->trataRetornoMensageiro($ro->salvarUsuario($usuarioTO), $usuarioTO);
    }


    /**
     * Retorna os dados para Acesso baseado em Usuário e Senha
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param array|null $parametros
     * @return array
     */
    public function retornarDadosLoginUnico(array $parametros = null)
    {

        try {

            $usTO = new VwUsuarioPerfilPedagogicoTO();
            $usTO->montaToDinamico($parametros);
            $usTO->setId_entidade($usTO->getSessao()->id_entidade);

// 			$to = new LoginTO();
// 			$to->setSenha($usTO->getSt_senha());
// 			$to->setUsuario($usTO->getSt_login());

// 			$login = new LoginBO();
// 			$mensageiro = $login->retornaVwPessoaLogin($to, $usTO->getId_entidade());
// 			if($mensageiro->getTipo()!=Ead1_IMensageiro::SUCESSO){
// 				return $mensageiro;
// 			}

            $orm = new VwUsuarioPerfilPedagogicoORM();
            $dados = $orm->consulta($usTO, false, false, array('st_tituloexibicao', 'st_saladeaula'));
            if (empty($dados)) {
                $mensageiro = new Ead1_Mensageiro('Nenhum registro encontrado!', Ead1_IMensageiro::AVISO);
            } else {
                $mensageiro = new Ead1_Mensageiro($dados, Ead1_IMensageiro::SUCESSO);
            }

            $retorno = (array)$mensageiro->toArrayAll();
            return Ead1_BO::arrayUtfEncode($retorno);

        } catch (Exception $e) {
            $mensageiro = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
            $retorno = (array)$mensageiro->toArrayAll();
            return Ead1_BO::arrayUtfEncode($retorno);
        }

    }


    /**
     * Retorna os dados de Login
     * @param array $parametros
     * @return array
     */
    public function retornarDadosLogin(array $parametros)
    {

        try {
            $mensageiro = new Ead1_Mensageiro();
            if (!isset($parametros['st_senha']) || $parametros['st_senha'] == false) {
                throw new Zend_Exception("Informe a senha!");
            }
            if (!isset($parametros['st_login']) || $parametros['st_login'] == false) {
                throw new Zend_Exception("Informe o Usuário!");
            }

            $to = new LoginTO();
            $to->setSenha($parametros['st_senha']);
            $to->setUsuario($parametros['st_login']);

            $bo = new LoginBO();
            $return = $bo->retornaVwPessoaLogin($to, $to->getSessao()->id_entidade);
            if ($return->getType() == Ead1_IMensageiro::TYPE_SUCESSO) {
                return (array)$return->toArrayAll();
            } else {
                $mensageiro->setMensageiro('E-mail ou Senha inválido.', Ead1_IMensageiro::AVISO);
                return (array)$mensageiro->toArrayAll();
            }

//            return (array)$bo->retornaVwPessoaLogin($to, $to->getSessao()->id_entidade)->toArrayAll();

        } catch (Exception $e) {
//            $mensageiro = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
            $mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
            return (array)$mensageiro->toArrayAll();
        }

    }


    /**
     * Recupera a Senha
     * @param array $parametros
     * @return array
     */
    public function recuperarSenha(array $parametros)
    {
        try {
            //verifica se o (st_cpf ou st_login) e st_email foi passado
            if (((isset($parametros['st_cpf']) || $parametros['st_cpf'] != false)
                    || (isset($parametros['st_login']) || $parametros['st_login'] != false))
                && (isset($parametros['st_email']) || $parametros['st_email'] != false)
            ) {
                //instancia a VwPessoaTO
                $vwPessoaTO = new VwPessoaTO();
                //seta os atributos da TO
                $vwPessoaTO->setSt_login(isset($parametros['st_login']) ? $parametros['st_login'] : null);
                $vwPessoaTO->setSt_cpf(isset($parametros['st_cpf']) ? $parametros['st_cpf'] : null);
                $vwPessoaTO->setSt_email(isset($parametros['st_email']) ? $parametros['st_email'] : null);
                $vwPessoaTO->setId_entidade($vwPessoaTO->getSessao()->id_entidade);

                //intancia o LoginBO
                $loginBO = new LoginBO();
                //chama o método de recuperar senha
                $mensageiro = $loginBO->recuperarSenhaPessoa($vwPessoaTO);
                //retorna
                return (array)$mensageiro->toArrayAll();
            } else { //se o st_cpf ou st_login não foi passado e st_email
                //cria os exceptions
                if (!isset($parametros['st_email']) || $parametros['st_email'] == false) {
                    throw new Zend_Exception("Informe o e-mail!");
                }
                if ((!isset($parametros['st_cpf']) || $parametros['st_cpf'] == false)
                    || (!isset($parametros['st_login']) || $parametros['st_login'] == false)
                ) {
                    throw new Zend_Exception("Informe o CPF ou Nome de usuário!");
                }
            }
        } catch (Zend_Exception $e) {
            $mensageiro = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
            return (array)$mensageiro->toArrayAll();
        }

    }


    /**
     * Verifica o E-mail
     * @param array $parametros
     * @return array
     */
    public function verificarEmail(array $parametros)
    {

        try {

            if (!isset($parametros['st_email']) || $parametros['st_email'] == false) {
                throw new Zend_Exception("Informe o e-mail!");
            }

            $vwPessoaTO = new VwPessoaTO();
            $vwPessoaTO->setSt_email($parametros['st_email'] ? $parametros['st_email'] : null);
            $vwPessoaTO->setId_entidade($vwPessoaTO->getSessao()->id_entidade);
            $vwPessoaTO->fetch(false, true, true);

            if ($vwPessoaTO->getId_usuario()) {
                $mensageiro = new Ead1_Mensageiro($vwPessoaTO->toArray(), Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new Ead1_Mensageiro("Nenhum Usuário encontrado!", Ead1_IMensageiro::AVISO);
            }

            return (array)$mensageiro->toArrayAll();

        } catch (Zend_Exception $e) {
            $mensageiro = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
            return (array)$mensageiro->toArrayAll();
        }

    }

    /**
     * @param UsuarioTO $usuarioTO
     * @param InformacaoAcademicaPessoaTO $dadosInformacaoAcademica
     * @return Ead1_TO|mixed|the
     * @throws Exception
     */
    private function cadastrarInformacoesAcedemicasPessoa(
        UsuarioTO $usuarioTO,
        InformacaoAcademicaPessoaTO $dadosInformacaoAcademica
    )
    {
        $ro = new PessoaRO();
        return $this->trataRetornoMensageiro($ro->salvarInformacaoAcademica($usuarioTO, $dadosInformacaoAcademica));
    }

}

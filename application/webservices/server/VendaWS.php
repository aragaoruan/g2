<?php

/**
 * Classe de regras de negócio para Serviços relacionados a Venda
 * @author Elcio Mauro Guimarães
 * @since 28/06/2012
 */
class VendaWS
{

    /**
     * @var Ead1_Mensageiro
     */
    public $mensageiro;


    public function __construct()
    {
        $this->mensageiro = new Ead1_Mensageiro();
    }

    /**
     * Método que retorna os Meios de Pagamento
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param array $parametros
     * @return array
     */
    public function meiospagamento(array $parametros = null)
    {

        try {
            $to = new MeioPagamentoTO();
            if ($parametros) {
                $to->montaToDinamico($parametros);
            }
            $orm = new MeioPagamentoORM();
            $dados = $orm->consulta($to, false, false, array('st_meiopagamento'));
            $this->mensageiro->setMensageiro($dados, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao retornar os Meios de Pagamento.', Ead1_IMensageiro::ERRO);
        }

        return (array)$this->mensageiro->toArrayAll();

    }


    /**
     * Método que cadastra uma pré-venda
     * @param array $parametros
     * @return array
     */
    public function prevenda(array $parametros)
    {
        $isValid = $this->validaPreMatricula($parametros);
        if($isValid)
        {
            $pvTO = new PreVendaTO();

            $pvTO->montaToDinamico($parametros);

            if (!empty($parametros['produtos'])) {

                $produtos = $parametros['produtos'];
                unset($parametros['produtos']);

                foreach ($produtos as $k => $produto) {
                    $parametros['produtos'][] = new PreVendaProdutoTO(array($k => $produto));
                }
            } else {
                $parametros['produtos'] = null;
            }


            $preBO = new PreVendaBO();
            $this->mensageiro = $preBO->cadastrarPreVenda($pvTO, $parametros['produtos']);

            return (array)$this->mensageiro->toArrayAll();
        }else{
            $this->mensageiro = $this->mensageiro->setMensageiro("Campos Inválidos. Verifique-os e tente de novo.",Ead1_IMensageiro::ERRO);
            return (array)$this->mensageiro->toArrayAll();
        }
    }


    /**
     * Roda o processo de Ativação automática pelo WS
     * @param array $parametros
     * @return array
     */
    public function ativarvenda(array $parametros)
    {
        ini_set('display_erros', 1);
        try {
            if (!isset($parametros['id_venda']) || !isset($parametros['id_entidade'])) {
                throw new \Exception("É obrigatório informar o id_venda e o id_entidade.");
            }
            $v = new \G2\Entity\Venda();
            $v->find($parametros['id_venda']);
            if ($v->getIdEvolucao()->getId_evolucao() == 10) {
                $mensageiro = new \Ead1_Mensageiro("Venda na evolução: " . $v->getIdEvolucao()->getSt_evolucao(), \Ead1_IMensageiro::ERRO);
            } else {
                if ($v->getIdEntidade() != $parametros['id_entidade']) {
                    throw new \Exception("A Venda informada não bate com a Entidade informada.");
                }
                $n = new \G2\Negocio\Recebimento();
                $mensageiro = $n->processoAtivacaoAutomatica($parametros['id_venda']);
            }

        } catch (\Exception $e) {
            $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return (array)$mensageiro->toArrayAll();

    }


    /**
     * Salva os lançamentos
     * @param array $parametros
     * @return array
     */
    public function salvarlancamentos(array $parametros)
    {


        $dao = new WebServiceDAO();
        $dao->beginTransaction();
        try {

            if (!isset($parametros['id_venda']) || !$parametros['id_venda']) {
                throw new Exception("Informe o id_venda");
            }

            $vendaTO = new VendaTO();
            $vendaTO->setId_venda($parametros['id_venda']);
            $vendaTO->fetch(true, true, true);

            if (!$vendaTO->getId_entidade()) {
                throw new Exception("Não encontramos esta venda");
            }

            if ($vendaTO->getId_situacao() != VendaTO::SITUACAO_VENDA_PENDENTE) {
                throw new Exception("Está venda não pode mai ser alterada. Para poder haver alteração ela precisa estar na situação PENDENTE.");
            }

            if ($vendaTO->getId_evolucao() != VendaTO::EVOLUCAO_AGUARDANDO_RECEBIMENTO) {
                throw new Exception("Está venda não pode mai ser alterada. Para poder haver alteração ela precisa estar na evolução AGUARDANDO RECEBIMENTO.");
            }

            $vendaBO = new VendaBO();
            $altervanda = false;

            if (!empty($parametros['nu_juros'])) {
                $vendaTO->setNu_juros($parametros['nu_juros']);
                $altervanda = true;
            }
            if (!empty($parametros['nu_parcelas'])) {
                $vendaTO->setNu_parcelas($parametros['nu_parcelas']);
                $altervanda = true;
            }
            if (!empty($parametros['nu_valorbruto'])) {
                $vendaTO->setNu_valorbruto($parametros['nu_valorbruto']);
                $altervanda = true;
            }
            if (!empty($parametros['nu_valorliquido'])) {
                $vendaTO->setNu_valorliquido($parametros['nu_valorliquido']);
                $altervanda = true;
            }
            if (!empty($parametros['st_observacao'])) {
                $vendaTO->setSt_observacao($parametros['st_observacao']);
                $altervanda = true;
            }

            if ($altervanda) {
                $mensageirovenda = $vendaBO->editarVenda($vendaTO);
                if ($mensageirovenda->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception("Erro ao cadastrar a Venda!");
                }
            }


            $arrOrdem = array();
            if (isset($parametros['lancamentos'])) {
                foreach ($parametros['lancamentos'] as $index => $lancamento) {
                    $lancamentoTO = new LancamentoTO();
                    $lancamentoTO->setId_meiopagamento($lancamento['id_meiopagamento']);

                    if ($lancamentoTO->getId_meiopagamento() == \G2\Constante\MeioPagamento::CARTAO_CREDITO || $lancamentoTO->getId_meiopagamento() == \G2\Constante\MeioPagamento::CARTAO_RECORRENTE) {
                        $lancamentoTO->setnu_cartao(isset($lancamento['nu_cartao']) && $lancamento['nu_cartao'] ? $lancamento['nu_cartao'] : 1);
                    }

                    $lancamentoTO->setNu_valor($lancamento['nu_valor']);
                    if (!isset($lancamento['id_usuariolancamento']) && !isset($lancamento['id_entidadelancamento'])) {
                        throw new Zend_Exception('Nenhum responsável setado para lançamento!');
                    }
                    $lancamentoTO->setBl_quitado(false);
                    $lancamentoTO->setId_usuariocadastro($vendaTO->getId_usuariocadastro());
                    $lancamentoTO->setId_entidade($vendaTO->getId_entidade());
                    $lancamentoTO->setId_usuariolancamento((isset($lancamento['id_usuariolancamento']) ? $lancamento['id_usuariolancamento'] : 0));
                    $lancamentoTO->setId_entidadelancamento((isset($lancamento['id_entidadelancamento']) ? $lancamento['id_entidadelancamento'] : 0));


                    if (isset($lancamento['dt_vencimento']) && !empty($lancamento['dt_vencimento'])) {

                        $data = explode('/', $lancamento['dt_vencimento']);
                        $lancamentoTO->setDt_vencimento(new Zend_Date(array('year' => $data[2], 'month' => $data[1], 'day' => $data[0])));
                        if ($lancamentoTO->getId_meiopagamento() == MeioPagamentoTO::CARTAO && (isset($lancamento['bl_areceber']) == false || $lancamento['bl_areceber'] == false)) {
                            $lancamentoTO->setSt_coddocumento((isset ($lancamento['nu_documento']) ? $lancamento['nu_documento'] : null));
                            $lancamentoTO->setDt_prevquitado(new Zend_Date(array('year' => $data[2], 'month' => $data[1], 'day' => $data[0])));
                        }

                    }
                    if (isset($lancamento['dt_quitado']) && !empty($lancamento['dt_quitado'])) {

                        if ($lancamentoTO->getId_meiopagamento() != MeioPagamentoTO::CARTAO) {
                            $data = explode('/', $lancamento['dt_quitado']);
                            $lancamentoTO->setDt_quitado(new Zend_Date(array('year' => $data[2], 'month' => $data[1], 'day' => $data[0])));
                            $lancamentoTO->setBl_quitado(true);
                        }
                    }
                    $lancamentoTO->setNu_quitado((isset($lancamento['nu_quitado']) ? $lancamento['nu_quitado'] : null));
                    $lancamentoTO->setId_tipolancamento(TipoLancamentoTO::CREDITO);
                    $lancamentoTO->setNu_cobranca(!empty($lancamento['nu_ordem']) ? $lancamento['nu_ordem'] : ($index + 1));

                    $arrOrdem[] = $lancamento['nu_ordem'];

                    $arrLancamentoTO[] = $lancamentoTO;
                }

                $mensageiro = $vendaBO->salvarArrayLancamentos($arrLancamentoTO, $vendaTO, false, false, $arrOrdem);


                $arvenda = array(
                    'id_venda' => $vendaTO->getId_venda(),
                    'nu_juros' => $vendaTO->getNu_juros(),
                    'nu_parcelas' => $vendaTO->getNu_parcelas(),
                    'nu_valorbruto' => $vendaTO->getNu_valorbruto(),
                    'nu_valorliquido' => $vendaTO->getNu_valorliquido(),
                    'st_observacao' => $vendaTO->getSt_observacao()
                );


                $this->mensageiro->setMensageiro(array('venda' => $arvenda, 'lancamentos' => $mensageiro->getCodigo()), $mensageiro->getTipo());
            } else {
                $this->mensageiro->setMensageiro("Não foi informado nenhum lançamento!", Ead1_IMensageiro::AVISO);
            }

            $dao->commit();
        } catch (Exception $e) {
            $dao->rollBack();
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }

        return (array)$this->mensageiro->toArrayAll();

    }

    /**
     * Método que cadastra vendas / Lançamentos
     */
    public function cadastrar(array $parametros)
    {
        $entidadeTO = new EntidadeTO();
        $entidadeTO->setId_entidade($parametros['id_entidade']);
        $entidadeTO->fetch(true, true, true);
        $id = $entidadeTO->getId_entidade();

        $idEntidadeVenda = NULL;
        if (array_key_exists('id_entidadevendag2', $parametros) && !empty($parametros['id_entidadevendag2'])) {
            $idEntidadeVenda = $parametros['id_entidadevendag2'];
        }


        $dao = new WebServiceDAO();
        $dao->beginTransaction();
        $pessoaWS = new PessoaWS();

        try {

            $parametros['php'] = true;
            $usuarioPessoaMensagem = $pessoaWS->retornar($parametros);
            if ($usuarioPessoaMensagem->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Exception("Erro ao retornar a Pessoa");
            }

            $usuarioPessoa = $usuarioPessoaMensagem->getMensagem();

            /*
             * verifica se a variavel $id que foi setado com o id_entidade é diferente da variavel $idEntidadeVenda
             * e se a $idEntidadeVenda não esta vazia, se aceitar esse criterio ele passa o valor da variavel $idEntidadeVenda
             * se não ele vai assumir o valor da variavel $id.
             */
            $formaPagamento = $dao->retornarFormaPagamento($id != $idEntidadeVenda && !empty($idEntidadeVenda) ? $idEntidadeVenda : $id);
            if (!$formaPagamento) {
                throw new Zend_Exception("Forma de Pagamento não encontrada para a categoria SITE!");
            }

            $vendaTO = new VendaTO();
            $vendaTO->setId_entidade($id != $idEntidadeVenda && !empty($idEntidadeVenda) ? $idEntidadeVenda : $id);
            $vendaTO->setId_usuario($usuarioPessoa["usuario"]->getid_usuario());
            $vendaTO->setId_usuariocadastro((isset($parametros['id_usuariocadastro']) && $parametros['id_usuariocadastro']) ? $parametros['id_usuariocadastro'] : $usuarioPessoa["usuario"]->getid_usuario());

            if (!empty($parametros['id_atendente'])){
                if ($this->atendenteCadastradoNaEntidade($parametros['id_atendente'],
                                                            $parametros['id_entidadevendag2'])){
                    $vendaTO->setId_atendente($parametros['id_atendente']);
                }else{
                    $cadastro = $this->cadastraAtendenteNaEntidade($parametros['id_atendente'],
                                                                    $parametros['id_entidadevendag2']);
                    if($cadastro){
                        $vendaTO->setId_atendente($parametros['id_atendente']);
                    }
                }
            }else{
                $vendaTO->setId_atendente(null);
            }

            if (isset($parametros['id_evolucao']) && $parametros['id_evolucao']) {
                $vendaTO->setId_evolucao($parametros['id_evolucao']);
            } elseif (isset($parametros['lancamentos'])) {
                $vendaTO->setId_evolucao(VendaTO::EVOLUCAO_AGUARDANDO_RECEBIMENTO);
            } else {
                $vendaTO->setId_evolucao(VendaTO::EVOLUCAO_AGUARDANDO_NEGOCIACAO);
            }

            if (isset($parametros['id_situacao']) && $parametros['id_situacao']) {
                $vendaTO->setId_situacao($parametros['id_situacao']);
            } else {
                $vendaTO->setId_situacao(VendaTO::SITUACAO_VENDA_PENDENTE);
            }

            $vendaTO->setBl_ativo(true);
            $vendaTO->setBl_contrato(true);
            $vendaTO->setSt_codpromocao(!empty($parametros['codpromocao']) ? $parametros['codpromocao'] : NULL);
            $vendaTO->setSt_nomconvenio(!empty($parametros['nomconvenio']) ? $parametros['nomconvenio'] : NULL);
            $vendaTO->setId_formapagamento($formaPagamento->id_formapagamento); // Forma Pagamento Aplicação Venda/Site 2
            $vendaTO->setNu_descontoporcentagem(0);
            $vendaTO->setNu_descontovalor(0);
            $vendaTO->setNu_juros((!empty($parametros['nu_juros']) ? $parametros['nu_juros'] : null));
            $vendaTO->setNu_parcelas((!empty($parametros['nu_parcelas']) ? $parametros['nu_parcelas'] : null));
            $vendaTO->setNu_valorbruto($parametros['nu_valorbruto']);
            $vendaTO->setNu_valorliquido($parametros['nu_valorliquido']);
            $vendaTO->setId_contratoafiliado((!empty($parametros['id_contratoafiliado']) ? $parametros['id_contratoafiliado'] : null));
            $vendaTO->setSt_observacao(!empty($parametros['st_observacao']) ? $parametros['st_observacao'] : null);
            $vendaTO->setDt_agendamento(!empty($parametros['dt_agendamento']) ? $parametros['dt_agendamento'] : null);
            $vendaTO->setRecorrenteOrderid(!empty($parametros['recorrente_orderid']) ? $parametros['recorrente_orderid'] : null);
            $vendaTO->setBl_transferidoentidade(!empty($parametros['bl_transferidoentidade']) ? $parametros['bl_transferidoentidade'] : null);
            $vendaTO->setId_campanhapontualidade(!empty($parametros['id_campanhapontualidade']) ? $parametros['id_campanhapontualidade'] : null);

            /**
             * @history GII-7152 ( http://jira.unyleya.com.br/browse/GII-7152 )
             * @date 2017-02-13
             */
            $vendaTO->setId_afiliado(!empty($parametros['id_afiliado']) ? $parametros['id_afiliado'] : null);
            $vendaTO->setId_nucleotelemarketing(!empty($parametros['id_nucleotelemarketing']) ? $parametros['id_nucleotelemarketing'] : null);
            $vendaTO->setId_vendedor(!empty($parametros['id_vendedor']) ? $parametros['id_vendedor'] : null);
            $vendaTO->setSt_siteorigem(!empty($parametros['st_siteorigem']) ? $parametros['st_siteorigem'] : null);

            /*$vendaTO->setId_tipocampanha();
            $vendaTO->setId_campanhacomercial();
            $vendaTO->setNu_diamensalidade();*/

            $vendaBO = new VendaBO();
            $mensageirovenda = $vendaBO->cadastrarVenda($vendaTO);
            if ($mensageirovenda->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Exception("Erro ao cadastrar a Venda!");
            }

            $idVenda = $mensageirovenda->getFirstMensagem()->getid_venda();

            if(!empty($parametros['st_urltermoresponsabilidade'])) {
                $tramite = new TramiteBO();
                $tramiteTO = new TramiteTO();
                $tramiteTO->setSt_tramite('Termo de responsabilidade aceito pelo aluno');
                $tramiteTO->setst_url($parametros['st_urltermoresponsabilidade']);
                $tramiteTO->setId_entidade($vendaTO->getId_entidade());
                $tramiteTO->setId_tipotramite(\G2\Constante\TipoTramite::VENDA_TERMO_RESPONSABILIDADE);
                $tramite->cadastrarTramite ($tramiteTO, CategoriaTramiteTO::VENDA, $vendaTO->getId_venda());
            }

            $produtoProjeto = false;

            foreach ($parametros['produtos'] as $produtoAtual) {

                if (!$produtoAtual['id_produto']) {
                    throw new Zend_Exception('Informe o Produto!');
                }

                $produto = false;

                $pTO = new ProdutoTO();
                $pTO->setId_produto($produtoAtual['id_produto']);

                $produtoBO = new ProdutoBO();
                $mensagemProduto = $produtoBO->retornarProduto($pTO);

                if ($mensagemProduto->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception('Erro ao buscar o Produto!');
                }
                $produto = $mensagemProduto->getFirstMensagem();

                if ($produto->getId_tipoproduto() == TipoProdutoTO::PROJETO_PEDAGOGICO && !$produtoProjeto) {
                    $produtoProjeto = true;
                    $contratoRegra = $this->buscarRegraContratoProduto($produto->getId_Produto(), $id, true);
                    if ($contratoRegra->getTipo() != Ead1_IMensageiro::SUCESSO) {
                        throw new Zend_Exception($contratoRegra->getFirstMensagem());
                    }
                    $contratoRegraTO = $contratoRegra->getFirstMensagem();
                }
                $vendaProdutoTO = new VendaProdutoTO();
                $vendaProdutoTO->setId_produto($produto->getId_Produto());
                $vendaProdutoTO->setId_venda($vendaTO->getId_venda());
                $vendaProdutoTO->setid_tiposelecao(isset($parametros['id_tiposelecao']) ? $parametros['id_tiposelecao'] : null);
                //$vendaProdutoTO->setId_campanhacomercial();
                $vendaProdutoTO->setNu_desconto($produtoAtual['nu_desconto']);
                $vendaProdutoTO->setNu_valorliquido($produtoAtual['nu_valorliquido']);
                $vendaProdutoTO->setNu_valorbruto($produtoAtual['nu_valorbruto']);
                $vendaProdutoTO->setId_turma((isset($produtoAtual['id_turma']) ? $produtoAtual['id_turma'] : null));
                $vendaProdutoTO->setId_campanhacomercial(isset($produtoAtual['id_campanhacomercial']) ? $produtoAtual['id_campanhacomercial'] : null);

                $arVendaProduto[] = $vendaProdutoTO;
            }

            $vendaBO = new VendaBO();
            $mensageirovp = $vendaBO->salvarArrVendaProduto($arVendaProduto);
            if ($mensageirovp->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Exception($mensageirovp->getFirstMensagem());
            }
            $arrOrdem = array();
            if (isset($parametros['lancamentos'])) {
                foreach ($parametros['lancamentos'] as $index => $lancamento) {
                    $lancamentoTO = new LancamentoTO();
                    $lancamentoTO->setId_meiopagamento($lancamento['id_meiopagamento']);

                    $lancamentoTO->setnu_cartao(isset($lancamento['nu_cartao']) && $lancamento['nu_cartao'] ? $lancamento['nu_cartao'] : 1);

                    $lancamentoTO->setNu_valor($lancamento['nu_valor']);
                    if (!isset($lancamento['id_usuariolancamento']) && !isset($lancamento['id_entidadelancamento'])) {
                        throw new Zend_Exception('Nenhum responsável setado para lançamento!');
                    }
                    $lancamentoTO->setBl_quitado(false);
                    $lancamentoTO->setId_usuariocadastro((isset($parametros['id_usuariocadastro']) && $parametros['id_usuariocadastro']) ? $parametros['id_usuariocadastro'] : $usuarioPessoa["usuario"]->getid_usuario());
                    $lancamentoTO->setId_entidade($vendaTO->getId_entidade());
                    $lancamentoTO->setId_usuariolancamento((isset($lancamento['id_usuariolancamento']) ? $lancamento['id_usuariolancamento'] : 0));
                    $lancamentoTO->setId_entidadelancamento((isset($lancamento['id_entidadelancamento']) ? $lancamento['id_entidadelancamento'] : 0));
                    $lancamentoTO->setIdSistemacobranca(\G2\Constante\Sistema::PAGARME);

                    if (isset($lancamento['dt_vencimento']) && !empty($lancamento['dt_vencimento'])) {

                        $data = explode('/', $lancamento['dt_vencimento']);
                        $lancamentoTO->setDt_vencimento(new Zend_Date(array('year' => $data[2], 'month' => $data[1], 'day' => $data[0])));
                        if ($lancamentoTO->getId_meiopagamento() == MeioPagamentoTO::CARTAO && (isset($lancamento['bl_areceber']) == false || $lancamento['bl_areceber'] == false)) {
                            $lancamentoTO->setSt_coddocumento((isset ($lancamento['nu_documento']) ? $lancamento['nu_documento'] : null));
                            $lancamentoTO->setDt_prevquitado(new Zend_Date(array('year' => $data[2], 'month' => $data[1], 'day' => $data[0])));
                        }

                    }
                    if (isset($lancamento['dt_quitado']) && !empty($lancamento['dt_quitado'])) {

                        if ($lancamentoTO->getId_meiopagamento() != MeioPagamentoTO::CARTAO) {
                            $data = explode('/', $lancamento['dt_quitado']);
                            $lancamentoTO->setDt_quitado(new Zend_Date(array('year' => $data[2], 'month' => $data[1], 'day' => $data[0])));
                            $lancamentoTO->setBl_quitado(true);
                        }
                    }
                    $lancamentoTO->setNu_quitado((isset($lancamento['nu_quitado']) ? $lancamento['nu_quitado'] : null));
                    $lancamentoTO->setId_tipolancamento(TipoLancamentoTO::CREDITO);
                    $lancamentoTO->setNu_cobranca(!empty($lancamento['nu_ordem']) ? $lancamento['nu_ordem'] : ($index + 1));
                    if ($index===0) {
                        $lancamentoTO->setNu_assinatura(!empty($parametros['recorrente_orderid']) ? $parametros['recorrente_orderid'] : null);
                    }

                    $arrOrdem[] = $lancamento['nu_ordem'];

                    $arrLancamentoTO[] = $lancamentoTO;
                }

                $vendaBO->salvarArrayLancamentos($arrLancamentoTO, $vendaTO, false, false, $arrOrdem);
            }

            if ($produtoProjeto) {

                $contratoTO = new ContratoTO();
                $contratoTO->setId_entidade($vendaTO->getId_entidade());
                $contratoTO->setId_usuario($usuarioPessoa["usuario"]->getid_usuario());
                $contratoTO->setId_usuariocadastro((isset($parametros['id_usuariocadastro']) && $parametros['id_usuariocadastro']) ? $parametros['id_usuariocadastro'] : $usuarioPessoa["usuario"]->getid_usuario());
                $contratoTO->setId_venda($vendaTO->getId_venda());
                $contratoTO->setId_contratoregra($contratoRegraTO->getId_contratoregra());
                $contratoTO->setId_evolucao(ContratoTO::EVOLUCAO_AGUARDANDO_CONFIRMACAO);
                $contratoTO->setId_situacao(ContratoTO::SITUACAO_PENDENTE);
                $contratoTO->setId_textosistema(null);
                $contratoTO->setBl_ativo(true);
                $contratoTO->setNu_codintegracao((isset($parametros['nu_codintegracao']) ? $parametros['nu_codintegracao'] : null));

                $contratoResponsavelTO = new ContratoResponsavelTO();


                $contratoResponsavelTO->setId_usuario(
                    (isset($parametros['id_usuarioresponsavel']) && (int)$parametros['id_usuarioresponsavel']) ? $parametros['id_usuarioresponsavel'] : $usuarioPessoa["usuario"]->getid_usuario()
                );

                $contratoResponsavelTO->setNu_porcentagem(100);
                $contratoResponsavelTO->setId_tipocontratoresponsavel(TipoContratoResponsavelTO::TIPO_CONTRATO_RESPONSAVEL_PEDAGOGICO); // Pedagógico
                $contratoResponsavelTO->setBl_ativo(true);

                $contratoResponsavelFinanceiroTO = clone $contratoResponsavelTO;
                $contratoResponsavelFinanceiroTO->setId_tipocontratoresponsavel(TipoContratoResponsavelTO::TIPO_CONTRATO_RESPONSAVEL_FINANCEIRO); // Financeiro

                $arContratoResponsavelFinanceiro[] = $contratoResponsavelFinanceiroTO;

                $ro = new ContratoRO();
                $contrato = $ro->salvarContratoMatricula($contratoTO, $arContratoResponsavelFinanceiro, $contratoResponsavelTO, null, null, null, $contratoRegraTO, null);
                if ($contrato->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception('Erro ao Salvar o Contrato!');
                }

            }

            $vendaTO->fetch(true, true, true);
            if (empty($contrato)) {
                $this->mensageiro->setMensageiro(array('venda' => $vendaTO), Ead1_IMensageiro::SUCESSO);
            } else {
                $this->mensageiro->setMensageiro(array('venda' => $vendaTO, 'contrato' => $contrato->getFirstMensagem()), Ead1_IMensageiro::SUCESSO);
            }
            $dao->commit();
        } catch (Exception $e) {
            $dao->rollBack();
            $this->mensageiro->setMensageiro('Erro no cadastro de venda: ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }

        return (array)$this->mensageiro->toArrayAll();

    }


    /**
     * Busca o número de vendas cadastradas para um produto.
     * @param array $parametros
     */
    public function buscarNumeroVendasCadastradasProduto(array $parametros)
    {
        try {
            $vendaBO = new ProdutoBO();
            $vwVendaProdutoTO = new VwProdutoVendaTO();
            $vwVendaProdutoTO->setId_produto($parametros['id_produto']);
            $vwVendaProdutoTO->setBl_ativo(true);
            $mensageiro = $vendaBO->retornarVwProdutoVenda($vwVendaProdutoTO)->subtractMensageiro();
            $nu_vendas = count($mensageiro->getMensagem());
            $this->mensageiro->setMensageiro(array('nu_vendas' => $nu_vendas));
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao retornar número de vendas cadastradas: ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return (array)$this->mensageiro->toArrayAll();
    }


    /**
     * Retorna a Regra de Contrato do Produto
     * @author Desconhecido
     * @update - Elcio, refazendo
     * @update - Kayo Silva <kayo.silva@unyleya.com.br>. O código que estava nesse método foi reescrito na classe \G2\Negocio\Contrato
     * @param integer $id_produto
     * @param integer $id_entidade
     * @return Ead1_Mensageiro
     * @see LojaBO->salvarVendaBasica()
     */
    public function buscarRegraContratoProduto($id_produto, $id_entidade, $ws = false)
    {
        try {
            //instancia a negocio de contrato
            $contratoNegocio = new \G2\Negocio\Contrato();
            //retorna os dados
            return $contratoNegocio->buscarRegraContratoProduto($id_produto, $id_entidade, $ws);
        } catch (Exception $e) {
            return new Ead1_Mensageiro('Erro ao retornar a regra do contrato para o produto especificado. ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * @author Vinícius Avelino Alcântara <vinicius.alcantara@unyleya.com.br>
     * Função criada para fazer verificação dos dados da pré-matrícula
     * @params array de dados $params
     * @return true or false
     * @since 2016-04-08
     */
    public function validaPreMatricula($params)
    {
        $bo = new Ead1_BO();
        if($params)
        {
            if(!isset($params['st_email']) || empty($params['st_email']) || !$bo->validaEmail($params['st_email']))
            {
                return false;
            }
            if(isset($params['nu_telefoneresidencial']) && !empty($params['nu_telefoneresidencial']))
            {
                if(!$bo->validaTelefone($params['nu_telefoneresidencial']))
                {
                    return false;
                }
            }

            if(!isset($params['nu_telefonecelular']) || empty($params['nu_telefonecelular']) || !$bo->validaTelefone($params['nu_telefonecelular']))
            {
                return false;
            }
            if(!isset($params['produtos']['id_produto']) || empty($params['produtos']['id_produto']))
            {
                return false;
            }

        }else
        {
            return false;
        }
        return true;
    }

    public function atendenteCadastradoNaEntidade($atendente, $entidade){
        $negocio = new \G2\Negocio\Negocio();
        $retorno = $negocio->findOneBy('\G2\Entity\VwAtendente', array('id_entidade'=> $entidade,
                                                                            'id_usuario' => $atendente,
                                                                            'id_situacao' => \G2\Constante\Situacao::TB_NUCLEOFUNCIONARIOTM_ATIVO,
                                                                            'bl_ativo'=> \G2\Constante\Situacao::TB_NUCLEOFUNCIONARIOTM_BL_ATIVO) );
        if(!empty($retorno)){
            return true;
        }else{
            return false;
        }
    }

    public function cadastraAtendenteNaEntidade($atendente, $entidade){
        $negocio = new \G2\Negocio\Negocio();
        $vinculoAtendente = new \G2\Entity\NucleoFuncionarioTm();
        $data = new \DateTime('today');
        $nucleo = $negocio->findOneBy('\G2\Entity\NucleoTm', array('id_entidade' => $entidade));

        $vinculoAtendente->setId_funcao(\G2\Constante\Funcao::ATENDENTE_CM);
        $vinculoAtendente->setId_usuario($atendente);
        $vinculoAtendente->setId_nucleotm($nucleo->getId_nucleotm());
        $vinculoAtendente->setDt_cadastro($data->format('Y-m-d'));
        $vinculoAtendente->setBl_ativo(G2\Constante\Situacao::TB_NUCLEOTM_BL_ATIVO);
        $vinculoAtendente->setId_usuariocadastro(\G2\Constante\Usuario::WEBSERVICE);
        $vinculoAtendente->setId_situacao(\G2\Constante\Situacao::TB_NUCLEOFUNCIONARIOTM_ATIVO);

        $resultado = $negocio->save($vinculoAtendente);
        if($resultado){
            return true;
        }else{
            return false;
        }
    }
}

<?php
/**
 * Classe de regras de negócio para WebServices que o sistema Gestor/Portal, provê para outros sistemas
 * @author edermariano
 *
 */
class WebServiceBO extends Ead1_BO
{

    /**
     * Atributo que serve para identificar erros de autenticação
     * @var string
     */
    public $codigo;

    /**
     * Método que verifica autenticação do usuário que irá consumir o serviço
     * @param string $chave md5 do id da entidade concatenado com a palavra webServiceGestor
     * @param int $id id da entidade
     * @return boolean
     */
    public function autenticar($chave, $id)
    {
        // Trocado para DOCTRINE em 24/02/2016
        //$entidadeTO = new EntidadeTO();
        //$entidadeTO->setId_entidade($id);
        //$entidadeORM = new EntidadeORM();
        //$entidadeTO = $entidadeORM->consulta($entidadeTO, true, true);

        $negocio = new \G2\Negocio\Negocio();
        $entidade = $negocio->find('\G2\Entity\Entidade', $id);

        if (!$entidade || !$entidade->getId_entidade()) {
            $this->codigo = "Erro de entidade.";
            return false;
        } else {
            if ($entidade->getSt_wschave() != $chave) {
                $this->codigo = "Erro de chave.";
                return false;
            }

            $sessao = new Zend_Session_Namespace('geral');
            $sessao->id_entidade = $id;
            $sessao->id_usuario  = 2;
            return true;
        }
    }

    /**
     * Método que retorna os produtos do tipo projeto pedagógico para a venda
     */
    public function areaProjetos($id)
    {
        $vwProdutoProjetoTipoValorTO = new VwProdutoProjetoTipoValorTO();
        $vwProdutoProjetoTipoValorTO->setId_entidade($id);
        $vwProdutoProjetoTipoValorTO->setBl_ativo(true);

        $ro = new ProjetoPedagogicoRO();
        return $ro->retornarVwProdutoProjetoTipoValor($vwProdutoProjetoTipoValorTO)->toArrayAll();
    }

    /**
     * Método que retorna os produtos do tipo projeto pedagógico para a venda
     */
    public function meioPagamento($id)
    {
        $vwFormaMeioPagamentoTO = new VwFormaMeioPagamentoTO();
        $vwFormaMeioPagamentoTO->setId_entidade($id);
        $vwFormaMeioPagamentoTO->setId_formapagamentoaplicacao(4); // SITE

        $dao = new FormaPagamentoDAO();
        $dados = $dao->retornarVwFormaMeioPagamento($vwFormaMeioPagamentoTO);
        $mensageiro = new Ead1_Mensageiro(Ead1_TO_Dinamico::encapsularTo($dados->toArray(), new VwFormaMeioPagamentoTO()));
        return $mensageiro->toArrayAll();
    }

    /**
     * Método que cadastra vendas / Lançamentos
     */
    public function venda($id, $cpf, $produtos, $parcelas, $valorBruto, $juros = null, $valorLiquido = null)
    {
        $dao = new WebServiceDAO();
        try {
            $usuarioPessoa = $this->consultarPessoa($id, $cpf, true);
            Zend_Debug::dump($usuarioPessoa);
            exit;
            $formaPagamento = $dao->retornarFormaPagamento($id);
            if (!$formaPagamento) {
                return $this->mensageiro->setMensageiro("Forma de Pagamento não encontrada para a categoria SITE!", Ead1_IMensageiro::ERRO);
            }
            $vendaTO = new VendaTO();
            $vendaTO->setId_entidade($id);
            $vendaTO->setId_usuario($idUsuario);
            $vendaTO->setId_usuariocadastro($idUsuario);
            $vendaTO->setId_evolucao(7);
            $vendaTO->setId_situacao(46);
            $vendaTO->setBl_ativo(true);
            $vendaTO->setBl_contrato(true);

            $vendaTO->setId_formapagamento($formaPagamento->id_formapagamento); // Forma Pagamento Aplicação Venda/Site 2
            $vendaTO->setNu_descontoporcentagem(0);
            $vendaTO->setNu_descontovalor(0);
            $vendaTO->setNu_juros($juros);
            $vendaTO->setNu_parcelas($parcelas);
            $vendaTO->setNu_valorbruto($valorBruto);
            $vendaTO->setNu_valorliquido($valorLiquido);

            /*$vendaTO->setId_tipocampanha();
            $vendaTO->setId_campanhacomercial();
            $vendaTO->setNu_diamensalidade();*/

            foreach ($produtos as $produtoAtual) {
                list($idProduto, $nuDesconto, $nuValorLiquido, $nuValorBruto, $idContratoregra) = explode("|", $produtoAtual);
                if ($idContratoRegraAtual) {
                    if ($idContratoRegraAtual != $idContratoregra) {
                        return $this->mensageiro->setMensageiro("Regras de Contrato diferentes.", Ead1_IMensageiro::ERRO)->toArrayAll();
                    }

                }
                $vendaProdutoTO = new VendaProdutoTO();
                $vendaProdutoTO->setId_produto($idProduto);
                $vendaProdutoTO->setId_venda($idVenda);
                //$vendaProdutoTO->setId_campanhacomercial();
                $vendaProdutoTO->setNu_desconto($nuDesconto);
                $vendaProdutoTO->setNu_valorliquido($nuValorLiquido);
                $vendaProdutoTO->setNu_valorbruto($nuValorBruto);

                $arVendaProduto[] = $vendaProdutoTO;
            }

            $contratoTO = new ContratoTO();
            $contratoTO->setId_entidade($id);
            $contratoTO->setId_usuario($idUsuario);
            $contratoTO->setId_usuariocadastro($idUsuario);
            $contratoTO->setId_venda($idVenda);
            $contratoTO->setId_contratoregra();
            $contratoTO->setId_evolucao(1);
            $contratoTO->setId_situacao(47);
            $contratoTO->setId_textosistema(null);
            $contratoTO->setBl_ativo(true);

            $contratoResponsavelTO = new ContratoResponsavelTO();
            $contratoResponsavelTO->setId_usuario($usuarioPessoa["usuario"]->id_usuario);
            $contratoResponsavelTO->setNu_porcentagem(100);
            $contratoResponsavelTO->setId_tipocontratoresponsavel(2); // Financeiro
            $contratoResponsavelTO->setBl_ativo(true);

            $contratoResponsavelFinanceiroTO = clone $contratoResponsavelTO;
            $contratoResponsavelFinanceiroTO->setId_tipocontratoresponsavel(1); // Pedagógico

            $arContratoResponsavelFinanceiro[] = $contratoResponsavelFinanceiroTO;

            $ro = new ContratoRO();
            $ro->salvarContratoMatricula($contratoTO, $arContratoResponsavelFinanceiro, $contratoResponsavelTO, $arMatriculaTO, $arLancamentoTO, $contratoRegraTO);
        } catch (Exception $e) {

        }
    }

    /**
     * Método que consulta pessoa pelo CPF na entidade
     * @param $id
     * @param $cpf
     * @param $php valor que se setado pra true retorna o mensageiro comum
     */
    public function consultarPessoa($id, $cpf, $php = false)
    {
        $usuarioTO = new UsuarioTO();
        $usuarioTO->setSt_cpf($cpf);

        $bo = new PessoaBO();
        $usuarioTO = $bo->retornaUsuario($usuarioTO);
        if (!$usuarioTO) {
            $mensagem = new Ead1_Mensageiro("Nenhum Usuário Encontrado!", Ead1_IMensageiro::AVISO);
        } else {
            $vwPessoaTO = new VwPessoaTO();
            $vwPessoaTO->setId_usuario($usuarioTO->getId_usuario());
            $vwPessoaTO->setId_entidade($id);
            $pessoa = $bo->retornarVwPessoa($vwPessoaTO);
            if ($pessoa) {
                $dados = $pessoa;
                $pessoaTO = Ead1_TO_Dinamico::encapsularTo($dados, new VwPessoaTO(), true);
                $pessoaTO = $this->trataVwPessoaParaWebService($pessoaTO);
                $mensagem = new Ead1_Mensageiro(array("usuario" => $usuarioTO, "pessoa" => $pessoaTO), Ead1_IMensageiro::AVISO, "pessoa");
            } else {
                $mensagem = new Ead1_Mensageiro(array("usuario" => $usuarioTO, "pessoa" => "Nenhuma Pessoa Encontrada!"), Ead1_IMensageiro::AVISO, "usuario");
            }
        }

        if ($php) {
            return $mensagem;
        } else {
            return $mensagem->toArrayAll();
        }
    }

    public function trataVwPessoaParaWebService(VwPessoaTO $vwPessoaTO)
    {
        switch ($vwPessoaTO->getId_tipotelefone()) {
            case TipoTelefoneTO::CELULAR:
                $vwPessoaTO->setSt_tipotelefone('M');
                break;
            case TipoTelefoneTO::COMERCIAL:
                $vwPessoaTO->setSt_tipotelefone('C');
                break;
            default:
                $vwPessoaTO->setSt_tipotelefone('R');
                break;
        }
        switch ($vwPessoaTO->getId_estadocivil()) {
            case EstadoCivilTO::SOLTEIRO:
                $vwPessoaTO->setSt_estadocivil('S');
                break;
            case EstadoCivilTO::CASADO:
                $vwPessoaTO->setSt_estadocivil('C');
                break;
            case EstadoCivilTO::SEPARADO:
                $vwPessoaTO->setSt_estadocivil('D');
            default:
                $vwPessoaTO->setSt_estadocivil('V');
                break;
        }
        return $vwPessoaTO;
    }

    /**
     * Método que retorna as áreas do projeto
     * @param int $id
     */
    public function areas($id)
    {
        $to = new AreaConhecimentoTO();
        $to->setId_entidade($id);
        $ro = new AreaRO();

        return $ro->retornaArea($to)->toArrayAll();
    }

    /**
     * Método que retorna os projetos
     * @param int $id
     * @param int $area
     */
    public function projetos($id, $area)
    {
        $to = new AreaConhecimentoTO();
        $to->setId_areaconhecimento($area);
        $to->setId_entidade($id);
        $ro = new AreaRO();

        return $ro->retornaArvoreAreaProjetoPedagogico($to)->toArrayAll();
    }

    /**
     * Método que retorna as disciplinas
     * @param int $idEntidade
     */
    public function disciplinas($id, $projeto)
    {
        $to = new VwModuloDisciplinaProjetoTrilhaTO();
        $to->setId_projetopedagogico($projeto);

        $ro = new ProjetoPedagogicoRO();
        return $ro->retornarVwModuloDisciplinaProjetoTrilha($to)->toArrayAll();
    }

    /**
     * Método para cadastrar pessoa
     *
     * @param $id
     * @param $cpf
     * @param $nome
     * @param $dataNascimento
     * @param $sexo
     * @param $estadoCivil
     * @param $ufNascimento
     * @param $cidadeNascimento
     * @param $uf
     * @param $cidade
     * @param $estadoProvincia
     * @param $endereco
     * @param $complemento
     * @param $cep
     * @param $bairro
     * @param $numero
     * @param $ddi
     * @param $ddd
     * @param $tipoTelefone
     * @param $telefone
     * @param $descricaoTelefone
     * @param $email
     */
    public function pessoa($id,
        // Dados pessoa
                           $cpf, $nome, $dataNascimento, $sexo, $estadoCivil, $ufNascimento, $cidadeNascimento,
        // Dados Endereço
                           $uf = null, $cidade = null, $estadoProvincia = null, $endereco = null, $complemento = null, $cep = null, $bairro = null, $numero = null,
        // Dados Telefone
                           $ddi = null, $ddd = null, $tipoTelefone = null, $telefone = null, $email = null
    )
    {

        $id = (int)$id;

        $usuarioTO = new UsuarioTO();

        $usuarioTO->getSessao()->id_entidade = $id;

        $usuarioTO->setSt_cpf($cpf);
        $usuarioTO->setSt_nomecompleto($nome);

        $pessoaTO = new PessoaTO();
        $pessoaTO->setId_entidade($id);
        $pessoaTO->setId_estadocivil($this->trataEstadoCivil($estadoCivil));
        $pessoaTO->setId_municipio($cidadeNascimento);
        $pessoaTO->setSg_uf($ufNascimento);
        $pessoaTO->setSt_nomeexibicao($nome);
        $pessoaTO->setId_situacao(1);
        $pessoaTO->setId_pais(1);
        $pessoaTO->setId_usuariocadastro(1);
        $pessoaTO->setSt_sexo($sexo);
        $pessoaTO->setDt_nascimento($dataNascimento);

        $contatoTelefoneTO = new ContatosTelefoneTO();
        $contatoTelefoneTO->setId_tipotelefone($this->trataTipoTelefone($tipoTelefone));
        $contatoTelefoneTO->setNu_ddi($ddi);
        $contatoTelefoneTO->setNu_ddd($ddd);
        $contatoTelefoneTO->setNu_telefone($telefone);

        $contatoTelefonePessoaTO = new ContatosTelefonePessoaTO();
        $contatoTelefonePessoaTO->setBl_padrao(true);
        $contatoTelefonePessoaTO->setId_entidade($id);

        $contatosEmailTO = new ContatosEmailTO();
        $contatosEmailTO->setSt_email($email);

        $contatosEmailPessoaPerfilTO = new ContatosEmailPessoaPerfilTO();
        $contatosEmailPessoaPerfilTO->setBl_ativo(true);
        $contatosEmailPessoaPerfilTO->setBl_padrao(true);
        $contatosEmailPessoaPerfilTO->setId_entidade($id);

        $enderecoTO = new EnderecoTO();
        $enderecoTO->setId_municipio($cidade);
        $enderecoTO->setId_pais(1);
        $enderecoTO->setId_tipoendereco(1);
        $enderecoTO->setSg_uf($uf);
        $enderecoTO->setSt_cep($cep);
        $enderecoTO->setSt_endereco($endereco);
        $enderecoTO->setSt_bairro($bairro);
        $enderecoTO->setSt_complemento($complemento);
        $enderecoTO->setSt_estadoprovincia($estadoProvincia);
        $enderecoTO->setNu_numero($numero);

        $pessoaEnderecoTO = new PessoaEnderecoTO();
        $pessoaEnderecoTO->setBl_padrao(true);
        $pessoaEnderecoTO->setBl_padrao(true);
        $pessoaEnderecoTO->setId_entidade($id);

        $mensageiro = $this->cadastrarPessoaGeral($usuarioTO, $pessoaTO, $contatoTelefoneTO, $contatoTelefonePessoaTO, $contatosEmailTO, $contatosEmailPessoaPerfilTO, $enderecoTO, $pessoaEnderecoTO);
        return $mensageiro->toArrayAll();
    }

    private function trataEstadoCivil($estadocivil)
    {
        switch ($estadocivil) {
            case 'S':
                $id_estadocivil = EstadoCivilTO::SOLTEIRO;
                break;
            case 'C':
                $id_estadocivil = EstadoCivilTO::CASADO;
                break;
            case 'D':
                $id_estadocivil = EstadoCivilTO::SEPARADO;
                break;
            default:
                $id_estadocivil = EstadoCivilTO::VIUVO;
                break;
        }
        return $id_estadocivil;
    }

    private function trataTipoTelefone($tipotelefone)
    {
        switch ($tipotelefone) {
            case 'M':
                $id_tipotelefone = TipoTelefoneTO::CELULAR;
                break;
            case 'C':
                $id_tipotelefone = TipoTelefoneTO::COMERCIAL;
                break;
            default:
                $id_tipotelefone = TipoTelefoneTO::RESIDENCIAL;
                break;
        }
        return $id_tipotelefone;
    }

    /**
     * Método que possui a lógica para o cadastramento geral de usuários
     *
     * @param UsuarioTO $usuarioTO
     * @param PessoaTO $pessoaTO
     * @param ContatosTelefoneTO $contatoTelefoneTO
     * @param ContatosTelefonePessoaTO $contatoTelefonePessoaTO
     * @param ContatosEmailTO $contatosEmailTO
     * @param ContatosEmailPessoaPerfilTO $contatosEmailPessoaPerfilTO
     * @param EnderecoTO $enderecoTO
     * @param PessoaEnderecoTO $pessoaEnderecoTO
     * @return Ead1_Mensageiro
     */
    private function cadastrarPessoaGeral(UsuarioTO $usuarioTO, PessoaTO $pessoaTO, ContatosTelefoneTO $contatoTelefoneTO, ContatosTelefonePessoaTO $contatoTelefonePessoaTO,
                                          ContatosEmailTO $contatosEmailTO, ContatosEmailPessoaPerfilTO $contatosEmailPessoaPerfilTO, EnderecoTO $enderecoTO, PessoaEnderecoTO $pessoaEnderecoTO)
    {
        $dao = new Ead1_DAO();
        $dao->beginTransaction();
        try {
            $consultaUsuarioPessoa = $this->consultarPessoa($pessoaTO->getId_entidade(), $usuarioTO->getSt_cpf(), true);

            switch ($consultaUsuarioPessoa->getCodigo()) {
                case 'pessoa':
                    $usuarioTO = $consultaUsuarioPessoa->getCurrent();
                    $pessoaTO = $consultaUsuarioPessoa->getCurrent();
                    break;
                case 'usuario':
                    $usuarioTO = $consultaUsuarioPessoa->getCurrent();
                    $this->cadastrarPessoa($usuarioTO, $pessoaTO);
                    break;
                default:
                    $this->cadastrarUsuario($usuarioTO);
                    $this->cadastrarPessoa($usuarioTO, $pessoaTO);
                    break;
            }
            if (!empty($enderecoTO->st_endereco)) {
                $this->cadastrarEndereco($usuarioTO, $enderecoTO, $pessoaEnderecoTO);
            }
            if (!empty($contatosEmailTO->st_email)) {
                $this->cadastrarEmail($usuarioTO, $contatosEmailTO, $contatosEmailPessoaPerfilTO);
            }
            if (!empty($contatoTelefoneTO->nu_telefone)) {
                $this->cadastrarTelefone($usuarioTO, $contatoTelefoneTO, $contatoTelefonePessoaTO);
            }

            $dados['usuario'] = $usuarioTO;
            $dados['pessoa'] = $pessoaTO;
            $dados['endereco'] = $enderecoTO;
            $dados['email'] = $contatosEmailTO;
            $dados['telefone'] = $contatoTelefoneTO;

            $mensageiro = new Ead1_Mensageiro($dados);
            $dao->commit();
        } catch (Exception $e) {
            $dao->rollBack();
            $mensageiro = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, 'Exception');
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            $mensageiro = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, 'Zend_Exception');
        } catch (Zend_Db_Exception $e) {
            $dao->rollBack();
            $mensageiro = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, 'Zend_Db_Exception');
        }

        return $mensageiro;
    }

    /**
     * Método que cadastra pessoa
     * @param $usuarioTO
     * @param $pessoaTO
     * @return PessoaTO
     */
    private function cadastrarPessoa(UsuarioTO $usuarioTO, PessoaTO $pessoaTO)
    {
        $ro = new PessoaRO();
        Zend_Debug::dump($usuarioTO);
        Zend_Debug::dump($pessoaTO);
        exit;
        return $this->trataRetornoMensageiro($ro->salvarPessoa($usuarioTO, $pessoaTO), $pessoaTO);
    }

    /**
     * Método que cadastra Usuarios
     * @param UsuarioTO $usuarioTO
     * @return UsuarioTO
     */
    private function cadastrarUsuario(UsuarioTO $usuarioTO)
    {
        $ro = new PessoaRO();
        return $this->trataRetornoMensageiro($ro->salvarUsuario($usuarioTO), $usuarioTO);
    }

    /**
     * Método que trata o retorno dos mensageiros para capturar o TO desejado no mensageiro
     * @param Ead1_Mensageiro $mensageiro
     * @param Ead1_TO $to
     */
    private function trataRetornoMensageiro(Ead1_Mensageiro $mensageiro, Ead1_TO $to = null)
    {
        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            while ($mensagem = $mensageiro->getCurrent()) {
                if ($mensagem instanceOf Ead1_TO) {
                    $to = $mensagem;
                    return $to;
                } elseif (is_string($mensagem)) {
                    return $mensagem;
                }
            }
        } else {
            throw new Exception("Erro no serviço! " . get_class($to) . ", " . $mensageiro->getCurrent() . "({$mensageiro->getCodigo()})");
        }
    }

    /**
     * Método para cadastro de endereço vinculado a uma pessoa
     *
     * @param int $idEntidade
     * @param UsuarioTO $usuarioTO
     * @param EnderecoTO $enderecoTO
     * @param PessoaEnderecoTO $pessoaEnderecoTO
     * @return Ead1_Mensageiro
     */
    private function cadastrarEndereco(UsuarioTO $usuarioTO, EnderecoTO $enderecoTO, PessoaEnderecoTO $pessoaEnderecoTO)
    {
        $ro = new PessoaRO();
        return $this->trataRetornoMensageiro($ro->salvarEndereco($usuarioTO, $enderecoTO, $pessoaEnderecoTO), $enderecoTO);
    }

    /**
     * Método que cadastra emails
     * @param $usuarioTO
     * @param $contatoEmailTO
     * @param $contatoEmailPessoaPerfilTO
     * @return Ead1_Mensageiro
     */
    private function cadastrarEmail($usuarioTO, $contatoEmailTO, $contatoEmailPessoaPerfilTO)
    {
        $ro = new PessoaRO();
        return $this->trataRetornoMensageiro($ro->salvarEmail($usuarioTO, $contatoEmailTO, $contatoEmailPessoaPerfilTO), $contatoEmailTO);
    }

    /**
     * Método que cadastra telefones
     * @param $usuarioTO
     * @param $contatoTelefoneTO
     * @param $contatoTelefonePessoaTO
     * @return Ead1_Mensageiro
     */
    private function cadastrarTelefone(UsuarioTO $usuarioTO, ContatosTelefoneTO $contatoTelefoneTO, ContatosTelefonePessoaTO $contatoTelefonePessoaTO)
    {
        $ro = new PessoaRO();
        return $this->trataRetornoMensageiro($ro->salvarTelefone($usuarioTO, $contatoTelefoneTO, $contatoTelefonePessoaTO), $contatoTelefoneTO);
    }

    /**
     * Método que retorna as UFs
     */
    public function uf()
    {
        $ro = new Ead1_RO();
        return $ro->uf()->toArrayAll();
    }

    /**
     * Método que retorna os Municípios
     */
    public function municipios($uf)
    {
        $to = new MunicipioTO();
        $to->setSg_uf($uf);
        $ro = new Ead1_RO();
        return $ro->municipios($to)->toArrayAll();
    }

    /**
     * Método que retorna as escolaridades
     */
    public function escolaridade()
    {
        $ro = new Ead1_RO();
        return $ro->nivelEnsino()->toArrayAll();
    }

    /**
     * Método que retorna os tipos de contatos
     */
    public function tiposContatos()
    {
        $ro = new Ead1_RO();
        return $ro->tipoTelefone()->toArrayAll();
    }

    /**
     * Método que retorna os tipos de contatos
     */
    public function tiposEnderecos()
    {
        $to = new TipoEnderecoTO();
        $to->setId_categoriaendereco(3); //Pessoa
        $ro = new Ead1_RO();
        return $ro->tipoEndereco($to)->toArrayAll();
    }

    /**
     * Método que retorna as escolaridades
     */
    public function sexo()
    {
        $arSexo['M'] = "Masculino";
        $arSexo['F'] = "Feminino";

        $mensageiro = new Ead1_Mensageiro($arSexo);
        return $mensageiro->toArrayAll();
    }

    /**
     * Método para converter o Mensageiro em Array UTF8 para o WS
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param Ead1_Mensageiro $mensageiro
     * @return array
     */
    public function convertMensageiro(Ead1_Mensageiro $mensageiro)
    {
        return Ead1_BO::arrayUtfEncode((array)$mensageiro->toArrayAll());
    }

    private function cadastrarVendaGeral(VendaTO $vendaTO, $arVendaProduto)
    {
        $dao = new VendaDAO();
        $dao->beginTransaction();
        try {
            $ro = new VendaRO();
            $dao->cadastrarVenda($vendaTO);
            $dao->commit();
        } catch (Exception $e) {
            $dao->rollBack();

        }
    }


}
<?php

/**
 * Classe para Serviço de Cupom
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-03-21
 */
class CupomWS
{

    private $mensageiro;
    private $negocio;

    public function __construct ()
    {
        $this->mensageiro = new Ead1_Mensageiro();
        $this->negocio = new G2\Negocio\Cupom();
    }

    /**
     * Método para serviço de consulta de cupom
     * @param array $parametros
     * @return Ead1_Mensageiro
     * @throws Exception
     */
    public function consultar (array $parametros)
    {
        try {
            if (empty($parametros['st_codigocupom'])) {
                throw new Exception("st_codigocupom é um parametro obrigatório.");
            }
            $result = $this->negocio->findCuponsToWs($parametros);
            if ($result->getType() == 'success') {
                $arrResult = array();
                foreach ($result->getMensagem() as $i => $cupom) {
                    $arrResult[] = array(
                        'st_codigocupom' => $cupom->getSt_codigocupom(),
                        'nu_desconto' => $cupom->getNu_desconto(),
                        'nu_valor' => $cupom->getNu_valor(),
                        'nu_valorliquido' => $cupom->getNu_valorliquido(),
                        'id_produto' => $cupom->getId_produto()
                    );
                }
                $this->mensageiro->setMensageiro($arrResult);
            } else {
                $this->mensageiro->setMensageiro($result->getMensagem(), Ead1_IMensageiro::AVISO);
            }
        } catch (Exception $ex) {
            $this->mensageiro->setMensageiro('Erro - ' . $ex->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return (array) $this->mensageiro->toArrayAll();
    }

}

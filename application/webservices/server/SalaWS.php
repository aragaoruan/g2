<?php

/**
 * @history AC-27284
 * @description G2 - Como parceiro qureo um webservice que me retorne as salas de um moodle e seus status
 * @date 2015-10-27
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */
class SalaWS
{

    /**
     * @var \G2\Negocio\Negocio
     */
    private $negocio;

    /**
     * @var \Ead1_Mensageiro
     */
    private $mensageiro;

    public function __construct()
    {
        $this->negocio = new \G2\Negocio\SalaDeAula();
        $this->mensageiro = new \Ead1_Mensageiro();
    }

    /**
     * @param string $url_moodle
     * @param array $codcurso
     * @return Ead1_Mensageiro
     * @throws Exception
     */
    public function moodle($url_moodle, $codcurso = array())
    {
        $url_moodle = str_replace(array('http://', 'https://'), '', $url_moodle);
        $tmpArray = explode('/', $url_moodle);
        $url_moodle = array_shift($tmpArray);

        if (!$url_moodle) {
            throw new Exception('O parâmetro "url_moodle" não foi enviado corretamente.');
        }

        if ($codcurso && !is_array($codcurso)) {
            $codcurso = array($codcurso);
        }

        // BUSCA AS ENTIDADES INTEGRACAO COM O URL QUE VEIO NO PARAMETRO
        $entidades = $this->negocio->getRepository('G2\Entity\EntidadeIntegracao')
            ->retornarEntidadeIntegracaoPorUrl(\G2\Constante\Sistema::MOODLE, $url_moodle);

        if (!is_array($entidades) || !$entidades) {
            throw new Exception("Nenhum moodle com esse url encontrado.");
        }

        // CRIA O ARRAY SO COM O ID DAS ENTIDADES PRA PODER USAR NO IN() MAIS TARDE
        $arr_entidades = array_map(function ($entity) {
            return $entity->getId_entidade();
        }, $entidades);

        $results = $this->negocio->retornaSalasMoodlePorEntidades($arr_entidades, $codcurso);

        $arr_return = array();
        foreach ($results as $result) {
            $status = 1;

            $dt_abertura = $result['dt_abertura'] ? $result['dt_abertura']->format('Y-m-d') : null;
            $dt_encerramento = $result['dt_encerramento'] ? $result['dt_encerramento']->format('Y-m-d') : null;
            $nu_diasextensao = $result['nu_diasextensao'];

            if ($result['id_situacao'] != \G2\Constante\Situacao::TB_SALADEAULA_ATIVA) {
                // SE A SITUACAO NAO FOR ATIVA
                $status = 0;
            } else if (!$dt_abertura) {
                // SE NAO POSSUIR DATA DE ABERTURA
                $status = 0;
            } else {
                $today = date('Y-m-d');
                if ($today < $dt_abertura) {
                    // SE A SALA AINDA NAO FOI INICIADA
                    $status = 0;
                }

                if ($dt_encerramento) {
                    if ($nu_diasextensao) {
                        // SE POSSUIR DIAS DE EXTENSAO, SOMAR NA DATA DE ENCERRAMENTO
                        $dt_encerramento = date('Y-m-d', strtotime($dt_encerramento . " + {$nu_diasextensao} days"));
                    }

                    if ($today > $dt_encerramento) {
                        // SE A SALA JA FOI ENCERRADA
                        $status = 0;
                    }
                }
            }

            switch ($result['id_tiposaladeaula']) {
                case \G2\Constante\TipoSalaDeAula::PERMANENTE :
                    $tipo_de_sala = 'Permanente';
                    break;
                case \G2\Constante\TipoSalaDeAula::PERIODICA :
                    $tipo_de_sala = 'Periódica';
                    break;
                default:
                    $tipo_de_sala = '';
                    break;
            }

            $arr_return[] = array(
                'id_sala_moodle' => $result['st_codsistemacurso'],
                'status_da_sala' => $status,
                'data_encerramento' => $result['dt_encerramento'] ? $result['dt_encerramento']->format('Y-m-d') : '',
                'tipo_de_sala' => $tipo_de_sala
            );
        }

        $this->mensageiro->setMensageiro($arr_return);

        return $this->mensageiro;
    }

}
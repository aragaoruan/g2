<?php

/**
 * Classe de regras de negócio para Serviços relacionados a Produto
 * @author Elcio Mauro Guimarães
 * @author Kayo Silva <kayo.silva@unyleya.com.br> 2014-02-12
 * @since 02/10/2012
 */
class ProdutoWS
{

    /**
     * @var Ead1_Mensageiro
     */
    public $mensageiro;
    private $_negocio;
    private $_negocioProjeto;

    /**
     * Define se o método vai retornar um array (usado para os webservices) ou se vai retornar um objeto
     * @var unknown_type
     */
    private $_returnarray = true;

    public function __construct()
    {
        $this->mensageiro = new Ead1_Mensageiro();
        $this->_negocio = new G2\Negocio\Produto();
        $this->_negocioProjeto = new G2\Negocio\ProjetoPedagogico();
    }

    /**
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @return string
     */
    public function test()
    {
        return 'Teste do Serviço';
    }

    /**
     * Método para Listar os Produtos
     * @param array $parametros
     * @return array
     */
    public function listar(array $parametros)
    {

        $to = new VwProdutoTO();

        if ($parametros['id_entidade'] != $to->getSessao()->id_entidade) {
            $this->mensageiro->setMensageiro('Acesso inválido', Ead1_IMensageiro::ERRO);
        } else {
            try {
                $result = $this->_negocio->findByProdutoCategoria($parametros);
                $this->mensageiro->setMensageiro($result, Ead1_IMensageiro::SUCESSO);
            } catch (Exception $e) {
                $this->mensageiro->setMensageiro('Erro ao retornar os dados. Entre em contato com a EAD1.', Ead1_IMensageiro::ERRO);
            }
        }
        return (array)$this->mensageiro->toArrayAll();
    }


    /**
     * Retorna as campanhas de um produto
     * @param array $parametros
     * @return array
     */
    public function campanha(array $parametros)
    {

        try {
            $to = new VwProdutoTO();
            $ccom = new \G2\Negocio\CampanhaComercial();
            $campanha = $ccom->retornaVwCampanhaProdutoCategoria($parametros, $to->getSessao()->id_entidade);

            if (empty($campanha)) {
                throw new Exception("Nenhuma campanha encontrada");
            }

            $mensageiro = new Ead1_Mensageiro($campanha, \Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $mensageiro = new Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return (array)$mensageiro->toArrayAll();

    }

    /**
     * Retorna a URL da Ementa de um Projeto Pedagógico vinculado ao Produto
     * @param array $parametros
     * @return array
     */
    public function ementa(array $parametros)
    {

        $to = new VwProdutoTO();
        $result = array();
        if ($parametros['id_entidade'] != $to->getSessao()->id_entidade) {
            $this->mensageiro->setMensageiro('Acesso inválido', Ead1_IMensageiro::ERRO);
        } else if (!$parametros['id_produto']) {
            $this->mensageiro->setMensageiro('ID inválido', Ead1_IMensageiro::ERRO);
        } else {
            try {
                $pp = new \G2\Entity\ProdutoProjetoPedagogico();
                $pp->findOneBy(array('id_produto' => $parametros['id_produto']));
                if ($pp->getId_produtoprojetopedagogico()) {
                    if ($pp->getId_projetopedagogico()->getId_anexoementa()) {
                        if ($pp->getId_projetopedagogico()->getId_anexoementa()->getSt_upload()) {
                            $arquivo = realpath(APPLICATION_REAL_PATH . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR . 'ementa' . DIRECTORY_SEPARATOR . $pp->getId_projetopedagogico()->getId_anexoementa()->getSt_upload());
                            if (!$arquivo) {
                                throw new \Exception("O arquivo não foi encontrado");
                            } else {
                                $result = array('id_produto' => $pp->getId_produto()->getId_produto()
                                , 'id_projetopedagogico' => $pp->getId_projetopedagogico()->getId_projetopedagogico()
                                , 'st_arquivo' => '/upload/ementa/' . $pp->getId_projetopedagogico()->getId_anexoementa()->getSt_upload());
                            }
                        } else {
                            throw new \Exception("O arquivo não foi informado");
                        }
                    } else {
                        throw new \Exception("O arquivo não foi informado no Projeto Pedagógico");
                    }
                } else {
                    throw new \Exception("Não existe vínculo do Produto com um Projeto Pedagógico");
                }
                $this->mensageiro->setMensageiro($result, Ead1_IMensageiro::SUCESSO);
            } catch (\Exception $e) {
                $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
            }
        }

        return (array)$this->mensageiro->toArrayAll();
    }

    /**
     * Retorna um produto
     * @param array $parametros
     * @throws Zend_Exception
     * @return multitype:string NULL
     */
    public function retornar(array $parametros)
    {

        try {

            $pTO = new ProdutoTO();
            if (!trim($parametros['id_produto']) || empty($parametros['id_produto'])) {
                throw new Zend_Exception("Você precisa informar o id_produto nos parâmetros.");
            }

            if ($parametros['id_entidade'] != $pTO->getSessao()->id_entidade) {
                throw new Zend_Exception("Entidade inválida.");
            }

            $pTO->setId_produto($parametros['id_produto']);
            $pTO->fetch(true, true, true);

            if (!$pTO->getSt_produto()) {
                throw new Zend_Exception("Este produto não foi encontrado nessa Entidade.");
            }

            $dados = $pTO->toBackboneArray(false);

            unset($dados['id_usuariocadastro'], $dados['id_produtoimagempadrao']);
            $bo = new ProdutoBO();

            $valor = $bo->retornarProdutoValorAtivo(new ProdutoValorTO(array('id_produto' => $pTO->getId_produto(), 'id_tipoprodutovalor' => 4)));
            if ($valor->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $dadosValor = $valor->getFirstMensagem();
                $dados['valor'] = $dadosValor->toBackboneArray(false);
                unset($dados['valor']['id_produto'], $dados['valor']['id_usuariocadastro'], $dados['valor']['nu_basepropor'], $dados['valor']['dt_cadastro']);
            }

            $dados['valorpromocional'] = array();
            $valorpromocional = $bo->retornarProdutoValorAtivo(new ProdutoValorTO(array('id_produto' => $pTO->getId_produto(), 'id_tipoprodutovalor' => 7)));
            if ($valorpromocional->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $dados['valorpromocional'] = $valorpromocional->getFirstMensagem()->toBackboneArray(false);
                unset($dados['valorpromocional']['id_produto'], $dados['valorpromocional']['id_usuariocadastro'], $dados['valorpromocional']['nu_basepropor'], $dados['valorpromocional']['dt_cadastro']);
            }

            //Retornando as Entidades
            $this->_returnarray = false;
            $meentidade = $this->listarentidades(array('id_produto' => $pTO->getId_produto()));
            if ($meentidade->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $dados['entidades'] = $meentidade->getMensagem();
            }
            $this->_returnarray = true;

            //Retornando as CARREIRAS
            $this->_returnarray = false;
            $mecarreiras = $this->listarcarreiras(array('id_produto' => $pTO->getId_produto()));
            if ($mecarreiras->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $dados['carreiras'] = $mecarreiras->getMensagem();
            }
            $this->_returnarray = true;


            //Retornando as UFs
            $this->_returnarray = false;
            $meuf = $this->listarufs(array('id_produto' => $pTO->getId_produto()));
            if ($meuf->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $dados['ufs'] = $meuf->getMensagem();
            }
            $this->_returnarray = true;

            // Retornando as AREAS
            $this->_returnarray = false;
            $meareas = $this->listarareas(array('id_produto' => $pTO->getId_produto()));
            if ($meareas->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $dados['areas'] = $meareas->getMensagem();
            }
            $this->_returnarray = true;

            // Retornando CATEGORIAS
            $this->_returnarray = false;
            $mearrcat = $this->listarcategorias(array('id_produto' => $pTO->getId_produto()));
            if ($mearrcat->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $dados['categorias'] = $mearrcat->getMensagem();
            }
            $this->_returnarray = true;

            //Retornando os CONCURSOS
            $this->_returnarray = false;
            $meconcursos = $this->listarconcursosproduto(array('id_produto' => $pTO->getId_produto()));
            if ($meconcursos->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $dados['concursos'] = $meconcursos->getMensagem();
            }
            $this->_returnarray = true;

            //Retornando as TAGS
            $this->_returnarray = false;
            $metag = $this->listartags(array('id_produto' => $pTO->getId_produto()));
            if ($metag->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $dados['tags'] = $metag->getMensagem();
            }
            $this->_returnarray = true;

            //Retornando Disciplinas
            $dados['disciplinas'] = $this->retornaDisciplinasProduto($pTO->getId_produto());

            //Retornando Livros
            $dados['livros'] = $this->retornaLivrosProduto($pTO->getId_produto());
            //Coordenadores
            $dados['coordenadores'] = $this->retornaCoordenadores($pTO->getId_produto());

            //Set mensageiro
            $mensageiro = new Ead1_Mensageiro($dados, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $mensageiro = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }

        return (array)$mensageiro->toArrayAll();
    }

    private function retornaCoordenadores($idProduto)
    {
        $negocio = new G2\Negocio\Produto();
        $arrResult = array();
        $result = $negocio->retornaCoordenadorCursoByIdProduto($idProduto);
        if ($result) {
            foreach ($result as $coordenador) {
                if ($coordenador->getBl_titular()) {
                    $arrResult[] = array(
                        'id_usuario' => $coordenador->getId_usuario(),
                        'st_nomecompleto' => $coordenador->getSt_nomecompleto()
                    );
                }
            }
        }
        return $arrResult;
    }

    /**
     * Retorna Produtos Livro By id Produto
     * @param integer $idProduto
     * @return array
     */
    private function retornaLivrosProduto($idProduto)
    {
        $arrRetorno = array();
        $arrLivros = $this->_negocio->findProdutoLivroByProduto($idProduto);
        if ($arrLivros->getType() == 'success') {
            foreach ($arrLivros->getMensagem() as $i => $livro) {
                $arrRetorno[$i] = array(
                    'id_livro' => $livro->getId_livro() ? $livro->getId_livro()->getId_livro() : null,
//                    'id_usuario' => $livro->getId_livro() ? $livro->getId_livro()->getId_usuariocadastro()->getId_usuario() : null,
                    'st_isbn' => $livro->getId_livro() ? $livro->getId_livro()->getSt_isbn() : null,
                    'st_livro' => $livro->getId_livro() ? $livro->getId_livro()->getSt_livro() : null,
                    'id_tipolivro' => $livro->getId_livro() ? $livro->getId_livro()->getId_tipolivro()->getId_tipolivro() : null,
                    'st_tipolivro' => $livro->getId_livro() ? $livro->getId_livro()->getId_tipolivro()->getSt_tipolivro() : null,
                    'st_edicao' => $livro->getId_livro() ? $livro->getId_livro()->getSt_edicao() : null,
                    'nu_anolancamento' => $livro->getId_livro() ? $livro->getId_livro()->getNu_anolancamento() : null,
                    'nu_pagina' => $livro->getId_livro() ? $livro->getId_livro()->getNu_pagina() : null,
                    'st_livrocolecao' => $livro->getId_livro() ? $livro->getId_livro()->getId_livrocolecao()->getSt_livrocolecao() : null,
                    'id_livrocolecao' => $livro->getId_livro() ? $livro->getId_livro()->getId_livrocolecao()->getId_livrocolecao() : null,
                );
                //autores
                $arrRetorno[$i]['autores'] = array();
                if ($livro->getId_livro()->getAutores()) {
                    foreach ($livro->getId_livro()->getAutores() as $autor) {
                        if ($autor->getBl_autor()) {
                            $arrRetorno[$i]['autores'][] = array(
                                'st_nomecompleto' => $autor->getId_usuario()->getSt_nomecompleto()
                            );
                        }
                    }
                }
            }
        }

        return $arrRetorno;
    }

    /**
     * Retorna Disciplinas vinculadas ao produto
     * @param integer $idProduto
     * @return array
     */
    private function retornaDisciplinasProduto($idProduto)
    {
        $arrRetorno = array();
        $arrDisciplinas = $this->_negocio->findProdutoDisciplinaByProduto($idProduto);
        if ($arrDisciplinas->getType() == 'success') {
            foreach ($arrDisciplinas->getMensagem() as $disciplina) {
                $arrRetorno[] = array(
                    'id_disciplina' => $disciplina['id_disciplina'],
                    'st_disciplina' => $disciplina['st_disciplina'],
                    'nu_cargahoraria' => $disciplina['nu_cargahoraria'],
                    'id_professor' => $disciplina['id_professor'],
                    'st_professor' => $disciplina['st_professor']
                );
            }
        }
        return $arrRetorno;
    }

    /**
     * Lista os Concursos de um Produto
     * @param array $parametros
     * @throws Zend_Exception
     * @return array|Ead1_Mensageiro
     */
    public function listarconcursosproduto(array $parametros)
    {

        try {

            if (!$parametros['id_produto']) {
                throw new Zend_Exception('O id_produto é obrigatório.');
            }
            $array = array();
            $con = new \G2\Negocio\Concurso();
            $meconcursos = $con->retornarConcursosProduto($parametros['id_produto']);
            if ($meconcursos->getTipo() == Ead1_IMensageiro::SUCESSO) {
                foreach ($meconcursos->getMensagem() as $c) {
                    $arconcurso = null;
                    $arconcurso = $this->_negocio->toArrayEntity($c->getId_concurso()); // $c->getId_concurso()->entityToArray();
                    unset(
                        $arconcurso["id_entidade"],
                        $arconcurso["dt_cadastro"],
                        $arconcurso["cargos"]
                    );
                    $arconcurso['dt_prova'] = isset($arconcurso['dt_prova']) ? $arconcurso['dt_prova']['date'] : NULL;
                    $arconcurso['dt_inicioinscricao'] = isset($arconcurso['dt_inicioinscricao']) ? $arconcurso['dt_inicioinscricao']['date'] : NULL;
                    $arconcurso['dt_fiminscricao'] = isset($arconcurso['dt_fiminscricao']) ? $arconcurso['dt_fiminscricao']['date'] : NULL;
                    $array[] = $arconcurso;
                }
            }

            $mensageiro = new Ead1_Mensageiro($array, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $mensageiro = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        if ($this->_returnarray) {
            return (array)$mensageiro->toArrayAll();
        } else {
            return $mensageiro;
        }
    }

    /**
     * Lista as Categorias de um produto
     * @param array $parametros
     * @throws Zend_Exception
     * @return array|Ead1_Mensageiro
     */
    public function listarcategorias(array $parametros)
    {

        try {

            $pTO = new ProdutoTO();
            $pTO->montaToDinamico($parametros);

            if (!$pTO->getId_produto()) {
                throw new Zend_Exception('O id_produto é obrigatório');
            }

            $array = array();
            $bo = new ProdutoBO();
            $mearrcat = $bo->retornaCategoriasDoProduto($pTO);
            if ($mearrcat->getTipo() == Ead1_IMensageiro::SUCESSO) {
                foreach ($mearrcat->getMensagem() as $categoria) {
                    $catorno = null;
                    $catorno = $categoria->toBackboneArray(false);
                    unset(
                        $catorno['id_usuariocadastro'],
                        $catorno['id_entidadecadastro'],
                        $catorno['id_uploadimagem'],
                        $catorno['id_categoriapai'],
                        $catorno['id_situacao'],
                        $catorno['dt_cadastro'],
                        $catorno['bl_ativo'],
                        $catorno['id_produto'],
                        $catorno['ar_categoriasfilha']
                    );
                    $array[] = $catorno;
                }
            }
            $mensageiro = new Ead1_Mensageiro($array, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $mensageiro = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        if ($this->_returnarray) {
            return (array)$mensageiro->toArrayAll();
        } else {
            return $mensageiro;
        }
    }

    /**
     * Listar as tags de um produto
     * @param array $parametros
     * @throws Zend_Exception
     * @return array|Ead1_Mensageiro
     */
    public function listartags(array $parametros)
    {

        try {

            $pTO = new ProdutoTO();
            $pTO->montaToDinamico($parametros);

            if (!$pTO->getId_produto()) {
                throw new Zend_Exception('O id_produto é obrigatório');
            }

            $array = array();
            $bo = new ProdutoBO();

            $metag = $bo->retornarTagsDeProduto($pTO->getId_produto());
            if ($metag->getTipo() == Ead1_IMensageiro::SUCESSO) {
                foreach ($metag->getMensagem() as $tagTO) {
                    $tag = null;
                    $tag = $tagTO->toBackboneArray(false);
                    unset($tag['id_entidade'], $tag['id_usuariocadastro'], $tag['dt_cadastro'], $tag['id_produto']);
                    $array[] = $tag;
                }
            }

            $mensageiro = new Ead1_Mensageiro($array, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $mensageiro = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        if ($this->_returnarray) {
            return (array)$mensageiro->toArrayAll();
        } else {
            return $mensageiro;
        }
    }

    /**
     * Retorna as Carreiras de um Produto
     * @param array $parametros
     * @throws Zend_Exception
     * @return array|Ead1_Mensageiro
     */
    public function listarcarreiras(array $parametros)
    {

        try {

            if (!$parametros['id_produto']) {
                throw new Zend_Exception('O id_produto é obrigatório.');
            }
            $array = array();
            $prodNegocio = new \G2\Negocio\Produto();
            $mecarreiras = $prodNegocio->retornarCarreirasProduto($parametros['id_produto']);
            if ($mecarreiras->getTipo() == Ead1_IMensageiro::SUCESSO) {
                foreach ($mecarreiras->getMensagem() as $d) {
                    $arcarreira = null;
                    $arcarreira = $d->getId_carreira()->toArray();
                    unset($arcarreira['dt_cadastro']);
                    $array[] = $arcarreira;
                }
            }

            $mensageiro = new Ead1_Mensageiro($array, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $mensageiro = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        if ($this->_returnarray) {
            return (array)$mensageiro->toArrayAll();
        } else {
            return $mensageiro;
        }
    }

    /**
     * Retorna as Áreas de um Produto
     * @param array $parametros
     * @throws Zend_Exception
     * @return array
     */
    public function listarareas(array $parametros)
    {

        try {

            if (!$parametros['id_produto']) {
                throw new Zend_Exception('O id_produto é obrigatório.');
            }

            $array = array();

            $prodNegocio = new \G2\Negocio\Produto();
            $retorno = $prodNegocio->retornarAreasProduto($parametros['id_produto']);
            if ($retorno->getTipo() == Ead1_IMensageiro::SUCESSO) {
                foreach ($retorno->getMensagem() as $areaproduto) {

                    $area = null;
                    $area = $areaproduto->getId_areaconhecimento()->entityToArray();
                    unset(
                        $area['id_situacao'], $area['bl_ativo'], $area['id_entidade'], $area['dt_cadastro'], $area['id_usuariocadastro'], $area['id_areaconhecimentopai'], $area['id_tipoareaconhecimento'], $area['bl_extras']
                    );

                    $array[] = $area;
                }
            }
            $mensageiro = new Ead1_Mensageiro($array, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $mensageiro = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        if ($this->_returnarray) {
            return (array)$mensageiro->toArrayAll();
        } else {
            return $mensageiro;
        }
    }

    /**
     * Lista as UFs de um Produto
     * @param array $parametros
     * @throws Zend_Exception
     * @return array|Ead1_Mensageiro
     */
    public function listarufs(array $parametros)
    {

        try {

            if (!$parametros['id_produto']) {
                throw new Zend_Exception('O id_produto é obrigatório.');
            }

            $array = array();

            $prodNegocio = new \G2\Negocio\Produto();
            $retorno = $prodNegocio->retornarUfsProduto($parametros['id_produto']);
            if ($retorno->getTipo() == Ead1_IMensageiro::SUCESSO) {
                foreach ($retorno->getMensagem() as $uf) {

                    $entity = null;
                    $entity = $uf->getSg_uf()->entityToArray();
                    unset(
                        $entity['nu_codigocapital'], $entity['nu_codigoibge']
                    );

                    $array[] = $entity;
                }
            }
            $mensageiro = new Ead1_Mensageiro($array, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $mensageiro = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        if ($this->_returnarray) {
            return (array)$mensageiro->toArrayAll();
        } else {
            return $mensageiro;
        }
    }

    /**
     * Lista as Entidades de um Produto
     * @param array $parametros
     * @throws Zend_Exception
     * @return array|Ead1_Mensageiro
     */
    public function listarentidades(array $parametros)
    {

        try {

            if (!$parametros['id_produto']) {
                throw new Zend_Exception('O id_produto é obrigatório.');
            }

            $array = array();

            $prodNegocio = new \G2\Negocio\Produto();
            $retorno = $prodNegocio->retornarEntidadesProduto($parametros['id_produto']);
            if ($retorno->getTipo() == Ead1_IMensageiro::SUCESSO) {
                foreach ($retorno->getMensagem() as $entidade) {

                    $array[] = array(
                        'id_entidade' => $entidade->getId_entidade()->getId_entidade(),
                        'st_nomeentidade' => $entidade->getId_entidade()->getSt_nomeentidade(),
                    );
                }
            }
            $mensageiro = new Ead1_Mensageiro($array, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $mensageiro = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        if ($this->_returnarray) {
            return (array)$mensageiro->toArrayAll();
        } else {
            return $mensageiro;
        }
    }

    /**
     * Método para listar os avaliações dos produtos
     * @param array $parametros
     * @return array
     */
    public function listarPA(array $parametros)
    {

        $to = new VwProdutoAvaliacaoTO();

        if ($parametros['id_entidade'] != $to->getSessao()->id_entidade) {
            $this->mensageiro->setMensageiro('Acesso inválido', Ead1_IMensageiro::ERRO);
        } else {
            try {
                $to->montaToDinamico($parametros);
                $orm = new VwProdutoAvaliacaoORM();
                $dados = $orm->consulta($to, false, false, array('$st_avaliacao'));
                $this->mensageiro->setMensageiro($dados, Ead1_IMensageiro::SUCESSO);
            } catch (Exception $e) {
                $this->mensageiro->setMensageiro('Erro ao retornar os dados. Entre em contato com a EAD1.', Ead1_IMensageiro::ERRO);
            }
        }
        return (array)$this->mensageiro->toArrayAll();
    }

    /**
     * Método que Salva um Produto
     * @param array $parametros
     * @throws Zend_Exception
     * @return string
     */
    public function salvar(array $parametros)
    {

        $dados = array();

        $pTO = new ProdutoTO();
        if ($parametros['id_entidade'] != $pTO->getSessao()->id_entidade) {
            $this->mensageiro->setMensageiro('Acesso inválido', Ead1_IMensageiro::ERRO);
        } else {
            try {

                $pTO->montaToDinamico($parametros);
                $pTO->setBl_ativo(1);
                $pTO->setBl_todascampanhas(1);
                $pTO->setBl_todasformas(1);
                $pTO->setBl_unico(0);
                $pTO->setId_entidade($pTO->getSessao()->id_entidade);
                $pTO->setId_usuariocadastro(1);
                $pTO->setId_situacao($pTO->getId_situacao() ? $pTO->getId_situacao() : 45);

                $bo = new ProdutoBO();

                if ($pTO->getId_produto()) {

                    $pVerifica = new ProdutoTO();
                    $pVerifica->setId_produto($pTO->getId_produto());
                    $pVerifica->setId_entidade($pTO->getId_entidade());
                    $pVerifica->fetch(false, true, true);

                    if (!$pVerifica->getSt_produto()) {
                        throw new Zend_Exception("Este produto não foi encontrado nessa Entidade.");
                    }

                    $meproduto = $bo->editarProduto($pTO);
                } else {
                    $meproduto = $bo->cadastrarProduto($pTO);
                }
                if ($meproduto->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception('Produto - ' . $meproduto->getFirstMensagem());
                } else {


                    $pTO->fetch(true, true, true);
                    $dados['produto'] = $pTO->toBackboneArray();

                    $pvTO = new ProdutoValorTO();
                    $pvTO->montaToDinamico($parametros);
                    $pvTO->setId_produto($pTO->getId_produto());
                    $pvTO->setId_tipoprodutovalor(4);

                    $pvTO->setDt_inicio(new \Zend_Date());
                    $pvTO->setDt_cadastro(new \Zend_Date());
                    $pvTO->getDt_inicio()->sub('1', \Zend_Date::DAY);
                    $pvTO->setDt_termino(new \Zend_Date());
                    $pvTO->getDt_termino()->add('30', \Zend_Date::YEAR);
                    $pvTO->setId_produtovalor($pvTO->getId_produtovalor() ? $pvTO->getId_produtovalor() : null);

                    $pvTO->setId_usuariocadastro(1);

                    $arrayValores = array($pvTO);

                    //Vincula Entidade Produto
                    if (isset($parametros['entidades']) && $parametros['entidades']) {
                        $this->_negocio->vinculaEntidadeProdutoWS($parametros['entidades'], $pTO->getId_produto());
                    }
                    //Retornando as Entidades
                    $this->_returnarray = false;
                    $meentidade = $this->listarentidades(array('id_produto' => $pTO->getId_produto()));
                    if ($meentidade->getTipo() == Ead1_IMensageiro::SUCESSO) {
                        $dados['entidades'] = $meentidade->getMensagem();
                    }
                    $this->_returnarray = true;


                    if (isset($parametros['nu_valorpromocional']) && $parametros['nu_valorpromocional']) {

                        $pvpTO = clone $pvTO;
                        $pvpTO->setId_tipoprodutovalor(7);
                        $pvpTO->setNu_valor($parametros['nu_valorpromocional']);
                        $pvpTO->setDt_inicio(new Zend_Date((isset($parametros['dt_iniciopromocional']) ? $parametros['dt_iniciopromocional'] : null)));
                        $pvpTO->setDt_termino(new Zend_Date((isset($parametros['dt_fimpromocional']) ? $parametros['dt_fimpromocional'] : null)));

                        $arrayValores[] = $pvpTO;
                    }

                    foreach ($arrayValores as $pvTO) {

                        if ($pvTO->getId_produtovalor()) {

                            $pVerifica = null;
                            $pVerifica = new ProdutoValorTO();
                            $pVerifica->setId_produto($pTO->getId_produto());
                            $pVerifica->setId_produtovalor($pvTO->getId_produtovalor());

                            $pVerifica->fetch(false, true, true);

                            if (!$pVerifica->getId_usuariocadastro()) {
                                throw new Zend_Exception("Este valor não foi encontrado nesse Produto.");
                            }

                            $meprodutovalor = $bo->editarProdutoValor($pvTO);
                        } else {
                            $meprodutovalor = $bo->cadastrarProdutoValor($pvTO);
                        }

                        if ($meprodutovalor->getTipo() != Ead1_IMensageiro::SUCESSO) {
                            throw new Zend_Exception($meprodutovalor->getFirstMensagem());
                        }
                    }

                    $valor = $bo->retornarProdutoValorAtivo(new ProdutoValorTO(array('id_produto' => $pTO->getId_produto(), 'id_tipoprodutovalor' => 4)));
                    if ($valor->getTipo() == Ead1_IMensageiro::SUCESSO) {
                        $dados['valor'] = $valor->getMensagem();
                    }

                    $valorpromocional = $bo->retornarProdutoValorAtivo(new ProdutoValorTO(array('id_produto' => $pTO->getId_produto(), 'id_tipoprodutovalor' => 7)));
                    if ($valorpromocional->getTipo() == Ead1_IMensageiro::SUCESSO) {
                        $dados['valorpromocional'] = $valorpromocional->getMensagem();
                    }

                    $prodNegocio = new \G2\Negocio\Produto();

                    //UFS
                    if (isset($parametros['ufs']) && $parametros['ufs']) {
                        foreach ($parametros['ufs'] as $uf) {
                            if ($uf) {
                                $prodNegocio->salvarProdutoUf(array('sg_uf' => $uf, 'id_produto' => (int)$pTO->getId_produto()));
                            }
                        }
                    }
                    //Retornando as UFs
                    $this->_returnarray = false;
                    $meuf = $this->listarufs(array('id_produto' => $pTO->getId_produto()));
                    if ($meuf->getTipo() == Ead1_IMensageiro::SUCESSO) {
                        $dados['ufs'] = $meuf->getMensagem();
                    }
                    $this->_returnarray = true;

                    //CARREIRAS
                    if (isset($parametros['carreiras']) && $parametros['carreiras']) {
                        foreach ($parametros['carreiras'] as $carreira) {
                            if ($carreira) {
                                $prodNegocio->salvarProdutoCarreira(array('id_carreira' => (int)$carreira, 'id_produto' => (int)$pTO->getId_produto()));
                            }
                        }
                    }
                    //Retornando as CARREIRAS
                    $this->_returnarray = false;
                    $mecarreiras = $this->listarcarreiras(array('id_produto' => $pTO->getId_produto()));
                    if ($mecarreiras->getTipo() == Ead1_IMensageiro::SUCESSO) {
                        $dados['carreiras'] = $mecarreiras->getMensagem();
                    }
                    $this->_returnarray = true;


                    //AREAS
                    if (isset($parametros['areas']) && $parametros['areas']) {
                        foreach ($parametros['areas'] as $area) {
                            $retorno = $prodNegocio->salvarProdutoArea(array('id_produto' => $pTO->getId_produto(), 'id_areaconhecimento' => $area));
                        }
                    }
                    // Retornando as AREAS
                    $this->_returnarray = false;
                    $meareas = $this->listarareas(array('id_produto' => $pTO->getId_produto()));
                    if ($meareas->getTipo() == Ead1_IMensageiro::SUCESSO) {
                        $dados['areas'] = $meareas->getMensagem();
                    }
                    $this->_returnarray = true;


                    //CATEGORIAS
                    $arcategoriasProduto = array();
                    if (isset($parametros['categorias']) && $parametros['categorias']) {
                        foreach ($parametros['categorias'] as $categoria) {
                            $categoriaProdutoTO = new CategoriaProdutoTO();
                            $categoriaProdutoTO->montaToDinamico(array('id_categoria' => (int)$categoria, 'id_produto' => (int)$pTO->getId_produto()));
                            if ($categoriaProdutoTO->getId_categoria()) {
                                $bo->salvarCateroriaProduto($categoriaProdutoTO);
                            }
                        }
                    }
                    // Retornando CATEGORIAS
                    $this->_returnarray = false;
                    $mearrcat = $this->listarcategorias(array('id_produto' => $pTO->getId_produto()));
                    if ($mearrcat->getTipo() == Ead1_IMensageiro::SUCESSO) {
                        $dados['categorias'] = $mearrcat->getMensagem();
                    }
                    $this->_returnarray = true;


                    //CONCURSOS
                    $con = new \G2\Negocio\Concurso();
                    if (isset($parametros['concursos']) && $parametros['concursos']) {
                        foreach ($parametros['concursos'] as $concurso) {
                            $arconcurso['id_produto'] = (int)$pTO->getId_produto();
                            $arconcurso['id_concurso'] = (int)$concurso;
                            $con->salvarConcursoProduto($arconcurso);
                        }
                    }
                    //Retornando os CONCURSOS
                    $this->_returnarray = false;
                    $meconcursos = $this->listarconcursosproduto(array('id_produto' => $pTO->getId_produto()));
                    if ($meconcursos->getTipo() == Ead1_IMensageiro::SUCESSO) {
                        $dados['concursos'] = $meconcursos->getMensagem();
                    }
                    $this->_returnarray = true;


                    // TAGS
                    if (isset($parametros['tags']) && $parametros['tags']) {
                        $bo->cadastrarArrTagProduto($pTO->getId_produto(), $parametros['tags']);
                    }
                    //Retornando as TAGS
                    $this->_returnarray = false;
                    $metag = $this->listartags(array('id_produto' => $pTO->getId_produto()));
                    if ($metag->getTipo() == Ead1_IMensageiro::SUCESSO) {
                        $dados['tags'] = $metag->getMensagem();
                    }
                    $this->_returnarray = true;
                }

                if ($_FILES) {

                    $pi = new ProdutoImagemTO();
                    $pi->setBl_ativo(true);
                    $pi->setId_produto($pTO->getId_produto());
                    $ar = new ArquivoTO();
                    $ar->setSt_caminhodiretorio('/upload/produto');
                    $ar->setAr_arquivo($_FILES['arquivo']);
                    $ar->setSt_extensaoarquivo(substr($_FILES['arquivo']['name'], (strrpos($_FILES['arquivo']['name'], '.') + 1)));

                    $retorno = $bo->uploadImagemProduto($pi, $ar);

                    $dados['imagem'] = $ar->getSt_url();

                    $bo->salvarProdutoImagemPadrao($pi);
                }

                $this->mensageiro->setMensageiro($dados, Ead1_IMensageiro::SUCESSO);
            } catch (Exception $e) {
                $this->mensageiro->setMensageiro('Erro - ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
            }
        }
        return (array)$this->mensageiro->toArrayAll();
    }

    /**
     * Os métodos abaixo foram criados a principio para atender ao site do IMP, porém pode-se alterar os mesmos e incrementá-los.
     *
     */

    /**
     * Método para Listar os produtos para as listagens do site IMP
     * @param array $parametros
     * @return array
     */
    public function listarProduto(array $parametros = [], array $entidades = [])
    {
        return $this->_negocio->listarProdutoWs($parametros, $entidades);
    }

    /**
     * Método para Listar os concursos referentes a entidade passada como parametro
     * @param array $parametros , array $entidades
     * @return array
     */
    public function listarConcurso(array $parametros = null, array $entidades = null)
    {
        return $this->_negocio->listarConcursoWs($parametros, $entidades);
    }

    /**
     * Método para retornar um produto em específico, neste método trazemos tb o vínculo dele com outras entidades Ex: Disciplina.
     * @param array $parametros , array $entidades
     * @return array
     */
    public function retornarProduto(array $parametros = null)
    {
        return $this->_negocio->retornarProdutoWs($parametros);
    }

    /**
     * Método para listar as categorias dos produtos de uma determinada entidade
     * @param array $parametros
     * @return array
     */
    public function buscarCategoriasProduto(array $parametros = null)
    {
        return $this->_negocio->buscarCategoria($parametros);
    }

    /**
     * Método para listar os resultados das buscas no site.
     * @param array $parametros
     * @return array
     */
    public function resultadoDaBusca(array $parametros = null)
    {
        return $this->_negocio->buscarProdutos($parametros);
    }

    public function listarCursosConcursosFooter(array $parametros = null, array $entidades = null)
    {
        return $this->_negocio->listarConcursoWs($parametros, $entidades);
    }

    public function mostrarProduto(array $parametros = null)
    {
        return $this->_negocio->retornarProdutoWs($parametros);
    }


    /**
     * COM-309 Cadastro completo para produto no ambiente AEG
     * @param array $parametros
     * @return Ead1_BO
     */
    public function cadastroCompleto($id, array $parametros)
    {
        try {
            $arrReturn = array();
            $parametros['id_entidade'] = isset($parametros['id_entidade']) ? $parametros['id_entidade'] : $id;
            //Salva Produto utilizando método do Elcio
            $result = $this->salvar($parametros);
            if ($result['type'] == 'success') { //Verifica se Salvou com sucesso
                $arrReturn = array_merge($arrReturn, $result['mensagem']);

                $parametros['id_produto'] = $result['mensagem']['produto']['id_produto']; //recupera o id do produto e salva na chave do $parametro

                $resultProdInte = $this->salvaProdutoIntegracao($parametros); //Salva Produto Integração
                if ($resultProdInte['type'] == 'success') { //Verifica se salvou o Produto Integração corretamente
                    $arrReturn = array_merge($arrReturn, array("produtointegracao" => $resultProdInte['mensagem']));
                }
                $resultProjeto = $this->salvaProjetoPedagogico($parametros); //Salva ProjetoPedagogico, Produto Projeto Pedagogico e relacionamento entre Projeto Entidade
                if ($resultProjeto['type'] == 'success') {
                    //Retorna Projeto Entidades
                    $resultProjetoEntidade = $this->retornaProjetoEntidade(array(
                        'id_projetopedagogico' => $resultProjeto['mensagem']['projetopedagogico']['id_projetopedagogico']
                    ));
                    if ($resultProjetoEntidade['type'] == 'success') {
                        $arrReturn = array_merge($arrReturn, $resultProjetoEntidade['mensagem']);
                    }
                    $arrReturn = array_merge($arrReturn, $resultProjeto['mensagem']);
                    //Salva Modulos
                    if (is_array($parametros['disciplina'])) {
                        $resultModulos = $this->_negocioProjeto->salvaModuloParaCadastroCompleto($resultProjeto['mensagem'], $parametros['disciplina']);
                        if ($resultModulos->getType() == 'success') {
                            $arrReturn = array_merge($arrReturn, $resultModulos->getMensagem());
                            //Salvar Sala
                            $resultSala = $this->_negocio->salvarSalaPorDisciplinaWs($arrReturn);
                            if ($resultSala->getType() == 'success') {
                                $arrReturn = array_merge($arrReturn, array('saladeaula' => $resultSala->getMensagem()));
                            }
                        }
                    }
                }

                if ($arrReturn) {
                    $this->mensageiro->setMensageiro($arrReturn, Ead1_IMensageiro::SUCESSO);
                } else {
                    $this->mensageiro->setMensageiro($arrReturn, Ead1_IMensageiro::AVISO);
                }
            } else {
                $this->mensageiro->setMensageiro($result['mensagem'], Ead1_IMensageiro::ERRO);
            }
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao cadastrar Serviço de Projeto Pedagógico. Entre em contato com a EAD1.' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return (array)$this->mensageiro->toArrayAll();
    }

    /**
     * Retorna Projetos Entidades
     * @param array $parametros
     * @return Ead1_BO
     */
    private function retornaProjetoEntidade(array $parametros)
    {
        try {
            //Busca Projeto Entidade
            $resultProjetoEntidade = $this->_negocioProjeto->findProjetoEntidade(array(
                'id_projetopedagogico' => $parametros['id_projetopedagogico'],
            ));
            $arrRetorno = array();
            if ($resultProjetoEntidade->getType() == 'success') {
                foreach ($resultProjetoEntidade->getMensagem() as $entidade) {
                    $arrRetorno['projetoentidade'][] = array(
                        'id_projetoentidade' => $entidade->getId_projetoentidade(),
                        'id_entidade' => $entidade->getId_entidade() ? $entidade->getId_entidade()->getId_entidade() : null,
                        'st_entidade' => $entidade->getId_entidade() ? $entidade->getId_entidade()->getSt_nomeentidade() : null,
                        'id_situacao' => $entidade->getId_situacao() ? $entidade->getId_situacao()->getId_situacao() : null,
                        'st_situacao' => $entidade->getId_situacao() ? $entidade->getId_situacao()->getSt_situacao() : null,
                        'id_usuariocadastro' => $entidade->getId_usuariocadastro() ? $entidade->getId_usuariocadastro()->getId_usuario() : null,
                        'st_usuariocadastro' => $entidade->getId_usuariocadastro() ? $entidade->getId_usuariocadastro()->getSt_nomecompleto() : null,
                        'dt_inicio' => $entidade->getDt_inicio()
                    );
                }
            }
            $this->mensageiro->setMensageiro($arrRetorno, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao cadastrar Produto Interacao. Entre em contato com a EAD1.' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return (array)$this->mensageiro->toArrayAll();
    }

    /**
     * Salva Projeto Pedagogico
     * @param array $parametros
     * @return Ead1_BO
     * @throws Zend_Exception
     */
    private function salvaProjetoPedagogico(array $parametros = array())
    {
        try {
            if (!$parametros['id_produto']) {
                throw new Zend_Exception("id_produto undefined to save Projeto Pedagogico");
            } else {
                $to = new ProjetoPedagogicoTO();
                $eTo = new EntidadeTO();
                $to->montaToDinamico($parametros);
                $to->setBl_ativo(1);
                $to->setBl_autoalocaraluno(true);
                $to->setBl_provafinal(false);
                $to->setBl_salasemprovafinal(false);
                $to->setBl_disciplinaextracurricular(false);
                $to->setBl_turma(false);
                $to->setId_entidadecadastro($parametros['id_entidade']);
                $ro = new ProjetoPedagogicoRO();

                //Procura projeto
                $procProjeto = $this->_negocioProjeto->findOneProjetoPedagogico($to->toArray());
                if ($procProjeto) {
                    $to->setId_projetopedagogico($procProjeto->getId_projetopedagogico());
                }

                //Salva Projeto Pedagogico
                if ($to->getId_projetopedagogico()) {
                    $mensageiro = $ro->editarProjetoPedagogico($to);
                } else {
                    $mensageiro = $ro->cadastrarProjetoPedagogico($to);
                }

                $arrProdPedagogico['id_produto'] = $parametros['id_produto'];
                $arrProdPedagogico['id_entidade'] = $parametros['id_entidade'];
                $arrRetorno = array();

                if ($mensageiro->getType() == 'success') {
                    $resultProjeto = $this->_negocioProjeto->findOneProjetoPedagogico($to->toArray());
                    if ($resultProjeto) {
                        $projetoPedagogico = $this->_negocio->toArrayEntity($resultProjeto);
                        $arrRetorno['projetopedagogico'] = $projetoPedagogico;
                        $arrProdPedagogico['id_projetopedagogico'] = $projetoPedagogico['id_projetopedagogico'];
                    }

                    //Salva produto projeto pedagogico
                    if ($arrProdPedagogico['id_projetopedagogico']) {
                        $result = $this->_negocio->salvarProdutoProjetoPedagogico($arrProdPedagogico);
                        $resultProduto = $this->_negocio->findProdutoProjetoPedagogicoByArray(array(
                            'id_projetopedagogico' => $arrProdPedagogico['id_projetopedagogico'],
                            'id_entidade' => $arrProdPedagogico['id_entidade'],
                        ));
                        if ($resultProduto->getType() == 'success') {
                            foreach ($resultProduto->getMensagem() as $produto) {
                                $arrRetorno['produtoprojetopedagogico'][] = $produto;
                            }
                        }
                        $this->mensageiro->setMensageiro($arrRetorno, Ead1_IMensageiro::SUCESSO);
                    }
                } else {
                    $this->mensageiro->setMensageiro($mensageiro->getText(), Ead1_IMensageiro::ERRO);
                }
            }
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao cadastrar Projeto Pedagógico. Entre em contato com a EAD1.' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return (array)$this->mensageiro->toArrayAll();
    }

    /**
     * Salva Vinculo de Produto com Produto Intereação
     * @param array $parametros
     * @return Ead1_Mensageiro
     * @throws Zend_Exception
     */
    private function salvaProdutoIntegracao(array $parametros)
    {
        try {
            if (!$parametros['id_produto']) {
                throw new Zend_Exception("id_produto undefined to save Produto Integracao");
            } else {
                //Recupra os dados que veio via parametro e seta somentes os referentes a Produto Integraçao
                $dados = array(
                    'id_produtointegracao' => (isset($parametros['id_produtointegracao']) ? $parametros['id_produtointegracao'] : null),
                    'id_produto' => (isset($parametros['id_produto']) ? $parametros['id_produto'] : null),
                    'id_sistema' => (isset($parametros['id_sistema']) ? $parametros['id_sistema'] : null),
                    'nu_codprodutosistema' => (isset($parametros['nu_codprodutosistema']) ? $parametros['nu_codprodutosistema'] : null),
                );
                //Chama a negocio para salvar Produto Integração
                $result = $this->_negocio->salvaProdutoIntegracao($dados);
                $arrReturn = array();
                //Pega o Resultado
                if ($result->getType() == 'success') {
                    //Percorre o resultado
                    foreach ($result->getMensagem() as $row) {
                        $arrReturn[] = array(
                            'id_produtointegracao' => $row->getId_produtointegracao(),
                            'id_produto' => $row->getId_produto() ? $row->getId_produto()->getId_produto() : null,
                            'id_sistema' => $row->getId_sistema() ? $row->getId_sistema()->getId_sistema() : null,
                            'nu_codprodutosistema' => $row->getNu_codprodutosistema() ? $row->getNu_codprodutosistema() : null,
                        );
                    }
                    $this->mensageiro->setMensageiro($arrReturn, Ead1_IMensageiro::SUCESSO);
                } else {
                    $this->mensageiro->setMensageiro($result->getText(), Ead1_IMensageiro::ERRO);
                }
            }
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao cadastrar Produto Interacao. Entre em contato com a EAD1.' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return (array)$this->mensageiro->toArrayAll();
    }
}

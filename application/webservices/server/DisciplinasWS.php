<?php

/**
 * Class DisciplinasWS
 * @autor: Débora Castro
 * 05/09/2013
 * @author Kayo Silva <kayo.silva@unyleya.com.br> 2014-01-13
 */
class DisciplinasWS
{

    /**
     * @var Ead1_Mensageiro
     */
    public $mensageiro;
    /** @var  \G2\Negocio\Disciplina $negocio */
    private $negocio;

    public function __construct ()
    {
        $this->mensageiro = new Ead1_Mensageiro();
        $this->negocio = new G2\Negocio\Disciplina();
    }

    /**
     * Método para Listar Disciplinas por Entidade (de acordo com cadastro de salas)
     * @param array $parametros
     * @return array
     */
    public function listar (array $parametros)
    {

        try {
            if (!isset($parametros['id_entidade'])) {
                throw new Exception("id_entidade é um parametro obrigatório.");
            }
            $result = $this->negocio->findByEntidadeSessao($parametros, array('st_disciplina' => 'ASC'));
            $arrReturn = array();
            if ($result) {
                foreach ($result as $row) {
                    $arrReturn[] = array(
                        'id_disciplina' => $row->getId_entidade(),
                        'st_disciplina' => $row->getSt_disciplina(),
                        'st_tituloexibicao' => $row->getSt_tituloexibicao(),
                        'st_descricao' => $row->getSt_Descricao(),
                        'nu_cargahoraria' => $row->getNu_cargahoraria(),
                        'st_tituloexibicao' => $row->getSt_tituloexibicao(),
                        'st_ementacertificado' => $row->getSt_ementacertificado(),
                        'id_tipodisciplina' => $row->getId_tipodisciplina(),
                        'st_situacao' => $row->getId_situacao()->getSt_situacao()
                    );
                }
            }
            $this->mensageiro->setMensageiro($arrReturn, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao retornar os dados. Entre em contato com a EAD1. ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }

        return (array) $this->mensageiro->toArrayAll();
    }

    /**
     * Me´todo para salvar Disciplina
     * @param array $parametros array de dados
     * @return Ead1_Mensageiro
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     */
    public function salvar ($id, array $parametros)
    {
        try {
            $valid = true;
            $id = (int) $id; //Recupera o id da entidade
            $to = new DisciplinaTO(); // Instancia a TO de Disciplina
            $to->getSessao()->id_entidade = $id; //atribui o id da entidade na sessão
            //Monta TO com parametros
            $to->montaToDinamico($parametros);
            $to->setId_entidade($id); //Seta o id da entidade
            $to->setDt_cadastro(new DateTime());

            //Inicia as verificações
            if (!$to->getSt_disciplina()) {
                $valid = false;
                $this->mensageiro->setMensageiro('st_disciplina é de preenchimento obrigatório!', Ead1_IMensageiro::ERRO);
            }
            if (!$to->getNu_cargahoraria()) {
                $valid = false;
                $this->mensageiro->setMensageiro('nu_cargahoraria é de preenchimento obrigatório!', Ead1_IMensageiro::ERRO);
            }
            if (!$to->getBl_compartilhargrupo()) {
                $valid = false;
                $this->mensageiro->setMensageiro('bl_compartilhargrupo é de preenchimento obrigatório!', Ead1_IMensageiro::ERRO);
            }
            if (!$to->getId_situacao()) {
                $valid = false;
                $this->mensageiro->setMensageiro('id_situacao é de preenchimento obrigatório!', Ead1_IMensageiro::ERRO);
            }
            if (!$to->getId_tipodisciplina()) {
                $valid = false;
                $this->mensageiro->setMensageiro('id_tipodisciplina é de preenchimento obrigatório!', Ead1_IMensageiro::ERRO);
            }
            if (!$to->getId_usuariocadastro()) {
                $valid = false;
                $this->mensageiro->setMensageiro('id_usuariocadastro é de preenchimento obrigatório!', Ead1_IMensageiro::ERRO);
            }
            //Se a variavel $valid setada como true no inicio do código não foi alterada para false tenta persistir no banco os dados
            if ($valid) {
                $disciplinaBO = new DisciplinaBO();
                $result = $this->negocio->salvaDisciplina($to->toArray());

                if ($result) { // Recupera o retorno
                    $this->mensageiro->setMensageiro(Ead1_IMensageiro::SUCESSO, Ead1_IMensageiro::SUCESSO);

                    //Salva área conhecimento
                    $areaConhecimento = $this->salvaAreaConhecimento($parametros, $result);
                    if ($areaConhecimento) {
                        $this->mensageiro->addMensagem($areaConhecimento);
                    }

                    //Salva Coordenador
                    if (isset($parametros['coordenador'])) {
                        $coordenadorDisciplina = $this->salvaCoordenadorDisciplina($id, $parametros['coordenador'], $result);
                        if ($coordenadorDisciplina) {
                            $this->mensageiro->addMensagem($coordenadorDisciplina);
                        }
                    }
                    //Salva Professor
                    if (isset($parametros['professor'])) {
                        $professorDisciplina = $this->salvaProfessorDisciplina($id, $parametros['professor'], $result);
                        if ($professorDisciplina) {
                            $this->mensageiro->addMensagem($professorDisciplina);
                        }
                    }
                } else {
                    $this->mensageiro->setMensageiro($result, Ead1_IMensageiro::ERRO);
                }
            }
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao retornar cadastrar Disciplina. Entre em contato com a EAD1.', Ead1_IMensageiro::ERRO);
        }
        return (array) $this->mensageiro->toArrayAll();
    }

    /**
     * Salva Dados de coordenador join disciplina
     * @param array $data Array de dados postado para salvar com chave coordenador
     * @param array $disciplina Dados da Disciplina que foi salva
     * @return array Dados do retorno do mensageiro
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     */
    private function salvaCoordenadorDisciplina ($id, array $data, $disciplina)
    {
        $bo = new ProjetoPedagogicoBO(); //instancia a bo de projeto pedagogico
        $perfil = $this->negocio->retornaPerfilDisciplinaByEntidadeSessao(array(
            'id_perfilpedagogico' => 4,
            'id_situacao' => 4,
            'id_entidade' => $id
        )); // Recupera o id do perfil de coordenador
        $arrCoordenador = array();
        $arrReturn = array();
        //Verifica se o array que foi submetiddo existe a chave de id_usuario
        if ($data) {
            //Percorre o array de id_usuario
            foreach ($data as $key => $usuario) {
                //verifica se o laço atual é um array, isso siginifca que existe valores nos registros
                if (is_array($usuario)) {
                    //percorre o segundo array que são os dados em si
                    foreach ($usuario as $i => $dado) {
                        if ($dado) {
//                        //cria um novo array para setar na TO
                            $arrCoordenador[$i][$key] = $data[$key][$i];
                            //Cria chaves fixas no array
                            $arrCoordenador[$i]['id_perfil'] = $perfil->getId_perfil(); //id_perfil
                            //Recupera os dados da disciplina que foi salva e seta o id_disicplina em uma nova chave do array dinamico
                            $arrCoordenador[$i]['id_disciplina'] = $disciplina->getId_disciplina();
                        }
                    }
                }
            }
            if ($arrCoordenador) {
                foreach ($arrCoordenador as $arrSave) {
                    $to = new UsuarioPerfilEntidadeReferenciaTO(); //instancia a TO
                    $to->montaToDinamico($arrSave); //Monta a To com os valores
                    $mensageiro = $bo->salvarCoordenadorUnico($to); //Chama o metodo da BO para salvar coordenador
                    $arrReturn['UsuarioPerfilEntidadeReferencia'][] = $mensageiro->getMensagem(); //Atribui a mensagem do retorno do metodo a um novo array
                }
            }
        }
        return $arrReturn;
    }

    /**
     * Salva Dados de professor join disciplina
     * @param array $data Array de dados postado para salvar com chave professor
     * @param array $disciplina Dados da Disciplina que foi salva
     * @return array Dados do retorno do mensageiro
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     */
    private function salvaProfessorDisciplina ($id, array $data, $disciplina)
    {
        $bo = new ProjetoPedagogicoBO(); //instancia a bo de projeto pedagogico
        $negocio = new \G2\Negocio\Disciplina(); //instancia a negocio de disciplina
        $perfil = $negocio->retornaPerfilDisciplinaByEntidadeSessao(array(
            'id_perfilpedagogico' => 1,
            'id_situacao' => 4,
            'id_entidade' => $id
        )); // Recupera o id do perfil de coordenador
        $arrCoordenador = array();
        $arrReturn = array();
        //Verifica se o array que foi submetiddo existe a chave de id_usuario
        if ($data) {
            //Percorre o array de id_usuario
            foreach ($data as $key => $usuario) {
                //verifica se o laço atual é um array, isso siginifca que existe valores nos registros
                if (is_array($usuario)) {
                    //percorre o segundo array que são os dados em si
                    foreach ($usuario as $i => $dado) {
                        if ($dado) {
//                        //cria um novo array para setar na TO
                            $arrCoordenador[$i][$key] = $data[$key][$i];
                            //Cria chaves fixas no array
                            $arrCoordenador[$i]['id_perfil'] = $perfil->getId_perfil(); //id_perfil
                            //Recupera os dados da disciplina que foi salva e seta o id_disicplina em uma nova chave do array dinamico
                            $arrCoordenador[$i]['id_disciplina'] = $disciplina->getId_disciplina();
                        }
                    }
                }
            }
            if ($arrCoordenador) {
                foreach ($arrCoordenador as $arrSave) {
                    $to = new UsuarioPerfilEntidadeReferenciaTO(); //instancia a TO
                    $to->montaToDinamico($arrSave); //Monta a To com os valores
                    $mensageiro = $bo->salvarCoordenadorUnico($to); //Chama o metodo da BO para salvar coordenador
                    $arrReturn['UsuarioPerfilEntidadeReferencia'][] = $mensageiro->getMensagem(); //Atribui a mensagem do retorno do metodo a um novo array
                }
            }
        }
        return $arrReturn;
    }

    /**
     * Salva dados de área conhecimento join disciplina
     * @param array  Parametros Array de dados submetido
     * @param array $disciplina Dados da disciplina salva
     * @return Ead1_Mensageiro/array
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     */
    private function salvaAreaConhecimento (array $data, $disciplina)
    {
        $negocio = new \G2\Negocio\Disciplina();
        $arrAreaConhecimento = array();
        $arrReturn = array();
        //Salva área conhecimento
        if (isset($data['id_areaconhecimento'])) {//Recupera os dados do array na chave id_areaconhecimento
            //Mescla os dados de area_conhecimento com os dados de disciplina que foi salvo
            $arrAreaConhecimento = array_merge(array('id_areaconhecimento' => $data['id_areaconhecimento']), $this->negocio->toArrayEntity($disciplina));
            return $this->negocio->salvaArrAreaJoinDisciplina($arrAreaConhecimento); // Envia para negocio para persisitir
        }
    }

}

<?php

/**
 * Classe para Serviço de Campanha Premio
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2015-07-08
 */
class CampanhaPremioWS
{

    private $mensageiro;
    private $negocio;

    public function __construct()
    {
        $this->mensageiro = new Ead1_Mensageiro();
        $this->negocio = new G2\Negocio\CampanhaComercial();
    }

    /**
     * Método para retornar consulta de campanhas premio
     * @param array $parametros
     * @return array
     */
    public function consultar(array $parametros, $id)
    {
        try {
            //verifica se foi passado o parametro corretamente
            if (!array_key_exists('produtos', $parametros) && !$parametros['produtos']) {
                throw new Exception("Informe o parametro com os id's dos produtos.");
            }
            $result = $this->negocio->retornarConsultaWsCampanhaPremio($parametros['produtos'], $id);
            if ($result) {
                $this->mensageiro->setMensageiro($result, Ead1_IMensageiro::SUCESSO);
            } else {
                $this->mensageiro->setMensageiro("Nenhum produto.", Ead1_IMensageiro::AVISO);
            }

        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao retornar os dados. Entre em contato com a EAD1. ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return (array)$this->mensageiro->toArrayAll();
    }

}
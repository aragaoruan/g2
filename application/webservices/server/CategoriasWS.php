<?php

/**
 * Classe para serviço de Categorias
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2013-01-20
 */
class CategoriasWS
{

    /**
     * @var Ead1_Mensageiro
     */
    public $mensageiro;

    /**
     * @var CategoriaBo 
     */
    private $bo;

    public function __construct ()
    {
        $this->mensageiro = new Ead1_Mensageiro();
        $this->bo = new CategoriaBO();
    }

    /**
     * Verifica se existe categoria existe e salva ou edita
     * @param integer $id
     * @param array $parametros
     * @return Ead1_Mensageiro
     * @throws Zend_Exception
     */
    public function salvar ($id, array $parametros)
    {
        try {
            $to = new CategoriaTO(); //Instancia a TO
            $to->montaToDinamico($parametros); //Seta os atributos da TO
            $to->setId_entidadecadastro($id); //Seta o id_entidadecadastro com o id que autenticação do WS
            //Verifica se os valores abaixo foram setados
            if (!$to->getSt_categoria()) {
                throw new Zend_Exception("st_categoria é de preenchimento obrigatório!");
            }
            if (!$to->getSt_nomeexibicao()) {
                throw new Zend_Exception("st_nomeexibicao é de preenchimento obrigatório!");
            }
            if (!$to->getId_usuariocadastro()) {
                throw new Zend_Exception("id_usuariocadastro é de preenchimento obrigatório!");
            }
            //Consulta a categoria pelo valores setados na TO
            $categorias = $this->bo->retornarCategoria($to);
            if ($categorias->getMensagem()) {//Verifica se veio registro
                foreach ($categorias->getMensagem() as $categoria) { //Percorre os registros e edita os que encontrou
                    $to->setId_categoria($categoria->getId_categoria()); //Set id_categoria
                    $this->mensageiro = $this->bo->salvarCategoria($to); //Salva
                }
            } else {//senão encontrou registro salva um novo
                $this->mensageiro = $this->bo->salvarCategoria($to);
            }
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro - ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return (array) $this->mensageiro->toArrayAll();
    }

    /**
     * Método para listar categorias
     * @param array $parametros
     * @return Ead1_Mensageiro
     * @throws Zend_Exception
     */
    public function listar ($id, array $parametros)
    {
        try {
            $to = new CategoriaTO();
            if ($parametros) {
                $arrReturn = array();
                $to->montaToDinamico($parametros);
                $to->setId_entidadecadastro(isset($parametros['id_entidade']) ? $parametros['id_entidade'] : $id);
                $cateogrias = $this->bo->retornarCategoria($to);
                if ($cateogrias->getMensagem()) {
                    foreach ($cateogrias->getMensagem() as $categoria) {
                        $date = $categoria->getDt_cadastro();
                        $arrReturn[] = array(
                            'id_categoria' => $categoria->getId_categoria(),
                            'st_nomeexibicao' => $categoria->getSt_nomeexibicao(),
                            'st_categoria' => $categoria->getSt_categoria(),
                            'id_entidadecadastro' => $categoria->getId_entidadecadastro(),
                            'id_uploadimagem' => $categoria->getId_uploadimagem(),
                            'id_situacao' => $categoria->getId_situacao(),
                            'dt_cadastro' => $date->toString('Y-M-d h:m:s'),
                            'bl_ativo' => $categoria->getBl_ativo()
                        );
                    }
                    $this->mensageiro->setMensageiro($arrReturn, Ead1_IMensageiro::SUCESSO);
                } else {
                    $this->mensageiro->setMensageiro('Sem resultado para exibição.', Ead1_IMensageiro::AVISO);
                }
            }
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro - ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return (array) $this->mensageiro->toArrayAll();
    }

}
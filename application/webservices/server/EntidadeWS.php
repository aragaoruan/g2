<?php

/**
 * Classe de regras de negócio para Serviços relacionados a Entidade
 * @author Elcio Mauro Guimarães
 * @author Kayo Silva <kayo.silva@unyleya.com.br> 2014-02-05
 * @since 30/08/2012
 */
class EntidadeWS
{
    /**
     * @var Ead1_Mensageiro
     */
    public $mensageiro;


    public function __construct()
    {
        $this->mensageiro = new Ead1_Mensageiro();
    }

    /**
     * Método para cadastrar Entidade
     * @param array $parametros
     */
    public function cadastrar(array $parametros)
    {

        $mensageiro = new Ead1_Mensageiro();
        $dao = new EntidadeDAO();
        $dao->beginTransaction();
        try {


            $eTO = new EntidadeTO();
            $eTO->montaToDinamico($parametros);
            if (!$eTO->getSt_nomeentidade() && !$eTO->getSt_razaosocial()) {
                throw new Zend_Exception('O Nome ou a Razão Social da entidade são obrigatíorios!');
            }

            $bo = new EntidadeBO();
            $meentidade = $bo->cadastrarEntidade($eTO);
            if ($meentidade->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Exception($meentidade->getFirstMensagem());
            }

            $eTO = $meentidade->getFirstMensagem();
            if ($eTO instanceof EntidadeTO) {
                $eTO->fetch(true, true, true);
            } else {
                throw new Zend_Exception('O resultado do cadastro da entidade, veio errado!');
            }

            if (!empty($parametros['st_conta'])) {
                $ceTO = new ContaEntidadeTO();
                $ceTO->montaToDinamico($parametros);
                $ceTO->setId_entidade($eTO->getId_entidade());
                $ceTO->setBl_padrao(true);
                $mece = $bo->cadastrarContaEntidade($ceTO);
                if ($mece->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception($mece->getFirstMensagem());
                }
            }


            if (!empty($parametros['nu_telefone'])) {
                $ctTO = new ContatosTelefoneTO();
                $ctTO->montaToDinamico($parametros);

                $cteTO = new ContatosTelefoneEntidadeTO();
                $cteTO->setBl_padrao(true);
                $mete = $bo->cadastrarTelefoneEntidade($eTO, $ctTO, $cteTO);
                if ($mete->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception($mete->getFirstMensagem());
                }
            }

            if (!empty($parametros['st_endereco'])) {
                $endTO = new EnderecoTO();
                $endTO->montaToDinamico($parametros);
                $eeTO = new EntidadeEnderecoTO();
                $eeTO->setBl_padrao(true);
                $eeTO->setId_entidade($eTO->getId_entidade());
                $meee = $bo->cadastrarEnderecoEntidade($eTO, $endTO, $eeTO);
                if ($meee->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception($meee->getFirstMensagem());
                }
            }

            if (!empty($parametros['st_email'])) {
                $eeTO = new EntidadeEmailTO();
                $eeTO->setId_entidade($eTO->getId_entidade());
                $eeTO->setSt_email($parametros['st_email']);
                $meee = $bo->cadastrarEntidadeEmail($eeTO);
                if ($meee->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception($meee->getFirstMensagem());
                }
            }

            $dao->commit();
            $mensageiro->setMensageiro($eTO, Ead1_IMensageiro::SUCESSO);
            return Ead1_BO::arrayUtfEncode((array)$mensageiro->toArrayAll());
        } catch (Exception $e) {
            $dao->rollBack();
            $mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
            return (array)$mensageiro->toArrayAll();
        }
    }

    /**
     * Metodo para serviço de listar unidades
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     * @param integer $id
     * @param array $parametros
     * @return Ead1_BO
     * @throws Exception
     */
    public function listarunidades($id, array $parametros)
    {
        $mensageiro = new Ead1_Mensageiro();
        $negocio = new \G2\Negocio\Entidade();
        try {
            if ($this->validaParametrosListaUnidades($parametros)) {
                $result = $negocio->listarUnidades($parametros);
                if ($result) {
                    $mensageiro->setMensageiro($result, Ead1_IMensageiro::SUCESSO);
                } else {
                    $mensageiro->setMensageiro(Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO,
                        Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO);
                }
            }
        } catch (Exception $e) {
            $mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return (array)$mensageiro->toArrayAll();
    }

    /**
     * Valida parametros passados para servico de listaunidades
     * @param array $parametros
     * @return boolean
     * @throws Zend_Exception
     * @throws Exception
     */
    private function validaParametrosListaUnidades(array $parametros)
    {
        try {
            $allowParams = array('id_entidadecadastro');
            foreach ($allowParams as $allow) {
                if (!array_key_exists($allow, $parametros)) {
                    throw new Exception("O parametro {$allow} deve ser passado");
                }
                if (empty($parametros[$allow])) {
                    throw new Exception("O parametro {$allow} não pode ser vazio");
                }
            }
            return true;
        } catch (Exception $e) {
            throw new Zend_Exception($e->getMessage());
        }
    }

    /**
     * Método que retorna as entidades filhas. Estas entidades filhas são chamadas de polos na Graduação
     * @author Arthur Luiz Lara Quites
     * @param array $parametros
     * @return array
     */
    public function recuperarEntidadesFilhas(array $parametros)
    {
        //verifica se o parametro que indica a entidade pai foi setado, caso contrário retorna erro
        if (!empty($parametros['id_entidadeg2'])) {
            $entidade = new \G2\Negocio\Entidade();
            try {
                $retorno['polos'] = $entidade->findBy('\G2\Entity\VwEntidadeRelacao',
                    array('id_entidadepai' => $parametros['id_entidadeg2']));
            } catch (Zend_Exception $e) {
                $this->mensageiro->setMensageiro("Não foi possível recuperar as entidades do banco de dados.",
                    Ead1_IMensageiro::ERRO);
            }

            //caso ñ exista entidades filhas, retorna erro, do contrário, retorna um array com as entidades filhas
            if (empty($retorno['polos'])) {
                $this->mensageiro->setMensageiro('Nenhuma entidade filha/polo encontrada.', Ead1_IMensageiro::ERRO);
            } else {
                $this->mensageiro->setMensageiro($entidade->toArrayEntity($retorno), Ead1_IMensageiro::SUCESSO);
            }
        } else {
            $this->mensageiro->setMensageiro('Informe o parâmetro id_entidadeg2.', Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro->toArrayAll();
    }

    /**
     * Método que retorna as entidades filhas com seus respectivos projetos pedagógicos cadastrados.
     * @author Arthur Luiz Lara Quites
     * @param array $parametros
     * @return array
     */
    public function recuperarEntidadesFilhasComProjetoPedagogico(array $parametros)
    {
        if (!empty($parametros['id_entidadecadastro'])) {
            $entidade = new \G2\Negocio\Entidade();
            try {
                $dados = $entidade->findBy('\G2\Entity\VwRecuperaPolosComProjetoPedagogico',
                    array('id_entidadecadastro' => $parametros['id_entidadecadastro']));
            } catch (Zend_Exception $e) {
                $this->mensageiro->setMensageiro("Não foi possível recuperar as entidades do banco de dados.",
                    Ead1_IMensageiro::ERRO);
            }
            $retorno = array();
            if (!empty($entidade->toArrayEntity($dados))) {
                foreach ($entidade->toArrayEntity($dados) as $key) {
                    if (array_key_exists($key['id_entidade'], $retorno)) {
                        array_push($retorno[$key['id_entidade']], array(
                            'id_entidade' => $key['id_entidade'],
                            'st_nomeentidade' => $key['st_nomeentidade'],
                            'id_projetopedagogico' => $key['id_projetopedagogico'],
                            'st_projetopedagogico' => $key['st_projetopedagogico']
                        ));
                    } else {
                        $retorno[$key['id_entidade']] = array(
                            'id_entidade' => $key['id_entidade'],
                            'st_nomeentidade' => $key['st_nomeentidade'],
                            'id_projetopedagogico' => $key['id_projetopedagogico'],
                            'st_projetopedagogico' => $key['st_projetopedagogico']
                        );
                    }
                }
            }
            $this->mensageiro->setMensageiro($retorno, Ead1_IMensageiro::SUCESSO);
        } else {
            $this->mensageiro->setMensageiro('Informe o parâmetro id_entidadecadastro.', Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro->toArrayAll();
    }
}

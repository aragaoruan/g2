<?php

class TiltSMS_TO_VerificaSMSTO extends TiltSMS_TO_TiltSMSTO {

	public $st_usuario;
	public $st_senha;
	public $nu_quantidade;
	/**
	 * @return the $st_usuario
	 */
	public function getSt_usuario() {
		return $this->st_usuario;
	}

	/**
	 * @return the $st_senha
	 */
	public function getSt_senha() {
		return $this->st_senha;
	}

	/**
	 * @return the $nu_quantidade
	 */
	public function getNu_quantidade() {
		return $this->nu_quantidade;
	}

	/**
	 * @param field_type $st_usuario
	 */
	public function setSt_usuario($st_usuario) {
		$this->st_usuario = $st_usuario;
	}

	/**
	 * @param field_type $st_senha
	 */
	public function setSt_senha($st_senha) {
		$this->st_senha = $st_senha;
	}

	/**
	 * @param field_type $nu_quantidade
	 */
	public function setNu_quantidade($nu_quantidade) {
		$this->nu_quantidade = $nu_quantidade;
	}

	
	

}
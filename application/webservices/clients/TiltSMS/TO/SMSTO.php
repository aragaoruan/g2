<?php

class TiltSMS_TO_SMSTO extends TiltSMS_TO_TiltSMSTO {

	public $st_usuario;
	public $st_senha;
	public $st_mensagem;
	public $ar_destinatarios;
	
	
	/**
	 * @return the $st_usuario
	 */
	public function getSt_usuario() {
		return $this->st_usuario;
	}

	/**
	 * @return the $st_senha
	 */
	public function getSt_senha() {
		return $this->st_senha;
	}

	/**
	 * @return the $st_mensagem
	 */
	public function getSt_mensagem() {
		return $this->st_mensagem;
	}

	/**
	 * @return the $ar_destinatarios
	 */
	public function getAr_destinatarios() {
		return $this->ar_destinatarios;
	}

	/**
	 * @param field_type $st_usuario
	 */
	public function setSt_usuario($st_usuario) {
		$this->st_usuario = $st_usuario;
	}

	/**
	 * @param field_type $st_senha
	 */
	public function setSt_senha($st_senha) {
		$this->st_senha = $st_senha;
	}

	/**
	 * @param field_type $st_mensagem
	 */
	public function setSt_mensagem($st_mensagem) {
//		$this->st_mensagem = str_replace(' ', '+', $st_mensagem);
		$this->st_mensagem = strip_tags($st_mensagem);
	}


	/**
	 * Recebe um array de destinatários
	 * Exemplo: array('553799121212', '556192928899');
	 * @param array $destinatarios
	 */
	public function setAr_destinatarios(array $ar_destinatarios) {
		if($ar_destinatarios){
			foreach($ar_destinatarios as $destinatario){
				$this->ar_destinatarios[] = $destinatario;
			}
		}
	}

}
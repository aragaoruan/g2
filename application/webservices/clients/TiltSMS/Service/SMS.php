<?php


/**
 * Classe para conexão ao WebService TiltSMS
 * @url https://docs.google.com/document/d/1ufGBRZY_6IDkgz1efgeweaqz4IPaMJGnqMmYu0wwjKc/edit
 * @author Elcio Mauro Guimarães
 * @since 04/06/2013
 */
class TiltSMS_Service_SMS extends WsBO
{

    const URL = "http://tiltsms.com.br/index.php?app=webservices";


    /**
     * Método que Envia o SMS
     * @param TiltSMS_TO_SMSTO $to
     * @throws Zend_Exception
     * @throws Exception
     * @return multitype:|boolean
     */
    public static function enviar(TiltSMS_TO_SMSTO $to)
    {

        try {

            if (!$to->getSt_usuario()) {
                throw new Zend_Exception("Informe o Usuário!");
            }
            if (!$to->getSt_senha()) {
                throw new Zend_Exception("Informe a Senha!");
            }
            if (!$to->getSt_mensagem()) {
                throw new Zend_Exception("Informe a Mensagem!");
            }

            $url = self::URL;
            $url .= "&ta=pv&u=" . $to->getSt_usuario();
            $url .= "&p=" . $to->getSt_senha();
            $url .= "&msg=" . $to->getSt_mensagem();

            if (count($to->getAr_destinatarios()) == 1) {
                $destinatario = $to->getAr_destinatarios();
                $url .= "&to=" . $destinatario[0];
            } elseif (count($to->getAr_destinatarios()) > 1) {
                $destinatario = implode(",", $to->getAr_destinatarios());
                $url .= "&to=" . $destinatario;
            } else {
                throw new Zend_Exception("Nenhum destinatário informado!");
            }

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 8);
            $retorno = curl_exec($ch);
            if ($retorno === false) {
                $err = curl_error($ch);
                curl_close($ch);
                throw new Zend_Exception('Erro na chamada do Serviço: ' . $err . '.');
            } else {
                curl_close($ch);
            }
            $arr = null;
            $retorno = explode("\n", trim($retorno));
            if ($retorno) {
                $retorno = array_map('trim', $retorno);
                foreach ($retorno as $mensagem) {
                    $arr[] = explode(' ', $mensagem);
                }
                foreach ($arr as $resultado) {
                    if ($resultado[0] == 'ERR') {
                        throw new Zend_Exception('Erro ' . $resultado[1] . ' no envio do SMS!.');
                    }
                }

                return $arr;

            } else {
                throw new Zend_Exception('Erro no envio do SMS, nada foi retornado!.');
            }


        } catch (Exception $e) {
            throw $e;
            return false;
        }


    }


    public function enviar2(TiltSMS_TO_SMSTO $to)
    {
        try {
            print 'envia2()';
            Zend_Debug::dump($to);
            if (!$to->getSt_mensagem()) {
                throw new Zend_Exception("Informe a Mensagem!");
            }

            $lgn = '6195706090';
            $pwd = '105030';
            $action = 'sendsms';

            $message = urlencode(htmlspecialchars($to->getSt_mensagem().'teste'));


            $url = "http://209.133.196.250/painel/api.ashx?";//api
//            $url = "http://209.133.196.250/painel/ServiceSms.asmx?";//webservice
            $url .= "action={$action}&lgn={$lgn}&pwd={$pwd}&msg={$message}&numbers=<numbers>";


            $arrNumbers = array();
            if ($to->getAr_destinatarios()) {
                foreach ($to->getAr_destinatarios() as $key => $destinatario) {
                    $arrNumbers[] = $destinatario;
                }

                $url = str_replace("<numbers>", implode(',', $arrNumbers), $url);

            }

            print $url;

            // perform request
            $cUrl = curl_init($url);
            curl_setopt_array($cUrl, array(
                CURLOPT_HEADER => false,
                CURLOPT_FOLLOWLOCATION => true
            ));
            $response = curl_exec($cUrl);
            curl_close($cUrl);

            Zend_Debug::dump($response, '$response');

            // decode the response into an array
            $decoded = json_decode($response, true);
            Zend_Debug::dump($decoded, '$decoded');

//


//            $sms = new SimpleXmlElement($retorno, LIBXML_NOCDATA);
//            Zend_Debug::dump($sms, '$sms');

//                throw new Exception("Erro ao enviar.");

            die('fim');
        } catch (Exception $e) {
            throw $e;
        }
    }


    /**
     * Verifica os últimos SMS enviados
     * @param TiltSMS_TO_VerificaSMSTO $to
     * @param unknown_type $quantidade
     * @throws Zend_Exception
     * @throws Exception
     * @return multitype:|boolean
     */
    public static function verificar(TiltSMS_TO_VerificaSMSTO $to, $quantidade = 10)
    {

        try {

            if (!$to->getSt_usuario()) {
                throw new Zend_Exception("Informe o Usuário!");
            }
            if (!$to->getSt_senha()) {
                throw new Zend_Exception("Informe a Senha!");
            }

            $url = self::URL;
            $url .= "&ta=ds&u=" . $to->getSt_usuario();
            $url .= "&p=" . $to->getSt_senha();
            $url .= "&c=" . $quantidade;


            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $retorno = curl_exec($ch);
            if ($retorno === false) {
                $err = curl_error($ch);
                curl_close($ch);
                throw new Zend_Exception('Erro na chamada do Serviço: ' . $err . '.');
            } else {
                curl_close($ch);
            }

            $arr = null;
            $retorno = explode("\n", trim($retorno));

            if ($retorno) {
                $retorno = array_map('trim', $retorno);
                foreach ($retorno as $mensagem) {
                    $mensagem = str_replace('"', '', $mensagem);
                    $array = explode(';', $mensagem);
                    if (isset($array[5])) {
                        switch ($array[5]) {
                            case 0:
                                array_push($array, 'Na fila');
                                break;
                            case 1:
                                array_push($array, 'Enviado, mas não confirmado');
                                break;
                            case 2:
                                array_push($array, 'Devolvido');
                                break;
                            case 3:
                                array_push($array, 'Enviado');
                                break;
                            case 4:
                                array_push($array, 'Desligado ou fora de Área');
                                break;
                            case 7:
                                array_push($array, 'Entregue na operadora');
                                break;
                            default:
                                array_push($array, 'Motivo não identificado');
                                break;
                        }
                    }

                    $arr[] = $array;

                }
                foreach ($arr as $resultado) {
                    if ($resultado[0] == 'ERR 100') {
                        throw new Zend_Exception('Erro ERR 100 na verificação do envio do SMS!.');
                    }
                }

                return $arr;

            } else {
                throw new Zend_Exception('Erro na verificação do envio do SMS, nada foi retornado!.');
            }


        } catch (Exception $e) {
            throw $e;
            return false;
        }


    }

}

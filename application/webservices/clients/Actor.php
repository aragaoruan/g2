<?php


/**
 * Classe para conexão dos serviços de Curso no Actor 2/Gestor 1
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 */
class Actor {
	
	
	protected $webservicecodentidade;
	protected $webservicechave;
	protected $servico;
	protected $client;
	
	const URL = 'http://actor.localhost.com.br/';

	/**
	 * @return the $client
	 */
	public function getClient() {
		return $this->client;
	}

	/**
	 * @param Zend_Rest_Client $client
	 */
	public function setClient($client) {
		
		try {
			
			if(!class_exists('Zend_Rest_Client')) {
				throw new Exception("A Classe Zend_Rest_Client é requerida para o funcionamento deste plugin!");
			}
			
			$this->client = new Zend_Rest_Client($client);
			$httpClient = $this->client->getHttpClient();
			$httpClient->setConfig(array("timeout" => 720));
			
		} catch (Exception $e) {
			throw $e;
		}
		
		
	}

	/**
	 * @return the $servico
	 */
	public function getServico() {
		return $this->servico;
	}

	/**
	 * @param field_type $servico
	 */
	public function setServico($servico) {
		$this->servico = $servico;
	}

	/**
	 * Método construtor
	 */
	public function __construct(){}

	/**
	 * @return the $webservicecodentidade
	 */
	public function getWebservicecodentidade() {
		return $this->webservicecodentidade;
	}

	/**
	 * @return the $webservicechave
	 */
	public function getWebservicechave() {
		return $this->webservicechave;
	}

	/**
	 * @param field_type $webservicecodentidade
	 */
	public function setWebservicecodentidade($webservicecodentidade) {
		$this->webservicecodentidade = $webservicecodentidade;
	}

	/**
	 * @param field_type $webservicechave
	 */
	public function setWebservicechave($webservicechave) {
		$this->webservicechave = $webservicechave;
	}
	
	
	/**
	 * Método que pega um Objeto e converte em Array
	 * @param object
	 */
	public static function object_to_array($object){
		$new=NULL;
		if(is_object($object)){
			$object=(array)$object;
			
			foreach($object as $key => $val) {
				if(is_string($val)){
					if(trim($val)==''){
						unset($object[$key]);
					} else {
						$object[$key] = trim($val);
					}
				}
			}
			
		}
		
		if(!$object){
			return null;
		}
		
		
		if(is_array($object)){
			$new=array();
			foreach($object as $key => $val) {
				$key=preg_replace("/^\\0(.*)\\0/","",$key);
				$key=str_replace("key_","",$key);
				$new[$key] = self::object_to_array($val);
			}
		}else{
			$new = $object;
		}
		return $new;
	}

}

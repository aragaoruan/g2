<?php

/**
 * BlackBoard Web Services e Snapshot Flat File para PHP
 * 
 * Classes para Consumo dos serviços do BlackBoard
 * Desenvolvido com o estudo do "BbPHP: Blackboard Web Services Library for PHP - Copyright (C) 2011 by St. Edward's University (www.stedwards.edu) - @author johns"
 * 
 * As classes e métodos foram refeitos para se ajustar a necessidade de consumir os dois tipos de serviços
 *  1 - Web Services
 *  2 - Snapshot Flat File
 *  
 *
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * 
 */
class BlackBoard {
	
	
	public $currentXml = null;
	
	/**
	 * Salva o Nome do Serviço no momento
	 */
	public $service 		= null;
	
	public 	$debug			= false;
	private static $CONTEXT = null;
	
	/**
	 * Variaveis de acesso
	 */
	private $session_id  	= null;
	private $login_tool  	= false;
	
	/**
	 * Dados de acesso ao WS
	 */
	private 	$url					= null;
	private 	$clientprogramid		= null;
	private 	$clientvendorid			= null;
	private 	$registrationpassword	= null;
	private 	$dataSourceID			= null;
	
	/**
	 * Dados de acesso para SnapShot
	 */
	private $snap_url    = null;
	private $snap_user   = null;
	private $snap_pass   = null;
	
	
	public function __construct() {
		
	}
	
	/**
	 * Inicia o Serviço
	 */
	public function iniciarServico(){
		
		self::$CONTEXT = new BlackBoard_Service_Context();
		$metodos = get_class_methods($this);
		foreach ($metodos as $metodo) {
				if (method_exists(self::$CONTEXT, $metodo)) {
					if(substr($metodo, 0, 3)=='get'){
						$metodoSet = 'set'.substr($metodo, 3);
						self::$CONTEXT->$metodoSet($this->$metodo());
					}
				}
		}
		self::$CONTEXT->debug = $this->debug;
		$this->setSession_id(self::$CONTEXT->initialize());
	}


	/**
	 * Efetua Login no WS
	 * 
	 */
	public function efetuaLogin(){
		
		if(self::$CONTEXT instanceof BlackBoard_Service_Context){
			$metodos = get_class_methods($this);
			foreach ($metodos as $metodo) {
					if (method_exists(self::$CONTEXT, $metodo)) {
						if(substr($metodo, 0, 3)=='get'){
							$metodoSet = 'set'.substr($metodo, 3);
							self::$CONTEXT->$metodoSet($this->$metodo());
						}
					}
			}
			$this->setLogin_tool(self::$CONTEXT->loginTool());
		} else {
			throw new BlackBoard_Exception('Rode o BlackBoard::iniciarServico() para Gerar o session_id');
		}
		
	}
	
	public function debug($mixPrint, $legenda = 'Debug', $exit = false){
		
		echo "<fieldset><legend>$legenda</legend><pre>";
		if(is_string($mixPrint)){
			var_dump(
					str_replace('&gt;&lt;', '&gt;<br>&lt;', htmlspecialchars($mixPrint))
					);
		} else {
			var_dump($mixPrint);
		}
		echo "<br /><br />";
	
		$aTrace = debug_backtrace();
		if($aTrace){
			$x = 0;
			foreach($aTrace as $trace){
				$x++;
				echo 
				
				(isset($trace['file']) ? $trace['file'] : 'No file')
				
				 . "(" . (isset($trace['line']) ? $trace['line'] : 'No line') .') - '.(isset($trace['class']) ? $trace['class'] : 'No Class').'->'.
				
				 (isset($trace['function']) ? $trace['function'] : 'No function')
				.'<br>';
				if($x==20) break;
			}
		}
	
		echo "</pre></fieldset><br />";
		if( $exit ) exit;
		
	}
	
	
	/**
	 * Constrói o cabeçalho para a requisição, o BlackBoard exige um cabeçalho de Autenticação para todas
	 * as Requisições
	 * @return string
	 */
	private function buildHeader() {
		
		if ($this->session_id == null) {
			$password = 'nosession';
		} else {
// 			$password = $this->session_id;
			$password = $this->getSession_id();
		}

		$stamp = gmdate("Y-m-d\TH:i:s\Z");
		$expire = gmdate("Y-m-d\TH:i:s\Z", mktime(date("H"), date("i")+10, date("s"), date("m"), date("d"), date("Y")));
		/**
		 * This header is sensitive to line breaks and you should avoid
		 * letting things like Eclipse do auto-formatting on it.    
		 * 
		 **/
// 	            <wsse:Timestamp xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
// 	                <wsse:Created xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">$stamp</wsse:Created>
// 	            </wsse:Timestamp>

		
		$header = <<<END
		<SOAP-ENV:Header>
	        <wsse:Security SOAP-ENV:mustunderstand="true" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
				<wsu:Timestamp wsu:Id="Timestamp-45" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
					<wsu:Created>$stamp</wsu:Created>
					<wsu:Expires>$expire</wsu:Expires>
				</wsu:Timestamp>
	            <wsse:UsernameToken xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
	                <wsse:Username xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">session</wsse:Username>
	                <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">$password</wsse:Password>
	            </wsse:UsernameToken>
	        </wsse:Security>
	    </SOAP-ENV:Header>
END;
		return $header;
		
	}
	
	
	/**
	 * Monta o XML Final para a Requisição
	 * @param String XML $xml
	 * @throws Exception
	 */
	private function buildRequest($xml) {
		try {
			
			$header   = $this->buildHeader();
			$request  = '<SOAP-ENV:Envelope xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
			$request .= $header.$xml;
			$request .= '</SOAP-ENV:Envelope>';
			
			return new BlackBoard_Mensageiro($request, BlackBoard_Mensageiro::SUCESSO);
			
		} catch (Exception $e) {
			throw $e;
			return new BlackBoard_Mensageiro($e->getMessage(), BlackBoard_Mensageiro::ERRO, $e);
		}
	}
	
	
	/**
	 * Executa a Requisição, este método é chamado por todos os serviços do WebServices
	 * @param String $xml
	 * @throws Exception
	 */
	public function doCompleteRequest($xml, $metodo, $xml_return = false){
		
		try {
			return $this->doRequest($this->buildRequest($xml)->getMensagem(), $metodo, $xml_return);
		} catch (Exception $e) {
			throw $e;
		}
		
	}
	

	/**
	 * Executa a Requisição ao WebService
	 * @param string $xml
	 * @throws BlackBoard_Exception
	 * @throws Exception
	 * @return boolean|NULL|BlackBoard_Mensageiro
	 */
	private function doRequest($xml, $metodo, $xml_return = false) {
		$method = explode('::', $metodo);
		
		try {
			
			$this->verificaCredenciaisWS();
			
			if($metodo!='BlackBoard_Service_Context::initialize' && $this->session_id==false){
				throw new BlackBoard_Exception("$metodo: Execute o Método BlackBoard::iniciarServico() para iniciar o WS");
			}
			
			if($metodo!='BlackBoard_Service_Context::initialize' && $metodo!='BlackBoard_Service_Context::loginTool' && $metodo!='BlackBoard_Service_Context::registerTool' && $this->login_tool==false){
				$this->efetuaLogin();
				if($this->getLogin_tool()!="true"){
					throw new BlackBoard_Exception("Erro ao logar no WS do BlackBoard");
				}
			}

			if($this->debug){
				$this->debug($xml, "Request: $metodo - ".$this->getUrl() . '/webapps/ws/services/' . $this->service . '.WS');
			}
				$ch = curl_init();
			
				curl_setopt($ch, CURLOPT_URL, $this->getUrl() . '/webapps/ws/services/' . $this->service . '.WS');
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: text/xml; charset=utf-8', 'SOAPAction: "' . $method[1] . '"'));
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                $result         = curl_exec($ch);
                $err = false;
                if($result==false) {
                    $err            = curl_error($ch);
                }
                curl_close($ch);

            if($this->debug){
                $this->debug($result, "Response: $metodo - ". $this->getUrl() . '/webapps/ws/services/' . $this->service . '.WS');
            }

            if($err)
                throw new Exception("Erro cURL : {$err}");
			
			
			$result_array = $this->xmlstr_to_array($result);
			if(isset($result_array['Body']['Fault'])){
				$erro = $result_array['Body']['Fault'];
				throw new BlackBoard_Exception($erro['faultcode'].' - '.$erro['faultstring']);
			}
			if($xml_return){
				return new SimpleXMLElement($result);
			}
			
			$final_result = (isset($result_array['Body'][$method[1] . 'Response']['return'])) ? $result_array['Body'][$method[1] . 'Response']['return'] : null;
			return $final_result;
		} catch (Exception $e) {
			throw $e;
			return new BlackBoard_Mensageiro($e->getMessage(), BlackBoard_Mensageiro::ERRO, $e);
		}

	}	

	public function xmlstr_to_array($xmlstr) {
		if($xmlstr){
			
			$doc = new DOMDocument();
			$load = $doc->loadXML($xmlstr);
            if($load==false){

                if(strripos($xmlstr, "Não encontrado")){
                    throw new Exception("Erro ao ler o XML do BlackBoard: Recurso não encontrado!");
                }

                throw new Exception("Erro ao ler o XML do BlackBoard ");
            }
			return $this->domnode_to_array($doc->documentElement);
			
		} else {
			return $xmlstr;
		}
	}
	
	public function domnode_to_array($node) {
		$output = array();
		
		switch ($node->nodeType) {
			case XML_CDATA_SECTION_NODE:
			case XML_TEXT_NODE:
				$output = trim($node->textContent);
				break;
			case XML_ELEMENT_NODE:
				for ($i=0, $m=$node->childNodes->length; $i<$m; $i++) {
					$child = $node->childNodes->item($i);
					$v = $this->domnode_to_array($child);
					if(isset($child->tagName)) {
						$t = $child->tagName;
						if (strpos($t, ':') !== false) {
							$temp = explode(':', $t);
							$key = $temp[1];
						} else {
							$key = $t;
						}
						$test = substr($t, strpos($t, ':') + 1, strlen($t) - strpos($t, ':') + 1);
						if(!isset($output[$key])) {
							$output[$key] = array();
						}
						$output[$key][] = $v;
					} elseif($v) {
						$output = (string) $v;
					}
				}	

				if(is_array($output)) {
					if($node->attributes->length) {
						$a = array();
						foreach($node->attributes as $attrName => $attrNode) {
							$a[$attrName] = (string) $attrNode->value;
						}
						$output['@attributes'] = $a;
					}
					foreach ($output as $t => $v) {
						if(is_array($v) && count($v)==1 && $t!='@attributes') {
// 							$output[$t] = $v[0];
							$output[$t] = ($v[0] ? $v[0] : null);
						}
					}
				}
			break;
		}
		
		return $output;
	}	
	
	
	
	/**
	 * Executa a chamada ao SnapShot
	 * @param string $filestring - String que represente o arquivo 
	 * @param string $metodo - Deve ser passada a magic __METHOD__ na chamada
	 * @throws BlackBoard_Exception
	 * @return string|mixed
	 */
	public function snapShotCall($filestring, $metodo){
		
		try {
			
			
			$this->verificaCredenciaisSnapShot();
			
			
// 			$url 	= "https://unyleya-test.blackboard.com/webapps/bb-data-integration-flatfile-BBLEARN/endpoint/person/store";
// 			$url 	= "https://unyleya.blackboard.com/webapps/bb-data-integration-flatfile-BBLEARN/endpoint/person/store";
// 			$USER 	= "891b330d-8aa0-4950-b1fc-199e83c70bed";
// 			$PASS 	= "snapshotg2";
			$url 	= $this->getSnap_url().$this->getEndpoint_url();
			$USER 	= $this->getSnap_user(); //"891b330d-8aa0-4950-b1fc-199e83c70bed";
			$PASS 	= $this->getSnap_pass(); //"snapshotg2";
			
			
// 		$dados = <<<END
// external_person_key|institution_role|user_id|firstname|lastname|email|row_status|available_ind|passwd
// G2S_1215|TDC|ELCIO.SNAP5|ELCIO2|GUIMARAES2|ELCIOGUIMARAES@GMAIL.COM|ENABLED|Y|12345
// END;
			
			if($this->debug){
				$this->debug($filestring, "Request: $metodo - ". $url);
			}
			
			if (function_exists('mb_strlen')) {
				$size = mb_strlen($filestring, '8bit');
			} else {
				$size = strlen($filestring);
			}
			
			$ch = 	curl_init($url);
					curl_setopt($ch, CURLOPT_USERPWD, $USER.":".$PASS);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
					curl_setopt($ch, CURLOPT_POST, true);
					curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/plain","Content-Length: " + $size));
					curl_setopt($ch, CURLOPT_POSTFIELDS, $filestring);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_VERBOSE, 1);
					curl_setopt($ch, CURLOPT_HEADER, 0);
			
			$response = curl_exec($ch);
			
			if($this->debug){
				$this->debug($response, "Response: $metodo - ". $url);
			}
			
			if($response === false) {
				$err            = curl_error($ch);
				curl_close($ch);
				throw new BlackBoard_Exception('Erro cURL '.$err.'.');
			} else {
				curl_close($ch);
				$md5_log = false;
				preg_match('~[A-Za-z0-9]{32}~', $response, $matches);
				if($matches[0]){
					$md5_log = $matches[0];
				}
				if($md5_log==false){
					return new BlackBoard_Mensageiro('Não foi possível recuperar o código para o log.', BlackBoard_Mensageiro::AVISO, $response);
				} else {
					return new BlackBoard_Mensageiro($md5_log, BlackBoard_Mensageiro::SUCESSO, $response);
				}
			}
			
			
		} catch (Exception $e) {
			throw $e;
			return new BlackBoard_Mensageiro($e->getMessage(), BlackBoard_Mensageiro::ERRO, $e);
		}
		
	}
	
	
	/**
	 * 
	 * Verifica se os dados foram salvos no BB
	 * http://help.blackboard.com/en-us/Learn/9.1_SP_12_and_SP_13/Administrator/140_System_Integration/010_Student_Information_System_%28SIS%29/030_SIS_Integration_Types/010_Snapshot_Flat_File/020_Snapshot_Flat_File_Automation
	 * @param md5 $dados
	 * @param string $url
	 * @throws Exception
	 * @return BlackBoard_Mensageiro
	 */
	public function snapShotVerify($dados, $url = 'https://unyleya.blackboard.com'){
		
		try {
			
			$this->verificaCredenciaisSnapShot();
			
			if (function_exists('mb_strlen')) {
				$size = mb_strlen($dados, '8bit');
			} else {
				$size = strlen($dados);
			}
			
			if($this->debug){
				$this->debug($dados, "Request: snapShotVerify - ". $this->getSnap_url().$endpoint.$dados);
			}
			
			$endpoint = '/webapps/bb-data-integration-flatfile-BBLEARN/endpoint/dataSetStatus/';
			
			$ch = 	curl_init($this->getSnap_url().$endpoint.$dados);
// 					curl_setopt($ch, CURLOPT_USERPWD, $USER.":".$PASS);
					curl_setopt($ch, CURLOPT_USERPWD, $this->getSnap_user().":".$this->getSnap_pass());
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
					curl_setopt($ch, CURLOPT_POST, true);
					curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/plain","Content-Length: " + $size));
					curl_setopt($ch, CURLOPT_POSTFIELDS, $dados);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_VERBOSE, 1);
					curl_setopt($ch, CURLOPT_HEADER, 0);
					
			/**
			 * @todo
			 * Tratar o XML
			 */		
			$response = curl_exec($ch);
			
			if($this->debug){
				$this->debug($response, "Response: snapShotVerify - ". $this->getSnap_url().$endpoint.$dados);
			}
			
			if($response === false) {
				$err = curl_error($ch);
				curl_close($ch);
				return new BlackBoard_Mensageiro('Erro na chamada do Serviço: '.$err.'.', BlackBoard_Mensageiro::AVISO, $response);
			} else {
				curl_close($ch);
				return new BlackBoard_Mensageiro($this->xmlstr_to_array($response), BlackBoard_Mensageiro::SUCESSO, $response);
			}
			
		} catch (Exception $e) {
			throw $e;
			return new BlackBoard_Mensageiro($e->getMessage(), BlackBoard_Mensageiro::ERRO, $e);
		}
		
	}
	
	
	
	/**
	 * Verifica se todas as credenciais do WS foram informadas
	 * @throws BlackBoard_Exception
	 * @return boolean
	 */
	public function verificaCredenciaisWS(){
		if(!$this->getDataSourceID() ||  !$this->getUrl() || !$this->getClientprogramid() || !$this->getClientvendorid() || !$this->getRegistrationpassword()){
			throw new BlackBoard_Exception("Você precisa informar todas as Credenciais (dataSourceID, url, clientprogramid, clientvendorid, registrationpassword) necessárias para usar este serviço!");
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * Verifica se todas as credenciais do SnapShot foram informadas
	 * @throws BlackBoard_Exception
	 * @return boolean
	 */
	public function verificaCredenciaisSnapShot(){
		if(!$this->getSnap_url() || !$this->getSnap_pass() || !$this->getSnap_user()){
			throw new BlackBoard_Exception("Você precisa informar todas as Credenciais (snap_url, snap_pass, snap_user) necessárias para usar este serviço!");
			return false;
		} else {
			return true;
		}
	}
	
	
	/**
	 * @return the $dataSourceID
	 */
	public function getDataSourceID() {
		return $this->dataSourceID;
	}

	/**
	 * @param NULL $dataSourceID
	 */
	public function setDataSourceID($dataSourceID) {
		$this->dataSourceID = $dataSourceID;
	}

	/**
	 * @return the $url
	 */
	public function getUrl() {
		return $this->url;
	}

	/**
	 * @return the $clientprogramid
	 */
	public function getClientprogramid() {
		return $this->clientprogramid;
	}

	/**
	 * @return the $clientvendorid
	 */
	public function getClientvendorid() {
		return $this->clientvendorid;
	}

	/**
	 * @return the $registrationpassword
	 */
	public function getRegistrationpassword() {
		return $this->registrationpassword;
	}

	/**
	 * @return the $snap_url
	 */
	public function getSnap_url() {
		return $this->snap_url;
	}

	/**
	 * @return the $snap_user
	 */
	public function getSnap_user() {
		return $this->snap_user;
	}

	/**
	 * @return the $snap_pass
	 */
	public function getSnap_pass() {
		return $this->snap_pass;
	}

	/**
	 * @param NULL $url
	 */
	public function setUrl($url) {
		$this->url = $url;
	}

	/**
	 * @param NULL $clientprogramid
	 */
	public function setClientprogramid($clientprogramid) {
		$this->clientprogramid = $clientprogramid;
	}

	/**
	 * @param NULL $clientvendorid
	 */
	public function setClientvendorid($clientvendorid) {
		$this->clientvendorid = $clientvendorid;
	}

	/**
	 * @param NULL $registrationpassword
	 */
	public function setRegistrationpassword($registrationpassword) {
		$this->registrationpassword = $registrationpassword;
	}

	/**
	 * @param NULL $snap_url
	 */
	public function setSnap_url($snap_url) {
		$this->snap_url = $snap_url;
	}

	/**
	 * @param NULL $snap_user
	 */
	public function setSnap_user($snap_user) {
		$this->snap_user = $snap_user;
	}

	/**
	 * @param NULL $snap_pass
	 */
	public function setSnap_pass($snap_pass) {
		$this->snap_pass = $snap_pass;
	}
	/**
	 * @return the $session_id
	 */
	public function getSession_id() {
		return $this->session_id;
	}

	/**
	 * @param NULL $session_id
	 */
	public function setSession_id($session_id) {
		$this->session_id = $session_id;
	}

	/**
	 * @return the $login_tool
	 */
	public function getLogin_tool() {
		return $this->login_tool;
	}
	
	/**
	 * @param boolean $login_tool
	 */
	public function setLogin_tool($login_tool) {
		$this->login_tool = $login_tool;
	}
	
	
}


<?php
/**
 *
 * @author ederlamar
 *        
 *        
 */
class TotvsLancamentosWebServices extends WsBO {
	
	/**
	 *
	 * @param $wsLink mixed
	 *       	 Link do Client Rest
	 *       	
	 */
	public function __construct($wsLink = null) {
		parent::__construct ( $wsLink = null );
	
	}
	
	
	/**
	 * Método que busca os lançamentos de uma determinada venda e gera os lançamentos no sistema Fluxus
	 * @param VendaTO $vendaTO
	 * @return Ead1_Mensageiro
	 */
	public function gerarLancamentosVenda(VendaTO $vendaTO){
		if($this->getEntidadeIntegracaoTO(SistemaTO::FLUXUS)){
            $negocio = new \G2\Negocio\TotvsVwIntegraGestorFluxus();
            $retornoIntegracao = $negocio->integrar(array('id_entidade' => $vendaTO->getId_entidade()));
            return new Ead1_Mensageiro($retornoIntegracao , Ead1_IMensageiro::SUCESSO);

			/*$dao = new FluxusDAO();
			$lancamentos = $dao->dadosLancamentosIntegracao($vendaTO);
			if($lancamentos->tipo == Ead1_IMensageiro::SUCESSO){
				foreach($lancamentos->getMensagem() as $vwIntegraGestorFluxusTO){
					$bo = new FluxusBO();
					//$vwIntegraGestorFluxusTO->setCGCCFO_FCFO($bo->setaMascaraCPF($vwIntegraGestorFluxusTO->getCGCCFO_FCFO()));
					$vwIntegraGestorFluxusTO->setTELEFONE_FCFO($bo->setaMascaraTelefone($vwIntegraGestorFluxusTO->getTELEFONE_FCFO()));
					$vwIntegraGestorFluxusTO->setFAX_FCFO($bo->setaMascaraTelefone($vwIntegraGestorFluxusTO->getFAX_FCFO()));
					
					//$this->trataTipoDocumento($vwIntegraGestorFluxusTO);
					//$this->trataContaCaixa($vwIntegraGestorFluxusTO);
					
					$arLancamentosIntegrados[$vwIntegraGestorFluxusTO->getId_lancamento()][] = $dao->spIntegracao($vwIntegraGestorFluxusTO, "[189.114.56.180]", $vwIntegraGestorFluxusTO->getSt_caminho());
				}
				return new Ead1_Mensageiro($arLancamentosIntegrados, Ead1_IMensageiro::SUCESSO);
			}
			return $lancamentos;*/
		}else{
			return new Ead1_Mensageiro("Não existe integração para a entidade: " . $vendaTO->getId_entidade(), Ead1_IMensageiro::AVISO);
		}
	}
	
	/**
	 * Método que busca todos os lançamentos da entidade da sessão e realiza a sincronização com o sistema fluxus se esta entidade possuir integração
	 * @return Ead1_Mensageiro
	 */
	public function sincronizarLancamentosNaoEnviados(){
		$vendaTO = new VendaTO();
		$vendaTO->setId_entidade($vendaTO->getSessao()->id_entidade);
		if(!$vendaTO->getId_entidade()){
			return new Ead1_Mensageiro("Sessão inválida!", Ead1_IMensageiro::ERRO, "TotvsLancamentosWebServices::sincronizarLancamentosNaoEnviados()");
		}else{
			return $this->gerarLancamentosVenda($vendaTO);
		}
	}
	
	/**
	 * Método que baixa no Gestor2 os lançamentos baixados no Fluxus que não estão baixados no gestor2 e que estão integrados
	 * @return Ead1_Mensageiro
	 */
	public function atualizarLancamentosBaixados(){
		$lancamentosABaixarTO = new Totvs_VwLancamentosABaixarTO();
		$lancamentosABaixarTO->setId_entidade($lancamentosABaixarTO->getSessao()->id_entidade);
		if(!$lancamentosABaixarTO->getId_entidade()){
			return new Ead1_Mensageiro("Sessão inválida!", Ead1_IMensageiro::ERRO, "TotvsLancamentosWebServices::atualizarLancamentosBaixados()");
		}else{
			$lancamentoABaixarTOs = $lancamentosABaixarTO->fetch();
			if($lancamentoABaixarTOs){
				foreach($lancamentoABaixarTOs as $lancamentoABaixarTOatual){
					$lancamentoTO = new LancamentoTO();
					$lancamentoTO->setId_lancamento($lancamentoABaixarTOatual->getId_lancamento());
					$lancamentoTO->setBl_quitado(true);
					$lancamentoTO->setDt_quitado($lancamentoABaixarTOatual->getDt_baixa());
					$lancamentoTO->setNu_quitado($lancamentoABaixarTOatual->getNu_valorbaixado());
					$vendaDAO = new VendaDAO();
					$arLancamentos[$lancamentoABaixarTOatual->getId_lancamento()] = $vendaDAO->editarLancamento($lancamentoTO, "id_lancamento = " . $lancamentoABaixarTOatual->getId_lancamento());
				}
				return new Ead1_Mensageiro($arLancamentos);
			}else{
				return new Ead1_Mensageiro("Não existem lançamentos à baixar para a sua entidade!", Ead1_IMensageiro::AVISO);
			}
		}
		
	}
	
	/**
	 * Método que compara o LancamentoTO com o Registro da Vw para ver se há necessidade de atualizações
	 * @param LancamentoTO $lancamentoTO
	 * @param Totvs_VwAtualizarFluxusGestorTO $lancamentoFluxus
	 * @return boolean
	 */
	private function validarAtualizacao(LancamentoTO &$lancamentoTO, Totvs_VwAtualizarFluxusGestorTO $lancamentoFluxus){
		
		//$retorno = new LancamentoTO(); se der errado voltar essa linha, remover a linha seguinte e tirar a passagem de parametro por referencia
		$retorno = &$lancamentoTO;
		
		$retorno->setId_lancamento($lancamentoFluxus->getId_lancamento());
		
		if($lancamentoTO->getNu_valor() != $lancamentoFluxus->getVALOR_ORIGINAL()){
			$retorno->setNu_valor($lancamentoFluxus->getVALOR_ORIGINAL());
		}
		
		if($lancamentoTO->getId_meiopagamento() != $lancamentoFluxus->getId_meiopagamento()){
			$retorno->setId_meiopagamento($lancamentoFluxus->getId_meiopagamento());
		}
		
		if($lancamentoFluxus->getSTATUSLAN() == Totvs_VwAtualizarFluxusGestorTO::STATUS_EM_ABERTO 
				|| $lancamentoFluxus->getSTATUSLAN() == Totvs_VwAtualizarFluxusGestorTO::STATUS_BAIXADO){

			$statuslan = ($lancamentoFluxus->getSTATUSLAN() == Totvs_VwAtualizarFluxusGestorTO::STATUS_BAIXADO ? true : false);
		
			if($lancamentoTO->getBl_quitado() != $statuslan){
				$retorno->setBl_quitado($statuslan);
			}
		}
		
		if($lancamentoTO->getDt_vencimento() && $lancamentoTO->getDt_vencimento()->toString('Y-m-d H:i:s') != $lancamentoFluxus->getDATA_VENCIMENTO()->format('Y-m-d H:i:s')){
			$retorno->setDt_vencimento($lancamentoFluxus->getDATA_VENCIMENTO());
		}
		
		if($lancamentoTO->getDt_quitado() && $lancamentoFluxus->getDATA_BAIXA() && $lancamentoTO->getDt_quitado()->toString('Y-m-d H:i:s') != $lancamentoFluxus->getDATA_BAIXA()->format('Y-m-d H:i:s')){
			$retorno->setDt_quitado($lancamentoFluxus->getDATA_BAIXA());
		}
		
		if($lancamentoTO->getDt_emissao() && $lancamentoTO->getDt_emissao()->toString('Y-m-d H:i:s') != $lancamentoFluxus->getDATA_EMISSAO()->format('Y-m-d H:i:s')){
			$retorno->setDt_emissao($lancamentoFluxus->getDATA_EMISSAO());
		}
		
		if($lancamentoTO->getSt_coddocumento() != $lancamentoFluxus->getNUMERO_DOCUMENTO()){
			$retorno->setSt_coddocumento($lancamentoFluxus->getNUMERO_DOCUMENTO());
		}
		
		if($lancamentoTO->getNu_quitado() != $lancamentoFluxus->getVALOR_BAIXADO()){
			$retorno->setNu_quitado($lancamentoFluxus->getVALOR_BAIXADO());
		}
		
		if($lancamentoTO->getDt_atualizado() && $lancamentoTO->getDt_atualizado()->toString('Y-m-d H:i:s') != $lancamentoFluxus->getDATA_ALTERACAO()->format('Y-m-d H:i:s')){
			$retorno->setDt_atualizado($lancamentoFluxus->getDATA_ALTERACAO());
		}
		
		if($lancamentoFluxus->getSTATUSLAN() == Totvs_VwAtualizarFluxusGestorTO::STATUS_EM_ABERTO 
					|| $lancamentoFluxus->getSTATUSLAN() == Totvs_VwAtualizarFluxusGestorTO::STATUS_BAIXADO){
			
			$bl_ativo = ($lancamentoFluxus->getSTATUSLAN() == Totvs_VwAtualizarFluxusGestorTO::STATUS_BAIXADO) ? true : false ;
		
			if($lancamentoTO->getBl_ativo() != $bl_ativo){
				$retorno->setBl_ativo($bl_ativo);
			}
		}
		
		if($lancamentoFluxus->getSTATUSLAN() == Totvs_VwAtualizarFluxusGestorTO::STATUS_BAIXADO){
			$retorno->setBl_quitado(true);
			$retorno->setDt_quitado($lancamentoFluxus->getDATA_BAIXA());
			$retorno->setNu_quitado($lancamentoFluxus->getVALOR_BAIXADO());
			$retorno->setBl_ativo(true);
		} else if ($lancamentoFluxus->getSTATUSLAN() == Totvs_VwAtualizarFluxusGestorTO::STATUS_CANCELADO){
			$retorno->setBl_ativo(false);
		} else if($lancamentoFluxus->getSTATUSLAN() == Totvs_VwAtualizarFluxusGestorTO::STATUS_BAIXADO_POR_ACORDO){
			$retorno->setBl_ativo(false);
			$retorno->setBl_quitado(false);
			
// 			$lancamentoVendaTO = new LancamentoVendaTO();
// 			$lancamentoVendaTO->setId_lancamento($lancamentoTO->getId_lancamento());
			
// 			$vendaDAO = new VendaDAO();
// 			$vendaDAO->deletarLancamentoVenda($lancamentoVendaTO);
			
		} else {
			$retorno->setDt_quitado(null);
			$retorno->setNu_quitado(0.00);
			$retorno->setBl_quitado(false);
			$retorno->setBl_ativo(true);
		}
		
		return $retorno;
	}
	
	/**
	 * Método que seta o tipo de documento na referência do to de dados para a integração via SP
	 * @param Totvs_VwIntegraGestorFluxusTO $vwIntegraGestorFluxusTO
	 */
	private function trataTipoDocumento(Totvs_VwIntegraGestorFluxusTO &$vwIntegraGestorFluxusTO){
		switch($vwIntegraGestorFluxusTO->getId_entidade()){
			case 2: // CETEB
				$vwIntegraGestorFluxusTO->setCODTDO(($vwIntegraGestorFluxusTO->getBl_entrada() ? "031" : "040"));
				break;
			case 5: // Escola Aberta
				$vwIntegraGestorFluxusTO->setCODTDO(($vwIntegraGestorFluxusTO->getBl_entrada() ? "031" : "040"));
				break;
		}
	}
	
	/**
	 * Método que seta a conta caixa na referência do to de dados para a integração via SP
	 * @param Totvs_VwIntegraGestorFluxusTO $vwIntegraGestorFluxusTO
	 */
	private function trataContaCaixa(Totvs_VwIntegraGestorFluxusTO &$vwIntegraGestorFluxusTO){
		switch($vwIntegraGestorFluxusTO->getId_entidade()){
			case 2: // CETEB
				$vwIntegraGestorFluxusTO->setCODCXA("006");
				break;
			case 5: // Escola Aberta
				$vwIntegraGestorFluxusTO->setCODCXA("006");
				break;			
		}
	}
}

?>
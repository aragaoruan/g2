<?php 

class BaiaoCMS {
	
    var $client 	= null;
    var $ApiKey 	= null;
    var $ApiSecret 	= null;
    var $host		= null;
    
    public function __construct($host, $ApiKey, $ApiSecret) 
    {
        $factory 			= new \QueryAuth\Factory();
        $signer  			= new La_QueryAuth_Signer(new \QueryAuth\ParameterCollection());
        $this->client  		= new \QueryAuth\Client($signer, $factory->newKeyGenerator());
        $this->ApiKey 		= $ApiKey;
        $this->ApiSecret 	= $ApiSecret;
        $this->host			= $host;
        
    }
    
    public function saveCategory(BaiaoCMS_CategoryVO $vo)
    {
        
    	$path    = '/v1/portal/category';
        return $this->makeSignedRequest($path, $vo->toArray());
        
    }
    
    public function savePartner(BaiaoCMS_PartnerVO $vo)
    {
        $path    = '/v1/portal/partner';
        return $this->makeSignedRequest($path, $vo->toArray());
    }
    
    public function saveTeacher(BaiaoCMS_TeacherVO $vo)
    {
        $path    = '/v1/portal/teacher';
        return $this->makeSignedRequest($path, $vo->toArray());
    }
    
    /**
     * Salva um Produto no CMS desenvolvido pela Baiao de Dois
     * @param BaiaoCMS_ProductVO $vo
     * @return string
     */
    public function saveProduct(BaiaoCMS_ProductVO $vo)
    {
        $path    = '/v1/portal/product';
        return $this->makeSignedRequest($path, $vo->toArray());
    }
    
    protected function makeSignedRequest($path, $params)
    {
    	
        $params = $this->client->getSignedRequestParams($this->ApiKey, $this->ApiSecret, 'POST', $this->host, $path, $params);
        $client = new Zend_Http_Client($this->host . $path);
        $client->setParameterPost($params);
        return $client->request('POST')->getBody();
    	
    }
}

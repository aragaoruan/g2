<?php

/**
 * super classe para serviços de integração com a maximize
 *
 */
class MaximizeWebServices extends WsBO
{

    protected $url = false;
    protected $tokenAutenticacao;

    public function __construct()
    {
        $negocio = new \G2\Negocio\Negocio();
        if (!$this->url) {
            /** @var \G2\Entity\Sistema $sistema */
            $sistema = $negocio->find('\G2\Entity\Sistema', \G2\Constante\Sistema::MAXIMIZE);
            if ($sistema instanceof \G2\Entity\Sistema && $sistema->getst_conexao()) {
                $this->url = $sistema->getst_conexao();
                $this->setTokenAutenticacao($sistema->getst_chaveacesso());
            } elseif ($sistema instanceof \G2\Entity\Sistema && empty($sistema->getst_conexao())) {
                throw new Exception('Conexão do sistema ' . G2\Constante\Sistema::arraySistemas(G2\Constante\Sistema::MAXIMIZE) . ' não cadastrado.');
            } else {
                throw new Exception('Sistema ' . G2\Constante\Sistema::arraySistemas(G2\Constante\Sistema::MAXIMIZE) . ' não configurado.');
            }
        }
        parent::__construct();
    }


    /**
     * @param $method (Eg.: '/c/prova/listar')
     * @param $data (Eg.: array('buscaTextual' => 'teste') )
     * @param string $format (Eg.: 'json')
     * @param $paramUrl (Para diferenciar se o paramentro vai no body ou na url)
     * @return Ead1_Mensageiro
     */
    public function exec($method, $data, $format = 'json', $paramUrl = false)
    {
        try {
            $url = $this->url . $method;

            if ($paramUrl) {
                $url = $this->url . $method . $data;
            }

            $ch = curl_init($url);
            curl_setopt_array($ch, array(
                CURLOPT_POST => true,
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_POSTFIELDS => json_encode($data),
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER => $this->getHttpHeader()
            ));
            $response = curl_exec($ch);
            if ($response === false) {
                throw new Exception(curl_error($ch));
            }

            curl_close($ch);

            if ($format === 'json') {
                $return = json_decode($response);
                if (!empty($return->msgErro)) {
                    throw new Exception($return->msgErro);
                }
            } else if ($format === 'string') {
                return $response;
            } else {
                throw new Exception('Formato de retorno não implementado');
            }
            return new Ead1_Mensageiro($return);
        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }

    }

    /**
     * @param $token
     * @return $this
     */
    public function setTokenAutenticacao($token)
    {
        $this->tokenAutenticacao = $token;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTokenAutenticacao()
    {
        return $this->tokenAutenticacao;
    }

    /**
     * @return bool|string
     */
    public function getUrl()
    {
        return $this->url;
    }


    /**
     * @return array
     */
    private function getHttpHeader()
    {
        $httpHeader = ["Content-Type: application/json"];

        if ($this->getTokenAutenticacao()) {
            array_push($httpHeader, "Token-Autenticacao: {$this->getTokenAutenticacao()}");
        }
        return $httpHeader;

    }

}

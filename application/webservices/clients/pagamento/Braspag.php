<?php
/**
 * super classe para serviços do moodle
 * @author ederlamar
 *
 */
class Braspag extends WsBO
{
	const T_XML_ENV = 'http://schemas.xmlsoap.org/soap/envelope/';
	const T_XML_XPATH = 'https://www.pagador.com.br/webservice/pagador';
	const T_INVALID_CALL = 'Ocorreu um erro na chamada ao Serviço. O XML veio vazio';

	/**
	 * Método para executar a chamda ao WebService por CURL
	 * @param string $xml
	 * @param string $action
	 * @throws Zend_Exception
	 * @return mixed
	 */
	protected function _exeCutaSOAPCurl($xml, $action, $url)
	{
		$header = array(
			"Content-type: text/xml;charset=\"utf-8\"",
			"Accept: text/xml",
			"Cache-Control: no-cache",
			"Pragma: no-cache",
			"POST: \"".$action."\"",
			"Content-length: ".strlen($xml),
		);

		$soap_do = curl_init();
		curl_setopt($soap_do, CURLOPT_URL, $url);
		curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 120);
		curl_setopt($soap_do, CURLOPT_TIMEOUT, 120);
		curl_setopt($soap_do, CURLOPT_PORT, 443);
		curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true );
		curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($soap_do, CURLOPT_POST, true );
		curl_setopt($soap_do, CURLOPT_POSTFIELDS, $xml);
		curl_setopt($soap_do, CURLOPT_HTTPHEADER, $header);

		$retorno = curl_exec($soap_do);

		if (false === $retorno) {
			$err = curl_error($soap_do);
			curl_close($soap_do);

			throw new Zend_Exception('Erro na chamada do Serviço: '. $err . '.');
		}

		curl_close($soap_do);
		return $retorno;
	}

	/**
	 * Método para ler o retorno do XML
	 * @param unknown_type $xml
	 * @param unknown_type $actionResponse
	 * @return Ead1_Mensageiro
	 */
	protected function _lerSOAPXmlCurl
	(
		$xmlFonte
		, $actionResponse
		, $xpath = self::T_XML_XPATH
	){
		try {
			$xml = simplexml_load_string($xmlFonte);
			$xml->registerXPathNamespace("def", $xpath);
			$nodes = $xml->xpath("//def:".$actionResponse);

			if (empty($nodes)) {
				$xml = simplexml_load_string($xmlFonte);
				$xml->registerXPathNamespace("def", self::T_XML_ENV);
				$nodes = $xml->xpath("//def:Fault");

				if (empty($nodes)) {
					return new Ead1_Mensageiro(
						self::T_INVALID_CALL
						, Ead1_IMensageiro::ERRO
						, $xmlFonte
					);
				}

				return new Ead1_Mensageiro(
					$nodes[0]->faultstring
					, Ead1_IMensageiro::ERRO
					, $xmlFonte
				);
			}

			return new Ead1_Mensageiro($nodes, Ead1_IMensageiro::SUCESSO);

		} catch (Exception $exc) {
			return new Ead1_Mensageiro(
				$exc->getMessage()
				, Ead1_IMensageiro::ERRO
				, $exc
			);
		}
	}
}

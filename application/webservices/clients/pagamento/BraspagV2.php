<?php


/**
 * super classe para serviços do moodle
 * @author ederlamar
 *
 */
class BraspagV2
{

    private $MerchantId = null;
    private $MerchantKey = null;
    private $URL = null;

    public function __construct($MerchantId, $MerchantKey, $URL)
    {
        $this->MerchantId = $MerchantId;
        $this->MerchantKey = $MerchantKey;
        $this->URL = $URL;
    }

    public static function GUID()
    {
        if (function_exists('com_create_guid') === true) {
            return trim(com_create_guid(), '{}');
        }

        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }

    /**
     * Executa uma requisição
     * @param $url
     * @param array $data
     * @param $header
     * @param string $method
     * @return Ead1_Mensageiro
     */
    public function httpRequest($service, $data = array(), $method = 'GET'){
        try {

            if(!$this->getMerchantId()) throw new \Exception("Informe o MerchantId");

            if(!$this->getMerchantKey()) throw new \Exception("Informe a MerchantKey");

            $header = array(
                "Content-Type: application/json",
                "MerchantId: ".$this->getMerchantId(),
                "MerchantKey: ".$this->getMerchantKey(),
                "RequestId: ".self::GUID(),
            );

            $curl = curl_init($service);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_VERBOSE, 1);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);

            if ($method == "POST" || $method == "PUT")
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));

            $result = curl_exec($curl);
            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $curl_error = curl_error($curl);

            curl_close($curl);

            if ($curl_error)
                throw new \Exception($curl_error);

            return new Ead1_Mensageiro($result, Ead1_IMensageiro::SUCESSO, $httpcode);

        } catch (\Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }

    }

    public function httpPost(){


    }

    /**
     * @return string
     */
    public function getMerchantId()
    {
        return $this->MerchantId;
    }

    /**
     * @return string
     */
    public function getMerchantKey()
    {
        return $this->MerchantKey;
    }

    /**
     * @return string
     */
    public function getURL()
    {
        return $this->URL;
    }





}

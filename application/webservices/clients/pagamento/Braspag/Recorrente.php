<?php


/**
 * super classe para serviços do moodle
 * @author ederlamar
 *
 */
class Braspag_Recorrente extends Braspag {
	
	const AUTORIZETRANSACTION 				= 'CreateCreditCardOrder';
	const AUTORIZETRANSACTIONRESULT 		= 'CreateCreditCardOrderResult';
	const AUTORIZETRANSACTIONRESPONSE 		= 'CreateCreditCardOrderResponse';
	
	const CANCELTRANSACTION 				= 'DisableOrder';
	const CANCELTRANSACTIONRESULT 			= 'DisableOrderResult';
	const CANCELTRANSACTIONRESPONSE 		= 'DisableOrderResponse';

	/**
	 * Autoriza um pagamento recorrente
	 * @author Elcio Mauro Guimar�es - elcioguimaraes@gmail.com
	 * @param CreateCreditCardOrderTO $cccoTO
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function autorizar(CreateCreditCardOrderTO $cccoTO) {
		
		try {		
			
			$xml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:rec="https://www.pagador.com.br/webservice/recorrente">
					   <soapenv:Header/>
					   <soapenv:Body>
					      <rec:CreateCreditCardOrder>
					         <rec:merchantId>'.$cccoTO->getMerchantId().'</rec:merchantId>
					         <rec:orderId>'.$cccoTO->getOrderId().'</rec:orderId>
					         <rec:startDate>'.$cccoTO->getStartDate().'</rec:startDate>
					         <rec:endDate>'.$cccoTO->getEndDate().'</rec:endDate>
					         <rec:recurrenceInterval>'.$cccoTO->getRecurrenceInterval().'</rec:recurrenceInterval>
					         <rec:amount>'. $cccoTO->getAmount().'</rec:amount>
					         <rec:paymentMethod>'.$cccoTO->getPaymentMethod().'</rec:paymentMethod>
					         <rec:holder>'.$cccoTO->getHolder().'</rec:holder>
					         <rec:cardNumber>'.$cccoTO->getCardNumber().'</rec:cardNumber>
					         <rec:expirationDate>'.$cccoTO->getExpirationDate().'</rec:expirationDate>
					         <rec:securityCode>'.$cccoTO->getSecurityCode().'</rec:securityCode>
					         <rec:numberInstallments>1</rec:numberInstallments>
					         <rec:paymentType>'.$cccoTO->getPaymentType().'</rec:paymentType>
					      </rec:CreateCreditCardOrder>
					   </soapenv:Body>
					</soapenv:Envelope>';
// 			Zend_Debug::dump($xml,__CLASS__.'('.__LINE__.')');
			$execucao 	= $this->_exeCutaSOAPCurl(utf8_encode($xml), self::AUTORIZETRANSACTION, $cccoTO->getUrlBraspag());
// 			Zend_Debug::dump($execucao,__CLASS__.'('.__LINE__.')');
			$mensageiro = $this->_lerSOAPXmlCurl($execucao, self::AUTORIZETRANSACTIONRESPONSE, 'https://www.pagador.com.br/webservice/recorrente');
// 			Zend_Debug::dump($mensageiro,__CLASS__.'('.__LINE__.')');
				
			if($mensageiro->getTipo()!=Ead1_IMensageiro::SUCESSO){
				throw new Zend_Exception('Erro ao Autorizar o pagamento: '. $mensageiro->getFirstMensagem());
			}
			
			$resultado = $this->_lerResultadoWebService($mensageiro->getFirstMensagem(), self::AUTORIZETRANSACTIONRESULT);
// 			Zend_Debug::dump($resultado,__CLASS__.'('.__LINE__.')');
			
			if($resultado->getTipo()!=Ead1_IMensageiro::SUCESSO){
				throw new Zend_Exception($resultado->getFirstMensagem());
			}

			return $resultado;		
		
		} catch (Exception $e) {
			return new Ead1_Mensageiro('Recorrente - '.$e->getMessage(), Ead1_IMensageiro::ERRO);
		}
		
		
	}
	

	
	/**
	 * Método para Ler o que vem do XML e traduzir isso para aplicação
	 * @param SimpleXMLElement $xml
	 * @param string $actionResult
	 * @return Ead1_Mensageiro
	 */
	private  function _lerResultadoWebService(SimpleXMLElement $xml, $actionResult){
	
		/**
		 * Evitando erros de Array vazio
		 */
		error_reporting(E_ALL & ~E_NOTICE);
	
	
		/**
		 * Convertendo o Objeto em Array
		 */
		$result = Braspag::object_to_array($xml);

		switch ($actionResult){

			case 'GetOrderResult':
				
				if(isset($result['GetOrderResult']['Code'])){
				
					switch ($result['GetOrderResult']['Code']) {
						case '0':
							return new Ead1_Mensageiro($result, Ead1_IMensageiro::SUCESSO);
						break;
				
						default:
							return new Ead1_Mensageiro($result['GetOrderResult']['Code'].' - '.$result['GetOrderResult']['Description'], Ead1_IMensageiro::AVISO, $result);
						break;
					}
				} else {
					return new Ead1_Mensageiro('Erro 02 ao tentar recuperar o pedido de recorrência.', Ead1_IMensageiro::ERRO, $result);
				};
				
			break;

            case 'ChangePaymentMethodCreditCardResponse':
                if(isset($result['ChangePaymentMethodCreditCardResult']['code'])){
                    switch ($result['ChangePaymentMethodCreditCardResult']['code']) {
                        case '0':
                            return new Ead1_Mensageiro('Cartão de crédito alteradoo com sucesso.', Ead1_IMensageiro::SUCESSO);
                            break;
                        default:
                            return new Ead1_Mensageiro($result['ChangePaymentMethodCreditCardResult']['code'].' - '.$result['ChangePaymentMethodCreditCardResult']['description'], Ead1_IMensageiro::AVISO, $result);
                            break;
                    }
                } else {
                    return new Ead1_Mensageiro('Erro ao tentar mudar o cartão de crédito da recorrência.', Ead1_IMensageiro::ERRO, $result);
                };
            break;

			case 'GetOrderRecurrenciesResult':

				if(isset($result['GetOrderRecurrenciesResult']['Code'])){
				
					switch ($result['GetOrderRecurrenciesResult']['Code']) {
						case '0':
							return new Ead1_Mensageiro($result, Ead1_IMensageiro::SUCESSO);
						break;
				
						default:
							return new Ead1_Mensageiro($result['GetOrderRecurrenciesResult']['Code'].' - '.$result['GetOrderRecurrenciesResult']['Description'], Ead1_IMensageiro::AVISO, $result);
						break;
					}
				
				} else {
					return new Ead1_Mensageiro('Erro 02 ao tentar recuperar as recorrências.', Ead1_IMensageiro::ERRO, $result);
				};
				
			break;	
			case self::AUTORIZETRANSACTIONRESULT:

				if(isset($result['CreateCreditCardOrderResult']['code'])){
	
					switch ($result['CreateCreditCardOrderResult']['code']) {
						case '0':
							return new Ead1_Mensageiro($result, Ead1_IMensageiro::SUCESSO);
							break;
	
						default:
							return new Ead1_Mensageiro($result['CreateCreditCardOrderResult']['description'], Ead1_IMensageiro::AVISO, $result);
							break;
					}
	
				} else {
					return new Ead1_Mensageiro('Erro ao tentar autorizar o pagamento recorrente.', Ead1_IMensageiro::ERRO, $result);
				};
	
			break; 
			
			case self::CANCELTRANSACTIONRESPONSE:
				
				if(isset($result['DisableOrderResult']['code'])){
					
					switch ($result['DisableOrderResult']['code']) {
						case '0':
							return new Ead1_Mensageiro($result, Ead1_IMensageiro::SUCESSO);
							break;
					
						default:
						return new Ead1_Mensageiro($result['DisableOrderResult']['description'], Ead1_IMensageiro::AVISO);
						break;
					}
					
				}
				
			break;
				
			default: return new Ead1_Mensageiro('Resultado nao configurado', Ead1_IMensageiro::ERRO);
	
		}
	
	
	
	}

	
	

	/**
	 * Cancela um pagamento recorrente
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @param DisableOrderTO $doTO
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function cancelar(DisableOrderTO $doTO) {
		
		
		try {
		
		
			$xml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:rec="https://www.pagador.com.br/webservice/recorrente">
			<soapenv:Header/>
			<soapenv:Body>
			<rec:DisableOrder>
			<rec:merchantId>'.$doTO->getMerchantId().'</rec:merchantId>
			<rec:orderId>'.$doTO->getOrderId().'</rec:orderId>
			</rec:DisableOrder>
			</soapenv:Body>
			</soapenv:Envelope>';
		
			$execucao 	= $this->_exeCutaSOAPCurl(utf8_encode($xml), self::CANCELTRANSACTION, $doTO->getUrlBraspag());
			$mensageiro = $this->_lerSOAPXmlCurl($execucao, self::CANCELTRANSACTIONRESPONSE, 'https://www.pagador.com.br/webservice/recorrente');
		
			if($mensageiro->getTipo()!=Ead1_IMensageiro::SUCESSO){
				throw new Zend_Exception('Erro ao cancelar o pagamento recorrente: '. $mensageiro->getFirstMensagem());
			}
		
			$resultado = $this->_lerResultadoWebService($mensageiro->getFirstMensagem(), self::CANCELTRANSACTIONRESPONSE);
		
			if($resultado->getTipo()!=Ead1_IMensageiro::SUCESSO){
				throw new Zend_Exception($resultado->getFirstMensagem());
			}
		
			return $resultado;
		
		} catch (Exception $e) {
			return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
		}
				
	}

	
	
	
	
	
	/**
	 * Retorna todos os detalhes de todas as recorrências de um pedido.
	 * @param GetOrderRecurrenciesTO $to
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 * 
	 * Lista de Retornos possíveis no Status da Recorrencia
	 * 0 - Pagamento confirmado 
	 * 1 - Pagamento Autorizado
	 * 2 - Negado, vai tentar novamente
	 * 3 - Negado, não agendar retentativa
	 * 4 - Negado, não tentar novamente.
	 * 5 - Negado, não tentar novamente, pedido desabilitado.
	 * 6 - Aguardando resposta
	 * 7 - Desqualificado (erro desqualificante, como cartão vencido)
	 * 8 - Erro interno
	 * 	  
	 */
	public function GetOrderRecurrencies(GetOrderRecurrenciesTO $to){
		
		try {

		
			$xml = '<?xml version="1.0" encoding="utf-8"?>
					<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
					  <soap:Body>
					    <GetOrderRecurrencies xmlns="https://www.pagador.com.br/webservice/recorrente">
					      <merchantId>'.$to->getMerchantId().'</merchantId>
					      <orderId>'.$to->getOrderId().'</orderId>
					    </GetOrderRecurrencies>
					  </soap:Body>
					</soap:Envelope>';
		
			$execucao 	= $this->_exeCutaSOAPCurl(utf8_encode($xml), 'GetOrderRecurrencies', $to->getUrlBraspag());
			$mensageiro = $this->_lerSOAPXmlCurl($execucao, 'GetOrderRecurrenciesResponse', 'https://www.pagador.com.br/webservice/recorrente');

			if($mensageiro->getTipo()!=Ead1_IMensageiro::SUCESSO){
				throw new Zend_Exception('Erro 01 ao tentar recuperar as recorrências.: '. $mensageiro->getFirstMensagem());
			}
		
			$resultado = $this->_lerResultadoWebService($mensageiro->getFirstMensagem(), 'GetOrderRecurrenciesResult');
		
			if($resultado->getTipo()!=Ead1_IMensageiro::SUCESSO){
				throw new Zend_Exception($resultado->getFirstMensagem());
			}
		
			return $resultado;
		
		} catch (Exception $e) {
			return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
		}
		
	}

	
	/**
	 * Retorna os detalhes de um pedido
	 * @param GetOrderTO $to
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 * 
	 * Lista dos Status possíveis
	 * 0 - Desabilitado pelo usuário
	 * 1 - Desabilitado, número máximo de tentativas alcançado.
	 * 2 - Desabilitado por erro desqualificante
	 * 3 - Habilitado
	 * 4 - Aguardando resposta
	 * 5 - Desabilitado, aguardando resposta
	 * 
	 */
	public function getOrder(GetOrderTO $to){
	
	
		try {
	
	
			$xml = '<?xml version="1.0" encoding="utf-8"?>
			<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
			<soap:Body>
			<GetOrder xmlns="https://www.pagador.com.br/webservice/recorrente">
			<merchantId>'.$to->getMerchantId().'</merchantId>
			<orderId>'.$to->getOrderId().'</orderId>
			</GetOrder>
			</soap:Body>
			</soap:Envelope>';
	
			$execucao 	= $this->_exeCutaSOAPCurl(utf8_encode($xml), 'GetOrder', $to->getUrlBraspag());
			$mensageiro = $this->_lerSOAPXmlCurl($execucao, 'GetOrderResponse', 'https://www.pagador.com.br/webservice/recorrente');
	
			if($mensageiro->getTipo()!=Ead1_IMensageiro::SUCESSO){
				throw new Zend_Exception('Erro 02 ao tentar recuperar o pedido de recorrência: '. $mensageiro->getFirstMensagem());
			}
			$resultado = $this->_lerResultadoWebService($mensageiro->getFirstMensagem(), 'GetOrderResult');
	
			if($resultado->getTipo()!=Ead1_IMensageiro::SUCESSO){
				throw new Zend_Exception($resultado->getFirstMensagem());
			}
	
			return $resultado;
	
		} catch (Exception $e) {
			return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
		}
	
	}

    /**
     * Altera o método de pagamento (número do cartão de crédito) do pedido para o recorrente
     * @author Gabriel Resende (gabriel.resende@unyleya.com.br)
     * @param CreateCreditCardOrderTO $cccoTO
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function changePaymentMethodCreditCard(CreateCreditCardOrderTO $cccoTO) {

        try {

            $xml = '<?xml version="1.0" encoding="utf-8"?>
                        <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                          <soap:Body>
                            <ChangePaymentMethodCreditCard xmlns="https://www.pagador.com.br/webservice/recorrente">
                              <merchantId>'.$cccoTO->getMerchantId().'</merchantId>
                              <orderId>'.$cccoTO->getOrderId().'</orderId>
                              <paymentMethod>'.$cccoTO->getPaymentMethod().'</paymentMethod>
                              <holder>string</holder>
                              <cardNumber>'.$cccoTO->getCardNumber().'</cardNumber>
                              <expirationDate>'.$cccoTO->getExpirationDate().'</expirationDate>
                              <securityCode>'.$cccoTO->getSecurityCode().'</securityCode>
                              <numberInstallments>1</numberInstallments>
                              <paymentType>'.$cccoTO->getPaymentType().'</paymentType>
                            </ChangePaymentMethodCreditCard>
                          </soap:Body>
                        </soap:Envelope>';

 			//Zend_Debug::dump($xml,__CLASS__.'('.__LINE__.')');exit;
            $execucao 	= $this->_exeCutaSOAPCurl(utf8_encode($xml), 'ChangePaymentMethodCreditCard', $cccoTO->getUrlBraspag());
 			//Zend_Debug::dump($execucao,__CLASS__.'('.__LINE__.')');
            $mensageiro = $this->_lerSOAPXmlCurl($execucao, 'ChangePaymentMethodCreditCardResponse', 'https://www.pagador.com.br/webservice/recorrente');
            //Zend_Debug::dump($mensageiro,__CLASS__.'('.__LINE__.')');//exit;
            if($mensageiro->getTipo()!=Ead1_IMensageiro::SUCESSO){
                $mensagem = $mensageiro->getFirstMensagem();
                throw new Zend_Exception('Erro ao Autorizar o pagamento: '. $mensageiro->getFirstMensagem());
            }

            $resultado = $this->_lerResultadoWebService($mensageiro->getFirstMensagem(), 'ChangePaymentMethodCreditCardResponse');
            //Zend_Debug::dump($resultado,__CLASS__.'('.__LINE__.')');
            if($resultado->getTipo()!=Ead1_IMensageiro::SUCESSO){
                throw new Zend_Exception($resultado->getFirstMensagem());
            }

            return $resultado;

        } catch (Exception $e) {
            //Zend_Debug::dump($resultado,__CLASS__.'('.__LINE__.')');exit;
            return new Ead1_Mensageiro('Recorrente - '.$e->getMessage(), Ead1_IMensageiro::ERRO);
        }

    }

}

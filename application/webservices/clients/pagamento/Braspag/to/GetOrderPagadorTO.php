<?php

/**
 * @author Elcio Mauro Guimar�es - elcioguimaraes@gmail.com
 */
class GetOrderPagadorTO {
	
	

	
	/**
	 * Identificador da Loja na Braspag
	 * @var Guid
	 */
	private $MerchantId;
	
	
	/**
	 * Número do pedido na Braspag
	 * @var Guid
	 */
	private $BraspagOrderId;
	
	/**
	 * Url de Pagamento da Braspag
	 * @var String
	 */
	private $UrlBraspag;
	
	
	/**
	 * @return the $RequestId
	 */
	public function getRequestId() {
		if (function_exists('com_create_guid')){
			return com_create_guid();
		}else{
			mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
			$charid = strtoupper(md5(uniqid(rand(), true)));
			$hyphen = chr(45);// "-"
			$uuid = chr(123)// "{"
			.substr($charid, 0, 8).$hyphen
			.substr($charid, 8, 4).$hyphen
			.substr($charid,12, 4).$hyphen
			.substr($charid,16, 4).$hyphen
			.substr($charid,20,12)
			.chr(125);// "}"
			return $uuid;
		}
	}

	/**
	 * @return the $MerchantId
	 */
	public function getMerchantId() {
		return $this->MerchantId;
	}

	/**
	 * @return the $BraspagOrderId
	 */
	public function getBraspagOrderId() {
		return $this->BraspagOrderId;
	}

	/**
	 * @return the $UrlBraspag
	 */
	public function getUrlBraspag() {
		return $this->UrlBraspag;
	}


	/**
	 * @param Guid $MerchantId
	 */
	public function setMerchantId($MerchantId) {
		$this->MerchantId = $MerchantId;
	}

	/**
	 * @param Guid $BraspagOrderId
	 */
	public function setBraspagOrderId($BraspagOrderId) {
		$this->BraspagOrderId = $BraspagOrderId;
	}

	/**
	 * @param string $UrlBraspag
	 */
	public function setUrlBraspag($UrlBraspag) {
		$this->UrlBraspag = $UrlBraspag;
	}

	


}

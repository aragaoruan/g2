<?php

/**
 * @author Elcio Mauro Guimar�es - elcioguimaraes@gmail.com
 */
class VoidCreditCardTransactionTO {
	
	
	
	/**
	 * Identificador da Loja na Braspag
	 * @var String
	 */
	private $MerchantId;
	
	/**
	 * Identificador da Compra na Braspag
	 * @var String
	 */
	private $BraspagTransactionId;
	
	/**
	 * Valor da Compra
	 * Para uma compra de R$ 1,00, passar o valor 100 
	 * @var int
	 */
	private $Amount;
	
	/**
	 * Identificador �nico para a transa��o
	 * Formado: GUID - 3F2504E0-4F89-11D3-9A0C-0305E82C3301
	 * @var String
	 */
	private $RequestId;
	
	/**
	 * Url de Pagamento da Braspag
	 * @var String
	 */
	private $UrlBraspag;
	
	
	/**
	 * @return the $UrlBraspag
	 */
	public function getUrlBraspag() {
		return $this->UrlBraspag;
	}
	
	/**
	 * @param string $UrlBraspag
	 */
	public function setUrlBraspag($UrlBraspag) {
		$this->UrlBraspag = $UrlBraspag;
	}
	
	/**
	 * @return the $MerchantId
	 */
	public function getMerchantId() {
		return $this->MerchantId;
	}

	/**
	 * @return the $BraspagTransactionId
	 */
	public function getBraspagTransactionId() {
		return $this->BraspagTransactionId;
	}

	/**
	 * @return the $Amount
	 */
	public function getAmount() {
		return $this->Amount;
	}

	/**
	 * @return the $RequestId
	 */
	public function getRequestId() {
		if (function_exists('com_create_guid')){
			return com_create_guid();
		}else{
			mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
			$charid = strtoupper(md5(uniqid(rand(), true)));
			$hyphen = chr(45);// "-"
			$uuid = chr(123)// "{"
			.substr($charid, 0, 8).$hyphen
			.substr($charid, 8, 4).$hyphen
			.substr($charid,12, 4).$hyphen
			.substr($charid,16, 4).$hyphen
			.substr($charid,20,12)
			.chr(125);// "}"
			return $uuid;
		}
	}

	/**
	 * @param string $MerchantId
	 */
	public function setMerchantId($MerchantId) {
		$this->MerchantId = $MerchantId;
	}

	/**
	 * @param string $BraspagTransactionId
	 */
	public function setBraspagTransactionId($BraspagTransactionId) {
		$this->BraspagTransactionId = $BraspagTransactionId;
	}

	/**
	 * @param number $Amount
	 */
	public function setAmount($Amount) {
		$this->Amount =  number_format($Amount,0,'','');
	}



}

<?php

/**
 * @author Elcio Mauro Guimar�es - elcioguimaraes@gmail.com
 */
class GetOrderRecurrenciesTO {
	
	
	
	/**
	 * Identificador da Loja na Braspag
	 * @var String
	 */
	private $MerchantId;
	

	/**
	 * Identificador da Compra
	 * @var Int
	 */
	private $OrderId;
	
	/**
	 * Url de Pagamento da Braspag
	 * @var String
	 */
	private $UrlBraspag;
	
	
	/**
	 * @return the $MerchantId
	 */
	public function getMerchantId() {
		return $this->MerchantId;
	}

	/**
	 * @return the $OrderId
	 */
	public function getOrderId() {
		return $this->OrderId;
	}

	/**
	 * @return the $UrlBraspag
	 */
	public function getUrlBraspag() {
		return $this->UrlBraspag;
	}

	/**
	 * @param string $MerchantId
	 */
	public function setMerchantId($MerchantId) {
		$this->MerchantId = $MerchantId;
	}

	/**
	 * @param number $OrderId
	 */
	public function setOrderId($OrderId) {
		$this->OrderId = $OrderId;
	}

	/**
	 * @param string $UrlBraspag
	 */
	public function setUrlBraspag($UrlBraspag) {
		$this->UrlBraspag = $UrlBraspag;
	}

	
	


}

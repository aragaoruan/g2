<?php

/**
 * Classe utilizada para o Pagamento Recorrente na Braspag
 * @author Elcio Mauro Guimaraes - elcioguimaraes@gmail.com
 */
class CreateCreditCardOrderTO {
	
	/**
	 * Intervalo de Pagamento
	 */
	const INTERVALO_MENSAL 		= 1;
	const INTERVALO_TRIMESTRAL 	= 3;
	const INTERVALO_SEMESTRAL 	= 6;
	const INTERVALO_ANUAL 		= 12;
	
	/**
	 * Tipo de pagamento
	 */
	const TIPO_PAGAMENTO_A_VISTA 			= 0; //0 = � vista
	const TIPO_PAGAMENTO_PARCELADO_LOJA 	= 1; //1 = parcelamento pela loja
	const TIPO_PAGAMENTO_PARCELADO_CARTAO 	= 2; //2 = parcelamento pelo emissor do cart�o
	
	/**
	 * Identificador da Loja na Braspag
	 * @var String
	 */
	private $merchantId;
	
	/**
	 * Identificador da Compra na Loja
	 * @var String
	 */
	private $orderId;
	
	/**
	 * Data de in�cio dos debitos no Cartao de Credito
	 * @var date - format DD/MM/YYYY
	 */
	private $startDate;
	
	/**
	 * Data de termino dos debitos no Cartao de Credito
	 * @var date - format DD/MM/YYYY
	 */
	private $endDate;
	
	/**
	 * Intervalo de Pagamento
	 * 
	 * const INTERVALO_MENSAL 		= 1;
	 * const INTERVALO_TRIMESTRAL 	= 3;
	 * const INTERVALO_SEMESTRAL 	= 6;
	 * const INTERVALO_ANUAL 		= 12;
	 * 
	 * @var int
	 */
	private $recurrenceInterval;
	
	/**
	 * Valor do debito
	 * @var float  - Valor total, formato 100 = 1,00
	 */
	private $amount;
	
	/**
	 * Retorna o Codigo correspondente ao paymentMethod
	 * 0 American Express
	 * 1 VISA TEF
	 * 2 American Express TEF
	 * 3 Redecard Webservice
	 * 27 American Express Arquivo
	 * 28 VISA Arquivo
	 * 29 Redecard Arquivo
	 * 30 Mastercard TEF
	 * 31 Diners TEF
	 * 33 Boleto Itau com Registro
	 * 34 VISA Moset
	 * 35 Boleto Unibanco
	 * 36 Boleto Bradesco
	 * 37 Boleto Ita�
	 * 40 Boleto Banco do Brasil
	 * 41 Hipercard
	 * 44 Boleto HSBC
	 * 46 Visa Sitef
	 * 47 Hipercard SiTef
	 * 48 American Express SiTef
	 * 49 Mastercard SiTef
	 * 50 Diners SiTef
	 * 71 Cielo VISA
	 * 72 Cielo DINERS
	 * 73 Cielo AMEX
	 * 120 Cielo MASTERCARD
	 * 126 Cielo ELO
	 * @var int 
	 */
	private $paymentMethod;
	
	/**
	 * Nome impresso no Cartao
	 * @var String
	 */
	private $holder;
	
	/**
	 * Numero do Cartao
	 * @var String
	 */
	private $cardNumber;
	
	/**
	 * Data de expiracao do cartao
	 * @var date format - DD/YYYY
	 */
	private $expirationDate;
	
	/**
	 * Codigo de segurancao do cartao
	 * @var string
	 */
	private $securityCode;
	
	/**
	 * Numero de parcelas
	 * @var int
	 */
	private $numberInstallments;
	
	/**
	 * Tipo de pagamento
	 * const TIPO_PAGAMENTO_A_VISTA 			= 0; //0 = a vista
	 * const TIPO_PAGAMENTO_PARCELADO_LOJA 		= 1; //1 = parcelamento pela loja
	 * const TIPO_PAGAMENTO_PARCELADO_CARTAO 	= 2; //2 = parcelamento pelo emissor do cartao
	 */
	private $paymentType;
	
	/**
	 * Url de Pagamento da Braspag
	 * @var String
	 */
	private $UrlBraspag;
	
	
	/**
	 * @return the $merchantId
	 */
	public function getMerchantId() {
		return $this->merchantId;
	}

	/**
	 * @return the $orderId
	 */
	public function getOrderId() {
		return $this->orderId;
	}

	/**
	 * @return the $startDate
	 */
	public function getStartDate() {
		return $this->startDate;
	}

	/**
	 * @return the $endDate
	 */
	public function getEndDate() {
		return $this->endDate;
	}

	/**
	 * @return the $recurrenceInterval
	 */
	public function getRecurrenceInterval() {
		return $this->recurrenceInterval;
	}

	/**
	 * @return the $amount
	 */
	public function getAmount() {
		return $this->amount;
	}

	/**
	 * @return the $paymentMethod
	 */
	public function getPaymentMethod() {
		return $this->paymentMethod;
	}

	/**
	 * @return the $holder
	 */
	public function getHolder() {
		return $this->holder;
	}

	/**
	 * @return the $cardNumber
	 */
	public function getCardNumber() {
		return $this->cardNumber;
	}

	/**
	 * @return the $expirationDate
	 */
	public function getExpirationDate() {
		return $this->expirationDate;
	}

	/**
	 * @return the $securityCode
	 */
	public function getSecurityCode() {
		return $this->securityCode;
	}

	/**
	 * @return the $numberInstallments
	 */
	public function getNumberInstallments() {
		return $this->numberInstallments;
	}

	/**
	 * @return the $paymentType
	 */
	public function getPaymentType() {
		if(!$this->paymentType){
			$this->setPaymentType(self::TIPO_PAGAMENTO_A_VISTA);
		}
		return $this->paymentType;
	}

	/**
	 * @param string $merchantId
	 */
	public function setMerchantId($merchantId) {
		$this->merchantId = $merchantId;
	}

	/**
	 * @param string $orderId
	 */
	public function setOrderId($orderId) {
		$this->orderId = $orderId;
	}

	/**
	 * @param date $startDate
	 */
	public function setStartDate($startDate) {
		$this->startDate = str_pad($startDate, 2, "0", STR_PAD_LEFT);
	}

	/**
	 * @param date $endDate
	 */
	public function setEndDate($endDate) {
		
		$this->endDate = ($endDate ? str_pad($endDate, 2, "0", STR_PAD_LEFT) : '');
		
	}

	/**
	 * @param number $recurrenceInterval
	 */
	public function setRecurrenceInterval($recurrenceInterval) {
		$this->recurrenceInterval = $recurrenceInterval;
	}

	/**
	 * @param number $amount
	 */
	public function setAmount($amount) {
		$this->amount = number_format($amount,2,',','');
	}

	/**
	 * @param number $paymentMethod
	 */
	public function setPaymentMethod($paymentMethod) {
		$this->paymentMethod = $paymentMethod;
	}

	/**
	 * @param string $holder
	 */
	public function setHolder($holder) {
		$this->holder = $holder;
	}

	/**
	 * @param string $cardNumber
	 */
	public function setCardNumber($cardNumber) {
		$this->cardNumber = $cardNumber;
	}

	/**
	 * @param date $expirationDate
	 */
	public function setExpirationDate($expirationDate) {
		$this->expirationDate = $expirationDate;
	}

	/**
	 * @param string $securityCode
	 */
	public function setSecurityCode($securityCode) {
		$this->securityCode = $securityCode;
	}

	/**
	 * @param number $numberInstallments
	 */
	public function setNumberInstallments($numberInstallments) {
		$this->numberInstallments = $numberInstallments;
	}

	/**
	 * @param field_type $paymentType
	 */
	public function setPaymentType($paymentType) {
		$this->paymentType = $paymentType;
	}

	/**
	 * @return the $UrlBraspag
	 */
	public function getUrlBraspag() {
		return $this->UrlBraspag;
	}

	/**
	 * @param string $UrlBraspag
	 */
	public function setUrlBraspag($UrlBraspag) {
		$this->UrlBraspag = $UrlBraspag;
	}

}


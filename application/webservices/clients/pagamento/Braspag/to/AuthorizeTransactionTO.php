<?php

/**
 * @author Elcio Mauro Guimaraes - elcioguimaraes@gmail.com
 */
class AuthorizeTransactionTO {
	
	const CIELO_VISA 			= 500;
	const CIELO_MASTERCARD 		= 501;
	const CIELO_AMEX 			= 502;
	
	const REDECARD_VISA			= 509;
	const REDECARD_MASTERCARD	= 510;
	const REDECARD_DINERS		= 511;
	
	const SIMULADO				= 997;
	
	const CIELO 	= 1;
	const REDECARD 	= 2;
	
	/*
	 * Dados da Compra
	 * <OrderData>
	 */
	
	/**
	 * Identificador da Loja na Braspag
	 * @var String
	 */
	private $MerchantId;
	
	/**
	 * Identificador da Compra
	 * @var Int
	 */
	private $OrderId;
	
	
	/*
	 * Dados do Comprador
	 * <CustomerData>
	 */
	
	/**
	 * Identificador do Cliente na Loja
	 * @var String
	 */
	private $CustomerIdentity;
	
	/**
	 * Nome do Cliente
	 * @var String
	 */
	private $CustomerName;
	
	/**
	 * E-mail do Cliente 
	 * @var String
	 */
	private $CustomerEmail;

	/*
	 * Dados do Cartao de Credito
	 */
	
	/**
	 * Bandeira do Cartao
	 * const CIELO_VISA 			= 500;
	 * const CIELO_MASTERCARD 		= 501;
	 * const CIELO_AMEX 			= 502;
	 * const REDECARD_VISA			= 509;
	 * const REDECARD_MASTERCARD	= 510;
	 * const REDECARD_DINERS		= 511;
	 * @var int
	 */
	private $PaymentMethod;
	
	/**
	 * Valor da Compra
	 * Para uma compra de R$ 1,00, passar o valor 100 
	 * @var int
	 */
	private $Amount;
	
	/**
	 * Moeda
	 * @var String
	 */
	private $Currency 	= 'BRL'; //BRL
	
	/**
	 * Pa�s
	 * @var String
	 */
	private $Country	= 'BRA'; //BRA
	
	/**
	 * N�mero de pagamentos
	 * @var int
	 */
	private $NumberOfPayments;
	
	/**
	 * Plano de Pagamento
	 * 0 - A vista 
	 * 1 - Parcelado
	 * @var int
	 */
	private $PaymentPlan;
	
	
	/**
	 * Tipo de Transacao
	 * 2 - Captura Automatica
	 * @var int
	 */
	private $TransactionType = 2;
	
	/**
	 * Nome do Cliente como esta IMPRESSO no Cartao de Credito
	 * @var String
	 */
	private $CardHolder;
	
	/**
	 * Numero do Cartao de Credito
	 * @var String
	 */
	private $CardNumber;
	
	/**
	 * Codigo de seguranca do Cartao de Credito
	 * @var String
	 */
	private $CardSecurityCode;
	
	/**
	 * Data de Expiracao do Cartao de Credito
	 * Formato MM/YYYY
	 * @var String
	 */
	private $CardExpirationDate; //MM/YY
	
	/**
	 * Identificador Unico para a transacao
	 * Formado: GUID - 3F2504E0-4F89-11D3-9A0C-0305E82C3301
	 * @var String
	 */
	private $RequestId;
	
	/**
	 * Url de Pagamento da Braspag
	 * @var String
	 */
	private $UrlBraspag;
	
	
	
	/**
	 * @author Elcio Mauro Guimaraes - elcioguimaraes@gmail.com
	 * @param array $dados
	 */
	public function __construct(array $dados = null) {
		
		if($dados){
			foreach($dados as $propriedade => $valor){
				if(!empty($propriedade)){
					$propriedade = trim($propriedade);
					$metodo = 'set'.$propriedade;
					if(method_exists($this, $metodo)){
						$this->$metodo($valor);
					}
				}
			}
		}
			
	}
	
	
	/**
	 * @return the $UrlBraspag
	 */
	public function getUrlBraspag() {
		return $this->UrlBraspag;
	}

	/**
	 * @param string $UrlBraspag
	 */
	public function setUrlBraspag($UrlBraspag) {
		$this->UrlBraspag = $UrlBraspag;
	}

	/**
	 * @return the $CardNumber
	 */
	public function getCardNumber() {
		return $this->CardNumber;
	}

	/**
	 * @param string $CardNumber
	 */
	public function setCardNumber($CardNumber) {
		$this->CardNumber = $CardNumber;
	}

	/**
	 * @return the $MerchantId
	 */
	public function getMerchantId() {
		return $this->MerchantId;
	}

	/**
	 * @return the $OrderId
	 */
	public function getOrderId() {
		return $this->OrderId;
	}

	/**
	 * @return the $CustomerIdentity
	 */
	public function getCustomerIdentity() {
		return $this->CustomerIdentity;
	}

	/**
	 * @return the $CustomerName
	 */
	public function getCustomerName() {
		return $this->CustomerName;
	}

	/**
	 * @return the $CustomerEmail
	 */
	public function getCustomerEmail() {
		return $this->CustomerEmail;
	}

	/**
	 * @return the $PaymentMethod
	 */
	public function getPaymentMethod() {
		return $this->PaymentMethod;
	}

	/**
	 * @return the $Amount
	 */
	public function getAmount() {
		return $this->Amount;
	}

	/**
	 * @return the $Currency
	 */
	public function getCurrency() {
		return $this->Currency;
	}

	/**
	 * @return the $Country
	 */
	public function getCountry() {
		return $this->Country;
	}

	/**
	 * @return the $NumberOfPayments
	 */
	public function getNumberOfPayments() {
		return $this->NumberOfPayments;
	}

	/**
	 * @return the $PaymentPlan
	 */
	public function getPaymentPlan() {
		return $this->PaymentPlan;
	}

	/**
	 * @return the $TransactionType
	 */
	public function getTransactionType() {
		return $this->TransactionType;
	}

	/**
	 * @return the $CardHolder
	 */
	public function getCardHolder() {
		return $this->CardHolder;
	}

	/**
	 * @return the $CardSecurityCode
	 */
	public function getCardSecurityCode() {
		return $this->CardSecurityCode;
	}

	/**
	 * @return the $CardExpirationDate
	 */
	public function getCardExpirationDate() {
		return $this->CardExpirationDate;
	}

	/**
	 * @return the $RequestId
	 */
	public function getRequestId() {
		if (function_exists('com_create_guid')){
			return com_create_guid();
		}else{
			mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
			$charid = strtoupper(md5(uniqid(rand(), true)));
			$hyphen = chr(45);// "-"
			$uuid = chr(123)// "{"
			.substr($charid, 0, 8).$hyphen
			.substr($charid, 8, 4).$hyphen
			.substr($charid,12, 4).$hyphen
			.substr($charid,16, 4).$hyphen
			.substr($charid,20,12)
			.chr(125);// "}"
			return $uuid;
		}
	}

	/**
	 * @param string $MerchantId
	 */
	public function setMerchantId($MerchantId) {
		$this->MerchantId = $MerchantId;
	}

	/**
	 * @param number $OrderId
	 */
	public function setOrderId($OrderId) {
		$this->OrderId = $OrderId;
	}

	/**
	 * @param string $CustomerIdentity
	 */
	public function setCustomerIdentity($CustomerIdentity) {
		$this->CustomerIdentity = $CustomerIdentity;
	}

	/**
	 * @param string $CustomerName
	 */
	public function setCustomerName($CustomerName) {
		$this->CustomerName = $CustomerName;
	}

	/**
	 * @param string $CustomerEmail
	 */
	public function setCustomerEmail($CustomerEmail) {
		$this->CustomerEmail = $CustomerEmail;
	}

	/**
	 * @param number $PaymentMethod
	 */
	public function setPaymentMethod($PaymentMethod) {
		$this->PaymentMethod = $PaymentMethod;
	}

	/**
	 * @param number $Amount
	 */
	public function setAmount($Amount) {
		$this->Amount =  $Amount;
	}

	/**
	 * @param string $Currency
	 */
	public function setCurrency($Currency) {
		$this->Currency = $Currency;
	}

	/**
	 * @param string $Country
	 */
	public function setCountry($Country) {
		$this->Country = $Country;
	}

	/**
	 * @param number $NumberOfPayments
	 */
	public function setNumberOfPayments($NumberOfPayments) {
		$this->NumberOfPayments = $NumberOfPayments;
	}

	/**
	 * @param number $PaymentPlan
	 */
	public function setPaymentPlan($PaymentPlan) {
		$this->PaymentPlan = $PaymentPlan;
	}

	/**
	 * @param number $TransactionType
	 */
	public function setTransactionType($TransactionType) {
		$this->TransactionType = $TransactionType;
	}

	/**
	 * @param string $CardHolder
	 */
	public function setCardHolder($CardHolder) {
		$this->CardHolder = $CardHolder;
	}

	/**
	 * @param string $CardSecurityCode
	 */
	public function setCardSecurityCode($CardSecurityCode) {
		$this->CardSecurityCode = $CardSecurityCode;
	}

	/**
	 * @param string $CardExpirationDate
	 */
	public function setCardExpirationDate($CardExpirationDate) {
		$this->CardExpirationDate = $CardExpirationDate;
	}

}

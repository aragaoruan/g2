<?php

/**
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 */
class CaptureCreditCardTransactionTO {
	
	/**
	 * É o campo identificador do pedido no formato: {00000000-0000-0000-0000-000000000000}
	 * @var Guid
	 */
	private $RequestId;
	
	/**
	 * Versão do contrato do Webservice (ex.: “1.0”)
	 * @var String
	 */
	private $Version = '1.0';
	
	/**
	 * É o campo identificador da loja no formato: {00000000-0000-0000-0000-000000000000}
	 * @var Guid
	 */
	private $MerchantId;
	
	/**
	 * Numero da transação na Braspag
	 * @var String
	 */
	private $BraspagTransactionId;
	
	/**
	 * Valor a ser operado (ex.: R$1,00 = 100)
	 * @var int
	 */
	private $Amount;
	
	/**
	 * Url do Serviço na Braspag
	 * @var String
	 */
	private $UrlBraspag;
	/**
	 * @return the $RequestId
	 */
	public function getRequestId() {
		
		if (function_exists('com_create_guid')){
			return com_create_guid();
		}else{
			mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
			$charid = strtoupper(md5(uniqid(rand(), true)));
			$hyphen = chr(45);// "-"
			$uuid = chr(123)// "{"
			.substr($charid, 0, 8).$hyphen
			.substr($charid, 8, 4).$hyphen
			.substr($charid,12, 4).$hyphen
			.substr($charid,16, 4).$hyphen
			.substr($charid,20,12)
			.chr(125);// "}"
			return $uuid;
		}
		
	}

	/**
	 * @return the $Version
	 */
	public function getVersion() {
		return $this->Version;
	}

	/**
	 * @return the $MerchantId
	 */
	public function getMerchantId() {
		return $this->MerchantId;
	}

	/**
	 * @return the $BraspagTransactionId
	 */
	public function getBraspagTransactionId() {
		return $this->BraspagTransactionId;
	}

	/**
	 * @return the $Amount
	 */
	public function getAmount() {
		return $this->Amount;
	}

	/**
	 * @return the $UrlBraspag
	 */
	public function getUrlBraspag() {
		return $this->UrlBraspag;
	}


	/**
	 * @param string $Version
	 */
	public function setVersion($Version) {
		$this->Version = $Version;
	}

	/**
	 * @param Guid $MerchantId
	 */
	public function setMerchantId($MerchantId) {
		$this->MerchantId = $MerchantId;
	}

	/**
	 * @param string $BraspagTransactionId
	 */
	public function setBraspagTransactionId($BraspagTransactionId) {
		$this->BraspagTransactionId = $BraspagTransactionId;
	}

	/**
	 * @param number $Amount
	 */
	public function setAmount($Amount) {
		$this->Amount = $Amount;
	}

	/**
	 * @param string $UrlBraspag
	 */
	public function setUrlBraspag($UrlBraspag) {
		$this->UrlBraspag = $UrlBraspag;
	}


}

<?php

/**
 * Classe utilizada para o CANCELAMENTO de um Pagamento Recorrente na Braspag
 * @author Elcio Mauro Guimar�es - elcioguimaraes@gmail.com
 */
class DisableOrderTO {
	
	
	/**
	 * Identificador da Loja na Braspag
	 * @var String
	 */
	private $merchantId;
	
	/**
	 * Identificador da Compra na Loja
	 * @var String
	 */
	private $orderId;
	
	
	/**
	 * Url de Pagamento da Braspag
	 * @var String
	 */
	private $UrlBraspag;
	
	/**
	 * @return the $merchantId
	 */
	public function getMerchantId() {
		return $this->merchantId;
	}

	/**
	 * @return the $orderId
	 */
	public function getOrderId() {
		return $this->orderId;
	}

	/**
	 * @param string $merchantId
	 */
	public function setMerchantId($merchantId) {
		$this->merchantId = $merchantId;
	}

	/**
	 * @param string $orderId
	 */
	public function setOrderId($orderId) {
		$this->orderId = $orderId;
	}

	/**
	 * @return the $UrlBraspag
	 */
	public function getUrlBraspag() {
		return $this->UrlBraspag;
	}
	
	/**
	 * @param string $UrlBraspag
	 */
	public function setUrlBraspag($UrlBraspag) {
		$this->UrlBraspag = $UrlBraspag;
	}

}


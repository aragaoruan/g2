<?php

/**
 * Braspag Pagador - Pagamento comum para Cartao de Credito
 * @author Elcio Mauro Guimaraes - elcioguimaraes@gmail.com
 * @package package_name
 *
 */
class Braspag_Pagador extends Braspag {
	
	var $entidade = null;
	var $client   = null;
	
	const AUTORIZETRANSACTIONRESULT 			= 'AuthorizeTransactionResult';
	const AUTORIZETRANSACTION 					= 'AuthorizeTransaction';
	const AUTORIZETRANSACTIONRESPONSE 			= 'AuthorizeTransactionResponse';
	
	const VOIDCREDITCARDTRANSACTIONRESULT 		= 'VoidCreditCardTransactionResult';
	const VOIDCREDITCARDTRANSACTION				= 'VoidCreditCardTransaction';
	const VOIDCREDITCARDTRANSACTIONRESPONSE 	= 'VoidCreditCardTransactionResponse';
	
	const CAPTURECREDITCARDTRANSACTIONRESULT 	= 'CaptureCreditCardTransactionResult';
	const CAPTURECREDITCARDTRANSACTION			= 'CaptureCreditCardTransaction';
	const CAPTURECREDITCARDTRANSACTIONRESPONSE 	= 'CaptureCreditCardTransactionResponse';
	
	
	/**
	 * Captura
	 * Conceito utilizado para Cartoes de Credito. 
	 * a segunda perna da transacao, apos a autorizacao, a transacao deve ser capturada, isto eh, deve-se confirmar que ela realmente ira ocorrer.
	 * @var int
	 */
	const TRANS_CAPTURADA 		= 0;
	
	/**
	 * autorizacao
	 * Conceito utilizado apenas para Cartoes de Credito e dehbito online. No caso do cartao, a autorizacao eh a primeira perna da transacao. 
	 * Apenas verifica se o cartao eh valido, se tem limite e retehm o limite ateh segunda ordem (por no maximo 5 dias corridos).
	 * @var int
	 */
	const TRANS_AUTORIZADA		= 1;
	
	/**
	 * @var int
	 */
	const TRANS_NAO_AUTORIZADA 	= 2;
	
	/**
	 * @var int
	 */
	const TRANS_DESQUALIFICANTE = 3;
	
	/**
	 * @var int
	 */
	const TRANS_AGUARDANDO 		= 4;
	
	
	var $erro = array (
			'110' => 'O Valor precisa ser maior que ZERO.',
			'111' => 'O Numero do Cartao e OBRIGATORIO.',
			'112' => 'A Validade do Cartao e OBRIGATORIA.',
			'113' => 'Validade do Cartao INVALIDA.',
			'114' => 'Quantidade de Parcelas INVALIDO.',
			'115' => 'Meio de pagamento INVALIDO, entre em contato com a Instituicao.',
			'118' => 'Tipo de Transacao INVALIDA.',
			'121' => 'O Nome e OBRIGAT�RIO.',
			'126' => 'Metodo de pagamento nao habillitado, entre em contato com a Instituicao.',
			'128' => 'Tipo de Transacao INVALIDA.',
			'134' => 'Endereco de E-mail INVALIDO.',
			'199' => 'Erro INDEFINIDO.'
	);
	
	/**
	 * Autorizacao do Pagamento
	 * @author Elcio Mauro Guimaraes - elcioguimaraes@gmail.com
	 * @param AuthorizeTransactionTO $atTO
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function autorizar(AuthorizeTransactionTO $atTO) {
		
		try {		
			
			if(!$atTO->getUrlBraspag()){
				throw new Zend_Exception("A Url para execucao do pagamento na Braspag nao foi informada!");
			}
			
			$xml = '<?xml version="1.0" encoding="utf-8"?>
			<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
			  <soap:Body>
			    <AuthorizeTransaction xmlns="https://www.pagador.com.br/webservice/pagador">
			      <request>
			        <OrderData>
			          <MerchantId>'.$atTO->getMerchantId().'</MerchantId>
			          <OrderId>'.$atTO->getOrderId().'</OrderId>
			          <BraspagOrderId xsi:nil="true" />
			        </OrderData>
			        <CustomerData>
			          <CustomerIdentity>'.$atTO->getCustomerIdentity().'</CustomerIdentity>
			          <CustomerName>'.$atTO->getCustomerName().'</CustomerName>
			          <CustomerEmail>'.$atTO->getCustomerEmail().'</CustomerEmail>
			          <CustomerAddressData xsi:nil="true" />
			          <DeliveryAddressData xsi:nil="true" />
			        </CustomerData>
			        <PaymentDataCollection>
			          <PaymentDataRequest xsi:type="CreditCardDataRequest">
			            <PaymentMethod>'.$atTO->getPaymentMethod().'</PaymentMethod>
			            <Amount>'.$atTO->getAmount() .'</Amount>
			            <Currency>'.$atTO->getCurrency().'</Currency>
			            <Country>'.$atTO->getCountry().'</Country>
			            <NumberOfPayments>'.$atTO->getNumberOfPayments().'</NumberOfPayments>
			            <PaymentPlan>'.$atTO->getPaymentPlan().'</PaymentPlan>
			            <TransactionType>'.$atTO->getTransactionType().'</TransactionType>
			            <CardHolder>'.$atTO->getCardHolder().'</CardHolder>
			            <CardNumber>'.$atTO->getCardNumber().'</CardNumber>
			            <CardSecurityCode>'.$atTO->getCardSecurityCode().'</CardSecurityCode>
			            <CardExpirationDate>'.$atTO->getCardExpirationDate().'</CardExpirationDate>
			            <AdditionalDataCollection xsi:nil="true" />
			          </PaymentDataRequest>
			        </PaymentDataCollection>
			        <RequestId>'.$atTO->getRequestId().'</RequestId>
			        <Version>1.0</Version>
			      </request>
			    </AuthorizeTransaction>
			  </soap:Body>
			</soap:Envelope>';

			$execucao 		= $this->_exeCutaSOAPCurl(utf8_encode($xml), self::AUTORIZETRANSACTION, $atTO->getUrlBraspag());
			$mensageiro 	= $this->_lerSOAPXmlCurl($execucao, self::AUTORIZETRANSACTIONRESPONSE);
			if($mensageiro->getTipo()!=Ead1_IMensageiro::SUCESSO){
				throw new Zend_Exception($mensageiro->getFirstMensagem());
			}

			$leitura = $this->_lerResultadoWebService($mensageiro->getFirstMensagem(), self::AUTORIZETRANSACTIONRESULT);
			return $leitura;
		
		} catch (Exception $e) {
			return new Ead1_Mensageiro('Pagador - '.$e->getMessage(), Ead1_IMensageiro::ERRO, (isset($resultado) ? $resultado->getCodigo() : null));
		}
		
		
	}
	
	/**
	 * Mehtodo para Ler o que vem do XML e traduzir isso para aplicacao
	 * @param SimpleXMLElement $xml
	 * @param string $actionResult
	 * @return Ead1_Mensageiro
	 */
	private  function _lerResultadoWebService(SimpleXMLElement $xml, $actionResult){
		
		/**
		 * Evitando erros de Array vazio
		 */
		error_reporting(E_ALL & ~E_NOTICE);
		
		
		/**
		 * Convertendo o Objeto em Array
		 */
		$result = self::object_to_array($xml);
		switch ($actionResult){
			case 'GetOrderDataResult':
				if($result['GetOrderDataResult']['Success'] == 'true'){
				
					switch ($result['GetOrderDataResult']['TransactionDataCollection']['OrderTransactionDataResponse']['Status']) {
						case 1: //O pedido foi capturado com Sucesso
						case 2:
							return new Ead1_Mensageiro($result, Ead1_IMensageiro::SUCESSO);
							break;
						case 3:
						case 4:
						case 5:
						case 7:
							return new Ead1_Mensageiro('Transação não autorizada, cancelada ou negada: Status - '.$result['GetOrderDataResult']['TransactionDataCollection']['OrderTransactionDataResponse']['Status'], Ead1_IMensageiro::ERRO, $result);
							break;
						default:
							return new Ead1_Mensageiro('Erro desconhecido: Status - '.$result['GetOrderDataResult']['TransactionDataCollection']['OrderTransactionDataResponse']['Status'], Ead1_IMensageiro::AVISO, $result);
						break;
					}
				
				} else {
					return new Ead1_Mensageiro('Erro ao recuperar o Pedido!', Ead1_IMensageiro::AVISO);
				}//if($result['GetOrderDataResult']['Success'] == 'true'){
			break;	
			case self::CAPTURECREDITCARDTRANSACTIONRESULT:
				if($result['CaptureCreditCardTransactionResult']['Success'] == 'true'){
				
					switch ($result['CaptureCreditCardTransactionResult']['TransactionDataCollection']['TransactionDataResponse']['Status']) {
						case 0: //O pedido foi capturado com Sucesso
							return new Ead1_Mensageiro($result, Ead1_IMensageiro::SUCESSO);
							break;
						case 2:
							return new Ead1_Mensageiro('Captura negada: '.$result['CaptureCreditCardTransactionResult']['TransactionDataCollection']['TransactionDataResponse']['ReturnMessage'], Ead1_IMensageiro::ERRO, $result);
							break;
						default:
							return new Ead1_Mensageiro('Erro desconhecido no cancelamento do pedido: '.$result['CaptureCreditCardTransactionResult']['TransactionDataCollection']['TransactionDataResponse']['ReturnMessage'], Ead1_IMensageiro::ERRO, $result);
						break;
					}
				
				} else {
					return new Ead1_Mensageiro('Erro ao capturar o Pedido!', Ead1_IMensageiro::ERRO);
				}//if($result['CaptureCreditCardTransactionResult']['Success'] == 'true'){
			break;	
			
			case self::VOIDCREDITCARDTRANSACTIONRESULT:
				if($result['VoidCreditCardTransactionResult']['Success'] == 'true'){
					
					switch ($result['VoidCreditCardTransactionResult']['TransactionDataCollection']['TransactionDataResponse']['Status']) {
						case 0: //O pedido foi cancelado com Sucesso
							return new Ead1_Mensageiro($result, Ead1_IMensageiro::SUCESSO);
						break;
						case 1:
							return new Ead1_Mensageiro('O pedido nao foi cancelado: '.$result['VoidCreditCardTransactionResult']['TransactionDataCollection']['TransactionDataResponse']['ReturnMessage'], Ead1_IMensageiro::ERRO, $actionResult);
						break;
						default:
							return new Ead1_Mensageiro('Erro desconhecido no cancelamento do pedido: '.$result['VoidCreditCardTransactionResult']['TransactionDataCollection']['TransactionDataResponse']['ReturnMessage'], Ead1_IMensageiro::ERRO,$actionResult);
						break;
					}
					
				} else {
					return new Ead1_Mensageiro('Erro ao cancelar o Pedido!', Ead1_IMensageiro::ERRO);
				}//if($result['VoidCreditCardTransactionResult']['Success'] == 'true'){
				
			break;
			
			
			case self::AUTORIZETRANSACTIONRESULT: 
				if($result['AuthorizeTransactionResult']['Success'] == 'true'){
					
					switch ($result['AuthorizeTransactionResult']['PaymentDataCollection']['PaymentDataResponse']['Status']) {
						case self::TRANS_AUTORIZADA:
						case self::TRANS_CAPTURADA:
							return new Ead1_Mensageiro($result, Ead1_IMensageiro::SUCESSO);
						break;
						
						default:
							return new Ead1_Mensageiro($result, Ead1_IMensageiro::AVISO);
						break;
					}
					
				} else {
					
					
					$codigo = $result['AuthorizeTransactionResult']
							['ErrorReportDataCollection']
							['ErrorReportDataResponse']['ErrorCode'] ? $result['AuthorizeTransactionResult']
							['ErrorReportDataCollection']
							['ErrorReportDataResponse']['ErrorCode'] : 199;
					
					if(isset($this->erro[$codigo])){
						$mensagem = $this->erro[$codigo];
					} else {
						if(isset($result['AuthorizeTransactionResult']['ErrorReportDataCollection']['ErrorReportDataResponse']['ErrorMessage'])){
							$mensagem = $result['AuthorizeTransactionResult']['ErrorReportDataCollection']['ErrorReportDataResponse']['ErrorCode'].' - '.$result['AuthorizeTransactionResult']['ErrorReportDataCollection']['ErrorReportDataResponse']['ErrorMessage'];
						} else {
							$mensagem = $this->erro[199];
						}
					}
					
					return new Ead1_Mensageiro($mensagem, Ead1_IMensageiro::ERRO, $result);
				};
				
			break; //case self::AUTORIZETRANSACTIONRESULT: 
			default: return new Ead1_Mensageiro('Resultado não configurado', Ead1_IMensageiro::ERRO);
			
		}
		
		
		
	}

	// 	TODO 
	public function verificar() {
		return new Ead1_Mensageiro('Pagamento já efetuado!', Ead1_IMensageiro::SUCESSO);
		// Talvez seja a captura, verificar se pode ser apagado
	}
	
	/**
	 * Este método permite realizar consultas os dados do pedido na Braspag via Webservice
	 * @param GetOrderPagadorTO $to
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function GetOrderData(GetOrderPagadorTO $to){
		
		try {
				
				$xml = '<?xml version="1.0" encoding="utf-8"?>
						<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
						  <soap:Body>
						    <GetOrderData xmlns="https://www.pagador.com.br/query/pagadorquery">
						      <orderDataRequest>
					         	<Version>1.0</Version>
					         	<RequestId>'.$to->getRequestId().'</RequestId>
						        <MerchantId>'.$to->getMerchantId().'</MerchantId>
						        <BraspagOrderId>'.$to->getBraspagOrderId().'</BraspagOrderId>
						      </orderDataRequest>
						    </GetOrderData>
						  </soap:Body>
						</soap:Envelope>';
				$execucao 	= $this->_exeCutaSOAPCurl(utf8_encode($xml), 'GetOrderData', $to->getUrlBraspag());
				$mensageiro = $this->_lerSOAPXmlCurl($execucao, 'GetOrderDataResponse', 'https://www.pagador.com.br/query/pagadorquery');
				if($mensageiro->getTipo()!=Ead1_IMensageiro::SUCESSO){
					throw new Zend_Exception($mensageiro->getFirstMensagem());
				}
				$resultado = $this->_lerResultadoWebService($mensageiro->getFirstMensagem(), 'GetOrderDataResult');
				if($resultado->getTipo()==Ead1_IMensageiro::ERRO){
					throw new Zend_Exception($resultado->getFirstMensagem());
				}
				return $resultado;
				
		} catch (Exception $e) {
			return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
		}
		
		
	}

	
	
	
	/**
	 * Verifica o pagamento pelo OrderID
	 * @param GetOrderIdDataPagadorTO $to
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function GetOrderIdData(GetOrderIdDataPagadorTO $to){
		
		try {
				
				$xml = '<?xml version="1.0" encoding="utf-8"?>
						<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
						  <soap:Body>
						    <GetOrderIdData xmlns="https://www.pagador.com.br/query/pagadorquery">
						      <orderIdDataRequest>
					         	<Version>1.0</Version>
					         	<RequestId>'.$to->getRequestId().'</RequestId>
						        <MerchantId>'.$to->getMerchantId().'</MerchantId>
						        <OrderId>'.$to->getOrderId().'</OrderId>
						      </orderIdDataRequest>
						    </GetOrderIdData>
						  </soap:Body>
						</soap:Envelope>';
				
				
				$execucao 	= $this->_exeCutaSOAPCurl(utf8_encode($xml), 'GetOrderIdData', $to->getUrlBraspag());
				$mensageiro = $this->_lerSOAPXmlCurl($execucao, 'GetOrderIdDataResponse', 'https://www.pagador.com.br/query/pagadorquery');
				if($mensageiro->getTipo()!=Ead1_IMensageiro::SUCESSO){
					throw new Zend_Exception($mensageiro->getFirstMensagem());
				}
				
				$resultado = $this->_lerResultadoWebService($mensageiro->getFirstMensagem(), 'GetOrderIdDataResult');
// 				Zend_Debug::dump($resultado,__CLASS__.'('.__LINE__.')');
				if($resultado->getTipo()==Ead1_IMensageiro::ERRO){
					throw new Zend_Exception($resultado->getFirstMensagem());
				}
				return $resultado;
				
		} catch (Exception $e) {
			return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
		}
		
		
	}
	
	/**
	 * Faz a captura da Transacao depois de aprovada
	 * Verificar usabilidade
	 * @param CaptureCreditCardTransactionTO $ccctTO
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function capturar(CaptureCreditCardTransactionTO $ccctTO) {
		
		try {
				
				$xml = '<?xml version="1.0" encoding="utf-8"?>
						<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
						  <soap:Body>
						    <CaptureCreditCardTransaction xmlns="https://www.pagador.com.br/webservice/pagador">
						      <request>
						        <MerchantId>'.$ccctTO->getMerchantId().'</MerchantId>
						        <TransactionDataCollection>
						          <TransactionDataRequest>
						            <BraspagTransactionId>'.$ccctTO->getBraspagTransactionId().'</BraspagTransactionId>
						            <Amount>'.number_format($ccctTO->getAmount(), 2, '', '').'</Amount>
						          </TransactionDataRequest>
						        </TransactionDataCollection>
							   <RequestId>'.$ccctTO->getRequestId().'</RequestId>
						       <Version>'.$ccctTO->getVersion().'</Version>
						      </request>
						    </CaptureCreditCardTransaction>
						  </soap:Body>
						</soap:Envelope>';
						
				$execucao 	= $this->_exeCutaSOAPCurl(utf8_encode($xml), self::CAPTURECREDITCARDTRANSACTION, $ccctTO->getUrlBraspag());
				$mensageiro = $this->_lerSOAPXmlCurl($execucao, self::CAPTURECREDITCARDTRANSACTIONRESPONSE);
				
				if($mensageiro->getTipo()!=Ead1_IMensageiro::SUCESSO){
					throw new Zend_Exception('Erro ao Capturar o pagamento: '. $mensageiro->getFirstMensagem());
				}
				
				$resultado = $this->_lerResultadoWebService($mensageiro->getFirstMensagem(), self::CAPTURECREDITCARDTRANSACTIONRESULT);
				
				if($resultado->getTipo()!=Ead1_IMensageiro::SUCESSO){
					throw new Zend_Exception($resultado->getFirstMensagem());
				}
				
				return $mensageiro;
		} catch (Exception $e) {
			return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
		}
		
	}
	
	
	
	/**
	 * Cancela uma compra feita no mesmo dia!
	 * @author Elcio Mauro Guimaraes - elcioguimaraes@gmail.com
	 * @param VoidCreditCardTransactionTO $vcctTO
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function cancelar(VoidCreditCardTransactionTO $vcctTO) {
		
		try {
				
				$xml = '<?xml version="1.0" encoding="utf-8"?>
				<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
				  <soap:Body>
				    <VoidCreditCardTransaction  xmlns="https://www.pagador.com.br/webservice/pagador">
				      <request>
				        <MerchantId>'.$vcctTO->getMerchantId().'</MerchantId>
				        <TransactionDataCollection>
				          <TransactionDataRequest>
				            <BraspagTransactionId>'.$vcctTO->getBraspagTransactionId().'</BraspagTransactionId>
				            <Amount>'.$vcctTO->getAmount().'</Amount>
				          </TransactionDataRequest>
				        </TransactionDataCollection>
					   <RequestId>'.$vcctTO->getRequestId().'</RequestId>
				       <Version>1.0</Version>
				      </request>
				    </VoidCreditCardTransaction>
				  </soap:Body>
				</soap:Envelope>';
				
				$execucao 	= $this->_exeCutaSOAPCurl(utf8_encode($xml), self::VOIDCREDITCARDTRANSACTION, $vcctTO->getUrlBraspag());
				$mensageiro = $this->_lerSOAPXmlCurl($execucao, self::VOIDCREDITCARDTRANSACTIONRESPONSE);
				
				if($mensageiro->getTipo()!=Ead1_IMensageiro::SUCESSO){
					throw new Zend_Exception('Erro ao Cancelar o pagamento: '. $mensageiro->getFirstMensagem());
				}
				
				return $this->_lerResultadoWebService($mensageiro->getFirstMensagem(), self::VOIDCREDITCARDTRANSACTIONRESULT);
		
		} catch (Exception $e) {
			return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
		}
		
	}


	/**
	 * A Fazer
	 * @author Elcio Mauro Guimaraes - elcioguimaraes@gmail.com
	 * @return Ead1_Mensageiro
	 */
	public function estornar() {
		// TODO Auto-generated method stub
		return new Ead1_Mensageiro('Funcionalidade nao desenvolvida!', Ead1_IMensageiro::AVISO);
	}

}


<?php


/**
 * super classe para serviços do moodle
 * @author ederlamar
 *
 */
class BraspagV2_Recorrente extends BraspagV2
{

    public function __construct($MerchantId, $MerchantKey, $URL)
    {
        parent::__construct($MerchantId , $MerchantKey, $URL);
    }

    /**
     * @param $id - RecurrentPaymentId
     * @return Ead1_Mensageiro
     */
    public function GetOrderRecurrencies($order)
    {

        try {

            $transactions = $order->RecurrentPayment->RecurrentTransactions;

            $returnTrasactions = array();

            if($transactions){
                foreach($transactions as $transaction){

                    $service = 'sales/' . $transaction->TransactionId;

                    $result = $this->httpRequest(\G2\Constante\BraspagV2::URL_API_BRASPAG_QUERY . $service);
                    if ($result->getTipo() == Ead1_IMensageiro::SUCESSO && $result->getFirstMensagem()) {
                        $transaction->Transaction = json_decode($result->getFirstMensagem());
                        $returnTrasactions[] = $transaction;
                    }


                }
            } else {
                return new Ead1_Mensageiro("Nenhuma transação encontrada", Ead1_IMensageiro::AVISO);
            }

            return new Ead1_Mensageiro($returnTrasactions, Ead1_IMensageiro::SUCESSO);

        } catch (\Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }

    }


    /**
     * Retorna os detalhes de um pedido
     *
     * @param $id - RecurrentPaymentId
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     *
     */
    public function getOrder($id)
    {

        try {

            $service = "RecurrentPayment/$id";
            $order = null;
            $result = $this->httpRequest(\G2\Constante\BraspagV2::URL_API_BRASPAG_QUERY . $service);

            if ($result->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $order = json_decode($result->getFirstMensagem());
                if ($order) {
                    return new Ead1_Mensageiro($order, Ead1_IMensageiro::SUCESSO);
                } else {
                    return new Ead1_Mensageiro("Nenhum registro encontrado", Ead1_IMensageiro::ERRO);
                }
            } else {
                return $result;
            }

        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }

    }

    public function changeRecurrentPayment($id, $data)
    {
        try {
            $service = "RecurrentPayment/$id/Payment";
            $result = $this->httpRequest(\G2\Constante\BraspagV2::URL_API_BRASPAG . $service, $data, 'PUT');

            if ($result->getTipo() == Ead1_IMensageiro::SUCESSO) {
                if ($result->getCodigo() == 200 || $result->getCodigo() == '200') {
                    $retorno = json_decode($result->getFirstMensagem());
                    if (!empty($retorno)) {
                        return new Ead1_Mensageiro($retorno[0]->Message, Ead1_IMensageiro::SUCESSO);
                    } else {
                        return new Ead1_Mensageiro('BraspagV2: Recorrência alterada com sucesso', Ead1_IMensageiro::SUCESSO);
                    }
                } else {
                    $error = json_decode($result->getFirstMensagem());
                    if (!empty($error)) {
                        return new Ead1_Mensageiro($error[0]->Message, Ead1_IMensageiro::SUCESSO);
                    } else {
                        return new Ead1_Mensageiro("Erro ao alterar a recorrência, erro desconhecido", Ead1_IMensageiro::ERRO);
                    }
                }

            } else {
                return $result;
            }

        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    public function getRecurrentPayment($id)
    {
        try {
            $service = "RecurrentPayment/$id";
            $result = $this->httpRequest(\G2\Constante\BraspagV2::URL_API_BRASPAG_QUERY . $service);
            if($result->getTipo()==Ead1_IMensageiro::SUCESSO){
                $order = json_decode($result->getFirstMensagem());
                if($order){
                    return new Ead1_Mensageiro($order, Ead1_IMensageiro::SUCESSO);
                } else {
                    return new Ead1_Mensageiro("Nenhum registro encontrado", Ead1_IMensageiro::ERRO);
                }
            } else {
                return $result;
            }

        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

}

<?php

/**
 * Classe para atualizar a Fonte da Compra
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package package_name
 *
 */
class AtualizaFonteLojaWebServices
{

    const STATUS_APROVADA = 'aprovada';
    const STATUS_RECUSADA = 'recusada';


    /**
     * Método que Atualiza o Status nas Lojas
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param int $id_sistema CONSTANTE NA SistemaTO
     * @param VendaIntegracaoTO $viTO
     * @param CONSTANTE STATUS_APROVADA | STATUS_RECUSADA
     * @return Ead1_Mensageiro
     */
    public static function atualizaStatusFonte(VendaIntegracaoTO $viTO, $status_transacao)
    {

        switch ($viTO->getId_sistema()) {
            case SistemaTO::WORDPRESS_ECOMMERCE:
                return self::_atualizaStatusFonteWordPress($viTO, $status_transacao);;
                break;

            default:
                return new Ead1_Mensageiro('Nenhuma Loja definida.', Ead1_IMensageiro::AVISO);;
                break;
        }


    }


    /**
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param VendaIntegracaoTO $viTO
     * @param string $status_transacao
     * @return Ead1_Mensageiro
     */
    protected static function _atualizaStatusFonteWordPress(VendaIntegracaoTO $viTO, $status_transacao)
    {


        try {


            $venda = new VendaTO();
            $venda->setId_venda($viTO->getId_venda());
            $venda->fetch(true, true, true);

            $eiTO = new EntidadeIntegracaoTO();
            $eiTO->setId_entidade($venda->getId_entidade());
            $eiTO->setId_sistema($viTO->getId_sistema());
            $eiTO->fetch(false, true, true);

            $ch = curl_init();

            $dados = array(
                'st_statustransacao' => $status_transacao
            , 'st_vendaexterna' => $viTO->getSt_vendaexterna()
            , 'st_codchave' => $eiTO->getSt_codchave()
            , 'gestor2_gateway_return' => true
            , 'nu_valorliquido' => $venda->getNu_valorliquido()
            );

            curl_setopt($ch, CURLOPT_URL, $viTO->getst_urlconfirmacao());
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_PORT, 80);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $dados);

            $output = curl_exec($ch);
            if ($output === false) {
                $err = curl_error($ch);
                curl_close($ch);
                throw new Zend_Exception('Erro na chamada do Serviço: ' . $err . '.');
            }
            curl_close($ch);
            $mensageiro = json_decode($output);
            if (!$mensageiro) {
                throw new Exception("Resposta do serviço que atualiza o 'Status Fonte WordPress' vazia.");
            }

            return new Ead1_Mensageiro($mensageiro->mensagem, $mensageiro->tipo);

        } catch (Exception $e) {
            return new Ead1_Mensageiro('Erro ao validar a compra no destino: ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }


    }


}

?>
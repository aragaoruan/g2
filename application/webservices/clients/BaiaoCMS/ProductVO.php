<?php
/**
 * Baiao de Dois CMS WS
 *
 * Classes para Consumo dos serviços do CMS
 * Desenvolvido com o base nos códigos enviador pelo Abdala
 *
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 *
 */
class BaiaoCMS_ProductVO extends BaiaoCMS_VO {
	
	
	public $id;
	public $title;
	public $subtitle;
	public $price;
	public $price_promotional;
	public $promotional_start;
	public $promotional_end;
	public $dot;
	public $dot_promotional;
	public $dot_start;
	public $dot_end;
	public $partner_id;
	public $partners_reseller = array();
	public $vacant;
	public $categories  = array();
	public $disciplines = array();
	
	
/**
	 * @return the $partners_reseller
	 */
	public function getPartners_reseller() {
		return $this->partners_reseller;
	}

	/**
	 * @return the $disciplines
	 */
	public function getDisciplines() {
		return $this->disciplines;
	}

	/**
	 * IDs de parceiros autorizados a fazer revenda
	 * @param array $partners_reseller
	 */
	public function setPartners_reseller($partners_reseller) {
		$this->partners_reseller = $partners_reseller;
		return $this;
	}

	/**
	 * Lista de disciplinas relacionadas com o produto (id, name,teacher_id,hours)
	 * @param array $disciplines
	 */
	public function setDisciplines(array $disciplines) {
		$this->disciplines = $disciplines;
		return $this;
	}

	/**
	 * @return the $id
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return the $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @return the $subtitle
	 */
	public function getSubtitle() {
		return $this->subtitle;
	}

	/**
	 * @return the $price
	 */
	public function getPrice() {
		return $this->price;
	}

	/**
	 * @return the $price_promotional
	 */
	public function getPrice_promotional() {
		return $this->price_promotional;
	}

	/**
	 * @return the $promotional_start
	 */
	public function getPromotional_start() {
		return $this->promotional_start;
	}

	/**
	 * @return the $promotional_end
	 */
	public function getPromotional_end() {
		return $this->promotional_end;
	}

	/**
	 * @return the $dot
	 */
	public function getDot() {
		return $this->dot;
	}

	/**
	 * @return the $dot_promotional
	 */
	public function getDot_promotional() {
		return $this->dot_promotional;
	}

	/**
	 * @return the $dot_start
	 */
	public function getDot_start() {
		return $this->dot_start;
	}

	/**
	 * @return the $dot_end
	 */
	public function getDot_end() {
		return $this->dot_end;
	}

	/**
	 * @return the $partner_id
	 */
	public function getPartner_id() {
		return $this->partner_id;
	}

	/**
	 * @return the $vacant
	 */
	public function getVacant() {
		return $this->vacant;
	}

	/**
	 * @return the $categories
	 */
	public function getCategories() {
		return $this->categories;
	}

	/**
	 * 
	 * Id do Produto no G2
	 * @param int $id
	 */
	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	/**
	 * @param string $title
	 */
	public function setTitle($title) {
		$this->title = $title;
		return $this;
	}

	/**
	 * @param string $subtitle
	 */
	public function setSubtitle($subtitle) {
		$this->subtitle = $subtitle;
		return $this;
	}

	/**
	 * @param decimal(0.00) $price
	 */
	public function setPrice($price) {
		$this->price = $price;
		return $this;
	}

	/**
	 * @param decimal(0.00) $price_promotional
	 */
	public function setPrice_promotional($price_promotional) {
		$this->price_promotional = $price_promotional;
		return $this;
	}

	/**
	 * @param Data/hora (2014-01-01 00:00:00) $promotional_start
	 */
	public function setPromotional_start($promotional_start) {
		$this->promotional_start = $promotional_start;
		return $this;
	}

	/**
	 * @param Data/hora (2014-01-01 00:00:00) $promotional_end
	 */
	public function setPromotional_end($promotional_end) {
		$this->promotional_end = $promotional_end;
		return $this;
	}

	/**
	 * @param int $dot
	 */
	public function setDot($dot) {
		$this->dot = $dot;
		return $this;
	}

	/**
	 * @param int $dot_promotional
	 */
	public function setDot_promotional($dot_promotional) {
		$this->dot_promotional = $dot_promotional;
		return $this;
	}

	/**
	 * @param Data/hora (2014-01-01 00:00:00) $dot_start
	 */
	public function setDot_start($dot_start) {
		$this->dot_start = $dot_start;
		return $this;
	}

	/**
	 * @param Data/hora (2014-01-01 00:00:00) $dot_end
	 */
	public function setDot_end($dot_end) {
		$this->dot_end = $dot_end;
		return $this;
	}

	/**
	 * 
	 * ID da Entidade no G2
	 * @param int $partner_id
	 */
	public function setPartner_id($partner_id) {
		$this->partner_id = $partner_id;
		return $this;
	}

	/**
	 * Quantidade de Estoque ou vagas
	 * @param int $vacant
	 */
	public function setVacant($vacant) {
		$this->vacant = $vacant;
		return $this;
	}

	/**
	 * Lista de IDs das categorias
	 * @param array $categories
	 */
	public function setCategories(array $categories) {
		$this->categories = $categories;
		return $this;
	}
	
	
	
}


<?php
/**
 * Baiao de Dois CMS WS
 *
 * Classes para Consumo dos serviços do CMS
 * Desenvolvido com o base nos códigos enviador pelo Abdala
 *
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 *
 */
class BaiaoCMS_CategoryVO extends BaiaoCMS_VO {

	public $id;
	public $name;
	
	
	/**
	 * @return the $id
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return the $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param field_type $id
	 */
	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	/**
	 * @param field_type $name
	 */
	public function setName($name) {
		$this->name = $name;
		return $this;
	}

	
	
	
	
}


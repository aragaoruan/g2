<?php
/**
 * Baiao de Dois CMS WS
 *
 * Classes para Consumo dos serviços do CMS
 * Desenvolvido com o base nos códigos enviador pelo Abdala
 *
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 *
 */
abstract class BaiaoCMS_VO {

	
	public function __construct (array $dados = null)
	{
		if (!is_null($dados)) {
			$arPropriedadeVO = get_object_vars($this);
			foreach ($dados as $propriedade => $valor) {
				if (array_key_exists($propriedade, $arPropriedadeVO) !== false) {
					if($valor){
						$this->$propriedade = $valor;
					}
				}
			}
		}
	}
	
	/**
	 * Converte o Objeto em Array
	 * @return Ambigous <NULL, unknown>
	 */
	public function toArray(){
		
		$array = null;
		
		$arChaves = get_object_vars($this);
		foreach ($arChaves as $chave => $valor){
			if($valor){
				$array[$chave] = $valor;
			}
		}

		return $array;
	
	}
	
	
}

<?php

/**
 * Classe de integração de alocação com o moodle
 * @author ederlamar
 *
 */
class MoodleAlocacaoWebServices extends MoodleWebServices
{

    const ROLEID_GERENTE = 1;
    const ROLEID_CRIADOR_CURSO = 2;
    const ROLEID_PROFESSOR = 3;
    const ROLEID_MODERADOR = 4;
    const ROLEID_ESTUDANTE = 5;
    const ROLEID_VISITANTE = 6;
    const ROLEID_USUARIO_AUTENTICADO = 7;
    const ROLEID_USUARIO_AUTENTICADO_PAGINA_INICIAL = 8;


    /**
     * Método que cadastra e suspende a inscrição de aluno em uma sala do moodle
     * Para ativar ele não envia o parametro $dadosEnrol[0]['suspend'] que preenchido apenas para suspender a inscrição de um perfil
     * @param int $idUsuario
     * @param int $idSalaDeAula
     * @param int $roleID ->id_perfil
     * @param int $id_entidade
     * @param boolean $inativar
     * @param string $namefunction
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function cadastrarPapel($idUsuario, $idSalaDeAula, $roleID, $id_entidade = null, $inativar = false, $namefunction = '')
    {
        $entidadeIntegracaoTO = $this->getEntidadeIntegracaoTO();

        if (!$entidadeIntegracaoTO->getId_entidade()) {
            throw new Zend_Exception("Não existe interface de integração para o sistema Moodle nesta entidade!");
        }

        try {

            $salaDeAulaIntegracaoTO = new SalaDeAulaIntegracaoTO();
            $salaDeAulaIntegracaoTO->setId_sistema(SistemaTO::MOODLE);
            $salaDeAulaIntegracaoTO->setId_saladeaula($idSalaDeAula);
            $salaDeAulaIntegracaoTO->fetch(false, true, true);


            //Se ele não tiver o código do curso ele cadastra o curso no MOODLE
            if (!$salaDeAulaIntegracaoTO->getSt_codsistemacurso()) {
                $vwSalaDeAulaTO = new VwSalaDeAulaTO();
                $vwSalaDeAulaTO->setId_saladeaula($idSalaDeAula);

                $moodleCurso = new MoodleCursosWebServices($entidadeIntegracaoTO->getId_entidade(),
                    $entidadeIntegracaoTO->getId_entidadeintegracao());

                $mensageiro = $moodleCurso->cadastrarCurso($vwSalaDeAulaTO->fetch());
                if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
                    $salaDeAulaIntegracaoTO->fetch(false, true, true);
                } else {
                    throw new Zend_Exception("ERRO ao tentar integrar o Curso!");
                }
            }

            //Seta os dados do usuariointegracao
            $usuarioIntegracaoTO = new UsuarioIntegracaoTO();
            $usuarioIntegracaoTO->setId_usuario($idUsuario);
            $usuarioIntegracaoTO->setId_sistema(\G2\Constante\Sistema::MOODLE);
            $usuarioIntegracaoTO->setId_entidade($id_entidade ? $id_entidade : $usuarioIntegracaoTO->getSessao()->id_entidade);
            $usuarioIntegracaoTO->setId_entidadeintegracao($entidadeIntegracaoTO->getId_entidadeintegracao());

            //pesquisa o usuário pelo "id_usuario" na tabela de usuariointegracao
            $usuarioIntegracaoTO->fetch(false, true, true);


            $moodleUsuario = new MoodleUsuariosWebServices(null, $entidadeIntegracaoTO->getId_entidadeintegracao());

            $vwPessoaTO = new VwPessoaTO();
            $vwPessoaTO->setId_usuario($idUsuario);
            $vwPessoaTO->setId_entidade($id_entidade ? $id_entidade : $vwPessoaTO->getSessao()->id_entidade);

            //se não encontrar o usuário...
            if (!$usuarioIntegracaoTO->getSt_codusuario()) {

                //...tenta cadastrar o usuário no MOODLE
                $mensageiro = $moodleUsuario->cadastrarUsuario([$vwPessoaTO->fetch(false, true)]);
                //se conseguir cadastrar o usuário, tenta pesquisá-lo pelo "id_usuario" na tabela de usuariointegracao novamente
                if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {

                    //Se não conseguir cadastrar o usuario, verifica se usuário já existe no moodle
                    $entIntegracaoTO = $this->getEntidadeIntegracaoTO();

                    if (!$entIntegracaoTO || !$entIntegracaoTO->getSt_codchave()) {
                        throw new Zend_Exception('Entidade não Integrada ao Sistema!');
                    }
                    //busca na vwPessoa os dados do usuário
                    $vwPessoaTO->fetch(false, true, true);

                    $ddsPesquisaUsuarioMoodle = $moodleUsuario
                        ->retornarUsuarioByUserName($vwPessoaTO->getSt_login() . "." . $entIntegracaoTO->getId_entidade());
                    $ddsUsuario = array();

                    //Se não encontrar o usuário no MOODLE mostra a mensagem
                    if (is_object($ddsPesquisaUsuarioMoodle)) {
                        $mensagemErro = get_object_vars($ddsPesquisaUsuarioMoodle);
                        throw new Zend_Exception("ERRO ao pesquisar o aluno no Moodle! " . $mensagemErro['message']);
                    }

                    //se não ocorrer erro tirar os dados do usuário do objeto retornado e coloca num array
                    if (count($ddsPesquisaUsuarioMoodle))
                        $ddsUsuario = get_object_vars($ddsPesquisaUsuarioMoodle[0]);

                    //comparação para ver se o id_usuario é o mesmo que está no cadastro do MOODLE
                    if (!count($ddsUsuario) || (isset($ddsUsuario["idnumber"]))
                        && $idUsuario != $ddsUsuario["idnumber"]
                    ) {
                        throw new Zend_Exception("Usuário não encontrado no moodle ou " . $mensageiro->getFirstMensagem());
                    }

                    //cadastra o usuário na tabela de usuarioIntegração
                    $usuarioIntegracao = new \G2\Negocio\UsuarioIntegracao();
                    $usuarioDados = $usuarioIntegracao->salvarIntegracao(array(
                        'id_sistema' => SistemaTO::MOODLE,
                        'id_usuariocadastro' => $idUsuario,
                        'id_usuario' => $idUsuario,
                        'dt_cadastro' => new \DateTime(),
                        'st_codusuario' => $ddsUsuario["id"],
                        'st_senhaintegrada' => $vwPessoaTO->getSt_senha() . '*G2',
                        'st_loginintegrado' => $ddsUsuario["username"],
                        'id_entidade' => $id_entidade ? $id_entidade : $entIntegracaoTO->getSessao()->id_entidade,
                        'bl_encerrado' => 0,
                        "id_entidadeintegracao" => $entIntegracaoTO->getId_entidadeintegracao()
                    ));

                    //após o cadastro efetuado com sucesso, pesquisa o usuário pelo "id_usuario" na tabela de usuariointegracao
                    if (!$usuarioDados) {
                        throw new Zend_Exception($mensageiro->getFirstMensagem());
                    }

                    //Atualiza a senha do usuario no MOODLE
                    $ddsUserUpdate['id'] = $ddsUsuario["id"];
                    $ddsUserUpdate['password'] = $vwPessoaTO->getSt_senha() . '*G2';
                    $ddsUserUpdate['description'] = 'Tentando atualizar a senha';
                    $ddsUserUpdate['alternatename'] = $namefunction;

                    $arrayUserUpdate[] = $ddsUserUpdate;

                    $ddsUpdate['wstoken'] = $entIntegracaoTO->getSt_codchave();
                    $ddsUpdate['users'] = $arrayUserUpdate;
                    $ddsUpdate['wsfunction'] = 'core_user_update_users';
                    $ddsUpdate['moodlewsrestformat'] = 'json';
                    $postUpdate = $this->clientRest->restPost("webservice/rest/server.php", $ddsUpdate);

                    $this->verificaWsErro($postUpdate->getBody(), 'UsuarioUpdate');
                    $ddsIntegracaoUpdate = json_decode($postUpdate->getBody());

                    if (is_object($ddsIntegracaoUpdate)) {
                        $mensagemErro = get_object_vars($ddsIntegracaoUpdate);
                        throw new Zend_Exception("ERRO ao atualizar o aluno no Moodle! " . $mensagemErro['message']);
                    }

                }

                $usuarioIntegracaoTO->fetch(false, true, true);
            }

            //busca na vwPessoa os dados do usuário
            $vwPessoaTO->fetch(false, true, true);

            //verifica se já foi procurar o usuario no moodle (dentro do if acima), se não foi prcura no moodle
            if (!isset($ddsPesquisaUsuarioMoodle)) {
                $ddsPesquisaUsuarioMoodle = $moodleUsuario
                    ->retornarUsuarioByUserName(
                        $usuarioIntegracaoTO->getSt_loginintegrado()
                    );
            }

            //verifica se não encontrou o usuário no moodle
            if (!count($ddsPesquisaUsuarioMoodle)) {
                throw new Exception("Usuário não encontrado no Moodle.");
            }

            $dadosEnrol[0]['roleid'] = (int)$roleID;
            $dadosEnrol[0]['userid'] = (int)$usuarioIntegracaoTO->getSt_codusuario();
            $dadosEnrol[0]['courseid'] = (int)$salaDeAulaIntegracaoTO->getSt_codsistemacurso();
            if ($inativar) {
                $dadosEnrol[0]['suspend'] = 1;
            }

            if (!$entidadeIntegracaoTO) {
                throw new Zend_Exception('Entidade não Integrada ao Sistema!');
            }
            $dados['wstoken'] = $entidadeIntegracaoTO->getSt_codchave();
            $dados['enrolments'] = $dadosEnrol;
            $dados['wsfunction'] = 'enrol_manual_enrol_users';
            $post = $this->clientRest->restPost("webservice/rest/server.php", $dados);
            $this->verificaWsErro($post->getBody(), 'Alocação');

            if (!simplexml_load_string($post->getBody())) {
                throw new Zend_Exception("Erro de XML no retorno do moodle!");
            }

            $dadosIntegracao = Ead1_BO::parse($post->getBody());

            if (isset($dadosIntegracao["MESSAGE"])) {
                foreach ($dadosIntegracao["MESSAGE"] as $chave => $dado) {
                    throw new Zend_Exception("ERRO ao integrar alocação! " . $dado);
                }
            }

            return new Ead1_Mensageiro($dadosEnrol[0]);
        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * @param $idUsuario
     * @param $roleID
     * @param $idContext
     * @param null $id_entidade
     * @return Ead1_Mensageiro
     */
    public function vincularPerfil($idUsuario, $roleID, $idContext, $id_entidade = null)
    {

        try {
            $entidadeIntegracaoTO = $this->getEntidadeIntegracaoTO();

            if (!$entidadeIntegracaoTO) {
                THROW new Zend_Exception('Entidade não Integrada ao Sistema!');
            }

            $usuarioIntegracaoTO = new UsuarioIntegracaoTO();
            $usuarioIntegracaoTO->setId_usuario($idUsuario);
            $usuarioIntegracaoTO->setId_sistema(SistemaTO::MOODLE);
            $usuarioIntegracaoTO->setId_entidade(($id_entidade ? $id_entidade : $usuarioIntegracaoTO->getSessao()->id_entidade));
            $usuarioIntegracaoTO->setId_entidadeintegracao($entidadeIntegracaoTO->getId_entidadeintegracao());
            $usuarioIntegracaoTO->fetch(false, true, true);

            if (!$usuarioIntegracaoTO->getSt_codusuario()) {
                $vwPessoaTO = new VwPessoaTO();
                $vwPessoaTO->setId_usuario($idUsuario);
                $vwPessoaTO->setId_entidade(($id_entidade ? $id_entidade : $vwPessoaTO->getSessao()->id_entidade));

                $moodleUsuario = new MoodleUsuariosWebServices(null, $entidadeIntegracaoTO->getId_entidadeintegracao());
                $mensageiro = $moodleUsuario->cadastrarUsuario($vwPessoaTO->fetch());
                if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception($mensageiro->getText());
                }
                $usuarioIntegracaoTO->fetch(false, true, true);
            }

            $dadosEnrol = array();
            $dados = array();

            $dadosEnrol[0]['roleid'] = $roleID;
            $dadosEnrol[0]['userid'] = $usuarioIntegracaoTO->getSt_codusuario();
            $dadosEnrol[0]['contextid'] = $idContext;
            $dados['wstoken'] = $entidadeIntegracaoTO->getSt_codchave();
            $dados['assignments'] = $dadosEnrol;
            $dados['wsfunction'] = 'core_role_assign_roles';
            $post = $this->clientRest->restPost("webservice/rest/server.php", $dados);
            $this->verificaWsErro($post->getBody(), 'Perfil');
            $dadosIntegracao = Ead1_BO::parse($post->getBody());

            if (isset($dadosIntegracao["MESSAGE"])) {
                foreach ($dadosIntegracao["MESSAGE"] as $chave => $dado) {
                    throw new Zend_Exception("ERRO ao integrar perfil! " . $dado);
                }
            }

            return new Ead1_Mensageiro($dadosEnrol[0]);
        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Método que cadastra alocação integração e realiza a chamada do serviço no moodle
     * @param int $idAlocacao
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function cadastrarAlocacaoIntegracao($idAlocacao, $id_entidade = null)
    {
        try {
            $vwAlocacaoTO = new VwAlocacaoTO();
            $vwAlocacaoTO->setId_alocacao($idAlocacao);
            $vwAlocacaoTO->fetch(null, true, true);

            if (!$vwAlocacaoTO->getId_usuario()) {
                throw new Zend_Exception("Aluno não alocado!");
            }

            $mensageiro = $this->cadastrarPapel(
                $vwAlocacaoTO->getId_usuario(),
                $vwAlocacaoTO->getId_saladeaula(),
                ($vwAlocacaoTO->getBl_institucional() ? $vwAlocacaoTO->getNu_perfilalunoobs()
                    : MoodleAlocacaoWebServices::ROLEID_ESTUDANTE),
                $id_entidade,
                false,
                'cadastrarAlocacaoIntegracao');

            $alocacaoIntegracaoTO = new AlocacaoIntegracaoTO();
            $alocacaoIntegracaoTO->setId_sistema(SistemaTO::MOODLE);
            $alocacaoIntegracaoTO->setId_alocacao($idAlocacao);
            $alocacaoIntegracaoTO
                ->setid_entidadeintegracao($this->getEntidadeIntegracaoTO()->getId_entidadeintegracao());
            $alocacaoIntegracaoTO->fetch(false, true, true);

            if (!$alocacaoIntegracaoTO->getId_alocacaointegracao()) {
                if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception($mensageiro->getCurrent());
                }

                $alocacaoIntegracaoTO
                    ->setId_usuariocadastro($alocacaoIntegracaoTO->getSessao()->id_usuariocadastro);
                $alocacaoIntegracaoTO->setSt_codalocacao(implode("#", $mensageiro->getMensagem()));
                $alocacaoIntegracaoTO->setid_entidadeintegracao($this->getEntidadeIntegracaoTO()
                    ->getId_entidadeintegracao());

                // Passando false para não cobrar transação
                $alocacaoIntegracaoORM = new AlocacaoIntegracaoORM();
                $alocacaoId = $alocacaoIntegracaoORM->insert($alocacaoIntegracaoTO->toArrayInsert(), false);
                if (!$alocacaoId) {
                    throw new Zend_Exception("Erro ao salvar Alocação integracao");
                }
                $alocacaoIntegracaoTO->setId_alocacaointegracao($alocacaoId);
                $alocacaoIntegracaoTO->fetch(false, true, true);
            }
            return new Ead1_Mensageiro($alocacaoIntegracaoTO);
        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Incluido parametro id_entidade para ser utilizado
     * no método de sincronização de notas vindo direto do moodle.
     * 22-11-2013
     * @param $courseid
     * @param $id_saladeaula
     * @param null $id_entidade
     * @param null $idusermoodle
     * @return Ead1_Mensageiro
     */
    public function recuperarNotas($courseid, $id_saladeaula, $id_entidade = null, $idusermoodle = null)
    {
        try {
            /**
             * Primeira alteração necessária
             * Dividir a comunicação com o WebService do Moodle.
             */
            $ch = curl_init();
            if (empty($id_entidade)) {
                throw new Zend_Exception('Náo foi informada a Entidade para buscar o Moodle');
            }

            $params = '';

            if ($idusermoodle) {
                $params .= '&userid=' . $idusermoodle;
            }

            $params .= '&courseid=' . $courseid;

            $integracaoMoodle = $this->getEntidadeIntegracaoTO();
            if (!$integracaoMoodle || !$integracaoMoodle->getSt_codsistema()) {
                throw new Zend_Exception("Erro ao recuperar dados de integração da entidade com o Moodle");
            }

            curl_setopt($ch, CURLOPT_URL, substr($integracaoMoodle->getSt_codsistema(), 0, -26) . "login/grades.php?" . $params);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $output = curl_exec($ch);

            $haystack = $output;
            $needle = '#.#';

            $pos = strripos($haystack, $needle);

            if ($pos === false) {
                $err = curl_error($ch);
                curl_close($ch);
                throw new Zend_Exception('Não foram encontradas notas para sincronização: ' . $err . '.');
            }

            curl_close($ch);

            $dados = explode("|", $output);

            $arrayVwAvaliacaoAlunos = array();
            if (!is_array($dados) && trim($dados[0]) == "") {
                return new Ead1_Mensageiro('Nenhuma nota encontrada no Moodle.', Ead1_IMensageiro::AVISO);
            }

            foreach ($dados as $valores) {
                $arValores = explode("#.#", $valores);
                if (is_array($arValores)) {
                    $arNotas[trim($arValores[0])] = $arValores[1];
                }
            }

            if (!isset($arNotas)) {
                return new Ead1_Mensageiro('Nenhuma nota encontrada no Moodle.', Ead1_IMensageiro::AVISO);
            }

            /**
             * GII-9258 - Tive que mudar esta parte do codigo para tambem
             * aceitar o tipo de avaliacao conceitual
             */
            $negocio = new \G2\Negocio\Negocio();

            $result = $negocio->findOneBy('\G2\Entity\VwAvaliacaoAluno',
                array('id_saladeaula' => $id_saladeaula));

            if (!$result) {
                return new Ead1_Mensageiro('Nenhuma Avaliação encontrada.', Ead1_IMensageiro::AVISO);
            }

            $tipoAvaliacao = $this->toArrayEntity($result);

            $vwAvaliacaoAluno = new VwAvaliacaoAlunoTO();
            $vwAvaliacaoAluno->setId_saladeaula($id_saladeaula);
            $vwAvaliacaoAluno->setId_tipoavaliacao(TipoAvaliacaoTO::DISTANCIA);

            if ($tipoAvaliacao['id_tipoavaliacao'] === \G2\Constante\TipoAvaliacao::CONCEITUAL) {
                $vwAvaliacaoAluno->setId_tipoavaliacao(\G2\Constante\TipoAvaliacao::CONCEITUAL);
            }

            $usuariosGestor = $vwAvaliacaoAluno->fetch();
            $arUsuariosMoodle = array_keys($arNotas);

            /**
             * Primeira alteração necessária - FIM
             * Até aqui, devemos ter separado
             * Talvez até mais de um método.
             */
            if (!$usuariosGestor) {
                return new Ead1_Mensageiro('Nenhuma nota encontrada no Portal.', Ead1_IMensageiro::AVISO);
            }

            foreach ($usuariosGestor as $vwAvaliacaoAlunoTO) {
                if (in_array($vwAvaliacaoAlunoTO->getSt_codusuario(), $arUsuariosMoodle)) {
                    $nota = ($arNotas[$vwAvaliacaoAlunoTO->getSt_codusuario()]);
                    $vwAvaliacaoAlunoTO->st_notaava = $nota;
                    $arrayVwAvaliacaoAlunos[] = $vwAvaliacaoAlunoTO;
                }
            }


            return new Ead1_Mensageiro($arrayVwAvaliacaoAlunos, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     *
     * Sincroniza notas com as notas do Moodle
     * @param array $arrVwAvaliacaoAluno
     * @throws Zend_Exception
     */
    public function sincronizarNotas($arrVwAvaliacaoAluno, $id_aluno = null)
    {
        try {

            /**
             * Segunda alteração necessária
             * Dividir o processo de Sincronização para que o mesmo possa fazer 1 a 1.
             * Criar um método que use este método de sincronização única e trabalhe mais de 1 usuário
             */
            $nota = null;
            $bo = new AvaliacaoBO();
            if (!empty($arrVwAvaliacaoAluno)) {
                $negocio = new \G2\Negocio\Negocio();
                foreach ($arrVwAvaliacaoAluno as $vwAvaliacaoAlunoTO) {

                    // GII-6605
                    // Se a evolução da matrícula for Concluinte ou Certificado, a nota do Moodle NÃO pode sobrescreve-la
                    $matricula = $negocio->getRepository(\G2\Entity\Matricula::class)
                        ->verificarMatriculaConcluinteOuCursando($vwAvaliacaoAlunoTO->getId_matricula());

                    if ($matricula) {
                        continue;
                    }

                    // GII-974
                    // Se o aluno tiver nota de APROVEITAMENTO para essa avaliação, a nota do Moodle NÃO pode sobrescreve-la
                    $nota_aproveitamento = $negocio->getRepository(\G2\Entity\AvaliacaoAluno::class)
                        ->verificarAlunoNotaAproveitamento(
                            $vwAvaliacaoAlunoTO->getId_matricula(),
                            $vwAvaliacaoAlunoTO->getId_avaliacao(),
                            $vwAvaliacaoAlunoTO->getId_avaliacaoconjuntoreferencia()
                        );

                    if ($nota_aproveitamento) {
                        continue;
                    }

                    if ($vwAvaliacaoAlunoTO->getSt_notaava() != null && $vwAvaliacaoAlunoTO->getSt_notaava() != '') {
                        $nota = round(floatval($vwAvaliacaoAlunoTO->getSt_notaava()));
                    } else {
                        $nota = 0;
                    }

                    $aaTO = new AvaliacaoAlunoTO();
                    $aaTO->setId_matricula($vwAvaliacaoAlunoTO->getId_matricula());
                    $aaTO->setId_avaliacao($vwAvaliacaoAlunoTO->getId_avaliacao());
                    $aaTO->setSt_nota($nota, false);
                    $aaTO->setDt_avaliacao(new Zend_Date());
                    $aaTO->setId_tiponota(AvaliacaoAlunoTO::NOTA_NORMAL);
                    $aaTO->setId_avaliacaoconjuntoreferencia($vwAvaliacaoAlunoTO->getId_avaliacaoconjuntoreferencia());

                    // GII-985 - Especificando a origem da avaliacao;
                    $aaTO->setId_sistema(\G2\Constante\Sistema::PORTAL_ALUNO);

                    $mensageiro = $bo->salvarAvaliacaoAluno($aaTO, $id_aluno);


                    /**
                     * GII-9258 - Esta mudança foi necessária para que seja possivel ocorrer a sincronização
                     * quando o tipo de avaliacao for conceitual
                     */
                    $tipoAvaliacao = null;
                    if ($vwAvaliacaoAlunoTO->getId_tipoavaliacao() == \G2\Constante\TipoAvaliacao::CONCEITUAL) {
                        $aaTO->setSt_nota(null);
                        $aaTO->setId_notaconceitual($nota, false);
                        $tipoAvaliacao = array(
                            'id_tipoavaliacao' => \G2\Constante\TipoAvaliacao::CONCEITUAL,
                            'id_notaconceitual' => $nota
                        );
                    }

                    $mensageiro = $bo->salvarAvaliacaoAluno($aaTO, $id_aluno, true, $tipoAvaliacao);

                    if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                        throw new Zend_Exception($mensageiro->getCurrent());
                    }
                }
            }

            /** Terceira alteração
             * Tratar realmente os erros, baseado em e-mails, tenho visto que talvez alguns erros estejam deixando rastros no sistema
             * Analisar isso.
             */
            return new Ead1_Mensageiro("Notas sincronizadas com Sucesso! ", Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Método responsável pela remoçao de um determinado papel atribuído ao usuário
     * @param $idUsuario
     * @param $roleID
     * @param $idContext
     * @param null $id_entidade
     * @return Ead1_Mensageiro
     */
    public function desvincularPerfil($idUsuario, $roleID, $idContext, $id_entidade = null)
    {

        try {
            $entidadeIntegracaoTO = $this->getEntidadeIntegracaoTO();
            if (!$entidadeIntegracaoTO) {
                throw new Zend_Exception('Entidade não Integrada ao Sistema!');
            }
            $usuarioIntegracaoTO = new UsuarioIntegracaoTO();
            $usuarioIntegracaoTO->setId_usuario($idUsuario);
            $usuarioIntegracaoTO->setId_sistema(SistemaTO::MOODLE);
            $usuarioIntegracaoTO->setId_entidade(($id_entidade ? $id_entidade : $usuarioIntegracaoTO->getSessao()->id_entidade));
            $usuarioIntegracaoTO->setId_entidadeintegracao($entidadeIntegracaoTO->getId_entidadeintegracao());
            $usuarioIntegracaoTO->fetch(false, true, true);


            $moodleUsuario = new MoodleUsuariosWebServices(null, $entidadeIntegracaoTO->getId_entidadeintegracao());

            if (!$usuarioIntegracaoTO->getSt_codusuario()) {
                $vwPessoaTO = new VwPessoaTO();
                $vwPessoaTO->setId_usuario($idUsuario);
                $vwPessoaTO->setId_entidade(($id_entidade ? $id_entidade : $vwPessoaTO->getSessao()->id_entidade));
                $mensageiro = $moodleUsuario->cadastrarUsuario($vwPessoaTO->fetch());

                if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception($mensageiro->getFirstMensagem());
                }

                $usuarioIntegracaoTO->fetch(false, true, true);
            }


            $ddsPesquisaUsuarioMoodle = $moodleUsuario->retornarUsuarioByUserName($usuarioIntegracaoTO->getSt_loginintegrado());

            //verifica se não encontrou o usuário no moodle
            if (!count($ddsPesquisaUsuarioMoodle)) {
                throw new Exception("Usuário não encontrado no Moodle. ID: {$idUsuario}");
            }

            $dadosEnrol = array();
            $dados = array();
            $dadosEnrol[0]['roleid'] = $roleID;
            $dadosEnrol[0]['contextlevel'] = 'course';
            $dadosEnrol[0]['userid'] = $usuarioIntegracaoTO->getSt_codusuario();
            $dadosEnrol[0]['instanceid'] = (int)$idContext;

            $dados['wstoken'] = $entidadeIntegracaoTO->getSt_codchave();
            $dados['unassignments'] = $dadosEnrol;

            $dados['wsfunction'] = 'core_role_unassign_roles';

            $post = $this->clientRest->restPost("webservice/rest/server.php", $dados);

            $this->verificaWsErro($post->getBody(), 'Perfil');
            $dadosIntegracao = Ead1_BO::parse($post->getBody());
            if (isset($dadosIntegracao["MESSAGE"])) {
                foreach ($dadosIntegracao["MESSAGE"] as $chave => $dado) {
                    throw new Zend_Exception("ERRO ao integrar perfil! " . $dado);
                }
            }

            return new Ead1_Mensageiro($dadosEnrol[0], Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }


    /**
     * @param $matricula
     * @param array $params
     * @return array
     * @throws Exception
     */
    public function suspenderAlunoSalas($matricula, $params = array())
    {
        $negocio = new \G2\Negocio\Negocio();

        if ($matricula && !($matricula instanceof \G2\Entity\Matricula)) {
            $matricula = $negocio->find('\G2\Entity\Matricula', $matricula);
        }

        if (!($matricula instanceof \G2\Entity\Matricula)) {
            throw new Exception('Nenhuma matrícula encontrada.');
        }

        $results = array();
        $success = array();
        $error = array();

        $id_matricula = $matricula->getId_matricula();
        $id_usuario = $matricula->getId_usuario()->getId_usuario();

        /*
         Verificar se o aluno possui a mesma sala em mais de
         um projeto, se possuir NÃO deve Suspender o papel do Moodle
        */

        /** @var \G2\Repository\Alocacao $rep */
        $salas = $negocio->getRepository('\G2\Entity\Alocacao')
            ->retornarAlocacoesSalas(
                array_merge(
                    array(
                        'mt.id_usuario' => $id_usuario
                    ),
                    $params
                )
            );

        if ($salas) {
            foreach ($salas as $sala) {
                $id_saladeaula = $sala['id_saladeaula'];

                /*
                Apesar de verificar com salas de outras matrículas, a verificação deve ser baseada numa sala da mesma
                matrícula
                */
                if ($sala['id_matricula'] != $id_matricula) {
                    continue;
                }

                $repeticoes = array_filter($salas, function ($item) use ($id_matricula, $id_saladeaula) {
                    // As repetições devem considerar somente as matrículas com evolução "Cursando"
                    return $item['id_matricula'] != $id_matricula
                        && $item['id_saladeaula'] == $id_saladeaula
                        && $item['id_evolucao'] == \G2\Constante\Evolucao::TB_MATRICULA_CURSANDO;
                });

                // Suspender o papel de Aluno do Moodle SOMENTE se a sala estiver em só uma matricula do aluno
                if (!$repeticoes) {
                    $results[] = $sala;

                    $mensageiro = $this->setEntidadeIntegracao($sala["id_entidadeintegracao"])
                        ->cadastrarPapel(
                            $id_usuario,
                            $id_saladeaula,
                            \MoodleAlocacaoWebServices::ROLEID_ESTUDANTE,
                            $sala['id_entidade'],
                            true,
                            'cadastrarAlocacaoIntegracao'
                        );

                    if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
                        $success[] = $sala;
                    } else {
                        $error[] = $sala;
                    }
                }

            }
        }

        return array(
            'results' => $results,
            'error' => $error,
            'success' => $success
        );
    }

    /**
     * @description Retornar o token de acesso ao Moodle
     * @param int $id_entidade
     * @param string $st_loginintegrado
     * @return Ead1_Mensageiro
     * @throws Exception
     */
    public function requestTokenAcesso($id_entidade, $st_loginintegrado)
    {
        try {

            if (!$st_loginintegrado)
                throw new \Exception("Login do usuário no Moodle não informado!");


            if (!$id_entidade)
                throw new \Exception("Entidade não informada!");


            $entidadeIntegracaoTO = $this->getEntidadeIntegracaoTO();

            if (!$entidadeIntegracaoTO->getId_entidade()) {
                throw new Zend_Exception("Não existe interface de integração para o sistema Moodle nesta entidade!");
            }

            $dados['wstoken'] = $entidadeIntegracaoTO->getSt_codchave();
            $dados['user']['username'] = $st_loginintegrado;
            $dados['wsfunction'] = 'auth_userkey_request_login_url';

            $post = $this->clientRest->restPost("webservice/rest/server.php", $dados);

            $this->verificaWsErro($post->getBody(), 'loginurl');
            $dadosIntegracao = Ead1_BO::parse($post->getBody());


            if (isset($dadosIntegracao['SINGLE'][0]['KEY'][0]['VALUE'][0])) {
                return new Ead1_Mensageiro($dadosIntegracao['SINGLE'][0]['KEY'][0]['VALUE'][0]);
            }

            if (isset($dadosIntegracao["MESSAGE"])) {
                foreach ($dadosIntegracao["MESSAGE"] as $chave => $dado) {
                    throw new Zend_Exception("ERRO ao integrar alocação! " . $dado);
                }
            }

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Retorna informações que possibilitam o Aluno a logar no webservice da plataforma Moodle por token
     * @param $id_matricula
     * @param $id_usuario
     * @return Ead1_Mensageiro
     */
    public function getWsToken($id_matricula, $id_usuario)
    {

        try {

            if (!$id_matricula) {
                throw new \Exception("Informe a Matrícula do Aluno");
            }

            $entidadeIntegracao = $this->getEntidadeIntegracaoTO();

            if (!$entidadeIntegracao->getSt_codsistema()) {
                throw new \Exception("Esta Entidade não está integrada");
            }

            $userIntegracao = new \G2\Entity\UsuarioIntegracao();
            $userIntegracao->findOneBy(array(
                'id_entidade' => $entidadeIntegracao->getId_entidade(),
                'id_usuario' => $id_usuario,
                'id_sistema' => SistemaTO::MOODLE,
                "id_entidadeintegracao" => $entidadeIntegracao->getId_entidadeintegracao()
            ));

            if (!$userIntegracao && !$userIntegracao->getSt_loginintegrado()) {
                throw new \Exception("Este Usuário não está integrado");
            }

            $url = substr($entidadeIntegracao->getSt_codsistema(), 0, -26);
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url . "/login/token.php");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(
                array(
                    'username' => $userIntegracao->getSt_loginintegrado(),
                    'password' => $userIntegracao->getSt_senhaintegrada(),
                    'service' => 'moodle_mobile_app'
                )
            ));

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $server_output = curl_exec($ch);
            curl_close($ch);

            if (!$server_output) {
                throw new \Exception("O Moodle não retornou resposta sobre este usuário");
            }

            $moodle_token = json_decode($server_output);
            if (!$moodle_token) {
                throw new \Exception("Não foi possível identificar a resposta do Moodle");
            }

            if (isset($moodle_token->error)) {
                throw new \Exception($moodle_token->error);
            }

            $moodle_token->userid = $userIntegracao->getSt_codusuario();
            $moodle_token->siteurl = $url;
            $moodle_token->st_loginintegrado = $userIntegracao->getSt_loginintegrado();
            $moodle_token->id_entidade = $userIntegracao->getId_entidade();

            return new \Ead1_Mensageiro($moodle_token, \Ead1_IMensageiro::SUCESSO);


        } catch (\Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }

    }

}

<?php

/**
 * Classe para interações de cursos
 * @author ederlamar
 * Class MoodleCursosWebServices
 */
class MoodleCursosWebServices extends MoodleWebServices
{

    /** @var Ead1_Mensageiro */
    public $mensageiro;

    /** @var SalaDeAulaDAO */
    private $dao;


    /**
     * MoodleCursosWebServices constructor.
     * @param null $id_entidade
     * @param null $idEntidadeIntegracao
     */
    public function __construct($id_entidade = null, $idEntidadeIntegracao = null)
    {
        parent::__construct($id_entidade, $idEntidadeIntegracao);
        $this->mensageiro = new Ead1_Mensageiro();
        $this->dao = new SalaDeAulaDAO();
    }


    /**
     * Método que cadastra um curso do Moodle
     * @param array $vwSalaDeAulaTOs
     * @param bool $array_disciplinaintegracao
     * @return Ead1_Mensageiro
     */
    public function cadastrarCurso(array $vwSalaDeAulaTOs, $array_disciplinaintegracao = false)
    {
        $dadosCurso = array();
        $dados = array();
        $salaDeAulaIntegradas = array();
        $courses = array();

        $this->dao->beginTransaction();
        try {
            foreach ($vwSalaDeAulaTOs as $vwSalaDeAulaTO) {
                $salaDeAulaIntegracaoTOconsulta = new SalaDeAulaIntegracaoTO();
                $salaDeAulaIntegracaoTOconsulta->setId_sistema(SistemaTO::MOODLE);
                $salaDeAulaIntegracaoTOconsulta->setId_saladeaula($vwSalaDeAulaTO->getId_saladeaula());
                $salaDeAulaIntegracaoTOconsulta->fetch(false, true, true);


                if (!$salaDeAulaIntegracaoTOconsulta->getSt_codsistemacurso()) {
                    $dadosCurso['fullname'] = $vwSalaDeAulaTO->getSt_saladeaula();
                    $dadosCurso['shortname'] = $vwSalaDeAulaTO->getSt_saladeaula();
                    $dadosCurso['categoryid'] = $this->entidadeIntegracaoTO->getSt_codarea();
                    $dadosCurso['idnumber'] = $vwSalaDeAulaTO->getId_saladeaula();

                    $startDate = ($vwSalaDeAulaTO->getBl_usardoperiodoletivo()
                        ? $vwSalaDeAulaTO->getDt_aberturaletivo() : $vwSalaDeAulaTO->getDt_abertura());


                    if (!$startDate) {
                        $startDate = new Zend_Date(null);
                    }
                    $dadosCurso['startdate'] = $startDate->getTimestamp();

                    if ($array_disciplinaintegracao != false) {

                        //Recupera os dados da tb_disciplinaintegração pelo id_disciplinaintegracao
                        $disciplinaIntegracao = $this->negocio
                            ->find('\G2\Entity\DisciplinaIntegracao',
                                (int)$array_disciplinaintegracao[$vwSalaDeAulaTO->getId_saladeaula()]);


                        if ($disciplinaIntegracao instanceof \G2\Entity\DisciplinaIntegracao
                            && $disciplinaIntegracao->getSt_codsistema()
                        ) {

                            //Recupera os dados da dos cursos do Moodle pelo st_codsistema
                            $retornoCourseMoodle = $this
                                ->getCourseMoodle($disciplinaIntegracao->getSt_codsistema(), null, true);

                            if ($retornoCourseMoodle->getTipo() == Ead1_IMensageiro::SUCESSO) {

                                $mensagemCourse = $retornoCourseMoodle->getMensagem();
                                $ddsCourse = get_object_vars($mensagemCourse[0]);

                                $dadosCurso['numsections'] = $ddsCourse['numsections'];
                                $dadosCurso['format'] = $ddsCourse['format'];

                            } else {
                                $dadosCurso['format'] = "topics";
                            }
                        }

                    } else {
                        $dadosCurso['format'] = "topics";
                    }
                    //visible padrão 0
                    $dadosCurso['visible'] = (int)0;

                    $courses[$vwSalaDeAulaTO->getId_saladeaula()] = $dadosCurso;
                }
            }


            $courses = array_values($courses);
            if (!empty($courses)) {

                $entidadeIntegracaoTO = $this->getEntidadeIntegracaoTO();
                if (!$entidadeIntegracaoTO) {
                    throw new Zend_Exception('Entidade não Integrada ao Sistema!');
                }
                $dados['courses'] = $courses;
                $dados['wsfunction'] = 'core_course_create_courses';
                $dados['wstoken'] = $entidadeIntegracaoTO->getSt_codchave();
                $post = $this->clientRest->restPost("webservice/rest/server.php", $dados);
                $this->verificaWsErro($post->getBody(), 'Curso');
                $wsRespose = $post->getBody();
                $dadosIntegracao = Ead1_BO::parse($wsRespose);
                if (isset($dadosIntegracao["MULTIPLE"][0]["SINGLE"])) {
                    foreach ($dadosIntegracao["MULTIPLE"][0]["SINGLE"] as $chave => $dado) {
                        $salaDeAulaIntegracaoTO = new SalaDeAulaIntegracaoTO();
                        $salaDeAulaIntegracaoTO->setId_saladeaula($vwSalaDeAulaTOs[$chave]->getId_saladeaula());
                        $salaDeAulaIntegracaoTO->setId_usuariocadastro($salaDeAulaIntegracaoTO->getSessao()->id_usuario);
                        $salaDeAulaIntegracaoTO->setId_sistema(SistemaTO::MOODLE);
                        $salaDeAulaIntegracaoTO->setSt_codsistemacurso($dado['KEY'][0]['VALUE'][0]);

                        $salaDeAulaIntegracaoTO->setBl_visivel(false);
                        $salaDeAulaIntegracaoTO->setId_disciplinaintegracao($array_disciplinaintegracao[$vwSalaDeAulaTOs[$chave]->getId_saladeaula()]);

                        $salaDeAulaIntegracaoORM = new SalaDeAulaIntegracaoORM();
                        $salaDeAulaIntegradas[] = $salaDeAulaIntegracaoORM->insert($salaDeAulaIntegracaoTO->toArrayInsert());
                    }
                } else {
                    throw new Zend_Exception((isset($dadosIntegracao['MESSAGE']) ? $dadosIntegracao['MESSAGE'][0] : $dadosIntegracao));
                }
            } else {
                throw new Zend_Exception("Sem salas para integrar!");
            }

            $this->dao->commit();
            return new Ead1_Mensageiro($salaDeAulaIntegradas);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Retorna dados do curso moodle WS
     * [wsfunction] => core_course_get_courses
     * @param $courseId
     * @param bool $id_entidade
     * @param bool $returnJson
     * @return Ead1_Mensageiro
     * @throws Exception
     */
    public function getCourseMoodle($courseId, $id_entidade = false, $returnJson = true)
    {
        $dados = array();
        try {
            if (!$courseId)
                throw new Exception('O código do curso moodle é obrigatório!');

            $ids['ids'][0] = (int)$courseId;

            $entidadeIntegracao = $this->getEntidadeIntegracaoTO();

            if (!$entidadeIntegracao->getSt_codchave()) {
                throw new Zend_Exception('Entidade não integrada ao Sistema!');
            }


            $dados['options'] = $ids;
            $dados['wsfunction'] = 'core_course_get_courses';
            $dados['wstoken'] = $entidadeIntegracao->getSt_codchave();
            if ($returnJson)
                $dados['moodlewsrestformat'] = 'json';

            $post = $this->clientRest->restPost("webservice/rest/server.php", $dados);
            $json = (array)json_decode($post->getBody());

            if ($json) {
                return new Ead1_Mensageiro($json);
            } else
                throw new Exception('Nenhum dado retornado. Curso não encontrado no moodle.');

        } catch (Zend_Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Método para importação de conteúdos do moodle
     * @param SalaDeAulaTO $salaDeAulaTo
     * @param unknown_type $id_entidade
     * @throws Zend_Validate_Exception
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function importarConteudoSalaDeAula(SalaDeAulaTO $salaDeAulaTo, $id_entidade = null)
    {
        try {

            $entidadeIntegracaoORM = new EntidadeIntegracaoORM();
            $entidadeIntegracaoTO = new EntidadeIntegracaoTO();
            $entidadeIntegracaoTO->setId_sistema(SistemaTO::MOODLE);
            $entidadeIntegracaoTO->setId_entidade(($id_entidade ? $id_entidade : $salaDeAulaTo->getSessao()->id_entidade));
            $entidadeIntegracaoTO = $entidadeIntegracaoORM->consulta($entidadeIntegracaoTO, false, true);
            if (!$entidadeIntegracaoTO) {
                THROW new Zend_Validate_Exception('Entidade não Integrada ao Sistema!');
            }

            if (!$salaDeAulaTo->getId_saladeaula()) {
                throw new Zend_Validate_Exception('O id_saladeaula é obrigatório e não foi passado!');
            }

            $salaDeAulaIntegracao = new SalaDeAulaIntegracaoTO();
            $salaDeAulaIntegracao->setId_saladeaula($salaDeAulaTo->getId_saladeaula());
            $salaDeAulaIntegracao->setId_sistema(SistemaTO::MOODLE);
            $salaDeAulaIntegracao->fetch(false, true, true);

            if (!$salaDeAulaIntegracao->getId_saladeaulaintegracao()) {
                throw new Zend_Validate_Exception("Nenhum conteudo encontrado!");
            }

            if ($salaDeAulaIntegracao->bl_conteudo) {
                return new Ead1_Mensageiro('Conteúdo já foi importado!', Ead1_IMensageiro::AVISO);
            }

            $disciplinaSalaDeAula = new DisciplinaSalaDeAulaTO();
            $disciplinaSalaDeAula->setId_saladeaula($salaDeAulaTo->getId_saladeaula());
            $disciplinaSalaDeAula->fetch(false, true, true);

            if (!$disciplinaSalaDeAula->getId_disciplina()) {
                throw new Zend_Validate_Exception("Nenhuma disciplina encontrada!");
            }

            $boDisciplina = new DisciplinaBO();
            $disciplinaIntegracao = new DisciplinaIntegracaoTO();
            $disciplinaIntegracao->setId_disciplina($disciplinaSalaDeAula->getId_disciplina());
            $disciplinaIntegracao->setId_sistema(SistemaTO::MOODLE);
            //Apenas as cadastradas e ativas
            $disciplinaIntegracao->setBl_ativo(true);
            //Apenas da entidade setada
            $disciplinaIntegracao->setId_entidade($entidadeIntegracaoTO->getId_entidade());

            $disciplinaIntegracao->fetch(false, true, true);

            if (!$disciplinaIntegracao->getId_disciplinaintegracao()) {
                throw new Zend_Validate_Exception("Não foi encontrada nenhum código para sincronização da disciplina!");
            }

            $dados["importfrom"] = (int)$disciplinaIntegracao->getSt_codsistema();
            $dados["importto"] = (int)$salaDeAulaIntegracao->getSt_codsistemacurso();
            $dados["deletecontent"] = 0;
            $dados["options"] = array(array('name' => 'blocks', 'value' => 1)
            , array('name' => 'activities', 'value' => 1)
            , array('name' => 'filters', 'value' => 1));
            $dados['wstoken'] = $entidadeIntegracaoTO->getSt_codchave();
            $dados['wsfunction'] = 'core_course_import_course';
            $post = $this->clientRest->restPost("webservice/rest/server.php", $dados);

            $this->verificaWsErro($post->getBody(), 'IntegracaoConteudo');
            $dadosIntegracao = Ead1_BO::parse($post->getBody());

            if (isset($dadosIntegracao["MESSAGE"])) {
                foreach ($dadosIntegracao["MESSAGE"] as $chave => $dado) {
                    throw new Zend_Exception("ERRO ao importar conteudos! ->" . $dado);
                }
            }

            return new Ead1_Mensageiro($dadosIntegracao, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $ve) {
            return new Ead1_Mensageiro($ve->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Método que altera um curso do Moodle
     * @param array $vwSalaDeAulaTOs
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function alterarVisibilidadeCurso(VwSalaDeAulaVisivelMoodleTO $vw)
    {
        $this->dao->beginTransaction();
        $dadosCurso = array();
        $dados = array();
        try {
            $salaDeAulaIntegracaoTOconsulta = new SalaDeAulaIntegracaoTO();
            $salaDeAulaIntegracaoTOconsulta->setId_sistema(SistemaTO::MOODLE);
            $salaDeAulaIntegracaoTOconsulta->setId_saladeaula($vw->getId_saladeaula());
            $salaDeAulaIntegracaoTOconsulta->fetch(false, true, true);
            if ($salaDeAulaIntegracaoTOconsulta->getSt_codsistemacurso()) {
                $dadosCurso['id'] = $vw->getId_saladeaulaintegracao();
                $dadosCurso['visible'] = (int)1;
                $courses[0] = $dadosCurso;
            }
            $courses = array_values($courses);
            if (!empty($courses)) {
                $entidadeIntegracaoORM = new EntidadeIntegracaoORM();
                $entidadeIntegracaoTO = new EntidadeIntegracaoTO();
                $entidadeIntegracaoTO->setId_entidade($vw->getId_entidade());
                $entidadeIntegracaoTO->setId_sistema(SistemaTO::MOODLE);
                $entidadeIntegracaoTO = $entidadeIntegracaoORM->consulta($entidadeIntegracaoTO, false, true);
                if (!$entidadeIntegracaoTO) {
                    THROW new Zend_Exception('Entidade não Integrada ao Sistema!');
                }
                $dados['courses'] = $courses;
                $dados['wsfunction'] = 'core_course_update_courses';
                $dados['wstoken'] = $entidadeIntegracaoTO->getSt_codchave();
                $post = $this->clientRest->restPost("webservice/rest/server.php", $dados);
                $this->verificaWsErro($post->getBody(), 'Curso');
                $wsRespose = $post->getBody();
                $dadosIntegracao = Ead1_BO::parse($wsRespose);
                if (isset($dadosIntegracao["MULTIPLE"][0]["SINGLE"])) {
                    foreach ($dadosIntegracao["MULTIPLE"][0]["SINGLE"] as $chave => $dado) {
                        $salaDeAulaIntegracaoTO = new SalaDeAulaIntegracaoTO();
                        $salaDeAulaIntegracaoTO->setId_saladeaula($vw->getId_saladeaula());
                        $salaDeAulaIntegracaoTO->setBl_visivel(1);

                        $salaDeAulaIntegracaoORM = new SalaDeAulaIntegracaoORM();
                        $salaDeAulaIntegracaoORM->update($vw->toArrayUpdate(false), $salaDeAulaIntegracaoORM->montarWhere($vw, true));
                    }
                } else {
                    throw new Zend_Exception($dadosIntegracao['MESSAGE'][0]);
                }
            } else {
                throw new Zend_Exception("Sem salas para alterar!");
            }

            $this->dao->commit();
            return new Ead1_Mensageiro($salaDeAulaIntegracaoTO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Busca o status das atividades no Moodle
     * @param EntidadeIntegracaoTO $ei
     * @param $userid
     * @param $courseid
     * @param bool $returnjson
     * @return array
     */
    public function retornarStatusCurso(EntidadeIntegracaoTO $ei, $userid, $courseid, $returnjson = true)
    {
        $concluidas = 0;
        $naoconcluidas = 0;
        $nu_percentualconcluido = 0;

        try {

            if ($returnjson)
                $dados['moodlewsrestformat'] = 'json';

            $dados['courseid'] = $courseid;
            $dados['userid'] = $userid;
            $dados['wsfunction'] = 'core_completion_get_activities_completion_status';
            $dados['wstoken'] = $ei->getSt_codchave();
            $post = $this->clientRest->restPost("webservice/rest/server.php", $dados);

            $json = (array)json_decode($post->getBody());

            if (isset($json['statuses']) && $json['statuses']) {
                foreach ($json['statuses'] as $status) {
                    if ($status->state == 1) {
                        $concluidas++;
                    } else {
                        $naoconcluidas++;
                    }
                }

                $nu_percentualconcluido = (int)round((($concluidas * 100) / ($concluidas + $naoconcluidas)));

            }

        } catch (\Exception $e) {
        }

        if (!isset($json['statuses']))
            $json['statuses'] = null;

        return array(
            'nu_concluidas' => $concluidas,
            'nu_naoconcluidas' => $naoconcluidas,
            'nu_percentualconcluido' => $nu_percentualconcluido,
            'ar_atividades' => $json['statuses']
        );


    }

    /**
     * Retorna sala de referencia do moodle
     * @param SalaDeAulaTO $salaDeAulaTO (id_saladeaula)
     * @return Ead1_Mensageiro
     */
    public function retornaSalaReferenciaMoodle(SalaDeAulaTO $salaDeAulaTO)
    {
        $negocio = new \G2\Negocio\SalaDeAula();
        try {
            $codCourse = '';
            $salaDeAulaIntegracao = $negocio->findOneBy('\G2\Entity\SalaDeAulaIntegracao', array('id_saladeaula' => (int)$salaDeAulaTO->getId_saladeaula()));
            if ($salaDeAulaIntegracao instanceof \G2\Entity\SalaDeAulaIntegracao && $salaDeAulaIntegracao->getId_disciplinaintegracao() instanceof \G2\Entity\DisciplinaIntegracao
                && $salaDeAulaIntegracao->getId_disciplinaintegracao()->getSt_codsistema()
            ) {
                $codCourse = $salaDeAulaIntegracao->getId_disciplinaintegracao()->getSt_codsistema();
            }
            if ($codCourse) {
                return $this->getCourseMoodle($codCourse, $salaDeAulaIntegracao->getId_disciplinaintegracao()->getId_entidade()->getId_entidade(), true);
            } else {
                return new Ead1_Mensageiro('Nenhuma sala de referência cadastrada para a sala de aula.', Ead1_IMensageiro::AVISO);
            }

        } catch (Exception $ex) {
            return new Ead1_Mensageiro('Erro ao retornar sala de referência: ' . $ex->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Importa dados de uma sala de aula do moodle para outra
     * Usa uma sala de Referência pré cadastrada pela equipe do acadêmico e parametrizada na disciplina da sala de aula
     * WS => core_course_import_course
     * @author Denise Xavier
     * @param SalaDeAulaTO $salaDeAula
     * @param bool $returnJson
     * @return Ead1_Mensageiro
     */
    public function importarSalaCursoReferenciaMoodle(SalaDeAulaTO $salaDeAula, $returnJson = true)
    {
        $dados = array();
        $codCourseFrom = false;
        $codCourseTo = false;
        $negocio = new \G2\Negocio\SalaDeAula();

        try {
            $salaDeAulaIntegracao = $negocio->findOneBy('\G2\Entity\SalaDeAulaIntegracao',
                array('id_saladeaula' => (int)$salaDeAula->getId_saladeaula()));

            if ($salaDeAulaIntegracao instanceof \G2\Entity\SalaDeAulaIntegracao
                && $salaDeAulaIntegracao->getId_disciplinaintegracao() instanceof \G2\Entity\DisciplinaIntegracao
                && $salaDeAulaIntegracao->getId_disciplinaintegracao()->getSt_codsistema()
            ) {
                $codCourseFrom = $salaDeAulaIntegracao->getId_disciplinaintegracao()->getSt_codsistema();
                $codCourseTo = $salaDeAulaIntegracao->getSt_codsistemacurso();
            }

            if (!$codCourseFrom || !$codCourseTo) {
                throw new Exception('O código do curso de destino e origem são obrigatórios. 
                Verifique a integração da sala e da disciplina (Sala de Referência)');
            }


            //busca os dados da integração com moodle
            $entidadeIntegracao = $this->getEntidadeIntegracaoTO();

            if (!$entidadeIntegracao && !$entidadeIntegracao->getSt_codchave()) {
                THROW new Zend_Exception('Entidade não integrada ao Sistema!');
            }

            $dados['importfrom'] = (int)$codCourseFrom;
            $dados['importto'] = (int)$codCourseTo;
            $dados['deletecontent'] = 1;
            $dados['options'] = array(
                array('name' => 'blocks', 'value' => 1),
                array('name' => 'activities', 'value' => 1),
                array('name' => 'filters', 'value' => 1)
            );

            $dados['wsfunction'] = 'core_course_import_course';
            $dados['wstoken'] = $entidadeIntegracao->getSt_codchave();
            if ($returnJson)
                $dados['moodlewsrestformat'] = 'json';

            $post = $this->clientRest->restPost("webservice/rest/server.php", $dados);

            $json = (array)json_decode($post->getBody());
            if (is_array($json)) {
                return new Ead1_Mensageiro($json, Ead1_IMensageiro::SUCESSO);
            }

            return new Ead1_Mensageiro($json, Ead1_IMensageiro::AVISO);


        } catch (Exception $ex) {
            return new Ead1_Mensageiro('Erro ao importar sala: ' . $ex->getMessage(), Ead1_IMensageiro::ERRO);
        }


    }


}

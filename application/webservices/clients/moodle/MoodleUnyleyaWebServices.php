<?php

/**
 * Classe para interações de serviço externo Unyeleya
 * @author 2016 -  Denise Xavier
 *
 */
class MoodleUnyleyaWebServices extends MoodleWebServices
{

    public $mensageiro;
    public $negocio;

    public function __construct($id_entidade = null, $idEntidadeIntegracao = null)
    {
        parent::__construct($id_entidade, $idEntidadeIntegracao);
        $this->mensageiro = new Ead1_Mensageiro();
        $this->negocio = new G2\Negocio\Negocio();
    }


    /**
     * Método que retorna atividades recentes de um curso
     * @param $idMatricula
     * @param $idSalaDeAula
     * @param null $idCourse
     * @return Ead1_Mensageiro
     * @throws Exception
     */
    public function listRecentActivities($idMatricula, $idSalaDeAula, $idCourse = null)
    {
        $array_retorno = array();
        try {

            $entidadeIntegracao = $this->getEntidadeIntegracaoTO();

            $matricula = $this->negocio->find('\G2\Entity\Matricula', $idMatricula);
            if (!$matricula instanceof \G2\Entity\Matricula || !$matricula->getId_matricula()) {
                throw new Exception('A matricula passada não é uma matricula válida');
            }
            $idUsuario = $matricula->getId_usuario()->getId_usuario();

            if (!$entidadeIntegracao && !$entidadeIntegracao->getId_entidadeintegracao()) {
                throw new Zend_Exception("Não existe interface de integração para o Moodle nesta entidade!");
            }

            //Pesquisa sala de aula integracao
            $salaDeAulaIntegracaoTO = $this->pesquisarSalaDeAulaIntegracao(new SalaDeAulaTO(array(
                'id_saladeaula' => $idSalaDeAula
            )), $idCourse, \G2\Constante\Sistema::MOODLE);

            //pesquisa o usuario integracao
            $usuarioIntegracao = $this->pesquisarUsuarioIntegracaoTO(new UsuarioTO(array(
                "id_usuario" => $idUsuario
            )));

            if (!$salaDeAulaIntegracaoTO->getSt_codsistemacurso() && !$usuarioIntegracao->getId_usuariointegracao()) {
                throw new Exception('A sala de aula e/ou o usuário 
                não possuem integração com o moodle! Verifique a(s) integração.');
            }

            $dados['wstoken'] = $entidadeIntegracao->getSt_codchavewsuny();
            $dados['course_id'] = (int)$salaDeAulaIntegracaoTO->getSt_codsistemacurso();
            $dados['user_id'] = (int)$usuarioIntegracao->getSt_codusuario();
            $dados['wsfunction'] = 'local_wsunyleya_recent_activity';
            $dados['moodlewsrestformat'] = 'json';

            $post = $this->clientRest->restPost("webservice/rest/server.php", $dados);

            $dadosRetorno = @json_decode($post->getBody());

            if ($dadosRetorno) {
                foreach ($dadosRetorno as $chave => $retorno) {

                    if (!isset($retorno->module_id))
                        throw new Zend_Exception("ERRO ao retornar as atividades recentes! ");

                    $array_retorno[$chave] =
                        array(
                            'id' => $retorno->module_id,
                            'name' => $retorno->module_name,
                            'added' => $retorno->module_added,
                            'url' => $retorno->module_url,
                            'since' => $retorno->since,
                            'data' => date('d/m/Y', $retorno->module_added),
                            'extratext' => $retorno->extratext,
                        );

                }
            }

            return new Ead1_Mensageiro($array_retorno, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }


    /**
     * Método que retorna atividades recentes de um curso
     * @param @idMatricula - int
     * @param @idSalaDeAula - int
     * @param @id_entidade - int
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function uploadUserProfileImage($idUsuario, $idEntidade, $fileRealPath)
    {

        try {

            if (!$idUsuario)
                throw new \Exception("Usuário não informado!");

            $path = realpath($fileRealPath);
            if (!$path)
                throw new \Exception("Imagem não encontrada!");

            $negocio = new \G2\Negocio\IntegracaoNegocio();
            $params = array('id_sistema' => \G2\Constante\Sistema::MOODLE, 'id_entidade' => $idEntidade ? $idEntidade : $negocio->sessao->id_entidade);
            $entidadeIntegracao = $negocio->findOneBy('\G2\Entity\EntidadeIntegracao', $params);


            if (!$entidadeIntegracao instanceof \G2\Entity\EntidadeIntegracao || !$entidadeIntegracao->getId_entidadeintegracao()) {
                throw new Zend_Exception("Não existe interface de integração para o Moodle nesta entidade!");
            }

            $this->setClientRest($entidadeIntegracao->getSt_codsistema());

            $usuarioIntegracao = new UsuarioIntegracaoTO();
            $usuarioIntegracao->setId_sistema(SistemaTO::MOODLE);
            $usuarioIntegracao->setId_usuario($idUsuario);
            $usuarioIntegracao->setId_entidade($entidadeIntegracao->getId_entidade());
            $usuarioIntegracao->fetch(false, true, true);

            if ($usuarioIntegracao->getId_usuariointegracao()) {

                $type = pathinfo($path, PATHINFO_EXTENSION);
                $data = file_get_contents($path);

                if (!$data || !$type)
                    throw new \Exception("Não foi possível recuperar os dados da Imagem!");

                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

                $dados['wstoken'] = $entidadeIntegracao->getSt_codchavewsuny();
                $dados['user_id'] = (int)$usuarioIntegracao->getSt_codusuario();
                $dados['base64_file_string'] = $base64;
                $dados['file_name'] = basename($path);
                $dados['wsfunction'] = 'local_wsunyleya_upload_profileimage';
                $dados['moodlewsrestformat'] = 'json';

                $post = $this->clientRest->restPost("webservice/rest/server.php", $dados);

                $dadosRetorno = @json_decode($post->getBody());
                if (!$dadosRetorno)
                    throw new \Exception("Houve algum erro no servidor!");

                if (!isset($dadosRetorno->tipo) || !$dadosRetorno->tipo) {

                    if (isset($dadosRetorno->mensagem)) {
                        throw new \Exception($dadosRetorno->mensagem);
                    } else if (isset($dadosRetorno->errorcode)) {
                        throw new \Exception($dadosRetorno->message);
                    } else {
                        throw new \Exception("Erro indefinido ao enviar arquivo ao Moodle");
                    }

                }

            } else {
                throw new Exception('O usuário não possui integração com o moodle! Verifique a(s) integração.');
            }
            return new Ead1_Mensageiro((array)$dadosRetorno, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Método que retorna atividades recentes de um perfil de usuario
     * @param @idMatricula - int
     * @param @idSalaDeAula - int
     * @param @id_entidade - int
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function listRecentInteractions($idUsuario, $idSalaDeAula, $idCourse = null)
    {
        try {
            $entidadeIntegracao = $this->getEntidadeIntegracaoTO();
            if (!$entidadeIntegracao->getId_entidadeintegracao()) {
                throw new Zend_Exception("Não existe interface de integração para o Moodle nesta entidade!");
            }

            $salaDeAulaIntegracaoTO = new SalaDeAulaIntegracaoTO();
            if ($idCourse) {
                $salaDeAulaIntegracaoTO->setSt_codsistemacurso($idCourse);
            } else {
                $salaDeAulaIntegracaoTO->setId_sistema(SistemaTO::MOODLE);
                $salaDeAulaIntegracaoTO->setId_saladeaula($idSalaDeAula);
                $salaDeAulaIntegracaoTO->fetch(false, true, true);
            }

            $usuarioIntegracao = new UsuarioIntegracaoTO();
            $usuarioIntegracao->setId_sistema(SistemaTO::MOODLE);
            $usuarioIntegracao->setId_usuario($idUsuario);
            $usuarioIntegracao->setId_entidade($entidadeIntegracao->getId_entidade());
            $usuarioIntegracao->fetch(false, true, true);

            if ($salaDeAulaIntegracaoTO->getSt_codsistemacurso() && $usuarioIntegracao->getId_usuariointegracao()) {

                $dados['wstoken'] = $entidadeIntegracao->getSt_codchavewsuny();
                $dados['course_id'] = (int)$salaDeAulaIntegracaoTO->getSt_codsistemacurso();
                $dados['user_id'] = (int)$usuarioIntegracao->getSt_codusuario();
                $dados['wsfunction'] = 'local_wsunyleya_recent_interactions';
                $dados['moodlewsrestformat'] = 'json';
                $post = $this->clientRest->restPost("webservice/rest/server.php", $dados);

                $dadosRetorno = @json_decode($post->getBody());
                if (!$dadosRetorno) {
                    throw new \Exception("Houve algum erro no servidor!");

                } else if (isset($dadosRetorno->mensagem)) {
                    throw new \Exception($dadosRetorno->mensagem);

                } else if (isset($dadosRetorno->errorcode)) {
                    throw new \Exception($dadosRetorno->message);

                }

            } else {
                throw new Exception('A sala de aula e/ou o usuário  não possuem integração com o moodle! Verifique a(s) integração.');
            }
            return new Ead1_Mensageiro($dadosRetorno, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }

}


<?php

/**
 *
 * @author ederlamar
 * @todo EM IMPLEMENTAÇÃO
 *
 */
class MoodleUsuariosWebServices extends MoodleWebServices
{


    /**
     * Retorna os campos a serem salvos nos campos customisáveis
     * @param VwPessoaTO $vwPessoaTO
     * @param EntidadeIntegracaoTO $entidadeIntegracaoTO
     * @return array|bool
     */
    protected function _buscarDadosMatricula(VwPessoaTO $vwPessoaTO, EntidadeIntegracaoTO $entidadeIntegracaoTO)
    {

        if (!$vwPessoaTO->getId_usuario()) {
            return false;
        }

        if (!$entidadeIntegracaoTO->getId_entidadeintegracao()) {
            return false;
        }

        $vwMatricula = new VwMatriculaTO();
        $vwMatricula->setId_usuario($vwPessoaTO->getId_usuario());
        $vwMatricula->setId_entidadematriz($entidadeIntegracaoTO->getId_entidade());
        $vwMatricula->setId_situacao(G2\Constante\Situacao::TB_MATRICULA_ATIVA);
        $vwMatricula->setId_evolucao(G2\Constante\Situacao::TB_MATRICULA_EVOLUCAO_CURSANDO);
        $vwMatriculas = $vwMatricula->fetch(false, false, false);
        $customfields = array();
        if ($vwMatriculas) {
            $polos = [];
            $cursos = [];
            $matriculas = [];
            foreach ($vwMatriculas as $matricula) {
                if ($matricula instanceof VwMatriculaTO) {
                    $polos[] = $matricula->getSt_entidadematricula();
                    $cursos[] = $matricula->getSt_projetopedagogico();
                    $matriculas[] = $matricula->getId_matricula();
                }
            }
            $customfields[] = array('type' => 'Polo', 'value' => implode(', ', array_unique($polos)));
            $customfields[] = array('type' => 'Curso', 'value' => implode(', ', array_unique($cursos)));
            $customfields[] = array('type' => 'Matricula', 'value' => implode(', ', array_unique($matriculas)));
        }

        return $customfields;
    }

    /**
     * Método que cadastra via WebService o(s) usuário(s) no moodle
     * @param array $users Array de VwPessoaTO
     * @param array $vwPessoaTOs
     * @return Ead1_Mensageiro
     */
    public function cadastrarUsuario(array $vwPessoaTOs = array())
    {
        try {
            $entidadeIntegracaoTO = $this->getEntidadeIntegracaoTO();

            if (!$entidadeIntegracaoTO) {
                THROW new Zend_Exception('Entidade não Integrada ao Sistema!');
            }
            if ($vwPessoaTOs) {
                foreach ($vwPessoaTOs as $vwPessoaTO) {
                    if ($vwPessoaTO) {
                        $usuarioIntegracaoTOConsulta = new UsuarioIntegracaoTO();
                        $usuarioIntegracaoTOConsulta->setId_sistema(SistemaTO::MOODLE);
                        $usuarioIntegracaoTOConsulta->setId_usuario($vwPessoaTO->getId_usuario());
                        $usuarioIntegracaoTOConsulta->setId_entidade($vwPessoaTO->getId_entidade());
                        $usuarioIntegracaoTOConsulta->setId_entidadeintegracao($entidadeIntegracaoTO->getId_entidadeintegracao());

                        $usuarioIntegracaoTOConsulta->fetch(false, true, true);

                        if (!$usuarioIntegracaoTOConsulta->getSt_codusuario()) {

                            if (empty($vwPessoaTO->getSt_email())) {
                                throw new Exception("E-mail não encontrado.");
                            }

                            $dadosUsuario['username'] = strtolower($vwPessoaTO->getSt_login()) . "." . $vwPessoaTO->getId_entidade();
                            $dadosUsuario['password'] = ($vwPessoaTO->getSt_senha()) . "*G2";
                            $dadosUsuario['firstname'] = $vwPessoaTO->getSt_nomecompleto();
                            $dadosUsuario['lastname'] = '_';
                            $dadosUsuario['email'] = $vwPessoaTO->getSt_email();
                            $dadosUsuario['description'] = 'Usuário importado pelo sistema Gestor2';
                            $dadosUsuario['idnumber'] = (string)$vwPessoaTO->getId_usuario();


                            $customfields = $this->_buscarDadosMatricula($vwPessoaTO, $entidadeIntegracaoTO);
                            if ($customfields) {
                                $dadosUsuario['customfields'] = $customfields;
                            }

                            $users[] = $dadosUsuario;
                        }
                    }
                }
            }

            if (!isset($users)) {
                throw new Zend_Exception("Sem usuários para integrar!");
            }
            $dados['wstoken'] = $entidadeIntegracaoTO->getSt_codchave();
            $dados['users'] = $users;
            $dados['wsfunction'] = 'core_user_create_users';
            $post = null;

            if (!$this->clientRest && !is_object($this->clientRest)) {
                throw new Zend_Exception("Não foi possível enviar os dados!");
            }

            $post = $this->clientRest->restPost("webservice/rest/server.php", $dados);
            $this->verificaWsErro($post->getBody(), 'Usuário');
            $dadosIntegracao = Ead1_BO::parse($post->getBody());

            if (isset($dadosIntegracao["MULTIPLE"][0]["SINGLE"])) {
                foreach ($dadosIntegracao["MULTIPLE"][0]["SINGLE"] as $chave => $dado) {
                    $usuarioIntegracaoTO = new UsuarioIntegracaoTO();
                    $usuarioIntegracaoTO->setId_usuario($vwPessoaTOs[$chave]->getId_usuario());
                    $usuarioIntegracaoTO->setId_entidade($vwPessoaTOs[$chave]->getId_entidade());
                    $usuarioIntegracaoTO->setId_usuariocadastro($vwPessoaTOs[$chave]->getId_usuario());
                    $usuarioIntegracaoTO->setId_sistema(SistemaTO::MOODLE);
                    $usuarioIntegracaoTO->setSt_codusuario($dado['KEY'][0]['VALUE'][0]);
                    $usuarioIntegracaoTO->setSt_loginintegrado($dado['KEY'][1]['VALUE'][0]);
                    $usuarioIntegracaoTO->setSt_senhaintegrada($users[$chave]['password']);
                    $usuarioIntegracaoTO->setId_entidadeintegracao($entidadeIntegracaoTO->getId_entidadeintegracao());

                    /**
                     * Passando false para nào cobrar transação
                     */
                    $usuarioIntegracaoORM = new UsuarioIntegracaoORM();
                    $usuariosIntegrados[] = $usuarioIntegracaoORM->insert($usuarioIntegracaoTO->toArrayInsert(), false);
                }
            }


            return new Ead1_Mensageiro($usuariosIntegrados, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Método criado para atualizar dados do usuário no moodle de acordo com as atualizações do g2
     * Débora Castro 03.12.2013 - debora.castro@unyleya.com.br
     * AC-128
     * @param array $vwPessoaTOs
     * @return Ead1_Mensageiro
     */
    public function atualizarUsuario(array $vwPessoaTOs = array(), $nameFunction = '')
    {
        try {
            $entidadeIntegracaoTO = $this->getEntidadeIntegracaoTO();
            if (!$entidadeIntegracaoTO) {
                THROW new Zend_Exception('Entidade não Integrada ao Sistema!');
            }

            foreach ($vwPessoaTOs as $vwPessoaTO) {
                $usuarioIntegracaoTOConsulta = new UsuarioIntegracaoTO();
                $usuarioIntegracaoTOConsulta->setId_sistema(SistemaTO::MOODLE);
                $usuarioIntegracaoTOConsulta->setId_usuario($vwPessoaTO->getId_usuario());
                $usuarioIntegracaoTOConsulta->setId_entidade($vwPessoaTO->getId_entidade());
                $usuarioIntegracaoTOConsulta->getId_entidadeintegracao($entidadeIntegracaoTO->getId_entidadeintegracao());
                $usuarioIntegracaoTOConsulta->fetch(false, true, true);

                if ($usuarioIntegracaoTOConsulta->getSt_codusuario()) {
                    $dadosUsuario['id'] = $usuarioIntegracaoTOConsulta->getSt_codusuario();
                    $dadosUsuario['firstname'] = $vwPessoaTO->getSt_nomecompleto();
                    $dadosUsuario['email'] = $vwPessoaTO->getSt_email();
                    $dadosUsuario['description'] = 'Usuário importado pelo sistema Gestor2 (atualizado)';
                    $dadosUsuario['alternatename'] = $nameFunction;

                    $customfields = $this->_buscarDadosMatricula($vwPessoaTO, $entidadeIntegracaoTO);
                    if ($customfields) {
                        $dadosUsuario['customfields'] = $customfields;
                    }

                    $users[] = $dadosUsuario;

                }
            }

            if (isset($users)) {
                $dados['wstoken'] = $entidadeIntegracaoTO->getSt_codchave();
                $dados['users'] = $users;
                $dados['wsfunction'] = 'core_user_update_users';
                $post = $this->clientRest->restPost("webservice/rest/server.php", $dados);
                $this->verificaWsErro($post->getBody(), 'Usuário');
                $dadosIntegracao = Ead1_BO::parse($post->getBody());
            } else {
                throw new Zend_Exception("Sem usuários para atualizar na integração!");
            }

            return new Ead1_Mensageiro($dadosIntegracao, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro("Erro ao atualizar dados moodle. " . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * @param array $idUsuariosMoodle
     * @return bool
     * @throws Zend_Exception
     */
    public function retornarUsuarios($idUsuariosMoodle = array())
    {
        $entidadeIntegracaoTO = $this->getEntidadeIntegracaoTO();

        if (!$entidadeIntegracaoTO) {
            THROW new Zend_Exception('Entidade não Integrada ao Sistema!');
        }

        $dados['userids'] = array($idUsuariosMoodle);
        $dados['wsfunction'] = 'core_user_get_users_by_id';
        $dados['wstoken'] = $entidadeIntegracaoTO->getSt_codchave();
        $post = $this->clientRest->restPost("webservice/rest/server.php", $dados);
        $retorno = Ead1_BO::parse($post->getBody());
        $this->verificaWsErro($post->getBody(), 'Alocação');

        if (isset($retorno["MULTIPLE"][0]["SINGLE"])) {
            $resposta = $retorno["MULTIPLE"][0]["SINGLE"][0]["KEY"][32]["VALUE"][0];
        } else {
            $resposta = false;
        }

        return $resposta;
    }

    /**
     * Procura o usuário no moodlel pelo username
     * @param $userName
     * @return array
     * @throws Exception
     */
    public function retornarUsuarioByUserName($userName)
    {
        try {
            $entidadeIntegracaoTO = $this->getEntidadeIntegracaoTO();
            //pesquisa pela "username" da WS do MOODLE o usuário em questão
            $dds['wstoken'] = $entidadeIntegracaoTO->getSt_codchave();
            $dds['wsfunction'] = 'core_user_get_users_by_field';
            $dds['field'] = 'username';
            $dds['values'] = array(0, $userName);
            $dds['moodlewsrestformat'] = 'json';
            $post = $this->clientRest->restPost("webservice/rest/server.php", $dds);

            $this->verificaWsErro($post->getBody(), 'Usuário');

            return json_decode($post->getBody());

        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Retorna a nota detalhada do Moodle
     * @param $usuario
     * @param $curso
     * @return mixed
     * @throws Exception
     */
    public function retornarNotaDetalhada($usuario, $curso)
    {
        try {

            $entidadeIntegracaoTO = $this->getEntidadeIntegracaoTO();
            $params['wstoken'] = $entidadeIntegracaoTO->getSt_codchave();
            $params['wsfunction'] = 'core_grades_get_grades';
            $params['courseid'] = $curso;
            $params['moodlewsrestformat'] = 'json';
            $params['userids[]'] = $usuario;

            $curl = curl_init();

            curl_setopt($curl, CURLOPT_URL, $entidadeIntegracaoTO->getSt_codsistema());
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(
                $params
            ));

            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $server_output = curl_exec($curl);
            $server_error = curl_error($curl);
            curl_close($curl);

            if ($server_error) {
                throw new Exception("Erro ao retornar as notas detalhadas do Moodle: " . $server_error);
            }

            return (json_decode($server_output));

        } catch (Exception $e) {
            throw $e;
        }
    }
}

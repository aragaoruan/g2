<?php

/**
 * super classe para serviços do moodle
 * @author ederlamar
 *
 */
class MoodleWebServices extends WsBO
{


    /**
     * MoodleWebServices constructor.
     * @param null $id_entidade
     * @param null $idEntidadeIntegracao
     * @throws Zend_Exception
     */
    public function __construct($id_entidade = null, $idEntidadeIntegracao = null)
    {

        //verifica se o id da entidade integração foi passado para só assim ir buscar os respectivos dados
        if ($idEntidadeIntegracao) {
            $this->getEntidadeIntegracaoTO(SistemaTO::MOODLE, $id_entidade, $idEntidadeIntegracao);

            //verifica se encontrou os dados de integração
            if (!$this->entidadeIntegracaoTO->getId_entidade()) {
                throw new Zend_Exception("Não existe interface de integração para o sistema Moodle nesta entidade!");
            }

            if ($this->entidadeIntegracaoTO->getSt_codsistema() != 'p') {
                parent::__construct($this->entidadeIntegracaoTO->getSt_codsistema());
            }
        }

    }

    /**
     * Método que verifica se houve erro durante o web service do moodle
     * @param $bodyString
     * @param null $reference
     * @throws Zend_Exception
     */
    public function verificaWsErro($bodyString, $reference = null)
    {
        if (strpos($bodyString, '<EXCEPTION') !== false) {
            if (!is_string($bodyString)) {
                throw new Zend_Exception($bodyString);
            }

            $xml = simplexml_load_string(trim($bodyString));
            if (is_object($xml)) {
                $arrErro = get_object_vars($xml);
                $erroString = "Erro de Integração Moodle"
                    . ($reference ? "[$reference]: " : "")
                    . (isset($arrErro['MESSAGE']) ? $arrErro['MESSAGE'] : "")
                    . (isset($arrErro['DEBUGINFO']) ? isset($arrErro['MESSAGE']) ? ", "
                        . $arrErro['DEBUGINFO'] : $arrErro['DEBUGINFO'] : "");
                throw new Zend_Exception($erroString);
            }
        }

    }
}

?>

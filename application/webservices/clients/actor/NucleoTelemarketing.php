<?php

/**
 * Classe para conexão dos serviços de núcleo de telemarketing no Actor 2/Gestor 1
 * @author Rafael Leite - rafael.leite@unyleya.com.br
 */
class actor_NucleoTelemarketing extends Actor {

    public function __construct($webservicecodentidade,$webservicechave, $url = self::URL){

        try {

            if(!$url){
                throw new Zend_Exception('URL Obrigatória!');
            }

            $this->setWebservicecodentidade($webservicecodentidade);
            $this->setWebservicechave($webservicechave);
            $this->setServico($url.'services/nucleotm.php');
            $this->setClient($this->getServico());

        } catch (Exception $e) {
            throw new Zend_Validate_Exception("Erro ao iniciar o Serviço: ".$e->getMessage());
        }

    }

    /**
     * Retornar os nucleos de Telemarketing do G1
     * @return array
     */
    public function retornarNucleoTelemarketing(){

        try {

            $this->getClient()->retornanucleotm()
                ->webservicecodentidade($this->getWebservicecodentidade())
                ->webservicechave($this->getWebservicechave());

            $dados = $this->object_to_array($this->getClient()->get()->retornanucleotm);

            return $dados ? $dados : false;

        } catch (Exception $e) {
            throw $e;
        }
    }
}

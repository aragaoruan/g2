<?php

/**
 * Classe para conexão dos serviços de Área de Interesse no Actor 2/Gestor 1
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 */
class Actor_AreaInteresse extends Actor {
	
	public function __construct($webservicecodentidade, $webservicechave, $url = self::URL){
		
		try {
			
			if(!$url){
				throw new Zend_Exception('URL Obrigatória!');
			}
			
			$this->setWebservicechave($webservicechave);
			$this->setWebservicecodentidade($webservicecodentidade);
			$this->setServico($url.'services/areainteresse.php');
			$this->setClient($this->getServico());
			
		} catch (Exception $e) {
			throw new Zend_Validate_Exception("Erro ao iniciar o Serviço: ".$e->getMessage());
		}
		
	}
	
	/**
	 * Retornar uma Área de Interesse
	 * @param int $codareainteresse
	 * @return array
	 */
	public function retornar($codareainteresse){
		
		try {
		
			$this->getClient()->retornaareainteresse()
				->webservicecodentidade($this->getWebservicecodentidade())
				->webservicechave($this->getWebservicechave())
				->codareainteresse($codareainteresse);

			$dados = $this->object_to_array($this->getClient()->get()->retornaareainteresse);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
				
	}
	
	
	/**
	 * Lista as Áreas de Interesse
	 * @throws Exception
	 * @return boolean|Ambigous <NULL, array, string, multitype:NULL >
	 */
	public function listar(){
		
		try {
		
			$this->getClient()->listarareainteresse()->webservicecodentidade($this->getWebservicecodentidade())->webservicechave($this->getWebservicechave());

			$dados = $this->object_to_array($this->getClient()->get()->listarareainteresse);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
				
	}
	
	/**
	 * Lista as Áreas de Interesse Global
	 * @throws Exception
	 * @param array $parametros
	 * @return boolean|Ambigous <NULL, array, string, multitype:NULL >
	 */
	public function listarareaglobal(array $parametros = null){
		
		try {
			
			if(!$parametros){
				$parametros = array('codareainteresseglobal'=>false);
			}
		
			$this->getClient()->listarareainteresseglobal()->parametros($parametros)->webservicecodentidade($this->getWebservicecodentidade())->webservicechave($this->getWebservicechave());

			$dados = $this->object_to_array($this->getClient()->get()->listarareainteresseglobal);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
				
	}
	
}

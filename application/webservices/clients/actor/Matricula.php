<?php

/**
 * Classe para conexão dos serviços de Matricula no Actor 2/Gestor 1
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 */
class Actor_Matricula extends Actor {
	
	public function __construct($webservicecodentidade, $webservicechave, $url = self::URL){
		
		try {
			
			if(!$url){
				throw new Zend_Exception('URL Obrigatória!');
			}
			
			$this->setWebservicechave($webservicechave);
			$this->setWebservicecodentidade($webservicecodentidade);
			$this->setServico($url.'services/matricula.php');
			$this->setClient($this->getServico());
			
		} catch (Exception $e) {
			throw new Zend_Validate_Exception("Erro ao iniciar o Serviço: ".$e->getMessage());
		}
		
	}
	
	/**
	 * Retornar uma Área
	 * @param int $codmatricula
	 * @return array
	 */
	public function retornar($codmatricula){
		
		try {
		
			$this->getClient()->retornamatricula()->webservicecodentidade($this->getWebservicecodentidade())->webservicechave($this->getWebservicechave())
				->codmatricula($codmatricula);

			$dados = $this->object_to_array($this->getClient()->get()->retornamatricula);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
				
	}
	
	/**
	 * Lista as Matrículas
	 * @param $codpessoa
	 * @throws Exception
	 * @return boolean|Ambigous <NULL, array, string, multitype:NULL >
	 */
	public function listar($codpessoa){
		
		try {
		
			$this->getClient()->listarmatricula()->webservicecodentidade($this->getWebservicecodentidade())->webservicechave($this->getWebservicechave())
			->codpessoa($codpessoa);

			$dados = $this->object_to_array($this->getClient()->get()->listarmatricula);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
				
	}
	
	
	/**
	 * Insere uma Matrícula
	 * @param array $dados
	 * @throws Exception
	 * @return boolean|Ambigous <NULL, array, string, multitype:NULL >
	 */
	public function inserir(array $dados){
		
		try {
		
			$this->getClient()->inserirmatricula()->webservicecodentidade($this->getWebservicecodentidade())->webservicechave($this->getWebservicechave())
			->dados($dados);

			$dados = $this->object_to_array($this->getClient()->post()->inserirmatricula);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
				
	}

	
	/**
	 * Atualiza os dados de uma Matrícula
	 * @param array $dados
	 * @throws Exception
	 * @return boolean|Ambigous <NULL, array, string, multitype:NULL >
	 */
	public function atualizar(array $dados){
		
		try {
		
			$this->getClient()->atualizarMatricula()->webservicecodentidade($this->getWebservicecodentidade())->webservicechave($this->getWebservicechave())
			->dados($dados);

			$dados = $this->object_to_array($this->getClient()->post()->atualizarMatricula);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
				
	}

	
	/**
	 * Insere uma Pré Matrícula
	 * @param array $parametros
	 * @throws Exception
	 * @return boolean|Ambigous <NULL, array, string, multitype:NULL >
	 */
	public function inserirprematricula(array $parametros){
		
		try {
		
			$this->getClient()->inserirprematricula()->webservicecodentidade($this->getWebservicecodentidade())->webservicechave($this->getWebservicechave())
			->parametros($parametros);

			$dados = $this->object_to_array($this->getClient()->post()->inserirprematricula);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
				
	}
	
	
}

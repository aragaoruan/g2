<?php

/**
 * Classe para conexão dos serviços de Disciplina no Actor 2/Gestor 1
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 */
class Actor_Disciplina extends Actor {
	
	public function __construct($webservicecodentidade, $webservicechave, $url = self::URL){
		
		try {
			
			if(!$url){
				throw new Zend_Exception('URL Obrigatória!');
			}
			
			$this->setWebservicechave($webservicechave);
			$this->setWebservicecodentidade($webservicecodentidade);
			$this->setServico($url.'services/disciplina.php');
			$this->setClient($this->getServico());
			
		} catch (Exception $e) {
			throw new Zend_Validate_Exception("Erro ao iniciar o Serviço: ".$e->getMessage());
		}
		
	}
	
	/**
	 * Retornar uma Área
	 * @param int $codmodulo
	 * @return array
	 */
	public function retornar($codmodulo){
		
		try {
		
			$this->getClient()->retornadisciplina()->webservicecodentidade($this->getWebservicecodentidade())->webservicechave($this->getWebservicechave())
				->codmodulo($codmodulo);

			$dados = $this->object_to_array($this->getClient()->get()->retornadisciplina);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
				
	}
	
	
	
	/**
	 * Lista os Módulos/Disciplinas
	 * @throws Exception
	 * @return boolean|Ambigous <NULL, array, string, multitype:NULL >
	 */
	public function listar(){
		
		try {
		
			$this->getClient()->listardisciplina()->webservicecodentidade($this->getWebservicecodentidade())->webservicechave($this->getWebservicechave());

			$dados = $this->object_to_array($this->getClient()->get()->listardisciplina);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
				
	}

	
	/**
	 * Insere um Modulo/disciplina
	 * @param array $dados
	 * @throws Exception
	 * @return boolean|Ambigous <NULL, array, string, multitype:NULL >
	 */
	public function inserir(array $dados){
		
		try {
		
			$this->getClient()->inserirdisciplina()->webservicecodentidade($this->getWebservicecodentidade())->webservicechave($this->getWebservicechave())->dados($dados);

			$dados = $this->object_to_array($this->getClient()->post()->inserirdisciplina);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
				
	}

	
	/**
	 * Atualiza um Modulo/disciplina
	 * @param array $dados
	 * @throws Exception
	 * @return boolean|Ambigous <NULL, array, string, multitype:NULL >
	 */
	public function atualizar(array $dados){
		
		try {
		
			$this->getClient()->atualizardisciplina()->webservicecodentidade($this->getWebservicecodentidade())->webservicechave($this->getWebservicechave())->dados($dados);

			$dados = $this->object_to_array($this->getClient()->post()->atualizardisciplina);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
				
	}
	
	
}

<?php

/**
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */
class actor_Vendedor extends Actor
{

    public function __construct($webservicecodentidade, $webservicechave, $url = self::URL)
    {
        try {
            if (!$url) {
                throw new Zend_Exception('URL Obrigatória!');
            }

            $this->setWebservicecodentidade($webservicecodentidade);
            $this->setWebservicechave($webservicechave);
            $this->setServico($url . 'services/vendedor.php');
            $this->setClient($this->getServico());

        } catch (Exception $e) {
            throw new Zend_Validate_Exception("Erro ao iniciar o Serviço: " . $e->getMessage());
        }
    }

    /**
     * @description Retornas vendedores do G1
     * @return array|bool|null
     * @throws Exception
     */
    public function retornarVendedores()
    {
        try {
            $this->getClient()
                ->retornavendedor()
                ->webservicecodentidade($this->getWebservicecodentidade())
                ->webservicechave($this->getWebservicechave());

            $dados = $this->object_to_array($this->getClient()->get()->retornavendedor);

            return $dados ? $dados : false;
        } catch (Exception $e) {
            throw $e;
        }
    }

}
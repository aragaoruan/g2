<?php

/**
 * Classe para conexão dos serviços de Pessoa no Actor 2/Gestor 1
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 */
class actor_Pessoa extends Actor {
	
	public function __construct($webservicecodentidade, $webservicechave, $url = self::URL){
		
		try {
			
			if(!$url){
				throw new Zend_Exception('URL Obrigatória!');
			}
			
			$this->setWebservicechave($webservicechave);
			$this->setWebservicecodentidade($webservicecodentidade);
			$this->setServico($url.'services/pessoa.php');
			$this->setClient($this->getServico());
			
		} catch (Exception $e) {
			throw new Zend_Validate_Exception("Erro ao iniciar o Serviço: ".$e->getMessage());
		}
		
	}
	
	
	/**
	 * Retorna uma Pessoa
	 * @param int $codpessoa
	 * @throws Exception
	 * @return boolean|Ambigous <NULL, array, string, multitype:NULL >
	 */
	public function retornar($codpessoa){
		
		try {
			
			$this->getClient()->retornapessoa()->webservicecodentidade($this->getWebservicecodentidade())->webservicechave($this->getWebservicechave())
				->codpessoa($codpessoa);

			$dados = $this->object_to_array($this->getClient()->get()->retornapessoa);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
				
	}
	
	/**
	 * Verifica se a pessoa pode Logar no Actor 2
	 * @param string $usuario
	 * @param string $senha
	 * @throws Exception
	 * @return boolean|Ambigous <NULL, array, string, multitype:NULL >
	 */
	public function login($usuario, $senha){
		
		try {
			
			$this->getClient()->retornapessoalogin()->webservicecodentidade($this->getWebservicecodentidade())->webservicechave($this->getWebservicechave())
				->usuario($usuario)->senha($senha);

			$dados = $this->object_to_array($this->getClient()->get()->retornapessoalogin);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
				
	}
	
	/**
	 * Recuperar a Senha
	 * @param string $numcpf
	 * @param string $desemail
	 * @throws Exception
	 * @return boolean|Ambigous <NULL, array, string, multitype:NULL >
	 */
	public function recuperarsenha($numcpf, $desemail){
		
		try {
			
			$this->getClient()->recuperarsenha()->webservicecodentidade($this->getWebservicecodentidade())->webservicechave($this->getWebservicechave())
				->numcpf($numcpf)->desemail($desemail);

			$dados = $this->object_to_array($this->getClient()->get()->recuperarsenha);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
				
	}
	
	
	/**
	 * Inserir Pessoa 
	 * @param array $dados
	 * @throws Zend_Exception
	 * @throws Exception
	 * @return boolean|Ambigous <NULL, array, string, multitype:NULL >
	 */
	public function inserir(array $dados){
		
		try {
		
			$this->getClient()->inserirpessoa()->webservicecodentidade($this->getWebservicecodentidade())->webservicechave($this->getWebservicechave())
			->dados($dados);
		
			$dados = $this->object_to_array($this->getClient()->post()->inserirpessoa);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
		
	}
	
	
	/**
	 * Atualiza um Pessoa
	 * @param array $dados
	 * @throws Exception
	 * @return boolean|Ambigous <NULL, array, string, multitype:NULL >
	 */
	public function atualizar(array $dados){
		
		try {

			$this->getClient()->atualizarpessoa()->webservicecodentidade($this->getWebservicecodentidade())->webservicechave($this->getWebservicechave())
			->dados($dados);

			$dados = $this->object_to_array($this->getClient()->post()->atualizarpessoa);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
		
	}



    /**
     * Retorna uma Pessoa se existir um usuário no G1
     * @param int $id_usuario
     * @throws Exception
     * @return boolean|Ambigous <NULL, array, string, multitype:NULL >
     */
    public function retornarPessoaSincronizacaoG1($id_usuario){
        try {

            $this->getClient()->retornaPessoaToG2()->webservicecodentidade($this->getWebservicecodentidade())->webservicechave($this->getWebservicechave())
                ->codpessoa($id_usuario);

            $dados = $this->object_to_array($this->getClient()->get()->retornaPessoaToG2);
            if(empty($dados)){
                return false;
            } else {
                return $dados;
            }

        } catch (Exception $e) {
            throw $e;
        }

    }

}

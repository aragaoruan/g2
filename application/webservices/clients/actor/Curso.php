<?php

/**
 * Classe para conexão dos serviços de Curso no Actor 2/Gestor 1
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 */
class Actor_Curso extends Actor {
	
	public function __construct($webservicecodentidade, $webservicechave, $url = self::URL){
		
		try {
			
			if(!$url){
				throw new Zend_Exception('URL Obrigatória!');
			}
			
			$this->setWebservicechave($webservicechave);
			$this->setWebservicecodentidade($webservicecodentidade);
			$this->setServico($url.'services/curso.php');
			$this->setClient($this->getServico());
			
		} catch (Exception $e) {
			throw new Zend_Validate_Exception("Erro ao iniciar o Serviço: ".$e->getMessage());
		}
		
	}
	
	/**
	 * Retornar um Curso
	 * @param int $codcurso
	 * @return array
	 */
	public function retornar($codcurso){
		
		try {
		
			$this->getClient()->retornacurso()
				->webservicecodentidade($this->getWebservicecodentidade())
				->webservicechave($this->getWebservicechave())
				->codcurso($codcurso);

			$dados = $this->object_to_array($this->getClient()->get()->retornacurso);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
				
	}
	
	/**
	 * Listar os cursos
	 * @param array $parametros
	 * @return array
	 */
	public function listar(array $parametros){
		
		try {
		
			if(!isset($parametros['codarea'])){
				$parametros['codarea'] = '';
			}
			
			$this->getClient()->listarcurso()
				->webservicecodentidade($this->getWebservicecodentidade())
				->webservicechave($this->getWebservicechave())
				->parametros($parametros);

			$dados = $this->object_to_array($this->getClient()->get()->listarcurso);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
				
	}
	
	/**
	 * Listar as formas de pagamento
	 * @param int $codcurso
	 * @return array
	 */
	public function listaformapagamento($codcurso){
		
		try {
			
			$this->getClient()->listaformapagamento()
				->webservicecodentidade($this->getWebservicecodentidade())
				->webservicechave($this->getWebservicechave())
				->codcurso($codcurso);

			$dados = $this->object_to_array($this->getClient()->get()->listaformapagamento);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
				
	}
	
	/**
	 * Listar o Programa do Curso
	 * @param int $codcurso
	 * @return array
	 */
	public function listaprogramacurso($codcurso){
		
		try {
			
			$this->getClient()->listaprogramacurso()->webservicecodentidade($this->getWebservicecodentidade())->webservicechave($this->getWebservicechave())
			->codcurso($codcurso);

			$dados = $this->object_to_array($this->getClient()->get()->listaprogramacurso);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
				
	}
	
	/**
	 * Listar os Cursos por Área de Interesse
	 * @param array $parametros
	 * @return array
	 */
	public function listacursoareainteresse(array $parametros){
		
		try {
			
			if(!isset($parametros['codareainteresse'])){ $parametros['codareainteresse'] = ''; }
			
			$this->getClient()->listacursoareainteresse()->webservicecodentidade($this->getWebservicecodentidade())->webservicechave($this->getWebservicechave())
			->parametros($parametros);
			$dados = $this->object_to_array($this->getClient()->get()->listacursoareainteresse);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
				
	}
	
	/**
	 * Retorna um Curso com sua Área de Interesse
	 * @param int $codcurso
	 * @return array
	 */
	public function retornacursoareainteresse($codcurso){
		
		try {
			
			$this->getClient()->retornacursoareainteresse()
				->webservicecodentidade($this->getWebservicecodentidade())
				->webservicechave($this->getWebservicechave())
				->codcurso($codcurso);

			$dados = $this->object_to_array($this->getClient()->get()->retornacursoareainteresse);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
				
	}

	/**
	 * Listar os Cursos por Área de Interesse Holding
	 * @param array $parametros
	 * @return array
	 */
	public function listacursoareainteresseholding(array $parametros){
		
		try {
			
			if(!isset($parametros['codareainteresse'])){ $parametros['codareainteresse'] = ''; }
			
			$this->getClient()->listacursoareainteresseholding()->webservicecodentidade($this->getWebservicecodentidade())->webservicechave($this->getWebservicechave())
			->parametros($parametros);
			
			$dados = $this->object_to_array($this->getClient()->get()->listacursoareainteresseholding);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
// 			Zend_Debug::dump($this->client->getHttpClient()->getLastRequest(),__CLASS__.'('.__LINE__.')');exit;
			throw $e;
		}
				
	}
	
	/**
	 * Retorna um Curso com sua Área de Interesse Holding
	 * @param int $codcurso
	 * @return array
	 */
	public function retornacursoareainteresseholding($codcurso){
		
		try {
			
			$this->getClient()->retornacursoareainteresseholding()
				->webservicecodentidade($this->getWebservicecodentidade())
				->webservicechave($this->getWebservicechave())
				->codcurso($codcurso);

			$dados = $this->object_to_array($this->getClient()->get()->retornacursoareainteresseholding);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
				
	}

}

<?php

/** 
 * @author Elcio
 * Classe para tratamento de cadastro de Papel no Sistema Actor II
 * 
 */
class PapelWebServices extends WsBO {
	
	/**
	 * Método construtor
	 * @param mixed $wsLink Link do Client Rest
	 */
	public function __construct(){
		parent::__construct();
		$this->setClientRest(Ead1_Ambiente::geral()->wsURL . 'services/papel.php');
	}
	
	/**
	 * Método que vincula o Papel ao Usuário
	 * @see WsBO::vincularPapel()
	 */
	
	public function vincularPapelProfessor(UsuarioPerfilEntidadeReferenciaTO $usuarioTO, SalaDeAulaProfessorTO $salaTO, $idSistema = SistemaTO::ACTOR){
		switch ($idSistema){
			case SistemaTO::ACTOR:
				
				$usuarioIntegracaoTO = new UsuarioIntegracaoTO();
				$usuarioIntegracaoTO->setId_usuario($usuarioTO->getId_usuario());
				$usuarioIntegracaoTO->setId_sistema(SistemaTO::ACTOR);
				$usuarioIntegracaoTO->fetch(false, true, true);
					if(empty($usuarioIntegracaoTO->st_codusuario)){
						
						$userTO = new UsuarioTO();
						$userTO->setId_usuario($usuarioTO->getId_usuario());
						$userTO->fetch(false,true,true);
						//Zend_Debug::dump($userTO);
						
						$this->setClientRest(Ead1_Ambiente::geral()->wsURL . 'services/pessoa.php');
						$userWS = $this->cadastrarPessoa($userTO);
						$usuarioIntegracaoTO->fetch(false, true, true);
					}
				
				$salaIntegracaoTO = new SalaDeAulaIntegracaoTO();
				$salaIntegracaoTO->setId_saladeaula($salaTO->getId_saladeaula());
				$salaIntegracaoTO->setId_sistema(SistemaTO::ACTOR);
				$salaIntegracaoTO->fetch(false, true, true);
					if(empty($salaIntegracaoTO->id_saladeaulaintegracao)){
						$slTO = new SalaDeAulaTO();
						$slTO->setId_saladeaula($salaIntegracaoTO->getId_saladeaula());
						$slTO->fetch(false, true, true);
						$this->setClientRest(Ead1_Ambiente::geral()->wsURL . 'services/disciplina.php');
						$this->cadastrarDisciplina($slTO);
						$salaIntegracaoTO->fetch(false, true, true);
					};
				
				
				if($usuarioIntegracaoTO->st_codusuario!=''){
					if($salaIntegracaoTO->id_saladeaulaintegracao!=''){
											
						$entidadeIntegracaoTO = $this->getEntidadeIntegracaoTO(SistemaTO::ACTOR);
			    		if($entidadeIntegracaoTO){
							$this->setClientRest(Ead1_Ambiente::geral()->wsURL . 'services/papel.php');
			    			$this->clientRest->inserirpapel()
									->webservicecodentidade($entidadeIntegracaoTO->getSt_codsistema())
									->webservicechave($entidadeIntegracaoTO->getSt_codchave())
									->dados(array(
											  'codpessoa'		=> $usuarioIntegracaoTO->st_codusuario 
											, 'codcurso'		=> $salaIntegracaoTO->st_codsistemacurso
											, 'codturma'		=> $salaIntegracaoTO->st_codsistemasala
											, 'codtipopess'		=> 1
											, 'codperfil'		=> 8
										));

							$dados = $this->clientRest->post()->inserirpapel;
							$papel = $this->object_to_array($dados);
							
							if($papel['retorno']=='erro'){
								throw new Zend_Exception($papel['mensagem']);
							}
							return array('retorno'=>$papel['retorno'], 'mensagem'=>$papel['mensagem'], 'papel'=>$papel['papel'][0]);
			    		}
					} else{
						throw new Zend_Exception('Sala de Aula ainda não integrada');
					}
				} else {
					throw new Zend_Exception('Usuário ainda não Integrado.');
				}

			break;
		}
		
	}
	
	/**
	 * Método que atualiza um Papel
	 * @param $papel['codpapel'] codpapel
	 * @param $papel['flastatus'] flastatus(opcional): Salva A(ativo) ou I(Inativo)
	 * @param $papel['flamaster'] flamater(opcional): Salva S(Papel Master) ou N (Papel secundário) Só pode existir 1 master por papel de Administrador.
	 */
	
	public function atualizaPapel($arrPapel, $idSistema = SistemaTO::ACTOR){
		switch ($idSistema){
			case SistemaTO::ACTOR:
				
				$entidadeIntegracaoTO = $this->getEntidadeIntegracaoTO(SistemaTO::ACTOR);
	    		if($entidadeIntegracaoTO){
					$this->clientRest->atualizarpapel()
							->webservicecodentidade($entidadeIntegracaoTO->getSt_codsistema())
							->webservicechave($entidadeIntegracaoTO->getSt_codchave())
							->dados(array(
									  'codpapel'		=> $arrPapel['codpapel'] 
									, 'flastatus'		=> $arrPapel['flastatus'] 
									, 'flamaster'		=> (isset($arrPapel['flamaster'])?$arrPapel['flamaster']:'') 
								));

					$dados = $this->clientRest->post()->atualizarpapel;
					$papel = $this->object_to_array($dados);
					
					if($papel['retorno']=='erro'){
						throw new Zend_Exception($papel['mensagem']);
					}
					
					return array('retorno'=>$papel['retorno'], 'mensagem'=>$papel['mensagem'], 'papel'=>$papel['papel'][0]);
	    		}



			break;
		}
		
	}
	
	/**
	 * Serviço que atualiza a matricula
	 * @param array $params
	 * DOC Envio:
	 * $params['codmatricula'] - matricula no actor
	 * $params['flastatus'] - (1)Ativo, (2)Não Efetivado/Inativo
	 * DOC Resposta:
	 * $response['retorno'] - sucesso/erro
	 * $response['mensagem'] - mensagem
	 * $response['status'] - se a requisição foi e voltou com sucesso
	 * $response['matricula'] - dados da matricula
	 * @return array $response
	 */
	public function atualizaMatricula(array $params){
		$entidadeIntegracaoTO = $this->getEntidadeIntegracaoTO(SistemaTO::ACTOR);
    	if(!$entidadeIntegracaoTO){
    		THROW new Zend_Validate_Exception("A Instituição não tem Integração com o Sistema ACTOR.");	
    	}
    	$this->setClientRest(Ead1_Ambiente::geral()->wsURL . 'services/matricula.php');
		$this->clientRest->atualizarMatricula()
				->webservicecodentidade($entidadeIntegracaoTO->getSt_codsistema())
				->webservicechave($entidadeIntegracaoTO->getSt_codchave())
				->dados($params);
    	$response = $this->clientRest->post()->atualizarMatricula;
		$response = $this->object_to_array($response);
		if($response['retorno']=='erro'){
			THROW new Zend_Exception($response['mensagem']);
		}
		return $response;
	}
	

}

?>
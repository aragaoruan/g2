<?php

/**
 * Classe para conexão dos serviços de Área no Actor 2/Gestor 1
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 */
class Actor_Area extends Actor {
	
	public function __construct($webservicecodentidade, $webservicechave, $url = self::URL){
		
		try {
			
			if(!$url){
				throw new Zend_Exception('URL Obrigatória!');
			}
			
			$this->setWebservicechave($webservicechave);
			$this->setWebservicecodentidade($webservicecodentidade);
			$this->setServico($url.'services/area.php');
			$this->setClient($this->getServico());
			
		} catch (Exception $e) {
			throw new Zend_Validate_Exception("Erro ao iniciar o Serviço: ".$e->getMessage());
		}
		
	}
	
	/**
	 * Retornar uma Área
	 * @param int $codarea
	 * @return array
	 */
	public function retornar($codarea){
		
		try {
		
			$this->getClient()->retornaarea()
				->webservicecodentidade($this->getWebservicecodentidade())
				->webservicechave($this->getWebservicechave())
				->codarea($codarea);

			$dados = $this->object_to_array($this->getClient()->get()->retornaarea);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
				
	}
	
	
	/**
	 * Lista as Áreas
	 * @throws Exception
	 * @return boolean|Ambigous <NULL, array, string, multitype:NULL >
	 */
	public function listar(){
		
		try {
		
			$this->getClient()->listararea()->webservicecodentidade($this->getWebservicecodentidade())->webservicechave($this->getWebservicechave());

			$dados = $this->object_to_array($this->getClient()->get()->listararea);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
				
	}
	
}

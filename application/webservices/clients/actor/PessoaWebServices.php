<?php


/**
 * Classe para conexão dos serviços de Pessoa no Actor 2/Gestor 1
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 */
class PessoaWebServices extends WsBO {
	
	protected $entidadeIntegracaoTO;
	protected $id_entidade;
	
	/**
	 * Método construtor
	 * @param mixed $wsLink Link do Client Rest
	 */
	public function __construct($id_entidade = null){
		parent::__construct();
		$this->id_entidade = $id_entidade;
		
	}
	
	/**
	 * Seta o Cliente REST
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @throws Exception
	 */
	public function setClient(){
		
		try {
			$this->entidadeIntegracaoTO = new EntidadeIntegracaoTO();
			$this->entidadeIntegracaoTO->setId_entidade($this->id_entidade);
			$this->entidadeIntegracaoTO->setId_sistema(SistemaTO::ACTOR);
			$this->entidadeIntegracaoTO->fetch(false, true, true);
			
			if(!$this->entidadeIntegracaoTO->getSt_codchave() || !$this->entidadeIntegracaoTO->getSt_caminho()){
				throw new Exception('Dados de acesso indefinidos');
			}
			
			$url = $this->entidadeIntegracaoTO->getSt_caminho().'services/pessoa.php';
			$this->setClientRest($url);
		} catch (Exception $e) {
			throw new Zend_Validate_Exception($e->getMessage());
		}

		
	}
	
	
	
	/**
	 * Busca o usuário no A2
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @param unknown_type $usuario
	 * @param unknown_type $senha
	 * @return Ead1_Mensageiro
	 */
	public function retornarDadosLoginUnico($usuario, $senha){
		
		try {
			
			
			$this->setClient();
			
			$this->clientRest->retornapessoalogin()
			->webservicecodentidade($this->entidadeIntegracaoTO->getSt_codsistema())
			->webservicechave($this->entidadeIntegracaoTO->getSt_codchave())
			->usuario($usuario)
			->senha($senha);
			
			$dados = $this->clientRest->post()->retornapessoalogin;
			
			$acesso = $this->object_to_array($dados);
			if(empty($acesso)){
				return new Ead1_Mensageiro('Nenhum registro encontrado!', Ead1_IMensageiro::AVISO);
			} else {
				return new Ead1_Mensageiro($acesso, Ead1_IMensageiro::SUCESSO);
			}
			
		} catch (Zend_Validate_Exception $e) {
			return new Ead1_Mensageiro('Não foi possível acessar o serviço: '.$e->getMessage(), Ead1_IMensageiro::AVISO);
		} catch (Exception $e) {
			return new Ead1_Mensageiro('Erro no serviçoo: '.$e->getMessage(), Ead1_IMensageiro::ERRO);
		}
		
	}
	
	
	
	/**
	 * Recuperar Senha Actor 2
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @param string $numcpf
	 * @param string $desemail
	 * @return Ead1_Mensageiro
	 */
	public function recuperarSenha($numcpf, $desemail){
		
		try {
			
			$this->setClient();
			
			$this->clientRest->recuperarsenha()
			->webservicecodentidade($this->entidadeIntegracaoTO->getSt_codsistema())
			->webservicechave($this->entidadeIntegracaoTO->getSt_codchave())
			->numcpf($numcpf)
			->desemail($desemail);
			$dados = $this->clientRest->post()->recuperarsenha;
			$acesso = $this->object_to_array($dados);
			if(empty($acesso)){
				return new Ead1_Mensageiro('Nenhum registro encontrado!', Ead1_IMensageiro::AVISO);
			} elseif($acesso['retorno']=='erro'){
				return new Ead1_Mensageiro($acesso['mensagem'], Ead1_IMensageiro::AVISO);
			} else {
				return new Ead1_Mensageiro($acesso, Ead1_IMensageiro::SUCESSO);
			}
			
		} catch (Zend_Validate_Exception $e) {
			return new Ead1_Mensageiro('Não foi possível acessar o serviço: '.$e->getMessage(), Ead1_IMensageiro::AVISO);
		} catch (Exception $e) {
			return new Ead1_Mensageiro('Erro no serviçoo!', Ead1_IMensageiro::ERRO);
		}
		
	}
	

}

?>
<?php

/**
 * Classe para conexão dos serviços de Papel no Actor 2/Gestor 1
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 */
class Actor_Papel extends Actor {
	
	public function __construct($webservicecodentidade, $webservicechave, $url = self::URL){
		
		try {
			
			if(!$url){
				throw new Zend_Exception('URL Obrigatória!');
			}
			
			$this->setWebservicechave($webservicechave);
			$this->setWebservicecodentidade($webservicecodentidade);
			$this->setServico($url.'services/papel.php');
			$this->setClient($this->getServico());
			
		} catch (Exception $e) {
			throw new Zend_Validate_Exception("Erro ao iniciar o Serviço: ".$e->getMessage());
		}
		
	}
	
	
	
	/**
	 * Listar a Papel
	 * @param int $filtro
	 * @return array
	 */
	public function listar(array $filtro = null){
		
		try {
			
			$this->getClient()->listarpapel()->webservicecodentidade($this->getWebservicecodentidade())->webservicechave($this->getWebservicechave())
				->filtro($filtro);

			$dados = $this->object_to_array($this->getClient()->get()->listarpapel);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
				
	}
	
	
	/**
	 * Inserir Papel 
	 * @param array $dados
	 * @throws Zend_Exception
	 * @throws Exception
	 * @return boolean|Ambigous <NULL, array, string, multitype:NULL >
	 */
	public function inserir(array $dados){
		
		try {
		
			$this->getClient()->inserirpapel()->webservicecodentidade($this->getWebservicecodentidade())->webservicechave($this->getWebservicechave())
			->dados($dados);
		
			$dados = $this->object_to_array($this->getClient()->post()->inserirpapel);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
		
	}
	
	
	/**
	 * Atualiza um Papel
	 * @param array $dados
	 * @throws Exception
	 * @return boolean|Ambigous <NULL, array, string, multitype:NULL >
	 */
	public function atualizar(array $dados){
		
		try {
		
			$this->getClient()->atualizarpapel()->webservicecodentidade($this->getWebservicecodentidade())->webservicechave($this->getWebservicechave())
			->dados($dados);
		
			$dados = $this->object_to_array($this->getClient()->post()->atualizarpapel);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
		
	}
}

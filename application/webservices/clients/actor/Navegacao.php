<?php

/**
 * Classe para conexão dos serviços de Navegação no Actor 2/Gestor 1
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 */
class Actor_Navegacao extends Actor {
	
	public function __construct($webservicecodentidade, $webservicechave, $url = self::URL){
		
		try {
			
			if(!$url){
				throw new Zend_Exception('URL Obrigatória!');
			}
			
			$this->setWebservicechave($webservicechave);
			$this->setWebservicecodentidade($webservicecodentidade);
			$this->setServico($url.'services/navegacao.php');
			$this->setClient($this->getServico());
			
		} catch (Exception $e) {
			throw new Zend_Validate_Exception("Erro ao iniciar o Serviço: ".$e->getMessage());
		}
		
	}
	
	/**
	 * Listar a Navegação
	 * @param int $filtro
	 * @return array
	 */
	public function listar(array $filtro = null){
		
		try {
			
			throw new Zend_Exception("Método com falha no Webservice");
			
			$this->getClient()->listarnavegacao()->webservicecodentidade($this->getWebservicecodentidade())->webservicechave($this->getWebservicechave())
				->filtro($filtro);

			$dados = $this->object_to_array($this->getClient()->get()->listarnavegacao);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
				
	}
	
	
	/**
	 * Inserir navegação 
	 * @param array $dados
	 * @throws Zend_Exception
	 * @throws Exception
	 * @return boolean|Ambigous <NULL, array, string, multitype:NULL >
	 */
	public function inserir(array $dados){
		
		try {
		
			throw new Zend_Exception("Método com falha no Webservice");
			
			$this->getClient()->inserirnavegacao()->webservicecodentidade($this->getWebservicecodentidade())->webservicechave($this->getWebservicechave())
			->dados($dados);
		
			$dados = $this->object_to_array($this->getClient()->get()->inserirnavegacao);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
		
	}
}

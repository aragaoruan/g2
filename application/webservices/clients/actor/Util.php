<?php

/**
 * Classe para conexão dos serviços utilitários no Actor 2/Gestor 1
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 */
class Actor_Util extends Actor {
	
	public function __construct($webservicecodentidade, $webservicechave, $url = self::URL){
		
		try {
			
			if(!$url){
				throw new Zend_Exception('URL Obrigatória!');
			}
			
			$this->setWebservicechave($webservicechave);
			$this->setWebservicecodentidade($webservicecodentidade);
			$this->setServico($url.'services/util.php');
			$this->setClient($this->getServico());
			
		} catch (Exception $e) {
			throw new Zend_Validate_Exception("Erro ao iniciar o Serviço: ".$e->getMessage());
		}
		
	}
	
	/**
	 * Listar as UFs
	 * @return array
	 */
	public function listaruf(){
		
		try {
		
			$this->getClient()->listaruf()
				->webservicecodentidade($this->getWebservicecodentidade())
				->webservicechave($this->getWebservicechave());

			$dados = $this->object_to_array($this->getClient()->get()->listaruf);
			if(empty($dados)){
				return false;
			} else {
				return $dados['ufs'];
			}
		
		} catch (Exception $e) {
			throw $e;
		}
				
	}
	
	
	/**
	 * Lista os Municípios
	 * @param string $coduf
	 * @throws Exception
	 * @return boolean|Ambigous <NULL, array, string, multitype:NULL >
	 */
	public function listarmunicipios($coduf){
		
		try {
		
			$this->getClient()->listarmunicipios()->webservicecodentidade($this->getWebservicecodentidade())->webservicechave($this->getWebservicechave())->coduf($coduf);

			$dados = $this->object_to_array($this->getClient()->get()->listarmunicipios);
			if(empty($dados)){
				return false;
			} else {
				return $dados['municipios'];
			}
		
		} catch (Exception $e) {
			throw $e;
		}
				
	}
	
}

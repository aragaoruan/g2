<?php

/**
 * Classe para conexão dos serviços de Sala no Actor 2/Gestor 1
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 */
class Actor_Sala extends Actor {
	
	public function __construct($webservicecodentidade, $webservicechave, $url = self::URL){
		
		try {
			
			if(!$url){
				throw new Zend_Exception('URL Obrigatória!');
			}
			
			$this->setWebservicechave($webservicechave);
			$this->setWebservicecodentidade($webservicecodentidade);
			$this->setServico($url.'services/saladeaula.php');
			$this->setClient($this->getServico());
			
		} catch (Exception $e) {
			throw new Zend_Validate_Exception("Erro ao iniciar o Serviço: ".$e->getMessage());
		}
		
	}
	
	
	/**
	 * Retorna uma Sala
	 * @param int $codcurso
	 * @param int $codturma
	 * @throws Exception
	 * @return boolean|Ambigous <NULL, array, string, multitype:NULL >
	 */
	public function retornar($codcurso, $codturma){
		
		try {
			
			$this->getClient()->retornasaladeaula()->webservicecodentidade($this->getWebservicecodentidade())->webservicechave($this->getWebservicechave())
				->codcurso($codcurso)->codturma($codturma);

			$dados = $this->object_to_array($this->getClient()->get()->retornasaladeaula);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
				
	}
	

	/**
	 * Lista as Salas de Aula de um Curso
	 * @param int $codcurso
	 * @throws Exception
	 * @return boolean|Ambigous <NULL, array, string, multitype:NULL >
	 */
	public function listar($codcurso){
		
		try {
			
			$this->getClient()->listarsaladeaula()->webservicecodentidade($this->getWebservicecodentidade())->webservicechave($this->getWebservicechave())
				->codcurso($codcurso);

			$dados = $this->object_to_array($this->getClient()->get()->listarsaladeaula);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
				
	}
	

	
	/**
	 * Inserir Sala 
	 * @param array $dados
	 * @throws Zend_Exception
	 * @throws Exception
	 * @return boolean|Ambigous <NULL, array, string, multitype:NULL >
	 */
	public function inserir(array $dados){
		
		try {
		
			$this->getClient()->inserirsaladeaula()->webservicecodentidade($this->getWebservicecodentidade())->webservicechave($this->getWebservicechave())
			->dados($dados);
		
			$dados = $this->object_to_array($this->getClient()->post()->inserirsaladeaula);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
		
	}
	
	
	/**
	 * Atualiza um Sala
	 * @param array $dados
	 * @throws Exception
	 * @return boolean|Ambigous <NULL, array, string, multitype:NULL >
	 */
	public function atualizar(array $dados){
		
		try {
		
			$this->getClient()->atualizarsaladeaula()->webservicecodentidade($this->getWebservicecodentidade())->webservicechave($this->getWebservicechave())
			->dados($dados);
		
			$dados = $this->object_to_array($this->getClient()->post()->atualizarsaladeaula);
			if(empty($dados)){
				return false;
			} else {
				return $dados;
			}
		
		} catch (Exception $e) {
			throw $e;
		}
		
	}
}

<?php

/**
 * Classe para conexão dos serviços de Afiliados no Actor 2/Gestor 1
 * @author Rafael Leite - rafael.leite@unyleya.com.br
 */
class actor_Afiliados extends Actor {

    public function __construct($webservicecodentidade,$webservicechave, $url = self::URL){

        try {

            if(!$url){
                throw new Zend_Exception('URL Obrigatória!');
            }

            $this->setWebservicecodentidade($webservicecodentidade);
            $this->setWebservicechave($webservicechave);
            $this->setServico($url.'services/afiliados.php');
            $this->setClient($this->getServico());

        } catch (Exception $e) {
            throw new Zend_Validate_Exception("Erro ao iniciar o Serviço: ".$e->getMessage());
        }

    }

    /**
     * Retornar os afiliados do G1
     * @return array
     */
    public function retornarAfiliados(){

        try {

            $this->getClient()->retornaafiliado()
                ->webservicecodentidade($this->getWebservicecodentidade())
                ->webservicechave($this->getWebservicechave());

            $dados = $this->object_to_array($this->getClient()->get()->retornaafiliado);

            if(empty($dados)){
                return false;
            } else {
                return $dados;
            }

        } catch (Exception $e) {
            throw $e;
        }

    }

}

<?php

class Leya_TO_LinkTO extends Leya_TO_LeyaTO {

	
	public $link;
	/**
	 * @return the $link
	 */
	public function getLink() {
		return $this->link;
	}

	/**
	 * @param field_type $link
	 */
	public function setLink($link) {
		$this->link = $link;
	}

	
}
<?php

class Leya_TO_IsbnPaperTO extends Leya_TO_LeyaTO {

	public $isbn_ebook;
	public $isbn_paper;
	/**
	 * @return the $isbn_ebook
	 */
	public function getIsbn_ebook() {
		return $this->isbn_ebook;
	}

	/**
	 * @return the $isbn_paper
	 */
	public function getIsbn_paper() {
		return $this->isbn_paper;
	}

	/**
	 * @param field_type $isbn_ebook
	 */
	public function setIsbn_ebook($isbn_ebook) {
		$this->isbn_ebook = $isbn_ebook;
	}

	/**
	 * @param field_type $isbn_paper
	 */
	public function setIsbn_paper($isbn_paper) {
		$this->isbn_paper = $isbn_paper;
	}

	
	

}
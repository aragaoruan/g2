<?php

class Leya_TO_CategoriesTO extends Leya_TO_LeyaTO {

	public $code;
	public $title;
	public $subcategories;
	
	
	/**
	 * @return the $code
	 */
	public function getCode() {
		return $this->code;
	}

	/**
	 * @return the $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @return the $subcategories
	 */
	public function getSubcategories() {
		return $this->subcategories;
	}

	/**
	 * @param field_type $code
	 */
	public function setCode($code) {
		$this->code = $code;
	}

	/**
	 * @param field_type $title
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * @param field_type $subcategories
	 */
	public function setSubcategories($subcategories) {
		
		if($subcategories){
			foreach($subcategories as $subcategory){
				$this->subcategories[] = new Leya_TO_CategoriesTO($subcategory);
			}
		}
		
	}

}
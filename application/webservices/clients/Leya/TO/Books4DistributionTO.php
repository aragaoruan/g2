<?php

class Leya_TO_Books4DistributionTO extends Leya_TO_LeyaTO {

	
	public $isbn;
	public $title;
	public $publisher;
	public $author;
	public $language;
	public $format;
	public $cover;
	public $synopsepublic;
	public $cat_code;
	public $cat;
	public $scat_code;
	public $scat;
	public $price;
	public $price_br;
	public $year;
	public $bisac;
	public $creation_date;
	public $countries_allowed;
	public $see_inside;
	public $multimedia;
	
	
	/**
	 * @return the $isbn
	 */
	public function getIsbn() {
		return $this->isbn;
	}

	/**
	 * @return the $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @return the $publisher
	 */
	public function getPublisher() {
		return $this->publisher;
	}

	/**
	 * @return the $author
	 */
	public function getAuthor() {
		return $this->author;
	}

	/**
	 * @return the $language
	 */
	public function getLanguage() {
		return $this->language;
	}

	/**
	 * @return the $format
	 */
	public function getFormat() {
		return $this->format;
	}

	/**
	 * @return the $cover
	 */
	public function getCover() {
		return $this->cover;
	}

	/**
	 * @return the $synopsepublic
	 */
	public function getSynopsepublic() {
		return $this->synopsepublic;
	}

	/**
	 * @return the $cat_code
	 */
	public function getCat_code() {
		return $this->cat_code;
	}

	/**
	 * @return the $cat
	 */
	public function getCat() {
		return $this->cat;
	}

	/**
	 * @return the $scat_code
	 */
	public function getScat_code() {
		return $this->scat_code;
	}

	/**
	 * @return the $scat
	 */
	public function getScat() {
		return $this->scat;
	}

	/**
	 * @return the $price
	 */
	public function getPrice() {
		return $this->price;
	}

	/**
	 * @return the $price_br
	 */
	public function getPrice_br() {
		return $this->price_br;
	}

	/**
	 * @return the $year
	 */
	public function getYear() {
		return $this->year;
	}

	/**
	 * @return the $bisac
	 */
	public function getBisac() {
		return $this->bisac;
	}

	/**
	 * @return the $creation_date
	 */
	public function getCreation_date() {
		return $this->creation_date;
	}

	/**
	 * @return the $countries_allowed
	 */
	public function getCountries_allowed() {
		return $this->countries_allowed;
	}

	/**
	 * @return the $see_inside
	 */
	public function getSee_inside() {
		return $this->see_inside;
	}

	/**
	 * @return the $multimedia
	 */
	public function getMultimedia() {
		return $this->multimedia;
	}

	/**
	 * @param field_type $isbn
	 */
	public function setIsbn($isbn) {
		$this->isbn = $isbn;
	}

	/**
	 * @param field_type $title
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * @param field_type $publisher
	 */
	public function setPublisher($publisher) {
		$this->publisher = $publisher;
	}

	/**
	 * @param field_type $author
	 */
	public function setAuthor($author) {
		$this->author = $author;
	}

	/**
	 * @param field_type $language
	 */
	public function setLanguage($language) {
		$this->language = $language;
	}

	/**
	 * @param field_type $format
	 */
	public function setFormat($format) {
		$this->format = $format;
	}

	/**
	 * @param field_type $cover
	 */
	public function setCover($cover) {
		$this->cover = $cover;
	}

	/**
	 * @param field_type $synopsepublic
	 */
	public function setSynopsepublic($synopsepublic) {
		$this->synopsepublic = $synopsepublic;
	}

	/**
	 * @param field_type $cat_code
	 */
	public function setCat_code($cat_code) {
		$this->cat_code = $cat_code;
	}

	/**
	 * @param field_type $cat
	 */
	public function setCat($cat) {
		$this->cat = $cat;
	}

	/**
	 * @param field_type $scat_code
	 */
	public function setScat_code($scat_code) {
		$this->scat_code = $scat_code;
	}

	/**
	 * @param field_type $scat
	 */
	public function setScat($scat) {
		$this->scat = $scat;
	}

	/**
	 * @param field_type $price
	 */
	public function setPrice($price) {
		$this->price = $price;
	}

	/**
	 * @param field_type $price_br
	 */
	public function setPrice_br($price_br) {
		$this->price_br = $price_br;
	}

	/**
	 * @param field_type $year
	 */
	public function setYear($year) {
		$this->year = $year;
	}

	/**
	 * @param field_type $bisac
	 */
	public function setBisac($bisac) {
		$this->bisac = $bisac;
	}

	/**
	 * @param field_type $creation_date
	 */
	public function setCreation_date($creation_date) {
		$this->creation_date = $creation_date;
	}

	/**
	 * @param field_type $countries_allowed
	 */
	public function setCountries_allowed($countries_allowed) {
		$this->countries_allowed = $countries_allowed;
	}

	/**
	 * @param field_type $see_inside
	 */
	public function setSee_inside($see_inside) {
		$this->see_inside = $see_inside;
	}

	/**
	 * @param field_type $multimedia
	 */
	public function setMultimedia($multimedia) {
		$this->multimedia = $multimedia;
	}

	
	
}

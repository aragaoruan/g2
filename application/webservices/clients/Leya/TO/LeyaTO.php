<?php

class Leya_TO_LeyaTO {

	
	/**
	 * Método que seta os atributos se existentes na classe instanciada e se for passado um array no construtor
	 * @see Ead1_TO_Dinamico
	 * @param array $dados
	 */
	public function __construct($dados = null){
		if( !is_null( $dados ) ){
			$arPropriedadeTO	= get_object_vars( $this );
			foreach( (array)$dados as $propriedade => $valor ){
				if( array_key_exists( $propriedade, $arPropriedadeTO ) !== false ){
					$metodo = 'set'.$propriedade;
					$this->$metodo($valor);
				}
			}	
		}
	} 
	
	/**
	 * Retorna as propriedades públicas do objeto
	 * @return $array
	 */
	public function toArray(){
		return get_object_vars($this);
	}
	
	
}

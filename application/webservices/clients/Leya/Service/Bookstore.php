<?php


/**
 * Classe para conexão ao WebService LeyaBookstore
 * @url https://docs.google.com/file/d/0Bw9xzpA5WHdxQzRCeUNIbVR2aXc/edit
 * @author Elcio Mauro Guimarães
 * @since 16/04/2013
 */
class Leya_Service_Bookstore extends WsBO  {
	
	
	const WSDL 			= "http://acs.leya.com/services/index.php?wsdl";
	
	
	/**
	 * getBooks4Distribution
	 * 
	 * É disponibilizado um método que permite receber a listagem de todos os livros
	 * disponíveis nesse momento no webservice para um determinado reseller. Este método,
	 * devolve sempre o catálogo completo e disponível.
	 * Recomendação da Leya: Este método deve ser evocado diariamente, preferencialmente
	 * durante a noite. Sempre que um livro for cancelado ou suspenso, o mesmo deixará de
	 * aparecer na listagem. Por esta razão, o reseller tem que garantir que os livros que não
	 * constarem da actualização diária, serão retirados do site de e-commerce.
	 * 
	 * @param string $st_codchave - Distribuidor 
	 * @throws Exception
	 * @return multitype:Leya_TO_Books4DistributionTO 
	 */
	public static function getBooks4Distribution($st_codchave){
		
		$client     = new SoapClient(self::WSDL, array("trace" => 1, "exception" => 0));
		try {
			
			$result 	= $client->__soapCall("getBooks4Distribution", array('ordersource'=>$st_codchave), NULL);
			$retorno 	= array();
			if($result){
				foreach($result as $stdClass){
					$retorno[] = new Leya_TO_Books4DistributionTO($stdClass);
				}
			}
			
			
// 			Zend_Debug::dump($client->__getLastRequest(),__CLASS__.'('.__LINE__.')');
// 			Zend_Debug::dump($client->__getLastRequestHeaders(),__CLASS__.'('.__LINE__.')');
// 			Zend_Debug::dump($client->__getLastResponse(),__CLASS__.'('.__LINE__.')');
			
			return $retorno;
			
		} catch (Exception $e) {
			throw $e;
		}
		
	}

	
	

	
	
	
	/**
	 * getCountries
	 * 
	 * É disponibilizado um método que permite receber a listagem de todos os países e
	 * respectivos códigos utilizados na listagem de permissões do catálogo.
	 * 
	 * @param string $st_codchave - Distribuidor 
	 * @throws Exception
	 * @return multitype:Leya_TO_Countries 
	 */
	public static function getCountries($st_codchave){
	
		$client     = new SoapClient(self::WSDL, array("trace" => 1, "exception" => 0));
		try {
	
			$result 	= $client->__soapCall("getCountries", array('ordersource'=>$st_codchave), NULL);
			$retorno 	= array();
			
			Zend_Debug::dump($result,__CLASS__.'('.__LINE__.')');exit;
			if($result){
				foreach($result as $stdClass){
					$retorno[] = new Leya_TO_CountriesTO($stdClass);
				}
			}
	
			return $retorno;
	
		} catch (Exception $e) {
			throw $e;
		}
	
	}
	

	
	
	
	/**
	 * getCategories
	 * É disponibilizado um método que permite receber a listagem de todas as
	 * categorias/subcategorias e respectivos códigos utilizados na listagem do catálogo.
	 * 
	 * @param string $st_codchave - Distribuidor 
	 * @throws Exception
	 * @return multitype:Leya_TO_Categories 
	 */
	public static function getCategories($st_codchave){
	
		$client     = new SoapClient(self::WSDL, array("trace" => 1, "exception" => 0));
		try {
	
			$result 	= $client->__soapCall("getCategories", array('ordersource'=>$st_codchave), NULL);
			$retorno 	= array();
			if($result){
				foreach($result as $stdClass){
					$retorno[] = new Leya_TO_CategoriesTO($stdClass);
				}
			}
	
			return $retorno;
	
		} catch (Exception $e) {
			throw $e;
		}
	
	}
	
	
	/**
	 * getIsbnPaper
	 * É disponibilizado um método que permite receber a listagem de todos os livros com a
	 * respectiva relação com o isbn em papel. Apenas ebooks com livros associados
	 * aparecem nesta listagem.
	 * 
	 * @param string $st_codchave - Distribuidor 
	 * @throws Exception
	 * @return multitype:Leya_TO_IsbnPaper 
	 */
	public static function getIsbnPaper($st_codchave){
	
		$client     = new SoapClient(self::WSDL, array("trace" => 1, "exception" => 0));
		try {
	
			$result 	= $client->__soapCall("getIsbnPaper", array('ordersource'=>$st_codchave), NULL);
			$retorno 	= array();
			if($result){
				foreach($result as $stdClass){
					$retorno[] = new Leya_TO_IsbnPaperTO($stdClass);
				}
			}
	
			return $retorno;
	
		} catch (Exception $e) {
			throw $e;
		}
	
	}
	
	/**
	 * getLink
	 * 
	 * 	É disponibilizado um método para solicitar o link de download de um livro específico. Este
	 * 	método deve ser invocado após o cliente final ter efectuado o pagamento.
	 * 	É obrigatório enviar os parâmetros isbn e ordersource. O parâmetro transid apesar de
	 * 	opcional deverá ser sempre utilizado. Este parâmetro é o id único que o distribuidor
	 * 	associa a esse pedido. Sempre que for enviado um pedido com o mesmo isbn,
	 * 	ordersource e transid, anteriormente solicitado, será devolvido o mesmo link já enviado.
	 * 
	 * 	Recomendação da Leya: Com o objectivo de ultrapassar eventuais problemas de
	 * 	comunicações entre o site de e-commerce e a plataforma da Leya aquando da evocação	deste método, 
	 *  recomendamos que o distribuidor implemente no seu site de e-commerce,
	 *  um sistema periódico (cron job) que evoque automaticamente este método até que o
	 *  retorno seja bem sucedido.
	 * 
	 * 
	 * @param string $isbn 
	 * @param string $transid
	 * @param string $st_codchave - Distribuidor 
	 * @throws Exception
	 * @return multitype:Leya_TO_Link 
	 */
	public static function getLink($isbn, $transid, $st_codchave){
	
		$client     = new SoapClient(self::WSDL, array("trace" => 1, "exception" => 0, 'cache_wsdl' => WSDL_CACHE_NONE));
		
		try {
	
			$result 	= $client->__soapCall("getLink", array(
					'isbn'=>$isbn,
					'ordersource'=>$st_codchave,
					'transid'=>$transid
					), NULL);
			$retorno 	= false;
			if($result){
				$retorno = new Leya_TO_LinkTO($result);
			}
	
			return $retorno;
	
		} catch (Exception $e) {
			throw new Zend_Exception("Leya Bookstore error: ".$e->getMessage());
		}
	
	}
	
}

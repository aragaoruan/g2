<?php
/**
 * Classe para tratamento de regras de negócio do WebService de Pessoa
 * @author Eder Lamar
 *
 */
class SaPessoaWebServices extends WsBO{
	
	/**
	 * Método que cadastra pessoa via web service nos sistemas integrados que precisam desse cadastro
	 * @see WsBO::cadastrarPessoa()
	 */
	public function cadastrarPessoa(UsuarioTO $usuarioTO, $idSistema = SistemaTO::ACTOR){
		switch ($idSistema){
			case SistemaTO::SISTEMA_AVALIACAO:
				$wsDAO = new WebServiceDAO();
				try{
					$interfaceIntegracao = $this->getEntidadeIntegracaoTO($idSistema);
					if(!$interfaceIntegracao){
						throw new Exception("Não há interface de integração para o sistema (" . SistemaTO::getSistema($idSistema) . "), ou você não está logado no sistema.");
					}
					
					if($usuarioTO->getId_usuario()){
						$pessoaTO = new PessoaTO();
						$pessoaTO->setId_entidade($pessoaTO->getSessao()->id_entidade);
						$pessoaTO->setId_usuario($usuarioTO->getId_usuario());

						$pessoaTO->fetch(null, true, true);
						$usuarioTO->fetch(null, true, true);
						
						$usuarioIntegracaoTO = new UsuarioIntegracaoTO();
						if($usuarioTO->getId_usuario() != null){
							$usuarioIntegracaoTO->setId_sistema($idSistema);
							$usuarioIntegracaoTO->setId_usuario($usuarioTO->getId_usuario());
							$usuarioIntegracaoTO->fetch(null, true, true);
							if($usuarioIntegracaoTO->getSt_codusuario()){
								return new Ead1_Mensageiro('Usuário já cadastrado!', Ead1_IMensageiro::SUCESSO);
							}
						}
					}
					$mensageiroWS = new Ead1_Mensageiro_Service($this->clientRest->salvarPessoa()
										->nome($usuarioTO->getSt_nomecompleto())
										->cpf(($usuarioTO->getSt_cpf()?$usuarioTO->getSt_cpf():null))
										->usuario($usuarioTO->getSt_login())
										->p($usuarioTO->getSt_senha())
										->sexo($pessoaTO->getSt_sexo())
										->pais($pessoaTO->getId_pais())
										->uf($pessoaTO->getSg_uf())
										->municipio($pessoaTO->getId_municipio())
										->idUsuario(null)
										->post()->salvarPessoa);
					if($mensageiroWS->getTipo() == Ead1_IMensageiro::SUCESSO){
						$wsDAO->beginTransaction();
						$this->cadastrarPessoaIntegracao($mensageiroWS->getCurrent(), $usuarioTO);
						$wsDAO->commit();
					}else{
						throw new Zend_Exception(implode(', ', $mensageiroWS->getMensagem()));
					}
					return new Ead1_Mensageiro('Cadastrado com Sucesso!');
				}catch(Zend_Exception $e){
					$wsDAO->rollBack();
					return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
				}catch(Exception $e){
					$wsDAO->rollBack();
					return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
				}
				break;
			default:
				parent::cadastrarPessoa($usuarioTO);
				break;
		}
	}
	
	/**
	 * Método que cadastra a referência dos dados de integração de pessoa
	 * @param mixed $dados
	 * @param UsuarioTO $dTO
	 */
	protected function cadastrarPessoaIntegracao($id, UsuarioTO $usuarioTO){
		$dao = new PessoaDAO();
		$dao->beginTransaction();
		try{
			$usuarioIntegracaoTO = new UsuarioIntegracaoTO();
			$usuarioIntegracaoTO->setId_usuario($usuarioTO->getId_usuario());
			$usuarioIntegracaoTO->setId_usuariocadastro($usuarioIntegracaoTO->getSessao()->id_usuario);
			$usuarioIntegracaoTO->setId_sistema(SistemaTO::SISTEMA_AVALIACAO);
			$usuarioIntegracaoTO->setSt_codusuario($id);
			$usuarioIntegracaoTO->setSt_loginintegrado($usuarioTO->getSt_login());
			$usuarioIntegracaoTO->setSt_senhaintegrada($usuarioTO->getSt_senha());

			$usuarioIntegracaoORM = new UsuarioIntegracaoORM();
			$usuarioIntegracaoORM->insert($usuarioIntegracaoTO->toArrayInsert());
			
			$dao->commit();
		}catch(Exception $e){
			$dao->rollBack();
			throw $e;
		}
	}
}
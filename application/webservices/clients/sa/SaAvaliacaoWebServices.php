<?php
/**
 * Classe para interaÃ§Ãµes de AvaliaÃ§Ã£o com o Sistema SA via WebService
 *
 */
class SaAvaliacaoWebServices extends WsBO{

	/**
	 * MÃ©todo que cadastra o bloco de respostas via WebServices
	 * @param string $guidAvaliacao
	 * @param array $arQuestoes
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarBlocoRespostas($guidAvaliacao, array $arQuestoes){
		try{
			$interfaceIntegracao = $this->getEntidadeIntegracaoTO(SistemaTO::SISTEMA_AVALIACAO);
			if(!$interfaceIntegracao){
				throw new Exception("NÃ£o hÃ¡ interface de integraÃ§Ã£o para o sistema (" . SistemaTO::getSistema(SistemaTO::SISTEMA_AVALIACAO) . "), ou vocÃª nÃ£o estÃ¡ logado no sistema.");
			}
			$mensageiroWS = new Ead1_Mensageiro_Service($this->clientRest->cadastrarBlocoRespostas()
								->guidAvaliacao($guidAvaliacao)
								->arQuestoes($arQuestoes)
								->post()->cadastrarBlocoRespostas);
			if($mensageiroWS->getTipo() == Ead1_IMensageiro::SUCESSO){
				return new Ead1_Mensageiro($mensageiroWS);
			}else{
				throw new Zend_Exception(implode(', ', $mensageiroWS->getMensagem()));
			}
		}catch(Zend_Exception $e){
			return new Ead1_Mensageiro('Erro no cadastro de bloco de respostas.', Ead1_IMensageiro::ERRO, $e->getMessage());
		}catch(Exception $e){
			return new Ead1_Mensageiro('Erro no cadastro de bloco de respostas.', Ead1_IMensageiro::ERRO, $e->getMessage());
		}
	}

	/**
	 * MÃ©todo que cadastra respostas via WebServices
	 * @param string $guidAvaliacao
	 * @param int $idQuestao
	 * @param int $idItemRespondido
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarResposta($guidAvaliacao, $idQuestao, $idItemRespondido){
		try{
			$interfaceIntegracao = $this->getEntidadeIntegracaoTO(SistemaTO::SISTEMA_AVALIACAO);
			if(!$interfaceIntegracao){
				throw new Exception("NÃ£o hÃ¡ interface de integraÃ§Ã£o para o sistema (" . SistemaTO::getSistema(SistemaTO::SISTEMA_AVALIACAO) . "), ou vocÃª nÃ£o estÃ¡ logado no sistema.");
			}
			$mensageiroWS = new Ead1_Mensageiro_Service($this->clientRest->cadastrarRespostaAvaliacao()
								->guidAvaliacao($guidAvaliacao)
								->idQuestao($idQuestao)
								->idItemRespondido($idItemRespondido)
								->post()->cadastrarRespostaAvaliacao);
			if($mensageiroWS->getTipo() == Ead1_IMensageiro::SUCESSO){
				return new Ead1_Mensageiro($mensageiroWS);
			}else{
				throw new Zend_Exception(implode(', ', $mensageiroWS->getMensagem()));
			}
		}catch(Zend_Exception $e){
			return new Ead1_Mensageiro('Erro ao cadastrar resposta.', Ead1_IMensageiro::ERRO, $e->getMessage());
		}catch(Exception $e){
			return new Ead1_Mensageiro('Erro ao cadastrar resposta.', Ead1_IMensageiro::ERRO, $e->getMessage());
		}
	}
	
	/**
	 * MÃ©todo que retorna as avaliaÃ§Ãµes, questÃµes, itens e respostas do Sistema de AvaliaÃ§Ã£o para correÃ§Ãµes no gestor 2.
	 * @param string $guidAvaliacao
	 * @return Ead1_Mensageiro
	 */
	public function retornarAvaliacaoQuestaoItensRespostas($guidAvaliacao){
		try{
			$interfaceIntegracao = $this->getEntidadeIntegracaoTO(SistemaTO::SISTEMA_AVALIACAO);
			if(!$interfaceIntegracao){
				throw new Exception("NÃ£o hÃ¡ interface de integraÃ§Ã£o para o sistema (" . SistemaTO::getSistema(SistemaTO::SISTEMA_AVALIACAO) . "), ou vocÃª nÃ£o estÃ¡ logado no sistema.");
			}
			$mensageiroWS = new Ead1_Mensageiro_Service($this->clientRest->retornarAvaliacaoQuestoesItens()
								->guidAvaliacao($guidAvaliacao)
								->post()->retornarAvaliacaoQuestoesItens);
			if($mensageiroWS->getTipo() == Ead1_IMensageiro::SUCESSO){
				$mensageiro = new Ead1_Mensageiro();
				return $mensageiro->setMensageiro($mensageiroWS)->setMensagem(Ead1_Mensageiro_Service::getArrayXML($mensageiroWS->getCurrent()));
			}else{
				throw new Zend_Exception(implode(', ', $mensageiroWS->getMensagem()));
			}
		}catch(Zend_Exception $e){
			return new Ead1_Mensageiro('Erro ao retornar itens de questï¿½o da avaliaï¿½ï¿½o', Ead1_IMensageiro::ERRO, $e->getMessage());
		}catch(Exception $e){
			return new Ead1_Mensageiro('Erro ao retornar itens de questï¿½o da avaliaï¿½ï¿½o', Ead1_IMensageiro::ERRO, $e->getMessage());
		}
	}
	
	/**
	 * MÃ©todo que chama a nota percentual de aprovaÃ§Ã£o atÃ© o momento!
	 * @param string $guidAvaliacao
	 * @throws Exception
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function retornarPercentualAprovacaoAvaliacao($guidAvaliacao){
		try{
			$interfaceIntegracao = $this->getEntidadeIntegracaoTO(SistemaTO::SISTEMA_AVALIACAO);
			if(!$interfaceIntegracao){
				throw new Exception("NÃ£o hÃ¡ interface de integraÃ§Ã£o para o sistema (" . SistemaTO::getSistema(SistemaTO::SISTEMA_AVALIACAO) . "), ou vocÃª nÃ£o estÃ¡ logado no sistema.");
			}
			$mensageiroWS = new Ead1_Mensageiro_Service($this->clientRest->retornarPercentualAprovacao()
								->guidAvaliacao($guidAvaliacao)
								->post()->retornarPercentualAprovacao);
			if($mensageiroWS->getTipo() == Ead1_IMensageiro::SUCESSO){
				return new Ead1_Mensageiro($mensageiroWS);
			}else{
				throw new Zend_Exception(implode(', ', $mensageiroWS->getMensagem()));
			}
		}catch(Zend_Exception $e){
			return new Ead1_Mensageiro('Erro ao retornar o percentual de aprovaï¿½ï¿½o.', Ead1_IMensageiro::ERRO, $e->getMessage());
		}catch(Exception $e){
			return new Ead1_Mensageiro('Erro ao retornar o percentual de aprovaï¿½ï¿½o.', Ead1_IMensageiro::ERRO, $e->getMessage());
		}
	}
	
	/**
	 * MÃ©todo que retorna o Modelo de Avaliacao
	 * @param int $idSistema
	 * @return Ead1_Mensageiro 
	 */
	public function retornarModeloAvaliacao($codTipoAvaliacao = null,$ativo = null,$nome = null,$pagina = 1,$limit = 9000000,$idSistema = SistemaTO::SISTEMA_AVALIACAO){
		try{
			$interfaceIntegracao = $this->getEntidadeIntegracaoTO($idSistema);
			//var_dump($mensageiro);
			if(!$interfaceIntegracao){
				THROW new Zend_Validate_Exception("Não há interface de integração para o sistema (" . SistemaTO::getSistema($idSistema) . ").");
			}
			$call = $this->_callRetornaModeloAvaliacaoSA($interfaceIntegracao->getSt_codchave(),$codTipoAvaliacao,$ativo,$nome,$pagina,$limit);
			die();
                        $mensageiro = new Ead1_Mensageiro_Service($call);
                        if($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO){
				$arrResposta = $this->_SimpleXMLElementToArray($mensageiro->mensagem[0],false,true);
				if(empty($arrResposta)){
					THROW new Zend_Validate_Exception('Nenhum Registro Encontrado!');
				}else{
					return new Ead1_Mensageiro($this->encapsulaModeloAvaliacaoRetorno($arrResposta),Ead1_IMensageiro::SUCESSO);
				}
			}else{
				THROW new Zend_Exception($mensageiro->mensagem);
			}
		}catch (Zend_Exception $e){
                        return new Ead1_Mensageiro("Erro ao Retornar Modelos de Avaliação!",Ead1_IMensageiro::ERRO,$e->getMessage());
		}catch (Zend_Validate_Exception $e){
			return new Ead1_Mensageiro($e->getMessage(),Ead1_IMensageiro::AVISO);
		}
	}
	
	/**
	 * MÃ©todo que encapsula o retorno do Modelo de Avaliacao
	 * @param array $arrResponse
	 * @return array $retorno;
	 */
	private function encapsulaModeloAvaliacaoRetorno(array $arrResponse){
		$retorno = array();
		foreach ($arrResponse as $key => $response){
			$modeloAvaliacaoOBJ = null;
			$modeloAvaliacaoOBJ->id_modeloavaliacao = $response['id'];
			$modeloAvaliacaoOBJ->id_tipoavaliacao = $response['tipoAvaliacao']['id'];
			$modeloAvaliacaoOBJ->st_tipoavaliacao = $response['tipoAvaliacao']['descricao'];
			$modeloAvaliacaoOBJ->st_modeloavaliacao = $response['nome'];
			$modeloAvaliacaoOBJ->st_descricao = (isset($response['descricao']) ? $response['descricao'] : null);
			$modeloAvaliacaoOBJ->nu_quantidade = (isset($response['quantidade']) ? $response['quantidade'] : 0);
			$modeloAvaliacaoOBJ->nu_tempominutos = $response['tempoMinutos'];
			$modeloAvaliacaoOBJ->nu_notaaprovacao = $response['notaAprovacao'];
			$modeloAvaliacaoOBJ->bl_ativo = (isset($response['ativo']) ? $response['ativo'] : null);
			$retorno[] = $modeloAvaliacaoOBJ;
		}
		return $retorno;
	}
	
	/**
	 *  Chamada de ServiÃ§o de SA que retorna os Modelos de AvaliaÃ§Ã£o
	 *  A resposta retorna os Modelos de AvaliaÃ§Ã£o e um Paginator.
	 *  
	 *  [Response Structs]
	 *  
	 *  obj->status
	 *  obj->response->array->modeloavaliacao->id
	 *  obj->response->array->modeloavaliacao->nome
	 *  obj->response->array->modeloavaliacao->descricao
	 *  obj->response->array->modeloavaliacao->ativo
	 *  obj->response->array->modeloavaliacao->dataCadastro
	 *  obj->response->array->modeloavaliacao->tipoAvaliacao->id
	 *  obj->response->array->modeloavaliacao->tipoAvaliacao->descricao
	 *  obj->response->array->modeloavaliacao->pessoaJuridica->id
	 *  obj->response->array->modeloavaliacao->pessoaJuridica->razaoSocial
	 *  obj->response->array->modeloavaliacao->pessoaCadastro->id
	 *  
	 *  @param Mixed $codPessoaJuridica - id da relaÃ§Ã£o da entidade
	 *  @param int $codTipoAvaliacao - id do tipo de avaliacao
	 *  @param Boolean $ativo - Retorna apenas os Modelos de AvaliaÃ§Ã£o Ativos/Inativos - null para todos
	 *  @param String $nome - nome do modelo de avaliacao
	 *  @param int $pagina - NÃºmero da pagina
	 *  @param int $limit - Limite por pagina
	 * @return SimpleXMLElement
	 */
	private function _callRetornaModeloAvaliacaoSA($codPessoaJuridica,$codTipoAvaliacao = null,$ativo = null,$nome = null,$pagina = 1,$limit = 9000000){
		$response = $this->clientRest->retornarModeloAvaliacao()
								->codPessoaJuridica($codPessoaJuridica)
								->codTipoAvaliacao($codTipoAvaliacao)
								->ativo($ativo)
								->nome($nome)
								->pagina($pagina)
								->limit($limit)
								->post()->retornarModeloAvaliacao;
		return $response;
	}
	
	/**
	 * MÃ©todo que cadastra as avaliaÃ§Ãµes para um determinado aluno no SA
	 * @param AvaliacaoAgendamentoTO $aaTO
	 */
	public function cadastrarAvaliacaoAluno(AvaliacaoAgendamentoTO $aaTO){
		$wsDAO = new WebServiceDAO();
		try{
			$interfaceIntegracao = $this->getEntidadeIntegracaoTO(SistemaTO::SISTEMA_AVALIACAO);
			if(!$interfaceIntegracao){
				throw new Exception("NÃ£o hÃ¡ interface de integraÃ§Ã£o para o sistema (" . SistemaTO::getSistema(SistemaTO::SISTEMA_AVALIACAO) . "), ou vocÃª nÃ£o estÃ¡ logado no sistema.");
			}
			$matriculaTO = new MatriculaTO();
			$matriculaTO->setId_matricula($aaTO->getId_matricula());
			$matriculaTO->fetch(true, true, true);
			
			$usuarioIntegracaoTO = new UsuarioIntegracaoTO();
			$usuarioIntegracaoTO->setId_sistema(SistemaTO::SISTEMA_AVALIACAO);
			$usuarioIntegracaoTO->setId_usuario($matriculaTO->getId_usuario());
			$usuarioIntegracaoTO->fetch(null, true, true);
			
			$usuarioTO = new UsuarioTO();
			$usuarioTO->setId_usuario($matriculaTO->getId_usuario());
			$usuarioTO->fetch(true, true, true);
			if(!$usuarioIntegracaoTO->getId_usuariointegracao()){
				
				$wsBO = new SaPessoaWebServices(Ead1_BO::geradorLinkWebService(SistemaTO::SISTEMA_AVALIACAO));
				$wsBO->cadastrarPessoa($usuarioTO, SistemaTO::SISTEMA_AVALIACAO);
				$usuarioIntegracaoTO->fetch(null, true, true);
				if(!$usuarioIntegracaoTO->getId_usuariointegracao()){
					return new Ead1_Mensageiro("UsuÃ¡rio nÃ£o integrado.", Ead1_IMensageiro::ERRO);
				}
			}
			
			$avaliacaoIntegracaoTO = new AvaliacaoIntegracaoTO();
			$avaliacaoIntegracaoTO->setId_avaliacao($aaTO->getId_avaliacao());
			$avaliacaoIntegracaoTO->fetch(null, true, true);
		
			if(!$avaliacaoIntegracaoTO->getId_avaliacaointegracao()){
				return new Ead1_Mensageiro("AvaliaÃ§Ã£o nÃ£o integrada.", Ead1_IMensageiro::ERRO);
			}
			
			$dtAgendamento = new Zend_Date($aaTO->getDt_agendamento(), Zend_Date::ISO_8601);
			$response = $this->clientRest->gerarAvaliacao()
								->idModeloAvaliacao($avaliacaoIntegracaoTO->getSt_codsistema())
								->idPessoa($usuarioIntegracaoTO->getSt_codusuario())
								->dtPrevisao($dtAgendamento->toString("yyyy-MM-dd"))
								->post()->gerarAvaliacao;
			$response = get_object_vars($response);
			if(array_key_exists('status', $response)){
				if($response['status'] == 'success'){
					$wsDAO->beginTransaction();
					$guidAvaliacao = $response['guidavaliacao'];
					$this->cadastrarAvaliacaoAgendamentoIntegracao($guidAvaliacao, $usuarioTO, $aaTO);
					$wsDAO->commit();
				}else{
					throw new Zend_Exception($response[0]);
				}
			}else{
				throw new Zend_Exception($response[0]);
			}
			
			return new Ead1_Mensageiro('Cadastrado com Sucesso! ' . $guidAvaliacao);
		}catch(Zend_Exception $e){
			$wsDAO->rollBack();
			return new Ead1_Mensageiro('Erro no cadastro de avaliaÃ§Ã£o.', Ead1_IMensageiro::ERRO, $e->getMessage());
		}catch(Exception $e){
			$wsDAO->rollBack();
			return new Ead1_Mensageiro('Erro no cadastro de avaliaÃ§Ã£o.', Ead1_IMensageiro::ERRO, $e->getMessage());
		}
	}
	
	/**
	 * MÃ©todo que cadastra a integraÃ§Ã£o do Agendamento
	 * @param mixed $idAvaliacaoIntegrada referÃªncia do agendamento no SA
	 * @param UsuarioTO $usuarioTO to com os dados do aluno
	 * @param AvaliacaoAgendamentoTO $avaliacaoAgendamentoTO To com os dados da avaliaÃ§Ã£o
	 */
	private function cadastrarAvaliacaoAgendamentoIntegracao($idAvaliacaoIntegrada, UsuarioTO $usuarioTO, AvaliacaoAgendamentoTO $avaliacaoAgendamentoTO){
		$wsDAO = new WebServiceDAO();
		$wsDAO->beginTransaction();
		try{
			$avaliacaoAgendamentoIntegracaoTO = new AvalAgendaIntegracaoTO();
			$avaliacaoAgendamentoIntegracaoTO->setSt_codsistema($idAvaliacaoIntegrada);
			$avaliacaoAgendamentoIntegracaoTO->setId_sistema(SistemaTO::SISTEMA_AVALIACAO);
			$avaliacaoAgendamentoIntegracaoTO->setId_avaliacaoagendamento($avaliacaoAgendamentoTO->getId_avaliacaoagendamento());
			$avaliacaoAgendamentoIntegracaoTO->setId_usuariocadastro($usuarioTO->getSessao()->id_usuario);
			
			$avaliacaoAgendamentoIntegracaoORM = new AvalAgendaIntegracaoORM();
			$avaliacaoAgendamentoIntegracaoORM->insert($avaliacaoAgendamentoIntegracaoTO->toArrayInsert());
			$wsDAO->commit();
			return new Ead1_Mensageiro("AvaliaÃ§Ã£o IntegraÃ§Ã£o cadastrada com sucesso.");
		}catch(Zend_Exception $e){
			$wsDAO->rollBack();
			throw $e;
			return new Ead1_Mensageiro("Erro ao cadastrar a AvaliaÃ§Ã£o IntegraÃ§Ã£o. " . $e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
		}	
	}
	
	/**
	 * MÃ©todo que retorna as avaliaÃ§Ãµes para impressÃ£o
	 * @param array $guidAvaliacoes
	 * @return new Ead1_Mensageiro_Service
	 */
	public function impressaoAvaliacoes($guidAvaliacoes){
		return new Ead1_Mensageiro_Service($this->clientRest->impressaoAvaliacoes()
				->guidAvaliacoes($guidAvaliacoes)
				->codPessoaJuridica($this->getEntidadeIntegracaoTO(SistemaTO::SISTEMA_AVALIACAO)->getSt_codchave())
		->post()->impressaoAvaliacoes);
	}
}
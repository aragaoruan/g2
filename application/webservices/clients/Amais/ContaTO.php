<?php

/**
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 */
class Amais_ContaTO extends Ead1_TO_Dinamico {
	
	
	
	const PLANO_MENSAL 		= 'M';
	const PLANO_TRIMESTRAL 	= 'T';
	const PLANO_SEMESTRAL 	= 'S';
	const PLANO_ANUAL 		= 'A';
	
	/**
	 * p tipo de plano habilitado para o aluno, de acordo com o estabelecido pela diretoria comercial 
	 * para a comercialização do produto (exemplo: M, T, S e A referentes a mensal, trimestral, semestral e anual)
	 * @var string
	 */
	public $p;
	
	/**
	 * em e-mail do aluno
	 * @var string
	 */
	public $em;
	
	/**
	 * n nome do aluno
	 * @var string
	 */
	public $n;
	
	/**
	 * c cidade do aluno (opcional)
	 * @var string
	 */
	public $c;

	/**
	 * s senha do aluno
	 * @var string
	 */
	public $s;
	
	/**
	 * uf unidade federativa do aluno (opcional)
	 * @var string
	 */
	public $uf;
	
	/**
	 * iv início da vigência do plano do aluno, formato DD/MM/YYYY
	 * @var string
	 */
	public $iv;
	
	/**
	 * fv fim da vigência do plano do aluno, formato DD/MM/YYYY
	 * @var string
	 */
	public $fv;
	
	/**
	 * cpf CPF do aluno, com ou sem pontos e hífen
	 * @var cpf
	 */
	public $cpf;
	
	/**
	 * j_username usuário que identifica o sistema externo
	 * @var unknown_type
	 */
	public $j_username;
	
	/**
	 * j_password senha do usuário do sistema externo
	 * @var unknown_type
	 */
	public $j_password;
	/**
	 * @return the $s
	 */
	public function getS() {
		return $this->s;
	}

	/**
	 * @param string $s
	 */
	public function setS($s) {
		$this->s = $s;
	}

	/**
	 * @return the $p
	 */
	public function getP() {
		return $this->p;
	}

	/**
	 * @return the $em
	 */
	public function getEm() {
		return $this->em;
	}

	/**
	 * @return the $n
	 */
	public function getN() {
		return $this->n;
	}

	/**
	 * @return the $c
	 */
	public function getC() {
		return $this->c;
	}

	/**
	 * @return the $uf
	 */
	public function getUf() {
		return $this->uf;
	}

	/**
	 * @return the $iv
	 */
	public function getIv() {
		return $this->iv;
	}

	/**
	 * @return the $fv
	 */
	public function getFv() {
		return $this->fv;
	}

	/**
	 * @return the $cpf
	 */
	public function getCpf() {
		return $this->cpf;
	}

	/**
	 * @return the $j_username
	 */
	public function getJ_username() {
		return $this->j_username;
	}

	/**
	 * @return the $j_password
	 */
	public function getJ_password() {
		return $this->j_password;
	}

	/**
	 * @param string $p
	 */
	public function setP($p) {
		$this->p = $p;
	}

	/**
	 * @param string $em
	 */
	public function setEm($em) {
		$this->em = $em;
	}

	/**
	 * @param string $n
	 */
	public function setN($n) {
		$this->n = $n;
	}

	/**
	 * @param string $c
	 */
	public function setC($c) {
		$this->c = $c;
	}

	/**
	 * @param string $uf
	 */
	public function setUf($uf) {
		$this->uf = $uf;
	}

	/**
	 * @param string $iv
	 */
	public function setIv($iv) {
		$this->iv = $iv;
	}

	/**
	 * @param string $fv
	 */
	public function setFv($fv) {
		$this->fv = $fv;
	}

	/**
	 * @param cpf $cpf
	 */
	public function setCpf($cpf) {
		$this->cpf = $cpf;
	}

	/**
	 * @param unknown_type $j_username
	 */
	public function setJ_username($j_username) {
		$this->j_username = $j_username;
	}

	/**
	 * @param unknown_type $j_password
	 */
	public function setJ_password($j_password) {
		$this->j_password = $j_password;
	}

	
	
}
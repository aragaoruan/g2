<?php

/**
 * Classe para conexão dos serviços de cadastro no A+
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 */
class Amais_Cadastro extends WsBO {
	
	protected $id_entidade;
	
	const ERRO 		= 0;
	const SUCESSO 	= 1;
	const AVISO 	= 2;
	
	/**
	 * Método construtor
	 * @param mixed $wsLink Link do Client Rest
	 */
	public function __construct($id_entidade){
		parent::__construct();
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * Insere ou Atualiza um usuário no A+
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @param Amais_contaTO $to
	 * @return Ead1_Mensageiro
	 */
	public function cadastrar(Amais_contaTO $to){
		
		try {
			
			$eTO = new EntidadeIntegracaoTO();
			$eTO->setId_entidade($this->id_entidade);
			$eTO->setId_sistema(SistemaTO::AMAIS);
			$eTO->fetch(false, true, true);
			if(!$eTO->getSt_codsistema() || !$eTO->getSt_codchave() || !$eTO->getSt_caminho()){
				throw new Exception('Dados de acesso indefinidos');
			}
			
			$to->setJ_password($eTO->getSt_codchave());
			$to->setJ_username($eTO->getSt_codsistema());
			
			$dados = $to->toArray();
			if($dados['iv'] instanceof Zend_Date){
				$dados['iv'] = $dados['iv']->toString('dd/MM/yyyy');
			} 
			
			if($dados['fv'] instanceof Zend_Date){
				$dados['fv'] = $dados['fv']->toString('dd/MM/yyyy');
			}
            
			$url = $eTO->getSt_caminho().'/aluno/habilitacao-externa.jsp?'.http_build_query($dados);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url );
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$retorno = curl_exec($ch);
			$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			
			if($retorno === false) {
				$err = curl_error($ch);
				curl_close($ch);
				throw new Zend_Exception('Erro na chamada do Serviço: '.$err.'.');
			} else {
				curl_close($ch);
				$retornows =  $this->tratarRetorno($retorno, array('httpcode'=>$httpcode, 'url'=>$eTO->getSt_caminho().'/aluno/habilitacao-externa.jsp'));
			}
			return $retornows;
		} catch (Zend_Validate_Exception $e) {
			return $this->tratarRetorno('ERRO: Não foi possível acessar o serviço: '.$e->getMessage());
		} catch (Exception $e) {
			return $this->tratarRetorno('ERRO: Problema no serviço: '.$e->getMessage());
		}
		
	}
	
	
	/**
	 * Trata o retorno do WS
	 * @param unknown_type $retorno
	 * @return multitype:string 
	 */
	private function tratarRetorno($retorno, array $response){
		
            $tipo = self::ERRO;
            $msg = "O serviço não retornou nada!";
//             print_r($retorno);exit;
            if($retorno){
                //var_dump($retorno);
                $tipo = self::SUCESSO;
                if($retorno=="OK") {
                    $msg = "Usuário cadastrado com sucesso!";
                } else if(substr($retorno, 0, 5)=='ERRO:') {
                    if(substr($retorno, 0, 39)=='ERRO: Ja existe outro usuario com login'){
                        $msg = "Usuário já cadastrado no A+!";
                    } else {
                        $tipo = self::AVISO;
                        $msg = trim(substr($retorno, 5));
                    }
                } else {
                    $tipo = self::ERRO;
                    //echo 'ERRO = -'.substr($retorno, strrpos($retorno, '<title>')+7, (strrpos($retorno, '</title>')-(strrpos($retorno, '<title>')+7) ))."-";
                    $erro_msg = substr($retorno, strrpos($retorno, '<title>')+7, (strrpos($retorno, '</title>')-(strrpos($retorno, '<title>')+7) ));
                    if($erro_msg=='ERRO: A URL requisitada não pôde ser recuperada'){
                        $url_str = substr($retorno, strrpos($retorno, '<pre>')+9, (strpos($retorno, 'HTTP/1.1')-(strrpos($retorno, '<pre>')+9) ));
                        $msg = "A URL \"$url_str\" não pode ser recuperada!";
                    } else {
                    	
                    	if(isset($response['httpcode']) && $response['httpcode']=='404'){
	                        $msg = "A Url do WS da Amais não foi encontrada: ".$response['url'];
                    	} else {
	                        $msg = "Não foi possível identificar a mensagem do servidor!";
                    	}
                    	
                    }
                }
            }
            //print_r($tipo);exit;
            return array('tipo'=>$tipo, 'Mensagem'=>$msg, 'retorno'=>$retorno);
	} 
}
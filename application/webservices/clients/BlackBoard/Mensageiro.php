<?php
/**
 * BlackBoard Web Services e Snapshot Flat File para PHP
 *
 * Classes para Consumo dos serviços do BlackBoard
 * Desenvolvido com o estudo do "BbPHP: Blackboard Web Services Library for PHP - Copyright (C) 2011 by St. Edward's University (www.stedwards.edu) - @author johns"
 *
 * As classes e métodos foram refeitos para se ajustar a necessidade de consumir os dois tipos de serviços
 *  1 - Web Services
 *  2 - Snapshot Flat File
 *
 *
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 *
 */
class BlackBoard_Mensageiro {
	
	const SUCESSO 	= 1;
	const AVISO 	= 2;
	const ERRO 		= 0;
	
	private $_tipo;
	private $_mensagem;
	private $_codigo;
	
	
	public function __construct($_mensagem = "Sucesso", $_tipo = self::SUCESSO, $_codigo = null){
		$this->_mensagem 	= $_mensagem;
		$this->_tipo 		= $_tipo;
		$this->_codigo 		= $_codigo;
	}
	
	public function _toArray(){
		return array('mensagem'=>$this->_mensagem, 'tipo'=>$this->_tipo, 'codigo'=>$this->_codigo);
	} 
	
	/**
	 * @return the $_tipo
	 */
	public function getTipo() {
		return $this->_tipo;
	}

	/**
	 * @return the $_mensagem
	 */
	public function getMensagem($chave = null) {
		if($chave)
			return $this->_mensagem[$chave];
		
		return $this->_mensagem;
	}

	/**
	 * @return the $_codigo
	 */
	public function getCodigo() {
		return $this->_codigo;
	}

	/**
	 * @param field_type $_tipo
	 */
	public function setTipo($_tipo) {
		$this->_tipo = $_tipo;
	}

	/**
	 * @param field_type $_mensagem
	 */
	public function setMensagem($_mensagem) {
		$this->_mensagem = $_mensagem;
	}

	/**
	 * @param field_type $_codigo
	 */
	public function setCodigo($_codigo) {
		$this->_codigo = $_codigo;
	}

	
}

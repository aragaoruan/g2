<?php

/**
 * BlackBoard Web Services e Snapshot Flat File para PHP
 * 
 * Classes para Consumo dos serviços do BlackBoard
 * Desenvolvido com o estudo do "BbPHP: Blackboard Web Services Library for PHP - Copyright (C) 2011 by St. Edward's University (www.stedwards.edu) - @author johns"
 * 
 * As classes e métodos foram refeitos para se ajustar a necessidade de consumir os dois tipos de serviços
 *  1 - Web Services
 *  2 - Snapshot Flat File
 *  
 * 
 * http://help.blackboard.com/en-us/Learn/9.1_SP_12_and_SP_13/Administrator/140_System_Integration/010_Student_Information_System_%28SIS%29/030_SIS_Integration_Types/010_Snapshot_Flat_File
 * http://help.blackboard.com/en-us/Learn/9.1_SP_12_and_SP_13/Administrator/140_System_Integration/010_Student_Information_System_%28SIS%29/030_SIS_Integration_Types/010_Snapshot_Flat_File/060_Snapshot_Flat_File_Examples
 *
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * 
 */
class BlackBoard_Service_Organization extends BlackBoard_Service {
	
	public $endpoint_url = "/webapps/bb-data-integration-flatfile-BBLEARN/endpoint/organization/store";
	
	/**
	 * @return the $endpoint_url
	 */
	public function getEndpoint_url() {
		return $this->endpoint_url;
	}
	
	public $service = 'Organization';
	
	/**
	 * @return the $service
	 */
	public function getService() {
		return $this->service;
	}

	/**
	 *
	 * Cria um ou mais Cursos no BlackBoard.
	 * Se for passado um BlackBoard_VO_CourseVO o mesmo será criado
	 * Se for passado um BlackBoard_VO_CourseFileVO, será obrigatório informar um array de BlackBoard_VO_CourseFileVO no segundo parâmetro
	 * para que sejam criadas as linhas do Arquivo.
	 *
	 * @param BlackBoard_VO $vo
	 * @param array $arrayFile
	 * @throws Exception
	 * @return Ambigous <string, mixed, BlackBoard_Mensageiro>|BlackBoard_Mensageiro
	 */
	public function createOrganization(BlackBoard_VO_OrganizationFileVO $vo, array $arrayFile = null){
		
		try {
			
			if($vo instanceof BlackBoard_VO_OrganizationVO){
				die('Criar o Consumo no WS');
	// 			return parent::buildBody("createCourse", "Course", $vo->toBBXml());
			} elseif ($vo instanceof BlackBoard_VO_OrganizationFileVO){
				$header = $vo->toSnapShotHeader();
				$body   = array($header);
				foreach($arrayFile as $fvo){
					$body[] = $fvo->toSnapShotBody();
				}
				$finalBody = implode("\n", $body);
				
				return $this->snapShotCall($finalBody, __METHOD__);
				
			}
			
			
		} catch (Exception $e) {
			throw $e;
			return new BlackBoard_Mensageiro("Erro ao Criar a Comunidade", BlackBoard_Mensageiro::ERRO, $e);
		}
		
	}
	

}
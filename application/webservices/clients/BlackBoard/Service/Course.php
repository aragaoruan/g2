<?php

/**
 * BlackBoard Web Services e Snapshot Flat File para PHP
 * 
 * Classes para Consumo dos serviços do BlackBoard
 * Desenvolvido com o estudo do "BbPHP: Blackboard Web Services Library for PHP - Copyright (C) 2011 by St. Edward's University (www.stedwards.edu) - @author johns"
 * 
 * As classes e métodos foram refeitos para se ajustar a necessidade de consumir os dois tipos de serviços
 *  1 - Web Services
 *  2 - Snapshot Flat File
 *  
 * 
 * http://help.blackboard.com/en-us/Learn/9.1_SP_12_and_SP_13/Administrator/140_System_Integration/010_Student_Information_System_%28SIS%29/030_SIS_Integration_Types/010_Snapshot_Flat_File
 * http://help.blackboard.com/en-us/Learn/9.1_SP_12_and_SP_13/Administrator/140_System_Integration/010_Student_Information_System_%28SIS%29/030_SIS_Integration_Types/010_Snapshot_Flat_File/060_Snapshot_Flat_File_Examples
 *
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * 
 */
class BlackBoard_Service_Course extends BlackBoard_Service {
	
	public $endpoint_url = "/webapps/bb-data-integration-flatfile-BBLEARN/endpoint/course/store";
	
	public $service = 'Course';
	

	/**
	 *
	 * Cria um ou mais Cursos no BlackBoard.
	 * Se for passado um BlackBoard_VO_CourseVO o mesmo será criado
	 * Se for passado um BlackBoard_VO_CourseFileVO, será obrigatório informar um array de BlackBoard_VO_CourseFileVO no segundo parâmetro
	 * para que sejam criadas as linhas do Arquivo.
	 *
	 * @param BlackBoard_VO $vo
	 * @param array $arrayFile
	 * @throws Exception
	 * @return Ambigous <string, mixed, BlackBoard_Mensageiro>|BlackBoard_Mensageiro
	 */
	public function createCourse(BlackBoard_VO $vo, array $arrayFile = null){
		
		try {
			
			if($vo instanceof BlackBoard_VO_CourseVO){
				die('Criar o Consumo no WS');
	// 			return parent::buildBody("createCourse", "Course", $vo->toBBXml());
			} elseif ($vo instanceof BlackBoard_VO_CourseFileVO){
				$header = $vo->toSnapShotHeader();
				$body   = array($header);
				foreach($arrayFile as $fvo){
					$body[] = $fvo->toSnapShotBody();
				}
				$finalBody = implode("\n", $body);
				
				return $this->snapShotCall($finalBody, __METHOD__);
				
			}
			
			
		} catch (Exception $e) {
			throw $e;
			return new BlackBoard_Mensageiro("Erro ao Criar o Curso", BlackBoard_Mensageiro::ERRO, $e);
		}
		
	}
	
	
	/**
	 * Retorna os dados de um Curso
	 * @param BlackBoard_VO_CourseFilterVO $filter
	 * @throws Exception
	 * @return BlackBoard_Mensageiro
	 */
	public function getCourse(BlackBoard_VO_CourseFilterVO $filter, $xml_return = false) {
		
		try {
	
			$body  = '<SOAP-ENV:Body xmlns:ns1="http://' . strtolower($this->service) . '.ws.blackboard">';
			$body .= "<ns1:getCourse>".$filter->toBBXml()."</ns1:getCourse>";
			$body .= '</SOAP-ENV:Body>';
	
			return new BlackBoard_Mensageiro(array('Courses'=>$this->doCompleteRequest($body, __METHOD__, $xml_return)), BlackBoard_Mensageiro::SUCESSO);
	
		} catch (Exception $e) {
			throw $e;
			return new BlackBoard_Mensageiro($e->getMessage(), BlackBoard_Mensageiro::ERRO, $e);
		}
		
	}
	
	/**
	 * Retorna os dados de uma Comunidade/Organização usando os filtros do  Curso
	 * @param BlackBoard_VO_CourseFilterVO $filter
	 * @throws Exception
	 * @return BlackBoard_Mensageiro
	 */
	public function getOrg(BlackBoard_VO_CourseFilterVO $filter) {
		
		try {
	
			$body  = '<SOAP-ENV:Body xmlns:ns1="http://' . strtolower($this->service) . '.ws.blackboard">';
			$body .= "<ns1:getOrg>".$filter->toBBXml()."</ns1:getOrg>";
			$body .= '</SOAP-ENV:Body>';
	
			return new BlackBoard_Mensageiro(array('Organizations'=>$this->doCompleteRequest($body, __METHOD__)), BlackBoard_Mensageiro::SUCESSO);
	
		} catch (Exception $e) {
			throw $e;
			return new BlackBoard_Mensageiro($e->getMessage(), BlackBoard_Mensageiro::ERRO, $e);
		}
		
	}
	
	
	
	/**
	 * @param BlackBoard_VO_GroupVO $vo
	 * @throws Exception
	 * @return BlackBoard_Mensageiro
	 */
	public function saveGroup(BlackBoard_VO_GroupVO $vo){
		
		try {
		
			$body  = '<SOAP-ENV:Body xmlns:ns1="http://' . strtolower($this->service) . '.ws.blackboard">';
			$body .= "<ns1:saveGroup><courseId>".$vo->getcourseId()."</courseId>".$vo->toBBXml()."</ns1:saveGroup>";
			$body .= '</SOAP-ENV:Body>';

			return new BlackBoard_Mensageiro(array('GroupID'=>$this->doCompleteRequest($body, __METHOD__)), BlackBoard_Mensageiro::SUCESSO);
		
		} catch (Exception $e) {
			throw $e;
			return new BlackBoard_Mensageiro($e->getMessage(), BlackBoard_Mensageiro::ERRO, $e);
		}
		
	}

	
	/**
	 * Save the given course (create or update a course or organization)
	 * @param BlackBoard_VO_CourseVO $vo
	 * @throws Exception
	 * @return BlackBoard_Mensageiro
	 */
	public function saveCourse(BlackBoard_VO_CourseVO $vo){
		
		try {
		
			$body  = '<SOAP-ENV:Body xmlns:ns1="http://' . strtolower($this->service) . '.ws.blackboard">';
			$body .= "<ns1:saveCourse>".$vo->toBBXml()."</ns1:saveCourse>";
			$body .= '</SOAP-ENV:Body>';
			
			return new BlackBoard_Mensageiro(array('CourseID'=>$this->doCompleteRequest($body, __METHOD__)), BlackBoard_Mensageiro::SUCESSO);
		
		} catch (Exception $e) {
			throw $e;
			return new BlackBoard_Mensageiro($e->getMessage(), BlackBoard_Mensageiro::ERRO, $e);
		}
		
	}
	
	
	/**
	 * @param BlackBoard_VO_GroupFilterVO $vo
	 * @throws Exception
	 * @return BlackBoard_Mensageiro
	 */
	public function getGroup($courseId, BlackBoard_VO_GroupFilterVO $vo)	{
		
		try {
		
			$body  = '<SOAP-ENV:Body xmlns:ns1="http://' . strtolower($this->service) . '.ws.blackboard" xmlns:ns2="http://' . strtolower($this->service) . '.ws.blackboard">';
			$body .= "<ns1:getGroup><courseId>".$courseId."</courseId>".$vo->toBBXml()."</ns1:getGroup>";
			$body .= '</SOAP-ENV:Body>';
			
			$retorno = $this->doCompleteRequest($body, __METHOD__);
			if($retorno){
				if(isset($retorno['available'])){
					return new BlackBoard_Mensageiro(array($retorno), BlackBoard_Mensageiro::SUCESSO);
				} else {
					return new BlackBoard_Mensageiro($retorno, BlackBoard_Mensageiro::SUCESSO);
				}
			} else {
				return new BlackBoard_Mensageiro("Nenhum Grupo encontrado.", BlackBoard_Mensageiro::AVISO);
			}
		
		} catch (Exception $e) {
			throw $e;
			return new BlackBoard_Mensageiro($e->getMessage(), BlackBoard_Mensageiro::ERRO, $e);
		}
		
	}
	
	
	/**
	 * @return the $endpoint_url
	 */
	public function getEndpoint_url() {
		return $this->endpoint_url;
	}

	/**
	 * @return the $service
	 */
	public function getService() {
		return $this->service;
	}
	
// 	/**
// 	 * @return the $service
// 	 */
// 	public function getService() {
// 		return $this->service;
// 	}

// 	/**
// 	 * @param Ambigous <string, NULL> $url
// 	 */
// 	public function setUrl($url) {
// 		$this->url = $url;
// 	}

// 	/**
// 	 * @param Ambigous <string, NULL> $service
// 	 */
// 	public function setService($service) {
// 		$this->service = $service;
// 	}


}
<?php 
/**
 * BlackBoard Web Services e Snapshot Flat File para PHP
 * 
 * Classes para Consumo dos serviços do BlackBoard
 * Desenvolvido com o estudo do "BbPHP: Blackboard Web Services Library for PHP - Copyright (C) 2011 by St. Edward's University (www.stedwards.edu) - @author johns"
 * 
 * As classes e métodos foram refeitos para se ajustar a necessidade de consumir os dois tipos de serviços
 *  1 - Web Services
 *  2 - Snapshot Flat File
 *  
 *
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * 
 */
class BlackBoard_Service_CourseMembership extends BlackBoard_Service {

	public $service = 'CourseMembership';
	
	/**
	 * Salva um membro no Curso
	 * 
	 * BlackBoard/Doc/CourseMembership.WS/index.html
	 * 
	 * @param BlackBoard_VO_CourseMembershipVO $vo
	 * @throws Exception
	 * @return Ambigous <boolean, NULL, BlackBoard_Mensageiro>
	 */
	public function saveCourseMembership(BlackBoard_VO_CourseMembershipVO $vo) {
		
		
		try {
		
			$body  = '<SOAP-ENV:Body>';
			$body .= '<ns2:saveCourseMembership xmlns="http://ws.platform.blackboard/xsd" xmlns:ns2="http://coursemembership.ws.blackboard" xmlns:ns3="http://coursemembership.ws.blackboard/xsd">';
			$body .= '<ns2:courseId>'.$vo->getCourseId().'</ns2:courseId>';
			$body .= $vo->toBBXml();
			$body .= "</ns2:saveCourseMembership>";
			$body .= '</SOAP-ENV:Body>';
			
			
			/**
			 * FUNCIONANDO NESTE FORMADO
			
			$body = '<SOAP-ENV:Body>
        <ns2:saveCourseMembership xmlns="http://ws.platform.blackboard/xsd" xmlns:ns2="http://coursemembership.ws.blackboard" xmlns:ns3="http://coursemembership.ws.blackboard/xsd">
            <ns2:courseId>_106_1</ns2:courseId>
            <ns2:cmArray>
                <ns3:available>true</ns3:available>
                <ns3:courseId>_106_1</ns3:courseId>
                <ns3:userId>_109_1</ns3:userId>
            </ns2:cmArray>
        </ns2:saveCourseMembership>
    </SOAP-ENV:Body>';
			
			 */
			
			$CourseMembershipId = $this->doCompleteRequest($body, __METHOD__);
			
			if($CourseMembershipId){
				return new BlackBoard_Mensageiro(array('CourseMembershipId'=>$CourseMembershipId), BlackBoard_Mensageiro::SUCESSO);
			} else {
				throw new BlackBoard_Exception("Não foi possível inserir o Usuário no Curso ".$vo->getCourseId());
			}
		
		} catch (\Exception $e) {
			throw $e;
			return new BlackBoard_Mensageiro($e->getMessage(), BlackBoard_Mensageiro::ERRO, $e);
		}
		
	}
	
	
	
	/**
	 * 
	 * Salva um Membro de um Grupo
	 * Para criar o grupo, ver BlackBoard_Service_Course::saveGroup()
	 * 
	 * @param BlackBoard_VO_GroupMembershipVO $vo
	 * @throws Exception
	 * @return BlackBoard_Mensageiro
	 * 
	 * 
	 * file:///D:/Zend/Apache2/htdocs/g2lite/application/webservices/clients/BlackBoard/Doc/CourseMembership.WS/index.html
	 * 
	 */
	public function saveGroupMembership($courseId, BlackBoard_VO_GroupMembershipVO $vo){
		try {		
			
			
			$body  = '<SOAP-ENV:Body>';
			$body .= '<ns2:saveGroupMembership xmlns="http://ws.platform.blackboard/xsd" xmlns:ns2="http://coursemembership.ws.blackboard" xmlns:ns3="http://coursemembership.ws.blackboard/xsd">';
			$body .= '<ns2:courseId>'.$courseId.'</ns2:courseId>';
			$body .= $vo->toBBXml();
			$body .= "</ns2:saveGroupMembership>";
			$body .= '</SOAP-ENV:Body>';
			
			$GroupMembershipId = $this->doCompleteRequest($body, __METHOD__);
			if($GroupMembershipId){
				return new BlackBoard_Mensageiro(array('GroupMembershipId'=>$GroupMembershipId), BlackBoard_Mensageiro::SUCESSO);
			} else {
				throw new BlackBoard_Exception("Não foi possível inserir o Usuário no Grupo.");
			}
		
		} catch (Exception $e) {
			throw $e;
			return new BlackBoard_Mensageiro($e->getMessage(), BlackBoard_Mensageiro::ERRO, $e);
		}
	}
	
	
	/**
	 * 
    Setting MembershipFilter.filterType = 1 will load CourseMembershipVO records by Id's.
    Setting MembershipFilter.filterType = 2 will load CourseMembershipVO records by course Id's.
    Setting MembershipFilter.filterType = 5 will load CourseMembershipVO records by User Id's.
    Setting MembershipFilter.filterType = 6 will load CourseMembershipVO records by course and user Id's.
    Setting MembershipFilter.filterType = 7 will load CourseMembershipVO records by course Id's and role Identifier.

	 * @param unknown_type $courseId
	 */
	public function getCourseMembership($courseId, BlackBoard_VO_MembershipFilterVO $filter){
		
		try {
		
// 			$member = new stdClass();
// 			$member->courseId = $argv[1];
// 			$member->f = new stdClass();
// 			$member->f->userIds = array($argv[2]);
// 			$member->f->filterType = 6;
// 			$results = $membership_client->getCourseMembership($member);
			
			
			$body  = '<SOAP-ENV:Body xmlns:ns1="http://' . strtolower($this->service) . '.ws.blackboard">';
			$body .= "<ns1:getCourseMembership><ns1:courseId>".$courseId."</ns1:courseId>".$filter->toBBXml()."</ns1:getCourseMembership>";
			$body .= '</SOAP-ENV:Body>';
		
			$user = $this->doCompleteRequest($body, __METHOD__);
			if($user){
				return new BlackBoard_Mensageiro(array('CourseMembership'=>$user), BlackBoard_Mensageiro::SUCESSO);
			} else {
				return new BlackBoard_Mensageiro("Nenhum usuário encontrado no Curso ".$courseId." do BlackBoard", BlackBoard_Mensageiro::AVISO);
			}
		
		} catch (Exception $e) {
			throw $e;
			return new BlackBoard_Mensageiro($e->getMessage(), BlackBoard_Mensageiro::ERRO, $e);
		}
		
	}

	
	/**
	 * 

    Setting MembershipFilter.filterType = 1 will load GroupMembershipVO records by Id's.
    Setting MembershipFilter.filterType = 2 will load GroupMembershipVO records by course Id's.
    Setting MembershipFilter.filterType = 3 will load GroupMembershipVO records by CourseMembershipVO Id's.
    Setting MembershipFilter.filterType = 4 will load GroupMembershipVO records Group Id's.

    
	 * @param unknown_type $courseId
	 * @param BlackBoard_VO_MembershipFilterVO $filter
	 * @throws Exception
	 * @return BlackBoard_Mensageiro
	 */
	public function getGroupMembership($courseId, BlackBoard_VO_MembershipFilterVO $filter){
		
		try {
		
			$body  = '<SOAP-ENV:Body xmlns:ns1="http://' . strtolower($this->service) . '.ws.blackboard">';
			$body .= "<ns1:getGroupMembership><ns1:courseId>".$courseId."</ns1:courseId>".$filter->toBBXml()."</ns1:getGroupMembership>";
			$body .= '</SOAP-ENV:Body>';
		
			$user = $this->doCompleteRequest($body, __METHOD__);
			if($user){
				return new BlackBoard_Mensageiro(array('GroupMembership'=>$user), BlackBoard_Mensageiro::SUCESSO);
			} else {
				return new BlackBoard_Mensageiro("Nenhum usuário encontrado no Grupo informado BlackBoard", BlackBoard_Mensageiro::AVISO);
			}
		
		} catch (Exception $e) {
			throw $e;
			return new BlackBoard_Mensageiro($e->getMessage(), BlackBoard_Mensageiro::ERRO, $e);
		}
		
	}
	
	
}

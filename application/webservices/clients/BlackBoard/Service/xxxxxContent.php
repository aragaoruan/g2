<?php 
/**
 * BlackBoard Web Services e Snapshot Flat File para PHP
 * 
 * Classes para Consumo dos serviços do BlackBoard
 * Desenvolvido com o estudo do "BbPHP: Blackboard Web Services Library for PHP - Copyright (C) 2011 by St. Edward's University (www.stedwards.edu) - @author johns"
 * 
 * As classes e métodos foram refeitos para se ajustar a necessidade de consumir os dois tipos de serviços
 *  1 - Web Services
 *  2 - Snapshot Flat File
 *  
 *
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * 
 */
class BlackBoard_Service_Content extends BlackBoard_Service {
	
	public $url = 'Content';
	
	public function getFilteredContent($args) {
		$body = '<ns1:courseId>' . $args['courseId'] . '</ns1:courseId>';
		$body .= '<ns1:filter xmlns:ns2="http://content.ws.blackboard/xsd">';
		
		foreach ($args['filter'] as $key => $arg) {
			$body .= '<ns2:' . $key . '>' . $arg . '</ns2:' . $key . '>';
		}

		$body .= '</ns1:filter>';		
		
		return parent::buildBody("getFilteredContent", "Content", $body);
	}	
	
	public function __call($method, $args = null) {
		return parent::buildBody($method, 'Content', $args[0]);
	}
}

<?php 
/**
 * BlackBoard Web Services e Snapshot Flat File para PHP
 * 
 * Classes para Consumo dos serviços do BlackBoard
 * Desenvolvido com o estudo do "BbPHP: Blackboard Web Services Library for PHP - Copyright (C) 2011 by St. Edward's University (www.stedwards.edu) - @author johns"
 * 
 * As classes e métodos foram refeitos para se ajustar a necessidade de consumir os dois tipos de serviços
 *  1 - Web Services
 *  2 - Snapshot Flat File
 *  
 *
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * 
 */
class BlackBoard_Service_Context extends BlackBoard_Service {
	
	public $service = 'Context';
	

	/** 
	 * Inicia o serviço, é sempre chamado quando alguma requisição vai acontecer
	 * @see BlackBoard::initialize()
	 */
	public function initialize(){
		
		try {
			
			$body  = '<SOAP-ENV:Body xmlns:ns1="http://' . strtolower($this->service) . '.ws.blackboard">';
			$body .= "<ns1:initialize></ns1:initialize>";
			$body .= '</SOAP-ENV:Body>';
			
			$session_id = $this->doCompleteRequest($body, __METHOD__);
			
			if(!$session_id){
				throw new BlackBoard_Exception("Sessão inválida! O WebService pode estar fora do ar ou os dados de acesso inválidos!");
			}
			
			return $session_id;
			
		} catch (Exception $e) {
			throw $e;			
		}
		
	}
	
	/**
	 * Efetua o Login no WS do BlackBoard
	 * É sempre chamado quando uma requisição é feita
	 * 	 */
	public function loginTool(){
		
		try {
		
			$body  = '<SOAP-ENV:Body xmlns:ns1="http://' . strtolower($this->service) . '.ws.blackboard">';
			$body .= "<ns1:loginTool>
<ns1:password>".$this->getRegistrationpassword()."</ns1:password>
<ns1:clientVendorId>".$this->getClientvendorid()."</ns1:clientVendorId>
<ns1:clientProgramId>".$this->getClientprogramid()."</ns1:clientProgramId>
<ns1:loginExtraInfo>
</ns1:loginExtraInfo>
<ns1:expectedLifeSeconds>5000</ns1:expectedLifeSeconds>
</ns1:loginTool>";
			$body .= '</SOAP-ENV:Body>';
		
			return $this->doCompleteRequest($body, __METHOD__);
		
		} catch (Exception $e) {
			throw $e;
		}
		
	}
	
	/**
	 * Registra no WS os Serviços a Serem usados
	 * @throws Exception
	 * @return Ambigous <boolean, NULL, BlackBoard_Mensageiro>
	 */
	public function registerTool(){
		
		try {
		$body  = '<SOAP-ENV:Body xmlns:ns1="http://' . strtolower($this->service) . '.ws.blackboard">';
		$body .= "<ns1:registerTool>
<ns1:clientVendorId>".$this->getClientvendorid()."</ns1:clientVendorId>
<ns1:clientProgramId>".$this->getClientprogramid()."</ns1:clientProgramId>
<ns1:registrationPassword>".$this->getRegistrationpassword()."</ns1:registrationPassword>
<ns1:initialSharedSecret>".$this->getRegistrationpassword()."</ns1:initialSharedSecret>
<ns1:description>Registrando os Serviços a serem usados</ns1:description>
<ns1:requiredToolMethods>Context.WS:login</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Context.WS:loginTool</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Context.WS:emulateUser</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Context.WS:extendSessionLife</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Context.WS:getMemberships</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Context.WS:getMyMemberships</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Context.WS:initialize</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Course.WS:changeCourseBatchUid</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Course.WS:changeCourseCategoryBatchUid</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Course.WS:changeCourseDataSourceId</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Course.WS:changeOrgBatchUid</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Course.WS:changeOrgCategoryBatchUid</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Course.WS:changeOrgDataSourceId</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Course.WS:createCourse</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Course.WS:createOrg</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Course.WS:getAvailableGroupTools</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Course.WS:getCartridge</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Course.WS:getCategories</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Course.WS:getClassifications</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Course.WS:getCourse</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Course.WS:getCourseCategoryMembership</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Course.WS:getGroup</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Course.WS:getOrg</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Course.WS:getOrgCategoryMembership</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Course.WS:getServerVersion</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Course.WS:getStaffInfo</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Course.WS:initializeCourse</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Course.WS:saveCartridge</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Course.WS:saveCourse</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Course.WS:saveCourseCategory</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Course.WS:saveGroup</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Course.WS:saveOrgCategory</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Course.WS:saveOrgCategoryMembership</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Course.WS:saveStaffInfo</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Course.WS:setCourseBannerImage</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Course.WS:updateCourse</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Course.WS:updateOrg</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Announcement.WS:getCourseAnnouncements</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Announcement.WS:getOrgAnnouncements</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Announcement.WS:getSystemAnnouncements</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Announcement.WS:initializeAnnouncementWS</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Announcement.WS:createCourseAnnouncements</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Announcement.WS:createOrgAnnouncements</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Announcement.WS:createSystemAnnouncements</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Announcement.WS:updateCourseAnnouncements</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Announcement.WS:updateOrgAnnouncements</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Announcement.WS:updateSystemAnnouncements</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Announcement.WS:getServerVersion</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Gradebook.WS:getAttempts</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Gradebook.WS:getGradebookColumns</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Gradebook.WS:getGradebookTypes</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Gradebook.WS:getGrades</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Gradebook.WS:getGradingSchemas</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Gradebook.WS:getRequiredEntitlements</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Gradebook.WS:getServerVersion</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Gradebook.WS:initializeGradebookWS</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Gradebook.WS:saveAttempts</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Gradebook.WS:saveColumns</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Gradebook.WS:saveGradebookTypes</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Gradebook.WS:saveGrades</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Gradebook.WS:saveGradingSchemas</ns1:requiredToolMethods>
<ns1:requiredToolMethods>CourseMembership.WS:saveCourseMembership</ns1:requiredToolMethods>
<ns1:requiredToolMethods>CourseMembership.WS:deleteCourseMembership</ns1:requiredToolMethods>
<ns1:requiredToolMethods>CourseMembership.WS:getCourseMembership</ns1:requiredToolMethods>
<ns1:requiredToolMethods>CourseMembership.WS:getCourseRoles</ns1:requiredToolMethods>
<ns1:requiredToolMethods>CourseMembership.WS:getGroupMembership</ns1:requiredToolMethods>
<ns1:requiredToolMethods>CourseMembership.WS:saveGroupMembership</ns1:requiredToolMethods>
<ns1:requiredToolMethods>CourseMembership.WS:deleteGroupMembership</ns1:requiredToolMethods>
<ns1:requiredToolMethods>CourseMembership.WS:getServerVersion</ns1:requiredToolMethods>
<ns1:requiredToolMethods>CourseMembership.WS:initializeCourseMembershipWS</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Content.WS:addContentFile</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Content.WS:updateContentFileLinkName</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Content.WS:getContentFiles</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Content.WS:getFilteredContent</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Content.WS:getReviewStatusByCourseId</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Content.WS:getFilteredCourseStatus</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Content.WS:getLinksByReferrerType</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Content.WS:getLinksByReferredToType</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Content.WS:getTOCsByCourseId</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Content.WS:initializeContentWS</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Content.WS:initializeVersion2ContentWS</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Content.WS:getRequiredEntitlements</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Content.WS:saveContent</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Content.WS:saveCourseTOC</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Content.WS:saveContentsReviewed</ns1:requiredToolMethods>
<ns1:requiredToolMethods>NotificationDistributorOperations.WS:getServerVersion</ns1:requiredToolMethods>
<ns1:requiredToolMethods>NotificationDistributorOperations.WS:initializeNotificationDistributorOperationsWS</ns1:requiredToolMethods>
<ns1:requiredToolMethods>NotificationDistributorOperations.WS:setDistributorAvailabilityForUser</ns1:requiredToolMethods>
<ns1:requiredToolMethods>NotificationDistributorOperations.WS:registerDistributorResults</ns1:requiredToolMethods>
<ns1:requiredToolMethods>NotificationDistributorOperations.WS:</ns1:requiredToolMethods>
<ns1:requiredToolMethods>User.WS:getServerVersion</ns1:requiredToolMethods>
<ns1:requiredToolMethods>User.WS:initializeUserWS</ns1:requiredToolMethods>
<ns1:requiredToolMethods>User.WS:saveUser</ns1:requiredToolMethods>
<ns1:requiredToolMethods>User.WS:saveObserverAssociation</ns1:requiredToolMethods>
<ns1:requiredToolMethods>User.WS:saveAddressBookEntry</ns1:requiredToolMethods>
<ns1:requiredToolMethods>User.WS:getUser</ns1:requiredToolMethods>
<ns1:requiredToolMethods>User.WS:getAddressBookEntry</ns1:requiredToolMethods>
<ns1:requiredToolMethods>User.WS:getObservee</ns1:requiredToolMethods>
<ns1:requiredToolMethods>User.WS:changeUserBatchUid</ns1:requiredToolMethods>
<ns1:requiredToolMethods>User.WS:changeUserDataSourceId</ns1:requiredToolMethods>
<ns1:requiredToolMethods>User.WS:getSystemRoles</ns1:requiredToolMethods>
<ns1:requiredToolMethods>User.WS:getInstitutionRoles</ns1:requiredToolMethods>
<ns1:requiredToolMethods>User.WS:getUserInstitutionRoles</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Calendar.WS:initializeCalendarWS</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Calendar.WS:createPersonalCalendarItem</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Calendar.WS:createCourseCalendarItem</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Calendar.WS:createInstitutionCalendarItem</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Calendar.WS:updatePersonalCalendarItem</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Calendar.WS:updateCourseCalendarItem</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Calendar.WS:updateInstitutionCalendarItem</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Calendar.WS:savePersonalCalendarItem</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Calendar.WS:saveCourseCalendarItem</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Calendar.WS:saveInstitutionCalendarItem</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Calendar.WS:getCalendarItem</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Calendar.WS:canUpdatePersonalCalendarItem</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Calendar.WS:canUpdateCourseCalendarItem</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Calendar.WS:canUpdateInstitutionCalendarItem</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Util.WS:initializeUtilWS</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Util.WS:getRequiredEntitlements</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Util.WS:checkEntitlement</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Util.WS:saveSetting</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Util.WS:loadSetting</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Util.WS:deleteSetting</ns1:requiredToolMethods>
<ns1:requiredToolMethods>Util.WS:getDataSources</ns1:requiredToolMethods>
		</ns1:registerTool>";
		$body .= '</SOAP-ENV:Body>';
		
			return $this->doCompleteRequest($body, __METHOD__);
		
		} catch (Exception $e) {
			throw $e;
		}
		
		
	}
	
}

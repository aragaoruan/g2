<?php 
/**
 * BlackBoard Web Services e Snapshot Flat File para PHP
 *
 * Classes para Consumo dos serviços do BlackBoard
 * Desenvolvido com o estudo do "BbPHP: Blackboard Web Services Library for PHP - Copyright (C) 2011 by St. Edward's University (www.stedwards.edu) - @author johns"
 *
 * As classes e métodos foram refeitos para se ajustar a necessidade de consumir os dois tipos de serviços
 *  1 - Web Services
 *  2 - Snapshot Flat File
 *
 *
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 *
 */
class BlackBoard_Service_User extends BlackBoard_Service {
	
	public $service = 'User';
	
	/**
	 * Retorna um array de usuários
	 * Observar o Uso do tipo de consulta na BlackBoard_VO_UserFilterVO
	 * 
	 * 	const GET_ALL_USERS_WITH_AVAILABILITY 			= 1;
	 *	const GET_USER_BY_ID_WITH_AVAILABILITY  		= 2;
	 *	const GET_USER_BY_BATCH_ID_WITH_AVAILABILITY 	= 3;
	 *	const GET_USER_BY_COURSE_ID_WITH_AVAILABILITY 	= 4;
	 *	const GET_USER_BY_GROUP_ID_WITH_AVAILABILITY 	= 5;
	 *	const GET_USER_BY_NAME_WITH_AVAILABILITY 		= 6;
	 *	const GET_USER_BY_SYSTEM_ROLE 					= 7;
	 *	const GET_ADDRESS_BOOK_ENTRY_BY_CURRENT_USERID 	= 9;
	 *	const GET_ADDRESS_BOOK_ENTRY_BY_ID 				= 8;
	 * 
	 * 
	 * @param BlackBoard_VO_UserFilterVO $filter
	 * @throws Exception
	 * @return BlackBoard_Mensageiro
	 */
	public function getUser(BlackBoard_VO_UserFilterVO $filter) {
		
		try {
			
			$body  = '<SOAP-ENV:Body xmlns:ns1="http://' . strtolower($this->service) . '.ws.blackboard">';
			$body .= "<ns1:getuser>".$filter->toBBXml()."</ns1:getuser>";
			$body .= '</SOAP-ENV:Body>';
		
			$user = $this->doCompleteRequest($body, __METHOD__);
			if($user){
				return new BlackBoard_Mensageiro(array('Users'=>$user), BlackBoard_Mensageiro::SUCESSO);
			} else {
				return new BlackBoard_Mensageiro("Nenhum usuário encontrado no BlackBoard", BlackBoard_Mensageiro::AVISO);
			}
		
		} catch (Exception $e) {
			throw $e;
			return new BlackBoard_Mensageiro($e->getMessage(), BlackBoard_Mensageiro::ERRO, $e);
		}
		
	}
	
	
	/**
	 * Insere ou altera um usuário no BB
	 * @param blackboard_vo_UserVO $user
	 * @return string
	 */
	public function saveUser(BlackBoard_VO_UserVO $user){
		
		try {
		
			$body  = '<SOAP-ENV:Body xmlns:ns1="http://' . strtolower($this->service) . '.ws.blackboard">';
			$body .= "<ns1:saveUser>".$user->toBBXml()."</ns1:saveUser>";
			$body .= '</SOAP-ENV:Body>';
			return new BlackBoard_Mensageiro(array('UserId'=>$this->doCompleteRequest($body, __METHOD__)), BlackBoard_Mensageiro::SUCESSO);
		
		} catch (Exception $e) {
			throw $e;
			return new BlackBoard_Mensageiro($e->getMessage(), BlackBoard_Mensageiro::ERRO, $e);
		}
		
	}
	
	
	public function saveObserverAssociation(BlackBoard_VO_ObserverAssociationVO $vo){
		
		try {
		
			$body  = '<SOAP-ENV:Body xmlns:ns1="http://' . strtolower($this->service) . '.ws.blackboard">';
			$body .= "<ns1:saveObserverAssociation>".$vo->toBBXml()."</ns1:saveObserverAssociation>";
			$body .= '</SOAP-ENV:Body>';
		
			return new BlackBoard_Mensageiro(array('Observer'=>$this->doCompleteRequest($body, __METHOD__)), BlackBoard_Mensageiro::SUCESSO);
		
		} catch (Exception $e) {
			return new BlackBoard_Mensageiro($e->getMessage(), BlackBoard_Mensageiro::ERRO, $e);
		}
		
	}
	
	public function getObservee(BlackBoard_VO_ObserverAssociationVO $vo){
		
		
		try {
			
			$voFilter = clone $vo;
			$voFilter->setObserveeId(null);
		
			$body  = '<SOAP-ENV:Body xmlns:ns1="http://' . strtolower($this->service) . '.ws.blackboard">';
			$body .= "<ns1:getObservee>".$voFilter->toBBXml()."</ns1:getObservee>";
			$body .= '</SOAP-ENV:Body>';
			
			return new BlackBoard_Mensageiro(array('Observer'=>$this->doCompleteRequest($body, __METHOD__)), BlackBoard_Mensageiro::SUCESSO);
		
		} catch (Exception $e) {
			return new BlackBoard_Mensageiro($e->getMessage(), BlackBoard_Mensageiro::ERRO, $e);
		}
		
		
	}
	
}



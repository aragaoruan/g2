<?php

/**
 * BlackBoard Web Services e Snapshot Flat File para PHP
 *
 * Classes para Consumo dos serviços do BlackBoard
 * Desenvolvido com o estudo do "BbPHP: Blackboard Web Services Library for PHP - Copyright (C) 2011 by St. Edward's University (www.stedwards.edu) - @author johns"
 *
 * As classes e métodos foram refeitos para se ajustar a necessidade de consumir os dois tipos de serviços
 *  1 - Web Services
 *  2 - Snapshot Flat File
 *
 *
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 *
 */
abstract class BlackBoard_VO
{


    abstract function getXmlNodeName();

    abstract function getXmlns();


    /**
     * Recebe o XML do BB convertido em Array e gera o Objeto Populado
     * @param array $array
     */
    public function buildVOfromXml(array $array)
    {
            $arChaves = array_keys(get_object_vars($this));
            foreach ($arChaves as $chave) {
                if (isset($array[$chave])) {
                    $metodo = 'set' . $chave;
                    if (is_array($array[$chave]) && isset($array[$chave]['@attributes']['nil'])) {
                        $this->$metodo(null);
                    } else {
                        $this->$metodo($array[$chave]);
                    }
                }
            }
    }


    /**
     * Converte um VO em uma String para ser adicionada no XML que irá ser enviado ao WS
     * @return string
     */
    public function toBBXml($setanull = true)
    {

        $arChaves = get_object_vars($this);
        $x = 1;
        $xml = '<' . $this->getXmlNodeName() . '>';
        foreach ($arChaves as $chave => $valor) {
            if (is_array($valor)) {
                if ($valor) {
                    foreach ($valor as $item) {
                        $xml .= "<$chave xmlns=\"" . $this->getXmlns() . "\">" . strip_tags($item) . "</$chave>";
                    }
                }
            } elseif ($valor instanceof BlackBoard_VO) {
                $xml .= $valor->toBBXml($setanull);
            } else {
                if ($valor === null) {
                    if ($setanull)
                        $xml .= "<$chave xsi:nil=\"true\" />";
                } else {
                    $xml .= "<$chave>" . strip_tags($valor) . "</$chave>";
                }
            }
        }
        $xml .= '</' . $this->getXmlNodeName() . '>';
        return $xml;
    }


    /**
     * Retorna o Header do Arquivo para integração com o BlackBoard
     * Só usado no caso de uma integração por arquivo SnapShot
     * @return string
     */
    public function toSnapShotHeader()
    {
        return implode('|', array_keys(get_object_vars($this)));
    }

    /**
     * Retorna os dados do VO formatados para acompanhar o Header do snapshot
     * @return string
     */
    public function toSnapShotBody()
    {
        $arChaves = get_object_vars($this);
        foreach ($arChaves as $chave => $valor) {
            if ($valor instanceof DateTime) {
                $this->$chave = $valor->format('Ymd');
            } elseif (is_string($valor)) {
                $this->$chave = utf8_decode($valor);
            }
        }
        return implode('|', array_values(get_object_vars($this)));
    }


}

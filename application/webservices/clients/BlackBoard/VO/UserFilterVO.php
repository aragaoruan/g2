<?php
/**
 * BlackBoard Web Services e Snapshot Flat File para PHP
 *
 * Classes para Consumo dos serviços do BlackBoard
 * Desenvolvido com o estudo do "BbPHP: Blackboard Web Services Library for PHP - Copyright (C) 2011 by St. Edward's University (www.stedwards.edu) - @author johns"
 *
 * As classes e métodos foram refeitos para se ajustar a necessidade de consumir os dois tipos de serviços
 *  1 - Web Services
 *  2 - Snapshot Flat File
 *
 *
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 *
 */
class BlackBoard_VO_UserFilterVO extends BlackBoard_VO {

	
	const GET_ALL_USERS_WITH_AVAILABILITY 			= 1;
	const GET_USER_BY_ID_WITH_AVAILABILITY  		= 2;
	const GET_USER_BY_BATCH_ID_WITH_AVAILABILITY 	= 3;
	const GET_USER_BY_COURSE_ID_WITH_AVAILABILITY 	= 4;
	const GET_USER_BY_GROUP_ID_WITH_AVAILABILITY 	= 5;
	const GET_USER_BY_NAME_WITH_AVAILABILITY 		= 6;
	const GET_USER_BY_SYSTEM_ROLE 					= 7;
	const GET_ADDRESS_BOOK_ENTRY_BY_CURRENT_USERID 	= 9;
	const GET_ADDRESS_BOOK_ENTRY_BY_ID 				= 8;
	
	
	/* (non-PHPdoc)
	 * @see BlackBoard_VO::toBBXml()
	 */
	public function toBBXml($setanull = true){
		if(!$this->getFilterType()){
			throw new BlackBoard_Exception('A propriedade FilterType precisa conter o valor referente ao tipo de consulta');
		}
		return parent::toBBXml($setanull);
	}
	
	/**
	 * @return the $_xmlNodeName
	 */
	public function getXmlNodeName() {
		return "filter";
	}

	/**
	 * @return the $_xml_xmlns
	 */
	public function getXmlns() {
		return "http://user.ws.blackboard/xsd";
	}

	
	/**
	 * @var string true | false
	 */
	public $available = 'true';
	
	/**
	 * @var array
	 */
	public $batchId = array();
	
	/**
	 * @var array
	 */
	public $courseId = array();
	
	/**
	 * @var array
	 */
	public $expansionData = array();
	
	/**
	 * @var int
	 * 
	 */
	public $filterType = null;
	
	/**
	 * @var array
	 */
	public $groupId = array();
	
	/**
	 * @var array
	 */
	public $id = array();
	
	/**
	 * @var array
	 */
	public $name = array();
	
	/**
	 * @var array
	 */
	public $systemRoles = array();
	
	
	/**
	 * @return the $available
	 */
	public function getAvailable() {
		return $this->available;
	}

	/**
	 * @return the $batchId
	 */
	public function getBatchId() {
		return $this->batchId;
	}

	/**
	 * @return the $courseId
	 */
	public function getCourseId() {
		return $this->courseId;
	}

	/**
	 * @return the $expansionData
	 */
	public function getExpansionData() {
		return $this->expansionData;
	}

	/**
	 * @return the $filterType
	 */
	public function getFilterType() {
		return $this->filterType;
	}

	/**
	 * @return the $groupId
	 */
	public function getGroupId() {
		return $this->groupId;
	}

	/**
	 * @return the $id
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return the $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @return the $systemRoles
	 */
	public function getSystemRoles() {
		return $this->systemRoles;
	}

	/**
	 * Adds available to search criteria to further filter out users to load if they enabled or not.
	 * Aceita 'true' ou 'false'
	 * @param string $available
	 */
	public function setAvailable($available) {
		$this->available = ($available=='false' ? 'false' : 'true');
	}

	/**
	 * The user batch uid is used as a search criteria to further filter out users to load
	 * Pesquisa o usuário baseado no ID do mesmo no sistema de origem, no caso G2
	 * @param multitype: $batchId
	 */
	public function setBatchId(array $batchId) {
		$this->batchId = $batchId;
	}

	/**
	 * This value should be retrieved from another API call.
	 * @param multitype: $courseId
	 */
	public function setCourseId(array $courseId) {
		$this->courseId = $courseId;
	}

	/**
	 * Expansion data is currently ignored.
	 * @param multitype: $expansionData
	 */
	public function setExpansionData(array $expansionData) {
		$this->expansionData = $expansionData;
	}

	/**
	 * This is one of the defined types in this class.
	 * 
	 * 	const GET_ALL_USERS_WITH_AVAILABILITY 			= 1;
	 *	const GET_USER_BY_ID_WITH_AVAILABILITY  		= 2;
	 *	const GET_USER_BY_BATCH_ID_WITH_AVAILABILITY 	= 3;
	 *	const GET_USER_BY_COURSE_ID_WITH_AVAILABILITY 	= 4;
	 *	const GET_USER_BY_GROUP_ID_WITH_AVAILABILITY 	= 5;
	 *	const GET_USER_BY_NAME_WITH_AVAILABILITY 		= 6;
	 *	const GET_USER_BY_SYSTEM_ROLE 					= 7;
	 *	const GET_ADDRESS_BOOK_ENTRY_BY_CURRENT_USERID 	= 9;
	 *	const GET_ADDRESS_BOOK_ENTRY_BY_ID 				= 8;
	 * 
	 * @param number $filterType
	 */
	public function setFilterType($filterType) {
		$this->filterType = $filterType;
	}

	/**
	 * This value should be retrieved from another API call.
	 * @param multitype: $groupId
	 */
	public function setGroupId(array $groupId) {
		$this->groupId = $groupId;
	}

	/**
	 * The user id is used as a search criteria to further filter out users to load
	 * @param multitype: $id
	 */
	public function setId(array $id) {
		$this->id = $id;
	}

	/**
	 * The user name is used as a search criteria to further filter out users to load
	 * @param multitype: $name
	 */
	public function setName(array $name) {
		$this->name = $name;
	}

	/**
	 * Adds systemRoles to search criteria to further filter out users to load.
	 * @param multitype: $systemRoles
	 */
	public function setSystemRoles(array $systemRoles) {
		$this->systemRoles = $systemRoles;
	}

}


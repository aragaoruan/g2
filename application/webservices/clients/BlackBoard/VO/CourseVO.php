<?php
/**
 * BlackBoard Web Services e Snapshot Flat File para PHP
 *
 * Classes para Consumo dos serviços do BlackBoard
 * Desenvolvido com o estudo do "BbPHP: Blackboard Web Services Library for PHP - Copyright (C) 2011 by St. Edward's University (www.stedwards.edu) - @author johns"
 *
 * As classes e métodos foram refeitos para se ajustar a necessidade de consumir os dois tipos de serviços
 *  1 - Web Services
 *  2 - Snapshot Flat File
 *
 *
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 *
 */
class BlackBoard_VO_CourseVO extends BlackBoard_VO {

	/**
	 * @return the $_xmlNodeName
	 */
	public function getXmlNodeName() {
		return "course";
	}

	/**
	 * @return the $_xml_xmlns
	 */
	public function getXmlns() {
		return "http://course.ws.blackboard/xsd";
	}
	
	
	public $allowGuests;
	public $allowObservers;
	public $available;
	public $batchUid;
	public $buttonStyleBbId;
	public $buttonStyleShape;
	public $buttonStyleType;
// 	public $cartridgeId;
	public $classificationId;
	public $courseDuration;
	public $courseId;
	public $coursePace;
	public $courseServiceLevel;
	public $dataSourceId;
	public $decAbsoluteLimit = 0;
	public $description;
	public $endDate = 0;
	public $enrollmentAccessCode;
	public $enrollmentEndDate = 0;
	public $enrollmentStartDate = 0;
	public $enrollmentType;
	public $expansionData = array();
	public $fee;
	public $hasDescriptionPage;
	public $id;
	public $institutionName;
//	public $locale;
	public $localeEnforced;
	public $lockedOut;
	public $name;
	public $navCollapsable;
	public $navColorBg;
	public $navColorFg;
	public $navigationStyle;
	public $numberOfDaysOfUse = 0;
	public $organization;
	public $showInCatalog;
	public $softLimit = 0;
	public $startDate = 0;
	public $uploadLimit = 0;
	
	
	
	/**
	 * @return the $buttonStyleType
	 */
	public function getButtonStyleType() {
		return $this->buttonStyleType;
	}

	/**
	 * @param field_type $buttonStyleType
	 */
	public function setButtonStyleType($buttonStyleType) {
		$this->buttonStyleType = $buttonStyleType;
		return $this;
	}

	/**
	 * @return the $allowGuests
	 */
	public function getAllowGuests() {
		return $this->allowGuests;
	}

	/**
	 * @return the $allowObservers
	 */
	public function getAllowObservers() {
		return $this->allowObservers;
	}

	/**
	 * @return the $buttonStyleBbId
	 */
	public function getButtonStyleBbId() {
		return $this->buttonStyleBbId;
	}

	/**
	 * @return the $buttonStyleShape
	 */
	public function getButtonStyleShape() {
		return $this->buttonStyleShape;
	}

	/**
	 * @return the $buttonStyle
	 */
	public function getButtonStyle() {
		return $this->buttonStyle;
	}

	/**
	 * @return the $coursePace
	 */
	public function getCoursePace() {
		return $this->coursePace;
	}

	/**
	 * @return the $courseServiceLevel
	 */
	public function getCourseServiceLevel() {
		return $this->courseServiceLevel;
	}

	/**
	 * @return the $decAbsoluteLimit
	 */
	public function getDecAbsoluteLimit() {
		return $this->decAbsoluteLimit;
	}

	/**
	 * @return the $enrollmentEndDate
	 */
	public function getEnrollmentEndDate() {
		return $this->enrollmentEndDate;
	}

	/**
	 * @return the $enrollmentStartDate
	 */
	public function getEnrollmentStartDate() {
		return $this->enrollmentStartDate;
	}

	/**
	 * @return the $enrollmentType
	 */
	public function getEnrollmentType() {
		return $this->enrollmentType;
	}

	/**
	 * @return the $expansionData
	 */
	public function getExpansionData() {
		return $this->expansionData;
	}

	/**
	 * @return the $fee
	 */
	public function getFee() {
		return $this->fee;
	}

	/**
	 * @return the $hasDescriptionPage
	 */
	public function getHasDescriptionPage() {
		return $this->hasDescriptionPage;
	}

	/**
	 * @return the $locale
	 */
	public function getLocale() {
		return $this->locale;
	}

	/**
	 * @return the $localeEnforced
	 */
	public function getLocaleEnforced() {
		return $this->localeEnforced;
	}

	/**
	 * @return the $lockedOut
	 */
	public function getLockedOut() {
		return $this->lockedOut;
	}

	/**
	 * @return the $navCollapsable
	 */
	public function getNavCollapsable() {
		return $this->navCollapsable;
	}

	/**
	 * @return the $navColorBg
	 */
	public function getNavColorBg() {
		return $this->navColorBg;
	}

	/**
	 * @return the $navColorFg
	 */
	public function getNavColorFg() {
		return $this->navColorFg;
	}

	/**
	 * @return the $navigationStyle
	 */
	public function getNavigationStyle() {
		return $this->navigationStyle;
	}

	/**
	 * @return the $organization
	 */
	public function getOrganization() {
		return $this->organization;
	}

	/**
	 * @return the $showInCatalog
	 */
	public function getShowInCatalog() {
		return $this->showInCatalog;
	}

	/**
	 * @return the $softLimit
	 */
	public function getSoftLimit() {
		return $this->softLimit;
	}

	/**
	 * @return the $uploadLimit
	 */
	public function getUploadLimit() {
		return $this->uploadLimit;
	}

	/**
	 * @param field_type $allowGuests
	 */
	public function setAllowGuests($allowGuests) {
		$this->allowGuests = $allowGuests;
		return $this;
	}

	/**
	 * @param field_type $allowObservers
	 */
	public function setAllowObservers($allowObservers) {
		$this->allowObservers = $allowObservers;
		return $this;
	}

	/**
	 * @param field_type $buttonStyleBbId
	 */
	public function setButtonStyleBbId($buttonStyleBbId) {
		$this->buttonStyleBbId = $buttonStyleBbId;
		return $this;
	}

	/**
	 * @param field_type $buttonStyleShape
	 */
	public function setButtonStyleShape($buttonStyleShape) {
		$this->buttonStyleShape = $buttonStyleShape;
		return $this;
	}

	/**
	 * @param field_type $buttonStyle
	 */
	public function setButtonStyle($buttonStyle) {
		$this->buttonStyle = $buttonStyle;
		return $this;
	}

	/**
	 * @param field_type $coursePace
	 */
	public function setCoursePace($coursePace) {
		$this->coursePace = $coursePace;
		return $this;
	}

	/**
	 * @param field_type $courseServiceLevel
	 */
	public function setCourseServiceLevel($courseServiceLevel) {
		$this->courseServiceLevel = $courseServiceLevel;
		return $this;
	}

	/**
	 * @param field_type $decAbsoluteLimit
	 */
	public function setDecAbsoluteLimit($decAbsoluteLimit) {
		$this->decAbsoluteLimit = $decAbsoluteLimit;
		return $this;
	}

	/**
	 * @param field_type $enrollmentEndDate
	 */
	public function setEnrollmentEndDate($enrollmentEndDate) {
		$this->enrollmentEndDate = $enrollmentEndDate;
		return $this;
	}

	/**
	 * @param field_type $enrollmentStartDate
	 */
	public function setEnrollmentStartDate($enrollmentStartDate) {
		$this->enrollmentStartDate = $enrollmentStartDate;
		return $this;
	}

	/**
	 * @param field_type $enrollmentType
	 */
	public function setEnrollmentType($enrollmentType) {
		$this->enrollmentType = $enrollmentType;
		return $this;
	}

	/**
	 * @param multitype: $expansionData
	 */
	public function setExpansionData($expansionData) {
		$this->expansionData = $expansionData;
		return $this;
	}

	/**
	 * @param field_type $fee
	 */
	public function setFee($fee) {
		$this->fee = $fee;
		return $this;
	}

	/**
	 * @param field_type $hasDescriptionPage
	 */
	public function setHasDescriptionPage($hasDescriptionPage) {
		$this->hasDescriptionPage = $hasDescriptionPage;
		return $this;
	}

	/**
	 * @param field_type $locale
	 */
	public function setLocale($locale) {
		$this->locale = $locale;
		return $this;
	}

	/**
	 * @param field_type $localeEnforced
	 */
	public function setLocaleEnforced($localeEnforced) {
		$this->localeEnforced = $localeEnforced;
		return $this;
	}

	/**
	 * @param field_type $lockedOut
	 */
	public function setLockedOut($lockedOut) {
		$this->lockedOut = $lockedOut;
		return $this;
	}

	/**
	 * @param field_type $navCollapsable
	 */
	public function setNavCollapsable($navCollapsable) {
		$this->navCollapsable = $navCollapsable;
		return $this;
	}

	/**
	 * @param field_type $navColorBg
	 */
	public function setNavColorBg($navColorBg) {
		$this->navColorBg = $navColorBg;
		return $this;
	}

	/**
	 * @param field_type $navColorFg
	 */
	public function setNavColorFg($navColorFg) {
		$this->navColorFg = $navColorFg;
		return $this;
	}

	/**
	 * @param field_type $navigationStyle
	 */
	public function setNavigationStyle($navigationStyle) {
		$this->navigationStyle = $navigationStyle;
		return $this;
	}

	/**
	 * @param field_type $organization
	 */
	public function setOrganization($organization) {
		$this->organization = $organization;
		return $this;
	}

	/**
	 * @param field_type $showInCatalog
	 */
	public function setShowInCatalog($showInCatalog) {
		$this->showInCatalog = $showInCatalog;
		return $this;
	}

	/**
	 * @param field_type $softLimit
	 */
	public function setSoftLimit($softLimit) {
		$this->softLimit = $softLimit;
		return $this;
	}

	/**
	 * @param field_type $uploadLimit
	 */
	public function setUploadLimit($uploadLimit) {
		$this->uploadLimit = $uploadLimit;
		return $this;
	}

	/**
	 * @return the $classificationId
	 */
	public function getClassificationId() {
		return $this->classificationId;
	}

	/**
	 * @return the $enrollmentAccessCode
	 */
	public function getEnrollmentAccessCode() {
		return $this->enrollmentAccessCode;
	}

	/**
	 * @return the $institutionName
	 */
	public function getInstitutionName() {
		return $this->institutionName;
	}

	/**
	 * @param field_type $classificationId
	 */
	public function setClassificationId($classificationId) {
		$this->classificationId = $classificationId;
		return $this;
	}

	/**
	 * @param field_type $enrollmentAccessCode
	 */
	public function setEnrollmentAccessCode($enrollmentAccessCode) {
		$this->enrollmentAccessCode = $enrollmentAccessCode;
		return $this;
	}

	/**
	 * @param field_type $institutionName
	 */
	public function setInstitutionName($institutionName) {
		$this->institutionName = $institutionName;
		return $this;
	}

	/**
	 * @return the $batchUid
	 */
	public function getBatchUid() {
		return $this->batchUid;
	}

	/**
	 * @param field_type $batchUid
	 */
	public function setBatchUid($batchUid) {
		$this->batchUid = $batchUid;
		return $this;
	}

	/**
	 * @return the $dataSourceId
	 */
	public function getDataSourceId() {
		return $this->dataSourceId;
	}

	/**
	 * @param field_type $dataSourceId
	 */
	public function setDataSourceId($dataSourceId) {
		$this->dataSourceId = $dataSourceId;
		return $this;
	}

	/**
	 * @return the $id
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return the $courseId
	 */
	public function getCourseId() {
		return $this->courseId;
	}

	/**
	 * @return the $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @return the $description
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * @return the $courseDuration
	 */
	public function getCourseDuration() {
		return $this->courseDuration;
	}

	/**
	 * @return the $numberOfDaysOfUse
	 */
	public function getNumberOfDaysOfUse() {
		return $this->numberOfDaysOfUse;
	}

	/**
	 * @return the $startDate
	 */
	public function getStartDate() {
		return $this->startDate;
	}

	/**
	 * @return the $endDate
	 */
	public function getEndDate() {
		return $this->endDate;
	}

	/**
	 * @return the $available
	 */
	public function getAvailable() {
		return $this->available;
	}

	/**
	 * @param field_type $id
	 */
	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	/**
	 * @param field_type $courseId
	 */
	public function setCourseId($courseId) {
		$this->courseId = $courseId;
		return $this;
	}

	/**
	 * @param field_type $name
	 */
	public function setName($name) {
		$this->name = $name;
		return $this;
	}

	/**
	 * @param field_type $description
	 */
	public function setDescription($description) {
		$this->description = $description;
		return $this;
	}

	/**
	 * set course duration value for this CourseVO. Legal Values are:

		    Continuous
		    DateRange
		    FixedNumDays

	 * @param field_type $courseDuration
	 */
	public function setCourseDuration($courseDuration) {
		$this->courseDuration = $courseDuration;
		return $this;
	}

	/**
	 * @param field_type $numberOfDaysOfUse
	 */
	public function setNumberOfDaysOfUse($numberOfDaysOfUse) {
		$this->numberOfDaysOfUse = $numberOfDaysOfUse;
		return $this;
	}

	/**
	 * @param field_type $startDate
	 */
	public function setStartDate($startDate) {
		
// 		if($startDate){
// 			if(is_numeric($startDate)){
// 				$startDate = '@'.$startDate;
// 			}
// 			$startDate = new \DateTime($startDate);
// 			$startDate = $startDate->format('Ymd');
// 		}
		
		$this->startDate = $startDate;
		return $this;
	}

	/**
	 * @param field_type $endDate
	 */
	public function setEndDate($endDate) {
		$this->endDate = $endDate;
		return $this;
	}

	/**
	 * @param field_type $available
	 */
	public function setAvailable($available) {
		$this->available = $available;
		return $this;
	}


	
}


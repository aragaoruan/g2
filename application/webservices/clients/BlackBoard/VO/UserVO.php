<?php
/**
 * BlackBoard Web Services e Snapshot Flat File para PHP
 *
 * Classes para Consumo dos serviços do BlackBoard
 * Desenvolvido com o estudo do "BbPHP: Blackboard Web Services Library for PHP - Copyright (C) 2011 by St. Edward's University (www.stedwards.edu) - @author johns"
 *
 * As classes e métodos foram refeitos para se ajustar a necessidade de consumir os dois tipos de serviços
 *  1 - Web Services
 *  2 - Snapshot Flat File
 *
 *
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 *
 */
class BlackBoard_VO_UserVO extends BlackBoard_VO {

	public $birthDate;

	/**
	 * Indica qual a origem dos dados do Registro, exemplo G2 teste = _2_1
	 * @var string
	 */
	public $dataSourceId = '_22_1';

	public $educationLevel;

	public $expansionData;

	/**
	 * Outros dados do Usu�rio
	 * @var BlackBoard_vo_UserExtendedInfoVO
	 */
	public $extendedInfo;

	public $genderType;

	/**
	 * Id do usu�rio no BB
	 * @var string
	 */
	public $id;

	/**
	 * Array de insRoles - ID da Fun��o de Institui��o
	 * Pode ser extra�do de http://jira.unyleya.com.br/secure/attachment/10525/Unyleya%20Community%20Configuration%20Guide%20V_1_4.xlsx
	 * @var array
	 */
	public $insRoles = array();

	/**
	 * Define se o Usu�rio est� Ativo no BB
	 * @var String true | false
	 */
	public $isAvailable = 'true';

	/**
	 * Nome de Usu�rio no BB
	 * @var string
	 */
	public $name;

	/**
	 * Senha do Usu�rio, passar a senha limpa do G2 tb_dadosacesso
	 * @var string
	 */
	public $password;

	public $studentId;


	/**
	[18:07:53] Bruno Coelho: o System Role é o seguinte, pra professor e aluno não precisa passar
	[18:08:22] Bruno Coelho: Mas para os perfis de designer instrucional, observador institucional e administrador de sistemas tem que passar
	[18:08:29] Bruno Coelho: os valores sao:
	[18:12:42] Bruno Coelho: observador_institucional , designer_instrucional, Z
	*/
	public $systemRoles = array();

	public $title;

	/**
	 * ID do usu�rio no G2
	 * @var string
	 */
	public $userBatchUid;
	
	
	/**
	 * @return the $_xmlNodeName
	 */
	public function getXmlNodeName() {
		return "user";
	}

	/**
	 * @return the $_xml_xmlns
	 */
	public function getXmlns() {
		return "http://user.ws.blackboard/xsd";
	}

	

	/**
	 * @return the $birthDate
	 */
	public function getBirthDate() {
		return $this->birthDate;
	}

	/**
	 * @return the $dataSourceId
	 */
	public function getDataSourceId() {
		return $this->dataSourceId;
	}

	/**
	 * @return the $educationLevel
	 */
	public function getEducationLevel() {
		return $this->educationLevel;
	}

	/**
	 * @return the $expansionData
	 */
	public function getExpansionData() {
		return $this->expansionData;
	}

	/**
	 * @return the $extendedInfo
	 */
	public function getExtendedInfo() {
		return $this->extendedInfo;
	}

	/**
	 * @return the $genderType
	 */
	public function getGenderType() {
		return $this->genderType;
	}

	/**
	 * @return the $id
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return the $insRoles
	 */
	public function getInsRoles() {
		return $this->insRoles;
	}

	/**
	 * @return the $isAvailable
	 */
	public function getIsAvailable() {
		return $this->isAvailable;
	}

	/**
	 * @return the $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @return the $password
	 */
	public function getPassword() {
		return $this->password;
	}

	/**
	 * @return the $studentId
	 */
	public function getStudentId() {
		return $this->studentId;
	}

	/**
	 * @return the $systemRoles
	 */
	public function getSystemRoles() {
		return $this->systemRoles;
	}

	/**
	 * @return the $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @return the $userBatchUid
	 */
	public function getUserBatchUid() {
		return $this->userBatchUid;
	}

	/**
	 * @param field_type $birthDate
	 */
	public function setBirthDate($birthDate) {
		$this->birthDate = $birthDate;
	}

	/**
	 * @param string $dataSourceId
	 */
	public function setDataSourceId($dataSourceId) {
		$this->dataSourceId = $dataSourceId;
	}

	/**
	 * @param field_type $educationLevel
	 */
	public function setEducationLevel($educationLevel) {
		$this->educationLevel = $educationLevel;
	}

	/**
	 * @param field_type $expansionData
	 */
	public function setExpansionData($expansionData) {
		$this->expansionData = $expansionData;
	}

	/**
	 * @param BlackBoard_vo_UserExtendedInfoVO|Array $extendedInfo
	 */
	public function setExtendedInfo($extendedInfo) {
		if($extendedInfo instanceof BlackBoard_vo_UserExtendedInfoVO){
			$this->extendedInfo = $extendedInfo;
		} elseif (is_array($extendedInfo)){
			$ei = new BlackBoard_VO_UserExtendedInfoVO();
			$ei->buildVOfromXml($extendedInfo);
			$this->extendedInfo = $ei;
		} else {
			$this->extendedInfo = null;
		}
	}

	/**
	 * @param field_type $genderType
	 */
	public function setGenderType($genderType) {
		$this->genderType = $genderType;
	}

	/**
	 * @param string $id
	 */
	public function setId($id) {
		$this->id = $id;
	}

	/**
	 * 
	 * Ids de Instituição Criados baseado no arquivo: http://jira.unyleya.com.br/secure/attachment/10525/Unyleya%20Community%20Configuration%20Guide%20V_1_4.xlsx
	 * 	TDC
	 *	TDC_IMP
	 *	TDC_AMAGIS
	 *	TDC_IMPA
	 *	TDC_[Nome do Parceiro X]
	 *	TDC_aluno
	 *	TDC_IMP_aluno
	 *	TDC_aluno_avulso
	 *	TDC_AMAGIS_aluno
	 *	TDC_IMPA_aluno
	 *	TDC_professor
	 *	TDC_IMP_professor
	 *	TDC_AMAGIS_professor
	 *	TDC_IMPA_professor
	 *
	 * Pode ser passado mais de um para salvar, exemplo:
	 * array('TDC','TDC_IMPA_aluno','TDC_IMPA_professor') 
	 * 
	 * @param multitype: $insRoles
	 */
	public function setInsRoles($insRoles) {
		if(is_array($insRoles)){
			$this->insRoles = $insRoles;
		} else {
			$this->insRoles = array($insRoles);
		}
	}

	/**
	 * @param string $isAvailable
	 */
	public function setIsAvailable($isAvailable) {
		$this->isAvailable = $isAvailable;
	}

	/**
	 * @param string $name
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * @param string $password
	 */
	public function setPassword($password) {
		$this->password = $password;
	}

	/**
	 * @param field_type $studentId
	 */
	public function setStudentId($studentId) {
		$this->studentId = $studentId;
	}

	/**
	 * 	[18:07:53] Bruno Coelho: o System Role é o seguinte, pra professor e aluno não precisa passar
	 * 	[18:08:22] Bruno Coelho: Mas para os perfis de designer instrucional, observador institucional e administrador de sistemas tem que passar
	 * 	[18:08:29] Bruno Coelho: os valores sao:
	 * 	[18:12:42] Bruno Coelho: observador_institucional , designer_instrucional, Z
	 * @param field_type $systemRoles
	 */
	public function setSystemRoles($systemRoles) {
		if(is_array($systemRoles)){
			$this->systemRoles = $systemRoles;
		} else {
			$this->systemRoles = array($systemRoles);
		}
	}

	/**
	 * @param field_type $title
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * @param string $userBatchUid
	 */
	public function setUserBatchUid($userBatchUid) {
		$this->userBatchUid = $userBatchUid;
	}


	
}


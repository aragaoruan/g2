<?php
/**
 * BlackBoard Web Services e Snapshot Flat File para PHP
 *
 * Classes para Consumo dos serviços do BlackBoard
 * Desenvolvido com o estudo do "BbPHP: Blackboard Web Services Library for PHP - Copyright (C) 2011 by St. Edward's University (www.stedwards.edu) - @author johns"
 *
 * As classes e métodos foram refeitos para se ajustar a necessidade de consumir os dois tipos de serviços
 *  1 - Web Services
 *  2 - Snapshot Flat File
 *
 *
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * 
 * file:///D:/Zend/Apache2/htdocs/g2lite/application/webservices/clients/BlackBoard/Doc/Course.WS/blackboard/ws/course/GroupVO.html
 *
 */
class BlackBoard_VO_GroupVO extends BlackBoard_VO {

	/**
	 * @return the $_xmlNodeName
	 */
	public function getXmlNodeName() {
		return "group";
	}

	/**
	 * @return the $_xml_xmlns
	 */
	public function getXmlns() {
		return "http://course.ws.blackboard/xsd";
	}
	
	
	public $available;
	public $courseId;
	public $description;
	public $descriptionType;
// 	public $expansionData = array(); // Expansion data is currently ignored.
	public $groupTools = array();
	public $id;
	public $title;
	
	
	
	/**
	 * @return the $available
	 */
	public function getAvailable() {
		return $this->available;
	}

	/**
	 * @return the $courseId
	 */
	public function getCourseId() {
		return $this->courseId;
	}

	/**
	 * @return the $description
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * @return the $descriptionType
	 */
	public function getDescriptionType() {
		return $this->descriptionType;
	}

	/**
	 * @return the $groupTools
	 */
	public function getGroupTools() {
		return $this->groupTools;
	}

	/**
	 * @return the $id
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return the $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @param field_type $available
	 */
	public function setAvailable($available) {
		$this->available = $available;
		return $this;
	}

	/**
	 * @param field_type $courseId
	 */
	public function setCourseId($courseId) {
		$this->courseId = $courseId;
		return $this;
	}

	/**
	 * @param field_type $description
	 */
	public function setDescription($description) {
		$this->description = $description;
		return $this;
	}

	/**
	 * @param field_type $descriptionType
	 */
	public function setDescriptionType($descriptionType) {
		$this->descriptionType = $descriptionType;
		return $this;
	}

	/**
	 * @param multitype: $groupTools
	 */
	public function setGroupTools(array $groupTools) {
		$this->groupTools = $groupTools;
		return $this;
	}

	/**
	 * @param field_type $id
	 */
	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	/**
	 * @param field_type $title
	 */
	public function setTitle($title) {
		$this->title = $title;
		return $this;
	}

	
	
	
	
}


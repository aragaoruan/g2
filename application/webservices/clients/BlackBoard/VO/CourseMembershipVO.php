<?php
/**
 * BlackBoard Web Services e Snapshot Flat File para PHP
 *
 * Classes para Consumo dos serviços do BlackBoard
 * Desenvolvido com o estudo do "BbPHP: Blackboard Web Services Library for PHP - Copyright (C) 2011 by St. Edward's University (www.stedwards.edu) - @author johns"
 *
 * As classes e métodos foram refeitos para se ajustar a necessidade de consumir os dois tipos de serviços
 *  1 - Web Services
 *  2 - Snapshot Flat File
 *
 *
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 *
 */
class BlackBoard_VO_CourseMembershipVO extends BlackBoard_VO {

	/**
	 * @return the $_xmlNodeName
	 */
	public function getXmlNodeName() {
		return "ns3:cmArray";
	}

	/**
	 * @return the $_xml_xmlns
	 */
	public function getXmlns() {
		return "http://coursemembership.ws.blackboard/xsd";
	}
	
	public $id;
	public $courseId;
	public $dataSourceId;
	public $available;
	public $roleId;
	public $userId;

	/**
	 * @return the $id
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return the $courseId
	 */
	public function getCourseId() {
		return $this->courseId;
	}

	/**
	 * @return the $dataSourceId
	 */
	public function getDataSourceId() {
		return $this->dataSourceId;
	}


	/**
	 * @return the $available
	 */
	public function getAvailable() {
		return $this->available;
	}

	/**
	 * @return the $roleId
	 */
	public function getRoleId() {
		return $this->roleId;
	}

	/**
	 * @return the $userId
	 */
	public function getUserId() {
		return $this->userId;
	}

	/**
	 * You should not try to 'make up' an id here - only use the ids that you are given from another api call. 
	 * The actual format of the id is Internal to AS: Treat an id as a series of characters.
	 * 
	 * the id to set - only required if you want to update an existing content item.
	 * @param field_type $id 
	 */
	public function setId($id) {
		$this->id = $id;
	}

	/**
	 * 
	 * This is the course associated to a particular user Id . The course id should be in the form "_nnn_1" where nnn is an integer.
	 * 
	 * @param field_type $courseId
	 */
	public function setCourseId($courseId) {
		$this->courseId = $courseId;
	}

	/**
	 * a data source Id value to use for this course membership
	 * @param field_type $dataSourceId
	 */
	public function setDataSourceId($dataSourceId) {
		$this->dataSourceId = $dataSourceId;
	}


	/**
	 * true if this course membership is to be made available, false otherwise.
	 * @param field_type $available
	 */
	public function setAvailable($available) {
		$this->available = $available;
	}

	/**
	 * 
	 * Sets the role for this CourseMembership. This id has to be the same as one of the Id's returned via CourseMembershipWS.getCourseRoles(CourseRoleFilter) 
	 * 
	 * @param field_type $roleId
	 */
	public function setRoleId($roleId) {
		$this->roleId = $roleId;
	}

	/**
	 * 
	 * Sets the user Id value for this CourseMembership.
	 * 
	 * @param field_type $userId
	 */
	public function setUserId($userId) {
		$this->userId = $userId;
	}
	

	
}


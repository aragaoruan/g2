<?php
/**
 * BlackBoard Web Services e Snapshot Flat File para PHP
 *
 * Classes para Consumo dos serviços do BlackBoard
 * Desenvolvido com o estudo do "BbPHP: Blackboard Web Services Library for PHP - Copyright (C) 2011 by St. Edward's University (www.stedwards.edu) - @author johns"
 *
 * As classes e métodos foram refeitos para se ajustar a necessidade de consumir os dois tipos de serviços
 *  1 - Web Services
 *  2 - Snapshot Flat File
 *
 *
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * 
 * /BlackBoard/Doc/CourseMembership.WS/blackboard/ws/coursemembership/MembershipFilter.html
 */
class BlackBoard_VO_MembershipFilterVO extends BlackBoard_VO {

	const GET_BY_ID 				= 1; //MembershipFilter.filterType = 1 will load CourseMembershipVO records by Id's.
	const GET_BY_COURSE_ID  		= 2; //MembershipFilter.filterType = 2 will load CourseMembershipVO records by course Id's.
	const GET_BY_COURSEMEMBERSHIP_ID= 3; //MembershipFilter.filterType = 2 will load CourseMembershipVO records by coursemembership Id's.
	const GET_BY_GROUP_ID  		    = 4; //MembershipFilter.filterType = 2 will load CourseMembershipVO records by group Id's.
	const GET_BY_USER_ID 			= 5; //MembershipFilter.filterType = 5 will load CourseMembershipVO records by User Id's.
	const GET_BY_COURSE_AND_USER_ID = 6; //MembershipFilter.filterType = 6 will load CourseMembershipVO records by course and user Id's.
	const GET_BY_COURSE_AND_ROLER	= 7; //MembershipFilter.filterType = 7 will load CourseMembershipVO records by course Id's and role Identifier.
	
	
	/* (non-PHPdoc)
	 * @see BlackBoard_VO::toBBXml()
	 */
	public function toBBXml($setanull = true){
		if(!$this->getFilterType()){
			throw new BlackBoard_Exception('A propriedade FilterType precisa conter o valor referente ao tipo de consulta');
		}
		return parent::toBBXml($setanull);
	}
	
	/**
	 * @return the $_xmlNodeName
	 */
	public function getXmlNodeName() {
		return "filter";
	}

	/**
	 * @return the $_xml_xmlns
	 */
	public function getXmlns() {
		return "http://coursemembership.ws.blackboard/xsd";
	}

	
	/**
	 * @var int
	 *
	 */
	public $filterType = null;
	
	
	/**
	 * This is one of the defined types in this class.
	const GET_BY_ID 				= 1; //MembershipFilter.filterType = 1 will load CourseMembershipVO records by Id's.
	const GET_BY_COURSE_ID  		= 2; //MembershipFilter.filterType = 2 will load CourseMembershipVO records by course Id's.
	const GET_BY_USER_ID 			= 5; //MembershipFilter.filterType = 5 will load CourseMembershipVO records by User Id's.
	const GET_BY_COURSE_AND_USER_ID = 6; //MembershipFilter.filterType = 6 will load CourseMembershipVO records by course and user Id's.
	const GET_BY_COURSE_AND_ROLER	= 7; //MembershipFilter.filterType = 7 will load CourseMembershipVO records by course Id's and role Identifier.
	 *
	 * @param number $filterType
	 */
	public function setFilterType($filterType) {
		$this->filterType = $filterType;
	}
	
	/**
	 * @return the $filterType
	 */
	public function getFilterType() {
		return $this->filterType;
	}
	
	
	/**
	 * @var array
	 */
	public $courseMembershipIds = array();
	public $groupMembershipIds = array();
	public $courseIds = array();
	public $groupIds = array();
	public $roleIds = array();
	public $userIds = array();
	/**
	 * @return the $courseMembershipIds
	 */
	public function getcourseMembershipIds() {
		return $this->courseMembershipIds;
	}

	public function getGroupMembershipIds() {
		return $this->groupMembershipIds;
	}

	/**
	 * @return the $courseIds
	 */
	public function getCourseIds() {
		return $this->courseIds;
	}

	/**
	 * @return the $groupIds
	 */
	public function getGroupIds() {
		return $this->groupIds;
	}

	/**
	 * @return the $roleIds
	 */
	public function getRoleIds() {
		return $this->roleIds;
	}

	/**
	 * @return the $userIds
	 */
	public function getUserIds() {
		return $this->userIds;
	}

	/**
	 * 
	 * Setar o ID dependendo do Filtro 
	 * Sets the course membership Id's for this filter.
	 * Sets the group membership Id's for this filter.
	 * @param multitype: $courseMembershipIds
	 */
	public function setcourseMembershipIds(array $courseMembershipIds) {
		$this->courseMembershipIds = $courseMembershipIds;
		return $this;
	}

	public function setGroupMembershipIds(array $groupMembershipIds) {
		$this->groupMembershipIds = $groupMembershipIds;
		return $this;
	}

	/**
	 * @param multitype: $courseIds
	 */
	public function setCourseIds(array $courseIds) {
		$this->courseIds = $courseIds;
		return $this;
	}

	/**
	 * @param multitype: $groupIds
	 */
	public function setGroupIds(array $groupIds) {
		$this->groupIds = $groupIds;
		return $this;
	}

	/**
	 * @param multitype: $roleIds
	 */
	public function setRoleIds(array $roleIds) {
		$this->roleIds = $roleIds;
		return $this;
	}

	/**
	 * @param multitype: $userIds
	 */
	public function setUserIds(array $userIds) {
		$this->userIds = $userIds;
		return $this;
	}

	
	

}


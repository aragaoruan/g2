<?php
/**
 * BlackBoard Web Services e Snapshot Flat File para PHP
 *
 * Classes para Consumo dos serviços do BlackBoard
 * Desenvolvido com o estudo do "BbPHP: Blackboard Web Services Library for PHP - Copyright (C) 2011 by St. Edward's University (www.stedwards.edu) - @author johns"
 *
 * As classes e métodos foram refeitos para se ajustar a necessidade de consumir os dois tipos de serviços
 *  1 - Web Services
 *  2 - Snapshot Flat File
 *
 *
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * 
 * file:///D:/Zend/Apache2/htdocs/g2lite/application/webservices/clients/BlackBoard/Doc/CourseMembership.WS/index.html
 *
 */
class BlackBoard_VO_GroupMembershipVO extends BlackBoard_VO {

	/**
	 * @return the $_xmlNodeName
	 */
	public function getXmlNodeName() {
		return "ns3:gmArray";
	}

	/**
	 * @return the $_xml_xmlns
	 */
	public function getXmlns() {
		return "http://coursemembership.ws.blackboard/xsd";
	}
	
	public $courseMembershipId;
// 	public $expansionData = array();  Expansion data is currently ignored.
	public $groupId;
	public $groupMembershipId;
	
	
	/**
	 * @return the $courseMembershipId
	 */
	public function getCourseMembershipId() {
		return $this->courseMembershipId;
	}

	/**
	 * @return the $groupId
	 */
	public function getGroupId() {
		return $this->groupId;
	}

	/**
	 * @return the $groupMembershipId
	 */
	public function getGroupMembershipId() {
		return $this->groupMembershipId;
	}

	/**
	 * 
	 * Sets the course membership Id value for this GroupMembership.
	 * Parameters: 
	 * 		courseMembershipId - the course membership Id value to use for this group membership
	 * 
	 * @param field_type $courseMembershipId
	 */
	public function setCourseMembershipId($courseMembershipId) {
		$this->courseMembershipId = $courseMembershipId;
		return $this;
	}

	/**
	 * 
	 * Sets the group Id value for this GroupMembership.
	 * Parameters:
	 * 		groupId - the group Id value to use for this group membership
	 * 
	 * @param field_type $groupId
	 */
	public function setGroupId($groupId) {
		$this->groupId = $groupId;
		return $this;
	}

	/**
	 * Sets the group Id value for this GroupMembership.
	 * Parameters:
	 * 		groupMembershipId - the group Id value to use for this group membership
	 * 
	 * 
	 * @param field_type $groupMembershipId
	 */
	public function setGroupMembershipId($groupMembershipId) {
		$this->groupMembershipId = $groupMembershipId;
		return $this;
	}

	
	
	
	
	
	
}


<?php
/**
 * BlackBoard Web Services e Snapshot Flat File para PHP
 *
 * Classes para Consumo dos serviços do BlackBoard
 * Desenvolvido com o estudo do "BbPHP: Blackboard Web Services Library for PHP - Copyright (C) 2011 by St. Edward's University (www.stedwards.edu) - @author johns"
 *
 * As classes e métodos foram refeitos para se ajustar a necessidade de consumir os dois tipos de serviços
 *  1 - Web Services
 *  2 - Snapshot Flat File
 *
 *
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * 
 * /BlackBoard/Doc/Course.WS/blackboard/ws/course/CourseFilter.html
 */
class BlackBoard_VO_CourseFilterVO extends BlackBoard_VO {

	const GET_ALL 						= 0; //0 - Load all (course|org)
	const GET_COURSE_BY_COURSE_ID  		= 1; //1 - Load by courseids and (course|org) flag
	const GET_COURSE_BY_BATCH_ID 		= 2; //2 - Load by batchUids and (course|org) flag
	const GET_COURSE_BY_ID 				= 3; //3 - Load by ids and (course|org) flag
	const GET_COURSE_BY_CATEGORY_ID 	= 4; //4 - Load by categoryids and (course|org) flag
	const GET_COURSE_BY_SEARCH_VALUES 	= 5; //5 - Load by searchkey,searchoperator,searchvalue,searchdate,searchdateoperator and (course|org) flag

	
	/* (non-PHPdoc)
	 * @see BlackBoard_VO::toBBXml()
	 */
	public function toBBXml($setanull = true){
		if(!$this->getFilterType()){
			throw new BlackBoard_Exception('A propriedade FilterType precisa conter o valor referente ao tipo de consulta');
		}
		return parent::toBBXml($setanull);
	}
	
	/**
	 * @return the $_xmlNodeName
	 */
	public function getXmlNodeName() {
		return "filter";
	}

	/**
	 * @return the $_xml_xmlns
	 */
	public function getXmlns() {
		return "http://course.ws.blackboard/xsd";
	}

	
	/**
	 * @var int
	 *
	 */
	public $filterType = null;
	
	
	/**
	 * This is one of the defined types in this class.
	 *  const GET_ALL 						= 0; //0 - Load all (course|org)
	 * 	const GET_COURSE_BY_COURSE_ID  		= 1; //1 - Load by courseids and (course|org) flag
	 *	const GET_COURSE_BY_BATCH_ID 		= 2; //2 - Load by batchUids and (course|org) flag
	 *	const GET_COURSE_BY_ID 				= 3; //3 - Load by ids and (course|org) flag
	 *	const GET_COURSE_BY_CATEGORY_ID 	= 4; //4 - Load by categoryids and (course|org) flag
	 *	const GET_COURSE_BY_SEARCH_VALUES 	= 5; //5 - Load by searchkey,searchoperator,searchvalue,searchdate,searchdateoperator and (course|org) flag
	 *
	 * @param number $filterType
	 */
	public function setFilterType($filterType) {
		$this->filterType = $filterType;
	}
	
	/**
	 * @return the $filterType
	 */
	public function getFilterType() {
		return $this->filterType;
	}
	
	/**
	 * @var string true | false
	 */
// 	public $available = 'true';
	
	/**
	 * @var array
	 */
	public $batchIds = array();
	
	public $categoryIds = array();
	
	/**
	 * @var array
	 */
	public $courseIds = array();
	
	public $courseTemplates = array();
	
	public $dataSourceIds = array();

// 	/**
// 	 * @var array
// 	 */
// 	public $expansionData = array();

	public $ids = array();
	
	public $searchDate;
	
	public $dateOperator;
	
	public $searchKey;
	
	public $searchOperator;
	
	public $searchValue;
	
	public $sourceBatchUids = array();
	
	public $userIds = array();
	
	
	/**
	 * @return the $available
	 */
// 	public function getAvailable() {
// 		return $this->available;
// 	}

	/**
	 * @return the $batchIds
	 */
	public function getBatchIds() {
		return $this->batchIds;
	}

	/**
	 * @return the $categoryIds
	 */
	public function getCategoryIds() {
		return $this->categoryIds;
	}

	/**
	 * @return the $courseIds
	 */
	public function getCourseIds() {
		return $this->courseIds;
	}

	/**
	 * @return the $courseTemplates
	 */
	public function getCourseTemplates() {
		return $this->courseTemplates;
	}

	/**
	 * @return the $dataSourceIds
	 */
	public function getDataSourceIds() {
		return $this->dataSourceIds;
	}

	/**
	 * @return the $ids
	 */
	public function getIds() {
		return $this->ids;
	}

	/**
	 * @return the $searchDate
	 */
	public function getSearchDate() {
		return $this->searchDate;
	}

	/**
	 * @return the $dateOperator
	 */
	public function getDateOperator() {
		return $this->dateOperator;
	}

	/**
	 * @return the $searchKey
	 */
	public function getSearchKey() {
		return $this->searchKey;
	}

	/**
	 * @return the $searchOperator
	 */
	public function getSearchOperator() {
		return $this->searchOperator;
	}

	/**
	 * @return the $searchValue
	 */
	public function getSearchValue() {
		return $this->searchValue;
	}

	/**
	 * @return the $sourceBatchUids
	 */
	public function getSourceBatchUids() {
		return $this->sourceBatchUids;
	}

	/**
	 * @return the $userIds
	 */
	public function getUserIds() {
		return $this->userIds;
	}

	/**
	 * set avaiable as search criteria, 0:false/1:true/2:all NOTE: This field is currently not used - reserved for future use.
	 * @param string $available
	 */
// 	public function setAvailable($available) {
// 		$this->available = $available;
// 		return $this;
// 	}

	/**
	 * set course batch uid array as search criteria
	 * @param multitype: $batchIds
	 */
	public function setBatchIds( array $batchIds) {
		$this->batchIds = $batchIds;
		return $this;
	}

	/**
	 * Set to the categories you are searching for courses in
	 * @param multitype: $categoryIds
	 */
	public function setCategoryIds( array $categoryIds) {
		$this->categoryIds = $categoryIds;
		return $this;
	}

	/**
	 * set course course_id array as search criteria (Note - course ids are the string entered in the UI)
	 * @param multitype: $courseIds
	 */
	public function setCourseIds( array $courseIds) {
		$this->courseIds = $courseIds;
		return $this;
	}

	/**
	 * set course template array as search criteria NOTE: This field is currently not used - reserved for future use.
	 * @param multitype: $courseTemplates
	 */
	public function setCourseTemplates( array $courseTemplates) {
		$this->courseTemplates = $courseTemplates;
		return $this;
	}

	/**
	 * set course data source id array as search criteria NOTE: This field is currently not used - reserved for future use.
	 * @param multitype: $dataSourceIds
	 */
	public function setDataSourceIds( array $dataSourceIds) {
		$this->dataSourceIds = $dataSourceIds;
		return $this;
	}

	/**
	 * set course id array as search criteria (Note: these ids are the internal ids generated by the server)
	 * @param multitype: $ids
	 */
	public function setIds( array $ids) {
		$this->ids = $ids;
		return $this;
	}

	/**
	 * Only used with filterType=5 The creation date to compare against.
	 * @param field_type $searchDate
	 */
	public function setSearchDate($searchDate) {
		$this->searchDate = $searchDate;
		return $this;
	}

	/**
	 * Only used with filterType=5 The comparison operator to use for the creation date.
	 * @param field_type $dateOperator
	 */
	public function setDateOperator($dateOperator) {
		$this->dateOperator = $dateOperator;
		return $this;
	}

	/**
	 * Only used with filterType=5 The searchKey is the field in which you are searching for the searchValue 
	 * with the given searchOperator matching criteria Possible values are: CourseId CourseName CourseDescription CourseInstructor 
	 * @param field_type $searchKey
	 */
	public function setSearchKey($searchKey) {
		$this->searchKey = $searchKey;
		return $this;
	}

	/**
	 * Only used with filterType=5 The searchOperator describes how to compare the searchValue to the CourseVO 
	 * values for the given searchKey Possible values are: Equals Contains StartsWith IsNotBlank 
	 * @param field_type $searchOperator
	 */
	public function setSearchOperator($searchOperator) {
		$this->searchOperator = $searchOperator;
		return $this;
	}

	/**
	 * Only used with filterType=5 The value you are searching for.
	 * @param field_type $searchValue
	 */
	public function setSearchValue($searchValue) {
		$this->searchValue = $searchValue;
		return $this;
	}

	/**
	 * set course's datasource's batch uid array as search criteria NOTE: This field is currently not used - reserved for future use.
	 * @param multitype: $sourceBatchUids
	 */
	public function setSourceBatchUids( array $sourceBatchUids) {
		$this->sourceBatchUids = $sourceBatchUids;
		return $this;
	}

	/**
	 * set course user id array as search criteria, the user is enrolled in the course.
	 * @param multitype: $userIds
	 */
	public function setUserIds( array $userIds) {
		$this->userIds = $userIds;
		return $this;
	}


}


<?php
/**
 * BlackBoard Web Services e Snapshot Flat File para PHP
 *
 * Classes para Consumo dos serviços do BlackBoard
 * Desenvolvido com o estudo do "BbPHP: Blackboard Web Services Library for PHP - Copyright (C) 2011 by St. Edward's University (www.stedwards.edu) - @author johns"
 *
 * As classes e métodos foram refeitos para se ajustar a necessidade de consumir os dois tipos de serviços
 *  1 - Web Services
 *  2 - Snapshot Flat File
 *
 *
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * 
 * /BlackBoard/Doc/Course.WS/blackboard/ws/course/GroupFilter.html
 */
class BlackBoard_VO_GroupFilterVO extends BlackBoard_VO {

// 	const GET_GROUP_BY_ID				= 0;
	const GET_GROUP_BY_COURSE_ID		= 2;
// 	const GET_ENROLLED_GROUP_BY_USER_ID = 2;
	
	/* (non-PHPdoc)
	 * @see BlackBoard_VO::toBBXml()
	 */
	public function toBBXml($setanull = true){
		if(!$this->getFilterType()){
			throw new BlackBoard_Exception('A propriedade FilterType precisa conter o valor referente ao tipo de consulta');
		}
		return parent::toBBXml($setanull);
	}
	
	/**
	 * @return the $_xmlNodeName
	 */
	public function getXmlNodeName() {
		return "filter";
	}

	/**
	 * @return the $_xml_xmlns
	 */
	public function getXmlns() {
		return "http://course.ws.blackboard/xsd";
	}

	
	/**
	 * @var int
	 *
	 */
	public $filterType = null;
	
	
	/**
	 * This is one of the defined types in this class.
	 * 
	 *  	const GET_GROUP_BY_ID				= 0;
	 *  	const GET_GROUP_BY_COURSE_ID		= 1;
	 *  	const GET_ENROLLED_GROUP_BY_USER_ID = 2;
	 *
	 * @param number $filterType
	 */
	public function setFilterType($filterType) {
		$this->filterType = $filterType;
	}
	
	/**
	 * @return the $filterType
	 */
	public function getFilterType() {
		return $this->filterType;
	}
	
	/**
	 * @var string true | false
	 */
	public $available = 'true';
	
	/**
	 * @var array
	 */
	public $groupIds = array();
	
	public $userIds = array();
	
	
	/**
	 * @return the $available
	 */
	public function getAvailable() {
		return $this->available;
	}

	/**
	 * @return the $groupIds
	 */
	public function getGroupIds() {
		return $this->groupIds;
	}

	/**
	 * @return the $userIds
	 */
	public function getUserIds() {
		return $this->userIds;
	}

	/**
	 * @param string $available
	 */
	public function setAvailable($available) {
		$this->available = $available;
		return $this;
	}

	/**
	 * @param multitype: $groupIds
	 */
	public function setGroupIds(array $groupIds) {
		$this->groupIds = $groupIds;
		return $this;
	}

	/**
	 * @param multitype: $userIds
	 */
	public function setUserIds(array $userIds) {
		$this->userIds = $userIds;
		return $this;
	}

}


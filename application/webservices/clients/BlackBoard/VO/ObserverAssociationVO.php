<?php
/**
 * BlackBoard Web Services e Snapshot Flat File para PHP
 *
 * Classes para Consumo dos serviços do BlackBoard
 * Desenvolvido com o estudo do "BbPHP: Blackboard Web Services Library for PHP - Copyright (C) 2011 by St. Edward's University (www.stedwards.edu) - @author johns"
 *
 * As classes e métodos foram refeitos para se ajustar a necessidade de consumir os dois tipos de serviços
 *  1 - Web Services
 *  2 - Snapshot Flat File
 *
 *
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 *
 */
class BlackBoard_VO_ObserverAssociationVO extends BlackBoard_VO {

	public $observeeId;
	public $observerId = array();
	
	
	/**
	 * @return the $_xmlNodeName
	 */
	public function getXmlNodeName() {
		return "user";
	}
	
	/**
	 * @return the $_xml_xmlns
	 */
	public function getXmlns() {
		return "http://user.ws.blackboard/xsd";
	}
	
	
	/**
	 * @return the $observeeId
	 */
	public function getObserveeId() {
		return $this->observeeId;
	}

	/**
	 * @return the $observerId
	 */
	public function getObserverId() {
		return $this->observerId;
	}

	/**
	 * set observee users to observer user in the ObserverAssociationVO
	 * @param field_type $observeeId
	 */
	public function setObserveeId($observeeId) {
		$this->observeeId = $observeeId;
		return $this;
	}

	/**
	 * set observer for the ObserverAssociationVO
	 * @param multitype: $observerId
	 */
	public function setObserverId(array $observerId = null) {
		$this->observerId = $observerId;
		return $this;
	}



	
}


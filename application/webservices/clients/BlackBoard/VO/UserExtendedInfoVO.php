<?php
/**
 * BlackBoard Web Services e Snapshot Flat File para PHP
 *
 * Classes para Consumo dos serviços do BlackBoard
 * Desenvolvido com o estudo do "BbPHP: Blackboard Web Services Library for PHP - Copyright (C) 2011 by St. Edward's University (www.stedwards.edu) - @author johns"
 *
 * As classes e métodos foram refeitos para se ajustar a necessidade de consumir os dois tipos de serviços
 *  1 - Web Services
 *  2 - Snapshot Flat File
 *
 *
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 *
 */
class BlackBoard_VO_UserExtendedInfoVO extends BlackBoard_VO {

	
	/**
	 * @return the $_xmlNodeName
	 */
	public function getXmlNodeName() {
		return "extendedInfo";
	}
	
	/**
	 * @return the $_xml_xmlns
	 */
	public function getXmlns() {
		return "http://user.ws.blackboard/xsd";
	}
	
	public $businessFax;

	public $businessPhone1;

	public $businessPhone2;

	public $city;

	public $company;

	public $country;

	public $department;

	/**
	 * E-mail do usu�rio no G2
	 * @var string
	 */
	public $emailAddress;

	public $expansionData;

	/**
	 * Salvar o Nome Completo do Usu�rio
	 * @var string
	 */
	public $familyName;

	/**
	 * Salvar um ponto .
	 * @var string
	 */
	public $givenName = '.';

	public $homeFax;

	public $homePhone1;

	public $homePhone2;

	public $jobTitle;

	public $middleName;

	public $mobilePhone;

	public $state;

	public $street1;

	public $street2;

	public $webPage;

	public $zipCode;
	/**
	 * @return the $businessFax
	 */
	public function getBusinessFax() {
		return $this->businessFax;
	}

	/**
	 * @return the $businessPhone1
	 */
	public function getBusinessPhone1() {
		return $this->businessPhone1;
	}

	/**
	 * @return the $businessPhone2
	 */
	public function getBusinessPhone2() {
		return $this->businessPhone2;
	}

	/**
	 * @return the $city
	 */
	public function getCity() {
		return $this->city;
	}

	/**
	 * @return the $company
	 */
	public function getCompany() {
		return $this->company;
	}

	/**
	 * @return the $country
	 */
	public function getCountry() {
		return $this->country;
	}

	/**
	 * @return the $department
	 */
	public function getDepartment() {
		return $this->department;
	}

	/**
	 * @return the $emailAddress
	 */
	public function getEmailAddress() {
		return $this->emailAddress;
	}

	/**
	 * @return the $expansionData
	 */
	public function getExpansionData() {
		return $this->expansionData;
	}

	/**
	 * @return the $familyName
	 */
	public function getFamilyName() {
		return $this->familyName;
	}

	/**
	 * @return the $givenName
	 */
	public function getGivenName() {
		return $this->givenName;
	}

	/**
	 * @return the $homeFax
	 */
	public function getHomeFax() {
		return $this->homeFax;
	}

	/**
	 * @return the $homePhone1
	 */
	public function getHomePhone1() {
		return $this->homePhone1;
	}

	/**
	 * @return the $homePhone2
	 */
	public function getHomePhone2() {
		return $this->homePhone2;
	}

	/**
	 * @return the $jobTitle
	 */
	public function getJobTitle() {
		return $this->jobTitle;
	}

	/**
	 * @return the $middleName
	 */
	public function getMiddleName() {
		return $this->middleName;
	}

	/**
	 * @return the $mobilePhone
	 */
	public function getMobilePhone() {
		return $this->mobilePhone;
	}

	/**
	 * @return the $state
	 */
	public function getState() {
		return $this->state;
	}

	/**
	 * @return the $street1
	 */
	public function getStreet1() {
		return $this->street1;
	}

	/**
	 * @return the $street2
	 */
	public function getStreet2() {
		return $this->street2;
	}

	/**
	 * @return the $webPage
	 */
	public function getWebPage() {
		return $this->webPage;
	}

	/**
	 * @return the $zipCode
	 */
	public function getZipCode() {
		return $this->zipCode;
	}

	/**
	 * @param field_type $businessFax
	 */
	public function setBusinessFax($businessFax) {
		$this->businessFax = $businessFax;
	}

	/**
	 * @param field_type $businessPhone1
	 */
	public function setBusinessPhone1($businessPhone1) {
		$this->businessPhone1 = $businessPhone1;
	}

	/**
	 * @param field_type $businessPhone2
	 */
	public function setBusinessPhone2($businessPhone2) {
		$this->businessPhone2 = $businessPhone2;
	}

	/**
	 * @param field_type $city
	 */
	public function setCity($city) {
		$this->city = $city;
	}

	/**
	 * @param field_type $company
	 */
	public function setCompany($company) {
		$this->company = $company;
	}

	/**
	 * @param field_type $country
	 */
	public function setCountry($country) {
		$this->country = $country;
	}

	/**
	 * @param field_type $department
	 */
	public function setDepartment($department) {
		$this->department = $department;
	}

	/**
	 * @param string $emailAddress
	 */
	public function setEmailAddress($emailAddress) {
		$this->emailAddress = $emailAddress;
	}

	/**
	 * @param field_type $expansionData
	 */
	public function setExpansionData($expansionData) {
		$this->expansionData = $expansionData;
	}

	/**
	 * @param string $familyName
	 */
	public function setFamilyName($familyName) {
		$this->familyName = $familyName;
	}

	/**
	 * @param string $givenName
	 */
	public function setGivenName($givenName) {
		$this->givenName = $givenName;
	}

	/**
	 * @param field_type $homeFax
	 */
	public function setHomeFax($homeFax) {
		$this->homeFax = $homeFax;
	}

	/**
	 * @param field_type $homePhone1
	 */
	public function setHomePhone1($homePhone1) {
		$this->homePhone1 = $homePhone1;
	}

	/**
	 * @param field_type $homePhone2
	 */
	public function setHomePhone2($homePhone2) {
		$this->homePhone2 = $homePhone2;
	}

	/**
	 * @param field_type $jobTitle
	 */
	public function setJobTitle($jobTitle) {
		$this->jobTitle = $jobTitle;
	}

	/**
	 * @param field_type $middleName
	 */
	public function setMiddleName($middleName) {
		$this->middleName = $middleName;
	}

	/**
	 * @param field_type $mobilePhone
	 */
	public function setMobilePhone($mobilePhone) {
		$this->mobilePhone = $mobilePhone;
	}

	/**
	 * @param field_type $state
	 */
	public function setState($state) {
		$this->state = $state;
	}

	/**
	 * @param field_type $street1
	 */
	public function setStreet1($street1) {
		$this->street1 = $street1;
	}

	/**
	 * @param field_type $street2
	 */
	public function setStreet2($street2) {
		$this->street2 = $street2;
	}

	/**
	 * @param field_type $webPage
	 */
	public function setWebPage($webPage) {
		$this->webPage = $webPage;
	}

	/**
	 * @param field_type $zipCode
	 */
	public function setZipCode($zipCode) {
		$this->zipCode = $zipCode;
	}

	
	
}


<?php
/**
 * BlackBoard Web Services e Snapshot Flat File para PHP
 *
 * Classes para Consumo dos serviços do BlackBoard
 * Desenvolvido com o estudo do "BbPHP: Blackboard Web Services Library for PHP - Copyright (C) 2011 by St. Edward's University (www.stedwards.edu) - @author johns"
 *
 * As classes e métodos foram refeitos para se ajustar a necessidade de consumir os dois tipos de serviços
 *  1 - Web Services
 *  2 - Snapshot Flat File
 *
 *
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 *
 */
class BlackBoard_VO_OrganizationFileVO extends BlackBoard_VO {

	/**
	 * @return the $_xmlNodeName
	 */
	public function getXmlNodeName() {
	}

	/**
	 * @return the $_xml_xmlns
	 */
	public function getXmlns() {
	}
	
	public function getSnapShotStoreUrl(){
		return "https://unyleya.blackboard.com/webapps/bb-data-integration-flatfile-BBLEARN/endpoint/organization/store";
	}
	
	public $external_organization_key;
	public $organization_id;
	public $organization_name;
	public $available_ind;
	public $row_status;
	public $duration;
	public $start_date;
	public $end_date;
	public $days_of_use;
	public $template_organization_key;
	
	
	/**
	 * @return the $external_organization_key
	 */
	public function getExternal_organization_key() {
		return $this->external_organization_key;
	}

	/**
	 * @return the $organization_id
	 */
	public function getOrganization_id() {
		return $this->organization_id;
	}

	/**
	 * @return the $organization_name
	 */
	public function getOrganization_name() {
		return $this->organization_name;
	}

	/**
	 * @return the $available_ind
	 */
	public function getAvailable_ind() {
		return $this->available_ind;
	}

	/**
	 * @return the $row_status
	 */
	public function getRow_status() {
		return $this->row_status;
	}

	/**
	 * @return the $duration
	 */
	public function getDuration() {
		return $this->duration;
	}

	/**
	 * @return the $start_date
	 */
	public function getStart_date() {
		return $this->start_date;
	}

	/**
	 * @return the $end_date
	 */
	public function getEnd_date() {
		return $this->end_date;
	}

	/**
	 * @return the $days_of_use
	 */
	public function getDays_of_use() {
		return $this->days_of_use;
	}

	/**
	 * @return the $template_organization_key
	 */
	public function getTemplate_organization_key() {
		return $this->template_organization_key;
	}

	/**
	 * Internal identifier used to uniquely identify the organization section. (e.g.,math101.1.Fall2002). This is NOT displayed 
	 * to users. Can contain only letters, digits, dashes and periods. No spaces or other punctuation allowed
	 * 
	 * Required
	 * Unique
	 * 
	 * Código interno do G2
	 * @param field_type $external_organization_key
	 */
	public function setExternal_organization_key($external_organization_key) {
		$this->external_organization_key = $external_organization_key;
	}

	/**
	 * Short identifier. Can contain only letters, digits, dashes and periods. No spaces or other punctuation ()&/’+” allowed. Displayed
	 * on organizations tab and organization catalog. Organization ID is immutable. The Snapshot tool will not allow updating of a Organization ID.
	 * 
	 * Required
	 * Unique
	 * 
	 * Código interno do G2
	 * @param field_type $organization_id
	 */
	public function setOrganization_id($organization_id) {
		$this->organization_id = $organization_id;
	}

	/**
	 * Primary visual identifier for the organization. Displayed in portal module, organizations tab and breadcrumb. 
	 * 
	 * Required
	 * 
	 * Nome da Sala de Aula (st_saladeaula)
	 * @param field_type $organization_name
	 */
	public function setOrganization_name($organization_name) {
		$this->organization_name = $organization_name;
	}

	/**
	 * Establishes organization availability. Values may be “Y” for yes, or “N” for no. If null, the default is “Y” 
	 * 
	 * Situação (ATIVO, INATIVO, ABERTA)
	 * ATIVO 	= N
	 * INATIVO 	= N
	 * ABERTA 	= Y
	 * 
	 * @param field_type $available_ind
	 */
	public function setAvailable_ind($available_ind) {
		$this->available_ind = $available_ind;
	}

	/**
	 * Sets the value of the record to one of the following:
	 * - enabled: Normal access
	 * - disabled: Record is visible in some areas of the UI, but may not be changed or accessed.
	 * - deleted: deprecated, don’t use. If null, default is enabled
	 * – NO GUI ACCESS
	 * 
	 * DISABLED : AO EXCLUIR DO GESTOR 2
	 * ENABLED: TODAS AS OUTROS CENÁRIOS
	 * 
	 * @param field_type $row_status
	 */
	public function setRow_status($row_status) {
		$this->row_status = $row_status;
	}

	/**
	 * Determines access control for enrolled users.
	 * 
	 * continuous: The organization is always acc essible.
	 * 
	 * range: The organization is accessible in the days between one date and another. Either the
	 * start date or the end date can be left open-ended to make a organization accessible from a
	 * certain date or until a certain date. 
	 * 
	 * fixed: The organization is accessible for a set number of days. If null, default is continuous
	 * 
	 * Continous: TDC disciplina/professor
	 * Range: IMP – Sala TCC/MCC por Programa/Curso
	 * 
	 * @param field_type $duration
	 */
	public function setDuration($duration) {
		$this->duration = $duration;
	}

	/**
	 * If DURATION = range , date on which access is granted to enrolled users.
	 * 
	 * Data de Início do G2
	 * date(yyyymmdd)
	 * @param field_type $start_date
	 */
	public function setStart_date(DateTime $start_date) {
		$this->start_date = $start_date;
	}

	/**
	 * If DURATION = range , date on which access is revoked from enrolled users.
	 * 
	 * Data de Fim do G2
	 * date(yyyymmdd)
	 * @param field_type $end_date
	 */
	public function setEnd_date(DateTime $end_date) {
		$this->end_date = $end_date;
	}

	/**
	 * 
	 * If duration = fixed, number of days that students may access the organization after enrolling
	 * 
	 * Número de dias que os alunos podem acessar o curso após a inscrição
	 * 
	 * numeric
	 * Quantidade de Dias de Acesso do G2
	 * 
	 * @param field_type $days_of_use
	 */
	public function setDays_of_use($days_of_use) {
		$this->days_of_use = $days_of_use;
	}

	/**
	 * 
	 * Designates source for copy operation. EXTERNAL_COURSE_KEY for originating organization web site.
	 * 
	 * 
	 * Código da Sala Modelo no G2. Equipe de administradores 
	 * Blackboard criará as disciplinas modelo na plataforma.
	 * @param field_type $template_organization_key
	 */
	public function setTemplate_organization_key($template_organization_key) {
		$this->template_organization_key = $template_organization_key;
	}

	
}


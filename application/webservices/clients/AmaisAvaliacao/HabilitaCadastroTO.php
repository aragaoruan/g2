<?php

/**
 * TO para os campos para habilitar o aluno no sistema Amais Avaliaçoes
 * Identificador do serviço no A+ avaliação: habilitarAluno
 * Retorno do A+ Avaliações: Mensagem de erro. Nula caso tenha processado com sucesso.
 *
 * @author    Neemias Santos <neemiassantos.16@gmail.com>
 */
class AmaisAvaliacao_HabilitaCadastroTO extends Ead1_TO_Dinamico
{
    /**
     * e-mail do avaliado
     *
     * @var
     */
    public $email;
    /**
     * CPF do avaliado
     *
     * @var
     */
    public $cpf;
    /**
     * Nome do avaliado
     *
     * @var
     */
    public $nome;
    /**
     * Matrícula do avaliado
     *
     * @var
     */
    public $matricula;
    /**
     * Início da vigência no ambiente de estudos
     *
     * @var
     */
    public $codigoPlanoSistemaOrigem;
    /**
     * Identificador do avaliado no sistema externo
     *
     * @var
     */
    public $codigoAlunoSistemaOrigem;
    /**
     * Cidade do avaliado
     *
     * @var
     */
    public $cidade;
    /**
     * Unidade federativa do avaliado
     *
     * @var
     */
    public $uf;
    /**
     * nome da turma
     *
     * @var
     */
    public $nomeTurma;
    /**
     * Um número que identifique a turma no sistema externo
     *
     * @var
     */
    public $codigoTurmaSistemaOrigem;
    /**
     * Token identificador da aplicação da prova no A+
     *
     * @var
     */
    public $identificadorAplicacaoProva;
    /**
     * Se o usuário deve estar ativo no A+
     *
     * @var
     */
    public $isAtivo;
    /**
     * Senha a ser utilizada caso o avaliado precise acessar o A+
     *
     * @var
     */
    public $senha;

    /**
     * @var
     */
    public $identificadorAplicadorProva;

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return habilitaCadastroTO
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * @param mixed $cpf
     * @return habilitaCadastroTO
     */
    public function setCpf($cpf)
    {
        $this->cpf = $cpf;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     * @return habilitaCadastroTO
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMatricula()
    {
        return $this->matricula;
    }

    /**
     * @param mixed $matricula
     * @return habilitaCadastroTO
     */
    public function setMatricula($matricula)
    {
        $this->matricula = $matricula;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCodigoPlanoSistemaOrigem()
    {
        return $this->codigoPlanoSistemaOrigem;
    }

    /**
     * @param mixed $codigoPlanoSistemaOrigem
     * @return habilitaCadastroTO
     */
    public function setCodigoPlanoSistemaOrigem($codigoPlanoSistemaOrigem)
    {
        $this->codigoPlanoSistemaOrigem = $codigoPlanoSistemaOrigem;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCodigoAlunoSistemaOrigem()
    {
        return $this->codigoAlunoSistemaOrigem;
    }

    /**
     * @param mixed $codigoAlunoSistemaOrigem
     * @return habilitaCadastroTO
     */
    public function setCodigoAlunoSistemaOrigem($codigoAlunoSistemaOrigem)
    {
        $this->codigoAlunoSistemaOrigem = $codigoAlunoSistemaOrigem;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCidade()
    {
        return $this->cidade;
    }

    /**
     * @param mixed $cidade
     * @return habilitaCadastroTO
     */
    public function setCidade($cidade)
    {
        $this->cidade = $cidade;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUf()
    {
        return $this->uf;
    }

    /**
     * @param mixed $uf
     * @return habilitaCadastroTO
     */
    public function setUf($uf)
    {
        $this->uf = $uf;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNomeTurma()
    {
        return $this->nomeTurma;
    }

    /**
     * @param mixed $nomeTurma
     * @return habilitaCadastroTO
     */
    public function setNomeTurma($nomeTurma)
    {
        $this->nomeTurma = $nomeTurma;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCodigoTurmaSistemaOrigem()
    {
        return $this->codigoTurmaSistemaOrigem;
    }

    /**
     * @param mixed $codigoTurmaSistemaOrigem
     * @return habilitaCadastroTO
     */
    public function setCodigoTurmaSistemaOrigem($codigoTurmaSistemaOrigem)
    {
        $this->codigoTurmaSistemaOrigem = $codigoTurmaSistemaOrigem;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdentificadorAplicacaoProva()
    {
        return $this->identificadorAplicacaoProva;
    }

    /**
     * @param mixed $identificadorAplicacaoProva
     * @return habilitaCadastroTO
     */
    public function setIdentificadorAplicacaoProva($identificadorAplicacaoProva)
    {
        $this->identificadorAplicacaoProva = $identificadorAplicacaoProva;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsAtivo()
    {
        return $this->isAtivo;
    }

    /**
     * @param mixed $isAtivo
     * @return habilitaCadastroTO
     */
    public function setIsAtivo($isAtivo)
    {
        $this->isAtivo = $isAtivo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSenha()
    {
        return $this->senha;
    }

    /**
     * @param mixed $senha
     * @return habilitaCadastroTO
     */
    public function setSenha($senha)
    {
        $this->senha = $senha;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdentificadorAplicadorProva()
    {
        return $this->identificadorAplicadorProva;
    }

    /**
     * @param mixed $identificadorAplicadorProva
     * @return AmaisAvaliacao_HabilitaCadastroTO
     */
    public function setIdentificadorAplicadorProva($identificadorAplicadorProva)
    {
        $this->identificadorAplicadorProva = $identificadorAplicadorProva;
        return $this;
    }

}



<?php

/**
 * Aluno, gerencia o envio dos alunos para o websevice da A+ Avaliações
 * Cadastro, atualização de dados
 *
 * @author    Neemias Santos <neemiassantos.16@gmail.com>
 */
class AmaisAvaliacao_HabilitarAluno extends WsBO
{

    public function cadastroAluno(AmaisAvaliacao_HabilitaCadastroTO $aluno)
    {
        try {
            //encapsula os valores recebidos no padrao para envio
            $params[] = new SoapVar($aluno->toArray(), SOAP_ENC_OBJECT, NULL, NULL, 'arg0');

            //Coloca os valores dentro de uma outra chave para enviar os valores corretamente
            $parametros = new SoapVar($params, SOAP_ENC_OBJECT, NULL, NULL, 'habilitarAluno');

            //cria a conexao com o webservice
            $client = new Zend_Soap_Client(\G2\Constante\AmaisAvaliacao::URLWSDL);

            //invoca os poderes do metodo de habilitar os alunos enviando os paramestros recebidos
            $client->habilitarAluno($parametros);

            /*
             * O retorno do webservice Amais Avaliação é NULL para sucesso.
             * Caso ele tenha dado sucesso retorna true para salvar o aluno na tb_usuariointegracao
             */
            if ($client->getLastResponse() == '') {
                return true;
            }

        } catch (SoapFault $s) {
            die('ERROR: [' . $s->faultcode . '] ' . $s->faultstring);
        } catch (Exception $e) {
            die('ERROR: ' . $e->getMessage());
        }
    }
}
<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    /**
     * Método que inicializa o MVC e a sessão
     */
    protected function _initBasics()
    {
//        Zend_Session::start();
        Zend_Layout::startMvc();
        Zend_Layout::getMvcInstance()->disableLayout(true);
    }

    /**
     * Pega a configuração do Zend_View a partir do arquivo de configuraçào
     * Seta a instância para
     */
    protected function _initZendView()
    {
        $this->bootstrap('view');
        $viewResource = $this->getResource('view');
        Zend_Registry::getInstance()->set('view', $viewResource);
    }

    protected function _initFrontControllerOutput()
    {

        $this->bootstrap('FrontController');
        $frontController = $this->getResource('FrontController');

        $response = new Zend_Controller_Response_Http;
        $frontController->setResponse($response);

        return $frontController;
    }

    /**
     * Inicia o cache de banco de dados
     */
    protected function _initCacheDbTable()
    {
        $frontendOptions = array(
//            'lifetime' => (86400 * 7),
            'lifetime' => NULL,
            'automatic_serialization' => true
        );
        $backendOptions = array(
            'cache_dir' => APPLICATION_PATH . '/../public/tmp/'
        );
        $cache = Zend_Cache::factory(
            'Core', 'File', $frontendOptions, $backendOptions
        );
        Zend_Db_Table_Abstract::setDefaultMetadataCache($cache);
    }

    protected function _initLog()
    {

        $options = $this->getOption('resources');
        if (!isset($options["log"])) {
            return false;
        }

        if (file_exists($options["log"]["stream"]["writerParams"]["stream"])
            && @filesize($options["log"]["stream"]["writerParams"]["stream"]) > 1713015) {
            $diretorio = dirname($options["log"]["stream"]["writerParams"]["stream"]);
            rename(
                $options["log"]["stream"]["writerParams"]["stream"],
                $diretorio . DIRECTORY_SEPARATOR . 'application-' . date('d-m-Y-His') . '.log'
            );
            $f = fopen($options["log"]["stream"]["writerParams"]["stream"], 'a');
            fclose($f);
            chmod($options["log"]["stream"]["writerParams"]["stream"], 0777);
        }

        $logger = Zend_Log::factory($options["log"]);
        Zend_Registry::set('logger', $logger);

        return $logger;
    }

//    protected function _initLogger()
//    {
//        $this->bootstrap('frontController');
//
//        if ($this->getEnvironment() == "testing") {
//            $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH . "/../data/logs/app.log");
//            $filter = new Zend_Log_Filter_Priority(Zend_Log::CRIT);
//        }
//
//        // register the Zend_Application_Resource_Log plugin first if you havent done so
//        $this->registerPluginResource('log', array('writer' => $writer));
//
//        // make sure to bootstrap
//        $this->bootstrap('log');
//
//        // retrieve Zend_Log and add the filter
//        $this->_logger = $this->getResource('log')
//            ->addFilter($filter);
//    }

    protected function _initFrontModules()
    {
        $front = Zend_Controller_Front::getInstance();
        $front->setControllerDirectory(
            array(
                'default' => APPLICATION_PATH . '/apps/default/controllers',
                'portal' => APPLICATION_PATH . '/apps/portal/controllers',
                'loja' => APPLICATION_PATH . '/apps/loja/controllers',
                'lite' => APPLICATION_PATH . '/apps/lite/controllers',
                'unico' => APPLICATION_PATH . '/apps/unico/controllers',
                'api' => APPLICATION_PATH . '/apps/api/controllers',
                'app-api' => APPLICATION_PATH . '/apps/appapi/controllers',
                'api-v2' => APPLICATION_PATH . '/apps/apiv2/controllers',
            )
        );
    }

    public function _initAutoloaderNamespaces()
    {
        require_once APPLICATION_PATH . '/../library/vendor/autoload.php';

        $autoloader = \Zend_Loader_Autoloader::getInstance();
        $fmmAutoloader = new \Doctrine\Common\ClassLoader('Bisna');
        $autoloader->pushAutoloader(array($fmmAutoloader, 'loadClass'), 'Bisna');
    }


    protected function _initZFDebug()
    {
//     die($this->getOption('ZFDebug'));
        if ($this->getOption('ZFDebug') == '1') {

            $autoloader = Zend_Loader_Autoloader::getInstance();
            $autoloader->registerNamespace('ZFDebug');

            $em = $this->bootstrap('doctrine')->getResource('doctrine')->getEntityManager();

            $options = array(
                'plugins' => array(
                    'Variables',
                    'ZFDebug_Controller_Plugin_Debug_Plugin_Doctrine2' => array(
                        'entityManagers' => array($em),
                    ),
                    'File' => array('basePath' => APPLICATION_PATH . '/../library'),
                    'Exception',
                    'Html',
                    'Memory',
                    'Time',
                )
            );

            $debug = new ZFDebug_Controller_Plugin_Debug($options);

            $this->bootstrap('frontController');

            $frontController = $this->getResource('frontController');
            $frontController->registerPlugin($debug);

        }
    }

    /**
     * Inicializador de plugins
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     */
    public function _initPlugins()
    {

        $bootstrap = $this->getApplication();

        if ($bootstrap instanceof Zend_Application) {
            $bootstrap = $this;
        }

        $bootstrap->bootstrap('FrontController');
        $front = $bootstrap->getResource('FrontController');

        //$front->registerPlugin(new \G2\Plugins\DownloadSeguro());
        $front->registerPlugin(new \G2\Plugins\GenerateEntities());

    }


    /**
     * Metodo para inicializar o arquivo de databases onde está guardado todas as configurações
     * de base de dados. Foi criado principalmente pela a necessidade de integração com outros BD.
     *
     * Neste método está sendo feita a sobrescrita do parametro do Doctrine para setar a mesma conexão
     * do Zend_DB, evitando deadlocks no banco.
     *
     * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
     * */
    public function _initDatabases()
    {
        $configFile = APPLICATION_PATH . '/configs/databases.ini';

        if (!file_exists($configFile)) {
            return;
        }

        $config = new Zend_Config_Ini($configFile, APPLICATION_ENV);
        $db = $this->getPluginResource('db')->getDbAdapter();
        Zend_Db_Table::setDefaultAdapter($db); //important
        Zend_Registry::set('db', $db);
        $ar = $config->toArray();
        if (isset($ar['resources']['doctrine']['dbal']['connections']['default']['parameters']['pdo'])) {
            $ar['resources']['doctrine']['dbal']['connections']['default']['parameters']['pdo'] = \Zend_Db_Table_Abstract::getDefaultAdapter()->getConnection();
        }
        $this->setOptions($ar);
    }

    /**
     * Método responsável por setar as versões do sistema e saber quando devemos fazer o reload da página.
     * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
     * */
    public function _initAppConfig()
    {
        $appFile = APPLICATION_PATH . '/../app.json';

        $sessao = new Zend_Session_Namespace('geral');

        if (Zend_Session::sessionExists()) {

            $file = json_decode(file_get_contents($appFile));

            setcookie('reload', 'false');
            setcookie('versao', $sessao->st_versao ?: $file->versao);
            if ($sessao->st_versao != null && $sessao->st_versao != $file->versao) {
                $sessao->st_versao = $file->versao;
                setcookie('reload', 'true');
            }

            $sessao->st_versao = $file->versao;
        }
    }

    public function _initHelpers()
    {
        Zend_Controller_Action_HelperBroker::addPrefix('G2_Helper');
    }
}

<?php
/**
 * Classe de persistência de Tramite
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage dao
 */
class TramiteDAO extends Ead1_DAO
	{

	/** * Metodo que cadastra um tramite
	 * @param TramiteTO $to
	 * @throws Zend_Exception
	 * @return int $id_tramite | Boolean false
	 */
	public function cadastrarTramite(TramiteTO $to)
		{try{
//			$this->beginTransaction();
			$orm = new TramiteORM();
			$to->setSt_tramite($to->getSt_tramite());
			$insert = $to->toArrayInsert();
			$id_tramite = $orm->insert($insert);
//			$this->commit();
			return $id_tramite;
		}catch (Exception $e){
//			$this->rollBack();
			$this->excecao =  $e->getMessage();
			THROW $e;

		}
	}

	/**
	 * Cadastra processamento
	 * @param ProcessamentoTO $to
	 * @throws Zend_Exception
	 * @return int $id_processamento | Boolean false
	 */
	public function cadastrarProcessamento(ProcessamentoTO $to){
		try{
			$this->beginTransaction();
			$orm = new ProcessamentoORM();
			$insert = $to->toArrayInsert();
			$id_processamento = $orm->insert($insert);
			$this->commit();
			return $id_processamento;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao =  $e->getMessage();
			THROW $e;
			return false;
		}
	}


    /**
     * Método que atualiza processamento
     * @param OcorrenciaTO $to
     * @throws Zend_Exception
     * @return boolean|Zend_Exception
     */
    public function editarProcessamento(ProcessamentoTO $to)
    {
        $this->beginTransaction();
        try {
            $orm = new ProcessamentoORM();
            $orm->update($to->toArrayUpdate(false, array('id_processamento', 'dt_processamento')), $orm->montarWhere($to, true));
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            throw $e;
            return $e;
        }
    }

    /**
     * Metodo que cadastra um vinculo de tramite com matricula
     * @param TramiteMatriculaTO $to
     * @return mixed
     * @throws Exception
     * @throws Zend_Exception
     */
    public function cadastrarTramiteMatricula(TramiteMatriculaTO $to)
    {
        try {
            $this->beginTransaction();
            $orm = new TramiteMatriculaORM();
            $insert = $to->toArrayInsert();
            $id_tramitematricula = $orm->insert($insert);
            $this->commit();
            return $id_tramitematricula;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
        }
    }

    /**
     * Metodo que cadastra um vinculo de tramite com venda
     * @param TramiteVendaTO $to
     * @throws Zend_Exception
     * @return int $id_tramitemvenda | Boolean false
     */
    public function cadastrarTramiteVenda(TramiteVendaTO $to)
    {
        try {
            $this->beginTransaction();
            $orm = new TramiteVendaORM();
            $insert = $to->toArrayInsert();
            $id_tramitemvenda = $orm->insert($insert);
            $this->commit();
            return $id_tramitemvenda;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que cadastra um vinculo de tramite com entrega de material
     * @param TramiteEntregaMaterialTO $to
     * @throws Zend_Exception
     * @return int $id_tramiteentregamaterial | Boolean false
     */
    public function cadastrarTramiteEntregaMaterial(TramiteEntregaMaterialTO $to)
    {
        try {
            $this->beginTransaction();
            $orm = new TramiteEntregaMaterialORM();
            $insert = $to->toArrayInsert();
            $id_tramiteentregamaterial = $orm->insert($insert);
            $this->commit();
            return $id_tramiteentregamaterial;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que retorna os tipos de tramite
     * @param TipoTramiteTO $to
     * @param Mixed $where
     * @throws Zend_Exception
     * @return Zend_Db_Table_Abstract | Boolean false
     */
    public function retornarTipoTramite(TipoTramiteTO $to, $where = null)
    {
        try {
            $orm = new TipoTramiteORM();
            if (!$where) {
                $where = $orm->montarWhere($to);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que retorna as Categorias de tramite
     * @param CategoriaTramiteTO $to
     * @param Mixed $where
     * @throws Zend_Exception
     * @return Zend_Db_Table_Abstract | Boolean false
     */
    public function retornarCategoriaTramite(CategoriaTramiteTO $to, $where = null)
    {
        try {
            $orm = new CategoriaTramiteORM();
            if (!$where) {
                $where = $orm->montarWhere($to);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que retorna processamentos ocorridos no sistema
     * @param ProcessamentoTO $to
     * @param Mixed $where
     * @throws Zend_Exception
     * @return Zend_Db_Table_Abstract | Boolean false
     */
    public function retornarProcessamento(ProcessamentoTO $to, $where = null)
    {
        try {
            $orm = new ProcessamentoORM();
            if (!$where) {
                $where = $orm->montarWhere($to);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que retorna processos do sistema
     * @param ProcessoTO $to
     * @param Mixed $where
     * @throws Zend_Exception
     * @return Zend_Db_Table_Abstract | Boolean false
     */
    public function retornarProcesso(ProcessoTO $to, $where = null)
    {
        try {
            $orm = new ProcessoORM();
            if (!$where) {
                $where = $orm->montarWhere($to);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que retorna os Tramites
     * @param TramiteTO $to
     * @param Mixed $where
     * @throws Zend_Exception
     * @return Zend_Db_Table_Abstract | Boolean false
     */
    public function retornarTramite(TramiteTO $to, $where = null)
    {
        try {
            $orm = new TramiteORM();
            if (!$where) {
                $where = $orm->montarWhere($to);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que retorna os vinculos de tramite com matricula
     * @param TramiteMatriculaTO $to
     * @param Mixed $where
     * @throws Zend_Exception
     * @return Zend_Db_Table_Abstract | Boolean false
     */
    public function retornarTramiteMatricula(TramiteMatriculaTO $to, $where = null)
    {
        try {
            $orm = new TramiteMatriculaORM();
            if (!$where) {
                $where = $orm->montarWhere($to);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Retorna os tramites a partir da tabela de relação tb_tramite_*
     * A tabela de relação é definida a partir da categoria de trâmite
     * Exemplo: para a categoria MATRICULA existe a tb_tramite_matricula
     * O idChave deve ser usado como chave da tabela de relação
     *
     * @param int $idCategoriaTramite - id da categoria do tramite a ser buscado
     * @param array $chaves - array de id's a ser utilizado na busca de um tramite na tabela de relacao
     * @param int $idTipoTramite - id do tipo de trâmite a ser buscado
     * @return array:
     */
    public function retornarTramiteSP($idCategoriaTramite, $chaves, $idTipoTramite = null)
    {
        $sqlChavesIN = implode(' ,', $chaves);
        $statement = Zend_Db_Table_Abstract::getDefaultAdapter()->prepare('EXECUTE sp_tramite ?, ?, ?');
        $retorno = $statement->execute(array($idCategoriaTramite, $sqlChavesIN, $idTipoTramite));
        return $statement->fetchAll();
    }

    /**
     * Copia o Tramite do Termo de Responsabilidade para a Matriícula se o mesmo existir
     * @param $id_venda
     * @param $id_matricula
     * @throws Exception
     */
    public function copiaTermoVendaMatricula($id_venda, $id_matricula){

        $this->beginTransaction();
        try {
            $orm = new TramiteORM();
            $tramite = $orm->getDefaultAdapter()
                ->query('select top 1 t.id_usuario, t.id_entidade, t.st_tramite
                            , t.dt_cadastro, t.bl_visivel, t.id_upload, t.st_url 
                            from tb_tramite as t join tb_tramitevenda as tv 
                            on(t.id_tramite = tv.id_tramite) 
                            where tv.id_venda = '.$id_venda.' and t.id_tipotramite = '.\G2\Constante\TipoTramite::VENDA_TERMO_RESPONSABILIDADE)->fetch();

            if($tramite){
                $tto = new TramiteTO($tramite);
                $tto->setId_tramite(null);
                $tto->setId_tipotramite(\G2\Constante\TipoTramite::MATRICULA_TERMO_RESPONSABILIDADE);
                $id_tramite = $this->cadastrarTramite($tto);
                $tmat = new TramiteMatriculaTO();
                $tmat->setId_tramite($id_tramite);
                $tmat->setId_matricula($id_matricula);
                $this->cadastrarTramiteMatricula($tmat);
            }
            $this->commit();
        } catch (Exception $e) {
            $this->rollBack();
            throw $e;
        }

    }

    /**
     * Método que atualiza um trâmite
     * @param TramiteTO $tramite
     * @throws Zend_Exception
     */
    public function updateTramite(TramiteTO $tramite)
    {
        $this->beginTransaction();
        try {
            $orm = new TramiteORM();
            $orm->update($tramite->toArrayUpdate(false, array('id_tramite', 'dt_cadastro', 'id_usuario')), $orm->montarWhere($tramite, true));
            $this->commit();
        } catch (Zend_Exception $e) {
            $this->rollBack();
            throw $e;
        }
    }

}

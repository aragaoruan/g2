<?php
/**
 * Classe que realiza interações com o banco para Sincronização de dados
 * @author edermariano
 *
 */
class SincronizadorDAO extends Ead1_DAO{
	
	/**
	 * Método que consulta áreas sem sincronização para a entidade em questão
	 * @param int $idEntidade
	 */
	public function areasNaoSincronizadas($idEntidade, $idSistema){
		$areaIntegracaoORM = new AreaIntegracaoORM();
		$select = $areaIntegracaoORM->select()
			->setIntegrityCheck(false)
			->from(array('ac' => 'tb_areaconhecimento'), '*')
			->joinLeft(array('ai' => 'tb_areaintegracao'), 'ac.id_areaconhecimento = ai.id_areaconhecimento  AND ai.id_sistema = ' . $idSistema, '')
			->where("ai.id_areaintegracao IS NULL AND ac.id_entidade = $idEntidade"." AND ac.bl_ativo = 1")
			->order("id_areaconhecimentopai ASC");
		return $areaIntegracaoORM->fetchAll($select);
	}
	
	/**
	 * Método que consulta áreas sincronizadas para a entidade em questão
	 * @param int $idEntidade
	 */
	public function areasSincronizadas($idEntidade, $idSistema){
		$areaIntegracaoORM = new AreaIntegracaoORM();
		$select = $areaIntegracaoORM->select()
			->setIntegrityCheck(false)
			->from(array('ac' => 'tb_areaconhecimento'), '*')
			->joinInner(array('ai' => 'tb_areaintegracao'), 'ac.id_areaconhecimento = ai.id_areaconhecimento AND ai.id_sistema = ' . $idSistema, '')
			->where("ac.id_entidade = ?", $idEntidade,Zend_Db::PARAM_INT)
			->order("id_areaconhecimentopai ASC");
		return $areaIntegracaoORM->fetchAll($select);
	}
}
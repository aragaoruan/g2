<?php

/**
 * Description of PortalMensagemPadraoMatriculaDAO
 *
 * @author Yannick Naquis Roulé
 */
class PortalMensagemPadraoMatriculaDAO extends Ead1_DAO {

    public function cadastrarAlerta(AlertaTO $aTO) {
        //var_dump($aTO);exit;
        try {
            $this->beginTransaction();
            $orm = new PortalMensagemPadraoMatriculaORM();
            $insert = $aTO->toArrayInsert();
            $id_avaliacao = $orm->insert($insert);
            $this->commit();
            return $id_avaliacao;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    public function retornarAlerta(AlertaTO $aTO, $where = null) {
        try {
            $orm = new PortalMensagemPadraoMatriculaORM();
            if (!$where) {
                $where = $orm->montarWhereView($aTO, false, true);
            }
            //print_r($where);exit;
            $obj = $orm->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

}

?>

<?php
/**
 * Classe de persistencia do produto com o banco de dados
 * @author Eduardo Romão <ejushiro@gmail.com>
 * @since 21/02/2011
 *
 * @package models
 * @subpackage dao
 */
class ProdutoDAO extends Ead1_DAO
{

    public $hasCombo = false;

    /**
     * Método que cadastra(insert) Campanha de um produto
     * @param CampanhaProdutoTO $cpTO
     * @see ProdutoBO::cadastrarCampanhaProduto();
     * @return boolean
     */
    public function cadastrarCampanhaProduto(CampanhaProdutoTO $cpTO)
    {
        $campanhaProdutoORM = new CampanhaProdutoORM();
        $this->beginTransaction();
        try {
            $insert = $cpTO->toArrayInsert();
            $campanhaProdutoORM->insert($insert);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que cadastra(insert) Tag
     * @param TagTO $tagTO
     * @see ProdutoBO::cadastrarCampanhaProduto();
     * @return boolean
     */
    public function cadastrarTag(TagTO $tagTO)
    {
        $tagORM = new TagORM();
        $this->beginTransaction();
        try {
            $insert = $tagTO->toArrayInsert();
            $id = $tagORM->insert($insert);
            $this->commit();
            return $id;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    public function cadastrarProdutoCombo(ProdutoComboTO $pcTO)
    {
        $pCORM = new ProdutoComboORM();
        $this->beginTransaction();

        try {
            $this->hasCombo = true;
            $insert = $pcTO->toArrayInsert();
            $pCORM->insert($insert);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que cadastra(insert) Tag e Produto
     * @param TagProdutoTO $tagProdutoTO
     * @return boolean
     */
    public function cadastrarTagProduto(TagProdutoTO $tagProdutoTO)
    {
        $tagORM = new TagProdutoORM();
        $this->beginTransaction();
        try {
            $insert = $tagProdutoTO->toArrayInsert();
            $tagORM->insert($insert);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que cadastra(insert) um Produto
     *
     * [Quem utiliza]
     * @see ProdutoRO::cadastrarProduto();
     *
     * @param ProdutoTO $pTO
     * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
     */
    public function cadastrarProduto(ProdutoTO $pTO)
    {
        $produtoORM = new ProdutoORM();
        $this->beginTransaction();
        try {
            $insert = $pTO->toArrayInsert();
            $id = $produtoORM->insert($insert);
            $this->commit();
            return $id;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    public function cadastrarProdutoPrr(ProdutoPrrTO $prrTO)
    {
        $produtoPrrORM = new ProdutoPrrORM();
        $this->beginTransaction();
        try {
            $insert = $prrTO->toArrayInsert();
            $id = $produtoPrrORM->insert($insert);
            $this->commit();
            return $id;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que cadastra um Projeto pedagógico a taxa em um produto
     *
     * @see ProdutoBO::cadastrarProdutoTaxaProjeto();
     *
     * @param ProdutoTaxaProjetoTO $ptpTO
     * @return boolean retorna true se for inserido no banco ou false senão conseguir
     */
    public function cadastrarProdutoTaxaProjeto(ProdutoTaxaProjetoTO $ptpTO)
    {
        $produtoTaxaProjetoORM = new ProdutoTaxaProjetoORM();
        $this->beginTransaction();
        try {
            $insert = $ptpTO->toArrayInsert();
            $produtoTaxaProjetoORM->insert($insert);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que cadastra(insert) um Produto do Projeto Pedagógico
     *
     * [Quem utiliza]
     * @see ProdutoBO::cadastrarProdutoProjetoPedagogico();
     *
     * @param ProdutoProjetoPedagogicoTO $pppTO
     * @return Boolean
     */
    public function cadastrarProdutoProjetoPedagogico(ProdutoProjetoPedagogicoTO $pppTO)
    {
        $produtoProjetoPedagogicoORM = new ProdutoProjetoPedagogicoORM();
        $this->beginTransaction();
        try {
            $insert = $pppTO->toArrayInsert();
            $produtoProjetoPedagogicoORM->insert($insert);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que cadastra(insert) um Produto Valor
     *
     * [Quem utiliza]
     * @see ProdutoBO::cadastrarProdutoValor();
     *
     * @param ProdutoValorTO $pvTO
     * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
     */
    public function cadastrarProdutoValor(ProdutoValorTO $pvTO)
    {
        $this->beginTransaction();
        try {
            $produtoValorORM = new ProdutoValorORM();
            $insert = $pvTO->toArrayInsert();
            $id = $produtoValorORM->insert($insert);
            $this->commit();
            return $id;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que cadastra(insert) LancamentoVenda;
     *
     * [Quem utiliza]
     * @see ProdutoBO::cadastrarLancamentoVenda();
     *
     * @param LancamentoVendaTO $lvTO
     * @return boolean
     */
    public function cadastrarLancamentoVenda(LancamentoVendaTO $lvTO)
    {
        $lancamentoVendaORM = new LancamentoVendaORM();
        $this->beginTransaction();
        try {
            $insert = $lvTO->toArrayInsert();
            $lancamentoVendaORM->insert($insert);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que cadastra(insert) um VendaProduto
     *
     * [Quem utiliza]
     * @see ProdutoBO::cadastrarVendaProduto(); VendaBO::salvarArrVendaProduto();
     *
     * @param VendaProdutoTO $vpTO
     * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
     */
    public function cadastrarVendaProduto(VendaProdutoTO $vpTO)
    {
        $vendaProdutoORM = new VendaProdutoORM();
        $this->beginTransaction();
        try {
            $insert = $vpTO->toArrayInsert();
            $id = $vendaProdutoORM->insert($insert);
            $this->commit();
            return $id;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que cadastra o vinculo do produto com a declaracao (textosistema)
     * @param ProdutoTextoSistemaTO $to
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function cadastrarProdutoTextoSistema(ProdutoTextoSistemaTO $to)
    {
        try {
            $this->beginTransaction();
            $insert = $to->toArrayInsert();
            $orm = new ProdutoTextoSistemaORM();
            $orm->insert($insert);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que cadastra o vinculo do produto com a taxa
     * @param ProdutoTaxaTO $to
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function cadastrarProdutoTaxa(ProdutoTaxaTO $to)
    {
        try {
            $this->beginTransaction();
            $insert = $to->toArrayInsert();
            $orm = new ProdutoTaxaORM();
            $orm->insert($insert);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que cadastra o vinculo do produto com o livro
     * @param ProdutoLivroTO $to
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function cadastrarProdutoLivro(ProdutoLivroTO $to)
    {
        try {
            $this->beginTransaction();
            $insert = $to->toArrayInsert();
            $orm = new ProdutoLivroORM();
            $orm->insert($insert);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que cadastra o vinculo do produto com o combo de produtos
     * @param ProdutoComboTO $to
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function inserirProdutoCombo(ProdutoComboTO $to)
    {
        try {
            $this->beginTransaction();
            $insert = $to->toArrayInsert();
            $orm = new ProdutoComboORM();
            $id_produtocombo = $orm->insert($insert);
            $to->setId_produtocombo($id_produtocombo);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que atualiza o vinculo do produto com o combo de produtos
     * @param ProdutoComboTO $to
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function atualizarProdutoCombo(ProdutoComboTO $to)
    {
        try {
            $this->beginTransaction();

            if ((int)$to->getId_produtocombo() == false) {
                throw new Zend_Exception("Informa o ID do Combo para Atualização");
            }

            $update = $to->toArrayUpdate(false, array('id_produtocombo', 'id_produto', 'id_produtoitem', 'id_usuario'));
            $orm = new ProdutoComboORM();
            $orm->update($update, 'id_produtocombo = ' . $to->getId_produtocombo());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }


    /**
     * Método que excluí um item do Combo
     * @param ProdutoComboTO $to
     * @throws Zend_Exception
     * @return boolean
     */
    public function excluirProdutoCombo(ProdutoComboTO $to)
    {
        try {
            $this->beginTransaction();
            if ((int)$to->getId_produto() == false) {
                throw new Zend_Exception("Informa o ID do Combo para Exclusão");
            }

            $orm = new ProdutoComboORM();
            $orm->delete('id_produto = ' . $to->getId_produto());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }


    /**
     * Metodo que cadastra o vinculo do produto com a avaliacao
     * @param ProdutoAvaliacaoTO $to
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function cadastrarProdutoAvaliacao(ProdutoAvaliacaoTO $to)
    {
        try {
            $this->beginTransaction();
            $insert = $to->toArrayInsert();
            $orm = new ProdutoAvaliacaoORM();
            $orm->insert($insert);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Método que cadastra(insert) uma Venda
     * @param VendaTO $vTO
     * @see ProdutoBO::cadastrarVenda();
     * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
     */
    public function cadastrarVenda(VendaTO $vTO)
    {
        $vendaDAO = new VendaDAO();
        return $vendaDAO->cadastrarVenda($vTO);
    }

    /**
     * Método que cadastra(insert) um Lançamento
     *
     * [Quem Utiliza]
     * @see ProdutoBO::cadastrarLancamento();
     *
     * @param LancamentoTO $lTO
     * @return int|false retorna o valor do id inserido no banco ou false se não conseguir
     */
    public function cadastrarLancamento(LancamentoTO $lTO)
    {
        $lancamentoORM = new LancamentoORM();
        $this->beginTransaction();
        try {
            $insert = $lTO->toArrayInsert();
            $id = $lancamentoORM->insert($insert);
            $this->commit();
            return $id;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que cadastra(insert) um Plano de Pagamento
     *
     * [Quem Utiliza]
     * @see ProdutoBO::cadastrarPlanoPagamento();
     *
     * @param PlanoPagamentoTO $planoPagamentoTO
     * @return int|false retorna o valor do id inserido no banco ou false se não conseguir
     */
    public function cadastrarPlanoPagamento(PlanoPagamentoTO $planoPagamentoTO)
    {
        $planoPagamentoORM = new PlanoPagamentoORM();
        $this->beginTransaction();
        try {
            $insert = $planoPagamentoTO->toArrayInsert();
            $id = $planoPagamentoORM->insert($insert);
            $this->commit();
            return $id;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que Edita(update) um Produto
     * @param ProdutoTO $pTO
     * @see ProdutoBO::editarProduto();
     * @return boolean
     */
    public function editarProduto(ProdutoTO $pTO)
    {
        $produtoORM = new ProdutoORM();
        $this->beginTransaction();
        try {
            $where = $produtoORM->montarWhere($pTO, true);
            if (empty($where)) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $update = $pTO->toArrayUpdate(false, array('id_produto', 'id_entidade', 'id_usuariocadastro', 'id_tipoproduto'));
            $produtoORM->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }


    /**
     * Método que Edita(update) um Tag
     * @param TagTO $tagTO
     * @return boolean
     */
    public function editarTag(TagTO $tagTO)
    {
        $tagORM = new TagORM();
        $this->beginTransaction();
        try {
            $where = $tagORM->montarWhere($tagTO, true);
            $update = $tagTO->toArrayUpdate(false, array('id_tag', 'id_entidade', 'id_usuariocadastro', 'dt_cadastro'));
            $tagORM->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que Edita(update) um ProdutoProjetoPedagogico
     * @param ProdutoProjetoPedagogicoTO $pppTO
     * @see ProdutoBO::editarProdutoProjetoPedagogico();
     * @return boolean
     */
    public function editarProdutoProjetoPedagogico(ProdutoProjetoPedagogicoTO $pppTO)
    {
        $produtoProjetoPedagogicoORM = new ProdutoProjetoPedagogicoORM();
        $this->beginTransaction();
        try {
           $where = $produtoProjetoPedagogicoORM->montarWhere($pppTO, true);
            if (empty($where)) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $update = $pppTO->toArrayUpdate(false, array('id_produtoprojetopedagogico'));
            $produtoProjetoPedagogicoORM->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que Edita(update) um ProdutoValor
     * @param ProdutoValorTO $pvTO
     * @see ProdutoBO::editarProdutoValor();
     * @return boolean
     */
    public function editarProdutoValor(ProdutoValorTO $pvTO)
    {
        $produtoValorORM = new ProdutoValorORM();
        $this->beginTransaction();
        try {
            $where = $produtoValorORM->montarWhere($pvTO, true);
            if (empty($where)) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $update = $pvTO->toArrayUpdate(false, array('id_produtovalor', 'id_produto', 'id_usuariocadastro', 'dt_cadastro'));
            $produtoValorORM->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que Edita(update) um VendaProduto
     * @param VendaProdutoTO $vpTO
     * @see ProdutoBO::editarVendaProduto(); VendaBO::salvarArrVendaProduto();
     * @return boolean
     */
    public function editarVendaProduto(VendaProdutoTO $vpTO)
    {
        $vendaProdutoORM = new VendaProdutoORM();
        $this->beginTransaction();
        try {
            $where = $vendaProdutoORM->montarWhere($vpTO, true);
            if (empty($where)) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $update = $vpTO->toArrayUpdate(false, array('id_vendaproduto'));
            $vendaProdutoORM->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que Edita(update) uma Venda
     * @param VendaTO $vTO
     * @param boolean $aceitanull
     * @see ProdutoBO::editarVenda() MatriculaBO::procedimentoAtivarMatricula();
     * @return boolean
     */
    public function editarVenda(VendaTO $vTO, $aceitanull = false)
    {
        $vendaORM = new VendaORM();
        $this->beginTransaction();
        try {
            $where = $vendaORM->montarWhere($vTO, true);
            if (empty($where)) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            //Gerando Tramite de Venda - Método foi comentado porque a venda vai ser registrada utilizando um método
            //que foi refatorado.

//            $tramiteBO = new TramiteBO();
//            $tramiteBO->cadastrarTramiteVenda($vTO);

            $update = $vTO->toArrayUpdate($aceitanull, array('id_venda'));
            $vendaORM->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que Edita(update) um Lancamento
     *
     * [Quem Utiliza]
     * @see ProdutoBO::editarLancamento();
     *
     * @param LancamentoTO $lTO
     * @return boolean
     */
    public function editarLancamento(LancamentoTO $lTO)
    {
        $vendaDAO = new VendaDAO();
        return $vendaDAO->editarLancamento($lTO);
    }

    /**
     * Método que Edita(update) um LancamentoVenda
     * @param LancamentoVendaTO $lvTO
     * @see ProdutoBO::editarLancamentoVenda();
     * @return boolean
     */
    public function editarLancamentoVenda(LancamentoVendaTO $lvTO)
    {
        $lancamentoVendaORM = new LancamentoVendaORM();
        $this->beginTransaction();
        try {
            $where = $lancamentoVendaORM->montarWhere($lvTO, true);
            if (empty($where)) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $update = $lvTO->toArrayUpdate(false, array('id_lancamento', 'id_venda'));
            $lancamentoVendaORM->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que deleta a CampanhaProduto
     * @param CampanhaProdutoTO $cpTO
     * @see ProdutoBO::deletarCampanhaProduto();
     * @return boolean
     */
    public function deletarCampanhaProduto(CampanhaProdutoTO $cpTO)
    {
        $campanhaProdutoORM = new CampanhaProdutoORM();
        $this->beginTransaction();
        try {
            $where = $campanhaProdutoORM->montarWhere($cpTO, true);
            if (empty($where)) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $campanhaProdutoORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que deleta a Tag de Produto
     * @param TagProdutoTO $tagProdutoTO
     * @return boolean
     */
    public function deletarTagProduto(TagProdutoTO $tagProdutoTO)
    {
        $tagProdutoORM = new TagProdutoORM();
        $this->beginTransaction();
        try {
            if (!(int)$tagProdutoTO->getId_produto() && !(int)$tagProdutoTO->getId_tag()) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $where = $tagProdutoORM->montarWhere($tagProdutoTO, false);
            $tagProdutoORM->delete($where);
            $this->commit();
            return true;
        } catch (Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Método que deleta a Tag
     * @param TagTO $tagTO
     * @return boolean
     */
    public function deletarTag(TagTO $tagTO)
    {
        $tagORM = new TagORM();
        $this->beginTransaction();
        try {
            $where = $tagORM->montarWhere($tagTO, true);
            if (empty($where)) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $tagORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que deleta o Produto
     * @param ProdutoTO $pTO
     * @see ProdutoBO::deletarProduto();
     * @return boolean
     */
    public function deletarProduto(ProdutoTO $pTO)
    {
        $produtoORM = new ProdutoORM();
        $this->beginTransaction();
        try {
            $where = $produtoORM->montarWhere($pTO, true);
            if (empty($where)) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $produtoORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que deleta o ProdutoPrr
     * @param ProdutoPrrTO $pTO
     * @see ProdutoPrrBO::deletarProdutoPrr();
     * @return boolean
     */
   /* public function deletarProdutoPrr(ProdutoTO $pTO)
    {
        try
        {
           $pTO->setBl_ativo(false);
           $this->editarProduto($pTO);
           return true;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }*/

    /**
     * Método que deleta as taxas vinculadas ao projeto de um Produto
     * @param int $id_produto
     * @see ProdutoBO::deletarProdutoTaxaProjetoPorProduto();
     * @return boolean
     */
    public function deletarProdutoTaxaProjetoPorProduto(int $id_produto)
    {
        $produtoTaxaProjetoORM = new ProdutoTaxaProjetoORM();
        $this->beginTransaction();
        try {
            $produtoTaxaProjetoORM->delete("id_produto = $id_produto");
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que deleta a ProdutoProjetoPedagogico
     * @param ProdutoProjetoPedagogicoTO $pppTO
     * @see ProdutoBO::deletarProdutoProjetoPedagogico();
     * @return boolean
     */
    public function deletarProdutoProjetoPedagogico(ProdutoProjetoPedagogicoTO $pppTO)
    {
        $produtoProjetoPedagogicoORM = new ProdutoProjetoPedagogicoORM();
        $this->beginTransaction();
        try {
            $where = $produtoProjetoPedagogicoORM->montarWhere($pppTO, true);
            if (empty($where)) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $produtoProjetoPedagogicoORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que deleta a ProdutoValor
     * @param ProdutoProdutoValorTO $pvTO
     * @see ProdutoBO::deletarProdutoValor();
     * @return boolean
     */
    public function deletarProdutoValor(ProdutoValorTO $pvTO)
    {
        $this->beginTransaction();
        try {
            $produtoValorORM = new ProdutoValorORM();
            $where = $produtoValorORM->montarWhere($pvTO, true);
            if (empty($where)) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $produtoValorORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que deleta a VendaProduto
     * @param VendaProdutoTO $vpTO
     * @see ProdutoBO::deletarVendaProduto();
     * @return boolean
     */
    public function deletarVendaProduto(VendaProdutoTO $vpTO)
    {
        $vendaProdutoORM = new VendaProdutoORM();
        $this->beginTransaction();
        try {
            $where = $vendaProdutoORM->montarWhere($vpTO, false);
            if (empty($where)) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $vendaProdutoORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que deleta a LancamentoVenda
     * @param LancamentoVendaTO $lvTO
     * @see ProdutoBO::deletarLancamentoVenda();
     * @return boolean
     */
    public function deletarLancamentoVenda(LancamentoVendaTO $lvTO)
    {
        $lancamentoVendaORM = new LancamentoVendaORM();
        $this->beginTransaction();
        try {
            $where = $lancamentoVendaORM->montarWhere($lvTO, true);
            if (empty($where)) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $lancamentoVendaORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que deleta a Venda
     * @param VendaTO $vTO
     * @see ProdutoBO::deletarVenda();
     * @return boolean
     */
    public function deletarVenda(VendaTO $vTO)
    {
        $vendaORM = new VendaORM();
        $this->beginTransaction();
        try {
            $where = $vendaORM->montarWhere($vTO, true);
            if (empty($where)) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $vendaORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que deleta o Lancamento
     * @param LancamentoTO $lTO
     * @see ProdutoBO::deletarLancamento();
     * @return boolean
     */
    public function deletarLancamento(LancamentoTO $lTO)
    {
        $lancamentoORM = new LancamentoORM();
        $this->beginTransaction();
        try {
            $where = $lancamentoORM->montarWhere($lTO, true);
            if (empty($where)) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $lancamentoORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que deleta o Plano de Pagamento
     * @param PlanoPagamentoTO $planoPagamentoTO
     * @see ProdutoBO::salvarArrPlanoPagamento();
     * @return boolean
     */
    public function deletarPlanoPagamento(PlanoPagamentoTO $planoPagamentoTO)
    {
        $planoPagamentoORM = new PlanoPagamentoORM();
        $this->beginTransaction();
        try {
            $where = $planoPagamentoORM->montarWhere($planoPagamentoTO);
            if (empty($where)) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $planoPagamentoORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que exclui o vinculo do produto com a declaracao (textosistema)
     * @param ProdutoTextoSistemaTO $to
     * @param Mixed $where
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function deletarProdutoTextoSistema(ProdutoTextoSistemaTO $to, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new ProdutoTextoSistemaORM();
            if (!$where) {
                $where = $orm->montarWhere($to, false);
            }
            if (!$where) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $orm->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que exclui o vinculo do produto com a taxa em um projeto
     * @param ProdutoTaxaTO $to
     * @param Mixed $where
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function deletarProdutoTaxaProjeto(ProdutoTaxaTO $to, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new ProdutoTaxaProjetoORM();
            if (!$where) {
                $where = "id_entidade = {$to->getId_entidade()} AND id_produto = {$to->getId_produto()} AND id_taxa = {$to->getId_taxa()}";
            }
            if (!$where) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $orm->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que exclui o vinculo do produto com a taxa
     * @param ProdutoTaxaTO $to
     * @param Mixed $where
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function deletarProdutoTaxa(ProdutoTaxaTO $to, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new ProdutoTaxaORM();
            if (!$where) {
                $where = $orm->montarWhere($to, false);
            }
            if (!$where) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $orm->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que exclui o vinculo do produto com o livro
     * @param ProdutoLivroTO $to
     * @param Mixed $where
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function deletarProdutoLivro(ProdutoLivroTO $to, $where = null)
    {
        try {
            $this->beginTransaction();

            if (!$to->getId_produto()) {
                THROW new Zend_Exception('O Produto é obrigatório!');
            }

            if (!$to->getId_livro()) {
                THROW new Zend_Exception('O Livro é obrigatório!');
            }

            $orm = new ProdutoLivroORM();
            if (!$where) {
                $where = $orm->montarWhere($to, false);
            }
            if (!$where) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $orm->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }


    /**
     * Metodo que exclui o vinculo do produto com o combo de produtos
     * @param ProdutoComboTO $to
     * @param Mixed $where
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function deletarProdutoCombo(ProdutoComboTO $to, $where = null)
    {
        try {
            $this->beginTransaction();

            if (!$to->getId_produto()) {
                THROW new Zend_Exception('O Combo é obrigatório!');
            }

            if (!$to->getId_produtoitem()) {
                THROW new Zend_Exception('O Produto que compõe o Combo é obrigatório!');
            }

            $orm = new ProdutoComboORM();
            if (!$where) {
                $where = $orm->montarWhere($to, false);
            }
            if (!$where) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $orm->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Método que retorna o Vinculo da Campanha com o Produto
     * @param CampanhaProdutoTO $cpTO
     * @see ProdutoBO::retornarCampanhaProduto();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarCampanhaProduto(CampanhaProdutoTO $cpTO)
    {
        $campanhaProdutoORM = new CampanhaProdutoORM();
        $where = $campanhaProdutoORM->montarWhere($cpTO);
        try {
            $obj = $campanhaProdutoORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }


    /**
     * Método que retorna Tag
     * @param TagTO $tagTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarTag(TagTO $tagTO)
    {
        $tagORM = new TagORM();
        $where = " id_entidade = " . $tagTO->getId_entidade() . " and st_tag like '%" . $tagTO->getSt_tag() . "%' ";
        try {
            $obj = $tagORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna Tag de Produto
     * @param TagProdutoTO $tagProdutoTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarTagProduto(TagProdutoTO $tagProdutoTO)
    {
        $tagProdutoORM = new TagProdutoORM();
        $where = $tagProdutoORM->montarWhere($tagProdutoTO);
        try {
            $obj = $tagProdutoORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Retorna Plano de pagamento do produto.
     * @param PlanoPagamentoTO $planoPagamentoTO
     * @see ProdutoBO::retornarPlanoPagamento();
     * @return Zend_Db_Table_Rowset_Abstract | false
     * @throws Zend_Exception
     */
    public function retornarPlanoPagamento(PlanoPagamentoTO $planoPagamentoTO)
    {
        $planoPagamentoORM = new PlanoPagamentoORM();
        $where = $planoPagamentoORM->montarWhere($planoPagamentoTO);
        try {
            $obj = $planoPagamentoORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna a view de produto
     * @param VwProdutoTO $vwpTO
     * @see ProdutoBO::retornarVwProduto();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwProduto(VwProdutoTO $vwpTO, $isCombo = false)
    {
        $vwProdutoORM = new VwProdutoORM();
        $where = $vwProdutoORM->montarWhere($vwpTO);
        //echo $where;exit;
        if ($isCombo == true) {
            $where .= ' AND id_tipoproduto in(1,6)';
        }

        try {
            $obj = $vwProdutoORM->fetchAll($where, 'st_produto');
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }


    /**
     * Retorna um Produto verificando o compartilhamento das Entidades
     * @param int $id_produto
     * @param int $id_entidade
     * @throws Zend_Exception
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @return Ambigous <mixed, Ambigous <string, boolean, mixed>>|boolean
     */
    public function retornarVwProdutoCompartilhado($id_produto, $id_entidade)
    {

        try {

	    	$sql = "select vw.* from vw_produto as vw 
						join tb_produto as p on (vw.id_produto = p.id_produto) AND p.bl_todasentidades = 1 and p.id_produto = $id_produto
						JOIN  vw_entidaderecursivaid AS er ON er.nu_entidadepai = p.id_entidade AND er.id_entidade = $id_entidade
					UNION
					select vw.* from vw_produto as vw 
						join tb_produto as p on (vw.id_produto = p.id_produto) AND p.bl_todasentidades = 0 and p.id_produto = $id_produto
						JOIN tb_produtoentidade as pe ON pe.id_produto = p.id_produto 
						AND pe.id_entidade = $id_entidade
					UNION
					select vw.* from vw_produto as vw 
						join tb_produto as p on (vw.id_produto = p.id_produto) AND  (p.id_entidade = $id_entidade)
						and p.id_produto = $id_produto order by vw.st_produto";

            $vwProdutoORM = new VwProdutoORM();
	        $rs = $vwProdutoORM->getAdapter()->query($sql)->fetchAll();
	        if($rs){
	        	return $rs;
	        } else {
	        	return false;
	        }

        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }


    /**
     * Método que retorna a view de produto
     * @param VwProdutoPrrTO $vwprrTO
     * @see ProdutoBO::retornarVwProdutoPrr();
     * @return Zend_Db_Table_Rowset_Abstract|false
     * criada em 29/08/2013 - AC-433
     * Débora Castro - deboracastro.pm@gmail.com
     */
    public function retornarVwProdutoPrr(VwProdutoPrrTO $vwprrTO)
    {
        $vwProdutoPrrORM = new VwProdutoPrrORM();
        $where = $vwProdutoPrrORM->montarWhere($vwprrTO);

        try {
            $obj = $vwProdutoPrrORM->fetchAll($where, 'st_produto');
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna a view de produto venda
     * @param VwProdutoTO $vwpTO
     * @see ProdutoBO::retornarVwProdutoVenda();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwProdutoVenda(VwProdutoVendaTO $to)
    {
        $orm = new VwProdutoVendaORM();
        $where = $orm->montarWhere($to);
        try {
            $obj = $orm->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna a view de produto venda
     * @param VwProdutoComboTO $vwpTO
     * @see ProdutoBO::retornarVwProdutoCombo();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwProdutoCombo(VwProdutoComboTO $to)
    {
        $orm = new VwProdutoComboORM();
        $where = $orm->montarWhere($to);
        try {
            $obj = $orm->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna o Tipo do Produto
     * @param TipoProdutoTO $tpTO
     * @see ProdutoBO::retornarTipoProduto();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarTipoProduto(TipoProdutoTO $tpTO)
    {
        $tipoProdutoORM = new TipoProdutoORM();
        $where = $tipoProdutoORM->montarWhere($tpTO);
        try {
            $obj = $tipoProdutoORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna o Tipo do Produto para o combo
     * @param TipoProdutoTO $tpTO
     * @see ProdutoBO::retornarTipoProduto();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarTipoProdutoCombo(TipoProdutoTO $tpTO)
    {
    	$tipoProdutoORM = new TipoProdutoORM();
    	$where = $tipoProdutoORM->montarWhere($tpTO);
    	$where .= ' id_tipoproduto in(1,6) '; // os combos podem ter apenas livros(6) e projetos pedagógicos (1)
    	try {
    		$obj = $tipoProdutoORM->fetchAll($where);
    		return $obj;
    	} catch (Zend_Exception $e) {
    		$this->excecao = $e->getMessage();
    		THROW new Zend_Exception($e->getMessage());
    		return false;
    	}
    }


    /**
     * Método que retorna o Produto
     * @param ProdutoTO $pTO
     * @see ProdutoBO::retornarProduto();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarProduto(ProdutoTO $pTO)
    {
        $produtoORM = new ProdutoORM();
        $where = $produtoORM->montarWhere($pTO);
        try {
            $obj = $produtoORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna o Produto do projeto pedagogico
     * @param ProdutoProjetoPedagogicoTO $pppTO
     * @see ProdutoBO::retornarProdutoProjetoPedagogico();
     * @return Zend_Db_Table_Rowset_Abstract|false
     * @deprecated

    public function retornarProdutoProjetoPedagogico(ProdutoProjetoPedagogicoTO $pppTO){
    $produtoProjetoPedagogicoORM = new ProdutoProjetoPedagogicoORM();
    $where = $produtoProjetoPedagogicoORM->montarWhere($pppTO);
    try{
    $obj = $produtoProjetoPedagogicoORM->fetchAll($where);
    return $obj;
    }catch(Zend_Exception $e){
    $this->excecao = $e->getMessage();
    THROW new Zend_Exception($e->getMessage());
    return false;
    }
    }*/

    /**
     * Método que retorna o Produto do projeto pedagogico
     * @param ProdutoProjetoPedagogicoTO $pppTO
     * @see ProdutoBO::retornarProdutoProjetoPedagogico();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarProdutoProjetoPedagogico(ProdutoProjetoPedagogicoTO $pppTO)
    {
        $produtoProjetoPedagogicoORM = new ProdutoProjetoPedagogicoORM();

        try {
            $where = $produtoProjetoPedagogicoORM->select()
                ->setIntegrityCheck(false)
                ->from(array('ppp' => 'tb_produtoprojetopedagogico'), '*')
                ->join(array('pp' => 'tb_projetopedagogico'), 'pp.id_projetopedagogico = ppp.id_projetopedagogico', array('pp.st_projetopedagogico', 'pp.st_tituloexibicao'))
                ->where($produtoProjetoPedagogicoORM->montarWhere($pppTO));

            $obj = $produtoProjetoPedagogicoORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna Tags de um produto
     * @param TagProdutoTO $tagProdutoTO
     * @see ProdutoBO::retornarProdutoProjetoPedagogico();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarTagsDeProduto(TagProdutoTO $tagProdutoTO)
    {
        $tagProdutoORM = new TagProdutoORM();
        try {
            $where = $tagProdutoORM->select()
                ->setIntegrityCheck(false)
                ->from(array('ta' => 'tb_tag'), '*')
                ->join(array('tp' => 'tb_tagproduto'), 'tp.id_tag = ta.id_tag', array('tp.id_produto'))
                ->where('id_produto = ' . $tagProdutoTO->getId_produto());

            $obj = $tagProdutoORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna o Produto Valor
     * @param ProdutoValorTO $pvTO
     * @see ProdutoBO::retornarProdutoValor();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarProdutoValor(ProdutoValorTO $pvTO)
    {
        $produtoValorORM = new ProdutoValorORM();
        $where = $produtoValorORM->montarWhere($pvTO);
        try {
            $obj = $produtoValorORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }


    /**
     * Retorna o Produto valor ativo
     * @param ProdutoValorTO $pvTO
     * @throws Zend_Exception
     * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
     */
    public function retornarProdutoValorAtivo(ProdutoValorTO $pvTO)
    {
        $produtoValorORM = new ProdutoValorORM();
        $where  = $produtoValorORM->montarWhere($pvTO);
        $where .= ' AND dt_inicio <= CAST(GETDATE() AS DATE)  AND ( dt_termino >= CAST(GETDATE() AS DATE) OR dt_termino IS NULL )';

        try {
            $obj = $produtoValorORM->fetchAll($where, 'dt_cadastro desc');
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna o Lançamento da venda
     * @param LancamentoVendaTO $lvTO
     * @param String $where
     * @see ProdutoBO::retornarLancamentoVenda() | MatriculaBO::ativarMatricula();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarLancamentoVenda(LancamentoVendaTO $lvTO, $where = null)
    {
        $lancamentoVendaORM = new LancamentoVendaORM();
        if (!$where) {
            $where = $lancamentoVendaORM->montarWhere($lvTO);
        }
        try {
            $obj = $lancamentoVendaORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna venda produto
     * @param VendaProdutoTO $vpTO
     * @param Mixed $where
     * @see ProdutoBO::retornarVendaProduto();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVendaProduto(VendaProdutoTO $vpTO, $where = null)
    {
        $vendaProdutoORM = new VendaProdutoORM();
        if (!$where) {
            $where = $vendaProdutoORM->montarWhere($vpTO);
        }
        try {
            $obj = $vendaProdutoORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna venda
     * @param VendaTO $vTO
     * @see ProdutoBO::retornarVenda();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVenda(VendaTO $vTO)
    {
        $vendaORM = new VendaORM();
        $where = $vendaORM->montarWhere($vTO);
        try {
            $obj = $vendaORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Método que retorna venda usuário
     * @param VwVendaUsuarioTO $vTO
     * @param Mixed $where
     * @see ProdutoBO::retornarVendaUsuario();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVendaUsuario(VwVendaUsuarioTO $vTO, $where = null)
    {
        try {
            $vendaORM = new VwVendaUsuarioORM();
            if (!$where) {
                $where = $vendaORM->montarWhere($vTO);
            }
            $obj = $vendaORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Método que retorna o Lancamento
     * @param LancamentoTO $lTO
     * @param String $where
     * @see ProdutoBO::retornarLancamento() | MatriculaBO::ativarMatricula();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarLancamento(LancamentoTO $lTO, $where = null)
    {
        $lancamentoORM = new LancamentoORM();
        if (!$where) {
            $where = $lancamentoORM->montarWhere($lTO);
        }
        try {
            $obj = $lancamentoORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna o Tipo Lancamento
     * @param TipoLancamentoTO $tlTO
     * @see ProdutoBO::retornarTipoLancamento();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarTipoLancamento(TipoLancamentoTO $tlTO)
    {
        $tipoLancamentoORM = new TipoLancamentoORM();
        $where = $tipoLancamentoORM->montarWhere($tlTO);
        try {
            $obj = $tipoLancamentoORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que retorna os dados da view de lancamento
     * @param VwLancamentoTO $vwLTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwLancamento(VwLancamentoTO $vwLTO)
    {
        $vwLancamentoORM = new VwLancamentoORM();
        $where = $vwLancamentoORM->montarWhereView($vwLTO, array('id_lancamento', 'id_venda', 'id_entidade', 'id_meiopagamento', 'id_tipolancamento'), true, true);
        try {
            $obj = $vwLancamentoORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que retorna o tipo de valor do produto
     * @param TipoProdutoValorTO $tpvTO
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarTipoProdutoValor(TipoProdutoValorTO $tpvTO)
    {
        try {
            $tipoProdutoValorORM = new TipoProdutoValorORM();
            $where = $tipoProdutoValorORM->montarWhere($tpvTO);
            $obj = $tipoProdutoValorORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que retorna o vinculo do produto com a declaracao (textosistema)
     * @param ProdutoTextoSistemaTO $to
     * @param Mixed $where
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarProdutoTextoSistema(ProdutoTextoSistemaTO $to, $where = null)
    {
        try {
            $orm = new ProdutoTextoSistemaORM();
            if (!$where) {
                $where = $orm->montarWhere($to);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que retorna o vinculo do produto com a taxa
     * @param ProdutoTaxaTO $to
     * @param Mixed $where
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarProdutoTaxa(ProdutoTaxaTO $to, $where = null)
    {
        try {
            $orm = new ProdutoTaxaORM();
            if (!$where) {
                $where = $orm->montarWhere($to);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que retorna o vinculo do produto com o Livro
     * @param ProdutoLivroTO $to
     * @param Mixed $where
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarProdutoLivro(ProdutoLivroTO $to, $where = null)
    {
        try {
            $orm = new ProdutoLivroORM();
            if (!$where) {
                $where = $orm->montarWhere($to);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que retorna o vinculo do produto com o combo de produtos
     * @param ProdutoComboTO $to
     * @param Mixed $where
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarProdutoCombo(ProdutoComboTO $to, $where = null)
    {
        try {
            $orm = new ProdutoComboORM();
            if (!$where) {
                $where = $orm->montarWhere($to);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que retorna a taxa
     * @param TaxaTO $to
     * @param Mixed $where
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarTaxa(TaxaTO $to, $where = null)
    {
        try {
            $orm = new TaxaORM();
            if (!$where) {
                $where = $orm->montarWhere($to, true);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Método que retorna o Produto Pelo id da avaliacao e da entidade
     * @param ProdutoAvaliacaoTO $to
     * @return Zend_Db_Table_Row_Abstract
     */
    public function retornarProdutoAvaliacaoEntidade(ProdutoAvaliacaoTO $to)
    {
        try {
            $produtoORM = new ProdutoORM();
            $produtoAvaliacaoORM = new ProdutoAvaliacaoORM();
            $rowsetProdutoAvaliacao = $produtoAvaliacaoORM->fetchAll($produtoAvaliacaoORM->montarWhere($to, false));
            if (!$rowsetProdutoAvaliacao->count()) {
                return $rowsetProdutoAvaliacao;
            }
            return $produtoORM->fetchAll("id_produto = " . $rowsetProdutoAvaliacao->current()->id_produto);
        } catch (Zend_Exception $e) {
            THROW $e;
            return false;
        }
    }

    /**
     * Método que retorna os dados da view dos Produtos do textosistema
     * @param VwProdutoTextoSistemaTO $to
     * @param Mixed $where
     * @return Zend_Db_Table_Row_Abstract
     */
    public function retornarVwProdutoTextoSistema(VwProdutoTextoSistemaTO $to, $where = null)
    {
        try {
            $orm = new VwProdutoTextoSistemaORM();
            if (!$where) {
                $where = $orm->montarWhereView($to, null, true);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            THROW $e;
            return false;
        }
    }


    /**
     * Método que retorna as imagens vinculadas a um produto
     * @param ProdutoImagemTO $to
     * @throws Zend_Exception
     * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
     */
    public function retornaProdutoImagem(ProdutoImagemTO $to)
    {
        try {
            $orm = new UploadORM();
            $where = $orm->select()
                ->from(array('u' => 'tb_upload'))
                ->setIntegrityCheck(false)
                ->joinInner(array('p' => 'tb_produtoimagem'), 'u.id_upload = p.id_upload', array())
                ->where('p.id_produto = ?', $to->getId_produto())
                ->where('p.bl_ativo = ? ', 1);
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            throw $e;
            return false;
        }
    }

    /**
     * Cadastra tb_produtoimagem
     * @param ProdutoImagemTO $to
     * @throws Zend_Exception
     * @return Ambigous <mixed, multitype:>|boolean
     */
    public function cadastrarProdutoImagem(ProdutoImagemTO $to)
    {
        $this->beginTransaction();
        try {
            $produtoImagemORM = new ProdutoImagemORM();
            $insert = $to->toArrayInsert();
            $id = $produtoImagemORM->insert($insert);
            $to->setId_produtoimagem($id);
            $this->commit();
            return $id;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }


    /**
     * Edita tb_produtoimagem
     * @param ProdutoImagemTO $to
     * @throws Zend_Exception
     * @return boolean
     */
    public function editarProdutoImagem(ProdutoImagemTO $to)
    {
        $produtoImagemORM = new ProdutoImagemORM();
        $this->beginTransaction();
        try {
            $where = $produtoImagemORM->montarWhere($to, true);
            if (empty($where)) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $update = $to->toArrayUpdate(false, array('id_produtoimagem'));
            $produtoImagemORM->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }


    /**
     * Retorna as categorias vinculadas a um produto
     * @param ProdutoTO $produtoTo
     * @throws Zend_Exception
     * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
     */
    public function retornaCategoriasProduto(ProdutoTO $produtoTo)
    {
        $categoriaProdutoORM = new CategoriaProdutoORM();
        try {
            return $categoriaProdutoORM->fetchAll("id_produto = " . $produtoTo->getId_produto());
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage());
            return false;
        }

    }

    /**
     * Retorna produtos de uma categoria
     * @param CategoriaProdutoTO $categoriaProdutoTO
     * @throws Zend_Exception
     * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
     */
    public function retornarCategoriaProduto(CategoriaProdutoTO $categoriaProdutoTO)
    {
        $categoriaProdutoORM = new CategoriaProdutoORM();
        $select = $categoriaProdutoORM->select()
            ->setIntegrityCheck(false)
            ->from(array('cp' => 'tb_categoriaproduto'), '*')
            ->join(array('pr' => 'tb_produto'), 'pr.id_produto = cp.id_produto', array('pr.st_produto'))
            ->where('id_categoria = ' . $categoriaProdutoTO->getId_categoria());
        try {
            return $categoriaProdutoORM->fetchAll($select);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage());
            return false;
        }

    }

    /**
     * Retorna as categorias de um produto
     * @param ProdutoTO $pTO
     * @throws Zend_Exception
     * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
     */
    public function retornarCategoriasDoProduto(ProdutoTO $pTO)
    {
        $categoriaProdutoORM = new CategoriaProdutoORM();
        $select = $categoriaProdutoORM->select()
            ->setIntegrityCheck(false)
            ->from(array('cp' => 'tb_categoriaproduto'), '*')
            ->join(array('pr' => 'tb_categoria'), 'pr.id_categoria = cp.id_categoria', array('pr.st_categoria'))
            ->where('id_produto = ' . $pTO->getId_produto());
        try {
            return $categoriaProdutoORM->fetchAll($select);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw $e;
            return false;
        }

    }


    /**
     * Método que desvincula a categoria do produto
     * @see ProdutoBO::salvarArCategoriasProduto
     * @param unknown_type $idProduto
     * @throws Zend_Exception
     */
    public function deletarCategoriaProdutoPorProduto($idProduto)
    {
        $this->beginTransaction();
        try {
            $orm = new CategoriaProdutoORM();
            $orm->delete("id_produto = $idProduto");
            $this->commit();
        } catch (Exception $e) {
            $this->rollBack();
            throw $e;
        }
    }


    /**
     * Método que vincula uma categoria ao produto
     * @see ProdutoBO::salvarArCategoriasProduto
     * @param CategoriaProdutoTO $catProdutoTo
     * @throws Zend_Exception
     */
    public function cadastrarArCategoriaProdutos(CategoriaProdutoTO $catProdutoTo)
    {
        $this->beginTransaction();
        try {
            $orm = new CategoriaProdutoORM();
            $catProdutoTo->setId_categoria($orm->insert($catProdutoTo->toArrayInsert()));
            $this->commit();
        } catch (Exception $e) {
            $this->rollBack();
            throw $e;
        }
    }

}

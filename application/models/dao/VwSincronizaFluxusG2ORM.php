<?php

/**
 * @author Rafael Rocha / Gabriel Resende
 *
 */
class Totvs_VwSincronizaFluxusG2ORM extends Ead1_ORM {

    public $_name = 'vw_sincroniza_fluxus_g2';
    //public $_primary = array('[CONTRATO]', '[PARCELA]');
    public $_primary = 'IDLAN';
    public $_schema = 'totvs';

}

?>
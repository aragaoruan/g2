<?php

/**
 * @author Rafael Rocha / Gabriel Resende
 */
class Totvs_VwSincronizaAcordoFluxusG2ORM extends Ead1_ORM {

    public $_name = 'vw_sincroniza_acordo_fluxus_g2';
    public $_primary = 'idacordo';
    public $_schema = 'totvs';

}

?>
<?php

/**
 * Classe que busca dados de Pesquisa em banco de dados
 * @author Eduardo Romão - ejushiro@gmail.com
 * 
 * @package models
 * @subpackage dao
 */
class PesquisarDAO extends Ead1_DAO
{

    public function retornarPesquisaTipoRecuperacao (PesquisarTipoRecuperacaoTO $trTO)
    {
        $trORM = new TipoRecuperacaoORM();
        $trTO->tipaTOConsulta();
        $where = $trORM->montarWherePesquisa($trTO, false, true);
        return $trORM->fetchAll($where);
    }

    public function retornarPesquisaTipoAvaliacao (PesquisarTipoAvaliacaoTO $taTO)
    {
        $taORM = new TipoAvaliacaoORM();
        $taTO->tipaTOConsulta();
        $where = $taORM->montarWherePesquisa($taTO, false, true);
        return $taORM->fetchAll($where);
    }

    /**
     * @see AvaliacaoBO::retornarAvaliacoesAplicacaoRelacao
     * @param PesquisarAvaliacao
     * @param Mixed $where
     * @return Zend_Db_Table_Rowset
     */
    public function retornarPesquisaAvaliacao (PesquisaAvaliacaoTO $aTO, $where = null)
    {
        try {
            $aORM = new VwPesquisaAvaliacaoORM();
            $aTO->tipaTOConsulta();
            if (!$where) {
                $where = $aORM->montarWherePesquisa($aTO, false, true);
            }
            return $aORM->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * @see PesquisarBO::pesquisarRelatorios
     * @param RelatorioTO $relatorioTO
     * @param Mixed $where
     * @return Zend_Db_Table_Rowset
     */
    public function pesquisarRelatorios (RelatorioTO $relatorioTO, $where = null)
    {
        try {
            $rORM = new RelatorioORM();
            $relatorioTO->tipaTOConsulta();
            if (!$where) {
                $where = $rORM->montarWherePesquisa($relatorioTO, false, true);
            }
            return $rORM->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * @see AvaliacaoBO::pesquisarAgendamentoAvaliacao
     * @param PesquisarAvaliacao
     * @param Mixed $where
     * @return Zend_Db_Table_Rowset
     */
    public function pesquisarAgendamentoAvaliacao (PesquisaAvaliacaoAgendamentoTO $to, $where = null)
    {
        try {
            $to->setDt_agendamentoinicio(null);
            $to->setDt_agendamentofim(null);
            $orm = new VwAvaliacaoAgendamentoORM();
            $to->tipaTOConsulta();
            if (!$where) {
                $where = $orm->montarWherePesquisa($to, false, true);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * @see AvaliacaoBO::pesquisarPessoaJuridica
     * @param PesquisaEntidadeTO $to
     * @param Mixed $where
     * @return Zend_Db_Table_Rowset
     */
    public function pesquisarPessoaJuridica (PesquisaEntidadeTO $to, $where = null)
    {
        try {
            $orm = new PesquisaEntidadeORM();
            $to->tipaTOConsulta();
            if (!$where) {
                $where = $orm->montarWherePesquisa($to, false, true);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * @see AvaliacaoBO::pesquisarAplicacaoAvaliacao
     * @param PesquisaAvaliacaoAplicacaoTO $to
     * @param Mixed $where
     * @return Zend_Db_Table_Rowset
     */
    public function pesquisarAplicacaoAvaliacao (PesquisaAvaliacaoAplicacaoTO $to, $where = null)
    {
        try {
            $orm = new VwAvaliacaoAplicacaoORM();
            $to->tipaTOConsulta();
            if (!$where) {
                $where = $orm->montarWherePesquisa($to, false, true);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * @see AvaliacaoBO::pesquisarTextosSistema
     * @param PesquisarTextoSistemaTO $to
     * @param Mixed $where
     * @return Zend_Db_Table_Rowset
     */
    public function pesquisarTextosSistema (PesquisarTextoSistemaTO $to, $where = null)
    {
        try {
            $orm = new VwTextoSistemaORM();
            $to->setId_entidade(null);
            $to->tipaTOConsulta();
            if (!$where) {
                $where = $orm->montarWherePesquisa($to, false, true);
                if ($where) {
                    $where = "( " . $where . " ) AND id_entidade = " . $to->getSessao()->id_entidade;
                } else {
                    $where = "id_entidade = " . $to->getSessao()->id_entidade;
                }
            }
            return $orm->fetchAll($where, 'st_textosistema');
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * 
     * @param PesquisarItemMaterial $im
     * @return Zend_Db_Table_Rowset
     */
    public function retornarPesquisaItemMaterial (PesquisarItemMaterialTO $imTO)
    {
        $imORM = new ItemDeMaterialORM();
        $imTO->tipaTOConsulta();
        $where = $imORM->montarWherePesquisa($imTO, false, true);
        if (strlen($where)) {
            $where .= ' and';
        }
        $where.= ' tm.id_entidade = ' . $imTO->getSessao()->id_entidade;
        $select = $imORM->select()
                ->from(array('im' => 'tb_itemdematerial'), array('*'))
                ->setIntegrityCheck(false)
                ->join(array('tm' => 'tb_tipodematerial'), 'im.id_tipodematerial = tm.id_tipodematerial', array('id_entidade'))
                ->where($where);
        return $imORM->fetchAll($select);
    }

    /**
     * 
     * @param PesquisarTipoMaterial $tm
     * @return Zend_Db_Table_Rowset
     */
    public function retornarPesquisaTipoMaterial (PesquisarTipoMaterialTO $tm)
    {
        $tmORM = new TipoDeMaterialORM();
        $tm->tipaTOConsulta();
        $where = $tmORM->montarWherePesquisa($tm, false, true);
        return $tmORM->fetchAll($where);
    }

    /**
     * Metodo que retorna os dados de Produto
     * @param PesquisarProdutoTO $to
     * return Ead1_Mensageiro
     */
    public function retornarPesquisaProduto (PesquisarProdutoTO $pto)
    {
        $pORM = new ProdutoORM();
        $arrWhere = array();
        if ($pto->getSt_produto()) {
            $arrWhere[] = " p.st_produto like '%{$pto->getSt_produto()}%'";
        }
        if ($pto->getId_tipoproduto()) {
            $arrWhere[] = " p.id_tipoproduto = {$pto->getId_tipoproduto()}";
        }
        $arrWhere[] = " p.id_entidade = " . $pto->getId_entidade();
        $arrWhere[] = " p.bl_ativo = 1";
        $where = implode(" AND ", $arrWhere);
        $select = $pORM->select()->
                setIntegrityCheck(false)
                ->from(array('p' => 'tb_produto'), array('id_produto', 'st_produto', 'id_entidade', 'bl_ativo'))
                ->join(array('tp' => 'tb_tipoproduto'), 'tp.id_tipoproduto = p.id_tipoproduto', array('st_tipoproduto'))
                ->where($where);
        return $pORM->fetchAll($select);
    }

    /**
     * Metodo que retorna Dados da Pessoa
     * @param PesquisarPessoaTO $to
     * @return Zend_Db_Table_Rowset_Abstract|string
     */
//	public function retornaPesquisaPessoa(PesquisarPessoaTO $to){
//		$vwPesquisarPessoa = new VwPesquisarPessoaORM();
//		$where = $vwPesquisarPessoa->montarWherePesquisa($to->tipaTODAO(), false, true);
//		try{
//			$obj = $vwPesquisarPessoa->fetchAll($where);
//			return $obj;
//		}catch(Zend_Exception $e){
//			$this->excecao = $e->getMessage();
//			return false;
//		}
//	}

    public function retornaPesquisaPessoa (PesquisarPessoaTO $to)
    {
        try {
            $vwPesquisarPessoa = new VwPesquisarPessoaORM();
            $to->tipaTOConsulta();
            $perfilTO = new VwPerfisEntidadeTO();
            $perfilDAO = new PerfilDAO();
            $perfilTO->setId_entidade($to->getId_entidade());
            $retornoEntidades = $perfilDAO->retornaEntidadeRecursivaPerfil($perfilTO);
            $entidadesFilhas = $to->montaDadosIN($retornoEntidades, 'id_entidade');
            $separador = ($entidadesFilhas) ? ',' : '';
            $where = "(";
            if ($to->getId_usuario()) {
                $pars = " id_usuario = " . $to->getId_usuario();
                $where .= $this->verificaAndOr($where, $pars);
            }
            if ($to->getSt_nomecompleto()) {
                $pars = "st_nomecompleto like '%" . $to->getSt_nomecompleto() . "%' collate Latin1_General_CI_AI_WS";
                $where .= $this->verificaAndOr($where, $pars);
            }
            if ($to->getSt_cpf()) {
                $pars = "st_cpf like '%" . $to->getSt_cpf() . "%'";
                $where .= $this->verificaAndOr($where, $pars);
            }
            if ($to->getSt_email()) {
                $pars = "st_email like '%" . $to->getSt_email() . "%'";
                $where .= $this->verificaAndOr($where, $pars);
            }
            if ($to->getId_situacao()) {
                $pars = "id_situacao = " . $to->getId_situacao();
                $where .= $this->verificaAndOr($where, $pars);
            }
            $where .= ")";
            if ($where == "()") {
                $where = ($to->getId_entidade()) ? " id_entidade in (" . $to->getId_entidade() . $separador . $entidadesFilhas . ") " : "";
            } else {
                $where .= " AND " . ($to->getId_entidade() ? " id_entidade in (" . $to->getId_entidade() . $separador . $entidadesFilhas . ") " : "" );
            }
            $obj = $vwPesquisarPessoa->fetchAll($where, 'st_nomecompleto');
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que retorna dados de pesquisa de perfis da entidade
     * @param PerfilTO $pTO
     * @param PerfilEntidadeTO $peTO
     * @return $obj|false Zend_Db_Table_Rowset_Abstract
     */
    public function pesquisarPerfilEntidade (PerfilTO $pTO, PerfilEntidadeTO $peTO)
    {
        $vwPesquisarPerfilEntidadeORM = new VwPesquisarPerfilEntidadeORM();
        $where = '';
        if ($pTO->getId_entidade()) {
            $where .= "id_entidade = " . $pTO->getId_entidade();
        }
        if ($pTO->getId_perfil()) {
            $where .= " AND id_perfil = " . $pTO->getId_perfil();
        }
        if ($pTO->getId_entidadeclasse()) {
            $where .= " AND id_entidadeclasse = " . $pTO->getId_entidadeclasse();
        }
        if ($pTO->getId_situacao()) {
            $where .= " AND id_situacao = " . $pTO->getId_situacao();
        }
        if ($peTO->getSt_nomeperfil() && $pTO->getSt_nomeperfil()) {
            $where .= " AND (st_nomeperfilalias like '%" . $peTO->getSt_nomeperfil() . "%' OR st_nomeperfil like '%" . $pTO->getSt_nomeperfil() . "%')";
        }
        try {
            $obj = $vwPesquisarPerfilEntidadeORM->fetchAll($where);
            return $obj;
        } catch (Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que retorna dados de Pesquisa de Organizações (Grupo, Instituição, Nucleo, Polo)
     * @param PesquisaOrganizacaoTO $poTO
     * @return $obj|false Zend_Db_Table_Rowset_Abstract
     */
    public function pesquisarOrganizacao (PesquisarOrganizacaoTO $poTO)
    {
        $vwPesquisarOrganizacaoORM = new VwPesquisarOrganizacaoORM();
        $arrWhere = array();
        $where = '';

        if ($poTO->getSt_nomeentidade()) {
            $arrWhere[] = " st_nomeentidade like '%" . $poTO->getSt_nomeentidade() . "%' ";
        }

        if ($poTO->getst_cnpj()) {
            $arrWhere[] = " st_cnpj like '%" . $poTO->getst_cnpj() . "%' ";
        }

        if ($poTO->getSt_razaosocial()) {
            $arrWhere[] = " st_razaosocial like '%" . $poTO->getSt_razaosocial() . "%' ";
        }

        if ($poTO->getId_entidadecadastro()) {
            $arrWhere[] = " id_entidadecadastro = " . $poTO->getId_entidadecadastro() . " ";
        }

        if (!empty($arrWhere)) {
            $where = implode(' AND ', $arrWhere);
        }

        try {
            $obj = $vwPesquisarOrganizacaoORM->fetchAll($where);
            return $obj;
        } catch (Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que pesquisa os fundamentos legais
     * @param PesquisarFundamentoLegalTO $pflTO
     * @return object|false Zend_Db_Table_Abstract
     */
    public function pesquisarFundamentoLegal (PesquisarFundamentoLegalTO $pflTO)
    {
        $vwFundamentoLegalORM = new VwFundamentoLegalORM();
        $where = $vwFundamentoLegalORM->montarWherePesquisa($pflTO);
        try {
            $obj = $vwFundamentoLegalORM->fetchAll($where);
            $this->commit();
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que pesquisa os afiliados
     * @param PesquisarContratoAfiliadoTO $pcaTO
     * @return object|false Zend_Db_Table_Abstract
     */
    public function pesquisarContratoAfiliado (PesquisarContratoAfiliadoTO $pcaTO)
    {
        $vwContratoAfiliadoORM = new VwContratoAfiliadoORM();
        $where = $vwContratoAfiliadoORM->montarWherePesquisa($pcaTO, false, true);
        try {
            $obj = $vwContratoAfiliadoORM->fetchAll($where);
            $this->commit();
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que pesquisa os protocolo
     * @param PesquisarProtocoloTO $pTO
     * @return object|false Zend_Db_Table_Abstract
     */
    public function pesquisarProtocolo (PesquisarProtocoloTO $pTO)
    {
        try {
            $orm = new VwPesquisarProtocoloORM();
            $where = $orm->montarWherePesquisa($pTO);
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            THROW $e;
            return false;
        }
    }

    /**
     * Método que pesquisa a disciplina
     * @param PesquisarDisciplinaTO $pdTO
     * @see PesquisarBO::pesquisarDisciplina();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function pesquisarDisciplina (PesquisarDisciplinaTO $pdTO)
    {
        $orm = new VwPesquisarDisciplinaNivelAreaORM();
        $where = "";
        $arrWhere = array();
        $arrWhere[] = "bl_ativa = 1";
        $arrWhere[] = "id_entidade = " . $pdTO->getId_entidade();
        if ($pdTO->getId_tipodisciplina()) {
            $arrWhere[] = "id_tipodisciplina = " . $pdTO->getId_tipodisciplina();
        }
        if ($pdTO->getId_nivelensino()) {
            $arrWhere[] = "id_nivelensino = " . $pdTO->getId_nivelensino();
        }
        if ($pdTO->getId_nivelensino()) {
            $arrWhere[] = "id_nivelensino = " . $pdTO->getId_nivelensino();
        }
        if ($pdTO->getId_situacao()) {
            $arrWhere[] = "id_situacao = " . $pdTO->getId_situacao();
        }
        if ($pdTO->getSt_disciplina() && $pdTO->getSt_areaconhecimento()) {
            $arrWhere[] = "(st_disciplina like '%{$pdTO->getSt_disciplina()}%' OR st_areaconhecimento like '%{$pdTO->getSt_areaconhecimento()}%') ";
        } else if ($pdTO->getSt_disciplina()) {
            $arrWhere[] = "st_disciplina like '%{$pdTO->getSt_disciplina()}%'";
        } else if ($pdTO->getSt_areaconhecimento()) {
            $arrWhere[] = "st_areaconhecimento like '%{$pdTO->getSt_areaconhecimento()}%'";
        }
        $where = implode(" AND ", $arrWhere);
        $select = $orm->select()
                ->distinct(true)
                ->from('vw_pesquisardisciplinanivelarea', '')
                ->columns(array('id_disciplina', 'st_disciplina', 'id_situacao', 'st_situacao', 'id_tipodisciplina', 'st_tipodisciplina'))
                ->where($where);
        try {
            $obj = $orm->fetchAll($select);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que pesquisa a área do conhecimento
     * @param PesquisarAreaTO $paTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function pesquisarArea (PesquisarAreaTO $paTO)
    {
        $vwAreaConhecimentoNivelSerieORM = new VwAreaConhecimentoNivelSerieORM();
        $paTO->tipaTOConsulta();
        $arrWhere = array();
        if ($paTO->getId_entidade()) {
            $arrWhere[] = "vw_areaconhecimentonivelserie.id_entidade = " . $paTO->getId_entidade();
        }
        if ($paTO->getId_situacao()) {
            $arrWhere[] = "vw_areaconhecimentonivelserie.id_situacao = " . $paTO->getId_situacao();
        }
        if ($paTO->getSt_areaconhecimento()) {
            $arrWhere[] = "vw_areaconhecimentonivelserie.st_areaconhecimento like '%" . $paTO->getSt_areaconhecimento() . "%'";
        }
        $where = implode(" AND ", $arrWhere);
        $select = $vwAreaConhecimentoNivelSerieORM->select()
                ->distinct(true)
                ->from('vw_areaconhecimentonivelserie', '')
                ->columns(array('id_areaconhecimento', 'id_entidade', 'st_areaconhecimento', 'st_descricao', 'bl_ativo', 'id_nivelensino', 'st_nivelensino', 'id_situacao', 'st_situacao'))
                ->where($where ? $where : null);
        try {
            $obj = $vwAreaConhecimentoNivelSerieORM->fetchAll($select);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que pesquisa as entidades filha de uma entidade
     * @param VwEntidadeClasseTO $ecTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function pesquisarEntidadeFilha (VwEntidadeClasseTO $ecTO)
    {
        $vwEntidadeClasseORM = new VwEntidadeClasseORM();
        $where = $vwEntidadeClasseORM->montarWherePesquisa($ecTO, false, true);
        try {
            $obj = $vwEntidadeClasseORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que pesquisa conjunto de avaliações.
     * @param PesquisarAvaliacaoConjunto $pcaTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function pesquisarConjuntoAvaliacao (PesquisarAvaliacaoConjuntoTO $pcaTO)
    {
        $vwAvalicaoConjuntoORM = new VwAvaliacaoConjuntoORM();
        $where = $vwAvalicaoConjuntoORM->montarWherePesquisa($pcaTO);
        $where = ($where ? "($where) AND id_entidade = " . $pcaTO->getSessao()->id_entidade : "id_entidade = " . $pcaTO->getSessao()->id_entidade);
        try {
            $obj = $vwAvalicaoConjuntoORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Método que pesquisa aproveitamento de disciplinas.
     * @param PesquisarAproveitamentoTO $paTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function pesquisarAproveitamento (PesquisarAproveitamentoTO $paTO)
    {
        $vwAproveitamentoORM = new VwAproveitamentoORM();
        $where = $vwAproveitamentoORM->montarWherePesquisa($paTO, false, true);

        $select = $vwAproveitamentoORM->select()
                ->distinct(true)
                ->from('vw_aproveitamento', '')
                ->columns(array('id_usuario', 'st_nomecompleto', 'id_aproveitamentodisciplina', 'id_municipio', 'sg_uf', 'id_usuariocadastro', 'dt_conclusao', 'st_cargahoraria', 'id_matricula', 'st_disciplinaoriginal', 'st_tituloexibicao'));
        if ($where) {
            $select = $select->where($where);
        }
        try {
            $obj = $vwAproveitamentoORM->fetchAll($select);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que pesquisa campanha.
     * @param PesquisarCampanhaComercialTO $pccTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function pesquisarCampanhaComercial (PesquisarCampanhaComercialTO $pccTO)
    {
        $campanhaComercialORM = new CampanhaComercialORM();
        $where = $campanhaComercialORM->montarWherePesquisa($pccTO, false, true);
        $select = $campanhaComercialORM->select()
                ->from(array('cc' => 'tb_campanhacomercial'), array('*'))
                ->setIntegrityCheck(false)
                ->join(array('tc' => 'tb_tipocampanha'), 'cc.id_tipocampanha = tc.id_tipocampanha', array('st_tipocampanha'))
                ->join(array('s' => 'tb_situacao'), 'cc.id_situacao = s.id_situacao', array('st_situacao'));
        if ($where) {

            $pwhere = explode(' AND ', $where);
            for ($x = 0; $x < count($pwhere); $x++) {
                $pwhere[$x] = 'cc.' . $pwhere[$x];
            }
            $where = implode(' AND ', $pwhere);

            $select = $select->where($where);
        }

        try {
            $obj = $campanhaComercialORM->fetchAll($select);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw $e;
            return false;
        }
    }

    /**
     * Método que pesquisa a(s) sala(s) de aula
     * @param PesquisarSalaDeAulaTO $psaTO
     * @see PesquisarBO::pesquisarSalaDeAula
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function pesquisarSalaDeAula (PesquisarSalaDeAulaTO $psaTO)
    {
        try {
            $orm = new VwPesquisaSalaDeAulaORM();
// 			$where = $orm->montarWherePesquisa($psaTO,false,true);
            $where = "";
            $arrWhere = array();
            $arrWhere[] = "id_entidade = " . $psaTO->getId_entidade();

            if ($psaTO->getSt_disciplina() && $psaTO->getSt_saladeaula()) {
                $arrWhere[] = "(st_disciplina like '%{$psaTO->getSt_disciplina()}%' OR st_saladeaula like '%{$psaTO->getSt_saladeaula()}%') ";
            } elseif ($psaTO->getSt_disciplina()) {
                $arrWhere[] = "st_disciplina like '%{$psaTO->getSt_disciplina()}%' ";
            } elseif ($psaTO->getSt_saladeaula()) {
                $arrWhere[] = "st_saladeaula like '%{$psaTO->getSt_saladeaula()}%' ";
            }

            if ($psaTO->getId_situacao()) {
                $arrWhere[] = "id_situacao = " . $psaTO->getId_situacao();
            }
            if ($psaTO->getId_tiposaladeaula()) {
                $arrWhere[] = "id_tiposaladeaula = " . $psaTO->getId_tiposaladeaula();
            }
            if ($psaTO->getId_modalidadesaladeaula()) {
                $arrWhere[] = "id_modalidadesaladeaula = " . $psaTO->getId_modalidadesaladeaula();
            }

            $where = implode(" AND ", $arrWhere);

// 			Zend_Debug::dump($where,__CLASS__.'('.__LINE__.')');exit;
            $select = $orm->select()->setIntegrityCheck(false)
                    ->distinct(true)
                    ->from('vw_pesquisasaladeaula', '')
                    ->columns(array('id_saladeaula', 'st_saladeaula', 'id_entidade', 'st_nomeentidade', 'id_disciplina', 'st_disciplina', 'id_situacao', 'st_situacao', 'id_modalidadesaladeaula', 'st_modalidadesaladeaula', 'id_tiposaladeaula', 'st_tiposaladeaula', 'dt_abertura', 'dt_encerramento', 'st_nomeprofessor'))
                    ->where($where);
            $obj = $orm->fetchAll($select);
            return $obj;
        } catch (Zend_Exception $e) {

            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Método que pesquisa as situações
     * @param SituacaoTO $sTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornaSituacao (SituacaoTO $sTO)
    {
        $situacaoORM = new SituacaoORM();
// 		$where = $situacaoORM->montarWherePesquisa($sTO->tipaTODAO(),false,true);
        $where[] = "st_tabela like '" . $sTO->getSt_tabela() . "'";
        try {
            $obj = $situacaoORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que pesquisa as evoluções
     * @param EvolucaoTO $eTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarEvolucao (EvolucaoTO $eTO)
    {
        $evolucaoORM = new EvolucaoORM();
        $where = $evolucaoORM->montarWherePesquisa($eTO->tipaTODAO(), false, true);
        try {
            $obj = $evolucaoORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que retorna o Tipo do Avaliacao
     * @param TipoAvaliacaoTO $taTO
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function retornarTipoAvaliacao (TipoAvaliacaoTO $taTO)
    {
        $tipoAvaliacaoORM = new TipoAvaliacaoORM();
        $where = $tipoAvaliacaoORM->montarWhere($taTO->tipaTODAO());
        try {
            $obj = $tipoAvaliacaoORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que retorna o Tipo do Produto
     * @param TipoProdutoTO $tpTO
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function retornarTipoProduto (TipoProdutoTO $tpTO)
    {
        $tipoProdutoORM = new TipoProdutoORM();
        $where = $tipoProdutoORM->montarWhere($tpTO->tipaTODAO());
        try {
            $obj = $tipoProdutoORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Retorna o tipo de disciplina
     * @param TipoDisciplinaTO $tdTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornaTipoDisciplina (TipoDisciplinaTO $tdTO)
    {
        $tipoDisciplinaORM = new TipoDisciplinaORM();
        $where = $tipoDisciplinaORM->montarWhere($tdTO->tipaTODAO());
        try {
            $obj = $tipoDisciplinaORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Retorna o nível de ensino
     * @param NivelEnsinoTO $neTO
     * @param string $consulta
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornaNivelEnsino (NivelEnsinoTO $neTO, $consulta = NULL)
    {
        $nivelEnsinoORM = new NivelEnsinoORM();
        $where = $nivelEnsinoORM->montarWhere($neTO);
        if ($where) {
            $where.= ' AND ' . $consulta;
        }
        try {
            $obj = $nivelEnsinoORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Retorna pessoa com perfil de professor
     * @param VwPesquisarProfessorTO $ppTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornaProfessor (VwPesquisarProfessorTO $ppTO)
    {
        $vwPesquisarProfessorORM = new VwPesquisarProfessorORM();
        $where = $vwPesquisarProfessorORM->montarWhereView($ppTO, false, true);
        try {
            $obj = $vwPesquisarProfessorORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * 
     * Pesquisar o contrato de um aluno
     * @param PesquisarContratoTO $pcTO
     * @see PesquisarBO::pesquisarContrato()
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function pesquisarContrato (PesquisarContratoTO $pcTO)
    {
        $view = new VwPesquisarContratoORM();
        $where = '';
        if ($pcTO->getId_entidade()) {
            $where .= "id_entidade = " . $pcTO->getId_entidade();
            $pcTO->setId_entidade(null);
        }
        $whereAnd = $view->montarWherePesquisa($pcTO, false, true);
        if ($whereAnd && !$where) {
            $where = $whereAnd;
        }
        if ($where && $whereAnd) {
            $where .= " AND " . $whereAnd;
        }
        try {
            $obj = $view->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Exception("Erro na camada de acesso a dados!");
            return false;
        }
    }

    /**
     * 
     * Pesquisar regra de contrato.
     * @param PesquisaRegraTO $crTO
     * @see PesquisarBO::pesquisarRegraContrato()
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function pesquisarRegraContrato (PesquisaRegraContratoTO $crTO)
    {
        $view = new ContratoRegraORM();
        $where = '';
        $where .= $view->montarWherePesquisa($crTO, false, true);
        try {
            $obj = $view->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Exception("Erro na camada de acesso a dados!");
            return false;
        }
    }

    /**
     * 
     * Pesquisar venda.
     * PS: Metodo usado no retorno de contratos pendentes no componente VendasPendentes
     * @param PesquisaVendaTO $pvTO
     * @see PesquisarBO::pesquisarVenda()
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function pesquisarVenda (PesquisarVendaTO $pvTO)
    {
        $view = new VwClienteVendaORM();
        $where = '';
        $where .= $view->montarWherePesquisa($pvTO, false, true);
        try {
            $obj = $view->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Exception("Erro na camada de a acesso a dados!");
            return false;
        }
    }

    /**
     * 
     * Pesquisa a Forma de Pagamento
     * @param PesquisaFormaPagamentoTO $fpTO
     * @see PesquisarBO::pesquisarFormaDePagamento()
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function pesquisarFormaDePagamento (PesquisaFormaPagamentoTO $fpTO)
    {
        try {
            $view = new VwFormaPagamentoORM();
            $where = $view->montarWherePesquisa($fpTO, false, true);
            $obj = $view->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Pesquisa de Assunto do Central de Ocorrências
     * @param PesquisarAssuntoCoTO $pesquisaCategoriaTO
     * @see PesquisarBO::pesquisarAssuntoCo()
     * @return Ead1_Mensageiro
     */
    public function pesquisarAssuntoCo (PesquisarAssuntoCoTO $pesquisaAssuntoCoTO)
    {
        $view = new VwAssuntoCoORM();
        $pesquisaAssuntoCoTO->tipaTOConsulta();
        $arrWhere = array();
        if ($pesquisaAssuntoCoTO->getId_entidadecadastro()) {
            $arrWhere[] = "vw_assuntoco.id_entidadecadastro = " . $pesquisaAssuntoCoTO->getId_entidadecadastro();
        }
        if ($pesquisaAssuntoCoTO->getId_situacao()) {
            $arrWhere[] = "vw_assuntoco.id_situacao = " . $pesquisaAssuntoCoTO->getId_situacao();
        }
        if ($pesquisaAssuntoCoTO->getSt_assuntoco()) {
            $arrWhere[] = "vw_assuntoco.st_assuntoco like '%" . $pesquisaAssuntoCoTO->getSt_assuntoco() . "%'";
        }
        if ($pesquisaAssuntoCoTO->getSt_assuntocopai()) {
            $arrWhere[] = "vw_assuntoco.st_assuntocopai like '%" . $pesquisaAssuntoCoTO->getSt_assuntoco() . "%'";
        }
        if ($pesquisaAssuntoCoTO->getId_tipoocorrencia()) {
            $arrWhere[] = "vw_assuntoco.id_tipoocorrencia = " . $pesquisaAssuntoCoTO->getId_tipoocorrencia();
        }
        $arrWhere[] = "vw_assuntoco.bl_ativo = 1";
        $where = implode(" AND ", $arrWhere);
        $select = $view->select()
                ->distinct(true)
                ->from('vw_assuntoco', '')
                ->columns(array('id_assuntoco', 'st_assuntoco', 'bl_ativo', 'dt_cadastro'
                    , 'id_assuntocopai', 'st_assuntocopai', 'id_situacao'
                    , 'id_tipoocorrencia', 'st_tipoocorrencia', 'st_situacao'))
                ->where($where ? $where : null)
                ->order('st_assuntoco');
        try {
            $obj = $view->fetchAll($select);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Pesquisa de Categorias de Produtos do Financeiro
     * @param PesquisarCategoriaTO $pesquisaCategoriaTO
     * @see PesquisarBO::pesquisarCategoria()
     * @return Ead1_Mensageiro
     */
    public function pesquisarCategoria (PesquisarCategoriaTO $pesquisaCategoriaTO)
    {
        $view = new VwCategoriaORM();
        $pesquisaCategoriaTO->tipaTOConsulta();
        $arrWhere = array();
        if ($pesquisaCategoriaTO->getId_entidadecadastro()) {
            $arrWhere[] = "vw_categoria.id_entidadecadastro = " . $pesquisaCategoriaTO->getId_entidadecadastro();
        }
        if ($pesquisaCategoriaTO->getId_situacao()) {
            $arrWhere[] = "vw_categoria.id_situacao = " . $pesquisaCategoriaTO->getId_situacao();
        }
        if ($pesquisaCategoriaTO->getSt_categoria()) {
            $arrWhere[] = "vw_categoria.st_categoria like '%" . $pesquisaCategoriaTO->getSt_categoria() . "%'";
        }
        if ($pesquisaCategoriaTO->getSt_categoriapai()) {
            $arrWhere[] = "vw_categoria.st_categoriapai like '%" . $pesquisaCategoriaTO->getSt_categoriapai() . "%'";
        }

        $where = implode(" AND ", $arrWhere);
        $select = $view->select()
                ->distinct(true)
                ->from('vw_categoria', '')
                ->columns(array('id_categoria', 'st_categoria', 'bl_ativo', 'dt_cadastro'
                    , 'id_categoriapai', 'st_categoriapai', 'id_situacao', 'st_situacao'))
                ->where($where ? $where : null)
                ->order('st_categoria');
        try {
            $obj = $view->fetchAll($select);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Pesquisa de Classes de Afiliados
     * @param PesquisarClasseAfiliadoTO $pesquisaClasseAfiliadoTO
     * @see PesquisarBO::pesquisarClasseAfiliado()
     * @return Ead1_Mensageiro
     */
    public function pesquisarClasseAfiliado (PesquisarClasseAfiliadoTO $pesquisaClasseAfiliadoTO)
    {
        $view = new VwClasseAfiliadoORM();
        $pesquisaClasseAfiliadoTO->tipaTOConsulta();
        $arrWhere = array();
        if ($pesquisaClasseAfiliadoTO->getId_entidade()) {
            $arrWhere[] = "vw_classeafiliado.id_entidade = " . $pesquisaClasseAfiliadoTO->getId_entidade();
        }
        if ($pesquisaClasseAfiliadoTO->getId_situacao()) {
            $arrWhere[] = "vw_classeafiliado.id_situacao = " . $pesquisaClasseAfiliadoTO->getId_situacao();
        }
        if ($pesquisaClasseAfiliadoTO->getSt_classeafiliado()) {
            $arrWhere[] = "vw_classeafiliado.st_classeafiliado like '%" . $pesquisaClasseAfiliadoTO->getSt_classeafiliado() . "%'";
        }

        $where = implode(" AND ", $arrWhere);
        $select = $view->select()
                ->distinct(true)
                ->from('vw_classeafiliado', '')
                ->columns(array('id_classeafiliado', 'st_classeafiliado', 'dt_cadastro', 'id_situacao', 'st_situacao', 'st_tipopessoa', 'st_contratoafiliado'))
                ->where($where ? $where : null)
                ->order('st_classeafiliado');
        try {
            $obj = $view->fetchAll($select);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Pesquisa de Setor
     * @param PesquisarSetorTO $pesquisaSetorTO
     * @throws Zend_Exception
     * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
     */
    public function pesquisarSetor (PesquisarSetorTO $pesquisaSetorTO)
    {
        $view = new VwSetorORM();
        $pesquisaSetorTO->tipaTOConsulta();

        $arrWhere = array();
        if ($pesquisaSetorTO->getId_entidadecadastro()) {
            $arrWhere[] = "vw_setor.id_entidadecadastro = " . $pesquisaSetorTO->getId_entidadecadastro();
        }
        if ($pesquisaSetorTO->getId_situacao()) {
            $arrWhere[] = "vw_setor.id_situacao = " . $pesquisaSetorTO->getId_situacao();
        }
        if ($pesquisaSetorTO->getSt_setor()) {
            $arrWhere[] = "vw_setor.st_setor like '%" . $pesquisaSetorTO->getSt_setor() . "%'";
        }
        if ($pesquisaSetorTO->getSt_setorpai()) {
            $arrWhere[] = "vw_setor.st_setorpai like '%" . $pesquisaSetorTO->getSt_setorpai() . "%'";
        }

        $where = implode(" AND ", $arrWhere);
        $select = $view->select()
                ->distinct(true)
                ->from('vw_setor', '')
                ->columns(array('id_setor', 'st_setor', 'id_setorpai', 'st_setorpai', 'dt_cadastro', 'id_situacao', 'st_situacao'))
                ->where($where ? $where : null)
                ->order('st_setor');
        try {
            $obj = $view->fetchAll($select);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Verifica se é AND ou OR
     * @param string $where primeira parte da pesquisa
     * @param string $pars 
     * @return string 
     */
    private function verificaAndOr ($where, $pars)
    {
        if ($where != "") {
            if ($where == "(") {
                return $where = " " . $pars;
            } elseif (substr($where, -2) == ") ") {
                return $where = " AND " . $pars;
            } else {
                return $where = " OR " . $pars;
            }
        } else {
            return '';
        }
    }

    public function pesquisarArquivoRetorno (PesquisarArquivoRetornoTO $pesquisarArquivoRetornoTo)
    {
        $table = new ArquivoRetornoORM();
        $pesquisarArquivoRetornoTo->tipaTOConsulta();
        $arrWhere = array();
// 		if($pesquisarArquivoRetornoTo->getBl_ativo()){
// 			$arrWhere[] = "tb_arquivoretorno.bl_ativo = " .$pesquisarArquivoRetornoTo->getBl_ativo();
// 		}
        if ($pesquisarArquivoRetornoTo->getId_entidade()) {
            $arrWhere[] = "tb_arquivoretorno.id_entidade = " . $pesquisarArquivoRetornoTo->getId_entidade();
        }
        if ($pesquisarArquivoRetornoTo->getSt_arquivoretorno()) {
            $arrWhere[] = "tb_arquivoretorno.st_arquivoretorno like '%" . $pesquisarArquivoRetornoTo->getSt_arquivoretorno() . "%'";
        }
        $where = implode(" AND ", $arrWhere);
        $select = $table->select()
                ->distinct(true)
                ->from('tb_arquivoretorno', '')
                ->columns(array('id_arquivoretorno', 'st_arquivoretorno', 'dt_cadastro', 'id_entidade'
                    , 'id_usuariocadastro', 'bl_ativo', 'st_banco'
                ))
                ->where($where ? $where : null)
                ->order('dt_cadastro desc');
        try {
            $obj = $table->fetchAll($select);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage());
            return false;
        }
    }

}
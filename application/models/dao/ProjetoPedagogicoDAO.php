<?php

/**
 * Classe de persistencia do projeto pedagógico com o banco de dados
 * @author Dimas Sulz <dimassulz@gmail.com>
 * @version 1.0
 *
 * @package models
 * @subpackage dao
 */
class ProjetoPedagogicoDAO extends Ead1_DAO
{

    /**
     * Método que cadastra (insert na tabela) do projeto pedagógico
     * @param ProjetoPedagogicoTO $ppTO
     * @see ProjetoPedagogicoBO::cadastrarProjetoPedagogico();
     * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
     */
    public function cadastrarProjetoPedagogico(ProjetoPedagogicoTO $ppTO)
    {
        $projetoPedagogicoORM = new ProjetoPedagogicoORM();
        $where = $ppTO->toArrayInsert();
        unset($where['dt_cadastro']);
        $this->beginTransaction();
        try {
            $id_projetoPedagogico = $projetoPedagogicoORM->insert($where);
            $this->commit();
            return $id_projetoPedagogico;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $arr['TO'] = $ppTO;
            $arr['WHERE'] = $where;
            $arr['SQL'] = $e->getMessage();
            $this->excecao = $arr;
            return false;
        }
    }

    /**
     * Metodo que cadastra o projeto pedagógico da entidade
     * @param ProjetoEntidadeTO $peTO
     * @see ProjetoPedagogicoBO::cadastrarProjetoPedagogico()
     * @throws Zend_Exception
     * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
     */
    public function cadastrarProjetoEntidade(ProjetoEntidadeTO $peTO)
    {
        $this->beginTransaction();
        try {
            $projetoEntidadeORM = new ProjetoEntidadeORM();
            $insert = $peTO->toArrayInsert();
            $id_projetoentidade = $projetoEntidadeORM->insert($insert);
            $this->commit();
            return $id_projetoentidade;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
//			THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que cadastra (insert) a área do projeto pedagógico
     * @param AreaProjetoPedagogicoTO $appTO
     * @see ProjetoPedagogicoBO::cadastrarAreaProjetoPedagogico();
     * @return boolean
     */
    public function cadastrarAreaProjetoPedagogico(AreaProjetoPedagogicoTO $appTO)
    {
        $areaProjetoPedagogicoORM = new AreaProjetoPedagogicoORM();
        $this->beginTransaction();
        try {
            $areaProjetoPedagogicoORM->insert($appTO->toArrayInsert());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que cadastra (insert) a o projeto extracurricular
     * @param AreaProjetoPedagogicoTO $appTO
     * @see ProjetoPedagogicoBO::cadastrarProjetoExtracurricular();
     * @return boolean
     */
    public function cadastrarProjetoExtracurricular(ProjetoExtracurricularTO $peTO)
    {
        $projetoExtracurricularORM = new ProjetoExtracurricularORM();
        $this->beginTransaction();
        try {
            $projetoExtracurricularORM->insert($peTO->toArrayInsert());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que cadastra (insert) o módulo do projeto pedagógico
     * @param ModuloTO $mTO
     * @see ProjetoPedagogicoBO::cadastrarModulo();
     * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
     */
    public function cadastrarModulo(ModuloTO $mTO)
    {
        $moduloORM = new ModuloORM();
        $this->beginTransaction();
        unset($mTO->arrDisciplinas);
        unset($mTO->children);
        $arr = $mTO->toArrayInsert();
        try {
            $id_modulo = $moduloORM->insert($arr);
            $this->commit();
            return $id_modulo;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que cadastra (insert) as disciplinas pertecentes a um módulo
     * @param ModuloDisciplinaTO $mdTO
     * @see ProjetoPedagogicoBO::cadastrarModuloDisciplina();
     * @return boolean
     */
    public function cadastrarModuloDisciplina(ModuloDisciplinaTO $mdTO)
    {
        $mModuloDisciplinaORM = new ModuloDisciplinaORM();
        $this->beginTransaction();
        $where = $mdTO->toArrayInsert();
        try {
            $mdTO->setId_modulodisciplina($mModuloDisciplinaORM->insert($where));
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $arr['WHERE'] = $where;
            $arr['TO'] = $mdTO;
            $arr['SQL'] = $e->getMessage();
            $this->excecao = $arr;
            return false;
        }
    }

    /**
     * Método que cadastra (insert) o(s) tipo(s) de prova
     * @param ProjetoTipoProvaTO $ptpTO
     * @see ProjetoPedagogicoBO::cadastrarProjetoTipoProva();
     * @return boolean
     */
    public function cadastrarProjetoTipoProva(ProjetoTipoProvaTO $ptpTO)
    {
        $projetoTipoProvaORM = new ProjetoTipoProvaORM();
        $this->beginTransaction();
        try {
            $projetoTipoProvaORM->insert($ptpTO->toArrayInsert());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que cadastra (insert) a recuperação de um tipo de prova
     * @param RecuperacaoTipoProvaTO $rtpTO ]
     * @see ProjetoPedagogicoBO::salvarArrayRecuperacaoTipoProva();
     * @return boolean
     */
    public function cadastrarProjetoRecuperacaoTipoProva(RecuperacaoTipoProvaTO $rtpTO)
    {
        $recuperacaoTipoProvaORM = new RecuperacaoTipoProvaORM();
        $this->beginTransaction();
        try {
            $recuperacaoTipoProvaORM->insert($rtpTO->toArrayInsert());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que cadastra (insert) o tipo de recuperação do projeto pedagógico
     * @param ProjetoRecuperacaoTO $prTO
     * @see ProjetoPedagogicoBO::cadastrarProjetoRecuperacao();
     * @return boolean
     */
    public function cadastrarProjetoRecuperacao(ProjetoRecuperacaoTO $prTO)
    {
        $projetoRecuperacaoORM = new ProjetoRecuperacaoORM();
        $this->beginTransaction();
        try {
            $projetoRecuperacaoORM->insert($prTO->toArrayInsert());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que cadastra (insert) o módulo junto com a série e o nível de ensino do projeto pedagógico
     * @param ModuloSerieNivelEnsinoTO $msneTO
     * @see ProjetoPedagogicoBO::cadastrarModuloSerieNivelEnsino();
     * @return boolean
     */
    public function cadastrarModuloSerieNivelEnsino(ModuloSerieNivelEnsinoTO $msneTO)
    {
        $moduloSerieNivelEnsinoORM = new ModuloSerieNivelEnsinoORM();
        $this->beginTransaction();
        try {
            $id_modulo = $moduloSerieNivelEnsinoORM->insert($msneTO->toArrayInsert());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que cadastra (insert) a serie e o nivel de ensino junto com o projeto pedagógico
     * @param ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO
     * @see ProjetoPedagogicoBO::cadastrarProjetoPedagogicoSerielNivelEnsino();
     * @return boolean
     */
    public function cadastrarProjetoPedagogicoSerieNivelEnsino(ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO)
    {
        $projetoPedagogicoSerieNivelEnsinoORM = new ProjetoPedagogicoSerieNivelEnsinoORM();
        $this->beginTransaction();
        try {
            $projetoPedagogicoSerieNivelEnsinoORM->insert($ppsneTO->toArrayInsert());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que cadastra a trilha de um projeto pedagógico
     * @param TrilhaTO $tTO
     * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
     */
    public function cadastrarTrilha(TrilhaTO $tTO)
    {
        $trilhaORM = new TrilhaORM();
        return $trilhaORM->insert($tTO->toArrayInsert());
    }

    /**
     * Cadastra as disciplinas de uma trilha do projeto pedagógico
     * @param TrilhaDisciplinaTO $tdTO
     * @return int
     */
    public function cadastrarTrilhaDisciplina(TrilhaDisciplinaTO $tdTO)
    {
        try {
            $this->beginTransaction();
            $trilhaDisciplinaORM = new TrilhaDisciplinaORM();
            $trilhaDisciplinaORM->insert($tdTO->toArrayInsert());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Cadastra um novo vínculo entre módulo e área de conhecimneto
     * @param ModuloAreaTO $moduloAreaTO
     * @throws Zend_Db_Exception
     * @return int
     */
    public function cadastrarModuloArea(ModuloAreaTO $moduloAreaTO)
    {
        $moduloAreaORM = new ModuloAreaORM();
        return $moduloAreaORM->insert($moduloAreaTO->toArrayInsert());
    }

    /**
     * Método que edita (update) um projeto pedagógico
     * @param ProjetoPedagogicoTO $ppTO
     * @see ProjetoPedagogicoBO::cadastrarProjetoPedagogico() | ProjetoPedagogicoBO::editarProjetoPedagogico() | ProjetoPedagogicoBO::deltarProjetoPedagogico();
     * @return boolean
     */
    public function editarProjetoPedagogico(ProjetoPedagogicoTO $ppTO)
    {
        try {
            $this->beginTransaction();
            $projetoPedagogicoORM = new ProjetoPedagogicoORM();
            $update = $ppTO->toArrayUpdate(false, array('id_projetopedagogico', 'dt_cadastro'));
            $projetoPedagogicoORM->update($update, " id_projetopedagogico = " . $ppTO->getId_projetopedagogico());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que edita (update) uma área do projeto pedagógico
     * @param AreaProjetoPedagogicoTO $appTO
     * @deprecated não utilizada 05/11/2010
     * @return boolean
     */
    public function editarAreaProjetoPedagogico(AreaProjetoPedagogicoTO $appTO)
    {
        $areaProjetoPedagogicoORM = new AreaProjetoPedagogicoORM();
        $update = $appTO->toArrayUpdate();
        $this->beginTransaction();
        try {
            $areaProjetoPedagogicoORM->update($update, " id_projetopedagogico = " . $appTO->getId_projetopedagogico() . " AND id_areaconhecimento = " . $appTO->getId_areaconhecimento());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que edita (update) um projeto extracurricular
     * @param ProjetoExtracurricularTO $peTO
     * @return boolean
     */
    public function editarProjetoExtracurricular(ProjetoExtracurricularTO $peTO)
    {
        $projetoExtracurricularORM = new ProjetoExtracurricularORM();
        $update = $peTO->toArrayUpdate(false, array('id_projetopedagogico', 'id_areaconhecimento'));
        $this->beginTransaction();
        try {
            $projetoExtracurricularORM->update($update, " id_projetopedagogico = " . $peTO->getId_projetopedagogico() . " AND id_areaconhecimento = " . $peTO->getId_areaconhecimento());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que edita (update) um módulo
     * @param ModuloTO $mTO
     * @see ProjetoPedagogicoBO::editarModulo()
     * @return boolean
     */
    public function editarModulo(ModuloTO $mTO)
    {
        $moduloORM = new ModuloORM();
        $this->beginTransaction();
        try {
            unset($mTO->arrDisciplinas);
            unset($mTO->children);
            $update = $mTO->toArrayUpdate(false, array('id_modulo', 'children', 'arrDisciplinas'));
            $moduloORM->update($update, 'id_modulo = ' . $mTO->getId_modulo());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }


    /**
     * Edita o Módulo Disciplina
     * @param ModuloDisciplinaTO $to
     * @return boolean
     */
    public function editarModuloDisciplina(ModuloDisciplinaTO $to)
    {
        $orm = new ModuloDisciplinaORM();
        $this->beginTransaction();
        try {
            $orm->update($to->toArrayUpdate(false, array('id_modulodisciplina')), 'id_modulodisciplina = ' . $to->getId_modulodisciplina());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que edita (update) o(s) tipo(s) de prova
     * @param ProjetoTipoProvaTO $ptpTO
     * @see ProjetoPedagogicoBO::editarProjetoTipoProva();
     * @return boolean
     */
    public function editarProjetoTipoProva(ProjetoTipoProvaTO $ptpTO)
    {
        $projetoTipoProvaORM = new ProjetoTipoProvaORM();
        $this->beginTransaction();
        try {
            $projetoTipoProvaORM->update($ptpTO->toArrayUpdate(false, array('id_tipoprova', 'id_projetopedagogico')), 'id_projetopedagogico = ' . $ptpTO->getId_projetopedagogico() . ' AND id_tipoprova = ' . $ptpTO->getId_tipoprova());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que edita (update) o tipo de recuperação do projeto pedagógico
     * @param ProjetoRecuperacaoTO $prTO
     * @see ProjetoPedagogicoBO::editarProjetoRecuperacao();
     * @return boolean
     */
    public function editarProjetoRecuperacao(ProjetoRecuperacaoTO $prTO)
    {
        $projetoRecuperacaoORM = new ProjetoRecuperacaoORM();
        $this->beginTransaction();
        try {
            $projetoRecuperacaoORM->update($prTO->toArrayUpdate(false, array('id_tiporecuperacao', 'id_projetopedagogico')), 'id_projetopedagogico = ' . $prTO->getId_projetopedagogico() . ' AND id_tiporecuperacao = ' . $prTO->getId_tiporecuperacao());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que edita (update) os dados da trilha
     * @param TrilhaTO $tTO
     * @param $tipo boolean informa se vai aceitar o tipo de trilha
     * @see ProjetoPedagogicoBO::editarTipoTrilha() | ProjetoPedagogicoBO::editarDadosTrilha()
     * @return boolean
     */
    public function editarDadosTrilha(TrilhaTO $tTO, $tipo = false)
    {
        $trilhaORM = new TrilhaORM();
        if ($tipo) {
            $update = $tTO->toArrayUpdate(false, array('id_trilha'));
        } else {
            $update = $tTO->toArrayUpdate(false, array('id_trilha', 'id_tipotrilha'));
        }
        $this->beginTransaction();
        try {
            $trilhaORM->update($update, 'id_trilha = ' . $tTO->getId_trilha());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Atualiza o vínculo entre um módulo e uma área de conhecimento
     * @param $moduloAreaTO
     * @return bool
     */
    public function editarModuloArea(ModuloAreaTO $moduloAreaTO)
    {
        $moduloAreaORM = new ModuloAreaORM();
        return $moduloAreaORM->update($moduloAreaTO->toArrayUpdate(), 'id_moduloarea = ' . $moduloAreaTO->getId_moduloarea()) > 0 ? true : false;
    }

    /**
     * Método que edita a trilha da Disciplina
     * @param TrilhaDisciplinaTO $tdTO
     * @see ProjetoPedagogicoBO::editarTrilhaDisciplina();
     * @return boolean
     */
    public function editarTrilhaDisciplina(TrilhaDisciplinaTO $tdTO)
    {
        $trilhaDisciplinaORM = new TrilhaDisciplinaORM();
        $this->beginTransaction();
        try {
            $trilhaDisciplinaORM->update($tdTO->toArrayUpdate(), 'id_trilha = ' . $tdTO->getId_trilha() . " AND id_disciplina = " . $tdTO->getId_disciplina());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }


    /**
     * Método que deleta a área de um projeto pedagógico
     * @param AreaProjetoPedagogicoTO $appTO
     * @see ProjetoPedagogicoBO::deletarAreaProjetoPedagogico();
     * @return boolean
     */
    public function deletarAreaProjetoPedagogico(AreaProjetoPedagogicoTO $appTO)
    {
        $areaProjetoPedagogicoORM = new AreaProjetoPedagogicoORM();
        $where = $areaProjetoPedagogicoORM->montarWhere($appTO);
        $this->beginTransaction();
        try {
            if (!$where) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $areaProjetoPedagogicoORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $arr['TO'] = $appTO;
            $arr['WHERE'] = $where;
            $arr['SQL'] = $e->getMessage();
            $this->excecao = $arr;
            return false;
        }
    }

    /**
     * Deleta o projeto pedagógico de acordo com a área do projeto
     * @param AreaProjetoPedagogicoTO $appTO
     * @see ProjetoPedagogicoBO::deletarProjetoPedagogicoSerieNivelEnsino();
     * @return boolean
     */
    public function deletarProjetoExtracurricular(ProjetoExtracurricularTO $peTO)
    {
        $projetoExtracurricularORM = new ProjetoExtracurricularORM();
        $where = 'id_areaconhecimento = ' . $peTO->getId_areaconhecimento() . ' AND id_projetopedagogico = ' . $peTO->getId_projetopedagogico();
        $this->beginTransaction();
        try {
            $projetoExtracurricularORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Deleta a série e o nivel de ensino de um determinado projeto pedagógico
     * @param ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO
     * @see ProjetoPedagogicoBO::deletarProjetoPedagogicoSerieNivelEnsino();
     * @return boolean
     */
    public function deletarProjetoPedagogicoSerieNivelEnsino(ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO)
    {
        $projetoPedagogicoSerieNivelEnsinoORM = new ProjetoPedagogicoSerieNivelEnsinoORM();
        $where = $projetoPedagogicoSerieNivelEnsinoORM->montarWhere($ppsneTO);

        $this->beginTransaction();
        try {
            if (!$where) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $projetoPedagogicoSerieNivelEnsinoORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que exclui a serie e o nivel de ensino de um módulo
     *
     * [Quem Utiliza]
     * @see ProjetoPedagogicoBO::salvarArrayModuloSerieNivelEnsino();
     *
     * @param ModuloSerieNivelEnsinoTO $msneTO
     * @return boolean
     */
    public function deletarModuloSerieNivelEnsino(ModuloSerieNivelEnsinoTO $msneTO)
    {
        $moduloSerieNivelEnsinoORM = new ModuloSerieNivelEnsinoORM();
        $where = $moduloSerieNivelEnsinoORM->consulta($msneTO);
        $this->beginTransaction();
        try {
            $moduloSerieNivelEnsinoORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que deleta as disciplinas de um módulo(exclusão lógica)
     * @param ModuloDisciplinaTO $mdTO
     * @see ProjetoPedagogicoBO::deletarDisciplinaModulo();
     * @return boolean
     */
    public function deletarDisciplinaModulo(ModuloDisciplinaTO $mdTO)
    {
        $moduloDisciplinaORM = new ModuloDisciplinaORM();
        $mdTO->tipaTODAO();
        $dados = $mdTO->toArrayUpdate(false, array('id_modulo', 'id_disciplina', 'id_serie', 'id_nivelensino'));
        $where = $moduloDisciplinaORM->montarWhere($mdTO, true);
        $this->beginTransaction();
        try {
            $moduloDisciplinaORM->update($dados, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que deleta o(s) tipo(s) de prova
     * @param ProjetoTipoProvaTO $ptpTO
     * @see ProjetoPedagogicoBO::deletarProjetoTipoProva();
     * @return boolean
     */
    public function deletarProjetoTipoProva(ProjetoTipoProvaTO $ptpTO)
    {
        $projetoTipoProvaORM = new ProjetoTipoProvaORM();
        $where = $projetoTipoProvaORM->montarWhere($ptpTO);
        $this->beginTransaction();
        try {
            if (!$where) {
                throw new Zend_Exception('Nenhum parametro do TO setado!');
            }
            $projetoTipoProvaORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que deleta o(s) tipo(s) de prova de recuperação
     * @param RecuperacaoTipoProvaTO $rtpTO
     * @return boolean
     */
    public function deletarRecuperacaoTipoProva(RecuperacaoTipoProvaTO $rtpTO)
    {
        $recuperacaoTipoProvaORM = new RecuperacaoTipoProvaORM();
        $where = $recuperacaoTipoProvaORM->montarWhere($rtpTO);
        $this->beginTransaction();
        try {
            if (!$where) {
                THROW new Zend_Exception('Nenhum parametro do TO setado!');
            }
            $recuperacaoTipoProvaORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que deleta o tipo de recuperação do projeto pedagógico
     * @param ProjetoRecuperacaoTO $prTO
     * @see ProjetoPedagogicoBO::deletarProjetoRecuperacao();
     * @return boolean
     */
    public function deletarProjetoRecuperacao(ProjetoRecuperacaoTO $prTO)
    {
        $projetoRecuperacaoORM = new ProjetoRecuperacaoORM();
        $where = 'id_projetopedagogico = ' . $prTO->getId_projetopedagogico() . ' AND id_tiporecuperacao = ' . $prTO->getId_tiporecuperacao();
        $this->beginTransaction();
        try {
            $projetoRecuperacaoORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que deleta o vinculo da trilha com a disciplina
     * @param TrilhaDisciplinaTO $tdTO
     * @see ProjetoPedagogicoBO::deletarTrilhaDisciplina();
     * @return boolean
     */
    public function deletarTrilhaDisciplina(TrilhaDisciplinaTO $tdTO)
    {
        $trilhaDisciplinaORM = new TrilhaDisciplinaORM();
        $where = $trilhaDisciplinaORM->montarWhere($tdTO);
        $this->beginTransaction();
        try {
            if (!$where) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $trilhaDisciplinaORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Faz um update na tabela tb_trilhadisciplina (seta o pre-requisito para NULL)
     * @param TrilhaTO $tTO recebe NULL
     * @param TrilhaDisciplinaTO $tdTO recebe NULL
     * @see ProjetoPedagogicoBO::editarTipoTrilha();
     * @return boolean
     */
    public function deletarVinculosTrilhaDisciplina(TrilhaTO $tTO = null, TrilhaDisciplinaTO $tdTO = null)
    {
        $trilhaDisciplinaORM = new TrilhaDisciplinaORM();
        if ($tTO->getId_trilha()) {
            $where = 'id_trilha = ' . $tTO->getId_trilha();
        } else {
            $where = 'id_disciplinaprerequisito = ' . $tdTO->getId_disciplinaprerequisito() . ' AND id_trilha = ' . $tdTO->getId_disciplinaprerequisito();
        }
        $this->beginTransaction();
        try {
            $trilhaDisciplinaORM->update('id_disciplinaprerequisito = NULL', $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Deleta um vínculo entre o módulo e a área de conhecimento
     * @param $moduloAreaTO
     * @throws Zend_Db_Exception
     * @return bool
     */
    public function deletarModuloArea(ModuloAreaTO $moduloAreaTO)
    {
        $moduloAreaORM = new ModuloAreaORM();
        $where = 'id_moduloarea = ' . $moduloAreaTO->getId_moduloarea();
        $moduloAreaORM->delete($where);
    }

    /**
     * Método que retorna os dados de projeto pedagógico
     * @param ProjetoPedagogicoTO $ppTO
     * @param $where - OPCIONAL
     * @see ProjetoPedagogicoBO::retornaVwProjetoPedagogico();
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function retornarVwProjetoPedagogico(ProjetoPedagogicoTO $ppTO, $where = NULL)
    {
        if (is_null($where)) {
            $where = $vwProjetoPedagogicoORM->montarWherePesquisa($ppTO, false, true);
        }

        $vwProjetoPedagogicoORM = new VwProjetoPedagogicoORM();
        return $vwProjetoPedagogicoORM->fetchAll($where);
    }

    /**
     * Método que retorna os dados de projeto pedagógico
     * @param ProjetoPedagogicoTO $ppTO
     * @see ProjetoPedagogicoBO::retornaProjetoPedagogico() | MaterialBO::retornarProjetosMaterial();
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function retornarProjetoPedagogico(ProjetoPedagogicoTO $ppTO, $where = null)
    {
        $projetoPedagogicoORM = new ProjetoPedagogicoORM();
        if (!$where) {
            $where = $projetoPedagogicoORM->montarWhere($ppTO);
        }
        $obj = $projetoPedagogicoORM->fetchAll($where);
        return $obj;
    }

    /**
     * Método que retorna tipo de projeto extracurricular
     * @param TipoExtracurricularTO $teTO
     * @see ProjetoPedagogicoBO::retornarTipoExtracurricular();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarTipoExtracurricular(TipoExtracurricularTO $teTO)
    {
        $tipoExtracurricularORM = new TipoExtracurricularORM();
        try {
            $obj = $tipoExtracurricularORM->fetchAll($tipoExtracurricularORM->montarWhere($teTO));
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna a serie e o nivel de ensino do projeto pedagógico
     * @param AreaProjetoPedagogicoTO $appTO
     * @see ProjetoPedagogicoBO::retornarProjetoPedagogicoSerieNivelEnsino();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarProjetoPedagogicoSerieNivelEnsino(ProjetoPedagogicoTO $ppTO)
    {
        $projetoPedagogicoSerieNivelEnsinoORM = new ProjetoPedagogicoSerieNivelEnsinoORM();
        try {
            $obj = $projetoPedagogicoSerieNivelEnsinoORM->fetchAll('id_projetopedagogico = ' . $ppTO->getId_projetopedagogico());
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna a serie e o nivel de ensino do projeto pedagógico
     * @param AreaProjetoPedagogicoTO $appTO
     * @see ProjetoPedagogicoBO::retornarProjetoExtracurricular();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarProjetoExtracurricular(ProjetoExtracurricularTO $peTO)
    {
        $projetoExtracurricularORM = new ProjetoExtracurricularORM();
        try {
            $obj = $projetoExtracurricularORM->fetchAll($projetoExtracurricularORM->montarWhere($peTO));
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna modulo projeto pedagógico
     * @param AreaProjetoPedagogicoTO $appTO
     * @see ProjetoPedagogicoBO::retornarModuloProjetoPedagogico();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarModuloProjetoPedagogico(ModuloTO $mTO)
    {
        $moduloORM = new ModuloORM();
        $mTO->bl_ativo = true;
        if ($mTO->getSt_modulo() != NULL || $mTO->getSt_tituloexibicao() != NULL) {
            $where = $moduloORM->montarWherePesquisa($mTO, false, true);
        } else {
            $where = $moduloORM->montarWhere($mTO);
        }
        try {
            $obj = $moduloORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retornar serie, nivel de ensino de um modulo do projeto pedagógico
     * @param ModuloSerieNivelEnsinoTO $msneTO
     * @see ProjetoPedagogicoBO::retornarModuloProjetoPedagogico();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarModuloSerieNivelEnsino(ModuloSerieNivelEnsinoTO $msneTO)
    {
        $moduloSerieNivelEnsinoORM = new ModuloSerieNivelEnsinoORM();
        try {
            $obj = $moduloSerieNivelEnsinoORM->fetchAll($moduloSerieNivelEnsinoORM->montarWhere($msneTO));
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que retorna as disciplinas do módulo do Projeto Pedagógico
     * @param ModuloDisciplinaTO $mdTO
     * @see ProjetoPedagogicoBO::retornarModuloDisciplina();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarModuloDisciplina(ModuloDisciplinaTO $mdTO)
    {
        $moduloDisciplinaORM = new ModuloDisciplinaORM();
        $mdTO->bl_ativo = null;
        $where = $moduloDisciplinaORM->montarWhere($mdTO);
        try {
            $obj = $moduloDisciplinaORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna as disciplina
     * @see ProjetoPedagogicoBO::retornarModuloDisciplinaStatus()
     * @throws Zend_Exception
     * @param TrilhaDisciplinaTO $tdTO
     * @param mixed $where
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function retornarTrilhaDisciplina(TrilhaDisciplinaTO $tdTO, $where = null)
    {
        try {
            $trilhaDisciplinaORM = new TrilhaDisciplinaORM();
            if (!$where) {
                $where = $trilhaDisciplinaORM->montarWherePesquisa($tdTO, false, true);
            }
            $obj = $trilhaDisciplinaORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            THROW new Zend_Exception($e->getMessage());
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Retorna os tipos de trilhas das disciplinas
     * @param TipoTrilhaTO $ttTO
     * @see ProjetoPedagogicoBO::retornarTipoTrilha();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarTipoTrilha(TipoTrilhaTO $ttTO)
    {
        $tipoTrilhaORM = new TipoTrilhaORM();
        try {
            $obj = $tipoTrilhaORM->fetchAll($tipoTrilhaORM->montarWhere($ttTO));
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna os tipo(s) de trilha(s) FIXA(S) da(s) disciplina(s)
     * @param TipoTrilhaFixaTO $ttfTO
     * @see ProjetoPedagogicoBO::retornarTipoTrilhaFixa();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarTipoTrilhaFixa(TipoTrilhaFixaTO $ttfTO)
    {
        $tipoTrilhaFixaORM = new TipoTrilhaFixaORM();
        try {
            $obj = $tipoTrilhaFixaORM->fetchAll($tipoTrilhaFixaORM->montarWhere($ttfTO));
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna as trilhas
     * @param TrilhaTO $tTO
     * @see ProjetoPedagogicoBO::retornarTrilha();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarTrilha(TrilhaTO $tTO)
    {
        $trilhaORM = new TrilhaORM();
        try {
            $obj = $trilhaORM->fetchAll($trilhaORM->montarWhere($tTO));
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna os tipos de aplicacao da avaliação do projeto pedagógico
     * @param TipoAplicacaoAvaliacaoTO $taaTO
     * @see ProjetoPedagogicoBO::retornarTipoAplicacaoAvaliacao();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarTipoAplicacaoAvaliacao(TipoAplicacaoAvaliacaoTO $taaTO)
    {
        $tipoAplicacaoAvaliacaoORM = new TipoAplicacaoAvaliacaoORM();
        try {
            $obj = $tipoAplicacaoAvaliacaoORM->fetchAll($tipoAplicacaoAvaliacaoORM->montarWhere($taaTO));
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna do projeto a duração e o tipo
     * @param ProjetoContratoDuracaoTipoTO $pcdtTO
     * @see ProjetoPedagogicoBO::retornarProjetoContratoDuracaoTipo();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarProjetoContratoDuracaoTipo(ProjetoContratoDuracaoTipoTO $pcdtTO)
    {
        $projetoContratoDuracaoTipoORM = new ProjetoContratoDuracaoTipoORM();
        try {
            $obj = $projetoContratoDuracaoTipoORM->fetchAll($projetoContratoDuracaoTipoORM->montarWhere($pcdtTO));
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna a multa e o tipo de um contrato do projeto pedagógico
     * @param ProjetoContratoDuracaoTipoTO $pcdtTO
     * @see ProjetoPedagogicoBO::retornarProjetoContratoMultaTipo();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarProjetoContratoMultaTipo(ProjetoContratoMultaTipoTO $pcmtTO)
    {
        $projetoContratoMultaTipoORM = new ProjetoContratoMultaTipoORM();
        try {
            $obj = $projetoContratoMultaTipoORM->fetchAll($projetoContratoMultaTipoORM->montarWhere($pcmtTO));
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna o valor e o tipo do contrato do projeto pedagógico
     * @param ProjetoPedagogicoValorTipoTO $ppvtTO
     * @see ProjetoPedagogicoBO::retornarProjetoPedagogicoValorTipo();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarProjetoPedagogicoValorTipo(ProjetoPedagogicoValorTipoTO $ppvtTO)
    {
        $projetoPedagogicoValorTipoORM = new ProjetoPedagogicoValorTipoORM();
        try {
            $obj = $projetoPedagogicoValorTipoORM->fetchAll($projetoPedagogicoValorTipoORM->montarWhere($ppvtTO));
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna o tipo de prova do projeto com os dados preenchidos
     * @param VwProjetoTipoProvaTO $ptpTO
     * @see ProjetoPedagogicoBO::retornarProjetoTipoProva();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarProjetoTipoProvaView(VwProjetoTipoProvaTO $ptpTO)
    {
        $projetoTipoProvaORM = new VwProjetoTipoProvaORM();
        try {
            $obj = $projetoTipoProvaORM->fetchAll('id_projetopedagogico = ' . $ptpTO->getId_projetopedagogico() . ' OR id_tipoprovaresult is null');
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna o tipo de prova
     * @param VwProjetoTipoProvaTO $ptpTO
     * @see ProjetoPedagogicoBO::retornarProjetoTipoProva();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarProjetoTipoProva(VwProjetoTipoProvaTO $ptpTO)
    {
        $projetoTipoProvaORM = new VwProjetoTipoProvaORM();
        try {
            $obj = $projetoTipoProvaORM->fetchAll($projetoTipoProvaORM->montarWhere($ptpTO));
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }


    /**
     * Método que retorna o tipo de prova do projeto pedagógico
     * @param TipoProvaTO $tpTO
     * @see ProjetoPedagogicoBO::retornarTipoProva();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarTipoProva(TipoProvaTO $tpTO)
    {
        $tipoProvaORM = new TipoProvaORM();
        try {
            $obj = $tipoProvaORM->fetchAll($tipoProvaORM->montarWhere($tpTO));
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna a recuperação do projeto pedagógico
     * @param ProjetoRecuperacaoTO $prTO
     * @see ProjetoPedagogicoBO::retornarTipoRecuperacao();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarProjetoRecuperacao(ProjetoRecuperacaoTO $prTO)
    {
        $projetoRecuperacaoORM = new ProjetoRecuperacaoORM();
        try {
            $obj = $projetoRecuperacaoORM->fetchAll($projetoRecuperacaoORM->montarWhere($prTO, true));
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna o tipo de recuperação do projeto pedagógico
     * @param TipoRecuperacaoTO $trTO
     * @see ProjetoPedagogicoBO::retornarTipoRecuperacao();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarTipoRecuperacao(TipoRecuperacaoTO $trTO)
    {
        $tipoRecuperacaoORM = new TipoRecuperacaoORM();
        try {
            $obj = $tipoRecuperacaoORM->fetchAll($tipoRecuperacaoORM->montarWhere($trTO));
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que retorna lista de areas relacionadas a projeto pedagogico
     * sem repetir as areas
     * @param VwProjetoAreaTO $vwProjetoAreaTO
     * @return VwProjetoAreaTO
     * @throws Zend_Exception
     * @author Rafael Bruno (RBD) <rafaelbruno.ti@gmail.com>
     */
    public function retornarVwProjetoAreaUnico(VwProjetoAreaTO $vwProjetoAreaTO)
    {
        try {
            $ORM = new VwProjetoAreaORM();
            $where = null;

            if (strlen($ORM->montarWhereView($vwProjetoAreaTO))) {
                $where .= $ORM->montarWhereView($vwProjetoAreaTO);
            }

            $query = $ORM->select()
                ->from(array('vw' => 'vw_projetoarea'), array(
                    'vw.st_areaconhecimento',
                    'vw.id_areaconhecimento',
                    'vw.id_entidade'
                ))->distinct()->where($where);

            $obj = $ORM->fetchAll($query);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /** Retorna todas as Áreas de Conhecimento com ou sem vínculo com um Curso
     * @param int $id_entidade
     * @param int $id_projetopedagogico
     * @throws Zend_Exception
     * @return Ambigous <multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
     */
    public function retornarVwProjetoAreaCadastro($id_entidade, $id_projetopedagogico = false)
    {

        try {
            $ORM = new VwProjetoAreaORM();

            $query = $ORM->getAdapter()->query("select DISTINCT ac.id_areaconhecimento, ac.st_areaconhecimento, ae.id_entidade , ap.id_projetopedagogico
    				, pr.st_projetopedagogico, pr.st_tituloexibicao
 from tb_areaconhecimento as ac JOIN tb_areaentidade AS ae ON ac.id_areaconhecimento = ae.id_areaconhecimento left join tb_areaprojetopedagogico as ap
 on (ac.id_areaconhecimento = ap.id_areaconhecimento and ap.id_projetopedagogico " . ($id_projetopedagogico ? " = " . $id_projetopedagogico : " is null ") . " )
left join tb_projetopedagogico as pr on (ap.id_projetopedagogico = pr.id_projetopedagogico) 
 where ac.bl_ativo = 1 and ae.id_entidade = $id_entidade order by st_areaconhecimento;");

            return Ead1_TO_Dinamico::encapsularTo($query->fetchAll(), new VwProjetoAreaTO());
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }


    }

    /**
     * Método que retorna a área do projeto pedagógico se há referência com o módulo
     * @param AreaProjetoPedagogicoTO $appTO
     * @see ProjetoPedagogicoBO::verificarAreaProjetoPedagogico()
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwAreaProjetoPedagogico(AreaProjetoPedagogicoTO $appTO)
    {
        $vwAreaProjetoPedagogicoORM = new VwAreaProjetoPedagogicoORM();
        $appORM = new AreaProjetoPedagogicoORM();
        try {
            $where = $appORM->montarWhere($appTO, true);
            $obj = $vwAreaProjetoPedagogicoORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna a dependência da disciplina do módulo
     * @param ModuloDisciplinaTO $mdTO
     * @see ProjetoPedagogicoBO::retornarDependenciaDisciplinaModulo();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarDependenciaDisciplinaModulo(ModuloDisciplinaTO $mdTO)
    {
        $vwDependenciaDisciplinaModuloORM = new VwDependenciaDisciplinaModuloORM();
        $mdORM = new ModuloDisciplinaORM();
        try {
            $obj = $vwDependenciaDisciplinaModuloORM->fetchAll($mdORM->montarWhere($mdTO));
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna as séries de um projeto pedagógico
     * @param int $idProjetoPedagogico
     */
    public function retornarSerieProjetoPedagogico($idProjetoPedagogico)
    {
        try {
            $projetoPedagogicoSerieNivelORM = new ProjetoPedagogicoSerieNivelEnsinoORM();
            $select = $projetoPedagogicoSerieNivelORM->select()->setIntegrityCheck(false)
                ->from(array("ppsne" => 'tb_projetopedagogicoserienivelensino'), '')
                ->join(array("n" => 'tb_nivelensino'), 'ppsne.id_nivelensino = n.id_nivelensino', '*')
                ->join(array("s" => 'tb_serie'), 'ppsne.id_serie = s.id_serie', '*')
                ->where('ppsne.id_projetopedagogico = ? ', $idProjetoPedagogico);
            $obj = $projetoPedagogicoSerieNivelORM->fetchAll($select);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retornar a série e o módulo
     * @param ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO
     * @see ProjetoPedagogicoBO::retornarSerieModulo();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarSerieModulo(ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO)
    {
        $vwSerieModuloORM = new VwSerieModuloORM();
        $ppsneORM = new ProjetoPedagogicoSerieNivelEnsinoORM();
        try {
            $obj = $vwSerieModuloORM->fetchAll($ppsneORM->montarWhere($ppsneTO));
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna o módulo série disciplina
     * @param ModuloSerieNivelEnsinoTO $msneTO
     * @see ProjetoPedagogicoBO::retornarModuloSerieDisciplina();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarModuloSerieDisciplina(ModuloSerieNivelEnsinoTO $msneTO)
    {
        $moduloSerieNivelEnsinoORM = new ModuloSerieNivelEnsinoORM();
        try {
            $obj = $moduloSerieNivelEnsinoORM->fetchAll($moduloSerieNivelEnsinoORM->montarWhere($msneTO));
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna os Modulos do Projeto Pedagogico
     * @param ModuloTO $mTO
     * @see ProjetoPedagogicoBO::retornarModulo();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarModulo(ModuloTO $mTO)
    {
        $moduloORM = new ModuloORM();
        try {
            $where = $moduloORM->montarWhere($mTO);
            $obj = $moduloORM->fetchAll($where, 'st_modulo ASC');
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna os Modulos e as Disciplinas
     * @param VwModuloDisciplinaProjetoTrilhaTO $vwMdptTO
     * @see ProjetoPedagogicoBO::retornarVwModuloDisciplinaProjetoTrilha() | ProjetoPedagogicoBO::retornarModuloDisciplinaStatus();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwModuloDisciplinaProjetoTrilha(VwModuloDisciplinaProjetoTrilhaTO $vwMdptTO)
    {
        $vwModuloDisciplinaProjetoTrilhaORM = new VwModuloDisciplinaProjetoTrilhaORM();
        try {
            $where = $vwModuloDisciplinaProjetoTrilhaORM->montarWhereView($vwMdptTO, false, true);
            $obj = $vwModuloDisciplinaProjetoTrilhaORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna Disciplinas de acordo com os dados da vw_modulodisciplinaprojetotrilha
     * @param VwModuloDisciplinaProjetoTrilhaTO $vwTO
     * @see ProjetoPedagogicoBO::retornarDisciplinasProjeto;
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwModuloDisciplinaProjetoTrilhaSelect(VwModuloDisciplinaProjetoTrilhaTO $vwTO)
    {
        $orm = new VwModuloDisciplinaProjetoTrilhaORM();
        try {
            $where = $orm->montarWhere($vwTO, false, true);
            $select = $orm->select()
                ->distinct(true)
                ->from('vw_modulodisciplinaprojetotrilha', '')
                ->columns(array('st_disciplina', 'nu_cargahoraria'))
                ->where($where);
            return $orm->fetchAll($select);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que Retorna os pre requisitos da disciplina
     * @param VwDisciplinaRequisitoTO $vwDrTO
     * @see ProjetoPedagogicoBO::retornarDisciplinaRequisito();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarDisciplinaRequisito(VwDisciplinaRequisitoTO $vwDrTO)
    {
        $vwDisciplinaRequisitoORM = new VwDisciplinaRequisitoORM();
        try {
            $obj = $vwDisciplinaRequisitoORM->fetchAll($vwDisciplinaRequisitoORM->montarWhereView($vwDrTO, false, true));
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Verifica se existe trilha no projeto pedagógico
     * @param TrilhaTO $tTO
     * @see ProjetoPedagogicoBO::verificarTrilha();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function verificarTrilha(TrilhaTO $tTO)
    {
        $tORM = new TrilhaORM();
        $trilhaDisciplinaORM = new TrilhaDisciplinaORM();
        try {
            $obj = $trilhaDisciplinaORM->fetchAll($tORM->montarWhere($tTO));
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna os Módulos, séries, disciplinas e disciplinas pré-requisito com base no código do projeto pedagógico
     * @param ProjetoPedagogicoTO $ppTO
     */
    public function retornarModuloSerieDisciplinaPreRequisito(VwModuloDisciplinaPreRequisitoTO $ppTO)
    {
        $orm = new VwModuloDisciplinaPreRequisitoORM();
        return $orm->fetchAll($orm->montarWherePesquisa($ppTO, false, true));
    }

    /**
     * Retorna os vínculos entre um módulo e uma área de conhecimento
     * @return Zend_Db_Table_Rowset
     */
    public function retornarModuloArea(ModuloAreaTO $moduloAreaTO)
    {
        $moduloAreaORM = new ModuloAreaORM();
        return $moduloAreaORM->fetchAll($moduloAreaORM->montarWhere($moduloAreaTO, false));
    }

    /**
     * Retorna a área do projeto nível ensino para o componente de pesquisa do projeto pedagógico (Contrato - Matrícula).
     * @param VwAreaProjetoNivelEnsinoTO $apneTO
     * @param Mixed $where
     * @see ProjetoPedagogicoBO::retonarAreaProjetoNivelEnsino();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retonarAreaProjetoNivelEnsino(VwAreaProjetoNivelEnsinoTO $apneTO, $where = null)
    {
        try {
            $orm = new VwAreaProjetoNivelEnsinoORM();
            if (!$where) {
                $where = $orm->montarWhereView($apneTO, false, true);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que retorna a view de disciplinas do modulo
     * @param VwModuloDisciplinaTO $vwMdTO
     * @param Mixed $where
     * @see ProjetoPedagogicoBO::retornarVwModuloDisciplina();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwModuloDisciplina(VwModuloDisciplinaTO $vwMdTO, $where = null)
    {
        try {
            $orm = new VwModuloDisciplinaORM();
            if (!$where) {
                $where = $orm->montarWhere($vwMdTO, false, true, false);
            }
            return $orm->fetchAll($where, array('nu_ordem', 'st_disciplina'));
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna os dados de Produto, Projeto Pedagógico, Tipo de Produto e Valor
     *
     * @see ProjetoPedagogicoBO::retornarVwProdutoProjetoTipoValor();
     *
     * @param VwProdutoProjetoTipoValorTO $vwProdutoProjetoTipoValorTO
     * @param mixed $vigente DEFAULT true (data atual), false (não olhar para datas), aceita que passe a data nesta variável
     * @return Zend_Db_Table_Rowset
     */
    public function retornarVwProdutoProjetoTipoValor(VwProdutoProjetoTipoValorTO $vwProdutoProjetoTipoValorTO, $vigente = true)
    {
        $vwProdutoProjetoTipoValorORM = new VwProdutoProjetoTipoValorORM();
        $where = "";
        $where .= $vwProdutoProjetoTipoValorORM->montarWhereView($vwProdutoProjetoTipoValorTO, false, true);
        $select = $vwProdutoProjetoTipoValorORM->select()
            ->distinct(true)
            ->from('vw_produtoprojetotipovalor', '')
            ->columns(array('id_produto', 'id_projetopedagogico', 'id_entidade', 'st_projetopedagogico',
                'st_descricaosituacao', 'bl_ativo', 'bl_turma', 'id_nivelensino', 'st_nivelensino', 'id_situacao', 'st_situacao',
                'st_produto', 'bl_ativo', 'bl_todasformas', 'bl_todascampanhas', 'id_entidadematriz', 'st_nomeentidade', 'st_tituloexibicao',
                'id_produtovalor', 'dt_inicio', 'id_tipoprodutovalor', 'st_tipoprodutovalor', 'nu_valor', 'nu_valormensal', 'nu_basepropor', 'id_contratoregra'))
            ->where($where)->order('st_projetopedagogico');
// 		Zend_Debug::dump($select->assemble());exit;
        return $vwProdutoProjetoTipoValorORM->fetchAll($select);
    }

    /**
     * Metodo que retorna a entidade da sessão e as entidades que compartilham os projetos pedagogicos com a entidade da sessão
     * @param EntidadeTO $entidadeTO
     * @param Mixed $where
     * @return Zend_Db_Table_Row_Abstract | false
     */
    public function retornarEntidadesProjetos(EntidadeTO $entidadeTO, $where = null)
    {
        try {
            $orm = new EntidadeORM();
            if (!$where) {
                $where = "pe.id_entidade = " . $entidadeTO->getId_entidade();
            }
            $select = $orm->select()->
            setIntegrityCheck(false)
                ->distinct(true)
                ->from(array("pe" => "tb_projetoentidade"), NULL)
                ->join(array("p" => "tb_projetopedagogico"),
                    "p.id_projetopedagogico = pe.id_projetopedagogico",
                    NULL)
                ->join(array("e" => "tb_entidade"),
                    "e.id_entidade = p.id_entidadecadastro OR e.id_entidade = pe.id_entidade")
                ->join(array("ee" => "tb_entidade"),
                    "ee.id_entidade = pe.id_entidade",
                    NULL)
                ->where($where);
            $entidadeDao = new EntidadeDAO();
            return $entidadeDao->retornaEntidade($entidadeTO, $select);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que retorna as disciplinas de uma entidade de acordo com os projetos pedagógicos
     * @param VwEntidadeProjetoDisciplinaTO $to
     * @param Mixed $where
     * @return Zend_Db_Table_Row_Abstract
     */
    public function retornarVwEntidadeProjetoDisciplina(VwEntidadeProjetoDisciplinaTO $to, $where = null)
    {
        try {
            $orm = new VwEntidadeProjetoDisciplinaORM();
            if (!$where) {
                $where = $orm->montarWherePesquisa($to, false, true);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Método que retorna os projetos da entidade
     * @param VwProjetoEntidadeTO $to
     * @param Mixed $where
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function retornarVwProjetoEntidade(VwProjetoEntidadeTO $to, $where = NULL)
    {
        $orm = new VwProjetoEntidadeORM();
        return $orm->fetchAll($where ? $where : $orm->montarWhereView($to, false, true));
    }

    /**
     * Método que retorna turmas disponiveis pra matricula
     * @param VwTurmasDisponiveisTO $to
     * @param Mixed $where
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function retornarVwTurmasDisponiveis(VwTurmasDisponiveisTO $to, $where = NULL)
    {
        $orm = new VwTurmasDisponiveisORM();
        return $orm->fetchAll($where ? $where : $orm->montarWhereView($to, false, true));
    }

    /**
     * Método que retorna usuários que podem ser cadastrados como coordenadores
     * @param VwUsuarioPerfilPedagogicoTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarVwUsuarioPerfilPedagogico(VwUsuarioPerfilPedagogicoTO $to)
    {
        $orm = new VwUsuarioPerfilPedagogicoORM();
        return $orm->fetchAll($orm->montarWhereView($to, false, true));
    }


    /**
     * Método que retorna Coordenador de projeto pedagogico ou de disciplinas ou professores - integração moodle
     * @param VwUsuarioPerfilEntidadeReferenciaTO $to
     */
    public function retornarVwUsuarioPerfilEntidadeReferencia(VwUsuarioPerfilEntidadeReferenciaTO $to, $where = null)
    {
        $orm = new VwUsuarioPerfilEntidadeReferenciaORM();
        if (empty($where)) {
            $where = $orm->montarWhereView($to, false, true);
        }
        return $orm->fetchAll($where, 'st_nomecompleto ASC');
//        return $orm->fetchAll($where,array('st_nomecompleto'=>'asc'));
    }

    /**
     * Método que retorna pessoas com perfil Coordenador de projeto pedagogico ou de disciplinas ou professor - integração moodle
     * @param VwUsuarioPerfilEntidadeTO $to
     */
    public function retornarVwUsuarioPerfilEntidade(VwUsuarioPerfilEntidadeTO $to)
    {
        $orm = new VwUsuarioPerfilEntidadeORM();
        return $orm->fetchAll($orm->montarWhereView($to, false, true), 'st_nomecompleto');
    }


    /**
     * Método que edita Coordenador de projeto pedagogico ou de disciplinas - integração moodle
     * @param UsuarioPerfilEntidadeReferenciaTO $to
     * @return int
     * @throws Exception
     */
    public function editarCoordenador(UsuarioPerfilEntidadeReferenciaTO $to)
    {
        try {
            $orm = new UsuarioPerfilEntidadeReferenciaORM();
            $result = $orm->update($to->toArrayUpdate(false, array('id_perfilreferencia')), $orm->montarWhere($to, true));
            return $result;
        } catch (Exception $e) {
            throw $e;
        }
    }


    /**
     * Método que decide salva Coordenador de projeto pedagogico ou de disciplinas - integração moodle
     * @param UsuarioPerfilEntidadeReferenciaTO $to
     * @return Ead1_Mensageiro
     */
    public function salvarCoordenador(UsuarioPerfilEntidadeReferenciaTO $to)
    {
        $this->beginTransaction();
        try {
            $orm = new UsuarioPerfilEntidadeReferenciaORM();
            $id = $orm->insert($to->toArrayInsert());
            $this->commit();
            return $id;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            throw $e;
        }
    }


    /**
     * Método que busca o antigo coordenador titular para setar o novo e edita o mesmo para bl_titular = 0
     * @param UsuarioPerfilEntidadeReferenciaTO $to
     * @param unknown_type $where
     * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >
     */
    public function retornaCoordenadorTitular(UsuarioPerfilEntidadeReferenciaTO $to, $where = null)
    {
        $orm = new UsuarioPerfilEntidadeReferenciaORM();
        return $orm->fetchAll($where ? $where : $orm->montarWhere($to, false));
    }

    /**
     * Método que retorna a área do projeto pedagógico
     * @param AreaProjetoPedagogicoTO $appTO
     * @see ProjetoPedagogicoBO::retornarAreaProjetoPedagogico()
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarAreasProjetoPedagogicoUnico(AreaProjetoPedagogicoTO $appTO)
    {
        try {
//			$areaProjetoPedagogicoORM = new VwAreaProjetoPedagogicoORM();
            $appORM = new AreaProjetoPedagogicoORM();
            $where = $appORM->montarWhere($appTO, true);
            $obj = $appORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }


    /**
     * Método que retorna a área do projeto pedagógico
     * @param AreaProjetoPedagogicoTO $appTO
     * @see ProjetoPedagogicoBO::retornarAreaProjetoPedagogico()
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarAreaProjetoPedagogico(AreaProjetoPedagogicoTO $appTO)
    {
        try {
//			$areaProjetoPedagogicoORM = new VwAreaProjetoPedagogicoORM();
            $appORM = new AreaProjetoPedagogicoORM();
            $where = $appORM->montarWhere($appTO, true);
            $obj = $appORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

}

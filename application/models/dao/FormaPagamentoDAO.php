<?php

/**
 * Classe de persistencia do financeiro com o banco de dados
 * @author Dimas Sulz <dimassulz@gmail.com>
 *
 * @package models
 * @subpackage dao
 */
class FormaPagamentoDAO extends FinanceiroDAO
{


    /**
     * Método que cadastra(insert) Forma de Pagamento a um produto
     * @param FormaPagamentoProdutoTO $cpTO
     * @see FormaPagamentoBO::cadastrarFormaPagamentoProduto();
     * @return boolean
     */
    public function cadastrarFormaPagamentoProduto(FormaPagamentoProdutoTO $cpTO)
    {
        $formaPagamentoProdutoORM = new FormaPagamentoProdutoORM();
        $this->beginTransaction();
        try {
            $insert = $cpTO->toArrayInsert();
            $formaPagamentoProdutoORM->insert($insert);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que deleta a FormaPagamentoProduto
     * @param FormaPagamentoProdutoTO $cpTO
     * @see ProdutoBO::deletarFormaPagamentoProduto();
     * @return boolean
     */
    public function deletarFormaPagamentoProduto(FormaPagamentoProdutoTO $cpTO)
    {
        $formaPagamentoProdutoORM = new FormaPagamentoProdutoORM();
        $this->beginTransaction();
        try {
            $where = $formaPagamentoProdutoORM->montarWhere($cpTO, true);
            if (empty($where)) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $formaPagamentoProdutoORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }


    /**
     * Lista as forma de pagamento de um produto
     * @param VwFormaPagamentoProdutoTO $vwcpTO
     * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
     */
    public function retornarVwFormaPagamentoProduto(VwFormaPagamentoProdutoTO $vwcpTO)
    {
        $vwFormaPagamentoProdutoORM = new VwFormaPagamentoProdutoORM();
        $where = $vwFormaPagamentoProdutoORM->montarWhereView($vwcpTO, false, true);
        try {
            $obj = $vwFormaPagamentoProdutoORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que cadastra(insert) uma forma de pagamento
     * @param FormaPagamentoTO $fpTO
     * @see FormaPagamentoBO::cadastrarFormaPagamento();
     * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
     */
    public function cadastrarFormaPagamento(FormaPagamentoTO $fpTO)
    {
        $formaPagamentoORM = new FormaPagamentoORM();
        $this->beginTransaction();
        try {
            $where = $fpTO->toArrayInsert();
            $id_formaPagamento = $formaPagamentoORM->insert($where);
            $this->commit();
            return $id_formaPagamento;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que cadastra (insert) a forma de pagamento para aplicações
     * @param FormaPagamentoAplicacoesTO $fpaTO
     * @see FormaPagamentoBO::cadastrarFormaPagamentoAplicacoes();
     * @return boolean
     */
    public function cadastrarFormaPagamentoAplicacoes(FormaPagamentoAplicacoesTO $fpaTO)
    {
        $formaPagamentoAplicacoesORM = new FormaPagamentoAplicacoesORM();
        $this->beginTransaction();
        try {
            $formaPagamentoAplicacoesORM->insert($fpaTO->toArrayInsert());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que cadastra os dias de vencimento da forma de pagamento
     * @param FormaDiaVencimentoTO $to
     * @return int $id_formadiavencimento
     */
    public function cadastrarFormaDiaVencimento(FormaDiaVencimentoTO $to)
    {
        $id_formadiavencimento = 0;
        try {
            $this->beginTransaction();
            $orm = new FormaDiaVencimentoORM();
            $id_formadiavencimento = $orm->insert($to->toArrayDAO());
            $this->commit();
        } catch (Zend_Exception $e) {
            $this->rollBack();
            THROW $e;
        }
        return $id_formadiavencimento;
    }

    /**
     * Método que cadastra os dias de vencimento do meio de pagamento
     * @param FormaMeioVencimentoTO $to
     * @return int $id_formadiavencimento
     */
    public function cadastrarFormaMeioVencimento(FormaMeioVencimentoTO $to)
    {
        $id_formameiovencimento = false;
        try {
            $this->beginTransaction();
            $orm = new FormaMeioVencimentoORM();
            $id_formameiovencimento = $orm->insert($to->toArrayDAO());
            $this->commit();
        } catch (Zend_Exception $e) {
            $this->rollBack();
            THROW $e;
        }
        return $id_formameiovencimento;
    }

    /**
     * Método que cadastra (insert) a forma de uma campanha comercial
     * @param CampanhaFormaPagamentoTO $cfpTO
     * @see FormaPagamentoBO::cadastrarCampanhaFormaPagamento();
     * @return boolean
     */
    public function cadastrarCampanhaFormaPagamento(CampanhaFormaPagamentoTO $cfpTO)
    {
        $campanhaFormaPagamentoORM = new CampanhaFormaPagamentoORM();
        $this->beginTransaction();
        try {
            $campanhaFormaPagamentoORM->insert($cfpTO->toArrayInsert());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que cadastra (insert) a distribuição da forma de pagamento
     * @param FormaPagamentoDistribuicaoTO $fpdTO
     * @see FormaPagamentoBO::cadastrarFormaPagamentoDistribuicao();
     * @deprecated Não existe mais a tabela
     * @return boolean
     */
    public function cadastrarFormaPagamentoDistribuicao(FormaPagamentoDistribuicaoTO $fpdTO)
    {
        $formaPagamentoDistribuicaoORM = new FormaPagamentoDistribuicaoORM();
        $this->beginTransaction();
        try {
            $formaPagamentoDistribuicaoORM->insert($fpdTO->toArrayInsert());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que cadastra (insert) a(s) parcela(s) de forma de pagamento
     * @param FormaPagamentoParcelaTO $fppTO
     * @see FormaPagamentoBO::cadastrarFormaPagamentoParcela();
     * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
     */
    public function cadastrarFormaPagamentoParcela(FormaPagamentoParcelaTO $fppTO)
    {
        $formaPagamentoParcelaORM = new FormaPagamentoParcelaORM();
        $this->beginTransaction();
        try {
            $id_formapagamento = $formaPagamentoParcelaORM->insert($fppTO->toArrayInsert());
            $this->commit();
            return $id_formapagamento;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que cadastra (insert) a forma de pagamento com os meios de pagamento e onde se decide como vai ser a divisão financeira
     * @param FormaMeioPagamentoTO $fmpTO
     * @see FormaPagamentoBO::cadastrarFormaMeioPagamento();
     * @return boolean
     */
    public function cadastrarFormaMeioPagamento(FormaMeioPagamentoTO $fmpTO)
    {
        $formaMeioPagamentoORM = new FormaMeioPagamentoORM();
        $this->beginTransaction();
        try {
            $insert = $fmpTO->toArrayInsert();
            $formaMeioPagamentoORM->insert($insert);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que cadastra (insert) meio de pagamento para integração com o Fluxos
     * @param MeioPagamentoIntegracaoBO $mpiTO
     * @see FormaPagamentoBO::cadastrarMeioPagamentoIntegracao();
     * @return boolean
     */
    public function cadastrarMeioPagamentoIntegracao(MeioPagamentoIntegracaoTO $mpiTO)
    {
        $formaMeioPagamentoIntegracaoORM = new MeioPagamentoIntegracaoORM();
        $this->beginTransaction();
        try {
            $insert = $mpiTO->toArrayInsert();
            $formaMeioPagamentoIntegracaoORM->insert($insert);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que cadastra a relacao da forma de pagamento com a aplicacao
     * @param FormaPagamentoAplicacaoRelacaoTO $fparTO
     * @return boolean
     */
    public function cadastrarFormaPagamentoAplicacaoRelacao(FormaPagamentoAplicacaoRelacaoTO $fparTO)
    {
        try {
            $this->beginTransaction();
            $orm = new FormaPagamentoAplicacaoRelacaoORM();
            $insert = $fparTO->toArrayInsert();
            $orm->insert($insert);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Db_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que edita (update) a forma de pagamento
     * @param FormaPagamentoTO $fpTO
     * @see FormaPagamentoBO::editarFormaPagamento()
     * @return boolean
     */
    public function editarFormaPagamento(FormaPagamentoTO $fpTO)
    {
        $formaPagamentoORM = new FormaPagamentoORM();
        $this->beginTransaction();
        try {
            $formaPagamentoORM->update($fpTO->toArrayUpdate(false, array('id_formapagamento')), 'id_formapagamento = ' . $fpTO->getId_formapagamento());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que edita (update) a(s) parcela(s) de forma de pagamento
     * @param FormaPagamentoParcelaTO $fppTO
     * @see FormaPagamentoBO::cadastrarFormaPagamentoParcela();
     * @return boolean
     */
    public function editarFormaPagamentoParcela(FormaPagamentoParcelaTO $fppTO)
    {
        $formaPagamentoParcelaORM = new FormaPagamentoParcelaORM();
        $this->beginTransaction();
        try {
            $formaPagamentoParcelaORM->update($fppTO->toArrayUpdate(false, array('id_formapagamentoparcela')), 'id_formapagamentoparcela = ' . $fppTO->getId_formapagamentoparcela());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que edita (update) a distribuição da forma de pagamento
     * @param FormaPagamentoDistribuicaoTO $fpdTO
     * @see FormaPagamentoBO::editarFormaPagamentoDistribuicao()
     * @deprecated Não ultiliza mais
     * @return boolean
     */
    public function editarFormaPagamentoDistribuicao(FormaPagamentoDistribuicaoTO $fpdTO)
    {
        $formaPagamentoDistribuicaoORM = new FormaPagamentoDistribuicaoORM();
        $this->beginTransaction();
        try {
            $formaPagamentoDistribuicaoORM->update($fpdTO->toArrayUpdate(false, array('id_formapagamento', 'id_meiopagamento')), 'id_formapagamento = ' . $fpdTO->getId_formapagamento() . ' AND id_meiopagamento = ' . $fpdTO->getId_meiopagamento());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que edita (update) o meio de pagamento de uma forma de pagamento
     * @param FormaMeioPagamentoTO $fmpTO
     * @see FormaPagamentoBO::editarFormaMeioPagamento()
     * @return boolean
     */
    public function editarFormaMeioPagamento(FormaMeioPagamentoTO $fmpTO)
    {
        $formaMeioPagamentoORM = new FormaMeioPagamentoORM();
        $this->beginTransaction();
        try {
            $formaMeioPagamentoORM->update($fmpTO->toArrayUpdate(false, array('id_formapagamento', 'id_meiopagamento')), 'id_formapagamento = ' . $fmpTO->getId_formapagamento() . ' AND id_meiopagamento = ' . $fmpTO->getId_meiopagamento());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que edita (update) o meio de pagamento de integracao com Fluxos
     * @param MeioPagamentoIntegracaoTO $mpiTO
     * @see FormaPagamentoBO::editarMeioPagamentoIntegracao()
     * @return boolean
     */
    public function editarMeioPagamentoIntegracao(MeioPagamentoIntegracaoTO $mpiTO)
    {
        $meioPagamentoIntegracaoORM = new MeioPagamentoIntegracaoORM();
        $this->beginTransaction();
        try {
            $meioPagamentoIntegracaoORM->update($mpiTO->toArrayUpdate(false, array('id_meiopagamentointegracao', 'id_meiopagamento')), 'id_meiopagamentointegracao = ' . $mpiTO->getId_meiopagamentointegracao() . ' AND id_meiopagamento = ' . $mpiTO->getId_meiopagamento());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que editar a relacao da forma de pagamento com a aplicacao
     * @param FormaPagamentoAplicacaoRelacaoTO $fparTO
     * @param Mixed $where
     * @return boolean
     */
    public function editarFormaPagamentoAplicacaoRelacao(FormaPagamentoAplicacaoRelacaoTO $fparTO, $where = null)
    {
        try {
            $orm = new FormaPagamentoAplicacaoRelacaoORM();
            $this->beginTransaction();
            if (!$where) {
                $where = $orm->montarWhere($fparTO, true);
            }
            $update = $fparTO->toArrayUpdate(false);
            $orm->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Db_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que deletar a relacao da forma de pagamento com a aplicacao
     * @param FormaPagamentoAplicacaoRelacaoTO $fparTO
     * @param Mixed $where
     * @return boolean
     */
    public function deletarFormaPagamentoAplicacaoRelacao(FormaPagamentoAplicacaoRelacaoTO $fparTO, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new FormaPagamentoAplicacaoRelacaoORM();
            if (!$where) {
                $where = $orm->montarWhere($fparTO, true);
            }
            $orm->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Db_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que deleta a forma de pagamento
     * @param FormaPagamentoTO $fpTO
     * @see FormaPagamentoBO::deletarFormaPagamento();
     * @return boolean
     */
    public function deletarFormaPagamento(FormaPagamentoTO $fpTO)
    {
        $formaPagamentoORM = new FormaPagamentoORM();
        $this->beginTransaction();
        try {
            $formaPagamentoORM->delete('id_formapagamento = ' . $fpTO->getId_formapagamento());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que deleta as aplicações que pertencem a essa forma de pagamento
     * @param FormaPagamentoAplicacoesTO $fpaTO
     * @see FormaPagamentoBO::deletarFormaPagamentoAplicacoes();
     * @return boolean
     */
    public function deletarFormaPagamentoAplicacoes(FormaPagamentoAplicacoesTO $fpaTO)
    {
        $formaPagamentoAplicacoesORM = new FormaPagamentoAplicacoesORM();
        $where = $formaPagamentoAplicacoesORM->montarWhere($fpaTO);
        $this->beginTransaction();
        try {
            if (empty($where)) {
                THROW new Zend_Exception('O TO não Pode Vir Vazio!');
            }
            $formaPagamentoAplicacoesORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que deleta as campanhas que pertencem a essa forma de pagamento
     * @param CampanhaFormaPagamentoTO $cfpTO
     * @see FormaPagamentoBO::deletarCampanhaFormaPagamento();
     * @return boolean
     */
    public function deletarCampanhaFormaPagamento(CampanhaFormaPagamentoTO $cfpTO)
    {
        $campanhaFormaPagamentoORM = new CampanhaFormaPagamentoORM();
        $this->beginTransaction();
        try {
            $where = "id_campanhacomercial = " . $cfpTO->getId_campanhacomercial();
            if (empty($where)) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $campanhaFormaPagamentoORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que deleta a(s) parcela(s) de forma de pagamento
     * @param FormaPagamentoParcelaTO $fppTO
     * @see FormaPagamentoBO::deletarFormaPagamentoParcela();
     * @return boolean
     */
    public function deletarFormaPagamentoParcela(FormaPagamentoParcelaTO $fppTO)
    {
        $formaPagamentoParcelaORM = new FormaPagamentoParcelaORM();
        $this->beginTransaction();
        try {
            $formaPagamentoParcelaORM->delete('id_formapagamentoparcela = ' . $fppTO->getId_formapagamentoparcela());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que deleta as distribuição da forma de pagamento
     * @param FormaPagamentoDistribuicaoTO $fpdTO
     * @see FormaPagamentoBO::deletarFormaPagamentoDistribuicao();
     * @deprecated Não utilizada mais
     * @return boolean
     */
    public function deletarFormaPagamentoDistribuicao(FormaPagamentoDistribuicaoTO $fpdTO)
    {
        $formaPagamentoDistribuicaoORM = new FormaPagamentoDistribuicaoORM();
        $this->beginTransaction();
        try {
            $formaPagamentoDistribuicaoORM->delete('id_formapagamento = ' . $fpdTO->getId_formapagamento() . ' AND id_meiopagamento = ' . $fpdTO->getId_meiopagamento());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que deleta os meios de pagamento de uma forma de pagamento
     * @param FormaMeioPagamentoTO $fmpTO
     * @see FormaPagamentoBO::deletarFormaMeioPagamento();
     * @return boolean
     */
    public function deletarFormaMeioPagamento(FormaMeioPagamentoTO $fmpTO)
    {
        $formaMeioPagamentoORM = new FormaMeioPagamentoORM();
        $where = $formaMeioPagamentoORM->montarWhere($fmpTO);
        $this->beginTransaction();
        try {
            if (empty($where)) {
                THROW new Zend_Exception('O TO não Pode Vir Vazio!');
            }
            $formaMeioPagamentoORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Db_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que exclui os dias de vencimento da forma de pagamento
     * @param FormaDiaVencimentoTO $to
     * @return Boolean $delected
     */
    public function deletarFormaDiaVencimento(FormaDiaVencimentoTO $to)
    {
        $delected = false;
        try {
            $this->beginTransaction();
            $orm = new FormaDiaVencimentoORM();
            $delected = $orm->delete($orm->montarWhere($to));
            $this->commit();
        } catch (Zend_Exception $e) {
            $this->rollBack();
            THROW $e;
        }
        return $delected;
    }

    /**
     * Método que exclui os dias de vencimento do meio de de pagamento
     * @param FormaMeioVencimentoTO $to
     * @return Boolean $delected
     */
    public function deletarFormaMeioVencimento(FormaMeioVencimentoTO $to)
    {
        $delected = false;
        try {
            $this->beginTransaction();
            $orm = new FormaMeioVencimentoORM();
            $delected = $orm->delete($orm->montarWhere($to));
            $this->commit();
        } catch (Zend_Exception $e) {
            $this->rollBack();
            THROW $e;
        }
        return $delected;
    }

    /**
     * Método que retorna a forma de pagamento
     * @param FormaPagamentoTO $fpTO
     * @param Mixed $where
     * @see ProjetoPedagogicoBO::retornarFormaPagamento();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarFormaPagamento(FormaPagamentoTO $fpTO, $where = null)
    {
        try {
            $formaPagamentoORM = new FormaPagamentoORM();
            if (!$where) {
                $where = $formaPagamentoORM->montarWhere($fpTO);
            }
            $obj = $formaPagamentoORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Db_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna a forma de pagamento por aplicaçãp
     * @param FormaPagamentoTO $fpTO
     * @param FormaPagamentoAplicacaoTO $fpaTO
     * @param Mixed $where
     * @see ProjetoPedagogicoBO::retornarFormaPagamentoPorAplicacao();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarFormaPagamentoPorAplicacao(FormaPagamentoTO $fpTO, FormaPagamentoAplicacaoTO $fpaTO, $where = null, array $aplicacoes = null)
    {
        try {
            $formaPagamentoORM = new FormaPagamentoORM();
            if (!$where) {
                $where = $formaPagamentoORM->montarWhere($fpTO);
            }

            if ($aplicacoes) {
                $sela = ' and fpar.id_formapagamentoaplicacao in (' . implode(',', $aplicacoes) . ') ';
            } elseif ($fpaTO->getId_formapagamentoaplicacao()) {
                $sela = ' and fpar.id_formapagamentoaplicacao = ' . $fpaTO->getId_formapagamentoaplicacao();
            } else {
                $sela = '';
            }
            $where = $formaPagamentoORM->select()
                ->setIntegrityCheck(false)
                ->from(array('fp' => 'tb_formapagamento'))
                ->joinInner(array('fpar' => 'tb_formapagamentoaplicacaorelacao'),
                    'fp.id_formapagamento = fpar.id_formapagamento ' . $sela . ' and fp.id_entidade = ' . $fpTO->getId_entidade(),
                    NULL)
                ->where($where)->order('st_formapagamento')
                ->where('id_situacao = 10');
            $obj = $formaPagamentoORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Método que retorna as aplicações (genérico)
     * @param AplicacoesPagamentoTO $apTO
     * @see ProjetoPedagogicoBO::retornarAplicacoesPagamento();
     * @todo estudar a possibilidade de levar esse método para RO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarAplicacoesPagamento(AplicacoesPagamentoTO $apTO)
    {
        $aplicacoesPagamentoORM = new AplicacoesPagamentoORM();
        $where = $aplicacoesPagamentoORM->montarWhere($apTO);
        try {
            $obj = $aplicacoesPagamentoORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna as aplicações (vinculo) da forma de pagamento
     * @param FormaPagamentoAplicacoesTO $fpaTO
     * @see ProjetoPedagogicoBO::retornarFormaPagamentoAplicacoes();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarFormaPagamentoAplicacoes(FormaPagamentoAplicacoesTO $fpaTO)
    {
        $formaPagamentoAplicacoesORM = new FormaPagamentoAplicacoesORM();
        $where = $formaPagamentoAplicacoesORM->montarWhere($fpaTO);
        try {
            $obj = $formaPagamentoAplicacoesORM->fetchAll($where);
            $this->commit();
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna as campanhas (vinculo) da forma de pagamento
     * @param CampanhaFormaPagamentoTO $cfpTO
     * @see ProjetoPedagogicoBO::retornarCampanhaFormaPagamento();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarCampanhaFormaPagamento(CampanhaFormaPagamentoTO $cfpTO)
    {
        $campanhaFormaPagamentoORM = new CampanhaFormaPagamentoORM();
        $where = $campanhaFormaPagamentoORM->montarWhere($cfpTO);
        try {
            $obj = $campanhaFormaPagamentoORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna o(s) meio(s) de pagamento
     * @param MeioPagamentoTO $mpTO
     * @see ProjetoPedagogicoBO::retornarMeioPagamento();
     * @todo estudar a possibilidade de levar esse método para RO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarMeioPagamento(MeioPagamentoTO $mpTO)
    {
        $meioPagamentoORM = new MeioPagamentoORM();
        $where = $meioPagamentoORM->montarWhere($mpTO);
        try {
            $obj = $meioPagamentoORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna o(s) meio(s) de pagamento integração
     * @param VwMeioPagamentoIntegracaoTO $vwmpiTO
     * @see ProjetoPedagogicoBO::retornarVwMeioPagamentoIntegracao();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwMeioPagamentoIntegracao(VwMeioPagamentoIntegracaoTO $vwmpiTO)
    {
        $vwMeioPagamentoIntegracaoORM = new VwMeioPagamentoIntegracaoORM();
        $where = $vwMeioPagamentoIntegracaoORM->montarWhere($vwmpiTO);
        try {
            $obj = $vwMeioPagamentoIntegracaoORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna a(s) forma(s) de pagamento da parcela
     * @param FormaPagamentoParcelaTO $fppTO
     * @see ProjetoPedagogicoBO::retornarFormaPagamentoParcela();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarFormaPagamentoParcela(FormaPagamentoParcelaTO $fppTO)
    {
        $formaPagamentoParcelaORM = new FormaPagamentoParcelaORM();
        $where = $formaPagamentoParcelaORM->montarWhere($fppTO);
        try {
            $obj = $formaPagamentoParcelaORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Exception("Erro ao retornar forma de pagamento parcela!");
            return false;
        }
    }

    /**
     * Método que retorna a distribuição da forma de pagamento
     * @param FormaPagamentoDistribuicaoTO $fpdTO
     * @see ProjetoPedagogicoBO::retornarFormaPagamentoDistribuicao();
     * @deprecated Não utiliza mais
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarFormaPagamentoDistribuicao(FormaPagamentoDistribuicaoTO $fpdTO)
    {
        $formaPagamentoDistribuicaoORM = new FormaPagamentoDistribuicaoORM();
        $where = $formaPagamentoDistribuicaoORM->montarWhere($fpdTO);
        try {
            $obj = $formaPagamentoDistribuicaoORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna o meio de pagamento de uma forma de pagamento
     * @param FormaMeioPagamentoTO $fmpTO
     * @see ProjetoPedagogicoBO::retornarFormaMeioPagamento();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarFormaMeioPagamento(FormaMeioPagamentoTO $fmpTO)
    {
        $formaMeioPagamentoORM = new FormaMeioPagamentoORM();
        $where = $formaMeioPagamentoORM->montarWhere($fmpTO);
        try {
            $obj = $formaMeioPagamentoORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna o tipo da divisao financeira de uma forma de pagamento
     * @param VwFormaMeioDivisaoTO $fmdfTO
     * @see ProjetoPedagogicoBO::retornarTipoDivisaoFinanceira();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarTipoDivisaoFinanceira(VwFormaMeioDivisaoTO $fmdfTO)
    {
        $vwFormaMeioDivisaoORM = new VwFormaMeioDivisaoORM();
        $where = $vwFormaMeioDivisaoORM->montarWhereView($fmdfTO);
        try {
            $obj = $vwFormaMeioDivisaoORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna o tipo da divisao financeira de uma forma de pagamento
     * @param TipoFormaPagamentoParcelaTO $tfppTO
     * @see ProjetoPedagogicoBO::retornarTipoFormaPagamentoParcela();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarTipoFormaPagamentoParcela(TipoFormaPagamentoParcelaTO $tfppTO)
    {
        $tipoFormaPagamentoParcelaORM = new TipoFormaPagamentoParcelaORM();
        $where = $tipoFormaPagamentoParcelaORM->montarWhere($tfppTO);
        try {
            $obj = $tipoFormaPagamentoParcelaORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna o vinculo de forma de pagamento, com o tipo de divisão e o meio de pagamento
     * @param FormaPagamentoTO $fpTO
     * @see ProjetoPedagogicoBO::retornarParcelasMeioPagamento();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarDivisaoFormaMeioPagamento(FormaPagamentoTO $fpTO)
    {
        $view = new VwDivisaoFormaMeioPagamentoORM();
        $where = $view->montarWhereView($fpTO);
        try {
            $obj = $view->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Exception("Erro ao retornar a view de divisão de forma meio pagamento!");
            return false;
        }
    }

    /**
     * Metodo que retorna a relação da aplicação da forma de pagamento
     * @param FormaPagamentoAplicacaoTO $fpaTO
     * @param Mixed $where
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarFormaPagamentoAplicacao(FormaPagamentoAplicacaoTO $fpaTO, $where = null)
    {
        try {
            $orm = new FormaPagamentoAplicacaoORM();
            if (!$where) {
                $where = $orm->montarWhere($fpaTO);
            }
            $obj = $orm->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Db_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que retorna a relacao da forma de pagamento com a aplicacao
     * @param FormaPagamentoAplicacaoTO $fpaTO
     * @param Mixed $where
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarFormaPagamentoAplicacaoRelacao(FormaPagamentoAplicacaoRelacaoTO $fparTO, $where = null)
    {
        try {
            $orm = new FormaPagamentoAplicacaoRelacaoORM();
            if (!$where) {
                $where = $orm->montarWhere($fparTO);
            }
            $obj = $orm->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Db_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna os dias de vencimento da forma de pagamento
     * @param FormaDiaVencimentoTO $to
     * @param Mixed $where
     * @return Zend_Db_Table_Rowset
     */
    public function retornarFormaDiaVencimento(FormaDiaVencimentoTO $to, $where = null)
    {
        $orm = new FormaDiaVencimentoORM();
        return $orm->fetchAll($where ? $where : $orm->montarWhere($to));
    }

    /**
     * Método que retorna os dias de vencimento do meio de pagamento
     * @param FormaMeioVencimentoTO $to
     * @param Mixed $where
     * @return Zend_Db_Table_Rowset
     */
    public function retornarFormaMeioVencimento(FormaMeioVencimentoTO $to, $where = null)
    {
        $orm = new FormaMeioVencimentoORM();
        return $orm->fetchAll($where ? $where : $orm->montarWhere($to));
    }

    /**
     * Método que retorna os dias de vencimento do meio de pagamento com dados mais completos
     * @param FormaMeioVencimentoTO $to
     * @param Mixed $where
     * @return Zend_Db_Table_Rowset
     */
    public function retornarFormaMeioVencimentoCompleto(FormaMeioVencimentoTO $to, $where = null)
    {
        $orm = new FormaMeioVencimentoORM();
        $select = $orm->select()
            ->from(array('fmv' => 'tb_formameiovencimento'), array('*'))
            ->setIntegrityCheck(false)
            ->join(array('mp' => 'tb_meiopagamento'), 'fmv.id_meiopagamento = mp.id_meiopagamento', array('st_meiopagamento'));

        if (!$where) {
            $where = $orm->montarWhere($to);
            if ($where) {
                $where = 'fmv.' . $where;
            }
        }
        $obj = $orm->fetchAll($select->where($where));
        return $obj;
    }

    /**
     * Método que retorna a vw_formameiopagamento
     * @param VwFormaMeioPagamentoTO $vwFormaMeioPagamentoTO
     */
    public function retornarVwFormaMeioPagamento(VwFormaMeioPagamentoTO $vwFormaMeioPagamentoTO)
    {
        $orm = new VwFormaMeioPagamentoORM();
        return $orm->fetchAll($orm->montarWhere($vwFormaMeioPagamentoTO));
    }
}

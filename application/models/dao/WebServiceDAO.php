<?php
/**
 * Classe de consultas específicas do Web Service
 * @author edermariano
 *
 */
class WebServiceDAO extends Ead1_DAO{
	
	/**
	 * Método que retorna a forma de pagamento
	 */
	public function retornarFormaPagamento($id){
		$orm = new FormaPagamentoORM();
		$select = $orm->select()
			->setIntegrityCheck(false)
			->from(array("fp" => "tb_formapagamento"), '*')
			->join(array("fpar"=>"tb_formapagamentoaplicacaorelacao"), "fp.id_formapagamento = fpar.id_formapagamento","*")
			->where("fp.id_entidade = $id")
			->where("fpar.id_formapagamentoaplicacao = 5"); // SITE
			
		return $orm->fetchRow($select);
	}
	
	/**
	 * Método que retorna os meios de pagamento
	 */
	public function retornarMeioPagamento(){
		
	}
}
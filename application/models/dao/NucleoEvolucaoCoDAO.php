<?php
/**
 * Classe de acesso a dados referentes ao NucleoEvolucaoCo
 * @package models
 * @subpackage dao
 */
class NucleoEvolucaoCoDAO extends Ead1_DAO {
	
	/**
	 * Método que lista evoluçoes do Nucleo
	 * @param NucleoEvolucaoCoTO $nucleoEvolucao
	 * @return Ambigous <boolean, multitype:>
	 * @see NucleoCoBO::retornaNucleoEvolucaoCo
	 */
	public function listarNucleoEvolucaoCo(NucleoEvolucaoCoTO $nucleoEvolucao){
		$orm = new NucleoEvolucaoCoORM();
		return $orm->consulta($nucleoEvolucao);
	}
	
	/**
	 * Método que salva o vinculo das evolucoes com o nucleo
	 * @param NucleoEvolucaoCoTO $nucleoEvolucao
	 * @throws Zend_Exception
	 * @return Ambigous <mixed, multitype:>
	 * @see NucleoCoBO::salvarNucleoEvolucaoCo
	 */
	public function salvarNucleoEvolucaoco(NucleoEvolucaoCoTO $nucleoEvolucao){
		$this->beginTransaction();
		try{
			$nucleo = new NucleoEvolucaoCoORM();
			$id = $nucleo->insert($nucleoEvolucao->toArrayInsert());
			$this->commit();
			return $id;
		}catch(Zend_Exception $e){
			$this->rollBack();
			throw $e;
		}
	}
	
	/**
	 * Método para edição - (não usado)
	 * @param NucleoEvolucaoCoTO $nucleoEvolucao
	 * @throws Zend_Exception
	 */
	public function editarNucleoEvolucaoco(NucleoEvolucaoCoTO $nucleoEvolucao){
		$this->beginTransaction();
		try {
			$nucleo = new NucleoEvolucaoCoORM();
			$nucleo->update($nucleoEvolucao->toArrayUpdate(false, array('id_nucleoco')), $nucleo->montarWhere($nucleoEvolucao, true));
			$this->commit();
		} catch (Zend_Exception $e) {
			$this->rollBack();
			throw $e;
		}
	}
	
	
	/**
	 * Deleta vinculo das evoluções com o nucleo
	 * @param unknown_type $id_nucleoco
	 * @throws Zend_Exception
	 * @see NucleoCoBO::salvarNucleoEvolucaoCo
	 */
	public function deletarEvolucoesNucleo($id_nucleoco){
		$this->beginTransaction();
		try{
			$orm = new NucleoEvolucaoCoORM();
			$orm->delete("id_nucleoco = $id_nucleoco");
			$this->commit();
		}catch(Zend_Exception $e){
			$this->rollBack();
			throw $e;
		}
	}
}

?>
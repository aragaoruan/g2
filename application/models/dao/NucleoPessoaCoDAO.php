<?php
/**
 * Classe NucleoPessoaCoDAO
 * @package models
 * @subpackage dao
 */
class NucleoPessoaCoDAO extends Ead1_DAO {

	/**
	 * Método que insere o NucleoPessoaCo
	 * @param NucleoPessoaCoTO $NuclePessoaCoTo
	 * @throws Zend_Exception
	 * @return Ambigous <mixed, multitype:>
	 */
	public function cadastrarNucleoPessoaCo(NucleoPessoaCoTO $NucleoPessoaCoTo){
		$this->beginTransaction();
		try{
			$nucleo = new NucleoPessoaCoORM();
			$id_nucleoassuntoco = $nucleo->insert($NucleoPessoaCoTo->toArrayInsert());
			$this->commit();
			return $id_nucleoassuntoco;
		}catch(Zend_Exception $e){
			$this->rollBack();
			throw $e;
		}	
	}
	
	/**
	 * Método de edição do NucleoPessoaCo
	 * @param NucleoPessoaCoTO $NucleoPessoaCoTo
	 * @throws Zend_Exception
	 */
	public function editarNucleoPessoaCo(NucleoPessoaCoTO $NucleoPessoaCoTo){
		$this->beginTransaction();
		try {
			$nucleo = new NucleoPessoaCoORM();
			$nucleo->update($NucleoPessoaCoTo->toArrayUpdate(true, array('id_nucleopessoaco')), $nucleo->montarWhere($NucleoPessoaCoTo, true));
			$this->commit();
		} catch (Zend_Exception $e) {
			$this->rollBack();
			throw $e;
		}
	}
	
	/**
	 * Método que exclui vinculo pessoa c/ nucleo
	 * @param unknown_type $idnucleoPessoa
	 * @throws Zend_Exception
	 */
	public function excluirNucleoPessoaCo($idnucleoPessoa){
		$this->beginTransaction();
		try{
			$orm = new NucleoPessoaCoORM();
			$orm->delete("id_nucleopessoaco = $idnucleoPessoa");
			$this->commit();
		}catch(Zend_Exception $e){
			$this->rollBack();
			throw $e;
		}
	}
	
	
}
<?php

/**
 * Classe de acesso a dados referente a TCCDAO
 * @author Elcio Mauo Guimarães - <elcioguimaraes@gmail.com>
 * @since 24/11/2012
 * @package models
 * @subpackage dao
 */
class TCCDAO extends Ead1_DAO {

	/**
	 * Método que insere o ParametroTCC
	 * 
	 * @param ParametroTCCTO $to
	 * @throws Zend_Exception
	 * @return Ambigous <mixed, multitype:>
	 * @see ParametroTCCBO::salvarParametroParametroTCC();
	 */
	public function cadastrarParametroTCC(ParametroTCCTO $to){
		$this->beginTransaction();
		try{
			$orm = new ParametroTCCORM();
			$id_parametrotcc = $orm->insert($to->toArrayInsert());
			$this->commit();
			return $id_parametrotcc;
		}catch(Zend_Exception $e){
			$this->rollBack();
			throw $e;
			return false;
		}	
	}
	
	/**
	 * Método de edição do ParametroTCC
	 * @param ParametroTCCTO $to
	 * @throws Zend_Exception
	 * @see ParametroTCCBO::salvarParametroParametroTCC();
	 */
	public function editarParametroTCC(ParametroTCCTO $to){
		$this->beginTransaction();
		try {
			$orm = new ParametroTCCORM();
			$orm->update($to->toArrayUpdate(false, array('id_parametrotcc')), $orm->montarWhere($to, true));
			$this->commit();
			return true;
		} catch (Zend_Exception $e) {
			$this->rollBack();
			throw $e;
			return false;
		}
	}
	
	/**
	 * Retorna o Peso total de parametros de um TCC
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @param unknown_type $id_projetopedagogico
	 * @throws Exception
	 * @return multitype:|boolean
	 */
	public function retornaTotalPesoParametroTCC(ParametroTCCTO $to){
		
		try {
			
			$orm 	= new ParametroTCCORM();
			$sql 	= $orm->select()->from($orm, array('SUM(nu_peso) as total', 'id_projetopedagogico'))
									->where('bl_ativo = 1')
									->where('id_projetopedagogico = '.$to->getId_projetopedagogico())
									->group('id_projetopedagogico');
			if($to->getId_parametrotcc()){
				$sql->where('id_parametrotcc != '.$to->getId_parametrotcc());
			}
			
			$linha  = $orm->fetchRow($sql);
			
			if($linha){
				return $linha->toArray();
			}
			return false;
			
		} catch (Exception $e) {
			throw $e;
			return false;
		}
		
	} 
	
	/**
	 * Método de retorno de ParametroTCC
	 * @param ParametroTCCTO $to
	 * @return Ambigous <boolean, multitype:>
	 * @see ParametroTCCBO::retornarParametroTCC();
	 */
	public function retornarParametroTCC(ParametroTCCTO $to){
		$orm = new ParametroTCCORM();
		return $orm->consulta($to);
	}
	

	/**
	 * Método que insere o AvaliacaoParametros
	 * 
	 * @param AvaliacaoParametrosTO $AvaliacaoParametrosTO
	 * @throws Zend_Exception
	 * @return Ambigous <mixed, multitype:>
	 * @see AvaliacaoParametrosBO::salvarAvaliacaoParametros();
	 */
	public function cadastrarAvaliacaoParametros(AvaliacaoParametrosTO $to){
		$this->beginTransaction();
		try{
			$orm = new AvaliacaoParametrosORM();
			$id_avaliacaoparametros = $orm->insert($to->toArrayInsert());
			$this->commit();
			return $id_avaliacaoparametros;
		}catch(Zend_Exception $e){
			$this->rollBack();
			throw $e;
			return false;
		}	
	}
	
	/**
	 * Método de edição do AvaliacaoParametros
	 * @param AvaliacaoParametrosTO $to
	 * @throws Zend_Exception
	 * @see AvaliacaoParametrosBO::salvarAvaliacaoParametros();
	 */
	public function editarAvaliacaoParametros(AvaliacaoParametrosTO $to){
		$this->beginTransaction();
		try {
			$orm = new AvaliacaoParametrosORM();
			$orm->update($to->toArrayUpdate(false, array('id_avaliacaoparametros')), $orm->montarWhere($to, true));
			$this->commit();
			return true;
		} catch (Zend_Exception $e) {
			$this->rollBack();
			throw $e;
			return false;
		}
	}
	
	/**
	 * Método de listagem de AvaliacaoParametros
	 * @param AvaliacaoParametrosTO $to
	 * @return Ambigous <boolean, multitype:>
	 * @see AvaliacaoParametrosBO::listarAvaliacaoParametros();
	 */
	public function retornarAvaliacaoParametros(AvaliacaoParametrosTO $to){
		$orm = new AvaliacaoParametrosORM();
		return $orm->consulta($to);
	}
	
	
	/**
	 * Retorna os dados da Vw_AvaliacaoParametroTCC
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @param VwAvaliacaoParametroTCCTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornaVwAvaliacaoParametroTCC(VwAvaliacaoParametroTCCTO $to){

		$orm = new VwAvaliacaoParametroTCCORM();

		try {
			$arrayTO = $orm->consulta($to, false, false, array('id_parametrotcc'));
			if($arrayTO){
				return new Ead1_Mensageiro($arrayTO, Ead1_IMensageiro::SUCESSO);
			} else {
				return new Ead1_Mensageiro('Nenhum parâmetro de TCC encontrado para este Projeto Pedagógico!', Ead1_IMensageiro::AVISO);
			}
	
		} catch (Exception $e) {
			return new Ead1_Mensageiro('Erro ao retornar os dados da Vw_AvaliacaoParametroTCC: '.$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	}
}


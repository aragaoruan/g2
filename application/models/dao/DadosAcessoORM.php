<?php
/**
 * Classe de mapeamento objeto relacional da tabela  
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage orm
 */
 
class DadosAcessoORM extends Ead1_ORM{
	public $_name = 'tb_dadosacesso';
	public $_primary = array('id_dadosacesso');
}
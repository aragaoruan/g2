<?php
/**
 * Classe de acesso a dados referente a CategoriaOcorrencia
 * @author Denise Xavier
 * @package models
 * @subpackage dao
 *
 */
class CategoriaOcorrenciaDAO extends Ead1_DAO {
		
	/**
	 * Método que lista CategoriaOcorrencia
	 * @see 
	 * @param CategoriaOcorrenciaTO $categoriaOcorrencia
	 * @return Ambigous <boolean, multitype:>
	 */
	public function listarCategoriaOcorrencia(CategoriaOcorrenciaTO $categoriaOcorrencia){
		$orm = new CategoriaOcorrenciaORM();
		return $orm->consulta($categoriaOcorrencia);
	}
	
	
	/**
	 * Método que lista VwCategoriaOcorrenciaTO
	 * @see NucleoCoBO::retornaCategoriaOcorrencia
	 * @param VwCategoriaOcorrenciaTO $vwCategoriaOcorrencia
	 * @return Ambigous <boolean, multitype:>
	 */
	public function listarVwCategoriaOcorrencia(VwCategoriaOcorrenciaTO $vwCategoriaOcorrencia){
		$orm = new VwCategoriaOcorrenciaORM();
		return $orm->consulta($vwCategoriaOcorrencia);
	}
	
	/**
	 * Método que salva CategoriaOcorrencia
	 * @param CategoriaOcorrenciaTO $categoriaOcorrencia
	 * @throws Zend_Exception
	 * @return Ambigous <mixed, multitype:>
	 * @see NucleoCoBO::salvarCategoriaOcorrencia
	 */
	public function salvarCategoriaOcorrencia(CategoriaOcorrenciaTO $categoriaOcorrencia){
		$this->beginTransaction();
		try{
			$orm = new CategoriaOcorrenciaORM();
			$id_categoriaOcorrencia = $orm->insert($categoriaOcorrencia->toArrayInsert());
			$this->commit();
			return $id_categoriaOcorrencia;
		}catch(Zend_Exception $e){
			$this->rollBack();
			throw $e;
		}
	}
	
	
	/**
	 * Método que edita CategoriaOcorrencia
	 * @param CategoriaOcorrenciaTO $categoriaOcorrencia
	 * @throws Zend_Exception
	 * @see NucleoCoBO::salvarCategoriaOcorrencia
	 */
	public function editarCategoriaOcorrencia(CategoriaOcorrenciaTO $categoriaOcorrencia){
		$this->beginTransaction();
		try {
			$orm = new CategoriaOcorrenciaORM();
			$orm->update($categoriaOcorrencia->toArrayUpdate(false, array('id_categoriaocorrencia')), $orm->montarWhere($categoriaOcorrencia, true));
			$this->commit();
		} catch (Zend_Exception $e) {
			$this->rollBack();
			throw $e;
		}
	}
	
	/**
	 * Método que verifica se existe CategoriaOcorrencia por (st_categoriaocorrencia/ id_situcao/ id_tipoocorrencia/ bl_ativo = 0 / id_entidadecadastro)
	 * para reativa-la 
	 * @param CategoriaOcorrenciaTO $categoriaOcorrencia
	 * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >
	 */
	public function verificaTipoOcorrencia(CategoriaOcorrenciaTO $categoriaOcorrencia){
		$orm = new CategoriaOcorrenciaORM();
		$select = $orm->select()->where('st_categoriaocorrencia = ?', $categoriaOcorrencia->getSt_categoriaocorrencia())
							->where('id_situacao = ?', $categoriaOcorrencia->getId_situacao())
							->where('id_tipoocorrencia = ? ', $categoriaOcorrencia->getId_tipoocorrencia())
							->where('id_entidadecadastro =? ', $categoriaOcorrencia->getId_entidadecadastro())
							->where('bl_ativo = ? ', 0);
		return $orm->fetchAll($select);
	}
}

?>
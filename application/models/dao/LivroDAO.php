<?php

/**
 * @author Elcio Mauro Guimarães
 * @package models
 * @subpackage dao
 */
class LivroDAO extends Ead1_DAO{
	
	
	/**
	 * Método que retorna os livros
	 * @param LivroTO $to
	 * @param Mixed $where
	 */
	public function retornarLivro(LivroTO $to, $where = NULL){
		$orm = new LivroORM();
		return $orm->fetchAll($where ? $where : $orm->montarWhere($to,false))->toArray();
	}

	/**
	 * Método que retorna os livros
	 * @param VwPesquisarLivroTO $to
	 * @param Mixed $where
	 */
	public function retornarVwPesquisarLivro(VwPesquisarLivroTO $to, $where = NULL){
		$orm = new VwPesquisarLivroORM();
		
		$arrWhere = false;
		
		if($to->getSt_livro()){
			$arrWhere[] = "st_livro like '%".$to->getSt_livro()."%'";
		}
		if($to->getSt_codigocontrole()){
			$arrWhere[] = "st_codigocontrole like '%".$to->getSt_codigocontrole()."%'";
		}
		if($to->getSt_livrocolecao()){
			$arrWhere[] = "st_livrocolecao like '%".$to->getSt_livrocolecao()."%'";
			$to->setSt_livrocolecao(null);
		}
		if($to->getSt_tipolivro()){
			$arrWhere[] = "st_tipolivro like '%".$to->getSt_tipolivro()."%'";
		}

		$to->setSt_codigocontrole(null);
		$to->setSt_tipolivro(null);
		$to->setSt_tipolivro(null);
		$to->setSt_livro(null);
		
		if($arrWhere){
			$where = "( ".implode(" OR ", $arrWhere)." ) ";
		}
		
		if($where) { 
			
			
			$conWhere = $orm->montarWhere($to,false);
			if($conWhere)
				$where .= " AND ". $conWhere;
			
			
		} else {
			$where = $orm->montarWhere($to,false);
		}
		
		return $orm->fetchAll($where, 'st_livro');
	}
	
	/**
	 * Método que retorna os tipos de livros 
	 * @param TipoLivroTO $to
	 * @param Mixed $where
	 */
	public function retornarTipoLivro(TipoLivroTO $to, $where = NULL){
		$orm = new TipoLivroORM();
		return $orm->fetchAll($where ? $where : $orm->montarWhere($to,false), 'st_tipolivro');
	}
	
	/**
	 * Método para salvar um livro
	 * @param LivroTO $to
	 * @param array $where
	 * @throws Zend_Exception
	 * @return LivroTO
	 */
	public function salvarLivro(LivroTO $to, array $where = null){
		$this->beginTransaction();
		try{
			
			$orm = new LivroORM();
			
			if($to->getId_livro() || $where){
				
				if(!$where){
					$where[] = 'id_livro = '. (int)$to->getId_livro();
				}
				
				$orm->update($to->toArrayUpdate(false, array('id_livro','id_usuariocadastro','id_entidadecadastro')), $where);
				$to->fetch(true, true, true);
			} else {
				
				$to->setBl_ativo(1);
				$id_livro = $orm->insert($to->toArrayInsert());
				$to->setId_livro($id_livro);
				$to->fetch(true, true, true);
			}
			
			$this->commit();
			return $to;
		}catch (Zend_Exception $e){
			$this->rollBack();
			THROW $e;
		}
	}

	
	
	/**
	 * Método de vínculo de livro com entidade
	 * @param LivroEntidadeTO $livroEntidadeTO
	 * @throws Zend_Exception
	 */
	public function cadastrarLivroEntidade(LivroEntidadeTO $livroEntidadeTO){
		$this->beginTransaction();
		try{
			$orm = new LivroEntidadeORM();
			$orm->insert($livroEntidadeTO->toArrayInsert());
			$this->commit();
		}catch(Zend_Exception $e){
			$this->rollBack();
			throw $e;
		}	
	}
	
	/**
	 * Método que deleta o livro entidade
	 * @see LivroBO::salvarArLivroEntidade();
	 * 
	 * @param integer $idLivro
	 * @throws Zend_Exception
	 */
	public function deletarLivroEntidadePorLivro($idLivro){
		$this->beginTransaction();
		try{
			$orm = new LivroEntidadeORM();
			$orm->delete("id_livro = $idLivro");
			$this->commit();
		}catch(Zend_Exception $e){
			$this->rollBack();
			throw $e;
		}	
	}
	
	/**
	 * Método que retorna as entidades do livro em questão
	 *
	 * @see LivroRO::retornarArLivroEntidade()
	 * @param LivroTO $livroTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarArLivroEntidade(LivroTO $livroTO){
		$orm = new LivroEntidadeORM();
		return $orm->fetchAll("id_livro = " . $livroTO->getId_livro());
	}
	
	
	/**
	 * Método que retorna as Coleções
	 * @param LivroColecaoTO $to
	 * @param Mixed $where
	 */
	public function retornarLivroColecao(LivroColecaoTO $to, $where = NULL){
		$orm = new LivroColecaoORM();
		return $orm->fetchAll($where ? $where : $orm->montarWhere($to,false), 'st_livrocolecao');
	}
	
	/**
	 * Método para salvar uma Coleção
	 * @param LivroColecaoTO $to
	 * @param array $where
	 * @throws Zend_Exception
	 * @return LivroTO
	 */
	public function salvarLivroColecao(LivroColecaoTO $to, array $where = null){
		$this->beginTransaction();
		try{
			
			$orm = new LivroColecaoORM();
			
			if($to->getId_LivroColecao() || $where){
				
				if(!$where){
					$where[] = 'id_livrocolecao = '. (int)$to->getId_livrocolecao();
				}
				
				$orm->update($to->toArrayUpdate(false, array('id_livrocolecao','id_usuariocadastro','id_entidadecadastro')), $where);
				$to->fetch(true, true, true);
			} else {
				$id_livrocolecao = $orm->insert($to->toArrayInsert());
				$to->setId_livrocolecao($id_livrocolecao);
				$to->fetch(true, true, true);
			}
			
			$this->commit();
			return $to;
		}catch (Zend_Exception $e){
			$this->rollBack();
			THROW $e;
		}
	}
	
	
	/**
	 * Método para vincular um Produto a um Livro em uma Entidade
	 * @param ProdutoLivroTO $to
	 * @throws Zend_Exception
	 * @return ProdutoLivroTO
	 */
	public function inserirProdutoLivro(ProdutoLivroTO $to){
		$this->beginTransaction();
		try{
			
			$orm = new ProdutoLivroORM();
			$orm->insert($to->toArrayInsert());
			
			$this->commit();
			return $to;
		}catch (Zend_Exception $e){
			$this->rollBack();
			THROW $e;
		}
	}
	
	/**
	 * Método para excluir um Produto/Livro/Entidade
	 * @param ProdutoLivroTO $to
	 * @throws Zend_Exception
	 * @return Boolean
	 */
	public function excluirProdutoLivro(ProdutoLivroTO $to){
		$this->beginTransaction();
		try{
			$orm = new ProdutoLivroORM();
			$orm->delete($orm->montarWhere($to));
			$this->commit();
			return true;
		}catch (Zend_Exception $e){
			$this->rollBack();
			THROW $e;
		}
	}
}


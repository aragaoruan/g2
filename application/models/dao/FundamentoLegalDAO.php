<?php
/**
 * Classe de persistencia das tabelas de fundamento legal do banco de dados
 * @author Dimas Sulz <dimassulz@gmail.com>
 * 
 * @package models
 * @subpackage dao
 */
class FundamentoLegalDAO extends SecretariaDAO{
	
	/**
	 * Método que cadastra(insert)o fundamento legal
	 * @param FundamentoLegalTO $flTO
	 * @see FundamentoLegalBO::cadastrarFundamentoLegal();
	 * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
	 */
	public function cadastrarFundamentoLegal(FundamentoLegalTO $flTO){
		try{
			$fundamentoLegalORM = new FundamentoLegalORM();
			$this->beginTransaction();
			$id_fundamentolegal = $fundamentoLegalORM->insert($flTO->toArrayInsert());
			$vwFundamentoLegal = new VwFundamentoLegalORM();
			$dadosFundamentoLegal = $vwFundamentoLegal->fetchRow("id_fundamentolegal = $id_fundamentolegal");
			$this->commit();
			return $dadosFundamentoLegal;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Método que edita (update) a tabela fundamento legal
	 * @param FundamentoLegalTO $flTO
	 * @see FundamentoLegalBO::editarFundamentoLegal()
	 * @return boolean
	 */
	public function editarFundamentoLegal(FundamentoLegalTO $flTO){
		$fundamentoLegalORM = new FundamentoLegalORM();
		$this->beginTransaction();
		try{
			$fundamentoLegalORM->update($flTO->toArrayUpdate(false, array('id_fundamentolegal', 'dt_cadastro')), 'id_fundamentolegal = '.$flTO->getId_fundamentolegal());
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Método que deleta o registro da tabela fundamento legal
	 * @param FundamentoLegalTO $flTO
	 * @see FundamentoLegalBO::deletarFundamentoLegal();
	 * @return boolean
	 */
	public function deletarFundamentoLegal(FundamentoLegalTO $flTO){
		$fundamentoLegalORM = new FundamentoLegalORM();
		$this->beginTransaction();
		try{
			$fundamentoLegalORM->delete('id_fundamentolegal = '.$flTO->getId_fundamentolegal());
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Método que retorna o(s) fundamento(s) legal(is)
	 * @param VwFundamentoLegalTO $flTO
	 * @see FundamentoLegalBO::retornarFundamentoLegal();
	 * @return Zend_Db_Table_Rowset_Abstract|false 
	 */
	public function retornarFundamentoLegal(VwFundamentoLegalTO $flTO){
		$fundamentoLegalORM = new VwFundamentoLegalORM();
		$where = $fundamentoLegalORM->montarWherePesquisa($flTO->tipaTODAO(),false,true);
		try{
			$obj = $fundamentoLegalORM->fetchAll($where);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	
	/**
	 * Método que retorna o(s) tipo(s) de fundamento(s) legal(is)
	 * @param TipoFundamentoLegalTO $tflTO
	 * @see FundamentoLegalBO::retornarTipoFundamentoLegal();
	 * @return Zend_Db_Table_Rowset_Abstract|false 
	 */
	public function retornarTipoFundamentoLegal(TipoFundamentoLegalTO $tflTO){
		$tipoFundamentoLegalORM = new TipoFundamentoLegalORM();
		$where = $tipoFundamentoLegalORM->montarWhere($tflTO->tipaTODAO());
		try{
			$obj = $tipoFundamentoLegalORM->fetchAll($where);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Método que retorna o fundamento legal da matricula
	 * @param VwFundamentoMatriculaTO $to
	 * @param Mixed $where
	 * @return Zend_Db_Table_Rowset_Abstract 
	 */
	public function retornarVwFundamentoMatricula(VwFundamentoMatriculaTO $to, $where = null){
		$orm = new VwFundamentoMatriculaORM();
		return $orm->fetchAll($where ? $where : $orm->montarWhereView($to,false,true));
	}
	
}	
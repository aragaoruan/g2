<?php
/**
 * Classe de persistencia do calendário letivo
 * @author Dimas Sulz <dimassulz@gmail.com>
 * 
 * @package models
 * @subpackage dao
 */
class CalendarioLetivoDAO extends Ead1_DAO{
	
	/**
	 * Método que cadastra (insert na tabela) do calendário letivo
	 * @param CalendarioLetivoTO $clTO
	 * @see CalendarioLetivoBO::cadastrarCalendarioLetivo();
	 * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
	 */
	public function cadastrarCalendarioLetivo(CalendarioLetivoTO $clTO){
		$calendarioLetivoORM = new CalendarioLetivoORM();
		$this->beginTransaction();
		try{
			$id_calendario = $calendarioLetivoORM->insert($clTO->toArrayInsert());
			$this->commit();
			return $id_calendario;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Método que cadastra (insert na tabela) do perído letivo
	 * @param PeriodoLetivoTO $plTO
	 * @see CalendarioLetivoBO::cadastrarPeriodoLetivo();
	 * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
	 */
	public function cadastrarPeriodoLetivo(PeriodoLetivoTO $plTO){
		$periodoLetivoORM = new PeriodoLetivoORM();
		$this->beginTransaction();
		try{
			$id_periodo = $periodoLetivoORM->insert($plTO->toArrayInsert());
			$this->commit();
			return $id_periodo;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Método que cadastra (insert na tabela) do dia letivo
	 * @param DiaLetivoTO $dlTO
	 * @see CalendarioLetivoBO::cadastrarDiaLetivo();
	 * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
	 */
	public function cadastrarDiaLetivo(DiaLetivoTO $dlTO){
		$diaLetivoORM = new DiaLetivoORM();
		$this->beginTransaction();
		try{
			$id_dia = $diaLetivoORM->insert($dlTO->toArrayInsert());
			$this->commit();
			return $id_dia;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Método que edita o calendário letivo
	 * @param CalendarioLetivoTO $clTO
	 * @see CalendarioLetivoBO::editarCalendarioLetivo()
	 * @return boolean
	 */
	public function editarCalendarioLetivo(CalendarioLetivoTO $clTO){
		$calendarioLetivoORM = new CalendarioLetivoORM();
		$this->beginTransaction();
		try{
			$calendarioLetivoORM->update($clTO->toArrayUpdate(false, array('id_calendarioletivo')), 'id_calendarioletivo = '.$clTO->getId_calendarioletivo());
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Método que edita o periodo letivo
	 * @param PeriodoLetivoTO $plTO
	 * @see CalendarioLetivoBO::editarPeriodoLetivo()
	 * @return boolean
	 */
	public function editarPeriodoLetivo(PeriodoLetivoTO $plTO){
		$periodoLetivoORM = new PeriodoLetivoORM();
		$this->beginTransaction();
		try{
			$periodoLetivoORM->update($plTO->toArrayUpdate(false, array('id_periodoletivo')), 'id_periodoletivo = '.$plTO->getId_periodoletivo());
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Método que edita o dia letivo
	 * @param DiaLetivoTO $dlTO
	 * @see CalendarioLetivoBO::editarDiaLetivo()
	 * @return boolean
	 */
	public function editarDiaLetivo(DiaLetivoTO $dlTO){
		$diaLetivoORM = new DiaLetivoORM();
		$this->beginTransaction();
		try{
			$diaLetivoORM->update($dlTO->toArrayUpdate(false, array('id_dialetivo')), 'id_dialetivo = '.$dlTO->getId_dialetivo());
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Método que deleta o calendário letivo
	 * @param CalendarioLetivoTO $clTO
	 * @see CalendarioLetivoBO::deletarCalendarioLetivo()
	 * @return boolean
	 */
	public function deletarCalendarioLetivo(CalendarioLetivoTO $clTO){
		$calendarioLetivoORM = new CalendarioLetivoORM();
		$where = 'id_calendarioletivo = '.$clTO->getId_calendarioletivo();
		$this->beginTransaction();
		try{
			$calendarioLetivoORM->delete($where);
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Método que deleta o período letivo
	 * @param PeriodoLetivoTO $plTO
	 * @see CalendarioLetivoBO::deletarPeriodoLetivo()
	 * @return boolean
	 */
	public function deletarPeriodoLetivo(PeriodoLetivoTO $plTO){
		$periodoLetivoORM = new PeriodoLetivoORM();
		$where = 'id_periodoletivo = '.$plTO->getId_periodoletivo();
		$this->beginTransaction();
		try{
			$periodoLetivoORM->delete($where);
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Método que deleta o dia letivo
	 * @param DiaLetivoTO $dlTO
	 * @see CalendarioLetivoBO::deletarDiaLetivo()
	 * @return boolean
	 */
	public function deletarDiaLetivo(DiaLetivoTO $dlTO){
		$diaLetivoORM = new DiaLetivoORM();
		$where = 'id_dialetivo = '.$dlTO->getId_dialetivo();
		$this->beginTransaction();
		try{
			$diaLetivoORM->delete($where);
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Método que retorna o calendário letivo
	 * @param CalendarioLetivoTO $clTO
	 * @see CalendarioLetivoBO::retornarCalendarioLetivo();
	 * @return Zend_Db_Table_Rowset_Abstract|false 
	 */
	public function retornarCalendarioLetivo(CalendarioLetivoTO $clTO){
		$calendarioLetivoORM = new CalendarioLetivoORM();
		try{
			$obj = $calendarioLetivoORM->fetchAll($calendarioLetivoORM->montarWhere($clTO));
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Método que retorna o período letivo
	 * @param PeriodoLetivoTO $plTO
	 * @see CalendarioLetivoBO::retornarPeriodoLetivo();
	 * @return Zend_Db_Table_Rowset_Abstract|false 
	 */
	public function retornarPeriodoLetivo(PeriodoLetivoTO $plTO){
		$periodoLetivoORM = new PeriodoLetivoORM();
		try{
			$where = '';
			if($plTO->getId_entidade()){
				$where .= "id_entidade = ".$plTO->getId_entidade();
			}
			if($plTO->getId_periodoletivo()){
				if($where){
					$where .= " AND ";
				}
				$where .= "id_periodoletivo = ".$plTO->getId_periodoletivo();
			}
			if($plTO->getId_usuariocadastro()){
				if($where){
					$where .= " AND ";
				}
				$where .= "id_usuariocadastro = ".$plTO->getId_usuariocadastro();
			}
			if($plTO->getDt_abertura()){
				if($where){
					$where .= " AND ";
				}
				$where .= "dt_abertura = '".$plTO->getDt_abertura()."'";
			}
			if($plTO->getDt_encerramento()){
				if($where){
					$where .= " AND ";
				}
				$where .= "dt_encerramento = '".$plTO->getDt_encerramento()."'";
			}
			if($plTO->getDt_fiminscricao()){
				if($where){
					$where .= " AND ";
				}
				$where .= "dt_fiminscricao = '".$plTO->getDt_fiminscricao()."'";
			}
			if($plTO->getDt_inicioinscricao()){
				if($where){
					$where .= " AND ";
				}
				$where .= "dt_inicioinscricao = '".$plTO->getDt_inicioinscricao()."'";
			}
			if($where){
				$where .= " AND ";
			}
			$where .= "dt_encerramento > getdate()";
			
			$obj = $periodoLetivoORM->fetchAll($where);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Método que retorna o dia letivo
	 * @param DiaLetivoTO $dlTO
	 * @see CalendarioLetivoBO::retornarDiaLetivo();
	 * @return Zend_Db_Table_Rowset_Abstract|false 
	 */
	public function retornarDiaLetivo(DiaLetivoTO $dlTO){
		$diaLetivoORM = new DiaLetivoORM();
		try{
			$obj = $diaLetivoORM->fetchAll($diaLetivoORM->montarWhere($dlTO));
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Método que pesquisa se existem vínculos do período letivo com Sala de Aula
	 * @param PeriodoLetivoTO $plTO
	 */
	public function pequisarPeriodoLetivoSalaDeAula(PeriodoLetivoTO $plTO){
		$salaDeAulaORM = new SalaDeAulaORM();
		try{
			return $salaDeAulaORM->fetchAll("id_periodoletivo = {$plTO->getId_periodoletivo()}");
		}catch(Zend_Exception $e){
			return false;
		}
	}
}	
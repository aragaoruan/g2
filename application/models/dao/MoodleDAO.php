<?php

/**
 * Classe de Abstração de dados do MOODLE
 * @author denisexavier
 */
class MoodleDAO extends Ead1_DAO
{


    /**
     * Retorna a vw_sincronizarmoodle
     * @return Ambigous <multitype:, multitype:mixed Ambigous <string, boolean, mixed> >
     */
    public function retornarVwSincronizarMoodle($id_entidadeintegracao = null)
    {
        $vwSincronizarMoodleTO = new VwSincronizarMoodleTO();

        $select = $this->db()
            ->select()
            ->limit(50)
            ->from('vw_sincronizarmoodle')
            ->where('bl_salaativa = 1')
            ->where('id_salasituacao = 8')
            ->order('NEWID()');

        if ($id_entidadeintegracao) {
            $select->where('id_entidadeintegracao = ' . $id_entidadeintegracao);
        }

        if ($vwSincronizarMoodleTO->getSessao()->id_entidade) {
            $select->where('id_entidade = ' . $vwSincronizarMoodleTO->getSessao()->id_entidade);
        }

        return $this->db()->fetchAll($select);

    }

    /**
     * Retorna a vw_sincronizaralunosmoodle
     * @return array
     */
    public function retornarVwSincronizarAlunosMoodle()
    {
        $vwSincronizarAlunosMoodleTO = new VwSincronizarAlunosMoodleTO();
        $where = ($vwSincronizarAlunosMoodleTO->getSessao()->id_entidade
            ? ' id_entidade = ' . $vwSincronizarAlunosMoodleTO->getSessao()->id_entidade : ' 1 = 1');
        $select = $this->db()->select()->limit(60)
            ->from('vw_sincronizaralunosmoodle')->where($where)->order('NEWID()');
        return $this->db()->fetchAll($select);
    }

    /**
     * Retorna a vw_sincronizarcategoriamoodle
     * @return Ambigous <multitype:, multitype:mixed Ambigous <string, boolean, mixed> >
     */
    public function retornarVwSincronizarCategoriaMoodle($id_entidadeintegracao = null)
    {
        $vwSincronizarCategoriaMoodleTO = new VwSincronizarCategoriaMoodleTO();

        $select = $this->db()->select()->limit(50)
            ->from('vw_sincronizarcategoriamoodle')
            ->order('NEWID()');


        if ($id_entidadeintegracao) {
            $select->where('id_entidadeintegracao = ' . $id_entidadeintegracao);
        }

        if ($vwSincronizarCategoriaMoodleTO->getSessao()->id_entidade) {
            $select->where('id_entidade = ' . $vwSincronizarCategoriaMoodleTO->getSessao()->id_entidade);
        }
        return $this->db()->fetchAll($select);
    }

    /**
     * Cadastra na tb_perfilreferenciaintegracao
     * @param PerfilReferenciaIntegracaoTO $to
     * @throws Zend_Exception
     * @return Ambigous <mixed, multitype:>
     * @see BlackBoardBO::processoCadastrarObservador
     * @see BlackBoardBO::processoCadastrarProfessor
     */
    public function salvarPerfilReferencia(PerfilReferenciaIntegracaoTO $to)
    {
        $this->beginTransaction();
        try {

            $orm = new PerfilReferenciaIntegracaoORM();
            if ($to->getId_perfilreferenciaintegracao()) {
                $orm->update($to->toArrayUpdate(false, array('id_perfilreferenciaintegracao')), 'id_perfilreferenciaintegracao = ' . $to->getId_perfilreferenciaintegracao());
                $id = $to->getId_perfilreferenciaintegracao();
            } else {
                $id = $orm->insert($to->toArrayInsert());
                $to->setId_perfilreferenciaintegracao($id);
            }

            $this->commit();
            return $id;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            throw $e;
        }
    }


    /**
     * @param VwAlunosEncerradosMoodleTO|null $to
     * @return array
     */
    public function retornaVwAlunosEncerradosMoodle(VwAlunosEncerradosMoodleTO $to = null)
    {
        $where = [];
        if ($to) {
            foreach ($to->toArray() as $key => $value) {
                if ($value) {
                    $where[] = $key . " = '" . $value . "'";
                }
            }
        }
        $select = $this->db()->select()->from('vw_alunosencerradosmoodle')->limit(600)->order('NEWID()');
        if ($where) {
            $select->where(implode(" AND ", $where));
        }
        return $this->db()->fetchAll($select);
    }


    /**
     * @param AlocacaoIntegracaoTO $to
     * @return int
     * @throws Zend_Exception
     */
    public function updateAlocacaoIntegracao(AlocacaoIntegracaoTO $to)
    {
        $this->beginTransaction();
        try {
            $orm = new AlocacaoIntegracaoORM();
            $result = $orm->update($to->toArrayUpdate(false, array('id_alocacaointegracao')), $orm->montarWhere($to, true));
            $this->commit();
            return $result;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            throw $e;
        }
    }

    /**
     * @param UsuarioIntegracaoTO $to
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function retornaUsuarioIntegracao(UsuarioIntegracaoTO $to)
    {
        $ORM = new UsuarioIntegracaoORM();
        $select = $ORM->select()->distinct(true)
            ->from(array('tb_usuariointegracao'), array('id_usuariointegracao', 'id_sistema', 'id_usuariocadastro', 'id_usuario', 'dt_cadastro', 'st_codusuario'
            , 'st_senhaintegrada', 'st_loginintegrado', 'id_entidade'))
            ->where('id_entidade = ' . $to->getId_entidade())
            ->where('id_sistema = ' . $to->getId_sistema())
            ->where('id_usuario = ' . $to->getId_usuario());

        return $ORM->fetchAll($select);
    }
}

?>

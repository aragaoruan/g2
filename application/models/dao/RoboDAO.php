<?php

/**
 * Classe de persistência de Dados Utilizados Pelos Robos
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage dao
 */
class RoboDAO extends Ead1_DAO {

    /**
     * Metodo que retorna os dados de mnsalidade para o ro
     * @param VwRoboMensalidadeTO $to
     * @param Mixed $where
     * @return Ead1_Mensageiro
     */
    public function retornarVwRoboMensalidade(VwRoboMensalidadeTO $to, $where = null) {
        try {
            $orm = new VwRoboMensalidadeORM();
            if (!$where) {
                $where = $orm->montarWhereView($to, false, true);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

}

<?php

/**
 * Classe de persistencia de textos de sistema com o banco de dados.
 * @author João Gabriel <jgvasconcelos16@gmail.com>
 *
 * @package models
 * @subpackage dao
 */
class TextosDAO extends Ead1_DAO {

    /**
     * Método que retorna textos do sistema.
     * @param TextoSistemaTO $tsTO
     * @see TextosBO::retornarTextoSistema();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarTextoSistema(TextoSistemaTO $tsTO) {
        try {
            $textoSistemaORM = new TextoSistemaORM();
            $where = $textoSistemaORM->montarWhere($tsTO);
            return $textoSistemaORM->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna a view de textos do sistema.
     * @param VwTextoSistemaTO $vwtsTO
     * @see TextosBO::retornarVwTextoSistema();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwTextoSistema(VwTextoSistemaTO $vwtsTO) {
        $vwTextoSistemaORM = new VwTextoSistemaORM();
        $where = $vwTextoSistemaORM->montarWhereView($vwtsTO, false, true, false);
        try {
            $obj = $vwTextoSistemaORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna a view de geração de declaração.
     * @param VwGerarDeclaracaoTO $vwgdTO
     * @param Mixed $where
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwGerarDeclaracao(VwGerarDeclaracaoTO $vwgdTO, $where = null) {
        try {
            $vwGerarDeclaracaoORM = new VwGerarDeclaracaoORM();
            if (!$where) {
                $where = $vwGerarDeclaracaoORM->montarWhereView($vwgdTO, false, true, false);
            }
            return $vwGerarDeclaracaoORM->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna a view de geração de ocorrência
     * @param VwGerarOcorrenciaTO $vwGerarOcorrenciaTO
     * @param Mixed $where
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwGerarOcorrencia(VwGerarOcorrenciaTO $vwGerarOcorrenciaTO, $where = null) {
        try {
            $vwGerarOcorrenciaORM = new VwGerarOcorrenciaORM();
            if (!$where) {
                $where = $vwGerarOcorrenciaORM->montarWhereView($vwGerarOcorrenciaTO, false, true, false);
            }
            return $vwGerarOcorrenciaORM->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna a view de geração de texto de classe afiliados
     * @param ClasseAfiliadoTO $classeAfiliadoTO
     * @param unknown_type $where
     * @throws Zend_Exception
     * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
     */
    public function retornarClasseAfiliado(ClasseAfiliadoTO $classeAfiliadoTO, $where = null) {
        try {
            $classeAfiliadoORM = new ClasseAfiliadoORM();
            if (!$where) {
                $where = $classeAfiliadoORM->montarWhere($classeAfiliadoTO, true);
            }
            return $classeAfiliadoORM->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna a categoria do texto
     * @param TextoCategoriaTO $tcTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarTextoCategoria(TextoCategoriaTO $tcTO) {
        try {
            $orm = new TextoCategoriaORM();
            $where = $orm->montarWhere($tcTO);
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna as variaveis do texto
     * @param TextoVariaveisTO $tvTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarTextoVariaveis(TextoVariaveisTO $tvTO) {
        try {
            $orm = new TextoVariaveisORM();
            $where = $orm->montarWhere($tvTO);
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna a view dos textos do sistema
     * @param VwGerarTextoTO $vwGtTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwGerarTexto(VwGerarTextoTO $vwGtTO) {
        try {
            $orm = new VwGerarTextoORM();
            $where = $orm->montarWhereView($vwGtTO, false, true);
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna a view para geração do texto da venda
     * @param VwGerarVendaTextoTO $vwGvtTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwGerarVendaTexto(VwGerarVendaTextoTO $vwGvtTO, $where = null) {
        try {
            $orm = new VwGerarVendaTextoORM();
            if (!$where) {
                $where = $orm->montarWhereView($vwGvtTO, false, true);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna a view para geração do texto da venda
     * @param VwGerarVendaTextoLancamentoTO $vwGvtTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwGerarVendaTextoLancamento(VwGerarVendaTextoLancamentoTO $vwGvtTO) {
        try {
            $orm = new VwGerarVendaTextoLancamentoORM();
            $where = $orm->montarWhereView($vwGvtTO, false, true);
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna a view do cabeçalho da instituição
     * @param VwGerarCabecalhoTO $vwGcTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwGerarCabecalho(VwGerarCabecalhoTO $vwGcTO) {
        try {
            $orm = new VwGerarCabecalhoORM();
            $where = $orm->montarWhereView($vwGcTO, false, true);
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna a view do cabeçalho da instituição
     * @param VwHistoricoTO $vwHTO
     * @see TextosBO::retornarVwHistorico
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwHistorico(VwHistoricoTO $vwHTO) {
        try {
            $orm = new VwHistoricoORM();
            $where = $orm->montarWhereView($vwHTO, false, true);
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna a view do estabelecimento
     * @param VwGridEstabelecimentoTO $to
     * @see TextosBO::retornarVwHistorico
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwGridEstabelecimento(VwGridEstabelecimentoTO $to) {
        try {
            $orm = new VwGridEstabelecimentoORM();
            $where = $orm->montarWhereView($to, false, true);
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna a view de dados para grids de declarações e certificados
     * @param VwGridTO $vwgTO
     * @see TextosBO::retornarDadosGrid
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwGrid(VwGridTO $vwgTO) {
        try {
            $orm = new VwGridORM();
            $where = $orm->montarWhereView($vwgTO, false, true);
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna a view da geração de texto do contrato
     * @param VwGerarContratoTO $vwGcTO
     * @see TextosBO::retornarVwGerarContrato
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwGerarContrato(VwGerarContratoTO $vwGcTO) {
        try {
            $orm = new VwGerarContratoORM();
            $where = $orm->montarWhereView($vwGcTO, false, true);
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna a view do historico geral
     * @param VwHistoricoGeralTO $vwHgTO
     * @see TextosBO::retornarVwHistoricoGeral
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwHistoricoGeral(VwHistoricoGeralTO $vwHgTO) {
        try {
            $orm = new VwHistoricoGeralORM();
            $where = $orm->montarWhereView($vwHgTO, false, true);
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            Zend_Debug::dump($this->excecao);
            exit;
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna a view da grid de histórico
     * @param VwGridHistoricoTO $vwHgTO
     * @see TextosBO::retornarVwHistoricoGeral
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwGridHistorico(VwGridHistoricoTO $vwHgTO) {
        try {
            $orm = new VwGridHistoricoORM();
            $where = $orm->montarWhereView($vwHgTO, false, true);
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            Zend_Debug::dump($this->excecao);
            exit;
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Retorna os dados da view vw_dadospessoais
     * @param VwDadosPessoaisTO $dadosPessoaisTO
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function retornarVwDadosPessoais(VwDadosPessoaisTO $dadosPessoaisTO) {
        $vwDadosPessoaisORM = new VwDadosPessoaisORM();
        $where = $vwDadosPessoaisORM->montarWhereView($dadosPessoaisTO, false, true);
        return $vwDadosPessoaisORM->fetchAll($where);
    }

    /**
     * Metodo que exclui o texto sistema
     * @param TextoSistemaTO $tsTO
     * @param Mixed $where
     * @return Boolean
     */
    public function deletarTextoSistema(TextoSistemaTO $tsTO, $where = null) {
        try {
            $this->beginTransaction();
            $orm = new TextoSistemaORM();
            if (!$where) {
                $where = $orm->montarWhere($tsTO, true);
            }
            $orm->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

}


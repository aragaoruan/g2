<?php
/**
 * Classe de persistência de Telemarketing ao banco de dados
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage dao
 */
class TelemarketingDAO extends Ead1_DAO{
	
	/**
	 * Metodo que cadastra um atendente para a venda
	 * @param AtendenteVendaTO $to
	 * @return $id_atendentevenda | false
	 */
	public function cadastrarAtendenteVenda(AtendenteVendaTO $to){
		try{
			$this->beginTransaction();
			$orm = new AtendenteVendaORM();
			$insert = $to->toArrayInsert();
			$id_atendentevenda = $orm->insert($insert);
			$this->commit();
			return $id_atendentevenda;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW $e;
			return false;
		}
	}
	
	/**
	 * Metodo que edita um vinculo de antendete com venda
	 * @param AtendenteVendaTO $to
	 * @param Mixed $where
	 * @return Boolean
	 */
	public function editarAtendenteVenda(AtendenteVendaTO $to, $where = null){
		try{
			$this->beginTransaction();
			$orm = new AtendenteVendaORM();
			if(!$where){
				$where = $orm->montarWhere($to,true);
			}
			$update = $to->toArrayUpdate(false,array('id_antendetevenda','id_venda','id_usuario'));
			$orm->update($update, $where);
			$this->commit();
			return true;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW $e;
			return false;
		}
	}
	
	/**
	 * Metodo que retorna os atendentes da venda
	 * @param AtendenteVendaTO $to
	 * @param Mixed $where
	 * @return Zend_Db_Table_Abstract | false
	 */
	public function retornarAtendenteVenda(AtendenteVendaTO $to, $where = null){
		try{
			$orm = new AtendenteVendaORM();
			if(!$where){
				$where = $orm->montarWhere($to,false);
			}
			return $orm->fetchAll($where);
		}catch (Zend_Exception $e){
			$this->excecao = $e->getMessage();
			THROW $e;
			return false;
		}
	}
	
}
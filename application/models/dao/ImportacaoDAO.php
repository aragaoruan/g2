<?php
/**
 * Classe de persistência de Importação ao banco de dados
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage dao
 */
class ImportacaoDAO extends Ead1_DAO{
	
	/**
	 * Metodo que cadastra uma pessoa na tabela de importacao
	 * @param PessoaImportacaoTO $piTO
	 * @return $id_pessoaimportacao
	 */
	public function cadastrarPessoaImportacao(PessoaImportacaoTO $piTO){
		$pessoaImportacaoORM 	= new PessoaImportacaoORM();
		$dados 					= $piTO->toArrayInsert();
		return $pessoaImportacaoORM->insert($dados, false);
	}
	
	/**
	 * Cadastra um aproveitamento na tabela de importacao
	 * @param AproveitamentoImportacaoTO $aiTO
	 * @return int $idAproveitamentoimportacao
	 */
	
	public function cadastrarAproveitamentoImportacao(AproveitamentoImportacaoTO $aproveitamentoImportacaoTO){
			$aproveitamentoImportacaoORM = new AproveitamentoImportacaoORM(); 
			return $aproveitamentoImportacaoORM->insert( $aproveitamentoImportacaoTO->toArrayInsert() );
	}
	
	/**
	 * Metodo que cadastra uma avaliacao na tabela de importacao
	 * @param AvaliacaoImportacaoTO $aiTO
	 * @return $id_avaliacaoimportacao
	 */
	public function cadastrarAvaliacaoImportacao( AvaliacaoImportacaoTO  $avaliacaoImportacaoTO ){
			$avaliacaoImportacaoORM 	= new AvaliacaoImportacaoORM(); 
			$dados						= $avaliacaoImportacaoTO->toArrayInsert();
			return $avaliacaoImportacaoORM->insert( $dados );
	}
	
	/**
	 * Metodo que cadastra um documento na tabela de importacao
	 * @param DocumentoEntregueImportacaoTO $dieTO
	 * @return $id_documentoentregueimportacao
	 */
	public function cadastrarDocumentoEntregueImportacao(DocumentoEntregueImportacaoTO $documentoEntregueImportacaoTO){
			$documentoEntregueImportacaoORM = new DocumentoEntregueImportacaoORM(); 
			return $documentoEntregueImportacaoORM->insert($documentoEntregueImportacaoTO->toArrayInsert());
	}
	
	/**
	 * Metodo que cadastra um lancamento na tabela de importacao
	 * @param LancamentoImportacaoTO $liTO
	 * @return $id_lancamentoimportacao
	 */
	public function cadastrarLancamentoImportacao(LancamentoImportacaoTO $lancamentoImportacaoTO){
			$lancamentoImportacaoORM = new LancamentoImportacaoORM(); 
			return $lancamentoImportacaoORM->insert($lancamentoImportacaoTO->toArrayInsert());
	}
	
	/**
	 * Metodo que cadastra um matricula na tabela de importacao
	 * @param MatriculaImportacaoTO $miTO
	 * @return $id_matriculaimportacao
	 */
	public function cadastrarMatriculaImportacao(MatriculaImportacaoTO $miTO){
		$matriculaImportacaoORM = new MatriculaImportacaoORM(); 
		$dados 					= $miTO->toArrayInsert();
		return $matriculaImportacaoORM->insert( $dados );
	}
	
	/**
	 * Metodo que cadastra um responsavel na tabela de importacao
	 * @param ResponsavelImportacaoTO $riTO
	 * @return $id_responsavelimportacao
	 */
	public function cadastrarResponsavelImportacao( ResponsavelImportacaoTO $riTO ){
		$orm 	= new ResponsavelImportacaoORM(); 
		$dados 	= $riTO->toArrayInsert();
		foreach( $dados as $coluna => $valor ){
			$dados[$coluna] = utf8_decode( $valor );
		}
		return $orm->insert($dados);
	}
	
	/**
	 * Cadastra o histórico de situação da matrícula de sistemas externos 
	 * @param HistoricoSituacaoImportacaoTO $historicoSituacaoImportacaoTO
	 * @return int 
	 */
	public function cadastrarHistoricoSituacaoImportacao( HistoricoSituacaoImportacaoTO $historicoSituacaoImportacaoTO ) {
		$historicoSituacaoImportacaoORM	= new HistoricoSituacaoImportacaoORM();
		return $historicoSituacaoImportacaoORM->insert( $historicoSituacaoImportacaoTO->toArrayInsert() );
	}
	
	/**
	 * Cadastra os tipos de lançamentos de sistemas externos
	 * @param TipoLancamentoImportacaoTO $tipoLancamentoImportacaoTO
	 * @return int chave primária
	 */
	public function cadastrarTipoLancamento( TipoLancamentoImportacaoTO $tipoLancamentoImportacaoTO ) {
		$tipoLancamentoImportacaoORM	= new TipoLancamentoImportacaoORM();
		return $tipoLancamentoImportacaoORM->insert( $tipoLancamentoImportacaoTO->toArrayInsert() );
	}
	
	/**
	 * Cadastra um novo projeto com sua disciplina vinculada 
	 * @param ProjetoDisciplinaImportacaoTO $projetoDisciplinaImportacaoTO
	 * @return mixed
	 */
	public function cadastrarProjetoDisciplina( ProjetoDisciplinaImportacaoTO $projetoDisciplinaImportacaoTO ) {
		$projetoDisciplinaImportacaoORM	= new ProjetoDisciplinaImportacaoORM();
		return $projetoDisciplinaImportacaoORM->insert( $projetoDisciplinaImportacaoTO->toArrayInsert() );
	}

	/**
	 * Metodo que edita uma pessoa na tabela de importacao
	 * @param PessoaImportacaoTO $piTO
	 * @param Mixed $where
	 * @throws Zend_Db_Exception
	 * @return Boolean
	 */
	public function editarPessoaImportacao(PessoaImportacaoTO $piTO, $where = null){
		try{
			$this->beginTransaction();
			$orm = new PessoaImportacaoORM();
			if(!$where){
				$where = $orm->montarWhere($piTO,true);
			}
			$dados = $piTO->toArrayUpdate(false,array('id_pessoaimportacao','id_entidadeimportado','nu_codpessoaorigem'));
			$orm->update($dados,$where);
			$this->commit();
			return true;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Db_Exception($e->getMessage());
			return false;
		}
	}
	
	/**
	 * Metodo que retorna os dados de importação de pessoa
	 * @param PessoaImportacaoTO $to
	 * @param Mixed $where
	 * @throws Zend_Db_Exception
	 * @return Zend_Db_Table_Row_Abstract | false;
	 */
	public function retornarPessoaImportacao(PessoaImportacaoTO $to, $where){
		try{
			$orm = new PessoaImportacaoORM();
			if(!$where){
				$where = $orm->montarWhere($to);
			}
			return $orm->fetchAll($where);
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Db_Exception($e->getMessage());
			return false;
		}
	}
	
	/**
	 * Metodo que retorna os estados civis de importação
	 * @param EstadocivilImportacaoTO $to
	 * @param Mixed $where
	 * @throws Zend_Db_Exception
	 * @return Zend_Db_Table_Row_Abstract | false;
	 */
	public function retornarEstadoCivilImportacao(EstadocivilImportacaoTO $to, $where = null){
		try{
			$orm = new EstadocivilImportacaoORM();
			if(!$where){
				$where = $orm->montarWhere($to);
			}
			return $orm->fetchAll($where);
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Db_Exception($e->getMessage());
			return false;
		}
	}
	
	/**
	 * Metodo que retorna as etnias de importação
	 * @param EtniaImportacaoTO $to
	 * @param Mixed $where
	 * @throws Zend_Db_Exception
	 * @return Zend_Db_Table_Row_Abstract | false;
	 */
	public function retornarEtniaImportacao(EtniaImportacaoTO $to,$where = null){
		try{
			$orm = new EtniaImportacaoORM();
			if(!$where){
				$where = $orm->montarWhere($to);
			}
			return $orm->fetchAll($where);
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Db_Exception($e->getMessage());
			return false;
		}
	}
	
	/**
	 * Metodo que retorna os sistemas da importacao
	 * @param SistemaImportacaoTO $to
	 * @param Mixed $where
	 * @throws Zend_Db_Exception
	 * @return Zend_Db_Table_Row_Abstract | false;
	 */
	public function retornarSistemaImportacao(SistemaImportacaoTO $to,$where = null){
		try{
			$orm = new SistemaImportacaoORM();
			if(!$where){
				$where = $orm->montarWhere($to);
			}
			return $orm->fetchAll($where);
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Db_Exception($e->getMessage());
			return false;
		}
	}

	/**
	 * Executa a procedure sp_importacaoexterna
	 * Gera o usuário e a matrícula caso
	 * @return string
	 */
	public function processarDadosUsuarioMatriculaImportacao( $blMatricula = 0 ) {
		$adapter	= Zend_Db_Table_Abstract::getDefaultAdapter();
		$statement	= $adapter->prepare( 'EXECUTE sp_importarcaoexterna ?' );
		$statement->execute( array( $blMatricula ) );
		return	 $statement->fetchAll();
	}
	
	public function execSPImportarAproveitamentoExterno() {
		$adapter	= Zend_Db_Table_Abstract::getDefaultAdapter();
		$statement	= $adapter->prepare( 'EXECUTE sp_importaraproveitamentoexterno' );
		$statement->execute();
		return true;
	}
	
	public function execSPImportarAvaliacaoExterna() {
		$adapter	= Zend_Db_Table_Abstract::getDefaultAdapter();
		$statement	= $adapter->prepare( 'EXECUTE sp_importaravaliacaoexterna' );
		$statement->execute();
		return true;
	}
	
}
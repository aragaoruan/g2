<?php

/**
 * Classe de persistência de Disciplina ao banco de dados
 * @author Eduardo Romão <ejushiro@gmail.com>
 *
 * @package models
 * @subpackage dao
 */
class DisciplinaDAO extends Ead1_DAO
{

    /**
     * Metodo que cadastra Disciplina
     * @param DisciplinaTO $dTO
     * @return var $id_disciplina | boolean false
     */
    public function cadastrarDisciplina(DisciplinaTO $dTO)
    {
        $tbDisciplinaORM = new DisciplinaORM();
        unset($dTO->arrDados);
        $insert = $dTO->toArrayInsert();
        $this->beginTransaction();
        try {
            $id_disciplina = $tbDisciplinaORM->insert($insert);
            $this->commit();
            return $id_disciplina;
        } catch (Exception $e) {
            $this->rollBack();
            $arr['TO'] = $dTO;
            $arr['WHERE'] = $insert;
            $arr['SQL'] = $e->getMessage();
            $this->excecao = $arr;
            return false;
        }
    }

    /**
     * Metodo que cadastra a Area da Disciplina
     * @param DisciplinaSerieNivelEnsinoTO $dsneTO
     * @return boolean
     */
    public function cadastrarAreaDisciplina(AreaDisciplinaTO $adTO)
    {
        $tbAreaDisciplinaORM = new AreaDisciplinaORM();
        $this->beginTransaction();
        try {
            $tbAreaDisciplinaORM->insert($adTO->toArrayDAO());
            $this->commit();
            return true;
        } catch (Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que Cadastra Serie, Nivel de Ensino da Area de Conhecimento na Disciplina
     * @param DisciplinaSerieNivelEnsinoTO $dsneTO
     * @return boolean
     */
    public function cadastrarDisciplinaSerieNivel(DisciplinaSerieNivelEnsinoTO $dsneTO)
    {
        $tbDisciplinaSerieNivelEnsinoORM = new DisciplinaSerieNivelEnsinoORM();
        $this->beginTransaction();
        try {
            $tbDisciplinaSerieNivelEnsinoORM->insert($dsneTO->toArrayDAO());
            $this->commit();
            return true;
        } catch (Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Meotodo que Edita Disciplina
     * @param DisciplinaTO $dTO
     * @return Boolean
     */
    public function editarDisciplina(DisciplinaTO $dTO)
    {
        $tbDisciplinaORM = new DisciplinaORM();
        unset($dTO->arrDados);
        $update = $dTO->toArrayUpdate(false, array('id_disciplina'));
        $this->beginTransaction();
        try {
            $tbDisciplinaORM->update($update, " id_disciplina = " . $dTO->getId_disciplina());
            $this->commit();
            return true;
        } catch (Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que Exclui a Area da Disciplina
     * @param AreaDisciplinaTO $adTO
     * @return boolean
     */
    public function deletarAreaDisciplina(AreaDisciplinaTO $adTO)
    {
        $tbAreaDisciplinaORM = new AreaDisciplinaORM();
        $where = $tbAreaDisciplinaORM->montarWhere($adTO);
        $this->beginTransaction();
        try {
            if (!$adTO->getId_disciplina()) {
                throw new Zend_Exception('É Necessario setar o ID da Disciplina!');
            }
            $tbAreaDisciplinaORM->delete($where);
            $this->commit();
            return true;
        } catch (Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }


    /**
     * Metodo que Exclui Serie, Nivel de Ensino da Area de Conhecimento da Disciplina
     * @param DisciplinaSerieNivelEnsinoTO $dsneTO
     * @return boolean
     */
    public function deletarDisciplinaSerieNivel(DisciplinaSerieNivelEnsinoTO $dsneTO)
    {
        $tbDisciplinaSerieNivelEnsinoORM = new DisciplinaSerieNivelEnsinoORM();
        $where = $tbDisciplinaSerieNivelEnsinoORM->montarWhere($dsneTO);
        $this->beginTransaction();
        try {
            if (!$where) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $result = $tbDisciplinaSerieNivelEnsinoORM->delete($where);
            if ($result) {
                $this->commit();
                return true;
            } else {
                throw new Exception('Houve um erro ao tentar excluir registro.');
            }

        } catch (Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que retorna Disciplina
     * @param DisciplinaTO $dTO
     * @param String $where
     * @see MaterialBO::retornarDisciplinasItemDeMaterial()
     * @return object $obj|boolean
     */
    public function retornaDisciplina(DisciplinaTO $dTO, $where = null)
    {
        $tbDisciplinaORM = new DisciplinaORM();
        if (!$where) {
            $where = $tbDisciplinaORM->montarWhere($dTO);
        }
        try {
            $obj = $tbDisciplinaORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $msg['WHERE'] = $where;
            $msg['SQL'] = $e->getMessage();
            $msg['TO'] = $dTO;
            $this->excecao = $msg;
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que retorna Disciplina da Area de Conhecimento
     * @param AreaDisciplinaTO $adTO
     * @return object $obj|boolean
     */
    public function retornaAreaDisciplina(AreaDisciplinaTO $adTO)
    {
        $areaDisciplinaORM = new AreaDisciplinaORM();
        $where = $areaDisciplinaORM->montarWhere($adTO);
        try {
            $obj = $areaDisciplinaORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que Retorna Serie no Nivel de Ensino na Area de Conhecimento da Disciplina
     * @param DisciplinaSerieNivelEnsinoTO $dsneTO
     * @return object $obj|boolean
     */
    public function retornaDisciplinaSerieNivel(DisciplinaSerieNivelEnsinoTO $dsneTO)
    {
        $disciplinaSerieNivelORM = new DisciplinaSerieNivelEnsinoORM();
        $where = $disciplinaSerieNivelORM->montarWhere($dsneTO);
        $this->beginTransaction();
        try {
            $obj = $disciplinaSerieNivelORM->fetchAll($where);
            $this->commit();
            return $obj;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que Retorna tipo de disciplina
     * @param TipoDisciplinaTO $tdTO
     * @return object $obj|boolean
     */
    public function retornaTipoDisciplina(TipoDisciplinaTO $tdTO)
    {
        $tipoDisciplinaORM = new TipoDisciplinaORM();
        $where = $tipoDisciplinaORM->montarWhere($tdTO);
        try {
            $obj = $tipoDisciplinaORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que Retorna dados de Disciplina por Area de Conhecimento e serie e nivel de ensino
     * @param VwDisciplinaAreaConhecimentoNivelSerieTO $vwdacsnTO
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function retornaDisciplinaAreaConhecimentoSerieNivel(VwDisciplinaAreaConhecimentoNivelSerieTO $vwdacsnTO)
    {
        try {
            $vwDisciplinaAreaConhecimentoSerieNivelORM = new VwDisciplinaAreaConhecimentoSerieNivelORM();
            $where = $vwDisciplinaAreaConhecimentoSerieNivelORM->montarWherePesquisa($vwdacsnTO, false, true);
            $obj = $vwDisciplinaAreaConhecimentoSerieNivelORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que Retorna dados de Série
     * @param SerieTO $sTO
     * @return object $obj|boolean
     */
    public function retornaSerie(SerieTO $sTO)
    {
        $sORM = new SerieORM();
        $where = $sORM->montarWhereView($sTO, false, true);
        try {
            $obj = $sORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            return false;
        }
    }

    /**
     * Metodo que Retorna dados da vw_professordisciplina
     * @param VwProfessorDisciplinaTO $vwProfessorDisciplinaTO
     * @return object $obj|boolean
     */
    public function retornarVwProfessorDisciplina(VwProfessorDisciplinaTO $vwProfessorDisciplinaTO, $order = null)
    {
        $vwProfessorDisciplinaORM = new VwProfessorDisciplinaORM();
        $where = $vwProfessorDisciplinaORM->montarWhereView($vwProfessorDisciplinaTO, false, true);
        try {
            $obj = $vwProfessorDisciplinaORM->fetchAll($where, $order);
            return $obj;
        } catch (Zend_Exception $e) {
            return false;
        }
    }

    /**
     * @param VwProfessorDisciplinaTO $vwProfessorDisciplinaTO
     * @param null $order
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function retornarProfessor(VwProfessorDisciplinaTO $vwProfessorDisciplinaTO, $order = null)
    {
        $vwProfessorDisciplinaORM = new VwProfessorDisciplinaORM();
        $where = $vwProfessorDisciplinaORM->montarWhereView($vwProfessorDisciplinaTO, false, true);
        $select = $vwProfessorDisciplinaORM->select()->distinct()
            ->from(array('ec' => 'vw_professordisciplina'), array('ec.st_nomecompleto', 'ec.id_usuario', 'ec.id_entidade'))
            ->where($where);

        if (!empty($order)) {
            $select->order($order);
        }

        return $vwProfessorDisciplinaORM->fetchAll($select);
    }

    /**
     * Metodo que Retorna dados da Vw_DisciplinaSerieNivel
     * @param VwDisciplinaSerieNivelTO $to
     * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
     */
    public function retornarVwDisciplinaSerieNivel(VwDisciplinaSerieNivelTO $to)
    {
        $orm = new VwDisciplinaSerieNivelORM();
        $where = $orm->montarWhereView($to, false, true);
        try {
            $obj = $orm->fetchAll($where, 'st_disciplina');
            return $obj;
        } catch (Zend_Exception $e) {
            return false;
        }
    }


    /**
     * Método que salva o DisciplinaIntegracao
     * @param DisciplinaIntegracaoTO $dIntegracaoTO
     * @return Ambigous <mixed, multitype:>|boolean
     */
    public function cadastrarDisciplinaIntegracao(DisciplinaIntegracaoTO $dIntegracaoTO)
    {
        $orm = new DisciplinaIntegracaoORM();
        $insert = $dIntegracaoTO->toArrayInsert();
        $this->beginTransaction();
        try {
            $id_disciplinaIntegracao = $orm->insert($insert);
            $this->commit();
            return $id_disciplinaIntegracao;
        } catch (Exception $e) {
            $this->rollBack();
            $arr['TO'] = $dIntegracaoTO;
            $arr['WHERE'] = $insert;
            $arr['SQL'] = $e->getMessage();
            $this->excecao = $arr;
            return false;
        }
    }


    /**
     * Método que edita o DisciplinaIntegracao
     * @param DisciplinaIntegracaoTO $dIntegracaoTO
     * @return boolean
     */
    public function editarDisciplinaIntegracao(DisciplinaIntegracaoTO $dIntegracaoTO)
    {
        $orm = new DisciplinaIntegracaoORM();
        $update = $dIntegracaoTO->toArrayUpdate(false, array('id_disciplinaintegracao'));
        $this->beginTransaction();
        try {
            $orm->update($update, " id_disciplinaintegracao = " . $dIntegracaoTO->getId_disciplinaintegracao());
            $this->commit();
            return true;
        } catch (Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }


    /**
     * Método que retorna Integracao das disciplinas ao moodle a entidade pelo id_disciplina
     * @param VwDisciplinaIntegracaoMoodleTO $vw
     * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
     */
    public function retornarVwDisciplinaIntegracaoMoodle(VwDisciplinaIntegracaoMoodleTO $vw)
    {
        $ORM = new VwDisciplinaIntegracaoMoodleORM();
        $where = $ORM->montarWhereView($vw, false, true);
        try {
            $obj = $ORM->fetchAll($where, 'st_nomeentidade');
            return $obj;
        } catch (Zend_Exception $e) {
            return false;
        }
    }
}
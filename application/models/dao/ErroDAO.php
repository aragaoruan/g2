<?php
/**
 * Classe de persistencia da tb_erro com o banco de dados
 * @author Denise Xavier <denisexavier@ead1.com.br>
 * @package models
 * @subpackage dao
 */
class ErroDAO extends Ead1_DAO {


    /**
     * Método que cadastra(insert) na tb_erro
     * @param ErroTO $erro
     * @see ErroBO::salvar
     * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
     */
    public function cadastrarErro (ErroTO $erro)
    {
        $erroORM = new ErroORM();
        $this->beginTransaction();
        try {
            $insert = $erro->toArrayInsert();
            $id = $erroORM->insert($insert);
            $this->commit();
            return $id;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }
}
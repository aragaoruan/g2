<?php

/**
 * Classe de acesso a dados referente a RemessaDAO
 * @author Elcio Mauo Guimarães - <elcioguimaraes@gmail.com>
 * @since 
 * @package models
 * @subpackage dao
 */
class RemessaDAO extends Ead1_DAO {

	
	/**
	 * Metodo que cadastra o material da remessa
	 * @param MaterialRemessaTO $to
	 * @return boolean
	 */
	public function cadastrarMaterialRemessa(MaterialRemessaTO $to){
		try{
			$this->beginTransaction();
			$orm = new MaterialRemessaORM();
			$insert = $to->toArrayInsert();
			$id_materialremessa = $orm->insert($insert);
			$this->commit();
			return $id_materialremessa;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW $e;
			return false;
		}
	}
	
	
	/**
	 * Metodo que deleta o material da remessa
	 * @param MaterialRemessaTO $to
	 * @return boolean
	 */
	public function deletarMaterialRemessa(MaterialRemessaTO $to, $where = null){
		try{
			$this->beginTransaction();
			$orm = new MaterialRemessaORM();

			if(!$where){
				$where = $orm->montarWhere($to);
			}
			if(!$where){
				THROW new Zend_Exception('O TO não Pode vir Vazio!');
			}
			$orm->delete($where);

			$this->commit();
			return true;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW $e;
			return false;
		}
	}
	
	/**
	 * Método que insere o Remessa
	 * 
	 * @param RemessaTO $remessaTO
	 * @throws Zend_Exception
	 * @return Ambigous <mixed, multitype:>
	 * @see RemessaBO::cadastrarRemessa();
	 */
	public function cadastrarRemessa(RemessaTO $remessaTO){
		$this->beginTransaction();
		try{
			$remessaORM = new RemessaORM();
			$remessaTO->setId_situacao(RemessaTO::ATIVO);
			$id_remessa = $remessaORM->insert($remessaTO->toArrayInsert());
			$this->commit();
			return $id_remessa;
		} catch (Zend_Exception $e){
			$this->rollBack();
			throw $e;
		}	
	}
	
	/**
	 * Método de edição do Remessa
	 * @param RemessaTO $remessaTO
	 * @throws Zend_Exception
	 * @see RemessaBO::editarRemessa();
	 */
	public function editarRemessa(RemessaTO $remessaTO){
		$this->beginTransaction();
		try {
			$remessaORM = new RemessaORM();
			$remessaORM->update($remessaTO->toArrayUpdate(true, array('id_remessa')), $remessaORM->montarWhere($remessaTO, true));
			$this->commit();
		} catch (Zend_Exception $e) {
			$this->rollBack();
			throw $e;
		}
	}
	
	/**
	 * Inativa uma Remessa
	 * @param RemessaTO $to
	 * @throws Zend_Exception
	 */
	public function inativarRemessa(RemessaTO $to){
		$this->beginTransaction();
		try {
			$remessaORM = new RemessaORM();
			
			$remessaTO = new RemessaTO();
			$remessaTO->setId_remessa($to->getId_remessa());
			
			$hoje = new Zend_Date();
			$remessaTO->setDt_fim($hoje->toString());
			$remessaTO->setId_situacao(RemessaTO::INATIVO);
			
			$remessaORM->update($remessaTO->toArrayUpdate(false, array('id_remessa')), $remessaORM->montarWhere($remessaTO, true));
			$this->commit();
		} catch (Zend_Exception $e) {
			$this->rollBack();
			throw $e;
		}
	}
	
	
	/**
	 * Método de listagem de Remessa
	 * @param RemessaTO $remessaTO
	 * @return Ambigous <boolean, multitype:>
	 * @see RemessaBO::listarRemessa();
	 */
	public function listarRemessa(RemessaTO $remessaTO){
		$remessaORM = new RemessaORM();
		return $remessaORM->consulta($remessaTO);
	}
	
	/**
	 * Método de listagem dos materiais sem Remessa
	 * @param VwMaterialSemRemessaTO $matsr
	 * @return Ambigous <boolean, multitype:>
	 * @see RemessaBO::listarMaterialSemRemessa();
	 */
	public function listarMaterialSemRemessa(VwMaterialSemRemessaTO $matsr){
		$vwMaterialSemRemessaORM = new VwMaterialSemRemessaORM();
		return $vwMaterialSemRemessaORM->consulta($matsr);
	}
	
	/**
	 * Método de listagem dos materiais da Remessa
	 * @param VwRemessaMaterialTO $matsr
	 * @return Ambigous <boolean, multitype:>
	 * @see RemessaBO::listarRemessaMaterial();
	 */
	public function listarRemessaMaterial(VwRemessaMaterialTO $remessaMaterialTO){
		$vwRemessaMaterialORM = new VwRemessaMaterialORM();
		return $vwRemessaMaterialORM->consulta($remessaMaterialTO);
	}
}

?>
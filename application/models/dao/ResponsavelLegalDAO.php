<?php
/**
 * Classe de persistência do objeto Responsavel legal com o banco de dados
 * @author Eduardo Romão - ejushiro@gmail.com
 * 
 * @package models
 * @subpackage dao
 */
class ResponsavelLegalDAO extends SecretariaDAO{

	/**
	 * Método que cadastra(insert) o Responsavel Legal
	 * @param EntidadeResponsavelLegalTO $erlTO
	 * @see ResponsavelLegalBO::cadastrarEntidadeResponsavelLegal();
	 * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
	 */
	public function cadastrarEntidadeResponsavelLegal(EntidadeResponsavelLegalTO $erlTO){
		$entidadeResponsavelLegalORM = new EntidadeResponsavelLegalORM();
		$this->beginTransaction();
		try{
			$insert = $erlTO->toArrayInsert();
			$id = $entidadeResponsavelLegalORM->insert($insert);
			$this->commit();
			return $id;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}

	/**
	 * Método que edita(update) o Responsavel Legal
	 * @param EntidadeResponsavelLegalTO $erlTO
	 * @see ResponsavelLegalBO::editarEntidadeResponsavelLegal();
	 * @return boolean
	 */
	public function editarEntidadeResponsavelLegal(EntidadeResponsavelLegalTO $erlTO){
		$entidadeResponsavelLegalORM = new EntidadeResponsavelLegalORM();
		$this->beginTransaction();
		try{
			$update = $erlTO->toArrayUpdate(false,array('id_entidaderesponsavellegal'));
			$where = $entidadeResponsavelLegalORM->montarWhere($erlTO,true);
			$entidadeResponsavelLegalORM->update($update,$where);
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}

	/**
	 * Método que exclui(delete) o Responsavel Legal
	 * @param EntidadeResponsavelLegalTO $erlTO
	 * @see ResponsavelLegalBO::deletarEntidadeResponsavelLegal();
	 * @return boolean
	 */
	public function deletarEntidadeResponsavelLegal(EntidadeResponsavelLegalTO $erlTO){
		$entidadeResponsavelLegalORM = new EntidadeResponsavelLegalORM();
		$this->beginTransaction();
		try{
			$where = $entidadeResponsavelLegalORM->montarWhere($erlTO,true);
			$entidadeResponsavelLegalORM->delete($where);
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		}
	}

	/**
	 * Método que exclui(delete) o Responsavel Legal
	 * @param vwResponsavelLegalTO $vrlTO
	 * @see ResponsavelLegalBO::retornarEntidadeResponsavelLegal();
	 * @return Zend_Db_Table_Rowset_Abstract|false 
	 */
	public function retornarEntidadeResponsavelLegal(VwResponsavelLegalTO $vrlTO, $where = null){
		$vwResponsavelLegalORM = new VwResponsavelLegalORM();
		if(!$where){
			$where = $vwResponsavelLegalORM->montarWhere($vrlTO);
		}
		try{
			$obj = $vwResponsavelLegalORM->fetchAll($where);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		}
	}

	/**
	 * Método que exclui(delete) o Tipo de Responsavel Legal
	 * @param TipoEntidadeResponsavelLegalTO $erlTO
	 * @see ResponsavelLegalBO::retornarTipoEntidadeResponsavelLegal();
	 * @return Zend_Db_Table_Rowset_Abstract|false 
	 */
	public function retornarTipoEntidadeResponsavelLegal(TipoEntidadeResponsavelLegalTO $terlTO){
		try{
			$tipoEntidadeResponsavelLegalORM = new TipoEntidadeResponsavelLegalORM();
			$where = $tipoEntidadeResponsavelLegalORM->montarWhere($terlTO);
			$obj = $tipoEntidadeResponsavelLegalORM->fetchAll($where);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			THROW $e;
			return false;
		}
	}
}
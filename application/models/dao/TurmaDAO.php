<?php
/**
 * Classe de abstração de dados de turma
 * @author eduardoromao
 */
class TurmaDAO extends Ead1_DAO
{

    /**
     * Método que cadastra a turma
     * @param TurmaTO $to
     * @throws Zend_Exception
     * @return int $id_turma
     */
    public function cadastrarTurma(TurmaTO $to)
    {
        try {
            $id_turma = null;
            $this->beginTransaction();
            $orm = new TurmaORM();
            $id_turma = $orm->insert($to->toArrayInsert());
            $this->commit();
        } catch (Zend_Exception $e) {
            $this->rollBack();
            THROW $e;
        }
        return $id_turma;
    }

    /**
     * Método que cadastra a relação de turma com entidade
     * @param TurmaEntidadeTO $to
     * @throws Zend_Exception
     * @return int $id_turmaentidade
     */
    public function cadastrarTurmaEntidade(TurmaEntidadeTO $to)
    {
        try {

            $id_turmaentidade = null;
            $this->beginTransaction();
            $orm = new TurmaEntidadeORM();
            $id_turmaentidade = $orm->insert($to->toArrayInsert());
            $this->commit();
        } catch (Zend_Exception $e) {
            $this->rollBack();
            THROW $e;
        }
        return $id_turmaentidade;
    }

    /**
     * Método que cadastra a relação de turma com projeto
     * @param TurmaProjetoTO $to
     * @throws Zend_Exception
     * @return int $id_turmaprojeto
     */
    public function cadastrarTurmaProjeto(TurmaProjetoTO $to)
    {
        try {
            $id_turmaprojeto = null;
            $this->beginTransaction();
            $orm = new TurmaProjetoORM();
            $id_turmaprojeto = $orm->insert($to->toArrayInsert());
            $this->commit();
        } catch (Zend_Exception $e) {
            $this->rollBack();
            THROW $e;
        }
        return $id_turmaprojeto;
    }

    /**
     * Método que cadastra a relação de turma com matricula
     * @param TurmaMatriculaTO $to
     * @throws Zend_Exception
     * @return int $id_turmamatricula
     */
    public function cadastrarTurmaMatricula(TurmaMatriculaTO $to)
    {
        try {
            $id_turmamatricula = null;
            $this->beginTransaction();
            $orm = new TurmaMatriculaORM();
            $id_turmamatricula = $orm->insert($to->toArrayInsert());
            $this->commit();
        } catch (Zend_Exception $e) {
            $this->rollBack();
            THROW $e;
        }
        return $id_turmamatricula;
    }

    /**
     * Método que edita a relação de turma com matricula
     * @param TurmaMatriculaTO $to
     * @param Mixed $where
     * @throws Zend_Exception
     * @return Boolean
     */
    public function editarTurmaMatricula(TurmaMatriculaTO $to, $where = NULL)
    {
        try {
            $editado = false;
            $this->beginTransaction();
            $orm = new TurmaMatriculaORM();
            $orm->update($to->toArrayUpdate(false, array('id_turmamatricula', 'id_matricula', 'id_turma', 'dt_cadastro', 'id_usuariocadastro')), $where ? $where : $orm->montarWhere($to, true));
            $this->commit();
            $editado = true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            THROW $e;
        }
        return $editado;
    }

    /**
     * Método que edita a relação de turma com entidade
     * @param TurmaEntidadeTO $to
     * @param Mixed $where
     * @throws Zend_Exception
     * @return Boolean
     */
    public function editarTurmaEntidade(TurmaEntidadeTO $to, $where = NULL)
    {
        try {
            $editado = false;
            $this->beginTransaction();
            $orm = new TurmaEntidadeORM();
            $orm->update($to->toArrayUpdate(false, array('id_turmaentidade', 'id_turma', 'id_entidade', 'dt_cadastro', 'id_usuariocadastro')), $where ? $where : $orm->montarWhere($to, true));
            $this->commit();
            $editado = true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            THROW $e;
        }
        return $editado;
    }

    /**
     * Método que edita a relação de turma com projeto
     * @param TurmaProjetoTO $to
     * @param Mixed $where
     * @throws Zend_Exception
     * @return Boolean
     */
    public function editarTurmaProjeto(TurmaProjetoTO $to, $where = null)
    {
        try {
            $editado = false;
            $this->beginTransaction();
            $orm = new TurmaProjetoORM();
            $orm->update($to->toArrayUpdate(false, array('id_turmaprojeto', 'id_turma', 'id_usuariocadastro', 'id_projetopedagogico', 'dt_cadastro')), $where ? $where : $orm->montarWhere($to, true));
            $this->commit();
            $editado = true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            THROW $e;
        }
        return $editado;
    }

    /**
     * Método que edita a turma
     * @param TurmaTO $to
     * @param Mixed $where
     * @throws Zend_Exception
     * @return Boolean
     */
    public function editarTurma(TurmaTO $to, $where = NULL)
    {
        try {
            $editado = false;
            $this->beginTransaction();
            $orm = new TurmaORM();
            $orm->update($to->toArrayUpdate(false, array('id_turma', 'id_usuariocadastro', 'id_entidadecadastro', 'dt_cadastro')), $where ? $where : $orm->montarWhere($to, true));
            $this->commit();
            $editado = true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            THROW $e;
        }
        return $editado;
    }

    /**
     * Método que retorna a turma
     * @param TurmaTO $to
     * @param Mixed $where
     */
    public function retornarTurma(TurmaTO $to, $where = NULL)
    {
        $orm = new TurmaORM();
        return $orm->fetchAll($where ? $where : $orm->montarWhere($to, false));
    }

    /**
     * Método que retorna a relação de turma com Entidade
     * @param TurmaEntidadeTO $to
     * @param Mixed $where
     */
    public function retornarTurmaEntidade(TurmaEntidadeTO $to, $where = NULL)
    {
        $orm = new TurmaEntidadeORM();
        return $orm->fetchAll($where ? $where : $orm->montarWhere($to, false));
    }

    /**
     * Método que retorna a relação de turma com Projeto
     * @param TurmaEntidadeTO $to
     * @param Mixed $where
     */
    public function retornarTurmaProjeto(TurmaProjetoTO $to, $where = NULL)
    {
        $orm = new TurmaProjetoORM();
        return $orm->fetchAll($where ? $where : $orm->montarWhere($to, false));
    }

    /**
     * Método que retorna a relação de turma com Matricula
     * @param TurmaMatriculaTO $to
     * @param Mixed $where
     */
    public function retornarTurmaMatricula(TurmaMatriculaTO $to, $where = NULL)
    {
        $orm = new TurmaMatriculaORM();
        return $orm->fetchAll($where ? $where : $orm->montarWhere($to, true));
    }

    /**
     * Método que retorna a view vw_turma
     * @param VwTurmaTO $to
     * @param Mixed $where
     */
    public function retornarVwTurma(VwTurmaTO $to, $where = NULL)
    {
        $orm = new VwTurmaORM();
        return $orm->fetchAll($where ? $where : $orm->montarWhereView($to, false, true));
    }

    /**
     * Método que retorna a view vw_conferenciaturmas
     * @param VwTurmaTO $to
     * @param Mixed $where
     */
    public function retornarVwConferenciaTurmas(VwConferenciaTurmasTO $to, $where = NULL)
    {
        $orm = new VwConferenciaTurmaORM();
        $retorno = $orm->fetchAll($where ? $where : $orm->montarWhereView($to, false, true));
        return $retorno;
    }

    public function retornarVwConferenciaProjetoConsolidado(VwConferenciaProjetoConsolidadoTO $to, $where = NULL, $order = NULL)
    {
        $orm = new VwConferenciaProjetoConsolidadoORM();
        $where = $where ? $where : $orm->montarWhereView($to, false, true);
        $retorno = $orm->fetchAll($where, $order);
        return $retorno;
    }
}
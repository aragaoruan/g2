<?php

/**
 * Classe de persistencia do produto com o banco de dados
 * @author Eduardo Romão <ejushiro@gmail.com>
 * @since 21/02/2011
 *
 * @package models
 * @subpackage dao
 */
class MatriculaDAO extends Ead1_DAO
{

    /**
     * Método que cadastra(insert) uma Matricula
     * @param MatriculaTO $mTO
     * @see MatriculaBO::cadastrarMatricula();
     * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
     */
    public function cadastrarMatricula(MatriculaTO $mTO)
    {
        $matriculaORM = new MatriculaORM();
        $this->beginTransaction();
        try {
            $insert = $mTO->toArrayInsert();
            $id = $matriculaORM->insert($insert);
            $this->commit();
            return $id;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception(
                $e->getMessage());
            return false;
        }
    }

    /**
     * Método que cadastra(insert) um registro
     * @param LivroRegistroTO $lrTO
     * @see MatriculaBO::cadastrarRegistro();
     * @return boolean
     */
    public function cadastrarRegistro(LivroRegistroTO $lrTO)
    {
        $livroRegistroORM = new LivroRegistroORM();
        $this->beginTransaction();
        try {
            $insert = $lrTO->toArrayInsert();
            $livroRegistroORM->insert($insert);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();;
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que cadastra(insert) um Contrato
     * @param ContratoTO $cTO
     * @see MatriculaBO::cadastrarContrato();
     * @throws Zend_Exception
     * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
     */
    public function cadastrarContrato(ContratoTO $cTO)
    {
        $contratoORM = new ContratoORM();
        $this->beginTransaction();
        try {
            $insert = $cTO->toArrayInsert();
            $id = $contratoORM->insert($insert);
            $this->commit();
            return $id;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage());
        }
    }

    /**
     * Metodo que cadastra Regra de Contrato
     * @see MatriculaBO::cadastrarContratoRegra
     * @param ContratoRegraTO $crTO
     * @return var $id_contratoregra | boolean false
     */
    public function cadastrarContratoRegra(
        ContratoRegraTO $crTO)
    {
        $contratoRegraORM = new ContratoRegraORM();
        unset($crTO->arrDados);
        $insert = $crTO->toArrayInsert();
        $this->beginTransaction();
        try {
            $id_contratoregra = $contratoRegraORM->insert(
                $insert);
            $this->commit();
            return $id_contratoregra;
        } catch (Exception $e) {
            $this->rollBack();
            $arr['TO'] = $crTO;
            $arr['WHERE'] = $insert;
            $arr['SQL'] = $e->getMessage();
            $this->excecao = $arr;
            return false;
        }
    }

    /**
     * Método que cadastra(insert) um Contratomatricula
     * @param ContratoMatriculaTO $cmTO
     * @see MatriculaBO::cadastrarContratoMatricula();
     * @return Boolean
     */
    public function cadastrarContratoMatricula(
        ContratoMatriculaTO $cmTO)
    {
        $contratoMatriculaORM = new ContratoMatriculaORM();
        $this->beginTransaction();
        try {
            $insert = $cmTO->toArrayInsert();
            $contratoMatriculaORM->insert($insert);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
        }
    }

    /**
     * Método que cadastra(insert) um Contratoconvenio
     * @param ContratoMatriculaTO $ccTO
     * @see MatriculaBO::cadastrarContratoMatricula();
     * @return Boolean
     */
    public function cadastrarContratoConvenio(ContratoConvenioTO $ccTO)
    {
        try {
            $this->beginTransaction();
            $contratoConvenioORM = new ContratoConvenioORM();
            $insert = $ccTO->toArrayInsert();
            $contratoConvenioORM->insert($insert);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Método que cadastra(insert) um Convenio
     * @param ConvenioTO $cTO
     * @see MatriculaBO::cadastrarConvenio();
     * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
     */
    public function cadastrarConvenio(ConvenioTO $cTO)
    {
        $convenioORM = new ConvenioORM();
        $this->beginTransaction();
        try {
            $insert = $cTO->toArrayInsert();
            $id = $convenioORM->insert(
                $insert);
            $this->commit();
            return $id;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception(
                $e->getMessage());
            return false;
        }
    }

    /**
     * Método que cadastra(insert) um Responsavel pelo contrato
     * @param ContratoResponsavelTO $crTO
     * @see MatriculaBO::cadastrarContratoResponsavel();
     * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
     */
    public function cadastrarContratoResponsavel(
        ContratoResponsavelTO $crTO)
    {
        $contratoResponsavelORM = new ContratoResponsavelORM();
        $this->beginTransaction();
        try {
            $insert = $crTO->toArrayInsert();
            $id = $contratoResponsavelORM->insert(
                $insert);
            $this->commit();
            return $id;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception(
                $e->getMessage());
            return false;
        }
    }

    /**
     * Método que cadastra(insert) um Envolvido no Contrato
     * @param ContratoEnvolvidoTO $ceTO
     * @see MatriculaBO::cadastrarContratoEnvolvido();
     * @return boolean
     */
    public function cadastrarContratoEnvolvido(
        ContratoEnvolvidoTO $ceTO)
    {
        $contratoEnvolvidoORM = new ContratoEnvolvidoORM();
        $this->beginTransaction();
        try {
            $insert = $ceTO->toArrayInsert();
            $contratoEnvolvidoORM->insert(
                $insert);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception(
                $e->getMessage());
            return false;
        }
    }

    /**
     * Cadastra uma venda de um contrato
     * @param VendaTO $vendaTO
     * @return int - chave primária  inserida
     */
    public function cadastrarVendaContrato(VendaTO $vendaTO)
    {
        $dao = new VendaDAO();
        return $dao->cadastrarVenda($vendaTO);
    }

    /**
     * Metodo que cadastra o nivel e serie da matricula
     * @param MatriculaNivelSerieTO $to
     * @return Boolean
     */
    public function cadastrarMatriculaNivelSerie(MatriculaNivelSerieTO $to)
    {
        try {
            $this->beginTransaction();
            $orm = new MatriculaNivelSerieORM();
            $insert = $to->toArrayInsert();
            $orm->insert($insert);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Edita uma venda de um contrato
     * @param VendaTO $vendaTO
     * @return int - chave primária  inserida
     */
    public function editarVenda(VendaTO $vendaTO)
    {
        $dao = new VendaDAO();
        return $dao->editarVenda($vendaTO);
    }

    /**
     * Cadastra um produto há uma venda
     * @param VendaProdutoTO $vendaProdutoTO
     * @return int - chave primária inserida
     */
    public function cadastrarVendaProdutoProjetoPedagogico(
        VendaProdutoTO $vendaProdutoTO)
    {
        $vendaProdutoORM = new VendaProdutoORM();
        return $vendaProdutoORM->insert(
            $vendaProdutoTO->toArrayInsert());
    }

    /**
     * Cadastra o vínculo de uma vendaproduto com as séries e níveis de ensino
     * @param VendaProdutoNivelSerieTO $vendaProdutoNivelSerieTO
     * @return int - chave primária inserida
     */
    public function cadastrarVendaProdutoNivelSerie(VendaProdutoNivelSerieTO $vendaProdutoNivelSerieTO)
    {
        $vendaProdutoNivelSerieORM = new VendaProdutoNivelSerieORM();
        return $vendaProdutoNivelSerieORM->insert($vendaProdutoNivelSerieTO->toArrayInsert());
    }

    /**
     *
     * Cadastra as disciplinas de uma determinada matrícula
     * @param int $idContrato - id_contrato do contrato contenndo as matriculas
     * @see MatriculaBO::procedimentoAtivarMatricula()
     * @return array
     */
    public function cadastrarMatriculaDisciplinaSP($idContrato)
    {
        try {
            $statement = Zend_Db_Table::getDefaultAdapter()->prepare('EXEC sp_matriculadisciplina ' . $idContrato);
            $statement->execute();
            $rs = $statement->fetchAll();
            return $rs;
        } catch (zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     *
     * Cadastra as disciplinas de uma determinada matrícula
     * @param int $idMatricula - id da matrícula
     * @see MatriculaBO::matricularDireto()
     * @return array
     */
    public function cadastrarMatriculaDisciplinaDiretaSP($idMatricula)
    {
        try {
            $statement = Zend_Db_Table::getDefaultAdapter()->prepare('EXEC sp_matriculadisciplinadireta ' . $idMatricula);
            $statement->execute();
            $rs = $statement->fetchAll();
            return $rs;
        } catch (zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Método que cadastra(insert) uma disciplina para ser aproveitada.
     * @param AproveitamentoMatriculaDisciplina $amdTO
     * @see MatriculaBO::cadastrarAproveitamento();
     * @return boolean
     */
    public function cadastrarAproveitamentoMatriculaDisciplina(
        AproveitamentoMatriculaDisciplinaTO $amdTO)
    {
        $aproveitamentoMatriculaDisciplinaORM = new AproveitamentoMatriculaDisciplinaORM();
        $this->beginTransaction();
        try {
            $insert = $amdTO->toArrayInsert();
            $aproveitamentoMatriculaDisciplinaORM->insert(
                $insert);
            $tramiteBO = new TramiteBO();
            $disciplinaORM = new DisciplinaORM();
            $disciplinaTO = new DisciplinaTO();
            $disciplinaTO->setId_disciplina($amdTO->getId_disciplina());
            $disciplinaTO->setBl_ativa(true);
            $disciplinaTO = $disciplinaORM->consulta($disciplinaTO, true, true);
            // $tramiteBO->cadastrarTramiteAproveitamentoDisciplina($amdTO->getId_matricula(), ($disciplinaTO ? $disciplinaTO->getSt_disciplina() : null));
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception(
                $e->getMessage());
            return false;
        }
    }

    /**
     * Método que cadastra(insert) um Aproveitamento de disciplina
     * @param AproveitamentoDisciplinaTO $amTO
     * @see MatriculaBO::cadastrarAproveitamentoDisciplina();
     * @return boolean
     */
    public function cadastrarAproveitamentoDisciplina(
        AproveitamentoDisciplinaTO $amTO)
    {
        $aproveitamentoDisciplinaORM = new AproveitamentoDisciplinaORM();
        $this->beginTransaction();
        try {
            $insert = $amTO->toArrayInsert();
            $id_aproveitamentodisciplina = $aproveitamentoDisciplinaORM->insert(
                $insert);
            $this->commit();
            return $id_aproveitamentodisciplina;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception(
                $e->getMessage());
            return false;
        }
    }

    /**
     * Método que Edita(update) uma Matricula Disciplina
     * @param MatriculaDisciplinaTO $mdTO
     * @param Mixed $where
     * @see MatriculaBO::cadastrarAproveitamento();
     * @return boolean
     */
    public function editarMatriculaDisciplina(MatriculaDisciplinaTO $mdTO, $where = null)
    {
        $matriculaDisciplinaORM = new MatriculaDisciplinaORM();
        $this->beginTransaction();
        try {
            if (!$where) {
                $where = $matriculaDisciplinaORM->montarWhere($mdTO, true);
            }
            if (!$where) {
                throw new Zend_Exception(
                    'O TO de matrícula disciplina não pode ser vazio!');
            }
            $update = $mdTO->toArrayUpdate(false, array('id_matriculadisciplina'));
            $matriculaDisciplinaORM->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que Edita(update) uma Matricula
     * @param MatriculaTO $mTO
     * @param null $where
     * @return bool
     * @throws Exception
     * @throws Zend_Exception
     */
    public function editarMatricula(MatriculaTO $mTO, $where = null)
    {
        $matriculaORM = new MatriculaORM();
        $this->beginTransaction();
        try {
            $tramiteBO = new TramiteBO();
            $tramiteBO->cadastrarTramiteEvolucaoMatricula($mTO);
            $tramiteBO->cadastrarTramiteTransferenciaEntidadeAtendimentoMatricula($mTO);
            $update = $mTO->toArrayUpdate(false, array('id_usuariocadastro', 'id_matricula'));
            if (!$where) {
                $where = $matriculaORM->montarWhere($mTO, true);
            }
            if (empty($where)) {
                THROW new Zend_Exception('O TO não pode vir vazio! 1');
            }
            $matriculaORM->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            throw $e;
        }
    }

    /**
     * Método que Edita(update) um Contrato
     * @param ContratoTO $cTO
     * @param Mixed $where
     * @see MatriculaBO::editarContrato();
     * @see ContratoBO::extenderContrato();
     * @return boolean
     */
    public function editarContrato(ContratoTO $cTO, $where = null)
    {
        try {
            $this->beginTransaction();
            $contratoORM = new ContratoORM();
            if (!$where) {
                $where = $contratoORM->montarWhere($cTO, true);
            }
            if (empty($where)) {
                THROW new Zend_Exception('O TO de Contrato não pode vir vazio! 2');
            }
            $update = $cTO->toArrayUpdate(false, array('id_contrato', 'dt_cadastro'));
            $contratoORM->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Meotodo que Edita Regra de Contrato
     * @param ContratoRegraTO $crTO
     * @see MatriculaBO::editarContratoRegra
     * @return Boolean
     */
    public function editarContratoRegra(ContratoRegraTO $crTO)
    {
        $contratoRegraORM = new ContratoRegraORM();
        $update = $crTO->toArrayUpdate(false, array('id_contratoregra'));
        $this->beginTransaction();
        try {
            $contratoRegraORM->update(
                $update, " id_contratoregra = " .
                $crTO->getId_contratoregra());
            $this->commit();
            return true;
        } catch (Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que Edita(update) um ContratoMatricula
     * @param ContratoMatriculaTO $cmTO
     * @see MatriculaBO::editarContratoMatricula();
     * @return boolean
     */
    public function editarContratoMatricula(
        ContratoMatriculaTO $cmTO)
    {
        $contratoMatriculaORM = new ContratoMatriculaORM();
        $this->beginTransaction();
        try {
            $where = $contratoMatriculaORM->montarWhere($cmTO, true);
            if (empty($where)) {
                THROW new Zend_Exception('O TO não pode vir vazio! 3');
            }
            $update = $cmTO->toArrayUpdate(false);
            $contratoMatriculaORM->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
        }
    }

    /**
     * Método que Edita(update) um ContratoConvenio
     * @param ContratoConvenioTO $ccTO
     * @see MatriculaBO::editarContratoConvenio();
     * @return boolean
     */
    public function editarContratoConvenio(
        ContratoConvenioTO $ccTO)
    {
        $contratoConvenioORM = new ContratoConvenioORM();
        $this->beginTransaction();
        try {
            $where = $contratoConvenioORM->montarWhere(
                $ccTO, true);
            if (empty($where)) {
                THROW new Zend_Exception(
                    'O TO não pode vir vazio! 4');
            }
            $update = $ccTO->toArrayUpdate(
                false);
            $contratoConvenioORM->update(
                $update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception(
                $e->getMessage());
            return false;
        }
    }

    /**
     * Método que Edita(update) um Convenio
     * @param ConvenioTO $cTO
     * @see MatriculaBO::editarConvenio();
     * @return boolean
     */
    public function editarConvenio(ConvenioTO $cTO)
    {
        $convenioORM = new ConvenioORM();
        $this->beginTransaction();
        try {
            $where = $convenioORM->montarWhere(
                $cTO, true);
            if (empty($where)) {
                THROW new Zend_Exception(
                    'O TO não pode vir vazio! 5');
            }
            $update = $cTO->toArrayUpdate(
                false, array('id_convenio'));
            $convenioORM->update(
                $update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception(
                $e->getMessage());
            return false;
        }
    }

    /**
     * Método que Edita(update) um Contrato Responsavel
     * @param ContratoResponsavelTO $crTO
     * @param string $where
     * @see MatriculaBO::editarContratoResponsavel();
     * @throws Zend_Exception
     * @return boolean
     */
    public function editarContratoResponsavel(ContratoResponsavelTO $crTO, $where = null)
    {
        $contratoResponsavelORM = new ContratoResponsavelORM();
        $this->beginTransaction();
        try {
            if (!$where) {
                $where = $contratoResponsavelORM->montarWhere($crTO, false);
            }
            if (empty($where)) {
                THROW new Zend_Exception('O TO não pode vir vazio! 6');
            }
            $update = $crTO->toArrayUpdate(false, array('id_contratoresponsavel', 'id_usuario'));
            $contratoResponsavelORM->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception(
                $e->getMessage());
            return false;
        }
    }

    /**
     * Método que Edita(update) um ContratoEnvolvido
     * @param ContratoEnvolvidoTO $ceTO
     * @see MatriculaBO::editarContratoResponsavel();
     * @return boolean
     */
    public function editarContratoEnvolvido(
        ContratoEnvolvidoTO $ceTO)
    {
        $contratoEnvolvidoORM = new ContratoEnvolvidoORM();
        $this->beginTransaction();
        try {
            $where = $contratoEnvolvidoORM->montarWhere(
                $ceTO, true);
            if (empty($where)) {
                THROW new Zend_Exception(
                    'O TO não pode vir vazio! 7');
            }
            $update = $ceTO->toArrayUpdate(
                false, array(
                'id_tipoenvolvido',
                'id_contrato'));
            $contratoEnvolvidoORM->update(
                $update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception(
                $e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que edita o aproveitamento da disciplina
     * @param AproveitamentoDisciplinaTO $adTO
     * @param Mixed $where
     */
    public function editarAproveitamento(AproveitamentoDisciplinaTO $adTO, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new AproveitamentoDisciplinaORM();
            if (!$where) {
                $where = $orm->montarWhere($adTO, true);
            }
            $update = $adTO->toArrayUpdate(false, array('id_aproveitamentodisciplina', 'id_usuariocadastro'));
            $orm->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Método que deleta a Matricula
     * @param MatriculaTO $mTO
     * @see MatriculaBO::deletarMatricula();
     * @return boolean
     */
    public function deletarMatricula(MatriculaTO $mTO)
    {
        $matriculaORM = new MatriculaORM();
        $this->beginTransaction();
        try {
            $where = $matriculaORM->montarWhere(
                $mTO);
            if (empty($where)) {
                THROW new Zend_Exception(
                    'O TO não pode vir vazio! 8');
            }
            $matriculaORM->delete(
                $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception(
                $e->getMessage());
            return false;
        }
    }

    /**
     * Método que deleta o Contrato
     * @param ContratoTO $cTO
     * @see MatriculaBO::deletarContrato();
     * @return boolean
     */
    public function deletarContrato(ContratoTO $cTO)
    {
        $contratoORM = new ContratoORM();
        $this->beginTransaction();
        try {
            $where = $contratoORM->montarWhere(
                $cTO, true);
            if (empty($where)) {
                THROW new Zend_Exception(
                    'O TO não pode vir vazio! 9');
            }
            $contratoORM->delete(
                $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception(
                $e->getMessage());
            return false;
        }
    }

    /**
     * Método que deleta o ContratoMatricula
     * @param ContratoMatriculaTO $cmTO
     * @see MatriculaBO::deletarContratoMatricula();
     * @return boolean
     */
    public function deletarContratoMatricula(
        ContratoMatriculaTO $cmTO)
    {
        $contratoMatriculaORM = new ContratoMatriculaORM();
        $this->beginTransaction();
        try {
            $where = $contratoMatriculaORM->montarWhere($cmTO, true);
            if (empty($where)) {
                THROW new Zend_Exception('O TO não pode vir vazio! 10');
            }
            $contratoMatriculaORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
        }
    }

    /**
     * Método que deleta o ContratoConvenio
     * @param ContratoConvenioTO $ccTO
     * @param Mixed $where
     * @see MatriculaBO::deletarContratoConvenio();
     * @return boolean
     */
    public function deletarContratoConvenio(ContratoConvenioTO $ccTO, $where = null)
    {
        try {
            $this->beginTransaction();
            $contratoConvenioORM = new ContratoConvenioORM();
            if (!$where) {
                $where = $contratoConvenioORM->montarWhere($ccTO, true);
            }
            if (empty($where)) {
                THROW new Zend_Exception('O TO não pode vir vazio! 11');
            }
            $contratoConvenioORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Método que deleta o Contrato]
     * @param ContratoTO $cTO
     * @see MatriculaBO::deletarConvenio();
     * @return boolean
     */
    public function deletarConvenio(ConvenioTO $cTO)
    {
        $convenioORM = new ConvenioORM();
        $this->beginTransaction();
        try {
            $where = $convenioORM->montarWhere(
                $cTO, true);
            if (empty($where)) {
                THROW new Zend_Exception(
                    'O TO não pode vir vazio! 12');
            }
            $convenioORM->delete(
                $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception(
                $e->getMessage());
            return false;
        }
    }

    /**
     * Método que deleta o ContratoResponsavel
     * @param ContratoResponsavelTO $crTO
     * @param string $where
     * @see MatriculaBO::deletarContratoResponsavel();
     * @throws Zend_Exception
     * @return boolean
     */
    public function deletarContratoResponsavel(
        ContratoResponsavelTO $crTO, $where = null)
    {
        $contratoResponsavelORM = new ContratoResponsavelORM();
        $this->beginTransaction();
        try {
            if (!$where) {
                $where = $contratoResponsavelORM->montarWhere(
                    $crTO);
            }
            if (empty($where)) {
                THROW new Zend_Exception(
                    'O TO não pode vir vazio! 13');
            }
            $contratoResponsavelORM->delete(
                $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception(
                $e->getMessage());
            return false;
        }
    }

    /**
     * Método que deleta o Aproveitamento com disciplina
     * @param AproveitamentoMatriculaDisciplinaTO $amdTO
     * @param string $where
     * @see MatriculaBO::cadastrarAproveitamento();
     * @throws Zend_Exception
     * @return boolean
     */
    public function deletarAproveitamentoMatriculaDisciplina(AproveitamentoMatriculaDisciplinaTO $amdTO, $where = null)
    {
        $aproveitamentoMatriculaDisciplinaORM = new AproveitamentoMatriculaDisciplinaORM();
        $this->beginTransaction();
        try {
            if (!$where) {
                $where = $aproveitamentoMatriculaDisciplinaORM->montarWhere($amdTO);
            }
            if (empty($where)) {
                THROW new Zend_Exception('O TO não pode vir vazio! 14');
            }
            $aproveitamentoMatriculaDisciplinaORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Método que deleta a Venda e os vínculos de VendaProduto
     * @param VendaTO $vendaTO
     * @see MatriculaBO::deletarVenda();
     * @throws Zend_Exception
     * @return boolean
     */
    public function deletarVenda(VendaTO $vendaTO)
    {
        $vendaORM = new VendaORM();
        $vendaProdutoORM = new VendaProdutoORM();
        $this->beginTransaction();
        try {
            $where = $vendaORM->montarWhere(
                $vendaTO);
            if (empty($where)) {
                THROW new Zend_Exception(
                    'O TO não pode vir vazio! 15');
            }
            $vendaProdutoORM->delete($where);
            $vendaORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
        }
    }

    /**
     * Método que deleta o ContratoEnvolvido
     * @param ContratoEnvolvidoTO $ceTO
     * @param Mixed $where
     * @see MatriculaBO::deletarContratoEnvolvido();
     * @return boolean
     */
    public function deletarContratoEnvolvido(ContratoEnvolvidoTO $ceTO, $where = null)
    {
        try {
            $this->beginTransaction();
            $contratoEnvolvidoORM = new ContratoEnvolvidoORM();
            if (!$where) {
                $where = $contratoEnvolvidoORM->montarWhere($ceTO, true);
            }
            if (empty($where)) {
                THROW new Zend_Exception('O TO não pode vir vazio! 16');
            }
            $contratoEnvolvidoORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage());
        }
    }

    /**
     * Método que exclui a disciplina aproveitada
     * @param VwAproveitamentoTO $vwAproveitamentoTO
     * @return Ead1_Mensageiro
     */
    public function deletarDisciplinaAproveitada(VwAproveitamentoTO $vwAproveitamentoTO)
    {
        try {
            $this->beginTransaction();
            $tbMatriculaDisciplinaORM = new MatriculaDisciplinaORM();
            $dadosMatriculaDisciplina['nu_aprovafinal'] = NULL;
            $dadosMatriculaDisciplina['id_evolucao'] = 11;
            $dadosMatriculaDisciplina['id_situacao'] = 53;
            $whereMatriculaDisciplina = "id_matricula = {$vwAproveitamentoTO->getId_matricula()} AND id_disciplina = {$vwAproveitamentoTO->getId_disciplina()}";
            $tbMatriculaDisciplinaORM->update($dadosMatriculaDisciplina, $whereMatriculaDisciplina);

            $tbAproveitamentoMatriculaDisciplina = new AproveitamentoMatriculaDisciplinaORM();
            $tbAproveitamentoMatriculaDisciplina->delete($whereMatriculaDisciplina);

            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage());
        }
    }

    /**
     * Metodo que excluir os vinculos dos produtos da venda com o nivel e serie
     * @param VendaProdutoNivelSerieTO $to
     * @param Mixed $where
     * @return Boolean
     */
    public function deletarVendaProdutoNivelSerie(VendaProdutoNivelSerieTO $to, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new VendaProdutoNivelSerieORM();
            if (!$where) {
                $where = $orm->montarWhere($to, false);
            }
            if (!$where) {
                THROW new Zend_Exception('O TO não pode vir vazio! 17');
            }
            $orm->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            THROW $e;
            return false;
        }
    }

    /**
     * Método que retorna Matricula
     * @param MatriculaTO $mTO
     * @param String $where
     * @see MatriculaBO::retornarMatricula() |MatriculaBO::procedimentoAtivarMatricula();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarMatricula(MatriculaTO $mTO, $where = null)
    {
        $matriculaORM = new MatriculaORM();
        if (!$where) {
            $where = $matriculaORM->montarWhere(
                $mTO);
        }
        try {
            $obj = $matriculaORM->fetchAll(
                $where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception(
                $e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna view de Matricula
     * @param VwMatriculaTO $vmTO
     * @param Mixed $where
     * @see MatriculaBO::retornarVwMatricula() | MatriculaBO::retornarGridAlunosConluintes();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwMatricula(VwMatriculaTO $vmTO, $where = null, $order = array())
    {
        try {
            $vwMatriculaORM = new VwMatriculaORM();
            if (!$where) {
                $where = $vwMatriculaORM->montarWhereView($vmTO, false, true);
            }
            $obj = $vwMatriculaORM->fetchAll($where, $order);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Método que retorna view de Matricula
     * @param VwMatriculaTO $to
     * @param Mixed $where
     * @see PessoaBO::pesquisarPessoaAutoComplete();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwMatriculaPesquisaPessoa(VwMatriculaTO $to)
    {
        try {
            $vwMatriculaORM = new VwMatriculaORM();
            $to->tipaTOConsulta();
            $perfilTO = new VwPerfisEntidadeTO();
            $perfilDAO = new PerfilDAO();
            $perfilTO->setId_entidade($to->getId_entidadematricula());
            $retornoEntidades = $perfilDAO->retornaEntidadeRecursivaPerfil($perfilTO);
            $entidadesFilhas = $to->montaDadosIN($retornoEntidades, 'id_entidade');
            $separador = ($entidadesFilhas) ? ',' : '';
            $where = "(";
            if ($to->getId_usuario()) {
                $pars = " id_usuario = " . $to->getId_usuario();
                $where .= $this->verificaAndOr($where, $pars);
            }
            if ($to->getSt_nomecompleto()) {
                $pars = "st_nomecompleto like '%" . $to->getSt_nomecompleto() . "%'";
                $where .= $this->verificaAndOr($where, $pars);
            }
            if ($to->getSt_cpf()) {
                $pars = "st_cpf like '%" . $to->getSt_cpf() . "%'";
                $where .= $this->verificaAndOr($where, $pars);
            }
            if ($to->getSt_email()) {
                $pars = "st_email like '%" . $to->getSt_email() . "%'";
                $where .= $this->verificaAndOr($where, $pars);
            }
            if ($to->getId_situacao()) {
                $where .= " AND id_situacao = " . $to->getId_situacao();
            }
            $where .= ")";
            if ($where == "()") {
                $where = ($to->getId_entidadematricula()) ? " id_entidadematricula in (" . $to->getId_entidadematricula() . $separador . $entidadesFilhas . ") " : "";
            } else {
                $where .= " AND " . ($to->getId_entidadematricula() ? " id_entidadematricula in (" . $to->getId_entidadematricula() . $separador . $entidadesFilhas . ") " : "");
            }
            //Zend_Debug::dump($where);exit;
            $obj = $vwMatriculaORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna view de grade e sala do aluno.
     * @param VwMatriculaGradeSalaTO $vmgsTO
     * @param Mixed $where
     * @see MatriculaBO::retornarVwMatriculaGradeSala();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwMatriculaGradeSala(VwMatriculaGradeSalaTO $vmgsTO, $where = null)
    {
        try {
            $vwMatriculaGradeSalaORM = new VwMatriculaGradeSalaORM();
            if (!$where) {
                $where = $vwMatriculaGradeSalaORM->montarWhere($vmgsTO);
            }
            $obj = $vwMatriculaGradeSalaORM->fetchAll($where, 'dt_abertura ASC');
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Método que retorna view de grade e sala do aluno.
     * @param VwGridTipoTO $vwGradeTipoTO
     * @param Mixed $where
     * @see MatriculaBO::retornarVwGradeTipo();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwGradeTipo(VwGradeTipoTO $vwGradeTipoTO, $where = null)
    {
        try {
            $vwGradeTipoORM = new VwGradeTipoORM();
            if (!$where) {
                $where = $vwGradeTipoORM->montarWhere($vwGradeTipoTO);
            }
            $obj = $vwGradeTipoORM->fetchAll($where, 'id_tipoavaliacao');
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que retorna módulo para notas de aluno
     * @param VwMatriculaGradeSalaTO $vmgsTO
     * @see AvaliacaoBO::retornarVwMatriculaGradeSala()
     * @return Zend_Db_Table_Rowset_Abstract | false
     */
    public function retornarVwMatriculaGradeSalaModulo(VwMatriculaGradeSalaTO $vmgsTO)
    {
        try {
            $orm = new VwMatriculaGradeSalaORM();
            $where = $orm->montarWhereView($vmgsTO, false, true);
            $select = $orm->select()
                ->distinct(true)
                ->from('vw_matriculagradesala', '')
                ->setIntegrityCheck(false)
                ->columns(array('id_usuario', 'id_matricula', 'id_modulo', 'st_modulo', 'st_moduloexibicao'))
                ->where($where);
            return $orm->fetchAll($select);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que retorna módulo para notas de aluno por tipo
     * @param VwGradeTipoTO $vwGradeTipoTO
     * @see AvaliacaoBO::retornarVwGradeTipo()
     * @return Zend_Db_Table_Rowset_Abstract | false
     */
    public function retornarVwGradeTipoModulo(VwGradeTipoTO $vwGradeTipoTO)
    {
        try {
            $orm = new VwGradeTipoORM();
            $where = $orm->montarWhereView($vwGradeTipoTO, false, true);
            $select = $orm->select()
                ->distinct(true)
                ->from('vw_gradetipo', '')
                ->setIntegrityCheck(false)
                ->columns(array('id_matricula', 'id_modulo', 'st_modulo'))
                ->where($where);
            return $orm->fetchAll($select);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Método que retorna Matricula
     * @param VwUsuarioMatriculaProjetoTO $vumpTO
     * @see MatriculaBO::retornarMatriculaUsuarioProjeto();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarMatriculaUsuarioProjeto(
        VwUsuarioMatriculaProjetoTO $vumpTO)
    {
        $matriculaORM = new VwUsuarioMatriculaProjetoORM();
        $where = $matriculaORM->montarWhere(
            $vumpTO);
        try {
            $obj = $matriculaORM->fetchAll(
                $where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception(
                $e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna Contrato
     * @param ContratoTO $cTO
     * @see MatriculaBO::retornarContrato() | MatriculaBO::ativarMatricula();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarContrato(ContratoTO $cTO, $where = null)
    {
        $contratoORM = new ContratoORM();
        if (!$where) {
            $where = $contratoORM->montarWhere($cTO);
        }
        try {
            $obj = $contratoORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception(
                $e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna ContratoMatricula
     * @param ContratoMatriculaTO $cmTO
     * @see MatriculaBO::retornarContratoMatricula();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarContratoMatricula(ContratoMatriculaTO $cmTO)
    {
        $contratoMatriculaORM = new ContratoMatriculaORM();
        $where = $contratoMatriculaORM->montarWhere($cmTO);

        if ($where == null)
            throw new Zend_Exception('Não pode ser realizado a query sem condição WHERE');

        try {
            $obj = $contratoMatriculaORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage());
        }
    }

    /**
     * Método que retorna ContratoConvenio
     * @param ContratoConvenioTO $ccTO
     * @see MatriculaBO::retornarContratoConvenio();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarContratoConvenio(ContratoConvenioTO $ccTO)
    {
        $contratoConvenioORM = new ContratoConvenioORM();
        $where = $contratoConvenioORM->montarWhere($ccTO);
        try {
            $obj = $contratoConvenioORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception(
                $e->getMessage());
        }
    }

    /**
     * Método que retorna Convenio
     * @param ConvenioTO $cTO
     * @see MatriculaBO::retornarConvenio();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarConvenio(ConvenioTO $cTO)
    {
        $convenioORM = new ConvenioORM();
        $where = $convenioORM->montarWhere($cTO);
        try {
            $obj = $convenioORM->fetchAll(
                $where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception(
                $e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna ContratoResponsavel
     * @param ContratoResponsavelTO $crTO
     * @param string $where
     * @see MatriculaBO::retornarContratoResponsavel();
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarContratoResponsavel(
        ContratoResponsavelTO $crTO, $where = null)
    {
        $contratoResponsavelORM = new ContratoResponsavelORM();
        if (!$where) {
            $where = $contratoResponsavelORM->montarWhere(
                $crTO);
        }
        try {
            $obj = $contratoResponsavelORM->fetchAll(
                $where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception(
                $e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna registro de matrícula.
     * @param LivroRegistroTO $lrTO
     * @param string $where
     * @see MatriculaBO::cadastrarRegistro();
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarRegistro(LivroRegistroTO $lrTO, $where = null, $orderby = null)
    {
        $livroRegistroORM = new LivroRegistroORM();
        if (!$where) {
            $where = $livroRegistroORM->montarWhere($lrTO);
        }
        try {
            if (!$orderby) {
                $obj = $livroRegistroORM->fetchAll($where);
            } else {
                $obj = $livroRegistroORM->fetchRow($where, $orderby);
            }
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna ContratoEnvolvido
     * @param ContratoEnvolvidoTO $ceTO
     * @see MatriculaBO::retornarContratoEnvolvido();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarContratoEnvolvido(
        ContratoEnvolvidoTO $ceTO)
    {
        $contratoEnvolvidoORM = new ContratoEnvolvidoORM();
        $where = $contratoEnvolvidoORM->montarWhere(
            $ceTO);
        try {
            $obj = $contratoEnvolvidoORM->fetchAll(
                $where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception(
                $e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna TipoContratoResponsavel
     * @param TipoContratoResponsavelTO $tcrTO
     * @see MatriculaBO::retornarTipoContratoResponsavel();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarTipoContratoResponsavel(
        TipoContratoResponsavelTO $tcrTO)
    {
        $tipoContratoResponsavelORM = new TipoContratoResponsavelORM();
        $where = $tipoContratoResponsavelORM->montarWhere(
            $tcrTO);
        try {
            $obj = $tipoContratoResponsavelORM->fetchAll(
                $where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception(
                $e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna TipoEnvolvido
     * @param TipoEnvolvidoTO $teTO
     * @see MatriculaBO::retornarTipoEnvolvido();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarTipoEnvolvido(TipoEnvolvidoTO $teTO)
    {
        $tipoEnvolvidoORM = new TipoEnvolvidoORM();
        $where = $tipoEnvolvidoORM->montarWhere(
            $teTO);
        try {
            $obj = $tipoEnvolvidoORM->fetchAll(
                $where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception(
                $e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna Evolucao
     * @param EvolucaoTO $eTO
     * @see MatriculaBO::retornarEvolucao();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarEvolucao(EvolucaoTO $eTO)
    {
        $evolucaoORM = new EvolucaoORM();
        $where = $evolucaoORM->montarWhere($eTO);
        try {
            $obj = $evolucaoORM->fetchAll(
                $where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception(
                $e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna usuarios do contrato responsavel
     * @param VwUsuarioContratoResponsavelTO $vwUcrTO
     * @see MatriculaBO::retornarVwUsuarioContratoResponsavel();
     * @return Zend_Db_Table_Rowset_Abstract|false
     * @throws Zend_Exception
     */
    public function retornarVwUsuarioContratoResponsavel(
        VwUsuarioContratoResponsavelTO $vwUcrTO)
    {
        $vwUsuarioContratoResponsavelORM = new VwUsuarioContratoResponsavelORM();
        $where = $vwUsuarioContratoResponsavelORM->montarWhereView(
            $vwUcrTO, false, true, false);
        try {
            $obj = $vwUsuarioContratoResponsavelORM->fetchAll(
                $where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception(
                $e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna pessoas juridicas responsaveis do contrato
     * @param VwEntidadeContratoResponsavelTO $vwTO
     * @see MatriculaBO::retornarVwEntidadeContratoResponsavel();
     * @return Zend_Db_Table_Rowset_Abstract|false
     * @throws Zend_Exception
     */
    public function retornarVwEntidadeContratoResponsavel(VwEntidadeContratoResponsavelTO $vwTO)
    {
        $vwEntidadeContratoResponsavelORM = new VwEntidadeContratoResponsavelORM();
        $where = $vwEntidadeContratoResponsavelORM->montarWhereView($vwTO, false, true, false);
        try {
            $obj = $vwEntidadeContratoResponsavelORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception(
                $e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna disciplinas para aproveitamento
     * @param VwMatriculaDisciplina $vwmdTO
     * @param Mixed $where
     * @see MatriculaBO::retornarVwMatriculaDisciplian();
     * @return Zend_Db_Table_Rowset_Abstract|false
     * @throws Zend_Exception
     */
    public function retornarVwMatriculaDisciplina(
        VwMatriculaDisciplinaTO $vwmdTO, $where = null)
    {
        $vwMatriculaDisciplinaORM = new VwMatriculaDisciplinaORM();
        if (!$where) {
            $where = $vwMatriculaDisciplinaORM->montarWhere(
                $vwmdTO, false, true, false);
        }
        try {
            $obj = $vwMatriculaDisciplinaORM->fetchAll(
                $where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Exception(
                $e->getMessage());
            return false;
        }
    }

    /**
     * Retorna um produto do tipo projeto pedagógico de uma determinada entidade
     * @param int $idProjetoPedagogico
     * @param int $idEntidade
     * @return Zend_Db_Table_Row
     */
    public function retornarProdutoProjetoPedagogico($idProjetoPedagogico, $idEntidade)
    {
        $vwProdutoProjetoTipoValorORM = new VwProdutoProjetoTipoValorORM();
        $select = $vwProdutoProjetoTipoValorORM->select(true)
            ->where('id_projetopedagogico = ?', $idProjetoPedagogico)
            ->where('id_entidade = ?', $idEntidade);
        return $vwProdutoProjetoTipoValorORM->fetchRow($select);
    }

    /**
     * Retornar as regras de contratos existentes na tabela tb_contratoregra
     * Realiza um filtro na consulta de acordo com os atributos do TO
     * @param ContratoRegraTO $contratoRegraTO
     * @return Zend_Db_Table_Rowset
     */
    public function retornarContratoRegra(ContratoRegraTO $contratoRegraTO)
    {
        $contratoRegraORM = new ContratoRegraORM();
        $where = $contratoRegraORM->montarWhere($contratoRegraTO);
        return $contratoRegraORM->fetchAll($where);
    }

    /**
     * Metodo que retorna as disciplinas da matricula
     * @throws Zend_Exception
     * @see ProjetoPedagogicoBO::retornarModuloDisciplinaStatus();
     * @param MatriculaDisciplinaTO $mdTO
     * @param String $where
     * @return Zend_Db_Table_Row_Abstract | false
     */
    public function retornarMatriculaDisciplina(
        MatriculaDisciplinaTO $mdTO, $where = null)
    {
        try {
            $matriculaDisciplinaORM = new MatriculaDisciplinaORM();
            if (!$where) {
                $where = $matriculaDisciplinaORM->montarWhere(
                    $mdTO);
            }
            return $matriculaDisciplinaORM->fetchAll(
                $where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception(
                $e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que retorna as disciplinas que podem ser aproveitadas pelo aluno
     * @param VwAproveitamentoDisciplinaInternoTO $vwAdiTO
     * @param Mixed $where
     * @see MatriculaBO::retornarVwAproveitamentoDisciplinaInterno();
     * @return Zend_Db_Table_Row_Abstract | false
     */
    public function retornarVwAproveitamentoDisciplinaInterno(VwAproveitamentoDisciplinaInternoTO $vwAdiTO, $where = null)
    {
        try {
            $orm = new VwAproveitamentoDisciplinaInternoORM();
            if (!$where) {
                $where = $orm->montarWhereView($vwAdiTO, false, true);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que retorna as disciplinas já aproveitadas pelo aluno
     * @param VwAproveitamentoTO $vwaTO
     * @param Mixed $where
     * @see MatriculaBO::retornarVwAproveitamentoDisciplina();
     * @return Zend_Db_Table_Row_Abstract | false
     */
    public function retornarVwAproveitamento(VwAproveitamentoTO $vwaTO, $where = null)
    {
        try {
            $orm = new VwAproveitamentoORM();
            if (!$where) {
                $where = $orm->montarWhereView($vwaTO, false, true);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que retorna as avaliações para transferências.
     * @param VwAproveitarAvaliacaoTO $vwaTO
     * @param Mixed $where
     * @see MatriculaBO::retornarVwAproveitarAvaliacao();
     * @return Zend_Db_Table_Row_Abstract | false
     */
    public function retornarVwAproveitarAvaliacao(VwAproveitarAvaliacaoTO $vwaTO, $where = null)
    {
        try {
            $orm = new VwAproveitarAvaliacaoORM();
            if (!$where) {
                $where = $orm->montarWhereView($vwaTO, false, true);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Retorna os produtos de um contrato
     * @param VwProdutoContratoTO $produtoContratoTO
     * @return Zend_Db_Table_Rowset
     */
    public function retornarVwProdutoContrato(VwProdutoContratoTO $produtoContratoTO)
    {
        $vwProdutoContratoORM = new VwProdutoContratoORM();
        $select = $vwProdutoContratoORM->select(true)
            ->distinct(true)
            ->columns(array('id_contrato', 'id_produto', 'id_tipoproduto', 'st_produto', 'id_entidade', 'id_projetopedagogico', 'id_produtovalor', 'id_tipoprodutovalor'
            , 'nu_valor', 'nu_valormensal', 'nu_basepropor', 'id_entidadematriz', 'id_venda', 'st_produto', 'st_projetopedagogico'
            ));

        $where = $vwProdutoContratoORM->montarWhere($produtoContratoTO);

        if (!empty($where)) {
            $select->where($where);
        }
        return $vwProdutoContratoORM->fetchAll($select);
    }

    /**
     * Retorna os livros registros existentes da tabela tb_livroregistro
     *
     * @param LivroRegistroTO $livroRegistroTO
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function retornarLivroRegistro(LivroRegistroTO $livroRegistroTO)
    {
        $livroRegistroORM = new LivroRegistroORM();
        $where = $livroRegistroORM->montarWhere($livroRegistroTO);
        return $livroRegistroORM->fetchAll($where);
    }

    /**
     * Metodo que retorna as series e niveis do produto da venda
     * @param VendaProdutoNivelSerieTO $to
     * @param Mixed $where
     * @return Zend_Db_Table_Row_Abstract
     */
    public function retornarVendaProdutoNivelSerie(VendaProdutoNivelSerieTO $to, $where = null)
    {
        try {
            $orm = new VendaProdutoNivelSerieORM();
            if (!$where) {
                $where = $orm->montarWhere($to, false);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            THROW $e;
            return false;
        }
    }

    /**
     * Método que retorna a grade do aluno da integracao
     * @param VwAlunoGradeIntegracaoTO $to
     * @param null $where
     * @param bool $isPosGraduacao
     * @return Zend_Db_Table_Rowset_Abstract
     * @throws Zend_Exception
     */
    public function retornarVwAlunoGradeIntegracao(VwAlunoGradeIntegracaoTO $to, $where = null)
    {
        $orm = new VwAlunoGradeIntegracaoORM();

        if (!$where) {
            $where = $orm->montarWhereView($to, false, true);
        }
        return $orm->fetchAll($where, array('nu_ordenacao', 'dt_abertura', 'id_projetopedagogico', 'id_modulo', 'id_disciplina'));
    }

    /**
     * Método que retorna as disciplinas extras da matricula
     * @param VwDisciplinasExtrasTO $to
     * @param Mixed $where
     * @throws Zend_Db_Exception
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset
     */
    public function retornarVwDisciplinasExtras(VwDisciplinasExtrasTO $to, $where = null)
    {
        $orm = new VwDisciplinasExtrasORM();
        if (!$where) {
            $where = $orm->montarWhereView($to, false, true);
        }
        return $orm->fetchAll($where);
    }

    /**
     * Verifica se é AND ou OR
     * @param string $where primeira parte da pesquisa
     * @param string $pars
     * @return string
     */
    private function verificaAndOr($where, $pars)
    {
        if ($where != "") {
            if ($where == "(") {
                return $where = " " . $pars;
            } elseif (substr($where, -2) == ") ") {
                return $where = " AND " . $pars;
            } else {
                return $where = " OR " . $pars;
            }
        } else {
            return '';
        }
    }

    /**
     * Metodo que retorna módulo para notas de aluno por tipo (sem tcc / ou conversando)
     * @param VwGradeTipotccTO $vwGradeTipoTO
     * @see PortalMatriculaBO::retornarGradePorTipoTcc()
     * @return Zend_Db_Table_Rowset_Abstract | false
     */
    public function retornarVwGradeTipoModuloTcc(VwGradeTipotccTO $vwGradeTipoTO)
    {
        try {
            $orm = new VwGradeTipoTccORM();
            $where = $orm->montarWhereView($vwGradeTipoTO, false, true);
            $select = $orm->select()
                ->distinct(true)
                ->from('vw_gradetipotcc', '')
                ->setIntegrityCheck(false)
                ->columns(array('id_matricula', 'id_modulo', 'st_modulo'))
                ->where($where);
            return $orm->fetchAll($select);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Método que retorna view de grade e sala do aluno.
     * @param VwGradeTipoTccTO $vwGradeTipoTO
     * @param Mixed $where
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwGradeTipoTcc(VwGradeTipoTccTO $vwGradeTipoTO, $where = null)
    {
        try {
            $vwGradeTipoORM = new VwGradeTipoTccORM();
            if (!$where) {
                $where = $vwGradeTipoORM->montarWhere($vwGradeTipoTO);
            }
            $obj = $vwGradeTipoORM->fetchAll($where, 'nu_ordem');
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Retorna os responsaveis financeiros de uma matricula
     * @param int $id_matricula
     * @return array
     */
    public function retornarResponsavelFinanceiro($id_matricula)
    {
        try {
            $vwORM = new VwMatriculaLancamentoORM();
            $query = 'SELECT DISTINCT id_usuariolancamento, st_usuariolancamento FROM vw_matriculalancamento WHERE id_matricula = ' . $id_matricula;
            return $vwORM->getDefaultAdapter()->query($query)->fetchAll();
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
        }
    }

    /**
     * Retorna os responsaveis financeiros de uma matricula
     * @param int $id_matricula
     * @return array
     */
    public function retornarIdVendaMatricula($id_matricula)
    {
        try {
            $orm = new MatriculaORM();

            $query = 'SELECT ct.id_venda FROM dbo.tb_contratomatricula AS cm ' .
                ' JOIN dbo.tb_contrato AS ct ON cm.id_contrato = ct.id_contrato ' .
                ' WHERE cm.bl_ativo = 1 AND cm.id_matricula = ' . $id_matricula;

            return $orm->getDefaultAdapter()->query($query)->fetch();
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Retorna a ultima venda ativa de produto projeto pedagogico da matricula
     * @param $id_matricula
     * @return bool|mixed
     */
    public function retornarIdUltimaVendaGraduacao($id_matricula)
    {
        try {
            $orm = new MatriculaORM();

            $query = 'SELECT TOP 1 vp.id_venda
				FROM tb_vendaproduto AS vp
							JOIN tb_produtoprojetopedagogico AS ppd ON ppd.id_produto = vp.id_produto
							JOIN tb_venda AS v ON v.id_venda = vp.id_venda AND v.bl_ativo = 1 AND v.id_evolucao = 10
								WHERE vp.id_matricula = ' . $id_matricula . '
									ORDER BY v.id_venda DESC';

            return $orm->getDefaultAdapter()->query($query)->fetch();
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

}

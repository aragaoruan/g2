<?php

/**
 * Classe de acesso a dados referente a TransacaoFinanceiraDAO
 * @author Elcio Mauo Guimarães - <elcioguimaraes@gmail.com>
 * @since
 * @package models
 * @subpackage dao
 */
class TransacaoFinanceiraDAO extends Ead1_DAO {

    /**
     * Método que insere o TransacaoFinanceira
     *
     * @param TransacaoFinanceiraTO $to
     * @throws Zend_Exception
     * @return Ambigous <mixed, multitype:>
     * @see TransacaoFinanceiraBO::salvarTransacaoFinanceira();
     */
    public function cadastrarTransacaoFinanceira(TransacaoFinanceiraTO $to){
        $this->beginTransaction();
        try{
            $orm = new TransacaoFinanceiraORM();
            $id_transacaofinanceira = $orm->insert($to->toArrayInsert());
            $this->commit();
            return $id_transacaofinanceira;
        }catch(Zend_Exception $e){
            $this->rollBack();
            throw $e;
            return false;
        }

    }

    /**
     * Método de edição do TransacaoFinanceira
     * @param TransacaoFinanceiraTO $to
     * @throws Zend_Exception
     * @see TransacaoFinanceiraBO::salvarTransacaoFinanceira();
     */
    public function editarTransacaoFinanceira(TransacaoFinanceiraTO $to){
        $this->beginTransaction();
        try {
            $orm = new TransacaoFinanceiraORM();
            $orm->update($to->toArrayUpdate(true, array('id_transacaofinanceira')), $orm->montarWhere($to, true));
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            throw $e;
            return false;
        }
    }

    /**
     * Método de listagem de TransacaoFinanceira
     * @param TransacaoFinanceiraTO $to
     * @return Ambigous <boolean, multitype:>
     * @see TransacaoFinanceiraBO::listarTransacaoFinanceira();
     */
    public function listarTransacaoFinanceira(TransacaoFinanceiraTO $to){
        $orm = new TransacaoFinanceiraORM();
        return $orm->consulta($to);
    }


    /**
     * Retorna transações incompletas
     * @throws Zend_Exception
     * @return Ambigous <boolean, multitype:>|boolean
     */
    public function listarTransacaoFinanceiraPendente(){

        $array = false;
        try {

            $orm = new TransacaoFinanceiraORM();
            $select = $orm->select()->from(array('tf'=>$orm->_name))
                ->where('(nu_status is null or nu_status >= 2)')
                ->where('(nu_verificacao < 15 or nu_verificacao is null) ')
                ->where('id_cartaobandeira is not null')
// 							->where('id_venda = 84122')
                ->where('id_venda not in (select id_venda
				from tb_transacaofinanceira as tfv
				where nu_status = 1 and id_cartaobandeira is not null
				and tfv.id_venda = tf.id_venda
				)');
            $select->order('dt_cadastro desc');

            $select->setIntegrityCheck(false);

            $resultado = $orm->fetchAll($select)->toArray();

            if($resultado){
                $array = Ead1_TO_Dinamico::encapsularTo($resultado, new TransacaoFinanceiraTO());
            }

            return $array;

        }catch(Exception $e){
            throw $e;
            return false;
        }

    }




    /**
     * Método que insere o TransacaoLancamento
     *
     * @param TransacaoLancamentoTO $to
     * @throws Zend_Exception
     * @return Ambigous <mixed, multitype:>
     * @see TransacaoLancamentoBO::salvarTransacaoLancamento();
     */
    public function cadastrarTransacaoLancamento(TransacaoLancamentoTO $to){
        $this->beginTransaction();
        try{
            $orm = new TransacaoLancamentoORM();
            $id_transacaolancamento = $orm->insert($to->toArrayInsert());
            $this->commit();
            return $id_transacaolancamento;
        }catch(Zend_Exception $e){
            $this->rollBack();
            throw $e;
            return false;
        }
    }


    /**
     * Método de listagem de TransacaoLancamento
     * @param TransacaoLancamentoTO $to
     * @return Ambigous <boolean, multitype:>
     * @see TransacaoLancamentoBO::listarTransacaoLancamento();
     */
    public function listarTransacaoLancamento(TransacaoLancamentoTO $to){
        $orm = new TransacaoLancamentoORM();
        return $orm->consulta($to);
    }


}

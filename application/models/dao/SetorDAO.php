<?php
/**
 * Classe de persistência de Setores no banco de dados
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 * 
 * @package models
 * @subpackage dao
 */
class SetorDAO extends Ead1_DAO {
	
	/**
	* Metodo que cadastra Setor
	* @param SetorTO $to
	* @return Ambigous <mixed, multitype:>|boolean
	*/
	public function cadastrarSetor(SetorTO $to) {
		$orm = new SetorORM();
		// unset($dTO->arrDados);
		$insert = $to->toArrayInsert();
		$this->beginTransaction();
		try {
			$id_setor = $orm->insert($insert);
			$this->commit ();
			return $id_setor;
		} catch ( Exception $e ) {
			$this->rollBack ();
			$arr ['TO'] = $to;
			$arr ['WHERE'] = $insert;
			$arr ['SQL'] = $e->getMessage ();
			$this->excecao = $arr;
			return false;
		}
	}
	
	/**
	 * Meotodo que Edita Setor
	 * @param SetorTO $to
	 * @return boolean
	 */
	public function editarSetor(SetorTO $to) {
		$orm = new SetorORM();
		unset($to->arrDados);
		$update = $to->toArrayUpdate(false, array('id_setor'));
		$this->beginTransaction();
		try {
			$orm->update($update, " id_setor = " . $to->getId_setor());
			$this->commit();
			return true;
		} catch(Exception $e) {
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Metodo que desvincula a Setor
	 * @param SetorTO $to
	 * @return boolean
	 */
	public function desvincularSetor(SetorTO $to) {
		$orm = new SetorORM();
		unset($to->arrDados);
		$to->setBl_ativo(false);
		$update = $to->toArrayUpdate(false, array('id_setor'));
		$this->beginTransaction();
		try {
			$orm->update($update, " id_setor = " . $to->getId_setor());
			$this->commit();
			return true;
		} catch(Exception $e) {
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Metodo que retorna Setor
	 * @param SetorTO $to
	 * @param unknown_type $where
	 * @throws Zend_Exception
	 * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
	 */
	public function retornarSetor(SetorTO $to, $where = null) {
		$orm = new SetorORM();
		if (! $where) {
			$where = $orm->montarWhere ( $to );
			//$where .= " AND bl_ativo = 1 "; 
		}
		try {
			$obj = $orm->fetchAll($where);
			return $obj;
		} catch (Zend_Exception $e) {
			$msg ['WHERE'] = $where;
			$msg ['SQL'] = $e->getMessage ();
			$msg ['TO'] = $to;
			$this->excecao = $msg;
			THROW new Zend_Exception ( $e->getMessage () );
			return false;
		}
	}
	
	/**
	 * Metodo que retorna VwSetorCargoFuncaoUsuarioTO
	 * @param VwSetorCargoFuncaoUsuarioTO $to
	 * @param unknown_type $where
	 * @throws Zend_Exception
	 * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
	 */
	public function retornarVwSetorCargoFuncaoUsuario(VwSetorCargoFuncaoUsuarioTO $to, $where = null) {
		$orm = new VwSetorCargoFuncaoUsuarioORM();
		if (! $where) {
			$where = $orm->montarWhere($to);
			//$where .= " AND bl_ativo = 1 "; 
		}
		try {
			$obj = $orm->fetchAll($where);
			return $obj;
		} catch (Zend_Exception $e) {
			$msg ['WHERE'] = $where;
			$msg ['SQL'] = $e->getMessage ();
			$msg ['TO'] = $to;
			$this->excecao = $msg;
			THROW new Zend_Exception ( $e->getMessage () );
			return false;
		}
	}
	
	/**
	 * Metodo que Cadastra um vinculo entre Setor e Funcionario
	 * @param SetorFuncionarioTO $to
	 * @return Ambigous <mixed, multitype:>|boolean
	 */
	public function cadastrarSetorFuncionario(SetorFuncionarioTO $to) {
		$orm = new SetorFuncionarioORM();
		$insert = $to->toArrayInsert();
		$this->beginTransaction();
		try {
			$id_setorfuncionario = $orm->insert($insert);
			$this->commit();
			return $id_setorfuncionario;
		} catch (Exception $e) {
			$this->rollBack ();
			$arr ['TO'] = $to;
			$arr ['WHERE'] = $insert;
			$arr ['SQL'] = $e->getMessage();
			$this->excecao = $arr;
			return false;
		}
	}
	
	/**
	 * Metodo que Edita um vinculo entre Setor e Funcionario
	 * @param SetorFuncionarioTO $to
	 * @return boolean
	 */
	public function editarSetorFuncionario(SetorFuncionarioTO $to) {
		$orm = new SetorFuncionarioORM();
		unset($to->arrDados);
		$update = $to->toArrayUpdate(false, array('id_setorfuncionario'));
		$this->beginTransaction();
		try {
			$orm->update($update, " id_setorfuncionario = " . $to->getId_setorfuncionario());
			$this->commit();
			return true;
		} catch(Exception $e) {
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Metodo que desvincula a Setor
	 * @param SetorTO $to
	 * @return boolean
	 */
	public function desvincularSetorFuncionario(SetorFuncionarioTO $to) {
		$this->beginTransaction();
		try{
			$orm = new SetorFuncionarioORM();
			$orm->delete(" id_setorfuncionario  = {$to->getId_setorfuncionario()}");
			$this->commit();
		}catch(Zend_Exception $e){
			$this->rollBack();
			throw $e;
		}
	}
	
	/**
	 * Metodo que retorna os vinculos de Setor e Funcionario
	 * @param SetorFuncionarioTO $to
	 * @param unknown_type $where
	 * @throws Zend_Exception
	 * @return unknown|boolean
	 */
	public function retornarSetorFuncionario(SetorFuncionarioTO $to, $where = null) {
		$orm = new SetorFuncionarioORM();
		if (! $where) {
			$where = $orm->montarWhere ( $to );
			//$where .= " AND bl_ativo = 1 ";
		}
		try {
			$obj = $orm->fetchAll($where);
			return $obj;
		} catch (Zend_Exception $e) {
			$msg ['WHERE'] = $where;
			$msg ['SQL'] = $e->getMessage ();
			$msg ['TO'] = $to;
			$this->excecao = $msg;
			THROW new Zend_Exception ( $e->getMessage () );
			return false;
		}
	}
	
	/**
	 * Metodo que retorna VwSetorFuncionario
	 * @param VwSetorFuncionarioTO $to
	 * @param unknown_type $where
	 * @throws Zend_Exception
	 * @return unknown|boolean
	 */
	public function retornarVwSetorFuncionario(VwSetorFuncionarioTO $to, $where = null) {
		$orm = new VwSetorFuncionarioORM();
		if (! $where) {
			$where = $orm->montarWhere($to);
			//$where .= " AND bl_ativo = 1 ";
		}
		try {
			$obj = $orm->fetchAll($where);
			return $obj;
		} catch (Zend_Exception $e) {
			$msg ['WHERE'] = $where;
			$msg ['SQL'] = $e->getMessage ();
			$msg ['TO'] = $to;
			$this->excecao = $msg;
			THROW new Zend_Exception ( $e->getMessage () );
			return false;
		}
	}
}
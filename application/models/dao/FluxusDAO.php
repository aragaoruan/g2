<?php

/**
 * Classe pra integrações com sistema Fluxus através de  LinkedServer
 * @author edermariano
 *
 */
class FluxusDAO extends Ead1_DAO {

    /**
     * Método que busca os dados da integração
     * @param VendaTO $vendaTO
     * @return Ead1_Mensageiro
     */
    public function dadosLancamentosIntegracao(VendaTO $vendaTO) {
        $vwIntegraGestorFluxusTO = new Totvs_VwIntegraGestorFluxusTO();
        $vwIntegraGestorFluxusTO->setId_venda($vendaTO->getId_venda());
        //Zend_Debug::dump($vendaTO, __FILE__ . "(" . __LINE__ . ")<br/>");exit;
        //$vwIntegraGestorFluxusTO->setId_venda(73504);
        //$vwIntegraGestorFluxusTO->setId_entidade($vendaTO->getId_entidade());

        $vwIntegraGestorFluxus = new Totvs_VwIntegraGestorFluxusORM();
        $retorno = $vwIntegraGestorFluxus->consulta($vwIntegraGestorFluxusTO);
        if (!empty($retorno)) {
            return new Ead1_Mensageiro($retorno, Ead1_IMensageiro::SUCESSO);
        } else {
            return new Ead1_Mensageiro($retorno, Ead1_IMensageiro::AVISO);
        }
    }

    /**
     * Método que executa a sp de integração
     * @param Totvs_VwIntegraGestorFluxusTO $vwIntegraGestorFluxusTO
     * @param String $ip Número IP do Servidor Vinculado
     * @param String $db DataBase que será conectado
     * @return Ead1_Mensageiro
     */
    public function spIntegracao(Totvs_VwIntegraGestorFluxusTO $vwIntegraGestorFluxusTO, $ip = null, $db = null) {
        try {
            $dataAtual = date("H:i:s \d\e d/m/Y");

            $ip = $ip ? $ip : $vwIntegraGestorFluxusTO->getSt_linkedserver();
            $db = $db ? $db : $vwIntegraGestorFluxusTO->getSt_caminho();

            $sql = "EXEC {$ip}.{$db}.dbo.Integra_GestorFluxus
					'{$vwIntegraGestorFluxusTO->getCODCOLIGADA_FCFO()}', 	/* código da coligada*/
					'{$vwIntegraGestorFluxusTO->getCODCFO_FCFO()}',  		/* código do cliente/fornecedor*/
					'{$vwIntegraGestorFluxusTO->getNOMEFANTASIA_FCFO()}', 	/* nome fantasia do cliente/fornecedor*/
					'{$vwIntegraGestorFluxusTO->getNOME_FCFO()}', 			/* nome/Razão Social do cliente/fornecedor*/
					'{$vwIntegraGestorFluxusTO->getCGCCFO_FCFO()}',			/* CNPJ/CPF do cliente/fornecedor*/
					'{$vwIntegraGestorFluxusTO->getINSCRESTADUAL_FCFO()}', 	/* Inscrição estadual do cliente/fornecedor*/
					'{$vwIntegraGestorFluxusTO->getPAGREC_FCFO()}',			/* Flag que diz se o lançamento é de cliente(receber) ou de fornecedor(pagar)*/
					'{$vwIntegraGestorFluxusTO->getRUA_FCFO()}',			/* Endereço*/
					'{$vwIntegraGestorFluxusTO->getNUMERO_FCFO()}',			/* Número*/
					'{$vwIntegraGestorFluxusTO->getCOMPLEMENTO_FCFO()}',	/* Complemento de Endereço*/
					'{$vwIntegraGestorFluxusTO->getBAIRRO_FCFO()}',			/* Bairro do endereço*/
					'{$vwIntegraGestorFluxusTO->getCIDADE_FCFO()}',			/* Cidade do Endereço*/
					'{$vwIntegraGestorFluxusTO->getCODETD_FCFO()}',			/* Unidade Federativa*/
					'{$vwIntegraGestorFluxusTO->getCEP_FCFO()}',			/* CEP*/
					'{$vwIntegraGestorFluxusTO->getTELEFONE_FCFO()}',		/* Telefone*/
					'{$vwIntegraGestorFluxusTO->getFAX_FCFO()}',			/* Fax*/
					'{$vwIntegraGestorFluxusTO->getEMAIL_FCFO()}',			/* E-mail*/
					'{$vwIntegraGestorFluxusTO->getCONTATO_FCFO()}',		/* Contato*/
					'{$vwIntegraGestorFluxusTO->getATIVO_FCFO()}',			/* Situação do cadastro*/			
					'{$vwIntegraGestorFluxusTO->getCODMUNICIPIO_FCFO()}',	/* Código do município*/
					'{$vwIntegraGestorFluxusTO->getPESSOAFISOUJUR_FCFO()}',	/* Flag que determina se o lançamento é de uma Pessoa Física ou Jurídica*/
					'{$vwIntegraGestorFluxusTO->getPAIS_FCFO()}',			/* País do endereço*/
					'{$vwIntegraGestorFluxusTO->getIDCFO_FCFO()}',			/* PK do Cliente Fornecedor*/
					'{$vwIntegraGestorFluxusTO->getCEI_FCFO()}',			/* Número do Contribuinte CEI*/
					{$vwIntegraGestorFluxusTO->getNACIONALIDADE_FCFO()},	/* Nacionalidade*/
					'{$vwIntegraGestorFluxusTO->getTIPOCONTRIBUINTEINSS_FCFO()}', /* INSS*/
					'{$vwIntegraGestorFluxusTO->getIDLAN()}', 				/* IDLAN*/
					'{$vwIntegraGestorFluxusTO->getNUMERODOCUMENTO()}', 	/* Número do Documento*/
					{$vwIntegraGestorFluxusTO->getPAGREC()},				/* Flag de contas a pagar ou a receber 1 = receber 2 = pagar*/
					{$vwIntegraGestorFluxusTO->getSTATUSLAN()},				/* Status do Lançamento 0 = em aberto 1 = baixado*/
					{$vwIntegraGestorFluxusTO->getDATAVENCIMENTO()} ,       /* Data de vencimento*/
					{$vwIntegraGestorFluxusTO->getDATAEMISSAO()},	        /* Data de emissão*/
					{$vwIntegraGestorFluxusTO->getDATAPREVBAIXA()},         /* Data previa para baixa*/
					{$vwIntegraGestorFluxusTO->getDATABAIXA()},	            /* Data da baixa*/
					'{$vwIntegraGestorFluxusTO->getVALORORIGINAL()}',		/* Valor do lançamento*/
					{$vwIntegraGestorFluxusTO->getVALORBAIXADO()},			/* Valor baixado*/
					{$vwIntegraGestorFluxusTO->getVALORCAP()},				/* Valor de capitalização*/
					{$vwIntegraGestorFluxusTO->getVALORJUROS()},			/* Valor dos juros*/
					{$vwIntegraGestorFluxusTO->getVALORDESCONTO()},			/* valor de desconto*/
					{$vwIntegraGestorFluxusTO->getVALOROP1()},				/* Valor de bolsa*/
					{$vwIntegraGestorFluxusTO->getVALOROP2()},				/* Valor INSSPJ */
					{$vwIntegraGestorFluxusTO->getVALOROP3()},				/* Valor ISS*/
					{$vwIntegraGestorFluxusTO->getVALORMULTA()},			/* Valor da multa*/
					{$vwIntegraGestorFluxusTO->getJUROSDIA()},				/* Valor do Juros ao dia*/
					{$vwIntegraGestorFluxusTO->getCAPMENSAL()},			    /* Cap Mensal*/
					{$vwIntegraGestorFluxusTO->getTAXASVENDOR()},			/* Taxas Vendor*/
					{$vwIntegraGestorFluxusTO->getJUROSVENDOR()},			/* Juros Vendor*/
					'{$vwIntegraGestorFluxusTO->getCODCXA()}',				/* Código da Conta caixa*/
					'{$vwIntegraGestorFluxusTO->getCODTDO()}',				/* Código do Tipo de documento 031 matrícula 040 parcelas*/
					'{$vwIntegraGestorFluxusTO->getCODTB1FLX()}',			/* Categoria*/
					'{$vwIntegraGestorFluxusTO->getCODTB2FLX()}',			/* Forma de Pagamento*/
					{$vwIntegraGestorFluxusTO->getDATAOP2()},               /* Data da Matrícula*/
					'{$vwIntegraGestorFluxusTO->getCAMPOALFAOP1()}',		/* Código do Contrato*/
					'{$vwIntegraGestorFluxusTO->getCAMPOALFAOP2()}',		/* Código da Matrícula*/
					'{$vwIntegraGestorFluxusTO->getHISTORICO()}$dataAtual',	/* Histórico do Lançamento*/
					'{$vwIntegraGestorFluxusTO->getCODCCUSTO()}',			/* Código do Centro de Custo*/
					{$vwIntegraGestorFluxusTO->getNUMEROCHEQUE()},
					{$vwIntegraGestorFluxusTO->getDATAVENCIMENTO_CHEQUE()},
					{$vwIntegraGestorFluxusTO->getCODIGOBANCO()},
					{$vwIntegraGestorFluxusTO->getCODIGOAGENCIA()},
					{$vwIntegraGestorFluxusTO->getCONTACORRENTE()},
					{$vwIntegraGestorFluxusTO->getCOMPENSADO()},
					NULL";

            $statement = Zend_Db_Table::getDefaultAdapter()->query($sql);
            $dadosRetorno = $statement->fetchAll();
            if ($this->retornoIntegracao($vwIntegraGestorFluxusTO, $sql, $dadosRetorno)) {
                $mensagem = "Lançamento ({$vwIntegraGestorFluxusTO->getId_lancamento()}) Integrado com Sucesso!";
                $tipo = Ead1_IMensageiro::SUCESSO;
                $codigo = $sql;
            } else {
                $mensagem = "Erro ao integrar o lançamento ({$vwIntegraGestorFluxusTO->getId_lancamento()}) do contrato {$vwIntegraGestorFluxusTO->getId_contrato()}!";
                $tipo = Ead1_IMensageiro::ERRO;
                $codigo = $sql;
                $this->gravarErroIntegracao($mensagem, $tipo, $codigo);
            }
            return $dadosRetorno;
        } catch (Exception $e) {
            $textoErro = "[FluxusDAO 110] " . $e->getMessage();
            $this->gravarErroIntegracao($textoErro, 'FluxusDAO::spIntegracao()', $sql);
            return new Ead1_Mensageiro($textoErro, Ead1_IMensageiro::ERRO, $sql);
        }
    }

    /**
     * Método que executa a sp de atualização da integração
     * @param Totvs_VwAtualizarFluxusGestorTO $vwAtualizarGestorFluxusTO
     * @param String $ip Número IP do Servidor Vinculado
     * @param String $db DataBase que será conectado
     * @return Ead1_Mensageiro
     * @author Rafael Costa Rocha rafael.rocha.mg@gmail.com
     */
    public function spAtualizaFluxus(Totvs_VwAtualizarFluxusGestorTO $vwAtualizarGestorFluxusTO, $ip, $db) {
        try {
            $this->beginTransaction();

            //$dataAtual = date ("H:i:s \d\e d/m/Y");

            $db = $db ? "." . $db : "";

            //$sql = "EXEC {$ip}{$db}.dbo.Atualiza_Fluxus
            $sql = "EXEC {$ip}{$db}.dbo.Atualiza_Fluxus   
					{$vwAtualizarGestorFluxusTO->getIDLAN()},                             /*st_codlancamento(IDLAN)*/
					{$vwAtualizarGestorFluxusTO->getId_venda()},                          /*id_venda(CAMPOALFAOP1)*/
					'{$vwAtualizarGestorFluxusTO->getCODCOLIGADA()}',                     /*st_codsistema da tb_entidadeintegracao(CODCOLIGADA)*/
					{$vwAtualizarGestorFluxusTO->getSTATUSLAN()},                         /*STATUS LANCAMENTO (1=BAIXADO)(STATUSLAN)*/
					'" . ($vwAtualizarGestorFluxusTO->getDATA_BAIXA() instanceof DateTime ? $vwAtualizarGestorFluxusTO->getDATA_BAIXA()->format('Y-m-d') : $vwAtualizarGestorFluxusTO->getDATA_BAIXA()) . "', /*dt_quitado(DATABAIXA)*/
					{$vwAtualizarGestorFluxusTO->getVALOR_BAIXADO()},                     /*nu_quitado(VALORBAIXADO)*/
					'Lancamento atualizado pelo G2',                                      /*HISTORICO*/
					'{$vwAtualizarGestorFluxusTO->getId_lancamento()}',                   /*NUMERO DOCUMENTO*/
					" . ($vwAtualizarGestorFluxusTO->getCONTA_CAIXA() ? $vwAtualizarGestorFluxusTO->getCONTA_CAIXA() : 'NULL' ) . ", /*CONTA CAIXA*/
					NULL,                                                                   /*CATEGORIA*/
					NULL,                                                                   /*FORMA DE PAGAMENTO*/
					NULL,                                                                   /*CENTRO DE CUSTO*/
					NULL;                                                                   /*ID EXTRATO*/";
            //Zend_Debug::dump($sql,__CLASS__.'('.__LINE__.')'); exit;
            $statement = Zend_Db_Table::getDefaultAdapter()->query($sql);
            $dadosRetorno = $statement->fetchAll();
            $this->commit();
            //TODO validar retorno da sp aqui
            if ($dadosRetorno) {
                return new Ead1_Mensageiro('Atualizado com sucesso!', Ead1_IMensageiro::SUCESSO, $dadosRetorno);
            } else {
                return new Ead1_Mensageiro('Erro ao atualizar lançamento do fluxus!', Ead1_IMensageiro::ERRO);
            }
        } catch (Zend_Db_Adapter_Exception $ae) {

            $this->rollBack();
            return new Ead1_Mensageiro($ae->getMessage(), Ead1_IMensageiro::ERRO);
        } catch (Exception $e) {

            $this->rollBack();
            $textoErro = "[FluxusDAO { " . __LINE__ . " } ] " . $e->getMessage();
            $this->gravarErroIntegracao($textoErro, 'FluxusDAO::spAtualizaFluxus()', $sql);
            return new Ead1_Mensageiro($textoErro, Ead1_IMensageiro::ERRO, $sql);
        } catch (Zend_Db_Table_Exception $dbe) {
            $this->rollBack();
            $textoErro = "[FluxusDAO { " . __LINE__ . " } ] " . $dbe->getMessage();
            $this->gravarErroIntegracao($textoErro, 'FluxusDAO::spAtualizaFluxus()', $sql);
            return new Ead1_Mensageiro("Erro ao atualizar lançamento do Fluxus! - {$dbe->getMessage()}", Ead1_IMensageiro::ERRO, $dbe->getMessage());
        }
    }

    /**
     * Método que trata os dados de retorno da integração
     * @param Totvs_VwIntegraGestorFluxusTO $vwIntegraGestorFluxus
     * @param Zend_Db_Table_Rowset $dadosIntegracao
     */
    private function retornoIntegracao(Totvs_VwIntegraGestorFluxusTO $vwIntegraGestorFluxus, $sql, array $dadosIntegracao = null) {
        $linhaDado = $dadosIntegracao[0];

        if (isset($linhaDado['IDLANCAMENTO']) && isset($linhaDado['CODCFO'])) {
            return ($this->gravarReferenciaLancamento($vwIntegraGestorFluxus, $linhaDado['IDLANCAMENTO'], $linhaDado['CODCFO'])->getTipo() == Ead1_IMensageiro::SUCESSO ? true : false);
        } elseif (isset($linhaDado['ERRO_INTEGRACAO'])) {
            $this->gravarErroIntegracao($linhaDado['ERRO_INTEGRACAO'], 'FluxusDAO::spIntegracao()', $sql);
            return false;
        } else {
            $this->gravarErroIntegracao();
            return false;
        }
    }

    /**
     * Método que grava a referência dos lançamentos integrados
     * 
     * @param Totvs_VwIntegraGestorFluxusTO $vwIntegraGestorFluxus Dados do lançamento
     * @param String $idLancamento Identificador do lançamento no Fluxus
     * @param String $codCfo Responsável financeiro ou cliente/fornecedor no Fluxus
     * @return Ead1_Mensageiro
     */
    private function gravarReferenciaLancamento(Totvs_VwIntegraGestorFluxusTO $vwIntegraGestorFluxus, $idLancamento, $codCfo) {
        $lancamentoIntegracaoTO = new LancamentoIntegracaoTO();
        $lancamentoIntegracaoTO->setId_usuariocadastro($lancamentoIntegracaoTO->getSessao()->id_usuario);
        $lancamentoIntegracaoTO->setId_entidade($lancamentoIntegracaoTO->getSessao()->id_entidade);
        $lancamentoIntegracaoTO->setId_sistema(SistemaTO::FLUXUS);
        $lancamentoIntegracaoTO->setId_lancamento($vwIntegraGestorFluxus->getId_lancamento());
        $lancamentoIntegracaoTO->setSt_codlancamento($idLancamento);
        $lancamentoIntegracaoTO->setSt_codresponsavel($codCfo);

        try {

            $lancamentoIntegracaoORM = new LancamentoIntegracaoORM();
            $idLancamentoIntegracao = $lancamentoIntegracaoORM->insert($lancamentoIntegracaoTO->toArrayInsert(), false);

            return new Ead1_Mensageiro("Vínculo registrado com sucesso!", Ead1_IMensageiro::SUCESSO, $idLancamentoIntegracao);
        } catch (Zend_Exception $e) {
            $this->excecao = $e;
            return new Ead1_Mensageiro("Erro ao vincular o lançamento do Fluxus com o do Gestor: ERRO: " . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Metodo que cadastrar os lancamentos como integrados
     * @param LancamentoIntegracaoTO $lancamentoIntegracaoTO
     * @throws Zend_Exception
     * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
     */
    public function cadastrarLancamentoIntegracao(LancamentoIntegracaoTO $lancamentoIntegracaoTO) {
        $this->beginTransaction();
        $lancamentoIntegracaoORM = new LancamentoIntegracaoORM();
        try {
            $insert = $lancamentoIntegracaoTO->toArrayInsert();
            $id_insert = $lancamentoIntegracaoORM->insert($insert);
            $this->commit();
            return $id_insert;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que edita um LacamentoIntegracaoTO
     * @param LancamentoIntegracaoTO $to
     * @param Mixed $where
     * @return Ead1_Mensageiro $mensageiro
     * @author Rafael Rocha rafael.rocha.mg@gmail.com
     */
    public function editarLancamentoIntegracao(LancamentoIntegracaoTO $to, $where = null, $aceitaNull = false) {
        $mensageiro = new Ead1_Mensageiro();
        try {
            $this->beginTransaction();
            $orm = new LancamentoIntegracaoORM();
            if (!$where) {
                $where = $orm->montarWhere($to, true);
                if (!$where) {
                    THROW new Zend_Exception("O WHERE Não pode vir vazio!");
                }
            }
            $dados = $to->toArrayUpdate($aceitaNull, array('id_lancamentointegracao'));
            $orm->update($dados, $where);
            $this->commit();
            return $mensageiro->setMensageiro("LancamentoIntegracaoTO atualizado com sucesso!", Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->rollBack();
            return $mensageiro->setMensageiro("Erro ao atualizar LancamentoIntegracaoTO!", Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Método que grava a referência do erro na SP da integração
     * @param String $textoErro
     * @param String $stOrigem Classe::metodo() que ocorreu o erro
     * @param String $codigo query ou execução que deu erro
     * @return Ead1_Mensageiro
     */
    private function gravarErroIntegracao($textoErro = 'Texto não informado.', $stOrigem = 'Origem não informada.', $codigo = null) {
        $erroTO = new ErroTO();
        $erroTO->setId_sistema(SistemaTO::FLUXUS);
        $erroTO->setId_entidade($erroTO->getSessao()->id_entidade);
        $erroTO->setId_usuario($erroTO->getSessao()->id_usuario);
        $erroTO->setSt_mensagem($textoErro);
        $erroTO->setSt_codigo($codigo);
        $erroTO->setSt_origem($stOrigem);

        try {
            $erroORM = new ErroORM();
            $erroTO->setId_erro($erroORM->insert($erroTO->toArrayInsert(), false));
            return new Ead1_Mensageiro($erroTO);
        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Método que executa a sp de atualização da flag  de lançamento
     * @param Totvs_VwAtualizarFluxusGestorTO $vwAtualizarGestorFluxusTO
     * @param String $ip Número IP do Servidor Vinculado
     * @param String $db DataBase que será conectado
     * @return Ead1_Mensageiro
     * @author Gabriel Resende gabriel.resende@unyleya.com.br
     */
    public function spAtualizaFlagFluxus(Totvs_VwAtualizarFluxusGestorTO $vwAtualizarGestorFluxusTO, $ip, $db) {
        try {
            /* Retirado porque retornava o seguinte erro:
              FluxusDAO { 348 } ] [Microsoft][SQL Server Native Client 10.0][SQL Server]A operação não pode ser realizada porque o provedor do OLE DB "SQLNCLI10"
              para o servidor vinculado "189.114.56.180" não pode iniciar uma transação distribuída.:
              EXEC [189.114.56.180].CorporeRM.dbo.sp_atualiza_flag_lancamento 261823, /st_codlancamento(IDLAN)/ '73393G2U' /id_venda(CAMPOALFAOP1)/
             */
            //$this->beginTransaction();

            $sql = "EXEC {$ip}.{$db}.dbo.sp_atualiza_flag_lancamento
					{$vwAtualizarGestorFluxusTO->getIDLAN()},             /*st_codlancamento(IDLAN)*/
					'{$vwAtualizarGestorFluxusTO->getCONTRATO()}'         /*CONTRATO*/";

            $statement = Zend_Db_Table::getDefaultAdapter()->query($sql);
            $dadosRetorno = $statement->fetchAll();
            // $this->commit();
            //TODO validar retorno da sp aqui
            if ($dadosRetorno) {
                return new Ead1_Mensageiro('Atualizado com sucesso!', Ead1_IMensageiro::SUCESSO, $dadosRetorno);
            } else {
                return new Ead1_Mensageiro('Erro ao atualizar a flag no fluxus!', Ead1_IMensageiro::ERRO);
            }
        } catch (Zend_Db_Adapter_Exception $ae) {

            //$this->rollBack();
            return new Ead1_Mensageiro($ae->getMessage(), Ead1_IMensageiro::ERRO);
        } catch (Exception $e) {

            //$this->rollBack();
            $textoErro = "[FluxusDAO { " . __LINE__ . " } ] " . $e->getMessage();
            $this->gravarErroIntegracao($textoErro, 'FluxusDAO::spAtualizaFlagFluxus()', $sql);
            return new Ead1_Mensageiro($textoErro, Ead1_IMensageiro::ERRO, $sql);
        } catch (Zend_Db_Table_Exception $dbe) {
            // $this->rollBack();
            $textoErro = "[FluxusDAO { " . __LINE__ . " } ] " . $dbe->getMessage();
            $this->gravarErroIntegracao($textoErro, 'FluxusDAO::spAtualizaFlagFluxus()', $sql);
            return new Ead1_Mensageiro("Erro ao atualizar a flag no Fluxus! - {$dbe->getMessage()}", Ead1_IMensageiro::ERRO, $dbe->getMessage());
        }
    }

    /**
     * Método que cadastra um registro da tb_acordorel na sincronização do fluxus
     * @param AcordoRelTO $to
     * @return \Ead1_Mensageiro
     */
    public function cadastrarAcordoRel(AcordoRelTO $to) {
        $this->beginTransaction();
        $acordoRelORM = new AcordoRelORM();
        try {
            $insert = $to->toArrayInsert();
            $id_insert = $acordoRelORM->insert($insert);
            $this->commit();
            return new Ead1_Mensageiro($to, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->rollBack();
            return new Ead1_Mensageiro($e, Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    public function retornarVwSincronizaFluxusG2(Totvs_VwSincronizaFluxusG2TO $to) {
        $orm = new Totvs_VwSincronizaFluxusG2ORM();
        $select = $orm->select()
                ->limit(50)
                ->from(array('vw' => 'vw_sincroniza_fluxus_g2'), '*', 'totvs')
                ->where($orm->montarWhere($to));

        $sql = $select->assemble();

        $statement = Zend_Db_Table::getDefaultAdapter()->query($sql);
        $dados = $statement->fetchAll();
        return Ead1_TO_Dinamico::encapsularTo($dados, new Totvs_VwSincronizaFluxusG2TO());
    }

}
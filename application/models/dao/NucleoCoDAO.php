<?php

/**
 * Classe de acesso a dados referentes ao NucleoCo
 * @package models
 * @subpackage dao
 */
class NucleoCoDAO extends Ead1_DAO {

	/**
	 * Método que insere o Núcleo
	 * 
	 * 
	 * @param NucleoCoTO $NucleoCoTo
	 * @throws Zend_Exception
	 * @return Ambigous <mixed, multitype:>
	 * @see NucleoCoBO::salvarNucleoCo();
	 */
	public function cadastrarNucleoCo(NucleoCoTO $NucleoCoTo){
		$this->beginTransaction();
		try{
			$nucleo = new NucleoCoORM();
			$id_nucleoco = $nucleo->insert($NucleoCoTo->toArrayInsert());
			$this->commit();
			return $id_nucleoco;
		}catch(Zend_Exception $e){
			$this->rollBack();
			throw $e;
		}	
	}
	
	
	/**
	 * Método de Edição do Núcleo
	 * @param NucleoCoTO $NucleoCoTo
	 * @throws Zend_Exception
	 * @see NucleoCoBO::salvarNucleoCo();
	 */
	public function editarNucleoCo(NucleoCoTO $NucleoCoTo){
		$this->beginTransaction();
		try {
			$nucleo = new NucleoCoORM();
			$nucleo->update($NucleoCoTo->toArrayUpdate(false, array('id_nucleoco')), $nucleo->montarWhere($NucleoCoTo, true));
			$this->commit();
		} catch (Zend_Exception $e) {
			$this->rollBack();
			throw $e;
		}
	}
	
	/**
	 * Método de listagem da VWNucleoCoTO
	 * @param VwNucleoCoTO $NucleoCoTo
	 * @return Ambigous <boolean, multitype:>
	 * @see NucleoCoBO::listarVwNucleoCo();
	 */
	public function listarVwNucleo(VwNucleoCoTO $NucleoCoTo){
		$nucleo = new VwNucleoCoORM();
		return $nucleo->consulta($NucleoCoTo, false, false, 'st_nucleoco ASC');
	}
	
	/**
	 * Método de listagem da NucleoCoTO
	 * @param NucleoCoTO $nucleoCo
	 * @return Ambigous <boolean, multitype:>
	 * @see NucleoCoBO::listarNucleoCo
	 */
	public function listarNucleoCo(NucleoCoTO $nucleoCo){
		$nucleo = new NucleoCoORM();
		return $nucleo->consulta($nucleoCo);
	}
	
	/**
	 * Deleta de forma lógica
	 * @param int $idNucleo
	 * @throws Zend_Exception
	 */
	public function deletarNucleo(NucleoCoTO $NucleoCoTo){

		if (! $NucleoCoTo->getId_nucleoco ())
		throw new Zend_Exception ( "Id do núcleo não informado" );
	
		$this->beginTransaction ();
		try {
			$nucleoto = new NucleoCoTO ();
			$nucleoto->setId_nucleoco ( $NucleoCoTo->getId_nucleoco () );
			$nucleoto->setBl_ativo ( false );
			
			$nucleo = new NucleoCoORM ();
			$nucleo->update ( $nucleoto->toArrayUpdate ( false, array (
					'id_nucleoco' 
			) ), $nucleo->montarWhere ( $nucleoto, true ) );
			$this->commit ();
		} catch ( Zend_Exception $e ) {
			$this->rollBack ();
			throw $e;
		}
					
	}

	/**
	 * Método que retorna do banco de dados registros da tb_nucleofinalidade
	 * @param NucleoFinalidadeTO $to
	 * @param unknown_type $where
	 * @throws Zend_Exception
	 * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
	 */
	public function retornarNucleoFinalidadeTO(NucleoFinalidadeTO $to, $where = null) {
		$orm = new NucleoFinalidadeORM();
		if (! $where) {
			$where = $orm->montarWhere ( $to );
			//$where .= " AND bl_ativo = 1 ";
		}
		try {
			$obj = $orm->fetchAll($where);
			return $obj;
		} catch (Zend_Exception $e) {
			$msg ['WHERE'] = $where;
			$msg ['SQL'] = $e->getMessage();
			$msg ['TO'] = $to;
			$this->excecao = $msg;
			THROW new Zend_Exception($e->getMessage());
			return false;
		}
	}
	
	/**
	 * Retorna dados da view
	 * @param VwNucleoOcorrenciaTO $to
	 * @param unknown_type $where
	 * @throws Zend_Exception
	 * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
	 */
	public function retornarVwNucleoOcorrenciaTO(VwNucleoOcorrenciaTO $to, $where = null) {
		$orm = new VwNucleoOcorrenciaORM();
		if (! $where) {
			$where = $orm->montarWhereView($to);
		}
		try {
                        //Fix DeadLock da vw_nucleoocorrencia
                        return false;
			$obj = $orm->fetchAll($where);
			return $obj;
		} catch (Zend_Exception $e) {
			$msg ['WHERE'] = $where;
			$msg ['SQL'] = $e->getMessage();
			$msg ['TO'] = $to;
			$this->excecao = $msg;
			THROW new Zend_Exception($e->getMessage());
			return false;
		}
	}


    /**
     * Método que retorna as pessoas vinculadas a um núcleo
     * @param VwPessoasNucleoAssuntoTO $to
     * @throws Zend_Exception
     * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
     */
    public function retornaPessoasNucleoAssunto(VwPessoasNucleoAssuntoTO $to) {
        $orm = new VwPessoasNucleoAssuntoORM();
        $where = $orm->montarWhereView($to);
        try {
            $obj = $orm->fetchAll($where, 'st_nomecompleto');
            return $obj;
        } catch (Zend_Exception $e) {
            $msg ['WHERE'] = $where;
            $msg ['SQL'] = $e->getMessage();
            $msg ['TO'] = $to;
            $this->excecao = $msg;
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }


    /**
     * @param VwAssuntoNucleoPessoaTO $to
     * @return bool|Zend_Db_Table_Rowset_Abstract
     * @throws Zend_Exception
     */
    public function retornaVwAssuntoNucleoPessoa(VwAssuntoNucleoPessoaTO $to) {
        $orm = new VwAssuntoNucleoPessoaORM();
        $where = $orm->montarWhereView($to, false, true);
        try {
            $obj = $orm->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $msg ['WHERE'] = $where;
            $msg ['SQL'] = $e->getMessage();
            $msg ['TO'] = $to;
            $this->excecao = $msg;
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

}
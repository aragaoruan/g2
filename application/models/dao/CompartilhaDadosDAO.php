<?php
/**
 * Classe de abstração de dados de Compartilhados
 * @author eduardoromao
 */
class CompartilhaDadosDAO extends Ead1_DAO{

	/**
	 * Método cadastra dados compartilhados
	 * @param CompartilhaDadosTO $to
	 * @return boolean $cadastrado
	 */
	public function cadastrarCompartilhaDados(CompartilhaDadosTO $to){
		try{
			$cadastrado = false;
			$this->beginTransaction();
			$orm = new CompartilhadadosORM();
			$orm->insert($to->toArrayInsert());
			$cadastrado = true;
			$this->commit();
		}catch (Zend_Exception $e){
			$this->rollBack();
			THROW $e;
		}
		return $cadastrado;
	}

	/**
	 * Método edita dados compartilhados
	 * @param CompartilhaDadosTO $to
	 * @param Mixed $where
	 * @return boolean $editado
	 */
	public function editarCompartilhaDados(CompartilhaDadosTO $to, $where = null){
		try{
			$editado = false;
			$this->beginTransaction();
			$orm = new CompartilhadadosORM();
			$where = $where ? $where : $orm->montarWhere($to,false);
			if(!$where){
				THROW new Zend_Exception("O Where não pode vir vazio!");
			}
			$orm->update($to->toArrayUpdate(false,array('id_compartilhadados')),$where);
			$editado = true;
			$this->commit();
		}catch (Zend_Exception $e){
			$this->rollBack();
			THROW $e;
		}
		return $editado;
	}

	/**
	 * Método que exclui dados compartilhados
	 * @param CompartilhaDadosTO $to
	 * @param Mixed $where
	 * @return boolean $deletado
	 */
	public function deletarCompartilhaDados(CompartilhaDadosTO $to, $where = null){
		try{
			$deletado = false;
			$this->beginTransaction();
			$orm = new CompartilhadadosORM();
			$where = $where ? $where : $orm->montarWhere($to,false);
			if(!$where){
				THROW new Zend_Exception("O Where não pode vir vazio!");
			}
			$orm->delete($where);
			$deletado = true;
			$this->commit();
		}catch (Zend_Exception $e){
			$this->rollBack();
			THROW $e;
		}
		return $deletado;
	}
	
	
	
	
	
	/**
	 * Método que retorna os dados compartilhados
	 * @param CompartilhaDadosTO $to
	 * @param Mixed $where
	 * @return Zend_Db_Table_Rowset
	 */
	public function retornarCompartilhaDados(CompartilhaDadosTO $to, $where = null){
		$orm = new CompartilhadadosORM();
		return $orm->fetchAll($where ? $where : $orm->montarWhere($to,false));
	}
	
	
	
	
}
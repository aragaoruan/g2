<?php

/**
 * @author Rafael Rocha / Gabriel Resende
 */
class Totvs_VwSincronizaValorVendaFluxusG2ORM extends Ead1_ORM {

    public $_name = 'vw_sincroniza_valorvenda_fluxus_g2';
    public $_primary = 'id_venda';
    public $_schema = 'totvs';

}

?>
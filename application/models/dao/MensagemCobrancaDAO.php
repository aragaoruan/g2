<?php

/**
 * Classe de acesso a dados referente a MensagemCobrancaDAO
 * @author Elcio Mauo Guimarães - <elcioguimaraes@gmail.com>
 * @since 
 * @package models
 * @subpackage dao
 */
class MensagemCobrancaDAO extends Ead1_DAO {

	/**
	 * Método que insere o MensagemCobranca
	 * 
	 * @param MensagemCobrancaTO $mensagemCobrancaTO
	 * @throws Zend_Exception
	 * @return Ambigous <mixed, multitype:>
	 * @see MensagemCobrancaBO::salvarMensagemCobranca();
	 */
	public function cadastrarMensagemCobranca(MensagemCobrancaTO $mensagemCobrancaTo){
		$this->beginTransaction();
		try{
			$orm = new MensagemCobrancaORM();
			$id_mensagemcobranca = $orm->insert($mensagemCobrancaTo->toArrayInsert());
			$this->commit();
			return $id_mensagemcobranca;
		}catch(Zend_Exception $e){
			$this->rollBack();
			throw $e;
		}	
	}
	
	/**
	 * Método de edição do MensagemCobranca
	 * @param MensagemCobrancaTO $mensagemCobrancaTo
	 * @throws Zend_Exception
	 * @see MensagemCobrancaBO::salvarMensagemCobranca();
	 */
	public function editarMensagemCobranca(MensagemCobrancaTO $mensagemCobrancaTo){
		$this->beginTransaction();
		try {
			$orm = new MensagemCobrancaORM();
			$orm->update($mensagemCobrancaTo->toArrayUpdate(false, array('id_mensagemcobranca')), $orm->montarWhere($mensagemCobrancaTo, true));
			$this->commit();
			return true;
		} catch (Zend_Exception $e) {
			$this->rollBack();
			throw $e;
			return false;
		}
	}
	
	/**
	 * Método de listagem de MensagemCobranca
	 * @param VwMensagemCobrancaTO $to
	 * @return Ambigous <boolean, multitype:>
	 * @see MensagemCobrancaBO::listarMensagemCobranca();
	 */
	public function listarMensagemCobranca(VwMensagemCobrancaTO $to){
		$orm = new VwMensagemCobrancaORM();
		return $orm->consulta($to);
	}
	
	/**
	 * Método de listagem de Mensagem de Cobranca para o Robo
	 * @param VwMensagemCobrancaRoboTO $to
	 * @return Ambigous <boolean, multitype:>
	 * @see MensagemCobrancaBO::listarMensagemCobranca();
	 */
	public function listarMensagemCobrancaRobo(VwMensagemCobrancaRoboTO $to){
		$orm = new VwMensagemCobrancaRoboORM();
		return $orm->consulta($to);
	}
	
}
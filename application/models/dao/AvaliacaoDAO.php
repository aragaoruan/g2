<?php

/**
 * Classe de persistência de avaliação ao banco de dados
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage dao
 */
class AvaliacaoDAO extends Ead1_DAO
{

    /**
     * Metodo que cadastra uma avaliacao
     * @param AvaliacaoTO $aTO
     * @see AvaliacaoBO::cadastrarAvaliacao
     * @return int $id_avaliacao | false
     */
    public function cadastrarAvaliacao(AvaliacaoTO $aTO)
    {
        // var_dump($aTO);
        try {
            $this->beginTransaction();
            $orm = new AvaliacaoORM();
            $insert = $aTO->toArrayInsert();
            $id_avaliacao = $orm->insert($insert);
            $this->commit();
            return $id_avaliacao;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que cadastra um vinculo de integração com avaliacao
     * @param AvaliacaoIntegracaoTO $to
     * @see AvaliacaoBO::cadastrarAvaliacao
     * @return int $id_avaliacaointegracao | false
     */
    public function cadastrarAvaliacaoIntegracao(AvaliacaoIntegracaoTO $to)
    {
        try {
            $this->beginTransaction();
            $orm = new AvaliacaoIntegracaoORM();
            $insert = $to->toArrayInsert();
            $id_avaliacaointegracao = $orm->insert($insert);
            $this->commit();
            return $id_avaliacaointegracao;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que cadastra uma avaliacao aluno
     * @param AvaliacaoDisciplinaTO $adTO
     * @see AvaliacaoBO::cadastrarAvaliacao
     * @see AvaliacaoBO::editarAvaliacao
     * @return int $id_avaliacaodisciplina | false
     */
    public function cadastrarAvaliacaoDisciplina(AvaliacaoDisciplinaTO $adTO)
    {
        try {
            $orm = new AvaliacaoDisciplinaORM();

            $adTOR = $orm->consulta($adTO, false);
            if ($adTOR) {
                if ($adTOR[0]->getId_avaliacaodisciplina()) {
                    return new Ead1_Mensageiro("Disciplina já cadastrada para essa Avaliação", Ead1_IMensageiro::AVISO);
                }
            }

            $this->beginTransaction();
            $insert = $adTO->toArrayInsert();
            $id_avaliacaodisciplina = $orm->insert($insert);
            $this->commit();
            return $id_avaliacaodisciplina;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que cadastra uma avaliacao aluno
     * @param AvaliacaoAlunoTO $aaTO
     * @see AvaliacaoBO::salvarAvaliacaoAluno
     * @return int $id_avaliacaoaluno | false
     */
    public function cadastrarAvaliacaoAluno(AvaliacaoAlunoTO $aaTO)
    {
        try {
            $this->beginTransaction();
            $orm = new AvaliacaoAlunoORM();
            $insert = $aaTO->toArrayInsert();
            $id_avaliacaoaluno = $orm->insert($insert);
            $this->commit();
            return $id_avaliacaoaluno;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            //Zend_Debug::dump($insert,__CLASS__.'('.__LINE__.')');exit;
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que edita uma avaliacao aluno
     * @param AvaliacaoAlunoTO $aaTO
     * @param Mixed $where
     * @see AvaliacaoBO::editarAvaliacaoAluno
     * @return boolean
     */
    public function editarAvaliacaoAluno(AvaliacaoAlunoTO $aaTO, $where = null, $camposIgnorados = null, $aceitaNull = false)
    {
        try {
            $this->beginTransaction();
            $orm = new AvaliacaoAlunoORM();
            if (!$where) {
                $where = $orm->montarWhere($aaTO, true);
            }

            if ($camposIgnorados != null)
                $ci = $camposIgnorados;
            else
                $ci = array('id_avaliacaoaluno');

            $update = $aaTO->toArrayUpdate($aceitaNull, $ci);

//            Zend_Debug::dump($ci);
//            Zend_Debug::dump($aceitaNull);
//            Zend_Debug::dump($where);
//            Zend_Debug::dump($update);
//            die;

            $orm->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
        }
    }

    /**
     * Metodo que cadastra um Conjunto de avaliacao
     * @param AvaliacaoConjuntoTO $acTO
     * @see AvaliacaoBO::cadastrarAvaliacaoConjunto
     * @return int $id_avaliacaoconjunto | false
     */
    public function cadastrarAvaliacaoConjunto(AvaliacaoConjuntoTO $acTO)
    {
        try {
            $this->beginTransaction();
            $orm = new AvaliacaoConjuntoORM();
            $insert = $acTO->toArrayInsert();
            $id_avaliacaoconjunto = $orm->insert($insert);
            $this->commit();
            return $id_avaliacaoconjunto;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que cadastra uma Relacao de Conjunto de avaliacao
     * @param AvaliacaoConjuntoRelacaoTO $acrTO
     * @see AvaliacaoBO::cadastrarAvaliacaoConjuntoRelacao
     * @return int $id_avaliacaoconjunto | false
     */
    public function cadastrarAvaliacaoConjuntoRelacao(AvaliacaoConjuntoRelacaoTO $acrTO)
    {
        try {
            $this->beginTransaction();
            $orm = new AvaliacaoConjuntoRelacaoORM();
            $insert = $acrTO->toArrayInsert();
            $id_avaliacaoconjuntorelacao = $orm->insert($insert);
            $this->commit();
            return $id_avaliacaoconjuntorelacao;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que cadastra a aplicacao da avaliacao
     * @param AvaliacaoAplicacaoTO $apTO
     * @see AvaliacaoBO::cadastrarAvaliacaoAplicacao
     * @return int $id_avaliacaoaplicacao | false
     */
    public function cadastrarAvaliacaoAplicacao(AvaliacaoAplicacaoTO $apTO)
    {
        try {
            $this->beginTransaction();
            $orm = new AvaliacaoAplicacaoORM();
            $insert = $apTO->toArrayInsert();
            $id_avaliacaoaplicacao = $orm->insert($insert);
            $this->commit();
            return $id_avaliacaoaplicacao;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que cadastra um agendamento de avaliacao
     * @param AvaliacaoAgendamentoTO $aaTO
     * @see AvaliacaoBO::cadastrarAvaliacaoAgendamento
     * @return int $id_avaliacaoagendamento | false
     */
    public function cadastrarAvaliacaoAgendamento(AvaliacaoAgendamentoTO $aaTO)
    {
        try {
            $this->beginTransaction();
            $orm = new AvaliacaoAgendamentoORM();
            $insert = $aaTO->toArrayInsert();
            $id_avaliacaoagendamento = $orm->insert($insert);
            $this->commit();
            return $id_avaliacaoagendamento;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que cadastra um conjunto de avaliacao de sala de aula
     * @param AvaliacaoConjuntoSalaTO $acsTO
     * @see AvaliacaoBO::cadastrarAvaliacaoConjuntoSala
     * @return int $id_avaliacaoconjuntosala | false
     */
    public function cadastrarAvaliacaoConjuntoSala(AvaliacaoConjuntoSalaTO $acsTO)
    {
        try {
            $this->beginTransaction();
            $orm = new AvaliacaoConjuntoSalaORM();
            $insert = $acsTO->toArrayInsert();
            $id_avaliacaoconjuntosala = $orm->insert($insert);
            $this->commit();
            return $id_avaliacaoconjuntosala;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que cadastra um conjunto de avaliacao de projeto pedagogico
     * @param AvaliacaoConjuntoProjetoTO $acpTO
     * @see AvaliacaoBO::cadastrarAvaliacaoConjuntoProjeto
     * @return int $id_avaliacaoconjuntoprojeto | false
     */
    public function cadastrarAvaliacaoConjuntoProjeto(AvaliacaoConjuntoProjetoTO $acpTO)
    {
        try {
            $this->beginTransaction();
            $orm = new AvaliacaoConjuntoProjetoORM();
            $insert = $acpTO->toArrayInsert();
            $id_avaliacaoconjuntoprojeto = $orm->insert($insert);
            $this->commit();
            return $id_avaliacaoconjuntoprojeto;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que cadastra a relação de aplicacao da avaliacao
     * @param AvaliacaoAplicacaoRelacaoTO $aprTO
     * @see AvaliacaoBO::cadastrarAvaliacaoAplicacaoRelacao
     * @return boolean
     */
    public function cadastrarAvaliacaoAplicacaoRelacao(AvaliacaoAplicacaoRelacaoTO $aprTO)
    {
        try {
            $this->beginTransaction();
            $orm = new AvaliacaoAplicacaoRelacaoORM();
            $insert = $aprTO->toArrayInsert();
            $orm->insert($insert);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que cadastra a referencia de um conjunto de avaliacao
     * @param AvaliacaoConjuntoReferenciaTO $acrTO
     * @see AvaliacaoBO::cadastrarAvaliacaoConjuntoReferencia
     * @return $id_avaliacaoconjuntoreferencia | false
     */
    public function cadastrarAvaliacaoConjuntoReferencia(AvaliacaoConjuntoReferenciaTO $acrTO)
    {
        $this->beginTransaction();
        try {
            $orm = new AvaliacaoConjuntoReferenciaORM();
            $insert = $acrTO->toArrayInsert();
            $id_avaliacaoconjuntoreferencia = $orm->insert($insert);
            $this->commit();
            return $id_avaliacaoconjuntoreferencia;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que edita uma avaliacao
     * @param AvaliacaoTO $aTO
     * @param Mixed $where
     * @see AvaliacaoBO::editarAvaliacao
     * @return boolean
     */
    public function editarAvaliacao(AvaliacaoTO $aTO, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new AvaliacaoORM();
            if (!$where) {
                $where = $orm->montarWhere($aTO, true);
            }
            $update = $aTO->toArrayUpdate(false, array('id_avaliacao', 'dt_cadastro', 'id_usuariocadastro', 'id_entidade'));
            $orm->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que edita uma avaliacao
     * @param AvaliacaoIntegracaoTO $aiTO
     * @param Mixed $where
     * @see AvaliacaoBO::editarAvaliacao
     * @return boolean
     */
    public function editarAvaliacaoIntegracao(AvaliacaoIntegracaoTO $aiTO, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new AvaliacaoIntegracaoORM();
            if (!$where) {
                $where = $orm->montarWhere($aiTO, true);
            }
            $update = $aiTO->toArrayUpdate(false, array('id_avaliacaointegracao', 'id_avaliacao', 'id_usuariocadastro', 'dt_cadastro'));
            $orm->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que edita um Conjunto de avaliacao
     * @param AvaliacaoConjuntoTO $acTO
     * @param Mixed $where
     * @see AvaliacaoBO::editarAvaliacaoConjunto
     * @return boolean
     */
    public function editarAvaliacaoConjunto(AvaliacaoConjuntoTO $acTO, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new AvaliacaoConjuntoORM();
            if (!$where) {
                $where = $orm->montarWhere($acTO, true);
            }
            $update = $acTO->toArrayUpdate(false, array('dt_cadastro', 'id_usuariocadastro', 'id_entidade', 'id_avaliacaoconjunto'));
            $orm->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que edita uma relacao de Conjunto de avaliacao
     * @param AvaliacaoConjuntoRelacaoTO $acrTO
     * @param Mixed $where
     * @see AvaliacaoBO::editarAvaliacaoConjuntoRelacao
     * @return boolean
     */
    public function editarAvaliacaoConjuntoRelacao(AvaliacaoConjuntoRelacaoTO $acrTO, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new AvaliacaoConjuntoRelacaoORM();
            if (!$where) {
                $where = $orm->montarWhere($acrTO, true);
            }
            $update = $acrTO->toArrayUpdate(false, array('id_avaliacaoconjuntorelacao'));
            $orm->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que edita uma aplicacao de uma avaliacao
     * @param AvaliacaoAplicacaoTO $apTO
     * @param Mixed $where
     * @see AvaliacaoBO::editarAvaliacaoAplicacao
     * @return boolean
     */
    public function editarAvaliacaoAplicacao(AvaliacaoAplicacaoTO $apTO, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new AvaliacaoAplicacaoORM();
            if (!$where) {
                $where = $orm->montarWhere($apTO, true);
            }
            $update = $apTO->toArrayUpdate(true, array('id_avaliacaoaplicacao', 'dt_cadastro', 'id_usuariocadastro', 'id_entidade'));
            $orm->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que edita um agendamento de uma avaliacao
     * @param AvaliacaoAgendamentoTO $aaTO
     * @param Mixed $where
     * @see AvaliacaoBO::editarAvaliacaoAgendamento
     * @return boolean
     */
    public function editarAvaliacaoAgendamento(AvaliacaoAgendamentoTO $aaTO, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new AvaliacaoAgendamentoORM();
            if (!$where) {
                $where = $orm->montarWhere($aaTO);
            }
            $update = $aaTO->toArrayUpdate(false, array('id_avaliacaoagendamento', 'id_avaliacaoaplicacao', 'id_matricula', 'id_usuariocadastro', 'dt_cadastro'));
            $orm->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que edita um conjunto de avaliacao de projeto pedagogico
     * @param AvaliacaoConjuntoProjetoTO $acpTO
     * @param Mixed $where
     * @see AvaliacaoBO::editarAvaliacaoConjuntoProjeto
     * @return boolean
     */
    public function editarAvaliacaoConjuntoProjeto(AvaliacaoConjuntoProjetoTO $acpTO, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new AvaliacaoConjuntoProjetoORM();
            if (!$where) {
                $where = $orm->montarWhere($acpTO);
            }
            $update = $acpTO->toArrayUpdate(false, array('id_avaliacaoconjuntoprojeto', 'id_projetopedagogico', 'id_avaliacaoconjunto'));
            $orm->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que edita um conjunto de avaliacao de sala de aula
     * @param AvaliacaoConjuntoSalaTO $acsTO
     * @param Mixed $where
     * @see AvaliacaoBO::editarAvaliacaoConjuntoSala
     * @return boolean
     */
    public function editarAvaliacaoConjuntoSala(AvaliacaoConjuntoSalaTO $acsTO, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new AvaliacaoConjuntoSalaORM();
            if (!$where) {
                $where = $orm->montarWhere($acsTO);
            }
            $update = $acsTO->toArrayUpdate(false, array('id_avaliacaoconjuntosala', 'id_projetopedagogico', 'id_avaliacaoconjunto'));
            $orm->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que edita uma referencia de  conjunto de avaliacao
     * @param AvaliacaoConjuntoReferenciaTO $acrTO
     * @param Mixed $where
     * @see AvaliacaoBO::editarAvaliacaoConjuntoReferencia
     * @return boolean
     */
    public function editarAvaliacaoConjuntoReferencia(AvaliacaoConjuntoReferenciaTO $acrTO, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new AvaliacaoConjuntoReferenciaORM();
            if (!$where) {
                $where = $orm->montarWhere($acrTO, true);
            }
            $update = $acrTO->toArrayUpdate(true, array('id_avaliacaoconjuntoreferencia', 'id_avaliacaoconjunto'));
            $orm->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que exclui uma avaliacao
     * @param AvaliacaoTO $aTO
     * @param Mixed $where
     * @see AvaliacaoBO::deletarAvaliacao
     * @return boolean
     */
    public function deletarAvaliacao(AvaliacaoTO $aTO, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new AvaliacaoORM();
            if (!$where) $where = $orm->montarWhere($aTO, true);
            if (empty($where)) THROW new Zend_Exception('O Where esta vazio!');
            $orm->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que exclui um conjunto de avaliacao
     * @param AvaliacaoConjuntoTO $acTO
     * @param Mixed $where
     * @see AvaliacaoBO::deletarAvaliacaoConjunto
     * @return boolean
     */
    public function deletarAvaliacaoConjunto(AvaliacaoConjuntoTO $acTO, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new AvaliacaoConjuntoORM();
            if (!$where) {
                $where = $orm->montarWhere($acTO, true);
            }
            if (empty($where)) {
                THROW new Zend_Exception('O Where esta vazio!');
            }
            $orm->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que exclui uma relacao de conjunto de avaliacao
     * @param AvaliacaoConjuntoRelacaoTO $acrTO
     * @param Mixed $where
     * @see AvaliacaoBO::deletarAvaliacaoConjuntoRelacao
     * @return boolean
     */
    public function deletarAvaliacaoConjuntoRelacao(AvaliacaoConjuntoRelacaoTO $acrTO, $where = null)
    {
        $orm = new AvaliacaoConjuntoRelacaoORM();
        if (!$where) {
            $where = $orm->montarWhere($acrTO, true);
        }
        if (empty($where)) {
            THROW new Zend_Exception('O where está vazio!');
        }
        $orm->delete($where);
        $this->commit();
        return true;
    }

    /**
     * Metodo que exclui uma aplicacao de uma avaliacao
     * @param AvaliacaoAplicacaoTO $apTO
     * @param Mixed $where
     * @see AvaliacaoBO::deletarAvaliacaoAplicacao
     * @return boolean
     */
    public function deletarAvaliacaoAplicacao(AvaliacaoAplicacaoTO $apTO, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new AvaliacaoAplicacaoORM();
            if (!$where) {
                $where = $orm->montarWhere($apTO, true);
            }
            if (empty($where)) {
                THROW new Zend_Exception('O Where esta vazio!');
            }
            $orm->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que exclui a relação de aplicacao da avaliacao
     * @param AvaliacaoAplicacaoRelacaoTO $aprTO
     * @param Mixed $where
     * @see AvaliacaoBO::deletarAvaliacaoAplicacaoRelacao
     * @return boolean
     */
    public function deletarAvaliacaoAplicacaoRelacao(AvaliacaoAplicacaoRelacaoTO $aprTO, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new AvaliacaoAplicacaoRelacaoORM();
            if (!$where) {
                $where = $orm->montarWhere($aprTO, true);
            }
            if (empty($where)) {
                THROW new Zend_Exception('O Where esta vazio!');
            }
            $orm->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que exclui um agendamento de avaliacao
     * @param AvaliacaoAgendamentoTO $aaTO
     * @param Mixed $where
     * @see AvaliacaoBO::deletarAvaliacaoAgendamento
     * @return boolean
     */
    public function deletarAvaliacaoAgendamento(AvaliacaoAgendamentoTO $aaTO, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new AvaliacaoAgendamentoORM();
            if (!$where) {
                $where = $orm->montarWhere($aaTO, true);
            }
            if (empty($where)) {
                THROW new Zend_Exception('O Where esta vazio!');
            }
            $orm->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que exclui a relação de avaliacao com disciplina
     * @param AvaliacaoDisciplinaTO $to
     * @param Mixed $where
     * @see AvaliacaoBO::deletarAvaliacaoDisciplina
     * @return boolean
     */
    public function deletarAvaliacaoDisciplina(AvaliacaoDisciplinaTO $to, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new AvaliacaoDisciplinaORM();
            if (!$where) $where = $orm->montarWhere($to, true);
            if (empty($where)) THROW new Zend_Exception('O Where esta vazio!');
            $orm->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que exclui os vinculos da avaliacao com a avaliacao de integracao
     * @param AvaliacaoIntegracaoTO $to
     * @param Mixed $where
     * @return boolean
     */
    public function deletarAvaliacaoIntegracao(AvaliacaoIntegracaoTO $to, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new AvaliacaoIntegracaoORM();
            if (!$where) $where = $orm->montarWhere($to, true);
            if (empty($where)) THROW new Zend_Exception('O Where esta vazio!');
            $orm->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que exclui um conjunto de avaliacao de sala de aula
     * @param AvaliacaoConjuntoSalaTO $acsTO
     * @param Mixed $where
     * @see AvaliacaoBO::deletarAvaliacaoConjuntoSala
     * @return boolean
     */
    public function deletarAvaliacaoConjuntoSala(AvaliacaoConjuntoSalaTO $acsTO, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new AvaliacaoConjuntoSalaORM();
            if (!$where) {
                $where = $orm->montarWhere($acsTO, true);
            }
            if (empty($where)) {
                THROW new Zend_Exception('O Where esta vazio!');
            }
            $orm->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que exclui um conjunto de avaliacao de projeto pedagogico
     * @param AvaliacaoConjuntoProjetoTO $acpTO
     * @param Mixed $where
     * @see AvaliacaoBO::deletarAvaliacaoConjuntoProjeto
     * @return boolean
     */
    public function deletarAvaliacaoConjuntoProjeto(AvaliacaoConjuntoProjetoTO $acpTO, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new AvaliacaoConjuntoProjetoORM();
            if (!$where) {
                $where = $orm->montarWhere($acpTO, true);
            }
            if (empty($where)) {
                THROW new Zend_Exception('O Where esta vazio!');
            }
            $orm->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que exclui a referencia de  conjunto de avaliacao
     * @param AvaliacaoConjuntoReferenciaTO $acrTO
     * @param Mixed $where
     * @see AvaliacaoBO::deletarAvaliacaoConjuntoReferencia
     * @return boolean
     */
    public function deletarAvaliacaoConjuntoReferencia(AvaliacaoConjuntoReferenciaTO $acrTO, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new AvaliacaoConjuntoReferenciaORM();
            if (!$where) {
                $where = $orm->montarWhere($acrTO, true);
            }
            if (empty($where)) {
                THROW new Zend_Exception('O Where esta vazio!');
            }
            $orm->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que retorna avaliacao
     * @param AvaliacaoTO $aTO
     * @param Mixed $where
     * @see AvaliacaoBO::retornarAvaliacao
     * @return Zend_Db_Table_Rowset_Abstract | false
     */
    public function retornarAvaliacao(AvaliacaoTO $aTO, $where = null)
    {
        try {
            $orm = new AvaliacaoORM();
            if (!$where) {
                $where = $orm->montarWhere($aTO);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que retorna avaliacao disciplina
     * @param VwAvaliacaoDisciplinaTO $aTO
     * @see AvaliacaoBO::retornarAvaliacaoDisciplina
     * @return Zend_Db_Table_Rowset_Abstract | false
     */
    public function retornarAvaliacaoDisciplina(VwAvaliacaoDisciplinaTO $aTO)
    {
        try {
            $orm = new VwAvaliacaoDisciplinaORM();
            return $orm->fetchAll("id_avaliacao = {$aTO->getId_avaliacao()}");
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que retorna tipo de avaliacao
     * @param TipoAvaliacaoTO $taTO
     * @param Mixed $where
     * @see AvaliacaoBO::retornarTipoAvaliacao
     * @return Zend_Db_Table_Rowset_Abstract | false
     */
    public function retornarTipoAvaliacao(TipoAvaliacaoTO $taTO, $where = null)
    {
        try {
            $orm = new TipoAvaliacaoORM();
            if (!$where) {
                $where = $orm->montarWhere($taTO);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que retorna um conjunto de avaliacao
     * @param AvaliacaoConjuntoTO $acTO
     * @param Mixed $where
     * @see AvaliacaoBO::retornarAvaliacaoConjunto
     * @return Zend_Db_Table_Rowset_Abstract | false
     */
    public function retornarAvaliacaoConjunto(AvaliacaoConjuntoTO $acTO, $where = null)
    {
        try {
            $orm = new AvaliacaoConjuntoORM();
            if (!$where) {
                $where = $orm->montarWhere($acTO);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que retorna tipo de calculo de avaliacao
     * @param TipoCalculoAvaliacaoTO $tcaTO
     * @param Mixed $where
     * @see AvaliacaoBO::retornarTipoCalculoAvaliacao
     * @return Zend_Db_Table_Rowset_Abstract | false
     */
    public function retornarTipoCalculoAvaliacao(TipoCalculoAvaliacaoTO $tcaTO, $where = null)
    {
        try {
            $orm = new TipoCalculoAvaliacaoORM();
            if (!$where) {
                $where = $orm->montarWhere($tcaTO);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que retorna a relacao do conjunto de avaliacoes
     * @param AvaliacaoConjuntoRelacaoTO $acrTO
     * @param Mixed $where
     * @see AvaliacaoBO::retornarAvaliacaoConjuntoRelacao
     * @return Zend_Db_Table_Rowset_Abstract | false
     */
    public function retornarAvaliacaoConjuntoRelacao(AvaliacaoConjuntoRelacaoTO $acrTO, $where = null)
    {
        try {
            $orm = new AvaliacaoConjuntoRelacaoORM();
            if (!$where) {
                $where = $orm->montarWhere($acrTO);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que retorna a aplicacao da avaliacao
     * @param AvaliacaoAplicacaoTO $apTO
     * @param Mixed $where
     * @see AvaliacaoBO::retornarAvaliacaoAplicacao
     * @return Zend_Db_Table_Rowset_Abstract | false
     */
    public function retornarAvaliacaoAplicacao(AvaliacaoAplicacaoTO $apTO, $where = null)
    {
        try {
            $orm = new AvaliacaoAplicacaoORM();
            if (!$where) {
                $where = $orm->montarWhere($apTO);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que retorna a relação de aplicacao da avaliacao
     * @param AvaliacaoAplicacaoRelacaoTO $aprTO
     * @param Mixed $where
     * @see AvaliacaoBO::retornarAvaliacaoAplicacaoRelacao
     * @return Zend_Db_Table_Rowset_Abstract | false
     */
    public function retornarAvaliacaoAplicacaoRelacao(AvaliacaoAplicacaoRelacaoTO $aprTO, $where = null)
    {
        try {
            $orm = new AvaliacaoAplicacaoRelacaoORM();
            if (!$where) {
                $where = $orm->montarWhere($aprTO);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que retorna um agendamento de avaliacao
     * @param AvaliacaoAgendamentoTO $aaTO
     * @param Mixed $where
     * @see AvaliacaoBO::retornarAvaliacaoAgendamento
     * @return Zend_Db_Table_Rowset_Abstract | false
     */
    public function retornarAvaliacaoAgendamento(AvaliacaoAgendamentoTO $aaTO, $where = null)
    {
        try {
            $orm = new AvaliacaoAgendamentoORM();
            if (!$where) {
                $where = $orm->montarWhere($aaTO);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que retorna um conjunto de avaliacao de sala de aula
     * @param AvaliacaoConjuntoSalaTO $acsTO
     * @param Mixed $where
     * @see AvaliacaoBO::retornarAvaliacaoConjuntoSala
     * @return Zend_Db_Table_Rowset_Abstract | false
     */
    public function retornarAvaliacaoConjuntoSala(AvaliacaoConjuntoSalaTO $acsTO, $where = null)
    {
        try {
            $orm = new AvaliacaoConjuntoSalaORM();
            if (!$where) {
                $where = $orm->montarWhere($acsTO);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que retorna um conjunto de avaliacao de projeto pedagogico
     * @param AvaliacaoConjuntoProjetoTO $acpTO
     * @param Mixed $where
     * @see AvaliacaoBO::retornarAvaliacaoConjuntoProjeto
     * @return Zend_Db_Table_Rowset_Abstract | false
     */
    public function retornarAvaliacaoConjuntoProjeto(AvaliacaoConjuntoProjetoTO $acpTO, $where = null)
    {
        try {
            $orm = new AvaliacaoConjuntoProjetoORM();
            if (!$where) {
                $where = $orm->montarWhere($acpTO);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que retorna a referencia conjunto de avaliacao
     * @param AvaliacaoConjuntoReferenciaTO $acrTO
     * @param Mixed $where
     * @see AvaliacaoBO::retornarAvaliacaoConjuntoReferencia
     * @return Zend_Db_Table_Rowset_Abstract | false
     */
    public function retornarAvaliacaoConjuntoReferencia(AvaliacaoConjuntoReferenciaTO $acrTO, $where = null)
    {
        try {
            $orm = new AvaliacaoConjuntoReferenciaORM();
            if (!$where) {
                $where = $orm->montarWhere($acrTO);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que retorna avaliacao
     * @param AvaliacaoIntegracaoTO $aiTO
     * @param Mixed $where
     * @see AvaliacaoBO::retornarAvaliacaoIntegracao
     * @return Zend_Db_Table_Rowset_Abstract | false
     */
    public function retornarAvaliacaoIntegracao(AvaliacaoIntegracaoTO $aiTO, $where = null)
    {
        try {
            $orm = new AvaliacaoIntegracaoORM();
            if (!$where) {
                $where = $orm->montarWhere($aiTO);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que retorna um conjunto de avaliacao de projeto pedagogico
     * @param VwAvaliacaoAplicacaoAgendamentoTO $vaaaTO
     * @param Mixed $where
     * @see AvaliacaoBO::retornarVwAvaliacaoAplicacaoAgendamento()
     * @return Zend_Db_Table_Rowset_Abstract | false
     */
    public function retornarVwAvaliacaoAplicacaoAgendamento(VwAvaliacaoAplicacaoAgendamentoTO $vaaaTO, $where = null)
    {
        try {
            $orm = new VwAvaliacaoAplicacaoAgendamentoORM();
            if (!$where) {
                $where = $orm->montarWhereView($vaaaTO, false, true);
            }
            return $orm->fetchAll($where, 'dt_agendamento');
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que retorna um conjunto de avaliacao, matricula, notas e modulo
     * @param VwAvaliacaoAlunoTO $vaaTO
     * @param Mixed $where
     * @see AvaliacaoBO::retornarVwAvaliacaoAluno()
     * @return Zend_Db_Table_Rowset_Abstract | false
     */
    public function retornarVwAvaliacaoAluno(VwAvaliacaoAlunoTO $vaaTO, $where = null)
    {
        try {
            $orm = new VwAvaliacaoAlunoORM();
            if (!$where) {
                $where = $orm->montarWhereView($vaaTO, false, true);
            }
            return $orm->fetchAll($where, 'st_tituloexibicaodisciplina');
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }


    /**
     * Metodo que retorna um conjunto de avaliacao, matricula, notas e modulo mara o histórico
     * @param VwAvaliacaoAlunoHistoricoTO $vaaTO
     * @param Mixed $where
     * @see AvaliacaoBO::retornarVwAvaliacaoAlunoHistorico()
     * @return Zend_Db_Table_Rowset_Abstract | false
     */
    public function retornarVwAvaliacaoAlunoHistorico(VwAvaliacaoAlunoHistoricoTO $vaaTO, $where = null)
    {
        try {
            $orm = new VwAvaliacaoAlunoHistoricoORM();
            if (!$where) {
                $where = $orm->montarWhereView($vaaTO, false, true);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que retorna disciplina para notas de aluno
     * @param VwAvaliacaoAlunoTO $vaaTO
     * @see AvaliacaoBO::retornarVwAvaliacaoAluno()
     * @return Zend_Db_Table_Rowset_Abstract | false
     */
    public function retornarVwAvaliacaoAlunoDisciplina(VwAvaliacaoAlunoTO $vaaTO)
    {
        try {
            $orm = new VwAvaliacaoAlunoORM();
            $where = $orm->montarWhereView($vaaTO, false, true);
            $select = $orm->select()
                ->distinct(true)
                ->from('vw_avaliacaoaluno', '')
                ->columns(array('id_matricula', 'id_modulo', 'st_tituloexibicaomodulo', 'id_disciplina', 'id_evolucao', 'st_evolucao', 'st_tituloexibicaodisciplina', 'id_situacao', 'st_situacao'))
                ->where($where);
            return $orm->fetchAll($select);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que retorna módulo para notas de aluno
     * @param VwAvaliacaoAlunoTO $vaaTO
     * @see AvaliacaoBO::retornarVwAvaliacaoAluno()
     * @return Zend_Db_Table_Rowset_Abstract | false
     */
    public function retornarVwAvaliacaoAlunoModulo(VwAvaliacaoAlunoTO $vaaTO)
    {
        try {
            $orm = new VwAvaliacaoAlunoORM();
            $where = $orm->montarWhereView($vaaTO, false, true);
            $select = $orm->select()
                ->distinct(true)
                ->from('vw_avaliacaoaluno', '')
                ->columns(array('id_matricula', 'id_modulo', 'st_tituloexibicaomodulo', 'id_evolucao', 'st_evolucao', 'id_situacao', 'st_situacao'))
                ->where($where);
            return $orm->fetchAll($select);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que retorna a view de avaliacao
     * @param VwPesquisaAvaliacaoTO $vwpaTO
     * @param Mixed $where
     * @see AvaliacaoBO::retornarVwPesquisaAvaliacao
     * @return Zend_Db_Table_Rowset_Abstract | false
     */
    public function retornarVwPesquisaAvaliacao(VwPesquisaAvaliacaoTO $vwpaTO, $where = null)
    {
        try {
            $orm = new VwPesquisaAvaliacaoORM();
            if (!$where) {
                $where = $orm->montarWhereView($vwpaTO, false, true);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que retorna a view de avaliacaoconjunto
     * @param VwAvaliacaoConjuntoTO $vwacTO
     * @param Mixed $where
     * @see AvaliacaoBO::retornarVwAvaliacaoConjunto
     * @return Zend_Db_Table_Rowset_Abstract | false
     */
    public function retornarVwAvaliacaoConjunto(VwAvaliacaoConjuntoTO $vwacTO, $where = null)
    {
        try {
            $orm = new VwAvaliacaoConjuntoORM();
            if (!$where) {
                $where = $orm->montarWhereView($vwacTO, false, true);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que retorna a view de avaliacaoconjuntorelacao
     * @param VwAvaliacaoConjuntoRelacaoTO $vwacrTO
     * @param Mixed $where
     * @see AvaliacaoBO::retornarVwAvaliacaoConuntoRelacao
     * @return Zend_Db_Table_Rowset_Abstract | false
     */
    public function retornarVwAvaliacaoConjuntoRelacao(VwAvaliacaoConjuntoRelacaoTO $vwacrTO, $where = null)
    {
        try {
            $orm = new VwAvaliacaoConjuntoRelacaoORM();
            if (!$where) {
                $where = $orm->montarWhereView($vwacrTO, false, true);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que retorna a view de avaliacaoconjuntoreferencia
     * @param VwAvaliacaoConjuntoReferenciaTO $vwacrTO
     * @param Mixed $where
     * @see AvaliacaoBO::retornarVwAvaliacaoConjuntoReferencia
     * @return Zend_Db_Table_Rowset_Abstract | false
     */
    public function retornarVwAvaliacaoConjuntoReferencia(VwAvaliacaoConjuntoReferenciaTO $vwacrTO, $where = null)
    {
        try {
            $orm = new VwAvaliacaoConjuntoReferenciaORM();
            if (!$where) {
                $where = $orm->montarWhereView($vwacrTO, false, true);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Método que retorna a integração da avaliacao
     * @param AvaliacaoIntegracaoTO $to
     * @param Mixed $where
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function retornarAvaliacaoIntegracaoTO(AvaliacaoIntegracaoTO $to, $where = null)
    {
        try {
            $orm = new AvaliacaoIntegracaoORM();
            if (!$where) {
                $where = $orm->montarWhere($to);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que exclui logicamente o registro de nota
     * @param AvaliacaoAlunoTO $aaTO
     * @param Mixed $where
     * @see AvaliacaoBO::excluirNotaAvaliacao()
     * @return boolean
     */
    public function excluirNotaAvaliacao(AvaliacaoAlunoTO $aaTO, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new AvaliacaoAlunoORM();
            if (!$where) {
                $where = $orm->montarWhere($aaTO, false);
            }
            if ($orm->fetchRow($where)) {
                $orm->update($aaTO->toArrayUpdate(false, array('id_avaliacaoaluno', 'id_matricula', 'id_avaliacao')), $where);
            }
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Metodo que exclui o vínculo de avaliação com disciplina
     * @param AvaliacaoDisciplinaTO $adTO
     * @see AvaliacaoBO::salvarAvaliacao()
     * @return Zend_Db_Table_Rowset_Abstract | false
     */
    public function excluirAvaliacaoDisciplina(AvaliacaoDisciplinaTO $adTO)
    {
        try {
            $this->beginTransaction();
            $tbAvaliacaoDisciplina = new AvaliacaoDisciplinaORM();
            $tbAvaliacaoDisciplina->delete($tbAvaliacaoDisciplina->montarWhere($adTO));

            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }


    /**
     * Retorna os módulos em que o aluno
     * possuis disciplinas na situação de cursando
     * @param MatriculaTO $to
     * @return Zend_Db_Table_Rowset
     */
    public function retornarModulosDisciplinaCursandoMatricula(MatriculaTO $to)
    {
        $moduloORM = new ModuloORM();
        $select = $moduloORM->select()
            ->distinct(true)
            ->from(array('mod' => 'tb_modulo'), array('id_modulo', 'st_modulo', 'st_tituloexibicao'))
            ->setIntegrityCheck(false)
            ->joinInner(array('aap' => 'vw_avaliacaoaplicacaoagendamento'), 'aap.id_modulo = mod.id_modulo', null)
            ->where('aap.id_matricula = ?', $to->getId_matricula())
            ->order('mod.st_tituloexibicao');
        return $moduloORM->fetchAll($select);
    }

    /**
     * Retorna a Avaliação Agendamento de acordo com o código do sistema integrado
     * @param string $guidAvaliacao
     * @return array
     */
    public function retornarAvaliacaoAgendamentoIntegracao($guidAvaliacao, $idSistema = SistemaTO::SISTEMA_AVALIACAO)
    {
        $avaliacaoAgendamentoIntegracaoORM = new AvalAgendaIntegracaoORM();
        $select = $avaliacaoAgendamentoIntegracaoORM->select()
            ->from(array('aint' => $avaliacaoAgendamentoIntegracaoORM->_name), '')
            ->setIntegrityCheck(false)
            ->joinInner(array('aa' => 'tb_avaliacaoagendamento'), 'aa.id_avaliacaoagendamento = aint.id_avaliacaoagendamento', 'aa.*')
            ->where("aint.id_sistema = ?", $idSistema)
            ->where("aint.st_codsistema = '$guidAvaliacao'");

        return $avaliacaoAgendamentoIntegracaoORM->fetchAll($select);
    }


    public function retornarIdAvaliacaoAluno(VwAvaliacaoAlunoTO $vaaTO)
    {
        try {
            $orm = new VwAvaliacaoAlunoORM();
            $where = $orm->montarWhereView($vaaTO, false, true);
            $select = $orm->select()
                ->distinct(true)
                ->from('vw_avaliacaoaluno', '')
                ->columns(array('id_avaliacaoaluno'))
                ->where($where);
            return $orm->fetchAll($select);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }


}
<?php
/**
 * Classe de persistencia da campanha comercial com o banco de dados
 * @author Dimas Sulz <dimassulz@gmail.com>
 *
 * @package models
 * @subpackage dao
 */
class CampanhaComercialDAO extends FinanceiroDAO{

	/**
	 * Método que cadastra(insert) uma campanha comercial
	 * @param CampanhaComercialTO $ccTO
	 * @see CampanhaComercialBO::cadastrarCampanhaComercial();
	 * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
	 */
	public function cadastrarCampanhaComercial(CampanhaComercialTO $ccTO){
		$campanhaComercialORM = new CampanhaComercialORM();
		$this->beginTransaction();
		try{
			$id_campanhacomercial = $campanhaComercialORM->insert($ccTO->toArrayInsert());
			$this->commit();
			return $id_campanhacomercial;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}

	/**
	 * Método que cadastra(insert) o convênio de uma campanha comercial
	 * @param CampanhaConvenioTO $ccTO
	 * @see CampanhaComercialBO::cadastrarCampanhaConvenio();
	 * @return boolean
	 */
	public function cadastrarCampanhaConvenio(CampanhaConvenioTO $ccTO){
		$arr['TO'] = $ccTO->toArray();
		$arr['WHERE'] = $ccTO->toArrayInsert();
		$campanhaConvenioORM = new CampanhaConvenioORM();
		$this->beginTransaction();
		try{
			$campanhaConvenioORM->insert($ccTO->toArrayInsert());
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$arr['SQL'] =$e->getMessage();
			$this->excecao = $arr;
			return false;
		}
	}

	/**
	 * Método que faz o vinculo da campanha com um perfil
	 * @param CampanhaPerfilTO $cpTO
	 * @see CampanhaComercialBO::cadastrarCampanhaPerfil();
	 * @return boolean
	 */
	public function cadastrarCampanhaPerfil(CampanhaPerfilTO $cpTO){
		$campanhaPerfilORM = new CampanhaPerfilORM();
		$this->beginTransaction();
		try{
			$campanhaPerfilORM->insert($cpTO->toArrayInsert());
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}


    /**
	 * Método que faz o vinculo da campanha com um lancamento
	 * @param LancamentoCampanhaTO $cpTO
	 * @see CampanhaComercialBO::cadastrarLancamentoCampanha();
	 * @return boolean
	 */
	public function cadastrarLancamentoCampanha(LancamentoCampanhaTO $lcTO){
		$orm = new LancamentoCampanhaORM();
		$this->beginTransaction();
		try{
			$orm->insert($lcTO->toArrayInsert());
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}


    /**
	 * Método que cadastra a campanha comercial junto com a área do conhecimento e projeto pedagógico
	 * @param CampanhaProjetoAreaTO $cpaTO
	 * @see CampanhaComercialBO::cadastrarCampanhaProjetoArea();
	 * @return boolean
	 */
	public function cadastrarCampanhaProjetoArea(CampanhaProjetoAreaTO $cpaTO){
		$campanhaProjetoAreaORM = new CampanhaProjetoAreaORM();
		$arr['TO'] = $cpaTO;
		$arr['WHERE'] = $cpaTO->toArrayInsert();
		$this->beginTransaction();
		try{
			$campanhaProjetoAreaORM->insert($cpaTO->toArrayInsert());
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$arr['SQL'] = $e->getMessage();
			$this->excecao = $arr;
			return false;
		}
	}

	/**
	 * Método que cadastra o desconto da campanha
	 * @param CampanhaDescontoTO $cdTO
	 * @see CampanhaComercialBO::cadastrarCampanhaDesconto();
	 * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
	 */
	public function cadastrarCampanhaDesconto(CampanhaDescontoTO $cdTO){
		$campanhaDescontoORM = new CampanhaDescontoORM();
		$this->beginTransaction();
		try{
			$id_campanhadesconto = $campanhaDescontoORM->insert($cdTO->toArrayInsert());
			$this->commit();
			return $id_campanhadesconto;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}

	/**
	 * Método que edita o desconto da campanha comercial
	 * @param CampanhaDescontoTO $cdTO
	 * @see CampanhaComercialBO::editarCampanhaDesconto();
	 * @return boolean
	 */
	public function editarCampanhaDesconto(CampanhaDescontoTO $cdTO){
		$campanhaDescontoORM = new CampanhaDescontoORM();
		$this->beginTransaction();
		try{
			$campanhaDescontoORM->update($cdTO->toArrayUpdate(false, array('id_campanhadesconto')), 'id_campanhadesconto = '.$cdTO->getId_campanhadesconto());
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}

	/**
	 * Método que edita (update) a o desconto da campanha comercial
	 * @param CampanhaComercialTO $ccTO
	 * @see CampanhaComercialBO::editarCampanhaComercial();
	 * @return boolean
	 */
	public function editarCampanhaComercial(CampanhaComercialTO $ccTO){
		$campanhaComercialORM = new CampanhaComercialORM();
		$this->beginTransaction();
		try{
			$campanhaComercialORM->update($ccTO->toArrayUpdate(false, array('id_campanhacomercial','dt_cadastro', 'id_usuariocadastro')), 'id_campanhacomercial = '.$ccTO->getId_campanhacomercial());
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}


    /**
	 * Método que deleta o convênio de uma campanha comercial
	 * @param CampanhaConvenioTO $ccTO
	 * @see CampanhaComercialBO::deletarCampanhaConvenio();
	 * @return boolean
	 */
	public function deletarCampanhaConvenio(CampanhaConvenioTO $ccTO){
		$campanhaConvenioORM = new CampanhaConvenioORM();
		$this->beginTransaction();
		try{
			$campanhaConvenioORM->delete('id_convenio = '.$ccTO->getId_convenio(). 'AND id_campanhacomercial = '.$ccTO->getId_campanhacomercial());
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}

	/**
	 * Método que deleta o desconto de uma campanha comercial
	 * @param CampanhaDescontoTO $cdTO
	 * @see CampanhaComercialBO::deletarCampanhaDesconto();
	 * @return boolean
	 */
	public function deletarCampanhaDesconto(CampanhaDescontoTO $cdTO){
		$campanhaDescontoORM = new CampanhaDescontoORM();
		$this->beginTransaction();
		try{
			$campanhaDescontoORM->delete('id_campanhadesconto = '.$cdTO->getId_campanhadesconto());
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}

	/**
	 * Método que deleta o perfil da campanha
	 * @param CampanhaPerfilTO $cpTO
	 * @see CampanhaComercialBO::deletarCampanhaPerfil();
	 * @return boolean
	 */
	public function deletarCampanhaPerfil(CampanhaPerfilTO $cpTO){
		$campanhaPerfilORM = new CampanhaPerfilORM();
		$where = $campanhaPerfilORM->montarWhere($cpTO,true);
		$arr['TO'] = $cpTO;
		$arr['WHERE'] = $where;
		$this->beginTransaction();
		try{
			$campanhaPerfilORM->delete($where);
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$arr['SQL'] = $e->getMessage();
			$this->excecao = $arr;
			return false;
		}
	}

	/**
	 * Método que deleta o vinculo da campanha com o projeto pedagogico e área do conhecimento
	 * @param CampanhaProjetoAreaTO $cpaTO
	 * @see CampanhaComercialBO::deletarCampanhaProjetoArea();
	 * @return boolean
	 */
	public function deletarCampanhaProjetoArea(CampanhaProjetoAreaTO $cpaTO){
		$campanhaProjetoAreaORM = new CampanhaProjetoAreaORM();
		$this->beginTransaction();
		try{
			$campanhaProjetoAreaORM->delete('id_projetopedagogico = '.$cpaTO->getId_projetopedagogico(). ' AND id_areaconhecimento = '.$cpaTO->getId_areaconhecimento().' AND id_campanhacomercial = '.$cpaTO->getId_campanhacomercial());
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}

	/**
	 * Metodo que Exclui o vinculo da campanha com o produto
	 * @param CampanhaProdutoTO $cpTO
	 * @param Mixed $where
	 * @return Ead1_Mensageiro
	 */
	public function deletarCampanhaProduto(CampanhaProdutoTO $cpTO,$where = null){
		try{
			$this->beginTransaction();
			$orm = new CampanhaProdutoORM();
			if(!$where){
				$where = $orm->montarWhere($cpTO);
			}
			if(!$where){
				THROW new Zend_Exception('O TO não pode vir vazio!');
			}
			$orm->delete($where);
			$this->commit();
			return true;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW $e;
			return false;
		}
	}

	/**
	 * Método que retorna a campanha comercial da forma de pagamento
	 * @param CampanhaComercialTO $ccTO
	 * @see CampanhaComercialBO::retornarCampanhaComercial();
	 * @todo estudar a possibilidade de levar esse método para RO
     * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function retornarCampanhaComercial(CampanhaComercialTO $ccTO){
		$campanhaComercialORM = new CampanhaComercialORM();
		$where = $campanhaComercialORM->montarWhere($ccTO);

        try {
			$obj = $campanhaComercialORM->fetchAll($where);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		}
	}

    /**
	 * Método que retorna a view de campanha com forma de pagamento.
	 * @param VwCampanhaFormaPagamentoTO $vwcfpTO
	 * @see CampanhaComercialBO::retornarVwCampanhaFormaPagamentoProduto();
     * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function retornarVwCampanhaFormaPagamento(VwCampanhaFormaPagamentoTO $vwcfpTO){
		$vwCampanhaFormaPagamentoORM = new VwCampanhaFormaPagamentoORM();
		$where = $vwCampanhaFormaPagamentoORM->montarWhereView($vwcfpTO, false, true);
		try{
			$obj = $vwCampanhaFormaPagamentoORM->fetchAll($where);
			return $obj;
		}catch (Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		}
	}

    /**
	 * Método que retorna a view de campanha com produtoo.
	 * @param VwCampanhaProduto $vwcpTO
	 * @see CampanhaComercialBO::retornarVwCampanhaProduto();
     * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function retornarVwCampanhaProduto(VwCampanhaProdutoTO $vwcpTO){
		$vwCampanhaProdutoORM = new VwCampanhaProdutoORM();
		$where = $vwCampanhaProdutoORM->montarWhereView($vwcpTO, false, true);
		try{
			$obj = $vwCampanhaProdutoORM->fetchAll($where);
			return $obj;
		}catch (Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		}
	}

    /**
	 * Método que retorna a view de campanha com forma de pagamento e produtoo.
	 * @param VwCampanhaFormaPagamentoProduto $vwcfppTO
	 * @see CampanhaComercialBO::retornarVwCampanhaFormaPagamentoProduto();
     * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function retornarVwCampanhaFormaPagamentoProduto(VwCampanhaFormaPagamentoProdutoTO $vwcfppTO){
		$vwCampanhaFormaPagamentoProdutoORM = new VwCampanhaFormaPagamentoProdutoORM();
		$where = $vwCampanhaFormaPagamentoProdutoORM->montarWhereView($vwcfppTO, false, true);
		try{
			$obj = $vwCampanhaFormaPagamentoProdutoORM->fetchAll($where);
			return $obj;
		}catch (Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		}
	}

    /**
	 * Método que retorna O desconto da Campanha
	 * @param CampanhaDescontoTO $cdTO
	 * @see CampanhaComercialBO::retornarCampanhaDesconto();
	 * @todo estudar a possibilidade de levar esse método para RO
     * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function retornarCampanhaDesconto(CampanhaDescontoTO $cdTO){
		$campanhaDescontoORM = new CampanhaDescontoORM();
		$where = $campanhaDescontoORM->montarWhere($cdTO);
		try{
			$obj = $campanhaDescontoORM->fetchAll($where);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		}
	}

    /**
	 * Método que retorna o tipo de desconto da campanha comercial
	 * @param TipoDescontoTO $tdTO
	 * @see CampanhaComercialBO::retornarTipoDesconto();
     * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function retornarTipoDesconto(TipoDescontoTO $tdTO){
		$tipoDescontoORM = new TipoDescontoORM();
		$where = $tipoDescontoORM->montarWhere($tdTO);
		try{
			$obj = $tipoDescontoORM->fetchAll($where);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		}
	}

    /**
	 * Método que retorna a campanha do convenio
	 * @param CampanhaConvenioTO $ccTO
	 * @see CampanhaComercialBO::retornarCampanhaConvenio();
     * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function retornarCampanhaConvenio(CampanhaConvenioTO $ccTO){
		$campanhaConvenioORM = new CampanhaConvenioORM();
		$where = $campanhaConvenioORM->montarWhere($ccTO);
		try{
			$obj = $campanhaConvenioORM->fetchAll($where);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		}
	}

    /**
	 * Método que retorna o perfil da campanha
	 * @param CampanhaPerfilTO $cpTO
	 * @see CampanhaComercialBO::retornarCampanhaConvenio();
     * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function retornarCampanhaPerfil(CampanhaPerfilTO $cpTO){
		$campanhaPerfilORM = new CampanhaPerfilORM();
		$where = $campanhaPerfilORM->montarWhere($cpTO);
		try{
			$obj = $campanhaPerfilORM->fetchAll($where);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		}
	}

    /**
	 * Método que retorna onde pertence a area do conhecimento com o projeto pedagógico da campanha
	 * @param CampanhaProjetoAreaTO $cpaTO
	 * @see CampanhaComercialBO::retornarCampanhaProjetoArea();
     * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function retornarCampanhaProjetoArea(CampanhaProjetoAreaTO $cpaTO){
		$campanhaProjetoAreaORM = new CampanhaProjetoAreaORM();
		$where = $campanhaProjetoAreaORM->montarWhere($cpaTO);
		try{
			$obj = $campanhaProjetoAreaORM->fetchAll($where);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		}
	}

    /**
	 * Metodo que retorna o tipo da campanha
	 * @param TipoCampanhaTO $tcTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoCampanha(TipoCampanhaTO $tcTO){
		try{
			$tipoCampanhaORM = new TipoCampanhaORM();
			$where = $tipoCampanhaORM->montarWhere($tcTO);
            $obj = $tipoCampanhaORM->fetchAll($where, 'id_tipocampanha desc');
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($e->getMessage());
			return false;
		}
	}
}

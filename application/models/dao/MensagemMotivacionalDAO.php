<?php
/**
 * Classe de persistência da mensagem motivacional ao banco de dados
 * @author Domício de Araújo Medeiros - domicio.medeiros@gmail.com
 * 
 * @package models
 * @subpackage dao
 */
class MensagemMotivacionalDAO extends Ead1_DAO {
	
	/**
	 * Método que cadastra as mensagens motivacionais.
	 * 
	 * @param MensagemMotivacionalTO $msg_motTO
	 * @return mixed|boolean
	 */
	public function cadastraMensagemMotivacional(MensagemMotivacionalTO $msgMotTO) {
		$mensagemMotivacionalORM = new MensagemMotivacionalORM();
		$this->beginTransaction();
		try{
			$dados = $msgMotTO->toArrayInsert();
			$id_mensagem = $mensagemMotivacionalORM->insert($dados);
			$this->commit();
			return $id_mensagem;
		}catch(Zend_Exception $e) {
			$this->rollBack();
			throw $e;
			return false;
		}
	}
	
	/**
	 * Método que realiza consultas das mensagens motivacionais.
	 * 
	 * @param MensagemMotivacionalTO $msgMotTO
	 * @param unknown_type $where
	 * @throws Zend_Exception
	 * @return Zend_Db_Table_Rowset | boolean
	 */
	public function retornaMensagemMotivacional(MensagemMotivacionalTO $msgMotTO, $where = null) {
		$mensagemMotivacionalORM = new MensagemMotivacionalORM();
		
		if(!$where) {
			$where = $mensagemMotivacionalORM->montarWhere($msgMotTO);
		}
		
		try {
			$obj = $mensagemMotivacionalORM->fetchAll($where);
			return $obj;
		} catch (Zend_Exception $e){
			throw $e;
		}
	}
	
	/**
	 * 
	 * @param MensagemMotivacionalTO $msgMotTO.
	 * @throws Zend_Exception
	 * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >
	 */
	public function retornaMensagemMotivacionalAleatoria(MensagemMotivacionalTO $msgMotTO, $where = null) {
		try {
			$msgMotivacionalORM = new MensagemMotivacionalORM();
			if($where == null){
				$where = $msgMotivacionalORM->montarWhere($msgMotTO);
			}

			$select = $msgMotivacionalORM->select()
				->from($msgMotivacionalORM->_name)
				->setIntegrityCheck(false)
				->order(new Zend_Db_Expr('NEWID()'))
				->limit(1);
			
			$resultado = $msgMotivacionalORM->fetchAll($select);
			
			return $resultado;
			
		} catch (Zend_Exception $e) {
			throw $e;
		}
	}
		
	
	/**
	 * Método que edita mensagem motivacional.
	 * 
	 * @param MensagemMotivacionalTO $msgMotTO
	 * @throws Zend_Exception
	 * @return boolean
	 */
	public function editarMensagemMotivacional(MensagemMotivacionalTO $msgMotTO) {
		
		try {
			$mensagemMotivacionalORM = new MensagemMotivacionalORM();
			$updateMensagem = $msgMotTO->toArrayUpdate(false, array('id_mensagemmotivacional'));
			$this->beginTransaction();
			$where = $mensagemMotivacionalORM->montarWhere($msgMotTO, true);
			$mensagemMotivacionalORM->update($updateMensagem, $where);
			$this->commit();
			return true;
		} catch (Zend_Exception $e) {
			$this->rollBack();
			throw $e;
			return false;
		}
		
	}
	
	/**
	 * Método que exclui mensagens motivacionais.
	 * 
	 * @param MensagemMotivacionalTO $msgMotTO
	 * @throws Zend_Exception
	 * @return boolean
	 */
	public function excluirMensagemMotivacional(MensagemMotivacionalTO $msgMotTO) {
		try {
			$this->beginTransaction();
			$orm = new MensagemMotivacionalORM();
			$where = $orm->montarWhere($msgMotTO);
			$orm->delete($where);
			$this->commit();
			return true;
		} catch (Zend_Exception $e) {
			$this->rollBack();
			throw $e;
			return false;
		}
	}
}
?>
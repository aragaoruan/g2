<?php
/**
 * Classe de persistência de área ao banco de dados
 * @author Dimas Sulz <dimassulz@gmail.com>
 *
 * @package models
 * @subpackage dao
 */
class AreaDAO extends Ead1_DAO{
	
	/**
	 * Método que cadastra a área do conhecimento
	 * @param AreaConhecimentoTO $acTO
	 * @return id_area|false 
	 */
	public function cadastrarArea(AreaConhecimentoTO $acTO){
		$areaConhecimentoORM = new AreaConhecimentoORM();
		$this->beginTransaction();
		$where = $acTO->toArrayInsert();
		try{
			$id_area = $areaConhecimentoORM->insert($where);
			$this->commit();
			return $id_area;
		}catch(Exception $e){
			$this->rollBack();
			$arr['TO'] = $acTO;
			$arr['WHERE'] = $where;
			$arr['SQL'] = $e->getMessage();
			$this->excecao = $arr;
			return false;
		}
	}
	
	/**
	 * Metodo que Cadastra Serie e Nivel de Ensino da Area
	 * @param AreaConhecimentoSerieNivelEnsinoTO $acsneTO
	 * @return boolean
	 */
	public function cadastrarAreaSerieNivel(AreaConhecimentoSerieNivelEnsinoTO $acsneTO){
		$areaConhecimentoSerieNivelEnsinoORM = new AreaConhecimentoSerieNivelEnsinoORM();
		$where = $acsneTO->toArrayInsert();
		$this->beginTransaction();
		try{
			$areaConhecimentoSerieNivelEnsinoORM->insert($where);
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			THROW $e;
			return false;
		}
	}
	
	/**
	 * Método que cadastra entidade de área.
	 * @param AreaEntidadeTO $aeTO
	 * @return boolean
	 */
	public function cadastrarAreaEntidade(AreaEntidadeTO $aeTO){
		try{
			$this->beginTransaction();
			$orm = new AreaEntidadeORM();
			$insert = $aeTO->toArrayInsert();
			$orm->insert($insert);
			$this->commit();
			return true;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($this->excecao);
			return false;
		}
		
	}
	
	/**
	 * Metodo que exclui entidades de área.
	 * @param AreaEntidadeTO $aeTO
	 * @return boolean
	 */
	public function excluirAreaEntidade(AreaEntidadeTO $aeTO){
		try{
			$this->beginTransaction();
			$orm = new AreaEntidadeORM();
			$where = $orm->montarWhere($aeTO);
			$orm->delete($where);
			$this->commit();
			return true;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($this->excecao);
			return false;
		}
	}
	
	
	/**
	 * Metodo que edita Area do conhecimento
	 * @param AreaConhecimentoSerieNivelEnsinoTO $acsneTO
	 * @return boolean
	 */
	public function editarArea(AreaConhecimentoTO $acTO){
		$areaConhecimentoORM = new AreaConhecimentoORM();
		$updateArea = $acTO->toArrayUpdate(false,array('id_areaconhecimento'));
		$this->beginTransaction();
		try{
			$areaConhecimentoORM->update($updateArea, " id_areaconhecimento = ".$acTO->getId_areaconhecimento());
			$this->commit();
			return true;
		}catch(Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Metodo que deleta Serie e Nivel de Ensino da Area
	 * @param AreaConhecimentoSerieNivelEnsinoTO $acsneTO
	 * @return boolean
	 */
	public function deletarAreaSerieNivel(AreaConhecimentoSerieNivelEnsinoTO $acsneTO){
		$areaConhecimentoSerieNivelEnsinoORM = new AreaConhecimentoSerieNivelEnsinoORM();
		$where = $areaConhecimentoSerieNivelEnsinoORM->montarWhere($acsneTO);
		$this->beginTransaction();
		try{
			if(!$where){
				THROW new Zend_Exception('O TO não pode vir vazio!');
			}
			$areaConhecimentoSerieNivelEnsinoORM->delete($where);
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW $e;
			return false;
		}
	}
	
	/**
	 * Método que retorna área do conhecimento
	 * @param AreaConhecimentoTO $acTO
	 * @return object|false Zend_Db_Table_Rowset_Abstract
	 */
	public function retornaArea(AreaConhecimentoTO $acTO, $where = null){
		$areaConhecimentoORM = new AreaConhecimentoORM();
		if(!$where){
			$where = $areaConhecimentoORM->montarWhere($acTO);
		}
		try{
			$obj = $areaConhecimentoORM->fetchAll($where, array("id_areaconhecimento","id_areaconhecimentopai"));
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($e->getMessage());
			return false;
		}
	}
	
	/**
	 * Método que retorna entidades da área.
	 * @param AreaEntidadeTO $aeTO
	 * @return object|false Zend_Db_Table_Rowset_Abstract
	 */
	public function retornarAreaEntidade(AreaEntidadeTO $aeTO, $where = null){
		$areaEntidadeORM = new AreaEntidadeORM();
		if(!$where){
			$where = $areaEntidadeORM->montarWhere($aeTO);
		}
		try{
			$obj = $areaEntidadeORM->fetchAll($where);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($e->getMessage());
			return false;
		}
	}
	
	/**
	 * Método que retorna as areas por entidade (AreaConhecimentoTO)
	 * @param EntidadeTO $to
	 * @return Zend_Db_Table_Rowset
	 */
	public function retornarAreasPorEntidade(EntidadeTO $to){
		try{
			$rowset = new Zend_Db_Table_Rowset(array());
			if(!$to->getId_entidade()){
				THROW new Zend_Exception('O id_entidade é obrigatório!');
			}
			$orm = new AreaConhecimentoORM();
			$select = $orm->select()
									->from(array('a' => 'tb_areaconhecimento'),array('*'))
									->join(array('ae' => 'tb_areaentidade'), 'a.id_areaconhecimento = ae.id_areaconhecimento',NULL)
									->where("a.bl_ativo =1 and ae.id_entidade = ?",$to->getId_entidade(),Zend_Db::PARAM_INT)
                                    ->order('a.st_areaconhecimento');
			$rowset = $orm->fetchAll($select);
		}catch (Zend_Exception $e){
			THROW $e;
		}
		return $rowset;
	}
	
	/**
	 * Metodo que Retorna as Areas que são Pais!
	 * @param AreaConhecimentoTO $acTO
	 * @return object|false Zend_Db_Table_Rowset_Abstract
	 */
	public function retornaAreaPai(AreaConhecimentoTO $acTO){
		$areaConhecimentoORM = new AreaConhecimentoORM();
		$where = ' id_entidade = '.$acTO->getId_entidade().' AND id_areaconhecimentopai is null';
		try{
			$obj = $areaConhecimentoORM->fetchAll($where);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Método que retorna a area do conhecimento a série e o nivel de ensino 
	 * @param AreaConhecimentoSerieNivelEnsinoTO $acsneTO
	 * @return object|false Zend_Db_Table_Rowset_Abstract
	 */
	public function retornaAreaSerieNivel(AreaConhecimentoSerieNivelEnsinoTO $acsneTO){
		$vwAreaConhecimentoSerieNivelEnsinoORM = new VwAreaConhecimentoNivelSerieORM();
		try{
			$obj = $vwAreaConhecimentoSerieNivelEnsinoORM->fetchAll('id_areaconhecimento = '.$acsneTO->getId_areaconhecimento());
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		}
	}
		
	/**
	 * Meotodo que retorna A Area e Projeto pedagogico
	 * @param VwProjetoAreaTO $paTO
	 * @return Ead1_Mensageiro
	 * @see AreaBO::retornaArrayProjetoArea
	 */
	public function retornaVwAreaProjetoPedagogico(VwProjetoAreaTO $paTO){
// 		return false;
		$vwProjetoAreaORM = new VwProjetoAreaORM();
		$where = $vwProjetoAreaORM->montarWhere($paTO); 
		try{
			$obj = $vwProjetoAreaORM->fetchAll($where, 'st_projetopedagogico');
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($e->getMessage());
			return false;
		}
	}
	
	
	/**
	 * Retorna os tipos de área de conhecimento existentes
	 * @param TipoAreaConhecimentoTO $tipoAreaConhecimentoTO
	 * @return Zend_Db_Table_Rowset_Abstract
	 */
	public function retornarTipoAreaConhecimento( TipoAreaConhecimentoTO $tipoAreaConhecimentoTO ) {
		$tipoAreaConhecimentoORM	= new TipoAreaConhecimentoORM();
		return $tipoAreaConhecimentoORM->fetchAll( $tipoAreaConhecimentoORM->montarWhere( $tipoAreaConhecimentoTO ) );
	}
}
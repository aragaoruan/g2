<?php

/**
 * Classe de persistencia da ocorrencia com o banco de dados
 * @author Denise Xavier <denisexavier@ead1.com.br>
 * @package models
 * @subpackage dao
 */
class CAtencaoDAO extends Ead1_DAO
{
    /**
     * Método que cadastra(insert) uma Ocorrencia
     * @param OcorrenciaTO $ocorrenciaTO
     * @see
     * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
     */

    public function cadastrarOcorrencia(OcorrenciaTO $ocorrenciaTO)
    {
        $ocorrenciaORM = new OcorrenciaORM();
        $this->beginTransaction();
        try {
            $insert = $ocorrenciaTO->toArrayInsert();
            $dt_cadastro = new Zend_Date(date('d-m-Y H:i:s'), Zend_Date::DATETIME, 'pt_BR');
            $insert['dt_cadastro'] = $dt_cadastro->toString('Y-MM-d H:m:s', null, 'pt_BR');
            $insert['dt_cadastroocorrencia'] = $dt_cadastro->toString('Y-MM-d H:m:s', null, 'pt_BR');
            $id = $ocorrenciaORM->insert($insert);

            //TRACER AC-27605
            if ((int)$ocorrenciaTO->getId_entidade() == 352) {
                if (extension_loaded('newrelic')) {
                    $trace = \Zend_Json::encode(array('POST' => $insert, 'PERSIST' => $ocorrenciaORM->find($id)->current()->toArray()));
                    newrelic_capture_params(true);
                    newrelic_notice_error($trace);
                }
            }

            $this->commit();
            return $id;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que salva um arquivo para tramite da ocorrencia ou da interacao
     * @param UploadTO $imagem
     * @throws Zend_Exception
     * @return Ambigous <mixed, multitype:>|boolean
     */
    public function salvarImagemUpload(UploadTO $imagem)
    {
        $dao = new UploadDAO();
        $this->beginTransaction();
        try {
            $id = $dao->cadastrarUpload($imagem);
            $this->commit();
            return $id;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que atualiza o id_upload no tramite caso tenha sido enviado algum arquivo
     * @param TramiteTO $tramite
     * @throws Zend_Exception
     */
    public function updateTramite(TramiteTO $tramite)
    {
        $this->beginTransaction();
        try {
            $orm = new TramiteORM();
            $orm->update($tramite->toArrayUpdate(true, array('id_tramite', 'dt_cadastro', 'id_usuario')), $orm->montarWhere($tramite, true));
            $this->commit();
        } catch (Zend_Exception $e) {
            $this->rollBack();
            throw $e;
        }
    }


    /**
     * Cadastra TramiteOcorrencia
     * @param TramiteOcorrenciaTO $tramiteOcorrencia
     * @throws Zend_Exception
     * @return Ambigous <mixed, multitype:>
     */
    public function salvarTramiteOcorrencia(TramiteOcorrenciaTO $tramiteOcorrencia)
    {
        $this->beginTransaction();
        try {
            $orm = new TramiteOcorrenciaORM();
            $insert = $tramiteOcorrencia->toArrayInsert();
            $id = $orm->insert($insert);
            $this->commit();
            return $id;
        } catch (\Exception $e) {
            $this->rollBack();
            throw $e;
        }

    }

    /**
     * Retorna Ocorrencias
     * @param VwOcorrenciaTO $vwOcorrencia
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function retornaOcorrencias(VwOcorrenciaTO $vwOcorrencia)
    {
        $ocorrenciaORM = new VwOcorrenciaORM();
        $order = array(
            'dt_cadastro DESC',
            'dt_ultimotramite'
        );
        return $ocorrenciaORM->fetchAll($ocorrenciaORM->montarWhereView($vwOcorrencia, false, true), $order);

    }

    /**
     * Vincula um responsavel a ocorrencia tb_responsavelocorrencia
     * @param OcorrenciaResponsavelTO $to
     * @throws Zend_Exception
     * @return Ambigous <mixed, multitype:>
     */
    public function vincularResponsavelOcorrencia(OcorrenciaResponsavelTO $to)
    {
        $this->beginTransaction();
        try {
            $orm = new OcorrenciaResponsavelORM();
            $insert = $to->toArrayInsert();
            $id = $orm->insert($insert);
            $this->commit();
            return $id;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            throw $e;
        }
    }

    /**
     * Método que atualiza responsavel de uma ocorrencia -[Deleta logicamente um atendente]
     * @param OcorrenciaResponsavelTO $to
     * @throws Zend_Exception
     * @return boolean|Zend_Exception
     */
    public function updateResponsavelOcorrencia(OcorrenciaResponsavelTO $to)
    {
        $this->beginTransaction();
        try {
            $orm = new OcorrenciaResponsavelORM();

            if (((int)$to->getId_ocorrenciaresponsavel()) == false) {
                throw new Zend_Exception('O id_ocorrenciaresponsavel é obrigatório e não foi passado!');
            }

            $where = trim($orm->montarWhere($to, true));
            $orm->update($to->toArrayUpdate(false, array('id_ocorrenciaresponsavel', 'id_ocorrencia', 'id_usuario')), $where, true);
            $this->commit();
            return true;
        } catch (Exception $e) {
            $this->rollBack();
            throw $e;
            return $e;
        }
    }


    /**
     * Método que atualiza ocorrencia
     * @param OcorrenciaTO $to
     * @return bool|Exception
     * @throws Exception
     */
    public function updateOcorrencia(OcorrenciaTO $to)
    {
        try {
            $this->beginTransaction();
            if (((int)$to->getId_ocorrencia()) == false) {
                throw new Zend_Exception('O id_ocorrencia é obrigatório e não foi passado!');
            }

            //Validando Objeto para ter id_ocorrencia e no minimo mais um parametro
            $ocorrenciaTOArray = get_object_vars($to);
            unset($ocorrenciaTOArray['id_ocorrencia']);

            if (implode("", $ocorrenciaTOArray) == "") {
                throw new Zend_Exception('O Objeto não esta tem paramentros setados suficiente para realizar update!');
            }

            $orm = new OcorrenciaORM();
            $id = $orm->update($to->toArrayUpdate(false, array('id_ocorrencia')), $orm->montarWhere($to, true));

            //TRACER AC-27605
            if ((int)$to->getId_entidade() == 352) {
                if (extension_loaded('newrelic')) {
                    $trace = \Zend_Json::encode(array('POST' => $to->toArray(), 'PERSIST' => $orm->find($id)->current()->toArray()));
                    newrelic_capture_params(true);
                    newrelic_notice_error($trace);
                }
            }

            $this->commit();
            return true;
        } catch (\Exception $e) {
            $this->rollBack();
            throw $e;
            return $e;
        }
    }


    /**
     * @param VwNucleoPessoaCoTO $nucleoPessoa
     * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >
     */
    public function retornaNucleoFuncoesCA(VwNucleoPessoaCoTO $nucleoPessoa)
    {
        $ORM = new VwNucleoPessoaCoORM();
        $select = $ORM->select()->distinct(true)->from(array('vw_nucleopessoaco'), array('st_nucleoco', 'id_nucleoco', 'id_usuario', 'id_funcao', 'st_nomecompleto', 'st_funcao', 'id_textonotificacao'))
            ->where('id_entidade = ' . $nucleoPessoa->getId_entidade())
            ->where('id_usuario = ' . $nucleoPessoa->getId_usuario());
        return $ORM->fetchAll($select);
    }


    /**
     * Retorna Atendentes de um núcleo
     * @param VwNucleoPessoaCoTO $nucleoPessoa
     * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >
     */
    public function retornaAtendentes(VwNucleoPessoaCoTO $nucleoPessoa)
    {
        $orm = new VwNucleoPessoaCoORM();
        $select = $orm->select()->distinct(true)->from(array('vw_nucleopessoaco'), array('st_nucleoco', 'id_nucleoco', 'id_usuario', 'id_funcao', 'st_nomecompleto', 'st_funcao', 'id_textonotificacao'))
            ->where('id_funcao in (' . VwNucleoPessoaCoTO::FUNCAO_ATENDENTE . ', ' . VwNucleoPessoaCoTO::FUNCAO_ATENDENTE_SETOR . ')')
            ->where('id_nucleoco = ' . $nucleoPessoa->getId_nucleoco());
        return $orm->fetchAll($select);
    }

    /**
     * Método que retorna ocorrencias pendentes para o usuário
     * @param VwOcorrenciaAguardandoTO $vwOcorrenciaAguardandoTO
     * @see CAtencaoBO::retornaVwOcorrenciaAguardando();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwOcorrenciaAguardando(VwOcorrenciaAguardandoTO $vwOcorrenciaAguardandoTO)
    {
        $vwOcorrenciaAguardandoORM = new VwOcorrenciaAguardandoORM();
        $where = $vwOcorrenciaAguardandoORM->montarWhere($vwOcorrenciaAguardandoTO);
        try {
            $obj = $vwOcorrenciaAguardandoORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna ocorrencias pendentes para o usuário
     * @param VwOcorrenciasPessoaTO $vwOcorrenciasPessoaTO
     * @see CAtencaoBO::retornaVwOcorrenciasPessoa();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwOcorrenciasPessoa(VwOcorrenciasPessoaTO $vwOcorrenciasPessoaTO)
    {
        $vwOcorrenciasPessoaORM = new VwOcorrenciasPessoaORM();
        $where = $vwOcorrenciasPessoaORM->montarWhere($vwOcorrenciasPessoaTO);
        try {
            $obj = $vwOcorrenciasPessoaORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Retorna ocorrencias g2
     * @param VwOcorrenciasPessoaTO $to
     * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >
     */
    public function listarOcorrenciasPessoa(VwOcorrenciasPessoaTO $to, $dt_inicial = null, $dt_final = null)
    {
        $orm = new VwOcorrenciasPessoaORM();
        $or = '';
        if ($to->getId_ocorrencia() || $to->getSt_nomeinteressado()) {
            $or = " and (st_nomeinteressado like '%";
            $or .= $to->getSt_nomeinteressado();
            $or .= "%'COLLATE SQL_Latin1_General_CP1_CI_AI or st_emailinteressado like '%";
            $or .= $to->getSt_emailinteressado();
            $or .= "%'COLLATE SQL_Latin1_General_CP1_CI_AI or id_ocorrencia = " . $to->getId_ocorrencia() . ")";
        }
        $andSituacao = '';
        if ($to->getId_situacao() == OcorrenciaTO::SITUACAO_NAO_ENCERRADA) {
            $andSituacao = " and id_situacao in (" . OcorrenciaTO::SITUACAO_ENCERRADA . "," . OcorrenciaTO::SITUACAO_AGUARDANDO_ENCERRAMENTO . ")";
        } else if ($to->getId_situacao()) {
            $andSituacao = " and id_situacao = " . $to->getId_situacao();
        }

        $data = '';
        if (!empty($dt_final) && !empty($dt_inicial)) {
            $dt_inicial = $to->trataDataBancoBR($dt_inicial);
            $dt_final = $to->trataDataBancoBR($dt_final);
            $data = " and dt_cadastroocorrencia between '" . $dt_inicial . "' and '" . $dt_final . "'";
        }

        $dataAtendimento = '';
        if ($to->getDt_atendimento()) {
            $dataAtendimento = " and dt_atendimento <= '" . $to->trataDataBancoBR($to->getDt_atendimento()) . "'";
        }
        $funcao = '';
        if ($to->getId_tipoocorrencia() == TipoOcorrenciaTO::ALUNO && ($to->getId_funcao() == FuncaoTO::ATENDENTE_CA || $to->getId_funcao() == FuncaoTO::RESPONSAVEL_CA)) {
            $funcao = " and nu_ordem in (1 , 2 , 3 ) ";
        }


        $where = $orm->montarWhereView($to, array('id_evolucao', 'id_funcao', 'id_atendente', 'id_assuntoco', 'id_assuntocopai', 'id_usuario', 'id_nucleoco'/*,'id_situacao'*/), true, true) . $or . $data . $andSituacao . $dataAtendimento . $funcao;
        return $orm->fetchAll($where, array("nu_ordem ASC", "nu_horastraso DESC"));
    }


    /**
     * Método que retorna as evoluções vinculadas a função de um atendente no nucleo e a evolucao da ocorrência detalhada
     * @param VwOcorrenciaEvolucao $to
     * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >
     */
    public function listarEvolucoesAtendimentoOcorrencia(VwOcorrenciaEvolucaoTO $to)
    {
        $orm = new VwOcorrenciaEvolucaoORM();
        $select = $orm->select()->distinct(true)->from(array('vw_ocorrenciaevolucao'), array('id_evolucao', 'st_evolucao'))
            ->where('id_ocorrencia = ' . $to->getId_ocorrencia() . ' AND id_funcao is null')
            ->orWhere('id_funcao = ' . $to->getId_funcao() . ' AND id_ocorrencia is null and id_nucleoco = ' . $to->getId_nucleoco());

        return $orm->fetchAll($select);
    }


    /**
     * Retorna atendentes de acordo com o assunto para encaminhar uma ocorência
     * (todos que tem a funcao de atendente ou atendente de setor para o assunto selecionado)
     * @param VwNucleoPessoaCoTO $nucleoPessoa
     * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >
     */
    public function retornaAtendentesEncaminharOcorrencia(VwNucleoPessoaCoTO $nucleoPessoa)
    {
        $orm = new VwNucleoPessoaCoORM();
        $select = $orm->select()->where('id_entidade = ?', $nucleoPessoa->getId_entidade())
            ->where('id_assuntoco = ?', $nucleoPessoa->getId_assuntoco());
        return $orm->fetchAll($select);
    }


    /**
     * Retorna a VwNucleoAssuntoCo com o id_textonotificao para enviar notificao ao usuario na nova interacao
     * @param VwNucleoAssuntoCoTO $vwnucleoassunto
     * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >
     */
    public function retornaTextoNotificaoCA(VwNucleoAssuntoCoTO $vwnucleoassunto)
    {
        $orm = new VwNucleoAssuntoCoORM();
        $select = $orm->select()->where('id_nucleoassuntoco is not null and id_assuntoco = ?', $vwnucleoassunto->getId_assuntoco())
            ->where('id_entidade = ? ', $vwnucleoassunto->getId_entidade());
        return $orm->fetchRow($select);
    }

    /**
     * Método que busca o id_emailconfig para enviar uma notificação ao usuario na nova interação
     * @param VwNucleoAssuntoCoTO $vwnucleoassunto
     * @return Ambigous <Zend_Db_Table_Row_Abstract, NULL, unknown>
     */
    public function retornarIdEmailConfiguracao(VwNucleoAssuntoCoTO $vwnucleoassunto)
    {
        $orm = new VwNucleoAssuntoCoORM();
        $select = $orm->select()->from(array('ne' => 'vw_nucleocoemail'),
            array('id_nucleoco', 'id_emailconfig')
        )
            ->setIntegrityCheck(false)
            ->where('ne.id_nucleoco = ? ', $vwnucleoassunto->getId_nucleoco())
            ->limit(1);
        return $orm->fetchRow($select);
    }


    /**
     * @param MatriculaTO $to
     * @param MatriculaTO $to
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function retornaOcorrenciaMatricula(MatriculaTO $to)
    {
        $orm = new OcorrenciaORM();
        $select = $orm->select()->from(array('ac' => 'tb_assuntoco'),
            array()
        )->setIntegrityCheck(false)
            ->join(array('oc' => 'tb_ocorrencia'), 'ac.id_assuntoco = oc.id_assuntoco AND (bl_cancelamento = 1 OR bl_trancamento = 1)')
            ->where('oc.id_matricula = ? ', $to->getId_matricula())
            ->where('oc.id_evolucao = ? ', OcorrenciaTO::EVOLUCAO_ENCERRAMENTO_PELO_ATENDENTE);
        return $orm->fetchAll($select);
    }


    /**
     * Retorna as interacoes de uma ocorrencia
     * @param VwOcorrenciaInteracaoTO $to
     * @return Ambigous <Zend_Db_Table_Row_Abstract, NULL, unknown>
     */
    public function retornaVwOcorrenciaInteracao(VwOcorrenciaInteracaoTO $to)
    {
        $orm = new VwOcorrenciaInteracaoORM();
        $select = $orm->select()->where('id_ocorrencia = ?', $to->getId_ocorrencia())
            ->order('dt_cadastro desc');
        return $orm->fetchAll($select);
    }
}

?>

<?php
/**
 * Classe de acesso a dados referentes a assunto
 * @package models
 * @subpackage dao
 */
class AssuntoCoDAO extends Ead1_DAO {

	/**
	 * Método que insere o assunto
	 * 
	 * @see AssuntoCoBO::salvarAssunto();
	 * 
	 * @param AssuntoCoTO $assuntoCoTo
	 * @throws Zend_Exception
	 * @return Ambigous <mixed, multitype:>
	 */
	public function cadastrarAssunto(AssuntoCoTO $assuntoCoTo){
		$this->beginTransaction();
		try{
			$tbAssunto = new AssuntoCoORM();
			$idAssunto = $tbAssunto->insert($assuntoCoTo->toArrayInsert());
			$this->commit();
			return $idAssunto;
		}catch(Zend_Exception $e){
			$this->rollBack();
			throw $e;
		}	
	}
	
	/**
	 * Método de edição do assunto
	 * @param AssuntoCoTO $assuntoCoTo
	 * @throws Zend_Exception
	 */
	public function editarAssunto(AssuntoCoTO $assuntoCoTo){
		$this->beginTransaction();
		try {
			$tbAssunto = new AssuntoCoORM();
			$tbAssunto->update($assuntoCoTo->toArrayUpdate(true, array('id_assuntoco')), $tbAssunto->montarWhere($assuntoCoTo, true));
			$this->commit();
		} catch (Zend_Exception $e) {
			$this->rollBack();
			throw $e;
		}
	}
	
	/**
	 * Método de listagem de assnto
	 * @param AssuntoCoTO $assuntoCoTo
	 */
	public function listarAssunto(AssuntoCoTO $assuntoCoTo){
		$tbAssunto = new AssuntoCoORM();
		return $tbAssunto->consulta($assuntoCoTo);
	}
	
	/**
	 * Método de vínculo de assunto com entidade
	 * @param AssuntoEntidadeCoTO $assuntoEntidadeCoTO
	 * @throws Zend_Exception
	 */
	public function cadastrarAssuntoEntidadeCo(AssuntoEntidadeCoTO $assuntoEntidadeCoTO){
		$this->beginTransaction();
		try{
			$orm = new AssuntoEntidadeCoORM();
			$orm->insert($assuntoEntidadeCoTO->toArrayInsert());
			$this->commit();
		}catch(Zend_Exception $e){
			$this->rollBack();
			throw $e;
		}	
	}
	
	/**
	 * Método que deleta o assunto entidade
	 * @see AssuntoCoBO::salvarArAssuntoEntidade();
	 * 
	 * @param integer $idAssuntoCo
	 * @throws Zend_Exception
	 */
	public function deletarAssuntoEntidadeCoPorAssunto($idAssuntoCo){
		$this->beginTransaction();
		try{
			$orm = new AssuntoEntidadeCoORM();
			$orm->delete("id_assuntoco = $idAssuntoCo");
			$this->commit();
		}catch(Zend_Exception $e){
			$this->rollBack();
			throw $e;
		}	
	}
	
	/**
	 * Método que retorna as entidades do assunto em questão
	 *
	 * @see AssuntoCoRO::retornarArAssuntoEntidade()
	 * @param AssuntoCoTO $assuntoTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarArAssuntoEntidade(AssuntoCoTO $assuntoTO){
		$orm = new AssuntoEntidadeCoORM();
		return $orm->fetchAll("id_assuntoco = " . $assuntoTO->getId_assuntoco());
	}
	
	/**
	 * Método que retorna os assuntos de uma certa entidade e que são do tipo ALUNO
	 *
	 * 
	 * @param AssuntoCoTO $assuntoTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarAssuntosTipoAluno(AssuntoCoTO $assuntoTO, array $where = null){
		try{
			$orm = new AssuntoCoORM();
			if(!$where){
				$where = $orm->montarWhere($assuntoTO);
			}
			return $orm->fetchAll($where);
		}catch (Zend_Exception $e){
			return $e->getMessage();
		}
	}
	
	/**
	 * @param VwAssuntoCoTo $VwAssuntoCoTo
	 * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >
	 * @see AssuntoCoBO::retornaVwAssuntoCoTo()
	 */
	public function retornaVwAssuntoCoPai(VwAssuntoCoTo $VwAssuntoCoTo){
		$orm = new VwAssuntoCoORM();
		return $orm->fetchAll($orm->montarWhereView($VwAssuntoCoTo, false, true, false)." and id_assuntocopai is null ");
		
	}
	
	/**
	 * @param VwAssuntoCoTo $VwAssuntoCoTo
	 * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >
	 * @see AssuntoCoBO::retornaVwAssuntoCoTo()
	 */
// 	public function retornaVwAssuntoCo(VwAssuntoCoTo $VwAssuntoCoTo){
// 		$orm = new VwAssuntoCoORM();
// 		return $orm->fetchAll($orm->montarWhereView($VwAssuntoCoTo, false, true, false));
	
// 	}
	
	
	/**
	 * @param AssuntoCoTO $assuntoCo
	 * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >
	 * @see AssuntoCoBO::retornaTipoAssunto
	 */
	public function retornaTipoOcorrencia(TipoOcorrenciaTO $assuntoCo){
			$orm = new TipoOcorrenciaORM();
			return $orm->fetchAll();
		}
		
		
		
	/**
	 * Método que deleta fisicamente um assunto
	 * @param AssuntoCoTO $assuntoCoTO
	 * @throws Zend_Exception
	 * @see AssuntoCoBO::deletaAssuntoCo()
	 */
	public function deletarAssuntoCo(AssuntoCoTO $assuntoCoTO){
		try {
			$this->beginTransaction();
				$orm = new AssuntoCoORM();
				$orm->delete('id_assuntoco = '.$assuntoCoTO->getId_assuntoco());
			$this->commit();
		} catch (Zend_Exception $e) {
			$this->rollBack();
				throw $e;
		}
		
	}	

    /**
     * @param VwAssuntosTo $to
     * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >
     * @see PortalCAtencaoBO::retornaComboAssuntoPai()
     */
    public function retornaVwAssuntos(VwAssuntosTO $to, $where = null){
        $orm = new VwAssuntosORM();
        return $orm->fetchAll($orm->montarWhereView($to, false, true, false).$where, 'st_assuntoco');

    }

        /**
         * Método responsavel por retornar VwAssuntoEntidade
         * @param VwAssuntoEntidadeTO $vwAssuntoEntidadeTO
         * @return fetchAll
         *
         * @author Rafael Bruno (RBD) <rafaelbruno.ti@gmail.com>
         */
        public function retornarVwAssuntoEntidade(VwAssuntoEntidadeTO $vwAssuntoEntidadeTO) {
            $orm = new VwAssuntoEntidadeORM();
            $query = $orm->select()
                        ->where($orm->montarWhere($vwAssuntoEntidadeTO))
                        ->order('id_assunto');

            return $orm->fetchAll($query);
        }

}
?>
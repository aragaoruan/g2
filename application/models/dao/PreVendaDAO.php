<?php
/**
 * Classe de persistência de pré-venda ao banco de dados
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage dao
 */
class PreVendaDAO extends Ead1_DAO {

	
	/**
	 * Metodo que cadastra uma pre venda
	 * @param PreVendaTO $pvTO
	 * @see PreVendaBO::cadastrarPreVenda
	 * @return int $id_prevenda | false
	 */
	public function cadastrarPreVenda(PreVendaTO $pvTO){
		try{
			$this->beginTransaction();
			$orm = new PreVendaORM();
			$insert = $pvTO->toArrayInsert();
			$id_prevenda = $orm->insert($insert);
			$this->commit();
			return $id_prevenda;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($this->excecao);
			return false;
		}
	}
	
	/**
	 * Metodo que edita uma pre venda
	 * @param PreVendaTO $pvTO
	 * @param Mixed $where
	 * @see PreVendaBO::editarPreVenda
	 * @return boolean
	 */
	public function editarPreVenda(PreVendaTO $pvTO, $where = null){
		try{
			$this->beginTransaction();
			$orm = new PreVendaORM();
			if(!$where){
				$where = $orm->montarWhere($pvTO,true);
			}
			$update = $pvTO->toArrayUpdate(false,array('id_prevenda','id_entidade'));
			$orm->update($update,$where);
			$this->commit();
			return true;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($this->excecao);
			return false;
		}
	}
	
	/**
	 * Metodo que cadastra pre venda com produto valor
	 * @param PreVendaProdutoValorTO $pvpvTO
	 * @see PreVendaBO::cadastrarPreVendaProdutoValor
	 * @return boolean
	 */
	public function cadastrarPreVendaProdutoValor(PreVendaProdutoValorTO $pvpvTO){
		try{
			$this->beginTransaction();
			$orm = new PreVendaProdutoValorORM();
			$insert = $pvpvTO->toArrayInsert();
			$orm->insert($insert);
			$this->commit();
			return true;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($this->excecao);
			return false;
		}
	}
	
	/**
	 * Metodo que cadastra pre venda com produto valor
	 * @param PreVendaProdutoTO $pvpTO
	 * @see PreVendaBO::cadastrarPreVendaProduto
	 * @return boolean
	 */
	public function cadastrarPreVendaProduto(PreVendaProdutoTO $pvpTO){
		try{
			$this->beginTransaction();
			$orm = new PreVendaProdutoORM();
			$insert = $pvpTO->toArrayInsert();
			$orm->insert($insert);
			$this->commit();
			return true;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($this->excecao);
			return false;
		}
	}
	
	/**
	 * Metodo que retorna pre venda com produto valor
	 * @param VwPreVendaProdutoValorTO $pvpvTO
	 * @see PreVendaBO::retornarPreVendaProdutoValor
	 * @return $obj | false
	 */
	public function retornarPreVendaProdutoValor(VwPreVendaProdutoValorTO $pvpvTO){
		$orm = new VwPreVendaProdutoValorORM();
		$where = $orm->montarWhereView($pvpvTO, false, true);
		try{
			$obj = $orm->fetchAll($where);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		} 
	}
	
	/**
	 * Metodo que retorna pre venda com produto
	 * @param VwPreVendaProdutorTO $pvpTO
	 * @see PreVendaBO::retornarPreVendaProduto
	 * @return $obj | false
	 */
	public function retornarPreVendaProduto(VwPreVendaProdutoTO $pvpTO){
		$orm = new VwPreVendaProdutoORM();
		$where = $orm->montarWhereView($pvpTO, false, true);
		try{
			$obj = $orm->fetchAll($where);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		} 
	}
	
	/**
	 * Metodo que retonna pre venda.
	 * @param VwPreVendaProdutoValorTO $pvTO
	 * @param Mixed $where
	 * @see PreVendaBO::retornarPreVenda
	 * @return $obj | false
	 */
	public function retornarPreVenda(PreVendaTO $pvTO,$where = null){
		try{
			$orm = new PreVendaORM();
			if(!$where){
				$where = $orm->montarWhere($pvTO);
			}
			return $orm->fetchAll($where);
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($e->getMessage());
			return false;
		} 
	}
	
	/**
	 * Método que retorna as pre vendas disponiveis ao distribuidor logado
	 * @param VwPrevendaDistribuicaoTO $to
	 * @param Mixed $where
	 * @see PreVendaBO::retornarPreVenda
	 * @return Zend_Db_Table_Row_Abstract
	 */
	public function retornarVwPreVendaDistribuicao(VwPrevendaDistribuicaoTO $to,$where = null){
		try{
			$orm = new VwPrevendaDistribuicaoORM();
			if(!$where){
				$where = $orm->montarWhereView($to,false,true,false);
			}
			return $orm->fetchAll($where);
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			THROW $e;
			return false;
		} 
	}
	
	/**
	 * Metodo que exclui pre venda com produto valor
	 * @param PreVendaProdutoValorTO $pvpvTO
	 * @see PreVendaBO::excluirPreVendaProdutoValor
	 * @return boolean
	 */
	public function excluirPreVendaProdutoValor(PreVendaProdutoValorTO $pvpvTO){
		try{
			$this->beginTransaction();
			$orm = new PreVendaProdutoValorORM();
			$where = $orm->montarWhere($pvpvTO);
			$orm->delete($where);
			$this->commit();
			return true;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($this->excecao);
			return false;
		}
	}
	
	/**
	 * Metodo que exclui pre venda com produto
	 * @param PreVendaProdutoTO $pvpTO
	 * @see PreVendaBO::excluirPreVendaProduto
	 * @return boolean
	 */
	public function excluirPreVendaProduto(PreVendaProdutoTO $pvpTO){
		try{
			$this->beginTransaction();
			$orm = new PreVendaProdutoORM();
			$where = $orm->montarWhere($pvpTO);
			$orm->delete($where);
			$this->commit();
			return true;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($this->excecao);
			return false;
		}
	}
	
}

?>
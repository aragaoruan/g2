<?php
/**
 * Classe DAO para informações relativas a relatório
 * @author edermariano
 *
 * @package models
 * @subpackage dao
 */
class RelatorioDAO extends Ead1_DAO{

	/**
	 * Método que insere o Relatório e retorna seu id
	 * @param RelatorioTO $relatorioTO
	 * @return mixed
	 */
	public function cadastrarRelatorio(RelatorioTO $relatorioTO){
		$this->beginTransaction();
		try{
			$tbRelatorios = new RelatorioORM();
			$idRelatorio = $tbRelatorios->insert($relatorioTO->toArrayInsert());
			$this->commit();
			return $idRelatorio;
		}catch(Zend_Exception $e){
			$this->rollBack();
			throw $e;
			return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	}

	/**
	 * Método que insere o Relatório e retorna seu id
	 * @param RelatorioTO $relatorioTO
	 * @return mixed
	 */
	public function atualizarRelatorio(RelatorioTO $relatorioTO){
		$this->beginTransaction();
		try{
			$tbRelatorios = new RelatorioORM();
			$tbRelatorios->update($relatorioTO->toArrayUpdate(false, array('id_relatorio', 'dt_cadastro')), $tbRelatorios->montarWhere($relatorioTO, true));
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			throw $e;
			return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	}

	/**
	 * Método que insere o campo de um relatório e retorna seu id
	 * @param CampoRelatorioTO $campoRelatorioTO
	 * @return mixed
	 */
	public function cadastrarCampoRelatorio(CampoRelatorioTO $campoRelatorioTO){
		$this->beginTransaction();
		try{
			$tbCampoRelatorio = new CampoRelatorioORM();
			$idCampoRelatorio = $tbCampoRelatorio->insert($campoRelatorioTO->toArrayInsert());
			$this->commit();
			return $idCampoRelatorio;
		}catch(Zend_Exception $e){
			$this->rollBack();
			throw $e;
			return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	}

	/**
	 * Método que insere a propriedade de um campo de um relatório e retorna seu id
	 * @param PropriedadeCampoRelatorioTO $propriedadeCampoRelatorioTO
	 * @return mixed
	 */
	public function cadastrarPropriedadeCampoRelatorio(PropriedadeCampoRelatorioTO $propriedadeCampoRelatorioTO){
		$this->beginTransaction();
		try{
			$tbPropriedadeCampoRelatorio = new PropriedadeCampoRelatorioORM();
			$idPropriedadeCampoRelatorio = $tbPropriedadeCampoRelatorio->insert($propriedadeCampoRelatorioTO->toArrayInsert());
			$this->commit();
			return $idPropriedadeCampoRelatorio;
		}catch(Zend_Exception $e){
			$this->rollBack();
			throw $e;
			return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	}

	/**
	 *  O usuário poderá parametrizar um relatório com alguns dados
	 *   esses dados podem ser armazenados e gerar este mesmo relatório no futuro com os mesmos dados.
	 *  Isso é realizado por intermédio de um hash que é uma combinação das chaves e valores com o id do relatório
	 *   e o id do usuário gerador e realizada uma conversão de base para a base64 e armazenado.
	 *  Este método retorna essas configurações específicas de um usuário.
	 */
	public function retornarConfiguracaoRelatorio($hash){
	}

	/**
	 * Método que retorna os tipos de origem para o relatório
	 * @param TipoOrigemRelatorioTO $tipoOrigemRelatorioTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoOrigemRelatorio(TipoOrigemRelatorioTO $tipoOrigemRelatorioTO = null){
		if(!$tipoOrigemRelatorioTO){
			$tipoOrigemRelatorioTO = new TipoOrigemRelatorioTO();
		}
		return new Ead1_Mensageiro(Ead1_ORM::instance(new TipoOrigemRelatorioORM())->consulta($tipoOrigemRelatorioTO));
	}

	/**
	 * Método que retorna as propriedades para o relatório
	 * @param TipoPropriedadeCampoRelTO $tipoPropriedadeCampoRelTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarPropriedadesRelatorio(TipoPropriedadeCampoRelTO $tipoPropriedadeCampoRelTO = null){
		if(!$tipoPropriedadeCampoRelTO){
			$tipoPropriedadeCampoRelTO = new TipoPropriedadeCampoRelTO();
		}
		return new Ead1_Mensageiro(Ead1_ORM::instance(new TipoPropriedadeCampoRelORM())->consulta($tipoPropriedadeCampoRelTO));
	}

	/**
	 * Método que retorna as possibilidades de filtros para o relatório
	 */
	public function retornarFiltros(){

	}

	/**
	 * Método que retorna os dados de acordo com uma configuração
	 * @param $vwDadosRelatorioTO
	 */
	public function retornarVwDadosRelatorio(VwDadosRelatorioTO $vwDadosRelatorioTO){
		$vwDadosRelatorioORM = new VwDadosRelatorioORM();
		return $vwDadosRelatorioORM->consulta($vwDadosRelatorioTO, false, false, 'nu_ordem ASC');
	}

	/**
	 * Método que retorna relatório
	 * @param RelatorioTO $relatorioTO
	 * @see RelatorioBO::retornarRelatorio();
	 * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function retornarRelatorio(RelatorioTO $relatorioTO){
		$relatorioORM = new RelatorioORM();
		$where = $relatorioORM->montarWhere($relatorioTO);


		try{
			$obj = $relatorioORM->fetchAll($where);

			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		}
	}

	/**
	 * Método que retorna campos de relatório
	 * @param CampoRelatorioTO $campoRelatorioTO
	 * @see RelatorioBO::retornarCampoRelatorio();
	 * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function retornarCampoRelatorio(CampoRelatorioTO $campoRelatorioTO){
		$campoRelatorioORM = new CampoRelatorioORM();
		$where = $campoRelatorioORM->montarWhere($campoRelatorioTO);
		try{
			$obj = $campoRelatorioORM->fetchAll($where);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		}
	}

	/**
	 * Método que retorna propriedade de campo de relatório
	 * @param PropriedadeCampoRelatorioTO $propriedadeCampoRelatorioTO
	 * @see RelatorioBO::retornarPropriedadeCampoRelatorio();
	 * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function retornarPropriedadeCampoRelatorio(PropriedadeCampoRelatorioTO $propriedadeCampoRelatorioTO){
		$propriedadeCampoRelatorioORM = new PropriedadeCampoRelatorioORM();
		$where = $propriedadeCampoRelatorioORM->montarWhere($propriedadeCampoRelatorioTO);
		try{
			$obj = $propriedadeCampoRelatorioORM->fetchAll($where);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		}
	}

	/**
	 * Método que retorna as views que não são do esquema principal DBO
	 */
	public function retornarViewsRelatorio(){
		$orm = new RelatorioORM();
		$query = $orm->getDefaultAdapter()->query("SELECT name AS st_view FROM sys.views WHERE schema_id <> 1");
		return $query->fetchAll();
	}

	/**
	 * Método que exclui todos os vínculos de configuração de campos do relatório
	 * @param int $idRelatorio
	 */
	public function deletarCamposRelatorio($idRelatorio){
		$this->beginTransaction();
		try{
			$this->deletarPropriedadeCampoRelatorio($idRelatorio);
			$delete = Ead1_ORM::instance(new CampoRelatorioORM())->delete("id_relatorio = $idRelatorio");
			$this->commit();
			return $delete;
		}catch(Zend_Exception $e){
			$this->rollBack();
			throw $e;
			return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	}

	/**
	 * Método que exclui todos os vínculos de configuração de campos do relatório
	 * @param int $idRelatorio
	 */
	public function deletarPropriedadeCampoRelatorio($idRelatorio){
		$this->beginTransaction();
		try{
			$campoRelatorioTO = new CampoRelatorioTO();
			$campoRelatorioTO->setId_relatorio($idRelatorio);
			$camposRelatorio = Ead1_ORM::instance(new CampoRelatorioORM())->consulta($campoRelatorioTO);
			if($camposRelatorio){
				foreach($camposRelatorio as $CampoRelatorioTOAtual){
					$arIdCampoRelatorio[] = $CampoRelatorioTOAtual->getId_camporelatorio();
				}
				if(count($arIdCampoRelatorio) > 0){
					$tbRelatorios = new PropriedadeCampoRelatorioORM();
					$idRelatorio = $tbRelatorios->delete("id_camporelatorio IN(" . implode(', ', $arIdCampoRelatorio) . ")");
				}
			}
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			throw $e;
			return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	}
}

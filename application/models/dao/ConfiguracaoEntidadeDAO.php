<?php

/**
 * 
 * Classe de persistência de configuracao da entidade com  banco de dados
 * @author Dimas Sulz <dimassulz@gmail.com>
 * @see 2011-06-10
 * 
 * @package models
 * @subpackage dao
 */
class ConfiguracaoEntidadeDAO extends Ead1_DAO {

    /**
     * Cadastra oo texto do sistema
     * Monta um array para insert
     * @see ConfiguracaoEntidadeBO::cadastrarTextoSistema()
     * @param TextoSistemaTO $textoSistemaTO
     * @return int id do registro | Exception
     */
    public function cadastrarTextoSistema(TextoSistemaTO $textoSistemaTO) {
        try {
            $textoSistemaORM = new TextoSistemaORM();
            return $textoSistemaORM->insert($textoSistemaTO->toArrayInsert());
        } catch (Exception $e) {
            return $this->excecao = $e->getMessage();
            throw $e;
        }
    }

    /**
     * Insere um novo emailentidadefuncionalidade
     * Gera o vínculo entre uma entidade, 
     * @param EmailEntidadeMensagemTO $emailEntidadeMensagemTO
     * @return mixed int|false
     */
    public function cadastrarEmailEntidadeMensagem(EmailEntidadeMensagemTO $emailEntidadeMensagemTO) {
        $emailEntidadeMensagemORM = new EmailEntidadeMensagemORM();
        $arInsert = $emailEntidadeMensagemTO->toArrayInsert();
        return $emailEntidadeMensagemORM->insert($arInsert);
    }

    /**
     * Metodo que cadastra a configuracao da entidade
     * @param ConfiguracaoEntidadeTO $to
     * @return $id_configuracaoentidade
     */
    public function cadastrarConfiguracaoEntidade(ConfiguracaoEntidadeTO $to) {
        try {
            $this->beginTransaction();
            $orm = new ConfiguracaoEntidadeORM();
            $insert = $to->toArrayInsert();
            $id_configucaraoentidade = $orm->insert($insert);
            $this->commit();
            return $id_configucaraoentidade;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que cadastra a configuracao financeira da entidade
     * @param EntidadeFinanceiroTO $to
     * @return $id_entidadefinanceiro
     */
    public function cadastrarEntidadeFinanceiro(EntidadeFinanceiroTO $to) {
        try {
            $this->beginTransaction();
            $orm = new EntidadeFinanceiroORM();
            $insert = $to->toArrayInsert();
            $id_entidadefinanceiro = $orm->insert($insert);
            $this->commit();
            return $id_entidadefinanceiro;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            THROW $e;
            return false;
        }
    }

    /**
     * Edita os textos do sistema
     * Monta um array de update ignorando a PK e a data de cadastro
     * @see ConfiguracaoEntidadeBO::editarTextoSistema()
     * @param TextoSistemaTO $textoSistemaTO
     * @return int id com o número de linhas afetadas
     */
    public function editarTextoSistema(TextoSistemaTO $textoSistemaTO) {
        try {
            $textoSistemaORM = new TextoSistemaORM();
            $camposUpdate = $textoSistemaTO->toArrayUpdate(false, array('id_textosistema'));
            return $textoSistemaORM->update($camposUpdate, 'id_textosistema = ' . $textoSistemaTO->getId_textosistema());
        } catch (Exception $e) {
            return $this->excecao = $e->getMessage();
            throw $e;
        }
    }

    /**
     * Metodo que edita a configuracao da entidade
     * @param ConfiguracaoEntidadeTO $to
     * @param Mixed $where
     * @return Boolean
     */
    public function editarConfiguracaoEntidade(ConfiguracaoEntidadeTO $to, $where = null) {
        try {
            $this->beginTransaction();
            $orm = new ConfiguracaoEntidadeORM();
            if (!$where) {
                $where = $orm->montarWhere($to, true);
            }
            $update = $to->toArrayUpdate(true, array('id_configuracaoentidade', 'id_entidade'));
            $orm->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que edita a configuracao financeira da entidade
     * @param EntidadeFinanceiroTO $to
     * @param Mixed $where
     * @return Boolean
     */
    public function editarEntidadeFinanceiro(EntidadeFinanceiroTO $to, $where = null) {
        try {
            $this->beginTransaction();
            $orm = new EntidadeFinanceiroORM();
            if (!$where) {
                $where = $orm->montarWhere($to, true);
            }
            $update = $to->toArrayUpdate(true, array('id_entidadefinanceiro', 'id_entidade'));
            $orm->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            THROW $e;
            return false;
        }
    }

    /**
     * Retorna os textos do sistema
     * Monta o where dinâmicamente
     * @see ConfiguracaoEntidadeBO::retornarTextoSistema()
     * @param VwTextoSistemaTO $vwTextoSistemaTO
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function retornarTextoSistema(VwTextoSistemaTO $vwTextoSistemaTO) {
        try {
            $vwTextoSistemaORM = new VwTextoSistemaORM();
            $where = $vwTextoSistemaORM->montarWhere($vwTextoSistemaTO);
            return $vwTextoSistemaORM->fetchAll($where);
        } catch (Exception $e) {
            $this->excecao = $e->getMessage();
            throw $e;
        }
    }

    /**
     * Retorna o texto de exibição
     * @see ConfiguracaoEntidadeBO::retornarTextoExibicao()
     * @param TextoExibicaoTO $textoExibicaoTO
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function retornarTextoExibicao(TextoExibicaoTO $textoExibicaoTO) {
        try {
            $textoExibicaoORM = new TextoExibicaoORM();
            $where = $textoExibicaoORM->montarWhere($textoExibicaoTO);
            return $textoExibicaoORM->fetchAll($where);
        } catch (Exception $e) {
            $this->excecao = $e->getMessage();
            throw $e;
        }
    }

    /**
     * Retorna a categoria do texto 
     * @see ConfiguracaoEntidadeBO::retornarTextoCategoria()
     * @param TextoCategoriaTO $textoCategoriaTO
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function retornarTextoCategoria(TextoCategoriaTO $textoCategoriaTO) {
        try {
            $textoCategoriaORM = new TextoCategoriaORM();
            $where = $textoCategoriaORM->montarWhere($textoCategoriaTO);
            return $textoCategoriaORM->fetchAll($where);
        } catch (Exception $e) {
            $this->excecao = $e->getMessage();
            throw $e;
        }
    }

    /**
     * Retorna o texto de exibição de uma categoria, faz um join entre as tabelas
     * @see ConfiguracaoEntidadeBO::retornarTextoExbicaoCategoria()
     * @param TextoExibicaoTO $textoExibicaoTO
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function retornarTextoExbicaoCategoria(TextoExibicaoTO $textoExibicaoTO) {
        try {

            $tbExibicao = new TextoExibicaoORM();
            $exibicao = $tbExibicao->find($textoExibicaoTO->getId_textoexibicao());
            $exibicaoAtual = $exibicao->current();
            $categorias = $exibicaoAtual->findTextoCategoriaORMViaTextoExibicaoCategoriaORM();

            return $categorias;
        } catch (Exception $e) {
            $this->excecao = $e->getMessage();
            throw $e;
        }
    }

    /**
     * Retorna os tipos de orientação para o texto
     * As orientações disponíveis são retrato e paisagem
     * @param OrientacaoTextoTO $orientacaoTextoTO
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function retornarOrientacaoTexto(OrientacaoTextoTO $orientacaoTextoTO) {
        $orientacaoTextoORM = new OrientacaoTextoORM();
        $where = $orientacaoTextoORM->montarWhere($orientacaoTextoTO);
        return $orientacaoTextoORM->fetchAll($where);
    }

    /**
     * Retorna as mensagens padrões do sistema
     * @param MensagemPadraoTO $mensagemPadraoTO
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function retornarMensagemPadrao(MensagemPadraoTO $mensagemPadraoTO=null, $where=null) {
        $mensagemPadraoORM = new MensagemPadraoORM();
        if ($mensagemPadraoTO) {
            $where = $mensagemPadraoORM->montarWhere($mensagemPadraoTO);
        }
        return $mensagemPadraoORM->fetchAll($where);
    }

    /**
     * 
     * @param EmailEntidadeMensagemTO $emailEntidadeMensagemTO
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function retornarEmailEntidadeMensagem(EmailEntidadeMensagemTO $emailEntidadeMensagemTO) {
        $emailEntidadeMensagemORM = new EmailEntidadeMensagemORM();

        if (empty($emailEntidadeMensagemTO->id_entidade) || $emailEntidadeMensagemTO->id_entidade == false) {
            $emailEntidadeMensagemTO->id_entidade = $emailEntidadeMensagemTO->getSessao()->id_entidade;
        }
        $where = $emailEntidadeMensagemORM->montarWhere($emailEntidadeMensagemTO);
        return $emailEntidadeMensagemORM->fetchAll($where);
    }

    /**
     * Metodo que retorna a configuração da entidade 
     * @param ConfiguracaoEntidadeTO $ceTO
     * @param Mixed $where
     * @return Zend_Db_Table_Row_Abstract
     */
    public function retornarConfiguracaoEntidade(ConfiguracaoEntidadeTO $ceTO, $where = null) {
        try {
            $orm = new ConfiguracaoEntidadeORM();
            if (!$where) {
                $where = $orm->montarWhere($ceTO, false);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que retorna a configuração da entidade 
     * @param ModeloGradeNotasTO $modeloGradeNotasTO
     * @param Mixed $where
     * @return Zend_Db_Table_Row_Abstract
     */
    public function retornarModeloGradeNotas(ModeloGradeNotasTO $modeloGradeNotasTO, $where = null) {
        try {
            $orm = new ModeloGradeNotasORM();
            if (!$where) {
                $where = $orm->montarWhere($modeloGradeNotasTO, false);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que retorna a configuração financeira da entidade 
     * @param EntidadeFinanceiroTO $to
     * @param Mixed $where
     * @return Zend_Db_Table_Row_Abstract
     */
    public function retornarEntidadeFinanceiro(EntidadeFinanceiroTO $to, $where = null) {
        try {
            $orm = new EntidadeFinanceiroORM();
            if (!$where) {
                $where = $orm->montarWhere($to, false);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Remove os registros da tabela tb_emailentidademensagem
     * @param int $idEntidade
     * @param int $idMensagemPadrao
     * @return boolean
     */
    public function deletarEmailEntidadeMensagem($idEntidade, $idMensagemPadrao) {
        $emailEntidadeMensagemORM = new EmailEntidadeMensagemORM();
        return $emailEntidadeMensagemORM->delete('id_entidade = ' . $idEntidade . 'AND id_mensagempadrao = ' . $idMensagemPadrao) > 0 ? true : false;
    }

}

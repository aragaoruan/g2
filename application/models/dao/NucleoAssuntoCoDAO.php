<?php

/**
 * Classe NucleoAssuntoCoDAO
 * @package models
 * @subpackage dao
 */
class NucleoAssuntoCoDAO extends Ead1_DAO
{

    /**
     * Método que insere o NucleoAssuntoCo
     *
     *
     * @param NucleoAssuntoCoTO $NucleoAssuntoCoTo
     * @throws Zend_Exception
     * @return Ambigous <mixed, multitype:>
     * @see NucleoAssuntoCoBO::salvarNucleoAssuntoCo();
     */
    public function cadastrarNucleoAssuntoCo(NucleoAssuntoCoTO $NucleoAssuntoCoTo)
    {
        $this->beginTransaction();
        try {
            $nucleo = new NucleoAssuntoCoORM();
            $id_nucleoassuntoco = $nucleo->insert($NucleoAssuntoCoTo->toArrayInsert());
            $this->commit();
            return $id_nucleoassuntoco;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            throw $e;
        }
    }

    /**
     * Método de edição do NucleoAssuntoCo
     * @param NucleoAssuntoCoTO $NucleoAssuntoCoTo
     * @throws Zend_Exception
     * @see NucleoAssuntoCoBO::salvarNucleoAssuntoCo();
     */
    public function editarNucleoAssuntoCo(NucleoAssuntoCoTO $NucleoAssuntoCoTo)
    {

        $this->beginTransaction();
        try {
            $nucleo = new NucleoAssuntoCoORM();
            $nucleo->update(
                $NucleoAssuntoCoTo->toArrayUpdate(true, array('id_nucleoassuntoco')),
                $nucleo->montarWhere($NucleoAssuntoCoTo, true)
            );
            $this->commit();
        } catch (Zend_Exception $e) {
            $this->rollBack();
            throw $e;
        }
    }

    /**
     * Método de listagem de ${instancia_classe}
     * @param NucleoAssuntoCoTO $NucleoAssuntoCoTo
     * @return Ambigous <boolean, multitype:>
     * @see NucleoAssuntoCoBO::listarNucleoAssuntoCo();
     */
    public function listarnucleo(NucleoAssuntoCoTO $NucleoAssuntoCoTo)
    {
        $nucleo = new NucleoAssuntoCoORM();
        return $nucleo->consulta($NucleoAssuntoCoTo);
    }


    /**
     * Listagem da VwNucleoAssuntoCo
     * @param VwNucleoAssuntoCoTO $vwnucleoAssuntoCoTo
     * @param boolean $adicionar
     * @throws Zend_Exception
     * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >
     */
    public function listarAssuntosNucleo(VwNucleoAssuntoCoTO $vwnucleoAssuntoCoTo, $adicionar = false)
    {
        if (!$vwnucleoAssuntoCoTo->getId_nucleoco()) {
            throw new Zend_Exception("Id do núcleo não informado");
        }
        $sel = " id_nucleoassuntoco is not null ";

        if ($adicionar) {
            $sel = " id_nucleoassuntoco is null ";
        }

        $orm = new VwNucleoAssuntoCoORM();
        $select = $orm->select()
            ->where(" id_nucleoco = ? ", $vwnucleoAssuntoCoTo->getId_nucleoco())
            ->where(" id_tipoocorrencia = ?", $vwnucleoAssuntoCoTo->getId_tipoocorrencia())
            ->where($sel);
        return $orm->fetchAll($select);
    }


    /**
     * Salva os assuntos de um núcleo
     * @param NucleoAssuntoCoTO $nucleoCoTo
     * @throws Zend_Exception
     * @see NucleoCoBO::salvarAssuntosNucleo
     */
    public function cadastrarAssuntoNucleoCo(NucleoAssuntoCoTO $nucleoCoTo)
    {
        $this->beginTransaction();
        try {
            $orm = new NucleoAssuntoCoORM();
            $orm->insert($nucleoCoTo->toArrayInsert());
            $this->commit();
        } catch (Zend_Exception $e) {
            $this->rollBack();
            throw $e;
        }
    }


    /**
     * Deleta o vínculo de um assunto ao núcleo
     * @param int $idnucleoAssunto
     * @throws Zend_Exception
     */
    public function excluirNucleoAssuntoCo($idnucleoAssunto)
    {
        $this->beginTransaction();
        try {
            $nucleoto = new NucleoAssuntoCoTO();
            $nucleoto->setId_nucleoassuntoco($idnucleoAssunto);
            $nucleoto->setBl_ativo(false);

            $nucleo = new NucleoAssuntoCoORM();
            $nucleo->update(
                $nucleoto->toArrayUpdate(false, array('id_nucleoassuntoco')),
                $nucleo->montarWhere($nucleoto, true)
            );
            $this->commit();
        } catch (Zend_Exception $e) {
            $this->rollBack();
            throw $e;
        }

    }


    /**
     * Lista Responsáveis
     * @param VwNucleoPessoaCoTO $nucleoPessoa
     * @return Ambigous <boolean, multitype:>
     */
    public function listaResponsaveisAssunto(VwNucleoPessoaCoTO $nucleoPessoa)
    {
        $orm = new VwNucleoPessoaCoORM();
        return $orm->consulta($nucleoPessoa, false, false, 'st_nomecompleto ASC');
    }

    /**
     * Retorna VwNuclecoAssuntoCoTO com os assuntos pai
     * @param VwNucleoAssuntoCoTO $VwAssuntoCoTo
     * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >
     */
    public function retornaVwNucleoAssuntoPai(VwNucleoAssuntoCoTO $VwAssuntoCoTo)
    {
        $orm = new VwNucleoAssuntoCoORM();
        return $orm
            ->fetchAll($orm->montarWhereView($VwAssuntoCoTo, false, true, false) . " and st_assuntopai is null ");

    }

}

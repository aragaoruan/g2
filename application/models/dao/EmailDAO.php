<?php

/**
 * Classe de acesso a dados referente ao e-mail e a fila
 * @author Arthur Cláudio de Almeida Pereira < arthur.almeida@ead1.com.br >
 */

class EmailDAO	extends Ead1_DAO {
	
	/**
	 * Salva uma nova mensagem 
	 * @param MensagemTO $mensagemTO
	 * @return mixed int|false chave primária gerada
	 */
	public function salvarMensagem ( MensagemTO $mensagemTO ) {
		$mensagemORM	= new MensagemORM();
		$insert = $mensagemTO->toArray();
		return $mensagemORM->insert($insert);
	}
	
	
	public function salvarEnvioMensagem( EnvioMensagemTO $envioMensagemTO ) {
		$envioMensagemORM	= new EnvioMensagemORM();
		return $envioMensagemORM->insert( $envioMensagemTO->toArrayInsert() );
	}
	
	/**
	 * Metodo que cadastra uma configuracao de email
	 * @param EmailConfigTO $ecTO
	 * @throws Zend_Exception
	 * @return int $id_emailconfig | false
	 */
	public function cadastrarEmailConfig(EmailConfigTO $ecTO){
		try{
			$this->beginTransaction();
			$orm = new EmailConfigORM();
			$insert = $ecTO->toArrayInsert();
			$id_emailconfig = $orm->insert($insert);
			$this->commit();
			return $id_emailconfig;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW $e;
			return false;
		}
	}
	
	/**
	 * Metodo que cadastra o vínculo de entidade com o email
	 * @param EntidadeEmailTO $entidadeEmailTO
	 * @throws Zend_Exception
	 * @return int $id_entidadeemail | false
	 */
	public function cadastrarEntidadeEmail(EntidadeEmailTO $entidadeEmailTO){
		try{
			$this->beginTransaction();
			$orm = new EntidadeEmailORM();
			$insert = $entidadeEmailTO->toArrayInsert();
			$id_entidadeemail = $orm->insert($insert);
			$this->commit();
			return $id_entidadeemail;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW $e;
			return false;
		}
	}
	
	/**
	 * Metodo que edita uma configuracaod e email
	 * @param EmailConfigTO $ecTO
	 * @param Mixed $where
	 * @return Boolean
	 */
	public function editarEmailConfig(EmailConfigTO $ecTO, $where = null){
		try{
			$this->beginTransaction();
			$orm = new EmailConfigORM();
			if(!$where){
				$where = $orm->montarWhere($ecTO,true);
			}
			$update = $ecTO->toArrayUpdate(false,array('id_emailconfig','id_usuariocadastro','dt_cadastro','id_entidade'));
			$orm->update($update, $where);
			$this->commit();
			return true;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW $e;
			return false;
		}
	}
	
	/**
	 * Metodo que edita um vínculo de entidade com email
	 * @param EntidadeEmailTO $entidadeEmailTO
	 * @param Mixed $where
	 * @return Boolean
	 */
	public function editarEntidadeEmail(EntidadeEmailTO $entidadeEmailTO, $where = null){
		try{
			$this->beginTransaction();
			$orm = new EntidadeEmailORM();
			if(!$where){
				$where = $orm->montarWhere($entidadeEmailTO,true);
			}
			$update = $entidadeEmailTO->toArrayUpdate();
			$orm->update($update, $where);
			$this->commit();
			return true;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW $e;
			return false;
		}
	}
	
	/**
	 * Metodo que retorna uma configuracao de email
	 * @param EmailConfigTO $ecTO
	 * @param Mixed $where
	 * @return Zend_Db_Table_Row_Abstract | false
	 */
	public function retornarEmailConfig(EmailConfigTO $ecTO, $where = null){
		try{
			$orm = new EmailConfigORM();
			if(!$where){
				$where = $orm->montarWhere($ecTO);
			}
			return $orm->fetchAll($where);
		}catch (Zend_Exception $e){
			$this->excecao = $e->getMessage();
			THROW $e;
			return false;
		}
	}
	
	/**
	 * Metodo que retorna o tipo de conexao do email
	 * @param TipoConexaoEmailTO $tceTO
	 * @param Mixed $where
	 * @return Zend_Db_Table_Row_Abstract | false
	 */
	public function retornarTipoConexaoEmail(TipoConexaoEmailTO $tceTO, $where = null){
		try{
			$orm = new TipoConexaoEmailORM();
			if(!$where){
				$where = $orm->montarWhere($tceTO);
			}
			return $orm->fetchAll($where);
		}catch (Zend_Exception $e){
			$this->excecao = $e->getMessage();
			THROW $e;
			return false;
		}
	}
	
	/**
	 * Retorna os dados de configuração de email das funcionalidades
	 * @param int $idEmailConfig
	 * @return Zend_Db_Table_Rowset_Abstract
	 */
	public function retornarDadosConfEmail( $idEmailConfig ) {
		$emailConfigORM	= new EmailConfigORM();
		$select			= $emailConfigORM->select()
										->from( array( 'ec' => 'tb_emailconfig'  ), array( 'ec.id_emailconfig', 'ec.id_entidade', 'ec.st_conta', 'ec.st_imap', 'ec.st_pop', 'ec.st_smtp', 'ec.bl_autenticacaosegura' ) )
										->setIntegrityCheck( false )
										->joinInner( array( 'consaida' => 'tb_tipoconexaoemail' ), 'consaida.id_tipoconexaoemail = ec.id_tipoconexaoemailsaida', array( 'id_tipoconexaoemail', 'st_tipoconexaoemail' ) )
										->joinInner( array( 'conentrada' => 'tb_tipoconexaoemail' ), 'conentrada.id_tipoconexaoemail = ec.id_tipoconexaoemailentrada', array( 'id_tipoconexaoemail', 'st_tipoconexaoemail' ) )
										->joinInner(array('ent' => 'tb_entidade'), 'ent.id_entidade = ec.id_entidade',array('st_nomeentidade'))
										->where( 'ec.bl_ativo = ?', 1 )
										->where( 'ec.id_emailconfig = ?', $idEmailConfig );
		return $emailConfigORM->fetchAll( $select );
	}
	
	/**
	 * Retorna as mensagens de e-mails não enviadas
	 * Caso seja informado os argumentos é realizado um filtro
	 * @param $idEntidade OPCIONAL
	 * @param $idFuncionalidade OPCIONAL
	 * @return Zend_Db_Table_Abstract
	 */
	public function retornarMensagemNaoEnviadas( $idEntidade = null, $idFuncionalidade = null ) {
		$mensagemORM	= new MensagemORM();
		$select			= $mensagemORM->select()
										->distinct( true )
										->from( array( 'me'	=> 'tb_mensagem' ), array( 'me.id_mensagem', 'me.id_funcionalidade', 'me.st_mensagem', 'me.id_entidade' ) )
										->setIntegrityCheck( false )
										->joinInner( array( 'em' => 'tb_enviomensagem' ), 'me.id_mensagem	= em.id_mensagem', null )
										->where( 'em.bl_enviado = ?', 0 );
		if( !empty( $idEntidade ) ) {
			$select->where( 'me.id_entidade = ?', $idEntidade );
		}
		
		if( !empty( $idFuncionalidade ) ) {
			$select->where( 'me.id_funcionalidade = ?', $idFuncionalidade );
		}
		
		return $mensagemORM->fetchAll( $select );
	}
	
	/**
	 * Retorna o texto de uma determinada mensagem
	 * @param int $idTexto
	 * @return Zend_Db_Table_Row
	 */
	public function retornarTextoMensagem( $idMensagem ) {
		$mensagemORM	= new MensagemORM();
		$select			= $mensagemORM->select( true )
										->columns( 'st_texto' )
										->where( 'id_mensagem = ?', $idMensagem );
		
		return $mensagemORM->fetchRow( $select );
	}
	
	/**
	 * Retorna os emails que ainda não form enviados
	 * @return Zend_Db_Table_Rowset_Abstract
	 */
	public function retornarEmailsNaoEnviados(){
		$mensagemORM	= new MensagemORM();
		echo $select			= $mensagemORM->select()
										->distinct( true )
										->from( array( 'me' => 'tb_mensagem' ), array( 'id_mensagem', 'st_mensagem', 'st_texto' ) )
										->setIntegrityCheck( false )
										->joinInner( array( 'em' => 'tb_enviomensagem' ), 'em.id_mensagem	= me.id_mensagem', array( 'id_enviomensagem', 'st_endereco', 'st_nome' ) )
										->where( 'em.id_tipoenvio = ?', TipoEnvioTO::TIPO_EMAIL )
										->where( 'em.dt_envio is null' )
										->where( 'em.bl_enviado = ?', 0 )->assemble();
										
		return $mensagemORM->fetchAll( $select );										
		
	}
	
	/**
	 * Retorna os destinatários da mensagem
	 * @param EnvioMensagemTO $envioMensagemTO
	 * @return Zend_Db_Table_Rowset_Abstract
	 */
	public function retornarDestinatariosMensagem( EnvioMensagemTO $envioMensagemTO ){
		$envioMensagemORM	= new EnvioMensagemORM();
		$where				= $envioMensagemORM->montarWhere( $envioMensagemTO );
		return $envioMensagemORM->fetchAll( $where );		
	}		

	
	/**
	 * Atualiza o status das mensagem para enviados
	 * Caso seja informado o id_enviomensagem é atualizado um destinatário específico
	 * @param int $idMensagem
	 * @param int $idEnvioMensagem OPCIONAL
	 * @return bool
	 */
	public function confirmarEnvioMensagem( $idMensagem, $idEnvioMensagem = null ){
		$envioMensagemORM	= new EnvioMensagemORM();
		return $envioMensagemORM->update( array( 'bl_enviado' => 1, 'dt_envio' => date( 'Y-m-d H:i:s' ) ), 'id_mensagem = '. $idMensagem ) >= 1 ? true : false ;
	}
	
	/**
	 * Retorna a mensagem padrão com a conta de e-mail vinculada e suas configurações
	 * @param int $idEntidade
	 * @param int $idMensagemPadrao
	 * @return Zend_Db_Table_Row_Abstract
	 */
	public function retornarConfEmailMensagemPadrao( $idEntidade, $idMensagemPadrao ) {
		$mensagemPadraoORM	= new MensagemPadraoORM();
		$select				= $mensagemPadraoORM->select()
												->from( array( 'eem' => 'tb_emailentidademensagem' ), array( 'id_mensagempadrao', 'id_textosistema', 'id_emailconfig' ) )
												->setIntegrityCheck( false )
												->joinInner( array( 'ec' => 'tb_emailconfig' ), 'ec.id_emailconfig	= eem.id_emailconfig', array( 'bl_autenticacaosegura', 'id_tipoconexaoemailentrada', 'id_tipoconexaoemailsaida', 'nu_portaentrada', 'nu_portasaida' ) )
												->joinInner( array( 'saida' => 'tb_tipoconexaoemail' ), 'saida.id_tipoconexaoemail	= ec.id_tipoconexaoemailsaida', array( 'tipoconexaoemailsaida' => 'st_tipoconexaoemail' ) )
												->joinInner( array( 'entrada' => 'tb_tipoconexaoemail' ), 'entrada.id_tipoconexaoemail	= ec.id_tipoconexaoemailentrada', array( 'tipoconexaoemailentrada' => 'st_tipoconexaoemail' ) )
												->joinLeft( array( 'ts' => 'tb_textosistema' ), 'eem.id_textosistema	= ts.id_textosistema' )
												->where( 'eem.id_mensagempadrao = ?', $idMensagemPadrao )
												->where( 'eem.id_entidade = ?', $idEntidade );
												
		return $mensagemPadraoORM->fetchRow( $select );												
	}
}
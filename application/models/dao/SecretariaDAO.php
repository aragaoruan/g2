<?php

/**
 * Classe de persistência do objeto Secretaria com o banco de dados
 * @author Arthur Cláudio de Almeida Pereira <arthur.almeida@ead1.com.br>
 *
 * @package models
 * @subpackage dao
 */
class SecretariaDAO extends Ead1_DAO
{

    /**
     * Realiza a inserção de entrega de um documento
     * @param EntregaDocumentosTO $entregaDocumentosTO
     * @return bool
     */
    public function salvarSituacaoEntregaDocumento(EntregaDocumentosTO $entregaDocumentosTO)
    {
        $entregaDocumentosORM = new EntregaDocumentosORM();

        $this->beginTransaction();
        try {
            $where = $entregaDocumentosORM->montarWhere($entregaDocumentosTO, true);
            $update = $entregaDocumentosTO->toArrayUpdate(false, array('id_entregadocumentos', 'id_matricula', 'id_usuario'));
            $entregaDocumentosORM->update($update, $where);
            $this->commit();
            return new Ead1_Mensageiro("Situação de entrega salva com Sucesso");
        } catch (Exception $exc) {
            $this->rollBack();
            $this->excecao = $dadosEntregaDocumentos;
            return new Ead1_Mensageiro("Erro ao salvar situação de entrega de Documentos", Ead1_IMensageiro::ERRO, $exc);
        }
    }

    /**
     * Método que cadastra(insert) um encerramento de sala de aula
     * @param EncerramentoSalaTO $encerramentoSalaTO
     * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
     */
    public function cadastrarEncerramentoSala(EncerramentoSalaTO $encerramentoSalaTO)
    {
        $encerramentoSalaORM = new EncerramentoSalaORM();
        $this->beginTransaction();
        try {
            $insert = $encerramentoSalaTO->toArrayInsert();
            $id = $encerramentoSalaORM->insert($insert);
            $this->commit();
            return $id;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que edita o encerramento de sala de aula
     * @param EncerramentoSalaTO $encerramentoSalaTO
     * @param Mixed $where
     */
    public function editarEncerramentoSala(EncerramentoSalaTO $encerramentoSalaTO, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new EncerramentoSalaORM();
            if (is_null($where)) {
                $where = $orm->montarWhere($encerramentoSalaTO, true);
            }
            $update = $encerramentoSalaTO->toArrayUpdate(false, array('id_encerramentosala'));
            $orm->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Realiza a inserção de entrega de um documento
     * @param EntregaDocumentosTO $edTO
     * @return bool
     */
    public function cadastrarEntregaDocumento(EntregaDocumentosTO $edTO)
    {
        $entregaDocumentosORM = new EntregaDocumentosORM();

        $this->beginTransaction();
        try {
            $id_entregadocumentos = $entregaDocumentosORM->insert($edTO->toArrayInsert());
            $edTO->setId_entregadocumentos($id_entregadocumentos);
            $this->commit();
            return new Ead1_Mensageiro($edTO, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $this->rollBack();
            return new Ead1_Mensageiro("Erro ao salvar entrega de Documentos", Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Vincula um documento como substituto para outro documento
     * @param int $idDocumentoOriginal - id do documento original
     * @param int $idDocumentoSubstituto - id do documento usado como substituto
     * @return bool
     */
    public function vincularDocumentoSubstituto($idDocumentoOriginal, $idDocumentoSubstituto)
    {

        $documentoSubstituicaoORM = new DocumentosSubstituicaoORM();
        $dadosSubstituicao = array('id_documentos' => $idDocumentoOriginal
        , 'id_documentossubstituto' => $idDocumentoSubstituto
        );
        return ($documentoSubstituicaoORM->insert($dadosSubstituicao) !== false);
    }

    /**
     * Vincula um projeto pedagógico a um documento
     * @param int $idProjetoPedagogico
     * @param int $idDocumento
     * @return bool
     */
    public function vincularProjetoPedagogicoDocumento($idProjetoPedagogico, $idDocumento)
    {

        $areaProjetoPedagogicoORM = new DocumentosProjetoPedagogicoORM();
        $dadosVinculo = array('id_documentos' => $idDocumento
        , 'id_projetopedagogico' => $idProjetoPedagogico
        );
        return ($areaProjetoPedagogicoORM->insert($dadosVinculo) !== false);
    }

    /**
     * Vincula um utilização a um documento
     * @param int $idUtilizacao
     * @param int $idDocumento
     * @return bool
     */
    public function vincularUtlizacaoDocumento($idUtilizacao, $idDocumento)
    {
        $documentosUtilizacaoRelacaoORM = new DocumentosUtilizacaoRelacaoORM();
        $dadosVinculo = array('id_documentos' => $idDocumento
        , 'id_documentosutilizacao' => $idUtilizacao
        );
        return ($documentosUtilizacaoRelacaoORM->insert($dadosVinculo) !== false);
    }

    /**
     * Exclui um documento marcado como substituto para outro documento
     * @param int $idDocumentoOriginal - id do documento original
     * @param int $idDocumentoSubstituto - id do documento usado como substituto
     * @return int $numLinhas - número de linhas apagadas
     */
    public function excluirDocumentoSubstituto($idDocumentoOriginal, $idDocumentoSubstituto)
    {
        $documentosSubstituicaoORM = new DocumentosSubstituicaoORM();
        return $documentosSubstituicaoORM->delete('id_documentos = ' . $idDocumentoOriginal . ' AND id_documentossubstituto = ' . $idDocumentoSubstituto);
    }

    /**
     * Exclui vínculo de documento com projeto pedagógico.
     * @param DocumentosProjetoPedagogicoTO $dppTO
     * @return boolean
     */
    public function excluirDocumentoProjetoPedagogico(DocumentosProjetoPedagogicoTO $dppTO)
    {
        $documentosProjetoPedagogicoORM = new DocumentosProjetoPedagogicoORM();
        $where = $documentosProjetoPedagogicoORM->montarWhere($dppTO);
        $this->beginTransaction();
        try {
            if (!$where) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $documentosProjetoPedagogicoORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $arr['TO'] = $dppTO;
            $arr['WHERE'] = $where;
            $arr['SQL'] = $e->getMessage();
            $this->excecao = $arr;
            return false;
        }
    }

    /**
     * Exclui vínculo de documento com sua utilização.
     * @param DocumentosUtilizacaoRelacaoTO $durTO
     * @return boolean
     */
    public function excluirUtilizacoesDocumento(DocumentosUtilizacaoRelacaoTO $durTO)
    {
        $documentosUtilizacaoRelacaoORM = new DocumentosUtilizacaoRelacaoORM();
        $where = $documentosUtilizacaoRelacaoORM->montarWhere($durTO);
        $this->beginTransaction();
        try {
            if (!$where) {
                throw new Zend_Exception('O TO de utilização não pode vir vazio!');
            }
            $documentosUtilizacaoRelacaoORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $arr['TO'] = $durTO;
            $arr['WHERE'] = $where;
            $arr['SQL'] = $e->getMessage();
            $this->excecao = $arr;
            return false;
        }
    }

    /**
     * Cadastra um novo documento
     * @param DocumentosTO
     * @param int $codUsuarioCadastro - código do usuário que está inserindo o novo documento
     * @param int $codEntidade - código da entidade do usuário
     * @return int chave primária gerada
     */
    public function cadastrarDocumento(DocumentosTO $docTO, $codUsuarioCadastro, $codEntidade)
    {
        $documentosORM = new DocumentosORM ();
        $dadosDocumento = $docTO->toArrayInsert();
        $dadosDocumento ['id_usuariocadastro'] = $codUsuarioCadastro;
        $dadosDocumento ['id_entidade'] = $codEntidade;
        $dadosDocumento ['bl_ativo'] = 1;
        return $documentosORM->insert($dadosDocumento);
    }

    /**
     * Método que cadastra(insert) na tabela disciplinasaladeaula
     * @param AulaSalaDeAulaTO $asaTO
     * @see SalaDeAulaBO::cadastrarDisciplinaSalaDeAula();
     * @return boolean
     */
    public function cadastrarDocumentosProjetoPedagogico(DocumentosProjetoPedagogicoTO $dppTO)
    {
        $documentosProjetoPedagogicoORM = new DocumentosProjetoPedagogicoORM();
        $this->beginTransaction();
        try {
            $where = $dppTO->toArrayInsert();
            $documentosProjetoPedagogicoORM->insert($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $arr['WHERE'] = $where;
            $arr['TO'] = $dppTO;
            $arr['SQL'] = $e->getMessage();
            $this->excecao = $arr;
            return false;
        }
    }

    /**
     * Edita um documento existente
     * @param DocumentosTO $docTO
     * @return int número de linhas editadas.
     *
     */
    public function editarDocumento(DocumentosTO $docTO)
    {
        $documentosORM = new DocumentosORM();
        $idDocumentos = $docTO->getId_documentos();
        $docTO->setId_documentos('');
        $dadosDocumento = $docTO->toArrayUpdate();
        $docTO->setId_documentos($idDocumentos);
        return $documentosORM->update($dadosDocumento, 'id_documentos = ' . $idDocumentos);
    }

    /**
     * Retorna os tipos de documentos existentes.
     * @return Zend_Db_Table_Rowset
     */
    public function retornarTipoDocumento()
    {
        $tipoDocumentoORM = new TipoDocumentoORM ();
        $rowset = $tipoDocumentoORM->fetchAll();
        return $rowset;
    }

    /**
     * Retorna as categorias existentes para os documentos da tabela tb_documentoscategoria
     * Realiza um filtro a partir dos atributos do TO
     * @throws Zend_Db_Exception
     * @return Zend_Db_Table_Rowset
     */
    public function retornarDocumentoCategoria()
    {
        $docCategoriaORM = new DocumentosCategoriaORM ();
        $rowset = $docCategoriaORM->fetchAll();
        return $rowset;
    }

    /**
     * Retorna as formas de utilização existentes para os documentos da tabela tb_documentosutilizacao
     * Realiza um filtro a partir dos atributos do TO
     * @throws Zend_Db_Exception
     * @return Zend_Db_Table_Rowset
     */
    public function retornarDocumentoUtilizacao()
    {
        $docUtilizacaoORM = new DocumentosUtilizacaoORM ();
        $rowset = $docUtilizacaoORM->fetchAll();
        return $rowset;
    }

    /**
     * Retorna as formas de utilização existentes para os documentos da tabela tb_documentosutilizacaorelacao
     * Realiza um filtro a partir dos atributos do TO
     * @throws Zend_Db_Exception
     * @return Zend_Db_Table_Rowset
     */
    public function retornarDocumentoUtilizacaoRelacao(DocumentosUtilizacaoRelacaoTO $durTO)
    {
        $documentoUtilizacaoRelacaoORM = new DocumentosUtilizacaoRelacaoORM();
        $where = $documentoUtilizacaoRelacaoORM->montarWhere($durTO);
        try {
            $obj = $documentoUtilizacaoRelacaoORM->fetchAll($where);
            $this->excecao = $where;
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Retorna as 'fases' de obrigatoriedade do documentos
     * cadastrados na tabela tb_documentosobrigatoriedade
     * @return Zend_Db_Table_Rowset
     */
    public function retornarDocumentoObrigatoriedade()
    {
        $docObrigatoriedadeORM = new DocumentosObrigatoriedadeORM();
        return $docObrigatoriedadeORM->fetchAll();
    }

    /**
     * Retorna os documentos cadastrados da tabela tb_documentos
     * @param DocumentosTO $docTO
     * @throws Zend_Db_Exception
     * @return Zend_db_Table_Rowset
     */
    public function retornarDocumento(DocumentosTO $docTO)
    {
        $docORM = new Material_VwDocumentosSecretariaORM();
        $sqlWhere = $docORM->montarWherePesquisa($docTO, false, true);
        return $docORM->fetchAll($sqlWhere);
    }

    /**
     * Método que retorna os documentos
     * @param DocumentosTO $docTO
     * @param Mixed $where
     * @see SecretariaBO::retornarDocumento();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarDocumentos(DocumentosTO $docTO, $where = null)
    {
        try {
            $documentoORM = new DocumentosORM();
            if (!$where) {
                $where = $documentoORM->montarWhere($docTO);
            }
            $obj = $documentoORM->fetchAll($where);
            $this->excecao = $where;
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que Retorna Documento da matrícula, seus responsáveis e seus dados de entrega
     * @see SecretariaBO::retornarDocumentoMatriculaEntrega();
     * @param VwMatriculaEntregaDocumentoTO $vmedTO
     * @param Mixed $where
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarDocumentoMatriculaEntrega(VwMatriculaEntregaDocumentoTO $vmedTO, $where = null)
    {
        try {
            $documentoORM = new VwMatriculaEntregaDocumentoORM();
            if (!$where) {
                $where = $documentoORM->montarWhereView($vmedTO, false, true);
            }
            $obj = $documentoORM->fetchAll($where);
            $this->excecao = $where;
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna projetos de documentos
     * @param DocumentosProjetoPedagogicoTO $dppTO
     * @see SecretariaBO::retornarDocumentosProjetoPedagogico();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarDocumentosProjetoPedagogico(VwDocumentosProjetoPedagogicoTO $dppTO)
    {
        $documentosProjetoPedagogicoORM = new VwDocumentosProjetoPedagogicoORM();
        $where = $documentosProjetoPedagogicoORM->montarWhereView($dppTO);
        try {
            $obj = $documentosProjetoPedagogicoORM->fetchAll($where);
            $this->excecao = $where;
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna dados da vw_encerramentocoordenador
     * @param VwEncerramentoCoordenadorTO $vwEncerramentoCoordenadorTO
     * @see SecretariaBO::retornarVwEncerramentoCoordenador
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwEncerramentoCoordenador(VwEncerramentoCoordenadorTO $vwEncerramentoCoordenadorTO, $where = null)
    {
        $vwEncerramentoCoordenadorORM = new VwEncerramentoCoordenadorORM();
        if (is_null($where)) {
            $where = $vwEncerramentoCoordenadorORM->montarWhere($vwEncerramentoCoordenadorTO);
        }
        try {
            $obj = $vwEncerramentoCoordenadorORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
        }
    }

    /**
     * Método que retorna dados da vw_encerramentosalas
     * @param VwEncerramentoSalasTO $vwEncerramentoSalasTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwEncerramentoSalas(VwEncerramentoSalasTO $vwEncerramentoSalasTO, $where = null, $order = null)
    {
        $vwEncerramentoSalasORM = new VwEncerramentoSalasORM();
        if (is_null($where)) {
            $where = $vwEncerramentoSalasORM->montarWhere($vwEncerramentoSalasTO);
        }
        try {
            $obj = $vwEncerramentoSalasORM->fetchAll($where, $order);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
        }
    }

    /**
     * Método que retorna dados da vw_encerramentosalascurso
     * @param VwEncerramentoSalasCursoTO $vwEncerramentoSalasCursoTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwEncerramentoSalasCurso(VwEncerramentoSalasCursoTO $vwEncerramentoSalasCursoTO, $where = null)
    {

        $vwEncerramentoSalasCursoORM = new VwEncerramentoSalasCursoORM();

        $select = $vwEncerramentoSalasCursoORM->select()
            ->from('vw_encerramentosalascurso', array(
                'id_encerramentosala',
                'st_disciplina',
                'nu_cargahoraria',
                'st_areaconhecimento',
                'st_projetopedagogico',
                'st_saladeaula',
                'st_professor',
                'QTD_ALUNOS_ENCERRAR',
                'dt_encerramentocoordenador',
                'dt_encerramentopedagogico',
                'dt_encerramentofinanceiro',
                'dt_encerramentoprofessor',
                'nu_valor',
                'nu_valorpago'
            ))
            ->where($where)
            ->distinct();
        try {
            $obj = $vwEncerramentoSalasCursoORM->fetchAll($select);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
        }
    }

    /**
     * Retorna os projetos pedagógicos vinculados ao documento
     * @param int $idDocumento
     * @return Zend_Db_Table_Rowset
     */
    public function retornarProjetoPedagogicoDocumento($idDocumento)
    {

        $projetoPedagogicoORM = new ProjetoPedagogicoORM();
        $select = $projetoPedagogicoORM->select()
            ->from(array('pp' => 'tb_projetopedagogico'), '*')
            ->setIntegrityCheck(false)
            ->joinInner(array('dpp' => 'tb_documentosprojetopedagogico'), ' dpp.id_projetopedagogico = pp.i_projetopedagogico', '')
            ->where('dpp.id_documentos = ?', $idDocumento);
        return $projetoPedagogicoORM->fetchAll($select);
    }

    /**
     * Retorna os documentos marcados como substituto de um documento
     * @param int $idDocumento
     * @return Zend_Db_Table_Rowset
     */
    public function retornarDocumentoSubstituto($idDocumento)
    {

        $documentoORM = new DocumentosORM();
        $select = $documentoORM->select()
            ->from(array('doc' => 'tb_documentos'), '*')
            ->setIntegrityCheck(false)
            ->joinInner(array('ds' => 'tb_documentossubstituicao'), ' ds.id_documentossubstituto = doc.id_documentos', '')
            ->where('ds.id_documentos = ?', $idDocumento);
        return $documentoORM->fetchAll($select);
    }

    /**
     * Metodo que retorna a view de entrega de documentos
     * @param VwEntregaDocumentoTO $to
     * @param Mixed $where
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function retornarVwEntregaDocumento(VwEntregaDocumentoTO $to, $where = null)
    {
        try {
            $orm = new VwEntregaDocumentoORM();
            if (!$where) {
                $where = $orm->montarWhere($to, false);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que retorna a view de entrega de documentos
     * @param VwResgateAlunosTO $to , Mixed $where
     * @return Zend_Db_Table_Rowset_Abstract
     * @author Rafael Rocha - rafael.rocha.mg@gmail.com
     */
    public function retornarVwResgateAlunos(VwResgateAlunosTO $to, array $where = null)
    {

        $orm = new VwResgateAlunosORM();

        $whereAux = $orm->montarWhereView($to, false, true);
        if (is_string($whereAux)) {
            $whereAux = array($whereAux);
        }

        if ($where) {
            foreach ($where as $w) {
                array_unshift($whereAux, $w);
            }
        }

        try {
            $query = $orm->select()
                ->from(array('vw' => 'vw_resgatealunos'), array(
                    'vw.st_nomecompleto',
                    'vw.id_matricula',
                    'vw.id_usuario',
                    'vw.st_cpf',
                    'vw.id_projetopedagogico',
                    'vw.st_projetopedagogico',
                    'vw.id_situacao',
                    'vw.st_situacao',
                    'vw.id_evolucao',
                    'vw.id_turma',
                    'vw.nu_celular',
                    'vw.id_usuarioresponsavel',
                    'vw.nu_aproveitamento',
                    'vw.st_usuarioresponsavel',
                    'vw.id_ocorrencia'
                ))->setIntegrityCheck(false)->distinct();

            foreach ($whereAux as $value) {
                $query->where($value);
            }
            //echo($query);
            return $orm->fetchAll($query);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Retorna salas de aulas para desalocar em caso de alteração na matrícula (trancamento, bloqueio)
     * @param VwSalaDisciplinaAlocacaoTO $vw
     * @param string $where
     * @throws Zend_Exception
     * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
     */
    public function retornaSalasDeAulaDesalocar(VwSalaDisciplinaAlocacaoTO $vw, $where = null)
    {
        try {
            $orm = new VwSalaDisciplinaAlocacaoORM();
            $select = $orm->select();
            if ($where) {
                $select->where($where);
            }
            $select->where('id_alocacao is not null')
                ->where('id_matricula = ?', $vw->getId_matricula());

            return $orm->fetchAll($select);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage());
            return false;
        }
    }

}


<?php
/**
 * Classe de persistência de núcleo ao banco de dados
 * @author João Gabriel - jgvasconcelos16@gmail.com
 *
 * @package models
 * @subpackage dao
 */
class NucleotmDAO extends Ead1_DAO {

	/**
	 * Método que cadastra núcleo.
	 * @param NucleotmTO $nTO
	 * @return boolean
	 */
	public function cadastrarNucleo(NucleotmTO $nTO){
		try{
			$this->beginTransaction();
			$orm = new NucleotmORM();
			$insert = $nTO->toArrayInsert();
			$id_nucleotm = $orm->insert($insert);
			$this->commit();
			return $id_nucleotm;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($this->excecao);
			return false;
		}
	}
	
	/**
	 * Método que cadastra funcionário de núcleo.
	 * @param NucleoFuncionariotmTO $nfTO
	 * @return boolean
	 */
	public function cadastrarFuncionarioNucleo(NucleoFuncionariotmTO $nfTO){
		try{
			$this->beginTransaction();
			$orm = new NucleoFuncionariotmORM();
			$insert = $nfTO->toArrayInsert();
			$insert = $orm->insert($insert);
			$this->commit();
			return $insert;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($this->excecao);
			return false;
		}
	}
	
	/**
	 * Método que cadastra produto de núcleo.
	 * @param NucleoProdutoTO $npTO
	 * @return boolean
	 */
	public function cadastrarProdutoNucleo(NucleoProdutoTO $npTO){
		try{
			$this->beginTransaction();
			$orm = new NucleoProdutoORM();
			$insert = $npTO->toArrayInsert();
			$orm->insert($insert);
			$this->commit();
			return true;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($this->excecao);
			return false;
		}
	}

	/**
	 * Metodo que Edita núcleo.
	 * @see NucleotmBO::editarNucleo());
	 * @param NucleotmTO $nTO
	 * @param String $where
	 * @throws Zend_Exception
	 * @return Boolean
	 */
	public function editarNucleo(NucleotmTO $nTO,$where = null){
		$this->beginTransaction();
		try{
			$orm = new NucleotmORM();
			if(!$where){
				$where =  $orm->montarWhere($nTO);
			}
			$update = $nTO->toArrayUpdate(false,array('id_nucleotm','id_usuariocadastro','dt_cadastro','id_entidadematriz'));
			$orm->update($update,$where);
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($e->getMessage());
			return false;
		}
	}

	/**
	 * Metodo que Edita produto de núcleo.
	 * @see NucleotmBO::editarProdutoNucleo());
	 * @param NucleoProdutoTO $npTO
	 * @param String $where
	 * @throws Zend_Exception
	 * @return Boolean
	 */
	public function editarProdutoNucleo(NucleoProdutoTO $npTO,$where = null){
		$this->beginTransaction();
		try{
			$orm = new NucleoProdutoORM();
			if(!$where){
				$where =  $orm->montarWhere($npTO);
			}
			$update = $nTO->toArrayUpdate(false,array('id_nucleotm','id_usuariocadastro','dt_cadastro','id_nucleoproduto', 'id_produto'));
			$orm->update($update,$where);
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($e->getMessage());
			return false;
		}
	}

	/**
	 * Metodo que Edita funcionário de núcleo.
	 * @see NucleotmBO::editarFuncionarioNucleo());
	 * @param NucleoFuncionariotmTO $nfTO
	 * @param String $where
	 * @throws Zend_Exception
	 * @return Boolean
	 */
	public function editarFuncionarioNucleo(NucleoFuncionariotmTO $nfTO,$where = null){
		$this->beginTransaction();
		try{
			$orm = new NucleoFuncionariotmORM();
			if(!$where){
				$where =  'id_nucleofuncionariotm = '.$nfTO->getId_nucleofuncionariotm();
			}
			$update = $nfTO->toArrayUpdate(false,array('id_nucleofuncionariotm', 'id_usuario', 'id_usuariocadastro','dt_cadastro','id_nucleotm'));
			$orm->update($update,$where);
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($e->getMessage());
			return false;
		}
	}
	
	/**
	 * Método que retorna núcleo com suas entidades.
	 * @param NucleotmTO $nTO
	 * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function retornarNucleoCompleto(NucleotmTO $nTO, $where = null){
		$orm = new NucleotmORM();
		$select = $orm->select()
				->from(array('n' => 'tb_nucleotm'), array('*'))
				->setIntegrityCheck(false)
				->join(array('en' => 'tb_entidade'), 'en.id_entidade = n.id_entidade', array('st_nomeentidade'))
				->join(array('em' => 'tb_entidade'), 'em.id_entidade = n.id_entidadematriz', array('st_nomeentidadematriz' => 'st_nomeentidade'))
				->join(array('s' => 'tb_situacao'), 's.id_situacao = n.id_situacao', array('st_situacao'));
		if(!$where){
			$where = $orm->montarWhere($nTO);
			if($where){
				$where = 'n.'.$where;
				$select = $select->where($where);
			}
		}else{
			$select = $select->where($where);
		}
		
		try{
			$obj = $orm->fetchAll($select);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			Zend_Debug::dump($e->getMessage());
			return false;
		} 
	}
	
	/**
	 * Método que retorna produtos do núcleo.
	 * @param NucleoProdutoTO $nTO
	 * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function retornarProdutoNucleo(NucleoProdutoTO $nTO, $where = null){
		$orm = new NucleoProdutoORM();
		if(!$where){
			$where = $orm->montarWhere($nTO);
		}
				
		try{
			$obj = $orm->fetchAll($where);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			Zend_Debug::dump($e->getMessage());
			return false;
		} 
	}
	
	/**
	 * Método que retorna funcionários de núcleo.
	 * @param NucleotmTO $nTO
	 * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function retornarFuncionarioNucleo(NucleoFuncionariotmTO $nfTO, $where = null){
		$orm = new NucleoFuncionariotmORM();
		$select = $orm->select()
				->from(array('nf' => 'tb_nucleofuncionariotm'), array('*'))
				->setIntegrityCheck(false)
				->join(array('n' => 'tb_nucleotm'), 'nf.id_nucleotm = n.id_nucleotm', array('st_nucleotm'))
				->join(array('f' => 'tb_funcao'), 'nf.id_funcao = f.id_funcao', array('st_funcao'))
				->join(array('s' => 'tb_situacao'), 's.id_situacao = nf.id_situacao', array('st_situacao'))
				->join(array('u' => 'tb_usuario'), 'u.id_usuario = nf.id_usuario', array('st_nomecompleto'))
				->order(array('u.st_nomecompleto'));

		if(!$where){
			$where = $orm->montarWhere($nfTO);
			if($where){
				$where = 'n.'.$where;
			}
		}
		try{
			$obj = $orm->fetchAll($select->where($where));
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		} 
	}
	
	/**
	 * Método que retorna os nucleos
	 * @param NucleotmTO $to
	 * @param Mixed $where
	 * @return Zend_Db_Table_Row_Abstract
	 */
	public function retornarNucleo(NucleotmTO $to,$where = null){
		try{
			$orm = new NucleotmORM();
			if(!$where){
				$where = $orm->montarWhere($to);
			}
			return $orm->fetchAll($where);
		}catch (Zend_Exception $e){
			THROW $e;
			return false;
		}
	}
	

	/**
	 * Metodo que exclui produto de núcleo.
	 * @param NucleoProdutoTO $npTO
	 * @return boolean
	 */
	public function excluirNucleoProduto(NucleoProdutoTO $npTO){
		try{
			$this->beginTransaction();
			$orm = new NucleoProdutoORM();
			$where = $orm->montarWhere($npTO);
			$orm->delete($where);
			$this->commit();
			return true;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($this->excecao);
			return false;
		}
	}
	
}

?>
<?php

/**
 * Classe de acesso a dados referente a PacoteDAO
 * @author Elcio Mauo Guimarães - <elcioguimaraes@gmail.com>
 * @since 
 * @package models
 * @subpackage dao
 */
class PacoteDAO extends Ead1_DAO {

	/**
	 * Método que insere o Pacote
	 * 
	 * @param PacoteTO $pacoteTO
	 * @throws Zend_Exception
	 * @return Ambigous <mixed, multitype:>
	 * @see PacoteBO::salvarPacote();
	 */
	public function cadastrarPacote(PacoteTO $pacoteTO){
		$this->beginTransaction();
		try{
			$orm = new PacoteORM();
			$id_pacote = $orm->insert($pacoteTO->toArrayInsert());
			$this->commit();
			return $id_pacote;
		}catch(Zend_Exception $e){
			$this->rollBack();
			throw $e;
		}	
	}
	
	/**
	 * Método de edição do Pacote
	 * @param PacoteTO $pacoteTO
	 * @throws Zend_Exception
	 * @see PacoteBO::salvarPacote();
	 */
	public function editarPacote(PacoteTO $pacoteTO){
		$this->beginTransaction();
		try {
			$orm = new PacoteORM();
			$orm->update($pacoteTO->toArrayUpdate(true, array('id_pacote')), $orm->montarWhere($pacoteTO, true));
			$this->commit();
		} catch (Zend_Exception $e) {
			$this->rollBack();
			throw $e;
		}
	}
	
	/**
	 * Método de listagem de Pacote
	 * @param PacoteTO $pacoteTO
	 * @return Ambigous <boolean, multitype:>
	 * @see PacoteBO::listarPacote();
	 */
	public function listarPacote(PacoteTO $pacoteTO){
		$orm = new PacoteORM();
		return $orm->consulta($pacoteTO);
	}
	
	
	/**
	 * Cadastra a entrega de um material para o processo de entrega
	 * @param EntregaMaterialTO $em
	 * @throws Zend_Exception
	 * @return Ambigous <mixed, multitype:>
	 */
	public function cadastrarEntregaMaterial(EntregaMaterialTO $em){
		$this->beginTransaction();
		try{
			$orm 				= new EntregaMaterialORM();
			$id_entregamaterial = $orm->insert($em->toArrayInsert());
			$this->commit();
			return $id_entregamaterial;
		}catch(Zend_Exception $e){
			$this->rollBack();
			throw $e;
		}
	}
	
	/**
	 * Edita a entrega de um material para o processo de entrega
	 * @param EntregaMaterialTO $em
	 * @return Ead1_Mensageiro
	 */
	public function editarEntregaMaterial(EntregaMaterialTO $em){
			$this->beginTransaction();
		try {
			$orm 				= new EntregaMaterialORM();
			$orm->update($em->toArrayUpdate(true, array('id_entregamaterial')), $orm->montarWhere($em, true));
			$this->commit();
		} catch (Zend_Exception $e) {
			$this->rollBack();
			throw $e;
		}
	}
	
	
}

?>
<?php

/**
 * Classe de persistencia de sala de aula com o banco de dados
 * @author Dimas Sulz <dimassulz@gmail.com>
 *
 * @package models
 * @subpackage dao
 */
class SalaDeAulaDAO extends Ead1_DAO
{

    /**
     * Método que cadastra(insert) a sala de aula
     * @param SalaDeAulaTO $saTO
     * @see SalaDeAulaBO::cadastrarSalaDeAula();
     * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
     */
    public function cadastrarSalaDeAula(SalaDeAulaTO $saTO)
    {
        $salaDeAulaORM = new SalaDeAulaORM();
        $this->beginTransaction();
        $insert = $saTO->toArrayInsert(); //Zend_Debug::dump($salaDeAulaORM->insert( $insert ));die;
        try {
            $id_saladeaula = $salaDeAulaORM->insert($insert);
            $this->commit();
            return $id_saladeaula;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que cadastra a entidade da sala de aula
     * @param SalaDeAulaEntidadeTO $to
     * @return boolean
     */
    public function cadastrarSalaDeAulaEntidade(SalaDeAulaEntidadeTO $to)
    {
        try {
            $this->beginTransaction();
            $orm = new SalaDeAulaEntidadeORM();
            $insert = $to->toArrayInsert();
            $orm->insert($insert);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Método que vai excluir e inserir a nova disciplina na sala de aula.
     * @param DisciplinaSalaDeAulaTO $dsaTO
     */
    public function vincularDisciplinaSalaDeAula(DisciplinaSalaDeAulaTO $dsaTO)
    {
        $disciplinaSalaDeAulaORM = new DisciplinaSalaDeAulaORM();
        $this->beginTransaction();
        try {
            $disciplinaSalaDeAulaORM->delete("id_saladeaula = {$dsaTO->getId_saladeaula()}");
            $disciplinaSalaDeAulaORM->insert($dsaTO->toArrayInsert());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que cadastra(insert) a sala de aula professor
     * @param SalaDeAulaProfessorTO $sapTO
     * @see SalaDeAulaBO::cadastrarSalaDeAulaProfessor();
     * @return boolean
     */
    public function cadastrarSalaDeAulaProfessor(SalaDeAulaProfessorTO $sapTO)
    {
        $salaDeAulaProfessorORM = new SalaDeAulaProfessorORM();
        $this->beginTransaction();
        try {
            $id_saladeaulaprofessor = $salaDeAulaProfessorORM->insert($sapTO->toArrayInsert());
            $this->commit();
            return $id_saladeaulaprofessor;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            THROW new Zend_Exception($e->getMessage());
            $msg['TO'] = $sapTO;
            $msg['WHERE'] = $sapTO->toArrayInsert();
            $msg['SQL'] = $e->getMessage();
            $this->excecao = $msg;
            return false;
        }
    }

    /**
     * Metodo que Cadastra a Alocação do Aluno
     * @param AlocacaoTO $aTO
     * @see SalaDeAulaBO::cadastrarAlocacao() | SalaDeAulaBO::salvarAlocacaoAluno();
     * @throws Zend_Exception
     * @return Integer $id | false
     */
    public function cadastrarAlocacao(AlocacaoTO $aTO)
    {
        $this->beginTransaction();
        try {
            $alocacaoORM = new AlocacaoORM();
            $insert = $aTO->toArrayInsert();
            $id = $alocacaoORM->insert($insert);
            $this->commit();
            return $id;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que cadastra(insert) a área projeto sala
     * @param AreaProjetoSalaTO $apsTO
     * @see SalaDeAulaBO::cadastrarAreaProjetoSala();
     * @return boolean
     */
    public function cadastrarAreaProjetoSala(AreaProjetoSalaTO $apsTO)
    {
        $areaProjetoSalaORM = new AreaProjetoSalaORM();
        $where = $apsTO->toArrayInsert();
        $this->beginTransaction();
        try {
            $id_areaprojetosala = $areaProjetoSalaORM->insert($where);
            $this->commit();
            return $id_areaprojetosala;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $arr['TO'] = $apsTO;
            $arr['WHERE'] = $where;
            $arr['SQL'] = $e->getMessage();
            $this->excecao = $arr;
            return false;
        }
    }


    /**
     * @param AreaProjetoSalaIntegracaoTO $apsTO
     * @return mixed|boolean
     */
    public function cadastrarAreaProjetoSalaIntegracao(AreaProjetoSalaIntegracaoTO $apsTO)
    {
        $areaProjetoSalaORM = new AreaProjetoSalaIntegracaoORM();
        $where = $apsTO->toArrayInsert();
        $this->beginTransaction();
        try {
            $id_areaprojetosalaintegracao = $areaProjetoSalaORM->insert($where);
            $this->commit();
            return $id_areaprojetosalaintegracao;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $arr['TO'] = $apsTO;
            $arr['WHERE'] = $where;
            $arr['SQL'] = $e->getMessage();
            $this->excecao = $arr;
            return false;
        }
    }

    /**
     * Método que cadastra(insert) na tabela aulasaladeaula
     * @param AulaSalaDeAulaTO $asaTO
     * @see SalaDeAulaBO::cadastrarAulaSalaDeAula();
     * @return boolean
     */
    public function cadastrarAulaSalaDeAula(AulaSalaDeAulaTO $asaTO)
    {
        $aulaSalaDeAulaORM = new AulaSalaDeAulaORM();
        $this->beginTransaction();
        try {
            $aulaSalaDeAulaORM->insert($asaTO->toArrayInsert());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que cadastra(insert) na tabela disciplinasaladeaula
     * @param AulaSalaDeAulaTO $asaTO
     * @see SalaDeAulaBO::cadastrarDisciplinaSalaDeAula();
     * @return boolean
     */
    public function cadastrarDisciplinaSalaDeAula(DisciplinaSalaDeAulaTO $dsdaTO)
    {
        $disciplinaSalaDeAulaORM = new DisciplinaSalaDeAulaORM();
        $this->beginTransaction();
        try {
            $where = $dsdaTO->toArrayInsert();
            $arr['WHERE'] = $where;
            $arr['TO'] = $dsdaTO;
            $disciplinaSalaDeAulaORM->insert($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $arr['SQL'] = $e->getMessage();
            $this->excecao = $arr;
            return false;
        }
    }

    /**
     * Método que cadastra o Vinculo de Referencia do Perfil do Usuario na Entidade
     * @param UsuarioPerfilEntidadeReferenciaTO $uperTO
     * @see SalaDeAulaBO::cadastrarUsuarioPerfilEntidadeReferenciaSala();
     * @return boolean false || int $id_perfilreferencia
     */
    public function cadastrarUsuarioPerfilEntidadeReferenciaSala(UsuarioPerfilEntidadeReferenciaTO $uperTO)
    {
        $uperTO->setId_disciplina(null);
        $uperTO->setId_projetopedagogico(null);
        $uperTO->setId_areaconhecimento(null);
        $usuarioPerfilEntidadeReferenciaORM = new UsuarioPerfilEntidadeReferenciaORM();
        $this->beginTransaction();
        try {
            $id_perfilreferencia = $usuarioPerfilEntidadeReferenciaORM->insert($uperTO->toArrayInsert());
            $this->commit();
            return $id_perfilreferencia;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            THROW new Zend_Exception($e->getMessage());
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que edita (update) a sala de aula
     * @param SalaDeAulaTO $saTO
     * @see SalaDeAulaBO::editarSalaDeAula()
     * @return boolean
     */
    public function editarSalaDeAula(SalaDeAulaTO $saTO)
    {
        $salaDeAulaORM = new SalaDeAulaORM();
        $this->beginTransaction();
        try {
            $salaDeAulaORM->update($saTO->toArrayUpdate(false, array('id_saladeaula')), 'id_saladeaula = ' . $saTO->getId_saladeaula());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que edita a entidade da sala de aula
     * @param SalaDeAulaEntidadeTO $to
     * @param Mixed $where
     * @return boolean
     */
    public function editarSalaDeAulaEntidade(SalaDeAulaEntidadeTO $to, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new SalaDeAulaEntidadeORM();
            if (!$where) {
                $where = $orm->montarWhere($to);
            }
            $update = $to->toArrayUpdate();

            $orm->update($update);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Método que edita (update) a sala de aula professor
     * @param SalaDeAulaProfessorTO $sapTO
     * @see SalaDeAulaBO::editarSalaDeAulaProfessor()
     * @return boolean
     */
    public function editarSalaDeAulaProfessor(SalaDeAulaProfessorTO $sapTO)
    {
        $salaDeAulaProfessorORM = new SalaDeAulaProfessorORM();
        $this->beginTransaction();
        try {
            $where = 'id_saladeaulaprofessor = ' . $sapTO->getId_saladeaulaprofessor() . '
											AND id_perfilreferencia = ' . $sapTO->getId_perfilReferencia();
            $salaDeAulaProfessorORM->update($sapTO->toArrayUpdate(false, array('id_saladeaulaprofessor', 'id_perfilreferencia')), $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            THROW new Zend_Exception($e->getMessage());
            $arr['WHERE'] = $where;
            $arr['TO'] = $sapTO;
            $arr['SQL'] = $e->getMessage();
            $this->excecao = $arr;
            return false;
        }
    }

    /**
     * Metodo que Edita a Alocação do Aluno
     * @see SalaDeAulaBO::editarAlocacao() | SalaDeAulaBO::salvarAlocacaoAluno();
     * @param AlocacaoTO $aTO
     * @param String $where
     * @throws Zend_Exception
     * @return Boolean
     */
    public function editarAlocacao(AlocacaoTO $aTO, $where = null)
    {
        $this->beginTransaction();
        try {
            $alocacaoORM = new AlocacaoORM();
            if (!$where) {
                $where = $alocacaoORM->montarWhere($aTO, true);
            }
            $update = $aTO->toArrayUpdate(false, array('id_alocacao', 'id_usuariocadastro', 'dt_cadastro', 'id_matriculadisciplina'));
            $alocacaoORM->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que edita (update) a tabela aulasaladeaula
     * @param AulaSalaDeAulaTO $asaTO
     * @see SalaDeAulaBO::editarAulaSalaDeAula()
     * @return boolean
     */
    public function editarAulaSalaDeAula(AulaSalaDeAulaTO $asaTO)
    {
        $aulaSalaDeAulaORM = new AulaSalaDeAulaORM();
        $this->beginTransaction();
        try {
            $aulaSalaDeAulaORM->update($asaTO->toArrayUpdate(false, array('id_aulasaladeaula')), 'id_aulasaladeaula = ' . $asaTO->getId_aulasaladeaula());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que edita a referencia do perfil do usuario com a sala de aula
     * @param UsuarioPerfilEntidadeReferenciaTO $uperTO
     * @see SalaDeAulaBO::deletarProfessorSalaDeAula()
     * @return boolean
     */
    public function editarUsuarioPerfilEntidadeReferencia(UsuarioPerfilEntidadeReferenciaTO $uperTO)
    {
        $usuarioPerfilEntidadeReferenciaORM = new UsuarioPerfilEntidadeReferenciaORM();
        $update = $uperTO->toArrayUpdate(false, array('id_perfil', 'id_areaconhecimento', 'id_projetopedagogico', 'id_disciplina', 'id_usuario', 'id_entidade', 'id_saladeaula', 'id_perfilreferencia'));
        unset($uperTO->bl_ativo);
        $where = $usuarioPerfilEntidadeReferenciaORM->montarWhere($uperTO, false);
//		Zend_Debug::dump($where);
//		Zend_Debug::dump($update);
//		exit;
        $this->beginTransaction();
        try {
            $usuarioPerfilEntidadeReferenciaORM->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            THROW new Zend_Exception($e->getMessage());
            $arr['TO'] = $uperTO;
            $arr['UPDATE'] = $update;
            $arr['WHERE'] = $where;
            $arr['SQL'] = $e->getMessage();
            $this->excecao = $arr;
            return false;
        }
    }

    /**
     * Metodo que exclui a entidade da sala de aula
     * @param SalaDeAulaEntidadeTO $to
     * @param Mixed $where
     * @return boolean
     */
    public function deletarSalaDeAulaEntidade(SalaDeAulaEntidadeTO $to, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new SalaDeAulaEntidadeORM();
            if (!$where) {
                $where = $orm->montarWhere($to);
            }
            if (!$where) {
                THROW new Zend_Exception('O TO não Pode vir Vazio!');
            }
            $orm->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Método que deleta a sala de aula
     * @param SalaDeAulaTO $saTO
     * @see SalaDeAulaBO::deletarSalaDeAula();
     * @return boolean
     */
    public function deletarSalaDeAula(SalaDeAulaTO $saTO)
    {
        $salaDeAulaORM = new SalaDeAulaORM();
        $this->beginTransaction();
        try {
            $salaDeAulaORM->delete('id_saladeaula = ' . $saTO->getId_saladeaula());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que deleta a sala de aula professor
     * @param SalaDeAulaProfessorTO $sapTO
     * @see SalaDeAulaBO::deletarSalaDeAulaProfessor();
     * @return boolean
     */
    public function deletarSalaDeAulaProfessor(SalaDeAulaProfessorTO $sapTO)
    {
        $salaDeAulaProfessorORM = new SalaDeAulaProfessorORM();
        $this->beginTransaction();
        try {
            $salaDeAulaProfessorORM->delete('id_saladeaula = ' . $sapTO->getId_saladeaula() . ' AND id_usuario = ' . $sapTO->getId_usuario . ' AND id_entidade = ' . $sapTO->getId_entidade());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que deleta o horario de uma sala de aula
     * @param SalaHorairoLocal $shlTO
     * @see SalaDeAulaBO::deletarSalaHorarioLocal();
     * @return boolean
     */
    public function deletarSalaHorarioLocal(SalaHorarioLocalTO $shlTO)
    {
        $salaHorarioLocalORM = new SalaHorarioLocalORM();
        $this->beginTransaction();
        try {
            $salaHorarioLocalORM->delete('id_saladeaula = ' . $shlTO->getId_saladeaula() . ' AND id_horarioaula = ' . $shlTO->getId_horarioaula() . ' AND id_localaula = ' . $shlTO->getId_localaula());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }


    /**
     * Método que deleta a alocação do aluno
     * @param int $id_alocacaointegracao
     * @see SalaDeAulaBO::reintegrarAlocacaoAluno();
     * @return boolean
     */
    public function deletarAlocacaoIntegracao($id_alocacaointegracao)
    {
        $orm = new AlocacaoIntegracaoORM();
        $this->beginTransaction();
        try {
            $orm->delete('id_alocacaointegracao = ' . $id_alocacaointegracao);
            $this->commit();
            return true;
        } catch (Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            throw $e;
            return false;
        }
    }


    /**
     * exclui uma a integração da sala de aula
     * @param unknown_type $id_saladeaulaintegracao
     * @throws Exception
     * @return boolean
     */
    public function deletarSalaDeAulaIntegracao($id_saladeaulaintegracao)
    {
        $orm = new SalaDeAulaIntegracaoORM();
        $this->beginTransaction();
        try {
            $orm->delete('id_saladeaulaintegracao = ' . $id_saladeaulaintegracao);
            $this->commit();
            return true;
        } catch (Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            throw $e;
            return false;
        }
    }

    /**
     * Método que deleta a área projeto sala
     * @param AreaProjetoSalaTO $apsTO
     * @see SalaDeAulaBO::deletarAreaProjetoSala();
     * @return boolean
     */
    public function deletarAreaProjetoSala(AreaProjetoSalaTO $apsTO)
    {
        $areaProjetoSalaORM = new AreaProjetoSalaORM();
        $this->beginTransaction();
        $where = $areaProjetoSalaORM->montarWhere($apsTO);
        try {
            if (!$where) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $areaProjetoSalaORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que deleta a tabela aulasaladeaula
     * @param AulaSalaDeAulaTO $asaTO
     * @see SalaDeAulaBO::deletarAulaSalaDeAula();
     * @return boolean
     */
    public function deletarAulaSalaDeAula(AulaSalaDeAulaTO $asaTO)
    {
        $aulaSalaDeAulaORM = new AulaSalaDeAulaORM();
        $this->beginTransaction();
        try {
            $aulaSalaDeAulaORM->delete('id_aulasaladeaula = ' . $asaTO->getId_aulasaladeaula());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que deleta dados da tabela disciplinasaladeaula
     * @param AulaSalaDeAulaTO $asaTO
     * @see SalaDeAulaBO::deletarDisciplinaSalaDeAula();
     * @return boolean
     */
    public function deletarDisciplinaSalaDeAula(DisciplinaSalaDeAulaTO $dsaTO)
    {
        $disciplinaSalaDeAulaORM = new DisciplinaSalaDeAulaORM();
        $this->beginTransaction();
        try {
            $where = $disciplinaSalaDeAulaORM->montarWhere($dsaTO);
            if (!$dsaTO->getId_saladeaula()) {
                THROW new Zend_Exception('O TO não pode vir sem o id da sala de aula!');
            }
            $disciplinaSalaDeAulaORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna as sala(s) de aula
     * @param SalaDeAulaTO $saTO
     * @see SalaDeAulaBO::retornarSalaDeAula();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarSalaDeAula(SalaDeAulaTO $saTO)
    {
        $salaDeAulaORM = new SalaDeAulaORM();
        $where = $salaDeAulaORM->montarWhere($saTO);
        try {
            $obj = $salaDeAulaORM->fetchAll($where);
            $this->excecao = $where;
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna tipos de sala de aula
     * @param TipoSalaDeAulaTO $tsaTO
     * @see SalaDeAulaBO::retornarTipoSalaDeAula();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarTipoSalaDeAula(TipoSalaDeAulaTO $tsaTO)
    {
        $tipoSalaDeAulaORM = new TipoSalaDeAulaORM();
        $where = $tipoSalaDeAulaORM->montarWhere($tsaTO);
        try {
            $obj = $tipoSalaDeAulaORM->fetchAll($where);
            $this->excecao = $where;
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna as modalidades sala de aula
     * @param ModalidadeSalaDeAulaTO $msaTO
     * @see SalaDeAulaBO::retornarModalidadeSalaDeAula();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarModalidadeSalaDeAula(ModalidadeSalaDeAulaTO $msaTO)
    {
        $modalidadeSalaDeAulaORM = new ModalidadeSalaDeAulaORM();
        $where = $modalidadeSalaDeAulaORM->montarWhere($msaTO);
        try {
            $obj = $modalidadeSalaDeAulaORM->fetchAll($where);
            $this->excecao = $where;
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna as sala(s) de aula
     * @param SalaDeAulaTO $saTO
     * @see SalaDeAulaBO::retornarSalaDeAula();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarSalaDeAulaProfessor(SalaDeAulaProfessorTO $sapTO)
    {
        $salaDeAulaProfessorORM = new SalaDeAulaProfessorORM();
        $where = $salaDeAulaProfessorORM->montarWhere($sapTO);
        try {
            $obj = $salaDeAulaProfessorORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna os horarios das salas de aula
     * @param SalaHorarioLocalTO $shlTO
     * @see SalaDeAulaBO::retornarVwSalaHorarioLocal();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwSalaHorarioLocal(VwSalaHorarioLocalTO $shlTO)
    {
        $vwSalaHorarioLocalORM = new VwSalaHorarioLocalORM();
        $where = $vwSalaHorarioLocalORM->montarWhere($shlTO);
        try {
            $obj = $vwSalaHorarioLocalORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna área projeto projeto sala
     * @param AreaProjetoSalaTO $apsTO
     * @see SalaDeAulaBO::retornarAreaProjetoSala();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarAreaProjetoSala(AreaProjetoSalaTO $apsTO)
    {
        $areaProjetoSalaORM = new AreaProjetoSalaORM();
        $where = $areaProjetoSalaORM->montarWhere($apsTO);
        try {
            $obj = $areaProjetoSalaORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna view de área projeto projeto sala
     * @param VwAreaProjetoSalaTO $apsTO
     * @see SalaDeAulaBO::retornarAreaProjetoSala();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwAreaProjetoSala(VwAreaProjetoSalaTO $apsTO)
    {
        $areaProjetoSalaORM = new VwAreaProjetoSalaORM();
        $where = $areaProjetoSalaORM->montarWhereView($apsTO);
        try {
            $obj = $areaProjetoSalaORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna dados da tabela aulasaladeaula
     * @param AulaSalaDeAulaTO $asaTO
     * @see SalaDeAulaBO::retornarAulaSalaDeAula();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarAulaSalaDeAula(AulaSalaDeAulaTO $asaTO)
    {
        $aulaSalaDeAulaORM = new AulaSalaDeAulaORM();
        $where = $aulaSalaDeAulaORM->montarWhere($asaTO);
        try {
            $obj = $aulaSalaDeAulaORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna dados da tabela tb_periodoletivo.
     * @param PeriodoLetivoTO $plTO
     * @see SalaDeAulaBO::retornarPeriodoLetivo();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarPeriodoLetivo(PeriodoLetivoTO $plTO)
    {
        $periodoLetivoORM = new PeriodoLetivoORM();
        $where = $periodoLetivoORM->montarWhere($plTO);
        try {
            $obj = $periodoLetivoORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna dados da tabela disciplina saladeaula
     * @param DisciplinaSalaDeAulaTO $dsaTO
     * @see SalaDeAulaBO::retornarDisciplinaSalaDeAula();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarDisciplinaSalaDeAula(DisciplinaSalaDeAulaTO $dsaTO)
    {
        $disciplinaSalaDeAulaORM = new DisciplinaSalaDeAulaORM();
        $where = $disciplinaSalaDeAulaORM->montarWhere($dsaTO);
        try {
            $obj = $disciplinaSalaDeAulaORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Retornar tipo avaliação sala de aula
     * @param TipoAvaliacaoSalaDeAulaTO $tasaTO
     * @see SalaDeAulaBO::retornarTipoAvaliacaoSalaDeAula();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarTipoAvaliacaoSalaDeAula(TipoAvaliacaoSalaDeAulaTO $tasaTO)
    {
        $tipoAvaliacaoSalaDeAulaORM = new TipoAvaliacaoSalaDeAulaORM();
        $where = $tipoAvaliacaoSalaDeAulaORM->montarWhere($tasaTO);
        try {
            $obj = $tipoAvaliacaoSalaDeAulaORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que Retorna a Referencia e a Sala de Aula do Professor
     * @param VwReferenciaSalaDeAulaProfessorTO $VWsdapTO
     * @param mixed $where
     * @see SalaDeAulaBO::regraProfessorSalaDeAula();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarReferenciaSalaDeAulaProfessor(VwReferenciaSalaDeAulaProfessorTO $VWsdapTO, $where = null)
    {
        $vwReferenciaSalaDeAulaProfessorORM = new VwReferenciaSalaDeAulaProfessorORM();
        $VWsdapTO->tipaTODAO();
        if (!$where) {
            $where = $vwReferenciaSalaDeAulaProfessorORM->montarWhere($VWsdapTO);
        }
        try {
            $obj = $vwReferenciaSalaDeAulaProfessorORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            THROW new Zend_Exception($e->getMessage());
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que retorna a referencia do perfil do usuario com a sala de aula
     * @param UsuarioPerfilEntidadeReferenciaTO $uperTO
     * @see SalaDeAulaBO::retornarUsuarioPerfilEntidadeReferenciaSala() | procedimentoVincularProfessorSalaDeAula();
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarUsuarioPerfilEntidadeReferenciaSala(UsuarioPerfilEntidadeReferenciaTO $uperTO)
    {
        $usuarioPerfilEntidadeReferenciaORM = new UsuarioPerfilEntidadeReferenciaORM();
        $where = $usuarioPerfilEntidadeReferenciaORM->montarWhere($uperTO);
        try {
            $obj = $usuarioPerfilEntidadeReferenciaORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            THROW new Zend_Exception($e->getMessage());
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que retorna o professor de uma sala de aula
     * @param VwProfessorSalaDeAulaTO $vwPsdaTO
     * @see SalaDeAulaBO::retornarProfessorSalaDeAula()
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarProfessorSalaDeAula(VwProfessorSalaDeAulaTO $vwPsdaTO)
    {
        $vwProfessorSalaDeAulaORM = new VwProfessorSalaDeAulaORM();
        unset($vwPsdaTO->bl_titular);
        unset($vwPsdaTO->nu_maximoalunos);
        $where = $vwProfessorSalaDeAulaORM->montarWhereView($vwPsdaTO, false, true);
        try {
            $obj = $vwProfessorSalaDeAulaORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que retorna as Salas de Aula disponiveis ao aluno
     * @see SalaDeAulaBO::retornarVwSalaDisciplinaProjeto();
     * @param VwSalaDisciplinaProjetoTO $vwSdpTO
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwSalaDisciplinaProjeto(VwSalaDisciplinaProjetoTO $vwSdpTO)
    {
        $vwSalaDisciplinaProjetoORM = new VwSalaDisciplinaProjetoORM();
        $where = $vwSalaDisciplinaProjetoORM->montarWhereView($vwSdpTO, false, true);
        try {
            $select = $vwSalaDisciplinaProjetoORM->select()
                ->distinct(true)
                ->from('vw_saladisciplinaprojeto', array('id_saladeaula', 'st_saladeaula', 'id_matriculadisciplina', 'id_alocacao'))
                ->where($where);
            $obj = $vwSalaDisciplinaProjetoORM->fetchAll($select);
            $this->excecao = $select->assemble();
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }//Alocadas

    /**
     * Metodo que retorna as Salas de Aula disponiveis ao aluno
     * @see SalaDeAulaBO::retornarVwSalaDisciplinaAlocacao();
     * @param VwSalaDisciplinaAlocacaoTO $vwSdaTO
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwSalaDisciplinaAlocacao(VwSalaDisciplinaAlocacaoTO $vwSdaTO, $where = null)
    {
        $vwSalaDisciplinaAlocacaoORM = new VwSalaDisciplinaAlocacaoORM();
        if (!$where) {
            $where = $vwSalaDisciplinaAlocacaoORM->montarWhereView($vwSdaTO, false, true);
        }

        try {
            if (empty($where)) {
                throw new Zend_Exception('Nenhum parâmetro informado.');
            }

            $select = $vwSalaDisciplinaAlocacaoORM->select()
                ->distinct(true)
                ->from('vw_saladisciplinaalocacao', array(
                    'id_saladeaula',
                    'st_saladeaula',
                    'id_matriculadisciplina',
                    'id_alocacao',
                    'dt_abertura',
                    'st_projetopedagogico'
                ))
                ->where($where);
            $obj = $vwSalaDisciplinaAlocacaoORM->fetchAll($select);
            $this->excecao = $select->assemble();
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }//Alocadas

    /**
     * Metodo que retorna as Salas de Aula do aluno
     * @see SalaDeAulaBO::retornarVwSalaDisciplinaProjeto();
     * @param VwSalaDisciplinaProjetoTO $vwSdpTO
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwSalaDisciplinaProjetoAlocadas(VwSalaDisciplinaProjetoTO $vwSdpTO)
    {
        $vwSalaDisciplinaProjetoORM = new VwSalaDisciplinaProjetoORM();
        $where = $vwSalaDisciplinaProjetoORM->montarWhereView($vwSdpTO, false, true) . 'and id_alocacao is not null';
        try {
            $select = $vwSalaDisciplinaProjetoORM->select()
                ->distinct(true)
                ->from('vw_saladisciplinaprojeto', array('id_saladeaula', 'st_saladeaula', 'id_matriculadisciplina', 'id_alocacao'))
                ->where($where);
            $obj = $vwSalaDisciplinaProjetoORM->fetchAll($select);
            $this->excecao = $select->assemble();
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }


    /**
     * Retorna os alunos de uma sala de aula
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param VwAlunosSala $to
     * @throws Zend_Exception
     * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
     */
    public function retornarVwAlunosSala(VwAlunosSalaTO $to)
    {

        $orm = new VwAlunosSalaORM();
        try {

            return $orm->consulta($to, false, false, array('st_projetopedagogico', 'st_nomecompleto'));

        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }


    /**
     * Metodo que retorna a Alocação do Aluno
     * @param AlocacaoTO $aTO
     * @see SalaDeAulaBO::retornarAlocacao()
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarAlocacao(AlocacaoTO $aTO, $where = false)
    {
        $alocacaoORM = new AlocacaoORM();
        if (!$where) {
            $where = $alocacaoORM->montarWhere($aTO);
        }
        try {
            $obj = $alocacaoORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que retorna as entidades da sala de aula
     * @param SalaDeAulaEntidadeTO $to
     * @param Mixed $where
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarSalaDeAulaEntidade(SalaDeAulaEntidadeTO $to, $where = null)
    {
        $orm = new SalaDeAulaEntidadeORM();
        if (!$where) {
            $where = $orm->montarWhere($to);
        }
        try {
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna integração da sala de aula
     * @see SalaDeAulaBO::retornarSalaDeAulaIntegracao();
     * @param SalaDeAulaIntegracaoTO $sdaiTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarSalaDeAulaIntegracao(SalaDeAulaIntegracaoTO $sdaiTO)
    {
        $orm = new SalaDeAulaIntegracaoORM();
        return $orm->fetchAll($orm->montarWhere($sdaiTO));
    }

    /**
     * Método que retorna as salas de aulas com integração do professor
     * @param VwSalaDeAulaProfessorIntegracaoTO $to
     * @param Mixed $where
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function retornarVwSalaDeAulaProfessorIntegracao(VwSalaDeAulaProfessorIntegracaoTO $to, $where = null, $order = null)
    {
        $orm = new VwSalaDeAulaProfessorIntegracaoORM();
        return $orm->fetchAll($where ? $where : $orm->montarWhereView($to, false, true), $order);
    }

    /**
     * Método que retorna as salas de aulas com integração do observador institucional e outros perfis.
     * @param VwSalaConfiguracaoTO $saTO
     * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
     */
    public function retornarVwSalaDeAulaPerfilProjetoIntegracao(VwSalaDeAulaPerfilProjetoIntegracaoTO $to, $order = null, $count = null, $offset = null, $where_string)
    {
        $orm = new VwSalaDeAulaPerfilProjetoIntegracaoORM();
        $where = $orm->montarWhereView($to, false, true);
        if ($where_string) {
            if ($where) {
                $where .= ' AND ';
            }
            $where .= $where_string;
        }
        try {
            $obj = $orm->fetchAll($where, $order, $count, $offset);
            $this->excecao = $where;
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna as configurações de sala de aula para encerramento
     * @param VwSalaConfiguracaoTO $saTO
     * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
     */
    public function retornaVwSalaConfiguracao(VwSalaConfiguracaoTO $saTO)
    {
        $orm = new VwSalaConfiguracaoORM();
        $where = $orm->montarWhereView($saTO, false, true);
        try {
            $obj = $orm->fetchAll($where);
            $this->excecao = $where;
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Retorna VwEncerramentoAlunos
     * @param VwEncerramentoAlunosTO $encerramento
     * @param $where_dt - Array de datas
     * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
     */
    public function retornaAlunosEncerramento(VwEncerramentoAlunosTO $encerramento, $where_dt = null)
    {
        $orm = new VwEncerramentoAlunosORM();
        $where = $orm->montarWhereView($encerramento, false, true) . (!empty($where_dt) ? ' AND ' . $where_dt : '');

        try {
            $obj = $orm->fetchAll($where);
            $this->excecao = $where;
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }


    /**
     * Encerra uma sala a partir do professor
     * @param EncerramentoSalaTO $to
     * @return Ambigous <mixed, multitype:>|boolean
     */
    public function cadastrarEncerramentoSala(EncerramentoSalaTO $to)
    {
        $ORM = new EncerramentoSalaORM();
        $this->beginTransaction();
        $insert = $to->toArrayInsert();
        try {
            $id = $ORM->insert($insert);
            $this->commit();
            return $id;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que insere na tb_alocacao os dados da turma encerrada
     * @param EncerramentoAlocacaoTO $to
     * @return Ambigous <mixed, multitype:>|boolean
     */
    public function cadastrarEncerramentoAlocacao(EncerramentoAlocacaoTO $to)
    {
        $ORM = new EncerramentoAlocacaoORM();
        $this->beginTransaction();
        $insert = $to->toArrayInsert();
        try {
            $id = $ORM->insert($insert);
            $this->commit();
            return $id;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            throw $e;
            return false;
        }
    }

    /**
     * Atualiaza a tabela tb_matriculadisciplina
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param MatriculaDisciplinaTO $to
     * @return boolean
     */
    public function atualizaMatriculaDisciplina(MatriculaDisciplinaTO $to)
    {

        $ORM = new MatriculaDisciplinaORM();
        $this->beginTransaction();
        try {
            $ORM->update($to->toArrayUpdate(false, array('id_matriculadisciplina')), 'id_matriculadisciplina = ' . $to->getId_matriculadisciplina());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Retorna os id_matriculas para uma rotina de sincronização automatica solicitada pelo Felipe
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @throws Exception
     * @return multitype:|boolean
     */
    public function retornaArrMatriculaVwAlunosAutoAlocar($id_entidade = null)
    {
        $orm = new VwAlunosAutoAlocarORM();
        try {
            $sql = $orm->select()->from($orm)->limit(60);

            if (!empty($id_entidade)) {
                $sql->where('id_entidadeatendimento = ' . $id_entidade);
            }
            $rs = $orm->getAdapter()->fetchAll($sql);
            if ($rs) {
                return $rs;
            }

            return false;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Método relaciona o local e o horário à sala de aula
     * @param SalaHorarioLocal $shlTO
     * @param UsuarioPerfilEntidadeReferenciaTO $sdapTO
     * @see SalaDeAulaBO::cadastrarSalaHorarioLocal();
     * @return boolean
     */
    public function cadastrarSalaHorarioLocal(SalaHorarioLocalTO $shlTO, UsuarioPerfilEntidadeReferenciaTO $sdapTO)
    {
        $salaHorarioLocalORM = new SalaHorarioLocalORM();
        $this->beginTransaction();
        try {
            $salaHorarioLocalORM->insert($shlTO->toArrayInsert());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            THROW new Zend_Exception($e->getMessage());
            $msg['TO'] = $shlTO;
            $msg['WHERE'] = $shlTO->toArrayInsert();
            $msg['SQL'] = $e->getMessage();
            $this->excecao = $msg;
            return false;
        }
    }

    public function verificaConflitoHorario(SalaHorarioLocalTO $shlTO, UsuarioPerfilEntidadeReferenciaTO $uperTO)
    {

        return false;
    }

    /**
     * Método que altera a sala de aula integraçao
     * @param SalaDeAulaIntegracaoTO $to
     * @return boolean
     */
    public function editarSalaDeAulaIntegracao(SalaDeAulaIntegracaoTO $to)
    {
        $ORM = new SalaDeAulaIntegracaoORM();
        $this->beginTransaction();
        try {
            $ORM->update($to->toArrayUpdate(false, array('id_saladeaulaintegracao')), 'id_saladeaulaintegracao = ' . $to->getId_saladeaulaintegracao());
            $this->commit();
            return true;
        } catch (Exception $e) {
            $this->rollBack();
// 			$this->excecao = $e->getMessage();
            throw $e;
            return false;
        }
    }


    /**
     * Edita o AreaProjetoSala
     * @param AreaProjetoSalaTO $to
     * @throws Exception
     * @return boolean
     */
    public function editarAreaProjetoSala(AreaProjetoSalaTO $to)
    {
        $orm = new AreaProjetoSalaORM();
        $this->beginTransaction();
        try {
            $orm->update($to->toArrayUpdate(false, array('id_areaprojetosala', 'id_projetopedagogico', 'id_nivelensino', 'id_saladeaula', 'id_areaconhecimento')), 'id_areaprojetosala = ' . $to->getid_areaprojetosala());
            $this->commit();
            return $to->getId_areaprojetosala();
        } catch (Exception $e) {
            $this->rollBack();
            throw $e;
            return false;
        }
    }


    /**
     * @param AreaProjetoSalaIntegracaoTO $to
     * @throws Exception
     * @return boolean
     */
    public function editarAreaProjetoSalaIntegracao(AreaProjetoSalaIntegracaoTO $to)
    {
        $orm = new AreaProjetoSalaIntegracaoORM();
        $this->beginTransaction();
        try {
//            die('antes de chamar a função do update');
            $orm->update($to->toArrayUpdate(false, array(
                'id_areaprojetosalaintegracao',
                'id_areaprojetosala',
                'id_projetopedagogico',
                'id_nivelensino',
                'id_saladeaula',
                'id_areaconhecimento'
            )), 'id_areaprojetosalaintegracao = ' . $to->getid_areaprojetosalaintegracao());
            $this->commit();
//            die('antes de chamar a função do update');
            return true;
        } catch (Exception $e) {
            $this->rollBack();
            throw $e;
            return false;
        }
    }


    /*
    * Metodo que chama a sp alocaalunoh7 para integrar os horarios das salas do aluno no H7
    * @param AlocacaoTO $alocacaoTO
    * @return Zend_Db_Table_Rowset_Abstract|false
    */
    public function executaAlocaAlunoH7(SalaDeAulaTO $salaDeAulaTO, VwAlocacaoTO $vwAlocacaoTO)
    {
        $this->beginTransaction();
        try {

            $adapter = Zend_Db_Table_Abstract::getDefaultAdapter();
            $statement = $adapter->prepare('EXECUTE sp_alocaalunoh7 ?, ?');

            $statement->execute(array($salaDeAulaTO->getId_entidade(), $vwAlocacaoTO->getId_usuario()));
            $this->commit();

            return true;
        } catch (Zend_Exception $e) {

            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Retonar as categorias da sala de aula
     * @param CategoriaSalaTO $to
     * @return bool|Zend_Db_Table_Rowset_Abstract
     */
    public function retornarCategoriaSala(CategoriaSalaTO $to)
    {
        $orm = new CategoriaSalaORM();
        return $orm->fetchAll();
    }

    /**
     * @param VwListagemProjetosSalaTO $to
     * @param integer $page
     * @param integer $rowCount
     * @return bool|Zend_Db_Table_Rowset_Abstract
     */
    public function retornaVwListagemProjetosSala(VwListagemProjetosSalaTO $to, $busca, $page = 1, $rowCount = 90000)
    {
        $orm = new VwListagemProjetosSalaORM();
        $where = $orm->montarWhereView($to, false, true);

        try {
//          $obj = $orm->fetchAll($where);
            $select = $orm->select()->distinct(true)->from(array('vw_listagem_projetos_sala'), $busca)
                ->where($where)->limitPage($page, $rowCount)
                ->order('st_projetopedagogico');
            return $orm->fetchAll($select);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }


    /**
     * Retorna as Salas de Aula e Sala de Aula Integração
     * @param unknown_type $where
     * @return boolean|multitype:
     */
    public function retornaSalasBlackBoard($where)
    {
        $orm = new SalaDeAulaORM();
        try {

            if ($where == false) {
                return false;
            }
            $select = $orm->select()
                ->distinct()
                ->from(array('sa' => 'tb_saladeaula'))
                ->setIntegrityCheck(false)
                ->join(array('si' => 'tb_saladeaulaintegracao'), 'si.id_saladeaula = sa.id_saladeaula')
                ->join(array('aps' => 'tb_areaprojetosala'), 'aps.id_saladeaula = sa.id_saladeaula', array())
                ->join(array('ds' => 'tb_disciplinasaladeaula'), 'ds.id_saladeaula = sa.id_saladeaula', array())
                ->join(array('d' => 'tb_disciplina'), 'ds.id_disciplina = d.id_disciplina')
                ->where($where);

//             echo $select->assemble();
//             exit;
            $x = $orm->fetchAll($select, 'id_tipodisciplina');
            if (isset($x) && !is_null($x)) {
                $x = $x->toArray();
            }

            return $x;

        } catch (Exception $e) {
            throw $e;
            return false;
        }
    }

    /**
     * @param int $id_matriculadisciplina
     * @throws Exception
     * @return mixed|boolean
     */
    public function retornarDadosMatriculaBlackBoard($id_matriculadisciplina)
    {

        try {

            $sql = "select mad.id_matriculadisciplina, mad.id_disciplina
							, m.id_matricula, m.id_usuario
							, pro.id_projetopedagogico, pro.st_projetopedagogico, pro.st_descricao
							, a.*
							, aps.*
							, p.*
							, si.id_saladeaulaintegracao, si.id_saladeaula, si.id_sistema, si.st_codsistemacurso, si.st_retornows
							, ui.id_usuariointegracao, ui.st_codusuario, ui.st_senhaintegrada, ui.st_loginintegrado

							from tb_matriculadisciplina as mad
							join tb_matricula as m on (mad.id_matricula = m.id_matricula and mad.id_situacao = 54)
							join tb_projetopedagogico as pro on (pro.id_projetopedagogico = m.id_projetopedagogico)
							join tb_alocacao as a on (a.id_matriculadisciplina = mad.id_matriculadisciplina and a.id_situacao = 55 and a.bl_ativo = 1)
							join vw_areaprojetosala as aps on (aps.id_projetopedagogico = m.id_projetopedagogico and a.id_saladeaula = aps.id_saladeaula)
							join vw_pessoa as p on (p.id_usuario = m.id_usuario and p.id_entidade = m.id_entidadematricula)
							join tb_saladeaulaintegracao as si on (si.id_saladeaula = a.id_saladeaula and si.id_sistema = 15)
							left join tb_usuariointegracao as ui on (ui.id_usuario = m.id_usuario and ui.id_sistema = 15 and ui.id_entidade = m.id_entidadematricula)
							where mad.id_matriculadisciplina = {$id_matriculadisciplina}";

            $orm = new SalaDeAulaORM();
            return $orm->getDefaultAdapter()->query($sql)->fetch();

        } catch (Exception $e) {
            throw $e;
            return false;
        }

    }


    /**
     * Retorna as Alocações pendentes
     * @param int $id_entidade
     * @param int $horas
     * @throws Exception
     * @return mixed|boolean
     */
    public function retornarAlocacaoPendenteBlackBoard($id_entidade, $horas = 120)
    {

        try {

            if (!$horas) $horas = 120;

            $where = '';
            if ($horas) {
                $where = ' AND DATEDIFF(HOUR,a.dt_cadastro,GETDATE()) < ' . $horas;
            }

            if (Ead1_Ambiente::getAmbiente() == Ead1_Ambiente::AMBIENTE_DESENVOLVIMENTO) {
                $top = '4';
            } else {
                $top = '40';
            }

            $sql = "select top $top a.id_alocacao, u.st_nomecompleto, sa.st_saladeaula, s.st_codsistemacurso from tb_saladeaula as sa join tb_saladeaulaintegracao as s
					on (s.id_saladeaula = sa.id_saladeaula)
				join tb_alocacao as a
					on (s.id_saladeaula = a.id_saladeaula) $where
				join tb_matriculadisciplina as md on (a.id_matriculadisciplina = md.id_matriculadisciplina)
				join tb_matricula as m on (m.id_matricula = md.id_matricula)
				join tb_usuario as u on (u.id_usuario = m.id_usuario)
				where s.id_sistema = 15
				and a.bl_ativo = 1 and sa.bl_ativa = 1
				and a.id_situacao = 55
				and m.id_entidadematricula = $id_entidade
				and s.st_codsistemacurso not in ('AGUARDANDOBLACKBOARD','AGUARDANDOVERIFICACAO')
				AND NOT EXISTS( SELECT   ai2.id_alocacao
						FROM     tb_alocacaointegracao AS ai2
                                   WHERE    ai2.id_sistema = 15 AND ai2.id_alocacao = a.id_alocacao)";

            $orm = new SalaDeAulaORM();
//            return $orm->getDefaultAdapter()->query($sql)->fetchAll();

            //Desativando retornos do blackboard 31/03/2016
            return NULL;

        } catch (Exception $e) {
            throw $e;
            return false;
        }

    }


    /**
     * Retorna os Observadores
     * @param int $id_entidade
     * @throws Exception
     * @return Ambigous <multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
     */
    public function retornarObservadoresBlackBoard($id_entidade)
    {

        try {

            if (Ead1_Ambiente::getAmbiente() == Ead1_Ambiente::AMBIENTE_DESENVOLVIMENTO) {
                $top = '4';
            } else {
                $top = '20';
            }

            $sql = "select top $top u.st_nomecompleto, upf.* , tri.st_codsistema, tri.id_saladeaulaintegracao  from tb_UsuarioPerfilEntidadeReferencia as upf
						join tb_usuarioperfilentidade as upe on (upe.id_usuario = upf.id_usuario and upe.id_entidade = upf.id_entidade and upe.id_perfil = upf.id_perfil)
						join tb_perfil as p
							on (p.id_perfil = upf.id_perfil)
							join tb_usuario as u on (u.id_usuario = upf.id_usuario)
							left join tb_PerfilReferenciaIntegracao as tri on (tri.id_perfilreferencia = upf.id_perfilreferencia and tri.id_sistema = 15)
						where p.id_perfilpedagogico = 3
						and p.id_entidade = $id_entidade
						and tri.st_codsistema is null
						and upf.bl_ativo = 1
						and upe.bl_ativo = 1
						and upe.id_situacao = 15
			 			order by st_nomecompleto";

            $orm = new SalaDeAulaORM();
            return $orm->getDefaultAdapter()->query($sql)->fetchAll();

        } catch (Exception $e) {
            throw $e;
            return false;
        }

    }


    /**
     * Retorna os Professores
     * @param int $id_entidade
     * @throws Exception
     * @return Ambigous <multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
     */
    public function retornarProfessoresBlackBoard($id_entidade)
    {

        try {

            if (Ead1_Ambiente::getAmbiente() == Ead1_Ambiente::AMBIENTE_DESENVOLVIMENTO) {
                $top = '4';
            } else {
                $top = '20';
            }

            $sql = "select top $top s.st_saladeaula,  u.st_nomecompleto
				  ,upf.id_perfilreferencia
			      ,upf.id_usuario
			      ,upf.id_entidade
			      ,upf.id_perfil
			      ,upf.id_areaconhecimento
			      ,upf.id_projetopedagogico
			      ,upf.id_saladeaula
			      ,upf.id_disciplina
			      ,upf.bl_ativo
			      ,upf.bl_titular
			      ,upf.nu_porcentagem
			      ,upf.id_livro
			      ,upf.bl_autor
			 , tri.st_codsistema, si.id_saladeaulaintegracao,p.id_perfilpedagogico  from tb_UsuarioPerfilEntidadeReferencia as upf
			 join tb_usuarioperfilentidade as upe on (upe.id_usuario = upf.id_usuario and upe.id_entidade = upf.id_entidade and upe.id_perfil = upf.id_perfil)
						join tb_perfil as p
							on (p.id_perfil = upf.id_perfil)
						join tb_usuario as u on (u.id_usuario = upf.id_usuario)
						join tb_saladeaulaintegracao as si on (si.id_saladeaula = upf.id_saladeaula and si.id_sistema = 15)
						join tb_saladeaula as s on (si.id_saladeaula = s.id_saladeaula)
						left join tb_PerfilReferenciaIntegracao as tri on (tri.id_saladeaulaintegracao = si.id_saladeaulaintegracao and tri.id_perfilreferencia = upf.id_perfilreferencia and tri.id_sistema = 15)
						where p.id_perfilpedagogico = 1
						and   p.id_entidade = $id_entidade
						and   tri.st_codsistema is null
						and   si.st_codsistemacurso not in ('AGUARDANDOBLACKBOARD','AGUARDANDOVERIFICACAO')
						and   upf.bl_ativo = 1
						and   upe.bl_ativo = 1
						and upe.id_situacao = 15
			UNION
			select top 20 vw.st_saladeaula, u.st_nomecompleto
				  ,upf.id_perfilreferencia
			      ,upf.id_usuario
			      ,upf.id_entidade
			      ,upf.id_perfil
			      ,upf.id_areaconhecimento
			      ,upf.id_projetopedagogico
			      ,vw.id_saladeaula
			      ,vw.id_disciplina
			      ,upf.bl_ativo
			      ,upf.bl_titular
			      ,upf.nu_porcentagem
			      ,upf.id_livro
			      ,upf.bl_autor
		   , tri.st_codsistema, si.id_saladeaulaintegracao, p.id_perfilpedagogico from tb_UsuarioPerfilEntidadeReferencia as upf
		   join tb_usuarioperfilentidade as upe on (upe.id_usuario = upf.id_usuario and upe.id_entidade = upf.id_entidade and upe.id_perfil = upf.id_perfil)
						join tb_perfil as p
							on (p.id_perfil = upf.id_perfil)
							join tb_usuario as u on (u.id_usuario = upf.id_usuario)
							join vw_saladeaula as vw on(upf.id_projetopedagogico = vw.id_projetopedagogico)
							join tb_saladeaulaintegracao as si on (si.id_saladeaula = vw.id_saladeaula and si.id_sistema = 15)
							left join tb_PerfilReferenciaIntegracao as tri on (tri.id_saladeaulaintegracao = si.id_saladeaulaintegracao and tri.id_perfilreferencia = upf.id_perfilreferencia and tri.id_sistema = 15)
						where p.id_perfilpedagogico = 2
						and p.id_entidade = $id_entidade
						and si.id_saladeaulaintegracao is not null
						and tri.st_codsistema is null
						and si.st_codsistemacurso not in ('AGUARDANDOBLACKBOARD','AGUARDANDOVERIFICACAO')
						and upf.bl_ativo = 1
						and upe.bl_ativo = 1
						and upe.id_situacao = 15
						order by st_saladeaula,  st_nomecompleto";
// Zend_Debug::dump($sql,__CLASS__.'('.__LINE__.')');exit;
            $orm = new SalaDeAulaORM();
            return $orm->getDefaultAdapter()->query($sql)->fetchAll();

        } catch (Exception $e) {
            throw $e;
            return false;
        }

    }


    /**
     * Retorna os alunos a serem Inativados no BB
     * @param int $id_entidade
     * @throws Exception
     * @return Ambigous <multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
     */
    public function retornarAlunosInativacaoBlackBoard($id_entidade, $id_alocacao = null)
    {
        try {


            if (Ead1_Ambiente::getAmbiente() == Ead1_Ambiente::AMBIENTE_DESENVOLVIMENTO) {
                $top = '4';
            } else {
                $top = '100';
            }

            $sel_data = '';
            $sel_alocacao_n = '';
            $sel_alocacao = '';

            if ($id_alocacao) {
                $sel_alocacao = "and al.id_alocacao = $id_alocacao";
                $sel_alocacao_n = "and 1=2";
                $sel_data = '';
            } else {
//				$sel_data = 'and CAST(GETDATE() AS DATE) > DATEADD(DAY,sa.nu_diasaluno,al.dt_cadastro) AND sa.bl_semdiasaluno = 0';
                $sel_data = 'AND CAST(GETDATE() AS DATE) > CAST(m.dt_termino AS DATE) AND m.dt_termino IS NOT NULL';
            }

            $sql = "SELECT top $top si.st_retornows, ai.id_alocacaointegracao, ai.st_codalocacao, ui.st_codusuario, sa.st_saladeaula, u.st_nomecompleto, '5' as id_perfilpedagogico, null as id_perfilreferenciaintegracao
							FROM dbo.tb_alocacao AS al
										JOIN dbo.tb_saladeaula AS sa ON al.id_saladeaula = sa.id_saladeaula
										JOIN dbo.tb_saladeaulaintegracao as si on si.id_saladeaula = sa.id_saladeaula AND si.id_sistema = 15
										JOIN dbo.tb_alocacaointegracao AS ai ON ai.id_alocacao = al.id_alocacao AND ai.bl_encerrado = 0 AND ai.id_sistema = 15
										Join dbo.tb_matriculadisciplina as md on md.id_matriculadisciplina = al.id_matriculadisciplina
										join dbo.tb_matricula as m on md.id_matricula = m.id_matricula
										join dbo.tb_usuariointegracao as ui on ui.id_usuario = m.id_usuario and ui.id_sistema = 15 and ui.id_entidade = sa.id_entidade
										join dbo.tb_usuario as u on u.id_usuario = ui.id_usuario
										WHERE 1=1 $sel_data
										and sa.id_entidade = $id_entidade $sel_alocacao
					UNION
					select distinct top 20  si.st_retornows, null as id_alocacalintegracao, tri.st_codsistema as st_codalocacao, ui.st_codusuario, sa.st_saladeaula,  u.st_nomecompleto, p.id_perfilpedagogico, id_perfilreferenciaintegracao
										from tb_UsuarioPerfilEntidadeReferencia as upf
										join tb_usuarioperfilentidade as upe on (upe.id_usuario = upf.id_usuario and upe.id_entidade = upf.id_entidade and upe.id_perfil = upf.id_perfil)
										join tb_perfil as p on (p.id_perfil = upf.id_perfil)
										join tb_usuario as u on (u.id_usuario = upf.id_usuario)
										join vw_saladeaula as sa on (sa.id_saladeaula = upf.id_saladeaula or sa.id_projetopedagogico = upf.id_projetopedagogico)
										join tb_saladeaulaintegracao as si on (si.id_saladeaula = sa.id_saladeaula and si.id_sistema = 15)
										join tb_usuariointegracao as ui on ui.id_usuario = u.id_usuario and ui.id_sistema = 15 and ui.id_entidade = sa.id_entidade
										join tb_PerfilReferenciaIntegracao as tri on (tri.id_saladeaulaintegracao = si.id_saladeaulaintegracao and tri.id_perfilreferencia = upf.id_perfilreferencia and tri.id_sistema = 15)
										where p.id_perfilpedagogico in (1,2,3)
											and p.id_entidade = $id_entidade
											and tri.st_codsistema is not null
											and si.st_codsistemacurso not in ('AGUARDANDOBLACKBOARD','AGUARDANDOVERIFICACAO')
											and (upf.bl_ativo = 0 or upe.bl_ativo = 0 -- or upe.id_situacao = 18
											)
											and tri.bl_encerrado = 0 $sel_alocacao_n
					order by sa.st_saladeaula, u.st_nomecompleto";
// 			Zend_Debug::dump($sql,__CLASS__.'('.__LINE__.')');exit;
            $orm = new SalaDeAulaORM();
            return $orm->getDefaultAdapter()->query($sql)->fetchAll();

        } catch (Exception $e) {
            throw $e;
            return false;
        }

    }


    /**
     * Retorna os Alunos sem Grupo no BB
     * @param unknown_type $id_entidade
     * @param unknown_type $id_matricula
     * @throws Exception
     * @return Ambigous <multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
     */
    public function retornaAlunosSemGrupoBB($id_entidade = null, $id_matricula = null)
    {

        try {

            if (Ead1_Ambiente::getAmbiente() == Ead1_Ambiente::AMBIENTE_DESENVOLVIMENTO) {
                $top = '4';
            } else {
                $top = '50';
            }

            $sel = $id_entidade ? "and m.id_entidadematricula = $id_entidade" : '';


            $sql = "select top $top a.id_alocacao, m.id_entidadematricula as id_entidade, u.st_nomecompleto, u.st_nomecompleto, ui.st_codusuario, ui.st_loginintegrado
						, s.st_saladeaula as st_curso
						, pp.st_projetopedagogico as st_grupo
						, aps.st_referencia as st_groupid
						, ai.id_alocacaointegracao, ai.id_alocacao, ai.st_codalocacao, ai.st_codalocacaogrupo, a.id_saladeaula, a.id_matriculadisciplina
						, si.st_codsistemacurso, si.st_retornows
						, m.id_projetopedagogico, m.id_entidadematricula, u.id_usuario

							from tb_alocacao as a
                            join tb_matriculadisciplina as md on a.id_matriculadisciplina = md.id_matriculadisciplina
                            join tb_matricula as m on (m.id_matricula = md.id_matricula)
                            JOIN tb_usuario as u ON (u.id_usuario = m.id_usuario)
                            join tb_saladeaula as s on a.id_saladeaula = s.id_saladeaula
                            join tb_projetopedagogico as pp on (pp.id_projetopedagogico = m.id_projetopedagogico)
                            join tb_areaprojetosala as apsala on (apsala.id_projetopedagogico = m.id_projetopedagogico) and (s.id_saladeaula = apsala.id_saladeaula)
                            left join tb_saladeaulaintegracao as si on (si.id_saladeaula = a.id_saladeaula and si.id_sistema = 15)
                            left join tb_usuariointegracao as ui on (ui.id_usuario = u.id_usuario and ui.id_sistema = si.id_sistema and ui.id_entidade = m.id_entidadematricula)
                            left join tb_areaprojetosalaintegracao as aps on (aps.id_projetopedagogico = m.id_projetopedagogico and aps.id_saladeaula = a.id_saladeaula)
                            left join tb_alocacaointegracao as ai on (ai.id_alocacao = a.id_alocacao and ai.id_sistema = si.id_sistema)
                            where ui.id_sistema = 15
							and ai.st_codalocacaogrupo is null
							and ai.st_codalocacao is not null
							and a.id_situacao = 55
							and a.bl_ativo = 1
							and pp.bl_ativo = a.bl_ativo
							and pp.id_situacao = 7
							and s.id_situacao = 8
							and s.bl_ativa = a.bl_ativo
							and m.id_situacao in (50, 120)
							and m.id_evolucao in (6, 26)
							$sel
							order by ai.dt_cadastro, s.st_saladeaula, pp.st_projetopedagogico, u.st_nomecompleto";

            $orm = new SalaDeAulaORM();
            return $orm->getDefaultAdapter()->query($sql)->fetchAll();

        } catch (Exception $e) {
            throw $e;
            return false;
        }

    }

    /**
     * Retorna Projetos da view Gestao de sala de aulas
     * Especifico porque valido o perfil do usuario coordenador
     * @param VwAreaProjetoSalaTO $apsTO
     * @return bool|Zend_Db_Table_Rowset_Abstract
     */
    public function retornarVwAreaProjetoSalaGestao(VwAreaProjetoSalaTO $apsTO)
    {
        $areaProjetoSalaORM = new VwAreaProjetoSalaORM();
        $where = "id_saladeaula = {$apsTO->getId_saladeaula()}  AND  (id_perfilpedagogico = {$apsTO->getId_perfilpedagogico()} OR id_perfilpedagogico is null ) ";

        try {
            $obj = $areaProjetoSalaORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Retorna as disciplinas que o aluno será desalocado na transferencia
     * Na transferencia de curso, não desaloca o aluno das salas que ele foi encerrado (id_encerramentosala IS NULL)
     * @param VwSalaDisciplinaAlocacaoTO $vwSdaTO
     * @param null $where
     * @return bool|Zend_Db_Table_Rowset_Abstract
     * @throws Zend_Exception
     */
    public function retornaDisciplinasParaDesalocarNaTransferenciaCurso(VwSalaDisciplinaAlocacaoTO $vwSdaTO, $where = null)
    {
        $vwSalaDisciplinaAlocacaoORM = new VwSalaDisciplinaAlocacaoORM();
        if (!$where) {
            $where = $vwSalaDisciplinaAlocacaoORM->montarWhereView($vwSdaTO, false, true);
        }
        try {
            $select = $vwSalaDisciplinaAlocacaoORM->select()
                ->distinct(true)
                ->from(array('vw' => 'vw_saladisciplinaalocacao'),
                    array('id_saladeaula', 'st_saladeaula', 'id_matriculadisciplina', 'id_alocacao', 'dt_abertura', 'st_projetopedagogico'))
                ->joinLeft(array('tb' => 'tb_encerramentoalocacao'), 'tb.id_alocacao = vw.id_alocacao', array())
                ->where('tb.id_encerramentosala IS NULL')
                ->where($where);

            $obj = $vwSalaDisciplinaAlocacaoORM->fetchAll($select);
            $this->excecao = $select->assemble();
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }


}

<?php
/**
 * Classe que busca dados de login em banco de dados
 * @author edermariano
 * 
 * @package models
 * @subpackage dao
 */
class LoginDAO extends Ead1_DAO{

	/**
	 * Método que verifica se usuário e senha existem
	 * 
	 * @param string $usuario
	 * @param string $senha
	 */
	public function verificarUsuario($usuario, $senha){
		$tbUsuario = new UsuarioORM();
		return $tbUsuario->fetchRow("st_login = '$usuario' AND st_senha = '$senha'");
	}

	/**
	 * Método que verifica dados do usuário simulado
	 * 
	 * @param string $usuario
	 */
	public function verificarUsuarioSimulado($usuario){
		$tbUsuario = new UsuarioORM();
		return $tbUsuario->fetchRow("st_login = '$usuario'");
	}
	
	/**
	 * Método que retorna os perfils do usuário através de seu ID
	 * @param int $idUsuario
	 */
	public function retornaUsuarioPerfilEntidade($idUsuario){
		$tbUsuarioPerfilEntidade = new VwUsuarioPerfilEntidadeORM();
		return $tbUsuarioPerfilEntidade->fetchAll("id_usuario = $idUsuario AND bl_ativo = 1",'st_nomeentidade')->toArray();
	}
	
	/**
	 * Método que retorna os dados pessoais do usuários com o perfil selecionado
	 * 
	 * @param int $idUsuario
	 * @param int $idEntidade
	 * @param int $idPerfil
	 * @return array
	 */
	public function retornaDadosUsuario($idUsuario, $idEntidade, $idPerfil){
                
                $tbUsuarioPerfilEntidade = new VwUsuarioPerfilEntidadeORM();
		$rs = $tbUsuarioPerfilEntidade->fetchRow("id_usuario = $idUsuario AND id_perfil = $idPerfil AND id_entidade = $idEntidade");                
                if($rs) {
                    return $rs->toArray();
                } else {
                    return false;
                }
	}
	
}
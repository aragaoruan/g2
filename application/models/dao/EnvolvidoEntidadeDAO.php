<?php
/**
 * Classe de persistencia de envolvido de entidade com o banco de dados
 * @author Eduardo Romão <ejushiro@gmail.com>
 * @since 18/10/2011
 * 
 * @package models
 * @subpackage dao
 */
class EnvolvidoEntidadeDAO extends Ead1_DAO {

	/**
	 * Metodo que cadastra um envolvido da entidade
	 * @param EnvolvidoEntidadeTO $eeTO
	 * @see EnvolvidoEntidadeBO::cadastrarEnvolvidoEntidade
	 * @return int $id_envolvidoentidade | false
	 */
	public function cadastrarEnvolvidoEntidade(EnvolvidoEntidadeTO $eeTO){
		try{
			$this->beginTransaction();
			$orm = new EnvolvidoEntidadeORM();
			$insert = $eeTO->toArrayInsert();
			$id_envolvidoentidade = $orm->insert($insert);
			$this->commit();
			return $id_envolvidoentidade;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($this->excecao);
			return false;
		}
	}
	
	/**
	 * Metodo que edita um envolvido de entidade
	 * @param EnvolvidoEntidadeTO $eeTO
	 * @see EnvolvidoEntidadeBO::editarEnvolvidoEntidade
	 * @return boolean
	 */
	public function editarEnvolvidoEntidade(EnvolvidoEntidadeTO $eeTO, $where = null){
		try{
			$this->beginTransaction();
			$orm = new EnvolvidoEntidadeORM();
			if(!$where){
				$where = $orm->montarWhere($eeTO,true);
			}
			$update = $eeTO->toArrayUpdate(false,array('id_envolvidoentidade'));
			$orm->update($update, $where);
			$this->commit();
			return true;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($this->excecao);
			return false;
		}
	}
	
	/**
	 * Metodo que retorna envolvido de entidade
	 * @param EnvolvidoEntidadeTO $eeTO
	 * @see EnvolvidoEntidadeBO::retornarEnvolvidoEntidade
	 * @return boolean
	 */
	public function retornarEnvolvidoEntidade(EnvolvidoEntidadeTO $eeTO){
		$orm = new EnvolvidoEntidadeORM();
		$where = $orm->montarWhere($eeTO);
		try{
			$obj = $orm->fetchAll($where);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		} 
	}
	
	/**
	 * Metodo que retorna envolvido de entidade com o nome da pessoa e tipo de envolvido
	 * @param EnvolvidoEntidadeTO $eeTO
	 * @see EnvolvidoEntidadeBO::retornarEnvolvidoEntidadePessoa
	 * @return boolean
	 */
	public function retornarEnvolvidoEntidadePessoa(EnvolvidoEntidadeTO $eeTO){
		$envolvidoEntidadeORM = new EnvolvidoEntidadeORM();
		$where = $envolvidoEntidadeORM->montarWhere($eeTO);
		$select = $envolvidoEntidadeORM->select()
				->distinct(true)
				->from(array('ee' => 'tb_envolvidoentidade'), array('id_entidade', 'id_usuario', 'id_envolvidoentidade', 'id_tipoenvolvido', 'bl_ativo'))
				->setIntegrityCheck(false)
				->join(array('u' => 'tb_usuario'), 'u.id_usuario = ee.id_usuario', 'st_nomecompleto')
				->join(array('te' => 'tb_tipoenvolvido'), 'te.id_tipoenvolvido = ee.id_tipoenvolvido', 'st_tipoenvolvido');
		if($where){
			$select = $select->where('ee.bl_ativo = 1');
		}
		try{
			$obj = $envolvidoEntidadeORM->fetchAll($select);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			Zend_Debug::dump($this->excecao);exit;
			return false;
		} 
	}
	
	/**
	 * Metodo que retorna a view de envolvidos da entidade
	 * @param VwEntidadeEnvolvidoTO $to
	 * @param Mixed $where
	 * @return Zend_Db_Table_Rowset_Abstract
	 */
	public function retornarVwEntidadeEnvolvido(VwEntidadeEnvolvidoTO $to,$where = null){
		try{
			$orm = new VwEntidadeEnvolvidoORM();
			if(!$where){
				$where = $orm->montarWhereView($to,false,true,false);
			}
			return $orm->fetchAll($where);
		}catch (Zend_Exception $e){
			$this->excecao = $e->getMessage();
			THROW $e;
			return false;
		}
	}
	
	/**
	 * Metodo que exclui envolvido de entidade
	 * @param EnvolvidoEntidadeTO $eeTO
	 * @return boolean
	 */
	public function excluirEnvolvidoEntidade(EnvolvidoEntidadeTO $eeTO){
		try{
			$this->beginTransaction();
			$orm = new EnvolvidoEntidadeORM();
			$where = $orm->montarWhere($eeTO);
			$orm->delete($where);
			$this->commit();
			return true;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($this->excecao);
			return false;
		}
	}
}

?>
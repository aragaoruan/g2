<?php

class ProtocoloDAO extends Ead1_DAO {

	/**
	 * Método que cadastra protocolo.
	 * @param ProtocoloTO $pTO
	 * @return boolean
	 */
	public function cadastrarProtocolo(ProtocoloTO $pTO){
		try{
			$this->beginTransaction();
			$orm = new ProtocoloORM();
			$insert = $pTO->toArrayInsert();
			$id_protocolo = $orm->insert($insert);
			$this->commit();
			return $id_protocolo;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($this->excecao);
			return false;
		}
	}
	
	/**
	 * Método que retorna protocolo
	 * @param ProtocoloTO $pTO
	 * @param Mixed $where
	 * @return Zend_Db_Table_Row_Abstract
	 */
	public function retornarProtocolo(ProtocoloTO $pTO, $where = null){
		try{
			$orm = new ProtocoloORM();
			if(!$where){
				$where = $orm->montarWhere($pTO);
			}
			return $orm->fetchAll($where);
		}catch (Zend_Exception $e){
			THROW $e;
			return false;
		}
	}
	
/**
	 * Método que retorna protocolo com o nome do usuario.
	 * @param ProtocoloTO $nTO
	 * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function retornarProtocoloUsuario(ProtocoloTO $nfTO, $where = null){
		$orm = new ProtocoloORM();
		$select = $orm->select()
				->distinct(true)
				->from(array('p' => 'tb_protocolo'), array('*'))
				->setIntegrityCheck(false)
				->join(array('u' => 'tb_usuario'), 'u.id_usuario = p.id_usuariocadastro', array('st_nomecompleto'));
				
		if(!$where){
			$where = $orm->montarWhere($nfTO);
			if($where){
				$where = 'p.'.$where;
			}
		}
		try{
			$obj = $orm->fetchAll($select->where($where));
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		} 
	}
	
/**
	 * Método que retorna tipos de função.
	 * @param FuncionalidadeTO $fTO
	 * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function retornarFuncaoTipo(FuncionalidadeTO $fTO){
		$funcionalidadeORM = new FuncionalidadeORM();
		$where = $funcionalidadeORM->montarWhere($fTO);
		$select = $funcionalidadeORM->select()
				->distinct(true)
				->from(array('fn' => 'tb_funcionalidade'), array('id_funcionalidade', 'st_funcionalidade', 'bl_ativo', 'id_situacao', 'id_funcionalidadepai'))
				->setIntegrityCheck(false)
				->join(array('ff' => 'tb_funcaofuncionalidade'), 'fn.id_funcionalidade = ff.id_funcionalidade', '');
		if($where){
			$select = $select->where($where);
		}
		try{
			$obj = $funcionalidadeORM->fetchAll($select);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		} 
	}
}

?>
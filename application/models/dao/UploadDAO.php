<?php
/**
 * Classe de persistencia da ocorrencia com o banco de dados
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage dao
 */
class UploadDAO extends Ead1_DAO {

	
	/**
	 * Método que cadastra os dados do arquivo em tabela
	 * @param UploadTO $imagem
	 * @throws Zend_Exception
	 * @return Ambigous <mixed, multitype:>|boolean
	 */
	public function cadastrarUpload(UploadTO $to){
		$orm = new UploadORM();
		$this->beginTransaction();
		try {
			$id = $orm->insert($to->toArrayInsert());
			$this->commit();
			return $id;
		} catch (Zend_Exception $e) {
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW $e;
		}
	}
	

}


<?php


/**
 * Classe de acesso a dados referente a ArquivoRetornoDAO
 * @author Elcio Mauo Guimarães - <elcioguimaraes@gmail.com>
 * @since 16/10/2012
 * @package models
 * @subpackage dao
 */
class ArquivoRetornoDAO extends Ead1_DAO {

	/**
	 * Método que insere o ArquivoRetorno
	 * 
	 * @param ArquivoRetornoTO $arquivoRetornoTO
	 * @throws Zend_Exception
	 * @return Ambigous <mixed, multitype:>
	 * @see ArquivoRetornoBO::salvarArquivoRetorno();
	 */
	public function cadastrarArquivoRetorno(ArquivoRetornoTO $arquivoRetornoTo){
		$this->beginTransaction();
		try{
			$orm = new ArquivoRetornoORM();
			$id_arquivoretorno = $orm->insert($arquivoRetornoTo->toArrayInsert());
			$this->commit();
			return $id_arquivoretorno;
		}catch(Zend_Exception $e){
			$this->rollBack();
			throw $e;
		}	
	}
	
	/**
	 * Método de edição do ArquivoRetorno
	 * @param ArquivoRetornoTO $arquivoRetornoTo
	 * @throws Zend_Exception
	 * @see ArquivoRetornoBO::salvarArquivoRetorno();
	 */
	public function editarArquivoRetorno(ArquivoRetornoTO $arquivoRetornoTo){
		$this->beginTransaction();
		try {
			$orm = new ArquivoRetornoORM();
			$orm->update($arquivoRetornoTo->toArrayUpdate(false, array('id_arquivoretorno')), $orm->montarWhere($arquivoRetornoTo, true));
			$this->commit();
		} catch (Zend_Exception $e) {
			$this->rollBack();
			throw $e;
		}
	}
	
	/**
	 * Método de listagem de ArquivoRetorno
	 * @param ArquivoRetornoTO $arquivoRetornoTo
	 * @return Ambigous <boolean, multitype:>
	 * @see ArquivoRetornoBO::listarArquivoRetorno();
	 */
	public function listarArquivoRetorno(ArquivoRetornoTO $arquivoRetornoTo){
		$orm = new ArquivoRetornoORM();
		return $orm->consulta($arquivoRetornoTo);
	}


	/**
	 * Método que insere o ArquivoRetornoLancamento
	 * 
	 * @param ArquivoRetornoLancamentoTO $arquivoRetornoLancamentoTO
	 * @throws Zend_Exception
	 * @return Ambigous <mixed, multitype:>
	 * @see ArquivoRetornoLancamentoBO::salvarArquivoRetornoLancamento();
	 */
	public function cadastrarArquivoRetornoLancamento(ArquivoRetornoLancamentoTO $arquivoRetornoLancamentoTo){
		$this->beginTransaction();
		try{
			$orm = new ArquivoRetornoLancamentoORM();
			$id_arquivoretornolancamento = $orm->insert($arquivoRetornoLancamentoTo->toArrayInsert());
			$this->commit();
			return $id_arquivoretornolancamento;
		}catch(Zend_Exception $e){
			$this->rollBack();
			throw $e;
		}	
	}
	
	/**
	 * Método de edição do ArquivoRetornoLancamento
	 * @param ArquivoRetornoLancamentoTO $arquivoRetornoLancamentoTo
	 * @throws Zend_Exception
	 * @see ArquivoRetornoLancamentoBO::salvarArquivoRetornoLancamento();
	 */
	public function editarArquivoRetornoLancamento(ArquivoRetornoLancamentoTO $arquivoRetornoLancamentoTo){
		$this->beginTransaction();
		try {
			$orm = new ArquivoRetornoLancamentoORM();
			$orm->update($arquivoRetornoLancamentoTo->toArrayUpdate(false, array('id_arquivoretornolancamento')), $orm->montarWhere($arquivoRetornoLancamentoTo, true));
			$this->commit();
		} catch (Zend_Exception $e) {
			$this->rollBack();
			throw $e;
		}
	}
	
	/**
	 * Método de listagem de ArquivoRetornoLancamento
	 * @param ArquivoRetornoLancamentoTO $arquivoRetornoLancamentoTo
	 * @return Ambigous <boolean, multitype:>
	 * @see ArquivoRetornoLancamentoBO::listarArquivoRetornoLancamento();
	 */
	public function listarArquivoRetornoLancamento(ArquivoRetornoLancamentoTO $arquivoRetornoLancamentoTo){
		$orm = new ArquivoRetornoLancamentoORM();
		return $orm->consulta($arquivoRetornoLancamentoTo);
	}
	
	
	/**
	 * Retorna os dados da Vw_retornolancamento
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @param VwRetornoLancamentoTO $to
	 * @param unknown_type $where
	 * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >
	 */
	public function retornarVwRetornoLancamento(VwRetornoLancamentoTO $to, array $where = null){
		$orm = new VwRetornoLancamentoORM();
		$whereTO = $orm->montarWhereView($to,false,true);

		if($where){
			foreach($where as $cond){
				$whereTO .= ' AND '.$cond;
			}
		}

		$retorno = $orm->fetchAll($whereTO);
		return Ead1_TO_Dinamico::encapsularTo($retorno, $to);
	}
	
}
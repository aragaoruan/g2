<?php
/**
 * Classe de persistencia do LocalAula
 * @author Paulo Silva - paulo.silva@unyleya.com.br
 * 
 * @package models
 * @subpackage dao
 */
class LocalAulaDAO extends Ead1_DAO {
	
	/**
	 * Metodo que Cadastra Local da Aula
	 * 
	 * @param LocalAulaTO $laTO        	
	 * @see LocalAulaBO::cadastrarLocalAula();
	 * @return int false o valor do id inserido no banco ou false senão conseguir
	 */
	public function cadastrarLocalAula(LocalAulaTO $laTO) {
		$localAulaORM = new LocalAulaORM ();
		$this->beginTransaction ();
		try {
			$id_localAula = $localAulaORM->insert ( $laTO->toArrayInsert () );
			$this->commit ();
			return $id_localAula;
		} catch ( Zend_Exception $e ) {
			$this->rollBack ();
			$this->excecao = $e->getMessage ();
			return false;
		}
	}
	
	/**
	 * Metodo que Edita o Local da Aula
	 * 
	 * @see LocalAulaBO::editarLocalAula();
	 * @return boolean
	 */
	public function editarLocalAula(LocalAulaTO $laTO) {
		$localAulaORM = new LocalAulaORM ();
		$this->beginTransaction ();
		try {
			$localAulaORM->update ( $laTO->toArrayUpdate ( false, array (
					'id_localaula' 
			) ), 'id_localaula = ' . $laTO->getId_localaula () );
			$this->commit ();
			return true;
		} catch ( Zend_Exception $e ) {
			$this->rollBack ();
			$this->excecao = $e->getMessage ();
			return false;
		}
	}
	
	/**
	 * Metodo que Exclui o Local da Aula
	 * 
	 * @see LocalAulaBO::deletarLocalAula();
	 * @return boolean
	 */
	public function deletarLocalAula(LocalAulaTO $laTO) {
		$localAulaORM = new LocalAulaORM ();
		$where = 'id_localaula = ' . $laTO->getId_localaula ();
		$this->beginTransaction ();
		try {
			$localAulaORM->delete ( $where );
			$this->commit ();
			return true;
		} catch ( Zend_Exception $e ) {
			$this->rollBack ();
			$this->excecao = $e->getMessage ();
			return false;
		}
	}
	
	/**
	 * Metodo que Retorna o Local da Aula
	 * 
	 * @param LocalAulaTO $laTO        	
	 * @see LocalAulaBO::retornarLocalAula();
	 * @return Zend_Db_Table_Rowset_Abstract false
	 */
	public function retornarLocalAula(LocalAulaTO $laTO) {
		$localAulaORM = new LocalAulaORM ();
		try {
			$obj = $localAulaORM->fetchAll ( $localAulaORM->montarWhere ( $laTO ) );
			return $obj;
		} catch ( Zend_Exception $e ) {
			$this->excecao = $e->getMessage ();
			return false;
		}
	}
}
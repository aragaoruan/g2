<?php
/**
 * Classe de persistência de função ao banco de dados
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage dao
 */
class FuncaoDAO extends Ead1_DAO {

	/**
	 * Método que retorna funcionalidade da função.
	 * @param FuncaoFuncionalidadeTO $ffTO
	 * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function retornarFuncaoFuncionalidade(FuncaoFuncionalidadeTO $ffTO){
		$funcaoFuncionalidadeORM = new FuncaoFuncionalidadeORM();
		$where = $funcaoFuncionalidadeORM->montarWhere($ffTO);
		$select = $funcaoFuncionalidadeORM->select()
				->from(array('fn' => 'tb_funcao'), array('*'))
				->setIntegrityCheck(false)
				->join(array('ff' => 'tb_funcaofuncionalidade'), 'fn.id_funcao = ff.id_funcao', array('id_funcionalidade'))
				->join(array('fc' => 'tb_funcionalidade'), 'fc.id_funcionalidade = ff.id_funcionalidade', array('st_funcionalidade'));
		if($where){
			$where = 'ff.'.$where;
			$select = $select->where($where);
		}
		try{
			$obj = $funcaoFuncionalidadeORM->fetchAll($select);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		} 
	}
	
	/**
	 * Método que retorna a função 
	 * @param FuncaoTO $to
	 * @param Mixed $where
	 * @return Zend_Db_Table_Row_Abstract
	 */
	public function retornarFuncao(FuncaoTO $to,$where = null){
		try{
			$orm = new FuncaoORM();
			if(!$where){
				$where = $orm->montarWhere($to);
			}
			return $orm->fetchAll($where);
		}catch (Zend_Exception $e){
			THROW $e;
			return false;
		}
	}

	/**
	 * Método que retorna função com dados adicionais.
	 * @param FuncaoTO $fTO
	 * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function retornarFuncaoCompleto(FuncaoTO $fTO){
		$orm = new FuncaoORM();
		$where = $orm->montarWhere($fTO);
		$select = $orm->select()
				->distinct(true)
				->from(array('fc' => 'tb_funcao'), array('*'))
				->setIntegrityCheck(false)
				->join(array('tf' => 'tb_tipofuncao'), 'fc.id_tipofuncao = tf.id_tipofuncao', 'st_tipofuncao');
		if($where){
			$select = $select->where($where);
		}
		try{
			$obj = $orm->fetchAll($select);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		} 
	}
	
	/**
	 * Método que retorna perfis de funcao.
	 * @param FuncaoPerfilTO $fpTO
	 * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function retornarFuncaoPerfil(FuncaoPerfilTO $fpTO){
		$funcaoPerfilORM = new FuncaoPerfilORM();
		$where = $funcaoPerfilORM->montarWhere($fpTO);
		try{
			$obj = $funcaoPerfilORM->fetchAll($where);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			Zend_Debug::dump($e->getMessage());
			return false;
		} 
	}
	
	/**
	 * Método que retorna tipo de função.
	 * @param TipoFuncaoTO $tfTO
	 * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function retornarTipoFuncao(TipoFuncaoTO $tfTO){
		$orm = new TipoFuncaoORM();
		$where = $orm->montarWhere($tfTO);
		try{
			$obj = $orm->fetchAll($where);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			Zend_Debug::dump($e->getMessage());
			return false;
		} 
	}
	
	/**
	 * Método que cadastra perfil de funcao.
	 * @param FuncaoPerfilTO $fpTO
	 * @return boolean
	 */
	public function cadastrarFuncaoPerfil(FuncaoPerfilTO $fpTO){
		try{
			$this->beginTransaction();
			$orm = new FuncaoPerfilORM();
			$insert = $fpTO->toArrayInsert();
			$orm->insert($insert);
			$this->commit();
			return true;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($this->excecao);
			return false;
		}
	}
	
/**
	 * Metodo que exclui perfil de função.
	 * @param FuncaoPerfilTO $fpTO
	 * @return boolean
	 */
	public function excluirFuncaoPerfil(FuncaoPerfilTO $fpTO){
		try{
			$this->beginTransaction();
			$orm = new FuncaoPerfilORM();
			$where = $orm->montarWhere($fpTO);
			$orm->delete($where);
			$this->commit();
			return true;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($this->excecao);
			return false;
		}
	}
	
	/**
	 * Metodo que retorna view de função de usuário.
	 * @param VwFuncaoUsuarioTO $vfuTO
	 * @return boolean
	 */
	public function retornarVwFuncaoUsuario(VwFuncaoUsuarioTO $vfuTO){
		$orm = new VwFuncaoUsuarioORM();
		$where = $orm->montarWhere($vfuTO);
		try{
			$obj = $orm->fetchAll($where);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			Zend_Debug::dump($e->getMessage());
			return false;
		} 
	}
	
	
	
	
}

?>
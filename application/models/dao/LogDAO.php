<?php
/**
 * Classe de persistencia da ocorrencia com o banco de dados
 * @author Denise Xavier <denisexavier@ead1.com.br>
 * @package models
 * @subpackage dao
 */
class LogDAO extends Ead1_DAO {

	/**
	 * Método que salva um log
	 * @param LogAcessoTO $to
	 * @throws Zend_Exception
	 * @return Ambigous <mixed, multitype:>|boolean
	 */
	public function salvarLog(LogAcessoTO $to){
		$ORM = new LogAcessoORM();
		try {
			$insert = $to->toArrayInsert();
			$id = $ORM->insert($insert);
			return $id;
		} catch (Zend_Exception $e) {
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($e->getMessage());
			return false;
		}
	}
	
	/**
	 * Método de listagem de VwLogMatricula
	 * @param VwLogMatriculaTO $to
	 * @return Ambigous <boolean, multitype:>
	 * @see listarVwLogMatricula::listarVwLogMatricula();
	 */
	public function listarVwLogMatricula(VwLogMatriculaTO $to, $where = null){
		$orm = new VwLogMatriculaORM();
		if(empty($where)){
			$where = $orm->montarWhereView($to, false, true);
		}
		try{
			$obj = $orm->fetchAll($where,'dt_cadastro desc')->toArray();
			if($obj){
				return Ead1_TO_Dinamico::encapsularTo($obj, $to);
			}
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		} 
	}
	
}



<?php
/**
 * Classe de persistência de comissão ao banco de dados
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage dao
 */
class ComissaoDAO extends Ead1_DAO {
	
	/**
	 * Metodo que retorna comissão com sua função e categoria
	 * @param ComissaoEntidadeTO $ceTO
	 * @return boolean
	 */
	public function retornarComissaoEntidadeFuncao(ComissaoEntidadeTO $ceTO){
		$orm= new ComissaoEntidadeORM();
		$where = $orm->montarWhere($ceTO);
		$select = $orm->select()
				->from(array('ce' => 'tb_comissaoentidade'), array('*'))
				->setIntegrityCheck(false)
				->join(array('fn' => 'tb_funcao'), 'fn.id_funcao = ce.id_funcao', array('st_funcao'))
				->join(array('tf' => 'tb_tipofuncao'), 'tf.id_tipofuncao = fn.id_tipofuncao', array('st_tipofuncao', 'id_tipofuncao'));
		if($where){
			$where = 'ce.'.$where;
			$select = $select->where($where);
		}
		try{
			$obj = $orm->fetchAll($select);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		} 
	}

	/**
	 * Método que retorna comissão.
	 * @param ComissaoEntidadeTO $ceTO
	 * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function retornarComissaoEntidade(ComissaoEntidadeTO $ceTO){
		$orm = new ComissaoEntidadeORM();
		$where = $orm->montarWhere($ceTO);
		try{
			$obj = $orm->fetchAll($where);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			Zend_Debug::dump($e->getMessage());
			return false;
		} 
	}
	
	/**
	 * Método que retorna tipo de valor de comissão.
	 * @param TipoValorComissaoTO $tvcTO
	 * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function retornarTipoValorComissao(TipoValorComissaoTO $tvcTO){
		$orm = new TipoValorComissaoORM();
		$where = $orm->montarWhere($tvcTO);
		try{
			$obj = $orm->fetchAll($where);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			Zend_Debug::dump($e->getMessage());
			return false;
		} 
	}

	/**
	 * Método que retorna tipo de comissão.
	 * @param TipoComissaoTO $tcTO
	 * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function retornarTipoComissao(TipoComissaoTO $tcTO){
		$orm = new TipoComissaoORM();
		$where = $orm->montarWhere($tcTO);
		try{
			$obj = $orm->fetchAll($where);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			Zend_Debug::dump($e->getMessage());
			return false;
		} 
	}

	/**
	 * Método que retorna tipo de meta.
	 * @param TipoMetaTO $tmTO
	 * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function retornarTipoMeta(TipoMetaTO $tmTO){
		$orm = new TipoMetaORM();
		$where = $orm->montarWhere($tmTO);
		try{
			$obj = $orm->fetchAll($where);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			Zend_Debug::dump($e->getMessage());
			return false;
		} 
	}

	/**
	 * Método que retorna base de calculo da comissão.
	 * @param BaseCalculoComTO $bccTO
	 * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function retornarBaseCalculoCom(BaseCalculoComTO $bccTO){
		$orm = new BaseCalculoComORM();
		$where = $orm->montarWhere($bccTO);
		try{
			$obj = $orm->fetchAll($where);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			Zend_Debug::dump($e->getMessage());
			return false;
		} 
	}

	/**
	 * Método que retorna tipo de cálculo de comissão.
	 * @param TipoCalculoComTO $tccTO
	 * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function retornarTipoCalculoCom(TipoCalculoComTO $tccTO){
		$orm = new TipoCalculoComORM();
		$where = $orm->montarWhere($tccTO);
		try{
			$obj = $orm->fetchAll($where);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			Zend_Debug::dump($e->getMessage());
			return false;
		} 
	}

	/**
	 * Método que retorna regra de comissão.
	 * @param RegraComissaoTO $rcTO
	 * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function retornarRegraComissao(RegraComissaoTO $rcTO){
		$orm = new RegraComissaoORM();
		$where = $orm->montarWhere($rcTO);
		try{
			$obj = $orm->fetchAll($where);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			Zend_Debug::dump($e->getMessage());
			return false;
		} 
	}
	

	/**
	 * Método que cadastra regra de comissão.
	 * @param RegraComissaoTO $rcTO
	 * @return boolean
	 */
	public function cadastrarRegraComissao(RegraComissaoTO $rcTO){
		try{
			$this->beginTransaction();
			$orm = new RegraComissaoORM();
			$insert = $rcTO->toArrayInsert();
			$chave = $orm->insert($insert);
			$this->commit();
			return $chave;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($this->excecao);
			return false;
		}
	}

	/**
	 * Método que cadastra comissão.
	 * @param ComissaoEntidadeTO $ceTO
	 * @return boolean
	 */
	public function cadastrarComissaoEntidade(ComissaoEntidadeTO $ceTO){
		try{
			$this->beginTransaction();
			$orm = new ComissaoEntidadeORM();
			$insert = $ceTO->toArrayInsert();
			$chave = $orm->insert($insert);
			$this->commit();
			return $chave;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($this->excecao);
			return false;
		}
	}

	/**
	 * Método que edita regra de comissão.
	 * @param RegraComissaoTO $rcTO
	 * @return boolean
	 */
	public function editarRegraComissao(RegraComissaoTO $rcTO, $where = null){
		try{
			$this->beginTransaction();
			$orm = new RegraComissaoORM();
			if(!$where){
				$where =  $orm->montarWhere($rcTO);
			}
			$update = $rcTO->toArrayUpdate(false, array('id_regracomissao', 'id_comissaoentidade', 'dt_cadastro'));
			$orm->update($update, $where);
			$this->commit();
			return true;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($this->excecao);
			return false;
		}
	}

	/**
	 * Método que edita comissão.
	 * @param ComissaoEntidadeTO $ceTO
	 * @return boolean
	 */
	public function editarComissaoEntidade(ComissaoEntidadeTO $ceTO, $where = null){
		try{
			$this->beginTransaction();
			$orm = new ComissaoEntidadeORM();
			if(!$where){
				$where =  $orm->montarWhere($ceTO);
			}
			$update = $ceTO->toArrayUpdate(false, array('id_comissaoentidade', 'id_entidade', 'id_funcao'));
			$orm->update($update, $where);
			$this->commit();
			return true;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($this->excecao);
			return false;
		}
	}
	
	/**
	 * Método que deleta regra de comissão.
	 * @param RegraComissaoTO $rcTO
	 * @return boolean
	 */
	public function excluirRegraComissao(RegraComissaoTO $rcTO){
		$this->beginTransaction();
		try{
			$orm = new RegraComissaoORM();
			$where = $orm->montarWhere($rcTO);
			$orm->delete($where);
			$this->commit();
			return true;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($this->excecao);
			return false;
		}
	}
}
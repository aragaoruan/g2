<?php

/**
 * Classe de persistência de Venda ao banco de dados
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage dao
 */
class VendaDAO extends Ead1_DAO
{


    /**
     * Método de listagem de Venda
     * @param VendaTO $to
     * @return Ambigous <boolean, multitype:>
     */
    public function retornarVenda(VendaTO $to)
    {
        $orm = new VendaORM();
        return $orm->consulta($to);
    }


    /**
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param VwDebitosAlunoTO $to
     * @return Ead1_Mensageiro
     */
    public function retornaVwDebitosAluno(VwDebitosAlunoTO $to)
    {

        $orm = new VwDebitosAlunoORM();
        try {
            $arrayTO = $orm->consulta($to, false, false, array('id_venda', 'dt_vencimento'));
            if ($arrayTO) {
                return new Ead1_Mensageiro($arrayTO, Ead1_IMensageiro::SUCESSO);
            } else {
                return new Ead1_Mensageiro('Nenhum registro de Parcela encontrado!', Ead1_IMensageiro::AVISO);
            }

        } catch (Exception $e) {
            return new Ead1_Mensageiro('Erro ao retornar os dados da VwDebitosAluno: ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }


    /**
     * Metodo que retorna o Usuario e Venda
     * @param VwVendaUsuarioTO $vwVendaUsuarioTO
     * @see VendaBO::retornarVwVendaUsuario();
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwVendaUsuario(VwVendaUsuarioTO $vwVendaUsuarioTO)
    {
        $vwVendaUsuarioORM = new VwVendaUsuarioORM();
        $where = $vwVendaUsuarioORM->montarWhereView($vwVendaUsuarioTO, array('*'), true);
        try {
            $obj = $vwVendaUsuarioORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }


    /**
     * Metodo que cadastrar os lancamentos de uma venda
     * @param LancamentoTO $lancamentoTO
     * @see MatriculaBO::cadastrarLancamento();
     * @throws Zend_Exception
     * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
     */
    public function cadastrarLancamento(LancamentoTO $lancamentoTO)
    {
        $lancamentoORM = new LancamentoORM();
        $this->beginTransaction();
        try {
            $insert = $lancamentoTO->toArrayInsert();
            $insert['nu_cartao'] = $lancamentoTO->getNu_cartao() ? $lancamentoTO->getNu_cartao() : null;
            $idLancamento = $lancamentoORM->insert($insert);
            //Verificando se a matricula é automatica
            $this->commit();
            $cloneLancamentoTO = clone $lancamentoTO;
            $cloneLancamentoTO->setId_lancamento($idLancamento);
            $matriculaBO = new MatriculaBO();
            $matriculaBO->verificaMatriculaAutomatica($cloneLancamentoTO);
            return $idLancamento;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage());
            return false;
        }
    }


    /**
     * Metodo que cadastrar o dia de vencimento de um meio de pagamento da venda.
     * @param VendaMeioVencimentoTO $vendaMeioVencimentoTO
     * @see MatriculaBO::cadastrarArrVendaMeioVencimento();
     * @throws Zend_Exception
     * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
     */
    public function cadastrarVendaMeioVencimento(VendaMeioVencimentoTO $vendaMeioVencimentoTO)
    {
        $vendaMeioVencimentoORM = new VendaMeioVencimentoORM();
        $this->beginTransaction();
        try {
            $insert = $vendaMeioVencimentoTO->toArrayInsert();
            $id_vendameiovencimento = $vendaMeioVencimentoORM->insert($insert);
            $this->commit();
            return $id_vendameiovencimento;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que cadastrar os lancamentos de venda
     * @param LancamentoVendaTO $lancamentoVendaTO
     * @see MatriculaBO::cadastrarLancamento();
     * @throws Zend_Exception
     * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
     */
    public function cadastrarLancamentoVenda(LancamentoVendaTO $lancamentoVendaTO)
    {
        $lancamentoVendaORM = new LancamentoVendaORM();
        try {
            $this->beginTransaction();
            $insert = $lancamentoVendaTO->toArrayInsert();
            $id_insert = $lancamentoVendaORM->insert($insert);
            $this->commit();
            return $id_insert;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * @param LancamentoVendaTO $to
     * @param null $where
     * @return bool
     * @throws Exception
     * @throws Zend_Exception
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     */
    public function editarLancamentoVenda(LancamentoVendaTO $to, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new LancamentoVendaORM();
            if (!$where) {
                $where = $orm->montarWhere($to, false);
            }
            $update = $to->toArrayUpdate(false, array('id_venda', 'id_lancamento'));
            $orm->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            throw $e;
        }
    }

    /**
     * Metodo que cadastra uma Venda
     *
     * @see MatriculaDAO::cadastrarVendaContrato()
     *
     * @param VendaTO $vTO
     * @throws Zend_Validate_Exception
     * @throws Zend_Db_Exception
     * @return int $id_venda
     */
    public function cadastrarVenda(VendaTO $vTO)
    {
        try {
            $this->beginTransaction();
            $orm = new VendaORM();
            $insert = $vTO->toArrayInsert();
            $id_venda = $orm->insert($insert);
            $this->commit();
            return $id_venda;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Db_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que cadastra os Envolvidos na venda
     * @param VendaEnvolvidoTO $to
     * @return Boolean
     */
    public function cadastrarVendaEnvolvido(VendaEnvolvidoTO $to)
    {
        try {
            $this->beginTransaction();
            $orm = new VendaEnvolvidoORM();
            $insert = $to->toArrayInsert();
            $orm->insert($insert);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }


    /**
     * Metodo que cadastra a venda integração
     * @param VendaIntegracaoTO $to
     * @return Boolean
     */
    public function cadastrarVendaIntegracao(VendaIntegracaoTO $to)
    {

        $this->beginTransaction();
        try {
            $orm = new VendaIntegracaoORM();
            $insert = $to->toArrayInsert();
            $id_vendaintegracao = $orm->insert($insert);
            $this->commit();
            return $id_vendaintegracao;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }


    /**
     * Metodo que edita a VendaIntegracao
     * @param VendaIntegracao $to
     * @param Mixed $where
     * @return Boolean
     */
    public function editarVendaIntegracao(VendaIntegracaoTO $to, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new VendaEnvolvidoORM();
            if (!$where) {
                $where = $orm->montarWhere($to, false);
            }
            $update = $to->toArrayUpdate(false, array('id_vendaintegracao', 'id_venda', 'id_sistema'));
            $orm->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }


    /**
     * Metodo que edita uma Venda
     *
     * @see MatriculaDAO::editarVendaContrato()
     *
     * @param VendaTO $vTO
     * @throws Zend_Validate_Exception
     * @throws Zend_Db_Exception
     * @return boolean
     */
    public function editarVenda(VendaTO $vTO, $aceitanull = false)
    {
        $produtoDAO = new ProdutoDAO();
        return $produtoDAO->editarVenda($vTO, $aceitanull);
    }

    /**
     * Metodo que edita os Envolvidos na venda
     * @param VendaEnvolvidoTO $to
     * @param Mixed $where
     * @return Boolean
     */
    public function editarVendaEnvolvido(VendaEnvolvidoTO $to, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new VendaEnvolvidoORM();
            if (!$where) {
                $where = $orm->montarWhere($to, false);
            }
            $update = $to->toArrayUpdate(false, array('id_vendaenvolvido', 'id_venda', 'id_usuario'));
            $orm->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }


    /**
     * Metodo que retorna as Formas de pagamento de uma Venda
     * @param VwFormaMeioDivisaoVendaTO $vwFormaMeioDivisaoVendaTO
     * @see VendaBO::retornarVwFormaMeioDivisaoVenda();
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwFormaMeioDivisaoVenda(VwFormaMeioDivisaoVendaTO $vwFormaMeioDivisaoVendaTO)
    {
        $vwFormaMeioDivisaoVendaORM = new VwFormaMeioDivisaoVendaORM();
        $where = $vwFormaMeioDivisaoVendaORM->montarWhereView($vwFormaMeioDivisaoVendaTO, array('*'), true);
        try {
            $obj = $vwFormaMeioDivisaoVendaORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que retorna os Produtos da Venda
     * @param VwProdutoVendaTO $vwProdutoVendaTO
     * @see VendaBO::retornarProdutoVenda();
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwProdutoVenda(VwProdutoVendaTO $vwProdutoVendaTO)
    {
        $vwProdutoVendaORM = new VwProdutoVendaORM();
        $where = $vwProdutoVendaORM->montarWhereView($vwProdutoVendaTO, array("id_campanhacomercial", "id_campanhacomercial", "id_produto", "id_tipoproduto", "id_venda", "id_vendaproduto", "id_vendaproduto"), true, true);
        try {
            $obj = $vwProdutoVendaORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que retorna os Produtos da Venda de um Livro
     * @param VwProdutoVendaLivroTO $vwProdutoVendaLivroTO
     * @see VendaBO::retornarProdutoVendaLivro();
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwProdutoVendaLivro(VwProdutoVendaLivroTO $vwProdutoVendaLivroTO)
    {
        $vwProdutoVendaLivroORM = new VwProdutoVendaLivroORM();
        $where = $vwProdutoVendaLivroORM->montarWhereView($vwProdutoVendaLivroTO, array("id_campanhacomercial", "id_campanhacomercial", "id_produto", "id_tipoproduto", "id_venda", "id_vendaproduto", "id_vendaproduto", "id_livro", "id_tipolivro", "id_livrocolecao"), true, true);
        try {
            $obj = $vwProdutoVendaLivroORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que retorna os Produtos taxa do projeto pedagógico
     * @param VwProdutoTaxaProjetoTO $vwProdutoTaxaProjetoTO
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwProdutoTaxaProjeto(VwProdutoTaxaProjetoTO $vwProdutoTaxaProjetoTO)
    {
        $vwProdutoTaxaProjetoORM = new VwProdutoTaxaProjetoORM();
        $where = $vwProdutoTaxaProjetoORM->montarWhereView($vwProdutoTaxaProjetoTO, false, true);
        try {
            $obj = $vwProdutoTaxaProjetoORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que edita o Lançamento
     * @param LancamentoTO $lTO
     * @param Mixed $where
     * @throws Zend_Exception
     * @see VendaBO::receberDinheiroLancamento();
     * @return Boolean
     */
    public function editarLancamento(LancamentoTO $lTO, $where = null, $aceitaNull = false)
    {
        $this->beginTransaction();
        try {
            $lancamentoTO = clone $lTO;
            $update = $lTO->toArrayUpdate($aceitaNull, array('id_lancamento'));

            $lancamentoORM = new LancamentoORM();
            $update['nu_cartao'] = $lancamentoTO->getNu_cartao() ? $lancamentoTO->getNu_cartao() : null;

            if (!$where) {
                $where = $lancamentoORM->montarWhere($lTO, true);
            }
            if (!$where || !$lTO->getId_lancamento()) {
                THROW new Zend_Exception('O TO não pode vir Vazio ou sem id_lancamento!');
            }

            $lancamentoORM->update($update, $where);

            if ($lancamentoTO->getBl_quitado()) {
                $tramiteBO = new TramiteBO();
                $tramiteBO->cadastrarTramiteLancamento(clone $lancamentoTO);
            }
            //Verificando se a matricula é automatica
            $this->commit();

            return true;
        } catch (Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que exclui um lançamento
     * @param LancamentoTO $to
     * @param Mixed $where
     * @throws Zend_Exception
     * @return Boolean
     */
    public function deletarLancamento(LancamentoTO $to, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new LancamentoORM();
            if (!$where) {
                $where = $orm->montarWhere($to, true);
            }
            $orm->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
        }
    }

    /**
     * Metodo que exclui um dia de vencimento do meio da venda
     * @param VendaMeioVencimentoTO $to
     * @param Mixed $where
     * @throws Zend_Exception
     * @return Boolean
     */
    public function deletarVendaMeioVencimento(VendaMeioVencimentoTO $to, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new VendaMeioVencimentoORM();
            if (!$where) {
                $where = $orm->montarWhere($to, true);
            }
            $orm->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que exclui os lançamentos salvos na transação
     * @param TransacaoLancamentoTO $to
     * @param Mixed $where
     * @throws Zend_Exception
     * @return Boolean
     */


    public function deletarTransacaoLancamento(TransacaoLancamentoTO $to, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new TransacaoLancamentoORM();
            if (!$where) {
                $where = $orm->montarWhere($to, true);
            }
            $orm->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
        }
    }

    /**
     * Metodo que exclui o vinculo entre a venda e o lançamento
     * @param LancamentoVendaTO $to
     * @param Mixed $where
     * @throws Zend_Exception
     * @return Boolean
     */
    public function deletarLancamentoVenda(LancamentoVendaTO $to, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new LancamentoVendaORM();
            if (!$where) {
                $where = $orm->montarWhere($to, true);
            }
            $orm->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
        }
    }

    /**
     * Metodo que retorna a sp da campanha junto com a venda e o produto
     * @param SpCampanhaVendaProdutoTO $cvpTO propriedades que podem vir NULL, id_produto e/ou id_venda
     * @see VendaBO::retornaSpCampanhaVendaProduto();
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornaSpCampanhaVendaProduto(SpCampanhaVendaProdutoTO $cvpTO)
    {
        $vwProdutoVendaORM = new VwProdutoVendaORM();
        try {
            $where = "{$cvpTO->getId_tipocampanha()},{$cvpTO->getId_formapagamento()},{$cvpTO->getId_entidade()},";
            if ($cvpTO->getId_produto()) {
                $where .= $cvpTO->getId_produto() . ",";
            } else {
                $where .= "NULL,";
            }
            if ($cvpTO->getId_venda()) {
                $where .= $cvpTO->getId_venda() . ',';
            } else {
                $where .= "NULL,";
            }
            if ($cvpTO->getId_campanhaselecionada()) {
                $where .= $cvpTO->getId_campanhaselecionada();
            } else {
                $where .= "NULL";
            }
            $statement = $vwProdutoVendaORM->getDefaultAdapter()
                ->prepare("EXEC sp_campanhavendaproduto {$where}");
            $statement->execute();
            $obj = $statement->fetchAll();
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage() . " - " . $where);
            return false;
        }
    }

    /**
     * Retorna os clientes da venda
     * @see VendaBO::retornarClienteVenda()
     * @param VwClienteVendaTO $vwCVTO
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarClienteVenda(VwClienteVendaTO $vwCVTO, $evolucaoMaiorOuIgual = false)
    {
        try {
            $vwClienteVendaORM = new VwClienteVendaORM();
            $id_entidade = $vwCVTO->getId_entidade() ? $vwCVTO->getId_entidade() : $vwCVTO->getSessao()->id_entidade;
            if (!$id_entidade) {
                throw new Zend_Exception('Entidade não encontrada.');
            }

            $where = " id_entidade = " . $id_entidade;
            if ($vwCVTO->getId_evolucao()) {
                if ($evolucaoMaiorOuIgual) {
                    $where .= " AND id_evolucao >= '{$vwCVTO->getId_evolucao()}' ";
                } else {
                    $where .= " AND id_evolucao = '{$vwCVTO->getId_evolucao()}' ";
                }
            }
            if ($vwCVTO->getId_venda()) {
                $where .= " AND id_venda = '{$vwCVTO->getId_venda()}' ";
            }
            if ($vwCVTO->getId_situacao()) {
                $where .= " AND id_situacao = '{$vwCVTO->getId_situacao()}' ";
            }
            if ($vwCVTO->getId_situacaocontrato()) {
                $where .= " AND id_situacaocontrato = '{$vwCVTO->getId_situacaocontrato()}' ";
            }
            if ($vwCVTO->getSt_cpf() AND $vwCVTO->getSt_nomecompleto()) {
                $where .= " AND ((st_cpf like '%{$vwCVTO->getSt_cpf()}%') OR (st_nomecompleto like '%{$vwCVTO->getSt_nomecompleto()}%'))";
            }

            if ($vwCVTO->getId_usuario()) {
                $where .= " AND id_usuario = '{$vwCVTO->getId_usuario()}'";
            }
            $obj = $vwClienteVendaORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage());
        }
    }

    /**
     * Retornar os lancamentos de uma venda
     * @param VwVendaLancamentoTO $vlTO
     * @param Mixed $where
     * @see MatriculaBO::confirmarRecebimento()
     * @see VendaBO::retornarVendaLancamento()
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVendaLancamento(VwVendaLancamentoTO $vlTO, $where = NULL)
    {
        $view = new VwVendaLancamentoORM();
        if (!$where) {
            $where = $view->montarWhereView($vlTO, false, true, false);
        }
        try {
            $obj = $view->fetchAll($where, array('nu_ordem', 'dt_vencimento'));
            //echo $view->getDefaultAdapter()->getProfiler()->getLastQueryProfile();
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Retornar os lancamentos de uma venda que já estejam quitados
     * @param VwVendaLancamentoQuitadosTO $vlTO
     * @param Mixed $where
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwVendaLancamentoQuitados(VwVendaLancamentoQuitadosTO $vlTO, $where = NULL)
    {
        $view = new VwVendaLancamentoQuitadosORM();
        if (!$where) {
            $where = $view->montarWhereView($vlTO, false, true);
        }
        try {
            $obj = $view->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage());
            return false;
        }
    }


    /**
     * Retorna os lançamentos que foram pagos no recorrente mas estão pendentes
     * @throws Zend_Exception
     * @return Ambigous <boolean, multitype:>|boolean
     */
    public function listarlancamentosCartaoRecorrentePendente()
    {

        $array = false;
        try {

            $orm = new VwVendaLancamentoORM();
            $select = $orm->select()->from(array('tf' => $orm->_name))->limit(50)
                ->where('dt_prevquitado is null')
                ->where('dt_quitado is null')
                ->where('bl_quitado = 0')
                ->where('nu_verificacao < 50')
                ->where('nu_ordem is not null')
                ->where('recorrente_orderid is not null')
                ->where('id_meiopagamento = ' . MeioPagamentoTO::RECORRENTE)
                //->where('dt_vencimento between cast(getdate()-30 as date) and cast(getdate() as date)')
                ->where('dt_vencimento < GETDATE()')
                ->setIntegrityCheck(false)
                ->order('dt_vencimento desc');

            $resultado = $orm->fetchAll($select)->toArray();
            if ($resultado) {
                $array = Ead1_TO_Dinamico::encapsularTo($resultado, new VwVendaLancamentoTO());
            }

            return $array;

        } catch (Exception $e) {
            throw $e;
            return false;
        }

    }


    /**
     * Retornar os lancamentos de uma venda que está em renegociação
     * @param VwVendaLancamentoTO $vlTO
     * @param Mixed $where
     * @see VendaBO::retornarVendaLancamentoRenegociacao()
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     * @author Rafael Rocha rafael.rocha.mg@gmail.com
     */
    public function retornarVendaLancamentoRenegociacao(VwVendaLancamentoTO $vlTO, $where = NULL)
    {
        $orm = new VwVendaLancamentoORM();

        $whereAux = $orm->montarWhereView($vlTO, false, true);
        if (is_string($whereAux)) {
            $whereAux = array($whereAux);
        }

        if ($where) {
            foreach ($where as $w) {
                array_unshift($whereAux, $w);
            }
        }

        try {
            $obj = $orm->fetchAll($whereAux);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Retornar os vendas para ativação de matrícula automática e confirmação de recebimento.
     * @param VwVendasRecebimentoTO $vwVendasRecebimentoTO
     * @param Mixed $where
     * @see VendaBO::retornarVwVendasRecebimento
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwVendasRecebimento(VwVendasRecebimentoTO $vwVendasRecebimentoTO, $where = NULL)
    {
        $view = new VwVendasRecebimentoORM();
        if (!$where) {
            $where = $view->montarWhereView($vwVendasRecebimentoTO, false, true);
        }
        try {
            $obj = $view->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     *
     * Retorna a venda com o produto de um determinado usuário
     * @param VwUsuarioVendaProdutoTO $uvpTO
     * @see VendaBO::retornarUsuarioVendaProduto()
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarUsuarioVendaProduto(VwUsuarioVendaProdutoTO $uvpTO)
    {
        $view = new VwUsuarioVendaProdutoORM();
        $where = $view->montarWhereView($uvpTO, false, true);
        try {
            $obj = $view->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Retorna o resumo financeiro da venda
     * @param VwResumoFinanceiroTO $rfTO
     * @see VendaBO::retornarResumoFinanceiroVenda() | VendaBO::retornarMeioPagamentoResumoVenda()
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarResumoFinanceiroVenda(VwResumoFinanceiroTO $rfTO)
    {
        $view = new VwResumoFinanceiroORM();
        $where = $view->montarWhereView($rfTO, false, true);
        try {
            $obj = $view->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que retorna o nivel e serie do produto da venda
     * @param VendaProdutoNivelSerieTO $to
     * @param Mixed $where
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function retornarVendaProdutoNivelSerie(VendaProdutoNivelSerieTO $to, $where = null)
    {
        try {
            $orm = new VendaProdutoNivelSerieORM();
            if (!$where) {
                $where = $orm->montarWhere($to, false);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que retorna o envolvido da venda
     * @param VendaEnvolvidoTO $to
     * @param Mixed $where
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function retornarVendaEnvolvido(VendaEnvolvidoTO $to, $where = null)
    {
        try {
            $orm = new VendaEnvolvidoORM();
            if (!$where) {
                $where = $orm->montarWhere($to, false);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que retorna os atendimentos da venda
     * @param VwVendaAtendimentoTO $to
     * @param Mixed $where
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function retornarVwVendaAtendimento(VwVendaAtendimentoTO $to, $where = null)
    {
        try {
            $orm = new VwVendaAtendimentoORM();
            if (!$where) {
                $where = $orm->montarWhere($to, false);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }


    /**
     * Cancela os lançamentos com base na tabela tb_TransacaoLancamento
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param TransacaoFinanceiraTO $to
     * @throws Zend_Validate_Exception
     * @return Ead1_Mensageiro
     */
    public function cancelarRecebimentoLancamentoTransacaoFinanceira(TransacaoFinanceiraTO $to)
    {

        $this->beginTransaction();
        try {
            $tranBO = new TransacaoFinanceiraBO();

            $tranLanTO = new TransacaoLancamentoTO();
            $tranLanTO->setId_transacaofinanceira($to->getId_transacaofinanceira());
            $me = $tranBO->listarTransacaoLancamento($tranLanTO);
            if ($me->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Validate_Exception('Não foi encontrado nenhum registro de lançamento para essa Transação.');
            }

            $lancamentos = false;

            foreach ($me->getMensagem() as $tl) {
                $lancamentos[] = $tl->getid_lancamento();
            }

            $qtdlancamentos = 0;
            if ($lancamentos) {
                $orm = new LancamentoORM();
                $where = $orm->getAdapter()->quoteInto('id_lancamento in (?)', $lancamentos);
                $qtdlancamentos = $orm->update(array('dt_prevquitado' => null), $where);
            }

            $this->commit();
            return new Ead1_Mensageiro($qtdlancamentos . ' lançamentos cancelados com sucesso!', Ead1_IMensageiro::SUCESSO);

        } catch (Zend_Validate_Exception $e) {
            $this->rollBack();
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Exception $e) {
            $this->rollBack();
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }


    }

    /**
     * Busca os dados da vw_vendalancamentoatrasado no bando de dados
     * @param VwVendaLancamentoAtrasadoTO $to
     * @param unknown_type $where
     * @throws Zend_Exception
     * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
     */
    public function retornarVwVendaLancamentoAtrasado(VwVendaLancamentoAtrasadoTO $to, $where = NULL)
    {
        $view = new VwVendaLancamentoAtrasadoORM();
        if (!$where) {
            $where = $view->montarWhereView($to, false, true);
        }
        try {
            $obj = $view->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna a ordem dos lancamentos na tb_vendalancamento
     * @param unknown_type $id_venda
     * @throws Zend_Exception
     * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
     */
    public function retornarOrdemVendaLancamento($id_venda)
    {
        //$sql = "SELECT (MAX(nu_ordem) + 1) AS nu_ordem FROM tb_lancamentovenda WHERE id_venda = $id_venda";
        $orm = new LancamentoVendaORM();
        try {
            $sql = $orm->select()->from($orm, array('(MAX(nu_ordem) + 1) AS nu_ordem'))->where("id_venda = $id_venda");
            $obj = $orm->fetchRow($sql);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage());
            return false;
        }
    }


    /**
     * Método que retorna o objeto passado atualizado
     * @throws Zend_Exception
     * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
     */
    public function editarObjetoAfiliado(VendaTO $to, $where = null)
    {
        try {
            $orm = new VendaORM();
            $update = $to->toArrayUpdate(false, array('id_venda', 'id_sistema'));
            $orm->update($update, $where);

            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage());
        }
    }

    /**
     * @param unknown_type $id_combo
     * @throws Zend_Exception
     * @return unknown|boolean
     */
    public function itensDoCombo($id_combo)
    {
        try {

            $to = new ProdutoTO();
            $to->setId_produto($id_combo);
            $to->fetch(true, true, true);

            if ($to->getId_tipoproduto() == TipoProdutoTO::COMBO) {

                $orm = new ProdutoORM();
                $query = "SELECT pc.* FROM vw_produtocombo pc WHERE pc.id_combo = $id_combo";
                $result = $orm->getDefaultAdapter()->query($query)->fetchAll();
                return $result;

            } else {
                return false;
            }


        } catch (Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage());
        }
    }


    /**
     * Metodo que cadastrar a venda aditivo
     * @param VendaAditivoTO $vendaAditivoTO
     * @throws Zend_Exception
     * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
     */
    public function cadastrarVendaAditivo(VendaAditivoTO $vendaAditivoTO)
    {
        $vendaAditivoORM = new VendaAditivoORM();
        $this->beginTransaction();
        try {
            $insert = $vendaAditivoTO->toArrayInsert();
            $idVendaAditivo = $vendaAditivoORM->insert($insert);
            $this->commit();
            return $idVendaAditivo;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage());
            return false;
        }
    }
}

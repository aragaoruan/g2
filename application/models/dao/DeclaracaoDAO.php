<?php
/**
 * Classe de Abstração de dados de Declaração
 * @author Eduardo Romão - ejushiro@gmail.com
 */
class DeclaracaoDAO extends Ead1_DAO{
	
	/**
	 * Método que cadastra uma Entrega de Declaracao
	 * @param EntregaDeclaracaoTO $to
	 * @return int $id_entregadeclaracao
	 */
	public function cadastrarEntregaDeclaracao(EntregaDeclaracaoTO $to){
		$id_entregadeclaracao = 0;
		try{
			$orm = new EntregaDeclaracaoORM();
			$dados = $to->toArrayInsert();
			$this->beginTransaction();
			$id_entregadeclaracao = $orm->insert($dados);
			$this->commit();
		}catch (Zend_Exception $e){
			$this->rollBack();
			THROW $e;
		}
		return $id_entregadeclaracao;
	}
	
	/**
	 * Método que edita uma entrega de declaracao
	 * @param EntregaDeclaracaoTO $to
	 * @param Mixed $where
	 * @return Boolean $editou
	 */
	public function editarEntregaDeclaracao(EntregaDeclaracaoTO $to, $where = null){
		$editou = false;
		try{
			$this->beginTransaction();
			$orm = new EntregaDeclaracaoORM();
			if(!$where){
				$where = $orm->montarWhere($to,true);
				if(!$where){
					THROW new Zend_Exception("O WHERE Não pode vir vazio!");
				}
			}
			$dados = $to->toArrayUpdate(false,array('id_entregadeclaracao'));
			$orm->update($dados, $where);
			$editou = true;
			$this->commit();
		}catch (Zend_Exception $e){
			$this->rollBack();
			THROW $e;
		}
		return $editou;
	}
	
	/**
	 * Método que deleta uma entrega de declaração
	 * @param EntregaDeclaracaoTO $to
	 * @param Mixed $where
	 * @return Boolean $deletou
	 */
	public function excluirEntregaDeclaracao(EntregaDeclaracaoTO $to, $where = null){
		$deletou = false;
		try{
			$orm = new EntregaDeclaracaoORM();
			if(!$where){
				$where = $orm->montarWhere($to,true);
				if(!$where){
					THROW new Zend_Exception("O WHERE Não pode vir vazio!");
				}
			}
			$orm->delete($where);
			$editou = true;
			$this->commit();
		}catch (Zend_Exception $e){
			$this->rollBack();
			THROW $e;
		}
		return $deletou;
	}
	
	/**
	 * Método que retorna as entregas de declaracao
	 * @param EntregaDeclaracaoTO $to
	 * @param Mixed $where
	 * @return Zend_Db_Table_Rowset $rowset
	 */
	public function retornarEntregaDeclaracao(EntregaDeclaracaoTO $to, $where = null){
		$rowset = new Zend_Db_Table_Rowset(array());
		try{
			$orm = new EntregaDeclaracaoORM();
			if(!$where){
				$where = $orm->montarWhere($to,true);
			}
			$rowset = $orm->fetchAll($where);
		}catch (Zend_Exception $e){
			THROW $e;
		}
		return $rowset;
	}
	
	/**
	 * Método que retorna os dados da view de entregas de declaracao
	 * @param VwEntregaDeclaracaoTO $to
	 * @param Mixed $where
	 * @return Zend_Db_Table_Rowset $rowset
	 */
	public function retornarVwEntregaDeclaracao(VwEntregaDeclaracaoTO $to, $where = null){
		$rowset = new Zend_Db_Table_Rowset(array());
		try{
			$orm = new VwEntregaDeclaracaoORM();
			if(!$where){
				$where = $orm->montarWhereView($to,false,true);
			}
			$rowset = $orm->fetchAll($where);
		}catch (Zend_Exception $e){
			THROW $e;
		}
		return $rowset;
	}
}
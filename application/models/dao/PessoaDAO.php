<?php

/**
 * Classe que busca dados de Pessoa em banco de dados
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage dao
 */
class PessoaDAO extends Ead1_DAO
{

    /**
     * Metodo que retorna Usuario
     * @param UsuarioTO $to
     * @return Array or Boolean
     */
    public function retornaUsuario(UsuarioTO $to)
    {
        $tbUsuario = new UsuarioORM();
        return $tbUsuario->consulta($to, null, true);
    }

    /**
     * Método que retorna os Usuarios
     * @param UsuarioTO $to
     * @param Mixed $where
     * @throws Zend_Exception
     * @throws Zend_Db_Exception
     * @return Zend_Db_Table_Rowset
     */
    public function retornarUsuarios(UsuarioTO $to, $where = null)
    {
        $orm = new UsuarioORM();
        if (!$where) {
            $where = $orm->montarWhere($to, false);
        }
        return $orm->fetchAll($where);
    }

    /**
     * Retorna Atalho de pessoa.
     * @param AtalhoPessoaTO $to
     */
    public function retornarAtalhoPessoa(AtalhoPessoaTO $to)
    {
        $orm = new AtalhoPessoaORM();
        $where = $orm->montarWhere($to);
        try {
            $obj = $orm->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            Zend_Debug::dump($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna os Usuarios com perfil Pedagogico
     * @param VwUsuarioPerfilPedagogicoTO $to
     * @param Mixed $where
     * @throws Zend_Exception
     * @throws Zend_Db_Exception
     * @return Zend_Db_Table_Rowset
     */
    public function retornarVwUsuarioperfilPedagogico(VwUsuarioPerfilPedagogicoTO $to, $where = null)
    {
        $orm = new VwUsuarioPerfilPedagogicoORM();
        if (!$where) {
            $where = $orm->montarWhereView($to, false, true);
        }

        return $orm->fetchAll($where);
    }

    /**
     * Metodo que retorna dados de Pessoa por Entidade
     * @param UsuarioTO $to
     * @return Array
     */
    public function retornaPessoa(UsuarioTO $to, $idEntidade)
    {
        $tbPessoa = new PessoaORM();

        if (!$idEntidade) {
            $idEntidade = $to->getSessao()->id_entidade;
        }
        return $tbPessoa->fetchRow("id_usuario = " . $to->getId_usuario() . " AND id_entidade = " . $idEntidade);
    }

    /**
     * Metodo que retorna Endereços da Pessoa
     * @param UsuarioTO $to
     * @return Array
     */
    public function retornaEnderecos(UsuarioTO $to, $idEntidade)
    {
        $vwPessoaEndereco = new VwPessoaEnderecoORM();
        return $vwPessoaEndereco->fetchAll("id_usuario = " . $to->getId_usuario() . " AND id_entidade = " . $idEntidade . " AND bl_ativo = 1")->toArray();
    }

    /**
     * Metodo que retorna Emails da Pessoa
     * @param UsuarioTO $to
     * @return Array
     */
    public function retornaEmails(UsuarioTO $to, $idEntidade)
    {
        $vwPessoaEmail = new VwPessoaEmailORM();
        $sessao = new Zend_Session_Namespace('geral');
        $emailArray = $vwPessoaEmail->fetchAll("id_usuario = " . $to->getId_usuario() . " AND id_entidade = " . $idEntidade . " AND bl_ativo = 1", "bl_padrao");
        if ($emailArray) {
            return $emailArray->toArray();
        }
        return false;
    }

    /**
     * Metodo que retorna Emails em duplicidade, pega o ID usuario e o E-mail e verifica se o mesmo existe para outro usuário que não aquele
     * @param UsuarioTO $to
     * @return Array
     */
    public function retornaEmailDuplicidade(UsuarioTO $to, $idEntidade, $st_email)
    {
        $vwPessoaEmail = new VwPessoaEmailORM();
        $sessao = new Zend_Session_Namespace('geral');
        return $vwPessoaEmail->fetchAll(
            "id_usuario != " . $to->getId_usuario() . "
				AND id_entidade = " . $idEntidade . "
				AND st_email = '" . $st_email . "'
                AND bl_ativo = 1 AND bl_padrao = 1"
            , "bl_padrao"
        )->toArray();
    }

    /**
     * Retorna o e-mail padrão de um usuário
     * Caso não encontre o e-mail padrão é retornado null.
     *
     * Retorna um objeto com as seguintes propridades:
     * 'id_email', 'st_email', 'id_entidade', 'id_usuario', 'id_perfil', 'st_nomecompleto'
     *
     * @param int $idUsuario
     * @param int $idEntidade
     * @return mixed Zend_Db_Table_Row|null
     */
    public function retornaEmailPerfil($idUsuario, $idEntidade)
    {
        $vwDadosPessoaisORM = new VwDadosPessoaisORM();
        $select = $vwDadosPessoaisORM->select(true)
            ->columns(array('id_email', 'st_email', 'id_entidade', 'id_usuario', 'st_nomecompleto'))
            ->where('id_entidade = ?', $idEntidade)
            ->where('id_usuario  = ?', $idUsuario);

        return $vwDadosPessoaisORM->fetchRow($select);
    }

    /**
     * Metodo que retorna Informação Profissional da Pessoa
     * @param UsuarioTO $to
     * @return Array
     */
    public function retornaInformacaoProfissional(UsuarioTO $to, $idEntidade)
    {
        $vwPessoaInformacaoProfissionalORM = new VwPessoaInformacaoProfissionalORM();
        $sessao = new Zend_Session_Namespace('geral');
        return $vwPessoaInformacaoProfissionalORM->fetchAll("id_usuario = " . $to->getId_usuario() . " AND id_entidade = " . $idEntidade)->toArray();
    }

    /**
     * Metodo que retorna Redes Sociais da Pessoa
     * @param UsuarioTO $to
     * @return Array
     */
    public function retornaRedesSociais(UsuarioTO $to, $idEntidade)
    {
        $vwPessoaRedeSocial = new VwPessoaRedeSocialORM();
        $sessao = new Zend_Session_Namespace('geral');
        return $vwPessoaRedeSocial->fetchAll("id_usuario = " . $to->getId_usuario() . " AND id_entidade = " . $idEntidade)->toArray();
    }

    /**
     * Metodo que retorna Telefones da Pessoa
     * @param UsuarioTO $to
     * @return Array
     */
    public function retornaTelefones(UsuarioTO $to, $idEntidade)
    {
        $vwPessoaTelefone = new VwPessoaTelefoneORM();
        $sessao = new Zend_Session_Namespace('geral');
        $telArray = $vwPessoaTelefone->fetchAll("id_usuario = " . $to->getId_usuario() . " AND id_entidade = " . $idEntidade, 'bl_padrao');
        if ($telArray) {
            return $telArray->toArray();
        }
        return false;
    }


    /**
     * Metodo que retorna Informações Academicas da Pessoa
     * @param UsuarioTO $to
     * @return Array
     */
    public function retornaInformacaoAcademica(UsuarioTO $to, $idEntidade)
    {
        $informacaoAcademicaPessoaORM = new InformacaoAcademicaPessoaORM();
        $sessao = new Zend_Session_Namespace('geral');
        return $informacaoAcademicaPessoaORM->fetchAll("id_usuario = " . $to->getId_usuario() . " AND id_entidade = " . $idEntidade)->toArray();
    }

    /**
     * Metodo que retorna informações de Conta Bancária
     * @param UsuarioTO $to
     * @return Zend_Db_Table_Row_Abstract
     */
    public function retornaContaBancaria(UsuarioTO $to, $idEntidade)
    {
        try {
            $orm = new VwPessoaContaORM();
            return $orm->fetchAll("id_usuario = " . $to->getId_usuario() . " AND id_entidade = " . $idEntidade);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que retorna informações de dados biomédicos
     * @param UsuarioTO $to
     * @return Array
     */
    public function retornaDadosBiomedicos(UsuarioTO $to, $idEntidade)
    {
        $tbDadosBiomedicos = new DadosBiomedicosORM();
        $sessao = new Zend_Session_Namespace('geral');
        return $tbDadosBiomedicos->fetchAll("id_usuario = " . $to->getId_usuario() . " AND id_entidade = " . $idEntidade)->toArray();
    }

    /**
     * Retorna os dados de acesso válidos
     * @param UsuarioTO $to
     * @param int $id_entidade
     * @return multitype:
     */
    public function retornarDadosAcessoValidos(UsuarioTO $to, $id_entidade)
    {

        $orm = new DadosAcessoORM();
        $dados = $orm->fetchRow("id_usuario = " . $to->getId_usuario() . " AND id_entidade = " . $id_entidade, 'id_dadosacesso desc');
        return new DadosAcessoTO(($dados ? $dados->toArray() : null));
    }

    /**
     * Método que retorna a view de emails da pessoa
     * @param VwPessoaEmailTO $to
     * @param Mixed $where
     * @return Zend_Db_Table_Rowset
     */
    public function retornarVwPessoaEmail(VwPessoaEmailTO $to, $where = null)
    {
        $orm = new VwPessoaEmailORM();
        return $orm->fetchAll($where ? $where : $orm->montarWhereView($to, false, true));
    }

    /**
     * Metodo que retorna o documento de identidade
     * @param UsuarioTO $to
     * @return Array
     */
    public function retornaIdentidade(UsuarioTO $to, $idEntidade)
    {
        $documentoIdentidade = new DocumentoIdentidadeORM();
        return $documentoIdentidade->fetchAll("id_usuario = " . $to->getId_usuario() . " AND id_entidade = " . $idEntidade)->toArray();
    }

    /**
     * Metodo que retorna o documento de titulo eleitor
     * @param UsuarioTO $to
     * @return Array
     */
    public function retornaTituloEleitor(UsuarioTO $to, $idEntidade)
    {
        $documentoTituloEleitor = new DocumentoTituloDeEleitorORM();
        $sessao = new Zend_Session_Namespace('geral');
        return $documentoTituloEleitor->fetchAll("id_usuario = " . $to->getId_usuario() . " AND id_entidade = " . $idEntidade)->toArray();
    }

    /**
     * Metodo que retorna o documento de certidao de nascimento
     * @param UsuarioTO $to
     * @return Array
     */
    public function retornaCertidaoNascimento(UsuarioTO $to, $idEntidade)
    {
        $certidaoNascimento = new DocumentoCertidaoDeNascimentoORM();
        $sessao = new Zend_Session_Namespace('geral');
        return $certidaoNascimento->fetchAll("id_usuario = " . $to->getId_usuario() . " AND id_entidade = " . $idEntidade)->toArray();
    }

    /**
     * Metodo que retorna o documento de reservista
     * @param UsuarioTO $to
     * @return Array
     */
    public function retornaReservista(UsuarioTO $to, $idEntidade)
    {
        $t = new DocumentoDeReservistaTO();
        $vwPessoaReservista = new DocumentoDeReservistaORM();
        $sessao = new Zend_Session_Namespace('geral');

        return $vwPessoaReservista->fetchAll("id_usuario = " . $to->getId_usuario() . " AND id_entidade = " . $to->getSessao()->id_entidade)->toArray();
    }

    /**
     * Retorna os usuários de uma lista de entidades informadas
     * Recebe um array unidimensional com a lista de id das entidades a serem utilizadas na busca
     * Utiliza o UsuarioTO para filtro na busca
     * @todo criar método para montar o where do to de usuário utiliznado um alias para os campos
     * @param $usuarioTO
     * @param $arEntidade
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function retornarUsuarioListaEntidade(UsuarioTO $usuarioTO, array $arEntidade)
    {

        $usuarioORM = new UsuarioORM();
        $whereIN = implode(', ', $arEntidade);

        $select = $usuarioORM->select()
            ->from(array('us' => 'tb_usuario'), array('us.id_usuario', 'us.st_cpf', 'us.st_nomecompleto', 'us.st_cpf', 'us.st_login', 'pe.dt_nascimento'))
            ->setIntegrityCheck(false)
            ->joinInner(array('pe' => 'tb_pessoa'), 'pe.id_usuario	= us.id_usuario', null)
            ->joinInner(array('en' => 'tb_entidade'), 'pe.id_entidade	= en.id_entidade', array('id_entidade', 'st_nomeentidade'))
            ->where('pe.bl_ativo = 1')
            ->where('pe.id_entidade in (' . $whereIN . ')');;
        if (!empty($usuarioTO->id_usuario)) {
            $select->where('us.id_usuario = ?', $usuarioTO->id_usuario);
        }

        if (!empty($usuarioTO->st_cpf)) {
            $select->where('us.st_cpf = ?', $usuarioTO->st_cpf);
        }
        return $usuarioORM->fetchAll($select);
    }

    /**
     * Retorna os e-mails do usuáro
     * @param int $idUsuario
     * @param int $idEntidade
     * @param boolean $blPadrao - OPCIONAL
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function retornarEmailUsuario($idUsuario, $idEntidade, $blPadrao = false)
    {

        $contatosEmailORM = new ContatosEmailORM();
        $select = $contatosEmailORM->select()
            ->from(array('ce' => 'tb_contatosemail'), array('id_email', 'st_email'))
            ->setIntegrityCheck(false)
            ->joinInner(array('cepp' => 'tb_contatosemailpessoaperfil'), 'ce.id_email = cepp.id_email', array('bl_padrao', 'id_perfil'))
            ->where('bl_ativo	= ?', 1)
            ->where('id_entidade = ?', $idEntidade)
            ->where('id_usuario  = ?', $idUsuario);
        if ($blPadrao) {
            $select->where('cepp.bl_padrao = ?', 1);
        }


        return $contatosEmailORM->fetchAll($select);
    }


    public function verificaLogin(UsuarioTO $uTO)
    {
        $tbUsuario = new UsuarioORM();
        return $tbUsuario->fetchAll("st_login = '" . $uTO->getSt_login() . "'");
    }


    /**
     * @param VwPessoaTO $to
     * @param $where
     * @return Zend_Db_Table_Rowset_Abstract
     * @throws Zend_Db_Exception
     */
    public function retornarUsuarioRecuperarSenha(VwPessoaTO $to, $where)
    {
        try {

            $orm = new VwPessoaORM();
            $select = $orm->select()
                ->setIntegrityCheck(false)
                ->from(array("vw" => "vw_pessoa"))
                ->joinInner(array('ec' => 'tb_compartilhadados'), "ec.id_entidadedestino = vw.id_entidade", array())
                ->where("ec.id_tipodados = ?", TipoDadosTO::LOGIN);


            if (array_key_exists('id_entidade', $where) && !empty($where['id_entidade'])) {
                $select->where('ec.id_entidadeorigem = ?', $where['id_entidade']);
            }

            if (array_key_exists('st_email', $where) && !empty($where['st_email'])) {
                $select->where("vw.st_email LIKE '%{$where['st_email']}%' ");
            }

            if (array_key_exists('st_login', $where) && !empty($where['st_login'])) {
                $select->where("vw.st_login LIKE '%{$where['st_login']}%' ");
            }

            if (array_key_exists('st_cpf', $where) && !empty($where['st_cpf'])) {
                $select->where("vw.st_cpf LIKE '%{$where['st_cpf']}%' ");
            }


            return $orm->fetchAll($select);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Db_Exception($e->getMessage());
        }
    }


    /**
     * Metodo que retorna dados da view vw_pessoa
     * @param VwPessoaTO $to
     * @param Mixed $where
     * @throws Zend_Db_Exception
     * @return Zend_Db_Table_Abstract | false
     */
    public function retornarVwPessoa(VwPessoaTO $to, $where = null)
    {
        try {
            $orm = new VwPessoaORM();
            if (!$where) {
                $where = $orm->montarWhereView($to, false, true);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Db_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que retorna dados da view vw_funcionario
     * @param VwFuncionarioTO $to
     * @param unknown_type $where
     * @throws Zend_Db_Exception
     * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
     */
    public function retornarVwFuncionario(VwFuncionarioTO $to, $where = null)
    {
        try {
            $orm = new VwFuncionarioORM();
            if (!$where) {
                $where = $orm->montarWhereView($to, false, true);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Db_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que cadastra usuario
     * @param $to
     * @return Mixed
     */
    public function cadastrarUsuario(UsuarioTO $to)
    {
        $tbUsuario = new UsuarioORM();
// 		$to->setSt_senha(md5($to->getSt_senha())); -- O próprio TO ja converte para MD5 se a senha não tem 32 caracteres

        $to->setId_usuariopai(($to->getId_usuariopai() ? $to->getId_usuariopai() : null));
        $insert = $to->toArrayInsert();
        $this->beginTransaction();
        try {
            $id_usuario = $tbUsuario->insert($insert);
            $this->commit();
            return $id_usuario;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Db_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que Cadastra Pessoa
     * @param $to
     * @return Mixed
     */
    public function cadastrarPessoa(UsuarioTO $uTO, PessoaTO $pTO)
    {
        $tbPessoa = new PessoaORM();
        $pTO->setBl_ativo(1);
        $pTO->setId_situacao(1);
        $insert = $pTO->toArrayInsert();
        $this->beginTransaction();
        try {
            $tbPessoa->insert($insert);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Db_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que Cadastra os dados de acesso
     * @param $to
     * @return Mixed
     */
    public function cadastrarDadosAcesso(DadosAcessoTO $dTO)
    {

        if (!$dTO->getId_usuario() || !$dTO->getId_entidade())
            return false;


        $orm = new DadosAcessoORM();
        $insert = $dTO->toArrayInsert();
        $this->beginTransaction();
        try {
            $orm->update(array('bl_ativo' => 0), array('id_usuario = ' . $dTO->getId_usuario() . ' and id_entidade = ' . $dTO->getId_entidade()));
            $orm->update(array('st_senha' => $dTO->getSt_senha()), array('id_usuario = ' . $dTO->getId_usuario() . ' and st_login = ' . "'" . $dTO->getSt_login() . "'"));
            $id_dadosacesso = $orm->insert($insert);
            $this->commit();
            return $id_dadosacesso;
        } catch (Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Db_Exception($e->getMessage());
            return false;
        }
    }


    public function atualizarDadosAcesso(DadosAcessoTO $dTO, array $entidades)
    {
        $orm = new DadosAcessoORM();
        try {
            $this->beginTransaction();
            foreach ($entidades as $e) {
                $dTO->id_entidade = $e;
                $dTO->bl_ativo = false;
                $insert = $dTO->toArrayInsert();
                $orm->insert($insert);
            }
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Db_Exception($e->getMessage());
        }
    }

    public function retornarDadosAcesso(DadosAcessoTO $to, $where = null)
    {
        $orm = new DadosAcessoORM();
        if (!$where) {
            $where = $orm->montarWhere($to, false);
        }
        return $orm->fetchAll($where);
    }

    /**
     * Metodo que Cadastra Endereço
     * @param $to
     * @return Boolean
     */
    public function cadastrarEndereco(UsuarioTO $uTO, EnderecoTO $eTO, PessoaEnderecoTO $peTO)
    {
        $this->beginTransaction();
        try {
            $tbEndereco = new EnderecoORM();
            $insertEndereco = $eTO->toArrayInsert();

            if (!isset($insertEndereco['id_pais']) || !$insertEndereco['id_pais']) {
                $insertEndereco['id_pais'] = 22;
            }

            $id_endereco = $tbEndereco->insert($insertEndereco);
            $peTO->setId_endereco($id_endereco);
            $insertPessoaEndereco = $peTO->toArrayInsert();
            $tbPessoaEndereco = new PessoaEnderecoORM();
            $id = $tbPessoaEndereco->insert($insertPessoaEndereco);
            $this->commit();
            return $id_endereco;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Db_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que Cadastra Email
     * @param $to
     * @return Boolean
     */
    public function cadastrarEmail(UsuarioTO $uTO, ContatosEmailTO $ceTO, ContatosEmailPessoaPerfilTO $ceppTO)
    {
        $this->beginTransaction();
        try {
            $tbContatosEmail = new ContatosEmailORM();
            $tbContatosEmailPessoaPerfil = new ContatosEmailPessoaPerfilORM();
            $insertContatosEmail = $ceTO->toArrayInsert();
            $id_email = $tbContatosEmail->insert($insertContatosEmail);
            $ceTO->setId_email($id_email);
            $ceppTO->setId_email($ceTO->getId_email());
            $insertContatosEmailPessoaPerfil = $ceppTO->toArrayInsert();
            $tbContatosEmailPessoaPerfil->insert($insertContatosEmailPessoaPerfil);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Db_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que Cadastra Informação Profissional da Pessoa
     * @param $to
     * @return Mixed
     */
    public function cadastrarInformacaoProfissional(UsuarioTO $uTO, InformacaoProfissionalPessoaTO $ippTO)
    {
        $tbInformacaoProfissionalPessoa = new InformacaoProfissionalPessoaORM();
        $insert = $ippTO->toArrayInsert();
        $this->beginTransaction();
        try {
            $pk = $tbInformacaoProfissionalPessoa->insert($insert);
            $this->commit();
            return $pk;
        } catch (Zend_Exception $e) {
            $arr['SQL'] = $e->getMessage();
            $arr['INSERT'] = $insert;
            $arr['TO'] = $ippTO;
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que Cadastra Rede Social
     * @param $to
     * @return Boolean
     */
    public function cadastrarRedeSocial(UsuarioTO $uTO, ContatosRedeSocialTO $crsTO, ContatosRedeSocialPessoaTO $crspTO)
    {
        $tbContatosRedeSocial = new ContatosRedeSocialORM();
        $tbContatosRedeSocialPessoa = new ContatosRedeSocialPessoaORM();
        $insert = $crsTO->toArrayDAO();
        $this->beginTransaction();
        try {
            $id_redeSocial = $tbContatosRedeSocial->insert($insert);
            $crspTO->setId_redesocial($id_redeSocial);
            $tbContatosRedeSocialPessoa->insert($crspTO->toArrayDAO());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que Cadastra Telefone
     * @param $to
     * @return Boolean
     */
    public function cadastrarTelefone(UsuarioTO $uTO, ContatosTelefoneTO $ctTO, ContatosTelefonePessoaTO $ctpTO)
    {
        $this->beginTransaction();
        try {
            $tbContatostelefone = new ContatosTelefoneORM();
            $tbContatostelefonePessoa = new ContatosTelefonePessoaORM();
            $insert = $ctTO->toArrayInsert();
            $id_telefone = $tbContatostelefone->insert($insert);
            $ctpTO->setId_telefone($id_telefone);
            $ctTO->setId_telefone($ctpTO->getId_telefone());
            $insert2 = $ctpTO->toArrayInsert();
            $tbContatostelefonePessoa->insert($insert2);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Db_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que Cadastra Informações Academicas da Pessoa
     * @param UsuarioTO $uTO
     * @param InformacaoAcademicaPessoaTO $iapTO
     * @return bool|mixed
     * @throws Exception
     * @throws Zend_Session_Exception
     */
    public function cadastrarInformacaoAcademica(UsuarioTO $uTO, InformacaoAcademicaPessoaTO $iapTO)
    {
        $tbInformacaoAcademicaPessoa = new InformacaoAcademicaPessoaORM();
        $sessao = new Zend_Session_Namespace('geral');
        $iapTO->setId_usuario($uTO->getId_usuario());

        if (empty($iapTO->getId_entidade()) && !empty($sessao->id_entidade)) {
            $iapTO->setId_entidade($sessao->id_entidade);
        }

        $insert = $iapTO->toArrayInsert();
        $this->beginTransaction();
        try {


            if (empty($iapTO->getId_entidade())) {
                throw new Zend_Exception("Id entidade não informado.");
            }

            $pk = $tbInformacaoAcademicaPessoa->insert($insert);
            $this->commit();
            return $pk;
        } catch (Zend_Exception $e) {
            $arr['SQL'] = $e->getMessage();
            $arr['TO'] = $iapTO;
            $arr['INSERT'] = $insert;
            $this->excecao = $e->getMessage();
            $this->rollBack();
            return false;
        }
    }

    /**
     * Metodo que Cadastra Conta Bancaria
     * @param $to
     * @return Boolean
     */
    public function cadastrarContaPessoa(UsuarioTO $uTO, ContaPessoaTO $cpTO)
    {
        $tbContaPessoa = new ContaPessoaORM();
        $insert = $cpTO->toArrayDAO();
        $this->beginTransaction();
        try {
            $pk = $tbContaPessoa->insert($insert);
            $this->commit();
            return $pk;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $arr['TO'] = $cpTO;
            $arr['INSERT'] = $insert;
            $arr['SQL'] = $e->getMessage();
            $this->excecao = $arr;
            return false;
        }
    }

    /**
     * Metodo que Cadastra Dados Biomedicos
     * @param $to
     * @return int
     */
    public function cadastrarDadosBiomedicos(UsuarioTO $uTO, DadosBiomedicosTO $dbTO)
    {
        $this->beginTransaction();
        try {
            $tbDadosBiomedicos = new DadosBiomedicosORM();
            $insert = $dbTO->toArrayDAO();
            $id_dadosbiomedicos = $tbDadosBiomedicos->insert($insert);
            $this->commit();
            return $id_dadosbiomedicos;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Db_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que Cadastra Documento de Identidade
     * @param $to
     * @return Boolean
     */
    public function cadastrarDocumentoIdentidade(DocumentoIdentidadeTO $diTO)
    {
        $this->beginTransaction();
        try {
            $tbDocumentoIdentidade = new DocumentoIdentidadeORM();
            $insert = $diTO->toArrayInsert();
            $tbDocumentoIdentidade->insert($insert);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Db_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que Cadastra Documento Titulo de Eleito
     * @param $to
     * @return Boolean
     */
    public function cadastrarDocumentoTituloEleitor(DocumentoTituloDeEleitorTO $dtiTO)
    {
        $this->beginTransaction();
        try {
            $tbDocumentoTituloEleitor = new DocumentoTituloDeEleitorORM();
            $insert = $dtiTO->toArrayInsert();
            $tbDocumentoTituloEleitor->insert($insert);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Db_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que Cadastra Documento Certidão de Nascimento
     * @param $to
     * @return Boolean
     */
    public function cadastrarDocumentoCertidaoNascimento(UsuarioTO $uTO, DocumentoCertidaoDeNascimentoTO $dcnTO)
    {
        $this->beginTransaction();
        try {
            $tbDocumentoCertidaoNascimento = new DocumentoCertidaoDeNascimentoORM();
            $insert = $dcnTO->toArrayInsert();
            $tbDocumentoCertidaoNascimento->insert($insert);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Db_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que Cadastra Documento Reservista
     * @param $to
     * @return Ead1_Mensageiro
     */
    public function cadastrarDocumentoReservista(UsuarioTO $uTO, DocumentoDeReservistaTO $drTO)
    {
        $this->beginTransaction();
        try {
            $tbDocumentoReservista = new DocumentoDeReservistaORM();
            $insert = $drTO->toArrayDAO();
            $tbDocumentoReservista->insert($insert);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Db_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Cadastra atalho de pessoa pra funcionalidade.
     * @param AtalhoPessoaTO $aTO
     * @throws Zend_Db_Exception
     */
    public function cadastrarAtalhoPessoa(AtalhoPessoaTO $aTO)
    {
        $this->beginTransaction();
        try {
            $orm = new AtalhoPessoaORM();
            $insert = $aTO->toArrayInsert();
            $id_atalhopessoa = $orm->insert($insert);
            $this->commit();
            return $id_atalhopessoa;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Db_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Edita atalho de pessoa pra entidade
     * @param AtalhoPessoaTO $aTO
     * @throws Zend_Db_Exception
     */
    public function editarAtalhoPessoa(AtalhoPessoaTO $aTO)
    {
        $this->beginTransaction();
        try {
            $orm = new AtalhoPessoaORM();
            $update = $aTO->toArrayUpdate(null, array('id_atalhopessoa'));
            $orm->update($update, 'id_atalhopessoa = ' . $aTO->getId_atalhopessoa());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Db_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que atualiza o usuário
     * @param $to To que contem dados de usuário
     * @return boolean
     */
    public function editarUsuario(UsuarioTO $to)
    {
        try {
            $this->beginTransaction();
            $tbUsuario = new UsuarioORM();
            $updateUsuario = $to->toArrayUpdate(false, array('id_usuario', 'id_usuariopai'));
            $tbUsuario->update($updateUsuario, " id_usuario = " . $to->getId_usuario());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Db_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que edita a pessoa
     * @param $pTO To contendo dados de pessoa
     * @param $uTO To contendo dados de usuário
     * @return boolean
     */
    public function editarPessoa(UsuarioTO $uTO = null, PessoaTO $pTO)
    {
        $tbPessoa = new PessoaORM();
        $pTO->setBl_ativo(true);
        $pTO->setId_situacao(1);
        $pTO->setId_entidade($pTO->getSessao()->id_entidade);
        $updatePessoa = $pTO->toArrayUpdate(false, array('id_usuario', 'id_entidade'));

        $this->beginTransaction();
        try {
            $where = $tbPessoa->montarWhere($pTO, true);
            $tbPessoa->update($updatePessoa, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Db_Exception($e->getMessage());
        }
    }

    /**
     * Metodo que edita o endereço
     * @param $eTO To contendo dados de endereço
     * @return boolean
     */
    public function editarEndereco(EnderecoTO $eTO, PessoaEnderecoTO $peTO)
    {
        $this->beginTransaction();
        try {
            $tbEndereco = new EnderecoORM();
            $tbPessoaEndereco = new PessoaEnderecoORM();
            $updateEndereco = $eTO->toArrayUpdate(false, array('id_endereco'));
            $updatePessoaEndereco = $peTO->toArrayUpdate(false, array('id_endereco'));
            $tbEndereco->update($updateEndereco, ' id_endereco = ' . $peTO->getId_endereco());
            $tbPessoaEndereco->update($updatePessoaEndereco, ' id_endereco = ' . $peTO->getId_endereco() . ' AND id_usuario = ' . $peTO->getId_usuario() . ' AND id_entidade = ' . $peTO->getId_entidade());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Db_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que edita o email
     * @param $eTO To contendo dados de email
     * @return boolean
     */
    public function editarEmail(ContatosEmailTO $eTO, ContatosEmailPessoaPerfilTO $eppTO)
    {
        $tbContatosEmail = new ContatosEmailORM();
        $tbContatosEmailPessoaPerfil = new ContatosEmailPessoaPerfilORM();
        $updateEmail = $eTO->toArrayUpdate(false, array('id_email'));
        $updateContatosEmail = $eppTO->toArrayUpdate(false, array('id_email'));
        $this->beginTransaction();
        try {
            $tbContatosEmail->update($updateEmail, 'id_email = ' . $eTO->getId_email());
            $tbContatosEmailPessoaPerfil->update($updateContatosEmail, 'id_email = ' . $eTO->getId_email() . ' AND id_entidade = ' . $eppTO->getId_entidade() . ' AND id_usuario = ' . $eppTO->getId_usuario());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que edita rede social
     * @param $crsTO To contendo os dados de rede social
     * @return boolean
     */
    public function editarRedeSocial(ContatosRedeSocialTO $crsTO, ContatosRedeSocialPessoaTO $crspTO)
    {
        $tbRedeSocial = new ContatosRedeSocialORM();
        $tbRedeSocialPessoa = new ContatosRedeSocialPessoaORM();
        $updateRedeSocial = $crsTO->toArrayUpdate(false, array('id_redesocial'));
        $updateRedeSocialPessoa = $crspTO->toArrayUpdate(false, array('id_redesocial'));
        $this->beginTransaction();
        try {
            $tbRedeSocial->update($updateRedeSocial, 'id_redesocial = ' . $crsTO->getId_redesocial());
            $tbRedeSocialPessoa->update($updateRedeSocialPessoa, 'id_redesocial = ' . $crsTO->getId_redesocial() . ' AND id_usuario = ' . $crspTO->getId_usuario() . ' AND id_entidade = ' . $crspTO->getId_entidade());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que edita o telefone
     * @param ContatosTelefoneTO $tTO
     * @param ContatosTelefonePessoaTO $tpTO
     * @return boolean
     */
    public function editarTelefone(ContatosTelefoneTO $tTO, ContatosTelefonePessoaTO $tpTO)
    {
        $this->beginTransaction();
        try {
            $tbContatosTelefone = new ContatosTelefoneORM();
            $tbContatosTelefonePessoa = new ContatosTelefonePessoaORM();
            $updateTelefone = $tTO->toArrayUpdate(false, array('id_telefone'));
            $updateTelefonePessoa = $tpTO->toArrayUpdate(false, array('id_telefone'));
            $tbContatosTelefone->update($updateTelefone, 'id_telefone = ' . $tTO->getId_telefone());
            $tbContatosTelefonePessoa->update($updateTelefonePessoa, 'id_telefone = ' . $tTO->getId_telefone() . ' AND id_usuario = ' . $tpTO->getId_usuario() . ' AND id_entidade = ' . $tpTO->getId_entidade());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Db_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que edita informação acadêmica
     * @param $iaTO To contendo informação acadêmica
     * @return boolean
     */
    public function editarInformacaoAcademica(InformacaoAcademicaPessoaTO $iaTO)
    {
        $tbInformacaoAcademica = new InformacaoAcademicaPessoaORM();
        $updateInfAcademica = $iaTO->toArrayUpdate(false, array('id_informacaoacademicapessoa'));
        $this->beginTransaction();
        try {
            $where = $tbInformacaoAcademica->montarWhere($iaTO, true);
            $tbInformacaoAcademica->update($updateInfAcademica, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $arr['SQL'] = $e->getMessage();
            $arr['TO'] = $iaTO;
            $arr['WHERE'] = $where;
            $arr['UPDATE'] = $updateInfAcademica;
            $this->excecao = $arr;
            return false;
        }
    }

    /**
     * Metodo que edita informação profissional
     * @param $ippTO To contendo dados de informção profissional
     * @return boolean
     */
    public function editarInformacaoProfissional(InformacaoProfissionalPessoaTO $ippTO)
    {
        $tbInformacaoProfissional = new InformacaoProfissionalPessoaORM();
        $updateInfProfissional = $ippTO->toArrayUpdate(false, array('id_informacaoprofissionalpessoa'));
        $this->beginTransaction();
        try {
            $where = $tbInformacaoProfissional->montarWhere($ippTO, true);
            $tbInformacaoProfissional->update($updateInfProfissional, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            return false;
        }
    }

    /**
     * Metodo que edita informacoes da conta
     * @param $cpTO To contendo dados de conta
     * @return boolean
     */
    public function editarContaPessoa(ContaPessoaTO $cpTO)
    {
        $tbContaPessoa = new ContaPessoaORM();
        $updateConta = $cpTO->toArrayUpdate(false, array('id_usuario', 'id_entidade', 'id_contapessoa'));
        $this->beginTransaction();
        try {
            $newCpTO = $cpTO;
            unset($newCpTO->nu_agencia);
            unset($newCpTO->nu_conta);
            $where = $tbContaPessoa->montarWhere($newCpTO);
            $tbContaPessoa->update($updateConta, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $arr['TO'] = $cpTO;
            $arr['WHERE'] = $where;
            $arr['UPDATE'] = $updateConta;
            $arr['SQL'] = $e->getMessage();
            $this->excecao = $arr;
            return false;
        }
    }

    /**
     * Metodo que edita dados biomédicos
     * @param $dbTO To contendo dados biomedicos
     * @return boolean
     */
    public function editarDadosBiomedicos(DadosBiomedicosTO $dbTO)
    {
        $tbDadosBiomedicos = new DadosBiomedicosORM();
        $updateDadosBiomedicos = $dbTO->toArrayUpdate(true, array('id_usuario', 'id_dadosbiomedicos', 'id_entidade'));
        $this->beginTransaction();
        try {
            $where = $tbDadosBiomedicos->montarWhere($dbTO, true);
            $tbDadosBiomedicos->update($updateDadosBiomedicos, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que edita dados de identidade
     * @param $dciTO To contendo dados de documento de identidade
     * @return boolean
     */
    public function editarDocumentoIdentidade(DocumentoIdentidadeTO $dciTO)
    {
        $this->beginTransaction();
        try {
            $tbDocumentoIdentidade = new DocumentoIdentidadeORM();
            $updateDocumentoIdentidade = $dciTO->toArrayUpdate(false, 'id_usuario');
            $where = 'id_usuario = ' . $dciTO->getId_usuario() . ' AND id_entidade = ' . $dciTO->getId_entidade();
            $tbDocumentoIdentidade->update($updateDocumentoIdentidade, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Db_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que edita titulo de eleitor
     * @param $dteTO To contendo dados de documento do titulo de eleitor
     * @return boolean
     */
    public function editarDocumentoTituloEleitor(DocumentoTituloDeEleitorTO $dteTO)
    {
        $this->beginTransaction();
        try {
            $tbDocumentoTituloEleitor = new DocumentoTituloDeEleitorORM();
            $updateDocumentoTituloEleitor = $dteTO->toArrayUpdate(false, 'id_usuario');
            $where = 'id_usuario = ' . $dteTO->getId_usuario() . ' AND id_entidade = ' . $dteTO->getId_entidade();
            $tbDocumentoTituloEleitor->update($updateDocumentoTituloEleitor, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que edita documento de certidão de nascimento
     * @param $cdnTO To contendo dados de certidao de nascimento
     * @return boolean
     */
    public function editarDocumentoCertidaoNascimento(DocumentoCertidaoDeNascimentoTO $cdnTO)
    {
        $this->beginTransaction();
        try {
            $tbDocumentoCertidaoNascimento = new DocumentoCertidaoDeNascimentoORM();
            $updateDocumentoCertidaoNascimento = $cdnTO->toArrayUpdate(false, 'id_usuario');
            $where = 'id_usuario = ' . $cdnTO->getId_usuario() . ' AND id_entidade = ' . $cdnTO->getId_entidade();
            $tbDocumentoCertidaoNascimento->update($updateDocumentoCertidaoNascimento, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Db_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que edita documento de reservista
     * @param $drTO To contendo dados de reservista
     * @return boolean
     */
    public function editarDocumentoReservista(DocumentoDeReservistaTO $drTO)
    {
        $this->beginTransaction();
        try {
            $tbDocumentoReservista = new DocumentoDeReservistaORM();
            $updateDocumentoReservista = $drTO->toArrayUpdate();
            $where = 'id_usuario = ' . $drTO->getId_usuario() . ' AND id_entidade = ' . $drTO->getId_entidade();
            $tbDocumentoReservista->update($updateDocumentoReservista, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Db_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Deleta o vinculo de telefone pessoa
     * @param ContatosTelefonePessoaTO $cptTO
     * @return int Número de linhas deletadas
     */
    public function deletarContatosTelefonePessoa(ContatosTelefonePessoaTO $cptTO)
    {
        $ormPessoaTelefone = new ContatosTelefonePessoaORM();
        $cptTO->tipaTODAO();
        $wherePessoaTelefone = 'id_usuario = ' . $cptTO->getId_usuario() . ' AND id_entidade = ' . $cptTO->getSessao()->id_entidade . ' AND id_telefone = ' . $cptTO->getId_telefone();
        return $ormPessoaTelefone->delete($wherePessoaTelefone);
    }

    /**
     * Deleta os telefones do contato
     * @param ContatosTelefonePessoaTO $cptTO
     * @return int Número de linhas deletadas
     */
    public function deletarContatosTelefone(ContatosTelefonePessoaTO $cptTO)
    {
        $ormTelefone = new ContatosTelefoneORM();
        $cptTO->tipaTODAO();
        $wherePessoa = 'id_telefone = ' . $cptTO->getId_telefone();
        return $ormTelefone->delete($wherePessoa);
    }

    /**
     * Deleta o vinculo de rede social pessoa
     * @param ContatosRedeSocialPessoaTO $crspTO
     * @return int Número de linhas deletadas
     */
    public function deletarContatosRedeSocialPessoa(ContatosRedeSocialPessoaTO $crspTO)
    {
        $ormPessoaRede = new ContatosRedeSocialPessoaORM();
        $crspTO->tipaTODAO();
        $where = 'id_usuario = ' . $crspTO->getId_usuario() . ' AND id_entidade = ' . $crspTO->getSessao()->id_entidade . ' AND id_redesocial = ' . $crspTO->getId_redesocial();
        return $ormPessoaRede->delete($where);
    }

    /**
     * Deleta a rede social do contato da pessoa
     * @param ContatosRedeSocialPessoaTO $crspTO
     * @return int Número de linhas deletadas
     */
    public function deletarContatosRedeSocial(ContatosRedeSocialPessoaTO $crspTO)
    {
        $ormRede = new ContatosRedeSocialORM();
        $crspTO->tipaTODAO();
        $where = 'id_redesocial = ' . $crspTO->getId_redesocial();
        return $ormRede->delete($where);
    }

    /**
     * Metodo que exclui informação acadêmica
     * @param $iaTO To contendo informação acadêmica
     * @return boolean
     */
    public function deletarInformacaoAcademica(InformacaoAcademicaPessoaTO $iaTO)
    {
        $tbInformacaoAcademica = new InformacaoAcademicaPessoaORM();
        $iaTO->tipaTODAO();
        $where = 'id_informacaoacademicapessoa = ' . $iaTO->getId_informacaoacademicapessoa();
        $this->beginTransaction();
        try {
            $tbInformacaoAcademica->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $arr['WHERE'] = $where;
            $arr['TO'] = $iaTO;
            $arr['SQL'] = $e->getMessage();
            $this->excecao = $arr;
            return false;
        }
    }

    /**
     * Metodo que exclui informação Profissional
     * @param $ippTO To contendo informação profissional
     * @return boolean
     */
    public function deletarInformacaoProfissional(InformacaoProfissionalPessoaTO $ippTO)
    {
        $tbInformacaoProfissional = new InformacaoProfissionalPessoaORM();
        $ippTO->tipaTODAO();
        $where = 'id_informacaoprofissionalpessoa = ' . $ippTO->getId_informacaoprofissionalpessoa();
        $this->beginTransaction();
        try {
            $tbInformacaoProfissional->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $arr['WHERE'] = $where;
            $arr['TO'] = $ippTO;
            $arr['SQL'] = $e->getMessage();
            $this->excecao = $arr;
            return false;
        }
    }

    /**
     * Metodo que exclui Conta da Pessoa
     * @param ContaPessoaTO $cpTO
     * @return boolean
     */
    public function deletarContaPessoa(ContaPessoaTO $cpTO)
    {
        $tbContaPessoa = new ContaPessoaORM();
        $cpTO->tipaTODAO();
        $where = 'id_contapessoa = ' . $cpTO->getId_contapessoa();
        $this->beginTransaction();
        try {
            $tbContaPessoa->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $arr['WHERE'] = $where;
            $arr['TO'] = $cpTO;
            $arr['SQL'] = $e->getMessage();
            $this->excecao = $arr;
            return false;
        }
    }

    /**
     * Deleta Atalho de pessoa na funcionalidade
     * @param $aTO
     */
    public function deletarAtalhoPessoa(AtalhoPessoaTO $aTO)
    {
        $orm = new AtalhoPessoaORM();
        $where = $orm->montarWhere($aTO);
        $this->beginTransaction();
        try {
            $orm->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Db_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Faz a pesquisa na vw_pessoa sem verificacao de entidades filhas, de forma geral
     * @param VwPessoaTO $to
     * @return bool|Zend_Db_Table_Rowset_Abstract
     * @throws Zend_Exception
     */
    public function pesquisaUsuarioSemEntidade(VwPessoaTO $to)
    {
        try {
            $vwPessoa = new VwPessoaORM();
            $where = "(";
            if ($to->getId_usuario()) {
                $pars = " id_usuario = " . $to->getId_usuario();
                $where .= $this->verificaAndOr($where, $pars);
            }
            if ($to->getSt_nomecompleto()) {
                $pars = "st_nomecompleto like '%" . $to->getSt_nomecompleto() . "%' collate Latin1_General_CI_AI_WS";
                $where .= $this->verificaAndOr($where, $pars);
            }
            if ($to->getSt_cpf()) {
                $pars = "st_cpf like '%" . $to->getSt_cpf() . "%'";
                $where .= $this->verificaAndOr($where, $pars);
            }
            if ($to->getSt_email()) {
                $pars = "st_email like '%" . $to->getSt_email() . "%'";
                $where .= $this->verificaAndOr($where, $pars);
            }
            if ($to->getId_situacao()) {
                $pars = "id_situacao = " . $to->getId_situacao();
                $where .= $this->verificaAndOr($where, $pars);
            }
            $where .= ")";
            if ($where == "()") {
                $where = ($to->getId_entidade()) ? " id_entidade in (" . $to->getId_entidade() . ") " : "";
            } else {
                $where .= " AND " . ($to->getId_entidade() ? " id_entidade in (" . $to->getId_entidade() . ") " : "");
            }
            $obj = $vwPessoa->fetchAll($where, 'st_nomecompleto');
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Verifica se é AND ou OR
     * @param string $where primeira parte da pesquisa
     * @param string $pars
     * @return string
     */
    private function verificaAndOr($where, $pars)
    {
        if ($where != "") {
            if ($where == "(") {
                return $where = " " . $pars;
            } elseif (substr($where, -2) == ") ") {
                return $where = " AND " . $pars;
            } else {
                return $where = " OR " . $pars;
            }
        } else {
            return '';
        }
    }

    /**
     * Realiza a importação dos dados básicos de usuário de uma entidade para outra
     * @param int $idUsuario
     * @param int $idEntidadeOrigem
     * @param int $idEntidadeDestino
     * @param int $idUsuarioCadastro
     * @return mixed array|false
     */
    public function importarUsuarioEntidade($idUsuario, $idEntidadeOrigem, $idEntidadeDestino, $idUsuarioCadastro)
    {
        try {
            $this->beginTransaction();
            $adapter = Zend_Db_Table_Abstract::getDefaultAdapter();
            $statement = $adapter->prepare('EXEC sp_importarusuario ' . $idUsuario . ' , ' . $idEntidadeOrigem . ' , ' . $idEntidadeDestino . ', ' . $idUsuarioCadastro);
            $statement->execute();
            $dadosPessoa = $statement->fetch();
            $this->commit();
            return ((count($dadosPessoa) < 1) ? false : $dadosPessoa);
        } catch (Zend_Exception $e) {
            $this->rollBack();
            THROW $e;
            return false;
        }
    }

}

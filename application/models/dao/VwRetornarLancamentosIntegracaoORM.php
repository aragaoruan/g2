<?php

/**
 * @author Rafael Rocha / Gabriel Resende
 */
class Totvs_VwRetornarLancamentosIntegracaoORM extends Ead1_ORM {

    public $_name = 'vw_retornar_lancamentos_integracao';
    public $_primary = array('id_venda', 'id_lancamento');
    public $_schema = 'totvs';

}
?>
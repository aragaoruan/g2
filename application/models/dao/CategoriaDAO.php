<?php
/**
 * Classe de persistência de Categorias de Produto no banco de dados
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 * 
 * @package models
 * @subpackage dao
 */
class CategoriaDAO extends Ead1_DAO {
	
	/**
	 * Metodo que cadastra Categoria
	 * 
	 * @param CategoriaTO $to        	
	 * @return Ead1_Mensageiro $mensageiro
	 */
	public function cadastrarCategoria(CategoriaTO $to) {
		$orm = new CategoriaORM ();
		// unset($dTO->arrDados);
		$insert = $to->toArrayInsert ();
		$this->beginTransaction ();
		try {
			$id_categoria = $orm->insert ( $insert );
			$this->commit ();
			return $id_categoria;
		} catch ( Exception $e ) {
			$this->rollBack ();
			$arr ['TO'] = $to;
			$arr ['WHERE'] = $insert;
			$arr ['SQL'] = $e->getMessage ();
			$this->excecao = $arr;
			return false;
		}
	}
	
	/**
	 * Meotodo que Edita Categoria
	 * 
	 * @param CategoriaTO $to        	
	 * @return Boolean
	 */
	public function editarCategoria(CategoriaTO $to) {
		$orm = new CategoriaORM ();
		unset ( $to->arrDados );
		$update = $to->toArrayUpdate ( false, array (
				'id_categoria' 
		) );
		$this->beginTransaction ();
		try {
			$orm->update ( $update, " id_categoria = " . $to->getId_categoria () );
			$this->commit ();
			return true;
		} catch ( Exception $e ) {
			$this->rollBack ();
			$this->excecao = $e->getMessage ();
			return false;
		}
	}
	
	/**
	 * Metodo que desvincula a Categoria
	 * 
	 * @param CategoriaTO $to        	
	 * @return boolean
	 */
	public function desvincularCategoria(CategoriaTO $to) {
		$orm = new CategoriaORM();
		unset($to->arrDados);
		$to->setBl_ativo(false);
		$update = $to->toArrayUpdate(false, array('id_categoria'));
		$this->beginTransaction();
		try {
			$orm->update($update, " id_categoria = " . $to->getId_categoria());
			$categoriaEntidadeORM = new CategoriaEntidadeORM();
			
			//deleta todos os vínculos da categoria com entidades
			$this->deletarCategoriaEntidade($to->getId_categoria());
			
			$this->commit();
			return true;
		} catch(Exception $e) {
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Metodo que retorna Categoria
	 * 
	 * @param CategoriaTO $to        	
	 * @param String $where        	
	 * @return object $obj|boolean
	 */
	public function retornarCategoria(CategoriaTO $to, $where = null) {
		$orm = new CategoriaORM ();
		if (! $where) {
			$where = $orm->montarWhere ( $to );
			//$where .= " AND bl_ativo = 1 "; 
		}
		try {
			$obj = $orm->fetchAll ( $where );
			return $obj;
		} catch ( Zend_Exception $e ) {
			$msg ['WHERE'] = $where;
			$msg ['SQL'] = $e->getMessage ();
			$msg ['TO'] = $to;
			$this->excecao = $msg;
			THROW new Zend_Exception ( $e->getMessage () );
			return false;
		}
	}
	
	/**
	 * Metodo que retorna Categoria
	 * 
	 * @param CategoriaTO $to        	
	 * @param String $where        	
	 * @return object $obj|boolean
	 */
	public function retornarCategoriaFilha(CategoriaTO $to, $where = null) {
		$orm = new CategoriaORM ();
		if (! $where) {
			$where = $orm->montarWhere ( $to );
			//$where .= " AND bl_ativo = 1 "; 
		}
		
		$select = $orm->select()
				->setIntegrityCheck(false)
				->from(array('ca' => 'tb_categoria'), '*')
				->join(array('ce' => 'tb_categoriaentidade'), 'ce.id_categoria = ca.id_categoria', array('ce.id_entidade'))
				->where($where);
		try {
			$obj = $orm->fetchAll ( $select );
			return $obj;
		} catch ( Zend_Exception $e ) {
			$msg ['WHERE'] = $where;
			$msg ['SQL'] = $e->getMessage ();
			$msg ['TO'] = $to;
			$this->excecao = $msg;
			THROW new Zend_Exception ( $e->getMessage () );
			return false;
		}
	}
	
	/**
	 * Metodo que retorna Categorias de uma Entidade
	 * 
	 * @param CategoriaEntidadeTO $categoriaEntidadeTO        	
	 * @return object $obj|boolean
	 */
	public function retornarCategoriaEntidade(CategoriaEntidadeTO $categoriaEntidadeTO) {
		$orm = new CategoriaEntidadeORM ();
		if (!$categoriaEntidadeTO->getId_entidade() && !$categoriaEntidadeTO->getId_categoria()) {
			throw new Zend_Exception ( "Necessário informar o id_entidade ou id_categoria" );
		}
		$where = $orm->montarWhere ( $categoriaEntidadeTO );
		try {
			$obj = $orm->fetchAll ( $where );
			return $obj;
		} catch ( Zend_Exception $e ) {
			$this->excecao = $e->getMessage ();
			return false;
		}
	}
	
	/**
	 * Método que retorna as entidades da categoria em questão
	 * @param CategoriaTO $categoriaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarArCategoriaEntidade(CategoriaTO $categoriaTO){
		$orm = new CategoriaEntidadeORM();
		return $orm->fetchAll("id_categoria = " . $categoriaTO->getId_categoria());
	}
	
	/**
	 * Método que deleta as categorias da entidade
	 * @see CategoriaBO::salvarArCategoriaEntidade();
	 *
	 * @param integer $idCategoria
	 * @throws Zend_Exception
	 */
	public function deletarCategoriaEntidade($idCategoria){
		$this->beginTransaction();
		try{
			$orm = new CategoriaEntidadeORM();
			$orm->delete("id_categoria = $idCategoria");
			$this->commit();
		}catch(Zend_Exception $e){
			$this->rollBack();
			throw $e;
		}
	}
	
	/**
	 * Método de vínculo de categoria com entidade
	 * @param CategoriaEntidadeTO $categoriaEntidadeTO
	 * @throws Zend_Exception
	 */
	public function cadastrarCategoriaEntidade(CategoriaEntidadeTO $categoriaEntidadeTO){
		$this->beginTransaction();
		try{
			$orm = new CategoriaEntidadeORM();
			$orm->insert($categoriaEntidadeTO->toArrayInsert());
			$this->commit();
		}catch(Zend_Exception $e){
			$this->rollBack();
			throw $e;
		}
	}
	
	/**
	 * Método que salva um arquivo de imagem para a Categoria
	 * @param UploadTO $imagem
	 * @throws Zend_Exception
	 * @return Ambigous <mixed, multitype:>|boolean
	 */
	public function salvarImagemUpload(UploadTO $imagem){
		$dao = new UploadDAO();
		$this->beginTransaction();
		try {
			$id = $dao->cadastrarUpload($imagem);
			$this->commit();
			return $id;
		} catch (Zend_Exception $e) {
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW $e;
			return false;
		}
	}
}
<?php

/**
 * Classe que busca dados de menu no banco de dados
 * @author Dimas Sulz <dimassulz@gmail.com>
 *
 * @package models
 * @subpackage dao
 */
class MenuDAO extends Ead1_DAO
{

    /**
     *
     * @param int $perfil Código do perfil que esta em sessão
     * @param boolean $principal Verifica se é para trazer o menu
     */
    public function mostrarMenuPerfil($perfil, $idEntidade, $principal = true, $perfilPai = null, $idSistema = 1)
    {
        $menuORM = new VwPerfilEntidadeFuncionalidadeORM();
        if ($principal === true) {
            $wherePerfil = "id_perfil = '{$perfil}' AND id_entidade = {$idEntidade} AND id_perfilpai IS NULL AND id_funcionalidadepai IS NULL and id_sistema = $idSistema";
        } else {
            $wherePerfil = "id_perfil = '{$perfil}' AND id_entidade = {$idEntidade} AND id_funcionalidadepai = '$perfilPai' and id_sistema = $idSistema ";
        }
        return $menuORM->fetchAll($wherePerfil, "nu_ordem ASC")->toArray();
    }

    /**
     * Método que busca as funcionalidades que não possuem pai
     * @param $perfil id do perfil
     * @param $idEntidade id da entidade
     */
    public function mostrarMenuPrincipal($perfil, $idEntidade)
    {
        $menuORM = new VwPerfilEntidadeFuncionalidadeORM();
        $wherePerfil = "id_perfil = '{$perfil}' AND id_entidade = {$idEntidade} and id_sistema = 1 AND id_funcionalidadepai IS NULL AND id_situacao != 132 AND id_tipofuncionalidade IN (1, 2, 3)";
        return $menuORM->fetchAll($wherePerfil, "id_funcionalidade ASC")->toArray();
    }

    /**
     * Método que busca as funcionalidades que possuem pai
     * @param $perfil id do perfil
     * @param $idEntidade id da entidade
     */
    public function mostrarMenuPorPerfilEntidade($perfil, $idEntidade)
    {
        $menuORM = new VwPerfilEntidadeFuncionalidadeORM();
        $wherePerfil = "id_perfil = '$perfil' AND id_entidade = $idEntidade and id_sistema = 1 AND id_funcionalidadepai is not null and id_situacao != 132 AND id_tipofuncionalidade IN (1, 2, 3)";
        return $menuORM->fetchAll($wherePerfil, "st_funcionalidade ASC")->toArray();
    }

    /**
     * Método que busca as funcionalidades por perfil
     * @param $perfil
     * @param $idEntidade
     * @param int $idSistema
     * @param int $idSituacao
     * @return array
     * @throws Zend_Exception
     */
    public function mostrarMenuPorPerfilEntidadeTodos($perfil, $idEntidade,
                                                      $idSistema = \G2\Constante\Sistema::GESTOR,
                                                      $idSituacao = \G2\Constante\Situacao::TB_FUNCIONALIDADE_HTML)
    {
        $menuORM = new VwPerfilEntidadeFuncionalidadeORM();
        $wherePerfil = " id_entidade = $idEntidade 
                        AND id_sistema = $idSistema 
                        AND id_situacao != " . \G2\Constante\Situacao::TB_FUNCIONALIDADE_FLEX . " 
                        AND id_tipofuncionalidade IN (
                                " . \G2\Constante\TipoFuncionalidade::PERFIL_MENU_PRINCIPAL . ", 
                                " . \G2\Constante\TipoFuncionalidade::PERFIL_MENU_LATERAL . ",
                                " . \G2\Constante\TipoFuncionalidade::PERFIL_SUB_MENU_LATERAL . "
                            )";

        if (isset($perfil) && !empty($perfil)) {
            $wherePerfil .= " AND id_perfil = " . $perfil;
        }

        $wherePerfil .= " AND id_tipofuncionalidade not in (
                                    " . \G2\Constante\TipoFuncionalidade::FUNCIONALIDADE_INTERNA . ",
                                    " . \G2\Constante\TipoFuncionalidade::BOTAO . "
                              )"; // retirando interno e botões
        $wherePerfil .= " AND id_situacao = " . $idSituacao;

        $result = $menuORM->fetchAll($wherePerfil, "nu_ordem ASC");

        if ($result) {
            return $result->toArray();
        }
    }

    /**
     * Método que verifica a situação de envio do TCC por aluno. (tb_alocacao -> id_situacaotcc)
     * @param $id_usuario id do usuário
     * @param $idEntidade id da entidade
     * @param $id_tipodisciplina id do tipo da disciplina
     */
    public function verificaSituacaoTcc($id_matricula, $idEntidade, $id_tipodisciplina = 2)
    {
        $menuORM = new VwAlunoGradeIntegracaoORM();
        $whereTcc = "id_matricula = $id_matricula AND id_entidade = $idEntidade and id_tipodisciplina = $id_tipodisciplina";
        return $menuORM->fetchAll($whereTcc)->toArray();
    }

    /**
     * Método que verifica a situação do agendamento de prova presencial (tb_avaliacaoagendamento)
     * @param $id_matricula id da matricula
     * @autor: Débora Castro <debora.castro@unyleya.com.br>
     */
    public function verificaSituacaoProvaPresencial($id_matricula)
    {
//        $negocioentity = new \G2\Negocio\GerenciaProva();
        $bo = new PortalAvaliacaoBO();
//        $params['id_matricula']=$id_matricula;
//        $params['id_entidade']=$idEntidade;
        $retorno = $bo->retornaMostraMenuAgendamento($id_matricula);
        return $retorno;
    }

}

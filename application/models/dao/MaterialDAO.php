<?php
/**
 * Classe de persistencia de material com o banco de dados
 * @author Dimas Sulz <dimassulz@gmail.com>
 * 
 * @package models
 * @subpackage dao
 */
class MaterialDAO extends Ead1_DAO{
	
	/**
	 * Método que cadastra (insert) item de material
	 * @param ItemDeMaterialTO $idmTO
	 * @see MaterialBO::cadastrarItemDeMaterial();
	 * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
	 */
	public function cadastrarItemDeMaterial(ItemDeMaterialTO $idmTO){
		$itemDeMaterialORM = new ItemDeMaterialORM();
		return $itemDeMaterialORM->insert($idmTO->toArrayInsert());
	}
	
	/**
	 * Metodo que cadastra o projeto pedagogico do material
	 * @param MaterialProjetoTO $mpTO
	 * @see MaterialBO::cadastrarMaterialProjeto();
	 * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
	 */
		public function cadastrarMaterialProjeto(MaterialProjetoTO $mpTO){
			try{
				$this->beginTransaction();
				$tb = new  MaterialProjetoORM();
				$id_materialprojeto = $tb->insert($mpTO->toArrayInsert()); 
				$this->commit();
				return $id_materialprojeto;
			}catch (Zend_Exception $e){
				$this->rollBack();
				$this->excecao = $e->getMessage();
				THROW new Zend_Exception($e->getMessage());
				return false;
			}
	}
	
	/**
	 * Método que cadastra (insert) item de material area
	 * @param ItemDeMaterialAreaTO $idmaTO
	 * @see MaterialBO::cadastrarItemDeMaterialArea();
	 * @return boolean
	 */
	public function cadastrarItemDeMaterialArea(ItemDeMaterialAreaTO $idmaTO){
		$itemDeMaterialAreaORM = new ItemDeMaterialAreaORM();
		return $itemDeMaterialAreaORM->insert($idmaTO->toArrayInsert());
	}
	
	/**
	 * Método que cadastra (insert) item de material disciplina
	 * @param ItemDeMaterialDisciplinaTO $idmdTO
	 * @see MaterialBO::cadastrarItemDeMaterialDisciplina();
	 * @return boolean
	 */
	public function cadastrarItemDeMaterialDisciplina(ItemDeMaterialDisciplinaTO $idmdTO){
		$itemDeMaterialDisciplinaORM = new ItemDeMaterialDisciplinaORM();
		$this->beginTransaction();
		try{
			$itemDeMaterialDisciplinaORM->insert($idmdTO->toArrayInsert());
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			THROW new Zend_Exception($e->getMessage());
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Metodo que cadastra a entrega de material
	 * @param EntregaMaterialTO $emTO
	 * @see MaterialBO::cadastrarEntregaMaterial();
	 * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
	 */
	public function cadastrarEntregaMaterial(EntregaMaterialTO $emTO){
		$this->beginTransaction();
		try{
			$entregaMaterialORM = new EntregaMaterialORM();
			$id_entregamaterial = $entregaMaterialORM->insert($emTO->toArrayInsert());
			$this->commit();
			return $id_entregamaterial;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($e->getMessage());
			return false;
		}
	}
	
	/**
	 * Metodo que cadastra a Observacao
	 * @param ObservacaoTO $oTO
	 * @see MaterialBO::cadastrarObservacao();
	 * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
	 */
	public function cadastrarObservacao(ObservacaoTO $oTO){
		$this->beginTransaction();
		try{
			$observacaoORM = new ObservacaoORM();
			$id_observacao = $observacaoORM->insert($oTO->toArrayInsert());
			$this->commit();
			return $id_observacao;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($e->getMessage());
			return false;
		}
	}
	
	/**
	 * Metodo que cadastra a Observacao da Entrega de Material
	 * @param ObservacaoEntregaMaterialTO $oemTO
	 * @see MaterialBO::cadastrarObservacaoEntregaMaterial();
	 * @return boolean
	 */
	public function cadastrarObservacaoEntregaMaterial(ObservacaoEntregaMaterialTO $oemTO){
		$this->beginTransaction();
		try{
			$observacaoEntregaMaterialORM = new ObservacaoEntregaMaterialORM();
			$observacaoEntregaMaterialORM->insert($oemTO->toArrayInsert());
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($e->getMessage());
			return false;
		}
	}
	
	/**
	 * Método que cadastra (insert) tipo de material
	 * @param TipoDeMaterialTO $tdmTO
	 * @see MaterialBO::cadastrarTipoDeMaterial();
	 * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
	 */
	public function cadastrarTipoDeMaterial(TipoDeMaterialTO $tdmTO){
		$tipoDeMaterialORM = new TipoDeMaterialORM();
		$this->beginTransaction();
		try{
			$id_tipodematerial = $tipoDeMaterialORM->insert($tdmTO->toArrayInsert());
			$this->commit();
			return $id_tipodematerial;
		}catch(Zend_Exception $e){
			$this->rollBack();
			THROW new Zend_Exception($e->getMessage());
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Método que edita (update) item de material
	 * @param ItemDeMaterialTO $idmTO
	 * @see MaterialBO::editarItemDeMaterial()
	 * @return boolean
	 */
	public function editarItemDeMaterial(ItemDeMaterialTO $idmTO){
		$itemDeMaterialORM = new ItemDeMaterialORM();
		$update = $idmTO->toArrayUpdate(false,array('id_itemdematerial'));
		$this->beginTransaction();
		try{
			$itemDeMaterialORM->update($update, " id_itemdematerial = ".$idmTO->getId_itemdematerial());
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Metodo que edita a entrega de material
	 * @param EntregaMaterialTO $emTO
	 * @param Mixed $where
	 * @see MaterialBO::editarEntregaMaterial();
	 * @return boolean
	 */
	public function editarEntregaMaterial(EntregaMaterialTO $emTO,$where = null){
		$this->beginTransaction();
		try{
			$entregaMaterialORM = new EntregaMaterialORM();
			$update = $emTO->toArrayUpdate(false,array('id_entregamaterial','id_matricula','id_usuariocadastro','id_itemdematerial','id_material','dt_entrega'));
			if(!$where){
				$where = $entregaMaterialORM->montarWhere($emTO);
			}
			$entregaMaterialORM->update($update,$where);
			$this->commit();
			return true;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($this->excecao);
			return false;
			
		}
	}
	
	/**
	 * Método que edita (update) tipo de material
	 * @param TipoDeMaterialTO $tdmTO
	 * @see MaterialBO::editarTipoDeMaterial()
	 * @return boolean
	 */
	public function editarTipoDeMaterial(TipoDeMaterialTO $tdmTO){
		$tipoDeMaterialORM = new TipoDeMaterialORM();
		$update = $tdmTO->toArrayUpdate(false,array('id_tipodematerial'));
		$this->beginTransaction();
		try{
			$tipoDeMaterialORM->update($update, " id_tipodematerial = ".$tdmTO->getId_tipodematerial());
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			THROW new Zend_Exception($e->getMessage());
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Deleta registro da tabela tipo de material
	 * @param TipoDeMaterialTO $tdmTO
	 * @see MaterialBO::deletarTipoDeMaterial();
	 * @return boolean
	 */
	public function deletarTipoDeMaterial(TipoDeMaterialTO $tdmTO){
		try{
			$this->beginTransaction();
		$tipoDeMaterialORM = new TipoDeMaterialORM();
		$where = 'id_tipodematerial = '.$tdmTO->getId_tipodematerial();
		$tipoDeMaterialORM->delete($where);
		$this->commit();
		return true;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW $e;
			return false;
		}
	}
	
	/**
	 * Deleta registro da tabela item de material
	 * @param ItemDeMaterialTO $idmTO
	 * @see MaterialBO::deletarItemDeMaterial();
	 * @return boolean
	 */
	public function deletarItemDeMaterial(ItemDeMaterialTO $idmTO){
		$itemDeMaterialORM = new ItemDeMaterialORM();
		$where = 'id_itemdematerial = '.$idmTO->getId_itemdematerial();
		$this->beginTransaction();
		try{
			$itemDeMaterialORM->delete($where);
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW $e;
			return false;
		}
	}
	
	/**
	 * Metodo que Exclui o Projeto do Material
	 * @param MaterialProjetoTO $mpTO
	 * @see MaterialBO::deletarMaterialProjeto();
	 * @return boolean
	 */
	public function deletarMaterialProjeto(MaterialProjetoTO $mpTO){
		$this->beginTransaction();
		try{
			$tb = new MaterialProjetoORM();
			$where = $tb->montarWhere($mpTO);
			if(empty($where)){
				THROW new Zend_Exception('O TO não pode vir vazio!');
			}
			$tb->delete($where);
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($e->getMessage());
			return false;
		}
	}
	
	/**
	 * Deleta registro da tabela item de material area
	 * @param ItemDeMaterialAreaTO $idmaTO
	 * @see MaterialBO::deletarItemDeMaterialArea();
	 * @return boolean
	 */
	public function deletarItemDeMaterialArea(ItemDeMaterialAreaTO $idmaTO){
		try{
			$this->beginTransaction();
			$itemDeMaterialAreaORM = new ItemDeMaterialAreaORM();
			$where = $itemDeMaterialAreaORM->montarWhere($idmaTO);
			$itemDeMaterialAreaORM->delete($where);
			$this->commit();
			return true;
		}catch (Zend_Exception $e){
			$this->rollBack();
			THROW $e;
			return false;
		}
	}
	
	/**
	 * Deleta registro da tabela item de material area
	 * @param ItemDeMaterialDisciplinaTO $idmdTO
	 * @see MaterialBO::deletarItemDeMaterialDisciplina();
	 * @return boolean
	 */
	public function deletarItemDeMaterialDisciplina(ItemDeMaterialDisciplinaTO $idmdTO){
		$itemDeMaterialDisciplinaORM = new ItemDeMaterialDisciplinaORM();
		$where = $itemDeMaterialDisciplinaORM->montarWhere($idmdTO); 
		$this->beginTransaction();
		try{
			if(empty($where)){
				THROW new Zend_Exception('O TO não pode vir vazio!');
			}
			$itemDeMaterialDisciplinaORM->delete($where);
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW $e;
			return false;
		}
	}
	
	/**
	 * Método que retorna tipo de material
	 * @param Material_VwTipoDeMaterial $vwTipoMaterialTO
	 * @see MaterialBO::retornarTipoDeMaterial();
	 * @return mixed Zend_Db_Table_Rowset_Abstract|false 
	 */
	public function retornarTipoDeMaterial(Material_VwTipoDeMaterialTO  $vwTipoMaterialTO){
		$vwTipoDeMaterialORM	= new Material_VwTipoDeMaterialORM();
		return $vwTipoDeMaterialORM->fetchAll($vwTipoDeMaterialORM->montarWherePesquisa($vwTipoMaterialTO, false, true) );
	}
	
	/**
	 * Método que retorna o item de material
	 * @param ItemDeMaterialTO $idmTO
	 * @see MaterialBO::retornarItemDeMaterial();
	 * @return Zend_Db_Table_Rowset_Abstract|false 
	 */
	public function retornarItemDeMaterial(ItemDeMaterialTO $idmTO){
		$itemDeMaterialORM = new ItemDeMaterialORM();
		try{
			$obj = $itemDeMaterialORM->fetchAll($itemDeMaterialORM->montarWhere($idmTO,false));
			$this->commit();
			return $obj;
		}catch(Zend_Exception $e){
			return false;
		}
	}
	
	/**
	 * Método que retorna o item de material de uma área do conhecimento
	 * @param ItemDeMaterialAreaTO $idmaTO
	 * @see MaterialBO::retornarItemDeMaterialArea();
	 * @return Zend_Db_Table_Rowset_Abstract|false 
	 */
	public function retornarItemDeMaterialArea(ItemDeMaterialAreaTO $idmaTO){
		$itemDeMaterialAreaORM = new ItemDeMaterialAreaORM();
		try{
			$obj = $itemDeMaterialAreaORM->fetchAll($itemDeMaterialAreaORM->montarWhere($idmaTO));
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Método que retorna o item de material de uma disciplina
	 * @param ItemDeMaterialDisciplinaTO $idmdTO
	 * @see MaterialBO::retornarItemDeMaterialDisciplina() | MaterialBO::retornarDisciplinasItemDeMaterial();
	 * @param String $where
	 * @return Zend_Db_Table_Rowset_Abstract|false 
	 */
	public function retornarItemDeMaterialDisciplina(ItemDeMaterialDisciplinaTO $idmdTO,$where = null){
		$itemDeMaterialDisciplinaORM = new ItemDeMaterialDisciplinaORM();
		try{
			if(!$where){
				$where = $itemDeMaterialDisciplinaORM->montarWhere($idmdTO);
			}
			$obj = $itemDeMaterialDisciplinaORM->fetchAll($where);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($e->getMessage());
			return false;
		}
	}
	
	/**
	 * Metodo que Retorna a view do material com o projeto pedagogico
	 * @param VwMaterialProjetoPedagogicoTO $vwMppTO
	 * @see MaterialBO::retornarVwMaterialProjetoPedagogico()
	 * @param String $where
	 * @return Zend_Db_Table_Rowset_Abstract|false 
	 */
	public function retornarVwMaterialProjetoPedagogico(VwMaterialProjetoPedagogicoTO $vwMppTO,$where = null){
		$vwMaterialProjetoPedagogicoORM = new VwMaterialProjetoPedagogicoORM();
		try{
			if(!$where){
				$where = $vwMaterialProjetoPedagogicoORM->montarWhere($vwMppTO);
			}
			$obj = $vwMaterialProjetoPedagogicoORM->fetchAll($where);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($e->getMessage());
			return false;
		}
	}
	
	/**
	 * Método que retorna o projeto pedagogico do material
	 * @param MaterialProjetoTO $mpTO
	 * @param String $where
	 * @see MaterialBO::retornarProjetosMaterial()
	 * @param String $where
	 * @return Zend_Db_Table_Rowset_Abstract|false 
	 */
	public function retornarMaterialProjeto(MaterialProjetoTO $mpTO,$where = null){
		$materialProjetoORM = new MaterialProjetoORM();
		try{
			if(!$where){
				$where = $materialProjetoORM->montarWhere($mpTO);
			}
			$obj = $materialProjetoORM->fetchAll($where);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($e->getMessage());
			return false;
		}
	}
	
	
	/**
	 * Retorna as áreas de conhecimento para vincular ao item de material
	 * @return Zend_Db_Table_Rowset
	 */
	public function retornarAreaConhecimento() {
		$areaConhecimentoORM	= new AreaConhecimentoORM();
		return $areaConhecimentoORM->fetchAll();
	}
	
	/**
	 * Metodo que retorna a entrega de material
	 * @param EntregaMaterialTO $emTO
	 * @throws Zend_Exception
	 * @return Zend_Db_Table_Rowset_Abstract
	 */
	public function retornarEntregaMaterial(EntregaMaterialTO $emTO){
		try{
			$entregaMaterialORM = new EntregaMaterialORM();
			$where = $entregaMaterialORM->montarWhere($emTO);
			return $entregaMaterialORM->fetchAll($where);
		}catch (Zend_Exception $e){
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($e->getMessage());
			return false;
		}
	}
	
	/**
	 * Metodo que retorna a observacao
	 * @param ObservacaoTO $oTO
	 * @param Mixed $where
	 * @see MaterialBO::retornarObservacao()
	 * @throws Zend_Exception
	 * @return Zend_Db_Table_Rowset_Abstract
	 */
	public function retornarObservacao(ObservacaoTO $oTO, $where = null){
		try{
			$observacaoORM = new ObservacaoORM();
			if(!$where){
				$where = $observacaoORM->montarWhere($oTO);
			}
			return $observacaoORM->fetchAll($oTO);
		}catch (Zend_Exception $e){
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($e->getMessage());
			return false;
		}
	}
	
	/**
	 * Metodo que retorna a observacao da entrega de material
	 * @param ObservacaoEntregaMaterialTO $oemTO
	 * @param Mixed $where
	 * @see MaterialBO::retornarObservacaoEntregaMaterial()
	 * @throws Zend_Exception
	 * @return Zend_Db_Table_Rowset_Abstract
	 */
	public function retornarObservacaoEntregaMaterial(ObservacaoEntregaMaterialTO $oemTO, $where = null){
		try{
			$observacaoEntregaMaterialORM = new ObservacaoEntregaMaterialORM();
			if(!$where){
				$where = $observacaoEntregaMaterialORM->montarWhere($oemTO);
			}
			return $observacaoEntregaMaterialORM->fetchAll($oemTO);
		}catch (Zend_Exception $e){
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($e->getMessage());
			return false;
		}
	}
	
	/**
	 * Metodo que retorna a os dados da entrega de material
	 * @param VwEntregaMaterialTO $vwEmTO
	 * @param Mixed $where
	 * @see MaterialBO::retornarVwEntregaMaterial()
	 * @throws Zend_Exception
	 * @return Zend_Db_Table_Rowset_Abstract
	 */
	public function retornarVwEntregaMaterial(VwEntregaMaterialTO $vwEmTO,$where = null){
		try{
			$vwEntregaMaterialORM = new VwEntregaMaterialORM();
			if(!$where){
				$where = $vwEntregaMaterialORM->montarWherePesquisa($vwEmTO,false,true);
			}
			return $vwEntregaMaterialORM->fetchAll($where);
		}catch (Zend_Exception $e){
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($e->getMessage());
			return false;
		}
	}
	
	/**
	 * Metodo que retorna a observacao da entrega de material
	 * @param VwObservacaoEntregaMaterialTO $vwOemTO
	 * @param Mixed $where
	 * @see MaterialBO::retornarVwObservacaoEntregaMaterial()
	 * @throws Zend_Exception
	 * @return Zend_Db_Table_Rowset_Abstract
	 */
	public function retornarVwObservacaoEntregaMaterial(VwObservacaoEntregaMaterialTO $vwOemTO,$where = null){
		try{
			$vwObservacaoEntregaMaterialORM = new VwObservacaoEntregaMaterialORM();
			if(!$where){
				$where = $vwObservacaoEntregaMaterialORM->montarWherePesquisa($vwOemTO,false,true);
			}
			return $vwObservacaoEntregaMaterialORM->fetchAll($where);
		}catch (Zend_Exception $e){
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($e->getMessage());
			return false;
		}
	}
}
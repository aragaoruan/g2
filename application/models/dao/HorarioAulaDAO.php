<?php
/**
 * Classe de persistencia do HorarioAula
 * @author Eduardo Romão - ejushiro@gmail.com
 * 
 * @package models
 * @subpackage dao
 */
class HorarioAulaDAO extends Ead1_DAO{
		
	/**
	 * Metodo que Cadastra Horario da Aula
	 * @param HorarioAulaTO $haTO
	 * @see HorarioAulaBO::cadastrarHorarioAula();
	 * @return int|false retorna o valor do id inserido no banco ou false senão conseguir
	 */
	public function cadastrarHorarioAula(HorarioAulaTO $haTO){
		$horarioAulaORM = new HorarioAulaORM();
		$this->beginTransaction();
		try{
			$id_horarioAula = $horarioAulaORM->insert($haTO->toArrayInsert());
			$this->commit();
			return $id_horarioAula;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}
		
	/**
	 * Metodo que Cadastra o Horario em Determinado Dia da Semana
	 * @see HorarioAulaBO::cadastrarHorarioDiaSemana();
	 * @return boolean
	 */
	public function cadastrarHorarioDiaSemana(HorarioDiaSemanaTO $hdsTO){
		$horarioDiaSemanaORM = new HorarioDiaSemanaORM();
		$this->beginTransaction();
		try{
			$horarioDiaSemanaORM->insert($hdsTO->toArrayInsert());
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}
		
	/**
	 * Metodo que Edita o Horario da Aula
	 * @see HorarioAulaBO::editarHorarioAula();
	 * @return boolean
	 */
	public function editarHorarioAula(HorarioAulaTO $haTO){
		$horarioAulaORM = new HorarioAulaORM();
		$this->beginTransaction();
		try{
			$horarioAulaORM->update($haTO->toArrayUpdate(false, array('id_horarioaula')), 'id_horarioaula = '.$haTO->getId_horarioaula());
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}
		
	/**
	 * Metodo que Exclui o Horario da Aula
	 * @see HorarioAulaBO::deletarHorarioAula();
	 * @return boolean
	 */
	public function deletarHorarioAula(HorarioAulaTO $haTO){
		$horarioAulaORM = new HorarioAulaORM();
		$where = 'id_horarioaula = '.$haTO->getId_horarioaula();
		$this->beginTransaction();
		try{
			$horarioAulaORM->delete($where);
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}
		
	/**
	 * Metodo que Exclui o Horario da Aula e seus vínculos com Dias de Semana
	 * @see HorarioAulaBO::deletarHorarioAulaDiaSemana();
	 * @return boolean
	 */
	public function deletarHorarioAulaDiaSemana(HorarioAulaTO $haTO){
		$horarioAulaORM = new HorarioAulaORM();
		$where = 'id_horarioaula = '.$haTO->getId_horarioaula();
		$hdsTO = new HorarioDiaSemanaTO();
		$hdsTO->setId_horarioaula($haTO->getId_horarioaula());
		$this->beginTransaction();
		try{
			$this->deletarHorarioDiaSemana($hdsTO);
			$horarioAulaORM->delete($where);
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}
		
	/**
	 * Metodo que Exclui Vinculo do Horario da Aula com o Dia da Semana
	 * @see HorarioAulaBO::deletarHorarioDiaSemana();
	 * @return boolean
	 */
	public function deletarHorarioDiaSemana(HorarioDiaSemanaTO $hdsTO){
		$horarioDiaSemanaORM = new HorarioDiaSemanaORM();
		$where = $horarioDiaSemanaORM->montarWhere($hdsTO);
		$this->beginTransaction();
		try{
			if(!$hdsTO->getId_horarioaula()){
				throw new Zend_Exception('É Necessario setar o ID do Horario da Aula!');
			}
			$horarioDiaSemanaORM->delete($where);
		
			$this->commit();
			return true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}
		
	/**
	 * Metodo que Retorna o Horario da Aula
	 * @param HorarioAulaTO $haTO
	 * @see HorarioAulaBO::retornarHorarioAula();
	 * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function retornarHorarioAula(HorarioAulaTO $haTO){
		$horarioAulaORM = new HorarioAulaORM();
		try{
			$obj = $horarioAulaORM->fetchAll($horarioAulaORM->montarWhere($haTO));
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		}
	}
		
	/**
	 * Metodo que Retorna o Dia da Semana do Horario da Aula
	 * @param HorarioDiaSemanaTO $hdsTO
	 * @see HorarioAulaBO::retornarHorarioDiaSemana();
	 * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function retornarHorarioDiaSemana(HorarioDiaSemanaTO $hdsTO){
		$horarioDiaSemanaORM = new HorarioDiaSemanaORM();
		try{
			$obj = $horarioDiaSemanaORM->fetchAll($horarioDiaSemanaORM->montarWhere($hdsTO));
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		}
	}
		
	/**
	 * Metodo que Retorna o Turno
	 * @param TurnoTO $tTO
	 * @see HorarioAulaBO::retornarTurno();
	 * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function retornarTurno(TurnoTO $tTO){
		$turnoORM = new TurnoORM();
		try{
			$obj = $turnoORM->fetchAll($turnoORM->montarWhere($tTO));
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		}
	}
		
	/**
	 * Metodo que Retorna DIa da Semana
	 * @param DiaSemanaTO $dsTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarDiaSemana(DiaSemanaTO $dsTO){
		$diaSemanaORM = new DiaSemanaORM();
		try{
			$obj = $diaSemanaORM->fetchAll($diaSemanaORM->montarWhere($dsTO));
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Metodo que Retorna o Turno e o Horario da Aula
	 * @param VwTurnoHorarioAulaTO $vwthaTO
 	 * @see HorarioAulaBO::retornarTurnoHorarioAula();
	 * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function retornarTurnoHorarioAula(VwTurnoHorarioAulaTO $vwthaTO){
		$vwTurnoHorarioAulaORM = new VwTurnoHorarioAulaORM();
		try{
			$obj = $vwTurnoHorarioAulaORM->fetchAll($vwTurnoHorarioAulaORM->montarWhere($vwthaTO));
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Metodo que Retorna o Dia da Semana por horario aula
	 * @param VwHorarioAulaDiaSemanaTO $vwhadsTO
 	 * @see HorarioAulaBO::retornarHorarioAulaDiaSemana();
	 * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function retornarHorarioAulaDiaSemana(VwHorarioAulaDiaSemanaTO $vwhadsTO){
		$vwHorarioAulaDiaSemanaORM = new VwHorarioAulaDiaSemanaORM();
		try{
			$obj = $vwHorarioAulaDiaSemanaORM->fetchAll($vwHorarioAulaDiaSemanaORM->montarWhere($vwhadsTO));
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Metodo que chama a sp criaperiodosh7 para integrar o HorarioAula no sistema Henry7
	 * @param HorarioAulaTO $haTO
	 * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function executaCriaperiodoh7(HorarioAulaTO $haTO) {
		$this->beginTransaction();
		try{
			$adapter	= Zend_Db_Table_Abstract::getDefaultAdapter();
			$statement	= $adapter->prepare( 'EXECUTE sp_criaperiodosh7 ?');
			$statement->execute( array( $haTO->getId_horarioaula() ) );
			$this->commit();
			return	 true;
		}catch(Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
}
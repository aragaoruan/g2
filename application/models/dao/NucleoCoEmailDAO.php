<?php
/**
 * Classe NucleoCoEmailDAO
 * @package models
 * @subpackage dao
 */
class NucleoCoEmailDAO extends Ead1_DAO {

	
	/**
	 * Método que lista dados da VwNucleoCoEmail 
	 * @param VwNucleoCoEmailTO $nucleoCoEmail
	 * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >
	 */
	public function listarEmail(VwNucleoCoEmailTO $nucleoCoEmail){
		$orm = new VwNucleoCoEmailORM();
		$select = $orm->select()
		->where(" id_entidade = ?",$nucleoCoEmail->getId_entidade());
		
		if($nucleoCoEmail->getAdicionar()){
			$array_nucleos = $nucleoCoEmail->fetch(false,false,true);
			foreach ($array_nucleos as $nucleoEmail){
				$id_email .= (empty($id_email) ? $nucleoEmail->id_emailconfig : ' ,'.$nucleoEmail->id_emailconfig );
			}
			$select->distinct(true)
				->from ( 'vw_nucleocoemail', array( 'id_emailconfig' ,'st_titulo' , 'st_usuario' , 'id_entidade' ) );
						
			$sel = " id_nucleoco <> ".$nucleoCoEmail->getId_nucleoco() . " OR id_nucleoco is NULL ";
 			if(!empty($id_email)){
 				$NOT_IN = "id_emailconfig NOT IN (".$id_email.")";
 				$select->where($NOT_IN);
 			}
		}
		else{
			$sel = " id_nucleoco = ".$nucleoCoEmail->getId_nucleoco();
			
		}
		$select 
  			->where($sel);
		
		return $orm->fetchAll($select);
		
		
	}
	
	/**
	 * Vincula e-mails ao núcleo
	 * @param NucleoCoEmailTO $nucleoCoEmailTo
	 * @throws Zend_Exception
	 */
	public function cadastrarNucleoCoEmail(NucleoCoEmailTO $nucleoCoEmailTo){
		$this->beginTransaction();
		try{
			$orm = new NucleoCoEmailORM();
			$orm->insert($nucleoCoEmailTo->toArrayInsert());
			$this->commit();
		}catch(Zend_Exception $e){
			$this->rollBack();
			throw $e;
		}
	}
	
	/**
	 * Exclui Vinculo do Email com o Núcleo
	 * @param NucleoCoEmailTO $nucleoCoEmailTO
	 * @throws Zend_Exception
	 */
	public function excluirNucleoCoEmail(NucleoCoEmailTO $nucleoCoEmailTO){
		$this->beginTransaction();
		try{
			$orm = new NucleoCoEmailORM();
			$orm->delete("id_nucleoco = " .$nucleoCoEmailTO->getId_nucleoco() ." AND id_emailconfig = ".$nucleoCoEmailTO->getId_emailconfig());
			$this->commit();
		}catch(Zend_Exception $e){
			$this->rollBack();
			throw $e;
		}
	}
	
	
	
}
<?php
/**
 * Classe que realiza interações de Configuração de Interface com o banco e/ou fonte de dados
 * @author edermariano
 *
 * @package models
 * @subpackage dao
 */
class InterfaceDAO extends Ead1_DAO{
	
	/**
	 * Método que retorna os dados da view interface pessoa
	 * @param VwInterfacePessoaTO $vwInterfacePessoaTO
	 */
	public function retornarVwInterfacePessoa(VwInterfacePessoaTO $vwInterfacePessoaTO){
		return $vwInterfacePessoaTO->fetch();
	}
	
	/**
	 * Método que exclui todas as configurações de uma pessoa
	 * @param InterfacePessoaTO $interfacePessoaTO
	 */
	public function deletarInterfacePessoa(InterfacePessoaTO $interfacePessoaTO){
		$orm = new InterfacePessoaORM();
		$where = "id_usuario = " . $interfacePessoaTO->getId_usuario();
		$where .= " AND id_entidade = " . $interfacePessoaTO->getId_entidade();
		return new Ead1_Mensageiro($orm->delete($where));
	}
	
	/**
	 * Método que edita uma configuração de interface da pessoa 
	 * @param InterfacePessoaTO $interfacePessoaTO
	 */
	public function editarInterfacePessoa(InterfacePessoaTO $interfacePessoaTO){
		$orm = new InterfacePessoaORM();
		$where = $orm->montarWhere($interfacePessoaTO, true);
		return $orm->update($interfacePessoaTO->toArrayUpdate(false), $where);
	}
	
	/**
	 * Método que insere uma configuração de interface da pessoa 
	 * @param InterfacePessoaTO $interfacePessoaTO
	 */
	public function inserirInterfacePessoa(InterfacePessoaTO $interfacePessoaTO){
		$orm = new InterfacePessoaORM();
		return $orm->insert($interfacePessoaTO->toArrayInsert());
	}
}
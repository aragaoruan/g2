<?php

/**
 * Classe de Abstração de dados de Mensagem
 * @author eduardoromao
 */
class MensagemDAO extends Ead1_DAO
{

    /**
     * Método que cadastra a Mensagem
     * @param MensagemTO $mensagemTO
     * @return $id_mensagem
     */
    public function cadastrarMensagem(MensagemTO $mensagemTO)
    {
        try {
            $id_mensagem = 0;
            $this->beginTransaction();
            $orm = new MensagemORM();
            $id_mensagem = $orm->insert($mensagemTO->toArrayInsert());
            $this->commit();
        } catch (Zend_Exception $e) {
            $this->rollBack();
            THROW $e;
        }
        return $id_mensagem;
    }

    /**
     * Método que cadastra o Envio da Mensagem
     * @param EnvioMensagemTO $envioMensagemTO
     * @return $id_enviomensagem
     */
    public function cadastrarEnvioMensagem(EnvioMensagemTO $envioMensagemTO)
    {
        try {
            $id_enviomensagem = 0;
            $this->beginTransaction();
            $orm = new EnvioMensagemORM();
            $id_enviomensagem = $orm->insert($envioMensagemTO->toArrayInsert());
            $this->commit();
        } catch (Zend_Exception $e) {
            $this->rollBack();
            THROW $e;
        }
        return $id_enviomensagem;
    }

    /**
     * Método que cadastra os destinatarios do Envio
     * @param EnvioDestinatarioTO $envioDestinatarioTO
     * @return $id_enviodestinatario
     */
    public function cadastrarEnvioDestinatario(EnvioDestinatarioTO $envioDestinatarioTO)
    {
        try {
            $id_enviodestinatario = 0;
            $this->beginTransaction();
            $orm = new EnvioDestinatarioORM();
            $id_enviodestinatario = $orm->insert($envioDestinatarioTO->toArrayInsert());
            $this->commit();
        } catch (Zend_Exception $e) {
            $this->rollBack();
            THROW $e;
        }
        return $id_enviodestinatario;
    }

    /**
     * Método que Edita o Envio da Mensagem
     * @param EnvioMensagemTO $envioMensagemTO
     * @return Boolean $editado
     */
    public function editarEnvioMensagem(EnvioMensagemTO $envioMensagemTO)
    {
        try {
            $editado = false;
            $this->beginTransaction();
            $orm = new EnvioMensagemORM();
            $orm->update(
                $envioMensagemTO->toArrayUpdate(
                    false
                    , array('id_enviomensagem', 'dt_cadastro', 'dt_enviar', 'id_tipoenvio', 'id_mensagem')
                )
                , $orm->montarWhere($envioMensagemTO, true));
            $editado = true;
            $this->commit();
        } catch (Zend_Exception $e) {
            $this->rollBack();
            THROW $e;
        }
        return $editado;
    }

    /**
     * Método que retorna os dados da view de envio de mensagem
     * @param VwEnvioMensagemTO $to
     * @param Mixed $where
     * @return Zend_Db_Table_Rowset
     */
    public function retornarVwEnvioMensagem(VwEnvioMensagemTO $to, $where = null)
    {
        $orm = new VwEnvioMensagemORM();
        return $orm->fetchAll($where ? $where : $orm->montarWherePesquisa($to, false, true), null, 120, 0);
    }

    /**
     * Método Que retorna o Meial de Mensagem da Entidade
     * @param EmailEntidadeMensagemTO $to
     * @param Mixed $where
     * @return Zend_Db_Table_Rowset
     */
    public function retornarEmailEntidadeMensagem(EmailEntidadeMensagemTO $to, $where = null)
    {
        $orm = new EmailEntidadeMensagemORM();
        return $orm->fetchAll($where ? $where : $orm->montarWhere($to));
    }

    /**
     * Método Que retorna o SMS de Mensagem da Entidade
     * @param SMSEntidadeMensagemTO $to
     * @param Mixed $where
     * @return Zend_Db_Table_Rowset
     */
    public function retornarSMSEntidadeMensagem(SMSEntidadeMensagemTO $to, $where = null)
    {
        $orm = new SMSEntidadeMensagemORM();
        return $orm->fetchAll($where ? $where : $orm->montarWhere($to));
    }

}

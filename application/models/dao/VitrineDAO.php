<?php
/**
 * @author Denise <denise.xavier07@gmail.com>
 * @package models
 * @subpackage dao
 * @author Rafael Rocha rafael.rocha.mg@gmail.com
 */
class VitrineDAO extends Ead1_DAO {

	/**
	 * Método que pesquisa no banco de dados o Afiliado responsável pela Vitrine
	 * @param VwPesquisaVitrineTO $vwPesquisaVitrine
	 * @param unknown_type $where
	 * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >
	 */
	public function pesquisarAfiliadoResponsavel(VwPesquisaVitrineTO $vwPesquisaVitrine, $where = null){
		try{
			$orm = new VwPesquisaVitrineORM();
			if(!$where){
				$where = $orm->montarWhere($vwPesquisaVitrine);
			}
			return $orm->fetchAll($where);
		}catch (Zend_Exception $e){
			return $e->getMessage();
		}
	}
	
	/**
	 * Método que insere uma Vitrine no banco de dados
	 * @param VitrineTO $to
	 * @return Ambigous <mixed, multitype:>|boolean
	 */
	public function cadastrarVitrine(VitrineTO $to){
		
		$orm = new VitrineORM();
		// unset($dTO->arrDados);
		$insert = $to->toArrayInsert();
		$this->beginTransaction();
		try {
			$id_vitrine = $orm->insert($insert);
			$this->commit();
			return $id_vitrine;
		} catch ( Exception $e ) {
			$this->rollBack();
			$arr['TO'] = $to;
			$arr['WHERE'] = $insert;
			$arr['SQL'] = $e->getMessage();
			$this->excecao = $arr;
			return false;
		}
	}
	
	/**
	 * Método que atualiza uma Vitrine no banco de dados
	 * @param VitrineTO $to
	 * @return boolean
	 */
	public function editarVitrine(VitrineTO $to){
		
		$orm = new VitrineORM();
		unset($to->arrDados);
		$update = $to->toArrayUpdate(false, array('id_vitrine'));
		$this->beginTransaction();
		try {
			$orm->update($update, " id_vitrine = " . $to->getId_vitrine());
			$this->commit();
			return true;
		} catch(Exception $e) {
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Método que busca um ou mais registros de Vitrine no banco de dados
	 * @param VitrineTO $to
	 * @param unknown_type $where
	 * @throws Zend_Exception
	 * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
	 */
	public function retornarVitrine(VitrineTO $to, $where = null){
		$orm = new VitrineORM();
		if (!$where) {
			$where = $orm->montarWhere($to);
			$where .= $where ? " AND bl_ativo = 1 " : " bl_ativo = 1 ";
		}
		try {
			$obj = $orm->fetchAll($where);
			return $obj;
		} catch (Zend_Exception $e) {
			$msg ['WHERE'] = $where;
			$msg ['SQL'] = $e->getMessage ();
			$msg ['TO'] = $to;
			$this->excecao = $msg;
			THROW new Zend_Exception($e->getMessage());
			return false;
		}
	}
	
	/**
	 * Método que insere um Produto da Vitrine no banco de dados
	 * @param VitrineProdutoTO $to
	 * @return Ambigous <mixed, multitype:>|boolean
	 */
	public function cadastrarVitrineProduto(VitrineProdutoTO $to){
		$to->setBl_ativo(true);
		
		$orm = new VitrineProdutoORM();
		// unset($dTO->arrDados);
		$insert = $to->toArrayInsert();
		$this->beginTransaction();
		try {
			$id_vitrineproduto = $orm->insert($insert);
			$this->commit();
			return $id_vitrineproduto;
		} catch (Exception $e) {
			$this->rollBack();
			$arr['TO'] = $to;
			$arr['WHERE'] = $insert;
			$arr['SQL'] = $e->getMessage();
			$this->excecao = $arr;
			return false;
		}
	}
	
	/**
	 * Método que atualiza um Produto da Vitrine no banco de dados
	 * @param VitrineProdutoTO $to
	 * @throws Zend_Exception
	 * @return boolean
	 */
	public function editarVitrineProduto(VitrineProdutoTO $to){
		
		$orm = new VitrineProdutoORM();
		unset ( $to->arrDados );
		$update = $to->toArrayUpdate(false, array('id_vitrineproduto'));
		$this->beginTransaction();
		try {
			$orm->update($update, " id_vitrineproduto = " . $to->getId_vitrineproduto());
			$this->commit();
			return true;
		} catch(Exception $e) {
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Método que busca um Produto de uma Vitrine no banco de dados
	 * @param VitrineProdutoTO $to
	 * @param unknown_type $where
	 * @throws Zend_Exception
	 * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
	 */
	public function retornarVitrineProduto(VitrineProdutoTO $to, $where = null){
		
		$orm = new VitrineProdutoORM();
		if (!$where) {
			$where = $orm->montarWhere($to);
			//$where .= " AND bl_ativo = 1 ";
		}
		try {
			$obj = $orm->fetchAll($where);
			return $obj;
		} catch(Zend_Exception $e) {
			$msg ['WHERE'] = $where;
			$msg ['SQL'] = $e->getMessage();
			$msg ['TO'] = $to;
			$this->excecao = $msg;
			THROW new Zend_Exception($e->getMessage());
			return false;
		}
	}
	
	/**
	 * Método que busca as VwVitrine no banco de dados
	 * @param VwVitrineTO $to
	 * @param unknown_type $where
	 * @throws Zend_Exception
	 * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
	 */
	public function retornarVwVitrine(VwVitrineTO $to, $where = null){
	
		$orm = new VwVitrineORM();
		if (!$where) {
			$where = $orm->montarWhere($to);
			$where .= $where ? " AND bl_ativo = 1 " : " bl_ativo = 1 ";
		}
		try {
			$obj = $orm->fetchAll($where);
			return $obj;
		} catch(Zend_Exception $e) {
			$msg ['WHERE'] = $where;
			$msg ['SQL'] = $e->getMessage();
			$msg ['TO'] = $to;
			$this->excecao = $msg;
			THROW new Zend_Exception($e->getMessage());
			return false;
		}
	}
	
	/**
	 * @param ModeloVitrineTO $to
	 * @param unknown_type $where
	 * @throws Zend_Exception
	 * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
	 */
	public function retornarModeloVitrine(ModeloVitrineTO $to, $where = null){
	
		$orm = new ModeloVitrineORM();
		if (!$where) {
			$where = $orm->montarWhere($to);
			//$where .= $where ? " AND bl_ativo = 1 " : " bl_ativo = 1 ";
		}
		try {
			$obj = $orm->fetchAll($where);
			return $obj;
		} catch(Zend_Exception $e) {
			$msg ['WHERE'] = $where;
			$msg ['SQL'] = $e->getMessage();
			$msg ['TO'] = $to;
			$this->excecao = $msg;
			THROW new Zend_Exception($e->getMessage());
			return false;
		}
	}
}

?>
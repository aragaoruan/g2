<?php

/**
 * Classe que retorna, edita e cadastra dados de Pperfil no banco de dados
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage dao
 */
class PerfilDAO extends Ead1_DAO
{

    /**
     * Metodo que cadastra Pefil no Sistema
     * @param PerfilTO $pTO
     * @param String $nomeExibicao
     * @return id_perfil|false
     */
    public function cadastrarPerfil(PerfilTO $pTO, $nomeExibicao = NULL)
    {
        $tbPerfil = new PerfilORM();
        $tbPerfilEntidade = new PerfilEntidadeORM();
        $insert = $pTO->toArrayInsert();
        $this->beginTransaction();
        try {
            $id_perfil = $tbPerfil->insert($insert);

            $peTO = new PerfilEntidadeTO();
            $peTO->setId_perfil($id_perfil);
            $peTO->setId_entidade($pTO->getId_entidade());
            $peTO->setSt_nomeperfil($nomeExibicao);

            $tbPerfilEntidade->insert($peTO->toArrayInsert());

            $this->commit();
            return $id_perfil;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que adiciona permissão ao perfil a determinada funcionalidade
     * @param PerfilFuncionalidadeTO $pfTO
     * @return boolean
     */
    public function cadastrarPerfilFuncionalidade(PerfilFuncionalidadeTO $pfTO)
    {
        $tbPerfilFuncionalidade = new PerfilFuncionalidadeORM();
        $this->beginTransaction();
        try {
            $id_funcionalidade = $tbPerfilFuncionalidade->insert($pfTO->toArrayInsert());
            $this->commit();
            return $id_funcionalidade;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que cadastra perfil entidade antigo perfilalias
     * @param PerfilEntidadeTO $peTO
     * @return boolean
     */
    public function cadastrarPerfilEntidade(PerfilEntidadeTO $peTO)
    {
        $tbPerfilEntidade = new PerfilEntidadeORM();
        $this->beginTransaction();
        try {
            if (!$tbPerfilEntidade->consulta($peTO)) {
                $tbPerfilEntidade->insert($peTO->toArrayInsert());
            }
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            Zend_Debug::dump($e->getMessage());
            return false;
        }
    }

    /**
     * Adiciona Permissao ao perfil a fazer uma ação em determinada funcionalidade
     * @param PerfilPermissaoFuncionalidadeTO $ppfTO
     * @return boolean
     */
    public function cadastrarPerfilPermissaoFuncionalidade(PerfilPermissaoFuncionalidadeTO $ppfTO)
    {
        $tbPerfilPermissaoFuncionalidade = new PerfilPermissaoFuncionalidadeORM();
        $this->beginTransaction();
        try {
            $tbPerfilPermissaoFuncionalidade->insert($ppfTO->toArrayInsert());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            throw $e;
            return false;
        }
    }

    /**
     * Metodo que adiciona o usuário do perfil em uma certa instituição (entidade)
     * @param UsuarioPerfilEntidadeTO $upeTO
     * @return bool
     * @throws Exception
     * @throws Zend_Exception
     * @updated Kayo Silva <kayo.silva@unyleya.com.br> 2015-05-15
     */
    public function cadastrarUsuarioPerfilEntidade(UsuarioPerfilEntidadeTO $upeTO)
    {
        $tbUsuarioPerfilEntidade = new UsuarioPerfilEntidadeORM();
        $this->beginTransaction();
        try {

            $inserir = $tbUsuarioPerfilEntidade->insert($upeTO->toArrayInsert());
            if ($inserir) {
                $this->commit();
                return true;
            } else {
                throw new Exception("Erro ao tentar inserir perfil.");
                return false;
            }


        } catch (Zend_Exception $e) {
            $this->rollBack();
            THROW new Zend_Exception($e->getMessage());
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método de cadastro do usuario perfil entidade referencia
     * @param UsuarioPerfilEntidadeReferenciaTO $uperTO
     * @return int|false inteiro com o id da inserção ou false
     */
    public function cadastrarUsuarioPerfilEntidadeReferencia(UsuarioPerfilEntidadeReferenciaTO $uperTO)
    {
        $usuarioPerfilEntidadeReferenciaORM = new UsuarioPerfilEntidadeReferenciaORM();
        $this->beginTransaction();
        try {
            $id = $usuarioPerfilEntidadeReferenciaORM->insert($usuarioPerfilEntidadeReferenciaORM->toArrayInsert());
            $this->commit();
            return $id;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que edita o perfil
     * @param PerfilTO $pTO
     * @param String $nomeExibicao
     * @return boolean
     */
    public function editarPerfil(PerfilTO $pTO, $nomeExibicao = NULL)
    {
        $tbPerfil = new PerfilORM();
        $tbPerfilEntidade = new PerfilEntidadeORM();
        $update = $pTO->toArrayUpdate(false, array('id_perfil'));
        $this->beginTransaction();
        try {
            $tbPerfil->update($update, "id_perfil = " . $pTO->getId_perfil() . " AND id_entidade = " . $pTO->getId_entidade());
            $peTO = new PerfilEntidadeTO();
            $peTO->setId_perfil($pTO->getId_perfil());
            $peTO->setId_entidade($pTO->getId_entidade());
            $peTO->setSt_nomeperfil($nomeExibicao);
            $updatePe = $peTO->toArrayUpdate(false, array('id_perfil', 'id_entidade'));
            $tbPerfilEntidade->update($updatePe, "id_perfil = " . $peTO->getId_perfil() . " AND id_entidade = " . $peTO->getId_entidade());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que adiciona Permissao ao perfil a determinada funcionalidade
     * @param PerfilFuncionalidadeTO $pfTO
     * @return boolean
     */
    public function editarPerfilFuncionalidade(PerfilFuncionalidadeTO $pfTO)
    {
        try {
            if (!$pfTO) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $this->beginTransaction();
            $tbPerfilFuncionalidade = new PerfilFuncionalidadeORM();
            $update = $pfTO->toArrayUpdate(false, array('id_perfil', 'id_funcionalidade'));
            $where = "id_perfil = {$pfTO->getId_perfil()} AND id_funcionalidade = {$pfTO->getId_funcionalidade()}";
            $tbPerfilFuncionalidade->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que edita as Funcionalidades do perfil que não são botões
     * @param PerfilFuncionalidadeTO $pfTO
     * @return boolean
     */
    public function editarPerfilFuncionalidadeSemBotoes(PerfilFuncionalidadeTO $pfTO)
    {
        $tbPerfilFuncionalidade = new PerfilFuncionalidadeORM();
        $update = $pfTO->toArrayUpdate(false, array('id_perfil', 'id_funcionalidade'));
        $this->beginTransaction();
        try {
            $where = "id_perfil = {$pfTO->getId_perfil()} AND id_funcionalidade NOT IN (SELECT id_funcionalidade FROM tb_funcionalidade WHERE id_tipofuncionalidade = 6)";
            $tbPerfilFuncionalidade->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            THROW new Zend_Exception($e->getMessage());
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que edita perfil da entidade
     * @param PerfilAliasTO $paTO
     * @return boolean
     */
    public function editarPerfilEntidade(PerfilEntidadeTO $peTO)
    {
        $tbPerfilEntidade = new PerfilEntidadeORM();
        $update = $peTO->toArrayUpdate(false, array('id_perfilalias'));
        $this->beginTransaction();
        try {
            $tbPerfilEntidade->update($update, "id_perfil = " . $peTO->getId_perfil() . " AND id_entidade = " . $peTO->getId_entidade());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que edita o usuário do perfil em uma certa instituição (entidade)
     * @param UsuarioPerfilEntidadeTO $upeTO
     * @return boolean
     */
    public function editarUsuarioPerfilEntidade(UsuarioPerfilEntidadeTO $upeTO)
    {
        $tbUsuarioPerfilEntidade = new UsuarioPerfilEntidadeORM();
        $update = $upeTO->toArrayUpdate(true, array('id_usuario', 'id_entidade', 'id_perfil'));
        $this->beginTransaction();
        try {
            $tbUsuarioPerfilEntidade->update($update, 'id_usuario = ' . $upeTO->getId_usuario() . ' AND id_entidade = ' . $upeTO->getId_entidade() . ' AND id_perfil = ' . $upeTO->getId_perfil());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que edita Permissao ao perfil a determinada funcionalidade
     * @param PerfilPermissaoFuncionalidadeTO $ppfTO
     * @todo Não testado
     * @return boolean
     */
    public function editarPerfilPermissaoFuncionalidade(PerfilPermissaoFuncionalidadeTO $ppfTO)
    {
        $tbPerfilPermissaoFuncionalidade = new PerfilPermissaoFuncionalidadeORM();
        $update = $ppfTO->toArrayUpdate(false, array('id_perfil', 'id_funcionalidade'));
        $this->beginTransaction();
        try {
            $tbPerfilPermissaoFuncionalidade->update($update, "id_perfil = " . $ppfTO->getId_perfil() . " AND id_funcionalidade = " . $ppfTO->getId_funcionalidade());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método de edição do perfil entidade referencia
     * @param UsuarioPerfilEntidadeReferenciaTO $uperTO
     * @return boolean
     */
    public function editarUsuarioPerfilEntidadeReferencia(UsuarioPerfilEntidadeReferenciaTO $uperTO)
    {
        $usuarioPerfilEntidadeReferenciaORM = new UsuarioPerfilEntidadeReferenciaORM();
        $update = $uperTO->toArrayUpdate(false, array('id_perfilreferencia'));
        $this->beginTransaction();
        try {
            $usuarioPerfilEntidadeReferenciaORM->update($update, "id_perfilreferencia = " . $uperTO->getId_perfilReferencia());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Deleta todos os vínculos de um determinado perfil da tabela tb_perfilpermissaofuncionalidade.
     * @param int $idPerfil
     * @return boolean
     */
    public function deletarPerfilPermissaoFuncionalidade($idPerfil)
    {
        try {
            $tbPerfilPermissaoFuncionalidade = new PerfilPermissaoFuncionalidadeORM();
            $where = 'id_perfil = ' . $idPerfil;
            return $tbPerfilPermissaoFuncionalidade->delete($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que exclui a entidade do perfil
     * @param PefilEntidadeTO $to
     * @param Mixed $where
     * @return boolean
     */
    public function deletarPerfilEntidade(PerfilEntidadeTO $to, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new PerfilEntidadeORM();
            if (!$where) {
                $where = $orm->montarWhere($to);
            }
            $orm->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /*
     * Metodo que retorna todos os Perfis da entidade
     * @param VwPerfisEntidadeTO $vwPeTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornaPerfisEntidade(VwPerfisEntidadeTO $vwPeTO)
    {
        $vwPerfisEntidadeORM = new VwPerfisEntidadeORM();
        try {
            $where = $vwPerfisEntidadeORM->montarWhereView($vwPeTO, false, true);
            $obj = $vwPerfisEntidadeORM->fetchAll($where, 'st_nomeperfil');
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /*
     * Metodo que retorna todas as entidade filhas da entidade em questão e trazendo o id do perfil caso encontrado
     * @param VwPerfisEntidadeTO $vwPeTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornaEntidadeRecursivaPerfil(VwPerfisEntidadeTO $vwPeTO)
    {
        $vwPerfisEntidadeORM = new VwPerfisEntidadeORM();
        try {
            if ($vwPeTO->getId_entidade() === NULL) {
                $vwPeTO->setId_entidade(0);
            }
            if ($vwPeTO->getId_entidadeclasse() === NULL) {
                $vwPeTO->setId_entidadeclasse(0);
            }
            if ($vwPeTO->getId_perfil() === NULL) {
                $vwPeTO->setId_perfil(0);
            }
            $consulta = ($vwPeTO->getId_entidade() !== 0 ? $vwPeTO->getId_entidade() : "NULL") . ", ";
            $consulta .= ($vwPeTO->getId_entidadeclasse() !== 0 ? $vwPeTO->getId_entidadeclasse() : "NULL") . ", ";
            $consulta .= ($vwPeTO->getId_perfil() !== 0 ? $vwPeTO->getId_perfil() : "NULL");
            $statement = $vwPerfisEntidadeORM->getDefaultAdapter()->prepare("EXEC sp_perfilentidaderecursivo {$consulta}");
            $statement->execute();
            $obj = $statement->fetchAll();
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            Zend_Debug::dump($e);
            exit;
            return false;
        }
    }

    /**
     * Metodo que retorna perfil
     * @param PerfilTO $pTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornaPerfil(PerfilTO $pTO)
    {
        $perfilORM = new PerfilORM();
        try {
            $obj = $perfilORM->fetchAll($perfilORM->montarWhere($pTO));
            return $obj;
        } catch (Zend_Exception $e) {
            THROW new Zend_Exception($e->getMessage());
            $this->excecao = $e->getMessage();
            return false;
        }
    }


    /**
     * Metodo que retorna as entidades do perfil
     * @param PerfilEntidadeTO $to
     * @param Mixed $where
     * @throws Zend_Exception
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarPerfilEntidade(PerfilEntidadeTO $to, $where = null)
    {
        $orm = new PerfilEntidadeORM();
        if (!$where) {
            $where = $orm->montarWhere($to);
        }
        try {
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna entidades do perfil com dados mais completos.
     * @param PerfilEntidadeTO $peTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarPerfilEntidadeCompleto(PerfilEntidadeTO $peTO)
    {

        $orm = new PerfilEntidadeORM();
        $where = $orm->montarWhere($peTO);
        $select = $orm->select()
            ->from(array('pe' => 'tb_perfilentidade'), array('*'))
            ->setIntegrityCheck(false)
            ->join(array('pr' => 'tb_perfil'), 'pr.id_perfil = pe.id_perfil', array('st_nomeperfil'))
            ->join(array('en' => 'tb_entidade'), 'en.id_entidade = pe.id_entidade', array('st_nomeentidade'));
        if ($where) {
            $where = 'pe.' . $where;
            $select = $select->where($where);
            $select->where('pr.bl_ativo = 1')
                ->where('en.bl_ativo = 1');
        }
        try {
            $obj = $orm->fetchAll($select);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            Zend_Debug::dump($e->getMessage());
            exit;
            return false;
        }
    }

    /**
     * Metodo que retorna as Pessoas em determinado Perfil
     * @param VwPessoaPerfilTO $vwPpTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornaPessoasPerfil(VwPessoaPerfilTO $vwPpTO)
    {
        $vwPessoasPerfil = new VwPessoaPerfilORM();
        $where = $vwPessoasPerfil->montarWhereView($vwPpTO, false, true);
        try {
            $obj = $vwPessoasPerfil->fetchAll($where, 'st_nomecompleto');
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que retorna os perfis da entidade(instituição) relacionado com as funcionalidades
     * @param VwPerfilEntidadeFuncionalidadeTO $vwPefTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornaPerfilEntidadeFuncionalidade(VwPerfilEntidadeFuncionalidadeTO $vwPefTO)
    {
        $vwPerfilEntidadeFuncionalidade = new VwPerfilEntidadeFuncionalidadeORM();
        try {
            $obj = $vwPerfilEntidadeFuncionalidade->fetchAll($vwPerfilEntidadeFuncionalidade->montarWhere($vwPefTO));
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Retorna as permissões de uma determinada funcionalidade para um perfil
     * @param VwPerfilPermissaoFuncionalidadeTO $vwPpfTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornaPerfilPermissaoFuncionalidade(VwPerfilPermissaoFuncionalidadeTO $vwPpfTO)
    {
        $vwPerfilPermissaoFuncionalidade = new VwPerfilPermissaoFuncionalidadeORM();
        try {
            $obj = $vwPerfilPermissaoFuncionalidade->fetchAll($vwPerfilPermissaoFuncionalidade->montarWhere($vwPpfTO));
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna o perfil entidade referência
     * @param UsuarioPerfilEntidadeReferenciaTO $uperTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarUsuarioPerfilEntidadeReferencia(UsuarioPerfilEntidadeReferenciaTO $uperTO)
    {
        $vwUsuarioPerfilEntidadeReferenciaORM = new UsuarioPerfilEntidadeReferenciaORM();
        try {
            $obj = $vwUsuarioPerfilEntidadeReferenciaORM->fetchAll($vwUsuarioPerfilEntidadeReferenciaORM->montarWhere($uperTO));
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna o usuario com perfil na entidade
     * @param UsuarioPerfilEntidadeTO $upeTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarUsuarioPerfilEntidade(UsuarioPerfilEntidadeTO $upeTO)
    {
        $vwUsuarioPerfilEntidadeORM = new UsuarioPerfilEntidadeORM();
        try {
            $obj = $vwUsuarioPerfilEntidadeORM->fetchAll($vwUsuarioPerfilEntidadeORM->montarWhere($upeTO));
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que retorna o Perfil do Usuario na Entidade
     * @param UsuarioPerfilEntidadeTO $upeTO
     * @return object Zend_Db_Table
     */
    public function retornaUsuarioPerfilEntidade(UsuarioPerfilEntidadeTO $upeTO)
    {
        $usuarioPerfilEntidadeORM = new UsuarioPerfilEntidadeORM();
        try {
            $obj = $usuarioPerfilEntidadeORM->fetchAll($usuarioPerfilEntidadeORM->montarWhere($upeTO));
            return $obj;
        } catch (Zend_Exception $e) {
            THROW new Zend_Exception($e->getMessage());
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna pessoas para o autocomplete que podem ser vinculadas à algum perfil
     */
    public function retornaPessoaDisponivelPerfil(VwUsuarioPerfilEntidadeTO $to)
    {
        $pessoaORM = new VwPesquisarPessoaORM();
        try {
            return $pessoaORM->fetchAll("st_nomecompleto like '%{$to->getSt_nomecompleto()}%' AND bl_ativo = 1");
        } catch (Zend_Exception $e) {
            return false;
        }
    }

    /**
     * Método que retorna as funcionalidades
     * @param FuncionalidadeTO $fTO
     * @param Mixed $where
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function retornaFuncionalidade(FuncionalidadeTO $fTO, $where = null)
    {
        try {
            $funcionalidadeORM = new FuncionalidadeORM();
            if (!$where) {
                $where = $funcionalidadeORM->montarWhere($fTO, false);
            }
            $obj = $funcionalidadeORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Retorna o(s) perfil(is) da pessoa de acordo com o usuário e a entidade
     * @param UsuarioTO $uTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     * @todo Talvez não seja necessário o UsuarioTO, por sessão
     */
    public function retornaUsuarioPerfil(UsuarioTO $uTO)
    {
        $vwUsuarioPerfilEntidadeORM = new VwUsuarioPerfilEntidadeORM();
        $where = 'id_usuario = ' . $uTO->getId_usuario() . ' AND id_entidade = ' . $uTO->getSessao()->id_entidade;

        try {
            $obj = $vwUsuarioPerfilEntidadeORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }


    /**
     * Retorna as funcionalidades vinculados ao perifl
     * @param PerfilFuncionalidadeTO $perfilFuncionalidadeTO
     */
    public function retornarPerfilFuncionalidade(PerfilFuncionalidadeTO $perfilFuncionalidadeTO)
    {
        $perfilFuncionalidadeORM = new PerfilFuncionalidadeORM();
        return $perfilFuncionalidadeORM->fetchAll($perfilFuncionalidadeORM->montarWhere($perfilFuncionalidadeTO, false));
    }

    /**
     * Metodo que retorna as funcionalidades da entidade
     * @param VwEntidadeFuncionalidadeTO $vwEfTO
     * @param string $where
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornaVwEntidadeFuncionalidade(VwEntidadeFuncionalidadeTO $vwEfTO, $where = null)
    {
        $vwEntidadeFuncionalidadeORM = new VwEntidadeFuncionalidadeORM();
        if (!$where) {
            $where = $vwEntidadeFuncionalidadeORM->montarWhereView($vwEfTO, false, true, false);
        }
        try {
            $obj = $vwEntidadeFuncionalidadeORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            THROW new Zend_Exception($e->getMessage());
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Retorna as funcionalidades do perfil
     * @param PerfilFuncionalidadeTO $pfTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornaFuncionalidadePerfil(PerfilFuncionalidadeTO $pfTO)
    {
        $perfilFuncionalidadeORM = new PerfilFuncionalidadeORM();
        $where = $perfilFuncionalidadeORM->montarWhere($pfTO);
        try {
            $obj = $perfilFuncionalidadeORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que retorna as permissões de um determinado perfil
     * @param PerfilTO $pTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornaPermissoesPerfil(PerfilTO $pTO)
    {
        try {
            $vw = new VwPerfilPermissoesFuncionalidadeORM();
            $obj = $vw->fetchAll("id_perfil = {$pTO->getId_perfil()}");
            return $obj;
        } catch (Zend_Exception $e) {
            THROW new Zend_Exception($e->getMessage());
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Retorna os perfis pedagógicos da tabela tb_perfilpedagogico
     * @param PerfilPedagogicoTO $perfilPedagogicoTO
     * @return Zend_Db_Table_Rowset
     */
    public function retornarPerfilPedagogico(PerfilPedagogicoTO $perfilPedagogicoTO)
    {
        try {
            $perfilPedaogicoORM = new PerfilPedagogicoORM();
            $where = $perfilPedaogicoORM->montarWherePesquisa($perfilPedagogicoTO, false, true);
            return $perfilPedaogicoORM->fetchAll($where);
        } catch (Zend_Exception $e) {
            THROW new Zend_Exception($e->getMessage());
            return false;
        }

    }

    /**
     * Método que retorna perfis pedagógicos disponíveis para a entidade.
     * @param PerfilTO $perfilTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarPerfilPedagogicoDisponivel(PerfilTO $perfilTO)
    {
        $perfilPedagogicoORM = new PerfilPedagogicoORM();
        $perfilORM = new PerfilORM();
        $where = 'id_perfilpedagogico NOT IN ( ';
        $select = $perfilORM->select()
            ->from(array('pp' => 'tb_perfil'), array('id_perfilpedagogico'))
            ->setIntegrityCheck(false)
            ->where('pp.id_entidade = ' . $perfilTO->getId_entidade() . ' and pp.id_perfilpedagogico IS NOT NULL');

        if ($perfilTO->id_perfilpedagogico) {
            $select->where("pp.id_perfilpedagogico != " . $perfilTO->id_perfilpedagogico);
        }
        $where .= $select . ' )';

        try {
            return $perfilPedagogicoORM->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna os dados da view de usuario perfis
     * @param VwUsuarioPerfisTO $to
     * @param Mixed $where
     * @return Zend_Db_Table_Rowset
     */
    public function retornarVwUsuarioPerfis(VwUsuarioPerfisTO $to, $where = null)
    {
        $orm = new VwUsuarioPerfisORM();
        return $orm->fetchAll($where ? $where : $orm->montarWhereView($to, false, true));
    }

    /**
     * Metodo que exclui(Logicamente) um perfil
     * @param PerfilTO $pTO
     * @param String $nomeExibicao
     * @return Ead1_Mensageiro
     */
    public function excluirPerfil(PerfilTO $pTO, $nomeExibicao = null)
    {

        $this->editarPerfil($pTO, $nomeExibicao);
    }


}
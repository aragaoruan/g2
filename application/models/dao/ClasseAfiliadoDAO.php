<?php

/**
 * Classe de persistencia do produto com o banco de dados
 * @author Denise Xavier - denisexavier@ead1.com.br
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 * @package models
 * @subpackage dao
 */
class ClasseAfiliadoDAO extends Ead1_DAO {

	/**
	 * Método que insere no banco uma classe de afiliado
	 * @param ClasseAfiliadoTO $to
	 * @return Ambigous <mixed, multitype:>|boolean
	 */
	public function cadastrarClasseAfiliado(ClasseAfiliadoTO $to){
		$orm = new ClasseAfiliadoORM();
		$insert = $to->toArrayInsert();
		$this->beginTransaction();
		try {
			$id_classeafiliado = $orm->insert($insert);
			$this->commit();
			return $id_classeafiliado;
		} catch (Exception $e) {
			$this->rollBack();
			$arr ['TO'] = $to;
			$arr ['WHERE'] = $insert;
			$arr ['SQL'] = $e->getMessage();
			$this->excecao = $arr;
			return false;
		}
	}
	
	/**
	 * Método que insere no banco um afiliado
	 * @param ContratoAfiliadoTO $to
	 * @return Ambigous <mixed, multitype:>|boolean
	 */
	public function cadastrarContratoAfiliado(ContratoAfiliadoTO $to){
		$orm = new ContratoAfiliadoORM();
		$insert = $to->toArrayInsert();
		$this->beginTransaction();
		try {
			$id_contratoafiliado = $orm->insert($insert);
			$this->commit();
			return $id_contratoafiliado;
		} catch (Exception $e) {
			$this->rollBack();
			$arr ['TO'] = $to;
			$arr ['WHERE'] = $insert;
			$arr ['SQL'] = $e->getMessage();
			$this->excecao = $arr;
			return false;
		}
	}
	
	/**
	 * Método que insere no banco um produto para um afiliado
	 * @param AfiliadoProdutoTO $to
	 * @return Ambigous <mixed, multitype:>|boolean
	 */
	public function cadastrarAfiliadoProduto(AfiliadoProdutoTO $to){
		$orm = new AfiliadoProdutoORM();
		$insert = $to->toArrayInsert();
		$this->beginTransaction();
		try {
			$id_afiliadoproduto = $orm->insert($insert);
			$this->commit();
			return $id_afiliadoproduto;
		} catch (Exception $e) {
			$this->rollBack();
			$arr ['TO'] = $to;
			$arr ['WHERE'] = $insert;
			$arr ['SQL'] = $e->getMessage();
			$this->excecao = $arr;
			throw $e;
			return false;
		}
	}
	
	/**
	 * Método que insere no banco um comissionamento para um produto de afiliado
	 * @param AfiliadoProdutoComissionamentoTO $to
	 * @return Ambigous <mixed, multitype:>|boolean
	 */
	public function cadastrarAfiliadoProdutoComissionamento(AfiliadoProdutoComissionamentoTO $to){
		$orm = new AfiliadoProdutoComissionamentoORM();
		$insert = $to->toArrayInsert();
		$this->beginTransaction();
		try {
			$id_afiliadoprodutocomissionamento = $orm->insert($insert);
			$this->commit();
			return $id_afiliadoprodutocomissionamento;
		} catch (Exception $e) {
			$this->rollBack();
			$arr ['TO'] = $to;
			$arr ['WHERE'] = $insert;
			$arr ['SQL'] = $e->getMessage();
			$this->excecao = $arr;
			return false;
		}
	}
	
	/**
	 * Método que atualiza no banco uma classe de afiliado
	 * @param ClasseAfiliadoTO $to
	 * @return boolean
	 */
	public function editarClasseAfiliado(ClasseAfiliadoTO $to) {
		$orm = new ClasseAfiliadoORM();
		unset($to->arrDados);
		$update = $to->toArrayUpdate(false, array('id_classeafiliado'));
		$this->beginTransaction();
		try {
			$orm->update($update, " id_classeafiliado = " . $to->getId_classeafiliado() . " ");
			$this->commit();
			return true;
		} catch(Exception $e) {
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Método que atualiza no banco um afiliado
	 * @param ContratoAfiliadoTO $to
	 * @return boolean
	 */
	public function editarContratoAfiliado(ContratoAfiliadoTO $to) {
		$orm = new ContratoAfiliadoORM();
		$update = $to->toArrayUpdate(false, array('id_contratoafiliado'));
		$this->beginTransaction();
		try {
			$orm->update($update, " id_contratoafiliado = " . $to->getId_contratoafiliado() . " ");
			$this->commit();
			return true;
		} catch(Exception $e) {
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Método que atualiza no banco vínculo de afiliado com produto
	 * @param AfiliadoProdutoTO $to
	 * @return boolean
	 */
	public function editarAfiliadoProduto(AfiliadoProdutoTO $to, $where = null) {
		$orm = new AfiliadoProdutoORM();
		$update = $to->toArrayUpdate(false, array('id_afiliadoproduto'));
		if(!$where){
			$where = " id_afiliadoproduto = " . $to->getId_afiliadoproduto() . " ";
		}
		$this->beginTransaction();
		try {
			$orm->update($update, $where);
			$this->commit();
			return true;
		} catch(Exception $e) {
			$this->rollBack();
			$this->excecao = $e->getMessage();
			throw $e;
			return false;
		}
	}
	
	/**
	 * Deleta afiliado
	 * @param ContratoAfiliadoTO $to
	 * @return boolean
	 */
	public function deletarContratoAfiliado(ContratoAfiliadoTO $to){
		$orm = new ContratoAfiliadoORM();
		$this->beginTransaction();
		try{
			$orm->delete('id_contratoafiliado = '.$to->getId_contratoafiliado());
			$this->commit();
			return true;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Deleta produto de afiliado
	 * @param AfiliadoProdutoTO $to
	 * @return boolean
	 */
	public function deletarAfiliadoProduto(AfiliadoProdutoTO $to){
		$orm = new AfiliadoProdutoORM();
		$this->beginTransaction();
		try{
			$orm->delete('id_afiliadoproduto = '.$to->getId_produtoafiliado());
			$this->commit();
			return true;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Deleta comissionamento de produto de afiliado
	 * @param AfiliadoProdutoComissionamentoTO $to
	 * @return boolean
	 */
	public function deletarAfiliadoProdutoComissionamento(AfiliadoProdutoComissionamentoTO $to){
		$orm = new AfiliadoProdutoComissionamentoORM();
		$this->beginTransaction();
		try{
			$orm->delete('id_afiliadoproduto = '.$to->getId_produtoafiliado());
			$this->commit();
			return true;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * @param ClasseAfiliadoComissionamentoTO $to
	 * @throws Zend_Exception
	 */
	public function desvincularComissionamentosClasseAfiliado(ClasseAfiliadoComissionamentoTO $to){
		$this->beginTransaction();
		try{
			$orm = new ClasseAfiliadoComissionamentoORM();
			$where = $orm->montarWhere($to);
			$orm->delete($where);
			//$orm->delete("id_classeafiliado = {$to->getId_classeafiliado()}");
			$this->commit();
		}catch(Zend_Exception $e){
			$this->rollBack();
			throw $e;
		}
	}
	
	/**
	 * @param ClasseAfiliadoTO $to
	 * @param unknown_type $where
	 * @throws Zend_Exception
	 * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
	 */
	public function retornarClasseAfiliado(ClasseAfiliadoTO $to, $where = null){
		$orm = new ClasseAfiliadoORM();
		if (! $where) {
			$where = $orm->montarWhere($to);
// 			if(is_array($where)){
// 				$where[] = " AND bl_ativo = 1 ";
// 			} else {
// 				$where .= " AND bl_ativo = 1 ";
// 			}
		}
		try {
			$obj = $orm->fetchAll($where);
			return $obj;
		} catch ( Zend_Exception $e ) {
			$msg ['WHERE'] = $where;
			$msg ['SQL'] = $e->getMessage ();
			$msg ['TO'] = $to;
			$this->excecao = $msg;
			THROW new Zend_Exception ( $e->getMessage () );
			return false;
		}
	}
	
	/**
	 * @param ContratoAfiliadoTO $to
	 * @param string $where
	 * @throws Zend_Exception
	 * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
	 */
	public function retornarContratoAfiliado(ContratoAfiliadoTO $to, $where = null){
		$orm = new ContratoAfiliadoORM();
		if (! $where) {
			$where = $orm->montarWhere($to);
		}
		try {
			$obj = $orm->fetchAll($where);
			return $obj;
		} catch ( Zend_Exception $e ) {
			$msg ['WHERE'] = $where;
			$msg ['SQL'] = $e->getMessage ();
			$msg ['TO'] = $to;
			$this->excecao = $msg;
			THROW new Zend_Exception ( $e->getMessage () );
			return false;
		}
	}
	
	/**
	 * @param AfiliadoProdutoTO $to
	 * @param string $where
	 * @throws Zend_Exception
	 * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
	 */
	public function retornarAfiliadoProduto(AfiliadoProdutoTO $to, $where = null){
		$orm = new AfiliadoProdutoORM();
		if (! $where) {
			$where = $orm->montarWhere($to);
		}
		try {
			$obj = $orm->fetchAll($where);
			return $obj;
		} catch ( Zend_Exception $e ) {
			$msg ['WHERE'] = $where;
			$msg ['SQL'] = $e->getMessage ();
			$msg ['TO'] = $to;
			$this->excecao = $msg;
			THROW new Zend_Exception ( $e->getMessage () );
			return false;
		}
	}
	
	/**
	 * Retorna Categoria com Produto e Comissão de um Afiliado
	 * @param AfiliadoProdutoTO $to
	 * @throws Zend_Exception
	 * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
	 */
	public function retornarAfiliadoCategoriaProdutoComissionamento(AfiliadoProdutoTO $to){
		$orm = new AfiliadoProdutoORM();
		$select = $orm->select()
					->setIntegrityCheck(false)
					->from(array('apcc' => 'tb_afiliadoproduto'),'*')
					->join(array('co' => 'tb_comissionamento'), 'co.id_comissionamento = apcc.id_comissionamento', array('co.nu_comissao'))
					->join(array('ca' => 'tb_categoria'), 'ca.id_categoria = apcc.id_categoria', array('ca.st_categoria'))
					->join(array('pr' => 'tb_produto'), 'pr.id_produto = apcc.id_produto', array('pr.st_produto'))
					->where('id_contratoafiliado = '.$to->getId_contratoafiliado().' and apcc.bl_ativo = 1');
		try {
			$obj = $orm->fetchAll($select);
			return $obj;
		} catch ( Zend_Exception $e ) {
			$msg ['SQL'] = $e->getMessage ();
			$msg ['TO'] = $to;
			$this->excecao = $msg;
			THROW new Zend_Exception ( $e->getMessage () );
			return false;
		}
	}
	
	/**
	 * Método que insere um comissionamento no banco de dados
	 * @param ComissionamentoTO $to
	 * @return Ambigous <mixed, multitype:>|boolean
	 */
	public function cadastrarComissionamento(ComissionamentoTO $to){
		$orm = new ComissionamentoORM();
		$insert = $to->toArrayInsert();
		$this->beginTransaction();
		try {
			$id_comissionamento = $orm->insert($insert);
			$this->commit();
			return $id_comissionamento;
		} catch (Exception $e) {
			$this->rollBack();
			$arr ['TO'] = $to;
			$arr ['WHERE'] = $insert;
			$arr ['SQL'] = $e->getMessage();
			$this->excecao = $arr;
			throw $e;
			return false;
		}
	}
	
	/**
	 * Método que atualiza um comissionamento no banco de dados
	 * @param ComissionamentoTO $to
	 * @return boolean
	 */
	public function editarComissionamento(ComissionamentoTO $to) {
		$orm = new ComissionamentoORM();
		unset($to->arrDados);
		$update = $to->toArrayUpdate(false, array('id_comissionamento'));
		$this->beginTransaction();
		try {
			$orm->update($update, " id_comissionamento = " . $to->getId_comissionamento() . " ");
			$this->commit();
			return true;
		} catch(Exception $e) {
			$this->rollBack();
			$this->excecao = $e->getMessage();
			throw $e;
			return false;
		}
	}
	
	/**
	 * Método que insere no banco um vínculo entre Meio de Pagamento e Comissionamento
	 * @param ComissionamentoMeioTO $to
	 * @return Ambigous <mixed, multitype:>|boolean
	 */
	public function cadastrarMeioPagamentoComissionamento(ComissionamentoMeioTO $to){
		$orm = new ComissionamentoMeioORM();
		$insert = $to->toArrayInsert();
		$this->beginTransaction();
		try {
			$id_comissionamento = $orm->insert($insert);
			$this->commit();
			return $id_comissionamento;
		} catch ( Exception $e ) {
			$this->rollBack ();
			$arr ['TO'] = $to;
			$arr ['WHERE'] = $insert;
			$arr ['SQL'] = $e->getMessage ();
			$this->excecao = $arr;
			return false;
		}
	}
	
	/**
	 * Método que deleta os vínculos entre meios de pagamento e comissionamento
	 * @see ClasseAfiliadoBO::salvarComissionamento();
	 * @param ComissionamentoTO $comissionamento
	 * @throws Zend_Exception
	 */
	public function desvincularMeiosPagamentoComissionamento(ComissionamentoTO $comissionamento){
		$this->beginTransaction();
		try{
			$orm = new ComissionamentoMeioORM();
			$orm->delete("id_comissionamento = {$comissionamento->getId_comissionamento()}");
			$this->commit();
		}catch(Zend_Exception $e){
			$this->rollBack();
			throw $e;
		}
	}
	
	/**
	 * Método que faz uma busca no banco e retorna Zend_DB_Table com os dados cos comissionamentos
	 * @param ComissionamentoTO $to
	 * @param unknown_type $where
	 * @throws Zend_Exception
	 * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
	 */
	public function retornarComissionamento(ComissionamentoTO $to, $where = null){
		$orm = new ComissionamentoORM();
		if (! $where) {
			$where = $orm->montarWhere($to);
			if(is_array($where)){
				$where[] = " AND bl_ativo = 1 ";
			} else {
				$where .= " AND bl_ativo = 1 ";
			}
		}
		try {
			$obj = $orm->fetchAll($where);
			return $obj;
		} catch ( Zend_Exception $e ) {
			$msg ['WHERE'] = $where;
			$msg ['SQL'] = $e->getMessage ();
			$msg ['TO'] = $to;
			$this->excecao = $msg;
			THROW new Zend_Exception ( $e->getMessage () );
			return false;
		}
	}
	
	/**
	 * Método que Retorna todos os comissionamentos de uma classe de afiliados
	 * @param ClasseAfiliadoTO $caTO
	 * @param unknown_type $where
	 * @throws Zend_Exception
	 * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
	 */
	public function retornarComissionamentosClasseAfiliados(ClasseAfiliadoComissionamentoTO $caTO, $where = null){
		$orm = new ClasseAfiliadoComissionamentoORM();
		if (! $where) {
			$where = $orm->montarWhere($caTO);
			if(is_array($where)){
				$where[] = " AND id_classeafiliado = {$caTO->getId_classeafiliado()} ";
			} else {
				$where .= " AND id_classeafiliado = {$caTO->getId_classeafiliado()} ";
			}
		}
		try {
			$obj = $orm->fetchAll($where);
			return $obj;
		} catch ( Zend_Exception $e ) {
			$msg ['WHERE'] = $where;
			$msg ['SQL'] = $e->getMessage ();
			$msg ['TO'] = $caTO;
			$this->excecao = $msg;
			THROW new Zend_Exception ( $e->getMessage () );
			return false;
		}
	}
	
	/**
	 * @param ComissionamentoTO $comissionamentoMeioTO
	 * @param mixed $where
	 * @throws Zend_Exception
	 * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
	 */
	public function retornarMeiosPagamentoComissionamentos(ComissionamentoMeioTO $comissionamentoMeioTO, $where = null){
		$orm = new ComissionamentoMeioORM();
		if (! $where) {
			$where = $orm->montarWhere($comissionamentoMeioTO);
			if(is_array($where)){
				$where[] = " AND id_comissionamento = {$comissionamentoMeioTO->getId_comissionamento()} ";
			} else {
				$where .= " AND id_comissionamento = {$comissionamentoMeioTO->getId_comissionamento()} ";
			}
		}
		try {
			$obj = $orm->fetchAll($where);
			return $obj;
		} catch ( Zend_Exception $e ) {
			$msg ['WHERE'] = $where;
			$msg ['SQL'] = $e->getMessage ();
			$msg ['TO'] = $comissionamentoMeioTO;
			$this->excecao = $msg;
			THROW new Zend_Exception ( $e->getMessage () );
			return false;
		}
	}
	
	/**
	 * @param TipoPessoaTO $to
	 * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >
	 * @see ClasseAfiliadoBO::retornaTipoPessoa
	 */
	public function retornaTipoPessoa(TipoPessoaTO $to){
		$orm = new TipoPessoaORM();
		return $orm->fetchAll();
	}
	
	/**
	 * @param ClasseAfiliadoComissionamentoTO $to
	 * @return Ambigous <mixed, multitype:>|boolean
	 */
	public function vincularClasseAfiliadoComissionamento(ClasseAfiliadoComissionamentoTO $to){
		$orm = new ClasseAfiliadoComissionamentoORM();
		$insert = $to->toArrayInsert();
		$this->beginTransaction();
		try {
			$retorno = $orm->insert($insert);
			$this->commit();
			return $retorno;
		} catch ( Exception $e ) {
			$this->rollBack ();
			$arr ['TO'] = $to;
			$arr ['WHERE'] = $insert;
			$arr ['SQL'] = $e->getMessage ();
			$this->excecao = $arr;
			return false;
		}
	}
}

?>
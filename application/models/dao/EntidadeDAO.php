<?php

/**
 * Classe de persistencia da instituição (entidade) com o banco de dados
 * @author Dimas Sulz <dimassulz@gmail.com>
 *
 * @package models
 * @subpackage dao
 */
class EntidadeDAO extends Ead1_DAO
{

    /**
     * Método que retorna as funcionalidades da entidade
     * @param EntidadeFuncionalidadeTO $efTO
     * @param Mixed $where
     * @throws Zend_Exception
     * @see EntidadeBO::salvarArrEntidadeFuncionalidade() | EntidadeBO::retornarEntidadeFuncionalidade()
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function retornarEntidadeFuncionalidade(EntidadeFuncionalidadeTO $efTO, $where = null)
    {
        try {
            $orm = new EntidadeFuncionalidadeORM();
            if (!$where) {
                $where = $orm->montarWhere($efTO);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que retorna os dados da entidade (instituição)
     * @param EntidadeTO $eTO
     * @param Mixed $where
     * @see EntidadeBO::retornaEntidade()
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function retornaEntidade(EntidadeTO $eTO, $where = null)
    {
        $entidadeORM = new EntidadeORM();
        if (!$where) {
            $where = $entidadeORM->montarWherePesquisa($eTO, false, true);
        }
        return $entidadeORM->fetchAll($where);
    }

    /**
     * Método que retorna a(s) classe(s) que pertence ao uma entidade
     * @param EntidadeRelacaoTO $erTO
     * @see EntidadeBO::retornaClasseDaEntidade()
     * @return object|false Zend_Db_Table_Rowset_Abstract
     */
    public function retornaClasseEntidade(EntidadeRelacaoTO $erTO)
    {
        $vwEntidadeClasse = new VwEntidadeClasseORM();
        $where = $vwEntidadeClasse->montarWhereView($erTO, false, true);
        try {
            $obj = $vwEntidadeClasse->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna as classes de entidade
     * @param EntidadeClasseTO $ecTO
     * @see EntidadeBO::retornaClasses()
     * @return object|false Zend_Db_Table_Rowset_Abstract
     */
    public function retornaClasses(EntidadeClasseTO $ecTO)
    {
        $EntidadeClasse = new EntidadeClasseORM();
        $where = $EntidadeClasse->montarWhere($ecTO);
        try {
            $obj = $EntidadeClasse->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna endereços da entidade (instituição)
     * @param EntidadeTO $eTO
     * @see EntidadeBO::retornaEnderecoEntidade()
     * @return object|false Zend_Db_Table_Rowset_Abstract
     */
    public function retornaEnderecoEntidade(EntidadeTO $eTO)
    {
        $vwEntidadeEndereco = new VwEntidadeEnderecoORM();
        $vwEntidadeEnderecoTO = Ead1_TO_Dinamico::mergeTO(new VwEntidadeEnderecoTO(), $eTO);
        $where = $vwEntidadeEndereco->montarWhereView($vwEntidadeEnderecoTO, false, true);
        try {
            $obj = $vwEntidadeEndereco->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna telefone da entidade (instituição)
     * @param EntidadeTO $eTO
     * @see EntidadeBO::retornaTelefonesEntidade()
     * @return object|false Zend_Db_Table_Rowset_Abstract
     */
    public function retornaTelefoneEntidade(EntidadeTO $eTO)
    {
        $vwEntidadeTelefone = new VwEntidadeTelefoneORM();
        $vwEntidadeTelefoneTO = Ead1_TO_Dinamico::mergeTO(new VwEntidadeTelefoneTO(), $eTO);
        $where = $vwEntidadeTelefone->montarWhere($vwEntidadeTelefoneTO);
        try {
            $obj = $vwEntidadeTelefone->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna telefone da entidade (instituição)
     * @param VwEntidadeTelefoneTO $vwetTO
     * @see EntidadeBO::retornaVwEntidadeTelefone()
     * @return object|false Zend_Db_Table_Rowset_Abstract
     */
    public function retornarVwEntidadeTelefone(VwEntidadeTelefoneTO $vwetTO)
    {
        $vwEntidadeTelefone = new VwEntidadeTelefoneORM();
        $where = $vwEntidadeTelefone->montarWhereView($vwetTO, false, true);
        try {
            $obj = $vwEntidadeTelefone->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna informação de contato da entidade (instituição)
     * @param VwEntidadeContatoTO $vwecTO
     * @see EntidadeBO::retornaContatosEntidade()
     * @return object|false Zend_Db_Table_Rowset_Abstract
     */
    public function retornaContatoEntidade(VwEntidadeContatoTO $vwecTO)
    {
        $vwEntidadeContato = new VwEntidadeContatoORM();
        $where = $vwEntidadeContato->montarWhereView($vwecTO, false, true);
        try {
            $obj = $vwEntidadeContato->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna informação de telefones de contato da entidade (instituição)
     * @param ContatoEntidadeTO $ceTO obrigado a ter o id_contatoentidade setado
     * @see EntidadeBO::retornaTelefonesContatoEntidade()
     * @return object|false Zend_Db_Table_Rowset_Abstract
     * @todo retirado o método de entidadeTO
     */
    public function retornaTelefoneContatoEntidade(ContatoEntidadeTO $ceTO)
    {
        $vwEntidadeContatoTelefone = new VwEntidadeContatoTelefoneORM();
        $where = $vwEntidadeContatoTelefone->montarWhere($ceTO);
        try {
            $obj = $vwEntidadeContatoTelefone->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna relação de projeto pedagógico com entidade.
     * @param ProjetoEntidadeTO $peTO
     * @see EntidadeBO::retornaProjetoEntidade()
     * @return object|false Zend_Db_Table_Rowset_Abstract
     */
    public function retornarProjetoEntidade(ProjetoEntidadeTO $peTO)
    {
        $projetoEntidadeORM = new ProjetoEntidadeORM();
        $where = $projetoEntidadeORM->montarWhere($peTO);
        try {
            $obj = $projetoEntidadeORM->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que retorna conta(s) da entidade (instituição)
     * @param EntidadeTO $eTO id_entidade setado
     * @see EntidadeBO::retornaContaEntidade()
     * @return object|false Zend_Db_Table_Rowset_Abstract
     */
    public function retornaContaEntidade(EntidadeTO $eTO)
    {
        $orm = new VwEntidadeContaORM();
        $where = ($eTO->getId_entidade() ? "id_entidade = " . $eTO->getId_entidade() : null);
        try {
            $obj = $orm->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que cadastra a funcionalidade da entidade
     * @param EntidadeFuncionalidadeTO $efTO
     * @return Boolean
     */
    public function cadastrarEntidadeFuncionalidade(EntidadeFuncionalidadeTO $efTO)
    {
        try {
            $this->beginTransaction();
            $orm = new EntidadeFuncionalidadeORM();
            $insert = $efTO->toArrayInsert();
            $orm->insert($insert);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que cadastra dados da entidade(instituição)
     * @param EntidadeTO $eTO contém todos os dados referente para fazer o cadastro
     * @return int|false id_entidade caso consiga fazer a inserção ou false caso não consiga
     */
    public function cadastrarEntidade(EntidadeTO $eTO)
    {
        $this->beginTransaction();
        try {
            $entidadeORM = new EntidadeORM();
            $insert = $eTO->toArrayInsert();
            $id_entidade = $entidadeORM->insert($insert);
            $this->commit();
            return $id_entidade;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
        }
    }

    /**
     * Método que cadastra o email da instituição
     * @param EntidadeEmailTO $eeTO
     * @see EntidadeBO::cadastrarEntidade();
     * @throws Zend_Exception
     * @return int $id_entidadeemail | false
     */
    public function cadastrarEntidadeEmail(EntidadeEmailTO $eeTO)
    {
        $this->beginTransaction();
        try {
            $orm = new EntidadeEmailORM();
            $insert = $eeTO->toArrayInsert();
            $id_entidadeemail = $orm->insert($insert);
            $this->commit();
            return $id_entidadeemail;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que Cria o Relacionamento Entre as Entidades (Pai/Filho)
     * @param EntidadeRelacaoTO $erTO
     * @return boolean
     */
    public function cadastrarEntidadeRelacao(EntidadeRelacaoTO $erTO)
    {
        $entidadeRelacaoORM = new EntidadeRelacaoORM();
        $this->beginTransaction();
        try {
            $entidadeRelacaoORM->insert($erTO->toArrayInsert());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que cadastra projetos pedagógicos na entidade.
     * @param ProjetoEntidadeTO $peTO
     * @return boolean
     */
    public function cadastrarProjetoEntidade(ProjetoEntidadeTO $peTO)
    {
        $projetoEntidadeORM = new ProjetoEntidadeORM();
        $this->beginTransaction();
        try {
            $projetoEntidadeORM->insert($peTO->toArrayInsert());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que cadastra dados de endereço da entidade(instituição)
     * @param EnderecoTO $endTO contém todos os dados referente a endereço
     * @return int|false id_endereco caso consiga fazer a inserção ou false caso não consiga
     */
    public function cadastrarEndereco(EnderecoTO $endTO)
    {
        $enderecoORM = new EnderecoORM();
        $this->beginTransaction();
        try {
            $id_endereco = $enderecoORM->insert($endTO->toArrayInsert());
            $this->commit();
            return $id_endereco;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo Que Vincula o Endereço a Entidade
     * @param EntidadeEnderecoTO $eeTO
     * @return int $id_entidadeendereco | boolean false
     */
    public function cadastrarEntidadeEndereco(EntidadeEnderecoTO $eeTO)
    {
        $entidadeEnderecoORM = new EntidadeEnderecoORM();
        $this->beginTransaction();
        try {
            $id_entidadeendereco = $entidadeEnderecoORM->insert($eeTO->toArrayInsert());
            $this->commit();
            return $id_entidadeendereco;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que Cadastra o Tipo de Endereço da Entidade
     * @param TipoEnderecoEntidadeTO $teeTO
     * @return boolean
     */
    public function cadastrarEntidadeTipoEndereco(TipoEnderecoEntidadeTO $teeTO)
    {
        $tipoEnderecoEntidadeORM = new TipoEnderecoEntidadeORM();
        $this->beginTransaction();
        try {
            $tipoEnderecoEntidadeORM->insert($teeTO->toArrayInsert());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que cadastra dados de telefone da entidade(instituição)
     * @param EntidadeTO $eTO contém todos os dados referente para fazer o cadastro
     * @param ContatosTelefoneTO $ctTO contém todos os dados referente a telefone
     * @param ContatosTelefoneEntidadeTO $cteTO contém todos os dados referente a tabela de relacionamento de telefone entidade
     * @return int|false id_telefone caso consiga fazer a inserção ou false caso não consiga
     */
    public function cadastrarTelefoneEntidade(EntidadeTO $eTO, ContatosTelefoneTO $ctTO, ContatosTelefoneEntidadeTO $cteTO)
    {
        $contatosTelefoneORM = new ContatosTelefoneORM();
        $contatosTelefoneEntidadeORM = new ContatosTelefoneEntidadeORM();
        $idTelefone = $contatosTelefoneORM->insert($ctTO->toArrayInsert());
        $ctTO->setId_telefone($idTelefone);
        $cteTO->setId_entidade($eTO->getId_entidade());
        $cteTO->setId_telefone($ctTO->getId_telefone());
        $contatosTelefoneEntidadeORM->insert($cteTO->toArrayInsert());
        return $idTelefone;
    }

    /**
     * Método que cadastra dados de contato(s) da entidade(instituição)
     * @param ContatoEntidadeTO $ceTO contém todos os dados referente aos contatos da entidade
     * @return int|false id_contato caso consiga fazer a inserção ou false caso não consiga
     */
    public function cadastrarContatoEntidade(ContatoEntidadeTO $ceTO)
    {
        $contatosEntidadeORM = new ContatoEntidadeORM();
        $insertContatos = $ceTO->toArrayInsert();
        $this->beginTransaction();
        try {
            $id_contato = $contatosEntidadeORM->insert($insertContatos);
            $this->commit();
            return $id_contato;
        } catch (Zend_Exception $e) {
            $this->rollBack();

            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que cadastra as contas da entidade(instituição)
     * @param ContaEntidadeTO $ceTO
     * @see EntidadeBO::cadastrarContaEntidade()
     * @throws Zend_Exception
     * @return int $id_contaentidade | boolean false
     */
    public function cadastrarContaEntidade(ContaEntidadeTO $ceTO)
    {
        try {
            $this->beginTransaction();
            $contaEntidadeORM = new ContaEntidadeORM();
            $insertConta = $ceTO->toArrayInsert();
            $id_contaentidade = $contaEntidadeORM->insert($insertConta);
            $this->commit();
            return $id_contaentidade;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
        }
    }

    /**
     * Método que edita dados da entidade(instituição)
     * @param EntidadeTO $entTO contém todos os dados referente a entidade para fazer a edição
     * @return boolean Ocorreu ou não a edição
     */
    public function editarEntidade(EntidadeTO $entTO)
    {
        try {
            $entidadeORM = new EntidadeORM();
            $updateEntidade = $entTO->toArrayUpdate(false, array('id_entidade', 'dt_cadastro'));
            $this->beginTransaction();

//            Zend_Debug::dump($updateEntidade);
//            die;
//            echo 'AQ';
//            Zend_Debug::dump($updateEntidade);
//            die;
            $entidadeORM->update($updateEntidade, " id_entidade = " . $entTO->getId_entidade());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
        }
    }

    /**
     * Metodo que edita o email da instituição
     * @param EntidadeEmailTO $eeTO
     * @param Mixed $where
     * @throws Zend_Exception
     * @see EntidadeBO::editarEntidadeEmail()
     * @return Boolean
     */
    public function editarEntidadeEmail(EntidadeEmailTO $eeTO, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new EntidadeEmailORM();
            if (!$where) {
                $where = $orm->montarWhere($eeTO, true);
            }
            $update = $eeTO->toArrayUpdate(false, array('id_entidadeemail', 'id_entidade'));
            $orm->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que edita um EnderecoEntidade
     * @param EntidadeEnderecoTO $to
     * @param Mixed $where
     */
    public function editarEntidadeEndereco(EntidadeEnderecoTO $to, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new EntidadeEnderecoORM();
            if (!$where) {
                $where = $orm->montarWhere($to);
            }
            $update = $to->toArrayUpdate(false, array('id_entidadeendereco', 'id_entidade', 'id_endereco'));
            $orm->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que edita dados de endereço da entidade(instituição)
     * @param EnderecoTO $endTO contém todos os dados referente a endereço
     * @param EntidadeEnderecoTO $eendTO contém todos os dados referente a tabela de relacionamento da entidade e do endereço
     * @see EntidadeBO::editarEnderecoEntidade()
     * @return boolean Ocorreu ou não a edição
     */
    public function editarEnderecoEntidade(EnderecoTO $endTO, EntidadeEnderecoTO $eendTO)
    {
        $enderecoORM = new EnderecoORM();
        $entidadeEnderecoORM = new EntidadeEnderecoORM();
        $updateEndereco = $endTO->toArrayUpdate(false, array('id_endereco'));
        $updateEntidadeEndereco = $eendTO->toArrayUpdate(false, array('id_endereco', 'id_entidade'));
        $this->beginTransaction();

        try {
            $enderecoORM->update($updateEndereco, " id_endereco = " . $endTO->getId_endereco());
            $entidadeEnderecoORM->update($updateEntidadeEndereco, "id_endereco = " . $endTO->getId_endereco() . " AND id_entidade = " . $eendTO->getId_entidade());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que edita dados de telefone da entidade(instituição)
     * @param ContatosTelefoneTO $ctTO contém todos os dados referente a telefone
     * @param ContatosTelefoneEntidadeTO $cteTO contém todos os dados referente a tabela de relacionamento da entidade e do telefone
     * @see EntidadeBO::editarTelefoneEntidade()
     * @return boolean Ocorreu ou não a edição
     */
    public function editarTelefoneEntidade(ContatosTelefoneTO $ctTO, ContatosTelefoneEntidadeTO $cteTO)
    {
        $contatosTelefoneORM = new ContatosTelefoneORM();
        $contatosTelefoneEntidadeORM = new ContatosTelefoneEntidadeORM();
        $updateTelefone = $ctTO->toArrayUpdate(false, array('id_telefone'));
        $updateTelefoneEntidade = $cteTO->toArrayUpdate(false, array('id_telefone', 'id_entidade'));
        $this->beginTransaction();
        try {
            $contatosTelefoneORM->update($updateTelefone, " id_telefone = " . $ctTO->getId_telefone());
            $contatosTelefoneEntidadeORM->update($updateTelefoneEntidade, "id_telefone = " . $cteTO->getId_telefone() . " AND id_entidade = " . $cteTO->getId_entidade());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que edita dados de telefone da entidade(instituição)
     * @param ContatosTelefoneTO $ctTO contém todos os dados referente a telefone
     * @see EntidadeBO::editarTelefoneEntidade()
     * @return boolean Ocorreu ou não a edição
     */
    public function editarContatoTelefone(ContatosTelefoneTO $ctTO)
    {
        $contatosTelefoneORM = new ContatosTelefoneORM();
        $this->beginTransaction();
        try {
            $contatosTelefoneORM->update($ctTO->toArrayUpdate(false, array('id_telefone')), 'id_telefone = ' . $ctTO->getId_telefone());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que edita dados de contato(s) da entidade(instituição)
     * @param ContatoEntidadeTO $ceTO contém todos os dados referente ao contato da entidade
     * @see EntidadeBO::editarContatoEntidade()
     * @return boolean Ocorreu ou não a edição
     */
    public function editarContatoEntidade(ContatoEntidadeTO $ceTO)
    {
        $contatoEntidadeORM = new ContatoEntidadeORM();
        $updateTelefone = $ceTO->toArrayUpdate(false, array('id_contatoentidade'));
        $this->beginTransaction();
        try {
            $contatoEntidadeORM->update($updateTelefone, " id_contatoentidade = " . $ceTO->getId_contatoentidade());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que edita dados de contato(s) de telefone da entidade(instituição)
     * @param ContatosTelefoneEntidadeTO $cteTO contém todos os dados referente ao contato de telefone da entidade
     * @see EntidadeBO::editarContatosTelefoneEntidade()
     * @return boolean Ocorreu ou não a edição
     */
    public function editarContatosTelefoneEntidade(ContatosTelefoneEntidadeTO $cteTO, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new ContatosTelefoneEntidadeORM();
            $update = $cteTO->toArrayUpdate();
            if (!$where) {
                $where = $orm->montarWhere($cteTO, true);
            }
            $orm->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($this->excecao);
            return false;
        }
    }

    /**
     * Método que edita o Endereco
     * @param EnderecoTO $eTO
     * @see EntidadeBO::editarContatoEntidade()
     * @return booelan
     */
    public function editarEndereco(EnderecoTO $eTO)
    {
        $this->beginTransaction();
        try {
            $orm = new EnderecoORM();
            $where = $orm->montarWhere($eTO, true);
            $update = $eTO->toArrayUpdate(false, array('id_endereco'));
            $orm->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que edita dados de telefone de um contato de uma instituição
     * @param ContatosTelefoneEntidadeContatoTO $ctecTO contém todos os dados referente ao contato de telefone dos contatos da entidade
     * @param ContatosTelefoneTO $ctTO contém todos os dados referente ao telefone
     * @see EntidadeBO::editarTelefoneContato()
     * @return boolean Ocorreu ou não a edição
     */
    public function editarTelefoneContato(ContatosTelefoneEntidadeContatoTO $ctecTO, ContatosTelefoneTO $ctTO)
    {
        $contatoTelefoneORM = new ContatosTelefoneORM();
        $contatoEntidadeORM = new ContatosTelefoneEntidadeContatoORM();
        $updateTelefone = $ctTO->toArrayUpdate(false, array('id_telefone'));
        $updateContatosTelefoneEntidade = $ctecTO->toArrayUpdate(false, array('id_telefone', 'id_contatoentidade'));
        $this->beginTransaction();
        try {
            $contatoTelefoneORM->update($updateTelefone, "id_telefone = " . $ctecTO->getId_contatoentidade());
            $contatoEntidadeORM->update($updateContatosTelefoneEntidade, "id_contatoentidade = " . $ctecTO->getId_contatoentidade() . " AND id_telefone = " . $ctecTO->getId_telefone());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Método que edita a(s) conta(s) da entidade(instituição)
     * @param ContaEntidadeTO $ceTO
     * @see EntidadeBO::editarContaEntidade()
     * @return boolean
     */
    public function editarContaEntidade(ContaEntidadeTO $ceTO, $where = null)
    {
        try {
            $contaEntidadeORM = new ContaEntidadeORM();
            $where = 'id_contaentidade = ' . $ceTO->getId_contaentidade();
            if (!$where) {
                $where = $contaEntidadeORM->montarWhere($ceTO);
            }
            $updateConta = $ceTO->toArrayUpdate(false, array('id_entidade', 'id_contaentidade'));
            $this->beginTransaction();
            $contaEntidadeORM->update($updateConta, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
        }
    }

    /**
     * Método que edita a relação da entidade(instituição)
     * @param EntidadeRelacaoTO $erTO
     * @see EntidadeBO::editarEntidadeRelacao()
     * @return boolean
     */
    public function editarEntidadeRelacao(EntidadeRelacaoTO $erTO, $where = null)
    {
        try {
            $entidadeRelacaoORM = new EntidadeRelacaoORM();
            if (!$where) {
                $where = $entidadeRelacaoORM->montarWhere($erTO, true);
            }
            $update = $erTO->toArrayUpdate(false, array('id_entidade', 'id_entidadepai'));
            $this->beginTransaction();
            $entidadeRelacaoORM->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Método que edita a funcionalidade da entidade
     * @param EntidadeFuncionalidadeTO $efTO
     * @param Mixed $where
     * @return Boolean
     */
    public function editarEntidadeFuncionalidade(EntidadeFuncionalidadeTO $efTO, $where)
    {
        try {
            $this->beginTransaction();
            $orm = new EntidadeFuncionalidadeORM();
            $update = $efTO->toArrayUpdate(false, array('id_entidade', 'id_funcionalidade'));
            if (!$where) {
                $where = $orm->montarWhere($efTO);
            }
            $orm->update($update, $where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Retorna todas as entidades filhas de uma determinada entidade
     * @param int $idEntidade
     * @return array
     */
    public function retornaEntidadesFilhas($idEntidadePai)
    {
        $statement = Zend_Db_Table::getDefaultAdapter()->query(' EXEC sp_entidaderecursivo ? ', array($idEntidadePai));
        return $statement->fetchAll();
    }

    /**
     * Metodo que retorna os emails da instituição
     * @param EntidadeEmailTO $eeTO
     * @param Mixed $where
     * @throws Zend_Exception
     * @see EntidadeBO::retornarEntidadeEmail();
     * @return Zend_Db_Table_Rowset_Abstract | false
     */
    public function retornarEntidadeEmail(EntidadeEmailTO $eeTO, $where = null)
    {
        try {
            $orm = new EntidadeEmailORM();
            if (!$where) {
                $where = $orm->montarWhere($eeTO);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * Metodo que retorna a view de endereço da entidade
     * @param VwEntidadeEnderecoTO $vwEeTO
     * @param Mixed $where
     * @return Zend_Db_Table_Rowset_Abstract | false
     */
    public function retornarVwEntidadeEndereco(VwEntidadeEnderecoTO $vwEeTO, $where = null)
    {
        try {
            $orm = new VwEntidadeEnderecoORM();
            if (!$where) {
                $where = $orm->montarWhereView($vwEeTO, false, true);
            }
            return $orm->fetchAll($where);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que retorna a view de entidade com suas filhas
     * @param VwEntidadeRecursivaTO $vwerTO
     * @param Mixed $where
     * @return Zend_Db_Table_Rowset_Abstract | false
     */
    public function retornarVwEntidadeRecursiva(VwEntidadeRecursivaTO $vwerTO, $where = null, $orderBy = null)
    {
        try {
            $orm = new VwEntidadeRecursivaORM();
            if (!$where) {
                $where = $orm->montarWhereView($vwerTO, false, true);
            }
            return $orm->fetchAll($where, $orderBy);
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que exclui telefone.
     * @param ContatosTelefoneTO $ctTO
     * @param Mixed $where
     * @return Zend_Db_Table_Rowset_Abstract | false
     */
    public function deletarContatoTelefone(ContatosTelefoneTO $ctTO)
    {
        $contatosTelefoneORM = new ContatosTelefoneORM();
        $this->beginTransaction();
        $where = $contatosTelefoneORM->montarWhere($ctTO);
        try {
            if (!$where) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $contatosTelefoneORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            Zend_Debug::dump($e->getMessage());
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que deleta a conta de uma entidade
     * @param ContaEntidadeTO $ceTO
     * @return Zend_Db_Table_Rowset_Abstract | false
     */
    public function deletarContaEntidade(ContaEntidadeTO $ceTO)
    {
        try {
            $this->beginTransaction();
            $orm = new ContaEntidadeORM();
            $where = $orm->montarWhere($ceTO, true);
            if (!$where) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $orm->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que deleta relacionamento de telefone com entidade.
     * @param ContatosTelefoneEntidadeTO $cteTO
     * @param Mixed $where
     * @return Zend_Db_Table_Rowset_Abstract | false
     */
    public function deletarContatoTelefoneEntidade(ContatosTelefoneEntidadeTO $cteTO)
    {
        $contatosTelefoneEntidadeORM = new ContatosTelefoneEntidadeORM();
        $this->beginTransaction();
        $where = $contatosTelefoneEntidadeORM->montarWhere($cteTO);
        try {
            if (!$where) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $contatosTelefoneEntidadeORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que deleta relacionamento de projeto com entidade.
     * @param ProjetoEntidadeTO $peTO
     * @param Mixed $where
     * @return Zend_Db_Table_Rowset_Abstract | false
     */
    public function deletarProjetoEntidade(ProjetoEntidadeTO $peTO, $where = null)
    {
        $projetoEntidadeORM = new ProjetoEntidadeORM();
        $this->beginTransaction();
        if (!$where) {
            $where = $projetoEntidadeORM->montarWhere($peTO);
        }
        try {
            if (!$where) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $projetoEntidadeORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que deleta relacionamento de contato com entidade.
     * @param ContatoEntidadeTO $ceTO
     * @param Mixed $where
     * @return Boolean
     */
    public function deletarContatoEntidade(ContatoEntidadeTO $ceTO)
    {
        $contatoEntidadeORM = new ContatoEntidadeORM();
        $this->beginTransaction();
        $where = $contatoEntidadeORM->montarWhere($ceTO);
        try {
            if (!$where) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $contatoEntidadeORM->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que deleta o vinculo das funcionalidades da entidade
     * @param EntidadeFuncionalidadeTO $efTO
     * @param Mixed $where
     * @return Boolean
     */
    public function deletarEntidadeFuncionalidade(EntidadeFuncionalidadeTO $efTO, $where = null)
    {
        try {
            $this->beginTransaction();
            $orm = new EntidadeFuncionalidadeORM();
            if (!$where) {
                $where = $orm->montarWhere($efTO, true);
            }
            if (!$where) {
                THROW new Zend_Exception('O TO não pode vir vazio!');
            }
            $orm->delete($where);
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * lista EntidadeInteracao
     * @param EntidadeIntegracaoTO $entidadeIntegracao
     * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >
     * @see EntidadeBO::retornaEntidadeIntegracao
     */
    public function retornaEntidadeIntegracao(EntidadeIntegracaoTO $entidadeIntegracao)
    {
        $ORM = new EntidadeIntegracaoORM();
        $where = $ORM->montarWhere($entidadeIntegracao, false);
        return $ORM->fetchAll($where);
    }

    /**
     * Salva EntidadeInteracao
     * @param EntidadeIntegracaoTO $entidadeIntegracao
     * @throws Zend_Exception
     * @return Ambigous <mixed, multitype:>
     * @see EntidadeBO::salvaEntidadeIntegracao
     */
    public function salvarEntidadeIntegracao(EntidadeIntegracaoTO $entidadeIntegracao)
    {
        $this->beginTransaction();
        try {
            $orm = new EntidadeIntegracaoORM();
            $idEntidadeIntegracao = $orm->insert($entidadeIntegracao->toArrayInsert());
            $this->commit();
            return $idEntidadeIntegracao;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            throw $e;
        }
    }

    /**
     * Edita EntidadeInteracao
     * @param EntidadeIntegracaoTO $entidadeIntegracao
     * @throws Zend_Exception
     * @see EntidadeBO::salvaEntidadeIntegracao
     */
    public function editarEntidadeIntegracao(EntidadeIntegracaoTO $entidadeIntegracao)
    {
        $this->beginTransaction();
        try {
            $orm = new EntidadeIntegracaoORM();
            $orm->update($entidadeIntegracao->toArrayUpdate(false, array('id_entidadeintegracao')), $orm->montarWhere($entidadeIntegracao, true));

            $this->commit();
        } catch (Zend_Exception $e) {
            $this->rollBack();
            throw $e;
        }
    }

    /**
     * Salva no banco o registro do upload do manual do aluno
     * @param UploadTO $upload
     * @throws Zend_Exception
     * @return Ambigous <Ambigous, boolean, mixed, multitype:>|boolean
     */
    public function salvarUploadManualAluno(UploadTO $upload)
    {
        $dao = new UploadDAO();
        $this->beginTransaction();
        try {
            $id = $dao->cadastrarUpload($upload);
            $this->commit();
            return $id;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

    /**
     * @param VwEntidadeCompartilhaDadosTO $to
     * @return bool|Zend_Db_Table_Rowset_Abstract
     * @throws Zend_Exception
     */
    public function retornarVwEntidadeCompartilhaDados(VwEntidadeCompartilhaDadosTO $to)
    {
        $orm = new VwEntidadeCompartilhaDadosORM();
        $where = $orm->montarWhereView($to, false, true);
        try {
            $obj = $orm->fetchAll($where, 'st_nomeentidade');
            return $obj;
        } catch (Zend_Exception $e) {
            $msg ['WHERE'] = $where;
            $msg ['SQL'] = $e->getMessage();
            $msg ['TO'] = $to;
            $this->excecao = $msg;
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Edita no banco o registro do upload do manual do aluno
     * @param UploadTO $imagem
     * @throws Zend_Exception
     * @return boolean
     */
    public function editarUploadManualAluno(UploadTO $upload)
    {
        try {
            $uploadORM = new UploadORM();
            $updateUpload['st_upload'] = $upload->getSt_upload();
            $this->beginTransaction();
            $uploadORM->update($updateUpload, " id_upload = " . $upload->getId_upload());
            $this->commit();
            return true;
        } catch (Zend_Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

}

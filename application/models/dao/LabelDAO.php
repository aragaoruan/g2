<?php
/**
 * Classe que retorna, edita e cadastra dados de Label no banco de dados
 * @author Eduardo Romão - ejushiro@gmail.com
 * 
 * @package models
 * @subpackage dao
 */
class LabelDAO extends Ead1_DAO{

	/**
	 * Metodo que retorna as funcionalidades da Entidade com o Label
	 * @param VwLabelFuncionalidadeTO $vwLfTO
	 * @return Zend_Db_Table_Rowset_Abstract
	 */
	public function retornaLabelFuncionalidade(VwLabelFuncionalidadeTO $vwLfTO){
		try{
			$vwLabelFuncionalidadeORM = new VwLabelFuncionalidadeORM();
			$where = $vwLabelFuncionalidadeORM->montarWhereView($vwLfTO,false,true,false);
			$obj = $vwLabelFuncionalidadeORM->fetchAll($where);
			return $obj;
		}catch (Zend_Exception $e){
			THROW new Zend_Exception($e->getMessage());
			return false;
		}
	}
	
	
	
}
<?php

/**
 * Classe de persistência do Primeiro Acesso no Portal do Aluno
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 * 
 * @package models
 * @subpackage dao
 */
class PrimeiroAcessoDAO extends Ead1_DAO {

    /**
     * Metodo que cadastra uma etapa
     * 
     * @param Ead1_TO_Dinamico $to
     * @param type $etapa
     * @return boolean
     */
    public function cadastrarEtapa(Ead1_TO_Dinamico $to) {
        $ormClass = get_class($to);
        $ormClass = str_replace('TO', '', $ormClass) . 'ORM';
        $orm = new $ormClass();
        // unset($dTO->arrDados);
        $insert = $to->toArrayInsert();
        $this->beginTransaction();
        try {
            $id = $orm->insert($insert);
            $this->commit();
            return $id;
        } catch (Exception $e) {
            $this->rollBack();
            $arr ['TO'] = $to;
            $arr ['WHERE'] = $insert;
            $arr ['SQL'] = $e->getMessage();
            $this->excecao = $arr;
            return false;
        }
    }

    /**
     * Metodo que edita uma etapa
     * 
     * @param Ead1_TO_Dinamico $to        	
     * @return Boolean
     */
    public function editarEtapa(Ead1_TO_Dinamico $to, $etapa) {
        $ormClass = get_class($to);
        $ormClass = str_replace('TO', '', $ormClass) . 'ORM';
        $orm = new $ormClass();

        unset($to->arrDados);
        $update = $to->toArrayUpdate(false, array(
            'id_pa_' . $etapa
        ));
        $this->beginTransaction();
        try {
            $getId = 'getId_pa_' . $etapa;
            $orm->update($update, ' id_pa_' . $etapa . ' = ' . $to->$getId());
            $this->commit();
            return true;
        } catch (Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que retorna uma etapa
     * 
     * @param Ead1_TO_Dinamico $to
     * @param type $where
     * @return boolean
     * @throws Zend_Exception
     */
    public function retornarEtapa(Ead1_TO_Dinamico $to, $where = null) {
        $ormClass = get_class($to);
        $ormClass = str_replace('TO', '', $ormClass) . 'ORM';
        $orm = new $ormClass();

        if (!$where) {
            $where = $orm->montarWhere($to);
            //$where .= " AND bl_ativo = 1 "; 
        }
        try {
            $obj = $orm->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $msg ['WHERE'] = $where;
            $msg ['SQL'] = $e->getMessage();
            $msg ['TO'] = $to;
            $this->excecao = $msg;
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Metodo que cadastra o progresso do primeiro acesso
     * @param PAMarcacaoEtapaTO $to
     * @return boolean
     */
    public function cadastrarMarcacaoEtapa(PAMarcacaoEtapaTO $to) {

        $orm = new PAMarcacaoEtapaORM();
        $insert = $to->toArrayInsert();
        $this->beginTransaction();
        try {
            $id = $orm->insert($insert);
            $this->commit();
            return $id;
        } catch (Exception $e) {
            $this->rollBack();
            $arr ['TO'] = $to;
            $arr ['WHERE'] = $insert;
            $arr ['SQL'] = $e->getMessage();
            $this->excecao = $arr;
            return false;
        }
    }

    /**
     * Metodo que edita o progresso 
     * 
     * @param PAMarcacaoEtapaTO $to
     * @return boolean
     */
    public function editarMarcacaoEtapa(PAMarcacaoEtapaTO $to) {

        $orm = new PAMarcacaoEtapaORM();

        unset($to->arrDados);
        $update = $to->toArrayUpdate(false, array(
            'id_pa_marcacaoetapa'
        ));
        $this->beginTransaction();
        try {
            $orm->update($update, ' id_pa_marcacaoetapa = ' . $to->getId_pa_marcacaoetapa());
            $this->commit();
            return true;
        } catch (Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    public function salvarEndereco(EnderecoTO $to) {

        try {
            $orm = new EnderecoORM();
            $this->beginTransaction();
            if (!$to->getId_endereco()) {
                $dados = $to->toArrayInsert();
                $id = $orm->insert($dados);
                $this->commit();
                return $id;
            } else {
                unset($to->arrDados);
                $update = $to->toArrayUpdate(false, array(
                    'id_endereco'
                ));
                $orm->update($update, ' id_endereco = ' . $to->getId_endereco());
                $this->commit();
                return true;
            }
        } catch (Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            echo $this->excecao;
            return false;
        }
    }

    public function salvarEmail(ContatosEmailTO $to) {
        try {
            $orm = new ContatosEmailORM();
            $this->beginTransaction();
            if (!$to->getId_email()) {
                Zend_Debug::dump('insert', __CLASS__ . '(' . __LINE__ . ')');
                exit;
                $dados = $to->toArrayInsert();
                $id = $orm->insert($dados);
                return $id;
            } else {
                unset($to->arrDados);
                $update = $to->toArrayUpdate(false, array(
                    'id_email'
                ));
                $orm->update($update, ' id_email = ' . $to->getId_email());
                return true;
            }
            $this->commit();
        } catch (Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    public function editarTelefone(ContatosTelefoneTO $to) {
        try {
            $orm = new ContatosTelefoneORM();
            $this->beginTransaction();
            if (!$to->getId_contatostelefone()) {
                $dados = $to->toArrayInsert();
                $id = $orm->insert($dados);
                $this->commit();
                return $id;
            } else {
                unset($to->arrDados);
                $update = $to->toArrayUpdate(false, array(
                    'id_contatostelefone'
                ));
                $orm->update($update, ' id_contatostelefone = ' . $to->getId_contatostelefone());
                $this->commit();
                return true;
            }
        } catch (Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            return false;
        }
    }

    /**
     * Metodo que retorna as ocorrencias em aberto
     * 
     * @param Ead1_TO_Dinamico $to
     * @param type $where
     * @return boolean
     * @throws Zend_Exception
     */
    public function retornarOcorrenciasAbertasDadosCadastrais(Ead1_TO_Dinamico $to, $where = null) {
        $ormClass = get_class($to);
        $ormClass = str_replace('TO', '', $ormClass) . 'ORM';
        $orm = new $ormClass();

        if (!$where) {
            $where = $orm->montarWhere($to);
        }
        $where .= " AND id_situacao != " . OcorrenciaTO::SITUACAO_ENCERRADA;

        try {
            $obj = $orm->fetchAll($where);
            return $obj;
        } catch (Zend_Exception $e) {
            $msg ['WHERE'] = $where;
            $msg ['SQL'] = $e->getMessage();
            $msg ['TO'] = $to;
            $this->excecao = $msg;
            THROW new Zend_Exception($e->getMessage());
        }
    }

    /**
     * 
     * @return boolean
     * @throws Zend_Exception
     */
    public function retornarPAMarcacaoSemContrato($prox_id = 0) {
        try {
            $orm = new MatriculaORM();

            $query = 'SELECT TOP 100 * FROM tb_pa_marcacaoetapa '
                    . 'WHERE dt_aceita IS NOT NULL AND bl_aceitacaocontrato = 1 '
                    . 'AND id_pa_marcacaoetapa > ' . ($prox_id ? $prox_id : '0')
                    . ' ORDER BY id_pa_marcacaoetapa';

            return Ead1_TO_Dinamico::encapsularTo($orm->getDefaultAdapter()->query($query)->fetchAll(), new PAMarcacaoEtapaTO());
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW $e;
            return false;
        }
    }

}

<?php
/**
 * Classe de persistencia de livro registro com o banco de dados
 * @author Eduardo Romão <ejushiro@gmail.com>
 * @since 21/02/2011
 * 
 * @package models
 * @subpackage dao
 */
class LivroRegistroDAO extends Ead1_DAO {

	/**
	 * Metodo que cadastra um livro de registro
	 * @param LivroRegistroEntidadeTO $lreTO
	 * @see LivroRegistroBO::cadastrarLivroRegistro
	 * @return int $id_livroregistroentidade | false
	 */
	public function cadastrarLivroRegistro(LivroRegistroEntidadeTO $lreTO){
		try{
			$this->beginTransaction();
			$orm = new LivroRegistroEntidadeORM();
			$insert = $lreTO->toArrayInsert();
			$id_livroregistroentidade = $orm->insert($insert);
			$this->commit();
			return $id_livroregistroentidade;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($this->excecao);
			return false;
		}
	}
	
	/**
	 * Metodo que edita um livro de registro
	 * @param LivroRegistroEntidadeTO $lrTO
	 * @see LivroRegistroBO::editarLivroRegistro
	 * @return boolean
	 */
	public function editarLivroRegistro(LivroRegistroEntidadeTO $lreTO, $where = null){
		try{
			$this->beginTransaction();
			$orm = new LivroRegistroEntidadeORM();
			if(!$where){
				$where = $orm->montarWhere($lreTO,true);
			}
			$update = $lreTO->toArrayUpdate(false,array('id_livroregistroentidade'));
			$orm->update($update, $where);
			$this->commit();
			return true;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($this->excecao);
			return false;
		}
	}
	
	/**
	 * Metodo que retorna livro de registro
	 * @param LivroRegistroTO $lrTO
	 * @see LivroRegistroBO::retornarLivroRegistroEntidade
	 * @return boolean
	 */
	public function retornarLivroRegistroEntidade(LivroRegistroEntidadeTO $lreTO){
		$orm = new LivroRegistroEntidadeORM();
		$where = $orm->montarWhere($lreTO);
		try{
			$obj = $orm->fetchAll($where);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			THROW $e;
			return false;
		} 
	}
	
	/**
	 * Metodo que retorna livro de registro
	 * @param LivroRegistroTO $lrTO
	 * @param Mixed $where
	 * @see LivroRegistroBO::retornarLivroRegistro
	 * @return boolean
	 */
	public function retornarLivroRegistro(LivroRegistroTO $lrTO,$where = null){
		try{
			$orm = new LivroRegistroORM();
			if(!$where){
				$where = $orm->montarWhere($lrTO);
			}
			$obj = $orm->fetchAll($where);
			return $obj;
		}catch(Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		} 
	}
	
	/**
	 * Metodo que exclui Livro de registro
	 * @param LivroRegistroEntidadeTO $fpTO
	 * @return boolean
	 */
	public function excluirLivroRegistro(LivroRegistroEntidadeTO $lreTO){
		try{
			$this->beginTransaction();
			$orm = new LivroRegistroEntidadeORM();
			$where = $orm->montarWhere($lreTO);
			$orm->delete($where);
			$this->commit();
			return true;
		}catch (Zend_Exception $e){
			$this->rollBack();
			$this->excecao = $e->getMessage();
			THROW new Zend_Exception($this->excecao);
			return false;
		}
	}
	
}

?>
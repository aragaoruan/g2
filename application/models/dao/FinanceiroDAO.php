<?php
/**
 * Classe de acesso a dados para o Financeiro
 * @author Eder Lamar<eder.mariano@ead1.com.br>
 * 
 * @package models
 * @subpackage dao
 */
class FinanceiroDAO extends Ead1_DAO {
	
	/**
	 * Método que busca registro na tabela de vendas (VendaORM) e retorna um mensageiro
	 *
	 * @TODO implementar
	 * 
	 * @param VendaTO $vTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVenda(VendaTO $vTO) {
		
	}
	
	/**
	 * Método que busca registro na tabela de vendas produtos e os dados necessários para a tela 
	 * 	através da view (VwProdutoVendaORM) e retorna um mensageiro.
	 *
	 * @param VwProdutoVendaTO $vpvTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVendaProduto(VwProdutoVendaTO $vpvTO) {
		$vendaProdutoORM = new VendaProdutoORM();
		try {
			$obj = $vendaProdutoORM->fetchAll($vendaProdutoORM->montarWhere($vpvTO, false, true));
			return $obj;
		}catch (Zend_Exception $e){
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Método que busca registro na tabela de lancamentos através da view (VwLancamentoORM) e retorna um mensageiro
	 *
	 * @param VwLancamentoTO $vlTO
	 * @return Zend_Db_Table_Rowset
	 */
	public function retornarLancamento( VwLancamentoTO $vlTO ) {
		$vwLancamentoORM = new VwLancamentoORM();
		try {
			$obj = $vwLancamentoORM->fetchAll( $vwLancamentoORM->montarWhere( $vlTO, false, true ) );
			return $obj;
		}catch ( Zend_Exception $e ){
			$this->excecao = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Método que realiza registro na tabela de vendas (VendaORM) e retorna um mensageiro
	 *
	 * @TODO implementar
	 * 
	 * @param VendaTO $vTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarVenda(VendaTO $vTO) {
		
	}
	
	/**
	 * Método que realiza registro na tabela de vendas produtos (VendaProdutoORM) e retorna um mensageiro
	 *
	 * @TODO implementar
	 * 
	 * @param VendaProdutoTO $vTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarVendaProduto(VendaProdutoTO $vpTO) {
	
	}
		
	/**
	 * Método que realiza atualização na tabela de vendas (VendaORM) e retorna um mensageiro
	 *
	 * @TODO implementar
	 * 
	 * @param VendaTO $vTO
	 * @return Ead1_Mensageiro
	 */
	public function atualizarVenda(VendaTO $vTO) {
	
	}
	
	/**
	 * Método que realiza atualização na tabela de vendas produtos (VendaProdutoORM) e retorna um mensageiro
	 *
	 * @TODO implementar
	 * 
	 * @param VendaProdutoTO $vTO
	 * @return Ead1_Mensageiro
	 */
	public function atualizarVendaProduto(VendaProdutoTO $vpTO) {
	
	}
	
	/**
	 * Método que realiza exclusão [LÓGICA] na tabela de vendas (VendaORM) e retorna um mensageiro
	 *
	 * @TODO implementar
	 * 
	 * @param VendaTO $vTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarVenda(VendaTO $vTO) {
	
	}
	
	/**
	 * Método que realiza exclusão na tabela de vendas produtos (VendaProdutoORM) e retorna um mensageiro
	 *
	 * @TODO implementar
	 * 
	 * @param VendaProdutoTO $vTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarVendaProduto(VendaProdutoTO $vpTO) {
	
	}
	
	/**
	 * Retorna os dados de configuração do boleto para a entidade
	 * @param int $idEntidade
	 * @return Zend_Db_Table_Row
	 */
	public function retornarDadosConfBoleto( $idEntidade ) {
		$contaEntidadeORM	= new ContaEntidadeORM();
		$select				= $contaEntidadeORM->select()
											->setIntegrityCheck( false ) 
											->from( array( 'ce' 		=> 'tb_contaentidade' ), array('id_contaentidade', 'st_conta', 'st_digitoconta', 'st_agencia', 'st_digitoagencia' ) )
											->joinInner( array( 'bc' 	=> 'tb_boletoconfig'), ' bc.id_contaentidade = ce.id_contaentidade' , array( 'bc.id_boletoconfig', 'bc.nu_carteira','st_contacedente','st_digitocedente' ) )
											->joinInner( array( 'ebc' 	=> 'tb_entidadeboletoconfig'), 'ebc.id_boletoconfig = bc.id_boletoconfig', '' )
											->joinInner( array( 'ef' 	=> 'tb_entidadefinanceiro' ), ' ef.id_entidadefinanceiro = ebc.id_entidadefinanceiro ', '' )
											->joinInner( array( 'b'		=> 'tb_banco'), 'b.st_banco = ce.st_banco', array( 'id_banco', 'st_banco', 'st_nomebanco' ) )
											->joinInner( array( 'tdc'	=> 'tb_tipodeconta'), 'tdc.id_tipodeconta = ce.id_tipodeconta', array( 'id_tipodeconta', 'st_tipodeconta' ) )
                                            ->joinInner( array( 'e'     =>  'tb_entidade'), 'e.id_entidade = ce.id_entidade', array('st_cnpj', 'st_razaosocial'))
											->joinInner( array( 'ee'	=>	'tb_entidadeendereco'), 'ee.id_entidade	= e.id_entidade', '' )
											->joinInner( array( 'en'	=>	'tb_endereco'), 'en.id_endereco	= ee.id_endereco', array( 'id_endereco', 'st_endereco', 'st_bairro', 'st_cep', 'st_complemento', 'nu_numero' ) )
											->joinInner( array( 'mu'	=>	'tb_municipio'), 'en.id_municipio	= mu.id_municipio', array( 'id_municipio', 'st_nomemunicipio', 'sg_uf' ) )

											->where( 'ce.id_entidade = ?', $idEntidade );
											
		return $contaEntidadeORM->fetchRow( $select );											
		
	}
	
	/**
	 * Retornas os dados de configuração de cartão de crédito para uma entidade
	 * @param int $idEntidade
	 * @param int $idCartaoBandeira
	 * @return Zend_Db_Table_Rowset_Abstract
	 */
	public function retornarDadosConfCartao( $idEntidade, $idCartaoBandeira = null ) {
		
		$contaEntidadeORM	= new ContaEntidadeORM();
		$select				= $contaEntidadeORM->select()
												->from( array( 'ce'	=> 'tb_contaentidade' ), array( 'id_contaentidade', 'id_banco', 'st_agencia', 'st_digitoagencia', 'st_conta', 'st_digitoconta' ) )
												->setIntegrityCheck( false )
												->joinInner( array( 'cc' => 'tb_cartaoconfig' ), 'cc.id_contaentidade	= ce.id_contaentidade', array( 'id_cartaoconfig','st_contratooperadora', 'st_gateway' ) )
												->joinInner( array( 'cb' => 'tb_cartaobandeira' ), 'cc.id_cartaobandeira	= cb.id_cartaobandeira', array( 'id_cartaobandeira', 'st_cartaobandeira' ) )
												->joinInner( array( 'co' => 'tb_cartaooperadora' ), 'co.id_cartaooperadora	= cc.id_cartaooperadora', array( 'id_cartaooperadora', 'st_cartaooperadora' ) )
												->joinInner( array( 'ecc' => 'tb_entidadecartaoconfig' ), 'ecc.id_cartaoconfig	= cc.id_cartaoconfig', null )
												->joinInner( array( 'ef' => 'tb_entidadefinanceiro' ), 'ef.id_entidadefinanceiro	= ecc.id_entidadefinanceiro', null )
												->where( 'ce.id_entidade = ?', $idEntidade );
												
		if( !empty( $idCartaoBandeira ) ){
			$select->where( 'cb.id_cartaobandeira = ?', $idCartaoBandeira );
		}												
		
		
		return $contaEntidadeORM->fetchAll( $select );
	}
	
	/**
	 * Retorna as bandeiras de cartão disponíveis de uma entidade
	 * @param CartaoBandeiraTO $cartaoBandeiraTO
	 * @param int $idEntidade
	 * @return Zend_Db_Table_Rowset
	 */
	public function retornarCartaoBandeira( CartaoBandeiraTO $cartaoBandeiraTO, $idEntidade, $id_sistema = null ){
		
		$cartaoBandeiraORM	= new CartaoBandeiraORM();
		$select				= $cartaoBandeiraORM->select()
												->from( array( 'cb' => 'tb_cartaobandeira' ), array( 'id_cartaobandeira', 'st_cartaobandeira' ) )
												->setIntegrityCheck( false )
												->joinInner( array( 'cc' => 'tb_cartaoconfig' ), 'cb.id_cartaobandeira	= cc.id_cartaobandeira', null )
												->joinInner( array( 'ce' => 'tb_contaentidade' ), 'ce.id_contaentidade = cc.id_contaentidade', null )
                                                ->joinInner( array( 'ecc' => 'tb_entidadecartaoconfig' ), 'ecc.id_cartaoconfig	= cc.id_cartaoconfig', null )
												->where( 'ce.id_entidade = ?', $idEntidade );
		
		if($id_sistema){
			$select->where('cc.id_sistema = ?', $id_sistema);
		}
		
//						Zend_Debug::dump($select->assemble(),__CLASS__.'('.__LINE__.')');exit;
		if( !empty( $cartaoBandeiraTO->id_cartaobandeira ) && is_int( $cartaoBandeiraTO->id_cartaobandeira ) ){
	  		$select->where( 'cb.id_cartaobandeira = ?', $cartaoBandeiraTO->id_cartaobandeira );
	  	}
	  												
	  	if( !empty( $cartaoBandeiraTO->st_cartaobandeira ) ){
	  		$select->where( 'cb.st_cartaobandeira LIKE ?', "%". $cartaoBandeiraTO->st_cartaobandeira ."%" );
	  	}												
		return $cartaoBandeiraORM->fetchAll( $select );	
	}
	
	/**
	 * Retorna as operadoras de cartão de crédito de uma entidade. 
	 * Realiza um filtro com os atributos do TO informado
	 * @param $cartaoOperadoraTO
	 * @param $idEntidade
	 * @param $idCartaoBandeira
	 * @return Zend_Db_Table_Rowset
	 */
	public function retornarCartaoOperadora( CartaoOperadoraTO $cartaoOperadoraTO, $idEntidade, $idCartaoBandeira = null ){
		
		$cartaoOperadoraORM	= new CartaoOperadoraORM();
		$select				= $cartaoOperadoraORM->select()
												->distinct( true )
												->from( array( 'co' => 'tb_cartaooperadora' ), array( 'id_cartaooperadora', 'st_cartaooperadora' ) )
												->setIntegrityCheck( false )
												->joinInner( array( 'cc' => 'tb_cartaoconfig' ), 'co.id_cartaooperadora	= cc.id_cartaooperadora', null )
												->joinInner( array( 'ce' => 'tb_contaentidade' ), 'ce.id_contaentidade = cc.id_contaentidade', null )
												->where( 'ce.id_entidade = ?', $idEntidade )
												;
		
												
		if( !empty( $cartaoOperadoraTO->id_cartaooperadora ) && is_int( $cartaoOperadoraTO->id_cartaooperadora ) ){
	  		$select->where( 'co.id_cartaooperadora = ?', $cartaoOperadoraTO->id_cartaooperadora );
	  	}
	  												
	  	if( !empty( $cartaoOperadoraTO->st_cartaooperadora ) ){
	  		$select->where( 'co.st_cartaooperadora LIKE ?', "%". $cartaoOperadoraTO->st_cartaooperadora ."%" );
	  	}

	  	if( !empty( $idCartaoBandeira ) ){
	  		$select->where( 'cc.id_cartaobandeira	= ?', $idCartaoBandeira );
	  	}
	  	
		return $cartaoOperadoraORM->fetchAll( $select );	
	
	}	
}
<?php
/**
 * Classe de persistência de Cargos no banco de dados
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 * 
 * @package models
 * @subpackage dao
 */
class CargoDAO extends Ead1_DAO {
	
	/**
	 * Metodo que retorna Setor
	 *
	 * @param CargoTO $to
	 * @param unknown_type $where
	 * @throws Zend_Exception
	 * @return Ambigous <Zend_Db_Table_Rowset_Abstract, multitype:, multitype:mixed Ambigous <string, boolean, mixed> >|boolean
	 */
	public function retornarCargo(CargoTO $to, $where = null) {
		$orm = new CargoORM();
		if (! $where) {
			$where = $orm->montarWhere($to);
			// $where .= " AND bl_ativo = 1 ";
		}
		try {
			$obj = $orm->fetchAll ( $where );
			return $obj;
		} catch ( Zend_Exception $e ) {
			$msg ['WHERE'] = $where;
			$msg ['SQL'] = $e->getMessage ();
			$msg ['TO'] = $to;
			$this->excecao = $msg;
			THROW new Zend_Exception ( $e->getMessage () );
			return false;
		}
	}
}
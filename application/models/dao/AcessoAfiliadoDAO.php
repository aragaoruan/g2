<?php

/**
 * Classe de acesso a dados referente a AcessoAfiliadoDAO
 * @author Elcio Mauo Guimarães - <elcioguimaraes@gmail.com>
 * @since 09/01/2013
 * @package models
 * @subpackage dao
 */
class AcessoAfiliadoDAO extends Ead1_DAO {

	/**
	 * Método que insere o AcessoAfiliado
	 * 
	 * @param AcessoAfiliadoTO $AcessoAfiliadoTO
	 * @throws Zend_Exception
	 * @return Ambigous <mixed, multitype:>
	 * @see AcessoAfiliadoBO::salvarAcessoAfiliado();
	 */
	public function cadastrarAcessoAfiliado(AcessoAfiliadoTO $to){
		$this->beginTransaction();
		try{
			$orm = new AcessoAfiliadoORM();
			$id_acessoafiliado = $orm->insert($to->toArrayInsert());
			$this->commit();
			return $id_acessoafiliado;
		}catch(Zend_Exception $e){
			$this->rollBack();
			throw $e;
			return false;
		}	
	}
	
	/**
	 * Método de edição do AcessoAfiliado
	 * @param AcessoAfiliadoTO $to
	 * @throws Zend_Exception
	 * @see AcessoAfiliadoBO::salvarAcessoAfiliado();
	 */
	public function editarAcessoAfiliado(AcessoAfiliadoTO $to){
		$this->beginTransaction();
		try {
			$orm = new AcessoAfiliadoORM();
			$orm->update($to->toArrayUpdate(false, array('id_acessoafiliado')), $orm->montarWhere($to, true));
			$this->commit();
			return true;
		} catch (Zend_Exception $e) {
			$this->rollBack();
			throw $e;
			return false;
		}
	}
	
	/**
	 * Método de listagem de AcessoAfiliado
	 * @param AcessoAfiliadoTO $to
	 * @return Ambigous <boolean, multitype:>
	 * @see AcessoAfiliadoBO::listarAcessoAfiliado();
	 */
	public function retornarAcessoAfiliado(AcessoAfiliadoTO $to){
		$orm = new AcessoAfiliadoORM();
		return $orm->consulta($to);
	}
}
<?php

/**
 * Class VwSalaDisciplinaEntidadeTO
 */
class VwSalaDisciplinaEntidadeTO extends Ead1_TO_Dinamico{


    public $id_projetopedagogico;
    public $id_entidade;
    public $id_disciplina;
    public $st_disciplina;
    public $st_saladeaula;
    public $dt_inicioinscricao;
    public $dt_fiminscricao;
    public $dt_abertura;
    public $dt_encerramento;
    public $st_professor;
    public $bl_conteudo;
    public $st_sistema;

    /**
     * @param $bl_conteudo
     */
    public function setBl_conteudo($bl_conteudo)
    {
        $this->bl_conteudo = $bl_conteudo;
    }

    /**
     * @return mixed
     */
    public function getBl_conteudo()
    {
        return $this->bl_conteudo;
    }

    /**
     * @param $dt_abertura
     */
    public function setDt_abertura($dt_abertura)
    {
        $this->dt_abertura = $dt_abertura;
    }

    /**
     * @return mixed
     */
    public function getDt_abertura()
    {
        return $this->dt_abertura;
    }

    /**
     * @param $dt_encerramento
     */
    public function setDt_encerramento($dt_encerramento)
    {
        $this->dt_encerramento = $dt_encerramento;
    }

    /**
     * @return mixed
     */
    public function getDt_encerramento()
    {
        return $this->dt_encerramento;
    }

    /**
     * @param $dt_fiminscricao
     */
    public function setDt_fiminscricao($dt_fiminscricao)
    {
        $this->dt_fiminscricao = $dt_fiminscricao;
    }

    /**
     * @return mixed
     */
    public function getDt_fiminscricao()
    {
        return $this->dt_fiminscricao;
    }

    /**
     * @param $dt_inicioinscricao
     */
    public function setDt_inicioinscricao($dt_inicioinscricao)
    {
        $this->dt_inicioinscricao = $dt_inicioinscricao;
    }

    /**
     * @return mixed
     */
    public function getDt_inicioinscricao()
    {
        return $this->dt_inicioinscricao;
    }

    /**
     * @param $id_disciplina
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
    }

    /**
     * @return mixed
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return mixed
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param $id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    /**
     * @return mixed
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param $st_disciplina
     */
    public function setSt_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
    }

    /**
     * @return mixed
     */
    public function getSt_disciplina()
    {
        return $this->st_disciplina;
    }

    /**
     * @param $st_professor
     */
    public function setSt_professor($st_professor)
    {
        $this->st_professor = $st_professor;
    }

    /**
     * @return mixed
     */
    public function getSt_professor()
    {
        return $this->st_professor;
    }

    /**
     * @param $st_saladeaula
     */
    public function setSt_saladeaula($st_saladeaula)
    {
        $this->st_saladeaula = $st_saladeaula;
    }

    /**
     * @return mixed
     */
    public function getSt_saladeaula()
    {
        return $this->st_saladeaula;
    }

    /**
     * @param $st_sistema
     */
    public function setSt_sistema($st_sistema)
    {
        $this->st_sistema = $st_sistema;
    }

    /**
     * @return mixed
     */
    public function getSt_sistema()
    {
        return $this->st_sistema;
    }

}
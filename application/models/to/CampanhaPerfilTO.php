<?php
/**
 * Classe para encapsular os dados de relacionamento entre perfil e campanha comercial.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class CampanhaPerfilTO extends Ead1_TO_Dinamico {

	/**
	 * Id da campanha comercial.
	 * @var int
	 */
	public $id_campanhacomercial;
	
	/**
	 * Id do perfil.
	 * @var int
	 */
	public $id_perfil;
	
	/**
	 * @return int
	 */
	public function getId_campanhacomercial() {
		return $this->id_campanhacomercial;
	}
	
	/**
	 * @return int
	 */
	public function getId_perfil() {
		return $this->id_perfil;
	}
	
	/**
	 * @param int $id_campanhacomercial
	 */
	public function setId_campanhacomercial($id_campanhacomercial) {
		$this->id_campanhacomercial = $id_campanhacomercial;
	}
	
	/**
	 * @param int $id_perfil
	 */
	public function setId_perfil($id_perfil) {
		$this->id_perfil = $id_perfil;
	}

}

?>
<?php
/**
 * Classe para encapsular o nível de ensino.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class NivelEnsinoTO extends Ead1_TO_Dinamico {

	/**
	 * Atributo que contém o id do nível de ensino.
	 * @var int
	 */
	public $id_nivelensino;
	
	/**
	 * Atributo que contém o nível de ensino.
	 * @var string
	 */
	public $st_nivelensino;
	
	/**
	 * @return int
	 */
	public function getId_nivelensino() {
		return $this->id_nivelensino;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nivelensino() {
		return $this->st_nivelensino;
	}
	
	/**
	 * @param int $id_nivelensino
	 */
	public function setId_nivelensino($id_nivelensino) {
		$this->id_nivelensino = $id_nivelensino;
	}
	
	/**
	 * @param string $st_nivelensino
	 */
	public function setSt_nivelensino($st_nivelensino) {
		$this->st_nivelensino = $st_nivelensino;
	}

}

?>
<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class VwFormaMeioDivisaoTO extends Ead1_TO_Dinamico{

	public $id_formapagamento;
	public $st_formapagamento;
	public $id_tipodivisaofinanceira;
	public $st_tipodivisaofinanceira;
	public $id_meiopagamento;
	public $st_meiopagamento;
	public $nu_parcelaquantidademin;
	public $nu_parcelaquantidademax;
	public $nu_juros;
	public $nu_maxparcelas;


	/**
	 * @return the $id_formapagamento
	 */
	public function getId_formapagamento(){ 
		return $this->id_formapagamento;
	}

	/**
	 * @return the $st_formapagamento
	 */
	public function getSt_formapagamento(){ 
		return $this->st_formapagamento;
	}

	/**
	 * @return the $id_tipodivisaofinanceira
	 */
	public function getId_tipodivisaofinanceira(){ 
		return $this->id_tipodivisaofinanceira;
	}

	/**
	 * @return the $st_tipodivisaofinanceira
	 */
	public function getSt_tipodivisaofinanceira(){ 
		return $this->st_tipodivisaofinanceira;
	}

	/**
	 * @return the $id_meiopagamento
	 */
	public function getId_meiopagamento(){ 
		return $this->id_meiopagamento;
	}

	/**
	 * @return the $st_meiopagamento
	 */
	public function getSt_meiopagamento(){ 
		return $this->st_meiopagamento;
	}

	/**
	 * @return the $nu_parcelaquantidademin
	 */
	public function getNu_parcelaquantidademin(){ 
		return $this->nu_parcelaquantidademin;
	}

	/**
	 * @return the $nu_parcelaquantidademax
	 */
	public function getNu_parcelaquantidademax(){ 
		return $this->nu_parcelaquantidademax;
	}

	/**
	 * @return the $nu_juros
	 */
	public function getNu_juros(){ 
		return $this->nu_juros;
	}

	/**
	 * @return the $nu_maxparcelas
	 */
	public function getNu_maxparcelas(){ 
		return $this->nu_maxparcelas;
	}


	/**
	 * @param field_type $id_formapagamento
	 */
	public function setId_formapagamento($id_formapagamento){ 
		$this->id_formapagamento = $id_formapagamento;
	}

	/**
	 * @param field_type $st_formapagamento
	 */
	public function setSt_formapagamento($st_formapagamento){ 
		$this->st_formapagamento = $st_formapagamento;
	}

	/**
	 * @param field_type $id_tipodivisaofinanceira
	 */
	public function setId_tipodivisaofinanceira($id_tipodivisaofinanceira){ 
		$this->id_tipodivisaofinanceira = $id_tipodivisaofinanceira;
	}

	/**
	 * @param field_type $st_tipodivisaofinanceira
	 */
	public function setSt_tipodivisaofinanceira($st_tipodivisaofinanceira){ 
		$this->st_tipodivisaofinanceira = $st_tipodivisaofinanceira;
	}

	/**
	 * @param field_type $id_meiopagamento
	 */
	public function setId_meiopagamento($id_meiopagamento){ 
		$this->id_meiopagamento = $id_meiopagamento;
	}

	/**
	 * @param field_type $st_meiopagamento
	 */
	public function setSt_meiopagamento($st_meiopagamento){ 
		$this->st_meiopagamento = $st_meiopagamento;
	}

	/**
	 * @param field_type $nu_parcelaquantidademin
	 */
	public function setNu_parcelaquantidademin($nu_parcelaquantidademin){ 
		$this->nu_parcelaquantidademin = $nu_parcelaquantidademin;
	}

	/**
	 * @param field_type $nu_parcelaquantidademax
	 */
	public function setNu_parcelaquantidademax($nu_parcelaquantidademax){ 
		$this->nu_parcelaquantidademax = $nu_parcelaquantidademax;
	}

	/**
	 * @param field_type $nu_juros
	 */
	public function setNu_juros($nu_juros){ 
		$this->nu_juros = $nu_juros;
	}

	/**
	 * @param field_type $nu_maxparcelas
	 */
	public function setNu_maxparcelas($nu_maxparcelas){ 
		$this->nu_maxparcelas = $nu_maxparcelas;
	}

}
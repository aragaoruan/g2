<?php
/**
 * Classe para encapsular os dados de DDI.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class DdiTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o número do DDI.
	 * @var int
	 */
	public $nu_ddi;
	
	/**
	 * Contém o id do país.
	 * @var int
	 */
	public $id_pais;
	
	/**
	 * @return int
	 */
	public function getId_pais() {
		return $this->id_pais;
	}
	
	/**
	 * @return int
	 */
	public function getNu_ddi() {
		return $this->nu_ddi;
	}
	
	/**
	 * @param int $id_pais
	 */
	public function setId_pais($id_pais) {
		$this->id_pais = $id_pais;
	}
	
	/**
	 * @param int $nu_ddi
	 */
	public function setNu_ddi($nu_ddi) {
		$this->nu_ddi = $nu_ddi;
	}

}

?>
<?php
/**
 * Classe para encapsular os dados de dia não letivo.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class DiaLetivoTO extends Ead1_TO_Dinamico {

	/**
	 * Id do dia.
	 * @var int
	 */
	public $id_dialetivo;
	
	/**
	 * Data do dia.
	 * @var string
	 */
	public $dt_data;
	
	/**
	 * Ocasião do dia.
	 * @var string
	 */
	public $st_nomeocasiao;
	
	/**
	 * Id do tipo de dia.
	 * @var int
	 */
	public $id_tipodia;
	
	/**
	 * Id do calendário letivo do dia.
	 * @var int
	 */
	public $id_calendarioletivo;
	
	/**
	 * @return string
	 */
	public function getDt_data() {
		return $this->dt_data;
	}
	
	/**
	 * @return int
	 */
	public function getId_calendarioletivo() {
		return $this->id_calendarioletivo;
	}
	
	/**
	 * @return int
	 */
	public function getId_dialetivo() {
		return $this->id_dialetivo;
	}
	
	/**
	 * @return int
	 */
	public function getId_tipodia() {
		return $this->id_tipodia;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nomeocasiao() {
		return $this->st_nomeocasiao;
	}
	
	/**
	 * @param string $dt_data
	 */
	public function setDt_data($dt_data) {
		$this->dt_data = $dt_data;
	}
	
	/**
	 * @param int $id_calendarioletivo
	 */
	public function setId_calendarioletivo($id_calendarioletivo) {
		$this->id_calendarioletivo = $id_calendarioletivo;
	}
	
	/**
	 * @param int $id_dialetivo
	 */
	public function setId_dialetivo($id_dialetivo) {
		$this->id_dialetivo = $id_dialetivo;
	}
	
	/**
	 * @param int $id_tipodia
	 */
	public function setId_tipodia($id_tipodia) {
		$this->id_tipodia = $id_tipodia;
	}
	
	/**
	 * @param string $st_ocasiao
	 */
	public function setSt_nomeocasiao($st_nomeocasiao) {
		$this->st_nomeocasiao = $st_nomeocasiao;
	}

}

?>
<?php
/**
 * Classe para encapsular os dados de view da sala de aula
 * @author Dimas Sulz <dimassulz@gmail.com>
 * 
 * @package models
 * @subpackage to
 */
class PesquisarSalaDeAulaTO extends Ead1_TO_Dinamico {
	
	/**
	 * Atributo que contém a classe flex que será renderizada por exemplo numa situação de edição
	 * @var String
	 */
	public $st_classeflex = 'br.com.ead1.gestor2.view.conteudo.pedagogico.cadastrar.CadastrarSalaDeAula';
		
	public $id_saladeaula;
	public $st_saladeaula;
	public $id_situacao;
	public $id_tiposaladeaula;
	public $id_modalidadesaladeaula;
	public $id_entidade;
	public $id_periodoletivo;
	public $st_nomeprofessor;
	public $st_projetopedagogico;
	public $st_disciplina;
	public $st_situacao;
	public $st_nomeentidade;
	public $st_sexo;
	public $st_tiposaladeaula;
	public $dt_abertura;
	public $dt_encerramento;
    public $id_categoriasala;
    public $st_categoriasala;
	public $dt_comextensao;

    /**
     * @param mixed $id_categoriasala
     */
    public function setId_categoriasala($id_categoriasala)
    {
        $this->id_categoriasala = $id_categoriasala;
    }

    /**
     * @return mixed
     */
    public function getId_categoriasala()
    {
        return $this->id_categoriasala;
    }

    /**
     * @param mixed $st_categoriasala
     */
    public function setSt_categoriasala($st_categoriasala)
    {
        $this->st_categoriasala = $st_categoriasala;
    }

    /**
     * @return mixed
     */
    public function getSt_categoriasala()
    {
        return $this->st_categoriasala;
    }
	

	
	/**
	 * @return the $dt_abertura
	 */
	public function getDt_abertura() {
		return $this->dt_abertura;
	}

	/**
	 * @return the $dt_encerramento
	 */
	public function getDt_encerramento() {
		return $this->dt_encerramento;
	}

	/**
	 * @param date $dt_abertura
	 */
	public function setDt_abertura($dt_abertura) {
		$this->dt_abertura = $dt_abertura;
	}

	/**
	 * @param date $dt_encerramento
	 */
	public function setDt_encerramento($dt_encerramento) {
		$this->dt_encerramento = $dt_encerramento;
	}

	/**
	 * @return unknown
	 */
	public function getId_saladeaula() {
		return $this->id_saladeaula;
	}
	
	/**
	 * @param number $id_saladeaula
	 */
	public function setId_saladeaula($id_saladeaula) {
		$this->id_saladeaula = $id_saladeaula;
	}

	
	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $id_tiposaladeaula
	 */
	public function getId_tiposaladeaula() {
		return $this->id_tiposaladeaula;
	}

	
	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $id_periodoletivo
	 */
	public function getId_periodoletivo() {
		return $this->id_periodoletivo;
	}

	/**
	 * @return string $st_nomeprofessor
	 */
	public function getSt_nomeprofessor() {
		return $this->st_nomeprofessor;
	}

	/**
	 * @return the $st_projetopedagogico
	 */
	public function getSt_projetopedagogico() {
		return $this->st_projetopedagogico;
	}

	/**
	 * @return the $st_disciplina
	 */
	public function getSt_disciplina() {
		return $this->st_disciplina;
	}

	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}

	/**
	 * @return the $st_nomeentidade
	 */
	public function getSt_nomeentidade() {
		return $this->st_nomeentidade;
	}

	/**
	 * @return the $st_sexo
	 */
	public function getSt_sexo() {
		return $this->st_sexo;
	}

	/**
	 * @return the $st_tiposaladeaula
	 */
	public function getSt_tiposaladeaula() {
		return $this->st_tiposaladeaula;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param field_type $id_tiposaladeaula
	 */
	public function setId_tiposaladeaula($id_tiposaladeaula) {
		$this->id_tiposaladeaula = $id_tiposaladeaula;
	}

	

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $id_periodoletivo
	 */
	public function setId_periodoletivo($id_periodoletivo) {
		$this->id_periodoletivo = $id_periodoletivo;
	}

	/**
	 * @param field_type $st_nomeprofessor
	 */
	public function setSt_nomeprofessor($st_nomeprofessor) {
		$this->st_nomeprofessor = $st_nomeprofessor;
	}

	/**
	 * @param field_type $st_projetopedagogico
	 */
	public function setSt_projetopedagogico($st_projetopedagogico) {
		$this->st_projetopedagogico = $st_projetopedagogico;
	}

	/**
	 * @param field_type $st_disciplina
	 */
	public function setSt_disciplina($st_disciplina) {
		$this->st_disciplina = $st_disciplina;
	}

	/**
	 * @param field_type $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

	/**
	 * @param field_type $st_nomeentidade
	 */
	public function setSt_nomeentidade($st_nomeentidade) {
		$this->st_nomeentidade = $st_nomeentidade;
	}

	/**
	 * @param field_type $st_sexo
	 */
	public function setSt_sexo($st_sexo) {
		$this->st_sexo = $st_sexo;
	}

	/**
	 * @param field_type $st_tiposaladeaula
	 */
	public function setSt_tiposaladeaula($st_tiposaladeaula) {
		$this->st_tiposaladeaula = $st_tiposaladeaula;
	}
	/**
	 * @return the $bl_titular
	 */
	public function getBl_titular() {
		return $this->bl_titular;
	}

	/**
	 * @param field_type $bl_titular
	 */
	public function setBl_titular($bl_titular) {
		$this->bl_titular = $bl_titular;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_entidadepai() {
		return $this->id_entidadepai;
	}
	
	/**
	 * @param unknown_type $id_entidadepai
	 */
	public function setId_entidadepai($id_entidadepai) {
		$this->id_entidadepai = $id_entidadepai;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_saladeaula() {
		return $this->st_saladeaula;
	}
	
	/**
	 * @param unknown_type $st_saladeaula
	 */
	public function setSt_saladeaula($st_saladeaula) {
		$this->st_saladeaula = $st_saladeaula;
	}

	public function getDt_comextensao()
	{
		return $this->dt_comextensao;
	}

	public function setDt_comextensao($dt_comextensao)
	{
		$this->dt_comextensao = $dt_comextensao;
	}



}

?>
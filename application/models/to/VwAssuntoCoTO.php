<?php

/**
 * @author Elcio Mauro Guimarães
 * @package models
 * @subpackage to
 *
 */
class VwAssuntoCoTO extends Ead1_TO_Dinamico {
	
	public $id_assuntoco;
	public $st_assuntoco;
	public $bl_abertura;
	public $bl_ativo;
	public $dt_cadastro;
	public $id_assuntocopai;
	public $st_assuntocopai;
	public $id_situacao;
	public $st_situacao;
	public $id_tipoocorrencia;
	public $st_tipoocorrencia;
	public $id_entidade;
	public $id_entidadecadastro;
	public $id_funcao;
	public $id_textosistema;
	public $id_nucleoco;
	
	/**
	 * @return the $id_assuntoco
	 */
	public function getId_assuntoco() {
		return $this->id_assuntoco;
	}

	/**
	 * @param field_type $id_assuntoco
	 */
	public function setId_assuntoco($id_assuntoco) {
		$this->id_assuntoco = $id_assuntoco;
	}

	/**
	 * @return the $st_assuntoco
	 */
	public function getSt_assuntoco() {
		return $this->st_assuntoco;
	}

	/**
	 * @param field_type $st_assuntoco
	 */
	public function setSt_assuntoco($st_assuntoco) {
		$this->st_assuntoco = $st_assuntoco;
	}

	/**
	 * @return the $bl_abertura
	 */
	public function getBl_abertura() {
		return $this->bl_abertura;
	}

	/**
	 * @param field_type $bl_abertura
	 */
	public function setBl_abertura($bl_abertura) {
		$this->bl_abertura = $bl_abertura;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @return the $id_assuntocopai
	 */
	public function getId_assuntocopai() {
		return $this->id_assuntocopai;
	}

	/**
	 * @param field_type $id_assuntocopai
	 */
	public function setId_assuntocopai($id_assuntocopai) {
		$this->id_assuntocopai = $id_assuntocopai;
	}

	/**
	 * @return the $st_assuntocopai
	 */
	public function getSt_assuntocopai() {
		return $this->st_assuntocopai;
	}

	/**
	 * @param field_type $st_assuntocopai
	 */
	public function setSt_assuntocopai($st_assuntocopai) {
		$this->st_assuntocopai = $st_assuntocopai;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}

	/**
	 * @param field_type $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

	/**
	 * @return the $id_tipoocorrencia
	 */
	public function getId_tipoocorrencia() {
		return $this->id_tipoocorrencia;
	}

	/**
	 * @param field_type $id_tipoocorrencia
	 */
	public function setId_tipoocorrencia($id_tipoocorrencia) {
		$this->id_tipoocorrencia = $id_tipoocorrencia;
	}

	/**
	 * @return the $st_tipoocorrencia
	 */
	public function getSt_tipoocorrencia() {
		return $this->st_tipoocorrencia;
	}

	/**
	 * @param field_type $st_tipoocorrencia
	 */
	public function setSt_tipoocorrencia($st_tipoocorrencia) {
		$this->st_tipoocorrencia = $st_tipoocorrencia;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	/**
	 * @return the $id_entidadecadastro
	 */
	public function getId_entidadecadastro() {
		return $this->id_entidadecadastro;
	}

	/**
	 * @param field_type $id_entidadecadastro
	 */
	public function setId_entidadecadastro($id_entidadecadastro) {
		$this->id_entidadecadastro = $id_entidadecadastro;
	}
	/**
	 * @return the $id_funcao
	 */
	public function getId_funcao() {
		return $this->id_funcao;
	}

	/**
	 * @param field_type $id_funcao
	 */
	public function setId_funcao($id_funcao) {
		$this->id_funcao = $id_funcao;
	}
	/**
	 * @return the $id_textosistema
	 */
	public function getId_textosistema() {
		return $this->id_textosistema;
	}

	/**
	 * @param field_type $id_textosistema
	 */
	public function setId_textosistema($id_textosistema) {
		$this->id_textosistema = $id_textosistema;
	}
	/**
	 * @return the $id_nucleoco
	 */
	public function getId_nucleoco() {
		return $this->id_nucleoco;
	}

	/**
	 * @param field_type $id_nucleoco
	 */
	public function setId_nucleoco($id_nucleoco) {
		$this->id_nucleoco = $id_nucleoco;
	}





}

?>
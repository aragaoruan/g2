<?php
/**
 * Classe para encapsular os dados de contato de email e perfil da pessoa.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class ContatosEmailPessoaPerfilTO extends Ead1_TO_Dinamico {

	/**
	 * Atributo que verifica se é o email padrao da pessoa.
	 * @var int
	 */
	public $bl_padrao;
	
	/**
	 * Atributo que contém o id do perfil.
	 * @var int
	 */
	public $id_perfil;
	
	/**
	 * Atributo que contém o id do usuário.
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Atributo que contém o id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Atributo que contém o id do email.
	 * @var int
	 */
	public $id_email;
	
	/**
	 * Atributo que diz se o email está ativo ou não para aquele perfil
	 * @var Boolean
	 */
	public $bl_ativo;
	
	/**
	 * @return Boolean
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}
	
	/**
	 * @param Boolean $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}
	/**
	 * @return int
	 */
	public function getBl_padrao() {
		return $this->bl_padrao;
	}
	
	/**
	 * @return int
	 */
	public function getId_email() {
		return $this->id_email;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_perfil() {
		return $this->id_perfil;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}
	
	/**
	 * @param int $bl_padrao
	 */
	public function setBl_padrao($bl_padrao) {
		$this->bl_padrao = $bl_padrao;
	}
	
	/**
	 * @param int $id_email
	 */
	public function setId_email($id_email) {
		$this->id_email = $id_email;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_perfil
	 */
	public function setId_perfil($id_perfil) {
		$this->id_perfil = $id_perfil;
	}
	
	/**
	 * @param int $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

}

?>
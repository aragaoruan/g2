<?php
/**
 * Classe para encapsular os dados da view vw_materialprojetopedagogico.
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class VwMaterialProjetoPedagogicoTO extends Ead1_TO_Dinamico{
	public $id_itemmaterialprojeto;
	public $id_itemdematerial;
	public $id_usuariocadastro;
	public $dt_cadastro;
	public $id_projetopedagogico;
	public $st_projetopedagogico;
	public $st_tituloexibicao;
	
	/**
	 * @return the $id_itemmaterialprojeto
	 */
	public function getId_itemmaterialprojeto() {
		return $this->id_itemmaterialprojeto;
	}

	/**
	 * @return the $id_itemdematerial
	 */
	public function getId_itemdematerial() {
		return $this->id_itemdematerial;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @return the $st_projetopedagogico
	 */
	public function getSt_projetopedagogico() {
		return $this->st_projetopedagogico;
	}

	/**
	 * @return the $st_tituloexibicao
	 */
	public function getSt_tituloexibicao() {
		return $this->st_tituloexibicao;
	}

	/**
	 * @param field_type $id_itemmaterialprojeto
	 */
	public function setId_itemmaterialprojeto($id_itemmaterialprojeto) {
		$this->id_itemmaterialprojeto = $id_itemmaterialprojeto;
	}

	/**
	 * @param field_type $id_itemdematerial
	 */
	public function setId_itemdematerial($id_itemdematerial) {
		$this->id_itemdematerial = $id_itemdematerial;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @param field_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @param field_type $st_projetopedagogico
	 */
	public function setSt_projetopedagogico($st_projetopedagogico) {
		$this->st_projetopedagogico = $st_projetopedagogico;
	}

	/**
	 * @param field_type $st_tituloexibicao
	 */
	public function setSt_tituloexibicao($st_tituloexibicao) {
		$this->st_tituloexibicao = $st_tituloexibicao;
	}

}
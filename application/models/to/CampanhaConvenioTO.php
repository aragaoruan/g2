<?php
/**
 * Classe para encapsular os dados de relacionamento entre convênio e campanha comercial.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class CampanhaConvenioTO extends Ead1_TO_Dinamico {

	/**
	 * Id da campanha comercial.
	 * @var int
	 */
	public $id_campanhacomercial;
	
	/**
	 * Id do convênio.
	 * @var int
	 */
	public $id_convenio;
	
	/**
	 * @return int
	 */
	public function getId_campanhacomercial() {
		return $this->id_campanhacomercial;
	}
	
	/**
	 * @return int
	 */
	public function getId_convenio() {
		return $this->id_convenio;
	}
	
	/**
	 * @param int $id_campanhacomercial
	 */
	public function setId_campanhacomercial($id_campanhacomercial) {
		$this->id_campanhacomercial = $id_campanhacomercial;
	}
	
	/**
	 * @param int $id_convenio
	 */
	public function setId_convenio($id_convenio) {
		$this->id_convenio = $id_convenio;
	}

}

?>
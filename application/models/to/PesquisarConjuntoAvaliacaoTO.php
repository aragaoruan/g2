<?php
/**
 * Classe para encapsular os dados de pesquisa de conjunto de avaliação.
 * @author Dimas Sulz <dimassulz@gmail.com>
 * 
 * @package models
 * @subpackage to
 */
class PesquisarConjuntoAvaliacaoTO extends Ead1_TO_Dinamico {

	public $id_avaliacaoconjunto;
	
	public $st_avaliacaoconjunto;
	
	public $id_situacao;
	
	public $st_situacao;
	
	public $id_tipoprova;
	
	public $st_tipoprova;
	
	public $id_tipocalculoavaliacao;
	
	public $st_tipocalculoavaliacao;
	
	public $id_entidade;
	
	/**
	 * @return unknown
	 */
	public function getId_avaliacaoconjunto() {
		return $this->id_avaliacaoconjunto;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_tipocalculoavaliacao() {
		return $this->id_tipocalculoavaliacao;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_tipoprova() {
		return $this->id_tipoprova;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_avaliacaoconjunto() {
		return $this->st_avaliacaoconjunto;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_tipocalculoavaliacao() {
		return $this->st_tipocalculoavaliacao;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_tipoprova() {
		return $this->st_tipoprova;
	}
	
	/**
	 * @param unknown_type $id_avaliacaoconjunto
	 */
	public function setId_avaliacaoconjunto($id_avaliacaoconjunto) {
		$this->id_avaliacaoconjunto = $id_avaliacaoconjunto;
	}
	
	/**
	 * @param unknown_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param unknown_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}
	
	/**
	 * @param unknown_type $id_tipocalculoavaliacao
	 */
	public function setId_tipocalculoavaliacao($id_tipocalculoavaliacao) {
		$this->id_tipocalculoavaliacao = $id_tipocalculoavaliacao;
	}
	
	/**
	 * @param unknown_type $id_tipoprova
	 */
	public function setId_tipoprova($id_tipoprova) {
		$this->id_tipoprova = $id_tipoprova;
	}
	
	/**
	 * @param unknown_type $st_avaliacaoconjunto
	 */
	public function setSt_avaliacaoconjunto($st_avaliacaoconjunto) {
		$this->st_avaliacaoconjunto = $st_avaliacaoconjunto;
	}
	
	/**
	 * @param unknown_type $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}
	
	/**
	 * @param unknown_type $st_tipocalculoavaliacao
	 */
	public function setSt_tipocalculoavaliacao($st_tipocalculoavaliacao) {
		$this->st_tipocalculoavaliacao = $st_tipocalculoavaliacao;
	}
	
	/**
	 * @param unknown_type $st_tipoprova
	 */
	public function setSt_tipoprova($st_tipoprova) {
		$this->st_tipoprova = $st_tipoprova;
	}

}

?>
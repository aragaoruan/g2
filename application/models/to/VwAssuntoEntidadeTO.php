<?php

/**
 * VwAssuntoEntidade
 *
 * @author Rafael Bruno <rafaelbruno.ti@gmail.com>
 */
class VwAssuntoEntidadeTO extends Ead1_TO_Dinamico {

    /**
     * @var type varchar(250)
     */
    public $st_nomeentidade;

    /**
     * @var type varchar(200)
     */
    public $st_assuntopai;

    /**
     * @var type int
     */
    public $id_assuntopai;

    /**
     * @var type varchar(200)
     */
    public $st_subassunto;

    /**
     * @var type int
     */
    public $id_subassunto;

    /**
     * @var type int not null
     */
    public $id_assunto;

    /**
     * @var type int
     */
    public $id_entidade;

    /**
     * @var type int not null
     */
    public $id_entidadecadastro;
    
    /**
     * @var type int not null
     */
    public $id_assuntoentidadeco;

    /**
     * Retorna texto do registro da entidade
     * @return type
     */
    public function getSt_nomeentidade() {
        return $this->st_nomeentidade;
    }

    /**
     * @param type $st_nomeentidade
     */
    public function setSt_nomeentidade($st_nomeentidade) {
        $this->st_nomeentidade = $st_nomeentidade;
    }

    /**
     * Retorna texto do assunto pai
     * @return type string
     */
    public function getSt_assuntopai() {
        return $this->st_assuntopai;
    }

    /**
     * @param type $st_assuntopai
     */
    public function setSt_assuntopai($st_assuntopai) {
        $this->st_assuntopai = $st_assuntopai;
    }

    /**
     * Retorna id do assunto pai
     * @return type int
     */
    public function getId_assuntopai() {
        return $this->id_assuntopai;
    }

    /**
     * @param type $id_assuntopai
     */
    public function setId_assuntopai($id_assuntopai) {
        $this->id_assuntopai = $id_assuntopai;
    }

    /**
     * Retorna texto subassunto
     * @return type string
     */
    public function getSt_subassunto() {
        return $this->st_subassunto;
    }

    /**
     * @param type $st_subassunto
     */
    public function setSt_subassunto($st_subassunto) {
        $this->st_subassunto = $st_subassunto;
    }

    /**
     * Retorna id subassunto
     * @return type int
     */
    public function getId_subassunto() {
        return $this->id_subassunto;
    }

    /**
     * @param type $id_subassunto
     */
    public function setId_subassunto($id_subassunto) {
        $this->id_subassunto = $id_subassunto;
    }

    /**
     * Retorna id assunto
     * @return type int
     */
    public function getId_assunto() {
        return $this->id_assunto;
    }

    /**
     * @param type $id_assunto
     */
    public function setId_assunto($id_assunto) {
        $this->id_assunto = $id_assunto;
    }

    /**
     * Retorna id entidade
     * @return type int
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @param type $id_entidade
     */
    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    /**
     * Retorna id entidade cadastro
     * @return type int
     */
    public function getId_entidadecadastro() {
        return $this->id_entidadecadastro;
    }

    /**
     * @param type $id_entidadecadastro
     */
    public function setId_entidadecadastro($id_entidadecadastro) {
        $this->id_entidadecadastro = $id_entidadecadastro;
    }

    /**
     * Retorna id_assuntoentidadeco
     * @return type int
     */
    public function getId_assuntoentidadeco(){
        return $this->id_assuntoentidadeco;
    }

    /**
     * @param type $id_assuntoentidadeco
     */
    public function setId_assuntoentidadeco($id_assuntoentidadeco){
        $this->id_assuntoentidadeco = $id_assuntoentidadeco;
    }


}
<?php

/**
 * Classe para encapsular da tabela
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class VwMatriculaTO extends Ead1_TO_Dinamico
{

    public $id_usuario;
    public $st_nomecompleto;
    public $st_cpf;
    public $dt_nascimento;
    public $st_nomepai;
    public $st_nomemae;
    public $id_matricula;
    public $st_projetopedagogico;
    public $id_situacao;
    public $st_situacao;
    public $id_evolucao;
    public $st_evolucao;
    public $dt_concluinte;
    public $id_entidadematriz;
    public $st_entidadematriz;
    public $id_entidadematricula;
    public $st_entidadematricula;
    public $id_entidadeatendimento;
    public $st_entidadeatendimento;
    public $bl_ativo;
    public $id_contrato;
    public $id_projetopedagogico;
    public $dt_termino;
    public $dt_cadastro;
    public $dt_inicio;
    public $id_turma;
    public $st_turma;
    public $dt_inicioturma;
    public $st_email;
    public $st_telefone;
    public $bl_institucional;
    public $dt_terminoturma;
    public $st_urlacesso;
    public $st_urlportal;
    public $st_urlportalmatricula;
    public $bl_documentacao;
    public $st_codcertificacao;
    public $st_siglaentidade;
    public $dt_cadastrocertificacao;
    public $dt_enviocertificado;
    public $dt_retornocertificadora;
    public $dt_envioaluno;
    public $st_uploadcontrato;
    public $bl_academico;
    public $id_vendaproduto;

    /**
     * @return mixed
     */
    public function getst_urlportal()
    {
        return $this->st_urlportal;
    }

    /**
     * @param mixed $st_urlportal
     */
    public function setst_urlportal($st_urlportal)
    {
        $this->st_urlportal = $st_urlportal;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getst_urlportalmatricula()
    {
        return $this->st_urlportalmatricula;
    }

    /**
     * @param mixed $st_urlportalmatricula
     */
    public function setst_urlportalmatricula($st_urlportalmatricula)
    {
        $this->st_urlportalmatricula = $st_urlportalmatricula;
        return $this;

    }




    /**
     * @return the $bl_documentacao
     */
    public function getBl_documentacao()
    {
        return $this->bl_documentacao;
    }

    /**
     * @param field_type $bl_documentacao
     */
    public function setBl_documentacao($bl_documentacao)
    {
        $this->bl_documentacao = $bl_documentacao;
    }

    public function setSt_Email($st_email)
    {
        $this->st_email = $st_email;
    }

    public function getSt_Email()
    {
        return $this->st_email;
    }

    public function setDt_Terminoturma($dt_terminoturma)
    {
        $this->dt_terminoturma = $dt_terminoturma;
    }

    public function getDt_Terminoturma()
    {
        return $this->dt_terminoturma;
    }

    public function setSt_Telefone($st_telefone)
    {
        $this->st_telefone = $st_telefone;
    }

    public function getSt_Telefone()
    {
        return $this->st_telefone;
    }

    public function setBl_Institucional($bl_institucional)
    {
        $this->bl_institucional = $bl_institucional;
    }

    public function getBl_Institucional()
    {
        return $this->bl_institucional;
    }


    /**
     * @return the $id_usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @return the $st_nomecompleto
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @return the $st_cpf
     */
    public function getSt_cpf()
    {
        return $this->st_cpf;
    }

    /**
     * @return the $dt_nascimento
     */
    public function getDt_nascimento()
    {
        return $this->dt_nascimento;
    }

    /**
     * @return the $st_nomepai
     */
    public function getSt_nomepai()
    {
        return $this->st_nomepai;
    }

    /**
     * @return the $st_nomemae
     */
    public function getSt_nomemae()
    {
        return $this->st_nomemae;
    }

    /**
     * @return the $id_matricula
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @return the $st_projetopedagogico
     */
    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    /**
     * @return the $id_situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @return the $st_situacao
     */
    public function getSt_situacao()
    {
        return $this->st_situacao;
    }

    /**
     * @return the $id_evolucao
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @return the $st_evolucao
     */
    public function getSt_evolucao()
    {
        return $this->st_evolucao;
    }

    /**
     * @return the $dt_concluinte
     */
    public function getDt_concluinte()
    {
        return $this->dt_concluinte;
    }

    /**
     * @return the $id_entidadematriz
     */
    public function getId_entidadematriz()
    {
        return $this->id_entidadematriz;
    }

    /**
     * @return the $st_entidadematriz
     */
    public function getSt_entidadematriz()
    {
        return $this->st_entidadematriz;
    }

    /**
     * @return the $id_entidadematricula
     */
    public function getId_entidadematricula()
    {
        return $this->id_entidadematricula;
    }

    /**
     * @return the $st_entidadematricula
     */
    public function getSt_entidadematricula()
    {
        return $this->st_entidadematricula;
    }

    /**
     * @return the $id_entidadeatendimento
     */
    public function getId_entidadeatendimento()
    {
        return $this->id_entidadeatendimento;
    }

    /**
     * @return the $st_entidadeatendimento
     */
    public function getSt_entidadeatendimento()
    {
        return $this->st_entidadeatendimento;
    }

    /**
     * @return the $bl_ativo
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @return the $id_contrato
     */
    public function getId_contrato()
    {
        return $this->id_contrato;
    }

    /**
     * @return the $id_projetopedagogico
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @return the $dt_termino
     */
    public function getDt_termino()
    {
        return $this->dt_termino;
    }


    /**
     * @param field_type $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @param field_type $st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
    }

    /**
     * @param field_type $st_cpf
     */
    public function setSt_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
    }

    /**
     * @param field_type $dt_nascimento
     */
    public function setDt_nascimento($dt_nascimento)
    {
        $this->dt_nascimento = $dt_nascimento;
    }

    /**
     * @param field_type $st_nomepai
     */
    public function setSt_nomepai($st_nomepai)
    {
        $this->st_nomepai = $st_nomepai;
    }

    /**
     * @param field_type $st_nomemae
     */
    public function setSt_nomemae($st_nomemae)
    {
        $this->st_nomemae = $st_nomemae;
    }

    /**
     * @param field_type $id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @param field_type $st_projetopedagogico
     */
    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
    }

    /**
     * @param field_type $id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    /**
     * @param field_type $st_situacao
     */
    public function setSt_situacao($st_situacao)
    {
        $this->st_situacao = $st_situacao;
    }

    /**
     * @param field_type $id_evolucao
     */
    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
    }

    /**
     * @param field_type $st_evolucao
     */
    public function setSt_evolucao($st_evolucao)
    {
        $this->st_evolucao = $st_evolucao;
    }

    /**
     * @param field_type $dt_concluinte
     */
    public function setDt_concluinte($dt_concluinte)
    {
        $this->dt_concluinte = $dt_concluinte;
    }

    /**
     * @param field_type $id_entidadematriz
     */
    public function setId_entidadematriz($id_entidadematriz)
    {
        $this->id_entidadematriz = $id_entidadematriz;
    }

    /**
     * @param field_type $st_entidadematriz
     */
    public function setSt_entidadematriz($st_entidadematriz)
    {
        $this->st_entidadematriz = $st_entidadematriz;
    }

    /**
     * @param field_type $id_entidadematricula
     */
    public function setId_entidadematricula($id_entidadematricula)
    {
        $this->id_entidadematricula = $id_entidadematricula;
    }

    /**
     * @param field_type $st_entidadematricula
     */
    public function setSt_entidadematricula($st_entidadematricula)
    {
        $this->st_entidadematricula = $st_entidadematricula;
    }

    /**
     * @param field_type $id_entidadeatendimento
     */
    public function setId_entidadeatendimento($id_entidadeatendimento)
    {
        $this->id_entidadeatendimento = $id_entidadeatendimento;
    }

    /**
     * @param field_type $st_entidadeatendimento
     */
    public function setSt_entidadeatendimento($st_entidadeatendimento)
    {
        $this->st_entidadeatendimento = $st_entidadeatendimento;
    }

    /**
     * @param field_type $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @param field_type $id_contrato
     */
    public function setId_contrato($id_contrato)
    {
        $this->id_contrato = $id_contrato;
    }

    /**
     * @param field_type $id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    /**
     * @param field_type $dt_termino
     */
    public function setDt_termino($dt_termino)
    {
        $this->dt_termino = $dt_termino;
    }

    /**
     * @return the $dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param field_type $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return the $dt_inicio
     */
    public function getDt_inicio()
    {
        return $this->dt_inicio;
    }

    /**
     * @param field_type $dt_inicio
     */
    public function setDt_inicio($dt_inicio)
    {
        $this->dt_inicio = $dt_inicio;
    }

    /**
     * @return the $id_turma
     */
    public function getId_turma()
    {
        return $this->id_turma;
    }

    /**
     * @param field_type $id_turma
     */
    public function setId_turma($id_turma)
    {
        $this->id_turma = $id_turma;
    }

    /**
     * @return the $st_turma
     */
    public function getSt_turma()
    {
        return $this->st_turma;
    }

    /**
     * @param field_type $st_turma
     */
    public function setSt_turma($st_turma)
    {
        $this->st_turma = $st_turma;
    }

    /**
     * @return the $dt_inicioturma
     */
    public function getDt_inicioturma()
    {
        return $this->dt_inicioturma;
    }

    /**
     * @param field_type $dt_inicioturma
     */
    public function setDt_inicioturma($dt_inicioturma)
    {
        $this->dt_inicioturma = $dt_inicioturma;
    }

    public function setSt_urlacesso($st_urlacesso)
    {
        $this->st_urlacesso = $st_urlacesso;
    }

    public function getSt_urlacesso()
    {
        return $this->st_urlacesso;
    }

    /**
     * @return string st_codcertificacao
     */
    public function getSt_codcertificacao()
    {
        return $this->st_codcertificacao;
    }

    /**
     * @param st_codcertificacao
     */
    public function setSt_codcertificacao($st_codcertificacao)
    {
        $this->st_codcertificacao = $st_codcertificacao;
    }

    public function getSt_siglaentidade()
    {
        return $this->st_siglaentidade;
    }

    public function setSt_siglaentidade($st_siglaentidade)
    {
        $this->st_siglaentidade = $st_siglaentidade;
    }

    public function getDt_cadastrocertificacao()
    {
        return $this->dt_cadastrocertificacao;
    }

    public function setDt_cadastrocertificacao($dt_cadastrocertificacao)
    {
        $this->dt_cadastrocertificacao = $dt_cadastrocertificacao;
    }

    public function getDt_enviocertificado()
    {
        return $this->dt_enviocertificado;
    }

    public function setDt_enviocertificado($dt_enviocertificado)
    {
        $this->dt_enviocertificado = $dt_enviocertificado;
    }

    public function getDt_retornocertificadora()
    {
        return $this->dt_retornocertificadora;
    }

    public function setDt_retornocertificadora($dt_retornocertificadora)
    {
        $this->dt_retornocertificadora = $dt_retornocertificadora;
    }

    public function getDt_envioaluno()
    {
        return $this->dt_envioaluno;
    }

    public function setDt_envioaluno($dt_envioaluno)
    {
        $this->dt_envioaluno = $dt_envioaluno;
    }

    public function getSt_uploadcontrato()
    {
        return $this->st_uploadcontrato;
    }

    public function setSt_uploadcontrato($st_uploadcontrato)
    {
        $this->st_uploadcontrato = $st_uploadcontrato;
    }

    public function getBl_academico()
    {
        return $this->bl_academico;
    }

    public function setBl_academico($bl_academico)
    {
        $this->bl_academico = $bl_academico;
    }

    public function getId_vendaproduto()
    {
        return $this->id_vendaproduto;
    }

    public function setId_vendaproduto($id_vendaproduto)
    {
        $this->id_vendaproduto = $id_vendaproduto;
    }

}
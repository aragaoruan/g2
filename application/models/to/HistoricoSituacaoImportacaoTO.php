<?php
/**
 * Classe de valor representativa da tatbela tb_historicosituacaoimportacao
 * @author Arthur Cláudio de Almeida Pereira <arthur.almeida@ead1.com.br>
 *
 */
class HistoricoSituacaoImportacaoTO extends Ead1_TO_Dinamico {
	
	public $id_historicosituacaoimportacao;
	public $nu_codsituacao;
	public $nu_codmatriculaorigem;
	public $dt_cadastrohistorico;
	public $id_sistemaimportacao;
	public $id_entidadeimportado;

	
	public function setId_historicosituacaoimportacao( $idHistoricoSituacaoImportacao ){
		$this->id_historicosituacaoimportacao = $idHistoricoSituacaoImportacao;
	}
	
	public function setNu_codsituacao( $nuCodSituacao ){
		$this->nu_codsituacao = $nuCodSituacao;
	}
	
	public function setNu_codmatriculaorigem( $nuCodMatriculaOrigem ){
		$this->nu_codmatriculaorigem	= $nuCodMatriculaOrigem;
	}
	
	public function setDt_cadastrohistorico( $dtCadastro ){
		$this->dt_cadastrohistorico	= $dtCadastro;
	}
	
	public function getId_historicosituacaoimportacao(){
		return $this->id_historicosituacaoimportacao;
	}
	
	public function setId_sistemaimportacao( $id_sistemaimportacao ){
		$this->id_sistemaimportacao	= $id_sistemaimportacao;
	}
	
	public function getId_sistemaimportacao(){
		return $this->id_sistemaimportacao;
	}
	
	public function setId_entidadeimportado( $id_entidadeimportado ){
		$this->id_entidadeimportado	= $id_entidadeimportado;
	}
	
	public function getId_entidadeimportado(){
		return $this->id_entidadeimportado;
	}
	
	public function getNu_codsituacao(){
		return $this->nu_codsituacao;
	}
	
	public function getNu_codmatriculaorigem(){
		return $this->nu_codmatriculaorigem;
	}
	
	public function getDt_cadastrohistorico(){
		return $this->dt_cadastrohistorico;
	}
}
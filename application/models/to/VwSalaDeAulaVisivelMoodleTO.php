<?php
/**
 * Classe para encapsular os dados de view de tornar visivel salas de aula do moodle
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @package models
 * @subpackage to
 */
class VwSalaDeAulaVisivelMoodleTO extends Ead1_TO_Dinamico {

        public $id_saladeaula;
        public $st_saladeaula;
        public $dt_abertura;
        public $id_saladeaulaintegracao;
        public $id_sistema;
        public $id_entidade;


    /**
     * @param mixed $dt_abertura
     */
    public function setDt_abertura($dt_abertura)
    {
        $this->dt_abertura = $dt_abertura;
    }

    /**
     * @return mixed
     */
    public function getDt_abertura()
    {
        return $this->dt_abertura;
    }

    /**
     * @param mixed $id_saladeaula
     */
    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
    }

    /**
     * @return mixed
     */
    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param mixed $id_saladeaulaintegracao
     */
    public function setId_saladeaulaintegracao($id_saladeaulaintegracao)
    {
        $this->id_saladeaulaintegracao = $id_saladeaulaintegracao;
    }

    /**
     * @return mixed
     */
    public function getId_saladeaulaintegracao()
    {
        return $this->id_saladeaulaintegracao;
    }

    /**
     * @param mixed $id_sistema
     */
    public function setId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
    }

    /**
     * @return mixed
     */
    public function getId_sistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param mixed $st_saladeaula
     */
    public function setSt_saladeaula($st_saladeaula)
    {
        $this->st_saladeaula = $st_saladeaula;
    }

    /**
     * @return mixed
     */
    public function get_stsaladeaula()
    {
        return $this->st_saladeaula;
    }


    /**
     * @param mixed $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return mixed
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }
}
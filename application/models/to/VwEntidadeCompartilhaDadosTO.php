<?php
/**
 * Classe para encapsular os dados da view de classe para entidade.
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @package models
 * @subpackage to
 */
class VwEntidadeCompartilhaDadosTO extends Ead1_TO_Dinamico {

    public $st_nomeentidade;
    public $id_entidadedestino;
    public $id_entidadeorigem;
    public $id_tipodados;

    /**
     * @return the $st_nomeentidade
     */
    public function getSt_nomeentidade(){
        return $this->st_nomeentidade;
    }

    /**
     * @return the $id_entidadedestino
     */
    public function getId_entidadedestino(){
        return $this->id_entidadedestino;
    }

    /**
     * @return the $id_entidadeorigem
     */
    public function getId_entidadeorigem(){
        return $this->id_entidadeorigem;
    }

    /**
     * @return the $id_tipodados
     */
    public function getId_tipodados(){
        return $this->id_tipodados;
    }


    /**
     * @param field_type $st_nomeentidade
     */
    public function setSt_nomeentidade($value){
        $this->st_nomeentidade = $value;
    }

    /**
     * @param field_type $id_entidadedestino
     */
    public function setId_entidadedestino($value){
        $this->id_entidadedestino = $value;
    }

    /**
     * @param field_type $id_entidadeorigem
     */
    public function setId_entidadeorigem($value){
        $this->id_entidadeorigem = $value;
    }

    /**
     * @param field_type $id_tipodados
     */
    public function setId_tipodados($value){
        $this->id_tipodados = $value;
    }
}
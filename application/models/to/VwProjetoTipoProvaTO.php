<?php
/**
 * Classe representativa da view vw_projetotipoprova
 * @author edermariano
 *
 * @package models
 * @subpackage to
 */
class VwProjetoTipoProvaTO extends Ead1_TO_Dinamico {

	/**
	 * id do resultado da tabela tb_projetotipoprova
	 * @var int
	 */
	public $id_tipoprovaresult;
	
	/**
	 * 
	 * id do tipo prova
	 * @var int
	 */
	public $id_tipoprova;
	
	/**
	 * 
	 * string do tipo de prova
	 * @var string
	 */
	public $st_tipoprova;
	
	/**
	 * 
	 * descricao do tipo de prova
	 * @var string
	 */
	public $st_descricao;

	/**
	 * 
	 * id do projeto pedagogico
	 * @var int
	 */
	public $id_projetopedagogico;
	
	/**
	 * 
	 * Número taxa da prova
	 * @var int
	 */
	public $nu_taxa;
	
	/**
	 * @return the $id_tipoprovaresult
	 */
	public function getId_tipoprovaresult() {
		return $this->id_tipoprovaresult;
	}

	/**
	 * @return the $id_tipoprova
	 */
	public function getId_tipoprova() {
		return $this->id_tipoprova;
	}

	/**
	 * @return the $st_tipoprova
	 */
	public function getSt_tipoprova() {
		return $this->st_tipoprova;
	}

	/**
	 * @return the $st_descricao
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}

	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @return the $nu_taxa
	 */
	public function getNu_taxa() {
		return $this->nu_taxa;
	}

	/**
	 * @param int $id_tipoprovaresult
	 */
	public function setId_tipoprovaresult($id_tipoprovaresult) {
		$this->id_tipoprovaresult = $id_tipoprovaresult;
	}

	/**
	 * @param int $id_tipoprova
	 */
	public function setId_tipoprova($id_tipoprova) {
		$this->id_tipoprova = $id_tipoprova;
	}

	/**
	 * @param string $st_tipoprova
	 */
	public function setSt_tipoprova($st_tipoprova) {
		$this->st_tipoprova = $st_tipoprova;
	}

	/**
	 * @param string $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}

	/**
	 * @param int $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @param int $nu_taxa
	 */
	public function setNu_taxa($nu_taxa) {
		$this->nu_taxa = $nu_taxa;
	}


}

?>
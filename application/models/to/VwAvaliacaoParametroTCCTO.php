<?php
/**
 * Classe para encapsular os dados da Vw_AvaliacaoParametrotcc.
 * @author Elcio Mauro Guimarães
 * 
 * @package models
 * @subpackage to
 */
class VwAvaliacaoParametroTCCTO extends Ead1_TO_Dinamico{
	
	public $id_matricula;
	public $id_saladeaula;
	public $id_disciplina;
	public $st_nota;
	public $id_avaliacao;
	public $id_avaliacaoaluno;
	public $id_avaliacaoconjuntoreferencia;
	public $nu_notamax;
	public $bl_ativoparametro;
	public $dt_avaliacao;
	public $st_parametrotcc;
	public $nu_notamaxparam;
	public $nu_notaparametro;
	public $id_parametrotcc;
	public $nu_peso;
	
	
	/**
	 * @return the $id_avaliacaoaluno
	 */
	public function getId_avaliacaoaluno() {
		return $this->id_avaliacaoaluno;
	}

	/**
	 * @param field_type $id_avaliacaoaluno
	 */
	public function setId_avaliacaoaluno($id_avaliacaoaluno) {
		$this->id_avaliacaoaluno = $id_avaliacaoaluno;
	}

	/**
	 * @return the $nu_peso
	 */
	public function getNu_peso() {
		return $this->nu_peso;
	}

	/**
	 * @param field_type $nu_peso
	 */
	public function setNu_peso($nu_peso) {
		$this->nu_peso = $nu_peso;
	}

	/**
	 * @return the $id_parametrotcc
	 */
	public function getId_parametrotcc() {
		return $this->id_parametrotcc;
	}

	/**
	 * @param field_type $id_parametrotcc
	 */
	public function setId_parametrotcc($id_parametrotcc) {
		$this->id_parametrotcc = $id_parametrotcc;
	}

	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}

	/**
	 * @return the $id_saladeaula
	 */
	public function getId_saladeaula() {
		return $this->id_saladeaula;
	}

	/**
	 * @return the $id_disciplina
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}

	/**
	 * @return the $st_nota
	 */
	public function getSt_nota() {
		return $this->st_nota;
	}

	/**
	 * @return the $id_avaliacao
	 */
	public function getId_avaliacao() {
		return $this->id_avaliacao;
	}

	/**
	 * @return the $id_avaliacaoconjuntoreferencia
	 */
	public function getId_avaliacaoconjuntoreferencia() {
		return $this->id_avaliacaoconjuntoreferencia;
	}

	/**
	 * @return the $nu_notamax
	 */
	public function getNu_notamax() {
		return  $this->nu_notamax;
		
		
	}

	/**
	 * @return the $bl_ativoparametro
	 */
	public function getBl_ativoparametro() {
		return $this->bl_ativoparametro;
	}

	/**
	 * @return the $dt_avaliacao
	 */
	public function getDt_avaliacao() {
		return $this->dt_avaliacao;
	}

	/**
	 * @return the $st_parametrotcc
	 */
	public function getSt_parametrotcc() {
		return $this->st_parametrotcc;
	}

	/**
	 * @return the $nu_notamaxparam
	 */
	public function getNu_notamaxparam() {
		return $this->nu_notamaxparam;
	}

	/**
	 * @return the $nu_notaparametro
	 */
	public function getNu_notaparametro() {
		return $this->nu_notaparametro;
	}

	/**
	 * @param field_type $id_matricula
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}

	/**
	 * @param field_type $id_saladeaula
	 */
	public function setId_saladeaula($id_saladeaula) {
		$this->id_saladeaula = $id_saladeaula;
	}

	/**
	 * @param field_type $id_disciplina
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}

	/**
	 * @param field_type $st_nota
	 */
	public function setSt_nota($st_nota) {
		$this->st_nota = $st_nota;
	}

	/**
	 * @param field_type $id_avaliacao
	 */
	public function setId_avaliacao($id_avaliacao) {
		$this->id_avaliacao = $id_avaliacao;
	}

	/**
	 * @param field_type $id_avaliacaoconjuntoreferencia
	 */
	public function setId_avaliacaoconjuntoreferencia($id_avaliacaoconjuntoreferencia) {
		$this->id_avaliacaoconjuntoreferencia = $id_avaliacaoconjuntoreferencia;
	}

	/**
	 * @param field_type $nu_notamax
	 */
	public function setNu_notamax($nu_notamax) {
		$this->nu_notamax = $nu_notamax;
	}

	/**
	 * @param field_type $bl_ativoparametro
	 */
	public function setBl_ativoparametro($bl_ativoparametro) {
		$this->bl_ativoparametro = $bl_ativoparametro;
	}

	/**
	 * @param field_type $dt_avaliacao
	 */
	public function setDt_avaliacao($dt_avaliacao) {
		$this->dt_avaliacao = $dt_avaliacao;
	}

	/**
	 * @param field_type $st_parametrotcc
	 */
	public function setSt_parametrotcc($st_parametrotcc) {
		$this->st_parametrotcc = $st_parametrotcc;
	}

	/**
	 * @param field_type $nu_notamaxparam
	 */
	public function setNu_notamaxparam($nu_notamaxparam) {
		$this->nu_notamaxparam = $nu_notamaxparam;
	}

	/**
	 * @param field_type $nu_notaparametro
	 */
	public function setNu_notaparametro($nu_notaparametro) {
		$this->nu_notaparametro = $nu_notaparametro;
	}

	
}
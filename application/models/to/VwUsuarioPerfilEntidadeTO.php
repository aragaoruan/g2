<?php
/**
 * Classe para encapsular os dados da view de perfil do usuário na entidade.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @since 14/10/2010
 * 
 * @package models
 * @subpackage to
 */
class VwUsuarioPerfilEntidadeTO extends Ead1_TO_Dinamico {

	/**
	 * Contem o id do usuario
	 * @var int
	 */
	public $id_usuario;

	/**
	 * Contem o nome completo do usuario
	 * @var string
	 */
	public $st_nomecompleto;

	/**
	 * Contem o id do perfil
	 * @var int
	 */
	public $id_perfil;

	/**
	 * Contem o nome do perfil
	 * @var string
	 */
	public $st_nomeperfil;

	/**
	 * Contem o nome de exibição do perfil da entidade
	 * @var string
	 */
	public $st_perfilalias;

	/**
	 * Contem a data de inicio do perfil
	 * @var string
	 */
	public $dt_inicio;

	/**
	 * Contem a data de termino do perfil
	 * @var string
	 */
	public $dt_termino;

	/**
	 * Contem o id da situacao do perfil da pessoa
	 * @var int
	 */
	public $id_situacaoperfil;

	/**
	 * Contem o nome da situacao do perfil da pessoa
	 * @var string
	 */
	public $st_situacaoperfil;

	/**
	 * Contem o id da entidade
	 * @var int
	 */
	public $id_entidade;

	/**
	 * Contem o nome da entidade
	 * @var string
	 */
	public $st_nomeentidade;
	
	/**
	 * Id da entidade cadastro
	 * @var int
	 */
	public $id_entidadecadastro;
	
	/**
	 * id do perfil pedagogico
	 * @var int
	 */
	public $id_perfilpedagogico;
	
	/**
	 * Id do sistema.
	 * @var int
	 */
	public $id_sistema;
	
	/**
	 * Caminho da imagem de logo.
	 * @var string
	 */
	public $st_urlimglogo;
	
	/**
	 * CPF
	 * @var string
	 */
	public $st_cpf;

	/**
	 * CPF
	 * @var string
	 */
	public $st_token;

	/**
	 * @return string
	 */
	public function getst_token()
	{
		return $this->st_token;
	}

	/**
	 * @param string $st_token
	 */
	public function setst_token($st_token)
	{
		$this->st_token = $st_token;
		return $this;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $st_nomecompleto
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}

	/**
	 * @return the $id_perfil
	 */
	public function getId_perfil() {
		return $this->id_perfil;
	}

	/**
	 * @return the $st_nomeperfil
	 */
	public function getSt_nomeperfil() {
		return $this->st_nomeperfil;
	}

	/**
	 * @return the $st_perfilalias
	 */
	public function getSt_perfilalias() {
		return $this->st_perfilalias;
	}

	/**
	 * @return the $dt_inicio
	 */
	public function getDt_inicio() {
		return $this->dt_inicio;
	}

	/**
	 * @return the $dt_termino
	 */
	public function getDt_termino() {
		return $this->dt_termino;
	}

	/**
	 * @return the $id_situacaoperfil
	 */
	public function getId_situacaoperfil() {
		return $this->id_situacaoperfil;
	}

	/**
	 * @return the $st_situacaoperfil
	 */
	public function getSt_situacaoperfil() {
		return $this->st_situacaoperfil;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidadecadastro() {
		return $this->id_entidadecadastro;
	}

	/**
	 * @return the $id_sistema
	 */
	public function getId_sistema() {
		return $this->id_sistema;
	}
	
	/**
	 * @return the $id_perfilpedagogico
	 */
	public function getId_perfilpedagogico() {
		return $this->id_perfilpedagogico;
	}

	/**
	 * @return the $st_nomeentidade
	 */
	public function getSt_nomeentidade() {
		return $this->st_nomeentidade;
	}
	
	/**
	 * @return the $st_urlimglogo
	 */
	public function getSt_urlimglogo() {
		return $this->st_nomeentidade;
	}

	/**
	 * @param $id_usuario the $id_usuario to set
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param $st_nomecompleto the $st_nomecompleto to set
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @param $id_perfil the $id_perfil to set
	 */
	public function setId_perfil($id_perfil) {
		$this->id_perfil = $id_perfil;
	}

	/**
	 * @param $st_nomeperfil the $st_nomeperfil to set
	 */
	public function setSt_nomeperfil($st_nomeperfil) {
		$this->st_nomeperfil = $st_nomeperfil;
	}

	/**
	 * @param $st_perfilalias the $st_perfilalias to set
	 */
	public function setSt_perfilalias($st_perfilalias) {
		$this->st_perfilalias = $st_perfilalias;
	}

	/**
	 * @param $dt_inicio the $dt_inicio to set
	 */
	public function setDt_inicio($dt_inicio) {
		$this->dt_inicio = $dt_inicio;
	}

	/**
	 * @param $dt_termino the $dt_termino to set
	 */
	public function setDt_termino($dt_termino) {
		$this->dt_termino = $dt_termino;
	}

	/**
	 * @param $id_situacaoperfil the $id_situacaoperfil to set
	 */
	public function setId_situacaoperfil($id_situacaoperfil) {
		$this->id_situacaoperfil = $id_situacaoperfil;
	}

	/**
	 * @param $st_situacaoperfil the $st_situacaoperfil to set
	 */
	public function setSt_situacaoperfil($st_situacaoperfil) {
		$this->st_situacaoperfil = $st_situacaoperfil;
	}

	/**
	 * @param $id_entidade the $id_entidade to set
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param $id_entidadecadastro the $id_entidadecadastro to set
	 */
	public function setId_entidadecadastro($id_entidadecadastro) {
		$this->id_entidadecadastro = $id_entidadecadastro;
	}

	/**
	 * @param $id_sistema the $id_sistema to set
	 */
	public function setId_sistema($id_sistema) {
		$this->id_sistema = $id_sistema;
	}

	/**
	 * @param $id_perfilpedagogico the $id_perfilpedagogico to set
	 */
	public function setId_perfilpedagogico($id_perfilpedagogico) {
		$this->id_perfilpedagogico = $id_perfilpedagogico;
	}

	/**
	 * @param $st_nomeentidade the $st_nomeentidade to set
	 */
	public function setSt_nomeentidade($st_nomeentidade) {
		$this->st_nomeentidade = $st_nomeentidade;
	}

	/**
	 * @param $st_urlimglogo the $st_urlimglogo to set
	 */
	public function setSt_urlimglogo($st_urlimglogo) {
		$this->st_urlimglogo = $st_urlimglogo;
	}

	

}

?>
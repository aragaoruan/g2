<?php
/**
 * Classe para encapsular os dados de responsável legal.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class EntidadeResponsavelLegalTO extends Ead1_TO_Dinamico {

	/**
	 * Id do responsável.
	 * @var int
	 */
	public $id_entidaderesponsavellegal;
	
	/**
	 * Id do tipo de responsável.
	 * @var int
	 */
	public $id_tipoentidaderesponsavel;
	
	/**
	 * Assinatura padrão.
	 * @var Boolean
	 */
	public $bl_padrao;
	
	/**
	 * Exclusão lógica.
	 * @var Boolean
	 */
	public $bl_ativo;
	
	/**
	 * Id do usuário responsável.
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Id da entidade do responsável.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Id da situação.
	 * @var int
	 */
	public $id_situacao;
	
	/**
	 * Número de registro do responsável.
	 * @var 
	 */
	public $st_registro;
	
	/**
	 * Órgão expeditor do responsável.
	 * @var String
	 */
	public $st_orgaoexpeditor;
	
	/**
	 * UF do responsável.
	 * @var String
	 */
	public $sg_uf;
	/**
	 * @return the $id_entidaderesponsavellegal
	 */
	public function getId_entidaderesponsavellegal() {
		return $this->id_entidaderesponsavellegal;
	}

	/**
	 * @return the $id_tipoentidaderesponsavel
	 */
	public function getId_tipoentidaderesponsavel() {
		return $this->id_tipoentidaderesponsavel;
	}

	/**
	 * @return the $bl_padrao
	 */
	public function getBl_padrao() {
		return $this->bl_padrao;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $nu_registro
	 */
	public function getSt_registro() {
		return $this->st_registro;
	}

	/**
	 * @return the $st_orgaoexpeditor
	 */
	public function getSt_orgaoexpeditor() {
		return $this->st_orgaoexpeditor;
	}

	/**
	 * @return the $sg_uf
	 */
	public function getSg_uf() {
		return $this->sg_uf;
	}

	/**
	 * @param int $id_entidaderesponsavellegal
	 */
	public function setId_entidaderesponsavellegal($id_entidaderesponsavellegal) {
		$this->id_entidaderesponsavellegal = $id_entidaderesponsavellegal;
	}

	/**
	 * @param int $id_tipoentidaderesponsavel
	 */
	public function setId_tipoentidaderesponsavel($id_tipoentidaderesponsavel) {
		$this->id_tipoentidaderesponsavel = $id_tipoentidaderesponsavel;
	}

	/**
	 * @param Boolean $bl_padrao
	 */
	public function setBl_padrao($bl_padrao) {
		$this->bl_padrao = $bl_padrao;
	}

	/**
	 * @param Boolean $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @param int $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param int $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param field_type st_registroo
	 */
	public function setSt_registro($st_registro) {
		$this->st_registro = $st_registro;
	}

	/**
	 * @param String $st_orgaoexpeditor
	 */
	public function setSt_orgaoexpeditor($st_orgaoexpeditor) {
		$this->st_orgaoexpeditor = $st_orgaoexpeditor;
	}

	/**
	 * @param String $sg_uf
	 */
	public function setSg_uf($sg_uf) {
		$this->sg_uf = $sg_uf;
	}

	
}

?>
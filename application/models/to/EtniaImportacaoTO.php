<?php
class EtniaImportacaoTO extends Ead1_TO_Dinamico{
	
	public $id_etniaimportacao;
	public $id_etnia;
	public $nu_codetniaorigem;
	public $st_etniaorigem;
	
	/**
	 * @return the $id_etniaimportacao
	 */
	public function getId_etniaimportacao() {
		return $this->id_etniaimportacao;
	}

	/**
	 * @return the $id_etnia
	 */
	public function getId_etnia() {
		return $this->id_etnia;
	}

	/**
	 * @return the $nu_codetniaorigem
	 */
	public function getNu_codetniaorigem() {
		return $this->nu_codetniaorigem;
	}

	/**
	 * @return the $st_etniaorigem
	 */
	public function getSt_etniaorigem() {
		return $this->st_etniaorigem;
	}

	/**
	 * @param field_type $id_etniaimportacao
	 */
	public function setId_etniaimportacao($id_etniaimportacao) {
		$this->id_etniaimportacao = $id_etniaimportacao;
	}

	/**
	 * @param field_type $id_etnia
	 */
	public function setId_etnia($id_etnia) {
		$this->id_etnia = $id_etnia;
	}

	/**
	 * @param field_type $nu_codetniaorigem
	 */
	public function setNu_codetniaorigem($nu_codetniaorigem) {
		$this->nu_codetniaorigem = $nu_codetniaorigem;
	}

	/**
	 * @param field_type $st_etniaorigem
	 */
	public function setSt_etniaorigem($st_etniaorigem) {
		$this->st_etniaorigem = $st_etniaorigem;
	}

}
<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class TaxaTO extends Ead1_TO_Dinamico{

	public $id_taxa;
	public $st_taxa;

	
	/**
	 * Taxa
	 * Extensão de Contrato
	 * @var unknown_type
	 */
	const EXTENSAO_CONTRATO = 1;
	
	const MATRICULA = 2;
	

	/**
	 * @return the $id_taxa
	 */
	public function getId_taxa(){ 
		return $this->id_taxa;
	}

	/**
	 * @return the $st_taxa
	 */
	public function getSt_taxa(){ 
		return $this->st_taxa;
	}


	/**
	 * @param field_type $id_taxa
	 */
	public function setId_taxa($id_taxa){ 
		$this->id_taxa = $id_taxa;
	}

	/**
	 * @param field_type $st_taxa
	 */
	public function setSt_taxa($st_taxa){ 
		$this->st_taxa = $st_taxa;
	}

}
<?php
/**
 * Classe de Configuração de Css do Gerador de XLS
 * @author Eduardo Romão - ejushiro@gmail.com
 * @since 29/03/2011
 * 
 * @package models
 * @subpackage to
 */
class XLSConfigurationTO extends Ead1_TO_Dinamico{
	
	public function __construct(){
		$this->setDefaults();
	}
	
	/**
	 * Metodo que seta o valor padrão
	 */
	private function setDefaults(){
		$this->title = null;
		$this->titleBgColor = Ead1_IColor::BRANCO;
		$this->footer = null;
		$this->footerBgColor = Ead1_IColor::BRANCO;
		$this->headerBgColor = Ead1_IColor::BRANCO;
		$this->striped = true;
		$this->stripedBgColor1 = Ead1_IColor::CINZA;
		$this->stripedBgColor2 = Ead1_IColor::BRANCO;
		$this->headerTextAlign = Ead1_IAlign::TEXT_ALIGN_CENTER;
		$this->footerTextAlign = Ead1_IAlign::TEXT_ALIGN_RIGHT;
		$this->titleTextAlign = Ead1_IAlign::TEXT_ALIGN_CENTER;
		$this->bodyTextAlign = Ead1_IAlign::TEXT_ALIGN_LEFT;
		$this->titleFontSize = '12px';
		$this->footerFontSize = '10px';
		$this->headerFontSize = '11px';
		$this->bodyFontSize = '10px';
		//$this->fontStyle = 'Arial';
		$this->filename = 'unnamed';
		$this->tableMinWidth = '500px';
		$this->orientacao = Ead1_IOrientacao::RETRATO;
		$this->tipoPapel = 'a4';
	}
	
	/**
	 * Contem o Texto que aparecera no arquivo e no Topo do XLS
	 * @var string
	 */
	public $title;
	
	/**
	 * Contem a cor do background do titulo
	 * @var string
	 */
	public $titleBgColor;
	
	/**
	 * Contem o Texto que aparecera no Rodapé do XLS
	 * Enter description here ...
	 * @var string
	 */
	public $footer;
	
	/**
	 * Contem a Cor do Background do rodapé
	 * @var string
	 */
	public $footerBgColor;
	
	/**
	 * Contem a Cor do Background do Cabeçalho da Tabela
	 * @var string
	 */
	public $headerBgColor;
	
	/**
	 * Contem a Cor do Background da Tabela
	 * @var string
	 */
	public $tableBgColor;
	
	/**
	 * Contem a Cor do Background do Corpo da Tabela
	 * @var string
	 */
	public $bodyBgColor;
	
	/**
	 * Contem o Valor se diz se a Tabela vai ser Listrada
	 * @var boolean
	 */
	public $striped = true;
	
	/**
	 * Contem a cor do Background da Listra 1 da Tabela
	 * @var string
	 */
	public $stripedBgColor1;
	
	/**
	 * Contem a cor do Background da Listra 2 da Tabela
	 * @var string
	 */
	public $stripedBgColor2;
	
	/**
	 * Contem o Alinhamento do Texto do Header
	 * @var string
	 */
	public $headerTextAlign;
	
	/**
	 * Contem o Alinhamento do Texto do Footer
	 * @var string
	 */
	public $footerTextAlign;
	
	/**
	 * Contem o Alinhamento do Texto do Titulo
	 * @var string
	 */
	public $titleTextAlign;
	
	/**
	 * Contem o Alinhamento do Texto da Tabela Toda
	 * @var string
	 */
	public $tableTextAlign;
	
	/**
	 * Contem o Alinhamento do Texto do Corpo da Tabela
	 * @var string
	 */
	public $bodyTextAlign;
	
	/**
	 * Contem o tamanho da fonte do titulo
	 * @var string
	 */
	public $titleFontSize;
	
	/**
	 * Contem o tamanho da fonte do footer
	 * @var string
	 */
	public $footerFontSize;
	
	/**
	 * Contem o tamanho da fonte do header
	 * @var string
	 */
	public $headerFontSize;
	
	/**
	 * Contem o tamanho da fonte do corpo da tabela
	 * @var string
	 */
	public $bodyFontSize;
	
	/**
	 * Contem o tamanho da fonte da tabela toda
	 * @var string
	 */
	public $tableFontSize;
	
	/**
	 * Contem o tamanho da Largura Maxima da Tabela
	 * @var string
	 */
	public $tableMaxWidth;
	
	/**
	 * Contem o tamanho da Largura Minima da Tabela
	 * @var string
	 */
	public $tableMinWidth;
	
	/**
	 * Contem o tamanho da Largura Tabela
	 * @var string
	 */
	public $tableWidth;
	
	/**
	 * Contem o tamanho Maximo da Altura Tabela
	 * @var string
	 */
	public $tableMaxHeight;
	
	/**
	 * Contem o tamanho Minimo da Altura Tabela
	 * @var string
	 */
	public $tableMinHeight;
	
	/**
	 * Contem o tamanho da Altura Tabela
	 * @var string
	 */
	public $tableHeight;
	
	/**
	 * Contem a Cor da Fonte do Titulo
	 * @var string
	 */
	public $titleFontColor;
	
	/**
	 * Contem a Cor da Fonte do Rodapé
	 * @var string
	 */
	public $footerFontColor;
	
	/**
	 * Contem a Cor da Fonte da Tabela
	 * @var string
	 */
	public $tableFontColor;
	
	/**
	 * Contem a Cor da Fonte do Cabeçalho
	 * @var string
	 */
	public $headerFontColor;
	
	/**
	 * Contem a Cor da Fonte do Corpo da Tabela
	 * @var string
	 */
	public $bodyFontColor;
	
	/**
	 * 
	 * Contém o tipo de fonte
	 * @var string
	 */
	public $fontStyle;
	
	/**
	 * Contem o Nome do arquivo final a ser salvo
	 * @var string
	 */
	public $filename;
	
	/**
	 * Contém os códigos HTML, podendo conter o codigo CSS inline, no formato:
	 * Exemplo:
	 * <body>
	 * 		<p style="text-align: center;">exemplo</p>
	 * </body>
	 * 
	 * @var String
	 */
	public $html;
	
	/**
	 * Indica a orientação do papel. Ex. Ead1_IOrientacao::PAISAGEM
	 * @var String
	 */
	public $orientacao;
	
	/**
	 * Indica o tipo de papel. Ex. a4
	 * @var String
	 */
	public $tipoPapel;
	
	/**
	 * @return the $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @return the $titleBgColor
	 */
	public function getTitleBgColor() {
		return $this->titleBgColor;
	}

	/**
	 * @return the $footer
	 */
	public function getFooter() {
		return $this->footer;
	}

	/**
	 * @return the $footerBgColor
	 */
	public function getFooterBgColor() {
		return $this->footerBgColor;
	}

	/**
	 * @return the $headerBgColor
	 */
	public function getHeaderBgColor() {
		return $this->headerBgColor;
	}

	/**
	 * @return the $striped
	 */
	public function getStriped() {
		return $this->striped;
	}

	/**
	 * @return the $stripedBgColor1
	 */
	public function getStripedBgColor1() {
		return $this->stripedBgColor1;
	}

	/**
	 * @return the $stripedBgColor2
	 */
	public function getStripedBgColor2() {
		return $this->stripedBgColor2;
	}

	/**
	 * @return the $headerTextAlign
	 */
	public function getHeaderTextAlign() {
		return $this->headerTextAlign;
	}

	/**
	 * @return the $footerTextAlign
	 */
	public function getFooterTextAlign() {
		return $this->footerTextAlign;
	}

	/**
	 * @return the $titleTextAlign
	 */
	public function getTitleTextAlign() {
		return $this->titleTextAlign;
	}

	/**
	 * @return the $tableTextAlign
	 */
	public function getTableTextAlign() {
		return $this->tableTextAlign;
	}

	/**
	 * @return the $bodyTextAlign
	 */
	public function getBodyTextAlign() {
		return $this->bodyTextAlign;
	}

	/**
	 * @return the $titleFontSize
	 */
	public function getTitleFontSize() {
		return $this->titleFontSize;
	}

	/**
	 * @return the $footerFontSize
	 */
	public function getFooterFontSize() {
		return $this->footerFontSize;
	}

	/**
	 * @return the $headerFontSize
	 */
	public function getHeaderFontSize() {
		return $this->headerFontSize;
	}

	/**
	 * @return the $bodyFontSize
	 */
	public function getBodyFontSize() {
		return $this->bodyFontSize;
	}

	/**
	 * @return the $tableFontSize
	 */
	public function getTableFontSize() {
		return $this->tableFontSize;
	}

	/**
	 * @param string $title
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * @param string $titleBgColor
	 */
	public function setTitleBgColor($titleBgColor) {
		$this->titleBgColor = $titleBgColor;
	}

	/**
	 * @param unknown_type $footer
	 */
	public function setFooter($footer) {
		$this->footer = $footer;
	}

	/**
	 * @param string $footerBgColor
	 */
	public function setFooterBgColor($footerBgColor) {
		$this->footerBgColor = $footerBgColor;
	}

	/**
	 * @param string $headerBgColor
	 */
	public function setHeaderBgColor($headerBgColor) {
		$this->headerBgColor = $headerBgColor;
	}

	/**
	 * @param boolean $striped
	 */
	public function setStriped($striped) {
		$this->striped = $striped;
	}

	/**
	 * @param string $stripedBgColor1
	 */
	public function setStripedBgColor1($stripedBgColor1) {
		$this->stripedBgColor1 = $stripedBgColor1;
	}

	/**
	 * @param string $stripedBgColor2
	 */
	public function setStripedBgColor2($stripedBgColor2) {
		$this->stripedBgColor2 = $stripedBgColor2;
	}

	/**
	 * @param string $headerTextAlign
	 */
	public function setHeaderTextAlign($headerTextAlign) {
		$this->headerTextAlign = $headerTextAlign;
	}

	/**
	 * @param string $footerTextAlign
	 */
	public function setFooterTextAlign($footerTextAlign) {
		$this->footerTextAlign = $footerTextAlign;
	}

	/**
	 * @param string $titleTextAlign
	 */
	public function setTitleTextAlign($titleTextAlign) {
		$this->titleTextAlign = $titleTextAlign;
	}

	/**
	 * @param string $tableTextAlign
	 */
	public function setTableTextAlign($tableTextAlign) {
		$this->tableTextAlign = $tableTextAlign;
	}

	/**
	 * @param string $bodyTextAlign
	 */
	public function setBodyTextAlign($bodyTextAlign) {
		$this->bodyTextAlign = $bodyTextAlign;
	}

	/**
	 * @param string $titleFontSize
	 */
	public function setTitleFontSize($titleFontSize) {
		$this->titleFontSize = $titleFontSize;
	}

	/**
	 * @param string $footerFontSize
	 */
	public function setFooterFontSize($footerFontSize) {
		$this->footerFontSize = $footerFontSize;
	}

	/**
	 * @param string $headerFontSize
	 */
	public function setHeaderFontSize($headerFontSize) {
		$this->headerFontSize = $headerFontSize;
	}

	/**
	 * @param string $bodyFontSize
	 */
	public function setBodyFontSize($bodyFontSize) {
		$this->bodyFontSize = $bodyFontSize;
	}

	/**
	 * @param string $tableFontSize
	 */
	public function setTableFontSize($tableFontSize) {
		$this->tableFontSize = $tableFontSize;
	}
	/**
	 * @return the $filename
	 */
	public function getFilename() {
		return $this->filename;
	}

	/**
	 * @param string $filename
	 */
	public function setFilename($filename) {
		$this->filename = $filename;
	}
	/**
	 * @return the $tableBgColor
	 */
	public function getTableBgColor() {
		return $this->tableBgColor;
	}

	/**
	 * @param string $tableBgColor
	 */
	public function setTableBgColor($tableBgColor) {
		$this->tableBgColor = $tableBgColor;
	}
	/**
	 * @return the $bodyBgColor
	 */
	public function getBodyBgColor() {
		return $this->bodyBgColor;
	}

	/**
	 * @param string $bodyBgColor
	 */
	public function setBodyBgColor($bodyBgColor) {
		$this->bodyBgColor = $bodyBgColor;
	}
	/**
	 * @return the $tableMaxWidth
	 */
	public function getTableMaxWidth() {
		return $this->tableMaxWidth;
	}

	/**
	 * @return the $tableMinWidth
	 */
	public function getTableMinWidth() {
		return $this->tableMinWidth;
	}

	/**
	 * @return the $tableWidth
	 */
	public function getTableWidth() {
		return $this->tableWidth;
	}

	/**
	 * @return the $tableMaxHeight
	 */
	public function getTableMaxHeight() {
		return $this->tableMaxHeight;
	}

	/**
	 * @return the $tableMinHeight
	 */
	public function getTableMinHeight() {
		return $this->tableMinHeight;
	}

	/**
	 * @return the $tableHeight
	 */
	public function getTableHeight() {
		return $this->tableHeight;
	}

	/**
	 * @param string $tableMaxWidth
	 */
	public function setTableMaxWidth($tableMaxWidth) {
		$this->tableMaxWidth = $tableMaxWidth;
	}

	/**
	 * @param string $tableMinWidth
	 */
	public function setTableMinWidth($tableMinWidth) {
		$this->tableMinWidth = $tableMinWidth;
	}

	/**
	 * @param string $tableWidth
	 */
	public function setTableWidth($tableWidth) {
		$this->tableWidth = $tableWidth;
	}

	/**
	 * @param string $tableMaxHeight
	 */
	public function setTableMaxHeight($tableMaxHeight) {
		$this->tableMaxHeight = $tableMaxHeight;
	}

	/**
	 * @param string $tableMinHeight
	 */
	public function setTableMinHeight($tableMinHeight) {
		$this->tableMinHeight = $tableMinHeight;
	}

	/**
	 * @param string $tableHeight
	 */
	public function setTableHeight($tableHeight) {
		$this->tableHeight = $tableHeight;
	}
	/**
	 * @return the $titleFontColor
	 */
	public function getTitleFontColor() {
		return $this->titleFontColor;
	}

	/**
	 * @return the $footerFontColor
	 */
	public function getFooterFontColor() {
		return $this->footerFontColor;
	}

	/**
	 * @return the $tableFontColor
	 */
	public function getTableFontColor() {
		return $this->tableFontColor;
	}

	/**
	 * @return the $headerFontColor
	 */
	public function getHeaderFontColor() {
		return $this->headerFontColor;
	}

	/**
	 * @return the $bodyFontColor
	 */
	public function getBodyFontColor() {
		return $this->bodyFontColor;
	}

	/**
	 * @param string $titleFontColor
	 */
	public function setTitleFontColor($titleFontColor) {
		$this->titleFontColor = $titleFontColor;
	}

	/**
	 * @param string $footerFontColor
	 */
	public function setFooterFontColor($footerFontColor) {
		$this->footerFontColor = $footerFontColor;
	}

	/**
	 * @param string $tableFontColor
	 */
	public function setTableFontColor($tableFontColor) {
		$this->tableFontColor = $tableFontColor;
	}

	/**
	 * @param string $headerFontColor
	 */
	public function setHeaderFontColor($headerFontColor) {
		$this->headerFontColor = $headerFontColor;
	}

	/**
	 * @param string $bodyFontColor
	 */
	public function setBodyFontColor($bodyFontColor) {
		$this->bodyFontColor = $bodyFontColor;
	}
	/**
	 * @return the $html
	 */
	public function getHtml() {
		return $this->html;
	}

	/**
	 * @return the $orientacao
	 */
	public function getOrientacao() {
		return $this->orientacao;
	}

	/**
	 * @return the $tipoPapel
	 */
	public function getTipoPapel() {
		return $this->tipoPapel;
	}

	/**
	 * @param String $html
	 */
	public function setHtml($html) {
		$this->html = $html;
	}

	/**
	 * @param String $orientacao
	 */
	public function setOrientacao($orientacao) {
		$this->orientacao = $orientacao;
	}

	/**
	 * @param String $tipoPapel
	 */
	public function setTipoPapel($tipoPapel) {
		$this->tipoPapel = $tipoPapel;
	}
	/**
	 * @return the $fontStyle
	 */
	public function getFontStyle() {
		return $this->fontStyle;
	}

	/**
	 * @param string $fontStyle
	 */
	public function setFontStyle($fontStyle) {
		$this->fontStyle = $fontStyle;
	}


}
<?php
/**
 * Classe para encapsular da tabela
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */

class TurmaSalaTO extends Ead1_TO_Dinamico
{

    public $id_turmasala;
    public $id_turma;
    public $id_saladeaula;
    public $dt_cadastro;

    /**
     * @return mixed
     */
    public function getId_turmasala()
    {
        return $this->id_turmasala;
    }

    /**
     * @param mixed $id_turmasala
     */
    public function setId_turmasala($id_turmasala)
    {
        $this->id_turmasala = $id_turmasala;
    }

    /**
     * @return mixed
     */
    public function getId_turma()
    {
        return $this->id_turma;
    }

    /**
     * @param mixed $id_turma
     */
    public function setId_turma($id_turma)
    {
        $this->id_turma = $id_turma;
    }

    /**
     * @return mixed
     */
    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param mixed $id_saladeaula
     */
    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
    }

    /**
     * @return mixed
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param mixed $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }


}
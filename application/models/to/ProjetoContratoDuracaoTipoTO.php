<?php
/**
 * Classe para encapsular os dados de tipo de duração do contrato do projeto pedagógico.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class ProjetoContratoDuracaoTipoTO extends Ead1_TO_Dinamico {
	
	/**
	 * Contem o valor da contante do tipo de duração do projeto pedagogico
	 * @var int
	 */
	const DURACAO_CURSO_COMPLETO = 1;
	
	/**
	 * Contem o valor da contante do tipo de duração do projeto pedagogico
	 * @var int
	 */
	const DURACAO_MESES = 2;
	 
	/**
	 * Contem o valor da contante do tipo de duração do projeto pedagogico
	 * @var int
	 */
	const DURACAO_DIAS = 3; 

	/**
	 * Id do tipo de duração do contrato do projeto pedagógico.
	 * @var int
	 */
	public $id_projetocontratoduracaotipo;
	
	/**
	 * Tipo de duração do contrato do projeto pedagógico.
	 * @var string
	 */
	public $st_projetocontratoduracaotipo;
	
	/**
	 * Descrição do tipo de duração do contrato do projeto pedagógico.
	 * @var string
	 */
	public $st_descricao;
	
	/**
	 * @return string
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}
	
	/**
	 * @param string $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}
	/**
	 * @return int
	 */
	public function getId_projetocontratoduracaotipo() {
		return $this->id_projetocontratoduracaotipo;
	}
	
	/**
	 * @return string
	 */
	public function getSt_projetocontratoduracaotipo() {
		return $this->st_projetocontratoduracaotipo;
	}
	
	/**
	 * @param int $id_projetocontratoduracaotipo
	 */
	public function setId_projetocontratoduracaotipo($id_projetocontratoduracaotipo) {
		$this->id_projetocontratoduracaotipo = $id_projetocontratoduracaotipo;
	}
	
	/**
	 * @param string $st_projetocontratoduracaotipo
	 */
	public function setSt_projetocontratoduracaotipo($st_projetocontratoduracaotipo) {
		$this->st_projetocontratoduracaotipo = $st_projetocontratoduracaotipo;
	}

}

?>
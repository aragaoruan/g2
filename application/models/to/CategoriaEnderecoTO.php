<?php
/**
 * Classe para encapsular os dados de categoria de um endereço.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class CategoriaEnderecoTO extends Ead1_TO_Dinamico {

	/**
	 * Atributo que contém o id da categoria de endereço.
	 * @var int
	 */
	public $id_categoriaendereco;
	
	/**
	 * Atributo que contém a categoria de endereço.
	 * @var string
	 */
	public $st_categoriaendereco;
	
	/**
	 * @return int
	 */
	public function getId_categoriaendereco() {
		return $this->id_categoriaendereco;
	}
	
	/**
	 * @return string
	 */
	public function getSt_categoriaendereco() {
		return $this->st_categoriaendereco;
	}
	
	/**
	 * @param int $id_categoriaendereco
	 */
	public function setId_categoriaendereco($id_categoriaendereco) {
		$this->id_categoriaendereco = $id_categoriaendereco;
	}
	
	/**
	 * @param string $st_categoriaendereco
	 */
	public function setSt_categoriaendereco($st_categoriaendereco) {
		$this->st_categoriaendereco = $st_categoriaendereco;
	}

}

?>
<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class TipoEnvioTO extends Ead1_TO_Dinamico{
	
	const EMAIL = 3;
	const HTML = 4;
    const SMS 	= 5;

	public $id_tipoenvio;
	public $st_tipoenvio;
	public $st_descricao;


	/**
	 * @return the $id_tipoenvio
	 */
	public function getId_tipoenvio(){ 
		return $this->id_tipoenvio;
	}

	/**
	 * @return the $st_tipoenvio
	 */
	public function getSt_tipoenvio(){ 
		return $this->st_tipoenvio;
	}

	/**
	 * @return the $st_descricao
	 */
	public function getSt_descricao(){ 
		return $this->st_descricao;
	}


	/**
	 * @param field_type $id_tipoenvio
	 */
	public function setId_tipoenvio($id_tipoenvio){ 
		$this->id_tipoenvio = $id_tipoenvio;
	}

	/**
	 * @param field_type $st_tipoenvio
	 */
	public function setSt_tipoenvio($st_tipoenvio){
		$this->st_tipoenvio = $st_tipoenvio;
	}

	/**
	 * @param field_type $st_descricao
	 */
	public function setSt_descricao($st_descricao){ 
		$this->st_descricao = $st_descricao;
	}

}
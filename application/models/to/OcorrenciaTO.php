<?php

/**
 * Classe para encapsular os dados de ocorrência.
 * @author Denise Xavier - denisexavier@ead1.com.br
 * @package models
 * @subpackage to
 */
class OcorrenciaTO extends Ead1_TO_Dinamico
{

    const EVOLUCAO_AGUARDANDO_ATENDIMENTO = 28;
    const EVOLUCAO_SENDO_ATENDIMENTO = 29;
    const EVOLUCAO_AGUARDANDO_INTERESSADO = 30;
    const EVOLUCAO_AGUARDANDO_ATENDENTE = 31;
    const EVOLUCAO_INTERESSADO_INTERAGIU = 32;
    const EVOLUCAO_AGUARDANDO_DOCUMENTACAO = 33;
    const EVOLUCAO_AGUARDANDO_PAGAMENTO = 34;
    const EVOLUCAO_AGUARDANDO_OUTROS_SETORES = 35;
    const EVOLUCAO_ENCERRAMENTO_PELO_INTERESSADO = 36;
    const EVOLUCAO_REABERTURA_PELO_INTERESSADO = 37;
    const EVOLUCAO_ENCERRAMENTO_PELO_ATENDENTE = 38;
    const EVOLUCAO_DEVOLVIDO_PARA_DISTRIBUICAO = 39;

    const SITUACAO_PENDENTE = 97;
    const SITUACAO_EM_ANDAMENTO = 98;
    const SITUACAO_ENCERRADA = 99;
    const SITUACAO_AGUARDANDO_ENCERRAMENTO = 112;
    //Apenas logica, nao existe no banco
    const SITUACAO_NAO_ENCERRADA = 999999;

    const TIPO_TRAMITE_AUTOMATICO = 12;
    const TIPO_TRAMITE_MANUAL = 13;

    const CATEGORIA_TRAMITE = 4;

    /**
     * Chave da tabela
     * @var int
     */
    public $id_ocorrencia;

    /**
     * Matrícula do aluno (quando for ocorrencia de aluno)
     * @var int
     */
    public $id_matricula;

    /**
     * Evolução da ocorrência
     * @var int
     */
    public $id_evolucao;

    /**
     * Situação da ocorrência
     * @var int
     */
    public $id_situacao;

    /**
     * Pessoa que criou a ocorrencia
     * @var int
     */
    public $id_usuariointeressado;

    /**
     * Tipo de Ocorrência
     * @var int
     */
    public $id_categoriaocorrencia;

    /**
     * Assunto da Ocorrência
     * @var int
     */
    public $id_assuntoco;

    /**
     * Sala de aula (caso o aluno tenha selecionado
     * @var int
     */
    public $id_saladeaula;

    /**
     * Título da ocorrência
     * @var string
     */
    public $st_titulo;

    /**
     * Descrição da ocorrência
     * @var string
     */
    public $st_ocorrencia;

    /**
     * id da ocorrência original
     * @var int
     */
    public $id_ocorrenciaoriginal;

    public $id_entidade;

    public $id_usuariocadastro;

    public $id_motivoocorrencia;

    public $dt_atendimento;

    public $dt_cadastroocorrencia;

    public $dt_cadastro;

    public $id_venda;

    /**
     * @var string
     */
    public $st_codissue;


    /**
     * @return integer
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @return integer $id_ocorrencia
     */
    public function getId_ocorrencia()
    {
        return $this->id_ocorrencia;
    }

    /**
     * @param integer $id_ocorrencia
     */
    public function setId_ocorrencia($id_ocorrencia)
    {
        $this->id_ocorrencia = $id_ocorrencia;
    }

    /**
     * @return integer $id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param integer $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return integer $id_matricula
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param integer $id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @return integer $id_evolucao
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param integer $id_evolucao
     */
    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
    }

    /**
     * @return integer $id_situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param integer $id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    /**
     * @return integer $id_usuariointeressado
     */
    public function getId_usuariointeressado()
    {
        return $this->id_usuariointeressado;
    }

    /**
     * @param integer $id_usuariointeressado
     */
    public function setId_usuariointeressado($id_usuariointeressado)
    {
        $this->id_usuariointeressado = $id_usuariointeressado;
    }

    /**
     * @return integer $id_categoriaocorrencia
     */
    public function getId_categoriaocorrencia()
    {
        return $this->id_categoriaocorrencia;
    }

    /**
     * @param integer $id_categoriaocorrencia
     */
    public function setId_categoriaocorrencia($id_categoriaocorrencia)
    {
        $this->id_categoriaocorrencia = $id_categoriaocorrencia;
    }

    /**
     * @return integer $id_assuntoco
     */
    public function getId_assuntoco()
    {
        return $this->id_assuntoco;
    }

    /**
     * @param integer $id_assuntoco
     */
    public function setId_assuntoco($id_assuntoco)
    {
        $this->id_assuntoco = $id_assuntoco;
    }

    /**
     * @return integer $id_saladeaula
     */
    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param integer $id_saladeaula
     */
    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
    }

    /**
     * @return string $st_titulo
     */
    public function getSt_titulo()
    {
        return $this->st_titulo;
    }

    /**
     * @param string $st_titulo
     */
    public function setSt_titulo($st_titulo)
    {
        $this->st_titulo = $st_titulo;
    }

    /**
     * @return string $st_ocorrencia
     */
    public function getSt_ocorrencia()
    {
        return $this->st_ocorrencia;
    }

    /**
     * @param string $st_ocorrencia
     */
    public function setSt_ocorrencia($st_ocorrencia)
    {
        $this->st_ocorrencia = $st_ocorrencia;
    }

    /**
     * @return integer $id_ocorrenciaoriginal
     */
    public function getId_ocorrenciaoriginal()
    {
        return $this->id_ocorrenciaoriginal;
    }

    /**
     * @param integer $id_ocorrenciaoriginal
     */
    public function setId_ocorrenciaoriginal($id_ocorrenciaoriginal)
    {
        $this->id_ocorrenciaoriginal = $id_ocorrenciaoriginal;
    }

    /**
     * @param integer $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @return integer $id_motivoocorrencia
     */
    public function getId_motivoocorrencia()
    {
        return $this->id_motivoocorrencia;
    }

    /**
     * @param integer $id_motivoocorrencia
     */
    public function setId_motivoocorrencia($id_motivoocorrencia)
    {
        $this->id_motivoocorrencia = $id_motivoocorrencia;
    }

    /**
     * @return mixed $dt_atendimento
     */
    public function getDt_atendimento()
    {
        return $this->dt_atendimento;
    }

    /**
     * @param mixed $dt_atendimento
     */
    public function setDt_atendimento($dt_atendimento)
    {
        $this->dt_atendimento = $dt_atendimento;
    }

    /**
     * @param mixed $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return mixed $dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @return mixed $dt_cadastroocorrencia
     */
    public function getDt_cadastroocorrencia()
    {
        return $this->dt_cadastroocorrencia;
    }

    /**
     * @param mixed $dt_cadastroocorrencia
     */
    public function setDt_cadastroocorrencia($dt_cadastroocorrencia)
    {
        $this->dt_cadastroocorrencia = $dt_cadastroocorrencia;
    }

    /**
     * @return integer $id_venda
     */
    public function getId_venda()
    {
        return $this->id_venda;
    }

    /**
     * @param integer $id_venda
     */
    public function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
    }


    /**
     * @return string
     */
    public function getSt_codissue()
    {
        return $this->st_codissue;
    }

    /**
     * @param string $st_codissue
     */
    public function setSt_codissue($st_codissue)
    {
        $this->st_codissue = $st_codissue;
    }
}

?>
<?php

/**
 * @author DeniseXavier denisexavier@ead1.com.br
 * @package models
 * @subpackage to
 */
class NucleoCoEmailTO extends Ead1_TO_Dinamico {
	
	public $id_nucleoco;
	public $id_emailconfig;
	/**
	 * @return the $id_nucleoco
	 */
	public function getId_nucleoco() {
		return $this->id_nucleoco;
	}

	/**
	 * @param field_type $id_nucleoco
	 */
	public function setId_nucleoco($id_nucleoco) {
		$this->id_nucleoco = $id_nucleoco;
	}

	/**
	 * @return the $id_emailconfig
	 */
	public function getId_emailconfig() {
		return $this->id_emailconfig;
	}

	/**
	 * @param field_type $id_emailconfig
	 */
	public function setId_emailconfig($id_emailconfig) {
		$this->id_emailconfig = $id_emailconfig;
	}

}

?>
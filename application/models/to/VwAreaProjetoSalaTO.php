<?php
/**
 * To da view vw_areaprojetosala
 * @author edermariano
 * @package models
 * @subpackage to
 */
class VwAreaProjetoSalaTO extends Ead1_TO_Dinamico {
	
	/**
	 * @var int
	 */
	public $id_areaprojetosala;

	/**
	 * @var int
	 */
	public $id_saladeaula;
	
	/**
	 * @var int
	 */
	public $id_areaconhecimento;
	
	/**
	 * @var int
	 */
	public $id_projetopedagogico;
	
	/**
	 * @var string
	 */
	public $st_areaconhecimento;
	
	/**
	 * @var int
	 */
	public $id_nivelensino;
	
	/**
	 * @var string
	 */
	public $st_saladeaula;
	
	/**
	 * @var string
	 */
	public $st_nivelensino;
	
	/**
	 * @var string
	 */
	public $st_projetopedagogico;
	
	/**
	 * @var string
	 */
	public $st_coordenadorprojeto;
	
	/**
	 * @var string
	 */
	public $st_referencia;
	
	/**
	 * @var string
	 */
	public $st_descricao;
	
	public $id_perfilpedagogico;
	
	/**
	 * @return the $id_areaprojetosala
	 */
	public function getId_areaprojetosala() {
		return $this->id_areaprojetosala;
	}

	/**
	 * @param number $id_areaprojetosala
	 */
	public function setId_areaprojetosala($id_areaprojetosala) {
		$this->id_areaprojetosala = $id_areaprojetosala;
	}

	/**
	 * @return the $st_descricao
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}

	/**
	 * @param string $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
		return $this;
	}

	/**
	 * @return the $st_referencia
	 */
	public function getSt_referencia() {
		return $this->st_referencia;
	}

	/**
	 * @param string $st_referencia
	 */
	public function setSt_referencia($st_referencia) {
		$this->st_referencia = $st_referencia;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getId_areaconhecimento() {
		return $this->id_areaconhecimento;
	}
	
	/**
	 * @return int
	 */
	public function getId_nivelensino() {
		return $this->id_nivelensino;
	}
	
	/**
	 * @return int
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}
	
	/**
	 * @return int
	 */
	public function getId_saladeaula() {
		return $this->id_saladeaula;
	}
	
	/**
	 * @return string
	 */
	public function getSt_areaconhecimento() {
		return $this->st_areaconhecimento;
	}
	
	/**
	 * @return string
	 */
	public function getSt_coordenadorprojeto() {
		return $this->st_coordenadorprojeto;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nivelensino() {
		return $this->st_nivelensino;
	}
	
	/**
	 * @return string
	 */
	public function getSt_saladeaula() {
		return $this->st_saladeaula;
	}
	
	/**
	 * @param int $id_areaconhecimento
	 */
	public function setId_areaconhecimento($id_areaconhecimento) {
		$this->id_areaconhecimento = $id_areaconhecimento;
	}
	
	/**
	 * @param int $id_nivelensino
	 */
	public function setId_nivelensino($id_nivelensino) {
		$this->id_nivelensino = $id_nivelensino;
	}
	
	/**
	 * @param int $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}
	
	/**
	 * @param int $id_saladeaula
	 */
	public function setId_saladeaula($id_saladeaula) {
		$this->id_saladeaula = $id_saladeaula;
	}
	
	/**
	 * @param string $st_areaconhecimento
	 */
	public function setSt_areaconhecimento($st_areaconhecimento) {
		$this->st_areaconhecimento = $st_areaconhecimento;
	}
	
	/**
	 * @param string $st_coordenadorprojeto
	 */
	public function setSt_coordenadorprojeto($st_coordenadorprojeto) {
		$this->st_coordenadorprojeto = $st_coordenadorprojeto;
	}
	
	
	/**
	 * @param string $st_nivelensino
	 */
	public function setSt_nivelensino($st_nivelensino) {
		$this->st_nivelensino = $st_nivelensino;
	}
	
	/**
	 * @param string $st_saladeaula
	 */
	public function setSt_saladeaula($st_saladeaula) {
		$this->st_saladeaula = $st_saladeaula;
	}
	
	/**
	 * @return string
	 */
	public function getSt_projetopedagogico() {
		return $this->st_projetopedagogico;
	}
	
	/**
	 * @param string $st_projetopedagogico
	 */
	public function setSt_projetopedagogico($st_projetopedagogico) {
		$this->st_projetopedagogico = $st_projetopedagogico;
	}

	public function getId_perfilpedagogico()
	{
		return $this->id_perfilpedagogico;
	}

	public function setId_perfilpedagogico($id_perfilpedagogico)
	{
		$this->id_perfilpedagogico = $id_perfilpedagogico;
	}



}

?>
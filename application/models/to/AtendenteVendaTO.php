<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class AtendenteVendaTO extends Ead1_TO_Dinamico{

	public $id_atendentevenda;
	public $id_usuario;
	public $id_venda;
	public $id_nucleotm;
	public $bl_ativo;
	public $dt_cadastro;


	/**
	 * @return the $id_atendentevenda
	 */
	public function getId_atendentevenda(){ 
		return $this->id_atendentevenda;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario(){ 
		return $this->id_usuario;
	}

	/**
	 * @return the $id_venda
	 */
	public function getId_venda(){ 
		return $this->id_venda;
	}

	/**
	 * @return the $id_nucleotm
	 */
	public function getId_nucleotm(){ 
		return $this->id_nucleotm;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo(){ 
		return $this->bl_ativo;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro(){ 
		return $this->dt_cadastro;
	}


	/**
	 * @param field_type $id_atendentevenda
	 */
	public function setId_atendentevenda($id_atendentevenda){ 
		$this->id_atendentevenda = $id_atendentevenda;
	}

	/**
	 * @param field_type $id_usuario
	 */
	public function setId_usuario($id_usuario){ 
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param field_type $id_venda
	 */
	public function setId_venda($id_venda){ 
		$this->id_venda = $id_venda;
	}

	/**
	 * @param field_type $id_nucleotm
	 */
	public function setId_nucleotm($id_nucleotm){ 
		$this->id_nucleotm = $id_nucleotm;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo){ 
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro){ 
		$this->dt_cadastro = $dt_cadastro;
	}

}
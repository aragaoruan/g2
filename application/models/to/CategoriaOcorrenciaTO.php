<?php

/**
 * @author Denise Xavier - denisexavier@ead1.com.br
 * @package models
 * @subpackage to
 */
class CategoriaOcorrenciaTO extends Ead1_TO_Dinamico
{


    /**
     * @var integer
     */
    public $id_categoriaocorrencia;

    /**
     * @var integer
     */
    public $id_situacao;

    /**
     * @var integer
     */
    public $id_tipoocorrencia;

    /**
     * @var integer
     */
    public $id_usuariocadastro;

    /**
     * @var integer
     */
    public $id_entidadecadastro;

    /**
     * @var string
     */
    public $st_categoriaocorrencia;

    /**
     * @var mixed
     */
    public $dt_cadastro;

    /**
     * @var boolean
     */
    public $bl_ativo;

    /**
     * @return int
     */
    public function getId_categoriaocorrencia()
    {
        return $this->id_categoriaocorrencia;
    }

    /**
     * @param int $id_categoriaocorrencia
     * @return CategoriaOcorrenciaTO
     */
    public function setId_categoriaocorrencia($id_categoriaocorrencia)
    {
        $this->id_categoriaocorrencia = $id_categoriaocorrencia;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param int $id_situacao
     * @return CategoriaOcorrenciaTO
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_tipoocorrencia()
    {
        return $this->id_tipoocorrencia;
    }

    /**
     * @param int $id_tipoocorrencia
     * @return CategoriaOcorrenciaTO
     */
    public function setId_tipoocorrencia($id_tipoocorrencia)
    {
        $this->id_tipoocorrencia = $id_tipoocorrencia;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param int $id_usuariocadastro
     * @return CategoriaOcorrenciaTO
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidadecadastro()
    {
        return $this->id_entidadecadastro;
    }

    /**
     * @param int $id_entidadecadastro
     * @return CategoriaOcorrenciaTO
     */
    public function setId_entidadecadastro($id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_categoriaocorrencia()
    {
        return $this->st_categoriaocorrencia;
    }

    /**
     * @param string $st_categoriaocorrencia
     * @return CategoriaOcorrenciaTO
     */
    public function setSt_categoriaocorrencia($st_categoriaocorrencia)
    {
        $this->st_categoriaocorrencia = $st_categoriaocorrencia;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param mixed $dt_cadastro
     * @return CategoriaOcorrenciaTO
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param bool $bl_ativo
     * @return CategoriaOcorrenciaTO
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }


}

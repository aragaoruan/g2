<?php

/**
 * @author Rafael Costa Rocha
 * @package models
 * @subpackage to
 *
 */
class VwSetorTO extends SetorTO {
	
	public $st_funcaocadastro;
	public $st_nomeentidadecadastro;
	public $st_setorpai;
	public $st_situacao;
	public $st_usuariocadastro;
	
	/**
	 * @return the $st_funcaocadastro
	 */
	public function getSt_funcaocadastro() {
		return $this->st_funcaocadastro;
	}

	/**
	 * @return the $st_nomeentidadecadastro
	 */
	public function getSt_nomeentidadecadastro() {
		return $this->st_nomeentidadecadastro;
	}

	/**
	 * @return the $st_setorpai
	 */
	public function getSt_setorpai() {
		return $this->st_setorpai;
	}

	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}

	/**
	 * @return the $st_usuariocadastro
	 */
	public function getSt_usuariocadastro() {
		return $this->st_usuariocadastro;
	}

	/**
	 * @param field_type $st_funcaocadastro
	 */
	public function setSt_funcaocadastro($st_funcaocadastro) {
		$this->st_funcaocadastro = $st_funcaocadastro;
	}

	/**
	 * @param field_type $st_nomeentidadecadastro
	 */
	public function setSt_nomeentidadecadastro($st_nomeentidadecadastro) {
		$this->st_nomeentidadecadastro = $st_nomeentidadecadastro;
	}

	/**
	 * @param field_type $st_setorpai
	 */
	public function setSt_setorpai($st_setorpai) {
		$this->st_setorpai = $st_setorpai;
	}

	/**
	 * @param field_type $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

	/**
	 * @param field_type $st_usuariocadastro
	 */
	public function setSt_usuariocadastro($st_usuariocadastro) {
		$this->st_usuariocadastro = $st_usuariocadastro;
	}
}

?>
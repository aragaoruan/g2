<?php

/**
 * TO UrlResumida
 *
 * @author Rafael Bruno <rafaelbruno.ti@gmail.com>
 */
class UrlResumidaTO extends Ead1_TO_Dinamico {

    /**
     * Contem id da url resumida 
     * @var type private
     */
    public $id_urlresumida;

    /**
     * Contem Texto da url resumida
     * @var type private
     */
    public $st_urlresumida;

    /**
     * Contem Texto da url completa
     * @var type 
     */
    public $st_urlcompleta;

    /**
     * @return int
     */
    public function getId_urlresumida() {
        return $this->id_urlresumida;
    }

    /**
     * @param $id_urlresumida
     */
    public function setId_urlresumida($id_urlresumida) {
        $this->id_urlresumida = $id_urlresumida;
    }

    /**
     * @return String
     */
    public function getSt_urlresumida() {
        return $this->st_urlresumida;
    }

    /**
     * @param $st_urlresumida
     */
    public function setSt_urlresumida($st_urlresumida) {
        $this->st_urlresumida = $st_urlresumida;
    }

    /**
     * @return String
     */
    public function getSt_urlcompleta() {
        return $this->st_urlcompleta;
    }

    /**
     * @param $st_urlcompleta
     */
    public function setSt_urlcompleta($st_urlcompleta) {
        $this->st_urlcompleta = $st_urlcompleta;
    }

}
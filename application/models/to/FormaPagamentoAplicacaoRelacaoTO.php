<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class FormaPagamentoAplicacaoRelacaoTO extends Ead1_TO_Dinamico{

	public $id_formapagamento;
	public $id_formapagamentoaplicacao;


	/**
	 * @return the $id_formapagamento
	 */
	public function getId_formapagamento(){ 
		return $this->id_formapagamento;
	}

	/**
	 * @return the $id_formapagamentoaplicacao
	 */
	public function getId_formapagamentoaplicacao(){ 
		return $this->id_formapagamentoaplicacao;
	}


	/**
	 * @param field_type $id_formapagamento
	 */
	public function setId_formapagamento($value){ 
		$this->id_formapagamento = $value;
	}

	/**
	 * @param field_type $id_formapagamentoaplicacao
	 */
	public function setId_formapagamentoaplicacao($value){ 
		$this->id_formapagamentoaplicacao = $value;
	}

}
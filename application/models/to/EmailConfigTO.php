<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class EmailConfigTO extends Ead1_TO_Dinamico{

	public $id_emailconfig;
	public $id_tipoconexaoemailsaida;
	public $id_tipoconexaoemailentrada;
	public $id_usuariocadastro;
	public $id_entidade;
	public $st_smtp;
	public $st_pop;
	public $st_imap;
	public $bl_autenticacaosegura;
	public $bt_autenticacaosegura;
	public $st_conta;
	public $bl_ativo;
	public $dt_cadastro;
	public $nu_portaentrada;
	public $nu_portasaida;
	public $st_usuario;
	public $st_senha;
	public $id_servidoremailentrada;


	/**
	 * @return the $id_emailconfig
	 */
	public function getId_emailconfig(){ 
		return $this->id_emailconfig;
	}

	/**
	 * @return the $id_tipoconexaoemailsaida
	 */
	public function getId_tipoconexaoemailsaida(){ 
		return $this->id_tipoconexaoemailsaida;
	}

	/**
	 * @return the $id_tipoconexaoemailentrada
	 */
	public function getId_tipoconexaoemailentrada(){ 
		return $this->id_tipoconexaoemailentrada;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro(){ 
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade(){ 
		return $this->id_entidade;
	}

	/**
	 * @return the $st_smtp
	 */
	public function getSt_smtp(){ 
		return $this->st_smtp;
	}

	/**
	 * @return the $st_pop
	 */
	public function getSt_pop(){ 
		return $this->st_pop;
	}

	/**
	 * @return the $st_imap
	 */
	public function getSt_imap(){ 
		return $this->st_imap;
	}

	/**
	 * @return the $bl_autenticacaosegura
	 */
	public function getBl_autenticacaosegura(){ 
		return $this->bl_autenticacaosegura;
	}

	/**
	 * @return the $bt_autenticacaosegura
	 */
	public function getBt_autenticacaosegura(){ 
		return $this->bt_autenticacaosegura;
	}

	/**
	 * @return the $st_conta
	 */
	public function getSt_conta(){ 
		return $this->st_conta;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo(){ 
		return $this->bl_ativo;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro(){ 
		return $this->dt_cadastro;
	}

	/**
	 * @return the $nu_portaentrada
	 */
	public function getNu_portaentrada(){ 
		return $this->nu_portaentrada;
	}

	/**
	 * @return the $nu_portasaida
	 */
	public function getNu_portasaida(){ 
		return $this->nu_portasaida;
	}

	/**
	 * @return the $st_usuario
	 */
	public function getSt_usuario(){ 
		return $this->st_usuario;
	}

	/**
	 * @return the $st_senha
	 */
	public function getSt_senha(){ 
		return $this->st_senha;
	}

	/**
	 * @return the $id_servidoremailentrada
	 */
	public function getId_servidoremailentrada(){ 
		return $this->id_servidoremailentrada;
	}


	/**
	 * @param field_type $id_emailconfig
	 */
	public function setId_emailconfig($id_emailconfig){ 
		$this->id_emailconfig = $id_emailconfig;
	}

	/**
	 * @param field_type $id_tipoconexaoemailsaida
	 */
	public function setId_tipoconexaoemailsaida($id_tipoconexaoemailsaida){ 
		$this->id_tipoconexaoemailsaida = $id_tipoconexaoemailsaida;
	}

	/**
	 * @param field_type $id_tipoconexaoemailentrada
	 */
	public function setId_tipoconexaoemailentrada($id_tipoconexaoemailentrada){ 
		$this->id_tipoconexaoemailentrada = $id_tipoconexaoemailentrada;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro){ 
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade){ 
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $st_smtp
	 */
	public function setSt_smtp($st_smtp){ 
		$this->st_smtp = $st_smtp;
	}

	/**
	 * @param field_type $st_pop
	 */
	public function setSt_pop($st_pop){ 
		$this->st_pop = $st_pop;
	}

	/**
	 * @param field_type $st_imap
	 */
	public function setSt_imap($st_imap){ 
		$this->st_imap = $st_imap;
	}

	/**
	 * @param field_type $bl_autenticacaosegura
	 */
	public function setBl_autenticacaosegura($bl_autenticacaosegura){ 
		$this->bl_autenticacaosegura = $bl_autenticacaosegura;
	}

	/**
	 * @param field_type $bt_autenticacaosegura
	 */
	public function setBt_autenticacaosegura($bt_autenticacaosegura){ 
		$this->bt_autenticacaosegura = $bt_autenticacaosegura;
	}

	/**
	 * @param field_type $st_conta
	 */
	public function setSt_conta($st_conta){ 
		$this->st_conta = $st_conta;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo){ 
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro){ 
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @param field_type $nu_portaentrada
	 */
	public function setNu_portaentrada($nu_portaentrada){ 
		$this->nu_portaentrada = $nu_portaentrada;
	}

	/**
	 * @param field_type $nu_portasaida
	 */
	public function setNu_portasaida($nu_portasaida){ 
		$this->nu_portasaida = $nu_portasaida;
	}

	/**
	 * @param field_type $st_usuario
	 */
	public function setSt_usuario($st_usuario){ 
		$this->st_usuario = $st_usuario;
	}

	/**
	 * @param field_type $st_senha
	 */
	public function setSt_senha($st_senha){ 
		$this->st_senha = $st_senha;
	}

	/**
	 * @param field_type $id_servidoremailentrada
	 */
	public function setId_servidoremailentrada($id_servidoremailentrada){ 
		$this->id_servidoremailentrada = $id_servidoremailentrada;
	}

}
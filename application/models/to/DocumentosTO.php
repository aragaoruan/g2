<?php
/**
 * To da tabela de documentos
 * @author edermariano
 * @package models
 * @subpackage to
 */
class DocumentosTO extends Ead1_TO_Dinamico {

	/**
	 * Id de documentos.
	 * @var int
	 */
	public $id_documentos;
	
	/**
	 * Id da entidade dona do documentos.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Nome dos documentos.
	 * @var String
	 */
	public $st_documentos;
	
	/**
	 * Diz se o documento deverá ser digitalizado.
	 * @var Boolean
	 */
	public $bl_digitaliza;
	
	/**
	 * Id do tipo de documento.
	 * @var int
	 */
	public $id_tipodocumento;
	
	/**
	 * Diz se o documento é obrigatório.
	 * @var Boolean
	 */
	public $bl_obrigatorio;
	
	/**
	 * Quantidade de documentos a ser entregue.
	 * @var int
	 */
	public $nu_quantidade;
	
	/**
	 * Id da situação.
	 * @var int
	 */
	public $id_situacao;
	
	/**
	 * Boolean de ativo
	 * @var bool
	 */
	public $bl_ativo;
	
	
	public $id_documentosobrigatoriedade;
	public $id_documentosutilizacao;
	public $id_documentoscategoria;
	
	
	/**
	 * Coleção de obtejos DocumentosSubstituicaoTO; 
	 * @var Ead1_TO_Collection
	 */
	public $list_DocumentosSubstituicaoTO;
	
	
	/**
	 * @return the $id_documentosobrigatoriedade
	 */
	public function getId_documentosobrigatoriedade() {
		return $this->id_documentosobrigatoriedade;
	}

	/**
	 * @return the $id_documentosutilizacao
	 */
	public function getId_documentosutilizacao() {
		return $this->id_documentosutilizacao;
	}

	/**
	 * @return the $id_documentoscategoria
	 */
	public function getId_documentoscategoria() {
		return $this->id_documentoscategoria;
	}

	/**
	 * @param $id_documentosobrigatoriedade the $id_documentosobrigatoriedade to set
	 */
	public function setId_documentosobrigatoriedade($id_documentosobrigatoriedade) {
		$this->id_documentosobrigatoriedade = $id_documentosobrigatoriedade;
	}

	/**
	 * @param $id_documentosutilizacao the $id_documentosutilizacao to set
	 */
	public function setId_documentosutilizacao($id_documentosutilizacao) {
		$this->id_documentosutilizacao = $id_documentosutilizacao;
	}

	/**
	 * @param $id_documentoscategoria the $id_documentoscategoria to set
	 */
	public function setId_documentoscategoria($id_documentoscategoria) {
		$this->id_documentoscategoria = $id_documentoscategoria;
	}

	public function setList_DocumentosSubstituicaoTO($collection)
	{
		$this->list_DocumentosSubstituicaoTO	= $collection; 
	}
	
	public function getList_DocumentosSubstituicaoTO()
	{
		return $this->list_DocumentosSubstituicaoTO;
	}
	
	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}
	
	/**
	 * @param $bl_ativo the $bl_ativo to set
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}
	
	
	/**
	 * @return the $collectionDocumentosSubstituicaoTO
	 */
	public function getCollectionDocumentosSubstituicaoTO() {
		return $this->collectionDocumentosSubstituicaoTO;
	}

	/**
	 * @param $collectionDocumentosSubstituicaoTO the $collectionDocumentosSubstituicaoTO to set
	 */
	public function setCollectionDocumentosSubstituicaoTO($collectionDocumentosSubstituicaoTO) {
		$this->collectionDocumentosSubstituicaoTO = $collectionDocumentosSubstituicaoTO;
	}

	/**
	 * @return Boolean
	 */
	public function getBl_digitaliza() {
		return $this->bl_digitaliza;
	}
	
	/**
	 * @return Boolean
	 */
	public function getBl_obrigatorio() {
		return $this->bl_obrigatorio;
	}
	
	/**
	 * @return int
	 */
	public function getId_documentos() {
		return $this->id_documentos;
	}
	
	/**
	 * @return int
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}
	
	/**
	 * @return int
	 */
	public function getId_tipodocumento() {
		return $this->id_tipodocumento;
	}
	
	/**
	 * @return int
	 */
	public function getNu_quantidade() {
		return $this->nu_quantidade;
	}
	
	/**
	 * @return String
	 */
	public function getSt_documentos() {
		return $this->st_documentos;
	}
	
	/**
	 * @param Boolean $bl_digitaliza
	 */
	public function setBl_digitaliza($bl_digitaliza) {
		$this->bl_digitaliza = $bl_digitaliza;
	}
	
	/**
	 * @param Boolean $bl_obrigatorio
	 */
	public function setBl_obrigatorio($bl_obrigatorio) {
		$this->bl_obrigatorio = $bl_obrigatorio;
	}
	
	/**
	 * @param int $id_documentos
	 */
	public function setId_documentos($id_documentos) {
		$this->id_documentos = $id_documentos;
	}
	
	/**
	 * @param int $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}
	
	/**
	 * @param int $id_tipodocumentos
	 */
	public function setId_tipodocumento($id_tipodocumentos) {
		$this->id_tipodocumento = $id_tipodocumentos;
	}
	
	/**
	 * @param int $nu_quantidade
	 */
	public function setNu_quantidade($nu_quantidade) {
		$this->nu_quantidade = $nu_quantidade;
	}
	
	/**
	 * @param String $st_documentos
	 */
	public function setSt_documentos($st_documentos) {
		$this->st_documentos = $st_documentos;
	}
	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param $id_entidade the $id_entidade to set
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}


}

?>
<?php
/**
 * Classe para encapsular os dados de tipo de endereco.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class TipoEnderecoTO extends Ead1_TO_Dinamico {
	
	const RESIDENCIAL 	= 1; //	Residencial
	const COMERCIAL 	= 2; //	Comercial
	const MATRIZ 		= 3; //	Matriz
	const FILIAL 		= 4; //	Filial
    const CORRESPONDENCIA = 5; // Correspondência

	/**
	 * Contém o id do tipo de endereço.
	 * @var int
	 */
	public $id_tipoendereco;
	
	/**
	 * Contém a descriação do tipo de endereço.
	 * @var string
	 */
	public $st_tipoendereco;
	
	/**
	 * Contém o id da categoria de endereço.
	 * @var int 
	 */
	public $id_categoriaendereco;
	
	/**
	 * @return int
	 */
	public function getId_categoriaendereco() {
		return $this->id_categoriaendereco;
	}
	
	/**
	 * @return int
	 */
	public function getId_tipoendereco() {
		return $this->id_tipoendereco;
	}
	
	/**
	 * @return string
	 */
	public function getSt_tipoendereco() {
		return $this->st_tipoendereco;
	}
	
	/**
	 * @param int $id_categoriaendereco
	 */
	public function setId_categoriaendereco($id_categoriaendereco) {
		$this->id_categoriaendereco = $id_categoriaendereco;
	}
	
	/**
	 * @param int $id_tipoendereco
	 */
	public function setId_tipoendereco($id_tipoendereco) {
		$this->id_tipoendereco = $id_tipoendereco;
	}
	
	/**
	 * @param string $st_tipoendereco
	 */
	public function setSt_tipoendereco($st_tipoendereco) {
		$this->st_tipoendereco = $st_tipoendereco;
	}

}

?>
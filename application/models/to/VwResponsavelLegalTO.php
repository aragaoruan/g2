<?php
/**
 * Classe para encapsular os dados da view de responsável legal.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @since 21/02/2011
 * 
 * @package models
 * @subpackage to
 */
class VwResponsavelLegalTO extends Ead1_TO_Dinamico {

	public $id_entidaderesponsavellegal;
	
	public $id_entidade;
	
	public $bl_padrao;
	
	public $id_situacao;
	
	public $st_situacao;
	
	public $id_tipoentidaderesponsavel;
	
	public $st_tipoentidaderesponsavel;
	
	public $id_usuario;
	
	public $st_registro;
	
	public $sg_uf;
	
	public $st_orgaoexpeditor;
	
	public $st_nomeexibicao;
	/**
	 * @return the $id_entidaderesponsavellegal
	 */
	public function getId_entidaderesponsavellegal() {
		return $this->id_entidaderesponsavellegal;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $bl_padrao
	 */
	public function getBl_padrao() {
		return $this->bl_padrao;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}

	/**
	 * @return the $id_tipoentidaderesponsavel
	 */
	public function getId_tipoentidaderesponsavel() {
		return $this->id_tipoentidaderesponsavel;
	}

	/**
	 * @return the $st_tipoentidaderesponsavel
	 */
	public function getSt_tipoentidaderesponsavel() {
		return $this->st_tipoentidaderesponsavel;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $nu_registro
	 */
	public function getSt_registro() {
		return $this->st_registro;
	}

	/**
	 * @return the $sg_uf
	 */
	public function getSg_uf() {
		return $this->sg_uf;
	}

	/**
	 * @return the $st_orgaoexpeditor
	 */
	public function getSt_orgaoexpeditor() {
		return $this->st_orgaoexpeditor;
	}

	/**
	 * @return the $st_nomeexibicao
	 */
	public function getSt_nomeexibicao() {
		return $this->st_nomeexibicao;
	}

	/**
	 * @param field_type $id_entidaderesponsavellegal
	 */
	public function setId_entidaderesponsavellegal($id_entidaderesponsavellegal) {
		$this->id_entidaderesponsavellegal = $id_entidaderesponsavellegal;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $bl_padrao
	 */
	public function setBl_padrao($bl_padrao) {
		$this->bl_padrao = $bl_padrao;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param field_type $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

	/**
	 * @param field_type $id_tipoentidaderesponsavel
	 */
	public function setId_tipoentidaderesponsavel($id_tipoentidaderesponsavel) {
		$this->id_tipoentidaderesponsavel = $id_tipoentidaderesponsavel;
	}

	/**
	 * @param field_type $st_tipoentidaderesponsavel
	 */
	public function setSt_tipoentidaderesponsavel($st_tipoentidaderesponsavel) {
		$this->st_tipoentidaderesponsavel = $st_tipoentidaderesponsavel;
	}

	/**
	 * @param field_type $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param field_type st_registroo
	 */
	public function setSt_registro($st_registro) {
		$this->st_registro = $st_registro;
	}

	/**
	 * @param field_type $sg_uf
	 */
	public function setSg_uf($sg_uf) {
		$this->sg_uf = $sg_uf;
	}

	/**
	 * @param field_type $st_orgaoexpeditor
	 */
	public function setSt_orgaoexpeditor($st_orgaoexpeditor) {
		$this->st_orgaoexpeditor = $st_orgaoexpeditor;
	}

	/**
	 * @param field_type $st_nomeexibicao
	 */
	public function setSt_nomeexibicao($st_nomeexibicao) {
		$this->st_nomeexibicao = $st_nomeexibicao;
	}

}

?>
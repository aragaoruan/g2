<?php
/**
 * Classe para encapsular os dados de horario e local com a sala de aula.
 * @author Paulo Silva - <paulo.silva@unyleya.com.br>
 * @package models
 * @subpackage to
 */
class SalaHorarioLocalTO extends Ead1_TO_Dinamico {

	
	/**
	 * Id da sala de aula.
	 * @var int
	 */
	public $id_saladeaula;
	
	/**
	 * Id do horario de aula.
	 * @var int
	 */
	public $id_horarioaula;
	
	/**
	 * Id do local.
	 * @var int
	 */
	public $id_localaula;
	
	
	/**
	 * Id do usuario que cadastrou
	 * @var int
	 */
	public $id_usuariocadastro;
	
	/**
	 * Data do Cadastro
	 * @var DateTime
	 */
	
	public $dt_cadastro;
	
	
	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param number $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param DateTime $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @return the $id_saladeaula
	 */
	public function getId_saladeaula() {
		return $this->id_saladeaula;
	}


	/**
	 * @return the $id_horarioaula
	 */
	public function getId_horarioaula() {
		return $this->id_horarioaula;
	}

	/**
	 * @return the $id_localaula
	 */
	public function getId_localaula() {
		return $this->id_localaula;
	}

	/**
	 * @param number $id_saladeaula
	 */
	public function setId_saladeaula($id_saladeaula) {
		$this->id_saladeaula = $id_saladeaula;
	}


	/**
	 * @param number $id_horarioaula
	 */
	public function setId_horarioaula($id_horarioaula) {
		$this->id_horarioaula = $id_horarioaula;
	}

	/**
	 * @param number $id_localaula
	 */
	public function setId_localaula($id_localaula) {
		$this->id_localaula = $id_localaula;
	}

}

?>
<?php
/**
 * Classe para encapsular os dados de endereço.
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class EnderecoEntidadeEnderecoTO extends Ead1_TO_Dinamico{

	/**
	 * Atributo que contém o id do pais da pessoa
	 * @var int
	 */
	public $id_pais;
	
	/**
	 * Atributo que contém a uf da pessoa
	 * @var string
	 */
	public $sg_uf;
	
	/**
	 * Atributo que contém o id do municipio da pessoa
	 * @var int
	 */
	public $id_municipio;
	
	/**
	 * Atributo que contém o id do endereco da pessoa
	 * @var int
	 */
	public $id_endereco;
	
	/**
	 * Atributo que contém o id do tipo de endereco da pessoa
	 * @var int
	 */
	public $id_tipoendereco;
	
	/**
	 * Atributo que verifica se é o endereco padrao da pessoa
	 * @var int
	 */
	public $bl_padrao;
	
	/**
	 * Atributo que contém o cep da pessoa
	 * @var int
	 */
	public $st_cep;
	
	/**
	 * Atributo que contém o endereco da pessoa
	 * @var string
	 */
	public $st_endereco;
	
	/**
	 * Atributo que contém o bairro da pessoa
	 * @var string
	 */
	public $st_bairro;
	
	/**
	 * Atributo que contém o complemento do endereco da pessoa
	 * @var string
	 */
	public $st_complemento;
	
	/**
	 * Atributo que contém o numero de endereco da pessoa
	 * @var int
	 */
	public $nu_numero;
	
	/**
	 * Atributo que contém o estado/provincia da pessoa
	 * @var string
	 */
	public $st_estadoprovincia;
	
	/**
	 * Atributo que contém a cidade da pessoa
	 * @var string
	 */
	public $st_cidade;
	
	/**
	 * Atributo que contem o valor da situacao da pessoa
	 * @var int
	 */
	public $bl_ativo;
	
	/**
	 * Atrbuto que contem o id da entidade
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * @return the $id_pais
	 */
	public function getId_pais() {
		return $this->id_pais;
	}

	/**
	 * @return the $sg_uf
	 */
	public function getSg_uf() {
		return $this->sg_uf;
	}

	/**
	 * @return the $id_municipio
	 */
	public function getId_municipio() {
		return $this->id_municipio;
	}

	/**
	 * @return the $id_endereco
	 */
	public function getId_endereco() {
		return $this->id_endereco;
	}

	/**
	 * @return the $id_tipoendereco
	 */
	public function getId_tipoendereco() {
		return $this->id_tipoendereco;
	}

	/**
	 * @return the $bl_padrao
	 */
	public function getBl_padrao() {
		return $this->bl_padrao;
	}

	/**
	 * @return the $st_cep
	 */
	public function getSt_cep() {
		return $this->st_cep;
	}

	/**
	 * @return the $st_endereco
	 */
	public function getSt_endereco() {
		return $this->st_endereco;
	}

	/**
	 * @return the $st_bairro
	 */
	public function getSt_bairro() {
		return $this->st_bairro;
	}

	/**
	 * @return the $st_complemento
	 */
	public function getSt_complemento() {
		return $this->st_complemento;
	}

	/**
	 * @return the $st_numero
	 */
	public function getNu_numero() {
		return $this->nu_numero;
	}

	/**
	 * @return the $st_estadoprovincia
	 */
	public function getSt_estadoprovincia() {
		return $this->st_estadoprovincia;
	}

	/**
	 * @return the $st_cidade
	 */
	public function getSt_cidade() {
		return $this->st_cidade;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param $id_pais the $id_pais to set
	 */
	public function setId_pais($id_pais) {
		$this->id_pais = $id_pais;
	}

	/**
	 * @param $sg_uf the $sg_uf to set
	 */
	public function setSg_uf($sg_uf) {
		$this->sg_uf = $sg_uf;
	}

	/**
	 * @param $id_municipio the $id_municipio to set
	 */
	public function setId_municipio($id_municipio) {
		$this->id_municipio = $id_municipio;
	}

	/**
	 * @param $id_endereco the $id_endereco to set
	 */
	public function setId_endereco($id_endereco) {
		$this->id_endereco = $id_endereco;
	}

	/**
	 * @param $id_tipoendereco the $id_tipoendereco to set
	 */
	public function setId_tipoendereco($id_tipoendereco) {
		$this->id_tipoendereco = $id_tipoendereco;
	}

	/**
	 * @param $bl_padrao the $bl_padrao to set
	 */
	public function setBl_padrao($bl_padrao) {
		$this->bl_padrao = $bl_padrao;
	}

	/**
	 * @param $st_cep the $st_cep to set
	 */
	public function setSt_cep($st_cep) {
		$this->st_cep = $st_cep;
	}

	/**
	 * @param $st_endereco the $st_endereco to set
	 */
	public function setSt_endereco($st_endereco) {
		$this->st_endereco = $st_endereco;
	}

	/**
	 * @param $st_bairro the $st_bairro to set
	 */
	public function setSt_bairro($st_bairro) {
		$this->st_bairro = $st_bairro;
	}

	/**
	 * @param $st_complemento the $st_complemento to set
	 */
	public function setSt_complemento($st_complemento) {
		$this->st_complemento = $st_complemento;
	}

	/**
	 * @param $st_numero the $st_numero to set
	 */
	public function setNu_numero($nu_numero) {
		$this->nu_numero = $nu_numero;
	}

	/**
	 * @param $st_estadoprovincia the $st_estadoprovincia to set
	 */
	public function setSt_estadoprovincia($st_estadoprovincia) {
		$this->st_estadoprovincia = $st_estadoprovincia;
	}

	/**
	 * @param $st_cidade the $st_cidade to set
	 */
	public function setSt_cidade($st_cidade) {
		$this->st_cidade = $st_cidade;
	}

	/**
	 * @param $bl_ativo the $bl_ativo to set
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @param $id_entidade the $id_entidade to set
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	
}

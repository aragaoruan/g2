<?php
/**
 * Classe para encapsular os dados de relacionamento entre projeto pedagógico e tipo de prova.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class ProjetoTipoProvaTO extends Ead1_TO_Dinamico {

	/**
	 * Id do tipo de prova.
	 * @var int
	 */
	public $id_tipoprova;
	
	/**
	 * id do projeto pedagógico.
	 * @var int
	 */
	public $id_projetopedagogico;
	
	/**
	 * Taxa cobrada para os tipos de prova final.
	 * @var int
	 */
	public $nu_taxa;
	
	/**
	 * @return int
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}
	
	/**
	 * @return int
	 */
	public function getId_tipoprova() {
		return $this->id_tipoprova;
	}
	
	/**
	 * @return int
	 */
	public function getNu_taxa() {
		return $this->nu_taxa;
	}
	
	/**
	 * @param int $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}
	
	/**
	 * @param int $id_tipoprova
	 */
	public function setId_tipoprova($id_tipoprova) {
		$this->id_tipoprova = $id_tipoprova;
	}
	
	/**
	 * @param int $nu_taxa
	 */
	public function setNu_taxa($nu_taxa) {
		$this->nu_taxa = $nu_taxa;
	}

}

?>
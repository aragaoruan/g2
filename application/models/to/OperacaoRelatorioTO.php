<?php
/**
 * Classe para encapsular os dados da tabela de operações do relatórios
 * @author Eder Lamar
 * @package models
 * @subpackage to
 */
class OperacaoRelatorioTO extends Ead1_TO_Dinamico{
	
	public $id_operacaorelatorio;
	public $st_operacaorelatorio;
	
	/**
	 * @return the $id_operacaorelatorio
	 */
	public function getId_operacaorelatorio() {
		return $this->id_operacaorelatorio;
	}

	/**
	 * @return the $st_operacaorelatorio
	 */
	public function getSt_operacaorelatorio() {
		return $this->st_operacaorelatorio;
	}

	/**
	 * @param $id_operacaorelatorio the $id_operacaorelatorio to set
	 */
	public function setId_operacaorelatorio($id_operacaorelatorio) {
		$this->id_operacaorelatorio = $id_operacaorelatorio;
	}

	/**
	 * @param $st_operacaorelatorio the $st_operacaorelatorio to set
	 */
	public function setSt_operacaorelatorio($st_operacaorelatorio) {
		$this->st_operacaorelatorio = $st_operacaorelatorio;
	}

}
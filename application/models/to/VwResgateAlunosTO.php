<?php
/**
 * vw_resgatealunos
 * @package models
 * @subpackage to
 */
class VwResgateAlunosTO extends Ead1_TO_Dinamico {
	
    public $id_alocacao;
    public $id_disciplina;
    public $id_evolucao;
    public $id_matricula;
    public $id_projetopedagogico;
    public $id_saladeaula;
    public $id_situacao;
    public $id_turma;
    public $id_usuario;
    public $nu_aproveitamento;
    public $nu_celular;
    public $nu_diassemacesso;
    public $st_cpf;
    public $st_nomecompleto;
    public $st_projetopedagogico;
    public $st_saladeaula;
    public $st_situacao;
    public $dt_cadastro;
    public $id_ocorrencia;
    public $id_situacaoocorrencia;
    public $id_usuarioresponsavel;
    public $st_usuarioresponsavel;
    public $id_entidade;
    
	/**
	 * @return the $id_alocacao
	 */
	public function getId_alocacao() {
		return $this->id_alocacao;
	}

	/**
	 * @return the $id_disciplina
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}

	/**
	 * @return the $id_evolucao
	 */
	public function getId_evolucao() {
		return $this->id_evolucao;
	}

	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}

	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @return the $id_saladeaula
	 */
	public function getId_saladeaula() {
		return $this->id_saladeaula;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $id_turma
	 */
	public function getId_turma() {
		return $this->id_turma;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $nu_aproveitamento
	 */
	public function getNu_aproveitamento() {
		return $this->nu_aproveitamento;
	}

	/**
	 * @return the $nu_celular
	 */
	public function getNu_celular() {
		return $this->nu_celular;
	}

	/**
	 * @return the $nu_diassemacesso
	 */
	public function getNu_diassemacesso() {
		return $this->nu_diassemacesso;
	}

	/**
	 * @return the $st_cpf
	 */
	public function getSt_cpf() {
		return $this->st_cpf;
	}

	/**
	 * @return the $st_nomecompleto
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}

	/**
	 * @return the $st_projetopedagogico
	 */
	public function getSt_projetopedagogico() {
		return $this->st_projetopedagogico;
	}

	/**
	 * @return the $st_saladeaula
	 */
	public function getSt_saladeaula() {
		return $this->st_saladeaula;
	}

	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param field_type $id_alocacao
	 */
	public function setId_alocacao($id_alocacao) {
		$this->id_alocacao = $id_alocacao;
	}

	/**
	 * @param field_type $id_disciplina
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}

	/**
	 * @param field_type $id_evolucao
	 */
	public function setId_evolucao($id_evolucao) {
		$this->id_evolucao = $id_evolucao;
	}

	/**
	 * @param field_type $id_matricula
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}

	/**
	 * @param field_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @param field_type $id_saladeaula
	 */
	public function setId_saladeaula($id_saladeaula) {
		$this->id_saladeaula = $id_saladeaula;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param field_type $id_turma
	 */
	public function setId_turma($id_turma) {
		$this->id_turma = $id_turma;
	}

	/**
	 * @param field_type $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param field_type $nu_aproveitamento
	 */
	public function setNu_aproveitamento($nu_aproveitamento) {
		$this->nu_aproveitamento = $nu_aproveitamento;
	}

	/**
	 * @param field_type $nu_celular
	 */
	public function setNu_celular($nu_celular) {
		$this->nu_celular = $nu_celular;
	}

	/**
	 * @param field_type $nu_diassemacesso
	 */
	public function setNu_diassemacesso($nu_diassemacesso) {
		$this->nu_diassemacesso = $nu_diassemacesso;
	}

	/**
	 * @param field_type $st_cpf
	 */
	public function setSt_cpf($st_cpf) {
		$this->st_cpf = $st_cpf;
	}

	/**
	 * @param field_type $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @param field_type $st_projetopedagogico
	 */
	public function setSt_projetopedagogico($st_projetopedagogico) {
		$this->st_projetopedagogico = $st_projetopedagogico;
	}

	/**
	 * @param field_type $st_saladeaula
	 */
	public function setSt_saladeaula($st_saladeaula) {
		$this->st_saladeaula = $st_saladeaula;
	}

	/**
	 * @param field_type $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}
        
        /**
	 * @return the $id_ocorrencia
	 */
	public function getId_ocorrencia() {
            return $this->id_ocorrencia;
	}

	/**
	 * @param field_type $id_ocorrencia
	 */
	public function setId_ocorrencia($id_ocorrencia) {
            $this->id_ocorrencia = $id_ocorrencia;
	}
        
        /**
	 * @return the $id_situacaoocorrencia
	 */
        public function getId_situacaoocorrencia() {
            return $this->id_situacaoocorrencia;
	}

	/**
	 * @param field_type $id_situacaoocorrencia
	 */
	public function setId_situacaoocorrencia($id_situacaoocorrencia) {
            $this->id_situacaoocorrencia = $id_situacaoocorrencia;
	}
        
        /**
	 * @return the $id_usuarioresponsavel
	 */
        public function getId_usuarioresponsavel() {
            return $this->id_usuarioresponsavel;
	}

	/**
	 * @param field_type $id_usuarioresponsavel
	 */
	public function setId_usuarioresponsavel($id_usuarioresponsavel) {
            $this->id_usuarioresponsavel = $id_usuarioresponsavel;
	}
        
        /**
	 * @return the $st_usuarioresponsavel
	 */
        public function getSt_usuarioresponsavel() {
            return $this->st_usuarioresponsavel;
	}

	/**
	 * @param field_type $st_usuarioresponsavel
	 */
	public function setSt_usuarioresponsavel($st_usuarioresponsavel) {
            $this->st_usuarioresponsavel = $st_usuarioresponsavel;
	}
        
        /**
	 * @return the $id_entidade
	 */
        public function getId_entidade() {
            return $this->id_entidade;
        }

        /**
	 * @param field_type $id_entidade
	 */
        public function setId_entidade($id_entidade) {
            $this->id_entidade = $id_entidade;
        }


}
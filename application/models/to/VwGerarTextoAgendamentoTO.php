<?php

class VwGerarTextoAgendamentoTO extends Ead1_TO_Dinamico {

    public $dt_agendamento;
    public $st_avaliacao;
    public $st_nomecompleto;
    public $id_avaliacaoagendamento;
    public $id_entidade;
    public $st_login;
    public $st_senha;
    public $st_email;
    public $id_usuario;

    public function getDt_agendamento() {
        return $this->dt_agendamento;
    }

    public function setDt_agendamento($dt_agendamento) {
        $this->dt_agendamento = $dt_agendamento;
        return $this;
    }

    public function getSt_avaliacao() {
        return $this->st_avaliacao;
    }

    public function setSt_avaliacao($st_avaliacao) {
        $this->st_avaliacao = $st_avaliacao;
        return $this;
    }

    public function getSt_nomecompleto() {
        return $this->st_nomecompleto;
    }

    public function setSt_nomecompleto($st_nomecompleto) {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    public function getId_avaliacaoagendamento() {
        return $this->id_avaliacaoagendamento;
    }

    public function setId_avaliacaoagendamento($id_avaliacaoagendamento) {
        $this->id_avaliacaoagendamento = $id_avaliacaoagendamento;
        return $this;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getSt_senha() {
        return $this->st_senha;
    }

    public function setSt_senha($st_senha) {
        $this->st_senha = $st_senha;
        return $this;
    }

    public function getSt_login() {
        return $this->st_login;
    }

    public function setSt_login($st_login) {
        $this->st_login = $st_login;
        return $this;
    }

    public function getSt_email() {
        return $this->st_email;
    }

    public function setSt_email($st_email) {
        $this->st_email = $st_email;
        return $this;
    }

    public function getId_usuario() {
        return $this->id_usuario;
    }

    public function setId_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
        return $this;
    }


}

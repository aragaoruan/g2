<?php
/**
 * Classe para encapsular os dados de relacionamento entre produto e núcleo de telemarketing.
 * @author edermariano
 * 
 * @package models
 * @subpackage to
 */

class NucleoProdutoTO extends Ead1_TO_Dinamico {

	public $id_nucleotm;
	
	public $id_nucleoproduto;
	
	public $id_usuariocadastro;
	
	public $dt_cadastro;
	
	public $bl_ativo;
	
	public $id_produto;
	
	/**
	 * @return unknown
	 */
	public function getId_nucleoproduto() {
		return $this->id_nucleoproduto;
	}
	
	/**
	 * @param unknown_type $id_nucleoproduto
	 */
	public function setId_nucleoproduto($id_nucleoproduto) {
		$this->id_nucleoproduto = $id_nucleoproduto;
	}

	
	/**
	 * @return unknown
	 */
	public function getId_produto() {
		return $this->id_produto;
	}
	
	/**
	 * @param unknown_type $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
	}

	
	/**
	 * @return unknown
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}
	
	/**
	 * @return unknown
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_nucleotm() {
		return $this->id_nucleotm;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}
	
	/**
	 * @param unknown_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}
	
	/**
	 * @param unknown_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}
	
	/**
	 * @param unknown_type $id_nucleotm
	 */
	public function setId_nucleotm($id_nucleotm) {
		$this->id_nucleotm = $id_nucleotm;
	}
	
	/**
	 * @param unknown_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

}

?>
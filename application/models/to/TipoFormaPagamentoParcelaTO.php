<?php
/**
 * Classe para encapsular os dados de tipo de parcela de formas de pagamento.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class TipoFormaPagamentoParcelaTO extends Ead1_TO_Dinamico {

	/**
	 * Id do tipo de parcela de forma de pagamento.
	 * @var int
	 */
	public $id_tipoformapagamentoparcela;
	
	/**
	 * Nome do tipo de parcela de forma de pagamento
	 * @var String
	 */
	public $st_tipoformapagamentoparcela;
	
	/**
	 * @return int
	 */
	public function getId_tipoformapagamentoparcela() {
		return $this->id_tipoformapagamentoparcela;
	}
	
	/**
	 * @return String
	 */
	public function getSt_tipoformapagamentoparcela() {
		return $this->st_tipoformapagamentoparcela;
	}
	
	/**
	 * @param int $id_tipoformapagamentoparcela
	 */
	public function setId_tipoformapagamentoparcela($id_tipoformapagamentoparcela) {
		$this->id_tipoformapagamentoparcela = $id_tipoformapagamentoparcela;
	}
	
	/**
	 * @param String $st_tipoformapagamentoparcela
	 */
	public function setSt_tipoformapagamentoparcela($st_tipoformapagamentoparcela) {
		$this->st_tipoformapagamentoparcela = $st_tipoformapagamentoparcela;
	}

	
}

?>
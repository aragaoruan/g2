<?php
/**
 * Classe para encapsular dados da vw_produtovendalivro
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage to
 */
class VwProdutoVendaLivroTO extends Ead1_TO_Dinamico{
	
	public $id_produto;
	public $st_produto;
	public $id_tipoproduto;
	public $st_tipoproduto;
	public $id_entidade;
	public $id_venda;
	public $nu_valorbruto;
	public $nu_valorliquido;
	public $nu_descontoporcentagem;
	public $nu_descontovalor;
	public $id_campanhacomercial;
	public $st_campanhacomercial;
	public $id_vendaproduto;
	public $id_tipoprodutovalor;
	public $st_tipoprodutovalor;
	public $bl_ativo;
	
	public $id_livro;
	public $id_situacao;
	public $id_entidadecadastro;
	public $id_usuariocadastro;
	public $id_tipolivro;
	public $id_livrocolecao;
	public $st_livro;
	public $st_isbn;
	public $st_codigocontrole;
	public $st_livrocolecao;
	public $st_tipolivro;

	/**
	 * @return mixed
	 */
	public function getid_entidade()
	{
		return $this->id_entidade;
	}

	/**
	 * @param mixed $id_entidade
	 */
	public function setid_entidade($id_entidade)
	{
		$this->id_entidade = $id_entidade;
		return $this;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_tipoprodutovalor() {
		return $this->id_tipoprodutovalor;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_tipoprodutovalor() {
		return $this->st_tipoprodutovalor;
	}
	
	/**
	 * @param unknown_type $id_tipoprodutovalor
	 */
	public function setId_tipoprodutovalor($id_tipoprodutovalor) {
		$this->id_tipoprodutovalor = $id_tipoprodutovalor;
	}
	
	/**
	 * @param unknown_type $st_tipoprodutovalor
	 */
	public function setSt_tipoprodutovalor($st_tipoprodutovalor) {
		$this->st_tipoprodutovalor = $st_tipoprodutovalor;
	}

	
	/**
	 * @return the $id_produto
	 */
	public function getId_produto() {
		return $this->id_produto;
	}

	/**
	 * @return the $st_produto
	 */
	public function getSt_produto() {
		return $this->st_produto;
	}

	/**
	 * @return the $id_tipoproduto
	 */
	public function getId_tipoproduto() {
		return $this->id_tipoproduto;
	}

	/**
	 * @return the $st_tipoproduto
	 */
	public function getSt_tipoproduto() {
		return $this->st_tipoproduto;
	}

	/**
	 * @return the $id_venda
	 */
	public function getId_venda() {
		return $this->id_venda;
	}

	/**
	 * @return the $nu_valorbruto
	 */
	public function getNu_valorbruto() {
		return $this->nu_valorbruto;
	}

	/**
	 * @return the $nu_valorliquido
	 */
	public function getNu_valorliquido() {
		return $this->nu_valorliquido;
	}

	/**
	 * @return the $nu_descontoporcentagem
	 */
	public function getNu_descontoporcentagem() {
		return $this->nu_descontoporcentagem;
	}

	/**
	 * @return the $nu_descontovalor
	 */
	public function getNu_descontovalor() {
		return $this->nu_descontovalor;
	}

	/**
	 * @return the $id_campanhacomercial
	 */
	public function getId_campanhacomercial() {
		return $this->id_campanhacomercial;
	}

	/**
	 * @return the $st_campanhacomercial
	 */
	public function getSt_campanhacomercial() {
		return $this->st_campanhacomercial;
	}

	/**
	 * @param $id_produto the $id_produto to set
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
	}

	/**
	 * @param $st_produto the $st_produto to set
	 */
	public function setSt_produto($st_produto) {
		$this->st_produto = $st_produto;
	}

	/**
	 * @param $id_tipoproduto the $id_tipoproduto to set
	 */
	public function setId_tipoproduto($id_tipoproduto) {
		$this->id_tipoproduto = $id_tipoproduto;
	}

	/**
	 * @param $st_tipoproduto the $st_tipoproduto to set
	 */
	public function setSt_tipoproduto($st_tipoproduto) {
		$this->st_tipoproduto = $st_tipoproduto;
	}

	/**
	 * @param $id_venda the $id_venda to set
	 */
	public function setId_venda($id_venda) {
		$this->id_venda = $id_venda;
	}

	/**
	 * @param $nu_valorbruto the $nu_valorbruto to set
	 */
	public function setNu_valorbruto($nu_valorbruto) {
		$this->nu_valorbruto = $nu_valorbruto;
	}

	/**
	 * @param $nu_valorliquido the $nu_valorliquido to set
	 */
	public function setNu_valorliquido($nu_valorliquido) {
		$this->nu_valorliquido = $nu_valorliquido;
	}

	/**
	 * @param $nu_descontoporcentagem the $nu_descontoporcentagem to set
	 */
	public function setNu_descontoporcentagem($nu_descontoporcentagem) {
		$this->nu_descontoporcentagem = $nu_descontoporcentagem;
	}

	/**
	 * @param $nu_descontovalor the $nu_descontovalor to set
	 */
	public function setNu_descontovalor($nu_descontovalor) {
		$this->nu_descontovalor = $nu_descontovalor;
	}

	/**
	 * @param $id_campanhacomercial the $id_campanhacomercial to set
	 */
	public function setId_campanhacomercial($id_campanhacomercial) {
		$this->id_campanhacomercial = $id_campanhacomercial;
	}

	/**
	 * @param $st_campanhacomercial the $st_campanhacomercial to set
	 */
	public function setSt_campanhacomercial($st_campanhacomercial) {
		$this->st_campanhacomercial = $st_campanhacomercial;
	}
	
	/**
	 * @return the $id_vendaproduto
	 */
	public function getId_vendaproduto() {
		return $this->id_vendaproduto;
	}

	/**
	 * @param field_type $id_vendaproduto
	 */
	public function setId_vendaproduto($id_vendaproduto) {
		$this->id_vendaproduto = $id_vendaproduto;
	}
	
	/**
	 * @param boolean $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}


	/**
	 * @return the $id_livro
	 */
	public function getId_livro() {
		return $this->id_livro;
	}
	
	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}
	
	/**
	 * @return the $id_entidadecadastro
	 */
	public function getId_entidadecadastro() {
		return $this->id_entidadecadastro;
	}
	
	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}
	
	/**
	 * @return the $id_tipolivro
	 */
	public function getId_tipolivro() {
		return $this->id_tipolivro;
	}
	
	/**
	 * @return the $id_livrocolecao
	 */
	public function getId_livrocolecao() {
		return $this->id_livrocolecao;
	}
	
	/**
	 * @return the $st_livro
	 */
	public function getSt_livro() {
		return $this->st_livro;
	}
	
	/**
	 * @return the $st_isbn
	 */
	public function getSt_isbn() {
		return $this->st_isbn;
	}
	
	/**
	 * @return the $st_codigocontrole
	 */
	public function getSt_codigocontrole() {
		return $this->st_codigocontrole;
	}
	
	/**
	 * @return the $st_livrocolecao
	 */
	public function getSt_livrocolecao() {
		return $this->st_livrocolecao;
	}
	
	/**
	 * @return the $st_tipolivro
	 */
	public function getSt_tipolivro() {
		return $this->st_tipolivro;
	}
	
	
	/**
	 * @param field_type $id_livro
	 */
	public function setId_livro($id_livro) {
		$this->id_livro = $id_livro;
	}
	
	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}
	
	/**
	 * @param field_type $id_entidadecadastro
	 */
	public function setId_entidadecadastro($id_entidadecadastro) {
		$this->id_entidadecadastro = $id_entidadecadastro;
	}
	
	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}
	
	/**
	 * @param field_type $id_tipolivro
	 */
	public function setId_tipolivro($id_tipolivro) {
		$this->id_tipolivro = $id_tipolivro;
	}
	
	/**
	 * @param field_type $id_livrocolecao
	 */
	public function setId_livrocolecao($id_livrocolecao) {
		$this->id_livrocolecao = $id_livrocolecao;
	}
	
	/**
	 * @param field_type $st_livro
	 */
	public function setSt_livro($st_livro) {
		$this->st_livro = $st_livro;
	}
	
	/**
	 * @param field_type $st_isbn
	 */
	public function setSt_isbn($st_isbn) {
		$this->st_isbn = $st_isbn;
	}
	
	/**
	 * @param field_type $st_codigocontrole
	 */
	public function setSt_codigocontrole($st_codigocontrole) {
		$this->st_codigocontrole = $st_codigocontrole;
	}
	
	
	/**
	 * @param field_type $st_livrocolecao
	 */
	public function setSt_livrocolecao($st_livrocolecao) {
		$this->st_livrocolecao = $st_livrocolecao;
	}
	
	
	
}
<?php

/**
 * Classe de VwRemessaMaterialTO
 
 * @package models
 * @subpackage to
 */
class VwRemessaMaterialTO extends Ead1_TO_Dinamico {

	public $id_projetopedagogico;
	public $id_situacao;
	public $nu_remessa;
	public $id_remessa;
	public $dt_inicio;
	public $dt_fim;
	public $id_itemdematerial;
	public $st_itemdematerial;
	public $id_disciplina;
	public $st_disciplina;
	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @param field_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @return the $nu_remessa
	 */
	public function getNu_remessa() {
		return $this->nu_remessa;
	}

	/**
	 * @param field_type $nu_remessa
	 */
	public function setNu_remessa($nu_remessa) {
		$this->nu_remessa = $nu_remessa;
	}

	/**
	 * @return the $id_remessa
	 */
	public function getId_remessa() {
		return $this->id_remessa;
	}

	/**
	 * @param field_type $id_remessa
	 */
	public function setId_remessa($id_remessa) {
		$this->id_remessa = $id_remessa;
	}

	/**
	 * @return the $dt_inicio
	 */
	public function getDt_inicio() {
		return $this->dt_inicio;
	}

	/**
	 * @param field_type $dt_inicio
	 */
	public function setDt_inicio($dt_inicio) {
		$this->dt_inicio = $dt_inicio;
	}

	/**
	 * @return the $dt_fim
	 */
	public function getDt_fim() {
		return $this->dt_fim;
	}

	/**
	 * @param field_type $dt_fim
	 */
	public function setDt_fim($dt_fim) {
		$this->dt_fim = $dt_fim;
	}

	/**
	 * @return the $id_itemdematerial
	 */
	public function getId_itemdematerial() {
		return $this->id_itemdematerial;
	}

	/**
	 * @param field_type $id_itemdematerial
	 */
	public function setId_itemdematerial($id_itemdematerial) {
		$this->id_itemdematerial = $id_itemdematerial;
	}

	/**
	 * @return the $st_itemdematerial
	 */
	public function getSt_itemdematerial() {
		return $this->st_itemdematerial;
	}

	/**
	 * @param field_type $st_itemdematerial
	 */
	public function setSt_itemdematerial($st_itemdematerial) {
		$this->st_itemdematerial = $st_itemdematerial;
	}

	/**
	 * @return the $id_disciplina
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}

	/**
	 * @param field_type $id_disciplina
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}

	/**
	 * @return the $st_disciplina
	 */
	public function getSt_disciplina() {
		return $this->st_disciplina;
	}

	/**
	 * @param field_type $st_disciplina
	 */
	public function setSt_disciplina($st_disciplina) {
		$this->st_disciplina = $st_disciplina;
	}

	
}

?>
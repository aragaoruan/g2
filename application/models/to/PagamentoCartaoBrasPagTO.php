<?php
class PagamentoCartaoBrasPagTO extends Ead1_TO_Dinamico {
	
	const CIELO_VISA 			= 500;
	const CIELO_MASTERCARD 		= 501;
	const CIELO_AMEX 			= 502;
	const CIELO_DINERS			= 503;
	const CIELO_ELO 			= 504;


    const REDECARD_VISA			= 509;
	const REDECARD_MASTERCARD	= 510;
	const REDECARD_DINERS		= 511;
	
	const SIMULADO				= 997;
	
	const CIELO 				= 1;
	const REDECARD 				= 2;
	
	const RECORRENTE_VISA		= 71;
	const RECORRENTE_AMEX		= 73;
	const RECORRENTE_MASTER		= 120;
	const RECORRENTE_ELO		= 126;
	const RECORRENTE_DINERS	    = 999999999999;

	var $erro = array (
			'110' => 'O Valor precisa ser maior que ZERO.',
			'111' => 'O Número do Cartão é OBRIGATÓRIO.',
			'112' => 'A Validade do Cartão é OBRIGATÓRIA.',		
			'113' => 'Validade do Cartão INVÁLIDA.',		
			'114' => 'Quantidade de Parcelas INVÁLIDO.',		
			'115' => 'Meio de pagamento inválido, entre em contato com a Instituição.',		
			'118' => 'Tipo de Transação INVÁLIDA.',		
			'121' => 'O Nome é OBRIGATÓRIO.',		
			'126' => 'Método de Pagamento não habilitado, entre em contato com a Instituição.',		
			'128' => 'Tipo de Transação INVÁLIDA.',	
			'134' => 'Endereço de E-mail INVÁLIDO.',		
			'199' => 'Erro INDEFINIDO.'	
			);
	
	
	
	public $id_meiopagamento;
	
	/**
	 * Id da venda para buscar seus lançamentos
	 * @var int
	 */
	public $id_venda;
	
	/**
	 * Id da bandeira utilizada para o pagamento ( VISA | MASTERCARD | AMEX)
	 * @var int
	 */
	public $id_cartaobandeira;
	
	/**
	 * Id da operadora utilizada para o pagamento ( CIELO | REDECARD )
	 * @var int
	 */
	public $id_cartaooperadora;
	
	/**
	 * Id da transação gerada para o pagamento dos lançamentos
	 * @var int
	 */
	public $id_transacao;
	
	/**
	 * Código de Segurança
	 * @var String
	 */
	public $st_codigoseguranca;
	
	/**
	 * Núemro do cartão de crédito
	 * @var String
	 */
	public $st_cartao;
	
	/**
	 * Ano de validade
	 * @var int
	 */
	public $nu_anovalidade;
	
	/**
	 * Mês de validade
	 * @var int
	 */
	public $nu_mesvalidade;
	
	
	/**
	 * Valor total, formato 100 = R$ 1,00
	 * @var int
	 */
	public $nu_valortotal;
	
	/**
	 * Quantidade de Parcelas
	 * @var int
	 */
	public $nu_parcelas;
	
	/**
	 * Nome do portador do cartão
	 * @var string
	 */
	public $st_nomeimpresso;
	
	
	/**
	 * Data de inicio no caso do pagamento recorrente
	 * @var string
	 */
	public $dt_inicio;
	
	/**
	 * Data de Termino no caso do pagamento recorrente
	 * @var String
	 */
	public $dt_termino;	
	
	/**
	 * Valor da parcela
	 * @var string
	 */
	public $nu_valorparcela;
	
	
	/**
	 * @return the $id_meiopagamento
	 */
	public function getId_meiopagamento() {
		return $this->id_meiopagamento;
	}

	/**
	 * @param field_type $id_meiopagamento
	 */
	public function setId_meiopagamento($id_meiopagamento) {
		$this->id_meiopagamento = $id_meiopagamento;
	}

	/**
	 * @return the $nu_valorparcela
	 */
	public function getNu_valorparcela() {
		return $this->nu_valorparcela;
	}

	/**
	 * @param string $nu_valorparcela
	 */
	public function setNu_valorparcela($nu_valorparcela) {
		$this->nu_valorparcela = $nu_valorparcela;
	}

	/**
	 * @return the $dt_inicio
	 */
	public function getDt_inicio() {
		return $this->dt_inicio;
	}

	/**
	 * @return the $dt_termino
	 */
	public function getDt_termino() {
		return $this->dt_termino;
	}

	/**
	 * @param string $dt_inicio
	 */
	public function setDt_inicio($dt_inicio) {
		$this->dt_inicio = $dt_inicio;
	}

	/**
	 * @param string $dt_termino
	 */
	public function setDt_termino($dt_termino) {
		$this->dt_termino = $dt_termino;
	}

	/**
	 * @return the $id_venda
	 */
	public function getId_venda() {
		return $this->id_venda;
	}

	/**
	 * @param number $id_venda
	 */
	public function setId_venda($id_venda) {
		$this->id_venda = $id_venda;
	}

	
	/**
	 * @return the $id_cartaooperadora
	 */
	public function getId_cartaooperadora() {
		return $this->id_cartaooperadora;
	}
	
	/**
	 * @param number $id_cartaooperadora
	 */
	public function setId_cartaooperadora($id_cartaooperadora) {
		$this->id_cartaooperadora = $id_cartaooperadora;
	}
	
	/**
	 * @return the $id_cartaobandeira
	 */
	public function getId_cartaobandeira() {

		/**
		 * Quando for efetuar testes, favor descomentar essa linha abaixo
		 */
//		if(Ead1_Ambiente::getAmbiente()!=Ead1_Ambiente::AMBIENTE_PRODUCAO && $this->getId_meiopagamento()==MeioPagamentoTO::CARTAO){
//			return self::SIMULADO;
//		}
		
		if(!$this->getId_cartaooperadora())
			return false;

		
		switch ($this->getId_cartaooperadora())	{
			
			/**
			 * Meios de pagamento habilitados:
				Cielo - 1035621174
				500 - Visa
				501 - Mastercard
				502 - Amex (confirmar liberação de bandeira na Cielo)
				503 - Diners
				504 - Elo 
			 */
			case self::CIELO:
				switch ($this->id_cartaobandeira){
					case CartaoBandeiraTO::BANDEIRA_VISA: 
						return ($this->getId_meiopagamento()==MeioPagamentoTO::CARTAO ? self::CIELO_VISA : self::RECORRENTE_VISA); 
						break;
					case CartaoBandeiraTO::BANDEIRA_MASTERCARD:
						return ($this->getId_meiopagamento()==MeioPagamentoTO::CARTAO ? self::CIELO_MASTERCARD : self::RECORRENTE_MASTER);
						break;
					case CartaoBandeiraTO::BANDEIRA_AMEX: 
						return ($this->getId_meiopagamento()==MeioPagamentoTO::CARTAO ? self::CIELO_AMEX : self::RECORRENTE_AMEX); 
						break;
					case CartaoBandeiraTO::BANDEIRA_ELO:
						return ($this->getId_meiopagamento()==MeioPagamentoTO::CARTAO ? self::CIELO_ELO : self::RECORRENTE_ELO);
						break;
					case CartaoBandeiraTO::BANDEIRA_DINERS:
						return ($this->getId_meiopagamento()==MeioPagamentoTO::CARTAO ? self::CIELO_DINERS : self::RECORRENTE_DINERS);
						break;
					default: return false;
				}				
			break;
			/**
			 * RedeCard não Liberado ainda
			 */
// 			case self::REDECARD:
// 				switch ($this->id_cartaobandeira){
// 					case CartaoBandeiraTO::BANDEIRA_VISA: return self::REDECARD_VISA; break;
// 					case CartaoBandeiraTO::BANDEIRA_MASTERCARD: return self::REDECARD_MASTERCARD; break;
// 					case CartaoBandeiraTO::BANDEIRA_DINERS: return self::REDECARD_DINERS; break;
// 					default: return false;
// 				}
// 			break;	
			default: return false;
		}
		
	}

	/**
	 * @param number $id_cartaobandeira
	 */
	public function setId_cartaobandeira($id_cartaobandeira) {
		$this->id_cartaobandeira = $id_cartaobandeira;
	}


	/**
	 * @return the $id_transacao
	 */
	public function getId_transacao() {
		return $this->id_transacao;
	}

	/**
	 * @param number $id_transacao
	 */
	public function setId_transacao($id_transacao) {
		$this->id_transacao = $id_transacao;
	}

	/**
	 * @return the $st_codigoseguranca
	 */
	public function getSt_codigoseguranca() {
		return $this->st_codigoseguranca;
	}

	/**
	 * @param string $st_codigoseguranca
	 */
	public function setSt_codigoseguranca($st_codigoseguranca) {
		$this->st_codigoseguranca = $st_codigoseguranca;
	}

	/**
	 * @return the $st_cartao
	 */
	public function getSt_cartao() {
		return $this->st_cartao;
	}

	/**
	 * @param string $st_cartao
	 */
	public function setSt_cartao($st_cartao) {
		$this->st_cartao = $st_cartao;
	}

	/**
	 * @return the $nu_anovalidade
	 */
	public function getNu_anovalidade() {
		return $this->nu_anovalidade;
	}

	/**
	 * @param number $nu_anovalidade
	 */
	public function setNu_anovalidade($nu_anovalidade) {
		$this->nu_anovalidade = $nu_anovalidade;
	}

	/**
	 * @return the $nu_mesvalidade
	 */
	public function getNu_mesvalidade() {
		
		return str_pad($this->nu_mesvalidade, 2, "0", STR_PAD_LEFT);
		
	}

	/**
	 * @param number $nu_mesvalidade
	 */
	public function setNu_mesvalidade($nu_mesvalidade) {
		$this->nu_mesvalidade = $nu_mesvalidade;
	}

	/**
	 * @return the $nu_valortotal
	 */
	public function getNu_valortotal() {
		return $this->nu_valortotal;
	}

	/**
	 * @param number $nu_valortotal
	 */
	public function setNu_valortotal($nu_valortotal) {
		$this->nu_valortotal = $nu_valortotal;
	}

	/**
	 * @return the $nu_parcelas
	 */
	public function getnu_parcelas() {
		
		if(!$this->nu_parcelas)
			$this->setnu_parcelas(1);
		
		
		return $this->nu_parcelas;
	}

	/**
	 * @param number $nu_parcelas
	 */
	public function setnu_parcelas($nu_parcelas) {
		$this->nu_parcelas = $nu_parcelas;
	}

	/**
	 * @return the $st_nomeimpresso
	 */
	public function getSt_nomeimpresso() {
		return $this->st_nomeimpresso;
	}

	/**
	 * @param string $st_nomeimpresso
	 */
	public function setSt_nomeimpresso($st_nomeimpresso) {
		$this->st_nomeimpresso = $st_nomeimpresso;
	}

	
	
}
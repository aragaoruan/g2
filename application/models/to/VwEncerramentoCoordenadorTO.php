<?php

/**
 * Classe para encapsular os dados da view de encerramento do coordenador
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 *
 * @package models
 * @subpackage to
 */
class VwEncerramentoCoordenadorTO extends Ead1_TO_Dinamico
{

    public $id_saladeaula;
    public $id_encerramentosala;
    public $id_disciplina;
    public $id_usuariocoordenador;
    public $id_usuariopedagogico;
    public $id_usuarioprofessor;
    public $id_usuariofinanceiro;
    public $dt_encerramentoprofessor;
    public $dt_encerramentocoordenador;
    public $dt_encerramentopedagogico;
    public $dt_encerramentofinanceiro;
    public $st_disciplina;
    public $st_saladeaula;
    public $id_entidade;
    public $id_coordenador;
    public $id_areaconhecimento;
    public $st_areaconhecimento;
    public $st_aluno;
    public $st_tituloavaliacao;
    public $id_tipodisciplina;
    public $id_matricula;
    public $nu_cargahoraria;
    public $st_upload;
    public $id_aluno;
    public $st_urlinteracoes;
    public $dt_recusa;
    public $id_usuariorecusa;
    public $st_motivorecusa;
    public $st_encerramentoprofessor;
    public $st_encerramentocoordenador;
    public $st_encerramentopedagogico;
    public $st_encerramentofinanceiro;

    /**
     * @param mixed $id_aluno
     */
    public function setId_aluno($id_aluno)
    {
        $this->id_aluno = $id_aluno;
    }

    /**
     * @return mixed
     */
    public function getId_aluno()
    {
        return $this->id_aluno;
    }

    /**
     * @return string
     */
    public function getSt_aluno()
    {
        return $this->st_aluno;
    }

    /**
     * @param string $st_aluno
     */
    public function setSt_aluno($st_aluno)
    {
        $this->st_aluno = $st_aluno;
    }

    /**
     * @return string
     */
    public function getSt_tituloavaliacao()
    {
        return $this->st_tituloavaliacao;
    }

    /**
     * @param string $st_tituloavaliacao
     */
    public function setSt_tituloavaliacao($st_tituloavaliacao)
    {
        $this->st_tituloavaliacao = $st_tituloavaliacao;
    }

    /**
     * @return int
     */
    public function getId_encerramentosala()
    {
        return $this->id_encerramentosala;
    }

    /**
     * @param int $id_encerramentosala
     */
    public function setId_encerramentosala($id_encerramentosala)
    {
        $this->id_encerramentosala = $id_encerramentosala;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return int
     */
    public function getId_areaconhecimento()
    {
        return $this->id_areaconhecimento;
    }

    /**
     * @param int $id_areaconhecimento
     */
    public function setId_areaconhecimento($id_areaconhecimento)
    {
        $this->id_areaconhecimento = $id_areaconhecimento;
    }

    /**
     * @return int
     */
    public function getId_tipodisciplina()
    {
        return $this->id_tipodisciplina;
    }

    /**
     * @param int $id_tipodisciplina
     */
    public function setId_tipodisciplina($id_tipodisciplina)
    {
        $this->id_tipodisciplina = $id_tipodisciplina;
    }

    /**
     * @return int
     */
    public function getId_coordenador()
    {
        return $this->id_coordenador;
    }

    /**
     * @param int $id_coordenador
     */
    public function setId_coordenador($id_coordenador)
    {
        $this->id_coordenador = $id_coordenador;
    }

    /**
     * @return int
     */
    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param int $id_saladeaula
     */
    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
    }

    /**
     * @return int
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param int $id_disciplina
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
    }

    /**
     * @return int
     */
    public function getId_usuarioprofessor()
    {
        return $this->id_usuarioprofessor;
    }

    /**
     * @param int $id_usuarioprofessor
     */
    public function setId_usuarioprofessor($id_usuarioprofessor)
    {
        $this->id_usuarioprofessor = $id_usuarioprofessor;
    }

    /**
     * @return int
     */
    public function getId_usuariocoordenador()
    {
        return $this->id_usuariocoordenador;
    }

    /**
     * @param int $id_usuariocoordenador
     */
    public function setId_usuariocoordenador($id_usuariocoordenador)
    {
        $this->id_usuariocoordenador = $id_usuariocoordenador;
    }

    /**
     * @return int
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @return int
     */
    public function getId_usuariopedagogico()
    {
        return $this->id_usuariopedagogico;
    }

    /**
     * @param int $id_usuariopedagogico
     */
    public function setId_usuariopedagogico($id_usuariopedagogico)
    {
        $this->id_usuariopedagogico = $id_usuariopedagogico;
    }

    /**
     * @return int
     */
    public function getId_usuariofinanceiro()
    {
        return $this->id_usuariofinanceiro;
    }

    /**
     * @param int $id_usuariofinanceiro
     */
    public function setId_usuariofinanceiro($id_usuariofinanceiro)
    {
        $this->id_usuariofinanceiro = $id_usuariofinanceiro;
    }

    public function getDt_encerramentoprofessor()
    {
        return $this->dt_encerramentoprofessor;
    }

    public function setDt_encerramentoprofessor($dt_encerramentoprofessor)
    {
        $this->dt_encerramentoprofessor = $dt_encerramentoprofessor;
    }

    public function getDt_encerramentocoordenador()
    {
        return $this->dt_encerramentocoordenador;
    }

    public function setDt_encerramentocoordenador($dt_encerramentocoordenador)
    {
        $this->dt_encerramentocoordenador = $dt_encerramentocoordenador;
    }

    public function getDt_encerramentopedagogico()
    {
        return $this->dt_encerramentopedagogico;
    }

    public function setDt_encerramentopedagogico($dt_encerramentopedagogico)
    {
        $this->dt_encerramentopedagogico = $dt_encerramentopedagogico;
    }

    public function getDt_encerramentofinanceiro()
    {
        return $this->dt_encerramentofinanceiro;
    }

    public function setDt_encerramentofinanceiro($dt_encerramentofinanceiro)
    {
        $this->dt_encerramentofinanceiro = $dt_encerramentofinanceiro;
    }

    /**
     * @return string
     */
    public function getSt_disciplina()
    {
        return $this->st_disciplina;
    }

    /**
     * @param string $st_disciplina
     */
    public function setSt_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
    }

    /**
     * @return string
     */
    public function getSt_areaconhecimento()
    {
        return $this->st_areaconhecimento;
    }

    /**
     * @param string $st_areaconhecimento
     */
    public function setSt_areaconhecimento($st_areaconhecimento)
    {
        $this->st_areaconhecimento = $st_areaconhecimento;
    }

    /**
     * @return string
     */
    public function getSt_saladeaula()
    {
        return $this->st_saladeaula;
    }

    /**
     * @param string $st_saladeaula
     */
    public function setSt_saladeaula($st_saladeaula)
    {
        $this->st_saladeaula = $st_saladeaula;
    }

    /**
     * @return $nu_cargahoraria
     */
    public function getNu_cargahoraria()
    {
        return $this->nu_cargahoraria;
    }

    /**
     * @param $nu_cargahoraria
     */
    public function setNu_cargahoraria($nu_cargahoraria)
    {
        $this->nu_cargahoraria = $nu_cargahoraria;
    }

    /**
     * @return $st_upload
     * @author Rafael Bruno <rafaelbruno.ti@gmail.com>
     */
    public function getSt_upload()
    {
        return $this->st_upload;
    }

    /**
     * @param $st_upload
     * @author Rafael Bruno <rafaelbruno.ti@gmail.com>
     */
    public function setSt_upload($st_upload)
    {
        $this->st_upload = $st_upload;
    }

    /**
     * @param mixed $st_urlinteracoes
     */
    public function setSt_urlinteracoes($st_urlinteracoes)
    {
        $this->st_urlinteracoes = $st_urlinteracoes;
    }

    /**
     * @return mixed
     */
    public function getSt_urlinteracoes()
    {
        return $this->st_urlinteracoes;
    }

    /**
     * @param mixed $dt_recusa
     */
    public function setDt_recusa($dt_recusa)
    {
        $this->dt_recusa = $dt_recusa;
    }

    /**
     * @return mixed
     */
    public function getDt_recusa()
    {
        return $this->dt_recusa;
    }

    /**
     * @param mixed $id_usuariorecusa
     */
    public function setId_usuariorecusa($id_usuariorecusa)
    {
        $this->id_usuariorecusa = $id_usuariorecusa;
    }

    /**
     * @return mixed
     */
    public function getId_usuariorecusa()
    {
        return $this->id_usuariorecusa;
    }

    /**
     * @param mixed $st_motivorecusa
     */
    public function setSt_motivorecusa($st_motivorecusa)
    {
        $this->st_motivorecusa = $st_motivorecusa;
    }

    /**
     * @return mixed
     */
    public function getSt_motivorecusa()
    {
        return $this->st_motivorecusa;
    }

    public function getSt_encerramentoprofessor()
    {
        return $this->st_encerramentoprofessor;
    }

    public function setSt_encerramentoprofessor($st_encerramentoprofessor)
    {
        $this->st_encerramentoprofessor = $st_encerramentoprofessor;
    }

    public function getSt_encerramentocoordenador()
    {
        return $this->st_encerramentocoordenador;
    }

    public function setSt_encerramentocoordenador($st_encerramentocoordenador)
    {
        $this->st_encerramentocoordenador = $st_encerramentocoordenador;
    }

    public function getSt_encerramentopedagogico()
    {
        return $this->st_encerramentopedagogico;
    }

    public function setSt_encerramentopedagogico($st_encerramentopedagogico)
    {
        $this->st_encerramentopedagogico = $st_encerramentopedagogico;
    }

    public function getSt_encerramentofinanceiro()
    {
        return $this->st_encerramentofinanceiro;
    }

    public function setSt_encerramentofinanceiro($st_encerramentofinanceiro)
    {
        $this->st_encerramentofinanceiro = $st_encerramentofinanceiro;
    }




}

<?php
/**
 * Classe para encapsular os dados de entidade.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class EntidadeTO extends Ead1_TO_Dinamico {

	const EAD1 = 1;
	const AVM = 14;

	/**
	 * Atributo que contém a imagem do logo da entidade.
	 * @var String
	 */
	public $str_urlimglogoentidade;

	/**
	 * Atributo que contém o id da entidade.
	 * @var int
	 */
	public $id_entidade;

	/**
	 * Atributo que contém o id da entidade pai dessa entidade.
	 * @var int
	 */
	public $id_entidadecadastro;

	/**
	 * Atributo que indica se a entidade é ativa ou inativa.
	 * @var boolean
	 */
	public $bl_ativo;

	/**
	 * Atributo que contém o nome da entidade
	 * @var string
	 */
	public $st_nomeentidade;

	/**
	 * Atributo que contém a sigla da entidade
	 * @var string
	 */
	public $st_siglaentidade;

	/**
	 * Atributo que contém a imagem do logo da entidade.
	 * @var String
	 */
	public $st_urlimglogo;

	/**
	 * Atributo que contém o id da situação da entidade.
	 * @var int
	 */
	public $id_situacao;

	/**
	 * Diz se a entidade usa ou não o sistema.
	 * @var Boolean
	 */
	public $bl_acessasistema;

	/**
	 * Contém o número de CNPJ da entidade.
	 * @var String
	 */
	public $st_cnpj;

	/**
	 * Contém a razão social da instituição.
	 * @var String
	 */
	public $st_razaosocial;

	/**
	 * Contém o número da inscrição estadual.
	 * @var String
	 */
	public $nu_inscricaoestadual;

	/**
	 * Contém a url do site da entidade.
	 * @var string
	 */
	public $st_urlsite;

	/**
	 * Contém o apelido da entidade.
	 * @var string
	 */
	public $st_apelido;

	/**
	 * Id do usuário que cadastrou a entidade.
	 * @var int
	 */
	public $id_usuariocadastro;

	/**
	 * Data de cadastro da entidade.
	 * @var string
	 */
	public $dt_cadastro;


	/**
	 * Atributo que contém o esquema de configuração utilizado na entidade
	 * @var int
	 */
	public $id_esquemaconfiguracao;

    /**
     * @var int
     */
    public $id_usuariosecretariado;

    /**
	 * @return string
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @return int
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @param string $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = ($dt_cadastro ? $dt_cadastro : date("Y-m-d h:i:s"));
	}

	/**
	 * @param int $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}
	/**
	 * @return string
	 */
	public function getSt_urlsite() {
		return $this->st_urlsite;
	}

	/**
	 * @param string $st_urlsite
	 */
	public function setSt_urlsite($st_urlsite) {
		$this->st_urlsite = $st_urlsite;
	}
	/**
	 * @return String
	 */
	public function getNu_inscricaoestadual() {
		return $this->nu_inscricaoestadual;
	}

	/**
	 * @param String $nu_inscricaoestadual
	 */
	public function setNu_inscricaoestadual($nu_inscricaoestadual) {
		$this->nu_inscricaoestadual = $nu_inscricaoestadual;
	}
	/**
	 * @return String
	 */
	public function getSt_razaosocial() {
		return $this->st_razaosocial;
	}

	/**
	 * @param String $st_razaosocial
	 */
	public function setSt_razaosocial($st_razaosocial) {
		$this->st_razaosocial = $st_razaosocial;
	}
	/**
	 * @return Boolean
	 */
	public function getBl_acessasistema() {
		return $this->bl_acessasistema;
	}

	/**
	 * @return int
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return String
	 */
	public function getSt_cnpj() {
		return $this->st_cnpj;
	}

	/**
	 * @param Boolean $bl_acessasistema
	 */
	public function setBl_acessasistema($bl_acessasistema) {
		$this->bl_acessasistema = $bl_acessasistema;
	}

	/**
	 * @param int $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param String $st_cnpj
	 */
	public function setSt_cnpj($st_cnpj) {
		$this->st_cnpj = $st_cnpj;
	}
	/**
	 * @return String
	 */
	public function getSt_urlimglogo() {
		return $this->st_urlimglogo;
	}

	/**
	 * @param String $st_urlimglogo
	 */
	public function setSt_urlimglogo($st_urlimglogo) {
		$this->st_urlimglogo = $st_urlimglogo;
	}


	/**
	 * @return boolean
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return int
	 */
	public function getId_entidadecadastro() {
		return $this->id_entidadecadastro;
	}

	/**
	 * @return string
	 */
	public function getSt_nomeentidade() {
		return $this->st_nomeentidade;
	}

	/**
	 * @param boolean $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param int $id_entidadecadastro
	 */
	public function setId_entidadecadastro($id_entidadecadastro) {
		$this->id_entidadecadastro = $id_entidadecadastro;
	}

	/**
	 * @param string $st_nomeentidade
	 */
	public function setSt_nomeentidade($st_nomeentidade) {
		$this->st_nomeentidade = $st_nomeentidade;
	}

	/**
	 * @param string $st_siglaentidade
	 */
	public function setSt_siglaentidade($st_siglaentidade)
	{
		$this->st_siglaentidade = $st_siglaentidade;
	}

	/**
	 * @return string
	 */
	public function getSt_siglaentidade()
	{
		return $this->st_siglaentidade;
	}

	/**
	 * @param string $st_apelido
	 */
	public function setSt_apelido($st_apelido)
	{
		$this->st_apelido = $st_apelido;
	}

	/**
	 * @return string
	 */
	public function getSt_apelido()
	{
		return $this->st_apelido;
	}

	/**
	 * @return int
	 */
	public function getId_esquemaconfiguracao()
	{
		return $this->id_esquemaconfiguracao;
	}

	/**
	 * @param int $id_esquemaconfiguracao
	 */
	public function setId_esquemaconfiguracao($id_esquemaconfiguracao)
	{
		$this->id_esquemaconfiguracao = $id_esquemaconfiguracao;
		return $this;

	}

    /**
     * @return mixed
     */
    public function getId_usuariosecretariado()
    {
        return $this->id_usuariosecretariado;
    }

    /**
     * @param mixed $id_usuariosecretariado
     * @return $this;
     */
    public function setId_usuariosecretariado($id_usuariosecretariado)
    {
        $this->id_usuariosecretariado = $id_usuariosecretariado;
        return $this;
    }

}

?>
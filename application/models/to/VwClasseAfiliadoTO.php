<?php
/**
 * Classe para encapsular os dados da vw_classeafiliado.
 * @author Rafael Rocha rafael.rocha.mg@gmail.com
 * @package models
 * @subpackage to
 */
class VwClasseAfiliadoTO extends ClasseAfiliadoTO {
	
	public $st_tipopessoa;
	public $st_tipovalorcomissao;
	public $st_situacao;
	public $st_textosistema;
	public $st_contratoafiliado;
	
	
	/**
	 * @return the $st_tipopessoa
	 */
	public function getSt_tipopessoa() {
		return $this->st_tipopessoa;
	}

	/**
	 * @param field_type $st_tipopessoa
	 */
	public function setSt_tipopessoa($st_tipopessoa) {
		$this->st_tipopessoa = $st_tipopessoa;
	}

	/**
	 * @return the $st_tipovalorcomissao
	 */
	public function getSt_tipovalorcomissao() {
		return $this->st_tipovalorcomissao;
	}

	/**
	 * @param field_type $st_tipovalorcomissao
	 */
	public function setSt_tipovalorcomissao($st_tipovalorcomissao) {
		$this->st_tipovalorcomissao = $st_tipovalorcomissao;
	}

	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}

	/**
	 * @param field_type $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

	/**
	 * @return the $st_textosistema
	 */
	public function getSt_textosistema() {
		return $this->st_textosistema;
	}

	/**
	 * @param field_type $st_textosistema
	 */
	public function setSt_textosistema($st_textosistema) {
		$this->st_textosistema = $st_textosistema;
	}
	/**
	 * @return the $st_contratoafiliado
	 */
	public function getSt_contratoafiliado() {
		return $this->st_contratoafiliado;
	}

	/**
	 * @param field_type $st_contratoafiliado
	 */
	public function setSt_contratoafiliado($st_contratoafiliado) {
		$this->st_contratoafiliado = $st_contratoafiliado;
	}

	

	
	
	
}

?>
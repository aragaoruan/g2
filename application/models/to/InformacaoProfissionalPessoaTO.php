<?php
/**
 * Classe para encapsular os dados de informação profissional da pessoa.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class InformacaoProfissionalPessoaTO extends Ead1_TO_Dinamico {

	/**
	 * Atributo que contem a pk da tabela
	 * @var int
	 */
	public $id_informacaoprofissionalpessoa;
	
	/**
	 * Atributo que contém o id do usuário.
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Atributo que contém o id da entidade da pessoa.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Atributo que contém o cargo da pessoa.
	 * @var string
	 */
	public $st_cargo;
	
	/**
	 * Atributo que contém a organização da pessoa.
	 * @var string
	 */
	public $st_organizacao;
	
	/**
	 * Atributo que contém o id da área de atuação da pessoa.
	 * @var int
	 */
	public $id_areaatuacao;
	
	/**
	 * Data de início da carreira profissional.
	 * @var String
	 */
	public $dt_inicio;
	
	/**
	 * Data de fim de carreira profissional até hoje.
	 * @var string
	 */
	public $dt_fim;
	
	/**
	 * @return string
	 */
	public function getDt_fim() {
		return $this->dt_fim;
	}
	
	/**
	 * @return String
	 */
	public function getDt_inicio() {
		return $this->dt_inicio;
	}
	
	/**
	 * @param string $dt_fim
	 */
	public function setDt_fim($dt_fim) {
		$this->dt_fim = $dt_fim;
	}
	
	/**
	 * @param String $dt_inicio
	 */
	public function setDt_inicio($dt_inicio) {
		$this->dt_inicio = $dt_inicio;
	}
	/**
	 * @return int
	 */
	public function getId_areaatuacao() {
		return $this->id_areaatuacao;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}
	
	/**
	 * @return string
	 */
	public function getSt_cargo() {
		return $this->st_cargo;
	}
	
	/**
	 * @return string
	 */
	public function getSt_organizacao() {
		return $this->st_organizacao;
	}
	
	/**
	 * @param int $id_areaatuacao
	 */
	public function setId_areaatuacao($id_areaatuacao) {
		$this->id_areaatuacao = $id_areaatuacao;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}
	
	/**
	 * @param string $st_cargo
	 */
	public function setSt_cargo($st_cargo) {
		$this->st_cargo = $st_cargo;
	}
	
	/**
	 * @param string $st_organizacao
	 */
	public function setSt_organizacao($st_organizacao) {
		$this->st_organizacao = $st_organizacao;
	}
	/**
	 * @return the $id_informacaoprofissionalpessoa
	 */
	public function getId_informacaoprofissionalpessoa() {
		return $this->id_informacaoprofissionalpessoa;
	}

	/**
	 * @param $id_informacaoprofissionalpessoa the $id_informacaoprofissionalpessoa to set
	 */
	public function setId_informacaoprofissionalpessoa($id_informacaoprofissionalpessoa) {
		$this->id_informacaoprofissionalpessoa = $id_informacaoprofissionalpessoa;
	}


}

?>
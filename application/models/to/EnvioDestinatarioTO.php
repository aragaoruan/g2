<?php

/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
class EnvioDestinatarioTO extends Ead1_TO_Dinamico
{

    public $id_enviodestinatario;
    public $id_tipodestinatario;
    public $id_enviomensagem;
    public $id_usuario;
    public $st_endereco;
    public $st_nome;
    public $id_matricula;
    public $nu_telefone;
    public $id_evolucao;

    /**
     * 
     * @return integer
     */
    public function getId_evolucao ()
    {
        return $this->id_evolucao;
    }

    /**
     * 
     * @param integer $id_evolucao
     */
    public function setId_evolucao ($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
    }

    /**
     * @return the $id_enviodestinatario
     */
    public function getId_enviodestinatario ()
    {
        return $this->id_enviodestinatario;
    }

    /**
     * @return the $id_tipodestinatario
     */
    public function getId_tipodestinatario ()
    {
        return $this->id_tipodestinatario;
    }

    /**
     * @return the $id_enviomensagem
     */
    public function getId_enviomensagem ()
    {
        return $this->id_enviomensagem;
    }

    /**
     * @return the $id_usuario
     */
    public function getId_usuario ()
    {
        return $this->id_usuario;
    }

    /**
     * @return the $st_endereco
     */
    public function getSt_endereco ()
    {
        return $this->st_endereco;
    }

    /**
     * @return the $st_nome
     */
    public function getSt_nome ()
    {
        return $this->st_nome;
    }

    /**
     * @param field_type $id_enviodestinatario
     */
    public function setId_enviodestinatario ($id_enviodestinatario)
    {
        $this->id_enviodestinatario = $id_enviodestinatario;
    }

    /**
     * @param integer $id_tipodestinatario
     */
    public function setId_tipodestinatario ($id_tipodestinatario)
    {
        $this->id_tipodestinatario = $id_tipodestinatario;
    }

    /**
     * @param field_type $id_enviomensagem
     */
    public function setId_enviomensagem ($id_enviomensagem)
    {
        $this->id_enviomensagem = $id_enviomensagem;
    }

    /**
     * @param integer $id_usuario
     */
    public function setId_usuario ($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @param field_type $st_endereco
     */
    public function setSt_endereco ($st_endereco)
    {
        $this->st_endereco = $st_endereco;
    }

    /**
     * @param field_type $st_nome
     */
    public function setSt_nome ($st_nome)
    {
        $this->st_nome = $st_nome;
    }

    /**
     * @return the $id_matricula
     */
    public function getId_matricula ()
    {
        return $this->id_matricula;
    }

    /**
     * @param field_type $id_matricula
     */
    public function setId_matricula ($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @param field_type $nu_telefone
     */
    public function setNu_telefone ($nu_telefone)
    {
        $this->nu_telefone = $nu_telefone;
    }

    /**
     * @return the $nu_telefone
     */
    public function getNu_telefone ()
    {
        return $this->nu_telefone;
    }

}

<?php
/**
 * Classe de encapsulamento dos atributos da tabela TB_TIPODOCUMENTO
 * @author Arthur Cláudio de Almeida Pereira <arthur.almeida@ead1.com.br>
 * @package models
 * @subpackage to
 */
class TipoDocumentoTO extends Ead1_TO_Dinamico{
	
	/**
	 * 
	 * @var int
	 */
	public $id_tipodocumento;
	
	/**
	 * 
	 * @var string
	 */
	public $st_tipodocumento;
	
	/**
	 * 
	 * @var boolean 
	 */
	public $bl_ativo;
	
	
	
	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @param $bl_ativo the $bl_ativo to set
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @return the $id_tipodocumento
	 */
	public function getId_tipodocumento() {
		return $this->id_tipodocumento;
	}

	/**
	 * @return the $st_tipodocumento
	 */
	public function getSt_tipodocumento() {
		return $this->st_tipodocumento;
	}

	/**
	 * @param $id_tipodocumento the $id_tipodocumento to set
	 */
	public function setId_tipodocumento($id_tipodocumento) {
		$this->id_tipodocumento = $id_tipodocumento;
	}

	/**
	 * @param $st_tipodocumento the $st_tipodocumento to set
	 */
	public function setSt_tipodocumento($st_tipodocumento) {
		$this->st_tipodocumento = $st_tipodocumento;
	}

	
	

	
	
}
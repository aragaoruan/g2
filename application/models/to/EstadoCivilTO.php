<?php
/**
 * Classe para encapsular dados de estado civil.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class EstadoCivilTO extends Ead1_TO_Dinamico {

	const SOLTEIRO = 1;
	const CASADO = 2;
	const SEPARADO = 3;
	const VIUVO = 4;
	
	/**
	 * Contém o id do estado civil.
	 * @var int
	 */
	public $id_estadocivil;
	
	/**
	 * Contém o nome do estado civil.
	 * @var String
	 */
	public $st_estadocivil;
	
	/**
	 * @return int
	 */
	public function getId_estadocivil() {
		return $this->id_estadocivil;
	}
	
	/**
	 * @return String
	 */
	public function getSt_estadocivil() {
		return $this->st_estadocivil;
	}
	
	/**
	 * @param int $id_estadocivil
	 */
	public function setId_estadocivil($id_estadocivil) {
		$this->id_estadocivil = $id_estadocivil;
	}
	
	/**
	 * @param String $st_estadocivil
	 */
	public function setSt_estadocivil($st_estadocivil) {
		$this->st_estadocivil = $st_estadocivil;
	}

}

?>
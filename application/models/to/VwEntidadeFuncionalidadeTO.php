<?php
/**
 * Classe para encapsular os dados da view de entidade funcionalidade
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class VwEntidadeFuncionalidadeTO extends Ead1_TO_Dinamico {
	
	/**
	 * Contem o id da entidade
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Contem o nome da entidade
	 * @var string
	 */
	public $st_nomeentidade;
	
	/**
	 * Contem o id da funcionalidade
	 * @var int
	 */
	public $id_funcionalidade;
	
	/**
	 * Contem o nome da funcionalidade
	 * @var int
	 */
	public $st_funcionalidade;
	
	/**
	 * Contem o id da funcionalidade pai
	 * @var int
	 */
	public $id_funcionalidadepai;
	
	/**
	 * Contem o valor se a funcionalidade esta ativa
	 * @var int
	 */
	public $bl_funcionalidadeativo;
	
	/**
	 * Contem o valor se a funcionalidade da entidade esta ativa
	 * @var int
	 */
	public $bl_entidadefuncionalidadeativo;
	
	/**
	 * Contem o id da situacao da funcionalidade
	 * @var int
	 */
	public $id_situacaofuncionalidade;
	
	/**
	 * Contem o nome da situacao da funcionalidade
	 * @var string
	 */
	public $st_situacaofuncionalidade;
	
	/**
	 * Contem o id da situacao da funcionalidade da entidade
	 * @var int
	 */
	public $id_situacaoentidadefuncionalidade;
	
	/**
	 * Contem o nome da situacao da funcionalidade da entidade
	 * @var string
	 */
	public $st_situacaoentidadefuncionalidade;
	
	/**
	 * Contem o nome da classe flex
	 * @var string
	 */
	public $st_classeflex;
	
	/**
	 * Contem a url do icone
	 * @var string
	 */
	public $st_urlicone;
	
	/**
	 * Contem o id do tipo da funcionalidade
	 * @var int
	 */
	public $id_tipofuncionalidade;
	
	/**
	 * Contem o nome do tipo da funcionalidade
	 * @var string
	 */
	public $st_tipofuncionalidade;
	
	/**
	 * Contem a ordem da funcionalidade
	 * @var int
	 */
	public $nu_ordemfuncionalidade;
	
	/**
	 * Contem a ordem da funcionalidade da entidade
	 * @var int
	 */
	public $nu_ordementidadefuncionalidade;
	
	/**
	 * Contem o nome da classe flex do botao
	 * @var string
	 */
	public $st_classeflexbotao;
	
	/**
	 * Contem o nome da label da funcionalidade da entidade
	 * @var string
	 */
	public $st_label;
	
	/**
	 * Contem o valor se a funcionalidade da entidade é obrigatoria
	 * @var int
	 */
	public $bl_obrigatorio;
	
	/**
	 * Contem o valor se a funcionalidade da entidade esta visivel
	 * @var int
	 */
	public $bl_visivel;
	
	/**
	 * Contém o id do sistema da funcionalidade.
	 * @var int
	 */
	public $id_sistema;
	
	/**
	 * Contem o nome do sistema da funcionalidade
	 * @var String
	 */
	public $st_sistema;
	
	/**
	 * @return int
	 */
	public function getId_sistema() {
		return $this->id_sistema;
	}
	
	/**
	 * @param int $id_sistema
	 */
	public function setId_sistema($id_sistema) {
		$this->id_sistema = $id_sistema;
	}

	
	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $st_nomeentidade
	 */
	public function getSt_nomeentidade() {
		return $this->st_nomeentidade;
	}

	/**
	 * @return the $id_funcionalidade
	 */
	public function getId_funcionalidade() {
		return $this->id_funcionalidade;
	}

	/**
	 * @return the $st_funcionalidade
	 */
	public function getSt_funcionalidade() {
		return $this->st_funcionalidade;
	}

	/**
	 * @return the $id_funcionalidadepai
	 */
	public function getId_funcionalidadepai() {
		return $this->id_funcionalidadepai;
	}

	/**
	 * @return the $bl_funcionalidadeativo
	 */
	public function getBl_funcionalidadeativo() {
		return $this->bl_funcionalidadeativo;
	}

	/**
	 * @return the $bl_entidadefuncionalidadeativo
	 */
	public function getBl_entidadefuncionalidadeativo() {
		return $this->bl_entidadefuncionalidadeativo;
	}

	/**
	 * @return the $id_situacaofuncionalidade
	 */
	public function getId_situacaofuncionalidade() {
		return $this->id_situacaofuncionalidade;
	}

	/**
	 * @return the $st_situacaofuncionalidade
	 */
	public function getSt_situacaofuncionalidade() {
		return $this->st_situacaofuncionalidade;
	}

	/**
	 * @return the $id_situacaoentidadefuncionalidade
	 */
	public function getId_situacaoentidadefuncionalidade() {
		return $this->id_situacaoentidadefuncionalidade;
	}

	/**
	 * @return the $st_situacaoentidadefuncionalidade
	 */
	public function getSt_situacaoentidadefuncionalidade() {
		return $this->st_situacaoentidadefuncionalidade;
	}

	/**
	 * @return the $st_classeflex
	 */
	public function getSt_classeflex() {
		return $this->st_classeflex;
	}

	/**
	 * @return the $st_urlicone
	 */
	public function getSt_urlicone() {
		return $this->st_urlicone;
	}

	/**
	 * @return the $id_tipofuncionalidade
	 */
	public function getId_tipofuncionalidade() {
		return $this->id_tipofuncionalidade;
	}

	/**
	 * @return the $st_tipofuncionalidade
	 */
	public function getSt_tipofuncionalidade() {
		return $this->st_tipofuncionalidade;
	}

	/**
	 * @return the $nu_ordemfuncionalidade
	 */
	public function getNu_ordemfuncionalidade() {
		return $this->nu_ordemfuncionalidade;
	}

	/**
	 * @return the $nu_ordementidadefuncionalidade
	 */
	public function getNu_ordementidadefuncionalidade() {
		return $this->nu_ordementidadefuncionalidade;
	}

	/**
	 * @return the $st_classeflexbotao
	 */
	public function getSt_classeflexbotao() {
		return $this->st_classeflexbotao;
	}

	/**
	 * @return the $st_label
	 */
	public function getSt_label() {
		return $this->st_label;
	}

	/**
	 * @return the $bl_obrigatorio
	 */
	public function getBl_obrigatorio() {
		return $this->bl_obrigatorio;
	}

	/**
	 * @return the $bl_visivel
	 */
	public function getBl_visivel() {
		return $this->bl_visivel;
	}

	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param string $st_nomeentidade
	 */
	public function setSt_nomeentidade($st_nomeentidade) {
		$this->st_nomeentidade = $st_nomeentidade;
	}

	/**
	 * @param int $id_funcionalidade
	 */
	public function setId_funcionalidade($id_funcionalidade) {
		$this->id_funcionalidade = $id_funcionalidade;
	}

	/**
	 * @param int $st_funcionalidade
	 */
	public function setSt_funcionalidade($st_funcionalidade) {
		$this->st_funcionalidade = $st_funcionalidade;
	}

	/**
	 * @param int $id_funcionalidadepai
	 */
	public function setId_funcionalidadepai($id_funcionalidadepai) {
		$this->id_funcionalidadepai = $id_funcionalidadepai;
	}

	/**
	 * @param int $bl_funcionalidadeativo
	 */
	public function setBl_funcionalidadeativo($bl_funcionalidadeativo) {
		$this->bl_funcionalidadeativo = $bl_funcionalidadeativo;
	}

	/**
	 * @param int $bl_entidadefuncionalidadeativo
	 */
	public function setBl_entidadefuncionalidadeativo($bl_entidadefuncionalidadeativo) {
		$this->bl_entidadefuncionalidadeativo = $bl_entidadefuncionalidadeativo;
	}

	/**
	 * @param int $id_situacaofuncionalidade
	 */
	public function setId_situacaofuncionalidade($id_situacaofuncionalidade) {
		$this->id_situacaofuncionalidade = $id_situacaofuncionalidade;
	}

	/**
	 * @param string $st_situacaofuncionalidade
	 */
	public function setSt_situacaofuncionalidade($st_situacaofuncionalidade) {
		$this->st_situacaofuncionalidade = $st_situacaofuncionalidade;
	}

	/**
	 * @param int $id_situacaoentidadefuncionalidade
	 */
	public function setId_situacaoentidadefuncionalidade($id_situacaoentidadefuncionalidade) {
		$this->id_situacaoentidadefuncionalidade = $id_situacaoentidadefuncionalidade;
	}

	/**
	 * @param string $st_situacaoentidadefuncionalidade
	 */
	public function setSt_situacaoentidadefuncionalidade($st_situacaoentidadefuncionalidade) {
		$this->st_situacaoentidadefuncionalidade = $st_situacaoentidadefuncionalidade;
	}

	/**
	 * @param string $st_classeflex
	 */
	public function setSt_classeflex($st_classeflex) {
		$this->st_classeflex = $st_classeflex;
	}

	/**
	 * @param string $st_urlicone
	 */
	public function setSt_urlicone($st_urlicone) {
		$this->st_urlicone = $st_urlicone;
	}

	/**
	 * @param int $id_tipofuncionalidade
	 */
	public function setId_tipofuncionalidade($id_tipofuncionalidade) {
		$this->id_tipofuncionalidade = $id_tipofuncionalidade;
	}

	/**
	 * @param string $st_tipofuncionalidade
	 */
	public function setSt_tipofuncionalidade($st_tipofuncionalidade) {
		$this->st_tipofuncionalidade = $st_tipofuncionalidade;
	}

	/**
	 * @param int $nu_ordemfuncionalidade
	 */
	public function setNu_ordemfuncionalidade($nu_ordemfuncionalidade) {
		$this->nu_ordemfuncionalidade = $nu_ordemfuncionalidade;
	}

	/**
	 * @param int $nu_ordementidadefuncionalidade
	 */
	public function setNu_ordementidadefuncionalidade($nu_ordementidadefuncionalidade) {
		$this->nu_ordementidadefuncionalidade = $nu_ordementidadefuncionalidade;
	}

	/**
	 * @param string $st_classeflexbotao
	 */
	public function setSt_classeflexbotao($st_classeflexbotao) {
		$this->st_classeflexbotao = $st_classeflexbotao;
	}

	/**
	 * @param string $st_label
	 */
	public function setSt_label($st_label) {
		$this->st_label = $st_label;
	}

	/**
	 * @param int $bl_obrigatorio
	 */
	public function setBl_obrigatorio($bl_obrigatorio) {
		$this->bl_obrigatorio = $bl_obrigatorio;
	}

	/**
	 * @param int $bl_visivel
	 */
	public function setBl_visivel($bl_visivel) {
		$this->bl_visivel = $bl_visivel;
	}
	/**
	 * @return the $st_sistema
	 */
	public function getSt_sistema() {
		return $this->st_sistema;
	}

	/**
	 * @param String $st_sistema
	 */
	public function setSt_sistema($st_sistema) {
		$this->st_sistema = $st_sistema;
	}


	
}
<?php

/**
 * Classe para encapsular da tabela
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class AvaliacaoAlunoTO extends Ead1_TO_Dinamico
{

    const ATIVO = 86;
    const INATIVO = 87;
    const REVISADA = 88;
    const NOTA_NORMAL = 1;
    const NOTA_CREDITO_PARCIAL = 2;

    public $id_avaliacaoaluno;
    public $id_matricula;
    public $id_avaliacaoconjuntoreferencia;
    public $id_avaliacao;
    public $id_avaliacaoagendamento;
    public $st_nota;
    public $dt_cadastro;
    public $id_usuariocadastro;
    public $bl_ativo;
    public $dt_avaliacao;
    public $id_situacao;
    public $id_usuariorevisor;
    public $id_upload;
    public $st_tituloavaliacao;
    public $dt_defesa;
    public $st_justificativa;
    public $id_avaliacaoalunoorigem;
    public $id_tiponota;
    public $id_usuarioalteracao;
    public $id_notaconceitual;
    public $id_sistema;

    /**
     * @return $st_tituloavaliacao
     */
    public function getSt_tituloavaliacao()
    {
        return $this->st_tituloavaliacao;
    }

    /**
     * @param $st_tituloavaliacao
     */
    public function setSt_tituloavaliacao($st_tituloavaliacao)
    {
        $this->st_tituloavaliacao = $st_tituloavaliacao;
    }

    /**
     * @return $id_upload
     */
    public function getId_upload()
    {
        return $this->id_upload;
    }

    /**
     * @param $id_avaliacaoaluno
     */
    public function setId_avaliacaoaluno($id_avaliacaoaluno)
    {
        $this->id_avaliacaoaluno = $id_avaliacaoaluno;
    }

    /**
     * @param $id_upload
     */
    public function setId_upload($id_upload)
    {
        $this->id_upload = $id_upload;
    }

    /**
     * @return $id_avaliacaoaluno
     */
    public function getId_avaliacaoaluno()
    {
        return $this->id_avaliacaoaluno;
    }

    /**
     * @return $id_avaliacaoalunoorigem
     */
    public function getId_avaliacaoalunoorigem()
    {
        return $this->id_avaliacaoalunoorigem;
    }

    /**
     * @return $id_matricula
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @return $id_avaliacaoconjuntoreferencia
     */
    public function getId_avaliacaoconjuntoreferencia()
    {
        return $this->id_avaliacaoconjuntoreferencia;
    }

    /**
     * @return $id_avaliacao
     */
    public function getId_avaliacao()
    {
        return $this->id_avaliacao;
    }

    /**
     * @return $id_avaliacaoagendamento
     */
    public function getId_avaliacaoagendamento()
    {
        return $this->id_avaliacaoagendamento;
    }

    /**
     * @return $st_nota
     */
    public function getSt_nota()
    {
        return $this->st_nota;
    }

    /**
     * @return $dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @return $id_usuariocadastro
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @return $bl_ativo
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @return $dt_avaliacao
     */
    public function getDt_avaliacao()
    {
        return $this->dt_avaliacao;
    }

    /**
     * @return $dt_defesa
     */
    public function getDt_defesa()
    {
        return $this->dt_defesa;
    }

    /**
     * @return $id_situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }


    /**
     * @param $id_avaliacaoalunoorigem
     */
    public function setId_avaliacaoalunoorigem($id_avaliacaoalunoorigem)
    {
        $this->id_avaliacaoalunoorigem = $id_avaliacaoalunoorigem;
    }

    /**
     * @param $id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @param $id_avaliacaoconjuntoreferencia
     */
    public function setId_avaliacaoconjuntoreferencia($id_avaliacaoconjuntoreferencia)
    {
        $this->id_avaliacaoconjuntoreferencia = $id_avaliacaoconjuntoreferencia;
    }

    /**
     * @param $id_avaliacao
     */
    public function setId_avaliacao($id_avaliacao)
    {
        $this->id_avaliacao = $id_avaliacao;
    }

    /**
     * @param $id_avaliacaoagendamento
     */
    public function setId_avaliacaoagendamento($id_avaliacaoagendamento)
    {
        $this->id_avaliacaoagendamento = $id_avaliacaoagendamento;
    }

    /**
     * @param $st_nota
     */
    public function setSt_nota($st_nota, $numFormat = true)
    {
        if($numFormat)
            $this->st_nota = number_format(floatval($st_nota), 2, '.', '');
        else
            $this->st_nota = $st_nota;
    }

    /**
     * @param $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @param $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @param $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @param $dt_avaliacao
     */
    public function setDt_avaliacao($dt_avaliacao)
    {
        $this->dt_avaliacao = $dt_avaliacao;
    }

    /**
     * @param $dt_defesa
     */
    public function setDt_defesa($dt_defesa)
    {
        $this->dt_defesa = $dt_defesa;
    }

    /**
     * @param $id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    /**
     * @return $id_usuariorevisor
     */
    public function getId_usuariorevisor()
    {
        return $this->id_usuariorevisor;
    }

    /**
     * @param $id_usuariorevisor
     */
    public function setId_usuariorevisor($id_usuariorevisor)
    {
        $this->id_usuariorevisor = $id_usuariorevisor;
    }

    /**
     * @return $st_justificativa
     */
    public function getSt_justificativa()
    {
        return $this->st_justificativa;
    }

    /**
     * @param $st_justificativa
     */
    public function setSt_justificativa($st_justificativa)
    {
        $this->st_justificativa = $st_justificativa;
    }

    /**
     * @return $id_tiponota
     */
    public function getId_tiponota()
    {
        return $this->id_tiponota;
    }

    /**
     * @param $st_justificativa
     */
    public function setId_tiponota($id_tiponota)
    {
        $this->id_tiponota = $id_tiponota;
    }

    /**
     * @return $id_usuarioalteracao
     */
    public function getId_usuarioalteracao()
    {
        return $this->id_usuarioalteracao;
    }

    /**
     * @param $id_usuarioalteracao
     */
    public function setId_usuarioalteracao($id_usuarioalteracao)
    {
        $this->id_usuarioalteracao = $id_usuarioalteracao;
    }

    /**
     * @return mixed
     */
    public function getId_sistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param mixed $id_sistema
     */
    public function setId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
    }



    /**
     * @return mixed
     */
    public function getId_notaconceitual()
    {
        return $this->id_notaconceitual;
    }

    /**
     * @param mixed $id_notaconceitual
     */
    public function setId_notaconceitual($id_notaconceitual)
    {
        $this->id_notaconceitual = $id_notaconceitual;
    }

}

<?php
/**
 * Classe que encapsula os dados da tb_categoriaproduto
 * @package models
 * @subpackage to
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 */
class CategoriaProdutoTO extends Ead1_TO_Dinamico {

	public $id_categoria;
	public $id_produto;
	public $id_usuariocadastro;
	public $dt_cadastro;
	
	/**
	 * @return the $id_categoria
	 */
	public function getId_categoria() {
		return $this->id_categoria;
	}

	/**
	 * @param field_type $id_categoria
	 */
	public function setId_categoria($id_categoria) {
		$this->id_categoria = $id_categoria;
	}

	/**
	 * @return the $id_produto
	 */
	public function getId_produto() {
		return $this->id_produto;
	}

	/**
	 * @param field_type $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}
}

?>
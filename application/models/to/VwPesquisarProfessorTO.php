<?php
/**
 * Classe para encapsular os dados de view que mostra professor
 * @author Dimas Sulz <dimassulz@gmail.com>
 * @package models
 * @subpackage to
 */
class VwPesquisarProfessorTO extends Ead1_TO_Dinamico {
	/**
	 * Contém o id do usuário
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Contém o id da entidade
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * 
	 * @var string
	 */
	public $st_nomeexibicao;
	
	/**
	 * 
	 * @var int
	 */
	public $id_situacao;
	
	/**
	 * 
	 * @var string
	 */
	public $st_nomecompleto;
	
	/**
	 * 
	 * @var string
	 */
	public $st_nomeentidade;
	
	/**
	 * 
	 * @var unknown_type
	 */
	public $id_perfil;
	
	/**
	 * 
	 * @var string
	 */
	public $st_nomeperfil;
	
	/**
	 * 
	 * @var int
	 */
	public $id_perfilpedagogico;
	
	/**
	 * 
	 * @var string
	 */
	public $st_perfilpedagogico;
	
	/**
	 * 
	 * @var int
	 */
	public $id_entidadeidentificacao;
	
	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $st_nomeexibicao
	 */
	public function getSt_nomeexibicao() {
		return $this->st_nomeexibicao;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $st_nomecompleto
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}

	/**
	 * @return the $st_nomeentidade
	 */
	public function getSt_nomeentidade() {
		return $this->st_nomeentidade;
	}

	/**
	 * @return the $id_perfil
	 */
	public function getId_perfil() {
		return $this->id_perfil;
	}

	/**
	 * @return the $st_nomeperfil
	 */
	public function getSt_nomeperfil() {
		return $this->st_nomeperfil;
	}

	/**
	 * @return the $id_perfilpedagogico
	 */
	public function getId_perfilpedagogico() {
		return $this->id_perfilpedagogico;
	}

	/**
	 * @return the $st_perfilpedagogico
	 */
	public function getSt_perfilpedagogico() {
		return $this->st_perfilpedagogico;
	}

	/**
	 * @param $id_usuario the $id_usuario to set
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param $id_entidade the $id_entidade to set
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param $st_nomeexibicao the $st_nomeexibicao to set
	 */
	public function setSt_nomeexibicao($st_nomeexibicao) {
		$this->st_nomeexibicao = $st_nomeexibicao;
	}

	/**
	 * @param $id_situacao the $id_situacao to set
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param $st_nomecompleto the $st_nomecompleto to set
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @param $st_nomeentidade the $st_nomeentidade to set
	 */
	public function setSt_nomeentidade($st_nomeentidade) {
		$this->st_nomeentidade = $st_nomeentidade;
	}

	/**
	 * @param $id_perfil the $id_perfil to set
	 */
	public function setId_perfil($id_perfil) {
		$this->id_perfil = $id_perfil;
	}

	/**
	 * @param $st_nomeperfil the $st_nomeperfil to set
	 */
	public function setSt_nomeperfil($st_nomeperfil) {
		$this->st_nomeperfil = $st_nomeperfil;
	}

	/**
	 * @param $id_perfilpedagogico the $id_perfilpedagogico to set
	 */
	public function setId_perfilpedagogico($id_perfilpedagogico) {
		$this->id_perfilpedagogico = $id_perfilpedagogico;
	}

	/**
	 * @param $st_perfilpedagogico the $st_perfilpedagogico to set
	 */
	public function setSt_perfilpedagogico($st_perfilpedagogico) {
		$this->st_perfilpedagogico = $st_perfilpedagogico;
	}
	/**
	 * @return the $id_entidadeidentificacao
	 */
	public function getId_entidadeidentificacao() {
		return $this->id_entidadeidentificacao;
	}

	/**
	 * @param $id_entidadeidentificacao the $id_entidadeidentificacao to set
	 */
	public function setId_entidadeidentificacao($id_entidadeidentificacao) {
		$this->id_entidadeidentificacao = $id_entidadeidentificacao;
	}


}

?>
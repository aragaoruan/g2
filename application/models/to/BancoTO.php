<?php
/**
 * Classe para encapsular os dados do banco.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class BancoTO extends Ead1_TO_Dinamico {

	/**
	 * Atributo que contém o id do banco.
	 * @var int
	 */
	public $id_banco;
	
	/**
	 * Atributo que contém o nome do banco.
	 * @var string
	 */
	public $st_nomebanco;
	
	/**
	 * Atributo que contém o número do banco
	 * @var string
	 */
	public $st_banco;
	/**
	 * @return the $id_banco
	 */
	public function getId_banco() {
		return $this->id_banco;
	}

	/**
	 * @return the $st_nomebanco
	 */
	public function getSt_nomebanco() {
		return $this->st_nomebanco;
	}

	/**
	 * @return the $st_banco
	 */
	public function getSt_banco() {
		return $this->st_banco;
	}

	/**
	 * @param int $id_banco
	 */
	public function setId_banco($id_banco) {
		$this->id_banco = $id_banco;
	}

	/**
	 * @param string $st_nomebanco
	 */
	public function setSt_nomebanco($st_nomebanco) {
		$this->st_nomebanco = $st_nomebanco;
	}

	/**
	 * @param string $st_banco
	 */
	public function setSt_banco($st_banco) {
		$this->st_banco = $st_banco;
	}

}

?>
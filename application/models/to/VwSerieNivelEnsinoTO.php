<?php
/**
 * Classe para encapsular os dados de view de série e nível de ensino.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @since 03/12/2010
 * 
 * @package models
 * @subpackage to
 */
class VwSerieNivelEnsinoTO extends Ead1_TO_Dinamico {

	/**
	 * Id da série.
	 * @var int
	 */
	public $id_serie;
	
	/**
	 * Nome da série.
	 * @var String
	 */
	public $st_serie;
	
	/**
	 * Id da série anterior.
	 * @var int
	 */
	public $id_serieanterior;
	
	/**
	 * Id do nível de ensino.
	 * @var int
	 */
	public $id_nivelensino;
	
	/**
	 * Nível Ensino.
	 * @var String
	 */
	public $st_nivelensino;
	
	/**
	 * @return int
	 */
	public function getId_nivelensino() {
		return $this->id_nivelensino;
	}
	
	/**
	 * @return int
	 */
	public function getId_serie() {
		return $this->id_serie;
	}
	
	/**
	 * @return int
	 */
	public function getId_serieanterior() {
		return $this->id_serieanterior;
	}
	
	/**
	 * @return String
	 */
	public function getSt_nivelensino() {
		return $this->st_nivelensino;
	}
	
	/**
	 * @return String
	 */
	public function getSt_serie() {
		return $this->st_serie;
	}
	
	/**
	 * @param int $id_nivelensino
	 */
	public function setId_nivelensino($id_nivelensino) {
		$this->id_nivelensino = $id_nivelensino;
	}
	
	/**
	 * @param int $id_serie
	 */
	public function setId_serie($id_serie) {
		$this->id_serie = $id_serie;
	}
	
	/**
	 * @param int $id_serieanterior
	 */
	public function setId_serieanterior($id_serieanterior) {
		$this->id_serieanterior = $id_serieanterior;
	}
	
	/**
	 * @param String $st_nivelensino
	 */
	public function setSt_nivelensino($st_nivelensino) {
		$this->st_nivelensino = $st_nivelensino;
	}
	
	/**
	 * @param String $st_serie
	 */
	public function setSt_serie($st_serie) {
		$this->st_serie = $st_serie;
	}

}

?>
<?php

/**
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage to
 *
 */
class EncerramentoAlocacaoTO extends Ead1_TO_Dinamico {

	public $id_encerramentosala;
	public $id_alocacao;
	
	/**
	 * @return the $id_encerramentosala
	 */
	public function getId_encerramentosala() {
		return $this->id_encerramentosala;
	}

	/**
	 * @param field_type $id_encerramentosala
	 */
	public function setId_encerramentosala($id_encerramentosala) {
		$this->id_encerramentosala = $id_encerramentosala;
	}

	/**
	 * @return the $id_alocacao
	 */
	public function getId_alocacao() {
		return $this->id_alocacao;
	}

	/**
	 * @param field_type $id_alocacao
	 */
	public function setId_alocacao($id_alocacao) {
		$this->id_alocacao = $id_alocacao;
	}


		


	
}

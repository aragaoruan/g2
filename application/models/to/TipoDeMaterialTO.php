<?php
/**
 * Classe para encapsular os dados de tipo de material..
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class TipoDeMaterialTO extends Ead1_TO_Dinamico {

	/**
	 * Id do tipo de material.
	 * @var int
	 */
	public $id_tipodematerial;
	
	/**
	 * Nome do tipo de material.
	 * @var string
	 */
	public $st_tipodematerial;
	
	/**
	 * Id da situação.
	 * @var int
	 */
	public $id_situacao;
	
	/**
	 * Id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}
	
	/**
	 * @return int
	 */
	public function getId_tipodematerial() {
		return $this->id_tipodematerial;
	}
	
	/**
	 * @return string
	 */
	public function getSt_tipodematerial() {
		return $this->st_tipodematerial;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}
	
	/**
	 * @param int $id_tipodematerial
	 */
	public function setId_tipodematerial($id_tipodematerial) {
		$this->id_tipodematerial = $id_tipodematerial;
	}
	
	/**
	 * @param string $st_tipodematerial
	 */
	public function setSt_tipodematerial($st_tipodematerial) {
		$this->st_tipodematerial = $st_tipodematerial;
	}

}

?>